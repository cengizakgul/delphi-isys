// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_SB_UTILS;

interface

uses
  Windows, wwdbdatetimepicker, SFrameEntry, Dialogs, Menus,
  Db, Buttons, StdCtrls, ComCtrls, Grids, Wwdbigrd, ISZippingRoutines,
  Wwdbgrid, Controls, ExtCtrls, Classes, Wwdatsrc, wwdblook, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, Printers, wwriched, EvConsts,
  SDataStructure, EvUtils, SSecurityInterface, EvStreamUtils, Variants,
  EvBasicUtils, SDDClasses, ISBasicClasses, EvContext, EvLegacy,
  kbmMemTable, ISKbmMemDataSet,
  DateUtils, EvDataAccessComponents,
  ISDataAccessComponents, SReportSettings, SShowMessage, SFieldCodeValues,
  StrUtils, MoveClient, SDataDictsystem, SDataDictTemp, SDataDictBureau,
  SACHTypes, evAchBase, EvCommonInterfaces, EvMainboard, EvExceptions,
  isBaseClasses, isBasicUtils, evAch, evClasses,
  XLSFile, XLSWorkbook,  LMDCustomButton, LMDButton, SDueDateCalculation,
  isUILMDButton, Forms, EvUIUtils, EvUIComponents, EvClientDataSet, EvPayroll;

type
  TEDIT_SB_UTILS = class(TFrameEntry)
    Panel1: TevPanel;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    Splitter1: TevSplitter;
    Panel2: TevPanel;
    wwgdClient: TevDBGrid;
    Panel3: TevPanel;
    wwdsClient: TevDataSource;
    TabSheet2: TTabSheet;
    evRichEdit1: TevRichEdit;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    btnACHKeyUtility: TevBitBtn;
    btnCreateZeroAch: TevBitBtn;
    btnQECCleanup: TevBitBtn;
    btnPrint: TevBitBtn;
    btnAutoCreatePayroll: TevBitBtn;
    btnLockQuarterlies: TevBitBtn;
    PopupMenu1: TPopupMenu;
    btnReplaceTaxDueDate: TevBitBtn;
    LoadCompaniesfromTextFile1: TMenuItem;
    dTxtFileDialog: TOpenDialog;
    btnSetW2PaperType: TevBitBtn;
    btnChangeSuiRates: TevBitBtn;
    btnExtendCalendar: TevBitBtn;
    evcsHoliday: TevClientDataSet;
    Calendar: TEvBasicClientDataSet;
    CalendarPAY_FREQUENCY: TStringField;
    CalendarCHECK_DATE: TDateField;
    CalendarPERIOD_BEGIN_DATE: TDateField;
    CalendarPERIOD_END_DATE: TDateField;
    CalendarCALL_IN_DATE: TDateTimeField;
    CalendarDELIVERY_DATE: TDateTimeField;
    CalendarLAST_REAL_CHECK_DATE: TDateField;
    CalendarLAST_REAL_CALL_IN_DATE: TDateTimeField;
    CalendarLAST_REAL_DELIVERY_DATE: TDateTimeField;
    btnUpdateFUTA: TevBitBtn;
    btnFUICredit: TevBitBtn;
    CalendarDAYS_PRIOR: TIntegerField;                  
    CalendarDAYS_POST: TIntegerField;
    OpenDialog1: TOpenDialog;
    btnRelinkMoved: TevBitBtn;
    bbtnPrintAdjReps: TevBitBtn;
    btnChangeEEActiveStatus: TevBitBtn;
    btnChangeSBBank: TevBitBtn;
    bbtnImportSUIRates: TevBitBtn;
    btnApplySUI: TevBitBtn;
    sbUtils: TScrollBox;
    pnlFixedSize: TevPanel;
    btnTaxableWagesAdjustment: TevBitBtn;
    btnCreditHold: TevBitBtn;
    btnHSACleanup: TevBitBtn;
    btnIgnoreFICA: TevBitBtn;
    btnCleanUpLocals: TevBitBtn;
    btnMCTUpdate: TevBitBtn;
    btnRecipBugFix: TevBitBtn;
    btnChangeTaxPaymentMethod: TevBitBtn;
    btnChangeEeAcaFormsType: TevBitBtn;
    btnACAOtherCO: TevBitBtn;
    procedure btnQECCleanupClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnACHKeyUtilityClick(Sender: TObject);
    procedure btnCreateZeroAchClick(Sender: TObject);
    procedure btnAutoCreatePayrollClick(Sender: TObject);
    procedure btnLockQuarterliesClick(Sender: TObject);
    procedure btnReplaceTaxDueDateClick(Sender: TObject);
    procedure LoadCompaniesfromTextFile1Click(Sender: TObject);
    procedure btnSetW2PaperTypeClick(Sender: TObject);
    procedure btnChangeSuiRatesClick(Sender: TObject);
    procedure btnExtendCalendarClick(Sender: TObject);
    procedure btnUpdateFUTAClick(Sender: TObject);
    procedure btnFUICreditClick(Sender: TObject);
    procedure btnRelinkMovedClick(Sender: TObject);
    procedure bbtnPrintAdjRepsClick(Sender: TObject);
    procedure btnChangeEEActiveStatusClick(Sender: TObject);
    procedure btnChangeSBBankClick(Sender: TObject);
    procedure bbtnImportSUIRatesClick(Sender: TObject);
    procedure btnApplySUIClick(Sender: TObject);
    procedure btnTaxableWagesAdjustmentClick(Sender: TObject);
    procedure btnCreditHoldClick(Sender: TObject);
    procedure btnHSACleanupClick(Sender: TObject);
    procedure btnIgnoreFICAClick(Sender: TObject);
    procedure btnCleanUpLocalsClick(Sender: TObject);
    procedure btnMCTUpdateClick(Sender: TObject);
    procedure btnRecipBugFixClick(Sender: TObject);
    procedure btnChangeTaxPaymentMethodClick(Sender: TObject);
    procedure btnChangeEeAcaFormsTypeClick(Sender: TObject);
    procedure btnACAOtherCOClick(Sender: TObject);

  private
    FClientNbr: Integer;
    procedure SetClientNbr(const Value: Integer);
    { Private declarations }
    procedure PrintAdjustmentReports(dat_b, dat_e: TDateTime; RunThroughQueue: boolean; PrintReports: boolean; TaxAdjLimit: Double);

    procedure CreateFUICreditReductionReport(const cd:TevClientDataSet;const reportPath:String);
  public
    property ClientNbr: Integer read FClientNbr write SetClientNbr;
    { Public declarations }
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet;
      var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_UTILS: TEDIT_SB_UTILS;

implementation

uses
   EvTypes, SysUtils, EvDataset, SQECParameters, SPD_EDIT_SB_UTILS_DETAIL, evAchEFT, SPD_EDIT_ZERO_ACH_PAYMENT,
   SPopulateRecords, LockQuarterliesDlg, SPD_EDIT_UPDATE_STATE_DUE_DATE, ChoseW2Dlg, ChangeSUIRateDlg,
   SAdjReportParamsDlg, SPayrollReport, SPD_EDIT_SB_EE_DBDT, SelectSBBankAccount, UpdateEEFICAFlag,
   SPD_EDIT_SB_IMPORT_SUI_RATES, SPD_EDIT_APPLY_SUI, SPD_EDIT_TaxWageAdjustment, SCalendarCreation,
   SPD_EDIT_HSACleanup, MCTTaxRateUpdate, SPD_EDIT_CHANGE_TAX_PAY_METHOD, SelectComboChoicesDlg,
  SDataDictclient;

{$R *.DFM}

procedure TEDIT_SB_UTILS.SetClientNbr(const Value: Integer);
begin
  FClientNbr := Value;
end;

procedure evDatasetToXls(DataSet: IevDataSet; const FileName: String);
var
  S: TSheet;
  R, i: integer;
  XLS: TXLSFile;
begin
  XLS := TXLSFile.Create;
  S := XLS.Workbook.Sheets[0];
  try
    for i := 0 to DataSet.Fields.Count - 1 do
      S.Cells[0, i].Value := DataSet.Fields[i].FieldName;
    R := 1;
    DataSet.First;
    while not DataSet.Eof do
    begin
      for i := 0 to DataSet.Fields.Count - 1 do
        if not DataSet.Fields[i].IsNull then
          S.Cells[R, i].Value := DataSet.Fields[i].Value;
      DataSet.Next;
      Inc(R);
    end;
    XLS.SaveAs(FileName);
  finally
    XLS.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnQECCleanupClick(Sender: TObject);
var
  iIndex2: Integer;
  EECustomNbr: string;
  QuarterDate: TDateTime;
  MinTax: Double;
  CleanupAction: TCleanupPayrollAction;
  ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean;
  t: IevQECTask;
  V: Variant;

begin
  inherited;

  if EvMessage('Are you sure you want to create Quarter End Cleanup for all selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  QECParameters := TQECParameters.Create(Nil);
  try
    if QECParameters.ShowModal <> mrOK then
      Exit;
    ForceCleanup := QECParameters.cbForceCleanup.Checked;
    if QECParameters.cbProcess.Checked then
      CleanupAction := cpaProcess
    else
    if QECParameters.cbSendToQueue.Checked then
      CleanupAction := cpaQueue
    else
      CleanupAction := cpaNone;
    PrintReports := QECParameters.cbPrintReports.Checked;
    CurrentQuarterOnly := QECParameters.cbCurrentQuarterOnly.Checked;
    if QECParameters.cbAllEmployees.Checked then
      EECustomNbr := ''
    else
      EECustomNbr := QECParameters.editEmployees.Text;
    QuarterDate := QECParameters.dtQuarterEndDate.Date;
    MinTax := StrToFloat(QECParameters.editMinTax.Text);
    V := VarArrayOf([BoolToYN(QECParameters.cbFederal.Checked), BoolToYN(QECParameters.cbEEOASDI.Checked),
      BoolToYN(QECParameters.cbEROASDI.Checked), BoolToYN(QECParameters.cbEEMedicare.Checked),
      BoolToYN(QECParameters.cbERMedicare.Checked), BoolToYN(QECParameters.cbERFUI.Checked),
      BoolToYN(QECParameters.cbState.Checked), BoolToYN(QECParameters.cbEESDI.Checked),
      BoolToYN(QECParameters.cbERSDI.Checked), BoolToYN(QECParameters.cbSUI.Checked),
      BoolToYN(QECParameters.cbLocal.Checked), BoolToYN(QECParameters.cbQtrEndCheckDate.Checked),
      BoolToYN(False)]);
  finally
    QECParameters.Free;
  end;

    t := mb_TaskQueue.CreateTask(QUEUE_TASK_QEC, True) as IevQECTask;
    t.ClNbr := ctx_DataAccess.ClientID;
    t.CoNbr := DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger;

    for iIndex2 := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[iIndex2]);
      with t.Filter.Add do
      begin
        ClNbr := wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger;
        CoNbr := wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger;
        Caption := 'Co #' + Trim(wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString)
                    + ' Quarter End Date: ' + DateToStr(QuarterDate);
      end;
    end;

    t.QTREndDate := QuarterDate;
    t.MinTax := MinTax;
    t.CleanupAction := Ord(CleanupAction);
    t.EECustomNbr := EECustomNbr;
    t.ForceCleanup := ForceCleanup;
    t.PrintReports := PrintReports;
    t.CurrentQuarterOnly := CurrentQuarterOnly;
    t.ExtraOptions := VarToStr(V[0]) + VarToStr(V[1]) + VarToStr(V[2]) + VarToStr(V[3]) + VarToStr(V[4])
      + VarToStr(V[5]) + VarToStr(V[6]) + VarToStr(V[7]) + VarToStr(V[8]) + VarToStr(V[9]) + VarToStr(V[10])
      + VarToStr(V[11]);
    t.Caption := 'Co #' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
               + ' Quarter End Date: ' + DateToStr(QuarterDate);
    ctx_EvolutionGUI.AddTaskQueue(t);

end;


procedure TEDIT_SB_UTILS.Deactivate;
begin
  inherited;
    DM_TEMPORARY.TMP_CO.Filter := '';
    DM_TEMPORARY.TMP_CO.Filtered := false;
end;

procedure TEDIT_SB_UTILS.Activate;
var
 sFilter : String;

begin
  inherited;

  sFilter := GetReadOnlyClintCompanyListFilter(false);
  if sFilter <> '' then
  begin
    DM_TEMPORARY.TMP_CO.Filter := sFilter;
    DM_TEMPORARY.TMP_CO.Filtered := true;
  end;
  wwdsClient.DataSet := CreateLookupDataset(DM_TEMPORARY.TMP_CO, Self);

  wwgdClient.Selected.Text :=
    'CUSTOM_COMPANY_NUMBER' + #9 + '8' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
    'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
  wwgdClient.ApplySelected;
  btnLockQuarterlies.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_CHANGE_QUARTER_LOCK_DATE') = stEnabled;
  bbtnImportSUIRates.Enabled := ctx_AccountRights.Functions.GetState('UPDATE_AS_OF') = stEnabled;

end;

procedure TEDIT_SB_UTILS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_TEMPORARY.TMP_CO, DM_SERVICE_BUREAU.SB,
    DM_COMPANY.CO, DM_CLIENT.CL], SupportDataSets, '');
end;


procedure TEDIT_SB_UTILS.btnPrintClick(Sender: TObject);
begin
  inherited;
  evRichEdit1.Print('');
end;

procedure TEDIT_SB_UTILS.btnACHKeyUtilityClick(Sender: TObject);
var
  i: Integer;
  d: TevClientDataSet;
  dBookMark: TBookMark;
  dRecNo: Integer;
  dProcessed: Integer;
  sState, BDate, EDate, FreqType, sACHDate, ACHSuffix, s: string;
  ACHDate: TDateTime;
  UtilHelper: TEDIT_SB_UTILS_DETAIL;
  UseCheckDate, AndSui: Boolean;
begin
  UtilHelper := TEDIT_SB_UTILS_DETAIL.Create(Self);
  UtilHelper.TurnTabsOff;
  UtilHelper.PageControl1.ActivePageIndex := 0;
  if UtilHelper.ShowModal = mrOk then
  begin
    sState := UpperCase(UtilHelper.evedState.Text);
    FreqType := UtilHelper.evdcDepFreq.Value;
    BDate := UtilHelper.evdtBDate.Text;
    EDate := UtilHelper.evdtEDate.Text;
    ACHDate := UtilHelper.evdtACHDate.Date;
    ACHSuffix := UtilHelper.evedKeySuffix.Text;
    UseCheckDate := (UtilHelper.CheckBox1.Checked);
    AndSui := UtilHelper.cbAndSui.Checked;
  end
  else
    Exit;
  DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE=''' + sState + '''');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('SY_STATES_NBR=' + IntToStr(DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR']) + ' and FREQUENCY_TYPE=''' + FreqType + '''');
  s := '-1';
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.First;
  while not DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Eof do
  begin
    s := s + ',' + IntToStr(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ['SY_STATE_DEPOSIT_FREQ_NBR']);
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Next;
  end;
  DM_TEMPORARY.TMP_CO_STATES.DataRequired('SY_STATE_DEPOSIT_FREQ_NBR in (' + s + ')');
  dProcessed := 0;
  ctx_DataAccess.OpenClient(0);
  d := wwgdClient.DataSource.DataSet as TevClientDataSet;
  dRecNo := d.RecNo;
  d.DisableControls;
  ctx_StartWait('Preparing...', wwgdClient.SelectedList.Count);
  try
    d.First;
    while not d.Eof do
    begin
      dBookMark := d.GetBookmark;
      try
        for i := 0 to wwgdClient.SelectedList.Count - 1 do
          if d.CompareBookmarks(dBookMark, wwgdClient.SelectedList[i]) = 0 then
          begin
            Inc(dProcessed);
            if DM_TEMPORARY.TMP_CO_STATES.Locate('CL_NBR;CO_NBR', d['CL_NBR;CO_NBR'], []) then
            begin
              ctx_UpdateWait('Processing ' + d['NAME'], dProcessed);
              ctx_DataAccess.OpenClient(d['CL_NBR']);
              with DM_COMPANY do
              begin
                CO_STATES.DataRequired('CO_STATES_NBR=' + IntToStr(DM_TEMPORARY.TMP_CO_STATES['CO_STATES_NBR']));
                if CO_STATES.RecordCount > 0 then
                begin
                  ctx_DataAccess.StartNestedTransaction([dbtClient]);
                  try
                    CO_STATE_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR is null and CO_STATES_NBR=' + IntToStr(CO_STATES['CO_STATES_NBR']) + ' and CHECK_DATE between ''' + bDate + ''' and ''' + EDate + '''');
                    CO_STATE_TAX_LIABILITIES.First;
                    if UseCheckDate then
                    begin
                      ACHDate := EncodeDate(1900, 1, 1);
                      while not CO_STATE_TAX_LIABILITIES.Eof do
                      begin
                        if not VarIsNull(CO_STATE_TAX_LIABILITIES['CHECK_DATE']) then
                          if ACHDate < CO_STATE_TAX_LIABILITIES['CHECK_DATE'] then
                            ACHDate := CO_STATE_TAX_LIABILITIES['CHECK_DATE'];
                        CO_STATE_TAX_LIABILITIES.Next;
                      end;
                      CO_STATE_TAX_LIABILITIES.First;
                    end;
                    sACHDate := FormatDateTime('MM/DD/YYYY', ACHDate) + ACHSuffix;
                    while not CO_STATE_TAX_LIABILITIES.Eof do
                    begin
                      CO_STATE_TAX_LIABILITIES.Edit;
                      CO_STATE_TAX_LIABILITIES['ACH_KEY'] := sACHDate;
                      CO_STATE_TAX_LIABILITIES.Post;
                      CO_STATE_TAX_LIABILITIES.Next;
                    end;
                    ctx_DataAccess.PostDataSets([CO_STATE_TAX_LIABILITIES]);
                    if AndSui then
                    begin
                      CO_SUI.DataRequired('CO_STATES_NBR=' + IntToStr(CO_STATES['CO_STATES_NBR']));
                      s := '-1';
                      CO_SUI.First;
                      while not CO_SUI.Eof do
                      begin
                        s := s + ',' + IntToStr(CO_SUI['CO_SUI_NBR']);
                        CO_SUI.Next;
                      end;
                      CO_SUI_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR is null and CO_SUI_NBR in (' + s + ') and CHECK_DATE between ''' + bDate + ''' and ''' + EDate + '''');
                      CO_SUI_LIABILITIES.First;
                      while not CO_SUI_LIABILITIES.Eof do
                      begin
                        CO_SUI_LIABILITIES.Edit;
                        CO_SUI_LIABILITIES['ACH_KEY'] := sACHDate;
                        CO_SUI_LIABILITIES.Post;
                        CO_SUI_LIABILITIES.Next;
                      end;
                      ctx_DataAccess.PostDataSets([CO_SUI_LIABILITIES]);
                    end;
                    ctx_DataAccess.CommitNestedTransaction;
                    Break;
                  except
                    ctx_DataAccess.RollbackNestedTransaction;
                    raise;
                  end;
                end;
              end;
            end;
          end;
      finally
        d.FreeBookmark(dBookMark);
      end;
      d.Next;
    end;
  finally
    DM_SYSTEM_STATE.SY_STATES.Close;
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Close;
    DM_TEMPORARY.TMP_CO_STATES.Close;
    ctx_DataAccess.OpenClient(0);
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL['CL_NBR']);
    d.RecNo := dRecNo;
    d.EnableControls;
    ctx_EndWait;
  end;
end;

procedure TEDIT_SB_UTILS.btnCreateZeroAchClick(Sender: TObject);
var
  i: Integer;
  d: TevClientDataSet;
  dBookMark: TBookMark;
  dRecNo: Integer;
  dProcessed: Integer;
  AgenciesFilter, StateByAgency, SuiByAgency, StateByNbr, LocalByAgency: TevClientDataSet;
  DueDate, EffectiveDate, PerionEndDate: TDateTime;
  PaymentSeqNumber: Integer;
  FAch: TevAchEFT;
  bSave: Boolean;
  AchFile: IevAchFile;
  Tran: IevTransaction;
  AchOptions: IevAchOptions;
  TempString: String;
  AchFiles: IisListOfValues;
  CompanyDatas: String;
begin
  dProcessed := 0;
  ctx_DataAccess.OpenClient(0);
  d := wwgdClient.DataSource.DataSet as TevClientDataSet;
  dRecNo := d.RecNo;
  d.DisableControls;
  AgenciesFilter := TevClientDataSet.Create(nil);
  StateByAgency := TevClientDataSet.Create(nil);
  LocalByAgency := TevClientDataSet.Create(nil);
  StateByNbr := TevClientDataSet.Create(nil);
  SuiByAgency := TevClientDataSet.Create(nil);
  try
    AgenciesFilter.ProviderName := ctx_DataAccess.SY_CUSTOM_VIEW.ProviderName;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'SY_GLOBAL_AGENCY_NBR, AGENCY_NAME');
      SetMacro('TABLENAME', 'SY_GLOBAL_AGENCY');
      SetMacro('CONDITION', '(STRLEN(Trim(RECEIVING_ABA_NUMBER)) > 0 and STRLEN(Trim(RECEIVING_ACCOUNT_NUMBER)) > 0 ) ');
      AgenciesFilter.DataRequest(AsVariant);
    end;
    AgenciesFilter.Open;
    with TEDIT_ZERO_ACH_PAYMENT.Create(nil) do
    try
      evDBLookupCombo1.LookupTable := AgenciesFilter;
      if ShowModal = mrOk then
      begin
        DueDate := dtpDueDate.Date;
        EffectiveDate := dtpEffectiveDate.Date;
        PerionEndDate := dtpPeriodEndDate.Date;
        PaymentSeqNumber := edPaymentSeqNumber.Value;
        Assert(AgenciesFilter.Locate('SY_GLOBAL_AGENCY_NBR', evDBLookupCombo1.LookupValue, []));
      end
      else
        Exit;
    finally
      Free;
    end;
    FAch := TevAchEFT.Create(nil);
    LoadACHOptionsFromRegistry(FAch.Options);

    TempString := FormatDateTime('YYYYMMDDHHNNSS', Now);

    FAch.Options.EffectiveDate := False;

    ctx_StartWait('Preparing...', wwgdClient.SelectedList.Count);
    try
      PopulateDataSets([DM_SYSTEM_STATE.SY_STATES,
                        DM_SYSTEM_STATE.SY_SUI,
                        DM_SYSTEM_STATE.SY_LOCALS,
                        DM_TEMPORARY.TMP_CO_STATES,
                        DM_TEMPORARY.TMP_CO_LOCAL_TAX]);
                        
      ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES,
                    DM_SYSTEM_STATE.SY_SUI,
                    DM_SYSTEM_STATE.SY_LOCALS,
                    DM_TEMPORARY.TMP_CO_STATES,
                    DM_TEMPORARY.TMP_CO_LOCAL_TAX]);

      StateByAgency.Data := DM_SYSTEM_STATE.SY_STATES.Data;
      SuiByAgency.Data := DM_SYSTEM_STATE.SY_SUI.Data;
      LocalByAgency.Data := DM_SYSTEM_STATE.SY_LOCALS.Data;

      StateByAgency.IndexFieldNames := 'SY_STATE_TAX_PMT_AGENCY_NBR';
      SuiByAgency.IndexFieldNames := 'SY_SUI_TAX_PMT_AGENCY_NBR';
      LocalByAgency.IndexFieldNames := 'SY_LOCAL_TAX_PMT_AGENCY_NBR';

      FAch.StartPoolingAch;
      d.First;
      try
        while not d.Eof do
        begin
          dBookMark := d.GetBookmark;
          try
            for i := 0 to wwgdClient.SelectedList.Count - 1 do
            begin
              if d.CompareBookmarks(dBookMark, wwgdClient.SelectedList[i]) = 0 then
              begin
                Inc(dProcessed);
                ctx_UpdateWait('Processing ' + d['NAME'], dProcessed);
                bSave := False;
                FAch.Backup;
                try
                  //AgenciesFilter.First;
                  //while not AgenciesFilter.Eof do
                  begin
                    StateByAgency.SetRange([AgenciesFilter['SY_GLOBAL_AGENCY_NBR']], [AgenciesFilter['SY_GLOBAL_AGENCY_NBR']]);
                    StateByAgency.First;
                    while not StateByAgency.Eof do
                    begin
                      if DM_TEMPORARY.TMP_CO_STATES.Locate('CL_NBR;CO_NBR;STATE', VarArrayOf([d['CL_NBR'], d['CO_NBR'], StateByAgency['STATE']]), [])
                        and (string(DM_TEMPORARY.TMP_CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]) then
                      begin
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                        with TExecDSWrapper.Create('GenericSelectWithCondition') do
                        begin
                          SetMacro('COLUMNS', 'COUNT( * ) RES');
                          SetMacro('TABLENAME', 'TMP_CO_STATE_TAX_LIABILITIES');
                          SetMacro('CONDITION', 'CL_NBR = :CLNBR and CO_NBR = :CONBR and DUE_DATE = :DUEDATE and STATE = :STATE');
                          SetParam('CLNBR', d['CL_NBR']);
                          SetParam('CONBR', d['CO_NBR']);
                          SetParam('DUEDATE', DueDate);
                          SetParam('STATE', StateByAgency.FieldByName('STATE').AsString);
                          ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
                        end;
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
                        if ctx_DataAccess.TEMP_CUSTOM_VIEW['RES'] = 0 then
                        begin //state
                          ctx_DataAccess.OpenClient(d['CL_NBR']);
                          DM_COMPANY.CO_STATES.DataRequired('ALL');
                          if DM_COMPANY.CO_STATES.Locate('CO_NBR;STATE', VarArrayOf([d['CO_NBR'], StateByAgency['STATE']]), [])
                            and not VarIsNull(DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']) then
                          begin
                            FAch.ProcessEFTPayment(0, Null, EffectiveDate,
                              PerionEndDate, //EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2))),
                              PaymentSeqNumber, //StrToInt(Copy(s, 11, 1)),
                              DM_COMPANY.CO_STATES['CO_STATES_NBR'],
                              0 {CO_TAX_PAYMENT_ACH['CO_TAX_PAYMENT_ACH_NBR']}, ttState);
                            bSave := True;
                          end;
                        end;
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                      end;
                      StateByAgency.Next;
                    end;
                    SuiByAgency.SetRange([AgenciesFilter['SY_GLOBAL_AGENCY_NBR']], [AgenciesFilter['SY_GLOBAL_AGENCY_NBR']]);
                    SuiByAgency.First;
                    while not SuiByAgency.Eof do
                    begin
                      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                      with TExecDSWrapper.Create('GenericSelectWithCondition') do
                      begin
                        SetMacro('COLUMNS', 'COUNT( * ) RES');
                        SetMacro('TABLENAME', 'TMP_CO_SUI_LIABILITIES');
                        SetMacro('CONDITION', 'CL_NBR = :CLNBR and CO_NBR = :CONBR and DUE_DATE = :DUEDATE and SY_SUI_NBR = :SY_SUI_NBR');
                        SetParam('CLNBR', d['CL_NBR']);
                        SetParam('CONBR', d['CO_NBR']);
                        SetParam('DUEDATE', DueDate);
                        SetParam('SY_SUI_NBR', SuiByAgency['SY_SUI_NBR']);
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
                      end;
                      ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
                      if ctx_DataAccess.TEMP_CUSTOM_VIEW['RES'] = 0 then
                      begin //sui
                        ctx_DataAccess.OpenClient(d['CL_NBR']);
                        DM_COMPANY.CO_SUI.DataRequired('ALL');
                        DM_COMPANY.CO_STATES.DataRequired('ALL');
                        DM_COMPANY.CO.DataRequired('ALL');
                        if DM_COMPANY.CO_SUI.Locate('CO_NBR;SY_SUI_NBR', VarArrayOf([d['CO_NBR'], SuiByAgency['SY_SUI_NBR']]), [])
                          and (string(DM_COMPANY.CO_SUI['PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]) then
                        begin
                          Assert(DM_COMPANY.CO_STATES.Locate('CO_STATES_NBR', DM_COMPANY.CO_SUI['CO_STATES_NBR'], []));
                          if not VarIsNull(DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']) then
                          begin
                            FAch.ProcessEFTPayment(0, Null, EffectiveDate,
                              PerionEndDate, //EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2))),
                              PaymentSeqNumber, //StrToInt(Copy(s, 11, 1)),
                              DM_COMPANY.CO_SUI['CO_SUI_NBR'],
                              0 {CO_TAX_PAYMENT_ACH['CO_TAX_PAYMENT_ACH_NBR']}, ttSui);
                            bSave := True;
                          end;
                        end;
                      end;
                      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                      SuiByAgency.Next;
                    end;
                    LocalByAgency.SetRange([AgenciesFilter['SY_GLOBAL_AGENCY_NBR']], [AgenciesFilter['SY_GLOBAL_AGENCY_NBR']]);
                    LocalByAgency.First;
                    while not LocalByAgency.Eof do
                    begin
                      if DM_TEMPORARY.TMP_CO_LOCAL_TAX.Locate('CL_NBR;CO_NBR;SY_LOCALS_NBR', VarArrayOf([d['CL_NBR'], d['CO_NBR'], LocalByAgency['SY_LOCALS_NBR']]), [])
                        and (string(DM_TEMPORARY.TMP_CO_LOCAL_TAX['PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]) then
                      begin
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                        with TExecDSWrapper.Create('GenericSelectWithCondition') do
                        begin
                          SetMacro('COLUMNS', 'COUNT( * ) RES');
                          SetMacro('TABLENAME', 'TMP_CO_LOCAL_TAX_LIABILITIES');
                          SetMacro('CONDITION', 'CL_NBR = :CLNBR and CO_NBR = :CONBR and DUE_DATE = :DUEDATE and SY_LOCALS_NBR = :SY_LOCALS_NBR');
                          SetParam('CLNBR', d['CL_NBR']);
                          SetParam('CONBR', d['CO_NBR']);
                          SetParam('DUEDATE', DueDate);
                          SetParam('SY_LOCALS_NBR', LocalByAgency.FieldByName('SY_LOCALS_NBR').AsString);
                          ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
                        end;
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
                        if ctx_DataAccess.TEMP_CUSTOM_VIEW['RES'] = 0 then
                        begin //
                          ctx_DataAccess.OpenClient(d['CL_NBR']);
                          DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
                          if DM_COMPANY.CO_LOCAL_TAX.Locate('CO_NBR;SY_LOCALS_NBR', VarArrayOf([d['CO_NBR'], LocalByAgency['SY_LOCALS_NBR']]), [])
                            and not VarIsNull(DM_COMPANY.CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR']) then
                          begin
                            FAch.ProcessEFTPayment(0, Null, EffectiveDate,
                              PerionEndDate, //EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2))),
                              PaymentSeqNumber, //StrToInt(Copy(s, 11, 1)),
                              DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'],
                              0, ttLoc);
                            bSave := True;
                          end;
                        end;
                        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
                      end;
                      LocalByAgency.Next;
                    end;
                    //AgenciesFilter.Next;
                  end;
                  if bSave then
                  begin

                    FAch.SaveClientAch(False);


                  end;
                except
                  on E: Exception do
                  begin
                    FAch.Restore;
                    EvMessage('Company ' + QuotedStr(d['NAME']) + ' raised an exception:'#13 + E.Message +
                      #13#13'The company is skipped. Process Ok to continue to next company.');
                  end;
                end;
                Break;
              end;
            end;
          finally
            d.FreeBookmark(dBookMark);
          end;
          d.Next;
        end;
      finally

        AchOptions := TevAchOptions.Create;
        FAch.Options.AssignTo2(AchOptions);

        AchFile := TevAchFile.Create(AchOptions,
          FAch.ORIGIN_BANK_ABA,
          DM_SERVICE_BUREAU.SB_BANKS.FieldByName('CTSID').AsString,
          FAch.Origin_Name,
          FAch.Dest_Name,
          FAch.Dest_Bank_Aba,
          FAch.Origin_Number,
          FAch.ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger,
          False, OFF_DONT_USE);

        AchFile.InTesting := False;

        FAch.ACHFile.Filter := 'ACH_TYPE = ''6:*'' or ACH_TYPE = ''7:*'' or ACH_TYPE = ''5:*''';
        FAch.ACHFile.Filtered := True;
        FAch.ACHFile.First;

        CompanyDatas := '';

        while not FAch.ACHFile.Eof do
        begin
          if copy(FAch.ACHFile.ACH_TYPE.AsString,1,1) = '5' then
          begin
            CompanyDatas := copy(FAch.ACHFile.ACH_LINE.AsString, 21,20);
          end
          else
          if copy(FAch.ACHFile.ACH_TYPE.AsString,1,1) = '6' then
          begin
            ctx_DataAccess.OpenClient(FAch.ACHFile.CL_NBR.AsInteger);
            DM_CLIENT.CL.Open;
            DM_COMPANY.CO.DataRequired('CO_NBR='+FAch.ACHFile.CO_NBR.AsString);
            AchFile.CL_NBR                        := FAch.ACHFile.CL_NBR.AsInteger;
            AchFile.CO_NBR                        := FAch.ACHFile.CO_NBR.AsInteger;
            AchFile.PRINT_CLIENT_NAME             := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
            AchFile.BANK_ACCOUNT_REGISTER_NAME    := DM_COMPANY.CO.BANK_ACCOUNT_REGISTER_NAME.AsString;
            AchFile.CLIENT_NAME                   := DM_CLIENT.CL.NAME.AsString;
            AchFile.LEGAL_NAME                    := DM_COMPANY.CO.LEGAL_NAME.AsString;
            AchFile.CUSTOM_COMPANY_NUMBER         := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
            AchFile.COMPANY_NAME                  := DM_COMPANY.CO.NAME.AsString;
            AchFile.WELLS_FARGO_ACH_FLAG          := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.AsString;
            AchFile.COMPANY_IDENTIFICATION        := FAch.ACHFile.FEIN.AsString;
            Tran := TevTransaction.Create;
            Tran.CompanyDatas := CompanyDatas;
            Tran.Amount := FAch.ACHFile.AMOUNT.AsFloat;
            Tran.ABA_NUMBER := FAch.ACHFile.ABA_NUMBER.AsString;
            Tran.BANK_ACCOUNT_NUMBER := FAch.ACHFile.BANK_ACCOUNT_NUMBER.AsString;
            if copy(FAch.ACHFile.TRAN.AsString,1,1) = '3' then
              Tran.BANK_ACCOUNT_TYPE := RECEIVING_ACCT_TYPE_SAVING
            else
              Tran.BANK_ACCOUNT_TYPE := RECEIVING_ACCT_TYPE_CHECKING;
            Tran.NAME := FAch.ACHFile.NAME.AsString;
            Tran.CUSTOM_EMPLOYEE_NUMBER := FAch.ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString;
            Tran.NAME := FAch.ACHFile.NAME.AsString;
            Tran.IN_PRENOTE := FAch.ACHFile.IN_PRENOTE.AsString;
            Tran.PR_NBR := FAch.ACHFile.PR_NBR.AsInteger;
            Tran.CHECK_DATE := FAch.ACHFile.EFFECTIVE_DATE.AsDateTime;
            Tran.RUN_NUMBER := FAch.ACHFile.RUN_NUMBER.AsInteger;
            Tran.EFFECTIVE_DATE := FAch.ACHFile.EFFECTIVE_DATE.AsDateTime;
            if copy(FAch.ACHFile.TRAN.AsString, 2, 1) = '9' then
              Tran.TRANSACTION_TYPE := EFT_TAX_PAYMENT_DEBIT_ZERO
            else
              Tran.TRANSACTION_TYPE := EFT_TAX_PAYMENT_CREDIT_ZERO;
            Tran.IMPOUND_TYPE := PAYMENT_TYPE_ACH;
            Tran.BANK_ACCOUNT_NBR := FAch.ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger;
          end;
          FAch.ACHFile.Next;
          if copy(FAch.ACHFile.ACH_TYPE.AsString,1,1) = '7' then
          begin
            Tran.Addenda := FAch.ACHFile.ACH_LINE.AsString;
            Tran.Addenda := PadRight(Tran.Addenda, ' ',87);
            FAch.ACHFile.Next;
          end;
          if Assigned(Tran) then
          begin
            AchFile.AddTransaction(Tran, DM_CLIENT.CL.NAME.AsString,
              DM_COMPANY.CO.LEGAL_NAME.AsString,
              DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString,
              DM_COMPANY.CO.NAME.AsString);
            Tran := nil;
          end;
        end;

        FAch.ACHFile.Filtered := False;
        FAch.StopPoolingAch;

        AchFiles := AchFile.GetAchFiles(TempString);

        FAch.ACHFile.Filtered := False;
        FAch.ACHFile.First;

        FAch.ACHFile.IndexName := '';

        while FAch.ACHFile.RecordCount > 0 do
          FAch.ACHFile.Delete;

        AchFile.PopulateReportDataset(FAch.ACHFile);

        FAch.ACHFile.First;

        if FAch.ACHFiles.Count > 0 then
        begin
          with TSaveDialog.Create(nil) do
          begin
            Application.OnModalBegin(nil);
            try
              FileName := FAch.FileName;
              if Execute then
              begin
//                (IInterface(FAch.ACHFiles[0].Value) as IisStringList).SaveToFile(FileName);

                if AchFiles.Count > 0 then
                  (IInterface(AchFiles.Values[0].Value) as IisStringList).VCLStrings.SaveToFile(FileName);

              end;
            finally
               Application.OnModalEnd(nil);
               Free;
            end;
          end;
        end;

        ctx_UpdateWait('Preparing report...');
        GenerateAchReport(False, True, FAch.FileSaved, FAch.FileName, '', FAch.ACHFile, FAch.ACHDescription);
      end;
    finally
      FreeAndNil(FAch);
      ctx_EndWait;
    end;
  finally
    d.RecNo := dRecNo;
    d.EnableControls;
    AgenciesFilter.Free;
    StateByAgency.Free;
    LocalByAgency.Free;
    StateByNbr.Free;
    SuiByAgency.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnAutoCreatePayrollClick(Sender: TObject);
var
  I: Integer;
  MyList: TStringList;
begin
  inherited;
  MyList := TStringList.Create;
  try
    wwdsClient.DataSet.DisableControls;
    for I := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
      MyList.Add(wwdsClient.DataSet.FieldByName('CL_NBR').AsString + '=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
    end;
    wwdsClient.DataSet.EnableControls;
    if MyList.Count > 0 then
      AutoCreateCompanies(MyList);
  finally
    MyList.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnLockQuarterliesClick(Sender: TObject);
var
  sDate: string;
  sLocked: String;
  I: Integer;
  frm: TLockQuarterliesDlgForm;
begin
  frm := TLockQuarterliesDlgForm.Create(nil);
  try
    if frm.ShowModal = mrOK then
    begin
      sDate := frm.DataEdit.Text;
      if frm.LockCheck.Checked then
        sLocked := 'Y'
      else
        sLocked := 'N';
    end
    else
      Exit;
  finally
    frm.Free;
  end;
  if GetQuarterNumber(StrToDate(sDate) + 1) = GetQuarterNumber(StrToDate(sDate)) then
    raise EInconsistentData.CreateHelp('This is not a quarter end date!', IDH_InconsistentData);
  if EvMessage('If you need to make changes in prior quarters for selected companies you will have to manually unlock them. Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait('Locking companies as of ' + sDate + '...');
  try
    wwdsClient.DataSet.DisableControls;
    for I := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
      ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
      DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      if DM_COMPANY.CO.FieldByName('TERMINATION_CODE').AsString = 'T' then
        continue;
      DM_COMPANY.CO.Edit;
      DM_COMPANY.CO.FieldByName('LOCK_DATE').AsString := sDate;
      DM_COMPANY.CO.FieldByName('QTR_LOCK_FOR_TAX_PMTS').AsString := sLocked;
      DM_COMPANY.CO.Post;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
    end;
    wwdsClient.DataSet.EnableControls;
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_SB_UTILS.btnReplaceTaxDueDateClick(Sender: TObject);
var
  f: TEDIT_UPDATE_STATE_DUE_DATE;
  d: TevClientDataSet;
  i, Processed: Integer;
  dBookMark2, dBookMark: TBookmark;
  cd: TevClientDataSet;
  sCond: string;
  cdTCDLocals: IevDataSet;
  StartDate, EndDate: TDateTime;
  sCheckFilter : String;
begin
  inherited;
  DM_COMPANY.CO_FED_TAX_LIABILITIES.IndexName := '';
  DM_COMPANY.CO_FED_TAX_LIABILITIES.IndexFieldNames := '';
  cd := Nil;
  f := TEDIT_UPDATE_STATE_DUE_DATE.Create(nil);
  try
    if f.ShowModal = mrOk then
    begin
      d := wwgdClient.DataSource.DataSet as TevClientDataSet;
      dBookMark2 := d.GetBookmark;
      d.DisableControls;
      ctx_StartWait('', wwgdClient.SelectedList.Count);
      try
        Processed := 0;
        d.First;
        while not d.Eof do
        begin
          dBookMark := d.GetBookmark;
          try
            for i := 0 to wwgdClient.SelectedList.Count - 1 do
              if d.CompareBookmarks(dBookMark, wwgdClient.SelectedList[i]) = 0 then
              begin
                Inc(Processed);
                ctx_UpdateWait('Processing ' + d['NAME'], Processed);
                ctx_DataAccess.OpenClient(d['CL_NBR']);
                case f.rgTypeChoice.ItemIndex of
                  0:
                    begin
                      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and STATE=' + QuotedStr(f.edState.DisplayValue));
                      if DM_COMPANY.CO_STATES.RecordCount > 0 then
                      begin
                        cd := DM_COMPANY.CO_STATE_TAX_LIABILITIES;
                        cd.DataRequired('CO_STATES_NBR=' + IntToStr(DM_COMPANY.CO_STATES['CO_STATES_NBR']) + '  and DUE_DATE=' + QuotedStr(DateToStr(f.edOldDate.DateTime)));
                      end;
                    end;
                  1:
                    begin
                      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and STATE=' + QuotedStr(f.edState.DisplayValue));
                      if DM_COMPANY.CO_STATES.RecordCount > 0 then
                      begin
                        DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and SY_SUI_NBR = ' + f.edSui.LookupValue);
                        sCond := '-1';
                        DM_COMPANY.CO_SUI.First;
                        while not DM_COMPANY.CO_SUI.Eof do
                        begin
                          sCond := sCond + ',' + IntToStr(DM_COMPANY.CO_SUI['CO_SUI_NBR']);
                          DM_COMPANY.CO_SUI.Next;
                        end;
                        cd := DM_COMPANY.CO_SUI_LIABILITIES;
                        cd.DataRequired('CO_SUI_NBR in (' + sCond + ')  and DUE_DATE=' + QuotedStr(DateToStr(f.edOldDate.DateTime)));
                      end;
                    end;
                  2:
                    begin
                      cd := DM_COMPANY.CO_FED_TAX_LIABILITIES;
                      cd.DataRequired('TAX_TYPE = ' + QuotedStr(TAX_LIABILITY_TYPE_ER_FUI) + ' and DUE_DATE=' + QuotedStr(DateToStr(f.edOldDate.DateTime)));
                    end;
                  3:
                    begin
                      if (f.evTCDLocalTax.State = cbChecked) then
                      begin
                        DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and STATE=' + QuotedStr(f.edState.DisplayValue) + ' and TCD_DEPOSIT_FREQUENCY_NBR is not null ');
                        if DM_COMPANY.CO_STATES.RecordCount > 0 then
                        begin
                          cdTCDLocals:= CreatecdTCDLocalsDataset;
                          DM_COMPANY.CO_LOCAL_TAX.Close;
                          DM_COMPANY.CO_LOCAL_TAX.LookupsEnabled := True;
                          DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR'])+ ' and SY_STATES_NBR=' + IntToStr(DM_COMPANY.CO_STATES['SY_STATES_NBR']));
                          DM_COMPANY.CO_LOCAL_TAX.Filter := 'LOCAL_TYPE in (''N'', ''R'', ''S'' ) and CombineForTaxPayments = ''Y'' ';
                          DM_COMPANY.CO_LOCAL_TAX.Filtered := True;
                          try
                            sCond := '-1';
                            DM_COMPANY.CO_LOCAL_TAX.First;
                            while not DM_COMPANY.CO_LOCAL_TAX.Eof do
                            begin
                             if cdTCDLocals.Locate('CO_LOCAL_TAX_NBR', DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'], [])  then
                               sCond := sCond + ',' + IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']);
                             DM_COMPANY.CO_LOCAL_TAX.Next;
                            end;
                          finally
                            DM_COMPANY.CO_LOCAL_TAX.Filter := '';
                            DM_COMPANY.CO_LOCAL_TAX.Filtered := false;
                            StartDate := EncodeDate(StrToInt(f.cbYears.Items[f.cbYears.ItemIndex]), f.cbMonth.ItemIndex + 1, 1);
                            EndDate :=  GetEndMonth(StartDate);
                            sCheckFilter :='';
                            if (f.evSelectCheckDate.State = cbChecked) then
                               sCheckFilter :=' and CHECK_DATE between ' + QuotedStr(DateToStr(StartDate)) + ' and ' + QuotedStr(DateToStr(EndDate));

                            cd := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;
                            cd.DataRequired('CO_LOCAL_TAX_NBR in (' + sCond + ') and DUE_DATE=' + QuotedStr(DateToStr(f.edOldDate.DateTime)) + sCheckFilter);
                          end;
                        end;
                      end
                      else
                      begin
                        DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and STATE=' + QuotedStr(f.edState.DisplayValue));
                        if DM_COMPANY.CO_STATES.RecordCount > 0 then
                        begin
                          DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR'])+ ' and SY_STATES_NBR=' + IntToStr(DM_COMPANY.CO_STATES['SY_STATES_NBR'])+ ' and SY_LOCALS_NBR='+IntToStr(f.DM_SYSTEM_STATE.SY_LOCALS['SY_LOCALS_NBR']) );

                          if DM_COMPANY.CO_LOCAL_TAX.RecordCount > 0 then
                          begin
                            cd := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;
                            cd.DataRequired('CO_LOCAL_TAX_NBR=' + IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']) + '  and DUE_DATE=' + QuotedStr(DateToStr(f.edOldDate.DateTime)));
                          end;
                        end;
                       end;
                    end;
                  else
                    Assert(False);
                    cd := nil;
                end;
                if Assigned(cd) then
                begin
                  cd.IndexName := '';
                  with cd do
                  begin
                    First;
                    while not Eof do
                    begin
                      if (cd['Status'] = TAX_DEPOSIT_STATUS_PENDING) or (cd['Status'] = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) or (cd['Status'] = TAX_DEPOSIT_STATUS_IMPOUNDED) then
                      begin
                        if cd.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
                        begin
                          Edit;
                          FieldValues['DUE_DATE'] := f.edNewDate.DateTime;
                          Post;
                        end;
                      end;
                      Next;
                    end;
                  end;
                  ctx_DataAccess.PostDataSets([cd]);
                  cd.Close;
                  cd := nil;
                end;
              end;
          finally
            d.FreeBookmark(dBookMark);
          end;
          d.Next;
        end;
      finally
        ctx_EndWait;
        d.GotoBookmark(dBookMark2);
        d.FreeBookmark(dBookMark2);
        d.EnableControls;
      end;
    end;
  finally
    FreeAndNil(f);
  end;
end;


procedure TEDIT_SB_UTILS.LoadCompaniesfromTextFile1Click(Sender: TObject);
var
  F: TextFile;
  S: string;
begin
  inherited;
  dTxtFileDialog.InitialDir := ExtractFilePath(Application.ExeName);
  if dTxtFileDialog.Execute then
  begin
    AssignFile(F, dTxtFileDialog.FileName);
    Reset(F);
    wwdsClient.DataSet.DisableControls;
    try
      while not EOF(F) do
      begin
        Readln(F, S);
        if wwdsClient.DataSet.Locate('CUSTOM_COMPANY_NUMBER', S, []) then
          wwgdClient.SelectRecord;
      end;
    finally
      wwdsClient.DataSet.EnableControls;
      CloseFile(F);
    end;
  end;
end;

procedure TEDIT_SB_UTILS.btnSetW2PaperTypeClick(Sender: TObject);
var
  I: Integer;
  sType: string;
begin
  with TChosePaperType.Create(nil) do
  try
    if ShowModal = mrOK then
      sType := Values[evRadioGroup1.ItemIndex]
    else
      Exit;
  finally
    Free;
  end;
  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    try
      for I := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
        ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
        DM_COMPANY.CO_ENLIST_GROUPS.DataRequired('SY_REPORT_GROUPS_NBR= 24 and CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
        if DM_COMPANY.CO_ENLIST_GROUPS.RecordCount > 0 then
        begin
         if DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('PROCESS_TYPE').AsString[1] = 'D' then
             continue;
         DM_COMPANY.CO_ENLIST_GROUPS.Edit;
        end
        else
         DM_COMPANY.CO_ENLIST_GROUPS.Insert;
        DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('CO_NBR').AsInteger := wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger;
        DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('MEDIA_TYPE').AsString := sType;
        DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger := TAX_RETURN_GROUP_W2;
        DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('PROCESS_TYPE').AsString := 'A';
        DM_COMPANY.CO_ENLIST_GROUPS.Post;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_ENLIST_GROUPS]);
      end;
    finally
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_SB_UTILS.btnChangeSuiRatesClick(Sender: TObject);
var
  I: Integer;
  SySuiNbr: Integer;
  EffDate: TDateTime;
  Rate: real;
  UseRateAsAdj: boolean;
begin
  with TdglChangeSUIRate.Create(nil) do
  try
    if ShowModal = mrOK then
    begin
      SySuiNbr := StrToInt(evDBLookupCombo1.LookupValue);
      Rate := StrToFloat(evEdit1.Text);
      EffDate := EncodeDate(evSpinEdit1.Value, evComboBox1.ItemIndex * 3 + 1, 1);
      UseRateAsAdj := evRateAsAdCheck.Checked;
    end
    else
      Exit;
  finally
    Free;
  end;
  DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', SySuiNbr, []);
  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    try
      for I := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
        ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

        DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString +
          ' and SY_SUI_NBR=' + IntToStr(SySuiNbr));

        if DM_COMPANY.CO_SUI.RecordCount > 0 then
        begin
          if not UseRateAsAdj then
          begin
            ctx_DBAccess.StartTransaction([dbtClient]);
            try
              ctx_DBAccess.UpdateFieldValue('CO_SUI', 'RATE', DM_COMPANY.CO_SUI.CO_SUI_NBR.AsInteger, EffDate, DayBeforeEndOfTime, Rate);
              ctx_DBAccess.CommitTransaction;
            except
              ctx_DBAccess.RollbackTransaction;
              raise;
            end;
          end
          else
          begin
            if DM_COMPANY.CO_SUI['RATE'] = null then
              EvMessage('Company ' + wwdsClient.DataSet['Custom_Company_Number'] + ' will be skipped. Value of Rate is null.')
            else
            begin
              if (DM_COMPANY.CO_SUI.FieldByName('RATE').AsFloat + Rate) < 0 then
                EvMessage('Company ' + wwdsClient.DataSet['Custom_Company_Number'] +  ' will be skipped. ' +
                  'Adjusted Rate cannot be below zero: ' + FloatToStr(DM_COMPANY.CO_SUI.FieldByName('RATE').AsFloat + Rate))
              else
              begin
                ctx_DBAccess.StartTransaction([dbtClient]);
                try
                  ctx_DBAccess.UpdateFieldValue('CO_SUI', 'RATE', DM_COMPANY.CO_SUI.CO_SUI_NBR.AsInteger, EffDate, DayBeforeEndOfTime, DM_COMPANY.CO_SUI.FieldByName('RATE').AsFloat + Rate);
                  ctx_DBAccess.CommitTransaction;
                except
                  ctx_DBAccess.RollbackTransaction;
                  raise;
                end;
              end;
            end;
          end;
        end
        else
        begin
          DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString +
            ' and SY_STATES_NBR=' + DM_SYSTEM_STATE.SY_SUI.FieldByName('SY_STATES_NBR').AsString);
          if DM_COMPANY.CO_STATES.RecordCount > 0 then
          begin
            if not UseRateAsAdj then
            begin
              DM_COMPANY.CO_SUI.Append;
              DM_COMPANY.CO_SUI['SY_SUI_NBR'] := SySuiNbr;
              DM_COMPANY.CO_SUI['CO_NBR'] := wwdsClient.DataSet['CO_NBR'];
              DM_COMPANY.CO_SUI.FieldByName('RATE').AsFloat := Rate;
              DM_COMPANY.CO_SUI.Post;
              ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SUI]);
            end
            else
              EvMessage('Company ' + wwdsClient.DataSet['Custom_Company_Number'] + ' will be skipped. It has no records to be adjusted.');
          end;
        end;
      end;
    finally
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    DM_COMPANY.CO_SUI.AsOfDate := 0;
    ctx_EndWait;
  end;
end;

procedure TEDIT_SB_UTILS.btnExtendCalendarClick(Sender: TObject);
var
  UtilHelper: TEDIT_SB_UTILS_DETAIL;
  P, I, CoNbr, CoErrorCount, CoWarningCount: Integer;
  Frequency : string;
  WCrossed, BCrossed, SCrossed, MCrossed, QCrossed: Boolean;
  CalendarLog: TStringList;
  CalendarDefaults: TCalendarDefaults;
  PeriodBeginDate, PeriodEndDate, CheckDate : TDate;

const
  M = ' calendar has check dates and batch dates in different years.  If this is not correct, you will need to recreate the calendar.';

begin
  inherited;

  UtilHelper := TEDIT_SB_UTILS_DETAIL.Create(Self);
  UtilHelper.Caption := 'Calendar Extender';
  CalendarLog := TStringList.Create;

  if Calendar.State = dsInactive then
    Calendar.CreateDataSet;

  GetHolidayList(evcsHoliday);
  wwdsClient.DataSet.DisableControls;
  DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
  DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DisableControls;
  try
    for I := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      CoErrorCount := 0;
      CoWarningCount := 0;
      Calendar.EmptyDataSet;
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
      ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
      CoNbr := wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger;
      DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO_CALENDAR_DEFAULTS.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('ALL');

      P := CalendarLog.Add(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' - ' + DM_COMPANY.CO.FieldByName('NAME').AsString);

      Frequency := DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString;

      if Frequency = '' then
      begin
        CalendarLog.Add(#9 + 'Error: Pay Frequency empty! Calendar was not created!');
        Continue;
      end;

      if HasWeekly(Frequency[1]) then
      begin
        if not GetDefaultsFromTable(CoNbr, CO_FREQ_WEEKLY, CalendarDefaults) then
        begin
          CalendarLog.Add(#9 + 'Error: Unable to determine weekly defaults!');
          Inc(CoErrorCount);
        end
        else
        begin
          CheckCalendarDefaults(CalendarDefaults, Frequency[1]);
          CheckDate := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBeginDate, PeriodEndDate, False,False,True);
          UpdateDefaultsByFrequency(CalendarDefaults);
        end;
      end;

      if HasBiWeekly(Frequency[1]) then
      begin
        if not GetDefaultsFromTable(CoNbr, CO_FREQ_BWEEKLY, CalendarDefaults) then
        begin
          CalendarLog.Add(#9 + 'Error: Unable to determine bi-weekly defaults!');
          Inc(CoErrorCount);
        end
        else
        begin
          CheckCalendarDefaults(CalendarDefaults, Frequency[1]);
          CheckDate := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBeginDate, PeriodEndDate,
                         false,false,True);
          UpdateDefaultsByFrequency(CalendarDefaults);
        end;
      end;

      if HasSemiMonthly(Frequency[1]) then
      begin
        if not GetDefaultsFromTable(CoNbr, CO_FREQ_SMONTHLY, CalendarDefaults) then
        begin
          CalendarLog.Add(#9 + 'Error: Unable to determine semi-monthly defaults!');
          Inc(CoErrorCount);
        end
        else
        begin
          CheckCalendarDefaults(CalendarDefaults, Frequency[1]);
          CheckDate := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                         CheckDate,PeriodBeginDate, PeriodEndDate,False,False,True);
          CheckDate := CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE2;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE2;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                         CheckDate,PeriodBeginDate, PeriodEndDate,False,False,False);
          UpdateDefaultsByFrequency(CalendarDefaults);
        end;
      end;

      if HasMonthly(Frequency[1]) then
      begin
        if not GetDefaultsFromTable(CoNbr, CO_FREQ_MONTHLY, CalendarDefaults) then
        begin
          CalendarLog.Add(#9 + 'Error: Unable to determine monthly defaults!');
          Inc(CoErrorCount);
        end
        else
        begin
          CheckCalendarDefaults(CalendarDefaults, Frequency[1]);
          CheckDate := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                         CheckDate,PeriodBeginDate, PeriodEndDate,False,False,True);
          UpdateDefaultsByFrequency(CalendarDefaults);
        end;
      end;

      if HasQuarterly(Frequency[1]) then
      begin
        if not GetDefaultsFromTable(CoNbr, CO_FREQ_QUARTERLY, CalendarDefaults) then
        begin
          CalendarLog.Add(#9 + 'Error: Unable to determine quarterly defaults!');
          Inc(CoErrorCount);
        end
        else
        begin
          CheckCalendarDefaults(CalendarDefaults, Frequency[1]);
          CheckDate := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
          PeriodBeginDate := CalendarDefaults.PERIOD_BEGIN_DATE;
          PeriodEndDate := CalendarDefaults.PERIOD_END_DATE;
          CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                         CheckDate,PeriodBeginDate, PeriodEndDate,False,False,True);
          UpdateDefaultsByFrequency(CalendarDefaults);
        end;
      end;

      AddToCalendar(Calendar, evcsHoliday, WCrossed, BCrossed, SCrossed, MCrossed, QCrossed);

      if WCrossed then
      begin
        CalendarLog.Add(#9 + 'Warning: Weekly' + M);
        Inc(CoWarningCount);
      end;

      if BCrossed then
      begin
        CalendarLog.Add(#9 + 'Warning: Bi-weekly' + M);
        Inc(CoWarningCount);
      end;

      if SCrossed then
      begin
        CalendarLog.Add(#9 + 'Warning: Semi-monthly' + M);
        Inc(CoWarningCount);
      end;

      if MCrossed then
      begin
        CalendarLog.Add(#9 + 'Warning: Monthly' + M);
        Inc(CoWarningCount);
      end;

      if QCrossed then
      begin
        CalendarLog.Add(#9 + 'Warning: Quarterly' + M);
        Inc(CoWarningCount);
      end;

      if (CoErrorCount = 0) then
      begin
        if CoWarningCount = 0 then
          CalendarLog[P] := CalendarLog[P] + ' ---> Calendar Created Successfully!'
        else
          CalendarLog[P] := CalendarLog[P] + ' ---> Calendar Created Successfully with Warnings!'
      end
      else
        CalendarLog[P] := CalendarLog[P] + ' ---> Company Calendar Creation aborted with Errors!!!';

      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_CALENDAR_DEFAULTS, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH]);

    end;

    if EvMessage('Finished Extending Calendars! View log?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then
    begin
      UtilHelper.Caption := 'Calendar Log';
      UtilHelper.Height := 462;
      UtilHelper.Width := 392;
      UtilHelper.TurnTabsOff;
      UtilHelper.PageControl1.ActivePageIndex := 2;
      UtilHelper.evreLog.Lines.AddStrings(CalendarLog);
      UtilHelper.ShowModal;
    end;

  finally
    wwdsClient.DataSet.EnableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.EnableControls;
    UtilHelper.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnUpdateFUTAClick(Sender: TObject);
var
  I: Integer;
  value: string;
  EffDate : Tdate;
begin
  value := '0.051';
  EffDate := SysDate;
  if InputQuery('Enter value', 'FUTA Rate', value) then
  begin
    if not EvDateDialog ( 'Effective Date', 'Please enter Effective Date for this value', EffDate ) then
       exit;
    ctx_StartWait;
    try
      wwdsClient.DataSet.DisableControls;
      try
        for I := 0 to wwgdClient.SelectedList.Count - 1 do
        begin
          wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
          ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
          DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
          if Trim(value) = '' then value := '';

          ctx_DBAccess.StartTransaction([dbtClient]);
          try
            ctx_DBAccess.UpdateFieldValue('CO','FUI_RATE_OVERRIDE', wwdsClient.DataSet.FieldByName('CO_NBR').asInteger, EffDate, DayBeforeEndOfTime, value);
              ctx_DBAccess.CommitTransaction;
          except
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        end;
        ctx_DataAccess.OpenClient(0);
      finally
        wwdsClient.DataSet.EnableControls;
      end;
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TEDIT_SB_UTILS.btnFUICreditClick(Sender: TObject);
var
  I, CheckCount: Integer;
  TaxWages, TmpTaxWages, OldYTDTaxWages, TaxWagesFull: Real;
  Year, reportPath : String;
  sQuery, sQueryEr, sSySuiFilter: String;
  cd : TevClientDataSet;
  Q: IevQuery;

  StateCredRed, CreatedEECheck : TevClientDataSet;
  sySuiList: TStringList;
//   t: IevProcessPayrollTask;  // Jake did not want to process payroll

  procedure CreatePrCheck(const tmpEENbr: integer; const tmpEETax : Currency);
  begin
    if tmpEETax > 0 then
    begin
      if DM_PAYROLL.PR.RecordCount = 0 then
      begin
        DM_PAYROLL.PR.Append;
        if Date < StrToDate('12/31/' + Year) then
          DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value := Date
        else
          DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString := '12/31/' + Year;
        DM_PAYROLL.PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
        DM_PAYROLL.PR.FieldByName('SCHEDULED').Value := 'N';
        DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT;
        DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
        DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value := 'N';
        DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
        DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y';
        DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
        DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := 'Y';
        DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_NONE;
        DM_PAYROLL.PR.FieldByName('TAX_IMPOUND').Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
        DM_PAYROLL.PR.FieldByName('TRUST_IMPOUND').Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
        DM_PAYROLL.PR.FieldByName('BILLING_IMPOUND').Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
        DM_PAYROLL.PR.FieldByName('DD_IMPOUND').Value := DM_COMPANY.CO.DD_IMPOUND.Value;
        DM_PAYROLL.PR.FieldByName('WC_IMPOUND').Value := DM_COMPANY.CO.WC_IMPOUND.Value;
        DM_PAYROLL.PR.Post;
      end;
      if not DM_PAYROLL.PR_BATCH.Locate('FREQUENCY', DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value, []) then
      begin
        DM_PAYROLL.PR_BATCH.Insert;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_IGNORE;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_IGNORE;
        DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
        DM_PAYROLL.PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'N';
        DM_PAYROLL.PR_BATCH.Post;
      end;

      inc(CheckCount);

      DM_PAYROLL.PR_CHECK.Insert;
      DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value := tmpEENbr;
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
      DM_PAYROLL.PR_CHECK.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;
      ctx_PayrollCalculation.AssignCheckDefaults(0, 0);
      ctx_DataAccess.TemplateNumber := 0;
      DM_PAYROLL.PR_CHECK.FieldByName('ER_FUI_TAX').Value := tmpEETax;
      DM_PAYROLL.PR_CHECK.Post;
    end;
  end;

  procedure ClearStateCredRedAmountAndTax;
  begin
    StateCredRed.First;
    while not StateCredRed.EOF do
    begin
     StateCredRed.Edit;
     StateCredRed.FieldByName('Amount').AsCurrency := 0;
     StateCredRed.post;
     StateCredRed.Next;
    end;
  end;

  function GetEETax:Currency;
  begin
    result :=0;
    StateCredRed.First;
    while not StateCredRed.EOF do
    begin
     result := result + StateCredRed.FieldByName('Amount').AsCurrency * StateCredRed.FieldByName('Rate').AsCurrency;
     StateCredRed.Next;
    end;
  end;

begin
   Year := '2015';

   if EvMessage('Do you wish to run the Credit Reduction utility against the selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
       Exit;
   reportPath := '';
   reportPath := RunFolderDialog('Please, select an output path:', reportPath);

   if Trim(reportPath) = '' then
   begin
       EvMessage('You have to enter report path to run this utility.');
       Exit;
   end;

   StateCredRed  := TevClientDataSet.Create(nil);
   CreatedEECheck  := TevClientDataSet.Create(nil);

   try
      CreatedEECheck.FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 40, True);
      CreatedEECheck.FieldDefs.Add('NAME', ftString, 40, True);
      CreatedEECheck.FieldDefs.Add('CheckCount', ftInteger, 0, false);
      CreatedEECheck.CreateDataSet;
      CreatedEECheck.LogChanges := False;

      with StateCredRed.FieldDefs.AddFieldDef do
      begin
        Name := 'State';
        DataType := ftString;
        Size := 2;
        Required := True;
      end;
      with StateCredRed.FieldDefs.AddFieldDef do
      begin
        Name := 'Rate';
        DataType := ftFloat;
      end;
      with StateCredRed.FieldDefs.AddFieldDef do
      begin
        Name := 'Amount';
        DataType := ftFloat;
      end;

      StateCredRed.CreateDataSet;
      StateCredRed.LogChanges := False;
      StateCredRed.IndexFieldNames := 'State';

      StateCredRed.AppendRecord(['AK', 0    , 0]);
      StateCredRed.AppendRecord(['AL', 0    , 0]);
      StateCredRed.AppendRecord(['AR', 0    , 0]);
      StateCredRed.AppendRecord(['AZ', 0    , 0]);
      StateCredRed.AppendRecord(['CA', 0.015, 0]);
      StateCredRed.AppendRecord(['CO', 0    , 0]);
      StateCredRed.AppendRecord(['CT', 0.021, 0]);
      StateCredRed.AppendRecord(['DC', 0    , 0]);
      StateCredRed.AppendRecord(['DE', 0    , 0]);
      StateCredRed.AppendRecord(['FL', 0    , 0]);
      StateCredRed.AppendRecord(['GA', 0    , 0]);
      StateCredRed.AppendRecord(['HI', 0    , 0]);
      StateCredRed.AppendRecord(['IA', 0    , 0]);
      StateCredRed.AppendRecord(['ID', 0    , 0]);
      StateCredRed.AppendRecord(['IL', 0    , 0]);
      StateCredRed.AppendRecord(['IN', 0    , 0]);
      StateCredRed.AppendRecord(['KS', 0    , 0]);
      StateCredRed.AppendRecord(['KY', 0    , 0]);
      StateCredRed.AppendRecord(['LA', 0    , 0]);
      StateCredRed.AppendRecord(['MA', 0    , 0]);
      StateCredRed.AppendRecord(['MD', 0    , 0]);
      StateCredRed.AppendRecord(['ME', 0    , 0]);
      StateCredRed.AppendRecord(['MI', 0    , 0]);
      StateCredRed.AppendRecord(['MN', 0    , 0]);
      StateCredRed.AppendRecord(['MO', 0    , 0]);
      StateCredRed.AppendRecord(['MS', 0    , 0]);
      StateCredRed.AppendRecord(['MT', 0    , 0]);
      StateCredRed.AppendRecord(['NC', 0    , 0]);
      StateCredRed.AppendRecord(['ND', 0    , 0]);
      StateCredRed.AppendRecord(['NE', 0    , 0]);
      StateCredRed.AppendRecord(['NH', 0    , 0]);
      StateCredRed.AppendRecord(['NJ', 0    , 0]);
      StateCredRed.AppendRecord(['NM', 0    , 0]);
      StateCredRed.AppendRecord(['NV', 0    , 0]);
      StateCredRed.AppendRecord(['NY', 0    , 0]);
      StateCredRed.AppendRecord(['OH', 0.015, 0]);
      StateCredRed.AppendRecord(['OK', 0    , 0]);
      StateCredRed.AppendRecord(['OR', 0    , 0]);
      StateCredRed.AppendRecord(['PA', 0    , 0]);
      StateCredRed.AppendRecord(['RI', 0    , 0]);
      StateCredRed.AppendRecord(['SC', 0    , 0]);
      StateCredRed.AppendRecord(['SD', 0    , 0]);
      StateCredRed.AppendRecord(['TN', 0    , 0]);
      StateCredRed.AppendRecord(['TX', 0    , 0]);
      StateCredRed.AppendRecord(['UT', 0    , 0]);
      StateCredRed.AppendRecord(['VA', 0    , 0]);
      StateCredRed.AppendRecord(['VT', 0    , 0]);
      StateCredRed.AppendRecord(['WA', 0    , 0]);
      StateCredRed.AppendRecord(['WI', 0    , 0]);
      StateCredRed.AppendRecord(['WV', 0    , 0]);
      StateCredRed.AppendRecord(['WY', 0    , 0]);
      StateCredRed.AppendRecord(['PR', 0    , 0]);
      StateCredRed.AppendRecord(['VI', 0.015, 0]);


      DM_SYSTEM_STATE.SY_FED_TAX_TABLE.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
      DM_SYSTEM.SY_SUI.DataRequired('EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''R''');

      sySuiList := DM_SYSTEM.SY_SUI.GetFieldValueList('SY_SUI_NBR',True);
      try
        sSySuiFilter := BuildINsqlStatement('t13.SY_SUI_NBR', sySuiList.CommaText)
      finally
         sySuiList.Free;
      end;


      sQueryEr :='SELECT t2.Ee_Nbr,SUM(t2.Er_Fui_Taxable_Wages) Er_Fui_Taxable_Wages FROM Pr t1, Pr_Check t2 ' +
                  'WHERE {AsOfNow<t1>} and {AsOfNow<t2>} and t1.Pr_Nbr = t2.Pr_Nbr AND ' +
                  't1.STATUS in (''P'',''V'') and t1.Check_Date BETWEEN :StartDate AND :EndDate AND t1.Co_Nbr = :CoNbr GROUP BY 1';

      sQuery :=
'SELECT'#13 +
'   t16.Ee_Nbr, t16.Check_Date, t16.State,'#13 +
'   IFDOUBLE(COMPAREINTEGER(COALESCE(t16.Sui_Taxable_Wages,0), ''>'', 0), t16.Amount,  0) Amount, t16.Amount AmountFull '#13 +
'FROM EE ee,'#13 +
'     ('#13 +
'      SELECT t2.Ee_Nbr, t1.Check_Date, t5.State, SUM(t3.Amount) Amount, SUM(Sui_Taxable_Wages) Sui_Taxable_Wages'#13 +
'      FROM Pr t1, Pr_Check t2, Pr_Check_Lines t3, Co_States  t5, Cl_E_Ds t6, Ee_States t4'#13 +
''#13 +
'      left join'#13 +
'       ( SELECT  t12.Ee_Nbr, t12.Ee_States_Nbr, SUM(t10.Sui_Taxable_Wages)  Sui_Taxable_Wages'#13 +
'         FROM Pr t8, Pr_Check t9, Pr_Check_Sui t10, Ee t11, Ee_States t12, Co_Sui t13, Co_States t14'#13 +
'         WHERE'#13 +
'            :d >= t8.EFFECTIVE_DATE and  :d < t8.EFFECTIVE_UNTIL and'#13 +
'            :d >= t9.EFFECTIVE_DATE and  :d < t9.EFFECTIVE_UNTIL and'#13 +
'            :d >= t10.EFFECTIVE_DATE and :d < t10.EFFECTIVE_UNTIL and'#13 +
'            :d >= t11.EFFECTIVE_DATE and :d < t11.EFFECTIVE_UNTIL and'#13 +
'            :d >= t12.EFFECTIVE_DATE and :d < t12.EFFECTIVE_UNTIL and'#13 +
'            :d >= t13.EFFECTIVE_DATE and :d < t13.EFFECTIVE_UNTIL and'#13 +
'            :d >= t14.EFFECTIVE_DATE and :d < t14.EFFECTIVE_UNTIL and'#13 +
''#13 +
sSySuiFilter + 'AND'#13 +
'            t8.Pr_Nbr = t9.Pr_Nbr  AND'#13 +
'            t9.Pr_Check_Nbr = t10.Pr_Check_Nbr  AND'#13 +
'            t11.Ee_Nbr = t9.Ee_Nbr  AND'#13 +
'            t11.Ee_Nbr = t12.Ee_Nbr  AND'#13 +
'            t13.Co_Sui_Nbr = t10.Co_Sui_Nbr  AND'#13 +
'            t14.Co_States_Nbr = t13.Co_States_Nbr  AND'#13 +
'            t12.Sui_Apply_Co_States_Nbr = t14.Co_States_Nbr  AND'#13 +
'            t8.Status = ''P'' AND'#13 +
'            t8.Check_Date BETWEEN :Dat_B AND :Dat_E AND'#13 +
'            t8.Co_Nbr = :Co_Nbr'#13 +
'         GROUP BY 1, 2'#13 +
'       ) t7 on t7.Ee_States_Nbr = t4.Ee_States_Nbr'#13 +
''#13 +
'      WHERE'#13 +
'         :d >= t1.EFFECTIVE_DATE and :d < t1.EFFECTIVE_UNTIL and'#13 +
'         :d >= t2.EFFECTIVE_DATE and :d < t2.EFFECTIVE_UNTIL and'#13 +
'         :d >= t3.EFFECTIVE_DATE and :d < t3.EFFECTIVE_UNTIL and'#13 +
'         :d >= t4.EFFECTIVE_DATE and :d < t4.EFFECTIVE_UNTIL and'#13 +
'         :d >= t5.EFFECTIVE_DATE and :d < t5.EFFECTIVE_UNTIL and'#13 +
'         :d >= t6.EFFECTIVE_DATE and :d < t6.EFFECTIVE_UNTIL and'#13 +
'         t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'         t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'         t4.Ee_States_Nbr = t3.Ee_States_Nbr  AND'#13 +
'         t6.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'         t5.Co_States_Nbr = t4.Sui_Apply_Co_States_Nbr AND'#13 +
'         t1.Status = ''P'' AND t1.Check_Date BETWEEN :Dat_B AND :Dat_E AND t1.Co_Nbr = :Co_Nbr AND NOT (t1.PAYROLL_TYPE = ''V'')  AND'#13 +
'         NOT (t2.Check_Status = ''V'') AND'#13 +
'         NOT (t2.Check_Type = ''V'') AND'#13 +
'         ((t6.E_D_Code_Type IN (''DA'', ''DB'', ''DC'', ''DD'', ''DE'', ''DF'', ''DG'', ''DH'', ''DI'', ''DK'', ''DL'', ''DM'', ''D7'', ''M3'', ''M5'', ''DW'', ''DV'', ''D3'', ''D2'', ''D!'', ''D@'', ''D#'', ''D$'', ''D%'', ''D-'', ''D5'', ''D6'') OR'#13 +
'           t6.E_D_Code_Type LIKE ''E%'')) AND (NOT (t6.Er_Exempt_Exclude_Fui IN (''E'', ''X'')) AND'#13 +
'         NOT (t2.Exclude_Employer_Fui = ''Y''))'#13 +
'      GROUP BY t2.Ee_Nbr, t1.Check_Date, t5.State'#13 +
'     ) t16'#13 +
''#13 +
' where'#13 +
'  :d >= ee.EFFECTIVE_DATE and  :d < ee.EFFECTIVE_UNTIL and'#13 +
'  ee.EE_NBR = t16.EE_NBR'#13 +
' ORDER BY ee.Ee_Nbr, t16.Check_Date, t16.State';

      ctx_StartWait;
      cd := TevClientDataSet.Create(nil);
      try
        wwdsClient.DataSet.DisableControls;
        try
          for I := 0 to wwgdClient.SelectedList.Count - 1 do
          begin
            ctx_UpdateWait('Adjusting company #' + wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
            wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);

            if ctx_DataAccess.ClientId <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
              ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

            CheckCount := 0;

            DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
            DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
            DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
            DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
            DM_PAYROLL.PR.DataRequired('PR_NBR=0');
            DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=0');
            DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=0');

            if DM_COMPANY.CO_STATES.IsEmpty then
               Continue;

            if DM_COMPANY.CO_SUI.IsEmpty then
               Continue;

            Q := TevQuery.Create(sQueryEr);
            Q.Params.AddValue('StartDate', '01/01/' + Year);
            Q.Params.AddValue('EndDate', '12/31/' + Year);
            Q.Params.AddValue('CoNbr', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
            Q.Execute;

            cd.Close;
            cd.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
            with TExecDSWrapper.Create(sQuery) do
            begin
              SetParam('Co_Nbr', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
              SetParam('d', Now);
              SetParam('Dat_B', '01/01/' + Year);
              SetParam('Dat_E', '12/31/' + Year);
              cd.DataRequest(AsVariant);
            end;
            cd.Open;
            cd.IndexFieldNames := 'Ee_Nbr;Check_Date;State';

            Q.Result.First;
            while not Q.Result.EOF do
            begin
                  cd.SetRange([ConvertNull(Q.Result.FieldByName('Ee_Nbr').AsInteger,-1)], [ConvertNull(Q.Result.FieldByName('Ee_Nbr').AsInteger,-1)]);
                  if cd.RecordCount > 0 then
                  begin
                    ClearStateCredRedAmountAndTax;

                    OldYTDTaxWages:= 0;
                    if Q.Result.Locate('EE_NBR', cd.fieldByName('Ee_Nbr').AsInteger, []) and
                         (Q.Result.FieldByName('Er_Fui_Taxable_Wages').AsCurrency > OldYTDTaxWages) then
                           OldYTDTaxWages := Q.Result.FieldByName('Er_Fui_Taxable_Wages').AsCurrency;

                    cd.First;
                    while not cd.EOF do
                    begin
                        ctx_UpdateWait('Adjusting company #' + wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' State # ' + cd.fieldByName('STATE').AsString);
                        if OldYTDTaxWages > 0 then
                        begin

                            TaxWages := cd.FieldByName('Amount').AsFloat;
                            TaxWagesFull := cd.FieldByName('AmountFull').AsFloat;

                            TmpTaxWages := OldYTDTaxWages - TaxWagesFull;
                            if TmpTaxWages > 0 then
                               TmpTaxWages := TaxWages
                            else
                               TmpTaxWages := OldYTDTaxWages;

                            if StateCredRed.Locate('State', cd.fieldByName('State').AsString, []) then
                            begin
                              StateCredRed.Edit;
                              StateCredRed.FieldByName('Amount').AsCurrency := StateCredRed.FieldByName('Amount').AsCurrency + TmpTaxWages;
                              StateCredRed.post;
                              OldYTDTaxWages := OldYTDTaxWages - TaxWagesFull;
                            end;
                        end
                        else
                         break;

                        cd.Next;
                    end;

                    CreatePrCheck(Q.Result.FieldByName('Ee_Nbr').AsInteger, GetEETax);
                  end;
             Q.Result.Next;
            end;

            if CheckCount > 0 then
            begin

              DM_COMPANY.CO.Edit;
              DM_COMPANY.CO.FieldByName('LAST_PREPROCESS_MESSAGE').AsString := 'Manually Reconciled-2015 940 Credit Adjustment';
              DM_COMPANY.CO['LAST_PREPROCESS'] := GetEndQuarter(DM_PAYROLL.PR['CHECK_DATE']);
              DM_COMPANY.CO.Post;

              ctx_DataAccess.PostDataSets([DM_PAYROLL.CO, DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_CHECK]);

              CreatedEECheck.Append;
              CreatedEECheck.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
              CreatedEECheck.FieldByName('NAME').AsString := DM_COMPANY.CO.NAME.AsString;
              CreatedEECheck.FieldByName('CheckCount').AsInteger := CheckCount;
              CreatedEECheck.post;

{  Jake did not want to process payroll
              t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PAYROLL, True) as IevProcessPayrollTask;
              t.Destination := rdtPrinter;
              t.PrList := IntToStr(ClientId) + ';' + DM_PAYROLL.PR.PR_NBR.AsString;
              t.Caption := t.Caption + 'Co:' + Trim(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString)
                + ' Pr:' + DM_PAYROLL.PR.CHECK_DATE.AsString + '-' + DM_PAYROLL.PR.RUN_NUMBER.AsString;
              ctx_EvolutionGUI.AddTaskQueue(t);
              t := nil;
}
            end;

            DM_PAYROLL.PR.Close;
            DM_PAYROLL.PR_BATCH.Close;
            DM_PAYROLL.PR_CHECK.Close;
          end;
        finally
          wwdsClient.DataSet.EnableControls;
        end;
        ctx_DataAccess.OpenClient(0);
      finally
        cd.Free;
        ctx_EndWait;
      end;
    finally
     if CreatedEECheck.RecordCount > 0 then
       CreateFUICreditReductionReport(CreatedEECheck, reportPath);

      StateCredRed.Free;
      CreatedEECheck.Free;
    end;

end;

procedure TEDIT_SB_UTILS.CreateFUICreditReductionReport(const cd:TevClientDataSet; const reportPath:String);
var
  ReportParams: TrwReportParams;
  ms: IisStream;
  ReportResult: TrwReportResults;
  F: TEvFileStream;
begin
 if Assigned(cd) then
 begin
  cd.Filtered := false;
  ReportParams := TrwReportParams.Create;
  try
    ReportParams.Add('DataSets', DataSetsToVarArray([cd]));
    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(2587, 'S', ReportParams);
    finally
      ms := ctx_RWLocalEngine.EndGroup(rdtNone);

      ReportResult := TrwReportResults.Create;
      try
        ReportResult.SetFromStream(ms);
        if (ReportResult.Count > 0) then
        begin
          F := TEvFileStream.Create(NormalizePath(reportPath) + 'FuiCreditReductionReport_' + FormatDateTime('MMDDYYHHMMSS', Now) + '.rwa', fmCreate);
          try
            F.CopyFrom(ReportResult[0].Data.RealStream, ReportResult[0].Data.RealStream.Size);
          finally
            F.Free;
          end;
        end;
      finally
        ReportResult.Free;
      end;
    end;
  finally
    ReportParams.Free;
  end;
 end;
end;

procedure TEDIT_SB_UTILS.btnRelinkMovedClick(Sender: TObject);
var
  i: integer;

begin
  if wwgdClient.SelectedList.Count <> 1 then
  begin
    evmessage('Must select one company');
    exit;
  end;
  i := 0;
  wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
  ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
  DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
  with TFormMoveClient.Create(nil) do
  try
    if ShowModal = mrOK then
  finally
    Free;
  end;

end;

procedure TEDIT_SB_UTILS.bbtnPrintAdjRepsClick(Sender: TObject);
var
  dat_b, dat_e : TDateTime;
  TaxAdjLimit: Double;
  RunThroughQueue: boolean;
  ShouldPrint: boolean;
  RunReports: boolean;
begin
  RunReports := False;
  dat_b := now;
  dat_e := now;
  TaxAdjLimit := 0;
  RunThroughQueue := True;
  ShouldPrint := False;
  with TdlgAdjReportParams.Create(nil) do
  try
    if ShowModal = mrOK then
    begin
      ShouldPrint := PrintResult;
      dat_b := edFrom.DateTime;
      dat_e := edTo.DateTime;
      RunThroughQueue := chbQueue.Checked;
      TaxAdjLimit := StrToFloat(edTaxAdjLimit.Text);
      RunReports := True;
    end;
  finally
    Free;
  end;
  if RunReports then
    PrintAdjustmentReports(dat_b, dat_e, RunThroughQueue, ShouldPrint, TaxAdjLimit);
end;

procedure TEDIT_SB_UTILS.PrintAdjustmentReports(dat_b, dat_e: TDateTime;
  RunThroughQueue, PrintReports: boolean; TaxAdjLimit: Double);
var
  i: integer;
  PrList: Variant;
  RunPar: TevPrRepPrnSettings;
  lParams: TrwReportParams;
  Dest: TrwReportDestination;
  CompanyNBR: integer;
  ReportsReady: boolean;

  function GetTaxLiabAmount(aPrNbr: string; aLiabTable: string): Currency;
  begin
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'Sum(Amount)');
        SetMacro('TABLENAME', aLiabTable);
        SetMacro('CONDITION', 'PR_NBR=' + aPrNbr);
        DataRequest(AsVariant);
      end;
      Open;
      Result := Abs( Fields[0].AsCurrency );
      Close;
    end;
  end;

  function FillPrList: boolean;
  var
    PrL, TaxPr: TStrings;
    i: integer;
  begin
    PrL := TStringList.Create;
    TaxPr := TStringList.Create;
    try
      PrL.Clear;
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then Close;

        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('Columns', 'PR_NBR');
          SetMacro('TABLENAME', 'PR');
          SetMacro('CONDITION', 'CO_NBR=:CoNbr and PAYROLL_TYPE=:PrType and CHECK_DATE between :dat_b and :dat_e');

          SetParam('CoNbr', CompanyNBR);
          SetParam('PrType', PAYROLL_TYPE_TAX_ADJUSTMENT);
          SetParam('dat_b', dat_b);
          SetParam('dat_e', dat_e);

          DataRequest(AsVariant);
        end;
        Open;
        while not Eof do
        begin
          TaxPr.Add( FieldByName('PR_NBR').AsString );
          Next;
        end;
        Close;

        for i := 0 to TaxPr.Count - 1 do
        if ( GetTaxLiabAmount(TaxPr[i], 'Co_Fed_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_Local_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_State_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_Sui_Liabilities') ) >= TaxAdjLimit then
          PrL.Add( TaxPr[i] );

        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('Columns', 'PR_NBR');
          SetMacro('TABLENAME', 'PR');
          SetMacro('CONDITION', 'CO_NBR=:CoNbr and PAYROLL_TYPE=:PrType and CHECK_DATE between :dat_b and :dat_e');

          SetParam('CoNbr', CompanyNBR);
          SetParam('PrType', PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
          SetParam('dat_b', dat_b);
          SetParam('dat_e', dat_e);

          DataRequest(AsVariant);
        end;
        Open;
        while not Eof do
        begin
          // if QEC payrolls are created the reports should print
          PrL.Add(FieldByName('PR_NBR').AsString);
          Next;
        end;
        Close;
      end;

      Result := PrL.Count > 0;

      PrList := VarArrayCreate([0, PrL.Count - 1], varVariant);
      for i := 0 to PrL.Count - 1 do
        PrList[i] := StrToInt(PrL[i]);

    finally
      PrL.Free;
      TaxPr.Free;
    end;
  end;

begin
  ctx_StartWait('Print Adjustment Reports');
  lParams := TrwReportParams.Create;
  RunPar := TevPrRepPrnSettings.Create(nil);
  Dest := rdtCancel;
  ReportsReady := False;
  ctx_RWLocalEngine.StartGroup;
  try
    for i := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[i]);
      ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
      DM_CLIENT.CL.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      CompanyNBR := wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger;

      if FillPrList then
      begin
        ctx_UpdateWait('Prepare reports for Company #' + wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

        with DM_COMPANY.CO_REPORTS do
        begin
          First;

          while not Eof do
          begin

            if DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS').IsNull then
            begin
              RunPar.Clear;
              RunPar.Copies := 0;
            end
            else
              RunPar.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));

            if RunPar.PrnWithAdjPayrolls then
            begin
              lParams.Clear;
              lParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('INPUT_PARAMS')));
              lParams.Add('Clients', varArrayOf([wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger]));
              lParams.Add('Companies', varArrayOf([CompanyNBR]));
              lParams.Add('ClientCompany', varArrayOf([wwdsClient.DataSet.FieldByName('CL_NBR').AsString + ':' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString]));
              lParams.Add('Payrolls', PrList);
              lParams.Add('ReportName', DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString +
                ' (' + DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString +
                DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');
              lParams.Copies := 1;

              ctx_RWLocalEngine.CalcPrintReport(
                DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger,
                DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString,
                lParams, DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString).OverrideVmrMbGroupNbr :=
                DM_COMPANY.CO_REPORTS.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
              ReportsReady := True;
            end;

            Next;
          end;
        end;

      end;
    end;
    if PrintReports then
      Dest := rdtPrinter
    else
      Dest := rdtPreview;
    // if nothing to print it should show an empty result
{    if not ReportsReady then
    begin
      ctx_RWLocalEngine.CalcPrintReport(  );
    end;}
  finally
    try
      if ReportsReady then
      begin
        ctx_UpdateWait('Run Adjustment Reports');
        if RunThroughQueue then
          ctx_RWLocalEngine.EndGroupForTask(Dest, 'Print Adjustment Reports')
        else
          ctx_RWLocalEngine.EndGroup(Dest, 'Print Adjustment Reports');
      end
      else
        EvMessage('There are no adjustment payrolls or no reports set up to print with adjustment payrolls');
    finally
      ctx_EndWait;
      lParams.Free;
      RunPar.Free;
    end;
  end;
end;


procedure TEDIT_SB_UTILS.btnChangeEEActiveStatusClick(Sender: TObject);
var
  EEDBDT: TEDIT_SB_EE_DBDT;
   I : integer;
begin
  inherited;

  for I := 0 to wwgdClient.SelectedList.Count - 1 do
   begin
    wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
   end;

  EEDBDT := TEDIT_SB_EE_DBDT.Create(Self);
  try
    ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
    DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
    EEDBDT.ShowModal;
  finally
    EEDBDT.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnChangeSBBankClick(Sender: TObject);
var
  SBBankAcctNbr, I: Integer;
  bSkipBlank, bTRUST_ACCOUNT,  bTAX_ACCOUNT,  bACH_ACCOUNT,
  bBILLING_ACCOUNT, bWORKERS_COMP_ACCOUNT,  bOBC_ACCOUNT : boolean;

begin
  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;


  with TdlgSelectSBBankAccount.Create(nil) do
  try
    bSkipBlank := false;
    bTRUST_ACCOUNT := false;
    bTAX_ACCOUNT := false;
    bACH_ACCOUNT := false;
    bBILLING_ACCOUNT := false;
    bWORKERS_COMP_ACCOUNT := false;
    bOBC_ACCOUNT := false;

    if ShowModal = mrOK then
    begin
      SBBankAcctNbr := StrToInt(evDBLookupCombo1.LookupValue);
      bSkipBlank := evcbSkipBlank.Checked;
      bTRUST_ACCOUNT := evcbTRUST_ACCOUNT.Checked;
      bTAX_ACCOUNT := evcbTAX_ACCOUNT.Checked;
      bACH_ACCOUNT := evcbACH_ACCOUNT.Checked;
      bBILLING_ACCOUNT := evcbBILLING_ACCOUNT.Checked;
      bWORKERS_COMP_ACCOUNT := evcbWORKERS_COMP_ACCOUNT.Checked;
      bOBC_ACCOUNT := evcbOBC_ACCOUNT.Checked;
    end
    else
      Exit;
  finally
    Free;
  end;

  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    try
      for I := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
        ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
        DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
        if DM_COMPANY.CO.RecordCount > 0 then
        begin
          DM_COMPANY.CO.Edit;

          if bTRUST_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('TRUST_SB_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('TRUST_SB_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          if bTAX_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          if bACH_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          if bBILLING_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          if bWORKERS_COMP_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('W_COMP_SB_BANK_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('W_COMP_SB_BANK_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          if bOBC_ACCOUNT then
           if not (DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').IsNull and bSkipBlank) then
            DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').AsInteger := SBBankAcctNbr;
          DM_COMPANY.CO.Post;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
        end;
      end;
    finally
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
  end;
end;


procedure TEDIT_SB_UTILS.bbtnImportSUIRatesClick(Sender: TObject);
begin
  inherited;
  ShowImportSUIRatesScreen;
end;

procedure TEDIT_SB_UTILS.btnApplySUIClick(Sender: TObject);
var
  f: TEDIT_APPLY_SUI;
  d: TevClientDataSet;
  i, Processed: Integer;
  dBookMark2, dBookMark: TBookmark;
  sCond, sMsg, sErr: string;

  QuarterDate: TDateTime;
  MinTax: Double;
  CleanupAction: TCleanupPayrollAction;
  ReportFilePath, EECustomNbr: string;
  ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean;
  V: Variant;
  Warnings: String;

  MyList: TStringList;
  bStatusDate, eStatusDate : TDate;
  eQuarterDate : TDate;
  pUpdateLiab : boolean;

begin

  inherited;
  evRichEdit1.Clear;
  f := TEDIT_APPLY_SUI.Create(nil);
  MyList := TStringList.Create;
  try
    if f.ShowModal = mrOk then
    begin
      d := wwgdClient.DataSource.DataSet as TevClientDataSet;
      dBookMark2 := d.GetBookmark;
      d.DisableControls;
      ctx_StartWait('', wwgdClient.SelectedList.Count);
      try
        Processed := 0;
        d.First;
        while not d.Eof do
        begin
          dBookMark := d.GetBookmark;
          try
            for i := 0 to wwgdClient.SelectedList.Count - 1 do
              if d.CompareBookmarks(dBookMark, wwgdClient.SelectedList[i]) = 0 then
              begin
                Inc(Processed);
                ctx_UpdateWait('Processing ' + d['NAME'], Processed);
                ctx_DataAccess.OpenClient(d['CL_NBR']);
                DM_COMPANY.CO_SUI_LIABILITIES.Close;

                DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and STATE=' + QuotedStr(f.edState.DisplayValue));
                if DM_COMPANY.CO_STATES.RecordCount > 0 then
                begin
                  sMsg:= wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' - ' +
                         wwdsClient.DataSet.FieldByName('NAME').AsString;
                  sErr := '';

                  bStatusDate := date;
                  DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + IntToStr(d['CO_NBR']) + ' and SY_SUI_NBR = ' + f.edSui.LookupValue);
                  if DM_COMPANY.CO_SUI.RecordCount > 0 then
                  begin
                    sCond := '-1';
                    DM_COMPANY.CO_SUI.First;
                    while not DM_COMPANY.CO_SUI.Eof do
                    begin
                      sCond := sCond + ',' + IntToStr(DM_COMPANY.CO_SUI['CO_SUI_NBR']);

                      AttachSUIToPayrolls(d.fieldByName('CO_NBR').AsInteger,
                                            DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').AsInteger,
                                             DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger,
                                             f.edBeginCheckDate.DateTime,
                                             f.edEndCheckDate.DateTime);

                      DM_COMPANY.CO_SUI.Next;
                    end;

                    MinTax := 0.10;
                    CleanupAction := cpaForceProcess;
                    ReportFilePath := mb_AppSettings['QuaterEndCleanupReportPath'];
                    EECustomNbr := '';
                    ForceCleanup := True;
                    PrintReports := False;
                    CurrentQuarterOnly := false;

                    mb_AppSettings['QuaterEndCleanupReportPath'] := ReportFilePath;
                    V := VarArrayOf([ BoolToYN(True), BoolToYN(True),
                                      BoolToYN(True), BoolToYN(True),
                                      BoolToYN(True), BoolToYN(True),
                                      BoolToYN(True), BoolToYN(True),
                                      BoolToYN(True), BoolToYN(False),
                                      BoolToYN(True), BoolToYN(True)]);

                    pUpdateLiab := false;
                    QuarterDate := GetBeginQuarter(f.edBeginCheckDate.DateTime);
                    eQuarterDate := GetEndQuarter(f.edEndCheckDate.DateTime);
                    Repeat
                        QuarterDate := GetEndQuarter(QuarterDate);
                        if not ctx_PayrollProcessing.DoQuarterEndCleanup(
                                           d.fieldByName('CO_NBR').AsInteger,
                                           QuarterDate,
                                           MinTax,
                                           Ord(CleanupAction),
                                           ReportFilePath,
                                           EECustomNbr,
                                           ForceCleanup,
                                           PrintReports,
                                           CurrentQuarterOnly,
                                           rlTryOnce,
                                           V,
                                           Warnings) then
                           sErr := '- Quarter End Cleanup payroll was not processed due to pending EE tax adjustments.'
                        else
                        begin
                          pUpdateLiab := true;
                        end;
                        QuarterDate := QuarterDate + 1;
                    until QuarterDate > eQuarterDate;

                    if pUpdateLiab then
                    begin
                        eStatusDate := now + 1;
                        DM_COMPANY.PR.DataRequired('PAYROLL_TYPE = ''A'' and CHECK_DATE>=' + QuotedStr(DateToStr(f.edBeginCheckDate.DateTime)) +
                                                                                               ' and CHECK_DATE<=' + QuotedStr(DateToStr(GetEndQuarter(f.edEndCheckDate.DateTime))) +
                                                                                               ' and STATUS_DATE>=' + QuotedStr(DateToStr(bStatusDate)) +
                                                                                               ' and STATUS_DATE<=' + QuotedStr(DateToStr(eStatusDate))  );
                        if DM_COMPANY.PR.RecordCount > 0 then
                        begin
                          sCond := '-1';
                          DM_COMPANY.PR.First;
                          while not DM_COMPANY.PR.Eof do
                          begin
                            sCond := sCond + ',' + IntToStr(DM_COMPANY.PR['PR_NBR']);
                            DM_COMPANY.PR.Next;
                          end;
                          DM_COMPANY.CO_SUI_LIABILITIES.DataRequired('PR_NBR in (' + sCond + ')  and CHECK_DATE>=' + QuotedStr(DateToStr(f.edBeginCheckDate.DateTime)) +
                                                                                            ' and CHECK_DATE<=' + QuotedStr(DateToStr(GetEndQuarter(f.edEndCheckDate.DateTime))) +
                                                                                            ' and STATUS_DATE>=' + QuotedStr(DateToStr(bStatusDate)) +
                                                                                            ' and STATUS_DATE<=' + QuotedStr(DateToStr(eStatusDate))  );
                        end
                        else
                        begin
                          MyList.Add(sMsg + '- Quarter End Cleanup payroll was not processed due to pending EE tax adjustments.');
                          MyList.Add('');
                        end;
                    end
                  end
                  else
                    sErr := '- missing company SUI tax information for ' + f.edSui.DisplayValue + '.';
                end
                else
                  sErr := '- missing company state tax information for ' + f.edState.DisplayValue + '.';

                if sErr <> '' then
                begin
                  MyList.Add(sMsg + sErr);
                  MyList.Add('');
                end;

                if DM_COMPANY.CO_SUI_LIABILITIES.Active and (DM_COMPANY.CO_SUI_LIABILITIES.RecordCount > 0) then
                begin
                  DM_COMPANY.CO_SUI_LIABILITIES.First;
                  while not DM_COMPANY.CO_SUI_LIABILITIES.Eof do
                  begin
                    DM_COMPANY.CO_SUI_LIABILITIES.Edit;
                    DM_COMPANY.CO_SUI_LIABILITIES.FieldValues['DUE_DATE'] := f.edDueDate.DateTime;
                    DM_COMPANY.CO_SUI_LIABILITIES.FieldValues['PERIOD_BEGIN_DATE'] := GetBeginQuarter(f.edBeginCheckDate.DateTime);
                    DM_COMPANY.CO_SUI_LIABILITIES.FieldValues['PERIOD_END_DATE'] := GetEndQuarter(f.edEndCheckDate.DateTime);
                    DM_COMPANY.CO_SUI_LIABILITIES.FieldValues['ACH_KEY'] := FormatDateTime('mm/dd/yyyy', GetEndQuarter(f.edEndCheckDate.DateTime)) +
                                               IntToStr(GetQuarterNumber(f.edEndCheckDate.DateTime));
                    DM_COMPANY.CO_SUI_LIABILITIES.Post;
                    DM_COMPANY.CO_SUI_LIABILITIES.Next;
                  end;

                  ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SUI_LIABILITIES]);
                end;
              end;
          finally
            d.FreeBookmark(dBookMark);
          end;
          d.Next;
        end;
      finally
        ctx_EndWait;
        d.GotoBookmark(dBookMark2);
        d.FreeBookmark(dBookMark2);
        d.EnableControls;

        if MyList.Count > 0 then
          evRichEdit1.Lines := MyList;
      end;
    end;
  finally
    FreeAndNil(f);
    MyList.Free;
  end;
end;

procedure TEDIT_SB_UTILS.btnTaxableWagesAdjustmentClick(Sender: TObject);
var
  TaxableWagesfrm :TEDIT_APPLY_TAXABLEWAGES;
begin
  inherited;
  if ((wwgdClient.SelectedList.Count > 1) or
     (wwgdClient.SelectedList.Count = 0)) then
  begin
    EvMessage('Please select only one company!');
    exit;
  end;

  if (wwgdClient.SelectedList.Count = 1) then
  begin
    wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[0]);
    TaxableWagesfrm := TEDIT_APPLY_TAXABLEWAGES.Create(Self);
    try
      ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
      DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      TaxableWagesfrm.ShowModal;
    finally
      TaxableWagesfrm.Free;
    end;
  end;
end;

procedure TEDIT_SB_UTILS.btnCreditHoldClick(Sender: TObject);
var
  i: Integer;
  UtilHelper: TEDIT_SB_UTILS_DETAIL;
  CheckDate: TDateTime;
  F, LibStatus, CreditHold: string;
begin
  inherited;

  UtilHelper := TEDIT_SB_UTILS_DETAIL.Create(Self);
  UtilHelper.Caption := 'Set NSF and Credit Hold';
  UtilHelper.TurnTabsOff;
  UtilHelper.PageControl1.ActivePageIndex := 3;
  UtilHelper.evcbCreditHold.Items.Text := Credit_Hold_Level_ComboChoices;
  UtilHelper.evcbCreditHold.MapList := True;
  UtilHelper.evcbCreditHold.Value := CREDIT_HOLD_LEVEL_LOW;

  UtilHelper.evcbLiabStatus.Items.Text := TaxDepositStatus_ComboChoices;
  UtilHelper.evcbLiabStatus.MapList := True;
  UtilHelper.evcbLiabStatus.Value := TAX_DEPOSIT_STATUS_NSF;

  if UtilHelper.ShowModal = mrOk then
  begin
    CheckDate := UtilHelper.evdtCheckDate.DateTime;
    CreditHold := UtilHelper.evcbCreditHold.Value;
    LibStatus := UtilHelper.evcbLiabStatus.Value;
  end
  else
    Exit;

  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    try
      for i := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
        ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

        F := 'CO_NBR=' + QuotedStr(wwdsClient.DataSet.FieldByName('CO_NBR').AsString) + ' and CHECK_DATE=' + QuotedStr(DateToStr(CheckDate));

        //Work the magic....
        DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
        DM_COMPANY.CO.Edit;
        DM_COMPANY.CO['CREDIT_HOLD'] := CreditHold;
        DM_COMPANY.CO.Post;

        DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired(F);
        DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
        while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
        begin
          if ((DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) or
            (DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_VOIDED) or
            (DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED) or
            (DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) or
            (DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_NSF)) then
          begin
            DM_COMPANY.CO_FED_TAX_LIABILITIES.Edit;
            DM_COMPANY.CO_FED_TAX_LIABILITIES['STATUS'] := LibStatus;
            DM_COMPANY.CO_FED_TAX_LIABILITIES.Post;
          end;
          DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
        end;

        DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired(F);
        DM_COMPANY.CO_STATE_TAX_LIABILITIES.First;
        while not DM_COMPANY.CO_STATE_TAX_LIABILITIES.Eof do
        begin
          if ((DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) or
            (DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_VOIDED) or
            (DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED) or
            (DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) or
            (DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_NSF)) then
          begin
            DM_COMPANY.CO_STATE_TAX_LIABILITIES.Edit;
            DM_COMPANY.CO_STATE_TAX_LIABILITIES['STATUS'] := LibStatus;
            DM_COMPANY.CO_STATE_TAX_LIABILITIES.Post;
          end;
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Next;
        end;

        DM_COMPANY.CO_SUI_LIABILITIES.DataRequired(F);
        DM_COMPANY.CO_SUI_LIABILITIES.First;
        while not DM_COMPANY.CO_SUI_LIABILITIES.Eof do
        begin
          if ((DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) or
            (DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_VOIDED) or
            (DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED) or
            (DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) or
            (DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_NSF)) then
          begin
            DM_COMPANY.CO_SUI_LIABILITIES.Edit;
            DM_COMPANY.CO_SUI_LIABILITIES['STATUS'] := LibStatus;
            DM_COMPANY.CO_SUI_LIABILITIES.Post;
          end;
          DM_COMPANY.CO_SUI_LIABILITIES.Next;
        end;

        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired(F);
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.First;
        while not DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Eof do
        begin
          if ((DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) or
            (DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_VOIDED) or
            (DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED) or
            (DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) or
            (DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_NSF)) then
          begin
            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Edit;
            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES['STATUS'] := LibStatus;
            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Post;
          end;
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Next;
        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO, DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES, DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
      end;
      ctx_DataAccess.OpenClient(0);
    finally
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
    FreeAndNil(UtilHelper);
  end;

end;

procedure TEDIT_SB_UTILS.btnHSACleanupClick(Sender: TObject);

Type
 BenefitAmoutType = (typeEE, typeER, typeNONE);

  Procedure ProcessHSACleanup(const clEDsNbr, priorityNumber:integer; const AAmountType: BenefitAmoutType = typeNONE );
  var
    TempAmount, AnnualMaximum: Real;

  begin
      DM_COMPANY.EE_SCHEDULED_E_DS.SetRange([clEDsNbr], [clEDsNbr]);
      try
        while not DM_COMPANY.EE_SCHEDULED_E_DS.Eof do
        begin
          if DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').IsNull or
             ((SysTime >= DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
              (SysTime <= DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').AsDateTime))  or
             (DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= SysTime) then
          begin
            DM_COMPANY.EE_SCHEDULED_E_DS.Edit;

            if (AAmountType <> typeNONE) and
               DM_COMPANY.EE.Locate('EE_NBR',DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger,[]) and
               (not DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('ANNUAL_MAXIMUM_AMOUNT').IsNull) then
            begin
                TempAmount := 0;
                AnnualMaximum := DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('ANNUAL_MAXIMUM_AMOUNT').AsFloat;
                case DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('FREQUENCY').AsString[1] of
                  SCHED_FREQ_RUN_ONCE, SCHED_FREQ_ONE_TIME: TempAmount := DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('TARGET_AMOUNT').AsFloat;
                  SCHED_FREQ_DAILY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/260);
                        if AnnualMaximum > (TempAmount*260) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_SCHEDULED_PAY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/GetTaxFreq(DM_COMPANY.EE.FieldByName('PAY_FREQUENCY').AsString));
                        if AnnualMaximum > (TempAmount*GetTaxFreq(DM_COMPANY.EE.FieldByName('PAY_FREQUENCY').AsString)) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_FIRST_SCHED_PAY, // : TempAmount := DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('TARGET_AMOUNT').AsFloat;
                  SCHED_FREQ_LAST_SCHED_PAY,
                  SCHED_FREQ_MID_SCHED_PAY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/12);
                        if AnnualMaximum > (TempAmount*12) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_ANNUALLY: TempAmount := AnnualMaximum;
                  SCHED_FREQ_SEMI_ANNUALLY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/2);
                        if AnnualMaximum > (TempAmount*2) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_QUARTERLY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/4);
                        if AnnualMaximum > (TempAmount*4) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_SCHEDULED_MONTHLY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/12);
                        if AnnualMaximum > (TempAmount*12) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_EVRY_OTHER_PAY:
                      begin
                        TempAmount := RoundTwo(AnnualMaximum/(GetTaxFreq(DM_COMPANY.EE.FieldByName('PAY_FREQUENCY').AsString) div 2));
                        if AnnualMaximum > (TempAmount*(GetTaxFreq(DM_COMPANY.EE.FieldByName('PAY_FREQUENCY').AsString) div 2)) then
                          TempAmount := TempAmount + 0.01;
                      end;
                  SCHED_FREQ_USER_ENTERED: TempAmount := 0;
                end;

                if TempAmount <> 0 then
                begin
                  DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('AMOUNT').Value := TempAmount;
                  DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').Value := null;
                end;
            end;


            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('SCHEDULED_E_D_GROUPS_NBR').AsInteger :=
                                     DM_COMPANY.CL_E_D_GROUPS.fieldByName('CL_E_D_GROUPS_NBR').AsInteger;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('PRIORITY_NUMBER').AsInteger:= priorityNumber;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('TARGET_ACTION').AsString := SCHED_ED_TARGET_NONE;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value := null;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('TARGET_AMOUNT').Value:=null;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('ANNUAL_MAXIMUM_AMOUNT').Value:=null;
            DM_COMPANY.EE_SCHEDULED_E_DS.FieldByName('NUMBER_OF_TARGETS_REMAINING').Value:=null;
            DM_COMPANY.EE_SCHEDULED_E_DS.Post;
          end;

          DM_COMPANY.EE_SCHEDULED_E_DS.Next;
        end;

      finally
        DM_COMPANY.EE_SCHEDULED_E_DS.CancelRange;
      end;
  end;

var
  f: TEDIT_HSACleanup;
  I, tmpClNbr: integer;
  pList: TStringList;
  sIndexFieldNames, sFilter:String;
begin
  inherited;

  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;

  wwdsClient.DataSet.DisableControls;
  try
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[0]);
      tmpClNbr:=wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger;
      for I := 1 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
        if tmpClNbr <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
        begin
          EvMessage('You can select only one client');
          Exit;
        end;
      end;

      ctx_DataAccess.OpenClient(tmpClNbr);
      DM_COMPANY.CL_E_DS.DataRequired('ALL');
      DM_COMPANY.CL_E_D_GROUPS.DataRequired('ALL');

      f := TEDIT_HSACleanup.Create(nil);
      try
        if f.ShowModal = mrOk then
        begin
          ctx_StartWait;
          try
              for I := 0 to wwgdClient.SelectedList.Count - 1 do
              begin
                wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);

                ctx_DBAccess.StartTransaction([dbtClient]);
                try
                  DM_COMPANY.EE.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);

                  pList := DM_COMPANY.EE.GetFieldValueList('EE_NBR',True);
                  try
                    sFilter := BuildINsqlStatement('EE_NBR', pList.CommaText);
                    DM_COMPANY.EE_SCHEDULED_E_DS.DataRequired(sFilter);
                  finally
                    pList.Free;
                  end;

                  if (DM_COMPANY.EE_SCHEDULED_E_DS.RecordCount > 0) and
                      (f.edtMultEDCode.LookupValue <> '') and
                      DM_COMPANY.CL_E_D_GROUPS.Locate('CL_E_D_GROUPS_NBR',f.edtMultEDCode.LookupValue ,[]) then
                  begin
                    sIndexFieldNames := DM_COMPANY.EE_SCHEDULED_E_DS.IndexFieldNames;
                    try
                      DM_COMPANY.EE_SCHEDULED_E_DS.IndexFieldNames := 'CL_E_DS_NBR';

                      if (f.edtEEContEDCode.LookupValue <> '') and
                         DM_COMPANY.CL_E_DS.Locate('CL_E_DS_NBR', f.edtEEContEDCode.LookupValue,[]) then
                        ProcessHSACleanup(DM_COMPANY.CL_E_DS.fieldByName('CL_E_DS_NBR').AsInteger, 1, typeEE);

                      if (f.edtEECathEDCode.LookupValue <> '') and
                         DM_COMPANY.CL_E_DS.Locate('CL_E_DS_NBR', f.edtEECathEDCode.LookupValue,[]) then
                        ProcessHSACleanup(DM_COMPANY.CL_E_DS.fieldByName('CL_E_DS_NBR').AsInteger, 2,typeNONE);

                      if (f.edtERContEDCode.LookupValue <> '') and
                        DM_COMPANY.CL_E_DS.Locate('CL_E_DS_NBR', f.edtERContEDCode.LookupValue,[]) then
                        ProcessHSACleanup(DM_COMPANY.CL_E_DS.fieldByName('CL_E_DS_NBR').AsInteger, 3,typeER);

                      if (f.edtOptERContEDCode.LookupValue <> '') and
                         DM_COMPANY.CL_E_DS.Locate('CL_E_DS_NBR', f.edtOptERContEDCode.LookupValue,[]) then
                        ProcessHSACleanup(DM_COMPANY.CL_E_DS.fieldByName('CL_E_DS_NBR').AsInteger, 4, typeNONE);

                    finally
                      DM_COMPANY.EE_SCHEDULED_E_DS.IndexFieldNames := sIndexFieldNames;
                    end;
                  end;

                  ctx_DataAccess.PostDataSets([DM_COMPANY.EE_SCHEDULED_E_DS, DM_COMPANY.EE_BENEFITS]);
                  ctx_DBAccess.CommitTransaction;
                except
                  ctx_DBAccess.RollbackTransaction;
                  raise;
                end;
              end;

          finally
            ctx_EndWait;
          end;
        end;
      finally
        FreeAndNil(f);
      end;
    finally
      ctx_DataAccess.OpenClient(0);
      wwdsClient.DataSet.EnableControls;
    end;
end;

procedure TEDIT_SB_UTILS.btnIgnoreFICAClick(Sender: TObject);
var
 I : integer;
 s, clNBR, coNBR: String;
 pList: TStringList;
 ListOfEE: IisStringList;

   procedure FicaUpdate(const aCond:string; const effDate:TDate);
   begin
      DM_EMPLOYEE.EE.First;
      while not DM_EMPLOYEE.EE.Eof do
      begin
        ctx_DBAccess.UpdateFieldValue('EE','MAKEUP_FICA_ON_CLEANUP_PR', DM_EMPLOYEE.EE.FieldByName('EE_NBR').asInteger, EffDate, DayBeforeEndOfTime, aCond);
        DM_EMPLOYEE.EE.Next;
      end;
   end;

begin
  inherited;

  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;

  with TdlgUpdateEEFICAFlag.Create(nil) do
  try
    if ShowModal = mrOK then
    begin
        if rgUpdateFICA.ItemIndex = 0 then
        begin
          ListOfEE := TisStringList.Create;
          ctx_StartWait;
          try
            wwdsClient.DataSet.DisableControls;
            try
              for I := 0 to wwgdClient.SelectedList.Count - 1 do
              begin
                wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);
                ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
                DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString + ' and MAKEUP_FICA_ON_CLEANUP_PR = ''' + GROUP_BOX_YES + '''');
                if DM_EMPLOYEE.EE.RecordCount > 0 then
                begin
                  pList := DM_COMPANY.EE.GetFieldValueList('EE_NBR',True);
                  try
                    s := wwdsClient.DataSet.FieldByName('CL_NBR').AsString + ','  +
                         wwdsClient.DataSet.FieldByName('CO_NBR').AsString + ','  + BuildINsqlStatement('EE_NBR', pList.CommaText);

                    FicaUpdate(GROUP_BOX_NO, edtEffective.Date);
                    ListOfEE.Add(s);
                  ctx_DataAccess.PostDataSets([DM_COMPANY.EE]);
                  finally
                    pList.Free;
                  end;
                end;
              end;
            finally
              if ListOfEE.Count > 0 then
              begin
               if Trim(edtFileName.Text) <> '' then
                   ListOfEE.SaveToFile(edtFileName.Text)
               else
                  with TSaveDialog.Create(nil) do
                  try
                     if Execute then
                       ListOfEE.SaveToFile(FileName);
                  finally
                    Free;
                  end;
              end;
              wwdsClient.DataSet.EnableControls;
            end;
          finally
            ctx_EndWait;
          end;
        end
        else
        begin
          if FileExists(edtFileName.Text) then
          begin
            ctx_StartWait;
            try
              try
                wwdsClient.DataSet.DisableControls;
                ListOfEE := TisStringList.Create;
                ListOfEE.LoadFromFile(edtFileName.Text);
                for I := 0 to ListOfEE.Count - 1 do
                begin
                  s:=ListOfEE[I];
                  clNBR := Fetch(s,',');
                  coNBR := Fetch(s,',');

                  ctx_DataAccess.OpenClient(StrToInt(clNBR));
                  DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + coNBR + 'and ' + s +  ' and MAKEUP_FICA_ON_CLEANUP_PR = ''' + GROUP_BOX_NO + '''');
                  if DM_EMPLOYEE.EE.RecordCount > 0 then
                  begin
                    FicaUpdate(GROUP_BOX_YES, edtEffective.Date);
                    ctx_DataAccess.PostDataSets([DM_COMPANY.EE]);
                 end;
                end;
              finally
                  wwdsClient.DataSet.EnableControls;
              end;
            finally
              ctx_EndWait;
            end;
          end
          else
            EvMessage('File does not exist');
        end;
    end
    else
      Exit;
  finally
    Free;
  end;

end;

procedure TEDIT_SB_UTILS.btnCleanUpLocalsClick(Sender: TObject);
var
  i, tmpClient: Integer;
  tmpIndexName: string;
  MyList: IisStringList;
begin
  inherited;

  if EvMessage('Are you sure you want to Clean up locals for all selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
    tmpIndexName:=DM_EMPLOYEE.EE_LOCALS.IndexFieldNames;
    DM_EMPLOYEE.EE_LOCALS.IndexFieldNames:= 'CO_LOCAL_TAX_NBR';
    tmpClient:= -1;
    MyList := TisStringList.Create;
    try
      MyList.Add('CL_NBR - CO_NBR - EE_NBR - Local - Pre County - New County');
      MyList.Add('------   ------   ------   -----   ----------   ----------');
      for i := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[i]);
        if tmpClient <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
        begin
           ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
           tmpClient:= wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger;
           DM_EMPLOYEE.EE_LOCALS.DataRequired('ALL');
        end;

        DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
        DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);

        if (DM_COMPANY.CO_LOCAL_TAX.RecordCount > 0) then
        begin
          DM_COMPANY.CO_LOCAL_TAX.First;
          while not DM_COMPANY.CO_LOCAL_TAX.Eof do
          begin
            if DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR',DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], []) and
               not DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('CountyName').IsNull then
            begin
              DM_EMPLOYEE.EE_LOCALS.SetRange([DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']], [DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']]);

              DM_EMPLOYEE.EE_LOCALS.First;
              while not DM_EMPLOYEE.EE_LOCALS.Eof do
              begin
                  if DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_COUNTY').IsNull or
                     (DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_COUNTY').AsString <>
                       DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('CountyName').AsString) then
                  begin
                    MyList.Add(wwdsClient.DataSet.FieldByName('CL_NBR').AsString + ' - ' +
                                wwdsClient.DataSet.FieldByName('CO_NBR').AsString + ' - ' +
                                 DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_NBR').asString + ' - ' +
                                  DM_COMPANY.CO_LOCAL_TAX.FieldByName('LocalName').asString + ' - ' +
                                   DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_COUNTY').asString + ' - ' +
                                    DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('CountyName').AsString);

                    DM_EMPLOYEE.EE_LOCALS.Edit;
                    DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_COUNTY').asString := DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('CountyName').AsString;
                    DM_EMPLOYEE.EE_LOCALS.Post;
                  end;

                DM_EMPLOYEE.EE_LOCALS.Next;
              end;
            end;

            DM_COMPANY.CO_LOCAL_TAX.Next;
          end;
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_LOCALS]);
        end;
      end;
    finally
      if MyList.Count > 2 then
         evRichEdit1.Lines := MyList.VCLStrings;
      DM_EMPLOYEE.EE_LOCALS.IndexFieldNames:= tmpIndexName;
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
  end;

end;

procedure TEDIT_SB_UTILS.btnMCTUpdateClick(Sender: TObject);
  function CreateReportFileName(ReportName, Path: string): string;
  begin
    Result := Path + StringReplace(StringReplace(StringReplace(
      ReportName, '/', '-', [rfReplaceAll]),
      ':', '-', [rfReplaceAll]), '\', '-', [rfReplaceAll]); // !!!
  end;
var
  I: Integer;
  reportPath : String;
  sQuery: String;
  Q: IevQuery;
  bDate, eDate: TDateTime;
  MyList: IisStringList;
  Rate: Double;

begin
   if EvMessage('Do you wish to run the MCT Update utility against the selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
       Exit;

   reportPath := '';
   reportPath := RunFolderDialog('Please, select an output path:', reportPath);

   if Trim(reportPath) = '' then
   begin
       EvMessage('You have to enter report path to run this utility.');
       Exit;
   end;

      reportPath:= NormalizePath(ExpandFileName(reportPath));

      sQuery :=
'SELECT t12.Co_Nbr, t11.CO_LOCAL_TAX_NBR, c.CUSTOM_COMPANY_NUMBER, c.NAME, t12.Taxable_Wages,  t11.Tax_Rate,'#13 +
'      CAST(COALESCE( IFDOUBLE(COMPAREDOUBLE(t12.Taxable_Wages, ''>'', 437500), 0.0034,'#13 +
'                     IFDOUBLE(COMPAREDOUBLE(t12.Taxable_Wages, ''>'', 375000), 0.0023,'#13 +
'                      IFDOUBLE(COMPAREDOUBLE(t12.Taxable_Wages, ''>'', 312500), 0.0011, 0))), 0) AS FLOAT)  Correct_Rate'#13 +
'    FROM'#13 +
'       CO c,'#13 +
'       Co_Local_Tax  t11,'#13 +
'       (SELECT SUM(t21.Local_Taxable_Wage) Taxable_Wages, t20.Co_Nbr'#13 +
'        FROM  Ee_Locals  t18, Co_Local_Tax t19, Pr t20, Pr_Check_Locals t21'#13 +
'        WHERE'#13 +
'          CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'          CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'          CURRENT_DATE >= t20.EFFECTIVE_DATE and CURRENT_DATE < t20.EFFECTIVE_UNTIL and'#13 +
'          CURRENT_DATE >= t21.EFFECTIVE_DATE and CURRENT_DATE < t21.EFFECTIVE_UNTIL and'#13 +
''#13 +
'          t19.Co_Local_Tax_Nbr = t18.Co_Local_Tax_Nbr  AND'#13 +
'          t21.Pr_Nbr = t20.Pr_Nbr  AND'#13 +
'          t21.Ee_Locals_Nbr = t18.Ee_Locals_Nbr AND'#13 +
'         t19.Sy_Locals_Nbr = 5724  AND'#13 +
'         t20.Co_Nbr = :CoNbr AND t20.Check_Date BETWEEN :dat_b AND :dat_e'#13 +
'        GROUP BY 2'#13 +
'       )  t12'#13 +
'    WHERE'#13 +
'      :d >= c.EFFECTIVE_DATE and :d < c.EFFECTIVE_UNTIL and'#13 +
'      :dat_b >= t11.EFFECTIVE_DATE and :dat_b < t11.EFFECTIVE_UNTIL and'#13 +
'      c.CO_NBR = :CoNbr AND'#13 +
'      c.Co_Nbr = t11.Co_Nbr  AND'#13 +
'      t12.Co_Nbr = t11.Co_Nbr  AND'#13 +
'      t11.Sy_Locals_Nbr = 5724';

   DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('Sy_Locals_Nbr = 5724');
   CheckCondition(DM_SYSTEM_LOCAL.SY_LOCALS.RecordCount > 0, 'System level MCT Mobility Tax rate is not exist.!');

   with TMCTTaxRAteUpdate.Create(nil) do
   try
      if ShowModal = mrOK then
      begin
          ctx_StartWait;
          try
            wwdsClient.DataSet.DisableControls;
            MyList := TisStringList.Create;
            try
              MyList.Add('Quarter     ' + IntToStr(cbQuarter.ItemIndex+ 1) + '  ' + edYear.Text );
              MyList.Add('CUSTOM_COMPANY_NUMBER  NAME                                         TAXABLE WAGES   PRIOR RATE UPDATED RATE');
              MyList.Add('--------------------- ---------------------------------------- ------------------ ------------ ------------');

              bDate := EncodeDate(StrToInt(edYear.Text), cbQuarter.ItemIndex*3+ 1, 1);
              eDate := GetEndQuarter(bDate);

              for I := 0 to wwgdClient.SelectedList.Count - 1 do
              begin
                ctx_UpdateWait('Adjusting company #' + wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
                wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);

                if ctx_DataAccess.ClientId <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
                  ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

                Q := TevQuery.Create(sQuery);
                Q.Params.AddValue('d', Now);
                Q.Params.AddValue('dat_b', bDate);
                Q.Params.AddValue('dat_e', eDate);
                Q.Params.AddValue('CoNbr', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
                Q.Execute;

                Q.Result.First;
                while not Q.Result.EOF do
                begin
                  if (not Q.Result.FieldByName('Correct_Rate').IsNull) and
                     ((RoundAll(Q.Result.FieldByName('Tax_Rate').AsFloat, 4) <> RoundAll(Q.Result.FieldByName('Correct_Rate').AsFloat, 4) ) or
                      (Q.Result.FieldByName('Tax_Rate').IsNull and
                       (RoundAll(DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('TAX_RATE').AsFloat, 4) <> RoundAll(Q.Result.FieldByName('Correct_Rate').AsFloat, 4)))) then
                  begin
                    Rate:=DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('TAX_RATE').AsFloat;
                    if not Q.Result.FieldByName('Tax_Rate').IsNull then
                       Rate:=Q.Result.FieldByName('Tax_Rate').AsFloat;

                    MyList.Add( Format('%-20s', [Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString])  + '  ' +
                                Format('%-40s', [Q.Result.FieldByName('NAME').AsString]) + ' ' +
                                Format('%18.2f', [Q.Result.FieldByName('Taxable_Wages').AsFloat]) + ' ' +
                                Format('%12.4f', [Rate]) + ' ' +
                                Format('%12.4f', [Q.Result.FieldByName('Correct_Rate').AsFloat]) );

                    ctx_DBAccess.StartTransaction([dbtClient]);
                    try
                      ctx_DBAccess.UpdateFieldValue('CO_LOCAL_TAX', 'TAX_RATE', Q.Result['CO_LOCAL_TAX_NBR'], bDate, DayBeforeEndOfTime, Q.Result.FieldByName('Correct_Rate').Value);
                      ctx_DBAccess.CommitTransaction;
                    except
                      ctx_DBAccess.RollbackTransaction;
                      raise;
                    end;
                  end;
                  Q.Result.Next;
                end;
              end;
            finally
              if MyList.Count > 3 then
              begin
                 reportPath:= ChangeFileExt(CreateReportFileName('MCT_Update _' + FormatDateTime('yyyy-mm-dd"T"hh:mm:ss', Now), reportPath), '.txt');
                 MyList.SaveToFile(reportPath);
              end;
              wwdsClient.DataSet.EnableControls;
            end;
            ctx_DataAccess.OpenClient(0);
          finally
            ctx_EndWait;
          end;
      end
      else
        Exit;
   finally
      Free;
   end;
end;

procedure TEDIT_SB_UTILS.btnRecipBugFixClick(Sender: TObject);
var
  i, j, k, n, tmpClient: Integer;
  MyList: IisStringList;
  Evo: IevHistData;
  Person: IevPerson;
  Check: IevCheck;
  Q, Q1: IevQuery;
  Tax, Diff: Double;
  State, S: String;
  EeStatesNbr, CoStatesNbr: Integer;
  AllOk: Boolean;
  Liabilities: IisListOfValues;
  TaxDates: IisListOfValues;
  CalcDueDates: TevCalcDueDates;
begin
  inherited;

  if EvMessage('Are you sure you want to run the reciprocation bug fix utility for all selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;



  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    DM_SYSTEM.SY_STATES.DataRequired('');
    DM_SYSTEM.SY_STATE_DEPOSIT_FREQ.DataRequired('');
    DM_SYSTEM.SY_GLOBAL_AGENCY.DataRequired('');
    tmpClient:= -1;
    MyList := TisStringList.Create;
    try
      for i := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[i]);
        if tmpClient <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
        begin
          ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
          tmpClient := wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger;
          Evo := TevHistData.Create;
        end;

        TaxDates := TisListOfValues.Create;

        DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);

        ctx_UpdateWait('Processing ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']);

        MyList.Add('Company #' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']);

        Q := TevQuery.Create('select distinct x.EE_NBR from PR_CHECK x join PR p on p.PR_NBR=x.PR_NBR where p.PROCESS_DATE > :CheckDate and p.CO_NBR=:CO_NBR');
        Q.Param['CheckDate'] := EncodeDate(2015, 2, 17);
        Q.Param['CO_NBR'] := wwdsClient.DataSet['CO_NBR'];

        Q.Result.First;

        while not Q.Result.Eof do
        begin
          ctx_UpdateWait('Processing ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'] + '  ' + IntToStr(Q.Result.RecNo) + '/' + IntToStr(Q.Result.RecordCount));
          Person := Evo.GetPerson(Q.Result.FieldByName('EE_NBR').AsInteger, GetEndYear(Now));
          for j := 0 to Person.CheckCount - 1 do
          begin
            Check := Person.Checks[j];
            DM_CLIENT.PR.DataRequired('PR_NBR='+IntToStr(Check.PrNbr));
            if (DM_CLIENT.PR.PROCESS_DATE.AsDateTime > EncodeDate(2015, 2, 17)) and (Check.EeNbr = Q.Result.FieldByName('EE_NBR').AsInteger) then
            begin
              Diff := Check.NetAmount - Check.NetWages;
              if Abs(Diff) > 0.005 then
              begin
                DM_CLIENT.EE.DataRequired('EE_NBR='+IntToStr(Check.EeNbr));
                if Diff > 0.005 then
                for k := 0 to Check.StateTaxCount - 1 do
                begin
                  Tax := Check.StateTaxes[k].StateTax;
                  if Tax = 0 then
                  begin
                    State := Check.StateTaxes[k].State;
                    EeStatesNbr := Evo.GetEeStatesNbr(Check.EeNbr, State, Now);
                    CoStatesNbr := Evo.GetEeCoStatesNbr(EeStatesNbr, Now);
                    DM_CLIENT.PR.DataRequired('PR_NBR='+IntToStr(Check.PrNbr));
                    AllOk := False;
                    ctx_DataAccess.UnlockPR;
                    try
                      DM_CLIENT.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR='+IntToStr(Check.CheckNbr)+' and EE_STATES_NBR='+IntToStr(EeStatesNbr));
                      if DM_CLIENT.PR_CHECK_STATES.RecordCount = 0 then
                      begin
                        DM_CLIENT.PR_CHECK_STATES.Insert;
                        DM_CLIENT.PR_CHECK_STATES.PR_CHECK_NBR.AsInteger := Check.CheckNbr;
                        DM_CLIENT.PR_CHECK_STATES.EE_STATES_NBR.AsInteger := EeStatesNbr;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('ER_SDI_TAX').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value := 0;
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := 'N';
                        DM_CLIENT.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE;
                      end
                      else
                        DM_CLIENT.PR_CHECK_STATES.Edit;

                      DM_CLIENT.PR_CHECK_STATES.STATE_TAX.Value := Diff;
                      DM_CLIENT.PR_CHECK_STATES.Post;
                      ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_STATES]);

                      if TaxDates.ValueExists(DateToStr(Check.CheckDate)) then
                      begin
                        Liabilities := IInterface(TaxDates.Value[DateToStr(Check.CheckDate)]) as IisListOfValues;
                      end
                      else
                      begin
                        Liabilities := TisListOfValues.Create;
                        TaxDates.AddValue(DateToStr(Check.CheckDate), Liabilities);
                      end;

                      if not Liabilities.ValueExists(IntToStr(CoStatesNbr)) then
                        Liabilities.AddValue(IntToStr(CoStatesNbr), Diff)
                      else
                        Liabilities.Value[IntToStr(CoStatesNbr)] := Liabilities.Value[IntToStr(CoStatesNbr)] + Diff;

                      Diff := 0;
                      AllOk := True;
                    finally
                      ctx_DataAccess.LockPR(AllOK);
                    end;
                    break;
                  end;
                end;
                if Abs(Diff) > 0.005 then
                begin
                  for k := 0 to Check.StateTaxCount - 1 do
                  begin
                    Tax := Check.StateTaxes[k].StateTax;
                    State := Check.StateTaxes[k].State;
                    EeStatesNbr := Evo.GetEeStatesNbr(Check.EeNbr, State, Now);
                    Q1 := TevQuery.Create('select OVERRIDE_STATE_TAX_VALUE from EE_STATES where {AsOfNow<EE_STATES>} and EE_STATES_NBR='+IntToStr(EeStatesNbr));

                    if ((ConvertNull(Q1.Result['OVERRIDE_STATE_TAX_VALUE'], 0) <> 0) and (Diff > 0) and (Abs(ConvertNull(Q1.Result['OVERRIDE_STATE_TAX_VALUE'], 0) - Tax) < 0.005))
                     or ((Diff < 0) and (Tax + Diff > -0.005)) then
                    begin
                      State := Check.StateTaxes[k].State;
                      EeStatesNbr := Evo.GetEeStatesNbr(Check.EeNbr, State, Now);
                      CoStatesNbr := Evo.GetEeCoStatesNbr(EeStatesNbr, Now);
                      DM_CLIENT.PR.DataRequired('PR_NBR='+IntToStr(Check.PrNbr));
                      AllOk := False;
                      ctx_DataAccess.UnlockPR;
                      try
                        DM_CLIENT.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR='+IntToStr(Check.CheckNbr)+' and EE_STATES_NBR='+IntToStr(EeStatesNbr));
                        if DM_CLIENT.PR_CHECK_STATES.RecordCount > 0 then
                        begin
                          DM_CLIENT.PR_CHECK_STATES.Edit;

                          DM_CLIENT.PR_CHECK_STATES.STATE_TAX.Value := DM_CLIENT.PR_CHECK_STATES.STATE_TAX.Value + Diff;
                          DM_CLIENT.PR_CHECK_STATES.Post;
                          ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_STATES]);

                          //if Diff > 0 then
                          begin

                            if TaxDates.ValueExists(DateToStr(Check.CheckDate)) then
                            begin
                              Liabilities := IInterface(TaxDates.Value[DateToStr(Check.CheckDate)]) as IisListOfValues;
                            end
                            else
                            begin
                              Liabilities := TisListOfValues.Create;
                              TaxDates.AddValue(DateToStr(Check.CheckDate), Liabilities);
                            end;

                            if not Liabilities.ValueExists(IntToStr(CoStatesNbr)) then
                              Liabilities.AddValue(IntToStr(CoStatesNbr), Diff)
                            else
                              Liabilities.Value[IntToStr(CoStatesNbr)] := Liabilities.Value[IntToStr(CoStatesNbr)] + Diff;

                          end;
                          Diff := 0;
                        end;
                        AllOk := True;
                      finally
                        ctx_DataAccess.LockPR(AllOK);
                      end;
                      break;
                    end;
                  end;
                end;
                if Abs(Diff) > 0.005 then
                begin
                  MyList.Add('EE#'+Trim(DM_CLIENT.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) + ' ' + DateToStr(Check.CheckDate)+' $'+FloatToStr(RoundTwo(Check.NetAmount - Check.NetWages))+' NOT fixed.');
                end
                else
                begin
                  MyList.Add('EE#'+Trim(DM_CLIENT.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) + ' ' + DateToStr(Check.CheckDate)+' fixed $'+FloatToStr(RoundTwo(Check.NetAmount - Check.NetWages)));
                end;
              end;
            end;
          end;
          Q.Result.Next;
        end;
        for k := 0 to TaxDates.Count - 1 do
        begin
          Liabilities := IInterface(TaxDates.Values[k].Value) as IisListOfValues;
          DM_CLIENT.PR.DataRequired('PR_NBR=0');
          DM_CLIENT.CO_PR_ACH.DataRequired('PR_NBR=0');
          DM_CLIENT.CO_STATE_TAX_LIABILITIES.DataRequired('PR_NBR=0');
          DM_CLIENT.PR.Insert;
          DM_CLIENT.PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
          DM_CLIENT.PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
          DM_CLIENT.PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
          DM_CLIENT.PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
          DM_CLIENT.PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
          DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger := DM_CLIENT.CO.CO_NBR.Value;
          DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime := StrToDate(TaxDates.Values[k].Name);
          DM_CLIENT.PR.FieldByName('PROCESS_DATE').AsDateTime := Date;
          DM_CLIENT.PR.FieldByName('RUN_NUMBER').AsInteger := ctx_PayrollCalculation.GetNextRunNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime);
          DM_CLIENT.PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
          DM_CLIENT.PR.FieldByName('SCHEDULED').Value := 'N';
          DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_TAX_ADJUSTMENT;
          DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
          DM_CLIENT.PR.FieldByName('EXCLUDE_ACH').Value := 'N';
          DM_CLIENT.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
          DM_CLIENT.PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y';
          DM_CLIENT.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
          DM_CLIENT.PR.FieldByName('EXCLUDE_BILLING').Value := 'Y';
          DM_CLIENT.PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_REPORTS;
          DM_CLIENT.PR.TAX_IMPOUND.Value := DM_CLIENT.CO.TAX_IMPOUND.Value;
          DM_CLIENT.PR.TRUST_IMPOUND.Value := DM_CLIENT.CO.TRUST_IMPOUND.Value;
          DM_CLIENT.PR.DD_IMPOUND.Value := DM_CLIENT.CO.DD_IMPOUND.Value;
          DM_CLIENT.PR.BILLING_IMPOUND.Value := DM_CLIENT.CO.BILLING_IMPOUND.Value;
          DM_CLIENT.PR.WC_IMPOUND.Value := DM_CLIENT.CO.WC_IMPOUND.Value;
          DM_CLIENT.PR.Post;
          MyList.Add('T payroll added ' + DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString+'-'+DM_CLIENT.PR.FieldByName('RUN_NUMBER').AsString);
          if DM_COMPANY.CO.TAX_SERVICE.AsString = 'Y' then
          begin
            DM_CLIENT.CO_PR_ACH.Insert;
            DM_CLIENT.CO_PR_ACH.FieldByName('PR_NBR').Value := DM_CLIENT.PR['PR_NBR'];
            DM_CLIENT.CO_PR_ACH.FieldByName('CO_NBR').Value := DM_CLIENT.CO.CO_NBR.Value;;
            DM_CLIENT.CO_PR_ACH.FieldByName('STATUS').Value := COMMON_REPORT_STATUS__PENDING;
            DM_CLIENT.CO_PR_ACH.FieldByName('ACH_DATE').Value := Date;
            DM_CLIENT.CO_PR_ACH.FieldByName('AMOUNT').Value := 0;
            DM_CLIENT.CO_PR_ACH.TAX_IMPOUND.Value := DM_CLIENT.PR.TAX_IMPOUND.Value;
            DM_CLIENT.CO_PR_ACH.TRUST_IMPOUND.Value := DM_CLIENT.PR.TRUST_IMPOUND.Value;
            DM_CLIENT.CO_PR_ACH.DD_IMPOUND.Value := DM_CLIENT.PR.DD_IMPOUND.Value;
            DM_CLIENT.CO_PR_ACH.BILLING_IMPOUND.Value := DM_CLIENT.PR.BILLING_IMPOUND.Value;
            DM_CLIENT.CO_PR_ACH.WC_IMPOUND.Value := DM_CLIENT.PR.WC_IMPOUND.Value;
            DM_CLIENT.CO_PR_ACH.STATUS_DATE.Value := Date;
            DM_CLIENT.CO_PR_ACH.Post;
          end;

          for n := 0 to Liabilities.Count - 1 do
          begin
            MyList.Add('     $' + FloatToStr(RoundTwo(Liabilities.Values[n].Value)));
            DM_CLIENT.CO_STATES.DataRequired('CO_STATES_NBR='+Liabilities.Values[n].Name);
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.Insert;
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('CO_STATES_NBR').Value := StrToInt(Liabilities.Values[n].Name);
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('DUE_DATE').Value := DM_CLIENT.PR['CHECK_DATE'];
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('CHECK_DATE').Value := DM_CLIENT.PR['CHECK_DATE'];
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('TAX_TYPE').Value := TAX_LIABILITY_TYPE_STATE;
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(Liabilities.Values[n].Value);
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('PR_NBR').Value := DM_CLIENT.PR.PR_NBR.AsInteger;
            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';

            DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('ACH_KEY').AsString :=
                        ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(STATE_TAX_DESC, DM_CLIENT.CO_STATES.SY_STATE_DEPOSIT_FREQ_NBR.AsInteger, DM_CLIENT.CO_STATES.SY_STATES_NBR.AsInteger,
                                                    DM_CLIENT.PR['CHECK_DATE']);

            DM_CLIENT.CO_STATE_TAX_LIABILITIES.Post;
          end;

          ctx_DataAccess.PostDataSets([DM_CLIENT.PR, DM_CLIENT.CO_PR_ACH, DM_CLIENT.CO_STATE_TAX_LIABILITIES]);

          CalcDueDates := TevCalcDueDates.Create;
          try
            CalcDueDates.Calculate(DM_CLIENT.PR['PR_NBR'], DM_CLIENT.PR['CHECK_DATE'], S);
          finally
            CalcDueDates.Free;
          end;

          ctx_DataAccess.PostDataSets([DM_CLIENT.CO_STATE_TAX_LIABILITIES]);

        end;
      end;
    finally
      if MyList.Count > 0 then
         evRichEdit1.Lines := MyList.VCLStrings;
      PageControl1.ActivePageIndex := 1;
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
  end;

end;

procedure TEDIT_SB_UTILS.btnChangeTaxPaymentMethodClick(Sender: TObject);
var
  f: TEDIT_CHANGE_TAX_PAY_METHOD;
  i: Integer;

begin
  inherited;

  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;

  f := TEDIT_CHANGE_TAX_PAY_METHOD.Create(nil);
  try
    if f.ShowModal = mrOk then
    begin
      ctx_StartWait;
      wwdsClient.DataSet.DisableControls;
      try
          for i := 0 to wwgdClient.SelectedList.Count - 1 do
          begin
            ctx_UpdateWait(wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
            wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[i]);

            if ctx_DataAccess.ClientId <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
              ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

            DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').asString + ' and STATE=' + QuotedStr(f.edState.DisplayValue));
            if DM_COMPANY.CO_STATES.RecordCount > 0 then
            begin
              if (f.cbCState.Value <> '') and (f.cbNState.Value <> '') then
              begin
                 if DM_COMPANY.CO_STATES.FieldValues['STATE_TAX_DEPOSIT_METHOD'] = f.cbCState.Value then
                 begin
                   DM_COMPANY.CO_STATES.Edit;
                   DM_COMPANY.CO_STATES.FieldValues['STATE_TAX_DEPOSIT_METHOD'] := f.cbNState.Value;
                   DM_COMPANY.CO_STATES.Post;
                 end;
              end;

              if (f.cbCSui.Value <> '') and (f.cbNSui.Value <> '') then
              begin
                 if DM_COMPANY.CO_STATES.FieldValues['SUI_TAX_DEPOSIT_METHOD'] = f.cbCSui.Value then
                 begin
                   DM_COMPANY.CO_STATES.Edit;
                   DM_COMPANY.CO_STATES.FieldValues['SUI_TAX_DEPOSIT_METHOD'] := f.cbNSui.Value;
                   DM_COMPANY.CO_STATES.Post;
                 end;

                 DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').asString + ' and CO_STATES_NBR = ' + DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').asString);
                 DM_COMPANY.CO_SUI.First;
                 while not DM_COMPANY.CO_SUI.Eof do
                 begin
                   if DM_COMPANY.CO_SUI.FieldValues['PAYMENT_METHOD'] = f.cbCSui.Value then
                   begin
                     DM_COMPANY.CO_SUI.Edit;
                     DM_COMPANY.CO_SUI.FieldValues['PAYMENT_METHOD'] := f.cbNSui.Value;
                     DM_COMPANY.CO_SUI.Post;
                   end;
                   DM_COMPANY.CO_SUI.Next;
                 end;
              end;
              if (f.cbCLocal.Value <> '') and (f.cbNLocal.Value <> '') then
              begin
                DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').asString + ' and SY_LOCALS_NBR=' + f.edLocal.LookupValue);
                DM_COMPANY.CO_LOCAL_TAX.First;
                while not DM_COMPANY.CO_LOCAL_TAX.Eof do
                begin
                  if DM_COMPANY.CO_LOCAL_TAX.FieldValues['PAYMENT_METHOD'] = f.cbCLocal.Value then
                  begin
                     DM_COMPANY.CO_LOCAL_TAX.Edit;
                     DM_COMPANY.CO_LOCAL_TAX.FieldValues['PAYMENT_METHOD'] := f.cbNLocal.Value;
                     DM_COMPANY.CO_LOCAL_TAX.Post;
                  end;
                  DM_COMPANY.CO_LOCAL_TAX.Next;
                end;
              end;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_STATES, DM_COMPANY.CO_LOCAL_TAX, DM_COMPANY.CO_SUI]);
          end;
      finally
        DM_SYSTEM.SY_STATES.close;
        DM_SYSTEM.SY_LOCALS.close;
        DM_COMPANY.CO_STATES.Close;
        DM_COMPANY.CO_SUI.Close;
        DM_COMPANY.CO_LOCAL_TAX.Close;
        ctx_EndWait;
        wwdsClient.DataSet.EnableControls;
      end;
    end;
  finally
    FreeAndNil(f);
  end;
end;

procedure TEDIT_SB_UTILS.btnChangeEeAcaFormsTypeClick(Sender: TObject);
var
  i: Integer;
  sAcaStatus, sAcaFormDefault, sQuery, sAcaType, AcaStatusToSelect: string;
  Q: IevQuery;
  effDate: TDateTime;
  reportData: TevClientDataSet;

  procedure AddToReportData(aClNbr, aCoNbr, aEeNbr: integer; const OldAcaType, NewAcaType: string; aEffDate: TDateTime);
  begin
    with reportData do
    begin
      Append;
      FieldByName('CL_NBR').AsInteger := aClNbr;
      FieldByName('CO_NBR').AsInteger := aCoNbr;
      FieldByName('EE_NBR').AsInteger := aEeNbr;
      FieldByName('OLD_ACA_TYPE').AsString := OldAcaType;
      FieldByName('NEW_ACA_TYPE').AsString := NewAcaType;
      FieldByName('EFFECTIVE_DATE').AsDateTime := aEffDate;
      Post;
    end;
  end;

  procedure RunEeAcaTypeChangeReport;
  var
    Report: TrwRepParams;
    Reports: TrwReportList;
    qTask: IevRunReportTask;
  begin
    Reports := TrwReportList.Create;
    try
      Report := Reports.AddReport;
      Report.Level := 'S';
      Report.NBR := 3319;
      Report.Caption := 'EE ACA Forms Type Change Report';
      Report.ReturnParams := true;

      Report.Params.Add('DataSets', DataSetsToVarArray([reportData]));

      // for debugging - begin
//      Report.Params.SaveToFile('C:\Users\asolovyov\Desktop\Reports\System Reports\S3319\105247\parameters.rwp');
      // for debugging - end

      qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_RUN_REPORT, True) as IevRunReportTask;
      qTask.Caption := 'Run EE ACA Forms Type Change report';
      qTask.Reports := Reports.GetAsStream;
      qTask.Destination := rdtPreview;
      ctx_EvolutionGUI.AddTaskQueue(qTask, False);
      EvMessage('Please see the view queue for the EE ACA Forms Type Change Report (S3319) listing changes made using the utility.');
    finally
      FreeAndNil(Reports);
    end;
  end;
begin
  inherited;

  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;

  AcaStatusToSelect :=
    'Full Time' + #9 + ACA_STATUS_FULL + #13 +
    'Full Time Ongoing' + #9 + ACA_STATUS_FULL_ONGOING + #13 +
    'Part Time' + #9 + ACA_STATUS_PART + #13 +
    'Part Time Ongoing' + #9 + ACA_STATUS_PART_ONGOING;

  if not SelectComboChoices( 'Select ACA Status to be reviewed for Forms Type update', AcaStatusToSelect, sAcaStatus ) then
    Exit;

  if sAcaStatus = '' then
    sAcaStatus := ''' ''';

  effDate := EncodeDate(2015, 12, 31);// Now;
  sQuery :=
    'select e.EE_NBR, e.Custom_Employee_Number EeCode, e.ACA_TYPE, e1.EE_NBR UpdateType ' +
    'from EE e left outer join (' +

    '  select distinct t.EE_NBR ' +
    '  from EE t ' +
    '  where t.CO_NBR = :CoNbr and t.ACA_STATUS in (' + sAcaStatus + ') and ' +
    '    ((t.EFFECTIVE_DATE BETWEEN :dat_b AND :dat_e) or (t.EFFECTIVE_UNTIL BETWEEN :dat_b AND :dat_e) or ' +
    '    ((t.EFFECTIVE_DATE <= :dat_b) and (t.EFFECTIVE_UNTIL >= :dat_e))) ' +

    ') e1 on e.EE_NBR = e1.EE_NBR ' +
    'where :dat_now >= e.EFFECTIVE_DATE and :dat_now < e.EFFECTIVE_UNTIL and ' +
    '  e.CO_NBR = :CoNbr ' +
    'order by e.Ee_Nbr';


  ctx_StartWait;
  wwdsClient.DataSet.DisableControls;

  reportData := TevClientDataSet.Create(nil);
  reportData.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
  reportData.FieldDefs.Add('CO_NBR', ftInteger, 0, True);
  reportData.FieldDefs.Add('EE_NBR', ftInteger, 0, True);
  reportData.FieldDefs.Add('OLD_ACA_TYPE', ftString, 1, False);
  reportData.FieldDefs.Add('NEW_ACA_TYPE', ftString, 1, False);
  reportData.FieldDefs.Add('EFFECTIVE_DATE', ftDateTime);
  reportData.CreateDataSet;
  reportData.LogChanges := False;

  try
    for i := 0 to wwgdClient.SelectedList.Count - 1 do
    begin
      ctx_UpdateWait(wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
      wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[i]);

      if ctx_DataAccess.ClientId <> wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger then
        ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

      DM_COMPANY.CO.DataRequired('CO_NBR=' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
      sAcaFormDefault := DM_COMPANY.CO.FieldByName('ACA_FORM_DEFAULT').AsString;

      ctx_UpdateWait('Processing ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']);

      Q := TevQuery.Create(sQuery);
      Q.Params.AddValue('dat_now', effDate);
      Q.Params.AddValue('dat_b', StartOfTheYear(effDate));
      Q.Params.AddValue('dat_e', effDate);
      Q.Params.AddValue('CoNbr', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
      Q.Execute;

      Q.Result.First;
      while not Q.Result.EOF do
      begin
        ctx_UpdateWait('Processing ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'] + #13 +
          'Emoloyee #' + Q.Result.FieldByName('EeCode').AsString);

        if (not Q.Result.FieldByName('UpdateType').IsNull) then
          sAcaType := sAcaFormDefault
        else
          sAcaType := EE_ACA_TYPE_NONE;

        if (Q.Result.FieldByName('ACA_TYPE').AsString <> sAcaType) then
        begin
          // update in DB
          ctx_DBAccess.UpdateFieldValue('EE', 'ACA_TYPE', Q.Result.FieldByName('EE_NBR').AsInteger, effDate, DayBeforeEndOfTime, sAcaType);

          // store data for report
          AddToReportData(
            wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger,
            wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger,
            Q.Result.FieldByName('EE_NBR').AsInteger,
            Q.Result.FieldByName('ACA_TYPE').AsString,
            sAcaType,
            effDate);
        end;
        Q.Result.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.EE]);
      DM_COMPANY.EE.Close;
      DM_COMPANY.EE.DataRequired('ALL');
    end;

    // run report
    RunEeAcaTypeChangeReport;
  finally
    ctx_EndWait;
    wwdsClient.DataSet.EnableControls;
    reportData.Close;
    FreeAndNil(reportData);
  end;
end;

procedure TEDIT_SB_UTILS.btnACAOtherCOClick(Sender: TObject);
var
  I: Integer;
  s: string;
  Q, Q1: IevQuery;
begin

  if wwgdClient.SelectedList.Count < 1 then
  begin
    EvMessage('You have to select at least one company');
    Exit;
  end;

  if EvMessage('Are you sure you want to run ACA Other CO utility for all selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  ctx_StartWait;
  try
    wwdsClient.DataSet.DisableControls;
    try
      DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.Open;

      for I := 0 to wwgdClient.SelectedList.Count - 1 do
      begin
        wwdsClient.DataSet.GotoBookmark(wwgdClient.SelectedList.Items[I]);

        s:= 'select a.SB_ACA_GROUP_NBR from SB_ACA_GROUP a ' +
              'where {AsOfNow<a>} and a.PRIMARY_CL_NBR = :clNBR and a.PRIMARY_CO_NBR = :coNBR ';
        Q1 := TevQuery.Create(s);
        Q1.Params.AddValue('clNBR', wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);
        Q1.Params.AddValue('coNBR', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
        Q1.Execute;
        if Q1.Result.RecordCount > 0 then
        begin

         ctx_DataAccess.OpenClient(wwdsClient.DataSet.FieldByName('CL_NBR').AsInteger);

         s:= 'select a.CO_ADDITIONAL_INFO_NAMES_NBR, b.CO_ADDITIONAL_INFO_VALUES_NBR, b.INFO_VALUE from CO_ADDITIONAL_INFO_NAMES a, CO_ADDITIONAL_INFO_VALUES b ' +
                  'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                     ' a.co_nbr = :coNBR and ' +
                     ' a.CO_ADDITIONAL_INFO_NAMES_NBR = b.CO_ADDITIONAL_INFO_NAMES_NBR and ' +
                     ' uppercase(a.NAME) = ''ALE MEMBER'' ';
         Q := TevQuery.Create(s);
         Q.Params.AddValue('coNBR', wwdsClient.DataSet.FieldByName('CO_NBR').AsInteger);
         Q.Execute;
         if Q.Result.RecordCount > 0 then
         begin
          try

            DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.DataRequired('CO_NBR = ' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);
            DM_COMPANY.CO_ADDITIONAL_INFO_VALUES.DataRequired('CO_NBR = ' + wwdsClient.DataSet.FieldByName('CO_NBR').AsString);

            Q.Result.First;
            while not Q.Result.EOF do
            begin
              DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.Insert;
              DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.FieldByName('SB_ACA_GROUP_NBR').AsInteger :=
                  Q1.Result.FieldByName('SB_ACA_GROUP_NBR').AsInteger;
              DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.FieldByName('ALE_EIN_NUMBER').AsString :=
                  Copy(Q.Result.FieldByName('INFO_VALUE').AsString, 1, 9);
              DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.FieldByName('ALE_COMPANY_NAME').AsString :=
                  Copy(Q.Result.FieldByName('INFO_VALUE').AsString, 11,  Length(Q.Result.FieldByName('INFO_VALUE').AsString) - 10);
              DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.Post;

              if DM_COMPANY.CO_ADDITIONAL_INFO_VALUES.Locate('CO_ADDITIONAL_INFO_VALUES_NBR', Q.Result.FieldByName('CO_ADDITIONAL_INFO_VALUES_NBR').AsInteger, []) then
                 DM_COMPANY.CO_ADDITIONAL_INFO_VALUES.Delete;
              Q.Result.Next;
            end;
            
            Q.Result.First;
            while not Q.Result.EOF do
            begin
              if DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.Locate('CO_ADDITIONAL_INFO_NAMES_NBR', Q.Result.FieldByName('CO_ADDITIONAL_INFO_NAMES_NBR').AsInteger, []) then
                 DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.Delete;
              Q.Result.Next;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_ADDITIONAL_INFO_VALUES, DM_COMPANY.CO_ADDITIONAL_INFO_NAMES, DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS]);
          except
            DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS.Cancel;
            raise;
          end;

         end;
        end
        else
          EvMessage('There is no corresponding ACA ALE Group with that company as the main company! CO# ' + wwdsClient.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

      end;
    finally
      wwdsClient.DataSet.EnableControls;
    end;
  finally
    ctx_EndWait;
  end;

end;

initialization
  RegisterClass(TEDIT_SB_UTILS);
end.

