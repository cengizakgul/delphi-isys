//******************************************************************************
// File Name:       SAdjReportParamsDlg
// Creation Date:   8/21/2007
// Project:         evdBureau
// Created By:      Andrey Solovyov
//
// Description:
//   Input Form for Printing Combined Adjustment Reports
//******************************************************************************

unit SAdjReportParamsDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ISBasicClasses,  StdCtrls, ActnList,
  ComCtrls, Spin, EvUIComponents;

type
  TdlgAdjReportParams = class(TForm)
    pnlMain: TevPanel;
    btnPrint: TevButton;
    btnPreview: TevButton;
    btnCancel: TevButton;
    dlgActions: TevActionList;
    Print: TAction;
    Preview: TAction;
    grbCheckDatePeriod: TevGroupBox;
    lblYear: TevLabel;
    cmbYear: TevComboBox;
    chb1stQtr: TevCheckBox;
    chb2ndQtr: TevCheckBox;
    chb3rdQtr: TevCheckBox;
    chb4thQtr: TevCheckBox;
    lblFrom: TevLabel;
    edFrom: TevDateTimePicker;
    lblTo: TevLabel;
    edTo: TevDateTimePicker;
    lblTaxLimit: TevLabel;
    edTaxAdjLimit: TevEdit;
    chbQueue: TevCheckBox;
    procedure FormShow(Sender: TObject);
    procedure edToChange(Sender: TObject);
    procedure cmbYearChange(Sender: TObject);
    procedure chb1stQtrClick(Sender: TObject);
    procedure edTaxAdjLimitChange(Sender: TObject);
    procedure PrintExecute(Sender: TObject);
    procedure PreviewExecute(Sender: TObject);
  private
    procedure DateChangedManually;
    procedure SetDates;
  public
    PrintResult: boolean;
  end;

implementation

uses DateUtils;

{$R *.dfm}

procedure TdlgAdjReportParams.DateChangedManually;
begin
  cmbYear.ItemIndex := -1;

  chb1stQtr.Checked := False;
  chb2ndQtr.Checked := False;
  chb3rdQtr.Checked := False;
  chb4thQtr.Checked := False;

  chb1stQtr.Enabled := False;
  chb2ndQtr.Enabled := False;
  chb3rdQtr.Enabled := False;
  chb4thQtr.Enabled := False;
end;

procedure TdlgAdjReportParams.FormShow(Sender: TObject);
var
  CurYear: word;
begin
  CurYear := YearOf( Now );
  cmbYear.Items.Clear;
  cmbYear.Items.Add( IntToStr(CurYear - 3) );
  cmbYear.Items.Add( IntToStr(CurYear - 2) );
  cmbYear.Items.Add( IntToStr(CurYear - 1) );
  cmbYear.Items.Add( IntToStr(CurYear) );
  cmbYear.Items.Add( IntToStr(CurYear + 1) );
  cmbYear.ItemIndex := 3;

  chb1stQtr.Checked := True;
  chb2ndQtr.Checked := True;
  chb3rdQtr.Checked := True;
  chb4thQtr.Checked := True;
  chbQueue.Checked := True;

  edFrom.DateTime := StartOfTheYear( Now );
  edTo.DateTime := EndOfTheYear( Now );

  PrintResult := True;
end;

procedure TdlgAdjReportParams.edToChange(Sender: TObject);
begin
  DateChangedManually;
end;

procedure TdlgAdjReportParams.cmbYearChange(Sender: TObject);
var
  CurQuarter: word;
begin
  if not chb1stQtr.Enabled and (cmbYear.ItemIndex > -1) then
  begin
    chb1stQtr.Enabled := True;
    chb2ndQtr.Enabled := True;
    chb3rdQtr.Enabled := True;
    chb4thQtr.Enabled := True;

    CurQuarter := (MonthOf( now ) div 4) + 1;
    case CurQuarter of
      1: chb1stQtr.Checked := True;
      2: chb2ndQtr.Checked := True;
      3: chb3rdQtr.Checked := True;
      4: chb4thQtr.Checked := True;
    end;
  end;
  SetDates;
end;

procedure TdlgAdjReportParams.SetDates;
var
  SelectedYear: word;
begin
  if cmbYear.ItemIndex > -1 then
  begin
    SelectedYear := StrToInt(cmbYear.Text);
    if chb4thQtr.Checked then edFrom.DateTime := StartOfAMonth( SelectedYear, 10 );
    if chb3rdQtr.Checked then edFrom.DateTime := StartOfAMonth( SelectedYear, 7 );
    if chb2ndQtr.Checked then edFrom.DateTime := StartOfAMonth( SelectedYear, 4 );
    if chb1stQtr.Checked then edFrom.DateTime := StartOfAYear( SelectedYear );

    if chb1stQtr.Checked then edTo.DateTime := EndOfAMonth( SelectedYear, 3 );
    if chb2ndQtr.Checked then edTo.DateTime := EndOfAMonth( SelectedYear, 6 );
    if chb3rdQtr.Checked then edTo.DateTime := EndOfAMonth( SelectedYear, 9 );
    if chb4thQtr.Checked then edTo.DateTime := EndOfAYear( SelectedYear );
  end;
end;

procedure TdlgAdjReportParams.chb1stQtrClick(Sender: TObject);
begin
  if (Sender as TevCheckBox) = chb1stQtr then
  begin
    if chb1stQtr.Checked then
    begin
      if chb4thQtr.Checked then chb3rdQtr.Checked := True;
      if chb3rdQtr.Checked then chb2ndQtr.Checked := True;
    end;
  end
  else if (Sender as TevCheckBox) = chb2ndQtr then
  begin
    if (not chb2ndQtr.Checked) and chb1stQtr.Checked and chb3rdQtr.Checked then
      chb2ndQtr.Checked := True;
  end
  else if (Sender as TevCheckBox) = chb3rdQtr then
  begin
    if (not chb3rdQtr.Checked) and chb2ndQtr.Checked and chb4thQtr.Checked then
      chb3rdQtr.Checked := True;
  end
  else if (Sender as TevCheckBox) = chb4thQtr then
  begin
    if chb4thQtr.Checked then
    begin
      if chb1stQtr.Checked then chb2ndQtr.Checked := True;
      if chb2ndQtr.Checked then chb3rdQtr.Checked := True;
    end;
  end;
  SetDates;
end;

procedure TdlgAdjReportParams.edTaxAdjLimitChange(Sender: TObject);
begin
  try
    StrToFloat(edTaxAdjLimit.Text);
  except
    edTaxAdjLimit.Text := '0.00';
  end;
end;

procedure TdlgAdjReportParams.PrintExecute(Sender: TObject);
begin
  PrintResult := True;
end;

procedure TdlgAdjReportParams.PreviewExecute(Sender: TObject);
begin
  PrintResult := False;
end;

end.
