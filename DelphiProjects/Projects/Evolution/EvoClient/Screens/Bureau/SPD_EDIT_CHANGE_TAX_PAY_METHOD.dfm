object EDIT_CHANGE_TAX_PAY_METHOD: TEDIT_CHANGE_TAX_PAY_METHOD
  Left = 784
  Top = 253
  Width = 693
  Height = 209
  Caption = 'Change Payment Method for Agency'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 16
    Top = 32
    Width = 25
    Height = 13
    Caption = 'State'
  end
  object evLabel4: TevLabel
    Left = 16
    Top = 96
    Width = 26
    Height = 13
    Caption = 'Local'
  end
  object evLabel5: TevLabel
    Left = 16
    Top = 64
    Width = 39
    Height = 13
    Caption = 'SUI       '
  end
  object evLabel2: TevLabel
    Left = 240
    Top = 8
    Width = 126
    Height = 13
    Caption = 'Current Payment Method   '
  end
  object evLabel3: TevLabel
    Left = 456
    Top = 8
    Width = 114
    Height = 13
    Caption = 'New Payment Method   '
  end
  object edState: TevDBLookupCombo
    Left = 56
    Top = 24
    Width = 161
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'STATE'#9'2'#9'STATE'#9'F')
    LookupTable = DM_SY_STATES.SY_STATES
    LookupField = 'STATE'
    Style = csDropDownList
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = edStateChange
  end
  object edLocal: TevDBLookupCombo
    Left = 56
    Top = 88
    Width = 161
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME'#9'40'#9'LOCALITY_NAME'#9#9
      'LocalTypeDescription'#9'20'#9'LocalTypeDescription'#9#9
      'CountyName'#9'40'#9'CountyName'#9#9
      'AgencyName'#9'40'#9'AgencyName'#9#9
      'SY_LOCALS_NBR'#9'10'#9'SY_LOCALS_NBR'#9'F'
      'SY_STATES_NBR'#9'10'#9'SY_STATES_NBR'#9'F')
    LookupTable = DM_SY_LOCALS.SY_LOCALS
    LookupField = 'SY_LOCALS_NBR'
    Style = csDropDownList
    Enabled = False
    TabOrder = 1
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = edLocalChange
  end
  object btnOk: TButton
    Left = 232
    Top = 136
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 2
    OnClick = btnOkClick
  end
  object btnCancel: TButton
    Left = 368
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object cbCState: TevDBComboBox
    Left = 232
    Top = 24
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 4
    UnboundDataType = wwDefault
  end
  object cbCSui: TevDBComboBox
    Left = 232
    Top = 56
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 5
    UnboundDataType = wwDefault
  end
  object cbNLocal: TevDBComboBox
    Left = 456
    Top = 88
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 6
    UnboundDataType = wwDefault
  end
  object cbCLocal: TevDBComboBox
    Left = 232
    Top = 88
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 7
    UnboundDataType = wwDefault
  end
  object cbNSui: TevDBComboBox
    Left = 456
    Top = 56
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 8
    UnboundDataType = wwDefault
  end
  object cbNState: TevDBComboBox
    Left = 456
    Top = 24
    Width = 209
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DropDownCount = 8
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 0
    ParentFont = False
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 9
    UnboundDataType = wwDefault
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 144
    Top = 136
  end
end
