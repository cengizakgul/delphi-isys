// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_APPLY_SUI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook,  SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvUIUtils, EvUIComponents;

type
  TEDIT_APPLY_SUI = class(TForm)
    evLabel1: TevLabel;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    edState: TevDBLookupCombo;
    evLabel2: TevLabel;
    edDueDate: TevDateTimePicker;
    evLabel3: TevLabel;
    edEndCheckDate: TevDateTimePicker;
    bOk: TevButton;
    evLabel4: TevLabel;
    edSui: TevDBLookupCombo;
    evLabel5: TevLabel;
    edBeginCheckDate: TevDateTimePicker;
    procedure bOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TEDIT_APPLY_SUI.bOkClick(Sender: TObject);
begin
  if (edState.DisplayValue <> '') and (edSui.DisplayValue <> '') then
  begin
    EvMessage('This utility will automatically create QEC that will only clean up SUI taxes.  It will also automatically process these QEC payrolls, even if there are Employee taxes that were cleaned up.');
    ModalResult := mrOk;
  end;
end;

procedure TEDIT_APPLY_SUI.FormCreate(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_STATES.Activate;
  DM_SYSTEM_STATE.SY_SUI.DataRequired('SUI_ACTIVE = ''Y'' ');

  edBeginCheckDate.DateTime := Date;
  edEndCheckDate.DateTime := Date;
  edDueDate.DateTime := Date;

end;


procedure TEDIT_APPLY_SUI.edStateChange(Sender: TObject);
begin
  edSui.Clear;
  DM_SYSTEM_STATE.SY_SUI.Filter :='SUI_ACTIVE = ''Y'' and SY_STATES_NBR ='+ IntToStr(DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR']);
  DM_SYSTEM_STATE.SY_SUI.Filtered :=True;
end;

end.
