// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_ZERO_ACH_PAYMENT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  wwdbdatetimepicker, wwdblook, SDataStructure,
  Spin, EvUIComponents;

type
  TEDIT_ZERO_ACH_PAYMENT = class(TForm)
    evLabel1: TevLabel;
    dtpDueDate: TevDBDateTimePicker;
    evButton1: TevButton;
    Memo1: TMemo;
    evLabel2: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    dtpEffectiveDate: TevDBDateTimePicker;
    evLabel3: TevLabel;
    dtpPeriodEndDate: TevDBDateTimePicker;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    edPaymentSeqNumber: TevSpinEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure evButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TEDIT_ZERO_ACH_PAYMENT.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #27 then
    Close;
end;

procedure TEDIT_ZERO_ACH_PAYMENT.evButton1Click(Sender: TObject);
begin
  if (dtpDueDate.Text <> '') and (dtpEffectiveDate.Text <> '') and (dtpPeriodEndDate.Text <> '') and (evDBLookupCombo1.Text <> '') then
    ModalResult := mrOk;
end;

end.
