inherited EDIT_SB_ENABLED_DASHBOARDS: TEDIT_SB_ENABLED_DASHBOARDS
  Width = 938
  Height = 532
  object btnAddDashboards: TevSpeedButton [0]
    Left = 400
    Top = 115
    Width = 74
    Height = 22
    Caption = 'Add'
    HideHint = True
    AutoSize = False
    OnClick = btnAddDashboardsClick
    NumGlyphs = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
      DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
      DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
      78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
      8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
      F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
      F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
      DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
      DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
    Layout = blGlyphRight
    ParentColor = False
    ShortCut = 0
  end
  object btnDelDashbords: TevSpeedButton [1]
    Left = 400
    Top = 197
    Width = 74
    Height = 22
    Caption = 'Remove'
    HideHint = True
    AutoSize = False
    OnClick = btnDelDashbordsClick
    NumGlyphs = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
      DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
      DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
      78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
      448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
      F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
      F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
      44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
      DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
    ParentColor = False
    ShortCut = 0
  end
  object grdSbEnanledDashboards: TevDBGrid [2]
    Left = 488
    Top = 15
    Width = 373
    Height = 350
    DisableThemesInTitle = False
    Selected.Strings = (
      'DESCRIPTION'#9'80'#9'Description'#9'F'
      'LEVEL_DESC'#9'8'#9'Level Desc'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SB_ENABLED_DASHBOARDS\grdSbEnanledDashboards'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = wwdsDetail
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object grdSyDashboards: TevDBGrid [3]
    Left = 12
    Top = 15
    Width = 373
    Height = 350
    DisableThemesInTitle = False
    Selected.Strings = (
      'DESCRIPTION'#9'80'#9'Description'#9#9)
    IniAttributes.Enabled = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsSyDashboard
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_ENABLED_DASHBOARDS.SB_ENABLED_DASHBOARDS
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 656
    Top = 344
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 600
    Top = 352
  end
  object dsSyDashboard: TevDataSource
    DataSet = cdSyDashboards
    Left = 288
    Top = 400
  end
  object cdSyDashboards: TevClientDataSet
    Left = 560
    Top = 288
    object cdSyDashboardsSY_DASHBOARDS_NBR: TIntegerField
      FieldName = 'SY_DASHBOARDS_NBR'
    end
    object cdSyDashboardsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 80
    end
  end
end
