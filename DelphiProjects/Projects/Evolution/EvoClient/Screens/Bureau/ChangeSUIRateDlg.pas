// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ChangeSUIRateDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, EvTypes,
     Buttons, ExtCtrls, SDataStructure,  wwdblook, EvUtils, Spin,
     EvExceptions, SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents;

type
  TdglChangeSUIRate = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    evEdit1: TevEdit;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evComboBox1: TevComboBox;
    evSpinEdit1: TevSpinEdit;
    evRateAsAdCheck: TevCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure evDBLookupCombo1Change(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DateUtils;

{$R *.dfm}

procedure TdglChangeSUIRate.FormCreate(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_SUI.DataRequired();
  evSpinEdit1.Value := YearOf(Date);
  evRateAsAdCheck.Checked := true;
end;

procedure TdglChangeSUIRate.evDBLookupCombo1Change(Sender: TObject);
begin
  evEdit1.Text := DM_SYSTEM_STATE.SY_SUI.FieldByName('NEW_COMPANY_DEFAULT_RATE').AsString;
end;

procedure TdglChangeSUIRate.OKBtnClick(Sender: TObject);
begin
  if evDBLookupCombo1.LookupValue = '' then
  begin
    ModalResult := mrNone;
    raise EInvalidParameters.Create('SUI can not be empty');
  end;
  if evEdit1.Text = '' then
  begin
    ModalResult := mrNone;
    raise EInvalidParameters.Create('Rate can not be empty');
  end;
end;

end.
