object LockQuarterliesDlgForm: TLockQuarterliesDlgForm
  Left = 376
  Top = 230
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Input required'
  ClientHeight = 98
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  DesignSize = (
    298
    98)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 8
    Top = 8
    Width = 152
    Height = 13
    Caption = 'Please, enter a quarter end date'
  end
  object OKBtn: TButton
    Left = 135
    Top = 65
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelBtn: TButton
    Left = 215
    Top = 65
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object DataEdit: TevEdit
    Left = 8
    Top = 28
    Width = 281
    Height = 21
    TabOrder = 0
  end
  object LockCheck: TevCheckBox
    Left = 8
    Top = 64
    Width = 97
    Height = 17
    Caption = 'Lock Tax Pmts'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
end
