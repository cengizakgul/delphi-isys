inherited EDIT_SB_CUSTOM_VENDORS: TEDIT_SB_CUSTOM_VENDORS
  object evLabel1: TevLabel [0]
    Left = 12
    Top = 266
    Width = 74
    Height = 16
    Caption = '~Vendor Name'
  end
  object evLabel2: TevLabel [1]
    Left = 12
    Top = 310
    Width = 51
    Height = 16
    Caption = '~Category'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TevLabel [2]
    Left = 12
    Top = 354
    Width = 36
    Height = 16
    Caption = '~Type '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [3]
    Left = 0
    Top = 0
    Width = 522
    Height = 253
    DisableThemesInTitle = False
    Selected.Strings = (
      'VENDOR_NAME'#9'80'#9'Vendor Name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SB_CUSTOM_VENDORS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object edtName: TevDBEdit [4]
    Left = 12
    Top = 282
    Width = 489
    Height = 21
    HelpContext = 5524
    DataField = 'VENDOR_NAME'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object lcbCategories: TevDBLookupCombo [5]
    Left = 12
    Top = 326
    Width = 261
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'DESCRIPTION'#9'30'#9'DESCRIPTION'#9'F')
    DataField = 'SY_VENDOR_CATEGORIES_NBR'
    DataSource = wwdsDetail
    LookupTable = DM_SY_VENDOR_CATEGORIES.SY_VENDOR_CATEGORIES
    LookupField = 'SY_VENDOR_CATEGORIES_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
  end
  object cbType: TevDBComboBox [6]
    Left = 12
    Top = 370
    Width = 169
    Height = 21
    HelpContext = 54501
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DataField = 'VENDOR_TYPE'
    DataSource = wwdsDetail
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_CUSTOM_VENDORS.SB_CUSTOM_VENDORS
    MasterDataSource = nil
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 384
    Top = 40
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 440
    Top = 48
  end
end
