object EDIT_UPDATE_STATE_DUE_DATE: TEDIT_UPDATE_STATE_DUE_DATE
  Left = 557
  Top = 180
  Width = 472
  Height = 438
  Caption = 'Make selection'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 16
    Top = 80
    Width = 25
    Height = 13
    Caption = 'State'
  end
  object evLabel2: TevLabel
    Left = 16
    Top = 236
    Width = 65
    Height = 13
    Caption = 'Old Due Date'
  end
  object evLabel3: TevLabel
    Left = 16
    Top = 276
    Width = 71
    Height = 13
    Caption = 'New Due Date'
  end
  object evLabel4: TevLabel
    Left = 16
    Top = 144
    Width = 26
    Height = 13
    Caption = 'Local'
  end
  object evLabel5: TevLabel
    Left = 16
    Top = 112
    Width = 39
    Height = 13
    Caption = 'SUI       '
  end
  object evLabel6: TevLabel
    Left = 16
    Top = 203
    Width = 66
    Height = 13
    Caption = 'Year / Month '
  end
  object edState: TevDBLookupCombo
    Left = 104
    Top = 72
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'STATE'#9'2'#9'STATE'#9'F')
    LookupTable = DM_SY_STATES.SY_STATES
    LookupField = 'STATE'
    Style = csDropDownList
    TabOrder = 1
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = edStateChange
  end
  object edOldDate: TevDateTimePicker
    Left = 104
    Top = 228
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 3
  end
  object edNewDate: TevDateTimePicker
    Left = 104
    Top = 268
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 4
  end
  object bOk: TevButton
    Left = 152
    Top = 316
    Width = 75
    Height = 25
    Caption = 'Convert'
    TabOrder = 5
    OnClick = bOkClick
  end
  object rgTypeChoice: TevRadioGroup
    Left = 16
    Top = 16
    Width = 209
    Height = 41
    Columns = 4
    ItemIndex = 0
    Items.Strings = (
      'State'
      'SUI'
      'FUI'
      'Local')
    TabOrder = 0
    OnClick = rgTypeChoiceClick
  end
  object edLocal: TevDBLookupCombo
    Left = 104
    Top = 136
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME'#9'40'#9'LOCALITY_NAME'#9#9
      'LocalTypeDescription'#9'20'#9'LocalTypeDescription'#9#9
      'CountyName'#9'40'#9'CountyName'#9#9
      'AgencyName'#9'40'#9'AgencyName'#9#9
      'SY_LOCALS_NBR'#9'10'#9'SY_LOCALS_NBR'#9'F'
      'SY_STATES_NBR'#9'10'#9'SY_STATES_NBR'#9'F')
    LookupTable = DM_SY_LOCALS.SY_LOCALS
    LookupField = 'SY_LOCALS_NBR'
    Style = csDropDownList
    Enabled = False
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object edSui: TevDBLookupCombo
    Left = 104
    Top = 104
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'SUI_TAX_NAME'#9'40'#9'Sui Tax Name'#9'F')
    LookupTable = DM_SY_SUI.SY_SUI
    LookupField = 'SY_SUI_NBR'
    Style = csDropDownList
    TabOrder = 6
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object evTCDLocalTax: TevCheckBox
    Left = 104
    Top = 169
    Width = 97
    Height = 17
    Caption = 'All EIT Taxes'
    TabOrder = 7
    OnClick = evTCDLocalTaxClick
  end
  object cbYears: TevComboBox
    Left = 104
    Top = 195
    Width = 81
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
  end
  object cbMonth: TevComboBox
    Left = 192
    Top = 195
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 9
    Items.Strings = (
      '1'#39'st quarter'
      '2'#39'nd quarter'
      '3'#39'rd quarter'
      '4'#39'th quarter')
  end
  object evSelectCheckDate: TevCheckBox
    Left = 232
    Top = 169
    Width = 169
    Height = 17
    Caption = 'Select Check Date Range'
    TabOrder = 10
    OnClick = evSelectCheckDateClick
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 48
    Top = 224
  end
end
