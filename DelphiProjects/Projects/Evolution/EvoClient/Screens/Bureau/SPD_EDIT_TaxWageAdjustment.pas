// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TaxWageAdjustment;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook, EvUIComponents, SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, LMDCustomButton, LMDButton,
  isUILMDButton, isUIwwDBLookupCombo, SDataDictclient, DB, Wwdatsrc, Grids,
  Wwdbigrd, Wwdbgrid, isUIEdit, evContext, EvUIUtils, EvTypes;

type
  TEDIT_APPLY_TAXABLEWAGES = class(TForm)
    edEndCheckDate: TevDateTimePicker;
    btnUpdate: TevButton;
    evLabel5: TevLabel;
    edBeginCheckDate: TevDateTimePicker;
    evLabel1: TevLabel;
    Label1: TLabel;
    grdColocals: TevDBGrid;
    DM_COMPANY: TDM_COMPANY;
    dsEE: TevDataSource;
    dsCO_locals: TevDataSource;
    evButton1: TevButton;
    grdEE: TevDBGrid;
    evLabel2: TevLabel;
    Label2: TLabel;
    Label3: TLabel;
    edAmount: TevEdit;
    evLabel3: TevLabel;
    procedure btnUpdateClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grdColocalsSelectionChanged(Sender: TObject);
  private
    { Private declarations }
    FAmount : double;
    procedure DoUpdate;
  public
    { Public declarations }
  end;

implementation

uses EvCommonInterfaces, EvDataSet, isDataSet;

{$R *.DFM}

procedure TEDIT_APPLY_TAXABLEWAGES.DoUpdate;
const
  sqlAllPrAmt =  'Select Sum(pl.Local_Taxable_wage) AMT from PR_Check_locals pl'#13 +
                      'where {asofnow<pl>} '#13 +
                      'and pl.PR_Check_nbr in ( select pc.PR_check_nbr from PR p'#13 +
                      '                join pr_check pc on p.pr_nbr= pc.pr_nbr'#13 +
                      '                where p.co_nbr = :conbr and PC.EE_NBR = :eenbr'#13 +
                      '                and p.status <> ''W'''#13 +
                      '                and p.Check_date between :dat_b and :dat_e'#13 +
                      '                and {asofnow<p>} and {asofnow<pc>} )'#13 +
                      'and pl.EE_locals_nbr in ( Select EE_locals_NBR from EE_locals'#13 +
                      '                       where {asofnow<EE_locals>} '#13 +
                      '                       and CO_LOCAL_TAX_NBR = :colocaltaxnbr'#13 +
                      '                       and EE_NBR = :eenbr) and pl.reportable = ''Y''';

  sqlLastPrAmt = 'Select first 1 pl.Local_Taxable_wage AMT, pl.pr_nbr, pl.pr_check_locals_nbr from PR_Check_locals pl, PR p'#13 +
              'where {asofnow<pl>} and {asofnow<p>} '#13 +
              'and p.pr_nbr = pl.pr_nbr'#13 +
              'and pl.PR_Check_nbr in ( select pc.PR_check_nbr from PR p'#13 +
              '                join pr_check pc on p.pr_nbr= pc.pr_nbr'#13 +
              '                where p.co_nbr = :conbr and PC.EE_NBR = :eenbr'#13 +
              '                and p.status <> ''W'''#13 +
              '                and p.Check_date between :dat_b and :dat_e'#13 +
              '                and {asofnow<p>} and {asofnow<pc>} )'#13 +
              'and pl.EE_locals_nbr in ( Select EE_locals_NBR from EE_locals'#13 +
              '                       where {asofnow<EE_locals>} '#13 +
              '                       and CO_LOCAL_TAX_NBR = :colocaltaxnbr'#13 +
              '                       and EE_NBR = :eenbr) and pl.reportable = ''Y'''#13 +
              'order by p.check_date DESC, p.run_number DESC';

var
  i : integer;
  Qry : IevQuery;
  SumLocalTaxWage, LastLocalTaxWage : double;
  pr_nbr: Integer;
  AllOk : Boolean;
begin
  dsEE.DataSet.DisableControls;
  try
    for i := 0 to grdEE.SelectedList.Count - 1 do
    begin
      dsEE.DataSet.GotoBookmark(grdEE.SelectedList.Items[I]);
      if (dsEE.DataSet.FieldByName('EE_NBR').AsInteger > 0) and (DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').asInteger > 0) then
      begin
        Qry := TevQuery.Create(sqlAllPrAmt);
        Qry.Params.AddValue('conbr',DM_COMPANY.CO.FieldByName('CO_NBR').asInteger);
        Qry.Params.AddValue('eenbr',dsEE.DataSet.FieldByName('EE_NBR').AsInteger);
        Qry.Params.AddValue('dat_b', edBeginCheckDate.Date);
        Qry.Params.AddValue('dat_e', edEndCheckDate.Date);
        Qry.Params.AddValue('coLocaltaxnbr',DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').asString);
        Qry.Execute;
        if Qry.Result.FieldByName('AMT').Isnull then continue;

        SumLocalTaxWage :=  Qry.Result.FieldByName('AMT').asFloat;
        if (SumLocalTaxWage = 0.0) and (FAmount = 0.0)then continue;
        if SumLocalTaxWage = Famount then continue;

        Qry := TevQuery.Create(sqlLastPrAmt);
        Qry.Params.AddValue('conbr',DM_COMPANY.CO.FieldByName('CO_NBR').asInteger);
        Qry.Params.AddValue('eenbr',dsEE.DataSet.FieldByName('EE_NBR').AsInteger);
        Qry.Params.AddValue('dat_b', edBeginCheckDate.Date);
        Qry.Params.AddValue('dat_e', edEndCheckDate.Date);
        Qry.Params.AddValue('coLocaltaxnbr',DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').asString);
        Qry.Execute;

        if Qry.Result.RecordCount = 0 then continue;

        LastLocalTaxWage := Qry.Result.FieldByName('AMT').asFloat;
        pr_nbr := Qry.Result.FieldByName('pr_nbr').AsInteger;
        AllOK := false;

        DM_COMPANY.PR_CHECK_LOCALS.DataRequired('PR_Check_Locals_NBR = '+ Qry.Result.FieldByName('PR_Check_Locals_nbr').asString);
        DM_COMPANY.PR.DataRequired('PR_NBR = '+ IntToStr(pr_nbr));
        if not DM_COMPANY.PR_CHECK_LOCALS.IsEmpty then
        begin
          ctx_DataAccess.UnlockPR;
          try
            with DM_COMPANY.PR_CHECK_LOCALS do
            begin
              Edit;
              FieldByName('LOCAL_TAXABLE_WAGE').Value := FAmount - (SumLocalTaxWage - LastLocalTaxWage);
              Post;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.PR_CHECK_LOCALS]);
            AllOK := True;
          finally
            ctx_DataAccess.LockPR(AllOK);
          end;
        end;
      end;// if
    end;//for

    EvMessage('Update Completed', mtInformation, [mbOK]);
  finally
    dsEE.DataSet.EnableControls;
  end;
end;

procedure TEDIT_APPLY_TAXABLEWAGES.btnUpdateClick(Sender: TObject);
begin
  if grdColocals.SelectedList.Count = 0 then exit;
  if grdEE.SelectedList.Count = 0 then exit;
  try
   FAmount := StrToFloat(edAmount.text);
  except
   raise exception.create('please enter amount correctly');
  end;

  if MessageDlg('This utility will automatically create an entry in the employee''s final payroll of the period, '#10#13 +
                'to adjust local Taxable Wages for all payrolls in the selected period.  Are you sure?',
            mtInformation, [mbYes, mbNo], 0) = mrYes then
  begin
    DoUpdate;
  end;
end;

procedure TEDIT_APPLY_TAXABLEWAGES.FormShow(Sender: TObject);
begin
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR =' + DM_COMPANY.CO.FieldByName('Co_nbr').asString + ' AND SY_STATES_NBR =41'); //Only PA
  DM_COMPANY.EE.DataRequired('CO_NBR =' + DM_COMPANY.CO.FieldByName('Co_nbr').asString);
  Label2.Caption := 'Company : ' +Trim(DM_COMPANY.CO.FieldByName('NAME').AsString)
             + '(' +Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString)+ ')';
  edBeginCheckDate.DateTime := EncodeDate(2012,1,1);
  edEndCheckDate.DateTime := EncodeDate(2012,12,31);
  btnUpdate.enabled := false;
  grdColocals.SelectRecord;
  grdEE.SelectRecord;
end;

procedure TEDIT_APPLY_TAXABLEWAGES.grdColocalsSelectionChanged(
  Sender: TObject);
begin
  btnUpdate.enabled := ((grdEE.SelectedList.Count > 0) and (grdColocals.SelectedList.Count > 0));
end;
end.
