// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_REPORTS;

interface

uses
  SysUtils,
  SFrameEntry,  Dialogs, wwdblook, StdCtrls, DBCtrls,
  ExtCtrls, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Controls, Buttons,
  Wwdbigrd, Grids, Wwdbgrid, ComCtrls, Classes, Db, SDataStructure,
  Wwdatsrc, Windows, SPackageEntry, EvConsts, SDDClasses, EvContext,
  ISBasicClasses, SDataDictclient, SDataDictsystem, SDataDictbureau, Menus,
  SDataDicttemp, isBaseClasses, Variants, isBasicUtils, SReportSettings,
  ISUtils, EvUIUtils, EvUIComponents, EvClientDataSet, isUIwwDBComboBox,
  isUIwwDBEdit, isUIwwDBLookupCombo, isUIDBMemo;

type
  TEDIT_SB_REPORTS = class(TFrameEntry)
    pctlEDIT_SB_REPORTS: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtReports_Detail: TTabSheet;
    lablDescription: TevLabel;
    lablNotes: TevLabel;
    dedtDescription: TevDBEdit;
    dmemNotes: TEvDBMemo;
    wwdgEDIT_SB_REPORTS: TevDBGrid;
    OpenDialog1: TOpenDialog;
    bevSB_REPORT1: TEvBevel;
    Label1: TevLabel;
    Label2: TevLabel;
    wwlcSB_REPORTS: TevDBLookupCombo;
    wwlcSY_REPORTS: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    memNotes: TEvDBMemo;
    evComboBox1: TevDBComboBox;
    DM_CLIENT: TDM_CLIENT;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    cbReportParams: TComboBox;
    Label5: TLabel;
    Label6: TLabel;
    cbReportParamValue: TComboBox;
    btnSetDefaultValue: TButton;
    Panel4: TPanel;
    grSelCompanies: TevDBCheckGrid;
    grSelReports: TevDBCheckGrid;
    Splitter1: TSplitter;
    dsCompanies: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
    procedure evComboBox1Change(Sender: TObject);
    procedure cbReportParamsChange(Sender: TObject);
    procedure btnSetDefaultValueClick(Sender: TObject);
    procedure grSelReportsSelectionChanged(Sender: TObject);
  private
    FDefaultParams: IisParamsCollection;
    FParamTypes: IisParamsCollection;
    procedure FillRWDescription;
    procedure SetDefaultParamForReports(const AReports: IisStringList; const ClNbr, CoNbr :integer; 
                                        const AParamName: String; const AValue: Variant);
    function  GetSelectedReportParam: IisListOfValues;
    function  GetSelectedReportParamValue: Variant;
  public
    { Public declarations }
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_REPORTS: TEDIT_SB_REPORTS;

implementation

uses
  EvUtils;

{$R *.DFM}

function TEDIT_SB_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_REPORTS;
end;

procedure TEDIT_SB_REPORTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS, SupportDataSets, 'REPORT_TYPE in (''' +
                   SYSTEM_REPORT_TYPE_NORMAL + ''', ''' +
                   SYSTEM_REPORT_TYPE_MULTICLIENT + ''', ''' +
                   SYSTEM_REPORT_TYPE_HR + ''', ''' +
                   SYSTEM_REPORT_TYPE_ASCII + ''')');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS, SupportDataSets, '');
end;

procedure TEDIT_SB_REPORTS.FillRWDescription;
begin
  if (DM_SERVICE_BUREAU.SB_REPORTS['REPORT_LEVEL'] = CH_DATABASE_SERVICE_BUREAU) and     DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Locate('SB_REPORT_WRITER_REPORTS_NBR', DM_SERVICE_BUREAU.SB_REPORTS['REPORT_WRITER_REPORTS_NBR'], []) then
    TMemoField(DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('RWDescription')).Value := DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.FieldByName('notes').Value
  else if (DM_SERVICE_BUREAU.SB_REPORTS['REPORT_LEVEL'] = CH_DATABASE_SYSTEM) and
          DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('SY_REPORT_WRITER_REPORTS_NBR', DM_SERVICE_BUREAU.SB_REPORTS['REPORT_WRITER_REPORTS_NBR'], []) then
  begin
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.GetBlobData('NOTES');
    TMemoField(DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('RWDescription')).Value := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.NOTES.AsString;
  end
  else
    TMemoField(DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('RWDescription')).Value := '';
end;

procedure TEDIT_SB_REPORTS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
    FillRWDescription;
end;

procedure TEDIT_SB_REPORTS.evComboBox1Change(Sender: TObject);
begin
  inherited;
  if evComboBox1.Value = CH_DATABASE_SYSTEM then
  begin
    Label2.Visible := True;
    wwlcSB_REPORTS.Visible := False;
    wwlcSY_REPORTS.Visible := True;
  end
  else if evComboBox1.Value = CH_DATABASE_SERVICE_BUREAU then
  begin
    Label2.Visible := True;
    wwlcSB_REPORTS.Visible := True;
    wwlcSY_REPORTS.Visible := False;
  end
  else
  begin
    Label2.Visible := False;
    wwlcSB_REPORTS.Visible := False;
    wwlcSY_REPORTS.Visible := False;
  end;
end;

procedure TEDIT_SB_REPORTS.Activate;
var
  sOldIndex: string;
  Par: IisListOfValues;
  i: Integer;
begin
  inherited;
  wwdsDetail.DataSet := nil;
  DM_SERVICE_BUREAU.SB_REPORTS.DisableControls;
  ctx_StartWait;
  sOldIndex := DM_SERVICE_BUREAU.SB_REPORTS.IndexName;
  DM_SERVICE_BUREAU.SB_REPORTS.IndexName := '';
  try
    while not DM_SERVICE_BUREAU.SB_REPORTS.Eof do
    begin
      DM_SERVICE_BUREAU.SB_REPORTS.Edit;
      FillRWDescription;
      DM_SERVICE_BUREAU.SB_REPORTS.Post;
      DM_SERVICE_BUREAU.SB_REPORTS.Next;
    end;
    DM_SERVICE_BUREAU.SB_REPORTS.First;
  finally
    DM_SERVICE_BUREAU.SB_REPORTS.MergeChangeLog;
    DM_SERVICE_BUREAU.SB_REPORTS.IndexName := sOldIndex;
    DM_SERVICE_BUREAU.SB_REPORTS.EnableControls;
    wwdsDetail.DataSet := DM_SERVICE_BUREAU.SB_REPORTS;
    ctx_EndWait;
  end;

  DM_TEMPORARY.TMP_CO.Open;

  btnSetDefaultValue.Enabled := False;

  // Prepare report params
  FParamTypes := TisParamsCollection.Create;
  Par := FParamTypes.AddParams('Checkbox');
  Par.AddValue('Checked', True);
  Par.AddValue('Unchecked',  False);

  FDefaultParams := TisParamsCollection.Create;
  Par := FDefaultParams.AddParams('Mask Sensitive Information');
  Par.AddValue('Param', 'SecureResult');
  Par.AddValue('Type',  'Checkbox');

  for i := 0 to FDefaultParams.Count - 1 do
    cbReportParams.Items.Add(FDefaultParams.ParamName(i));
  cbReportParams.Sorted := True;
  cbReportParams.ItemIndex := 0;
  cbReportParamsChange(nil);
end;

procedure TEDIT_SB_REPORTS.SetDefaultParamForReports(const AReports: IisStringList;
  const ClNbr, CoNbr :integer; const AParamName: String; const AValue: Variant);
var
  s: string;
  InputParams: TrwReportParams;
  Param: TrwReportParam;
begin
  // Apply change by clients
  InputParams := TrwReportParams.Create;
  DM_COMPANY.CO_REPORTS.Close;
  DM_COMPANY.CO_REPORTS.RetrieveCondition := '';
  try
      if ctx_DataAccess.ClientID <> ClNbr then
        ctx_DataAccess.OpenClient(ClNbr);
      DM_COMPANY.CO_REPORTS.RetrieveCondition := 'CO_NBR in (' + IntToStr(CoNbr) + ')';
      DM_COMPANY.CO_REPORTS.Open;
      try
        while not DM_COMPANY.CO_REPORTS.Eof do
        begin
          s := DM_COMPANY.CO_REPORTS.REPORT_LEVEL.AsString + DM_COMPANY.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsString;
          if AReports.IndexOf(s) <> -1 then
          begin
            if not DM_COMPANY.CO_REPORTS.INPUT_PARAMS.IsNull then
              try
                InputParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.INPUT_PARAMS));
              except
                InputParams.Clear;
              end
            else
              InputParams.Clear;

            Param := InputParams.ParamByName(AParamName);
            if (AValue = Null) and Assigned(Param) then
              Param.Free
            else
            begin
              if not Assigned(Param) then
                InputParams.Add(AParamName, AValue, True)
              else
                Param.Value := AValue;
            end;

            DM_COMPANY.CO_REPORTS.Edit;
            InputParams.SaveToBlobField(TBlobField(DM_COMPANY.CO_REPORTS.INPUT_PARAMS));
            DM_COMPANY.CO_REPORTS.Post;
          end;

          DM_COMPANY.CO_REPORTS.Next;
        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_REPORTS]);
      finally
        DM_COMPANY.CO_REPORTS.Close;
        DM_COMPANY.CO_REPORTS.RetrieveCondition := '';
      end;
  finally
    InputParams.Free;
  end;
end;

procedure TEDIT_SB_REPORTS.cbReportParamsChange(Sender: TObject);
var
  Param: IisListOfValues;
  i: Integer;
begin
  cbReportParamValue.Items.Clear;

  Param := FParamTypes.ParamsByName(GetSelectedReportParam.Value['Type']);
  for i := 0 to Param.Count - 1 do
    cbReportParamValue.Items.Add(Param[i].Name);
  cbReportParamValue.Items.Add('Hardcoded Default');
  cbReportParamValue.ItemIndex := 0;
end;

function TEDIT_SB_REPORTS.GetSelectedReportParam: IisListOfValues;
begin
  Result := FDefaultParams.ParamsByName(cbReportParams.Text);
end;

function TEDIT_SB_REPORTS.GetSelectedReportParamValue: Variant;
var
  Param: IisListOfValues;
begin
  Param := FParamTypes.ParamsByName(GetSelectedReportParam.Value['Type']);
  Result := Param.TryGetValue(cbReportParamValue.Text, Null);
end;

procedure TEDIT_SB_REPORTS.btnSetDefaultValueClick(Sender: TObject);
var
  SelReports: IisStringList;
  BM: String;
begin
   if not AreYouSure('Report parameters will be changed for selected companies. Continue?') then
     Exit;

   ctx_StartWait('Report parameters is being changed. Please wait...');
   try
     // Build report list
     SelReports := TisStringList.CreateUnique;
     BM := DM_SERVICE_BUREAU.SB_REPORTS.Bookmark;
     DM_SERVICE_BUREAU.SB_REPORTS.DisableControls;
     try
       DM_SERVICE_BUREAU.SB_REPORTS.First;
       while not DM_SERVICE_BUREAU.SB_REPORTS.Eof do
       begin
         if grSelReports.IsSelectedRecord then
           SelReports.Add(DM_SERVICE_BUREAU.SB_REPORTS.REPORT_LEVEL.AsString + DM_SERVICE_BUREAU.SB_REPORTS.REPORT_WRITER_REPORTS_NBR.AsString);
         DM_SERVICE_BUREAU.SB_REPORTS.Next;
       end;
    finally
      DM_SERVICE_BUREAU.SB_REPORTS.Bookmark := BM;
      DM_SERVICE_BUREAU.SB_REPORTS.EnableControls;
    end;

    BM := DM_TEMPORARY.TMP_CO.Bookmark;
    DM_TEMPORARY.TMP_CO.DisableControls;
    try
      DM_TEMPORARY.TMP_CO.First;
      while not DM_TEMPORARY.TMP_CO.Eof do
      begin
        if grSelCompanies.IsSelectedRecord then
           SetDefaultParamForReports(SelReports, DM_TEMPORARY.TMP_CO.CL_NBR.AsInteger, DM_TEMPORARY.TMP_CO.CO_NBR.AsInteger,
                                GetSelectedReportParam.Value['Param'], GetSelectedReportParamValue);
        DM_TEMPORARY.TMP_CO.Next;
      end;
    finally
      DM_TEMPORARY.TMP_CO.Bookmark := BM;
      DM_TEMPORARY.TMP_CO.EnableControls;
    end;

  finally
    ctx_EndWait;
  end;

  EvMessage('Report parameters have been changed');
end;

procedure TEDIT_SB_REPORTS.grSelReportsSelectionChanged(Sender: TObject);
begin
  btnSetDefaultValue.Enabled := (grSelReports.SelectedList.Count > 0) and (grSelCompanies.SelectedList.Count > 0);
end;

initialization
  Classes.RegisterClass(TEDIT_SB_REPORTS);

end.
