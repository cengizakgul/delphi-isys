unit SelectSBBankAccount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SDDClasses, SDataDictbureau, SDataStructure, wwdblook,
  ISBasicClasses,  EvTypes, EvUtils, DBCtrls, EvConsts, EvExceptions, EvUIComponents;

type
  TdlgSelectSBBankAccount = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evLabel1: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evcbTRUST_ACCOUNT: TevCheckBox;
    evcbTAX_ACCOUNT: TevCheckBox;
    evcbACH_ACCOUNT: TevCheckBox;
    evcbOBC_ACCOUNT: TevCheckBox;
    evcbWORKERS_COMP_ACCOUNT: TevCheckBox;
    evcbBILLING_ACCOUNT: TevCheckBox;
    evcbSkipBlank: TevCheckBox;
    procedure OKBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure evDBLookupCombo1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgSelectSBBankAccount: TdlgSelectSBBankAccount;

implementation

{$R *.dfm}

procedure TdlgSelectSBBankAccount.OKBtnClick(Sender: TObject);
begin
  if evDBLookupCombo1.LookupValue = '' then
  begin
    ModalResult := mrNone;
    raise EInvalidParameters.Create('SB Bank Account can not be empty');
  end;
end;

procedure TdlgSelectSBBankAccount.FormCreate(Sender: TObject);
begin
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired();
end;

procedure TdlgSelectSBBankAccount.evDBLookupCombo1Change(Sender: TObject);
begin
  evcbSkipBlank.Checked := false;
  evcbTRUST_ACCOUNT.Checked := false;
  evcbTAX_ACCOUNT.Checked := false;
  evcbACH_ACCOUNT.Checked := false;
  evcbBILLING_ACCOUNT.Checked := false;
  evcbWORKERS_COMP_ACCOUNT.Checked := false;
  evcbOBC_ACCOUNT.Checked := false;

  evcbTRUST_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('TRUST_ACCOUNT').AsString = GROUP_BOX_YES;
  evcbTAX_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('TAX_ACCOUNT').AsString = GROUP_BOX_YES;
  evcbACH_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('ACH_ACCOUNT').AsString = GROUP_BOX_YES;
  evcbBILLING_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('BILLING_ACCOUNT').AsString = GROUP_BOX_YES;
  evcbWORKERS_COMP_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('WORKERS_COMP_ACCOUNT').AsString = GROUP_BOX_YES;
  evcbOBC_ACCOUNT.Enabled :=
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('OBC_ACCOUNT').AsString = GROUP_BOX_YES;
end;

end.
