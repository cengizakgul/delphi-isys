object TEDIT_SB_HIRE_ACT: TTEDIT_SB_HIRE_ACT
  Left = 315
  Top = 167
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Update employees due to Hire Act'
  ClientHeight = 460
  ClientWidth = 636
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 419
    Width = 636
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btnExit: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
      OnClick = btnExitClick
    end
    object btnApply: TButton
      Left = 544
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Process'
      ModalResult = 1
      TabOrder = 1
      OnClick = btnApplyClick
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 41
    Width = 636
    Height = 378
    Align = alClient
    TabOrder = 1
    object CoListGrid: TevDBGrid
      Left = 1
      Top = 1
      Width = 634
      Height = 349
      DisableThemesInTitle = False
      Selected.Strings = (
        'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9
        'NAME'#9'40'#9'Name'#9)
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TTEDIT_SB_HIRE_ACT\CoListGrid'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = COListDataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      UseTFields = True
      PaintOptions.AlternatingRowColor = clCream
      DefaultSort = 'CUSTOM_COMPANY_NUMBER'
    end
    object evPanel4: TevPanel
      Left = 1
      Top = 350
      Width = 634
      Height = 27
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 1
      object CompaniesCountLabel: TevLabel
        Left = 16
        Top = 8
        Width = 103
        Height = 13
        Caption = '0 companies selected'
      end
    end
  end
  object evPanel3: TevPanel
    Left = 0
    Top = 0
    Width = 636
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object evLabel1: TevLabel
      Left = 11
      Top = 13
      Width = 607
      Height = 13
      Caption = 
        'Please check the list of companies to update employees who were ' +
        'set to Exempt = Yes due to the Hire Act'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object COList: TevClientDataSet
    ProviderName = 'CO_PROV'
    ReadOnly = True
    Left = 144
    Top = 64
  end
  object COListDataSource: TDataSource
    DataSet = COList
    Left = 200
    Top = 64
  end
end
