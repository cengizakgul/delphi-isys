object ChosePaperType: TChosePaperType
  Left = 494
  Top = 328
  BorderStyle = bsDialog
  ClientHeight = 168
  ClientWidth = 193
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 19
    Top = 135
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 99
    Top = 135
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object evRadioGroup1: TevRadioGroup
    Left = 8
    Top = 7
    Width = 177
    Height = 121
    Caption = 'W-2 Paper Type'
    TabOrder = 0
    OnClick = evRadioGroup1Click
  end
end
