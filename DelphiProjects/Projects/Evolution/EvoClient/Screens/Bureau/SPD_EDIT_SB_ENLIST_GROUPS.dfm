inherited EDIT_SB_ENLIST_GROUPS: TEDIT_SB_ENLIST_GROUPS
  object evGroupBox1: TevGroupBox [0]
    Left = 9
    Top = 8
    Width = 546
    Height = 521
    Caption = 
      '   Define the tax return groups that will auto-enlist for a new ' +
      'company setup.   '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 0
    object evLabel2: TevLabel
      Left = 114
      Top = 28
      Width = 255
      Height = 16
      Caption = '~Tax Return Groups:  System Level Default Settings  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel3: TevLabel
      Left = 130
      Top = 247
      Width = 174
      Height = 16
      Caption = '~New Company Setup Modifications'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLblMediaType: TevLabel
      Left = 10
      Top = 492
      Width = 56
      Height = 13
      Caption = 'Media Type'
    end
    object evLabel1: TevLabel
      Left = 10
      Top = 469
      Width = 47
      Height = 16
      Caption = '~Process'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lablName: TevLabel
      Left = 10
      Top = 445
      Width = 37
      Height = 16
      Caption = '~Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object wwdgSY_Enlist_Groups: TevDBGrid
      Left = 10
      Top = 52
      Width = 525
      Height = 145
      DisableThemesInTitle = False
      Selected.Strings = (
        'NAME'#9'40'#9'Name'#9'F'
        'Process'#9'10'#9'Process'#9#9)
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SB_ENLIST_GROUPS\wwdgSY_Enlist_Groups'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = dsSyEnlist
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
    inline SbEnlistReturnMiniNavigationFrame: TMiniNavigationFrame
      Left = 86
      Top = 210
      Width = 198
      Height = 25
      TabOrder = 1
      inherited SpeedButton1: TevSpeedButton
        Left = 2
      end
      inherited SpeedButton2: TevSpeedButton
        Left = 168
      end
      inherited evActionList1: TevActionList
        Left = 72
        Top = 8
      end
    end
    object wwdg_SB_Enlist_Groups: TevDBGrid
      Left = 10
      Top = 266
      Width = 524
      Height = 162
      DisableThemesInTitle = False
      Selected.Strings = (
        'ReportGroups'#9'40'#9'Reportgroups'#9#9
        'Process'#9'12'#9'Process'#9'F'
        'NMediaType'#9'40'#9'MediaType'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SB_ENLIST_GROUPS\wwdg_SB_Enlist_Groups'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = wwdsDetail
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 2
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
    object edReportGroup: TevDBLookupCombo
      Left = 148
      Top = 437
      Width = 298
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'40'#9'Name'#9#9)
      DataField = 'SY_REPORT_GROUPS_NBR'
      DataSource = wwdsDetail
      LookupTable = DM_SY_REPORT_GROUPS.SY_REPORT_GROUPS
      LookupField = 'SY_REPORT_GROUPS_NBR'
      Style = csDropDownList
      Enabled = False
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
    end
    object cbProcessType: TevDBComboBox
      Left = 148
      Top = 461
      Width = 189
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'PROCESS_TYPE'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 4
      UnboundDataType = wwDefault
      OnChange = cbProcessTypeChange
      OnExit = cbProcessTypeExit
    end
    object cbMediaType: TevDBComboBox
      Left = 148
      Top = 484
      Width = 234
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'MEDIA_TYPE'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 5
      UnboundDataType = wwDefault
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_ENLIST_GROUPS.SB_ENLIST_GROUPS
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = nil
    Left = 294
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 376
    Top = 40
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 424
    Top = 16
  end
  object dsSyEnlist: TevDataSource
    DataSet = cdSyEnlist
    Left = 400
    Top = 248
  end
  object cdSyEnlist: TevClientDataSet
    Left = 400
    Top = 208
  end
end
