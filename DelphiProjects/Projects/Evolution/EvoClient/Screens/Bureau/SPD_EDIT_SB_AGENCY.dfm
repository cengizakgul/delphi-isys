inherited EDIT_SB_AGENCY: TEDIT_SB_AGENCY
  Width = 772
  Height = 583
  object pctlEDIT_SB_AGENCY: TevPageControl [0]
    Left = 0
    Top = 41
    Width = 772
    Height = 542
    HelpContext = 20502
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_AGENCY: TevDBGrid
        Left = 0
        Top = 0
        Width = 625
        Height = 385
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'40'#9'Name'
          'CITY'#9'20'#9'City'
          'STATE'#9'2'#9'State')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_AGENCY\wwdgEDIT_SB_AGENCY'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Details'
      object bevSBagency1: TEvBevel
        Left = 0
        Top = 0
        Width = 764
        Height = 514
        Align = alClient
      end
      object lablName: TevLabel
        Left = 16
        Top = 16
        Width = 76
        Height = 16
        Caption = '~Agency Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress1: TevLabel
        Left = 16
        Top = 112
        Width = 47
        Height = 13
        Caption = 'Address 1'
        FocusControl = dedtAddress1
      end
      object lablAddress2: TevLabel
        Left = 16
        Top = 160
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = dedtAddress2
      end
      object lablCity: TevLabel
        Left = 16
        Top = 208
        Width = 17
        Height = 13
        Caption = 'City'
        FocusControl = dedtCity
      end
      object lablCounty: TevLabel
        Left = 16
        Top = 256
        Width = 33
        Height = 13
        Caption = 'County'
        FocusControl = dedtCounty
      end
      object lablFax: TevLabel
        Left = 312
        Top = 304
        Width = 17
        Height = 13
        Caption = 'Fax'
        FocusControl = wwdeFax
      end
      object lablE_Mail: TevLabel
        Left = 16
        Top = 304
        Width = 32
        Height = 13
        Caption = 'E-Mail '
        FocusControl = dedtE_Mail
      end
      object lablFax_Description: TevLabel
        Left = 440
        Top = 304
        Width = 53
        Height = 13
        Caption = 'Description'
        FocusControl = dedtFax_Description
      end
      object lablState: TevLabel
        Left = 164
        Top = 208
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = dedtState
      end
      object lablZip_Code: TevLabel
        Left = 208
        Top = 208
        Width = 43
        Height = 13
        Caption = 'Zip Code'
        FocusControl = wwdeZip_Code
      end
      object lablPrint_Name: TevLabel
        Left = 16
        Top = 64
        Width = 61
        Height = 16
        Caption = '~Print Name'
        FocusControl = wwdePrint_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtName: TevDBEdit
        Left = 16
        Top = 32
        Width = 257
        Height = 21
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress1: TevDBEdit
        Left = 16
        Top = 128
        Width = 257
        Height = 21
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress2: TevDBEdit
        Left = 16
        Top = 176
        Width = 257
        Height = 21
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtCity: TevDBEdit
        Left = 16
        Top = 224
        Width = 137
        Height = 21
        DataField = 'CITY'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtCounty: TevDBEdit
        Left = 16
        Top = 272
        Width = 137
        Height = 21
        DataField = 'COUNTY'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtFax_Description: TevDBEdit
        Left = 440
        Top = 320
        Width = 153
        Height = 21
        DataField = 'FAX_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 12
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtE_Mail: TevDBEdit
        Left = 16
        Top = 320
        Width = 257
        Height = 21
        DataField = 'E_MAIL_ADDRESS'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeFax: TevDBEdit
        Left = 312
        Top = 320
        Width = 121
        Height = 21
        DataField = 'FAX'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 208
        Top = 224
        Width = 73
        Height = 21
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtState: TevDBEdit
        Left = 164
        Top = 224
        Width = 33
        Height = 21
        CharCase = ecUpperCase
        DataField = 'STATE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&,@}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object grbxPrimary_Contact: TevGroupBox
        Left = 296
        Top = 16
        Width = 313
        Height = 129
        Caption = 'Primary Contact'
        TabOrder = 9
        object lablName1: TevLabel
          Left = 16
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Name'
          FocusControl = dedtName1
        end
        object lablPhone1: TevLabel
          Left = 16
          Top = 72
          Width = 31
          Height = 13
          Caption = 'Phone'
          FocusControl = wwedPhone1
        end
        object lablDescription1: TevLabel
          Left = 144
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription1
        end
        object dedtName1: TevDBEdit
          Left = 17
          Top = 40
          Width = 280
          Height = 21
          DataField = 'CONTACT1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwedPhone1: TevDBEdit
          Left = 16
          Top = 88
          Width = 121
          Height = 21
          DataField = 'PHONE1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtDescription1: TevDBEdit
          Left = 144
          Top = 88
          Width = 153
          Height = 21
          DataField = 'DESCRIPTION1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object grbxSecondary_Contact: TevGroupBox
        Left = 296
        Top = 152
        Width = 313
        Height = 129
        Caption = 'Secondary Contact'
        TabOrder = 10
        object lablName2: TevLabel
          Left = 16
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Name'
          FocusControl = dedtName2
        end
        object lablPhone2: TevLabel
          Left = 16
          Top = 72
          Width = 31
          Height = 13
          Caption = 'Phone'
          FocusControl = wwdePhone2
        end
        object lablDescription2: TevLabel
          Left = 144
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription2
        end
        object dedtName2: TevDBEdit
          Left = 16
          Top = 40
          Width = 184
          Height = 21
          DataField = 'CONTACT2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtDescription2: TevDBEdit
          Left = 144
          Top = 88
          Width = 153
          Height = 21
          DataField = 'DESCRIPTION2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone2: TevDBEdit
          Left = 16
          Top = 88
          Width = 121
          Height = 21
          DataField = 'PHONE2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object wwdePrint_Name: TevDBEdit
        Left = 16
        Top = 80
        Width = 257
        Height = 21
        DataField = 'PRINT_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Flags && Settings'
      object bevSBagency2: TEvBevel
        Left = 0
        Top = 0
        Width = 764
        Height = 514
        Align = alClient
      end
      object lablAgency_Type: TevLabel
        Left = 16
        Top = 16
        Width = 72
        Height = 16
        Caption = '~Agency Type'
        FocusControl = wwcbAgency_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablNotes: TevLabel
        Left = 16
        Top = 216
        Width = 28
        Height = 13
        Caption = 'Notes'
        FocusControl = dmemNotes
      end
      object lablModem_Nbr: TevLabel
        Left = 16
        Top = 160
        Width = 75
        Height = 13
        Caption = 'Modem Number'
        FocusControl = wwdeModem_Nbr
        Visible = False
      end
      object lablAccount_Nbr: TevLabel
        Left = 16
        Top = 112
        Width = 80
        Height = 13
        Caption = 'Account Number'
        FocusControl = dedtAccount_Nbr
      end
      object lablBank: TevLabel
        Left = 16
        Top = 64
        Width = 25
        Height = 13
        Caption = 'Bank'
        FocusControl = wwlcBank
      end
      object dmemNotes: TEvDBMemo
        Left = 16
        Top = 232
        Width = 593
        Height = 137
        DataField = 'NOTES'
        DataSource = wwdsDetail
        TabOrder = 4
      end
      object wwdeModem_Nbr: TevDBEdit
        Left = 16
        Top = 176
        Width = 129
        Height = 21
        DataField = 'MODEM_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAccount_Nbr: TevDBEdit
        Left = 16
        Top = 128
        Width = 257
        Height = 21
        DataField = 'ACCOUNT_NUMBER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwcbAgency_Type: TevDBComboBox
        Left = 16
        Top = 32
        Width = 257
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'AGENCY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object drgpAccount_Type: TevDBRadioGroup
        Left = 304
        Top = 16
        Width = 190
        Height = 75
        Caption = '~Account Type'
        DataField = 'ACCOUNT_TYPE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Checking'
          'Savings'
          'Money Market')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'C'
          'S'
          'M')
      end
      object drgpNegative_Direct_Deposit_Allowed: TevDBRadioGroup
        Left = 304
        Top = 104
        Width = 190
        Height = 50
        Caption = '~Negative Direct Deposit Allowed'
        Columns = 2
        DataField = 'NEGATIVE_DIRECT_DEP_ALLOWED'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'Y'
          'N')
        Visible = False
      end
      object wwlcBank: TevDBLookupCombo
        Left = 16
        Top = 80
        Width = 257
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'SB_BANKS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_BANKS.SB_BANKS
        LookupField = 'SB_BANKS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
      end
    end
  end
  object Panel1: TevPanel [1]
    Left = 0
    Top = 0
    Width = 772
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object lablAgency: TevLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Agency'
    end
    object dtxtNAME: TevDBText
      Left = 64
      Top = 8
      Width = 545
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
  object wwDataSource1: TevDataSource
    Left = 536
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 312
    Top = 16
  end
end
