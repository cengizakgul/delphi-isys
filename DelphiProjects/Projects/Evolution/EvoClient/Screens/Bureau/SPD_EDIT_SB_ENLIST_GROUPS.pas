// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_ENLIST_GROUPS;

interface

uses SFrameEntry,  wwdbdatetimepicker, StdCtrls, ExtCtrls,
  DBCtrls, Mask, wwdbedit, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Db, Wwdatsrc, ISBasicClasses, wwdblook, Wwdotdot, Wwdbcomb,
  SDDClasses, SDataDictsystem, SDataDictbureau, SDataStructure, Buttons,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, evUtils, Forms, SPD_MiniNavigationFrame,
  SPackageEntry, EvConsts, SFieldCodeValues, EvExceptions, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, Variants;

type
  TEDIT_SB_ENLIST_GROUPS = class(TFrameEntry)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    dsSyEnlist: TevDataSource;
    cdSyEnlist: TevClientDataSet;
    evGroupBox1: TevGroupBox;
    evLabel2: TevLabel;
    wwdgSY_Enlist_Groups: TevDBGrid;
    SbEnlistReturnMiniNavigationFrame: TMiniNavigationFrame;
    evLabel3: TevLabel;
    wwdg_SB_Enlist_Groups: TevDBGrid;
    edReportGroup: TevDBLookupCombo;
    cbProcessType: TevDBComboBox;
    cbMediaType: TevDBComboBox;
    evLblMediaType: TevLabel;
    evLabel1: TevLabel;
    lablName: TevLabel;
    Procedure GetSbEnlistFilter;
    procedure SetMediaTypeComboBox(Group: Integer);
    procedure SetMediaTypeVisible(Group: Integer);
    procedure SbEnlistReturnMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure SbEnlistReturnMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbProcessTypeExit(Sender: TObject);
    procedure cbProcessTypeChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure AfterClick( Kind: Integer ); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

var
  EDIT_SB_ENLIST_GROUPS: TEDIT_SB_ENLIST_GROUPS;

implementation

{$R *.DFM}


procedure TEDIT_SB_ENLIST_GROUPS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind in [NavOk, NavCommit] then
  begin
   if wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger  in [TAX_RETURN_GROUP_W2,TAX_RETURN_GROUP_1099M,TAX_RETURN_GROUP_1095B, TAX_RETURN_GROUP_1095C] then
    if cbProcessType.ItemIndex = 0 then
     if cbMediaType.ItemIndex = -1 then
      raise EUpdateError.CreateHelp('You need to select Media Type.', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_SB_ENLIST_GROUPS.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCommit:
    begin
      GetSbEnlistFilter;
      (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
    end;
  end;
end;


procedure TEDIT_SB_ENLIST_GROUPS.GetSbEnlistFilter;
var
 s : String;
begin
   s := '-1';
   with wwdsDetail.DataSet do
   begin
     First;
     while not Eof do
     begin
        s := s + ','+ FieldByName('SY_REPORT_GROUPS_NBR').AsString;
        Next;
     end;
   end;
  cdSyEnlist.Filter := ' Not SY_REPORT_GROUPS_NBR in ( ' + s + ')';
  cdSyEnlist.Filtered := True;
end;

procedure TEDIT_SB_ENLIST_GROUPS.SetMediaTypeComboBox(Group: Integer);
var
 i : integer;
 s, sTilde : String;

begin
    sTilde := '~';
    if Group = TAX_RETURN_GROUP_W2 then
     s := W2MediaType_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1099M then
     s := MediaType1099M_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1095B then
     s := MediaType1095B_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1095C then
     s := MediaType1095C_ComboChoices
    else 
    begin
     s := MediaType_ComboChoices;
     sTilde := '';
    end;
    if cbProcessType.ItemIndex = 1 then sTilde := '';
    evLblMediaType.Caption := sTilde + 'Media Type      ';
    cbMediaType.Items.Text := s;
    cbMediaType.ApplyList;
    cbMediaType.ItemIndex := -1;
    if (assigned(cbMediaType.Field)) and (not cbMediaType.Field.IsNull) then
    begin
      for i:=0 to cbMediaType.Items.Count - 1 do
      begin
         s := cbMediaType.Items[i];
         if s[Length(s)] = cbMediaType.Field.AsString[1] then
         begin
            cbMediaType.ItemIndex := i;
            break;
         end;
      end;
    end;
end;

procedure TEDIT_SB_ENLIST_GROUPS.SetMediaTypeVisible(Group: Integer);
begin
    if (Group = TAX_RETURN_GROUP_W2) or (Group = TAX_RETURN_GROUP_1099M) or
       (Group = TAX_RETURN_GROUP_1095B) or (Group = TAX_RETURN_GROUP_1095C)  then
    begin
     evLblMediaType.Visible := cbProcessType.ItemIndex = 0;
     cbMediaType.Visible := cbProcessType.ItemIndex = 0;
    end
    else
    begin
     evLblMediaType.Visible := False;
     cbMediaType.Visible := False;
    end;
end;


function TEDIT_SB_ENLIST_GROUPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_ENLIST_GROUPS;
end;


procedure TEDIT_SB_ENLIST_GROUPS.Activate;
var
 s : String;
begin
  inherited;
    s := 'Select a.NAME, ''Enlist'' Process, ''A'' Process_type, a.SY_REPORT_GROUPS_NBR ' +
         'from SY_REPORT_GROUPS a ' +
         'where {AsOfNow<a>} and a.NAME like ''Tax Return-%'' and not a.NAME like ''%Corrections%''';

     with TExecDSWrapper.Create(s) do
     begin
       cdSyEnlist.Close;
       ctx_DataAccess.GetSyCustomData(cdSyEnlist, AsVariant);
     end;
     cdSyEnlist.LogChanges := False;

    GetSbEnlistFilter;

    SbEnlistReturnMiniNavigationFrame.DataSource := wwdsDetail;
    SbEnlistReturnMiniNavigationFrame.InsertFocusControl := cbProcessType;
    SbEnlistReturnMiniNavigationFrame.SpeedButton1.OnClick := SbEnlistReturnMiniNavigationFrameSpeedButton1Click;
    SbEnlistReturnMiniNavigationFrame.SpeedButton2.OnClick := SbEnlistReturnMiniNavigationFrameSpeedButton2Click;

end;


function TEDIT_SB_ENLIST_GROUPS.GetInsertControl: TWinControl;
begin
  Result := cbMediaType;
end;



procedure TEDIT_SB_ENLIST_GROUPS.SbEnlistReturnMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;

  if wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR'] > 0 then
  begin
    SbEnlistReturnMiniNavigationFrame.InsertRecordExecute(Sender);
    if wwdsDetail.DataSet.State in [dsinsert] then
    begin
     wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger :=
              wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR'];
     wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := PROCESS_TYPE_DONT_ENLIST;

     SetMediaTypeComboBox(wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR']);

     evLblMediaType.Visible := False;
     cbMediaType.Visible := False;
   end;
  end;

end;

procedure TEDIT_SB_ENLIST_GROUPS.SbEnlistReturnMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  SbEnlistReturnMiniNavigationFrame.DeleteRecordExecute(Sender);
  GetSbEnlistFilter;
end;

procedure TEDIT_SB_ENLIST_GROUPS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) and (wwdsDetail.DataSet.RecordCount > 0) then
  begin
    SetMediaTypeComboBox(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
    if wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString = 'A' then
      cbProcessType.ItemIndex := 0
    else
    if wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString = 'D' then
      cbProcessType.ItemIndex := 1;
  end;

  if assigned(evLblMediaType) then
    SetMediaTypeVisible(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
end;

procedure TEDIT_SB_ENLIST_GROUPS.cbProcessTypeExit(Sender: TObject);
begin
  inherited;
  if(cbProcessType.ItemIndex = 0) and (wwdsDetail.DataSet.State in [dsinsert, dsEdit])  then
    wwdsDetail.DataSet.FieldByName('MEDIA_TYPE').Value := Null;
end;

procedure TEDIT_SB_ENLIST_GROUPS.cbProcessTypeChange(Sender: TObject);
begin
  inherited;
  SetMediaTypeVisible(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
  if wwdsDetail.DataSet.State in [dsinsert, dsEdit] then
  begin
    wwdsDetail.DataSet.FieldByName('MEDIA_TYPE').Value := Null;
    SetMediaTypeComboBox(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
  end;  
end;

initialization
  RegisterClass(TEDIT_SB_ENLIST_GROUPS);

end.
