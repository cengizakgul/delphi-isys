// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_AGENCY_REPORTS;

interface

uses
  SFrameEntry, SDataStructure, StdCtrls, wwdblook,  ExtCtrls, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ComCtrls, DBCtrls, Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_AGENCY_REPORTS = class(TFrameEntry)
    lablReport: TevLabel;
    pctlMain: TevPageControl;
    tshtBrowse: TTabSheet;
    panlMain: TevPanel;
    spltMain: TevSplitter;
    wwdgAgency: TevDBGrid;
    wwdgAgency_Report: TevDBGrid;
    tshtAgency_Reports: TTabSheet;
    Label1: TevLabel;
    DBText1: TevDBText;
    wwlcDescription: TevDBLookupCombo;
    Bevel1: TEvBevel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_AGENCY_REPORTS: TEDIT_SB_AGENCY_REPORTS;

implementation

uses EvUtils;

{$R *.DFM}

function TEDIT_SB_AGENCY_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_AGENCY_REPORTS;
end;

function TEDIT_SB_AGENCY_REPORTS.GetInsertControl: TWinControl;
begin
  Result := wwlcDescription;
end;

procedure TEDIT_SB_AGENCY_REPORTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_SERVICE_BUREAU.SB_AGENCY, DM_SERVICE_BUREAU.SB_REPORTS, DM_SYSTEM_MISC.SY_REPORTS], SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SB_AGENCY_REPORTS);

end.
