object EDIT_SB_UTILS_FUI_CREDIT: TEDIT_SB_UTILS_FUI_CREDIT
  Left = 447
  Top = 160
  Width = 252
  Height = 368
  Caption = 'FUI Credit Adjustment'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel2: TevLabel
    Left = 8
    Top = 243
    Width = 22
    Height = 13
    Caption = 'Year'
  end
  object evLabel1: TevLabel
    Left = 8
    Top = 267
    Width = 23
    Height = 13
    Caption = 'Rate'
  end
  object Button12: TButton
    Left = 8
    Top = 299
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object Button13: TButton
    Left = 152
    Top = 299
    Width = 75
    Height = 25
    Caption = 'Done'
    ModalResult = 1
    TabOrder = 1
  end
  object edYear: TevEdit
    Left = 64
    Top = 240
    Width = 97
    Height = 21
    TabOrder = 2
    Text = '0'
  end
  object edRate: TevEdit
    Left = 64
    Top = 264
    Width = 97
    Height = 21
    TabOrder = 3
    Text = '0'
  end
  object grdFuiCredit: TevDBGrid
    Left = 8
    Top = 8
    Width = 217
    Height = 225
    DisableThemesInTitle = False
    Selected.Strings = (
      'STATE'#9'2'#9'State'#9#9
      'NAME'#9'20'#9'Name'#9#9)
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsFuiCredit
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 4
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object dsFuiCredit: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 192
    Top = 240
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 160
    Top = 248
  end
end
