// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_DELIVERY_COMPANY_SVCS;

interface

uses SFrameEntry, StdCtrls, Mask, wwdbedit,  Controls, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls, Classes, Db, Wwdatsrc,
  SDataStructure, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_DELIVERY_COMPANY_SVCS = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    lablDelivery_Company: TevLabel;
    dtxtDeliver_Company: TevDBText;
    dedtDescription: TevDBEdit;
    lablServices: TevLabel;
    lablReference_Fee: TevLabel;
    wwdeReference_Fee: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_DELIVERY_COMPANY_SVCS: TEDIT_SB_DELIVERY_COMPANY_SVCS;

implementation

uses EvUtils;

{$R *.DFM}

function TEDIT_SB_DELIVERY_COMPANY_SVCS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY_SVCS;
end;

function TEDIT_SB_DELIVERY_COMPANY_SVCS.GetInsertControl: TWinControl;
begin
  Result := dedtDESCRIPTION;
end;

procedure TEDIT_SB_DELIVERY_COMPANY_SVCS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SB_DELIVERY_COMPANY_SVCS);

end.
