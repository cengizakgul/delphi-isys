// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_DELIVERY_COMPANY;

interface

uses SFrameEntry, StdCtrls, DBCtrls,  Wwdotdot, Wwdbcomb,
  Mask, wwdbedit, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_DELIVERY_COMPANY = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    DBText1: TevDBText;
    tshtDelivery_Company_Detail: TTabSheet;
    lablName: TevLabel;
    dedtName: TevDBEdit;
    lablDelivery_Contact: TevLabel;
    dedtDelivery_Contact: TevDBEdit;
    lablPhone: TevLabel;
    lablPhone_Type: TevLabel;
    lablSupplies_Contact: TevLabel;
    dedtSupplies_Contact: TevDBEdit;
    lablPhone2: TevLabel;
    lablPhone_Type2: TevLabel;
    lablWeb_Site: TevLabel;
    dedtWeb_Site: TevDBEdit;
    lablNotes: TevLabel;
    wwcbPhone_Type: TevDBComboBox;
    wwcbPhone_Type2: TevDBComboBox;
    dmemNotes: TEvDBMemo;
    wwdePhone: TevDBEdit;
    wwdePhone2: TevDBEdit;
    bevSB_DELIVERY1: TEvBevel;
    bevSB_DELIVERY2: TEvBevel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_DELIVERY_COMPANY: TEDIT_SB_DELIVERY_COMPANY;

implementation

uses SDataStructure;

{$R *.DFM}

function TEDIT_SB_DELIVERY_COMPANY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY;
end;

function TEDIT_SB_DELIVERY_COMPANY.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

initialization
  RegisterClass(TEDIT_SB_DELIVERY_COMPANY);

end.
