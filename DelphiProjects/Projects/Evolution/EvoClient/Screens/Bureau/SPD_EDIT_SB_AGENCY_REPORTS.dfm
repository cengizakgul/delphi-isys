inherited EDIT_SB_AGENCY_REPORTS: TEDIT_SB_AGENCY_REPORTS
  object Label1: TevLabel [0]
    Left = 16
    Top = 16
    Width = 36
    Height = 13
    Caption = 'Agency'
  end
  object DBText1: TevDBText [1]
    Left = 64
    Top = 16
    Width = 545
    Height = 21
    DataField = 'NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pctlMain: TevPageControl [2]
    Left = 16
    Top = 40
    Width = 593
    Height = 416
    HelpContext = 34001
    ActivePage = tshtBrowse
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object panlMain: TevPanel
        Left = 0
        Top = 0
        Width = 585
        Height = 388
        Align = alClient
        BevelOuter = bvNone
        Caption = 'panlMain'
        TabOrder = 0
        object spltMain: TevSplitter
          Left = 281
          Top = 0
          Width = 3
          Height = 388
          Cursor = crHSplit
        end
        object wwdgAgency: TevDBGrid
          Left = 0
          Top = 0
          Width = 281
          Height = 388
          Selected.Strings = (
            'NAME'#9'40'#9'Name'
            'STATE'#9'2'#9'State'
            'AGENCY_TYPE'#9'1'#9'Agency Type')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = False
          IndicatorColor = icBlack
        end
        object wwdgAgency_Report: TevDBGrid
          Left = 284
          Top = 0
          Width = 301
          Height = 388
          Selected.Strings = (
            'Report Description Lookup'#9'40'#9'Description')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = False
          IndicatorColor = icBlack
        end
      end
    end
    object tshtAgency_Reports: TTabSheet
      Caption = 'Details'
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 585
        Height = 388
        Align = alClient
      end
      object lablReport: TevLabel
        Left = 16
        Top = 16
        Width = 39
        Height = 13
        Caption = '~Report'
        FocusControl = wwlcDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwlcDescription: TevDBLookupCombo
        Left = 16
        Top = 32
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description')
        DataField = 'SB_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_REPORTS.SB_REPORTS
        LookupField = 'SB_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = False
        ShowButton = True
        AllowClearKey = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_AGENCY.SB_AGENCY
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 276
    Top = 112
  end
end
