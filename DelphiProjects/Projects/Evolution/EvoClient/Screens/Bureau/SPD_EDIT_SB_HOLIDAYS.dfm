inherited EDIT_SB_HOLIDAYS: TEDIT_SB_HOLIDAYS
  Width = 562
  Height = 442
  object pctlEDIT_SB_HOLIDAYS: TevPageControl [0]
    Left = 16
    Top = 16
    Width = 601
    Height = 419
    HelpContext = 22002
    ActivePage = tshtBrowse
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object lablDate: TevLabel
        Left = 312
        Top = 312
        Width = 30
        Height = 13
        Caption = '~Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablName: TevLabel
        Left = 16
        Top = 312
        Width = 35
        Height = 13
        Caption = '~Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdgEDIT_SB_HOLIDAYS: TevDBGrid
        Left = 0
        Top = 0
        Width = 593
        Height = 305
        DisableThemesInTitle = False
        Selected.Strings = (
          'HOLIDAY_NAME'#9'40'#9'Name'
          'HOLIDAY_DATE'#9'10'#9'Date')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
      end
      object dedtName: TevDBEdit
        Left = 16
        Top = 328
        Width = 273
        Height = 21
        DataField = 'HOLIDAY_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object drgpUsed_By: TevDBRadioGroup
        Left = 432
        Top = 312
        Width = 145
        Height = 73
        Caption = '~Used By'
        DataField = 'USED_BY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Service Bureau'
          'Bank'
          'Both')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'S'
          'B'
          'A')
      end
      object wwdpHoliday_Date: TevDBDateTimePicker
        Left = 312
        Top = 328
        Width = 97
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'HOLIDAY_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 2
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
end
