inherited EDIT_SB_ACA_GROUP: TEDIT_SB_ACA_GROUP
  Width = 727
  Height = 508
  object pctlEDIT_ACA_GROUP: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 696
    Height = 484
    HelpContext = 20502
    ActivePage = tshtBrowse
    TabOrder = 0
    OnChange = pctlEDIT_ACA_GROUPChange
    OnChanging = pctlEDIT_ACA_GROUPChanging
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_AGENCY: TevDBGrid
        Left = 0
        Top = 0
        Width = 688
        Height = 456
        DisableThemesInTitle = False
        Selected.Strings = (
          'ACA_GROUP_DESCRIPTION'#9'40'#9'ACA ALE Group Description'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_ACA_GROUP\wwdgEDIT_SB_AGENCY'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tbDetail: TTabSheet
      Caption = 'Details'
      object evGroupBox1: TevGroupBox
        Left = 0
        Top = 0
        Width = 688
        Height = 456
        Align = alClient
        Caption = ' ALE Group '
        Color = clBtnFace
        ParentColor = False
        TabOrder = 0
        object evLabel3: TevLabel
          Left = 10
          Top = 135
          Width = 177
          Height = 13
          Caption = 'Companies that belong to this Group  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel4: TevLabel
          Left = 10
          Top = 109
          Width = 73
          Height = 13
          Caption = 'Main Company '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablName: TevLabel
          Left = 10
          Top = 85
          Width = 55
          Height = 13
          Caption = 'Main Client '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel2: TevLabel
          Left = 10
          Top = 25
          Width = 135
          Height = 13
          Caption = 'ACA ALE Group Description '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object grdAcaCoList: TevDBGrid
          Left = 13
          Top = 158
          Width = 655
          Height = 280
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company Number'#9'F'
            'CO_NAME'#9'75'#9'Company Name'#9'F'
            'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
            'CL_NAME'#9'40'#9'Client Name'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SB_ACA_GROUP\grdAcaCoList'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsCoAcaList
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object edtDesc: TevDBEdit
          Left = 10
          Top = 48
          Width = 431
          Height = 21
          DataField = 'ACA_GROUP_DESCRIPTION'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object cbMainClient: TevDBLookupCombo
          Left = 100
          Top = 77
          Width = 341
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'Name'#9#9
            'CUSTOM_CLIENT_NUMBER'#9'20'#9'Custom client number'#9#9)
          DataField = 'PRIMARY_CL_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_TMP_CL.TMP_CL
          LookupField = 'CL_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = cbMainClientChange
          OnCloseUp = cbMainClientCloseUp
        end
        object cbMainCompany: TevDBLookupCombo
          Left = 100
          Top = 101
          Width = 341
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'75'#9'Name'#9'F'
            'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom company number'#9'F'
            'CUSTOM_CLIENT_NUMBER'#9'20'#9'CUSTOM_CLIENT_NUMBER'#9'F')
          DataField = 'PRIMARY_CO_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_TMP_CO.TMP_CO
          LookupField = 'CO_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
          OnBeforeDropDown = cbMainCompanyBeforeDropDown
        end
      end
    end
    object tbOther: TTabSheet
      Caption = 'Other Companies'
      ImageIndex = 2
      object evLabel1: TevLabel
        Left = 10
        Top = 265
        Width = 75
        Height = 13
        Caption = 'Company Name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel5: TevLabel
        Left = 10
        Top = 313
        Width = 18
        Height = 13
        Caption = 'EIN'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel6: TevLabel
        Left = 210
        Top = 313
        Width = 77
        Height = 13
        Caption = 'Number of FTEs'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object grdOther: TevDBGrid
        Left = 10
        Top = 14
        Width = 539
        Height = 239
        DisableThemesInTitle = False
        Selected.Strings = (
          'ALE_COMPANY_NAME'#9'40'#9'ALE Company Name'#9'F'
          'ALE_EIN_NUMBER'#9'10'#9'EIN Number'#9'F'
          'ALE_MAIN_COMPANY'#9'1'#9'Main Company'#9'F'
          'ALE_NUMBER_OF_FTES'#9'10'#9'Number of FTEs'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_ACA_GROUP\grdOther'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsOther
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object edCompanyName: TevDBEdit
        Left = 10
        Top = 288
        Width = 307
        Height = 21
        DataField = 'ALE_COMPANY_NAME'
        DataSource = dsOther
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edEIN: TevDBEdit
        Left = 10
        Top = 330
        Width = 101
        Height = 21
        DataField = 'ALE_EIN_NUMBER'
        DataSource = dsOther
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object rgMainCompany: TevDBRadioGroup
        Left = 8
        Top = 360
        Width = 185
        Height = 39
        Caption = '~Main Company'
        Columns = 2
        DataField = 'ALE_MAIN_COMPANY'
        DataSource = dsOther
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      inline navOthers: TMiniNavigationFrame
        Left = 8
        Top = 418
        Width = 229
        Height = 25
        TabOrder = 5
        inherited SpeedButton1: TevSpeedButton
          Left = 3
          Width = 80
          Caption = 'Create'
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
            CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
            E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
            7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
            82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
            9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
            55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
            5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
            CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
            0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
            76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
            FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
            16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
            8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
            FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
            38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
            9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
            FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
            51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
            9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
            66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
            9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
            76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
            9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
            FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
            59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
            9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
            FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
            0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
            87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
            FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
            55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
            556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
            B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
            88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
            E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
            78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        end
        inherited SpeedButton2: TevSpeedButton
          Left = 140
          Width = 80
          Caption = 'Delete'
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
            CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
            6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
            A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
            CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
            A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
            2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
            C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
            9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
            5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
            F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
            9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
            576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
            FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
            97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
            4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
            F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
            8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
            3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
            F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
            334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
            F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
            2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
            EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
            3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
            EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
            84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
            8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
            F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
            8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
            9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
            F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
            8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
            2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
            C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
            807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
            C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
            EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
            CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
            6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        end
        inherited evActionList1: TevActionList
          Left = 88
          Top = 16
        end
      end
      object edNumberofFTE: TevDBEdit
        Left = 210
        Top = 330
        Width = 107
        Height = 21
        DataField = 'ALE_NUMBER_OF_FTES'
        DataSource = dsOther
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_ACA_GROUP.SB_ACA_GROUP
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = nil
    Left = 294
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 424
    Top = 16
  end
  object dsCoAcaList: TevDataSource
    DataSet = cdAcaCoList
    Left = 400
    Top = 248
  end
  object cdAcaCoList: TevClientDataSet
    Left = 400
    Top = 208
    object cdAcaCoListCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdAcaCoListCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdAcaCoListCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdAcaCoListCO_NAME: TStringField
      DisplayWidth = 75
      FieldName = 'CO_NAME'
      Size = 75
    end
    object cdAcaCoListCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdAcaCoListCL_NAME: TStringField
      FieldName = 'CL_NAME'
      Size = 40
    end
    object cdAcaCoListSB_ACA_GROUP_NBR: TIntegerField
      FieldName = 'SB_ACA_GROUP_NBR'
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 512
    Top = 40
  end
  object dsOther: TevDataSource
    DataSet = DM_SB_ACA_GROUP_ADD_MEMBERS.SB_ACA_GROUP_ADD_MEMBERS
    OnStateChange = dsOtherStateChange
    MasterDataSource = wwdsDetail
    Left = 512
    Top = 280
  end
end
