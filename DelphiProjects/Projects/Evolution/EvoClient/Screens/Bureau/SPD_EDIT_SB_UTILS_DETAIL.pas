// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_UTILS_DETAIL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFieldCodeValues, Mask, wwdbedit, Wwdotdot, Wwdbcomb, 
  wwdbdatetimepicker, StdCtrls, ComCtrls, EvUtils, SDataStructure,
  ISBasicClasses, Spin, ExtCtrls, evConsts, EvUIUtils, EvUIComponents,
  isUIwwDBComboBox, isUIwwDBDateTimePicker, isUIEdit;
  
type
  TEDIT_SB_UTILS_DETAIL = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    evGroupBox1: TevGroupBox;
    evlbState: TevLabel;
    evlbDepFreq: TevLabel;
    evlbBDate: TevLabel;
    evlbEDate: TevLabel;
    evlbACHDate: TevLabel;
    evlbKeySuffix: TevLabel;
    evLabel7: TevLabel;
    evedState: TevEdit;
    evdtBDate: TevDBDateTimePicker;
    evdtEDate: TevDBDateTimePicker;  
    evdtACHDate: TevDBDateTimePicker;
    evedKeySuffix: TevEdit;
    evdcDepFreq: TevDBComboBox;
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    cbAndSui: TevCheckBox;
    TabSheet3: TTabSheet;
    Label2: TLabel;
    evcbAdjustmentCheck: TevDBComboBox;
    Button5: TButton;
    Button6: TButton;
    evlbNbrMonths: TevLabel;
    evseCycles: TevSpinEdit;
    tsErrorLog: TTabSheet;
    evreLog: TevRichEdit;
    Button9: TButton;
    Button10: TButton;
    OpenDialog1: TOpenDialog;
    Button11: TButton;
    Label6: TLabel;
    evcbAdjustmentCallIn: TevDBComboBox;
    Label7: TLabel;
    evcbAdjustmentDelivery: TevDBComboBox;
    tsNFSCreditHold: TTabSheet;
    evrgCreditHold: TevRadioGroup;
    evLabel4: TevLabel;
    evdtCheckDate: TevDateTimePicker;
    evLabel6: TevLabel;
    evcbCreditHold: TevDBComboBox;
    evLabel8: TevLabel;
    evcbLiabStatus: TevDBComboBox;
    Button12: TButton;
    Button13: TButton;
    procedure TabSheet1Show(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure evrgCreditHoldClick(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    State, BDate, EDate, FreqType, ACHKey: String;
    procedure TurnTabsOff;
  end;

var
  EDIT_SB_UTILS_DETAIL: TEDIT_SB_UTILS_DETAIL;

implementation

{$R *.DFM}

procedure TEDIT_SB_UTILS_DETAIL.TabSheet1Show(Sender: TObject);
begin
  evdcDepFreq.Items.Text := LocalDepFrequencyType_ComboChoices;
end;

procedure TEDIT_SB_UTILS_DETAIL.TurnTabsOff;
var
  I: Integer;
begin
  for I := 0 to PageControl1.PageCount - 1 do
    PageControl1.Pages[I].TabVisible := False;
end;

procedure TEDIT_SB_UTILS_DETAIL.Button1Click(Sender: TObject);
begin
   if (evedState.Text <> '') and (evdcDepFreq.Value <> '') and (evdtBDate.Text <> '') and(evdtEDate.Text <> '') and (evedKeySuffix.Text <> '') then
     if (evdtACHDate.Text <> '') or (CheckBox1.Checked) then
       ModalResult := mrOk
     else
       EvMessage('All the fields must have a value! If you wish to cancel please click the cancel button')
   else
     EvMessage('All the fields must have a value! If you wish to cancel please click the cancel button');
end;



procedure TEDIT_SB_UTILS_DETAIL.Button11Click(Sender: TObject);
var
  FName: String;
begin
  if OpenDialog1.Execute then
  begin
    FName := OpenDialog1.FileName;
    evreLog.Lines.SaveToFile(FName);
  end;
end;

procedure TEDIT_SB_UTILS_DETAIL.evrgCreditHoldClick(Sender: TObject);
begin
  evcbCreditHold.Enabled := (evrgCreditHold.ItemIndex = 0);
  if not evcbCreditHold.Enabled then
    evcbCreditHold.Value := CREDIT_HOLD_LEVEL_NONE
  else
    evcbCreditHold.Value := CREDIT_HOLD_LEVEL_LOW;
  evcbLiabStatus.Enabled := (evrgCreditHold.ItemIndex = 1);
  if not evcbLiabStatus.Enabled then
    evcbLiabStatus.Value := TAX_DEPOSIT_STATUS_NSF
  else
    evcbLiabStatus.Value := TAX_DEPOSIT_STATUS_IMPOUNDED;
end;

end.
