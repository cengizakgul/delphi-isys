inherited EDIT_SB_UTILS: TEDIT_SB_UTILS
  Width = 721
  Height = 551
  object Panel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 721
    Height = 551
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object PageControl1: TevPageControl
      Left = 1
      Top = 1
      Width = 719
      Height = 549
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Cleanup Utils'
        object sbUtils: TScrollBox
          Left = 0
          Top = 0
          Width = 711
          Height = 521
          VertScrollBar.Position = 198
          Align = alClient
          TabOrder = 0
          object pnlFixedSize: TevPanel
            Left = 0
            Top = -198
            Width = 876
            Height = 699
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter1: TevSplitter
              Left = 297
              Top = 0
              Width = 2
              Height = 699
            end
            object Panel2: TevPanel
              Left = 0
              Top = 0
              Width = 297
              Height = 699
              Align = alLeft
              Caption = 'Panel1'
              TabOrder = 0
              object wwgdClient: TevDBGrid
                Left = 1
                Top = 1
                Width = 295
                Height = 697
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_SB_UTILS\wwgdClient'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsClient
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                PopupMenu = PopupMenu1
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                DefaultSort = 'CUSTOM_COMPANY_NUMBER'
                NoFire = False
              end
            end
            object Panel3: TevPanel
              Left = 299
              Top = 0
              Width = 577
              Height = 699
              Align = alClient
              TabOrder = 1
              object btnACHKeyUtility: TevBitBtn
                Left = 12
                Top = 44
                Width = 180
                Height = 30
                Caption = 'ACH Key Utility'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnClick = btnACHKeyUtilityClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD888DDDDDDD
                  DDDDDDDDDDDDDDDDDDDDD8000800000000DDDDFFFDDDDDDDDDDD80BBB07FFFFF
                  F00DDF888F8888888DDD3B333B0FFFFFF0CDD8DDD8F888888D8D3B003B0FFAF9
                  F0CDD8FFF8F88D8D8D8D3BBBBB0FFFFFF0CDD88888F888888D8DD3BBB0000888
                  8DDDDD888F888DDDDDDDDD3B07FFFFF8DDDDDDD8F888888DDDDDDD3B0EEEE6F8
                  0DDDDDD8FDDDDD8DDDDDDD3B0EEEE6F8C0DDDDD8FDDDDD8D8DDDD03B0EEEE6F0
                  CC0DDDD8FDDDDD8D88DDDC3B000666FCCCC0D8D8FFFDDD8D888DDC3BBB07FFFC
                  CCCCD8D888F8888D8888DC3BBB0CCCCCCCCDD8D888FDDDD8888DDD3B033DDDDD
                  CCDDDDD8FDDDDDDD88DDDDD38DDDDDDDCDDDDDDDDDDDDDDD8DDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnCreateZeroAch: TevBitBtn
                Left = 12
                Top = 80
                Width = 180
                Height = 30
                Caption = 'Create Zero ACH File'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                OnClick = btnCreateZeroAchClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD88888888
                  8888DDDD888888888888DDDD8FFFFFFFFFF8DDDD8DDDDDDDDDD8DDDD8FFF6667
                  FFF8DDDD8DDD888DDDD8DDDD8FF67FF67FF8DDDD8DD8DDD8DDD8DDDD8FFFFFF6
                  7FF8DDDD8DDDDDD8DDD8DDDD8FFF6667FFF8DDDD8DDD888DDDD8DDDD8FF67FF6
                  7FF8DDDD8DD8DDD8DDD8DDDD8FF67FF67FF8DDDD8DD8DDD8DDD8DDDFCFFF6667
                  FFF8DDD8FDDD888DDDD8DDDFCF767FFFFFF8DDD8FDD8DDDDDDD8FCDFC7FC7FF6
                  7FF88FD8FD8FDDD8DDD8DFCD7FCF6667FFF8D8FDD8FD888DDDD8DDDD777FFFFF
                  FFF8DDDDDDDDDDDDDDD8DFCFCFC888888888D8F8F8F888888888FCDFCDFCDDDD
                  DDDD8FD8FD8FDDDDDDDDDDDFCDDDDDDDDDDDDDD8FDDDDDDDDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnQECCleanup: TevBitBtn
                Left = 12
                Top = 8
                Width = 180
                Height = 30
                Caption = 'Q/E Cleanup'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnClick = btnQECCleanupClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD808008D
                  DDDDDDDDDDF8FFDDDDDDDDDD00788808DDDDDDDDFF8888FDDDDDDDD077878880
                  DDDDDDDF8888888FDDDDDD0788787880DDDDDDF88888888FDDDDD87777777780
                  8DDDDD888888888FDDDDDD887777776608DDDDDD888888DDFDDDD82222277678
                  808DDD88DDD88D888FDDD2AAAAA22877780DD8DDDDDDD88888FD82AAAAAAA770
                  608DD8DDDDDDD88D8FDD2AAAAAAAA270660D8DDDDDDDDD8D88FD111111AAAA28
                  E608888888DDDDDD88FD199991AAAA288E60888888DDDD8DD88FD19991AAA28D
                  DE66D88888DDD8DDD888D19991AAA28DDDE6D88888DDD8DDDD88DD1191A228DD
                  DD88DD8888D88DDDDDDDDDDD1128DDDDDDDDDDDD888DDDDDDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnAutoCreatePayroll: TevBitBtn
                Left = 12
                Top = 116
                Width = 180
                Height = 30
                Caption = 'Auto-Create Payrolls'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                OnClick = btnAutoCreatePayrollClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D0DDAAA0DDDD
                  DDDDDDDD888DDDDDDDDDDA0AAAAA0DDDDDDDD8D88888DDDDDDDDDAAAA0AAA0DD
                  DDDDD8888D888DDDDDDDDAAA0DDA0DDDDDDDD888DDD8DDDDDDDDDAAAA088888D
                  DDDDD8888DDDDDDDDDDD888888AAAA08888D8DDDDD8888D8888D8F7A0F7AAA0F
                  FF8D8DD8DDD888DDDD8D87AAA0AAAA07FF8D8D888D8888DDDD8D8F7AAAAA0A0F
                  CF8D8DD88888D8D8FD8D8FF7AAA0777FC78D8DDD888DDDD8FDDD8FFF7777FC7F
                  C7FC8DDDDDDD8FD8FD8F8FFFFFFF7FC77FCD8DDDDDDDD8FDD8FD8FFFFFFF7777
                  777D8DDDDDDDDDDDDDDD888888888FCFCFCD88888888D8F8F8FDDDDDDDDDFCDF
                  CDFCDDDDDDDD8FD8FD8FDDDDDDDDDDDFCDDDDDDDDDDDDDD8FDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnLockQuarterlies: TevBitBtn
                Left = 12
                Top = 152
                Width = 180
                Height = 30
                Caption = 'Lock Quarterlies'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                OnClick = btnLockQuarterliesClick
                Color = clBlack
                Glyph.Data = {
                  F6060000424DF606000000000000360000002800000018000000180000000100
                  180000000000C006000000000000000000000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6
                  F61A1A1A00000000000000000000000000000000000000000000000000000000
                  0000000000000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF626262919191ACACACAAAAAAAAAAAAAAAAAAA7A7A7A4A4A4A6A6
                  A6AAAAAAAAAAAAAAAAAAADADAD9898980D0D0DFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF686868CCCCCCC3C3C3C3C3C3C3C3C3C0C0C0
                  ECECECFFFFFFEDEDEDBEBEBEC3C3C3C3C3C3C9C9C99393931E1E1EFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF686868C6C6C6BFBFBFBF
                  BFBFBFBFBFCACACA4545453A3A3AFFFFFFB9B9B9BFBFBFBFBFBFC5C5C5929292
                  1E1E1EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6868
                  68C6C6C6BFBFBFBFBFBFBFBFBFCFCFCF575757494949FFFFFFB6B6B6BFBFBFBF
                  BFBFC5C5C59292921E1E1EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF686868C6C6C6BFBFBFBFBFBFC3C3C39C9C9C3434348484844E4E
                  4EEAEAEABCBCBCBFBFBFC5C5C59292921E1E1EFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF686868C6C6C6BFBFBFBFBFBFC3C3C39C9C9C
                  3131315F5F5F434343EBEBEBBCBCBCBFBFBFC5C5C59292921E1E1EFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF686868C6C6C6BFBFBFBF
                  BFBFBFBFBFC9C9C9929292414141D6D6D6BCBCBCBFBFBFBFBFBFC5C5C5929292
                  1E1E1EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6868
                  68C8C8C8B9B9B9B9B9B9B9B9B9B9B9B9BEBEBEC4C4C4B9B9B9B9B9B9B9B9B9B9
                  B9B9BFBFBF8B8B8B1E1E1EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF626262BEBEBEFFFFFFFFFFFFFFFFFFFEFEFEFBFBFBFBFBFBFBFB
                  FBFDFDFDFFFFFFFFFFFFFFFFFFE9E9E90A0A0AFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B14C4C4CE4E4E4676767393939AFAFAF
                  BCBCBCB9B9B9BBBBBBAEAEAE4F4F4F575757BDBDBD6A6A6A989898FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000DDDDDDA4
                  A4A4A6A6A6262626D7D7D7CECECECFCFCFA0A0A0BEBEBE696969797979000000
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFF0000007C7C7CF4F4F40303036262620E0E0E393939313131D4D4D49A
                  9A9A000000ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFD0D0D06E6E6EEAEAEA191919FFFFFFFFFFFFFFFF
                  FF6F6F6FCCCCCC8A8A8A7A7A7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBEBE6E6E6EE9E9E90F0F0F
                  FFFFFFFFFFFFFFFFFF575757CECECE8C8C8C6E6E6EFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBC69
                  6969F3F3F36565655B5B5BD3D3D37575756F6F6FD5D5D5808080676767FFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFE0E0E0444444E8E8E8FBFBFB858585383838646464E0E0E0BBBBBB20
                  2020C5C5C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3D3D31A1A1A3D3D3DFFFFFFFFFFFFFFFF
                  FF717171000000E8E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9
                  0505051111110909097C7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                Margin = 0
              end
              object btnReplaceTaxDueDate: TevBitBtn
                Left = 12
                Top = 188
                Width = 180
                Height = 30
                Caption = 'Replace Tax Due Date'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                OnClick = btnReplaceTaxDueDateClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDD44444444444444DD88888888888888DD4777777777
                  7774DD8DDDDDDDDDDDD8DD47174474474474DD8D8D88D88D88D8DD477777EEEE
                  7774DD8DDDDD8888DDD8DD471744E66E4474DD8D8D8D8DD8D8D8DD477777EEEE
                  7774DD8DDDDD8888DDD8DD47174474474474DDDDDDDDDDDD88D8880888888877
                  7774FFFFFFFFFFDDDDD86666666660444444888888888FD888886FFFFFFF6044
                  4444888888888FD888886F6FFF6F6044444488D888D88FD888886FF6F6FF68DD
                  DDDD888D8D888FDDDDDD6F6F6F6F68DDDDDD88D8D8D88FDDDDDD6FFFFFFF68DD
                  DDDD888888888FDDDDDD6666666668DDDDDD888888888FDDDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnSetW2PaperType: TevBitBtn
                Left = 12
                Top = 224
                Width = 180
                Height = 30
                Caption = 'Set W-2 Paper Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                OnClick = btnSetW2PaperTypeClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD888888888
                  8888DDD8888888888888DDD877777777FFF8FFFFFFFFFFFFFDD8888888888888
                  7FF8888888888888FDD88FFFFFFFFFF87FF88DDDDDDDDDD8FDD88FFFFFFFFFF8
                  7FF88DDDDDDDDDD8FDD88FFFFFFFFFF87FF88DDDDDDDDDD8FDD88FFFFFF77778
                  7FF88DDDDDDFFFF8FDD88FFFFFF888887FF88DDDDDD88888FDD88FFFFFF8FFF7
                  FFF88DDDDDD8DDD8DDD88FFFFFF8FF7FFFF88DDDDDD8DD8DDDD88FFFFFF8F7F7
                  77788DDDDDD8D8DDDDD8888888887FF88888888888888DD88888DDD8FFFFFFF8
                  FFF7DDDDDDDDDDD8DDD8DDD8FFFFFFF8FF7DDDD8DDDDDDD8DD8DDDD8FFFFFFF8
                  F7DDDDD8DDDDDDD8D8DDDDD8888888887DDDDDD8888888888DDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnChangeSuiRates: TevBitBtn
                Left = 12
                Top = 260
                Width = 180
                Height = 30
                Caption = 'Change SUI Rates'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                OnClick = btnChangeSuiRatesClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD66666666
                  6666DDDD888888888888DDDDD6EE6EE6EE6DDDDDD8FD8FD8FD8FFF2A26EE6EE6
                  EE6D8DD8D8FD8FD8FD8FFF2A26EE6EE6EE6D8DD8D8FD8FD8FD8FFF2A26EE6EE6
                  EE6D8DD8D8FD8FD8FD8FFF2A76EE6EE6EE6D8DD8D8FF8FD8FD8FFF2A76666EE6
                  EE6D8DD8D8888FD8FD8FDFAA77726EE6EE6DD8D8DDDD8FD8FD8FDDDDBB3D6EE6
                  EE6DDD88888D8FF8FD8FDDDDB3336666EE6DDDDDD88D8888FD8FDDDDBB33DDD6
                  EE6DDDDDDD88DDD8FF8FDDDDBBB3DDD6666DDDDDDDD8DDD8888DDDDDB333DDDD
                  DDDDDDDDD888DDDDDDDDDDAABBBBDDDDDDDDDD88DDDDDDDDDDDDDDDDAAA2DDDD
                  DDDDDDDD8880DDDDDDDDDDDDAAA2DDDDDDDDDDDD8880DDDDDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnExtendCalendar: TevBitBtn
                Left = 202
                Top = 8
                Width = 180
                Height = 30
                Caption = 'Extend Calendar'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                OnClick = btnExtendCalendarClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD4444444444
                  4444DD88888888888888D887777777777774DFFFFFFFFFFFFFF8444444444444
                  447488888888888888F84F7FFFFFFFFFF4748DDDDDDDDDDDD8F84F1F44F44F44
                  F4748D8D88D88D88D8F84F7FFFFFFFFFF4748DDDDDDDDDDDD8F84F1F44F44F44
                  F4748D8D88D88D88D8F84F7FFFFFFFFFF4748DDDDDDDDDDDD8F84F1F44F44F44
                  F4748D8D88D88D88D8F84F7FFFFFFFFFF4748DDDDDDDDDDDD8F84F1744744744
                  74848D8D88D88D88D8F84F777777777774848DDDDDDDDDDDD8F8444444444444
                  448488888888888888F8466666666666648488888888888888F8466666666666
                  648D88888888888888FD44444444444444DD88888888888888DD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnUpdateFUTA: TevBitBtn
                Left = 202
                Top = 44
                Width = 180
                Height = 30
                Caption = 'Upd FUTA Cr Override'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                OnClick = btnUpdateFUTAClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD888DDDDDDDDDDDDFFFFDDDDDDDDDD88
                  0111DDDDDDDDDFFF8888DDDDDDDDD0000999DDDDDDDDF8888DDD88DDD8820777
                  0FFFDDDDDDDF8DDD8888AA8DDAA07FF7099988DDD888DDDD8DDDDAA8DA80FFF8
                  0CCCD88DD8D8DDD88888DDAA8AA0F0700444DD88D888D8D88888888AA8D0F0F0
                  8DDDDDD88DD8D8D8DDDDAAADAA80F070DDDD888D88D8D8D8DDDDA8ADDAA20770
                  DDDD8D8DD88D8DD8DDDDAAADDDAA0F08DDDD888DDD888D8DDDDDDDDDDDDD0F0D
                  DDDDDDDDDDDD8D8DDDDDDDDDDDDD0F0DDDDDDDDDDDDD8D8DDDDDDDDDDDDDD0DD
                  DDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnFUICredit: TevBitBtn
                Left = 202
                Top = 80
                Width = 180
                Height = 30
                Caption = 'FUI CreditAdjustment 2015'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 10
                OnClick = btnFUICreditClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD22DDD
                  2222DDDDDDDFFDDDFFFFDDDD133AA2DDAAA2DDDD83D88FDD888FDDD99933AA2D
                  A2A2DDD8888D88FD8F8FDD9999933AA2AAA2DD888888D88F888FDD99992222AA
                  2DDDDD8888DFFF88FDDDD9B999AAAA2AA2DDD88888D888F88FDDD9F999A22A2D
                  AA2DD8D888D8F8FD88FD9FB999AAAA21DAA28D8888D888FDD88F9BFB99999911
                  DDDD88D888DDDDD8DDDD9FBF99999911DDDD8D8D88888888DDDD9BFBF9999911
                  DDDD88D8D888888DDDDDD99FBFB9911AAA2DD88D8D888DD888DDDDD999911AAA
                  AAA2DDD88888D888888DDDDDD2DDAAAAA2DDDDDDD8DD88888DDDDDDDDDAAAAAA
                  2DDDDDDDDD888888DDDDDDDDDDDAAAA2DDDDDDDDDDD8888DDDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object btnRelinkMoved: TevBitBtn
                Left = 202
                Top = 116
                Width = 180
                Height = 30
                Caption = 'Relink Moved Client'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 11
                OnClick = btnRelinkMovedClick
                Color = clBlack
                Margin = 0
              end
              object bbtnPrintAdjReps: TevBitBtn
                Left = 202
                Top = 152
                Width = 180
                Height = 30
                Caption = 'Print Combined Adj. Reports'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 12
                OnClick = bbtnPrintAdjRepsClick
                Color = clBlack
                Margin = 0
              end
              object btnChangeEEActiveStatus: TevBitBtn
                Left = 12
                Top = 296
                Width = 180
                Height = 30
                Caption = 'Change EE Active Status'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 13
                OnClick = btnChangeEEActiveStatusClick
                Color = clBlack
                Margin = 0
              end
              object btnChangeSBBank: TevBitBtn
                Left = 202
                Top = 188
                Width = 180
                Height = 30
                Caption = 'Change SB Bank Accounts'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 14
                OnClick = btnChangeSBBankClick
                Color = clBlack
                Margin = 0
              end
              object bbtnImportSUIRates: TevBitBtn
                Left = 202
                Top = 296
                Width = 180
                Height = 30
                Caption = 'Import SUI Rates'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 15
                OnClick = bbtnImportSUIRatesClick
                Color = clBlack
                Margin = 0
              end
              object btnApplySUI: TevBitBtn
                Left = 202
                Top = 224
                Width = 180
                Height = 30
                Caption = 'Apply SUI'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 16
                OnClick = btnApplySUIClick
                Color = clBlack
                Margin = 0
              end
              object btnTaxableWagesAdjustment: TevBitBtn
                Left = 202
                Top = 260
                Width = 180
                Height = 30
                Caption = 'Taxable Wages Adjustment'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 17
                OnClick = btnTaxableWagesAdjustmentClick
                Color = clBlack
                Margin = 0
              end
              object btnCreditHold: TevBitBtn
                Left = 12
                Top = 332
                Width = 180
                Height = 30
                Caption = 'Credit Hold and NSF'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 18
                OnClick = btnCreditHoldClick
                Color = clBlack
                Margin = 0
              end
              object btnHSACleanup: TevBitBtn
                Left = 202
                Top = 332
                Width = 180
                Height = 30
                Caption = 'HSA Cleanup'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 19
                OnClick = btnHSACleanupClick
                Color = clBlack
                Margin = 0
              end
              object btnIgnoreFICA: TevBitBtn
                Left = 12
                Top = 368
                Width = 180
                Height = 30
                Caption = 'Update EE FICA Flag'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 20
                OnClick = btnIgnoreFICAClick
                Color = clBlack
                Margin = 0
              end
              object btnCleanUpLocals: TevBitBtn
                Left = 202
                Top = 368
                Width = 180
                Height = 30
                Caption = 'Clean Up Locals'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 21
                OnClick = btnCleanUpLocalsClick
                Color = clBlack
                Margin = 0
              end
              object btnMCTUpdate: TevBitBtn
                Left = 12
                Top = 404
                Width = 180
                Height = 30
                Caption = 'MCT Update'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 22
                OnClick = btnMCTUpdateClick
                Color = clBlack
                NumGlyphs = 2
                Margin = 0
              end
              object btnRecipBugFix: TevBitBtn
                Left = 202
                Top = 404
                Width = 180
                Height = 30
                Caption = 'Reciprocation Fix'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 23
                OnClick = btnRecipBugFixClick
                Color = clBlack
                Margin = 0
              end
              object btnChangeTaxPaymentMethod: TevBitBtn
                Left = 12
                Top = 440
                Width = 180
                Height = 30
                Caption = 'Change Tax Payment Method'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 24
                OnClick = btnChangeTaxPaymentMethodClick
                Color = clBlack
                NumGlyphs = 2
                Margin = 0
              end
              object btnChangeEeAcaFormsType: TevBitBtn
                Left = 202
                Top = 440
                Width = 180
                Height = 30
                Caption = 'Change EE ACA Forms Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 25
                OnClick = btnChangeEeAcaFormsTypeClick
                Color = clBlack
                NumGlyphs = 2
                Margin = 0
              end
              object btnACAOtherCO: TevBitBtn
                Left = 12
                Top = 475
                Width = 180
                Height = 30
                Caption = 'ACA Other CO'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 26
                OnClick = btnACAOtherCOClick
                Color = clBlack
                NumGlyphs = 2
                Margin = 0
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Quarter End Cleanup'
        ImageIndex = 1
        object evRichEdit1: TevRichEdit
          Left = 0
          Top = 0
          Width = 317
          Height = 359
          Lines.Strings = (
            '')
          TabOrder = 0
        end
        object btnPrint: TevBitBtn
          Left = 340
          Top = 322
          Width = 99
          Height = 37
          Caption = 'Print'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnPrintClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 152
    Top = 122
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
    Left = 256
    Top = 50
  end
  inherited wwdsList: TevDataSource
    Left = 66
    Top = 106
  end
  object wwdsClient: TevDataSource
    Left = 184
    Top = 192
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 53
    Top = 217
  end
  object PopupMenu1: TPopupMenu
    Left = 173
    Top = 281
    object LoadCompaniesfromTextFile1: TMenuItem
      Caption = 'Load Companies from Text File'
      OnClick = LoadCompaniesfromTextFile1Click
    end
  end
  object dTxtFileDialog: TOpenDialog
    Filter = 'Text file (*.txt)|*.txt'
    InitialDir = 'c:\Evolution'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 136
    Top = 24
  end
  object evcsHoliday: TevClientDataSet
    Left = 196
    Top = 372
  end
  object Calendar: TEvBasicClientDataSet
    Left = 264
    Top = 376
    object CalendarPAY_FREQUENCY: TStringField
      FieldName = 'PAY_FREQUENCY'
      Size = 2
    end
    object CalendarCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CalendarPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object CalendarPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object CalendarCALL_IN_DATE: TDateTimeField
      FieldName = 'CALL_IN_DATE'
    end
    object CalendarDELIVERY_DATE: TDateTimeField
      FieldName = 'DELIVERY_DATE'
    end
    object CalendarLAST_REAL_CHECK_DATE: TDateField
      FieldName = 'LAST_REAL_CHECK_DATE'
    end
    object CalendarLAST_REAL_CALL_IN_DATE: TDateTimeField
      FieldName = 'LAST_REAL_CALL_IN_DATE'
    end
    object CalendarLAST_REAL_DELIVERY_DATE: TDateTimeField
      FieldName = 'LAST_REAL_DELIVERY_DATE'
    end
    object CalendarDAYS_PRIOR: TIntegerField
      FieldName = 'DAYS_PRIOR'
    end
    object CalendarDAYS_POST: TIntegerField
      FieldName = 'DAYS_POST'
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 80
    Top = 48
  end
end
