object dlgSelectSBBankAccount: TdlgSelectSBBankAccount
  Left = 440
  Top = 300
  Width = 309
  Height = 270
  Caption = 'Select SB Bank Account'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    301
    243)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 24
    Top = 16
    Width = 143
    Height = 13
    Caption = 'Select New SB Bank Account'
  end
  object OKBtn: TButton
    Left = 63
    Top = 194
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 186
    Top = 194
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object evDBLookupCombo1: TevDBLookupCombo
    Left = 25
    Top = 44
    Width = 225
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
      'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
      'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
    LookupTable = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
    LookupField = 'SB_BANK_ACCOUNTS_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = evDBLookupCombo1Change
  end
  object evcbTRUST_ACCOUNT: TevCheckBox
    Left = 25
    Top = 116
    Width = 129
    Height = 17
    Caption = 'Trust Bank Account'
    Enabled = False
    TabOrder = 3
  end
  object evcbTAX_ACCOUNT: TevCheckBox
    Left = 25
    Top = 140
    Width = 120
    Height = 17
    Caption = 'Tax Bank Account'
    Enabled = False
    TabOrder = 4
  end
  object evcbACH_ACCOUNT: TevCheckBox
    Left = 25
    Top = 164
    Width = 124
    Height = 17
    Caption = 'ACH Bank Account'
    Enabled = False
    TabOrder = 5
  end
  object evcbOBC_ACCOUNT: TevCheckBox
    Left = 160
    Top = 164
    Width = 122
    Height = 17
    Caption = 'OBC Account'
    Enabled = False
    TabOrder = 6
  end
  object evcbWORKERS_COMP_ACCOUNT: TevCheckBox
    Left = 160
    Top = 140
    Width = 121
    Height = 17
    Caption = 'WC Bank Account'
    Enabled = False
    TabOrder = 7
  end
  object evcbBILLING_ACCOUNT: TevCheckBox
    Left = 160
    Top = 116
    Width = 124
    Height = 17
    Caption = 'Billing Bank Account'
    Enabled = False
    TabOrder = 8
  end
  object evcbSkipBlank: TevCheckBox
    Left = 25
    Top = 76
    Width = 163
    Height = 17
    Caption = 'Skip Blank Bank Accounts'
    TabOrder = 9
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 200
    Top = 8
  end
end
