// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_ACA_GROUP;

interface

uses SFrameEntry,  wwdbdatetimepicker, StdCtrls, ExtCtrls,
  DBCtrls, Mask, wwdbedit, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Db, Wwdatsrc, ISBasicClasses, wwdblook, Wwdotdot, Wwdbcomb,
  SDDClasses, SDataDictsystem, SDataDictbureau, SDataStructure, Buttons,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, evUtils, Forms, SPD_MiniNavigationFrame,
  SPackageEntry, EvConsts, SFieldCodeValues, EvExceptions, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBLookupCombo, SDataDicttemp, SysUtils, Variants, EvCommonInterfaces, EvDataset;

type
  TEDIT_SB_ACA_GROUP = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    dsCoAcaList: TevDataSource;
    cdAcaCoList: TevClientDataSet;
    DM_TEMPORARY: TDM_TEMPORARY;
    tshtBrowse: TTabSheet;
    wwdgEDIT_SB_AGENCY: TevDBGrid;
    tbDetail: TTabSheet;
    evGroupBox1: TevGroupBox;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    lablName: TevLabel;
    evLabel2: TevLabel;
    grdAcaCoList: TevDBGrid;
    edtDesc: TevDBEdit;
    cdAcaCoListCL_NBR: TIntegerField;
    cdAcaCoListCO_NBR: TIntegerField;
    cdAcaCoListCUSTOM_COMPANY_NUMBER: TStringField;
    cdAcaCoListCO_NAME: TStringField;
    cdAcaCoListCUSTOM_CLIENT_NUMBER: TStringField;
    cdAcaCoListCL_NAME: TStringField;
    cdAcaCoListSB_ACA_GROUP_NBR: TIntegerField;
    cbMainClient: TevDBLookupCombo;
    cbMainCompany: TevDBLookupCombo;
    tbOther: TTabSheet;
    grdOther: TevDBGrid;
    evLabel1: TevLabel;
    edCompanyName: TevDBEdit;
    dsOther: TevDataSource;
    evLabel5: TevLabel;
    edEIN: TevDBEdit;
    evLabel6: TevLabel;
    rgMainCompany: TevDBRadioGroup;
    navOthers: TMiniNavigationFrame;
    edNumberofFTE: TevDBEdit;
    pctlEDIT_ACA_GROUP: TevPageControl;
    procedure SetCoFilter;    
    procedure RefreshcdCoList;
    procedure SetSbAcaGroup;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbMainCompanyBeforeDropDown(Sender: TObject);
    procedure cbMainClientCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure cbMainClientChange(Sender: TObject);
    procedure navOthersSpeedButton1Click(Sender: TObject);
    procedure navOthersSpeedButton2Click(Sender: TObject);
    procedure dsOtherStateChange(Sender: TObject);
    procedure pctlEDIT_ACA_GROUPChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure pctlEDIT_ACA_GROUPChange(Sender: TObject);

  private
    { Private declarations }

    function CheckMainCompany(const withNbr: boolean=True):boolean;
  public
    { Public declarations }
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;

    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

var
  EDIT_SB_ACA_GROUP: TEDIT_SB_ACA_GROUP;

implementation

{$R *.DFM}

procedure TEDIT_SB_ACA_GROUP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_ACA_GROUP_ADD_MEMBERS, EditDataSets, 'ALL');
  AddDSWithCheck(DM_TEMPORARY.TMP_CL, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_TEMPORARY.TMP_CO, SupportDataSets, 'ALL');
end;


function TEDIT_SB_ACA_GROUP.CheckMainCompany(const withNbr: boolean=True):boolean;
var
   s, sNbr: string;
   Q: IEvQuery;
begin
  sNbr := '';
  if withNbr then
  sNbr := ' SB_ACA_GROUP_ADD_MEMBERS_NBR <> :Nbr and ';

  s:= 'select count(SB_ACA_GROUP_ADD_MEMBERS_NBR) from SB_ACA_GROUP_ADD_MEMBERS where {AsOfNow<SB_ACA_GROUP_ADD_MEMBERS>} and ' +
          sNbr + ' SB_ACA_GROUP_NBR = :AcaNbr and ALE_MAIN_COMPANY = ''Y'' ';
  Q := TevQuery.Create(s);
  if withNbr then
     Q.Params.AddValue('Nbr', dsOther.DataSet.FieldByName('SB_ACA_GROUP_ADD_MEMBERS_NBR').AsInteger);
  Q.Params.AddValue('AcaNbr', wwdsDetail.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger);
  Q.Execute;
  result := Q.Result.Fields[0].AsInteger > 0;
end;

procedure TEDIT_SB_ACA_GROUP.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

begin
  inherited;
  if Kind = NavOK then
    if pctlEDIT_ACA_GROUP.ActivePage = tbOther then
    begin
     if dsOther.DataSet.State = dsInsert then
        dsOther.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger :=
           wwdsDetail.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger;

     dsOther.DataSet.UpdateRecord;

     if (Trim(edEIN.Text) = '') then
          raise EUpdateError.CreateHelp('EIN Number can''t be empty', IDH_ConsistencyViolation);

     if (dsOther.DataSet.FieldByName('ALE_MAIN_COMPANY').AsString = GROUP_BOX_YES) and CheckMainCompany then
        raise EUpdateError.CreateHelp('Main company already selected.', IDH_ConsistencyViolation);
    end;

  if Kind = NavDelete then
    if (cdAcaCoList.RecordCount > 0) then
          raise EUpdateError.CreateHelp('This ALE Group has one or more companies attached and can not be deleted until they are removed.', IDH_ConsistencyViolation);
end;


procedure TEDIT_SB_ACA_GROUP.SetSbAcaGroup;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and cdAcaCoList.Active then
  begin
     cdAcaCoList.UserFilter := 'SB_ACA_GROUP_NBR = ' + IntToStr(wwdsDetail.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger);
     cdAcaCoList.UserFiltered := True;
  end;

  cbMainCompany.Enabled:= Not CheckMainCompany(false);
end;

function TEDIT_SB_ACA_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_ACA_GROUP;
end;


procedure TEDIT_SB_ACA_GROUP.RefreshcdCoList;
var
 s : String;
begin
   s :='  select a.CL_NBR, a.CO_NBR, a.CUSTOM_COMPANY_NUMBER, a.NAME CO_NAME, b.CUSTOM_CLIENT_NUMBER,b.NAME CL_NAME, a.SB_ACA_GROUP_NBR '#13 +
       '    from TMP_CO a, TMP_CL b'#13 +
       '    where a.SB_ACA_GROUP_NBR > 0 and'#13 +
       '          a.CL_NBR = b.CL_NBR'#13 +
       '    order by CL_NBR, CO_NBR';

   with TExecDSWrapper.Create(s) do
   begin
     cdAcaCoList.Close;
     ctx_DataAccess.GetTmpCustomData(cdAcaCoList, AsVariant);
   end;
   cdAcaCoList.LogChanges := False;
   SetSbAcaGroup;
end;


procedure TEDIT_SB_ACA_GROUP.Activate;
begin
  inherited;
  RefreshcdCoList;
  SetCoFilter;
  cbMainCompany.RefreshDisplay;

  navOthers.DataSource := dsOther;
  navOthers.InsertFocusControl := edCompanyName;
  navOthers.SpeedButton1.OnClick := navOthersSpeedButton1Click;
  navOthers.SpeedButton2.OnClick := navOthersSpeedButton2Click;
end;


procedure TEDIT_SB_ACA_GROUP.Deactivate;
begin
  inherited;
  DM_TEMPORARY.TMP_CO.UserFilter := '';
  DM_TEMPORARY.TMP_CO.UserFiltered := false;

end;

function TEDIT_SB_ACA_GROUP.GetInsertControl: TWinControl;
begin
  Result := edtDesc;
end;

procedure TEDIT_SB_ACA_GROUP.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if csLoading in ComponentState then
    Exit;

  SetSbAcaGroup;

end;

procedure TEDIT_SB_ACA_GROUP.SetCoFilter;
begin
  if DM_TEMPORARY.TMP_CO.Active then
  begin
    DM_TEMPORARY.TMP_CO.UserFilter := 'CL_NBR = ' + IntToStr(DM_TEMPORARY.TMP_CL.fieldByName('CL_NBR').AsInteger);
    DM_TEMPORARY.TMP_CO.UserFiltered := True;
    DM_TEMPORARY.TMP_CO.Locate('CO_NBR', wwdsDetail.DataSet.FieldByName('PRIMARY_CO_NBR').AsInteger, []);
    cbMainCompany.RefreshDisplay;
  end;
end;

procedure TEDIT_SB_ACA_GROUP.cbMainCompanyBeforeDropDown(Sender: TObject);
begin
  inherited;
  SetCoFilter;
end;

procedure TEDIT_SB_ACA_GROUP.cbMainClientCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
   if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
       wwdsDetail.DataSet.Edit;

   wwdsDetail.DataSet.FieldByName('PRIMARY_CO_NBR').Clear;
end;

procedure TEDIT_SB_ACA_GROUP.cbMainClientChange(Sender: TObject);
begin
  inherited;
  SetCoFilter;
end;

procedure TEDIT_SB_ACA_GROUP.navOthersSpeedButton1Click(Sender: TObject);
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_ACA_GROUP.State = dsBrowse then
     edCompanyName.SetFocus;

  navOthers.InsertRecordExecute(Sender);
  if dsOther.DataSet.State in [dsinsert] then
   dsOther.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger :=
          wwdsDetail.DataSet.FieldByName('SB_ACA_GROUP_NBR').AsInteger;
end;

procedure TEDIT_SB_ACA_GROUP.navOthersSpeedButton2Click(Sender: TObject);
begin
  inherited;
  navOthers.DeleteRecordExecute(Sender);

end;

procedure TEDIT_SB_ACA_GROUP.dsOtherStateChange(Sender: TObject);
begin
  inherited;
  grdOther.Enabled := not (dsOther.DataSet.State in [dsInsert,dsEdit]);
end;

procedure TEDIT_SB_ACA_GROUP.pctlEDIT_ACA_GROUPChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if pctlEDIT_ACA_GROUP.ActivePage = tbOther then
      AllowChange := dsOther.DataSet.State = dsBrowse
  else
  if pctlEDIT_ACA_GROUP.ActivePage = tbDetail then
      AllowChange := wwdsDetail.DataSet.State = dsBrowse;
end;

procedure TEDIT_SB_ACA_GROUP.pctlEDIT_ACA_GROUPChange(Sender: TObject);
  procedure SetMainCompanyEnabled;
  begin
    rgMainCompany.Enabled := False;
    if Trim(cbMainCompany.Value) = '' then
       rgMainCompany.Enabled := True;
  end;
begin
  inherited;
  if pctlEDIT_ACA_GROUP.ActivePage = tbDetail then
  begin
      cbMainCompany.Enabled:= Not CheckMainCompany(false);
      SetMainCompanyEnabled;
  end
  else
  if pctlEDIT_ACA_GROUP.ActivePage = tbOther then
      SetMainCompanyEnabled;
end;

initialization
  RegisterClass(TEDIT_SB_ACA_GROUP);

end.
