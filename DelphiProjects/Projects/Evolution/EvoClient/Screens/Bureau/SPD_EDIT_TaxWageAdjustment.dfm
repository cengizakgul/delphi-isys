object EDIT_APPLY_TAXABLEWAGES: TEDIT_APPLY_TAXABLEWAGES
  Left = 337
  Top = 235
  Width = 707
  Height = 423
  BorderIcons = []
  Caption = 'Apply Taxable Wages to Payrolls '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel5: TevLabel
    Left = 8
    Top = 36
    Width = 67
    Height = 13
    Caption = 'Check Period:'
  end
  object evLabel1: TevLabel
    Left = 8
    Top = 201
    Width = 46
    Height = 13
    Caption = 'Employee'
  end
  object Label1: TLabel
    Left = 205
    Top = 37
    Width = 7
    Height = 13
    Caption = '~'
  end
  object evLabel2: TevLabel
    Left = 8
    Top = 60
    Width = 78
    Height = 13
    Caption = 'Company Locals'
  end
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 48
    Height = 16
    Caption = 'Label2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 349
    Width = 397
    Height = 16
    Caption = 'This will update this local'#39's Taxable Wages for the period'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel3: TevLabel
    Left = 416
    Top = 35
    Width = 39
    Height = 13
    Caption = 'Amount:'
  end
  object edEndCheckDate: TevDateTimePicker
    Left = 216
    Top = 32
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 1
  end
  object btnUpdate: TevButton
    Left = 512
    Top = 345
    Width = 86
    Height = 25
    Caption = 'Update'
    TabOrder = 2
    OnClick = btnUpdateClick
    Color = clBlack
    ParentColor = False
    Margin = 0
  end
  object edBeginCheckDate: TevDateTimePicker
    Left = 80
    Top = 32
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 0
  end
  object grdColocals: TevDBGrid
    Left = 8
    Top = 76
    Width = 673
    Height = 121
    HelpContext = 1502
    DisableThemesInTitle = False
    Selected.Strings = (
      'LocalName'#9'40'#9'Local'#9'F'
      'localState'#9'2'#9'State'#9'F'
      'LocalTypeDesc'#9'30'#9'Local Type'#9'F'
      'LOCAL_ACTIVE'#9'1'#9'Active'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_APPLY_TAXABLEWAGES\grdColocals'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsCO_locals
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 3
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    OnSelectionChanged = grdColocalsSelectionChanged
    NoFire = False
  end
  object evButton1: TevButton
    Left = 606
    Top = 345
    Width = 75
    Height = 25
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 4
    Color = clBlack
    ParentColor = False
    Margin = 0
  end
  object grdEE: TevDBGrid
    Left = 8
    Top = 217
    Width = 673
    Height = 121
    HelpContext = 1502
    DisableThemesInTitle = False
    Selected.Strings = (
      'Trim_Number'#9'9'#9'Aligned EE Code'#9'F'
      'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
      'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
      'Employee_MI_Calculate'#9'2'#9'MI'#9'F'
      'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
      'CURRENT_TERMINATION_CODE_DESC'#9'20'#9'Status'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_APPLY_TAXABLEWAGES\grdEE'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsEE
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 5
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    OnSelectionChanged = grdColocalsSelectionChanged
    NoFire = False
  end
  object edAmount: TevEdit
    Left = 459
    Top = 32
    Width = 219
    Height = 21
    TabOrder = 6
    Text = '0.0'
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 280
    Top = 8
  end
  object dsEE: TevDataSource
    DataSet = DM_EE.EE
    Left = 112
    Top = 232
  end
  object dsCO_locals: TevDataSource
    DataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
    Left = 104
    Top = 88
  end
end
