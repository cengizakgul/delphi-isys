// Screens of "Bureau"
unit scr_Bureau;

interface

uses
  SBureauScr,
  SPD_EDIT_SB_ACCOUNTANT,
  SPD_EDIT_SB_AGENCY,
  SPD_EDIT_SB_BANKS,
  SPD_EDIT_SB_DELIVERY_COMPANY,
  SPD_EDIT_SB_DELIVERY_COMPANY_SVCS,
  SPD_EDIT_SB_HOLIDAYS,
  SPD_EDIT_SB_ENLIST_GROUPS,
  SPD_EDIT_SB_REFERRALS,
  SPD_EDIT_SB_REPORTS,
  SPD_EDIT_SB_AGENCY_REPORTS,
  SPD_EDIT_SB_UTILS,
  SPD_EDIT_SB_UTILS_DETAIL,
  SPD_EDIT_ZERO_ACH_PAYMENT,
  SPD_EDIT_UPDATE_STATE_DUE_DATE,
  SPD_EDIT_CHANGE_TAX_PAY_METHOD,
  SPD_EDIT_APPLY_SUI,
  SPD_EDIT_HSACleanup,
  SPD_EDIT_SB_ACA_GROUP,  
  ChoseW2Dlg,
  ChangeSUIRateDlg,
  SPD_EDIT_SB_OTHER_SERVICE,
  MoveClient,
  SAdjReportParamsDlg,
  SPD_EDIT_SB_EE_DBDT,
  SelectSBBankAccount,
  UpdateEEFICAFlag,
  LockQuarterliesDlg,
  SPD_EDIT_SB_IMPORT_SUI_RATES,
  SPD_EDIT_SB_CUSTOM_VENDORS,
  SPD_EDIT_SB_VENDOR,
  SPD_EDIT_SB_VENDOR_DETAIL,
  SPD_EDIT_SB_ENABLED_DASHBOARDS;

implementation

end.
