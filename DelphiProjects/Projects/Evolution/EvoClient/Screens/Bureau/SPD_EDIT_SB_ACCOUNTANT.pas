// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_ACCOUNTANT;

interface

uses SFrameEntry, Dialogs, StdCtrls, Buttons,  DBCtrls, Mask,
  wwdbedit, Controls, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Db, Wwdatsrc, ISBasicClasses, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIDBImage, isUIwwDBEdit;

type
  TEDIT_SB_ACCOUNTANT = class(TFrameEntry)
    pctlEDIT_SB_ACCOUNTANT: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtAccountant_Detail: TTabSheet;
    wwdgEDIT_SB_ACCOUNTANT: TevDBGrid;
    grbxPrimary_Contact: TevGroupBox;
    lablName2: TevLabel;
    dedtName2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    lablDescription2: TevLabel;
    wwdePhone1: TevDBEdit;
    lablPhone1: TevLabel;
    gbSB_Accountant1: TevGroupBox;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablPrint_Name: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    dedtState: TevDBEdit;
    wwdePrint_Name: TevDBEdit;
    gbSB_Accountant2: TevGroupBox;
    lablName3: TevLabel;
    lablPhone2: TevLabel;
    lablDescription3: TevLabel;
    dedtName3: TevDBEdit;
    dedtDescription3: TevDBEdit;
    wwdePhone2: TevDBEdit;
    gbSB_Accountant3: TevGroupBox;
    lablDescription: TevLabel;
    lablFax: TevLabel;
    dedtDescription: TevDBEdit;
    wwdeFax: TevDBEdit;
    lablE_Mail: TevLabel;
    dedtE_Mail: TevDBEdit;
    bevSBaccountant1: TEvBevel;
    lablSignature: TevLabel;
    dimgSignature: TevDBImage;
    bbtnLoad_Signature: TevBitBtn;
    odlgGetBitmap: TOpenDialog;
    procedure bbtnLoad_SignatureClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_ACCOUNTANT: TEDIT_SB_ACCOUNTANT;

implementation

uses EvUtils, SDataStructure, Graphics;

{$R *.DFM}

procedure TEDIT_SB_ACCOUNTANT.bbtnLoad_SignatureClick(Sender: TObject);
var
  TempPicture: TPicture;
begin
  inherited;
  if odlgGetBitmap.Execute then
  begin
    if not (dimgSignature.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      dimgSignature.DataSource.DataSet.Edit;
    TempPicture := TPicture.Create;
    try
      TempPicture.LoadFromFile(odlgGetBitmap.FileName);
      dimgSignature.Picture := TempPicture;
    finally
      TempPicture.Free;
    end;
  end;
end;

function TEDIT_SB_ACCOUNTANT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_ACCOUNTANT;
end;

function TEDIT_SB_ACCOUNTANT.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

initialization
  RegisterClass(TEDIT_SB_ACCOUNTANT);

end.
