inherited EDIT_SB_REPORTS: TEDIT_SB_REPORTS
  object pctlEDIT_SB_REPORTS: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    HelpContext = 34501
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_REPORTS: TevDBGrid
        Left = 0
        Top = 0
        Width = 641
        Height = 333
        DisableThemesInTitle = False
        Selected.Strings = (
          'NDescription'#9'40'#9'Name'#9'F'
          'RWDESCRIPTION'#9'40'#9'Description'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_REPORTS\wwdgEDIT_SB_REPORTS'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object memNotes: TEvDBMemo
        Left = 0
        Top = 333
        Width = 641
        Height = 71
        Align = alBottom
        DataField = 'RWDescription'
        DataSource = wwdsDetail
        ReadOnly = True
        TabOrder = 1
      end
    end
    object tshtReports_Detail: TTabSheet
      Caption = 'Details'
      object bevSB_REPORT1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
      end
      object lablDescription: TevLabel
        Left = 16
        Top = 16
        Width = 65
        Height = 13
        Caption = '~Description'
        FocusControl = dedtDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablNotes: TevLabel
        Left = 16
        Top = 119
        Width = 28
        Height = 13
        Caption = 'Notes'
        FocusControl = dmemNotes
      end
      object Label1: TevLabel
        Left = 16
        Top = 63
        Width = 26
        Height = 13
        Caption = 'Level'
      end
      object Label2: TevLabel
        Left = 200
        Top = 63
        Width = 32
        Height = 13
        Caption = 'Report'
        Visible = False
      end
      object wwlcSB_REPORTS: TevDBLookupCombo
        Left = 200
        Top = 79
        Width = 169
        Height = 21
        HelpContext = 34510
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'30'#9'REPORT_DESCRIPTION'
          'SB_REPORT_WRITER_REPORTS_NBR'#9'10'#9'SB_REPORT_WRITER_REPORTS_NBR'#9'F')
        DataField = 'REPORT_WRITER_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS
        LookupField = 'SB_REPORT_WRITER_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 3
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcSY_REPORTS: TevDBLookupCombo
        Left = 200
        Top = 79
        Width = 169
        Height = 21
        HelpContext = 34510
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'30'#9'REPORT_DESCRIPTION'
          'SY_REPORT_WRITER_REPORTS_NBR'#9'10'#9'SY_REPORT_WRITER_REPORTS_NBR'#9'F')
        DataField = 'REPORT_WRITER_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS
        LookupField = 'SY_REPORT_WRITER_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 2
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object dedtDescription: TevDBEdit
        Left = 16
        Top = 32
        Width = 353
        Height = 21
        HelpContext = 34502
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dmemNotes: TEvDBMemo
        Left = 16
        Top = 135
        Width = 545
        Height = 170
        DataField = 'COMMENTS'
        DataSource = wwdsDetail
        ScrollBars = ssBoth
        TabOrder = 4
      end
      object evComboBox1: TevDBComboBox
        Left = 16
        Top = 79
        Width = 145
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REPORT_LEVEL'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 13
        Items.Strings = (
          'System'#9'S'
          'Bureau'#9'B')
        ItemIndex = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
        OnChange = evComboBox1Change
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Report Default Parameters'
      ImageIndex = 2
      object Splitter1: TSplitter
        Left = 300
        Top = 39
        Width = 7
        Height = 121
      end
      object Panel2: TPanel
        Left = 0
        Top = 160
        Width = 427
        Height = 78
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 16
          Width = 101
          Height = 13
          Caption = 'Input Form Parameter'
        end
        object Label6: TLabel
          Left = 272
          Top = 16
          Width = 89
          Height = 13
          Caption = 'New Default Value'
        end
        object cbReportParams: TComboBox
          Left = 8
          Top = 34
          Width = 241
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbReportParamsChange
        end
        object cbReportParamValue: TComboBox
          Left = 272
          Top = 34
          Width = 177
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
        end
        object btnSetDefaultValue: TButton
          Left = 470
          Top = 32
          Width = 145
          Height = 25
          Caption = 'Set New Default Value'
          TabOrder = 2
          OnClick = btnSetDefaultValueClick
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 39
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = 
          'Please select Reports and Companies which you want to set defaul' +
          't parameters for'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object grSelCompanies: TevDBCheckGrid
        Left = 307
        Top = 39
        Width = 120
        Height = 121
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9#9
          'NAME'#9'40'#9'Name'#9#9)
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_SB_REPORTS\grSelCompanies'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsCompanies
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = False
        PaintOptions.AlternatingRowColor = clCream
        OnSelectionChanged = grSelReportsSelectionChanged
      end
      object grSelReports: TevDBCheckGrid
        Left = 0
        Top = 39
        Width = 300
        Height = 121
        DisableThemesInTitle = False
        Selected.Strings = (
          'NDescription'#9'40'#9'Name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_SB_REPORTS\grSelReports'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsDetail
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 3
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = False
        PaintOptions.AlternatingRowColor = clCream
        OnSelectionChanged = grSelReportsSelectionChanged
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 157
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_REPORTS.SB_REPORTS
    MasterDataSource = nil
    Left = 190
  end
  inherited wwdsList: TevDataSource
    Left = 122
  end
  object OpenDialog1: TOpenDialog
    Left = 364
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 448
    Top = 8
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 480
    Top = 8
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 512
    Top = 8
  end
  object dsCompanies: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 392
    Top = 128
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 544
    Top = 8
  end
end
