inherited EDIT_SB_OTHER_SERVICE: TEDIT_SB_OTHER_SERVICE
  object lablName: TevLabel [0]
    Left = 8
    Top = 360
    Width = 97
    Height = 13
    Caption = '~Other Service'
    FocusControl = dedtOtherService
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 8
    Top = 8
    Width = 320
    Height = 345
    Selected.Strings = (
      'NAME'#9'60'#9'Name')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
    IniAttributes.SectionName = 'TEDIT_SB_OTHER_SERVICE\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = wwdsDetail
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object dedtOtherService: TevDBEdit [2]
    Left = 8
    Top = 376
    Width = 321
    Height = 21
    HelpContext = 38002
    DataField = 'NAME'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
end
