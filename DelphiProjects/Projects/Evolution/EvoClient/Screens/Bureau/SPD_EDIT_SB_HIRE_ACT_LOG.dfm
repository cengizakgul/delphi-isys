object SPD_EDIT_SB_HIRE_ACT_LOG: TSPD_EDIT_SB_HIRE_ACT_LOG
  Left = 315
  Top = 190
  Width = 787
  Height = 565
  Caption = 'Hire Act update log'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 490
    Width = 779
    Height = 41
    Align = alBottom
    TabOrder = 0
    object evButton1: TevButton
      Left = 8
      Top = 8
      Width = 97
      Height = 25
      Caption = 'Save to File...'
      TabOrder = 0
      OnClick = evButton1Click
    end
  end
  object LogDetails: TMemo
    Left = 0
    Top = 0
    Width = 779
    Height = 490
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'log'
    Filter = 'Text files (*.txt)|*.txt'
    InitialDir = 'C:\'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 120
    Top = 496
  end
end
