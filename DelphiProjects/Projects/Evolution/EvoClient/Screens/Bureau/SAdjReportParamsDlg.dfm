object dlgAdjReportParams: TdlgAdjReportParams
  Left = 617
  Top = 339
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Print Combined Adjustment Reports'
  ClientHeight = 234
  ClientWidth = 318
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    318
    234)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TevPanel
    Left = 5
    Top = 5
    Width = 309
    Height = 193
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object lblTaxLimit: TevLabel
      Left = 10
      Top = 135
      Width = 126
      Height = 13
      Caption = 'Tax Adjustment Threshold '
    end
    object grbCheckDatePeriod: TevGroupBox
      Left = 10
      Top = 10
      Width = 291
      Height = 105
      Caption = 'Check Date Period'
      TabOrder = 0
      object lblYear: TevLabel
        Left = 10
        Top = 20
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object lblFrom: TevLabel
        Left = 10
        Top = 75
        Width = 23
        Height = 13
        Caption = 'From'
      end
      object lblTo: TevLabel
        Left = 160
        Top = 75
        Width = 13
        Height = 13
        Caption = 'To'
      end
      object cmbYear: TevComboBox
        Left = 10
        Top = 35
        Width = 86
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cmbYearChange
      end
      object chb1stQtr: TevCheckBox
        Tag = 1
        Left = 115
        Top = 15
        Width = 81
        Height = 17
        Caption = '1st Quarter'
        TabOrder = 1
        OnClick = chb1stQtrClick
      end
      object chb2ndQtr: TevCheckBox
        Tag = 2
        Left = 115
        Top = 40
        Width = 81
        Height = 17
        Caption = '2nd Quarter'
        TabOrder = 2
        OnClick = chb1stQtrClick
      end
      object chb3rdQtr: TevCheckBox
        Tag = 3
        Left = 205
        Top = 15
        Width = 81
        Height = 17
        Caption = '3rd Quarter'
        TabOrder = 3
        OnClick = chb1stQtrClick
      end
      object chb4thQtr: TevCheckBox
        Tag = 4
        Left = 205
        Top = 40
        Width = 81
        Height = 17
        Caption = '4th Quarter'
        TabOrder = 4
        OnClick = chb1stQtrClick
      end
      object edFrom: TevDateTimePicker
        Left = 40
        Top = 70
        Width = 101
        Height = 21
        Date = 39315.000000000000000000
        Time = 39315.000000000000000000
        TabOrder = 5
        OnChange = edToChange
      end
      object edTo: TevDateTimePicker
        Left = 180
        Top = 70
        Width = 101
        Height = 21
        Date = 39315.000000000000000000
        Time = 39315.000000000000000000
        TabOrder = 6
        OnChange = edToChange
      end
    end
    object edTaxAdjLimit: TevEdit
      Left = 165
      Top = 133
      Width = 136
      Height = 21
      TabOrder = 1
      Text = '0.00'
      OnChange = edTaxAdjLimitChange
    end
    object chbQueue: TevCheckBox
      Left = 10
      Top = 165
      Width = 161
      Height = 17
      Caption = 'Run through the Queue'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
  end
  object btnPrint: TevButton
    Left = 68
    Top = 203
    Width = 75
    Height = 25
    Action = Print
    Anchors = [akRight, akBottom]
    ModalResult = 1
    TabOrder = 1
  end
  object btnPreview: TevButton
    Left = 153
    Top = 203
    Width = 75
    Height = 25
    Action = Preview
    Anchors = [akRight, akBottom]
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TevButton
    Left = 238
    Top = 203
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object dlgActions: TevActionList
    Left = 10
    Top = 10
    object Print: TAction
      Caption = 'Print'
      OnExecute = PrintExecute
    end
    object Preview: TAction
      Caption = 'Preview'
      OnExecute = PreviewExecute
    end
  end
end
