// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ChoseW2Dlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls,  SFieldCodeValues, EvUIComponents;

type
  TChosePaperType = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evRadioGroup1: TevRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure evRadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    Values: TStringList;
  end;

implementation

{$R *.dfm}

procedure TChosePaperType.FormCreate(Sender: TObject);
var
  j: Integer;
  t: TStringList;
begin
  Values := TStringList.Create;
  t := TStringList.Create;
  with evRadioGroup1 do
  try
    t.Text := W2MediaType_ComboChoices;
    Items.Clear;
    Values.Clear;
    for j := 0 to Pred(t.Count) do
    begin
      Items.Add(Copy(t[j], 1, Pred(Pos(#9, t[j]))));
      Values.Add(Copy(t[j], Succ(Pos(#9, t[j])), Length(t[j])));
    end;
  finally
    t.Free;
  end;
end;

procedure TChosePaperType.FormDestroy(Sender: TObject);
begin
  Values.Free;
end;

procedure TChosePaperType.evRadioGroup1Click(Sender: TObject);
begin
  OKBtn.Enabled := evRadioGroup1.ItemIndex >= 0;
end;

end.
