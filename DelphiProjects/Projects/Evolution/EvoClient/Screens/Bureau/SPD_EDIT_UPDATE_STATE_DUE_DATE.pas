// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_UPDATE_STATE_DUE_DATE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook,  SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvBasicUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBLookupCombo;

type
  TEDIT_UPDATE_STATE_DUE_DATE = class(TForm)
    evLabel1: TevLabel;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    edState: TevDBLookupCombo;
    evLabel2: TevLabel;
    edOldDate: TevDateTimePicker;
    evLabel3: TevLabel;
    edNewDate: TevDateTimePicker;
    bOk: TevButton;
    rgTypeChoice: TevRadioGroup;
    evLabel4: TevLabel;
    edLocal: TevDBLookupCombo;
    evLabel5: TevLabel;
    edSui: TevDBLookupCombo;
    evTCDLocalTax: TevCheckBox;
    cbYears: TevComboBox;
    cbMonth: TevComboBox;
    evLabel6: TevLabel;
    evSelectCheckDate: TevCheckBox;
    procedure bOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rgTypeChoiceClick(Sender: TObject);
    procedure edStateChange(Sender: TObject);
    procedure evTCDLocalTaxClick(Sender: TObject);
    procedure evSelectCheckDateClick(Sender: TObject);
  private
    { Private declarations }
    procedure checkTCDLocalTax;    
  public
    { Public declarations }
  end;

implementation

uses EvUtils,EvConsts;

{$R *.DFM}

procedure TEDIT_UPDATE_STATE_DUE_DATE.bOkClick(Sender: TObject);
begin
  if ((rgTypeChoice.ItemIndex = 1) and (edState.DisplayValue <> '') and (edSui.DisplayValue <> '') ) or
     (((rgTypeChoice.ItemIndex = 0) or (rgTypeChoice.ItemIndex = 3)) and (edState.DisplayValue <> '') ) or
     (rgTypeChoice.ItemIndex = 2) then
    ModalResult := mrOk;
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.FormCreate(Sender: TObject);
var
 pYear, pYearEnd : integer;
begin
  DM_SYSTEM_STATE.SY_STATES.Activate;
  DM_SYSTEM_STATE.SY_LOCALS.Activate;
  DM_SYSTEM_STATE.SY_SUI.DataRequired('SUI_ACTIVE = ''Y'' ');
  edOldDate.DateTime := Date;
  edNewDate.DateTime := Date;
  evTCDLocalTax.Enabled := false;
  evTCDLocalTax.State :=  cbUnchecked;
  rgTypeChoiceClick(self);

  pYear    := GetYear(Now)-3;
  pYearEnd := GetYear(Now) + 2;

  cbYears.Clear;
  with cbYears do
  begin
    repeat
      Items.Add(IntToStr(pYear));
      inc(pYear);
    until pYear > pYearEnd;
  end;
  cbYears.ItemIndex := 3;

  cbMonth.Items.Text := Items_Months;
  cbMonth.ItemIndex := GetMonth(Now)-1;

  cbYears.Enabled := false;
  cbMonth.Enabled := false;
  evSelectCheckDate.Enabled := false;
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.rgTypeChoiceClick(Sender: TObject);
begin

  edState.Enabled := not (rgTypeChoice.ItemIndex = 2);
  edSui.Enabled   := (rgTypeChoice.ItemIndex = 1);
  edLocal.Enabled := (rgTypeChoice.ItemIndex = 3);

  checkTCDLocalTax;
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.edStateChange(Sender: TObject);
begin
  if rgTypeChoice.ItemIndex <> 1 then
     edSui.Clear;
  if rgTypeChoice.ItemIndex <> 2 then
     edLocal.Clear;

  DM_SYSTEM_STATE.SY_SUI.Filter :='SUI_ACTIVE = ''Y'' and SY_STATES_NBR ='+ IntToStr(DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR']);
  DM_SYSTEM_STATE.SY_SUI.Filtered :=True;

  DM_SYSTEM_STATE.SY_LOCALS.Filter :='SY_STATES_NBR ='+ IntToStr(DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR']);
  DM_SYSTEM_STATE.SY_LOCALS.Filtered :=True;

  checkTCDLocalTax;
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.checkTCDLocalTax;
begin
  evTCDLocalTax.Enabled := (edState.DisplayValue = 'PA') and (rgTypeChoice.ItemIndex = 3);

  if not evTCDLocalTax.Enabled then
  begin
     evTCDLocalTax.State := cbUnchecked;
     evSelectCheckDate.State := cbUnchecked;
  end;

  evTCDLocalTaxClick(self);
  evSelectCheckDateClick(self);
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.evTCDLocalTaxClick(Sender: TObject);
begin
  edLocal.Enabled := (rgTypeChoice.ItemIndex = 3);
  evSelectCheckDate.Enabled := evTCDLocalTax.State = cbChecked;

  if (evTCDLocalTax.State = cbChecked) then
  begin
     edLocal.Enabled := false;;
     edLocal.DisplayValue := '';
  end
  else
     evSelectCheckDate.State := cbUnchecked;

  evSelectCheckDateClick(self);
end;

procedure TEDIT_UPDATE_STATE_DUE_DATE.evSelectCheckDateClick(
  Sender: TObject);
begin
  cbYears.Enabled   := (evSelectCheckDate.State = cbChecked) and (evTCDLocalTax.State = cbChecked);
  cbMonth.Enabled   := (evSelectCheckDate.State = cbChecked) and (evTCDLocalTax.State = cbChecked);
end;

end.
