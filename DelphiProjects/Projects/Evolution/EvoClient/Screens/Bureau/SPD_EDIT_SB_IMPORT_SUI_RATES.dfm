object TEDIT_SB_IMPORT_SUI_RATES: TTEDIT_SB_IMPORT_SUI_RATES
  Left = 401
  Top = 247
  Width = 744
  Height = 561
  Caption = 'Import SUI Rates'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 482
    Width = 728
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      728
      41)
    object btnExit: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
      OnClick = btnExitClick
    end
    object btnBack: TButton
      Left = 536
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '< Back'
      TabOrder = 1
      OnClick = btnBackClick
    end
    object btnNext: TButton
      Left = 648
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Next >'
      TabOrder = 2
      OnClick = btnNextClick
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 0
    Width = 728
    Height = 482
    Align = alClient
    TabOrder = 1
    object Wizard: TNotebook
      Left = 1
      Top = 1
      Width = 726
      Height = 480
      Align = alClient
      TabOrder = 0
      object TPage
        Left = 0
        Top = 0
        Caption = 'Settings'
        object evLabel1: TevLabel
          Left = 16
          Top = 12
          Width = 155
          Height = 13
          Caption = 'Please, provide parameters'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel3: TevLabel
          Left = 16
          Top = 48
          Width = 48
          Height = 13
          Caption = 'Import File'
        end
        object evLabel4: TevLabel
          Left = 16
          Top = 82
          Width = 83
          Height = 13
          Caption = 'Output File Folder'
        end
        object evLabel5: TevLabel
          Left = 16
          Top = 119
          Width = 98
          Height = 13
          Caption = 'Exception File Folder'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object spdImportFile: TevSpeedButton
          Left = 534
          Top = 45
          Width = 25
          Height = 25
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000008484000084840000848400008484000084840000848400008484000084
            84000084840000000000FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00000000000084840000848400008484000084840000848400008484000084
            8400008484000084840000000000FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF000000000000848400008484000084840000848400008484000084
            840000848400008484000084840000000000FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000008484000084840000848400008484000084
            84000084840000848400008484000084840000000000FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000000000000000FF
            FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00
            FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          Margin = 3
          OnClick = spdImportFileClick
          ShortCut = 0
        end
        object spdOutputFile: TevSpeedButton
          Left = 534
          Top = 78
          Width = 25
          Height = 25
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000008484000084840000848400008484000084840000848400008484000084
            84000084840000000000FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00000000000084840000848400008484000084840000848400008484000084
            8400008484000084840000000000FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF000000000000848400008484000084840000848400008484000084
            840000848400008484000084840000000000FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000008484000084840000848400008484000084
            84000084840000848400008484000084840000000000FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000000000000000FF
            FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00
            FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          Margin = 3
          OnClick = spdOutputFileClick
          ShortCut = 0
        end
        object spdExceptionFile: TevSpeedButton
          Left = 534
          Top = 113
          Width = 25
          Height = 25
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
            0000008484000084840000848400008484000084840000848400008484000084
            84000084840000000000FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00000000000084840000848400008484000084840000848400008484000084
            8400008484000084840000000000FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF000000000000848400008484000084840000848400008484000084
            840000848400008484000084840000000000FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000008484000084840000848400008484000084
            84000084840000848400008484000084840000000000FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
            00000000000000000000000000000000000000000000000000000000000000FF
            FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
            FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
            FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
            FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
            000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00
            FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
            00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          Margin = 3
          OnClick = spdExceptionFileClick
          ShortCut = 0
        end
        object evLabel6: TevLabel
          Left = 16
          Top = 160
          Width = 25
          Height = 13
          Caption = 'State'
        end
        object evLabel7: TevLabel
          Left = 16
          Top = 200
          Width = 258
          Height = 13
          Caption = 'Rate can only be changed as of beginning of a quarter'
        end
        object ImportFileEdit: TevEdit
          Left = 122
          Top = 46
          Width = 410
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object ExceptionFileEdit: TevEdit
          Left = 122
          Top = 116
          Width = 410
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object OutputFileEdit: TevEdit
          Left = 122
          Top = 80
          Width = 410
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object StatesCombo: TevComboBox
          Left = 122
          Top = 152
          Width = 55
          Height = 21
          Style = csDropDownList
          DropDownCount = 20
          ItemHeight = 13
          TabOrder = 3
        end
        object evGroupBox1: TevGroupBox
          Left = 16
          Top = 218
          Width = 281
          Height = 81
          Caption = ' Effective Date '
          TabOrder = 4
          object evLabel8: TevLabel
            Left = 11
            Top = 24
            Width = 22
            Height = 13
            Caption = 'Year'
          end
          object evDateTimePicker1: TevDateTimePicker
            Left = 8
            Top = 45
            Width = 73
            Height = 21
            Date = 40645.000000000000000000
            Format = 'yyyy'
            Time = 40645.000000000000000000
            DateMode = dmUpDown
            TabOrder = 0
          end
          object rb1Q: TevRadioButton
            Left = 104
            Top = 16
            Width = 81
            Height = 17
            Caption = '1st Quarter'
            Checked = True
            TabOrder = 1
            TabStop = True
          end
          object rb2Q: TevRadioButton
            Left = 192
            Top = 16
            Width = 81
            Height = 17
            Caption = '2nd Quarter'
            TabOrder = 2
          end
          object rb3Q: TevRadioButton
            Left = 104
            Top = 48
            Width = 81
            Height = 17
            Caption = '3rd Quarter'
            TabOrder = 3
          end
          object rb4Q: TevRadioButton
            Left = 192
            Top = 48
            Width = 81
            Height = 17
            Caption = '4th Quarter'
            TabOrder = 4
          end
        end
        object evGroupBox2: TevGroupBox
          Left = 16
          Top = 310
          Width = 281
          Height = 46
          Caption = ' EIN Type '
          TabOrder = 5
          object rbState: TevRadioButton
            Left = 16
            Top = 21
            Width = 57
            Height = 17
            Caption = 'State'
            TabOrder = 0
          end
          object rbSUI: TevRadioButton
            Left = 80
            Top = 21
            Width = 49
            Height = 17
            Caption = 'SUI'
            Checked = True
            TabOrder = 1
            TabStop = True
          end
        end
        object evGroupBox3: TevGroupBox
          Left = 16
          Top = 368
          Width = 281
          Height = 57
          Caption = ' Ignore zeros in EIN '
          TabOrder = 6
          object cbLeading: TevRadioButton
            Left = 16
            Top = 24
            Width = 65
            Height = 17
            Caption = 'Leading'
            TabOrder = 0
          end
          object cbTrailing: TevRadioButton
            Left = 104
            Top = 24
            Width = 65
            Height = 17
            Caption = 'Trailing'
            TabOrder = 1
          end
          object cbBoth: TevRadioButton
            Left = 200
            Top = 24
            Width = 49
            Height = 17
            Caption = 'Both'
            Checked = True
            TabOrder = 2
            TabStop = True
          end
        end
      end
      object TPage
        Left = 0
        Top = 0
        Caption = 'Mapping'
        object evPanel3: TevPanel
          Left = 0
          Top = 0
          Width = 734
          Height = 484
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object evPanel4: TevPanel
            Left = 0
            Top = 0
            Width = 734
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object evLabel2: TevLabel
              Left = 16
              Top = 12
              Width = 95
              Height = 13
              Caption = 'Please, map SUI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
          object evPanel5: TevPanel
            Left = 0
            Top = 27
            Width = 734
            Height = 457
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 0
              Top = 248
              Width = 734
              Height = 3
              Cursor = crVSplit
              Align = alTop
            end
            object evPanel6: TevPanel
              Left = 0
              Top = 0
              Width = 734
              Height = 248
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object evPanel8: TevPanel
                Left = 0
                Top = 0
                Width = 734
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object evLabel10: TevLabel
                  Left = 16
                  Top = 8
                  Width = 50
                  Height = 13
                  Caption = 'Import SUI'
                end
                object evLabel11: TevLabel
                  Left = 370
                  Top = 8
                  Width = 65
                  Height = 13
                  Caption = 'Evolution SUI'
                end
              end
              object evPanel9: TevPanel
                Left = 0
                Top = 207
                Width = 734
                Height = 41
                Align = alBottom
                TabOrder = 1
                object btnAdd: TevBitBtn
                  Left = 17
                  Top = 8
                  Width = 90
                  Height = 25
                  Caption = 'Add'
                  TabOrder = 0
                  OnClick = btnAddClick
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000010000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
                    DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
                    DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86EEEEE68D
                    DDDDDDF8888888FDDDDDD86EEEEEEE68DDDDDF888888888FDDDD86EEEEEEEEE6
                    8DDDF88888888888FDDD6666EEEEE6666DDD8888888888888DDDDDD6EEEEE6DD
                    DDDDDDD8888888DDDDDDDDD6E6666688888DDDD8888888FFFFFFDDD6E6000000
                    0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
                    FFF0DDD8888DDDDDDDD8DDD6EE0000000000DDD8888888888888DDD6EEEEE6DD
                    DDDDDDD8888888DDDDDDDDD6666666DDDDDDDDD8888888DDDDDD}
                  NumGlyphs = 2
                end
                object btnRemove: TevBitBtn
                  Left = 129
                  Top = 8
                  Width = 90
                  Height = 25
                  Caption = 'Remove'
                  TabOrder = 1
                  OnClick = btnRemoveClick
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000000000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
                    DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
                    888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
                    FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
                    0000DDD8888888888888DDD6EEEEE6DDDDDDDDD8888888DDDDDD8886EEEEE688
                    8DDDFFF8888888FFFDDD6666EEEEE6666DDD8888888888888DDDD6EEEEEEEEE6
                    DDDDD88888888888DDDDDD6EEEEEEE6DDDDDDD888888888DDDDDDDD6EEEEE6DD
                    DDDDDDD8888888DDDDDDDDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
                    DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
                  NumGlyphs = 2
                end
                object btnRemoveAll: TevBitBtn
                  Left = 241
                  Top = 8
                  Width = 90
                  Height = 25
                  Caption = 'Remove All'
                  TabOrder = 2
                  OnClick = btnRemoveAllClick
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000000000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
                    DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
                    888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
                    FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E6000000
                    0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD88886E60FFFFF
                    FFF0FFF8888DDDDDDDD86666E600000000008888888888888888D6EEE60FFFFF
                    FFF0D888888DDDDDDDD8DD6EE60FFFFFFFF0DD88888DDDDDDDD8DDD6EE000000
                    0000DDD8888888888888DDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
                    DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
                  NumGlyphs = 2
                end
              end
              object evPanel10: TevPanel
                Left = 0
                Top = 25
                Width = 734
                Height = 182
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 2
                object evPanel12: TevPanel
                  Left = 0
                  Top = 0
                  Width = 353
                  Height = 182
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object ImportSUIGrid: TevDBGrid
                    Left = 0
                    Top = 0
                    Width = 353
                    Height = 182
                    DisableThemesInTitle = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TTEDIT_SB_IMPORT_SUI_RATES\ImportSUIGrid'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = ImportSUIDataSource
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Courier New'
                    Font.Style = []
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                    ParentFont = False
                    TabOrder = 0
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = clCream
                  end
                end
                object evPanel13: TevPanel
                  Left = 353
                  Top = 0
                  Width = 381
                  Height = 182
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object EvolutionSUIGrid: TevDBGrid
                    Left = 0
                    Top = 0
                    Width = 381
                    Height = 182
                    DisableThemesInTitle = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TTEDIT_SB_IMPORT_SUI_RATES\EvolutionSUIGrid'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = EvolutionSUIDataSource
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'Courier New'
                    Font.Style = []
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                    ParentFont = False
                    TabOrder = 0
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = clCream
                  end
                end
              end
            end
            object evPanel7: TevPanel
              Left = 0
              Top = 251
              Width = 734
              Height = 206
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object evPanel11: TevPanel
                Left = 0
                Top = 0
                Width = 734
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object evLabel9: TevLabel
                  Left = 16
                  Top = 8
                  Width = 60
                  Height = 13
                  Caption = 'Mapped SUI'
                end
              end
              object MappedSUIGrid: TevDBGrid
                Left = 0
                Top = 25
                Width = 734
                Height = 181
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TTEDIT_SB_IMPORT_SUI_RATES\MappedSUIGrid'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = MappedSUIDataSource
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Courier New'
                Font.Style = []
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                ParentFont = False
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = True
                PaintOptions.AlternatingRowColor = clCream
              end
            end
          end
        end
      end
    end
  end
  object ImportFileDlg: TOpenDialog
    Filter = 
      'Comma delimited file (*.csv)|*.csv|Comma delimited file (*.txt)|' +
      '*.txt'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 600
    Top = 8
  end
  object ImportSUIDataSource: TDataSource
    Left = 449
    Top = 17
  end
  object EvolutionSUIDataSource: TDataSource
    Left = 489
    Top = 17
  end
  object MappedSUIDataSource: TDataSource
    Left = 529
    Top = 17
  end
end
