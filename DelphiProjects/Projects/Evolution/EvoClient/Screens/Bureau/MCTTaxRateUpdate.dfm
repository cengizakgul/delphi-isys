object MCTTaxRateUpdate: TMCTTaxRateUpdate
  Left = 533
  Top = 369
  BorderStyle = bsDialog
  Caption = 'MCT Update'
  ClientHeight = 110
  ClientWidth = 321
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    321
    110)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel3: TevLabel
    Left = 16
    Top = 16
    Width = 68
    Height = 13
    Caption = 'Effective Date'
  end
  object OKBtn: TButton
    Left = 95
    Top = 60
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 207
    Top = 60
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object cbQuarter: TevComboBox
    Left = 96
    Top = 13
    Width = 100
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 2
    Text = '1st quarter'
    Items.Strings = (
      '1st quarter'
      '2nd quarter'
      '3rd quarter'
      '4th quarter')
  end
  object edYear: TevSpinEdit
    Left = 208
    Top = 13
    Width = 73
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 0
  end
end
