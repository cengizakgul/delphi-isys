// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_HOLIDAYS;

interface

uses SFrameEntry,  wwdbdatetimepicker, StdCtrls, ExtCtrls,
  DBCtrls, Mask, wwdbedit, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_HOLIDAYS = class(TFrameEntry)
    pctlEDIT_SB_HOLIDAYS: TevPageControl;
    tshtBrowse: TTabSheet;
    wwdgEDIT_SB_HOLIDAYS: TevDBGrid;
    lablDate: TevLabel;
    lablName: TevLabel;
    dedtName: TevDBEdit;
    drgpUsed_By: TevDBRadioGroup;
    wwdpHoliday_Date: TevDBDateTimePicker;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_HOLIDAYS: TEDIT_SB_HOLIDAYS;

implementation

uses SDataStructure;

{$R *.DFM}

function TEDIT_SB_HOLIDAYS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_HOLIDAYS;
end;

function TEDIT_SB_HOLIDAYS.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

initialization
  RegisterClass(TEDIT_SB_HOLIDAYS);

end.
