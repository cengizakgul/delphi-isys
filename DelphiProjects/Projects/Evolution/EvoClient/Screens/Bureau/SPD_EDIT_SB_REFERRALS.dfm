inherited EDIT_SB_REFERRALS: TEDIT_SB_REFERRALS
  object lablName: TevLabel [0]
    Left = 8
    Top = 360
    Width = 68
    Height = 13
    Caption = 'Referral Name'
    FocusControl = dedtReferral
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 8
    Top = 8
    Width = 320
    Height = 345
    Selected.Strings = (
      'NAME'#9'40'#9'Name')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = wwdsDetail
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    IndicatorColor = icBlack
  end
  object dedtReferral: TevDBEdit [2]
    Left = 8
    Top = 376
    Width = 321
    Height = 21
    HelpContext = 38002
    DataField = 'NAME'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
end
