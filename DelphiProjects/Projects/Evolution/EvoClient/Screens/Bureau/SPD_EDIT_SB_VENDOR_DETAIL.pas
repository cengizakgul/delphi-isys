// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_VENDOR_DETAIL;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, SDataDictbureau, Forms,
  SPD_MiniNavigationFrame, Wwdbcomb, isUIwwDBComboBox, ComCtrls, EvUtils, SysUtils,
  EvExceptions, EvCommonInterfaces, EvDataset;

type
  TEDIT_SB_VENDOR_DETAIL = class(TFrameEntry)
    pgSB_Vendor_Detail: TevPageControl;
    tshtBrowse: TTabSheet;
    wwdgEDIT_SB_ACCOUNTANT: TevDBGrid;
    tshtVendorDetail: TTabSheet;
    bevSBaccountant1: TEvBevel;
    grdVedorDetail: TevDBGrid;
    grdDetailValue: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    gbSB_Accountant1: TevGroupBox;
    Label1: TevLabel;
    dedtDetail: TevDBEdit;
    edCategory: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    dedtValue: TevDBEdit;
    MiniNavigationFrame: TMiniNavigationFrame;
    dsSBVendor: TevDataSource;
    dsSBVendorDetailValues: TevDataSource;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    lblVendor: TevLabel;
    evLabel2: TevLabel;
    procedure SetSBVendorDetail;
    procedure MiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton2Click(Sender: TObject);
    procedure dsSBVendorDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure pgSB_Vendor_DetailChange(Sender: TObject);
    procedure pgSB_Vendor_DetailChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure dsSBVendorDetailValuesStateChange(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    { Public declarations }
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;    
  end;

implementation

uses SPackageEntry;

{$R *.DFM}

procedure TEDIT_SB_VENDOR_DETAIL.Activate;
begin
  inherited;
  MiniNavigationFrame.DataSource := dsSBVendorDetailValues;
  MiniNavigationFrame.InsertFocusControl := dedtValue;
  MiniNavigationFrame.SpeedButton1.OnClick := MiniNavigationFrameSpeedButton1Click;
  MiniNavigationFrame.SpeedButton2.OnClick := MiniNavigationFrameSpeedButton2Click;

end;


function TEDIT_SB_VENDOR_DETAIL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_VENDOR_DETAIL;
end;

procedure TEDIT_SB_VENDOR_DETAIL.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_VENDORS, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES, EditDataSets, 'ALL');
end;

function TEDIT_SB_VENDOR_DETAIL.GetInsertControl: TWinControl;
begin
  Result := dedtDetail;
end;


procedure TEDIT_SB_VENDOR_DETAIL.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

  function CheckSbVendorDetail:boolean;
  var
     s: string;
     Q: IEvQuery;
  begin
   result:= true;
   if not DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.DETAIL.IsNull then
   begin
    s:= 'select count(SB_VENDOR_DETAIL_NBR) ' +
           ' from SB_VENDOR_DETAIL where {AsOfNow<SB_VENDOR_DETAIL>} and ' +
           '   SB_VENDOR_DETAIL_NBR <> :Nbr and ' +
           '   SB_VENDOR_NBR = :sbVendorNbr and ' +
           '   DETAIL = :pDetail ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('Nbr', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.AsInteger);
    Q.Params.AddValue('sbVendorNbr', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_NBR.AsInteger);
    Q.Params.AddValue('pDetail', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.DETAIL.AsString);

    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
   end;
  end;

  function CheckSbVendorDetailValue:boolean;
  var
     s: string;
     Q: IEvQuery;
  begin
   result:= true;
   if not DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.DETAIL_VALUES.IsNull then
   begin
    s:= 'select count(SB_VENDOR_DETAIL_VALUES_NBR) ' +
           ' from SB_VENDOR_DETAIL_VALUES where {AsOfNow<SB_VENDOR_DETAIL_VALUES>} and ' +
           '     SB_VENDOR_DETAIL_VALUES_NBR <> :Nbr and ' +
           '     SB_VENDOR_DETAIL_NBR = :sbVendorDetailNbr and ' +
           '     DETAIL_VALUES = :detailValue ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('Nbr', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_NBR.AsInteger);
    Q.Params.AddValue('sbVendorDetailNbr', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_NBR.AsInteger);
    Q.Params.AddValue('detailValue', DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.DETAIL_VALUES.asString);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
   end;
  end;


begin
  inherited;

  if Kind = NavDelete then
    if (DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.RecordCount > 0) then
       raise EUpdateError.CreateHelp('Please delete all the Detail values before deleting the Detail.', IDH_ConsistencyViolation);

  if Kind = NavOK then
  begin

    if wwdsDetail.DataSet.State in [dsEdit,dsInsert] then
    begin
       wwdsDetail.DataSet.UpdateRecord;
       DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_NBR.AsInteger := DM_SERVICE_BUREAU.SB_VENDOR.SB_VENDOR_NBR.AsInteger;

       if CheckSbVendorDetail then
          raise EUpdateError.CreateHelp('Detail already exists..', IDH_ConsistencyViolation);

    end;

     if dsSBVendorDetailValues.DataSet.State in [dsEdit,dsInsert] then
     begin
         dsSBVendorDetailValues.DataSet.UpdateRecord;
         DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_NBR.Value :=
               DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.Value;

       if CheckSbVendorDetailValue then
          raise EUpdateError.CreateHelp('Detail Value already exists.', IDH_ConsistencyViolation);

     end;
  end;
end;


procedure TEDIT_SB_VENDOR_DETAIL.MiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin

  if DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.AsInteger > 0 then
  begin
    inherited;
    MiniNavigationFrame.InsertRecordExecute(Sender);

    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_NBR.Value :=
         DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.Value;
  end;
end;

procedure TEDIT_SB_VENDOR_DETAIL.MiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.DeleteRecordExecute(Sender);

end;

procedure TEDIT_SB_VENDOR_DETAIL.SetSBVendorDetail;
begin
   lblVendor.Caption := dsSBVendor.DataSet.FieldByName('NAME').AsString;

   if DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.Active then
   begin
     DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFilter := 'SB_VENDOR_NBR = ' + IntToStr(DM_SERVICE_BUREAU.SB_VENDOR.SB_VENDOR_NBR.AsInteger);
     DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFiltered := True;
   end;
end;


procedure TEDIT_SB_VENDOR_DETAIL.dsSBVendorDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if Assigned(dsSBVendor.DataSet) and dsSBVendor.DataSet.Active then
      SetSBVendorDetail;
end;

procedure TEDIT_SB_VENDOR_DETAIL.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if csLoading in ComponentState then
    Exit;

  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
  begin
     if DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.Active then
     begin
       DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFilter := 'SB_VENDOR_DETAIL_NBR = ' + IntToStr(DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.AsInteger);
       DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFiltered := True;
     end;
     MiniNavigationFrame.Enabled:= DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.SB_VENDOR_DETAIL_NBR.AsInteger > 0;
  end
  else
     MiniNavigationFrame.Enabled:=false;

end;

procedure TEDIT_SB_VENDOR_DETAIL.pgSB_Vendor_DetailChange(
  Sender: TObject);
begin
  inherited;
  if (pgSB_Vendor_Detail.ActivePage = tshtVendorDetail) then
     SetSBVendorDetail;
end;

procedure TEDIT_SB_VENDOR_DETAIL.pgSB_Vendor_DetailChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  AllowChange:= Assigned(dsSBVendor.DataSet) and (dsSBVendor.DataSet.RecordCount > 0); 
end;

procedure TEDIT_SB_VENDOR_DETAIL.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  grdVedorDetail.Enabled := not (wwdsDetail.DataSet.State in [dsInsert,dsEdit]);
end;

procedure TEDIT_SB_VENDOR_DETAIL.dsSBVendorDetailValuesStateChange(
  Sender: TObject);
begin
  inherited;
  grdDetailValue.Enabled := not (dsSBVendorDetailValues.DataSet.State in [dsInsert,dsEdit]);
end;

initialization
  RegisterClass(TEDIT_SB_VENDOR_DETAIL);
end.
