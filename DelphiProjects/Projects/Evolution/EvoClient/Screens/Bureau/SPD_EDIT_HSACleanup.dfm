object EDIT_HSACleanup: TEDIT_HSACleanup
  Left = 467
  Top = 209
  Width = 390
  Height = 280
  Caption = 'HSA Cleanup  '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object fpShiftSummary: TisUIFashionPanel
    Left = 8
    Top = 8
    Width = 354
    Height = 218
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Details'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    object evLabel5: TevLabel
      Left = 12
      Top = 39
      Width = 151
      Height = 13
      Caption = 'Multiple Scheduled E/D Group  '
    end
    object evLabel3: TevLabel
      Left = 12
      Top = 64
      Width = 130
      Height = 13
      Caption = 'EE Contribution E/D Code  '
    end
    object evLabel1: TevLabel
      Left = 12
      Top = 89
      Width = 108
      Height = 13
      Caption = 'EE Catchup E/D Code'
    end
    object evLabel2: TevLabel
      Left = 12
      Top = 114
      Width = 125
      Height = 13
      Caption = 'ER Contribution E/D Code'
    end
    object evLabel4: TevLabel
      Left = 12
      Top = 139
      Width = 167
      Height = 13
      Caption = 'Optional ER Contribution E/D Code'
    end
    object bOk: TevButton
      Left = 193
      Top = 172
      Width = 97
      Height = 25
      Caption = 'Apply'
      TabOrder = 0
      OnClick = bOkClick
      Color = clBlack
      Margin = 0
    end
    object edtMultEDCode: TevDBLookupCombo
      Left = 193
      Top = 35
      Width = 138
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'40'#9'Name'#9#9)
      LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupField = 'CL_E_D_GROUPS_NBR'
      Style = csDropDownList
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object edtEEContEDCode: TevDBLookupCombo
      Left = 193
      Top = 60
      Width = 138
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'E/D Code'#9#9
        'DESCRIPTION'#9'40'#9'Description'#9#9)
      LookupTable = DM_CL_E_DS.CL_E_DS
      LookupField = 'CL_E_DS_NBR'
      Style = csDropDownList
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object edtEECathEDCode: TevDBLookupCombo
      Left = 193
      Top = 85
      Width = 138
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'E/D Code'#9#9
        'DESCRIPTION'#9'40'#9'Description'#9#9)
      LookupTable = DM_CL_E_DS.CL_E_DS
      LookupField = 'CL_E_DS_NBR'
      Style = csDropDownList
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object edtERContEDCode: TevDBLookupCombo
      Left = 193
      Top = 110
      Width = 138
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'E/D Code'#9#9
        'DESCRIPTION'#9'40'#9'Description'#9#9)
      LookupTable = DM_CL_E_DS.CL_E_DS
      LookupField = 'CL_E_DS_NBR'
      Style = csDropDownList
      TabOrder = 4
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object edtOptERContEDCode: TevDBLookupCombo
      Left = 193
      Top = 135
      Width = 138
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'E/D Code'#9#9
        'DESCRIPTION'#9'40'#9'Description'#9#9)
      LookupTable = DM_CL_E_DS.CL_E_DS
      LookupField = 'CL_E_DS_NBR'
      Style = csDropDownList
      TabOrder = 5
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 328
    Top = 176
  end
end
