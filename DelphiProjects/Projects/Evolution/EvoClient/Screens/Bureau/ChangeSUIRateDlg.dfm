object dglChangeSUIRate: TdglChangeSUIRate
  Left = 533
  Top = 369
  BorderStyle = bsDialog
  Caption = 'Change SUI Rate'
  ClientHeight = 162
  ClientWidth = 321
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    321
    162)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 16
    Top = 16
    Width = 18
    Height = 13
    Caption = 'SUI'
  end
  object evLabel2: TevLabel
    Left = 16
    Top = 56
    Width = 23
    Height = 13
    Caption = 'Rate'
  end
  object evLabel3: TevLabel
    Left = 16
    Top = 96
    Width = 42
    Height = 13
    Caption = 'Effective'
  end
  object OKBtn: TButton
    Left = 79
    Top = 128
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 159
    Top = 128
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object evDBLookupCombo1: TevDBLookupCombo
    Left = 72
    Top = 12
    Width = 225
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
    LookupTable = DM_SY_SUI.SY_SUI
    LookupField = 'SY_SUI_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = evDBLookupCombo1Change
  end
  object evEdit1: TevEdit
    Left = 72
    Top = 53
    Width = 97
    Height = 21
    TabOrder = 3
    Text = '0'
  end
  object evComboBox1: TevComboBox
    Left = 72
    Top = 93
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 4
    Text = '1st quarter'
    Items.Strings = (
      '1st quarter'
      '2nd quarter'
      '3rd quarter'
      '4th quarter')
  end
  object evSpinEdit1: TevSpinEdit
    Left = 224
    Top = 93
    Width = 73
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object evRateAsAdCheck: TevCheckBox
    Left = 176
    Top = 55
    Width = 137
    Height = 17
    Caption = 'Use Rate as Adjustment'
    TabOrder = 6
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 272
    Top = 120
  end
end
