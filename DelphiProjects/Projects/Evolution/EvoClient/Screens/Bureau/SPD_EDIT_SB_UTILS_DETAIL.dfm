object EDIT_SB_UTILS_DETAIL: TEDIT_SB_UTILS_DETAIL
  Left = 397
  Top = 225
  Width = 627
  Height = 528
  Caption = 'EDIT_SB_UTILS_DETAIL'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 611
    Height = 490
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'ACH KEY Details'
      OnShow = TabSheet1Show
      object evGroupBox1: TevGroupBox
        Left = 8
        Top = 0
        Width = 257
        Height = 201
        Caption = 'ACH Key Fix'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object evlbState: TevLabel
          Left = 24
          Top = 33
          Width = 25
          Height = 13
          Caption = 'State'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evlbDepFreq: TevLabel
          Left = 24
          Top = 74
          Width = 89
          Height = 13
          Caption = 'Deposit Frequency'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evlbBDate: TevLabel
          Left = 24
          Top = 112
          Width = 101
          Height = 13
          Caption = 'Begining Check Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evlbEDate: TevLabel
          Left = 144
          Top = 112
          Width = 93
          Height = 13
          Caption = 'Ending Check Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evlbACHDate: TevLabel
          Left = 24
          Top = 152
          Width = 48
          Height = 13
          Caption = 'ACH Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evlbKeySuffix: TevLabel
          Left = 144
          Top = 152
          Width = 47
          Height = 13
          Caption = 'Key Suffix'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel7: TevLabel
          Left = 120
          Top = 168
          Width = 11
          Height = 20
          Caption = '+'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evedState: TevEdit
          Left = 24
          Top = 48
          Width = 25
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object evdtBDate: TevDBDateTimePicker
          Left = 24
          Top = 128
          Width = 89
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 4
        end
        object evdtEDate: TevDBDateTimePicker
          Left = 144
          Top = 128
          Width = 89
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 5
        end
        object evdtACHDate: TevDBDateTimePicker
          Left = 24
          Top = 168
          Width = 89
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowButton = True
          TabOrder = 6
        end
        object evedKeySuffix: TevEdit
          Left = 144
          Top = 168
          Width = 25
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object evdcDepFreq: TevDBComboBox
          Left = 24
          Top = 88
          Width = 209
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 0
          ParentFont = False
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 3
          UnboundDataType = wwDefault
        end
        object CheckBox1: TCheckBox
          Left = 128
          Top = 48
          Width = 105
          Height = 17
          Caption = 'Use Check Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object cbAndSui: TevCheckBox
          Left = 56
          Top = 48
          Width = 65
          Height = 17
          Caption = 'and SUI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object Button1: TButton
        Left = 192
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Done'
        TabOrder = 1
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 8
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 2
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Extend Calendar'
      ImageIndex = 2
      object Label2: TLabel
        Left = 8
        Top = 51
        Width = 151
        Height = 13
        Caption = 'In case of holiday or Weekend?'
      end
      object evlbNbrMonths: TevLabel
        Left = 10
        Top = 11
        Width = 87
        Height = 13
        Caption = 'Number of Months'
      end
      object Label6: TLabel
        Left = 8
        Top = 91
        Width = 151
        Height = 13
        Caption = 'In case of holiday or Weekend?'
      end
      object Label7: TLabel
        Left = 8
        Top = 131
        Width = 151
        Height = 13
        Caption = 'In case of holiday or Weekend?'
      end
      object evcbAdjustmentCheck: TevDBComboBox
        Left = 8
        Top = 67
        Width = 170
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Forwards'
          'Backwards'
          'Keep')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object Button5: TButton
        Left = 8
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
      object Button6: TButton
        Left = 192
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 2
      end
      object evseCycles: TevSpinEdit
        Left = 10
        Top = 26
        Width = 57
        Height = 22
        MaxValue = 48
        MinValue = 1
        TabOrder = 3
        Value = 12
      end
      object evcbAdjustmentCallIn: TevDBComboBox
        Left = 8
        Top = 107
        Width = 170
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Forwards'
          'Backwards'
          'Keep')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
      end
      object evcbAdjustmentDelivery: TevDBComboBox
        Left = 8
        Top = 147
        Width = 170
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Forwards'
          'Backwards'
          'Keep')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
    end
    object tsErrorLog: TTabSheet
      Caption = 'Log'
      ImageIndex = 4
      object evreLog: TevRichEdit
        Left = -1
        Top = 0
        Width = 375
        Height = 345
        PlainText = True
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Button9: TButton
        Left = 24
        Top = 360
        Width = 75
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
      object Button10: TButton
        Left = 260
        Top = 360
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 2
      end
      object Button11: TButton
        Left = 145
        Top = 360
        Width = 75
        Height = 25
        Caption = 'Save Log'
        TabOrder = 3
        OnClick = Button11Click
      end
    end
    object tsNFSCreditHold: TTabSheet
      Caption = 'NSF and Credit Hold'
      ImageIndex = 3
      object evLabel4: TevLabel
        Left = 152
        Top = 16
        Width = 63
        Height = 13
        Caption = 'Check Date?'
      end
      object evLabel6: TevLabel
        Left = 8
        Top = 72
        Width = 52
        Height = 13
        Caption = 'Credit Hold'
      end
      object evLabel8: TevLabel
        Left = 8
        Top = 120
        Width = 65
        Height = 13
        Caption = 'Liabilty Status'
      end
      object evrgCreditHold: TevRadioGroup
        Left = 8
        Top = 16
        Width = 113
        Height = 41
        Caption = 'Credit Hold'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'On'
          'Off')
        TabOrder = 0
        OnClick = evrgCreditHoldClick
      end
      object evdtCheckDate: TevDateTimePicker
        Left = 152
        Top = 32
        Width = 105
        Height = 21
        Date = 38636.000000000000000000
        Time = 38636.000000000000000000
        TabOrder = 1
      end
      object evcbCreditHold: TevDBComboBox
        Left = 8
        Top = 88
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object evcbLiabStatus: TevDBComboBox
        Left = 8
        Top = 136
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        Enabled = False
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object Button12: TButton
        Left = 8
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 4
      end
      object Button13: TButton
        Left = 192
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 5
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 201
    Top = 25
  end
end
