object UpdateEELocals: TUpdateEELocals
  Left = 455
  Top = 313
  BorderStyle = bsDialog
  ClientHeight = 378
  ClientWidth = 396
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel4: TevLabel
    Left = 16
    Top = 45
    Width = 77
    Height = 13
    Caption = 'Effective Date   '
  end
  object evLabel2: TevLabel
    Left = 16
    Top = 18
    Width = 25
    Height = 13
    Caption = 'State'
  end
  object OKBtn: TButton
    Left = 67
    Top = 327
    Width = 90
    Height = 25
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 195
    Top = 327
    Width = 90
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object pgUpdateEELocals: TevPageControl
    Left = 14
    Top = 73
    Width = 367
    Height = 232
    ActivePage = TabSheet1
    TabOrder = 2
    OnChange = pgUpdateEELocalsChange
    object TabSheet1: TTabSheet
      Caption = 'Set Tax Code or Override Rate to Null '
      object evLabel1: TevLabel
        Left = 8
        Top = 8
        Width = 83
        Height = 13
        Caption = 'Local Tax Type   '
      end
      object cbLocalTax: TevComboBox
        Left = 8
        Top = 32
        Width = 289
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbLocalTaxChange
      end
      object rgUpdateField: TevRadioGroup
        Left = 8
        Top = 63
        Width = 342
        Height = 130
        Items.Strings = (
          'Set the Tax Code to Null on the EE Local Taxes.'
          'Set the Override Rate to Null on the EE Local Taxes.'
          'Set Tax Code and Override Rate to Null on the EE Local Taxes.')
        TabOrder = 1
        OnClick = cbLocalTaxChange
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Set Local Enabled and Deduct '
      ImageIndex = 1
      object evLabel3: TevLabel
        Left = 8
        Top = 16
        Width = 337
        Height = 13
        Caption = 
          'Update any Local Non-Residential Taxes attached to the employees' +
          '     '
      end
      object evLabel5: TevLabel
        Left = 40
        Top = 89
        Width = 3
        Height = 13
      end
      object rgUpdateField2: TevRadioButton
        Left = 16
        Top = 56
        Width = 313
        Height = 17
        Caption = 'Set Local Enabled to No and set Deduct to Never.'
        TabOrder = 0
        OnClick = rgUpdateField2Click
      end
    end
  end
  object dEffectiveDate: TevDateTimePicker
    Left = 152
    Top = 37
    Width = 119
    Height = 21
    Date = 40909.000000000000000000
    Time = 40909.000000000000000000
    TabOrder = 3
  end
  object edState: TevDBLookupCombo
    Left = 152
    Top = 10
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'STATE'#9'2'#9'STATE'#9'F'
      'NAME'#9'20'#9'Name'#9'F')
    LookupTable = DM_SY_STATES.SY_STATES
    LookupField = 'STATE'
    Style = csDropDownList
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 312
    Top = 24
  end
end
