unit SelectComboChoicesDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SDDClasses, SDataDictbureau, SDataStructure, wwdblook,
  isBaseClasses,
  ISBasicClasses,  EvTypes, EvUtils, DBCtrls, EvConsts, EvExceptions, EvUIComponents,
  isUIwwDBLookupCombo, isUIEdit, ComCtrls, LMDCustomButton, LMDButton,
  isUILMDButton, ExtCtrls, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvUIUtils;

type
  TfrmSelectComboChoices = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    gbACAStatus: TevGroupBox;
  private
    FComboChoices: TStrings;
  public
    destructor Destroy; override;

    function GetSelectedValues: string;
    procedure PrepareCheckBoxes(const aCombo: string);
  end;


function SelectComboChoices(const aCaption, aComboChoices: string; var aChoice: string): boolean;
// returns comma-separated _ComboChoices fields values (see SFieldCodeValues)

implementation

{$R *.dfm}

function SelectComboChoices(const aCaption, aComboChoices: string; var aChoice: string): boolean;
var
  dlg: TfrmSelectComboChoices;
begin
  aChoice := '';
  dlg := TfrmSelectComboChoices.Create(nil);
  try
    dlg.Caption := aCaption;
    dlg.PrepareCheckBoxes(aComboChoices);
    Result := dlg.ShowModal = mrOk;
    if Result then
      aChoice := dlg.GetSelectedValues;
  finally
    FreeAndNil(dlg);
  end;
end;

{ TfrmSelectComboChoices }

destructor TfrmSelectComboChoices.Destroy;
begin
  if Assigned(FComboChoices) then
    FreeAndNil(FComboChoices);
  inherited;
end;

function TfrmSelectComboChoices.GetSelectedValues: string;
var
  i: integer;
  chb: TComponent;
begin
  Result := '';
  Assert(Assigned(FComboChoices));
  for i := 0 to FComboChoices.Count - 1 do
  begin
    chb := FindComponent('chb' + FComboChoices.ValueFromIndex[i]);
    if Assigned(chb) and (chb is TevCheckBox) then
      if TevCheckBox(chb).Checked then
        Result := Result + '''' + FComboChoices.ValueFromIndex[i] + ''',';
  end;
  if Result <> '' then
    Result := Copy(Result, 1, Length(Result) - 1);
end;

procedure TfrmSelectComboChoices.PrepareCheckBoxes(const aCombo: string);
var
  i: integer;
  chb: TevCheckBox;
begin
  FComboChoices := TStringList.Create;
  FComboChoices.NameValueSeparator := #9;
  FComboChoices.Text := aCombo;

  Self.Height := 135 + (FComboChoices.Count - 1) * 25;

  for i := 0 to FComboChoices.Count - 1 do
  begin
    chb := TevCheckBox.Create(Self);
    chb.Parent := gbACAStatus;
    chb.Name := 'chb' + FComboChoices.ValueFromIndex[i];
    chb.Caption := FComboChoices.Names[i];
    chb.Left := 15;
    chb.TabOrder := i;
    chb.Top := 20 + i * 25;
    chb.Width := gbACAStatus.Width - 30;
  end;
end;

end.
