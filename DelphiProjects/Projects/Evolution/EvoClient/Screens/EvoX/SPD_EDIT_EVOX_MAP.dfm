inherited EDIT_EVOX_MAP: TEDIT_EVOX_MAP
  AutoScroll = False
  AutoSize = False
  object evPanel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 38
    Align = alTop
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 338
      Top = 13
      Width = 94
      Height = 13
      Caption = 'Current map file:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object MapFilePath: TevLabel
      Left = 439
      Top = 13
      Width = 147
      Height = 13
      Caption = 'Current map file path and name'
    end
    object NewButton: TevButton
      Left = 8
      Top = 7
      Width = 75
      Height = 25
      Caption = 'New'
      TabOrder = 0
      OnClick = NewButtonClick
      Color = clBlack
      Margin = 0
    end
    object OpenButton: TevButton
      Left = 88
      Top = 7
      Width = 75
      Height = 25
      Caption = 'Open...'
      TabOrder = 1
      OnClick = OpenButtonClick
      Color = clBlack
      Margin = 0
    end
    object SaveButton: TevButton
      Left = 168
      Top = 7
      Width = 75
      Height = 25
      Caption = 'Save...'
      Enabled = False
      TabOrder = 2
      OnClick = SaveButtonClick
      Color = clBlack
      Margin = 0
    end
    object SaveAsButton: TevButton
      Left = 248
      Top = 7
      Width = 75
      Height = 25
      Caption = 'Save As...'
      Enabled = False
      TabOrder = 3
      OnClick = SaveAsButtonClick
      Color = clBlack
      Margin = 0
    end
  end
  object evPageControl1: TevPageControl [1]
    Left = 0
    Top = 38
    Width = 435
    Height = 227
    ActivePage = MappingSheet
    Align = alClient
    Enabled = False
    TabOrder = 1
    object DataSourceSheet: TTabSheet
      Caption = 'Data Source'
      object evPanel2: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel2: TevLabel
          Left = 8
          Top = 6
          Width = 144
          Height = 13
          Caption = 'Select Data Source Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DSTypesList: TComboBox
          Left = 160
          Top = 4
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 0
          OnChange = DSTypesListChange
        end
      end
      object evPanel4: TevPanel
        Left = 0
        Top = 28
        Width = 427
        Height = 171
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object DSOptions: TevPageControl
          Left = 0
          Top = 0
          Width = 427
          Height = 201
          ActivePage = CSVSheet
          Align = alTop
          TabOrder = 0
          object NoSheet: TTabSheet
            Caption = 'Unknown options'
            TabVisible = False
          end
          object CSVSheet: TTabSheet
            Caption = 'CSV sheet'
            ImageIndex = 1
            TabVisible = False
            object evGroupBox1: TevGroupBox
              Left = 4
              Top = 2
              Width = 309
              Height = 143
              Caption = ' Options '
              TabOrder = 0
              object evLabel3: TevLabel
                Left = 8
                Top = 23
                Width = 91
                Height = 13
                Caption = 'Delimiter character:'
              end
              object evLabel4: TevLabel
                Left = 8
                Top = 55
                Width = 80
                Height = 13
                Caption = 'Quote character:'
              end
              object DelimiterCharList: TComboBox
                Left = 105
                Top = 20
                Width = 65
                Height = 21
                Style = csDropDownList
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 0
                Text = 'Comma'
                OnChange = DelimiterCharListChange
                Items.Strings = (
                  'Comma'
                  'Tab')
              end
              object QuoteCharList: TComboBox
                Left = 104
                Top = 53
                Width = 97
                Height = 21
                Style = csDropDownList
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 1
                Text = 'Double quote'
                OnChange = QuoteCharListChange
                Items.Strings = (
                  'Double quote'
                  'Single quote')
              end
              object MustbeQuotedCheck: TCheckBox
                Left = 8
                Top = 83
                Width = 289
                Height = 17
                Caption = 'Each field is enclosed in quotes'
                TabOrder = 2
                OnClick = MustbeQuotedCheckClick
              end
              object FieldsInFirstLineCheck: TCheckBox
                Left = 8
                Top = 109
                Width = 281
                Height = 17
                Caption = 'First line contains field names'
                TabOrder = 3
                OnClick = FieldsInFirstLineCheckClick
              end
            end
            object evMemo1: TevMemo
              Left = 328
              Top = 8
              Width = 457
              Height = 129
              Lines.Strings = (
                '                      Definition of the CSV Format'
                ''
                
                  '   1.  Each record is located on a separate line, delimited by a' +
                  ' line'
                '       break (CRLF).  For example:'
                ''
                '       aaa,bbb,ccc CRLF'
                '       zzz,yyy,xxx CRLF'
                ''
                
                  '   2.  The last record in the file may or may not have an ending' +
                  ' line'
                '       break.  For example:'
                ''
                '       aaa,bbb,ccc CRLF'
                '       zzz,yyy,xxx'
                ''
                
                  '   3.  There maybe an optional header line appearing as the firs' +
                  't line'
                
                  '       of the file with the same format as normal record lines. ' +
                  ' This'
                
                  '       header will contain names corresponding to the fields in ' +
                  'the file'
                
                  '       and should contain the same number of fields as the recor' +
                  'ds in'
                
                  '       the rest of the file (the presence or absence of the head' +
                  'er line'
                
                  '       should be indicated via the optional "header" parameter o' +
                  'f this'
                '       MIME type).  For example:'
                ''
                '       field_name,field_name,field_name CRLF'
                '       aaa,bbb,ccc CRLF'
                '       zzz,yyy,xxx CRLF'
                ''
                ''
                
                  '   4.  Within the header and each record, there may be one or mo' +
                  're'
                
                  '       fields, separated by commas.  Each line should contain th' +
                  'e same'
                
                  '       number of fields throughout the file.  Spaces are conside' +
                  'red part'
                
                  '       of a field and should not be ignored.  The last field in ' +
                  'the'
                '       record must not be followed by a comma.  For example:'
                ''
                '       aaa,bbb,ccc'
                ''
                
                  '   5.  Each field may or may not be enclosed in double quotes (h' +
                  'owever'
                
                  '       some programs, such as Microsoft Excel, do not use double' +
                  ' quotes'
                
                  '       at all).  If fields are not enclosed with double quotes, ' +
                  'then'
                
                  '       double quotes may not appear inside the fields.  For exam' +
                  'ple:'
                ''
                '       "aaa","bbb","ccc" CRLF'
                '       zzz,yyy,xxx'
                ''
                
                  '   6.  Fields containing line breaks (CRLF), double quotes, and ' +
                  'commas'
                '       should be enclosed in double-quotes.  For example:'
                ''
                '       "aaa","b CRLF'
                '       bb","ccc" CRLF'
                '       zzz,yyy,xxx'
                ''
                
                  '   7.  If double-quotes are used to enclose fields, then a doubl' +
                  'e-quote'
                
                  '       appearing inside a field must be escaped by preceding it ' +
                  'with'
                '       another double quote.  For example:'
                ''
                '       "aaa","b""bb","ccc"')
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 1
            end
          end
          object FixedSheet: TTabSheet
            Caption = 'FixedSheet'
            ImageIndex = 2
            TabVisible = False
            object evPanel30: TevPanel
              Left = 0
              Top = 0
              Width = 301
              Height = 191
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object evPanel32: TevPanel
                Left = 0
                Top = 0
                Width = 301
                Height = 20
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object evLabel26: TevLabel
                  Left = 8
                  Top = 1
                  Width = 255
                  Height = 13
                  Caption = 
                    'List of fields in the input file. The first position in file is ' +
                    '1'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
              end
              object FixedFieldsGrid: TevDBGrid
                Left = 0
                Top = 20
                Width = 301
                Height = 171
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EVOX_MAP\FixedFieldsGrid'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsFixedFields
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                ReadOnly = False
                TabOrder = 1
                TitleAlignment = taCenter
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = True
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object evPanel31: TevPanel
              Left = 301
              Top = 0
              Width = 118
              Height = 191
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object ApplyBtn: TButton
                Left = 10
                Top = 80
                Width = 105
                Height = 25
                Caption = 'Apply Changes'
                TabOrder = 0
                OnClick = ApplyBtnClick
              end
              object DBNavigator1: TDBNavigator
                Left = 10
                Top = 32
                Width = 112
                Height = 25
                DataSource = dsFixedFields
                VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
                Hints.Strings = (
                  'First record'
                  'Prior record'
                  'Next record'
                  'Last record'
                  'Insert Field'
                  'Delete Field'
                  'Edit Field'
                  'Post edit'
                  'Cancel edit'
                  'Refresh data')
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = DBNavigator1Click
              end
              object Button1: TButton
                Left = 10
                Top = 120
                Width = 105
                Height = 25
                Caption = 'Cancel Changes'
                TabOrder = 2
                OnClick = Button1Click
              end
              object evMemo2: TevMemo
                Left = 137
                Top = 14
                Width = 336
                Height = 163
                Lines.Strings = (
                  
                    '                      Definition of the Fixed-Column ASCII Forma' +
                    't'
                  ''
                  'Fixed column format means that the values for a variable are '
                  'always located in the same column.'
                  ''
                  'The values can be right next to each other or they can be '
                  'separated by one or more spaces (e.g., age and gender). The '
                  
                    'basic rule for fixed-column ASCII files is that the values for a' +
                    ' '
                  'variable must always be located in the same column.'
                  ''
                  'So all the records in such file will be the same size.')
                ReadOnly = True
                ScrollBars = ssVertical
                TabOrder = 3
              end
            end
          end
        end
        object evPanel3: TevPanel
          Left = 0
          Top = 201
          Width = 427
          Height = 617
          Align = alClient
          TabOrder = 1
          object evPanel5: TevPanel
            Left = 1
            Top = 1
            Width = 770
            Height = 64
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object evLabel5: TevLabel
              Left = 296
              Top = 11
              Width = 121
              Height = 13
              Caption = 'Current Example File:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object evLabel6: TevLabel
              Left = 136
              Top = 12
              Width = 79
              Height = 13
              Caption = 'Rows to load:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object ExampleFilePath: TevLabel
              Left = 422
              Top = 11
              Width = 166
              Height = 13
              Caption = 'Current example file path and name'
              ShowAccelChar = False
            end
            object LoadExampleButton: TevButton
              Left = 8
              Top = 7
              Width = 113
              Height = 25
              Caption = 'Load Example File...'
              Enabled = False
              TabOrder = 0
              OnClick = LoadExampleButtonClick
              Color = clBlack
              Margin = 0
            end
            object RowsToLoad: TevSpinEdit
              Left = 224
              Top = 8
              Width = 57
              Height = 22
              Enabled = False
              MaxValue = 999
              MinValue = 1
              TabOrder = 1
              Value = 1
            end
            object SaveExampleWithMapCheck: TCheckBox
              Left = 8
              Top = 40
              Width = 177
              Height = 17
              Caption = 'Save example data with map'
              Checked = True
              State = cbChecked
              TabOrder = 2
              OnClick = SaveExampleWithMapCheckClick
            end
          end
          object ExampleGrid: TevDBGrid
            Left = 1
            Top = 65
            Width = 770
            Height = 149
            DisableThemesInTitle = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EVOX_MAP\ExampleGrid'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsExample
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
            ParentFont = False
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object MappingSheet: TTabSheet
      Caption = 'Mapping'
      ImageIndex = 1
      object evPanel6: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 38
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel8: TevLabel
          Left = 128
          Top = 12
          Width = 94
          Height = 13
          Caption = 'Import Package:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CheckMapButton: TevButton
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Check map...'
          TabOrder = 0
          OnClick = CheckMapButtonClick
          Color = clBlack
          Margin = 0
        end
        object ImportPackagesList: TComboBox
          Left = 224
          Top = 8
          Width = 201
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          OnChange = ImportPackagesListChange
        end
      end
      object evPanel7: TevPanel
        Left = 0
        Top = 38
        Width = 427
        Height = 161
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object evSplitter1: TevSplitter
          Left = 225
          Top = 0
          Height = 161
          Beveled = True
        end
        object evPanel8: TevPanel
          Left = 0
          Top = 0
          Width = 225
          Height = 161
          Align = alLeft
          TabOrder = 0
          object evPanel10: TevPanel
            Left = 1
            Top = 1
            Width = 223
            Height = 38
            Align = alTop
            TabOrder = 0
            object evLabel7: TevLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Input Fields'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object MapFieldButton: TevButton
              Left = 88
              Top = 8
              Width = 75
              Height = 25
              Caption = 'Map Field'
              TabOrder = 0
              OnClick = MapFieldButtonClick
              Color = clBlack
              Margin = 0
            end
          end
          object evPanel12: TevPanel
            Left = 1
            Top = 39
            Width = 223
            Height = 121
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object InputFieldsGrid: TevDBGrid
              Left = 0
              Top = 0
              Width = 223
              Height = 121
              DisableThemesInTitle = False
              Selected.Strings = (
                'FieldName'#9'10'#9'Field Name'#9#9
                'Assigned'#9'8'#9'Assigned'#9#9
                'Example'#9'20'#9'Example'#9#9)
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_EVOX_MAP\InputFieldsGrid'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = ImputFieldsDataSource
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = clCream
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
        end
        object evPanel9: TevPanel
          Left = 228
          Top = 0
          Width = 199
          Height = 161
          Align = alClient
          TabOrder = 1
          object evPanel11: TevPanel
            Left = 1
            Top = 1
            Width = 197
            Height = 38
            Align = alTop
            TabOrder = 0
            object evLabel9: TevLabel
              Left = 8
              Top = 12
              Width = 83
              Height = 13
              Caption = 'Mapped Fields'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object UnmapFieldButton: TevButton
              Left = 104
              Top = 7
              Width = 75
              Height = 25
              Caption = 'Unmap Field'
              TabOrder = 0
              OnClick = UnmapFieldButtonClick
              Color = clBlack
              Margin = 0
            end
            object UnmapAllButton: TevButton
              Left = 184
              Top = 7
              Width = 75
              Height = 25
              Caption = 'Unmap All'
              TabOrder = 1
              OnClick = UnmapAllButtonClick
              Color = clBlack
              Margin = 0
            end
            object OpenCloseGroupBtn: TevButton
              Left = 280
              Top = 7
              Width = 75
              Height = 25
              Caption = 'Open Group'
              TabOrder = 2
              OnClick = OpenCloseGroupBtnClick
              Color = clBlack
              Margin = 0
            end
            object AddRecBtn: TevButton
              Left = 360
              Top = 7
              Width = 75
              Height = 25
              Caption = 'Add Group'
              TabOrder = 3
              OnClick = AddRecBtnClick
              Color = clBlack
              Margin = 0
            end
            object DelRecBtn: TevButton
              Left = 440
              Top = 7
              Width = 75
              Height = 25
              Caption = 'Delete Group'
              TabOrder = 4
              OnClick = DelRecBtnClick
              Color = clBlack
              Margin = 0
            end
          end
          object evPanel13: TevPanel
            Left = 1
            Top = 39
            Width = 197
            Height = 121
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object evPanel15: TevPanel
              Left = 0
              Top = 0
              Width = 197
              Height = 121
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object evSplitter2: TevSplitter
                Left = 0
                Top = -89
                Width = 197
                Height = 3
                Cursor = crVSplit
                Align = alBottom
              end
              object evPanel16: TevPanel
                Left = 0
                Top = -86
                Width = 197
                Height = 207
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 0
                object OptionsPage: TevPageControl
                  Left = 0
                  Top = 0
                  Width = 197
                  Height = 207
                  ActivePage = MappingTypeSheet
                  Align = alClient
                  TabOrder = 0
                  object MappingTypeSheet: TTabSheet
                    Caption = 'Mapping Type'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    object MapTypesPageControl: TevPageControl
                      Left = 0
                      Top = 0
                      Width = 189
                      Height = 179
                      ActivePage = DirectMapSheet
                      Align = alClient
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      Style = tsFlatButtons
                      TabOrder = 0
                      OnChange = MapTypesPageControlChange
                      object DirectMapSheet: TTabSheet
                        Caption = 'Direct'
                        object evPanel19: TevPanel
                          Left = 0
                          Top = 0
                          Width = 181
                          Height = 25
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 0
                          object evLabel16: TevLabel
                            Left = 12
                            Top = 5
                            Width = 429
                            Height = 13
                            Caption = 
                              'The simplest type of mapping. Input values will be sent to Evolu' +
                              'tion directly'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = [fsBold]
                            ParentFont = False
                          end
                        end
                        object DateTimeDirectPanel: TevPanel
                          Left = 0
                          Top = 25
                          Width = 181
                          Height = 123
                          Align = alClient
                          BevelOuter = bvNone
                          TabOrder = 1
                          object evLabel11: TevLabel
                            Left = 24
                            Top = 6
                            Width = 86
                            Height = 13
                            Caption = 'Date format string:'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                          end
                          object evLabel19: TevLabel
                            Left = 24
                            Top = 30
                            Width = 86
                            Height = 13
                            Caption = 'Time format string:'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                          end
                          object DirectDateFormat: TComboBox
                            Left = 117
                            Top = 3
                            Width = 116
                            Height = 21
                            Style = csDropDownList
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ItemHeight = 13
                            ParentFont = False
                            TabOrder = 0
                            OnChange = DirectDateFormatChange
                            Items.Strings = (
                              ''
                              'mm/dd/yyyy'
                              'dd/mm/yyyy'
                              'yyyy/mm/dd'
                              'yyyymmdd')
                          end
                          object DirectTimeFormat: TComboBox
                            Left = 117
                            Top = 27
                            Width = 116
                            Height = 21
                            Style = csDropDownList
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ItemHeight = 13
                            ParentFont = False
                            TabOrder = 1
                            OnChange = DirectTimeFormatChange
                            Items.Strings = (
                              ''
                              'hh:nn:ss'
                              'hhnnss'
                              '/hh/nn/ss')
                          end
                        end
                      end
                      object CustomFunctionMapSheet: TTabSheet
                        Caption = 'Custom Function'
                        ImageIndex = 1
                        object evPanel20: TevPanel
                          Left = 0
                          Top = 0
                          Width = 181
                          Height = 25
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 0
                          object evLabel20: TevLabel
                            Left = 12
                            Top = 5
                            Width = 418
                            Height = 13
                            Caption = 
                              'Custom function will transform your data before sending them to ' +
                              'Evolution'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = [fsBold]
                            ParentFont = False
                          end
                        end
                        object evPanel21: TevPanel
                          Left = 0
                          Top = 25
                          Width = 181
                          Height = 123
                          Align = alClient
                          BevelOuter = bvNone
                          TabOrder = 1
                          object CustomFuntionDateTimePanel: TevPanel
                            Left = 369
                            Top = 0
                            Width = 347
                            Height = 123
                            Align = alClient
                            BevelOuter = bvNone
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                            TabOrder = 0
                            object evLabel21: TevLabel
                              Left = 14
                              Top = 5
                              Width = 86
                              Height = 13
                              Caption = 'Date format string:'
                              Font.Charset = DEFAULT_CHARSET
                              Font.Color = clWindowText
                              Font.Height = -11
                              Font.Name = 'MS Sans Serif'
                              Font.Style = []
                              ParentFont = False
                            end
                            object evLabel22: TevLabel
                              Left = 14
                              Top = 29
                              Width = 86
                              Height = 13
                              Caption = 'Time format string:'
                              Font.Charset = DEFAULT_CHARSET
                              Font.Color = clWindowText
                              Font.Height = -11
                              Font.Name = 'MS Sans Serif'
                              Font.Style = []
                              ParentFont = False
                            end
                            object CustomDateFormat: TComboBox
                              Left = 107
                              Top = 2
                              Width = 116
                              Height = 21
                              Style = csDropDownList
                              Font.Charset = DEFAULT_CHARSET
                              Font.Color = clWindowText
                              Font.Height = -11
                              Font.Name = 'MS Sans Serif'
                              Font.Style = []
                              ItemHeight = 13
                              ParentFont = False
                              TabOrder = 0
                              OnChange = CustomDateFormatChange
                              Items.Strings = (
                                ''
                                'mm/dd/yyyy'
                                'dd/mm/yyyy')
                            end
                            object CustomTimeFormat: TComboBox
                              Left = 107
                              Top = 26
                              Width = 116
                              Height = 21
                              Style = csDropDownList
                              Font.Charset = DEFAULT_CHARSET
                              Font.Color = clWindowText
                              Font.Height = -11
                              Font.Name = 'MS Sans Serif'
                              Font.Style = []
                              ItemHeight = 13
                              ParentFont = False
                              TabOrder = 1
                              OnChange = CustomTimeFormatChange
                              Items.Strings = (
                                ''
                                'hh:nn:ss')
                            end
                          end
                          object evPanel23: TevPanel
                            Left = 0
                            Top = 0
                            Width = 369
                            Height = 123
                            Align = alLeft
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                            TabOrder = 1
                            object evLabel23: TevLabel
                              Left = 8
                              Top = 7
                              Width = 70
                              Height = 13
                              Caption = 'Function name'
                            end
                            object CustomFunctionList: TComboBox
                              Left = 87
                              Top = 4
                              Width = 217
                              Height = 21
                              Style = csDropDownList
                              ItemHeight = 0
                              TabOrder = 0
                              OnChange = CustomFunctionListChange
                            end
                            object FunctionDescriptionMemo: TMemo
                              Left = 8
                              Top = 33
                              Width = 345
                              Height = 84
                              BevelInner = bvNone
                              BevelKind = bkTile
                              BorderStyle = bsNone
                              Color = clBtnFace
                              Lines.Strings = (
                                'FunctionDescriptionMemo')
                              TabOrder = 1
                            end
                          end
                        end
                      end
                      object MapSheet: TTabSheet
                        Caption = 'Map'
                        ImageIndex = 2
                        object evPanel22: TevPanel
                          Left = 0
                          Top = 0
                          Width = 575
                          Height = 25
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 0
                          object evLabel24: TevLabel
                            Left = 12
                            Top = 5
                            Width = 335
                            Height = 13
                            Caption = 'Your data will be mapped before sending them to Evolution'
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -11
                            Font.Name = 'MS Sans Serif'
                            Font.Style = [fsBold]
                            ParentFont = False
                          end
                        end
                        object evPanel24: TevPanel
                          Left = 0
                          Top = 25
                          Width = 575
                          Height = 123
                          Align = alClient
                          BevelOuter = bvNone
                          Font.Charset = DEFAULT_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -11
                          Font.Name = 'MS Sans Serif'
                          Font.Style = []
                          ParentFont = False
                          TabOrder = 1
                          object evPanel25: TevPanel
                            Left = 0
                            Top = 0
                            Width = 113
                            Height = 123
                            Align = alLeft
                            BevelOuter = bvNone
                            TabOrder = 0
                            object evButton1: TevButton
                              Left = 14
                              Top = 5
                              Width = 75
                              Height = 25
                              Caption = 'Add value'
                              TabOrder = 0
                              OnClick = evButton1Click
                              Color = clBlack
                              Margin = 0
                            end
                            object evButton2: TevButton
                              Left = 14
                              Top = 32
                              Width = 75
                              Height = 25
                              Caption = 'Edit value'
                              TabOrder = 1
                              OnClick = evButton2Click
                              Color = clBlack
                              Margin = 0
                            end
                            object evButton3: TevButton
                              Left = 14
                              Top = 64
                              Width = 75
                              Height = 25
                              Caption = 'Delete value'
                              TabOrder = 2
                              OnClick = evButton3Click
                              Color = clBlack
                              Margin = 0
                            end
                          end
                          object evPanel26: TevPanel
                            Left = 113
                            Top = 0
                            Width = 462
                            Height = 123
                            Align = alClient
                            BevelOuter = bvNone
                            TabOrder = 1
                            object UserMapGrid: TevDBGrid
                              Left = 0
                              Top = 0
                              Width = 462
                              Height = 123
                              DisableThemesInTitle = False
                              Selected.Strings = (
                                'From'#9'20'#9'From'#9
                                'To'#9'20'#9'To'#9
                                'Description'#9'100'#9'Description'#9)
                              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                              IniAttributes.SectionName = 'TEDIT_EVOX_MAP\UserMapGrid'
                              IniAttributes.Delimiter = ';;'
                              ExportOptions.ExportType = wwgetSYLK
                              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                              TitleColor = clBtnFace
                              FixedCols = 0
                              ShowHorzScrollBar = True
                              Align = alClient
                              DataSource = UserMapDataSource
                              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                              TabOrder = 0
                              TitleAlignment = taLeftJustify
                              TitleFont.Charset = DEFAULT_CHARSET
                              TitleFont.Color = clWindowText
                              TitleFont.Height = -11
                              TitleFont.Name = 'MS Sans Serif'
                              TitleFont.Style = []
                              TitleLines = 1
                              OnDblClick = UserMapGridDblClick
                              PaintOptions.AlternatingRowColor = clCream
                              PaintOptions.ActiveRecordColor = clBlack
                              NoFire = False
                            end
                          end
                        end
                      end
                    end
                  end
                  object AsOfDateSheet: TTabSheet
                    Caption = 'Update As Of Date Settings'
                    ImageIndex = 1
                    object EnableAsOfDateCheck: TevCheckBox
                      Left = 8
                      Top = 2
                      Width = 257
                      Height = 17
                      Caption = 'Enable "Update As Of Date" logic for this field'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      OnClick = EnableAsOfDateCheckClick
                    end
                    object AsOFDateGb1: TevGroupBox
                      Left = 8
                      Top = 19
                      Width = 353
                      Height = 102
                      TabOrder = 1
                      object evLabel27: TevLabel
                        Left = 224
                        Top = 54
                        Width = 86
                        Height = 13
                        Caption = 'Date format string:'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                      end
                      object UseDateEditorRb: TevRadioButton
                        Left = 7
                        Top = 12
                        Width = 193
                        Height = 17
                        Caption = 'Use date editor'#39's value'
                        Checked = True
                        TabOrder = 0
                        TabStop = True
                        OnClick = UseDateEditorRbClick
                      end
                      object UseFieldsValueRb: TevRadioButton
                        Left = 8
                        Top = 54
                        Width = 193
                        Height = 17
                        Caption = 'Use input field'#39's value'
                        TabOrder = 3
                        OnClick = UseFieldsValueRbClick
                      end
                      object AsOfDateFields: TevComboBox
                        Left = 40
                        Top = 72
                        Width = 169
                        Height = 21
                        BevelKind = bkFlat
                        Style = csDropDownList
                        ItemHeight = 0
                        TabOrder = 4
                        OnChange = AsOfDateFieldsChange
                      end
                      object AsOfDateEditor: TevDateTimePicker
                        Left = 39
                        Top = 28
                        Width = 144
                        Height = 21
                        Date = 40028.000000000000000000
                        Time = 40028.000000000000000000
                        TabOrder = 1
                        OnChange = AsOfDateEditorChange
                      end
                      object AsOfdateEditorBtn: TevButton
                        Left = 192
                        Top = 24
                        Width = 17
                        Height = 25
                        Caption = '...'
                        TabOrder = 2
                        OnClick = AsOfdateEditorBtnClick
                        Color = clBlack
                        Margin = 0
                      end
                      object AsOfDateFormatsList: TComboBox
                        Left = 221
                        Top = 72
                        Width = 116
                        Height = 21
                        Style = csDropDownList
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ItemHeight = 13
                        ParentFont = False
                        TabOrder = 5
                        OnChange = AsOfDateFormatsListChange
                        Items.Strings = (
                          ''
                          'mm/dd/yyyy'
                          'dd/mm/yyyy')
                      end
                    end
                  end
                end
              end
              object evPanel17: TevPanel
                Left = 0
                Top = 0
                Width = 197
                Height = 558
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object evPanel14: TevPanel
                  Left = -43
                  Top = 0
                  Width = 240
                  Height = 558
                  Align = alRight
                  BevelOuter = bvLowered
                  TabOrder = 0
                  object evPanel35: TevPanel
                    Left = 1
                    Top = 1
                    Width = 238
                    Height = 232
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object evLabel10: TevLabel
                      Left = 10
                      Top = 3
                      Width = 121
                      Height = 13
                      Caption = 'Import Package Field'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object evLabel12: TevLabel
                      Left = 7
                      Top = 27
                      Width = 58
                      Height = 13
                      Caption = 'Intern Name'
                    end
                    object evLabel13: TevLabel
                      Left = 16
                      Top = 50
                      Width = 49
                      Height = 13
                      Caption = 'Evo Table'
                    end
                    object evLabel14: TevLabel
                      Left = 21
                      Top = 74
                      Width = 44
                      Height = 13
                      Caption = 'Evo Field'
                    end
                    object evLabel30: TevLabel
                      Left = 5
                      Top = 98
                      Width = 60
                      Height = 13
                      Caption = 'Lookup Tabl'
                    end
                    object evLabel31: TevLabel
                      Left = 5
                      Top = 122
                      Width = 61
                      Height = 13
                      Caption = 'Lookup Field'
                    end
                    object evLabel15: TevLabel
                      Left = 24
                      Top = 146
                      Width = 43
                      Height = 13
                      Caption = 'Datatype'
                    end
                    object evLabel25: TevLabel
                      Left = 46
                      Top = 169
                      Width = 20
                      Height = 13
                      Caption = 'Size'
                    end
                    object evLabel17: TevLabel
                      Left = 32
                      Top = 209
                      Width = 34
                      Height = 13
                      Caption = 'Default'
                    end
                    object ExchangeFieldEdit: TEdit
                      Left = 69
                      Top = 23
                      Width = 164
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 0
                    end
                    object EvoTableEdit: TEdit
                      Left = 69
                      Top = 47
                      Width = 164
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 1
                    end
                    object EvoFieldEdit: TEdit
                      Left = 69
                      Top = 71
                      Width = 164
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 2
                    end
                    object LookupTableEdit: TEdit
                      Left = 69
                      Top = 95
                      Width = 164
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 3
                    end
                    object LookupFieldEdit: TEdit
                      Left = 69
                      Top = 119
                      Width = 164
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 4
                    end
                    object PackageFieldDataTypeEdit: TEdit
                      Left = 69
                      Top = 142
                      Width = 116
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 5
                    end
                    object PackageFieldSizeEdit: TEdit
                      Left = 69
                      Top = 166
                      Width = 60
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 6
                    end
                    object RequiredByDDCheck: TCheckBox
                      Left = 69
                      Top = 188
                      Width = 164
                      Height = 17
                      Caption = 'Required by DataDictionary'
                      Enabled = False
                      TabOrder = 7
                    end
                    object PackageFieldDefaultEdit: TEdit
                      Left = 69
                      Top = 203
                      Width = 116
                      Height = 21
                      Color = clBtnFace
                      ReadOnly = True
                      TabOrder = 8
                    end
                  end
                  object evPanel36: TevPanel
                    Left = 1
                    Top = 233
                    Width = 238
                    Height = 324
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object evPanel37: TevPanel
                      Left = 0
                      Top = 156
                      Width = 238
                      Height = 168
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object PackageFieldDescription: TMemo
                        Left = 0
                        Top = 0
                        Width = 238
                        Height = 168
                        Align = alClient
                        BevelInner = bvNone
                        BevelKind = bkTile
                        BorderStyle = bsNone
                        Color = clBtnFace
                        ReadOnly = True
                        TabOrder = 0
                      end
                    end
                    object evPanel38: TevPanel
                      Left = 0
                      Top = 0
                      Width = 238
                      Height = 156
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 1
                      object evLabel18: TevLabel
                        Left = 56
                        Top = -2
                        Width = 63
                        Height = 13
                        Caption = 'List of Values'
                      end
                      object evLabel32: TevLabel
                        Left = 56
                        Top = 138
                        Width = 53
                        Height = 13
                        Caption = 'Description'
                      end
                      object PackageFieldLOV: TListBox
                        Left = 1
                        Top = 15
                        Width = 236
                        Height = 121
                        Color = clBtnFace
                        ItemHeight = 13
                        TabOrder = 0
                      end
                    end
                  end
                end
                object evPanel18: TevPanel
                  Left = 0
                  Top = 0
                  Width = 492
                  Height = 558
                  Align = alClient
                  TabOrder = 1
                  object MappedFieldsgrid: TevDBGrid
                    Left = 1
                    Top = 1
                    Width = 490
                    Height = 556
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'InputField'#9'17'#9'Input Field'#9'F'
                      'Example'#9'17'#9'Example'#9'F'
                      'MappingType'#9'13'#9'Mapping Type'#9'F'
                      'AsOfDate'#9'13'#9'As Of Date'#9'F'
                      'DisplayName'#9'18'#9'Exchange Field'#9'F')
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_EVOX_MAP\MappedFieldsgrid'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    OnRowChanged = MappedFieldsgridRowChanged
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = MappedFieldsDataSource
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
                    TabOrder = 0
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    OnCalcCellColors = MappedFieldsgridCalcCellColors
                    OnDblClick = MappedFieldsgridDblClick
                    PaintOptions.AlternatingRowColor = clCream
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
            end
          end
        end
      end
    end
    object MapSettingsSheet: TTabSheet
      Caption = 'Map Settings'
      ImageIndex = 3
      object evPanel28: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 199
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object evGroupBox2: TevGroupBox
          Left = 8
          Top = 8
          Width = 457
          Height = 129
          Caption = ' How to handle null values for updating existing records '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object evLabel28: TevLabel
            Left = 40
            Top = 96
            Width = 91
            Height = 13
            Caption = 'Null values pattern:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel29: TevLabel
            Left = 240
            Top = 96
            Width = 94
            Height = 13
            Caption = '(no spaces allowed)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object IgnoreNullRb: TevRadioButton
            Left = 16
            Top = 24
            Width = 401
            Height = 17
            Caption = 
              'Ignore Null Values (EvoExchange should ignore any fields with nu' +
              'll values)'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TabStop = True
            OnClick = IgnoreNullRbClick
          end
          object ImportNullsRb: TevRadioButton
            Left = 16
            Top = 48
            Width = 433
            Height = 17
            Caption = 
              'Import Null Values (EvoExchange should import any null values in' +
              'to the mapped fields)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = ImportNullsRbClick
          end
          object ImportNullPatternRb: TevRadioButton
            Left = 16
            Top = 72
            Width = 217
            Height = 17
            Caption = 'Import Null Values using user'#39's pattern '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = ImportNullPatternRbClick
          end
          object NullValuePatternEdit: TevEdit
            Left = 136
            Top = 94
            Width = 97
            Height = 21
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            Text = 'NULL'
            OnChange = NullValuePatternEditChange
          end
        end
        object gbGenericPayrollImportOptions: TevGroupBox
          Left = 8
          Top = 144
          Width = 457
          Height = 176
          Caption = ' Generic Payroll Import Options '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object CreateAutoCheckCheckBox: TevCheckBox
            Left = 16
            Top = 21
            Width = 185
            Height = 17
            Caption = 'Auto-create checks'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = CreateAutoCheckCheckBoxClick
          end
          object ParamsGroupBox: TevGroupBox
            Left = 16
            Top = 37
            Width = 331
            Height = 129
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object rgrpPaySalary: TevRadioGroup
              Left = 11
              Top = 16
              Width = 150
              Height = 41
              Caption = ' Pay Salary '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 0
              OnClick = rgrpPaySalaryClick
            end
            object rgrpPayHours: TevRadioGroup
              Left = 170
              Top = 16
              Width = 150
              Height = 41
              Caption = ' Pay Standard Hours '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 1
              OnClick = rgrpPaySalaryClick
            end
            object rgrpLoadDefaults: TevRadioGroup
              Left = 11
              Top = 72
              Width = 150
              Height = 41
              Caption = ' Load Payroll Defaults '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 2
              OnClick = rgrpPaySalaryClick
            end
            object rgrpIncludeTOA: TevRadioGroup
              Left = 170
              Top = 72
              Width = 150
              Height = 41
              Caption = ' Include Time Off Requests '
              Columns = 2
              ItemIndex = 1
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 3
              OnClick = rgrpPaySalaryClick
            end
          end
        end
      end
    end
    object ErrorLogSheet: TTabSheet
      Caption = 'Import'
      ImageIndex = 2
      object evPanel27: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object ImportNowButton: TevButton
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Import Now...'
          TabOrder = 0
          OnClick = ImportNowButtonClick
          Color = clBlack
          Margin = 0
        end
        object SaveLogBtn: TevButton
          Left = 88
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Save Log file...'
          Enabled = False
          TabOrder = 1
          OnClick = SaveLogBtnClick
          Color = clBlack
          Margin = 0
        end
      end
      object evPanel29: TevPanel
        Left = 0
        Top = 41
        Width = 435
        Height = 160
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object evSplitter3: TevSplitter
          Left = 0
          Top = 77
          Width = 435
          Height = 3
          Cursor = crVSplit
          Align = alBottom
        end
        object evPanel33: TevPanel
          Left = 0
          Top = 0
          Width = 435
          Height = 77
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object ErrorsLogGrid: TevDBGrid
            Left = 0
            Top = 0
            Width = 435
            Height = 77
            DisableThemesInTitle = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EVOX_MAP\ErrorsLogGrid'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnRowChanged = ErrorsLogGridRowChanged
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = ErrorsLogDataSource
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            OnCalcCellColors = ErrorsLogGridCalcCellColors
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object evPanel34: TevPanel
          Left = 0
          Top = 80
          Width = 435
          Height = 80
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object ErrorDetailsmemo: TevMemo
            Left = 0
            Top = 0
            Width = 435
            Height = 80
            Align = alClient
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 653
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    Left = 686
    Top = 34
  end
  inherited wwdsList: TevDataSource
    Left = 610
    Top = 34
  end
  object MapFileOpenDlg: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'Evolution Map files (*.xml)|*.xml'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Open map file'
    Left = 528
    Top = 40
  end
  object dsExample: TevDataSource
    Left = 490
    Top = 34
  end
  object ExampleFileDlg: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Open example file'
    Left = 568
    Top = 40
  end
  object ImputFieldsDataSource: TevDataSource
    Left = 456
    Top = 32
  end
  object MapFileSaveDlg: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'Evolution map files (*.xml)|*.xml'
    Left = 528
    Top = 72
  end
  object MappedFieldsDataSource: TevDataSource
    Left = 416
    Top = 32
  end
  object UserMapDataSource: TevDataSource
    Left = 384
    Top = 32
  end
  object ErrorsLogDataSource: TDataSource
    Left = 344
    Top = 32
  end
  object ImportDataDialog: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Open input file'
    Left = 256
    Top = 40
  end
  object dsFixedFields: TevDataSource
    Left = 768
    Top = 40
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'log'
    Filter = 'Log files (*.log)|*.log'
    Left = 568
    Top = 72
  end
end
