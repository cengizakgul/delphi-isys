unit SPD_EDIT_EVOX_MAP_VALUE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TEDIT_EVOX_MAP_VALUE = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    FromEdit: TEdit;
    ToEdit: TEdit;
    Button1: TButton;
    Button2: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


  function ShowValueDialog(const ACaption: String; var AFrom: String; var ATo: String; ADoNotEditTo: boolean): boolean;

implementation

{$R *.dfm}

var
  MyForm: TEDIT_EVOX_MAP_VALUE;

function ShowValueDialog(const ACaption: String; var AFrom: String; var ATo: String; ADoNotEditTo: boolean): boolean;
begin
  Result := false;
  MyForm := TEDIT_EVOX_MAP_VALUE.Create(nil);
  try
    MyForm.Caption := ACaption;
    MyForm.FromEdit.Text := AFrom;
    MyForm.ToEdit.Text := ATo;

    if ADoNotEditTo then
      MyForm.ToEdit.Enabled := false
    else
      MyForm.ToEdit.Enabled := true;

    MyForm.ShowModal;
    if MyForm.ModalResult = mrOk then
    begin
      AFrom := MyForm.FromEdit.Text;
      ATo := MyForm.ToEdit.Text;
      Result := true;
    end;
  finally
    FreeAndnil(MyForm);
  end;
end;

end.
