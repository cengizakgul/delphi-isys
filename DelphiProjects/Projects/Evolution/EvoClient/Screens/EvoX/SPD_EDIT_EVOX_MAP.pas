unit SPD_EDIT_EVOX_MAP;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, ISBasicClasses,  Buttons, EvTypes, EvExceptions,
  DB, Wwdatsrc, EvCommonInterfaces, EvContext, isBaseClasses, StdCtrls, EvUtils,
  ExtCtrls, EvExchangeMapFileExt, EvStreamUtils, ComCtrls, Mask, wwdbedit,
  EvDataSet,DBCtrls, EvExchangeConsts, ISBasicUtils, Grids,
  Wwdbigrd, Wwdbgrid, Spin, kbmMemTable, ISKbmMemDataSet, SDataStructure,
  ISDataAccessComponents, EvDataAccessComponents, DBGrids, SPD_EVOX_SELECT_COMPANY,
  isTypes, isLogFileDataSet, DateUtils, EvExchangeResultLog, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, LMDCustomButton, LMDButton, isUILMDButton;

type
  TOpenedRecord = record
    Name: string;
    IsGroup: boolean;
    RecNo: integer;
  end;

  TEDIT_EVOX_MAP = class(TFrameEntry)
    evPanel1: TevPanel;
    NewButton: TevButton;
    OpenButton: TevButton;
    SaveButton: TevButton;
    evLabel1: TevLabel;
    MapFilePath: TevLabel;
    SaveAsButton: TevButton;
    MapFileOpenDlg: TOpenDialog;
    evPageControl1: TevPageControl;
    DataSourceSheet: TTabSheet;
    MappingSheet: TTabSheet;
    evPanel2: TevPanel;
    evLabel2: TevLabel;
    evPanel4: TevPanel;
    DSOptions: TevPageControl;
    NoSheet: TTabSheet;
    CSVSheet: TTabSheet;
    evGroupBox1: TevGroupBox;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evMemo1: TevMemo;
    DSTypesList: TComboBox;
    DelimiterCharList: TComboBox;
    QuoteCharList: TComboBox;
    MustbeQuotedCheck: TCheckBox;
    FieldsInFirstLineCheck: TCheckBox;
    dsExample: TevDataSource;
    ExampleFileDlg: TOpenDialog;
    evPanel3: TevPanel;
    evPanel5: TevPanel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    ExampleFilePath: TevLabel;
    LoadExampleButton: TevButton;
    RowsToLoad: TevSpinEdit;
    ExampleGrid: TevDBGrid;
    SaveExampleWithMapCheck: TCheckBox;
    evPanel6: TevPanel;
    CheckMapButton: TevButton;
    evPanel7: TevPanel;
    evPanel8: TevPanel;
    evLabel8: TevLabel;
    ImportPackagesList: TComboBox;
    evPanel10: TevPanel;
    evLabel7: TevLabel;
    evPanel12: TevPanel;
    InputFieldsGrid: TevDBGrid;
    ImputFieldsDataSource: TevDataSource;
    evSplitter1: TevSplitter;
    evPanel9: TevPanel;
    evPanel11: TevPanel;
    evLabel9: TevLabel;
    UnmapFieldButton: TevButton;
    MapFieldButton: TevButton;
    UnmapAllButton: TevButton;
    ErrorLogSheet: TTabSheet;
    MapFileSaveDlg: TSaveDialog;
    MappedFieldsDataSource: TevDataSource;
    evPanel13: TevPanel;
    evPanel15: TevPanel;
    evPanel16: TevPanel;
    evSplitter2: TevSplitter;
    evPanel17: TevPanel;
    evPanel14: TevPanel;
    evPanel18: TevPanel;
    MappedFieldsgrid: TevDBGrid;
    UserMapDataSource: TevDataSource;
    ErrorsLogDataSource: TDataSource;
    ImportDataDialog: TOpenDialog;
    evPanel27: TevPanel;
    ImportNowButton: TevButton;
    OpenCloseGroupBtn: TevButton;
    AddRecBtn: TevButton;
    DelRecBtn: TevButton;
    FixedSheet: TTabSheet;
    evPanel30: TevPanel;
    dsFixedFields: TevDataSource;
    evPanel31: TevPanel;
    ApplyBtn: TButton;
    DBNavigator1: TDBNavigator;
    evPanel32: TevPanel;
    FixedFieldsGrid: TevDBGrid;
    evLabel26: TevLabel;
    Button1: TButton;
    evMemo2: TevMemo;
    OptionsPage: TevPageControl;
    MappingTypeSheet: TTabSheet;
    AsOfDateSheet: TTabSheet;
    MapTypesPageControl: TevPageControl;
    DirectMapSheet: TTabSheet;
    evPanel19: TevPanel;
    evLabel16: TevLabel;
    DateTimeDirectPanel: TevPanel;
    evLabel11: TevLabel;
    evLabel19: TevLabel;
    DirectDateFormat: TComboBox;
    DirectTimeFormat: TComboBox;
    CustomFunctionMapSheet: TTabSheet;
    evPanel20: TevPanel;
    evLabel20: TevLabel;
    evPanel21: TevPanel;
    CustomFuntionDateTimePanel: TevPanel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    CustomDateFormat: TComboBox;
    CustomTimeFormat: TComboBox;
    evPanel23: TevPanel;
    evLabel23: TevLabel;
    CustomFunctionList: TComboBox;
    FunctionDescriptionMemo: TMemo;
    MapSheet: TTabSheet;
    evPanel22: TevPanel;
    evLabel24: TevLabel;
    evPanel24: TevPanel;
    evPanel25: TevPanel;
    evButton1: TevButton;
    evButton2: TevButton;
    evButton3: TevButton;
    evPanel26: TevPanel;
    UserMapGrid: TevDBGrid;
    EnableAsOfDateCheck: TevCheckBox;
    AsOFDateGb1: TevGroupBox;
    evLabel27: TevLabel;
    UseDateEditorRb: TevRadioButton;
    UseFieldsValueRb: TevRadioButton;
    AsOfDateFields: TevComboBox;
    AsOfDateEditor: TevDateTimePicker;
    AsOfdateEditorBtn: TevButton;
    AsOfDateFormatsList: TComboBox;
    MapSettingsSheet: TTabSheet;
    evPanel28: TevPanel;
    evGroupBox2: TevGroupBox;
    IgnoreNullRb: TevRadioButton;
    ImportNullsRb: TevRadioButton;
    ImportNullPatternRb: TevRadioButton;
    evLabel28: TevLabel;
    NullValuePatternEdit: TevEdit;
    evLabel29: TevLabel;
    evPanel29: TevPanel;
    evPanel33: TevPanel;
    ErrorsLogGrid: TevDBGrid;
    evSplitter3: TevSplitter;
    evPanel34: TevPanel;
    ErrorDetailsmemo: TevMemo;
    SaveLogBtn: TevButton;
    SaveLogDlg: TSaveDialog;
    gbGenericPayrollImportOptions: TevGroupBox;
    CreateAutoCheckCheckBox: TevCheckBox;
    ParamsGroupBox: TevGroupBox;
    rgrpPaySalary: TevRadioGroup;
    rgrpPayHours: TevRadioGroup;
    rgrpLoadDefaults: TevRadioGroup;
    rgrpIncludeTOA: TevRadioGroup;
    evPanel35: TevPanel;
    evLabel10: TevLabel;
    evLabel12: TevLabel;
    ExchangeFieldEdit: TEdit;
    evLabel13: TevLabel;
    EvoTableEdit: TEdit;
    evLabel14: TevLabel;
    EvoFieldEdit: TEdit;
    evLabel30: TevLabel;
    LookupTableEdit: TEdit;
    evLabel31: TevLabel;
    LookupFieldEdit: TEdit;
    evLabel15: TevLabel;
    PackageFieldDataTypeEdit: TEdit;
    evLabel25: TevLabel;
    PackageFieldSizeEdit: TEdit;
    RequiredByDDCheck: TCheckBox;
    evLabel17: TevLabel;
    PackageFieldDefaultEdit: TEdit;
    evPanel36: TevPanel;
    evPanel37: TevPanel;
    PackageFieldDescription: TMemo;
    evPanel38: TevPanel;
    PackageFieldLOV: TListBox;
    evLabel18: TevLabel;
    evLabel32: TevLabel;
    procedure NewButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure SaveAsButtonClick(Sender: TObject);
    procedure OpenButtonClick(Sender: TObject);
    procedure DSTypesListChange(Sender: TObject);
    procedure LoadExampleButtonClick(Sender: TObject);
    procedure DelimiterCharListChange(Sender: TObject);
    procedure QuoteCharListChange(Sender: TObject);
    procedure MustbeQuotedCheckClick(Sender: TObject);
    procedure FieldsInFirstLineCheckClick(Sender: TObject);
    procedure ImportPackagesListChange(Sender: TObject);
    procedure MappedFieldsgridRowChanged(Sender: TObject);
    procedure UnmapFieldButtonClick(Sender: TObject);
    procedure UnmapAllButtonClick(Sender: TObject);
    procedure MapFieldButtonClick(Sender: TObject);
    procedure AutoMapButtonClick(Sender: TObject);
    procedure DirectDateFormatChange(Sender: TObject);
    procedure DirectTimeFormatChange(Sender: TObject);
    procedure CustomDateFormatChange(Sender: TObject);
    procedure CustomTimeFormatChange(Sender: TObject);
    procedure MapTypesPageControlChange(Sender: TObject);
    procedure CustomFunctionListChange(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure evButton2Click(Sender: TObject);
    procedure evButton3Click(Sender: TObject);
    procedure UserMapGridDblClick(Sender: TObject);
    procedure CheckMapButtonClick(Sender: TObject);
    procedure ImportNowButtonClick(Sender: TObject);
    procedure OpenCloseGroupBtnClick(Sender: TObject);
    procedure AddRecBtnClick(Sender: TObject);
    procedure DelRecBtnClick(Sender: TObject);
    procedure MappedFieldsgridDblClick(Sender: TObject);
    procedure AddFixedFieldBtnClick(Sender: TObject);
    procedure DeleteFixedFieldBtnClick(Sender: TObject);
    procedure ApplyBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure MappedFieldsgridCalcCellColors(Sender: TObject;
      Field: TField; State: TGridDrawState; Highlight: Boolean;
      AFont: TFont; ABrush: TBrush);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure EnableAsOfDateCheckClick(Sender: TObject);
    procedure UseDateEditorRbClick(Sender: TObject);
    procedure UseFieldsValueRbClick(Sender: TObject);
    procedure AsOfDateFieldsChange(Sender: TObject);
    procedure AsOfDateEditorChange(Sender: TObject);
    procedure AsOfDateFormatsListChange(Sender: TObject);
    procedure AsOfdateEditorBtnClick(Sender: TObject);
    procedure IgnoreNullRbClick(Sender: TObject);
    procedure ImportNullsRbClick(Sender: TObject);
    procedure ImportNullPatternRbClick(Sender: TObject);
    procedure NullValuePatternEditChange(Sender: TObject);
    procedure ErrorsLogGridRowChanged(Sender: TObject);
    procedure SaveLogBtnClick(Sender: TObject);
    procedure ErrorsLogGridCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CreateAutoCheckCheckBoxClick(Sender: TObject);
    procedure rgrpPaySalaryClick(Sender: TObject);
    procedure SaveExampleWithMapCheckClick(Sender: TObject);
  private
    FMapFileName: String;
    FMapFile: IEvExchangeMapFile;
    FMapFileChanged: boolean;
    FOpened: TOpenedRecord;
    FPrevDSTypesListIndex: integer;
    FNoSaveAsOfDateEnabled: boolean; // stupid flag because OnClick behavoir of checkbox when program changes its value

    FPackage: IEvExchangePackage;
    FPackageList: IisStringListRO;

    FReader: IEvExchangeDataReader;
    FReaderList: IisStringListRO;

    FExampleFileName: String;
    FExampleDataSet: IevDataSet;

    FdsInputFields: IevDataSet;

    FdsMappedFields: IevDataSet;

    FdsUsermap: IevDataSet;

    FdsErrorsLog: IevDataSet;

    FFixedFieldsDataSet: IevDataSet;

    FSilentImportPackagesList: boolean;
  protected
    procedure AskToSaveIfChanged;
    procedure EnableDataSourceTabControls;
    procedure FindAndSelectValueInlist(const AComboBox: TComboBox; const AValue: String);
    procedure ErrorsFromListToDataset(const AList: IEvExchangeResultLog);
    procedure ClearErrosDataset;
    procedure ApplySecurityFunctions;

    // Map's DataSource related
    procedure FillDSTypesList;
    procedure SelectDSOptionsSheetByControl;
    procedure DSOptionsFromObjectToSheet;
    procedure DSOptionsFromSheetToObject;
    procedure SetDSTypeByMap;
    function  GetFixedFieldsDefs: IisListofValues;

    // Example related
    procedure InitExampleControls;
    procedure PrepareInputFieldsList;
    procedure LoadExample(const AFileName: String; const ARowsToLoad: integer; const ASilentMode: boolean);

    // package related
    procedure FillPackageFieldsInMap;
    procedure FillPackageTypesList;
    procedure AssignPackageForControl;
    function  GetPackageByDisplayName(const ADisplayPackageName : String): IEvExchangePackage;
    procedure FillPackageFieldInfo;
    procedure InitGenericPayrollOptions;

    // map related
    procedure CalculateAssignedTimes;
    procedure FillMappedFieldsInGrid;
    procedure MapFromObjectToScreen;
    procedure MapFromScreenToObject;
    procedure ClearMappedData;
    procedure FillMapFieldInfo;
    procedure SelectMapTypeSheet;
    procedure SaveDirectTypeInfo(const AOnChangeFlag: boolean);
    procedure SaveCustomFunctionInfo(const AOnChangeFlag: boolean);
    procedure SaveMapTypeInfo(const AOnChangeFlag: boolean);
    procedure SaveAsOFDateInfo;
    procedure ReadDirectTypeInfo;
    procedure ReadCustomFunctionInfo;
    procedure ReadMapTypeInfo;
    procedure ReadAsOfDateInfo;
    procedure FillListOfCustomFunctions;
    procedure FillCustomFunctionDescription;
    procedure FillUserMapDataset;
    function  GetExampleData(const AInputFieldName: String): Variant;
    procedure RefreshGroupsGUI;
    procedure ManageAsOfDateGUI;
    procedure SetAsOfDateValueInGrid(const AMapField: IevEchangeMapField; const ADoEditAndPost: boolean);
    procedure FillMapSettings;
    procedure SaveMapSettings;
  public
    constructor Create(AOwner: TComponent); override;

    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.dfm}

uses isDataSet, EvExchangeMapFile, EvImportMapRecord,
  rwCustomDataDictionary, rwEngineTypes, EvExchangeUtils,
  SPD_EDIT_EVOX_MAP_VALUE, EvFixedReader, EvMainboard,
  EvoXDlgAsOfDate, EvConsts, isLogFile;

const
  UnSavedFile = 'Unsaved map file';
  InputFieldSize = 50;
  FixedFieldName = 'Field Name';
  FixedFieldStart = 'Start';
  FixedFieldEnd = 'End';

  TaskGUIDField = 'TaskGuid';
  TaskNameField = 'TaskName';
  TaskStatusField = 'Status';
  TaskIntervalField ='Interval';
  TaskPatternField = 'Pattern';
  TaskFolderPathField = 'FolderPath';
  TaskMapFileField = 'Mapfile';
  TaskEMailField = 'EMail';
  TaskNotificationField = 'NotificationType';
  TaskStopOnErrorField = 'StopOnError';

type
  TEvExchangeDataSet = class(TEvDataSet)
  protected
    function  CreateVCLDataSet: TisKbmMemDataSet; override;
  end;

procedure TEDIT_EVOX_MAP.Activate;
begin
  inherited;

  FPackageList := Context.EvolutionExchange.GetEvExchangePackagesList;
  FReaderList := Context.EvolutionExchange.GetDataReadersList;

  FMapFileChanged := false;
  MapFIlePath.Caption := '';
  ExampleFilePath.Caption := '';
  FunctionDescriptionMemo.Text := '';
  RowsToLoad.Value := 1;
  FillDSTypesList;
  FillPackageTypesList;
  evPageControl1.TabIndex := 0;
  MapTypesPageControl.ActivePageIndex := 0;

  FdsInputFields := TEvDataSet.Create;
  FdsInputFields.vclDataSet.FieldDefs.Add('FieldName', ftString, 50, false);
  FdsInputFields.vclDataSet.FieldDefs.Add('Assigned', ftInteger, 0, false);
  FdsInputFields.vclDataSet.FieldDefs.Add('Example', ftString, 500, false);
  ImputFieldsDataSource.DataSet := FdsInputFields.vclDataSet;

//  FdsMappedFields := TevDataSet.Create;
  FdsMappedFields := TEvExchangeDataSet.Create;
  FdsMappedFields.vclDataSet.FieldDefs.Add('InputField', ftString, InputFieldSize, false);
  FdsMappedFields.vclDataSet.FieldDefs.Add('Example', ftString, 50, false);
  FdsMappedFields.vclDataSet.FieldDefs.Add('MappingType', ftString, 30, false);
  FdsMappedFields.vclDataSet.FieldDefs.Add('DisplayName', ftString, 50, true);
  FdsMappedFields.vclDataSet.FieldDefs.Add('ExchangeField', ftString, 50, true);
  FdsMappedFields.vclDataSet.FieldDefs.Add('ItIsGroup', ftString, 1, true);
  FdsMappedFields.vclDataSet.FieldDefs.Add('GroupRecordNumber', ftInteger, 0, true);
  FdsMappedFields.vclDataSet.FieldDefs.Add('AsOfDate', ftString, InputFieldSize, true);
  MappedFieldsDataSource.DataSet := FdsMappedFields.vclDataSet;

  FdsUserMap := TevDataSet.Create;
  FdsUserMap.vclDataSet.FieldDefs.Add('From', ftString, 500, false);
  FdsUserMap.vclDataSet.FieldDefs.Add('To', ftString, 500, false);
  FdsUserMap.vclDataSet.FieldDefs.Add('Description', ftString, 100, false);
  UserMapDataSource.DataSet := FdsUserMap.vclDataSet;

  FdsErrorsLog := TevDataSet.Create;
  FdsErrorsLog.vclDataSet.FieldDefs.Add('Error#', ftInteger, 0, true);
  FdsErrorsLog.vclDataSet.FieldDefs.Add('Error message', ftString, 1000, true);
  FdsErrorsLog.vclDataSet.FieldDefs.Add('ErrorType', ftInteger, 0, true);
  ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;

  FOpened.Name := '';
  FOpened.RecNo := -1;
  RefreshGroupsGUI;

  OptionsPage.ActivePage := MappingTypeSheet;
  ApplySecurityFunctions;
end;

procedure TEDIT_EVOX_MAP.Deactivate;
begin
  inherited;
  if ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_EDIT_EVOX_MAP') = stEnabled then
    AskToSaveIfChanged;

  if Assigned(FExampleDataSet) then
  begin
    dsExample.DataSet := nil;
    FExampleDataSet.vclDataSet.Active := false;
    FExampleDataSet := nil;
  end;

  if Assigned(FFixedFieldsDataSet) then
  begin
    dsFixedFields.DataSet := nil;
    FFixedFieldsDataSet.vclDataSet.Active := false;
    FFixedFieldsDataSet := nil;
  end;

  ImputFieldsDataSource.DataSet := nil;
  UserMapDataSource.DataSet := nil;
  MappedFieldsDataSource.DataSet := nil;
  ErrorsLogDataSource.DataSet := nil;

  FdsErrorsLog := nil;
  FdsInputFields := nil;
  FdsMappedFields := nil;
  FdsUserMap := nil;

  FMapFile := nil;
  FPackage := nil;
  FPackageList := nil;
  FReader := nil;
  FReaderList := nil;
end;

procedure TEDIT_EVOX_MAP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
end;

procedure TEDIT_EVOX_MAP.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
end;

constructor TEDIT_EVOX_MAP.Create(AOwner: TComponent);
begin
  inherited;
  FMapFile := TEvExchangeMapFileExt.Create;
end;

procedure TEDIT_EVOX_MAP.NewButtonClick(Sender: TObject);
begin
  inherited;
  AskToSaveIfChanged;

  FMapFileName := UnSavedFile;
  MapFilePath.Caption := UnSavedFile;

  FMapFile := TEvExchangeMapFileExt.Create;
  FOpened.Name := '';
  RefreshGroupsGUI;

  CheckCondition(FPackageList.Count > 0, 'No packages found. Please, check log file for more error details');

  FPackage := Context.EvolutionExchange.GetEvExchangePackage(FPackageList[0]);
  FMapFile.SetPackageInformation(FPackage);

  EnableDataSourceTabControls;

  InitExampleControls;
  MapFromObjectToScreen;

  FMapFileChanged := false;
  SaveButton.Enabled := true;
  SaveAsButton.Enabled := true;
  ApplySecurityFunctions;
end;

procedure TEDIT_EVOX_MAP.AskToSaveIfChanged;
begin
  if FMapFileChanged then
    if (EvMessage('You will lose all unsaved changes in your map file. Do you want to save them before exit?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
      SaveButton.Click;
end;

procedure TEDIT_EVOX_MAP.SaveButtonClick(Sender: TObject);
var
  S: String;
  F: TextFile;
begin
  inherited;

  if FMapFileName = UnSavedFile then
  begin
    if MapFileSaveDlg.Execute then
    begin
      if FileExists(MapFileSaveDlg.FileName) then
      begin
        if not (EvMessage('File exists. Do you want to replace it?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
          exit;
      end;
      FMapFileName := MapFileSaveDlg.FileName;
      MapFilePath.Caption := FMapFileName;
    end;
  end;

  if FMapFileName <> '' then
    FMapFileChanged := false;

  MapFromScreenToObject;

  S := FMapFile.GetXML;

  AssignFile(F, FMapFileName);
  Rewrite(F);
  try
    WriteLn(F,S);
  finally
    CloseFile(F);
  end;    
end;

procedure TEDIT_EVOX_MAP.SaveAsButtonClick(Sender: TObject);
begin
  inherited;

  if MapFileSaveDlg.Execute then
  begin
    FMapFileName := MapFileSaveDlg.FileName;
    MapFilePath.Caption := FMapFileName;
    SaveButton.Click;
  end;
end;

procedure TEDIT_EVOX_MAP.OpenButtonClick(Sender: TObject);
begin
  inherited;
  AskToSaveIfChanged;

  if MapFileOpenDlg.Execute then
  begin
    FMapFileName := MapFileOpenDlg.FileName;
    MapFilePath.Caption := FMapFileName;
    (FMapFile as IEvExchangeMapFileExt).LoadFromXMLFile(FMapFileName);
    Context.EvolutionExchange.UpgradeMapFileToLastPackageVer(FMapFile);

    FOpened.Name := '';
    RefreshGroupsGUI;

    if FMapFIle.GetPackageName <> '' then
      FPackage := Context.EvolutionExchange.GetEvExchangePackage(FMapFIle.GetPackageName)
    else
      FPackage := Context.EvolutionExchange.GetEvExchangePackage(FPackageList[0]);
    FMapFile.SetPackageInformation(FPackage);
    
    Context.EvolutionExchange.PreparePackageGroupRecords(FMapFile, FPackage);
    Context.EvolutionExchange.UpgradeDataTypesInMapFile(FMapFile, FPackage);

    EnableDataSourceTabControls;

    InitExampleControls;
    MapFromObjectToScreen;

    FMapFileChanged := false;
    SaveButton.Enabled := true;
    SaveAsButton.Enabled := true;
    ApplySecurityFunctions;
  end;
end;

procedure TEDIT_EVOX_MAP.FillDSTypesList;
var
  i: integer;
begin
  DSTypesList.Items.Clear;
  for i := 0 to FReaderlist.Count - 1 do
    DSTypesList.Items.Add(FReaderList[i]);
  if DSTypesList.Items.Count > 0 then
    DSTypesList.ItemIndex := 0;
  SelectDSOptionsSheetByControl;
end;

procedure TEDIT_EVOX_MAP.DSTypesListChange(Sender: TObject);
begin
  inherited;
  if DSTypesList.ItemIndex <> FPrevDSTypesListindex then
  begin
    if not (EvMessage('All data source options, fields, example data and ALL MAPPING WILL BE LOST, if you change data source type!' + #13#10 +
      'Do you want to continue?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
    begin
      DSTypesList.ItemIndex := FPrevDSTypesListIndex;
      Abort;
    end;
    FMapFileChanged := true;
    SelectDSOptionsSheetByControl;

    FMapFile := TEvExchangeMapFileExt.Create;
    FMapFile.SetDataSourceName(DSTypesList.Items[DSTypesList.ItemIndex]);

    FOpened.Name := '';
    RefreshGroupsGUI;
    MapFromObjectToScreen;
  end;
end;

procedure TEDIT_EVOX_MAP.SelectDSOptionsSheetByControl;
begin
  FPrevDSTypesListIndex := DSTypesList.ItemIndex;
  if DSTypesList.Items[DSTypesList.ItemIndex] = sCSVReaderName then
  begin
    FMapFile.SetDataSourceName(sCSVReaderName);
    FReader := Context.EvolutionExchange.GetDataReader(sCSVReaderName);
    DSOptions.ActivePageIndex := 1;
    DSOptionsFromObjectToSheet;
  end
  else if DSTypesList.Items[DSTypesList.ItemIndex] = sASCIIFixedReaderName then
  begin
    FMapFile.SetDataSourceName(sASCIIFixedReaderName);
    FReader := Context.EvolutionExchange.GetDataReader(sASCIIFixedReaderName);
    DSOptions.ActivePageIndex := 2;
    DSOptionsFromObjectToSheet;
  end
  else
  begin
    FMapFile.SetDataSourceName('');
    DSOptions.ActivePageIndex := 0;
  end;

  InitExampleControls;
end;

procedure TEDIT_EVOX_MAP.DSOptionsFromObjectToSheet;
var
  Options: IisStringList;
  i: integer;
  sFieldsDefs: String;
  FieldsDefs: IisListOfValues;
  FieldDef: IFixedReaderFieldDefinition;
begin
  Options := FMapFile.GetDataSourceOptions;

  if DSTypesList.Items[DSTypesList.ItemIndex] = sCSVReaderName then
  begin
    // defaults
    DelimiterCharList.ItemIndex := 0;
    QuoteCharList.ItemIndex := 0;
    MustbeQuotedCheck.Checked := false;
    FieldsInFirstLineCheck.Checked := false;

    for i := 0 to Options.Count - 1 do
      if (Options.Names[i] = fieldNamesInFirstRow) and (Options.Values[Options.Names[i]] = '1') then
        FieldsInFirstLineCheck.Checked := true
      else if (Options.Names[i] = fieldDelimiter) and (Options.Values[Options.Names[i]] = '1') then
        DelimiterCharList.ItemIndex := 1
      else if (Options.Names[i] = DoubleQuoteChar) and (Options.Values[Options.Names[i]] = '1') then
        QuoteCharList.ItemIndex := 1
      else if (Options.Names[i] = DoubleQuoteRequired) and (Options.Values[Options.Names[i]] = '1') then
        MustbeQuotedCheck.Checked := true;
   end
   else if DSTypesList.Items[DSTypesList.ItemIndex] = sASCIIFixedReaderName then
   begin
     if not Assigned(FFixedFieldsDataSet) then
     begin
       dsFixedFields.DataSet := nil;
       FFixedFieldsDataSet := TEvExchangeDataSet.Create; // TevDataSet.Create;
       FFixedFieldsDataSet.vclDataSet.FieldDefs.Add(FixedFieldName, ftString, 20, true);
       FFixedFieldsDataSet.vclDataSet.FieldDefs.Add(FixedFieldStart, ftInteger, 0, true);
       FFixedFieldsDataSet.vclDataSet.FieldDefs.Add(FixedFieldEnd, ftInteger, 0, true);
       FFixedFieldsDataSet.vclDataSet.Active := true;
     end
     else
       // studip loop instead of clear in order to not break sort in grid...
       try
         FFixedFieldsDataSet.vclDataSet.DisableControls;
         FFixedFieldsDataSet.First;
         while not FFixedFieldsDataSet.eof do
           FFixedFieldsDataSet.Delete;
       finally
         FFixedFieldsDataSet.vclDataSet.EnableControls;
       end;

     sFieldsDefs := Options.Values[fieldsList];
     if sFieldsDefs <> '' then
     begin
       FFixedFieldsDataSet.vclDataSet.DisableControls;
       try
         FieldsDefs := (FReader As IevFixedReader).ParseStringWithFieldsDefinitions(sFieldsDefs);
         for i := 0 to FieldsDefs.Count - 1 do
         begin
           FieldDef := IInterface(FieldsDefs.Values[i].Value) as IFixedReaderFieldDefinition;
           FFixedFieldsDataSet.Append;
           FFixedFieldsDataSet[FixedFieldName] := FieldDef.GetName;
           FFixedFieldsDataSet[FixedFieldStart] := FieldDef.GetStartPos;
           FFixedFieldsDataSet[FixedFieldEnd] := FieldDef.GetEndPos;
           FFixedFieldsDataSet.Post;
         end;
       finally
         FFixedFieldsDataSet.vclDataSet.EnableControls;
       end;
     end;
     FFixedFieldsDataSet.First;
     if not Assigned(dsFixedFields.DataSet) then
       dsFixedFields.DataSet := FFixedFieldsDataSet.vclDataSet;
   end
   else
     Exception.Create('Unsupported data source type');
end;

procedure TEDIT_EVOX_MAP.DSOptionsFromSheetToObject;
var
  Options: IisStringList;

  function CalcOption(const ACondition: boolean; const AOption: String) : String;
  begin
    if ACondition then
      Result := AOption + '=1'
    else
      Result := AOption + '=0'
  end;
begin
  Options := FMapFile.GetDataSourceOptions;

  if DSTypesList.Items[DSTypesList.ItemIndex] = sCSVReaderName then
  begin
    Options.Clear;
    Options.Add(CalcOption(FieldsInFirstLineCheck.Checked, fieldNamesInFirstRow));
    Options.Add(CalcOption(DelimiterCharList.ItemIndex = 1, fieldDelimiter));
    Options.Add(CalcOption(QuoteCharList.ItemIndex = 1, DoubleQuoteChar));
    Options.Add(CalcOption(MustbeQuotedCheck.Checked, DoubleQuoteRequired));
  end
  else if DSTypesList.Items[DSTypesList.ItemIndex] = sCSVReaderName then
  begin
    // doing nothing because all changes are saved to object, when user press "Apply Changes" button
  end
  else
     Exception.Create('Unsupported data source type');
end;

procedure TEDIT_EVOX_MAP.SetDSTypeByMap;
var
  i: integer;
begin
  for i := 0 to DSTypesList.Items.Count - 1 do
    if DSTypesList.Items[i] = FMapFile.GetDataSourceName then
    begin
      DSTypesList.ItemIndex := i;
      SelectDSOptionsSheetByControl;
      exit;
    end;
  Assert(false, 'Unknown data source type in map file: ' + FMapFile.GetDataSourceName);
end;

procedure TEDIT_EVOX_MAP.MapFromObjectToScreen;
begin
  SetDSTypeByMap;
  DSOptionsFromObjectToSheet;
  FillMapSettings;

  //loading example
  if FMapFile.GetExampleData.Size > 0 then
  begin
    dsExample.DataSet := nil;
    FExampleDataSet := TevDataSet.Create;
    FExampleDataSet.AsStream := FMapFile.GetExampleData;
    FExampleFileName := FMapFile.GetExampleFileName;
    ExampleFilePath.Caption := FExampleFileName;
    dsExample.DataSet := FExampleDataSet.vclDataSet;
  end;

  AssignPackageForControl;
  FillPackageFieldsInMap;
  PrepareInputFieldsList;
  FillMappedFieldsInGrid;
  CalculateAssignedTimes;
end;

procedure TEDIT_EVOX_MAP.MapFromScreenToObject;
begin
  DSOptionsFromSheetToObject;
  SaveMapSettings;

  if SaveExampleWithMapCheck.Checked and Assigned(FExampleDataSet) then
  begin
    FMapFile.SetExampleFileName(FExampleFileName);
    FMapFile.SetExampleData(FExampleDataSet.AsStream);
  end
  else
  begin
    FMapFile.SetExampleFileName('');
    FMapFile.GetExampleData.Clear;
  end;

  FmapFile.SetPackageInformation(FPackage);  // updating the version of the package
end;

procedure TEDIT_EVOX_MAP.EnableDataSourceTabControls;
begin
  evPageControl1.Enabled := true;
  LoadExampleButton.Enabled := true;
  RowsToLoad.Enabled := true;
  ExampleGrid.Enabled := true;
end;

procedure TEDIT_EVOX_MAP.InitExampleControls;
begin
  ExampleFilePath.Caption := '';
  RowsToLoad.Value := 1;
  dsExample.DataSet := nil;
  FExampleFileName := '';
  FExampleDataSet := nil;
  dsFixedFields.DataSet := nil;
  FFixedFieldsDataSet := nil;
end;

procedure TEDIT_EVOX_MAP.LoadExampleButtonClick(Sender: TObject);
var
  FormatsList: IisStringListRO;
  i: integer;
  sName, sExt: String;
  sFilter: String;
begin
  inherited;
  FormatsList := FReader.GetSupportedFormatFileExt;
  sFilter := '';
  for i := 0 to FormatsList.Count - 1 do
  begin
    sExt := FormatsList.Names[i];
    sName := FormatsList.Values[sExt];
    sFilter := sFilter + sName + ' (' + sExt + ')|' + sExt + '|';
  end;

  if sFilter <> '' then
    ExampleFileDlg.Filter := sFilter
  else
    ExampleFileDlg.Filter := 'Any file (*.*)|*.*';

  if ExampleFileDlg.Execute then
  begin
    FMapFileChanged := true;
    FExampleFileName := ExampleFileDlg.FileName;
    ExampleFilePath.Caption := FExampleFileName;

    // loading
    LoadExample(FExampleFileName, RowsToLoad.Value, false);
  end;
end;

procedure TEDIT_EVOX_MAP.DelimiterCharListChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.QuoteCharListChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.MustbeQuotedCheckClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.FieldsInFirstLineCheckClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.PrepareInputFieldsList;
var
  i: integer;
  MapFieldsList: IisListOfValues;
  MapField: IevEchangeMapField;
  ListForSort: IisStringList;
begin
  // just filling list, doesn't set up relations with map fields
  FdsInputFields.vclDataSet.DisableControls;
  try
    FdsInputFields.vclDataSet.Active := false;
    FdsInputFields.vclDataSet.Active := true;

    // reading fields names from example file
    if FExampleFileName <> '' then
    begin
      FExampleDataSet.First;
      for i := 0 to FExampleDataSet.vclDataSet.FieldCount - 1 do
      begin
        if not (Pos(UnusedFieldPrefix,  FExampleDataSet.vclDataSet.FieldDefs[i].Name) > 0) then
        begin
          FdsInputFields.Append;
          FdsInputFields['FieldName'] := FExampleDataSet.vclDataSet.FieldDefs[i].Name;
          FdsInputFields['Assigned'] := 0;
          if not FExampleDataSet.Eof then
            FdsInputFields['Example'] := FExampleDataSet.vclDataSet.Fields[i].Value;
          FdsInputFields.Post;
        end;
      end;
    end;

    // adding from map file if necessary
    if Assigned(FMapFile) then
    begin
      MapFieldsList := FMapFile.GetFieldsList;
      for i := 0 to MapFieldsList.Count - 1 do
      begin
        MapField := IInterface(MapFieldsList.Values[i].Value) as IevEchangeMapField;
        if not FdsInputFields.vclDataSet.Locate('FieldName', MapField.GetInputFieldName, [loCaseInsensitive]) then
        begin
          FdsInputFields.Append;
          FdsInputFields['FieldName'] := MapField.GetInputFieldName;
          FdsInputFields['Assigned'] := 0;
          FdsInputFields.Post;
        end;
      end;
    end;

    // filing list of input fields for AsOfDate
    AsOfDateFields.Items.Clear;
    ListForSort := TisStringList.Create;
    FdsInputFields.First;
    while not FdsInputFields.eof do
    begin
      ListForSort.Add(FdsInputFields['FieldName']);
      FdsInputFields.Next;
    end;
    ListForSort.Sort;
    AsOfDateFields.Items.Text := ListForSort.Text;
    AsOfDateFields.ItemIndex := -1;

    FdsInputFields.First;
  finally
    FdsInputFields.vclDataSet.EnableControls;
  end;
end;

procedure TEDIT_EVOX_MAP.CalculateAssignedTimes;
var
  i: integer;
  cnt: integer;
  MapFieldsList: IisListOfValues;
  MapField: IevEchangeMapField;
  SavePlace: TBookmark;
begin
  SavePlace := FdsInputFields.vclDataSet.GetBookmark;
  try
    FdsInputFields.vclDataSet.DisableControls;
    try
      FdsInputFields.First;
      while not FdsInputFields.eof do
      begin
        MapFieldsList := FMapFile.GetFieldsList;
        cnt := 0;
        for i := 0 to MapFieldsList.Count - 1 do
        begin
          MapField := IInterface(MapFieldsList.Values[i].Value) as IevEchangeMapField;
          if AnsiSametext(MapField.GetInputFieldName, FdsInputFields['FieldName']) then
            Inc(cnt);
          if MapField.GetAsOfDateEnabled and (MapField.GetAsOfDateSource = stMappedField)
            and AnsiSametext(MapField.GetAsOfDateField, FdsInputFields['FieldName']) then
            Inc(cnt);
        end;
        FdsInputFields.Edit;
        FdsInputFields['Assigned'] := cnt;
        FdsInputFields.Post;

        FdsInputFields.Next;
      end;
    finally
      FdsInputFields.vclDataSet.EnableControls;
    end;
  finally
    try
      try
        FdsInputFields.vclDataSet.GotoBookmark(SavePlace);
      finally
        FdsInputFields.vclDataSet.FreeBookmark(SavePlace);
      end;
    except
      on E:Exception do
        FdsInputFields.First;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.FillPackageFieldsInMap;
var
  i: integer;
  PackageFieldsList: IisListOfValues;
  PackageField: IEvExchangePackageField;
  OldIndexName: String;
  GroupsList, SectionsList: IisListofValues;
  GroupValue: IEvExchangePackageGroup;
  SectionValue: IEvExchangePackageSection;
  GroupRecordNumbers: TisDynIntegerArray;

  procedure AddMappedField(aPackageField: IEvExchangePackageField);
  begin
    FdsMappedFields.Append;
    FdsMappedFields['ExchangeField'] := aPackageField.GetExchangeFieldName;
    FdsMappedFields['DisplayName'] := aPackageField.GetDisplayName;
    FdsMappedFields['ItIsGroup'] := '0';
    FdsMappedFields['GroupRecordNumber'] := -1;
    if aPackageField.IsBelongsToGroup and (not aPackageField.IsGroupDefinitionField) then
      FdsMappedFields['GroupRecordNumber'] := aPackageField.GetRecordNumber;
    if PackageField.GetAsOfDateEnabled then
      FdsMappedFields['AsOfDate'] := 'Not Defined'
    else
      FdsMappedFields['AsOfDate'] := 'Not Applicable';
    FdsMappedFields.Post;
  end;

begin
  FdsMappedFields.vclDataSet.DisableControls;
  try
    OldIndexName := (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName;
    (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName := '';
    try
      FdsMappedFields.vclDataSet.Active := false;
      FdsMappedFields.vclDataSet.Active := true;

      if (FOpened.Name = '') then
      begin
        // adding sections to the begin
        SectionsList := FPackage.GetSectionsList;
        for i := 0 to SectionsList.Count - 1 do
        begin
          SectionValue := IInterface(SectionsList.Values[i].Value) as IEvExchangePackageSection;
          FdsMappedFields.Append;
          FdsMappedFields['ExchangeField'] := SectionValue.GetName;
          FdsMappedFields['DisplayName'] := SectionValue.GetDisplayName;
          FdsMappedFields['ItIsGroup'] := '2';
          FdsMappedFields['GroupRecordNumber'] := -1;
          FdsMappedFields['AsOfDate'] := '';
          FdsMappedFields.Post;
        end;
        // no opened group/section
        PackageFieldsList := FPackage.GetFieldsList;
        for i := 0 to PackageFieldsList.Count - 1 do
        begin
          PackageField := IInterface(PackageFieldsList.Values[i].Value) as IEvExchangePackageField;
          if not (PackageField.IsBelongsToGroup or PackageField.IsBelongsToSection('<ANY>')) then  // we are skipping group's fields
          begin
            AddMappedField( PackageField );
          end;
        end;
        // adding groups to the end
        GroupsList := FPackage.GetGroupsList;
        for i := 0 to GroupsList.Count - 1 do
        begin
          GroupValue := IInterface(GroupsList.Values[i].Value) as IEvExchangePackageGroup;
          FdsMappedFields.Append;
          FdsMappedFields['ExchangeField'] := GroupValue.GetName;
          FdsMappedFields['DisplayName'] := GroupValue.GetDisplayName;
          FdsMappedFields['ItIsGroup'] := '1';
          FdsMappedFields['GroupRecordNumber'] := -1;
          FdsMappedFields['AsOfDate'] := '';
          FdsMappedFields.Post;
        end;
      end
      else if (FOpened.Name <> '') and FOpened.IsGroup then
      begin
        // group is opened
        GroupRecordNumbers := FPackage.GetGroupRecordNumbers(FOpened.Name);
        if Length(GroupRecordNumbers) = 0 then
        begin
          i := FPackage.AddNewRecordToGroup(FOpened.Name);
          SetLength(GroupRecordNumbers, Succ(Length(GroupRecordNumbers)));
          GroupRecordNumbers[High(GroupRecordNumbers)] := i;
        end;

        PackageFieldsList := FPackage.GetFieldsList;
        for i := 0 to PackageFieldsList.Count - 1 do
        begin
          PackageField := IInterface(PackageFieldsList.Values[i].Value) as IEvExchangePackageField;
          if PackageField.IsBelongsToGroup and (not PackageField.IsGroupDefinitionField)
            and (PackageField.GetGroupName = FOpened.Name) then
          begin
            AddMappedField( PackageField );
          end;
        end;  // for i
      end
      else if (FOpened.Name <> '') and not FOpened.IsGroup then
      begin
        // section is opened
        PackageFieldsList := FPackage.GetFieldsList;
        for i := 0 to PackageFieldsList.Count - 1 do
        begin
          PackageField := IInterface(PackageFieldsList.Values[i].Value) as IEvExchangePackageField;
          if PackageField.IsBelongsToSection(FOpened.Name) then
            AddMappedField(PackageField);
        end;  // for i
      end;
    finally
      (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName := OldIndexName;
    end;
  finally
    FdsMappedFields.vclDataSet.EnableControls;
  end;

  FdsMappedFields.First;
end;

procedure TEDIT_EVOX_MAP.FillPackageTypesList;
var
  i: integer;
  CurPackage: IEvExchangePackage;
begin
  ImportPackagesList.Items.Clear;
  for i := 0 to FPackageList.Count - 1 do
  begin
    CurPackage := Context.EvolutionExchange.GetEvExchangePackage(FPackageList[i]);
    ImportPackagesList.Items.Add(CurPackage.GetDisplayName);
  end;
  if ImportPackagesList.Items.Count > 0 then
    try
      FSilentImportPackagesList := true;
      ImportPackagesList.ItemIndex := 0;
    finally
      FSilentImportPackagesList := false;
    end;
end;

procedure TEDIT_EVOX_MAP.ImportPackagesListChange(Sender: TObject);
var
  bFlag: boolean;
  NewPackage: IEvExchangePackage;
begin
  inherited;
  if (not FSilentImportPackagesList) and (ImportPackagesList.ItemIndex <> -1) then
  begin
    if FOpened.Name <> '' then
      OpenCloseGroupBtn.Click;

    NewPackage := GetPackageByDisplayName(ImportPackagesList.Items[ImportPackagesList.ItemIndex]);
    if AnsiSametext(NewPackage.GetName, FPackage.GetName) then
      exit;

    bFlag := true;
    if FMapFile.GetFieldsList.Count > 0 then
      bFlag := EvMessage('You will lose all your mapping. Do you want to continue?', mtWarning, [mbYes, MbNo], mbNo) = mrYes;

    if bFlag then
    begin
      FMapFileChanged := true;
      ClearMappedData;  // clearing all mapping information due package is changed.
      FPackage := GetPackageByDisplayName(ImportPackagesList.Items[ImportPackagesList.ItemIndex]);
      FillPackageFieldsInMap;
      FillMappedFieldsInGrid;
      FOpened.Name := '';
      RefreshGroupsGUI;
      MapFromScreenToObject;
      InitGenericPayrollOptions;
    end
    else
      AssignPackageForControl;
  end;
end;

procedure TEDIT_EVOX_MAP.ClearMappedData;
begin
  FMapFile.GetFieldsList.Clear;
  PrepareInputFieldsList;
  CalculateAssignedTimes;
end;

function TEDIT_EVOX_MAP.GetPackageByDisplayName(const ADisplayPackageName: String): IEvExchangePackage;
var
  i: integer;
  CurPackage: IEvExchangePackage;
begin
  Result := nil;
  for i := 0 to FPackageList.Count - 1 do
  begin
    CurPackage := Context.EvolutionExchange.GetEvExchangePackage(FpackageList[i]);
    if AnsiSameText(CurPackage.GetDisplayName, ADisplayPackageName) then
    begin
      Result := CurPackage;
      exit;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.AssignPackageForControl;
var
  i: integer;
begin
  if FPackage.GetDisplayName <> ImportPackagesList.Text then
  try
    FSilentImportPackagesList := true;
    ImportPackagesList.ItemIndex := -1;
    for i := 0 to ImportPackagesList.Items.Count - 1 do
      if AnsiSameText(FPackage.GetDisplayName, ImportPackagesList.Items[i]) then
      begin
        ImportPackagesList.ItemIndex := i;
        exit;
      end;
  finally
    FSilentImportPackagesList := false;
  end;
end;

procedure TEDIT_EVOX_MAP.MappedFieldsgridRowChanged(Sender: TObject);
begin
  inherited;
  FillPackageFieldInfo;
  FillMapFieldInfo;
  RefreshGroupsGUI;
end;

procedure TEDIT_EVOX_MAP.FillPackageFieldInfo;
var
  Field: IEvExchangePackageField;
  FieldValues: IisListOfValues;
  i: integer;
begin
  ExchangeFieldEdit.Text := '';
  EvoTableEdit.Text := '';
  EvoFieldEdit.Text := '';
  LookupTableEdit.Text := '';
  LookupFieldEdit.Text := '';
  PackageFieldDataTypeEdit.Text := '';
  PackageFieldSizeEdit.Text := '';
  RequiredByDDCheck.Checked := false;
  PackageFieldDefaultEdit.Text := '';
  PackageFieldLOV.Clear;

  if (not FdsMappedFields.eof) and (FdsMappedFields['ExchangeField'] <> null) and
    (FdsMappedFields['ItIsGroup'] = '0') then
  begin
    Field := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']);
    ExchangeFieldEdit.Text := Field.GetExchangeFieldName;
    EvoTableEdit.Text := Field.GetEvTableName;
    EvoFieldEdit.Text := Field.GetEvFieldName;
    LookupTableEdit.Text := Field.GetLookupTableName;
    LookupFieldEdit.Text := Field.GetLookupFieldName;
    PackageFieldDataTypeEdit.Text := Field.GetTypeName;
    if Field.GetFieldSize > 0 then
      PackageFieldSizeEdit.Text := IntToStr(Field.GetFieldSize);
    RequiredByDDCheck.Checked := Field.GetRequiredByDD;
    if ExchangeFieldEdit.Text <> 'ACA Status' then
      PackageFieldDefaultEdit.Text := Field.GetDefaultValue;
    PackageFieldDescription.Text := Field.GetDescription;

    FieldValues := Field.GetFieldValues;
    for i := 0 to FieldValues.Count - 1 do
      PackageFieldLOV.Items.Add(FieldValues.Values[i].Name + '=' + FieldValues.Values[i].Value);

    if AnsiSameText(FPackage.GetName,EvoXBasicClientPackage) then
    begin
      if (EvoTableEdit.Text='CL_AGENCY') and
         (EvoFieldEdit.Text='CLIENT_AGENCY_CUSTOM_FIELD')then
      begin
        PackageFieldLOV.Items.Add('W=Workers Comp Agency');
        PackageFieldLOV.Items.Add('B=EE Name, Block SSN on Check Stub');
        PackageFieldLOV.Items.Add('C=Puts Company Name instead of Agency Name on Agent & Agency Direct Deposit');
        PackageFieldLOV.Items.Add('E=Individual Check by EE');
        PackageFieldLOV.Items.Add('S=EE Name & SSN on Check Stub Detail');
        PackageFieldLOV.Items.Add('Null=One check for all EE''s with Agency');
      end
      //Use State Reciprocate Laws on W2 Filler 41,1 
      else if (EvoTableEdit.Text='CL') and
              (EvoFieldEdit.Text='FILLER')then
      begin
        PackageFieldLOV.Items.Add('Y=Yes');
        PackageFieldLOV.Items.Add('N=No');
        PackageFieldDefaultEdit.Text := 'N';
        RequiredByDDCheck.Checked := True;
      end
      else if (EvoTableEdit.Text='CL_DELIVERY_GROUP') and
              (EvoFieldEdit.Text='STUFF')
      then PackageFieldDefaultEdit.Text := 'Y'
      else if (EvoTableEdit.Text='CL') and
              (EvoFieldEdit.Text='AUTO_SAVE_MINUTES')
      then PackageFieldDefaultEdit.Text := '0';
    end;

    AsOfDateSheet.Enabled := Field.GetAsOfDateEnabled;
    AsOfDateSheet.TabVisible := Field.GetAsOfDateEnabled;
  end;
  MappingTypeSheet.TabVisible := true;
  OptionsPage.ActivePageIndex := 0;
end;

procedure TEDIT_EVOX_MAP.FillMappedFieldsInGrid;
var
  MapField: IevEchangeMapField;
  PackageField: IEvExchangePackageField;
  SavePlace: TBookmark;
  OldIndexName: String;
  sGroupMappedFields: String;
  i: integer;
begin
  SavePlace := FdsMappedFields.vclDataset.GetBookmark;
  try
    FdsMappedFields.vclDataSet.DisableControls;
    try
      OldIndexName := (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName;
      (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName := '';
      try
        FdsMappedFields.First;
        while not FdsMappedFields.eof do
        begin
          if (FdsMappedFields['ExchangeField'] <> null) and (FdsMappedFields['ItIsGroup'] = '0') then
          begin
            MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
            if Assigned(MapField) then
            begin
              FdsMappedFields.Edit;
              FdsMappedFields['InputField'] := MapField.GetInputFieldName;
              FdsMappedFields['MappingType'] := MapField.GetMapRecord.GetMapTypeName;
              FdsMappedFields['Example'] := GetExampleData(MapField.GetInputFieldName);
              SetAsOFDateValueInGrid(MapField, false);

              FdsMappedFields.Post;
            end;
          end;
          if (FdsMappedFields['ExchangeField'] <> null) and (FdsMappedFields['ItIsGroup'] = '1') then
          begin
            sGroupMappedFields := '';
            for i := 0 to FMapFile.GetFieldsList.Count - 1 do
            begin
              MapField := IInterface(FMapFile.GetFieldsList.Values[i].Value) as IevEchangeMapField;
              if MapField.GetGroupRecordNumber > 0 then
              begin
                PackageField := FPackage.GetFieldByName(MapField.GetExchangeFieldName);
                if (PackageField.GetGroupName = FdsMappedFields['ExchangeField']) then
                  if sGroupMappedFields = '' then
                    sGroupMappedFields := MapField.GetInputFieldName
                  else
                    sGroupMappedFields := sGroupMappedFields + ', ' + MapField.GetInputFieldName;
              end;
            end;

            FdsMappedFields.Edit;
            if Length(sGroupMappedFields) > InputFieldSize then
              sGroupMappedFields := Copy(sGroupMappedFields, 1, InputFieldSize - 3) + '...';
            FdsMappedFields['InputField'] := sGroupMappedFields;

            FdsMappedFields.Post;
          end;
          FdsMappedFields.Next;
        end;  // while
      finally
        (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).IndexName := oldIndexName;
      end;
    finally
      FdsMappedFields.vclDataSet.EnableControls;
    end;
  finally
    try
      try
        FdsMappedFields.vclDataSet.GotoBookmark(SavePlace);
      finally
        FdsMappedFields.vclDataSet.FreeBookmark(SavePlace);
      end;
    except
      on E: Exception do
        FdsMappedFields.First;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.UnmapFieldButtonClick(Sender: TObject);
begin
  inherited;
  if (not FdsMappedFields.eof) and (FdsMappedFields['ExchangeField'] <> null)
    and (FdsMappedFields['InputField'] <> null) then
  begin
    FMapFileChanged := true;
    FdsMappedFields.Edit;
    FdsMappedFields['InputField'] := null;
    FdsMappedFields['MappingType'] := null;
    FdsMappedFields['Example'] := null;
    SetAsOfDateValueInGrid(nil, false);
    FdsMappedFields.Post;
    FMapFile.DeleteFieldByExchangeName(FdsMappedFields['ExchangeField']);
    FillMapFieldInfo;
    CalculateAssignedTimes;
  end;
end;

procedure TEDIT_EVOX_MAP.FillMapFieldInfo;
begin
  if (not FdsMappedFields.eof) and (FdsMappedFields['InputField'] <> null) and
    (FdsMappedFields['ItIsGroup'] = '0') then
  begin
    SelectMapTypeSheet;
  end
  else
  begin
    EnableAsOfDateCheck.Enabled := false;
    DateTimeDirectPanel.Enabled := false;
    CustomFuntionDateTimePanel.Enabled := false;
    MapTypesPageControl.ActivePage := DirectMapSheet;
    MapTypesPageControl.Enabled := false;
    CustomDateFormat.ItemIndex := -1;
    CustomTimeFormat.ItemIndex := -1;
    DirectDateFormat.ItemIndex := -1;
    DirectTimeFormat.ItemIndex := -1;
    FdsUsermap.vclDataSet.Active := false;
    FdsUsermap.vclDataSet.Active := true;
    //asofdate
    FNoSaveAsOfDateEnabled := true;
    try
      EnableAsOfDateCheck.Checked := false;
    finally
      FNoSaveAsOfDateEnabled := false;
    end;
    ManageAsOfDateGUI;
  end;
end;

procedure TEDIT_EVOX_MAP.UnmapAllButtonClick(Sender: TObject);
begin
  inherited;
  if FMapFile.GetFieldsList.Count > 0 then
    if (EvMessage('Do you want to unmap ALL fields?', mtWarning, [mbYes, MbNo], mbNo) = mrYes) then
    begin
      FMapFileChanged := true;
      FMapFile.GetFieldsList.Clear;
      MapFromObjectToScreen;
    end;
end;

procedure TEDIT_EVOX_MAP.MapFieldButtonClick(Sender: TObject);
var
  MapField: IevEchangeMapField;
  PackageField: IEvExchangePackageField;
  MapRecord, PackageMapRecord: IEvImportMapRecord;
  sError: String;
begin
  inherited;
  CheckCondition(FdsMappedFields['ItIsGroup'] = '0', 'Error: Group is opened');

  if (not FdsInputFields.eof) and (FdsinputFields['FieldName'] <> null) and
    (not FdsMappedFields.eof) and (FdsMappedFields['ExchangeField'] <> null) then
  begin
    MapField := TevEchangeMapField.Create;
    MapField.SetInputFieldName(FdsInputFields['FieldName']);
    MapField.SetExchangeFieldName(FdsMappedFields['ExchangeField']);

    PackageField := FPackage.GetFieldByName(MapField.GetExchangeFieldName);
    PackageMapRecord := PackageField.GetMapRecord(PackageField.GetPackageMapRecordName);

    MapField.SetFieldSize(PackageField.GetFieldSize);
    MapField.SetFieldType(PackageField.GetType);

    if (FdsInputFields['Example'] <> null) and (PackageMapRecord.MapType = mtpDirect)
      and (PackageMapRecord.KeyLookupTable = '') then
    begin
      // checking type
      sError := IfVariantIs(FdsInputFields['Example'], MapField.GetFieldType, MapField.GetDateFormat, MapField.GetTimeFormat);
      if sError <> '' then
        if not (EvMessage('Example data does not match field''s datatype: "' + sError +
          '". Do you want to map anyway?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
          exit;
      // checking size
      if MapField.GetFieldType = ddtString then
      begin
        if Length(FdsInputFields.vclDataset.FieldByName('Example').AsString) > MapField.GetFieldSize then
          if not (EvMessage('Example data has length more than allowed by Evolution. "' +
          '"Do you want to map anyway?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
            exit;
      end;
    end;

    // replacement
    if FdsMappedFields['InputField'] <> null then
      if not (EvMessage('Do you want to replace current mapping?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
        exit
      else
        UnmapFieldButton.Click;

    FMapFileChanged := true;

    MapRecord := ((PackageField.GetMapRecord(PackageField.GetDefaultMapRecordName) as IisInterfacedObject).GetClone) as IEvImportMapRecord;
    MapField.SetMapRecord(MapRecord);
    if (FOpened.Name <> '') and FOpened.IsGroup then
      MapField.SetGroupRecordNumber(FdsMappedFields['GroupRecordNumber']);

    FMapFile.AddField(MapField);

    FillMappedFieldsInGrid;
    FillMapFieldInfo;
    CalculateAssignedTimes;
  end;
end;

procedure TEDIT_EVOX_MAP.AutoMapButtonClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;

end;

procedure TEDIT_EVOX_MAP.SelectMapTypeSheet;
var
  DataType: TDataDictDataType;
  MapType: TEvImportMapType;
begin
  ReadAsOfDateInfo;
  
  MapType := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']).GetMapRecord.MapType;

  MapTypesPageControl.Enabled := true;
  DirectMapSheet.TabVisible := true;
  CustomFunctionMapSheet.TabVisible := true;
  MapSheet.TabVisible := true;

  DataType := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetType;

  FillListOfCustomFunctions;
  if CustomFunctionList.Items.Count = 0 then
    CustomFunctionMapSheet.TabVisible := false
  else
    CustomFunctionMapSheet.TabVisible := true;

  case DataType of
    ddtInteger,
    ddtFloat,
    ddtCurrency,
    ddtString, ddtBLOB:    begin
                    case MapType of
                    mtpDirect: begin
                                 MapTypesPageControl.ActivePageIndex := 0;
                                 ReadDirectTypeInfo;
                                 DateTimeDirectPanel.Enabled := false;
                                end;
                    mtpCustom: begin
                                 if CustomFunctionList.Items.Count = 0 then
                                    Assert(false, 'No custom functions avaliable');
                                 MapTypesPageControl.ActivePageIndex := 1;
                                 ReadCustomFunctionInfo;
                                 CustomFuntionDateTimePanel.Enabled := false;
                               end;
                    mtpMap:    begin
                                  MapTypesPageControl.ActivePageIndex := 2;
                                  ReadMapTypeInfo;
                               end;
                    else
                      Assert(false, 'Unsupported mapping type');
                    end;
                  end;

    ddtDateTime:  begin
                    MapSheet.TabVisible := false;
                    case MapType of
                    mtpDirect:  begin
                                  MapTypesPageControl.ActivePageIndex := 0;
                                  DateTimeDirectPanel.Enabled := true;
                                  ReadDirectTypeInfo;
                                end;
                    mtpCustom:  begin
                                 if CustomFunctionList.Items.Count = 0 then
                                    Assert(false, 'No custom functions avaliable');
                                  MapTypesPageControl.ActivePageIndex := 1;
                                  CustomFuntionDateTimePanel.Enabled := true;
                                  ReadCustomFunctionInfo;
                                end;
                    mtpMap:    Assert(false, 'You cannot map DateTime data');
                    else
                      Assert(false, 'Unsupported mapping type');
                    end;
                  end;
  else
    Assert(false, 'Unsupported data type');
  end;
end;

procedure TEDIT_EVOX_MAP.SaveDirectTypeInfo(const AOnChangeFlag: boolean);
var
  MapField: IevEchangeMapField;
  MapRecord: IEvImportMapRecord;
  DataType: TDataDictDataType;
begin
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapRecord := MapField.GetMapRecord;
  if (MapRecord.MapType <> mtpDirect) or AOnChangeFlag then
  begin
    FMapFileChanged := true;
    MapRecord.MapType := mtpDirect;

    // looking for map record name
    MapRecord.SetMapRecordName('Direct');

    FdsMappedFields.Edit;
    FdsMappedFields['MappingType'] := MapRecord.GetMapTypeName;
    FdsMappedFields.Post;
    DataType := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetType;
    if DataType = ddtDateTime then
    begin
      MapField.SetDateFormat(Trim(DirectDateFormat.Text));
      MapField.SetTimeFormat(Trim(DirectTimeFormat.Text));
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.DirectDateFormatChange(Sender: TObject);
begin
  inherited;
  SaveDirectTypeInfo(true);
end;

procedure TEDIT_EVOX_MAP.DirectTimeFormatChange(Sender: TObject);
begin
  inherited;
  SaveDirectTypeInfo(true);
end;

procedure TEDIT_EVOX_MAP.SaveCustomFunctionInfo(const AOnChangeFlag: boolean);
var
  MapField: IevEchangeMapField;
  MapRecord: IEvImportMapRecord;
  DataType: TDataDictDataType;
begin
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapRecord := MapField.GetMapRecord;
  if (MapRecord.MapType <> mtpCustom) or AOnChangeFlag then
  begin
    FMapFileChanged := true;
    MapRecord.MapType := mtpCustom;
    MapRecord.SetMapRecordName(CustomFunctionList.Text);
    MapRecord.FuncName := CustomFunctionList.Text;
    FdsMappedFields.Edit;
    FdsMappedFields['MappingType'] := MapRecord.GetMapTypeName;
    FdsMappedFields.Post;
    DataType := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetType;
    if DataType = ddtDateTime then
    begin
      MapField.SetDateFormat(Trim(CustomDateFormat.Text));
      MapField.SetTimeFormat(Trim(CustomTimeFormat.Text));
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.SaveMapTypeInfo(const AOnChangeFlag: boolean);
var
  MapField: IevEchangeMapField;
  MapRecord: IEvImportMapRecord;
  tmpList: TStringList;
  sTmp: String;
  SavePlace: TBookmark;
begin
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapRecord := MapField.GetMapRecord;
  if MapRecord.MapType <> mtpMap then
    FdsUsermap.Clear;
  if (MapRecord.MapType <> mtpMap) or AOnChangeFlag then
  begin
    FMapFileChanged := true;
    MapRecord.MapType := mtpMap;
    MapRecord.SetMapRecordName('Map');
    FdsMappedFields.Edit;
    FdsMappedFields['MappingType'] := MapRecord.GetMapTypeName;
    FdsMappedFields.Post;

    // saving map list
    if not FdsUsermap.eof then
    begin
      FdsUsermap.vclDataSet.DisableControls;
      try
        SavePlace := FdsUsermap.vclDataSet.GetBookmark;
        try
          tmpList := TStringList.Create;
          try
            FdsUsermap.First;
            while not FdsUsermap.eof do
            begin
              if (FdsUsermap['From'] <> null) and (FdsUsermap['To'] <> null) then
              begin
                if FdsUsermap['From'] <> null then
                  sTmp := FdsUsermap['From'] + '='
                else
                  sTmp := '=';
                if FdsUsermap['To'] <> null then
                  sTmp := sTmp + FdsUsermap['To'];
                tmpList.Add(sTmp);
              end;
              FdsUsermap.Next;
            end;
            MapRecord.MapValues := tmpList.Text;
          finally
            FreeAndNil(tmpList);
          end;
        finally
          try
            try
              FdsUsermap.vclDataSet.GotoBookmark(SavePlace);
            finally
              FdsUsermap.vclDataSet.FreeBookmark(SavePlace);
            end;
          except
            on E:Exception do
              FdsUsermap.First;
          end;
        end;
      finally
        FdsUsermap.vclDataSet.EnableControls;
      end;
    end
    else
      MapRecord.MapValues := '';
  end;
end;

procedure TEDIT_EVOX_MAP.CustomDateFormatChange(Sender: TObject);
begin
  inherited;
  SaveCustomFunctionInfo(true);
end;

procedure TEDIT_EVOX_MAP.CustomTimeFormatChange(Sender: TObject);
begin
  inherited;
  SaveCustomFunctionInfo(true);
end;

procedure TEDIT_EVOX_MAP.MapTypesPageControlChange(Sender: TObject);
begin
  inherited;
  if MapTypesPageControl.ActivePageIndex = 0 then
    SaveDirectTypeInfo(false)
  else if MapTypesPageControl.ActivePageIndex = 1 then
    SaveCustomFunctionInfo(false)
  else if MapTypesPageControl.ActivePageIndex = 2 then
    SaveMapTypeInfo(false);
  SelectMapTypeSheet;
end;

procedure TEDIT_EVOX_MAP.ReadCustomFunctionInfo;
var
  MapField: IevEchangeMapField;
  MapRecord: IEvImportMapRecord;
  DataType: TDataDictDataType;
begin
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapRecord := MapField.GetMapRecord;

  CheckCondition(MapRecord.MapType = mtpCustom, 'Something wrong with mapping type');

  DataType := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetType;

  FindAndSelectValueInlist(CustomFunctionList, MapField.GetMapRecord.GetMapRecordName);
  FillCustomFunctionDescription;

  if DataType = ddtDateTime then
  begin
    FindAndSelectValueInlist(CustomDateFormat, MapField.GetDateFormat);
    FindAndSelectValueInlist(CustomTimeFormat, MapField.GetTimeFormat);
  end
  else
  begin
    CustomDateFormat.ItemIndex := 0;
    CustomTimeFormat.ItemIndex := 0;
  end;
end;

procedure TEDIT_EVOX_MAP.ReadDirectTypeInfo;
var
  MapField: IevEchangeMapField;
  MapRecord: IEvImportMapRecord;
  DataType: TDataDictDataType;
begin
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapRecord := MapField.GetMapRecord;

  CheckCondition(MapRecord.MapType = mtpDirect, 'Something wrong with mapping type');

  DataType := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetType;
  if DataType = ddtDateTime then
  begin
    FindAndSelectValueInlist(DirectDateFormat, MapField.GetDateFormat);
    FindAndSelectValueInlist(DirectTimeFormat, MapField.GetTimeFormat);
  end
  else
  begin
    DirectDateFormat.ItemIndex := 0;
    DirectTimeFormat.ItemIndex := 0;
  end;
end;

procedure TEDIT_EVOX_MAP.ReadMapTypeInfo;
begin
  FillUserMapDataset;
end;

procedure TEDIT_EVOX_MAP.FindAndSelectValueInlist(const AComboBox: TComboBox; const AValue: String);
var
  i: integer;
begin
  for i := 0 to AComboBox.Items.Count - 1 do
  begin
    if AComboBox.Items[i] = AValue then
    begin
      AComboBox.ItemIndex := i;
      exit;
    end;
  end;
  AComboBox.ItemIndex := -1;
end;

procedure TEDIT_EVOX_MAP.FillListOfCustomFunctions;
var
  PackageField : IEvExchangePackageField;
  MapRecords: IisListOfValues;
  MapRecord: IEvImportMapRecord;
  i: integer;
begin
  CustomFunctionList.Items.Clear;
  PackageField := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']);
  CheckCondition(Assigned(PackageField), 'Package field not found');

  MapRecords := PackageField.GetMapRecordsByType(mtpCustom);
  for i := 0 to MapRecords.Count - 1 do
  begin
    MapRecord := IInterface(MapRecords.Values[i].Value) as IEvImportMapRecord;
    if MapRecord.IsVisibleToUser then
      CustomFunctionList.Items.Add(MapRecords.Values[i].Name);
  end;
  CustomFunctionList.ItemIndex := 0;
end;

procedure TEDIT_EVOX_MAP.CustomFunctionListChange(Sender: TObject);
begin
  inherited;
  SaveCustomFunctionInfo(true);
  FillCustomFunctionDescription;
end;

procedure TEDIT_EVOX_MAP.FillCustomFunctionDescription;
begin
  if (CustomFunctionList.Items.Count > 0) and (CustomFunctionList.Text <> '') then
    FunctionDescriptionMemo.Text := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']).GetMapRecord(CustomFunctionList.Text).Description
  else
    FunctionDescriptionMemo.Text := '';
end;

procedure TEDIT_EVOX_MAP.FillUserMapDataset;
var
  i: integer;
  MapRecord: IEvImportMapRecord;
  tmpList: TStringList;
begin
  FdsUsermap.vclDataSet.DisableControls;
  try
    FdsUsermap.vclDataSet.Active := false;
    FdsUsermap.vclDataSet.Active := true;

    MapRecord := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']).GetMapRecord;

    CheckCondition(MapRecord.MapType = mtpMap, 'Wrong map type');

    if Trim(MapRecord.MapValues) <> '' then
    begin
      tmpList := TStringList.Create;
      try
        tmpList.Text := MapRecord.MapValues;
        for i := 0 to tmpList.Count - 1 do
        begin
          FdsUsermap.Append;
          FdsUsermap['From'] := tmpList.Names[i];
          FdsUsermap['To'] := tmpList.Values[tmpList.Names[i]];
          FdsUsermap.Post;
        end;
      finally
        FreeAndNil(tmpList);
      end;
    end
    else
      if PackageFieldLOV.Items.Count > 0 then
      begin
        for i := 0 to PackageFieldLOV.Count - 1 do
        begin
          FdsUsermap.Append;
          FdsUsermap['To'] := PackageFieldLOV.Items.Names[i];
          FdsUsermap['Description'] := PackageFieldLOV.Items.Values[PackageFieldLOV.Items.Names[i]];
          FdsUsermap.Post;
        end;
      end;

    FdsUsermap.First;
  finally
    FdsUsermap.vclDataSet.EnableControls;
  end;
end;

procedure TEDIT_EVOX_MAP.evButton1Click(Sender: TObject);
var
  bResult: boolean;
  sFrom: String;
  sTo: String;
begin
  inherited;
  sFrom := '';
  sTo := '';
  bResult := ShowValueDialog('Adding', SFrom, sTo, false);
  if bResult then
  begin
    FdsUsermap.Append;
    if Trim(sFrom) <> '' then
      FdsUsermap['From'] := sFrom;
    if Trim(sTo) <> '' then
      FdsUsermap['To'] := sTo;
    FdsUsermap.Post;
    SaveMapTypeInfo(true);
  end;
end;

procedure TEDIT_EVOX_MAP.evButton2Click(Sender: TObject);
var
  bResult: boolean;
  sFrom: String;
  sTo: String;
begin
  inherited;
  if not FdsUsermap.eof then
  begin
    if FdsUsermap['From'] <> null then
      sFrom := FdsUsermap['From']
    else
      sFrom := '';

    if FdsUsermap['To'] <> null then
      sTo := FdsUsermap['To']
    else
      sTo := '';

    if (PackageFieldLOV.Items.Count > 0) and (PackageFieldLOV.Items.IndexOfName(sTo) <> -1) then
      bResult := ShowValueDialog('Editing', SFrom, sTo, true)
    else
      bResult := ShowValueDialog('Editing', SFrom, sTo, false);
      
    if bResult then
    begin
      FdsUsermap.Edit;
      if Trim(sFrom) <> '' then
        FdsUsermap['From'] := sFrom;
      if Trim(sTo) <> '' then
        FdsUsermap['To'] := sTo;
      FdsUsermap.Post;
      SaveMapTypeInfo(true);
    end;
  end;  
end;

procedure TEDIT_EVOX_MAP.evButton3Click(Sender: TObject);
begin
  inherited;
  if not FdsUsermap.eof then
  begin
    FdsUsermap.Delete;
    SaveMapTypeInfo(true);
  end;
end;

procedure TEDIT_EVOX_MAP.UserMapGridDblClick(Sender: TObject);
begin
  inherited;
  evButton2.Click;
end;

procedure TEDIT_EVOX_MAP.CheckMapButtonClick(Sender: TObject);
var
  CheckResult: IisListOfValues;
  ErrorsList: IEvExchangeResultLog;
begin
  inherited;
  CheckResult := Context.EvolutionExchange.CheckMapFile(FMapFile, nil);
  ErrorsList := IInterface(CheckResult.FindValue(EvoXExtErrorsList).Value) as IEvExchangeResultLog;
  try
    ErrorDetailsmemo.Lines.Clear;
    ErrorsLogDataSource.DataSet := nil;
    if ErrorsList.Count = 0 then
    begin
      ClearErrosDataset;
      ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
      EvMessage('Map file is OK', mtWarning, [mbOk], mbOk);
      SaveLogBtn.Enabled := false;
    end
    else
    begin
      ErrorsFromListToDataset(ErrorsList);
      evPageControl1.ActivePage := ErrorlogSheet;
      ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
      EvMessage('Map file has errors', mtWarning, [mbOk], mbOk);
      SaveLogBtn.Enabled := true;
    end;
  finally
    ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
  end;
end;

procedure TEDIT_EVOX_MAP.ImportNowButtonClick(Sender: TObject);
var
  ImportResult: IisListOfValues;
  ErrorsList: IEvExchangeResultLog;
  InputData: IevDataSet;
  i: integer;
  FormatsList: IisStringListRO;
  sName, sExt: String;
  sFilter: String;
  Reader: IEvExchangeDataReader;
  iAllRows, iImportedRows: integer;
  bCustomCompanyAssigned, bClientAssigned: boolean;
  sCustomCoNbr: String;
  sCompanyName: String;
  S, sHowToHandleNulls: String;
  StartTime, ElapsedTime: TdateTime;
  sElapsedTime: String;
begin
  inherited;

  MapFromScreenToObject;


  bCustomCompanyAssigned := Assigned(FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField));
  bClientAssigned := Assigned(FMapFile.GetFieldByExchangeName(NewCustomClientNbrPackageField));

  if not bCustomCompanyAssigned and not bClientAssigned then
  begin
    if Contains(FPackage.GetName, EvoXClient) then
      EvMessage('You should map input value for "Client Code". Import cancelled', mtWarning, [mbOk], mbOk)
    else
      EvMessage('You should map input value for "Custom Company Nbr". Import cancelled', mtWarning, [mbOk], mbOk);
    exit;
  end;

  // select imput data file
  FormatsList := FReader.GetSupportedFormatFileExt;
  sFilter := '';
  for i := 0 to FormatsList.Count - 1 do
  begin
    sExt := FormatsList.Names[i];
    sName := FormatsList.Values[sExt];
    sFilter := sFilter + sName + ' (' + sExt + ')|' + sExt + '|';
  end;

  if sFilter <> '' then
    ImportDataDialog.Filter := sFilter
  else
    ImportDataDialog.Filter := 'Any file (*.*)|*.*';

  if not ImportDataDialog.Execute then
    exit;

  // user's confirmation
  if bCustomCompanyAssigned or bClientAssigned then
  begin
    if not (EvMessage('Input file: "' + ImportDataDialog.FileName + '"' + #13#10 +
      'Do you want to start import?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
      exit;
  end
  else
  begin
    if not (EvMessage('One company inport' + #13#10 + 'Company#: "' + sCustomCoNbr + '"' + #13#10 +
      'Company name: "' + sCompanyName + '"' + #13#10 + 'Input file: "' + ImportDataDialog.FileName + '"' + #13#10 +
      'Do you want to start import?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
      exit;
  end;

  // reading input data to dataset
  ctx_StartWait('Reading and parsing input file...');
  try
    Reader := Context.EvolutionExchange.GetDataReader(FMapFile.GetDataSourceName);
  finally
    ctx_EndWait;
  end;
  ErrorsList := TEvExchangeResultLog.Create;
  Reader.SetDataReaderOptions(FMapFile.GetDataSourceOptions);
  InputData := Reader.ReadDataFromFile(ImportDataDialog.FileName, FMapFile, ErrorsList);

  if ErrorsList.Count > 0 then
  begin
    ErrorsFromListToDataset(ErrorsList);
    evPageControl1.ActivePage := ErrorlogSheet;
    SaveLogBtn.Enabled := true;
    EvMessage('Cannot read input data. Import stopped', mtWarning, [mbOk], mbOk);
    exit;
  end
  else
    SaveLogBtn.Enabled := false;

  S := 'Input file contains ' + IntToStr(InputData.RecordCount) + ' rows.' + #13#10;
  sHowToHandleNulls := FMapFile.GetMapFileOptions.Values[EvoXHowToHandleNullsOption];
  if sHowToHandleNulls = EvoXIgnoreNulls then
    S := S + 'NULL values will be IGNORED during updates of existing records'
  else if sHowToHandleNulls = EvoXReplaceValueByNull then
    S := S + 'NULL values will be USED during updates of existing records'
  else if sHowToHandleNulls = EvoXUseSpecialNullString then
    S := S + 'If imput value equals "' + FMapFile.GetMapFileOptions.Values[EvoXSpecialNullStringOption] + '" it will be USED as NULL during updates of existing records';
  S := S + #13#10 + 'Do you want to continue?';
  if not (EvMessage(S, mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
    exit;

  // import
  ctx_StartWait('Importing...');
  StartTime := SysTime;
  try
    if bCustomCompanyAssigned or bClientAssigned then
      ImportResult := Context.EvolutionExchange.Import(InputData, FMapFile, nil)
    else
      ImportResult := Context.EvolutionExchange.Import(InputData, FMapFile, nil, sCustomCoNbr);
  finally
    ctx_EndWait;
  end;
  ElapsedTime := Abs(SysTime - StartTime);
  sElapsedTime := 'Elapsed time: '+ FormatDateTime('hh:nn:ss', ElapsedTime);

  ErrorsList := IInterface(ImportResult.FindValue(EvoXExtErrorsList).Value) as IEvExchangeResultLog;
  try
    ErrorsLogDataSource.DataSet := nil;
    ErrorDetailsmemo.Lines.Clear;
    if ErrorsList.Count = 0 then
    begin
      ClearErrosDataset;
      SaveLogBtn.Enabled := false;
      iAllRows := ImportResult.Value[EvoXInputRowsAmount];
      iImportedRows := ImportResult.Value[EvoXImportedRowsAmount];;
      ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
      EvMessage('Import finished successfully' + #13#10 +'Rows processed: ' + IntToStr(iAllRows) + #13#10
        + 'Imported rows: ' + IntToStr(iImportedRows) + #13#10 + sElapsedTime , mtWarning, [mbOk], mbOk);
    end
    else
    begin
      ErrorsFromListToDataset(ErrorsList);
      SaveLogBtn.Enabled := true;
      evPageControl1.ActivePage := ErrorlogSheet;
      ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
      EvMessage('Import finished with problems' + #13#10 + 'Found ' + IntToStr(ErrorsList.GetAmountOfErrors) + ' error(s) and ' +
        IntToStr(ErrorsList.GetAmountOfWarnings) + ' warning(s)' + #13#10 + sElapsedTime, mtWarning, [mbOk], mbOk);
    end;
  finally
    ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;
  end;
  ctx_DataAccess.ClearCache;
end;

procedure TEDIT_EVOX_MAP.ErrorsFromListToDataset(const AList: IEvExchangeResultLog);
var
  i: integer;
begin
  FdsErrorsLog.vclDataSet.DisableControls;
  try
    FdsErrorsLog.vclDataSet.Active := false;
    FdsErrorsLog.vclDataSet.Active := true;
    for i := 0 to AList.Count - 1 do
    begin
      FdsErrorsLog.Append;
      FdsErrorsLog['Error#'] := i + 1;
      FdsErrorslog['Error message'] := AList.Items[i].GetText;
      FdsErrorsLog['ErrorType'] := Integer(AList.Items[i].GetEventType);
      FdsErrorsLog.Post;
    end;
  finally
    FdsErrorsLog.vclDataSet.EnableControls;
  end;
  if FdsErrorsLog.RecordCount > 0 then
    FdsErrorsLog.First;
end;

procedure TEDIT_EVOX_MAP.ClearErrosDataset;
begin
  FdsErrorsLog.vclDataSet.DisableControls;
  try
    FdsErrorsLog.vclDataSet.Active := false;
    FdsErrorsLog.vclDataSet.Active := true;
  finally
    FdsErrorsLog.vclDataSet.EnableControls;
  end;
  ErrorDetailsmemo.Lines.Clear;
end;

function TEDIT_EVOX_MAP.GetExampleData(const AInputFieldName: String): Variant;
var
  SavePlace: TBookmark;
begin
  Result := null;
  SavePlace := FdsInputFields.vclDataSet.GetBookmark;
  try
    FdsInputFields.vclDataSet.DisableControls;
    try
      FdsInputFields.First;
      if FdsInputFields.vclDataSet.Locate('FieldName', AInputFieldName, [loCaseInsensitive	]) then
        if FdsInputFields['Example'] <> null then
          Result := Copy(FdsInputFields['Example'],1, 50);
    finally
      FdsInputFields.vclDataSet.EnableControls;
    end;
  finally
    try
      try
        FdsInputFields.vclDataSet.GotoBookmark(SavePlace);
      finally
        FdsInputFields.vclDataSet.FreeBookmark(SavePlace);
      end;
    except
      on E:Exception do
        FdsInputFields.First;
    end;
  end;
end;

{ TEvExchangeDataSet }

procedure TEDIT_EVOX_MAP.RefreshGroupsGUI;
var
  ItIsGroup: string;
begin
  ItIsGroup := '0';
  if (not FdsMappedFields.vclDataSet.Active) or (FdsMappedFields['ItIsGroup'] = '0') then
  begin
    OpenCloseGroupBtn.Enabled := false;
    MapFieldButton.Enabled := true;
    UnmapFieldButton.Enabled := true;
    UnmapAllButton.Enabled := true;
  end
  else
  begin
    ItIsGroup := VarToStr(FdsMappedFields['ItIsGroup']);
    OpenCloseGroupBtn.Enabled := true;
    MapFieldButton.Enabled := false;
    UnmapFieldButton.Enabled := false;
    UnmapAllButton.Enabled := false;
  end;

  if FOpened.Name = '' then
  begin
    AddRecBtn.Visible := false;
    DelRecBtn.Visible := false;
    if ItIsGroup = '1' then
      OpenCloseGroupBtn.Caption := 'Open Group'
    else
      OpenCloseGroupBtn.Caption := 'Open Section';
    if (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).Active and (FOpened.RecNo > -1) then
    begin
      (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).RecNo := FOpened.RecNo;
      FOpened.RecNo := -1;
    end;
  end
  else
  begin
    OpenCloseGroupBtn.Caption := StringReplace(OpenCloseGroupBtn.Caption, 'Open', 'Close', []);
    OpenCloseGroupBtn.Enabled := true;
    if OpenCloseGroupBtn.Caption = 'Close Group' then
    begin
      AddRecBtn.Visible := true;
      DelRecBtn.Visible := true;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.OpenCloseGroupBtnClick(Sender: TObject);
begin
  inherited;
  if FOpened.Name = '' then
  begin
    if (FdsMappedFields['ExchangeField'] <> null) then
    begin
      FOpened.RecNo := (FdsMappedFields.vclDataSet as TEvBasicClientDataSet).RecNo;
      FOpened.Name := FdsMappedFields['ExchangeField'];
      FOpened.IsGroup := FdsMappedFields['ItIsGroup'] = '1';
      if FOpened.IsGroup then
      begin
        if FPackage.GetFieldsDefsByGroupName(FOpened.Name).Count = 0 then
        begin
          EvMessage('Cannot open: Group is empty', mtWarning, [mbOk], mbOk);
          FOpened.Name := '';
        end;
      end
      else begin
        if FPackage.GetFieldsDefsBySectionName(FOpened.Name).Count = 0 then
        begin
          EvMessage('Cannot open: Section is empty', mtWarning, [mbOk], mbOk);
          FOpened.Name := '';
        end;
      end;
    end;
  end
  else // close group/section
    FOpened.Name := '';

  FillPackageFieldsInMap;
  FillMappedFieldsInGrid;
  CalculateAssignedTimes;

  MappedFieldsgrid.OnRowChanged(MappedFieldsgrid);
  RefreshGroupsGUI;
end;

procedure TEDIT_EVOX_MAP.AddRecBtnClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  CheckCondition(FOpened.Name <> '', 'No open group');
  FPackage.AddNewRecordToGroup(FOpened.Name);
  FillPackageFieldsInMap;
  FillMappedFieldsInGrid;
  CalculateAssignedTimes;
  MappedFieldsgrid.OnRowChanged(MappedFieldsgrid);
end;

procedure TEDIT_EVOX_MAP.DelRecBtnClick(Sender: TObject);
var
  iRecordNumber: integer;
begin
  inherited;
  CheckCondition(FOpened.Name <> '', 'No open group');

  if (not FdsMappedFields.eof) and (FdsMappedFields['ExchangeField'] <> null) then
  begin
    iRecordNumber := FdsMappedFields['GroupRecordNumber'];
    if (EvMessage('Do you want to delete record #' + IntToStr(iRecordNumber) + '?', mtWarning, [mbYes, MbNo], mbNo) = mrYes) then
    begin
      FMapFileChanged := true;

      // unmapping fields
      FdsMappedFields.vclDataSet.DisableControls;
      try
        FdsMappedFields.First;
        while not FdsMappedFields.Eof do
        begin
          if FdsMappedFields['GroupRecordNumber'] = iRecordNumber then
            FMapFile.DeleteFieldByExchangeName(FdsMappedFields['ExchangeField']);
          FdsMappedFields.Next;
        end;
      finally
        FdsMappedFields.vclDataSet.EnableControls;
      end;

      // deleting group from package
      FPackage.DeleteRecordFromGroup(FOpened.Name, iRecordNumber);
      FillPackageFieldsInMap;
      FillMappedFieldsInGrid;
      CalculateAssignedTimes;
      MappedFieldsgrid.OnRowChanged(MappedFieldsgrid);
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.MappedFieldsgridDblClick(Sender: TObject);
begin
  inherited;
  if OpenCloseGroupBtn.Enabled and (FOpened.Name = '') then
    OpenCloseGroupBtn.Click;
end;

procedure TEDIT_EVOX_MAP.AddFixedFieldBtnClick(Sender: TObject);
begin
  inherited;
  FFixedFieldsDataSet.Append;
  FixedFieldsGrid.SetFocus;
end;

procedure TEDIT_EVOX_MAP.DeleteFixedFieldBtnClick(Sender: TObject);
begin
  inherited;
  if not FFixedFieldsDataSet.eof then
    FFixedFieldsDataSet.Delete;
  FixedFieldsGrid.SetFocus;
end;

function TEDIT_EVOX_MAP.GetFixedFieldsDefs: IisListOfValues;
var
  FieldDef: IFixedReaderFieldDefinition;
  SavePlace: TBookmark;
  FieldNames: IisStringList;
begin
  Result := TisListOfValues.Create;
  FieldNames := TisStringList.Create;

  try
    SavePlace := FFixedFieldsDataSet.vclDataSet.GetBookmark;
    try
      FFixedFieldsDataSet.vclDataSet.DisableControls;
      FFixedFieldsDataSet.First;
      while not FFixedFieldsDataSet.Eof do
      begin
        CheckCondition(FFixedFieldsDataSet[FixedFieldStart] > 0, 'Field: ' + FFixedFieldsDataSet[FixedFieldName] +
          '. Wrong start position: ' + intToStr(FFixedFieldsDataSet[FixedFieldStart]));
        CheckCondition(FFixedFieldsDataSet[FixedFieldEnd] > 0, 'Field: ' + FFixedFieldsDataSet[FixedFieldName] +
          '. Wrong end position: ' + intToStr(FFixedFieldsDataSet[FixedFieldEnd]));
        FieldDef := TFixedReaderFieldDefinition.Create;
        FieldDef.SetName(FFixedFieldsDataSet[FixedFieldName]);
        FieldDef.SetPositions(FFixedFieldsDataSet[FixedFieldStart], FFixedFieldsDataSet[FixedFieldEnd]);

        CheckCondition(not Result.ValueExists(FieldDef.StartPosAsString),
          'Two fields with the same start position are not allowed. Position: ' + IntToStr(FieldDef.GetStartPos));
        CheckCondition(FieldNames.IndexOf(FieldDef.GetName) = -1, 'You cannot have two fields with the same name: "' + FieldDef.GetName + '"');

        FieldNames.Add(FieldDef.GetName);
        Result.AddValue(FieldDef.StartPosAsString, FieldDef);

        FFixedFieldsDataSet.Next;
      end;

      (FReader as IevFixedReader).CheckForOverlapping(Result);
    finally
      FFixedFieldsDataSet.vclDataSet.EnableControls;
    end;
  finally
    try
      try
        FFixedFieldsDataSet.vclDataSet.GotoBookmark(SavePlace);
      finally
        FFixedFieldsDataSet.vclDataSet.FreeBookmark(SavePlace);
      end;
    except
      on E:Exception do
        FFixedFieldsDataSet.First;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.ApplyBtnClick(Sender: TObject);
var
  FieldsDefs: IisListOfValues;
  Options: IisStringList;
begin
  inherited;
  FMapFileChanged := true;

  if (FFixedFieldsDataSet.vclDataset.State = dsEdit) or (FFixedFieldsDataSet.vclDataset.State = dsInsert) then
    FFixedFieldsDataSet.Post;

  FieldsDefs := GetFixedFieldsDefs;
  Options := FMapFile.GetDataSourceOptions;
  Options.Values[fieldsList] := (FReader as IevFixedReader).FieldDefinitionsAsString(FieldsDefs);
  FMapFile.SetDataSourceOptions(Options);
  DSOptionsFromObjectToSheet;

  if FExampleFileName <> '' then
    LoadExample(FExampleFileName, RowsToLoad.Value, true)
end;

procedure TEDIT_EVOX_MAP.Button1Click(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  DSOptionsFromObjectToSheet;
end;

procedure TEDIT_EVOX_MAP.LoadExample(const AFileName: String;  const ARowsToLoad: integer; const ASilentMode: boolean);
var
  ErrorsList: IEvExchangeResultLog;
  i: integer;
begin
  DSOptionsFromSheetToObject;
  dsExample.DataSet := nil;

  if not FileExists(AFileName) then
  begin
    ShowMessage('Cannot read example data because file does not exist');
    exit;
  end;

  if Assigned(FExampleDataSet) then
    FExampleDataSet.vclDataSet.Active := false;
  ErrorsList := TEvExchangeResultLog.Create;
  FReader.SetDataReaderOptions(FMapFile.GetDataSourceOptions);
  FExampleDataSet := FReader.ReadExampleFromFile(AFileName, ErrorsList, ARowsToLoad);
  dsExample.DataSet := FExampleDataSet.vclDataSet;

  if ErrorsList.Count = 0 then
  begin
    PrepareInputFieldsList;
    FillMappedFieldsInGrid;
    CalculateAssignedTimes;
    if not ASilentMode then
      ShowMessage('Example data loaded successfully');
  end
  else
  begin
    FExampleDataSet := TevDataSet.Create;
    FExampleDataSet.vclDataSet.FieldDefs.Add('Error message', ftString, 1024, false);
    FExampleDataSet.vclDataSet.Active := true;
    for i := 0 to ErrorsList.Count - 1 do
    begin
      FExampleDataSet.Append;
      FExampleDataSet.vclDataSet.Fields[0].Value := Copy(ErrorsList.Items[i].GetText, 1, 1000);
      FExampleDataSet.Post;
    end;
    dsExample.DataSet := FExampleDataSet.vclDataSet;
    SaveExampleWithMapCheck.Checked := false;
    WinExec(PChar('notepad.exe "' + FExampleFileName + '"'), SW_SHOWNORMAL);
    ShowMessage('Example data loaded with errors. See errors in grid instead of data');
  end;
end;

procedure TEDIT_EVOX_MAP.MappedFieldsgridCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if (not ((not FdsMappedFields.vclDataSet.Active) or (FdsMappedFields['ItIsGroup'] = '0')))
    and (Field.FieldName = 'DisplayName') then
  begin
    AFont.Style := [fsBold];
    if (FdsMappedFields['ItIsGroup'] = '2') then
      AFont.Color := clBlue;
  end;
end;

procedure TEDIT_EVOX_MAP.DBNavigator1Click(Sender: TObject;  Button: TNavigateBtn);
begin
  inherited;
  FMapFileChanged := true;
  if Button = nbInsert then
    FixedFieldsGrid.SetActiveField(FixedFieldName);
end;

procedure TEDIT_EVOX_MAP.EnableAsOfDateCheckClick(Sender: TObject);
begin
  inherited;
  if not FNoSaveAsOfDateEnabled then
    FMapFileChanged := true;
  ManageAsOfDateGUI;
  SaveAsOFDateInfo;
end;

procedure TEDIT_EVOX_MAP.ManageAsOfDateGUI;
begin
  AsOFDateGb1.Enabled := EnableAsOfDateCheck.Checked;
  if EnableAsOfDateCheck.Checked then
  begin
    if UseDateEditorRb.Checked then
    begin
      AsOfDateEditor.Enabled := true;
      AsOfdateEditorBtn.Enabled := true;
      AsOfDateFields.Enabled := false;
      AsOfDateFormatsList.Enabled := false;
    end
    else
    begin
      AsOfDateEditor.Enabled := false;
      AsOfdateEditorBtn.Enabled := false;
      AsOfDateFields.Enabled := true;
      AsOfDateFormatsList.Enabled := true;
    end;
  end
  else
  begin
    AsOFDateGb1.Enabled := false;
    UseDateEditorRb.Checked := true;
    AsOfDateFields.ItemIndex := -1;
    AsOfDateFormatsList.ItemIndex := -1;
    AsOfDateEditor.Date := Today;
  end;
end;

procedure TEDIT_EVOX_MAP.UseDateEditorRbClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  ManageAsOfDateGUI;
  SaveAsOFDateInfo;
end;

procedure TEDIT_EVOX_MAP.UseFieldsValueRbClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  ManageAsOfDateGUI;
  SaveAsOFDateInfo;
end;

procedure TEDIT_EVOX_MAP.ReadAsOfDateInfo;
var
  MapField: IevEchangeMapField;
begin
  FNoSaveAsOfDateEnabled := true;
  try
    MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
    EnableAsOfDateCheck.Enabled := true;
    EnableAsOfDateCheck.Checked := MapField.GetAsOfDateEnabled;

    if MapField.GetAsOfDateEnabled then
    begin
      if MapField.GetAsOfDateSource = stDateEditor then
      begin
        UseDateEditorRb.Checked := true;
        AsOfDateEditor.DateTime := MapField.GetAsOfDateValue;
        AsOfDateFields.ItemIndex := -1;
        AsOfDateFormatsList.ItemIndex := -1;
      end
      else
      begin
        UseFieldsValueRb.Checked := true;
        FindAndSelectValueInlist(AsOfDateFields, MapField.GetAsOfDateField);
        FindAndSelectValueInlist(AsOfDateFormatsList, MapField.GetAsOfDateFieldDateFormat);
      end;
    end;

    ManageAsOfDateGUI;
  finally
    FNoSaveAsOfDateEnabled := false;
  end;
end;

procedure TEDIT_EVOX_MAP.AsOfDateFieldsChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  SaveAsOFDateInfo;
  CalculateAssignedTimes;
end;

procedure TEDIT_EVOX_MAP.AsOfDateEditorChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  SaveAsOFDateInfo;
end;

procedure TEDIT_EVOX_MAP.AsOfDateFormatsListChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  SaveAsOFDateInfo;
end;

procedure TEDIT_EVOX_MAP.SaveAsOFDateInfo;
var
  MapField: IevEchangeMapField;
begin
  if FNoSaveAsOfDateEnabled then
    exit;
  MapField := FMapFile.GetFieldByExchangeName(FdsMappedFields['ExchangeField']);
  MapField.SetAsOfDateEnabled(EnableAsOfDateCheck.Checked);
  if MapField.GetAsOfDateEnabled then
    if UseDateEditorRb.Checked then
    begin
      MapField.SetAsOfDateSource(stDateEditor);
      MapField.SetAsOfDateValue(AsOfDateEditor.Date);
    end
    else
    begin
      MapField.SetAsOfDateSource(stMappedField);
      MapField.SetAsOfDateField(Trim(AsOfDateFields.Text));
      MapField.SetAsOfDateFieldDateFormat(Trim(AsOfDateFormatsList.Text));
    end;
  SetAsOfDateValueInGrid(MapField, true);
end;

procedure TEDIT_EVOX_MAP.SetAsOfDateValueInGrid(const AMapField: IevEchangeMapField; const ADoEditAndPost: boolean);
var
  PackageField: IEvExchangePackageField;
begin
  // checking package level
  PackageField := FPackage.GetFieldByName(FdsMappedFields['ExchangeField']);
  if ADoEditAndPost then
    FdsMappedFields.Edit;
  if PackageField.GetAsOfDateEnabled then
    FdsMappedFields['AsOfDate'] := 'Not Defined'
  else
    FdsMappedFields['AsOfDate'] := 'Not Applicable';
  if ADoEditAndPost then
    FdsMappedFields.Post;

  // checking map field level
  if FdsMappedFields['AsOfDate'] <> 'Not Applicable' then
  begin
    if Assigned(AMapField) and AMapField.GetAsOfDateEnabled then
    begin
      if ADoEditAndPost then
        FdsMappedFields.Edit;
      if AMapField.GetAsOfDateSource = stDateEditor then
        FdsMappedFields['AsOfDate'] := DateToStr(AMapField.GetAsOfDateValue)
      else
        if Trim(AMapField.GetAsOfDateField) <> '' then
          FdsMappedFields['AsOfDate'] := Copy(AMapField.GetAsOfDateField, 1, InputFieldSize)
        else
          FdsMappedFields['AsOfDate'] := 'Choose the field!';
      if ADoEditAndPost then
        FdsMappedFields.Post;
    end;
  end;
end;

procedure TEDIT_EVOX_MAP.AsOfdateEditorBtnClick(Sender: TObject);
var
  tmpDate: TDate;
begin
  inherited;
  tmpDate := ShowAsOfDateDlg(AsOfDateEditor.Date);
  if tmpDate <> AsOfDateEditor.Date then
  begin
    AsOfDateEditor.Date := tmpDate;
    FMapFileChanged := true;
    SaveAsOFDateInfo;
  end;
end;

{ TEvExchangeDataSet }

function TEvExchangeDataSet.CreateVCLDataSet: TisKbmMemDataSet;
begin
  Result := TEvBasicClientDataSet.Create(nil);
end;

procedure TEDIT_EVOX_MAP.IgnoreNullRbClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  NullValuePatternEdit.Enabled := false;
end;

procedure TEDIT_EVOX_MAP.ImportNullsRbClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  NullValuePatternEdit.Enabled := false;
end;

procedure TEDIT_EVOX_MAP.ImportNullPatternRbClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
  NullValuePatternEdit.Enabled := true;
  if Trim(NullValuePatternEdit.Text) = '' then
    NullValuePatternEdit.Text := 'NULL';
end;

procedure TEDIT_EVOX_MAP.NullValuePatternEditChange(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.FillMapSettings;
var
  MapOptions: IisStringList;
  S: String;
begin
  // how to handle nulls
  NullValuePatternEdit.Text := 'NULL';
  MapOptions := FMapFile.GetMapFileOptions;
  S := MapOptions.Values[EvoXHowToHandleNullsOption];
  if S = EvoXIgnoreNulls then
  begin
    IgnoreNullRb.Checked := true;
    NullValuePatternEdit.Enabled := false;
  end
  else if S = EvoXReplaceValueByNull then
  begin
    ImportNullsRb.Checked := true;
    NullValuePatternEdit.Enabled := false;
  end
  else if S = EvoXUseSpecialNullString then
  begin
    ImportNullPatternRb.Checked := true;
    NullValuePatternEdit.Enabled := true;
    S := MapOptions.Values[EvoXSpecialNullStringOption];
    if Trim(S) <> '' then
      NullValuePatternEdit.Text := Trim(S);
  end
  else
    raise Exception.Create('Unknown value for Null values option: "' + S + '"');

  // payroll package options
  InitGenericPayrollOptions;
  if MapOptions.IndexOfName(EvoXAutoCheckParam) <> -1 then
    CreateAutoCheckCheckBox.Checked := AnsiSameText(MapOptions.Values[EvoXAutoCheckParam], 'TRUE');
  if MapOptions.IndexOfName(EvoXPaySalaryParam) <> -1 then
  begin
    if not AnsiSameText(MapOptions.Values[EvoXPaySalaryParam], 'TRUE') then
      rgrpPaySalary.ItemIndex := 1
  end;
  if MapOptions.IndexOfName(EvoXPayStandHoursParam) <> -1 then
  begin
    if not AnsiSameText(MapOptions.Values[EvoXPayStandHoursParam], 'TRUE') then
      rgrpPayHours.ItemIndex := 1;
  end;
  if MapOptions.IndexOfName(EvoXLoadPayrollDefaultsParam) <> -1 then
  begin
    if not AnsiSameText(MapOptions.Values[EvoXLoadPayrollDefaultsParam], 'TRUE') then
      rgrpLoadDefaults.ItemIndex := 1;
  end;
  if MapOptions.IndexOfName(EvoXIncludeTOAParam) <> -1 then
  begin
    if AnsiSameText(MapOptions.Values[EvoXIncludeTOAParam], 'TRUE') then
      rgrpIncludeTOA.ItemIndex := 0;
  end;
end;

procedure TEDIT_EVOX_MAP.SaveMapSettings;
var
  MapOptions: IisStringList;
  sValue, sNullPattern: String;
begin
  // how to handle nulls
  MapOptions := FMapFile.GetMapFileOptions;
  if IgnoreNullRb.Checked then
  begin
    sValue := EvoXIgnoreNulls;
    sNullPattern := '';
  end
  else if ImportNullsRb.Checked then
  begin
    sValue := EvoXReplaceValueByNull;
    sNullPattern := '';
  end
  else
  begin
    sValue := EvoXUseSpecialNullString;
    sNullPattern := Trim(NullValuePatternEdit.Text);
    CheckCondition(sNullPattern <> '', 'Pattern cannot be empty string');
  end;
  if MapOptions.IndexOfName(EvoXHowToHandleNullsOption) = - 1 then
    MapOptions.Add(EvoXHowToHandleNullsOption + '=' + sValue)
  else
    MapOptions.Values[EvoXHowToHandleNullsOption] := sValue;
  if MapOptions.IndexOfName(EvoXSpecialNullStringOption) = - 1 then
    MapOptions.Add(EvoXSpecialNullStringOption + '=' + sNullPattern)
  else
    MapOptions.Values[EvoXSpecialNullStringOption] := sNullPattern;

  // generic payroll import options
  if AnsiSameText(FPackage.GetName, EvoGenericPrPackage) then
  begin
    if MapOptions.IndexOfName(EvoXAutoCheckParam) = - 1 then
      MapOptions.Add(EvoXAutoCheckParam + '=' + SysUtils.BoolToStr(CreateAutoCheckCheckBox.Checked, true))
    else
      MapOptions.Values[EvoXAutoCheckParam] := SysUtils.BoolToStr(CreateAutoCheckCheckBox.Checked, true);

    if MapOptions.IndexOfName(EvoXPaySalaryParam) = - 1 then
      MapOptions.Add(EvoXPaySalaryParam + '=' + SysUtils.BoolToStr(rgrpPaySalary.ItemIndex = 0, true))
    else
      MapOptions.Values[EvoXPaySalaryParam] := SysUtils.BoolToStr(rgrpPaySalary.ItemIndex = 0, true);

    if MapOptions.IndexOfName(EvoXPayStandHoursParam) = - 1 then
      MapOptions.Add(EvoXPayStandHoursParam + '=' + SysUtils.BoolToStr(rgrpPayHours.ItemIndex = 0, true))
    else
      MapOptions.Values[EvoXPayStandHoursParam] := SysUtils.BoolToStr(rgrpPayHours.ItemIndex = 0, true);

    if MapOptions.IndexOfName(EvoXLoadPayrollDefaultsParam) = - 1 then
      MapOptions.Add(EvoXLoadPayrollDefaultsParam + '=' + SysUtils.BoolToStr(rgrpLoadDefaults.ItemIndex = 0, true))
    else
      MapOptions.Values[EvoXLoadPayrollDefaultsParam] := SysUtils.BoolToStr(rgrpLoadDefaults.ItemIndex = 0, true);

    if MapOptions.IndexOfName(EvoXIncludeTOAParam) = - 1 then
      MapOptions.Add(EvoXIncludeTOAParam + '=' + SysUtils.BoolToStr(rgrpIncludeTOA.ItemIndex = 0, true))
    else
      MapOptions.Values[EvoXIncludeTOAParam] := SysUtils.BoolToStr(rgrpIncludeTOA.ItemIndex = 0, true);
  end;
end;

procedure TEDIT_EVOX_MAP.ErrorsLogGridRowChanged(Sender: TObject);
begin
  inherited;
  if Assigned(FdsErrorsLog) and (FdsErrorsLog.RecordCount > 0) and (FdsErrorsLog['Error message'] <> null) then
    ErrorDetailsmemo.Text := FdsErrorsLog['Error message']
  else
    ErrorDetailsmemo.Lines.Clear;
end;

procedure TEDIT_EVOX_MAP.ApplySecurityFunctions;
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_EDIT_EVOX_MAP') <> stEnabled then
  begin
    NewButton.Enabled := false;
    SaveButton.Enabled := false;
    SaveAsButton.Enabled := false;

    DataSourceSheet.Enabled := false;
    MappingSheet.Enabled := false;
    MapSettingsSheet.Enabled := false;
  end;
  if ctx_AccountRights.Functions.GetState('USER_CAN_IMPORT_USING_EVOX') <> stEnabled then
  begin
    ImportNowButton.Enabled := false;
  end;
end;

procedure TEDIT_EVOX_MAP.SaveLogBtnClick(Sender: TObject);
var
  F: TextFile;
begin
  inherited;
  if (FdsErrorsLog = nil) or (not FdsErrorsLog.vclDataSet.Active) or (FdsErrorsLog.RecordCount = 0) then
  begin
    EvMessage('Log is empty. Nothing to save', mtWarning, [mbOk], mbOk);
    exit;
  end;

  SaveLogDlg.FileName := ChangeFileExt(FMapFileName, '.log');
  if not SaveLogDlg.Execute then
    exit;

  FdsErrorsLog.vclDataSet.DisableControls;
  try
    AssignFile(F, SaveLogDlg.FileName);
    Rewrite(F);
    try
      FdsErrorsLog.First;
      while not FdsErrorsLog.Eof do
      begin
        Writeln(F, GetPrefixByType(TLogEventType(FdsErrorsLog['ErrorType'])) + FdsErrorsLog['Error message']);
        FdsErrorsLog.Next;
      end;
      FdsErrorsLog.First;
    finally
      CloseFile(F);
    end;
  finally
    FdsErrorsLog.vclDataSet.EnableControls;
  end;
end;

procedure TEDIT_EVOX_MAP.ErrorsLogGridCalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
var
  EventType: TLogEventType;
begin
  inherited;
  if (FdsErrorsLog = nil) or (not FdsErrorsLog.vclDataSet.Active) or (FdsErrorsLog.RecordCount = 0) then
    exit;

  if Field.FieldName = 'Error#' then
  begin
    EventType := TLogEventType(FdsErrorsLog['ErrorType']);
    if (EventType = etError) or (EventType = etFatalError) then
      ABrush.Color := clRed
    else if (EventType = etWarning) then
      ABrush.Color := clYellow;
  end;
end;

procedure TEDIT_EVOX_MAP.CreateAutoCheckCheckBoxClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;

  if CreateAutoCheckCheckBox.Checked then
    ParamsGroupBox.Enabled := true
  else
    ParamsGroupBox.Enabled := false;
end;

procedure TEDIT_EVOX_MAP.InitGenericPayrollOptions;
begin
  CreateAutoCheckCheckBox.Checked := false;
  rgrpPaySalary.ItemIndex := 0;
  rgrpPayHours.ItemIndex := 0;
  rgrpLoadDefaults.ItemIndex := 0;
  rgrpIncludeTOA.ItemIndex := 1;

  if AnsiSameText(FPackage.GetName, EvoGenericPrPackage) then
    gbGenericPayrollImportOptions.Visible := true
  else
    gbGenericPayrollImportOptions.Visible := false;
end;

procedure TEDIT_EVOX_MAP.rgrpPaySalaryClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

procedure TEDIT_EVOX_MAP.SaveExampleWithMapCheckClick(Sender: TObject);
begin
  inherited;
  FMapFileChanged := true;
end;

initialization
  RegisterClass(TEDIT_EVOX_MAP);
end.
