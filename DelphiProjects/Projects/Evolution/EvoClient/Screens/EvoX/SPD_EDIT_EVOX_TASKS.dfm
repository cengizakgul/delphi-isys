inherited EDIT_EVOX_TASKS: TEDIT_EVOX_TASKS
  Width = 779
  Height = 508
  AutoScroll = False
  AutoSize = False
  object evPageControl2: TevPageControl [0]
    Left = 0
    Top = 41
    Width = 779
    Height = 467
    ActivePage = TaskParametersSheet
    Align = alClient
    TabOrder = 0
    OnChange = evPageControl2Change
    object ScheduledTasksSheet: TTabSheet
      Caption = 'Scheduled Tasks'
      object evSplitter3: TevSplitter
        Left = 0
        Top = 207
        Width = 693
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object evPanel28: TevPanel
        Left = 0
        Top = 210
        Width = 693
        Height = 213
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object TaskLogGrid: TwwDBGrid
          Left = 0
          Top = 0
          Width = 693
          Height = 213
          DisableThemesInTitle = True
          LineStyle = gls3D
          ControlInfoInDataset = False
          Selected.Strings = (
            'EventType'#9'20'#9'Event Type'#9'F'
            'TimeStamp'#9'24'#9'Time Stamp'#9'F'
            'EventClass'#9'25'#9'Event Class'#9'F'
            'Text'#9'61'#9'Text'
            'Details'#9'8000'#9'Details')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = TaskLogFileDataSource
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Microsoft Sans Serif'
          Font.Style = []
          KeyOptions = []
          MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect]
          ParentFont = False
          PopupMenu = ShowLogDetails
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = RUSSIAN_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -11
          TitleFont.Name = 'Microsoft Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          UseTFields = False
          OnDblClick = TaskLogGridDblClick
          object wwLogGridIButton: TwwIButton
            Left = 0
            Top = 0
            Width = 13
            Height = 22
            AllowAllUp = True
          end
        end
      end
      object evPanel29: TevPanel
        Left = 0
        Top = 0
        Width = 693
        Height = 207
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object TaskListGrid: TevDBGrid
          Left = 0
          Top = 0
          Width = 693
          Height = 207
          DisableThemesInTitle = False
          Selected.Strings = (
            'TaskName'#9'20'#9'Task Name'#9'F'
            'Status'#9'8'#9'Status'#9'F'
            'Interval'#9'3'#9'Interval (min)'#9'F'
            'Pattern'#9'3'#9'Pattern'#9'F'
            'StopOnError'#9'3'#9'Stop On Error'#9'F'
            'FolderPath'#9'49'#9'Folder Path'#9'F'
            'Mapfile'#9'50'#9'Map File'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_EVOX_TASKS\TaskListGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = TasksListDataSource
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          OnDblClick = TaskListGridDblClick
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object TaskParametersSheet: TTabSheet
      Caption = 'Parameters'
      ImageIndex = 1
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 771
        Height = 439
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object evGroupBox3: TevGroupBox
          Left = 0
          Top = 0
          Width = 281
          Height = 53
          Caption = ' Task Status '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object StatusLabel: TevLabel
            Left = 11
            Top = 22
            Width = 47
            Height = 13
            Caption = 'Inactive'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ActivateTaskButton: TevButton
            Left = 94
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Activate'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = ActivateTaskButtonClick
          end
          object DeactivateTaskButton: TevButton
            Left = 190
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Deactivate'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = DeactivateTaskButtonClick
          end
        end
        object evGroupBox1: TevGroupBox
          Left = 0
          Top = 64
          Width = 689
          Height = 353
          Caption = ' Task Parameters '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          object CancelTskChangesBtn: TButton
            Left = 394
            Top = 318
            Width = 105
            Height = 25
            Caption = 'Cancel Changes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = CancelTskChangesBtnClick
          end
          object ApplyTaskChangesBtn: TButton
            Left = 138
            Top = 318
            Width = 105
            Height = 25
            Caption = 'Apply Changes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = ApplyTaskChangesBtnClick
          end
          object evGroupBox4: TevGroupBox
            Left = 8
            Top = 128
            Width = 673
            Height = 176
            Caption = ' Folders and Files '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object evLabel29: TevLabel
              Left = 10
              Top = 23
              Width = 199
              Height = 13
              Caption = '~Folder where data files are located'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel30: TevLabel
              Left = 97
              Top = 56
              Width = 120
              Height = 13
              Caption = '~Path to the map file'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel33: TevLabel
              Left = 108
              Top = 88
              Width = 109
              Height = 13
              Caption = '~Data files pattern'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel34: TevLabel
              Left = 40
              Top = 116
              Width = 177
              Height = 13
              Caption = '~Interval between imports (min)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel1: TevLabel
              Left = 344
              Top = 86
              Width = 237
              Height = 13
              Caption = 'Note: Files with *.bak extention are never imported'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel2: TevLabel
              Left = 19
              Top = 147
              Width = 197
              Height = 13
              Caption = '~Task status'#39's check interval (min)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object TaskMapFileEdit: TevEdit
              Left = 216
              Top = 52
              Width = 385
              Height = 21
              MaxLength = 200
              TabOrder = 2
            end
            object TaskDataFolderEdit: TevEdit
              Left = 216
              Top = 20
              Width = 385
              Height = 21
              MaxLength = 200
              TabOrder = 0
            end
            object evButton5: TevButton
              Left = 608
              Top = 51
              Width = 17
              Height = 25
              Caption = '...'
              TabOrder = 3
              OnClick = evButton5Click
            end
            object TaskFilesPatternEdit: TevEdit
              Left = 216
              Top = 83
              Width = 121
              Height = 21
              MaxLength = 30
              TabOrder = 4
              Text = '*.csv'
            end
            object IntervalEdit: TevSpinEdit
              Left = 216
              Top = 113
              Width = 57
              Height = 22
              MaxValue = 719
              MinValue = 1
              TabOrder = 5
              Value = 10
              OnChange = IntervalEditClick
            end
            object evButton1: TevButton
              Left = 608
              Top = 19
              Width = 17
              Height = 25
              Caption = '...'
              TabOrder = 1
              OnClick = evButton1Click
            end
            object TaskQueueInterval: TevSpinEdit
              Left = 216
              Top = 143
              Width = 57
              Height = 22
              MaxValue = 5
              MinValue = 1
              TabOrder = 6
              Value = 5
              OnChange = TaskQueueIntervalClick
            end
          end
          object evGroupBox5: TevGroupBox
            Left = 8
            Top = 21
            Width = 361
            Height = 100
            Caption = ' Task Name and Id '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object evLabel27: TevLabel
              Left = 32
              Top = 24
              Width = 65
              Height = 13
              Caption = '~Task Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel28: TevLabel
              Left = 24
              Top = 52
              Width = 74
              Height = 13
              Caption = 'Internal Task Id'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object TaskNameEdit: TevEdit
              Left = 104
              Top = 21
              Width = 129
              Height = 21
              MaxLength = 20
              TabOrder = 0
            end
            object TaskGUIDEdit: TEdit
              Left = 104
              Top = 48
              Width = 241
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
            end
            object SuspendOnErrorCheck: TevCheckBox
              Left = 103
              Top = 76
              Width = 194
              Height = 17
              Caption = 'Suspend task on any import error'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              OnClick = SuspendOnErrorCheckClick
            end
          end
          object evGroupBox2: TevGroupBox
            Left = 376
            Top = 20
            Width = 305
            Height = 101
            Caption = ' EMail Notifications '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object evLabel31: TevLabel
              Left = 29
              Top = 49
              Width = 67
              Height = 13
              Caption = 'EMail Address'
            end
            object evLabel32: TevLabel
              Left = 16
              Top = 22
              Width = 80
              Height = 13
              Caption = 'Notification Type'
            end
            object TaskEMailEdit: TevEdit
              Left = 103
              Top = 47
              Width = 194
              Height = 21
              MaxLength = 100
              TabOrder = 1
            end
            object NotifTypeCheck: TevComboBox
              Left = 105
              Top = 18
              Width = 88
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 0
              Text = 'Never'
              Items.Strings = (
                'Never'
                'Always'
                'Successful'
                'Exceptions')
            end
          end
        end
      end
    end
  end
  object TevPanel [1]
    Left = 0
    Top = 0
    Width = 779
    Height = 41
    Align = alTop
    TabOrder = 1
    object SchedulerStatusLabel: TevLabel
      Left = 427
      Top = 15
      Width = 108
      Height = 13
      Caption = 'Scheduler Inactive'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object NewTaskButton: TevButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'New Task'
      TabOrder = 0
      OnClick = NewTaskButtonClick
    end
    object RefreshTasksButton: TevButton
      Left = 184
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Refresh List'
      TabOrder = 1
      OnClick = RefreshTasksButtonClick
    end
    object DeleteTaskButton: TevButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Delete Task'
      TabOrder = 2
      OnClick = DeleteTaskButtonClick
    end
    object AutoShowLogCheck: TevCheckBox
      Left = 282
      Top = 13
      Width = 127
      Height = 17
      Caption = 'Auto show task log'
      TabOrder = 3
      OnClick = AutoShowLogCheckClick
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 653
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    Left = 686
    Top = 34
  end
  inherited wwdsList: TevDataSource
    Left = 610
    Top = 34
  end
  object TasksListDataSource: TevDataSource
    Left = 610
    Top = 66
  end
  object ISBasicClientDataSet1: TISBasicClientDataSet
    Active = True
    FieldDefs = <
      item
        Name = 'TaskName'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Interval'
        DataType = ftInteger
      end
      item
        Name = 'Pattern'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'StopOnError'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FolderPath'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Mapfile'
        DataType = ftString
        Size = 200
      end>
    Left = 720
    Top = 72
    object ISBasicClientDataSet1TaskName: TStringField
      DisplayWidth = 20
      FieldName = 'TaskName'
    end
    object ISBasicClientDataSet1Status: TStringField
      DisplayWidth = 15
      FieldName = 'Status'
      Size = 15
    end
    object ISBasicClientDataSet1Interval: TIntegerField
      DisplayWidth = 10
      FieldName = 'Interval'
    end
    object ISBasicClientDataSet1Pattern: TStringField
      DisplayWidth = 30
      FieldName = 'Pattern'
      Size = 30
    end
    object ISBasicClientDataSet1StopOnError: TStringField
      DisplayWidth = 1
      FieldName = 'StopOnError'
      Size = 1
    end
    object ISBasicClientDataSet1FolderPath: TStringField
      DisplayWidth = 200
      FieldName = 'FolderPath'
      Size = 200
    end
    object ISBasicClientDataSet1Mapfile: TStringField
      DisplayWidth = 200
      FieldName = 'Mapfile'
      Size = 200
    end
  end
  object TaskLogFileDataSource: TevDataSource
    Left = 650
    Top = 66
  end
  object MapFileDlg: TOpenDialog
    Filter = 'Map files (*.xml)|*.xml'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 496
    Top = 96
  end
  object ShowLogDetails: TPopupMenu
    Left = 544
    Top = 104
    object ShowDetails1: TMenuItem
      Caption = 'Show Details'
      OnClick = ShowDetails1Click
    end
  end
end
