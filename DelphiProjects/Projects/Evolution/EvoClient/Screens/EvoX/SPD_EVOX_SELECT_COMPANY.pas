unit SPD_EVOX_SELECT_COMPANY;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, ISBasicClasses, 
  ExtCtrls, DB, EvCommonInterfaces, EvDataSet, SDataStructure, StdCtrls,
  Buttons, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TEVOX_SELECT_COMPANY = class(TForm)
    evPanel1: TevPanel;
    CompaniesListGrid: TevDBGrid;
    CompaniesListDataSource: TDataSource;
    Panel3: TevPanel;
    BitBtn1: TevBitBtn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FQ: IevQuery;
  public
    { Public declarations }
  end;

  procedure EvoXSelectCompany(out ACustomCoNbr, ACompanyName: String); // returns CustomCoNbr

implementation

{$R *.dfm}

var
  MyForm: TEVOX_SELECT_COMPANY;

procedure EvoXSelectCompany(out ACustomCoNbr, ACompanyName: String); // returns CustomCoNbr
begin
  ACustomCoNbr := '';
  ACompanyName := '';
  MyForm := TEVOX_SELECT_COMPANY.Create(nil);
  try
    MyForm.FQ.Execute;
    MyForm.CompaniesListDataSource.DataSet := MyForm.FQ.result.vclDataSet;
    try
      if (DM_COMPANY.CO.Active) and (DM_COMPANY.CO.recordCount > 0) then
        MyForm.FQ.Result.Locate('CUSTOM_COMPANY_NUMBER', DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'], [loCaseInsensitive	]);

      MyForm.ShowModal;

      if (MyForm.ModalResult = mrOk) and (MyForm.FQ.Result.RecordCount > 0) then
      begin
        ACustomCoNbr := MyForm.FQ.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
        ACompanyName := MyForm.FQ.Result.FieldByName('NAME').AsString;
      end;
    finally
      MyForm.CompaniesListDataSource.DataSet := nil;
    end;
  finally
    FreeAndNil(MyForm);
  end;
end;

procedure TEVOX_SELECT_COMPANY.FormCreate(Sender: TObject);
begin
  FQ := TevQuery.Create('SELECT CUSTOM_COMPANY_NUMBER, NAME, FEIN, CL_NBR, CO_NBR FROM TMP_CO order by CUSTOM_COMPANY_NUMBER');
end;

end.
