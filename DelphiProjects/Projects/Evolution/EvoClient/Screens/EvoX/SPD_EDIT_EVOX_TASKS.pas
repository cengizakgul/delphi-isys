unit SPD_EDIT_EVOX_TASKS;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, ISBasicClasses,  Buttons, EvTypes, EvExceptions,
  DB, Wwdatsrc, EvCommonInterfaces, EvContext, isBaseClasses, StdCtrls, EvUtils,
  ExtCtrls, EvStreamUtils, ComCtrls, Mask, wwdbedit,
  EvDataSet,DBCtrls, EvExchangeConsts, ISBasicUtils, Grids,
  Wwdbigrd, Wwdbgrid, Spin, kbmMemTable, ISKbmMemDataSet, SDataStructure,
  ISDataAccessComponents, EvDataAccessComponents, DBGrids,
  isTypes, isLogFileDataSet, Menus, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TEDIT_EVOX_TASKS = class(TFrameEntry)
    TasksListDataSource: TevDataSource;
    ISBasicClientDataSet1: TISBasicClientDataSet;
    ISBasicClientDataSet1TaskName: TStringField;
    ISBasicClientDataSet1Status: TStringField;
    ISBasicClientDataSet1Interval: TIntegerField;
    ISBasicClientDataSet1Pattern: TStringField;
    ISBasicClientDataSet1StopOnError: TStringField;
    ISBasicClientDataSet1FolderPath: TStringField;
    ISBasicClientDataSet1Mapfile: TStringField;
    TaskLogFileDataSource: TevDataSource;
    evPageControl2: TevPageControl;
    ScheduledTasksSheet: TTabSheet;
    evSplitter3: TevSplitter;
    evPanel28: TevPanel;
    TaskLogGrid: TwwDBGrid;
    wwLogGridIButton: TwwIButton;
    evPanel29: TevPanel;
    TaskListGrid: TevDBGrid;
    TaskParametersSheet: TTabSheet;
    NewTaskButton: TevButton;
    RefreshTasksButton: TevButton;
    DeleteTaskButton: TevButton;
    AutoShowLogCheck: TevCheckBox;
    MapFileDlg: TOpenDialog;
    evPanel1: TevPanel;
    evGroupBox3: TevGroupBox;
    StatusLabel: TevLabel;
    ActivateTaskButton: TevButton;
    DeactivateTaskButton: TevButton;
    evGroupBox1: TevGroupBox;
    CancelTskChangesBtn: TButton;
    ApplyTaskChangesBtn: TButton;
    evGroupBox4: TevGroupBox;
    evLabel29: TevLabel;
    evLabel30: TevLabel;
    evLabel33: TevLabel;
    evLabel34: TevLabel;
    TaskMapFileEdit: TevEdit;
    TaskDataFolderEdit: TevEdit;
    evButton5: TevButton;
    TaskFilesPatternEdit: TevEdit;
    IntervalEdit: TevSpinEdit;
    evGroupBox5: TevGroupBox;
    evLabel27: TevLabel;
    evLabel28: TevLabel;
    TaskNameEdit: TevEdit;
    TaskGUIDEdit: TEdit;
    evGroupBox2: TevGroupBox;
    evLabel31: TevLabel;
    evLabel32: TevLabel;
    TaskEMailEdit: TevEdit;
    NotifTypeCheck: TevComboBox;
    SuspendOnErrorCheck: TevCheckBox;
    evButton1: TevButton;
    evLabel1: TevLabel;
    SchedulerStatusLabel: TevLabel;
    ShowLogDetails: TPopupMenu;
    ShowDetails1: TMenuItem;
    TaskQueueInterval: TevSpinEdit;
    evLabel2: TevLabel;
    procedure ActivateTaskButtonClick(Sender: TObject);
    procedure DeactivateTaskButtonClick(Sender: TObject);
    procedure RefreshTasksButtonClick(Sender: TObject);
    procedure DeleteTaskButtonClick(Sender: TObject);
    procedure NewTaskButtonClick(Sender: TObject);
    procedure ApplyTaskChangesBtnClick(Sender: TObject);
    procedure CancelTskChangesBtnClick(Sender: TObject);
    procedure evPageControl2Change(Sender: TObject);
    procedure evButton5Click(Sender: TObject);
    procedure TaskListGridDblClick(Sender: TObject);
    procedure FTasksListAfterScroll(DataSet: TDataSet);
    procedure evButton1Click(Sender: TObject);
    procedure TaskLogGridDblClick(Sender: TObject);
    procedure ShowDetails1Click(Sender: TObject);
    procedure AutoShowLogCheckClick(Sender: TObject);
    procedure SuspendOnErrorCheckClick(Sender: TObject);
    procedure IntervalEditClick(Sender: TObject);
    procedure TaskQueueIntervalClick(Sender: TObject);
  private
    //sheduler data
    FTasksList: IevDataSet;
    FTaskLogFile: TisLogDataSet;
  protected
    procedure CompareIntervals;

    // tasks related
    procedure FillTasksList;
    procedure FillOneTask(const ATaskGUID: TisGUID);
    procedure FromDataSetToTaskObject(const ATaskGUID: String);
    procedure FromDataSetToTabScheet;
    procedure FromTabSheetToDataSet;
    procedure EnableGUIAccordingTaskStatus(const AStatus: TEvoXTaskStatus);
    procedure UpdateStatusInDataSet(const AStatus: TEvoXTaskStatus);
    procedure InitTaskTabScheet;
    procedure FillLogData;
    procedure ClearlogData;
    procedure RefreshSchedulerStatus;
    procedure ShowLogDetailsWindow;
  public
    constructor Create(AOwner: TComponent); override;

    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.dfm}

uses isDataSet, rwCustomDataDictionary, rwEngineTypes,
  EvMainboard, EvExchangeSchedulerMod, FileCtrl, SPD_EDIT_EVOX_SHOW_LOG;

const
  TaskGUIDField = 'TaskGuid';
  TaskNameField = 'TaskName';
  TaskStatusField = 'Status';
  TaskIntervalField ='Interval';
  TaskPatternField = 'Pattern';
  TaskFolderPathField = 'FolderPath';
  TaskMapFileField = 'Mapfile';
  TaskEMailField = 'EMail';
  TaskNotificationField = 'NotificationType';
  TaskStopOnErrorField = 'StopOnError';
  TaskStatusCheckIntervalField = 'TaskStatusCheckInterval';

procedure TEDIT_EVOX_TASKS.Activate;
begin
  inherited;
  Mainboard.EvoXScheduler.LoadTasksWithoutActivation;
  RefreshSchedulerStatus;

  FTasksList := TevDataSet.Create;
  FTasksList.vclDataSet.FieldDefs.Add(TaskGUIDField, ftString, 32, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskNameField, ftString, 20, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskStatusField, ftString, 15, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskIntervalField, ftInteger, 0, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskPatternField, ftString, 30, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskFolderPathField, ftString, 200, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskMapfileField, ftString, 200, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskEMailField, ftString, 100, false);
  FTasksList.vclDataSet.FieldDefs.Add(TaskNotificationField, ftInteger, 0, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskStopOnErrorField, ftString, 1, true);
  FTasksList.vclDataSet.FieldDefs.Add(TaskStatusCheckIntervalField, ftInteger, 0, true);
  FTasksList.vclDataSet.Active := true;
  TasksListDataSource.DataSet := FTasksList.vclDataSet;

  FTaskLogFile := TisLogDataSet.Create(self);

  FillTasksList;
  FromDataSetToTabScheet;
  AutoShowLogCheck.Checked := mb_AppSettings.AsBoolean['Settings\AlwaysDoPreviewForEvoXTasks'];
end;

procedure TEDIT_EVOX_TASKS.Deactivate;
begin
  inherited;
  FTasksList.vclDataSet.AfterScroll := nil;
  ClearLogData;

  TaskLogFileDataSource.DataSet := nil;
  TasksListDataSource.DataSet := nil;

  FTasksList := nil;
  mb_AppSettings.AsBoolean['Settings\AlwaysDoPreviewForEvoXTasks'] := AutoShowLogCheck.Checked;
end;

procedure TEDIT_EVOX_TASKS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
end;

constructor TEDIT_EVOX_TASKS.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TEDIT_EVOX_TASKS.FillTasksList;
var
  TasksGuids: IisStringList;
  i: integer;
begin
  FTasksList.vclDataSet.AfterScroll := nil;
  try
    FTasksList.vclDataSet.DisableControls;
    try
      FTasksList.Clear;

      TasksGuids := Mainboard.EvoXScheduler.GetTaskGUIDsList;
      for i := 0 to TasksGuids.Count - 1 do
        FillOneTask(TasksGuids[i]);

      FTasksList.First;  
    finally
      FTasksList.vclDataSet.EnableControls;
    end;
    if AutoShowLogCheck.Checked then
      FillLogData;
  finally
    FTasksList.vclDataSet.AfterScroll := FTasksListAfterScroll;
  end;
end;

procedure TEDIT_EVOX_TASKS.FromDataSetToTaskObject(const ATaskGUID: String);
var
  TaskParams: IisListOfValues;
  S: String;
  i: integer;
  Task: IEvExchangeScheduledTask;
begin
  CheckCondition((not FTasksList.eof) and (FTaskslist[TaskGUIDField] <> null), 'No data in dataset to store in task object');
  TaskParams := TisListOfValues.Create;

  S := FTasksList[TaskNameField];
  TaskParams.AddValue(EvoXTaskName, S);

  i := FTasksList[TaskIntervalField];
  TaskParams.AddValue(EvoXTaskTimeInterval, i);

  S := FTasksList[TaskPatternField];
  TaskParams.AddValue(EvoXTaskFilePattern, S);

  S := FTasksList[TaskMapFileField];
  TaskParams.AddValue(EvoXTaskMapFilePath, S);

  S := FTasksList[TaskFolderPathField];
  TaskParams.AddValue(EvoXTaskFolderPath, S);

  S := FTasksList[TaskEMailField];
  TaskParams.AddValue(EvoXTaskEmailAddress, S);

  i := FTasksList[TaskNotificationField];
  i := Integer(TEvoxTaskEmailNotification(i));
  TaskParams.AddValue(EvoXTaskEMailNotification, i);

  S := FTasksList[TaskStopOnErrorField];
  TaskParams.AddValue(EvoXTaskStopOnError, S);

  i := FTasksList[TaskStatusCheckIntervalField];
  TaskParams.AddValue(EvoXTaskStatusCheckInterval, i);

  Task := MainBoard.EvoXScheduler.GetTask(ATaskGUID);
  CheckCondition(Assigned(Task), 'Task not found. Id: ' + ATaskGUID);
  Task.SetTaskParams(TaskParams);
end;

procedure TEDIT_EVOX_TASKS.FromDataSetToTabScheet;
begin
  InitTaskTabScheet;

  if FTasksList[TaskGUIDField] <> null then
  begin
    evGroupBox3.Enabled := true;
    evGroupBox1.Enabled := true;

    TaskNameEdit.Text := FTasksList[TaskNameField];
    TaskGUIDEdit.Text := FTasksList[TaskGUIDField];

    EnableGUIAccordingTaskStatus(StringToTaskStatus(FTasksList[TaskStatusField]));
    SuspendOnErrorCheck.Checked := FTasksList[TaskStopOnErrorField] = 'Y';

    TaskDataFolderEdit.Text := FTasksList[TaskFolderPathField];
    TaskMapFileEdit.Text := FTasksList[TaskMapFileField];
    TaskFilesPatternEdit.Text := FTasksList[TaskPatternField];
    TaskEMailEdit.Text := FTasksList[TaskEMailField];
    IntervalEdit.Value := FTasksList[TaskIntervalField];
    NotifTypeCheck.ItemIndex := FTasksList[TaskNotificationField];
    TaskQueueInterval.Value := FTasksList[TaskStatusCheckIntervalField];
  end;
end;

procedure TEDIT_EVOX_TASKS.FromTabSheetToDataSet;
begin
  CheckCondition(TaskGUIDEdit.Text = FTasksList[TaskGUIDField], 'Internal error. Task Id on Tabsheet is not the same as in dataset');

  CheckCondition(Trim(TaskNameEdit.Text) <> '', 'Task name cannot be empty');
  CheckCondition(Trim(TaskDataFolderEdit.Text) <> '', 'Data files folder cannot be empty');
  CheckCondition(DirectoryExists(NormalizePath(Trim(TaskDataFolderEdit.Text))), 'Data files folder does not exist');
  CheckCondition(Trim(TaskMapFileEdit.Text) <> '', 'Path to the map file cannot be empty');
  CheckCondition(FileExists(Trim(TaskMapFileEdit.Text)), 'Map file does not exist');
  CheckCondition(Trim(TaskFilesPatternEdit.Text) <> '', 'Data files pattern cannot be empty');
  if NotifTypeCheck.ItemIndex > 0 then
    CheckCondition(Trim(TaskEMailEdit.Text) <> '', 'Email address cannot be empty');

  try
    FTasksList.Edit;
    FTasksList[TaskNameField] := TaskNameEdit.Text;

    if SuspendOnErrorCheck.Checked then
      FTasksList[TaskStopOnErrorField] := 'Y'
    else
      FTasksList[TaskStopOnErrorField] := 'N';

    FTasksList[TaskFolderPathField] := TaskDataFolderEdit.Text;
    FTasksList[TaskMapFileField] := TaskMapFileEdit.Text;
    FTasksList[TaskPatternField] := TaskFilesPatternEdit.Text;
    FTasksList[TaskEMailField] := TaskEMailEdit.Text;
    FTasksList[TaskIntervalField] := IntervalEdit.Value;
    FTasksList[TaskNotificationField] := NotifTypeCheck.ItemIndex;
    FTasksList[TaskStatusCheckIntervalField] := TaskQueueInterval.Value;

    FTasksList.Post;
  except
    on E:Exception do
    begin
      FTasksList.vclDataSet.Cancel;
      raise;
    end;
  end;    
end;

procedure TEDIT_EVOX_TASKS.ActivateTaskButtonClick(Sender: TObject);
var
  Task: IEvExchangeScheduledTask;
begin
  inherited;
  CheckCondition(TaskGUIDEdit.Text = FTasksList[TaskGUIDField], 'Internal error. Task Id on Tabsheet is not the same as in dataset');
  Task := Mainboard.EvoXScheduler.GetTask(TaskGUIDEdit.Text);
  if Assigned(Task) then
  begin
    Task.ActivateTask;
    EnableGUIAccordingTaskStatus(Task.GetTaskStatus);
    UpdateStatusInDataSet(Task.GetTaskStatus);
  end
  else
    raise Exception.Create('Task not found');
end;

procedure TEDIT_EVOX_TASKS.DeactivateTaskButtonClick(Sender: TObject);
var
  Task: IEvExchangeScheduledTask;
begin
  inherited;
  CheckCondition(TaskGUIDEdit.Text = FTasksList[TaskGUIDField], 'Internal error. Task Id on Tabsheet is not the same as in dataset');
  Task := Mainboard.EvoXScheduler.GetTask(TaskGUIDEdit.Text);
  if Assigned(Task) then
  begin
    Task.DeactivateTask;
    EnableGUIAccordingTaskStatus(Task.GetTaskStatus);
    UpdateStatusInDataSet(Task.GetTaskStatus);
  end
  else
    raise Exception.Create('Task not found');
end;

procedure TEDIT_EVOX_TASKS.FillOneTask(const ATaskGUID: TisGUID);
var
  Task: IEvExchangeScheduledTask;
  TaskStatus: TEvoXTaskStatus;
  TaskParameters: IisListOfValues;
begin
  Task := Mainboard.EvoXScheduler.GetTask(ATaskGUID);
  if Assigned(Task) then
  begin
    TaskStatus := Task.GetTaskStatus;
    TaskParameters := Task.GetTaskParams;

    FTasksList.Append;
    FTasksList[TaskGUIDField] := ATaskGUID;
    FTasksList[TaskStatusField] := TaskStatusToString(TaskStatus);
    FTasksList[TaskNameField] := TaskParameters.Value[EvoXTaskName];
    FTasksList[TaskIntervalField] := TaskParameters.Value[EvoXTaskTimeInterval];
    FTasksList[TaskPatternField] := TaskParameters.Value[EvoXTaskFilePattern];
    FTasksList[TaskMapFileField] := TaskParameters.Value[EvoXTaskMapFilePath];
    FTasksList[TaskFolderPathField] := TaskParameters.Value[EvoXTaskFolderPath];
    FTasksList[TaskEMailField] := TaskParameters.Value[EvoXTaskEmailAddress];
    FTasksList[TaskNotificationField] := TaskParameters.Value[EvoXTaskEMailNotification];
    FTasksList[TaskStopOnErrorField] := TaskParameters.Value[EvoXTaskStopOnError];
    if TaskParameters.ValueExists(EvoXTaskStatusCheckInterval) then
      FTasksList[TaskStatusCheckIntervalField] := TaskParameters.Value[EvoXTaskStatusCheckInterval]
    else
    begin
      if (TaskParameters.Value[EvoXTaskStopOnError] = 'Y') then
        FTasksList[TaskStatusCheckIntervalField] := FTasksList[TaskIntervalField]
      else
        FTasksList[TaskStatusCheckIntervalField] := EvoXDefaultTaskStatusCheckInterval;
    end;

    FTasksList.Post;
  end;
end;

procedure TEDIT_EVOX_TASKS.EnableGUIAccordingTaskStatus(const AStatus: TEvoXTaskStatus);
begin
  ActivateTaskButton.Enabled := false;
  DeactivateTaskButton.Enabled := false;
  StatusLabel.Caption := TaskStatusToString(AStatus);
  if StatusLabel.Caption = EvoXTaskStatusActive then
    DeactivateTaskButton.Enabled := true
  else
    ActivateTaskButton.Enabled := true;
end;

procedure TEDIT_EVOX_TASKS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
end;

procedure TEDIT_EVOX_TASKS.UpdateStatusInDataSet(const AStatus: TEvoXTaskStatus);
begin
  if FTasksList[TaskStatusField] <> TaskStatusToString(AStatus) then
  begin
    FTasksList.Edit;
    FTasksList[TaskStatusField] := TaskStatusToString(AStatus);
    FTasksList.Post;
  end;
end;

procedure TEDIT_EVOX_TASKS.RefreshTasksButtonClick(Sender: TObject);
begin
  inherited;
  FillTasksList;
  FromDataSetToTabScheet;
  RefreshSchedulerStatus;
end;

procedure TEDIT_EVOX_TASKS.DeleteTaskButtonClick(Sender: TObject);
var
  TaskGUID: TisGUID;
begin
  inherited;

  if FTasksList[TaskGUIDField] <> null then
    try
      if not (EvMessage('Do you really want to delete the selected task?', mtWarning, [mbYes, MbNo], mbYes) = mrYes) then
        exit;
      ClearLogData;  
      TaskGUID := FTasksList[TaskGUIDField];
      Mainboard.EvoXScheduler.DeleteTask(TaskGUID);
      FTasksList.Delete;
      FromDataSetToTabScheet;
    except
      on E: Exception do
      begin
        try
          FTasksList.vclDataSet.DisableControls;
          FTasksList.Delete;
          FillOneTask(TaskGUID);
          FromDataSetToTabScheet;
        finally
          FTasksList.vclDataSet.EnableControls;
        end;
        raise;
      end;
    end;
end;

procedure TEDIT_EVOX_TASKS.NewTaskButtonClick(Sender: TObject);
var
  TaskGUID: TisGUID;
  Task: IEvExchangeScheduledTask;
begin
  inherited;
  ClearlogData;
  Task := Mainboard.EvoXScheduler.AddNewTask;
  TaskGUID := Task.GetTaskGUID;
  FillOneTask(TaskGuid);
  FromDataSetToTabScheet;
end;

procedure TEDIT_EVOX_TASKS.InitTaskTabScheet;
begin
  StatusLabel.Caption := '';
  TaskNameEdit.Text := '';
  TaskGUIDEdit.Text := '';
  SuspendOnErrorCheck.Checked := false;
  NotifTypeCheck.ItemIndex := 0;
  TaskEMailEdit.Text := '';
  TaskDataFolderEdit.Text := '';
  TaskFilesPatternEdit.Text := '*.*';
  IntervalEdit.Value := 10;
  TaskQueueInterval.Value := EvoXDefaultTaskStatusCheckInterval;

  evGroupBox3.Enabled := false;
  evGroupBox1.Enabled := false;
end;

procedure TEDIT_EVOX_TASKS.ApplyTaskChangesBtnClick(Sender: TObject);
var
  Task: IEvExchangeScheduledTask;
  TaskGUID : TisGUID;
begin
  inherited;
  if FTasksList[TaskGUIDField] <> null then
  begin
    TaskGUID := FTasksList[TaskGUIDField];
    Task := Mainboard.EvoXScheduler.GetTask(TaskGUID);
    if Assigned(Task) then
    begin
      try
        FromTabSheetToDataSet;
        FromDataSetToTaskObject(TaskGUID);
      except
        on E: Exception do
        begin
          try
            FTasksList.vclDataSet.DisableControls;
            FTasksList.Delete;
            FillOneTask(TaskGUID);
          finally
            FTasksList.vclDataSet.EnableControls;
          end;
          raise;
        end;
      end;
    end
    else
      EvMessage('Cannot find task: "' + TaskGUID + '"', mtWarning, [mbOk], mbOk);
  end;
end;

procedure TEDIT_EVOX_TASKS.CancelTskChangesBtnClick(Sender: TObject);
begin
  inherited;
  FromDataSetToTabScheet;
end;

procedure TEDIT_EVOX_TASKS.evPageControl2Change(Sender: TObject);
begin
  inherited;
  if evPageControl2.ActivePageIndex = 1 then
    FromDataSetToTabScheet;
end;

procedure TEDIT_EVOX_TASKS.evButton5Click(Sender: TObject);
begin
  inherited;
  MapFileDlg.FileName := TaskMapFileEdit.Text;
  if MapFileDlg.Execute then
    TaskMapFileEdit.Text := MapFileDlg.FileName;
end;

procedure TEDIT_EVOX_TASKS.FillLogData;
var
  S: String;
  Task: IEvExchangeScheduledTask;
begin
  ClearLogData;

  if FTasksList[TaskGUIDField] <> null then
  begin
    Task := Mainboard.EvoXScheduler.GetTask(FTasksList[TaskGUIDField]);
    if Assigned(Task) then
    begin
      S := Task.GetLogFileName;
      if S <> '' then
      begin
        TaskLogFileDataSource.DataSet := FTaskLogFile;
        FTaskLogFile.FileName := S;
        FTaskLogFile.Open;
      end;
    end;
  end;
end;

procedure TEDIT_EVOX_TASKS.TaskListGridDblClick(Sender: TObject);
begin
  inherited;
  ClearlogData;
  FillLogData;
end;

procedure TEDIT_EVOX_TASKS.FTasksListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ClearlogData;
  if (not FTasksList.eof) and AutoShowLogCheck.Checked then
    FillLogData;
end;

procedure TEDIT_EVOX_TASKS.ClearlogData;
begin
  FTaskLogFile.Close;
end;

procedure TEDIT_EVOX_TASKS.evButton1Click(Sender: TObject);
var
  Dir: String;
begin
  inherited;
  Dir := TaskDataFolderEdit.Text;
  if SelectDirectory('Please, select the folder where data files will be located', '', Dir) then
    TaskDataFolderEdit.Text := Dir;
end;

procedure TEDIT_EVOX_TASKS.RefreshSchedulerStatus;
begin
  if Mainboard.EvoXScheduler.Active then
    SchedulerStatusLabel.Caption := 'EvoX Scheduler is active'
  else
    SchedulerStatusLabel.Caption := 'EvoX Scheduler is inactive';
end;

procedure TEDIT_EVOX_TASKS.TaskLogGridDblClick(Sender: TObject);
begin
  inherited;
  ShowLogDetailsWindow;
end;

procedure TEDIT_EVOX_TASKS.ShowLogDetailsWindow;
begin
  if FTaskLogFile.Active and (FTaskLogFile['Text'] <> null) then
    ShowLogRecordDialog(VarToStr(FTaskLogFile['EventType']), VarToStr(FTaskLogFile['TimeStamp']), VarToStr(FTaskLogFile['EventClass']),
      VarToStr(FTaskLogFile['Text']), VarToStr(FTaskLogFile['Details']));
end;

procedure TEDIT_EVOX_TASKS.ShowDetails1Click(Sender: TObject);
begin
  inherited;
  ShowLogDetailsWindow;
end;

procedure TEDIT_EVOX_TASKS.AutoShowLogCheckClick(Sender: TObject);
begin
  inherited;
  if AutoShowLogCheck.Checked then
    FillLogData;
end;

procedure TEDIT_EVOX_TASKS.SuspendOnErrorCheckClick(Sender: TObject);
begin
  inherited;
  CompareIntervals;
end;

procedure TEDIT_EVOX_TASKS.CompareIntervals;
begin
  if SuspendOnErrorCheck.Checked then
  begin
    if TaskQueueInterval.Value > IntervalEdit.Value then
      TaskQueueInterval.Value := IntervalEdit.Value;
    if TaskQueueInterval.Value > EvoXDefaultTaskStatusCheckInterval then
      TaskQueueInterval.Value := EvoXDefaultTaskStatusCheckInterval;
  end;
end;

procedure TEDIT_EVOX_TASKS.IntervalEditClick(Sender: TObject);
begin
  inherited;
  CompareIntervals;
end;

procedure TEDIT_EVOX_TASKS.TaskQueueIntervalClick(Sender: TObject);
begin
  inherited;
  CompareIntervals;
end;

initialization
  RegisterClass(TEDIT_EVOX_TASKS);
end.
