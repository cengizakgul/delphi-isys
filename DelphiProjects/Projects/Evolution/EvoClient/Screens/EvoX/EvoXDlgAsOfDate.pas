// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvoXDlgAsOfDate;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, EvTypes,
  Buttons, ExtCtrls, ComCtrls,  TypInfo, EvBasicUtils, db,
  ISBasicClasses, EvExceptions, EvUIUtils, EvUIComponents;

type
  TEvoXdlAsOfDate = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evRadioGroup1: TevRadioGroup;
    evDateTimePicker1: TevDateTimePicker;
    procedure evRadioGroup1Click(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  protected
  end;


function ShowAsOfDateDlg(const AStartDate: TDate): TDate;

implementation

uses
  EvUtils;

{$R *.dfm}

function ShowAsOfDateDlg(const AStartDate: TDate): TDate;
var
  Year: Integer;
  EvoXdlAsOfDate: TEvoXdlAsOfDate;
begin
  Result := AStartDate;

  EvoXdlAsOfDate := TEvoXdlAsOfDate.Create(nil);
  try
    Year := GetYear(Now);
    EvoXdlAsOfDate.evRadioGroup1.Items[0] := '1/1/' + IntToStr(Succ(Year));
    EvoXdlAsOfDate.evRadioGroup1.Items[1] := '10/1/' + IntToStr(Year);
    EvoXdlAsOfDate.evRadioGroup1.Items[2] := '7/1/' + IntToStr(Year);
    EvoXdlAsOfDate.evRadioGroup1.Items[3] := '4/1/' + IntToStr(Year);
    EvoXdlAsOfDate.evRadioGroup1.Items[4] := '1/1/' + IntToStr(Year);
    EvoXdlAsOfDate.evRadioGroup1.Items[5] := '10/1/' + IntToStr(Pred(Year));
    EvoXdlAsOfDate.evRadioGroup1.ItemIndex := -1;
    EvoXdlAsOfDate.evDateTimePicker1.Date := AStartDate;

    if EvoXdlAsOfDate.ShowModal = mrOK then
    begin
      if EvoXdlAsOfDate.evRadioGroup1.ItemIndex <= 5 then
        Result := Trunc(StrToDate(EvoXdlAsOfDate.evRadioGroup1.Items[EvoXdlAsOfDate.evRadioGroup1.ItemIndex]))
      else
        Result := EvoXdlAsOfDate.evDateTimePicker1.Date;
    end;
  finally
    FreeAndNil(EvoXdlAsOfDate);
  end;
end;

procedure TEvoXdlAsOfDate.evRadioGroup1Click(Sender: TObject);
begin
  evDateTimePicker1.Enabled := evRadioGroup1.ItemIndex = 6;
end;

procedure TEvoXdlAsOfDate.OKBtnClick(Sender: TObject);
begin
  if evRadioGroup1.ItemIndex < 0 then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, select date');
    Exit;
  end;
end;

end.
