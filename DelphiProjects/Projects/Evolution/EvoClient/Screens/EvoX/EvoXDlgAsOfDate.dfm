object EvoXdlAsOfDate: TEvoXdlAsOfDate
  Left = 452
  Top = 287
  BorderStyle = bsDialog
  Caption = 'Select effective date, please'
  ClientHeight = 192
  ClientWidth = 232
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 147
    Top = 17
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 147
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object evRadioGroup1: TevRadioGroup
    Left = 8
    Top = 8
    Width = 129
    Height = 177
    Caption = 'Date'
    Items.Strings = (
      ' 1/1/2009'
      ' 10/1/2008'
      ' 7/1/2008'
      ' 4/1/2008'
      ' 1/1/2008'
      ' 10/1/2007'
      '')
    TabOrder = 0
    OnClick = evRadioGroup1Click
  end
  object evDateTimePicker1: TevDateTimePicker
    Left = 36
    Top = 155
    Width = 95
    Height = 21
    Date = 37928.000000000000000000
    Time = 37928.000000000000000000
    Enabled = False
    TabOrder = 1
  end
end
