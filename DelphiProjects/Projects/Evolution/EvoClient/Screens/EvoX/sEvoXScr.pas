// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sEvoXScr;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvCommonInterfaces,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, ISBasicClasses, EvMainboard,
  EvContext, EvUIComponents;

type
  TEvoXFrm = class(TFramePackageTmpl, IevEvoXScreens)
  protected
    function  InitPackage: Boolean; override;
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
  end;

implementation

{$R *.DFM}

var
  EvoXFrm: TEvoXFrm;

function GetEvoXScreens: IevEvoXScreens;
begin
  if not Assigned(EvoXFrm) then
    EvoXFrm := TEvoXFrm.Create(nil);
  Result := EvoXFrm;
end;


{ TPaychoiceFrm }

function TEvoXFrm.InitPackage: Boolean;
begin
  if Context.License.EvoX then
    Result := inherited InitPackage
  else
    Result := False;
end;

function TEvoXFrm.PackageCaption: string;
begin
  Result := 'EvoX Screens';
end;

function TEvoXFrm.PackageSortPosition: string;
begin
  Result := '';
end;

procedure TEvoXFrm.UserPackageStructure;
begin
  AddStructure('\&Company\Imports\Evolution Exchange|010', 'TEDIT_EVOX_MAP');
  AddStructure('\&Company\Imports\Evolution Exchange Tasks|011', 'TEDIT_EVOX_TASKS');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEvoXScreens, IevEvoXScreens, 'Screens EvoX');

finalization
  FreeAndNil(EvoXFrm);

end.
