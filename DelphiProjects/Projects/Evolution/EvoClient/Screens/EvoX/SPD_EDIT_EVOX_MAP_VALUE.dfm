object EDIT_EVOX_MAP_VALUE: TEDIT_EVOX_MAP_VALUE
  Left = 348
  Top = 189
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  ClientHeight = 106
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 23
    Height = 13
    Caption = 'From'
  end
  object Label2: TLabel
    Left = 16
    Top = 40
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object FromEdit: TEdit
    Left = 48
    Top = 14
    Width = 321
    Height = 21
    TabOrder = 0
  end
  object ToEdit: TEdit
    Left = 48
    Top = 38
    Width = 321
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 112
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Save'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TButton
    Left = 224
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
