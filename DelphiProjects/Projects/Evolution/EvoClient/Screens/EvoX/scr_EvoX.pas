// Screens of "Evo Exchange"
unit scr_EvoX;

interface

uses
  sEvoXScr,
  SPD_EDIT_EVOX_MAP,
  EvoXDlgAsOfDate,
  SPD_EDIT_EVOX_TASKS,
  SPD_EDIT_EVOX_SHOW_LOG,
  SPD_EDIT_EVOX_MAP_VALUE,
  SPD_EVOX_SELECT_COMPANY;
  
implementation

end.
