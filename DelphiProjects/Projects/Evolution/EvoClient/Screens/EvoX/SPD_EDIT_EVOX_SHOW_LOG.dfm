object EDIT_EVOX_SHOW_LOG: TEDIT_EVOX_SHOW_LOG
  Left = 436
  Top = 296
  Width = 605
  Height = 471
  Caption = 'Task log record'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 113
    Align = alTop
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 8
      Top = 8
      Width = 55
      Height = 13
      Caption = 'Event Type'
    end
    object evLabel2: TevLabel
      Left = 376
      Top = 8
      Width = 53
      Height = 13
      Caption = 'TimeStamp'
    end
    object evLabel3: TevLabel
      Left = 8
      Top = 37
      Width = 57
      Height = 13
      Caption = 'EventClass'
    end
    object evLabel4: TevLabel
      Left = 8
      Top = 67
      Width = 21
      Height = 13
      Caption = 'Text'
    end
    object evLabel5: TevLabel
      Left = 8
      Top = 94
      Width = 32
      Height = 13
      Caption = 'Details'
    end
    object EventType: TevEdit
      Left = 72
      Top = 5
      Width = 201
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object EventTimeStamp: TevEdit
      Left = 440
      Top = 5
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object EventClass: TevEdit
      Left = 72
      Top = 34
      Width = 513
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object EventText: TevEdit
      Left = 72
      Top = 61
      Width = 513
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 113
    Width = 597
    Height = 324
    Align = alClient
    TabOrder = 1
    object EventDetails: TMemo
      Left = 1
      Top = 1
      Width = 595
      Height = 322
      Align = alClient
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
end
