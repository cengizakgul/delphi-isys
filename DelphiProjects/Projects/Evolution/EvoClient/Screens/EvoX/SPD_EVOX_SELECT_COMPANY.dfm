object EVOX_SELECT_COMPANY: TEVOX_SELECT_COMPANY
  Left = 456
  Top = 267
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Please, select a company'
  ClientHeight = 431
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 41
    Width = 613
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object CompaniesListGrid: TevDBGrid
      Left = 0
      Top = 0
      Width = 613
      Height = 390
      DisableThemesInTitle = False
      Selected.Strings = (
        'CUSTOM_COMPANY_NUMBER'#9'20'#9'Number'#9'F'
        'NAME'#9'40'#9'Name'#9'F'
        'FEIN'#9'9'#9'EIN'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEVOX_SELECT_COMPANY\CompaniesListGrid'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = CompaniesListDataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgFixedProportionalResize, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object Panel3: TevPanel
    Left = 0
    Top = 0
    Width = 613
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object BitBtn1: TevBitBtn
      Left = 5
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Select company'
      ModalResult = 1
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
        8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
        FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
        FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
        FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
        C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
        C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
        DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
        CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
      NumGlyphs = 2
    end
  end
  object CompaniesListDataSource: TDataSource
    Left = 512
    Top = 8
  end
end
