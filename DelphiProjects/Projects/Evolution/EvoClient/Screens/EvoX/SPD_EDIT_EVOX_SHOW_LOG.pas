unit SPD_EDIT_EVOX_SHOW_LOG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses,  ExtCtrls, EvUIComponents;

type
  TEDIT_EVOX_SHOW_LOG = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    EventDetails: TMemo;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    EventType: TevEdit;
    EventTimeStamp: TevEdit;
    EventClass: TevEdit;
    EventText: TevEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure ShowLogRecordDialog(const AEventType, ATimeStamp, AEventClass, AText, ADetails: String);

implementation

{$R *.dfm}

var
  MyForm: TEDIT_EVOX_SHOW_LOG;

procedure ShowLogRecordDialog(const AEventType, ATimeStamp, AEventClass, AText, ADetails: String);
begin
  MyForm := TEDIT_EVOX_SHOW_LOG.Create(nil);
  try
    MyForm.EventType.Text := AEventType;
    MyForm.EventTimeStamp.Text := ATimeStamp;
    MyForm.EventClass.Text := AEventClass;
    MyForm.EventText.Text := AText;
    MyForm.EventDetails.Text := ADetails;
    MyForm.ShowModal;
  finally
    FreeAndnil(MyForm);
  end;
end;

end.
