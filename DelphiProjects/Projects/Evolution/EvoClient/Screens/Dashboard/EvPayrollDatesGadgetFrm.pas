unit EvPayrollDatesGadgetFrm;

interface

uses
  Windows, Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  Graphics, Messages, Buttons, StdCtrls, SysUtils, Forms, isBaseClasses,
  EvDataset, EvContext, EvCommonInterfaces, EvUtils, SFieldCodeValues;

type
  TEvCalendar = class;

  TCalendarItemType  = (itHeader, itFooter, itCW, itWeek, itDay, itCalendar);
  TDateStyleType = (stSelected, stToday, stDay, stWeekEnd, stIndirect, stCheckDate, stProcessedDate);

  TOnGetCursorEvent = procedure(Sender: TEvCalendar;
    ItemType: TCalendarItemType; var ACursor: TCursor) of object;

  TOnGetItemStyleEvent = procedure(Sender: TEvCalendar;
    AStyleType: TDateStyleType; ADate: TDate; var AFont: TFont;
    var AColor: TColor) of object;

  TCalendarPanel = class(TGraphicControl)
  private
    FFlat: Boolean;
    FToday: Boolean;
    procedure SetFlat( AValue: boolean );
    procedure WMMouseMove(var Message: TWMMouseMove); message WM_MOUSEMOVE;    
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    property Flat: Boolean read FFlat write SetFlat default False;
    property Today: Boolean read FToday write FToday;
  end;

  TDayNumberPanel = class(TCalendarPanel)
  protected
    procedure Paint; override;
  end;

  TCalendarOption = (
    coFlatButtons,
    csFlatDays,
    coFlatHeaders,
    coMonthButtonVisible,
    csSelectionEnabled,
    csSetTodayOnStartup,
    coShowDateLabel,
    coShowFooter,
    coShowNextMonth,
    coShowPreviousMonth,
    coShowHeader,
    coShowWeekDays,
    coShowWeeks,
    coTransparentButtons,
    csUseWeekEndColor,
    csUseWeekEndFont,
    coYearButtonVisible);

  TCalendarOptions = set of TCalendarOption;
  TFirstDayOfWeek  = (fdSunday, fdMonday);
  TCalendarHeaderFormat = (hfMMMMYYYY, hfYYYYMMMM, hfMMYYYY, hfYYYYMM);

  TDateHints = class(TPersistent)
  private
    FHints: array[0..6] of ShortString;
    FOnChange: TNotifyEvent;
    procedure SetHint(Index: integer; AValue: ShortString);
    function GetHint(Index: integer): ShortString;
  protected
    procedure Change; virtual;
  public
    constructor Create;
    procedure Assign(Source: TPersistent); override;
    property Hints[Index: integer]: ShortString read GetHint; default;
  published
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property NextMonth: ShortString Index 0 read GetHint write SetHint;
    property PreviousMonth: ShortString Index 1 read GetHint write SetHint;
    property NextYear: ShortString Index 2 read GetHint write SetHint;
    property PreviousYear: ShortString Index 3 read GetHint write SetHint;
    property Today: ShortString Index 4 read GetHint write SetHint;
  end;

  TDateMessages = class(TPersistent)
  private
    FMonthNames: array[1..12] of ShortString;
    FMessages: array[0..7] of ShortString;
    FHints: TDateHints;
    FOnChange: TNotifyEvent;

    procedure SetMessage(Index: integer; AValue: ShortString);
    function GetMessage(Index: integer): ShortString;
    procedure SetMonthName(Index: integer; AValue: ShortString);
    function GetMonthName(Index: integer): ShortString;
    procedure SetOnChange(AOnChange: TNotifyEvent);
  protected
    procedure Change; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property Messages[Index: integer]: ShortString read GetMessage; default;
    property MonthNames[Index: integer]: ShortString read GetMonthName;
  published
    property OnChange: TNotifyEvent read FOnChange write SetOnChange;
    property Hints: TDateHints read FHints write FHints;
    property Week: ShortString Index 0 read GetMessage write SetMessage;
    property Sunday: ShortString Index 1 read GetMessage write SetMessage;
    property Monday: ShortString Index 2 read GetMessage write SetMessage;
    property Tuesday: ShortString Index 3 read GetMessage write SetMessage;
    property Wednesday: ShortString Index 4 read GetMessage write SetMessage;
    property Thursday: ShortString Index 5 read GetMessage write SetMessage;
    property Friday: ShortString Index 6 read GetMessage write SetMessage;
    property Saturday: ShortString Index 7 read GetMessage write SetMessage;
    property January: ShortString Index 1 read GetMonthName write SetMonthName;
    property February: ShortString Index 2 read GetMonthName write SetMonthName;
    property March: ShortString Index 3 read GetMonthName write SetMonthName;
    property April: ShortString Index 4 read GetMonthName write SetMonthName;
    property May: ShortString Index 5 read GetMonthName write SetMonthName;
    property June: ShortString Index 6 read GetMonthName write SetMonthName;
    property July: ShortString Index 7 read GetMonthName write SetMonthName;
    property August: ShortString Index 8 read GetMonthName write SetMonthName;
    property September: ShortString Index 9 read GetMonthName write SetMonthName;
    property October: ShortString Index 10 read GetMonthName write SetMonthName;
    property November: ShortString Index 11 read GetMonthName write SetMonthName;
    property December: ShortString Index 12 read GetMonthName write SetMonthName;
  end;

  TCalendarButton = class(TSpeedButton)
  private
    procedure CMDesignHitTest(var Msg: TCMDesignHitTest); message CM_DESIGNHITTEST;
  end;

  TEvCalendar = class(TCustomPanel)
  private
    FPayrollDates: IisListOfValues;
    FOnGetItemStyleEvent: TOnGetItemStyleEvent;
    FOnGetCursor: TOnGetCursorEvent;
    FYear: integer;
    FMonth: integer;
    FDay: integer;
    FDateLabel: TLabel;
    FPanel_Header: TPanel;
    FPanel_Footer: TPanel;
    FPanel_Calendar: TPanel;
    FPanel_Days: array[0..6, 0..7] of TCalendarPanel;
    FYear_Plus: TCalendarButton;
    FYear_Minus: TCalendarButton;
    FMonth_Plus: TCalendarButton;
    FMonth_Minus: TCalendarButton;
    FDayColor: TColor;
    FSelectedColor: TColor;
    FDayNameColor: TColor;
    FWeekColor: TColor;
    FTodayColor: TColor;
    FIndirectColor: TColor;
    FWeekEndColor: TColor;
    FDateFormat: String;
    FOptions: TCalendarOptions;
    FMessages: TDateMessages;
    FFirstDayOfWeek: TFirstDayOfWeek;
    FHeaderFormat: TCalendarHeaderFormat;
    FSelectedFont: TFont;
    FTodayFont: TFont;
    FDayFont: TFont;
    FIndirectFont: TFont;
    FWeekDaysFont: TFont;
    FWeeksFont: TFont;
    FWeekEndFont: TFont;
    FSelectionStart: TDateTime;
    FSelectionEnd: TDateTime;
    FCursor: TCursor;
    procedure SetCursor(AValue: TCursor);
    procedure SetSelectedColor(AValue: TColor);
    procedure SetWeekEndColor(AValue: TColor);
    procedure SetDayColor(AValue: TColor);
    procedure SetWeekColor(AValue: TColor);
    procedure SetTodayColor(AValue: TColor);
    procedure SetDayNameColor(AValue: TColor);
    procedure SetHeaderColor(AValue: TColor);
    procedure SetFooterColor(AValue: TColor);
    procedure SetIndirectColor(AValue: TColor);
    function GetHeaderColor: TColor;
    function GetFooterColor: TColor;
    procedure SetOptions(AValue: TCalendarOptions);
    procedure SetWeekDaysFont(AValue: TFont);
    procedure SetIndirectFont(AValue: TFont);
    procedure SetWeeksFont(AValue: TFont);
    procedure SetWeekEndFont(AValue: TFont);
    function GetDateLabelFont: TFont;
    procedure SetDateLabelFont(AValue: TFont);
    procedure SetSelectedFont(AValue: TFont);
    procedure SetTodayFont(AValue: TFont);
    procedure SetDayFont(AValue: TFont);
    procedure SetDateFormat(AValue: string);
    function GetYearMinusGlyph: TBitmap;
    procedure SetYearMinusGlyph(AValue: TBitmap);
    function GetYearPlusGlyph: TBitmap;
    procedure SetYearPlusGlyph(AValue: TBitmap);
    function GetMonthMinusGlyph: TBitmap;
    procedure SetMonthMinusGlyph(AValue: TBitmap);
    function GetMonthPlusGlyph: TBitmap;
    procedure SetMonthPlusGlyph(AValue: TBitmap);
    procedure SetFirstDayOfWeek(AValue: TFirstDayOfWeek);
    procedure SetYear(AValue: integer);
    procedure SetMonth(AValue: integer);
    procedure SetDay(AValue: integer);
    procedure _SetDate(AValue: TDateTime);
    procedure SetDate(AValue: TDateTime);
    function GetDate: TDateTime;
    procedure SetHeaderFormat(AValue: TCalendarHeaderFormat);
    Procedure CMBorderChanged( Var Message: TMessage ); Message CM_BORDERCHANGED;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure CMSysColorChange(var Message: TMessage); message CM_SYSCOLORCHANGE;
    procedure CMColorChanged(var Message: TMessage); message CM_COLORCHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
  protected
    procedure SetPanelColor(APanel: TCalendarPanel; AYear, AMonth: integer); virtual;
    procedure CMFontChanged(var Msg: TMessage); message CM_FONTCHANGED;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure OnChangeMessages(Sender: TObject);
    procedure OnResizePanels(Sender: TObject);
    procedure OnCanResizePanels(Sender: TObject; var NewWidth, NewHeight: integer; var Resize: boolean);
    procedure SetButtonPositions;
    procedure SetButtonCaptions;
    procedure Loaded; override;
    procedure RepaintCalendar; virtual;
    function SetWeekStart(ADayIndex: integer): integer;
    procedure DoClick(Sender: TObject);
    procedure DoDblClick(Sender: TObject);
    procedure YearButtonClick(Sender: TObject);
    procedure MonthButtonClick(Sender: TObject);
    procedure DoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure SetSelectionStart(AValue: TDateTime);
    procedure SetSelectionEnd(AValue: TDateTime);
    procedure Paint; override;
    procedure DoGetCursor(ItemType: TCalendarItemType; var ACursor: TCursor); virtual;
    procedure DoGetItemStyle(ItemStyle: TDateStyleType; ADate: TDate; var AFont: TFont; var AColor: TColor); virtual;
    property Year: integer read FYear write SetYear;
    property Month: integer read FMonth write SetMonth;
    property Day: integer read FDay write SetDay;
    property DateFormat: string read FDateFormat write SetDateFormat;
    property OnGetItemStyle: TOnGetItemStyleEvent read FOnGetItemStyleEvent write FOnGetItemStyleEvent;
    property OnGetCursor: TOnGetCursorEvent read FOnGetCursor write FOnGetCursor;
    property Cursor: TCursor read FCursor write SetCursor;
    property DayColor: TColor read FDayColor write SetDayColor;
    property WeekEndColor: TColor read FWeekEndColor write SetWeekEndColor;
    property SelectedColor: TColor read FSelectedColor write SetSelectedColor;
    property DayNameColor: TColor read FDayNameColor write SetDayNameColor;
    property WeekColor: TColor read FWeekColor write SetWeekColor;
    property TodayColor: TColor read FTodayColor write SetTodayColor;
    property IndirectColor: TColor read FIndirectColor write SetIndirectColor;
    property HeaderColor: TColor read GetHeaderColor write SetHeaderColor;
    property FooterColor: TColor read GetFooterColor write SetFooterColor;
    property Options: TCalendarOptions read FOptions write SetOptions;
    property Messages: TDateMessages read FMessages write FMessages;
    property SelectedFont: TFont read FSelectedFont write SetSelectedFont;
    property DayFont: TFont read FDayFont write SetDayFont;
    property TodayFont: TFont read FTodayFont write SetTodayFont;
    property WeeksFont: TFont read FWeeksFont write SetWeeksFont;
    property WeekEndFont: TFont read FWeekEndFont write SetWeekEndFont;
    property DateLabelFont: TFont read GetDateLabelFont write SetDateLabelFont;
    property WeekDaysFont: TFont read FWeekDaysFont write SetWeekDaysFont;
    property IndirectFont: TFont read FIndirectFont write SetIndirectFont;
    property YearMinusGlyph: TBitmap read GetYearMinusGlyph write SetYearMinusGlyph;
    property YearPlusGlyph: TBitmap read GetYearPlusGlyph write SetYearPlusGlyph;
    property MonthMinusGlyph: TBitmap read GetMonthMinusGlyph write SetMonthMinusGlyph;
    property MonthPlusGlyph: TBitmap read GetMonthPlusGlyph write SetMonthPlusGlyph;
    property HeaderFormat: TCalendarHeaderFormat read FHeaderFormat write SetHeaderFormat default hfMMMMYYYY;
    property FirstDayOfWeek: TFirstDayOfWeek read FFirstDayOfWeek write SetFirstDayOfWeek default fdMonday;
  public
    property PayrollDates: IisListOfValues read FPayrollDates write FPayrollDates;
    property Date: TDateTime read GetDate write SetDate;
    property SelectionStart: TDateTime read FSelectionStart;
    property SelectionEnd: TDateTime read FSelectionEnd;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function DaysInMonth(AYear, AMonth: integer): integer;
    function WeeksInYear(AYear: integer): integer;
    function MonthToWeek(AMonth: integer): integer;
  end;

  TEvPayrollDatesGadget = class(TEvDashboardGadget)
    procedure pnlContentResize(Sender: TObject);
  private
    FCalendar: TEvCalendar;
    procedure OnResize;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


implementation

uses Types;

const
  NavigateButtonWidth = 23;

{$R *.DFM}

{ TEvPayrollDatesGadget }

constructor TEvPayrollDatesGadget.Create(AOwner: TComponent);
begin
  inherited;
  CompanyDrivenGadget := True;
  FCalendar := TevCalendar.Create(pnlContent);
  FCalendar.Parent := pnlContent;
  FCalendar.ShowHint := True;
  OnResize;
end;

procedure TEvPayrollDatesGadget.OnResize;
begin
  FCalendar.Left := (pnlContent.ClientWidth - pnlContent.Margins.Left - pnlContent.Margins.Right + pnlContent.ShadowDepth - FCalendar.Width) div 2;
  FCalendar.Top := (pnlContent.ClientHeight - pnlContent.Margins.Top - pnlContent.Margins.Bottom - pnlContent.TitleHight + pnlContent.ShadowDepth - FCalendar.Height) div 2 + pnlContent.TitleHight;
end;

procedure TEvPayrollDatesGadget.PrepareData(const AInputParams, AData: IisListOfValues);
var
  Q: IevQuery;
  DATES: IisListOfValues;
  ITEM: IisListOfValues;
  i: integer;
  d: string;

  procedure AddItem(const aStrDate: string);
  begin
    if d <> aStrDate then
    begin
      d := aStrDate;
      i := 1;
    end
    else
      Inc(i);

    DATES.AddValue(aStrDate + '_' + IntToStr(i), ITEM)
  end;

begin
  inherited;

  DATES := TisListOfValues.Create;

  if AInputParams.Value['Cl_Nbr'] <> 0 then
  begin
    ctx_DataAccess.OpenClient(AInputParams.Value['Cl_Nbr']);
    Q := TevQuery.Create('select x.SCHEDULED_PROCESS_DATE, x.SCHEDULED_CHECK_DATE, x.ACTUAL_PROCESS_DATE,'+
      ' y.FREQUENCY, y.PERIOD_BEGIN_DATE, y.PERIOD_END_DATE '+
      ' from PR_SCHEDULED_EVENT x '+
      ' join PR_SCHEDULED_EVENT_BATCH y on y.PR_SCHEDULED_EVENT_NBR=x.PR_SCHEDULED_EVENT_NBR'+
      ' where {AsOfNow<x>} and {AsOfNow<y>} and x.CO_NBR= :Co_Nbr order by x.SCHEDULED_CHECK_DATE');
    Q.Params.AddValue('Co_Nbr', AInputParams.Value['Co_Nbr']);
    Q.Result.First;
    if not Q.Result.Eof then
    begin
      d := '';

      while not Q.Result.Eof do
      begin
        ITEM := TisListOfValues.Create;
        ITEM.AddValue('CHECK_DATE', Q.Result.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime);
        if Q.Result.FieldByName('ACTUAL_PROCESS_DATE').IsNull then
          ITEM.AddValue('ACTUAL_PROCESS_DATE', 0)
        else
          ITEM.AddValue('ACTUAL_PROCESS_DATE', Q.Result.FieldByName('ACTUAL_PROCESS_DATE').AsDateTime);

        with BreakCompanyFreqIntoPayFreqList(Q.Result.FieldByName('FREQUENCY').AsString) do
        try
          if Count > 0 then
            ITEM.AddValue('FREQUENCY', Strings[0]);
        finally
          Free;
        end;

        ITEM.AddValue('PERIOD_BEGIN_DATE', Q.Result.FieldByName('PERIOD_BEGIN_DATE').AsDateTime);
        ITEM.AddValue('PERIOD_END_DATE', Q.Result.FieldByName('PERIOD_END_DATE').AsDateTime);

        if Q.Result.FieldByName('ACTUAL_PROCESS_DATE').IsNull then
          AddItem(DateToStr(Q.Result.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime))
        else if not DATES.ValueExists(Q.Result.FieldByName('SCHEDULED_CHECK_DATE').AsString) then
          AddItem(Q.Result.FieldByName('SCHEDULED_CHECK_DATE').AsString);

        Q.Result.Next;
      end;
    end;
  end;

  AData.AddValue('DATES', DATES);
end;

procedure TEvPayrollDatesGadget.PresentData(const AData: IisListOfValues);
begin
  inherited;
  FCalendar.PayrollDates := IInterface(AData.Value['DATES']) as IisListOfValues;
  FCalendar.Refresh;
  FCalendar.Date := Date;
end;

{ TCalendarPanel }

constructor TCalendarPanel.Create(AOwner: TComponent);
begin
  inherited;
  ControlStyle := ControlStyle + [csAcceptsControls];
  FFlat := False;
  FToday := True;
  ParentColor := False;
end;

procedure TCalendarPanel.SetFlat( AValue: boolean );
begin
  if FFlat <> AValue then
  begin
    FFlat:=AValue;
    Invalidate;
  end;
end;

procedure TCalendarPanel.Paint;
var
  Flags: longint;
  ARect: TRect;
begin
  inherited;
  ARect := ClientRect;

  Canvas.Pen.Style   := psSolid;
  Canvas.Pen.Mode    := pmCopy;
  Canvas.Pen.Color   := clWhite;
  Canvas.Pen.Width   := 1;
  Canvas.Brush.Color := Color;
  Canvas.Font.Assign(Font);

  InflateRect(ARect, -2, -2);
  Canvas.FillRect(ARect);

  Canvas.Brush.Style := bsClear;
  Flags := DrawTextBiDiModeFlags(DT_EXPANDTABS or DT_CENTER or DT_VCENTER or DT_SINGLELINE);
  DrawText(Canvas.Handle, pchar(Text), Length(Text), ARect, Flags);
end;

procedure TCalendarPanel.WMMouseMove(var Message: TWMMouseMove);
var
  aPoint: TPoint;
begin
  inherited;

  if Hint <> '' then
  begin
    aPoint.X := Message.XPos;
    aPoint.Y := Message.YPos;
    aPoint := ClientToScreen(aPoint);
    Application.ActivateHint(aPoint);
  end;
end;


{ TDayNumberPanel }

procedure TDayNumberPanel.Paint;
var
  ARect: TRect;
begin
  inherited;
  ARect := ClientRect;

  if FToday then
  begin
    Canvas.Brush.Style := bsClear;
    Canvas.Pen.Color   := clRed;
    Canvas.Pen.Width := 2;
    Canvas.Ellipse(ARect);
  end;
end;


{TDateHints}

constructor TDateHints.Create;
begin
  inherited Create;
  FHints[0] := 'Next month';
  FHints[1] := 'Previous month';
  FHints[2] := 'Next year';
  FHints[3] := 'Previous year';
  FHints[4] := 'Set date to today';
  FHints[5] := 'This date is today';
  FHints[6] := 'Clear date selection';
end;

procedure TDateHints.Assign(Source: TPersistent);
var
  I: integer;
begin
  if Source is TDateHints then
  begin
    for I := 0 to 6 do
      FHints[I] := TDateHints(Source).Hints[I];
  end
  else
    inherited Assign(Source);
end;

function TDateHints.GetHint(Index: integer): ShortString;
begin
  Result := FHints[Index];
end;

procedure TDateHints.SetHint(Index: integer; AValue: ShortString);
begin
  if FHints[Index] <> AValue then
  begin
    FHints[Index] := AValue;
    Change;
  end;
end;

procedure TDateHints.Change;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

{TDateMessages}

constructor TDateMessages.Create;
var
  I: integer;
begin
  inherited Create;
  FMessages[0] := 'W#';
  for I := 1 to 7 do
    FMessages[I] := ShortDayNames[I];
  for I := 1 to 12 do
    FMonthNames[I] := LongMonthNames[I];
  Hints := TDateHints.Create;
end;

destructor TDateMessages.Destroy;
begin
  FHints.Free;
  inherited Destroy;
end;

procedure TDateMessages.Assign(Source: TPersistent);
var
  I: integer;
begin
  if Source is TDateMessages then
  begin
    for I := 0 to 7 do
      FMessages[I] := TDateMessages(Source).Messages[I];
    for I := 1 to 12 do
      FMonthNames[I] := TDateMessages(Source).MonthNames[I];

    FHints.Assign(TDateMessages(Source).Hints);
  end
  else
    inherited Assign(Source);
end;

function TDateMessages.GetMessage(Index: integer): ShortString;
begin
  Result := FMessages[Index];
end;

procedure TDateMessages.SetMessage(Index: integer; AValue: ShortString);
begin
  if FMessages[Index] <> AValue then
  begin
    FMessages[Index] := AValue;
    Change;
  end;
end;

function TDateMessages.GetMonthName(Index: integer): ShortString;
begin
  Result := FMonthNames[Index];
end;

procedure TDateMessages.SetOnChange(AOnChange: TNotifyEvent);
begin
  FOnChange := AOnChange;
  FHints.OnChange := AOnChange;
end;

procedure TDateMessages.SetMonthName(Index: integer; AValue: ShortString);
begin
  if FMonthNames[Index] <> AValue then
  begin
    FMonthNames[Index] := AValue;
    Change;
  end;
end;

procedure TDateMessages.Change;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

{TCalendarButton}

procedure TCalendarButton.CMDesignHitTest(var Msg: TCMDesignHitTest);
begin
  if Parent.CanFocus and
    PtInRect(Rect(0, 0, Width, Height), SmallPointToPoint(Msg.Pos)) then
    Msg.Result := 1;
end;

{TEvCalendar}

constructor TEvCalendar.Create(AOwner: TComponent);
var
  I, X: byte;
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle - [csSetCaption];

  Height  := 200;
  Width   := 220;
  Caption := '';
  ParentColor := False;

  BorderWidth := 1;
  BevelInner  := bvNone;
  BevelOuter  := bvNone;
  BorderStyle := bsSingle;

  OnResize    := OnResizePanels;
  OnCanResize := OnCanResizePanels;

  FIndirectColor := Color;
  FSelectedColor := $6A240A;
  FDayColor      := Color;
  FDayNameColor  := Color;
  FWeekColor     := Color;
  FTodayColor    := Color;

  FOptions := [
    coShowDateLabel,
    coShowHeader,
    coShowFooter,
    coShowWeekDays,
    coShowWeeks,
    coFlatButtons,
    coTransparentButtons,
    coYearButtonVisible,
    coMonthButtonVisible,
    csUseWeekEndFont,
    csUseWeekEndColor,
    coShowNextMonth,
    coShowPreviousMonth
    ];

  Messages := TDateMessages.Create;
  Messages.OnChange := OnChangeMessages;

  FSelectedFont := TFont.Create;
  FSelectedFont.Color := clWhite;
  FSelectedFont.OnChange := OnChangeMessages;

  FTodayFont := TFont.Create;
  FTodayFont.Style := [fsBold];
  FTodayFont.OnChange := OnChangeMessages;

  FIndirectFont := TFont.Create;
  FIndirectFont.Color := clSilver;
  FIndirectFont.OnChange := OnChangeMessages;

  FDayFont := TFont.Create;
  FDayFont.Color := $6A240A;
  FDayFont.OnChange := OnChangeMessages;

  FWeekDaysFont := TFont.Create;
  FWeekDaysFont.Color := $6A240A;
  FWeekDaysFont.OnChange := OnChangeMessages;

  FWeeksFont := TFont.Create;
  FWeeksFont.Color := clMaroon;
  FWeeksFont.OnChange := OnChangeMessages;

  FWeekEndFont := TFont.Create;
  FWeekEndFont.Color := $6A240A;
  FWeekEndFont.OnChange := OnChangeMessages;

  FWeekEndColor := clWhite;

  FFirstDayOfWeek := fdSunday;
  FHeaderFormat   := hfMMMMYYYY;

  FDateFormat := 'mm/dd/yyyy';

  FPanel_Header := TPanel.Create(Self);
  FPanel_Header.Parent     := Self;
  FPanel_Header.Height     := 26;
  FPanel_Header.Align      := alTop;
  FPanel_Header.Font.Style := [fsBold];
  FPanel_Header.Font.Color := clWhite;
  FPanel_Header.Color      := $6A240A;
  FPanel_Header.BevelInner := bvNone;
  FPanel_Header.BevelOuter := bvNone;
  FPanel_Header.OnClick    := DoClick;
  FPanel_Header.OnDblClick := DoDblClick;

  FPanel_Footer := TPanel.Create(Self);
  FPanel_Footer.Parent     := Self;
  FPanel_Footer.Height     := 26;
  FPanel_Footer.Align      := alBottom;
  FPanel_Footer.BevelInner := bvNone;
  FPanel_Footer.BevelOuter := bvNone;//bvRaised;
  FPanel_Footer.Color      := clWhite;
  FPanel_Footer.OnClick    := DoClick;
  FPanel_Footer.OnDblClick := DoDblClick;

  FDateLabel := TLabel.Create(Self);
  FDateLabel.Parent := FPanel_Footer;
  FDateLabel.Left   := 10;
  FDateLabel.Font.Style := [fsBold];
  FDateLabel.Font.Color := clBlack;
  SetDateLabelFont(Font);
  FDateLabel.OnClick    := DoClick;
  FDateLabel.OnDblClick := DoDblClick;

  FPanel_Calendar := TPanel.Create(Self);
  FPanel_Calendar.Font.Color := clWhite;
  FPanel_Calendar.Parent  := Self;
  FPanel_Calendar.Top     := FPanel_Header.Top + FPanel_Header.Height + 1;
  FPanel_Calendar.Align   := alClient;
  FPanel_Calendar.BevelInner := bvNone;
  FPanel_Calendar.BevelOuter := bvNone;//bvRaised;
  FPanel_Calendar.ParentColor := true;
  FPanel_Calendar.OnClick := DoClick;
  FPanel_Calendar.OnDblClick := DoDblClick;

  FYear_Plus := TCalendarButton.Create(Self);
  FYear_Plus.Parent  := FPanel_Header;
  FYear_Plus.Width   := NavigateButtonWidth;
  FYear_Plus.Top     := 2;
  FYear_Plus.Caption := '>>';
  FYear_Plus.Flat    := true;
  FYear_Plus.OnClick := YearButtonClick;

  FYear_Minus := TCalendarButton.Create(Self);
  FYear_Minus.Parent  := FPanel_Header;
  FYear_Minus.Width   := NavigateButtonWidth;
  FYear_Minus.Top     := 2;
  FYear_Minus.Caption := '<<';
  FYear_Minus.Flat    := true;
  FYear_Minus.OnClick := YearButtonClick;

  FMonth_Plus := TCalendarButton.Create(Self);
  FMonth_Plus.Parent  := FPanel_Header;
  FMonth_Plus.Width   := NavigateButtonWidth;
  FMonth_Plus.Top     := 2;
  FMonth_Plus.Caption := '>';
  FMonth_Plus.Flat    := true;
  FMonth_Plus.OnClick := MonthButtonClick;

  FMonth_Minus := TCalendarButton.Create(Self);
  FMonth_Minus.Parent  := FPanel_Header;
  FMonth_Minus.Width   := NavigateButtonWidth;
  FMonth_Minus.Top     := 2;
  FMonth_Minus.Caption := '<';
  FMonth_Minus.Flat    := true;
  FMonth_Minus.OnClick := MonthButtonClick;

  for I := 0 to 6 do
  begin
    for X := 0 to 7 do
    begin
      if (X <> 0) and (I <> 0) then
        FPanel_Days[I, X] := TDayNumberPanel.Create(Self)
      else
        FPanel_Days[I, X] := TCalendarPanel.Create(Self);
        
      with FPanel_Days[I, X] do
      begin
        Parent := FPanel_Calendar;

        if I = 0 then
          Color := FWeekColor
        else if X = 0 then
          Color := DayNameColor
        else
          Color := FDayColor;

        if (X <> 0) and (I <> 0) then
        begin
          OnMouseDown := DoMouseDown;
          OnClick     := DoClick;
          OnDblClick  := DoDblClick;
        end;
      end;
    end;
  end;

  FSelectionStart := 0;
  FSelectionEnd   := 0;

  _SetDate(SysUtils.Date);

end;

destructor TEvCalendar.Destroy;
var
  I, X: byte;
begin
  FDateLabel.Free;
  FMessages.Free;
  FSelectedFont.Free;
  FTodayFont.Free;
  FDayFont.Free;
  FWeekDaysFont.Free;
  FWeeksFont.Free;
  FWeekEndFont.Free;
  FIndirectFont.Free;

  for I := 0 to 6 do
    for X := 0 to 7 do
      FPanel_Days[I, X].Free;

  FYear_Plus.Free;
  FYear_Minus.Free;
  FMonth_Plus.Free;
  FMonth_Minus.Free;
  FPanel_Calendar.Free;
  FPanel_Header.Free;
  FPanel_Footer.Free;

  inherited Destroy;
end;

procedure TEvCalendar.Loaded;
begin
  inherited;

  SetYearPlusGlyph(FYear_Plus.Glyph);
  SetYearMinusGlyph(FYear_Minus.Glyph);
  SetMonthPlusGlyph(FMonth_Plus.Glyph);
  SetMonthMinusGlyph(FMonth_Minus.Glyph);

  if csSetTodayOnStartup in FOptions then
    _SetDate(SysUtils.Date);

  RepaintCalendar;
end;

procedure TEvCalendar.SetButtonCaptions;
var
  X, I, Y: shortint;
begin
  FPanel_Days[0, 0].Caption := Messages[0];
  for I := 0 to 6 do
  begin
    FPanel_Days[I, 0].Font.Assign(FWeeksFont);
  end;
                          
  X := byte(FFirstDayOfWeek);
  Y := 0;

  for I := X to 6 do
  begin
    Inc(Y);
    FPanel_Days[0, Y].Caption := Messages[I + 1];
    FPanel_Days[0, Y].Font.Assign(FWeekDaysFont);
  end;

  for I := 0 to X - 1 do
  begin
    Inc(Y);
    FPanel_Days[0, Y].Caption := Messages[I + 1];
    FPanel_Days[0, Y].Font.Assign(FWeekDaysFont);
  end;

  FMonth_Minus.Hint := FMessages.Hints[1];
  FMonth_Plus.Hint  := FMessages.Hints[0];
  FYear_Minus.Hint  := FMessages.Hints[3];
  FYear_Plus.Hint   := FMessages.Hints[2];
end;

procedure TEvCalendar.SetButtonPositions;
var
  I, X: byte;
  PanelHeight: integer;
  PanelWidth: integer;
  DifferenceWidth: byte;
  DifferenceHeight: byte;
  NumberOfRows: byte;
  NumberOfCols: byte;
  Correction: byte;
  ButtonPosition: integer;
begin

  ButtonPosition := FPanel_Header.ClientWidth - FYear_Plus.ClientWidth - 3;
  if not FYear_Plus.Visible then
    ButtonPosition := -100;
  FYear_Plus.Left := ButtonPosition;

  ButtonPosition := 3;
  if not FYear_Minus.Visible then
    ButtonPosition := -100;
  FYear_Minus.Left := ButtonPosition;

  if FYear_Plus.Visible then
    ButtonPosition := FYear_Plus.Left - FMonth_Plus.Width - 2
  else
    ButtonPosition := FPanel_Header.ClientWidth - FMonth_Plus.ClientWidth - 3;

  if not FMonth_Plus.Visible then
    ButtonPosition := -100;
  FMonth_Plus.Left := ButtonPosition;

  if FYear_Minus.Visible then
    ButtonPosition := FYear_Minus.Left + FYear_Minus.Width + 2
  else
    ButtonPosition := 3;

  if not FMonth_Minus.Visible then
    ButtonPosition := -100;
  FMonth_Minus.Left := ButtonPosition;

  NumberOfRows := 7;
  NumberOfCols := 8;

  if not (coShowWeeks in Options) then
    Dec(NumberOfCols);
  if not (coShowWeekDays in Options) then
    Dec(NumberOfRows);

  PanelHeight := FPanel_Calendar.ClientHeight div NumberOfRows;
  PanelWidth  := FPanel_Calendar.ClientWidth div NumberOfCols;

  DifferenceHeight := ((FPanel_Calendar.ClientHeight) mod NumberOfRows) div 2;
  DifferenceWidth  := ((FPanel_Calendar.ClientWidth) mod NumberOfCols) div 2;

  for I := 0 to 6 do
  begin
    for X := 0 to 7 do
    begin
      with FPanel_Days[I, X] do
      begin
        Width  := PanelWidth;
        Height := PanelHeight;

        if (coShowWeeks in Options) then
          Correction := 0
        else
          Correction := 1;

        if (coShowWeeks in Options) or
          ( not (coShowWeeks in Options) and (X <> 0)) then
          Left := ((X - Correction) * PanelWidth) + DifferenceWidth
        else
          Left := -100;

        if (coShowWeekDays in Options) then
          Correction := 0
        else
          Correction := 1;

        if (coShowWeekDays in Options) or
          ( not (coShowWeekDays in Options) and (I <> 0)) then
          Top := ((I - Correction) * PanelHeight) + DifferenceHeight
        else
          Top := -100;
      end;
    end;
  end;
end;

procedure TEvCalendar.OnResizePanels(Sender: TObject);
begin
  SetButtonPositions;
end;

procedure TEvCalendar.OnChangeMessages(Sender: TObject);
begin
  RepaintCalendar;
end;

procedure TEvCalendar.OnCanResizePanels(Sender: TObject;
  var NewWidth, NewHeight: integer; var Resize: boolean);
begin
  if NewWidth < (NavigateButtonWidth * 4) + 6 then
  begin
    NewWidth := (NavigateButtonWidth * 4) + 16;
    Resize   := true;
  end;
end;

procedure TEvCalendar.CMBorderChanged( Var Message: TMessage );
begin
  inherited;
  RepaintCalendar;
end;

procedure TEvCalendar.CMCtl3DChanged(var Message: TMessage);
begin
  if NewStyleControls and (BorderStyle = bsSingle) then
    RecreateWnd;
  inherited;
end;

procedure TEvCalendar.CMSysColorChange(var Message: TMessage);
begin
  inherited;
  if not (csLoading in ComponentState) then
  begin
    Message.Msg := WM_SYSCOLORCHANGE;
    DefaultHandler(Message);
  end;
end;

procedure TEvCalendar.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  RepaintCalendar;
end;

procedure TEvCalendar.CMColorChanged(var Message: TMessage);
begin
  inherited;
  RecreateWnd;
end;

procedure TEvCalendar.CMFontChanged(var Msg: TMessage);
begin
  inherited;
  RepaintCalendar;
end;

procedure TEvCalendar.CreateParams(var Params: TCreateParams);
const
  BorderStyles: array[TBorderStyle] of DWORD = (0, WS_BORDER);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or BorderStyles[BorderStyle];
    if NewStyleControls and Ctl3D and (BorderStyle = bsSingle) then
    begin
      Style   := Style and not WS_BORDER;
      ExStyle := ExStyle or WS_EX_CLIENTEDGE;
    end;
    WindowClass.Style := WindowClass.Style and not (CS_HREDRAW or CS_VREDRAW);
  end;
end;

procedure TEvCalendar.DoGetItemStyle(ItemStyle: TDateStyleType;
  ADate: TDate; var AFont: TFont; var AColor: TColor);
begin
  if Assigned(FOnGetItemStyleEvent) then
    FOnGetItemStyleEvent(Self, ItemStyle, ADate, AFont, AColor);
end;

procedure TEvCalendar.DoGetCursor(ItemType: TCalendarItemType; var ACursor: TCursor);
begin
  if assigned(FOnGetCursor) then
    FOnGetCursor(Self, ItemType, ACursor);
end;

procedure TEvCalendar.Paint;
begin
  inherited;
  RepaintCalendar;
end;

procedure TEvCalendar.SetSelectedColor(AValue: TColor);
begin
  if FSelectedColor <> AValue then
  begin
    FSelectedColor := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetWeekEndColor(AValue: TColor);
begin
  if FWeekEndColor <> AValue then
  begin
    FWeekEndColor := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetHeaderColor(AValue: TColor);
begin
  if FPanel_Header.Color <> AValue then
    FPanel_Header.Color := AValue;
end;

function TEvCalendar.GetHeaderColor: TColor;
begin
  Result := FPanel_Header.Color;
end;

procedure TEvCalendar.SetFooterColor(AValue: TColor);
begin
  if FPanel_Footer.Color <> AValue then
    FPanel_Footer.Color := AValue;
end;

function TEvCalendar.GetFooterColor: TColor;
begin
  Result := FPanel_Footer.Color;
end;

procedure TEvCalendar.SetDayNameColor(AValue: TColor);
var
  I: byte;
begin
  if FDayNameColor <> AValue then
  begin
    FDayNameColor := AValue;
    for I := 1 to 7 do
      FPanel_Days[0, I].Color := FDayNameColor;
  end;
end;

procedure TEvCalendar.SetWeekColor(AValue: TColor);
var
  I: byte;
begin
  if FWeekColor <> AValue then
  begin
    FWeekColor := AValue;
    for I := 0 to 6 do
      FPanel_Days[I, 0].Color := FWeekColor;
  end;
end;

procedure TEvCalendar.SetTodayColor(AValue: TColor);
begin
  if FTodayColor <> AValue then
  begin
    FTodayColor := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetIndirectColor(AValue: TColor);
begin
  if FIndirectColor <> AValue then
  begin
    FIndirectColor := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetCursor(AValue: TCursor);
begin
  if FCursor <> AValue then
  begin
    FCursor := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetDayColor(AValue: TColor);
var
  I, X: byte;
begin
  if FDayColor <> AValue then
  begin
    FDayColor := AValue;

    for I := 1 to 6 do
    begin
      for X := 1 to 7 do
      begin
        FPanel_Days[I, X].Color := FDayColor;
      end;
    end;

    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetOptions(AValue: TCalendarOptions);
var
  X, Y: integer;
begin
  if FOptions <> AValue then
  begin
    FOptions := AValue;

    if not (coShowHeader in Options) then
    begin
      FPanel_Header.Align := alNone;
      FPanel_Header.Top   := -1000;
    end
    else
      FPanel_Header.Align := alTop;

    if not (coShowFooter in Options) then
    begin
      FPanel_Footer.Align := alNone;
      FPanel_Footer.Top   := -1000;
    end
    else
      FPanel_Footer.Align := alBottom;

    FYear_Plus.Flat   := coFlatButtons in FOptions;
    FYear_Minus.Flat  := coFlatButtons in FOptions;
    FMonth_Plus.Flat  := coFlatButtons in FOptions;
    FMonth_Minus.Flat := coFlatButtons in FOptions;

    FYear_Plus.Transparent   := coTransparentButtons in FOptions;
    FYear_Minus.Transparent  := coTransparentButtons in FOptions;
    FMonth_Plus.Transparent  := coTransparentButtons in FOptions;
    FMonth_Minus.Transparent := coTransparentButtons in FOptions;

    FYear_Plus.Visible   := coYearButtonVisible in FOptions;
    FYear_Minus.Visible  := coYearButtonVisible in FOptions;
    FMonth_Plus.Visible  := coMonthButtonVisible in FOptions;
    FMonth_Minus.Visible := coMonthButtonVisible in FOptions;

    SetDateLabelFont(FDateLabel.Font);

    for X := 1 to 6 do
      for Y := 1 to 7 do
        FPanel_Days[X, Y].Flat:=csFlatDays in FOptions;

    for X := 0 to 6 do
      FPanel_Days[X, 0].Flat:=coFlatHeaders in FOptions;

    for Y := 1 to 7 do
      FPanel_Days[0,Y].Flat:=coFlatHeaders in FOptions;

    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetWeekDaysFont(AValue: TFont);
begin
  FWeekDaysFont.Assign(AValue);
end;

procedure TEvCalendar.SetIndirectFont(AValue: TFont);
begin
  FIndirectFont.Assign(AValue);
end;

procedure TEvCalendar.SetSelectedFont(AValue: TFont);
begin
  FSelectedFont.Assign(AValue);
end;

procedure TEvCalendar.SetTodayFont(AValue: TFont);
begin
  FTodayFont.Assign(AValue);
end;

function TEvCalendar.GetDateLabelFont: TFont;
begin
  Result := FDateLabel.Font;
end;

procedure TEvCalendar.SetDateLabelFont(AValue: TFont);
begin
  FDateLabel.Font.Assign(AValue);

  if coShowDateLabel in FOptions then
    FDateLabel.Top := (FPanel_Footer.Height - FDateLabel.Height) div 2
  else
    FDateLabel.Top := -100;
end;

procedure TEvCalendar.SetDayFont(AValue: TFont);
begin
  FDayFont.Assign(AValue);
end;

procedure TEvCalendar.SetWeeksFont(AValue: TFont);
begin
  FWeeksFont.Assign(AValue);
end;

procedure TEvCalendar.SetWeekEndFont(AValue: TFont);
begin
  FWeekEndFont.Assign(AValue);
end;

function TEvCalendar.GetYearMinusGlyph: TBitmap;
begin
  Result := FYear_Minus.Glyph;
end;

procedure TEvCalendar.SetYearMinusGlyph(AValue: TBitmap);
begin
  with FYear_Minus do
  begin
    Glyph.Assign(AValue);
    if Glyph.Empty then
      Caption := '<<'
    else
      Caption := '';
  end;
end;

function TEvCalendar.GetYearPlusGlyph: TBitmap;
begin
  Result := FYear_Plus.Glyph;
end;

procedure TEvCalendar.SetYearPlusGlyph(AValue: TBitmap);
begin
  with FYear_Plus do
  begin
    Glyph.Assign(AValue);
    if Glyph.Empty then
      Caption := '>>'
    else
      Caption := '';
  end;
end;

function TEvCalendar.GetMonthMinusGlyph: TBitmap;
begin
  Result := FMonth_Minus.Glyph;
end;

procedure TEvCalendar.SetMonthMinusGlyph(AValue: TBitmap);
begin
  with FMonth_Minus do
  begin
    Glyph.Assign(AValue);
    if Glyph.Empty then
      Caption := '<'
    else
      Caption := '';
  end;
end;

function TEvCalendar.GetMonthPlusGlyph: TBitmap;
begin
  Result := FMonth_Plus.Glyph;
end;

procedure TEvCalendar.SetMonthPlusGlyph(AValue: TBitmap);
begin
  with FMonth_Plus do
  begin
    Glyph.Assign(AValue);
    if Glyph.Empty then
      Caption := '>'
    else
      Caption := '';
  end;
end;

procedure TEvCalendar.SetFirstDayOfWeek(AValue: TFirstDayOfWeek);
begin
  if FFirstDayOfWeek <> AValue then
  begin
    FFirstDayOfWeek := AValue;
    RecreateWnd;
  end;
end;

function TEvCalendar.DaysInMonth(AYear, AMonth: integer): integer;
const
  NumberOfDays: array[1..12] of integer =
    (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := 0;
  if not (AMonth in [1..12]) then
    Exit;
  Result := NumberOfDays[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then
    Inc(Result);
end;

function TEvCalendar.MonthToWeek(AMonth: integer): integer;
var
  I, _DayOfWeek: integer;
begin
  Result := 0;
  for I := 1 to FMonth - 1 do
    Inc(Result, DaysInMonth(FYear, I));

  _DayOfWeek := SetWeekStart(DayOfWeek(EncodeDate(FYear, 1, 1)));
  if _DayOfWeek > 4 then
    Dec(Result, 7 - _DayOfWeek)
  else
    Inc(Result, _DayOfWeek - 1);

  Result := (Result div 7) + 1;
  if (FFirstDayOfWeek = fdSunday) and
    (DayOfWeek(EncodeDate(FYear, AMonth, 1)) = 7) then
    Dec(Result);
end;

function TEvCalendar.SetWeekStart( ADayIndex: Integer ): Integer;
begin
  Result := ADayIndex;
  if FFirstDayOfWeek = fdMonday then
  	if Result = 1 then
      Result := 7
    else
      Dec(Result);
End;

function TEvCalendar.WeeksInYear(AYear: integer): integer;
var
  _DayOfWeek: integer;
begin
  if IsLeapYear(AYear) then
    Result := 366
  else
    Result := 365;

  _DayOfWeek := SetWeekStart(DayOfWeek(EncodeDate(AYear, 1, 1)));
  if _DayOfWeek > 4 then
    Dec(Result, _DayOfWeek)
  else
    Inc(Result, 7 - _DayOfWeek);

  _DayOfWeek := SetWeekStart(DayOfWeek(EncodeDate(AYear, 12, 31)));
  if _DayOfWeek > 3 then
    Inc(Result, 7 - _DayOfWeek)
  else
    Dec(Result, _DayOfWeek);

  Result := Result div 7;
  if Result = 51 then
    Result := 52;
end;

procedure TEvCalendar.SetYear(AValue: integer);
begin
  if AValue <> FYear then
  begin
    if AValue < 1900 then
      FYear := 1900
    else if AValue > 2100 then
      FYear := 2100
    else
      FYear := AValue;

    if not (csLoading in ComponentState) then
      RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetMonth(AValue: integer);
begin
  if AValue <> FMonth then
  begin
    if AValue < 1 then
      FMonth := 1
    else if AValue > 12 then
      FMonth := 12
    else
      FMonth := AValue;

    if DaysInMonth(FYear, FMonth) < FDay then
      FDay := DaysInMonth(FYear, FMonth);

    if not (csLoading in ComponentState) then
      RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetDay(AValue: integer);
begin
  if AValue <> FDay then
  begin
    if AValue < 1 then
      FDay := 1
    else if AValue > DaysInMonth(FYear, FMonth) then
      FDay := DaysInMonth(FYear, FMonth)
    else
      FDay := AValue;

    if not (csLoading in ComponentState) then
      RepaintCalendar;
  end;
end;

procedure TEvCalendar._SetDate(AValue: TDateTime);
var
  Year, Month, Day: word;
begin
  DecodeDate(AValue, Year, Month, Day);
  FYear  := Year;
  FMonth := Month;
  FDay   := Day;
end;

procedure TEvCalendar.SetDate(AValue: TDateTime);
begin
  _SetDate(AValue);
  RepaintCalendar;
end;

function TEvCalendar.GetDate: TDateTime;
begin
  Result := EncodeDate(FYear, FMonth, FDay);
end;

procedure TEvCalendar.SetHeaderFormat(AValue: TCalendarHeaderFormat);
begin
  if AValue <> FHeaderFormat then
  begin
    FHeaderFormat := AValue;
    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetPanelColor(APanel: TCalendarPanel; AYear, AMonth: integer);
var
  PanelDate:  TDateTime;
  IsSelected: boolean;
  MonthNumber: Byte;

  procedure InternalSetPanel( APanel: TCalendarPanel; ItemStyle: TDateStyleType );
  var
    FFont:      TFont;
    FColor:     TColor;
  begin
    FFont := TFont.Create;
    try
      case ItemStyle of
        stSelected:
          begin
            FColor := FSelectedColor;
            FFont.Assign(FSelectedFont);
          end;
        stToday:
          begin
            FColor := FTodayColor;
            FFont.Assign(FTodayFont);
          end;
        stDay, stWeekEnd:
          begin
            FColor := FDayColor;
            FFont.Assign(FDayFont);
          end;
(*        stWeekEnd:
          begin
            if csUseWeekEndFont in FOptions then
              APanel.Font.Assign(FWeekEndFont)
            else
              APanel.Font.Assign(FDayFont);

            if csUseWeekEndColor in FOptions then
              FColor := FWeekEndColor
            else
              FColor := FDayColor;
          end; *)
        stIndirect:
          begin
            FColor := FIndirectColor;
            FFont.Assign(FIndirectFont);
          end;
        stCheckDate:
          begin
            FColor := FDayColor;
            FFont.Assign(FDayFont);
            FFont.Color := clRed;
            FFont.Style := [fsBold];
          end;
        stProcessedDate:
          begin
            FColor := FDayColor;
            FFont.Assign(FDayFont);
            FFont.Color := clGreen;
            FFont.Style := [fsBold];
          end;
      end;

      DoGetItemStyle( ItemStyle, PanelDate, FFont, FColor);
      APanel.Font.Assign(FFont);
      APanel.Color := FColor;
    finally
      FFont.Free;
    end;
  end;

var
  Item: IisListOfValues;

  function ShowHintMessage(aSel: boolean): boolean;
  var
    i: integer;
    Hnt: string;
  begin
    Result := False;
    Hnt := '';
    i := 1;
    if aSel then
      InternalSetPanel( APanel, stSelected );

    while (AMonth = FMonth) and Assigned(FPayrollDates) and FPayrollDates.ValueExists(DateToStr(PanelDate) + '_' + IntToStr(i)) do
    begin
      Item := IInterface(FPayrollDates.Value[DateToStr(PanelDate) + '_' + IntToStr(i)]) as IisListOfValues;
      if Item.Value['ACTUAL_PROCESS_DATE'] = 0 then
      begin
        if i = 1 then
        begin
          if not aSel  then
            InternalSetPanel(APanel, stCheckDate);
          Hnt := 'Scheduled Process Date'#13#10'Check Date '+DateToStr(Item.Value['CHECK_DATE']);
        end;
      end
      else
      begin
        if i = 1 then
        begin
          if not aSel  then
            InternalSetPanel(APanel, stProcessedDate);
          Hnt := 'Check Date'#13#10'Actual Process Date '+DateToStr(Item.Value['ACTUAL_PROCESS_DATE']);
        end;
      end;

      Hnt := Hnt +
        #13#10'Frequency #'+IntToStr(i)+ ' ' +ReturnDescription(FrequencyType_ComboChoices, Item.Value['FREQUENCY'])+
        #13#10'Period begin date #'+IntToStr(i)+ ' ' +DateToStr(Item.Value['PERIOD_BEGIN_DATE'])+
        #13#10'Period end date #'+IntToStr(i)+ ' ' +DateToStr(Item.Value['PERIOD_END_DATE']);

      Inc(i);
    end;

    if i = 2 then
    begin
      Hnt := StringReplace(Hnt, 'Frequency #1', 'Frequency', []);
      Hnt := StringReplace(Hnt, 'Period begin date #1', 'Period begin date', []);
      Hnt := StringReplace(Hnt, 'Period end date #1', 'Period end date', []);
    end;

    if Hnt <> '' then
    begin
      APanel.Hint := Hnt;
      APanel.ShowHint := True;
      Result := True;
    end;
  end;
begin
  APanel.Tag := AMonth;

  PanelDate  := EncodeDate(AYear, AMonth, StrToInt(APanel.Caption));
  IsSelected := (PanelDate >= Trunc(FSelectionStart)) and
    (PanelDate <= FSelectionEnd);

  APanel.Hint := '';
  APanel.ShowHint := False;
  APanel.Today := PanelDate = SysUtils.Date;

  if IsSelected then
    ShowHintMessage( True)
  else
  begin
    if not ShowHintMessage(False) then
    begin
      if PanelDate = SysUtils.Date then
      begin
        InternalSetPanel( APanel, stToday );
        APanel.Hint  := FMessages.Hints[5];
      end
      else
      begin
        if AMonth = FMonth then
        begin
          if DayOfWeek(PanelDate) in [1, 7] then
            InternalSetPanel( APanel, stWeekEnd )
          else
            InternalSetPanel( APanel, stDay );
        end
        else
        begin
          InternalSetPanel( APanel, stIndirect );

          MonthNumber := FMonth + 1;
          if MonthNumber > 12 then
            MonthNumber := 1;

          if ( not (coShowNextMonth in FOptions)) and
            (AMonth = MonthNumber) then
            APanel.Font.Color := APanel.Color;

          MonthNumber := FMonth - 1;
          if MonthNumber < 1 then
            MonthNumber := 12;

          if ( not (coShowPreviousMonth in FOptions)) and
            (AMonth = MonthNumber) then
            APanel.Font.Color := APanel.Color;
        end;
      end;
    end;  
  end;

  APanel.Invalidate;
end;

procedure TEvCalendar.RepaintCalendar;
var
  AType:  TCalendarItemType;
  ACursor: TCursor;
  FirstDate: TDateTime;
  TotalDays: integer;
  WeekDay: integer;
  CurrentDay: integer;
  WeekNumber: integer;
  WeeksYear: integer;
  X, Y:   integer;
  SMonth: ShortString;
  Item: IisListOfValues;
  SelectedDate: TDateTime;
begin
  SetButtonPositions;
  SetButtonCaptions;

  FirstDate := EncodeDate(FYear, FMonth, 1);
  WeekDay   := SetWeekStart(DayOfWeek(FirstDate));
  TotalDays := DaysInMonth(FYear, FMonth);

  If FDay > TotalDays Then FDay:=TotalDays;

  CurrentDay := 1;

  for X := 0 to 5 do
  begin
    for Y := 0 to 6 do
    begin
      if (X = 0) and (Y + 1 < WeekDay) then
      begin
        if FMonth = 1 then
        begin
          FPanel_Days[X + 1, Y + 1].Caption :=
            IntToStr(DaysInMonth(FYear - 1, 12) - WeekDay + Y + 2);

          SetPanelColor(FPanel_Days[X + 1, Y + 1], FYear - 1, 12);
        end
        else
        begin
          FPanel_Days[X + 1, Y + 1].Caption :=
            IntToStr(DaysInMonth(FYear, FMonth - 1) - WeekDay + Y + 2);

          SetPanelColor(FPanel_Days[X + 1, Y + 1], FYear, FMonth - 1);
        end;
      end
      else
      begin
        if CurrentDay > TotalDays then
        begin
          FPanel_Days[X + 1, Y + 1].Caption :=
            IntToStr(CurrentDay - TotalDays);

          if FMonth = 12 then
            SetPanelColor(FPanel_Days[X + 1, Y + 1], FYear + 1, 1) Else
            SetPanelColor(FPanel_Days[X + 1, Y + 1], FYear, FMonth + 1);
        end
        else
        begin
          FPanel_Days[X + 1, Y + 1].Caption := IntToStr(CurrentDay);
          SetPanelColor(FPanel_Days[X + 1, Y + 1], FYear, FMonth);
        end;

        Inc(CurrentDay);
      end;
    end;
  end;


  SMonth := IntToStr(FMonth);
  if Length(SMonth) = 1 then
    SMonth := '0' + SMonth;

  case FHeaderFormat of
    hfMMMMYYYY: FPanel_Header.Caption :=
        Format('%s, %d', [FMessages.FMonthNames[FMonth], FYear]);
    hfYYYYMMMM: FPanel_Header.Caption :=
        Format('%d, %s', [FYear, FMessages.FMonthNames[FMonth]]);
    hfMMYYYY: FPanel_Header.Caption   := Format('%s.%d', [SMonth, FYear]);
    hfYYYYMM: FPanel_Header.Caption   := Format('%d.%s', [FYear, SMonth]);
  end;

  FPanel_Header.Invalidate;

  WeeksYear := WeeksInYear(FYear);

  for X := 0 to 5 do
  begin
    WeekNumber := MonthToWeek(FMonth) + X;
    if WeekNumber > WeeksYear then
      WeekNumber := WeekNumber - WeeksYear;
    FPanel_Days[X + 1, 0].Caption := IntToStr(WeekNumber);
    FPanel_Days[X + 1, 0].Invalidate;
  end;

  ACursor := Cursor;
  DoGetCursor(itHeader, ACursor);
  FPanel_Header.Cursor := ACursor;

  ACursor := Cursor;
  DoGetCursor(itFooter, ACursor);
  FPanel_Footer.Cursor := ACursor;

  ACursor := Cursor;
  FPanel_Calendar.Cursor := ACursor;

  for X := 0 to 6 do
  begin
    for Y := 0 to 7 do
    begin
      ACursor := Cursor;

      if (X = 0) and (Y = 0) then
        AType := itCW
      else if (X = 0) and (Y <> 0) then
        AType := itWeek
      else if (X <> 0) and (Y = 0) then
        AType := itDay
      else
        AType := itCalendar;

      DoGetCursor(AType, ACursor);
      FPanel_Days[X, Y].Cursor := ACursor;
    end;
  end;

  if FSelectionStart = 0 then
    SelectedDate := now
  else
    SelectedDate := FSelectionStart;

  if Assigned(FPayrollDates) and FPayrollDates.ValueExists(DateToStr(SelectedDate) + '_1') then
    Item := IInterface(FPayrollDates.Value[DateToStr(SelectedDate) + '_1']) as IisListOfValues;

  if Assigned(Item) then
  begin
    if Item.Value['ACTUAL_PROCESS_DATE'] <> 0 then
      FDateLabel.Caption := ' Processed on '+DateToStr(Item.Value['ACTUAL_PROCESS_DATE'])
    else
      FDateLabel.Caption := ' Check Date '+DateToStr(Item.Value['CHECK_DATE']);
  end
  else
    FDateLabel.Caption := '';
end;

procedure TEvCalendar.MonthButtonClick(Sender: TObject);
begin
  if Sender = FMonth_Plus then
  begin
    if FMonth = 12 then
    begin
      Inc(FYear);
      FMonth := 1;
    end
    else
      Inc(FMonth)
  end
  else
  begin
    if FMonth = 1 then
    begin
      Dec(FYear);
      FMonth := 12;
    end
    else
      Dec(FMonth)
  end;

  RepaintCalendar;
  Click;
end;

procedure TEvCalendar.DoDblClick(Sender: TObject);
begin
  DblClick;
end;

procedure TEvCalendar.DoClick(Sender: TObject);
begin
  Click;
end;

procedure TEvCalendar.YearButtonClick(Sender: TObject);
begin
  if Sender = FYear_Plus then
    Inc(FYear)
  else
    Dec(FYear);

  RepaintCalendar;
  Click;
end;

procedure TEvCalendar.SetSelectionStart(AValue: TDateTime);
begin
  if AValue <> FSelectionStart then
  begin
    FSelectionStart := AValue;

    if (FSelectionStart > FSelectionEnd) or
      (( not (csSelectionEnabled in FOptions)) and
      (FSelectionStart <> FSelectionEnd)) then
      SetSelectionEnd(FSelectionStart);

    _SetDate(AValue);

    RepaintCalendar;
  end;
end;

procedure TEvCalendar.SetSelectionEnd(AValue: TDateTime);
begin
  if AValue <> FSelectionEnd then
  begin
    FSelectionEnd := AValue;

    if (FSelectionEnd < FSelectionStart) or
      (( not (csSelectionEnabled in FOptions)) and
      (FSelectionStart <> FSelectionEnd)) then
      SetSelectionStart(FSelectionEnd);

    RepaintCalendar;
  end;
end;

procedure TEvCalendar.DoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var
  _Year:     word;
  PanelDate: TDateTime;
  APanel:    TCalendarPanel;
begin
  APanel := (Sender as TCalendarPanel);

  // don't react click on the prior and next month dates
  if APanel.Tag <> FMonth then
    exit;

  if (APanel.Tag = 12) and (FMonth = 1) then
    _Year := FYear - 1
  else if (APanel.Tag = 1) and (FMonth = 12) then
    _Year := FYear + 1
  else
    _Year := FYear;

  PanelDate := Trunc(EncodeDate(_Year, APanel.Tag, StrToInt(APanel.Caption)));

  if ssLeft in Shift then
    SetSelectionStart(PanelDate + Frac(FSelectionStart));

  if ssRight in Shift then
    if (csSelectionEnabled in FOptions) then
    begin
      SetSelectionEnd(PanelDate + Frac(FSelectionEnd));
    end;
end;

procedure TEvCalendar.SetDateFormat(AValue: string);
begin
  if FDateFormat <> AValue then
  begin
    FDateFormat := AValue;
    RepaintCalendar;
  end;
end;


procedure TEvPayrollDatesGadget.pnlContentResize(Sender: TObject);
begin
  inherited;
  OnResize;
end;

end.
