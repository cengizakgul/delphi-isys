unit EvShortcutsGadgetFrm;

interface

uses
  Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, ISBasicClasses,
  EvUIComponents, EvContext, isBaseClasses, EvConsts;

type
  TEvShortcutsGadget = class(TEvDashboardGadget)
    bNewEmployee: TevSpeedButton;
    bNewPayroll: TevSpeedButton;
    btnWebBrowser: TevSpeedButton;
    procedure bNewEmployeeClick(Sender: TObject);
    procedure bNewPayrollClick(Sender: TObject);
    procedure btnWebBrowserClick(Sender: TObject);
  private
    FCl_Nbr: Integer;
    FCo_Nbr: Integer;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  public
    procedure AfterConstruction; override;    
  end;


implementation

uses EvUIUtils;

{$R *.DFM}

procedure TEvShortcutsGadget.AfterConstruction;
begin
  inherited;
  CompanyDrivenGadget := True;
end;

procedure TEvShortcutsGadget.bNewEmployeeClick(Sender: TObject);
begin
  ActivateFrameAs('Employee - Employee', [FCl_Nbr, FCo_Nbr], 3)
end;

procedure TEvShortcutsGadget.bNewPayrollClick(Sender: TObject);
begin
  ActivateFrameAs('Payroll - Payroll', [FCl_Nbr, FCo_Nbr], 1)
end;

procedure TEvShortcutsGadget.PrepareData(const AInputParams, AData: IisListOfValues);
begin
  inherited;
  AData.AddValue('Cl_Nbr', AInputParams.Value['Cl_Nbr']);
  AData.AddValue('Co_Nbr', AInputParams.Value['Co_Nbr']);
end;

procedure TEvShortcutsGadget.PresentData(const AData: IisListOfValues);
begin
  inherited;
  FCl_Nbr := AData.Value['Cl_Nbr'];
  FCo_Nbr := AData.Value['Co_Nbr'];

  // TODO: Probably we will have to add some extra logic for security and/or company setup
  // It would allow usa to disable the buttons when it does not make sense to click it

  btnWebBrowser.Visible := ctx_AccountRights.GetElementState(etScreen, 'TEVWEBBROWSERFRM') = stEnabled;
end;

procedure TEvShortcutsGadget.btnWebBrowserClick(Sender: TObject);
begin
  ActivateFrameAs('Misc - Web Browser', []);
end;

end.
