unit EvFavoriteReportsGadgetFrm;

interface

uses
  Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm, DB,
  Wwdatsrc, ISBasicClasses, EvUIComponents, EvCommonInterfaces, StdCtrls,
  DBCtrls, dbcgrids, isBaseClasses, Forms, Windows;

type
  TEvFavoriteReportsGadget = class(TEvDashboardGadget)
    dsrReports: TevDataSource;
    ScrollBox1: TScrollBox;
    grReports: TDBCtrlGrid;
    dbtDescription: TevDBText;
    procedure grReportsDblClick(Sender: TObject);
    procedure dbtDescriptionClick(Sender: TObject);
  private
    FFavoriteReports: IevDataSet;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  public
    procedure AfterConstruction; override;  
  end;


implementation

uses sDataStructure, isDataSet, EvDataset, EvContext, EvUIUtils,
  SPD_CO_FAVORITE_REPORTS, SysUtils;

{$R *.DFM}

{ TEvFavoriteReportsGadget }

procedure TEvFavoriteReportsGadget.PrepareData(const AInputParams, AData: IisListOfValues);
const
  ResStruct: array [0..3] of TDSFieldDef = (
    (FieldName: 'Nbr';  DataType: ftInteger;  Size: 0;  Required: True),
    (FieldName: 'Description';  DataType: ftString;  Size: 128;  Required: True),
    (FieldName: 'CoNbr'; DataType: ftInteger;  Size: 0;  Required: True),
    (FieldName: 'ClNbr'; DataType: ftInteger;  Size: 0;  Required: True));
var
  ResultDS: IevDataSet;
  Q: IevQuery;

  CoNbr, ClNbr: integer;
begin
  inherited;

  ResultDS := TevDataset.Create(ResStruct);
  ResultDS.LogChanges := False;

  ClNbr := AInputParams.TryGetValue('Cl_Nbr', 0);
  CoNbr := AInputParams.TryGetValue('Co_Nbr', 0);
  ctx_DataAccess.OpenClient( ClNbr );

  Q := TevQuery.Create('Select UR.CO_USER_REPORTS_NBR, '+
                       ' CR.CUSTOM_NAME || ''('' || CR.REPORT_LEVEL || cast(CR.REPORT_WRITER_REPORTS_NBR as varchar(20))  || '')'' NDescription ' +
                       ' From CO_REPORTS CR, CO_USER_REPORTS UR '+
                       ' where CR.CO_REPORTS_NBR = UR.CO_REPORTS_NBR '+
                       ' and {AsOfNow<CR>} and {AsOfNow<UR>} '+
                       ' and CR.CO_NBR = UR.CO_NBR '+
                       ' and CR.CO_NBR = :CONBR '+
                       ' and UR.SB_USER_NBR = :SBUSERNBR' );
  Q.Params.AddValue('CONBR', CoNbr);
  Q.Params.AddValue('SBUSERNBR', Context.UserAccount.InternalNbr);
  Q.Execute;

  while not Q.Result.Eof do
  begin
    ResultDS.AppendRecord([Q.Result.Fields[0].AsInteger, Q.Result.Fields[1].AsString, CoNbr, ClNbr]);
    Q.Result.Next;
  end;

  ResultDS.IndexFieldNames := 'Description';
  AData.AddValue('FavoriteReports', ResultDS);
end;

procedure TEvFavoriteReportsGadget.PresentData(
  const AData: IisListOfValues);
var
  DS: IevDataSet;
begin
  inherited;

  DS := IInterface(AData.Value['FavoriteReports']) as IevDataSet;
  DS.First;
  if Assigned(FFavoriteReports) then
    if not DS.Locate('Nbr', FFavoriteReports.FieldByName('Nbr').AsInteger, []) then
      DS.First;

  dsrReports.DataSet := nil;

  dbtDescription.Left := 6;
  dbtDescription.Width := grReports.PanelWidth - dbtDescription.Left * 2;

  FFavoriteReports := DS;
  if FFavoriteReports.RecordCount > 0 then
    dsrReports.DataSet := FFavoriteReports.vclDataSet;
  ShowScrollBar(grReports.Handle, SB_VERT, True);

  if grReports.CanFocus then
    grReports.SetFocus;
end;

procedure TEvFavoriteReportsGadget.AfterConstruction;
begin
  inherited;
  CompanyDrivenGadget := True;
end;

procedure TEvFavoriteReportsGadget.grReportsDblClick(Sender: TObject);
begin
  if FFavoriteReports.FieldByName('Nbr').AsInteger > 0 then
  begin
    ctx_DataAccess.OpenClient( FFavoriteReports.FieldByName('ClNbr').AsInteger );
    DM_COMPANY.CO.DataRequired('CO_NBR=' + FFavoriteReports.FieldByName('CoNbr').AsString);

    TCO_FavoriteReports.execute(
      FFavoriteReports.FieldByName('CoNbr').AsString,
      DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString,
      FFavoriteReports.FieldByName('Nbr').AsInteger
    );
  end;
end;

procedure TEvFavoriteReportsGadget.dbtDescriptionClick(Sender: TObject);
begin
  grReports.SetFocus;
end;

end.
