unit EvDashboardGadgetFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, isUIFashionPanel, StdCtrls, ISBasicClasses, isBasicUtils,
  EvUIComponents, EvMainboard, isMessenger, evContext, evCommonInterfaces,
  isThreadManager, isBaseClasses, GIFImage, evStreamUtils, LMDCustomButton,
  LMDButton, isUILMDButton, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, pngimage;

type
  TEvDashboardGadget = class(TFrame, IisMessageRecipient)
    pnlContent: TisUIFashionPanel;
    procedure pnlContentResize(Sender: TObject);
    procedure pnlContentDblClick(Sender: TObject);
  private
    FDestroying: Boolean;
    FFrozen: Boolean;
    FGUIContext: IevContext;
    FPageButtons: TList;
    FActivePageControl: TControl;
    FPagingTimer: TTimer;
    FRefreshTimer: TTimer;
    FRefreshTaskID: TTaskID;
    FPagingTimerEnabled: Boolean;
    FPleaseWaitPanel: TevPanel;
    FPleaseWaitImage: TevImage;
    FLastRefreshParams: IisListOfValues;
    FQueuedRefreshParams: IisListOfValues;
    FCompanyDrivenGadget: Boolean;
    procedure OnPageButtonClick(Sender: TObject);
    procedure OnPagingTimer(Sender: TObject);
    procedure OnRefreshTimer(Sender: TObject);
    procedure SetPageCount(const AValue: Integer);
    function  GetPageCount: Integer;
    procedure AlignPageButtons;
    function  GetActivePageIndex: Integer;
    procedure SetActivePageIndex(const AValue: Integer);
    procedure SetActivePageControl(const AValue: TControl);
    procedure BuildDefaultPages;
    function  GetPagingTimerInterval: Integer;
    procedure SetPagingTimerInterval(const AValue: Integer);
    procedure Thread_Refresh(const Params: TTaskParamList);
    function  GetPagingTimerEnabled: Boolean;
    procedure SetPagingTimerEnabled(const AValue: Boolean);
    procedure PleaseWaitPanelResize(Sender: TObject);
    function  GetRefreshTimerInterval: Integer;
    procedure SetRefreshTimerInterval(const AValue: Integer);
  protected
    procedure Notify(const AMessage: String; const AParams: IisListOfValues); virtual; // Can be used for custom messaging. See isMessenger.pas for details
    function  Destroying: Boolean;

    procedure PrepareData(const AInputParams, AData: IisListOfValues); virtual; // STEP1: A background thread context. Override it for data preparation. (no GUI acceess from here!).
    procedure PresentData(const AData: IisListOfValues); virtual; // STEP2: Main thread context. Override it for data presentation. (can access GUI form here).
    procedure DoOnChangePage; virtual;                            // Override for custom action if page is changed. Keep inhereted logic for default behaviour.
    procedure DoOnPagingTimer; virtual;                           // Override for custom action needed on paging timer event. Keep inhereted logic for default behaviour.
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   Refresh;
    function    InProgress: Boolean;

    property PageCount: Integer read GetPageCount write SetPageCount;
    property ActivePageIndex: Integer read GetActivePageIndex write SetActivePageIndex;
    property ActivePageControl: TControl read FActivePageControl write SetActivePageControl;
    property PagingTimerInterval: Integer read GetPagingTimerInterval write SetPagingTimerInterval;
    property PagingTimerEnabled: Boolean read GetPagingTimerEnabled write SetPagingTimerEnabled;
    property RefreshTimerInterval: Integer read GetRefreshTimerInterval write SetRefreshTimerInterval;
    property CompanyDrivenGadget: Boolean read FCompanyDrivenGadget write FCompanyDrivenGadget;
    property Frozen: boolean read FFrozen write FFrozen;
  end;

implementation

uses Types;

{$R *.dfm}
{$R EvDashboard.res}

{ TEvDashboardGadget }

constructor TEvDashboardGadget.Create(AOwner: TComponent);
var
  S: IisStream;
  f: String;
begin
  inherited;
  FGUIContext := Context;
  FPageButtons := TList.Create;
  BuildDefaultPages;
  Mainboard.Messenger.Subscribe(Self, ['DashboardGadget.Refresh'], True);
  Mainboard.Messenger.Subscribe(Self, ['DashboardGadget.PresentData'], True);
  Mainboard.Messenger.Subscribe(Self, ['Dashboard.ChangeCompany'], True);

  Mainboard.Messenger.Subscribe(Self, ['DashboardGadget.Unfreeze'], True);
  Mainboard.Messenger.Subscribe(Self, ['DashboardGadget.Freeze'], True);

  FPleaseWaitImage := TevImage.Create(Self);
  FPleaseWaitImage.AutoSize := True;
  FPleaseWaitImage.Transparent := True;
  FPleaseWaitImage.Visible := True;

  FPleaseWaitPanel := TevPanel.Create(Self);  
  FPleaseWaitPanel.Align := alClient;
  FPleaseWaitPanel.BevelInner := bvNone;
  FPleaseWaitPanel.BevelOuter := bvNone;
  FPleaseWaitPanel.ParentColor := True;
  FPleaseWaitPanel.Visible := False;
  FPleaseWaitPanel.OnResize := PleaseWaitPanelResize;
  FPleaseWaitPanel.Parent := pnlContent;  

  FPleaseWaitImage.Parent := FPleaseWaitPanel;

  S := GetBinaryResource('progress_animation', 'GIF');
  f := AppTempFolder + GetUniqueID + '.gif';
  S.SaveToFile(f);
  try
    FPleaseWaitImage.Picture.LoadFromFile(f);
  finally
    DeleteFile(f);
  end;

  Refresh;
end;

destructor TEvDashboardGadget.Destroy;
var
  T: TTaskID;
begin
  FDestroying := True;
  Mainboard.Messenger.Unsubscribe(Self, []);

  InterlockedExchange(Integer(T), Integer(FRefreshTaskID));
  if T <> 0 then
  begin
    GlobalThreadManager.TerminateTask(T);
    GlobalThreadManager.WaitForTaskEnd(T);
  end;

  PagingTimerInterval := 0;   // Destroy paging timer
  RefreshTimerInterval := 0;  // Destroy refresh timer
  PageCount := 0;             // Destroy paging buttons
  FPageButtons.Free;

  FGUIContext := nil;
  inherited;
end;

function TEvDashboardGadget.GetPageCount: Integer;
begin
  Result := FPageButtons.Count;
end;

procedure TEvDashboardGadget.SetPageCount(const AValue: Integer);
var
  i: Integer;
  Btn: TevRadioButton;
begin
  if AValue = PageCount then
    Exit;

  for i := 0 to FPageButtons.Count - 1 do
    TevRadioButton(FPageButtons[i]).Free;

  FPageButtons.Clear;

  for i := 1 to AValue do
  begin
    Btn := TevRadioButton.Create(nil);
    Btn.Caption := '';
    Btn.OnClick := OnPageButtonClick;
    Btn.Parent := pnlContent;
    Btn.Width := Btn.Height;

    FPageButtons.Add(Btn);
  end;

  ActivePageIndex := 0;

  AlignPageButtons;
end;

procedure TEvDashboardGadget.pnlContentResize(Sender: TObject);
begin
  AlignPageButtons;
end;

procedure TEvDashboardGadget.AlignPageButtons;
var
  i, x: Integer;
  Btn: TevRadioButton;
begin
  x := pnlContent.ClientWidth - pnlContent.Margins.Right - pnlContent.ShadowDepth - 4;

  for i := PageCount - 1 downto 0 do
  begin
    Btn := TevRadioButton(FPageButtons[i]);
    Btn.Color := pnlContent.TitleColor;
    Btn.Top := pnlContent.Margins.Top + pnlContent.TitleHight div 2 - Btn.Height div 2;
    Btn.Left := x - Btn.Width;
    x := x - Btn.Width - 1;
  end;
end;

function TEvDashboardGadget.GetActivePageIndex: Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to PageCount - 1 do
    if TevRadioButton(FPageButtons[i]).Checked then
    begin
      Result := i;
      Break;
    end;
end;

procedure TEvDashboardGadget.OnPageButtonClick(Sender: TObject);
begin
  DoOnChangePage;
end;

procedure TEvDashboardGadget.DoOnChangePage;
var
  i: Integer;
  s: String;
begin
  // Any TControl which named according to the convention PAGE# is considered as a page for default binding.
  // e.g. Page0, Page1

  s := 'Page' + IntToStr(ActivePageIndex);
  for i := 0 to pnlContent.ControlCount - 1 do
    if AnsiSameText(pnlContent.Controls[i].Name, s) then
      ActivePageControl := pnlContent.Controls[i]
    else if StartsWith(pnlContent.Controls[i].Name, 'Page') then
    begin
      if ActivePageControl = pnlContent.Controls[i] then
        ActivePageControl := nil
      else
        pnlContent.Controls[i].Hide;
    end;
end;

procedure TEvDashboardGadget.SetActivePageIndex(const AValue: Integer);
begin
  if PageCount > 0 then
    if AValue >= PageCount then
      TevRadioButton(FPageButtons[PageCount - 1]).Checked := True

    else if AValue < 0 then
    begin
      if ActivePageIndex >= 0 then
      begin
        TevRadioButton(FPageButtons[ActivePageIndex]).Checked := False;
        DoOnChangePage;
      end;
    end

    else
      TevRadioButton(FPageButtons[AValue]).Checked := True;
end;

procedure TEvDashboardGadget.SetActivePageControl(const AValue: TControl);
begin
  if AValue <> FActivePageControl then
  begin
    if Assigned(FActivePageControl) then
      FActivePageControl.Hide;
    FActivePageControl := AValue;
    if Assigned(FActivePageControl) then
      FActivePageControl.Show;
  end;
end;

procedure TEvDashboardGadget.BuildDefaultPages;
var
  i, j: Integer;
begin
  j := 0;
  for i := 0 to pnlContent.ControlCount - 1 do
    if StartsWith(pnlContent.Controls[i].Name, 'Page') then
      Inc(j);

  PageCount := j;    
end;

function TEvDashboardGadget.GetPagingTimerInterval: Integer;
begin
  if Assigned(FPagingTimer) then
    Result := FPagingTimer.Interval
  else
    Result := 0;
end;

procedure TEvDashboardGadget.SetPagingTimerInterval(const AValue: Integer);
begin
  if AValue <= 0 then
    FreeAndNil(FPagingTimer)
  else
  begin
    if not Assigned(FPagingTimer) then
    begin
      FPagingTimer := TTimer.Create(nil);
      FPagingTimer.OnTimer := OnPagingTimer;
    end;
    FPagingTimer.Interval := AValue;
    FPagingTimer.Enabled := FPagingTimerEnabled;
  end;
end;

procedure TEvDashboardGadget.OnPagingTimer(Sender: TObject);
begin
  DoOnPagingTimer;
end;

procedure TEvDashboardGadget.DoOnPagingTimer;
begin
  if ActivePageIndex < PageCount - 1 then
    ActivePageIndex := ActivePageIndex + 1
  else
    ActivePageIndex := 0;
end;

procedure TEvDashboardGadget.PresentData(const AData: IisListOfValues);
begin
end;

function TEvDashboardGadget.Destroying: Boolean;
begin
  Result := FDestroying or (csDestroying in ComponentState);
end;

procedure TEvDashboardGadget.Notify(const AMessage: String; const AParams: IisListOfValues);
begin
  if CompanyDrivenGadget and AnsiSameText(AMessage, 'Dashboard.ChangeCompany') then
    Mainboard.Messenger.PostMessage(Self, 'DashboardGadget.Refresh', AParams)

  else if AnsiSameText(AMessage, 'DashboardGadget.Refresh') and not Frozen then
  begin
    if not CompanyDrivenGadget or (Assigned(AParams) and AParams.ValueExists('Cl_Nbr') and AParams.ValueExists('Co_Nbr')) then
      if not InProgress then
      begin
        FQueuedRefreshParams := nil;
        pnlContent.Enabled := False;
        PagingTimerEnabled := False;
        FPleaseWaitPanel.Show;
        FPleaseWaitPanel.BringToFront;
        ActivePageIndex := -1;
        ActivePageControl := nil;
        FRefreshTaskID := FGUIContext.RunParallelTask(Thread_Refresh, Self, MakeTaskParams([AParams]), ClassName + '.Thread_Refresh')
      end
      else
        FQueuedRefreshParams := AParams;
  end

  else if AnsiSameText(AMessage, 'DashboardGadget.PresentData') then
  begin
    try
      pnlContent.Enabled := True; // Need to enable here to allow SetFocus
      PresentData(AParams);
    finally
      FPleaseWaitPanel.Hide;
      PagingTimerEnabled := True;
      ActivePageIndex := 0;
    end;

    if Assigned(FQueuedRefreshParams) then
    begin
      Mainboard.Messenger.PostMessage(Self, 'DashboardGadget.Refresh', FQueuedRefreshParams);
      FQueuedRefreshParams := nil;      
    end;
  end
  else if AnsiSameText(AMessage, 'DashboardGadget.Freeze') then
    Frozen := True
  else if AnsiSameText(AMessage, 'DashboardGadget.Unfreeze') then
    Frozen := False;
end;

procedure TEvDashboardGadget.PrepareData(const AInputParams, AData: IisListOfValues);
begin
end;

procedure TEvDashboardGadget.Thread_Refresh(const Params: TTaskParamList);
var
  InputParams, Data: IisListOfValues;
begin
  try
    if Length(Params) > 0 then
      InputParams := IInterface(Params[0]) as IisListOfValues;

    Data := TisListOfValues.Create;
    PrepareData(InputParams, Data);
    FLastRefreshParams := InputParams;
    Mainboard.Messenger.PostMessage(Self, 'DashboardGadget.PresentData', Data);
  finally
    InterlockedExchange(Integer(FRefreshTaskID), 0);
  end;
end;

function TEvDashboardGadget.InProgress: Boolean;
begin
  Result := FRefreshTaskID <> 0;
end;

function TEvDashboardGadget.GetPagingTimerEnabled: Boolean;
begin
  Result := FPagingTimerEnabled;
end;

procedure TEvDashboardGadget.SetPagingTimerEnabled(const AValue: Boolean);
begin
  FPagingTimerEnabled := AValue;
  if Assigned(FPagingTimer) then
    FPagingTimer.Enabled := FPagingTimerEnabled;
end;

procedure TEvDashboardGadget.pnlContentDblClick(Sender: TObject);
begin
  Refresh;
end;

procedure TEvDashboardGadget.Refresh;
begin
  Mainboard.Messenger.PostMessage(Self, 'DashboardGadget.Refresh', FLastRefreshParams);
end;

procedure TEvDashboardGadget.PleaseWaitPanelResize(Sender: TObject);
begin
  FPleaseWaitImage.Top := FPleaseWaitPanel.ClientHeight div 2 - FPleaseWaitImage.Height div 2;
  FPleaseWaitImage.Left := FPleaseWaitPanel.ClientWidth div 2 - FPleaseWaitImage.Width div 2;
end;

function TEvDashboardGadget.GetRefreshTimerInterval: Integer;
begin
  if Assigned(FRefreshTimer) then
    Result := FRefreshTimer.Interval
  else
    Result := 0;
end;

procedure TEvDashboardGadget.SetRefreshTimerInterval(const AValue: Integer);
begin
  if AValue <= 0 then
    FreeAndNil(FRefreshTimer)
  else
  begin
    if not Assigned(FRefreshTimer) then
    begin
      FRefreshTimer := TTimer.Create(nil);
      FRefreshTimer.OnTimer := OnRefreshTimer;
    end;
    FRefreshTimer.Interval := AValue;
    FRefreshTimer.Enabled := True;
  end;
end;

procedure TEvDashboardGadget.OnRefreshTimer(Sender: TObject);
begin
  Refresh;
end;

end.

