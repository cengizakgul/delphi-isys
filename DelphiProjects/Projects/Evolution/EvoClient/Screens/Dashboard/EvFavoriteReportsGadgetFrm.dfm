inherited EvFavoriteReportsGadget: TEvFavoriteReportsGadget
  Height = 249
  inherited pnlContent: TisUIFashionPanel
    Height = 249
    Title = 'Favorite Reports'
    object ScrollBox1: TScrollBox
      Left = 12
      Top = 36
      Width = 263
      Height = 193
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alClient
      TabOrder = 0
      object grReports: TDBCtrlGrid
        Left = 0
        Top = 0
        Width = 259
        Height = 189
        Align = alClient
        AllowDelete = False
        AllowInsert = False
        Color = 15790320
        DataSource = dsrReports
        PanelHeight = 27
        PanelWidth = 242
        ParentColor = False
        TabOrder = 0
        RowCount = 7
        SelectedColor = 15720905
        OnDblClick = grReportsDblClick
        object dbtDescription: TevDBText
          Left = 0
          Top = 4
          Width = 246
          Height = 19
          Anchors = [akLeft, akTop, akRight, akBottom]
          BiDiMode = bdLeftToRight
          DataField = 'Description'
          DataSource = dsrReports
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBiDiMode = False
          ParentFont = False
          Transparent = True
          OnClick = dbtDescriptionClick
          OnDblClick = grReportsDblClick
        end
      end
    end
  end
  object dsrReports: TevDataSource
    Left = 13
    Top = 32
  end
end
