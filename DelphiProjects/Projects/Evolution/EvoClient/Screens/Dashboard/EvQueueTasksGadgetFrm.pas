unit EvQueueTasksGadgetFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, EvDashboardGadgetFrm, Controls,
  StdCtrls, DBCtrls, ISBasicClasses, EvUIComponents, DB, Wwdatsrc, Graphics,
  dbcgrids, ExtCtrls, isUIFashionPanel, EvUtils,
  evMainboard, evCommonInterfaces, isBaseClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  EvClientDataSet, isDataSet, EvDataSet, EvBasicUtils, EvContext, GIFImage,
  isUIDBImage, Forms;

type
  TEvQueueTasksGadget = class(TEvDashboardGadget)
    dsrTasks: TevDataSource;
    ScrollBox1: TScrollBox;
    grTasks: TDBCtrlGrid;
    dbtStatus: TevDBText;
    dbtTime: TevDBText;
    dbtUser: TevDBText;
    dbtTaskType: TevDBText;
    imgNew: TevImage;
    dbtTitle: TevDBText;
    procedure pnlContentResize(Sender: TObject);
    procedure grTasksPaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure grTasksDblClick(Sender: TObject);
    procedure dbtTaskTypeClick(Sender: TObject);
  private
    FTasks: IevDataSet;
    function ShortenString(const aStr: string; aMaxLength: integer): string;
  protected
    procedure Notify(const AMessage: String; const AParams: IisListOfValues); override;
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


implementation

uses SQueueViewForm;

{$R *.DFM}

{ TEvQueueTasksGadget }

procedure TEvQueueTasksGadget.PrepareData(const AInputParams, AData: IisListOfValues);
const
  ResStruct: array [0..16] of TDSFieldDef = (
    (FieldName: 'Id';  DataType: ftString;  Size: 32;  Required: False),
    (FieldName: 'Nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'TaskType';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'Caption';  DataType: ftString;  Size: 128;  Required: False),
    (FieldName: 'Status';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'Finished';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'Seen';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'TimeStamp';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'Message';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'UserName';  DataType: ftString;  Size: 128;  Required: False),
    (FieldName: 'NewTask';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'WaitForResTask';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'FinishedSucs';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'ExecTask';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'FinishedExcep';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'FinishedWarn';  DataType: ftBoolean;  Size: 0;  Required: False),
    (FieldName: 'FailedExcep';  DataType: ftBoolean;  Size: 0;  Required: False));

var
  TaskInf: IevTaskInfo;

  function GetTaskState: String;
  begin
    case TaskInf.State of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := TaskInf.ProgressText;
    end;
  end;

  function GetTaskProgressText: String;
  begin
    if TaskInf.State = tsFinished then
      Result := ''
    else
      Result := TaskInf.ProgressText;
  end;

  function GetStatFieldValue(fn:string):variant;
  begin
    result := false;
    if fn = 'NewTask' then
    begin
       result := Iff(TaskInf.State = tsInactive,true,false);
    end else
    if fn ='WaitForResTask' then
    begin
       result := Iff(TaskInf.State = tsWaiting,true,false);
    end else
    if fn = 'ExecTask' then
    begin
       result := Iff(TaskInf.State = tsExecuting,true,false);
    end else
    if fn = 'FinishedSucs' then
    begin
       result := Iff((TaskInf.State = tsFinished) and (pos('SUCCESS',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn ='FinishedExcep' then
    begin
       result := Iff((TaskInf.State = tsFinished) and (pos('EXCEPTION',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn ='FinishedWarn' then
    begin
      result := Iff((TaskInf.State = tsFinished) and (pos('WARNING',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn = 'FailedExcep' then
    begin
    end;
  end;

var
  TaskList: IisListOfValues;
  i: Integer;
  ResultDS: IevDataSet;
  sUser: String;
begin
  inherited;

  // Ask for task list
  TaskList := mb_TaskQueue.GetUserTaskInfoList;

  ResultDS := TevDataset.Create(ResStruct);
  ResultDS.LogChanges := False;

  // Fill up dataset
  sUser := Context.UserAccount.User;
  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    with TaskInf do
      if (State = tsFinished) and AnsiSameText(User, sUser) then
        ResultDS.AppendRecord([ID, Nbr, TaskType, ShortenString(Caption, 64), GetTaskState, State = tsFinished,
          SeenByUser, LastUpdate, GetTaskProgressText, User,
          GetStatFieldValue('NewTask'),
          GetStatFieldValue('WaitForResTask'),
          GetStatFieldValue('ExecTask'),
          GetStatFieldValue('FinishedSucs'),
          GetStatFieldValue('FinishedExcep'),
          GetStatFieldValue('FinishedWarn'),
          GetStatFieldValue('FailedExcep')]);
  end;

  ResultDS.IndexFieldNames := 'TimeStamp:D';

  AData.AddValue('Tasks', ResultDS);
end;

procedure TEvQueueTasksGadget.PresentData(const AData: IisListOfValues);
var
  DS: IevDataSet;
begin
  inherited;

  DS := IInterface(AData.Value['Tasks']) as IevDataSet;
  DS.First;
  if Assigned(FTasks) then
    if not DS.Locate('ID', FTasks.FieldByName('ID').AsString, []) then
      DS.First;

  dsrTasks.DataSet := nil;

  dbtTaskType.Left := 6;
  dbtTitle.Left := 6;
  dbtStatus.Left := 6;
  dbtUser.Left := 6;

  dbtTaskType.Width := grTasks.PanelWidth - dbtTaskType.Left * 2;
  dbtTitle.Width := grTasks.PanelWidth - dbtTitle.Left * 2;
  dbtStatus.Width := grTasks.PanelWidth - dbtStatus.Left * 2;
  dbtTime.Left := dbtUser.BoundsRect.Left;
  dbtTime.Width := grTasks.PanelWidth - dbtUser.Left - dbtTime.Left;
  imgNew.Left := grTasks.PanelWidth - imgNew.Width - dbtTaskType.Left;

  FTasks := DS;
  if FTasks.RecordCount > 0 then
    dsrTasks.DataSet := FTasks.vclDataSet;
  ShowScrollBar(grTasks.Handle, SB_VERT, True);

  if grTasks.CanFocus then
    grTasks.SetFocus;
end;

procedure TEvQueueTasksGadget.pnlContentResize(Sender: TObject);
begin
  inherited;
  grTasks.RowCount := grTasks.ClientHeight div 79;
end;

procedure TEvQueueTasksGadget.grTasksPaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  if FTasks.FieldByName('FinishedExcep').AsBoolean then
    dbtStatus.Font.Color := clRed
  else if FTasks.FieldByName('FinishedWarn').AsBoolean then
    dbtStatus.Font.Color := clBlue
  else if FTasks.FieldByName('ExecTask').AsBoolean then
    dbtStatus.Font.Color := clGreen;

  imgNew.Visible := not FTasks.FieldByName('Seen').AsBoolean;
end;

procedure TEvQueueTasksGadget.grTasksDblClick(Sender: TObject);
begin
  with TTQueueViewForm.Create(nil) do
    try
      Show(FTasks.FieldByName('ID').AsString);
    finally
      Free;
    end;
end;

procedure TEvQueueTasksGadget.Notify(const AMessage: String; const AParams: IisListOfValues);
begin
  if AnsiSameStr(AMessage, 'GlobalCallbacks.TaskQueueEvent') then
  begin
    if (TevTaskQueueEvent(AParams.Value['Event']) = tqeTaskListChange) and
       AnsiSameText(AParams.Value['UserAtDomain'], EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain))
    then
      Refresh;
  end
  else
    inherited;
end;

constructor TEvQueueTasksGadget.Create(AOwner: TComponent);
begin
  inherited;
  Mainboard.Messenger.Subscribe(Self, ['GlobalCallbacks.TaskQueueEvent'], False);
end;

procedure TEvQueueTasksGadget.dbtTaskTypeClick(Sender: TObject);
begin
  grTasks.SetFocus;
end;

function TEvQueueTasksGadget.ShortenString(const aStr: string;
  aMaxLength: integer): string;
begin
  Result := aStr;
  if Length(Result) > aMaxLength then
    Result := Copy(Result, 1, aMaxLength - 3) + '...';
end;

end.
