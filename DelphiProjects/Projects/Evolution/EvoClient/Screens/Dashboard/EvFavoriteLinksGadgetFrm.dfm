inherited EvFavoriteLinksGadget: TEvFavoriteLinksGadget
  Width = 253
  Height = 254
  ParentColor = False
  inherited pnlContent: TisUIFashionPanel
    Width = 253
    Height = 254
    OnCanResize = pnlContentCanResize
    Title = 'Favorite Links'
    object ScrollBox1: TScrollBox
      Left = 12
      Top = 36
      Width = 221
      Height = 198
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 0
      object grLinks: TDBCtrlGrid
        Left = 0
        Top = 0
        Width = 217
        Height = 194
        Align = alClient
        AllowDelete = False
        AllowInsert = False
        Color = 15790320
        DataSource = dsrLinks
        PanelHeight = 48
        ParentColor = False
        TabOrder = 0
        RowCount = 4
        SelectedColor = 15720905
        OnDblClick = dbtPathClick
        object dbtPath: TevDBText
          Left = 41
          Top = 24
          Width = 151
          Height = 14
          Cursor = crHandPoint
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Path'
          DataSource = dsrLinks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          Transparent = True
          OnClick = dbtPathClick
          OnDblClick = dbtPathClick
        end
        object dbtName: TevDBText
          Left = 41
          Top = 5
          Width = 140
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Name'
          DataSource = dsrLinks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          OnClick = imgPictClick
          OnDblClick = dbtPathClick
        end
        object imgPict: TISDBImage
          Left = 5
          Top = 8
          Width = 29
          Height = 29
          BorderStyle = bsNone
          Color = 15790320
          Ctl3D = False
          DataField = 'Picture'
          DataSource = dsrLinks
          ParentCtl3D = False
          Stretch = True
          TabOrder = 0
          OnClick = imgPictClick
          OnDblClick = dbtPathClick
        end
      end
    end
  end
  object dsrLinks: TevDataSource
    Left = 164
    Top = 100
  end
end
