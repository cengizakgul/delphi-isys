unit EvCompanyUpdatesGadgetFrm;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  EvDataSet, EvContext, EvCommonInterfaces, EvConsts, isBaseClasses, StdCtrls,
  ISBasicClasses, EvUIComponents, EvMainboard;

type
  TEvCompanyUpdatesGadget = class(TEvDashboardGadget)
    memMessage: TevMemo;
  private
    FMessages: IisListOfValues;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
    procedure DoOnChangePage; override;
  public
    procedure AfterConstruction; override;
  end;


implementation


{$R *.DFM}

{ TEvCompanyUpdatesGadget }

procedure TEvCompanyUpdatesGadget.AfterConstruction;
begin
  inherited;
  PagingTimerInterval := 30000; // 30 sec
  RefreshTimerInterval := 300000; // 5 min
end;

procedure TEvCompanyUpdatesGadget.DoOnChangePage;
begin
  inherited;
  if ActivePageIndex = -1 then
    memMessage.Text := ''
  else
    memMessage.Text := FMessages.Values[ActivePageIndex].Value;
end;

procedure TEvCompanyUpdatesGadget.PrepareData(const AInputParams, AData: IisListOfValues);
var
  Res: IisListOfValues;
begin
  inherited;
  Res := Mainboard.DashboardDataProvider.GetCompanyMessages;
  AData.AddValue('Messages', Res);
end;

procedure TEvCompanyUpdatesGadget.PresentData(const AData: IisListOfValues);
begin
  inherited;
  FMessages := IInterface(AData.Value['Messages']) as IisListOfValues;
  PageCount := FMessages.Count;
end;

end.
