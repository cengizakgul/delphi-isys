inherited EvPendingItemsGadget: TEvPendingItemsGadget
  Width = 315
  Height = 394
  inherited pnlContent: TisUIFashionPanel
    Width = 315
    Height = 394
    Title = 'Pending Items'
    object ScrollBox1: TScrollBox
      Left = 12
      Top = 36
      Width = 283
      Height = 338
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alClient
      TabOrder = 0
      object grTasks: TDBCtrlGrid
        Left = 0
        Top = 0
        Width = 279
        Height = 334
        Align = alClient
        AllowDelete = False
        AllowInsert = False
        Color = 13434879
        DataSource = dsrTasks
        PanelHeight = 83
        PanelWidth = 262
        ParentColor = False
        TabOrder = 0
        RowCount = 4
        SelectedColor = 11133413
        OnDblClick = grTasksDblClick
        OnPaintPanel = grTasksPaintPanel
        object dbtNotes: TevDBText
          Left = 4
          Top = 28
          Width = 253
          Height = 26
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Notes'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
          WordWrap = True
          OnClick = dbtSubjectClick
          OnDblClick = grTasksDblClick
        end
        object dbtDue: TevDBText
          Left = 4
          Top = 58
          Width = 178
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Due'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
          OnClick = dbtSubjectClick
          OnDblClick = grTasksDblClick
        end
        object dbtSubject: TevDBText
          Left = 4
          Top = 2
          Width = 251
          Height = 25
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Subject'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          WordWrap = True
          OnClick = dbtSubjectClick
          OnDblClick = grTasksDblClick
        end
      end
    end
  end
  object dsrTasks: TevDataSource
    Left = 183
    Top = 70
  end
end
