unit EvPendingItemsGadgetFrm;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  evDataSet, evCommonInterfaces, StdCtrls, DBCtrls, ISBasicClasses, Graphics,
  isBaseClasses, isDataSet, evStreamUtils, isSchedule, Windows,
  evContext, evUIUtils,
  EvUIComponents, dbcgrids, DB, Wwdatsrc, Forms;

type
  TEvPendingItemsGadget = class(TEvDashboardGadget)
    dsrTasks: TevDataSource;
    ScrollBox1: TScrollBox;
    grTasks: TDBCtrlGrid;
    dbtNotes: TevDBText;
    dbtDue: TevDBText;
    dbtSubject: TevDBText;
    procedure pnlContentResize(Sender: TObject);
    procedure grTasksPaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure dbtSubjectClick(Sender: TObject);
    procedure grTasksDblClick(Sender: TObject);
  private
    FTasks: IevDataSet;
    function ShortenString(const aStr: string; aMaxLength: integer): string;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  public
    procedure AfterConstruction; override;
  end;


implementation

uses DateUtils;


{$R *.DFM}

{ TEvPendingItemsGadget }

procedure TEvPendingItemsGadget.PrepareData(const AInputParams, AData: IisListOfValues);
const
  ResStruct: array [0..3] of TDSFieldDef = (
    (FieldName: 'Nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'Subject';  DataType: ftString;  Size: 128;  Required: False),
    (FieldName: 'Notes';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'Due';  DataType: ftDateTime;  Size: 0;  Required: False));

var
  Q: IevQuery;
  ResultDS: IevDataSet;
  d: TDateTime;
  notes: String;
  sName: String;
  S: IisStream;
  Evt: IisScheduleEvent;

begin
  inherited;

  ResultDS := TevDataset.Create(ResStruct);
  ResultDS.LogChanges := False;

  Q := TevQuery.Create('SELECT sb_user_notice_nbr, name, notes, task, last_dismiss FROM sb_user_notice t WHERE {AsOfNow<t>} AND t.sb_user_nbr = :nbr');
  Q.Params.AddValue('nbr', Context.UserAccount.InternalNbr);
  Q.Result.First;

  while not Q.Result.Eof do
  begin
    S := TisStream.Create;
    TBlobField(Q.Result.Fields[3]).SaveToStream(S.RealStream);
    S.Position := 0;
    try
      Evt := ObjectFactory.CreateInstanceFromStream(S) as IisScheduleEvent;
      if Evt.Enabled then
      begin
        d := Evt.GetClosestStartTimeTo(Now);
        if d > 0 then
        begin
          notes := ShortenString(Q.Result.Fields[2].AsString, 78);
          sName := ShortenString(Q.Result.Fields[1].AsString, 59);

          // don't have a time now to implement via Canvas.TextWidth
          ResultDS.AppendRecord([Q.Result.Fields[0].AsInteger,
            sName,
            notes,
            d]);
        end;
      end;
    except
      // eat up broken items
    end;

    Q.Result.Next;
  end;

  ResultDS.IndexFieldNames := 'Due';

  AData.AddValue('Tasks', ResultDS);
end;
     
procedure TEvPendingItemsGadget.PresentData(const AData: IisListOfValues);
var
  DS: IevDataSet;
begin
  inherited;

  DS := IInterface(AData.Value['Tasks']) as IevDataSet;
  (DS.FieldByName('Due') as TDateTimeField).DisplayFormat := 'mm/dd/yyyy h:nn AM/PM';

  DS.First;
  if Assigned(FTasks) then
    if not DS.Locate('Nbr', FTasks.FieldByName('Nbr').AsInteger, []) then
      DS.First;

  dsrTasks.DataSet := nil;

  dbtSubject.Left := 6;
  dbtNotes.Left := 6;
  dbtDue.Left := 6;
  dbtSubject.Width := grTasks.PanelWidth - dbtSubject.Left * 2;
  dbtNotes.Width := dbtSubject.Width;
  dbtDue.Width := dbtSubject.Width;

  FTasks := DS;
  if FTasks.RecordCount > 0 then
    dsrTasks.DataSet := FTasks.vclDataSet;
  ShowScrollBar(grTasks.Handle, SB_VERT, True);

  if grTasks.CanFocus then
    grTasks.SetFocus;
end;

procedure TEvPendingItemsGadget.pnlContentResize(Sender: TObject);
begin
  inherited;
  grTasks.RowCount := grTasks.ClientHeight div 56;
end;

procedure TEvPendingItemsGadget.grTasksPaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  if DateOf(FTasks.FieldByName('Due').AsDateTime) = DateOf(Now) then
    dbtDue.Font.Color := clRed
  else
    dbtDue.Font.Color := clWindowText;
end;

procedure TEvPendingItemsGadget.AfterConstruction;
begin
  inherited;
  RefreshTimerInterval := 120000; //2 min
end;

procedure TEvPendingItemsGadget.dbtSubjectClick(Sender: TObject);
begin
  inherited;
  grTasks.SetFocus;
end;

procedure TEvPendingItemsGadget.grTasksDblClick(Sender: TObject);
begin
  ActivateFrameAs('Misc - User Scheduler', ['NavigateTo', FTasks.FieldByName('Nbr').AsInteger]);
end;

function TEvPendingItemsGadget.ShortenString(const aStr: string;
  aMaxLength: integer): string;
begin
  Result := aStr;
  if Length(Result) > aMaxLength then
    Result := Copy(Result, 1, aMaxLength - 3) + '...';
end;

end.
