unit EvDashboardFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, DB,
  Dialogs, ExtCtrls, EvDashboardGadgetFrm, EvMainboard, ISBasicClasses,
  EvUIComponents, StdCtrls, wwdblook, isUIwwDBLookupCombo, SDDClasses,
  SDataDicttemp, SDataStructure, EvStreamUtils, EvQueueTasksGadgetFrm,
  EvChartOfTheDayGadgetFrm, EvCompanyUpdatesGadgetFrm, EvFavoriteLinksGadgetFrm,
  EvFavoriteReportsGadgetFrm, EvPayrollDatesGadgetFrm, EvPendingItemsGadgetFrm,
  EvShortcutsGadgetFrm, EvContext, isBaseClasses, isMessenger, SShowLogo, SDataDictclient,
  EvWebBrowserFrm;

type
  TEvDashboard = class(TFrame, IisMessageRecipient)
    DM_TEMPORARY: TDM_TEMPORARY;
    evPanel1: TevPanel;
    evSplitter1: TevSplitter;
    evPanel3: TevPanel;
    evPanel2: TevPanel;
    pnlCompanyFilter: TevPanel;
    evLabel1: TevLabel;
    cbCompanies: TevDBLookupCombo;
    frmCompanyUpdates: TEvCompanyUpdatesGadget;
    evPanel4: TevPanel;
    frmPendingTasks: TEvPendingItemsGadget;
    frmQueueTasks: TEvQueueTasksGadget;
    frmShortcuts: TEvShortcutsGadget;
    imgSBLogo: TevImage;
    frmChartOfTheDay: TEvChartOfTheDayGadget;
    frmPayrollSpecificDates: TEvPayrollDatesGadget;
    frmFavoriteLinks: TEvFavoriteLinksGadget;
    frmFavoriteReports: TEvFavoriteReportsGadget;
    evPanel5: TevPanel;
    imgEvoLogo: TevImage;
    lByName: TevLabel;
    procedure cbCompaniesCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
    procedure cbCompaniesDropDown(Sender: TObject);
    procedure cbCompaniesExit(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    procedure Notify(const AMessage: String; const AParams: IisListOfValues);
    function  Destroying: Boolean;

    procedure InitLogoData;
    procedure OnChangeCompany(const ANew_Cl_Nbr, ANew_Co_Nbr: Integer);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;    
  end;

implementation


{$R *.dfm}

procedure TEvDashboard.AfterConstruction;
begin
  inherited;
  mb_Messenger.Subscribe(Self, ['EvDashboard.Initialize'], True);
  mb_Messenger.PostMessage(Self, 'EvDashboard.Initialize');
  InitLogoData;
end;

procedure TEvDashboard.InitLogoData;
var
  EvoLogo, SBLogo, BrandLogo: IisStream;
  ByName: String;
begin
  TevLogoPresenter.GetLogoInfo(EvoLogo, BrandLogo, SBLogo, ByName);
  if not Assigned(SBLogo) then
    if Assigned(BrandLogo) then
      SBLogo := BrandLogo
    else
      SBLogo := EvoLogo;

  imgSBLogo.Picture.Bitmap.LoadFromStream(SBLogo.RealStream);
  imgEvoLogo.Picture.Bitmap.LoadFromStream(EvoLogo.RealStream);

  lByName.Caption := '';
end;

procedure TEvDashboard.OnChangeCompany(const ANew_Cl_Nbr, ANew_Co_Nbr: Integer);
var
  Params: IisListOfValues;
begin
  Params := TisListOfValues.Create;
  Params.AddValue('Cl_Nbr', ANew_Cl_Nbr);
  Params.AddValue('Co_Nbr', ANew_Co_Nbr);

  if DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([ANew_Cl_Nbr, ANew_Co_Nbr]), []) then
  begin
    ctx_DataAccess.OpenClient( ANew_Cl_Nbr );
    DM_COMPANY.CO.DataRequired;
    DM_COMPANY.CO.Locate('CO_NBR', ANew_Co_Nbr, []);
  end;

  mb_Messenger.Broadcast('Dashboard.ChangeCompany', Params);
end;

procedure TEvDashboard.cbCompaniesCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  mb_Messenger.Broadcast('DashboardGadget.Unfreeze');
  OnChangeCompany(DM_TEMPORARY.TMP_CO.CL_NBR.AsInteger, DM_TEMPORARY.TMP_CO.CO_NBR.AsInteger);
end;

function TEvDashboard.Destroying: Boolean;
begin
  Result := csDestroying in ComponentState;
end;

procedure TEvDashboard.Notify(const AMessage: String; const AParams: IisListOfValues);
var
  s: String;
begin
  if AMessage = 'EvDashboard.Initialize' then
  begin
    DM_TEMPORARY.TMP_CO.DataRequired;
    DM_COMPANY.CO.DataRequired(DM_COMPANY.CO.RetrieveCondition);

    if DM_COMPANY.CO.RecordCount = 0 then
    begin
      if DM_TEMPORARY.TMP_CO.IndexFieldNames <> 'CUSTOM_COMPANY_NUMBER' then
      begin
        DM_TEMPORARY.TMP_CO.IndexFieldNames := 'CUSTOM_COMPANY_NUMBER';
        DM_TEMPORARY.TMP_CO.First;
      end;
      s := DM_TEMPORARY.TMP_CO.CUSTOM_COMPANY_NUMBER.AsString
    end
    else begin
      s := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([DM_CLIENT.CL.CL_NBR.Value, DM_COMPANY.CO.CO_NBR.Value]), []);
    end;

    cbCompanies.LookupValue := s;
    cbCompanies.OnCloseUp(Self, nil, nil, True);
  end;
end;

procedure TEvDashboard.BeforeDestruction;
begin
  mb_Messenger.Unsubscribe(Self, []);
  inherited;
end;

procedure TEvDashboard.cbCompaniesDropDown(Sender: TObject);
begin
  // disable all gadgets to avoid automatical closeup of Company dropdown list when some gadget gets a focus
  // did not find a better way
  mb_Messenger.Broadcast('DashboardGadget.Freeze');
end;

procedure TEvDashboard.cbCompaniesExit(Sender: TObject);
begin
  mb_Messenger.Broadcast('DashboardGadget.Unfreeze');
end;

procedure TEvDashboard.FrameResize(Sender: TObject);
begin
  frmFavoriteReports.Width := (Self.Width - evPanel1.Width - frmPayrollSpecificDates.Width) div 2; 
end;

end.
