unit EvFavoriteLinksGadgetFrm;

interface

uses
  Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  StdCtrls, DBCtrls, ISBasicClasses, EvUIComponents, dbcgrids, DB,
  Wwdatsrc, isUIDBImage, evUIUtils, EvStreamUtils, IsDataSet, EvDataSet,
  EvCommonInterfaces, isBaseClasses, isUtils, ISBasicUtils, Forms,
  Windows;

type
  TEvFavoriteLinksGadget = class(TEvDashboardGadget)
    dsrLinks: TevDataSource;
    ScrollBox1: TScrollBox;
    grLinks: TDBCtrlGrid;
    dbtPath: TevDBText;
    dbtName: TevDBText;
    imgPict: TISDBImage;
    procedure pnlContentCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure dbtPathClick(Sender: TObject);
    procedure imgPictClick(Sender: TObject);
  private
    FLinks: IevDataSet;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
  end;


implementation


{$R *.DFM}

procedure TEvFavoriteLinksGadget.pnlContentCanResize(Sender: TObject;
  var NewWidth, NewHeight: Integer; var Resize: Boolean);
begin
  inherited;
  grLinks.RowCount := grLinks.ClientHeight div 46
end;

procedure TEvFavoriteLinksGadget.PrepareData(const AInputParams, AData: IisListOfValues);
const
  ResStruct: array [0..2] of TDSFieldDef = (
    (FieldName: 'Name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'Path';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'Picture';  DataType: ftBlob;  Size: 0;  Required: False));

var
  Links: IisParamsCollection;
  ResultDS: IevDataSet;
  i: Integer;
  S: IisStream;
begin
  inherited;

  Links := GetExternalLinks;
  ResultDS := TevDataset.Create(ResStruct);
  ResultDS.LogChanges := False;
  for i := 0 to Links.Count - 1 do
    if Links[i].Value['Dashboard'] then
    begin
      ResultDS.Append;
      ResultDS.FieldByName('Name').AsString := Links[i].Value['Name'];
      ResultDS.FieldByName('Path').AsString := Links[i].Value['Path'];
      S := IInterface(Links[i].Value['Icon']) as IisStream;
      if Assigned(S) and (S.Size > 0) then
        S := IconToBitmap(S);
      if Assigned(S) and (S.Size > 0) then
        TBlobField(ResultDS.FieldByName('Picture')).LoadFromStream(S.RealStream);
      ResultDS.Post;
    end;

  AData.AddValue('Links', ResultDS);
end;

procedure TEvFavoriteLinksGadget.PresentData(const AData: IisListOfValues);
var
  DS: IevDataSet;
begin
  inherited;

  DS := IInterface(AData.Value['Links']) as IevDataSet;
  DS.First;
  if Assigned(FLinks) then
    if not DS.Locate('Name', FLinks.FieldByName('Name').AsString, []) then
      DS.First;

  dsrLinks.DataSet := nil;

  dbtName.Width := grLinks.PanelWidth - dbtName.Left - 4;
  dbtPath.Width := dbtName.Width;

  FLinks := DS;
  if FLinks.RecordCount > 0 then
    dsrLinks.DataSet := FLinks.vclDataSet;
  ShowScrollBar(grLinks.Handle, SB_VERT, True);

  if grLinks.CanFocus then
    grLinks.SetFocus;
end;

procedure TEvFavoriteLinksGadget.dbtPathClick(Sender: TObject);
begin
  grLinks.SetFocus;
  RunIsolatedProcess(dbtPath.Field.Text, '', []);
end;

procedure TEvFavoriteLinksGadget.imgPictClick(Sender: TObject);
begin
  grLinks.SetFocus;
end;

end.
