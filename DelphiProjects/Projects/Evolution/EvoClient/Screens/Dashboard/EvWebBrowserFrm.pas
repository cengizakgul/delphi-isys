// Copyright � 2000-2013 iSystems LLC. All rights reserved.
unit EvWebBrowserFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, isUIFashionPanel, ExtCtrls, DB, Wwdatsrc, ISBasicClasses,
  EvUIComponents, OleCtrls, SHDocVw, StdCtrls, LMDCustomButton, LMDButton,
  isUILMDButton, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIEdit, SPackageEntry, EvUIUtils, isBaseClasses;

type
  TevWebBrowserFrm = class(TFrameEntry)
    NavigationPanel: TisUIFashionPanel;
    BrowserPanel: TisUIFashionPanel;
    btnGoBackBtn: TevSpeedButton;
    btnGoForwardBtn: TevSpeedButton;
    btnGoHomeBtn: TevSpeedButton;
    btnStopBtn: TevSpeedButton;
    UrlEdit: TevEdit;
    ScrollBox1: TScrollBox;
    WebBrowser: TisWebBrowser;
    procedure btnGoBackBtnClick(Sender: TObject);
    procedure btnGoForwardBtnClick(Sender: TObject);
    procedure btnGoHomeBtnClick(Sender: TObject);
    procedure btnStopBtnClick(Sender: TObject);
    procedure WebBrowserDocumentComplete(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure WebBrowserCommandStateChange(Sender: TObject;
      Command: Integer; Enable: WordBool);
    procedure UrlEditKeyPress(Sender: TObject; var Key: Char);
  private
    FHomeURL: String;
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

{ TevWebBrowserFrm }

procedure TevWebBrowserFrm.Activate;
var
  ExtLinks: IisParamsCollection;
  i: Integer;
begin
  inherited;
  (Owner as TFramePackageTmpl).HideToolBar;
  UrlEdit.Width := NavigationPanel.ClientWidth - UrlEdit.Left - 30;

  ExtLinks := GetExternalLinks;
  for i := 0 to ExtLinks.Count - 1 do
    if ExtLinks[i].Value['Default'] then
    begin
      FHomeURL := ExtLinks[i].Value['Path'];
      Break;
    end;
    
  if FHomeURL = '' then
    FHomeURL := 'http://www.google.com';

  btnGoHomeBtn.Click;
end;

procedure TevWebBrowserFrm.Deactivate;
begin
  (Owner as TFramePackageTmpl).ShowToolBar;
  inherited;
end;

procedure TevWebBrowserFrm.btnGoBackBtnClick(Sender: TObject);
begin
  inherited;
  WebBrowser.GoBack;
end;

procedure TevWebBrowserFrm.btnGoForwardBtnClick(Sender: TObject);
begin
  inherited;
  WebBrowser.GoForward;
end;

procedure TevWebBrowserFrm.btnGoHomeBtnClick(Sender: TObject);
begin
  inherited;
  UrlEdit.Text := FHomeURL;
  WebBrowser.Navigate(UrlEdit.Text);
end;

procedure TevWebBrowserFrm.btnStopBtnClick(Sender: TObject);
begin
  inherited;
  WebBrowser.Stop;
end;

procedure TevWebBrowserFrm.WebBrowserDocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  inherited;
  UrlEdit.Text := WebBrowser.LocationURL;
end;

procedure TevWebBrowserFrm.WebBrowserCommandStateChange(Sender: TObject;
  Command: Integer; Enable: WordBool);
begin
   case Command of
   CSC_NAVIGATEBACK: btnGoBackBtn.Enabled := Enable;
   CSC_NAVIGATEFORWARD: btnGoForwardBtn.Enabled := Enable;
//   CSC_UPDATECOMMANDS: btnStopBtn.Enabled := Enable;
   end;
end;

procedure TevWebBrowserFrm.UrlEditKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    WebBrowser.Navigate(UrlEdit.Text);
end;


initialization
  RegisterClass(TevWebBrowserFrm);

end.