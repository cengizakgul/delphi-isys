inherited EvQueueTasksGadget: TEvQueueTasksGadget
  Width = 230
  Height = 454
  inherited pnlContent: TisUIFashionPanel
    Width = 230
    Height = 454
    Title = 'Tasks Completed'
    object ScrollBox1: TScrollBox
      Left = 12
      Top = 36
      Width = 198
      Height = 398
      HorzScrollBar.Visible = False
      VertScrollBar.Visible = False
      Align = alClient
      TabOrder = 0
      object grTasks: TDBCtrlGrid
        Left = 0
        Top = 0
        Width = 194
        Height = 394
        Align = alClient
        AllowDelete = False
        AllowInsert = False
        Color = 13434879
        DataSource = dsrTasks
        PanelHeight = 98
        PanelWidth = 177
        ParentColor = False
        TabOrder = 0
        RowCount = 4
        SelectedColor = 11133413
        OnDblClick = grTasksDblClick
        OnPaintPanel = grTasksPaintPanel
        object dbtStatus: TevDBText
          Left = 4
          Top = 43
          Width = 161
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Status'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
        object dbtTime: TevDBText
          Left = 25
          Top = 61
          Width = 144
          Height = 17
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          DataField = 'TimeStamp'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
        object dbtUser: TevDBText
          Left = 4
          Top = 61
          Width = 114
          Height = 17
          DataField = 'UserName'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
        object dbtTaskType: TevDBText
          Left = 4
          Top = 4
          Width = 168
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          DataField = 'TaskType'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
        object imgNew: TevImage
          Left = 149
          Top = 7
          Width = 18
          Height = 11
          Anchors = [akTop, akRight]
          AutoSize = True
          Center = True
          Picture.Data = {
            0954474946496D61676547494638396112000B00B30000FF6969FF0606FF1E1E
            FF0F0FFF6C6CFF1818FF1B1BFFE1E1FFBABAFFDEDEFF0C0CFF6666FFFFFFFF00
            0000000000000021F90400000000002C0000000012000B0000044590B10344BB
            F88A7524434A26620A320D63DA0C07D13017ECBD184C18AF4CC7346C043D99A4
            470BE060449ECE00D80991BA062011528914894FD5DA2879128B82AAB0C83222
            003B}
          Transparent = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
        object dbtTitle: TevDBText
          Left = 4
          Top = 18
          Width = 168
          Height = 25
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Caption'
          DataSource = dsrTasks
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          WordWrap = True
          OnClick = dbtTaskTypeClick
          OnDblClick = grTasksDblClick
        end
      end
    end
  end
  object dsrTasks: TevDataSource
    Left = 125
    Top = 136
  end
end
