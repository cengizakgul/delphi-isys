unit EvChartOfTheDayGadgetFrm;

interface

uses
  Windows, Forms, Messages, Classes, Controls, ExtCtrls, isUIFashionPanel, EvDashboardGadgetFrm,
  EvMainboard, OleCtrls, SHDocVw, ISBasicClasses, ISZippingRoutines,
  isBaseClasses, StdCtrls, EvUIComponents, isBasicUtils, EvStreamUtils;

type
  TEvChartOfTheDayGadget = class(TEvDashboardGadget)
    ScrollBox1: TScrollBox;
    WebBrowser: TisWebBrowser;
  private
    FReports: IisListOfValues;
  protected
    procedure PrepareData(const AInputParams, AData: IisListOfValues); override;
    procedure PresentData(const AData: IisListOfValues); override;
    procedure DoOnChangePage; override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;


implementation

uses SysUtils, EvContext, EvCommonInterfaces;


{$R *.DFM}

{ TEvChartOfTheDayGadget }

var HookID: THandle;

function MouseProc(nCode: Integer; wParam, lParam: Longint): Longint; stdcall;
var
  szClassName: array[0..255] of Char;
const
  ie_name = 'Internet Explorer_Server';
begin
  case nCode < 0 of
    True:
      Result := CallNextHookEx(HookID, nCode, wParam, lParam)
      else
        case wParam of
          WM_RBUTTONDOWN,
          WM_RBUTTONUP:
            begin
              GetClassName(PMOUSEHOOKSTRUCT(lParam)^.HWND, szClassName, SizeOf(szClassName));
              if lstrcmp(@szClassName[0], @ie_name[1]) = 0 then
                Result := HC_SKIP
              else
                Result := CallNextHookEx(HookID, nCode, wParam, lParam);
            end
            else
              Result := CallNextHookEx(HookID, nCode, wParam, lParam);
        end;
  end;
end;

procedure TEvChartOfTheDayGadget.AfterConstruction;
begin
  inherited;
  CompanyDrivenGadget := True;

  WebBrowser.Navigate('about:blank');  
  HookID := SetWindowsHookEx(WH_MOUSE, MouseProc, 0, GetCurrentThreadId());
  PagingTimerInterval := 60000; // 1 min
//  RefreshTimerInterval := 120000; // 2 min
end;

procedure TEvChartOfTheDayGadget.BeforeDestruction;
begin
  PagingTimerEnabled := False;
  if HookID <> 0 then
    UnHookWindowsHookEx(HookID);

  inherited;
end;

procedure TEvChartOfTheDayGadget.DoOnChangePage;
var
  RepData: IisStream;
  fn: String;
begin
  inherited;
  if ActivePageIndex = -1 then
    WebBrowser.Navigate('about:blank')
  else
  begin
    RepData := TisStream.Create;
    RepData.AsString := FReports[ActivePageIndex].Value;
    fn := AppTempFolder + 'dashboard_report.html';
    RepData.SaveToFile(fn);
    RepData := nil;
    WebBrowser.Navigate('file:///' + fn);
  end;
end;

procedure TEvChartOfTheDayGadget.PrepareData(const AInputParams, AData: IisListOfValues);
var
  R: IisListOfValues;
  s: String;
  ZipInfo, FolderInfo: TisFileInfo;
begin
  inherited;

  s := AppDir + 'FusionCharts.zip';
  if not FileExists(s) then
    Exit;

  ZipInfo := GetFileInfo(s);

  s := AppTempFolder + 'FusionCharts';
  if DirectoryExists(s) then
    FolderInfo := GetFileInfo(s)
  else
  begin
    FolderInfo.FileName := s;
    FolderInfo.CreationTime := 0;
  end;

  if ZipInfo.CreationTime > FolderInfo.CreationTime then
  begin
    ClearDir(FolderInfo.FileName);
    ForceDirectories(FolderInfo.FileName);
    ExtractFromFile(ZipInfo.FileName, FolderInfo.FileName, '*');
  end;

  R := Mainboard.DashboardDataProvider.GetCompanyReports(
    AInputParams.TryGetValue('Cl_Nbr', 0), AInputParams.TryGetValue('Co_Nbr', 0),
    Context.UserAccount.AccountType = uatRemote);

  AData.AddValue('Reports', R);
end;

procedure TEvChartOfTheDayGadget.PresentData(const AData: IisListOfValues);
begin
  inherited;
  FReports := IInterface(AData.Value['Reports']) as IisListOfValues;
  PageCount := FReports.Count;
end;

end.
