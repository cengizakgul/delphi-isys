object EvDashboardGadget: TEvDashboardGadget
  Left = 0
  Top = 0
  Width = 295
  Height = 208
  TabOrder = 0
  object pnlContent: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 295
    Height = 208
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    OnDblClick = pnlContentDblClick
    OnResize = pnlContentResize
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Dashboard Gadget'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
  end
end
