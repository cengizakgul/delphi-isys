object EvDashboard: TEvDashboard
  Left = 0
  Top = 0
  Width = 1018
  Height = 664
  TabOrder = 0
  OnResize = FrameResize
  object evSplitter1: TevSplitter
    Left = 751
    Top = 0
    Height = 664
    Align = alRight
  end
  object evPanel1: TevPanel
    Left = 754
    Top = 0
    Width = 264
    Height = 664
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    inline frmPendingTasks: TEvPendingItemsGadget
      Left = 0
      Top = 0
      Width = 264
      Height = 288
      Align = alTop
      TabOrder = 0
      inherited pnlContent: TisUIFashionPanel
        Width = 264
        Height = 288
        Margins.Top = 6
        Margins.Left = 6
        Margins.Right = 6
        inherited ScrollBox1: TScrollBox
          Left = 18
          Top = 42
          Width = 220
          Height = 226
          inherited grTasks: TDBCtrlGrid
            Width = 216
            Height = 222
            PanelHeight = 55
            PanelWidth = 199
          end
        end
      end
    end
    inline frmQueueTasks: TEvQueueTasksGadget
      Left = 0
      Top = 288
      Width = 264
      Height = 259
      Align = alClient
      TabOrder = 1
      inherited pnlContent: TisUIFashionPanel
        Width = 264
        Height = 259
        Margins.Top = 6
        Margins.Left = 6
        Margins.Right = 6
        inherited ScrollBox1: TScrollBox
          Left = 18
          Top = 42
          Width = 220
          Height = 197
          inherited grTasks: TDBCtrlGrid
            Width = 216
            Height = 193
            PanelHeight = 48
            PanelWidth = 199
            inherited imgNew: TevImage
              Picture.Data = {
                0954474946496D61676547494638396112000B00B30000FF6969FF0606FF1E1E
                FF0F0FFF6C6CFF1818FF1B1BFFE1E1FFBABAFFDEDEFF0C0CFF6666FFFFFFFF00
                0000000000000021F90400000000002C0000000012000B0000044590B10344BB
                F88A7524434A26620A320D63DA0C07D13017ECBD184C18AF4CC7346C043D99A4
                470BE060449ECE00D80991BA062011528914894FD5DA2879128B82AAB0C83222
                003B}
            end
          end
        end
      end
    end
    object evPanel5: TevPanel
      Left = 0
      Top = 547
      Width = 264
      Height = 117
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      DesignSize = (
        264
        117)
      object imgEvoLogo: TevImage
        Left = 8
        Top = 8
        Width = 248
        Height = 79
        Anchors = [akLeft, akTop, akRight, akBottom]
        Center = True
        Proportional = True
        Stretch = True
        Transparent = True
      end
      object lByName: TevLabel
        Left = 10
        Top = 91
        Width = 245
        Height = 13
        Alignment = taCenter
        Anchors = [akLeft, akRight, akBottom]
        AutoSize = False
        Caption = 'lByName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
    end
  end
  object evPanel3: TevPanel
    Left = 0
    Top = 0
    Width = 751
    Height = 664
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object evPanel2: TevPanel
      Left = 0
      Top = 0
      Width = 751
      Height = 166
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object pnlCompanyFilter: TevPanel
        Left = 0
        Top = 0
        Width = 237
        Height = 166
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 10
          Top = 122
          Width = 44
          Height = 13
          Caption = 'Company'
        end
        object imgSBLogo: TevImage
          Left = 0
          Top = 0
          Width = 237
          Height = 103
          Align = alTop
          Center = True
          Proportional = True
          Stretch = True
          Transparent = True
        end
        object cbCompanies: TevDBLookupCombo
          Left = 10
          Top = 137
          Width = 225
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company #'#9'F'
            'NAME'#9'40'#9'Name'#9#9)
          LookupTable = DM_TMP_CO.TMP_CO
          LookupField = 'CUSTOM_COMPANY_NUMBER'
          Options = [loTitles, loDisplaySelectedFields]
          Style = csDropDownList
          DropDownCount = 16
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnDropDown = cbCompaniesDropDown
          OnCloseUp = cbCompaniesCloseUp
          OnExit = cbCompaniesExit
        end
      end
      inline frmCompanyUpdates: TEvCompanyUpdatesGadget
        Left = 404
        Top = 0
        Width = 347
        Height = 166
        Align = alClient
        TabOrder = 1
        inherited pnlContent: TisUIFashionPanel
          Width = 347
          Height = 166
          Margins.Top = 6
          Margins.Left = 6
          inherited memMessage: TevMemo
            Left = 18
            Top = 42
            Width = 309
            Height = 104
          end
        end
      end
      inline frmShortcuts: TEvShortcutsGadget
        Left = 237
        Top = 0
        Width = 167
        Height = 166
        Align = alLeft
        TabOrder = 2
        inherited pnlContent: TisUIFashionPanel
          Width = 167
          Height = 166
          Margins.Top = 6
          Margins.Left = 6
          inherited bNewEmployee: TevSpeedButton
            Left = 14
            Top = 38
            Width = 137
          end
          inherited bNewPayroll: TevSpeedButton
            Left = 14
            Top = 63
            Width = 137
          end
          inherited btnWebBrowser: TevSpeedButton
            Left = 14
            Top = 88
            Width = 137
          end
        end
      end
    end
    object evPanel4: TevPanel
      Left = 0
      Top = 401
      Width = 751
      Height = 263
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      inline frmPayrollSpecificDates: TEvPayrollDatesGadget
        Left = 0
        Top = 0
        Width = 254
        Height = 263
        Align = alLeft
        TabOrder = 0
        inherited pnlContent: TisUIFashionPanel
          Width = 254
          Height = 263
          Margins.Top = 6
          Margins.Left = 6
          Margins.Bottom = 6
        end
      end
      inline frmFavoriteLinks: TEvFavoriteLinksGadget
        Left = 254
        Top = 0
        Width = 239
        Height = 263
        Align = alClient
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        inherited pnlContent: TisUIFashionPanel
          Width = 239
          Height = 263
          Margins.Top = 6
          Margins.Left = 6
          Margins.Bottom = 6
          inherited ScrollBox1: TScrollBox
            Left = 18
            Top = 42
            Width = 201
            Height = 195
            inherited grLinks: TDBCtrlGrid
              Width = 197
              Height = 191
              PanelHeight = 47
              PanelWidth = 180
              inherited dbtPath: TevDBText
                Width = 136
              end
              inherited dbtName: TevDBText
                Width = 136
              end
            end
          end
        end
      end
      inline frmFavoriteReports: TEvFavoriteReportsGadget
        Left = 493
        Top = 0
        Width = 258
        Height = 263
        Align = alRight
        TabOrder = 2
        inherited pnlContent: TisUIFashionPanel
          Width = 258
          Height = 263
          Margins.Top = 6
          Margins.Left = 6
          Margins.Bottom = 6
          inherited ScrollBox1: TScrollBox
            Left = 18
            Top = 42
            Width = 220
            Height = 195
            inherited grReports: TDBCtrlGrid
              Width = 216
              Height = 191
              PanelWidth = 199
              inherited dbtDescription: TevDBText
                Width = 233
                Height = 23
              end
            end
          end
        end
      end
    end
    inline frmChartOfTheDay: TEvChartOfTheDayGadget
      Left = 0
      Top = 166
      Width = 751
      Height = 235
      Align = alClient
      TabOrder = 2
      inherited pnlContent: TisUIFashionPanel
        Width = 751
        Height = 235
        Margins.Top = 6
        Margins.Left = 6
        inherited ScrollBox1: TScrollBox
          Left = 18
          Top = 42
          Width = 713
          Height = 173
          inherited WebBrowser: TisWebBrowser
            Width = 713
            Height = 173
            ControlData = {
              4C000000952A00008B0A00000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
      end
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 64
    Top = 120
  end
end
