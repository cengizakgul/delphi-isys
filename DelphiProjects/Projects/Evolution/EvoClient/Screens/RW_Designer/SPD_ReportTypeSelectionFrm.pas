unit SPD_ReportTypeSelectionFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TReportTypeSelection = class(TForm)
    btnRW: TButton;
    btnRM: TButton;
    Label1: TLabel;
    procedure btnRWClick(Sender: TObject);
  end;

  function AskForReportType: String;

implementation

{$R *.dfm}

function AskForReportType: String;
var
  Res: Word;
begin
  with TReportTypeSelection.Create(Application) do
  begin
    Res := ShowModal;
    if Res = mrYes then
      Result := 'RW'
    else if Res = mrNo then
      Result := 'RM'
    else
      Result := '';
    Free;
  end;
end;


procedure TReportTypeSelection.btnRWClick(Sender: TObject);
begin
  Close;
  ModalResult := TButton(Sender).ModalResult;
end;

end.
