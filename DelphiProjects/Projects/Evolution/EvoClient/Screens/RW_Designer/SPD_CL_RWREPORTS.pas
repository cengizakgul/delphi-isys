// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CL_RWREPORTS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  ISZippingRoutines, EvUtils,
  StdCtrls, Buttons, ISBasicClasses, EvContext, ISBasicUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TCLRWReports = class(TFrameEntry)
    btnRWDesigner: TevBitBtn;
    evBitBtn1: TevBitBtn;
    edSneaky: TEdit;
    procedure btnRWDesignerClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
  private
  public
  end;

implementation

{$R *.DFM}

var
  FPasswordEntered: Boolean = False;

{ TCLRWReports }

procedure TCLRWReports.btnRWDesignerClick(Sender: TObject);
var
  Notes, ClName, AncClName: String;

  procedure ShDsgn;
  begin
    ctx_RWDesigner.ShowDesigner(nil, 'Report', Notes, ClName, AncClName);
  end;

begin
  ShDsgn;
end;

procedure TCLRWReports.evBitBtn1Click(Sender: TObject);
begin
  RunProcess(AppDir + 'isRWCompare.exe', '', []);
end;


initialization
  RegisterClass(TCLRWReports);


end.
