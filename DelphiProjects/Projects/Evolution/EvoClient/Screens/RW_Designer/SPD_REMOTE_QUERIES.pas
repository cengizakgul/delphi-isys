// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REMOTE_QUERIES;

interface

uses
  SPD_EDIT_Open_BASE, Dialogs, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls,
  DBCtrls, Controls, Classes, EvUtils, Sysutils, EvSecElement,
  SPD_EDIT_CL_BASE, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDDClasses, ISDataAccessComponents, EvStreamUtils, EvContext,
  EvDataAccessComponents, SPD_EDIT_CO_BASE, SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  Forms, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TREMOTE_QUERIES = class(TEDIT_CO_BASE)
    dsQuery: TevDataSource;
    OpnDlg: TOpenDialog;
    SvDlg: TSaveDialog;
    TabSheet2: TTabSheet;
    Panel11: TevPanel;
    lQuery: TevLabel;
    Panel31: TevPanel;
    wwDBGrid1: TevDBGrid;
    cldsRes: TevClientDataSet;
    chbWizard: TCheckBox;
    btnOpen: TevBitBtn;
    btnQB: TevBitBtn;
    btnExport: TevBitBtn;
    chbClientData: TCheckBox;
    btnNew: TevBitBtn;
    procedure btnQBClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure dsQueryStateChange(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure chbClientDataClick(Sender: TObject);
  private
    FQuery: IEvDualStream;
    procedure OpenQuery;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;


implementation

{$R *.DFM}

{ TREMOTE_QUERIES }

procedure TREMOTE_QUERIES.btnQBClick(Sender: TObject);
var
  CoId: Integer;
begin
  if chbClientData.Checked then
    CoId := 0
  else
    CoId := DM_TEMPORARY.TMP_CO.CO_NBR.AsInteger;

  if ctx_RWDesigner.ShowQueryBuilder(FQuery, ctx_DataAccess.ClientId, CoId, chbWizard.Checked) then
  begin
    if (EvMessage('Save Query to a file?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      SvDlg.FileName := lQuery.Caption;
      if SvDlg.Execute then
      begin
        FQuery.SaveToFile(SvDlg.FileName);
        lQuery.Caption := ChangeFileExt(ExtractFileName(SvDlg.FileName), '');
      end
    end;
    OpenQuery;
  end;
end;


procedure TREMOTE_QUERIES.Activate;
begin
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;
  
  wwdsList.DataSet.Locate('CL_NBR', TevClientDataSet(wwdsMaster.DataSet).ClientID, []);
  if not BitBtn1.Enabled then
    BitBtn1Click(Self);

  FQuery := TEvDualStreamHolder.Create;
  ctx_RWLocalEngine.BeginWork;
end;

procedure TREMOTE_QUERIES.Deactivate;
begin
  ctx_RWLocalEngine.EndWork;
  inherited;
end;

procedure TREMOTE_QUERIES.btnOpenClick(Sender: TObject);
begin
  if OpnDlg.Execute then
  begin
    dsQuery.DataSet.Close;
    FQuery.LoadFromFile(OpnDlg.FileName);
    lQuery.Caption := ChangeFileExt(ExtractFileName(OpnDlg.FileName), '');
    OpenQuery;
  end;
end;

procedure TREMOTE_QUERIES.btnExportClick(Sender: TObject);
var
  key:word;
begin
  //DataSetToExcel(dsQuery.DataSet);
  key := Ord('E');
  wwDBGrid1.KeyDown(key, [ssCtrl]);
end;

procedure TREMOTE_QUERIES.dsQueryStateChange(Sender: TObject);
begin
  btnExport.Enabled := dsQuery.DataSet.Active;
end;

procedure TREMOTE_QUERIES.btnNewClick(Sender: TObject);
begin
  dsQuery.DataSet.Close;
  FQuery.Size := 0;
  lQuery.Caption := 'New Query';
  btnQB.Click;
end;

procedure TREMOTE_QUERIES.OpenQuery;
var
  Co_Nbr: Integer;
begin
  Update;
  ctx_StartWait('Running Query Builder Query...');
  try
    cldsRes.Close;

    if chbClientData.Checked then
      Co_Nbr := 0
    else
      Co_Nbr := DM_TEMPORARY.TMP_CO.CO_NBR.AsInteger;

    ctx_RWDesigner.RunQBQuery(FQuery, DM_TEMPORARY.TMP_CO.CL_NBR.AsInteger, Co_Nbr, nil, cldsRes);
  finally
    ctx_EndWait;
  end;
end;

procedure TREMOTE_QUERIES.BitBtn1Click(Sender: TObject);
begin
  inherited;

  if cldsRes.Active and
     (EvMessage('Reopen query for selected company?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    PageControl1.ActivePage := TabSheet2;
    OpenQuery;
  end;
end;

procedure TREMOTE_QUERIES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  inherited;
end;

procedure TREMOTE_QUERIES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CL, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CL;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_CLIENT.CL;
end;

procedure TREMOTE_QUERIES.chbClientDataClick(Sender: TObject);
begin
  if cldsRes.Active then
    OpenQuery;
end;

initialization
  RegisterClass(TREMOTE_QUERIES);

end.
