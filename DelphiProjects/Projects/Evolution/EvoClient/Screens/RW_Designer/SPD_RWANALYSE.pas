unit SPD_RWANALYSE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  StdCtrls, SFrameEntry, Buttons, ComCtrls,
  ImgList, SDataStructure, SReportSettings, EvUtils, EvTypes, EvConsts,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  Wwdatsrc, ExtCtrls, EvStreamUtils, EvDataAccessComponents,
  ISDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TRWAnalyse = class(TFrameEntry)
    dsResult: TevClientDataSet;
    dsComponents: TevClientDataSet;
    evPanel1: TevPanel;
    btnReport: TevBitBtn;
    chbSystem: TevCheckBox;
    chbSB: TevCheckBox;
    evPanel2: TevPanel;
    GroupBox2: TevGroupBox;
    tvSelect: TevTreeView;
    btnAdd: TevSpeedButton;
    btnDel: TevSpeedButton;
    evPanel3: TevPanel;
    GroupBox1: TevGroupBox;
    tvLibrary: TevTreeView;
    ilButtons: TImageList;
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnReportClick(Sender: TObject);
  private
//    FComponents: array of IrwLibraryComponent;
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}


{ TRWAnalyse }

procedure TRWAnalyse.Activate;
{var
  LibComponents: IrwLibraryComponents;
  CompList: TStringList;
  aInd: Integer;

  procedure FillNode(ANode: TTreeNode; AParentName: String);
  var
    i: Integer;
    N: TTreeNode;
    Comp: IrwLibraryComponent;
  begin
    for i := 0 to CompList.Count - 1 do
    begin
      Comp := LibComponents.GetComponent(CompList[i]);
      if SameText(Comp.GetAncestorClassName, AParentName) or
         (AParentName = '') and not Assigned(LibComponents.GetComponent(Comp.GetAncestorClassName)) then
      begin
        Inc(aInd);
        FComponents[aInd] := Comp;
        N := tvLibrary.Items.AddChildObject(ANode, Comp.GetDisplayName + ' (' + Comp.GetClassName + ')', Pointer(aInd));
        if Comp.IsInheritsFrom('TrwCustomReport') then
          N.ImageIndex := 38
        else if Comp.IsInheritsFrom('TrwSysLabel') then
          N.ImageIndex := 34
        else if Comp.IsInheritsFrom('TrwPCLLabel') then
          N.ImageIndex := 41
        else if Comp.IsInheritsFrom('TrwLabel') then
          N.ImageIndex := 30
        else if Comp.IsInheritsFrom('TrwDBText') then
          N.ImageIndex := 33
        else if Comp.IsInheritsFrom('TrwImage') then
          N.ImageIndex := 40
        else if Comp.IsInheritsFrom('TrwDBImage') then
          N.ImageIndex := 42
        else if Comp.IsInheritsFrom('TrwContainer') then
          N.ImageIndex := 37
        else if Comp.IsInheritsFrom('TrwQuery') then
          N.ImageIndex := 14
        else if Comp.IsInheritsFrom('TrwBuffer') then
          N.ImageIndex := 15
        else if Comp.IsInheritsFrom('TrwShape') then
          N.ImageIndex := 35
        else if Comp.IsInheritsFrom('TrwFrame') then
          N.ImageIndex := 36
        else if Comp.IsInheritsFrom('TrwDBGrid') then
          N.ImageIndex := 92
        else
          N.ImageIndex := 75;
        N.SelectedIndex := N.ImageIndex;
        N.StateIndex := N.ImageIndex;

        FillNode(N, Comp.GetClassName);
      end;
    end;
  end;

}
begin
  inherited;
{
  WaitObject.ctx_StartWait;
  try
    tvLibrary.Items.BeginUpdate;
    tvLibrary.Items[0].DeleteChildren;
    tvSelect.Items.Clear;
    tvLibrary.SortType := stNone;

//    LibComponents := TRWAdapter(RWEngineInterface.rwAdapter).RWEngine.LibraryComponents;
    CompList := TStringList.Create;
    try
      CompList.CommaText := LibComponents.GetComponentList;
      SetLength(FComponents, CompList.Count);
      aInd := -1;
      FillNode(tvLibrary.Items[0], '');
    finally
      FreeAndNil(CompList);
    end;

    tvLibrary.SortType := stText;
    tvLibrary.Items[0].Expand(False);
    tvLibrary.Items.EndUpdate;
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Open;

  finally
    WaitObject.ctx_EndWait;
  end;}
end;

procedure TRWAnalyse.btnAddClick(Sender: TObject);

  procedure AddNode(ANode: TTreeNode; AParentNode: TTreeNode);
  var
    N: TTreeNode;
    i: Integer;
  begin
    for i := 0 to tvSelect.Items.Count - 1 do
      if tvSelect.Items[i].Data = ANode.Data then
        Exit;
        
    N := tvSelect.Items.AddChildObject(AParentNode, ANode.Text, ANode.Data);
    N.SelectedIndex := ANode.SelectedIndex;
    N.StateIndex := ANode.StateIndex;
    N.ImageIndex := ANode.ImageIndex;

    for i := 0 to ANode.Count - 1 do
      AddNode(ANode.Item[i], N);
  end;

begin
  if not Assigned(tvLibrary.Selected) or (tvLibrary.Selected.Level = 0) then
    Exit;

  AddNode(tvLibrary.Selected, nil);
end;

procedure TRWAnalyse.btnDelClick(Sender: TObject);
begin
  if not Assigned(tvSelect.Selected) or (tvSelect.Selected.Level > 0) then
    Exit;

  tvSelect.Selected.Free;
  tvSelect.Selected := nil;
end;

procedure TRWAnalyse.btnReportClick(Sender: TObject);
{var
  MS: IEvDualStream;
  ReportParams: TrwReportParams;
  RepMaxCount, RepCount: Integer;
  Lev: String;
  LibComponents: IrwLibraryComponents;
  i: Integer;
  RMS: IisStream;

  procedure AnalyzeLevel(const DBLevel: String);
  var
    DS: TDataSet;
    IDFld: TField;
    R: TrwReportRec;
    h: String;

    function AnalyzeReport: String;
    var
      Res: TrwReportContentRec;
      L: TStringList;
      i, j: Integer;
      Comp: IrwLibraryComponent;
    begin
      Result := '';

//      Res := TRWAdapter(RWEngineInterface.rwAdapter).RWEngine.AnalyzeReport(@R);

      L := TStringList.Create;
      try
        L.CommaText := Res.UsedLibComponents;
        for i := 0 to L.Count - 1 do
        begin
          Comp := LibComponents.GetComponent(L[i]);
          if not Assigned(Comp) then
            Continue;

          for j := 0 to tvSelect.Items.Count - 1 do
          begin
            if Comp.IsInheritsFrom(FComponents[Integer(tvSelect.Items[j].Data)].GetClassName) then
            begin
              if Result <> '' then
                Result := Result + #13;
              Result := Result + Comp.GetDisplayName +'(' + Comp.GetClassName + ')';
              break;
            end;
          end;
        end;

      finally
        FreeAndNil(L);
      end;
    end;

  begin
    if DBLevel = CH_DATABASE_SYSTEM then
    begin
      DS := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS;
      IDFld := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR;
    end
    else if DBLevel = CH_DATABASE_SERVICE_BUREAU then
    begin
      DS := DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS;
      IDFld := DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS_NBR;
    end
    else
      Exit;

    DS.First;
    while not DS.Eof do
    begin
      WaitObject.ctx_UpdateWait('Reports left: ' + IntToStr(RepCount), RepMaxCount - RepCount);
      try
        MS.Clear;
        RWEngineInterface.GetReportResources(IDFld.AsInteger, DBLevel, MS);
        Initialize(R);
        R.Data := MS;
        R.FormatType := rwRFTbinary;

        h := AnalyzeReport;

        if h <> '' then
        begin
          dsResult.Append;
          dsResult.FieldByName('RepName').AsString := DS.FieldByName('REPORT_DESCRIPTION').AsString + '  (' + IDFld.AsString + ')';
          dsResult.FieldByName('Level').AsString := Lev;
          dsResult.FieldByName('Components').AsString := h;
          dsResult.Post;
        end;

      except
        on E: Exception do
          EvErrMessage('Report: '+DS.FieldByName('REPORT_DESCRIPTION').AsString + ' ('+Lev + ')' +
            #13+E.Message);
      end;

      Dec(RepCount);
      DS.Next;
    end;
  end;
}
begin
{  if (tvSelect.Items.Count = 0) or not (chbSystem.Checked or chbSB.Checked) then
    Exit;

  MS := TMemoryStream.Create;
  ReportParams := TrwReportParams.Create;
  dsResult.CreateDataSet;
  dsResult.Open;
  dsComponents.CreateDataSet;
  dsComponents.Open;

  RepCount := 0;
  if chbSystem.Checked then
    RepCount := RepCount + DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.RecordCount;
  if chbSB.Checked then
  begin
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Open;
    RepCount := RepCount + DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.RecordCount;
  end;
  RepMaxCount := RepCount;

//  TRWAdapter(RWEngineInterface.rwAdapter).CacheReportLicenceInfo;
//  LibComponents := TRWAdapter(RWEngineInterface.rwAdapter).RWEngine.LibraryComponents;

  try
    WaitObject.ctx_StartWait('Analyzing list of reports...');
    WaitObject.ctx_StartWait('Reports left: ' + IntToStr(RepMaxCount), RepMaxCount);
    try
      if chbSystem.Checked then
      begin
        Lev := 'System';
        AnalyzeLevel(CH_DATABASE_SYSTEM);
      end;

      if chbSB.Checked then
      begin
        Lev := 'SB';
        AnalyzeLevel(CH_DATABASE_SERVICE_BUREAU);
      end;
    finally
      WaitObject.ctx_EndWait;
      WaitObject.ctx_EndWait;
    end;


    WaitObject.ctx_StartWait('Generating of result report...');
    try
      if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'RW Analyze', [loCaseInsensitive]) then
        raise EMissingReport.CreateHelp('"RW Analyze" report does not exist in the database!', IDH_MissingReport);

      for i := 0 to tvSelect.Items.Count - 1 do
      begin
        dsComponents.Append;
        dsComponents.FieldByName('CompName').AsString := FComponents[Integer(tvSelect.Items[i].Data)].GetDisplayName +
         ' (' + FComponents[Integer(tvSelect.Items[i].Data)].GetClassName + ')';
        dsComponents.Post;
      end;

      ReportParams.Add('DataSets', DataSetsToVarArray([dsComponents, dsResult]), False);

      RWEngineInterface.StartGroup;
      try
        RWEngineInterface.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsInteger,
          CH_DATABASE_SYSTEM, ReportParams);
      finally
        RMS := RWEngineInterface.EndGroup(rdtNone);
      end;

    finally
      WaitObject.ctx_EndWait;
    end;

  finally
    dsResult.Close;
    dsComponents.Close;
    FreeAndNil(ReportParams);
    FreeAndNil(MS);
  end;

  RWEngineInterface.Preview(RMS);}
end;


procedure TRWAnalyse.Deactivate;
begin
  inherited;
end;


initialization
  RegisterClass(TRWAnalyse);

end.
