object ReportTypeSelection: TReportTypeSelection
  Left = 550
  Top = 286
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Report Type'
  ClientHeight = 90
  ClientWidth = 301
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 27
    Top = 8
    Width = 246
    Height = 13
    Caption = 'What type of report do you want to create?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnRW: TButton
    Left = 8
    Top = 37
    Width = 133
    Height = 42
    Caption = 'Report Writer'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 6
    ParentFont = False
    TabOrder = 0
    OnClick = btnRWClick
  end
  object btnRM: TButton
    Left = 160
    Top = 37
    Width = 133
    Height = 42
    Caption = 'Report Master'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 7
    ParentFont = False
    TabOrder = 1
    OnClick = btnRWClick
  end
end
