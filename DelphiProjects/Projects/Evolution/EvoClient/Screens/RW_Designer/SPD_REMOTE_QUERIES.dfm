inherited REMOTE_QUERIES: TREMOTE_QUERIES
  Width = 1112
  inherited Panel1: TevPanel
    Width = 1112
    inherited pnlFavoriteReport: TevPanel
      Left = 960
    end
    inherited pnlTopLeft: TevPanel
      Width = 960
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1112
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1104
        inherited pnlBorder: TevPanel
          Width = 1100
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1100
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              Left = 409
              Width = 17
            end
          end
          inherited Panel3: TevPanel
            Width = 1052
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 1052
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 509
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 459
                IniAttributes.SectionName = 'TREMOTE_QUERIES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 512
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Queries'
      ImageIndex = 1
      object Panel11: TevPanel
        Left = 0
        Top = 0
        Width = 1104
        Height = 58
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lQuery: TevLabel
          Left = 0
          Top = 0
          Width = 267
          Height = 54
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          WordWrap = True
        end
        object chbWizard: TCheckBox
          Left = 541
          Top = 33
          Width = 81
          Height = 17
          Caption = 'Wizard View'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object btnOpen: TevBitBtn
          Left = 402
          Top = 2
          Width = 131
          Height = 25
          Caption = 'Open Existing Query'
          TabOrder = 1
          OnClick = btnOpenClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD8800
            088DDDDDDDDDFF8888FFD888888800EE6008DFFFFFDF88DD888880000000E7EE
            6660F88888D8DDDD88883B033336E76686608D8888D8DD8888883BB0333666EE
            68808DD888D888DD88883BB80336E7EE66608DDD88D8DDDD88883B8AA036E766
            86608DD8D8D8DD8888883B8AA80666EEE8808DD88DD888DDD8883BB88BB6EEEE
            EEE68DDDDDD8DDDDDDD83B8AA03366EEE6688DD88F8D88DDD88D3B8AA08DD866
            68DD8DD88FDDDD888DDDD338AA08DDDDDDDDD88D88FDDDDDDDDDDDDDDAA0DDDD
            DDDDDDDDD88FDDDDDDDDDA088AA0DDDDDDDDD8FDD88FDDDDDDDDDAA0AAA0DDDD
            DDDDD88F888FDDDDDDDDDAAAAA0DDDDDDDDDD88888FDDDDDDDDD}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
        object btnQB: TevBitBtn
          Left = 541
          Top = 2
          Width = 116
          Height = 25
          Caption = 'Query Builder'
          TabOrder = 2
          OnClick = btnQBClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD888888888
            8888DDDFFFFFFFFFFFFFDD44444444444444DD88F8F8F8F8F8F83334F4F4F4F4
            F4F488D88888888888883FFF4FF7F7F7F7F48DDD8888888888883FFFF4FF4444
            4FF48DDDD888888888883F88874FF4864FF48DDDDD8888FDD88837AA0764FF48
            4FF48D88FDD8888FD88837AA07664FF44FF48D88FD8D8888F8883788876E64FF
            4FF48DDDDD8DD888888833AA036E764FFFF48D88F88DDD88888833AA03666E64
            FFF48D88F8888DD88888DDDAA06EEEE64FF4DDD88F8DDDDD888888DDAA066EEE
            64F4DDDD88F88DDDD888A088AA0D86668D448FDD88FDD888DD88AA0AAA0DDDDD
            DDD488F888FDDDDDDDD8AAAAA0DDDDDDDDDD88888FDDDDDDDDDD}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
        object btnExport: TevBitBtn
          Left = 665
          Top = 2
          Width = 144
          Height = 25
          Caption = 'Export Result To Excel'
          TabOrder = 3
          OnClick = btnExportClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0DDDDDDDDD
            DDDDDDFDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD8FDDDDDDDDDDDDDD0B0DDDDDDD
            DDDDDD88FDDDDDDDDDDD000BB0888888888DFF888FDDDDDDDDDD0BBBBB022222
            2222888888FD888888880BBBBBB07FFFFFF28888888FDDDDDDD80BBBBB08FF72
            22F28888888DDDD888D8000BB022222722F2888888D8888D88D8880B02222272
            27F2DD888D8888D88DD8DD008F22272222F2DD88DD888D8888D8DD02FFF27227
            FFF2DD8DDDD8D88DDDD8DD82FF2722227FF2DDD8DD8D8888DDD8DD82F2722722
            27F2DDD8D8D88D888DD8DD82F2227F7222F2DDD8D888DDD888D8DD82FFFFFFFF
            FFF2DDD8DDDDDDDDDDD8DDD2222222222222DDD8888888888888}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
        object chbClientData: TCheckBox
          Left = 666
          Top = 33
          Width = 148
          Height = 17
          Caption = 'Show data for entire client'
          TabOrder = 4
          OnClick = chbClientDataClick
        end
        object btnNew: TevBitBtn
          Left = 263
          Top = 2
          Width = 131
          Height = 25
          Caption = 'Create New Query'
          TabOrder = 5
          OnClick = btnNewClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD8866
            688DDDDDDDDDDD888DDD88888888667766688888888D88DD888D8FFFFF767777
            66668DDDDDD8DDDD88888FFFFF76776686668DDDDDD8DD8888888FFFFF766677
            68868DDDDDD888DD88888FF88876777766668DDDDDD8DDDD88888F7EE6767766
            86668DD88FD8DD88D8888F7EE676668888868DD88FD888DDDDD888888886888F
            C88888DDDDD8DDD8FDDD888EE688888FC88888D88FDDDDD8FDDDDDDEE68DFC8F
            C8FCDDD88FDD8FD8FD8FDDDDEE68DFCDDFCDDDDD88FDD8FDD8FDD88DDEE6DDDD
            DDDDDDDDD88FDDDDDDDDDE688EE6DFCFCFCDD8FDD88FD8F8F8FDDEE6EEE6FCDF
            CDFCD88F888F8FD8FD8FDEEEEE6DDDDFCDDDD88888FDDDD8FDDD}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
      end
      object Panel31: TevPanel
        Left = 0
        Top = 58
        Width = 1104
        Height = 125
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 1
        object wwDBGrid1: TevDBGrid
          Left = 2
          Top = 2
          Width = 1100
          Height = 121
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TREMOTE_QUERIES\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dsQuery
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          DefaultSort = '-'
          NoFire = False
        end
      end
    end
  end
  inherited PageControlImages: TevImageList
    Top = 152
  end
  object dsQuery: TevDataSource
    AutoEdit = False
    DataSet = cldsRes
    OnStateChange = dsQueryStateChange
    Left = 240
    Top = 111
  end
  object OpnDlg: TOpenDialog
    DefaultExt = 'rwq'
    Filter = 'Query Builder Queries (*.rwq)|*.rwq|All Files (*.*)|*.*'
    Title = 'Open Query Builder Query'
    Left = 280
    Top = 159
  end
  object SvDlg: TSaveDialog
    DefaultExt = 'rwq'
    Filter = 'Query Builder Queries (*.rwq)|*.rwq|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Query Builder Query'
    Left = 320
    Top = 159
  end
  object cldsRes: TevClientDataSet
    Left = 236
    Top = 146
  end
end
