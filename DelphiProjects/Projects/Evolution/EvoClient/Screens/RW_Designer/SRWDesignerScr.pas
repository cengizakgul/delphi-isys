// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SRWDesignerScr;

interface

uses
  SysUtils, SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList, ISBasicClasses,
  EvUtils, SReportSettings, EvReportWriterProxy, EvStreamUtils, DB, EvCommonInterfaces,
  SSecurityInterface, SPD_ReportTypeSelectionFrm, EvContext, EvMainboard, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TrwDesignerInterfaceObject = class(TFramePackageTmpl, IevRWDesigner)
  private
    function  rwProxy: IevReportWriterProxy;
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;

  public
    procedure  ShowDesigner(AData: IEvDualStream; AReportName: string; var ANotes: string; var AClassName: string; var AAncestorName: string);
    function   ShowQueryBuilder(AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; UserView: Boolean): Boolean;
    procedure  RunQBQuery(AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TevClientDataSet);
    function   ReCreateWizardReport(AResStream: IEvDualStream): Boolean;
  end;

var
  rwDesignerFrm: TrwDesignerInterfaceObject;

implementation

{$R *.DFM}

function GetRWDesignerScreens: IevRWDesigner;
begin
  if not Assigned(rwDesignerFrm) then
    rwDesignerFrm := TrwDesignerInterfaceObject.Create(nil);
  Result := rwDesignerFrm;
end;


{ TrwDesignerInterfaceObject }

procedure TrwDesignerInterfaceObject.ShowDesigner(AData: IEvDualStream; AReportName: string; var ANotes: string; var AClassName: string; var AAncestorName: string);
var
  RT: Integer;
  RepStruct: TevRWReportInfo;
begin
  RT := 0;

  if not Assigned(AData) or Assigned(AData) and (AData.Size = 0) then
  begin
    AClassName := AskForReportType;
    if AClassName = 'RM' then
      RT := 1
    else if AClassName = 'RW' then
      RT := 0
    else
        Exit;
    end;

  rwProxy.StartSession;
  try
    if rwProxy.ShowDesigner(AData, AReportName,RT) and Assigned(AData) then
    begin
      RepStruct := rwProxy.GetReportInfo(AData);
      AClassName := RepStruct.ReportClassName;
      AAncestorName := RepStruct.AncestorClassName;
      ANotes := RepStruct.Description;
    end
  finally
    rwProxy.EndSession;
  end;
end;


function TrwDesignerInterfaceObject.PackageCaption: string;
begin
  Result := '&Misc';
end;


procedure TrwDesignerInterfaceObject.UserPackageStructure;
begin
  AddStructure('Query Builder|012', 'TREMOTE_QUERIES');
  AddStructure('Report Writer|013', 'TCLRWREPORTS');
  AddStructure('RW Analyze|014', 'TRWANALYSE');
end;

function TrwDesignerInterfaceObject.PackageSortPosition: string;
begin
  Result := '169';
end;

function TrwDesignerInterfaceObject.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

procedure TrwDesignerInterfaceObject.RunQBQuery(AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TevClientDataSet);
begin
  rwProxy.RunQueryBuilderQuery(AQuery, AClientNbr, ACompanyNbr, AParams, AResultDataSet);
end;

function TrwDesignerInterfaceObject.ShowQueryBuilder(AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; UserView: Boolean): Boolean;
begin
  Result := rwProxy.ShowQueryBuilder(AQuery, AClientNbr, ACompanyNbr, UserView);
end;

function TrwDesignerInterfaceObject.ReCreateWizardReport(AResStream: IEvDualStream): Boolean;
//var
//  ReportInfo: TrwReportRec;
begin
{  ReportInfo.Data := AResStream;
  ReportInfo.FormatType := rwRFTbinary;
  ReportInfo.Password := '';     //### Licence logic ?

  Result := ctx_RWDesigner.ReCreateWizardReport(@ReportInfo);
  ReportInfo.Data.Position := 0;}

  Result := False;
end;


function TrwDesignerInterfaceObject.rwProxy: IevReportWriterProxy;
begin
  Result := ctx_RWLocalEngine.ReportWriteProxy as IevReportWriterProxy;
end;


initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRWDesignerScreens, IevRWDesigner, 'Screens RW Designer');

finalization
  rwDesignerFrm.Free;

end.
