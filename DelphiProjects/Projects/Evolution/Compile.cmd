@ECHO OFF
REM Parameters: 
REM    1. Version(optional)

SET pAppVersion=%1

ECHO *Compiling
..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion%

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
XCOPY /Y /Q .\AppProjects\*.mes ..\..\Bin\Evolution\*.* > NUL
FOR %%a IN (..\..\Bin\Evolution\*.exe) DO ..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL
FOR %%a IN (..\..\Bin\Evolution\*.exe) DO ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
FOR %%a IN (..\..\Bin\Evolution\*.dll) DO ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
FOR %%a IN (..\..\Bin\Evolution\*.mes) DO DEL /F /Q ..\..\Bin\Evolution\*.mes
FOR %%a IN (..\..\Bin\Evolution\*.map) DO DEL /F /Q ..\..\Bin\Evolution\*.map
FOR %%a IN (..\..\Bin\Evolution\*.exe) DO ..\..\Common\IsUtils\isExeProtector.exe p %%a > NUL
FOR %%a IN (..\..\Bin\Evolution\*.dll) DO ..\..\Common\IsUtils\isExeProtector.exe p %%a > NUL

REM FOR %%a IN (..\..\Bin\Evolution\*.exe) DO "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin\amd64\editbin.exe" %%a /LARGEADDRESSAWARE > NUL

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end
IF "%pAppVersion%" == "" PAUSE
