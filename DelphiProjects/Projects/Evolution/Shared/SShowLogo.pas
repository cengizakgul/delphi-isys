// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SShowLogo;

interface

uses
  Windows, SysUtils, Controls, extctrls, DBCtrls, Graphics, EvUtils, Variants, jpeg,
  EvContext, EvMainboard, EvTypes, EvStreamUtils, isBasicUtils, EvDataSet, EvCommonInterfaces, DB,
  stdctrls, Classes, Forms, SDataStructure, EvUIComponents, EvCompanyUpdatesGadgetFrm,
  EvConsts;


type
  TevLogoPresenter = class(TComponent)
  private
    FLogoImage: TImage;
//    FCompanyUpdatesGadget: TEvCompanyUpdatesGadget;
//    FMessageLabel: TevLabel;
//    procedure MessageOnChange(Sender: TObject);
  public
    class procedure AssignGraphicData(const APicture: TPicture; const AGraphicData: TGraphicData);
    class procedure GetLogoInfo(out AEvoLogo: IisStream; out ABrandLogo: IisStream; out ASBLogo: IisStream; out AByName: String);
    procedure Refresh;
    procedure ShowLogo(const AParent: TWinControl);
    procedure AlignLogo;
  end;

function  LogoPresenter: TevLogoPresenter;

implementation

uses Types;

{$R SShowLogo.RES}

var
  FLogoPresenter: TevLogoPresenter;

function LogoPresenter: TevLogoPresenter;
begin
  if not Assigned(FLogoPresenter) then
    FLogoPresenter := TevLogoPresenter.Create(Application.MainForm);

  Result := FLogoPresenter
end;

class procedure TevLogoPresenter.AssignGraphicData(const APicture: TPicture; const AGraphicData: TGraphicData);
var
  Pict: TGraphic;
begin
  Pict := nil;
  try
    if AnsiSameText(AGraphicData.FileExt, 'bmp') then
      Pict := TBitmap.Create
    else if AnsiSameText(AGraphicData.FileExt, 'jpg') then
      Pict := TJPEGImage.Create
    else
      Assert(False);

    AGraphicData.Data.Position := 0;
    Pict.LoadFromStream(AGraphicData.Data.RealStream);
    APicture.Graphic := Pict;
  finally
    Pict.Free;
  end;
end;

class procedure TevLogoPresenter.GetLogoInfo(out AEvoLogo: IisStream; out ABrandLogo: IisStream; out ASBLogo: IisStream; out AByName: String);
var
  Q: IevQuery;
begin
  AEvoLogo:= GetBinaryResource('MAINLOGO', 'BMP');
  if Assigned(AEvoLogo) then
    AEvoLogo.Position := 0;

  if Assigned(ctx_DomainInfo.BrandInfo.TitlePicture.Data) then
    ABrandLogo := ctx_DomainInfo.BrandInfo.TitlePicture.Data;

  Q := TevQuery.Create('SELECT SB_NAME, SB_LOGO FROM sb t WHERE {AsOfNow<t>}');
  Q.Execute;

  if Context.UserAccount.SBAccountantNbr = 0 then
  begin
    ASBLogo := TisStream.Create;
    (Q.Result.Fields[1] as TBlobField).SaveToStream(ASBLogo.RealStream);
    ASBLogo.Position := 0;

    AByName := Q.Result.Fields[0].AsString;
  end
  else
  begin
    Q := TevQuery.Create('SELECT name FROM sb_accountant t WHERE t.sb_accountant_nbr = :nbr AND {AsOfNow<t>}');
    Q.Params.AddValue('nbr', Context.UserAccount.SBAccountantNbr);
    Q.Execute;
    Assert(Q.Result.RecordCount = 1);
    AByName := Q.Result.Fields[0].AsString;
  end;
end;

{ TevLogoPresenter }

procedure TevLogoPresenter.AlignLogo;
var
  FMainPanel: TWinControl;
  FMainPanelX, FMainPanelY: Integer;
begin
  if not Assigned(FLogoImage) or not Assigned(FLogoImage.Parent) then
    Exit;

  FMainPanel := FLogoImage.Parent;
  FMainPanelX := 0;
  FMainPanelY := 0;
  while FMainPanel.Name <> 'MainWorkPanel' do
    if FMainPanel.Parent <> nil then
    begin
      Inc(FMainPanelX, FMainPanel.Left);
      Inc(FMainPanelY, FMainPanel.Top);
      FMainPanel := FMainPanel.Parent;
    end
    else
      Exit;

  if FLogoImage.ComponentCount < 2 then
  begin
    FLogoImage.Left := ((FMainPanel.Width - FLogoImage.Width) div 2) - FMainPanelX;
    FLogoImage.Top := ((FMainPanel.Height - FLogoImage.Height) div 2) - FMainPanelY;
    if FLogoImage.Top < 0 then
      FLogoImage.Top := 0;
  end
  else
  begin
    FLogoImage.Left := ((FMainPanel.Width - FLogoImage.Width) div 2) - FMainPanelX;
    FLogoImage.Top := ((FMainPanel.Height - FLogoImage.Height - TLabel(FLogoImage.Components[0]).Height - 10 -
                        TevImage(FLogoImage.Components[1]).Height) div 2) - FMainPanelY;
    if FLogoImage.Top < 0 then
      FLogoImage.Top := 0;
    with TLabel(FLogoImage.Components[0]) do
    begin
      Top := FLogoImage.Top + FLogoImage.Height + 10;
      Left := FLogoImage.Left;
    end;
    with TevImage(FLogoImage.Components[1]) do
    begin
      Top := TLabel(FLogoImage.Components[0]).Top + TLabel(FLogoImage.Components[0]).Height + 10;
      Left := FLogoImage.Left;
    end;
  end;

//  MessageOnChange(nil);
end;

procedure TevLogoPresenter.Refresh;
var
  P: TWinControl;
begin
  if not Assigned(FLogoImage) then
    Exit;

  P := FLogoImage.Parent;
  FreeAndNil(FLogoImage);

{  if Assigned(FCompanyUpdatesGadget) then
    FCompanyUpdatesGadget.Refresh;}

  if Assigned(P) then
    ShowLogo(P);
end;

procedure TevLogoPresenter.ShowLogo(const AParent: TWinControl);
var
  Lab: TevLabel;
  SBImg: TevImage;
  b: Boolean;
  EvoLogo, BrandLogo, SBLogo: IisStream;
  ByName: String;
begin
{  if not Assigned(FCompanyUpdatesGadget) and (ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_TO_DASHBOARD') <> stEnabled) then
  begin
    FCompanyUpdatesGadget := TevCompanyUpdatesGadget.Create(Self);
    FCompanyUpdatesGadget.memMessage.OnChange := MessageOnChange;

    FMessageLabel := TevLabel.Create(Self);
    with FMessageLabel do
    begin
      Transparent := True;
      ShowAccelChar := False;
      Font.Size := 13;
      Caption := '';
    end;
  end;}

  if not Assigned(FLogoImage) then
  begin
    FLogoImage := TImage.Create(Self);

    GetLogoInfo(EvoLogo, BrandLogo, SBLogo, ByName);

    with FLogoImage do
    begin
      Autosize := True;
      Stretch := True;
    end;

    FLogoImage.Transparent := True;
    if Assigned(BrandLogo) and (BrandLogo.Size > 0) then
    begin
      AssignGraphicData(FLogoImage.Picture, ctx_DomainInfo.BrandInfo.TitlePicture);
    end
    else
    begin
      if Assigned(EvoLogo) and (EvoLogo.Size > 0) then
        FLogoImage.Picture.Bitmap.LoadFromStream(EvoLogo.RealStream);

      Lab := TevLabel.Create(FLogoImage);
      with Lab do
      begin
        AutoSize := True;
        Transparent := True;
        ShowAccelChar := False;
        Font.Size := 13;
        Font.Color := clBtnShadow;
        Font.Style := Font.Style+ [fsBold];
//        Caption := 'by '+ ByName;
        AutoSize := False;
        Alignment := taRightJustify;
        Width := FLogoImage.Width;
      end;

      SBImg := TevImage.Create(FLogoImage);
      with SBImg do
      begin
        Autosize := True;
        Transparent := True;
        Stretch := True;
        if Assigned(SBLogo) and (SBLogo.Size > 0) then
          Picture.Bitmap.LoadFromStream(SBLogo.RealStream)
      end;
    end;
  end;

  b := FLogoImage.Parent <> AParent;
  if b then
    FLogoImage.Parent := AParent;

  if b and (FLogoImage.ComponentCount >= 2) then
  begin
    TevLabel(FLogoImage.Components[0]).Parent := AParent;
    TevImage(FLogoImage.Components[1]).Parent := AParent;
  end;

{  if b and Assigned(FMessageLabel) then
    FMessageLabel.Parent := AParent;}

  AlignLogo;
end;

{procedure TevLogoPresenter.MessageOnChange(Sender: TObject);
begin
  if Assigned(FMessageLabel) then
  begin
    FMessageLabel.Caption := FCompanyUpdatesGadget.memMessage.Text;
    FMessageLabel.AutoSize := False;
    FMessageLabel.AutoSize := True;

    if Assigned(FMessageLabel.Parent) then
    begin
      FMessageLabel.Left := (FMessageLabel.Parent.ClientWidth - FMessageLabel.Width) div 2;
      FMessageLabel.Top :=  FLogoImage.BoundsRect.Bottom + 50;
    end;
  end;
end;}

end.

