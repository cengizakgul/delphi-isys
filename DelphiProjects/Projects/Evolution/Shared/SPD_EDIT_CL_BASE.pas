// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_BASE;

interface

uses
  SDataStructure, StdCtrls, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, Classes, Db, Wwdatsrc,
   Windows, SPackageEntry, Graphics, EvConsts, EvUtils,
  SPD_EDIT_Open_BASE, EvSecElement, EvTypes, SDDClasses, ISBasicClasses,
  SDataDictclient, SDataDicttemp, EvExceptions, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  Forms, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_BASE = class(TEDIT_OPEN_BASE)
    DM_CLIENT: TDM_CLIENT;
    evLabel47: TevLabel;
    pnlTopRight: TevPanel;
    Label5: TevLabel;
    DBText2: TevDBText;
    DBText1: TevDBText;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CL, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CL;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_CLIENT.CL;
end;

procedure TEDIT_CL_BASE.Activate;
begin
  if not (Self is FindClass('TEDIT_CL')) then
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  inherited;
  if TevClientDataSet(wwdsMaster.DataSet).ClientID <> 0 then
     wwdsList.DataSet.Locate('CL_NBR', TevClientDataSet(wwdsMaster.DataSet).ClientID, [])
  else
     wwdsList.DataSet.First;

  if not BitBtn1.Enabled or (TevClientDataSet(wwdsMaster.DataSet).ClientID = 0) then
    BitBtn1Click(Self);

  pnlFavoriteReport.visible := False;
end;

procedure TEDIT_CL_BASE.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  inherited;
end;

end.
