unit EvDBVersion;

interface

uses Windows, Types, isTypes, isBaseClasses, EvConsts, isUtils, isBasicUtils;

type
  TevAppDBVerInfo = record
    AppMinVersion:    TisVersionInfo;
    SystemDBVersion:  TisVersionInfo;
    BureauDBVersion:  TisVersionInfo;
    ClientDBVersion:  TisVersionInfo;
    TempDBVersion:    TisVersionInfo;
    ServiceDBVersion: TisVersionInfo;
  end;

  TevAppDBVerInfos = array of TevAppDBVerInfo;

  IevAppDBVersions = interface
  ['{FD9E57E6-D509-4546-81C7-E2A8B8828904}']
    function GetCompatibilityInfo(const aAppVersion: String): TevAppDBVerInfo;
    function GetAllInfo: TevAppDBVerInfos;
    function Current: TevAppDBVerInfo;
  end;

  function AppDBVersions: IevAppDBVersions;

implementation

var
  Versions: IevAppDBVersions;

type
  TevAppDBVersions = class(TisInterfacedObject, IevAppDBVersions)
  private
    FVersions: IisParamsCollection;
    FVersionsProcessed: TevAppDBVerInfos;
    procedure AddVersions;
    procedure AddVerInfo(const aAppVer, aSystemDBVersion, aBureauDBVersion, aClientDBVersion, aTempDBVersion, aServiceDBVersion: String);
  protected
    procedure DoOnConstruction; override;

    function  GetCompatibilityInfo(const aAppVersion: String): TevAppDBVerInfo;
    function  GetAllInfo: TevAppDBVerInfos;
    function  Current: TevAppDBVerInfo;
  end;


function AppDBVersions: IevAppDBVersions;
begin
  Result := Versions;
end;

{ TevAppDBVersions }

procedure TevAppDBVersions.AddVerInfo(const aAppVer, aSystemDBVersion,
  aBureauDBVersion, aClientDBVersion, aTempDBVersion, aServiceDBVersion: String);
var
  item: IisListOfValues;

  procedure Add(aName, aValue: String);
  begin
    if aValue = '' then
      aValue := FVersions[FVersions.Count - 2].Value[aName];

    item.AddValue(aName, aValue);
  end;

begin
  item := FVersions.AddParams(aAppVer);

  Add(DB_System, aSystemDBVersion);
  Add(DB_S_Bureau, aBureauDBVersion);
  Add(DB_Cl_Base, aClientDBVersion);
  Add(DB_TempTables, aTempDBVersion);
  Add(DB_Service, aServiceDBVersion);
end;

procedure TevAppDBVersions.AddVersions;
begin
  // Add a new record whenever any Evolution database gets pached
  // Note: Build number of app item is meaningless. It always should be 1.

  //            app          system       bureau      client        temp       service
  AddVerInfo('15.15.0.1', '15.0.0.7',  '15.0.0.5', '15.0.0.15', '15.0.0.2', '15.0.0.3');
  AddVerInfo('15.37.0.1', '15.0.0.8',  '15.0.0.6', '15.0.0.16', '',         ''        );
  AddVerInfo('16.0.0.1',  '16.0.0.0',  '16.0.0.0', '16.0.0.0',  '16.0.0.0', '');
  AddVerInfo('16.1.0.1',  '16.0.0.0',  '16.0.0.0', '16.0.0.1',  '16.0.0.0', '');
  AddVerInfo('16.24.0.1', '16.0.0.0',  '16.0.0.1', '16.0.0.2',  '16.0.0.0', '');
  AddVerInfo('16.35.0.1', '16.0.0.1',  '16.0.0.2', '16.0.0.3',  '16.0.0.1', '');
  AddVerInfo('16.45.0.1', '16.0.0.2',  '16.0.0.2', '16.0.0.3',  '16.0.0.1', '');
  AddVerInfo('16.48.0.1', '16.0.0.2',  '16.0.0.3', '16.0.0.3',  '16.0.0.1', '');
  AddVerInfo('17.0.0.1',  '17.0.0.0',  '17.0.0.0', '17.0.0.0',  '17.0.0.0', '');
end;

function TevAppDBVersions.Current: TevAppDBVerInfo;
begin
  Result := FVersionsProcessed[High(FVersionsProcessed)];
end;

procedure TevAppDBVersions.DoOnConstruction;
var
  i: Integer;
begin
  inherited;
  FVersions := TisParamsCollection.Create;

  AddVersions;

  SetLength(FVersionsProcessed, FVersions.Count);
  for i := 0 to FVersions.Count - 1 do
  begin
    FVersionsProcessed[i].AppMinVersion := StrToVersionRec(FVersions.ParamName(i));
    FVersionsProcessed[i].SystemDBVersion := StrToVersionRec(FVersions[i].Value[DB_System]);
    FVersionsProcessed[i].BureauDBVersion := StrToVersionRec(FVersions[i].Value[DB_S_Bureau]);
    FVersionsProcessed[i].ClientDBVersion := StrToVersionRec(FVersions[i].Value[DB_Cl_Base]);
    FVersionsProcessed[i].TempDBVersion := StrToVersionRec(FVersions[i].Value[DB_TempTables]);
    FVersionsProcessed[i].ServiceDBVersion := StrToVersionRec(FVersions[i].Value[DB_Service]);
  end;
  
  FVersions := nil;
end;


function TevAppDBVersions.GetAllInfo: TevAppDBVerInfos;
begin
  Result := FVersionsProcessed;
end;

function TevAppDBVersions.GetCompatibilityInfo(const aAppVersion: String): TevAppDBVerInfo;
var
  i: Integer;
  appVer: TisVersionInfo;
begin
  appVer := StrToVersionRec(aAppVersion);

  if CompareVersions(appVer, StrToVersionRec(AppVersion)) = EqualsValue then
  begin
    // This is for running from IDE. AppVersion is not set properly, so we need to take current information. 
    Result := Current;
    Exit;
  end;

  ZeroMemory(@Result, SizeOf(Result));
  for i := Low(FVersionsProcessed) to High(FVersionsProcessed) do
  begin
    if CompareVersions(appVer, FVersionsProcessed[i].AppMinVersion) <> GreaterThanValue then
    begin
      if (i = High(FVersionsProcessed)) or (CompareVersions(appVer, FVersionsProcessed[i + 1].AppMinVersion) = LessThanValue) then
      begin
        Result := FVersionsProcessed[i];
        break;
      end;
    end;
  end;
end;

initialization
  Versions := TevAppDBVersions.Create;

finalization
  Versions := nil;

end.
