unit EvAuditViewFrm;

interface

uses Windows, Forms, DB, Variants, Types, SysUtils, DateUtils,
     kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
     EvDataAccessComponents, Wwdatsrc, ISBasicClasses, EvUIComponents,
     Controls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, ComCtrls, Classes,
     ExtCtrls, evDataSet, isDataSet, EvCommonInterfaces, evContext, EvUtils,
     EvConsts, Buttons, isBasicUtils, EvStreamUtils, wwdblook,
     LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
     SDataStructure, isTypes, EvTypes;

type
  TevAuditView = class(TForm)
    pnlToolBar: TevPanel;
    pnlFilter: TevPanel;
    evLabel1: TevLabel;
    dteBeginDate: TevDateTimePicker;
    btnApply: TevButton;
    fpAuditForm: TisUIFashionPanel;
    procedure FormCreate(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dteBeginDateChange(Sender: TObject);
    procedure dteBeginDateKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FCurrentFrame: TFrame;
    FCurrentDBType: TevDBType;
    FCurrentTable: String;
    FCurrentField: String;
    FCurrentNbr: Integer;
    FCurrentDate: TDate;
    FFirstTimeFocusCtrl: TWinControl;
    procedure SyncApplyButton;
  public
    class procedure ShowDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TisDate);
    class procedure ShowTableAudit(const ATable: String; const ABeginDate: TisDate);
    class procedure ShowRecordAudit(const ATable: String; const ANbr: Integer; const ABeginDate: TisDate);
    class procedure ShowFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TisDate);
  end;

implementation

uses EvAuditDatabaseFrm, EvAuditTableFrm, EvAuditRecordFrm, EvAuditVerFieldViewFrm;

{$R *.DFM}

{ TevAuditView }

class procedure TevAuditView.ShowFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TisDate);
var
  Frm: TevAuditView;
begin
  Frm := TevAuditView.Create(Application);
  try
    Frm.fpAuditForm.Title := 'Field Audit Information';
    Frm.FCurrentTable := ATable;
    Frm.FCurrentField := AField;
    Frm.FCurrentNbr := ANbr;

    if ABeginDate > EmptyDate then
      Frm.dteBeginDate.Date := ABeginDate;

    if GetddTableClassByName(ATable).IsVersionedField(AField) then
    begin
      Frm.FCurrentFrame := TevAuditVerFieldView.Create(Frm);
      TevAuditVerFieldView(Frm.FCurrentFrame).Init(Frm.FCurrentTable, Frm.FCurrentField, Frm.FCurrentNbr);
      Frm.FFirstTimeFocusCtrl := TevAuditVerFieldView(Frm.FCurrentFrame).grTimeLine;
      Frm.pnlToolBar.Visible := False;
      Frm.FCurrentFrame.Align := alClient;
    end
    else
    begin
      Frm.FCurrentFrame := TevAuditRecord.Create(Frm);
      Frm.FFirstTimeFocusCtrl := TevAuditRecord(Frm.FCurrentFrame).grVersionAudit;
    end;

    Frm.Caption := Format('Audit of field %s.%s (nbr=%d)', [Frm.FCurrentTable, Frm.FCurrentField, Frm.FCurrentNbr]);

    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

class procedure TevAuditView.ShowRecordAudit(const ATable: String; const ANbr: Integer; const ABeginDate: TisDate);
var
  Frm: TevAuditView;
begin

  Frm := TevAuditView.Create(Application);
  try
    Frm.fpAuditForm.Title := 'Record Audit Information';
    Frm.FCurrentTable := ATable;
    Frm.FCurrentField := '';
    Frm.FCurrentNbr := ANbr;

    if ABeginDate > EmptyDate then
      Frm.dteBeginDate.Date := ABeginDate;
    
    Frm.FCurrentFrame := TevAuditRecord.Create(Frm);
    Frm.Caption := Format('Audit of record %s (nbr=%d)', [Frm.FCurrentTable, Frm.FCurrentNbr]);
    Frm.FFirstTimeFocusCtrl := TevAuditRecord(Frm.FCurrentFrame).grVersionAudit;
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

procedure TevAuditView.FormCreate(Sender: TObject);
begin
  dteBeginDate.Date := IncMonth(SysDate, -12);
end;

procedure TevAuditView.btnApplyClick(Sender: TObject);
begin
  if FCurrentFrame is TevAuditRecord then
    TevAuditRecord(FCurrentFrame).Init(FCurrentTable, FCurrentField, FCurrentNbr, Trunc(DateOf(dteBeginDate.Date)))
  else if FCurrentFrame is TevAuditTable then
    TevAuditTable(FCurrentFrame).Init(FCurrentTable, Trunc(DateOf(dteBeginDate.Date)))
  else if FCurrentFrame is TevAuditDatabase then
    TevAuditDatabase(FCurrentFrame).Init(FCurrentDBType, Trunc(DateOf(dteBeginDate.Date)));

  FCurrentDate := DateOf(dteBeginDate.Date);
  SyncApplyButton;
end;

procedure TevAuditView.FormShow(Sender: TObject);
begin
  FCurrentFrame.Parent := fpAuditForm;

  if pnlToolBar.Visible then
  begin
    FCurrentFrame.Align := alClient;
    FCurrentFrame.ParentColor := True;
    btnApply.Click;
  end;

  ActiveControl := FFirstTimeFocusCtrl;

end;

procedure TevAuditView.dteBeginDateChange(Sender: TObject);
begin
  SyncApplyButton;
end;

procedure TevAuditView.SyncApplyButton;
begin
  btnApply.Enabled := FCurrentDate <> DateOf(dteBeginDate.Date);
end;

procedure TevAuditView.dteBeginDateKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnApply.Click;
end;

procedure TevAuditView.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

class procedure TevAuditView.ShowTableAudit(const ATable: String; const ABeginDate: TisDate);
var
  Frm: TevAuditView;
begin

  Frm := TevAuditView.Create(Application);
  try
    Frm.fpAuditForm.Title := 'Table Audit Information';
    Frm.FCurrentTable := ATable;
    Frm.FCurrentFrame := TevAuditTable.Create(Frm);
    Frm.Caption := Format('Audit of table %s', [Frm.FCurrentTable]);
    Frm.FFirstTimeFocusCtrl := TevAuditTable(Frm.FCurrentFrame).grVersionAudit;
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

class procedure TevAuditView.ShowDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TisDate);
var
  Frm: TevAuditView;
  s: String;
begin

  Frm := TevAuditView.Create(Application);
  try
    Frm.fpAuditForm.Title := 'Database Audit Information';
    Frm.FCurrentDBType := ADBType;
    Frm.FCurrentFrame := TevAuditDatabase.Create(Frm);

    s := GetDBNameByType(Frm.FCurrentDBType);
    if Frm.FCurrentDBType = dbtClient then
      s := s + IntToStr(ctx_DataAccess.ClientID);
    Frm.Caption := Format('Audit of database %s', [AnsiUpperCase(s)]);
    
    Frm.FFirstTimeFocusCtrl := TevAuditDatabase(Frm.FCurrentFrame).grVersionAudit;
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

end.
