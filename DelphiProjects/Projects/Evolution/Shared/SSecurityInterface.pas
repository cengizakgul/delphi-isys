// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSecurityInterface;

interface

uses Classes, EvConsts;

type
  ISecurityAtom = interface;
  ISecurityElement = interface;
  TLinkType = (ltEqual, ltMinLimit, ltMaxLimit, ltKeepLowest, ltNone);

  TAtomElementRelation = record
    AtomContext: ShortString;
    ElementState: ShortString;
    Atom2Element, Element2Atom: TLinkType;
  end;
  TAtomElementRelationsArray = array of TAtomElementRelation;

  TElementState = record
    Tag: ShortString;
    Priority: Integer;
  end;
  TElementStatesArray = array of TElementState;

  TAtomContextRecord = record
    Tag, Caption, IconName, SelectedIconName: ShortString;
    Description: ShortString;
    Priority: Integer;
    //Icon: TIcon;
  end;
  TAtomContextRecordArray = array of TAtomContextRecord;

  TGetAtomElementRelationsArrayEvent = procedure(const Sender: ISecurityElement; const Atom: ISecurityAtom; var Relations: TAtomElementRelationsArray) of object;
  TGetElementStatesArrayEvent = procedure(const Sender: ISecurityElement; var States: TElementStatesArray) of object;
  TSetSecurityStateEvent = procedure(const Sender: TComponent) of object;
  TGetAtomContextRecordArrayEvent = procedure(const Sender: ISecurityAtom; var ContextRecords: TAtomContextRecordArray) of object;

  ISecurityAtom = interface
    ['{7505EEF6-96E9-4C1F-A82D-5B0DA85CBEDC}']
    function Component: TComponent;
    function IsAtom: Boolean;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetCaption: ShortString;
    function SGetPossibleSecurityContexts: TAtomContextRecordArray;
  end;

  ISecurityElement = interface
    ['{C0BA4CE7-ED46-41C7-A92D-1663B39C8D27}']
    function Component: TComponent;
    function Atom: ISecurityAtom;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetStates: TElementStatesArray;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray;
    procedure SSetCurrentSecurityState(const State: ShortString);
    function SecurityState: ShortString;
  end;
  
  ISchemeBuilder = interface
    ['{1974C1DD-373F-4E10-BF3F-CD77655DC8AD}']
    procedure AddAtom(const Atom: ISecurityAtom);
    procedure AddElement(const Element: ISecurityElement);
  end;

const
  secScreenOff: TAtomContextRecord = (Tag: ctDisabled; Caption: 'Disabled'; IconName: 'TREESCREENSDISABLED';
    Description: 'Screen'; Priority: 0);
  secScreenOn: TAtomContextRecord = (Tag: ctEnabled; Caption: 'Enabled'; IconName: 'TREESCREENSENABLED';
    Description: 'Screen'; Priority: 255);
  secScreenRO: TAtomContextRecord = (Tag: ctReadOnly; Caption: 'ReadOnly'; IconName: 'TREESCREENSREADONLY';
    Description: 'Screen'; Priority: 128);
  secMenuOff: TAtomContextRecord = (Tag: ctDisabled; Caption: 'Disabled'; IconName: 'TREEMENUSDISABLED';
    Description: 'Menu'; Priority: 0);
  secMenuOn: TAtomContextRecord = (Tag: ctEnabled; Caption: 'Enabled'; IconName: 'TREEMENUSENABLED';
    Description: 'Menu'; Priority: 255);
  secFunctionOff: TAtomContextRecord = (Tag: ctDisabled; Caption: 'Disabled'; IconName: 'TREEFUNCTIONSDISABLED';
    Description: 'Function'; Priority: 0);
  secFunctionOn: TAtomContextRecord = (Tag: ctEnabled; Caption: 'Enabled'; IconName: 'TREEFUNCTIONSENABLED';
    Description: 'Function'; Priority: 255);

procedure AddContext(const r: TAtomContextRecord; var ar: TAtomContextRecordArray);

implementation

procedure AddContext(const r: TAtomContextRecord; var ar: TAtomContextRecordArray);
begin
  SetLength(ar, Length(ar)+ 1);
  ar[High(ar)] := r;
end;

end.
