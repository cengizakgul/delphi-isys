unit EvModuleRegister;

interface

uses Classes, SysUtils, Windows, isBaseClasses, EvCommonInterfaces, isBasicUtils;

type
  TevModule = class(TisNamedObject, IevModuleDescriptor)
  private
    FType: TGUID;
    FFactoryEntry: PCreateModuleInstance;
    FExtModuleHandle: THandle;
  protected
    function  ModuleName: string;
    function  InstanceType: TGUID;
  public
    destructor Destroy; override;
  end;


  TevModuleRegister = class(TisCollection, IevModuleRegister)
  private
    function   FindModuleByType(const AModuleType: TGUID): IevModuleDescriptor;
    function   FindModuleByName(const AModuleName: String): IevModuleDescriptor;
    function   GetItem(const AIndex: Integer): IevModuleDescriptor;
  protected
    procedure  DoOnConstruction; override;
    procedure  RegisterModule(const AFactoryEntry: PCreateModuleInstance; const AType: TGUID; const AModuleName: String);
    function   CreateModuleInstance(const AModuleType: TGUID; const NoError: Boolean = False): IInterface; overload;
    function   CreateModuleInstance(const AModuleName: String; const NoError: Boolean = False): IInterface; overload;
    function   LoadExternalModule(const AModuleFileName: String): Boolean;
    function   GetModules: IisList;
    function   GetModuleInfo(const AModuleType: TGUID): IevModuleDescriptor; overload;
    function   GetModuleInfo(const AModuleName: String): IevModuleDescriptor; overload;
    function   IsProxyModule(const AModuleInstance: IInterface): Boolean;    
  public
    destructor Destroy; override;
  end;


implementation

{ TevModuleRegister }

function TevModuleRegister.CreateModuleInstance(const AModuleType: TGUID; const NoError: Boolean = False): IInterface;
var
  Module: IevModuleDescriptor;
begin
  Module := FindModuleByType(AModuleType);
  CheckCondition(NoError or Assigned(Module), 'Module ' + GUIDToString(AModuleType) + ' is not registered');

  if Assigned(Module) then
    Result := TevModule((Module as IisInterfacedObject).GetImplementation).FFactoryEntry
  else
    Result := nil;
end;

function TevModuleRegister.CreateModuleInstance(const AModuleName: String; const NoError: Boolean = False): IInterface;
var
  Module: IevModuleDescriptor;
begin
  Module := FindModuleByName(AModuleName);
  CheckCondition(NoError or Assigned(Module), 'Module "' + AModuleName + '" is not registered');

  if Assigned(Module) then
    Result := TevModule((Module as IisInterfacedObject).GetImplementation).FFactoryEntry
  else
    Result := nil;  
end;


destructor TevModuleRegister.Destroy;
begin
  inherited;
end;

function TevModuleRegister.FindModuleByType(const AModuleType: TGUID): IevModuleDescriptor;
var
  i: Integer;
begin
  Result := nil;

  Lock;
  try
    for i := 0 to ChildCount - 1 do
      if IsEqualGUID(GetItem(i).InstanceType, AModuleType) then
      begin
        Result := GetItem(i);
        break;
      end;
  finally
    Unlock;
  end;
end;

function TevModuleRegister.FindModuleByName(const AModuleName: String): IevModuleDescriptor;
var
  i: Integer;
begin
  Result := nil;

  Lock;
  try
    for i := 0 to ChildCount - 1 do
      if AnsiSameText(GetItem(i).ModuleName, AModuleName) then
      begin
        Result := GetItem(i);
        break;
      end;
  finally
    Unlock;
  end;
end;


function TevModuleRegister.GetItem(const AIndex: Integer): IevModuleDescriptor;
begin
  Result := GetChild(AIndex) as IevModuleDescriptor;
end;

function TevModuleRegister.GetModules: IisList;
var
  i: Integer;
begin
  Lock;
  try
    Result := TisList.Create;
    for i := 0 to ChildCount - 1 do
      Result.Add(GetItem(i));
  finally
    Unlock;
  end;
end;

{$IFDEF NOPACKAGES}
function TevModuleRegister.LoadExternalModule(const AModuleFileName: String): Boolean;
begin
  Result := True;
end;
{$ELSE}
function TevModuleRegister.LoadExternalModule(const AModuleFileName: String): Boolean;
var
  FullFileName: String;
  ExtModule: THandle;
  ExpectedIndex: Integer;
begin
    Result := False;
    FullFileName := AModuleFileName;
    if ExtractFilePath(FullFileName) = '' then
      FullFileName := ChangeFileExt(AppDir + FullFileName, '.bpl');

    ExpectedIndex := ChildCount;
    ExtModule := LoadPackage(FullFileName);

    if ExtModule <> 0 then
      if ExpectedIndex = ChildCount then
        // Incompatible module;
        UnloadPackage(ExtModule)
      else
      begin
        TevModule(GetChild(ExpectedIndex).GetImplementation).FExtModuleHandle := ExtModule;
        Result := True;
      end;
end;
{$ENDIF}

procedure TevModuleRegister.RegisterModule(const AFactoryEntry: PCreateModuleInstance; const AType: TGUID; const AModuleName: String);
var
  Module: IisNamedObject;
begin
  CheckCondition(FindModuleByType(AType) = nil, 'Module ' + GUIDToString(AType) + ' is already registered');

  Module := TevModule.Create;
  Module.Name := AModuleName;
  Module.Owner := Self;
  TevModule(Module.GetImplementation).FFactoryEntry := AFactoryEntry;
  TevModule(Module.GetImplementation).FType := AType;
end;

function TevModuleRegister.GetModuleInfo(const AModuleType: TGUID): IevModuleDescriptor;
begin
  Result := FindModuleByType(AModuleType);
end;

function TevModuleRegister.GetModuleInfo(const AModuleName: String): IevModuleDescriptor;
begin
  Result := FindModuleByName(AModuleName);
end;

function TevModuleRegister.IsProxyModule(const AModuleInstance: IInterface): Boolean;
begin
  Result := Supports(AModuleInstance, IevProxyModule);
end;

procedure TevModuleRegister.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

{ TevModule }

destructor TevModule.Destroy;
begin
  if FExtModuleHandle <> 0 then
    UnloadPackage(FExtModuleHandle);
  inherited;
end;

function TevModule.InstanceType: TGUID;
begin
  Result := FType;
end;

function TevModule.ModuleName: string;
begin
  Result := GetName;
end;

end.
