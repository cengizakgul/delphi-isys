inherited DBDTChoiceQuery: TDBDTChoiceQuery
  Left = 466
  Width = 590
  Height = 457
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 380
    Width = 574
    inherited btnYes: TevBitBtn
      Left = 318
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 444
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 207
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 574
    Height = 380
    Selected.Strings = (
      'DIVISION_NAME'#9'20'#9'Division Name'
      'BRANCH_NAME'#9'20'#9'Branch Name'
      'DEPARTMENT_NAME'#9'20'#9'Department Name'
      'TEAM_NAME'#9'20'#9'Team Name'
      'DBDT_CUSTOM_NUMBERS'#9'30'#9'Custom D/B/D/T Numbers')
    IniAttributes.SectionName = 'TDBDTChoiceQuery\dgChoiceList'
  end
  inherited dsChoiceList: TevDataSource
    DataSet = nil
    Left = 192
    Top = 40
  end
  inherited cdsChoiceList: TevClientDataSet
    Left = 344
    Top = 80
    object cdsChoiceListDIVISION_NAME: TStringField
      DisplayLabel = 'Division Name'
      DisplayWidth = 20
      FieldName = 'DIVISION_NAME'
      Size = 40
    end
    object cdsChoiceListBRANCH_NAME: TStringField
      DisplayLabel = 'Branch Name'
      DisplayWidth = 20
      FieldName = 'BRANCH_NAME'
      Size = 40
    end
    object cdsChoiceListDEPARTMENT_NAME: TStringField
      DisplayLabel = 'Department Name'
      DisplayWidth = 20
      FieldName = 'DEPARTMENT_NAME'
      Size = 40
    end
    object cdsChoiceListTEAM_NAME: TStringField
      DisplayLabel = 'Team Name'
      DisplayWidth = 20
      FieldName = 'TEAM_NAME'
      Size = 40
    end
    object cdsChoiceListDBDT_CUSTOM_NUMBERS: TStringField
      DisplayLabel = 'Custom D/B/D/T Numbers'
      DisplayWidth = 30
      FieldName = 'DBDT_CUSTOM_NUMBERS'
      Size = 80
    end
    object cdsChoiceListBRANCH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BRANCH_NBR'
      Visible = False
    end
    object cdsChoiceListDEPARTMENT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEPARTMENT_NBR'
      Visible = False
    end
    object cdsChoiceListTEAM_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TEAM_NBR'
      Visible = False
    end
    object cdsChoiceListDIVISION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DIVISION_NBR'
      Visible = False
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 184
    Top = 168
  end
end
