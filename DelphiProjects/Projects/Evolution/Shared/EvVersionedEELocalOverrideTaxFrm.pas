unit EvVersionedEELocalOverrideTaxFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, isUIwwDBComboBox, SFieldCodeValues;

type
  TEvVersionedEELocalOverrideTax = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    pnlBottom: TevPanel;
    lblTax_Type: TevLabel;
    lblTax_Type_Value: TevLabel;
    EvCombo_Tax_Type: TevDBComboBox;
    EvCombo_Tax_Type_Value: TevDBEdit;
    procedure FormShow(Sender: TObject);
    procedure EvCombo_Tax_TypeChange(Sender: TObject);
  protected
    function ValidInput: Boolean; override;
  end;

implementation

{$R *.dfm}


{ TEvVersionedEELocal }

procedure TEvVersionedEELocalOverrideTax.FormShow(Sender: TObject);
begin
  inherited;

  Fields[0].AddValue('Title', lblTax_Type.Caption);
  Fields[0].AddValue('Width', 20);
  EvCombo_Tax_Type.DataField := Fields.ParamName(0);
  EvCombo_Tax_Type.Items.Text := OverrideValueType_ComboChoices;
  lblTax_Type.Caption := Iff(Fields[0].Value['Required'], '~', '') + lblTax_Type.Caption;

  Fields[1].AddValue('Title', lblTax_Type_Value.Caption);
  Fields[1].AddValue('Width', 20);
  EvCombo_Tax_Type_Value.DataField := Fields.ParamName(1);
end;

function TEvVersionedEELocalOverrideTax.ValidInput: Boolean;
begin
  Result := inherited ValidInput;
  if Result then
    if EvCombo_Tax_Type.Value <> OVERRIDE_VALUE_TYPE_NONE then
      Result := not Data.FieldByName(EvCombo_Tax_Type_Value.DataField).IsNull;
end;

procedure TEvVersionedEELocalOverrideTax.EvCombo_Tax_TypeChange(Sender: TObject);
var
  s: String;
begin
  UpdateFieldOnChange(Sender);
  
  s := 'Override Tax Value';
  if EvCombo_Tax_Type.Value <> OVERRIDE_VALUE_TYPE_NONE then
    s := '~' + s;
  lblTax_Type_Value.Caption := s;

  SyncControlsState;
end;

end.

