// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChoiceFromListQueryEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,SPD_ChoiceFromListQuery,
  Db, Wwdatsrc,  Grids, Wwdbigrd, Wwdbgrid, StdCtrls, Buttons,
  ExtCtrls, ActnList, EvUtils, ISBasicClasses, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TChoiceFromListQueryEx = class(TChoiceFromListQuery)
    btnAll: TevBitBtn;
    ActionList1: TActionList;
    HandleSelected: TAction;
    HandleAll: TAction;
    procedure HandleSelectedUpdate(Sender: TObject);
    procedure HandleAllUpdate(Sender: TObject);
    procedure HandleAllExecute(Sender: TObject);
    procedure HandleSelectedExecute(Sender: TObject);
  private
    { Private declarations }
    FConfirmAllMessage: string;
  public
    property ConfirmAllMessage: string read FConfirmAllMessage write FConfirmAllMessage;
    { Public declarations }
  end;


implementation

{$R *.DFM}

procedure TChoiceFromListQueryEx.HandleSelectedUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := dgChoiceList.SelectedList.Count > 0;
end;

procedure TChoiceFromListQueryEx.HandleAllUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := assigned(dgChoiceList.DataSource) and
                                 assigned(dgChoiceList.DataSource.DataSet) and
                                 (dgChoiceList.DataSource.DataSet.RecordCount > 0);
end;

procedure TChoiceFromListQueryEx.HandleAllExecute(Sender: TObject);
begin
  if ( trim(FConfirmAllMessage) = '' ) or
    ( EvMessage(FConfirmAllMessage, mtConfirmation, [mbNo, mbYes]) = mrYes )then
  begin
    dgChoiceList.SelectAll;
    ModalResult := btnYes.ModalResult;
  end;  
end;

procedure TChoiceFromListQueryEx.HandleSelectedExecute(Sender: TObject);
begin
  ModalResult := btnYes.ModalResult;
end;

end.
