unit EvControllerInterfaces;

interface

uses isBaseClasses, EvStreamUtils, isLogFile;

type
  IevAppController = interface
  ['{A811732F-E7AC-4951-80DA-5C159E9FB965}']
    function  GetPort: String;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
    property  Port: String read GetPort;
  end;


  IevRPControl = interface
  ['{1FEBD749-FE2B-4E40-9922-2CB7F54BD021}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetLoadStatus: IisListOfValues;
    function  GetExecStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;


  IevRBControl = interface
  ['{F3C5E265-6C64-4682-BA36-B49D66891637}']
    function  GetMachineInfo: IisListOfValues;
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetDomainList: IisListOfValues;
    function  GetLicenseInfo(const ADomainName: String): IisListOfValues;
    procedure SetLicenseInfo(const ADomainName: String; const ASerialNumber, ARegistrationCode: String);
    function  GetFolders(const ADomainName: String): IisListOfValues;
    procedure SetFolders(const ADomainName: String; const AFolders: IisListOfValues);
    procedure GetEMailInfo(var AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetEMailInfo(const AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetDatabasePasswords(const AParams: IisListOfValues);
    function  GetDatabasePasswords: IisListOfValues;
    procedure SetDatabaseParams(const ADomainName: String; const AParams: IisListOfValues);
    function  GetDatabaseParams(const ADomainName: String): IisListOfValues;
    function  GetRPPoolInfo: IisParamsCollection;
    procedure SetRPPoolInfo(const AInfo: IisParamsCollection);
    function  GetStackInfo: String;
    procedure GetSystemAccount(const ADomainName: String; out AUser: String);
    procedure SetSystemAccount(const ADomainName, AUser: String);
    procedure GetAdminAccount(const ADomainName: String; out AUser: String);
    procedure SetAdminAccount(const ADomainName, AUser: String);
    function  GetRPPoolStatus: IisParamsCollection;
    function  UsersPoolStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    function  GetBBDiagnosticCheck: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    function  GetPrintersInfo: IisListOfValues;
    procedure SetPrintersInfo(const APrintersInfo: IisListOfValues);
    procedure SendMessage(const aText: String; const aToUsers: String = '');
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    function  GetSystemStatus: IisParamsCollection;
    function  GetAUS: String;
    procedure SetAUS(const AValue: String);
    function  GetInternalRR: IisStringList;
    procedure SetInternalRR(const ARRList: IisStringList);
    function  GetInternalUserRR(const ADomainName: String): Boolean;
    procedure SetInternalUserRR(const ADomainName: String; const AValue: Boolean);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetKnownADRServices(const AServices: IisListOfValues);
    procedure ResetAdminPassword(const ADomainName: String; const APassword: String);
  end;


  IevRRControl = interface
  ['{C46B6A7B-E17D-40DF-AD95-FCEE93B6C1F1}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    function  GetConnectionStatus: IisParamsCollection;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetSessionsStatus: IisParamsCollection;
    function  GetStackInfo: String;
  end;


  IevAAControl = interface
  ['{FECEB1D9-4AA8-4B96-90D5-F0F76F5427F2}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;


  IevACControl = interface
  ['{2D84B53D-7B72-43CE-8457-A7D0760AF4F1}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
    function  GetStatus: IisParamsCollection;
    function  GetDomainStatus(const ADomainName: String): IisListOfValues;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetTransRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;    
    function  GetDBList(const ADomainName: String): IisStringList;
    procedure SyncDBList(const ADomainName: String);
    procedure CheckDBTransactions(const ADomainName: String; const ADataBase: String);
    procedure CheckDB(const ADomainName: String; const ADataBase: String);
    procedure FullDBCopy(const ADomainName: String; const ADataBase: String);
    procedure CheckDBData(const ADomainName: String; const ADataBase: String);
  end;


  IevASControl = interface
  ['{40ACC9BC-C3D8-408B-802A-247D100B3125}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
    function  GetStatus: IisParamsCollection;
    function  GetDomainStatus(const ADomainName: String): IisListOfValues;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetTransRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetClientDBList(const ADomainName: String): IisStringList;
    procedure CheckDB(const ADomainName: String; const ADataBase: String);
    procedure FullDBCopy(const ADomainName: String; const ADataBase: String);
    procedure CheckDBData(const ADomainName: String; const ADataBase: String);
    procedure CheckDBTransactions(const ADomainName: String; const ADataBase: String);
    procedure RebuildTT(const ADomainName: String; const ADataBase: String);        
  end;


  IevDBMControl = interface
  ['{F8041068-B1EA-49BF-9DD6-61D9E3DD7A0D}']
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetStatus: IisListOfValues;
    function  GetExecutingItems: IisParamsCollection;
  end;

implementation

end.

