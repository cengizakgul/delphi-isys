// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_HideRecordHelper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, Buttons,  DB, Wwdatsrc,
  ISBasicClasses, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TCanHideRecordEvent = function( DataSet: TevClientDataSet): boolean of object;
  TCanUnhideRecordEvent = function( DataSet: TevClientDataSet): boolean of object;

  THideRecordHelper = class(TFrame)
    evActionList1: TevActionList;
    butnNext: TevBitBtn;
    HideCurRecord: TAction;
    dsHelper: TevDataSource;
    evBitBtn1: TevBitBtn;
    UnhideCurRecord: TAction;
    procedure HideCurRecordUpdate(Sender: TObject);
    procedure HideCurRecordExecute(Sender: TObject);
    procedure dsHelperDataChange(Sender: TObject; Field: TField);
    procedure UnhideCurRecordUpdate(Sender: TObject);
    procedure UnhideCurRecordExecute(Sender: TObject);
  private
    FDataSet: TEvClientDataSet;
    FIndicatorFieldNAme: string;
    FScheduledEEFlag : boolean;

    FOnCanHideRecord: TCanHideRecordEvent;
    FOnCheckBeforeHidingRecord: TCanHideRecordEvent;
    FOnMayHideRecord: TCanHideRecordEvent;
    FOnCanUnhideRecord: TCanUnhideRecordEvent;
    FOnMayUnhideRecord: TCanUnhideRecordEvent;
    FOnCheckBeforeUnhidingRecord: TCanUnhideRecordEvent;
    function GetScheduledEEFlagCondition (IsHide:boolean) : boolean;
  public
    sHideAction: string;
    sUnhideAction: string;
    constructor Create(AOwner: TComponent); override;
    procedure SetDataSet( ds: TEvClientDataSet; aFieldBName: string  );

    function CanHideAny( DataSet: TevClientDataSet): boolean;
    property OnCanHideRecord: TCanHideRecordEvent read FOnCanHideRecord write FOnCanHideRecord;
    property OnMayHideRecord: TCanHideRecordEvent read FOnMayHideRecord write FOnMayHideRecord;
    property OnCheckBeforeHidingRecord: TCanHideRecordEvent read FOnCheckBeforeHidingRecord write FOnCheckBeforeHidingRecord;

    function CanUnhideAny( DataSet: TevClientDataSet): boolean;
    property OnCanUnhideRecord: TCanUnhideRecordEvent read FOnCanUnhideRecord write FOnCanUnhideRecord ;
    property OnMayUnhideRecord: TCanUnhideRecordEvent read FOnMayUnhideRecord write FOnMayUnhideRecord ;
    property OnCheckBeforeUnhidingRecord: TCanUnhideRecordEvent read FOnCheckBeforeUnhidingRecord write FOnCheckBeforeUnhidingRecord;
    property ScheduledEEFlag : boolean read FScheduledEEFlag write FScheduledEEFlag;
  end;


implementation

uses
  evutils, sPackageEntry, EvConsts;

{$R *.dfm}

{ THideRecordHelper }

function DigOutPackage( c: TComponent ): TFramePackageTmpl;
begin
  while assigned(c) and not (c is TFramePackageTmpl) do
    c := c.Owner;
  Result := c as TFramePackageTmpl;
  Assert( assigned(Result) );
end;

procedure THideRecordHelper.SetDataSet(ds: TEvClientDataSet; aFieldBName: string);
begin
  FIndicatorFieldName := aFieldBName;
  FDataSet := ds;
  dsHelper.DataSet := ds;
end;

procedure THideRecordHelper.HideCurRecordUpdate(Sender: TObject);
var
  bCanBeHided: boolean;
  bMayBeHided: boolean;
begin
  bCanBeHided := DSIsActive( FDataSet )
          and (FIndicatorFieldName <> '')
          and (FDataSet.RecordCount > 0)
          and GetScheduledEEFlagCondition(True)
          and assigned(FOnCanHideRecord)
          and FOnCanHideRecord( FDataSet );
  (Sender as TCustomAction).Visible := bCanBeHided;
  bMayBeHided := bCanBeHided;
  if bMayBeHided then
    if assigned(FOnMayHideRecord) then
      bMayBeHided := FOnMayHideRecord(FDataSet)
    else
      bMayBeHided := FDataSet.State = dsBrowse;
  (Sender as TCustomAction).Enabled := bMayBeHided;
end;

procedure THideRecordHelper.HideCurRecordExecute(Sender: TObject);
var
 wasEditing: boolean;
 s:string;
begin
  if not assigned(FOnCheckBeforeHidingRecord) or FOnCheckBeforeHidingRecord(FDataSet) then
  begin
    if FScheduledEEFlag then
      s:='Are you sure you want to make this Scheduled E/D inactive?'
    else
      s:= sHideAction + ' this record.'+#13#10+'Are you sure? ';

    if (EvMessage( s, mtConfirmation, [mbOk,mbCancel] ) = mrOk) then
    begin
      wasEditing := FDataSet.State in [dsInsert, dsEdit];
      if not wasEditing then
        FDataSet.Edit;
      FDataSet[FIndicatorFieldNAme] := GROUP_BOX_NO;
      if not wasEditing then
        DigOutPackage(Self).ClickButton(NavOK);
    end;
  end;
end;

function THideRecordHelper.CanHideAny(DataSet: TevClientDataSet): boolean;
begin
  Result := true;
end;

procedure THideRecordHelper.dsHelperDataChange(Sender: TObject; Field: TField);
begin
  HideCurRecord.Update;
  UnhideCurRecord.Update;
end;

constructor THideRecordHelper.Create(AOwner: TComponent);
begin
  inherited;
  FScheduledEEFlag := False;
  sHideAction := 'Hide';
  sUnhideAction := 'unhide';
end;

procedure THideRecordHelper.UnhideCurRecordUpdate(Sender: TObject);
var
  bCanBeUnHided: boolean;
  bMayBeUnHided: boolean;
begin
  bCanBeUnHided := DSIsActive( FDataSet )
          and (FIndicatorFieldName <> '')
          and (FDataSet.RecordCount > 0)
          and GetScheduledEEFlagCondition(false)
          and assigned(FOnCanUnhideRecord)
          and FOnCanUnhideRecord( FDataSet );
  (Sender as TCustomAction).Visible := bCanBeUnHided;
  bMayBeUnHided := bCanBeUnHided;
  if bMayBeUnHided then
    if assigned(FOnMayUnHideRecord) then
      bMayBeUnHided := FOnMayUnHideRecord(FDataSet)
    else
      bMayBeUnHided := FDataSet.State = dsBrowse;
  (Sender as TCustomAction).Enabled := bMayBeUnHided;
end;

function THideRecordHelper.CanUnhideAny(
  DataSet: TevClientDataSet): boolean;
begin
  Result := true;
end;

procedure THideRecordHelper.UnhideCurRecordExecute(Sender: TObject);
var
  wasEditing: boolean;
begin
  if not assigned(FOnCheckBeforeUnhidingRecord) or FOnCheckBeforeUnhidingRecord(FDataSet) then
  begin

    if FScheduledEEFlag then
      if (EvMessage('Are you sure you want to make this Scheduled E/D active again? ', mtConfirmation, [mbOk,mbCancel] ) <> mrOk)then exit;

    wasEditing := FDataSet.State in [dsInsert, dsEdit];
    if not wasEditing then
      FDataSet.Edit;
    FDataSet[FIndicatorFieldNAme] := GROUP_BOX_YES;
    if not wasEditing then
      DigOutPackage(Self).ClickButton(NavOK);
  end;
end;

function THideRecordHelper.GetScheduledEEFlagCondition (IsHide:boolean): boolean;
begin
   if FScheduledEEFlag then
   begin
     if IsHide then
       Result := True 
     else
       Result := (FDataSet[FIndicatorFieldNAme] = GROUP_BOX_NO);
   end
   else begin
     if IsHide then
       Result := (FDataSet[FIndicatorFieldNAme] <> GROUP_BOX_NO)
     else
       Result := (FDataSet[FIndicatorFieldNAme] = GROUP_BOX_NO);
   end;

end;

end.
