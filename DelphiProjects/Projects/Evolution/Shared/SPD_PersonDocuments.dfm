object PersonDocuments: TPersonDocuments
  Left = 0
  Top = 0
  Width = 961
  Height = 670
  TabOrder = 0
  OnEnter = FrameEnter
  object pnlDocsBackground: TevPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 670
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object evPanel1: TevPanel
      Left = 0
      Top = 571
      Width = 812
      Height = 99
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object fpDocDetails: TisUIFashionPanel
        Left = 8
        Top = 0
        Width = 452
        Height = 92
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpDocDetails'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Document Detail'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        DesignSize = (
          452
          92)
        object evLabel15: TevLabel
          Left = 18
          Top = 35
          Width = 71
          Height = 13
          Anchors = []
          AutoSize = False
          Caption = '~Description'
          FocusControl = dbeDocDescr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel28: TevLabel
          Left = 231
          Top = 35
          Width = 53
          Height = 13
          Anchors = []
          AutoSize = False
          Caption = 'Begin Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel1: TevLabel
          Left = 335
          Top = 35
          Width = 45
          Height = 13
          Anchors = []
          AutoSize = False
          Caption = 'End Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dbeDocDescr: TevDBEdit
          Left = 18
          Top = 50
          Width = 204
          Height = 21
          Anchors = []
          AutoSize = False
          DataField = 'NAME'
          DataSource = dsEditDoc
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdpInitial_Effective_date: TevDBDateTimePicker
          Left = 231
          Top = 50
          Width = 95
          Height = 21
          HelpContext = 10561
          Anchors = []
          AutoSize = False
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'BEGIN_DATE'
          DataSource = dsEditDoc
          Epoch = 1950
          ShowButton = True
          TabOrder = 1
        end
        object evDBDateTimePicker1: TevDBDateTimePicker
          Left = 335
          Top = 50
          Width = 95
          Height = 21
          HelpContext = 10561
          Anchors = []
          AutoSize = False
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'END_DATE'
          DataSource = dsEditDoc
          Epoch = 1950
          ShowButton = True
          TabOrder = 2
        end
      end
      object fpControls: TisUIFashionPanel
        Left = 470
        Top = 0
        Width = 341
        Height = 92
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Controls'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object pnlDocs2: TPanel
          Left = 2
          Top = 26
          Width = 317
          Height = 40
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object evLabel2: TevLabel
            Left = 24
            Top = -50
            Width = 66
            Height = 19
            Caption = 'Controls'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object btnAddDoc: TevBitBtn
            Left = 12
            Top = 14
            Width = 60
            Height = 25
            Caption = 'Add'
            TabOrder = 0
            OnClick = btnAddDocClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
              9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
              CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
              FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
              FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
              9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
              FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
              51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
              9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
              66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
              9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
              9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
              FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
              9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
              FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
              87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
              FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
              B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
              E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object btnShowDoc: TevBitBtn
            Left = 256
            Top = 14
            Width = 60
            Height = 25
            Caption = 'Show'
            TabOrder = 1
            OnClick = btnShowDocClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000DCDCDCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCF4F4F4DDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCF6F6F5BBBBB9AFAFAD
              AEAEACAEAEACAEAEACAEAEACAEAFACAFAFADAFAFADB0B0AEB5B3AFC2BAB13F7C
              AB477FAA4881ACB0BDC7BCBCBBB0AFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFB0
              AFAFB0AFAFB1B0B0B3B3B3B9B9B98686868888888A8A8ABFBFBFB0B0ADFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3077AE6F9B
              BE1FADFF93CEEA4491C6B1B0B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF838383A2A2A2C0C0C0D3D3D39D9D9DAEAEABFFFFFF
              FFFFFF969696C6C6C6FFFFFFBCBBBA7775756F6D6B706E6C74716F8C8580AFA5
              9DA0C3D9B0ECFF3F91C6AEAEAEFFFFFFFFFFFF979696C7C7C7FFFFFFBCBCBB76
              76756D6D6D6D6D6D717070858484A5A5A5C8C8C7EFEEEE9D9D9DADADABFFFFFF
              FCFBFBFFFFFFFFFFFF8C8A89949493D5D5D9E1E3E7E1E2E6D6D6DA9B99988A84
              80F6EDE7BFDFEB3F92CAADADADFFFFFFFDFDFCFFFFFFFFFFFF8A8A8A959594D7
              D7D7E5E5E5E4E4E4D8D8D8999999858484EEEEEEE1E1E19F9F9EADADABFFFFFF
              F8F8F8FEFEFEBCBBBA9A9998E9E6E5ECC88EF5CE7FFADC8DF7E3A9E8E8E79E9B
              9BB6AEA73690CCFFFFFFADADADFFFFFFFAF9F9FFFFFFBCBCBB999999E8E7E7C1
              C1C1C4C3C3D1D1D1DBDADAE9E9E99C9C9CAEAEAE9E9E9EFFFFFFADADABFFFFFF
              F7F7F6C4C4C482807EE4E2E2F2C284FDCD82FFD885FFE998FFF1A2F8E5A9E2E1
              E58A807BFFFFFFFFFFFFADADADFFFFFFF8F8F8C5C5C5818080E4E3E3BBBBBBC4
              C4C4CECDCDDDDDDDE4E4E4DCDCDCE4E3E380807FFFFFFFFFFFFFADADABFFFFFF
              F5F4F3FEFDFC7E7B78FDF7F41688F553AFFF0D8AFF499FE3FFE998F9DB8CF4F5
              FA878481FFFFFFFFFFFFADADADFFFFFFF5F5F5FEFEFE7B7B7BF8F8F8A7A7A7C3
              C2C2ABABABAFAFAFDDDDDDD0D0D0F7F7F7858484FFFFFFFFFFFFADADABFFFFFF
              F2F2F1C3C4C4827E7BFFFCFA94CCC0B8FBFF89F2FF098AFFFFD886F3CC7DFAFC
              FF898685FFFFFFFFFFFFADADADFFFFFFF3F3F3C5C5C57E7E7DFEFDFDC8C8C7FA
              F9F9F2F2F1ABABABCECDCDC2C2C1FEFEFE868686FFFFFFFFFFFFADADABFFFFFF
              EFEEEDF6F5F48A8886F2F2F3ECB77BDBE6F491CDFF90C0EBF6CD8AF0CC93F1F4
              F88D8A89FFFFFFFFFFFFADADADFFFFFFF0EFEFF7F6F6888888F3F3F3B1B0B0EA
              EAEAD8D8D8CAC9C9C4C4C4C5C5C5F7F6F68A8A8AFFFFFFFFFFFFADADABFFFFFF
              ECECEBBFBFBFBEBCBBAEADACFFFFFFE9B87FEDBC80EEBF82EEC78FFFFFFFB3B2
              B2C9C7C6FFFFFFFFFFFFADADADFFFFFFEDEDEDC0C0C0BDBCBCAEAEAEFFFFFFB2
              B2B2B6B6B5B8B8B8C0C0C0FFFFFFB3B3B3C8C8C7FFFFFFFFFFFFADAEABFFFFFF
              E9E8E7EEEDECF2F1F09D9A99B1B0B1FBFDFFFFFFFFFFFFFFFBFDFFB4B3B49896
              94FFFFFFFFFFFFFFFFFFAEAEAEFFFFFFE9E9E9EEEEEEF3F2F29B9B9BB2B1B1FF
              FFFFFFFFFFFFFFFFFFFFFFB4B4B4979696FFFFFFFFFFFFFFFFFFAEAEABFFFFFF
              E6E6E59B9B9BBDBDBCE7E6E5BCBAB79391908C89888C8987969492AFAEACCECF
              CEFFFFFFFFFFFFFFFFFFAEAEAEFFFFFFE7E7E69C9C9CBEBDBDE7E7E6BABABA92
              9292898989898989959594AFAFAFD0D0D0FFFFFFFFFFFFFFFFFFAEAEACFFFFFF
              E1E0DFE4E3E1E4E3E2E4E2E1E4E3E2FFFFFFAAAAA7FFFFFFEDEEEECECECCFFFF
              FFFFFFFFFFFFFFFFFFFFAFAFAFFFFFFFE1E1E1E4E3E3E4E4E4E4E3E3E4E4E4FF
              FFFFAAAAAAFFFFFFF0EFEFCECECEFFFFFFFFFFFFFFFFFFFFFFFFAFAFADFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEACCCCCAFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFB0AFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFECEBEBCDCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B6B0B0AD
              AEAEACAEAEABAEAEABAEAEABADAEABAEAEABAFAFADB7B7B5FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFB8B8B8B1B0B0AFAFAFAEAEAEAEAEAEAEAEAEAEAEAEAE
              AEAEB0AFAFB8B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
            OnSetSecurityRO = btnShowDocSetSecurityRO
          end
          object btnSaveDoc: TevBitBtn
            Left = 195
            Top = 14
            Width = 60
            Height = 25
            Caption = 'Save'
            TabOrder = 2
            OnClick = btnSaveDocClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCE7E7E7CCCCCC
              CCCCCCCCCCCCCCCCCCAC7314AD7519AC7316AC7214AB7214AA7113AA7013AA70
              13AA7113AC7419B07A23E8E8E8CDCCCCCDCCCCCDCCCCCDCCCC6C6C6C6E6E6E6C
              6C6C6B6B6B6B6B6B6A6A6A6969696969696A6A6A6D6D6D737373C4AB7AB78216
              B37A06D7D0D4D8D1D3A96D0BF7EFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFAC7419A4A4A47A7979727171D3D2D2D3D3D3676767EEEEEEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6D6D6DB78218F6CC8A
              F0C172F9F7FCFFFFFFA66905F5EBDA979A9F535456959697FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFAA71147A7979C4C4C4B8B7B7FAF9F9FFFFFF636363E9E9E99C
              9C9C555454979696FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6A6A6AB68116F3CA86
              EBB660F8F5F8FFFFFFA56803F5EAD9FFFFFF575757FFFFFFFBFBFAF6F6F5F5F5
              F4F5F7F7FFFFFFAA7113797979C2C2C1ADADADF7F7F7FFFFFF626262E8E8E8FF
              FFFF575757FFFFFFFCFCFBF7F7F7F7F6F6F8F8F8FFFFFF6A6A6AB68116F1CA89
              E8B155F9F8FDFAF8FBA66905F6ECDB8C8E925455568B8B8CE3E2E3DFDEDEDEDE
              DEDDDEE0FFFFFFAA7114797979C2C2C1A8A8A7FAFAFAFAFAFA636363EBEBEA8F
              8F8F5554548C8C8CE4E3E3E0DFDFE0DFDFE0DFDFFFFFFF6A6A6AB68115F3CB8F
              E6AD4DFDFFFFEEE7E5A76B09F6EFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFAC7419797979C4C3C3A3A3A3FFFFFFE8E8E8656565EEEEEEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6D6D6DB68115F3D095
              E4AA46E8CBA5FFFFFFA66D10A76F13A76D10A66D0EA66C0EA56C0EA56B0EA66C
              0EA96F12AC7419B07A23797979C9C8C8A1A0A0C6C6C6FFFFFF66666668686867
              67676666666666666666666565656666666868686D6D6D737373B68115F3D39C
              E3A53EE2A237E4A132E6A437E7A53AE7A63AE7A63AE7A63CE7A840E8AA43F6D6
              9EB88212FFFFFFFFFFFF797979CCCCCC9C9C9C9898989797979A9A9A9C9C9C9C
              9C9C9C9C9C9D9D9D9F9F9EA1A0A0CECECE7A7979FFFFFFFFFFFFB68114F4D5A4
              E09E31F4E0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4E1B8E19F32F5D6
              A4B68114FFFFFFFFFFFF797979CECECE959594DBDADAFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFDCDBDB969595CFCFCF797979FFFFFFFFFFFFB68014F5DAAE
              DF9822FCFFFF797C80A1A3A5FEFBF2797A7CA3A4A6A0A3A7FBFEFFDF9722F5DA
              AEB68014FFFFFFFFFFFF787877D4D4D48F8F8FFFFFFF7E7E7DA4A4A4FAFAFA7B
              7B7BA5A5A5A4A4A4FFFFFF8E8E8ED4D4D4787877FFFFFFFFFFFFB68013F7DFB9
              DD9215FCFCFCFDF8ECFFF8EAFDF6E8FCF6E8FBF4E7F9F4E8FAFAFADC9115F7DF
              B9B68013FFFFFFFFFFFF787877DADADA8A8A8AFEFDFDF7F7F7F7F6F6F5F5F5F5
              F5F5F3F3F3F3F3F3FBFBFB898989DADADA787877FFFFFFFFFFFFB67F12FAE5C5
              DA8C09FEFEFF7878797A7979A2A1A19F9E9EF5EBE09B9B9DFCFCFFDA8C09FAE5
              C5B67F12FFFFFFFFFFFF777676E1E1E1858484FFFFFF7979797A7979A1A1A19F
              9F9EEBEBEA9C9C9CFEFEFE858484E1E1E1777676FFFFFFFFFFFFB67F12FBECD2
              D88400FFFFFFF1E5DAF2E6DAF2E6DAF0E4D8EEE2D7EEE2D8FEFFFFD88300FBEC
              D2B67F12FFFFFFFFFFFF777676E9E9E97E7E7DFFFFFFE4E4E4E5E5E5E5E5E5E4
              E3E3E2E2E2E2E2E2FFFFFF7E7E7DE9E9E9777676FFFFFFFFFFFFB78114FFECCD
              FCE6C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE6C3FFEC
              CDB78114FFFFFFFFFFFF797979E8E8E8E2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFE2E2E2E8E8E8797979FFFFFFFFFFFFDFC695B78114
              B57E0FB57C0BB57C09B57C09B57C09B57C09B57C09B57C09B57C0BB57E0FB781
              14DFC695FFFFFFFFFFFFBFBFBF79797976767574737374737374737374737374
              7373747373747373747373767675797979BFBFBFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object btnEditDoc: TevBitBtn
            Left = 73
            Top = 14
            Width = 60
            Height = 25
            Caption = 'Update'
            TabOrder = 3
            OnClick = btnEditDocClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5DADADACC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDC8BEB1BD9768B88444B98545FFFFFFFFFFFFCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCDCDCDFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDBCBCBB9292927E
              7E7D7F7E7EFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFFFFFFFFFFFF
              E1E1E1BF9C72BE8A4ADEAC66F4C57BB88343FFFFFFFFFFFFB98545B78343B681
              42B78242B98444BB8849FFFFFFFFFFFFFFFFFFE1E1E1979797838383A4A4A4BD
              BCBC7E7E7DFFFFFFFFFFFF7F7E7E7D7C7C7C7B7B7D7C7C7F7E7EFFFFFFF9F9F9
              C2A37EC18C4CECBC72F1C581F8D39DB78242FFFFFFFFFFFFB78343F9C97EF6C6
              7BF5CC8CD2A771FFFFFFFFFFFFFFFFFFFAF9F99F9F9E868686B3B3B3BDBCBCCD
              CCCC7D7C7CFFFFFFFFFFFF7D7C7CC0C0C0BEBDBDC4C4C4A1A1A1FFFFFFD9D3CB
              BA8646E9B86DEDC07DEFCE9CD5AC71B88343FFFFFFFFFFFFB68141F2C47EF0C4
              82EAB86FBC8947E5E5E5FFFFFFFFFFFFD2D2D180807FB0AFAFB8B8B8C8C8C7A5
              A5A57E7E7DFFFFFFFFFFFF7C7B7BBCBCBBBCBCBBB0AFAF828282FFFFFFC09E74
              D2A059E6B368EDCC9AC08F51D8B995F6EFE6FFFFFFFFFFFFB78241F5D9ACF0CF
              9FEAB86CD4A25CC3A988FFFFFFFFFFFF999999989898AAAAAAC6C6C6888888B5
              B5B5EEEEEEFFFFFFFFFFFF7C7B7BD3D3D3CAC9C9B0AFAF9A9A9AFFFFFFB88444
              DDAB61E2B167D4AB77D7BA95FFFFFFFFFFFFFFFFFFFFFFFFB98443C6995FD7B2
              82E6B266E0AE65B98547FFFFFFFFFFFF7E7E7DA3A3A3A9A8A8A5A5A5B5B5B5FF
              FFFFFFFFFFFFFFFFFFFFFF7E7E7D939393ACACACAAA9A9A6A6A6FFFFFFB88344
              DFAB5FE2B774C6995FFEFDFCFEFEFEFFFFFFFFFFFFFFFFFFBD8B4DFDFBF9C697
              5FE6BB79E5B36AB78344FFFFFFFFFFFF7E7E7DA3A3A3AFAFAF939393FEFDFDFE
              FEFEFFFFFFFFFFFFFFFFFF868585FBFBFB919191B3B3B3ABABABFFFFFFB88343
              DFB373DBA85EB78243FDFDFDCECECDFFFFFFFFFFFFFFFFFFFFFEFDFEFEFEB682
              42DFAC63E4B877B78243FFFFFFFFFFFF7E7E7DABABABA1A0A07D7C7CFEFDFDCE
              CDCDFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE7C7B7BA4A4A4B1B0B0FFFFFFB88342
              E2BB83D59F52BF8B48D5D3D0BC894BFFFFFFFFFFFFFFFFFFFFFFFFE3E2E1C08C
              49D9A557E5C087B78242FFFFFFFFFFFF7D7C7CB4B4B4979797858484D3D2D283
              8383FFFFFFFFFFFFFFFFFFFFFFFFE2E2E28685859D9D9DB9B9B9FFFFFFB98547
              E1C192CF9A4AC18C48C0955EBA8545FFFFFFFFFFFFF8F8F8E2E2E2C1A481C792
              4BD39E4FE3C395B88241FFFFFFFFFFFF80807FBBBBBB9292928685858F8F8F80
              807FFFFFFFFFFFFFF9F9F8E2E2E2A09F9F8B8B8B969595BEBDBDFFFFFFDCC1A0
              D6B386D4A661C99243E4C79DB98443FFFFFFFFFFFFCAC3BAC2A581BC8746CD98
              4AD8AC6DD9B98ECFAB80FFFFFFFFFFFFBDBCBCAEAEAE9F9F9E8A8A8AC2C2C17E
              7E7DFFFFFFFFFFFFC2C2C1A1A0A0818080909090A5A5A5B4B4B4FFFFFFFFFFFF
              BD8D4FD1A668C48B39D9B580B88241FFFFFFFFFFFFBA8646C28D45C99344D09E
              56E7CEAABD8B4FF7F1E9FFFFFFFFFFFFFFFFFF8686869F9F9E838383AFAFAF7D
              7C7CFFFFFFFFFFFF80807F8686868B8B8B979696CAC9C9868585FFFFFFCDCDCD
              CCA472E6CFACE9D3B2EAD5B4B88341FFFFFFFFFFFFB98444CD9C54DAB57FEAD5
              B5C69A64D6B791FFFFFFFFFFFFFFFFFFCDCCCC9F9F9ECACACACFCFCFD1D1D17D
              7C7CFFFFFFFFFFFF7F7E7E959594AFAFAFD1D1D1959594B2B2B2FFFFFFBC8849
              B98444B88241B8813FB88241BA8545FFFFFFFFFFFFB98342EAD5B6DBBD95C090
              54D1AD81FFFFFFFFFFFFFFFFFFFFFFFF8282827F7E7E7D7C7C7C7B7B7D7C7C80
              807FFFFFFFFFFFFF7E7E7DD1D1D1B8B8B88A8A8AA7A7A7FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA8544BB8849CBA374F1E7
              DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF7F7E7E8281819E9E9EE5E5E5FFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object btnDeleteDoc: TevBitBtn
            Left = 134
            Top = 14
            Width = 60
            Height = 25
            Caption = 'Delete'
            TabOrder = 4
            OnClick = btnDeleteDocClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
              CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
              F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
              6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
              A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
              CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
              A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
              2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
              C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
              9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
              5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
              F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
              9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
              576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
              FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
              97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
              4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
              F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
              8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
              3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
              F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
              334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
              F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
              2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
              EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
              3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
              EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
              84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
              8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
              F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
              8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
              9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
              F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
              8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
              2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
              C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
              807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
              C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
              EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
              CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
              6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
        end
      end
    end
    object pnlDocs1: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 571
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pnlGridBorder: TevPanel
        Left = 0
        Top = 0
        Width = 968
        Height = 571
        Align = alLeft
        BevelOuter = bvNone
        BorderWidth = 8
        TabOrder = 0
        object fpGridDocuments: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 801
          Height = 555
          Align = alLeft
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpGridDocuments'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Documents'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object pnlFPBody: TevPanel
            Left = 12
            Top = 36
            Width = 769
            Height = 499
            Align = alClient
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object grDocs: TevDBGrid
              Left = 0
              Top = 0
              Width = 769
              Height = 499
              DisableThemesInTitle = False
              Selected.Strings = (
                'NAME'#9'40'#9'Description'#9#9
                'FILE_NAME'#9'40'#9'File Name'#9#9
                'CREATION_DATE'#9'18'#9'Attached On'#9'F'
                'BEGIN_DATE'#9'10'#9'Begin Date'#9#9
                'END_DATE'#9'10'#9'End Date'#9#9)
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TPersonDocuments\grDocs'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = dsDocs
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = btnShowDocClick
              PaintOptions.AlternatingRowColor = clMoneyGreen
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
        end
      end
    end
  end
  object odDocs: TOpenDialog
    Filter = 
      'Text files (*.txt)|*.txt|Word Documents (*.doc)|*.doc|Excel Docu' +
      'ments (*.xls)|*.xls|All Files (*.*)|*.*'
    Title = 'Load Document'
    Left = 16
    Top = 16
  end
  object sdDocs: TSaveDialog
    Filter = 
      'Text files (*.txt)|*.txt|Word Documents (*.doc)|*.doc|Excel Docu' +
      'ments (*.xls)|*.xls|All Files (*.*)|*.*'
    Title = 'Save Document'
    Left = 72
    Top = 16
  end
  object dsDocs: TevDataSource
    DataSet = cdBrowse
    Left = 191
    Top = 55
  end
  object cdBrowse: TevClientDataSet
    AfterScroll = cdBrowseAfterScroll
    ClientID = -1
    Left = 232
    Top = 56
    object cdBrowseNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object cdBrowseFILE_NAME: TStringField
      FieldName = 'FILE_NAME'
      Size = 40
    end
    object cdBrowseCREATION_DATE: TDateTimeField
      FieldName = 'CREATION_DATE'
    end
    object cdBrowseBEGIN_DATE: TDateTimeField
      FieldName = 'BEGIN_DATE'
    end
    object cdBrowseEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object cdBrowseCL_PERSON_DOCUMENTS_NBR: TIntegerField
      FieldName = 'CL_PERSON_DOCUMENTS_NBR'
    end
    object cdBrowseCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
  end
  object dsEditDoc: TevDataSource
    OnDataChange = dsEditDocDataChange
    Left = 231
    Top = 95
  end
end
