// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_MiniNavigationFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons,  ActnList, db, EvUtils, ISBasicClasses, EvUIUtils, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TConfirmEvent = procedure( Sender: TObject; var Allow: boolean ) of object;

  TMiniNavigationFrame = class(TFrame)
    SpeedButton1: TevSpeedButton;
    SpeedButton2: TevSpeedButton;
    evActionList1: TevActionList;
    InsertRecord: TAction;
    DeleteRecord: TAction;
    procedure DeleteRecordExecute(Sender: TObject);
    procedure DeleteRecordUpdate(Sender: TObject);
    procedure InsertRecordExecute(Sender: TObject);
    procedure InsertRecordUpdate(Sender: TObject);
  private
    { Private declarations }
    FDataSource: TDataSource;
    FInsertFocusControl: TWinControl;
    FOnDeleteRecordConfirm: TConfirmEvent;
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
  public
    { Public declarations }
    property DataSource: TDataSource read FDataSource write FDataSource;
    property InsertFocusControl: TWinControl read FInsertFocusControl write FInsertFocusControl;

    property OnDeleteRecordConfirm: TConfirmEvent read FOnDeleteRecordConfirm write FOnDeleteRecordConfirm;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO;
  end;

implementation

{$R *.DFM}
procedure TMiniNavigationFrame.DeleteRecordExecute(Sender: TObject);
var
  allow: boolean;
begin
  allow := true;
  if assigned(FOnDeleteRecordConfirm) then
    FOnDeleteRecordConfirm( Self, allow );
  if allow and (EvMessage('Delete record.  Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    DataSource.DataSet.Delete;
end;

procedure TMiniNavigationFrame.DeleteRecordUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned(DataSource) and
                                 assigned(DataSource.DataSet) and
                                 DataSource.DataSet.Active and
                                 (DataSource.DataSet.State in [dsBrowse]) and
                                 (DataSource.DataSet.RecordCount > 0) and not FSecurityRO;
end;

procedure TMiniNavigationFrame.InsertRecordExecute(Sender: TObject);
begin
  DataSource.DataSet.CheckBrowseMode;
  if DataSource.DataSet.State in [dsBrowse] then
  begin
    DataSource.DataSet.Insert;
    if Assigned(InsertFocusControl) then
      InsertFocusControl.SetFocus;
  end;
end;

procedure TMiniNavigationFrame.InsertRecordUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned(DataSource) and assigned(DataSource.DataSet) and
                                 not (DataSource.DataSet.State in [dsInsert]) and
                                 DataSource.DataSet.Active and not FSecurityRO;
end;


procedure TMiniNavigationFrame.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  evActionList1.UpdateAction(InsertRecord);
  evActionList1.UpdateAction(DeleteRecord);
end;

end.
