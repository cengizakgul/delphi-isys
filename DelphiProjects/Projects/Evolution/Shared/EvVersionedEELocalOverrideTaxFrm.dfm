inherited EvVersionedEELocalOverridetax: TEvVersionedEELocalOverridetax
  Left = 373
  Top = 159
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 333
    inherited grFieldData: TevDBGrid
      Height = 206
    end
    inherited pnlEdit: TevPanel
      Top = 250
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 333
    Width = 791
    Height = 100
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 337
      Height = 100
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Additional Local Details'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lblTax_Type: TevLabel
        Left = 20
        Top = 36
        Width = 88
        Height = 13
        Caption = 'Override Tax Type'
      end
      object lblTax_Type_Value: TevLabel
        Left = 177
        Top = 36
        Width = 91
        Height = 13
        Caption = 'Override Tax Value'
      end
      object EvCombo_Tax_Type: TevDBComboBox
        Left = 20
        Top = 53
        Width = 133
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'OVERRIDE_LOCAL_TAX_TYPE'
        DataSource = dsFieldData
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
        OnChange = EvCombo_Tax_TypeChange
      end
      object EvCombo_Tax_Type_Value: TevDBEdit
        Left = 177
        Top = 53
        Width = 130
        Height = 21
        DataField = 'OVERRIDE_LOCAL_TAX_VALUE'
        DataSource = dsFieldData
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
