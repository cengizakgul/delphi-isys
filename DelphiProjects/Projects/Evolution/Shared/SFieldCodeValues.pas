// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SFieldCodeValues;

interface

uses
  Classes, SysUtils, SDDClasses, StrUtils, typinfo, EvConsts,
  SDataDictSystem, SDataDictBureau, SDataDictClient, SDataDictTemp,
  EvLegacy;

var
  RegularCheckReportNbrs: array [Char] of Integer;
  MiscCheckReportNbrs: array [Char] of Integer;

const
  Database_ComboChoices =
    'System' + #9 + CH_DATABASE_SYSTEM + #13 +
    'Client' + #9 + CH_DATABASE_CLIENT + #13 +
    'Service Bureau' + #9 + CH_DATABASE_SERVICE_BUREAU + #13 +
    'Temp' + #9 + CH_DATABASE_TEMP;


  MediaType_ComboChoices =
    'Check Legal Top Stub 2/Up'#9+ MEDIA_TYPE_CHECK_REGULAR_TOP+#13+
    'Check Legal Bottom Stub 2/Up'#9+ MEDIA_TYPE_CHECK_REGULAR+#13+
    'Check Letter Top Stub'#9+ MEDIA_TYPE_CHECK_LETTER_TOP+#13+
    'Check Letter Bottom Stub'#9+ MEDIA_TYPE_CHECK_LETTER_BOTTOM+#13+
    'Check Letter Bottom Stub - F&F'#9+ MEDIA_TYPE_CHECK_LETTER_BOTTOM_FF+#13+
    'Check Pressure Seal Legal'#9+ MEDIA_TYPE_CHECK_PRESS_LEGAL+#13+
    'Check Pressure Seal Legal Moore'#9+ MEDIA_TYPE_CHECK_PRESS_LEGAL_MOORE+#13+
    'Check Pressure Seal Legal Greatland'#9+ MEDIA_TYPE_CHECK_PRESS_LEGAL_GREATLAND+#13+
    'Check Pressure Seal Legal VersaSeal'#9+ MEDIA_TYPE_CHECK_PRESS_LEGAL_VERSASEAL+#13+
    'Check Pressure Seal Legal F&F'#9+ MEDIA_TYPE_CHECK_PRESS_LEGAL_FF+#13+
    'Check Pressure Seal Letter'#9+ MEDIA_TYPE_CHECK_PRESS+#13+
    'Check Pressure Seal Letter Greatland'#9+ MEDIA_TYPE_CHECK_PRESS_LETTER_GREATLAND+#13+
    '1096 Red'#9+ MEDIA_TYPE_1096_RED+#13+
    '1099 2up'#9+ MEDIA_TYPE_1099_2UP+#13+
    '1099 3up'#9+ MEDIA_TYPE_1099_3UP+#13+
    '1099R Red'#9+ MEDIA_TYPE_1099R_RED+#13+
    '1099R Copy 2'#9+ MEDIA_TYPE_1099R_COPY2+#13+
    '1099R Copy BC'#9+ MEDIA_TYPE_1099R_COPYBC+#13+
    '1099R Copy 1'#9+ MEDIA_TYPE_1099R_COPY1+#13+
    '1099R Copy D'#9+ MEDIA_TYPE_1099R_COPYD+#13+
    '1099 Interest Red'#9+ MEDIA_TYPE_1099_INTEREST_RED+#13+
    '1099 M Pressure Sealed'#9 +	MEDIA_TYPE_1099M_Pressure+#13+
    '1099 M Pressure Sealed & Fullfillment'#9 + MEDIA_TYPE_1099M_PressureFull+#13+

    'W2 EE Copy 4up'#9+ MEDIA_TYPE_W2_EE+#13+
    'W2 EE Copy Pressure'#9+ MEDIA_TYPE_W2_EE_PRESSURE+#13+
    'W2 EE Copy Pressure Greatland'#9+ MEDIA_TYPE_W2_EE_PRESSURE_GREATLAND+#13+
    'W2 Other Copy'#9+ MEDIA_TYPE_W2_OTHERS+#13+
    'Letter Plain'#9+ MEDIA_TYPE_PLAIN_LETTER+#13+
    'Legal Plain'#9+ MEDIA_TYPE_PLAIN_LEGAL+#13+
    //'Letter Perforated'#9+ MEDIA_TYPE_PERF_LETTER+#13+
    'Pickup Sheet'#9+ MEDIA_TYPE_PICKUP_SHEET+#13+
    'DHL label #1239'#9+ MEDIA_TYPE_DHL_LABEL+#13+

    '1095B Letter Portrait'#9+ MEDIA_TYPE_1095B_LETTER_PORTRAIT+#13+
    '1095B Pressure Seal Business Forms'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_BUSINESS_FORMS+#13+
    '1095B Pressure Seal Greatland'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_GREATLAND+#13+
    '1095B Pressure Seal Forms & Fulfillment'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_FORMS_FULFILLMENT+#13+

    '1095C Letter Portrait'#9+ MEDIA_TYPE_1095C_LETTER_PORTRAIT+#13+
    '1095C Pressure Seal Business Forms'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_BUSINESS_FORMS+#13+
    '1095C Pressure Seal Greatland'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_GREATLAND+#13+
    '1095C Pressure Seal Forms & Fulfillment'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_FORMS_FULFILLMENT+#13+

    'Custom 2'#9+ MEDIA_TYPE_CUSTOM_2+#13+
    'Custom 3'#9+ MEDIA_TYPE_CUSTOM_3+#13+
    'Custom 4'#9+ MEDIA_TYPE_CUSTOM_4+#13+
    'Custom 5'#9+ MEDIA_TYPE_CUSTOM_5+#13+
    'Custom 6'#9+ MEDIA_TYPE_CUSTOM_6+#13+
    'Custom 7'#9+ MEDIA_TYPE_CUSTOM_7+#13+
    'Custom 8'#9+ MEDIA_TYPE_CUSTOM_8+#13+
    'Custom 9'#9+ MEDIA_TYPE_CUSTOM_9;


  W2MediaType_ComboChoices =
    'W2 EE Copy 4up'#9+ MEDIA_TYPE_W2_EE+#13+
    'W2 EE Copy Pressure'#9+ MEDIA_TYPE_W2_EE_PRESSURE+ #13+
    'W2 EE Copy Pressure Greatland'#9+ MEDIA_TYPE_W2_EE_PRESSURE_GREATLAND;

  MediaType1099M_ComboChoices =
    '1099 3 up' + #9 + MEDIA_TYPE_1099_3UP + #13 +
	  '1099 M Pressure Seal' + #9 + MEDIA_TYPE_1099M_Pressure + #13 +
	  '1099 M Pressure Seal & Fullfillment' + #9 + MEDIA_TYPE_1099M_PressureFull;

   MediaType1095B_ComboChoices =
    '1095B Letter Portrait'#9+ MEDIA_TYPE_1095B_LETTER_PORTRAIT+#13+
    '1095B Pressure Seal Business Forms'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_BUSINESS_FORMS+#13+
    '1095B Pressure Seal Greatland'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_GREATLAND+#13+
    '1095B Pressure Seal Forms & Fulfillment'#9+ MEDIA_TYPE_1095B_PRESSURE_SEAL_FORMS_FULFILLMENT;

  MediaType1095C_ComboChoices =
    '1095C Letter Portrait'#9+ MEDIA_TYPE_1095C_LETTER_PORTRAIT+#13+
    '1095C Pressure Seal Business Forms'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_BUSINESS_FORMS+#13+
    '1095C Pressure Seal Greatland'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_GREATLAND+#13+
    '1095C Pressure Seal Forms & Fulfillment'#9+ MEDIA_TYPE_1095C_PRESSURE_SEAL_FORMS_FULFILLMENT;

  MailBoxAutoRelease_ComboChoices =
    'Immediately'#9+   MAIL_BOX_AUTO_RELEASE_IMMED+#13+
    //'On Schedule'#9+   MAIL_BOX_AUTO_RELEASE_SCHED+#13+
    'Manual'#9+         MAIL_BOX_AUTO_RELEASE_NEVER;

  SySuiFreq_CompChoices =
    'Quarterly'+ #9+ SY_SUI_FREQ_QUARTERLY;

  FieldMetaType_ComboChoices =
    'String'#9+ FieldMetaString+ #13+
    'Unique String'#9+ FieldMetaUniqueString+ #13+
    'Integer'#9+ FieldMetaInteger+ #13+
    'Float'#9+ FieldMetaFloat+ #13+
    'Currency'#9+ FieldMetaCurrency+ #13+
    'TimeStamp'#9+ FieldMetaTimeStamp+ #13+
    'Date'#9+ FieldMetaDate+ #13+
    'Foreign Key'#9+ FieldMetaFK+ #13+
    'Sb Blob'#9+ FieldMetaSbBlob+ #13+
    'Cl Blob'#9+ FieldMetaClBlob;

  ReturnAutoEnlist_ComboChoices =
    'Use Enlist Groups'#9+ ReturnAutoEnlist_All+ #13+
    'Do Not Enlist'#9+ ReturnAutoEnlist_None;

  GL_Level_ComboChoices =
    'Default' + #9 + GL_LEVEL_NONE + #13 +
    'Division' + #9 + GL_LEVEL_DIVISION + #13 +
    'Branch' + #9 + GL_LEVEL_BRANCH + #13 +
    'Department' + #9 + GL_LEVEL_DEPT + #13 +
    'Team' + #9 + GL_LEVEL_TEAM + #13 +
    'Employee' + #9 + GL_LEVEL_EMPLOYEE + #13 +
    'Job' + #9 + GL_LEVEL_JOB;

  GL_Data_ComboChoices =
    'Default' + #9 + GL_DATA_NONE + #13 +
    'Cobra Credit' + #9 + GL_DATA_COBRA + #13 +
    'E/D' + #9 + GL_DATA_ED + #13 +
    'Federal' + #9 + GL_DATA_FEDERAL + #13 +
    'EE OASDI' + #9 + GL_DATA_EE_OASDI + #13 +
    'ER OASDI' + #9 + GL_DATA_ER_OASDI + #13 +
    'EE Medicare' + #9 + GL_DATA_EE_MEDICARE + #13 +
    'ER Medicare' + #9 + GL_DATA_ER_MEDICARE + #13 +
    'FUI' + #9 + GL_DATA_FUI + #13 +
    'EIC' + #9 + GL_DATA_EIC + #13 +
    'Backup Withholding' + #9 + GL_DATA_BACKUP + #13 +
    'State' + #9 + GL_DATA_STATE + #13 +
    'EE SDI' + #9 + GL_DATA_EE_SDI + #13 +
    'ER SDI' + #9 + GL_DATA_ER_SDI + #13 +
    'Local' + #9 + GL_DATA_LOCAL + #13 +
    'EE SUI' + #9 + GL_DATA_EE_SUI + #13 +
    'ER SUI' + #9 + GL_DATA_ER_SUI + #13 +
    'Billing' + #9 + GL_DATA_BILLING + #13 +
    'Agency Check' + #9 + GL_DATA_AGENCY_CHECK + #13 +
    'Net Pay' + #9 + GL_DATA_NET_PAY + #13 +
    'Tax Impound' + #9 + GL_DATA_TAX_IMPOUND + #13 +
    'Trust Impound' + #9 + GL_DATA_TRUST_IMPOUND + #13 +
    'WC Impound' + #9 + GL_DATA_WC_IMPOUND + #13 +
    '3rd Party Check' + #9 + GL_DATA_3P_CHECK + #13 +
    '3rd Party Tax' + #9 + GL_DATA_3P_TAX + #13 +
    'Tax Check' + #9 + GL_DATA_TAX_CHECK + #13 +
    'Federal Offset' + #9 + GL_DATA_FEDERAL_OFFSET + #13 +
    'EE OASDI Offset' + #9 + GL_DATA_EE_OASDI_OFFSET + #13 +
    'ER OASDI Offset' + #9 + GL_DATA_ER_OASDI_OFFSET + #13 +
    'EE Medicare Offset' + #9 + GL_DATA_EE_MEDICARE_OFFSET + #13 +
    'ER Medicare Offset' + #9 + GL_DATA_ER_MEDICARE_OFFSET + #13 +
    'FUI Offset' + #9 + GL_DATA_FUI_OFFSET + #13 +
    'EIC Offset' + #9 + GL_DATA_EIC_OFFSET + #13 +
    'Backup Withholding Offset' + #9 + GL_DATA_BACKUP_OFFSET + #13 +
    'State Offset' + #9 + GL_DATA_STATE_OFFSET + #13 +
    'EE SDI Offset' + #9 + GL_DATA_EE_SDI_OFFSET + #13 +
    'ER SDI Offset' + #9 + GL_DATA_ER_SDI_OFFSET + #13 +
    'Local Offset' + #9 + GL_DATA_LOCAL_OFFSET + #13 +
    'EE SUI Offset' + #9 + GL_DATA_EE_SUI_OFFSET + #13 +
    'ER SUI Offset' + #9 + GL_DATA_ER_SUI_OFFSET + #13 +
    'Billing Offset' + #9 + GL_DATA_BILLING_OFFSET + #13 +
    'Agency Check Offset' + #9 + GL_DATA_AGENCY_CHECK_OFFSET + #13 +
    'ER Match Offset' + #9 + GL_DATA_ER_MATCH_OFFSET + #13 +
    'Tax Check Offset' + #9 + GL_DATA_TAX_CHECK_OFFSET + #13 +
    'Tax Impound Offset' + #9 + GL_DATA_TAX_IMPOUND_OFFSET + #13 +
    'Trust Impound Offset' + #9 + GL_DATA_TRUST_IMPOUND_OFFSET + #13 +
    'WC Impound Offset' + #9 + GL_DATA_WC_IMPOUND_OFFSET + #13 +
    'E/D Offset' + #9 + GL_DATA_ED_OFFSET + #13 +
    'VT Healthcare' + #9 + GL_DATA_VT_HEALTHCARE + #13 +
    'UI Rounding' + #9 + GL_DATA_UI_ROUNDING + #13 +
    'VT Healthcare Offset' + #9 + GL_DATA_VT_HEALTHCARE_OFFSET + #13 +
    'UI Rounding Offset' + #9 + GL_DATA_UI_ROUNDING_OFFSET;


//----------------------------------
// TAX MODULE RELATED CONSTANTS
//----------------------------------

// Tax return copy
  TaxReturnSBorClient_ComboChoices =
    'Service Bureau' + #9 + TAX_RETURN_SB_OR_CLIENT_SB + #13 +
    'Client' + #9 + TAX_RETURN_SB_OR_CLIENT_CLIENT + #13 +
    'Employee' + #9 + TAX_RETURN_SB_OR_CLIENT_EE + #13 +
    'Agency' + #9 + TAX_RETURN_SB_OR_CLIENT_AGENCY;

// report types
  ReportPriorities_ComboChoices =
    '1' + #9+ '1'+ #13+
    '2' + #9+ '2'+ #13+
    '3' + #9+ '3'+ #13+
    '4' + #9+ '4'+ #13+
    '5' + #9+ '5'+ #13+
    '6' + #9+ '6'+ #13+
    '7' + #9+ '7'+ #13+
    '8' + #9+ '8'+ #13+
    '9' + #9+ '9';

// Tax return media type
  TaxReturnPCLorEDI_ComboChoices =
    'PCL Form' + #9 + TAX_RETURN_PCL_OR_EDI_PCL + #13 +
    'EDI File' + #9 + TAX_RETURN_PCL_OR_EDI_EDI;

  EftEndDateCalcMethod_ComboChoices =
    'Based on company dep. frequency'#9+ EFT_END_DATE_METHOD_ON_FREQ+ #13+
    'End of quarter'#9+ EFT_END_DATE_METHOD_QUARTER_END+ #13+
    'End of month'#9+ EFT_END_DATE_METHOD_MONTH_END+ #13+
    'Last check date'#9+ EFT_END_DATE_METHOD_LAST_CHECK+ #13+
    'Last processed check date'#9+ EFT_END_DATE_METHOD_PROCESSED_LAST_CHECK+ #13+
    'Virginia base on company dep. frequency'#9+ EFT_END_DATE_METHOD_ON_FREQ_VI+ #13+
    'California base on company dep. frequency'#9+ EFT_END_DATE_METHOD_ON_FREQ_CA+ #13+
    'Massachusetts base on company dep. frequency'#9+ EFT_END_DATE_METHOD_ON_FREQ_MA;

// Tax retuns status
  TaxReturnStatus_ComboChoices =
    'Unprocessed' + #9 + TAX_RETURN_STATUS_UNPROCESSED + #13 +
    'Processed' + #9 + TAX_RETURN_STATUS_PROCESSED + #13 +
    'On Hold' + #9 + TAX_RETURN_STATUS_HOLD + #13 +
    'Should be reprocessed' + #9 + TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED;

// Tax return type
  TaxReturnType_ComboChoices =
    'Cover' + #9 + TAX_RETURN_TYPE_TOP + #13 +
    'Federal' + #9 + TAX_RETURN_TYPE_FED + #13 +
    'Federal EE driven' + #9 + TAX_RETURN_TYPE_FED_EE + #13 +
    'EE Electronic Copy' + #9 + TAX_RETURN_TYPE_FED_EE_EeCopy + #13 +
    'EE Electronic Copy W2' + #9 + TAX_RETURN_TYPE_FED_EE_EeCopy_W2 + #13 +
    'EE Electronic Copy 1095B' + #9 + TAX_RETURN_TYPE_EE_EeCopy_1095B + #13 +
    'EE Electronic Copy 1095C' + #9 + TAX_RETURN_TYPE_EE_EeCopy_1095C + #13 +
    'State' + #9 + TAX_RETURN_TYPE_STATE + #13 +
    'State unempl.' + #9 + TAX_RETURN_TYPE_SUI + #13 +
    'County' + #9 + TAX_RETURN_TYPE_COUNTY + #13 +
    'Other' + #9 + TAX_RETURN_TYPE_OTHER + #13 +
    'Local' + #9 + TAX_RETURN_TYPE_LOCAL + #13 +
    'Summary' + #9 + TAX_RETURN_TYPE_BOTTOM + #13 +
    'EE Electronic Copy 1095 Corrected' + #9 + TAX_RETURN_TYPE_1095_CORRECTED;

// Query Type constants
  Query_Type_ComboChoices = 'Static Query' + #9 + QUERY_TYPE_STATIC + #13 +
    'Dynamic Query' + #9 + QUERY_TYPE_DYNAMIC + #13 +
    'Nested Query' + #9 + QUERY_TYPE_NESTED + #13 +
    'Execute Statement' + #9 + QUERY_TYPE_EXECUTE + #13 +
    'Cleanup Statements' + #9 + QUERY_TYPE_CLEANUP + #13 +
    'Disabled' + #9 + QUERY_TYPE_DISABLED;

// Alignment constants
  Alignment_ComboChoices = 'Left' + #9 + ALIGNMENT_LEFT + #13 +
    'Right' + #9 + ALIGNMENT_RIGHT;

// Login related changes
//----------------------------------

  User_Security_Level_ComboChoices =
    'Administrator' + #9 + USER_SEC_LEVEL_ADMINISTRATOR+ #13+
    'Manager' + #9 + USER_SEC_LEVEL_MANAGER + #13 +
    'Supervisor' + #9 + USER_SEC_LEVEL_SUPERVISOR + #13 +
    'User' + #9 + USER_SEC_LEVEL_USER;

  User_Department_ComboChoices =
    'Customer Service' + #9 + USER_DEPARTMENT_CUSTOMER_SERVICE + #13 +
    'Tax' + #9 + USER_DEPARTMENT_TAX + #13 +
    'Finance' + #9 + USER_DEPARTMENT_FINANCE + #13 +
    'Operations' + #9 + USER_DEPARTMENT_OPERATIONS + #13 +
    'IS' + #9 + USER_DEPARTMENT_IS + #13 +
    'Sales' + #9 + USER_DEPARTMENT_SALES + #13 +
    'Client' + #9 + USER_DEPARTMENT_CLIENT;

  CR_Category_ComboChoices =
    'Tax' + #9 + CR_CATEGORY_TAX + #13 +
    'Finance' + #9 + CR_CATEGORY_FINANCE + #13 +
    'Management' + #9 + CR_CATEGORY_MANAGEMENT + #13 +
    'None' + #9 + CR_CATEGORY_NONE;

//------------------------------------

  SbAchFileLimitations_ComboChoises =
    'N/A' + #9 + SB_ACH_FILE_LIMITATIONS_NONE + #13 +
    'Warn' + #9 + SB_ACH_FILE_LIMITATIONS_WARN + #13 +
    'Stop' + #9 + SB_ACH_FILE_LIMITATIONS_STOP;

  SbExceptionPaymentType_ComboChoises =
    'N/A' + #9 + PAYMENT_TYPE_NONE + #13 +
    'ACH' + #9 + PAYMENT_TYPE_ACH + #13 +
    'External Check' + #9 + PAYMENT_TYPE_EXTERNAL_CHECK + #13 +
    'Internal Check' + #9 + PAYMENT_TYPE_INTERNAL_CHECK + #13 +
    'Other' + #9 + PAYMENT_TYPE_OTHER + #13 +
    'Wire Transfer' + #9 + PAYMENT_TYPE_WIRE_TRANSFER + #13 +
    'Cash' + #9 + PAYMENT_TYPE_CASH;

  CoPayrollProcessLimitations_ComboChoises =
    'N/A' + #9 + CO_PAYROLL_PROCESS_LIMITATIONS_NONE + #13 +
    'Warn' + #9 + CO_PAYROLL_PROCESS_LIMITATIONS_WARN + #13 +
    'Stop' + #9 + CO_PAYROLL_PROCESS_LIMITATIONS_STOP;

  CoAchProcessLimitations_ComboChoises =
    'N/A' + #9 + CO_ACH_PROCESS_LIMITATIONS_NONE + #13 +
    'Warn' + #9 + CO_ACH_PROCESS_LIMITATIONS_WARN + #13 +
    'Create File w/out Co' + #9 + CO_ACH_PROCESS_LIMITATIONS_WOC + #13 +
    'Stop' + #9 + CO_ACH_PROCESS_LIMITATIONS_STOP;

  CoExceptionPaymentType_ComboChoises =
    'N/A' + #9 + PAYMENT_TYPE_NONE + #13 +
    'ACH' + #9 + PAYMENT_TYPE_ACH + #13 +
    'External Check' + #9 + PAYMENT_TYPE_EXTERNAL_CHECK + #13 +
    'Wire Transfer' + #9 + PAYMENT_TYPE_WIRE_TRANSFER + #13 +
    'Other' + #9 + PAYMENT_TYPE_OTHER + #13 +
    'Cash' + #9 + PAYMENT_TYPE_CASH;

  PaymentType_ComboChoices =
    'Check' + #9 + PAYMENT_TYPE_CHECK + #13 +
    'Direct Deposit' + #9 + PAYMENT_TYPE_DIRECTDEPOSIT + #13 +
    'None' + #9 + PAYMENT_TYPE_NONE;

  PaymentType_ComboChoices2 =
    'Cash' + #9 + PAYMENT_TYPE_CASH + #13 +
    'Check' + #9 + PAYMENT_TYPE_CHECK + #13 +
    'ACH' + #9 + PAYMENT_TYPE_ACH + #13 +
    'Wire Transfer' + #9 + PAYMENT_TYPE_WIRE_TRANSFER + #13 +
    'Other' + #9 + PAYMENT_TYPE_OTHER;

  Benefit_Deduction_Type_ComboChoices =
    'N/A' + #9 + BENEFIT_DEDUCTION_TYPE_NA + #13 +
    'EE' + #9 + BENEFIT_DEDUCTION_TYPE_EE + #13 +
    'ER' + #9 + BENEFIT_DEDUCTION_TYPE_ER;

  Termination_ComboChoices =
    'Active' + #9 + TERMINATION_ACTIVE + #13 +
    'Inactive' + #9 + TERMINATION_IN_ACTIVE + #13 +
    'Out of Business' + #9 + TERMINATION_OUT_OF_BUSINESS + #13 +
    'Other Service' + #9 + TERMINATION_OTHER_SERVICE + #13 +
    'Seasonal Active' + #9 + TERMINATION_SEASONAL_ACTIVE + #13 +
    'Seasonal Inactive' + #9 + TERMINATION_SEASONAL_INACTIVE + #13 +
    'In House' + #9 + TERMINATION_IN_HOUSE + #13 +
    'Sold' + #9 + TERMINATION_SOLD + #13 +
    'Sample' + #9 + TERMINATION_SAMPLE + #13 +
    'Never Processed' + #9 + TERMINATION_NEVER_PROCESSED + #13 +
    'In Conversion' + #9 + TERMINATION_IN_CONVERSION;


  Cobra_Events_ComboChoices =
    'Termination' + #9 + COBRA_TERMINATION_EVENT_TERMINATION + #13 +
    'Termination/Retirement/Medicare' + #9 + COBRA_TERMINATION_EVENT_TERM_RET_MED + #13 +
    'Death' + #9 + COBRA_TERMINATION_EVENT_DEATH + #13 +
    'Becoming ineligible dependent' + #9 + COBRA_TERMINATION_EVENT_INELIGIBLE_DEPENDANT + #13 +
    'Reduced hours' + #9 + COBRA_TERMINATION_EVENT_REDUCED_HOURS + #13 +
    'Leave of Absence' + #9 + COBRA_TERMINATION_EVENT_LOA + #13 +
    'Divorce/Separation' + #9 + COBRA_TERMINATION_EVENT_DIVORCE_OR_SEPERATION + #13 +
    'Loss of Coverage' + #9 + COBRA_TERMINATION_EVENT_LOSS_OF_COVERAGE  + #13 +
    'Involuntary layoff' + #9 + COBRA_TERMINATION_EVENT_INVOLUNTARY_LAYOFF  + #13 +
    'Voluntary resignation' + #9 + COBRA_TERMINATION_EVENT_VOLUNTARY_RESIGNATION  + #13 +
    'Military leave' + #9 + COBRA_TERMINATION_EVENT_MILITARY_LEAVE   + #13 +
    'Not Applicable' + #9 + COBRA_TERMINATION_EVENT_NA;



  Benefit_Plan_Status_ComboChoices =
    'Waiting' + #9 + BENEFIT_PLAN_STATUS_WAITING + #13 +
    'Pending' + #9 + BENEFIT_PLAN_STATUS_PENDING + #13 +
    'Active'  + #9 + BENEFIT_PLAN_STATUS_ACTIVE;

  Cobra_Cobra_Status_ComboChoices =
    'Waiting' + #9 + COBRA_COBRA_STATUS_WAITING + #13 +
    'Pending' + #9 + COBRA_COBRA_STATUS_PENDING + #13 +
    'Active'  + #9 + COBRA_COBRA_STATUS_ACTIVE;

  Cobra_Enrollment_Status_ComboChoices =
    'Waiting' + #9 + COBRA_ENROLLMENT_STATUS_WAITING + #13 +
    'Pending' + #9 + COBRA_ENROLLMENT_STATUS_PENDING + #13 +
    'Active'  + #9 + COBRA_ENROLLMENT_STATUS_ACTIVE + #13 +
    'Waived'  + #9 + COBRA_ENROLLMENT_STATUS_WAIVED;

  PERFORMANCE_RATINGS_Type_ComboChoices =
    'Amount' + #9 + PERFORMANCE_RATINGS_Type_Amount + #13 +
    'Additional Amount' + #9 + PERFORMANCE_RATINGS_Type_AddAmount + #13 +
    'Percent' + #9 + PERFORMANCE_RATINGS_Type_Percent + #13 +
    'No Change' + #9 + PERFORMANCE_RATINGS_Type_NoChange;

  Time_Clock_Import_Type_comboChoices =
    'Jason' + #9 + IMPORT_TYPE_JASON + #13 +
    'Simplex' + #9 + IMPORT_TYPE_SIMPLEX + #13 +
    'Time America' + #9 + IMPORT_TYPE_AMERICA + #13 +
    'Kronos' + #9 + IMPORT_TYPE_KRONOS + #13 +
    'Jantek' + #9 + IMPORT_TYPE_JANTEK + #13 +
    'Infotronics' + #9 + IMPORT_TYPE_INFOTRONICS + #13 +
    'Novatime' + #9 + IMPORT_TYPE_NOVATIME + #13 +
    'ADP' + #9 + IMPORT_TYPE_ADP + #13 +
    'Other' + #9 + IMPORT_TYPE_OTHER + #13 +
    'None' + #9 + IMPORT_TYPE_NONE;

  Time_Clock_Hardware_Type_ComboChoices =
    'Verifone' + #9 + IMPORT_HARDWARE_TYPE_VERIFONE + #13 +
    'PC' + #9 + IMPORT_HARDWARE_TYPE_PC + #13 +
    'None' + #9 + IMPORT_HARDWARE_TYPE_NONE;

  Import_Format_ComboChoices =
    'None' + #9 + IMPORT_FORMAT_NONE;

  Export_Format_ComboChoices =
    'None' + #9 + EXPORT_FORMAT_NONE;

  OBC_Bank_ComboChoices =
    'Wells Fargo' + #9 + OBC_WELLS_FARGO_NONE + #13 +
    'Citibank' + #9 + OBC_CITIBANK_NONE + #13 +
    'Wells Fargo/Keybank' + #9 + OBC_DENVER_NONE + #13 +
    'Wells Cashiers' + #9 + OBC_WELLS_CASHIERS + #13 +
    'BB&T' + #9 + OBC_BBT_MONEYGRAM + #13 +
    'Union Bank of California' + #9 + OBC_CA_UNION_BANK + #13 +
    'User defined' + #9 + OBC_USER_DEFINED;

  Bank_acc_OBC_Bank_ComboChoices =
    'Wells Fargo' + #9 + OBC_WELLS_FARGO_NONE + #13 +
    'Citibank' + #9 + OBC_CITIBANK_NONE + #13 +
    'Wells Fargo/Keybank' + #9 + OBC_DENVER_NONE + #13 +
    'Wells Cashiers' + #9 + OBC_WELLS_CASHIERS + #13 +
    'BB&T' + #9 + OBC_BBT_MONEYGRAM + #13 +
    'Union Bank of California' + #9 + OBC_CA_UNION_BANK + #13 +
    'User defined' + #9 + OBC_USER_DEFINED + #13 +
    'N/A' + #9 + OBC_NA;

  BusinesType_ComboChoices =
    'Government' + #9 + BUSINESS_TYPE_GOVERNMENT + #13 +
    'Manufacturing' + #9 + BUSINESS_TYPE_MANUFACTURING + #13 +
    'Education' + #9 + BUSINESS_TYPE_EDUCATION + #13 +
    'Medical' + #9 + BUSINESS_TYPE_MEDICAL + #13 +
    'Municipal' + #9 + BUSINESS_TYPE_MUNICIPAL + #13 +
    'Construction' + #9 + BUSINESS_TYPE_CONSTRUCTION + #13 +
    'Hospitality' + #9 + BUSINESS_TYPE_HOSPITALITY + #13 +
    'Agriculture' + #9 + BUSINESS_TYPE_AGRICULTURE + #13 +
    '944' + #9 + BUSINESS_TYPE_944 + #13 +
    'Household' + #9 + BUSINESS_TYPE_HOUSEHOLD + #13 +
    'Military' + #9 + BUSINESS_TYPE_MILITARY + #13 +
    'RailRoad' + #9 + BUSINESS_TYPE_RAILROAD + #13 +
    'Seasonally Active' + #9 + BUSINESS_TYPE_SEASONALLY_ACTIVE + #13 +
    'Other' + #9 + BUSINESS_TYPE_OTHER+ #13 +
    'N/A' + #9 + BUSINESS_TYPE_NONE;

  EmployerType_ComboChoices =
    'None apply' + #9 + EMPLOYER_TYPE_NONE + #13 +
    '501c non-govt.' + #9 + EMPLOYER_TYPE_NON_GOV + #13 +
    'State/local non-501c.' + #9 + EMPLOYER_TYPE_STATE_LOCAL_NON_501c + #13 +
    'State/local 501c.' + #9 + EMPLOYER_TYPE_STATE_LOCAL_501c + #13 +
    'Federal govt.' + #9 + EMPLOYER_TYPE_FEDERAL_GOV;

  CorporationType_ComboChoices =
    'S-Corporation' + #9 + CORPORATION_TYPE_SCORP + #13 +
    'Partnership' + #9 + CORPORATION_TYPE_PARTNERSHIP + #13 +
    'Licensed Sole Proprietor' + #9 + CORPORATION_TYPE_LIC_SOLE_PROP + #13 +
    'Corporation' + #9 + CORPORATION_TYPE_CORPORATION + #13 +
    'LLC' + #9 + CORPORATION_TYPE_LLC + #13 +
    'Other' + #9 + CORPORATION_TYPE_OTHER;

  AgencyFrequency_ComboChoices =
    'Every Pay' + #9 + FREQUENCY_TYPE_DAILY + #13 +
    'Every Scheduled Pay' + #9 + FREQUENCY_TYPE_SCHEDULED_PAY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Semi-Annual' + #9 + FREQUENCY_TYPE_SEMI_ANNUAL + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL + #13 +
    'When EE Target Met' + #9 + FREQUENCY_TYPE_TARGET_MET;

  AcaAvgHoursWorked_ComboChoices =
    'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY;

  MinimumHoursWorked_ComboChoices =
    'Week' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Month' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarter' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL + #13 +
    'None' + #9 + FREQUENCY_TYPE_NONE2;

  FrequencyType_ComboChoices =
    'Daily' + #9 + FREQUENCY_TYPE_DAILY + #13 +
    'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Bi-Weekly' + #9 + FREQUENCY_TYPE_BIWEEKLY + #13 +
    'Semi-Monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Semi-Annual' + #9 + FREQUENCY_TYPE_SEMI_ANNUAL + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL;

  ReturnFrequencyType_ComboChoices =
    'Semi-Monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL;

  LocalDepFrequencyType_ComboChoices =
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL + #13 +
    'Semi-Annual' + #9 + FREQUENCY_TYPE_SEMI_ANNUAL + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Split-Monthly ID' + #9 + FREQUENCY_TYPE_SPLIT_MONTHLY_ID+ #13 +
    //'Monthly NJ' + #9 + FREQUENCY_TYPE_MONTHLY_NJ + #13 +
    'Semi-Monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13 +
    'Quad-Monthly' + #9 + FREQUENCY_TYPE_QUAD_MONTHLY + #13 +
    'Quad-Monthly KS' + #9 + FREQUENCY_TYPE_QUAD_MONTHLY_KS + #13 +
    'Quad-Monthly IL & MA' + #9 + FREQUENCY_TYPE_QUAD_MONTHLY_IL_MA + #13 +
    'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Weekly CT' + #9 + FREQUENCY_TYPE_WEEKLY_CT + #13 +
    'Weekly CO' + #9 + FREQUENCY_TYPE_WEEKLY_CO + #13 +    
    'Semi-Weekly' + #9 + FREQUENCY_TYPE_SEMI_WEEKLY + #13 +
    'Check Date' + #9 + FREQUENCY_TYPE_CHECK_DATE + #13 +
    'Now' + #9 + FREQUENCY_TYPE_NOW + #13 +
    'Follow fed.' + #9 + FREQUENCY_TYPE_FOLLOW_FED + #13 +
    'Check Date NY' + #9 + FREQUENCY_TYPE_CHECK_DATE_NY + #13 +
    'Quarterly NY' + #9 + FREQUENCY_TYPE_QUARTERLY_NY;

  ReturnLocalDepFrequencyType_ComboChoices =
    '-- All Frequencies --'+ #9 + FREQUENCY_TYPE_DONT_CARE + #13 +
    LocalDepFrequencyType_ComboChoices;

  ReturnAgPaymentMethod_ComboChoices =
    '-- Don''t care --'#9+ AG_PAYMENT_METHOD_DONT_CARE+ #13+
    'EFT'#9+ AG_PAYMENT_METHOD_EFT+ #13+
    'Check'#9+ AG_PAYMENT_METHOD_CHECK;

  TAX945TaxCheckFilter_ComboChoices =
    '-- Don''t care --'#9+ TAX945_CHECKS_DONT_CARE+ #13+
    'Presented'#9+ TAX945_CHECKS_EXIST+ #13+
    'Not Presented'#9+ TAX945_CHECKS_DONOTEXIST;

  FederalTaxDepositFrequency_ComboChoices =
    'Next Day' + #9 + FREQUENCY_TYPE_DAILY + #13 +
    'Semi-Weekly' + #9 + FREQUENCY_TYPE_SEMI_WEEKLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY;

  SUITaxDepositFrequency_ComboChoices =
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY;

  FederalReportingFrequency_ComboChoices =
    'Semi-Weekly' + #9 + FREQUENCY_TYPE_SEMI_WEEKLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Next Day' + #9 + FREQUENCY_TYPE_NEXT_DAY + #13 +
    'None' + #9 + FREQUENCY_TYPE_NONE;

  FUITaxDepositFrequency_ComboChoices =
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL;

  LocalTaxDepositFrequency_ComboChoices =
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL;

  TCDTaxDepositFrequency_ComboChoices =
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY;

  TCDPaymentMethod_ComboChoices =
    'EFT Credit' + #9 + TAX_PAYMENT_METHOD_CREDIT + #13 +
    'EFT Debit' + #9 + TAX_PAYMENT_METHOD_DEBIT + #13 +
    'Check' + #9 + TAX_PAYMENT_METHOD_CHECK + #13 +
    'Mark as Paid' + #9 + TAX_PAYMENT_METHOD_NOTICES;

  WhenDueType_ComboChoices =
    '# of Days After Event' + #9 + WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT + #13 +
    '# of Business days After Event' + #9 + WHEN_DUE_BUS_DAYS_AFTER_THIS_EVENT + #13 +
    '# Days Before Next Event' + #9 + WHEN_DUE_SOME_DAYS_BEFORE_NEXT_EVENT + #13 +
    'Follow Fed Due Date Logic' + #9 + WHEN_DUE_FOLLOW_FEDERAL + #13 +
    'Last Day of the Next Month' + #9 + WHEN_DUE_LAST_DAY_OF_NEXT_MONTH + #13 +
    'Semi-Monthly 15th & EOM' + #9 + WHEN_DUE_SEMI_MONTHLY_15TH_EOM + #13 +
    'Last Day of Second Month' + #9 + WHEN_DUE_LAST_DAY_OF_SECOND_MONTH + #13 +
    'Follow Fed No 100K' + #9 + WHEN_DUE_FOLLOW_FEDERAL_NO_100K + #13 +
    '# of Weekdays' + #9 + WHEN_DUE_SOME_DAYS_OF_WEEKDAYS;


  ReturnWhenDueType_ComboChoices =
    '# of Days After Event' + #9 + WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT + #13 +
    'Last Day of the Next Month' + #9 + WHEN_DUE_LAST_DAY_OF_NEXT_MONTH + #13 +
    'Semi-Monthly 15th & EOM' + #9 + WHEN_DUE_SEMI_MONTHLY_15TH_EOM + #13 +
    'Last Day of Second Month' + #9 + WHEN_DUE_LAST_DAY_OF_SECOND_MONTH;


  ReturnPrintWhen_ComboChoices =
    'Always'  + #9 + PRINT_WHEN_ALWAYS + #13 +
    'No dues' + #9 + PRINT_WHEN_NO_DUES + #13 +
    'Dues'    + #9 + PRINT_WHEN_DUES;

  ReturnConsolidatedFilter_ComboChoices =
    'Always'  + #9 + CONSOLIDATED_FILTER_ALWAYS + #13 +
    'Consolidated' + #9 + CONSOLIDATED_FILTER_CONSOLIDATED + #13 +
    'Non-Consolidated'    + #9 + CONSOLIDATED_FILTER_NONCONSOLIDATED;

  ReturnPrintCoTaxServiceFilter_ComboChoices =
    'All'            + #9+ CO_TAX_SERVICE_FILTER_ALL     + #13+
    'Tax clients'    + #9+ CO_TAX_SERVICE_FILTER_WITH    + #13+
    'Non-tax clients'+ #9+ CO_TAX_SERVICE_FILTER_WITHOUT;

  Co_Sort_Level_ComboChoices =
    'Company Number' + #9 + CLIENT_LEVEL_COMPANY + #13 +
    'Company Name' + #9 + CLIENT_LEVEL_COMPANY_NAME + #13 +
    'Division Number' + #9 + CLIENT_LEVEL_DIVISION + #13 +
    'Division Name' + #9 + CLIENT_LEVEL_DIVISION_NAME + #13 +
    'Branch Number' + #9 + CLIENT_LEVEL_BRANCH + #13 +
    'Branch Name' + #9 + CLIENT_LEVEL_BRANCH_NAME + #13 +
    'Department Number' + #9 + CLIENT_LEVEL_DEPT + #13 +
    'Department Name' + #9 + CLIENT_LEVEL_DEPT_NAME + #13 +
    'Team Number' + #9 + CLIENT_LEVEL_TEAM + #13 +
    'Team Name' + #9 + CLIENT_LEVEL_TEAM_NAME;

  Co_thru_team_Level_ComboChoices =
    'Company' + #9 + CLIENT_LEVEL_COMPANY + #13 +
    'Division' + #9 + CLIENT_LEVEL_DIVISION + #13 +
    'Branch' + #9 + CLIENT_LEVEL_BRANCH + #13 +
    'Department' + #9 + CLIENT_LEVEL_DEPT + #13 +
    'Team' + #9 + CLIENT_LEVEL_TEAM;

  Div_thru_team_Level_ComboChoices =
    'Division' + #9 + CLIENT_LEVEL_DIVISION + #13 +
    'Branch' + #9 + CLIENT_LEVEL_BRANCH + #13 +
    'Department' + #9 + CLIENT_LEVEL_DEPT + #13 +
    'Team' + #9 + CLIENT_LEVEL_TEAM + #13 +
    'None' + #9 + Div_thru_team_Level_ComboChoices_None;

  Co_ReportGroupLevel_ComboChoices =
    'Company' + #9 + CLIENT_LEVEL_COMPANY + #13 +
    'Division' + #9 + CLIENT_LEVEL_DIVISION + #13 +
    'Branch' + #9 + CLIENT_LEVEL_BRANCH + #13 +
    'Department' + #9 + CLIENT_LEVEL_DEPT + #13 +
    'Team' + #9 + CLIENT_LEVEL_TEAM;

  Co_ReportGroupType_ComboChoices =
    'SummaryDetail' + #9 + CO_REPORTS_SUMMARYDETAIL + #13 +
    'Summary' + #9 + CO_REPORTS_SUMMARY + #13 +
    'Detail' + #9 + CO_REPORTS_DETAIL;

  Co_ReportGroupSort_ComboChoices =
    'GroupCode' + #9 + CO_REPORTS_GROUPCODE + #13 +
    'GroupName' + #9 + CO_REPORTS_GROUPNAME + #13 +
    'GroupNone' + #9 + CO_REPORTS_GROUPNONE;

  Co_ReportDetailSort_ComboChoices =
    'LastName' + #9 + CO_REPORTS_DETAILSORT_LASTNAME + #13 +
    'SocialNumber' + #9 + CO_REPORTS_DETAILSORT_SS + #13 +
    'EmployeeCode' + #9 + CO_REPORTS_DETAILSORT_EECODE;

  MonthNumber_ComboChoices =
    'None'   + #9 + MONTH_NUMBER_NONE + #13 +
    '1' + #9 + MONTH_NUMBER_1 + #13 +
    '2' + #9 + MONTH_NUMBER_2 + #13 +
    '3' + #9 + MONTH_NUMBER_3 + #13 +
    '4' + #9 + MONTH_NUMBER_4 + #13 +
    '5' + #9 + MONTH_NUMBER_5 + #13 +
    '6' + #9 + MONTH_NUMBER_6 + #13 +
    '7' + #9 + MONTH_NUMBER_7 + #13 +
    '8' + #9 + MONTH_NUMBER_8 + #13 +
    '9' + #9 + MONTH_NUMBER_9 + #13 +
    '10' + #9 + MONTH_NUMBER_10 + #13 +
    '11' + #9 + MONTH_NUMBER_11 + #13 +
    '12' + #9 + MONTH_NUMBER_12;

{
TAX_DEP_LEVEL_NOTICES           = 'N' ;
TAX_DEP_LEVEL_NOTICES_AND_CHECK = 'C' ;
TAX_DEP_LEVEL_CHECKS            = 'H' ;
TAX_DEP_LEVEL_NOTICES_AND_EFT   = 'E' ;
TAX_DEP_LEVEL_EFT               = 'F' ;
}

  EE_Labor_Distribution_Options_Comnbochoices =
    'Distribute Taxes' + #9 + DISTRIBUTE_TAXES + #13 +
    'Distribute Deductions' + #9 + DISTRIBUTE_DEDUCTIONS + #13 +
    'Distribute Both' + #9 + DISTRIBUTE_BOTH + #13 +
    'Distribute None' + #9 + DISTRIBUTE_NONE;


  CO_Labor_Distribution_Options_Comnbochoices =
    'Distribute Taxes' + #9 + DISTRIBUTE_TAXES + #13 +
    'Distribute Deductions' + #9 + DISTRIBUTE_DEDUCTIONS + #13 +
    'Distribute Both' + #9 + CO_DISTRIBUTE_BOTH + #13 +
    'Distribute None' + #9 + DISTRIBUTE_NONE;

  Co_Consolidation_Type_ComboChoices =
    'All Tax Returns' + #9 + CONSOLIDATION_TYPE_ALL_REPORTS + #13 +
    'State Only' + #9 + CONSOLIDATION_TYPE_STATE + #13 +
    'Fed Only' + #9 + CONSOLIDATION_TYPE_FED + #13 +
    'ACA Only' + #9 + CONSOLIDATION_TYPE_ACA;

  Co_Consolidation_Scope_ComboChoices =
    'Active Always'#9 + CONSOLIDATION_SCOPE_ALL + #13 +
    'Payroll Reports Only'#9+ CONSOLIDATION_SCOPE_PAYROLL_ONLY + #13 +
    'ACA Only'#9 + CONSOLIDATION_SCOPE_ACA;

  Co_Consolidation_Scope_ComboChoices1 =
    'Active Always'#9 + CONSOLIDATION_SCOPE_ALL + #13 +
    'Payroll Reports Only'#9+ CONSOLIDATION_SCOPE_PAYROLL_ONLY;

  Co_Consolidation_Scope_ComboChoices2 =
    'ACA Only'#9 + CONSOLIDATION_SCOPE_ACA;


  TaxTransferMethod_ComboChoices =
    'Modem' + #9 + TAX_TRANSFER_METHOD_MODEM + #13 +
    'FTP Site' + #9 + TAX_TRANSFER_METHOD_FTP + #13 +
    'Manual' + #9 + TAX_TRANSFER_METHOD_MANUAL;

  TaxPaymentMethod_ComboChoices =
    'EFT Credit' + #9 + TAX_PAYMENT_METHOD_CREDIT + #13 +
    'EFT Debit' + #9 + TAX_PAYMENT_METHOD_DEBIT + #13 +
    'Check' + #9 + TAX_PAYMENT_METHOD_CHECK + #13 +
    'Mark as Paid' + #9 + TAX_PAYMENT_METHOD_NOTICES + #13 +
    'Check 2' + #9 + TAX_PAYMENT_METHOD_NOTICES_CHECKS + #13 +
    'EFT Credit 2' + #9 + TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT + #13 +
    'EFT Debit 2' + #9 + TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT;

  TaxDepositType_ComboChoices =
    'EFT Credit' + #9 + TAX_DEPOSIT_TYPE_CREDIT + #13 +
    'EFT Debit' + #9 + TAX_DEPOSIT_TYPE_DEBIT + #13 +
    'Check' + #9 + TAX_DEPOSIT_TYPE_CHECK + #13 +
    'Notice' + #9 + TAX_DEPOSIT_TYPE_NOTICE + #13 +
    'Manual' + #9 + TAX_DEPOSIT_TYPE_MANUAL;

  SY_TaxPaymentMethod_ComboChoices =
    'EFT Credit' + #9 + TAX_PAYMENT_METHOD_CREDIT + #13 +
    'EFT Debit' + #9 + TAX_PAYMENT_METHOD_DEBIT + #13 +
    'None' + #9 + TAX_PAYMENT_METHOD_NONE_SY_ONLY + #13 +
    'Both' + #9 + TAX_PAYMENT_METHOD_BOTH_SY_ONLY;

  ReceivingAcctType_ComboChoices =
    'Checking' + #9 + RECEIVING_ACCT_TYPE_CHECKING + #13 +
    'Savings' + #9 + RECEIVING_ACCT_TYPE_SAVING + #13 +
    'Money Market' + #9 + RECEIVING_ACCT_TYPE_MONEY_MARKET;

  EFTEnrollmentStatus_ComboChoices =
    'None' + #9 + EFT_ENROLLMENT_STATUS_NONE + #13 +
    'Pending' + #9 + EFT_ENROLLMENT_STATUS_PENDING + #13 +
    'Active' + #9 + EFT_ENROLLMENT_STATUS_ACTIVE + #13 +
    'Term' + #9 + EFT_ENROLLMENT_STATUS_TERM;

  SortField_ComboChoices =
    'EE Code' + #9 + SORT_FIELD_EE_Code + #13 +
    'SS #' + #9 + SORT_FIELD_SS_Nbr + #13 +
    'Alpha' + #9 + SORT_FIELD_ALPHA;
{
  MAG_MEDIA_TYPE_DISKETTE = 'D';
  MAG_MEDIA_TYPE_MODEM = 'M';
  MAG_MEDIA_TYPE_EDI = 'E';

  MagMedia_ComboChoices =
    'Diskette' + #9 + MAG_MEDIA_TYPE_DISKETTE + #13 +
    'Modem' + #9 + MAG_MEDIA_TYPE_MODEM + #13 +
    'EDI' + #9 + MAG_MEDIA_TYPE_EDI;
}

{  CreationDate_ComboChoices =
    'Yes' + #9 + CREATION_DATE_UTIL_HASEXECUTED + #13 +
    'No' + #9 + CREATION_DATE_UTIL_HASNOTEXECUTED;}

  ReportResultType_ComboChoices =
  'Paper'+#9+ REPORT_RESULT_TYPE_PAPER + #13 +
  'Electronic'+#9+ REPORT_RESULT_TYPE_FILE + #13 +
  'Both'+#9+ REPORT_RESULT_TYPE_BOTH;

  EE_ReportResultType_ComboChoices =
  'Both'+#9+ EE_REPORT_RESULT_TYPE_BOTH  + #13 +
  'Paper'+#9+ EE_REPORT_RESULT_TYPE_PAPER + #13 +
  'Electronic'+#9+ EE_REPORT_RESULT_TYPE_FILE;

  EE_W2FormatType_ComboChoices =
  'Both'+#9+ EE_REPORT_RESULT_TYPE_BOTH + #13+
  'Paper'+#9+ EE_REPORT_RESULT_TYPE_PAPER;

  EE_ACA_FORMAT_ComboChoices =
  'None'+#9+ EE_ACA_FORMAT_NONE + #13+
  'Both'+#9+ EE_ACA_FORMAT_BOTH + #13+
  'Paper'+#9+ EE_ACA_FORMAT_PAPER;

  ACA_TYPE_ComboChoices =
  'None'+#9+ EE_ACA_TYPE_NONE + #13+
  '1095B'+#9+ EE_ACA_TYPE_1095B + #13+
  '1095C'+#9+ EE_ACA_TYPE_1095C;


  EE_W2FormatType_ComboChoices2 =
  'Both'+#9+ EE_REPORT_RESULT_TYPE_BOTH + #13+
  'Paper'+#9+ EE_REPORT_RESULT_TYPE_PAPER + #13+
  'Both'+#9+ EE_REPORT_RESULT_TYPE_BOTH2;

  EE_ACA_COVERAGE_OFFER_ComboChoices =
  '1A'+#9+ EE_ACA_COVERAGE_OFFER_1A + #13+
  '1B'+#9+ EE_ACA_COVERAGE_OFFER_1B + #13+
  '1C'+#9+ EE_ACA_COVERAGE_OFFER_1C + #13+
  '1D'+#9+ EE_ACA_COVERAGE_OFFER_1D + #13+
  '1E'+#9+ EE_ACA_COVERAGE_OFFER_1E + #13+
  '1F'+#9+ EE_ACA_COVERAGE_OFFER_1F + #13+
  '1G'+#9+ EE_ACA_COVERAGE_OFFER_1G + #13+
  '1H'+#9+ EE_ACA_COVERAGE_OFFER_1H + #13+
  '1I'+#9+ EE_ACA_COVERAGE_OFFER_1I;

  EE_ACA_SAFE_HARBOR_ComboChoices =
  'No Code'+#9+ ACA_SAFE_HARBOR_NO_CODE + #13+
  '2A'+#9+ ACA_SAFE_HARBOR_2A + #13+
  '2B'+#9+ ACA_SAFE_HARBOR_2B + #13+
  '2C'+#9+ ACA_SAFE_HARBOR_2C + #13+
  '2D'+#9+ ACA_SAFE_HARBOR_2D + #13+
  '2E'+#9+ ACA_SAFE_HARBOR_2E + #13+
  '2F'+#9+ ACA_SAFE_HARBOR_2F + #13+
  '2G'+#9+ ACA_SAFE_HARBOR_2G + #13+
  '2H'+#9+ ACA_SAFE_HARBOR_2H + #13+
  '2I'+#9+ ACA_SAFE_HARBOR_2I;


  ACA_HEALTH_CARE_START_ComboChoices =
  'None'+#9+ ACA_HEALTH_CARE_START_NONE + #13+
  'Date of Hire'+#9+ ACA_HEALTH_CARE_START_DATE_OF_HIRE + #13+
  'First of Following Month after DOH'+#9+ ACA_HEALTH_CARE_START_FOLLOWING_MONTH + #13+
  'First of Second Month after DOH'+#9+ ACA_HEALTH_CARE_START_SECOND_MONTH + #13+
  'First of Third Month after DOH'+#9+ ACA_HEALTH_CARE_START_THIRD_MONTH + #13+
  '# of Days after DOH'+#9+ ACA_HEALTH_CARE_START_DAYS_AFTER;

  GroupBox_Enlist_ProcessType_ComboChoices =
    'Enlist' + #9 + PROCESS_TYPE_ENLIST + #13+
    'Don''t Enlist' + #9 + PROCESS_TYPE_DONT_ENLIST;

  AutoLaborDist_ComboChoices =
    'Suppress' + #9 + AUTO_LABOR_DIST_SUPPRESS + #13 +
    'Show Location or Job' + #9 + AUTO_LABOR_DIST_DO_NOT_SUPPRESS + #13 +
    'Show Location and Job' + #9 + AUTO_LABOR_DIST_DO_NOT_SUPPRESS_PLUS;

  WithholdingDefault_ComboChoices =
    'Single' + #9 + WITHHOLDING_DEFAULT_SINGLE + #13 +
    'Married' + #9 + WITHHOLDING_DEFAULT_MARRIED;

  TargetsOnCheck_ComboChoices =
    'Always Print' + #9 + TARGETS_ALWAYS_PRINT + #13 +
    'Print Until Reached' + #9 + TARGETS_PRINT_TILL_REACHED + #13 +
    'Never Print' + #9 + TARGETS_NEVER_PRINT;

  CheckForm_ComboChoices =
    'Legal Bottom Stub 2/Up - Biz Forms' + #9 + CHECK_FORM_REGULAR + #13 +
    'Legal Top Stub 2/Up - Biz Forms' + #9 + CHECK_FORM_REGULAR_TOP + #13 +
    'Letter Bottom Stub - Biz Forms' + #9 + CHECK_FORM_LETTER_BTM + #13 +
    'Letter Bottom Stub -Punch- Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_STUB_PUNCH + #13 +
    'Letter Bottom Stub -Spanish- Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_SPANISH + #13 +
    'Letter Bottom Stub Rates - Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_RATE + #13 +
    'Letter Bottom Stub - F&F' + #9 + CHECK_FORM_LETTER_BTM_FF + #13 +
    'Letter Bottom Stub/CO Addr - Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_WITH_ADDR + #13 +
    'Pressure Seal Letter - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL + #13 +
    'Pressure Seal Letter Greatland' + #9 + CHECK_FORM_PRESSURE_SEAL_GREATLAND+ #13+
    'Pressure Seal Legal - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL + #13 +
    'Pressure Seal Legal No DBDT - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT + #13 +
    'Pressure Seal Legal w/ovrflw - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW + #13 +
    'Pressure Seal Legal Moore' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE + #13 +
    'Pressure Seal Legal Greatland' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND + #13 +
    'Pressure Seal Legal VersaSeal' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL + #13 +
    'Pressure Seal Legal VS CD' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD + #13 +
    'Pressure Seal Legal F&F' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_FF + #13 +
    'Check Letter Stub Only' + #9 + CHECK_FORM_LETTER_BTM_STUB_ONLY + #13 +

    'Legal Bottom Stub 2/Up(New) - Biz Forms' + #9 + CHECK_FORM_REGULAR_NEW + #13 +
    'Legal Top Stub 2/Up (New) - Biz Forms' + #9 + CHECK_FORM_REGULAR_TOP_NEW + #13 +
    'Letter Bottom Stub (New) - Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_NEW + #13 +
    'Letter Bottom Stub(New)-Spanish-BizFms' + #9 + CHECK_FORM_LETTER_BTM_SPANISH_NEW + #13 +
    'Letter Bottom 2 Stubs (New) - BizForms' + #9 + CHECK_FORM_LETTER_BTM_2_STUB_NEW + #13 +
    'Letter Bottom Stub Rates(New)-Biz Forms' + #9 + CHECK_FORM_LETTER_BTM_RATE_NEW + #13 +
    'Letter Bottom Stub - F&F (New)' + #9 + CHECK_FORM_LETTER_BTM_FF_NEW + #13 +
    'Letter Bottom Stub/CO Addr(New)-BizFms' + #9 + CHECK_FORM_LETTER_BTM_WITH_ADDR_NEW + #13 +
    'Letter Top Stub (New) - Biz Forms' + #9 + CHECK_FORM_LETTER_TOP_NEW + #13 +
    'Pressure Seal Letter(New) - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL_NEW + #13 +
    'Pressure Seal Letter Greatland (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW+ #13+
    'Pressure Seal Legal(New) - Biz Forms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW + #13 +
    'Pressure Seal Legal(New)w/ovrfl-BizFms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW + #13 +
    'Pressure Seal Legal(New)No DBDT-BizFms' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW + #13 +
    'Pressure Seal Legal Moore (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW + #13 +
    'Pressure Seal Legal Greatland (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW + #13 +
    'Pressure Seal Legal VersaSeal (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW + #13 +
    'Pressure Seal Legal VS CD (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW + #13 +
    'Pressure Seal Legal F&F (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW + #13 +
    'Pressure Seal Legal - ANSI (New)' + #9 + CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW + #13 +
    'Check Letter Stub Only (New)' + #9 + CHECK_FORM_LETTER_BTM_STUB_ONLY_NEW;

  AgencyHolidayRules_ComboBoxChoices =
    'Pay taxes last day before the date'+ #9+ HOLIDAY_RULE_BEFORE+ #13+
    'Pay taxes next day after the date'+ #9+ HOLIDAY_RULE_AFTER+ #13+
    'Pay taxes on the date'+ #9+ HOLIDAY_RULE_DISREGARD;

  ScheduledCalcMethod_ComboBoxChoices =
    'Fixed' + #9 + CALC_METHOD_FIXED + #13 +
    '% of Gross' + #9 + CALC_METHOD_GROSS + #13 +
    '% of Net' + #9 + CALC_METHOD_NET + #13 +
    '% of Taxable Wages' + #9 + CALC_METHOD_TAX_WAGES + #13 +
    '% of Disposable Earnings' + #9 + CALC_METHOD_DISP_EARN + #13 +
    'Garnishment' + #9 + CALC_METHOD_GARNISHMENT + #13 +
    'Garnishment % of Gross' + #9 + CALC_METHOD_GARNISHMENT_GROSS + #13 +
    '% of E/D Group Amt' + #9 + CALC_METHOD_PERC + #13 +
    '% of E/D Group Hours' + #9 + CALC_METHOD_PERC_HOURS + #13 +
    '% of E/D Group Amt and Hours' + #9 + CALC_METHOD_PERC_AMT_HOURS + #13 +
    '% of Primary Rate' + #9 + CALC_METHOD_PERC_PRIMARY_RATE + #13 +
    'Levy' + #9 + CALC_METHOD_LEVY + #13 +
    'Pension Match' + #9 + CALC_METHOD_PENSION + #13 +
    'Rate * Hours' + #9 + CALC_METHOD_RATE_HOURS + #13 +
    'Rate * Amount' + #9 + CALC_METHOD_RATE_AMOUNT + #13 +
    'State SDI' + #9 + CALC_METHOD_NY_SDI + #13 +
    'Washington L & I' + #9 + CALC_METHOD_WASHINGTON_L_I + #13 +
    'NY Child Support' + #9 + CALC_METHOD_NYCHILDSUPPORT + #13 +
    'None' + #9 + CALC_METHOD_NONE;


  LocalCalcMethod_ComboChoices =
    'Flat' + #9 + CALC_METHOD_FIXED + #13 +
    'Tax Tables' + #9 + CALC_METHOD_TAX_TABLES + #13 +
    'Tax Tables Reversed' + #9 + CALC_METHOD_TAX_TABLES_REVERSED + #13 +
    '% of Gross' + #9 + CALC_METHOD_GROSS + #13 +
    '% of Gross Tax Adj by Misc Amount' + #9 + CALC_METHOD_GROSS_ADJ + #13 +
    '% of Gross Minus Exemptions' + #9 + CALC_METHOD_GROSS_MINUS_EXEMPTIONS + #13 +
    '% of State Tax' + #9 + CALC_METHOD_PERC_STATE + #13 +
    '% of Medicare Wages' + #9 + CALC_METHOD_PERC_MEDICARE_WAGES;

  EeToaOperCode_ComboChoices =
    'Accrual'+ #9+ EE_TOA_OPER_CODE_ACCRUAL+ #13+
    'Used'+ #9+ EE_TOA_OPER_CODE_USED+ #13+
    'Void'+ #9+ EE_TOA_OPER_CODE_VOID+ #13+
    'Adjustment'+ #9+ EE_TOA_OPER_CODE_ADJ+ #13+
    'Reset'+ #9+ EE_TOA_OPER_CODE_RESET+ #13+
    'Rollover'+ #9+ EE_TOA_OPER_CODE_ROLLOVER+ #13+
    'Incoming Rollover'+ #9+ EE_TOA_OPER_CODE_ROLLOVER_IN+ #13+
    'Manual Adjustment'+ #9+ EE_TOA_OPER_CODE_MANUAL_ADJ + #13+
    'Pending'+ #9+ EE_TOA_OPER_CODE_REQUEST_PENDING+ #13+
    'Approved'+ #9+ EE_TOA_OPER_CODE_REQUEST_APPROVED+ #13+
    'Denied'+ #9+ EE_TOA_OPER_CODE_REQUEST_DENIED;

  TimeOffAccrualFrequency_ComboChoices =
    'Per pay period' + #9 + TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD + #13 +
    'Monthly' + #9 + TIME_OFF_ACCRUAL_FREQ_NONTHLY + #13 +
    'Quarterly' + #9 + TIME_OFF_ACCRUAL_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + TIME_OFF_ACCRUAL_FREQ_SEMI_ANNUAL + #13 +
    'Annual' + #9 + TIME_OFF_ACCRUAL_FREQ_ANNUAL;

  TimeOffRolloverFrequency_ComboChoices =
    'If set on EE level/ None' + #9 + TIME_OFF_ACCRUAL_FREQ_NONE + #13 +
    'Once on Rollover Date' + #9 + TIME_OFF_ACCRUAL_FREQ_ONCE + #13 +
    'Once # days after Effective Date' + #9 + TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_DAYS + #13 +
    'Once # months after Effective Date' + #9 + TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_MONTHS + #13 +
    'Per pay period' + #9 + TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD + #13 +
    'Monthly' + #9 + TIME_OFF_ACCRUAL_FREQ_NONTHLY + #13 +
    'Quarterly' + #9 + TIME_OFF_ACCRUAL_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + TIME_OFF_ACCRUAL_FREQ_SEMI_ANNUAL + #13 +
    'Annual' + #9 + TIME_OFF_ACCRUAL_FREQ_ANNUAL;

  TimeOffAccrualPayrollOfMonth_ComboChoices =
    'First' + #9 + TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_FIRST + #13 +
    'Last' + #9 + TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_LAST + #13 +
    'Nearest to 15''th' + #9 + TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_MIDDLEST + #13 +
    'None' + #9 + TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE;

  TIME_OFF_PROCESS_ORDER_USE_RESET_ROLLOVER_ACCRUE = '9';
  TIME_OFF_PROCESS_ORDER_USE_RESET_ACCRUE_ROLLOVER = '2';
  TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_RESET_ACCRUE = '3';
  TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_ACCRUE_RESET = '4';
  TIME_OFF_PROCESS_ORDER_USE_ACCRUE_ROLLOVER_RESET = '5';
  TIME_OFF_PROCESS_ORDER_USE_ACCRUE_RESET_ROLLOVER = '6';
  TIME_OFF_PROCESS_ORDER_RESET_USE_ROLLOVER_ACCRUE = '7';
  TIME_OFF_PROCESS_ORDER_RESET_USE_ACCRUE_ROLLOVER = '8';
  TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_USE_ACCRUE = '1';
  TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_ACCRUE_USE = 'A';
  TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_USE_ROLLOVER = 'B';
  TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_ROLLOVER_USE = 'C';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_RESET_ACCRUE = 'D';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_ACCRUE_RESET = 'E';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_USE_ACCRUE = 'F';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_ACCRUE_USE = 'G';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_USE_RESET = 'H';
  TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_RESET_USE = 'I';
  TIME_OFF_PROCESS_ORDER_ACCRUE_USE_RESET_ROLLOVER = 'J';
  TIME_OFF_PROCESS_ORDER_ACCRUE_USE_ROLLOVER_RESET = 'K';
  TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_USE_ROLLOVER = 'L';
  TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_ROLLOVER_USE = 'M';
  TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_USE_RESET = 'N';
  TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_RESET_USE = 'O';

  TimeOffProcessOrder_ComboChoices =
    'Use, Reset, Rollover, Accrue'#9+ TIME_OFF_PROCESS_ORDER_USE_RESET_ROLLOVER_ACCRUE+ #13+
    'Use, Reset, Accrue, Rollover'#9+ TIME_OFF_PROCESS_ORDER_USE_RESET_ACCRUE_ROLLOVER+ #13+
    'Use, Rollover, Reset, Accrue'#9+ TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_RESET_ACCRUE+ #13+
    'Use, Rollover, Accrue, Reset'#9+ TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_ACCRUE_RESET+ #13+
    'Use, Accrue, Rollover, Reset'#9+ TIME_OFF_PROCESS_ORDER_USE_ACCRUE_ROLLOVER_RESET+ #13+
    'Use, Accrue, Reset, Rollover'#9+ TIME_OFF_PROCESS_ORDER_USE_ACCRUE_RESET_ROLLOVER+ #13+
    'Reset, Use, Rollover, Accrue'#9+ TIME_OFF_PROCESS_ORDER_RESET_USE_ROLLOVER_ACCRUE+ #13+
    'Reset, Use, Accrue, Rollover'#9+ TIME_OFF_PROCESS_ORDER_RESET_USE_ACCRUE_ROLLOVER+ #13+
    'Reset, Rollover, Use, Accrue'#9+ TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_USE_ACCRUE+ #13+
    'Reset, Rollover, Accrue, Use'#9+ TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_ACCRUE_USE+ #13+
    'Reset, Accrue, Use, Rollover'#9+ TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_USE_ROLLOVER+ #13+
    'Reset, Accrue, Rollover, Use'#9+ TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_ROLLOVER_USE+ #13+
    'Rollover, Use, Reset, Accrue'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_RESET_ACCRUE+ #13+
    'Rollover, Use, Accrue, Reset'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_ACCRUE_RESET+ #13+
    'Rollover, Reset, Use, Accrue'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_USE_ACCRUE+ #13+
    'Rollover, Reset, Accrue, Use'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_ACCRUE_USE+ #13+
    'Rollover, Accrue, Use, Reset'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_USE_RESET+ #13+
    'Rollover, Accrue, Reset, Use'#9+ TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_RESET_USE+ #13+
    'Accrue, Use, Reset, Rollover'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_USE_RESET_ROLLOVER+ #13+
    'Accrue, Use, Rollover, Reset'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_USE_ROLLOVER_RESET+ #13+
    'Accrue, Reset, Use, Rollover'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_USE_ROLLOVER+ #13+
    'Accrue, Reset, Rollover, Use'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_ROLLOVER_USE+ #13+
    'Accrue, Rollover, Use, Reset'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_USE_RESET+ #13+
    'Accrue, Rollover, Reset, Use'#9+ TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_RESET_USE;

  AccrualCalcMethod_ComboChoices =
    'Balances only' + #9 + TIME_OFF_ACCRUAL_CALC_METHOD_BALANCES_ONLY + #13 +
    'Accrue hours' + #9 + TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_HOURS + #13 +
    'Accrue by frequency' + #9 + TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_BY_FREQ + #13 +
    'Average the hours' + #9 + TIME_OFF_ACCRUAL_CALC_METHOD_AVERAGE_THE_HOURS + #13+
    'Custom Semi-Monthly' + #9 + TIME_OFF_ACCRUAL_CALC_METHOD_COLUMBIA_EDP + #13;

  AccrualResetCode_ComboChoices =
    'None' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_NONE + #13+
    'Once # days after Effective Date' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_HIRE_DATE_AFTER_DAYS + #13 +
    'Once # months after Effective Date' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_HIRE_DATE_AFTER_MONTHS + #13 +
    'Per pay period' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_PER_PAY_PERIOD + #13 +
    'Monthly' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_MONTHLY + #13 +
    'Quarterly' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_QARTERLY + #13 +
    'Semi-Annual' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_SEMI_ANNUAL + #13 +
    'Annual' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL+ #13+
    'Annual(by hire date)' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL_HIRE_DATE+ #13+
    'Annual(by reset date)' + #9 + TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL_RESET_DATE;

  AccrualActive_ComboChoices =
    'All EEs' + #9 + TIME_OFF_ACCRUAL_ACTIVE_ALL_EES + #13 +
    'Active EEs only' + #9 + TIME_OFF_ACCRUAL_ACTIVE_ALL_ACTIVE_EES + #13 +
    'EEs with reg/man checks only' + #9 + TIME_OFF_ACCRUAL_ACTIVE_ALL_EES_HAVE_CHECK + #13;

  AnnualResetCode_ComboChoices =
    'First' + #9 + ANN_RES_CODE_1 + #13 +
    'Second' + #9 + ANN_RES_CODE_2 + #13 +
    'Third' + #9 + ANN_RES_CODE_3 + #13;

  AccrualDivDesc_ComboChoices =
    'Years' + #9 + ACC_DIV_DESC_YEARS + #13 +
    'Months' + #9 + ACC_DIV_DESC_MONTHS + #13 +
    'Weeks' + #9 + ACC_DIV_DESC_WEEKS + #13;

  SystemReportType_ComboChoices =
    'Normal' + #9 + SYSTEM_REPORT_TYPE_NORMAL + #13 +
    'Multiclient' + #9 + SYSTEM_REPORT_TYPE_MULTICLIENT + #13 +
    'Tax Return' + #9 + SYSTEM_REPORT_TYPE_TAX_RETURN + #13 +
    'Tax Return XML' + #9 + SYSTEM_REPORT_TYPE_TAX_RETURN_XML + #13 +
    'Tax Return Multiclient' + #9 + SYSTEM_REPORT_TYPE_TAX_RETURN_MULTICLIENT + #13 +
    'Check' + #9 + SYSTEM_REPORT_TYPE_CHECK + #13 +
    'ASCII File' + #9 + SYSTEM_REPORT_TYPE_ASCII + #13 +
    'SB Global' + #9 + SYSTEM_REPORT_TYPE_SB_GLOBAL + #13 +
    'Tax Coupon' + #9 + SYSTEM_REPORT_TYPE_TAXCOUPON + #13 +
    'HR' +#9 + SYSTEM_REPORT_TYPE_HR + #13 +
    'Mail Merge' +#9 + SYSTEM_REPORT_TYPE_MERGE;

  RenewalStatus_ComboChoices =
    'None' + #9 + RENEWAL_STATUS_NONE + #13 +
    'Monthly' + #9 + RENEWAL_STATUS_MONTHLY + #13 +
    'Quarterly' + #9 + RENEWAL_STATUS_QUARTERLY + #13 +
    'Semi-Annual' + #9 + RENEWAL_STATUS_SEMI_ANNUAL + #13 +
    'Annual' + #9 + RENEWAL_STATUS_ANNUAL+ #13 +
    '18 Months' + #9 + RENEWAL_STATUS_18_MONTHS+ #13 +
    '2 Years' + #9 + RENEWAL_STATUS_2_YEARS+ #13 +
    '3 Years' + #9 + RENEWAL_STATUS_3_YEARS+ #13 +
    '5 Years' + #9 + RENEWAL_STATUS_5_YEARS+ #13 +
    '10 Years' + #9 + RENEWAL_STATUS_10_YEARS+ #13 +
    'Never' + #9 + RENEWAL_STATUS_NEVER;

{
  Sorting Options (used for Reports and Page-Breaking...)
  If    Sort = '3' (Div+Br+Dept),
  then  Break could = 1, 2, or 3.
}
  SORT_ComboChoices =
    'Summary' + #9 + SORT_SUMMARY + #13 +
    'Numeric' + #9 + SORT_NUMERIC + #13 +
    'Alpha' + #9 + SORT_ALPHA + #13 +
    'Summary by DBDT' + #9 + SORT_SUMMARY_BY_DBDT + #13 +
    'Numeric by DBDT' + #9 + SORT_NUMERIC_BY_DBDT + #13 +
    'Alpha by DBDT' + #9 + SORT_ALPHA_BY_DBDT + #13;

{BREAK_ComboChoices =
'Division'                    +#9+   BREAK_D    +#13+
'Branch'                      +#9+   BREAK_DB   +#13+
'Department'                  +#9+   BREAK_DBD  +#13+
'Team'                        +#9+   BREAK_DBDT +#13+
'None'                        +#9+   BREAK_NONE ;
}

  SB_Bank_Account_Suppress_Offset_Choices =
    'Yes' + #9 + SB_BANK_SUPPRESS_OFFSET_YES + #13 +
    'Except DD' + #9 + SB_BANK_SUPPRESS_OFFSET_EXCEPT_DD + #13 +
    'No' + #9 + SB_BANK_SUPPRESS_OFFSET_NO;

  SBServicesTaxType_ComboChoices =
    'N/A' + #9 + GROUP_BOX_NOT_APPLICATIVE + #13 +
    'ER OASDI' + #9 + TAX_LIABILITY_TYPE_EE_OASDI + #13 +
    'ER Medicare' + #9 + TAX_LIABILITY_TYPE_ER_MEDICARE + #13 +
    'ER FUI' + #9 + TAX_LIABILITY_TYPE_ER_FUI + #13 +
    'ER SDI' + #9 + TAX_LIABILITY_TYPE_ER_SDI + #13 +
    'ER SUI' + #9 + TAX_LIABILITY_TYPE_ER_SUI + #13 +
    'COBRA' + #9 + TAX_LIABILITY_TYPE_COBRA_CREDIT;

  GroupBox_YesNoLast4_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Last 4' + #9 + GROUP_BOX_LAST4;

  FedTaxLiabilityType_ComboChoices =
    'Federal' + #9 + TAX_LIABILITY_TYPE_FEDERAL + #13 +
    'EE OASDI' + #9 + TAX_LIABILITY_TYPE_EE_OASDI + #13 +
    'EE Medicare' + #9 + TAX_LIABILITY_TYPE_EE_MEDICARE + #13 +
    'EE EIC' + #9 + TAX_LIABILITY_TYPE_EE_EIC + #13 +
    'EE 945 Backup Withholding' + #9 + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING + #13 +
    'ER OASDI' + #9 + TAX_LIABILITY_TYPE_ER_OASDI + #13 +
    'ER Medicare' + #9 + TAX_LIABILITY_TYPE_ER_MEDICARE + #13 +
    'ER FUI' + #9 + TAX_LIABILITY_TYPE_ER_FUI + #13 +
    'Federal 945' + #9 + TAX_LIABILITY_TYPE_FEDERAL_945 + #13 +
    'Cobra Credit' + #9 + TAX_LIABILITY_TYPE_COBRA_CREDIT;

  StateTaxLiabilityType_ComboChoices =
    'State' + #9 + TAX_LIABILITY_TYPE_STATE + #13 +
    'EE SDI' + #9 + TAX_LIABILITY_TYPE_EE_SDI + #13 +
    'ER SDI' + #9 + TAX_LIABILITY_TYPE_ER_SDI;

  HomeStateType_ComboChoices =
    'EE Default' + #9 + HOME_STATE_TYPE_DEFAULT + #13 +
    'Payroll Override' + #9 + HOME_STATE_TYPE_OVERRIDE;

  UseDBAOnTaxReturn_ComboChoices =
    'DBA' + #9 + USE_DBA_ON_TAX_RETURN_DBA + #13 +
    'Legal' + #9 + USE_DBA_ON_TAX_RETURN_LEGAL + #13 +
    'Primary' + #9 + USE_DBA_ON_TAX_RETURN_PRIMARY;         

  TaxLiabilityAdjustmentType_ComboChoices =
    'Line 7d' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_7D + #13 +
    'Line 7e' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_7E + #13 +
    'Interest' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_INTEREST + #13 +
    'Penalty' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_PENALTY + #13 +
    'Prior Quarter' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_PRIOR_QUARTER + #13 +
    'New Client Setup' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + #13 +
    'Rounding adjustment' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END + #13 +
    'Rounding to dollars' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_ROUNDING_TO_DOLLAR + #13 +
    'Credit overpaid' +#9+ TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT +#13+
    'Conversion' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION +#13+
    'State Tax Credit'+ #9+ TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT+ #13+
    'Other' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_OTHER+ #13+
    'N/A' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_NONE;

  FedTaxLiabilityAdjustmentType_ComboChoices =
    'Line 7d' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_7D + #13 +
    'Line 7e' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_7E + #13 +
    'Interest' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_INTEREST + #13 +
    'Penalty' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_PENALTY + #13 +
    'Prior Quarter' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_PRIOR_QUARTER + #13 +
    'New Client Setup' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + #13 +
    'Rounding adjustment' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END + #13 +
    'Rounding to dollars' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_ROUNDING_TO_DOLLAR + #13 +
    'Credit overpaid' +#9+ TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT +#13+
    'Conversion' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION +#13+
    'State Tax Credit'+ #9+ TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT+ #13+
    'Other' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_OTHER+ #13+
    '3121q' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_3121q+ #13+
    'Form 6765 Credit' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_Form6765Credit+ #13+
    'Form 6765 Prior Period' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_Form6765PriorPeriod+ #13+
    'N/A' + #9 + TAX_LIABILITY_ADJUSTMENT_TYPE_NONE;

  ValueType_ComboChoices =
    'Amount' + #9 + VALUE_TYPE_AMOUNT + #13 +
    'Percentage' + #9 + VALUE_TYPE_PERCENTAGE + #13 +
    'None' + #9 + VALUE_TYPE_NONE;

  ScheduledFrequency_ComboChoices =
    'Annual' + #9 + SCHED_FREQ_ANNUALLY + #13 +
    'Closest Scheduled to 15th of Month' + #9 + SCHED_FREQ_MID_SCHED_PAY  + #13 +
    'Every Pay' + #9 + SCHED_FREQ_DAILY + #13 +
    'Every Scheduled Pay' + #9 + SCHED_FREQ_SCHEDULED_PAY + #13 +
    'Every Other Scheduled Pay' + #9 + SCHED_FREQ_EVRY_OTHER_PAY + #13 +
    'First Scheduled of Month' + #9 + SCHED_FREQ_FIRST_SCHED_PAY + #13 +
    'Last Scheduled of Month' + #9 + SCHED_FREQ_LAST_SCHED_PAY + #13 +
    'Quarterly' + #9 + SCHED_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + SCHED_FREQ_SEMI_ANNUALLY;

  ScheduledEDFrequency_ComboChoices =
    'Every Pay' + #9 + SCHED_FREQ_DAILY + #13 +
    'Every Scheduled Pay' + #9 + SCHED_FREQ_SCHEDULED_PAY + #13 +
    'Every Other Scheduled Pay' + #9 + SCHED_FREQ_EVRY_OTHER_PAY + #13 +
    'First Scheduled of Month' + #9 + SCHED_FREQ_FIRST_SCHED_PAY + #13 +
    'Last Scheduled of Month' + #9 + SCHED_FREQ_LAST_SCHED_PAY + #13 +
    'Closest Scheduled to 15th of Month' + #9 + SCHED_FREQ_MID_SCHED_PAY + #13 +
    'Quarterly' + #9 + SCHED_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + SCHED_FREQ_SEMI_ANNUALLY + #13 +
    'Annual' + #9 + SCHED_FREQ_ANNUALLY + #13 +
    'User Entered' + #9 + SCHED_FREQ_USER_ENTERED;

  ScheduledFrequency_Reports_ComboChoices =
    'Every Pay' + #9 + SCHED_FREQ_DAILY + #13 +
    'Every Scheduled Pay' + #9 + SCHED_FREQ_SCHEDULED_PAY + #13 +
    'Monthly' + #9 + SCHED_FREQ_SCHEDULED_MONTHLY + #13 +
    'Quarterly' + #9 + SCHED_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + SCHED_FREQ_SEMI_ANNUALLY + #13 +
    'Annual' + #9 + SCHED_FREQ_ANNUALLY;

  ScheduledFrequency_Billing_ComboChoices =
    'Every Pay' + #9 + SCHED_FREQ_DAILY + #13 +
    'Every Scheduled Pay' + #9 + SCHED_FREQ_SCHEDULED_PAY + #13 +
    'Monthly' + #9 + SCHED_FREQ_SCHEDULED_MONTHLY + #13 +
    'Quarterly' + #9 + SCHED_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + SCHED_FREQ_SEMI_ANNUALLY + #13 +
    'Annual' + #9 + SCHED_FREQ_ANNUALLY + #13 +
    'One Time' + #9 + SCHED_FREQ_ONE_TIME;

  LocalFrequency_ComboChoices =
    'Every Pay' + #9 + SCHED_FREQ_DAILY + #13 +
    'Every Scheduled Pay' + #9 + SCHED_FREQ_SCHEDULED_PAY + #13 +
    'Every Other Scheduled Pay' + #9 + SCHED_FREQ_EVRY_OTHER_PAY + #13 +
    'First Scheduled of Month' + #9 + SCHED_FREQ_FIRST_SCHED_PAY + #13 +
    'Last Scheduled of Month' + #9 + SCHED_FREQ_LAST_SCHED_PAY + #13 +
    'Closest Scheduled to 15th of Month' + #9 + SCHED_FREQ_MID_SCHED_PAY + #13 +
    'Quarterly' + #9 + SCHED_FREQ_QUARTERLY + #13 +
    'Annual' + #9 + SCHED_FREQ_ANNUALLY;

  WeekNumberToPrint_ComboChoices =
    'First Scheduled of Month' + #9 + SCHED_FREQ_FIRST_SCHED_PAY + #13 +
    'Last Scheduled of Month' + #9 + SCHED_FREQ_LAST_SCHED_PAY + #13 +
    'Closest Scheduled to 15th of Month' + #9 + SCHED_FREQ_MID_SCHED_PAY;

  INCLUDE_REPT_OR_ADDRESS_ComboChoices =
    'Show and Override Address on Checks' + #9 + INCLUDE_REPT_AND_ADDRESS_ON_CHECK + #13 +
    'Show' + #9 + INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK+ #13 +
    'Hide' + #9 + EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;

  TipMinWageMakeUp_ComboChoices =
    'None' + #9 + TIP_MINWAGE_NONE + #13 +
    'Pay Minimum Wage Makeup' + #9 + TIP_MINWAGE_PAY_MAKEUP + #13 +
    'Force Tip Make Up' + #9 + TIP_MINWAGE_PAY_FORCE_TIP_MAKEUP;

  PW_MinWageMakeUpMethod_ComboChoices =

  'None' + #9 + PW_MAKEUP_METHOD_NONE + #13 +
    'Use EE''s First Rate of Pay' + #9 + PW_MAKEUP_METHOD_EE_FIRST_RATE + #13 +
    'Use Piecework Minimum Wage' + #9 + PW_MAKEUP_METHOD_PIECEWORK + #13 +
    'Use Job Rate' + #9 + PW_MAKEUP_METHOD_JOB_RATE;

  ED_ComboBoxChoices =

  'Deferred Comp' + #9 + ED_ST_DEFERRED_COMP + #13 +
    'Mass Retirement' + #9 + ED_ST_MASS_RETIREMENT + #13 +
    'Pension' + #9 + ED_ST_PENSION + #13 +
    '401k' + #9 + ED_ST_401K + #13 +
    '403b' + #9 + ED_ST_403B + #13 +
    '457' + #9 + ED_ST_457 + #13 +
    '501c' + #9 + ED_ST_501C + #13 +
    'Simple' + #9 + ED_ST_SIMPLE + #13 +
    'SEP' + #9 + ED_ST_SEP + #13 +
    'Roth 401k' + #9 + ED_ST_ROTH_401K + #13 +
    'Roth 403b' + #9 + ED_ST_ROTH_403B + #13 +
    'Roth IRA' + #9 + ED_ST_ROTH_IRA + #13 +
    'Pension Catch up' + #9 + ED_ST_CATCH_UP + #13 +
    '401k Catch up' + #9 + ED_ST_401K_CATCH_UP + #13 +
    '403b Catch up' + #9 + ED_ST_403B_CATCH_UP + #13 +
    '457 Catch up' + #9 + ED_ST_457_CATCH_UP + #13 +
    '501c Catch up' + #9 + ED_ST_501C_CATCH_UP + #13 +
    'Roth Catch up' + #9 + ED_ST_ROTH_CATCH_UP + #13 +
    'Simple Catch up' + #9 + ED_ST_SIMPLE_CATCH_UP + #13 +
    'TIAA-CREF' + #9 + ED_ST_TIAA_CREFF + #13 +
    'Section 125 Cafeteria Plan Insurance' + #9 + ED_ST_PRETAX_INS + #13 +
    'Section 125 Cafeteria Plan S/B Insurance' + #9 + ED_ST_SB_PREAX_INS + #13 +
    'Section 125 Cafeteria Plan Dep Care' + #9 + ED_ST_DEPENDENT_CARE + #13 +
    'Discriminatory S125 Post Tax' + #9 + ED_ST_DISCRIMINARY_S125 + #13 +
    'State Pension' + #9 + ED_ST_STATE_PENSION + #13 +
    'Special Taxed Deduction' + #9 + ED_ST_SP_TAXED_DEDUCTION + #13 +
//    'Special Taxed Healthcare' + #9 + ED_ST_HEALTHCARE + #13 +
    'Nonqualified Stock Below Value' + #9 + ED_ST_NON_QUAL_BELOW + #13 +
    'Nonqualified Stock Up to Value' + #9 + ED_ST_NON_QUAL_UP + #13 +
    'Incentive Stock Options' + #9 + ED_ST_INCENTIVE_STOCK + #13 +
    '1099 Earnings' + #9 + ED_ST_EXEMPT_EARNINGS + #13 +
    'Special Taxed 1099 Earnings' + #9 + ED_ST_SP_TAXED_1099_EARNINGS + #13 +
    'Non-Taxable/No 1099' + #9 + ED_ST_NON_TAXABLE_EARNING + #13 +
    'Specially Taxed Earning' + #9 + ED_ST_SP_TAXED_EARNING + #13 +
    '1099-R' + #9 + ED_ST_1099_R + #13 +
    '1099-R Specially Taxed' + #9 + ED_ST_1099_R_SP_TAXED + #13 +
    '1099 DIV' + #9 + ED_ST_1099_DIV + #13 +
    '1099 Interest' + #9 + ED_ST_INTEREST + #13 +
    'HSA Single' + #9 + ED_ST_HSA_SINGLE + #13 +
    'HSA Family' + #9 + ED_ST_HSA_FAMILY + #13 +
    'ER Insurance Premium' + #9 + ED_MEMO_ER_INSURANCE_PREMIUM + #13 +
    'ER HSA Single' + #9 + ED_MEMO_ER_HSA_SINGLE + #13 +
    'ER HSA Family' + #9 + ED_MEMO_ER_HSA_FAMILY + #13 +
    'ER Employee Taxable HSA Single' + #9 + ED_EWOD_TAXABLE_ER_HSA_SINGLE + #13 +
    'ER Employee Taxable HSA Family' + #9 + ED_EWOD_TAXABLE_ER_HSA_FAMILY + #13 +
    'HSA Catch up' + #9 + ED_ST_HSA_CATCH_UP + #13 +

  'Short Term Third Party Sick Pay' + #9 + ED_3RD_SHORT + #13 +
    'Long Term Third Party Sick Pay' + #9 + ED_3RD_LONG + #13 +
    'Non Taxable Third Party Sick Pay' + #9 + ED_3RD_NON_TAX + #13 +
    'CA Supplemental Religious' + #9 + ED_3RD_CA_SUP_RELIGIOUS + #13 +
    'CA Supplemental Sole Stockholder' + #9 + ED_3RD_CA_SUP_STOCKHOLDER + #13 +

  'Memo' + #9 + ED_MEMO_SIMPLE + #13 +
    'Tipped Sales' + #9 + ED_MEMO_TIPPED_SALES + #13 +
    'Pension Match' + #9 + ED_MEMO_PENSION + #13 +
    'Pension Match with SUI override' + #9 + ED_MEMO_PENSION_SUI + #13 +
    'Memo-Additional' + #9 + ED_MEMO_LINE_9 + #13 +
    'Washington ER L & I' + #9 + ED_MEMO_WASHINGTON_L_I + #13 +
    'Wyoming Workers Comp' + #9 + ED_MEMO_WY_WORKERS_COMP + #13 +
    'New Mexico ER Workers Comp' + #9 + ED_MEMO_NM_WORKERS_COMP + #13 +
    'Cobra Credit' + #9 + ED_MEMO_COBRA_CREDIT + #13 +
    'Tipped Credit Makeup' + #9 + ED_MU_TIPPED_CREDIT_MAKEUP + #13 +
    'Minimum Wage Makeup' + #9 + ED_MU_MIN_WAGE_MAKEUP + #13 +
    'Meal Credit' + #9 + ED_EWOD_MEAL_CREDIT + #13 +
    '2% Shareholders' + #9 + ED_EWOD_2PERC_SHAREHOLDER + #13 +
    'Group Term Life' + #9 + ED_EWOD_GROUP_TERM_LIFE + #13 +
    'Moving Expense Taxable' + #9 + ED_EWOD_MOVING_EXP_TAXABLE + #13 +
    'Moving Expense Non Taxable' + #9 + ED_EWOD_MOVING_EXP_NON_TAX + #13 +
    'Tips' + #9 + ED_EWOD_TIPS + #13 +
    'Charge Tips' + #9 + ED_EWOD_CHARGE_TIPS + #13 +
    'Special Taxed Memo' + #9 + ED_EWOD_SPC_TAXED_EARNINGS + #13 +
    'Taxable Auto' + #9 + ED_EWOD_TAXABLE_AUTO + #13 +
    'Direct Deposit' + #9 + ED_DIRECT_DEPOSIT + #13 +
    'Standard Deduction' + #9 + ED_DED_STANDARD + #13 +
    'Child Support' + #9 + ED_DED_CHILD_SUPPORT + #13 +
    'Garnishment' + #9 + ED_DED_GARNISH + #13 +
    'Levy' + #9 + ED_DED_LEVY + #13 +
    'Reimbursement' + #9 + ED_DED_REIMBURSEMENT + #13 +
    'Service Bureau COBRA' + #9 + ED_DED_SB_COBRA + #13 +
    'Bankruptcy' + #9 + ED_DED_BANKRUPTCY + #13 +
    'Special SDI' + #9 + ED_DED_SPECIAL_SDI + #13 +
    'Pension Loan' + #9 + ED_DED_PENSION_LOAN + #13 +
    'WA/WY EE Workers Comp' + #9 + ED_DED_WASHINGTON_L_I + #13 +
    'New Mexico EE Workers Comp' + #9 + ED_DED_NM_WORKERS_COMP + #13 +
    'Local Ded/Tax' + #9 + ED_DED_LOCAL_TAX + #13 +
    'Percent of Tax' + #9 + ED_DED_PERCENT_OF_TAX + #13 +
    'Percent of State Taxable Wages' + #9 + ED_DED_PERCENT_OF_TAX_WAGES + #13 +
    'Percent of SUI Taxable Wages' + #9 + ED_MEMO_PERCENT_OF_TAX_WAGES + #13 +
    'After Tax Pension' + #9 + ED_DED_AFTER_TAX_PENSION + #13 +
    'Overtime' + #9 + ED_OEARN_OVERTIME + #13 +
    'Waitstaff Overtime' + #9 + ED_OEARN_WAITSTAFF_OVERTIME + #13 +
    'Weighted 1/2 Time Overtime' + #9 + ED_OEARN_WEIGHTED_HALF_TIME_OT + #13 +
    'Weighted 1 1/2 Time Overtime' + #9 + ED_OEARN_WEIGHTED_3_HALF_TIME_OT + #13 +
    'Average Overtime' + #9 + ED_OEARN_AVG_OVERTIME + #13 +
    'Average Multiple Overtime' + #9 + ED_OEARN_AVG_MULT_OVERTIME + #13 +
    'Waitstaff/Regular Overtime' + #9 + ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME + #13 +
    'Banquet Tips' + #9 + ED_OEARN_BANQUET_TIPS + #13 +
    'Charge Tips' + #9 + ED_OEARN_CHARGE_TIPS + #13 +
    'Supplemental Wage Charge Tips' + #9 + ED_OEARN_SUPP_CHARGE_TIPS + #13 +
    'Piecework' + #9 + ED_OEARN_PIECEWORK + #13 +
    'Regular' + #9 + ED_OEARN_REGULAR + #13 +
    'Salary' + #9 + ED_OEARN_SALARY + #13 +
    'Standard Earnings' + #9 + ED_OEARN_STD_EARNINGS + #13 +
    'Sick' + #9 + ED_OEARN_SICK + #13 +
    'Vacation' + #9 + ED_OERAN_VACATION + #13 +
    'Severance Pay' + #9 + ED_OEARN_SEVERANCE_PAY + #13 +
    'Unworked Earnings' + #9 + ED_OEARN_UNWORKED;

  ED_ComboBoxGroupings =

  'All Other Earnings' + #9 +

  ED_OEARN_OVERTIME + #9 +
    ED_OEARN_WAITSTAFF_OVERTIME + #9 +
    ED_OEARN_WEIGHTED_HALF_TIME_OT + #9 +
    ED_OEARN_WEIGHTED_3_HALF_TIME_OT + #9 +
    ED_OEARN_AVG_OVERTIME + #9 +
    ED_OEARN_AVG_MULT_OVERTIME + #9 +
    ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME + #9 +
    ED_OEARN_BANQUET_TIPS + #9 +
    ED_OEARN_CHARGE_TIPS + #9 +
    ED_OEARN_SUPP_CHARGE_TIPS + #9 +
    ED_OEARN_PIECEWORK + #9 +
    ED_OEARN_REGULAR + #9 +
    ED_OEARN_SALARY + #9 +
    ED_OEARN_STD_EARNINGS + #9 +
    ED_OEARN_SICK + #9 +
    ED_OERAN_VACATION + #9 +
    ED_OEARN_SEVERANCE_PAY + #9 +
    ED_OEARN_UNWORKED + #9 +
    ED_3RD_CA_SUP_RELIGIOUS + #9 +
    ED_3RD_CA_SUP_STOCKHOLDER + #13 +

  'Taxable Memos' + #9 +

  ED_EWOD_2PERC_SHAREHOLDER + #9 +
    ED_EWOD_GROUP_TERM_LIFE + #9 +
    ED_EWOD_MOVING_EXP_TAXABLE + #9 +
    ED_EWOD_MOVING_EXP_NON_TAX + #9 +
    ED_EWOD_TIPS + #9 +
    ED_EWOD_CHARGE_TIPS + #9 +
    ED_EWOD_SPC_TAXED_EARNINGS + #9 +
    ED_EWOD_MEAL_CREDIT + #9 +
    ED_EWOD_TAXABLE_AUTO + #9 +
    ED_EWOD_TAXABLE_ER_HSA_SINGLE + #9 +
    ED_EWOD_TAXABLE_ER_HSA_FAMILY + #13 +

  'Makeup type  E. Codes' + #9 +

  ED_MU_TIPPED_CREDIT_MAKEUP + #9 +
    ED_MU_MIN_WAGE_MAKEUP + #13 +

  'Memo' + #9 +

  ED_MEMO_SIMPLE + #9 +
    ED_MEMO_TIPPED_SALES + #9 +
    ED_MEMO_PENSION + #9 +
    ED_MEMO_PENSION_SUI + #9 +
    ED_MEMO_LINE_9 + #9 +
    ED_MEMO_PERCENT_OF_TAX_WAGES + #9 +
    ED_MEMO_WASHINGTON_L_I + #9 +
    ED_MEMO_NM_WORKERS_COMP + #9 +
    ED_MEMO_WY_WORKERS_COMP + #9 +
    ED_MEMO_COBRA_CREDIT + #9 +
    ED_MEMO_ER_HSA_SINGLE + #9 +
    ED_MEMO_ER_HSA_FAMILY + #9 +
    ED_MEMO_ER_INSURANCE_PREMIUM + #13 +

  'Third Party Sick Pay' + #9 +

  ED_3RD_SHORT + #9 +
    ED_3RD_LONG + #9 +
    ED_3RD_NON_TAX + #13 +

  'Specially Taxed E/D''s' + #9 +

  ED_ST_DEFERRED_COMP + #9 +
    ED_ST_MASS_RETIREMENT + #9 +
    ED_ST_PENSION + #9 +
    ED_ST_401K + #9 +
    ED_ST_403B + #9 +
    ED_ST_457 + #9 +
    ED_ST_501C + #9 +
    ED_ST_SIMPLE + #9 +
    ED_ST_SEP + #9 +
    ED_ST_ROTH_401K + #9 +
    ED_ST_ROTH_403B + #9 +
    ED_ST_ROTH_IRA + #9 +
    ED_ST_CATCH_UP + #9 +
    ED_ST_401K_CATCH_UP + #9 +
    ED_ST_403B_CATCH_UP + #9 +
    ED_ST_457_CATCH_UP + #9 +
    ED_ST_501C_CATCH_UP + #9 +
    ED_ST_ROTH_CATCH_UP + #9 +
    ED_ST_SIMPLE_CATCH_UP + #9 +
    ED_ST_TIAA_CREFF + #9 +
    ED_ST_PRETAX_INS + #9 +
    ED_ST_SB_PREAX_INS + #9 +
    ED_ST_DISCRIMINARY_S125 + #9 +
    ED_ST_STATE_PENSION + #9 +
    ED_ST_SP_TAXED_DEDUCTION + #9 +
//    ED_ST_HEALTHCARE + #9 +
    ED_ST_DEPENDENT_CARE + #9 +
    ED_ST_NON_QUAL_BELOW + #9 +
    ED_ST_NON_QUAL_UP + #9 +
    ED_ST_INCENTIVE_STOCK + #9 +
    ED_ST_EXEMPT_EARNINGS + #9 +
    ED_ST_SP_TAXED_1099_EARNINGS + #9 +
    ED_ST_1099_R + #9 +
    ED_ST_1099_R_SP_TAXED + #9 +
    ED_ST_1099_DIV + #9 +
    ED_ST_INTEREST + #9 +
    ED_ST_HSA_SINGLE + #9 +
    ED_ST_HSA_FAMILY + #9 +
    ED_ST_HSA_CATCH_UP + #9 +
    ED_ST_NON_TAXABLE_EARNING + #9 +
    ED_ST_SP_TAXED_EARNING + #13 +

  'All Other Deductions' + #9 +

  ED_DED_STANDARD + #9 +
    ED_DIRECT_DEPOSIT + #9 +
    ED_DED_CHILD_SUPPORT + #9 +
    ED_DED_GARNISH + #9 +
    ED_DED_LEVY + #9 +
    ED_DED_REIMBURSEMENT + #9 +
    ED_DED_SB_COBRA + #9 +
    ED_DED_BANKRUPTCY + #9 +
    ED_DED_SPECIAL_SDI + #9 +
    ED_DED_PERCENT_OF_TAX + #9 +
    ED_DED_PERCENT_OF_TAX_WAGES + #9 +
    ED_DED_AFTER_TAX_PENSION + #9 +
    ED_DED_PENSION_LOAN + #9 +
    ED_DED_NM_WORKERS_COMP + #9 +
    ED_DED_WASHINGTON_L_I + #9 +
    ED_DED_LOCAL_TAX;

  LocalType_ComboChoices =
    'School' + #9 + LOCAL_TYPE_SCHOOL + #13 +
    'Occupational' + #9 + LOCAL_TYPE_OCCUPATIONAL + #13 +
    'Income Residential' + #9 + LOCAL_TYPE_INC_RESIDENTIAL + #13 +
    'Income Non-Residential' + #9 + LOCAL_TYPE_INC_NON_RESIDENTIAL + #13 +
    'Other' + #9 + LOCAL_TYPE_OTHER;

  TresholdPeriod_ComboChoices =
    'Annual' + #9 + TRESHOLD_ANNUAL + #13 +
    'Pay Period' + #9 + TRESHOLD_PERIOD_PAY_PERIOD + #13 +
    'Month' + #9 + TRESHOLD_PERIOD_MONTH + #13 +
    'Two Months' + #9 + TRESHOLD_PERIOD_TWO_MONTHS + #13 +
    'All Up To Check Date' + #9 + TRESHOLD_PERIOD_ALLUPTOCHECKDATE + #13 +
    'Quarter' + #9 + TRESHOLD_PERIOD_QUARTER + #13 +
    'Same as Frequency' + #9 + TRESHOLD_PERIOD_SAME_AS_FREQ + #13 +
    'None' + #9 + TRESHOLD_NONE;

  ReciprocalType_ComboChoices =
    'Yes' + #9 + RECIPROCATE_YES + #13 +
    'No' + #9 + RECIPROCATE_NO + #13 +
    'Other' + #9 + RECIPROCATE_OTHER + #13 +
    'Only With Reciprocal States' + #9 + RECIPROCATE_WITH_REC_STATES;

  StateTaxChartType_ComboChoices =
    'Standard' + #9 + STATE_TAX_CHART_TYPE_STANDARD + #13 +
    'CT Exemption' + #9 + STATE_TAX_CHART_TYPE_CT_EXEMPTION + #13 +
    'CT Personal Tax Credit' + #9 + STATE_TAX_CHART_TYPE_CT_PERS_TAX_CR + #13 +
    'Flat Amount' + #9 + STATE_TAX_CHART_TYPE_CT_FLAT_AMOUNT;

  TaxDepositStatus_ComboChoices =
    'Pending' + #9 + TAX_DEPOSIT_STATUS_PENDING + #13 +
    'Paid' + #9 + TAX_DEPOSIT_STATUS_PAID + #13 +
    'Paid-Send Notice' + #9 + TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE + #13 +
    'NSF' + #9 + TAX_DEPOSIT_STATUS_NSF + #13 +
    'Voided' + #9 + TAX_DEPOSIT_STATUS_VOIDED + #13 +
    'Send-Notice' + #9 + TAX_DEPOSIT_STATUS_SEND_NOTICE + #13 +
    'Notice-Sent' + #9 + TAX_DEPOSIT_STATUS_NOTICE_SENT + #13 +
    'Deleted' + #9 + TAX_DEPOSIT_STATUS_DELETED + #13 +
    'Manually impounded' + #9 + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + #13 +
    'Paid w/o funds' + #9 + TAX_DEPOSIT_STATUS_PAID_WO_FUNDS + #13 +
    'Impounded' + #9 + TAX_DEPOSIT_STATUS_IMPOUNDED;

  CreditHoldAndStatus_ComboChoices =
    'Pending' + #9 + TAX_DEPOSIT_STATUS_PENDING + #13 +
    'Paid' + #9 + TAX_DEPOSIT_STATUS_PAID + #13 +
    'Paid-Send Notice' + #9 + TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE + #13 +
    'NSF' + #9 + TAX_DEPOSIT_STATUS_NSF + #13 +
    'Notice-Sent' + #9 + TAX_DEPOSIT_STATUS_NOTICE_SENT + #13 +
    'Manually impounded' + #9 + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + #13 +
    'Paid w/o funds' + #9 + TAX_DEPOSIT_STATUS_PAID_WO_FUNDS + #13 +
    'Impounded' + #9 + TAX_DEPOSIT_STATUS_IMPOUNDED;

  EE_Type_ComboChoices =
    'Full Time' + #9 + EE_TYPE_FULL_TIME + #13 +
    'Full Time Temp' + #9 + EE_TYPE_FULL_TIME_TEMP + #13 +
    'Part Time' + #9 + EE_TYPE_PART_TIME + #13 +
    'Part Time Temp' + #9 + EE_TYPE_PART_TIME_TEMP + #13 +
    'Seasonal' + #9 + EE_TYPE_SEASONAL + #13 +
    'Student' + #9 + EE_TYPE_STUDENT + #13 +
    'Half Time' + #9 + EE_TYPE_HALF_TIME + #13 +
    '1099' + #9 + EE_TYPE_1099 + #13 +
    'Other' + #9 + EE_TYPE_OTHER + #13 +
    'All Employees' + #9 + EE_TYPE_ALL;

  ACAStatus_ComboChoices =
    'Full Time' + #9 + ACA_STATUS_FULL + #13 +
    'Part Time' + #9 + ACA_STATUS_PART + #13 +
    'Variable Hour'+ #9 + ACA_STATUS_VARIABLE+ #13 +
    'Seasonal' + #9 + ACA_STATUS_SEASONAL + #13 +
    'Seasonal < 120 days'+ #9 + ACA_STATUS_LESS_SEASONAL + #13 +
    'Full Time Ongoing' + #9 + ACA_STATUS_FULL_ONGOING + #13 +
    'Part Time Ongoing' + #9 + ACA_STATUS_PART_ONGOING + #13 +
    'Seasonal Ongoing' + #9 + ACA_STATUS_SEASONAL_ONGOING + #13 +
    'New Hire' + #9 + ACA_STATUS_NEW_HIRE + #13 +
    'Does Not Apply' + #9 + ACA_STATUS_NA;

  PositionStatus_ComboChoices =
    'N/A' + #9 + POSITION_STATUS_NA + #13 +
    'Full Time' + #9 + POSITION_STATUS_FULL + #13 +
    'Full Time Temp' + #9 + POSITION_STATUS_FULL_TEMP + #13 +
    'Part Time' + #9 + POSITION_STATUS_PART + #13 +
    'Part Time temp' + #9 + POSITION_STATUS_PART_TEMP + #13 +
    'Half Time' + #9 + POSITION_STATUS_HALF_TIME + #13 +
    'Seasonal' + #9 + POSITION_STATUS_SEASONAL + #13 +
    'Student' + #9 + POSITION_STATUS_STUDENT + #13 +
    '1099' + #9 + POSITION_STATUS_1099 + #13 +
    'Other' + #9 + POSITION_STATUS_OTHER+ #13 +
    'Seasonal < 120 days'+ #9 + POSITION_STATUS_LESS_SEASONAL + #13 +
    'Variable'+ #9 + POSITION_STATUS_VARIABLE+ #13 +
    'Per Diem'+ #9 + POSITION_STATUS_PER_DIEM;

  PhoneType_ComboChoices =
    'Primary Phone' + #9 + PHONE_TYPE_PHONE + #13 +
    'Phone 2' + #9 + PHONE_TYPE_PHONE2 + #13 +
    'Pager' + #9 + PHONE_TYPE_PAGER + #13 +
    'Fax' + #9 + PHONE_TYPE_FAX + #13 +
    'Cell Phone' + #9 + PHONE_TYPE_CELL_PHONE + #13 +
    'Emergency Phone' + #9 + PHONE_TYPE_EMERGENCY_PHONE + #13 +
    'After Hours' + #9 + PHONE_TYPE_AFTER_HOURS + #13 +
    'Self Serve contact'+ #9+ PHONE_TYPE_SELF_SERVE + #13 +
    'None' + #9 + PHONE_TYPE_NONE;

  Company_PhoneType_ComboChoices =
    'Primary Phone' + #9 + PHONE_TYPE_PHONE + #13 +
    'Phone 2' + #9 + PHONE_TYPE_PHONE2 + #13 +
    'Pager' + #9 + PHONE_TYPE_PAGER + #13 +
    'Fax' + #9 + PHONE_TYPE_FAX + #13 +
    'Cell Phone' + #9 + PHONE_TYPE_CELL_PHONE + #13 +
    'Emergency Phone' + #9 + PHONE_TYPE_EMERGENCY_PHONE + #13 +
    'After Hours' + #9 + PHONE_TYPE_AFTER_HOURS + #13 +
    'Self Serve contact'+ #9+ PHONE_TYPE_SELF_SERVE + #13 +
    'Certified Payroll'+ #9+ PHONE_TYPE_CERTIFIED_PAYROLL + #13 +
    'Tax Returns'+ #9+ PHONE_TYPE_TAX_RETURN + #13 +
    'None' + #9 + PHONE_TYPE_NONE;

  ReportMethod_ComboChoices =
    'E-mail' + #9 + REPORT_METHOD_EMAIL + #13 +
    'FTP' + #9 + REPORT_METHOD_FTP + #13 +
    'Electronic' + #9 + REPORT_METHOD_ELECTRONIC + #13 +
    'Disk' + #9 + REPORT_METHOD_DISK + #13 +
    'Paper' + #9 + REPORT_METHOD_PAPER + #13 +
    'None' + #9 + REPORT_METHOD_NONE;

  PageBreakDuplex_ComboChoices =
    'Page Break Only' + #9 + PAGE_BREAK_ONLY + #13 +
    'Duplex Only' + #9 + DUPLEX_ONLY + #13 +
    'Page Break + Duplex' + #9 + PAGE_BREAK_DUPLEX + #13 +
    'None' + #9 + NO_PAGE_AND_NO_DUPLEX;

  PensionType_ComboChoices =
    '401K' + #9 + PENSION_TYPE_401K + #13 +
    '403B' + #9 + PENSION_TYPE_403B + #13 +
    '457' + #9 + PENSION_TYPE_457 + #13 +
    'Simple' + #9 + PENSION_TYPE_SIMPLE + #13 +
    'SEP' + #9 + PENSION_TYPE_SEP + #13 +
    'Pension Plan' + #9 + PENSION_TYPE_PENSION_PLAN + #13 +
    'Deferred Comp.' + #9 + PENSION_TYPE_DEFERRED_COMP + #13 +
    'State Pension' + #9 + PENSION_TYPE_STATE_PENSION + #13 +
    'MA Retirement' + #9 + PENSION_TYPE_MA_RETIREMENT;

  AgencyType_ComboChoices =
    'Pension Admin' + #9 + AGENCY_TYPE_PENSION_ADMIN + #13 +
    'Insurance Admin' + #9 + AGENCY_TYPE_INSURANCE_ADMIN + #13 +
    'Child Support' + #9 + AGENCY_TYPE_CHILD_SUPPORT + #13 +
    'Garnishment' + #9 + AGENCY_TYPE_GARNISHMENT + #13 +
    'Levy' + #9 + AGENCY_TYPE_LEVY + #13 +
    'Union Dues' + #9 + AGENCY_TYPE_UNION_DUES + #13 +
    'Other' + #9 + AGENCY_TYPE_OTHER;

  CommonReportStatus_ComboChoices =
    'Pending' + #9 + COMMON_REPORT_STATUS__PENDING + #13 +
    'In Process' + #9 + COMMON_REPORT_STATUS__IN_PROCESS + #13 +
    'Hold' + #9 + COMMON_REPORT_STATUS__HOLD + #13 +
    'Completed' + #9 + COMMON_REPORT_STATUS__COMPLETED + #13 +
    'Deleted' + #9 + COMMON_REPORT_STATUS__DELETED;

  Sy_StateReciprocal_ComboChoices =
    'Do not report' + #9 + SYSTEM_STATE_RECIPROCAL_TYPE_DO_NOT_REPORT + #13 +
    'Wages combined if no tax' + #9 + SYSTEM_STATE_RECIPROCAL_TYPE_REPORT_ALL_REQUIRED_NO_TAX + #13 +
    'Wages broken out if no tax' + #9 + SYSTEM_STATE_RECIPROCAL_TYPE_REPORT_NO + #13 +
    'Wages combined regardless' + #9 + SYSTEM_STATE_RECIPROCAL_TYPE_REPORT_ALL_COMBINED + #13 +
    'Wages broken out regardless' + #9 + SYSTEM_STATE_RECIPROCAL_TYPE_REPORT_BY_STATE;

  StateReciprocal_ComboChoices =
    'Take None' + #9 + STATE_RECIPROCAL_TYPE_NONE + #13 +
    'Take Difference Between States' + #9 + STATE_RECIPROCAL_TYPE_DIFFERENCE + #13 +
    'Take a Flat Amount' + #9 + STATE_RECIPROCAL_TYPE_FLAT + #13 +
    'Take a Percentage' + #9 + STATE_RECIPROCAL_TYPE_PERCENTAGE + #13 +
    'Take the Full Amount for Each State' + #9 + STATE_RECIPROCAL_TYPE_FULL;

  CheckType945_ComboChoices =
    'None' + #9 + CHECK_TYPE_945_NONE + #13 +
    '945' + #9 + CHECK_TYPE_945_945 + #13 +
    '943' + #9 + CHECK_TYPE_945_943 + #13 +
    'Guam' + #9 + CHECK_TYPE_945_GUAM + #13 +
    'Virgin Islands' + #9 + CHECK_TYPE_945_VIRGIN_ISLANDS + #13 +
    'Puerto Rico' + #9 + CHECK_TYPE_945_PUERTO_RICO;

  ScheduledEDTarget_ComboChoices =
    'None' + #9 + SCHED_ED_TARGET_NONE + #13 +
    'Reset Balance' + #9 + SCHED_ED_TARGET_RESET + #13 +
    'Reset Balance and Issue Agency Check' + #9 + SCHED_ED_TARGET_RESET_WITH_CHECK + #13 +
    'Do Not Reset Balance but Leave Line' + #9 + SCHED_ED_TARGET_LEAVE + #13 +
    'Do Not Reset Balance and Remove Line' + #9 + SCHED_ED_TARGET_REMOVE;

  PayFrequencyType_ComboChoices =
    'Daily' + #9 + FREQUENCY_TYPE_DAILY + #13 +
    'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Bi-Weekly' + #9 + FREQUENCY_TYPE_BIWEEKLY + #13 +
    'Semi-Monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY;

  TaxFrequencyType_ComboChoices =
    'Daily' + #9 + FREQUENCY_TYPE_DAILY + #13 +
    'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13 +
    'Bi-Weekly' + #9 + FREQUENCY_TYPE_BIWEEKLY + #13 +
    'Semi-Monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13 +
    'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13 +
    'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13 +
    'Semi-Annual' + #9 + FREQUENCY_TYPE_SEMI_ANNUAL + #13 +
    'Annual' + #9 + FREQUENCY_TYPE_ANNUAL;

  W2Type_ComboChoices =
    'None' + #9 + W2_TYPE_NONE + #13 +
    'W-2' + #9 + W2_TYPE_FEDERAL + #13 +
    '1099' + #9 + W2_TYPE_FOREIGN + #13 +
    'W-2 & 1099' + #9 + W2_TYPE_W2_1099 + #13 +
    'Puerto Rico' + #9 + W2_TYPE_PUERTO_RICO;

  EIC_ComboChoices =
    'None' + #9 + EIC_NONE + #13 +
    'Single' + #9 + EIC_SINGLE + #13 +
    'Married' + #9 + EIC_MARRIED + #13 +
    'Married Both Spouses Filing for EIC' + #9 + EIC_BOTH_SPOUSES;

  Ethnicity_ComboChoices =
    'Asian' + #9 + ETHNIC_ASIAN + #13 +
    'American Indian or Alaska Native' + #9 + ETHNIC_INDIAN + #13 +
    'Black or African American' + #9 + ETHNIC_AFRICAN + #13 +
    'White' + #9 + ETHNIC_CAUCASIAN + #13 +
    'Hispanic or Latino' + #9 + ETHNIC_HISPANIC + #13 +
    'Native Hawaiian or other Pacific Islander' + #9 + ETHNIC_PACIFIC + #13 +
    'Two or More Races' + #9 + ETHNIC_MANY + #13 +
    'Other' + #9 + ETHNIC_OTHER + #13 +
    'Not Applicable' + #9 + ETHNIC_NOT_APPLICATIVE;

  EEOCodes_ComboChoices =
    'None' + #9 + EEO_NONE + #13 +
    'Officials & Managers' + #9 + EEO_MANAGER + #13 +
    'Professional' + #9 + EEO_PROFESSIONAL + #13 +
    'Technician' + #9 + EEO_TECHNICIAN + #13 +
    'Salesworker' + #9 + EEO_SALESWORKER + #13 +
    'Office Clerical' + #9 + EEO_CLERICAL + #13 +
    'Craftworkers-Skilled' + #9 + EEO_CRAFT + #13 +
    'Operatives-SemiSkilled' + #9 + EEO_OPERATIVES + #13 +
    'Laborers-Unskilled' + #9 + EEO_LABORER + #13 +
    'Service Workers' + #9 + EEO_SERVICE;

  EE_TerminationCode_ComboChoices =
    'Active' + #9 + EE_TERM_ACTIVE + #13 +
    'Involuntary Layoff' + #9 + EE_TERM_INV_LAYOFF + #13 +
    'Involuntary Termination Due to Misconduct' + #9 + EE_TERM_INV_MISCONDUCT + #13 +
    'Termination Due to Layoff' + #9 + EE_TERM_LAYOFF + #13 +
    'Release Without Prejudice' + #9 + EE_TERM_RELEASE + #13 +
    'Voluntary Resignation' + #9 + EE_TERM_RESIGN + #13 +
    'Termination Due to Retirement' + #9 + EE_TERM_RETIREMENT + #13 +
    'Termination Due to Transfer' + #9 + EE_TERM_TRANSFER + #13 +
    'Military Leave' + #9 + EE_TERM_MILITARY_LEAVE + #13 +
    'Leave of Absence' + #9 + EE_TERM_LEAVE + #13 +
    'Seasonal' + #9 + EE_TERM_SEASONAL + #13 +
    'Termination Due to Death' + #9 + EE_TERM_DEATH + #13 +
    'Suspended' + #9 + EE_TERM_SUSPENDED + #13 + 
    'Terminated' + #9 + EE_TERM_TERMINATED + #13 +
    'FMLA' + #9 + EE_TERM_FMLA + #13 +
    'Jury Duty' + #9 + EE_TERM_JURYDUTY + #13 +
    'Short Term Disability' + #9 + EE_TERM_SHORTDISABILITY + #13 +
    'Long Term Disability' + #9 + EE_TERM_LONGDISABILITY + #13 +
    'Other/See Notes' + #9 + EE_TERM_SEENOTES;



  Relationship_ComboChoices =
    'Partner' + #9 + REL_TYPE_PARTNER + #13 +
    'Spouse' + #9 + REL_TYPE_SPOUSE + #13 +
    'Child' + #9 + REL_TYPE_CHILD + #13 +
    'Step-Child' + #9 + REL_TYPE_STEPCHILD + #13 +
    'Legal Guardian' + #9 + REL_TYPE_GUARDIAN + #13 +
    'Other' + #9 + REL_TYPE_OTHER;

  Relationship_Beneficiary_ComboChoices =
    'Partner' + #9 + REL_TYPE_PARTNER + #13 +
    'Spouse' + #9 + REL_TYPE_SPOUSE + #13 +
    'Child' + #9 + REL_TYPE_CHILD + #13 +
    'Step-Child' + #9 + REL_TYPE_STEPCHILD + #13 +
    'Legal Guardian' + #9 + REL_TYPE_GUARDIAN + #13 +
    'Other' + #9 + REL_TYPE_OTHER + #13 +
    'Parent' + #9 + REL_TYPE_PARENT + #13 +
    'Sibling' + #9 + REL_TYPE_SIBLING + #13 +
    'Grandparent' + #9 + REL_TYPE_GRANDPARENT + #13 +
    'Other Relative' + #9 + REL_TYPE_OTHERRELATIVE + #13 +
    'Friend' + #9 + REL_TYPE_FRIEND + #13 +
    'Legal  Ward' + #9 + REL_TYPE_LEGALWARD + #13 +
    'Trustee' + #9 + REL_TYPE_TRUSTEE;

  NewHireReport_ComboChoices =
    'Pending' + #9 + NEW_HIRE_REPORT_PENDING + #13 +
    'Completed' + #9 + NEW_HIRE_REPORT_COMPLETED + #13 +
    'Completed by Predecessor' + #9 + NEW_HIRE_REPORT_COMP_BY_PREDECESSOR;

  OS_ComboChoices =
    'Windows 95' + #9 + OS_WINDOWS_95 + #13 +
    'Windows 98' + #9 + OS_WINDOWS_98 + #13 +
    'Windows 3.1' + #9 + OS_WINDOWS_31 + #13 +
    'Windows NT' + #9 + OS_WINDOWS_NT + #13 +
    'Windows 2000' + #9 + OS_WINDOWS_2000 + #13 +
    'Windows XP Home' + #9 + OS_WINDOWS_XP_HOME + #13 +
    'Windows XP Pro' + #9 + OS_WINDOWS_XP_PRO + #13 +
    'Windows Server 2003' + #9 + OS_WINDOWS_SERVER_2003 + #13 +
    'Windows Vista' + #9 + OS_WINDOWS_VISTA + #13 +
    'MS-DOS' + #9 + OS_DOS + #13 +
    'OS/2 Warp' + #9 + OS_OS_2_WARP + #13 +
    'Mac OS' + #9 + OS_MAC + #13 +
    'Unix' + #9 + OS_UNIX + #13 +
    'Other' + #9 + OS_OTHER + #13 +
    'N/A' + #9 + OS_NA;

  Network_ComboChoices =
    'Windows NT 3.x' + #9 + NETWORK_WINDOWS_NT_3 + #13 +
    'Windows NT 4.x' + #9 + NETWORK_WINDOWS_NT_4 + #13 +
    'Windows NT 5.x' + #9 + NETWORK_WINDOWS_NT_5 + #13 +
    'Novell 3.x' + #9 + NETWORK_NOVELL_3 + #13 +
    'Novell 4.x' + #9 + NETWORK_NOVELL_4 + #13 +
    'Novell 5.x' + #9 + NETWORK_NOVELL_5 + #13 +
    'Lantastic' + #9 + NETWORK_LANTASTIC + #13 +
    'AS/400' + #9 + NETWORK_AS_400 + #13 +
    'Unix' + #9 + NETWORK_UNIX + #13 +
    'Other' + #9 + NETWORK_OTHER + #13 +
    'None' + #9 + NETWORK_NONE;

  ModemSpeed_ComboChoices =
    '28.8K' + #9 + MODEM_SPEED_28_8 + #13 +
    '33.6K' + #9 + MODEM_SPEED_33_6 + #13 +
    '56K' + #9 + MODEM_SPEED_56 + #13 +
    '64K' + #9 + MODEM_SPEED_64 + #13 +
    '128K' + #9 + MODEM_SPEED_128 + #13 +
    'Other' + #9 + MODEM_SPEED_OTHER + #13 +
    'N/A' + #9 + MODEM_SPEED_NA;

  SBServicesType_ComboChoices =
    'Trust' + #9 + SB_SERVICES_TYPE_TRUST + #13 +
    'Tax' + #9 + SB_SERVICES_TYPE_TAX + #13 +
    'Direct Deposit' + #9 + SB_SERVICES_TYPE_DIRECT_DEPOSIT + #13 +
    'Section 125' + #9 + SB_SERVICES_TYPE_S_125 + #13 +
    'COBRA' + #9 + SB_SERVICES_TYPE_COBRA + #13 +
    'Workers Comp' + #9 + SB_SERVICES_TYPE_WORKERS_COMP + #13 +
    'GL' + #9 + SB_SERVICES_TYPE_GL + #13 +
    'Delivery Services' + #9 + SB_SERVICES_TYPE_DELIVERY_SERVICES + #13 +
    'W2' + #9 + SB_SERVICES_TYPE_W2 + #13 +
    '1099' + #9 + SB_SERVICES_TYPE_1099 + #13 +
    'HR' + #9 + SB_SERVICES_TYPE_HR + #13 +
    'Time & Attendance' + #9 + SB_SERVICES_TYPE_TIME_ATTENDANCE + #13 +
    'Other' + #9 + SB_SERVICES_TYPE_OTHER + #13 +
    'Standard' + #9 + SB_SERVICES_TYPE_STANDARD;

  SBServicesBasedOn_ComboChoices =
    'All Checks' + #9 + SB_SERVICES_BASED_ON_ALL_CHECKS + #13 +
    'All Checks Except Misc Checks' + #9 + SB_SERVICES_BASED_ON_PAYROLL_CHECKS_ONLY + #13 +
    'Live Payroll Checks' + #9 + SB_SERVICES_BASED_ON_REGULAR_CHECKS_ONLY + #13 +
    'Per Active Employee' + #9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE + #13 +
    'Per Employee' + #9 + SB_SERVICES_BASED_ON_PER_ALL_EMPLOYEES + #13 +
    'Self Serve' + #9 + SB_SERVICES_BASED_ON_PER_SELFSERVE_EMPLOYEES + #13 +
    'Per Employee Using S/B Pretax'#9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX + #13 +
    'Per Employee Using S/B Pretax Codes' + #9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX_CODES + #13 +
    'Per Employee Using Time Off Accrual'#9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA + #13 +
    'Per Employee Using Time Off Accrual Codes'#9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA_CODES + #13 +
    'Billing Checks Only' + #9 + SB_SERVICES_BASED_ON_BILLING_CHECKS_ONLY + #13 +
    'Agency Checks Only' + #9 + SB_SERVICES_BASED_ON_AGENCY_CHECKS_ONLY + #13 +
    'Tax Checks Only' + #9 + SB_SERVICES_BASED_ON_TAX_CHECKS_ONLY + #13 +
    'Manual Checks Only' + #9 + SB_SERVICES_BASED_ON_MANUAL_CHECKS + #13 +
    'Printed Payroll Checks Only' + #9 + SB_SERVICES_BASED_ON_PRINTED_CHECKS_ONLY + #13 +
    'Cobra Checks only' + #9 + SB_SERVICES_BASED_ON_COBRA_CHECKS_ONLY + #13 +
    'Per Void Checks' + #9 + SB_SERVICES_BASED_ON_PER_NEW_CHECK + #13 +    
    'All But Billing Checks' + #9 + SB_SERVICES_BASED_ON_ALL_BUT_BILLING + #13 +
    'All But Agency Checks' + #9 + SB_SERVICES_BASED_ON_ALL_BUT_AGENCY + #13 +
    'All But Tax Checks' + #9 + SB_SERVICES_BASED_ON_ALL_BUT_TAX + #13 +
    'Per Report' + #9 + SB_SERVICES_BASED_ON_PER_REPORT + #13 +
    'Per Specific Report' + #9 + SB_SERVICES_BASED_ON_PER_SPECIFIC_REPORT + #13 +
    'Per Report Number' + #9 + SB_SERVICES_BASED_ON_PER_REPORT_NUMBER + #13 +
    'Per Employee Paid' + #9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID + #13 +
    'Trust Impound' + #9 + SB_SERVICES_BASED_ON_TRUST_IMPOUND + #13 +
    'Tax Impound' + #9 + SB_SERVICES_BASED_ON_TAX_IMPOUND + #13 +
    'ER Tax Impound' + #9 + SB_SERVICES_BASED_ON_TAX_ER_IMPOUND + #13 +
    'Workers Comp Impound' + #9 + SB_SERVICES_BASED_ON_WORKERS_COMP + #13 +
    '% of All Earnings' + #9 + SB_SERVICES_BASED_ON_PERC_EARNINGS + #13 +
    'Per State' + #9 + SB_SERVICES_BASED_ON_PER_STATE + #13 +
    'Per Locality' + #9 + SB_SERVICES_BASED_ON_PER_LOCAL + #13 +
    'Per Local Taxing Agency' + #9 + SB_SERVICES_BASED_ON_PER_TAX_AGENCY + #13 +
    'Direct Deposit' + #9 + SB_SERVICES_BASED_ON_DIRECT_DEPOSIT + #13 +
    'Employee New Hire Report' + #9 + SB_SERVICES_BASED_ON_EMPLOYEE_NEW_HIRE_REPORT + #13 +
    'Employee Changes' + #9 + SB_SERVICES_BASED_ON_EMPLOYEE_CHANGES + #13 +
    'New Hired Employees' + #9 + SB_SERVICES_BASED_ON_NEW_HIRED_EMPLOYEES + #13 +
    'Termed Employees' + #9 + SB_SERVICES_BASED_ON_TERMED_EMPLOYEES + #13 +
    'All Termed Employees' + #9 + SB_SERVICES_BASED_ON_ALL_TERMED_EMPLOYEES + #13 +
    'Per Hourly Employee' + #9 + SB_SERVICES_BASED_ON_PER_HOURLY_EMPLOYEE + #13 +
    '1099''s' + #9 + SB_SERVICES_BASED_ON_1099 + #13 +
    'W2''s' + #9 + SB_SERVICES_BASED_ON_W2 + #13 +
    'E/D Code' + #9 + SB_SERVICES_BASED_ON_ED_CODE + #13 +
    'E/D Code Count' + #9 + SB_SERVICES_BASED_ON_ED_CODE_COUNT + #13 +
    'E/D Group' + #9 + SB_SERVICES_BASED_ON_ED_GROUP + #13 +
    'Per VMR Voucher' + #9 + SB_SERVICES_BASED_ON_PER_VMR_VOUCHER + #13 +
 //   'Per VMR Report' + #9 + SB_SERVICES_BASED_ON_PER_VMR_REPORT + #13 +
    'Per Web User' + #9 + SB_SERVICES_BASED_ON_PER_WEB_USER + #13 +
    'Per New Prenote' + #9 + SB_SERVICES_BASED_ON_PER_NEW_PRENOTE + #13 +
    'Flat Amount' + #9 + SB_SERVICES_BASED_ON_FLAT_AMOUNT + #13 +
    'Per Employee Per Month' + #9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_PER_MONTH + #13 +
    'Per Employee Paid Per Month' + #9 + SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID_PER_MONTH + #13 +
    '1095B''s' + #9 + SB_SERVICES_BASED_ON_1095Bs + #13 +
    '1095C''s' + #9 + SB_SERVICES_BASED_ON_1095Cs + #13 +
    'ACA Active' + #9 + SB_SERVICES_BASED_ON_ACA_ACTIVE + #13 +
    'ACA Year To Date' + #9 + SB_SERVICES_BASED_ON_ACA_YEAR_TO_DATE + #13 +
    'Analytics' + #9 + SB_SERVICES_BASED_ON_ANALYTICS + #13 +
    'Per Override Analytics Dashboard' + #9 + SB_SERVICES_BASED_ON_PER_OVERRIDE_DASHBOARD + #13 +
    'Per Override Analytics User' + #9 + SB_SERVICES_BASED_ON_PER_OVERRIDE_USER + #13 +
    'Per Override Analytics Lookback Year' + #9 + SB_SERVICES_BASED_ON_PER_OVERRIDE_LOOKBACKYEAR + #13 +
    'Per Active EE Enabled for Analytics' + #9 + SB_SERVICES_BASED_ON_PER_ACTIVE_EE_ENABLED_FOR_ANALYTICS + #13 +
    'Per Evolution Payroll User' + #9 + SB_SERVICES_BASED_ON_PER_EVOLUTION_PAYROLL_USER;



  COBillingStatus_ComboChoices =
    'Ignore' + #9 + CO_BILLING_STATUS_IGNORE + #13 +
    'Check' + #9 + CO_BILLING_STATUS_CHECK + #13 +
    'Ready for ACH' + #9 + CO_BILLING_STATUS_READY_FOR_ACH + #13 +
    'ACH Sent' + #9 + CO_BILLING_STATUS_ACH_SENT + #13 +
    'Internal Check' + #9 + CO_BILLING_STATUS_INTERNAL_CHECK + #13 +
    'External Check' + #9 + CO_BILLING_STATUS_EXTERNAL_CHECK + #13 +
    'Wire Transfer' + #9 + CO_BILLING_STATUS_WIRE_TRANSFER + #13 +
    'Cash' + #9 + CO_BILLING_STATUS_CASH + #13 +
    'Other' + #9 + CO_BILLING_STATUS_OTHER;

  ModemConnType_ComboChoices =
    'Regular Direct' + #9 + MODEM_CONN_TYPE_REGULAR_DIRECT + #13 +
    'Regular Switch' + #9 + MODEM_CONN_TYPE_REGULAR_SWITCH + #13 +
    'ISDN' + #9 + MODEM_CONN_TYPE_ISDN + #13 +
    'Leased' + #9 + MODEM_CONN_TYPE_LEASED + #13 +
    'Other' + #9 + MODEM_CONN_TYPE_OTHER;

  SchedFrequencies_ComboChoices =
    'Weekly' + #9 + CO_FREQ_WEEKLY + #13 +
    'Bi-Weekly' + #9 + CO_FREQ_BWEEKLY + #13 +
    'Semi-Monthly' + #9 + CO_FREQ_SMONTHLY + #13 +
    'Monthly' + #9 + CO_FREQ_MONTHLY + #13 +
    'Quarterly' + #9 + CO_FREQ_QUARTERLY;

  PayFrequencies_ComboChoices =
    'Weekly' + #9 + CO_FREQ_WEEKLY + #13 +
    'Bi-Weekly' + #9 + CO_FREQ_BWEEKLY + #13 +
    'Semi-Monthly' + #9 + CO_FREQ_SMONTHLY + #13 +
    'Monthly' + #9 + CO_FREQ_MONTHLY + #13 +
    'Weekly, Bi-Weekly' + #9 + CO_FREQ_WEEKLY_BWEEKLY + #13 +
    'Weekly, Semi-Monthly' + #9 + CO_FREQ_WEEKLY_SMONTHLY + #13 +
    'Weekly, Monthly' + #9 + CO_FREQ_WEEKLY_MONTHLY + #13 +
    'Bi-Weekly, Semi-Monthly' + #9 + CO_FREQ_BWEEKLY_SMONTHLY + #13 +
    'Bi-Weekly, Monthly' + #9 + CO_FREQ_BWEEKLY_MONTHLY + #13 +
    'Semi-Monthly, Monthly' + #9 + CO_FREQ_SMONTHLY_MONTHLY + #13 +
    'Weekly, Bi-Weekly, Semi-Monthly' + #9 + CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY + #13 +
    'Weekly, Bi-Weekly, Monthly' + #9 + CO_FREQ_WEEKLY_BWEEKLY_MONTHLY + #13 +
    'Weekly, Semi-Monthly, Monthly' + #9 + CO_FREQ_WEEKLY_SMONTHLY_MONTHLY + #13 +
    'Bi-Weekly, Semi-Monthly, Monthly' + #9 + CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY + #13 +
    'Weekly, Bi-Weekly, Semi-Monthly, Monthly' + #9 + CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY + #13 +
    'Daily' + #9 + CO_FREQ_DAILY + #13 +
    'Quarterly' + #9 + CO_FREQ_QUARTERLY;

{*****************************************************************}
// Begin payroll-related changes

  PayrollStatus_ComboChoices =
    'Pending' + #9 + PAYROLL_STATUS_PENDING + #13 +
    'Processed' + #9 + PAYROLL_STATUS_PROCESSED + #13 +
    'Completed' + #9 + PAYROLL_STATUS_COMPLETED + #13 +
    'Voided' + #9 + PAYROLL_STATUS_VOIDED + #13 +
    'Deleted' + #9 + PAYROLL_STATUS_DELETED + #13 +
    'Hold' + #9 + PAYROLL_STATUS_HOLD + #13 +
    'SB Review' + #9 + PAYROLL_STATUS_SB_REVIEW + #13 +
    'Backdated' + #9 + PAYROLL_STATUS_BACKDATED + #13 +
    'Required Mgr. Approval' + #9 + PAYROLL_STATUS_REQUIRED_MGR_APPROVAL + #13 +
    'Processing' + #9 + PAYROLL_STATUS_PROCESSING + #13 +
    'Pre-Processing' + #9 + PAYROLL_STATUS_PREPROCESSING;

  PayrollCheckDateStatus_ComboChoices =
    'Normal' + #9 + DATE_STATUS_NORMAL + #13 +
    'Ignore' + #9 + DATE_STATUS_IGNORE;


  PayrollCheckStatus_ComboChoices =
    'Outstanding' + #9 + CHECK_STATUS_OUTSTANDING + #13 +
    'OBC Sent' + #9 + CHECK_STATUS_OBC_SENT + #13 +
    'Cleared' + #9 + CHECK_STATUS_CLEARED + #13 +
    'NSF' + #9 + CHECK_STATUS_NSF + #13 +
    'NSF Paid' + #9 + CHECK_STATUS_NSF_PAID + #13 +
    'Void' + #9 + CHECK_STATUS_VOID + #13 +
    'Deleted' + #9 + CHECK_STATUS_DELETED + #13 +
    'Returned' + #9 + CHECK_STATUS_RETURNED + #13 +
    'Stop Payment' + #9 + CHECK_STATUS_STOP_PAYMENT + #13 +
    'Dead' + #9 + CHECK_STATUS_DEAD;

  MiscCheckStatus_ComboChoices =
    'Outstanding' + #9 + MISC_CHECK_STATUS_OUTSTANDING + #13 +
    'Pending For ACH' + #9 + MISC_CHECK_STATUS_PENDING_FOR_ACH + #13 +
    'Cleared' + #9 + MISC_CHECK_STATUS_CLEARED + #13 +
    'NSF' + #9 + MISC_CHECK_STATUS_NSF + #13 +
    'NSF Paid' + #9 + MISC_CHECK_STATUS_NSF_PAID + #13 +
    'Void' + #9 + MISC_CHECK_STATUS_VOID + #13 +
    'Deleted' + #9 + MISC_CHECK_STATUS_DELETED + #13 +
    'Returned' + #9 + MISC_CHECK_STATUS_RETURNED + #13 +
    'Stop Payment' + #9 + MISC_CHECK_STATUS_STOP_PAYMENT + #13 +
    'Dead' + #9 + MISC_CHECK_STATUS_DEAD;

   MiscCheckType_ComboChoices =
    'Agency' + #9 + MISC_CHECK_TYPE_AGENCY + #13 +
    'Tax' + #9 + MISC_CHECK_TYPE_TAX + #13 +
    'Billing' + #9 + MISC_CHECK_TYPE_BILLING + #13 +
    'Agency Void' + #9 + MISC_CHECK_TYPE_AGENCY_VOID + #13 +
    'Tax Void' + #9 + MISC_CHECK_TYPE_TAX_VOID + #13 +
    'Billing Void' + #9 + MISC_CHECK_TYPE_BILLING_VOID;

  EftpsTaxType_ComboChoices =
    'Regular' + #9 + EFTPS_TAX_TYPE_REGULAR + #13 +
    'Tax Impound' + #9 + EFTPS_TAX_TYPE_TAX_IMPOUND + #13 +
    'Trust Impound' + #9 + EFTPS_TAX_TYPE_TRUST_IMPOUND;

  PayrollType_ComboChoices =
    'Regular' + #9 + PAYROLL_TYPE_REGULAR + #13 +
    'Supplemental' + #9 + PAYROLL_TYPE_SUPPLEMENTAL + #13 +
    'Reversal' + #9 + PAYROLL_TYPE_REVERSAL + #13 +
    'Client Correction' + #9 + PAYROLL_TYPE_CL_CORRECTION + #13 +
    'SB Correction' + #9 + PAYROLL_TYPE_SB_CORRECTION + #13 +
    'Setup Run' + #9 + PAYROLL_TYPE_SETUP + #13 +
    'Misc Check Adjustment' + #9 + PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT + #13 +
    'Tax Adjustment' + #9 + PAYROLL_TYPE_TAX_ADJUSTMENT + #13 +
    'Wage Adj with Qtr' + #9 + PAYROLL_TYPE_WAGE_ADJUSTMENT_QTR + #13 +
    'Wage Adj no Qtr' + #9 + PAYROLL_TYPE_WAGE_ADJUSTMENT_NO_QTR + #13 +
    'Qtr End Tax Adjustment' + #9 + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + #13 +
    'Tax Deposit' + #9 + PAYROLL_TYPE_TAX_DEPOSIT + #13 +
    'Import' + #9 + PAYROLL_TYPE_IMPORT + #13 +
    'Backdated Setup' + #9 + PAYROLL_TYPE_BACKDATED;

  CheckType2_ComboChoices =
    'Regular' + #9 + CHECK_TYPE2_REGULAR + #13 +
    'Void' + #9 + CHECK_TYPE2_VOID + #13 +
    '3rd Party' + #9 + CHECK_TYPE2_3RD_PARTY + #13 +
    'Manual' + #9 + CHECK_TYPE2_MANUAL + #13 +
    'Cobra Credit' + #9 + CHECK_TYPE2_COBRA_CREDIT + #13 +
    'Setup YTD' + #9 + CHECK_TYPE2_YTD + #13 +
    'Setup QTD' + #9 + CHECK_TYPE2_QTD;

  CheckType3_ComboChoices =
    'Regular' + #9 + CHECK_TYPE2_REGULAR + #13 +
    '3rd Party' + #9 + CHECK_TYPE2_3RD_PARTY + #13 +
    'Manual' + #9 + CHECK_TYPE2_MANUAL + #13 +
    'Setup YTD' + #9 + CHECK_TYPE2_YTD + #13 +
    'Setup QTD' + #9 + CHECK_TYPE2_QTD;

  OverrideValueType_ComboChoices =
    'None' + #9 + OVERRIDE_VALUE_TYPE_NONE + #13 +
    'Regular Amount' + #9 + OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT + #13 +
    'Regular Percent' + #9 + OVERRIDE_VALUE_TYPE_REGULAR_PERCENT + #13 +
    'Additional Amount' + #9 + OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT + #13 +
    'Additional Percent' + #9 + OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;

  PayrollInvoicePrinted_ComboChoices =
    'Pending' + #9 + INVOICE_PRINT_STATUS_Pending + #13 +
    'Billing Calculated' + #9 + INVOICE_PRINT_STATUS_Billing_Calculated + #13 +
    'Invoice Printed' + #9 + INVOICE_PRINT_STATUS_Invoice_Printed + #13 +
    'Calculated do not print' + #9 + INVOICE_PRINT_STATUS_calculated_do_not_print + #13 +
    'Excluded' + #9 + INVOICE_PRINT_STATUS_Excluded;


  TimeOffShow_ComboChoices =
    'Accrued, used, unused' + #9 + TIME_OFF_SHOW_ACCRUED_USED_UNUSED + #13 +
    'Accrued, unused' + #9 + TIME_OFF_SHOW_ACCRUED_UNUSED + #13 +
    'Accrued, used' + #9 + TIME_OFF_SHOW_ACCRUED_USED + #13 +
    'Accrued' + #9 + TIME_OFF_SHOW_ACCRUED + #13 +
    'Accrued This Pay' + #9 + TIME_OFF_SHOW_ACCRUED_THIS_PAY + #13 +
    'Accrued This Pay, Used This Pay, Unused' + #9 + TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY_UNUSED + #13 +
    'Accrued This Pay, Unused' + #9 + TIME_OFF_SHOW_ACCRUED_THIS_PAY_UNUSED + #13 +
    'Accrued This Pay, Used This Pay' + #9 + TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY + #13 +
    'Unused' + #9 + TIME_OFF_SHOW_UNUSED + #13 +
    'Used' + #9 + TIME_OFF_SHOW_USED + #13 +
    'Balance Prior, Accrued, Used, Balance After' + #9 + TIME_OFF_SHOW_BALPRIOR_ACCRUED_USED_BALAFTER + #13 +
    'None' + #9 + TIME_OFF_SHOW_NONE;

  BillingPayment_ComboChoices =
    'Check Client''s Account' + #9 + BILLING_PAYMENT_METHOD_CLIENT + #13 +
//'Check SB Bank Account'    +#9+ BILLING_PAYMENT_METHOD_SB          +#13+
  'Direct Debit' + #9 + BILLING_PAYMENT_METHOD_DEBIT + #13 +
    'Invoice Only' + #9 + BILLING_PAYMENT_METHOD_INVOICE + #13 +
    '            ' + #9 + BILLING_PAYMENT_METHOD_NA;

  Cash_Management_ComboChoices =
    'None' + #9 + PAYMENT_TYPE_NONE + #13 +
    'ACH' + #9 + PAYMENT_TYPE_ACH + #13 +
    'Internal Check' + #9 + PAYMENT_TYPE_INTERNAL_CHECK + #13 +
    'External Check' + #9 + PAYMENT_TYPE_EXTERNAL_CHECK + #13 +
    'Other' + #9 + PAYMENT_TYPE_OTHER + #13 +
    'Wire Transfer' + #9 + PAYMENT_TYPE_WIRE_TRANSFER + #13 +
    'Cash' + #9 + PAYMENT_TYPE_CASH;

  AlwaysPay_ComboChoices =
    'No' + #9 + ALWAYS_PAY_NO + #13 +
    'All Payrolls' + #9 + ALWAYS_PAY_ALL_PAYROLLS + #13 +
    'Current Payroll' + #9 + ALWAYS_PAY_CURRENT_PAYROLL;

  AutopayCompany_ComboChoices =
    'Remote' + #9 + AutopayCompany_Remote + #13 +
    'Fax' + #9 + AutopayCompany_FAX + #13 +
    'Auto pay' + #9 + AutopayCompany_AutoPay + #13 +
    'Call in' + #9 + AutopayCompany_CallIn + #13 +
    'Call Out' + #9 + AutopayCompany_CallOut + #13 +
    'Email' + #9 + AutopayCompany_Email + #13 +
    'Web' + #9 + AutopayCompany_Web + #13 +
    'Esheets' + #9 + AutopayCompany_Esheets + #13 +
    'Timeclock Import' + #9 + AutopayCompany_TimeclockImport + #13 +
    'HCM' + #9 + AutopayCompany_HCM + #13 +
    'Other'  + #9 + AutopayCompany_Other;

  EnableClientFeatures_ComboChoices =
    'Both' + #9 + CLIENT_FEATURE_BOTH + #13 +
    'Self Serve' + #9 + CLIENT_FEATURE_SELFSERVE + #13 +
    'HR' + #9 + CLIENT_FEATURE_HR + #13 +
    'No' + #9 + CLIENT_FEATURE_NO;

  EnableCompanyFeatures_ComboChoices =
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Remote HR' + #9 + GROUP_BOX_REMOTE_HR + #13 +
    'Web HR' + #9 + GROUP_BOX_YES + #13 +
    'Benefits' + #9 + GROUP_BOX_BENEFITS + #13 +
    'Advanced HR' + #9 + GROUP_BOX_ADVANCED_HR + #13 +
    'AHR Employee Portal Only' + #9 + GROUP_BOX_EMPLOYEE_PORTAL_ONLY;

  EnableCompanyFeatures_ComboChoicesAll =
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Remote HR' + #9 + GROUP_BOX_REMOTE_HR + #13 +
    'Web HR' + #9 + GROUP_BOX_YES;

  EnableCompanyFeatures_ComboChoicesBenefit =
    'Benefits' + #9 + GROUP_BOX_BENEFITS;

  EnableCompanyFeatures_ComboChoicesHR =
    'Advanced HR' + #9 + GROUP_BOX_ADVANCED_HR + #13 +
    'AHR Employee Portal Only' + #9 + GROUP_BOX_EMPLOYEE_PORTAL_ONLY;

  GroupBox_TrustService_ComboChoices =
    'All' + #9 + TRUST_SERVICE_ALL + #13 +
    'Agency Only' + #9 + TRUST_SERVICE_AGENCY + #13 +
    'No' + #9 + TRUST_SERVICE_NO;

  GroupBox_ShowEDHours_ComboChoices =
    'None' + #9 + SHOW_ED_HOURS_ON_NONE + #13 +
    'Both' + #9 + SHOW_ED_HOURS_ON_BOTH + #13 +
    'Check' + #9 + SHOW_ED_HOURS_ON_CHECK + #13 +
    'Reports' + #9 + SHOW_ED_HOURS_ON_REPORTS;

  GroupBox_Analytics_Personel_ComboChoices =
    'N/A' + #9 + USER_ANALYTICS_PERSONNEL_NA + #13 +
    'Service Bureau Admin' + #9 + USER_ANALYTICS_PERSONNEL_SB_ADMIN + #13 +
    'Client User' + #9 + USER_ANALYTICS_PERSONNEL_CL_USER;

  GroupBox_Analytics_License_ComboChoices =
    'N/A' + #9 + ANALYTICS_LICENSE_NA + #13 +
    'Basic' + #9 + ANALYTICS_LICENSE_BASIC;
    // + #13 +
    //'Plus' + #9 + ANALYTICS_LICENSE_PLUS + #13 +
    //'Professional' + #9 + ANALYTICS_LICENSE_PROFESSIONAL;

  GroupBox_Aca_policy_Origin_ComboChoices =
    'A. Small Business Health Options Program (SHOP)' + #9 + ACA_POLICY_ORIGIN_A + #13 +
    'B. Employer-Sponsored Coverage' + #9 + ACA_POLICY_ORIGIN_B + #13 +
    'C. Government-Sponsored Program' + #9 + ACA_POLICY_ORIGIN_C + #13 +
    'D. Individual Market Insurance' + #9 + ACA_POLICY_ORIGIN_D + #13 +
    'E. Multiemployer Plan' + #9 + ACA_POLICY_ORIGIN_E + #13 +
    'F. Miscellaneous Minimum Essential Coverage' + #9 + ACA_POLICY_ORIGIN_F;

  GroupBox_Aca_Cert_eligibility_Type_ComboChoices =
    'Qualifying Offer Method' + #9 + ACA_CERT_ELIGIBILITY_TYPE_A + #13 +
    'Qualifying Offer Method Transition Relief' + #9 + ACA_CERT_ELIGIBILITY_TYPE_B + #13 +
    'Section 4980H Transition Relief' + #9 + ACA_CERT_ELIGIBILITY_TYPE_C + #13 +
    '98% Offer Method' + #9 + ACA_CERT_ELIGIBILITY_TYPE_D;

  GroupBox_YesNo_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO;

  GroupBox_YesNoBoth_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Both' + #9 + GROUP_BOX_ACA_BOTH;

  GroupBox_EvoPayroll_No_ComboChoices =
    'Evolution Payroll' + #9 + GROUP_BOX_EVOPAYROLL + #13 +
    'No' + #9 + GROUP_BOX_NO;

  GroupBox_Entry_Mode_ComboChoices =
    'Expert' + #9 + ENTRY_MODE_EXPERT + #13 +
    'Novice' + #9 + ENTRY_MODE_NOVICE;

  GroupBox_SetupComplete_ComboChoices =
    'No' + #9 + SETUP_COMPLETED_NO + #13 +
    'Payroll' + #9 + SETUP_COMPLETED_PAYROLL + #13 +
    'Payroll / HR Live' + #9 + SETUP_COMPLETED_PAYROLL_HR_LIVE + #13 +
    'Payroll / HR Demo' + #9 + SETUP_COMPLETED_PAYROLL_HR_DEMO + #13 +
    'HR Only Live' + #9 + SETUP_COMPLETED_HR_ONLY_LIVE + #13 +
    'HR Only Demo' + #9 + SETUP_COMPLETED_HR_ONLY_DEMO;

  GroupBox_Show_In_Payroll_ComboChoices =
    'No' + #9 + SHOW_IN_PAYROLL_NO + #13 +
    'Personal' + #9 + SHOW_IN_PAYROLL_PERSONAL + #13 +
    'Pay' + #9 + SHOW_IN_PAYROLL_PAY + #13 +
    'Shifts' + #9 + SHOW_IN_PAYROLL_SHIFTS + #13 +
    'Delivery' + #9 + SHOW_IN_PAYROLL_DELIVERY + #13 +
    'Default' + #9 + SHOW_IN_PAYROLL_DEFAULT;

  GroupBox_ALE_Transition_ComboChoices =
    'None' + #9 + GROUP_BOX_ALE_TRANSITION_NONE + #13 +
    '50-99' + #9 + GROUP_BOX_ALE_TRANSITION_50_99 + #13 +
    '100+' + #9 + GROUP_BOX_ALE_TRANSITION_100;

  GroupBox_PayrollProduct_ComboChoices =
    'SB Batch Entry' + #9 + PAYROLL_PRODUCT_SB_BATCH_ENTRY + #13 +
    'Evolution Payroll' + #9 + PAYROLL_PRODUCT_EVOLUTION_PAYROLL + #13 +
    'Evolution HCM' + #9 + PAYROLL_PRODUCT_EVOLUTION_HCM + #13 +
    'WebClient' + #9 + PAYROLL_PRODUCT_WEBCLIENT + #13 +
    'Remote Client' + #9 + PAYROLL_PRODUCT_REMOTE_CLIENT;

  GroupBox_AllNoneSelect_ComboChoices =
    'All' + #9 + GROUP_BOX_YES + #13 +
    'None' + #9 + GROUP_BOX_NO + #13 +
    'Select' + #9 + GROUP_BOX_CONDITIONAL ;

  GroupBox_YesNoAsk_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Ask' + #9 + GROUP_BOX_ASK;

  GroupBox_YesNoNotApplicative_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Not Applicable' + #9 + GROUP_BOX_NOT_APPLICATIVE;

  GroupBox_YesNoDontcare_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    '-- Don''t care --' + #9 + GROUP_BOX_NOT_APPLICATIVE;

  GroupBox_YesNoConditional_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Conditional' + #9 + GROUP_BOX_CONDITIONAL;

  GroupBox_PensionW2_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'System Rules' + #9 + GROUP_BOX_SYSTEM;

  GroupBox_ExemptExclude_ComboChoices =
    'Exempt' + #9 + GROUP_BOX_EXEMPT + #13 +
    'Block' + #9 + GROUP_BOX_EXCLUDE + #13 +
    'Include' + #9 + GROUP_BOX_INCLUDE;

  GroupBox_LLExemptExclude_ComboChoices =
    'Exempt' + #9 + GROUP_BOX_EXEMPT + #13 +
    'Block' + #9 + GROUP_BOX_EXCLUDE + #13 +
    'Include' + #9 + GROUP_BOX_INCLUDE + #13 +
    'PA Block' + #9 + GROUP_BOX_PABLOCK;

  GroupBox_LL1ExemptExclude_ComboChoices =
    'Exempt' + #9 + GROUP_BOX_EXEMPT + #13 +
    'Block' + #9 + GROUP_BOX_EXCLUDE + #13 +
    'Include' + #9 + GROUP_BOX_INCLUDE + #13;

  GroupBox_LL2ExemptExclude_ComboChoices =
    'PA Block' + #9 + GROUP_BOX_PABLOCK;

  GroupBox_ExcludeTimeOff_ComboChoices =
    'All' + #9 + TIME_OFF_EXCLUDE_ACCRUAL_ALL + #13 +
    'Accrual' + #9 + TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL + #13 +
    'No' + #9 + TIME_OFF_EXCLUDE_ACCRUAL_NONE;

  GroupBox_YesNoFullAccess_ComboChoices =
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Read Only' + #9 + GROUP_BOX_YES + #13 +
    'Full Access' + #9 + GROUP_BOX_FULL_ACCESS;

  GroupBox_YesNoReadOnly_ComboChoices =
    'No' + #9 + GROUP_BOX_NO + #13 +
    'Read Only' + #9 + GROUP_BOX_READONLY + #13 +
    'Full Access' + #9 + GROUP_BOX_YES;

  GroupBox_ESS_or_EP_ComboChoices =
    'Employee Portal' + #9 + GROUP_BOX_EP+ #13 +
    'Employee Self Serve' + #9 + GROUP_BOX_ESS;


  GroupBox_BenefitAvailable_ComboChoices =
    'Employee' + #9 + GROUP_BOX_BENEFIT_EMPLOYEE + #13 +
    'Employee & Dependents' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO;


  GroupBox_Last_qual_event_ComboChoices =
    'Marriage/Divorce/Legal Separation/Annulment' + #9 + LAST_QUAL_MARIAGE + #13 +
    'Death of Spouse' + #9 + LAST_QUAL_DEATH_SPOUSE + #13 +
    'Birth/Adoption ' + #9 + LAST_QUAL_BIRTH + #13 +
    'Death of Dependent' + #9 + LAST_QUAL_DEATH_DEPENDENT + #13 +
    'Change from Part Time to Full Time by Employee' + #9 + LAST_QUAL_FULL + #13 +
    'Change from Full Time to Part Time by Employee' + #9 + LAST_QUAL_PART + #13 +
    'Employee becomes Eligible for Coverage' + #9 + LAST_QUAL_ELIGIBLE + #13 +
    'Loss of Spouse''s Employment or Benefits' + #9 + LAST_QUAL_LOST_SPOUSE + #13 +
    'Commencement of Spouse''s Employment or Benefits' + #9 + LAST_QUAL_SPOUSE + #13 +
    'Dependent becomes Ineligible for Coverage' + #9 + LAST_QUAL_DEPENDENT + #13 +
    'Family Medical Leave Act (FMLA) leave' + #9 + LAST_QUAL_FMLA + #13 +
    'Medicare/Medicaid Eligibility' + #9 + LAST_QUAL_MEDICARE + #13 +
    'Court Order' + #9 + LAST_QUAL_COURT_ORDER;

{The entry below is here becuase there was inconsistency with in the group boxes
 that need the values above in form SPD_EDIT_CL_ED_STATE_EXEMPT_EXCLUDE}

  GroupBox_ExmptExde_ComboChoices =
    'Exempt' + #9 + GROUP_BOX_EXE + #13 +
    'Block' + #9 + GROUP_BOX_EXC + #13 +
    'Include' + #9 + GROUP_BOX_INCLUDE;

  GroupBox_SbClient_ComboChoices =
    'Service Bureau' + #9 + GROUP_BOX_SB + #13 +
    'Client' + #9 + GROUP_BOX_CLIENT;

  GroupBox_UsedBy_ComboChoices =
    'Service Bureau' + #9 + GROUP_BOX_SB + #13 +
    'Bank' + #9 + GROUP_BOX_BANK + #13 +
    'Both' + #9 + GROUP_BOX_BOTH;

  GroupBox_SingleMarried_ComboChoices =
    'Single' + #9 + GROUP_BOX_SINGLE + #13 +
    'Married' + #9 + GROUP_BOX_MARRIED;

  GroupBox_Gender_ComboChoices =
    'Male' + #9 + GROUP_BOX_MALE + #13 +
    'Female' + #9 + GROUP_BOX_FEMALE + #13 +
    'N/A' + #9 + GROUP_BOX_UNKOWN;

  GroupBox_EEorER_ComboChoices =
    'EE Primary' + #9 + GROUP_BOX_EE + #13 +
    'ER Primary' + #9 + GROUP_BOX_ER + #13 +
    'EE Other' + #9 + GROUP_BOX_EE_OTHER + #13 +
    'ER Other' + #9 + GROUP_BOX_ER_OTHER;

  GroupBox_EmployeeorEmployer_ComboChoices =
    'Employer' + #9 + GROUP_BOX_EMPLOYER + #13 +
    'Employee' + #9 + GROUP_BOX_EMPLOYEE;

  GroupBox_AccountType_ComboChoices =
    'Checking' + #9 + GROUP_BOX_CHECKING + #13 +
    'Savings' + #9 + GROUP_BOX_SAVINGS + #13 +
    'Money Market' + #9 + GROUP_BOX_MONEY;

  GroupBox_DebitORCredit_ComboChoices =
    'Debit' + #9 + GROUP_BOX_DEBIT + #13 +
    'Credit' + #9 + GROUP_BOX_CREDIT;

  GroupBox_BankFormat_ComboChoices =
    'Anexsys' + #9 + GROUP_BOX_CHICAGO + #13 + // was 'First Chicago'
    'Bank of America' + #9 + GROUP_BOX_NATIONS + #13 + // was 'NationsBank'
    'Batch Provider' + #9 + GROUP_BOX_BATCH_PROVIDER;

  GroupBox_BenefitType_ComboChoices =
    'Plan' + #9 + GROUP_BOX_PLAN + #13 +
    'Policy' + #9 + GROUP_BOX_POLICY;

  GroupBox_BenefitStatus_ComboChoices =
    'Completed' + #9 + GROUP_BOX_COMPLETED + #13 +
    'Pending' + #9 + GROUP_BOX_PENDING;

  GroupBox_Invoice_ComboChoices =
    'No Invoice' + #9 + GROUP_BOX_NO_INVOICE + #13 +
    'Invoice w/Payroll' + #9 + GROUP_BOX_INVOICE_W_PAYROLL;

  GroupBox_Type_ComboChoices =
    'Amount' + #9 + GROUP_BOX_AMOUNT + #13 +
    '%' + #9 + GROUP_BOX_PERCENT + #13 +
    'None' + #9 + GROUP_BOX_NONE;

  GroupBox_ConnectionType_ComboChoices =
    'Direct Connection' + #9 + GROUP_BOX_DIR_CONNECT + #13 +
    'Modem' + #9 + GROUP_BOX_MODEM;

  GroupBox_CustomSystem_ComboChoices =
    'Yes' + #9 + GROUP_BOX_SUMERIZE_YES + #13 +
    'No' + #9 + GROUP_BOX_SUMERIZE_NO;

  GroupBox_SalaryHourlyBoth_ComboChoices =
    'Salary' + #9 + GROUP_BOX_SALARY + #13 +
    'Hourly' + #9 + GROUP_BOX_HOURLY + #13 +
    'Both' + #9 + GROUP_BOX_BOTH2;

  GroupBox_HourlySalariedBoth_ComboChoices =
    'Hourly' + #9 + GROUP_BOX_HOURLY + #13 +
    'Salaried' + #9 + GROUP_BOX_SALARY + #13 +
    'Both' + #9 + GROUP_BOX_BOTH2;

  GroupBox_ExcludeTax_ComboChoices =
    'Deposits' + #9 + GROUP_BOX_DEPOSIT + #13 +
    'Liabilities' + #9 + GROUP_BOX_LIABILITIES + #13 +
    'Both' + #9 + GROUP_BOX_BOTH2 + #13 +
    'None' + #9 + GROUP_BOX_NONE;

  GroupBox_Exclude_ComboChoices =
    'Reports' + #9 + GROUP_BOX_REPORTS + #13 +
    'Checks' + #9 + GROUP_BOX_CHECKS + #13 +
    'Both' + #9 + GROUP_BOX_BOTH2 + #13 +
    'None' + #9 + GROUP_BOX_NONE;

  GroupBox_EINorSSN_ComboChoices =
    'EIN' + #9 + GROUP_BOX_EIN + #13 +
    'SSN' + #9 + GROUP_BOX_SSN;

  GroupBox_COorINDIV_ComboChoices =
    '1099' + #9 + GROUP_BOX_COMPANY + #13 +
    'W-2' + #9 + GROUP_BOX_INDIVIDUAL;

  GroupBox_WichChecks_ComboChoices =
    'First' + #9 + GROUP_BOX_FIRST + #13 +
    'Last' + #9 + GROUP_BOX_LAST + #13 +
    'Closest to 15' + #9 + GROUP_BOX_CLOSEST_TO_15 + #13 +
    'All' + #9 + GROUP_BOX_ALL;

  GroupBox_ED_PrintOnWorksheet_ComboChoices =
    'Don''t print at all' + #9 + ED_PRINT_ON_WORKSHEET_No + #13 +
    'Only on bottom' + #9 + ED_PRINT_ON_WORKSHEET_OnlyOnBottom + #13 +
    'On bottom and on top #1' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsFirst + #13 +
    'On bottom and on top #2' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsSecond + #13 +
    'On bottom and on top #3' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsThird + #13 +
    'On bottom and on top #4' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsForth + #13 +
    'On bottom and on top #5' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsFifth + #13 +
    'On bottom and on top #6' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsSixth + #13 +
    'On bottom and on top #7' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsSeventh + #13 +
    'On bottom and on top #8' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsEight + #13 +
    'On bottom and on top #9' + #9 + ED_PRINT_ON_WORKSHEET_OnBottomAndTopAsNinth;

  GroupBox_Bank_Register_Status_ComboChoices =
    'Cleared' + #9 + BANK_REGISTER_STATUS_Cleared + #13 +
    'Returned' + #9 + BANK_REGISTER_STATUS_Returned + #13 +
    'Issued' + #9 + BANK_REGISTER_STATUS_Issued + #13 +
    'Outstanding' + #9 + BANK_REGISTER_STATUS_Outstanding + #13 +
    'Stop payment' + #9 + BANK_REGISTER_STATUS_Stop_payment + #13 +
    'Dead' + #9 + BANK_REGISTER_STATUS_Dead + #13 +
    'NSF' + #9 + BANK_REGISTER_STATUS_NSF + #13 +
    'Paid NSF' + #9 + BANK_REGISTER_STATUS_Paid_NSF + #13 +
    'Write Off' + #9 + BANK_REGISTER_STATUS_WriteOff + #13 +
    'Pending' + #9 + BANK_REGISTER_STATUS_Pending;

  GroupBox_BANK_REGISTER_ManualType_ComboChoices =
    'Interest' + #9 + BANK_REGISTER_MANUALTYPE_Interest + #13 +  // I
    'Fees' + #9 + BANK_REGISTER_MANUALTYPE_Fees + #13 +  // F
    'Manual deposit' + #9 + BANK_REGISTER_MANUALTYPE_ManualDeposit + #13 + //
    'Non-system check' + #9 + BANK_REGISTER_MANUALTYPE_NonSystemCheck + #13 +
    'Misc' + #9 + BANK_REGISTER_MANUALTYPE_Misc + #13 +
    'Sweep' + #9 + BANK_REGISTER_MANUALTYPE_Sweep + #13 +
    'Adjustment' + #9 + BANK_REGISTER_MANUALTYPE_Adjustment + #13 +
    'Void. Was made by mistake' + #9 + BANK_REGISTER_MANUALTYPE_MadeByMistake_Void + #13 +
    'Non-manual' + #9 + BANK_REGISTER_MANUALTYPE_None + #13 +
    'System Check' + #9 + BANK_REGISTER_MANUALTYPE_SystemCheck + #13 +
    'Wire Transfer' + #9 + BANK_REGISTER_MANUALTYPE_WireTransfer + #13 +
    'Other' + #9 + BANK_REGISTER_MANUALTYPE_Other;

  GroupBox_Bank_Register_Type_ComboChoices =
    'Regular' + #9 + BANK_REGISTER_TYPE_Regular + #13 +
    'Void' + #9 + BANK_REGISTER_TYPE_Void + #13 +
    'Manual' + #9 + BANK_REGISTER_TYPE_Manual + #13 +
    'Agency' + #9 + BANK_REGISTER_TYPE_Agency + #13 +
    'Tax' + #9 + BANK_REGISTER_TYPE_Tax + #13 +
    'Billing' + #9 + BANK_REGISTER_TYPE_Billing + #13 +
    'Agency Void' + #9 + BANK_REGISTER_TYPE_Agency_Void + #13 +
    'Tax Void' + #9 + BANK_REGISTER_TYPE_Tax_Void + #13 +
    'Billing Void' + #9 + BANK_REGISTER_TYPE_Billing_Void + #13 +
    'Trust Impound' + #9 + BANK_REGISTER_TYPE_TrustImpound + #13 +
    'Tax Impound' + #9 + BANK_REGISTER_TYPE_TaxImpound + #13 +
    'Billing Impound' + #9 + BANK_REGISTER_TYPE_BillingImpound + #13 +
    'Workers Comp Impound' + #9 + BANK_REGISTER_TYPE_WCImpound + #13 +
    'EFT Payment' + #9 + BANK_REGISTER_TYPE_EFTPayment + #13 +
    'Trust Offset' + #9 + BANK_REGISTER_TYPE_TrustOffset + #13 +
    'Tax Offset' + #9 + BANK_REGISTER_TYPE_TaxOffset + #13 +
    'Billing Offset' + #9 + BANK_REGISTER_TYPE_BillingOffset + #13 +
    'Workers Comp Offset' + #9 + BANK_REGISTER_TYPE_WCOffset + #13 +
    'Dir.Deposit Offset' + #9 + BANK_REGISTER_TYPE_DirDepOffset + #13 +
    'Consolidated' + #9 + BANK_REGISTER_TYPE_Consolidated;

  Print_Signature_ComboChoices =
    'All Checks' + #9 + PRINT_SIGNATURE_ON_ALL_CHECKS + #13 +
    'Regular Checks' + #9 + PRINT_SIGNATURE_ON_PAYROLL_CHECKS + #13 +
    'Misc. Checks' + #9 + PRINT_SIGNATURE_ON_MISC_CHECKS;

  Credit_Hold_Level_ComboChoices =
    'None' + #9 + CREDIT_HOLD_LEVEL_NONE + #13 +
    'Low' + #9 + CREDIT_HOLD_LEVEL_LOW + #13 +
    'Medium' + #9 + CREDIT_HOLD_LEVEL_MEDIUM + #13 +
    'High' + #9 + CREDIT_HOLD_LEVEL_HIGH + #13 +
    'Read Only' + #9 + CREDIT_HOLD_LEVEL_READONLY;

  GroupBox_FullandRemoteReadOnly_ComboChoices =
    'Maintenance' + #9 + READ_ONLY_MAINTENANCE + #13 +
    'Read Only' + #9 + READ_ONLY_READ_ONLY + #13 +
    'Full Access' + #9 + GROUP_BOX_NO;

  Tax_Service_ComboChoices =
    'Full'#9+ TAX_SERVICE_FULL+ #13+
    'Direct'#9+ TAX_SERVICE_DIRECT+ #13+
    'None'#9+ TAX_SERVICE_NONE;

  Deduct_ComboChoices =
    'Always'#9 + DEDUCT_ALWAYS + #13+
    'No Overrides'#9 + DEDUCT_NO_OVERRIDES + #13+
    'Never'#9 + DEDUCT_NEVER;

  GroupBox_WCOvertimeToReport_ComboChoices =
    'Overtime Only' + #9 + GROUP_BOX_WC_OVERTIME_ONLY + #13 +
    'Overtime and Premium' + #9 + GROUP_BOX_WC_OVERTIME_AND_PREMIUM + #13 +
    'None' + #9 + GROUP_BOX_WC_NONE;

//HR fields

  GroupBox_PersonalOrCompany_ComboChoices =
    'Personal' + #9 + CAR_PERSONAL + #13+
    'Company' + #9 + CAR_COMPANY;

  GroupBox_CarColor_ComboChoices =
    'Red' + #9 + CAR_COLOR_RED + #13 +
    'Blue' + #9 + CAR_COLOR_BLUE + #13 +
    'Black' + #9 + CAR_COLOR_BLACK + #13 +
    'Silver' + #9 + CAR_COLOR_SILVER + #13 +
    'Green' + #9 + CAR_COLOR_GREEN + #13 +
    'Yellow' + #9 + CAR_COLOR_YELLOW + #13 +
    'White' + #9 + CAR_COLOR_WHITE + #13 +
    'Brown' + #9 + CAR_COLOR_BROWN + #13 +
    'Gold' + #9 + CAR_COLOR_GOLD + #13 +
    'Other' + #9 + CAR_COLOR_OTHER;

  GroupBox_DesiredRegularOrTemporary_ComboChoices =
    'Regular' + #9 + DESIRED_REGULAR + #13 +
    'Temporary' + #9 + DESIRED_TEMPORARY;

  GroupBox_ApplicantStatus_ComboChoices =
    'Sent an Application' + #9 + APPLICANT_STATUS_SENT_APPLICATION + #13 +
    'Resume Received' + #9 + APPLICANT_STATUS_RESUME_RECEIVED + #13 +
    'Contacted' + #9 + APPLICANT_STATUS_CONTACTED + #13 +
    'First Interview' + #9 + APPLICANT_STATUS_FIRST_INTRVIEW + #13 +
    'Second Interview' + #9 + APPLICANT_STATUS_SECOND_INTERVIEW + #13 +
    'Declined Interview' + #9 + APPLICANT_STATUS_DECLINED_INTERVIEW + #13 +
    'Position Offered' + #9 + APPLICANT_STATUS_POSITION_OFFERED + #13 +
    'Position Accepted' + #9 + APPLICANT_STATUS_POSITION_ACCEPTED + #13 +
    'Declined Position' + #9 + APPLICANT_STATUS_DECLINED_POSITION + #13 +
    'Hired' + #9 + APPLICANT_STATUS_HIRED + #13 +
    'Retracted Offer' + #9 + APPLICANT_STATUS_RETRACTED_OFFER + #13 +
    'Failed to Complete Process/Paperwork' + #9 + APPLICANT_STATUS_FAILED_TO_COMPLETE_PROCESS + #13 +
    'Failed Physical/Screening' + #9 + APPLICANT_STATUS_FAILED_PHYSICAL_SCREENING + #13 +
    'Opening Withdrawn' + #9 + APPLICANT_STATUS_OPENING_WITHDRAWN + #13 +
    'Rejected' +#9 + APPLICANT_STATUS_REJECTED;

  GroupBox_ApplicantType_ComboChoices =
    'None' + #9 + APPLICANT_TYPE_NONE + #13 +
    'Application' + #9 + APPLICANT_TYPE_APPLICATION + #13 +
    'Resume' + #9 + APPLICANT_TYPE_RESUME + #13 +
    'Application + Resume' + #9 + APPLICANT_TYPE_APPLICATION_RESUME;

  GroupBox_DegreeLevel_ComboChoices =
    'Associates' + #9 + DEGREE_LEVEL_ASSOCIATES + #13 +
    'Bachelors' + #9 + DEGREE_LEVEL_BACHELORS + #13 +
    'Masters' + #9 + DEGREE_LEVEL_MASTERS + #13 +
    'Doctorate' + #9 + DEGREE_LEVEL_DOCTORATE + #13 +
    'GED' + #9 + DEGREE_LEVEL_GED + #13 +
    'Certificate' + #9 + DEGREE_LEVEL_CERTIFICATE + #13 +
    'High School Diploma' + #9 + DEGREE_LEVEL_HIGH_SCHOOL_DIPLOMA + #13 +
    'Other' + #9 + DEGREE_LEVEL_OTHER;

  GroupBox_CaseStatus_ComboChoices =
    'Open' + #9 + CASE_STATUS_OPEN + #13 +
    'Closed' + #9 + CASE_STATUS_CLOSED;

  GroupBox_PensionPlan1099R_ComboChoices =
    'None' + #9 + PENSION_PLAN_1099R_NONE + #13 +
    'IRA' + #9 + PENSION_PLAN_1099R_IRA + #13 +
    'SEP' + #9 + PENSION_PLAN_1099R_SEP + #13 +
    'Simple' + #9 + PENSION_PLAN_1099R_SIMPLE;
//end of HR

  GroupBox_LocalEEorER_ComboChoices =
    'EE' + #9 + GROUP_BOX_EE + #13 +
    'ER' + #9 + GROUP_BOX_ER;

  BlockACHImpound_ComboChoices =
    'None' + #9 + BLOCK_ACH_IMPOUND_NONE + #13 +
    'Tax' + #9 + BLOCK_ACH_IMPOUND_TAX + #13 +
    'Trust' + #9 + BLOCK_ACH_IMPOUND_TRUST + #13 +
    'Both' + #9 + BLOCK_ACH_IMPOUND_BOTH;

  CheckTOBalance_ComboChoices =
    'Yes' + #9 + GROUP_BOX_YES + #13 +
    'No' + #9 + GROUP_BOX_NO + #13 +
    'On Submit and Pre-process' + #9 + CHECK_TO_BALANCE_ON_SUBMIT;

  ReprintTOBalance_ComboChoices =
    'Current' + #9 + GROUP_BOX_CURRENT + #13 +
    'Historical' + #9 + GROUP_BOX_HISTORICAL;

  Category_Type_ComboChoices =
    'W/H'#9 + CATEGORY_TYPE_WH + #13 +
    'SUI'#9 + CATEGORY_TYPE_SUI + #13 +
    'SUI Magmedia'#9 + CATEGORY_TYPE_SUI_MAG + #13 +
    'EFT'#9 + CATEGORY_TYPE_EFT + #13 +
    'Forms'#9 + CATEGORY_TYPE_FORMS + #13 +
    'W-2 Magmedia'#9 + CATEGORY_TYPE_W2;

  Report_Level_ComboChoices =
    'System'#9 + REPORT_LEVEL_SYSTEM + #13 +
    'Bureau'#9 + REPORT_LEVEL_BUREAU + #13 +
    'Client'#9 + REPORT_LEVEL_CLIENT;

  GroupBox_PrApproved_ComboChoices =
    'Ignore'#9 + PR_APPROVED_IGNORE + #13 +
    'Yes'#9 + PR_APPROVED_YES + #13 +
    'No'#9 + PR_APPROVED_NO;

  BenefitAmountType_ComboChoices =
    'None' + #9 + BENEFIT_AMOUNT_TYPE_NONE + #13 +
    'Single Percent' + #9 + BENEFIT_AMOUNT_TYPE_SINGLE_PERCENT + #13 +
    'Single Amount' + #9 + BENEFIT_AMOUNT_TYPE_SINGLE_AMOUNT + #13 +
    'Double Percent' + #9 + BENEFIT_AMOUNT_TYPE_DOUBLE_PERCENT + #13 +
    'Double Amount' + #9 + BENEFIT_AMOUNT_TYPE_DOUBLE_AMOUNT + #13 +
    'Family Percent' + #9 + BENEFIT_AMOUNT_TYPE_FAMILY_PERCENT + #13 +
    'Family Amount' + #9 + BENEFIT_AMOUNT_TYPE_FAMILY_AMOUNT + #13 +
    'Flat Rate' + #9 + BENEFIT_AMOUNT_TYPE_FLAT_RATE;

  VISAType_ComboChoices =
    'None' + #9 + VISA_TYPE_NONE + #13 +
    'B1' + #9 + VISA_TYPE_B1 + #13 +
    'F1' + #9 + VISA_TYPE_F1 + #13 +
    'F2' + #9 + VISA_TYPE_F2 + #13 +
    'H1A' + #9 + VISA_TYPE_H1A + #13 +
    'H1B' + #9 + VISA_TYPE_H1B + #13 +
    'H1B Res' + #9 + VISA_TYPE_H1B_RES + #13 +
    'H2A' + #9 + VISA_TYPE_H2A + #13 +
    'H2B' + #9 + VISA_TYPE_H2B + #13 +
    'H3' + #9 + VISA_TYPE_H3 + #13 +
    'H4' + #9 + VISA_TYPE_H4 + #13 +
    'J1' + #9 + VISA_TYPE_J1 + #13 +
    'J2' + #9 + VISA_TYPE_J2 + #13 +
    'L1' + #9 + VISA_TYPE_L1 + #13 +
    'L2' + #9 + VISA_TYPE_L2 + #13 +
    'N1-7' + #9 + VISA_TYPE_N1_7 + #13 +
    'O1' + #9 + VISA_TYPE_O1 + #13 +
    'R1' + #9 + VISA_TYPE_R1 + #13 +
    'R2' + #9 + VISA_TYPE_R2 + #13 +
    'TC' + #9 + VISA_TYPE_TC + #13 +
    'TN' + #9 + VISA_TYPE_TN + #13 +
    'Other' + #9 + VISA_TYPE_OTHER;

  CoNameOnInvoice_ComboChoices =
    'Yes' + #9 + NAME_ON_INVOICE_ACCOUNTANT + #13 +
    'No' + #9 + NAME_ON_INVOICE_BUREAU;


  Cobra_Reason_For_Refusal_ComboChoices =
    'Spouse Coverage' + #9 + COBRA_REASON_FOR_REFUSAL_SPOUSE_COVERAGE + #13 +
    'Covered Under Another Plan' + #9 + COBRA_REASON_FOR_REFUSAL_ANOTHER_PLAN + #13 +
    'Highly Compensated' + #9 + COBRA_REASON_FOR_REFUSAL_HIGHLY_COMPENSATED + #13 +
    'Other' + #9 + COBRA_REASON_FOR_REFUSAL_OTHER;

  DeductionsTakeFirst_ComboChoices =
    'Child Support, Garnishment' + #9 + TAKE_FIRST_CHILD_GARNISHMENT  + #13 +
    'Garnishment, Child Support' + #9 + TAKE_FIRST_GARNISHMENT_CHILD  + #13 +
    'Child Support' + #9 + TAKE_FIRST_CHILD  + #13 +
    'Garnishment' + #9 + TAKE_FIRST_GARNISHMENT  + #13 +
    'Use E/D Priorities' + #9 + TAKE_FIRST_NONE;

  Benefit_Rate_Type_ComboChoices =
    'Amount' + #9 + BENEFIT_RATES_RATE_TYPE_AMOUNT + #13 +
    'Percent' + #9 + BENEFIT_RATES_RATE_TYPE_PERCENT;

  Amount_Type_ComboChoices =
    'Amount' + #9 + BENEFIT_RATES_RATE_TYPE_AMOUNT + #13 +
    'Percent' + #9 + BENEFIT_RATES_RATE_TYPE_PERCENT;

  Calc_State_First_ComboChoices =
    'No' + #9 + CALC_FED_STATE_SUI + #13 +
    'State,SUI' + #9 + CALC_STATE_SUI_FED + #13 +
    'SUI,State' + #9 + CALC_SUI_STATE_FED;

  Healthcare_Coverage_ComboChoices =
    'N/A' + #9 + HEALTHCARE_NA + #13 +
    'No ER Paid Ins/Not Eligible' + #9 + HEALTHCARE_NOT_ELIGIBLE + #13 +
    'Eligible/Not Covered' + #9 + HEALTHCARE_ELIGIBLE_NOT_INSURED + #13 +
    'Eligible/Covered' + #9 + HEALTHCARE_ELIGIBLE_INSURED;

  ExcludeTaxes_ComboChoices =
    'Additional' + #9 + EXCLUDE_TAXES_ADDITIONAL + #13 +
    'Override Regular' + #9 + EXCLUDE_TAXES_OVERRIDE_REGULAR + #13 +
    'Both' + #9 + EXCLUDE_TAXES_BOTH + #13 +
    'None' + #9 + EXCLUDE_TAXES_NONE;

  WorkersComp_Wage_Limit_Frequency_ComboChoices =
    'Every Pay' + #9 + WC_LIMIT_FREQ_EVERYPAY + #13 +
    'Monthly' + #9 + WC_LIMIT_FREQ_MONTHLY + #13 +
    'Quarterly' + #9 + WC_LIMIT_FREQ_QUARTERLY + #13 +
    'Semi-Annual' + #9 + WC_LIMIT_FREQ_SEMI_ANNUALLY + #13 +
    'Annual' + #9 + WC_LIMIT_FREQ_ANNUALLY;


  UserSchedTask_ComboChoices =
    'Message' + #9 + UST_MESSAGE;

  UserSchedReminder_ComboChoices =
    'None'#9''#13 +
    '5 min'#9'5'#13 +
    '10 min'#9'10'#13 +
    '15 min'#9'15'#13 +
    '30 min'#9'30'#13 +
    '1 hour'#9'60'#13 +
    '2 hour'#9'120'#13 +
    '3 hour'#9'180'#13 +
    '4 hour'#9'240'#13 +
    '5 hour'#9'300'#13 +
    '6 hour'#9'360'#13 +
    '7 hour'#9'420'#13 +
    '8 hour'#9'480'#13 +
    '9 hour'#9'540'#13 +
    '10 hour'#9'600'#13 +
    '11 hour'#9'660'#13 +
    '12 hour'#9'720'#13 +
    '18 hour'#9'1080'#13 +
    '1 day'#9'1440'#13 +
    '2 day'#9'2880'#13 +
    '3 day'#9'4320'#13 +
    '4 day'#9'5760'#13 +
    '1 week'#9'10080'#13 +
    '2 weeks'#9'20160';

  ESS_GroupType_ComboChoices =
    'Personal Info'#9 + ESS_GROUPTYPE_PERSONALINFO
     + #13+ 'Time Off'#9 + ESS_GROUPTYPE_TIMEOFF
     + #13 + 'Benefit'#9 + ESS_GROUPTYPE_BENEFIT;

  EELoginType_ComboChoices =
    'Low'#9 + LOGIN_TYPE_DEFAULT + #13 +
    'Medium'#9 + LOGIN_TYPE_QUESTIONS_RESTORE + #13 +
    'High'#9 + LOGIN_TYPE_QUESTIONS;

  Display_Cost_Period_ComboChoices =
    'None' + #9 + DISPLAY_COST_PERIOD_NONE + #13 +
    'Pay Period' + #9 + DISPLAY_COST_PERIOD_PAY + #13 +
    'Semi-Monthly' + #9 + DISPLAY_COST_PERIOD_SEMI_MONTHLY + #13 +
    'Monthly' + #9 + DISPLAY_COST_PERIOD_MONTHLY + #13 +
    'Quarterly' + #9 + DISPLAY_COST_PERIOD_QUARTERLY + #13 +
    'Annually' + #9 + DISPLAY_COST_PERIOD_ANNUALLY;

  Requires_DOB_ComboChoices =
    'Both' + #9 + REQUIRES_DOB_BOTH + #13 +
    'Dependent' + #9 + REQUIRES_DOB_DEPENDENT + #13 +
    'Beneficiary' + #9 + REQUIRES_DOB_BENEFICIARY + #13 +
    'None' + #9 + REQUIRES_DOB_NONE;

  Apply_Discount_ComboChoices =
    'Agency' + #9 + APPLY_DISCOUNT_AGENCY + #13 +
    'Employer' + #9 + APPLY_DISCOUNT_EMPLOYER + #13 +
    'Taxable Fringe' + #9 + APPLY_DISCOUNT_TAXABLE_FRINGE + #13 +
    'None' + #9 + APPLY_DISCOUNT_NONE;


  Data_Type_ComboChoices =
    'String' + #9 + DATA_TYPE_STRING + #13 +
    'Date' + #9 + DATA_TYPE_DATE + #13 +
    'Amount' + #9 + DATA_TYPE_AMOUNT + #13 +
    'Value' + #9 + DATA_TYPE_VALUE;

  Benefit_Category_Type_ComboChoices =
   'Health' + #9 + BENEFIT_CATEGORY_TYPE_HEALTH + #13 +
   'HSA' + #9 + BENEFIT_CATEGORY_TYPE_HSA + #13 +
   'FSA' + #9 + BENEFIT_CATEGORY_TYPE_FSA + #13 +
   'Dental' + #9 + BENEFIT_CATEGORY_TYPE_DENTAL + #13 +
   'Vision' + #9 + BENEFIT_CATEGORY_TYPE_VISION + #13 +
   'Dependent Care' + #9 + BENEFIT_CATEGORY_TYPE_DEPENDENT_CARE + #13 +
   'Life Insurance' + #9 + BENEFIT_CATEGORY_TYPE_LIFE_INSURANCE + #13 +
   'Short Term Disability' + #9 + BENEFIT_CATEGORY_TYPE_SHORT_TERM_DISABILITY + #13 +
   'Long Term Disability' + #9 + BENEFIT_CATEGORY_TYPE_LONG_TERM_DISABILITY + #13 +
   'Pension' + #9 + BENEFIT_CATEGORY_TYPE_PENSION + #13 +
   'Additional Health 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_1 + #13 +
   'Additional Health 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_2 + #13 +
   'Additional Health 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_3 + #13 +
   'Additional Dental 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_1 + #13 +
   'Additional Dental 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_2 + #13 +
   'Additional Dental 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_3 + #13 +
   'Additional Life Insurance 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_LIFE_INSURANCE_1 + #13 +
   'Additional Life Insurance 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_LIFE_INSURANCE_2 + #13 +
   'Additional Life Insurance 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_LIFE_INSURANCE_3 + #13 +
   'Additional Disability 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DISABILITY_1 + #13 +
   'Additional Disability 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DISABILITY_2 + #13 +
   'Additional Disability 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_DISABILITY_3 + #13 +
   'Additional Vision 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_1 + #13 +
   'Additional Vision 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_2 + #13 +
   'Additional Vision 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_3 + #13 +
   'Miscellaneous 1' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_MISCELLANEOUS_1 + #13 +
   'Miscellaneous 2' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_MISCELLANEOUS_2 + #13 +
   'Miscellaneous 3' + #9 + BENEFIT_CATEGORY_TYPE_ADDITIONAL_MISCELLANEOUS_3;


  Enrollment_Frequency_ComboChoices =
   'Annual'       + #9 + ENROLLMENT_FREQUENCY_ANNUAL + #13 +
   'Semi-Annual'  + #9 + ENROLLMENT_FREQUENCY_SEMIANNUAL + #13 +
   'Quarterly'    + #9 + ENROLLMENT_FREQUENCY_QUARTERLY + #13 +
   'Always'       + #9 + ENROLLMENT_FREQUENCY_ALWAYS + #13 +
   'None'         + #9 + ENROLLMENT_FREQUENCY_NONE;

  Language_ComboChoices =
   'English' + #9 + LANGUAGE_ENGLISH + #13 +
   'Spanish' + #9 + LANGUAGE_SPANISH + #13 +
   'Chinese' + #9 + LANGUAGE_CHINESE + #13 +
   'Korean'  + #9 + LANGUAGE_KOREAN  + #13 +
   'Creole'  + #9 + LANGUAGE_CREOLE  + #13 +
   'Polish'  + #9 + LANGUAGE_POLISH  + #13 +
   'Russian' + #9 + LANGUAGE_RUSSIAN + #13 +
   'Other'   + #9 + LANGUAGE_OTHER;

  Cafeteria_Freq_ComboChoices =
   'Annual' + #9 + CAFETERIA_FREQ_ANNUAL + #13 +
   'Monthly' + #9 + CAFETERIA_FREQ_MONTHLY + #13 +
   'Quarterly' + #9 + CAFETERIA_FREQ_QUARTERLY + #13 +
   'PerPay'  + #9 + CAFETERIA_FREQ_PERPAY;

  Person_type_ComboChoices =
   'Dependent' + #9 + PERSON_TYPE_DEPENDENT + #13 +
   'Beneficiary' + #9 + PERSON_TYPE_BENEFICIARY;

   GroupBox_UsedAsBenefit_ComboChoices =
    'None' + #9 + USEDASBENEFIT_NONE + #13 +
    'EE' + #9 + USEDASBENEFIT_EE + #13 +
    'Client' + #9 + USEDASBENEFIT_CLIENT;


  GroupBox_Category_ComboChoices =
    'Yes' + #9 + GROUP_BOX_EMPLOYEE + #13 +
    'No' + #9 + GROUP_BOX_COMPANY;

  GroupBox_Level_ComboChoices =
    'System' + #9 + LEVEL_SYSTEM + #13 +
    'Bureau' + #9 + LEVEL_BUREAU;

  GroupBox_Level_Company_ComboChoices =
    'Bureau' + #9 + LEVEL_BUREAU + #13 +
    'Company' + #9 + LEVEL_COMPANY;

  GroupBox_Vendor_Type_ComboChoices =
    'Strategic' + #9 + VENDOR_TYPE_STRATEGIC + #13 +
    'Integrated' + #9 + VENDOR_TYPE_INTEGRATED;

  GroupBox_Vendor_Type_ComboChoicesSB =
    'Other' + #9 + VENDOR_TYPE_OTHER;

  GroupBox_SalaryType_ComboChoices =
    'N/A' + #9 + SALARY_TYPE_NA + #13 +
    'Hourly Rate' + #9 + SALARY_TYPE_Hourly_Rate + #13 +
    'Multiple Hourly Rate (Jobs)' + #9 + SALARY_TYPE_MHR_Jobs + #13 +
    'Multiple Hourly Rate (Shifts)' + #9 + SALARY_TYPE_MHR_Shifts + #13 +
    'Fixed Number of Hours (40 or fewer)' + #9 + SALARY_TYPE_Fixed_Number + #13 +
    'Salary for Varying Hours' + #9 + SALARY_TYPE_Salary_For_Varying + #13 +
    'Daily Rate' + #9 + SALARY_TYPE_Daily_Rate + #13 +
    'Piece Rate' + #9 + SALARY_TYPE_Piece_Rate + #13 +
    'Flat Rate' + #9 + SALARY_TYPE_Flate_Rate + #13 +
    'Other Non-Hourly Pay' + #9 + SALARY_TYPE_Other_Non_Hourly + #13 +
    'Exempt' + #9 + SALARY_TYPE_Exempt;

  UserStereoType_ComboChoices =
    'S/B internal' + #9 + STEREOTYPE_LOCAL + #13 +
    'Web' + #9 + STEREOTYPE_WEB + #13 +
    'Remote' + #9 + STEREOTYPE_REMOTE;


  GroupBox_Show_EIN_on_Check_ComboChoices =
    'No' + #9 + GROUP_BOX_NO + #13 +
    'State EIN' + #9 + GROUP_BOX_STATE_EIN + #13 +
    'Federal EIN' + #9 + GROUP_BOX_FEDERAL_EIN;

  GroupBox_ACA_Safe_Harbor_type_ComboChoices =
    'None' + #9 + GROUP_BOX_NO + #13 +
    'Federal Poverty' + #9 + GROUP_BOX_FEDERAL_POVERTY + #13 +
    'Rate of Pay' + #9 + GROUP_BOX_RATE_OF_PAY + #13 +
    'W-2 Wages' + #9 + GROUP_BOX_W2_WAGES;

  GroupBox_QUAL_EVENT_START_DATE_ComboChoices =
    'Enrollment Date' + #9 + QUAL_EVENT_Enrollment_Date + #13 +
    'Beginning of Next Month' + #9 + QUAL_EVENT_Beginning_of_Next_Month + #13 +
    'After Duration Ends' + #9 + QUAL_EVENT_After_Duration_Ends;

const
  FieldsToPopulate: array[1..871] of TableField =
  // Tablefield is a record defined in unit SDDClasses.pas
  // T: TableName, constant from unit SDataDictionary.pas
  // F: FieldName, string
  // C: String to populate an EvCombobox, build up as follows:
  //      'PresentationString'#9'OptionCodeConstant'#13
  //        'PresentationString' = string shown in EvComboBoxes for a particular
  //          choice
  //        'OptionCodeConstant' = (1-letter) code entered in database when the
  //          option is selected, as defined in unit EvConsts.pas
  // D: Default choice constant ((1-letter) code), as defined in
  //      unit EvConsts.pas
  // G: Grouping, ask Dima
  // S: Secured, starred    : boolean
    (
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'INC_TAX_PAYMENT_CODE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'CHANGE_FREQ_ON_THRESHOLD'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'FIRST_THRESHOLD_PERIOD'; C: TresholdPeriod_ComboChoices; D: TRESHOLD_NONE),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'SECOND_THRESHOLD_PERIOD'; C: TresholdPeriod_ComboChoices; D: TRESHOLD_NONE),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'PAY_AND_SHIFTBACK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'HIDE_FROM_USER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'PAY_AND_SHIFTBACK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'HIDE_FROM_USER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'CUSTOM_DEBIT_AFTER_STATUS'; C: TaxDepositStatus_ComboChoices; D: TAX_DEPOSIT_STATUS_PAID),
    (T: TSY_GLOBAL_AGENCY; F: 'CUSTOM_DEBIT_POST_TO_REGISTER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSB_BANKS; F: 'ALLOW_HYPHENS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'ELIGIBLE_FOR_REHIRE'; C: GroupBox_YesNoConditional_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'AUTO_UPDATE_RATES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_SCHEDULED_E_DS; F: 'CAP_STATE_TAX_CREDIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_CHECK; F: 'RECIPROCATE_SUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE_LOCALS; F: 'INCLUDE_IN_PRETAX'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE_LOCALS; F: 'WORK_ADDRESS_OVR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SHOW_WHICH_BALANCE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'BILLING_CHECK_CPA'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'PRINT_CLIENT_NAME'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'RECIPROCATE_STATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_CHILD_SUPPORT_CASES; F: 'IL_MED_INS_ELIGIBLE'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TEE_DIRECT_DEPOSIT; F: 'ALLOW_HYPHENS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK_LOCALS; F: 'REPORTABLE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'FREQUENCY_TYPE'; C: LocalDepFrequencyType_ComboChoices; ),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'WHEN_DUE_TYPE'; C: WhenDueType_ComboChoices; ),
    (T: TSY_LOCAL_DEPOSIT_FREQ; F: 'THRD_MNTH_DUE_END_OF_NEXT_MNTH'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TSY_FED_EXEMPTIONS; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_FEDERAL'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_FUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_EMPLOYEE_OASDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_EMPLOYEE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_EMPLOYEE_EIC'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_EMPLOYER_OASDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_EXEMPTIONS; F: 'EXEMPT_EMPLOYER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_FED_TAX_TABLE_BRACKETS; F: 'MARITAL_STATUS'; C: GroupBox_SingleMarried_ComboChoices; ),
    (T: TSY_LOCALS; F: 'LOCAL_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_LOCALS; F: 'LOCAL_TYPE'; C: LocalType_ComboChoices; ),
    (T: TSY_LOCALS; F: 'MINIMUM_HOURS_WORKED_PER'; C: MinimumHoursWorked_ComboChoices; D: FREQUENCY_TYPE_NONE2),
    (T: TSY_LOCALS; F: 'CALCULATION_METHOD'; C: LocalCalcMethod_ComboChoices; ),
    (T: TSY_LOCALS; F: 'TAX_DEPOSIT_FOLLOW_STATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_LOCALS; F: 'USE_MISC_TAX_RETURN_CODE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCALS; F: 'PAY_WITH_STATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCALS; F: 'PRINT_RETURN_IF_ZERO'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_LOCALS; F: 'ROUND_TO_NEAREST_DOLLAR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_LOCALS; F: 'TAX_TYPE'; C: GroupBox_LocalEEorER_ComboChoices; D: GROUP_BOX_EE),
    (T: TSY_LOCALS; F: 'TAX_FREQUENCY'; C: LocalFrequency_ComboChoices; D: SCHED_FREQ_DAILY),
    (T: TSY_LOCALS; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TSY_LOCALS; F: 'COMBINE_FOR_TAX_PAYMENTS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'FREQUENCY_TYPE'; C: LocalDepFrequencyType_ComboChoices; ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'WHEN_DUE_TYPE'; C: WhenDueType_ComboChoices; ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'THRESHOLD_PERIOD'; C: TresholdPeriod_ComboChoices; ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'SECOND_THRESHOLD_PERIOD'; C: TresholdPeriod_ComboChoices; ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'THRD_MNTH_DUE_END_OF_NEXT_MNTH'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'CHANGE_STATUS_ON_THRESHOLD'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO ),
    (T: TSY_STATE_DEPOSIT_FREQ; F: 'INC_TAX_PAYMENT_CODE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_SUI; F: 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'; C: GroupBox_EEorER_ComboChoices; ),
    (T: TSY_SUI; F: 'USE_MISC_TAX_RETURN_CODE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_SUI; F: 'ROUND_TO_NEAREST_DOLLAR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_SUI; F: 'SUI_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_SUI; F: 'FREQUENCY_TYPE'; C: SySuiFreq_CompChoices; D: SY_SUI_FREQ_QUARTERLY),
    (T: TSY_SUI; F: 'PAY_WITH_STATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_SUI; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TSY_STATES; F: 'INC_SUI_TAX_PAYMENT_CODE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATES; F: 'RECIPROCITY_TYPE'; C: Sy_StateReciprocal_ComboChoices; D: SYSTEM_STATE_RECIPROCAL_TYPE_DO_NOT_REPORT),
    (T: TSY_STATES; F: 'PRINT_W2'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_STATES; F: 'CAP_STATE_TAX_CREDIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATES; F: 'SUI_EFT_TYPE'; C: SY_TaxPaymentMethod_ComboChoices; ),
    (T: TSY_STATES; F: 'STATE_EFT_TYPE'; C: SY_TaxPaymentMethod_ComboChoices; ),
    (T: TSY_STATES; F: 'SDI_RECIPROCATE'; C: ReciprocalType_ComboChoices; ),
    (T: TSY_STATES; F: 'SUI_RECIPROCATE'; C: ReciprocalType_ComboChoices; ),
    (T: TSY_STATES; F: 'STATE_WITHHOLDING'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'PRINT_STATE_RETURN_IF_ZERO'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'USE_STATE_MISC_TAX_RETURN_CODE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'UIFSA_NEW_HIRE_REPORTING'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'TAX_DEPOSIT_FOLLOW_FEDERAL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_STATES; F: 'PRINT_SUI_RETURN_IF_ZERO'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'SHOW_S125_IN_GROSS_WAGES_SUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'PAY_SDI_WITH'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATES; F: 'ROUND_TO_NEAREST_DOLLAR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSY_STATES; F: 'INACTIVATE_MARITAL_STATUS';C: GroupBox_YesNo_ComboChoices; D: 'N'),

    (T: TSY_STATE_EXEMPTIONS; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TSY_STATE_EXEMPTIONS; F: 'EXEMPT_STATE'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TSY_STATE_EXEMPTIONS; F: 'EXEMPT_EMPLOYER_SDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_EXEMPTIONS; F: 'EXEMPT_EMPLOYEE_SDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_EXEMPTIONS; F: 'EXEMPT_EMPLOYEE_SUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_EXEMPTIONS; F: 'EXEMPT_EMPLOYER_SUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_TAX_CHART; F: 'ENTRY_TYPE'; C: StateTaxChartType_ComboChoices; D: STATE_TAX_CHART_TYPE_STANDARD),
    (T: TSY_STATE_MARITAL_STATUS; F: 'DEDUCT_FICA'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_MARITAL_STATUS; F: 'DEDUCT_FEDERAL'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSY_STATE_MARITAL_STATUS; F: 'ACTIVE_STATUS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_LOCAL_EXEMPTIONS; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TSY_LOCAL_EXEMPTIONS; F: 'EXEMPT'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TSY_GLOBAL_AGENCY; F: 'RECEIVING_ACCOUNT_TYPE'; C: ReceivingAcctType_ComboChoices; D: RECEIVING_ACCT_TYPE_CHECKING),
    (T: TSY_GLOBAL_AGENCY; F: 'ACCEPTS_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_GLOBAL_AGENCY; F: 'ACCEPTS_DEBIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'ACCEPTS_CREDIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_GLOBAL_AGENCY; F: 'NEGATIVE_DIRECT_DEP_ALLOWED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'MAG_MEDIA'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_GLOBAL_AGENCY; F: 'IGNORE_HOLIDAYS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'SEND_ZERO_EFT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'SEND_ZERO_COUPON'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'PRINT_ZERO_RETURN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_GLOBAL_AGENCY; F: 'CHECK_ONE_PAYMENT_TYPE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'EFTP_ONE_PAYMENT_TYPE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'ACH_ONE_PAYMENT_TYPE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_GLOBAL_AGENCY; F: 'AGENCY_TYPE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSY_AGENCY_DEPOSIT_FREQ; F: 'FREQUENCY_TYPE'; C: TCDTaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_MONTHLY),
    (T: TSY_AGENCY_DEPOSIT_FREQ; F: 'WHEN_DUE_TYPE'; C: WhenDueType_ComboChoices; ),

    (T: TSY_GL_AGENCY_FIELD_OFFICE; F: 'CATEGORY_TYPE'; C: Category_Type_ComboChoices; D: CATEGORY_TYPE_WH),

    (T: TSY_GL_AGENCY_HOLIDAYS; F: 'CALC_TYPE'; C: AgencyHolidayRules_ComboBoxChoices; D: HOLIDAY_RULE_AFTER),

    (T: TSY_HR_REFUSAL_REASON; F: 'HEALTHCARE_COVERAGE'; C: Healthcare_Coverage_ComboChoices; D: HEALTHCARE_NA),

    (T: TSY_REPORTS; F: 'ACTIVE_REPORT'; C: ReportPriorities_ComboChoices; ),

    (T: TSY_REPORTS_GROUP; F: 'AGENCY_COPY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_REPORTS_GROUP; F: 'SB_COPY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_REPORTS_GROUP; F: 'CL_COPY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSY_REPORTS_GROUP; F: 'MEDIA_TYPE'; C: ReportResultType_ComboChoices; D: REPORT_RESULT_TYPE_PAPER),

    (T: TSY_REPORT_WRITER_REPORTS; F: 'REPORT_TYPE'; C: SystemReportType_ComboChoices),
    (T: TSY_REPORT_WRITER_REPORTS; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; D: MEDIA_TYPE_PLAIN_LETTER),
    (T: TSB_REPORT_WRITER_REPORTS; F: 'REPORT_TYPE'; C: SystemReportType_ComboChoices; ),
    (T: TSB_REPORT_WRITER_REPORTS; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; D: MEDIA_TYPE_PLAIN_LETTER),
    (T: TCL_REPORT_WRITER_REPORTS; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; D: MEDIA_TYPE_PLAIN_LETTER),
    (T: TSB_PAPER_INFO; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; D: MEDIA_TYPE_PLAIN_LETTER),

    (T: TSY_GL_AGENCY_REPORT; F: 'SYSTEM_TAX_TYPE'; C: TaxReturnType_ComboChoices; ),
    (T: TSY_GL_AGENCY_REPORT; F: 'DEPOSIT_FREQUENCY'; C: ReturnLocalDepFrequencyType_ComboChoices; D:FREQUENCY_TYPE_DONT_CARE ),
    (T: TSY_GL_AGENCY_REPORT; F: 'RETURN_FREQUENCY'; C: ReturnFrequencyType_ComboChoices; ),
    (T: TSY_GL_AGENCY_REPORT; F: 'WHEN_DUE_TYPE'; C: ReturnWhenDueType_ComboChoices; ),
    (T: TSY_GL_AGENCY_REPORT; F: 'ENLIST_AUTOMATICALLY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TSY_GL_AGENCY_REPORT; F: 'PAYMENT_METHOD'; C: ReturnAgPaymentMethod_ComboChoices; D: AG_PAYMENT_METHOD_DONT_CARE),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAX945_CHECKS'; C: TAX945TaxCheckFilter_ComboChoices; D: TAX945_CHECKS_DONT_CARE),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAX943_COMPANY'; C: GroupBox_YesNoDontcare_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAX944_COMPANY'; C: GroupBox_YesNoDontcare_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAXRETURN940'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSY_GL_AGENCY_REPORT; F: 'PRINT_WHEN'; C: ReturnPrintWhen_ComboChoices; D: PRINT_WHEN_ALWAYS),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAX_SERVICE_FILTER'; C: ReturnPrintCoTaxServiceFilter_ComboChoices; D: CO_TAX_SERVICE_FILTER_ALL),
    (T: TSY_GL_AGENCY_REPORT; F: 'CONSOLIDATED_FILTER'; C: ReturnConsolidatedFilter_ComboChoices; D: CONSOLIDATED_FILTER_ALWAYS),
    (T: TSY_GL_AGENCY_REPORT; F: 'TAX_RETURN_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TSY_QUEUE_PRIORITY; F: 'THREADS'; D: '0'),
    (T: TSB_QUEUE_PRIORITY; F: 'THREADS'; D: '0'),

    (T: TSB_ENLIST_GROUPS; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; ),
    (T: TSB_ENLIST_GROUPS; F: 'PROCESS_TYPE';  C: GroupBox_Enlist_ProcessType_ComboChoices;D: PROCESS_TYPE_DONT_ENLIST),
    (T: TCO_ENLIST_GROUPS; F: 'MEDIA_TYPE'; C: MediaType_ComboChoices; ),
    (T: TCO_ENLIST_GROUPS; F: 'PROCESS_TYPE'; C: GroupBox_Enlist_ProcessType_ComboChoices; D: PROCESS_TYPE_DONT_ENLIST),

    (T: TSB_AGENCY; F: 'AGENCY_TYPE'; C: AgencyType_ComboChoices; ),
    (T: TSB_AGENCY; F: 'ACCOUNT_TYPE'; C: GroupBox_AccountType_ComboChoices; D: GROUP_BOX_CHECKING),
    (T: TSB_AGENCY; F: 'NEGATIVE_DIRECT_DEP_ALLOWED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSB_BANKS; F: 'USE_CHECK_TEMPLATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANKS; F: 'ADDENDA'; D: 'N'),
    (T: TSB_BANKS; F: 'CHECK_TEMPLATE'; D: '1'),
    (T: TSB_BANK_ACCOUNTS; F: 'SUPPRESS_OFFSET_ACCOUNT'; C: SB_Bank_Account_Suppress_Offset_Choices; D: SB_BANK_SUPPRESS_OFFSET_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'BLOCK_NEGATIVE_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'OPERATING_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'BILLING_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'ACH_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'TRUST_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'TAX_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'OBC_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'WORKERS_COMP_ACCOUNT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'BANK_ACCOUNT_TYPE'; C: GroupBox_AccountType_ComboChoices; D: GROUP_BOX_CHECKING),
    (T: TSB_BANK_ACCOUNTS; F: 'BANK_RETURNS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_BANK_ACCOUNTS; F: 'BANK_CHECK'; C: Bank_acc_OBC_Bank_ComboChoices; D: OBC_NA),

    (T: TSB_DELIVERY_COMPANY; F: 'DELIVERY_CONTACT_PHONE_TYPE'; C: PhoneType_ComboChoices; D:PHONE_TYPE_PHONE),
    (T: TSB_DELIVERY_COMPANY; F: 'SUPPLIES_CONTACT_PHONE_TYPE'; C: PhoneType_ComboChoices; D:PHONE_TYPE_PHONE),
    (T: TSB_HOLIDAYS; F: 'USED_BY'; C: GroupBox_UsedBy_ComboChoices; ),
    (T: TSB_SERVICES; F: 'SERVICE_TYPE'; C: SBServicesType_ComboChoices; ),
    (T: TSB_SERVICES; F: 'FREQUENCY'; C: ScheduledFrequency_Billing_ComboChoices; D: SCHED_FREQ_DAILY),
    (T: TSB_SERVICES; F: 'WEEK_NUMBER'; C: WeekNumberToPrint_ComboChoices; D:' '), //bogus value
    (T: TSB_SERVICES; F: 'BASED_ON_TYPE'; C: SBServicesBasedOn_ComboChoices; ),
    (T: TSB_SERVICES; F: 'COMMISSION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSB_SERVICES; F: 'SALES_TAXABLE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_SERVICES; F: 'TAX_TYPE'; C: SBServicesTaxType_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TSB_SERVICES; F: 'PARTNER_BILLING'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSB_USER_MESSAGES; F: 'USER_STEREOTYPE'; C: UserStereoType_ComboChoices; D: STEREOTYPE_LOCAL),
    (T: TSB; F: 'EFTPS_BANK_FORMAT'; C: GroupBox_BankFormat_ComboChoices; D:GROUP_BOX_BATCH_PROVIDER ),
    (T: TSB; F: 'USE_PRENOTE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSB; F: 'IMPOUND_TRUST_MONIES_AS_RECEIV'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSB; F: 'PAY_TAX_FROM_PAYABLES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB; F: 'MICR_FONT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB; F: 'DEFAULT_CHECK_FORMAT'; C: CheckForm_ComboChoices; D: CHECK_FORM_REGULAR),
    (T: TSB; F: 'AR_EXPORT_FORMAT'; C: OBC_Bank_ComboChoices; D: OBC_WELLS_FARGO_NONE),
    (T: TSB; F: 'ERROR_SCREEN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TSB; F: 'PSWD_FORCE_MIXED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB; F: 'EE_LOGIN_TYPE'; C: EELoginType_ComboChoices; D: LOGIN_TYPE_DEFAULT),

    (T: TSB; F: 'MISC_CHECK_FORM';  C: CheckForm_ComboChoices; D: CHECK_FORM_REGULAR),

    (T: TSB; F: 'SB_ACH_FILE_LIMITATIONS'; C: SbAchFileLimitations_ComboChoises; D: SB_ACH_FILE_LIMITATIONS_NONE),
    (T: TSB; F: 'SB_EXCEPTION_PAYMENT_TYPE'; C: SbExceptionPaymentType_ComboChoises; D: PAYMENT_TYPE_NONE),
    (T: TSB; F: 'ANALYTICS_LICENSE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB; F: 'ENFORCE_EE_DOB_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSB_ACA_GROUP_ADD_MEMBERS; F: 'ALE_MAIN_COMPANY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_E_D_CODES; F: 'SHOW_IN_ESS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_E_D_CODES; F: 'ACA'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_GROUP_MANAGER; F: 'SECONDARY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_TIME_OFF_ACCRUAL; F: 'SHOW_E_DS_IN_ESS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'USE_BALANCE_CAP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_ADDITIONAL_INFO_NAMES; F: 'SHOW_IN_EVO_PAYROLL'; C: GroupBox_Show_In_Payroll_ComboChoices; D: SHOW_IN_PAYROLL_NO),

    (T: TCO_DASHBOARDS; F: 'DASHBOARD_LEVEL'; C: GroupBox_Level_ComboChoices; D: LEVEL_SYSTEM),
    (T: TCO; F: 'MOBILE_APP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'INTERNAL_BENCHMARKING'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_BENEFITS; F: 'ACA_BENEFIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),


    (T: TCO_ACA_CERT_ELIGIBILITY; F: 'ACA_CERT_ELIGIBILITY_TYPE'; C: GroupBox_Aca_Cert_eligibility_Type_ComboChoices; D: ACA_CERT_ELIGIBILITY_TYPE_A),

    (T: TSB_USER; F: 'SECURITY_LEVEL'; C: User_Security_Level_ComboChoices; G: USER_SEC_LEVEL_NONE; D: USER_SEC_LEVEL_USER),
    (T: TSB_USER; F: 'DEPARTMENT'; C: User_Department_ComboChoices; ),
    (T: TSB_USER; F: 'ACTIVE_USER'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TSB_USER; F: 'HR_PERSONNEL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO ),
    (T: TSB_USER; F: 'ANALYTICS_PERSONNEL'; C: GroupBox_Analytics_Personel_ComboChoices; D: USER_ANALYTICS_PERSONNEL_NA ),
    (T: TSB_USER; F: 'EVOLUTION_PRODUCT'; C: GroupBox_EvoPayroll_No_ComboChoices; D: GROUP_BOX_NO),


    (T: TSB_ACCOUNTANT; F: 'BANK_ACCOUNT_TYPE'; C: GroupBox_AccountType_ComboChoices; D: GROUP_BOX_CHECKING ),

    (T: TSB_TEAM; F: 'CR_CATEGORY'; C: CR_Category_ComboChoices; D: CR_CATEGORY_NONE),
    (T: TSB_MAIL_BOX; F: 'AUTO_RELEASE_TYPE'; C: MailBoxAutoRelease_ComboChoices; D: MAIL_BOX_AUTO_RELEASE_IMMED),
    (T: TSB_MAIL_BOX; F: 'REQUIRED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TCO_GENERAL_LEDGER; F: 'LEVEL_TYPE'; C: GL_Level_ComboChoices; D: GL_LEVEL_NONE),
    (T: TCO_GENERAL_LEDGER; F: 'DATA_TYPE'; C: GL_Data_ComboChoices; D: GL_DATA_NONE),

    (T: TCL; F: 'TERMINATION_CODE'; C: Termination_ComboChoices; D: TERMINATION_ACTIVE),
    (T: TCL; F: 'PRINT_CPA'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'LEASING_COMPANY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'RECIPROCATE_SUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL; F: 'SECURITY_FONT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL; F: 'ENABLE_HR'; C: EnableClientFeatures_ComboChoices; D: CLIENT_FEATURE_NO),
    (T: TCL; F: 'MAINTENANCE_HOLD'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'READ_ONLY'; C: GroupBox_FullandRemoteReadOnly_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL; F: 'BLOCK_INVALID_SSN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCL_MAIL_BOX_GROUP; F: 'AUTO_RELEASE_TYPE'; C: MailBoxAutoRelease_ComboChoices; D: MAIL_BOX_AUTO_RELEASE_IMMED),
    (T: TCL_MAIL_BOX_GROUP; F: 'REQUIRED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TCL_AGENCY; F: 'REPORT_METHOD'; C: ReportMethod_ComboChoices; D: REPORT_METHOD_PAPER),
    (T: TCL_AGENCY; F: 'FREQUENCY'; C: AgencyFrequency_ComboChoices; ),
    (T: TCL_AGENCY; F: 'PAYMENT_TYPE'; C: PaymentType_ComboChoices; D: Payment_Type_Check),
    (T: TCL_BANK_ACCOUNT; F: 'BANK_ACCOUNT_TYPE'; C: GroupBox_AccountType_ComboChoices; D: GROUP_BOX_CHECKING),
    (T: TCL_BILLING; F: 'INVOICE_SEPARATELY'; C: GroupBox_Invoice_ComboChoices; D: GROUP_BOX_INVOICE_W_PAYROLL ),
    (T: TCL_BILLING; F: 'PAYMENT_METHOD'; C: BillingPayment_ComboChoices; D: BILLING_PAYMENT_METHOD_NA ),
    (T: TCL_COMMON_PAYMASTER; F: 'COMBINE_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_COMMON_PAYMASTER; F: 'COMBINE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_COMMON_PAYMASTER; F: 'COMBINE_FUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_COMMON_PAYMASTER; F: 'COMBINE_STATE_SUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_DELIVERY_GROUP; F: 'STUFF'; C: GroupBox_YesNo_ComboChoices; ),

    (T: TCL_CO_CONSOLIDATION; F: 'CONSOLIDATION_TYPE'; C: Co_Consolidation_Type_ComboChoices; ),
    (T: TCL_CO_CONSOLIDATION; F: 'SCOPE'; C: Co_Consolidation_Scope_ComboChoices; D: CONSOLIDATION_SCOPE_ALL),

    (T: TCL_PENSION; F: 'PENSION_TYPE'; C: PensionType_ComboChoices; ),
    (T: TCL_PENSION; F: 'STOP_WHEN_EE_REACHES_MAX'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_PENSION; F: 'MARK_PENSION_ON_W2'; C: GroupBox_PensionW2_ComboChoices; D: GROUP_BOX_SYSTEM),
    (T: TCL_E_DS; F: 'OVERSTATE_HOURS_FOR_OVERTIME'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_E_DS; F: 'E_D_CODE_TYPE'; C: ED_ComboBoxChoices; G: ED_ComboBoxGroupings; ),
    (T: TCL_E_DS; F: 'OVERRIDE_FED_TAX_TYPE'; C: GroupBox_Type_ComboChoices; D:GROUP_BOX_NONE ),
    (T: TCL_E_DS; F: 'SD_FREQUENCY'; C: ScheduledEDFrequency_ComboChoices; D: SCHED_FREQ_DAILY),
    (T: TCL_E_DS; F: 'SD_CALCULATION_METHOD'; C: ScheduledCalcMethod_ComboBoxChoices; D: CALC_METHOD_FIXED),
    (T: TCL_E_DS; F: 'PW_MIN_WAGE_MAKE_UP_METHOD'; C: PW_MinWageMakeUpMethod_ComboChoices; D: PW_MAKEUP_METHOD_NONE),
    (T: TCL_E_DS; F: 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'; C: TipMinWageMakeUp_ComboChoices; D: TIP_MINWAGE_NONE),
    (T: TCL_E_DS; F: 'SKIP_HOURS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_E_DS; F: 'UPDATE_HOURS'; C: GroupBox_ShowEDHours_ComboChoices; D: SHOW_ED_HOURS_ON_BOTH),
    (T: TCL_E_DS; F: 'MAKE_UP_DEDUCTION_SHORTFALL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SHOW_YTD_ON_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_E_DS; F: 'SD_EXCLUDE_WEEK_1'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_EXCLUDE_WEEK_2'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_EXCLUDE_WEEK_3'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_EXCLUDE_WEEK_4'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_EXCLUDE_WEEK_5'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_AUTO'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SCHEDULED_DEFAULTS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_E_DS; F: 'SD_ROUND'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'EE_EXEMPT_EXCLUDE_FEDERAL'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'EE_EXEMPT_EXCLUDE_OASDI'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'EE_EXEMPT_EXCLUDE_MEDICARE'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'EE_EXEMPT_EXCLUDE_EIC'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'ER_EXEMPT_EXCLUDE_OASDI'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'ER_EXEMPT_EXCLUDE_MEDICARE'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'ER_EXEMPT_EXCLUDE_FUI'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_DS; F: 'OVERRIDE_RATE_TYPE'; C: OverrideValueType_ComboChoices; D: OVERRIDE_VALUE_TYPE_NONE),
    (T: TCL_E_DS; F: 'SHOW_ON_INPUT_WORKSHEET'; C: GroupBox_ED_PrintOnWorksheet_ComboChoices; D: ED_PRINT_ON_WORKSHEET_OnlyOnBottom),
    (T: TCL_E_DS; F: 'SHOW_ED_ON_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_E_DS; F: 'APPLY_BEFORE_TAXES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCL_E_DS; F: 'WHICH_CHECKS'; D: 'A'),
    (T: TCL_E_DS; F: 'MONTH_NUMBER'; D: 'N'),
    (T: TCL_E_DS; F: 'SD_PLAN_TYPE'; C: MonthNumber_ComboChoices; D: MONTH_NUMBER_NONE),
    (T: TCL_E_DS; F: 'SD_WHICH_CHECKS'; C: GroupBox_WichChecks_ComboChoices; D: GROUP_BOX_ALL),
    (T: TCL_E_DS; F: 'SD_ALWAYS_PAY'; C: AlwaysPay_ComboChoices; D: ALWAYS_PAY_NO),
    (T: TCL_E_DS; F: 'SD_DEDUCTIONS_TO_ZERO';  C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'SD_USE_PENSION_LIMIT'; D: 'N'),
    (T: TCL_E_DS; F: 'SD_DEDUCT_WHOLE_CHECK';  C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_E_DS; F: 'ROUND_OT_CALCULATION';  C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    // Default NO for 'Show Four Decimals On Checks' field. Reso#34953. Andrew
    (T: TCL_E_DS; F: 'FILLER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCL_E_D_GROUPS; F: 'SUBTRACT_DEDUCTIONS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCL_E_D_LOCAL_EXMPT_EXCLD; F: 'EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'; C: GroupBox_ExmptExde_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYEE_EXEMPT_EXCLUDE_SDI'; C: GroupBox_ExmptExde_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYEE_EXEMPT_EXCLUDE_SUI'; C: GroupBox_ExmptExde_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYER_EXEMPT_EXCLUDE_SDI'; C: GroupBox_ExmptExde_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYER_EXEMPT_EXCLUDE_SUI'; C: GroupBox_ExmptExde_ComboChoices; ),
    (T: TCL_E_D_STATE_EXMPT_EXCLD; F: 'EMPLOYEE_STATE_OR_TYPE'; C: GroupBox_Type_ComboChoices; D: GROUP_BOX_AMOUNT),
    (T: TCL_PERSON; F: 'ETHNICITY'; C: Ethnicity_ComboChoices; D: ETHNIC_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'EIN_OR_SOCIAL_SECURITY_NUMBER'; C: GroupBox_EINorSSN_ComboChoices; D: GROUP_BOX_SSN),
    (T: TCL_PERSON; F: 'GENDER'; C: GroupBox_Gender_ComboChoices; D: GROUP_BOX_UNKOWN),
    (T: TCL_PERSON; F: 'RELIABLE_CAR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_PERSON; F: 'VETERAN'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'VIETNAM_VETERAN'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'DISABLED_VETERAN'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'MILITARY_RESERVE'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'SMOKER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_PERSON; F: 'I9_ON_FILE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_PERSON; F: 'VISA_TYPE'; C: VISAType_ComboChoices; D: VISA_TYPE_NONE),
    (T: TCL_PERSON; F: 'WEB_PASSWORD'; S: True),
    (T: TCL_PERSON_DEPENDENTS; F: 'GENDER'; C: GroupBox_Gender_ComboChoices; ),
    (T: TCL_PERSON_DEPENDENTS; F: 'RELATION_TYPE'; C: Relationship_Beneficiary_ComboChoices; ),
    (T: TCL_PERSON_DEPENDENTS; F: 'PERSON_TYPE'; C: Person_type_ComboChoices; D: PERSON_TYPE_DEPENDENT),
    (T: TCL_PERSON_DEPENDENTS; F: 'EXISTING_PATIENT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_PERSON_DEPENDENTS; F: 'FULL_TIME_STUDENT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'SHORT_TERM'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'LONG_TERM'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'NON_TAX_IN_PAYROLL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'STATE_TAX_IN_PAYROLL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'LOCAL_TAX_IN_PAYROLL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_3_PARTY_SICK_PAY_ADMIN; F: 'W2'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_TIMECLOCK_IMPORTS; F: 'CONNECTION_TYPE'; C: GroupBox_ConnectionType_ComboChoices; ),
    (T: TCL_TIMECLOCK_IMPORTS; F: 'SB_SUPPORTED'; C: GroupBox_YesNo_ComboChoices; ),

    (T: TCL_TIMECLOCK_IMPORTS; F: 'TIMECLOCK_TYPE'; C: Time_Clock_Import_Type_comboChoices; D: IMPORT_TYPE_NONE),
    (T: TCL_TIMECLOCK_IMPORTS; F: 'HARDWARE_TYPE'; C: Time_Clock_Hardware_Type_ComboChoices; D: IMPORT_HARDWARE_TYPE_NONE),
    (T: TCL_TIMECLOCK_IMPORTS; F: 'PAYROLL_IMPORT_FORMAT'; C: Import_Format_ComboChoices; D: IMPORT_FORMAT_NONE),
    (T: TCL_TIMECLOCK_IMPORTS; F: 'PAYROLL_EXPORT_FORMAT'; C: Export_Format_ComboChoices; D: EXPORT_FORMAT_NONE),

    (T: TCO; F: 'CO_PAYROLL_PROCESS_LIMITATIONS'; C: CoPayrollProcessLimitations_ComboChoises; D: CO_PAYROLL_PROCESS_LIMITATIONS_NONE),
    (T: TCO; F: 'CO_ACH_PROCESS_LIMITATIONS'; C: CoAchProcessLimitations_ComboChoises; D: CO_ACH_PROCESS_LIMITATIONS_NONE),
    (T: TCO; F: 'CO_EXCEPTION_PAYMENT_TYPE'; C: CoExceptionPaymentType_ComboChoises; D: PAYMENT_TYPE_NONE),

    (T: TCO; F: 'EE_PRINT_VOUCHER_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'ENABLE_ANALYTICS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ANALYTICS_LICENSE'; C: GroupBox_Analytics_License_ComboChoices; D: ANALYTICS_LICENSE_NA),
    (T: TCO; F: 'BENEFITS_ELIGIBLE_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'PAYROLL_ENTRY_MODE'; C: GroupBox_Entry_Mode_ComboChoices; D: ENTRY_MODE_EXPERT),
    (T: TCO; F: 'PAYROLL_PRODUCT'; C: GroupBox_PayrollProduct_ComboChoices; D: PAYROLL_PRODUCT_SB_BATCH_ENTRY),

    (T: TCO; F: 'ACA_FORM_DEFAULT'; C: ACA_TYPE_ComboChoices; D: EE_ACA_TYPE_NONE),
    (T: TCO; F: 'ACA_SELF_INSURED'; C: GroupBox_YesNoBoth_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ADVANCED_HR_CORE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ADVANCED_HR_ATS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ADVANCED_HR_TALENT_MGMT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ADVANCED_HR_ONLINE_ENROLLMENT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO; F: 'CALCULATE_STATES_FIRST'; C: Calc_State_First_ComboChoices; D: CALC_FED_STATE_SUI),
    (T: TCO; F: 'EXCLUDE_R_C_B_0R_N'; C: GroupBox_Exclude_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'AUTO_REDUCTION_DEFAULT_EES'; C: GroupBox_HourlySalariedBoth_ComboChoices; D: GROUP_BOX_BOTH2),
    (T: TCO; F: 'SUMMARIZE_SUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SHOW_SHORTFALL_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'BREAK_CHECKS_BY_DBDT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'NAME_ON_INVOICE'; C: CoNameOnInvoice_ComboChoices; D: NAME_ON_INVOICE_BUREAU),
    (T: TCO; F: 'AUTO_ENLIST'; C: ReturnAutoEnlist_ComboChoices; D: ReturnAutoEnlist_All),
    (T: TCO; F: 'CALCULATE_LOCALS_FIRST'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'WEEKEND_ACTION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'ALE_TRANSITION_RELIEF'; C: GroupBox_ALE_Transition_ComboChoices; D: GROUP_BOX_ALE_TRANSITION_NONE),
    (T: TCO; F: 'TERMINATION_CODE'; C: Termination_ComboChoices; D: TERMINATION_ACTIVE),
    (T: TCO; F: 'FEDERAL_TAX_DEPOSIT_FREQUENCY'; C: FederalTaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_SEMI_WEEKLY),
    (T: TCO; F: 'FED_943_TAX_DEPOSIT_FREQUENCY'; C: FederalTaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_SEMI_WEEKLY),
    (T: TCO; F: 'FED_945_TAX_DEPOSIT_FREQUENCY'; C: FederalTaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_SEMI_WEEKLY),
    (T: TCO; F: 'FEDERAL_TAX_TRANSFER_METHOD'; C: Div_thru_team_Level_ComboChoices; D: Div_thru_team_Level_ComboChoices_None),
    (T: TCO; F: 'FEDERAL_TAX_PAYMENT_METHOD'; C: TaxPaymentMethod_ComboChoices; D: TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT),
    (T: TCO; F: 'FUI_TAX_DEPOSIT_FREQUENCY'; C: FUITaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_QUARTERLY),
    (T: TCO; F: 'CO_CHECK_PRIMARY_SORT'; C: SortField_ComboChoices; ),
    (T: TCO; F: 'CO_CHECK_SECONDARY_SORT'; C: Co_thru_team_Level_ComboChoices; D: CLIENT_LEVEL_COMPANY),
    (T: TCO; F: 'CHECK_TYPE'; C: TargetsOnCheck_ComboChoices; D: TARGETS_ALWAYS_PRINT),
    (T: TCO; F: 'BUSINESS_TYPE'; C: BusinesType_ComboChoices; D: BUSINESS_TYPE_NONE),
    (T: TCO; F: 'EMPLOYER_TYPE'; C: EmployerType_ComboChoices; D: EMPLOYER_TYPE_NONE ),
    (T: TCO; F: 'CORPORATION_TYPE'; C: CorporationType_ComboChoices; ),
    (T: TCO; F: 'BANK_ACCOUNT_LEVEL'; C: Co_thru_team_Level_ComboChoices; ),
    (T: TCO; F: 'DBDT_LEVEL'; C: Co_thru_team_Level_ComboChoices; ),
    (T: TCO; F: 'BILLING_LEVEL'; C: Co_thru_team_Level_ComboChoices; D: CLIENT_LEVEL_COMPANY),
    (T: TCO; F: 'PAY_FREQUENCY_HOURLY_DEFAULT'; C: PayFrequencyType_ComboChoices; D: FREQUENCY_TYPE_WEEKLY ), //was FrequencyType_ComboChoices
    (T: TCO; F: 'PAY_FREQUENCY_SALARY_DEFAULT'; C: PayFrequencyType_ComboChoices; D: FREQUENCY_TYPE_WEEKLY), //was FrequencyType_ComboChoices
    (T: TCO; F: 'AUTO_LABOR_DIST_LEVEL'; C: AutoLaborDist_ComboChoices; ),
    (T: TCO; F: 'SECONDARY_SORT_FIELD'; C: Co_Sort_Level_ComboChoices; ),
    (T: TCO; F: 'PRIMARY_SORT_FIELD'; C: SortField_ComboChoices; ),
    (T: TCO; F: 'PAY_FREQUENCIES'; C: PayFrequencies_ComboChoices; ),
    (T: TCO; F: 'NETWORK_ADMIN_PHONE_TYPE'; C: PhoneType_ComboChoices; D: PHONE_TYPE_NONE),
    (T: TCO; F: 'EFTPS_ENROLLMENT_STATUS'; C: EFTEnrollmentStatus_ComboChoices; D: EFT_ENROLLMENT_STATUS_NONE),
    (T: TCO; F: 'HARDWARE_O_S'; C: OS_ComboChoices; D: OS_NA),
    (T: TCO; F: 'NETWORK'; C: Network_ComboChoices; D: NETWORK_NONE),
    (T: TCO; F: 'MODEM_SPEED'; C: ModemSpeed_ComboChoices; D: MODEM_SPEED_NA),
    (T: TCO; F: 'MODEM_CONNECTION_TYPE'; C: ModemConnType_ComboChoices; D: MODEM_CONN_TYPE_REGULAR_DIRECT),
    (T: TCO; F: 'USE_DBA_ON_TAX_RETURN'; C: UseDBAOnTaxReturn_ComboChoices; D: USE_DBA_ON_TAX_RETURN_PRIMARY),
    (T: TCO; F: 'SETUP_COMPLETED'; C: GroupBox_SetupComplete_ComboChoices; ),
    (T: TCO; F: 'FEDERAL_TAX_EXEMPT_STATUS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'FED_TAX_EXEMPT_EE_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'FED_TAX_EXEMPT_EE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'FED_TAX_EXEMPT_ER_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'FED_TAX_EXEMPT_ER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'FUI_TAX_EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'APPLY_MISC_LIMIT_TO_1099'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'WITHHOLDING_DEFAULT'; C: GroupBox_SingleMarried_ComboChoices; ),
    (T: TCO; F: 'SHOW_SHIFTS_ON_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SHOW_YTD_ON_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'SHOW_RATE_BY_HOURS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SHOW_EIN_NUMBER_ON_CHECK'; C: GroupBox_Show_EIN_on_Check_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SHOW_TIP_CREDIT_ON_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SHOW_SS_NUMBER_ON_CHECK'; C: GroupBox_YesNoLast4_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'COLLATE_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'PRINT_MANUAL_CHECK_STUBS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SUCCESSOR_COMPANY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO ; F: 'NON_PROFIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'RESTAURANT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'PRINT_CPA'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'CREDIT_HOLD'; C: Credit_Hold_Level_ComboChoices; D: CREDIT_HOLD_LEVEL_NONE),
    (T: TCO; F: 'TAX_SERVICE'; C: Tax_Service_ComboChoices; ),
    (T: TCO; F: 'EXTERNAL_TAX_EXPORT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'TIME_OFF_ACCRUAL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'TRUST_SERVICE'; C: GroupBox_TrustService_ComboChoices; ),
    (T: TCO; F: 'OBC'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'REMOTE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'REMOTE_OF_CLIENT'; C: Print_Signature_ComboChoices; D: PRINT_SIGNATURE_ON_ALL_CHECKS),
    (T: TCO; F: 'EXTERNAL_NETWORK_ADMINISTRATOR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'MEDICAL_PLAN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'SMOKER_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'AUTOPAY_COMPANY'; C: AutopayCompany_ComboChoices; D: AutopayCompany_CallIn),
    (T: TCO; F: 'DEDUCTIONS_TO_ZERO'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'DISTRIBUTE_DEDUCTIONS_DEFAULT'; C: CO_Labor_Distribution_Options_Comnbochoices; D: CO_DISTRIBUTE_BOTH),
    (T: TCO; F: 'AUTO_LABOR_DIST_SHOW_DEDUCTS'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO; F: 'MAKE_UP_TAX_DEDUCT_SHORTFALLS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'CHARGE_COBRA_ADMIN_FEE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO; F: 'SHOW_RATES_ON_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'SHOW_DIR_DEP_NBR_ON_CHECKS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO; F: 'IMPOUND_WORKERS_COMP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'REVERSE_CHECK_PRINTING'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'CHECK_TIME_OFF_AVAIL'; C: CheckTOBalance_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'HOLD_RETURN_QUEUE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'PRORATE_FLAT_FEE_FOR_DBDT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TCO; F: 'SHOW_TIME_CLOCK_PUNCH'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'REPRINT_TO_BALANCE'; C: ReprintTOBalance_ComboChoices; D: GROUP_BOX_CURRENT),
    (T: TCO; F: 'FINAL_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'LAST_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO; F: 'THIRD_PARTY_IN_GROSS_PR_REPORT'; C: BlockACHImpound_ComboChoices; D: BLOCK_ACH_IMPOUND_NONE),
    (T: TCO; F: 'ENABLE_DD'; C: GroupBox_YesNoReadOnly_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO; F: 'ESS_OR_EP'; C:GroupBox_ESS_or_EP_ComboChoices; D:GROUP_BOX_ESS),
    (T: TCO; F: 'ANNUAL_FORM_TYPE'; C:W2Type_ComboChoices; D:W2_TYPE_FEDERAL),
    (T: TCO; F: 'ACA_AVG_HOURS_WORKED_PERIOD'; C:AcaAvgHoursWorked_ComboChoices; D:FREQUENCY_TYPE_WEEKLY),
    (T: TCO; F: 'ACA_DEFAULT_STATUS'; C:ACAStatus_ComboChoices; D:ACA_STATUS_NA),
    (T: TCO; F: 'ACA_EDUCATION_ORG'; C:GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO; F: 'ACA_USE_AVG_HOURS_WORKED'; C:GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO; F: 'ACA_SAFE_HARBOR_TYPE_DEFAULT'; C:GroupBox_ACA_Safe_Harbor_type_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO; F: 'ACA_HEALTH_CARE_START'; C:ACA_HEALTH_CARE_START_ComboChoices; D:ACA_HEALTH_CARE_START_NONE),
    (T: TCO; F: 'ACA_SERVICES'; C:GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO; F: 'ENFORCE_EE_DOB'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_SHIFTS; F: 'AUTO_CREATE_ON_NEW_HIRE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_TAX_RETURN_QUEUE; F: 'PRODUCE_ASCII_FILE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TCO_E_D_CODES; F: 'DISTRIBUTE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_JOBS; F: 'JOB_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_JOBS; F: 'CERTIFIED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO ),
    (T: TCO_JOBS; F: 'STATE_CERTIFIED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_BENEFITS; F: 'FREQUENCY'; C: ScheduledFrequency_ComboChoices; ),
    (T: TCO_BENEFITS; F: 'EMPLOYEE_TYPE'; C: EE_Type_ComboChoices; ),
    (T: TCO_BENEFITS; F: 'BENEFIT_TYPE'; C: GroupBox_BenefitType_ComboChoices; ),
    (T: TCO_BENEFITS; F: 'TAXABLE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_BENEFITS; F: 'ROUND'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_BENEFITS; F: 'ALLOW_EE_CONTRIBUTION'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'APPLY_DISCOUNT'; C: Apply_Discount_ComboChoices; D:APPLY_DISCOUNT_NONE),
    (T: TCO_BENEFITS; F: 'DISPLAY_COST_PERIOD'; C: Display_Cost_Period_ComboChoices; D:DISPLAY_COST_PERIOD_ANNUALLY),
    (T: TCO_BENEFITS; F: 'W2_FLAG'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO_BENEFITS; F: 'READ_ONLY'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'ALLOW_HSA'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'REQUIRES_DOB'; C: Requires_DOB_ComboChoices; D:REQUIRES_DOB_BOTH),
    (T: TCO_BENEFITS; F: 'REQUIRES_SSN'; C: Requires_DOB_ComboChoices; D:REQUIRES_DOB_BOTH),
    (T: TCO_BENEFITS; F: 'REQUIRES_PCP'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'SHOW_RATES'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO_BENEFITS; F: 'SHOW_DISCOUNT_RATING'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'QUAL_EVENT_ELIGIBLE'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO_BENEFITS; F: 'QUAL_EVENT_ENROLL_DURATION'; D:'30'),
    (T: TCO_BENEFITS; F: 'REQUIRES_BENEFICIARIES'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'CAFETERIA_PLAN'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_BENEFITS; F: 'EE_BENEFIT'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO_BENEFITS; F: 'QUAL_EVENT_START_DATE'; C: GroupBox_QUAL_EVENT_START_DATE_ComboChoices; D:QUAL_EVENT_Enrollment_Date),

    (T: TCO_BENEFIT_PACKAGE; F: 'CAFETERIA_FREQUENCY'; C: Cafeteria_Freq_ComboChoices; D:CAFETERIA_FREQ_ANNUAL),
    (T: TCO_BENEFIT_PACKAGE; F: 'PAYOUT_FLAG'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),

    (T: TCO_BENEFIT_DISCOUNT; F: 'AMOUNT_TYPE'; C: Amount_Type_ComboChoices;),
    (T: TCO_ADDITIONAL_INFO_NAMES; F: 'DATA_TYPE'; C: Data_Type_ComboChoices; D: DATA_TYPE_STRING),
    (T: TCO_ADDITIONAL_INFO_NAMES; F: 'CATEGORY'; C: GroupBox_Category_ComboChoices; D: GROUP_BOX_COMPANY),
    (T: TCO_ADDITIONAL_INFO_NAMES; F: 'REQUIRED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_BENEFIT_CATEGORY; F: 'CATEGORY_TYPE'; C: Benefit_Category_Type_ComboChoices;),
    (T: TCO_BENEFIT_CATEGORY; F: 'ENROLLMENT_FREQUENCY'; C: Enrollment_Frequency_ComboChoices;),
    (T: TCO_BENEFIT_CATEGORY; F: 'READ_ONLY'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),

    (T: TCO_LOCAL_TAX; F: 'LOCAL_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_LOCAL_TAX; F: 'SELF_ADJUST_TAX'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX; F: 'AUTOCREATE_ON_NEW_HIRE'; C: GroupBox_YesNoAsk_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_LOCAL_TAX; F: 'PAYMENT_METHOD'; C: TaxPaymentMethod_ComboChoices; ),
    (T: TCO_LOCAL_TAX; F: 'EFT_STATUS'; C: EFTEnrollmentStatus_ComboChoices; D: EFT_ENROLLMENT_STATUS_NONE),
    (T: TCO_REPORTS; F: 'REPORT_LEVEL'; C: Report_Level_ComboChoices; D: REPORT_LEVEL_SYSTEM),
    (T: TCO_REPORTS; F: 'FAVORITE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO) ,
    (T: TCO_SUI; F: 'PAYMENT_METHOD'; C: TaxPaymentMethod_ComboChoices; ),
    (T: TCO_SUI; F: 'FINAL_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SUI; F: 'SUI_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_SUI; F: 'APPLIED_FOR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SUI; F: 'SUI_REIMBURSER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SUI; F: 'ALTERNATE_WAGE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_TAX_DEPOSITS; F: 'STATUS'; C: TaxDepositStatus_ComboChoices; ),
    (T: TCO_TAX_DEPOSITS; F: 'DEPOSIT_TYPE'; C: TaxDepositType_ComboChoices; D:TAX_DEPOSIT_TYPE_MANUAL),
    (T: TCO_SERVICES; F: 'SALES_TAX'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SERVICES; F: 'DISCOUNT_TYPE'; C: GroupBox_Type_ComboChoices; D: GROUP_BOX_NONE),
    (T: TCO_SERVICES; F: 'FREQUENCY'; C: ScheduledFrequency_Billing_ComboChoices; D: SCHED_FREQ_DAILY),
    (T: TCO_SERVICES; F: 'WEEK_NUMBER'; C: WeekNumberToPrint_ComboChoices; ),
    (T: TCO_STATES; F: 'STATE_EFT_ENROLLMENT_STATUS'; C: EFTEnrollmentStatus_ComboChoices; D: EFT_ENROLLMENT_STATUS_NONE),
    (T: TCO_STATES; F: 'SUI_EFT_ENROLLMENT_STATUS'; C: EFTEnrollmentStatus_ComboChoices; D: EFT_ENROLLMENT_STATUS_NONE),
    (T: TCO_STATES; F: 'STATE_TAX_DEPOSIT_METHOD'; C: TaxPaymentMethod_ComboChoices; ),
    (T: TCO_STATES; F: 'SUI_TAX_DEPOSIT_METHOD'; C: TaxPaymentMethod_ComboChoices; ),
    (T: TCO_STATES; F: 'SUI_TAX_DEPOSIT_FREQUENCY'; C: SUITaxDepositFrequency_ComboChoices; D: FREQUENCY_TYPE_QUARTERLY),
    (T: TCO_STATES; F: 'USE_DBA_ON_TAX_RETURN'; C: UseDBAOnTaxReturn_ComboChoices; D: USE_DBA_ON_TAX_RETURN_PRIMARY),
    (T: TCO_STATES; F: 'FINAL_TAX_RETURN'; D: GROUP_BOX_NONE),
    (T: TCO_STATES; F: 'MO_TAX_CREDIT_ACTIVE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'APPLIED_FOR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'COMPANY_PAID_HEALTH_INSURANCE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_STATES; F: 'TCD_PAYMENT_METHOD'; C: TCDPaymentMethod_ComboChoices; D: TAX_PAYMENT_METHOD_CHECK),
    (T: TCO_DIVISION; F: 'PAY_FREQUENCY_HOURLY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_DIVISION; F: 'PAY_FREQUENCY_SALARY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_DIVISION; F: 'PRINT_DIV_ADDRESS_ON_CHECKS'; C: INCLUDE_REPT_OR_ADDRESS_ComboChoices; ),
    (T: TCO_DIVISION; F: 'HOME_STATE_TYPE'; C: HomeStateType_ComboChoices; ),
    (T: TCO_BRANCH; F: 'PAY_FREQUENCY_HOURLY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_BRANCH; F: 'PAY_FREQUENCY_SALARY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_BRANCH; F: 'PRINT_BRANCH_ADDRESS_ON_CHECK'; C: INCLUDE_REPT_OR_ADDRESS_ComboChoices; ),
    (T: TCO_BRANCH; F: 'HOME_STATE_TYPE'; C: HomeStateType_ComboChoices; ),
    (T: TCO_BRCH_PR_BATCH_DEFLT_ED; F: 'SALARY_HOURLY_OR_BOTH'; C: GroupBox_SalaryHourlyBoth_ComboChoices; ),
    (T: TCO_DEPT_PR_BATCH_DEFLT_ED; F: 'SALARY_HOURLY_OR_BOTH'; C: GroupBox_SalaryHourlyBoth_ComboChoices; ),
    (T: TCO_DIV_PR_BATCH_DEFLT_ED; F: 'SALARY_HOURLY_OR_BOTH'; C: GroupBox_SalaryHourlyBoth_ComboChoices; ),
    (T: TCO_TEAM_PR_BATCH_DEFLT_ED; F: 'SALARY_HOURLY_OR_BOTH'; C: GroupBox_SalaryHourlyBoth_ComboChoices; ),
    (T: TCO_MANUAL_ACH; F: 'STATUS'; C: CommonReportStatus_ComboChoices; ),

    (T: TCO_WORKERS_COMP; F: 'SUBTRACT_PREMIUM'; C: GroupBox_WCOvertimeToReport_ComboChoices; D: GROUP_BOX_YES),


    (T: TCO_PR_CHECK_TEMPLATES; F: 'TAX_AT_SUPPLEMENTAL_RATE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'PRORATE_SCHEDULED_E_DS'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_DD'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_DD_EXCEPT_NET'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_TIME_OFF_ACCURAL'; C: GroupBox_ExcludeTimeOff_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_AUTO_DISTRIBUTION'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_ALL_SCHED_E_D_CODES'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_FEDERAL'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_ADDITIONAL_FEDERAL'; C: ExcludeTaxes_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYEE_OASDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYEE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYEE_EIC'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYER_FUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYER_OASDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'EXCLUDE_EMPLOYER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_DEPARTMENT; F: 'PRINT_DEPT_ADDRESS_ON_CHECKS'; C: INCLUDE_REPT_OR_ADDRESS_ComboChoices; ),
    (T: TCO_PR_ACH; F: 'STATUS'; C: CommonReportStatus_ComboChoices; ),
    (T: TCO_BILLING_HISTORY; F: 'STATUS'; C: COBillingStatus_ComboChoices; ),
    (T: TCO_BILLING_HISTORY; F: 'EXPORTED_TO_A_R'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'OVERRIDE_FREQUENCY'; C: TaxFrequencyType_ComboChoices; ),
    (T: TCO_PR_CHECK_TEMPLATES; F: 'FEDERAL_OVERRIDE_TYPE'; C: OverrideValueType_ComboChoices; ),
    (T: TCO_FED_TAX_LIABILITIES; F: 'STATUS'; C: TaxDepositStatus_ComboChoices; ),
    (T: TCO_FED_TAX_LIABILITIES; F: 'PREV_STATUS'; C: TaxDepositStatus_ComboChoices; D: TAX_DEPOSIT_STATUS_DEFAULT_PREV_STATUS),
    (T: TCO_FED_TAX_LIABILITIES; F: 'ADJUSTMENT_TYPE'; C: FedTaxLiabilityAdjustmentType_ComboChoices; D: TAX_LIABILITY_ADJUSTMENT_TYPE_NONE),
    (T: TCO_FED_TAX_LIABILITIES; F: 'TAX_TYPE'; C: FedTaxLiabilityType_ComboChoices; ),
    (T: TCO_FED_TAX_LIABILITIES; F: 'THIRD_PARTY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_FED_TAX_LIABILITIES; F: 'IMPOUNDED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_LOCAL_TAX_LIABILITIES; F: 'STATUS'; C: TaxDepositStatus_ComboChoices; ),
    (T: TCO_LOCAL_TAX_LIABILITIES; F: 'PREV_STATUS'; C: TaxDepositStatus_ComboChoices; D: TAX_DEPOSIT_STATUS_DEFAULT_PREV_STATUS),
    (T: TCO_LOCAL_TAX_LIABILITIES; F: 'ADJUSTMENT_TYPE'; C: TaxLiabilityAdjustmentType_ComboChoices; D: TAX_LIABILITY_ADJUSTMENT_TYPE_NONE),
    (T: TCO_LOCAL_TAX_LIABILITIES; F: 'THIRD_PARTY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX_LIABILITIES; F: 'IMPOUNDED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX; F: 'EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX; F: 'FINAL_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX; F: 'APPLIED_FOR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'STATUS'; C: TaxDepositStatus_ComboChoices; ),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'PREV_STATUS'; C: TaxDepositStatus_ComboChoices; D: TAX_DEPOSIT_STATUS_DEFAULT_PREV_STATUS),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'ADJUSTMENT_TYPE'; C: TaxLiabilityAdjustmentType_ComboChoices; D: TAX_LIABILITY_ADJUSTMENT_TYPE_NONE),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'TAX_TYPE'; C: StateTaxLiabilityType_ComboChoices; ),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'THIRD_PARTY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATE_TAX_LIABILITIES; F: 'IMPOUNDED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SALESPERSON; F: 'SALESPERSON_TYPE'; C: PositionStatus_ComboChoices; ),
    (T: TCO_SALESPERSON_FLAT_AMT; F: 'FREQUENCY'; C: ScheduledFrequency_Billing_ComboChoices; D: SCHED_FREQ_ONE_TIME),
    (T: TCO_SALESPERSON_FLAT_AMT; F: 'WEEK_NUMBER'; C: WeekNumberToPrint_ComboChoices; ),
    (T: TCO_DEPARTMENT; F: 'PAY_FREQUENCY_HOURLY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_DEPARTMENT; F: 'PAY_FREQUENCY_SALARY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_DEPARTMENT; F: 'HOME_STATE_TYPE'; C: HomeStateType_ComboChoices; ),
    (T: TCO_TEAM; F: 'PAY_FREQUENCY_HOURLY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_TEAM; F: 'PAY_FREQUENCY_SALARY'; C: FrequencyType_ComboChoices; ),
    (T: TCO_TEAM; F: 'HOME_STATE_TYPE'; C: HomeStateType_ComboChoices; ),
    (T: TCO_TEAM; F: 'PRINT_GROUP_ADDRESS_ON_CHECK'; C: INCLUDE_REPT_OR_ADDRESS_ComboChoices; ),
    (T: TCO_BATCH_LOCAL_OR_TEMPS; F: 'EXCLUDE_LOCAL'; C: GroupBox_YesNo_ComboChoices;),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'EXCLUDE_STATE'; C: GroupBox_YesNo_ComboChoices;),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'EXCLUDE_ADDITIONAL_STATE'; C: ExcludeTaxes_ComboChoices;),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'EXCLUDE_SDI'; C: GroupBox_YesNo_ComboChoices;),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'EXCLUDE_SUI'; C: GroupBox_YesNo_ComboChoices;),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'TAX_AT_SUPPLEMENTAL_RATE'; C: GroupBox_YesNo_ComboChoices;),
    (T: TCO_STATES; F: 'STATE_EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'EE_SDI_EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'ER_SDI_EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'SUI_EXEMPT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_STATES; F: 'USE_STATE_FOR_SUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCO_STATES; F: 'IGNORE_STATE_TAX_DEP_THRESHOLD'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TCO_STATES; F: 'STATE_NON_PROFIT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SUI_LIABILITIES; F: 'STATUS'; C: TaxDepositStatus_ComboChoices; ),
    (T: TCO_SUI_LIABILITIES; F: 'PREV_STATUS'; C: TaxDepositStatus_ComboChoices; D: TAX_DEPOSIT_STATUS_DEFAULT_PREV_STATUS),
    (T: TCO_SUI_LIABILITIES; F: 'ADJUSTMENT_TYPE'; C: TaxLiabilityAdjustmentType_ComboChoices; D: TAX_LIABILITY_ADJUSTMENT_TYPE_NONE),
    (T: TCO_SUI_LIABILITIES; F: 'THIRD_PARTY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_SUI_LIABILITIES; F: 'IMPOUNDED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_PHONE; F: 'PHONE_TYPE'; C: Company_PhoneType_ComboChoices; ),
    (T: TCO_BATCH_STATES_OR_TEMPS; F: 'STATE_OVERRIDE_TYPE'; C: OverrideValueType_ComboChoices; ),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'FREQUENCY'; C: TimeOffAccrualFrequency_ComboChoices; ),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'PAYROLL_OF_THE_MONTH_TO_ACCRUE'; C: TimeOffAccrualPayrollOfMonth_ComboChoices; D: TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'CALCULATION_METHOD'; C: AccrualCalcMethod_ComboChoices; ),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'ANNUAL_RESET_CODE'; C: AccrualResetCode_ComboChoices; D: TIME_OFF_ACCRUAL_RESET_CODE_NONE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'AUTO_CREATE_ON_NEW_HIRE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'ACCRUAL_ACTIVE'; C: AccrualActive_ComboChoices; D: TIME_OFF_ACCRUAL_ACTIVE_ALL_EES),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'SHOW_USED_BALANCE_ON_CHECK'; C: TimeOffShow_ComboChoices; ),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'ROLLOVER_FREQ'; C: TimeOffRolloverFrequency_ComboChoices; D: TIME_OFF_ACCRUAL_FREQ_NONE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'ROLLOVER_PAYROLL_OF_MONTH'; C: TimeOffAccrualPayrollOfMonth_ComboChoices; D: TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'RESET_PAYROLL_OF_MONTH'; C: TimeOffAccrualPayrollOfMonth_ComboChoices; D: TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'CHECK_TIME_OFF_AVAIL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'PROCESS_ORDER'; C: TimeOffProcessOrder_ComboChoices; D: TIME_OFF_PROCESS_ORDER_USE_RESET_ROLLOVER_ACCRUE),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'ACCRUE_ON_STANDARD_HOURS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_TIME_OFF_ACCRUAL; F: 'SHOW_ESS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TEE_DIRECT_DEPOSIT; F: 'EE_BANK_ACCOUNT_TYPE'; C: ReceivingAcctType_ComboChoices; ),
    (T: TEE_DIRECT_DEPOSIT; F: 'IN_PRENOTE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_DIRECT_DEPOSIT; F: 'READ_ONLY_REMOTES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TEE_STATES; F: 'OVERRIDE_STATE_TAX_TYPE'; C: OverrideValueType_ComboChoices; D: OVERRIDE_VALUE_TYPE_NONE),
    (T: TEE_STATES; F: 'RECIPROCAL_METHOD'; C: StateReciprocal_ComboChoices; D: STATE_RECIPROCAL_TYPE_NONE),
    (T: TEE_STATES; F: 'IMPORTED_MARITAL_STATUS'; C: GroupBox_SingleMarried_ComboChoices; D: GROUP_BOX_SINGLE),
    (T: TEE_STATES; F: 'CALCULATE_TAXABLE_WAGES_1099'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_TIME_OFF_ACCRUAL; F: 'STATUS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE_TIME_OFF_ACCRUAL; F: 'JUST_RESET'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_TIME_OFF_ACCRUAL; F: 'ROLLOVER_FREQ'; C: TimeOffRolloverFrequency_ComboChoices; D: TIME_OFF_ACCRUAL_FREQ_NONE),
    (T: TEE_TIME_OFF_ACCRUAL; F: 'ROLLOVER_PAYROLL_OF_MONTH'; C: TimeOffAccrualPayrollOfMonth_ComboChoices; D: TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE),
    (T: TEE_TIME_OFF_ACCRUAL_OPER; F: 'OPERATION_CODE'; C: EeToaOperCode_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'TARGET_ACTION'; C: ScheduledEDTarget_ComboChoices; D: SCHED_ED_TARGET_NONE),
    (T: TEE_SCHEDULED_E_DS; F: 'CALCULATION_TYPE'; C: ScheduledCalcMethod_ComboBoxChoices; D:CALC_METHOD_FIXED ),
    (T: TEE_SCHEDULED_E_DS; F: 'FREQUENCY'; C: ScheduledEDFrequency_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'WHICH_CHECKS'; C: GroupBox_WichChecks_ComboChoices; D: GROUP_BOX_ALL),
    (T: TEE_SCHEDULED_E_DS; F: 'PLAN_TYPE'; C: MonthNumber_ComboChoices; D: MONTH_NUMBER_NONE),
    (T: TEE_SCHEDULED_E_DS; F: 'BENEFIT_AMOUNT_TYPE'; C: BenefitAmountType_ComboChoices; D: BENEFIT_AMOUNT_TYPE_NONE),
    (T: TEE_SCHEDULED_E_DS; F: 'DEDUCTIONS_TO_ZERO'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TEE_SCHEDULED_E_DS; F: 'USE_PENSION_LIMIT'; D: 'N'),

    (T: TEE_CHILD_SUPPORT_CASES; F: 'ARREARS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TEE_SCHEDULED_E_DS; F: 'ALWAYS_PAY'; C: AlwaysPay_ComboChoices; D: ALWAYS_PAY_NO),
    (T: TEE_SCHEDULED_E_DS; F: 'DEDUCT_WHOLE_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_SCHEDULED_E_DS; F: 'EXCLUDE_WEEK_1'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'EXCLUDE_WEEK_2'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'EXCLUDE_WEEK_3'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'EXCLUDE_WEEK_4'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'EXCLUDE_WEEK_5'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TEE_SCHEDULED_E_DS; F: 'SCHEDULED_E_D_ENABLED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TEE_STATES; F: 'STATE_EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_STATES; F: 'EE_SDI_EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_STATES; F: 'EE_SUI_EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_STATES; F: 'ER_SDI_EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_STATES; F: 'ER_SUI_EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_STATES; F: 'SALARY_TYPE'; C: GroupBox_SalaryType_ComboChoices; D: SALARY_TYPE_NA),

    (T: TEE; F: 'DISTRIBUTE_TAXES'; C: EE_Labor_Distribution_Options_Comnbochoices; D: DISTRIBUTE_BOTH),
    (T: TEE; F: 'FEDERAL_MARITAL_STATUS'; C: GroupBox_SingleMarried_ComboChoices; D: GROUP_BOX_SINGLE),
    (T: TEE; F: 'NUMBER_OF_DEPENDENTS'; D: '0'),
    (T: TEE; F: 'COMPANY_OR_INDIVIDUAL_NAME'; C: GroupBox_COorINDIV_ComboChoices; D: GROUP_BOX_INDIVIDUAL),
    (T: TEE; F: 'OVERRIDE_FED_TAX_TYPE'; C: OverrideValueType_ComboChoices; D: OVERRIDE_VALUE_TYPE_NONE),
    (T: TEE; F: 'PAY_FREQUENCY'; C: PayFrequencyType_ComboChoices; ),
    (T: TEE; F: 'NEXT_PAY_FREQUENCY'; C: PayFrequencyType_ComboChoices; D: FREQUENCY_TYPE_WEEKLY),
    (T: TEE; F: 'W2_TYPE'; C: W2Type_ComboChoices; D: W2_TYPE_FEDERAL),
    (T: TEE; F: 'EIC'; C: EIC_ComboChoices; D: EIC_NONE),
    (T: TEE; F: 'NEW_HIRE_REPORT_SENT'; C: NewHireReport_ComboChoices; D: NEW_HIRE_REPORT_PENDING), //TRAVIS//
    (T: TEE; F: 'CURRENT_TERMINATION_CODE'; C: EE_TerminationCode_ComboChoices; D: EE_TERM_ACTIVE),
    (T: TEE; F: 'POSITION_STATUS'; C: PositionStatus_ComboChoices; D: POSITION_STATUS_NA),
    (T: TEE; F: 'ACA_STATUS'; C: ACAStatus_ComboChoices; D: ACA_STATUS_NA),
    (T: TEE; F: 'W2_DECEASED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'W2_STATUTORY_EMPLOYEE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'W2_LEGAL_REP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'W2_DEFERRED_COMP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'W2_PENSION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EXEMPT_EXCLUDE_EE_FED'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE; F: 'EXEMPT_EMPLOYEE_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EXEMPT_EMPLOYER_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EXEMPT_EMPLOYEE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EXEMPT_EMPLOYER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EXEMPT_EMPLOYER_FUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'TIPPED_DIRECTLY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'FLSA_EXEMPT'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'GOV_GARNISH_PRIOR_CHILD_SUPPT'; C: DeductionsTakeFirst_ComboChoices; D: TAKE_FIRST_CHILD_GARNISHMENT),
    (T: TEE; F: 'BASE_RETURNS_ON_THIS_EE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'TAX_AMT_DETERMINED_1099R'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'TOTAL_DISTRIBUTION_1099R'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'PENSION_PLAN_1099R'; C: GroupBox_PensionPlan1099R_ComboChoices; D: PENSION_PLAN_1099R_NONE),
    (T: TEE; F: 'MAKEUP_FICA_ON_CLEANUP_PR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'GENERATE_SECOND_CHECK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'HIGHLY_COMPENSATED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'CORPORATE_OFFICER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'EE_ENABLED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'SELFSERVE_ENABLED'; C: GroupBox_YesNoFullAccess_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'SELFSERVE_PASSWORD'; s:true),
    (T: TEE; F: 'PRINT_VOUCHER'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'HEALTHCARE_COVERAGE'; C: Healthcare_Coverage_ComboChoices; D: HEALTHCARE_NOT_ELIGIBLE),
    (T: TEE; F: 'WC_WAGE_LIMIT_FREQUENCY'; C: WorkersComp_Wage_Limit_Frequency_ComboChoices; D: WC_LIMIT_FREQ_ANNUALLY),
    (T: TEE; F: 'BENEFITS_ENABLED'; C: GroupBox_YesNoFullAccess_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'TIME_OFF_ENABLED'; C: GroupBox_YesNoFullAccess_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'EXISTING_PATIENT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'DEPENDENT_BENEFITS_AVAILABLE'; C: GroupBox_BenefitAvailable_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'LAST_QUAL_BENEFIT_EVENT'; C: GroupBox_Last_qual_event_ComboChoices; D: LAST_QUAL_NA),
    (T: TEE; F: 'W2_FORM_ON_FILE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'W2'; C: EE_ReportResultType_ComboChoices; D: EE_REPORT_RESULT_TYPE_BOTH),
    (T: TEE; F: 'ACA_COVERAGE_OFFER'; C: EE_ACA_COVERAGE_OFFER_ComboChoices ),
    (T: TEE; F: 'EE_ACA_SAFE_HARBOR'; C: EE_ACA_SAFE_HARBOR_ComboChoices ),

    (T: TEE; F: 'ENABLE_ANALYTICS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'ACA_POLICY_ORIGIN'; C: GroupBox_Aca_policy_Origin_ComboChoices; D: ACA_POLICY_ORIGIN_B),

    (T: TEE; F: 'BENEFITS_ELIGIBLE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TEE; F: 'DIRECT_DEPOSIT_ENABLED'; C: GroupBox_YesNoFullAccess_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'ACA_FORM_ON_FILE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'ACA_FORMAT'; C: EE_ACA_FORMAT_ComboChoices; D: EE_ACA_FORMAT_NONE),
    (T: TEE; F: 'ACA_TYPE'; C: ACA_TYPE_ComboChoices; D: EE_ACA_TYPE_NONE),
    (T: TEE; F: 'BASE_ACA_ON_THIS_EE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE; F: 'COMMENSURATE_WAGE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'WORKATHOME'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE; F: 'ACA_SAFE_HARBOR_TYPE'; C:GroupBox_ACA_Safe_Harbor_type_ComboChoices; D:GROUP_BOX_NO),

    (T: TEE_LOCALS; F: 'EXEMPT_EXCLUDE'; C: GroupBox_ExemptExclude_ComboChoices; D: GROUP_BOX_INCLUDE),
    (T: TEE_LOCALS; F: 'DEDUCT'; C: Deduct_ComboChoices; D: DEDUCT_ALWAYS),
    (T: TEE_LOCALS; F: 'LOCAL_ENABLED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE_LOCALS; F: 'OVERRIDE_LOCAL_TAX_TYPE'; C: OverrideValueType_ComboChoices; D: OVERRIDE_VALUE_TYPE_NONE),

    (T: TEE_PENSION_FUND_SPLITS; F: 'EMPLOYEE_OR_EMPLOYER'; C: GroupBox_EmployeeorEmployer_ComboChoices; ),
    (T: TEE_RATES; F: 'PRIMARY_RATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_RATES; F: 'RATE_AMOUNT'; D: '0'),

    (T: TPR; F: 'PAYROLL_TYPE'; C: PayrollType_ComboChoices; D: PAYROLL_TYPE_REGULAR),
    (T: TPR; F: 'SCHEDULED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR; F: 'CHECK_DATE_STATUS';  C: PayrollCheckDateStatus_ComboChoices;  D: DATE_STATUS_NORMAL),
    (T: TPR; F: 'EXCLUDE_TAX_DEPOSITS'; C: GroupBox_ExcludeTax_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'EXCLUDE_R_C_B_0R_N'; C: GroupBox_Exclude_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'EXCLUDE_ACH'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'EXCLUDE_BILLING'; C: GroupBox_AllNoneSelect_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'EXCLUDE_AGENCY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'MARK_LIABS_PAID_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'EXCLUDE_TIME_OFF'; C: GroupBox_ExcludeTimeOff_ComboChoices; D: TIME_OFF_EXCLUDE_ACCRUAL_NONE),
    (T: TPR; F: 'STATUS'; C: PayrollStatus_ComboChoices; D: PAYROLL_STATUS_PENDING),
    (T: TPR; F: 'INVOICE_PRINTED'; C: PayrollInvoicePrinted_ComboChoices; D: INVOICE_PRINT_STATUS_Pending),
    (T: TPR; F: 'SAME_DAY_PULL_AND_REPLACE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'COMBINE_RUNS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR; F: 'PRINT_ALL_REPORTS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR; F: 'APPROVED_BY_FINANCE'; C: GroupBox_PrApproved_ComboChoices; D: PR_APPROVED_IGNORE),
    (T: TPR; F: 'APPROVED_BY_TAX'; C: GroupBox_PrApproved_ComboChoices; D: PR_APPROVED_IGNORE),
    (T: TPR; F: 'APPROVED_BY_MANAGEMENT'; C: GroupBox_PrApproved_ComboChoices; D: PR_APPROVED_IGNORE),
    (T: TPR; F: 'UNLOCKED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),

    (T: TPR_SCHEDULED_EVENT_BATCH; F: 'FREQUENCY'; C: SchedFrequencies_ComboChoices; ),
    (T: TPR_SCHEDULED_E_DS; F: 'EXCLUDE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_MISCELLANEOUS_CHECKS; F: 'CHECK_STATUS'; C: MiscCheckStatus_ComboChoices; ),
    (T: TPR_MISCELLANEOUS_CHECKS; F: 'MISCELLANEOUS_CHECK_TYPE'; C: MiscCheckType_ComboChoices; ),
    (T: TPR_MISCELLANEOUS_CHECKS; F: 'EFTPS_TAX_TYPE'; C: EftpsTaxType_ComboChoices; D: EFTPS_TAX_TYPE_REGULAR ),

    (T: TPR_BATCH; F: 'FREQUENCY'; C: PayFrequencyType_ComboChoices; ),

    (T: TPR_BATCH; F: 'PAY_SALARY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_BATCH; F: 'PAY_STANDARD_HOURS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_BATCH; F: 'LOAD_DBDT_DEFAULTS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_BATCH; F: 'EXCLUDE_TIME_OFF'; C: GroupBox_ExcludeTimeOff_ComboChoices; D: TIME_OFF_EXCLUDE_ACCRUAL_NONE),

    (T: TPR_CHECK_LINES; F: 'USER_OVERRIDE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK_LINES; F: 'REDUCED_HOURS'; D: 'N'),
    (T: TPR_CHECK_LINES; F: 'WORK_ADDRESS_OVR'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK_LINES; F: 'AGENCY_STATUS'; D: CHECK_LINE_AGENCY_STATUS_PENDING),

    (T: TPR_CHECK; F: 'CHECK_TYPE'; C: CheckType2_ComboChoices; ),
    (T: TPR_CHECK; F: 'OR_CHECK_FEDERAL_TYPE'; C: OverrideValueType_ComboChoices; D: OVERRIDE_VALUE_TYPE_NONE),
    (T: TPR_CHECK; F: 'TAX_FREQUENCY'; C: TaxFrequencyType_ComboChoices; ),
    (T: TPR_CHECK; F: 'CHECK_STATUS'; C: PayrollCheckStatus_ComboChoices; ),

    (T: TPR_CHECK; F: 'CALCULATE_OVERRIDE_TAXES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TPR_CHECK; F: 'EXCLUDE_FEDERAL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_ADDITIONAL_FEDERAL'; C: ExcludeTaxes_ComboChoices; D: EXCLUDE_TAXES_NONE),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYEE_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYER_OASDI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYEE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYEE_EIC'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_EMPLOYER_FUI'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'TAX_AT_SUPPLEMENTAL_RATE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_DD'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_DD_EXCEPT_NET'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_TIME_OFF_ACCURAL'; C: GroupBox_ExcludeTimeOff_ComboChoices; D: TIME_OFF_EXCLUDE_ACCRUAL_NONE),
    (T: TPR_CHECK; F: 'EXCLUDE_AUTO_DISTRIBUTION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_ALL_SCHED_E_D_CODES'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_FROM_AGENCY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'PRORATE_SCHEDULED_E_DS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TPR_CHECK; F: 'CHECK_TYPE_945'; C: CheckType945_ComboChoices; D: CHECK_TYPE_945_NONE),

    (T: TPR_CHECK; F: 'DISABLE_SHORTFALLS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TPR_CHECK_LOCALS; F: 'EXCLUDE_LOCAL'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TPR_CHECK_LINE_LOCALS; F: 'EXEMPT_EXCLUDE'; C: GroupBox_LLExemptExclude_ComboChoices; D: GROUP_BOX_EXEMPT),

    (T: TPR_CHECK_STATES; F: 'TAX_AT_SUPPLEMENTAL_RATE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TPR_CHECK_STATES; F: 'EXCLUDE_STATE'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TPR_CHECK_STATES; F: 'EXCLUDE_SDI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TPR_CHECK_STATES; F: 'EXCLUDE_SUI'; C: GroupBox_YesNo_ComboChoices; ),
    (T: TPR_CHECK_STATES; F: 'EXCLUDE_ADDITIONAL_STATE'; C: ExcludeTaxes_ComboChoices; ),

    (T: TPR_CHECK_STATES; F: 'STATE_OVERRIDE_TYPE'; C: OverrideValueType_ComboChoices; ),

    (T: TTMP_CO_BANK_ACCOUNT_REGISTER; F: 'STATUS'; C: GroupBox_Bank_Register_Status_ComboChoices; ),
    (T: TTMP_CO_BANK_ACCOUNT_REGISTER; F: 'REGISTER_TYPE'; C: GroupBox_Bank_Register_Type_ComboChoices; ),
    (T: TTMP_CO_BANK_ACCOUNT_REGISTER; F: 'MANUAL_TYPE'; C: GroupBox_Bank_Register_ManualType_ComboChoices; ),

    (T: TCO_HR_CAR; F: 'PERSONAL_OR_COMPANY'; C: GroupBox_PersonalOrCompany_ComboChoices; D:CAR_COMPANY),
    (T: TCO_HR_CAR; F: 'COLOR'; C: GroupBox_CarColor_ComboChoices; D:CAR_COLOR_OTHER),
    (T: TCO_HR_APPLICANT; F: 'DESIRED_REGULAR_OR_TEMPORARY'; C: GroupBox_DesiredRegularOrTemporary_ComboChoices; D:DESIRED_REGULAR),
    (T: TCO_HR_APPLICANT; F: 'APPLICANT_STATUS'; C: GroupBox_ApplicantStatus_ComboChoices; D: APPLICANT_STATUS_SENT_APPLICATION),
    (T: TCO_HR_APPLICANT; F: 'APPLICANT_CONTACTED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_HR_APPLICANT; F: 'APPLICANT_TYPE'; C: GroupBox_ApplicantType_ComboChoices;),
    (T: TCO_HR_APPLICANT; F: 'POSITION_STATUS'; C: PositionStatus_ComboChoices; D:POSITION_STATUS_FULL),

    (T: TCO_HR_APPLICANT_INTERVIEW; F: 'CALL_BACK'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCL_HR_PERSON_EDUCATION; F: 'DEGREE_LEVEL'; C: GroupBox_DegreeLevel_ComboChoices; D:DEGREE_LEVEL_OTHER),
    (T: TEE_HR_ATTENDANCE; F: 'EXCUSED'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_HR_ATTENDANCE; F: 'WARNING_ISSUED'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_HR_CO_PROVIDED_EDUCATN; F: 'COMPLETED'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_HR_CO_PROVIDED_EDUCATN; F: 'TAXABLE'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_HR_CO_PROVIDED_EDUCATN; F: 'RENEWAL_STATUS'; C: RenewalStatus_ComboChoices; D: RENEWAL_STATUS_NONE),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'CASE_STATUS'; C: GroupBox_CaseStatus_ComboChoices; D:CASE_STATUS_OPEN),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'FIRST_AID'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'DEATH'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'EMERGENCY_ROOM'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'PRIVACY_CASE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_HR_INJURY_OCCURRENCE; F: 'OVERNIGHT_HOSPITALIZATION'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_BENEFIT_PAYMENT; F: 'VOID'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TEE_BENEFITS; F: 'ENROLLMENT_STATUS'; C: Cobra_Enrollment_Status_ComboChoices; D: COBRA_ENROLLMENT_STATUS_ACTIVE),
    (T: TEE_BENEFITS; F: 'DEDUCTION_FREQUENCY'; C: ScheduledFrequency_ComboChoices; D: SCHED_FREQ_SCHEDULED_PAY ),
    (T: TEE_BENEFITS; F: 'BENEFIT_TYPE'; C: GroupBox_BenefitType_ComboChoices; D: GROUP_BOX_PLAN),
    (T: TEE_BENEFITS; F: 'COBRA_STATUS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO ),
    (T: TEE_BENEFITS; F: 'COBRA_TERMINATION_EVENT'; C: Cobra_Events_ComboChoices; D: ' '),
    (T: TEE_BENEFITS; F: 'COBRA_REASON_FOR_REFUSAL'; C: Cobra_Reason_For_Refusal_ComboChoices; D: ' '),
    (T: TEE_BENEFITS; F: 'COBRA_MEDICAL_PARTICIPANT'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_BENEFITS; F: 'COBRA_SS_DISABILITY'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_BENEFITS; F: 'RATE_TYPE'; C: Benefit_RATE_TYPE_ComboChoices; D: BENEFIT_RATES_RATE_TYPE_AMOUNT),
    (T: TEE_BENEFITS; F: 'STATUS'; C: GroupBox_BenefitStatus_ComboChoices; D: GROUP_BOX_COMPLETED ),

    (T: TEE_HR_PERFORMANCE_RATINGS; F: 'INCREASE_TYPE'; C: PERFORMANCE_RATINGS_Type_ComboChoices; D:PERFORMANCE_RATINGS_Type_NoChange),

    (T: TEE_BENEFICIARY; F: 'PRIMARY_BENEFICIARY'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_BENEFIT_SUBTYPE; F: 'ACA_LOWEST_COST'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_BENEFIT_SUBTYPE; F: 'AGE_BANDS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TCO_BENEFIT_RATES; F: 'RATE_TYPE'; C: Benefit_RATE_TYPE_ComboChoices; D: BENEFIT_RATES_RATE_TYPE_AMOUNT),
    (T: TCO_BENEFIT_RATES; F: 'PERIOD_END'; C: ''; D: '1/1/2100'),

    (T: TEE_BENEFIT_PAYMENT; F: 'PAYMENT_TYPE'; C: PaymentType_ComboChoices2 ; D: PAYMENT_TYPE_CHECK),
    (T: TEE_BENEFIT_PAYMENT; F: 'RECONCILED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TEE_BENEFIT_PAYMENT; F: 'BENEFIT_DEDUCTION_TYPE'; C: Benefit_Deduction_Type_ComboChoices; D: BENEFIT_DEDUCTION_TYPE_NA),

    (T: TCL_E_DS; F: 'COBRA_ELIGIBLE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO_E_D_CODES; F: 'EE_OR_ER_BENEFIT'; C: Benefit_Deduction_Type_ComboChoices; D: BENEFIT_DEDUCTION_TYPE_NA),
    (T: TCO_E_D_CODES; F: 'USED_AS_BENEFIT'; C: GroupBox_UsedAsBenefit_ComboChoices; D: USEDASBENEFIT_NONE),

    (T: TCL_HR_COURSE; F: 'RENEWAL_STATUS'; C: RenewalStatus_ComboChoices; D:RENEWAL_STATUS_NONE),
    (T: TCO; F: 'SHOW_MANUAL_CHECKS_IN_ESS'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'PAYROLL_REQUIRES_MGR_APPROVAL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'QTR_LOCK_FOR_TAX_PMTS'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_GROUP; F: 'GROUP_TYPE'; C:ESS_GroupType_ComboChoices; D:ESS_GROUPTYPE_PERSONALINFO),
    (T: TCO_GROUP_MANAGER; F: 'SEND_EMAIL'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TCO_LOCAL_TAX; F: 'LAST_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_LOCAL_TAX; F: 'DEDUCT'; C: Deduct_ComboChoices; D:DEDUCT_ALWAYS),
    (T: TCO_STATES; F: 'LAST_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TCO_SUI; F: 'LAST_TAX_RETURN'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),
    (T: TEE_CHANGE_REQUEST; F: 'REQUEST_TYPE';{ C: GroupBox_YesNo_ComboChoices;} D:'H'),
    (T: TEE_CHANGE_REQUEST; F: 'STATUS'; D:'N'),

    (T: TEE_DIRECT_DEPOSIT; F: 'FORM_ON_FILE'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TPR_CHECK; F: 'UPDATE_BALANCE'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_YES),
    (T: TSB; F: 'MARK_LIABS_PAID_DEFAULT'; C: GroupBox_YesNo_ComboChoices; D:GROUP_BOX_NO),

    (T: TSB; F: 'TRUST_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_ACH),
    (T: TSB; F: 'TAX_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_ACH),
    (T: TSB; F: 'DD_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_ACH),
    (T: TSB; F: 'BILLING_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_ACH),
    (T: TSB; F: 'WC_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_ACH),

    (T: TSY_DASHBOARDS; F: 'DASHBOARD_TYPE'; C: GroupBox_YesNo_ComboChoices; D: DASHBOARD_TYPE_NA),
    (T: TSY_DASHBOARDS; F: 'RELEASED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSY_ANALYTICS_TIER; F: 'MOBILE_APP'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TSY_ANALYTICS_TIER; F: 'INTERNAL_BENCHMARKING'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),

    (T: TSY_VENDORS; F: 'VENDOR_TYPE'; C: GroupBox_Vendor_Type_ComboChoices; D: VENDOR_TYPE_STRATEGIC),

    (T: TSB_DASHBOARDS; F: 'DASHBOARD_TYPE'; C: GroupBox_YesNo_ComboChoices; D: DASHBOARD_TYPE_NA),
    (T: TSB_ENABLED_DASHBOARDS; F: 'DASHBOARD_LEVEL'; C: GroupBox_Level_ComboChoices; D: LEVEL_SYSTEM),
    (T: TSB_CUSTOM_VENDORS; F: 'VENDOR_TYPE'; C: GroupBox_Vendor_Type_ComboChoicesSB; D: VENDOR_TYPE_OTHER),
    (T: TSB_VENDOR; F: 'VENDORS_LEVEL'; C: GroupBox_Level_ComboChoices; D: LEVEL_SYSTEM),
    (T: TSB_VENDOR_DETAIL; F: 'DETAIL_LEVEL'; C: GroupBox_Level_Company_ComboChoices; D: LEVEL_BUREAU),
    (T: TSB_VENDOR_DETAIL; F: 'CO_DETAIL_REQUIRED'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),


    (T: TCO; F: 'TRUST_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO; F: 'TAX_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO; F: 'DD_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO; F: 'BILLING_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO; F: 'WC_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),

    (T: TPR; F: 'TRUST_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TPR; F: 'TAX_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TPR; F: 'DD_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TPR; F: 'BILLING_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TPR; F: 'WC_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),

    (T: TCO_PR_ACH; F: 'TRUST_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO_PR_ACH; F: 'TAX_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO_PR_ACH; F: 'DD_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO_PR_ACH; F: 'BILLING_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),
    (T: TCO_PR_ACH; F: 'WC_IMPOUND'; C: Cash_Management_ComboChoices; D: PAYMENT_TYPE_NONE),

    (T: TCO; F: 'ENABLE_ESS'; C: GroupBox_YesNoReadOnly_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ENABLE_TIME_OFF'; C: GroupBox_YesNoReadOnly_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ENABLE_BENEFITS'; C: GroupBox_YesNoReadOnly_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'ENABLE_HR'; C: EnableCompanyFeatures_ComboChoices; D: GROUP_BOX_NO),
    (T: TCL_PERSON; F: 'SERVICE_MEDAL_VETERAN'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_PERSON; F: 'OTHER_PROTECTED_VETERAN'; C: GroupBox_YesNoNotApplicative_ComboChoices; D: GROUP_BOX_NOT_APPLICATIVE),
    (T: TCL_E_DS; F: 'SHOW_ON_REPORT'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_YES),
    (T: TCL_PERSON; F: 'NATIVE_LANGUAGE'; C: Language_ComboChoices; D: LANGUAGE_ENGLISH),
    (T: TEE_DIRECT_DEPOSIT; F: 'SHOW_IN_EE_PORTAL'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO),
    (T: TCO; F: 'PRENOTE'; C: GroupBox_YesNo_ComboChoices; D: GROUP_BOX_NO)
  );


  function QueueTaskType_ComboChoices: String;
  function FieldConstList: TStringList;
  function GetConstChoisesByField(const AFieldName: string): string;

var  FieldCodeValues: TevFieldCodeValues;

implementation

var
  FFieldConstList: TStringList = nil;


function QueueTaskType_ComboChoices: String;

  function RemoveTaskWord(const ATaskName: String): String;
  begin
    Result := Copy(ATaskName, 6, Length(ATaskName) - 5);
  end;

begin
  Result := RemoveTaskWord(QUEUE_TASK_PREPROCESS_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_PAYROLL) + #13 +
            RemoveTaskWord(QUEUE_TASK_PROCESS_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_PAYROLL) + #13 +
            RemoveTaskWord(QUEUE_TASK_CREATE_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_CREATE_PAYROLL) + #13 +
            RemoveTaskWord(QUEUE_TASK_PREPROCESS_QUARTER_END) + #9 + TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_QUARTER_END) + #13 +
            RemoveTaskWord(QUEUE_TASK_PROCESS_TAX_RETURNS) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_RETURNS) + #13 +
            RemoveTaskWord(QUEUE_TASK_PROCESS_TAX_PAYMENTS) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_PAYMENTS) + #13 +
            RemoveTaskWord(QUEUE_TASK_RUN_REPORT) + #9 + TaskTypeToMethodName(QUEUE_TASK_RUN_REPORT) + #13 +
            RemoveTaskWord(QUEUE_TASK_REBUILD_TMP_TABLES) + #9 + TaskTypeToMethodName(QUEUE_TASK_REBUILD_TMP_TABLES) + #13 +
            RemoveTaskWord(QUEUE_TASK_PAYROLL_ACH) + #9 + TaskTypeToMethodName(QUEUE_TASK_PAYROLL_ACH) + #13 +
            RemoveTaskWord(QUEUE_TASK_PROCESS_PRENOTE_ACH) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_PRENOTE_ACH) + #13 +
            RemoveTaskWord(QUEUE_TASK_PROCESS_MANUAL_ACH) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_MANUAL_ACH) + #13 +
            RemoveTaskWord(QUEUE_TASK_REBUILD_VMR_HIST) + #9 + TaskTypeToMethodName(QUEUE_TASK_REBUILD_VMR_HIST) + #13 +
            RemoveTaskWord(QUEUE_TASK_EVOX_IMPORT) + #9 + TaskTypeToMethodName(QUEUE_TASK_EVOX_IMPORT) + #13 +
            RemoveTaskWord(QUEUE_TASK_QEC) + #9 + TaskTypeToMethodName(QUEUE_TASK_QEC) + #13 +
            RemoveTaskWord(QUEUE_TASK_REPRINT_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_REPRINT_PAYROLL) + #13 +
            RemoveTaskWord(QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS) + #9 + TaskTypeToMethodName(QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS) + #13 +
            RemoveTaskWord(QUEUE_TASK_ACA_STATUS_UPDATE) + #9 + TaskTypeToMethodName(QUEUE_TASK_ACA_STATUS_UPDATE);
end;



function FieldConstList: TStringList;

  procedure InitFieldConstList;
  var
    i, j, k: Integer;
    h, ChStr: string;
    Choices: TStringList;
    ff: TStringList;
  begin
    ff := TStringList.Create;

    Choices := TStringList.Create;

    try

      for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      begin
        Choices.Text := FieldsToPopulate[i].C;
        ChStr := '';
        for j := 0 to Choices.Count - 1 do
        begin
          k := Pos(#9, Choices[j]);
          h := Copy(Choices[j], k + 1, Length(Choices[j]) - k);
          if ChStr <> '' then
            ChStr := ChStr + #13;
          ChStr := ChStr + Copy(Choices[j], k + 1, Length(Choices[j]) - k) + '=' + Copy(Choices[j], 0, k - 1);
        end;

        ff.Add(RightStr(FieldsToPopulate[i].T.ClassName, Pred(Length(FieldsToPopulate[i].T.ClassName))) + '_' + FieldsToPopulate[i].F + '=' + ChStr);
      end;

      ff.Sorted := True;
      FFieldConstList := ff;

    finally
      Choices.Free;
    end;
  end;

begin
  if not Assigned(FFieldConstList) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FFieldConstList) then
        InitFieldConstList;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FFieldConstList;
end;


function GetConstChoisesByField(const AFieldName: string): string;
begin
  Result := FieldConstList.Values[AFieldName];
end;


{ TevFieldCodeValues }
procedure InitCheckReportNbrs;
var
  i: Char;
begin
  for i := Low(RegularCheckReportNbrs) to High(RegularCheckReportNbrs) do
    RegularCheckReportNbrs[i] := 220; //defaults to
  RegularCheckReportNbrs[CHECK_FORM_REGULAR] := 220;
  RegularCheckReportNbrs[CHECK_FORM_REGULAR_TOP] := 230;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM] := 579;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_SPANISH] := 1779;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_RATE] := 1584;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_FF] := 1103;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_WITH_ADDR] := 732;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL] := 240;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL] := 232;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT] := 1142;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW] := 232;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE] := 757;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_GREATLAND] := 838;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND] := 877;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL] := 929;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD] := 1193;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_FF] := 1459;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_ONLY] := 1708;

  RegularCheckReportNbrs[CHECK_FORM_REGULAR_NEW] := 1748;
  RegularCheckReportNbrs[CHECK_FORM_REGULAR_TOP_NEW] := 1749;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_NEW] := 1750;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_SPANISH_NEW] := 1780;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_2_STUB_NEW] := 3083;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_RATE_NEW] := 1753;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_FF_NEW] := 1751;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_WITH_ADDR_NEW] := 1754;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_TOP_NEW] := 2750;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_NEW] := 1761;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW] := 1602;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW] := 1602;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW] := 1758;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW] := 1757;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW] := 1762;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW] := 1756;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW] := 1759;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW] := 1760;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW] := 1755;
  RegularCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW] := 2510;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_ONLY_NEW] := 1752;
  RegularCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_PUNCH] := 1977;


  for i := Low(MiscCheckReportNbrs) to High(MiscCheckReportNbrs) do
    MiscCheckReportNbrs[i] := 221; //defaults to
  MiscCheckReportNbrs[CHECK_FORM_REGULAR] := 221;
  MiscCheckReportNbrs[CHECK_FORM_REGULAR_TOP] := 231;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM] := 580;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_RATE] := 580;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_SPANISH] := 580;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_FF] := 1104;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_WITH_ADDR] := 733;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL] := 239;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL] := 233;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW] := 1768;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT] := 233;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW] := 233;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE] := 756;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_GREATLAND] := 839;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND] := 878;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL] := 930;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_FF] := 1460;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD] := 930;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_ONLY] := 580;

  MiscCheckReportNbrs[CHECK_FORM_REGULAR_NEW] := 1763;
  MiscCheckReportNbrs[CHECK_FORM_REGULAR_TOP_NEW] := 1764;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_NEW] := 1765;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_RATE_NEW] := 1765;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_SPANISH_NEW] := 1765;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_2_STUB_NEW] := 1765;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_FF_NEW] := 1766;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_WITH_ADDR_NEW] := 1767;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_TOP_NEW] := 3282; // was 1765; changed ticket 104753 - new check form for Misc checks with Top Stub
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_NEW] := 1773;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW] := 1768;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW] := 1768;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW] := 1771;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW] := 1774;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW] := 1770;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW] := 1772;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW] := 1769;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW] := 1768;
  MiscCheckReportNbrs[CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW] := 1772;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_ONLY_NEW] := 1765;
  MiscCheckReportNbrs[CHECK_FORM_LETTER_BTM_STUB_PUNCH] := 1765;
end;

var
  i: Integer;
initialization
  FieldCodeValues := TevFieldCodeValues.Create;
  FieldCodeValues.Initialize;
  InitCheckReportNbrs;
  for i := 1 to High(FieldsToPopulate) do
    Assert(IsPublishedProp(FieldsToPopulate[i].T, FieldsToPopulate[i].F), FieldsToPopulate[i].T.ClassName + '.' + FieldsToPopulate[i].F + ' does not exist');

finalization
  FreeAndNil(FieldCodeValues);
  FreeAndNil(FFieldConstList);
end.
