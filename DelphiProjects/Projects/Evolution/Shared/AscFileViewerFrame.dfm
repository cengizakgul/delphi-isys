object AscFileViewer: TAscFileViewer
  Left = 0
  Top = 0
  Width = 452
  Height = 246
  TabOrder = 0
  object evPanel1: TevPanel
    Left = 0
    Top = 208
    Width = 452
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bSave: TevBitBtn
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 0
      OnClick = bSaveClick
    end
  end
  object evMemo: TevRichEdit
    Left = 0
    Top = 0
    Width = 452
    Height = 208
    Align = alClient
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object SaveDialog: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 104
    Top = 216
  end
end
