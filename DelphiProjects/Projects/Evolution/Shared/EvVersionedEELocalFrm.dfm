inherited EvVersionedEELocal: TEvVersionedEELocal
  Left = 668
  Top = 274
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 293
    inherited grFieldData: TevDBGrid
      Height = 166
    end
    inherited pnlEdit: TevPanel
      Top = 210
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 293
    Width = 791
    Height = 140
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 140
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Additional Local Details'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object rgLocalEnabled: TevDBRadioGroup
        Left = 20
        Top = 35
        Width = 270
        Height = 37
        Caption = 'Local Enabled'
        Columns = 2
        DataSource = dsFieldData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
        OnChange = UpdateFieldOnChange
      end
      object rdDeduct: TevDBRadioGroup
        Left = 20
        Top = 74
        Width = 270
        Height = 37
        Caption = 'Deduct'
        Columns = 3
        DataSource = dsFieldData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Always'
          'No Overrides'
          'Never')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'D'
          'N')
        OnChange = UpdateFieldOnChange
      end
    end
  end
end
