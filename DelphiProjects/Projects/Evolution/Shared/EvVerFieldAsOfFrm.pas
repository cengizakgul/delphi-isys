unit EvVerFieldAsOfFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, DB,
  SysUtils, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Graphics, evCommonInterfaces,
  isBaseClasses, isBasicUtils, ExtCtrls, isUIFashionPanel;

type
  TevVerFieldAsOf = class(TFrame)
    dsFieldData: TevDataSource;
    grFieldData: TevDBGrid;
    fpChange: TisUIFashionPanel;
    procedure grFieldDataCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
  private
    FDifInfo: IisListOfValues;
  public
    procedure Init(const AData: IevDataSet; const ADifInfo: IisListOfValues; const AShowTextValue: Boolean); reintroduce;
  end;

implementation

uses isDataSet;

{$R *.dfm}

{ TevVerFieldAsOf }

procedure TevVerFieldAsOf.Init(const AData: IevDataSet; const ADifInfo: IisListOfValues; const AShowTextValue: Boolean);
var
  s: String;
begin
  FDifInfo := ADifInfo;
  if Assigned(AData) then
  begin
    dsFieldData.DataSet := AData.vclDataSet;

    s := 'begin_date'#9'12'#9'Period Begin'#9'F'#13 +
         'end_date'#9'12'#9'Period End'#9'F'#13;
    if AShowTextValue then
      s := s + 'text_value'#9'34'#9'Value'#9'F'
    else
      s := s + 'value'#9'34'#9'Value'#9'F';

    grFieldData.Selected.Text := s;
  end
  else
    dsFieldData.DataSet := nil;
end;

procedure TevVerFieldAsOf.grFieldDataCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
var
  s: String;
begin
  if Assigned(FDifInfo) then
  begin
    if gdSelected in State then
    begin
      if grFieldData.IsActiveRowAlternatingColor then
        ABrush.Color := grFieldData.PaintOptions.AlternatingRowColor
      else
        ABrush.Color := grFieldData.Color;

      AFont.Color := clBlack;
    end;

    s := FDifInfo.TryGetValue(IntToStr(Field.DataSet.RecNo), '');
    if s = 'D' then
    begin
      AFont.Color := clRed;
      AFont.Style := AFont.Style + [fsStrikeOut];
    end
    else if (s = 'N') or Contains(s, Field.FieldName) then
      AFont.Color := clRed;
  end;
end;



end.

