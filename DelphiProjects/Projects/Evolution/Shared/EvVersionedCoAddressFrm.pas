unit EvVersionedCoAddressFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, EvVersionedAddressFrm;

type
  TevVersionedCoAddress = class(TevVersionedAddress)
    lName: TevLabel;
    edName: TevDBEdit;
    lDBA: TevLabel;
    edDBA: TevDBEdit;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedCoAddress }

procedure TevVersionedCoAddress.FormShow(Sender: TObject);
begin
  inherited;

  Fields[5].AddValue('Title', lName.Caption);
  Fields[6].AddValue('Title', lDBA.Caption);

  Fields[0].AddValue('DisplayIndex', 2);
  Fields[1].AddValue('DisplayIndex', 3);
  Fields[2].AddValue('DisplayIndex', 4);
  Fields[3].AddValue('DisplayIndex', 5);
  Fields[4].AddValue('DisplayIndex', 6);
  Fields[5].AddValue('DisplayIndex', 0);
  Fields[6].AddValue('DisplayIndex', 1);

  Fields[5].AddValue('Width', 30);
  Fields[6].AddValue('Width', 15);

  lName.Caption := Iff(Fields[5].Value['Required'], '~', '') + lName.Caption;
  edName.DataField := Fields.ParamName(5);

  lDBA.Caption := Iff(Fields[6].Value['Required'], '~', '') + lDBA.Caption;
  edDBA.DataField := Fields.ParamName(6);
end;

end.

