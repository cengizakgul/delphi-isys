unit EvExceptions;

interface

uses isExceptions, EvStreamUtils;

type
  EevException = class(EisException);

  ECanNotClose = class(EevException);
  ELockedResource = class(EevException);
  EPrinterNotDefined = class(EevException);
  EDeviceNotAvailable = class(EevException);
  EDeviceIOException = class(EevException);
  EErrorMessageException = class(EevException);
  ETaxDepositException = class(EevException);
  ETaxSilentException = class(EevException);
  ECountMaxHitException = class(EevException);
  ERASConnectionError = class(EevException);
  EAccrualException = class(EevException);
  ESilentAccrualException = class(EAccrualException);
  EInvalidHashTotal = class(EevException);
  EMergeException = class(EevException);
  ECanNotInsertMiscCheck = class(EevException);
  ENoTargetActionSpecified = class(EevException);
  ENoCalcTypeForScheduledED = class(EevException);
  ENoPayrollDay = class(EevException);
  ENoWeeklySDICap = class(EevException);
  EWrongALDPrecentages = class(EevException);
  ENoBirthDate = class(EevException);
  EWrongMatchSetup = class(EevException);
  ECanNotCopyVoidedPayroll = class(EevException);
  ECanNotCopyReversalPayroll = class(EevException);
  ENoNextCheckDate = class(EevException);
  EInvalidParameters = class(EevException);
  EInconsistentData = class(EevException);
  EACHNoMatchingData = class(EevException);
  ENoRASInstalled = class(EevException);
  EMissingReport = class(EevException);
  EClientCreate = class(EevException);
  EMaritalStatusNotSetup = class(EevException);
  EEmployeeStateNotSetup = class(EevException);
  ENegativeCheck = class(EevException);
  EEmptyEEHomeState = class(EevException);
  ENoDirDepAttached = class(EevException);
  EDirDepIsNotAllowed = class(EevException);
  ESalaryNotSetup = class(EevException);
  EDuplicatedLiabilityAdjustment = class(EevException);
  ELoadSecurityInfo = class(EevException);
  ECanNotPerformOperation = class(EevException);
  EVersionCheck = class(EevException);
  ECheckPrintingException = class(EevException);
  EDataLostException = class(EevException);
  EPeriodIsLocked = class(EevException);
  EPayrollLimitation = class(EevException);
  EADRError = class(EevException);
  EADRChangeLogCorrupted = class(EADRError);
  EADRTooManyChanges = class(EADRError);
  EADRMissingRequest = class(EADRError);
  ESendEmailError = class(EevException);
  EMaintenance = class(EevException);
  ETempTablesOutOfSync = class(EevException);
  EBrokenDatabase = class(EevException);
  EDataValidationError = class(EevException);
  EEmployeeInPrenote =  class(EevException);

  // Security Exceptions
  ESecurity = class(EevException);
  ESecurityViolation = class(ESecurity);
  EInvalidLogin = class(ESecurity);
  ELockedAccount = class(ESecurity);
  EBlockedAccount = class(ESecurity);
  ENoRights = class(ESecurity);
  ENoRightsToClient = class(ESecurity);
  EChangePasswordException = class(ESecurity);
  
  EInvalidRecord = class(EevException)
  private
    FTableName: String;
    FFieldName: String;
  protected
    procedure RestoreFromStream(const AStream: IisStream); override;
  public
    function  GetStream: IevDualStream; override;
  public
    property TableName: String read FTableName;
    property FieldName: String read FFieldName;
    constructor Create(const AMessage, ATableName, AFieldName: string; const AIncludeStack: Boolean = False;
                       const AErrorCode: Integer = 0; const AHelpContext: Integer = 0); reintroduce;
  end;

const
// Help ID's for errors
  IDH_EDBAccessLibFunctionNotFound = 1000000; // Function or Procedure is not found in DBAccess library
  IDH_InvalidLogin = 1000001; // Invalid Login or Password
  IDH_InconsistentData = 1000002; // Problems with data (lost link between master and detail, unexpected or invalid value,
                                  //out-of-sync temp tables, another user changed the record, program bug)
  IDH_ServerNotAvailable = 1000003; // One of middle-tier servers can't be reached
  IDH_RBNotAvailable = 1000004; // Request Broker can't be reached
  IDH_OnlySingleBroker = 1000006; // Server can be connected only to one Request Broker at the time
  IDH_NoRASInstalled = 1000007; // The workstation doesn't have RAS installed
  IDH_MissingReport = 1000008; // Required report is not found in the database
  IDH_MissingPackage = 1000009; // Missing required package
  IDH_MaximumConnections = 1000011; // Maximum connections exceeded
  IDH_SecurityViolation = 1000012; // User doesn't have rights for certain operation
  IDH_Maintenance = 1000013; // Database is down for maintenance
  IDH_ClientCreate = 1000014; // Can't create new client database
  IDH_NotExistingClient = 1000015; // Client database doesn't exist
  IDH_InvalidParameters = 1000016; // Not sufficient or invalid user input to perform operation
  IDH_LoadSecurityInfo = 1000017; // Couldn't load security information
  IDH_MergeException = 1000018; // Problems with bank account register merging
  IDH_LockedResource = 1000019; // Resource is in exclusive use by someone and can't be modified by others
  IDH_CanNotClose = 1000020; // Closing of the current form is not allowed (not all conditions are satisfied)
  IDH_CanNotPerformOperation = 1000021; // current set of data doesn't allow the operation
  IDH_ConsistencyViolation = 1000022; // attempt to make data inconsistent or invalid
  IDH_RASConnectionError = 1000023; // problem with dial-up connection
  IDH_ChangePasswordException = 1000024; // invalid input while changing password
  IDH_PayrollWarnings = 1000025; // payroll warnings were isuued during tax payments procedures
  IDH_NeedsManualCorrection = 1000026; // clean-up can't be done automatically
  IDH_OutOfBalance = 1000027; // company is out of balance
  IDH_VersionCheck = 1000028; // program must be updated
  IDH_UNIXServer = 1000029; // can't work with files on UNIX server
  IDH_GBAKProcess = 1000030; // can't create GBAK.exe process
  IDH_IncorrectInstallation = 1000031; // problems with installation
  IDH_SocketError = 1000032; // socket connection error
  IDH_VariantError = 1000033; // invalid variant conversion or similar
  IDH_BadPath = 1000034; // invalid file path
  IDH_IncorrectDBVersion = 1000035; // database version mismatch
  IDH_CheckPrintingException = 1000036; // attempt to print checks for unprocessed payroll
  IDH_ReportCalcException = 1000037; // various error during report calculation
  IDH_DataLostException = 1000038; // data doesn't fit into specified place. one or more digits are lost
  IDH_EventAlerterInitFailed = 1000039; // failed to init Event alerts. most likely: HOSTS file is misconfigured on Linux server
  IDH_PeriodIsLocked = 100040; // data cannot be modified or added to closed period
  IDH_TaskCancelled = 100041; // queue task has been cancelled
  IDH_FileIsLocked = 100042; // file operation failed because file was locked (used by someone else)
  IDH_CorrectionRun = 100043; // correction runs can not be handled the same way as other payrolls

implementation

{ EInvalidRecord }

constructor EInvalidRecord.Create(const AMessage, ATableName,
  AFieldName: string; const AIncludeStack: Boolean; const AErrorCode,
  AHelpContext: Integer);
begin
  inherited Create(AMessage, AIncludeStack, AErrorCode, AHelpContext);
  FTableName := ATableName;
  FFieldName := AFieldName;
end;

function EInvalidRecord.GetStream: IevDualStream;
begin
  Result := inherited GetStream;
  Result.WriteShortString(TableName);
  Result.WriteShortString(FieldName);  
end;

procedure EInvalidRecord.RestoreFromStream(const AStream: IevDualStream);
begin
  inherited;
  FTableName := AStream.ReadShortString;
  FFieldName := AStream.ReadShortString;
end;

initialization
  RegisterExceptions([
    EevException, ECanNotClose, ELockedResource, EPrinterNotDefined,
    EDeviceNotAvailable, EDeviceIOException, EErrorMessageException,
    ETaxDepositException, ETaxSilentException, ECountMaxHitException,
    ERASConnectionError, EAccrualException, ESilentAccrualException,
    ENoRights, EInvalidHashTotal, EMergeException,
    ECanNotInsertMiscCheck, ENoTargetActionSpecified, ENoCalcTypeForScheduledED,
    ENoPayrollDay, ENoWeeklySDICap, EWrongALDPrecentages, ENoBirthDate,
    EWrongMatchSetup, ECanNotCopyVoidedPayroll,
    ECanNotCopyReversalPayroll, ENoNextCheckDate, EInvalidLogin,
    EInvalidParameters, EInconsistentData, EACHNoMatchingData,
    ENoRASInstalled, EMissingReport, EClientCreate,
    EMaritalStatusNotSetup, EEmployeeStateNotSetup,
    ENegativeCheck, EEmptyEEHomeState, ENoDirDepAttached, EDirDepIsNotAllowed,
    ESalaryNotSetup,EDuplicatedLiabilityAdjustment, ELoadSecurityInfo,
    ECanNotPerformOperation, EChangePasswordException,
    EVersionCheck, EMaintenance, ESecurityViolation,
    ECheckPrintingException, EDataLostException,
    EPeriodIsLocked, 
    ENoRightsToClient, EADRError, EADRChangeLogCorrupted, EADRTooManyChanges,
    EADRMissingRequest, ESendEmailError, ELockedAccount, EBlockedAccount,
    ETempTablesOutOfSync, EBrokenDatabase, EInvalidRecord, EDataValidationError]);

end.
