// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_BROWSE_MASTER_DETAIL_BASE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_BROWSE_DETAIL_BASE, DB, Wwdatsrc,  Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, ExtCtrls, EvUIComponents;

type
  TEDIT_BROWSE_MASTER_DETAIL_BASE = class(TEDIT_BROWSE_DETAIL_BASE)
    gMaster: TevDBGrid;
    evSplitter1: TevSplitter;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
