// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CategorizedComboDialog;

interface

uses
  Windows, ComCtrls, Controls, Grids, StdCtrls, Buttons, Classes, Forms, Sysutils,
  Graphics;

type
  TSortableStringGrid = class(TStringGrid)
  public
    procedure Sort(aCol: integer);
  end;

  TCategorizedComboDialog = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    StringGrid1: TStringGrid;
    HeaderControl1: THeaderControl;
    Label2: TLabel;
    procedure HeaderControl1SectionResize(HeaderControl: THeaderControl;
      Section: THeaderSection);
    procedure FormCreate(Sender: TObject);
    procedure HeaderControl1DrawSection(HeaderControl: THeaderControl;
      Section: THeaderSection; const Rect: TRect; Pressed: Boolean);
    procedure HeaderControl1SectionClick(HeaderControl: THeaderControl;
      Section: THeaderSection);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure StringGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    SortIndex: integer;
    procedure SortByColumn(aCol: integer);
    procedure CoordinateSizes;
  public
    { Public declarations }
    SelectedCode: string;
    procedure Setup(SChoices, SGrouping, CurrentValue: string);
  end;

implementation

{$R *.DFM}

function ConvertTabsToCR(const S: string): string;
var
  I: integer;
begin
  result := S;

  while True do
  begin
    I := LastDelimiter(#9, result);
    if I > 0 then
      result[I] := #13
    else
      break;
  end;
end;

procedure TSortableStringGrid.Sort(aCol: integer);
var
  Found: boolean;
  I, J: integer;
begin
  for I := 0 to RowCount - 1 do
  begin
    Found := False;
    for J := RowCount - 2 downto I do
      if CompareStr(Cells[aCol, J + 1], Cells[aCol, J]) < 0 then
      begin
        MoveRow(J + 1, J);
        Found := true;
      end;
    if not Found then
      break;
  end;
end;

procedure TCategorizedComboDialog.Setup(SChoices, SGrouping, CurrentValue: string);
var
  GroupingStringList: TStringList;
  ChoiceStringList: TStringList;
  Grouping: TStringList;
  Choices: TStringList;
  ItemCount: integer;
  I, J, J1, K: integer;
begin
  ItemCount := 0;
  GroupingStringList := TStringList.Create;
  ChoiceStringList := TStringList.Create;
  Grouping := TStringList.Create;
  Choices := TStringList.Create;
  Grouping.Text := SGrouping;
  Choices.Text := SChoices;

  for I := 0 to Grouping.Count - 1 do
  begin
    GroupingStringList.Text := ConvertTabsToCR(Grouping[I]);
    ItemCount := ItemCount + GroupingStringList.Count - 1;
  end;

  with StringGrid1 do
  begin
    ColCount := 3;
    RowCount := ItemCount;

    K := 0;
    for I := 0 to Grouping.Count - 1 do
    begin
      GroupingStringList.Text := ConvertTabsToCR(Grouping[I]);
      for J1 := 1 to GroupingStringList.Count - 1 do
        for J := 0 to Choices.Count - 1 do
        begin
          ChoiceStringList.Text := ConvertTabsToCr(Choices[J]);

          if (ChoiceStringList[1] = GroupingStringList[J1]) then
          begin
            Cells[0, K] := GroupingStringList[0];
            Cells[1, K] := ChoiceStringList[0];
            Cells[2, K] := ChoiceStringList[1];
            K := K + 1;
          end;

        end;
    end;
  end;

  with StringGrid1 do
    for I := 0 to RowCount - 1 do
      if Cells[2, I] = CurrentValue then
      begin
        Row := I;
        break;
      end;
  SortByColumn(2); // First by code
  SortByColumn(0); // then by Category
end;

procedure TCategorizedComboDialog.CoordinateSizes;
var
  I: integer;
begin
  for I := 0 to HeaderControl1.Sections.Count - 1 do
    with StringGrid1 do
      ColWidths[I] := HeaderControl1.Sections.Items[I].Width - 2;
end;

procedure TCategorizedComboDialog.HeaderControl1SectionResize(
  HeaderControl: THeaderControl; Section: THeaderSection);
begin
  CoordinateSizes;
end;

procedure TCategorizedComboDialog.FormCreate(Sender: TObject);
begin
  CoordinateSizes;
end;

procedure TCategorizedComboDialog.HeaderControl1DrawSection(
  HeaderControl: THeaderControl; Section: THeaderSection;
  const Rect: TRect; Pressed: Boolean);
begin
  if SortIndex >= 0 then
    if (HeaderControl.Sections[SortIndex] = Section) then

//     if Section.Text = 'Code' then
    begin
      HeaderControl.Canvas.Brush.Color := clYellow;
      HeaderControl.Brush.Color := clYellow;
    end;
  HeaderControl.Canvas.FillRect(Rect);
  HeaderControl.Canvas.TextOut(Rect.Left + 4, Rect.Top + 2, Section.Text);

  HeaderControl.Canvas.Brush.Color := clSilver;
  HeaderControl.Brush.Color := clSilver;
end;

procedure TCategorizedComboDialog.SortByColumn(aCol: integer);
begin
  TSortableStringGrid(StringGrid1).Sort(aCol);
  SortIndex := aCol;
  HeaderControl1.Refresh;
  with StringGrid1 do
    if Row > 5 then
      TopRow := Row - 5
    else
      TopRow := 0;
end;

procedure TCategorizedComboDialog.HeaderControl1SectionClick(
  HeaderControl: THeaderControl; Section: THeaderSection);
var
  I: integer;
begin
  for I := 0 to HeaderControl.Sections.Count - 1 do
//         if HeaderControl.Sections.Items [I].Text = Section.Text then
    if HeaderControl.Sections.Items[I] = Section then
    begin
      SortByColumn(I);
      break;
    end;
end;

procedure TCategorizedComboDialog.BitBtn1Click(Sender: TObject);
begin
  with StringGrid1 do
    SelectedCode := Cells[2, Row];
end;

procedure TCategorizedComboDialog.BitBtn2Click(Sender: TObject);
begin
  SelectedCode := '';
end;

procedure TCategorizedComboDialog.StringGrid1DblClick(Sender: TObject);
begin
  BitBtn1.Click;
end;

end.
