object AskGlobalUpdateParamsBase: TAskGlobalUpdateParamsBase
  Left = 406
  Top = 337
  Width = 370
  Height = 250
  Caption = 'AskGlobalUpdateParamsBase'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 354
    Height = 161
    Align = alClient
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 173
    Width = 354
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object evPanel4: TevPanel
      Left = 168
      Top = 0
      Width = 186
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      Caption = ' '
      TabOrder = 0
      object evBitBtn2: TevBitBtn
        Left = 16
        Top = 6
        Width = 75
        Height = 25
        Action = OK
        TabOrder = 0
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
          DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
          DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
          DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
          DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
          DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
          2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
          A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
          AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
        NumGlyphs = 2
        Margin = 0
      end
      object evBitBtn1: TevBitBtn
        Left = 103
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
          DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
          9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
          DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
          DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
          DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
          91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
          999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
        NumGlyphs = 2
        Margin = 0
      end
    end
  end
  object evPanel3: TevPanel
    Left = 0
    Top = 161
    Width = 354
    Height = 12
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      354
      12)
    object EvBevel1: TEvBevel
      Left = 8
      Top = 0
      Width = 345
      Height = 9
      Anchors = [akLeft, akTop, akRight]
      Shape = bsBottomLine
    end
  end
  object evActionList1: TevActionList
    Left = 16
    Top = 8
    object OK: TAction
      Caption = 'Update'
    end
  end
end
