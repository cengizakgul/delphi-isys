inherited evVersionedCOTier: TevVersionedCOTier
  Left = 468
  Top = 317
  ClientHeight = 366
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 366
    inherited grFieldData: TevDBGrid
      Height = 239
    end
    inherited pnlEdit: TevPanel
      Top = 283
      object lValue: TevLabel [2]
        Left = 243
        Top = 11
        Width = 32
        Height = 13
        Caption = 'Tier ID'
      end
      inherited pnlButtons: TevPanel
        Top = 19
      end
      object cbClCoCons: TevDBLookupCombo
        Left = 243
        Top = 26
        Width = 184
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'TIER_ID'#9'40'#9'Tier ID'#9'F'
          'DASHBOARD_DEFAULT_COUNT'#9'10'#9'Dashboard Default Count'#9'F'
          'DASHBOARD_MAX_COUNT'#9'10'#9'Dashboard Max Count'#9'F'
          'LOOKBACK_YEARS_DEFAULT'#9'10'#9'Lookback Years Default'#9'F'
          'LOOKBACK_YEARS_MAX'#9'10'#9'Lookback Years Max'#9'F'
          'USERS_DEFAULT_COUNT'#9'10'#9'Users Default Count'#9'F'
          'USERS_MAX_COUNT'#9'10'#9'Users Max Count'#9'F')
        DataField = 'SY_ANALYTICS_TIER_NBR'
        DataSource = dsFieldData
        LookupTable = DM_SY_ANALYTICS_TIER.SY_ANALYTICS_TIER
        LookupField = 'SY_ANALYTICS_TIER_NBR'
        Options = [loTitles]
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 536
    Top = 112
  end
end
