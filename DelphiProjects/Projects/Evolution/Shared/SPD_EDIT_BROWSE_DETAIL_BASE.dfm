inherited EDIT_BROWSE_DETAIL_BASE: TEDIT_BROWSE_DETAIL_BASE
  object PC: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 270
    HelpContext = 33501
    ActivePage = tshtBrowse
    Align = alClient
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object gBrowse: TevDBGrid
        Left = 0
        Top = 0
        Width = 320
        Height = 242
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_BROWSE_DETAIL_BASE\gBrowse'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = gBrowseDblClick
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
    end
  end
end
