unit EvCommonInterfaces;

interface

uses Classes, Types, Graphics, Controls, ISDataAccessComponents, SDDClasses,  EvStreamUtils,
     EvDataAccessComponents, EvTypes, SUniversalDataReader, isTypes, isDataSet, sSecurityClassDefs,
     isBaseClasses, DB, EvConsts, SReportSettings,  rwInputFormContainerFrm, isThreadManager, isSettings,
     rwPreviewContainerFrm, SDDStreamClasses, isLogFile, ISZippingRoutines, evTransportInterfaces,
     rwCustomDataDictionary, isStatistics, isMessenger, isBasicUtils, EvClientDataSet;

type

  TevUserAccountType = (uatUnknown, uatGuest, uatServiceBureau, uatRemote, uatWeb, uatSelfServe, uatSBAdmin);
  TevSecurityLevel = (slUnknown, slNone, slAdministrator, slManager, slSupervisor, slSBUser, slEmployee);
  TevTOAOperation = (toaApprove, toaDeny);

  IevContext = interface;
  IevTaskQueue = interface;
  IevDomainInfo = interface;
  IevSecAccountRights = interface;
  IevACHOptions = interface;
  IevTask = interface;
  IevGlobalFlagInfo = interface;

  TevUserInfo = record
    Domain:         String;
    UserName:       String;
    Password:       String;
  end;                              


  TevDBConnectionInfo = record
    Path:     String;
    User:     String;
    Password: String;
  end;

  PCreateModuleInstance = function : IInterface;


  TevBatchUpdateSQLInfo = record
    SQL:          String;
    NbrParamName: string;
    Params: IisListOfValues;
    Macros:  IisListOfValues;
  end;

  TevBatchUpdateParams = record
    SQLType: TUpdateKind;
    Params:  IisListOfValues;
  end;

  TevBatchUpdateSQL = array [TUpdateKind] of TevBatchUpdateSQLInfo;
  TevBatchUpdateSequence = array of TevBatchUpdateParams;

  TevTaskState = (tsInactive, tsWaiting, tsExecuting, tsFinished);
  TevTaskRequestState = (rsWaiting, rsStarting, rsExecuting, rsFinished);

  TevTaskQueueEvent = (tqeTaskListChange, tqeTaskReadyToPrint);

  TevEmailSendRule = (esrAlways, esrSuccessfull, esrExceptions, esrExceptionWarnings);

  TevAPIKeyType = (apiWebClient, apiSelfServe, apiVendor);

  TisMailPriority = (mpLow, mpNormal, mpHigh);

  IevModuleDescriptor = interface
    ['{261AE801-63ED-4280-A5AF-5C96889C75C7}']
    function  ModuleName: string;
    function  InstanceType: TGUID;
  end;


  IevDataSet = interface(IDataSet)
  ['{6DB1E059-F925-4344-BCFC-92C12FD26BA6}']
    function  GetAsStream: IEvDualStream;
    procedure SetAsStream(const AStream: IEvDualStream);
    property  AsStream: IEvDualStream read GetAsStream write SetAsStream;
  end;

  ISQLite = interface
  ['{D40784D8-9C3F-4EB5-BA86-EF37F2071350}']
    procedure Execute(const SQL: String);
    function Select(const SQL: String; const Params: array of Variant): IevDataset;
    procedure Insert(const TableName: String; const Dataset: TDataset; const CheckDate: TDateTime = 0; const Condition: String = ''; Order: Boolean = False);
    procedure InsertRecord(const TableName: String; const Dataset: TDataset);    
    function Value(const SQL: String; const Params: array of Variant): Variant;
    procedure CreateIndex(const Table, Index, Fields: String);
    procedure StartTransaction;
    procedure Commit;
    procedure Backup(const FileName: String);
    procedure Restore(const FileName: String);
  end;

  IevParam = interface
  ['{D58B80BE-A189-4DB7-859A-A50A8BC60990}']
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;
    function  GetName: String;
    property  Name: String read GetName;
    property  Value: Variant read GetValue write SetValue;
  end;

  IevParams = interface
  ['{5018020B-03BA-446A-939F-9DC4C8842263}']
    function Count: Integer;
    function GetParam(const AIndex: Integer): IevParam; overload;
    function GetParam(const AName: String): IevParam; overload;
    function FindParam(const AName: String): IevParam;
    function Add(const AName: String; const AValue: Variant): Integer;
  end;

  IevQuery = interface
  ['{8439C8B4-BC88-4F6E-AAF6-80DF172DFE7B}']
    function  QueryName: String;
    function  GetParams: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    function  GetMacros: IisListOfValues;
    procedure SetMacros(const AValue: IisListOfValues);
    procedure SetParam(Index: String; Value: Variant);
    procedure SetMacro(Index: String; Value: Variant);
    function  GetLoadBlobs: Boolean;
    procedure SetLoadBlobs(const AValue: Boolean);
    procedure Execute;
    function  Result: IEvDataSet;
    property  Params: IisListOfValues read GetParams write SetParams;
    property  Macros: IisListOfValues read GetMacros write SetMacros;
    property  Param[Index: String]: Variant write SetParam;
    property  Macro[Index: String]: Variant write SetMacro;
    property  LoadBlobs: Boolean read GetLoadBlobs write SetLoadBlobs;
  end;

  IevStoredProc = interface
  ['{4DE43B40-AEDA-4DE7-B4DB-15A0BDA8D98C}']
    function  ProcName: String;
    function  GetParams: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    procedure Execute;
    function  Result: IisListOfValues;
    property  Params: IisListOfValues read GetParams write SetParams;
  end;


  IevTable = interface
  ['{5C41B270-806B-494F-8C0E-76754500A03C}']
    function  TableName: String;
    function  FieldList: String;
    function  AsOfDate: TisDate;
    procedure Open;
    procedure SaveChanges;
    function  GetFields: TFields;
    function  GetBof: Boolean;
    function  GetEof: Boolean;
    function  GetFieldCount: Integer;
    function  GetRecordCount: Integer;
    function  GetRecNo: Integer;
    procedure SetRecNo(const AValue: Integer);
    function  GetFieldValue(const Index: string): Variant;
    procedure SetFieldValue(const Index: string; const AValue: Variant);
    function  GetIndexFieldNames: String;
    procedure SetIndexFieldNames(const AValue: String);
    function  GetFilter: String;
    procedure SetFilter(const AValue: String);
    function  GetFiltered: Boolean;
    procedure SetFiltered(const AValue: Boolean);
    function  GetBlobsLoadMode: TevBlobsLoadMode;
    procedure SetBlobsLoadMode(const AValue: TevBlobsLoadMode);
    procedure First;
    procedure Last;
    procedure Next;
    procedure Prior;
    procedure AppendRecord(const AValues: array of const);
    procedure Append;
    procedure Insert;
    procedure Delete;
    procedure Edit;
    procedure Post;
    procedure Cancel;
    procedure DeleteAll;
    function  GetChangePacket: IevDataChangePacket;
    function  GetData: IisStream;
    procedure SetData(const AData: IisStream);    
    function  Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
    function  FindKey(const KeyValues:array of const): Boolean;
    function  FieldByName(const AFieldName: String): TField;
    procedure SetRange(const StartValues, EndValues:array of const);
    procedure CancelRange;
    function  IsEvBlobField(const AFieldName: String): Boolean;    
    function  IsBlobLoaded(const AFieldName: String): Boolean;
    function  GetBlobData(const AFieldName: String): IisStream;
    procedure UpdateBlobData(const AFieldName: String; const AData: IisStream); overload;
    procedure UpdateBlobData(const AFieldName: String; const AData: String); overload;
    property  Fields: TFields read GetFields;
    property  Bof: Boolean read GetBof;
    property  Eof: Boolean read GetEof;
    property  FieldCount: Integer read GetFieldCount;
    property  FieldValues[const FieldName: string]: Variant read GetFieldValue write SetFieldValue; default;
    property  RecordCount: Integer read GetRecordCount;
    property  RecNo: Integer read GetRecNo write SetRecNo;
    property  IndexFieldNames:string read GetIndexFieldNames write SetIndexFieldNames;
    property  Filter: string read GetFilter write SetFilter;
    property  Filtered: Boolean read GetFiltered write SetFiltered;
    property  BlobsLoadMode: TevBlobsLoadMode read GetBlobsLoadMode write SetBlobsLoadMode;
    property  Data: IisStream read GetData write SetData;
  end;


  // Holder for all dynamic modules
  IevModuleRegister = interface
  ['{5E41EA62-8E80-47E0-99ED-90542A04A656}']
    procedure  RegisterModule(const AFactoryEntry: PCreateModuleInstance; const AType: TGUID; const AModuleName: String);
    function   CreateModuleInstance(const AModuleType: TGUID; const NoError: Boolean = False): IInterface; overload;
    function   CreateModuleInstance(const AModuleName: String; const NoError: Boolean = False): IInterface; overload;
    function   LoadExternalModule(const AModuleFileName: String): Boolean;
    function   GetModules: IisList;
    function   GetModuleInfo(const AModuleType: TGUID): IevModuleDescriptor; overload;
    function   GetModuleInfo(const AModuleName: String): IevModuleDescriptor; overload;
    function   IsProxyModule(const AModuleInstance: IInterface): Boolean;
  end;


  IevContextManager = interface
  ['{444F554F-F704-43B9-B4A0-4A42EAA1F3E6}']
    function  GetThreadContext: IevContext;
    function  SetThreadContext(const AContext: IevContext): IevContext;
    function  CreateContext(const AUser, APassword: String; const AID: TisGUID = '';
                 const ARemoteID: TisGUID = ''): IevContext;
    function  CopyContext(const ASource: IevContext): IevContext;
    function  CreateThreadContext(const AUser, APassword: String; const AID: TisGUID = '';
                 const ARemoteID: TisGUID = ''): IevContext;
    procedure DestroyThreadContext;
    function  FindContextByID(const AID: TisGUID): IevContext;
    function  FindUserContexts(const AUser: String): IisList;
    function  FindDomainContexts(const ADomain: String = sDefaultDomain): IisList;
    procedure StoreThreadContext;
    procedure RestoreThreadContext;
  end;


  IevPrinterInfo = interface
  ['{15C1DCBB-7590-4896-ADDD-8F5D8BC90304}']
    function  GetName: String;
    function  GetVOffset: Integer;
    procedure SetVOffset(const AValue: Integer);
    function  GetHOffset: Integer;
    procedure SetHOffset(const AValue: Integer);
    property  Name: String read GetName;
    property  VOffset: Integer read GetVOffset write SetVOffset;
    property  HOffset: Integer read GetHOffset write SetHOffset;
  end;

  IevPrintersList = interface
  ['{8305EFFC-3CF6-4052-9294-A8C7110BF95B}']
    procedure Lock;
    procedure Unlock;
    function  Count: Integer;
    function  GetItem(Index: Integer): IevPrinterInfo;
    procedure Load;
    procedure Save;
    function  FindPrinter(const AName: String): IevPrinterInfo;
    property  Items[Index: Integer]: IevPrinterInfo read GetItem; default;
  end;


  IevMachineInfo = interface
  ['{17478026-EE9D-48E4-8BC3-A8CE1AD9339D}']
    function Name: String;
    function ID: Cardinal;
    function IPaddress: String;
    function CipherKey: String;
    function Printers:  IevPrintersList;
  end;


  IevVMRPrinterBinInfo = interface
  ['{DA7C337D-720A-43AD-8FCA-15C62365200B}']
    function  GetName: String;
    procedure SetName(const AValue: String);
    function  GetSbPaperNbr: Integer;
    procedure SetSbPaperNbr(const AValue: Integer);
    function  GetMediaType: String;
    procedure SetMediaType(const AValue: String);
    function  GetPaperWeight: Double;
    procedure SetPaperWeight(const AValue: Double);
    property  Name: String read GetName write SetName;
    property  SbPaperNbr: Integer read GetSbPaperNbr write SetSbPaperNbr;
    property  MediaType: String read GetMediaType write SetMediaType;
    property  PaperWeight: Double read GetPaperWeight write SetPaperWeight;
  end;


  IevVMRPrinterInfo = interface
  ['{CE93FE6C-5440-46ED-A6E4-C2735B81C49A}']
    function  GetName: String;
    procedure SetName(const AValue: String);
    function  GetTag: String;
    procedure SetTag(const AValue: String);
    function  GetLocation: String;
    procedure SetLocation(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetVOffset: Integer;
    procedure SetVOffset(const AValue: Integer);
    function  GetHOffset: Integer;
    procedure SetHOffset(const AValue: Integer);
    function  BinCount: Integer;
    function  FindBin(const AName: String): IevVMRPrinterBinInfo;
    function  FindBinByMediaType(const AMediaType: String): IevVMRPrinterBinInfo;
    function  FindBinNameByMediaType(const AMediaType: String): String;
    function  GetBin(Index: Integer): IevVMRPrinterBinInfo;
    function  AddBin: IevVMRPrinterBinInfo;
    procedure DeleteBin(const ABin: IevVMRPrinterBinInfo);
    property  Name: String read GetName write SetName;
    property  Tag: String read GetTag write SetTag;
    property  Location: String read GetLocation write SetLocation;
    property  Active: Boolean read GetActive write SetActive;
    property  VOffset: Integer read GetVOffset write SetVOffset;
    property  HOffset: Integer read GetHOffset write SetHOffset;
    property  Bins[Index: Integer]: IevVMRPrinterBinInfo read GetBin;
  end;


  IevVMRPrintersList = interface
  ['{190AAD49-7B6A-49EB-BE4D-02FA636692C7}']
    function  Count: Integer;
    function  GetItem(Index: Integer): IevVMRPrinterInfo;
    function  FindPrinter(const AName: String): IevVMRPrinterInfo;
    function  Add: IevVMRPrinterInfo;
    procedure Delete(const AItem: IevVMRPrinterInfo);
    property  Items[Index: Integer]: IevVMRPrinterInfo read GetItem; default;
  end;


  // Log File
  IevLogFile = interface(ILogFile)
  ['{AAE12E33-6169-44C3-8C65-7729A11A2309}']
    procedure AddContextEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String);
    function  GetEMailNotifierActive: Boolean;
    procedure SetEMailNotifierActive(const AValue: Boolean);
    property  EMailNotifierActive: Boolean read GetEMailNotifierActive write SetEMailNotifierActive;
  end;

  IevTrafficDumping = interface
  ['{342FF8E1-1167-4E2B-95E8-872CD98EF058}']
    function  GetDumpTraffic: Boolean;
    function  GetDumpFileName: String;
    procedure StartDumping(const ADumpFileName: String);
    procedure StopDumping;
  end;

  IevTCPClient = interface(IisTCPClient)
  ['{422BE415-0EE9-44C6-899D-255ED9BE16A8}']
    procedure Logoff;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  GetHost: String;
    function  GetPort: String;
    procedure SetHost(const AValue: String);
    procedure SetPort(const AValue: String);
    function  GetEncryptionType: TevEncryptionType;
    procedure SetEncryptionType(const AValue: TevEncryptionType);
    function  GetCompressionLevel: TisCompressionLevel;
    procedure SetCompressionLevel(const AValue: TisCompressionLevel);
    function  GetArtificialLatency: Integer;
    procedure SetArtificialLatency(const AValue: Integer);
    function  Session: IevSession;
    function  ConnectedHost: String;
    procedure SetCreateConnectionTimeout(const AValue: Integer);
    property  Host: String read GetHost write SetHost;
    property  Port: String read GetPort write SetPort;
    property  EncryptionType: TevEncryptionType read GetEncryptionType write SetEncryptionType;
    property  CompressionLevel: TisCompressionLevel read GetCompressionLevel write SetCompressionLevel;
    property  ArtificialLatency: Integer read GetArtificialLatency write SetArtificialLatency;
  end;


  // Account Security Descriptor

  IevSecStateInfo = interface
  ['{ECD06E42-92EE-4EF5-9325-1A99E355E239}']
    function  GetState: TevSecElementState;
    function  GetFromGroup: Boolean;
    procedure SetState(const AValue: TevSecElementState);
    procedure SetFromGroup(const AValue: Boolean);
    property  State: TevSecElementState read GetState write SetState;
    property  FromGroup: Boolean read GetFromGroup write SetFromGroup;
  end;


  IevSecElements = interface
  ['{738323A5-3C9F-4481-8E49-460B260F6E89}']
    function  AccountID: String;
    function  Count: Integer;
    procedure ClearModified;
    function  Modified: Boolean;
    procedure Clear;
    procedure BeginChange;
    procedure EndChange;
    function  AddElement(const AElementName: String): Integer;
    function  IndexOf(const AElementName: String): Integer;
    function  GetElementByIndex(const AIndex: Integer): String;
    procedure SetStateByIndex(const AIndex: Integer; const AState: TevSecElementState; const AFromGroup: Boolean);
    function  GetStateInfo(const AIndex: Integer): IevSecStateInfo; overload;
    function  GetStateInfo(const AElementName: String): IevSecStateInfo; overload;
    function  GetState(const AElementName: String): TevSecElementState;
    procedure SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
  end;


  IevSecFieldInfo = interface(IevSecStateInfo)
  ['{E0F9B49D-6A5F-4063-BD74-5EBD5B07C1C6}']
    function  GetFieldType: String;
    function  GetFieldSize: Integer;
    procedure SetFieldType(const AValue: String);
    procedure SetFieldSize(const AValue: Integer);
    property  FieldType: String read GetFieldType write SetFieldType;
    property  FieldSize: Integer read GetFieldSize write SetFieldSize;
  end;

  IevSecFields = interface(IevSecElements)
  ['{504A4608-9914-47CF-9E2F-9FAD001FCD95}']
    function GetSecFieldInfo(const AIndex: Integer): IevSecFieldInfo; overload;
    function GetSecFieldInfo(const ATableName, AFieldName: String): IevSecFieldInfo; overload;
  end;


  IevSecClients = interface(IevSecElements)
  ['{15CA8F19-B295-4E63-B158-69A03B9D7B19}']
    function IsClientEnabled(const AClientID: Integer): Boolean;
  end;


  IevSecRecordInfo = interface(IevSecStateInfo)
  ['{1B0CBF6E-8909-4B25-A3F9-579EA7CC37A1}']
    function  GetDBType: TevDBType;
    function  GetClientID: Integer;
    function  GetFilterType: String;
    function  GetFilter: String;
    procedure SetDBType(const AValue: TevDBType);
    procedure SetClientID(const AValue: Integer);
    procedure SetFilterType(const AValue: String);
    procedure SetFilter(const AValue: String);
    property  DBType: TevDBType read GetDBType write SetDBType;
    property  ClientID: Integer read GetClientID write SetClientID;
    property  FilterType: String read GetFilterType write SetFilterType;
    property  Filter: String read GetFilter write SetFilter;
  end;


  IevSecRecords = interface(IevSecElements)
  ['{DC23DD70-153E-46D1-AFCA-25566FF207B1}']
    function GetSecRecordInfo(const AIndex: Integer): IevSecRecordInfo;
    function IsCompanyEnabled(const AClientID: Integer; const ACompanyNbr: Integer): Boolean;
    function IsDivisionEnabled(const AClientID: Integer; const ADivisionNbr: Integer): Boolean;
    function IsBranchEnabled(const AClientID: Integer; const ABranchNbr: Integer): Boolean;
    function IsDepartmentEnabled(const AClientID: Integer; const ADepartmentNbr: Integer): Boolean;
    function IsTeamEnabled(const AClientID: Integer; const ATeamNbr: Integer): Boolean;
  end;


  IevSecAccountRights = interface
  ['{62DB9B90-9526-4B52-9438-ACFAE7C186D4}']
    function  AccountID: String;
    function  Level: TevSecurityLevel;
    function  Menus: IevSecElements;
    function  Screens: IevSecElements;
    function  Functions: IevSecElements;
    function  Clients: IevSecClients;
    function  Fields: IevSecFields;
    function  Records: IevSecRecords;
    function  GetElementState(const AElementType: Char; const AElementName: String): TevSecElementState;
    procedure SetElementState(const AElementType: Char; const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
  end;


  // User account
  IevUserAccount = interface
  ['{4A81EF8F-260C-43EC-BE56-FA8D7BDB6E4B}']
    function  AccountID: String;
    function  InternalNbr: Integer;
    function  Domain: String;
    function  User: String;
    function  Password: String;
    function  AccountType: TevUserAccountType;
    function  FirstName: String;
    function  LastName: String;
    function  EMail: String;
    function  SBAccountantNbr: Integer;
    function  PasswordChangeDate: TDateTime;
  end;


  // Context modules
  IevDataAccess = interface
  ['{F57F7D0F-79AD-4B34-9300-F32E5E30D434}']
    procedure StartNestedTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
    function  CommitNestedTransaction: IisValueMap;
    procedure RollbackNestedTransaction;
    procedure OpenDataSets(const ADatasets: array of TevClientDataSet);
    procedure PostDataSets(const ADatasets: array of TevClientDataSet; const AResolveCircularReferences: Boolean = False);
    procedure CancelDataSets(const DS: array of TevClientDataSet);
    procedure Activate(const DS: array of TevClientDataSet);
    function  GetDataSet(const ddTableClass: TddTableClass): TddTable;
    function  GetCachedDataSet(const ClassIndex: Integer): TddTable;
    function  GetHolder: TComponent;
    procedure OpenClient(const AClientNbr: Integer);
    function  ClientID: Integer;
    procedure ClearCache(const ADBType: TevDBTypes = []);
    procedure ClearCachedData;
    function  GetAsOfDate: TisDate;
    procedure SetAsOfDate(const AValue: TisDate);
    procedure UnlockPR;
    procedure LockPR(const AllOK: Boolean);
    procedure LockPRSimple;
    procedure UnlockPRSimple;
    function  PayrollLocked: Boolean;
    function  QecRecommended(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    procedure DoOnQecFinished(const CoNbr: Integer; const CheckDate: TDateTime);
    function  GetCheckMaxAmountAndHours: Boolean;
    procedure SetCheckMaxAmountAndHours(const AValue: Boolean);
    function  GetRecalcLineBeforePost: Boolean;
    procedure SetRecalcLineBeforePost(const AValue: Boolean);
    function  GetNewBatchInserted: Boolean;
    procedure SetNewBatchInserted(const AValue: Boolean);
    function  GetValidate: Boolean;
    procedure SetValidate(const AValue: Boolean);
    function  GetPayrollIsProcessing: Boolean;
    procedure SetPayrollIsProcessing(const AValue: Boolean);
    function  GetImporting: Boolean;
    procedure SetImporting(const AValue: Boolean);
    function  GetCopying: Boolean;
    procedure SetCopying(const AValue: Boolean);
    function  GetNewCheckInserted: Boolean;
    procedure SetNewCheckInserted(const AValue: Boolean);
    function  GetCheckCalcInProgress: Boolean;
    procedure SetCheckCalcInProgress(const AValue: Boolean);
    function  GetTemplateNumber: Integer;
    procedure SetTemplateNumber(const AValue: Integer);
    function  GetNextInternalCheckNbr: Integer;
    procedure SetNextInternalCheckNbr(const AValue: Integer);
    function  GetLastInternalCheckNbr: Integer;
    procedure SetLastInternalCheckNbr(const AValue: Integer);
    function  GetNextInternalCheckLineNbr: Integer;
    procedure SetNextInternalCheckLineNbr(const AValue: Integer);
    function  GetLastInternalCheckLineNbr: Integer;
    procedure SetLastInternalCheckLineNbr(const AValue: Integer);
    function  GetVoidingAgencyCheck: Boolean;
    procedure SetVoidingAgencyCheck(const AValue: Boolean);
    function  GetDeletingVoidCheck: Boolean;
    procedure SetDeletingVoidCheck(const AValue: Boolean);
    function  GetTempCommitDisable: Boolean;
    procedure SetTempCommitDisable(const AValue: Boolean);
    function  GetEESchedEDChanged: string;
    procedure SetEESchedEDChanged(const AValue: string);
    function  GetSkipPrCheckPostCheck: Boolean;
    procedure SetSkipPrCheckPostCheck(const AValue: Boolean);
    function  GetCheckACHstatus: Boolean;
    function  GetPrCheckLineLocations: IevDataset;
    function  GetPrCheckLineLocationsM: IevDataset;
    function  GetSQLite: ISQLite;
    procedure SetSQLite(const Value: ISQLite);
    procedure AddTOAForEE;
    procedure AddEdsForEE;
    procedure SetCheckACHstatus(const AValue: Boolean);
    procedure OpenEEDataSetForCompany(aDataSet: TevClientDataSet; CoNbr: Integer; const Condition: string = '');
    procedure CheckCompanyLock(const CoNbr: Integer = 0; dAsOfDate: TDateTime = 0; const DoNotReOpen: Boolean = False; CheckForTaxPmtsFlag: boolean = False);
    procedure GetCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetSyCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetSbCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetTmpCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    function  CUSTOM_VIEW: TevClientDataSet;
    function  SY_CUSTOM_VIEW: TevClientDataSet;
    function  SB_CUSTOM_VIEW: TevClientDataSet;
    function  TEMP_CUSTOM_VIEW: TevClientDataSet;
    function  HISTORY_VIEW: TevClientDataSet;
    function  GetCustomViewByddDatabase(const ddDatabaseClass: TddDatabaseClass): TevClientDataSet;
    function  DelayedCommit: Boolean;
    procedure LocateCheckReport(const CheckForm: String; const RegularCheck: Boolean);
    procedure FillSBCheckList(aRegularChecks: TStrings; aMiscChecks: TStrings);
    procedure UnlinkLiabilitiesFromDeposit(const d: TEvClientDataSet; const AFilter: string);
    function  TaxPaymentRollback(const SbTaxPaymentNbr, i : Integer): Boolean;
    function  CheckSbTaxPaymentstatus(const SbTaxPaymentNbr : integer): Boolean;
    function  GetClCoReadOnlyRight: Boolean;
    function  GetClReadOnlyRight: Boolean;
    function  GetCoReadOnlyRight: Boolean;
    function  GetEEReadOnlyRight: Boolean;
    procedure SetPayrollLockInTransaction(const APrNbr: Integer; const AUnlocked: Boolean);
    function  GetIsForTaxCalculator: Boolean;
    procedure SetIsForTaxCalculator(const Value: Boolean);

    property  AsOfDate: TisDate read GetAsOfDate write SetAsOfDate;
    property  CheckMaxAmountAndHours: Boolean read GetCheckMaxAmountAndHours write SetCheckMaxAmountAndHours;
    property  RecalcLineBeforePost: Boolean read GetRecalcLineBeforePost write SetRecalcLineBeforePost;
    property  NewBatchInserted: Boolean read GetNewBatchInserted write SetNewBatchInserted;
    property  Validate: Boolean read GetValidate write SetValidate;
    property  PayrollIsProcessing: Boolean read GetPayrollIsProcessing write SetPayrollIsProcessing;
    property  Importing: Boolean read GetImporting write SetImporting;
    property  Copying: Boolean read GetCopying write SetCopying;
    property  NewCheckInserted: Boolean read GetNewCheckInserted write SetNewCheckInserted;
    property  CheckCalcInProgress: Boolean read GetCheckCalcInProgress write SetCheckCalcInProgress;
    property  TemplateNumber: Integer read GetTemplateNumber write SetTemplateNumber;
    property  NextInternalCheckNbr: Integer read GetNextInternalCheckNbr write SetNextInternalCheckNbr;
    property  LastInternalCheckNbr: Integer read GetLastInternalCheckNbr write SetLastInternalCheckNbr;
    property  NextInternalCheckLineNbr: Integer read GetNextInternalCheckLineNbr write SetNextInternalCheckLineNbr;
    property  LastInternalCheckLineNbr: Integer read GetLastInternalCheckLineNbr write SetLastInternalCheckLineNbr;
    property  VoidingAgencyCheck: Boolean read GetVoidingAgencyCheck write SetVoidingAgencyCheck;
    property  DeletingVoidCheck: Boolean read GetDeletingVoidCheck write SetDeletingVoidCheck;
    property  TempCommitDisable: Boolean read GetTempCommitDisable write SetTempCommitDisable;
    property  EESchedEDChanged: String read GetEESchedEDChanged write SetEESchedEDChanged;
    property  SkipPrCheckPostCheck: Boolean read GetSkipPrCheckPostCheck write SetSkipPrCheckPostCheck;
    property  CheckACHstatus: Boolean read GetCheckACHstatus write SetCheckACHstatus;
    property  PrCheckLineLocations: IevDataset read GetPrCheckLineLocations;
    property  PrCheckLineLocationsM: IevDataset read GetPrCheckLineLocationsM;
    property  SQLite: ISQLite read GetSQLite write SetSQLite;
    property  IsForTaxCalculator: Boolean read GetIsForTaxCalculator write SetIsForTaxCalculator;
  end;


  IevDBAccess = interface
    ['{53096510-E7A9-4D9A-8CE9-34BDD595C7D5}']
    //only for local implementation!
    procedure EnableSecurity;
    procedure DisableSecurity;
    procedure SetChangedByNbr(const AValue: Integer = 0);
    function  GetChangedByNbr: Integer;
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
    procedure EnableReadTransaction;
    procedure DisableReadTransaction;
    procedure LockTableInTransaction(const aTable: String; const aCondition: String = '');
    function  PatchDB(const ADBName: String; const AAppVersion: String; AllowDowngrade: Boolean): Boolean;
    function  CustomScript(const ADBName: String; const AScript: String): Boolean;        
    function  SyncDBAndApp(const ADBName: String): Boolean;
    procedure SyncUDFAndApp();    
    procedure ImportDatabase(const ADBPath, ADBPassword: String);
    function  GetDBServerFolderContent(const APath, AUser, APassword: String): IevDataSet;
    function  GetDBFilesList: IevDataSet;
    procedure TidyUpDBFolders;
    procedure ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    procedure UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    function  FullTTRebuildInProgress: Boolean;
    procedure ChangeFBAdminPassword(const APassword, ANewPassword: String);
    procedure ChangeFBUserPassword(const AAdminPassword, ANewPassword: String);
    function  GetDBServersInfo: IisParamsCollection;    
    //

    procedure SetCurrentClientNbr(const AValue: Integer);
    function  GetCurrentClientNbr: Integer;
    procedure StartTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
    function  CommitTransaction: IisValueMap;
    procedure RollbackTransaction;
    function  InTransaction: Boolean;
    function  GetDataSets(const DataSetParams: TGetDataParams): IisStream;
    procedure ApplyDataChangePacket(const APacket: IevDataChangePacket);
    procedure RunInTransaction(const AMethodName: String; const AParams: IisListOfValues);
    function  GetClientsList(const allList: Boolean = false; const fromTT: Boolean = false): string;
    procedure DeleteClientDB(ClientNumber: Integer);
    function  CreateClientDB(const ClDataset : IevDataset): Integer;
    function  OpenQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean = True): IevDataSet;
    procedure ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues);
    function  ExecStoredProc(const AProcName: String; const AParams: IisListOfValues): IisListOfValues;
    function  GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
    procedure SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
    function  GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;
    procedure BeginRebuildAllClients;
    procedure RebuildTemporaryTables(const aClientNbr: Integer; const aAllClientsMode: Boolean; const aTempClientsMode: Boolean = False);
    procedure EndRebuildAllClients;
    procedure CleanDeletedClientsFromTemp;
    function  GetDBVersion(const ADBType: TevDBType): String;
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    function  BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompression: Boolean): IisStream;
    procedure RestoreDB(const ADBName: String; const AData: IevDualStream; const ACompressed: Boolean);
    procedure SweepDB(const ADBName: String);
    procedure DropDB(const ADBName: String);
    procedure UndeleteClientDB(const AClientID: Integer);    
    function  GetLastTransactionNbr(const ADBType: TevDBType): Integer;
    function  GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
    function  GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
    procedure ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
    function  GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IisStream;
    procedure CheckDBHash(const ADBType: TevDBType; const AData: IisStream; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
    procedure SetADRContext(const AValue: Boolean);
    function  GetADRContext: Boolean;
    function  GetClientROFlag: Char;
    procedure SetClientROFlag(const AValue: Char);
    function  GetFieldValues(const ATable, AField: String; const ANbr: Integer; const AsOfDate: TDateTime; const AEndDate: TDateTime; const AMultiColumn: Boolean): IisListOfValues;
    function  GetFieldVersions(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ASettings: IisListOfValues = nil): IevDataSet;
    procedure UpdateFieldVersion(const ATable: String; const ANbr: Integer; const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TDateTime;
                                 const AFieldsAndValues: IisListOfValues);
    procedure UpdateFieldValue(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime; const AEndDate: TDateTime; const AValue: Variant);
    function  GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
    function  GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
    function  GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
    function  GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer;} const ABeginDate: TDateTime): IevDataset;
    function  GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
    function  GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
    function  GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
    property  CurrentClientNbr: Integer read GetCurrentClientNbr write SetCurrentClientNbr;
  end;


  IevPayrollCalculation = interface
    ['{1988C1C9-AC7F-4736-9C0C-04D3FE4D7BC2}']
    // from SLiabPeriodCalculations
    function GetFixedAmount(EE_SCHEDULED_E_DS, PR: TevClientDataset; DefaultValue: Variant): Real;
    procedure GetFedTaxDepositPeriod(Frequency: Char; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    function GetFedTaxDepositFreq(CoNbr: Integer): string;
    procedure GetTaxDepositPeriodFromDataSet(const ds: TDataSet; const Date: TDateTime; const SY_AGENCY_NBR: Integer; var BeginDate, EndDate, DueDate: TDateTime; const TaxState: string);    
    procedure GetStateTaxDepositPeriod(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    procedure GetStateTaxDepositPeriodAndSeqNumber(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
      TDateTime; out SeqNumber: Integer);
    procedure GetLocalTaxDepositPeriodAndSeqNumber(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
      TDateTime; out SeqNumber: Integer);
    function LiabilityACH_KEY_CalcMethod(TaxTypeDesc : char; DepFreqNbr, SyNbr : Integer;
      const PrCheckDate:TDateTime; const calcEndDate:TDateTime = 0): String;
    function GetStateTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
    procedure GetLocalTaxDepositPeriod(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    function GetLocalTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
    function GetSequenceNumber(const Date: TDateTime; const Freq: Char = FREQUENCY_TYPE_QUARTERLY): Integer;
    procedure GetSuiTaxDepositPeriod(SySuiNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    // from SPayrollCalculations
    function CheckDueDateForHoliday(DueDate: TDateTime; const GlAgencyNbr: Integer): TDateTime;
    function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDueDate, PeriodEndDate: TDateTime): TDateTime; overload;
    function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime; overload;
    function CheckForReversedNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
    function CheckIfBlockCheckLineOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet): Boolean;
    procedure BlockEverythingOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet; WithTransaction: Boolean);
    function NextPeriodBeginDate: TDateTime;
    function NextPeriodEndDate: TDateTime;
    function NextCheckDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
    function GetNextCheckDate(AfterDate: TDateTime; CompanyFrequency: Char; WithNoPayrollAttached: Boolean = False): TDateTime;
    function NextScheduledCallInDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
    function GetNextPeriodBeginDate(BatchFrequency: Char): TDateTime;
    function GetNextPeriodEndDate(BatchFrequency: Char; PeriodBeginDate: TDateTime): TDateTime;
    function FindNextPeriodBeginEndDate: Boolean;
    function FindNextPeriodBeginEndDateExt(PR, PR_BATCH: TevClientDataSet): Boolean;
    function ListBackdatedPayrollProblems(CheckDate: TDateTime): string;
    function HolidayDay(Day: TDateTime; BankOnly: Boolean = False): string;
    function IsNonBusinessDay(Day: TDateTime): Boolean;
    function GetNextRunNumber(CheckDate: TDateTime): Integer;
    function GetCompanyPayFrequencies: String;
    function FrequencyBelongs(OneFrequency, MultipleFrequencies: string): Boolean;
    function GetCustomBankAccountNumber(PR_CHECK: TevClientDataSet): string;
    function GetABANumber: string;
    function ConstructCheckSortIndex: string;
    function GetBankAccountNumber(PR_CHECK: TevClientDataSet; var AccountNbr: Integer): Boolean;
    function GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS: TevClientDataSet; var AccountNbr: Integer): Boolean;
    function GetHomeDBDTFieldValue(FieldName, Level: string; EmployeeNbr: Integer;
      CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE: TevClientDataSet; UsedByServer: Boolean): Variant;
    procedure Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES,
      CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet; DoPrePost, PaySalary, PayHourly: Boolean;
      RefreshScheduledEDs: Boolean = False; CreatingNewBatch: Boolean = False; const DontPost: Boolean = False);
    procedure AddExtraCheckLines(PR_CHECK_LINES: TevClientDataSet);
    procedure CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet);
    procedure CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, CL_E_D_GROUP_CODES, CL_PERSON, CL_PIECES, CO_SHIFTS,
      CO_STATES, EE, EE_SCHEDULED_E_DS, EE_RATES, EE_STATES, EE_WORK_SHIFTS, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet;
     LogEvent :TOnPrCheckLinesLogMessage = nil);
    procedure CopyPayroll(OldPayrollNbr: Integer);
    procedure VoidPayroll(OldPayrollNbr: Integer);
    procedure GetLimitedTaxAndDedYTD(PayrollNbr, CheckNbr, EENbr, RecordNbr: Integer; TaxType: TLimitedTaxType; BeginDate, EndDate: TDateTime;
      PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; IgnoreCurrentPayroll: Boolean = False; ForConsolidatesOnly: Boolean = False);
    procedure GetLimitedTaxAndDedYTDforGTN(CheckNbr, RecordNbr: Integer; TaxType: TLimitedTaxType; PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; StartDate: TDateTime = 0; EndDate: TDateTime = 0; IgnoreCurrentPayroll: Boolean = False);
    function ScheduledEDRecalc(EDType: string; Gross, Net: Real; PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, CL_E_DS, CL_PENSION, CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE,
      SY_STATES: TevClientDataSet; JustPreProcessing: Boolean = True; ProrateNbr: Integer = 1; ProrateRatio: Real = 1; OldNet: Real = 0): Boolean;
    function GetCompanyED(EDType: string): Integer;
    function RecalcRateForED(EDCodeType: string): Boolean;
    procedure DoRecalcRate(PR_CHECK, PR_CHECK_LINES, CL_E_DS, CO, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS, EE,
      EE_RATES: TevClientDataSet; UsedByServer: Boolean);
    procedure FillInDBDTBasedOnRate(EERateNbr: Integer; PR_CHECK_LINES, EE_RATES: TevClientDataSet);
    procedure CalculateOvertime(DataSet: TevClientDataSet; EDType: string; EE_NBR: Integer);
    procedure CalculateMultipleOvertime(DataSet: TevClientDataSet; EE_NBR: Integer);
    function AnalyzeTargets(EE, EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK: TevClientDataSet; EmployeeNbr: Integer; JustPreProcessing: Boolean; AdjustBalanceOnly: Boolean = False): Boolean;
    function CheckDDPrenote(SchedEDNbr: Integer; CheckDate: TDateTime): Boolean;
    procedure AddToBankAccountRegister(
                const CoNbr, BankAccountNbr, TaxDepositNbr : integer;
                  const Amount : currency;
                   const Status, BankRegisterType : Char;
                    const CheckDate, DueDate, ProcessDate : TDateTime;
                     out BankAccRegNbr : Integer);
    procedure AddConsolidatedPaymentsToBankAccountRegister(
               LiabilityCoTable: TevClientDataset;
                PayingCompanyNbr, TaxDepositNbr : integer;
                 const Status, BankRegisterType : Char);
    procedure AddConsolidatedPaymentsToBar(
                    DS: array of TevClientDataSet;
                     const TaxDepositNbr: Integer;
                      const Status, BankRegisterType : char;
                      const bDepNbrFilter : Boolean = True);
    procedure UpdateAllLiabilityTablesStatus(TaxDepositNbr: Integer);
    procedure AssignCheckDefaults(TemplateNbr, PaymentSerialNbr: Integer);
    procedure CopyPRTemplate(TemplateNumber: Integer);
    procedure CopyPRTemplateDetails(TemplateNumber: Integer; PR_CHECK, PR_CHECK_STATES, PR_CHECK_LOCALS,
      CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS: TevClientDataSet);
    // From DM_PAYROLL
    function GetTempCheckLinesFieldValue(FieldName: string): Variant;
    procedure AssignTempCheckLines(DS: TevClientDataSet);
    procedure ReleaseTempCheckLines;
    function GetNextPaymentSerialNbr: Integer;
    // from SGrossToNet
    function GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction: Boolean; JustPreProcess: Boolean;
      FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
    procedure GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
      PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: TevClientDataSet; DisableYTDs, DisableShortfalls, NetToGross: Boolean);
    // new stuff
    procedure GetLimitedEDs(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
    procedure EraseLimitedEDs;
    procedure GetLimitedTaxes(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
    procedure EraseLimitedTaxes;
    procedure OpenDetailPayroll(PrNbr: Integer);
    procedure RefreshScheduledEDsForBatch(const BatchNbr: Integer; const NoManuals: Boolean = False);
    function CalcWorkersComp(PrNbr: Integer; sLevel: string = 'CO_NBR'; dds: TevClientDataSet = nil; const wcompNbr: integer = 0;FromMainProcessing:Boolean = False ): Currency;
//    EEOnly = 0 - all taxes
//    EEOnly = 1 - EE taxes only
//    EEOnly = 2 - ER taxes only
    procedure GetDistrTaxes(PrNbr: Integer; var cdsTaxDistr: TevClientDataSet; EEOnly: Integer = 0);
    procedure SetStateTax(NewStateTax: Double);
    procedure AttachPayrollToCalendar(PR: TevClientDataSet);
    function  DM_HISTORICAL_TAXES: TDataModule;
    procedure SetEeFilter(const AFilter, ACaption: string);
    function  GetEeFilter: string;
    function  GetEeFilterCaption: string;
    procedure ProcessFeedback(const AShow: Boolean; const ADisplayString: String = '');
    function  GetLastFeedBack: string;
  end;


  IevPayrollCheckPrint = interface
  ['{63867E11-C839-4DB7-B785-0BAA64EF8321}']
    procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckType: TCheckType;
      CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
      CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
      HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);
    function CheckStubBlobToDatasets(const BlobData: string): variant;
  end;

  TevEEChangeRequestType = (tpHRChangeRequest, tpBenefitRequest);

  IevEEChangeRequest = interface
  ['{6B50B1EA-2A4A-47CF-8C9D-4DD6F4191883}']
    function  GetEENbr: integer;
    function  GetCustomEENumber: String;
    procedure SetCustomEENumber(const ACustomEENumber: String);
    function  GetEEFirstName: String;
    procedure SetEEFirstName(const AEEFirstName: String);
    function  GetEELastName: String;
    procedure SetEELastName(const AEELastName: String);
    function  GetRequestData: IevDataChangePacket;
    procedure SetRequestData(const ARequestData: IevDataChangePacket);
    function  GetEEChangeRequestNbr: integer;
    procedure SetEEChangeRequestNbr(const AEEChangeRequestNbr: integer);
    function  GetEEChangeRequestType: TevEEChangeRequestType;
    procedure SetEEChangeRequestType(const AValue: TevEEChangeRequestType);
    function  GetOldFormatRequestData: String;
    procedure SetOldFormatRequestData(const AValue: String);

    property  EENbr: integer read GetEENbr;
    property  CustomEENumber: String read GetCustomEENumber write SetCustomEENumber;
    property  EEFirstName: String read GetEEFirstName write SetEEFirstName;
    property  EELastName: String read GetEELastName write SetEELastName;
    property  EEChangeRequestNbr: integer read GetEEChangeRequestNbr write SetEEChangeRequestNbr;
    property  EEChangeRequestType: TevEEChangeRequestType read GetEEChangeRequestType write SetEEChangeRequestType;
    property  RequestData: IevDataChangePacket read GetRequestData write SetRequestData;
    property  OldFormatRequestData: String read GetOldFormatRequestData write SetOldFormatRequestData;
  end;

  IevRemoteMiscRoutines = interface
  ['{82B98C91-D4B0-4154-9C3B-AD26FB4C41A0}']
    function  PreProcessForQuarter(const Cl_NBR, CO_NBR: Integer; const ForceProcessing, Consolidation: Boolean;
                                   const BegDate, EndDate: TDateTime; const QecCleanUpLimit: Currency;
                                   const TaxAdjLimit: Currency; const PrintReports: boolean): IisListOfValues;
    function  PayTaxes(const FedTaxLiabilityList, StateTaxLiabilityList, SUITaxLiabilityList, LocalTaxLiabilityList: string;
                      const ForceChecks: Boolean; const EftpReference: string; const SbTaxPaymentNbr : Integer;
                      const PostProcessReportName: String): IisStream;
    function  PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;
    function  FinalizeTaxPayments(const Params: IisStream; const AACHOptions: IisStringList;
      const SbTaxPaymentNbr : Integer; const PostProcessReportName, DefaultMiscCheckForm: String): IisStream;

    // former API DoInvoke
    function CalculateTaxes(const Params: IisStream): IisStream;
    function  GetSystemAccountNbr: Integer;
    function CalculateChecklines(const Params: IisStream): IisStream;
    function RefreshEDS(const Params: IisStream): IisStream;
    function CreateCheck(const Params: IisStream): IisStream;
    function CreateManualCheck(const Params: IisStream): IisStream;
    function TCImport(const Params: IisStream): IisStream;
    function GetW2(const Params: IisStream; const Param : String): IisStream;
    function ChangeCheckStatus(const Params: IisStream): IisStream;
    function HolidayDay(const Params: IisStream): IisStream;
    function NextPayrollDates(const Params: IisStream): IisStream;
    function ReportToPDF(const Params: IisStream): IisStream;
    function AllReportsToPDF(const Params: IisStream): IevDataset;

    function  RunReport(const aReportNbr: integer; const AParams: IisListOfValues): String;
    function  GetPDFReport(const aTaskId: string): IisStream;
    function  GetAllPDFReports(const aTaskId: string): IevDataset;
    function  GetTaskList: IEvDataSet;
    function  GetSysReportByNbr(const aReportNbr: integer): IisStream;
    function  GetReportFileByNbrAndType(const aReportNbr: integer; const aReportType: string): IisStream;

    function GetEEW2List(const pEENBR: Integer): IevDataset;
    function GetEEScheduled_E_DS(const eeList: string): IevDataset;
    procedure CalcEEScheduled_E_DS_Rate(const aCO_BENEFIT_SUBTYPE_NBR, aCL_E_DS_NBR: Integer;
                                        const aBeginDate, aEndDate: TDateTime;
                                        out aRate_Type: String; out aValue: Variant); overload;
    procedure CalcEEScheduled_E_DS_Rate(const aEE_BENEFITS_NBR, aCL_E_DS_NBR: Integer;
                                      const aBeginDate, aEndDate: TDateTime;    
                                      const aAMOUNT, aANNUAL_MAXIMUM_AMOUNT, aTARGET_AMOUNT: variant;
                                      const aFREQUENCY:string;
                                      out aRate_Type: String; out aValue: Variant); overload;
    function GetSBEmail(const aCoNbr: integer): String;
    function GetWCReportList(const aCoNbr: integer; const AReportNbrs: IisStringList): IevDataset;
    procedure GetPayrollTaxTotals(const PrNbr: Integer; var Totals: Variant);
    procedure GetPayrollEDTotals(const PrNbr: Integer; var Totals: Variant;
      var Checks: Integer; var Hours, GrossWages, NetWages: Currency);
    procedure  DeleteRowsInDataSets(const AClientNbr: Integer; const ATableNames, AConditions: IisStringList;
      ARowNbrs: IisListOfValues);
    function  GetCoCalendarDefaults(const ACoNbr: integer): IisListOfValues;
    function  CreateCoCalendar(const ACoNbr: integer; const ACoSettings: IisListOfValues; const ABasedOn, AMoveCheckDate, AMoveCallInDate, AMoveDeliveryDate: String): IisStringList;
    procedure SetEeEmail(const AEENbr: integer; const ANewEMail: String);
    // new RW calls for Ad-Hoc Web reporting
    function StoreReportParametersFromXML(const Co_nbr, Co_Reports_Nbr: integer; const XMLParameters: String): integer;
    function GetCoReportsParametersAsXML(const Co_nbr: integer; const Co_Reports_Nbr: integer): string;
    function ConvertXMLtoRWParameters(const sXMLParameters: AnsiString):IIsStream;

    function  GetManualTaxesListForCheck(const APrCheckNbr: integer): IevDataset;

    // API billing
    procedure SaveAPIBilling(const Params: IisListOfValues);
    function GetAPIBilling: IevDataSet;

    //TOA requests
    procedure AddTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    procedure UpdateTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    function  GetTOARequestsForEE(const EENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    function  GetTOARequestsForMgr(const MgrEENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    procedure ProcessTOARequests(const Operation: TevTOAOperation; const Notes: String;
      const EETimeOffAccrualOperNbrs: IisStringList; const CheckMgr: boolean = True);
    function  GetTOARequestsForCO(const CoNBR:Integer; const Codes:string; const FromDate, ToDate : TDateTime; const Manager, TOType, EECode: Variant): IisInterfaceList;
    procedure UpdateTOARequestsForCO(const Requests: IisInterfaceList; const CustomCoNbr:string);

    // for EvOX and API
    procedure AddTOAForNewEE(const CoNbr: integer; const EENbr: integer);
    procedure AddEDsForNewEE(const CoNbr: integer; const EENbr: integer);

    // EE change requests (REQUEST_TYPE = 'H')
    procedure CreateEEChangeRequest(const AEEChangeRequest: IevEEChangeRequest);
    function  GetEEChangeRequestList(const AEENbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  GetEEChangeRequestListForMgr(const AMgrNbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  ProcessEEChangeRequests(const ARequestsNbrs: IisStringList; const AOperation: boolean): IisStringList; // Returns numbers of requests which processed with erros

    // EE benefits requests
    function  GetBenefitRequest(const ARequestNbr: integer): IisListOfValues;
    function  StoreBenefitRequest(const AEENbr: integer; const ARequestData: IisListOfValues; const ARequestNbr: integer): integer;
    procedure DeleteBenefitRequest(const ARequestNbr: integer);
    function  GetBenefitRequestList(const AEENbr: integer): IisStringList;
    function  SendEMailToHR(const AEENbr: integer; const ASubject: String; const AMessage: String; const AAttachements: IisListOfValues): IisListOfValues;
    procedure SendEmailToManagers(const AEeNbr: integer; const ARequestType: String; const AMessageDetails: String = '');    
    function  GetEeExistingBenefits(const AEENbr: integer; const ACurrentOnly: boolean = True): IevDataset;
    function  GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer; const ASearchForDate: TDateTime; const ACategory: String;
      const AReturnCoBenefitsDataOnly: boolean): IisListOfValues;
    function  GetSummaryBenefitEnrollmentStatus(const AEENbrList: IisStringList; const AType: String): IevDataset;
    procedure RevertBenefitRequest(const ARequestNbr: integer);
    procedure SubmitBenefitRequest(const ARequestNbr: integer; const ASignatureParams: IisListOfValues);
    function  GetBenefitRequestConfirmationReport(const ARequestNbr: integer;const isResultPDF: boolean=true): IisStream;
    procedure SendBenefitEmailToEE(const AEENbr: integer; const ASubject: String; const AMessage: String;
      const ARequestNbr: integer = -1; const ACategoryType: String = '');
    function  GetEmptyBenefitRequest: IisListOfValues;
    procedure CloseBenefitEnrollment(const AEENbr: integer; const ACategoryType: String; const ARequestNbr: integer = -1);
    procedure ApproveBenefitRequest(const ARequestNbr: integer);
    function  GetBenefitEmailNotificationsList: IisListofValues;

    function  GetDataDictionaryXML: IisStream;

    function DeleteEE(const AEENbr : Integer): boolean;
    
    function GetACAHistory(const aEENbr, aYear : Integer): IEvDataSet;
    function PostACAHistory(const aEENbr, aYear: Integer; const aCd: IEvDataSet): boolean;
    function GetACAOfferCodes(const aCoNbr: integer; Const AsOfDate: TDateTime): IEvDataSet;
    function GetACAReliefCodes(const aCoNbr: integer; Const AsOfDate: TDateTime): IEvDataSet;
  end;


  IevVmrEngine = interface
    ['{24660AC5-B056-43E1-B66E-548E47FC0DFF}']
    procedure VmrPreProcessReportList(const RepList: TrwReportList);
    function  VmrPostProcessReportResult(var ms: IEvDualStream): Boolean; overload;
    function  VmrPostProcessReportResult(const Res: TrwReportResults): Boolean; overload;
    function  VmrStorePostProcessReportResult(const ms: IEvDualStream): Boolean; overload;
    function  VmrStorePostProcessReportResult(const Res: TrwReportResults): Boolean; overload;
    procedure ProcessRelease(const TopBoxNbr: Integer;ReleaseType:TVmrReleaseTypeSet);
    procedure ProcessRevert(const TopBoxNbr: Integer);
    procedure SetChecksExtraInfo(const EInfo:IisStringList);
    function  GetChecksExtraInfo:IisStringList;
  end;


  IevVmrRemote = interface
  ['{BD22CFCC-46C6-4ABE-A139-D8ACE08FE0ED}']
    function  StoreFile(const FileName: string; const Content: IisStream; const AutoRename: Boolean): string;
    function  RetrieveFile(const FileName: string): IisStream;
    function  FileDoesExist(const FileName: string): Boolean;
    procedure DeleteFile(const FileName: string);
    procedure DeleteDirectory(const DirName: string);
    function  CheckScannedBarcode(const BarCode: string; out ExpectedBarcode, LastScannedBarcode: string): Boolean;
    procedure RemoveScannedBarcode(const BarCode: string);
    procedure RemoveScannedBarcodes(const MailBoxBarCode: string);
    function  PutInBoxes(const BarCode, ToBarCode: string): string;
    function  FindInBoxes(const BarCode: string): string;
    procedure RemoveFromBoxes(const BarCode: string);
    function  RetrievePickupSheetsXMLData: IisStream;

    function  GetSBPrinterList : IevVMRPrintersList;
    function  GetPrinterList(const aLocation:string): IevVMRPrintersList;
    procedure SetPrinterList(const aLocation : string; const APrinterList: IevVMRPrintersList;const IsDelete: Boolean);
    procedure PrintRwResults(const ARepResults: TrwReportResults);

    procedure ProcessRelease(const TopBoxNbr: Integer; const ReleaseType:TVmrReleaseTypeSet; out UnprintedReports: TrwReportResults);
    procedure ProcessRevert(const TopBoxNbr: Integer);
    function  GetVMRReportList(const pCLNbr, pCONbr:Integer; const  pCheckFromDate, pCheckToDate:TDateTime; const pVMRReportType : Integer): IevDataset;
    function  GetVMRReport(const aSelectedReportNbrs: IisStringList; const ISPrint : Boolean; const sComment : string): IisStream;
    procedure PurgeMailBox(const BoxNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeClientMailBox(const ClientNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeMailBoxContentList(const SBMailBoxContentNbrs: IisStringList);
    function GetRemoteVMRReportList(const ALocation:string): IevDataset;
    function GetRemoteVMRReport(const SBMailBoxContentNbr: integer): IisStream;
  end;

  
  IevRWDesigner = interface
  ['{047C6BF5-71C9-40C4-A5AE-E5424955310D}']
    procedure  ShowDesigner(AData: IEvDualStream; AReportName: string; var ANotes: string; var AClassName: string; var AAncestorName: string);
    function   ShowQueryBuilder(AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; UserView: Boolean): Boolean;
    procedure  RunQBQuery(AData: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TevClientDataSet);
    function   ReCreateWizardReport(AData: IEvDualStream): Boolean;
  end;


  IevPayrollProcessing = interface
    ['{3B9BE7D4-42C8-4D76-8E4A-02790888CC22}']
    function GetPayrollBatchDefaults(const PrNbr: Integer): IisListOfValues;
    function GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction: Boolean; JustPreProcess: Boolean;
      FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
    procedure TaxCalculator(aPR, aPR_BATCH, aPR_CHECK, aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI, aPR_CHECK_LOCALS: IisStream;
      DisableYTDs, DisableShortfalls, NetToGross: Boolean);      
    function ProcessBilling(prNumber: Integer; ForPayroll: Boolean;
                             InvoiceNumber: Integer; ReturnDS: Boolean): Variant;
    function  DoQuarterEndCleanup(CoNbr: Integer; QTREndDate: TDateTime; MinTax: Double;
                                  CleanupAction: Integer; const ReportFilePath: string;
                                  const EECustomNbr: string; ForceCleanup: Boolean;
                                  PrintReports: Boolean; CurrentQuarterOnly: Boolean;
                                  const LockType: TResourceLockType; ExtraOptions: Variant;
                                  out Warnings: String): Boolean;
    function  DoPAMonthEndCleanup(CoNbr: Integer; MonthEndDate: TDateTime; MinTax: Double;
                                  const AutoProcess: Boolean; const EECustomNbr: string;
                                  const PrintReports: Boolean; const LockType: TResourceLockType;
                                  out Warnings: String): Boolean;
    function ProcessPayroll(PayrollNumber: Integer; JustPreProcess: Boolean;
      out Log: string;
      // Log contains a detailed log of the start times of the steps taken
      // during executing the ProcessPayroll functionality. The format is the
      // same as that of the Text property of a TStringList.
      const CheckReturnQueue: Boolean;
      const LockType: TResourceLockType = rlTryOnce;
      const PrBatchNbr: Integer = 0): String;
    function PrintPayroll(PrNbr: Integer; PrintChecks: Boolean;UseSbMiskCheckForm:boolean=false;DefaultMiskCheckForm:String=''): IisStream;
    function ReprintPayroll(PrNbr: Integer; PrintReports, PrintInvoices, CurrentTOA: Boolean;
      PayrollChecks, AgencyChecks, TaxChecks, BillingChecks, CheckForm, MiscCheckForm: String; CheckDate: TDateTime;
      SkipCheckStubCheck: Boolean; DontPrintBankInfo: Boolean; HideBackground: Boolean; DontUseVMRSettings: Boolean; ForcePrintVoucher: boolean;
      PrReprintHistoryNBR: integer; Reason: String; out AWarnings, AErrors: String): IisStream;
    procedure ReCreateVmrHistory(const CoNbr: Integer; const PeriodBeginDate, PeriodEndDate: TDateTime);
    procedure ReRunPayrolReportsForVmr(const PrNbr: Integer);
    procedure DoERTaxAdjPayroll(aDate: TDateTime);
    procedure DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime; MinLiability: Double);
    procedure CreateTaxAdjCheck(aDate: TDateTime);
    procedure CleanupTaxableTips(PeriodBegin: TDateTime; PeriodEnd: TDateTime);
    function CreatePR(Co_Nbr: Integer; CheckDate: TDateTime): Integer;
    procedure RefreshSchduledEDsCheck(const CheckNbr: Integer);
    function VoidCheck(CheckToVoid: Integer; PrBatchNbr: Integer): Integer;
    procedure UnvoidCheck(CheckNbr: Integer);

    procedure RedistributeDBDT(const PrCheckLineNbr: Integer; Redistribution: IisParamsCollection);

    procedure CreatePRBatchDetails(PrBatchNumber: Integer; TwoChecksPerEE, CalcCheckLines: Boolean; CheckType, Check945Type: String;
      NumberOfChecks, SalaryOrHourly: Integer; EENumbers: Variant;
      const TcFileIn, TcFileOut: IisStream;
      const TcLookupOption: TCImportLookupOption; const TcDBDTOption: TCImportDBDTOption;
      const TcFourDigitYear: Boolean; const TcFileFormat: TCFileFormatOption;
      const TcAutoImportJobCodes: Boolean; const TcUseEmployeePayRates: boolean;
      const UpdateBalance: Boolean; const IncludeTimeOffRequests: Boolean);
    procedure RefreshSchduledEDsBatch(const BatchNbr: Integer; const RegularChecksOnly: Boolean);  
    function  LoadYTD(Cl_Nbr: Integer; Co_Nbr: Integer; Year, EeNbr: Integer): Variant;
    function  GetEEYTD(const AYear: Integer; const AEeNbr: Integer): IisListOfValues;
    function ProcessBillingForServices(ClNbr: Integer; CoNbr: Integer; const List: string; const EffDate: TDateTime; const Notes: string;var bManualACH: Boolean): Real;
    function  IsQuarterEndCleanupNecessary(CoNbr: Integer; QTREndDate: TDateTime): Boolean;
    procedure ProcessPrenoteACH(ACHOptions: IevACHOptions; APrenoteCLAccounts: Boolean; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    procedure ProcessManualACH(ACHOptions: IevACHOptions; bPreprocess: Boolean; BatchBANK_REGISTER_TYPE: String; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    // Payroll routines
    procedure CopyPayroll(const APayrollNbr: Integer);
    procedure VoidPayroll(const APayrollNbr: Integer; Const VoidTaxChecks, VoidAgencyChecks,
                          VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling: Boolean);
    procedure DeletePayroll(const APayrollNbr: Integer);
    function DeleteBatch(const ABatchNbr: Integer): boolean;
    procedure SetPayrollLock(const APrNbr: Integer; const AUnlocked: Boolean);
  end;

  IevCashManagement = interface
    ['{427EFBCB-F1A4-4493-A82E-6ED8D8A670B3}']
    procedure PrepareCashManagement(const Payrolls: IevDataset;
      const Options: IevAchOptions; const SbData: IisListOfValues;
      const Companies: IisParamsCollection);
    procedure ProcessCashManagementForCompany(const IncludeOffsets: Integer;
      const CompanyData: IisListOfValues;  const Options: IevAchOptions;
      const SbData: IisListOfValues);
    procedure FinalizeCashManagement(const Companies: IisListOfValues;
      const Options: IevACHOptions; const SbData: IisListOfValues;
      const IncludeOffsets: Integer; const Preprocess: Boolean;
      var AchFileReport: IisStream; var AchRegularReport: IisStream;
      var AchDetailedReport: IisStream; var NonAchReport: IisStream;
      var WTFile: IisStream; var FileName: String; var AchSave: IisListOfValues;
      var Exceptions: String; var Warnings: String;
      const FromDate, ToDate: TDateTime; const ACHFolder: String);
    procedure UpdateAchStatus(Payrolls: TevClientDataSet; const NewStatusIndex: Integer);
  end;

  IevRWRemoteEngine = interface
    ['{0B028300-F83C-42FE-BDB4-7F24E245C720}']
    function  RunReports(const AReportList: TrwReportList): IEvDualStream;
    function  RunReports2(const AReportList: TrwReportList): TrwReportResults;
    function  RunQBQuery(const AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer): IEvDualStream;
    procedure StopReportExecution;
    procedure SetLowerRWEnginePriority(const AValue: Boolean);
  end;


  IevRWLocalEngine = interface
  ['{F8182AE7-52CD-4140-AA0E-520FC083CE39}']
    function  ReportWriteProxy: IInterface;
    function  StartGroup: Variant;
    function  EndGroup(ADestination: TrwReportDestination; const Descr: string = '';
      const OverrideCoNbr: Integer = 0; const OverridePrNbr: Integer = 0; const OverrideEventDate: TDateTime = 0; const PrReprintHistoryNBR: Integer = 0): IisStream;
    function  EndGroupForTask(ADestination: TrwReportDestination; const Descr: string = ''; const OverrideCoNbr: Integer = 0;
      const OverridePrNbr: Integer = 0; const OverrideEventDate: TDateTime = 0): TisGUID;
    function  CalcPrintReport(const aName: string; const AReportParams: TrwReportParams; const ACaption: String = ''): TrwRepParams; overload;
    function  CalcPrintReport(ANBR: Integer; ALevel: string; AReportParams: TrwReportParams; ACaption: String = ''): TrwRepParams; overload;
    function  CalcPrintReport(const Rep: TrwRepParams): TrwRepParams; overload;
    procedure CompileReport(AData: IEvDualStream; var AClassName: String; var AAncestorClassName: String);
    procedure BeginWork;
    procedure EndWork;
    procedure SetUnprintedReportsStorage(const AValue: TrwReportResults);
    function  ShowInputForm(Report_NBR: Integer; DBType: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
    function  ShowAncestorInputForm(AClassName: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
    function  ShowModalInputForm(Report_NBR: Integer; DBType: string; AParams: TrwReportParams; ARunTime: Boolean = True; AShowWarning: Boolean = True): TModalResult;
    function  GetInputFormBoundRect: TRect;
    function  CloseInputForm(ACloseResult: TModalResult; AParams: TrwReportParams): Boolean;
    function  GetRepAncestor(Report_NBR: Integer; DBType: string): String;
    function  ClassNameToDescr(AClassName: String): string;
    procedure Preview(AResults: TrwReportResults; AParent: TrwPreviewContainer = nil; const ASecuredPreview: Boolean = False); overload;
    procedure Preview(AResults: IisStream); overload;
    procedure Print(AResults: TrwReportResults; const APrinterName: String = ''); overload;
    procedure Print(AResults: IisStream; const APrinterName: String = ''); overload;
    procedure Print(const AQueueTaskID: TisGUID; const APrinterName: String = ''); overload;
    procedure SaveToFile(AResults: TrwReportResults); overload;
    procedure SaveToFile(AResults: IisStream); overload;
    function  ConvertRWAtoPDF(const ARWAData: IEvDualStream; const APDFInfoRec: TrwPDFDocInfo): IEvDualStream;
    procedure LogicalQuery(ASQL: String; AParams: TParams; AResult: TISBasicClientDataSet);
    function  GetLastExceptions: String;
    function  GetLastWarnings: String;
    procedure GetReportResources(Report_NBR: Integer; DBType: String; AData: IEvDualStream);
    function  ConvertOldPreviewFormat(AStream: IEvDualStream): Boolean;
    function  ReportsByAncestor(const AAncestors: array of string; const ALevels: string = 'SBC'; AOwner: TComponent = nil): TevClientDataSet;
    function  RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    procedure MakeSecureRWA(var AData: IEvDualStream; const PreviewPassword, PrintPassword: String);
    procedure RunQBQuery(AData: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TEvBasicClientDataSet; ARemote: Boolean = False);
    function  MergeGraphicReports(AResults: TrwReportResults; const AReportName: string): TrwReportResults;
    function  ReportsOfGroup: TrwReportList;
  end;


  IevMailMessage = interface
  ['{164CFA67-80E9-4E2C-8BB9-06EF4EC37DB3}']
    function  GetAddressFrom: String;
    procedure SetAddressFrom(const AValue: String);
    function  GetAddressTo: String;
    procedure SetAddressTo(const AValue: String);
    function  GetAddressCC: String;
    procedure SetAddressCC(const AValue: String);
    function  GetAddressBCC: String;
    procedure SetAddressBCC(const AValue: String);
    function  GetSubject: String;
    procedure SetSubject(const AValue: String);
    function  GetPriority: TisMailPriority;
    procedure SetPriority(const AValue: TisMailPriority);
    function  GetConfirmation: Boolean;
    procedure SetConfirmation(const AValue: Boolean);
    function  GetBody: String;
    procedure SetBody(const AValue: String);
    procedure AttachFile(const AFileName: String);
    procedure AttachStream(const AName: String; const AStream: IisStream);
    function  Attachments: IisListOfValues;
    property  AddressFrom: String read GetAddressFrom write SetAddressFrom;
    property  AddressTo: String read GetAddressTo write SetAddressTo;
    property  AddressCC: String read GetAddressCC write SetAddressCC;
    property  AddressBCC: String read GetAddressBCC write SetAddressBCC;
    property  Priority: TisMailPriority read GetPriority write SetPriority;
    property  Confirmation: Boolean read GetConfirmation write SetConfirmation;
    property  Subject: String read GetSubject write SetSubject;
    property  Body: String read GetBody write SetBody;
  end;


  IevEMailer = interface
  ['{79B1713C-5FCF-45DB-9E7D-8EF08AD3672D}']
    function  GetSmtpServer: String;
    procedure SetSmtpServer(const AValue: String);
    function  GetSmtpPort: Integer;
    procedure SetSmtpPort(const AValue: Integer);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  CreateMessage: IevMailMessage;
    procedure SendMail(const AMessage: IevMailMessage);
    procedure SendQuickMail(const ATo, AFrom, ASubject, ABody: String);
    property  SmtpServer: String read GetSmtpServer write SetSmtpServer;
    property  SmtpPort: Integer read GetSmtpPort write SetSmtpPort;
    property  UserName: String read GetUserName write SetUserName;
    property  Password: String read GetPassword write SetPassword;
  end;


  IevSingleSignOn = interface
  ['{B62991F9-49FA-4CDB-90E6-7CEC238E9A12}']
    function  GetSwipeClockOneTimeURL(const ACoNbr: integer): String;
    function  GetMRCOneTimeURL(const ACoNbr: integer): string;
    function  GetSwipeClockInfo: IisListOfValues;
  end;


  IevSecurityStructure = interface
  ['{D9F09889-71EA-46E9-B345-D162D4570AC8}']
    function  GetObj: TSecurityAtomCollection;
    procedure SetObj(const AValue: TSecurityAtomCollection);
    function  Rebuild: IevDualStream;
    property  Obj: TSecurityAtomCollection read GetObj write SetObj;
  end;


  IevSecurity = interface
  ['{9AD8F19F-50DC-4FC7-A428-AED33EDA640A}']
    function  GetAuthorization: IevUserAccount;
    function  AccountRights: IevSecAccountRights;
    function  GetSecStructure: IevSecurityStructure;
    function  GetUserSecStructure(const AUserInternalNbr: Integer): IevSecurityStructure;
    function  GetGroupSecStructure(const ASecGroupInternalNbr: Integer): IevSecurityStructure;
    procedure SetGroupSecStructure(const ASecGroupInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    procedure SetUserSecStructure(const AUserInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    function  GetUserGroupLevelRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetUserRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetGroupRights(const ASecGroupInternalNbr: Integer): IevSecAccountRights;
    procedure CreateEEAccount(const AUserName, APassword, ACompanyNumber, ASSN: String;
                              const ACheckNumber: Integer; const ACheckTotalEarnings: Currency);
    procedure ChangePassword(const ANewPassword: String);
    procedure EnableSystemAccountRights;
    procedure DisableSystemAccountRights;
    procedure Refresh(const AReasonInfo: String);
    function  GetAvailableQuestions(const AUserName: String): IisStringList;
    function  GetAvailableExtQuestions(const AUserName: String): IisStringList;    
    procedure SetQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure SetExtQuestions(const AQuestionAnswerList: IisListOfValues);    
    function  GetQuestions(const AUserName: String): IisStringList;
    procedure ResetPassword(const AUserName: String; const AQuestionAnswerList: IisListOfValues; const ANewPassword: String);
    procedure CheckAnswers(const AUserName: String; const AQuestionAnswerList: IisListOfValues);
    function  GenerateNewPassword: String;
    procedure ValidatePassword(const APassword: String);
    function  GetPasswordRules: IisListOfValues;
    function  GetExtQuestions(const AUserName: String; const APassword: String): IisStringList;
    procedure CheckExtAnswers(const AUserName: String; const APassword: String; const AQuestionAnswerList: IisListOfValues);
    function  GetSecurityRules: IisListOfValues;    
  end;


  IevScreenPackage = interface
  ['{907E0AE5-DEBF-46FD-B1F4-68139655FF31}']
    function  PackageCaption: string;
    function  PackageBitmap: TBitmap;
    function  PackageSortPosition: string;
    function  InitPackage: Boolean;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean;
    function  DeactivatePackage: Boolean;
    function  ActivateFrame(FClass: Pointer; Path: string): Boolean;
    procedure UserPackageStructure;
    function  AskCommitChanges: Boolean;
    function  CanClose: Boolean;
  end;


  IevSystemScreens = interface(IevScreenPackage)
  ['{37D2AE8B-3333-44FA-AD84-2B6BB448E249}']
  end;

  IevClientScreens = interface(IevScreenPackage)
  ['{5BD11612-3868-4E4E-ACF1-060723DB4499}']
  end;

  IevCompanyScreens = interface(IevScreenPackage)
  ['{243C6EBF-AF2D-4861-857D-0E49EB09C426}']
  end;

  IevEmployeeScreens = interface(IevScreenPackage)
  ['{2FF63E86-B6D7-482F-98E3-F1CB3AF9BF9C}']
  end;

  IevBureauScreens = interface(IevScreenPackage)
  ['{053C9222-DDB3-4A50-8C29-D5BEAFCC8304}']
  end;

  IevFinanceScreens = interface(IevScreenPackage)
  ['{DBC46724-839A-44FA-BDF7-1CA0120073B8}']
  end;

  IevSBAdminScreens = interface(IevScreenPackage)
  ['{1D430C9A-56D4-411A-9834-C09184F7DA3E}']
  end;

  IevAdminUsersScreens = interface(IevScreenPackage)
  ['{476902CD-758F-466C-9CF3-F56ECE679C94}']
  end;

  IevSBReportsScreens = interface(IevScreenPackage)
  ['{4F169E56-838D-48FD-B6C4-4809C0CF0C34}']
  end;

  IevReportsScreens = interface(IevScreenPackage)
  ['{B11BC465-E8AF-46BB-A961-03F1EEEBF74F}']
  end;

  IevPayrollScreens = interface(IevScreenPackage)
  ['{3F887A72-5683-484C-9509-8C47182230F4}']
    procedure OpenPayroll;
    function  SilentCheckToaBalance: Boolean;
    procedure CheckToaBalance;
    procedure ClearCheckToaSetupAccess;
  end;

  IevPayrollExtScreens = interface(IevScreenPackage)
  ['{2A33BB86-EB59-469A-B8E4-B014708428FE}']
  end;

  IevPayrollQueueScreens = interface(IevScreenPackage)
  ['{C5D564FB-2286-44F0-B295-9654D565CCFA}']
  end;

  IevVMRScreens = interface(IevScreenPackage)
  ['{9F4B12BC-C42A-4B9E-858A-1F38DD59D687}']
  end;

  IevTaxPaymentsScreens = interface(IevScreenPackage)
  ['{4F143E99-042A-466A-8735-AFF0F5B18848}']
  end;

  IevTaxReportsScreens = interface(IevScreenPackage)
  ['{84E8271C-56F7-439F-B2FE-408CAD863C0A}']
  end;

  IevACHScreens = interface(IevScreenPackage)
  ['{4F1E11C6-A725-4483-B79E-E6F3D12A1D4F}']
  end;

  IevImportADPScreens = interface(IevScreenPackage)
  ['{49A9787A-CA19-4033-8A3C-B451476258CC}']
  end;

  IevImportRapidScreens = interface(IevScreenPackage)
  ['{353A9C57-481B-478D-8F2C-E5574ADA844A}']
  end;

  IevImportMillenniumScreens = interface(IevScreenPackage)
  ['{B5847A74-2C49-4AF4-A500-98E213F8431F}']
  end;

  IevImportPayChoiceScreens = interface(IevScreenPackage)
  ['{FC477B1F-06EE-4314-8863-00C461E19036}']
  end;

  IevHRScreens = interface(IevScreenPackage)
  ['{D3756637-9403-437A-8F4A-A2313A38AA4E}']
  end;

  IevTestingScreens = interface(IevScreenPackage)
  ['{3C77A8AC-8E7D-447B-A792-13413A0427C2}']
  end;

  IevEvoXScreens = interface(IevScreenPackage)
  ['{4F1BE05F-2D66-426D-B219-08CAFC2FF716}']
  end;

  IevUserSchedulerScreens = interface(IevScreenPackage)
  ['{E87EA806-82F7-4B83-979A-0AA64484769D}']
    procedure ShowRemindersWindow(const AEventID: String);
  end;

  IevWebBrowserScreens = interface(IevScreenPackage)
  ['{73ABBFCC-8FB5-4504-A096-209109896F6D}']
  end;

  
  IevEvolutionGUI = interface
  ['{59BF771F-AC91-4E8C-ABD7-2E6BB2E0D522}']
    procedure  CreateMainForm;
    procedure  DestroyMainForm;
    procedure  ApplySecurity(const Component: TComponent);
    function   GUIContext: IevContext;
    procedure  AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure  AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure  OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure  CheckTaskQueueStatus;
    procedure  PrintTask(const ATaskID: TisGUID);
    procedure  OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure  OnWaitingServerResponse;
    procedure  OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure  PasswordChangeClose;
  end;


  // Global Flags Manager
  IevGlobalFlagInfo = interface
  ['{787A288D-D986-47CF-9229-5F2B3C0CDDFF}']
    function  GetFlagName: String;  
    function  GetFlagType: String;
    function  GetTag: String;
    function  GetDomain: String;
    function  GetUserName: String;
    function  GetContextID: TisGUID;
    function  GetLockTime: TDateTime;
    function  IncUsage: Integer;
    function  DecUsage: Integer;
    property  FlagName: String read GetFlagName;
    property  FlagType: String read GetFlagType;
    property  Tag: String read GetTag;
    property  Domain: String read GetDomain;
    property  UserName: String read GetUserName;
    property  ContextID: TisGUID read GetContextID;
    property  LockTime: TDateTime read GetLockTime;
  end;


  IevGlobalFlagsManager = interface
  ['{B305F9E8-74C0-4CF9-AAE8-CB316A34FAD0}']
    procedure EnableExclusiveMode;
    procedure DisableExclusiveMode;
    function  SetFlag(const AFlagName: String): Boolean; overload;
    function  ReleaseFlag(const AFlagName: String): Boolean; overload;
    function  TryLock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryLock(const AFlagName: String): Boolean; overload;
    function  TryUnlock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryUnlock(const AFlagName: String): Boolean; overload;
    procedure ForcedUnlock(const AType: String; const ATag: String); overload;
    procedure ForcedUnlock(const AFlagName: String); overload;
    function  GetLockedFlag(const AType: String; const ATag: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlag(const AFlagName: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlagList(const AType: String = ''): IisList;
    function  GetSetFlagList(const AType: String = ''): IisList;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
  end;


  // Task Queue

  IevTask = interface
  ['{1DC99249-70C5-440A-B3B2-2A40E946DE2A}']
    function   GetID: TisGUID;
    procedure  SetID(const AValue: TisGUID);
    function   GetNbr: Integer;
    function   GetTaskType: String;
    function   GetUser: String;
    function   GetDomain: String;
    function   GetPriority: Integer;
    procedure  SetPriority(const AValue: Integer);
    function   GetMaxThreads: Integer;
    procedure  SetMaxThreads(const AValue: Integer);
    function   GetNotificationEmail: string;
    procedure  SetNotificationEmail(const AValue: string);
    function   GetSendEmailNotification: Boolean;
    procedure  SetSendEmailNotification(const AValue: Boolean);
    function   GetEmailSendRule: TevEmailSendRule;
    procedure  SetEmailSendRule(const AValue: TevEmailSendRule);
    function   GetLastUpdate: TDateTime;
    function   GetState: TevTaskState;
    function   GetCaption: String;
    procedure  SetCaption(const AValue: string);
    function   GetProgressText: string;
    function   GetLog: String;
    function   GetExceptions: String;
    function   GetWarnings: String;
    function   GetNotes: String;
    function   GetSeenByUser: Boolean;
    function   GetDaysToStayInQueue: Integer;
    function   GetFirstRunAt: TDateTime;
    function   GetRunStatistics: IisStatEvent;
    procedure  AddException(const AText: string);
    procedure  AddWarning(const AText: string);
    procedure  AddNotes(const AText: string);
    function   GetAsStream: IEvDualStream;
    procedure  SetAsStream(const AStream: IEvDualStream);
    property   AsStream: IEvDualStream read GetAsStream write SetAsStream;
    property   ID: TisGUID read GetID write SetID;
    property   Nbr: Integer read GetNbr;
    property   TaskType: String read GetTaskType;
    property   User: String read GetUser;
    property   Domain: String read GetDomain;
    property   Priority: Integer read GetPriority write SetPriority;
    property   MaxThreads: Integer read GetMaxThreads write SetMaxThreads;
    property   NotificationEmail: String read GetNotificationEmail write SetNotificationEmail;
    property   SendEmailNotification: Boolean read GetSendEmailNotification write SetSendEmailNotification;
    property   EmailSendRule: TevEmailSendRule read GetEmailSendRule write SetEmailSendRule;
    property   State: TevTaskState read GetState;
    property   Caption: String read GetCaption write SetCaption;
    property   ProgressText: String read GetProgressText;
    property   Log: String read GetLog;
    property   Exceptions: String read GetExceptions;
    property   Warnings: String read GetWarnings;
    property   Notes: String read GetNotes;
    property   LastUpdate: TDateTime read GetLastUpdate;
    property   SeenByUser: Boolean read GetSeenByUser;
    property   DaysToStayInQueue: Integer read GetDaysToStayInQueue;
    property   FirstRunAt: TDateTime read GetFirstRunAt;
    property   RunStatistics: IisStatEvent read GetRunStatistics;
  end;


  IevPrintableTask = interface(IevTask)
  ['{77F47396-A7E2-46CD-AEAC-FA5A83C97209}']
    function   ReportResults: IevDualStream;
    function   GetDestination: TrwReportDestination;
    procedure  SetDestination(const AValue: TrwReportDestination);
    property   Destination: TrwReportDestination read GetDestination write SetDestination;
  end;


  IevRunReportTask = interface(IevPrintableTask)
  ['{B0F4AF63-87CC-4270-A9A4-B865B630610D}']
    function   GetReports: IevDualStream;
    procedure  SetReports(const AValue: IevDualStream);
    property   Reports: IevDualStream read GetReports write SetReports;
  end;


  IevCreatePayrollTask = interface(IevTask)
  ['{DE30BB55-93AA-4DEC-9284-C68223B56EB8}']
    function GetAutoCommit: Boolean;
    function GetTcAutoImportJobCodes: Boolean;
    function GetBatchFreq: String;
    function GetBlock401K: Integer;
    function GetBlockACH: Integer;
    function GetBlockAgencies: Integer;
    function GetBlockBilling: Integer;
    function GetBlockChecks: Integer;
    function GetBlockTaxDeposits: Integer;
    function GetBlockTimeOff: Integer;
    function GetCheckComments: String;
    function GetCheckDate: TDateTime;
    function GetCheckDateReplace: Integer;
    function GetCheckFreq: String;
    function GetCLNbr: Integer;
    function GetCONbr: Integer;
    function GetEEList: string;
    function GetLoadDefaults: Integer;
    function GetPayHours: Integer;
    function GetPaySalary: Integer;
    function GetPeriodBeginDate: TDateTime;
    function GetPeriodEndDate: TDateTime;
    function GetPRFilterNbr: Integer;
    function GetPrList: string;
    function GetPRTemplateNbr: Integer;
    function GetSalaryOrHourly: Integer;
    function GetTcDBDTOption: TCImportDBDTOption;
    function GetTcFileFormat: TCFileFormatOption;
    function GetTcFourDigitYear: Boolean;
    function GetTcImportSource: string;
    function GetTcImportSourceFile: IevDualStream;
    function GetTcLookupOption: TCImportLookupOption;
    function GetTcUseEmployeePayRates: Boolean;
    function GetIncludeTimeOffRequests: Boolean;
    procedure SetAutoCommit(const AValue: Boolean);
    procedure SetTcAutoImportJobCodes(const AValue: Boolean);
    procedure SetBatchFreq(const AValue: String);
    procedure SetBlock401K(const AValue: Integer);
    procedure SetBlockACH(const AValue: Integer);
    procedure SetBlockAgencies(const AValue: Integer);
    procedure SetBlockBilling(const AValue: Integer);
    procedure SetBlockChecks(const AValue: Integer);
    procedure SetBlockTaxDeposits(const AValue: Integer);
    procedure SetBlockTimeOff(const AValue: Integer);
    procedure SetCheckComments(const AValue: String);
    procedure SetCheckDate(const AValue: TDateTime);
    procedure SetCheckDateReplace(const AValue: Integer);
    procedure SetCheckFreq(const AValue: String);
    procedure SetCLNbr(const AValue: Integer);
    procedure SetCONbr(const AValue: Integer);
    procedure SetEEList(const AValue: string);
    procedure SetLoadDefaults(const AValue: Integer);
    procedure SetPayHours(const AValue: Integer);
    procedure SetPaySalary(const AValue: Integer);
    procedure SetPeriodBeginDate(const AValue: TDateTime);
    procedure SetPeriodEndDate(const AValue: TDateTime);
    procedure SetPRFilterNbr(const AValue: Integer);
    procedure SetPrList(const AValue: string);
    procedure SetPRTemplateNbr(const AValue: Integer);
    procedure SetSalaryOrHourly(const AValue: Integer);
    procedure SetTcDBDTOption(const AValue: TCImportDBDTOption);
    procedure SetTcFileFormat(const AValue: TCFileFormatOption);
    procedure SetTcFourDigitYear(const AValue: Boolean);
    procedure SetTcImportSource(const AValue: string);
    procedure SetTcImportSourceFile(const AValue: IevDualStream);
    procedure SetTcLookupOption(const AValue: TCImportLookupOption);
    procedure SetTcUseEmployeePayRates(const AValue: Boolean);
    procedure SetIncludeTimeOffRequests(const AValue: Boolean);

    property CLNbr: Integer read GetCLNbr write SetCLNbr;     // Internal client number
    property CONbr: Integer read GetCONbr write SetCONbr;     // Internal company number
    property PrList: string read GetPrList write SetPrList;   // Contains internal payroll number after task is finished.
                                                          // Format is <internal client number>;<internal payroll number>
    property CheckDate: TDateTime read GetCheckDate write SetCheckDate; // Defines payroll check date
    property PeriodBeginDate: TDateTime read GetPeriodBeginDate write SetPeriodBeginDate; // Batch period begin date
    property PeriodEndDate: TDateTime read GetPeriodEndDate write SetPeriodEndDate; // Batch period end date
    property BatchFreq: String read GetBatchFreq write SetBatchFreq; // Defines batch frequency
    property IncludeTimeOffRequests: Boolean read GetIncludeTimeOffRequests write SetIncludeTimeOffRequests; 

   { Defines what to do if payroll check date is not in a payroll calendar. Possible values:
     0 - default - N/A - check date is in a calendar or payroll is unscheduled
     1 - replace in a calendar
     2 - create unscheduled payroll                                }
    property CheckDateReplace: Integer read GetCheckDateReplace write SetCheckDateReplace;

    { Defines what employees should be included in this payroll. Possible values:
      0 - default - all
      1 - salary employees
      2 - employees, paid hourly                                 }
    property SalaryOrHourly: Integer read GetSalaryOrHourly write SetSalaryOrHourly;

    { Employee filter. If the string is empty (default), checks
      will be created for all eligible employees, if not, it should
      contain comma separated list of employee internal numbers,
      then checks will be created for these employees only          }
    property EEList: string read GetEEList write SetEEList;

    property CheckComments: String read GetCheckComments write SetCheckComments;  // Defines optional comments, that would be printed on checks

    { Blocks agency check creation. Possible values:
      0 - default - creation is blocked
      any other - creation is no blocked             }
    property BlockAgencies: Integer read GetBlockAgencies write SetBlockAgencies;

    { Blocks 401k creation. Possible values:
      0 - default - 401k is blocked
      any other - 401k is not blocked        }
    property Block401K: Integer read GetBlock401K write SetBlock401K;

    { Blocks ACH creation. Possible values:
      0 - default - ACH is blocked
      any other - ACH is no blocked         }
    property BlockACH: Integer read GetBlockACH write SetBlockACH;

    { Blocks invoice creation. Possible values:
      0 - default - creation is blocked
      any other - creation is no blocked        }
    property BlockBilling: Integer read GetBlockBilling write SetBlockBilling;

    { Blocks tax deposit and liabilities creating. Possible values:
      0 - default - tax deposits are blocked
      1 - liabilities are blocked
      2 - both tax deposits and liabilities are blocked
      3 - both tax deposits and liabilities are not blocked         }
    property BlockTaxDeposits: Integer read GetBlockTaxDeposits write SetBlockTaxDeposits;

    { Blocks check and report creation. Possible values:
      0 - default - reports are blocked
      1 - checks are blocked
      2 - both checks and reports are blocked
      3 - both checks and reports are not blocked        }
    property BlockChecks: Integer read GetBlockChecks write SetBlockChecks;

    { Blocks time off creating. Possible values:
      0 - default - all time off is blocked
      1 - only accrual is blocked
      2 - none blocked                           }
    property BlockTimeOff: Integer read GetBlockTimeOff write SetBlockTimeOff;

    property CheckFreq: String read GetCheckFreq write SetCheckFreq;

    { Defines if salary should be paid. Possible values:
      0 - default - salary should be paid
      any other - salary should not be paid              }
    property PaySalary: Integer read GetPaySalary write SetPaySalary;

    { Defines if standard hours should be paid. Possible values:
      0 - default - standard hours should be paid
      any other - standard hours should not be paid              }
    property PayHours: Integer read GetPayHours write SetPayHours;

    { Defines if defaults from DBDT level should be loaded.
      Possible values:
      0 - default - defaults will be loaded
      any other - defaults will not be loaded               }
    property LoadDefaults: Integer read GetLoadDefaults write SetLoadDefaults;

    { Defines internal number for check template to be used.
      Default value is 0 - no check template used            }
    property PRTemplateNbr: Integer read GetPRTemplateNbr write SetPRTemplateNbr;

    { Defines internal number for payroll filter to be applied.
      Default value is 0 - no payroll filter applied            }
    property PRFilterNbr: Integer read GetPRFilterNbr write SetPRFilterNbr;

    { Defines if created payroll should be automatically submitted
      to service bureau. Default value is false                    }
    property AutoCommit: Boolean read GetAutoCommit write SetAutoCommit;

    property TcImportSource: string read GetTcImportSource write SetTcImportSource;

    { Defines employee lookup option. Possible values:
      0 - default - by name
      1 - by employee number
      2 - by SSN                                       }
    property TcLookupOption: TCImportLookupOption read GetTcLookupOption write SetTcLookupOption;

    { Defines how DBDT codes should be matched. Possible values:
      0 - default - full match
      1 - partial match                                          }
    property TcDBDTOption: TCImportDBDTOption read GetTcDBDTOption write SetTcDBDTOption;

    { Defines import file format. Possible values:
      0 - default - fixed length
      1 - comma delimited                          }
    property TcFileFormat: TCFileFormatOption read GetTcFileFormat write SetTcFileFormat;

    // Defines if four digits year should be used. Default value is false
    property TcFourDigitYear: Boolean read GetTcFourDigitYear write SetTcFourDigitYear;

    // Defines if job codes should be automatically created if needed. Default value is false
    property TcAutoImportJobCodes: Boolean read GetTcAutoImportJobCodes write SetTcAutoImportJobCodes;

    // Defines if employee pay rates should be used instead of values in the import file. Default value is false
    property TcUseEmployeePayRates: Boolean read GetTcUseEmployeePayRates write SetTcUseEmployeePayRates;

    property TcImportSourceFile: IevDualStream read GetTcImportSourceFile write SetTcImportSourceFile; // Import file stream
  end;


  IevProcessPayrollTask = interface(IevPrintableTask)
  ['{C4F7B1EE-FD81-479C-B5BF-0D29E67F79B3}']
    function  GetAllPayrolls: Boolean;
    function  GetCoList: string;
    function  GetPayrollAge: TDateTime;
    function  GetPrintChecks: Boolean;
    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetAllPayrolls(const Value: Boolean);
    procedure SetCoList(const Value: string);
    procedure SetPayrollAge(const Value: TDateTime);
    procedure SetPrintChecks(const Value: Boolean);
    procedure SetPrList(const Value: string);
    procedure SetPRMessage(const Value: string);
    property  AllPayrolls: Boolean read GetAllPayrolls write SetAllPayrolls;
    property  PayrollAge: TDateTime read GetPayrollAge write SetPayrollAge;
    property  PrList: string read GetPrList write SetPrList;
    property  PrintChecks: Boolean read GetPrintChecks write SetPrintChecks;
    property  PRMessage: string read GetPRMessage write SetPRMessage;
    property  CoList: string read GetCoList write SetCoList;
  end;


  IevPreprocessPayrollTask = interface(IevTask)
  ['{748136A2-A9EC-463E-94E5-ACFE29DDF610}']
    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetPrList(const Value: string);
    procedure SetPRMessage(const Value: string);
    property  PrList: string read GetPrList write SetPrList;
    property  PRMessage: string read GetPRMessage write SetPRMessage;
  end;


  IevReprintPayrollTask = interface(IevPrintableTask)
  ['{DD000F2C-7208-4F72-A6E3-F914371F7351}']
    function  GetClientID: Integer;
    procedure SetClientID(const AValue: Integer);
    function  GetPrNbr: Integer;
    procedure SetPrNbr(const AValue: Integer);
    function  GetCheckDate: TDateTime;
    procedure SetCheckDate(const AValue: TDateTime);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetPrintInvoices: Boolean;
    procedure SetPrintInvoices(const AValue: Boolean);
    function  GetCurrentTOA: Boolean;
    procedure SetCurrentTOA(const AValue: Boolean);
    function  GetPayrollChecks: String;
    procedure SetPayrollChecks(const AValue: String);
    function  GetAgencyChecks: String;
    procedure SetAgencyChecks(const AValue: String);
    function  GetTaxChecks: String;
    procedure SetTaxChecks(const AValue: String);
    function  GetBillingChecks: String;
    procedure SetBillingChecks(const AValue: String);
    function  GetCheckForm: String;
    procedure SetCheckForm(const AValue: String);
    function  GetMiscCheckForm: String;
    procedure SetMiscCheckForm(const AValue: String);
    function  GetReason: String;
    procedure SetReason(const AValue: String);
    function  GetSkipCheckStubCheck: Boolean;
    procedure SetSkipCheckStubCheck(const AValue: Boolean);
    function  GetDontPrintBankInfo: Boolean;
    procedure SetDontPrintBankInfo(const AValue: Boolean);
    function  GetHideBackground: Boolean;
    procedure SetHideBackground(const AValue: Boolean);
    function  GetDontUseVMRSettings: Boolean;
    procedure SetDontUseVMRSettings(const AValue: Boolean);
    function  GetPrReprintHistoryNBR: Integer;
    procedure SetPrReprintHistoryNBR(const AValue: Integer);
    property  ClientID: Integer read GetClientID write SetClientID;
    property  PrNbr: Integer read GetPrNbr write SetPrNbr;
    property  CheckDate: TDateTime read GetCheckDate write SetCheckDate;
    property  PrintReports: Boolean read GetPrintReports write SetPrintReports;
    property  PrintInvoices: Boolean read GetPrintInvoices write SetPrintInvoices;
    property  CurrentTOA: Boolean read GetCurrentTOA write SetCurrentTOA;
    property  PayrollChecks: String read GetPayrollChecks write SetPayrollChecks;
    property  AgencyChecks: String read GetAgencyChecks write SetAgencyChecks;
    property  TaxChecks: String read GetTaxChecks write SetTaxChecks;
    property  BillingChecks: String read GetBillingChecks write SetBillingChecks;
    property  CheckForm: String read GetCheckForm write SetCheckForm;
    property  MiscCheckForm: String read GetMiscCheckForm write SetMiscCheckForm;
    property  SkipCheckStubCheck: Boolean read GetSkipCheckStubCheck write SetSkipCheckStubCheck;
    property  DontPrintBankInfo: Boolean read GetDontPrintBankInfo write SetDontPrintBankInfo;
    property  HideBackground: Boolean read GetHideBackground write SetHideBackground;
    property  DontUseVMRSettings: Boolean read GetDontUseVMRSettings write SetDontUseVMRSettings;
    property  PrReprintHistoryNBR: integer read GetPrReprintHistoryNBR write setPrReprintHistoryNBR;
    property  Reason: String read GetReason write SetReason;
  end;


  IevTaxPaymentsTaskParam = interface
  ['{AE1900B2-C3FB-41C1-BEF4-62C2D9AD7115}']
    function  GetClientID: Integer;
    procedure SetClientID(const AValue: Integer);
    function  GetCustomClientNumber: string;
    procedure SetCustomClientNumber(const AValue: String);
    function  GetFedTaxLiabilityList: string;
    procedure SetFedTaxLiabilityList(const AValue: string);
    function  GetStateTaxLiabilityList: string;
    procedure SetStateTaxLiabilityList(const AValue: string);
    function  GetSUITaxLiabilityList: string;
    procedure SetSUITaxLiabilityList(const AValue: string);
    function  GetLocalTaxLiabilityList: string;
    procedure SetLocalTaxLiabilityList(const AValue: string);
    property  ClientID: Integer read GetClientID write SetClientID;
    property  CustomClientNumber: string read GetCustomClientNumber write SetCustomClientNumber;
    property  FedTaxLiabilityList: string read GetFedTaxLiabilityList write SetFedTaxLiabilityList;
    property  StateTaxLiabilityList: string read GetStateTaxLiabilityList write SetStateTaxLiabilityList;
    property  SUITaxLiabilityList: string read GetSUITaxLiabilityList write SetSUITaxLiabilityList;
    property  LocalTaxLiabilityList: string read GetLocalTaxLiabilityList write SetLocalTaxLiabilityList;
  end;

  IevTaxPaymentsTaskParams = interface
  ['{9DCED002-EBCE-4775-B4BD-3EF06A1B0872}']
    function  Add: IevTaxPaymentsTaskParam;
    function  GetItem(Index: Integer): IevTaxPaymentsTaskParam;
    function  Count: Integer;
    property  Items[Index: Integer]: IevTaxPaymentsTaskParam read GetItem; default;
  end;


  IevProcessTaxPaymentsTask = interface(IevTask)
  ['{EF6297B1-37F9-4E4D-AF9D-B1598CAA3F48}']
    function  GetParams: IevTaxPaymentsTaskParams;
    function  GetForceChecks: Boolean;
    procedure SetForceChecks(const AValue: Boolean);
    function  GetSbTaxPaymentNbr: Integer;
    procedure SetSbTaxPaymentNbr(const AValue: Integer);
    function  GetEftpReference: String;
    procedure SetPostProcessReportName(const AValue: String);
    function  GetPostProcessReportName: String;
    procedure SetEftpReference(const AValue: String);
    function  GetDefaultMiscCheckForm: String;
    procedure SetDefaultMiscCheckForm(const AValue: String);
    function  GetChecks: IevDualStream;
    function  GetAchFile: IevDualStream;
    function  GetAchReport: IevDualStream;
    function  GetEftFile: IevDualStream;
    function  GetNyDebitFile: IevDualStream;
    function  GetMaDebitFile: IevDualStream;
    function  GetCaDebitFile: IevDualStream;
    function  GetUnivDebitFile: IevDualStream;
    function  GetOneMiscCheck: IevDualStream;
    function  GetOneMiscCheckReport: IevDualStream;
    property  Params: IevTaxPaymentsTaskParams read GetParams;
    property  ForceChecks: Boolean read GetForceChecks write SetForceChecks;
    property  SbTaxPaymentNbr: Integer read GetSbTaxPaymentNbr write SetSbTaxPaymentNbr;
    property  PostProcessReportName: String read GetPostProcessReportName write SetPostProcessReportName;
    property  EftpReference: string read GetEftpReference write SetEftpReference;
    property  DefaultMiscCheckForm: String read GetDefaultMiscCheckForm write SetDefaultMiscCheckForm;
    property  Checks: IevDualStream read GetChecks;
    property  AchFile: IevDualStream read GetAchFile;
    property  AchReport: IevDualStream read GetAchReport;
    property  EftFile: IevDualStream read GetEftFile;
    property  NyDebitFile: IevDualStream read GetNyDebitFile;
    property  MaDebitFile: IevDualStream read GetMaDebitFile;
    property  CaDebitFile: IevDualStream read GetCaDebitFile;
    property  UnivDebitFile: IevDualStream read GetUnivDebitFile;
    property  OneMiscCheck: IevDualStream read GetOneMiscCheck;
    property  OneMiscCheckReport: IevDualStream read GetOneMiscCheckReport;
  end;

  IevExchangePackage = interface;
  IEvExchangeMapFile = interface;

  IevEvoXImportTask = interface(IevTask)
  ['{E01D014B-6988-41A8-8C94-A3B0EAA850FF}']
    function  GetInputDatasets: IisListOfValues;
    procedure SetInputDataSets(const AInputDataSets: IisListOfValues);
    function  GetMapFile: IEvExchangeMapFile;
    procedure SetMapFile(const AMapFile: IEvExchangeMapFile);
    function  GetPackage: IevExchangePackage;
    procedure SetPackage(const APackage: IevExchangePackage);
    function  GetEffectiveDate: TDateTime;
    procedure SetEffectiveDate(const AEffectiveDate: TDateTime);
    function  GetOptions: IisStringList;
    procedure SetOptions(const AOptions: IisStringList);
    function  GetResults: IisListofValues;
    property  InputDatasets: IisListOfValues read GetInputDatasets write SetInputDataSets;
    property  MapFile: IEvExchangeMapFile read GetMapFile write SetMapFile;
    property  Package: IevExchangePackage read GetPackage write SetPackage;
    property  EffectiveDate: TDateTime read GetEffectiveDate write SetEffectiveDate;
    property  Options: IisStringList read GetOptions write SetOptions;
    property  Results: IisListOfValues read GetResults;
  end;

  IevACHOptions = interface
  ['{1610EBE2-B3DE-4DC7-84A7-A70A77FCC148}']
    function  GetDisplayCoNbrAndName: Boolean;
    procedure SetDisplayCoNbrAndName(const AValue: Boolean);
    function  GetBalanceBatches: Boolean;
    procedure SetBalanceBatches(const AValue: Boolean);
    function  GetBlockDebits: Boolean;
    procedure SetBlockDebits(const AValue: Boolean);
    function  GetCombineTrans: Integer;
    procedure SetCombineTrans(const AValue: Integer);
    function  GetUseSBEIN: Boolean;
    procedure SetUseSBEIN(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetSunTrust: Boolean;
    procedure SetSunTrust(const AValue: Boolean);
    function  GetBlockSBCredit: Boolean;
    procedure SetBlockSBCredit(const AValue: Boolean);
    function  GetFedReserve: Boolean;
    procedure SetFedReserve(const AValue: Boolean);
    function  GetIntercept: Boolean;
    procedure SetIntercept(const AValue: Boolean);
    function  GetPayrollHeader: Boolean;
    procedure SetPayrollHeader(const AValue: Boolean);
    function  GetDescReversal: Boolean;
    procedure SetDescReversal(const AValue: Boolean);
    function  GetBankOne: Boolean;
    procedure SetBankOne(const AValue: Boolean);
    function  GetEffectiveDate: Boolean;
    procedure SetEffectiveDate(const AValue: Boolean);
    function  GetCombineThreshold: Currency;
    procedure SetCombineThreshold(const AValue: Currency);
    function  GetCoId: String;
    procedure SetCoId(const AValue: String);
    function  GetSBEINOverride: String;
    procedure SetSBEINOverride(const AValue: String);
    function  GetHeader: String;
    procedure SetHeader(const AValue: String);
    function  GetFooter: String;
    procedure SetFooter(const AValue: String);
    function  GetPostProcessReportName: String;
    procedure SetPostProcessReportName(const AValue: String);
    property  DisplayCoNbrAndName: Boolean read GetDisplayCoNbrAndName write SetDisplayCoNbrAndName;
    property  BalanceBatches: Boolean read GetBalanceBatches write SetBalanceBatches;
    property  BlockDebits: Boolean read GetBlockDebits write SetBlockDebits;
    property  CombineTrans: Integer read GetCombineTrans write SetCombineTrans;
    property  UseSBEIN: Boolean read GetUseSBEIN write SetUseSBEIN;
    property  CTS: Boolean read GetCTS write SetCTS;
    property  SunTrust: Boolean read GetSunTrust write SetSunTrust;
    property  BlockSBCredit: Boolean read GetBlockSBCredit write SetBlockSBCredit;
    property  FedReserve: Boolean read GetFedReserve write SetFedReserve;
    property  Intercept: Boolean read GetIntercept write SetIntercept; // cbIntercept
    property  PayrollHeader: Boolean read GetPayrollHeader write SetPayrollHeader; // cbPayrollHeader
    property  DescReversal: Boolean read GetDescReversal write SetDescReversal;// cbDescReversal
    property  BankOne: Boolean read GetBankOne write SetBankOne; // cbBankOne
    property  EffectiveDate: Boolean read GetEffectiveDate write SetEffectiveDate; // cbEffectiveDate
    property  CombineThreshold: Currency read GetCombineThreshold write SetCombineThreshold; // eThreshold   !!
    property  CoId: string read GetCoId write SetCoId; // eCoId
    property  SBEINOverride: string read GetSBEINOverride write SetSBEINOverride; // eSBEINOverride
    property  Header: string read GetHeader write SetHeader; // eHeader
    property  Footer: string read GetFooter write SetFooter; // eFooter
    property  PostProcessReportName: String read GetPostProcessReportName write SetPostProcessReportName;
  end;

  IevProcessPrenoteACHTask = interface(IevTask)
  ['{BEBA23FC-0540-414B-A396-98E87F6D15B9}']
    function  GetACHOptions: IevACHOptions;
    function  GetPrenoteCLAccounts: Boolean;
    procedure SetPrenoteCLAccounts(const AValue: Boolean);
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetAchFile: IEvDualStream;
    property  ACHOptions: IevACHOptions read GetACHOptions;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;
    property  PrenoteCLAccounts: Boolean read GetPrenoteCLAccounts write SetPrenoteCLAccounts;
    property  ACHDataStream: IEvDualStream read GetACHDataStream write SetACHDataStream;
    property  AchFile: IEvDualStream read GetAchFile;
    property  AchRegularReport: IEvDualStream read GetAchRegularReport;
    property  AchDetailedReport: IEvDualStream read GetAchDetailedReport;
    property  AchSave: IEvDualStream read GetAchSave;
  end;

  IevProcessManualACHTask = interface(IevTask)
  ['{E98D37BC-A829-4FE8-91F0-2E307C4DDEE5}']
    function  GetACHOptions: IevACHOptions;
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;
    function  GetACHDataStream: IEvDualStream;
    function  GetBatchBANK_REGISTER_TYPE: String;
    procedure SetBatchBANK_REGISTER_TYPE(const AValue: String);
    property  BatchBANK_REGISTER_TYPE: String read GetBatchBANK_REGISTER_TYPE write SetBatchBANK_REGISTER_TYPE;
    property  ACHOptions: IevACHOptions read GetACHOptions;
    property  Preprocess: Boolean read GetPreprocess write SetPreprocess;
    property  ACHDataStream: IEvDualStream read GetACHDataStream write SetACHDataStream;
    property  AchFile: IEvDualStream read GetAchFile;
    property  AchRegularReport: IEvDualStream read GetAchRegularReport;
    property  AchDetailedReport: IEvDualStream read GetAchDetailedReport;
    property  AchSave: IEvDualStream read GetAchSave;
  end;

  IevCashManagementTask = interface(IevTask)
  ['{D12FAE61-D69B-47A6-AD43-C9D5E5D5334C}']
    function  GetBlockCompanyDD: Boolean;
    procedure SetBlockCompanyDD(const AValue: Boolean);
    function  GetACHOptions: IevACHOptions;
    function  GetReversalACH: Boolean;
    procedure SetReversalACH(const AValue: Boolean);
    function  GetPeriodFrom: TDateTime;
    procedure SetPeriodFrom(const AValue: TDateTime);
    function  GetPeriodTo: TDateTime;
    procedure SetPeriodTo(const AValue: TDateTime);
    function  GetStatusFromIndex: Integer;
    procedure SetStatusFromIndex(const AValue: Integer);
    function  GetStatusToIndex: Integer;
    procedure SetStatusToIndex(const AValue: Integer);
    function  GetBlockNegativeChecks: Boolean;
    procedure SetBlockNegativeChecks(const AValue: Boolean);
    function  GetIncludeOffsets: Integer;
    procedure SetIncludeOffsets(const AValue: Integer);
    function  GetSB_BANK_ACCOUNTS_NBR: Integer;
    procedure SetSB_BANK_ACCOUNTS_NBR(const AValue: Integer);
    function  GetCombine: Integer;
    procedure SetCombine(const AValue: Integer);
    function  GetThreshold: Currency;
    procedure SetThreshold(const AValue: Currency);
    function  GetUseSBEIN: Boolean;
    procedure SetUseSBEIN(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetHSA: Boolean;
    procedure SetHSA(const AValue: Boolean);
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetNonAchReport: IEvDualStream;
    function  GetWTFile: IEvDualStream;
    function  GetAchSave: IEvDualStream;
    function  GetBlockNegativeLiabilities: Boolean;
    procedure SetBlockNegativeLiabilities(const AValue: Boolean);
    property  ACHDataStream: IEvDualStream read GetACHDataStream write SetACHDataStream;
    property  ACHOptions: IevACHOptions read GetACHOptions;
    property  ReversalACH: Boolean read GetReversalACH write SetReversalACH;
    property  PeriodFrom: TDateTime read GetPeriodFrom write SetPeriodFrom;
    property  PeriodTo: TDateTime read GetPeriodTo write SetPeriodTo;
    property  StatusFromIndex: Integer read GetStatusFromIndex write SetStatusFromIndex;
    property  StatusToIndex: Integer read GetStatusToIndex write SetStatusToIndex;
    property  BlockNegativeChecks: Boolean read GetBlockNegativeChecks write SetBlockNegativeChecks;
    property  IncludeOffsets: Integer read GetIncludeOffsets write SetIncludeOffsets;
    property  SB_BANK_ACCOUNTS_NBR: Integer read GetSB_BANK_ACCOUNTS_NBR write SetSB_BANK_ACCOUNTS_NBR;
    property  Combine: Integer read GetCombine write SetCombine;
    property  Threshold: Currency read GetThreshold write SetThreshold;
    property  UseSBEIN: Boolean read GetUseSBEIN write SetUseSBEIN;
    property  CTS: Boolean read GetCTS write SetCTS;
    property  HSA: Boolean read GetHSA write SetHSA;
    property  Preprocess: Boolean read GetPreprocess write SetPreprocess;
    property  AchFile: IEvDualStream read GetAchFile;
    property  AchRegularReport: IEvDualStream read GetAchRegularReport;
    property  AchDetailedReport: IEvDualStream read GetAchDetailedReport;
    property  AchSave: IEvDualStream read GetAchSave;
    property  NonAchReport: IEvDualStream read GetNonAchReport;
    property  WTFile: IEvDualStream read GetWTFile;
    property  BlockCompanyDD: Boolean read GetBlockCompanyDD write SetBlockCompanyDD;
    property  BlockNegativeLiabilities: Boolean read GetBlockNegativeLiabilities write SetBlockNegativeLiabilities;
  end;

  IevPreprocessQuarterEndTaskFilterItem = interface
  ['{1A97DE10-C5A0-493F-81FB-FFEAB3FDE501}']
    function  GetCaption: string;
    function  GetClNbr: Integer;
    function  GetCoNbr: Integer;
    function  GetConsolidation: Boolean;
    procedure SetCaption(const AValue: string);
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoNbr(const AValue: Integer);
    procedure SetConsolidation(const AValue: Boolean);
    property  ClNbr: Integer read GetClNbr write SetClNbr;
    property  CoNbr: Integer read GetCoNbr write SetCoNbr;
    property  Consolidation: Boolean read GetConsolidation write SetConsolidation;
    property  Caption: string read GetCaption write SetCaption;
  end;

  IevPreprocessQuarterEndTaskFilter = interface
  ['{B922D92F-30BB-4694-A191-AF571055CBD6}']
    function  GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskFilterItem;
    function  Add: IevPreprocessQuarterEndTaskFilterItem;
    procedure Delete(const AItem: IevPreprocessQuarterEndTaskFilterItem);
    procedure Clear;
    function  Count: Integer;
    property  Items[Index: Integer]: IevPreprocessQuarterEndTaskFilterItem read GetItem; default;
  end;

  IevPreprocessQuarterEndTaskResultItem = interface
  ['{C70E5B1D-F179-4229-B5B5-F1B0B2799F05}']
   procedure SetCoCustomNumber(const AValue: string);
   procedure SetCoName(const AValue: string);
   procedure SetConsolidation(const AValue: Boolean);
   procedure SetTimeStampStarted(const AValue: TDateTime);
   procedure SetTimeStampFinished(const AValue: TDateTime);
   procedure SetErrorMessage(const AValue: string);
   procedure SetTaxAdjReports(const AValue: IEvDualStream);
   function  GetCoCustomNumber: string;
   function  GetCoName: string;
   function  GetConsolidation: Boolean;
   function  GetTimeStampStarted: TDateTime;
   function  GetTimeStampFinished: TDateTime;
   function  GetErrorMessage: string;
   function  GetTaxAdjReports: IEvDualStream;
   property  CoCustomNumber: string read GetCoCustomNumber;
   property  CoName: string read GetCoName;
   property  Consolidation: Boolean read GetConsolidation;
   property  TimeStampStarted: TDateTime read GetTimeStampStarted;
   property  TimeStampFinished: TDateTime read GetTimeStampFinished;
   property  ErrorMessage: string read GetErrorMessage;
   property  TaxAdjReports: IEvDualStream read GetTaxAdjReports;
  end;

  IevPreprocessQuarterEndTaskResult = interface
  ['{E18D8642-04C1-4900-B201-E8DA195E16F6}']
    function GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskResultItem;
    function Add: IevPreprocessQuarterEndTaskResultItem;
    function Count: Integer;
    property Items[Index: Integer]: IevPreprocessQuarterEndTaskResultItem read GetItem; default;
  end;

  TTaxServiceFilterType = (tsAll, tsTax, tsNonTax);

  IevPreprocessQuarterEndTask = interface(IevPrintableTask)
  ['{47D2CBFE-9998-4129-92DE-B5B64EC6C287}']
    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    function  GetFilter: IevPreprocessQuarterEndTaskFilter;
    function  GetQecAdjustmentLimit: Currency;
    function  GetQecProduceReports: Boolean;
    function  GetResults: IevPreprocessQuarterEndTaskResult;
    function  GetTaxAdjustmentLimit: Currency;
    function  GetTaxServiceFilter: TTaxServiceFilterType;
    procedure SetBegDate(const AValue: TDateTime);
    procedure SetEndDate(const AValue: TDateTime);
    procedure SetQecAdjustmentLimit(const AValue: Currency);
    procedure SetQecProduceReports(const AValue: Boolean);
    procedure SetTaxAdjustmentLimit(const AValue: Currency);
    procedure SetTaxServiceFilter(const AValue: TTaxServiceFilterType);
    property  Filter: IevPreprocessQuarterEndTaskFilter read GetFilter;
    property  Results: IevPreprocessQuarterEndTaskResult read GetResults;
    property  BegDate: TDateTime read GetBegDate write SetBegDate;
    property  EndDate: TDateTime read GetEndDate write SetEndDate;
    property  TaxAdjustmentLimit: Currency read GetTaxAdjustmentLimit write SetTaxAdjustmentLimit;
    property  QecAdjustmentLimit: Currency read GetQecAdjustmentLimit write SetQecAdjustmentLimit;
    property  QecProduceReports: Boolean read GetQecProduceReports write SetQecProduceReports;
    property  TaxServiceFilter: TTaxServiceFilterType read GetTaxServiceFilter write SetTaxServiceFilter;
  end;


  IevRebuildTempTablesTask = interface(IevTask)
  ['{C6831146-1952-4CC2-AEDD-9F3DD4FFBBB0}']
    function  GetFullRebuild: Boolean;
    procedure SetFullRebuild(const AValue: Boolean);
    function  GetUseRange: Boolean;
    procedure SetUseRange(const AValue: Boolean);
    function  GetUseNumbers: Boolean;
    procedure SetUseNumbers(const AValue: Boolean);
    function  GetFromClient: Integer;
    procedure SetFromClient(const AValue: Integer);
    function  GetToClient: Integer;
    procedure SetToClient(const AValue: Integer);
    function  GetClientNumbers: String;
    procedure SetClientNumbers(const AValue: String);
    property  FullRebuild: Boolean read GetFullRebuild write SetFullRebuild;
    property  UseRange: Boolean read GetUseRange write SetUseRange;
    property  UseNumbers: Boolean read GetUseNumbers write SetUseNumbers;
    property  FromClient: Integer read GetFromClient write SetFromClient;
    property  ToClient: Integer read GetToClient write SetToClient;
    property  ClientNumbers: String read GetClientNumbers write SetClientNumbers;
  end;

  IevBenefitEnrollmentNotificationsTask = interface(IevTask)
  ['{94A39EFF-7D31-444D-A286-18E35F438357}']
    function  GetForAllClients: Boolean;
    function  GetClientNumbers: String;
    procedure SetClientNumbers(const AValue: String);
    function  GetEnableLogging: Boolean;
    procedure SetEnableLogging(const AValue: Boolean);

    property  ClientNumbers: String read GetClientNumbers write SetClientNumbers;
    property  ForAllClients: boolean read GetForAllClients;
    property  EnableLogging: boolean read GetEnableLogging write SetEnableLogging;
  end;

  IevQECTaskResultItem = interface
  ['{43F9BC5C-844E-49A9-A8EF-DE7658D48DC7}']
   procedure SetQECReports(const AValue: IEvDualStream);
   function  GetQECReports: IEvDualStream;
   property  QECReports: IEvDualStream read GetQECReports;
  end;

  IevQECTaskResult = interface
  ['{F1A11277-A739-4E36-8561-CB511BC506D3}']
    function GetItem(AIndex: Integer): IevQECTaskResultItem;
    function Add: IevQECTaskResultItem;
    function Count: Integer;
    property Items[Index: Integer]: IevQECTaskResultItem read GetItem; default;
  end;

  IevQECTaskFilterItem = interface
  ['{466EE59F-57E8-453E-AA75-7E34769714CC}']
    function  GetCaption: string;
    function  GetClNbr: Integer;
    function  GetCoNbr: Integer;
    procedure SetCaption(const AValue: string);
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoNbr(const AValue: Integer);
    property  ClNbr: Integer read GetClNbr write SetClNbr;
    property  CoNbr: Integer read GetCoNbr write SetCoNbr;
    property  Caption: string read GetCaption write SetCaption;
  end;

  IevQECTaskFilter = interface
  ['{AE27CAE3-AC57-4D6A-BBB1-B6A7146267C8}']
    function GetItem(AIndex: Integer): IevQECTaskFilterItem;
    function Add: IevQECTaskFilterItem;
    function Count: Integer;
    property  Items[Index: Integer]: IevQECTaskFilterItem read GetItem; default;
  end;


  IevQECTask = interface(IevPrintableTask)
  ['{3940F850-CC40-43F7-A459-FBE26A1CA283}']
    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetQTREndDate: TDateTime;
    procedure SetQTREndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetCleanupAction: Integer;
    procedure SetCleanupAction(const AValue: Integer);
    function  GetEECustomNbr: String;
    procedure SetEECustomNbr(const AValue: String);
    function  GetForceCleanup: Boolean;
    procedure SetForceCleanup(const AValue: Boolean);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetCurrentQuarterOnly: Boolean;
    procedure SetCurrentQuarterOnly(const AValue: Boolean);
    function  GetExtraOptions: String;
    procedure SetExtraOptions(const AValue: String);
    function  GetFilter: IevQECTaskFilter;
    property  Filter: IevQECTaskFilter read GetFilter;
    property ClNbr: Integer read GetClNbr write SetClNbr;
    property CoNbr: Integer read GetCoNbr write SetCoNbr;
    property QTREndDate: TDateTime read GetQTREndDate write SetQTREndDate;
    property MinTax: Double read GetMinTax write SetMinTax;
    property CleanupAction: Integer read GetCleanupAction write SetCleanupAction;
    property EECustomNbr: String read GetEECustomNbr write SetEECustomNbr;
    property ForceCleanup: Boolean read GetForceCleanup write SetForceCleanup;
    property PrintReports: Boolean read GetPrintReports write SetPrintReports;
    property CurrentQuarterOnly: Boolean read GetCurrentQuarterOnly write SetCurrentQuarterOnly;
    property ExtraOptions: String read GetExtraOptions write SetExtraOptions;
  end;

  IevPAMCTask = interface(IevPrintableTask)
  ['{CB63918C-123B-4B62-9BEE-36F13C267CE3}']
    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetMonthEndDate: TDateTime;
    procedure SetMonthEndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetEECustomNbr: String;
    function  GetAutoProcess: Boolean;
    procedure SetAutoProcess(const AValue: Boolean);
    procedure SetEECustomNbr(const AValue: String);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetFilter: IevQECTaskFilter;
    property ClNbr: Integer read GetClNbr write SetClNbr;
    property CoNbr: Integer read GetCoNbr write SetCoNbr;
    property MonthEndDate: TDateTime read GetMonthEndDate write SetMonthEndDate;
    property MinTax: Double read GetMinTax write SetMinTax;
    property EECustomNbr: String read GetEECustomNbr write SetEECustomNbr;
    property AutoProcess: Boolean read GetAutoProcess write SetAutoProcess;
    property PrintReports: Boolean read GetPrintReports write SetPrintReports;
    property Filter: IevQECTaskFilter read GetFilter;
  end;

  IevRebuildVMRHistoryTask = interface(IevTask)
  ['{77B707C2-013E-4E77-B45E-F9F3007AF689}']
    function  GetPeriodBegDate: TDateTime;
    procedure SetPeriodBegDate(const AValue: TDateTime);
    function  GetPeriodEndDate: TDateTime;
    procedure SetPeriodEndDate(const AValue: TDateTime);
    function  GetCoList: IisStringList;
    property  PeriodBegDate: TDateTime read GetPeriodBegDate write SetPeriodBegDate;
    property  PeriodEndDate: TDateTime read GetPeriodEndDate write SetPeriodEndDate;
    property  CoList: IisStringList read GetCoList;
  end;

  IevACATaskCompanyFilterItem = interface
  ['{842D4083-8743-4ECE-8F7C-DCF819DE0634}']
    function  GetClNbr: Integer;
    function  GetCoFilter: String;
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoFilter(const AValue: String);
    property  ClNbr: Integer read GetClNbr write SetClNbr;
    property  CoFilter: String read GetCoFilter write SetCoFilter;
  end;

  IevACATaskCompanyFilter = interface
  ['{163FD0E9-73CB-4BCF-8023-40FD06BA4D8C}']
    function  GetItem(AIndex: Integer): IevACATaskCompanyFilterItem;
    function  Add: IevACATaskCompanyFilterItem;
    procedure Delete(const AItem: IevACATaskCompanyFilterItem);
    procedure Clear;
    function  Count: Integer;
    property  Items[Index: Integer]: IevACATaskCompanyFilterItem read GetItem; default;
  end;

  IevACATaskEEItem = interface
  ['{9A5D2DFC-0412-4BE1-B36A-87EDF15E68AA}']
   function  GetCO_NBR: Integer;
   function  GetEE_NBR: Integer;
   function  GetCUSTOM_EMPLOYEE_NUMBER:string;
   function  GetFIRST_NAME: string;
   function  GetLAST_NAME: string;
   function  GetOLD_ACA_STATUS: string;
   function  GetNEW_ACA_STATUS: string;
   function  GetUpdateType: string;

   procedure SetCO_NBR(const AValue: Integer);
   procedure SetEE_NBR(const AValue: Integer);
   procedure SetCUSTOM_EMPLOYEE_NUMBER(const AValue: string);
   procedure SetFIRST_NAME(const AValue: string);
   procedure SetLAST_NAME(const AValue: string);
   procedure SetOLD_ACA_STATUS(const AValue: string);
   procedure SetNEW_ACA_STATUS(const AValue: string);
   procedure SetUpdateType(const AValue: string);

   property  CO_NBR: Integer read GetCO_NBR write SetCO_NBR;
   property  EE_NBR: Integer read GetEE_NBR write SetEE_NBR;
   property  CUSTOM_EMPLOYEE_NUMBER: string read GetCUSTOM_EMPLOYEE_NUMBER write SetCUSTOM_EMPLOYEE_NUMBER;
   property  FIRST_NAME: string read GetFIRST_NAME write SetFIRST_NAME;
   property  LAST_NAME: string read GetLAST_NAME write SetLAST_NAME;
   property  OLD_ACA_STATUS: string read GetOLD_ACA_STATUS write SetOLD_ACA_STATUS;
   property  NEW_ACA_STATUS: string read GetNEW_ACA_STATUS write SetNEW_ACA_STATUS;
   property  UpdateType: string read GetUpdateType write SetUpdateType;

  end;

  IevACATaskCompanyEE = interface
  ['{8AFF7EC7-9145-4C99-B6B6-3219D5BC82BA}']
    function GetItem(AIndex: Integer): IevACATaskEEItem;
    function Add: IevACATaskEEItem;
    function Count: Integer;
    property Items[Index: Integer]: IevACATaskEEItem read GetItem; default;

    function  GetUpdateDate: TDateTime;
    function  GetClNbr: Integer;
    procedure SetUpdateDate(const AValue: TDateTime);
    procedure SetClNbr(const AValue: Integer);
    property  UpdateDate: TDateTime read GetUpdateDate write SetUpdateDate;
    property  ClNbr: Integer read GetClNbr write SetClNbr;
  end;

  IevACATaskResult = interface
  ['{67AB6BAB-E116-4BCC-92C6-24A51419B508}']
    function GetItem(AIndex: Integer): IevACATaskCompanyEE;
    function Add: IevACATaskCompanyEE;
    function Count: Integer;
    property Items[Index: Integer]: IevACATaskCompanyEE read GetItem; default;
  end;

  IevACAUpdateTask = interface(IevTask)
  ['{9DBADA67-052B-434B-8966-F00CCC0E8DFA}']
    function  GetFilter: IevACATaskCompanyFilter;
    function  GetResults: IevACATaskResult;
    property  Filter: IevACATaskCompanyFilter read GetFilter;
    property  Results: IevACATaskResult read GetResults;
  end;


  TProcessTaxReturnsFilterSelections = (fsAll, fsTaxOnly, fsNonTaxOnly, fsTaxDirect, fsCustom);
  TProcessTaxReturnsEeSort = (eesSSN, eesLastName);

  IevProcessTaxReturnsTask = interface(IevTask)
  ['{A9B4343A-F7C7-4691-A9A8-0C10D5057584}']
    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    procedure SetEndDate(const AValue: TDateTime);
    function  GetAskForRefund: Boolean;
    function  GetEeSortMode: TProcessTaxReturnsEeSort;
    procedure SetAskForRefund(const AValue: Boolean);
    procedure SetEeSortMode(const AValue: TProcessTaxReturnsEeSort);
    function  GetFilter: IisStringList;
    function  GetTaxFilter: TProcessTaxReturnsFilterSelections;
    procedure SetTaxFilter(const AValue: TProcessTaxReturnsFilterSelections);
    function  GetResults: IisListOfValues;
    function  GetSecureReport: Boolean;
    procedure SetSecureReport(const AValue: Boolean);
    property  Filter: IisStringList read GetFilter;
    property  TaxFilter: TProcessTaxReturnsFilterSelections read GetTaxFilter write SetTaxFilter;
    property  BegDate: TDateTime read GetBegDate;
    property  EndDate: TDateTime read GetEndDate write SetEndDate;
    property  EeSortMode: TProcessTaxReturnsEeSort read GetEeSortMode write SetEeSortMode;
    property  AskForRefund: Boolean read GetAskForRefund write SetAskForRefund;
    property  SecureReport: Boolean read GetSecureReport  write SetSecureReport;
    property  Results: IisListOfValues read GetResults;
  end;

  IevTaskRequest = interface
  ['{C33CE034-7E9B-411F-9AC2-6E4C86B99D1E}']
    function  RequestName: String;
    function  TaskRoutineName: String;
    function  NextTaskPhase: String;
    function  Params:  IisListOfValues;
    function  Results: IisListOfValues;
    function  GetExceptions: String;
    function  GetWarnings: String;
    function  GetStartedAt: TDateTime;
    function  GetFinishedAt: TDateTime;    
    property  StartedAt: TDateTime read GetStartedAt;
    property  FinishedAt: TDateTime read GetFinishedAt;
    function  State: TevTaskRequestState;
    function  GetProgressText: String;
    property  ProgressText: String read GetProgressText;
  end;


  IevTaskRequestInfo = interface
  ['{01FDE22E-90CE-4A08-B44B-2B92C7FB28A9}']
    function GetName: String;
    function GetProgressText: String;
    function GetStatus: String;
    function GetStartedAt: TDateTime;
    function GetFinishedAt: TDateTime;
    property Name: String read GetName;
    property StartedAt: TDateTime read GetStartedAt;
    property FinishedAt: TDateTime read GetFinishedAt;
    property Status: String read GetStatus;
    property ProgressText: String read GetProgressText;
  end;


  IevTaskInQueue = interface(IevTask)
  ['{4E2EC11A-D64A-4194-87CF-BE0959AD680C}']
    procedure  SetDefaults;
    function   TaskPulse: Boolean;
    procedure  Terminate;
    procedure  SetNbr(const AValue: Integer);
    procedure  SetLastUpdateToNow;
    function   GetRequests: IisList;
    procedure  SetTaskStorageLocation(const ALocation: String);
    procedure  ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
    procedure  DoOnActivate;
  end;


  IevTaskInfo = interface
  ['{12EAB65D-AE7D-47BD-9817-CEAC4A37BC7D}']
    function   GetID: TisGUID;
    function   GetNbr: Integer;
    function   GetTaskType: String;
    function   GetUser: String;
    function   GetDomain: String;
    function   GetPriority: Integer;
    function   GetNotificationEmail: string;
    function   GetSendEmailNotification: Boolean;
    function   GetEmailSendRule: TevEmailSendRule;
    function   GetLastUpdate: TDateTime;
    function   GetState: TevTaskState;
    function   GetCaption: String;
    function   GetProgressText: string;
    function   GetSeenByUser: Boolean;
    function   GetDaysToStayInQueue: Integer;
    function   GetFirstRunAt: TDateTime;
    property   ID: TisGUID read GetID;
    property   Nbr: Integer read GetNbr;
    property   TaskType: String read GetTaskType;
    property   User: String read GetUser;
    property   Domain: String read GetDomain;
    property   Priority: Integer read GetPriority;
    property   NotificationEmail: String read GetNotificationEmail;
    property   SendEmailNotification: Boolean read GetSendEmailNotification;
    property   EmailSendRule: TevEmailSendRule read GetEmailSendRule;
    property   State: TevTaskState read GetState;
    property   Caption: String read GetCaption;
    property   ProgressText: String read GetProgressText;
    property   LastUpdate: TDateTime read GetLastUpdate;
    property   SeenByUser: Boolean read GetSeenByUser;
    property   DaysToStayInQueue: Integer read GetDaysToStayInQueue;
    property   FirstRunAt: TDateTime read GetFirstRunAt;
  end;


  IevTaskQueue = interface
  ['{5B01D782-89DC-47AB-94C2-E0BC8014A5E1}']
    function  CreateTask(const ATaskType: String; const ApplyDefaults: Boolean): IevTask;
    procedure AddTask(const ATask: IevTask);
    procedure AddTasks(const ATaskList: IisList); // list of IevTask
    procedure RemoveTask(const AID: TisGUID);
    function  GetTaskInfo(const AID: TisGUID): IevTaskInfo;
    function  GetTask(const AID: TisGUID): IevTask;
    function  GetTaskRequests(const AID: TisGUID): IisListOfValues;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure Pulse;
    function  GetUserTaskStatus: IisListOfValues;
    function  GetUserTaskInfoList: IisListOfValues;  // list of IevTaskInfo
    procedure ModifyTaskProperty(const AID: TisGUID; const APropName: String; const ANewValue: Variant);
    property  Active: Boolean read GetActive write SetActive;
  end;


  IevAsyncCallInfo = interface
  ['{D6BCCF06-5555-44A3-80C8-02D13B89BA14}']
    function  Name: String;
    function  CallID: TisGUID;
    function  Priority: Integer;
    function  User: String;
    function  StartTime: TDateTime;    
  end;


  IevAsyncFunctions = interface
  ['{CA99B408-385F-41C5-BF5F-60E4FCB62453}']
    function  MakeCall(const AFunctionName: String; const AParams: IisListOfValues; const APriority: Integer; var ACallID: TisGUID): Boolean;
    function  SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
    function  GetCalcCapacity: Integer;
    procedure SetCalcCapacity(const AValue: Integer);
    procedure TerminateCall(const ACallID: TisGUID);
    function  GetExecSatus: IisList; //list of IevAsyncCallInfo
    property  CalcCapacity: Integer read GetCalcCapacity write SetCalcCapacity;
  end;


  IevTaskScheduler = interface
  ['{84FA307B-207F-4D94-B960-E1337B266138}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure RefreshTaskList;
    property  Active: Boolean read GetActive write SetActive;
  end;


  IevUserScheduler = interface
  ['{BC50ED79-C253-4136-BE08-EFED3BE3EB36}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure RefreshTaskList;
    procedure DismissEvent(const AEventID: String);
    procedure SnoozeEvent(const AEventID: String; const ASnoozeUntil: TDateTime);
    function  GetEventsInfo(const ACutOffDate: TDateTime): IisParamsCollection;
    property  Active: Boolean read GetActive write SetActive;
  end;


  IevProxyModule = interface
  ['{D9303866-4876-4BD7-A7F0-35F6E38301CB}']
    function  Connected: Boolean;
    procedure OnDisconnect;  // Call it manually where it needs 
  end;

  IevAPIKey = interface;

  // License
  IevLicense = interface
  ['{E6A5EE61-7126-4B66-87BB-E243B8D7671E}']
    procedure Refresh;
    procedure Check;
    function  GetLicenseInfo: IisListOfValues;
    function  ADPImport: Boolean;
    function  WebPayroll: Boolean;
    function  HR: Boolean;
    function  VMR: Boolean;
    function  PaysuiteImport: Boolean;
    function  CTSReports: Boolean;
    function  SelfServe: Boolean;
    function  EvoX: Boolean;
    function  Benefits: Boolean;
    function  FindAPIKeyByName(const AAPIKey: TisGUID): IisListOfValues;
    function  FindAPIKey(const AAPIKey: TevVendorKeyType): IisListOfValues;
  end;


  // Time Source
  IevTimeSource = interface
  ['{C3093F4C-2920-4921-8752-F671E53893BD}']
    function  GetDateTime: TDateTime;
  end;


  TevCBEventType = (cetAnyEvent, cetGlobalFlagEvent, cetTaskQueueEvent);

  IevGlobalCallbacks = interface
  ['{A05D29E8-70A7-4759-AF6A-D9927714A3F0}']
    procedure Subscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String);
    procedure Unsubscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String);
    procedure NotifySecurityChange(const aChangedItem: String);
    procedure NotifyTaskSchedulerChange(const aChangedItem: String);
    procedure NotifyTaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
    procedure NotifyGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure NotifyGlobalSettingsChange(const aChangedItem: String);
    procedure NotifyPopupMessage(const aText, aFromUser: String; const aToUser: String);
    procedure NotifySetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean);
    procedure SecurityChange(const aChangedItem: String);
    procedure TaskSchedulerChange(const aChangedItem: String);
    procedure TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
    procedure GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure GlobalSettingsChange(const aChangedItem: String);
    procedure PopupMessage(const aText, aFromUser: String; const aToUser: String);
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean);
  end;


  IevContextCallbacks = interface
  ['{EA1C541A-3C50-46C2-B5EB-8ED5B6A3FF51}']
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
  end;


  // Statistics module

  IevStatistics = interface(IisStatistics)
  ['{A09853E5-89F1-44B4-952E-87F1BDF3D680}']
    function  GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetDumpIntoFile: Boolean;
    procedure SetDumpIntoFile(const AValue: Boolean);
    function  GetFileName: String;
    procedure SetFileName(const AValue: String);
    property  Enabled: Boolean read GetEnabled write SetEnabled;
    property  FileName: String read GetFileName write SetFileName;
    property  DumpIntoFile: Boolean read GetDumpIntoFile write SetDumpIntoFile;
  end;


  IEvExchange = interface;
  IEvExchangeEngine = interface;

  // Context
  IevContext = interface
  ['{1DC855E6-D3FF-4AFB-9693-6963266C0788}']
    function  GetID: TisGUID;
    function  GetRemoteID: TisGUID;
    function  Callbacks: IevContextCallbacks;
    procedure SetCallbacks(const AValue: IevContextCallbacks);
    function  RunParallelTask(const Task: PTaskProc; const AParams: TTaskParamList; const ATaskDescription: String = '';
                              const APriority: TThreadPriority = tpNormal; const AThreadManager: IThreadManager = nil): TTaskID; overload;
    function  RunParallelTask(const Task: PTaskProcObj; const AObject: TObject;  const AParams: TTaskParamList;
                              const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal;
                              const AThreadManager: IThreadManager = nil): TTaskID; overload;
    procedure WaitForTaskEnd(const ATaskID: TTaskID = 0);
    function  TaskIsFinished(const ATaskID: TTaskID = 0): Boolean;
    function  UserAccount: IevUserAccount;
    function  GetScreenPackage(const AModuleType: TGUID): IevScreenPackage;
    function  EMailer: IevEMailer;
    function  EvolutionGUI: IevEvolutionGUI;
    function  DataAccess: IevDataAccess;
    function  DBAccess: IevDBAccess;
    function  Security: IevSecurity;
    function  DomainInfo: IevDomainInfo;
    function  PayrollCalculation: IevPayrollCalculation;
    function  PayrollProcessing: IevPayrollProcessing;
    function  PayrollCheckPrint: IevPayrollCheckPrint;
    function  RemoteMiscRoutines: IevRemoteMiscRoutines;
    function  VmrEngine: IevVmrEngine;
    function  VmrRemote: IevVmrRemote;
    function  RWDesigner: IevRWDesigner;
    function  RWRemoteEngine: IevRWRemoteEngine;
    function  RWLocalEngine: IevRWLocalEngine;
    function  License: IevLicense;
    function  EvolutionExchange: IEvExchange;
    function  EvolutionExchangeEngine: IEvExchangeEngine;
    function  UserScheduler: IevUserScheduler;
    function  Statistics: IevStatistics;
    function  CashManagement: IevCashManagement;
    function  SingleSignOn: IevSingleSignOn;
  end;


  // Settings

  IevFileFolders = interface
  ['{0E93D089-94A0-4333-8A88-51AB81A39BD0}']
    function  GetTaskQueueFolder: String;
    procedure SetTaskQueueFolder(const AValue: String);
    function  GetVMRFolder: String;
    procedure SetVMRFolder(const AValue: String);
    function  GetACHFolder: String;
    procedure SetACHFolder(const AValue: String);
    function  GetASCIIPrefix: String;
    procedure SetASCIIPrefix(const AValue: String);
    property  TaskQueueFolder: String read GetTaskQueueFolder write SetTaskQueueFolder;
    property  VMRFolder: String read GetVMRFolder write SetVMRFolder;
    property  ACHFolder: String read GetACHFolder write SetACHFolder;
    property  ASCIIPrefix: String read GetASCIIPrefix write SetASCIIPrefix;
  end;

  IevSystemAccountInfo = interface
  ['{83851EEC-D39C-40B8-9710-58C1A0F11CAF}']
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
    property  UserName: String read GetUserName write SetUserName;
    property  Password: String read GetPassword;
  end;

  IevDBLocation = interface
  ['{7017E01C-ADA3-4DAE-ACE9-0159B5534FF0}']
    function  GetDBType: TevDBType;
    procedure SetDBType(const AValue: TevDBType);
    function  GetPath: String;
    procedure SetPath(const AValue: String);
    function  GetBeginRangeNbr: Cardinal;
    procedure SetBeginRangeNbr(const AValue: Cardinal);
    function  GetEndRangeNbr: Cardinal;
    procedure SetEndRangeNbr(const AValue: Cardinal);
    property  DBType: TevDBType read GetDBType write SetDBType;
    property  Path: String read GetPath write SetPath;
    property  BeginRangeNbr: Cardinal read GetBeginRangeNbr write SetBeginRangeNbr;
    property  EndRangeNbr: Cardinal read GetEndRangeNbr write SetEndRangeNbr;
  end;

  IevDBLocationList = interface
  ['{BC745D97-DCAD-49CF-BF31-201F74DBEE7D}']
    procedure Lock;
    procedure Unlock;    
    procedure Clear;
    function  Count: Integer;
    function  Add: IevDBLocation;
    procedure Delete(const ADBLocation: IevDBLocation);
    function  GetItem(AIndex: Integer): IevDBLocation;
    function  GetDBPath(const ADatabaseName: String): String;
    property  Items[Index: Integer]: IevDBLocation read GetItem; default;
  end;

  IevLicenseInfo = interface
  ['{4E749F47-04BC-4A17-938D-8B319A30A104}']
    function  GetSerialNumber: String;
    procedure SetSerialNumber(const AValue: String);
    function  GetCode: String;
    procedure SetCode(const AValue: String);
    function  GetLastChange: String;
    procedure SetLastChange(const AValue: String);
    property  SerialNumber: String read GetSerialNumber write SetSerialNumber;
    property  Code: String read GetCode write SetCode;
    property  LastChange: String read GetLastChange write SetLastChange;
  end;


  IevBrandInfo = interface
  ['{44BD7518-1859-490F-BE65-C2C8BC4F6A7C}']
    procedure SetLogoPicture(const APicture: TGraphicData);
    procedure SetTitlePicture(const APicture: TGraphicData);
    function  GetLogoPicture: TGraphicData;
    function  GetTitlePicture: TGraphicData;
    property  LogoPicture:TGraphicData read GetLogoPicture write SetLogoPicture;
    property  TitlePicture:TGraphicData read GetTitlePicture write SetTitlePicture;
  end;

  IevDomainInfo = interface
  ['{6107544E-CC6B-4952-A477-FBBAC6CFAD7A}']
    function  GetDomainName: String;
    function  GetDomainDescription: String;
    procedure SetDomainName(const AValue: String);
    procedure SetDomainDescription(const AValue: String);
    function  GetDBChangesBroadcast: Boolean;
    procedure SetDBChangesBroadcast(const AValue: Boolean);
    function  DBLocationList: IevDBLocationList;
    function  ADRDBLocationList: IevDBLocationList;
    function  FileFolders: IevFileFolders;
    function  LicenseInfo: IevLicenseInfo;
    function  BrandInfo: IevBrandInfo;
    function  SystemAccountInfo: IevSystemAccountInfo;
    function  GetInternalUsersThroughRR: Boolean;
    procedure SetInternalUsersThroughRR(const AValue: Boolean);
    function  GetAdminAccount: String;
    procedure SetAdminAccount(const AValue: String);
    property  DBChangesBroadcast: Boolean read GetDBChangesBroadcast write SetDBChangesBroadcast;
    property  DomainName: String read GetDomainName write SetDomainName;
    property  DomainDescription: String read GetDomainDescription write SetDomainDescription;
    property  InternalUsersThroughRR: Boolean read GetInternalUsersThroughRR write SetInternalUsersThroughRR;
    property  AdminAccount: String read GetAdminAccount write SetAdminAccount;
  end;


  IevDomainInfoList = interface
  ['{18E909E3-15F2-4A43-A5E9-0A0D316F88E7}']
    procedure Lock;
    procedure Unlock;
    function  Count: Integer;
    function  Add: IevDomainInfo;
    procedure Delete(const ADomain: IevDomainInfo);
    function  GetDomainInfo(const ADomainName: String): IevDomainInfo;
    function  GetItem(AIndex: Integer): IevDomainInfo;
    property  Items[Index: Integer]: IevDomainInfo read GetItem; default;
  end;


  IevEMailInfo = interface
  ['{FEA631A0-3DD1-4A57-810C-1D4AF164595D}']
    function  GetSMTPServer: String;
    procedure SetSMTPServer(const AValue: String);
    function  GetSMTPPort: String;
    procedure SetSMTPPort(const AValue: String);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetNotificationDomainName: String;
    procedure SetNotificationDomainName(const AValue: String);
    property  SMTPServer: String read GetSMTPServer write SetSMTPServer;
    property  SMTPPort: String read GetSMTPPort write SetSMTPPort;
    property  UserName: String read GetUserName write SetUserName;
    property  Password: String read GetPassword write SetPassword;
    property  NotificationDomainName: String read GetNotificationDomainName write SetNotificationDomainName;
  end;


  IevSettings = interface
  ['{B80EE91F-A4C4-48D4-BCAB-0483F75C1A37}']
    procedure Load;
    procedure Save;
    function  GetEnvironmentID: String;
    procedure SetEnvironmentID(const AValue: String);
    function  GetDBConPoolSize: Integer;
    procedure SetDBConPoolSize(const AValue: Integer);
    function  GetDBAdminPassword: String;
    procedure SetDBAdminPassword(const AValue: String);
    function  GetDBUserPassword: String;
    procedure SetDBUserPassword(const AValue: String);
    function  DomainInfoList: IevDomainInfoList;
    function  EMailInfo: IevEMailInfo;
    function  InternalRR: IisStringList;
    property  EnvironmentID: String read GetEnvironmentID write SetEnvironmentID;
    property  DBConPoolSize: Integer read GetDBConPoolSize write SetDBConPoolSize;
    property  DBAdminPassword: String read GetDBAdminPassword write SetDBAdminPassword;
    property  DBUserPassword: String read GetDBUserPassword write SetDBUserPassword;
  end;

  // RAS Access
  IevRASAccessGUI = interface
  ['{E188566D-32EF-465A-BADC-6A56406E8345}']
    procedure UpdateRASStatus(const AStatus: String);
    procedure UpdateRASDetails(const ADetails: String);
    procedure ShowMessage(const AText: String);
    procedure OnBusyConnection(const AConnectionName: String; var ADisconnect: Boolean);
  end;

  IevRASAccess = interface
  ['{D6E4D327-A602-4861-917D-6F776B71F2E8}']
    function  GetRASEntries: string;
    procedure BecomeIdle;
    procedure DialRasConnection(ConnectionName, UserName, Password: string);
    procedure HangupRasConnection(ConnectionName: string);
    procedure SetConnection(ConnectionName: string);
    function  GetFirstConnectionName: string;
    function  RefreshStatus: Boolean;
    function  IsInternallyConnected: Boolean;
    procedure SetGUICallback(const AGUICallback: IevRASAccessGUI);
  end;


  // Evo Version Update
  IevVersionUpdateRemote = interface
  ['{9740CFAA-7C53-42E8-9EA6-08D58BC6EA33}']
    function  GetVersionInfo: IisListOfValues;
    function  Check(const aAppFileInfo: IisParamsCollection; out aUpdateInfo: IisParamsCollection): Boolean;
    function  GetUpdatePack(const aAppFileInfo: IisParamsCollection): IisParamsCollection;
  end;

  IevVersionUpdateLocal = interface
  ['{936CC0AD-6707-47DD-A1C5-710EE835CE39}']
    function  CheckIfVersionValid(out ARequiredVersion: String): Boolean;
    procedure PrepareUpdate(out aUpdatePackFileName: String);
    procedure ApplyUpdate(const aUpdatePackFileName, aAppFile: String);
    function  PrepareUpdateIfNeeds(const AskConfirmation: Boolean): Boolean;
    function  PerformUpdateIfNeeds: Boolean;
  end;


  // ADR
  TevADRDBStatus = (dsUnknown, dsAbsent, dsPreparing, dsPresent, dsMaintenance, dsError);

  IevADRDBInfo = interface
 ['{84E13529-18A5-48F1-AD07-0A63BC241677}']
    procedure SetDatabase(const AValue: String);
    function  GetDatabase: String;
    procedure SetVersion(const AValue: String);
    function  GetVersion: String;
    procedure SetLastTransactionNbr(const AValue: Integer);
    function  GetLastTransactionNbr: Integer;
    procedure SetStatus(const AValue: TevADRDBStatus);
    function  GetStatus: TevADRDBStatus;
    property  Database: String read GetDatabase write SetDatabase;
    property  Version: String read GetVersion write SetVersion;
    property  LastTransactionNbr: Integer read GetLastTransactionNbr write SetLastTransactionNbr;
    property  Status: TevADRDBStatus read GetStatus write SetStatus;
  end;


  IevADRClient = interface
  ['{C7234BD5-3BA4-457C-8D64-A15D403F03E6}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisList;  // list of IevADRRequest
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    function  GetDBList: IisStringList;
    procedure SyncDBList(const ATopPosition: Boolean = False);
    procedure CheckDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATopPosition: Boolean = False);
    procedure FullDBCopy(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ACheckQueue: Boolean = False; const ATopPosition: Boolean = False);
    procedure SynchronizeDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATargetDBVersion: String;
                            const ATargetLastTransactionNbr: Integer; const ACheckQueue: Boolean = False;
                            const ATopPosition: Boolean = False);
    procedure CheckDBData(const ADataBase: String; const ATopPosition: Boolean = False);
    procedure CheckDBTransactions(const ADataBase: String; const ATopPosition: Boolean = False);
    function  GetStatus: IisListOfValues;
    property  Active: Boolean read GetActive write SetActive;
  end;


  IevADRServer = interface
  ['{F0C5F3FC-AF0E-4DC0-A288-257825369252}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisList;  // list of IevADRRequest
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SyncDBList(const ADBList: IisStringList);
    procedure RestoreDB(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer;
                        const ADBData: IisStream);
    procedure ApplySyncData(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ASyncData: IisStream);
    procedure CheckDBData(const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ADBData: IisStream);
    procedure CheckDBTransactions(const ADataBase, ADBVersion: String; const ALastTransactionNbr, ATransCount: Integer);
    procedure DeleteDBRequests(const ADatabase: String);
    function  GetStatus: IisListOfValues;
    procedure RebuildTT(const AOriginatedAt:TUTCDateTime; const ADatabase: String);
    property  Active: Boolean read GetActive write SetActive;
  end;




  // XML RPC
  IevXmlRpcServer = interface
  ['{30A78B5C-77F4-427B-A3FD-087183F55C39}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetSSLEnable: Boolean;
    procedure SetSSLEnable(const AValue: Boolean);
    function  GetSSLRootCertFile: String;
    procedure SetSSLRootCertFile(const AValue: String);
    function  GetSSLCertFile: String;
    procedure SetSSLCertFile(const AValue: String);
    function  GetSSLKeyFile: String;
    procedure SetSSLKeyFile(const AValue: String);
    function  GetRBHost: String;
    procedure SetRBHost(const AValue: String); 
    function  GetPort: String;
    procedure SetPort(const APort: String);
    property  Active: Boolean read GetActive write SetActive;
    property  SSLEnable: Boolean read GetSSLEnable write SetSSLEnable;
    property  SSLRootCertFile: String read GetSSLRootCertFile write SetSSLRootCertFile;
    property  SSLCertFile: String read GetSSLCertFile write SetSSLCertFile;
    property  SSLKeyFile: String read GetSSLKeyFile write SetSSLKeyFile;
  end;


  // Client for Big Brother
  IevBBClient = interface
  ['{FBCE5C40-2670-4885-94A2-4C3B1271A63D}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  Connected: boolean;
    function  DiagnosticCheck: String;    
    property  Active: Boolean read GetActive write SetActive;
  end;


  IEvExchangeScheduler = interface;
  IevDBMaintenance = interface;
  IevDashboardDataProvider = interface;

  // Mainboard
  IevMainboard = interface
  ['{362F5ABC-F486-4F0B-AE1D-4D56D26EFE97}']
    procedure InitializeModules;
    procedure DeinitializeModules;
    procedure ActivateModules;
    procedure DeactivateModules;
    function  AppConfiguration:   IisSettings;
    function  AppSettings:        IisSettings;
    function  LogFile:            IevLogFile;
    function  Messenger:          IisMessenger;
    function  MachineInfo:        IevMachineInfo;
    function  ModuleRegister:     IevModuleRegister;
    function  ContextManager:     IevContextManager;
    function  GlobalSettings:     IevSettings;
    function  GlobalCallbacks:    IevGlobalCallbacks;
    function  TimeSource:         IevTimeSource;
    function  TaskQueue:          IevTaskQueue;
    function  TaskScheduler:      IevTaskScheduler;
    function  AsyncFunctions:     IevAsyncFunctions;
    function  GlobalFlagsManager: IevGlobalFlagsManager;
    function  XmlRPCServer:       IevXmlRPCServer;
    function  RASAccess:          IevRASAccess;
    function  VersionUpdateLocal: IevVersionUpdateLocal;
    function  VersionUpdateRemote:IevVersionUpdateRemote;
    function  TCPClient:          IevTCPClient;
    function  TCPServer:          IevTCPServer;
    function  BBClient:           IevBBClient;
    function  EvoXScheduler:      IEvExchangeScheduler;
    function  ADRClient:          IevADRClient;
    function  ADRServer:          IevADRServer;
    function  DBMaintenance:      IevDBMaintenance;
    function  DashboardDataProvider: IevDashboardDataProvider;
  end;

  IevAPIKey = interface
  ['{D5DF1984-623F-4448-867D-2D8AC22D6DCF}']
    function GetAPIKey : TisGUID;
    function GetAPIKeyType : TevAPIKeyType;
    function GetVendorCustomNbr : integer;
    function GetVendorName : String;
    function GetApplicationName : String;

    procedure SetAPIKey(AAPIKey : TisGUID);
    procedure SetAPIKeyType(AAPIKeyType : TevAPIKeyType);
    procedure SetVendorCustomNbr(AVendorCustomNbr : integer);
    procedure SetVendorName(AVendorName : String);
    procedure SetApplicationName(AAplicationName : String);

    property APIKey : TisGUID read GetAPIKey write SetAPIKey;
    property APIKeyType : TevAPIKeyType read GetAPIKeyType write SetAPIKeyType;
    property ApplicationName : String read GetApplicationName write SetApplicationName;
    property VendorCustomNbr : integer read GetVendorCustomNbr write SetVendorCustomNbr; // 0 - is reserved for iSystems
    property VendorName : String read GetVendorName write SetVendorName;
  end;

  ////////////////////////////////////////////////
  // Evolution Exchange
  /////////////////////////////////////////////////

  TEvImportMapType = (mtpDirect, mtpLookup, mtpMap, mtpCustom, mtpMapLookup, mtpMapCustom); // replacement for IEMap.TMapType
  TEvoXAsOfDateSourceType = (stDateEditor, stMappedField);

  // replacement for IEMap.TMapRecord
  IEvImportMapRecord = interface(IisInterfacedObject)
  ['{A9ABAEDA-C364-426A-B025-403289E57BA6}']
    function GetEField: string;
    function GetETable: string;
    function GetFuncName: String;
    function GetIField: string;
    function GetITable: string;
    function GetKeyField: string;
    function GetKeyLookupField: string;
    function GetKeyLookupTable: string;
    function GetKeyValue: string;
    function GetGroupName: string;
    function GetRecordNumber: integer;
    function GetLookupTable: String;
    function GetLookupField: String;
    function GetLookupField1: String;
    function GetLookupTable1: String;
    function GetMapDefault: String;
    function GetMapType: TEvImportMapType;
    function GetMapTypeName: String;
    function GetMapValues: String;
    function GetMLookupField: String;
    function GetMLookupTable: String;
    function GetMMapDefault: String;
    function GetMMapValues: String;
    function GetDescription: String;
    function GetMapRecordName: String;
    function GetXML: String;

    function IsVisibleToUser: boolean;

    procedure SetEField(const Value: string);
    procedure SetETable(const Value: string);
    procedure SetFuncName(const Value: String);
    procedure SetIField(const Value: string);
    procedure SetITable(const Value: string);
    procedure SetKeyField(const Value: string);
    procedure SetKeyLookupField(const Value: string);
    procedure SetKeyLookupTable(const Value: string);
    procedure SetKeyValue(const Value: string);
    procedure SetGroupName(const Value: string);
    procedure SetRecordNumber(const Value: integer);
    procedure SetLookupTable(const Value: String);
    procedure SetLookupField(const Value: String);
    procedure SetLookupField1(const Value: String);
    procedure SetLookupTable1(const Value: String);
    procedure SetMapDefault(const Value: String);
    procedure SetMapType(const Value: TEvImportMapType);
    procedure SetMapValues(const Value: String);
    procedure SetMLookupField(const Value: String);
    procedure SetMLookupTable(const Value: String);
    procedure SetMMapDefault(const Value: String);
    procedure SetMMapValues(const Value: String);
    procedure SetDescription(const Value: String);
    procedure SetMapRecordName(const Value: String);
    procedure SetVisibleToUser(const Value: boolean);

    property ITable: string read GetITable write SetITable;
    property IField: string read GetIField write SetIField;
    property ETable: string read GetETable write SetETable;
    property EField: string read GetEField write SetEField;
    property KeyField: string read GetKeyField write SetKeyField;
    property KeyLookupTable: string read GetKeyLookupTable write SetKeyLookupTable;
    property KeyLookupField: string read GetKeyLookupField write SetKeyLookupField;
    property KeyValue: string read GetKeyValue write SetKeyValue;
    property GroupName: string read GetGroupName write SetGroupName;
    property RecordNumber: integer read GetRecordNumber write SetRecordNumber;

    property MapType: TEvImportMapType read GetMapType write SetMapType;
    // MapType = mtpCustom
    property FuncName: String read GetFuncName write SetFuncName;
    // MapType = mtpMap
    property MapValues: String read GetMapValues write SetMapValues;
    property MapDefault: String read GetMapDefault write SetMapDefault;
    // MapType = mtpLookup
    property LookupTable: String read GetLookupTable write SetLookupTable;
    property LookupField: String read GetLookupField write SetLookupField;
    property LookupTable1: String read GetLookupTable1 write SetLookupTable1;
    property LookupField1: String read GetLookupField1 write SetLookupField1;
    // MapType = mtpMapLookup
    property MLookupTable: String read GetMLookupTable write SetMLookupTable;
    property MLookupField: String read GetMLookupField write SetMLookupField;
    property MMapValues: String read GetMMapValues write SetMMapValues;
    property MMapDefault: String read GetMMapDefault write SetMMapDefault;

    property Description: String read GetDescription write SetDescription;
  end;

  IevEchangeMapField = interface(IisInterfacedObject)
  ['{E3C7E75E-BCB0-4423-8607-E37A33A78BFC}']
    function  GetInputFieldName: String;
    function  GetFieldType: TDataDictDataType;
    function  GetDBFieldType: TFieldType;
    function  GetFieldSize: integer; // zero for all types except Strings
    function  GetDateFormat: String;
    function  GetTimeFormat: String;
    function  GetExchangeFieldName: String;
    function  GetMapRecord: IEvImportMapRecord;
    function  GetGroupRecordNumber: integer;
    function  GetXML: String;
    function  GetAsOfDateEnabled: boolean;
    function  GetAsOfDateSource: TEvoXAsOfDateSourceType;
    function  GetAsOfDateValue: TDateTime;
    function  GetAsOfDateField: String;  // it's name of input field
    function  GetAsOfDateFieldDateFormat: String;

    procedure SetInputFieldName(const AFieldName: String);
    procedure SetFieldType(const AFieldType: TDataDictDataType);
    procedure SetFieldSize(const AFieldSize: integer);
    procedure SetDateFormat(const ADateFormat: String);
    procedure SetTimeFormat(const ATimeFormat: String);
    procedure SetExchangeFieldName(const AFieldName: String);
    procedure SetMapRecord(const AMapRecord: IEvImportMapRecord);
    procedure SetGroupRecordNumber(const Value: integer);
    procedure SetAsOfDateEnabled(const Value: boolean);
    procedure SetAsOfDateSource(const Value: TEvoXAsOfDateSourceType);
    procedure SetAsOfDateValue(const Value: TDateTime);
    procedure SetAsOfDateField(const Value: String); // it's name of input field
    procedure SetAsOfDateFieldDateFormat(const Value: String);

    procedure DoReconcile;
  end;

  IEvExchangeMapFile = interface(IisInterfacedObject)
  ['{9C3E0CB1-26A4-4A34-88AB-06C2E5154C99}']
    function  GetDescription: String;
    function  GetPackageName: String;
    function  GetPackageVersion: integer;
    function  GetExampleFileName: String;
    function  GetExampleData: IEvDualStream;
    function  GetDataSourceName: String;
    function  GetDataSourceOptions: IisStringList;
    function  GetFieldsList: IisListOfValues;
    function  GetFieldByExchangeName(const AExchangeFieldName: String): IevEchangeMapField;
    function  GetMapFileOptions: IisStringList;
    function  GetXML: String;

    procedure SetDescription(ADescription: String);
    procedure SetPackageInformation(APackageInformation: IEvExchangePackage);
    procedure SetExampleFileName(AExampleFileName: String);
    procedure SetExampleData(AExampleData: IEvDualStream);
    procedure SetDataSourceName(ADataSourceName: String);
    procedure SetDataSourceOptions(ADataSourceOptions: IisStringList);
    procedure SetMapFileOptions(AMapFileOptions: IisStringList);

    procedure AddField(const AField: IevEchangeMapField);
    procedure DeleteFieldByExchangeName(const AExchangeFieldName: String);

    procedure DoReconcile;
  end;

  IEvExchangePackageField = interface(IisInterfacedObject)
  ['{8341F566-BA9C-4B59-A01F-87E036D1B6C4}']
    function  GetExchangeFieldName : String;
    function  GetDisplayName: String;
    function  GetEvTableName: String;
    function  GetEvFieldName: String;
    function  GetEvDisplayName: String;
    function  GetDDField: TddsField;
    function  GetType: TDataDictDataType;
    function  GetTypeName: String;
    function  GetFieldSize: integer;
    function  GetFieldValues: IisListOfValues;
    function  GetDefaultValue: String;
    function  GetRequiredByDD: boolean;
    function  GetRelatedParentField: String;
    function  GetMapRecord(AMapRecordName: String): IEvImportMapRecord;
    function  GetFirstMapRecordByType(const AMapRecordType: TEvImportMapType): IEvImportMapRecord;
    function  GetMapRecords: IisListOfValues;
    function  GetMapRecordsByType(const AMapType: TEvImportMapType): IisListOfValues;
    function  GetDefaultMapRecordName: String;
    function  GetPackageMapRecordName: String;
    function  GetKeyTables: IisStringListRO;
    function  GetKeyFields: IisStringListRO;
    function  GetKeyValues: IisStringListRO;
    function  GetSectionRecords: IisStringListRO;
    function  GetDescription: String;
    function  GetIgnoreRequiredByDD: boolean;
    function  GetCheckRequiredByDD: boolean;
    function  GetUseEvoNamesForMap: boolean;
    function  GetTrimInputSpaces: boolean;
    function  GetAlwaysRequired: boolean;
    function  GetGroupName: String;
    function  GetRecordNumber: integer;
    function  GetDefExchangeFieldName: String;
    function  GetPreFormatFunctionName: String;
    function  GetAsOfDateEnabled: boolean;
    function  GetRequiredIfAnyMappedForTable: boolean;
    function  GetRequiredIfAnyMappedForOtherTable: String;
    function  GetDelayedPostTableNumber: integer;
    function  GetNotPostThisFieldFlag: boolean;
    function  GetLookupTableName: String;
    function  GetLookupFieldName: String;
    function  GetXML: String;

    function  IsCheckRuleForRequirement: boolean;
    function  IsCalcDefault: boolean;
    function  IsKeyField: boolean;
    function  IsGroupDefinitionField: boolean;
    function  IsBelongsToGroup: boolean;
    function  IsBelongsToSection(const aSectionName: string): boolean;

    procedure SetDisplayName(const Value: String);
    procedure SetDescription(const Value: String);
    procedure SetCheckRuleForRequiremet(const Value: boolean);
    procedure SetRelatedparentField(const Value: String);
    procedure SetCalcDefault(AValue: boolean);
    procedure SetMapRecords(AValue: IisListOfValues);
    procedure SetDefaultMapRecordName(const AName: String);
    procedure SetPackageMapRecordName(const AName: String);
    procedure SetKeys(AKeyTables: IisStringListRO; AKeyFields: IisStringListRO; AKeyValues: IisStringListRO);
    procedure SetSectionRecords(ASectionRecords: IisStringListRO);
    procedure SetFieldSize(const Value: integer);
    procedure SetAsKeyField(const Value: boolean);
    procedure SetIgnoreRequiredByDD(const Value: boolean);
    procedure SetCheckRequiredByDD(const Value: boolean);
    procedure SetUseEvoNamesForMap(const Value: boolean);
    procedure SetTrimInputSpaces(const Value: boolean);
    procedure SetAlwaysRequired(const Value: boolean);
    procedure SetGroupName(const Value: String);
    procedure SetRecordNumber(const Value: integer);
    procedure SetDefExchangeFieldName(const Value: String);
    procedure SetPreFormatFunctionName(const Value: String);
    procedure SetAsOfDateEnabled(const Value: boolean);
    procedure SetRequiredIfAnyMappedForTable(const Value: boolean);
    procedure SetRequiredIfAnyMappedForOtherTable(const ATableName: String);
    procedure SetDelayedPostTableNumber(const AValue: integer);
    procedure SetNotPostThisFieldFlag(const AValue: boolean);
    procedure SetLookupData(const ATableName, FieldName: String; AFieldSize: integer = 0);

    procedure DoReconcile;
  end;

  IEvExchangePackageGroup = interface(IisInterfacedObject)
  ['{76FE9EF4-EE65-4DA8-82BE-F3B3946036D6}']
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetXML: String;

    procedure DoReconcile;
  end;

  IEvExchangePackageSection = interface(IisInterfacedObject)
  ['{904546FE-A312-4257-999A-EA161D1C50F6}']
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetXML: String;

    procedure DoReconcile;
  end;

  IEvExchangePackage = interface(IisInterfacedObject)
  ['{E3CC0175-46E7-4CB1-963E-72E3ED2348F9}']
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetVersion: integer;
    function  GetFieldsList: IisListOfValues;
    function  GetFieldByName(const AFieldName: String): IEvExchangePackageField;
    function  GetEngineName: String;

    function  GetXML: String;

    function  GetGroupsList: IisListOfValues;
    function  GetSectionsList: IisListOfValues;
    function  GetGroupByName(const AGroupName: String): IEvExchangePackageGroup;
    function  GetSectionByName(const ASectionName: String): IEvExchangePackageSection;
    function  GetFieldsDefsByGroupName(const AGroupName: String): IisListOfValues;
    function  GetFieldsDefsBySectionName(const ASectionName: String): IisListOfValues;
    function  AddNewRecordToGroup(const AGroupName: String): integer;
    procedure AddRecordWithNumberToGroup(const AGroupName: String; const ArecordNumber: integer);
    procedure DeleteRecordFromGroup(const AGroupName: String; const ARecordNumber: integer);
    procedure DeleteAllGroupRecords;
    function  isGroupRecordExist(const AGroupName: String; const ARecordNumber: integer): boolean;
    function  GetGroupRecordNumbers(const AGroupName: String): TisDynIntegerArray;
    function  GetListOfEvAndVirtualTables: IisStringList;
    function  GetFieldsForEvOrVirtualTable(const AEvTable: String): IisListOfValues;

    procedure DoReconcile;
  end;

  IEvExchangeResultLogEvent = interface(IisInterfacedObject)
  ['{0ABF9D80-4E0D-43FF-B39A-BB9701708D35}']
    function GetEventType: TLogEventType;
    function GetText: String;
  end;

  IEvExchangeResultLog = interface(IisInterfacedObject)
  ['{3D05C13D-31DC-409A-92D2-01E9EAF83C43}']
    function  AddEvent(const AEventType: TLogEventType; const AText: String): IEvExchangeResultLogEvent;
    function  Count: Integer;
    function  GetAmountOfErrors: integer;
    function  GetAmountOfWarnings: integer;
    function  GetAmountOfinformation: integer;
    procedure Clear;
    function  GetAsStringList: IisStringList;
    function  Text: String;
    procedure LoadFromStringList(const AErrorsList: IisStringList);
    function  GetItem(Index: Integer): IEvExchangeResultLogEvent;
    property  Items[Index: Integer]: IEvExchangeResultLogEvent read GetItem; default;
  end;

  IEvExchangeDataReader = interface
  ['{F81184FD-D1CD-43DC-A394-00FEB06D6687}']
    function  GetSupportedFormatNames: IisStringListRO;
    function  GetSupportedFormatFileExt: IisStringListRO;
    function  GetDescriptionOfOptions: IisStringListRO;
    function  GetReaderName: String;
    function  GetDataReaderOptions: IisStringList;

    procedure SetDataReaderOptions(const AOptions: IisStringList);
    procedure SetDefaultDataReaderOptions;

    function  ReadExampleFromFile(const AFileName: String; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadExampleFromStream(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadDataFromFile(const AFileName: String; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
    function  ReadDataFromStream(const AStream: IevDualStream; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
  end;

  IEvExchange = interface
  ['{51C90339-2B17-46D9-BBBA-8C1FEE44F306}']
    function   GetEvExchangePackagesList: IisStringListRO;
    function   GetEvExchangePackage(const APackageName : String): IEvExchangePackage;
    function   GetDataReadersList: IisStringListRO;
    function   GetDataReader(const ADataReaderName: String): IEvExchangeDataReader;
    procedure  UpgradeMapFileToLastPackageVer(const AMapFile: IEvExchangeMapFile);
    procedure  PreparePackageGroupRecords(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);
    procedure  UpgradeDataTypesInMapFile(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);

    function   Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile; const AOptions: IisStringListRO;
      const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues; // input dataset will be changed after import!!!
    function   CheckMapFile(const AMapFile: IEvExchangeMapFile; const AOptions: IisStringListRO): IisListOfValues;
  end;

  IEvExchangeEngine = interface
  ['{B7990F51-E1C4-42D3-BE4A-F10AA404D8D6}']
    function  Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile;  const APackage: IevExchangePackage;
      const AOptions: IisStringListRO; const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;  // input dataset can be changed after import!!!
    function  CheckMapFile(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage;
     const AOptions: IisStringListRO): IisListOfValues;
  end;

  TEvoXTaskStatus = (tskEvoXInactive, tskEvoXActive, tskEvoXActiveSuspended, tskEvoXIncorrect);
  TEvoXTaskErrors = (CannotDeleteStorageFile, CannotSaveTaskStatusToFile);

  IEvExchangeScheduledTask = interface
  ['{F63601C0-1D75-4C10-9778-2FB03F88EDFE}']
    function  GetTaskStatus: TEvoXTaskStatus;
    function  HasJobsInTaskQueue: boolean;
    procedure ActivateTask;
    procedure DeactivateTask;
    function  GetTaskParams: IisListOfValues;
    procedure SetTaskParams(const ATaskParams: IisListOfValues);
    function  GetTaskGUID: TisGUID;
    function  GetLogFileName: String;
  end;

  IEvExchangeScheduler = interface
  ['{DC0A84A8-7C09-4D1B-8EC7-053202218BEF}']
    function  GetTaskGUIDsList: IisStringList;
    function  GetTask(const ATaskGUID: TisGUID): IEvExchangeScheduledTask;
    function  AddNewTask: IEvExchangeScheduledTask;
    procedure DeleteTask(const ATaskGUID: TisGUID);
    function  GetListOfSpoiledTasks: IisListOfValues;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
    procedure AddTaskToSpoiledList(const ATaskID: TisGUID; const AErrorCode: TEvoXTaskErrors; const ADescription: String);
    procedure CheckForTheSameFolder(const APath: String);
    function  GetTasksRootStorageFolder: String;
    procedure LoadTasksWithoutActivation;
  end;

  ///// End of EvoX ///////////////////////////////

  // TOA requests
  IEvTOARequest = interface
  ['{19D2A632-1447-4905-A5B3-810C2000A812}']
    procedure SetAccrualDate(const Value: TDateTime);
    procedure SetHours(const Value: integer);
    procedure SetMinutes(const Value: integer);
    procedure SetNotes(const Value: String);
    procedure SetEETimeOffAccrualOperNbr(const Value: integer);
    procedure SetStatus(const Value: String);
    procedure SetCL_E_DS_NBR(const Value: integer);

    function  GetEETimeOffAccrualOperNbr: integer;
    function  GetEETimeOffAccrualNbr: integer;
    function  GetAccrualDate: TDateTime;
    function  GetHours: integer;
    function  GetMinutes: integer;
    function  GetStatus: String;
    function  GetNotes: String;
    function  GetEENbr: integer;
    function  GetHoursAndMinAsDouble: Double;
    function  GetCL_E_DS_NBR: integer;

    property EENbr: integer read GetEENbr;
    property EETimeOffAccrualOperNbr: integer read GetEETimeOffAccrualOperNbr write SetEETimeOffAccrualOperNbr;
    property EETimeOffAccrualNbr: integer read GetEETimeOffAccrualNbr;
    property AccrualDate: TDateTime read GetAccrualDate write SetAccrualDate;
    property Hours: integer read GetHours write SetHours;
    property HoursAndMinAsDouble: Double read GetHoursAndMinAsDouble;
    property Minutes: integer read GetMinutes write SetMinutes;
    property Status: String read GetStatus write SetStatus;
    property Notes: String read GetNotes write SetNotes;
    property CL_E_DS_NBR: integer read GetCL_E_DS_NBR write SetCL_E_DS_NBR;
  end;

  IEvTOABusinessRequest = interface
  ['{CBC35FE4-EE6E-4F5E-A8F7-4FED15481103}']
    procedure SetBalance(const Value: Double);
    procedure SetCustomEENumber(const Value: String);
    procedure SetDate(const Value: TDateTime);
    procedure SetDescription(const Value: String);
    procedure SetEENbr(const Value: integer);
    procedure SetEETimeOffAccrualNbr(const Value: integer);
    procedure SetFirstName(const Value: String);
    procedure SetLastName(const Value: String);
    procedure SetNotes(const Value: String);
    procedure SetManagerNote(const Value: String);
    procedure SetStatus(const Value: String);

    function  GEENbr: integer;
    function  GetFirstName: String;
    function  GetLastName: String;
    function  GetCustomEENumber: String;
    function  GetEETimeOffAccrualNbr: integer;
    function  GetBalance: Double;
    function  GetDescription: String;
    function  GetStatus: String;
    function  GetDate: TDateTime;
    function  GetNotes: String;
    function  GetManagerNote: String;
    function  GetItems: IisInterfaceList;

    property EENbr: integer read GEENbr write SetEENbr;
    property FirstName: String read GetFirstName write SetFirstName;
    property LastName: String read GetLastName write SetLastName;
    property CustomEENumber: String read GetCustomEENumber write SetCustomEENumber;
    property EETimeOffAccrualNbr: integer read GetEETimeOffAccrualNbr write SetEETimeOffAccrualNbr;
    property Balance: Double read GetBalance write SetBalance;
    property Description: String read GetDescription write SetDescription;
    property Status: String read GetStatus write SetStatus;
    property Date: TDateTime read GetDate write SetDate;
    property Notes: String read GetNotes write SetNotes;
    property Manager_Note: String read GetManagerNote write SetManagerNote;    
    property Items: IisInterfaceList read GetItems;
  end;

  IevAchAccount = interface
  ['{0884DD2E-1946-4A2D-A92B-2076D3C484FE}']
    function  GetAba: String;
    procedure SetAba(const Value: String);
    function  GetNumber: String;
    procedure SetNumber(const Value: String);
    function  GetAccountType: String;
    procedure SetAccountType(const Value: String);
    property  Aba: String read GetAba write SetAba;
    property  Number: String read GetNumber write SetNumber;
    property  AccountType: String read GetAccountType write SetAccountType;
  end;

  IevAchBankData = interface
  ['{676AEDF3-DA1D-4021-8BCE-401B15DBC655}']
    function  GetBureauAccount: IevAchAccount;
    procedure SetBureauAccount(const Value: IevAchAccount);
    function  GetClientAccount: IevAchAccount;
    procedure SetClientAccount(const Value: IevAchAccount);
    function  GetEffectiveDate: TDateTime;
    procedure SetEffectiveDate(const Value: TDateTime);
    property  BureauAccount: IevAchAccount read GetBureauAccount write SetBureauAccount;
    property  ClientAccount: IevAchAccount read GetClientAccount write SetClientAccount;
    property  EffectiveDate: TDateTime read GetEffectiveDate write SetEffectiveDate;
  end;

  IevAchTaxes = interface
  ['{5EC9B941-BC4B-4982-B7E4-8F76EAD7657F}']
    function  GetBankData: IevAchBankData;
    procedure SetBankData(const Value: IevAchBankData);
    function  GetStateAmount: Currency;
    procedure SetStateAmount(const Value: Currency);
    function  GetLocalAmount: Currency;
    procedure SetLocalAmount(const Value: Currency);
    function  GetSuiAmount: Currency;
    procedure SetSuiAmount(const Value: Currency);
    function  GetTotal: Currency;
    procedure SetTotal(const Value: Currency);
    function  GetSuiLiabilities: IisListOfValues;
    procedure SetSuiLiabilities(const Value: IisListOfValues);
    function  GetLocalLiabilities: IisListOfValues;
    procedure SetLocalLiabilities(const Value: IisListOfValues);
    function  GetStateLiabilities: IisListOfValues;
    procedure SetStateLiabilities(const Value: IisListOfValues);
    function  GetImpounds: IisParamsCollection;
    procedure SetImpounds(const Value: IisParamsCollection);
    property  SuiLiabilities: IisListOfValues read GetSuiLiabilities write SetSuiLiabilities;
    property  LocalLiabilities: IisListOfValues read GetLocalLiabilities write SetLocalLiabilities;
    property  StateLiabilities: IisListOfValues read GetStateLiabilities write SetStateLiabilities;
    property  Impounds: IisParamsCollection read GetImpounds write SetImpounds;
    property  BankData: IevAchBankData read GetBankData write SetBankData;
  end;

  IevAchBilling = interface
  ['{1A40596A-7223-4AB9-BC33-41087CCE592A}']
    function  GetBankData: IevAchBankData;
    procedure SetBankData(const Value: IevAchBankData);
    function  GetImpounds: IisParamsCollection;
    procedure SetImpounds(const Value: IisParamsCollection);
    function  GetAmount: Currency;
    procedure SetAmount(const Value: Currency);
    property  Amount: Currency read GetAmount write SetAmount;
    property  Impounds: IisParamsCollection read GetImpounds write SetImpounds;
    property  BankData: IevAchBankData read GetBankData write SetBankData;
  end;

  IevAchTrust = interface
  ['{AFA83581-3E4E-4A49-9B3A-3249FA84114B}']
    function  GetBankData: IevAchBankData;
    procedure SetBankData(const Value: IevAchBankData);
    function  GetAmount: Currency;
    procedure SetAmount(const Value: Currency);
    function  GetTRUST_SERVICE: Boolean;
    procedure SetTRUST_SERVICE(const Value: Boolean);
    function  GetOBC: Boolean;
    procedure SetOBC(const Value: Boolean);
    function  GetTRUST_OBC_BATCH_COMMENTS: String;
    function  GetTRUST_OBC_COMMENTS: String;
    property  TRUST_SERVICE: Boolean read GetTRUST_SERVICE write SetTRUST_SERVICE;
    property  OBC: Boolean read GetOBC write SetOBC;
    property  TRUST_OBC_BATCH_COMMENTS: String read GetTRUST_OBC_BATCH_COMMENTS;
    property  TRUST_OBC_COMMENTS: String read GetTRUST_OBC_COMMENTS;
    property  Amount: Currency read GetAmount write SetAmount;
    property  BankData: IevAchBankData read GetBankData write SetBankData;
  end;

  IevAchDirDep = interface
  ['{B0E80E54-820D-43C9-9437-4CD70D3B08D4}']
    function  GetBankData: IevAchBankData;
    procedure SetBankData(const Value: IevAchBankData);
    function  GetEmployees: IisParamsCollection;
    procedure SetEmployees(const Value: IisParamsCollection);
    function  GetImpounds: IisParamsCollection;
    procedure SetImpounds(const Value: IisParamsCollection);
    function  GetChildSupport: IisParamsCollection;
    procedure SetChildSupport(const Value: IisParamsCollection);
    function  GetAgencies: IisParamsCollection;
    procedure SetAgencies(const Value: IisParamsCollection);
    property  Employees: IisParamsCollection read GetEmployees write SetEmployees;
    property  ChildSupport: IisParamsCollection read GetChildSupport write SetChildSupport;
    property  Agencies: IisParamsCollection read GetAgencies write SetAgencies;
    property  Impounds: IisParamsCollection read GetImpounds write SetImpounds;
    property  BankData: IevAchBankData read GetBankData write SetBankData;
  end;

  IevAchWorkersComp = interface
  ['{D01A56E4-5EB4-4603-8E65-1E10908807DB}']
    function  GetBankData: IevAchBankData;
    procedure SetBankData(const Value: IevAchBankData);
    function  GetAmount: Currency;
    procedure SetAmount(const Value: Currency);
    property  BankData: IevAchBankData read GetBankData write SetBankData;
    property  Amount: Currency read GetAmount write SetAmount;
  end;



  // DB Maintenance

  TevDBParam = record
    Name: String;
    SizeMB: Cardinal;
  end;

  TevBackupDBParam = record
    Name: String;
    Scramble: Boolean;
    SizeMB: Cardinal;
  end;

  TevRestoreDBParam = record
    Name: String;
    FileName: String;
    SizeMB: Cardinal;
  end;

  TevImportDBParam = record
    SourceDB: String;
    DestinationDB: String;
    SourcePassword: String;
    DBSizeMB: Cardinal;
  end;

  TevQueueItemState = (qisQueued, qisExecuting, qisFinished, qisFinishedWithErrors);
  TevQueueItemStates = set of TevQueueItemState;

  IevDBItem = interface(IisInterfacedObject)
  ['{361154F2-FE11-42F9-A4B8-7C98E5A41034}']
    function  GetParams: IisListOfValues;
    function  GetDBName: String;
    function  GetDBSizeMB: Cardinal;
    function  GetDomain: String;
    function  GetADRContext: Boolean;
    function  GetDBLocation: String;
    function  GetDBHost: String;
    function  GetState: TevQueueItemState;
    function  GetStartedAt: TDateTime;
    function  GetFinishedAt: TDateTime;
    function  GetStatus: String;
    function  GetDetails: String;
    function  GetProgress: Integer;
  end;

  TevQCEOperation = (qceAdd, qceUpdate, qceDelete, qceMoveToTop, qceMoveToBottom);

  TOnQueueChangedEvent = procedure (const AItem: IevDBItem; const AOperation: TevQCEOperation) of object;

  TevQueueStatus = record
    Active: Boolean;
    Executing: Integer;
    Queued: Integer;
    Finished: Integer;
    Errored: Integer;
    Skipped: Integer;
    TotalProgress: Integer;
    StartTime: TDateTime;
    ElapsedTimeSec: Cardinal;
    EstimatedTimeLeftSec: Cardinal;
  end;


  PevAsyncCallBack = procedure (const AError: String) of object;

  IevDBMaintenance = interface
  ['{E2859B24-103D-4C8D-B925-C521547C9DD1}']
    procedure InitThreadsPerServer;
    function  ThreadsPerServer: IisListOfValues;
    procedure UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);
    function  GetOnQueueChanged: TOnQueueChangedEvent;
    procedure SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
    procedure ClearQueue;
    function  GetStatus: TevQueueStatus;
    function  GetElapsedSeconds: Cardinal;
    function  GetExecutingItems: IisParamsCollection;
    function  GetErroredItems: IisParamsCollection;     
    procedure RunBackup(const ADatabases: array of TevBackupDBParam; const AFolder: String; const ACompress: Boolean);
    procedure RunRestore(const ADatabases: array of TevRestoreDBParam);
    procedure RunImport(const ADatabases: array of TevImportDBParam);
    procedure RunPatching(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean);
    procedure RunPatchingAsync(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean; const ACallBack: PevAsyncCallBack);
    procedure RunSweep(const ADatabases: array of TevDBParam);
    procedure RunCustomScript(const ADatabases: array of TevBackupDBParam; const AScript: String; const ASweep: Boolean);
    procedure RunTidyUpDBFolders;
    property  OnQueueChanged: TOnQueueChangedEvent read GetOnQueueChanged write SetOnQueueChanged;
  end;

  IevDashboardDataProvider = interface
  ['{7808B528-7E62-4720-AA62-CAA1E9453471}']
    function  GetCompanyReports(const ACl_Nbr, ACo_Nbr: Integer; RemoteUser: boolean): IisListOfValues;
    function  GetCompanyMessages: IisListOfValues;
  end;

  IevDashboardDataProviderExt = interface(IevDashboardDataProvider)
  ['{E128F91C-B9E2-449D-9996-CDCF8F2690E5}']
    procedure StoreReportData(const AData: String);
  end;
  
implementation

end.
