unit rwPreviewContainerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isBasicutils{WM_USER_NOTIFY_SETFOCUS};


type
  TrwPreviewContainer = class(TFrame)
    procedure FrameResize(Sender: TObject);
  private
    FPreview: IInterface;
    procedure SetContentSizeByContainer;
    procedure WMMouseWheel(var Message: TWMMouseWheel); message WM_MOUSEWHEEL;
    procedure WMUserNotifySetFocus(var Message: TMessage); message WM_USER_NOTIFY_SETFOCUS;
  public
    procedure HoldPreview(const APreview: IInterface);
    procedure ClosePreview;
  end;

implementation

{$R *.dfm}

uses EvReportWriterProxy;

{ TrwPreviewContainer }

procedure TrwPreviewContainer.SetContentSizeByContainer;
begin
  if Assigned(FPreview) then
    IevRWPreview(FPreview).BoundsRect := ClientRect;
end;

procedure TrwPreviewContainer.FrameResize(Sender: TObject);
begin
  SetContentSizeByContainer;
end;

procedure TrwPreviewContainer.HoldPreview(const APreview: IInterface);
begin
  FPreview := APreview as IevRWPreview;
  SetContentSizeByContainer;
end;

procedure TrwPreviewContainer.ClosePreview;
begin
  HoldPreview(nil);
end;

{$WARN SYMBOL_PLATFORM OFF}
//RE 38359
procedure TrwPreviewContainer.WMMouseWheel(var Message: TWMMouseWheel);
begin
  if HandleAllocated then
    if ( InSendMessageEx(nil) and (ISMEX_REPLIED or ISMEX_SEND) ) = ISMEX_SEND then
    //if sender is blocked
    begin
      Message.Result := 0; //don't need any further processing
      Win32Check( PostMessage(handle, TMessage(Message).Msg, TMessage(Message).WParam, TMessage(Message).LParam ) );
    end
end;
{$WARN SYMBOL_PLATFORM ON}

procedure TrwPreviewContainer.WMUserNotifySetFocus(var Message: TMessage);
var
  fm: TCustomForm;
begin
{  ControlState := ControlState + [csFocusing];
  try
  finally
    ControlState := ControlState - [csFocusing];
  end;}
  fm := GetParentForm(Self);
  if assigned(fm) and (fm.ActiveControl <> fm) then
    fm.SetFocusedControl(fm);
end;

end.
