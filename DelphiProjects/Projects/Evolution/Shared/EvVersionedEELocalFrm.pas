unit EvVersionedEELocalFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, IsUtils, EvFieldValueSelectFrm,
  isVCLBugFix, SFieldCodeValues, EvMainboard, EvTypes;

type
  TEvVersionedEELocal = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    rgLocalEnabled: TevDBRadioGroup;
    rdDeduct: TevDBRadioGroup;
    pnlBottom: TevPanel;
    procedure FormShow(Sender: TObject);
    procedure dsFieldDataDataChange(Sender: TObject; Field: TField);
  private
    FCo_Nbr: Integer;
    FEe_Nbr: Integer;
    FCo_Local_Tax_Nbr: Integer;
    FBroadcastParams: IisListOfValues;
    procedure AddNewLocalTax(const ANbr: Integer);
    function  SelectNewLocal: Integer;
  protected
    procedure DoOnSave; override;
    procedure DoAfterSave; override;
    procedure DisplayOrOverrideNavInsertMessage; override;
  end;

implementation

{$R *.dfm}

{ TEvVersionedEELocal }

procedure TEvVersionedEELocal.DisplayOrOverrideNavInsertMessage;
begin
  ShowMessage('Please Note: Changes should be made as of the next check date.');
end;

procedure TEvVersionedEELocal.FormShow(Sender: TObject);
var
  Q: IevQuery;
begin
  inherited;
  DefaultToQuarters := False;

  Q := TevQuery.Create('SELECT t2.co_nbr, t1.ee_nbr, t1.co_local_tax_nbr FROM ee_locals t1, co_local_tax t2 WHERE {AsOfNow<t1>} AND {AsOfNow<t2>} AND ' +
                       't1.co_local_tax_nbr = t2.co_local_tax_nbr AND t1.ee_locals_nbr = :nbr');
  Q.Params.AddValue('nbr', Nbr);
  FCo_Nbr := Q.Result.Fields[0].AsInteger;
  FEE_Nbr := Q.Result.Fields[1].AsInteger;
  FCo_Local_Tax_Nbr := Q.Result.Fields[2].AsInteger;

  Fields[0].AddValue('Title', rgLocalEnabled.Caption);
  Fields[0].AddValue('Width', 15);
  rgLocalEnabled.Caption := Iff(Fields[0].Value['Required'], '~', '') + rgLocalEnabled.Caption;
  rgLocalEnabled.DataField := Fields.ParamName(0);

  Fields[1].AddValue('Title', rdDeduct.Caption);
  Fields[1].AddValue('Width', 15);
  rdDeduct.Caption := Iff(Fields[1].Value['Required'], '~', '') + rdDeduct.Caption;
  rdDeduct.DataField := Fields.ParamName(1);
end;

procedure TEvVersionedEELocal.DoOnSave;
var
  Res: Word;
  fl: Boolean;
  NewLocalNbr: Integer;
begin
  FBroadcastParams := nil;
  NewLocalNbr := 0;

  if (Data.FieldByName('begin_date').AsDateTime > BeginOfTime) and (Data.FieldByName('end_date').AsDateTime = DayBeforeEndOfTime) and
     (Data.FieldByName(Fields.ParamName(0)).AsString = GROUP_BOX_NO) and
     ((Data.State = dsEdit) and not VarSameValue(Data.FieldByName(Fields.ParamName(0)).OldValue, Data.FieldByName(Fields.ParamName(0)).NewValue) or (Data.State = dsInsert))
  then
    fl := True
  else
    fl := False;

  if fl then
  begin
    Res := EvMessage('You are going to disable a Local Tax for this employee. Do you want to add another Local Tax?',
                      mtConfirmation, mbYesNoCancel, mbCancel);
    if Res = mrCancel then
      AbortEx;

    if Res = mrYes then
    begin
      NewLocalNbr := SelectNewLocal;
      if NewLocalNbr = 0 then
        AbortEx;
    end;
  end;

  if NewLocalNbr > 0 then
    AddNewLocalTax(NewLocalNbr);

  inherited;
end;

procedure TEvVersionedEELocal.dsFieldDataDataChange(Sender: TObject; Field: TField);
begin
  inherited;

  if not Assigned(Field) or AnsiSameText(Field.FieldName, Fields.ParamName(0)) then
  begin
    rdDeduct.Enabled := Data.FieldByName(Fields.ParamName(0)).AsString <> GROUP_BOX_NO;
    if (Data.State in [dsEdit, dsInsert]) and not rdDeduct.Enabled then
      Data.FieldByName(Fields.ParamName(1)).AsString := DEDUCT_NEVER
    else if Data.State = dsInsert then
      Data.FieldByName(Fields.ParamName(1)).Clear
    else if Data.State = dsEdit then
      Data.FieldByName(Fields.ParamName(1)).Value := Data.FieldByName(Fields.ParamName(1)).OldValue;
  end;
end;

procedure TEvVersionedEELocal.AddNewLocalTax(const ANbr: Integer);
var
  T: IevTable;
  Flds: TevFieldValues;
  Ee_Locals_Nbr, Sy_Locals_Nbr: Integer;
  CountyName: String;
  LV: IisListOfValues;
  Q: IevQuery;
  ChangeType: TevTableChangeType;
begin
  Flds := FieldCodeValues.FindFields('EE_LOCALS');

  Q := TevQuery.Create('SELECT t.ee_locals_nbr FROM ee_locals t WHERE {AsOfNow<t>} AND t.co_local_tax_nbr = :nbr and t.ee_nbr = :ee_nbr');
  Q.Params.AddValue('nbr', ANbr);
  Q.Params.AddValue('ee_nbr', FEE_Nbr);
  Ee_Locals_Nbr := Q.Result.Fields[0].AsInteger;

  if Ee_Locals_Nbr = 0 then
  begin
    Q := TevQuery.Create('SELECT t.sy_locals_nbr FROM co_local_tax t WHERE {AsOfNow<t>} AND t.co_local_tax_nbr = :nbr');
    Q.Params.AddValue('nbr', ANbr);
    Sy_Locals_Nbr := Q.Result.Fields[0].AsInteger;

    Q := TevQuery.Create('SELECT t2.county_name FROM sy_locals t1, sy_county t2 WHERE {AsOfNow<t1>} AND {AsOfNow<t2>} AND '+
                         't1.sy_county_nbr = t2.sy_county_nbr AND t1.sy_locals_nbr = :nbr');
    Q.Params.AddValue('nbr', Sy_Locals_Nbr);
    CountyName := Q.Result.Fields[0].AsString;

    T := TevTable.Create('EE_LOCALS', '', '1 = 0');
    T.Open;
    T.Append;
    Ee_Locals_Nbr := ctx_DBAccess.GetGeneratorValue('G_EE_LOCALS', dbtClient, 1); // Exception of rules. We need a real NBR here.
    T.FieldByName('EE_LOCALS_NBR').AsInteger := Ee_Locals_Nbr;
    T.FieldByName('EE_NBR').AsInteger := FEE_Nbr;
    T.FieldByName('CO_LOCAL_TAX_NBR').AsInteger := ANbr;
    T.FieldByName('EE_COUNTY').AsString := CountyName;
    T.FieldByName('LOCAL_ENABLED').AsString := GROUP_BOX_NO;
    T.FieldByName('DEDUCT').AsString := DEDUCT_NEVER;
    T.FieldByName('EXEMPT_EXCLUDE').AsString := Flds.GetDefaultValue('EXEMPT_EXCLUDE');
    T.FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := Flds.GetDefaultValue('OVERRIDE_LOCAL_TAX_TYPE');
    T.FieldByName('INCLUDE_IN_PRETAX').AsString := Flds.GetDefaultValue('INCLUDE_IN_PRETAX');
    T.FieldByName('WORK_ADDRESS_OVR').AsString := Flds.GetDefaultValue('WORK_ADDRESS_OVR');
    T.Post;
    T.SaveChanges;

    ChangeType := tcInsert;
  end
  else
    ChangeType := tcUpdate;

  LV := TisListOfValues.Create;
  LV.AddValue('LOCAL_ENABLED', GROUP_BOX_YES);
  LV.AddValue('DEDUCT', Flds.GetDefaultValue('DEDUCT'));
  ctx_DBAccess.UpdateFieldVersion('EE_LOCALS', Ee_Locals_Nbr, EmptyDate, EmptyDate, Data['begin_date'], DayBeforeEndOfTime, LV);

  FBroadcastParams := TisListOfValues.Create;
  FBroadcastParams.AddValue('Table', 'EE_LOCALS');
  FBroadcastParams.AddValue('Nbr', Ee_Locals_Nbr);
  FBroadcastParams.AddValue('ChangeType', ChangeType);
end;

function TEvVersionedEELocal.SelectNewLocal: Integer;
var
  Filter: IisStringList;
  Q: IevQuery;
begin
  Q := TevQuery.Create('SELECT t.co_local_tax_nbr FROM ee_locals t WHERE {AsOfNow<t>} AND t.local_enabled = ''Y'' AND t.ee_nbr = :ee_nbr');
  Q.Params.AddValue('ee_nbr', FEE_Nbr);
  Filter := Q.Result.GetColumnValues('co_local_tax_nbr');
  if Filter.IndexOf(IntToStr(FCo_Local_Tax_Nbr)) = -1 then
    Filter.Add(IntToStr(FCo_Local_Tax_Nbr));

  Q := TevQuery.Create('SELECT t.co_local_tax_nbr nbr FROM co_local_tax t WHERE {AsOfNow<t>} AND t.co_nbr = :co_nbr AND t.local_active = ''Y'' AND ' +
                       't.co_local_tax_nbr NOT IN (' + Filter.CommaText + ')');
  Q.Params.AddValue('co_nbr', FCo_Nbr);
  Filter := Q.Result.GetColumnValues('nbr');

  Result := 0;
  TEvFieldValueSelect.SelectLookupValue('Local Tax', 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', Nbr, Result, SysDate, Filter);
end;

procedure TEvVersionedEELocal.DoAfterSave;
begin
  inherited;
  if Assigned(FBroadcastParams) then
  begin
    mb_Messenger.Broadcast(MSG_DB_TABLE_CHANGE, FBroadcastParams);
    FBroadcastParams := nil;
  end;
end;

end.

