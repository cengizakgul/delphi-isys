inherited EDIT_BROWSE_MASTER_DETAIL_BASE: TEDIT_BROWSE_MASTER_DETAIL_BASE
  Width = 601
  Height = 519
  inherited PC: TevPageControl
    Width = 601
    Height = 519
    inherited tshtBrowse: TTabSheet
      object evSplitter1: TevSplitter [0]
        Left = 305
        Top = 0
        Height = 491
      end
      inherited gBrowse: TevDBGrid
        Left = 308
        Width = 285
        Height = 491
        Align = alClient
        TabOrder = 1
      end
      object gMaster: TevDBGrid
        Left = 0
        Top = 0
        Width = 305
        Height = 491
        DisableThemesInTitle = False
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsMaster
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
      end
    end
  end
end
