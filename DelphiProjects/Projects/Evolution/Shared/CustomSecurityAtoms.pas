unit CustomSecurityAtoms;

interface
                
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvSecElement;

type
  TdmCustomSecAtoms = class(TDataModule)
    eTaxExemptions: TevSecElement;
    aTaxExemptions: TevSecAtom;
    eUnlockCreditHold: TevSecElement;
    aUnlockCreditHold: TevSecAtom;
    aDeletePr: TevSecAtom;
    eDeletePr: TevSecElement;
    eAdjustShortFalls: TevSecElement;
    aAdjustShortFalls: TevSecAtom;
    eAdjustTaxes: TevSecElement;
    aAdjustTaxes: TevSecAtom;
    eVoidPayroll: TevSecElement;
    aVoidPayroll: TevSecAtom;
    aCopyPayroll: TevSecAtom;
    eCopyPayroll: TevSecElement;
    aPayrollRegister: TevSecAtom;
    ePayrollRegister: TevSecElement;
    ePrCreationWizard: TevSecElement;
    aPrCreationWizard: TevSecAtom;
    aEditRwDict: TevSecAtom;
    eEditRwDict: TevSecElement;
    eSeeSystemRwDict: TevSecElement;
    aSeeSystemRwDict: TevSecAtom;
    aUnvoidVoidedCheck: TevSecAtom;
    eUnvoidVoidedCheck: TevSecElement;
    aEditRwLib: TevSecAtom;
    eEditRwLib: TevSecElement;
    aPayLiabWoFunds: TevSecAtom;
    ePayLiabWoFunds: TevSecElement;
    aOfflineClient: TevSecAtom;
    eOfflineClient: TevSecElement;
    aManualOasdiMedicare: TevSecAtom;
    eManualOasdiMedicare: TevSecElement;
    aReturnDelete: TevSecAtom;
    eReturnDelete: TevSecElement;
    aUpdateAsOf: TevSecAtom;
    eUpdateAsOf: TevSecElement;
    aAdminQueue: TevSecAtom;
    eAdminQueue: TevSecElement;
    eaPrintUnprocessedChecks: TevSecElement;
    aPrintUnprocessedChecks: TevSecAtom;
    aReprintProcessedChecks: TevSecAtom;
    eaReprintProcessedChecks: TevSecElement;
    aReprintPayrollReports: TevSecAtom;
    eaReprintPayrollReports: TevSecElement;
    aReprintProcessedMiscChecks: TevSecAtom;
    eaReprintProcessedMiscChecks: TevSecElement;
    aCreateBackdatedPayrolls: TevSecAtom;
    eaCreateBackdatedPayrolls: TevSecElement;
    aRollbackTaxPayments: TevSecAtom;
    eRollbackTaxPayments: TevSecElement;
    aChangeQuarterLockDate: TevSecAtom;
    eaChangeQuarterLockDate: TevSecElement;
    aUndeleteRecord: TevSecAtom;
    eUndeleteRecord: TevSecElement;
    aVMR: TevSecAtom;
    eVMR: TevSecElement;
    aAbilitySendPayrollDirectly: TevSecAtom;
    eAbilitySendPayrollDirectly: TevSecElement;
    aMarkPayrollCompleted: TevSecAtom;
    eMarkPayrollCompleted: TevSecElement;
    eEeHeDocuments: TevSecElement;
    aEeHeDocuments: TevSecAtom;
    aDeleteClient: TevSecAtom;
    eDeleteClient: TevSecElement;
    aCoBankAccounts: TevSecAtom;
    eCoBankAccounts: TevSecElement;
    aSubmitPayroll: TevSecAtom;
    eSubmitPayroll: TevSecElement;
    aVoidPrevQtrCheck: TevSecAtom;
    eVoidPrevQtrCheck: TevSecElement;
    aVoidCheck: TevSecAtom;
    eVoidCheck: TevSecElement;
    aTimeClockImport: TevSecAtom;
    eTimeClockImport: TevSecElement;
    aCreateManualACH: TevSecAtom;
    aAllowEvoXImport: TevSecAtom;
    aCreateEvoXMap: TevSecAtom;
    aAllowEvoXExport: TevSecAtom;
    aChangeSecuredFlagInReports: TevSecAtom;
    aAccessProcessedreportsInWebClient: TevSecAtom;
    eBlockChangingOfSSN: TevSecElement;
    aBlockChangingOfSSN: TevSecAtom;
    aCLReadOnlyMode: TevSecAtom;
    eCLReadOnlyMode: TevSecElement;
    aApproveBenefitsEnrollment: TevSecAtom;
    aDefineBenefitsEnrollment: TevSecAtom;
    aGloballyUpdatePayrates: TevSecAtom;
    aAccessDeleteVMRMailBoxes: TevSecAtom;
    aSwipeClockLogin: TevSecAtom;
    aMRCSignOn: TevSecAtom;
    aDisplayUsername: TevSecAtom;
    eDisplayUsername: TevSecElement;
    eApplyEEQuickEntry: TevSecElement;
    aApplyEEQuickEntry: TevSecAtom;
    aBlockVoidingChecks: TevSecAtom;
    eBlockVoidingChecks: TevSecElement;
    aSetEffectivePeriodForLocals: TevSecAtom;
    eSetEffectivePeriodForLocals: TevSecElement;
    aDashBoard: TevSecAtom;
    eDashBoard: TevSecElement;
    ePreprocessBatch: TevSecElement;
    aPreprocessBatch: TevSecAtom;
    aUpdateACAAsOf: TevSecAtom;
    eUpdateACAAsOf: TevSecElement;
  end;


implementation

{$R *.DFM}

end.
