// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_ddTable;

interface

uses
  SysUtils, Windows, Classes;

type
  TDM_ddTable = class(TDataModule)
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); override;
  end;

implementation

{$R *.dfm}

{ TDM_ddTable }

constructor TDM_ddTable.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
  inherited;
  if Assigned(RemoveDataModule) then
    RemoveDataModule(Self);
end;

end.
