object evAuditVerFieldView: TevAuditVerFieldView
  Left = 0
  Top = 0
  Width = 961
  Height = 582
  AutoScroll = False
  Constraints.MinHeight = 350
  Constraints.MinWidth = 650
  TabOrder = 0
  OnResize = FrameResize
  object sbAuditForm: TScrollBox
    Left = 0
    Top = 0
    Width = 961
    Height = 582
    Align = alClient
    Color = clWindow
    ParentColor = False
    TabOrder = 0
    object fpAuditTop: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 957
      Height = 153
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'fpAuditTop'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Sequence of Changes'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Top = 8
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object pnlLowered: TevPanel
        Left = 20
        Top = 44
        Width = 909
        Height = 81
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvLowered
        TabOrder = 0
        object grTimeLine: TDBCtrlGrid
          Left = 2
          Top = 2
          Width = 905
          Height = 77
          Align = alClient
          AllowDelete = False
          AllowInsert = False
          ColCount = 3
          Color = clWhite
          DataSource = dsTimeLine
          Orientation = goHorizontal
          PanelBorder = gbNone
          PanelHeight = 60
          PanelWidth = 301
          ParentColor = False
          TabOrder = 0
          RowCount = 1
          SelectedColor = clHighlight
          OnPaintPanel = grTimeLinePaintPanel
          object dbtDateTime: TevDBText
            Left = 10
            Top = 6
            Width = 200
            Height = 21
            DataField = 'change_time'
            DataSource = dsTimeLine
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object dbtUserInfo: TevDBText
            Left = 10
            Top = 32
            Width = 200
            Height = 17
            DataField = 'user_name'
            DataSource = dsTimeLine
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
        end
      end
    end
    object pnlBottom: TevPanel
      Left = 0
      Top = 153
      Width = 957
      Height = 425
      Align = alClient
      BevelOuter = bvNone
      Color = clWindow
      TabOrder = 1
      OnResize = pnlBottomResize
      object evSplitter1: TevSplitter
        Left = 300
        Top = 0
        Height = 425
      end
      inline frmLeftViewer: TevVerFieldAsOf
        Left = 0
        Top = 0
        Width = 300
        Height = 425
        Align = alLeft
        AutoScroll = False
        TabOrder = 0
        inherited fpChange: TisUIFashionPanel
          Width = 300
          Height = 425
          Title = 'Before Change'
          Margins.Left = 8
          Margins.Right = 4
          Margins.Bottom = 8
          inherited grFieldData: TevDBGrid
            Left = 20
            Width = 256
            Height = 361
            OnRowChanged = frmLeftViewergrFieldDataRowChanged
          end
        end
      end
      inline frmRightViewer: TevVerFieldAsOf
        Left = 303
        Top = 0
        Width = 654
        Height = 425
        Align = alClient
        AutoScroll = False
        TabOrder = 1
        inherited fpChange: TisUIFashionPanel
          Width = 654
          Height = 425
          Title = 'After Change'
          Margins.Left = 4
          Margins.Right = 8
          Margins.Bottom = 8
          inherited grFieldData: TevDBGrid
            Left = 16
            Width = 610
            Height = 361
            OnRowChanged = frmRightViewergrFieldDataRowChanged
          end
        end
      end
    end
  end
  object dsTimeLine: TevDataSource
    OnDataChange = dsTimeLineDataChange
    Left = 240
  end
end
