inherited evVersionedCoLegalAddress: TevVersionedCoLegalAddress
  Left = 794
  Top = 486
  ClientHeight = 556
  ClientWidth = 989
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 989
    Height = 338
    inherited grFieldData: TevDBGrid
      Width = 941
      Height = 211
    end
    inherited pnlEdit: TevPanel
      Top = 255
      Width = 941
      inherited pnlButtons: TevPanel
        Left = 632
      end
    end
  end
  inherited pnlBottom: TevPanel
    Top = 338
    Width = 989
    Height = 218
    inherited isUIFashionPanel1: TisUIFashionPanel
      Height = 218
      Title = 'Company Legal Address'
      inherited lAddress1: TevLabel
        Left = 21
        Top = 74
      end
      inherited lAddress2: TevLabel
        Left = 21
        Top = 113
      end
      inherited lCity: TevLabel
        Top = 152
      end
      inherited lState: TevLabel
        Top = 152
      end
      inherited lZipCode: TevLabel
        Left = 215
        Top = 152
      end
      object lName: TevLabel [5]
        Left = 20
        Top = 35
        Width = 28
        Height = 13
        Caption = 'Name'
        FocusControl = edName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      inherited edAddress1: TevDBEdit
        Left = 21
        Top = 89
        TabOrder = 1
      end
      inherited edAddress2: TevDBEdit
        Left = 21
        Top = 128
        TabOrder = 2
      end
      inherited edCity: TevDBEdit
        Top = 167
        TabOrder = 3
      end
      inherited edState: TevDBEdit
        Top = 167
        TabOrder = 4
      end
      inherited edZipCode: TevDBEdit
        Left = 215
        Top = 167
        TabOrder = 5
      end
      object edName: TevDBEdit
        Left = 20
        Top = 50
        Width = 270
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
