unit EvVersionedEnableHRFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, IsUtils, EvFieldValueSelectFrm,
  isVCLBugFix, SFieldCodeValues, EvMainboard, EvTypes, wwdblook,
  isUIwwDBLookupCombo, isUIwwDBComboBox, EvExceptions;

type
  TEvVersionedEnableHR = class(TevVersionedFieldBase)
    lblHR: TevLabel;
    evcbHR: TevDBComboBox;
  private
  protected
    procedure DoOnSave; override;
  end;

implementation

{$R *.dfm}

{ TEvVersionedEnableHR }

procedure TEvVersionedEnableHR.DoOnSave;
begin

  if (Data.FieldByName('ENABLE_HR').AsString = GROUP_BOX_ADVANCED_HR) and
         (Length(Trim(DM_CLIENT.CO.FieldByName('HCM_URL').AsString)) = 0) then
      raise EUpdateError.CreateHelp('Please populate the HCM URL field prior to changing this field to Advanced HR.', IDH_ConsistencyViolation);


  inherited;
end;

end.

