inherited EDIT_CL_BASE: TEDIT_CL_BASE
  inherited Panel1: TevPanel
    Height = 54
    Color = 4276545
    inherited pnlFavoriteReport: TevPanel
      Height = 54
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      Visible = False
      inherited spbFavoriteReports: TevSpeedButton
        Left = 14
        Top = 16
      end
      inherited btnSwipeClock: TevSpeedButton
        Left = 45
        Top = 16
      end
      inherited BtnMRCLogIn: TevSpeedButton
        Left = 76
        Top = 16
      end
      object evLabel47: TevLabel
        Left = 2
        Top = 2
        Width = 111
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object pnlTopRight: TevPanel
      Left = 0
      Top = 0
      Width = 320
      Height = 54
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object Label5: TevLabel
        Left = 8
        Top = 8
        Width = 38
        Height = 13
        Caption = 'CLIENT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBText2: TevDBText
        Left = 57
        Top = 7
        Width = 47
        Height = 15
        AutoSize = True
        DataField = 'CUSTOM_CLIENT_NUMBER'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText1: TevDBText
        Left = 126
        Top = 7
        Width = 47
        Height = 15
        AutoSize = True
        DataField = 'NAME'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  inherited PageControl1: TevPageControl
    Top = 54
    Height = 212
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Height = 183
        inherited pnlBorder: TevPanel
          Height = 179
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Height = 179
          inherited Panel3: TevPanel
            inherited BitBtn1: TevBitBtn
              Caption = 'Open Client'
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Height = 72
            inherited Splitter1: TevSplitter
              Left = 349
              Height = 68
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 349
              Height = 68
              Title = 'Client'
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 299
                Height = 307
                IniAttributes.SectionName = 'TEDIT_CL_BASE\wwdbgridSelectClient'
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 352
              Width = 19
              Height = 68
            end
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_CL.CL
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CL.TMP_CL
    Top = 26
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 350
    Top = 50
  end
end
