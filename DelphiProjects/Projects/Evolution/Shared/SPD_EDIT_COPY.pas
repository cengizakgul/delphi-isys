// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_COPY;

interface

uses
  Forms, Db,  Wwdatsrc, StdCtrls, Grids, Wwdbigrd,
  Wwdbgrid, Classes, Controls, ExtCtrls,  EvConsts, SDataStructure,
  Dialogs, EvUtils, SysUtils, Buttons, Variants, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, SPayrollReport, SDDClasses, SDataDictclient,
  ComCtrls, SDataDicttemp, DBClient, DBTables, Windows, Printers,Graphics, EvCommonInterfaces, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;


type
  TEDIT_COPY = class(TForm)
    Panel1: TevPanel;
    wwdsCopy: TevDataSource;
    wwcsCopy: TevClientDataSet;
    wwcsCoReports: TevClientDataSet;
    wwdsCoReports: TevDataSource;
    wwcsEDsToCopy: TevClientDataSet;
    evPageControl1: TevPageControl;
    tshCopy: TTabSheet;
    wwgdCopy: TevDBGrid;
    wwcsCopySecond: TevClientDataSet;
    wwdsCopySecond: TevDataSource;
    tshExceptions: TTabSheet;
    bttnCancel: TevBitBtn;
    bttnCopySelected: TevBitBtn;
    bttnCopyAll: TevBitBtn;
    wwdsExceptions: TevDataSource;
    wwgdExceptions: TevDBGrid;
    btnDone: TevBitBtn;
    btnPrint: TevBitBtn;
    DM_TEMPORARY: TDM_TEMPORARY;
    wwcsExceptions: TevClientDataSet;
    PrintDialog1: TPrintDialog;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bttnCopySelectedClick(Sender: TObject);
    procedure wwcsCopyFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure bttnCopyAllClick(Sender: TObject);
    procedure wwgdCopy1FilterChange(Sender: TObject; DataSet: TDataSet;
      NewFilter: String);
    procedure tshExceptionsShow(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    ChangeCount: Integer;
    FilterStr: String;
    procedure CopyCOEDCodes;
    procedure CopyOverToCOEDCodes;
    procedure CopyOverToCOEDCodesAll;
    procedure CopyCLEDGroupCodes;
    procedure CopyOverToCLEDGroupCodes;
    procedure CopyOverToCLEDGroupCodesAll;
    procedure CopyCLEDTOCOEDCodes;
    procedure AttatchEEEDCodes;
    procedure DoAttachEERates;
    procedure DoAttachEEs;
    procedure DoAttachEEEDCodes(const ProcessType:boolean);
    function PrepareNewRow(CS: TevClientDataSet): string;
    procedure SetupForCopy;
    procedure CopyReports;
    procedure CopyJobs;    
    procedure UpdatePwAPSetup;
    procedure UpdatePwAP;
    procedure GLSetup;
    procedure CopyGL;
    procedure AskToShowExceptions;
    procedure PrintExceptions;
    procedure Setup_UpdateEEEDCodes;
    procedure UpdateEEEDCodes;
  public
    { Public declarations }
    StartDate:TDateTime;
    EndDate:TDateTime;
    CopyTo: string;
    CopyFrom: string;
    CoNbr: string;
    EdNbr: Integer;
    UpdateEDs: Boolean;
    FieldsToCopy: TStringList;
    EdList: TStringList;
    CustomEDFunction: procedure of object;
  end;

implementation

uses EvContext;

{$R *.DFM}

procedure TEDIT_COPY.CopyCOEDCodes;
begin
  ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_E_DS]);

  wwcsCopy.Data := DM_CLIENT.CL_E_DS.Data;

  wwgdCopy.Selected.Text :=
    'CUSTOM_E_D_CODE_NUMBER' + #9 + '20' + #9 + 'E/D Code' + #9 + 'F' + #13 + #10 +
    'DESCRIPTION' + #9 + '40' + #9 + 'Description' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;

  FieldsToCopy.Add('CL_E_DS_NBR');
  FieldsToCopy.Add('DISTRIBUTE');
end;

procedure TEDIT_COPY.FormShow(Sender: TObject);
begin
  tshCopy.TabVisible := False;
  tshExceptions.TabVisible := False;
  tshCopy.Visible := True;

  if CopyTo = 'COEDCodes' then
      CopyCOEDCodes
  else if CopyTo = 'CLEDGroupCodes' then
      CopyCLEDGroupCodes
  else if CopyTo = 'EEScheduledEDS' then
      AttatchEEEDCodes
  else if CopyTo = 'EERates' then
      AttatchEEEDCodes
  else if CopyTo = 'EEs' then
      AttatchEEEDCodes
  else if (CopyTo = 'CoReports') or (CopyTo = 'CO_JOBS') then
      SetupForCopy
  else if (CopyTo = 'PwAP') then
      UpdatePwAPSetup
  else if (CopyTo = 'GeneralLedger') then
    GLSetup
  else if (CopyTo = 'EEEDCodes') then
    Setup_UpdateEEEDCodes;
end;

procedure TEDIT_COPY.FormCreate(Sender: TObject);
begin
  FieldsToCopy := TStringList.Create;
  FieldsToCopy.Clear;
  ChangeCount := 0;
  EdList := TStringList.Create;
end;

procedure TEDIT_COPY.CopyOverToCOEDCodes;
var
  i: integer;
begin
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls; {Disable controls to improve performance}
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);
      DM_COMPANY.CO_E_D_CODES.Insert;
      DM_COMPANY.CO_E_D_CODES.FieldByName('CL_E_DS_NBR').AsInteger := wwdsCopy.DataSet.FieldByName('CL_E_DS_NBR').AsInteger;
      DM_COMPANY.CO_E_D_CODES.FieldByName('DISTRIBUTE').AsString := 'N';

      if Context.License.HR then
          SetUpCO_E_D_CODESDefault(wwdsCopy.DataSet.FieldByName('E_D_CODE_TYPE').AsString,
                                      LowerCase(wwdsCopy.DataSet.FieldByName('DESCRIPTION').AsString));
      DM_COMPANY.CO_E_D_CODES.Post;
    end;
    EnableControls; { Re-enable controls }
  end;
end;

procedure TEDIT_COPY.bttnCopySelectedClick(Sender: TObject);
var msg: String;
begin
  // Additional warning message when updating EE Scheduled EDs from Client level.
  if CopyTo = 'EEEDCodes' then
  begin
    msg := 'This operation will overwrite the Scheduled E/D values for the fields you specified in the companies that you specified that have' + #13#10 +
           'the Client E/D code assigned to it. Please note that ALL employee Scheduled E/D''s with that Client E/D will be affected.' + #13#10 +
           'This includes Scheduled E/D''s that have an Effective End Date and Scheduled E/D''s for employees of any Termination Status. Continue?';
    if (EvMessage(msg, mtConfirmation, [mbNo, mbYes], mbNo) = mrNo) then
    begin
      ModalResult := mrCancel;
      Exit;
    end;
  end;

  // Default warning message
  msg := 'This operation will overwrite existing entries!  Are you sure you want to do this?';

  // Warning message when ctx_DataAccess.Copying GLs
  if CopyTo = 'GeneralLedger' then
    msg := 'This operation will overwrite existing General Ledger Formats for matching records. ' + #13#10 +
           'For un-matching records, this operation will create new General Ledger records ' + #13#10 +
           'only if the Level Value and Data Values exist on the receiving company. Continue?';

  if (EvMessage(msg, mtConfirmation, [mbNo, mbYes], mbNo) = mrYes) then
  begin
    ctx_StartWait;
    try
      if CopyTo = 'COEDCodes' then
        CopyOverToCOEDCodes
      else if CopyTo = 'CLEDGroupCodes' then
        CopyOverToCLEDGroupCodes
      else if CopyTo = 'EEScheduledEDS' then
        DoAttachEEEDCodes(false)
      else if CopyTo = 'EERates' then
        DoAttachEERates
      else if CopyTo = 'EEs' then
        DoAttachEEs
      else if CopyTo = 'CoReports' then
          CopyReports
      else if CopyTo = 'CO_ED_CODES' then
          CopyCLEDTOCOEDCodes
      else if CopyTo = 'PwAP' then
        UpdatePwAP
      else if CopyTo = 'GeneralLedger' then
        CopyGL
      else if CopyTo = 'EEEDCodes' then
        UpdateEEEDCodes
      else if CopyTo = 'CO_JOBS' then
          CopyJobs
    finally
      ctx_EndWait;
    end;
  end
  else
    ModalResult := mrCancel;
end;

procedure TEDIT_COPY.wwcsCopyFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if CopyTo = 'COEDCodes' then
    Accept := (not DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', wwcsCopy.FieldByName('CL_E_DS_NBR').AsInteger, []))
  else
    if CopyTo = 'CLEDGroupCodes' then
    Accept := (not DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_DS_NBR', wwcsCopy.FieldByName('CL_E_DS_NBR').AsInteger, []))
end;

procedure TEDIT_COPY.bttnCopyAllClick(Sender: TObject);
var msg: String;
begin
  // Additional warning message when updating EE Scheduled EDs from Client level.
  if CopyTo = 'EEEDCodes' then
  begin
    msg := 'This operation will overwrite the Scheduled E/D values for the fields you specified in the companies that you specified that have' + #13#10 +
           'the Client E/D code assigned to it. Please note that ALL employee Scheduled E/D''s with that Client E/D will be affected.' + #13#10 +
           'This includes Scheduled E/D''s that have an Effective End Date and Scheduled E/D''s for employees of any Termination Status. Continue?';
    if (EvMessage(msg, mtConfirmation, [mbNo, mbYes], mbNo) = mrNo) then
    begin
      ModalResult := mrCancel;
      Exit;
    end;
  end;

  // Default warning message
  msg := 'This operation will overwrite existing entries!  Are you sure you want to do this?';

  // Warning message when ctx_DataAccess.Copying GLs
  if CopyTo = 'GeneralLedger' then
    msg := 'This operation will overwrite existing General Ledger Formats for matching records.' + #13#10 +
           'For un-matching records, this operation will create new General Ledger records' + #13#10 +
           'only if the Level Value and Data Values exist on the receiving company.  Continue?';

  if (EvMessage(msg, mtConfirmation, [mbNo, mbYes], mbNo) = mrYes) then
  begin
    ctx_StartWait;
    try
      if CopyTo = 'COEDCodes' then
        CopyOverToCOEDCodesAll
      else if CopyTo = 'CLEDGroupCodes' then
        CopyOverToCLEDGroupCodesAll
      else if CopyTo = 'EEScheduledEDS' then
        DoAttachEEEDCodes(true)
      else if CopyTo = 'EERates' then
      begin
        wwgdCopy.SelectAll;
        DoAttachEERates
      end
      else if CopyTo = 'EEs' then
      begin
        wwgdCopy.SelectAll;
        DoAttachEEs
      end
      else if CopyTo = 'CoReports' then
      begin
        if EvMessage('This operation will copy the reports you have selected to all clients and companies. Are you sure you want to do this?',
            mtConfirmation, [mbNo, mbYes], mbNo) = mrYes then
        begin
          wwgdCopy.SelectAll;
          CopyReports;
        end;
      end
      else if CopyTo = 'PwAP' then
      begin
        if FilterStr <> '' then
        begin
          wwgdCopy.SelectAll;
          UpdatePwAP;
        end
        else
          if (EvMessage('This operation will update field for selected reports in all clients and companies. Are you sure you want to do this?', mtConfirmation, [mbNo, mbYes], mbNo) = mrYes) then
          begin
            wwgdCopy.SelectAll;
            UpdatePwAP;
          end
      end
      else if CopyTo = 'GeneralLedger' then
      begin
        wwgdCopy.SelectAll;
        CopyGL;
      end
      else if CopyTo = 'EEEDCodes' then
      begin
        wwgdCopy.SelectAll;
        UpdateEEEDCodes;
      end
      else if CopyTo = 'CO_JOBS' then
      begin
        if EvMessage('This operation will copy the jobs you have selected to all clients and companies. Are you sure you want to do this?',
            mtConfirmation, [mbNo, mbYes], mbNo) = mrYes then
        begin
          wwgdCopy.SelectAll;
          CopyJobs;
        end;
      end;
    finally
      ctx_EndWait;
    end;
  end
  else
    ModalResult := mrCancel;
end;

procedure TEDIT_COPY.CopyOverToCOEDCodesAll;
var
  MyTrans: TTransactionManager;
begin
  MyTrans := TTransactionManager.Create(Self);
  MyTrans.Initialize([DM_COMPANY.CO_E_D_CODES]);
  with wwgdCopy, wwcsCopy do
  begin
    DisableControls; {Disable controls to improve performance}
    First;
    while not Eof do
    begin
      DM_COMPANY.CO_E_D_CODES.Insert;
      DM_COMPANY.CO_E_D_CODES.FieldByName('CL_E_DS_NBR').AsInteger := FieldByName('CL_E_DS_NBR').AsInteger;
      DM_COMPANY.CO_E_D_CODES.FieldByName('DISTRIBUTE').AsString := 'N';

      if Context.License.HR then
        SetUpCO_E_D_CODESDefault(FieldByName('E_D_CODE_TYPE').AsString,LowerCase(FieldByName('DESCRIPTION').AsString));

      DM_COMPANY.CO_E_D_CODES.Post;
      Next;
    end;
    EnableControls; { Re-enable controls }
  end;

  try
    MyTrans.ApplyUpdates;
  finally
    MyTrans.Free;
  end;

end;

procedure TEDIT_COPY.CopyCLEDGroupCodes;
begin

  DM_CLIENT.CL_E_DS.DataRequired('ALL');

  wwcsCopy.Data := DM_CLIENT.CL_E_DS.Data;

  wwgdCopy.Selected.Text :=
    'CUSTOM_E_D_CODE_NUMBER' + #9 + '20' + #9 + 'E/D Code' + #9 + 'F' + #13 + #10 +
    'DESCRIPTION' + #9 + '40' + #9 + 'Description' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;

  FieldsToCopy.Add('CL_E_DS_NBR');
  FieldsToCopy.Add('DISTRIBUTE'); //Travis

end;

procedure TEDIT_COPY.CopyOverToCLEDGroupCodes;
var
  i: integer;
begin
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls; {Disable controls to improve performance}
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);
      DM_CLIENT.CL_E_D_GROUP_CODES.Insert;
      DM_CLIENT.CL_E_D_GROUP_CODES.FieldByName('CL_E_DS_NBR').AsInteger := wwdsCopy.DataSet.FieldByName('CL_E_DS_NBR').AsInteger;
      DM_CLIENT.CL_E_D_GROUP_CODES.Post;
    end;
    EnableControls; { Re-enable controls }
  end;
end;

procedure TEDIT_COPY.CopyOverToCLEDGroupCodesAll;
begin
  with wwgdCopy, wwcsCopy do
  begin
    DisableControls; {Disable controls to improve performance}
    First;
    while not Eof do
    begin
      DM_CLIENT.CL_E_D_GROUP_CODES.Insert;
      DM_CLIENT.CL_E_D_GROUP_CODES.FieldByName('CL_E_DS_NBR').AsInteger := FieldByName('CL_E_DS_NBR').AsInteger;
      DM_CLIENT.CL_E_D_GROUP_CODES.Post;
      Next;
    end;
    EnableControls; { Re-enable controls }
  end;
end;

procedure TEDIT_COPY.AttatchEEEDCodes;
begin
  ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('SelectEmployeesToAttach') do
  begin
    SetParam('CoNbr', StrToInt(CoNbr));
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  wwcsCopy.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

  wwgdCopy.Selected.Text :=
    'CUSTOM_EMPLOYEE_NUMBER' + #9 + '20' + #9 + 'EE Number' + #9 + 'F' + #13 + #10 +
    'FIRST_NAME' + #9 + '20' + #9 + 'First Name' + #9 + 'F' + #13 + #10 +
    'LAST_NAME' + #9 + '20' + #9 + 'Last Name' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;
end;


function TEDIT_COPY.PrepareNewRow(CS: TevClientDataSet): string;
var
  I: Integer;
  MyStrings: TStringList;
  FieldsString: string;
begin
  MyStrings := TStringList.Create;
  CS.GetFieldNames(MyStrings);
  for I := 0 to MyStrings.Count - 1 do
  begin
    FieldsString := FieldsString + ';' + MyStrings[I];
  end;
  Delete(FieldsString, 1, 1);
  result := FieldsString;
end;


procedure TEDIT_COPY.DoAttachEEEDCodes(const ProcessType:boolean);
var
  i: integer;
  MyStrings: string;
  test: array of variant;
  SaveRetrieve, indexFields : String;

  procedure UpdateSCHEDULED_E_DS;
  var
    x: integer;
  Begin
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Edit;
      if Assigned(CustomEDFunction) then
        CustomEDFunction
      else
      begin
        for x := 0 to DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldCount - 1 do
        begin
          if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_SCHEDULED_E_DS_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_DIRECT_DEPOSIT_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EFFECTIVE_DATE') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'CHANGED_BY') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'ACTIVE_RECORD') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'CREATION_DATE') then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields.Fields[x].AsVariant := Test[x];
          end;
        end;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger := wwcsCopy.FieldByName('EE_NBR').AsInteger;
      end;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
      EdList.Add(wwcsCopy.FieldByName('EE_NBR').AsString);
  End;

  procedure DoAttachEEEDCodesBase;
  var
    x: integer;
  var
     bLoop, bUpdate: Boolean;
  begin
      if (not UpdateEDs) and (not DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([wwcsCopy.FieldByName('EE_NBR').Value, EdNbr]), [])) then
      begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Insert;
        for x := 0 to DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldCount - 1 do
        begin
          if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_SCHEDULED_E_DS_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EE_DIRECT_DEPOSIT_NBR') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'REC_VERSION') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EFFECTIVE_DATE') and
             (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields[x].FieldName <> 'EFFECTIVE_UNTIL') then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Fields.Fields[x].AsVariant := Test[x];
          end;
        end;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger := wwcsCopy.FieldByName('EE_NBR').AsInteger;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
        EdList.Add(wwcsCopy.FieldByName('EE_NBR').AsString);
      end
      else
      begin
           // ignore filter option selected
         if StartDate = 0 then
         begin
            if (UpdateEDs) and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR;EFFECTIVE_END_DATE', VarArrayOf([wwcsCopy.FieldByName('EE_NBR').Value, EdNbr, Null]), [])) then
                 UpdateSCHEDULED_E_DS;
         end
         else
         begin
            if (UpdateEDs) and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([wwcsCopy.FieldByName('EE_NBR').Value, EdNbr]), [])) then
            begin
              bLoop := True;
              While bLoop do
              begin
                bUpdate := false;
                if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger = wwcsCopy.FieldByName('EE_NBR').Value) and
                   (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger = EdNbr) then
                begin
                   //   use only start date
                   if EndDate = 0 then
                   begin
                      if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= StartDate) then
                          bUpdate := true;
                   end
                   else
                   begin
                   //   use date range
                      if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime = StartDate) and
                         (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').AsDateTime = EndDate)  then
                          bUpdate := true;
                   end;
                end
                else
                 bLoop := false;

                if bUpdate then
                    UpdateSCHEDULED_E_DS;

                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Next;
              end;
            end;
         end;
      end
  end;

begin
  MyStrings := PrepareNewRow(DM_EMPLOYEE.EE_SCHEDULED_E_DS);
  test := DM_EMPLOYEE.EE_SCHEDULED_E_DS[MyStrings];
  SaveRetrieve := DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition;
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition := '';
  indexFields := DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames;
  try
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames:= 'EE_NBR;CL_E_DS_NBR;EFFECTIVE_END_DATE';
    with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
    begin
      DisableControls;

      if ProcessType then
      begin
           //  ALL
        wwcsCopy.first;
        while not wwcsCopy.Eof do
        begin
            DoAttachEEEDCodesBase;
            wwcsCopy.next;
        end;
      end
      else
      begin
          //  Selected
        for i := 0 to SelectedList.Count - 1 do
        begin
          GotoBookmark(SelectedList.items[i]);
          DoAttachEEEDCodesBase;
        end;
      end;
      EnableControls; { Re-enable controls }
    end;
   finally
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition := SaveRetrieve;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames :=  indexFields;
   end;
end;

procedure TEDIT_COPY.CopyReports;
var
  i, x: integer;
  CustomName: String;
  CoNbr :Integer;
  F: TField;
begin
  DM_COMPANY.CO.Close;
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);

      ctx_DataAccess.OpenClient(wwcsCopy.FieldByName('CL_NBR').AsInteger);
      CoNbr := wwcsCopy.FieldByName('CO_NBR').AsInteger;

      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(CoNbr));
      DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR='+IntToStr(CoNbr));

      wwcsCoReports.First;
      while not wwcsCoReports.Eof do
      begin
        CustomName  := wwcsCoReports.FieldByName('CUSTOM_NAME').AsString;

        if DM_COMPANY.CO_REPORTS.Locate('CUSTOM_NAME', CustomName, []) then
          DM_COMPANY.CO_REPORTS.Edit
        else
          DM_COMPANY.CO_REPORTS.Insert;

        for x := 0 to wwcsCoReports.FieldCount - 1 do //DM_COMPANY.CO_REPORTS.FieldCount - 1 do
        begin
          if (wwcsCoReports.Fields[x].FieldName <> 'CO_REPORTS_NBR') and
             (wwcsCoReports.Fields[x].FieldName <> 'CO_NBR') and
             (wwcsCoReports.Fields[x].FieldName <> 'REC_VERSION') and
             (wwcsCoReports.Fields[x].FieldName <> 'EFFECTIVE_DATE') and
             (wwcsCoReports.Fields[x].FieldName <> 'EFFECTIVE_UNTIL') then
          begin
             F := DM_COMPANY.CO_REPORTS.FindField(wwcsCoReports.Fields[x].FieldName);
             if Assigned(F) then
                F.Assign(wwcsCoReports.Fields[x]);
          end;
        end;
        DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger := CoNbr;
        DM_COMPANY.CO_REPORTS.Post;
        wwcsCoReports.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_REPORTS]);
    end;
    EnableControls; { Re-enable controls }
  end;
end;


procedure TEDIT_COPY.CopyJobs;
var
  i, x: integer;
  Description: String;
  CoNbr :Integer;
  F: TField;
begin
  DM_COMPANY.CO.Close;
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);

      ctx_DataAccess.OpenClient(wwcsCopy.FieldByName('CL_NBR').AsInteger);
      CoNbr := wwcsCopy.FieldByName('CO_NBR').AsInteger;

      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(CoNbr));
      DM_COMPANY.CO_JOBS.DataRequired('CO_NBR='+IntToStr(CoNbr));

      wwcsCoReports.First;
      while not wwcsCoReports.Eof do
      begin
        Description  := wwcsCoReports.FieldByName('DESCRIPTION').AsString;

        if DM_COMPANY.CO_JOBS.Locate('DESCRIPTION', Description, []) then
          DM_COMPANY.CO_JOBS.Edit
        else
          DM_COMPANY.CO_JOBS.Insert;

        for x := 0 to wwcsCoReports.FieldCount - 1 do
        begin
          if (wwcsCoReports.Fields[x].FieldName <> 'CO_JOBS_NBR') and
             (wwcsCoReports.Fields[x].FieldName <> 'CO_NBR') and
             (wwcsCoReports.Fields[x].FieldName <> 'CO_LOCATIONS_NBR') and             
             (wwcsCoReports.Fields[x].FieldName <> 'REC_VERSION') and
             (wwcsCoReports.Fields[x].FieldName <> 'EFFECTIVE_DATE') and
             (wwcsCoReports.Fields[x].FieldName <> 'EFFECTIVE_UNTIL') then
          begin
             F := DM_COMPANY.CO_JOBS.FindField(wwcsCoReports.Fields[x].FieldName);
             if Assigned(F) then
                F.Assign(wwcsCoReports.Fields[x]);
          end;
        end;
        DM_COMPANY.CO_JOBS.FieldByName('CO_NBR').AsInteger := CoNbr;
        DM_COMPANY.CO_JOBS.Post;
        wwcsCoReports.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS]);
    end;
    EnableControls;
  end;
end;

procedure TEDIT_COPY.SetupForCopy;
begin
  bttnCopyAll.Caption  := 'Copy to All';//'Copy to All Companies';
  bttnCopySelected.Caption  := 'Copy to Selected';//'Copy to Selected Companies';
  wwcsCopy.Filter := 'CUSTOM_COMPANY_NUMBER<>'+QuotedStr(DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

  DM_TEMPORARY.TMP_CO.DataRequired('ALL');

  wwcsCopy.Data := DM_TEMPORARY.TMP_CO.Data;
  if wwcsCopy.IndexDefs.IndexOf('SORT') <> -1 then
    wwcsCopy.DeleteIndex('SORT');
  wwcsCopy.AddIndex('SORT', 'CUSTOM_COMPANY_NUMBER', [ixCaseInsensitive], '');
  wwcsCopy.IndexName := 'SORT';
  wwgdCopy.Selected.Text :=
    'CUSTOM_COMPANY_NUMBER' + #9 + '20' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
    'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;

  wwcsCopy.Filtered := True;
end;

procedure TEDIT_COPY.CopyCLEDTOCOEDCodes;
var
  I, T: integer;
begin

 DM_COMPANY.CO_E_D_CODES.DataRequired('ALL');

  with wwgdCopy do
  begin
    for i := 0 to SelectedList.Count - 1 do
    begin
      wwgdCopy.DataSource.DataSet.GotoBookmark(SelectedList.items[i]);
      for T := 0 to EdList.Count - 1 do
      begin
        if not DM_COMPANY.CO_E_D_CODES.Locate('CO_NBR;CL_E_DS_NBR', VarArrayOf([wwdsCopy.DataSet.FieldByName('CO_NBR').Value, EDList[T]]), []) then
        begin
          DM_Client.CL_E_DS.Locate('CL_E_DS_NBR',VarArrayOf([EDList[T]]), []);
          DM_COMPANY.CO_E_D_CODES.Insert;
          DM_COMPANY.CO_E_D_CODES.FieldByName('CL_E_DS_NBR').AsInteger := StrToInt(EDList[T]);
          DM_COMPANY.CO_E_D_CODES.FieldByName('CO_NBR').AsInteger := wwdsCopy.DataSet.FieldByName('CO_NBR').AsInteger;
          DM_COMPANY.CO_E_D_CODES.FieldByName('DISTRIBUTE').AsString := 'Y';

          if Context.License.HR then
             SetUpCO_E_D_CODESDefault(DM_Client.CL_E_DS.FieldByName('E_D_CODE_TYPE').AsString,
                                      LowerCase(DM_Client.CL_E_DS.FieldByName('DESCRIPTION').AsString));
          DM_COMPANY.CO_E_D_CODES.Post;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_COPY.DoAttachEERates;
var
  i: integer;
  SaveRetrieve: String;
begin
  SaveRetrieve := DM_EMPLOYEE.EE_RATES.RetrieveCondition;
  DM_EMPLOYEE.EE_RATES.RetrieveCondition := '';
  DM_EMPLOYEE.EE_RATES.IndexFieldNames := 'EE_NBR';
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    try
      for i := 0 to SelectedList.Count - 1 do
      begin
        GotoBookmark(SelectedList.items[i]);
        if DM_EMPLOYEE.EE_RATES.Locate('EE_NBR', wwcsCopy['EE_NBR'], []) then
          while not DM_EMPLOYEE.EE_RATES.Eof and
                (DM_EMPLOYEE.EE_RATES['EE_NBR'] = wwcsCopy['EE_NBR']) do
          begin
            CustomEDFunction;
            DM_EMPLOYEE.EE_RATES.Next;
          end
      end;
    finally
      EnableControls; { Re-enable controls }
      DM_EMPLOYEE.EE_RATES.RetrieveCondition := SaveRetrieve;
    end;
  end;
end;

procedure TEDIT_COPY.DoAttachEEs;
var
  i: integer;
begin
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    try
      for i := 0 to SelectedList.Count - 1 do
      begin
        GotoBookmark(SelectedList.items[i]);
        if DM_EMPLOYEE.EE.Locate('EE_NBR', wwcsCopy['EE_NBR'], []) then
          CustomEDFunction;
      end;
    finally
      EnableControls;
    end;
  end;
end;

procedure TEDIT_COPY.UpdatePwAPSetup; // Update 'Print with Adjustment Payrolls' field setup (reso# 46084)
begin
  bttnCopyAll.Caption  := 'Update for All';//'Update for All Companies';
  bttnCopySelected.Caption  := 'Update for Selected';//'Update for Selected Companies';
  Self.Caption := 'Companies';

  DM_TEMPORARY.TMP_CO.DataRequired('ALL');

  wwcsCopy.Data := DM_TEMPORARY.TMP_CO.Data;
  if wwcsCopy.IndexDefs.IndexOf('SORT') <> -1 then
    wwcsCopy.DeleteIndex('SORT');
  wwcsCopy.AddIndex('SORT', 'CUSTOM_COMPANY_NUMBER', [ixCaseInsensitive], '');
  wwcsCopy.IndexName := 'SORT';
  wwgdCopy.Selected.Text :=
    'CUSTOM_COMPANY_NUMBER' + #9 + '20' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
    'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;
end;

procedure TEDIT_COPY.UpdatePwAp; // Update 'Print with Adjustment Payrolls' field (reso# 46084)
var
  i: integer;
  CustomName: String;
  CoNbr :Integer;
  FRunParams: TevPrRepPrnSettings;
begin
  FRunParams := TevPrRepPrnSettings.Create(nil);

  DM_COMPANY.CO.Close;
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);

      ctx_DataAccess.OpenClient(wwcsCopy.FieldByName('CL_NBR').AsInteger);
      CoNbr := wwcsCopy.FieldByName('CO_NBR').AsInteger;

      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(CoNbr));
      DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR='+IntToStr(CoNbr));

      wwcsCoReports.First;
      while not wwcsCoReports.Eof do
      begin
        CustomName  := wwcsCoReports.FieldByName('CUSTOM_NAME').AsString;

        if DM_COMPANY.CO_REPORTS.Locate('CUSTOM_NAME', CustomName, []) then
        begin
          DM_COMPANY.CO_REPORTS.Edit;

          if not DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS').IsNull then
            try
              FRunParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')))
            except
              FRunParams.Clear;
            end
          else
          FRunParams.Clear;

          if (CopyFrom = 'Set') and (Assigned(FRunParams)) then
            FRunParams.PrnWithAdjPayrolls := True;
          if (CopyFrom = 'Unset') and (Assigned(FRunParams)) then
            FRunParams.PrnWithAdjPayrolls := False;

          FRunParams.SaveToBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));
        end;

        if (DM_COMPANY.CO_REPORTS.State = dsEdit) then DM_COMPANY.CO_REPORTS.Post;
        wwcsCoReports.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_REPORTS]);
    end;
    EnableControls;
  end;

  FRunParams.Free;
end;

procedure TEDIT_COPY.wwgdCopy1FilterChange(Sender: TObject;
  DataSet: TDataSet; NewFilter: String);
begin
  FilterStr := NewFilter;
end;

/////////////////////////////////////////////////////
// General Ledger Copy Feature. RESO#25329. Andrew //
/////////////////////////////////////////////////////

procedure TEDIT_COPY.GLSetup;
begin
  DM_TEMPORARY.TMP_CO.DataRequired('ALL');
  wwcsCopy.Data := DM_TEMPORARY.TMP_CO.Data;
  if wwcsCopy.IndexDefs.IndexOf('SORT') <> -1 then
  wwcsCopy.DeleteIndex('SORT');
  wwcsCopy.AddIndex('SORT', 'CUSTOM_COMPANY_NUMBER', [ixCaseInsensitive], '');
  wwcsCopy.IndexName := 'SORT';
  wwgdCopy.Selected.Text :=
    'CUSTOM_COMPANY_NUMBER' + #9 + '20' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
    'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
  wwdsCopy.Dataset := wwcsCopy;
  wwcsCopy.First;

  bttnCopySelected.Caption := 'Copy to Selected';
  bttnCopySelected.ModalResult := mrNone;
  bttnCopyAll.Caption := 'Copy to All';
  bttnCopyAll.ModalResult := mrNone;
  Caption := 'Copy General Ledger';
  Self.SetFocusedControl(bttnCancel);

  //Definition of Exception Report dataset (wwcsExceptions) fields.
  with TIntegerField.Create(wwcsExceptions) do
  begin
    FieldName := 'CO_NBR';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 0;
    Visible := False;
  end;
  with TIntegerField.Create(wwcsExceptions) do
  begin
    FieldName := 'CL_NBR';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 1;
    Visible := False;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'Custom_Client_Number';
    Size := 20;
    DisplayLabel := 'Client';
    DataSet := wwcsExceptions;
    FieldKind := fkLookup;
    LookupDataset := DM_TEMPORARY.TMP_CO;
    LookupKeyFields := 'CO_NBR;CL_NBR';
    LookupResultField := 'CUSTOM_CLIENT_NUMBER';
    KeyFields := 'CO_NBR;CL_NBR';
    Lookup := True;
    Index := 2;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'Custom_Company_Number';
    Size := 20;
    DisplayLabel := 'Company';
    DataSet := wwcsExceptions;
    FieldKind := fkLookup;
    LookupDataset := DM_TEMPORARY.TMP_CO;
    LookupKeyFields := 'CO_NBR;CL_NBR';
    LookupResultField := 'CUSTOM_COMPANY_NUMBER';
    KeyFields := 'CO_NBR;CL_NBR';
    Lookup := True;
    Index := 3;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'LevelType_Name';
    Size := 30;
    DisplayLabel := 'Level Type';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 4;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'Level_Name';
    Size := 30;
    DisplayLabel := 'Level Value';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 5;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'DataType_Name';
    Size := 30;
    DisplayLabel := 'Data Type';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 6;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'Data_Name';
    Size := 30;
    DisplayLabel := 'Data Value';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 7;
  end;
  with TStringField.Create(wwcsExceptions) do
  begin
    FieldName := 'GENERAL_LEDGER_FORMAT';
    Size := 15;
    DisplayLabel := 'General Ledger Format';
    DataSet := wwcsExceptions;
    FieldKind := fkData;
    Index := 8;
  end;
  wwcsExceptions.CreateDataSet;
  wwdsExceptions.DataSet := wwcsExceptions;
end;

procedure TEDIT_COPY.CopyGL;
var i : Integer;
    CoNbr, ClNbr : String;
    LT, DT : Char;
    LevelName, DataName : String;
    DDs : TevClientDataSet;
    Lfn, Llfn, Dfn, Dlfn : String;
    DBDT: TevClientDataSet;

  procedure AddException;
  begin
    wwcsExceptions.Insert;
    wwcsExceptions['CO_NBR'] := CoNbr;
    wwcsExceptions['CL_NBR'] := ClNbr;
    wwcsExceptions['LevelType_Name'] := wwcsCopySecond['LevelType_Name'];
    wwcsExceptions['Level_Name'] := wwcsCopySecond['Level_Name'];
    wwcsExceptions['DataType_Name'] := wwcsCopySecond['DataType_Name'];
    wwcsExceptions['Data_Name'] := wwcsCopySecond['Data_Name'];
    wwcsExceptions['GENERAL_LEDGER_FORMAT'] := wwcsCopySecond['GENERAL_LEDGER_FORMAT'];
    wwcsExceptions.Post;
  end;

begin
  if wwgdCopy.SelectedList.Count = 0 then exit;
  DM_COMPANY.CO.Close;
  wwcsCopy.DisableControls;
  for i := 0 to wwgdCopy.SelectedList.Count - 1 do
  begin
    wwcsCopy.GotoBookmark(wwgdCopy.SelectedList.items[i]);
    ctx_DataAccess.OpenClient(wwcsCopy.FieldByName('CL_NBR').AsInteger);
    CoNbr := wwcsCopy.FieldByName('CO_NBR').AsString;
    ClNbr := wwcsCopy.FieldByName('CL_NBR').AsString;

    DM_COMPANY.CO.DataRequired('CO_NBR='+CoNbr);
    DM_CLIENT.CO_GENERAL_LEDGER.DataRequired('CO_NBR='+CoNbr);

    wwcsCopySecond.First;
    while not(wwcsCopySecond.Eof) do
    begin
      if DM_CLIENT.CO_GENERAL_LEDGER.Locate('LevelType_Name;Level_Name;DataType_Name;Data_Name', VarArrayOf([wwcsCopySecond['LevelType_Name'],
        wwcsCopySecond['Level_Name'], wwcsCopySecond['DataType_Name'], wwcsCopySecond['Data_Name']]), [])
      then
      begin
        DM_CLIENT.CO_GENERAL_LEDGER.Edit;
        DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('GENERAL_LEDGER_FORMAT').Value := wwcsCopySecond['GENERAL_LEDGER_FORMAT'];
      end
      else
      begin
        DM_CLIENT.CO_GENERAL_LEDGER.Insert;
        LT := wwcsCopySecond.FieldByName('LEVEL_TYPE').AsString[1];
        LevelName := wwcsCopySecond.FieldByName('Level_Name').AsString;
        DDs := nil;
        Lfn := '';
        case LT of
          GL_LEVEL_DIVISION:
            begin
              Lfn := 'CO_DIVISION_NBR';               // Level field name
              Llfn := 'CUSTOM_DIVISION_NUMBER';       // Level loolup field name
            end;
          GL_LEVEL_BRANCH:
            begin
              Lfn := 'CO_BRANCH_NBR';                 // Level field name
              Llfn := 'CUSTOM_BRANCH_NUMBER';         // Level loolup field name
            end;
          GL_LEVEL_DEPT:
            begin
              Lfn := 'CO_DEPARTMENT_NBR';             // Level field name
              Llfn := 'CUSTOM_DEPARTMENT_NUMBER';     // Level loolup field name
            end;
          GL_LEVEL_TEAM:
            begin
              Lfn := 'CO_TEAM_NBR';                   // Level field name
              Llfn := 'CUSTOM_TEAM_NUMBER';           // Level loolup field name
            end;
          GL_LEVEL_JOB:
            begin
              Lfn := 'CO_JOBS_NBR';                   // Level field name
              Llfn := 'Description';                  // Level loolup field name
            end;
          GL_LEVEL_EMPLOYEE:
            begin
              DM_CLIENT.CO_GENERAL_LEDGER.Cancel;     // This is spec requirement:
              wwcsCopySecond.Next;                    //   "GL records with Level Type of Employee are not exceptions.
              Continue;                               //    They should simply not be copied."
            end;
        end;
        DT := wwcsCopySecond.FieldByName('DATA_TYPE').AsString[1];
        DataName := wwcsCopySecond.FieldByName('Data_Name').AsString;
        case DT of
          GL_DATA_ED,GL_DATA_ED_OFFSET:
            begin
              DDs := DM_COMPANY.CO_E_D_CODES;     // DATA Dataset
              Dfn := 'CL_E_DS_NBR';               // DATA field  name
              Dlfn := 'ED_Lookup';                // DATA lookup field name
            end;
          GL_DATA_AGENCY_CHECK,GL_DATA_AGENCY_CHECK_OFFSET:
            begin
              DDs := DM_CLIENT.CL_AGENCY;     // DATA Dataset
              Dfn := 'CL_AGENCY_NBR';               // DATA field  name
              Dlfn := 'Agency_Name';                // DATA lookup field name
            end;
          GL_DATA_WC_IMPOUND,GL_DATA_WC_IMPOUND_OFFSET:
            begin
              DDs := DM_COMPANY.CO_WORKERS_COMP;     // DATA Dataset
              Dfn := 'CO_WORKERS_COMP_NBR';               // DATA field  name
              Dlfn := 'WORKERS_COMP_CODE';                // DATA lookup field name
            end;
          GL_DATA_STATE,GL_DATA_EE_SDI,GL_DATA_ER_SDI,GL_DATA_EE_SDI_OFFSET,GL_DATA_ER_SDI_OFFSET,GL_DATA_STATE_OFFSET:
            begin
              DDs := DM_COMPANY.CO_STATES;     // DATA Dataset
              Dfn := 'CO_STATES_NBR';               // DATA field  name
              Dlfn := 'STATE';                // DATA lookup field name
            end;
          GL_DATA_EE_SUI,GL_DATA_EE_SUI_OFFSET:
            begin
              DDs := DM_COMPANY.CO_SUI;     // DATA Dataset
              Dfn := 'CO_SUI_NBR';               // DATA field  name
              Dlfn := 'Sui_Name';                // DATA lookup field name
            end;
          GL_DATA_ER_SUI,GL_DATA_ER_SUI_OFFSET:
            begin
              DDs := DM_COMPANY.CO_SUI;     // DATA Dataset
              Dfn := 'CO_SUI_NBR';               // DATA field  name
              Dlfn := 'Sui_Name';                // DATA lookup field name
            end;
          GL_DATA_LOCAL,GL_DATA_LOCAL_OFFSET:
            begin
              DDs := DM_COMPANY.CO_LOCAL_TAX;     // DATA Dataset
              Dfn := 'CO_LOCAL_TAX_NBR';               // DATA field  name
              Dlfn := 'LocalName';                // DATA lookup field name
            end;
        end;

        if (Lfn <> '') and (LevelName <> '') then
        begin
          if LT = GL_LEVEL_JOB then
          begin
            DM_COMPANY.CO_JOBS.DataRequired('CO_NBR=' + CoNbr);

            if DM_COMPANY.CO_JOBS.Locate(Llfn, LevelName, []) then
              DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('LEVEL_NBR').Value := DM_COMPANY.CO_JOBS[Lfn]
            else
            begin
              AddException;
              DM_CLIENT.CO_GENERAL_LEDGER.Cancel;
              wwcsCopySecond.Next;
              Continue;
            end;
          end
          else
          begin
            DBDT := TevClientDataSet.Create(nil);
            try
              with TExecDSWrapper.Create('SelectDBDTWithName') do
              begin
                SetParam('CoNbr', CoNbr);
                ctx_DataAccess.GetCustomData(DBDT, AsVariant);
              end;

              ctx_DataAccess.OpenDataSets([DBDT]);

              if DBDT.Locate(Llfn, LevelName, []) then
                DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('LEVEL_NBR').Value := DBDT[Lfn]
              else
              begin
                AddException;
                DM_CLIENT.CO_GENERAL_LEDGER.Cancel;
                wwcsCopySecond.Next;
                Continue;
              end;
            finally
              DBDT.Free;
            end;
          end;  
        end;
        if Assigned(DDs) and (DataName<>'') then
        begin
          if Assigned(DDs.FindField('CO_NBR')) then
            DDs.DataRequired('CO_NBR='+CoNbr)
          else
            DDs.DataRequired();
          if DDs.Locate(Dlfn, DataName, []) then
            DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('DATA_NBR').Value := DDs[Dfn]
          else
          begin
            AddException;
            DM_CLIENT.CO_GENERAL_LEDGER.Cancel;
            wwcsCopySecond.Next;
            Continue;
          end;
        end;
        DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('LEVEL_TYPE').Value := LT;
        DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('DATA_TYPE').Value := DT;
        DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('GENERAL_LEDGER_FORMAT').Value := wwcsCopySecond['GENERAL_LEDGER_FORMAT'];
      end;
      DM_CLIENT.CO_GENERAL_LEDGER.Post;
      wwcsCopySecond.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_CLIENT.CO_GENERAL_LEDGER]);
  end;
  AskToShowExceptions;
end;

procedure TEDIT_COPY.AskToShowExceptions;
var
  MsgDlg : TForm;
  i: Integer;
begin
  MsgDlg := CreateMessageDialog('Copy of General Ledger records is complete.', mtInformation,[mbIgnore, mbOk]);
  MsgDlg.Caption := 'Copy complete';
  for i := 0 to MsgDlg.ComponentCount - 1 do
  with MsgDlg.Components[i] do
  begin
    if (ClassName = 'TButton') and (Name = 'Ignore') then
    with MsgDlg.Components[i] as TButton do
    begin
      Caption := 'View Exceptions';
      Width := 129;
      Left := Left - 23;
      Enabled := wwcsExceptions.RecordCount > 0;
    end
    else if (ClassName = 'TButton') and (Name = 'OK') then
    with MsgDlg.Components[i] as TButton do
    begin
      Caption := 'Done';
      Width := Width + 20;
      Left := Left - 43;
    end
    else if (ClassName = 'TLabel') and (Name = 'Message') then
    with MsgDlg.Components[i] as TLabel do
    begin
      Left := Left + 7;
    end;
  end;
  if MsgDlg.ShowModal = mrIgnore then
    tshExceptions.Show
  else
    ModalResult := mrOk;
end;

procedure TEDIT_COPY.tshExceptionsShow(Sender: TObject);
begin
  btnDone.SetFocus;
end;

procedure TEDIT_COPY.btnPrintClick(Sender: TObject);
begin
  PrintExceptions;
end;

procedure TEDIT_COPY.PrintExceptions;
var
  I, J, X: Integer;
  rtDest: TRect;
begin
  if wwcsExceptions.RecordCount = 0 then
    Exit;

  wwcsExceptions.First;
  if PrintDialog1.Execute then
  begin
    Printer.BeginDoc;
    Printer.Canvas.Font := wwgdExceptions.Font;
    with wwcsExceptions do
    for I := 0 to RecordCount do
    begin
      X := 0;
      for J := 0 to Fields.Count - 1 do
        with Fields[J]do
        begin
          if not(Visible) then Continue;
          rtDest := Rect(100 + X, 100 * (I + 2), 200 + X, 100 * (I + 3));
          if I = 0 then //Draw header
          begin
            Printer.Canvas.Font.Style := Printer.Canvas.Font.Style + [fsBold];
            DrawText(Printer.Canvas.Handle, PChar(DisplayLabel), Length(DisplayLabel), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
            Printer.Canvas.Font.Style := Printer.Canvas.Font.Style - [fsBold];
          end
          else
            DrawText(Printer.Canvas.Handle, PChar(AsString), Length(AsString), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
          X := X + Size*10;
        end;
      if (I <> 0) then Next;
    end;
    Printer.EndDoc;
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////
/// Update Emploee's Scheduled EDs after changes in Client ED. RESO#17601. Andrew ///
/////////////////////////////////////////////////////////////////////////////////////

procedure TEDIT_COPY.Setup_UpdateEEEDCodes;
begin
  bttnCopyAll.Caption  := 'Update for All';
  bttnCopySelected.Caption  := 'Update for Selected';
  Self.Caption := 'Select Companies';
  wwcsCopy.Filter := 'CL_NBR='+QuotedStr(DM_CLIENT.CL.FieldByName('CL_NBR').AsString);

  DM_TEMPORARY.TMP_CO.DataRequired('ALL');

  wwcsCopy.Data := DM_TEMPORARY.TMP_CO.Data;
  if wwcsCopy.IndexDefs.IndexOf('SORT') <> -1 then
    wwcsCopy.DeleteIndex('SORT');
  wwcsCopy.AddIndex('SORT', 'CUSTOM_COMPANY_NUMBER', [ixCaseInsensitive], '');
  wwcsCopy.IndexName := 'SORT';
  wwcsCopy.First;
  wwgdCopy.Selected.Text :=
    'CUSTOM_COMPANY_NUMBER' + #9 + '20' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
    'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
  wwdsCopy.DataSet := wwcsCopy;

  wwcsCopy.Filtered := True;
end;

procedure TEDIT_COPY.UpdateEEEDCodes;
var
  i: integer;
  sTmp: string;
begin
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition := '';
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('ALL');
  with wwgdCopy, TevClientDataSet(wwgdCopy.datasource.dataset) do
  begin
    DisableControls;
    for i := 0 to SelectedList.Count - 1 do
    begin
      GotoBookmark(SelectedList.items[i]);
      DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + QuotedStr(wwcsCopy.FieldByName('CO_NBR').AsString));
      sTmp:=DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames := 'EE_NBR;CL_E_DS_NBR';
      try
          DM_EMPLOYEE.EE.First;
          while not(DM_EMPLOYEE.EE.Eof) do
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.SetRange([DM_EMPLOYEE.EE['EE_NBR'],EdNbr], [DM_EMPLOYEE.EE['EE_NBR'],EdNbr]);
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.First;
            while not DM_EMPLOYEE.EE_SCHEDULED_E_DS.Eof do
            begin
              if Assigned(CustomEDFunction) then
              begin
                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Edit;
                CustomEDFunction;
                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
              end;
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.Next;
            end;
            DM_EMPLOYEE.EE.Next;
          end;
       finally
         DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames := sTmp;
       end;
    end;
    ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
    EnableControls; { Re-enable controls }
  end;
end;

end.



