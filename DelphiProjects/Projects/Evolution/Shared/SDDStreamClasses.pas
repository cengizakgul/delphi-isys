unit SDDStreamClasses;

interface

uses
  SysUtils, rwCustomDataDictionary, Classes, Math, SyncObjs, DB, rwEngineTypes, EvStreamUtils,
  SimpleXML, isBasicUtils, isBaseClasses;

type
  TddsField = class(TDataDictField)
  private
    FRequired: boolean;
    FContextHelp: string;
    FDefaultValue: string;
    FDisplayFormat: String;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Required: boolean read FRequired write FRequired;
    property ContextHelp: string read FContextHelp write FContextHelp;
    property DefaultValue: string read FDefaultValue write FDefaultValue;
    property DisplayFormat: String read FDisplayFormat write FDisplayFormat;
  end;


  TddsForeignKey = class(TDataDictForeignKey)
  private
    FMasterDetailRelation: Boolean;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property MasterDetailRelation: Boolean read FMasterDetailRelation write FMasterDetailRelation default False;
  end;


  TddsTable = class(TDataDictTable)
  private
    FDisplayKey: String;
  public
    function  GetMasterKey: TddsForeignKey;
    procedure Assign(Source: TPersistent); override;
  published
    property DisplayKey: String read FDisplayKey write FDisplayKey;
  end;

  
  TddsDataDictionary = class(TDataDictionary)
  public
    function  GetTableClass: TDataDictTableClass; override;
    function  GetFieldClass: TDataDictFieldClass; override;
    function  GetForeignKeyClass: TDataDictForeignKeyClass; override;
    procedure Initialize;
  end;


  function EvoDataDictionary: TddsDataDictionary;
  function EvoDataDictionaryXML: IisStream;

implementation

uses SDDClasses, SDataStructure,  SFieldCodeValues, EvTypes, EvConsts;

var
  FCS: IisLock;
  FDataDictionary: TddsDataDictionary;
  FXMLDataDictionary: IisStream;

function EvoDataDictionary: TddsDataDictionary;
begin
  FCS.Lock;
  try
    if not Assigned(FDataDictionary) then
    begin
      FDataDictionary := TddsDataDictionary.Create(nil);
      FDataDictionary.Initialize;
    end;
    Result := FDataDictionary;
  finally
    FCS.Unlock;
  end;
end;


{ TddsField }

procedure TddsField.Assign(Source: TPersistent);
begin
  inherited;
  FRequired := TddsField(Source).FRequired;
  FContextHelp := TddsField(Source).FContextHelp;
  FDefaultValue := TddsField(Source).FDefaultValue;
  FDisplayFormat := TddsField(Source).FDisplayFormat;
end;

{ TddsDataDictionary }

function TddsDataDictionary.GetFieldClass: TDataDictFieldClass;
begin
  Result := TddsField;
end;

function TddsDataDictionary.GetForeignKeyClass: TDataDictForeignKeyClass;
begin
  Result := TddsForeignKey; 
end;

function TddsDataDictionary.GetTableClass: TDataDictTableClass;
begin
  Result := TddsTable;
end;

procedure TddsDataDictionary.Initialize;

  procedure AddTables(db: TddDatabaseClass);
  var
    tableList: TddTableList;
    fieldList: TddFieldList;
    table: TddTableClass;
    streamTable: TddsTable;
    streamField: TddsField;
    i, j, k: Integer;
    FKList: TddReferencesDesc;
    FK: TddsForeignKey;
  begin
    tableList := db.GetTableList;
    fieldList := nil;
    for i := 0 to High(tableList) do
    begin
      streamTable := TddsTable(Tables.Add);
      streamTable.Name := tableList[i];
      table := GetddTableClassByName(tableList[i]);
      fieldList := table.GetFieldList;

      for j := 0 to High(fieldList) do
      begin
        streamField := TddsField(streamTable.Fields.Add);
        streamField.Name := fieldList[j];
        streamField.Required := table.GetIsRequiredField(fieldList[j]);
        streamField.DisplayName := table.GetFieldDisplayLabel(fieldList[j]);

        case table.GetFieldType(streamField.Name) of
          ftUnknown:  streamField.FieldType := ddtUnknown;

          ftString:   begin
                        streamField.FieldType := ddtString;
                        streamField.Size := table.GetFieldSize(streamField.Name);
                      end;

          ftSmallint,
          ftInteger,
          ftWord:     streamField.FieldType := ddtInteger;

          ftBoolean:  streamField.FieldType := ddtBoolean;
          ftFloat:    streamField.FieldType :=  ddtFloat;
          ftCurrency: streamField.FieldType := ddtCurrency;

          ftDate,
          ftTime,
          ftTimeStamp,
          ftDateTime: streamField.FieldType := ddtDateTime;

          ftBlob:     streamField.FieldType := ddtBLOB;
          ftMemo:     streamField.FieldType := ddtMemo;
          ftGraphic:  streamField.FieldType := ddtGraphic;
          ftArray:    streamField.FieldType := ddtArray;
        else
          Assert(false, 'Unsupported data type');
        end;

        for k := Low(FieldsToPopulate) to High(FieldsToPopulate) do
          if (AnsiCompareText(FieldsToPopulate[k].F, fieldList[j]) = 0) and
             (FieldsToPopulate[k].T = table) then
          begin
            streamField.FieldValues.Text := GetConstChoisesByField(tableList[i] + '_' + fieldList[j]);
            streamField.DefaultValue := FieldsToPopulate[k].D;
            streamField.DisplayFormat := table.GetDisplayFormat(streamField.Name);
            break;
          end;
      end;

      // Primary key
      fieldList := table.GetPrimaryKey;
      for j := 0 to High(fieldList) do
        streamTable.PrimaryKey.AddKeyField(streamTable.Fields.FieldByName(fieldList[j]));

      // Logical key
      fieldList := table.GetLogicalKeys;
      for j := 0 to High(fieldList) do
        streamTable.LogicalKey.AddKeyField(streamTable.Fields.FieldByName(fieldList[j]));

      streamTable.DisplayKey := table.GetDisplayKey;        
    end;

    // Foreign Key
    for i := 0 to High(tableList) do
    begin
      table := GetddTableClassByName(tableList[i]);
      streamTable := Tables.TableByName(tableList[i]) as TddsTable;

      FKList := table.GetParentsDesc;
      for j := 0 to High(FKList) do
      begin
        FK := TddsForeignKey(streamTable.ForeignKeys.AddForeignKey(Tables.TableByName(FKList[j].Table.GetTableName)));
        FK.MasterDetailRelation := FKList[j].MasterDetail;
        for k := 0 to High(FKList[j].SrcFields) do
          FK.Fields.AddKeyField(streamTable.Fields.FieldByName(FKList[j].SrcFields[k]));
      end;
    end; 
  end;

begin
  Tables.Clear;

  AddTables(TDM_SYSTEM);
  AddTables(TDM_SERVICE_BUREAU);
  AddTables(TDM_TEMPORARY);
  AddTables(TDM_CLIENT);
end;


function EvoDataDictionaryXML: IisStream;
var
  TablesNode: IXmlElement;

  procedure AddTables(db: TddDatabaseClass);
  const
    dbTypes: array [Low(TevDBType) .. High(TevDBType)] of string = ('', '', 'SY', 'SB', 'CL', 'TMP');
  var
    tableList: TddTableList;
    fieldList: TddFieldList;
    table: TddTableClass;
    i, j, k: Integer;
    TableNode, FieldsNode, FieldNode, FieldItemsNode, FieldItemNode, FKsNode, PKNode, FKNode: IXmlElement;
    sFldName, sValues, sDefValue, s, sFieldType, sDscr: String;
    FKList: TddReferencesDesc;
    FieldType: TFieldType;
    v: Variant;
  begin
    tableList := db.GetTableList;
    fieldList := nil;
    for i := 0 to High(tableList) do
    begin
      table := GetddTableClassByName(tableList[i]);

      if not Assigned(table) or table.ReadOnlyTable then
        continue;

      TableNode := TablesNode.AppendElement('Table');
      TableNode.SetVarAttr('Name', table.GetTableName);

      if not table.GetIsHistorical then
        TableNode.SetVarAttr('Historical', '0');

      TableNode.SetVarAttr('DBType', dbTypes[db.GetDBType]);

      s := table.GetEntityName();
      if s <> '' then
        TableNode.SetVarAttr('Alias', s);

      // Fields
      FieldsNode := TableNode.AppendElement('Fields');
      fieldList := table.GetFieldList;
      for j := 0 to High(fieldList) do
      begin
        sFldName := fieldList[j];

        sValues := '';
        sDefValue := '';
        for k := Low(FieldsToPopulate) to High(FieldsToPopulate) do
          if (AnsiCompareText(FieldsToPopulate[k].F, sFldName) = 0) and (FieldsToPopulate[k].T = table) then
          begin
            sValues := FieldsToPopulate[k].C;
            sDefValue := FieldsToPopulate[k].D;
            break;
          end;

        FieldType := table.GetFieldType(sFldName);
        case FieldType  of
          ftString:
          begin
            if sValues = '' then
              sFieldType := 'StringField'
            else if sValues = GroupBox_YesNo_ComboChoices then
              sFieldType := 'BooleanField'
            else
              sFieldType := 'EnumField';
          end;

          ftSmallint,
          ftInteger,
          ftWord:     sFieldType := 'IntegerField';

          ftFloat,
          ftCurrency: sFieldType := 'FloatField';

          ftDate:     sFieldType := 'DateField';

          ftTimeStamp,
          ftDateTime: sFieldType := 'TimeStampField';

          ftBlob,
          ftMemo,
          ftGraphic:  sFieldType := 'BlobField';
        else
          Assert(false, 'Unsupported data type');
        end;

        FieldNode := FieldsNode.AppendElement(sFieldType);
        FieldNode.SetVarAttr('Name', fieldList[j]);

        case FieldType of
          ftString:
          begin
            if sFieldType <> 'BooleanField' then
              FieldNode.SetVarAttr('Size', table.GetFieldSize(sFldName));

            if sFieldType = 'EnumField' then
            begin
              FieldItemsNode := FieldNode.AppendElement('Items');
              while sValues <> '' do
              begin
                FieldItemNode := FieldItemsNode.AppendElement('Item');
                s := GetNextStrValue(sValues, #13);
                sDscr := GetNextStrValue(s, #9);
                FieldItemNode.SetVarAttr('Value', s);
                if sDscr <> '' then
                  FieldItemNode.SetVarAttr('Description', sDscr);
              end;
            end;
          end;

          ftFloat,
          ftCurrency: FieldNode.SetVarAttr('Precision', table.GetFieldPrecision(sFldName));
        end;

        if table.GetIsRequiredField(fieldList[j]) and (sFieldType <> 'BooleanField') then
          FieldNode.SetVarAttr('Required', '1');

        s := table.GetFieldEntityProperty(fieldList[j]);
        if s <> '' then
          FieldNode.SetVarAttr('Alias', s);

        if (sFieldType <> 'BooleanField') and (sDefValue <> '') or (sFieldType = 'BooleanField') and (sDefValue = GROUP_BOX_YES)  then
        begin
          if sFieldType = 'DateField' then
            FieldNode.SetDateAttr('Default', TextToDate(sDefValue))
          else
          begin
             if sFieldType = 'TimeStampField' then
               v := StrToDateTime(sDefValue)
             else
               v := sDefValue;

             FieldNode.SetVarAttr('Default', v);
           end;
        end;
      end;

      // Primary key
      fieldList := table.GetPrimaryKey;
      if Length(fieldList) > 0 then
      begin
        PKNode := TableNode.AppendElement('PrimaryKey');
        for j := 0 to High(fieldList) do
          PKNode.AppendElement('Field').SetVarAttr('Name', fieldList[j]);
      end;

      // Foreign keys
      FKList := table.GetParentsDesc;
      if Length(FKList) > 0 then
      begin
        FKsNode := TableNode.AppendElement('ForeignKeys');
        for j := 0 to High(FKList) do
        begin
          FKNode := FKsNode.AppendElement('Key');
          FKNode.SetVarAttr('Table', FKList[j].Table.GetTableName);
          if FKList[j].ParentEntityPropName <> '' then
            FKNode.SetVarAttr('Alias', FKList[j].ParentEntityPropName);
          for k := 0 to High(FKList[j].SrcFields) do
            FKNode.AppendElement('Field').SetVarAttr('Name', FKList[j].SrcFields[k]);
        end;
      end;
    end;
  end;

var
  Doc: IXmlDocument;
begin
  FCS.Lock;
  try
    if not Assigned(FXMLDataDictionary) then
    begin
      Doc := CreateXmlDocument('EvolutionDataDictionary');
      Doc.DocumentElement.SetAttr('xmlns', 'http://www.isystemsllc.com/Evolution/13.0.0/EvDataDictionarySchema.xsd');

      TablesNode := Doc.DocumentElement.AppendElement('Tables');

      AddTables(TDM_SYSTEM);
      AddTables(TDM_SERVICE_BUREAU);
      AddTables(TDM_TEMPORARY);
      AddTables(TDM_CLIENT);

      FXMLDataDictionary := TisStream.Create;
      Doc.Save(FXMLDataDictionary);
    end;
    Result := FXMLDataDictionary.GetClone;
    Result.Position := 0;
  finally
    FCS.Unlock;
  end;
end;

{ TddsForeignKey }

procedure TddsForeignKey.Assign(Source: TPersistent);
begin
  inherited;
  FMasterDetailRelation := TddsForeignKey(Source).MasterDetailRelation;
end;

{ TddsTable }

procedure TddsTable.Assign(Source: TPersistent);
begin
  inherited;
  DisplayKey := TddsTable(Source).DisplayKey;
end;

function TddsTable.GetMasterKey: TddsForeignKey;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to ForeignKeys.Count - 1 do
    if TddsForeignKey(ForeignKeys[i]).MasterDetailRelation then
    begin
      Result := TddsForeignKey(ForeignKeys[i]);
      break;
    end;
end;

initialization
  FCS := TisLock.Create;
  RegisterClasses([TddsField, TddsForeignKey, TddsTable, TddsDataDictionary]);
  //EvoDataDictionaryXML.SaveToFile('c:\dd.xml');

finalization
  FDataDictionary.Free;
  UnregisterClasses([TddsField, TddsForeignKey, TddsTable, TddsDataDictionary]);

end.
