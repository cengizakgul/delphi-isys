unit EvVersionedPersonNameFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit;

type
  TevVersionedPersonName = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    lFirstName: TevLabel;
    lMiddleName: TevLabel;
    lLastName: TevLabel;
    edFirstName: TevDBEdit;
    edMiddleName: TevDBEdit;
    edLastName: TevDBEdit;
    pnlBottom: TevPanel;
    procedure FormShow(Sender: TObject);
  protected
    procedure EditBoxOnChange(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedPersonName }

procedure TevVersionedPersonName.EditBoxOnChange(Sender: TObject);
begin
  (Sender as TevDBEdit).UpdateRecord;
end;

procedure TevVersionedPersonName.FormShow(Sender: TObject);
begin
  inherited;

  Fields[0].AddValue('Title', lFirstName.Caption);
  Fields[0].AddValue('Width', 18);
  lFirstName.Caption := Iff(Fields[0].Value['Required'], '~', '') + lFirstName.Caption;
  edFirstName.DataField := Fields.ParamName(0);

  Fields[1].AddValue('Title', lMiddleName.Caption);
  Fields[1].AddValue('Width', 6);
  lMiddleName.Caption := Iff(Fields[1].Value['Required'], '~', '') + lMiddleName.Caption;
  edMiddleName.DataField := Fields.ParamName(1);

  Fields[2].AddValue('Title', lLastName.Caption);
  Fields[2].AddValue('Width', 18);
  lLastName.Caption := Iff(Fields[2].Value['Required'], '~', '') + lLastName.Caption;
  edLastName.DataField := Fields.ParamName(2);
end;

end.

