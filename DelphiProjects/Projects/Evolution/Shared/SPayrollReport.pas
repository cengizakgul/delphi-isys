// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPayrollReport;

interface

uses SysUtils, Classes, EvUtils, EvTypes, DB, EvConsts, Variants, EvBasicUtils, EvStreamUtils,
     EvContext;

type

  TevPrRepPrnSettings = class(TComponent)
  private
    FFrequency: String;
    FPriority: Word;
    FMonthNumber: Byte;
    FWeekOfMonth: String;
    FCopies: Byte;
    FSummarized: Boolean;
    FHideForRemotes: Boolean;
    FPrnWithAdjPayrolls: Boolean;
    FDisplayOnDashboard: Boolean;

    function  WeekOfMonthIsNotEmpty: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure SaveToBlobField(AField: TBlobField);
    procedure ReadFromBlobField(AField: TBlobField);
    procedure Clear;

  published
    property Frequency: String read FFrequency  write FFrequency;
    property Copies: Byte read FCopies write FCopies default 1;
    property Priority: Word read FPriority write FPriority default 0;
    property MonthNumber: Byte read FMonthNumber write FMonthNumber default 0;
    property WeekOfMonth: String read FWeekOfMonth write FWeekOfMonth stored WeekOfMonthIsNotEmpty;
    property Summarized: Boolean read FSummarized write FSummarized default False;
    property HideForRemotes: Boolean read FHideForRemotes write FHideForRemotes default False;
    property PrnWithAdjPayrolls: Boolean read FPrnWithAdjPayrolls write FPrnWithAdjPayrolls default False;
    property DisplayOnDashboard: Boolean read FDisplayOnDashboard write FDisplayOnDashboard default False;
  end;


procedure PayrollReports(const PrNbr, CoNbr, ClientNbr: Integer; ADestination: TrwReportDestination = rdtPrinter);
function  PayrollsReports(const CoNbr, ClientNbr: Integer; PrList: variant): IisStream;

implementation


uses SDataStructure,  SReportSettings;


type
  TRepInfo = class(TCollectionItem)
  private
    FPriority: Word;
    FInputParams: TrwReportParams;
    FNBR: Integer;
    FLevel: String;
    FCaption: String;
    FOverrideVmrMbGroupNbr: Integer;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

    property NBR: Integer read FNBR write FNBR;
    property Level: String read FLevel write FLevel;
    property Caption: String read FCaption write FCaption;
    property Priority: Word read FPriority write FPriority;
    property InputParams: TrwReportParams read FInputParams;
    property OverrideVmrMbGroupNbr: Integer read FOverrideVmrMbGroupNbr write FOverrideVmrMbGroupNbr;
  end;


  TRepInfoList = class(TCollection)
  private
    FDestination: TrwReportDestination;
    function GetItem(Index: Integer): TRepInfo;

  public
    function  AddReport: TRepInfo;
    procedure SortByPriority;
    procedure Run;
    function  GetResult: IisStream;
    property  Items[Index: Integer]: TRepInfo read GetItem; default;
    property  Destination: TrwReportDestination read FDestination write FDestination;
  end;



procedure PayrollReports(const PrNbr, CoNbr, ClientNbr: Integer; ADestination: TrwReportDestination = rdtPrinter);
var
  RunPar: TevPrRepPrnSettings;
  RepList: TRepInfoList;
  R: TRepInfo;
  CheckDate: TDateTime;
  fl_print_not_all_reports: Boolean;
  fl_scheduled_payroll: Boolean;
  IsAdjustmentPayroll: boolean;
  PrList: TList;
  Arr: Variant;
  i: Integer;
  sOldIndex: string;

  function CheckWeekNbr: Boolean;
  var
    cYear, cMonth, cDay: Word;
    PriorDate: TDateTime;
    pYear, pMonth, pDay: Word;
    NextDate: TDateTime;
    nYear, nMonth, nDay: Word;
    pDiff, nDiff, cDiff: Integer;
    CalcDate: TDateTime;
  begin
    Result := False;

    if RunPar.WeekOfMonth = '' then
      Exit;

    DecodeDate(CheckDate, cYear, cMonth, cDay);

    case RunPar.WeekOfMonth[1] of
      SCHED_FREQ_FIRST_SCHED_PAY:
        CalcDate := EncodeDate(cYear, cMonth, 1);

      SCHED_FREQ_LAST_SCHED_PAY:
        if cMonth = 12 then
          CalcDate := EncodeDate(cYear, 12, 31)
        else
          CalcDate := EncodeDate(cYear, cMonth + 1, 1) - 1;

      SCHED_FREQ_MID_SCHED_PAY:
        CalcDate := EncodeDate(cYear, cMonth, 15);
    else
      Exit;
    end;

    DM_PAYROLL.PR_SCHEDULED_EVENT.FindNearest([CalcDate]);
    NextDate := DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime;
    DecodeDate(NextDate, nYear, nMonth, nDay);
    DM_PAYROLL.PR_SCHEDULED_EVENT.Prior;
    PriorDate := DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime;
    DecodeDate(PriorDate, pYear, pMonth, pDay);

    if (cYear = pYear) and (cMonth = pMonth) then
      pDiff := Trunc(Abs(PriorDate - CalcDate))
    else
      pDiff := MaxInt;

    if (cYear = nYear) and (cMonth = nMonth) then
      nDiff := Trunc(Abs(NextDate - CalcDate))
    else
      nDiff := MaxInt;

    cDiff := Trunc(Abs(CheckDate - CalcDate));

    Result := (cDiff <= nDiff) and (cDiff <= pDiff);

    if Result and (CheckDate > CalcDate) and (cDiff = pDiff) then
      Result := False;
  end;


  procedure PayrollsToInclude(Freq: Char);
  var
    dBeginDate: TDateTime;
    cYear, cMonth, cDay: Word;
  begin
    PrList.Clear;
    DecodeDate(CheckDate, cYear, cMonth, cDay);

    case Freq of
      'm':
        dBeginDate := EncodeDate(cYear, cMonth, 1);

      'q':
        dBeginDate := GetBeginQuarter(CheckDate);

      's':
        if (cMonth <= 6) then
          dBeginDate := EncodeDate(cYear, 1, 1)
        else
          dBeginDate := EncodeDate(cYear, 7, 1);

      'a':
         dBeginDate := EncodeDate(cYear, 1, 1);
    else
      dBeginDate := CheckDate;
    end;

    DM_PAYROLL.PR.First;
    while not DM_PAYROLL.PR.Eof do
    begin
      if (DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime >= dBeginDate) and
         (DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime <= CheckDate) and
         (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> 'D')  then
        PrList.Add(Pointer(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
      DM_PAYROLL.PR.Next;
    end;
  end;


  function CheckFreqToPrint: Boolean;
  var
    cYear, cMonth, cDay: Word;
    p: Char;
  begin
    Result := False;
    PrList.Clear;

    if RunPar.Frequency = '' then
      Exit;

    if not fl_scheduled_payroll then
    begin
      Result := RunPar.Frequency[1] = SCHED_FREQ_DAILY;
      Exit;
    end;

    DecodeDate(CheckDate, cYear, cMonth, cDay);
    p := ' ';

    case RunPar.Frequency[1] of
      SCHED_FREQ_DAILY,
      SCHED_FREQ_SCHEDULED_PAY:
        Result := True;

      SCHED_FREQ_SCHEDULED_MONTHLY:
        if CheckWeekNbr then
        begin
          Result := True;
          p := 'm';
        end;

      SCHED_FREQ_QUARTERLY:
        if CheckWeekNbr and ((cMonth - GetMonth(GetBeginQuarter(CheckDate)) + 1) = RunPar.MonthNumber) then
        begin
          Result := True;
          p := 'q';
        end;

      SCHED_FREQ_SEMI_ANNUALLY:
      begin
        if cMonth > 6 then
          cMonth := cMonth - 6;
        if CheckWeekNbr and (cMonth = RunPar.MonthNumber) then
        begin
          Result := True;
          p := 's';
        end;
      end;

      SCHED_FREQ_ANNUALLY:
        if CheckWeekNbr and (cMonth = RunPar.MonthNumber)  then
        begin
          Result := True;
          p := 'a';
        end;

      SCHED_FREQ_RUN_ONCE:
        if RunPar.Copies > 0 then
        begin
          Result := True;
        end;
    else
      Exit;
    end;

    if RunPar.Summarized and (p <> ' ') then
      PayrollsToInclude(p);
  end;

begin
  PrList := TList.Create;
  RepList := TRepInfoList.Create(TRepInfo);
  RunPar := TevPrRepPrnSettings.Create(nil);

  DM_PAYROLL.PR.DisableControls;
  DM_COMPANY.CO_REPORTS.DisableControls;
  DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
  DM_PAYROLL.PR_REPORTS.DisableControls;
  sOldIndex := DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames;

  SaveDSStates([DM_PAYROLL.PR]);
  try
    ctx_DataAccess.OpenClient(ClientNbr);

    DM_PAYROLL.PR.CheckDSConditionAndClient(ClientNbr, '');
    ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR]);
    if not DM_PAYROLL.PR.Locate('pr_nbr', PrNbr, []) then
      Exit;
    CheckDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime;
    fl_scheduled_payroll := DM_PAYROLL.PR.FieldByName('SCHEDULED').AsString = 'Y';

    fl_print_not_all_reports := (DM_PAYROLL.PR.FieldByName('PRINT_ALL_REPORTS').AsString = 'N');

    IsAdjustmentPayroll := (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = 'T') or
                            (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = 'A');

    if fl_print_not_all_reports then
    begin
      DM_PAYROLL.PR_REPORTS.CheckDSConditionAndClient(ClientNbr, 'pr_nbr = ' + IntToStr(PrNbr));
      ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR_REPORTS]);
    end;

    DM_PAYROLL.PR.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    DM_COMPANY.CO_REPORTS.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    DM_PAYROLL.PR_SCHEDULED_EVENT.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_REPORTS, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR]);
    DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := 'SCHEDULED_CHECK_DATE';

    with DM_COMPANY.CO_REPORTS do
    begin
      First;

      while not Eof do
      begin
        if fl_print_not_all_reports and
           not DM_PAYROLL.PR_REPORTS.Locate('CO_REPORTS_NBR',
                 DM_COMPANY.CO_REPORTS.FieldByName('CO_REPORTS_NBR').ASInteger, []) then
        begin
          Next;
          Continue;
        end;

        if DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS').IsNull then
        begin
          RunPar.Clear;
          RunPar.Copies := 0;
        end
        else
          RunPar.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));

        if ((IsAdjustmentPayroll and RunPar.PrnWithAdjPayrolls) or not (IsAdjustmentPayroll or RunPar.PrnWithAdjPayrolls))
          and (RunPar.Copies > 0) and CheckFreqToPrint then
        begin
          R := RepList.AddReport;
          R.NBR := DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger;

          R.Level := DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString;
          if R.Level = CH_DATABASE_CLIENT then
            R.Level := R.Level + IntToStr(ClientID);

          R.Priority := RunPar.Priority;
          R.OverrideVmrMbGroupNbr := DM_COMPANY.CO_REPORTS.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
          R.InputParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('INPUT_PARAMS')));
          R.InputParams.Add('Clients', varArrayOf([ClientNbr]));
          R.InputParams.Add('Companies', varArrayOf([DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger]));
          R.InputParams.Add('ClientCompany', varArrayOf([IntToStr(ClientID) + ':' + DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsString]));
          R.InputParams.Add('Co_Reports_Nbr', DM_COMPANY.CO_REPORTS.CO_REPORTS_NBR.AsInteger);

          if PrList.Count = 0 then
            R.InputParams.Add('Payrolls', varArrayOf([PrNbr]))
          else
          begin
            Arr := VarArrayCreate([0, PrList.Count - 1], varVariant);
            for i := 0 to PrList.Count - 1 do
              Arr[i] := Integer(PrList[i]);
            R.InputParams.Add('Payrolls', Arr);
          end;

          R.Caption := DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString;
          R.InputParams.Add('ReportName', DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString +
            ' (' + DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString +
            DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');
          R.InputParams.Copies := RunPar.Copies;

          if RunPar.DisplayOnDashboard then
            R.InputParams.Add(__DASHBOARD, True);
        end;

        Next;
      end;
    end;

    RepList.Destination := ADestination;
    RepList.SortByPriority;
    RepList.Run;
    DM_COMPANY.CO_REPORTS.First;

    while not DM_COMPANY.CO_REPORTS.Eof do
    begin
      RunPar.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));
      if (RunPar.Frequency = SCHED_FREQ_RUN_ONCE) and (RunPar.Copies <> 0) then
      begin
        RunPar.Copies := 0;
        DM_COMPANY.CO_REPORTS.Edit;
        RunPar.SaveToBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));
        DM_COMPANY.CO_REPORTS.Post;
      end;
      DM_COMPANY.CO_REPORTS.Next;
    end;

    try
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_REPORTS]);
    except
      ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_REPORTS]);
    end;

  finally
    DM_COMPANY.CO_REPORTS.CheckDSCondition('');
    DM_PAYROLL.PR_SCHEDULED_EVENT.CheckDSCondition('');
    DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := sOldIndex;

    DM_PAYROLL.PR.EnableControls;
    DM_COMPANY.CO_REPORTS.EnableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
    DM_PAYROLL.PR_REPORTS.EnableControls;

    PrList.Free;
    RunPar.Free;
    RepList.Free;
    LoadDSStates([DM_PAYROLL.PR]);
  end;
end;

function PayrollsReports(const CoNbr, ClientNbr: Integer; PrList: variant): IisStream;
var
  RunPar: TevPrRepPrnSettings;
  RepList: TRepInfoList;
  R: TRepInfo;
  sOldIndex: string;

begin
  RepList := TRepInfoList.Create(TRepInfo);
  RunPar := TevPrRepPrnSettings.Create(nil);

  DM_PAYROLL.PR.DisableControls;
  DM_COMPANY.CO_REPORTS.DisableControls;
  DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
  DM_PAYROLL.PR_REPORTS.DisableControls;
  sOldIndex := DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames;

  SaveDSStates([DM_PAYROLL.PR]);
  try
    ctx_DataAccess.OpenClient(ClientNbr);

    DM_PAYROLL.PR.CheckDSConditionAndClient(ClientNbr, '');
    ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR]);

    DM_PAYROLL.PR.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    DM_COMPANY.CO_REPORTS.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    DM_PAYROLL.PR_SCHEDULED_EVENT.CheckDSConditionAndClient(ClientNbr, 'co_nbr = ' + IntToStr(CoNbr));
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_REPORTS, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR]);
    DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := 'SCHEDULED_CHECK_DATE';

    with DM_COMPANY.CO_REPORTS do
    begin
      First;

      while not Eof do
      begin

        if DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS').IsNull then
        begin
          RunPar.Clear;
          RunPar.Copies := 0;
        end
        else
          RunPar.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));

        if RunPar.PrnWithAdjPayrolls {and (RunPar.Copies > 0)} then
        begin
          R := RepList.AddReport;
          R.NBR := DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger;

          R.Level := DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString;
          if R.Level = CH_DATABASE_CLIENT then
            R.Level := R.Level + IntToStr(ClientID);

          R.Priority := RunPar.Priority;
          R.OverrideVmrMbGroupNbr := DM_COMPANY.CO_REPORTS.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
          R.InputParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('INPUT_PARAMS')));
          R.InputParams.Add('Clients', varArrayOf([ClientNbr]));
          R.InputParams.Add('Companies', varArrayOf([DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger]));
          R.InputParams.Add('ClientCompany', varArrayOf([IntToStr(ClientID) + ':' + DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsString]));
          R.InputParams.Add('Co_Reports_Nbr', DM_COMPANY.CO_REPORTS.CO_REPORTS_NBR.AsInteger);          
          R.InputParams.Add('Payrolls', PrList);

          R.Caption := DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString;
          R.InputParams.Add('ReportName', DM_COMPANY.CO_REPORTS.FieldByName('CUSTOM_NAME').AsString +
            ' (' + DM_COMPANY.CO_REPORTS.FieldByName('REPORT_LEVEL').AsString +
            DM_COMPANY.CO_REPORTS.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');
          R.InputParams.Copies := RunPar.Copies;

          if RunPar.DisplayOnDashboard then
            R.InputParams.Add(__DASHBOARD, True);
        end;

        Next;
      end;
    end;

    RepList.SortByPriority;
    Result := RepList.GetResult;
  finally
    DM_COMPANY.CO_REPORTS.CheckDSCondition('');
    DM_PAYROLL.PR_SCHEDULED_EVENT.CheckDSCondition('');
    DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := sOldIndex;

    DM_PAYROLL.PR.EnableControls;
    DM_COMPANY.CO_REPORTS.EnableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
    DM_PAYROLL.PR_REPORTS.EnableControls;

    RunPar.Free;
    RepList.Free;
    LoadDSStates([DM_PAYROLL.PR]);
  end;
end;


{ TevPrRepPrnSettings }

procedure TevPrRepPrnSettings.Clear;
begin
  FFrequency := '';
  FPriority := 0;
  FMonthNumber := 0;
  FWeekOfMonth := '';
  FCopies := 1;
  FSummarized := False;
  FHideForRemotes := False;
  FPrnWithAdjPayrolls := False;
  FDisplayOnDashboard := False;
end;

constructor TevPrRepPrnSettings.Create(AOwner: TComponent);
begin
  inherited;
  Clear;
end;

procedure TevPrRepPrnSettings.ReadFromBlobField(AField: TBlobField);
var
  MS: TMemoryStream;
begin
  Clear;
  MS := TIsMemoryStream.Create;
  try
    AField.SaveToStream(MS);
    MS.Position := 0;
    MS.ReadComponent(Self);
  finally
    MS.Free;
  end;
end;


procedure TevPrRepPrnSettings.SaveToBlobField(AField: TBlobField);
var
  MS: TMemoryStream;
begin
  MS := TIsMemoryStream.Create;
  try
    MS.WriteComponent(Self);
    MS.Position := 0;
    AField.LoadFromStream(MS);
  finally
    MS.Free;
  end;
end;


function TevPrRepPrnSettings.WeekOfMonthIsNotEmpty: Boolean;
begin
  Result := Length(FWeekOfMonth) > 0;
end;



{ TRepInfoList }

function TRepInfoList.AddReport: TRepInfo;
begin
  Result := TRepInfo(Add);
end;

function TRepInfoList.GetItem(Index: Integer): TRepInfo;
begin
  Result := TRepInfo(inherited Items[Index]);
end;

procedure TRepInfoList.Run;
var
  i: Integer;
begin
  ctx_RWLocalEngine.StartGroup;
  try
    for i := 0 to Count -1 do
      ctx_RWLocalEngine.CalcPrintReport(Items[i].NBR, Items[i].Level, Items[i].InputParams, Items[i].Caption).
        OverrideVmrMbGroupNbr := Items[i].OverrideVmrMbGroupNbr;
  finally
    ctx_RWLocalEngine.EndGroupForTask(Destination);
  end;
end;

function TRepInfoList.GetResult: IisStream;
var
  i: Integer;
begin
  ctx_RWLocalEngine.StartGroup;
  try
    for i := 0 to Count -1 do
      ctx_RWLocalEngine.CalcPrintReport(Items[i].NBR, Items[i].Level, Items[i].InputParams, Items[i].Caption).
        OverrideVmrMbGroupNbr := Items[i].OverrideVmrMbGroupNbr;
  finally
    Result := ctx_RWLocalEngine.EndGroup(rdtNone);
  end;
end;

procedure TRepInfoList.SortByPriority;
var
  C1, C2: TRepInfo;
  i, j: Integer;
begin
  for i := 0 to Count - 2 do
    for j := i + 1 to Count - 1 do
    begin
      C1 := Items[i];
      C2 := Items[j];
      if Items[i].Priority > Items[j].Priority then
      begin
        C1.Index := j;
        C2.Index := i;
      end;
    end;
end;


{ TRepInfo }

constructor TRepInfo.Create(Collection: TCollection);
begin
  inherited;
  FInputParams := TrwReportParams.Create;
end;

destructor TRepInfo.Destroy;
begin
  FInputParams.Free;
  inherited;
end;


end.
