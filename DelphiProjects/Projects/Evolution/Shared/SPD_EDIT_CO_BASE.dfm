inherited EDIT_CO_BASE: TEDIT_CO_BASE
  Width = 903
  Height = 592
  inherited Panel1: TevPanel
    Width = 903
    Height = 54
    inherited pnlFavoriteReport: TevPanel
      Left = 751
      Width = 152
      Height = 54
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      inherited spbFavoriteReports: TevSpeedButton
        Left = 51
        Top = 16
      end
      inherited btnSwipeClock: TevSpeedButton
        Left = 82
        Top = 16
      end
      inherited BtnMRCLogIn: TevSpeedButton
        Left = 113
        Top = 16
      end
      object lblAdditionalTools: TevLabel
        Left = 2
        Top = 2
        Width = 148
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object pnlTopLeft: TevPanel
      Left = 0
      Top = 0
      Width = 751
      Height = 54
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object lablClient: TevLabel
        Left = 8
        Top = 8
        Width = 38
        Height = 13
        Caption = 'CLIENT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 8
        Top = 24
        Width = 53
        Height = 13
        Caption = 'COMPANY'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtxClientNbr: TevDBText
        Left = 66
        Top = 7
        Width = 111
        Height = 15
        Color = 4276545
        DataField = 'CUSTOM_CLIENT_NUMBER'
        DataSource = dsCL
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object dbtxClientName: TevDBText
        Left = 178
        Top = 7
        Width = 241
        Height = 15
        Color = 4276545
        DataField = 'NAME'
        DataSource = dsCL
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object DBText1: TevDBText
        Left = 66
        Top = 23
        Width = 111
        Height = 17
        Color = 4276545
        DataField = 'CUSTOM_COMPANY_NUMBER'
        DataSource = wwdsMaster
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object CompanyNameText: TevDBText
        Left = 178
        Top = 23
        Width = 241
        Height = 17
        Color = 4276545
        DataField = 'NAME'
        DataSource = wwdsMaster
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object pnlUser: TevPanel
        Left = 432
        Top = 9
        Width = 193
        Height = 33
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        Visible = False
        object lablUserFirst: TevLabel
          Left = 0
          Top = 1
          Width = 19
          Height = 13
          Caption = 'First'
        end
        object dbtxUserFirst: TevDBText
          Left = 24
          Top = 1
          Width = 161
          Height = 17
          DataField = 'UserFirstName'
          DataSource = wwdsList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtxUserLast: TevDBText
          Left = 24
          Top = 17
          Width = 161
          Height = 17
          DataField = 'UserLastName'
          DataSource = wwdsList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablUserLast: TevLabel
          Left = 0
          Top = 17
          Width = 20
          Height = 13
          Caption = 'Last'
        end
      end
    end
  end
  inherited PageControl1: TevPageControl
    Top = 54
    Width = 903
    Height = 538
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 895
        Height = 509
        inherited pnlBorder: TevPanel
          Width = 891
          Height = 505
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 891
          Height = 505
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 0
              Width = 800
              BevelOuter = bvNone
              object sbEDIT_CO_BASE_Inner: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splEDIT_CO_BASE: TevSplitter
                    Left = 790
                    Top = 6
                    Width = 0
                    Height = 549
                    Align = alRight
                    Visible = False
                  end
                  object pnlEDIT_CO_BASE_LEFT: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                  object pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 790
                    Top = 6
                    Width = 0
                    Height = 549
                    Align = alRight
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    Visible = False
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 579
              Top = 219
              Visible = False
            end
          end
          inherited Panel3: TevPanel
            Width = 843
            inherited BitBtn1: TevBitBtn
              Left = 2
              Width = 121
              Caption = 'Open company'
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 843
            Height = 398
            inherited Splitter1: TevSplitter
              Left = 0
              Height = 394
              Visible = False
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 3
              Width = 300
              Height = 394
              Align = alClient
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 250
                Height = 314
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 303
              Width = 536
              Height = 394
              Align = alRight
              Visible = False
            end
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_CO.CO
    Left = 152
    Top = 42
  end
  inherited wwdsDetail: TevDataSource
    Left = 206
    Top = 34
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Top = 34
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 50
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 260
    Top = 42
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 366
    Top = 42
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 408
    Top = 45
  end
end
