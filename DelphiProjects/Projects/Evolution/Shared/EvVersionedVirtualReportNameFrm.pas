unit EvVersionedVirtualReportNameFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem;

type
  TevVersionedVirtualReportName = class(TevVersionedFieldBase)
    lValue: TevLabel;
    cbSystemReportsGroup: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedPersonName }

procedure TevVersionedVirtualReportName.FormShow(Sender: TObject);
begin
  inherited;
  DM_SYSTEM_MISC.SY_REPORTS_GROUP.DataRequired('SY_REPORTS_GROUP_NBR > 20000');
end;

end.

