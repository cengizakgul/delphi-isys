// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TINY_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,  SFrameEntry, Grids, Wwdbigrd, Wwdbgrid, Db,
  Wwdatsrc, ISBasicClasses, EvUIComponents;

type
  TEDIT_TINY_BASE = class(TFrameEntry)
    pnlBrowse: TevPanel;
    pnlDBControls: TevPanel;
    dgBrowse: TevDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

end.
