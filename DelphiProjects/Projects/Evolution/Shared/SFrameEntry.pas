// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SFrameEntry;

interface

uses
  Forms, Messages, Controls, Classes,  Db,
  SDataStructure, SSecurityInterface, Wwdatsrc, Graphics, TypInfo, EvConsts,
  stdctrls, Dialogs, SFieldCodeValues, ComCtrls, ISBasicClasses, EvContext,
  EvCommonInterfaces, EvUIUtils, EvUIComponents, EvClientDataSet, SDDClasses,
  isMessenger, EvMainboard, isBaseClasses;

type
  TSecurityStateList = class
  private
  public
    States: array of record
      Tag: Char;
      Caption: string;
      IconName: string;
    end;
    constructor Create;
    function IndexOfTag(const Tag: Char): Integer;
    procedure InsertAfter(const AfterTag, NewTag: Char; const NewCaption: ShortString; const NewIconName: ShortString = '');
    procedure Append(const NewTag: Char; const NewCaption: ShortString; const NewIconName: ShortString = '');
  end;

  TFrameEntry = class(TFrame, ISecurityAtom, ISecurityElement, IisMessageRecipient)
    wwdsMaster: TevDataSource;
    wwdsDetail: TevDataSource;
    wwdsList: TevDataSource;
  private
    FSecurityState: ShortString;
    FDestroying: Boolean;
    procedure WMActivateParams(var Message: TMessage); message WM_ACTIVATE_FRAME_PARAMS;
    procedure SetHelpForControls(const Control: TWinControl; Keyword: string);
    // security
    function Component: TComponent;
    function IsAtom: Boolean; virtual;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetCaption: ShortString;
    function SGetPossibleSecurityContexts: TAtomContextRecordArray;
    function Atom: ISecurityAtom;
    function SGetStates: TElementStatesArray;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray;
    procedure SSetCurrentSecurityState(const State: ShortString);
    function GetSecurityStateList: TSecurityStateList; virtual;
    //

    // isMessenger
    procedure Notify(const AMessage: String; const AParams: IisListOfValues);
    function  Destroying: Boolean;
  protected
    // security
    procedure OnGetSecurityStates(const States: TSecurityStateList); virtual;
    procedure OnSetSecurityState(const State: Char); virtual;
    function SecurityState: ShortString;
    //
    function GetDefaultDataSet: TevClientDataSet; virtual;
    procedure CheckDefaultDataSet(var CheckCondition: string; var bCheck: Boolean); virtual;
    procedure SetControlsReccuring(const C: TWinControl; const PropertyName: string; const NewValue: Integer); virtual;
    function GetIfReadOnly: Boolean; virtual;
    function IsModified: Boolean; virtual;
    function GetDataSetConditions(sName: string): string; virtual;
    procedure ClearReadOnly; virtual;
    procedure SetReadOnly(Value: Boolean); virtual;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); virtual;
    procedure ApplySecurity;
    procedure DoOnBroadcastTableChange(const ATable: String; const ANbr: Integer; const AChangeType: TevTableChangeType); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override; 
    function GetInsertControl: TWinControl; virtual;
    function CanOpen: Boolean; virtual;
    function CanClose: Boolean; virtual;
    procedure OnGlobalFlagChange(const AResourceInfo: IevGlobalFlagInfo; const AOperation: Char); virtual;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); virtual;
    procedure RetrieveCustomQueryDataSets(var CustomQueryDataSets: TArrayDS); virtual;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); virtual;
    procedure AfterClick(Kind: Integer); virtual;
    procedure DoBeforePost; virtual;
    procedure DoAfterPost; virtual;
    procedure DoOnCancel; virtual;    
    procedure DoBeforeDelete; virtual;
    procedure DoAfterDelete; virtual;
    procedure DoBeforeCommit; virtual;
    procedure DoAfterCommit(const AError: Boolean); virtual;
    procedure Activate; virtual;
    procedure Deactivate; virtual;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); virtual;
    procedure OnBeforeBuildScheme; virtual;
    function  DeleteMessage: Word; virtual;
  end;

var
  LastPathOpened: string;

implementation

{$R *.DFM}

uses EvSecElement, sysutils, EvSecurityUtils, SPackageEntry, EvUtils;

{ TFrameEntry }

function TFrameEntry.DeleteMessage: Word;
begin
  Result := mrNone;
  if EvMessage('Are you sure?', mtConfirmation, mbOKCancel) = mrOK then
    Result := mrOK;
end;

function TFrameEntry.CanClose: Boolean;
begin
  Result := True;
  with Owner as TFramePackageTmpl do
    if btnNavOk.Enabled or btnNavCancel.Enabled then
    begin
      EvErrMessage('You must post or cancel changes.');
      Result := False;
    end;
end;

function TFrameEntry.GetInsertControl: TWinControl;
begin
  Result := nil;
end;

procedure TFrameEntry.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
var
  DS: TevClientDataSet;
  CheckCondition: string;
  bCheck: Boolean;
begin
  DS := GetDefaultDataSet;
  if Assigned(DS) then
  begin
    NavigationDataSet := DS;
    AddDS(DS, InsertDataSets);
    AddDS(DS, EditDataSets);
    DeleteDataSet := DS;
  end;
  if not Assigned(wwdsDetail.DataSet) then
    wwdsDetail.DataSet := DS;

  CheckDefaultDataSet(CheckCondition, bCheck);
  if Assigned(DS) and bCheck then
    DS.CheckDSCondition(CheckCondition);
end;

procedure TFrameEntry.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  Handled := False;
end;

procedure TFrameEntry.Activate;
begin
  ctx_DataAccess.ClearCachedData;  // clear cached flags  like CL.READ_ONLY etc.

  ApplySecurity;
  HelpType := htKeyword;
  HelpKeyword := StringReplace(LastPathOpened, '-', '', [rfReplaceAll]);
  HelpKeyword := StringReplace(HelpKeyword, ' ', '', [rfReplaceAll]);
  SetHelpForControls(Self, HelpKeyword);
end;

procedure TFrameEntry.Deactivate;
var
  i: Integer;
  l: TList;
begin
  l := TList.Create;
  try
    for i := 0 to ComponentCount-1 do
      if Components[i] is TevClientDataSet then
        if TevClientDataSet(Components[i]).AttachedTo <> nil then
          l.Add(Components[i]);
    for i := 0 to l.Count-1 do
      TevClientDataSet(l[i]).Close;
  finally
    l.Free;
  end;
end;

procedure TFrameEntry.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  if Kind in [NavInsert, NavDelete] then
    Value := Value and not GetIfReadOnly;
end;

function TFrameEntry.GetDefaultDataSet: TevClientDataSet;
begin
  Result := nil;
end;

constructor TFrameEntry.Create(AOwner: TComponent);
var
  i: Integer;
begin
  inherited;
  mb_Messenger.Subscribe(Self, [MSG_DB_TABLE_CHANGE], True);

  for i := 0 to Pred(ControlCount) do
    if (Controls[i] is TevPageControl) and
       (Controls[i].Parent = Self) then
    begin
      TevPageControl(Controls[i]).ActivePageIndex := 0;
      Break;
    end;
end;

function TFrameEntry.SGetCaption: ShortString;
var
  i: Integer;
begin
  Result := '-N/A-';
  for i := Low(Structure) to High(Structure) do
    if AnsiSameText(Structure[i].PClass, ClassName) then
    begin
      Result := Structure[i].Path;
      Result := Copy(Result, 2, Length(Result));
      if Pos('\', Result) > 0 then
        Result := Copy(Result, Succ(Pos('\', Result)), Length(Result))
      else
        Result := '';
      Break;
    end;
end;

function TFrameEntry.SGetPossibleSecurityContexts: TAtomContextRecordArray;
var
  i: Integer;
  l: TSecurityStateList;
begin
  l := GetSecurityStateList;
  try
    SetLength(Result, Length(l.States));
    for i := 0 to High(l.States) do
      with l.States[i] do
      begin
        Result[i].Tag := Tag;
        Result[i].Caption := Caption;
        Result[i].IconName := IconName;
        Result[i].SelectedIconName := '';
        Result[i].Description := 'Screen';
        Result[i].Priority := High(l.States)- i;
      end;
  finally
    l.Free;
  end;
end;

function TFrameEntry.SGetTag: ShortString;
begin
  Result := ClassName;
end;

function TFrameEntry.SGetType: ShortString;
begin
  Result := atScreen;
end;

function TFrameEntry.IsAtom: Boolean;
begin
  Result := True;
end;

function TFrameEntry.Component: TComponent;
begin
  Result := Self;
end;

procedure TFrameEntry.CheckDefaultDataSet(var CheckCondition: string;
  var bCheck: Boolean);
begin
  bCheck := True;
  if GetDefaultDataSet <> nil then
    CheckCondition := GetDataSetConditions(GetDefaultDataSet.Name);
end;

procedure TFrameEntry.SetControlsReccuring(const C: TWinControl; const PropertyName: string; const NewValue: Integer);
var
  i: Integer;                                                        
begin
  for i := 0 to Pred(C.ControlCount) do
  begin
    if IsPublishedProp(C.Controls[i], PropertyName) then
      SetOrdProp(C.Controls[i], PropertyName, NewValue);
    if C.Controls[i] is TWinControl then
      SetControlsReccuring(TWinControl(C.Controls[i]), PropertyName, NewValue);
  end;
end;

procedure TFrameEntry.AfterClick(Kind: Integer);
begin
end;

function TFrameEntry.IsModified: Boolean;
begin
  Result := (Owner as TFramePackageTmpl).IsModified;
end;

function TFrameEntry.GetDataSetConditions(sName: string): string;
begin
  Result := '';
end;

function TFrameEntry.CanOpen: Boolean;
begin
  Result := True;
end;

function TFrameEntry.GetIfReadOnly: Boolean;
var
  c: ISecurityElement;
begin
  Result := ctx_DataAccess.AsOfDate <> SysDate;

  if not Result then
  begin
    c := FindSecurityElement(Self, ClassName);
    if Assigned(c) then
      Result := c.SecurityState = stReadOnly;
  end;
end;

procedure TFrameEntry.SetReadOnly(Value: Boolean);
begin
  if Value then
    SetControlsReccuring(Self, 'SecurityRO', Integer(True));
end;

procedure TFrameEntry.OnBeforeBuildScheme;
  procedure CreateSecElementForDataset(const d: TevClientDataSet; const t: TevElementType);
  var
    s: TComponent;
  begin
    if Assigned(d) then
    begin
      s := Self;
      if not Assigned(FindSecurityElement(s, d.TableName)) then
        with TevSecElement.Create(S) do
        begin
          ElementTag := d.TableName;
          ElementType := t;
          AtomComponent := s;
        end;
    end;
  end;
  procedure CreateSecElementForDatasetArray(const d: TArrayDS; const t: TevElementType);
  var
    i: Integer;
  begin
    for i := Low(d) to High(d) do
      CreateSecElementForDataset(d[i], t);
  end;
var
  nds, dds: TevClientDataSet;
  ids, eds, sds: TArrayDS;
begin
  nds := nil;
  SetLength(ids, 0);
  SetLength(eds, 0);
  dds := nil;
  SetLength(sds, 0);
  RetrieveDataSets(nds, ids, eds, dds, sds);
  CreateSecElementForDataset(nds, otDataset);
  CreateSecElementForDataset(dds, otDataset);
  CreateSecElementForDatasetArray(ids, otDataset);
  CreateSecElementForDatasetArray(eds, otDataset);
  CreateSecElementForDatasetArray(sds, otDataSet);
end;

procedure TFrameEntry.RetrieveCustomQueryDataSets(
  var CustomQueryDataSets: TArrayDS);
begin
  ; // to be overriden
end;


procedure TFrameEntry.WMActivateParams(var Message: TMessage);
begin
  if Message.WParam <> 0 then
    OnActivateParams(PTFrameActivateParamsRec(Message.WParam)^.Context, PTFrameActivateParamsRec(Message.WParam)^.Params);
end;


procedure TFrameEntry.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
end;

procedure TFrameEntry.SetHelpForControls(const Control: TWinControl; Keyword: string);
var
  i: Integer;
  s: string;
begin
  for i := 0 to Pred(Control.ControlCount) do
    if Control.Controls[i] is TWinControl then
    begin
      s := Keyword;
      if Control.Controls[i] is TTabSheet then
        s := s + StringReplace(StringReplace(StringReplace(StringReplace(TTabSheet(Control.Controls[i]).Caption, '&', '', [rfReplaceAll]), ' ', '', [rfReplaceAll]), '-', '', [rfReplaceAll]), ',', '', [rfReplaceAll]);
      Control.Controls[i].HelpType := htKeyword;
      Control.Controls[i].HelpKeyword := s;
      SetHelpForControls(TWinControl(Control.Controls[i]), s);
    end;
end;

function TFrameEntry.GetSecurityStateList: TSecurityStateList;
var
  nds, dds: TevClientDataSet;
  ids, eds, sds: TArrayDS;
begin
  Result := TSecurityStateList.Create;
  with secScreenOn do
    Result.Append(Tag[1], Caption, IconName);
  nds := nil;
  SetLength(ids, 0);
  SetLength(eds, 0);
  dds := nil;
  SetLength(sds, 0);
  RetrieveDataSets(nds, ids, eds, dds, sds);
  if Assigned(nds)
  or Assigned(dds)
  or (Length(ids) > 0)
  or (Length(eds) > 0) then
    with secScreenRO do
      Result.Append(Tag[1], Caption, IconName);
  with secScreenOff do
    Result.Append(Tag[1], Caption, IconName);
  OnGetSecurityStates(Result);
end;

function TFrameEntry.Atom: ISecurityAtom;
begin
  Result := Self;
end;

function TFrameEntry.SecurityState: ShortString;
begin
  Result := FSecurityState;
end;

function TFrameEntry.SGetAtomElementRelationsArray: TAtomElementRelationsArray;
var
  i: Integer;
  l: TSecurityStateList;
begin
  l := GetSecurityStateList;
  try
    SetLength(Result, Length(l.States));
    for i := 0 to High(l.States) do
      with l.States[i] do
      begin
        Result[i].AtomContext := Tag;
        Result[i].ElementState := Tag;
        Result[i].Atom2Element := ltEqual;
        Result[i].Element2Atom := ltEqual;
      end;
  finally
    l.Free;
  end;
end;

function TFrameEntry.SGetStates: TElementStatesArray;
var
  i: Integer;
  l: TSecurityStateList;
begin
  l := GetSecurityStateList;
  try
    SetLength(Result, Length(l.States));
    for i := 0 to High(l.States) do
      with l.States[i] do
      begin
        Result[i].Tag := Tag;
        Result[i].Priority := High(l.States)- i;
      end;
  finally
    l.Free;
  end;
end;

procedure TFrameEntry.SSetCurrentSecurityState(const State: ShortString);
begin
  if State <> '' then
  begin
    FSecurityState := State;
    OnSetSecurityState(State[1]);
  end;
end;

procedure TFrameEntry.OnSetSecurityState(const State: Char);
begin
end;

procedure TFrameEntry.OnGetSecurityStates(
  const States: TSecurityStateList);
begin
end;


procedure TFrameEntry.OnGlobalFlagChange(const AResourceInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
end;

procedure TFrameEntry.ApplySecurity;
begin
  ClearReadOnly;
  ctx_EvolutionGUI.ApplySecurity(Self);
  SetReadOnly(GetIfReadOnly);
end;

procedure TFrameEntry.ClearReadOnly;
begin
  SetControlsReccuring(Self, 'SecurityRO', Integer(False));
end;

procedure TFrameEntry.DoAfterCommit(const AError: Boolean);
begin
end;

procedure TFrameEntry.DoBeforeCommit;
begin
end;

procedure TFrameEntry.DoBeforeDelete;
begin
end;

procedure TFrameEntry.DoAfterDelete;
begin
end;

procedure TFrameEntry.DoAfterPost;
begin
end;

procedure TFrameEntry.DoBeforePost;
begin
end;

procedure TFrameEntry.DoOnCancel;
begin
end;

function TFrameEntry.Destroying: Boolean;
begin
  Result := FDestroying; 
end;

procedure TFrameEntry.Notify(const AMessage: String; const AParams: IisListOfValues);
begin
  if AMessage = MSG_DB_TABLE_CHANGE then
    DoOnBroadcastTableChange(AParams.Value['Table'], AParams.Value['Nbr'], TevTableChangeType(AParams.Value['ChangeType']));
end;

destructor TFrameEntry.Destroy;
begin
  FDestroying := True;
  mb_Messenger.Unsubscribe(Self, []);
  
  inherited;
end;

procedure TFrameEntry.DoOnBroadcastTableChange(const ATable: String; const ANbr: Integer; const AChangeType: TevTableChangeType);
begin
end;

{ TSecurityStateList }

procedure TSecurityStateList.Append(const NewTag: Char; const NewCaption,
  NewIconName: ShortString);
begin
  SetLength(States, Length(States)+1);
  with States[High(States)] do
  begin
    Tag := NewTag;
    Caption := NewCaption;
    IconName := NewIconName;
  end;
end;

constructor TSecurityStateList.Create;
begin
  SetLength(States, 0);
end;

function TSecurityStateList.IndexOfTag(const Tag: Char): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to High(States) do
    if States[i].Tag = Tag then
    begin
      Result := i;
      Break;
    end;
end;

procedure TSecurityStateList.InsertAfter(const AfterTag, NewTag: Char;
  const NewCaption, NewIconName: ShortString);
var
  i, j: Integer;
begin
  i := IndexOfTag(AfterTag);
  Assert(i <> -1, 'Can not find state to insert after');
  SetLength(States, Length(States)+1);
  for j := High(States)-1 downto i+1 do
    States[j+1] := States[j];
  with States[i+1] do
  begin
    Tag := NewTag;
    Caption := NewCaption;
    IconName := NewIconName;
  end;
end;

end.
