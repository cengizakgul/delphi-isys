inherited EDIT_TINY_BASE: TEDIT_TINY_BASE
  object pnlBrowse: TevPanel [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 233
    Align = alTop
    BevelOuter = bvNone
    Caption = 'pnlBrowse'
    TabOrder = 0
    object dgBrowse: TevDBGrid
      Left = 0
      Top = 0
      Width = 443
      Height = 233
      DisableThemesInTitle = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
      IniAttributes.SectionName = 'TEDIT_TINY_BASE\dgBrowse'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = wwdsDetail
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object pnlDBControls: TevPanel [1]
    Left = 0
    Top = 233
    Width = 443
    Height = 44
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
  end
end
