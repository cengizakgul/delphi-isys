// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EEIFuncs;

interface

uses
  IEMap, EvUtils, SysUtils, SFieldCodeValues,  EEImport, Variants, Classes,
  EvTypes, DateUtils, ISUtils, ISErrorUtils, StrUtils, EEHRTraxDef, EvContext,
  EvConsts, EvExceptions, EvUIUtils;

function AddValue(const AIEMap: IIEMap; const CoID, EmpID, TableName, FieldName, KeyValue, OldValue, NewValue: string; EffectiveDate: TDateTime): Boolean;
procedure PostChanges(const AIEMap: IIEMap);
procedure ImportFromFile(FileName: string);
procedure LogError(ErrorFile, ErrorMessage: string);

implementation

uses
  EvCommonInterfaces;
  
var
  UpdateRecord: TUpdateRecordArray;

function AddValue(const AIEMap: IIEMap; const CoID, EmpID, TableName, FieldName, KeyValue, OldValue, NewValue: string; EffectiveDate: TDateTime): Boolean;
var
  OTables, OFields: TStringArray;
  OValues: TVariantArray;
  KeyTables, KeyFields: TStringArray;
  KeyValues: TVariantArray;
  i, j: Integer;
  v: Variant;
  UsedMapRecord: IEvImportMapRecord;
begin
  if NewValue = '' then
    v := null
  else if (FieldName = 'DIVISION') or
          (FieldName = 'BRANCH') or
          (FieldName = 'DEPT.') then
    v := PadStringLeft(NewValue, ' ', 20)
  else if (FieldName = 'STATE') or
          (FieldName = 'STATE MARITAL') or
          (FieldName = 'STATE RMS') or
          (FieldName = 'PAY STATUS') or
          (FieldName = 'TAX FORM') or
          (FieldName = 'SHIFT DIFF') or
          (FieldName = 'ED ACTIVE WEEK') then
    v := Trim(NewValue)
  else
    v := CapitalizeWords(Trim(NewValue));
  Result := False;
  if (OldValue = '') and
     (NewValue = '') then
    Exit;
  if (FieldName = 'DIVISION') or (KeyValue = '') then
    i := 2
  else
    i := 3;
  SetLength(KeyTables, i);
  SetLength(KeyFields, i);
  SetLength(KeyValues, i);
  KeyTables[0] := 'CO';
  KeyTables[1] := 'EE';
  KeyFields[0] := 'CUSTOM_COMPANY_NUMBER';
  KeyFields[1] := 'CUSTOM_EMPLOYEE_NUMBER';
  KeyValues[0] := CoID;
  KeyValues[1] := EmpID;
  if (FieldName <> 'DIVISION') and (KeyValue <> '') then
  begin
    if SameText(LeftStr(TableName, 6), 'CL_HR_') then
    begin
      SetLength(KeyTables, 4);
      SetLength(KeyFields, 4);
      SetLength(KeyValues, 4);
      AIEMap.CurrentDataRequired(AIEMap.IDS('EE'), 'CUSTOM_EMPLOYEE_NUMBER=''' + EmpID + '''');
      KeyTables[2] := 'CL_PERSON';
      KeyFields[2] := 'CL_PERSON_NBR';
      KeyValues[2] := AIEMap.IDS('EE').FieldByName('CL_PERSON_NBR').AsInteger;
    end;
    KeyTables[High(KeyTables)] := '';
    KeyFields[High(KeyFields)] := '';
    KeyValues[High(KeyValues)] := KeyValue;
  end;
  if FieldName = 'SHIFT DIFF' then
  begin
    AIEMap.CurrentDataRequired(AIEMap.IDS('CO'), 'CUSTOM_COMPANY_NUMBER=''' + CoID + '''');
    AIEMap.CurrentDataRequired(AIEMap.IDS('CO_SHIFTS'), 'CO_NBR=' + AIEMap.IDS('CO').FieldByName('CO_NBR').AsString + ' and NAME=''' + Trim(OldValue) + '''');
    AIEMap.CurrentDataRequired(AIEMap.IDS('EE'), 'CUSTOM_EMPLOYEE_NUMBER=''' + EmpID + '''');
    AIEMap.CurrentDataRequired(AIEMap.IDS('EE_WORK_SHIFTS'), 'EE_NBR=' + IntToStr(AIEMap.IDS('EE').FieldByName('EE_NBR').AsInteger) + ' and CO_SHIFTS_NBR=' + IntToStr(AIEMap.IDS('CO_SHIFTS').FieldByName('CO_SHIFTS_NBR').AsInteger));
    if AIEMap.IDS('EE_WORK_SHIFTS').RecordCount = 0 then
      AIEMap.CurrentDataRequired(AIEMap.IDS('CO_SHIFTS'), 'CO_NBR=' + AIEMap.IDS('CO').FieldByName('CO_NBR').AsString + ' and NAME=''' + Trim(NewValue) + '''');
    if AIEMap.IDS('CO_SHIFTS').RecordCount > 0 then
    begin
      SetLength(KeyTables, Succ(Length(KeyTables)));
      SetLength(KeyFields, Succ(Length(KeyFields)));
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyTables[High(KeyTables)] := 'EE_WORK_SHIFTS';
      KeyFields[High(KeyFields)] := 'CO_SHIFTS_NBR';
      KeyValues[High(KeyValues)] := AIEMap.IDS('CO_SHIFTS')['CO_SHIFTS_NBR'];
    end;
  end;
  if AIEMap.MapValue(True, TableName, FieldName, v, KeyTables, KeyFields, KeyValues, OTables, OFields, OValues, UsedMapRecord) then
  begin
    for i := Low(OTables) to High(OTables) do
    begin
      SetLength(UpdateRecord, Succ(Length(UpdateRecord)));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyTables, Length(KeyTables));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyFields, Length(KeyFields));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyValues, Length(KeyValues));
      for j := 0 to High(KeyTables) do
      begin
        UpdateRecord[High(UpdateRecord)].KeyTables[j] := KeyTables[j];
        UpdateRecord[High(UpdateRecord)].KeyFields[j] := KeyFields[j];
        UpdateRecord[High(UpdateRecord)].KeyValues[j] := KeyValues[j];
      end;
      UpdateRecord[High(UpdateRecord)].Table := OTables[i];
      UpdateRecord[High(UpdateRecord)].Field := OFields[i];
      UpdateRecord[High(UpdateRecord)].Value := OValues[i];
      UpdateRecord[High(UpdateRecord)].EffectiveDate := EffectiveDate;
    end;
    Result := True;
  end;
end;

procedure PostChanges(const AIEMap: IIEMap);
begin
  try
    AIEMap.PostUpdates(UpdateRecord);
  finally
    SetLength(UpdateRecord, 0);
  end;
end;

procedure LogError(ErrorFile, ErrorMessage: string);
var
  F: Text;
begin
  AssignFile(F, ErrorFile);
  try
    if FileExists(ErrorFile) then
      Append(F)
    else
      Rewrite(F);
    WriteLn(F, '***************************** ' + DateTimeToStr(Now) + ' ******************************');
    WriteLn(F, ErrorMessage);
  finally
    CloseFile(F);
  end;
end;

procedure ImportFromFile(FileName: string);
const
  ID_COMPANY = 0;
  ID_EMPID = 1;
  ID_AUD_FILE = 2;
  ID_AUD_CODE = 3;
  ID_AUD_FIELD = 4;
  ID_OLD_VALUE = 5;
  ID_NEW_VALUE = 6;
  ID_AUD_USERID = 7;
  ID_AUD_DLC = 8;
  ID_AUD_TIME = 9;
  ID_TAG_CODE = 10;
  ID_TAG_USER = 11;
  ID_TAG_DATE = 12;
  ID_TAG_TIME = 13;
var
  tFile, tLine: TStringList;
  i: Integer;
  d: TDateTime;
  empId: string;
  td: IIEDefinition;
  table: string;
  ImpEngine: IIEMap;
begin
  ImpEngine := TIEMap.Create;

  d := SysTime;
  tFile := TStringList.Create;
  tLine := TStringList.Create;
  td := THRTraxDefinition.Create(ImpEngine);
  try
    ImpEngine.StartMapping(HRTraxMap, td, False);
    tFile.LoadFromFile(FileName);
    ImpEngine.CurrentDataRequired(ImpEngine.IDS('TMP_CO'));
    ctx_StartWait('Importing...', Pred(tFile.Count));
    try
      for i := 0 to Pred(tFile.Count) do
        if Trim(tFile[i]) <> '' then
        try
          tLine.CommaText := tFile[i];
          if i = 0 then
            empId := tLine[ID_EMPID];
          if (ImpEngine.IDS('TMP_CO')['CUSTOM_COMPANY_NUMBER'] <> tLine[ID_COMPANY]) or
             (ctx_DataAccess.ClientID <> ImpEngine.IDS('TMP_CO')['CL_NBR']) then
            if ImpEngine.IDS('TMP_CO').Locate('CUSTOM_COMPANY_NUMBER', tLine[ID_COMPANY], []) then
            begin
              ctx_DataAccess.OpenClient(ImpEngine.IDS('TMP_CO')['CL_NBR']);
              ImpEngine.CurrentDataRequired(ImpEngine.IDS('CO'), 'CO_NBR = ''' + IntToStr(ImpEngine.IDS('TMP_CO').FieldByName('CO_NBR').AsInteger) + '''');
            end
            else
              raise EInconsistentData.CreateHelp('No such company ' + tLine[ID_COMPANY], IDH_InconsistentData);

          if empId <> tLine[ID_EMPID] then
          begin
            empId := tLine[ID_EMPID];
            ctx_DataAccess.StartNestedTransaction([dbtClient]);
            try
              PostChanges(ImpEngine);
              ctx_DataAccess.CommitNestedTransaction;
            except
              on E: Exception do
              begin
                LogError(ExtractFileDir(FileName) + '\errors.log', AdjustLineBreaks(E.Message));
                ctx_DataAccess.RollbackNestedTransaction;
              end;
            end;
          end;
  //  WF specific import
          table := tLine[ID_AUD_FILE];
          if not SameText(LeftStr(table, 6), 'EE_HR_') and
             not SameText(LeftStr(table, 6), 'CL_HR_') and
             not SameText(table, 'EE_EMERGENCY_CONTACTS') then
            table := '';
          AddValue(ImpEngine, tLine[ID_COMPANY], PadStringLeft(tLine[ID_EMPID], ' ', 9),
                   table, tLine[ID_AUD_FIELD],
                   tLine[ID_AUD_CODE], tLine[ID_OLD_VALUE],
                   tLine[ID_NEW_VALUE], d);
          ctx_UpdateWait('Importing...', i);
        except
          on E: Exception do
            LogError(ExtractFileDir(FileName) + '\errors.log', AdjustLineBreaks(E.Message));
        end;
      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        PostChanges(ImpEngine);
        ctx_DataAccess.CommitNestedTransaction;
      except
        on E: Exception do
        begin
          LogError(ExtractFileDir(FileName) + '\errors.log', AdjustLineBreaks(E.Message));
          ctx_DataAccess.RollbackNestedTransaction;
        end;
      end;
    finally
      ctx_EndWait;
    end;
    EvMessage('Done!');
  finally
    ImpEngine.EndMapping;
    tFile.Free;
    tLine.Free;
  end;
end;

end.

