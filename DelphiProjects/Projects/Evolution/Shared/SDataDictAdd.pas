unit SDataDictAdd;

interface

uses
  Classes, SDDClasses, DB, SDataDictClient;

type
  TPR_CHECK_LINES_DISTRIBUTED = class(TddCLTable)
  public
    class function ReadOnlyTable: Boolean; override;
    class function GetClassIndex: Integer; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetParameters: TddParamList; override;
  published
    property PR_CHECK_NBR: TIntegerField index 1 read GetIntegerField;
    property PERC: TFloatField index 2 read GetFloatField;
    property CO_DIVISION_NBR: TIntegerField index 3 read GetIntegerField;
    property CO_BRANCH_NBR: TIntegerField index 4 read GetIntegerField;
    property CO_DEPARTMENT_NBR: TIntegerField index 5 read GetIntegerField;
    property CO_TEAM_NBR: TIntegerField index 6 read GetIntegerField;
  end;
  
procedure Register;

implementation

uses
  SDataStructure;

class function TPR_CHECK_LINES_DISTRIBUTED.GetClassIndex: Integer;
begin
  Result := 1001;
end;

class function TPR_CHECK_LINES_DISTRIBUTED.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_CLIENT;
  Result[High(Result)].Table := TPR_CHECK;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'PR_CHECK_NBR';
  Result[High(Result)].DestFields[0] := 'PR_CHECK_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_CLIENT;
  Result[High(Result)].Table := TCO_DIVISION;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CO_DIVISION_NBR';
  Result[High(Result)].DestFields[0] := 'CO_DIVISION_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_CLIENT;
  Result[High(Result)].Table := TCO_BRANCH;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CO_BRANCH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_BRANCH_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_CLIENT;
  Result[High(Result)].Table := TCO_DEPARTMENT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CO_DEPARTMENT_NBR';
  Result[High(Result)].DestFields[0] := 'CO_DEPARTMENT_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_CLIENT;
  Result[High(Result)].Table := TCO_TEAM;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CO_TEAM_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TEAM_NBR';
end;

class function TPR_CHECK_LINES_DISTRIBUTED.GetParameters: TddParamList;
begin
  SetLength(Result, 3);
  Result[0].Name := 'DTYPE';
  Result[0].ParamType := ftString;
  Result[0].ParamSize := 1;
  Result[1].Name := 'PR';
  Result[1].ParamType := ftString;
  Result[1].ParamSize := 8000;
  Result[2].Name := 'CHANGE_DATE';
  Result[2].ParamType := ftDateTime;
end;

procedure Register;
begin
  RegisterComponents('EvoDdClient', [
    TPR_CHECK_LINES_DISTRIBUTED]);
end;

class function TPR_CHECK_LINES_DISTRIBUTED.ReadOnlyTable: Boolean;
begin
  Result := True;
end;

initialization
  RegisterClasses([TPR_CHECK_LINES_DISTRIBUTED]);

finalization
  UnregisterClasses([TPR_CHECK_LINES_DISTRIBUTED]);

end.
