unit EvVersionedE_D_CODE_TYPEFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem, SDataDictclient, isVCLBugFix, EvMainboard;

type
  TevVersionedE_D_CODE_TYPE = class(TevVersionedFieldBase)
    lValue: TevLabel;
    DM_CLIENT: TDM_CLIENT;
    wwcdE_D_Code_Type: TevDBComboDlg;
    procedure wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnNavOkClick(Sender: TObject);
    procedure btnNavDeleteClick(Sender: TObject);
   Private
    FBroadcastParams: IisListOfValues;
    function DefaultValueOnSpecial(AValue: String; ADefault: String): Variant;
    function ToState(const s: string): string;
    procedure SetProperFedValues(const AEndDate: TDateTime=0);
    procedure SetProperStateValues(const AEndDate: TDateTime=0);
    procedure SetProperLocalValues(const AEndDate: TDateTime=0);
   protected
    procedure DoOnSave; override;
    procedure DoAfterSave; override;
  end;

implementation

{$R *.dfm}

Uses SPD_CategorizedComboDialog, SFieldCodeValues;


{ TevVersionedCLCOCons }

procedure TevVersionedE_D_CODE_TYPE.wwcdE_D_Code_TypeCustomDlg(
  Sender: TObject);
var
  i: Integer;
begin
  for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      if (FieldsToPopulate[i].F = 'E_D_CODE_TYPE') and
         (FieldsToPopulate[i].T =  DM_CLIENT.CL_E_DS.ClassType) then
  begin
    with TCategorizedComboDialog.Create(self) do
    begin
      Setup(FieldsToPopulate[i].c, FieldsToPopulate[i].g, Text);
      if ShowModal = mrOk then
        wwcdE_D_Code_Type.Text := SelectedCode;
        dsFieldData.DataSet.UpdateRecord;
        SyncControlsState;
      Free;
    end;
  end;
end;

procedure TevVersionedE_D_CODE_TYPE.FormShow(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  DM_SYSTEM.SY_FED_EXEMPTIONS.DataRequired('ALL');
end;

procedure TevVersionedE_D_CODE_TYPE.DoOnSave;
begin
  inherited;

  FBroadcastParams := nil;
  if not DM_SYSTEM.SY_FED_EXEMPTIONS.Active then
    Exit;

  SetProperFedValues;
  SetProperStateValues;
  SetProperLocalValues;

  FBroadcastParams := TisListOfValues.Create;
  FBroadcastParams.AddValue('Table', 'CL_E_DS');
  FBroadcastParams.AddValue('Nbr', Nbr);
  FBroadcastParams.AddValue('ChangeType', tcUpdate);
end;

function TevVersionedE_D_CODE_TYPE.DefaultValueOnSpecial(AValue: String; ADefault: String): Variant;
begin
     //  Default Federal to Block and Everything else to Include;
    if AValue = '-' then
      Result := ADefault
    else
      Result := AValue;
end;

function TevVersionedE_D_CODE_TYPE.ToState(const s: string): string;
var
  i: Integer;
begin
  Result := s;
  for i := 1 to Length(Result) do
    if Result[i] = GROUP_BOX_EXEMPT then
      Result[i] := GROUP_BOX_EXE
    else
      if Result[i] = GROUP_BOX_EXCLUDE then
        Result[i] := GROUP_BOX_EXC;
end;


procedure TevVersionedE_D_CODE_TYPE.SetProperFedValues(const AEndDate: TDateTime=0);
var
  Found: Boolean;
  Code: String;
  LV: IisListOfValues;
  endDate: TDateTime;
begin

  code :=  wwcdE_D_Code_Type.Text;
  if not DM_SYSTEM.SY_FED_EXEMPTIONS.Active then
     Exit;

  endDate:= AEndDate;
  if endDate = 0 then
     endDate:=Data['end_date'];

  Found := DM_SYSTEM.SY_FED_EXEMPTIONS.Locate('E_D_CODE_TYPE', Code, []);

  LV := TisListOfValues.Create;
  LV.AddValue('DESCRIPTION', ReturnDescription(ED_ComboBoxChoices, code));
  LV.AddValue('EE_EXEMPT_EXCLUDE_FEDERAL', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_FEDERAL']), GROUP_BOX_EXCLUDE));
  LV.AddValue('EE_EXEMPT_EXCLUDE_OASDI', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_OASDI']), GROUP_BOX_INCLUDE));
  LV.AddValue('EE_EXEMPT_EXCLUDE_MEDICARE', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_MEDICARE']), GROUP_BOX_INCLUDE));
  LV.AddValue('EE_EXEMPT_EXCLUDE_EIC', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_EIC']), GROUP_BOX_INCLUDE));
  LV.AddValue('ER_EXEMPT_EXCLUDE_OASDI', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_OASDI']), GROUP_BOX_INCLUDE));
  LV.AddValue('ER_EXEMPT_EXCLUDE_MEDICARE', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_MEDICARE']), GROUP_BOX_INCLUDE));
  LV.AddValue('ER_EXEMPT_EXCLUDE_FUI', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS['EXEMPT_FUI']), GROUP_BOX_INCLUDE));
  ctx_DBAccess.UpdateFieldVersion('CL_E_DS', Nbr, EmptyDate, EmptyDate, Data['begin_date'], endDate, LV);

end;

procedure TevVersionedE_D_CODE_TYPE.SetProperStateValues(const AEndDate: TDateTime=0);
var
  Found: Boolean;
  Code: String;
  LV: IisListOfValues;
  Q: IevQuery;
  endDate: TDateTime;
begin

  endDate:= AEndDate;
  if endDate = 0 then
     endDate:=Data['end_date'];


  Q := TevQuery.Create('SELECT t1.CL_E_D_STATE_EXMPT_EXCLD_NBR, t1.CL_E_DS_NBR, t1.SY_STATES_NBR ' +
                       ' FROM CL_E_D_STATE_EXMPT_EXCLD t1 WHERE {AsOfDate<t1>} AND t1.CL_E_DS_NBR = :nbr');
  Q.Params.AddValue('nbr', Nbr);
  Q.Params.AddValue('StatusDate', Data['begin_date']);
  Q.Execute;

  code :=  wwcdE_D_Code_Type.Text;

  if Q.Result.RecordCount > 0 then
  begin
    DM_SYSTEM.SY_STATE_EXEMPTIONS.DataRequired('ALL');
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      Found := DM_SYSTEM.SY_STATE_EXEMPTIONS.Locate('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([Q.Result['SY_STATES_NBR'], Code]), []);

      LV := TisListOfValues.Create;
      LV.AddValue('EMPLOYEE_EXEMPT_EXCLUDE_STATE', DefaultValueOnSpecial(ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS['EXEMPT_STATE'])), GROUP_BOX_INCLUDE));
      LV.AddValue('EMPLOYEE_EXEMPT_EXCLUDE_SDI',   DefaultValueOnSpecial(ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SDI'])), GROUP_BOX_INCLUDE));
      LV.AddValue('EMPLOYEE_EXEMPT_EXCLUDE_SUI',   DefaultValueOnSpecial(ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SUI'])), GROUP_BOX_INCLUDE));
      LV.AddValue('EMPLOYER_EXEMPT_EXCLUDE_SDI',   DefaultValueOnSpecial(ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SDI'])), GROUP_BOX_INCLUDE));
      LV.AddValue('EMPLOYER_EXEMPT_EXCLUDE_SUI',   DefaultValueOnSpecial(ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SUI'])), GROUP_BOX_INCLUDE));
      ctx_DBAccess.UpdateFieldVersion('CL_E_D_STATE_EXMPT_EXCLD', Q.Result.FieldValues['CL_E_D_STATE_EXMPT_EXCLD_NBR'], EmptyDate, EmptyDate, Data['begin_date'], endDate, LV);

      Q.Result.Next;
    end;
  end;

end;

procedure TevVersionedE_D_CODE_TYPE.SetProperLocalValues(const AEndDate: TDateTime=0);
var
  Found: Boolean;
  Code: String;
  LV: IisListOfValues;
  Q: IevQuery;
  endDate: TDateTime;  
begin
  endDate:= AEndDate;
  if endDate = 0 then
     endDate:=Data['end_date'];

  Q := TevQuery.Create('SELECT t1.CL_E_D_LOCAL_EXMPT_EXCLD_NBR, t1.CL_E_DS_NBR, t1.SY_LOCALS_NBR ' +
                       ' FROM CL_E_D_LOCAL_EXMPT_EXCLD t1 WHERE {AsOfDate<t1>} AND t1.CL_E_DS_NBR = :nbr');
  Q.Params.AddValue('nbr', Nbr);
  Q.Params.AddValue('StatusDate', Data['begin_date']);
  Q.Execute;

  code :=  wwcdE_D_Code_Type.Text;

  if Q.Result.RecordCount > 0 then
  begin
    DM_SYSTEM.SY_LOCAL_EXEMPTIONS.DataRequired('ALL');
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      Found := DM_SYSTEM.SY_LOCAL_EXEMPTIONS.Locate('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([Q.Result['SY_LOCALS_NBR'], Code]), []);
      LV := TisListOfValues.Create;
      LV.AddValue('EXEMPT_EXCLUDE', DefaultValueOnSpecial(DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_LOCAL_EXEMPTIONS['EXEMPT']), GROUP_BOX_INCLUDE));
      ctx_DBAccess.UpdateFieldVersion('CL_E_D_LOCAL_EXMPT_EXCLD', Q.Result.FieldValues['CL_E_D_LOCAL_EXMPT_EXCLD_NBR'], EmptyDate, EmptyDate, Data['begin_date'], endDate, LV);
      Q.Result.Next;
    end;
  end;
end;

procedure TevVersionedE_D_CODE_TYPE.DoAfterSave;
begin
  inherited;
   if Assigned(FBroadcastParams) then
  begin
    mb_Messenger.Broadcast(MSG_DB_TABLE_CHANGE, FBroadcastParams);
    FBroadcastParams := nil;
  end;
end;

procedure TevVersionedE_D_CODE_TYPE.btnNavOkClick(Sender: TObject);
var
  mr: TModalResult;
begin
    mr := EvMessage('The change will update the taxation for this E/D. Continue?', mtConfirmation, [mbYes, mbNo, mbCancel]);
    if mr <> mrYes then
    begin
      if mr = mrCancel then
      begin
        Data.Cancel;
        ActiveControl := grFieldData;
      end;
      exit;
    end;
 inherited;

end;

procedure TevVersionedE_D_CODE_TYPE.btnNavDeleteClick(Sender: TObject);
var
  EndDate: TDate;
begin
  Data.vclDataSet.DisableControls;
  try
    EndDate := Data['end_date'];
  finally
    Data.vclDataSet.EnableControls;
  end;

  inherited;

  FBroadcastParams := nil;
  if not DM_SYSTEM.SY_FED_EXEMPTIONS.Active then
    Exit;

  SetProperFedValues(EndDate);
  SetProperStateValues(EndDate);
  SetProperLocalValues(EndDate);

  FBroadcastParams := TisListOfValues.Create;
  FBroadcastParams.AddValue('Table', 'CL_E_DS');
  FBroadcastParams.AddValue('Nbr', Nbr);
  FBroadcastParams.AddValue('ChangeType', tcUpdate);
  DoAfterSave;
end;

end.

