inherited evVersionedE_D_CODE_TYPE: TevVersionedE_D_CODE_TYPE
  Left = 468
  Top = 317
  ClientHeight = 366
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 366
    inherited grFieldData: TevDBGrid
      Height = 239
    end
    inherited pnlEdit: TevPanel
      Top = 283
      object lValue: TevLabel [2]
        Left = 243
        Top = 11
        Width = 52
        Height = 13
        Caption = 'Code Type'
      end
      inherited pnlButtons: TevPanel
        Top = 19
      end
      object wwcdE_D_Code_Type: TevDBComboDlg
        Left = 243
        Top = 26
        Width = 110
        Height = 21
        HelpContext = 8124
        OnCustomDlg = wwcdE_D_Code_TypeCustomDlg
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DX,D!,D#,D8,D5,D&,D@,DH,DG,DA,DB,DC,DD,DE,DF,DL,DM,DW,DK,D7,' +
          'DV,ET,EU,EV,EW,EX,EO,EP,EQ,M1,M2,M3,M4,M5,M6,ER,ES,E4,E3,E5,E6,E' +
          '1,E8,E7,D1,DN,DQ,DP,DR,DS,DO,DT,DU,DZ,EF,EG,EH,EI,EJ,EK,EL,EM,EB' +
          ',EN,EA,EC,ED,EE,E!,E#,E\]'
        Picture.AutoFill = False
        TabOrder = 3
        WordWrap = False
        UnboundDataType = wwDefault
        OnChange = UpdateFieldOnChange
      end
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 376
    Top = 64
  end
end
