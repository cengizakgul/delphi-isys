inherited dlAsOfDateDBLC: TdlAsOfDateDBLC
  Left = 353
  Top = 266
  ClientHeight = 200
  ClientWidth = 267
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel [0]
    Left = 146
    Top = 47
    Width = 27
    Height = 13
    Caption = 'Value'
  end
  inherited OKBtn: TButton
    TabOrder = 3
  end
  inherited CancelBtn: TButton
    TabOrder = 4
  end
  object evDBLookupCombo1: TevDBLookupCombo
    Left = 146
    Top = 64
    Width = 111
    Height = 21
    DropDownAlignment = taLeftJustify
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
  end
end
