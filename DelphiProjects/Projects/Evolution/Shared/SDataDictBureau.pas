// Copyright � 2000-2012 iSystems LLC. All rights reserved.
// This is automatically generated file
// Do not modify it!

unit SDataDictbureau;

interface

uses
  SDDClasses, db, classes, EvTypes;

type
  TSB = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_NAME: TStringField index 5 read GetStringField;
    property ADDRESS1: TStringField index 6 read GetStringField;
    property ADDRESS2: TStringField index 7 read GetStringField;
    property CITY: TStringField index 8 read GetStringField;
    property STATE: TStringField index 9 read GetStringField;
    property ZIP_CODE: TStringField index 10 read GetStringField;
    property E_MAIL_ADDRESS: TStringField index 11 read GetStringField;
    property PARENT_SB_MODEM_NUMBER: TStringField index 12 read GetStringField;
    property DEVELOPMENT_MODEM_NUMBER: TStringField index 13 read GetStringField;
    property DEVELOPMENT_FTP_PASSWORD: TStringField index 14 read GetStringField;
    property EIN_NUMBER: TStringField index 15 read GetStringField;
    property EFTPS_TIN_NUMBER: TStringField index 16 read GetStringField;
    property EFTPS_BANK_FORMAT: TStringField index 17 read GetStringField;
    property USE_PRENOTE: TStringField index 18 read GetStringField;
    property IMPOUND_TRUST_MONIES_AS_RECEIV: TStringField index 19 read GetStringField;
    property PAY_TAX_FROM_PAYABLES: TStringField index 20 read GetStringField;
    property AR_EXPORT_FORMAT: TStringField index 21 read GetStringField;
    property DEFAULT_CHECK_FORMAT: TStringField index 22 read GetStringField;
    property MICR_FONT: TStringField index 23 read GetStringField;
    property MICR_HORIZONTAL_ADJUSTMENT: TIntegerField index 24 read GetIntegerField;
    property AUTO_SAVE_MINUTES: TIntegerField index 25 read GetIntegerField;
    property PHONE: TStringField index 26 read GetStringField;
    property FAX: TStringField index 27 read GetStringField;
    property COVER_LETTER_NOTES: TBlobField index 28 read GetBlobField;
    property INVOICE_NOTES: TBlobField index 29 read GetBlobField;
    property TAX_COVER_LETTER_NOTES: TBlobField index 30 read GetBlobField;
    property AR_IMPORT_DIRECTORY: TStringField index 31 read GetStringField;
    property ACH_DIRECTORY: TStringField index 32 read GetStringField;
    property SB_URL: TStringField index 33 read GetStringField;
    property DAYS_IN_PRENOTE: TIntegerField index 34 read GetIntegerField;
    property SB_LOGO: TBlobField index 35 read GetBlobField;
    property USER_PASSWORD_DURATION_IN_DAYS: TFloatField index 36 read GetFloatField;
    property DUMMY_TAX_CL_NBR: TIntegerField index 37 read GetIntegerField;
    property ERROR_SCREEN: TStringField index 38 read GetStringField;
    property PSWD_MIN_LENGTH: TIntegerField index 39 read GetIntegerField;
    property PSWD_FORCE_MIXED: TStringField index 40 read GetStringField;
    property MISC_CHECK_FORM: TStringField index 41 read GetStringField;
    property VMR_CONFIDENCIAL_NOTES: TBlobField index 42 read GetBlobField;
    property MARK_LIABS_PAID_DEFAULT: TStringField index 43 read GetStringField;
    property TRUST_IMPOUND: TStringField index 44 read GetStringField;
    property TAX_IMPOUND: TStringField index 45 read GetStringField;
    property DD_IMPOUND: TStringField index 46 read GetStringField;
    property BILLING_IMPOUND: TStringField index 47 read GetStringField;
    property WC_IMPOUND: TStringField index 48 read GetStringField;
    property DAYS_PRIOR_TO_CHECK_DATE: TIntegerField index 49 read GetIntegerField;
    property SB_EXCEPTION_PAYMENT_TYPE: TStringField index 50 read GetStringField;
    property SB_MAX_ACH_FILE_TOTAL: TFloatField index 51 read GetFloatField;
    property SB_ACH_FILE_LIMITATIONS: TStringField index 52 read GetStringField;
    property SB_CL_NBR: TIntegerField index 53 read GetIntegerField;
    property DASHBOARD_MSG: TStringField index 54 read GetStringField;
    property EE_LOGIN_TYPE: TStringField index 55 read GetStringField;
    property ESS_TERMS_OF_USE: TBlobField index 56 read GetBlobField;
    property WC_TERMS_OF_USE: TBlobField index 57 read GetBlobField;
    property ESS_TERMS_OF_USE_MODIFY_DATE: TDateField index 58 read GetDateField;
    property ESS_LOGO: TBlobField index 59 read GetBlobField;
    property THEME: TStringField index 60 read GetStringField;
    property THEME_VALUE: TBlobField index 61 read GetBlobField;
    property WC_TERMS_OF_USE_MODIFY_DATE: TDateTimeField index 62 read GetDateTimeField;
    property ANALYTICS_LICENSE: TStringField index 63 read GetStringField;
    property SESSION_LOCKOUT: TIntegerField index 64 read GetIntegerField;
    property SESSION_TIMEOUT: TIntegerField index 65 read GetIntegerField;
    property ENFORCE_EE_DOB_DEFAULT: TStringField index 66 read GetStringField;
  end;

  TSB_ACCOUNTANT = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ACCOUNTANT_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property ADDRESS1: TStringField index 6 read GetStringField;
    property ADDRESS2: TStringField index 7 read GetStringField;
    property CITY: TStringField index 8 read GetStringField;
    property STATE: TStringField index 9 read GetStringField;
    property ZIP_CODE: TStringField index 10 read GetStringField;
    property CONTACT1: TStringField index 11 read GetStringField;
    property PHONE1: TStringField index 12 read GetStringField;
    property DESCRIPTION1: TStringField index 13 read GetStringField;
    property CONTACT2: TStringField index 14 read GetStringField;
    property PHONE2: TStringField index 15 read GetStringField;
    property DESCRIPTION2: TStringField index 16 read GetStringField;
    property FAX: TStringField index 17 read GetStringField;
    property FAX_DESCRIPTION: TStringField index 18 read GetStringField;
    property E_MAIL_ADDRESS: TStringField index 19 read GetStringField;
    property PRINT_NAME: TStringField index 20 read GetStringField;
    property TITLE: TStringField index 21 read GetStringField;
    property SIGNATURE: TBlobField index 22 read GetBlobField;
    property CREDIT_BANK_ACCOUNT_NBR: TIntegerField index 23 read GetIntegerField;
    property DEBIT_BANK_ACCOUNT_NBR: TIntegerField index 24 read GetIntegerField;
    property ACCOUNT_NUMBER: TStringField index 25 read GetStringField;
    property BANK_ACCOUNT_TYPE: TStringField index 26 read GetStringField;
    property SB_BANKS_NBR: TIntegerField index 27 read GetIntegerField;
  end;

  TSB_AGENCY = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_AGENCY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property ADDRESS1: TStringField index 6 read GetStringField;
    property ADDRESS2: TStringField index 7 read GetStringField;
    property CITY: TStringField index 8 read GetStringField;
    property STATE: TStringField index 9 read GetStringField;
    property ZIP_CODE: TStringField index 10 read GetStringField;
    property CONTACT1: TStringField index 11 read GetStringField;
    property PHONE1: TStringField index 12 read GetStringField;
    property DESCRIPTION1: TStringField index 13 read GetStringField;
    property CONTACT2: TStringField index 14 read GetStringField;
    property PHONE2: TStringField index 15 read GetStringField;
    property DESCRIPTION2: TStringField index 16 read GetStringField;
    property FAX: TStringField index 17 read GetStringField;
    property FAX_DESCRIPTION: TStringField index 18 read GetStringField;
    property E_MAIL_ADDRESS: TStringField index 19 read GetStringField;
    property AGENCY_TYPE: TStringField index 20 read GetStringField;
    property SB_BANKS_NBR: TIntegerField index 21 read GetIntegerField;
    property ACCOUNT_NUMBER: TStringField index 22 read GetStringField;
    property ACCOUNT_TYPE: TStringField index 23 read GetStringField;
    property NEGATIVE_DIRECT_DEP_ALLOWED: TStringField index 24 read GetStringField;
    property MODEM_NUMBER: TStringField index 25 read GetStringField;
    property NOTES: TBlobField index 26 read GetBlobField;
    property FILLER: TStringField index 27 read GetStringField;
    property PRINT_NAME: TStringField index 28 read GetStringField;
    property COUNTY: TStringField index 29 read GetStringField;
  end;

  TSB_AGENCY_REPORTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_AGENCY_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_REPORTS_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSB_BANKS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_BANKS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property ADDRESS1: TStringField index 6 read GetStringField;
    property ADDRESS2: TStringField index 7 read GetStringField;
    property CITY: TStringField index 8 read GetStringField;
    property STATE: TStringField index 9 read GetStringField;
    property ZIP_CODE: TStringField index 10 read GetStringField;
    property CONTACT1: TStringField index 11 read GetStringField;
    property PHONE1: TStringField index 12 read GetStringField;
    property DESCRIPTION1: TStringField index 13 read GetStringField;
    property CONTACT2: TStringField index 14 read GetStringField;
    property PHONE2: TStringField index 15 read GetStringField;
    property DESCRIPTION2: TStringField index 16 read GetStringField;
    property FAX: TStringField index 17 read GetStringField;
    property FAX_DESCRIPTION: TStringField index 18 read GetStringField;
    property E_MAIL_ADDRESS: TStringField index 19 read GetStringField;
    property ABA_NUMBER: TStringField index 20 read GetStringField;
    property TOP_ABA_NUMBER: TStringField index 21 read GetStringField;
    property BOTTOM_ABA_NUMBER: TStringField index 22 read GetStringField;
    property ADDENDA: TStringField index 23 read GetStringField;
    property CHECK_TEMPLATE: TBlobField index 24 read GetBlobField;
    property USE_CHECK_TEMPLATE: TStringField index 25 read GetStringField;
    property MICR_ACCOUNT_START_POSITION: TIntegerField index 26 read GetIntegerField;
    property MICR_CHECK_NUMBER_START_POSITN: TIntegerField index 27 read GetIntegerField;
    property FILLER: TStringField index 28 read GetStringField;
    property PRINT_NAME: TStringField index 29 read GetStringField;
    property BRANCH_IDENTIFIER: TStringField index 30 read GetStringField;
    property ALLOW_HYPHENS: TStringField index 31 read GetStringField;
    property ACH_DATE: TDateField index 32 read GetDateField;
    property ACH_NUMBER: TStringField index 33 read GetStringField;
  end;

  TSB_BANK_ACCOUNTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_BANK_ACCOUNTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_BANKS_NBR: TIntegerField index 5 read GetIntegerField;
    property CUSTOM_BANK_ACCOUNT_NUMBER: TStringField index 6 read GetStringField;
    property BANK_ACCOUNT_TYPE: TStringField index 7 read GetStringField;
    property NAME_DESCRIPTION: TStringField index 8 read GetStringField;
    property ACH_ORIGIN_SB_BANKS_NBR: TIntegerField index 9 read GetIntegerField;
    property BANK_RETURNS: TStringField index 10 read GetStringField;
    property CUSTOM_HEADER_RECORD: TStringField index 11 read GetStringField;
    property NEXT_AVAILABLE_CHECK_NUMBER: TIntegerField index 12 read GetIntegerField;
    property END_CHECK_NUMBER: TIntegerField index 13 read GetIntegerField;
    property NEXT_BEGIN_CHECK_NUMBER: TIntegerField index 14 read GetIntegerField;
    property NEXT_END_CHECK_NUMBER: TIntegerField index 15 read GetIntegerField;
    property FILLER: TStringField index 16 read GetStringField;
    property SUPPRESS_OFFSET_ACCOUNT: TStringField index 17 read GetStringField;
    property BLOCK_NEGATIVE_CHECKS: TStringField index 18 read GetStringField;
    property BATCH_FILER_ID: TStringField index 19 read GetStringField;
    property MASTER_INQUIRY_PIN: TStringField index 20 read GetStringField;
    property LOGO_SB_BLOB_NBR: TIntegerField index 21 read GetIntegerField;
    property SIGNATURE_SB_BLOB_NBR: TIntegerField index 22 read GetIntegerField;
    property BEGINNING_BALANCE: TFloatField index 23 read GetFloatField;
    property OPERATING_ACCOUNT: TStringField index 24 read GetStringField;
    property BILLING_ACCOUNT: TStringField index 25 read GetStringField;
    property ACH_ACCOUNT: TStringField index 26 read GetStringField;
    property TRUST_ACCOUNT: TStringField index 27 read GetStringField;
    property TAX_ACCOUNT: TStringField index 28 read GetStringField;
    property OBC_ACCOUNT: TStringField index 29 read GetStringField;
    property WORKERS_COMP_ACCOUNT: TStringField index 30 read GetStringField;
    property RECCURING_WIRE_NUMBER: TIntegerField index 31 read GetIntegerField;
    property BANK_CHECK: TStringField index 32 read GetStringField;
  end;

  TSB_BLOB = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 2 read GetDateField;
    property SB_BLOB_NBR: TIntegerField index 3 read GetIntegerField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DATA: TBlobField index 5 read GetBlobField;
  end;

  TSB_DELIVERY_COMPANY = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DELIVERY_COMPANY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property DELIVERY_CONTACT: TStringField index 6 read GetStringField;
    property DELIVERY_CONTACT_PHONE: TStringField index 7 read GetStringField;
    property DELIVERY_CONTACT_PHONE_TYPE: TStringField index 8 read GetStringField;
    property SUPPLIES_CONTACT: TStringField index 9 read GetStringField;
    property SUPPLIES_CONTACT_PHONE: TStringField index 10 read GetStringField;
    property SUPPLIES_CONTACT_PHONE_TYPE: TStringField index 11 read GetStringField;
    property WEB_SITE: TStringField index 12 read GetStringField;
    property DELIVERY_COMPANY_NOTES: TBlobField index 13 read GetBlobField;
  end;

  TSB_DELIVERY_COMPANY_SVCS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_DELIVERY_COMPANY_NBR: TIntegerField index 5 read GetIntegerField;
    property DESCRIPTION: TStringField index 6 read GetStringField;
    property REFERENCE_FEE: TFloatField index 7 read GetFloatField;
  end;

  TSB_DELIVERY_METHOD = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DELIVERY_METHOD_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_DELIVERY_METHOD_NBR: TIntegerField index 5 read GetIntegerField;
  end;

  TSB_DELIVERY_SERVICE = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DELIVERY_SERVICE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_DELIVERY_SERVICE_NBR: TIntegerField index 5 read GetIntegerField;
  end;

  TSB_DELIVERY_SERVICE_OPT = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DELIVERY_SERVICE_OPT_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_DELIVERY_SERVICE_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_DELIVERY_METHOD_NBR: TIntegerField index 6 read GetIntegerField;
    property OPTION_TAG: TStringField index 7 read GetStringField;
    property OPTION_INTEGER_VALUE: TIntegerField index 8 read GetIntegerField;
    property OPTION_STRING_VALUE: TStringField index 9 read GetStringField;
  end;

  TSB_ENLIST_GROUPS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ENLIST_GROUPS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_REPORT_GROUPS_NBR: TIntegerField index 5 read GetIntegerField;
    property MEDIA_TYPE: TStringField index 6 read GetStringField;
    property PROCESS_TYPE: TStringField index 7 read GetStringField;
  end;

  TSB_GLOBAL_AGENCY_CONTACTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_GLOBAL_AGENCY_CONTACTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
    property CONTACT_NAME: TStringField index 6 read GetStringField;
    property PHONE: TStringField index 7 read GetStringField;
    property FAX: TStringField index 8 read GetStringField;
    property E_MAIL_ADDRESS: TStringField index 9 read GetStringField;
    property NOTES: TBlobField index 10 read GetBlobField;
  end;

  TSB_HOLIDAYS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_HOLIDAYS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property HOLIDAY_DATE: TDateField index 5 read GetDateField;
    property HOLIDAY_NAME: TStringField index 6 read GetStringField;
    property USED_BY: TStringField index 7 read GetStringField;
  end;

  TSB_MAIL_BOX = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MAIL_BOX_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CL_NBR: TIntegerField index 5 read GetIntegerField;
    property CL_MAIL_BOX_GROUP_NBR: TIntegerField index 6 read GetIntegerField;
    property COST: TFloatField index 7 read GetFloatField;
    property REQUIRED: TStringField index 8 read GetStringField;
    property NOTIFICATION_EMAIL: TStringField index 9 read GetStringField;
    property PR_NBR: TIntegerField index 10 read GetIntegerField;
    property RELEASED_TIME: TDateTimeField index 11 read GetDateTimeField;
    property PRINTED_TIME: TDateTimeField index 12 read GetDateTimeField;
    property SB_COVER_LETTER_REPORT_NBR: TIntegerField index 13 read GetIntegerField;
    property SCANNED_TIME: TDateTimeField index 14 read GetDateTimeField;
    property SHIPPED_TIME: TDateTimeField index 15 read GetDateTimeField;
    property SY_COVER_LETTER_REPORT_NBR: TIntegerField index 16 read GetIntegerField;
    property NOTE: TStringField index 17 read GetStringField;
    property UP_LEVEL_MAIL_BOX_NBR: TIntegerField index 18 read GetIntegerField;
    property TRACKING_INFO: TStringField index 19 read GetStringField;
    property AUTO_RELEASE_TYPE: TStringField index 20 read GetStringField;
    property BARCODE: TStringField index 21 read GetStringField;
    property CREATED_TIME: TDateTimeField index 22 read GetDateTimeField;
    property DISPOSE_CONTENT_AFTER_SHIPPING: TStringField index 23 read GetStringField;
    property ADDRESSEE: TStringField index 24 read GetStringField;
    property DESCRIPTION: TStringField index 25 read GetStringField;
    property SB_DELIVERY_METHOD_NBR: TIntegerField index 26 read GetIntegerField;
    property SB_MEDIA_TYPE_NBR: TIntegerField index 27 read GetIntegerField;
  end;

  TSB_MAIL_BOX_CONTENT = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MAIL_BOX_CONTENT_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_MAIL_BOX_NBR: TIntegerField index 5 read GetIntegerField;
    property DESCRIPTION: TStringField index 6 read GetStringField;
    property FILE_NAME: TStringField index 7 read GetStringField;
    property MEDIA_TYPE: TStringField index 8 read GetStringField;
    property PAGE_COUNT: TIntegerField index 9 read GetIntegerField;
    property USERSIDE_PRINTED: TDateTimeField index 10 read GetDateTimeField;
  end;

  TSB_MAIL_BOX_OPTION = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MAIL_BOX_OPTION_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_MAIL_BOX_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_MAIL_BOX_CONTENT_NBR: TIntegerField index 6 read GetIntegerField;
    property OPTION_TAG: TStringField index 7 read GetStringField;
    property OPTION_INTEGER_VALUE: TIntegerField index 8 read GetIntegerField;
    property OPTION_STRING_VALUE: TStringField index 9 read GetStringField;
  end;

  TSB_MEDIA_TYPE = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MEDIA_TYPE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_MEDIA_TYPE_NBR: TIntegerField index 5 read GetIntegerField;
  end;

  TSB_MEDIA_TYPE_OPTION = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MEDIA_TYPE_OPTION_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_MEDIA_TYPE_NBR: TIntegerField index 5 read GetIntegerField;
    property OPTION_TAG: TStringField index 6 read GetStringField;
    property OPTION_INTEGER_VALUE: TIntegerField index 7 read GetIntegerField;
    property OPTION_STRING_VALUE: TStringField index 8 read GetStringField;
  end;

  TSB_MULTICLIENT_REPORTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_MULTICLIENT_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property REPORT_LEVEL: TStringField index 6 read GetStringField;
    property REPORT_NBR: TIntegerField index 7 read GetIntegerField;
    property INPUT_PARAMS: TBlobField index 8 read GetBlobField;
  end;

  TSB_OPTION = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_OPTION_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property OPTION_TAG: TStringField index 5 read GetStringField;
    property OPTION_INTEGER_VALUE: TIntegerField index 6 read GetIntegerField;
    property OPTION_STRING_VALUE: TStringField index 7 read GetStringField;
  end;

  TSB_OTHER_SERVICE = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_OTHER_SERVICE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
  end;

  TSB_PAPER_INFO = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_PAPER_INFO_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property HEIGHT: TFloatField index 6 read GetFloatField;
    property WIDTH: TFloatField index 7 read GetFloatField;
    property WEIGHT: TFloatField index 8 read GetFloatField;
    property MEDIA_TYPE: TStringField index 9 read GetStringField;
  end;

  TSB_QUEUE_PRIORITY = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_QUEUE_PRIORITY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property THREADS: TIntegerField index 5 read GetIntegerField;
    property METHOD_NAME: TStringField index 6 read GetStringField;
    property PRIORITY: TIntegerField index 7 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 8 read GetIntegerField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 9 read GetIntegerField;
  end;

  TSB_REFERRALS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_REFERRALS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
  end;

  TSB_REPORTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property COMMENTS: TBlobField index 6 read GetBlobField;
    property REPORT_WRITER_REPORTS_NBR: TIntegerField index 7 read GetIntegerField;
    property REPORT_LEVEL: TStringField index 8 read GetStringField;
    property INPUT_PARAMS: TBlobField index 9 read GetBlobField;
  end;

  TSB_REPORT_WRITER_REPORTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_REPORT_WRITER_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property REPORT_DESCRIPTION: TStringField index 5 read GetStringField;
    property REPORT_TYPE: TStringField index 6 read GetStringField;
    property REPORT_FILE: TBlobField index 7 read GetBlobField;
    property NOTES: TBlobField index 8 read GetBlobField;
    property MEDIA_TYPE: TStringField index 9 read GetStringField;
    property CLASS_NAME: TStringField index 10 read GetStringField;
    property ANCESTOR_CLASS_NAME: TStringField index 11 read GetStringField;
  end;

  TSB_SALES_TAX_STATES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SALES_TAX_STATES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property STATE: TStringField index 5 read GetStringField;
    property STATE_TAX_ID: TStringField index 6 read GetStringField;
    property SALES_TAX_PERCENTAGE: TFloatField index 7 read GetFloatField;
  end;

  TSB_SEC_CLIENTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_CLIENTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CL_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 6 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 7 read GetIntegerField;
  end;

  TSB_SEC_GROUPS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
  end;

  TSB_SEC_GROUP_MEMBERS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_GROUP_MEMBERS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSB_SEC_RIGHTS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_RIGHTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 6 read GetIntegerField;
    property TAG: TStringField index 7 read GetStringField;
    property CONTEXT: TStringField index 8 read GetStringField;
  end;

  TSB_SEC_ROW_FILTERS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_ROW_FILTERS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_SEC_GROUPS_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 6 read GetIntegerField;
    property DATABASE_TYPE: TStringField index 7 read GetStringField;
    property TABLE_NAME: TStringField index 8 read GetStringField;
    property FILTER_TYPE: TStringField index 9 read GetStringField;
    property CUSTOM_EXPR: TStringField index 10 read GetStringField;
    property CL_NBR: TIntegerField index 11 read GetIntegerField;
  end;

  TSB_SEC_TEMPLATES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SEC_TEMPLATES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property TEMPLATE: TBlobField index 6 read GetBlobField;
  end;

  TSB_SERVICES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SERVICES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SERVICE_NAME: TStringField index 5 read GetStringField;
    property FREQUENCY: TStringField index 6 read GetStringField;
    property MONTH_NUMBER: TStringField index 7 read GetStringField;
    property BASED_ON_TYPE: TStringField index 8 read GetStringField;
    property SB_REPORTS_NBR: TIntegerField index 9 read GetIntegerField;
    property COMMISSION: TStringField index 10 read GetStringField;
    property SALES_TAXABLE: TStringField index 11 read GetStringField;
    property FILLER: TStringField index 12 read GetStringField;
    property PRODUCT_CODE: TStringField index 13 read GetStringField;
    property WEEK_NUMBER: TStringField index 14 read GetStringField;
    property SERVICE_TYPE: TStringField index 15 read GetStringField;
    property MINIMUM_AMOUNT: TFloatField index 16 read GetFloatField;
    property MAXIMUM_AMOUNT: TFloatField index 17 read GetFloatField;
    property SB_DELIVERY_METHOD_NBR: TIntegerField index 18 read GetIntegerField;
    property TAX_TYPE: TStringField index 19 read GetStringField;
    property PARTNER_BILLING: TStringField index 20 read GetStringField;
  end;

  TSB_SERVICES_CALCULATIONS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_SERVICES_CALCULATIONS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_SERVICES_NBR: TIntegerField index 5 read GetIntegerField;
    property NEXT_MIN_QUANTITY_BEGIN_DATE: TDateTimeField index 6 read GetDateTimeField;
    property NEXT_MAX_QUANTITY_BEGIN_DATE: TDateTimeField index 7 read GetDateTimeField;
    property NEXT_PER_ITEM_BEGIN_DATE: TDateTimeField index 8 read GetDateTimeField;
    property NEXT_FLAT_AMOUNT_BEGIN_DATE: TDateTimeField index 9 read GetDateTimeField;
    property MINIMUM_QUANTITY: TFloatField index 10 read GetFloatField;
    property NEXT_MINIMUM_QUANTITY: TFloatField index 11 read GetFloatField;
    property MAXIMUM_QUANTITY: TFloatField index 12 read GetFloatField;
    property NEXT_MAXIMUM_QUANTITY: TFloatField index 13 read GetFloatField;
    property PER_ITEM_RATE: TFloatField index 14 read GetFloatField;
    property NEXT_PER_ITEM_RATE: TFloatField index 15 read GetFloatField;
    property FLAT_AMOUNT: TFloatField index 16 read GetFloatField;
    property NEXT_FLAT_AMOUNT: TFloatField index 17 read GetFloatField;
  end;

  TSB_TASK = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_TASK_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_USER_NBR: TIntegerField index 5 read GetIntegerField;
    property SCHEDULE: TStringField index 6 read GetStringField;
    property DESCRIPTION: TStringField index 7 read GetStringField;
    property TASK: TBlobField index 8 read GetBlobField;
    property LAST_RUN: TDateTimeField index 9 read GetDateTimeField;
    property NOTES: TStringField index 10 read GetStringField;
  end;

  TSB_TAX_PAYMENT = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_TAX_PAYMENT_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property STATUS: TStringField index 6 read GetStringField;
    property STATUS_DATE: TDateTimeField index 7 read GetDateTimeField;
  end;

  TSB_TEAM = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_TEAM_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property TEAM_DESCRIPTION: TStringField index 5 read GetStringField;
    property CR_CATEGORY: TStringField index 6 read GetStringField;
  end;

  TSB_TEAM_MEMBERS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_TEAM_MEMBERS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_TEAM_NBR: TIntegerField index 5 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSB_USER = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_USER_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property USER_ID: TStringField index 5 read GetStringField;
    property USER_SIGNATURE: TBlobField index 6 read GetBlobField;
    property LAST_NAME: TStringField index 7 read GetStringField;
    property FIRST_NAME: TStringField index 8 read GetStringField;
    property MIDDLE_INITIAL: TStringField index 9 read GetStringField;
    property ACTIVE_USER: TStringField index 10 read GetStringField;
    property DEPARTMENT: TStringField index 11 read GetStringField;
    property PASSWORD_CHANGE_DATE: TDateTimeField index 12 read GetDateTimeField;
    property SECURITY_LEVEL: TStringField index 13 read GetStringField;
    property USER_UPDATE_OPTIONS: TStringField index 14 read GetStringField;
    property USER_FUNCTIONS: TStringField index 15 read GetStringField;
    property USER_PASSWORD: TStringField index 16 read GetStringField;
    property EMAIL_ADDRESS: TStringField index 17 read GetStringField;
    property CL_NBR: TIntegerField index 18 read GetIntegerField;
    property SB_ACCOUNTANT_NBR: TIntegerField index 19 read GetIntegerField;
    property WRONG_PSWD_ATTEMPTS: TIntegerField index 20 read GetIntegerField;
    property LINKS_DATA: TStringField index 21 read GetStringField;
    property LOGIN_QUESTION1: TIntegerField index 22 read GetIntegerField;
    property LOGIN_ANSWER1: TStringField index 23 read GetStringField;
    property LOGIN_QUESTION2: TIntegerField index 24 read GetIntegerField;
    property LOGIN_ANSWER2: TStringField index 25 read GetStringField;
    property LOGIN_QUESTION3: TIntegerField index 26 read GetIntegerField;
    property LOGIN_ANSWER3: TStringField index 27 read GetStringField;
    property HR_PERSONNEL: TStringField index 28 read GetStringField;
    property SEC_QUESTION1: TIntegerField index 29 read GetIntegerField;
    property SEC_ANSWER1: TStringField index 30 read GetStringField;
    property SEC_QUESTION2: TIntegerField index 31 read GetIntegerField;
    property SEC_ANSWER2: TStringField index 32 read GetStringField;
    property WC_TOU_ACCEPT_DATE: TDateTimeField index 33 read GetDateTimeField;
    property ANALYTICS_PERSONNEL: TStringField index 34 read GetStringField;
    property EVOLUTION_PRODUCT: TStringField index 35 read GetStringField;
  end;

  TSB_USER_NOTICE = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_USER_NOTICE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_USER_NBR: TIntegerField index 5 read GetIntegerField;
    property NAME: TStringField index 6 read GetStringField;
    property NOTES: TBlobField index 7 read GetBlobField;
    property TASK: TBlobField index 8 read GetBlobField;
    property LAST_DISMISS: TDateTimeField index 9 read GetDateTimeField;
    property NEXT_REMINDER: TDateTimeField index 10 read GetDateTimeField;
  end;

  TSB_STORAGE = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_STORAGE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property TAG: TStringField index 5 read GetStringField;
    property STORAGE_DATA: TBlobField index 6 read GetBlobField;
  end;

  TSB_USER_MESSAGES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_USER_MESSAGES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property USER_STEREOTYPE: TStringField index 5 read GetStringField;
    property ORDER_NUMBER: TIntegerField index 6 read GetIntegerField;
    property DATA: TBlobField index 7 read GetBlobField;
  end;

  TSB_USER_PREFERENCES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_USER_PREFERENCES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_USER_NBR: TIntegerField index 5 read GetIntegerField;
    property PREFERENCE_TYPE: TStringField index 6 read GetStringField;
    property PREFERENCE_VALUE: TBlobField index 7 read GetBlobField;
    property LAST_MODIFIED_DATE: TDateTimeField index 8 read GetDateTimeField;
  end;

  TSB_DASHBOARDS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_DASHBOARDS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DASHBOARD_TYPE: TStringField index 5 read GetStringField;
    property SY_ANALYTICS_TIER_NBR: TIntegerField index 6 read GetIntegerField;
    property NOTES: TBlobField index 7 read GetBlobField;
    property DASHBOARD_ID: TIntegerField index 8 read GetIntegerField;
    property DESCRIPTION: TStringField index 9 read GetStringField;
  end;

  TSB_ENABLED_DASHBOARDS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ENABLED_DASHBOARDS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DASHBOARD_LEVEL: TStringField index 5 read GetStringField;
    property DASHBOARD_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSB_ACA_GROUP = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ACA_GROUP_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property ACA_GROUP_DESCRIPTION: TStringField index 5 read GetStringField;
    property PRIMARY_CL_NBR: TIntegerField index 6 read GetIntegerField;
    property PRIMARY_CO_NBR: TIntegerField index 7 read GetIntegerField;
  end;

  TSB_CUSTOM_VENDORS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_CUSTOM_VENDORS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property VENDOR_NAME: TStringField index 5 read GetStringField;
    property SY_VENDOR_CATEGORIES_NBR: TIntegerField index 6 read GetIntegerField;
    property VENDOR_TYPE: TStringField index 7 read GetStringField;
  end;

  TSB_VENDOR = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_VENDOR_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property VENDORS_LEVEL: TStringField index 5 read GetStringField;
    property VENDOR_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSB_VENDOR_DETAIL = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_VENDOR_DETAIL_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_VENDOR_NBR: TIntegerField index 5 read GetIntegerField;
    property DETAIL: TStringField index 6 read GetStringField;
    property DETAIL_LEVEL: TStringField index 7 read GetStringField;
    property CO_DETAIL_REQUIRED: TStringField index 8 read GetStringField;
  end;

  TSB_VENDOR_DETAIL_VALUES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_VENDOR_DETAIL_VALUES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_VENDOR_DETAIL_NBR: TIntegerField index 5 read GetIntegerField;
    property DETAIL_VALUES: TStringField index 6 read GetStringField;
  end;

  TSB_ANALYTICS_ENABLED = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ANALYTICS_ENABLED_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CL_NBR: TIntegerField index 5 read GetIntegerField;
    property CO_NBR: TIntegerField index 6 read GetIntegerField;
    property ENABLE_ANALYTICS: TStringField index 7 read GetStringField;
  end;

  TSB_ACA_GROUP_ADD_MEMBERS = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ACA_GROUP_ADD_MEMBERS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_ACA_GROUP_NBR: TIntegerField index 5 read GetIntegerField;
    property ALE_COMPANY_NAME: TStringField index 6 read GetStringField;
    property ALE_EIN_NUMBER: TIntegerField index 7 read GetIntegerField;
    property ALE_MAIN_COMPANY: TStringField index 8 read GetStringField;
    property ALE_NUMBER_OF_FTES: TIntegerField index 9 read GetIntegerField;
  end;

  TSB_ACA_OFFER_CODES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ACA_OFFER_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_FED_ACA_OFFER_CODES_NBR: TIntegerField index 5 read GetIntegerField;
    property ACA_OFFER_CODE_DESCRIPTION: TStringField index 6 read GetStringField;
  end;

  TSB_ACA_RELIEF_CODES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ACA_RELIEF_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_FED_ACA_RELIEF_CODES_NBR: TIntegerField index 5 read GetIntegerField;
    property ACA_RELIEF_CODE_DESCRIPTION: TStringField index 6 read GetStringField;
  end;

  TSB_ADDITIONAL_INFO_NAMES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ADDITIONAL_INFO_NAMES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CATEGORY: TStringField index 5 read GetStringField;
    property DATA_TYPE: TStringField index 6 read GetStringField;
    property NAME: TStringField index 7 read GetStringField;
    property REQUIRED: TStringField index 8 read GetStringField;
    property SEQ_NUM: TIntegerField index 9 read GetIntegerField;
    property SHOW_IN_EVO_PAYROLL: TStringField index 10 read GetStringField;
  end;

  TSB_ADDITIONAL_INFO_VALUES = class(TddSBTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SB_ADDITIONAL_INFO_VALUES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SB_ADDITIONAL_INFO_NAMES_NBR: TIntegerField index 5 read GetIntegerField;
    property INFO_VALUE: TStringField index 6 read GetStringField;
  end;

  TDM_BUREAU_ = class(TddDatabase)
  public
    class function GetDBType: TevDBType; override;
  private
    function GetSB: TSB;
  published
    property SB: TSB read GetSB;
  private
    function GetSB_ACCOUNTANT: TSB_ACCOUNTANT;
  published
    property SB_ACCOUNTANT: TSB_ACCOUNTANT read GetSB_ACCOUNTANT;
  private
    function GetSB_AGENCY: TSB_AGENCY;
  published
    property SB_AGENCY: TSB_AGENCY read GetSB_AGENCY;
  private
    function GetSB_AGENCY_REPORTS: TSB_AGENCY_REPORTS;
  published
    property SB_AGENCY_REPORTS: TSB_AGENCY_REPORTS read GetSB_AGENCY_REPORTS;
  private
    function GetSB_BANKS: TSB_BANKS;
  published
    property SB_BANKS: TSB_BANKS read GetSB_BANKS;
  private
    function GetSB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS;
  published
    property SB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS read GetSB_BANK_ACCOUNTS;
  private
    function GetSB_BLOB: TSB_BLOB;
  published
    property SB_BLOB: TSB_BLOB read GetSB_BLOB;
  private
    function GetSB_DELIVERY_COMPANY: TSB_DELIVERY_COMPANY;
  published
    property SB_DELIVERY_COMPANY: TSB_DELIVERY_COMPANY read GetSB_DELIVERY_COMPANY;
  private
    function GetSB_DELIVERY_COMPANY_SVCS: TSB_DELIVERY_COMPANY_SVCS;
  published
    property SB_DELIVERY_COMPANY_SVCS: TSB_DELIVERY_COMPANY_SVCS read GetSB_DELIVERY_COMPANY_SVCS;
  private
    function GetSB_DELIVERY_METHOD: TSB_DELIVERY_METHOD;
  published
    property SB_DELIVERY_METHOD: TSB_DELIVERY_METHOD read GetSB_DELIVERY_METHOD;
  private
    function GetSB_DELIVERY_SERVICE: TSB_DELIVERY_SERVICE;
  published
    property SB_DELIVERY_SERVICE: TSB_DELIVERY_SERVICE read GetSB_DELIVERY_SERVICE;
  private
    function GetSB_DELIVERY_SERVICE_OPT: TSB_DELIVERY_SERVICE_OPT;
  published
    property SB_DELIVERY_SERVICE_OPT: TSB_DELIVERY_SERVICE_OPT read GetSB_DELIVERY_SERVICE_OPT;
  private
    function GetSB_ENLIST_GROUPS: TSB_ENLIST_GROUPS;
  published
    property SB_ENLIST_GROUPS: TSB_ENLIST_GROUPS read GetSB_ENLIST_GROUPS;
  private
    function GetSB_GLOBAL_AGENCY_CONTACTS: TSB_GLOBAL_AGENCY_CONTACTS;
  published
    property SB_GLOBAL_AGENCY_CONTACTS: TSB_GLOBAL_AGENCY_CONTACTS read GetSB_GLOBAL_AGENCY_CONTACTS;
  private
    function GetSB_HOLIDAYS: TSB_HOLIDAYS;
  published
    property SB_HOLIDAYS: TSB_HOLIDAYS read GetSB_HOLIDAYS;
  private
    function GetSB_MAIL_BOX: TSB_MAIL_BOX;
  published
    property SB_MAIL_BOX: TSB_MAIL_BOX read GetSB_MAIL_BOX;
  private
    function GetSB_MAIL_BOX_CONTENT: TSB_MAIL_BOX_CONTENT;
  published
    property SB_MAIL_BOX_CONTENT: TSB_MAIL_BOX_CONTENT read GetSB_MAIL_BOX_CONTENT;
  private
    function GetSB_MAIL_BOX_OPTION: TSB_MAIL_BOX_OPTION;
  published
    property SB_MAIL_BOX_OPTION: TSB_MAIL_BOX_OPTION read GetSB_MAIL_BOX_OPTION;
  private
    function GetSB_MEDIA_TYPE: TSB_MEDIA_TYPE;
  published
    property SB_MEDIA_TYPE: TSB_MEDIA_TYPE read GetSB_MEDIA_TYPE;
  private
    function GetSB_MEDIA_TYPE_OPTION: TSB_MEDIA_TYPE_OPTION;
  published
    property SB_MEDIA_TYPE_OPTION: TSB_MEDIA_TYPE_OPTION read GetSB_MEDIA_TYPE_OPTION;
  private
    function GetSB_MULTICLIENT_REPORTS: TSB_MULTICLIENT_REPORTS;
  published
    property SB_MULTICLIENT_REPORTS: TSB_MULTICLIENT_REPORTS read GetSB_MULTICLIENT_REPORTS;
  private
    function GetSB_OPTION: TSB_OPTION;
  published
    property SB_OPTION: TSB_OPTION read GetSB_OPTION;
  private
    function GetSB_OTHER_SERVICE: TSB_OTHER_SERVICE;
  published
    property SB_OTHER_SERVICE: TSB_OTHER_SERVICE read GetSB_OTHER_SERVICE;
  private
    function GetSB_PAPER_INFO: TSB_PAPER_INFO;
  published
    property SB_PAPER_INFO: TSB_PAPER_INFO read GetSB_PAPER_INFO;
  private
    function GetSB_QUEUE_PRIORITY: TSB_QUEUE_PRIORITY;
  published
    property SB_QUEUE_PRIORITY: TSB_QUEUE_PRIORITY read GetSB_QUEUE_PRIORITY;
  private
    function GetSB_REFERRALS: TSB_REFERRALS;
  published
    property SB_REFERRALS: TSB_REFERRALS read GetSB_REFERRALS;
  private
    function GetSB_REPORTS: TSB_REPORTS;
  published
    property SB_REPORTS: TSB_REPORTS read GetSB_REPORTS;
  private
    function GetSB_REPORT_WRITER_REPORTS: TSB_REPORT_WRITER_REPORTS;
  published
    property SB_REPORT_WRITER_REPORTS: TSB_REPORT_WRITER_REPORTS read GetSB_REPORT_WRITER_REPORTS;
  private
    function GetSB_SALES_TAX_STATES: TSB_SALES_TAX_STATES;
  published
    property SB_SALES_TAX_STATES: TSB_SALES_TAX_STATES read GetSB_SALES_TAX_STATES;
  private
    function GetSB_SEC_CLIENTS: TSB_SEC_CLIENTS;
  published
    property SB_SEC_CLIENTS: TSB_SEC_CLIENTS read GetSB_SEC_CLIENTS;
  private
    function GetSB_SEC_GROUPS: TSB_SEC_GROUPS;
  published
    property SB_SEC_GROUPS: TSB_SEC_GROUPS read GetSB_SEC_GROUPS;
  private
    function GetSB_SEC_GROUP_MEMBERS: TSB_SEC_GROUP_MEMBERS;
  published
    property SB_SEC_GROUP_MEMBERS: TSB_SEC_GROUP_MEMBERS read GetSB_SEC_GROUP_MEMBERS;
  private
    function GetSB_SEC_RIGHTS: TSB_SEC_RIGHTS;
  published
    property SB_SEC_RIGHTS: TSB_SEC_RIGHTS read GetSB_SEC_RIGHTS;
  private
    function GetSB_SEC_ROW_FILTERS: TSB_SEC_ROW_FILTERS;
  published
    property SB_SEC_ROW_FILTERS: TSB_SEC_ROW_FILTERS read GetSB_SEC_ROW_FILTERS;
  private
    function GetSB_SEC_TEMPLATES: TSB_SEC_TEMPLATES;
  published
    property SB_SEC_TEMPLATES: TSB_SEC_TEMPLATES read GetSB_SEC_TEMPLATES;
  private
    function GetSB_SERVICES: TSB_SERVICES;
  published
    property SB_SERVICES: TSB_SERVICES read GetSB_SERVICES;
  private
    function GetSB_SERVICES_CALCULATIONS: TSB_SERVICES_CALCULATIONS;
  published
    property SB_SERVICES_CALCULATIONS: TSB_SERVICES_CALCULATIONS read GetSB_SERVICES_CALCULATIONS;
  private
    function GetSB_TASK: TSB_TASK;
  published
    property SB_TASK: TSB_TASK read GetSB_TASK;
  private
    function GetSB_TAX_PAYMENT: TSB_TAX_PAYMENT;
  published
    property SB_TAX_PAYMENT: TSB_TAX_PAYMENT read GetSB_TAX_PAYMENT;
  private
    function GetSB_TEAM: TSB_TEAM;
  published
    property SB_TEAM: TSB_TEAM read GetSB_TEAM;
  private
    function GetSB_TEAM_MEMBERS: TSB_TEAM_MEMBERS;
  published
    property SB_TEAM_MEMBERS: TSB_TEAM_MEMBERS read GetSB_TEAM_MEMBERS;
  private
    function GetSB_USER: TSB_USER;
  published
    property SB_USER: TSB_USER read GetSB_USER;
  private
    function GetSB_USER_NOTICE: TSB_USER_NOTICE;
  published
    property SB_USER_NOTICE: TSB_USER_NOTICE read GetSB_USER_NOTICE;
  private
    function GetSB_STORAGE: TSB_STORAGE;
  published
    property SB_STORAGE: TSB_STORAGE read GetSB_STORAGE;
  private
    function GetSB_USER_MESSAGES: TSB_USER_MESSAGES;
  published
    property SB_USER_MESSAGES: TSB_USER_MESSAGES read GetSB_USER_MESSAGES;
  private
    function GetSB_USER_PREFERENCES: TSB_USER_PREFERENCES;
  published
    property SB_USER_PREFERENCES: TSB_USER_PREFERENCES read GetSB_USER_PREFERENCES;
  private
    function GetSB_DASHBOARDS: TSB_DASHBOARDS;
  published
    property SB_DASHBOARDS: TSB_DASHBOARDS read GetSB_DASHBOARDS;
  private
    function GetSB_ENABLED_DASHBOARDS: TSB_ENABLED_DASHBOARDS;
  published
    property SB_ENABLED_DASHBOARDS: TSB_ENABLED_DASHBOARDS read GetSB_ENABLED_DASHBOARDS;
  private
    function GetSB_ACA_GROUP: TSB_ACA_GROUP;
  published
    property SB_ACA_GROUP: TSB_ACA_GROUP read GetSB_ACA_GROUP;
  private
    function GetSB_CUSTOM_VENDORS: TSB_CUSTOM_VENDORS;
  published
    property SB_CUSTOM_VENDORS: TSB_CUSTOM_VENDORS read GetSB_CUSTOM_VENDORS;
  private
    function GetSB_VENDOR: TSB_VENDOR;
  published
    property SB_VENDOR: TSB_VENDOR read GetSB_VENDOR;
  private
    function GetSB_VENDOR_DETAIL: TSB_VENDOR_DETAIL;
  published
    property SB_VENDOR_DETAIL: TSB_VENDOR_DETAIL read GetSB_VENDOR_DETAIL;
  private
    function GetSB_VENDOR_DETAIL_VALUES: TSB_VENDOR_DETAIL_VALUES;
  published
    property SB_VENDOR_DETAIL_VALUES: TSB_VENDOR_DETAIL_VALUES read GetSB_VENDOR_DETAIL_VALUES;
  private
    function GetSB_ANALYTICS_ENABLED: TSB_ANALYTICS_ENABLED;
  published
    property SB_ANALYTICS_ENABLED: TSB_ANALYTICS_ENABLED read GetSB_ANALYTICS_ENABLED;
  private
    function GetSB_ACA_GROUP_ADD_MEMBERS: TSB_ACA_GROUP_ADD_MEMBERS;
  published
    property SB_ACA_GROUP_ADD_MEMBERS: TSB_ACA_GROUP_ADD_MEMBERS read GetSB_ACA_GROUP_ADD_MEMBERS;
  private
    function GetSB_ACA_OFFER_CODES: TSB_ACA_OFFER_CODES;
  published
    property SB_ACA_OFFER_CODES: TSB_ACA_OFFER_CODES read GetSB_ACA_OFFER_CODES;
  private
    function GetSB_ACA_RELIEF_CODES: TSB_ACA_RELIEF_CODES;
  published
    property SB_ACA_RELIEF_CODES: TSB_ACA_RELIEF_CODES read GetSB_ACA_RELIEF_CODES;
  private
    function GetSB_ADDITIONAL_INFO_NAMES: TSB_ADDITIONAL_INFO_NAMES;
  published
    property SB_ADDITIONAL_INFO_NAMES: TSB_ADDITIONAL_INFO_NAMES read GetSB_ADDITIONAL_INFO_NAMES;
  private
    function GetSB_ADDITIONAL_INFO_VALUES: TSB_ADDITIONAL_INFO_VALUES;
  published
    property SB_ADDITIONAL_INFO_VALUES: TSB_ADDITIONAL_INFO_VALUES read GetSB_ADDITIONAL_INFO_VALUES;
  end;


procedure Register;

implementation

uses SDataDictSystem;

{TSB}

class function TSB.GetClassIndex: Integer;
begin
  Result := 151;
end;

class function TSB.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB.GetEvTableNbr: Integer;
begin
  Result := 1;
end;

class function TSB.GetEntityName: String;
begin
  Result := 'ServiceBureau';
end;

class function TSB.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 451
  else if AFieldName = 'SB_NBR' then Result := 19
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 8
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 452
  else if AFieldName = 'SB_NAME' then Result := 28
  else if AFieldName = 'ADDRESS1' then Result := 5
  else if AFieldName = 'ADDRESS2' then Result := 6
  else if AFieldName = 'CITY' then Result := 29
  else if AFieldName = 'STATE' then Result := 30
  else if AFieldName = 'ZIP_CODE' then Result := 20
  else if AFieldName = 'E_MAIL_ADDRESS' then Result := 31
  else if AFieldName = 'PARENT_SB_MODEM_NUMBER' then Result := 32
  else if AFieldName = 'DEVELOPMENT_MODEM_NUMBER' then Result := 33
  else if AFieldName = 'DEVELOPMENT_FTP_PASSWORD' then Result := 35
  else if AFieldName = 'EIN_NUMBER' then Result := 49
  else if AFieldName = 'EFTPS_TIN_NUMBER' then Result := 50
  else if AFieldName = 'EFTPS_BANK_FORMAT' then Result := 51
  else if AFieldName = 'USE_PRENOTE' then Result := 53
  else if AFieldName = 'IMPOUND_TRUST_MONIES_AS_RECEIV' then Result := 54
  else if AFieldName = 'PAY_TAX_FROM_PAYABLES' then Result := 55
  else if AFieldName = 'AR_EXPORT_FORMAT' then Result := 57
  else if AFieldName = 'DEFAULT_CHECK_FORMAT' then Result := 58
  else if AFieldName = 'MICR_FONT' then Result := 59
  else if AFieldName = 'MICR_HORIZONTAL_ADJUSTMENT' then Result := 22
  else if AFieldName = 'AUTO_SAVE_MINUTES' then Result := 23
  else if AFieldName = 'PHONE' then Result := 36
  else if AFieldName = 'FAX' then Result := 37
  else if AFieldName = 'COVER_LETTER_NOTES' then Result := 7
  else if AFieldName = 'INVOICE_NOTES' then Result := 4
  else if AFieldName = 'TAX_COVER_LETTER_NOTES' then Result := 3
  else if AFieldName = 'AR_IMPORT_DIRECTORY' then Result := 27
  else if AFieldName = 'ACH_DIRECTORY' then Result := 26
  else if AFieldName = 'SB_URL' then Result := 25
  else if AFieldName = 'DAYS_IN_PRENOTE' then Result := 18
  else if AFieldName = 'SB_LOGO' then Result := 9
  else if AFieldName = 'USER_PASSWORD_DURATION_IN_DAYS' then Result := 13
  else if AFieldName = 'DUMMY_TAX_CL_NBR' then Result := 17
  else if AFieldName = 'ERROR_SCREEN' then Result := 48
  else if AFieldName = 'PSWD_MIN_LENGTH' then Result := 15
  else if AFieldName = 'PSWD_FORCE_MIXED' then Result := 47
  else if AFieldName = 'MISC_CHECK_FORM' then Result := 46
  else if AFieldName = 'VMR_CONFIDENCIAL_NOTES' then Result := 2
  else if AFieldName = 'MARK_LIABS_PAID_DEFAULT' then Result := 45
  else if AFieldName = 'TRUST_IMPOUND' then Result := 44
  else if AFieldName = 'TAX_IMPOUND' then Result := 43
  else if AFieldName = 'DD_IMPOUND' then Result := 42
  else if AFieldName = 'BILLING_IMPOUND' then Result := 40
  else if AFieldName = 'WC_IMPOUND' then Result := 39
  else if AFieldName = 'DAYS_PRIOR_TO_CHECK_DATE' then Result := 14
  else if AFieldName = 'SB_EXCEPTION_PAYMENT_TYPE' then Result := 38
  else if AFieldName = 'SB_MAX_ACH_FILE_TOTAL' then Result := 1
  else if AFieldName = 'SB_ACH_FILE_LIMITATIONS' then Result := 41
  else if AFieldName = 'SB_CL_NBR' then Result := 24
  else if AFieldName = 'DASHBOARD_MSG' then Result := 543
  else if AFieldName = 'EE_LOGIN_TYPE' then Result := 544
  else if AFieldName = 'ESS_TERMS_OF_USE' then Result := 550
  else if AFieldName = 'WC_TERMS_OF_USE' then Result := 551
  else if AFieldName = 'ESS_TERMS_OF_USE_MODIFY_DATE' then Result := 568
  else if AFieldName = 'ESS_LOGO' then Result := 569
  else if AFieldName = 'THEME' then Result := 571
  else if AFieldName = 'THEME_VALUE' then Result := 572
  else if AFieldName = 'WC_TERMS_OF_USE_MODIFY_DATE' then Result := 573
  else if AFieldName = 'ANALYTICS_LICENSE' then Result := 574
  else if AFieldName = 'SESSION_LOCKOUT' then Result := 619
  else if AFieldName = 'SESSION_TIMEOUT' then Result := 620
  else if AFieldName = 'ENFORCE_EE_DOB_DEFAULT' then Result := 689
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else if FieldName = 'SB_NAME' then Result := 'Name'
  else if FieldName = 'ADDRESS1' then Result := 'Address:Address.Street1'
  else if FieldName = 'ADDRESS2' then Result := 'Address:Address.Street2'
  else if FieldName = 'CITY' then Result := 'Address:Address.City'
  else if FieldName = 'STATE' then Result := 'Address:Address.State'
  else if FieldName = 'ZIP_CODE' then Result := 'Address:Address.ZipCode'
  else if FieldName = 'PHONE' then Result := 'Phone'
  else if FieldName = 'FAX' then Result := 'Fax'
  else if FieldName = 'SB_URL' then Result := 'Url'
  else if FieldName = 'ESS_TERMS_OF_USE' then Result := 'EssTermsOfUse'
  else if FieldName = 'WC_TERMS_OF_USE' then Result := 'WcTermsOfUse'
  else if FieldName = 'ESS_TERMS_OF_USE_MODIFY_DATE' then Result := 'EssTermsOfUseModifyDate'
  else if FieldName = 'ESS_LOGO' then Result := 'EssLogo'
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 32);
  Result[0] := 'SB_NBR';
  Result[1] := 'SB_NAME';
  Result[2] := 'ADDRESS1';
  Result[3] := 'CITY';
  Result[4] := 'STATE';
  Result[5] := 'ZIP_CODE';
  Result[6] := 'DEVELOPMENT_MODEM_NUMBER';
  Result[7] := 'EIN_NUMBER';
  Result[8] := 'EFTPS_BANK_FORMAT';
  Result[9] := 'USE_PRENOTE';
  Result[10] := 'IMPOUND_TRUST_MONIES_AS_RECEIV';
  Result[11] := 'PAY_TAX_FROM_PAYABLES';
  Result[12] := 'AR_EXPORT_FORMAT';
  Result[13] := 'DEFAULT_CHECK_FORMAT';
  Result[14] := 'MICR_FONT';
  Result[15] := 'MICR_HORIZONTAL_ADJUSTMENT';
  Result[16] := 'AUTO_SAVE_MINUTES';
  Result[17] := 'PHONE';
  Result[18] := 'FAX';
  Result[19] := 'ERROR_SCREEN';
  Result[20] := 'PSWD_FORCE_MIXED';
  Result[21] := 'MISC_CHECK_FORM';
  Result[22] := 'MARK_LIABS_PAID_DEFAULT';
  Result[23] := 'TRUST_IMPOUND';
  Result[24] := 'TAX_IMPOUND';
  Result[25] := 'DD_IMPOUND';
  Result[26] := 'BILLING_IMPOUND';
  Result[27] := 'WC_IMPOUND';
  Result[28] := 'SB_EXCEPTION_PAYMENT_TYPE';
  Result[29] := 'SB_ACH_FILE_LIMITATIONS';
  Result[30] := 'EE_LOGIN_TYPE';
  Result[31] := 'ANALYTICS_LICENSE';
end;

class function TSB.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SB_NAME' then Result := 60
  else if FieldName = 'ADDRESS1' then Result := 30
  else if FieldName = 'ADDRESS2' then Result := 30
  else if FieldName = 'CITY' then Result := 20
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'E_MAIL_ADDRESS' then Result := 80
  else if FieldName = 'PARENT_SB_MODEM_NUMBER' then Result := 20
  else if FieldName = 'DEVELOPMENT_MODEM_NUMBER' then Result := 20
  else if FieldName = 'DEVELOPMENT_FTP_PASSWORD' then Result := 128
  else if FieldName = 'EIN_NUMBER' then Result := 9
  else if FieldName = 'EFTPS_TIN_NUMBER' then Result := 9
  else if FieldName = 'EFTPS_BANK_FORMAT' then Result := 1
  else if FieldName = 'USE_PRENOTE' then Result := 1
  else if FieldName = 'IMPOUND_TRUST_MONIES_AS_RECEIV' then Result := 1
  else if FieldName = 'PAY_TAX_FROM_PAYABLES' then Result := 1
  else if FieldName = 'AR_EXPORT_FORMAT' then Result := 1
  else if FieldName = 'DEFAULT_CHECK_FORMAT' then Result := 1
  else if FieldName = 'MICR_FONT' then Result := 1
  else if FieldName = 'PHONE' then Result := 20
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'AR_IMPORT_DIRECTORY' then Result := 80
  else if FieldName = 'ACH_DIRECTORY' then Result := 80
  else if FieldName = 'SB_URL' then Result := 80
  else if FieldName = 'ERROR_SCREEN' then Result := 1
  else if FieldName = 'PSWD_FORCE_MIXED' then Result := 1
  else if FieldName = 'MISC_CHECK_FORM' then Result := 1
  else if FieldName = 'MARK_LIABS_PAID_DEFAULT' then Result := 1
  else if FieldName = 'TRUST_IMPOUND' then Result := 1
  else if FieldName = 'TAX_IMPOUND' then Result := 1
  else if FieldName = 'DD_IMPOUND' then Result := 1
  else if FieldName = 'BILLING_IMPOUND' then Result := 1
  else if FieldName = 'WC_IMPOUND' then Result := 1
  else if FieldName = 'SB_EXCEPTION_PAYMENT_TYPE' then Result := 1
  else if FieldName = 'SB_ACH_FILE_LIMITATIONS' then Result := 1
  else if FieldName = 'DASHBOARD_MSG' then Result := 200
  else if FieldName = 'EE_LOGIN_TYPE' then Result := 1
  else if FieldName = 'THEME' then Result := 40
  else if FieldName = 'ANALYTICS_LICENSE' then Result := 1
  else if FieldName = 'ENFORCE_EE_DOB_DEFAULT' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'USER_PASSWORD_DURATION_IN_DAYS' then Result := 6
  else if FieldName = 'SB_MAX_ACH_FILE_TOTAL' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_NBR';
end;

class function TSB.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_NAME';
end;

{TSB_ACCOUNTANT}

class function TSB_ACCOUNTANT.GetClassIndex: Integer;
begin
  Result := 152;
end;

class function TSB_ACCOUNTANT.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ACCOUNTANT.GetEvTableNbr: Integer;
begin
  Result := 2;
end;

class function TSB_ACCOUNTANT.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ACCOUNTANT.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ACCOUNTANT.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 453
  else if AFieldName = 'SB_ACCOUNTANT_NBR' then Result := 67
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 66
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 454
  else if AFieldName = 'NAME' then Result := 72
  else if AFieldName = 'ADDRESS1' then Result := 62
  else if AFieldName = 'ADDRESS2' then Result := 63
  else if AFieldName = 'CITY' then Result := 73
  else if AFieldName = 'STATE' then Result := 79
  else if AFieldName = 'ZIP_CODE' then Result := 68
  else if AFieldName = 'CONTACT1' then Result := 64
  else if AFieldName = 'PHONE1' then Result := 74
  else if AFieldName = 'DESCRIPTION1' then Result := 69
  else if AFieldName = 'CONTACT2' then Result := 65
  else if AFieldName = 'PHONE2' then Result := 75
  else if AFieldName = 'DESCRIPTION2' then Result := 70
  else if AFieldName = 'FAX' then Result := 76
  else if AFieldName = 'FAX_DESCRIPTION' then Result := 71
  else if AFieldName = 'E_MAIL_ADDRESS' then Result := 77
  else if AFieldName = 'PRINT_NAME' then Result := 78
  else if AFieldName = 'TITLE' then Result := 61
  else if AFieldName = 'SIGNATURE' then Result := 60
  else if AFieldName = 'CREDIT_BANK_ACCOUNT_NBR' then Result := 585
  else if AFieldName = 'DEBIT_BANK_ACCOUNT_NBR' then Result := 586
  else if AFieldName = 'ACCOUNT_NUMBER' then Result := 587
  else if AFieldName = 'BANK_ACCOUNT_TYPE' then Result := 588
  else if AFieldName = 'SB_BANKS_NBR' then Result := 589
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ACCOUNTANT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ACCOUNTANT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 5);
  Result[0] := 'SB_ACCOUNTANT_NBR';
  Result[1] := 'NAME';
  Result[2] := 'CONTACT1';
  Result[3] := 'PRINT_NAME';
  Result[4] := 'BANK_ACCOUNT_TYPE';
end;

class function TSB_ACCOUNTANT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'ADDRESS1' then Result := 30
  else if FieldName = 'ADDRESS2' then Result := 30
  else if FieldName = 'CITY' then Result := 20
  else if FieldName = 'STATE' then Result := 3
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'CONTACT1' then Result := 30
  else if FieldName = 'PHONE1' then Result := 20
  else if FieldName = 'DESCRIPTION1' then Result := 10
  else if FieldName = 'CONTACT2' then Result := 30
  else if FieldName = 'PHONE2' then Result := 20
  else if FieldName = 'DESCRIPTION2' then Result := 10
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'FAX_DESCRIPTION' then Result := 10
  else if FieldName = 'E_MAIL_ADDRESS' then Result := 80
  else if FieldName = 'PRINT_NAME' then Result := 40
  else if FieldName = 'TITLE' then Result := 30
  else if FieldName = 'ACCOUNT_NUMBER' then Result := 20
  else if FieldName = 'BANK_ACCOUNT_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ACCOUNTANT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ACCOUNTANT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ACCOUNTANT_NBR';
end;

class function TSB_ACCOUNTANT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CREDIT_BANK_ACCOUNT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANK_ACCOUNTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'DEBIT_BANK_ACCOUNT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANK_ACCOUNTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANKS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_ACCOUNTANT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ACCOUNTANT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ACCOUNTANT_NBR';
end;

class function TSB_ACCOUNTANT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_AGENCY}

class function TSB_AGENCY.GetClassIndex: Integer;
begin
  Result := 153;
end;

class function TSB_AGENCY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_AGENCY.GetEvTableNbr: Integer;
begin
  Result := 3;
end;

class function TSB_AGENCY.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_AGENCY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_AGENCY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 455
  else if AFieldName = 'SB_AGENCY_NBR' then Result := 86
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 85
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 456
  else if AFieldName = 'NAME' then Result := 93
  else if AFieldName = 'ADDRESS1' then Result := 80
  else if AFieldName = 'ADDRESS2' then Result := 81
  else if AFieldName = 'CITY' then Result := 94
  else if AFieldName = 'STATE' then Result := 95
  else if AFieldName = 'ZIP_CODE' then Result := 87
  else if AFieldName = 'CONTACT1' then Result := 82
  else if AFieldName = 'PHONE1' then Result := 96
  else if AFieldName = 'DESCRIPTION1' then Result := 88
  else if AFieldName = 'CONTACT2' then Result := 83
  else if AFieldName = 'PHONE2' then Result := 97
  else if AFieldName = 'DESCRIPTION2' then Result := 89
  else if AFieldName = 'FAX' then Result := 98
  else if AFieldName = 'FAX_DESCRIPTION' then Result := 90
  else if AFieldName = 'E_MAIL_ADDRESS' then Result := 99
  else if AFieldName = 'AGENCY_TYPE' then Result := 105
  else if AFieldName = 'SB_BANKS_NBR' then Result := 91
  else if AFieldName = 'ACCOUNT_NUMBER' then Result := 100
  else if AFieldName = 'ACCOUNT_TYPE' then Result := 106
  else if AFieldName = 'NEGATIVE_DIRECT_DEP_ALLOWED' then Result := 107
  else if AFieldName = 'MODEM_NUMBER' then Result := 101
  else if AFieldName = 'NOTES' then Result := 84
  else if AFieldName = 'FILLER' then Result := 102
  else if AFieldName = 'PRINT_NAME' then Result := 103
  else if AFieldName = 'COUNTY' then Result := 92
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_AGENCY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_AGENCY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'SB_AGENCY_NBR';
  Result[1] := 'NAME';
  Result[2] := 'AGENCY_TYPE';
  Result[3] := 'ACCOUNT_TYPE';
  Result[4] := 'NEGATIVE_DIRECT_DEP_ALLOWED';
  Result[5] := 'PRINT_NAME';
end;

class function TSB_AGENCY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'ADDRESS1' then Result := 30
  else if FieldName = 'ADDRESS2' then Result := 30
  else if FieldName = 'CITY' then Result := 20
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'CONTACT1' then Result := 30
  else if FieldName = 'PHONE1' then Result := 20
  else if FieldName = 'DESCRIPTION1' then Result := 10
  else if FieldName = 'CONTACT2' then Result := 30
  else if FieldName = 'PHONE2' then Result := 20
  else if FieldName = 'DESCRIPTION2' then Result := 10
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'FAX_DESCRIPTION' then Result := 10
  else if FieldName = 'E_MAIL_ADDRESS' then Result := 80
  else if FieldName = 'AGENCY_TYPE' then Result := 1
  else if FieldName = 'ACCOUNT_NUMBER' then Result := 20
  else if FieldName = 'ACCOUNT_TYPE' then Result := 1
  else if FieldName = 'NEGATIVE_DIRECT_DEP_ALLOWED' then Result := 1
  else if FieldName = 'MODEM_NUMBER' then Result := 20
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'PRINT_NAME' then Result := 40
  else if FieldName = 'COUNTY' then Result := 20
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_AGENCY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_AGENCY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_AGENCY_NBR';
end;

class function TSB_AGENCY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANKS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_AGENCY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_AGENCY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SB_AGENCY_NBR';
end;

class function TSB_AGENCY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_AGENCY_REPORTS}

class function TSB_AGENCY_REPORTS.GetClassIndex: Integer;
begin
  Result := 154;
end;

class function TSB_AGENCY_REPORTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_AGENCY_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 4;
end;

class function TSB_AGENCY_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_AGENCY_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_AGENCY_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 457
  else if AFieldName = 'SB_AGENCY_REPORTS_NBR' then Result := 111
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 110
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 458
  else if AFieldName = 'SB_AGENCY_NBR' then Result := 112
  else if AFieldName = 'SB_REPORTS_NBR' then Result := 113
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_AGENCY_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_AGENCY_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_AGENCY_REPORTS_NBR';
  Result[1] := 'SB_AGENCY_NBR';
  Result[2] := 'SB_REPORTS_NBR';
end;

class function TSB_AGENCY_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_AGENCY_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_AGENCY_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_AGENCY_REPORTS_NBR';
end;

class function TSB_AGENCY_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SB_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_AGENCY_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_AGENCY_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_AGENCY_NBR';
  Result[1] := 'SB_REPORTS_NBR';
end;

{TSB_BANKS}

class function TSB_BANKS.GetClassIndex: Integer;
begin
  Result := 155;
end;

class function TSB_BANKS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_BANKS.GetEvTableNbr: Integer;
begin
  Result := 5;
end;

class function TSB_BANKS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_BANKS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_BANKS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 459
  else if AFieldName = 'SB_BANKS_NBR' then Result := 120
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 119
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 460
  else if AFieldName = 'NAME' then Result := 129
  else if AFieldName = 'ADDRESS1' then Result := 114
  else if AFieldName = 'ADDRESS2' then Result := 115
  else if AFieldName = 'CITY' then Result := 130
  else if AFieldName = 'STATE' then Result := 131
  else if AFieldName = 'ZIP_CODE' then Result := 121
  else if AFieldName = 'CONTACT1' then Result := 116
  else if AFieldName = 'PHONE1' then Result := 132
  else if AFieldName = 'DESCRIPTION1' then Result := 122
  else if AFieldName = 'CONTACT2' then Result := 117
  else if AFieldName = 'PHONE2' then Result := 133
  else if AFieldName = 'DESCRIPTION2' then Result := 123
  else if AFieldName = 'FAX' then Result := 134
  else if AFieldName = 'FAX_DESCRIPTION' then Result := 124
  else if AFieldName = 'E_MAIL_ADDRESS' then Result := 135
  else if AFieldName = 'ABA_NUMBER' then Result := 140
  else if AFieldName = 'TOP_ABA_NUMBER' then Result := 125
  else if AFieldName = 'BOTTOM_ABA_NUMBER' then Result := 126
  else if AFieldName = 'ADDENDA' then Result := 136
  else if AFieldName = 'CHECK_TEMPLATE' then Result := 118
  else if AFieldName = 'USE_CHECK_TEMPLATE' then Result := 141
  else if AFieldName = 'MICR_ACCOUNT_START_POSITION' then Result := 127
  else if AFieldName = 'MICR_CHECK_NUMBER_START_POSITN' then Result := 128
  else if AFieldName = 'FILLER' then Result := 137
  else if AFieldName = 'PRINT_NAME' then Result := 138
  else if AFieldName = 'BRANCH_IDENTIFIER' then Result := 139
  else if AFieldName = 'ALLOW_HYPHENS' then Result := 142
  else if AFieldName = 'ACH_DATE' then Result := 659
  else if AFieldName = 'ACH_NUMBER' then Result := 660
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_BANKS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_BANKS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 11);
  Result[0] := 'SB_BANKS_NBR';
  Result[1] := 'NAME';
  Result[2] := 'CONTACT1';
  Result[3] := 'ABA_NUMBER';
  Result[4] := 'TOP_ABA_NUMBER';
  Result[5] := 'BOTTOM_ABA_NUMBER';
  Result[6] := 'ADDENDA';
  Result[7] := 'CHECK_TEMPLATE';
  Result[8] := 'USE_CHECK_TEMPLATE';
  Result[9] := 'PRINT_NAME';
  Result[10] := 'ALLOW_HYPHENS';
end;

class function TSB_BANKS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'ADDRESS1' then Result := 30
  else if FieldName = 'ADDRESS2' then Result := 30
  else if FieldName = 'CITY' then Result := 20
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'CONTACT1' then Result := 30
  else if FieldName = 'PHONE1' then Result := 20
  else if FieldName = 'DESCRIPTION1' then Result := 10
  else if FieldName = 'CONTACT2' then Result := 30
  else if FieldName = 'PHONE2' then Result := 20
  else if FieldName = 'DESCRIPTION2' then Result := 10
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'FAX_DESCRIPTION' then Result := 10
  else if FieldName = 'E_MAIL_ADDRESS' then Result := 80
  else if FieldName = 'ABA_NUMBER' then Result := 9
  else if FieldName = 'TOP_ABA_NUMBER' then Result := 10
  else if FieldName = 'BOTTOM_ABA_NUMBER' then Result := 10
  else if FieldName = 'ADDENDA' then Result := 12
  else if FieldName = 'USE_CHECK_TEMPLATE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'PRINT_NAME' then Result := 40
  else if FieldName = 'BRANCH_IDENTIFIER' then Result := 9
  else if FieldName = 'ALLOW_HYPHENS' then Result := 1
  else if FieldName = 'ACH_NUMBER' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_BANKS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_BANKS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_BANKS_NBR';
end;

class function TSB_BANKS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_BANKS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'ACH_ORIGIN_SB_BANKS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACCOUNTANT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
end;

class function TSB_BANKS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_BANK_ACCOUNTS}

class function TSB_BANK_ACCOUNTS.GetClassIndex: Integer;
begin
  Result := 156;
end;

class function TSB_BANK_ACCOUNTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_BANK_ACCOUNTS.GetEvTableNbr: Integer;
begin
  Result := 6;
end;

class function TSB_BANK_ACCOUNTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_BANK_ACCOUNTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_BANK_ACCOUNTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 461
  else if AFieldName = 'SB_BANK_ACCOUNTS_NBR' then Result := 146
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 143
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 462
  else if AFieldName = 'SB_BANKS_NBR' then Result := 147
  else if AFieldName = 'CUSTOM_BANK_ACCOUNT_NUMBER' then Result := 156
  else if AFieldName = 'BANK_ACCOUNT_TYPE' then Result := 172
  else if AFieldName = 'NAME_DESCRIPTION' then Result := 157
  else if AFieldName = 'ACH_ORIGIN_SB_BANKS_NBR' then Result := 148
  else if AFieldName = 'BANK_RETURNS' then Result := 174
  else if AFieldName = 'CUSTOM_HEADER_RECORD' then Result := 158
  else if AFieldName = 'NEXT_AVAILABLE_CHECK_NUMBER' then Result := 149
  else if AFieldName = 'END_CHECK_NUMBER' then Result := 150
  else if AFieldName = 'NEXT_BEGIN_CHECK_NUMBER' then Result := 151
  else if AFieldName = 'NEXT_END_CHECK_NUMBER' then Result := 152
  else if AFieldName = 'FILLER' then Result := 159
  else if AFieldName = 'SUPPRESS_OFFSET_ACCOUNT' then Result := 171
  else if AFieldName = 'BLOCK_NEGATIVE_CHECKS' then Result := 170
  else if AFieldName = 'BATCH_FILER_ID' then Result := 175
  else if AFieldName = 'MASTER_INQUIRY_PIN' then Result := 153
  else if AFieldName = 'LOGO_SB_BLOB_NBR' then Result := 154
  else if AFieldName = 'SIGNATURE_SB_BLOB_NBR' then Result := 155
  else if AFieldName = 'BEGINNING_BALANCE' then Result := 144
  else if AFieldName = 'OPERATING_ACCOUNT' then Result := 166
  else if AFieldName = 'BILLING_ACCOUNT' then Result := 165
  else if AFieldName = 'ACH_ACCOUNT' then Result := 164
  else if AFieldName = 'TRUST_ACCOUNT' then Result := 163
  else if AFieldName = 'TAX_ACCOUNT' then Result := 162
  else if AFieldName = 'OBC_ACCOUNT' then Result := 161
  else if AFieldName = 'WORKERS_COMP_ACCOUNT' then Result := 160
  else if AFieldName = 'RECCURING_WIRE_NUMBER' then Result := 145
  else if AFieldName = 'BANK_CHECK' then Result := 553
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_BANK_ACCOUNTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_BANK_ACCOUNTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 17);
  Result[0] := 'SB_BANK_ACCOUNTS_NBR';
  Result[1] := 'SB_BANKS_NBR';
  Result[2] := 'CUSTOM_BANK_ACCOUNT_NUMBER';
  Result[3] := 'BANK_ACCOUNT_TYPE';
  Result[4] := 'ACH_ORIGIN_SB_BANKS_NBR';
  Result[5] := 'BANK_RETURNS';
  Result[6] := 'NEXT_AVAILABLE_CHECK_NUMBER';
  Result[7] := 'SUPPRESS_OFFSET_ACCOUNT';
  Result[8] := 'BLOCK_NEGATIVE_CHECKS';
  Result[9] := 'OPERATING_ACCOUNT';
  Result[10] := 'BILLING_ACCOUNT';
  Result[11] := 'ACH_ACCOUNT';
  Result[12] := 'TRUST_ACCOUNT';
  Result[13] := 'TAX_ACCOUNT';
  Result[14] := 'OBC_ACCOUNT';
  Result[15] := 'WORKERS_COMP_ACCOUNT';
  Result[16] := 'BANK_CHECK';
end;

class function TSB_BANK_ACCOUNTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CUSTOM_BANK_ACCOUNT_NUMBER' then Result := 20
  else if FieldName = 'BANK_ACCOUNT_TYPE' then Result := 1
  else if FieldName = 'NAME_DESCRIPTION' then Result := 40
  else if FieldName = 'BANK_RETURNS' then Result := 1
  else if FieldName = 'CUSTOM_HEADER_RECORD' then Result := 80
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'SUPPRESS_OFFSET_ACCOUNT' then Result := 1
  else if FieldName = 'BLOCK_NEGATIVE_CHECKS' then Result := 1
  else if FieldName = 'BATCH_FILER_ID' then Result := 9
  else if FieldName = 'MASTER_INQUIRY_PIN' then Result := 10
  else if FieldName = 'OPERATING_ACCOUNT' then Result := 1
  else if FieldName = 'BILLING_ACCOUNT' then Result := 1
  else if FieldName = 'ACH_ACCOUNT' then Result := 1
  else if FieldName = 'TRUST_ACCOUNT' then Result := 1
  else if FieldName = 'TAX_ACCOUNT' then Result := 1
  else if FieldName = 'OBC_ACCOUNT' then Result := 1
  else if FieldName = 'WORKERS_COMP_ACCOUNT' then Result := 1
  else if FieldName = 'BANK_CHECK' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_BANK_ACCOUNTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'BEGINNING_BALANCE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_BANK_ACCOUNTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_BANK_ACCOUNTS_NBR';
end;

class function TSB_BANK_ACCOUNTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANKS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANKS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'ACH_ORIGIN_SB_BANKS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BANKS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BLOB;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'LOGO_SB_BLOB_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BLOB_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BLOB;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SIGNATURE_SB_BLOB_NBR';
  Result[High(Result)].DestFields[0] := 'SB_BLOB_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_BANK_ACCOUNTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACCOUNTANT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANK_ACCOUNTS_NBR';
  Result[High(Result)].DestFields[0] := 'CREDIT_BANK_ACCOUNT_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACCOUNTANT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BANK_ACCOUNTS_NBR';
  Result[High(Result)].DestFields[0] := 'DEBIT_BANK_ACCOUNT_NBR';
end;

class function TSB_BANK_ACCOUNTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_BANKS_NBR';
  Result[1] := 'BANK_ACCOUNT_TYPE';
  Result[2] := 'CUSTOM_BANK_ACCOUNT_NUMBER';
end;

{TSB_BLOB}

class function TSB_BLOB.GetClassIndex: Integer;
begin
  Result := 157;
end;

class function TSB_BLOB.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_BLOB.GetEvTableNbr: Integer;
begin
  Result := 7;
end;

class function TSB_BLOB.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_BLOB.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_BLOB.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 540
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 541
  else if AFieldName = 'SB_BLOB_NBR' then Result := 178
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 542
  else if AFieldName = 'DATA' then Result := 177
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_BLOB.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_BLOB.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_BLOB_NBR';
end;

class function TSB_BLOB.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_BLOB.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_BLOB.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_BLOB_NBR';
end;

class function TSB_BLOB.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_BLOB.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BLOB_NBR';
  Result[High(Result)].DestFields[0] := 'LOGO_SB_BLOB_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_BANK_ACCOUNTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_BLOB_NBR';
  Result[High(Result)].DestFields[0] := 'SIGNATURE_SB_BLOB_NBR';
end;

class function TSB_BLOB.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_DELIVERY_COMPANY}

class function TSB_DELIVERY_COMPANY.GetClassIndex: Integer;
begin
  Result := 158;
end;

class function TSB_DELIVERY_COMPANY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DELIVERY_COMPANY.GetEvTableNbr: Integer;
begin
  Result := 8;
end;

class function TSB_DELIVERY_COMPANY.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DELIVERY_COMPANY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DELIVERY_COMPANY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 463
  else if AFieldName = 'SB_DELIVERY_COMPANY_NBR' then Result := 181
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 180
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 464
  else if AFieldName = 'NAME' then Result := 182
  else if AFieldName = 'DELIVERY_CONTACT' then Result := 183
  else if AFieldName = 'DELIVERY_CONTACT_PHONE' then Result := 184
  else if AFieldName = 'DELIVERY_CONTACT_PHONE_TYPE' then Result := 188
  else if AFieldName = 'SUPPLIES_CONTACT' then Result := 185
  else if AFieldName = 'SUPPLIES_CONTACT_PHONE' then Result := 186
  else if AFieldName = 'SUPPLIES_CONTACT_PHONE_TYPE' then Result := 189
  else if AFieldName = 'WEB_SITE' then Result := 187
  else if AFieldName = 'DELIVERY_COMPANY_NOTES' then Result := 179
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DELIVERY_COMPANY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DELIVERY_COMPANY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_DELIVERY_COMPANY_NBR';
  Result[1] := 'NAME';
  Result[2] := 'DELIVERY_CONTACT_PHONE_TYPE';
  Result[3] := 'SUPPLIES_CONTACT_PHONE_TYPE';
end;

class function TSB_DELIVERY_COMPANY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'DELIVERY_CONTACT' then Result := 40
  else if FieldName = 'DELIVERY_CONTACT_PHONE' then Result := 20
  else if FieldName = 'DELIVERY_CONTACT_PHONE_TYPE' then Result := 1
  else if FieldName = 'SUPPLIES_CONTACT' then Result := 40
  else if FieldName = 'SUPPLIES_CONTACT_PHONE' then Result := 20
  else if FieldName = 'SUPPLIES_CONTACT_PHONE_TYPE' then Result := 1
  else if FieldName = 'WEB_SITE' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DELIVERY_COMPANY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DELIVERY_COMPANY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DELIVERY_COMPANY_NBR';
end;

class function TSB_DELIVERY_COMPANY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_DELIVERY_COMPANY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_COMPANY_SVCS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_COMPANY_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_COMPANY_NBR';
end;

class function TSB_DELIVERY_COMPANY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_DELIVERY_COMPANY_SVCS}

class function TSB_DELIVERY_COMPANY_SVCS.GetClassIndex: Integer;
begin
  Result := 159;
end;

class function TSB_DELIVERY_COMPANY_SVCS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetEvTableNbr: Integer;
begin
  Result := 9;
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DELIVERY_COMPANY_SVCS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 465
  else if AFieldName = 'SB_DELIVERY_COMPANY_SVCS_NBR' then Result := 192
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 190
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 466
  else if AFieldName = 'SB_DELIVERY_COMPANY_NBR' then Result := 193
  else if AFieldName = 'DESCRIPTION' then Result := 194
  else if AFieldName = 'REFERENCE_FEE' then Result := 191
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_DELIVERY_COMPANY_SVCS_NBR';
  Result[1] := 'SB_DELIVERY_COMPANY_NBR';
  Result[2] := 'DESCRIPTION';
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'REFERENCE_FEE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DELIVERY_COMPANY_SVCS_NBR';
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_COMPANY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_COMPANY_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_COMPANY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_DELIVERY_COMPANY_SVCS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSB_DELIVERY_METHOD}

class function TSB_DELIVERY_METHOD.GetClassIndex: Integer;
begin
  Result := 160;
end;

class function TSB_DELIVERY_METHOD.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DELIVERY_METHOD.GetEvTableNbr: Integer;
begin
  Result := 10;
end;

class function TSB_DELIVERY_METHOD.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DELIVERY_METHOD.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DELIVERY_METHOD.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 467
  else if AFieldName = 'SB_DELIVERY_METHOD_NBR' then Result := 197
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 195
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 468
  else if AFieldName = 'SY_DELIVERY_METHOD_NBR' then Result := 196
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DELIVERY_METHOD.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DELIVERY_METHOD.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[1] := 'SY_DELIVERY_METHOD_NBR';
end;

class function TSB_DELIVERY_METHOD.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DELIVERY_METHOD.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DELIVERY_METHOD.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DELIVERY_METHOD_NBR';
end;

class function TSB_DELIVERY_METHOD.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_DELIVERY_METHOD;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SY_DELIVERY_METHOD_NBR';
end;

class function TSB_DELIVERY_METHOD.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SERVICES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_SERVICE_OPT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
end;

class function TSB_DELIVERY_METHOD.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_DELIVERY_SERVICE}

class function TSB_DELIVERY_SERVICE.GetClassIndex: Integer;
begin
  Result := 161;
end;

class function TSB_DELIVERY_SERVICE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DELIVERY_SERVICE.GetEvTableNbr: Integer;
begin
  Result := 11;
end;

class function TSB_DELIVERY_SERVICE.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DELIVERY_SERVICE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DELIVERY_SERVICE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 469
  else if AFieldName = 'SB_DELIVERY_SERVICE_NBR' then Result := 200
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 198
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 470
  else if AFieldName = 'SY_DELIVERY_SERVICE_NBR' then Result := 199
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DELIVERY_SERVICE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DELIVERY_SERVICE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_DELIVERY_SERVICE_NBR';
  Result[1] := 'SY_DELIVERY_SERVICE_NBR';
end;

class function TSB_DELIVERY_SERVICE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DELIVERY_SERVICE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DELIVERY_SERVICE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DELIVERY_SERVICE_NBR';
end;

class function TSB_DELIVERY_SERVICE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_DELIVERY_SERVICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_DELIVERY_SERVICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_DELIVERY_SERVICE_NBR';
end;

class function TSB_DELIVERY_SERVICE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_SERVICE_OPT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_SERVICE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_SERVICE_NBR';
end;

class function TSB_DELIVERY_SERVICE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_DELIVERY_SERVICE_OPT}

class function TSB_DELIVERY_SERVICE_OPT.GetClassIndex: Integer;
begin
  Result := 162;
end;

class function TSB_DELIVERY_SERVICE_OPT.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DELIVERY_SERVICE_OPT.GetEvTableNbr: Integer;
begin
  Result := 12;
end;

class function TSB_DELIVERY_SERVICE_OPT.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DELIVERY_SERVICE_OPT.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DELIVERY_SERVICE_OPT.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 471
  else if AFieldName = 'SB_DELIVERY_SERVICE_OPT_NBR' then Result := 205
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 201
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 472
  else if AFieldName = 'SB_DELIVERY_SERVICE_NBR' then Result := 204
  else if AFieldName = 'SB_DELIVERY_METHOD_NBR' then Result := 203
  else if AFieldName = 'OPTION_TAG' then Result := 206
  else if AFieldName = 'OPTION_INTEGER_VALUE' then Result := 202
  else if AFieldName = 'OPTION_STRING_VALUE' then Result := 207
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DELIVERY_SERVICE_OPT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DELIVERY_SERVICE_OPT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_DELIVERY_SERVICE_OPT_NBR';
  Result[1] := 'SB_DELIVERY_SERVICE_NBR';
  Result[2] := 'OPTION_TAG';
end;

class function TSB_DELIVERY_SERVICE_OPT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'OPTION_TAG' then Result := 40
  else if FieldName = 'OPTION_STRING_VALUE' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DELIVERY_SERVICE_OPT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DELIVERY_SERVICE_OPT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DELIVERY_SERVICE_OPT_NBR';
end;

class function TSB_DELIVERY_SERVICE_OPT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_SERVICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_SERVICE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_SERVICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_METHOD;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_DELIVERY_SERVICE_OPT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_DELIVERY_SERVICE_OPT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ENLIST_GROUPS}

class function TSB_ENLIST_GROUPS.GetClassIndex: Integer;
begin
  Result := 163;
end;

class function TSB_ENLIST_GROUPS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ENLIST_GROUPS.GetEvTableNbr: Integer;
begin
  Result := 13;
end;

class function TSB_ENLIST_GROUPS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ENLIST_GROUPS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ENLIST_GROUPS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 473
  else if AFieldName = 'SB_ENLIST_GROUPS_NBR' then Result := 210
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 208
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 474
  else if AFieldName = 'SY_REPORT_GROUPS_NBR' then Result := 209
  else if AFieldName = 'MEDIA_TYPE' then Result := 212
  else if AFieldName = 'PROCESS_TYPE' then Result := 211
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ENLIST_GROUPS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ENLIST_GROUPS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ENLIST_GROUPS_NBR';
  Result[1] := 'SY_REPORT_GROUPS_NBR';
  Result[2] := 'PROCESS_TYPE';
end;

class function TSB_ENLIST_GROUPS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MEDIA_TYPE' then Result := 2
  else if FieldName = 'PROCESS_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ENLIST_GROUPS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ENLIST_GROUPS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ENLIST_GROUPS_NBR';
end;

class function TSB_ENLIST_GROUPS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_GROUPS_NBR';
end;

class function TSB_ENLIST_GROUPS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ENLIST_GROUPS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORT_GROUPS_NBR';
end;

{TSB_GLOBAL_AGENCY_CONTACTS}

class function TSB_GLOBAL_AGENCY_CONTACTS.GetClassIndex: Integer;
begin
  Result := 164;
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetEvTableNbr: Integer;
begin
  Result := 14;
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 475
  else if AFieldName = 'SB_GLOBAL_AGENCY_CONTACTS_NBR' then Result := 216
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 215
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 476
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 217
  else if AFieldName = 'CONTACT_NAME' then Result := 213
  else if AFieldName = 'PHONE' then Result := 219
  else if AFieldName = 'FAX' then Result := 220
  else if AFieldName = 'E_MAIL_ADDRESS' then Result := 221
  else if AFieldName = 'NOTES' then Result := 214
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_GLOBAL_AGENCY_CONTACTS_NBR';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
  Result[2] := 'CONTACT_NAME';
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CONTACT_NAME' then Result := 30
  else if FieldName = 'PHONE' then Result := 20
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'E_MAIL_ADDRESS' then Result := 80
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_GLOBAL_AGENCY_CONTACTS_NBR';
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_GLOBAL_AGENCY_CONTACTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CONTACT_NAME';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
end;

{TSB_HOLIDAYS}

class function TSB_HOLIDAYS.GetClassIndex: Integer;
begin
  Result := 165;
end;

class function TSB_HOLIDAYS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_HOLIDAYS.GetEvTableNbr: Integer;
begin
  Result := 15;
end;

class function TSB_HOLIDAYS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_HOLIDAYS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_HOLIDAYS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 477
  else if AFieldName = 'SB_HOLIDAYS_NBR' then Result := 224
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 222
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 478
  else if AFieldName = 'HOLIDAY_DATE' then Result := 223
  else if AFieldName = 'HOLIDAY_NAME' then Result := 225
  else if AFieldName = 'USED_BY' then Result := 226
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_HOLIDAYS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_HOLIDAYS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_HOLIDAYS_NBR';
  Result[1] := 'HOLIDAY_DATE';
  Result[2] := 'HOLIDAY_NAME';
  Result[3] := 'USED_BY';
end;

class function TSB_HOLIDAYS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'HOLIDAY_NAME' then Result := 40
  else if FieldName = 'USED_BY' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_HOLIDAYS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_HOLIDAYS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_HOLIDAYS_NBR';
end;

class function TSB_HOLIDAYS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_HOLIDAYS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_HOLIDAYS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'HOLIDAY_DATE';
end;

{TSB_MAIL_BOX}

class function TSB_MAIL_BOX.GetClassIndex: Integer;
begin
  Result := 166;
end;

class function TSB_MAIL_BOX.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MAIL_BOX.GetEvTableNbr: Integer;
begin
  Result := 16;
end;

class function TSB_MAIL_BOX.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MAIL_BOX.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MAIL_BOX.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 479
  else if AFieldName = 'SB_MAIL_BOX_NBR' then Result := 242
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 233
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 480
  else if AFieldName = 'CL_NBR' then Result := 241
  else if AFieldName = 'CL_MAIL_BOX_GROUP_NBR' then Result := 240
  else if AFieldName = 'COST' then Result := 232
  else if AFieldName = 'REQUIRED' then Result := 251
  else if AFieldName = 'NOTIFICATION_EMAIL' then Result := 248
  else if AFieldName = 'PR_NBR' then Result := 239
  else if AFieldName = 'RELEASED_TIME' then Result := 231
  else if AFieldName = 'PRINTED_TIME' then Result := 230
  else if AFieldName = 'SB_COVER_LETTER_REPORT_NBR' then Result := 238
  else if AFieldName = 'SCANNED_TIME' then Result := 229
  else if AFieldName = 'SHIPPED_TIME' then Result := 228
  else if AFieldName = 'SY_COVER_LETTER_REPORT_NBR' then Result := 237
  else if AFieldName = 'NOTE' then Result := 247
  else if AFieldName = 'UP_LEVEL_MAIL_BOX_NBR' then Result := 236
  else if AFieldName = 'TRACKING_INFO' then Result := 246
  else if AFieldName = 'AUTO_RELEASE_TYPE' then Result := 250
  else if AFieldName = 'BARCODE' then Result := 245
  else if AFieldName = 'CREATED_TIME' then Result := 227
  else if AFieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING' then Result := 249
  else if AFieldName = 'ADDRESSEE' then Result := 244
  else if AFieldName = 'DESCRIPTION' then Result := 243
  else if AFieldName = 'SB_DELIVERY_METHOD_NBR' then Result := 235
  else if AFieldName = 'SB_MEDIA_TYPE_NBR' then Result := 234
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MAIL_BOX.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MAIL_BOX.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 10);
  Result[0] := 'SB_MAIL_BOX_NBR';
  Result[1] := 'REQUIRED';
  Result[2] := 'AUTO_RELEASE_TYPE';
  Result[3] := 'BARCODE';
  Result[4] := 'CREATED_TIME';
  Result[5] := 'DISPOSE_CONTENT_AFTER_SHIPPING';
  Result[6] := 'ADDRESSEE';
  Result[7] := 'DESCRIPTION';
  Result[8] := 'SB_DELIVERY_METHOD_NBR';
  Result[9] := 'SB_MEDIA_TYPE_NBR';
end;

class function TSB_MAIL_BOX.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'REQUIRED' then Result := 1
  else if FieldName = 'NOTIFICATION_EMAIL' then Result := 80
  else if FieldName = 'NOTE' then Result := 512
  else if FieldName = 'TRACKING_INFO' then Result := 40
  else if FieldName = 'AUTO_RELEASE_TYPE' then Result := 1
  else if FieldName = 'BARCODE' then Result := 40
  else if FieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING' then Result := 1
  else if FieldName = 'ADDRESSEE' then Result := 512
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MAIL_BOX.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'COST' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MAIL_BOX.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MAIL_BOX_NBR';
end;

class function TSB_MAIL_BOX.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'UP_LEVEL_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_METHOD;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MEDIA_TYPE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_REPORT_WRITER_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_COVER_LETTER_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_WRITER_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_COVER_LETTER_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

class function TSB_MAIL_BOX.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'UP_LEVEL_MAIL_BOX_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX_CONTENT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX_OPTION;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_NBR';
end;

class function TSB_MAIL_BOX.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'BARCODE';
end;

{TSB_MAIL_BOX_CONTENT}

class function TSB_MAIL_BOX_CONTENT.GetClassIndex: Integer;
begin
  Result := 167;
end;

class function TSB_MAIL_BOX_CONTENT.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MAIL_BOX_CONTENT.GetEvTableNbr: Integer;
begin
  Result := 17;
end;

class function TSB_MAIL_BOX_CONTENT.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MAIL_BOX_CONTENT.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MAIL_BOX_CONTENT.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 481
  else if AFieldName = 'SB_MAIL_BOX_CONTENT_NBR' then Result := 255
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 252
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 482
  else if AFieldName = 'SB_MAIL_BOX_NBR' then Result := 254
  else if AFieldName = 'DESCRIPTION' then Result := 257
  else if AFieldName = 'FILE_NAME' then Result := 256
  else if AFieldName = 'MEDIA_TYPE' then Result := 258
  else if AFieldName = 'PAGE_COUNT' then Result := 253
  else if AFieldName = 'USERSIDE_PRINTED' then Result := 649
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MAIL_BOX_CONTENT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MAIL_BOX_CONTENT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'SB_MAIL_BOX_CONTENT_NBR';
  Result[1] := 'SB_MAIL_BOX_NBR';
  Result[2] := 'DESCRIPTION';
  Result[3] := 'FILE_NAME';
  Result[4] := 'MEDIA_TYPE';
  Result[5] := 'PAGE_COUNT';
end;

class function TSB_MAIL_BOX_CONTENT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 512
  else if FieldName = 'FILE_NAME' then Result := 80
  else if FieldName = 'MEDIA_TYPE' then Result := 2
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MAIL_BOX_CONTENT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MAIL_BOX_CONTENT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MAIL_BOX_CONTENT_NBR';
end;

class function TSB_MAIL_BOX_CONTENT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_MAIL_BOX_CONTENT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX_OPTION;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_CONTENT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_CONTENT_NBR';
end;

class function TSB_MAIL_BOX_CONTENT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_MAIL_BOX_OPTION}

class function TSB_MAIL_BOX_OPTION.GetClassIndex: Integer;
begin
  Result := 168;
end;

class function TSB_MAIL_BOX_OPTION.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MAIL_BOX_OPTION.GetEvTableNbr: Integer;
begin
  Result := 18;
end;

class function TSB_MAIL_BOX_OPTION.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MAIL_BOX_OPTION.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MAIL_BOX_OPTION.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 483
  else if AFieldName = 'SB_MAIL_BOX_OPTION_NBR' then Result := 263
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 259
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 484
  else if AFieldName = 'SB_MAIL_BOX_NBR' then Result := 262
  else if AFieldName = 'SB_MAIL_BOX_CONTENT_NBR' then Result := 261
  else if AFieldName = 'OPTION_TAG' then Result := 265
  else if AFieldName = 'OPTION_INTEGER_VALUE' then Result := 260
  else if AFieldName = 'OPTION_STRING_VALUE' then Result := 264
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MAIL_BOX_OPTION.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MAIL_BOX_OPTION.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_MAIL_BOX_OPTION_NBR';
  Result[1] := 'SB_MAIL_BOX_NBR';
  Result[2] := 'OPTION_TAG';
end;

class function TSB_MAIL_BOX_OPTION.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'OPTION_TAG' then Result := 80
  else if FieldName = 'OPTION_STRING_VALUE' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MAIL_BOX_OPTION.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MAIL_BOX_OPTION.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MAIL_BOX_OPTION_NBR';
end;

class function TSB_MAIL_BOX_OPTION.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX_CONTENT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MAIL_BOX_CONTENT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MAIL_BOX_CONTENT_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_MAIL_BOX_OPTION.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_MAIL_BOX_OPTION.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_MEDIA_TYPE}

class function TSB_MEDIA_TYPE.GetClassIndex: Integer;
begin
  Result := 169;
end;

class function TSB_MEDIA_TYPE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MEDIA_TYPE.GetEvTableNbr: Integer;
begin
  Result := 19;
end;

class function TSB_MEDIA_TYPE.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MEDIA_TYPE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MEDIA_TYPE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 485
  else if AFieldName = 'SB_MEDIA_TYPE_NBR' then Result := 268
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 266
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 486
  else if AFieldName = 'SY_MEDIA_TYPE_NBR' then Result := 267
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MEDIA_TYPE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MEDIA_TYPE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_MEDIA_TYPE_NBR';
  Result[1] := 'SY_MEDIA_TYPE_NBR';
end;

class function TSB_MEDIA_TYPE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MEDIA_TYPE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MEDIA_TYPE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MEDIA_TYPE_NBR';
end;

class function TSB_MEDIA_TYPE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_MEDIA_TYPE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_MEDIA_TYPE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_MEDIA_TYPE_NBR';
end;

class function TSB_MEDIA_TYPE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MEDIA_TYPE_OPTION;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MEDIA_TYPE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MEDIA_TYPE_NBR';
end;

class function TSB_MEDIA_TYPE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_MEDIA_TYPE_NBR';
end;

{TSB_MEDIA_TYPE_OPTION}

class function TSB_MEDIA_TYPE_OPTION.GetClassIndex: Integer;
begin
  Result := 170;
end;

class function TSB_MEDIA_TYPE_OPTION.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MEDIA_TYPE_OPTION.GetEvTableNbr: Integer;
begin
  Result := 20;
end;

class function TSB_MEDIA_TYPE_OPTION.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MEDIA_TYPE_OPTION.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MEDIA_TYPE_OPTION.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 487
  else if AFieldName = 'SB_MEDIA_TYPE_OPTION_NBR' then Result := 272
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 269
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 488
  else if AFieldName = 'SB_MEDIA_TYPE_NBR' then Result := 271
  else if AFieldName = 'OPTION_TAG' then Result := 274
  else if AFieldName = 'OPTION_INTEGER_VALUE' then Result := 270
  else if AFieldName = 'OPTION_STRING_VALUE' then Result := 273
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MEDIA_TYPE_OPTION.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MEDIA_TYPE_OPTION.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_MEDIA_TYPE_OPTION_NBR';
  Result[1] := 'SB_MEDIA_TYPE_NBR';
  Result[2] := 'OPTION_TAG';
end;

class function TSB_MEDIA_TYPE_OPTION.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'OPTION_TAG' then Result := 40
  else if FieldName = 'OPTION_STRING_VALUE' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MEDIA_TYPE_OPTION.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MEDIA_TYPE_OPTION.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MEDIA_TYPE_OPTION_NBR';
end;

class function TSB_MEDIA_TYPE_OPTION.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MEDIA_TYPE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].DestFields[0] := 'SB_MEDIA_TYPE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_MEDIA_TYPE_OPTION.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_MEDIA_TYPE_OPTION.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'OPTION_TAG';
  Result[1] := 'SB_MEDIA_TYPE_NBR';
end;

{TSB_MULTICLIENT_REPORTS}

class function TSB_MULTICLIENT_REPORTS.GetClassIndex: Integer;
begin
  Result := 171;
end;

class function TSB_MULTICLIENT_REPORTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_MULTICLIENT_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 21;
end;

class function TSB_MULTICLIENT_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_MULTICLIENT_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_MULTICLIENT_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 489
  else if AFieldName = 'SB_MULTICLIENT_REPORTS_NBR' then Result := 278
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 275
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 490
  else if AFieldName = 'DESCRIPTION' then Result := 279
  else if AFieldName = 'REPORT_LEVEL' then Result := 280
  else if AFieldName = 'REPORT_NBR' then Result := 277
  else if AFieldName = 'INPUT_PARAMS' then Result := 276
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_MULTICLIENT_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_MULTICLIENT_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_MULTICLIENT_REPORTS_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'REPORT_LEVEL';
  Result[3] := 'REPORT_NBR';
end;

class function TSB_MULTICLIENT_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else if FieldName = 'REPORT_LEVEL' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_MULTICLIENT_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_MULTICLIENT_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_MULTICLIENT_REPORTS_NBR';
end;

class function TSB_MULTICLIENT_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_MULTICLIENT_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_MULTICLIENT_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSB_OPTION}

class function TSB_OPTION.GetClassIndex: Integer;
begin
  Result := 172;
end;

class function TSB_OPTION.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_OPTION.GetEvTableNbr: Integer;
begin
  Result := 22;
end;

class function TSB_OPTION.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_OPTION.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_OPTION.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 491
  else if AFieldName = 'SB_OPTION_NBR' then Result := 283
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 281
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 492
  else if AFieldName = 'OPTION_TAG' then Result := 285
  else if AFieldName = 'OPTION_INTEGER_VALUE' then Result := 282
  else if AFieldName = 'OPTION_STRING_VALUE' then Result := 284
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_OPTION.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_OPTION.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_OPTION_NBR';
  Result[1] := 'OPTION_TAG';
end;

class function TSB_OPTION.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'OPTION_TAG' then Result := 80
  else if FieldName = 'OPTION_STRING_VALUE' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_OPTION.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_OPTION.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_OPTION_NBR';
end;

class function TSB_OPTION.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_OPTION.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_OPTION.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'OPTION_TAG';
end;

{TSB_OTHER_SERVICE}

class function TSB_OTHER_SERVICE.GetClassIndex: Integer;
begin
  Result := 173;
end;

class function TSB_OTHER_SERVICE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_OTHER_SERVICE.GetEvTableNbr: Integer;
begin
  Result := 23;
end;

class function TSB_OTHER_SERVICE.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_OTHER_SERVICE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_OTHER_SERVICE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 493
  else if AFieldName = 'SB_OTHER_SERVICE_NBR' then Result := 287
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 286
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 494
  else if AFieldName = 'NAME' then Result := 288
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_OTHER_SERVICE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_OTHER_SERVICE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_OTHER_SERVICE_NBR';
  Result[1] := 'NAME';
end;

class function TSB_OTHER_SERVICE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 60
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_OTHER_SERVICE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_OTHER_SERVICE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_OTHER_SERVICE_NBR';
end;

class function TSB_OTHER_SERVICE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_OTHER_SERVICE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_OTHER_SERVICE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_PAPER_INFO}

class function TSB_PAPER_INFO.GetClassIndex: Integer;
begin
  Result := 174;
end;

class function TSB_PAPER_INFO.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_PAPER_INFO.GetEvTableNbr: Integer;
begin
  Result := 24;
end;

class function TSB_PAPER_INFO.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_PAPER_INFO.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_PAPER_INFO.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 495
  else if AFieldName = 'SB_PAPER_INFO_NBR' then Result := 293
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 289
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 496
  else if AFieldName = 'DESCRIPTION' then Result := 294
  else if AFieldName = 'HEIGHT' then Result := 292
  else if AFieldName = 'WIDTH' then Result := 291
  else if AFieldName = 'WEIGHT' then Result := 290
  else if AFieldName = 'MEDIA_TYPE' then Result := 295
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_PAPER_INFO.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_PAPER_INFO.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'SB_PAPER_INFO_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'HEIGHT';
  Result[3] := 'WIDTH';
  Result[4] := 'WEIGHT';
  Result[5] := 'MEDIA_TYPE';
end;

class function TSB_PAPER_INFO.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else if FieldName = 'MEDIA_TYPE' then Result := 2
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_PAPER_INFO.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'HEIGHT' then Result := 6
  else if FieldName = 'WIDTH' then Result := 6
  else if FieldName = 'WEIGHT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_PAPER_INFO.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_PAPER_INFO_NBR';
end;

class function TSB_PAPER_INFO.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_PAPER_INFO.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_PAPER_INFO.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSB_QUEUE_PRIORITY}

class function TSB_QUEUE_PRIORITY.GetClassIndex: Integer;
begin
  Result := 175;
end;

class function TSB_QUEUE_PRIORITY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_QUEUE_PRIORITY.GetEvTableNbr: Integer;
begin
  Result := 25;
end;

class function TSB_QUEUE_PRIORITY.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_QUEUE_PRIORITY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_QUEUE_PRIORITY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 497
  else if AFieldName = 'SB_QUEUE_PRIORITY_NBR' then Result := 297
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 296
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 498
  else if AFieldName = 'THREADS' then Result := 298
  else if AFieldName = 'METHOD_NAME' then Result := 302
  else if AFieldName = 'PRIORITY' then Result := 299
  else if AFieldName = 'SB_USER_NBR' then Result := 300
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 301
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_QUEUE_PRIORITY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_QUEUE_PRIORITY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_QUEUE_PRIORITY_NBR';
  Result[1] := 'THREADS';
  Result[2] := 'METHOD_NAME';
  Result[3] := 'PRIORITY';
end;

class function TSB_QUEUE_PRIORITY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'METHOD_NAME' then Result := 255
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_QUEUE_PRIORITY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_QUEUE_PRIORITY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_QUEUE_PRIORITY_NBR';
end;

class function TSB_QUEUE_PRIORITY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_QUEUE_PRIORITY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_QUEUE_PRIORITY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_REFERRALS}

class function TSB_REFERRALS.GetClassIndex: Integer;
begin
  Result := 176;
end;

class function TSB_REFERRALS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_REFERRALS.GetEvTableNbr: Integer;
begin
  Result := 26;
end;

class function TSB_REFERRALS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_REFERRALS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_REFERRALS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 499
  else if AFieldName = 'SB_REFERRALS_NBR' then Result := 304
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 303
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 500
  else if AFieldName = 'NAME' then Result := 305
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_REFERRALS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_REFERRALS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_REFERRALS_NBR';
  Result[1] := 'NAME';
end;

class function TSB_REFERRALS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_REFERRALS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_REFERRALS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_REFERRALS_NBR';
end;

class function TSB_REFERRALS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_REFERRALS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_REFERRALS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_REPORTS}

class function TSB_REPORTS.GetClassIndex: Integer;
begin
  Result := 177;
end;

class function TSB_REPORTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 27;
end;

class function TSB_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 501
  else if AFieldName = 'SB_REPORTS_NBR' then Result := 310
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 308
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 502
  else if AFieldName = 'DESCRIPTION' then Result := 311
  else if AFieldName = 'COMMENTS' then Result := 307
  else if AFieldName = 'REPORT_WRITER_REPORTS_NBR' then Result := 309
  else if AFieldName = 'REPORT_LEVEL' then Result := 312
  else if AFieldName = 'INPUT_PARAMS' then Result := 306
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_REPORTS_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'REPORT_WRITER_REPORTS_NBR';
  Result[3] := 'REPORT_LEVEL';
end;

class function TSB_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else if FieldName = 'REPORT_LEVEL' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_REPORTS_NBR';
end;

class function TSB_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_AGENCY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SERVICES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_REPORTS_NBR';
end;

class function TSB_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSB_REPORT_WRITER_REPORTS}

class function TSB_REPORT_WRITER_REPORTS.GetClassIndex: Integer;
begin
  Result := 178;
end;

class function TSB_REPORT_WRITER_REPORTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_REPORT_WRITER_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 30;
end;

class function TSB_REPORT_WRITER_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_REPORT_WRITER_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_REPORT_WRITER_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 507
  else if AFieldName = 'SB_REPORT_WRITER_REPORTS_NBR' then Result := 323
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 322
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 508
  else if AFieldName = 'REPORT_DESCRIPTION' then Result := 326
  else if AFieldName = 'REPORT_TYPE' then Result := 328
  else if AFieldName = 'REPORT_FILE' then Result := 321
  else if AFieldName = 'NOTES' then Result := 320
  else if AFieldName = 'MEDIA_TYPE' then Result := 327
  else if AFieldName = 'CLASS_NAME' then Result := 325
  else if AFieldName = 'ANCESTOR_CLASS_NAME' then Result := 324
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_REPORT_WRITER_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_REPORT_WRITER_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_REPORT_WRITER_REPORTS_NBR';
  Result[1] := 'REPORT_DESCRIPTION';
  Result[2] := 'REPORT_TYPE';
  Result[3] := 'MEDIA_TYPE';
end;

class function TSB_REPORT_WRITER_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'REPORT_DESCRIPTION' then Result := 40
  else if FieldName = 'REPORT_TYPE' then Result := 1
  else if FieldName = 'MEDIA_TYPE' then Result := 2
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'ANCESTOR_CLASS_NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_REPORT_WRITER_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_REPORT_WRITER_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_REPORT_WRITER_REPORTS_NBR';
end;

class function TSB_REPORT_WRITER_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_REPORT_WRITER_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_MAIL_BOX;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_COVER_LETTER_REPORT_NBR';
end;

class function TSB_REPORT_WRITER_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'REPORT_DESCRIPTION';
end;

{TSB_SALES_TAX_STATES}

class function TSB_SALES_TAX_STATES.GetClassIndex: Integer;
begin
  Result := 179;
end;

class function TSB_SALES_TAX_STATES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SALES_TAX_STATES.GetEvTableNbr: Integer;
begin
  Result := 31;
end;

class function TSB_SALES_TAX_STATES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SALES_TAX_STATES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SALES_TAX_STATES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 509
  else if AFieldName = 'SB_SALES_TAX_STATES_NBR' then Result := 331
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 329
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 510
  else if AFieldName = 'STATE' then Result := 332
  else if AFieldName = 'STATE_TAX_ID' then Result := 333
  else if AFieldName = 'SALES_TAX_PERCENTAGE' then Result := 330
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SALES_TAX_STATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SALES_TAX_STATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_SALES_TAX_STATES_NBR';
  Result[1] := 'STATE';
  Result[2] := 'STATE_TAX_ID';
end;

class function TSB_SALES_TAX_STATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'STATE_TAX_ID' then Result := 19
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SALES_TAX_STATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SALES_TAX_PERCENTAGE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SALES_TAX_STATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SALES_TAX_STATES_NBR';
end;

class function TSB_SALES_TAX_STATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_SALES_TAX_STATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SALES_TAX_STATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'STATE';
end;

{TSB_SEC_CLIENTS}

class function TSB_SEC_CLIENTS.GetClassIndex: Integer;
begin
  Result := 180;
end;

class function TSB_SEC_CLIENTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_CLIENTS.GetEvTableNbr: Integer;
begin
  Result := 32;
end;

class function TSB_SEC_CLIENTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_CLIENTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_CLIENTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 511
  else if AFieldName = 'SB_SEC_CLIENTS_NBR' then Result := 338
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 334
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 512
  else if AFieldName = 'CL_NBR' then Result := 337
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 336
  else if AFieldName = 'SB_USER_NBR' then Result := 335
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_CLIENTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_CLIENTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_SEC_CLIENTS_NBR';
  Result[1] := 'CL_NBR';
end;

class function TSB_SEC_CLIENTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_CLIENTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_CLIENTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_CLIENTS_NBR';
end;

class function TSB_SEC_CLIENTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SEC_CLIENTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SEC_CLIENTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_SEC_GROUPS}

class function TSB_SEC_GROUPS.GetClassIndex: Integer;
begin
  Result := 181;
end;

class function TSB_SEC_GROUPS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_GROUPS.GetEvTableNbr: Integer;
begin
  Result := 33;
end;

class function TSB_SEC_GROUPS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_GROUPS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_GROUPS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 513
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 340
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 339
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 514
  else if AFieldName = 'NAME' then Result := 341
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_GROUPS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_GROUPS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_SEC_GROUPS_NBR';
  Result[1] := 'NAME';
end;

class function TSB_SEC_GROUPS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_GROUPS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_GROUPS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_GROUPS_NBR';
end;

class function TSB_SEC_GROUPS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_SEC_GROUPS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_QUEUE_PRIORITY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_CLIENTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUP_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_RIGHTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_ROW_FILTERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
end;

class function TSB_SEC_GROUPS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_SEC_GROUP_MEMBERS}

class function TSB_SEC_GROUP_MEMBERS.GetClassIndex: Integer;
begin
  Result := 182;
end;

class function TSB_SEC_GROUP_MEMBERS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_GROUP_MEMBERS.GetEvTableNbr: Integer;
begin
  Result := 34;
end;

class function TSB_SEC_GROUP_MEMBERS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_GROUP_MEMBERS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_GROUP_MEMBERS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 515
  else if AFieldName = 'SB_SEC_GROUP_MEMBERS_NBR' then Result := 345
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 342
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 516
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 344
  else if AFieldName = 'SB_USER_NBR' then Result := 343
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_GROUP_MEMBERS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_GROUP_MEMBERS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_SEC_GROUP_MEMBERS_NBR';
  Result[1] := 'SB_SEC_GROUPS_NBR';
  Result[2] := 'SB_USER_NBR';
end;

class function TSB_SEC_GROUP_MEMBERS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_GROUP_MEMBERS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_GROUP_MEMBERS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_GROUP_MEMBERS_NBR';
end;

class function TSB_SEC_GROUP_MEMBERS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SEC_GROUP_MEMBERS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SEC_GROUP_MEMBERS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_SEC_GROUPS_NBR';
  Result[1] := 'SB_USER_NBR';
end;

{TSB_SEC_RIGHTS}

class function TSB_SEC_RIGHTS.GetClassIndex: Integer;
begin
  Result := 183;
end;

class function TSB_SEC_RIGHTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_RIGHTS.GetEvTableNbr: Integer;
begin
  Result := 35;
end;

class function TSB_SEC_RIGHTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_RIGHTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_RIGHTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 517
  else if AFieldName = 'SB_SEC_RIGHTS_NBR' then Result := 349
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 346
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 518
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 348
  else if AFieldName = 'SB_USER_NBR' then Result := 347
  else if AFieldName = 'TAG' then Result := 351
  else if AFieldName = 'CONTEXT' then Result := 350
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_RIGHTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_RIGHTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_SEC_RIGHTS_NBR';
  Result[1] := 'TAG';
  Result[2] := 'CONTEXT';
end;

class function TSB_SEC_RIGHTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TAG' then Result := 128
  else if FieldName = 'CONTEXT' then Result := 128
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_RIGHTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_RIGHTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_RIGHTS_NBR';
end;

class function TSB_SEC_RIGHTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SEC_RIGHTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SEC_RIGHTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_SEC_ROW_FILTERS}

class function TSB_SEC_ROW_FILTERS.GetClassIndex: Integer;
begin
  Result := 184;
end;

class function TSB_SEC_ROW_FILTERS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_ROW_FILTERS.GetEvTableNbr: Integer;
begin
  Result := 36;
end;

class function TSB_SEC_ROW_FILTERS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_ROW_FILTERS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_ROW_FILTERS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 519
  else if AFieldName = 'SB_SEC_ROW_FILTERS_NBR' then Result := 354
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 352
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 520
  else if AFieldName = 'SB_SEC_GROUPS_NBR' then Result := 355
  else if AFieldName = 'SB_USER_NBR' then Result := 356
  else if AFieldName = 'DATABASE_TYPE' then Result := 358
  else if AFieldName = 'TABLE_NAME' then Result := 357
  else if AFieldName = 'FILTER_TYPE' then Result := 359
  else if AFieldName = 'CUSTOM_EXPR' then Result := 360
  else if AFieldName = 'CL_NBR' then Result := 353
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_ROW_FILTERS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_ROW_FILTERS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_SEC_ROW_FILTERS_NBR';
  Result[1] := 'DATABASE_TYPE';
  Result[2] := 'TABLE_NAME';
  Result[3] := 'FILTER_TYPE';
end;

class function TSB_SEC_ROW_FILTERS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DATABASE_TYPE' then Result := 1
  else if FieldName = 'TABLE_NAME' then Result := 40
  else if FieldName = 'FILTER_TYPE' then Result := 1
  else if FieldName = 'CUSTOM_EXPR' then Result := 255
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_ROW_FILTERS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_ROW_FILTERS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_ROW_FILTERS_NBR';
end;

class function TSB_SEC_ROW_FILTERS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SEC_GROUPS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SEC_ROW_FILTERS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SEC_ROW_FILTERS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_SEC_TEMPLATES}

class function TSB_SEC_TEMPLATES.GetClassIndex: Integer;
begin
  Result := 185;
end;

class function TSB_SEC_TEMPLATES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SEC_TEMPLATES.GetEvTableNbr: Integer;
begin
  Result := 37;
end;

class function TSB_SEC_TEMPLATES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SEC_TEMPLATES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SEC_TEMPLATES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 521
  else if AFieldName = 'SB_SEC_TEMPLATES_NBR' then Result := 363
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 362
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 522
  else if AFieldName = 'NAME' then Result := 364
  else if AFieldName = 'TEMPLATE' then Result := 361
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SEC_TEMPLATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SEC_TEMPLATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_SEC_TEMPLATES_NBR';
  Result[1] := 'NAME';
  Result[2] := 'TEMPLATE';
end;

class function TSB_SEC_TEMPLATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SEC_TEMPLATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SEC_TEMPLATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SEC_TEMPLATES_NBR';
end;

class function TSB_SEC_TEMPLATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_SEC_TEMPLATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SEC_TEMPLATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_SERVICES}

class function TSB_SERVICES.GetClassIndex: Integer;
begin
  Result := 186;
end;

class function TSB_SERVICES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SERVICES.GetEvTableNbr: Integer;
begin
  Result := 38;
end;

class function TSB_SERVICES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SERVICES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SERVICES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 523
  else if AFieldName = 'SB_SERVICES_NBR' then Result := 371
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 367
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 524
  else if AFieldName = 'SERVICE_NAME' then Result := 374
  else if AFieldName = 'FREQUENCY' then Result := 377
  else if AFieldName = 'MONTH_NUMBER' then Result := 372
  else if AFieldName = 'BASED_ON_TYPE' then Result := 378
  else if AFieldName = 'SB_REPORTS_NBR' then Result := 373
  else if AFieldName = 'COMMISSION' then Result := 380
  else if AFieldName = 'SALES_TAXABLE' then Result := 381
  else if AFieldName = 'FILLER' then Result := 375
  else if AFieldName = 'PRODUCT_CODE' then Result := 366
  else if AFieldName = 'WEEK_NUMBER' then Result := 382
  else if AFieldName = 'SERVICE_TYPE' then Result := 383
  else if AFieldName = 'MINIMUM_AMOUNT' then Result := 368
  else if AFieldName = 'MAXIMUM_AMOUNT' then Result := 369
  else if AFieldName = 'SB_DELIVERY_METHOD_NBR' then Result := 370
  else if AFieldName = 'TAX_TYPE' then Result := 376
  else if AFieldName = 'PARTNER_BILLING' then Result := 570
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SERVICES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SERVICES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 10);
  Result[0] := 'SB_SERVICES_NBR';
  Result[1] := 'SERVICE_NAME';
  Result[2] := 'FREQUENCY';
  Result[3] := 'BASED_ON_TYPE';
  Result[4] := 'COMMISSION';
  Result[5] := 'SALES_TAXABLE';
  Result[6] := 'WEEK_NUMBER';
  Result[7] := 'SERVICE_TYPE';
  Result[8] := 'TAX_TYPE';
  Result[9] := 'PARTNER_BILLING';
end;

class function TSB_SERVICES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SERVICE_NAME' then Result := 40
  else if FieldName = 'FREQUENCY' then Result := 1
  else if FieldName = 'MONTH_NUMBER' then Result := 2
  else if FieldName = 'BASED_ON_TYPE' then Result := 2
  else if FieldName = 'COMMISSION' then Result := 1
  else if FieldName = 'SALES_TAXABLE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'PRODUCT_CODE' then Result := 10
  else if FieldName = 'WEEK_NUMBER' then Result := 1
  else if FieldName = 'SERVICE_TYPE' then Result := 1
  else if FieldName = 'TAX_TYPE' then Result := 1
  else if FieldName = 'PARTNER_BILLING' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SERVICES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MINIMUM_AMOUNT' then Result := 6
  else if FieldName = 'MAXIMUM_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SERVICES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SERVICES_NBR';
end;

class function TSB_SERVICES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SB_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_DELIVERY_METHOD;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].DestFields[0] := 'SB_DELIVERY_METHOD_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SERVICES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SERVICES_CALCULATIONS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SERVICES_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SERVICES_NBR';
end;

class function TSB_SERVICES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SERVICE_NAME';
end;

{TSB_SERVICES_CALCULATIONS}

class function TSB_SERVICES_CALCULATIONS.GetClassIndex: Integer;
begin
  Result := 187;
end;

class function TSB_SERVICES_CALCULATIONS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_SERVICES_CALCULATIONS.GetEvTableNbr: Integer;
begin
  Result := 39;
end;

class function TSB_SERVICES_CALCULATIONS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_SERVICES_CALCULATIONS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_SERVICES_CALCULATIONS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 525
  else if AFieldName = 'SB_SERVICES_CALCULATIONS_NBR' then Result := 397
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 388
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 526
  else if AFieldName = 'SB_SERVICES_NBR' then Result := 398
  else if AFieldName = 'NEXT_MIN_QUANTITY_BEGIN_DATE' then Result := 384
  else if AFieldName = 'NEXT_MAX_QUANTITY_BEGIN_DATE' then Result := 385
  else if AFieldName = 'NEXT_PER_ITEM_BEGIN_DATE' then Result := 386
  else if AFieldName = 'NEXT_FLAT_AMOUNT_BEGIN_DATE' then Result := 387
  else if AFieldName = 'MINIMUM_QUANTITY' then Result := 389
  else if AFieldName = 'NEXT_MINIMUM_QUANTITY' then Result := 390
  else if AFieldName = 'MAXIMUM_QUANTITY' then Result := 391
  else if AFieldName = 'NEXT_MAXIMUM_QUANTITY' then Result := 392
  else if AFieldName = 'PER_ITEM_RATE' then Result := 393
  else if AFieldName = 'NEXT_PER_ITEM_RATE' then Result := 394
  else if AFieldName = 'FLAT_AMOUNT' then Result := 395
  else if AFieldName = 'NEXT_FLAT_AMOUNT' then Result := 396
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_SERVICES_CALCULATIONS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_SERVICES_CALCULATIONS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_SERVICES_CALCULATIONS_NBR';
  Result[1] := 'SB_SERVICES_NBR';
end;

class function TSB_SERVICES_CALCULATIONS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_SERVICES_CALCULATIONS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MINIMUM_QUANTITY' then Result := 6
  else if FieldName = 'NEXT_MINIMUM_QUANTITY' then Result := 6
  else if FieldName = 'MAXIMUM_QUANTITY' then Result := 6
  else if FieldName = 'NEXT_MAXIMUM_QUANTITY' then Result := 6
  else if FieldName = 'PER_ITEM_RATE' then Result := 6
  else if FieldName = 'NEXT_PER_ITEM_RATE' then Result := 6
  else if FieldName = 'FLAT_AMOUNT' then Result := 6
  else if FieldName = 'NEXT_FLAT_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_SERVICES_CALCULATIONS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_SERVICES_CALCULATIONS_NBR';
end;

class function TSB_SERVICES_CALCULATIONS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SERVICES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_SERVICES_NBR';
  Result[High(Result)].DestFields[0] := 'SB_SERVICES_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_SERVICES_CALCULATIONS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_SERVICES_CALCULATIONS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_TASK}

class function TSB_TASK.GetClassIndex: Integer;
begin
  Result := 188;
end;

class function TSB_TASK.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_TASK.GetEvTableNbr: Integer;
begin
  Result := 40;
end;

class function TSB_TASK.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_TASK.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_TASK.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 527
  else if AFieldName = 'SB_TASK_NBR' then Result := 401
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 400
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 528
  else if AFieldName = 'SB_USER_NBR' then Result := 402
  else if AFieldName = 'SCHEDULE' then Result := 403
  else if AFieldName = 'DESCRIPTION' then Result := 404
  else if AFieldName = 'TASK' then Result := 399
  else if AFieldName = 'LAST_RUN' then Result := 552
  else if AFieldName = 'NOTES' then Result := 567
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_TASK.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_TASK.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 5);
  Result[0] := 'SB_TASK_NBR';
  Result[1] := 'SB_USER_NBR';
  Result[2] := 'SCHEDULE';
  Result[3] := 'DESCRIPTION';
  Result[4] := 'TASK';
end;

class function TSB_TASK.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SCHEDULE' then Result := 255
  else if FieldName = 'DESCRIPTION' then Result := 255
  else if FieldName = 'NOTES' then Result := 255
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_TASK.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_TASK.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_TASK_NBR';
end;

class function TSB_TASK.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_TASK.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_TASK.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_TAX_PAYMENT}

class function TSB_TAX_PAYMENT.GetClassIndex: Integer;
begin
  Result := 189;
end;

class function TSB_TAX_PAYMENT.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_TAX_PAYMENT.GetEvTableNbr: Integer;
begin
  Result := 41;
end;

class function TSB_TAX_PAYMENT.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_TAX_PAYMENT.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_TAX_PAYMENT.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 529
  else if AFieldName = 'SB_TAX_PAYMENT_NBR' then Result := 407
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 405
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 530
  else if AFieldName = 'DESCRIPTION' then Result := 408
  else if AFieldName = 'STATUS' then Result := 409
  else if AFieldName = 'STATUS_DATE' then Result := 406
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_TAX_PAYMENT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_TAX_PAYMENT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_TAX_PAYMENT_NBR';
  Result[1] := 'STATUS';
end;

class function TSB_TAX_PAYMENT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 80
  else if FieldName = 'STATUS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_TAX_PAYMENT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_TAX_PAYMENT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_TAX_PAYMENT_NBR';
end;

class function TSB_TAX_PAYMENT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_TAX_PAYMENT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_TAX_PAYMENT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_TEAM}

class function TSB_TEAM.GetClassIndex: Integer;
begin
  Result := 190;
end;

class function TSB_TEAM.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_TEAM.GetEvTableNbr: Integer;
begin
  Result := 42;
end;

class function TSB_TEAM.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_TEAM.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_TEAM.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 531
  else if AFieldName = 'SB_TEAM_NBR' then Result := 411
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 410
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 532
  else if AFieldName = 'TEAM_DESCRIPTION' then Result := 412
  else if AFieldName = 'CR_CATEGORY' then Result := 413
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_TEAM.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_TEAM.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_TEAM_NBR';
  Result[1] := 'TEAM_DESCRIPTION';
  Result[2] := 'CR_CATEGORY';
end;

class function TSB_TEAM.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TEAM_DESCRIPTION' then Result := 40
  else if FieldName = 'CR_CATEGORY' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_TEAM.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_TEAM.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_TEAM_NBR';
end;

class function TSB_TEAM.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_TEAM.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_TEAM_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_TEAM_NBR';
  Result[High(Result)].DestFields[0] := 'SB_TEAM_NBR';
end;

class function TSB_TEAM.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'TEAM_DESCRIPTION';
end;

{TSB_TEAM_MEMBERS}

class function TSB_TEAM_MEMBERS.GetClassIndex: Integer;
begin
  Result := 191;
end;

class function TSB_TEAM_MEMBERS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_TEAM_MEMBERS.GetEvTableNbr: Integer;
begin
  Result := 43;
end;

class function TSB_TEAM_MEMBERS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_TEAM_MEMBERS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_TEAM_MEMBERS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 533
  else if AFieldName = 'SB_TEAM_MEMBERS_NBR' then Result := 415
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 414
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 534
  else if AFieldName = 'SB_TEAM_NBR' then Result := 416
  else if AFieldName = 'SB_USER_NBR' then Result := 417
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_TEAM_MEMBERS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_TEAM_MEMBERS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_TEAM_MEMBERS_NBR';
  Result[1] := 'SB_TEAM_NBR';
  Result[2] := 'SB_USER_NBR';
end;

class function TSB_TEAM_MEMBERS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_TEAM_MEMBERS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_TEAM_MEMBERS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_TEAM_MEMBERS_NBR';
end;

class function TSB_TEAM_MEMBERS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_TEAM;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_TEAM_NBR';
  Result[High(Result)].DestFields[0] := 'SB_TEAM_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_TEAM_MEMBERS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_TEAM_MEMBERS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_TEAM_NBR';
  Result[1] := 'SB_USER_NBR';
end;

{TSB_USER}

class function TSB_USER.GetClassIndex: Integer;
begin
  Result := 192;
end;

class function TSB_USER.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSB_USER.GetEvTableNbr: Integer;
begin
  Result := 44;
end;

class function TSB_USER.GetEntityName: String;
begin
  Result := 'EvolutionUser';
end;

class function TSB_USER.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'EVOLUTION_PRODUCT' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_USER.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 535
  else if AFieldName = 'SB_USER_NBR' then Result := 428
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 422
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 536
  else if AFieldName = 'USER_ID' then Result := 435
  else if AFieldName = 'USER_SIGNATURE' then Result := 419
  else if AFieldName = 'LAST_NAME' then Result := 420
  else if AFieldName = 'FIRST_NAME' then Result := 436
  else if AFieldName = 'MIDDLE_INITIAL' then Result := 440
  else if AFieldName = 'ACTIVE_USER' then Result := 441
  else if AFieldName = 'DEPARTMENT' then Result := 442
  else if AFieldName = 'PASSWORD_CHANGE_DATE' then Result := 421
  else if AFieldName = 'SECURITY_LEVEL' then Result := 443
  else if AFieldName = 'USER_UPDATE_OPTIONS' then Result := 437
  else if AFieldName = 'USER_FUNCTIONS' then Result := 438
  else if AFieldName = 'USER_PASSWORD' then Result := 439
  else if AFieldName = 'EMAIL_ADDRESS' then Result := 434
  else if AFieldName = 'CL_NBR' then Result := 429
  else if AFieldName = 'SB_ACCOUNTANT_NBR' then Result := 427
  else if AFieldName = 'WRONG_PSWD_ATTEMPTS' then Result := 426
  else if AFieldName = 'LINKS_DATA' then Result := 433
  else if AFieldName = 'LOGIN_QUESTION1' then Result := 425
  else if AFieldName = 'LOGIN_ANSWER1' then Result := 432
  else if AFieldName = 'LOGIN_QUESTION2' then Result := 424
  else if AFieldName = 'LOGIN_ANSWER2' then Result := 431
  else if AFieldName = 'LOGIN_QUESTION3' then Result := 423
  else if AFieldName = 'LOGIN_ANSWER3' then Result := 430
  else if AFieldName = 'HR_PERSONNEL' then Result := 545
  else if AFieldName = 'SEC_QUESTION1' then Result := 546
  else if AFieldName = 'SEC_ANSWER1' then Result := 547
  else if AFieldName = 'SEC_QUESTION2' then Result := 548
  else if AFieldName = 'SEC_ANSWER2' then Result := 549
  else if AFieldName = 'WC_TOU_ACCEPT_DATE' then Result := 575
  else if AFieldName = 'ANALYTICS_PERSONNEL' then Result := 576
  else if AFieldName = 'EVOLUTION_PRODUCT' then Result := 621
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_USER.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else if FieldName = 'LAST_NAME' then Result := 'LastName'
  else if FieldName = 'FIRST_NAME' then Result := 'FirstName'
  else if FieldName = 'EMAIL_ADDRESS' then Result := 'Email'
  else if FieldName = 'HR_PERSONNEL' then Result := 'IsHR'
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_USER.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 13);
  Result[0] := 'SB_USER_NBR';
  Result[1] := 'USER_ID';
  Result[2] := 'LAST_NAME';
  Result[3] := 'FIRST_NAME';
  Result[4] := 'ACTIVE_USER';
  Result[5] := 'DEPARTMENT';
  Result[6] := 'PASSWORD_CHANGE_DATE';
  Result[7] := 'SECURITY_LEVEL';
  Result[8] := 'USER_UPDATE_OPTIONS';
  Result[9] := 'USER_FUNCTIONS';
  Result[10] := 'USER_PASSWORD';
  Result[11] := 'HR_PERSONNEL';
  Result[12] := 'ANALYTICS_PERSONNEL';
end;

class function TSB_USER.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'USER_ID' then Result := 128
  else if FieldName = 'LAST_NAME' then Result := 30
  else if FieldName = 'FIRST_NAME' then Result := 20
  else if FieldName = 'MIDDLE_INITIAL' then Result := 1
  else if FieldName = 'ACTIVE_USER' then Result := 1
  else if FieldName = 'DEPARTMENT' then Result := 1
  else if FieldName = 'SECURITY_LEVEL' then Result := 1
  else if FieldName = 'USER_UPDATE_OPTIONS' then Result := 512
  else if FieldName = 'USER_FUNCTIONS' then Result := 512
  else if FieldName = 'USER_PASSWORD' then Result := 32
  else if FieldName = 'EMAIL_ADDRESS' then Result := 80
  else if FieldName = 'LINKS_DATA' then Result := 128
  else if FieldName = 'LOGIN_ANSWER1' then Result := 80
  else if FieldName = 'LOGIN_ANSWER2' then Result := 80
  else if FieldName = 'LOGIN_ANSWER3' then Result := 80
  else if FieldName = 'HR_PERSONNEL' then Result := 1
  else if FieldName = 'SEC_ANSWER1' then Result := 80
  else if FieldName = 'SEC_ANSWER2' then Result := 80
  else if FieldName = 'ANALYTICS_PERSONNEL' then Result := 1
  else if FieldName = 'EVOLUTION_PRODUCT' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_USER.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_USER.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_USER_NBR';
end;

class function TSB_USER.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACCOUNTANT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ACCOUNTANT_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ACCOUNTANT_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_USER.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_QUEUE_PRIORITY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_CLIENTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_GROUP_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_RIGHTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_SEC_ROW_FILTERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_TEAM_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_TASK;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER_NOTICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER_PREFERENCES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
end;

class function TSB_USER.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'USER_ID';
end;

{TSB_USER_NOTICE}

class function TSB_USER_NOTICE.GetClassIndex: Integer;
begin
  Result := 193;
end;

class function TSB_USER_NOTICE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_USER_NOTICE.GetEvTableNbr: Integer;
begin
  Result := 45;
end;

class function TSB_USER_NOTICE.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_USER_NOTICE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_USER_NOTICE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 537
  else if AFieldName = 'SB_USER_NOTICE_NBR' then Result := 449
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 539
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 538
  else if AFieldName = 'SB_USER_NBR' then Result := 448
  else if AFieldName = 'NAME' then Result := 450
  else if AFieldName = 'NOTES' then Result := 447
  else if AFieldName = 'TASK' then Result := 446
  else if AFieldName = 'LAST_DISMISS' then Result := 445
  else if AFieldName = 'NEXT_REMINDER' then Result := 444
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_USER_NOTICE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_USER_NOTICE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_USER_NOTICE_NBR';
  Result[1] := 'SB_USER_NBR';
  Result[2] := 'NAME';
end;

class function TSB_USER_NOTICE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 128
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_USER_NOTICE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_USER_NOTICE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_USER_NOTICE_NBR';
end;

class function TSB_USER_NOTICE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_USER_NOTICE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_USER_NOTICE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSB_STORAGE}

class function TSB_STORAGE.GetClassIndex: Integer;
begin
  Result := 194;
end;

class function TSB_STORAGE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_STORAGE.GetEvTableNbr: Integer;
begin
  Result := 46;
end;

class function TSB_STORAGE.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_STORAGE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_STORAGE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 554
  else if AFieldName = 'SB_STORAGE_NBR' then Result := 555
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 556
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 557
  else if AFieldName = 'TAG' then Result := 558
  else if AFieldName = 'STORAGE_DATA' then Result := 559
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_STORAGE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_STORAGE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_STORAGE_NBR';
  Result[1] := 'TAG';
  Result[2] := 'STORAGE_DATA';
end;

class function TSB_STORAGE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TAG' then Result := 20
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_STORAGE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_STORAGE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_STORAGE_NBR';
end;

class function TSB_STORAGE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_STORAGE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_STORAGE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'TAG';
end;

{TSB_USER_MESSAGES}

class function TSB_USER_MESSAGES.GetClassIndex: Integer;
begin
  Result := 195;
end;

class function TSB_USER_MESSAGES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_USER_MESSAGES.GetEvTableNbr: Integer;
begin
  Result := 47;
end;

class function TSB_USER_MESSAGES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_USER_MESSAGES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ORDER_NUMBER' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_USER_MESSAGES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 560
  else if AFieldName = 'SB_USER_MESSAGES_NBR' then Result := 561
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 562
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 563
  else if AFieldName = 'USER_STEREOTYPE' then Result := 564
  else if AFieldName = 'ORDER_NUMBER' then Result := 565
  else if AFieldName = 'DATA' then Result := 566
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_USER_MESSAGES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_USER_MESSAGES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_USER_MESSAGES_NBR';
  Result[1] := 'USER_STEREOTYPE';
  Result[2] := 'DATA';
end;

class function TSB_USER_MESSAGES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'USER_STEREOTYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_USER_MESSAGES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_USER_MESSAGES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_USER_MESSAGES_NBR';
end;

class function TSB_USER_MESSAGES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_USER_MESSAGES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_USER_MESSAGES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_USER_PREFERENCES}

class function TSB_USER_PREFERENCES.GetClassIndex: Integer;
begin
  Result := 196;
end;

class function TSB_USER_PREFERENCES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_USER_PREFERENCES.GetEvTableNbr: Integer;
begin
  Result := 48;
end;

class function TSB_USER_PREFERENCES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_USER_PREFERENCES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_USER_PREFERENCES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 577
  else if AFieldName = 'SB_USER_PREFERENCES_NBR' then Result := 578
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 579
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 580
  else if AFieldName = 'SB_USER_NBR' then Result := 581
  else if AFieldName = 'PREFERENCE_TYPE' then Result := 582
  else if AFieldName = 'PREFERENCE_VALUE' then Result := 583
  else if AFieldName = 'LAST_MODIFIED_DATE' then Result := 584
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_USER_PREFERENCES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_USER_PREFERENCES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_USER_PREFERENCES_NBR';
  Result[1] := 'SB_USER_NBR';
end;

class function TSB_USER_PREFERENCES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'PREFERENCE_TYPE' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_USER_PREFERENCES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_USER_PREFERENCES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_USER_PREFERENCES_NBR';
end;

class function TSB_USER_PREFERENCES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_USER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_USER_NBR';
  Result[High(Result)].DestFields[0] := 'SB_USER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_USER_PREFERENCES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_USER_PREFERENCES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_DASHBOARDS}

class function TSB_DASHBOARDS.GetClassIndex: Integer;
begin
  Result := 197;
end;

class function TSB_DASHBOARDS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_DASHBOARDS.GetEvTableNbr: Integer;
begin
  Result := 49;
end;

class function TSB_DASHBOARDS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_DASHBOARDS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_DASHBOARDS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 590
  else if AFieldName = 'SB_DASHBOARDS_NBR' then Result := 591
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 592
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 593
  else if AFieldName = 'DASHBOARD_TYPE' then Result := 594
  else if AFieldName = 'SY_ANALYTICS_TIER_NBR' then Result := 595
  else if AFieldName = 'NOTES' then Result := 596
  else if AFieldName = 'DASHBOARD_ID' then Result := 597
  else if AFieldName = 'DESCRIPTION' then Result := 598
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_DASHBOARDS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_DASHBOARDS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_DASHBOARDS_NBR';
  Result[1] := 'SY_ANALYTICS_TIER_NBR';
  Result[2] := 'DASHBOARD_ID';
  Result[3] := 'DESCRIPTION';
end;

class function TSB_DASHBOARDS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DASHBOARD_TYPE' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 80
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_DASHBOARDS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_DASHBOARDS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_DASHBOARDS_NBR';
end;

class function TSB_DASHBOARDS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_DASHBOARDS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_DASHBOARDS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ENABLED_DASHBOARDS}

class function TSB_ENABLED_DASHBOARDS.GetClassIndex: Integer;
begin
  Result := 198;
end;

class function TSB_ENABLED_DASHBOARDS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ENABLED_DASHBOARDS.GetEvTableNbr: Integer;
begin
  Result := 50;
end;

class function TSB_ENABLED_DASHBOARDS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ENABLED_DASHBOARDS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ENABLED_DASHBOARDS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 599
  else if AFieldName = 'SB_ENABLED_DASHBOARDS_NBR' then Result := 600
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 601
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 602
  else if AFieldName = 'DASHBOARD_LEVEL' then Result := 603
  else if AFieldName = 'DASHBOARD_NBR' then Result := 604
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ENABLED_DASHBOARDS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ENABLED_DASHBOARDS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_ENABLED_DASHBOARDS_NBR';
  Result[1] := 'DASHBOARD_NBR';
end;

class function TSB_ENABLED_DASHBOARDS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DASHBOARD_LEVEL' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ENABLED_DASHBOARDS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ENABLED_DASHBOARDS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ENABLED_DASHBOARDS_NBR';
end;

class function TSB_ENABLED_DASHBOARDS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_ENABLED_DASHBOARDS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ENABLED_DASHBOARDS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ACA_GROUP}

class function TSB_ACA_GROUP.GetClassIndex: Integer;
begin
  Result := 199;
end;

class function TSB_ACA_GROUP.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ACA_GROUP.GetEvTableNbr: Integer;
begin
  Result := 51;
end;

class function TSB_ACA_GROUP.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ACA_GROUP.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ACA_GROUP.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 605
  else if AFieldName = 'SB_ACA_GROUP_NBR' then Result := 606
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 607
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 608
  else if AFieldName = 'ACA_GROUP_DESCRIPTION' then Result := 609
  else if AFieldName = 'PRIMARY_CL_NBR' then Result := 610
  else if AFieldName = 'PRIMARY_CO_NBR' then Result := 611
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ACA_GROUP.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ACA_GROUP.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_ACA_GROUP_NBR';
  Result[1] := 'ACA_GROUP_DESCRIPTION';
end;

class function TSB_ACA_GROUP.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ACA_GROUP_DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ACA_GROUP.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ACA_GROUP.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ACA_GROUP_NBR';
end;

class function TSB_ACA_GROUP.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_ACA_GROUP.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACA_GROUP_ADD_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ACA_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ACA_GROUP_NBR';
end;

class function TSB_ACA_GROUP.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_CUSTOM_VENDORS}

class function TSB_CUSTOM_VENDORS.GetClassIndex: Integer;
begin
  Result := 200;
end;

class function TSB_CUSTOM_VENDORS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_CUSTOM_VENDORS.GetEvTableNbr: Integer;
begin
  Result := 52;
end;

class function TSB_CUSTOM_VENDORS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_CUSTOM_VENDORS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_CUSTOM_VENDORS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 612
  else if AFieldName = 'SB_CUSTOM_VENDORS_NBR' then Result := 613
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 614
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 615
  else if AFieldName = 'VENDOR_NAME' then Result := 616
  else if AFieldName = 'SY_VENDOR_CATEGORIES_NBR' then Result := 617
  else if AFieldName = 'VENDOR_TYPE' then Result := 618
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_CUSTOM_VENDORS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_CUSTOM_VENDORS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_CUSTOM_VENDORS_NBR';
  Result[1] := 'VENDOR_NAME';
  Result[2] := 'SY_VENDOR_CATEGORIES_NBR';
end;

class function TSB_CUSTOM_VENDORS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'VENDOR_NAME' then Result := 80
  else if FieldName = 'VENDOR_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_CUSTOM_VENDORS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_CUSTOM_VENDORS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_CUSTOM_VENDORS_NBR';
end;

class function TSB_CUSTOM_VENDORS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_CUSTOM_VENDORS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_CUSTOM_VENDORS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_VENDOR}

class function TSB_VENDOR.GetClassIndex: Integer;
begin
  Result := 201;
end;

class function TSB_VENDOR.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_VENDOR.GetEvTableNbr: Integer;
begin
  Result := 53;
end;

class function TSB_VENDOR.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_VENDOR.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_VENDOR.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 622
  else if AFieldName = 'SB_VENDOR_NBR' then Result := 623
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 624
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 625
  else if AFieldName = 'VENDORS_LEVEL' then Result := 626
  else if AFieldName = 'VENDOR_NBR' then Result := 627
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_VENDOR.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_VENDOR.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SB_VENDOR_NBR';
  Result[1] := 'VENDOR_NBR';
end;

class function TSB_VENDOR.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'VENDORS_LEVEL' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_VENDOR.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_VENDOR.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_VENDOR_NBR';
end;

class function TSB_VENDOR.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_VENDOR.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_VENDOR_DETAIL;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_VENDOR_NBR';
  Result[High(Result)].DestFields[0] := 'SB_VENDOR_NBR';
end;

class function TSB_VENDOR.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_VENDOR_DETAIL}

class function TSB_VENDOR_DETAIL.GetClassIndex: Integer;
begin
  Result := 202;
end;

class function TSB_VENDOR_DETAIL.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_VENDOR_DETAIL.GetEvTableNbr: Integer;
begin
  Result := 54;
end;

class function TSB_VENDOR_DETAIL.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_VENDOR_DETAIL.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_VENDOR_DETAIL.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 628
  else if AFieldName = 'SB_VENDOR_DETAIL_NBR' then Result := 629
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 630
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 631
  else if AFieldName = 'SB_VENDOR_NBR' then Result := 632
  else if AFieldName = 'DETAIL' then Result := 633
  else if AFieldName = 'DETAIL_LEVEL' then Result := 634
  else if AFieldName = 'CO_DETAIL_REQUIRED' then Result := 635
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_VENDOR_DETAIL.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_VENDOR_DETAIL.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_VENDOR_DETAIL_NBR';
  Result[1] := 'SB_VENDOR_NBR';
  Result[2] := 'DETAIL';
end;

class function TSB_VENDOR_DETAIL.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DETAIL' then Result := 40
  else if FieldName = 'DETAIL_LEVEL' then Result := 1
  else if FieldName = 'CO_DETAIL_REQUIRED' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_VENDOR_DETAIL.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_VENDOR_DETAIL.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_VENDOR_DETAIL_NBR';
end;

class function TSB_VENDOR_DETAIL.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_VENDOR;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_VENDOR_NBR';
  Result[High(Result)].DestFields[0] := 'SB_VENDOR_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_VENDOR_DETAIL.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_VENDOR_DETAIL_VALUES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_VENDOR_DETAIL_NBR';
  Result[High(Result)].DestFields[0] := 'SB_VENDOR_DETAIL_NBR';
end;

class function TSB_VENDOR_DETAIL.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_VENDOR_DETAIL_VALUES}

class function TSB_VENDOR_DETAIL_VALUES.GetClassIndex: Integer;
begin
  Result := 203;
end;

class function TSB_VENDOR_DETAIL_VALUES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_VENDOR_DETAIL_VALUES.GetEvTableNbr: Integer;
begin
  Result := 55;
end;

class function TSB_VENDOR_DETAIL_VALUES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_VENDOR_DETAIL_VALUES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_VENDOR_DETAIL_VALUES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 636
  else if AFieldName = 'SB_VENDOR_DETAIL_VALUES_NBR' then Result := 637
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 638
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 639
  else if AFieldName = 'SB_VENDOR_DETAIL_NBR' then Result := 640
  else if AFieldName = 'DETAIL_VALUES' then Result := 641
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_VENDOR_DETAIL_VALUES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_VENDOR_DETAIL_VALUES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_VENDOR_DETAIL_VALUES_NBR';
  Result[1] := 'SB_VENDOR_DETAIL_NBR';
  Result[2] := 'DETAIL_VALUES';
end;

class function TSB_VENDOR_DETAIL_VALUES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DETAIL_VALUES' then Result := 80
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_VENDOR_DETAIL_VALUES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_VENDOR_DETAIL_VALUES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_VENDOR_DETAIL_VALUES_NBR';
end;

class function TSB_VENDOR_DETAIL_VALUES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_VENDOR_DETAIL;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_VENDOR_DETAIL_NBR';
  Result[High(Result)].DestFields[0] := 'SB_VENDOR_DETAIL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_VENDOR_DETAIL_VALUES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_VENDOR_DETAIL_VALUES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ANALYTICS_ENABLED}

class function TSB_ANALYTICS_ENABLED.GetClassIndex: Integer;
begin
  Result := 204;
end;

class function TSB_ANALYTICS_ENABLED.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ANALYTICS_ENABLED.GetEvTableNbr: Integer;
begin
  Result := 56;
end;

class function TSB_ANALYTICS_ENABLED.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ANALYTICS_ENABLED.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ANALYTICS_ENABLED.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 642
  else if AFieldName = 'SB_ANALYTICS_ENABLED_NBR' then Result := 643
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 644
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 645
  else if AFieldName = 'CL_NBR' then Result := 646
  else if AFieldName = 'CO_NBR' then Result := 647
  else if AFieldName = 'ENABLE_ANALYTICS' then Result := 648
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ANALYTICS_ENABLED.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ANALYTICS_ENABLED.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SB_ANALYTICS_ENABLED_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'ENABLE_ANALYTICS';
end;

class function TSB_ANALYTICS_ENABLED.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ENABLE_ANALYTICS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ANALYTICS_ENABLED.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ANALYTICS_ENABLED.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ANALYTICS_ENABLED_NBR';
end;

class function TSB_ANALYTICS_ENABLED.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_ANALYTICS_ENABLED.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ANALYTICS_ENABLED.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ACA_GROUP_ADD_MEMBERS}

class function TSB_ACA_GROUP_ADD_MEMBERS.GetClassIndex: Integer;
begin
  Result := 205;
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetEvTableNbr: Integer;
begin
  Result := 57;
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 650
  else if AFieldName = 'SB_ACA_GROUP_ADD_MEMBERS_NBR' then Result := 651
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 652
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 653
  else if AFieldName = 'SB_ACA_GROUP_NBR' then Result := 654
  else if AFieldName = 'ALE_COMPANY_NAME' then Result := 655
  else if AFieldName = 'ALE_EIN_NUMBER' then Result := 656
  else if AFieldName = 'ALE_MAIN_COMPANY' then Result := 657
  else if AFieldName = 'ALE_NUMBER_OF_FTES' then Result := 658
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ACA_GROUP_ADD_MEMBERS_NBR';
  Result[1] := 'SB_ACA_GROUP_NBR';
  Result[2] := 'ALE_COMPANY_NAME';
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ALE_COMPANY_NAME' then Result := 40
  else if FieldName = 'ALE_MAIN_COMPANY' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ACA_GROUP_ADD_MEMBERS_NBR';
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ACA_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ACA_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ACA_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ACA_GROUP_ADD_MEMBERS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ACA_OFFER_CODES}

class function TSB_ACA_OFFER_CODES.GetClassIndex: Integer;
begin
  Result := 206;
end;

class function TSB_ACA_OFFER_CODES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSB_ACA_OFFER_CODES.GetEvTableNbr: Integer;
begin
  Result := 58;
end;

class function TSB_ACA_OFFER_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ACA_OFFER_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ACA_OFFER_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 661
  else if AFieldName = 'SB_ACA_OFFER_CODES_NBR' then Result := 662
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 663
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 664
  else if AFieldName = 'SY_FED_ACA_OFFER_CODES_NBR' then Result := 665
  else if AFieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := 666
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ACA_OFFER_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ACA_OFFER_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ACA_OFFER_CODES_NBR';
  Result[1] := 'SY_FED_ACA_OFFER_CODES_NBR';
  Result[2] := 'ACA_OFFER_CODE_DESCRIPTION';
end;

class function TSB_ACA_OFFER_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ACA_OFFER_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ACA_OFFER_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ACA_OFFER_CODES_NBR';
end;

class function TSB_ACA_OFFER_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_FED_ACA_OFFER_CODES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_FED_ACA_OFFER_CODES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_FED_ACA_OFFER_CODES_NBR';
end;

class function TSB_ACA_OFFER_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ACA_OFFER_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ACA_RELIEF_CODES}

class function TSB_ACA_RELIEF_CODES.GetClassIndex: Integer;
begin
  Result := 207;
end;

class function TSB_ACA_RELIEF_CODES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSB_ACA_RELIEF_CODES.GetEvTableNbr: Integer;
begin
  Result := 59;
end;

class function TSB_ACA_RELIEF_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ACA_RELIEF_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ACA_RELIEF_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 667
  else if AFieldName = 'SB_ACA_RELIEF_CODES_NBR' then Result := 668
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 669
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 670
  else if AFieldName = 'SY_FED_ACA_RELIEF_CODES_NBR' then Result := 671
  else if AFieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := 672
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ACA_RELIEF_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ACA_RELIEF_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ACA_RELIEF_CODES_NBR';
  Result[1] := 'SY_FED_ACA_RELIEF_CODES_NBR';
  Result[2] := 'ACA_RELIEF_CODE_DESCRIPTION';
end;

class function TSB_ACA_RELIEF_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ACA_RELIEF_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ACA_RELIEF_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ACA_RELIEF_CODES_NBR';
end;

class function TSB_ACA_RELIEF_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_FED_ACA_RELIEF_CODES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_FED_ACA_RELIEF_CODES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_FED_ACA_RELIEF_CODES_NBR';
end;

class function TSB_ACA_RELIEF_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ACA_RELIEF_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ADDITIONAL_INFO_NAMES}

class function TSB_ADDITIONAL_INFO_NAMES.GetClassIndex: Integer;
begin
  Result := 208;
end;

class function TSB_ADDITIONAL_INFO_NAMES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetEvTableNbr: Integer;
begin
  Result := 60;
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ADDITIONAL_INFO_NAMES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 673
  else if AFieldName = 'SB_ADDITIONAL_INFO_NAMES_NBR' then Result := 674
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 675
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 676
  else if AFieldName = 'CATEGORY' then Result := 677
  else if AFieldName = 'DATA_TYPE' then Result := 678
  else if AFieldName = 'NAME' then Result := 679
  else if AFieldName = 'REQUIRED' then Result := 680
  else if AFieldName = 'SEQ_NUM' then Result := 681
  else if AFieldName = 'SHOW_IN_EVO_PAYROLL' then Result := 682
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
  Result[1] := 'NAME';
  Result[2] := 'SEQ_NUM';
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CATEGORY' then Result := 1
  else if FieldName = 'DATA_TYPE' then Result := 1
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'REQUIRED' then Result := 1
  else if FieldName = 'SHOW_IN_EVO_PAYROLL' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ADDITIONAL_INFO_VALUES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
end;

class function TSB_ADDITIONAL_INFO_NAMES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSB_ADDITIONAL_INFO_VALUES}

class function TSB_ADDITIONAL_INFO_VALUES.GetClassIndex: Integer;
begin
  Result := 209;
end;

class function TSB_ADDITIONAL_INFO_VALUES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetEvTableNbr: Integer;
begin
  Result := 61;
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetEntityName: String;
begin
  Result := '';
end;

class function TSB_ADDITIONAL_INFO_VALUES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 683
  else if AFieldName = 'SB_ADDITIONAL_INFO_VALUES_NBR' then Result := 684
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 685
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 686
  else if AFieldName = 'SB_ADDITIONAL_INFO_NAMES_NBR' then Result := 687
  else if AFieldName = 'INFO_VALUE' then Result := 688
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SB_ADDITIONAL_INFO_VALUES_NBR';
  Result[1] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
  Result[2] := 'INFO_VALUE';
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'INFO_VALUE' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SB_ADDITIONAL_INFO_VALUES_NBR';
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_BUREAU_;
  Result[High(Result)].Table := TSB_ADDITIONAL_INFO_NAMES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
  Result[High(Result)].DestFields[0] := 'SB_ADDITIONAL_INFO_NAMES_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSB_ADDITIONAL_INFO_VALUES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TDM_BUREAU_}

class function TDM_BUREAU_.GetDBType: TevDBType;
begin
  Result := dbtBureau;
end;

function TDM_BUREAU_.GetSB: TSB;
begin
  Result := GetDataSet(TSB) as TSB;
end;

function TDM_BUREAU_.GetSB_ACCOUNTANT: TSB_ACCOUNTANT;
begin
  Result := GetDataSet(TSB_ACCOUNTANT) as TSB_ACCOUNTANT;
end;

function TDM_BUREAU_.GetSB_AGENCY: TSB_AGENCY;
begin
  Result := GetDataSet(TSB_AGENCY) as TSB_AGENCY;
end;

function TDM_BUREAU_.GetSB_AGENCY_REPORTS: TSB_AGENCY_REPORTS;
begin
  Result := GetDataSet(TSB_AGENCY_REPORTS) as TSB_AGENCY_REPORTS;
end;

function TDM_BUREAU_.GetSB_BANKS: TSB_BANKS;
begin
  Result := GetDataSet(TSB_BANKS) as TSB_BANKS;
end;

function TDM_BUREAU_.GetSB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS;
begin
  Result := GetDataSet(TSB_BANK_ACCOUNTS) as TSB_BANK_ACCOUNTS;
end;

function TDM_BUREAU_.GetSB_BLOB: TSB_BLOB;
begin
  Result := GetDataSet(TSB_BLOB) as TSB_BLOB;
end;

function TDM_BUREAU_.GetSB_DELIVERY_COMPANY: TSB_DELIVERY_COMPANY;
begin
  Result := GetDataSet(TSB_DELIVERY_COMPANY) as TSB_DELIVERY_COMPANY;
end;

function TDM_BUREAU_.GetSB_DELIVERY_COMPANY_SVCS: TSB_DELIVERY_COMPANY_SVCS;
begin
  Result := GetDataSet(TSB_DELIVERY_COMPANY_SVCS) as TSB_DELIVERY_COMPANY_SVCS;
end;

function TDM_BUREAU_.GetSB_DELIVERY_METHOD: TSB_DELIVERY_METHOD;
begin
  Result := GetDataSet(TSB_DELIVERY_METHOD) as TSB_DELIVERY_METHOD;
end;

function TDM_BUREAU_.GetSB_DELIVERY_SERVICE: TSB_DELIVERY_SERVICE;
begin
  Result := GetDataSet(TSB_DELIVERY_SERVICE) as TSB_DELIVERY_SERVICE;
end;

function TDM_BUREAU_.GetSB_DELIVERY_SERVICE_OPT: TSB_DELIVERY_SERVICE_OPT;
begin
  Result := GetDataSet(TSB_DELIVERY_SERVICE_OPT) as TSB_DELIVERY_SERVICE_OPT;
end;

function TDM_BUREAU_.GetSB_ENLIST_GROUPS: TSB_ENLIST_GROUPS;
begin
  Result := GetDataSet(TSB_ENLIST_GROUPS) as TSB_ENLIST_GROUPS;
end;

function TDM_BUREAU_.GetSB_GLOBAL_AGENCY_CONTACTS: TSB_GLOBAL_AGENCY_CONTACTS;
begin
  Result := GetDataSet(TSB_GLOBAL_AGENCY_CONTACTS) as TSB_GLOBAL_AGENCY_CONTACTS;
end;

function TDM_BUREAU_.GetSB_HOLIDAYS: TSB_HOLIDAYS;
begin
  Result := GetDataSet(TSB_HOLIDAYS) as TSB_HOLIDAYS;
end;

function TDM_BUREAU_.GetSB_MAIL_BOX: TSB_MAIL_BOX;
begin
  Result := GetDataSet(TSB_MAIL_BOX) as TSB_MAIL_BOX;
end;

function TDM_BUREAU_.GetSB_MAIL_BOX_CONTENT: TSB_MAIL_BOX_CONTENT;
begin
  Result := GetDataSet(TSB_MAIL_BOX_CONTENT) as TSB_MAIL_BOX_CONTENT;
end;

function TDM_BUREAU_.GetSB_MAIL_BOX_OPTION: TSB_MAIL_BOX_OPTION;
begin
  Result := GetDataSet(TSB_MAIL_BOX_OPTION) as TSB_MAIL_BOX_OPTION;
end;

function TDM_BUREAU_.GetSB_MEDIA_TYPE: TSB_MEDIA_TYPE;
begin
  Result := GetDataSet(TSB_MEDIA_TYPE) as TSB_MEDIA_TYPE;
end;

function TDM_BUREAU_.GetSB_MEDIA_TYPE_OPTION: TSB_MEDIA_TYPE_OPTION;
begin
  Result := GetDataSet(TSB_MEDIA_TYPE_OPTION) as TSB_MEDIA_TYPE_OPTION;
end;

function TDM_BUREAU_.GetSB_MULTICLIENT_REPORTS: TSB_MULTICLIENT_REPORTS;
begin
  Result := GetDataSet(TSB_MULTICLIENT_REPORTS) as TSB_MULTICLIENT_REPORTS;
end;

function TDM_BUREAU_.GetSB_OPTION: TSB_OPTION;
begin
  Result := GetDataSet(TSB_OPTION) as TSB_OPTION;
end;

function TDM_BUREAU_.GetSB_OTHER_SERVICE: TSB_OTHER_SERVICE;
begin
  Result := GetDataSet(TSB_OTHER_SERVICE) as TSB_OTHER_SERVICE;
end;

function TDM_BUREAU_.GetSB_PAPER_INFO: TSB_PAPER_INFO;
begin
  Result := GetDataSet(TSB_PAPER_INFO) as TSB_PAPER_INFO;
end;

function TDM_BUREAU_.GetSB_QUEUE_PRIORITY: TSB_QUEUE_PRIORITY;
begin
  Result := GetDataSet(TSB_QUEUE_PRIORITY) as TSB_QUEUE_PRIORITY;
end;

function TDM_BUREAU_.GetSB_REFERRALS: TSB_REFERRALS;
begin
  Result := GetDataSet(TSB_REFERRALS) as TSB_REFERRALS;
end;

function TDM_BUREAU_.GetSB_REPORTS: TSB_REPORTS;
begin
  Result := GetDataSet(TSB_REPORTS) as TSB_REPORTS;
end;

function TDM_BUREAU_.GetSB_REPORT_WRITER_REPORTS: TSB_REPORT_WRITER_REPORTS;
begin
  Result := GetDataSet(TSB_REPORT_WRITER_REPORTS) as TSB_REPORT_WRITER_REPORTS;
end;

function TDM_BUREAU_.GetSB_SALES_TAX_STATES: TSB_SALES_TAX_STATES;
begin
  Result := GetDataSet(TSB_SALES_TAX_STATES) as TSB_SALES_TAX_STATES;
end;

function TDM_BUREAU_.GetSB_SEC_CLIENTS: TSB_SEC_CLIENTS;
begin
  Result := GetDataSet(TSB_SEC_CLIENTS) as TSB_SEC_CLIENTS;
end;

function TDM_BUREAU_.GetSB_SEC_GROUPS: TSB_SEC_GROUPS;
begin
  Result := GetDataSet(TSB_SEC_GROUPS) as TSB_SEC_GROUPS;
end;

function TDM_BUREAU_.GetSB_SEC_GROUP_MEMBERS: TSB_SEC_GROUP_MEMBERS;
begin
  Result := GetDataSet(TSB_SEC_GROUP_MEMBERS) as TSB_SEC_GROUP_MEMBERS;
end;

function TDM_BUREAU_.GetSB_SEC_RIGHTS: TSB_SEC_RIGHTS;
begin
  Result := GetDataSet(TSB_SEC_RIGHTS) as TSB_SEC_RIGHTS;
end;

function TDM_BUREAU_.GetSB_SEC_ROW_FILTERS: TSB_SEC_ROW_FILTERS;
begin
  Result := GetDataSet(TSB_SEC_ROW_FILTERS) as TSB_SEC_ROW_FILTERS;
end;

function TDM_BUREAU_.GetSB_SEC_TEMPLATES: TSB_SEC_TEMPLATES;
begin
  Result := GetDataSet(TSB_SEC_TEMPLATES) as TSB_SEC_TEMPLATES;
end;

function TDM_BUREAU_.GetSB_SERVICES: TSB_SERVICES;
begin
  Result := GetDataSet(TSB_SERVICES) as TSB_SERVICES;
end;

function TDM_BUREAU_.GetSB_SERVICES_CALCULATIONS: TSB_SERVICES_CALCULATIONS;
begin
  Result := GetDataSet(TSB_SERVICES_CALCULATIONS) as TSB_SERVICES_CALCULATIONS;
end;

function TDM_BUREAU_.GetSB_TASK: TSB_TASK;
begin
  Result := GetDataSet(TSB_TASK) as TSB_TASK;
end;

function TDM_BUREAU_.GetSB_TAX_PAYMENT: TSB_TAX_PAYMENT;
begin
  Result := GetDataSet(TSB_TAX_PAYMENT) as TSB_TAX_PAYMENT;
end;

function TDM_BUREAU_.GetSB_TEAM: TSB_TEAM;
begin
  Result := GetDataSet(TSB_TEAM) as TSB_TEAM;
end;

function TDM_BUREAU_.GetSB_TEAM_MEMBERS: TSB_TEAM_MEMBERS;
begin
  Result := GetDataSet(TSB_TEAM_MEMBERS) as TSB_TEAM_MEMBERS;
end;

function TDM_BUREAU_.GetSB_USER: TSB_USER;
begin
  Result := GetDataSet(TSB_USER) as TSB_USER;
end;

function TDM_BUREAU_.GetSB_USER_NOTICE: TSB_USER_NOTICE;
begin
  Result := GetDataSet(TSB_USER_NOTICE) as TSB_USER_NOTICE;
end;

function TDM_BUREAU_.GetSB_STORAGE: TSB_STORAGE;
begin
  Result := GetDataSet(TSB_STORAGE) as TSB_STORAGE;
end;

function TDM_BUREAU_.GetSB_USER_MESSAGES: TSB_USER_MESSAGES;
begin
  Result := GetDataSet(TSB_USER_MESSAGES) as TSB_USER_MESSAGES;
end;

function TDM_BUREAU_.GetSB_USER_PREFERENCES: TSB_USER_PREFERENCES;
begin
  Result := GetDataSet(TSB_USER_PREFERENCES) as TSB_USER_PREFERENCES;
end;

function TDM_BUREAU_.GetSB_DASHBOARDS: TSB_DASHBOARDS;
begin
  Result := GetDataSet(TSB_DASHBOARDS) as TSB_DASHBOARDS;
end;

function TDM_BUREAU_.GetSB_ENABLED_DASHBOARDS: TSB_ENABLED_DASHBOARDS;
begin
  Result := GetDataSet(TSB_ENABLED_DASHBOARDS) as TSB_ENABLED_DASHBOARDS;
end;

function TDM_BUREAU_.GetSB_ACA_GROUP: TSB_ACA_GROUP;
begin
  Result := GetDataSet(TSB_ACA_GROUP) as TSB_ACA_GROUP;
end;

function TDM_BUREAU_.GetSB_CUSTOM_VENDORS: TSB_CUSTOM_VENDORS;
begin
  Result := GetDataSet(TSB_CUSTOM_VENDORS) as TSB_CUSTOM_VENDORS;
end;

function TDM_BUREAU_.GetSB_VENDOR: TSB_VENDOR;
begin
  Result := GetDataSet(TSB_VENDOR) as TSB_VENDOR;
end;

function TDM_BUREAU_.GetSB_VENDOR_DETAIL: TSB_VENDOR_DETAIL;
begin
  Result := GetDataSet(TSB_VENDOR_DETAIL) as TSB_VENDOR_DETAIL;
end;

function TDM_BUREAU_.GetSB_VENDOR_DETAIL_VALUES: TSB_VENDOR_DETAIL_VALUES;
begin
  Result := GetDataSet(TSB_VENDOR_DETAIL_VALUES) as TSB_VENDOR_DETAIL_VALUES;
end;

function TDM_BUREAU_.GetSB_ANALYTICS_ENABLED: TSB_ANALYTICS_ENABLED;
begin
  Result := GetDataSet(TSB_ANALYTICS_ENABLED) as TSB_ANALYTICS_ENABLED;
end;

function TDM_BUREAU_.GetSB_ACA_GROUP_ADD_MEMBERS: TSB_ACA_GROUP_ADD_MEMBERS;
begin
  Result := GetDataSet(TSB_ACA_GROUP_ADD_MEMBERS) as TSB_ACA_GROUP_ADD_MEMBERS;
end;

function TDM_BUREAU_.GetSB_ACA_OFFER_CODES: TSB_ACA_OFFER_CODES;
begin
  Result := GetDataSet(TSB_ACA_OFFER_CODES) as TSB_ACA_OFFER_CODES;
end;

function TDM_BUREAU_.GetSB_ACA_RELIEF_CODES: TSB_ACA_RELIEF_CODES;
begin
  Result := GetDataSet(TSB_ACA_RELIEF_CODES) as TSB_ACA_RELIEF_CODES;
end;

function TDM_BUREAU_.GetSB_ADDITIONAL_INFO_NAMES: TSB_ADDITIONAL_INFO_NAMES;
begin
  Result := GetDataSet(TSB_ADDITIONAL_INFO_NAMES) as TSB_ADDITIONAL_INFO_NAMES;
end;

function TDM_BUREAU_.GetSB_ADDITIONAL_INFO_VALUES: TSB_ADDITIONAL_INFO_VALUES;
begin
  Result := GetDataSet(TSB_ADDITIONAL_INFO_VALUES) as TSB_ADDITIONAL_INFO_VALUES;
end;

procedure Register;
begin
  RegisterComponents('EvoDdBureau', [
    TSB,
    TSB_ACCOUNTANT,
    TSB_AGENCY,
    TSB_AGENCY_REPORTS,
    TSB_BANKS,
    TSB_BANK_ACCOUNTS,
    TSB_BLOB,
    TSB_DELIVERY_COMPANY,
    TSB_DELIVERY_COMPANY_SVCS,
    TSB_DELIVERY_METHOD,
    TSB_DELIVERY_SERVICE,
    TSB_DELIVERY_SERVICE_OPT,
    TSB_ENLIST_GROUPS,
    TSB_GLOBAL_AGENCY_CONTACTS,
    TSB_HOLIDAYS,
    TSB_MAIL_BOX,
    TSB_MAIL_BOX_CONTENT,
    TSB_MAIL_BOX_OPTION,
    TSB_MEDIA_TYPE,
    TSB_MEDIA_TYPE_OPTION,
    TSB_MULTICLIENT_REPORTS,
    TSB_OPTION,
    TSB_OTHER_SERVICE,
    TSB_PAPER_INFO,
    TSB_QUEUE_PRIORITY,
    TSB_REFERRALS,
    TSB_REPORTS,
    TSB_REPORT_WRITER_REPORTS,
    TSB_SALES_TAX_STATES,
    TSB_SEC_CLIENTS,
    TSB_SEC_GROUPS,
    TSB_SEC_GROUP_MEMBERS,
    TSB_SEC_RIGHTS,
    TSB_SEC_ROW_FILTERS,
    TSB_SEC_TEMPLATES,
    TSB_SERVICES,
    TSB_SERVICES_CALCULATIONS,
    TSB_TASK,
    TSB_TAX_PAYMENT,
    TSB_TEAM,
    TSB_TEAM_MEMBERS,
    TSB_USER,
    TSB_USER_NOTICE,
    TSB_STORAGE,
    TSB_USER_MESSAGES,
    TSB_USER_PREFERENCES,
    TSB_DASHBOARDS,
    TSB_ENABLED_DASHBOARDS,
    TSB_ACA_GROUP,
    TSB_CUSTOM_VENDORS,
    TSB_VENDOR,
    TSB_VENDOR_DETAIL,
    TSB_VENDOR_DETAIL_VALUES,
    TSB_ANALYTICS_ENABLED,
    TSB_ACA_GROUP_ADD_MEMBERS,
    TSB_ACA_OFFER_CODES,
    TSB_ACA_RELIEF_CODES,
    TSB_ADDITIONAL_INFO_NAMES,
    TSB_ADDITIONAL_INFO_VALUES]);
end;

initialization
  RegisterClasses([
    TSB,
    TSB_ACCOUNTANT,
    TSB_AGENCY,
    TSB_AGENCY_REPORTS,
    TSB_BANKS,
    TSB_BANK_ACCOUNTS,
    TSB_BLOB,
    TSB_DELIVERY_COMPANY,
    TSB_DELIVERY_COMPANY_SVCS,
    TSB_DELIVERY_METHOD,
    TSB_DELIVERY_SERVICE,
    TSB_DELIVERY_SERVICE_OPT,
    TSB_ENLIST_GROUPS,
    TSB_GLOBAL_AGENCY_CONTACTS,
    TSB_HOLIDAYS,
    TSB_MAIL_BOX,
    TSB_MAIL_BOX_CONTENT,
    TSB_MAIL_BOX_OPTION,
    TSB_MEDIA_TYPE,
    TSB_MEDIA_TYPE_OPTION,
    TSB_MULTICLIENT_REPORTS,
    TSB_OPTION,
    TSB_OTHER_SERVICE,
    TSB_PAPER_INFO,
    TSB_QUEUE_PRIORITY,
    TSB_REFERRALS,
    TSB_REPORTS,
    TSB_REPORT_WRITER_REPORTS,
    TSB_SALES_TAX_STATES,
    TSB_SEC_CLIENTS,
    TSB_SEC_GROUPS,
    TSB_SEC_GROUP_MEMBERS,
    TSB_SEC_RIGHTS,
    TSB_SEC_ROW_FILTERS,
    TSB_SEC_TEMPLATES,
    TSB_SERVICES,
    TSB_SERVICES_CALCULATIONS,
    TSB_TASK,
    TSB_TAX_PAYMENT,
    TSB_TEAM,
    TSB_TEAM_MEMBERS,
    TSB_USER,
    TSB_USER_NOTICE,
    TSB_STORAGE,
    TSB_USER_MESSAGES,
    TSB_USER_PREFERENCES,
    TSB_DASHBOARDS,
    TSB_ENABLED_DASHBOARDS,
    TSB_ACA_GROUP,
    TSB_CUSTOM_VENDORS,
    TSB_VENDOR,
    TSB_VENDOR_DETAIL,
    TSB_VENDOR_DETAIL_VALUES,
    TSB_ANALYTICS_ENABLED,
    TSB_ACA_GROUP_ADD_MEMBERS,
    TSB_ACA_OFFER_CODES,
    TSB_ACA_RELIEF_CODES,
    TSB_ADDITIONAL_INFO_NAMES,
    TSB_ADDITIONAL_INFO_VALUES]);

end.
