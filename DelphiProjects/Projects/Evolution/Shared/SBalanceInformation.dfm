object BalanceDM: TBalanceDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 354
  Top = 115
  Height = 733
  Width = 913
  object cdsSummary: TevClientDataSet
    IndexName = 'cdsSummaryIndex'
    IndexDefs = <
      item
        Name = 'cdsSummaryIndex'
        Fields = 'ClNbr;CoNbr;SortKey'
      end>
    OnCalcFields = cdsSummaryCalcFields
    Left = 36
    Top = 18
    object cdsSummaryClNbr: TIntegerField
      FieldName = 'ClNbr'
    end
    object cdsSummaryCoNbr: TIntegerField
      FieldName = 'CoNbr'
    end
    object cdsSummarySortKey: TStringField
      FieldName = 'SortKey'
      Size = 50
    end
    object cdsSummaryTextDescription: TStringField
      FieldName = 'TextDescription'
      Size = 80
    end
    object cdsSummaryAmountWages: TCurrencyField
      FieldName = 'AmountWages'
    end
    object cdsSummaryAmountTaxHist: TCurrencyField
      FieldName = 'AmountTaxHist'
    end
    object cdsSummaryAmountTaxLiab: TCurrencyField
      FieldName = 'AmountTaxLiab'
    end
    object cdsSummaryWagesDSName: TStringField
      FieldName = 'WagesDSName'
      Size = 255
    end
    object cdsSummaryLiabDSName: TStringField
      FieldName = 'LiabDSName'
      Size = 255
    end
    object cdsSummaryDifLiabHist: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'DifLiabHist'
    end
    object cdsSummaryDifWageHist: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'DifWageHist'
    end
    object cdsSummaryClName: TStringField
      FieldName = 'ClName'
      Size = 40
    end
    object cdsSummaryCoName: TStringField
      FieldName = 'CoName'
      Size = 40
    end
    object cdsSummaryClNumber: TStringField
      FieldName = 'ClNumber'
    end
    object cdsSummaryCoNumber: TStringField
      FieldName = 'CoNumber'
    end
    object cdsSummaryConsolidation: TBooleanField
      FieldName = 'Consolidation'
    end
    object cdsSummaryTaxPercent: TFloatField
      FieldName = 'TaxPercent'
    end
    object cdsSummaryTaxAmount: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'TaxAmount'
    end
    object cdsSummaryMargin: TFloatField
      FieldName = 'Margin'
    end
    object cdsSummaryDontCheck: TBooleanField
      FieldName = 'DontCheck'
    end
    object cdsSummaryMNSui: TIntegerField
      FieldName = 'MNSui'
    end
    object cdsSummaryAmountEE_Tax_: TCurrencyField
      FieldName = 'AmountEE_Tax_'
    end
    object cdsSummaryAmountER_Tax_: TCurrencyField
      FieldName = 'AmountER_Tax_'
    end
    object cdsSummaryDifWageHist_EE_ER: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'DifWageHist_EE_ER'
      Calculated = True
    end
    object cdsSummarySkipBalanceControl: TBooleanField
      FieldName = 'SkipBalanceControl'
    end
    object cdsSummaryFICAEE_Wages_: TCurrencyField
      FieldName = 'FICAEE_Wages_'
    end
    object cdsSummaryFICADifWageHist_: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'FICADifWageHist_'
      Calculated = True
    end
  end
  object cdsCoList: TevClientDataSet
    IndexFieldNames = 'ClNbr;CoNbr'
    FieldDefs = <
      item
        Name = 'ClNbr'
        DataType = ftInteger
      end
      item
        Name = 'CoNbr'
        DataType = ftInteger
      end
      item
        Name = 'MainCoNbr'
        DataType = ftInteger
      end
      item
        Name = 'Consolidation'
        DataType = ftBoolean
      end>
    Left = 270
    Top = 21
  end
  object Provider: TISProvider
    DataSet = cdsCoList
    Left = 207
    Top = 21
  end
  object cdsAllEarnings: TevClientDataSet
    Left = 42
    Top = 129
  end
  object cdsAllMemos: TevClientDataSet
    Left = 42
    Top = 186
  end
  object cdsTips: TevClientDataSet
    Left = 42
    Top = 246
  end
  object cdsGTL: TevClientDataSet
    Left = 42
    Top = 300
  end
  object cdsAggCheckLines: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr'
      end>
    Left = 345
    Top = 63
  end
  object cdsW2Box: TevClientDataSet
    IndexFieldNames = 'W2_BOX_CALC'
    OnCalcFields = cdsW2BoxCalcFields
    Left = 42
    Top = 354
    object cdsW2BoxCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsW2BoxCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsW2BoxCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsW2BoxCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsW2BoxCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsW2BoxCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsW2BoxPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object cdsW2BoxAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsW2BoxPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsW2BoxRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object cdsW2BoxCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsW2BoxPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsW2BoxPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object cdsW2BoxCHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object cdsW2BoxCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdsW2BoxFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdsW2BoxMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Size = 1
    end
    object cdsW2BoxLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdsW2BoxSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdsW2BoxEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsW2BoxCUSTOM_EMPLOYEE_NUMBER: TStringField
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object cdsW2BoxCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object cdsW2BoxCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdsW2BoxE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Size = 2
    end
    object cdsW2BoxDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdsW2BoxW2_BOX: TStringField
      FieldName = 'W2_BOX'
      Size = 4
    end
    object cdsW2BoxEE_EXEMPT_EXCLUDE_FEDERAL: TStringField
      FieldName = 'EE_EXEMPT_EXCLUDE_FEDERAL'
      Size = 1
    end
    object cdsW2BoxEE_EXEMPT_EXCLUDE_EIC: TStringField
      FieldName = 'EE_EXEMPT_EXCLUDE_EIC'
      Size = 1
    end
    object cdsW2BoxEE_EXEMPT_EXCLUDE_OASDI: TStringField
      FieldName = 'EE_EXEMPT_EXCLUDE_OASDI'
      Size = 1
    end
    object cdsW2BoxEE_EXEMPT_EXCLUDE_MEDICARE: TStringField
      FieldName = 'EE_EXEMPT_EXCLUDE_MEDICARE'
      Size = 1
    end
    object cdsW2BoxER_EXEMPT_EXCLUDE_OASDI: TStringField
      FieldName = 'ER_EXEMPT_EXCLUDE_OASDI'
      Size = 1
    end
    object cdsW2BoxER_EXEMPT_EXCLUDE_MEDICARE: TStringField
      FieldName = 'ER_EXEMPT_EXCLUDE_MEDICARE'
      Size = 1
    end
    object cdsW2BoxER_EXEMPT_EXCLUDE_FUI: TStringField
      FieldName = 'ER_EXEMPT_EXCLUDE_FUI'
      Size = 1
    end
    object cdsW2BoxW2_BOX_CALC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'W2_BOX_CALC'
      Size = 4
    end
  end
  object cdsCheckLines: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 342
    Top = 21
  end
  object cdsFederalEExempt: TevClientDataSet
    Left = 186
    Top = 96
  end
  object cdsFederalDExempt: TevClientDataSet
    Left = 255
    Top = 114
  end
  object cdsFederalTax: TevClientDataSet
    Left = 120
    Top = 114
  end
  object cdsEICEExempt: TevClientDataSet
    Left = 186
    Top = 204
  end
  object cdsEICDExempt: TevClientDataSet
    Left = 255
    Top = 222
  end
  object cdsEEOASDIEExempt: TevClientDataSet
    Left = 186
    Top = 306
  end
  object cdsEEOASDIDExempt: TevClientDataSet
    Left = 255
    Top = 324
  end
  object cdsEEMedicareEExempt: TevClientDataSet
    Left = 189
    Top = 414
  end
  object cdsEEMedicareDExempt: TevClientDataSet
    Left = 258
    Top = 432
  end
  object cdsEROASDIEExempt: TevClientDataSet
    Left = 426
    Top = 310
  end
  object cdsEROASDIDExempt: TevClientDataSet
    Left = 495
    Top = 328
  end
  object cdsERMedicareEExempt: TevClientDataSet
    Left = 426
    Top = 406
  end
  object cdsERMedicareDExempt: TevClientDataSet
    Left = 495
    Top = 424
  end
  object cdsFUIEExempt: TevClientDataSet
    Left = 417
    Top = 205
  end
  object cdsFUIDExempt: TevClientDataSet
    Left = 486
    Top = 223
  end
  object cdsChecks: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 423
    Top = 21
  end
  object cdsAggChecks: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr'
      end>
    Left = 423
    Top = 63
  end
  object cdsLiab: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    OnCalcFields = cdsLiabCalcFields
    Left = 495
    Top = 21
    object cdsLiabCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsLiabCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsLiabCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsLiabCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsLiabCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsLiabCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsLiabPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsLiabRUN_NUMBER: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'RUN_NUMBER'
    end
    object cdsLiabCHECK_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'CHECK_DATE'
    end
    object cdsLiabTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
    object cdsLiabAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsLiabTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Size = 1
    end
  end
  object cdsAggLiab: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr'
      end>
    Left = 498
    Top = 63
  end
  object cdsFederalLiab: TevClientDataSet
    Left = 120
    Top = 162
  end
  object cdsEICLiab: TevClientDataSet
    Left = 120
    Top = 243
  end
  object cdsEEOASDILiab: TevClientDataSet
    Left = 111
    Top = 327
  end
  object cdsEEMedicareLiab: TevClientDataSet
    Left = 111
    Top = 358
  end
  object cdsERMedicareLiab: TevClientDataSet
    Left = 354
    Top = 321
  end
  object cdsEROASDILiab: TevClientDataSet
    Left = 345
    Top = 260
  end
  object cdsFUILiab: TevClientDataSet
    Left = 345
    Top = 206
  end
  object cdsBackupWithholdingLiab: TevClientDataSet
    Left = 330
    Top = 378
  end
  object cdsFederal1099: TevClientDataSet
    Left = 420
    Top = 129
  end
  object cds3Party: TevClientDataSet
    Left = 339
    Top = 138
  end
  object cds3PartyFederalLiab: TevClientDataSet
    Left = 567
    Top = 108
  end
  object cds3PartyEEOASDILiab: TevClientDataSet
    Left = 567
    Top = 156
  end
  object cds3PartyEROASDILiab: TevClientDataSet
    Left = 573
    Top = 201
  end
  object cdsAggCheckStates: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;State'
      end>
    Left = 573
    Top = 63
  end
  object cdsCheckStates: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 573
    Top = 21
  end
  object cdsAggStateLiab: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;State'
      end>
    Left = 663
    Top = 66
  end
  object cdsStateLiab: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    OnCalcFields = cdsLiabCalcFields
    Left = 660
    Top = 24
    object cdsStateLiabCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsStateLiabCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsStateLiabCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsStateLiabCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsStateLiabCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsStateLiabCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsStateLiabPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsStateLiabRUN_NUMBER: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'RUN_NUMBER'
    end
    object cdsStateLiabCHECK_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'CHECK_DATE'
    end
    object cdsStateLiabTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
    object cdsStateLiabAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsStateLiabCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object cdsStateLiabSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
  end
  object cdsStateTaxLiab: TevClientDataSet
    Left = 669
    Top = 180
  end
  object cdsStateTax: TevClientDataSet
    Left = 669
    Top = 135
  end
  object cdsEESDILiab: TevClientDataSet
    Left = 672
    Top = 231
  end
  object cdsERSDILiab: TevClientDataSet
    Left = 672
    Top = 282
  end
  object cdsState1099: TevClientDataSet
    Left = 657
    Top = 336
  end
  object cdsCheckLineStates: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 597
    Top = 393
  end
  object cdsAggCheckLineStates: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    Aggregates = <
      item
      end>
    AggregatesActive = True
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_CLIENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CLIENT_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'COMPANY_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PR_CHECK_LINES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'RUN_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAYMENT_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_PERSON_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FIRST_NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MIDDLE_INITIAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LAST_NAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'SOCIAL_SECURITY_NUMBER'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_EMPLOYEE_NUMBER'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_E_D_CODE_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'E_D_CODE_TYPE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SY_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EMPLOYEE_EXEMPT_EXCLUDE_SDI'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EMPLOYER_EXEMPT_EXCLUDE_SDI'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'State'
        DataType = ftString
        Size = 2
      end>
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;State'
      end>
    Left = 600
    Top = 435
  end
  object cdsStateDExempt: TevClientDataSet
    Left = 627
    Top = 507
  end
  object cdsStateEExempt: TevClientDataSet
    Left = 558
    Top = 489
  end
  object cdsEESDIEExempt: TevClientDataSet
    Left = 33
    Top = 510
  end
  object cdsEESDIDExempt: TevClientDataSet
    Left = 102
    Top = 528
  end
  object cdsERSDIDExempt: TevClientDataSet
    Left = 249
    Top = 531
  end
  object cdsERSDIEExempt: TevClientDataSet
    Left = 180
    Top = 513
  end
  object cdsEESUIEExempt: TevClientDataSet
    Left = 293
    Top = 510
  end
  object cdsEESUIDExempt: TevClientDataSet
    Left = 362
    Top = 528
  end
  object cdsERSUIDExempt: TevClientDataSet
    Left = 509
    Top = 531
  end
  object cdsERSUIEExempt: TevClientDataSet
    Left = 440
    Top = 513
  end
  object cdsCheckLineSUI: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 597
    Top = 345
    object cdsCheckLineSUICL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsCheckLineSUICUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsCheckLineSUICLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsCheckLineSUICO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsCheckLineSUICUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsCheckLineSUICOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsCheckLineSUIPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object cdsCheckLineSUIAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsCheckLineSUIPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsCheckLineSUIRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object cdsCheckLineSUICHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsCheckLineSUIPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsCheckLineSUIPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object cdsCheckLineSUICHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object cdsCheckLineSUICL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdsCheckLineSUIFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdsCheckLineSUIMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Size = 1
    end
    object cdsCheckLineSUILAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdsCheckLineSUISOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdsCheckLineSUIEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckLineSUICUSTOM_EMPLOYEE_NUMBER: TStringField
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object cdsCheckLineSUISTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object cdsCheckLineSUICL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object cdsCheckLineSUICUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdsCheckLineSUIE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Size = 2
    end
    object cdsCheckLineSUIDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdsCheckLineSUISY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object cdsCheckLineSUIEMPLOYEE_EXEMPT_EXCLUDE_SUI: TStringField
      FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_SUI'
      Size = 1
    end
    object cdsCheckLineSUIEMPLOYER_EXEMPT_EXCLUDE_SUI: TStringField
      FieldName = 'EMPLOYER_EXEMPT_EXCLUDE_SUI'
      Size = 1
    end
    object cdsCheckLineSUISY_SUI_NBR: TIntegerField
      FieldName = 'SY_SUI_NBR'
    end
    object cdsCheckLineSUIEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField
      FieldKind = fkLookup
      FieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      KeyFields = 'SY_SUI_NBR'
      Size = 1
      Lookup = True
    end
  end
  object cdsAggCheckSUI: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;Sy_SUI_Nbr'
      end>
    Left = 291
    Top = 624
  end
  object cdsCheckSUI: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 291
    Top = 582
    object cdsCheckSUICL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsCheckSUICUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsCheckSUICLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsCheckSUICO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsCheckSUICUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsCheckSUICOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsCheckSUIPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsCheckSUIRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object cdsCheckSUICHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsCheckSUIPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsCheckSUIPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object cdsCheckSUICHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object cdsCheckSUICL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdsCheckSUIFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdsCheckSUIMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Size = 1
    end
    object cdsCheckSUILAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdsCheckSUISOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdsCheckSUIEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckSUICUSTOM_EMPLOYEE_NUMBER: TStringField
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object cdsCheckSUICOMPANY_OR_INDIVIDUAL_NAME: TStringField
      FieldName = 'COMPANY_OR_INDIVIDUAL_NAME'
      Size = 1
    end
    object cdsCheckSUICO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object cdsCheckSUISTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object cdsCheckSUISUI_TAXABLE_WAGES: TFloatField
      FieldName = 'SUI_TAXABLE_WAGES'
    end
    object cdsCheckSUISUI_TAX: TFloatField
      FieldName = 'SUI_TAX'
    end
    object cdsCheckSUISUI_GROSS_WAGES: TFloatField
      FieldName = 'SUI_GROSS_WAGES'
    end
    object cdsCheckSUIDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdsCheckSUISY_SUI_NBR: TIntegerField
      FieldName = 'SY_SUI_NBR'
    end
    object cdsCheckSUIEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField
      FieldKind = fkLookup
      FieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      KeyFields = 'SY_SUI_NBR'
      Size = 1
      Lookup = True
    end
  end
  object cdsAggSUILiab: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;Sy_Sui_Nbr'
      end>
    Left = 381
    Top = 627
  end
  object cdsSUILiab: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    OnCalcFields = cdsLiabCalcFields
    Left = 378
    Top = 585
    object cdsSUILiabCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsSUILiabCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsSUILiabCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsSUILiabCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsSUILiabCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsSUILiabCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsSUILiabPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsSUILiabRUN_NUMBER: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'RUN_NUMBER'
    end
    object cdsSUILiabCHECK_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'CHECK_DATE'
    end
    object cdsSUILiabAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsSUILiabCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object cdsSUILiabSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object cdsSUILiabSY_SUI_NBR: TIntegerField
      FieldName = 'SY_SUI_NBR'
    end
    object cdsSUILiabEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField
      FieldKind = fkLookup
      FieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      KeyFields = 'SY_SUI_NBR'
      Size = 1
      Lookup = True
    end
  end
  object cdsEESUITax: TevClientDataSet
    Left = 679
    Top = 381
  end
  object cdsEESUILiab: TevClientDataSet
    Left = 682
    Top = 477
  end
  object cdsERSUILiab: TevClientDataSet
    Left = 682
    Top = 528
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 414
    Top = 534
  end
  object cdsEESUIMemo: TevClientDataSet
    Left = 449
    Top = 585
  end
  object cdsERSUIMemo: TevClientDataSet
    Left = 518
    Top = 603
  end
  object cdsEESDI1099: TevClientDataSet
    Left = 567
    Top = 249
  end
  object cdsEESUI1099: TevClientDataSet
    Left = 546
    Top = 309
  end
  object cdsERSUI1099: TevClientDataSet
    Left = 609
    Top = 297
  end
  object cdsERSDI1099: TevClientDataSet
    Left = 618
    Top = 243
  end
  object cdsERSUITax: TevClientDataSet
    Left = 640
    Top = 426
  end
  object cdsCoSum: TevClientDataSet
    FieldDefs = <
      item
        Name = 'ClName'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CoName'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ClNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CoNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ClNbr'
        DataType = ftInteger
      end
      item
        Name = 'CoNbr'
        DataType = ftInteger
      end
      item
        Name = 'Consolidation'
        DataType = ftBoolean
      end
      item
        Name = 'OutBalance'
        DataType = ftBoolean
      end
      item
        Name = 'OutBalWages'
        DataType = ftBoolean
      end
      item
        Name = 'Margin'
        DataType = ftFloat
      end
      item
        Name = 'OutBalance_'
        DataType = ftBoolean
      end
      item
        Name = 'OutBalance_EE_ER'
        DataType = ftBoolean
      end>
    OnCalcFields = cdsSummaryCalcFields
    Left = 129
    Top = 24
    object cdsCoSumClName: TStringField
      FieldName = 'ClName'
      Size = 40
    end
    object cdsCoSumCoName: TStringField
      FieldName = 'CoName'
      Size = 40
    end
    object cdsCoSumClNumber: TStringField
      FieldName = 'ClNumber'
    end
    object cdsCoSumCoNumber: TStringField
      FieldName = 'CoNumber'
    end
    object cdsCoSumClNbr: TIntegerField
      FieldName = 'ClNbr'
    end
    object cdsCoSumCoNbr: TIntegerField
      FieldName = 'CoNbr'
    end
    object cdsCoSumConsolidation: TBooleanField
      FieldName = 'Consolidation'
    end
    object cdsCoSumOutBalance: TBooleanField
      FieldName = 'OutBalance'
    end
    object cdsCoSumOutBalWages: TBooleanField
      FieldName = 'OutBalWages'
    end
    object cdsCoSumMargin: TFloatField
      FieldName = 'Margin'
    end
    object cdsCoSumOutBalance_: TBooleanField
      FieldName = 'OutBalance_'
    end
    object cdsCoSumOutBalance_EE_ER: TBooleanField
      FieldName = 'OutBalance_EE_ER'
    end
    object cdsCoSumbTaxWageLimit: TBooleanField
      FieldName = 'bTaxWageLimit'
    end
  end
  object cdsCheckLocal: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 673
    Top = 600
    object cdsCheckLocalLocal_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsCheckLocalState: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LocalState'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 2
      Lookup = True
    end
    object cdsCheckLocalCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsCheckLocalCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsCheckLocalCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsCheckLocalCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsCheckLocalCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsCheckLocalCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsCheckLocalPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsCheckLocalRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object cdsCheckLocalCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsCheckLocalPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsCheckLocalPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object cdsCheckLocalCHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object cdsCheckLocalCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdsCheckLocalFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdsCheckLocalMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Size = 1
    end
    object cdsCheckLocalLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdsCheckLocalSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdsCheckLocalEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckLocalCUSTOM_EMPLOYEE_NUMBER: TStringField
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object cdsCheckLocalCOMPANY_OR_INDIVIDUAL_NAME: TStringField
      FieldName = 'COMPANY_OR_INDIVIDUAL_NAME'
      Size = 1
    end
    object cdsCheckLocalEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object cdsCheckLocalCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object cdsCheckLocalSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
    object cdsCheckLocalLOCAL_TAXABLE_WAGE: TFloatField
      FieldName = 'LOCAL_TAXABLE_WAGE'
    end
    object cdsCheckLocalLOCAL_TAX: TFloatField
      FieldName = 'LOCAL_TAX'
    end
  end
  object cdsAggCheckLocal: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;SY_LOCALS_NBR'
      end>
    Left = 673
    Top = 642
  end
  object cdsAggLocalLiab: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;SY_LOCALS_NBR'
      end>
    Left = 763
    Top = 645
  end
  object cdsLocalLiab: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    OnCalcFields = cdsLiabCalcFields
    Left = 760
    Top = 603
    object cdsLocalLiabLocal_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsLocalLiabState: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LocalState'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 2
      Lookup = True
    end
    object cdsLocalLiabCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsLocalLiabCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsLocalLiabCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsLocalLiabCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsLocalLiabCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsLocalLiabCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsLocalLiabPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsLocalLiabRUN_NUMBER: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'RUN_NUMBER'
    end
    object cdsLocalLiabCHECK_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'CHECK_DATE'
    end
    object cdsLocalLiabAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsLocalLiabSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
  end
  object cdsCheckLineLocal: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    Left = 597
    Top = 547
    object cdsCheckLineLocalCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsCheckLineLocalCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdsCheckLineLocalCLIENT_NAME: TStringField
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdsCheckLineLocalCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsCheckLineLocalCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsCheckLineLocalCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdsCheckLineLocalPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object cdsCheckLineLocalAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsCheckLineLocalPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdsCheckLineLocalRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object cdsCheckLineLocalCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsCheckLineLocalPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsCheckLineLocalPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object cdsCheckLineLocalCHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object cdsCheckLineLocalCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdsCheckLineLocalFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdsCheckLineLocalMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Size = 1
    end
    object cdsCheckLineLocalLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdsCheckLineLocalSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdsCheckLineLocalEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckLineLocalCUSTOM_EMPLOYEE_NUMBER: TStringField
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object cdsCheckLineLocalCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object cdsCheckLineLocalCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdsCheckLineLocalE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Size = 2
    end
    object cdsCheckLineLocalDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdsCheckLineLocalSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
    object cdsCheckLineLocalEXEMPT_EXCLUDE: TStringField
      FieldName = 'EXEMPT_EXCLUDE'
      Size = 1
    end
    object cdsCheckLineLocalPR_CHECK_LINE_LOCALS_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINE_LOCALS_NBR'
    end
    object cdsCheckLineLocalLocal_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsCheckLineLocalState: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LocalState'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 2
      Lookup = True
    end
  end
  object cdsAggCheckLineLocal: TevClientDataSet
    IndexName = 'idxAggregationCoNbr'
    Aggregates = <
      item
      end>
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'idxAggregationCoNbr'
        Fields = 'Co_Nbr;SY_LOCALS_NBR'
      end>
    Left = 600
    Top = 589
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 270
    Top = 420
  end
  object cdsLocalTaxLiab: TevClientDataSet
    Left = 627
    Top = 180
  end
  object cdsLocalTax: TevClientDataSet
    Left = 627
    Top = 135
  end
  object cdsLocalDExempt: TevClientDataSet
    Left = 627
    Top = 483
  end
  object cdsLocalEExempt: TevClientDataSet
    Left = 621
    Top = 414
  end
  object cdsLocal1099: TevClientDataSet
    Left = 630
    Top = 483
  end
  object cdEEMedicare: TevClientDataSet
    Left = 40
    Top = 640
    object cdEEMedicareCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdEEMedicareCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdEEMedicareMAKEUP_FICA_ON_CLEANUP_PR: TStringField
      FieldName = 'MAKEUP_FICA_ON_CLEANUP_PR'
      Size = 1
    end
    object cdEEMedicareEeMedicateWagesTotal: TFloatField
      FieldName = 'EeMedicateWagesTotal'
    end
    object cdEEMedicareEeMedicateWagesTotalQuarter: TFloatField
      FieldName = 'EeMedicateWagesTotalQuarter'
    end
  end
end
