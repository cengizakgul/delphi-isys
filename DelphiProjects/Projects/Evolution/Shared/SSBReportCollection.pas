unit SSBReportCollection;

interface
uses
  SFrameEntry,
  StdCtrls, Mask, wwdbedit, Controls, Grids,
  ComCtrls, Classes, Db,
  DBCtrls, Menus, sysutils, Forms, Dialogs,
  Buttons,
  StrUtils, Variants,
  Wwdbigrd, Wwdbgrid,
  Wwdatsrc,
  SDataStructure,
  EvConsts, EvUtils, Wwdotdot, Wwdbcomb, ISZippingRoutines,
  EvContext,
  EvSecElement, SSecurityInterface, SPackageEntry,
  SDDClasses,
  ISBasicClasses,
  EvStreamUtils,
  SDataDictbureau, EvDataAccessComponents,
  EvUIUtils,
  EvUIComponents,
  EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton,
  isUIDBMemo, isUIwwDBEdit,
  isUIwwDBComboBox
  ,isBaseClasses

  ;

const
  POSTLIMIT = 8;

type
  // Back-call procedure defined on caller form ; supposed to create new report record
  // with new ID
  // field value and bring dataset to dsEdit state
  // Resides on form because it may need additional datasets
  // like System reports, which updates Versions also
  // We do not want to bring those specifics into this class

  TNewReportRecordBackCall = procedure (Sender: TObject;
                                    var ReportType: string;
                                        jClienNumber: integer;
                                        ImportedNbr: integer;
                                    var ReportName: string;
                                    var AssignedNewNbr: integer
                                       ) of object;

  IEvReportCollectionHelper = interface
  ['{4B103BD9-E6A0-4EA4-83C1-2DD58F467E0D}']
    procedure SaveReportCollection( dbGrid : TevDBGrid; DBType:string; ClientNumber:integer; FromTableDirect:boolean);
    procedure LoadCollection(dbGrid: TevDBGrid; DBType:string; ClientNumber:integer; dataset:TDataSet; aBackCall:TNewReportRecordBackCall);
  end;


  TEvRepCollectionHelper = class(TisInterfacedObject, IEvReportCollectionHelper)
  private
    procedure SaveReportCollectionToFile( dbGrid: TevDBGrid;
                                          DBType:string;
                                          jClientNumber:integer;
                                          CollectionFileName: string;
                                          FromTableDirect:boolean
                                         );
    procedure LoadCollectionFromFile(dbGrid: TevDBGrid; DBType:string; ClientNumber:integer; CollectionFileName:string; dataset:TDataset; aBackCall:TNewReportRecordBackCall);
    procedure ImportReportInstance(iReportInstance:IisListOfValues; dbGrid: TevDBGrid;
                                   DbType:string; ReportType:string; ClientNumber:integer;
                                   wrkDataset:TDataset; aBackCall:TNewReportRecordBackCall);

    function ImportMixConfirmation(ImportType: string;
                                   ImportClientNumber: integer;
                                   LocalType: string;
                                   LocalClientNumber: integer;
                               var bForeignEnvDetected: boolean
                                  ): boolean;
    class function NextCopyName(AlterName: string; jFieldSize: integer): string;

  public
    class function DoubleApostropheSQLStrLeft(s: string; Limit: integer): string;

    class function ApostropheCount(s: string): integer;

    class function LineHasCopyHeader(line: string): boolean;

    class function isLinesRelated(oldLine, proposedLine: string;
                                  jFieldSize: integer): boolean;

    class function CopyNameVariationEx(originalName: string; jCopy,
      jFieldLength: integer): string;

    class function ListOfRelatedNamesFromDelta(const Deltalist: TstringList;
                                        const ProposedName: string;
                                        const jFieldSize: integer): string;

    class function CopyNameVariation( originalName: string;
                                      jCopy,jFieldLength: integer): string;

    {IEvReportCollectionHelper interface implementation}
    procedure SaveReportCollection( dbGrid : TevDBGrid; DBType:string; ClientNumber:integer; FromTableDirect:boolean);
    procedure LoadCollection(dbGrid: TevDBGrid; DBType:string; ClientNumber:integer; dataset:TDataSet; aBackCall:TNewReportRecordBackCall);
  end;

implementation


{ TEvRepCollectionHelper }

procedure TEvRepCollectionHelper.SaveReportCollectionToFile(dbGrid: TevDBGrid;
                                                            DBType:string;
                                                            jClientNumber:integer;
                                                            CollectionFileName:string;
                                                            FromTableDirect:boolean
                                                            );
var
  currentBookmark:TBookMark;
  BookList:TList;
  dataset : TDataset;
  jMark : integer;
  wrkMark:TBookMark;
  iReportCollectionInstance : IisParamsCollection;
  iReportItemInstance : IisListOfValues;
  iSaveStream : IisStream;
  iContentStream : IisStream;
  jReportNbr : integer;
  PackedS :IisStream;
  jExportCount: integer;
begin
  jExportCount := 0;
  BookList := dbGrid.SelectedList;
  dataset := dbGrid.DataSource.DataSet;
  currentBookmark := dataset.GetBookmark;
  iReportCollectionInstance := TisParamsCollection.Create;
  iReportItemInstance := iReportCollectionInstance.AddParams('Parameters');
  iReportItemInstance.AddValue('CollectionType',DBType);
  iReportItemInstance.Addvalue('ClientNumber', IntToStr(jClientNumber));
  dbGrid.DataSource.DataSet.DisableControls;
  ctx_StartWait('Retrieving report data...');
  try
    try
      if dataSet.State in [dsBrowse, dsEdit] then
      begin
        for jMark := 0 to BookList.Count - 1 do
        begin
          wrkMark := TBookMark(BookList[jMark]);
          dataset.GotoBookMark(wrkMark);
          // we set Name of report item in exported object as natural numeric, and starts with 1 - always.
          iReportItemInstance := iReportCollectionInstance.AddParams( IntToStr(jMark+1));
//        SB_REPORT_WRITER_REPORTS_NBR - for SB Reports
//        SY_REPORT_WRITER_REPORTS_NBR - for iSystem - Miscellaneous
//        CL_REPORT_WRITER_REPORTS_NBR - for Clients - RWReports
          jReportNbr := dataset.Fields[0].AsInteger;
          iReportItemInstance.AddValue('NBR', jReportNbr);
          iReportItemInstance.AddValue('Name', dataset.FieldByName('REPORT_DESCRIPTION').AsString);
          iReportItemInstance.AddValue('Type', dataset.FieldByName('REPORT_TYPE').AsString);
          iReportItemInstance.AddValue('MediaType', dataset.FieldByName('MEDIA_TYPE').AsString);
          iReportItemInstance.AddValue('Notes', dataset.FieldByName('NOTES').AsString);
          iReportItemInstance.AddValue('RWDescription', dataset.FieldByName('RWDescription').AsString);

          iContentStream := TisStream.Create;

          jExportCount := jExportCount + 1;

          if FromTableDirect then
          begin
            PackedS := TevClientDataSet(dataset).GetBlobData('REPORT_FILE');
            PackedS.Position := 0;
// in case if empty BLOB file obtained
            if PackedS.Size = 0 then
              if mrOk <> EvMessage(
              'Report content for ' + IntToStr(jReportNbr) + #13#10
              + '"'+dataset.FieldByName('REPORT_DESCRIPTION').AsString+'"' + #13#10
              + 'is empty. Is it ok?',
               mtWarning,
              [mbOk, mbAbort],
              mbAbort,
              0)
              then
              begin
                jExportCount := 0;
                iSaveStream := nil;
                Abort;
              end;
            if PackedS.Size > 0 then
              InflateStream(PackedS.RealStream, iContentStream.RealStream);
          end
          else
          begin
            ctx_RWLocalEngine.GetReportResources(
                                                  jReportNbr,
                                                  DBType, //CH_DATABASE_SERVICE_BUREAU, for example
                                                  iContentStream
                                                );
          end;
          iReportItemInstance.AddValue('Data', iContentStream);

          iContentStream.Position:=0;
        end; // of for-loop over selected items

        iSaveStream := TisStream.CreateFromNewFile(CollectionFileName);
        iSaveStream.WriteObject(iReportCollectionInstance);

        iSaveStream := nil;
      end; // of check of Dataset State
    finally
      dataset.FreeBookmark(currentBookmark);
      if jExportCount = 0 then
        EvMessage('Report collection export failed. No file was created.')
      else
        EvMessage('Report collection of ' + IntToStr(jExportCount) + ' has been exported to file.');
    end;
  finally
      ctx_EndWait();
      dbGrid.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEvRepCollectionHelper.SaveReportCollection(dbGrid: TevDBGrid; DBType:string; ClientNumber:integer; FromTableDirect:boolean);
var
  aSaveDialog:TSaveDialog;
  CollectionFileName : string;
begin
  Assert ( Assigned(dbGrid));
  aSaveDialog := TSaveDialog.Create(dbGrid);
  try
    aSaveDialog.Filter := 'Report Collection File (*.evr)|*.evr';
    aSaveDialog.Options :=  [ofOverwritePrompt,ofHideReadOnly,ofEnableSizing];
    aSaveDialog.Title := 'Export Collection of Reports To File';
    if dbGrid.SelectedList.Count > 0 then
      if aSaveDialog.Execute then
      begin
        CollectionFileName := ChangeFileExt(aSaveDialog.FileName, '.evr');
        SaveReportCollectionToFile(dbGrid, DBType,ClientNumber, CollectionFileName,FromTableDirect);
      end;
  finally
    aSaveDialog.Free;
  end;
end;

procedure TEvRepCollectionHelper.ImportReportInstance(iReportInstance:IisListOfValues; dbGrid: TevDBGrid; DbType:string; ReportType:string; ClientNumber:integer;  wrkDataset:TDataset; aBackCall:TNewReportRecordBackCall);
var
  jNbr: integer;
  sName: string;
  sType: string;
  sMediaType: string;
  sNotes: string;
  sRWDescription: string;
  iData: IisStream;
  iZipped: IisStream;
  sNbrFieldName: string;
  oldValueDescriptionRequired:boolean;
begin
  jNBR := iReportInstance.Value['NBR'];
  sName := iReportinstance.Value['Name'];
  sType := iReportinstance.Value['Type'];
  sMediaType := iReportinstance.Value['MediaType'];
  sNotes := iReportinstance.Value['Notes'];
  sRWDescription := iReportinstance.Value['RWDescription'];
  // that's how to convert Variant into iSystem interface - cannot convert
  // directly, because it is not dual interface
  iData := IInterface(iReportinstance.Value['Data']) as IisStream;
  iData.Position := 0;
  iZipped := TisStream.Create();
  DeflateStream(iData.RealStream, iZipped.RealStream);
  iZipped.Position := 0;

  oldValueDescriptionRequired := wrkDataset.FieldByName('REPORT_DESCRIPTION').Required;
  try
    // aBackCall define conditions works to detect situation of mixed environment import
    // and necessity to re-ajust NBR field :
    // if aBackCall assigned then all imports considered brand-new readjusted
    if  Assigned(aBackCall) then
    begin
      // reajustment here
      aBackcall(wrkDataset,
                sType,
                ClientNumber,jNbr,sName,jNbr);
    end
    else
    begin // the same environment - no reajustment required
      sNbrFieldName := wrkDataset.Fields[0].FieldName;
      if wrkDataset.Locate( sNbrFieldName, jNBR,[]) then
        wrkDataset.Edit
      else
      begin

        wrkDataset.FieldByName('REPORT_DESCRIPTION').Required := False;
        wrkDataset.Insert;
    //  SB_REPORT_WRITER_REPORTS_NBR, or SY_REPORT_WRITER_REPORTS_NBR, or CL_REPORT_WRITER_REPORTS_NBR
        wrkDataset.Fields[0].AsInteger := jNBR;
      end;
    end;
    TBlobField(wrkDataset.FieldByName('REPORT_FILE')).LoadFromStream(iZipped.RealStream);
    wrkDataset.FieldByName('REPORT_DESCRIPTION').AsString := sName;
    wrkDataset.FieldByName('REPORT_TYPE').AsString := sType;
    wrkDataset.FieldByName('MEDIA_TYPE').AsString := sMediaType;
    wrkDataset.FieldByName('NOTES').AsString := sNotes;
    wrkDataset.FieldByName('RWDescription').AsString := sRWDescription;
  finally
    wrkDataset.FieldByName('REPORT_DESCRIPTION').Required := oldValueDescriptionRequired;
  end;

  wrkDataset.Post;

end;

function TEvRepCollectionHelper.ImportMixConfirmation(
                                                  ImportType:string;
                                                  ImportClientNumber:integer;
                                                  LocalType:string;
                                                  LocalClientNumber:integer;
                                              var bForeignEnvDetected :boolean
                                                     ):boolean;
var
  sMessage:string;
begin
  Result := True;
  bForeignEnvDetected:= (Comparetext( ImportType,LocalType) <> 0) or (ImportClientNumber <> LocalClientNumber);
  if bForeignEnvDetected then
  begin
     sMessage := 'You are importing ';
     if Comparetext( ImportType,LocalType) <> 0 then
     begin
        case ImportType[1] of
          'S' : sMessage := sMessage + ' System ';
          'B' : sMessage := sMessage + ' Service Bureau ';
          'C' : sMessage := sMessage + ' Client Number '+IntToStr(ImportClientNumber);
        end;
        sMessage := sMessage+#13#10+'report into '+#13#10;
        case LocalType[1] of
          'S' : sMessage := sMessage + ' System ';
          'B' : sMessage := sMessage + ' Service Bureau ';
          'C' : sMessage := sMessage + ' Client Number '+IntToStr(LocalClientNumber);
        end;
        sMessage := sMessage + ' Report Set'+#13#10+'Are you sure?';
     end
     else
     begin
        sMessage := sMessage + ' reports from Client Number '+IntToStr(ImportClientNumber)
        +#13#10+' into Client Number '+IntToStr(LocalClientNumber) + ' Report Set '
        +#13#10+'Are you sure?';
     end;
     Result := (mrYes = MessageDlg(sMessage, mtConfirmation , [mbYes,mbNo],0));
  end;
end;



procedure TEvRepCollectionHelper.LoadCollectionFromFile(
                  dbGrid: TevDBGrid;
                  DBType:string;
                  ClientNumber:integer;
                  CollectionFileName: string;
                  dataset: TDataset;
                  aBackCall:TNewReportRecordBackCall
                  );
var
  iReportCollection: IisParamsCollection ;
  inStream: IisStream;
  jReport: integer;
  iReportInstance : IisListOfValues;
  jProcessed: integer;
  InstanceName: string;
  jOrder,jer: integer;
  sCollectionType: string;
  sClientNumber: string;
  jClientNumber: integer;
  bForeignEnvDetected:boolean;
  jPostCount: integer;
  sAlterName: string;
  jFieldSize: integer;
begin
    jProcessed := 0;
// direct enable manipulation of grid cannot be replaced by dataset.DisableControls
// because BLOB operations got affected on that level ... it is unexpected behavior
// manifested in SPD_EDIT_SB_RWREPORTS (SB level)
  dataset.DisableControls; //block also master-detail relationships, according Delphi Help
  ctx_StartWait('Retrieving report data...');
  try
    inStream := TIsStream.CreateFromFile(CollectionFileName);
    iReportCollection := inStream.ReadObject(iReportCollection) as IisParamsCollection;

    if Assigned(iReportCollection) then
    begin
      for jReport := 0 to iReportCollection.Count - 1 do
      begin
         iReportInstance := iReportCollection.Params[jReport];
         Assert( Assigned (iReportInstance));
         InstanceName := iReportCollection.ParamName(jReport);
         if CompareText('Parameters',InstanceName)=0 then
         begin
           sCollectionType := VarToStr(iReportInstance.Value['CollectionType']);
           sClientNumber := VarToStr(iReportInstance.Value['ClientNumber']);
           Val( sClientNumber,jClientNumber,jer);

           if ImportMixConfirmation (sCollectionType, jClientNumber, DBType, ClientNumber, bForeignEnvDetected) then
              Continue
           else
              Exit;
         end;

         Val( InstanceName,jOrder,jer);
         if (jer = 0) and (jOrder > 0) then
         begin
           if bForeignEnvDetected then
              ImportReportInstance(iReportInstance, dbGrid, DBType, sCollectionType, ClientNumber,  dataset, aBackCall)
           else
              ImportReportInstance(iReportInstance, dbGrid, DBType, sCollectionType, ClientNumber,  dataset, nil);

           Inc(jProcessed);
         end;
      end;
    end;
    // now, push all delta to server
    jPostCount := 0;
    while jPostCount < POSTLIMIT do
    begin
      try
        jPostCount := jPostCount + 1;
        ctx_DataAccess.PostDataSets([dataset as TevClientDataset]);
        jPostCount := POSTLIMIT;
      except
      // generally, here we come when we have conflict between update of existing record and new-imported ones
        dataset.Edit;
        sAlterName := dataset.FieldByName('REPORT_DESCRIPTION').AsString;
        jFieldSize := dataset.FieldByName('REPORT_DESCRIPTION').DataSize - 1;
        dataset.FieldByName('REPORT_DESCRIPTION').AsString := TEvRepCollectionHelper.NextCopyName(sAlterName,jFieldSize );
        dataset.Post;
      end;
    end;
  finally
    ctx_EndWait();
    if jProcessed >0 then
      if jProcessed >1 then
        EvMessage(Format('Imported %d reports',[jProcessed]))
      else
        EvMessage('Imported single report');
// direct enable manipulation of grid cannot be replaced by
    dataset.EnableControls;
  end;
end;



procedure TEvRepCollectionHelper.LoadCollection(dbGrid: TevDBGrid; DBType:string; ClientNumber:integer; dataset:TDataset; aBackCall:TNewReportRecordBackCall);
var
  aOpenDialog : TOpenDialog;
begin
  Assert( Assigned(dbGrid));

  aOpenDialog :=  TOpenDialog.Create(nil);
  try
    aOpenDialog.Filter := 'Report Collection File (*.evr)|*.evr';
    aOpenDialog.Options :=  [ofOverwritePrompt,ofHideReadOnly,ofEnableSizing];
    aOpenDialog.Title := 'Import Collection of Reports From File';
    if aOpenDialog.Execute then
    begin
      LoadCollectionFromFile(dbGrid, DBType,ClientNumber, aOpenDialog.FileName, dataset, aBackCall);
    end;
  finally
    aOpenDialog.Free;
  end;
end;

class function TEvRepCollectionHelper.isLinesRelated( oldLine, proposedLine: string; jFieldSize: integer):boolean;
var
  oldTail: string;
  oldAjusted: string;

  sAjusted: string;
  sCopyPart: string;
  aCopyTail: string;
  jApostropheCount: integer;
begin
// because it is already apostrophe-ajusted
   jApostropheCount := TEvRepCollectionHelper.ApostropheCount(proposedLine) div 2;
   Result := False;
   oldAjusted := System.Copy(oldLine, 1, jFieldSize);
   sAjusted := System.Copy(ProposedLine, 1, jFieldSize+jApostropheCount);
   if CompareText(oldLine, sAjusted) = 0 then
   begin
     Result := True;
     Exit;
   end;

   if AnsiStartsText('Copy ', oldAjusted) then
     oldTail :=   System.Copy(oldAjusted, Length('Copy ZZ - ')+1, Length(oldAjusted))
   else
     oldtail := oldAjusted;

   if AnsiStartsText('Copy ', sAjusted) then
     aCopyTail := System.Copy(sAjusted, Length('Copy ZZ - ')+1, Length(sAjusted))
   else
    aCopyTail := sAjusted;


   sCopyPart := AnsiLeftStr(sAjusted, Length('Copy ZZ - '));

   if (CompareText(aCopyTail,oldTail) = 0)
      or
      (CompareText(aCopyTail, oldLine) = 0)
   then
          Result := True;
end;


class function TEvRepCollectionHelper.CopyNameVariation( originalName: string;
                                                         jCopy:integer;
                                                         jFieldLength:integer
                                                        ):string;
begin
// already double-apostrophed !!
  Assert(jCopy >= 0);
  if jCopy = 0 then
    Result:=OriginalName
  else
    Result :=  'Copy '+Format('%.2d',[jCopy])+' - '
              + System.Copy(OriginalName,1,jFieldLength-10);
end;

class function TEvRepCollectionHelper.LineHasCopyHeader(line: string):boolean;
begin
  Result := False;
  if AnsiStartsText('Copy ', line) then
    if Length(line) > 11 then
      if Comparetext(System.Copy(line,8,3),' - ') = 0 then
        Result := True;
end;

class function TEvRepCollectionHelper.NextCopyName( AlterName:string; jFieldSize:integer):string;
var
  jCount,jEr: integer;
  sCopy:string;
begin
  jCount := 0;
  if LineHasCopyHeader(AlterName) then
  begin
    sCopy := System.Copy(AlterName,6,2);
    Val(sCopy, jCount, jEr);
    Inc(jCount);
    Result := 'Copy '+Format('%.2d',[jCount])+System.Copy(AlterName,8,Length(AlterName));
  end
  else
  begin
    Inc(jCount);
    Result := 'Copy '+Format('%.2d',[jCount])+' - '+ System.Copy(AlterName,1,Length(AlterName));
    Result := AnsiLeftStr(Result, jFieldSize);
  end;

end;

class function TEvRepCollectionHelper.CopyNameVariationEx( originalName: string;
                                                         jCopy:integer;
                                                         jFieldLength:integer
                                                        ):string;
var
  s: string;
begin
  if LineHascopyHeader(Originalname) then
    s := System.Copy(originalName, Length('Copy ZZ - ')+1, Length(OriginalName))
  else
    s := originalName;

  Result := CopyNameVariation(s, jCopy,jFieldlength);
end;

class function TEvRepCollectionHelper.ListOfRelatedNamesFromDelta(const DeltaList:TstringList;
                                                           const ProposedName:string;
                                                           const jFieldSize:integer):string;
// function to leave only one-report-related names
var
  sAjusted: string;
  jLine: integer;
begin
  Result := '';
  sAjusted := proposedName;
  if AnsiStartsStr('Copy ',sAjusted)
     and
     (CompareStr(System.Copy(sAjusted,8,3),' - ') = 0)
  then
    sAjusted := AnsiRightStr( sAjusted, Length(ProposedName)-Length('Copy ZZ - '));

  sAjusted := System.Copy(sAjusted, 1, jFieldSize);
  for jLine := 0 to DeltaList.Count - 1 do
  begin
     if isLinesRelated(DeltaList[jLine], sAjusted, jFieldSize) then
     begin
       Result := Result + DeltaList[jLine] + #13#10;
       Continue;
     end;
  end;
end;

class function TEvRepCollectionHelper.ApostropheCount(s:string):integer;
var
  j: integer;
begin
  Result := 0;
  for j := 1 to Length(s) do
    if Ord(s[j]) = 39 then
      Result := Result + 1;
end;

class function TEvRepCollectionHelper.DoubleApostropheSQLStrLeft(s:string; Limit:integer):string;
var
  j: integer;

  function SQLLen( es: string):integer;
  begin
    Result := Length(AnsiReplacestr(es, '''''',''''));
  end;

begin
// works for already apostrophe-duplicated string
  Result := '';
  j:= 1;
  while (j <= Length(s)) and (SQLLen(Result) < Limit) do
  begin
    if Ord(s[j]) <> 39 then
      Result := Result + s[j]
    else
    begin
      if j=Length(s) then
        Exit // Apostrophe at the end of the line
      else
      begin
        if ord(s[j+1]) = 39 then
        begin
          Result := Result +s[j]+s[j+1];
          j := j + 1;
        end
        else
        begin
          Result := Result +s[j]+''''; // duplicate apostrophe
          j := j + 1;
       end;
      end;
    end;
    j := j + 1;
  end;
end;

end.
