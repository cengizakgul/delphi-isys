inherited SafeChoiceFromListQuery: TSafeChoiceFromListQuery
  Width = 554
  Height = 447
  Caption = 'SafeChoiceFromListQuery'
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 368
    Width = 538
    TabOrder = 2
    DesignSize = (
      538
      41)
    inherited btnYes: TevBitBtn
      Left = 320
      Width = 100
    end
    inherited btnNo: TevBitBtn
      Left = 429
      Width = 100
    end
    inherited btnAll: TevBitBtn
      Left = 211
      Width = 100
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 538
    Height = 333
    IniAttributes.SectionName = 'TSafeChoiceFromListQuery\dgChoiceList'
    TabOrder = 0
    PaintOptions.AlternatingRowColor = 14544093
  end
  object pnlEffectiveDate: TevPanel [2]
    Left = 0
    Top = 333
    Width = 538
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object evLabel1: TevLabel
      Left = 8
      Top = 15
      Width = 107
      Height = 13
      Caption = 'Begin Effective Date   '
    end
    object dtBeginEffective: TevDateTimePicker
      Left = 122
      Top = 7
      Width = 89
      Height = 21
      Date = 2.000000000000000000
      Time = 2.000000000000000000
      TabOrder = 0
    end
  end
  inherited dsChoiceList: TevDataSource
    DataSet = cdsChoiceList
  end
  object cdsChoiceList: TevClientDataSet [4]
    Left = 96
    Top = 128
  end
end
