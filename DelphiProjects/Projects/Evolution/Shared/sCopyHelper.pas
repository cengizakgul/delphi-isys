// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy
unit sCopyHelper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList,  Menus, ImgList, sCopier, ISBasicClasses, EvUIComponents, EvUtils, EvClientDataSet;

type
  TCopierNeededEvent = procedure( Sender: TObject; var copier: TCopier ) of object;

  TCopyHelper = class(TFrame)
    CopyPopupMenu: TevPopupMenu;
    CopyActionList: TevActionList;
    CopyAction: TAction;
    DeleteAction: TAction;
    Copyto1: TMenuItem;
    Deletefrom1: TMenuItem;
    CopyImageList: TevImageList;
    CopyAddressAction: TAction;
    Copyaddressto1: TMenuItem;
    procedure CopyActionUpdate(Sender: TObject);
    procedure CopyActionExecute(Sender: TObject);
    procedure DeleteActionUpdate(Sender: TObject);
    procedure DeleteActionExecute(Sender: TObject);
    procedure CopyAddressActionExecute(Sender: TObject);
    procedure CopyAddressActionUpdate(Sender: TObject);
  private
    { Private declarations }
    FGrid: TevDBGrid;
    FDeleteEnabled: boolean;
    FCopyAddressEnabled: boolean;
    FOnCopierNeeded: TCopierNeededEvent;
    FUserFriendlyName: string;
    procedure SetGrid(const Value: TevDBGrid);
    procedure SetOnCopierNeeded(const Value: TCopierNeededEvent);
  protected
    function AllAssigned: boolean;
    function DataSet: TevClientDataSet;
    function CreateCopier: TCopier; virtual;
  public
    { Public declarations }
    FDSToSave: TArrayDS; //for passing to GetDataSetsToReopen
    constructor Create( Owner: TComponent ); override;
    procedure SetDSToSave( dss: array of TevClientDataSet );

    property Grid: TevDBGrid read FGrid write SetGrid;
    property DeleteEnabled: boolean read FDeleteEnabled write FDeleteEnabled default true ;
    property CopyAddressEnabled: boolean read FCopyAddressEnabled write FCopyAddressEnabled default false;
    property OnCopierNeeded: TCopierNeededEvent read FOnCopierNeeded write SetOnCopierNeeded;
    property UserFriendlyName: string read FUserFriendlyName write FUserFriendlyName;
  end;

implementation

{$R *.DFM}
uses
  wwdbigrd, Wwdbgrid;

{ TFrame1 }

procedure TCopyHelper.SetGrid(const Value: TevDBGrid);
begin
  FGrid := Value;
  if not assigned( FGrid.PopupMenu ) then
    FGrid.PopupMenu := CopyPopupMenu;
  FGrid.Options := FGrid.Options + [dgRowSelect,dgMultiSelect];
end;

procedure TCopyHelper.CopyActionUpdate(Sender: TObject);
begin
 (Sender as TAction).Enabled := AllAssigned
 and (FGrid.DataSource.DataSet.RecordCount > 0) and (FGrid.SelectedList.Count > 0);
end;

procedure TCopyHelper.CopyActionExecute(Sender: TObject);
var
  copier: TCopier;
begin
  copier := CreateCopier;
  if assigned( copier ) then
  try
    copier.BeforeCopy;
    try
      copier.Copy;
    finally
      copier.AfterCopy;
    end;
  finally
    FreeAndNil( copier );
  end;
end;

procedure TCopyHelper.DeleteActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Visible := FDeleteEnabled;
  if FDeleteEnabled then
    (Sender as TAction).Enabled := AllAssigned and (FGrid.DataSource.DataSet.RecordCount > 0)
                                   and (FGrid.SelectedList.Count > 0)
  else
    (Sender as TAction).Enabled := false;
end;

procedure TCopyHelper.DeleteActionExecute(Sender: TObject);
var
  copier: TCopier;
begin
  copier := CreateCopier;
  if assigned( copier ) then
  try
    copier.BeforeDelete;
    try
      copier.Delete;
    finally
      copier.AfterDelete;
    end;
  finally
    FreeAndNil( copier );
  end;
end;


constructor TCopyHelper.Create(Owner: TComponent);
begin
  inherited;
  FDeleteEnabled := true;
end;

function TCopyHelper.DataSet: TevClientDataSet;
begin
  Result := TEvClientDataSet(FGrid.DataSource.DataSet);
end;

function TCopyHelper.AllAssigned: boolean;
begin
  Result := assigned( FGrid ) and assigned( FGrid.DataSource ) and assigned( FGrid.DataSource.DataSet );
end;

function TCopyHelper.CreateCopier: TCopier;
begin
  Result := nil;
  if assigned( FOnCopierNeeded ) then
    FOnCopierNeeded( Self, Result );
end;

procedure TCopyHelper.SetOnCopierNeeded(const Value: TCopierNeededEvent);
begin
  FOnCopierNeeded := Value;
end;


procedure TCopyHelper.SetDSToSave(dss: array of TevClientDataSet);
begin
  SetLength( FDSToSave, 0 );
  AddDS( dss, FDSToSave );

end;

procedure TCopyHelper.CopyAddressActionExecute(Sender: TObject);
begin
  //
end;

procedure TCopyHelper.CopyAddressActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Visible := FCopyAddressEnabled;
  if FCopyAddressEnabled then
     (Sender as TAction).Enabled := AllAssigned
                                    and (FGrid.DataSource.DataSet.RecordCount > 0)
                                    and (FGrid.SelectedList.Count > 0)
  else
    (Sender as TAction).Enabled := false;
end;

end.
