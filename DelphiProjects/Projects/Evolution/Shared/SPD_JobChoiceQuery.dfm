inherited JobChoiceQuery: TJobChoiceQuery
  Left = 602
  Top = 214
  Width = 497
  Height = 381
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 302
    Width = 481
    inherited btnYes: TevBitBtn
      Left = 243
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 369
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 113
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 481
    Height = 302
    Selected.Strings = (
      'DESCRIPTION'#9'40'#9'Description'#9'F'
      'CO_NBR'#9'10'#9'Co Nbr'#9'F')
    IniAttributes.SectionName = 'TJobChoiceQuery\dgChoiceList'
  end
  inherited dsChoiceList: TevDataSource
    Left = 192
    Top = 40
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 128
    Top = 32
  end
end
