unit EvContext;

interface

uses Classes, SyncObjs, SysUtils, Windows, isBaseClasses, EvCommonInterfaces, isBasicUtils, EvConsts,
     isThreadManager;

type
  TevContextManager = class(TisInterfacedObject, IevContextManager)
  private
    FContextList: TStringList;
    procedure AddContextToList(const AContext: TObject);
    procedure RemoveContextFromList(const AContext: TObject);
    function  GetContextFromList(const AIndex: Integer): IevContext;
    function  DoCreateThreadContext(const AUser, APassword: String; const AID, ARemoteID: TisGUID): IevContext;
  protected
    procedure DoOnConstruction; override;
    function  GetThreadContext: IevContext;
    function  SetThreadContext(const AContext: IevContext): IevContext;
    function  CreateThreadContext(const AUser, APassword: String; const AID: TisGUID = ''; const ARemoteID: TisGUID = ''): IevContext;
    function  CreateContext(const AUser, APassword: String; const AID: TisGUID = ''; const ARemoteID: TisGUID = ''): IevContext;
    function  CopyContext(const ASource: IevContext): IevContext;
    procedure DestroyThreadContext;
    function  FindContextByID(const AID: TisGUID): IevContext;
    function  FindUserContexts(const AUser: String): IisList;
    function  FindDomainContexts(const ADomain: String = sDefaultDomain): IisList;
    procedure StoreThreadContext;
    procedure RestoreThreadContext;
  public
    destructor Destroy; override;
  end;


// Shortcuts to thread context functions

  function  Context: IevContext;
  procedure ctx_StartWait(const AText: string = ''; const AMaxProgress: Integer = 0);
  procedure ctx_UpdateWait(const AText: string = ''; const ACurrentProgress: Integer = 0;  const AMaxProgress: Integer = -1);
  procedure ctx_EndWait;
  function  ctx_EvolutionGUI: IevEvolutionGUI;
  function  ctx_DataAccess: IevDataAccess;
  function  ctx_DBAccess: IevDBAccess;
  function  ctx_Security: IevSecurity;
  function  ctx_DomainInfo: IevDomainInfo;
  function  ctx_AccountRights: IevSecAccountRights;
  function  ctx_PayrollCalculation:IevPayrollCalculation;
  function  ctx_PayrollProcessing: IevPayrollProcessing;
  function  ctx_PayrollCheckPrint: IevPayrollCheckPrint;
  function  ctx_RemoteMiscRoutines: IevRemoteMiscRoutines;
  function  ctx_VmrEngine: IevVmrEngine;
  function  ctx_VmrRemote: IevVmrRemote;
  function  ctx_RWDesigner: IevRWDesigner;
  function  ctx_RWRemoteEngine: IevRWRemoteEngine;
  function  ctx_RWLocalEngine: IevRWLocalEngine;
  function  ctx_EMailer: IevEMailer;
  function  ctx_Statistics: IevStatistics;
  function  ctx_CashManagement: IevCashManagement;


implementation

uses EvMainboard, EvClasses, EvBasicUtils;

type
  TevContext = class(TisInterfacedObject, IevContext)
  private
    FID: TisGUID;
    FRemoteID: TisGUID;
    FUserAccount: IevUserAccount;
    FEMailer: IevEMailer;
    FCallbacks: IevContextCallbacks;
    FEvolutionGUI: IevEvolutionGUI;
    FDataAccess: IevDataAccess;
    FDBAccess: IevDBAccess;
    FSecurity: IevSecurity;
    FDomainInfo: IevDomainInfo;
    FPayrollCalculation:IevPayrollCalculation;
    FPayrollProcessing: IevPayrollProcessing;
    FPayrollCheckPrint: IevPayrollCheckPrint;
    FRemoteMiscRoutines: IevRemoteMiscRoutines;
    FVmrEngine: IevVmrEngine;
    FVmrRemote: IevVmrRemote;
    FRWDesigner: IevRWDesigner;
    FRWRemoteEngine: IevRWRemoteEngine;
    FRWLocalEngine: IevRWLocalEngine;
    FLicense: IevLicense;
    FEvolutionExchange: IEvExchange;
    FEvolutionExchangeEngine: IevExchangeEngine;
    FUserScheduler: IevUserScheduler;
    FStatistics: IevStatistics;
    FCashManagement: IevCashManagement;
    FSingleSignOn: IevSingleSignOn;
    procedure DoRunParallelTask(const AParams: TTaskParamList);
    procedure DoRunParallelTaskObj(const AParams: TTaskParamList);
    function  CreateModule(const AModuleType: TGUID; const ADontError: Boolean = False): IInterface;
  protected
    function  GetID: TisGUID;
    function  GetRemoteID: TisGUID;
    function  Callbacks: IevContextCallbacks;
    procedure SetCallbacks(const AValue: IevContextCallbacks);
    function  RunParallelTask(const Task: PTaskProc; const AParams: TTaskParamList; const ATaskDescription: String = '';
                              const APriority: TThreadPriority = tpNormal; const AThreadManager: IThreadManager = nil): TTaskID; overload;
    function  RunParallelTask(const Task: PTaskProcObj; const AObject: TObject; const AParams: TTaskParamList;
                              const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal;
                              const AThreadManager: IThreadManager = nil): TTaskID; overload;
    procedure WaitForTaskEnd(const ATaskID: TTaskID = 0);
    function  TaskIsFinished(const ATaskID: TTaskID = 0): Boolean;
    function  UserAccount: IevUserAccount;
    function  GetScreenPackage(const AModuleType: TGUID): IevScreenPackage;
    function  EMailer: IevEMailer;
    function  CashManagement: IevCashManagement;
    function  EvolutionGUI: IevEvolutionGUI;
    function  DataAccess: IevDataAccess;
    function  DBAccess: IevDBAccess;
    function  Security: IevSecurity;
    function  DomainInfo: IevDomainInfo;
    function  PayrollCalculation: IevPayrollCalculation;
    function  PayrollProcessing: IevPayrollProcessing;
    function  PayrollCheckPrint: IevPayrollCheckPrint;
    function  RemoteMiscRoutines: IevRemoteMiscRoutines;
    function  VmrEngine: IevVmrEngine;
    function  VmrRemote: IevVmrRemote;
    function  RWDesigner: IevRWDesigner;
    function  RWRemoteEngine: IevRWRemoteEngine;
    function  RWLocalEngine: IevRWLocalEngine;
    function  License: IevLicense;
    function  EvolutionExchange : IEvExchange;
    function  EvolutionExchangeEngine : IEvExchangeEngine;
    function  UserScheduler: IevUserScheduler;
    function  Statistics: IevStatistics;
    function  SingleSignOn: IevSingleSignOn;
  public
    constructor Create(const ADomain, AUser, APassword, AID, ARemoteID: TisGUID); reintroduce;
    destructor  Destroy; override;
  end;


  TevDummyContext = class(TisInterfacedObject, IevContext)
  private
    FDataAccess: IevDataAccess;
  protected
    function  GetID: TisGUID;
    function  GetRemoteID: TisGUID;
    function  Callbacks: IevContextCallbacks;
    procedure SetCallbacks(const AValue: IevContextCallbacks);
    function  RunParallelTask(const Task: PTaskProc; const Params: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal; const AThreadManager: IThreadManager = nil): TTaskID; overload;
    function  RunParallelTask(const Task: PTaskProcObj; const AObject: TObject; const AParams: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal; const AThreadManager: IThreadManager = nil): TTaskID; overload;
    procedure WaitForTaskEnd(const ATaskID: TTaskID = 0);
    function  TaskIsFinished(const ATaskID: TTaskID = 0): Boolean;
    function  UserAccount: IevUserAccount;
    function  GetScreenPackage(const AModuleType: TGUID): IevScreenPackage;
    function  EMailer: IevEMailer;
    function  CashManagement: IevCashManagement;
    function  EvolutionGUI: IevEvolutionGUI;
    function  DataAccess: IevDataAccess;
    function  DBAccess: IevDBAccess;
    function  Security: IevSecurity;
    function  DomainInfo: IevDomainInfo;
    function  PayrollCalculation: IevPayrollCalculation;
    function  PayrollProcessing: IevPayrollProcessing;
    function  PayrollCheckPrint: IevPayrollCheckPrint;
    function  RemoteMiscRoutines: IevRemoteMiscRoutines;
    function  VmrEngine: IevVmrEngine;
    function  VmrRemote: IevVmrRemote;
    function  RWDesigner: IevRWDesigner;
    function  RWRemoteEngine: IevRWRemoteEngine;
    function  RWLocalEngine: IevRWLocalEngine;
    function  License: IevLicense;
    function  EvolutionExchange: IEvExchange;
    function  EvolutionExchangeEngine: IEvExchangeEngine;
    function  UserScheduler: IevUserScheduler;
    function  Statistics: IevStatistics;
    function  SingleSignOn: IevSingleSignOn;
  end;


  TevDummyContextCallbacks = class(TisInterfacedObject, IevContextCallbacks)
  protected
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure TaskQueueEvent(const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
    procedure GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
  end;


var DummyContextCallbacks: IevContextCallbacks;
var ContextManager: TevContextManager;

threadvar
  tvCurrentThreadContext: IevContext; // It gets cleared by DestroyThreadContext
  tvThreadContextStack: IisList;

procedure OnCreateThread;
begin
  tvCurrentThreadContext := nil;
  tvThreadContextStack := nil;
end;

procedure OnDestroyThread;
begin
  tvThreadContextStack := nil;
  tvCurrentThreadContext := nil;
end;


function  Context: IevContext;
begin
  Result := tvCurrentThreadContext;
end;


procedure ctx_StartWait(const AText: string = ''; const AMaxProgress: Integer = 0);
begin
  if Context.Callbacks <> nil then
    Context.Callbacks.StartWait(AText, AMaxProgress);
end;

procedure ctx_UpdateWait(const AText: string = ''; const ACurrentProgress: Integer = 0;  const AMaxProgress: Integer = -1);
begin
  if Context.Callbacks <> nil then
    Context.Callbacks.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;


procedure ctx_EndWait;
begin
  if Context.Callbacks <> nil then
    Context.Callbacks.EndWait;
end;


function  ctx_EvolutionGUI: IevEvolutionGUI;
begin
  Result := Context.EvolutionGUI;
end;


function  ctx_DataAccess: IevDataAccess;
begin
  Result := Context.DataAccess;
end;


function  ctx_DBAccess: IevDBAccess;
begin
  Result := Context.DBAccess;
end;


function  ctx_Security: IevSecurity;
begin
  Result := Context.Security;
end;


function  ctx_DomainInfo: IevDomainInfo;
begin
  Result := Context.DomainInfo;
end;


function  ctx_AccountRights: IevSecAccountRights;
begin
  Result := ctx_Security.AccountRights;
end;


function  ctx_PayrollCalculation:IevPayrollCalculation;
begin
  Result := Context.PayrollCalculation;
end;

function  ctx_PayrollProcessing: IevPayrollProcessing;
begin
  Result := Context.PayrollProcessing;
end;


function  ctx_PayrollCheckPrint: IevPayrollCheckPrint;
begin
  Result := Context.PayrollCheckPrint;
end;

function  ctx_RemoteMiscRoutines: IevRemoteMiscRoutines;
begin
  Result := Context.RemoteMiscRoutines;
end;

function ctx_VmrEngine: IevVmrEngine;
begin
  Result := Context.VmrEngine;
end;

function ctx_VmrRemote: IevVmrRemote;
begin
  Result := Context.VmrRemote;
end;

function ctx_RWDesigner: IevRWDesigner;
begin
  Result := Context.RWDesigner;
end;

function ctx_RWRemoteEngine: IevRWRemoteEngine;
begin
  Result := Context.RWRemoteEngine;
end;

function ctx_CashManagement: IevCashManagement;
begin
  Result := Context.CashManagement;
end;

function ctx_RWLocalEngine: IevRWLocalEngine;
begin
  Result := Context.RWLocalEngine;
end;


function ctx_eMailer: IevEMailer;
begin
  Result := Context.eMailer;
end;

function  ctx_Statistics: IevStatistics;
begin
  Result := Context.Statistics;
end;


{ TevContextManager }

procedure TevContextManager.AddContextToList(const AContext: TObject);
begin
  Lock;
  try
    FContextList.AddObject(TevContext(AContext).GetID, AContext);
  finally
    Unlock;
  end;
end;

function TevContextManager.CopyContext(const ASource: IevContext): IevContext;
begin
  Result := TevContext.Create('', '', '', '', '');
  TevContext((Result as IisInterfacedObject).GetImplementation).FUserAccount :=
    TevContext((ASource  as IisInterfacedObject).GetImplementation).FUserAccount;
end;

function TevContextManager.CreateContext(const AUser, APassword: String;
  const AID: TisGUID = ''; const ARemoteID: TisGUID = ''): IevContext;
begin
  StoreThreadContext;
  try
    Result := DoCreateThreadContext(AUser, APassword, AID, ARemoteID);
  finally
    RestoreThreadContext;
  end;
end;

function TevContextManager.CreateThreadContext(const AUser, APassword: String;
  const AID: TisGUID = ''; const ARemoteID: TisGUID = ''): IevContext;
begin
  Result := DoCreateThreadContext(AUser, APassword, AID, ARemoteID);
end;

destructor TevContextManager.Destroy;
begin
  FreeAndNil(FContextList);
  inherited;
  ContextManager := nil;
end;

procedure TevContextManager.DestroyThreadContext;
begin
  SetThreadContext(nil);
end;

function TevContextManager.DoCreateThreadContext(const AUser, APassword: String;
  const AID, ARemoteID: TisGUID): IevContext;
var
  sDomain, sUser: String;
  PrevCtx: IevContext;
begin
  DecodeUserAtDomain(AUser, sUser, sDomain);
  Result := TevContext.Create(sDomain, sUser, APassword, AID, ARemoteID);
  PrevCtx := SetThreadContext(Result);
  try
    TevContext((Result as IisInterfacedObject).GetImplementation).FUserAccount := Result.Security.GetAuthorization;
  except
    SetThreadContext(PrevCtx);
    raise;
  end;
end;

procedure TevContextManager.DoOnConstruction;
begin
  inherited;
  InitLock;
  
  FContextList := TStringList.Create;
  FContextList.Duplicates := dupError;
  FContextList.CaseSensitive := False;
  FContextList.Sorted := True;

  ContextManager := Self;  
end;

function TevContextManager.FindContextByID(const AID: TisGUID): IevContext;
var
  i: Integer;
begin
  Lock;
  try
    i := FContextList.IndexOf(AID);
    if i <> -1 then
      Result :=  TevContext(FContextList.Objects[i])
    else
      Result := nil;
  finally
    Unlock;
  end;
end;

function TevContextManager.FindDomainContexts(const ADomain: String): IisList;
var
  i: Integer;
  C: IevContext;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to FContextList.Count - 1 do
    begin
      C := GetContextFromList(i);
      if AnsiSameText(C.UserAccount.Domain, ADomain) then
        Result.Add(C);
    end;
  finally
    Unlock;
  end;
end;

function TevContextManager.FindUserContexts(const AUser: String): IisList;
var
  i: Integer;
  C: IevContext;
  sUser, sDomain: String;
begin
  DecodeUserAtDomain(AUser, sUser, sDomain);
  Result := TisList.Create;
  Lock;
  try
    for i := 0 to FContextList.Count - 1 do
    begin
      C := GetContextFromList(i);
      if AnsiSameText(C.UserAccount.User, sUser) and AnsiSameText(C.UserAccount.Domain, sDomain) then
        Result.Add(C);
    end;
  finally
    Unlock;
  end;
end;

function TevContextManager.GetContextFromList(const AIndex: Integer): IevContext;
begin
  Lock;
  try
    Result := TevContext(FContextList.Objects[AIndex]);
  finally
    Unlock;
  end;
end;

function TevContextManager.GetThreadContext: IevContext;
begin
  Result := tvCurrentThreadContext;
end;

procedure TevContextManager.RemoveContextFromList(const AContext: TObject);
var
  i: Integer;
begin
  Lock;
  try
    i := FContextList.IndexOf(TevContext(AContext).GetID);
    if i <> -1 then
      FContextList.Delete(i);
  finally
    Unlock;
  end;
end;

procedure TevContextManager.RestoreThreadContext;
begin
  SetThreadContext(tvThreadContextStack[tvThreadContextStack.Count - 1] as IevContext);
  tvThreadContextStack.Delete(tvThreadContextStack.Count - 1);
end;

function TevContextManager.SetThreadContext(const AContext: IevContext): IevContext;
begin
  Result := tvCurrentThreadContext;
  tvCurrentThreadContext := AContext;
end;



procedure TevContextManager.StoreThreadContext;
begin
  if not Assigned(tvThreadContextStack) then
    tvThreadContextStack := TisList.Create;
  tvThreadContextStack.Add(Context);
end;

{ TevContext }

function TevContext.DataAccess: IevDataAccess;
begin
  if not Assigned(FDataAccess) then
    FDataAccess := CreateModule(IevDataAccess) as IevDataAccess;

  Result := FDataAccess;
end;

function TevContext.DBAccess: IevDBAccess;
begin
  if not Assigned(FDBAccess) then
    FDBAccess := CreateModule(IevDBAccess) as IevDBAccess;

  Result := FDBAccess;
end;

function TevContext.EvolutionGUI: IevEvolutionGUI;
begin
  if not Assigned(FEvolutionGUI) then
    FEvolutionGUI := CreateModule(IevEvolutionGUI) as IevEvolutionGUI;

  Result := FEvolutionGUI;
end;


function TevContext.DomainInfo: IevDomainInfo;
begin
  if not Assigned(FDomainInfo) then
  begin
    FDomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(UserAccount.Domain);
    CheckCondition(Assigned(FDomainInfo), 'Evolution domain "' + UserAccount.Domain + '" is not registered');
  end;

  Result := FDomainInfo;
end;

function TevContext.Security: IevSecurity;
begin
  if not Assigned(FSecurity) then
    FSecurity := CreateModule(IevSecurity) as IevSecurity;

  Result := FSecurity;
end;

function TevContext.UserAccount: IevUserAccount;
begin
  Result := FUserAccount;
end;



constructor TevContext.Create(const ADomain, AUser, APassword, AID, ARemoteID: TisGUID);
begin
  if AID <> '' then
    FID := AID
  else
    FID := GetUniqueID;

  if ARemoteID = '' then
    FRemoteID := FID
  else
    FRemoteID := ARemoteID;  
     
  FUserAccount := TevUserAccount.Create(ADomain, AUser, APassword);
  ContextManager.AddContextToList(Self);
end;


function TevContext.PayrollCalculation: IevPayrollCalculation;
begin
  if not Assigned(FPayrollCalculation) then
    FPayrollCalculation := CreateModule(IevPayrollCalculation) as IevPayrollCalculation;

  Result := FPayrollCalculation;
end;

function TevContext.PayrollProcessing: IevPayrollProcessing;
begin
  if not Assigned(FPayrollProcessing) then
    FPayrollProcessing := CreateModule(IevPayrollProcessing) as IevPayrollProcessing;

  Result := FPayrollProcessing;
end;

function TevContext.RWDesigner: IevRWDesigner;
begin
  if not Assigned(FRWDesigner) then
    FRWDesigner := CreateModule(IevRWDesigner) as IevRWDesigner;

  Result := FRWDesigner;
end;

function TevContext.RWLocalEngine: IevRWLocalEngine;
begin
  if not Assigned(FRWLocalEngine) then
    FRWLocalEngine := CreateModule(IevRWLocalEngine) as IevRWLocalEngine;

  Result := FRWLocalEngine;
end;

function TevContext.RWRemoteEngine: IevRWRemoteEngine;
begin
  if not Assigned(FRWRemoteEngine) then
    FRWRemoteEngine := CreateModule(IevRWRemoteEngine) as IevRWRemoteEngine;

  Result := FRWRemoteEngine;
end;

function TevContext.VmrEngine: IevVmrEngine;
begin
  if not Assigned(FVmrEngine) then
    FVmrEngine := CreateModule(IevVmrEngine,true) as IevVmrEngine;

  Result := FVmrEngine;
end;

function TevContext.Callbacks: IevContextCallbacks;
begin
  if not Assigned(FCallbacks) then
  begin
    FCallbacks := CreateModule(IevContextCallbacks, True) as IevContextCallbacks;
    if not Assigned(FCallbacks) then
      FCallbacks := DummyContextCallbacks;
  end;

  Result := FCallbacks;
end;

function TevContext.GetID: TisGUID;
begin
  Result := FID;
end;

destructor TevContext.Destroy;
begin
  if Assigned(ContextManager) then
    ContextManager.RemoveContextFromList(Self);
  inherited;
end;

procedure TevContext.SetCallbacks(const AValue: IevContextCallbacks);
begin
  FCallbacks := AValue;
end;


function TevContext.RunParallelTask(const Task: PTaskProc; const AParams: TTaskParamList; const ATaskDescription: String;
  const APriority: TThreadPriority; const AThreadManager: IThreadManager): TTaskID;
var
  Params: TTaskParamList;
  TM: IThreadManager;
begin
  if Assigned(AThreadManager) then
    TM := AThreadManager
  else
    TM := GlobalThreadManager;

  Params := AParams;
  SetLength(Params, Length(Params) + 2);
  Params[High(Params) - 1] := Integer(Pointer(@Task));
  Params[High(Params)] := Mainboard.ContextManager.CopyContext(Context);
  Result := TM.RunTask(DoRunParallelTask, Self, Params, ATaskDescription, APriority);
end;

procedure TevContext.WaitForTaskEnd(const ATaskID: TTaskID);
begin
  GlobalThreadManager.WaitForTaskEnd(ATaskID);
end;

procedure TevContext.DoRunParallelTask(const AParams: TTaskParamList);
var
  Ctx: IevContext;
  Params: TTaskParamList;
  Proc: PTaskProc;
begin
  Proc := PTaskProc(Pointer(Integer(AParams[High(AParams) - 1])));
  Ctx := IInterface(AParams[High(AParams)]) as IevContext;

  Mainboard.ContextManager.SetThreadContext(Ctx);
  try
    Params := AParams;
    SetLength(Params, Length(Params) - 2);
    Proc(Params);
  finally
    Mainboard.ContextManager.DestroyThreadContext;
  end;
end;

function TevContext.RunParallelTask(const Task: PTaskProcObj; const AObject: TObject; const AParams: TTaskParamList;
  const ATaskDescription: String; const APriority: TThreadPriority; const AThreadManager: IThreadManager): TTaskID;
var
  Params: TTaskParamList;
  TM: IThreadManager;
begin
  if Assigned(AThreadManager) then
    TM := AThreadManager
  else
    TM := GlobalThreadManager;

  Params := AParams;
  SetLength(Params, Length(Params) + 3);
  Params[High(Params) - 2] := Integer(Pointer(@Task));
  Params[High(Params) - 1] := Integer(Pointer(AObject));
  Params[High(Params)] := Mainboard.ContextManager.CopyContext(Context);
  Result := TM.RunTask(DoRunParallelTaskObj, Self, Params, ATaskDescription, APriority);
end;

procedure TevContext.DoRunParallelTaskObj(const AParams: TTaskParamList);
var
  Ctx: IevContext;
  Params: TTaskParamList;
  Method: TMethod;
begin
  Method.Code := Pointer(Integer(AParams[High(AParams) - 2]));
  Method.Data := Pointer(Integer(AParams[High(AParams) - 1]));
  Ctx := IInterface(AParams[High(AParams)]) as IevContext;

  Mainboard.ContextManager.SetThreadContext(Ctx);
  try
    Params := AParams;
    SetLength(Params, Length(Params) - 3);
    PTaskProcObj(Method)(Params);
  finally
    Mainboard.ContextManager.DestroyThreadContext;
  end;
end;

function TevContext.PayrollCheckPrint: IevPayrollCheckPrint;
begin
  if not Assigned(FPayrollCheckPrint) then
    FPayrollCheckPrint := CreateModule(IevPayrollCheckPrint) as IevPayrollCheckPrint;

  Result := FPayrollCheckPrint;
end;

function TevContext.EMailer: IevEMailer;
begin
  if not Assigned(FEMailer) then
    FEMailer := CreateModule(IevEMailer) as IevEMailer;

  Result := FEMailer;
end;


function TevContext.CreateModule(const AModuleType: TGUID; const ADontError: Boolean = False): IInterface;
begin
  Result := Mainboard.ModuleRegister.CreateModuleInstance(AModuleType, ADontError);
end;

function TevContext.GetScreenPackage(const AModuleType: TGUID): IevScreenPackage;
begin
  // Screen modules are singe instance only. The reference get cached inside of implementation unit.
  Result := CreateModule(AModuleType) as IevScreenPackage;
end;

function TevContext.RemoteMiscRoutines: IevRemoteMiscRoutines;
begin
  if not Assigned(FRemoteMiscRoutines) then
    FRemoteMiscRoutines := CreateModule(IevRemoteMiscRoutines) as IevRemoteMiscRoutines;

  Result := FRemoteMiscRoutines;
end;

function TevContext.License: IevLicense;
begin
  if not Assigned(FLicense) then
    FLicense := CreateModule(IevLicense) as IevLicense;

  Result := FLicense;
end;

function TevContext.VmrRemote: IevVmrRemote;
begin
  if not Assigned(FVmrRemote) then
    FVmrRemote := CreateModule(IevVmrRemote) as IevVmrRemote;

  Result := FVmrRemote;
end;


function TevContext.GetRemoteID: TisGUID;
begin
  Result := FRemoteID;
end;

function TevContext.TaskIsFinished(const ATaskID: TTaskID): Boolean;
begin
  Result := GlobalThreadManager.TaskIsTerminated(ATaskID);
end;

function TevContext.EvolutionExchange: IEvExchange;
begin
  if not Assigned(FEvolutionExchange) then
    FEvolutionExchange := CreateModule(IevExchange) as IevExchange;

  Result := FEvolutionExchange;
end;

function TevContext.EvolutionExchangeEngine: IEvExchangeEngine;
begin
  if not Assigned(FEvolutionExchangeEngine) then
    FEvolutionExchangeEngine := CreateModule(IevExchangeEngine) as IevExchangeEngine;

  Result := FEvolutionExchangeEngine;
end;

function TevContext.UserScheduler: IevUserScheduler;
begin
  if not Assigned(FUserScheduler) then
    FUserScheduler := CreateModule(IevUserScheduler) as IevUserScheduler;

  Result := FUserScheduler;
end;

function TevContext.Statistics: IevStatistics;
begin
  if not Assigned(FStatistics) then
    FStatistics := CreateModule(IevStatistics) as IevStatistics;

  Result := FStatistics;
end;

function TevContext.CashManagement: IevCashManagement;
begin
  if not Assigned(FCashManagement) then
    FCashManagement := CreateModule(IevCashManagement) as IevCashManagement;

  Result := FCashManagement;
end;

function TevContext.SingleSignOn: IevSingleSignOn;
begin
  if not Assigned(FSingleSignOn) then
    FSingleSignOn := CreateModule(IevSingleSignOn) as IevSingleSignOn;

  Result := FSingleSignOn;
end;

{ TevDummyContext }

function TevDummyContext.Callbacks: IevContextCallbacks;
begin
  Result := nil;
end;

function TevDummyContext.DataAccess: IevDataAccess;
begin
  if not Assigned(FDataAccess) then
    FDataAccess := Mainboard.ModuleRegister.CreateModuleInstance(IevDataAccess, False) as IevDataAccess;

  Result := FDataAccess;
end;

function TevDummyContext.DBAccess: IevDBAccess;
begin
  Result := nil;
end;

function TevDummyContext.DomainInfo: IevDomainInfo;
begin
  Result := nil;
end;

function TevDummyContext.EMailer: IevEMailer;
begin
  Result := nil;
end;

function TevDummyContext.EvolutionGUI: IevEvolutionGUI;
begin
  Result := nil;
end;

function TevDummyContext.GetID: TisGUID;
begin
  Result := '';
end;

function TevDummyContext.GetScreenPackage(const AModuleType: TGUID): IevScreenPackage;
begin
  Result := nil;
end;

function TevDummyContext.PayrollCalculation: IevPayrollCalculation;
begin
  Result := nil;
end;

function TevDummyContext.PayrollCheckPrint: IevPayrollCheckPrint;
begin
  Result := nil;
end;

function TevDummyContext.PayrollProcessing: IevPayrollProcessing;
begin
  Result := nil;
end;

function TevDummyContext.RunParallelTask(const Task: PTaskProc; const Params: TTaskParamList;
  const ATaskDescription: String; const APriority: TThreadPriority; const AThreadManager: IThreadManager): TTaskID;
begin
  Result := 0;
  Assert(False);
end;

function TevDummyContext.RemoteMiscRoutines: IevRemoteMiscRoutines;
begin
  Result := nil;
end;

function TevDummyContext.RunParallelTask(const Task: PTaskProcObj; const AObject: TObject; const AParams: TTaskParamList; const ATaskDescription: String = ''; const APriority: TThreadPriority = tpNormal; const AThreadManager: IThreadManager = nil): TTaskID;
begin
  Result := 0;
  Assert(False);
end;

function TevDummyContext.RWDesigner: IevRWDesigner;
begin
  Result := nil;
end;

function TevDummyContext.RWLocalEngine: IevRWLocalEngine;
begin
  Result := nil;
end;

function TevDummyContext.RWRemoteEngine: IevRWRemoteEngine;
begin
  Result := nil;
end;

function TevDummyContext.Security: IevSecurity;
begin
  Result := nil;
end;

procedure TevDummyContext.SetCallbacks(const AValue: IevContextCallbacks);
begin
end;

function TevDummyContext.UserAccount: IevUserAccount;
begin
  Result := nil;
end;

function TevDummyContext.VmrEngine: IevVmrEngine;
begin
  Result := nil;
end;

procedure TevDummyContext.WaitForTaskEnd(const ATaskID: TTaskID);
begin
  Assert(False);
end;

function TevDummyContext.License: IevLicense;
begin
  Result := nil;
end;

function TevDummyContext.VmrRemote: IevVmrRemote;
begin
  Result := nil;
end;

function TevDummyContext.GetRemoteID: TisGUID;
begin
  Result := '';
end;

function TevDummyContext.TaskIsFinished(const ATaskID: TTaskID): Boolean;
begin
  Result := True;
  Assert(False);
end;

function TevDummyContext.EvolutionExchange: IEvExchange;
begin
  Result := nil;
end;

function TevDummyContext.EvolutionExchangeEngine: IEvExchangeEngine;
begin
  Result := nil;
end;

function TevDummyContext.UserScheduler: IevUserScheduler;
begin
  Result := nil;
end;

function TevDummyContext.Statistics: IevStatistics;
begin
  Result := nil;
end;

function TevDummyContext.CashManagement: IevCashManagement;
begin
  Result := nil;
end;

function TevDummyContext.SingleSignOn: IevSingleSignOn;
begin
  Result := nil;
end;

{ TevDummyContextCallbacks }

procedure TevDummyContextCallbacks.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
begin
end;

procedure TevDummyContextCallbacks.EndWait;
begin
end;

procedure TevDummyContextCallbacks.GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
end;

procedure TevDummyContextCallbacks.StartWait(const AText: string; AMaxProgress: Integer);
begin
end;

procedure TevDummyContextCallbacks.TaskQueueEvent(const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
begin
end;

procedure TevDummyContextCallbacks.UpdateWait(const AText: string; ACurrentProgress: Integer; AMaxProgress: Integer);
begin
end;

procedure TevDummyContextCallbacks.UserSchedulerEvent(const AEventID, ASubject: String;
  const AScheduledTime: TDateTime);
begin
end;

initialization
  RegisterTMCallbacks(OnCreateThread, OnDestroyThread);
  tvCurrentThreadContext := TevDummyContext.Create;
  DummyContextCallbacks := TevDummyContextCallbacks.Create;

finalization
  UnregisterTMCallbacks(OnCreateThread, OnDestroyThread);
  tvCurrentThreadContext := nil;
  tvThreadContextStack := nil;

end.

