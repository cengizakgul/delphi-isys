unit SACHTypes;

interface

uses
  classes, EvCommonInterfaces, EvMainboard, EvClasses;

const
  OFF_DONT_USE = 0;
  OFF_ONLY_EE_DD = 1;
  OFF_USE_OFFSETS = 2;

  TRAN_TYPE_COMPANY = '0';
  TRAN_TYPE_OFFSET = '1';
  TRAN_TYPE_OUT = '2';

type
  TAchCombineOptions = (COMBINE_DO_NOT, COMBINE_CO_NO_DD, COMBINE_CO_AND_SB, COMBINE_CO_W_DD, COMBINE_CO_WP_DD);

type
  TACHOptions = class(TPersistent) //to get streaming
  private
    FBlockDebits: boolean;
    FFedReserve: boolean;
    FBlockSBCredit: boolean;
    FIntercept: boolean;
    FCTS: boolean;
    FBalanceBatches: boolean;
    FUseSBEIN: boolean;
    FCombineTrans: Integer;
    FPayrollHeader: boolean;
    FEffectiveDate: boolean;
    FDescReversal: boolean;
    FSunTrust: boolean;
    FBankOne: boolean;
    FCombineThreshold: Currency;
    FCoId: string;
    FHeader: string;
    FSBEINOverride: string;
    FFooter: string;
    FDisplayCoNbrAndName: Boolean;
    FPostProcessReportName: String;
    function GetCombine: TAchCombineOptions;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Assign2(const Source: IevACHOptions);
    procedure AssignTo2(const Target: IevACHOptions);
  published
    property BalanceBatches: boolean read FBalanceBatches write FBalanceBatches; // cbBalanceBatches; IsColumbiaEDP
    property BlockDebits: boolean  read FBlockDebits write FBlockDebits; // cbBlockDebits
    property DisplayCoNbrAndName: Boolean read FDisplayCoNbrAndName write FDisplayCoNbrAndName;
    property CombineTrans: Integer  read FCombineTrans write FCombineTrans; // cbCombine
    property Combine: TAchCombineOptions read GetCombine;
    property UseSBEIN: boolean  read FUseSBEIN write FUseSBEIN; // cbUseSBEIN
    property CTS: boolean  read FCTS write FCTS; // cbCTS
    property SunTrust: boolean  read FSunTrust write FSunTrust; // cbSunTrust
    property BlockSBCredit: boolean  read FBlockSBCredit write FBlockSBCredit; // cbBlockSBCredit
    property FedReserve: boolean  read FFedReserve write FFedReserve; // cbFedReserve
    property Intercept: boolean  read FIntercept write FIntercept; // cbIntercept
    property PayrollHeader: boolean  read FPayrollHeader write FPayrollHeader; // cbPayrollHeader
    property DescReversal: boolean  read FDescReversal write FDescReversal; // cbDescReversal
    property BankOne: boolean  read FBankOne write FBankOne; // cbBankOne
    property EffectiveDate: boolean  read FEffectiveDate write FEffectiveDate; // cbEffectiveDate

    property CombineTreshold: Currency  read FCombineThreshold write FCombineThreshold; // eThreshold   !!
    property CoId: string  read FCoId write FCoId; // eCoId
    property SBEINOverride: string  read FSBEINOverride write FSBEINOverride; // eSBEINOverride
    property Header: string  read FHeader write FHeader; // eHeader
    property Footer: string  read FFooter write FFooter; // eFooter
    property PostProcessReportName: String read FPostProcessReportName write FPostProcessReportName;
  end;

procedure LoadACHOptionsFromRegistry(const ACHOptions: TACHOptions);
procedure LoadACHOptionsFromRegistryI(const ACHOptions: IevACHOptions);

implementation

uses
  typinfo, sysutils, EvUtils;

procedure TACHOptions.Assign(Source: TPersistent);
begin
  if Source is TACHOptions then
  begin
    FBalanceBatches := TACHOptions(Source).FBalanceBatches;
    FBlockDebits := TACHOptions(Source).FBlockDebits;
    FCombineTrans := TACHOptions(Source).FCombineTrans;
    FUSESBEIN := TACHOptions(Source).FUSESBEIN;
    FCTS := TACHOptions(Source).FCTS;
    FSunTrust := TACHOptions(Source).FSunTrust;
    FBlockSBCredit := TACHOptions(Source).FBlockSBCredit;
    FFedReserve := TACHOptions(Source).FFedReserve;
    FIntercept := TACHOptions(Source).FIntercept;
    FPayrollHeader := TACHOptions(Source).FPayrollHeader;
    FDescReversal := TACHOptions(Source).FDescReversal;
    FBankOne := TACHOptions(Source).FBankOne;
    FEffectiveDate := TACHOptions(Source).FEffectiveDate;

    FCombineThreshold := TACHOptions(Source).FCombineThreshold;
    FCoId := TACHOptions(Source).FCoId;
    FSBEINOverride := TACHOptions(Source).FSBEINOverride;
    FHeader := TACHOptions(Source).FHeader;
    FFooter := TACHOptions(Source).FFooter;
    FDisplayCoNbrAndName := TACHOptions(Source).FDisplayCoNbrAndName;
  end
  else
    inherited;
end;

procedure TACHOptions.Assign2(const Source: IevACHOptions);
begin
  inherited;
  FBalanceBatches := Source.BalanceBatches;
  FBlockDebits := Source.BlockDebits;
  FCombineTrans := Source.CombineTrans;
  FUseSBEIN := Source.UseSBEIN;
  FCTS := Source.CTS;
  FSunTrust := Source.SunTrust;
  FBlockSBCredit := Source.BlockSBCredit;
  FFedReserve := Source.FedReserve;
  FIntercept := Source.Intercept;
  FPayrollHeader := Source.PayrollHeader;
  FDescReversal := Source.DescReversal;
  FBankOne := Source.BankOne;
  FEffectiveDate := Source.EffectiveDate;
  FCombineThreshold := Source.CombineThreshold;
  FCoId := Source.CoId;
  FSBEINOverride := Source.SBEINOverride;
  FHeader := Source.Header;
  FFooter := Source.Footer;
  FDisplayCoNbrAndName := Source.DisplayCoNbrAndName;
  FPostProcessReportName := Source.PostProcessReportName;
end;

procedure TACHOptions.AssignTo2(const Target: IevACHOptions);
begin
  Target.BalanceBatches := FBalanceBatches;
  Target.BlockDebits := FBlockDebits;
  Target.CombineTrans := FCombineTrans;
  Target.UseSBEIN := FUseSBEIN;
  Target.CTS := FCTS;
  Target.SunTrust := FSunTrust;
  Target.BlockSBCredit := FBlockSBCredit;
  Target.FedReserve := FFedReserve;
  Target.Intercept := FIntercept;
  Target.PayrollHeader := FPayrollHeader;
  Target.DescReversal := FDescReversal;
  Target.BankOne := FBankOne;
  Target.EffectiveDate := FEffectiveDate;
  Target.CombineThreshold := FCombineThreshold;
  Target.CoId := FCoId;
  Target.SBEINOverride := FSBEINOverride;
  Target.Header := FHeader;
  Target.Footer := FFooter;
  Target.DisplayCoNbrAndName := FDisplayCoNbrAndName;
end;

procedure LoadACHOptionsFromRegistry(const ACHOptions: TACHOptions);

  function ReadBool(keyname: string): boolean;
  begin
    Result := mb_AppSettings.AsBoolean[keyname];
  end;

begin
  if not Unattended then
  begin
    ACHOptions.FBalanceBatches := ReadBool('BalanceACHBatches');
    ACHOptions.FBlockDebits := ReadBool('ACHBlockDebits');
    ACHOptions.FCombineTrans := mb_AppSettings.AsInteger['CombineACHTrans'];
    ACHOptions.FUSESBEIN := ReadBool('USESBEINForACH');
    ACHOptions.FCTS := ReadBool('ACHCTS');
    ACHOptions.FSunTrust := ReadBool('ACHSunTrust');
    ACHOptions.FBlockSBCredit := ReadBool('ACHBlockSBCredit');
    ACHOptions.FFedReserve := ReadBool('ACHFedReserve');
    ACHOptions.FIntercept := ReadBool('ACHIntercept');
    ACHOptions.FPayrollHeader := ReadBool('ACHPayrollHeader');
    ACHOptions.FDescReversal := ReadBool('ACHDescReversal');
    ACHOptions.FBankOne := ReadBool('ACHBankOne');
    ACHOptions.FEffectiveDate := ReadBool('ACHEffectiveDate');
    ACHOptions.FDisplayCoNbrAndName := ReadBool('DisplayCoNbrAndName');

    ACHOptions.FCombineThreshold := mb_AppSettings.AsFloat['CombineACHTreshold'];
    ACHOptions.FCoId := mb_AppSettings.GetValue('ACHCoId', '1');
    ACHOptions.FSBEINOverride := mb_AppSettings['ACHSBEINOverride'];
    ACHOptions.FHeader := mb_AppSettings['ACHHeader'];
    ACHOptions.FFooter := mb_AppSettings['ACHFooter'];
    ACHOptions.FPostProcessReportName := mb_AppSettings.AsString['PostProcessACHReport'];
  end
  else
  begin
    ACHOptions.FBalanceBatches := False;
    ACHOptions.FBlockDebits := False;
    ACHOptions.FCombineTrans := 0;
    ACHOptions.FUSESBEIN := False;
    ACHOptions.FCTS := False;
    ACHOptions.FSunTrust := False;
    ACHOptions.FBlockSBCredit := False;
    ACHOptions.FFedReserve := False;
    ACHOptions.FIntercept := False;
    ACHOptions.FPayrollHeader := False;
    ACHOptions.FDescReversal := False;
    ACHOptions.FBankOne := False;
    ACHOptions.FEffectiveDate := False;

    ACHOptions.FCombineThreshold := 0;
    ACHOptions.FCoId := '1';
    ACHOptions.FSBEINOverride := '';
    ACHOptions.FHeader := '';
    ACHOptions.FFooter := '';
  end;
end;

procedure LoadACHOptionsFromRegistryI(const ACHOptions: IevACHOptions);

  function ReadBool(keyname: string): boolean;
  begin
    Result := mb_AppSettings.AsBoolean[keyname];
  end;

begin
  ACHOptions.SetBalanceBatches(ReadBool('BalanceACHBatches'));
  ACHOptions.SetBlockDebits(ReadBool('ACHBlockDebits'));
  ACHOptions.SetCombineTrans(mb_AppSettings.AsInteger['CombineACHTrans']);
  ACHOptions.SetUSESBEIN(ReadBool('USESBEINForACH'));
  ACHOptions.SetCTS(ReadBool('ACHCTS'));
  ACHOptions.SetSunTrust(ReadBool('ACHSunTrust'));
  ACHOptions.SetBlockSBCredit(ReadBool('ACHBlockSBCredit'));
  ACHOptions.SetFedReserve(ReadBool('ACHFedReserve'));
  ACHOptions.SetIntercept(ReadBool('ACHIntercept'));
  ACHOptions.SetPayrollHeader(ReadBool('ACHPayrollHeader'));
  ACHOptions.SetDescReversal(ReadBool('ACHDescReversal'));
  ACHOptions.SetBankOne(ReadBool('ACHBankOne'));
  ACHOptions.SetEffectiveDate(ReadBool('ACHEffectiveDate'));
  ACHOptions.SetDisplayCoNbrAndName(ReadBool('DisplayCoNbrAndName'));

  ACHOptions.SetCombineThreshold(mb_AppSettings.AsFloat['CombineACHTreshold']);
  ACHOptions.SetCoId(mb_AppSettings.GetValue('ACHCoId', '1'));
  ACHOptions.SetSBEINOverride(mb_AppSettings['ACHSBEINOverride']);
  ACHOptions.SetHeader(mb_AppSettings['ACHHeader']);
  ACHOptions.SetFooter(mb_AppSettings['ACHFooter']);
  ACHOptions.SetPostProcessReportName(mb_AppSettings.AsString['PostProcessACHReport']);
end;

function TACHOptions.GetCombine: TAchCombineOptions;
begin
  Result := TAchCombineOptions(FCombineTrans);
end;

end.
