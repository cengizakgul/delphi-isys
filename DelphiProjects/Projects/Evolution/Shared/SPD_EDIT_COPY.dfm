object EDIT_COPY: TEDIT_COPY
  Left = 435
  Top = 141
  Width = 514
  Height = 382
  Caption = 'Copy'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 498
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object evPageControl1: TevPageControl
      Left = 0
      Top = 0
      Width = 498
      Height = 344
      ActivePage = tshCopy
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 0
      object tshCopy: TTabSheet
        Caption = 'tshCopy'
        DesignSize = (
          490
          313)
        object wwgdCopy: TevDBGrid
          Left = 0
          Top = 0
          Width = 498
          Height = 281
          DisableThemesInTitle = False
          ControlInfoInDataSet = True
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_COPY\wwgdCopy'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = wwdsCopy
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object bttnCancel: TevBitBtn
          Left = 361
          Top = 286
          Width = 129
          Height = 25
          Anchors = [akRight, akBottom]
          Cancel = True
          Caption = 'Cancel'
          ModalResult = 2
          TabOrder = 1
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
            DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
            9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
            DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
            DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
            DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
            91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
            999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
          NumGlyphs = 2
          Margin = 0
        end
        object bttnCopySelected: TevBitBtn
          Left = 225
          Top = 286
          Width = 129
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Copy Selected'
          Default = True
          ModalResult = 1
          TabOrder = 2
          OnClick = bttnCopySelectedClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD6666666666666DDD8888888888888DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6FFF6FFF6FFF6DDD8DDD8DDD8DDD8DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6444644464446DDD8888888888888DDD644464446444
            6DDD8888888888888DDD6444644464446DDD8888888888888DDD677767776777
            6DDD8DDD8DDD8DDD8DDD6F7767776777688D8DFFFFFFFFFFFFFD6F7000000000
            00008DF88888888888886660AAA2AAA2AAA088F8DDD8DDD8DDD86660AAA2AAA2
            AAA088F8DDD8DDD8DDD8DD80AAA2AAA2AAA0DDF8DDD8DDD8DDD8DD8000000000
            0000DDF8888888888888DDD0000000000000DDD8888888888888}
          NumGlyphs = 2
          Margin = 0
        end
        object bttnCopyAll: TevBitBtn
          Left = 89
          Top = 286
          Width = 129
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Copy All'
          ModalResult = 1
          TabOrder = 3
          OnClick = bttnCopyAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD6666666666666DDD8888888888888DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6F7767776777688D8DFFFFFFFFFFFFFD6F7000000000
            00008DF88888888888886F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86660AAA2AAA2AAA088F8DDD8DDD8DDD86660AAA2AAA2
            AAA088F8DDD8DDD8DDD8DD80AAA2AAA2AAA0DDF8DDD8DDD8DDD8DD8000000000
            0000DDF8888888888888DDD0000000000000DDD8888888888888}
          NumGlyphs = 2
          Margin = 0
        end
      end
      object tshExceptions: TTabSheet
        Caption = 'tshExceptions'
        ImageIndex = 1
        OnShow = tshExceptionsShow
        DesignSize = (
          490
          313)
        object wwgdExceptions: TevDBGrid
          Left = 0
          Top = 0
          Width = 498
          Height = 281
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_COPY\wwgdExceptions'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = wwdsExceptions
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object btnDone: TevBitBtn
          Left = 199
          Top = 288
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Done'
          TabOrder = 1
          Margin = 0
        end
        object btnPrint: TevBitBtn
          Left = 336
          Top = 288
          Width = 153
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'Print Exceptions'
          TabOrder = 2
          OnClick = btnPrintClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
    end
  end
  object wwdsCopy: TevDataSource
    DataSet = wwcsCopy
    Left = 416
    Top = 24
  end
  object wwcsCopy: TevClientDataSet
    OnFilterRecord = wwcsCopyFilterRecord
    Filtered = True
    Left = 376
    Top = 24
  end
  object wwcsCoReports: TevClientDataSet
    Left = 376
    Top = 64
  end
  object wwdsCoReports: TevDataSource
    DataSet = wwcsCoReports
    Left = 416
    Top = 64
  end
  object wwcsEDsToCopy: TevClientDataSet
    Left = 376
    Top = 104
  end
  object wwcsCopySecond: TevClientDataSet
    Left = 376
    Top = 144
  end
  object wwdsCopySecond: TevDataSource
    DataSet = wwcsCopySecond
    Left = 416
    Top = 144
  end
  object wwdsExceptions: TevDataSource
    Left = 284
    Top = 27
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 324
    Top = 27
  end
  object wwcsExceptions: TevClientDataSet
    FieldDefs = <
      item
        Name = '141'
        DataType = ftSmallint
      end>
    Left = 244
    Top = 27
  end
  object PrintDialog1: TPrintDialog
    Left = 436
    Top = 251
  end
end
