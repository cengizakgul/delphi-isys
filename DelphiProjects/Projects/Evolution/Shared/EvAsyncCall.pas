unit EvAsyncCall;

interface

uses Classes, SyncObjs, Windows, SysUtils, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvMainboard,
     isThreadManager, EvContext, EvConsts, SReportSettings, EvStreamUtils, Math, isLogFile,
       EvUtils, EvBasicUtils, SDataStructure, EvTypes, isErrorUtils;

type
  IevCallInfo = interface
  ['{D6BCCF06-5555-44A3-80C8-02D13B89BA14}']
    function  GetName: String;
    function  User: String;
    function  StartTime: TDateTime;
    function  GetEvoPriority: Integer;
    procedure SetEvoPriority(const AValue: Integer);
    function  CallID: TisGUID;
    function  TaskID: TTaskID;
    procedure SetLowerPriority;
    procedure SetNormalPriority;
    procedure SetRWEngineRef(const ARWEngine: IevRWRemoteEngine);
    procedure Terminate;
    property  Name: String read GetName;
    property  EvoPriority: Integer read GetEvoPriority write SetEvoPriority;
  end;


  IevCurrentCalls = interface(IisList)
  ['{6E0B4B38-D625-49DC-89C0-17E24AE82159}']
    procedure Lock;
    procedure Unlock;
    function  GetCount: Integer;
    function  GetItem(Index: Integer): IevCallInfo;
    function  AddCallInfo(const AName: String; const ACallID: TisGUID; const ATaskID: TTaskID; const APriority: Integer;
                          const User: String): IevCallInfo;
    function  FindCallInfoByID(const ACallID: TisGUID): IevCallInfo;
    property  Items[Index: Integer]: IevCallInfo read GetItem; default;
    property  Count: Integer read GetCount;
  end;


  TevAsyncCallProc = function (const AParams: IisListOfValues): IisListOfValues of object;
  TevAsyncCallReturnProc = procedure (const ACallID: String; const AResults: IisListOfValues) of object;

  IevAsyncCalls = interface
  ['{F7017ABA-B732-4716-915A-EEA8B7E04F9F}']
    procedure RegisterCall(const AName: String; const AObj: TObject; const AProc: TevAsyncCallProc);
    function  IsRegistered(const AName: String): Boolean;
    function  MakeCall(const AName: String; const AParams: IisListOfValues; const APriority: Integer;
                       var ACallID: TisGUID; const ACallbackObj: TObject = nil;
                       const ACallbackProc: TevAsyncCallReturnProc = nil): Boolean;
    function  SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
    function  GetCalcCapacity: Integer;
    procedure SetCalcCapacity(const AValue: Integer);
    procedure TerminateCall(const ACallID: TisGUID);
    function  GetCurrentCalls: IevCurrentCalls;
    property  CalcCapacity: Integer read GetCalcCapacity write SetCalcCapacity;
  end;


  TevAsyncCalls = class(TisInterfacedObject, IevAsyncCalls)
  private
    FCurrentCalls: IevCurrentCalls;
    FCallRegistry: IisStringList;
    FCalcCapacity: Integer;
    procedure Lock;
    procedure UnLock;
    procedure ExecInThread(const Params: TTaskParamList);
    procedure DeleteCallInfo(const ACallInfo: IevCallInfo);
    procedure AdjustPriorities;
  protected
    procedure DoOnConstruction; override;
    procedure RegisterCall(const AName: String; const AObj: TObject; const AProc: TevAsyncCallProc);
    function  IsRegistered(const AName: String): Boolean;
    function  MakeCall(const AName: String; const AParams: IisListOfValues; const APriority: Integer;
              var ACallID: TisGUID; const ACallbackObj: TObject = nil;
              const ACallbackProc: TevAsyncCallReturnProc = nil): Boolean;
    function  SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
    function  GetCalcCapacity: Integer;
    procedure SetCalcCapacity(const AValue: Integer);
    procedure TerminateCall(const ACallID: TisGUID);
    function  GetCurrentCalls: IevCurrentCalls;
  public
    destructor Destroy; override;
  end;

implementation

type
  IevAsyncCallEntryPoint = interface
  ['{95623CE6-5117-4B0B-ABFE-5B11637174C9}']
    function GetObject: TObject;
    function GetProc: Pointer;
  end;

  TevAsyncCallEntryPoint = class(TisInterfacedObject, IevAsyncCallEntryPoint)
  private
    FObject: TObject;
    FProc: Pointer;
  protected
    function GetObject: TObject;
    function GetProc: Pointer;
  public
    constructor Create(const AObject: TObject; const AProc: Pointer); reintroduce;
  end;


  TevCallInfo = class(TisNamedObject, IevCallInfo)
  private
    FUser: String;
    FStartTime: TDateTime;
    FEvoPriority: Integer;
    FThreadID: Integer;
    FCallID: TisGUID;
    FTaskID: TTaskID;
    FRWEngine: IevRWRemoteEngine;
  protected
    function  User: String;
    function  StartTime: TDateTime;
    function  GetEvoPriority: Integer;
    procedure SetEvoPriority(const AValue: Integer);
    function  CallID: TisGUID;
    function  TaskID: TTaskID;
    procedure SetLowerPriority;
    procedure SetNormalPriority;
    procedure SetRWEngineRef(const ARWEngine: IevRWRemoteEngine);
    procedure Terminate;
  public
    constructor Create(const AName: String; const ACallID: TisGUID; const ATaskID: TTaskID;
                          const APriority: Integer; const AUser: String); reintroduce;
  end;


  TevCurrentCalls = class(TisList, IevCurrentCalls)
  protected
    function  GetItem(Index: Integer): IevCallInfo;
    function  CompareItems(const Item1, Item2: IInterface): Integer; override;
    function  AddCallInfo(const AName: String; const ACallID: TisGUID; const ATaskID: TTaskID;
                          const APriority: Integer; const AUser: String): IevCallInfo;
    function  FindCallInfoByID(const ACallID: TisGUID): IevCallInfo;
    property  Items[Index: Integer]: IevCallInfo read GetItem; default;
  end;


{ TevAsyncCalls }

procedure TevAsyncCalls.DeleteCallInfo(const ACallInfo: IevCallInfo);
begin
  Lock;
  try
    FCurrentCalls.Remove(ACallInfo);
    AdjustPriorities;
  finally
    Unlock;
  end;
end;

destructor TevAsyncCalls.Destroy;
begin
  TerminateCall('');
  inherited;
end;

procedure TevAsyncCalls.DoOnConstruction;
begin
  inherited;

  FCurrentCalls := TevCurrentCalls.Create;
  FCalcCapacity := HowManyProcessors;

  FCallRegistry := TisStringList.CreateAsIndex;
end;

procedure TevAsyncCalls.ExecInThread(const Params: TTaskParamList);
var
  Proc: TMethod;
  EP: IevAsyncCallEntryPoint;
  TaskParams: IisListOfValues;
  ThreadContext: IevContext;
  CallerContext: IevContext;
  CallbackMethod: TMethod;
  Results: IisListOfValues;
  CallInfo: IevCallInfo;
  i: Integer;
  sError: String;
begin
  // Important! This section works synchronically with MakeCall
  Lock;
  try
    for i := 0 to FCurrentCalls.Count - 1 do
    begin
      CallInfo := FCurrentCalls[i];
      if CallInfo.TaskID = GetCurrentThreadTaskID then
        break
      else
        CallInfo := nil;
    end;

    Assert(Assigned(CallInfo));
  finally
    Unlock;
  end;

  Results := nil;
  CallbackMethod.Data := Pointer(Integer(Params[3]));
  CallbackMethod.Code := Pointer(Integer(Params[4]));
  try
    try
      CallerContext := IInterface(Params[1]) as IevContext;
      ThreadContext := Mainboard.ContextManager.CopyContext(CallerContext);
      ThreadContext.Statistics.Enabled := CallerContext.Statistics.Enabled;
      ThreadContext.SetCallbacks(CallerContext.Callbacks);
      Mainboard.ContextManager.SetThreadContext(ThreadContext);
      ThreadContext := nil;

      if ctx_Statistics.Enabled then
      begin
        ctx_Statistics.BeginEvent('Async call');
        ctx_Statistics.ActiveEvent.Properties.AddValue('Function',  Params[5]);
      end;

      try
        EP := IInterface(Params[0]) as IevAsyncCallEntryPoint;
        Proc.Code := EP.GetProc;
        Proc.Data := EP.GetObject;
        TaskParams := IInterface(Params[2]) as IisListOfValues;

        Lock;
        try
          CallInfo.SetRWEngineRef(ctx_RWRemoteEngine);
        finally
          Unlock;
        end;

        try
          sError := '';
          Results := TevAsyncCallProc(Proc)(TaskParams);
        except
          on E: Exception do
          begin
            sError := GetErrorCallStack(E);
            raise;
          end;  
        end;
      finally
        try
          if ctx_Statistics.Enabled and Assigned(Results) then
          begin
            Results.AddValue(PARAM_STATISTICS, ctx_Statistics.ActiveEvent);
            ctx_Statistics.EndEvent;
          end;

          Mainboard.ContextManager.DestroyThreadContext;
        except
          on E: Exception do
          begin
            GlobalLogFile.AddEvent(etFatalError, 'Async Calls', E.Message, 'Primary error:  ' + sError);
            raise;
          end;
        end;
      end;

    except
      on E: Exception do
      begin
        if not CurrentThreadTaskTerminated then
        begin
          if not Assigned(Results) then
            Results := TisListOfValues.Create;

          Results.AddValue(PARAM_ERROR, BuildStackedErrorStr(E));
        end;  
      end;
    end;

  finally
    DeleteCallInfo(CallInfo);
    if not CurrentThreadTaskTerminated then
      if Assigned(CallbackMethod.Code) and Assigned(CallbackMethod.Data) then
        TevAsyncCallReturnProc(CallbackMethod)(CallInfo.CallID, Results)
      else
        CallerContext.Callbacks.AsyncCallReturn(CallInfo.CallID, Results);
  end;
end;

function TevAsyncCalls.GetCalcCapacity: Integer;
begin
  Lock;
  try
    Result := FCalcCapacity;
  finally
    Unlock;
  end;
end;

procedure TevAsyncCalls.Lock;
begin
  FCurrentCalls.Lock;
end;

function TevAsyncCalls.MakeCall(const AName: String; const AParams: IisListOfValues;
  const APriority: Integer; var ACallID: TisGUID; const ACallbackObj: TObject = nil;
  const ACallbackProc: TevAsyncCallReturnProc = nil): Boolean;
var
  i: Integer;
  EP: IevAsyncCallEntryPoint;
  CallInf: IevCallInfo;
  TaskID: TTaskID;
begin
  Result := False;

  Lock;
  try
    if FCurrentCalls.Count >= GetCalcCapacity then
    begin
      // trying to "slip in"
      CallInf := FCurrentCalls[GetCalcCapacity - 1];
      if APriority >= CallInf.EvoPriority then
        exit;
    end;

    i := FCallRegistry.IndexOf(AName);
    CheckCondition(i <> -1, 'Invalid asynchronous call "' + AName + '"');

    EP := FCallRegistry.Objects[i] as IevAsyncCallEntryPoint;

    if ACallID = '' then
      ACallID := GetUniqueID
    else
      CheckCondition(FCurrentCalls.FindCallInfoByID(ACallID) = nil, 'CallID is not unique');

    TaskID := GlobalThreadManager.RunTask(ExecInThread, Self, MakeTaskParams([EP, Context, AParams, ACallbackObj, @ACallbackProc, AName]), 'Async ' + AName);

    if TaskID <> 0 then
    begin
      FCurrentCalls.AddCallInfo(AName, ACallID, TaskID, APriority,
        EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
      AdjustPriorities;
      Result := True;
    end;
  finally
    Unlock;
  end;
end;


procedure TevAsyncCalls.RegisterCall(const AName: String; const AObj: TObject; const AProc: TevAsyncCallProc);
var
  P: IevAsyncCallEntryPoint;
begin
  P := TevAsyncCallEntryPoint.Create(AObj, @AProc);
  FCallRegistry.AddObject(AName, P);
end;

procedure TevAsyncCalls.AdjustPriorities;
var
  i: Integer;
  CallInf: IevCallInfo;
begin
  Lock;
  try
    for i := 0 to FCurrentCalls.Count - 1 do
    begin
      CallInf := FCurrentCalls[i];
      if  i <  GetCalcCapacity then
        CallInf.SetNormalPriority
      else
        CallInf.SetLowerPriority;      
    end;
  finally
    Unlock;
  end;
end;

procedure TevAsyncCalls.SetCalcCapacity(const AValue: Integer);
begin
  Lock;
  try
    FCalcCapacity := AValue;
    AdjustPriorities;
  finally
    UnLock;
  end;
end;


function TevAsyncCalls.SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
var
  CallInf: IevCallInfo;
begin
  Result := -1;

  Lock;
  try
    CallInf := FCurrentCalls.FindCallInfoByID(ACallID);
    if Assigned(CallInf) then
    begin
      Result := CallInf.EvoPriority;
      CallInf.EvoPriority := ANewPriority;
      FCurrentCalls.Sort;
      AdjustPriorities;      
    end;
  finally
    Unlock;
  end;
end;

procedure TevAsyncCalls.UnLock;
begin
  FCurrentCalls.UnLock;
end;

procedure TevAsyncCalls.TerminateCall(const ACallID: TisGUID);
var
  i: Integer;
  TskList: array of TTaskID;
  CallInf: IevCallInfo;
begin
  if ACallID = '' then
  begin
    Lock;
    try
      SetLength(TskList,  FCurrentCalls.Count);
      for i := 0 to FCurrentCalls.Count - 1 do
      begin
        TskList[i] := FCurrentCalls[i].TaskID;
        FCurrentCalls[i].Terminate;
      end;
    finally
      Unlock;
    end;

    for i := Low(TskList) to High(TskList) do
      GlobalThreadManager.WaitForTaskEnd(TskList[i]);
  end

  else
  begin
    CallInf := FCurrentCalls.FindCallInfoByID(ACallID);
    if Assigned(CallInf) then
      CallInf.Terminate;
  end;
end;


function TevAsyncCalls.IsRegistered(const AName: String): Boolean;
begin
  Result := FCallRegistry.IndexOf(AName) <> -1;
end;

function TevAsyncCalls.GetCurrentCalls: IevCurrentCalls;
begin
  Result := FCurrentCalls;
end;

{ TevCallInfo }

function TevCallInfo.CallID: TisGUID;
begin
  Result := FCallID;
end;

constructor TevCallInfo.Create(const AName: String; const ACallID: TisGUID; const ATaskID: TTaskID;
  const APriority: Integer; const AUser: String);
begin
  inherited Create;
  SetName(AName);
  FUser := AUser;
  FCallID := ACallID;
  FTaskID := ATaskID;
  FEvoPriority := APriority;
  FThreadID := GlobalThreadManager.GetThreadIDByTaskID(ATaskID);
  Assert(FThreadID <> 0);
  FStartTime := Now;
end;

function TevCallInfo.GetEvoPriority: Integer;
begin
  Result := FEvoPriority;
end;

procedure TevCallInfo.SetEvoPriority(const AValue: Integer);
begin
  FEvoPriority := AValue;
end;

procedure TevCallInfo.SetLowerPriority;
begin
  SetThreadPriority(FThreadID, THREAD_PRIORITY_LOWEST);
  if Assigned(FRWEngine) and not Mainboard.ModuleRegister.IsProxyModule(FRWEngine) then
    FRWEngine.SetLowerRWEnginePriority(True); // Doesn't make sense to do this remotely
end;

procedure TevCallInfo.SetNormalPriority;
begin
  SetThreadPriority(FThreadID, THREAD_PRIORITY_BELOW_NORMAL);  // for a havy calculation it is better choice
  if Assigned(FRWEngine) and not Mainboard.ModuleRegister.IsProxyModule(FRWEngine) then
    FRWEngine.SetLowerRWEnginePriority(False); // Doesn't make sense to do this remotely
end;

procedure TevCallInfo.SetRWEngineRef(const ARWEngine: IevRWRemoteEngine);
begin
  FRWEngine := ARWEngine;
end;

function TevCallInfo.StartTime: TDateTime;
begin
  Result := FStartTime;
end;

function TevCallInfo.TaskID: TTaskID;
begin
  Result := FTaskID;
end;

procedure TevCallInfo.Terminate;
begin
  GlobalThreadManager.TerminateTask(FTaskID);
  if Assigned(FRWEngine) and not Mainboard.ModuleRegister.IsProxyModule(FRWEngine) then
    FRWEngine.StopReportExecution; // Doesn't make sense to do this remotely
end;

function TevCallInfo.User: String;
begin
  Result := FUser;
end;

{ TevCurrentCalls }

function TevCurrentCalls.AddCallInfo(const AName: String; const ACallID: TisGUID; const ATaskID: TTaskID;
  const APriority: Integer; const AUser: String): IevCallInfo;
begin
  Lock;
  try
    Result := TevCallInfo.Create(AName, ACallID, ATaskID, APriority, AUser);
    Add(Result);
    Sort;
  finally
    UnLock;
  end;
end;

function TevCurrentCalls.CompareItems(const Item1, Item2: IInterface): Integer;
var
 P1, P2: Integer;
begin
 P1 := (Item1 as IevCallInfo).EvoPriority;
 P2 := (Item2 as IevCallInfo).EvoPriority;
 Result := CompareValue(P1, P2);
end;

function TevCurrentCalls.FindCallInfoByID(const ACallID: TisGUID): IevCallInfo;
var
 i: Integer;
begin
  Lock;
  try
    Result := nil;
    for i := 0 to Count - 1 do
      if AnsiSameText(Items[i].CallID, ACallID) then
      begin
        Result := Items[i];
        break;
      end;
  finally
    UnLock;
  end;
end;

function TevCurrentCalls.GetItem(Index: Integer): IevCallInfo;
begin
  Result := inherited Items[Index] as IevCallInfo;
end;


{ TevAsyncCallEntryPoint }

constructor TevAsyncCallEntryPoint.Create(const AObject: TObject; const AProc: Pointer);
begin
  inherited Create;
  FObject := AObject;
  FProc := AProc;
end;

function TevAsyncCallEntryPoint.GetObject: TObject;
begin
  Result := FObject;
end;

function TevAsyncCallEntryPoint.GetProc: Pointer;
begin
  Result := FProc;
end;

end.

