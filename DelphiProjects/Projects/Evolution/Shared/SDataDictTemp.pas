// Copyright � 2000-2012 iSystems LLC. All rights reserved.
// This is automatically generated file
// Do not modify it!

unit SDataDicttemp;

interface

uses
  SDDClasses, db, classes, EvTypes;

type
  TTMP_CL = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CL_NBR: TIntegerField index 1 read GetIntegerField;
    property CUSTOM_CLIENT_NUMBER: TStringField index 2 read GetStringField;
    property NAME: TStringField index 3 read GetStringField;
    property READ_ONLY: TStringField index 4 read GetStringField;
  end;

  TTMP_CL_CO_CONSOLIDATION = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CL_CO_CONSOLIDATION_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CONSOLIDATION_TYPE: TStringField index 3 read GetStringField;
    property DESCRIPTION: TStringField index 4 read GetStringField;
    property PRIMARY_CO_NBR: TIntegerField index 5 read GetIntegerField;
    property SCOPE: TStringField index 6 read GetStringField;
  end;

  TTMP_CL_PERSON = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CL_PERSON_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property SOCIAL_SECURITY_NUMBER: TStringField index 3 read GetStringField;
  end;

  TTMP_CO = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CUSTOM_COMPANY_NUMBER: TStringField index 3 read GetStringField;
    property NAME: TStringField index 4 read GetStringField;
    property FEIN: TStringField index 5 read GetStringField;
    property ACH_SB_BANK_ACCOUNT_NBR: TIntegerField index 6 read GetIntegerField;
    property TAX_SERVICE: TStringField index 7 read GetStringField;
    property TRUST_SERVICE: TStringField index 8 read GetStringField;
    property OBC: TStringField index 9 read GetStringField;
    property FEDERAL_TAX_DEPOSIT_FREQUENCY: TStringField index 10 read GetStringField;
    property FEDERAL_TAX_PAYMENT_METHOD: TStringField index 11 read GetStringField;
    property PROCESS_PRIORITY: TIntegerField index 12 read GetIntegerField;
    property START_DATE: TDateField index 13 read GetDateField;
    property TERMINATION_DATE: TDateField index 14 read GetDateField;
    property TERMINATION_CODE: TStringField index 15 read GetStringField;
    property SY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField index 16 read GetIntegerField;
    property EFTPS_ENROLLMENT_STATUS: TStringField index 17 read GetStringField;
    property EFTPS_ENROLLMENT_DATE: TDateTimeField index 18 read GetDateTimeField;
    property EFTPS_SEQUENCE_NUMBER: TStringField index 19 read GetStringField;
    property EFTPS_ENROLLMENT_NUMBER: TStringField index 20 read GetStringField;
    property CL_CO_CONSOLIDATION_NBR: TIntegerField index 21 read GetIntegerField;
    property CUSTOMER_SERVICE_SB_USER_NBR: TIntegerField index 22 read GetIntegerField;
    property DBA: TStringField index 23 read GetStringField;
    property AUTO_ENLIST: TStringField index 24 read GetStringField;
    property LAST_FUI_CORRECTION: TDateTimeField index 25 read GetDateTimeField;
    property LAST_SUI_CORRECTION: TDateTimeField index 26 read GetDateTimeField;
    property LAST_QUARTER_END_CORRECTION: TDateTimeField index 27 read GetDateTimeField;
    property LAST_PREPROCESS: TDateTimeField index 28 read GetDateTimeField;
    property LAST_PREPROCESS_MESSAGE: TStringField index 29 read GetStringField;
    property HOLD_RETURN_QUEUE: TStringField index 30 read GetStringField;
    property HOME_CO_STATES_NBR: TIntegerField index 31 read GetIntegerField;
    property SUMMARIZE_SUI: TStringField index 32 read GetStringField;
    property CREDIT_HOLD: TStringField index 33 read GetStringField;
    property AUTOPAY_COMPANY: TStringField index 34 read GetStringField;
    property BANK_ACCOUNT_REGISTER_NAME: TStringField index 35 read GetStringField;
    property SB_ACA_GROUP_NBR: TIntegerField index 36 read GetIntegerField;
    property LEGAL_NAME: TStringField index 37 read GetStringField;
    property USE_DBA_ON_TAX_RETURN: TStringField index 38 read GetStringField;
    property ENABLE_HR: TStringField index 39 read GetStringField;
    property ADVANCED_HR_CORE: TStringField index 40 read GetStringField;
    property ADVANCED_HR_ATS: TStringField index 41 read GetStringField;
    property ADVANCED_HR_TALENT_MGMT: TStringField index 42 read GetStringField;
    property ADVANCED_HR_ONLINE_ENROLLMENT: TStringField index 43 read GetStringField;
    property PAYROLL_PRODUCT: TStringField index 44 read GetStringField;
  end;

  TTMP_CO_BANK_ACCOUNT_REGISTER = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property SB_BANK_ACCOUNTS_NBR: TIntegerField index 4 read GetIntegerField;
    property PR_CHECK_NBR: TIntegerField index 5 read GetIntegerField;
    property CHECK_SERIAL_NUMBER: TIntegerField index 6 read GetIntegerField;
    property CHECK_DATE: TDateField index 7 read GetDateField;
    property STATUS: TStringField index 8 read GetStringField;
    property STATUS_DATE: TDateTimeField index 9 read GetDateTimeField;
    property REGISTER_TYPE: TStringField index 10 read GetStringField;
    property MANUAL_TYPE: TStringField index 11 read GetStringField;
    property CO_MANUAL_ACH_NBR: TIntegerField index 12 read GetIntegerField;
    property CO_PR_ACH_NBR: TIntegerField index 13 read GetIntegerField;
    property CO_TAX_PAYMENT_ACH_NBR: TIntegerField index 14 read GetIntegerField;
    property PROCESS_DATE: TDateTimeField index 15 read GetDateTimeField;
    property FILLER: TStringField index 16 read GetStringField;
    property TRANSACTION_EFFECTIVE_DATE: TDateTimeField index 17 read GetDateTimeField;
    property AMOUNT: TFloatField index 18 read GetFloatField;
    property CLEARED_AMOUNT: TFloatField index 19 read GetFloatField;
    property CLEARED_DATE: TDateField index 20 read GetDateField;
    property PR_NBR: TIntegerField index 21 read GetIntegerField;
    property PR_MISCELLANEOUS_CHECKS_NBR: TIntegerField index 22 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 23 read GetIntegerField;
    property GROUP_IDENTIFIER: TStringField index 24 read GetStringField;
    property NOTES_POPULATED: TStringField index 25 read GetStringField;
  end;

  TTMP_CO_FED_TAX_LIABILITIES = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_FED_TAX_LIABILITIES_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property DUE_DATE: TDateField index 4 read GetDateField;
    property STATUS: TStringField index 5 read GetStringField;
    property STATUS_DATE: TDateTimeField index 6 read GetDateTimeField;
    property TAX_TYPE: TStringField index 7 read GetStringField;
    property ADJUSTMENT_TYPE: TStringField index 8 read GetStringField;
    property PR_NBR: TIntegerField index 9 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 10 read GetIntegerField;
    property IMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField index 11 read GetIntegerField;
    property THIRD_PARTY: TStringField index 12 read GetStringField;
    property CHECK_DATE: TDateField index 13 read GetDateField;
    property FILLER: TStringField index 14 read GetStringField;
    property AMOUNT: TFloatField index 15 read GetFloatField;
  end;

  TTMP_CO_LOCAL_TAX = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_LOCAL_TAX_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property SY_LOCALS_NBR: TIntegerField index 4 read GetIntegerField;
    property LOCAL_EIN_NUMBER: TStringField index 5 read GetStringField;
    property SY_LOCAL_DEPOSIT_FREQ_NBR: TIntegerField index 6 read GetIntegerField;
    property PAYMENT_METHOD: TStringField index 7 read GetStringField;
  end;

  TTMP_CO_LOCAL_TAX_LIABILITIES = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_LOCAL_TAX_LIABILITIES_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property SY_LOCALS_NBR: TIntegerField index 4 read GetIntegerField;
    property DUE_DATE: TDateField index 5 read GetDateField;
    property STATUS: TStringField index 6 read GetStringField;
    property STATUS_DATE: TDateTimeField index 7 read GetDateTimeField;
    property ADJUSTMENT_TYPE: TStringField index 8 read GetStringField;
    property PR_NBR: TIntegerField index 9 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 10 read GetIntegerField;
    property IMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField index 11 read GetIntegerField;
    property THIRD_PARTY: TStringField index 12 read GetStringField;
    property CHECK_DATE: TDateField index 13 read GetDateField;
    property FILLER: TStringField index 14 read GetStringField;
    property AMOUNT: TFloatField index 15 read GetFloatField;
  end;

  TTMP_CO_MANUAL_ACH = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_MANUAL_ACH_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property EE_NBR: TIntegerField index 4 read GetIntegerField;
    property DEBIT_DATE: TDateField index 5 read GetDateField;
    property CREDIT_DATE: TDateField index 6 read GetDateField;
    property STATUS: TStringField index 7 read GetStringField;
    property STATUS_DATE: TDateTimeField index 8 read GetDateTimeField;
    property FROM_CO_NBR: TIntegerField index 9 read GetIntegerField;
    property FROM_EE_NBR: TIntegerField index 10 read GetIntegerField;
    property FROM_SB_BANK_ACCOUNTS_NBR: TIntegerField index 11 read GetIntegerField;
    property FROM_CL_BANK_ACCOUNT_NBR: TIntegerField index 12 read GetIntegerField;
    property FROM_EE_DIRECT_DEPOSIT_NBR: TIntegerField index 13 read GetIntegerField;
    property TO_CO_NBR: TIntegerField index 14 read GetIntegerField;
    property TO_EE_NBR: TIntegerField index 15 read GetIntegerField;
    property TO_SB_BANK_ACCOUNTS_NBR: TIntegerField index 16 read GetIntegerField;
    property TO_CL_BANK_ACCOUNT_NBR: TIntegerField index 17 read GetIntegerField;
    property TO_EE_DIRECT_DEPOSIT_NBR: TIntegerField index 18 read GetIntegerField;
    property FILLER: TStringField index 19 read GetStringField;
    property AMOUNT: TFloatField index 20 read GetFloatField;
  end;

  TTMP_CO_PR_ACH = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_PR_ACH_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property ACH_DATE: TDateTimeField index 4 read GetDateTimeField;
    property STATUS: TStringField index 5 read GetStringField;
    property STATUS_DATE: TDateTimeField index 6 read GetDateTimeField;
    property PR_NBR: TIntegerField index 7 read GetIntegerField;
    property FILLER: TStringField index 8 read GetStringField;
    property AMOUNT: TFloatField index 9 read GetFloatField;
    property TRUST_IMPOUND: TStringField index 10 read GetStringField;
    property TAX_IMPOUND: TStringField index 11 read GetStringField;
    property DD_IMPOUND: TStringField index 12 read GetStringField;
    property BILLING_IMPOUND: TStringField index 13 read GetStringField;
    property WC_IMPOUND: TStringField index 14 read GetStringField;
  end;

  TTMP_CO_REPORTS = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_REPORTS_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property REPORT_WRITER_REPORTS_NBR: TIntegerField index 4 read GetIntegerField;
    property REPORT_LEVEL: TStringField index 5 read GetStringField;
    property CUSTOM_NAME: TStringField index 6 read GetStringField;
  end;

  TTMP_CO_STATES = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_STATES_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property STATE: TStringField index 4 read GetStringField;
    property STATE_TAX_DEPOSIT_METHOD: TStringField index 5 read GetStringField;
    property SUI_TAX_DEPOSIT_METHOD: TStringField index 6 read GetStringField;
    property SY_STATE_DEPOSIT_FREQ_NBR: TIntegerField index 7 read GetIntegerField;
    property SUI_TAX_DEPOSIT_FREQUENCY: TStringField index 8 read GetStringField;
    property STATE_EIN: TStringField index 9 read GetStringField;
    property SUI_EIN: TStringField index 10 read GetStringField;
    property STATE_SDI_EIN: TStringField index 11 read GetStringField;
    property TCD_DEPOSIT_FREQUENCY_NBR: TIntegerField index 12 read GetIntegerField;
    property TCD_PAYMENT_METHOD: TStringField index 13 read GetStringField;
  end;

  TTMP_CO_STATE_TAX_LIABILITIES = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_STATE_TAX_LIABILITIES_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property STATE: TStringField index 4 read GetStringField;
    property TAX_TYPE: TStringField index 5 read GetStringField;
    property DUE_DATE: TDateField index 6 read GetDateField;
    property STATUS: TStringField index 7 read GetStringField;
    property STATUS_DATE: TDateTimeField index 8 read GetDateTimeField;
    property ADJUSTMENT_TYPE: TStringField index 9 read GetStringField;
    property PR_NBR: TIntegerField index 10 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 11 read GetIntegerField;
    property IMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField index 12 read GetIntegerField;
    property THIRD_PARTY: TStringField index 13 read GetStringField;
    property CHECK_DATE: TDateField index 14 read GetDateField;
    property FILLER: TStringField index 15 read GetStringField;
    property AMOUNT: TFloatField index 16 read GetFloatField;
  end;

  TTMP_CO_SUI_LIABILITIES = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_SUI_LIABILITIES_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property SY_SUI_NBR: TIntegerField index 4 read GetIntegerField;
    property DUE_DATE: TDateField index 5 read GetDateField;
    property STATUS: TStringField index 6 read GetStringField;
    property STATUS_DATE: TDateTimeField index 7 read GetDateTimeField;
    property ADJUSTMENT_TYPE: TStringField index 8 read GetStringField;
    property PR_NBR: TIntegerField index 9 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 10 read GetIntegerField;
    property IMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField index 11 read GetIntegerField;
    property THIRD_PARTY: TStringField index 12 read GetStringField;
    property CHECK_DATE: TDateField index 13 read GetDateField;
    property FILLER: TStringField index 14 read GetStringField;
    property AMOUNT: TFloatField index 15 read GetFloatField;
  end;

  TTMP_CO_TAX_DEPOSITS = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property TAX_PAYMENT_REFERENCE_NUMBER: TStringField index 4 read GetStringField;
    property STATUS: TStringField index 5 read GetStringField;
    property STATUS_DATE: TDateTimeField index 6 read GetDateTimeField;
    property PR_NBR: TIntegerField index 7 read GetIntegerField;
    property SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField index 8 read GetIntegerField;
    property DEPOSIT_TYPE: TStringField index 9 read GetStringField;
    property FILLER: TStringField index 10 read GetStringField;
    property SB_TAX_PAYMENT_NBR: TIntegerField index 11 read GetIntegerField;
    property TRACE_NUMBER: TStringField index 12 read GetStringField;
  end;

  TTMP_CO_TAX_PAYMENT_ACH = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_TAX_PAYMENT_ACH_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property ACH_DATE: TDateTimeField index 4 read GetDateTimeField;
    property STATUS: TStringField index 5 read GetStringField;
    property STATUS_DATE: TDateTimeField index 6 read GetDateTimeField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 7 read GetIntegerField;
    property FILLER: TStringField index 8 read GetStringField;
    property AMOUNT: TFloatField index 9 read GetFloatField;
  end;

  TTMP_CO_TAX_RETURN_QUEUE = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property CO_TAX_RETURN_QUEUE_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property STATUS: TStringField index 4 read GetStringField;
    property STATUS_DATE: TDateTimeField index 5 read GetDateTimeField;
    property SB_COPY_PRINTED: TStringField index 6 read GetStringField;
    property CLIENT_COPY_PRINTED: TStringField index 7 read GetStringField;
    property AGENCY_COPY_PRINTED: TStringField index 8 read GetStringField;
    property DUE_DATE: TDateField index 9 read GetDateField;
    property PERIOD_END_DATE: TDateField index 10 read GetDateField;
    property FILLER: TStringField index 11 read GetStringField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 12 read GetIntegerField;
    property CL_CO_CONSOLIDATION_NBR: TIntegerField index 13 read GetIntegerField;
    property PRODUCE_ASCII_FILE: TStringField index 14 read GetStringField;
    property SY_GL_AGENCY_REPORT_NBR: TIntegerField index 15 read GetIntegerField;
    property CO_REPORTS_NBR: TIntegerField index 16 read GetIntegerField;
  end;

  TTMP_EE = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property EE_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CL_PERSON_NBR: TIntegerField index 3 read GetIntegerField;
    property CO_NBR: TIntegerField index 4 read GetIntegerField;
    property SELFSERVE_USERNAME: TStringField index 5 read GetStringField;
    property SELFSERVE_PASSWORD: TStringField index 6 read GetStringField;
  end;

  TTMP_PR = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property PR_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property RUN_NUMBER: TIntegerField index 4 read GetIntegerField;
    property CHECK_DATE: TDateField index 5 read GetDateField;
    property PROCESS_DATE: TDateTimeField index 6 read GetDateTimeField;
    property SCHEDULED: TStringField index 7 read GetStringField;
    property PAYROLL_TYPE: TStringField index 8 read GetStringField;
    property STATUS: TStringField index 9 read GetStringField;
    property STATUS_DATE: TDateTimeField index 10 read GetDateTimeField;
    property APPROVED_BY_FINANCE: TStringField index 11 read GetStringField;
    property APPROVED_BY_TAX: TStringField index 12 read GetStringField;
    property APPROVED_BY_MANAGEMENT: TStringField index 13 read GetStringField;
  end;

  TTMP_PR_MISCELLANEOUS_CHECKS = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property PR_MISCELLANEOUS_CHECKS_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property PR_NBR: TIntegerField index 3 read GetIntegerField;
    property CO_TAX_DEPOSITS_NBR: TIntegerField index 4 read GetIntegerField;
  end;

  TTMP_PR_SCHEDULED_EVENT = class(TddTMPTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
  published
    property PR_SCHEDULED_EVENT_NBR: TIntegerField index 1 read GetIntegerField;
    property CL_NBR: TIntegerField index 2 read GetIntegerField;
    property CO_NBR: TIntegerField index 3 read GetIntegerField;
    property PR_NBR: TIntegerField index 4 read GetIntegerField;
    property STATUS: TStringField index 5 read GetStringField;
    property EVENT_DATE: TDateTimeField index 6 read GetDateTimeField;
    property SCHEDULED_PROCESS_DATE: TDateTimeField index 7 read GetDateTimeField;
    property ACTUAL_PROCESS_DATE: TDateTimeField index 8 read GetDateTimeField;
    property SCHEDULED_DELIVERY_DATE: TDateTimeField index 9 read GetDateTimeField;
    property ACTUAL_DELIVERY_DATE: TDateTimeField index 10 read GetDateTimeField;
    property SCHEDULED_CHECK_DATE: TDateField index 11 read GetDateField;
    property NORMAL_CHECK_DATE: TDateField index 12 read GetDateField;
  end;

  TDM_TEMPORARY_ = class(TddDatabase)
  public
    class function GetDBType: TevDBType; override;
  private
    function GetTMP_CL: TTMP_CL;
  published
    property TMP_CL: TTMP_CL read GetTMP_CL;
  private
    function GetTMP_CL_CO_CONSOLIDATION: TTMP_CL_CO_CONSOLIDATION;
  published
    property TMP_CL_CO_CONSOLIDATION: TTMP_CL_CO_CONSOLIDATION read GetTMP_CL_CO_CONSOLIDATION;
  private
    function GetTMP_CL_PERSON: TTMP_CL_PERSON;
  published
    property TMP_CL_PERSON: TTMP_CL_PERSON read GetTMP_CL_PERSON;
  private
    function GetTMP_CO: TTMP_CO;
  published
    property TMP_CO: TTMP_CO read GetTMP_CO;
  private
    function GetTMP_CO_BANK_ACCOUNT_REGISTER: TTMP_CO_BANK_ACCOUNT_REGISTER;
  published
    property TMP_CO_BANK_ACCOUNT_REGISTER: TTMP_CO_BANK_ACCOUNT_REGISTER read GetTMP_CO_BANK_ACCOUNT_REGISTER;
  private
    function GetTMP_CO_FED_TAX_LIABILITIES: TTMP_CO_FED_TAX_LIABILITIES;
  published
    property TMP_CO_FED_TAX_LIABILITIES: TTMP_CO_FED_TAX_LIABILITIES read GetTMP_CO_FED_TAX_LIABILITIES;
  private
    function GetTMP_CO_LOCAL_TAX: TTMP_CO_LOCAL_TAX;
  published
    property TMP_CO_LOCAL_TAX: TTMP_CO_LOCAL_TAX read GetTMP_CO_LOCAL_TAX;
  private
    function GetTMP_CO_LOCAL_TAX_LIABILITIES: TTMP_CO_LOCAL_TAX_LIABILITIES;
  published
    property TMP_CO_LOCAL_TAX_LIABILITIES: TTMP_CO_LOCAL_TAX_LIABILITIES read GetTMP_CO_LOCAL_TAX_LIABILITIES;
  private
    function GetTMP_CO_MANUAL_ACH: TTMP_CO_MANUAL_ACH;
  published
    property TMP_CO_MANUAL_ACH: TTMP_CO_MANUAL_ACH read GetTMP_CO_MANUAL_ACH;
  private
    function GetTMP_CO_PR_ACH: TTMP_CO_PR_ACH;
  published
    property TMP_CO_PR_ACH: TTMP_CO_PR_ACH read GetTMP_CO_PR_ACH;
  private
    function GetTMP_CO_REPORTS: TTMP_CO_REPORTS;
  published
    property TMP_CO_REPORTS: TTMP_CO_REPORTS read GetTMP_CO_REPORTS;
  private
    function GetTMP_CO_STATES: TTMP_CO_STATES;
  published
    property TMP_CO_STATES: TTMP_CO_STATES read GetTMP_CO_STATES;
  private
    function GetTMP_CO_STATE_TAX_LIABILITIES: TTMP_CO_STATE_TAX_LIABILITIES;
  published
    property TMP_CO_STATE_TAX_LIABILITIES: TTMP_CO_STATE_TAX_LIABILITIES read GetTMP_CO_STATE_TAX_LIABILITIES;
  private
    function GetTMP_CO_SUI_LIABILITIES: TTMP_CO_SUI_LIABILITIES;
  published
    property TMP_CO_SUI_LIABILITIES: TTMP_CO_SUI_LIABILITIES read GetTMP_CO_SUI_LIABILITIES;
  private
    function GetTMP_CO_TAX_DEPOSITS: TTMP_CO_TAX_DEPOSITS;
  published
    property TMP_CO_TAX_DEPOSITS: TTMP_CO_TAX_DEPOSITS read GetTMP_CO_TAX_DEPOSITS;
  private
    function GetTMP_CO_TAX_PAYMENT_ACH: TTMP_CO_TAX_PAYMENT_ACH;
  published
    property TMP_CO_TAX_PAYMENT_ACH: TTMP_CO_TAX_PAYMENT_ACH read GetTMP_CO_TAX_PAYMENT_ACH;
  private
    function GetTMP_CO_TAX_RETURN_QUEUE: TTMP_CO_TAX_RETURN_QUEUE;
  published
    property TMP_CO_TAX_RETURN_QUEUE: TTMP_CO_TAX_RETURN_QUEUE read GetTMP_CO_TAX_RETURN_QUEUE;
  private
    function GetTMP_EE: TTMP_EE;
  published
    property TMP_EE: TTMP_EE read GetTMP_EE;
  private
    function GetTMP_PR: TTMP_PR;
  published
    property TMP_PR: TTMP_PR read GetTMP_PR;
  private
    function GetTMP_PR_MISCELLANEOUS_CHECKS: TTMP_PR_MISCELLANEOUS_CHECKS;
  published
    property TMP_PR_MISCELLANEOUS_CHECKS: TTMP_PR_MISCELLANEOUS_CHECKS read GetTMP_PR_MISCELLANEOUS_CHECKS;
  private
    function GetTMP_PR_SCHEDULED_EVENT: TTMP_PR_SCHEDULED_EVENT;
  published
    property TMP_PR_SCHEDULED_EVENT: TTMP_PR_SCHEDULED_EVENT read GetTMP_PR_SCHEDULED_EVENT;
  end;


const
  TMP_Constraints: array [0..53] of string = (
    'ALTER TABLE TMP_CL_CO_CONSOLIDATION ADD CONSTRAINT FK_TMP_CL_CO_CONSOLIDATION_1 FOREIGN KEY (CL_NBR) REFERENCES TMP_CL (CL_NBR)',
    'ALTER TABLE TMP_CL_CO_CONSOLIDATION ADD CONSTRAINT FK_TMP_CL_CO_CONSOLIDATION_2 FOREIGN KEY (CL_NBR, PRIMARY_CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CL_PERSON ADD CONSTRAINT FK_TMP_CL_PERSON_1 FOREIGN KEY (CL_NBR) REFERENCES TMP_CL (CL_NBR)',
    'ALTER TABLE TMP_CO ADD CONSTRAINT FK_TMP_CO_1 FOREIGN KEY (CL_NBR) REFERENCES TMP_CL (CL_NBR)',
    'ALTER TABLE TMP_CO ADD CONSTRAINT FK_TMP_CO_2 FOREIGN KEY (CL_NBR, CL_CO_CONSOLIDATION_NBR) REFERENCES TMP_CL_CO_CONSOLIDATION (CL_NBR, CL_CO_CONSOLIDATION_NBR)',
    'ALTER TABLE TMP_CO ADD CONSTRAINT FK_TMP_CO_3 FOREIGN KEY (CL_NBR, HOME_CO_STATES_NBR) REFERENCES TMP_CO_STATES (CL_NBR, CO_STATES_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_5 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_2 FOREIGN KEY (CL_NBR, CO_PR_ACH_NBR) REFERENCES TMP_CO_PR_ACH (CL_NBR, CO_PR_ACH_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_3 FOREIGN KEY (CL_NBR, CO_MANUAL_ACH_NBR) REFERENCES TMP_CO_MANUAL_ACH (CL_NBR, CO_MANUAL_ACH_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_4 FOREIGN KEY (CL_NBR, CO_TAX_PAYMENT_ACH_NBR) REFERENCES TMP_CO_TAX_PAYMENT_ACH (CL_NBR, CO_TAX_PAYMENT_ACH_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_6 FOREIGN KEY (CL_NBR, PR_MISCELLANEOUS_CHECKS_NBR) REFERENCES TMP_PR_MISCELLANEOUS_CHECKS (CL_NBR, PR_MISCELLANEOUS_CHECKS_NBR)',
    'ALTER TABLE TMP_CO_BANK_ACCOUNT_REGISTER ADD CONSTRAINT FK_TMP_CO_BANK_ACCOUNT_REG_7 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_FED_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_FED_TAX_LIABILITIES_4 FOREIGN KEY (CL_NBR, IMPOUND_CO_BANK_ACCT_REG_NBR) REFERENCES TMP_CO_BANK_ACCOUNT_REGISTER (CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR)',
    'ALTER TABLE TMP_CO_FED_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_FED_TAX_LIABILITIES_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_FED_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_FED_TAX_LIABILITIES_3 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_FED_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_FED_TAX_LIABILITIES_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_LOCAL_TAX ADD CONSTRAINT FK_TMP_CO_LOCAL_TAX_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_LOCAL_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_LOCAL_TAX_LIAB_4 FOREIGN KEY (CL_NBR, IMPOUND_CO_BANK_ACCT_REG_NBR) REFERENCES TMP_CO_BANK_ACCOUNT_REGISTER (CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR)',
    'ALTER TABLE TMP_CO_LOCAL_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_LOCAL_TAX_LIAB_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_LOCAL_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_LOCAL_TAX_LIAB_3 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_LOCAL_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_LOCAL_TAX_LIAB_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_4 FOREIGN KEY (CL_NBR, EE_NBR) REFERENCES TMP_EE (CL_NBR, EE_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_2 FOREIGN KEY (CL_NBR, FROM_EE_NBR) REFERENCES TMP_EE (CL_NBR, EE_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_3 FOREIGN KEY (CL_NBR, TO_EE_NBR) REFERENCES TMP_EE (CL_NBR, EE_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_5 FOREIGN KEY (CL_NBR, TO_CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_MANUAL_ACH ADD CONSTRAINT FK_TMP_CO_MANUAL_ACH_6 FOREIGN KEY (CL_NBR, FROM_CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_PR_ACH ADD CONSTRAINT FK_TMP_CO_PR_ACH_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_PR_ACH ADD CONSTRAINT FK_TMP_CO_PR_ACH_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_REPORTS ADD CONSTRAINT FK_TMP_CO_REPORTS_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_STATES ADD CONSTRAINT FK_TMP_CO_STATES_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_STATE_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_STATE_TAX_LIAB_4 FOREIGN KEY (CL_NBR, IMPOUND_CO_BANK_ACCT_REG_NBR) REFERENCES TMP_CO_BANK_ACCOUNT_REGISTER (CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR)',
    'ALTER TABLE TMP_CO_STATE_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_STATE_TAX_LIAB_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_STATE_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_STATE_TAX_LIAB_3 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_STATE_TAX_LIABILITIES ADD CONSTRAINT FK_TMP_CO_STATE_TAX_LIAB_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_SUI_LIABILITIES ADD CONSTRAINT FK_TMP_CO_SUI_LIABILITIES_4 FOREIGN KEY (CL_NBR, IMPOUND_CO_BANK_ACCT_REG_NBR) REFERENCES TMP_CO_BANK_ACCOUNT_REGISTER (CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR)',
    'ALTER TABLE TMP_CO_SUI_LIABILITIES ADD CONSTRAINT FK_TMP_CO_SUI_LIABILITIES_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_SUI_LIABILITIES ADD CONSTRAINT FK_TMP_CO_SUI_LIABILITIES_3 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_SUI_LIABILITIES ADD CONSTRAINT FK_TMP_CO_SUI_LIABILITIES_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_TAX_DEPOSITS ADD CONSTRAINT FK_TMP_CO_TAX_DEPOSITS_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_CO_TAX_DEPOSITS ADD CONSTRAINT FK_TMP_CO_TAX_DEPOSITS_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_TAX_PAYMENT_ACH ADD CONSTRAINT FK_TMP_CO_TAX_PAYMENT_ACH_2 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_CO_TAX_PAYMENT_ACH ADD CONSTRAINT FK_TMP_CO_TAX_PAYMENT_ACH_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_TAX_RETURN_QUEUE ADD CONSTRAINT FK_TMP_CO_TAX_RETURN_QUEUE_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_CO_TAX_RETURN_QUEUE ADD CONSTRAINT FK_TMP_CO_TAX_RETURN_QUEUE_2 FOREIGN KEY (CL_NBR, CL_CO_CONSOLIDATION_NBR) REFERENCES TMP_CL_CO_CONSOLIDATION (CL_NBR, CL_CO_CONSOLIDATION_NBR)',
    'ALTER TABLE TMP_CO_TAX_RETURN_QUEUE ADD CONSTRAINT FK_TMP_CO_TAX_RETURN_QUEUE_3 FOREIGN KEY (CL_NBR, CO_REPORTS_NBR) REFERENCES TMP_CO_REPORTS (CL_NBR, CO_REPORTS_NBR)',
    'ALTER TABLE TMP_EE ADD CONSTRAINT FK_TMP_EE_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_EE ADD CONSTRAINT FK_TMP_EE_2 FOREIGN KEY (CL_NBR, CL_PERSON_NBR) REFERENCES TMP_CL_PERSON (CL_NBR, CL_PERSON_NBR)',
    'ALTER TABLE TMP_PR ADD CONSTRAINT FK_TMP_PR_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)',
    'ALTER TABLE TMP_PR_MISCELLANEOUS_CHECKS ADD CONSTRAINT FK_TMP_PR_MISC_CHECKS_2 FOREIGN KEY (CL_NBR, CO_TAX_DEPOSITS_NBR) REFERENCES TMP_CO_TAX_DEPOSITS (CL_NBR, CO_TAX_DEPOSITS_NBR)',
    'ALTER TABLE TMP_PR_MISCELLANEOUS_CHECKS ADD CONSTRAINT FK_TMP_PR_MISC_CHECKS_1 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_PR_SCHEDULED_EVENT ADD CONSTRAINT FK_TMP_PR_SCHEDULED_EVENT_2 FOREIGN KEY (CL_NBR, PR_NBR) REFERENCES TMP_PR (CL_NBR, PR_NBR)',
    'ALTER TABLE TMP_PR_SCHEDULED_EVENT ADD CONSTRAINT FK_TMP_PR_SCHEDULED_EVENT_1 FOREIGN KEY (CL_NBR, CO_NBR) REFERENCES TMP_CO (CL_NBR, CO_NBR)'
  );

  TMP_Indices: array [0..64] of string = (
    'CREATE UNIQUE INDEX LK_TMP_CL ON TMP_CL (CUSTOM_CLIENT_NUMBER)',
    'CREATE UNIQUE INDEX I_TMP_CL_2 ON TMP_CL (NAME)',
    'CREATE INDEX I_TMP_CL_CO_CONSOLIDATION_1 ON TMP_CL_CO_CONSOLIDATION (DESCRIPTION)',
    'CREATE UNIQUE INDEX I_TMP_CL_PERSON ON TMP_CL_PERSON (SOCIAL_SECURITY_NUMBER,CL_NBR)',
    'CREATE UNIQUE INDEX LK_TMP_CO ON TMP_CO (CUSTOM_COMPANY_NUMBER)',
    'CREATE INDEX I_TMP_CO_1 ON TMP_CO (CUSTOMER_SERVICE_SB_USER_NBR)',
    'CREATE INDEX I_TMP_CO_2 ON TMP_CO (DBA)',
    'CREATE INDEX I_TMP_CO_3 ON TMP_CO (FEIN)',
    'CREATE INDEX I_TMP_CO_4 ON TMP_CO (NAME)',
    'CREATE INDEX I_TMP_CO_5 ON TMP_CO (START_DATE)',
    'CREATE INDEX I_TMP_CO_6 ON TMP_CO (TERMINATION_DATE)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_1 ON TMP_CO_BANK_ACCOUNT_REGISTER (AMOUNT)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_2 ON TMP_CO_BANK_ACCOUNT_REGISTER (CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_3 ON TMP_CO_BANK_ACCOUNT_REGISTER (CHECK_SERIAL_NUMBER)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_4 ON TMP_CO_BANK_ACCOUNT_REGISTER (CLEARED_DATE)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_5 ON TMP_CO_BANK_ACCOUNT_REGISTER (TRANSACTION_EFFECTIVE_DATE)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_6 ON TMP_CO_BANK_ACCOUNT_REGISTER (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_BANK_ACCOUNT_REG_7 ON TMP_CO_BANK_ACCOUNT_REGISTER (CL_NBR,GROUP_IDENTIFIER,CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_FED_TAX_LIABILITIES_1 ON TMP_CO_FED_TAX_LIABILITIES (AMOUNT)',
    'CREATE INDEX I_TMP_CO_FED_TAX_LIABILITIES_2 ON TMP_CO_FED_TAX_LIABILITIES (CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_FED_TAX_LIABILITIES_3 ON TMP_CO_FED_TAX_LIABILITIES (DUE_DATE)',
    'CREATE INDEX I_TMP_CO_FED_TAX_LIABILITIES_4 ON TMP_CO_FED_TAX_LIABILITIES (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_LOCAL_TAX_1 ON TMP_CO_LOCAL_TAX (LOCAL_EIN_NUMBER)',
    'CREATE INDEX I_TMP_CO_LOCAL_TAX_LIAB_1 ON TMP_CO_LOCAL_TAX_LIABILITIES (CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_LOCAL_TAX_LIAB_2 ON TMP_CO_LOCAL_TAX_LIABILITIES (DUE_DATE)',
    'CREATE INDEX I_TMP_CO_LOCAL_TAX_LIAB_3 ON TMP_CO_LOCAL_TAX_LIABILITIES (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_LOCAL_TAX_LIAB_4 ON TMP_CO_LOCAL_TAX_LIABILITIES (SY_LOCALS_NBR)',
    'CREATE INDEX I_TMP_CO_MANUAL_ACH_1 ON TMP_CO_MANUAL_ACH (CREDIT_DATE)',
    'CREATE INDEX I_TMP_CO_MANUAL_ACH_2 ON TMP_CO_MANUAL_ACH (DEBIT_DATE)',
    'CREATE INDEX I_TMP_CO_MANUAL_ACH_3 ON TMP_CO_MANUAL_ACH (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_PR_ACH_1 ON TMP_CO_PR_ACH (ACH_DATE)',
    'CREATE INDEX I_TMP_CO_PR_ACH_2 ON TMP_CO_PR_ACH (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_REPORTS_1 ON TMP_CO_REPORTS (REPORT_WRITER_REPORTS_NBR)',
    'CREATE INDEX I_TMP_CO_STATES_1 ON TMP_CO_STATES (STATE_EIN)',
    'CREATE INDEX I_TMP_CO_STATES_2 ON TMP_CO_STATES (STATE_SDI_EIN)',
    'CREATE INDEX I_TMP_CO_STATES_3 ON TMP_CO_STATES (SUI_EIN)',
    'CREATE INDEX I_TMP_CO_STATE_TAX_LIAB_1 ON TMP_CO_STATE_TAX_LIABILITIES (AMOUNT)',
    'CREATE INDEX I_TMP_CO_STATE_TAX_LIAB_2 ON TMP_CO_STATE_TAX_LIABILITIES (CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_STATE_TAX_LIAB_3 ON TMP_CO_STATE_TAX_LIABILITIES (DUE_DATE)',
    'CREATE INDEX I_TMP_CO_STATE_TAX_LIAB_4 ON TMP_CO_STATE_TAX_LIABILITIES (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_SUI_LIABILITIES_1 ON TMP_CO_SUI_LIABILITIES (AMOUNT)',
    'CREATE INDEX I_TMP_CO_SUI_LIABILITIES_2 ON TMP_CO_SUI_LIABILITIES (CHECK_DATE)',
    'CREATE INDEX I_TMP_CO_SUI_LIABILITIES_3 ON TMP_CO_SUI_LIABILITIES (DUE_DATE)',
    'CREATE INDEX I_TMP_CO_SUI_LIABILITIES_4 ON TMP_CO_SUI_LIABILITIES (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_SUI_LIABILITIES_5 ON TMP_CO_SUI_LIABILITIES (SY_SUI_NBR)',
    'CREATE INDEX I_TMP_CO_TAX_DEPOSITS_1 ON TMP_CO_TAX_DEPOSITS (STATUS)',
    'CREATE INDEX I_TMP_CO_TAX_DEPOSITS_2 ON TMP_CO_TAX_DEPOSITS (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_DEPOSITS_3 ON TMP_CO_TAX_DEPOSITS (TAX_PAYMENT_REFERENCE_NUMBER)',
    'CREATE INDEX I_TMP_CO_TAX_PAYMENT_ACH_1 ON TMP_CO_TAX_PAYMENT_ACH (ACH_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_PAYMENT_ACH_2 ON TMP_CO_TAX_PAYMENT_ACH (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_RETURN_QUEUE_1 ON TMP_CO_TAX_RETURN_QUEUE (DUE_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_RETURN_QUEUE_2 ON TMP_CO_TAX_RETURN_QUEUE (PERIOD_END_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_RETURN_QUEUE_3 ON TMP_CO_TAX_RETURN_QUEUE (STATUS_DATE)',
    'CREATE INDEX I_TMP_CO_TAX_RETURN_QUEUE_4 ON TMP_CO_TAX_RETURN_QUEUE (SY_GL_AGENCY_REPORT_NBR)',
    'CREATE INDEX I_TMP_CO_TAX_RETURN_QUEUE_5 ON TMP_CO_TAX_RETURN_QUEUE (SY_REPORTS_GROUP_NBR)',
    'CREATE INDEX I_TMP_EE_1 ON TMP_EE (SELFSERVE_USERNAME)',
    'CREATE INDEX I_TMP_PR_2 ON TMP_PR (PROCESS_DATE)',
    'CREATE UNIQUE INDEX I_TMP_PR_3 ON TMP_PR (CHECK_DATE,RUN_NUMBER,CL_NBR,CO_NBR)',
    'CREATE INDEX I_TMP_PR_4 ON TMP_PR (STATUS)',
    'CREATE INDEX I_TMP_PR_5 ON TMP_PR (STATUS_DATE)',
    'CREATE INDEX I_TMP_PR_SCHEDULED_EVENT_1 ON TMP_PR_SCHEDULED_EVENT (ACTUAL_DELIVERY_DATE)',
    'CREATE INDEX I_TMP_PR_SCHEDULED_EVENT_2 ON TMP_PR_SCHEDULED_EVENT (ACTUAL_PROCESS_DATE)',
    'CREATE INDEX I_TMP_PR_SCHEDULED_EVENT_3 ON TMP_PR_SCHEDULED_EVENT (EVENT_DATE)',
    'CREATE INDEX I_TMP_PR_SCHEDULED_EVENT_4 ON TMP_PR_SCHEDULED_EVENT (SCHEDULED_DELIVERY_DATE)',
    'CREATE INDEX I_TMP_PR_SCHEDULED_EVENT_5 ON TMP_PR_SCHEDULED_EVENT (SCHEDULED_PROCESS_DATE)'
  );


procedure Register;

implementation

{TTMP_CL}

class function TTMP_CL.GetClassIndex: Integer;
begin
  Result := 301;
end;

class function TTMP_CL.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CL.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CL.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'CL_NBR';
  Result[1] := 'CUSTOM_CLIENT_NUMBER';
  Result[2] := 'NAME';
  Result[3] := 'READ_ONLY';
end;

class function TTMP_CL.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CUSTOM_CLIENT_NUMBER' then Result := 20
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'READ_ONLY' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CL.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CL.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'CL_NBR';
end;

class function TTMP_CL.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TTMP_CL.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_CO_CONSOLIDATION;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_PERSON;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
end;

class function TTMP_CL.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'CUSTOM_CLIENT_NUMBER';
end;

{TTMP_CL_CO_CONSOLIDATION}

class function TTMP_CL_CO_CONSOLIDATION.GetClassIndex: Integer;
begin
  Result := 302;
end;

class function TTMP_CL_CO_CONSOLIDATION.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CL_CO_CONSOLIDATION.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CL_CO_CONSOLIDATION.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 5);
  Result[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CONSOLIDATION_TYPE';
  Result[3] := 'DESCRIPTION';
  Result[4] := 'SCOPE';
end;

class function TTMP_CL_CO_CONSOLIDATION.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CONSOLIDATION_TYPE' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 20
  else if FieldName = 'SCOPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CL_CO_CONSOLIDATION.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CL_CO_CONSOLIDATION.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CL_CO_CONSOLIDATION.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'PRIMARY_CO_NBR';
  Result[High(Result)].DestFields[1] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CL_CO_CONSOLIDATION.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_RETURN_QUEUE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].DestFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].DestFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CL_CO_CONSOLIDATION.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CL_PERSON}

class function TTMP_CL_PERSON.GetClassIndex: Integer;
begin
  Result := 303;
end;

class function TTMP_CL_PERSON.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CL_PERSON.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CL_PERSON.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'CL_PERSON_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'SOCIAL_SECURITY_NUMBER';
end;

class function TTMP_CL_PERSON.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SOCIAL_SECURITY_NUMBER' then Result := 11
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CL_PERSON.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CL_PERSON.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CL_PERSON_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CL_PERSON.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CL_PERSON.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_EE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_PERSON_NBR';
  Result[High(Result)].DestFields[0] := 'CL_PERSON_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CL_PERSON.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO}

class function TTMP_CO.GetClassIndex: Integer;
begin
  Result := 304;
end;

class function TTMP_CO.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 19);
  Result[0] := 'CO_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CUSTOM_COMPANY_NUMBER';
  Result[3] := 'NAME';
  Result[4] := 'FEIN';
  Result[5] := 'TAX_SERVICE';
  Result[6] := 'TRUST_SERVICE';
  Result[7] := 'OBC';
  Result[8] := 'FEDERAL_TAX_DEPOSIT_FREQUENCY';
  Result[9] := 'FEDERAL_TAX_PAYMENT_METHOD';
  Result[10] := 'PROCESS_PRIORITY';
  Result[11] := 'START_DATE';
  Result[12] := 'TERMINATION_CODE';
  Result[13] := 'EFTPS_ENROLLMENT_STATUS';
  Result[14] := 'AUTO_ENLIST';
  Result[15] := 'HOLD_RETURN_QUEUE';
  Result[16] := 'SUMMARIZE_SUI';
  Result[17] := 'CREDIT_HOLD';
  Result[18] := 'AUTOPAY_COMPANY';
end;

class function TTMP_CO.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CUSTOM_COMPANY_NUMBER' then Result := 20
  else if FieldName = 'NAME' then Result := 75
  else if FieldName = 'FEIN' then Result := 9
  else if FieldName = 'TAX_SERVICE' then Result := 1
  else if FieldName = 'TRUST_SERVICE' then Result := 1
  else if FieldName = 'OBC' then Result := 1
  else if FieldName = 'FEDERAL_TAX_DEPOSIT_FREQUENCY' then Result := 1
  else if FieldName = 'FEDERAL_TAX_PAYMENT_METHOD' then Result := 1
  else if FieldName = 'TERMINATION_CODE' then Result := 1
  else if FieldName = 'EFTPS_ENROLLMENT_STATUS' then Result := 1
  else if FieldName = 'EFTPS_SEQUENCE_NUMBER' then Result := 4
  else if FieldName = 'EFTPS_ENROLLMENT_NUMBER' then Result := 4
  else if FieldName = 'DBA' then Result := 75
  else if FieldName = 'AUTO_ENLIST' then Result := 1
  else if FieldName = 'LAST_PREPROCESS_MESSAGE' then Result := 80
  else if FieldName = 'HOLD_RETURN_QUEUE' then Result := 1
  else if FieldName = 'SUMMARIZE_SUI' then Result := 1
  else if FieldName = 'CREDIT_HOLD' then Result := 1
  else if FieldName = 'AUTOPAY_COMPANY' then Result := 1
  else if FieldName = 'BANK_ACCOUNT_REGISTER_NAME' then Result := 15
  else if FieldName = 'LEGAL_NAME' then Result := 75
  else if FieldName = 'USE_DBA_ON_TAX_RETURN' then Result := 1
  else if FieldName = 'ENABLE_HR' then Result := 1
  else if FieldName = 'ADVANCED_HR_CORE' then Result := 1
  else if FieldName = 'ADVANCED_HR_ATS' then Result := 1
  else if FieldName = 'ADVANCED_HR_TALENT_MGMT' then Result := 1
  else if FieldName = 'ADVANCED_HR_ONLINE_ENROLLMENT' then Result := 1
  else if FieldName = 'PAYROLL_PRODUCT' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_CO_CONSOLIDATION;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].DestFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'HOME_CO_STATES_NBR';
  Result[High(Result)].DestFields[1] := 'CO_STATES_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_FED_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_LOCAL_TAX;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_LOCAL_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'TO_CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'FROM_CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_PR_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATE_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_SUI_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_PAYMENT_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_RETURN_QUEUE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_EE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR_SCHEDULED_EVENT;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_CO_CONSOLIDATION;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CO_NBR';
  Result[High(Result)].DestFields[1] := 'PRIMARY_CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'CUSTOM_COMPANY_NUMBER';
end;

{TTMP_CO_BANK_ACCOUNT_REGISTER}

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetClassIndex: Integer;
begin
  Result := 305;
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 11);
  Result[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'SB_BANK_ACCOUNTS_NBR';
  Result[4] := 'CHECK_DATE';
  Result[5] := 'STATUS';
  Result[6] := 'STATUS_DATE';
  Result[7] := 'REGISTER_TYPE';
  Result[8] := 'MANUAL_TYPE';
  Result[9] := 'TRANSACTION_EFFECTIVE_DATE';
  Result[10] := 'NOTES_POPULATED';
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'REGISTER_TYPE' then Result := 1
  else if FieldName = 'MANUAL_TYPE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'GROUP_IDENTIFIER' then Result := 15
  else if FieldName = 'NOTES_POPULATED' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else if FieldName = 'CLEARED_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_PR_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_PR_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_PR_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_MANUAL_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_MANUAL_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_PAYMENT_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR_MISCELLANEOUS_CHECKS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[High(Result)].DestFields[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_FED_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].DestFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_LOCAL_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].DestFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATE_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].DestFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_SUI_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].DestFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_BANK_ACCOUNT_REGISTER.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_FED_TAX_LIABILITIES}

class function TTMP_CO_FED_TAX_LIABILITIES.GetClassIndex: Integer;
begin
  Result := 306;
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 9);
  Result[0] := 'CO_FED_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'DUE_DATE';
  Result[4] := 'STATUS';
  Result[5] := 'STATUS_DATE';
  Result[6] := 'TAX_TYPE';
  Result[7] := 'ADJUSTMENT_TYPE';
  Result[8] := 'THIRD_PARTY';
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'TAX_TYPE' then Result := 1
  else if FieldName = 'ADJUSTMENT_TYPE' then Result := 1
  else if FieldName = 'THIRD_PARTY' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_FED_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].DestFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_FED_TAX_LIABILITIES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_LOCAL_TAX}

class function TTMP_CO_LOCAL_TAX.GetClassIndex: Integer;
begin
  Result := 307;
end;

class function TTMP_CO_LOCAL_TAX.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_LOCAL_TAX.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_LOCAL_TAX.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'CO_LOCAL_TAX_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'SY_LOCALS_NBR';
  Result[4] := 'LOCAL_EIN_NUMBER';
  Result[5] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[6] := 'PAYMENT_METHOD';
end;

class function TTMP_CO_LOCAL_TAX.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'LOCAL_EIN_NUMBER' then Result := 15
  else if FieldName = 'PAYMENT_METHOD' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_LOCAL_TAX.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_LOCAL_TAX.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_LOCAL_TAX_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_LOCAL_TAX.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_LOCAL_TAX.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_LOCAL_TAX.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_LOCAL_TAX_LIABILITIES}

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetClassIndex: Integer;
begin
  Result := 308;
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 9);
  Result[0] := 'CO_LOCAL_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'SY_LOCALS_NBR';
  Result[4] := 'DUE_DATE';
  Result[5] := 'STATUS';
  Result[6] := 'STATUS_DATE';
  Result[7] := 'ADJUSTMENT_TYPE';
  Result[8] := 'THIRD_PARTY';
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'ADJUSTMENT_TYPE' then Result := 1
  else if FieldName = 'THIRD_PARTY' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_LOCAL_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].DestFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_LOCAL_TAX_LIABILITIES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_MANUAL_ACH}

class function TTMP_CO_MANUAL_ACH.GetClassIndex: Integer;
begin
  Result := 309;
end;

class function TTMP_CO_MANUAL_ACH.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_MANUAL_ACH.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_MANUAL_ACH.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'CO_MANUAL_ACH_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'DEBIT_DATE';
  Result[4] := 'CREDIT_DATE';
  Result[5] := 'STATUS';
  Result[6] := 'STATUS_DATE';
end;

class function TTMP_CO_MANUAL_ACH.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_MANUAL_ACH.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_MANUAL_ACH.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_MANUAL_ACH_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_MANUAL_ACH.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_EE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'EE_NBR';
  Result[High(Result)].DestFields[0] := 'EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_EE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'FROM_EE_NBR';
  Result[High(Result)].DestFields[0] := 'EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_EE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'TO_EE_NBR';
  Result[High(Result)].DestFields[0] := 'EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'TO_CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'FROM_CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_MANUAL_ACH.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_MANUAL_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_MANUAL_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_MANUAL_ACH.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_PR_ACH}

class function TTMP_CO_PR_ACH.GetClassIndex: Integer;
begin
  Result := 310;
end;

class function TTMP_CO_PR_ACH.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_PR_ACH.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_PR_ACH.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 12);
  Result[0] := 'CO_PR_ACH_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'ACH_DATE';
  Result[4] := 'STATUS';
  Result[5] := 'STATUS_DATE';
  Result[6] := 'PR_NBR';
  Result[7] := 'TRUST_IMPOUND';
  Result[8] := 'TAX_IMPOUND';
  Result[9] := 'DD_IMPOUND';
  Result[10] := 'BILLING_IMPOUND';
  Result[11] := 'WC_IMPOUND';
end;

class function TTMP_CO_PR_ACH.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'TRUST_IMPOUND' then Result := 1
  else if FieldName = 'TAX_IMPOUND' then Result := 1
  else if FieldName = 'DD_IMPOUND' then Result := 1
  else if FieldName = 'BILLING_IMPOUND' then Result := 1
  else if FieldName = 'WC_IMPOUND' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_PR_ACH.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_PR_ACH.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_PR_ACH_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_PR_ACH.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_PR_ACH.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_PR_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_PR_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_PR_ACH.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_REPORTS}

class function TTMP_CO_REPORTS.GetClassIndex: Integer;
begin
  Result := 311;
end;

class function TTMP_CO_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'CO_REPORTS_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'REPORT_WRITER_REPORTS_NBR';
  Result[4] := 'REPORT_LEVEL';
  Result[5] := 'CUSTOM_NAME';
end;

class function TTMP_CO_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'REPORT_LEVEL' then Result := 1
  else if FieldName = 'CUSTOM_NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_REPORTS_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_RETURN_QUEUE;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CO_REPORTS_NBR';
  Result[High(Result)].DestFields[1] := 'CO_REPORTS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_STATES}

class function TTMP_CO_STATES.GetClassIndex: Integer;
begin
  Result := 312;
end;

class function TTMP_CO_STATES.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_STATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_STATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 11);
  Result[0] := 'CO_STATES_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'STATE';
  Result[4] := 'STATE_TAX_DEPOSIT_METHOD';
  Result[5] := 'SUI_TAX_DEPOSIT_METHOD';
  Result[6] := 'SUI_TAX_DEPOSIT_FREQUENCY';
  Result[7] := 'STATE_EIN';
  Result[8] := 'SUI_EIN';
  Result[9] := 'STATE_SDI_EIN';
  Result[10] := 'TCD_PAYMENT_METHOD';
end;

class function TTMP_CO_STATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'STATE_TAX_DEPOSIT_METHOD' then Result := 1
  else if FieldName = 'SUI_TAX_DEPOSIT_METHOD' then Result := 1
  else if FieldName = 'SUI_TAX_DEPOSIT_FREQUENCY' then Result := 1
  else if FieldName = 'STATE_EIN' then Result := 30
  else if FieldName = 'SUI_EIN' then Result := 30
  else if FieldName = 'STATE_SDI_EIN' then Result := 30
  else if FieldName = 'TCD_PAYMENT_METHOD' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_STATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_STATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_STATES_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_STATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_STATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CO_STATES_NBR';
  Result[High(Result)].DestFields[1] := 'HOME_CO_STATES_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_STATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_STATE_TAX_LIABILITIES}

class function TTMP_CO_STATE_TAX_LIABILITIES.GetClassIndex: Integer;
begin
  Result := 313;
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 10);
  Result[0] := 'CO_STATE_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'STATE';
  Result[4] := 'TAX_TYPE';
  Result[5] := 'DUE_DATE';
  Result[6] := 'STATUS';
  Result[7] := 'STATUS_DATE';
  Result[8] := 'ADJUSTMENT_TYPE';
  Result[9] := 'THIRD_PARTY';
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'TAX_TYPE' then Result := 1
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'ADJUSTMENT_TYPE' then Result := 1
  else if FieldName = 'THIRD_PARTY' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_STATE_TAX_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].DestFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_STATE_TAX_LIABILITIES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_SUI_LIABILITIES}

class function TTMP_CO_SUI_LIABILITIES.GetClassIndex: Integer;
begin
  Result := 314;
end;

class function TTMP_CO_SUI_LIABILITIES.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_SUI_LIABILITIES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_SUI_LIABILITIES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 9);
  Result[0] := 'CO_SUI_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'SY_SUI_NBR';
  Result[4] := 'DUE_DATE';
  Result[5] := 'STATUS';
  Result[6] := 'STATUS_DATE';
  Result[7] := 'ADJUSTMENT_TYPE';
  Result[8] := 'THIRD_PARTY';
end;

class function TTMP_CO_SUI_LIABILITIES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'ADJUSTMENT_TYPE' then Result := 1
  else if FieldName = 'THIRD_PARTY' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_SUI_LIABILITIES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_SUI_LIABILITIES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_SUI_LIABILITIES_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_SUI_LIABILITIES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  Result[High(Result)].DestFields[0] := 'CO_BANK_ACCOUNT_REGISTER_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_SUI_LIABILITIES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_SUI_LIABILITIES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_TAX_DEPOSITS}

class function TTMP_CO_TAX_DEPOSITS.GetClassIndex: Integer;
begin
  Result := 315;
end;

class function TTMP_CO_TAX_DEPOSITS.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_TAX_DEPOSITS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_TAX_DEPOSITS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'STATUS';
  Result[4] := 'STATUS_DATE';
  Result[5] := 'DEPOSIT_TYPE';
end;

class function TTMP_CO_TAX_DEPOSITS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TAX_PAYMENT_REFERENCE_NUMBER' then Result := 20
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'DEPOSIT_TYPE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'TRACE_NUMBER' then Result := 15
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_TAX_DEPOSITS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_TAX_DEPOSITS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_TAX_DEPOSITS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_TAX_DEPOSITS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_FED_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_LOCAL_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATE_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_SUI_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_PAYMENT_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR_MISCELLANEOUS_CHECKS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_TAX_DEPOSITS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_TAX_PAYMENT_ACH}

class function TTMP_CO_TAX_PAYMENT_ACH.GetClassIndex: Integer;
begin
  Result := 316;
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'ACH_DATE';
  Result[4] := 'STATUS';
  Result[5] := 'STATUS_DATE';
  Result[6] := 'CO_TAX_DEPOSITS_NBR';
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_PAYMENT_ACH_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_CO_TAX_PAYMENT_ACH.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_CO_TAX_RETURN_QUEUE}

class function TTMP_CO_TAX_RETURN_QUEUE.GetClassIndex: Integer;
begin
  Result := 317;
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 12);
  Result[0] := 'CO_TAX_RETURN_QUEUE_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'STATUS';
  Result[4] := 'STATUS_DATE';
  Result[5] := 'SB_COPY_PRINTED';
  Result[6] := 'CLIENT_COPY_PRINTED';
  Result[7] := 'AGENCY_COPY_PRINTED';
  Result[8] := 'DUE_DATE';
  Result[9] := 'PERIOD_END_DATE';
  Result[10] := 'SY_REPORTS_GROUP_NBR';
  Result[11] := 'PRODUCE_ASCII_FILE';
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'SB_COPY_PRINTED' then Result := 1
  else if FieldName = 'CLIENT_COPY_PRINTED' then Result := 1
  else if FieldName = 'AGENCY_COPY_PRINTED' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'PRODUCE_ASCII_FILE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'CO_TAX_RETURN_QUEUE_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_CO_CONSOLIDATION;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].DestFields[0] := 'CL_CO_CONSOLIDATION_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_NBR';
  Result[High(Result)].DestFields[0] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CO_REPORTS_NBR';
  Result[High(Result)].DestFields[1] := 'CO_REPORTS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_CO_TAX_RETURN_QUEUE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_EE}

class function TTMP_EE.GetClassIndex: Integer;
begin
  Result := 318;
end;

class function TTMP_EE.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_EE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_EE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'EE_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CL_PERSON_NBR';
  Result[3] := 'CO_NBR';
end;

class function TTMP_EE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SELFSERVE_USERNAME' then Result := 64
  else if FieldName = 'SELFSERVE_PASSWORD' then Result := 64
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_EE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_EE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'EE_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_EE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CL_PERSON;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CL_PERSON_NBR';
  Result[High(Result)].DestFields[0] := 'CL_PERSON_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_EE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'EE_NBR';
  Result[High(Result)].DestFields[0] := 'EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'EE_NBR';
  Result[High(Result)].DestFields[0] := 'FROM_EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_MANUAL_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'EE_NBR';
  Result[High(Result)].DestFields[0] := 'TO_EE_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_EE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_PR}

class function TTMP_PR.GetClassIndex: Integer;
begin
  Result := 319;
end;

class function TTMP_PR.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_PR.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_PR.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 12);
  Result[0] := 'PR_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'RUN_NUMBER';
  Result[4] := 'CHECK_DATE';
  Result[5] := 'SCHEDULED';
  Result[6] := 'PAYROLL_TYPE';
  Result[7] := 'STATUS';
  Result[8] := 'STATUS_DATE';
  Result[9] := 'APPROVED_BY_FINANCE';
  Result[10] := 'APPROVED_BY_TAX';
  Result[11] := 'APPROVED_BY_MANAGEMENT';
end;

class function TTMP_PR.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SCHEDULED' then Result := 1
  else if FieldName = 'PAYROLL_TYPE' then Result := 1
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'APPROVED_BY_FINANCE' then Result := 1
  else if FieldName = 'APPROVED_BY_TAX' then Result := 1
  else if FieldName = 'APPROVED_BY_MANAGEMENT' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_PR.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_PR.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'PR_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_PR.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_PR.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_FED_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_LOCAL_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_PR_ACH;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_STATE_TAX_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_SUI_LIABILITIES;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR_MISCELLANEOUS_CHECKS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR_SCHEDULED_EVENT;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_PR.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_PR_MISCELLANEOUS_CHECKS}

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetClassIndex: Integer;
begin
  Result := 320;
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'PR_NBR';
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_TAX_DEPOSITS;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].DestFields[0] := 'CO_TAX_DEPOSITS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO_BANK_ACCOUNT_REGISTER;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[High(Result)].DestFields[0] := 'PR_MISCELLANEOUS_CHECKS_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
end;

class function TTMP_PR_MISCELLANEOUS_CHECKS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TTMP_PR_SCHEDULED_EVENT}

class function TTMP_PR_SCHEDULED_EVENT.GetClassIndex: Integer;
begin
  Result := 321;
end;

class function TTMP_PR_SCHEDULED_EVENT.GetEntityName: String;
begin
  Result := '';
end;

class function TTMP_PR_SCHEDULED_EVENT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TTMP_PR_SCHEDULED_EVENT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'PR_SCHEDULED_EVENT_NBR';
  Result[1] := 'CL_NBR';
  Result[2] := 'CO_NBR';
  Result[3] := 'STATUS';
  Result[4] := 'EVENT_DATE';
  Result[5] := 'SCHEDULED_PROCESS_DATE';
  Result[6] := 'SCHEDULED_DELIVERY_DATE';
end;

class function TTMP_PR_SCHEDULED_EVENT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TTMP_PR_SCHEDULED_EVENT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TTMP_PR_SCHEDULED_EVENT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'PR_SCHEDULED_EVENT_NBR';
  Result[1] := 'CL_NBR';
end;

class function TTMP_PR_SCHEDULED_EVENT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_PR;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'PR_NBR';
  Result[High(Result)].DestFields[0] := 'PR_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_TEMPORARY_;
  Result[High(Result)].Table := TTMP_CO;
  SetLength(Result[High(Result)].SrcFields, 2);
  SetLength(Result[High(Result)].DestFields, 2);
  Result[High(Result)].SrcFields[0] := 'CO_NBR';
  Result[High(Result)].DestFields[0] := 'CO_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].SrcFields[1] := 'CL_NBR';
  Result[High(Result)].DestFields[1] := 'CL_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TTMP_PR_SCHEDULED_EVENT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TTMP_PR_SCHEDULED_EVENT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TDM_TEMPORARY_}

class function TDM_TEMPORARY_.GetDBType: TevDBType;
begin
  Result := dbtTemporary;
end;

function TDM_TEMPORARY_.GetTMP_CL: TTMP_CL;
begin
  Result := GetDataSet(TTMP_CL) as TTMP_CL;
end;

function TDM_TEMPORARY_.GetTMP_CL_CO_CONSOLIDATION: TTMP_CL_CO_CONSOLIDATION;
begin
  Result := GetDataSet(TTMP_CL_CO_CONSOLIDATION) as TTMP_CL_CO_CONSOLIDATION;
end;

function TDM_TEMPORARY_.GetTMP_CL_PERSON: TTMP_CL_PERSON;
begin
  Result := GetDataSet(TTMP_CL_PERSON) as TTMP_CL_PERSON;
end;

function TDM_TEMPORARY_.GetTMP_CO: TTMP_CO;
begin
  Result := GetDataSet(TTMP_CO) as TTMP_CO;
end;

function TDM_TEMPORARY_.GetTMP_CO_BANK_ACCOUNT_REGISTER: TTMP_CO_BANK_ACCOUNT_REGISTER;
begin
  Result := GetDataSet(TTMP_CO_BANK_ACCOUNT_REGISTER) as TTMP_CO_BANK_ACCOUNT_REGISTER;
end;

function TDM_TEMPORARY_.GetTMP_CO_FED_TAX_LIABILITIES: TTMP_CO_FED_TAX_LIABILITIES;
begin
  Result := GetDataSet(TTMP_CO_FED_TAX_LIABILITIES) as TTMP_CO_FED_TAX_LIABILITIES;
end;

function TDM_TEMPORARY_.GetTMP_CO_LOCAL_TAX: TTMP_CO_LOCAL_TAX;
begin
  Result := GetDataSet(TTMP_CO_LOCAL_TAX) as TTMP_CO_LOCAL_TAX;
end;

function TDM_TEMPORARY_.GetTMP_CO_LOCAL_TAX_LIABILITIES: TTMP_CO_LOCAL_TAX_LIABILITIES;
begin
  Result := GetDataSet(TTMP_CO_LOCAL_TAX_LIABILITIES) as TTMP_CO_LOCAL_TAX_LIABILITIES;
end;

function TDM_TEMPORARY_.GetTMP_CO_MANUAL_ACH: TTMP_CO_MANUAL_ACH;
begin
  Result := GetDataSet(TTMP_CO_MANUAL_ACH) as TTMP_CO_MANUAL_ACH;
end;

function TDM_TEMPORARY_.GetTMP_CO_PR_ACH: TTMP_CO_PR_ACH;
begin
  Result := GetDataSet(TTMP_CO_PR_ACH) as TTMP_CO_PR_ACH;
end;

function TDM_TEMPORARY_.GetTMP_CO_REPORTS: TTMP_CO_REPORTS;
begin
  Result := GetDataSet(TTMP_CO_REPORTS) as TTMP_CO_REPORTS;
end;

function TDM_TEMPORARY_.GetTMP_CO_STATES: TTMP_CO_STATES;
begin
  Result := GetDataSet(TTMP_CO_STATES) as TTMP_CO_STATES;
end;

function TDM_TEMPORARY_.GetTMP_CO_STATE_TAX_LIABILITIES: TTMP_CO_STATE_TAX_LIABILITIES;
begin
  Result := GetDataSet(TTMP_CO_STATE_TAX_LIABILITIES) as TTMP_CO_STATE_TAX_LIABILITIES;
end;

function TDM_TEMPORARY_.GetTMP_CO_SUI_LIABILITIES: TTMP_CO_SUI_LIABILITIES;
begin
  Result := GetDataSet(TTMP_CO_SUI_LIABILITIES) as TTMP_CO_SUI_LIABILITIES;
end;

function TDM_TEMPORARY_.GetTMP_CO_TAX_DEPOSITS: TTMP_CO_TAX_DEPOSITS;
begin
  Result := GetDataSet(TTMP_CO_TAX_DEPOSITS) as TTMP_CO_TAX_DEPOSITS;
end;

function TDM_TEMPORARY_.GetTMP_CO_TAX_PAYMENT_ACH: TTMP_CO_TAX_PAYMENT_ACH;
begin
  Result := GetDataSet(TTMP_CO_TAX_PAYMENT_ACH) as TTMP_CO_TAX_PAYMENT_ACH;
end;

function TDM_TEMPORARY_.GetTMP_CO_TAX_RETURN_QUEUE: TTMP_CO_TAX_RETURN_QUEUE;
begin
  Result := GetDataSet(TTMP_CO_TAX_RETURN_QUEUE) as TTMP_CO_TAX_RETURN_QUEUE;
end;

function TDM_TEMPORARY_.GetTMP_EE: TTMP_EE;
begin
  Result := GetDataSet(TTMP_EE) as TTMP_EE;
end;

function TDM_TEMPORARY_.GetTMP_PR: TTMP_PR;
begin
  Result := GetDataSet(TTMP_PR) as TTMP_PR;
end;

function TDM_TEMPORARY_.GetTMP_PR_MISCELLANEOUS_CHECKS: TTMP_PR_MISCELLANEOUS_CHECKS;
begin
  Result := GetDataSet(TTMP_PR_MISCELLANEOUS_CHECKS) as TTMP_PR_MISCELLANEOUS_CHECKS;
end;

function TDM_TEMPORARY_.GetTMP_PR_SCHEDULED_EVENT: TTMP_PR_SCHEDULED_EVENT;
begin
  Result := GetDataSet(TTMP_PR_SCHEDULED_EVENT) as TTMP_PR_SCHEDULED_EVENT;
end;

procedure Register;
begin
  RegisterComponents('EvoDdTemp', [
    TTMP_CL,
    TTMP_CL_CO_CONSOLIDATION,
    TTMP_CL_PERSON,
    TTMP_CO,
    TTMP_CO_BANK_ACCOUNT_REGISTER,
    TTMP_CO_FED_TAX_LIABILITIES,
    TTMP_CO_LOCAL_TAX,
    TTMP_CO_LOCAL_TAX_LIABILITIES,
    TTMP_CO_MANUAL_ACH,
    TTMP_CO_PR_ACH,
    TTMP_CO_REPORTS,
    TTMP_CO_STATES,
    TTMP_CO_STATE_TAX_LIABILITIES,
    TTMP_CO_SUI_LIABILITIES,
    TTMP_CO_TAX_DEPOSITS,
    TTMP_CO_TAX_PAYMENT_ACH,
    TTMP_CO_TAX_RETURN_QUEUE,
    TTMP_EE,
    TTMP_PR,
    TTMP_PR_MISCELLANEOUS_CHECKS,
    TTMP_PR_SCHEDULED_EVENT]);
end;

initialization
  RegisterClasses([
    TTMP_CL,
    TTMP_CL_CO_CONSOLIDATION,
    TTMP_CL_PERSON,
    TTMP_CO,
    TTMP_CO_BANK_ACCOUNT_REGISTER,
    TTMP_CO_FED_TAX_LIABILITIES,
    TTMP_CO_LOCAL_TAX,
    TTMP_CO_LOCAL_TAX_LIABILITIES,
    TTMP_CO_MANUAL_ACH,
    TTMP_CO_PR_ACH,
    TTMP_CO_REPORTS,
    TTMP_CO_STATES,
    TTMP_CO_STATE_TAX_LIABILITIES,
    TTMP_CO_SUI_LIABILITIES,
    TTMP_CO_TAX_DEPOSITS,
    TTMP_CO_TAX_PAYMENT_ACH,
    TTMP_CO_TAX_RETURN_QUEUE,
    TTMP_EE,
    TTMP_PR,
    TTMP_PR_MISCELLANEOUS_CHECKS,
    TTMP_PR_SCHEDULED_EVENT]);

end.
