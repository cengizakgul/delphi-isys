// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvTaskParamFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, EvCommonInterfaces;

type
  TTaskParamFrame = class(TFrame)
    lNoParams: TLabel;
  protected
    FTask: IevTask;
    FModified: Boolean;
    procedure SetModified(const Value: Boolean); virtual;
    function  GetModified: Boolean; virtual;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); reintroduce; virtual;
    procedure   AfterConstruction; override;
    procedure   ApplyUpdates; virtual;
    procedure   CancelUpdates; virtual;
    property    Modified: Boolean read GetModified write SetModified;
  end;

  TTaskParamFrameClass = class of TTaskParamFrame;

implementation

{$R *.DFM}

{ TTaskParamFrame }

procedure TTaskParamFrame.AfterConstruction;
begin
  inherited;
  lNoParams.Visible := ControlCount = 1;
end;

procedure TTaskParamFrame.ApplyUpdates;
begin
  FModified := False;
end;

procedure TTaskParamFrame.CancelUpdates;
begin

end;

constructor TTaskParamFrame.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited Create(AOwner);
  Parent := AParent;
  FTask := ATask;
  FModified := False;
end;

function TTaskParamFrame.GetModified: Boolean;
begin
  Result := FModified;
end;

procedure TTaskParamFrame.SetModified(const Value: Boolean);
begin
  FModified := Value;
end;

end.
