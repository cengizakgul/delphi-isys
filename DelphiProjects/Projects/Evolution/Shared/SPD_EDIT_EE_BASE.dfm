inherited EDIT_EE_BASE: TEDIT_EE_BASE
  Width = 1220
  inherited Panel1: TevPanel
    Width = 1220
    Height = 54
    Color = clBtnShadow
    inherited pnlFavoriteReport: TevPanel
      Left = 1068
      Width = 152
      Height = 54
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      inherited spbFavoriteReports: TevSpeedButton
        Left = 56
        Top = 16
      end
      inherited btnSwipeClock: TevSpeedButton
        Left = 86
        Top = 16
      end
      inherited BtnMRCLogIn: TevSpeedButton
        Left = 116
        Top = 16
      end
      object evLabel47: TevLabel
        Left = 2
        Top = 2
        Width = 148
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object pnlTopLeft: TevPanel
      Left = 0
      Top = 0
      Width = 1068
      Height = 54
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object lEeFilter: TLabel
        Left = 116
        Top = 29
        Width = 47
        Height = 13
        Caption = 'lEeFilter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblEENbr: TevLabel
        Left = 328
        Top = 6
        Width = 47
        Height = 13
        Caption = 'EE CODE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblCompany: TevLabel
        Left = 408
        Top = 24
        Width = 53
        Height = 13
        Caption = 'COMPANY'
        Color = 4276545
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object lblClient: TevLabel
        Left = 408
        Top = 6
        Width = 38
        Height = 13
        Caption = 'CLIENT'
        Color = 4276545
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object EmployeeNbrText: TevDBText
        Left = 328
        Top = 24
        Width = 70
        Height = 18
        DataField = 'CUSTOM_EMPLOYEE_NUMBER'
        DataSource = wwdsEmployee
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtxClientNbr: TevDBText
        Left = 465
        Top = 6
        Width = 104
        Height = 15
        Color = 4276545
        DataField = 'CUSTOM_CLIENT_NUMBER'
        DataSource = dsCL
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object dbtxClientName: TevDBText
        Left = 571
        Top = 6
        Width = 90
        Height = 15
        AutoSize = True
        Color = 4276545
        DataField = 'NAME'
        DataSource = dsCL
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object DBText4: TevDBText
        Left = 8
        Top = 29
        Width = 43
        Height = 14
        AutoSize = True
        DataField = 'SOCIAL_SECURITY_NUMBER'
        DataSource = wwdsSubMaster2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText3: TevDBText
        Left = 170
        Top = 4
        Width = 88
        Height = 27
        AutoSize = True
        DataField = 'FIRST_NAME'
        DataSource = wwdsSubMaster2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object DBText2: TevDBText
        Left = 8
        Top = 2
        Width = 94
        Height = 29
        AutoSize = True
        DataField = 'LAST_NAME'
        DataSource = wwdsSubMaster2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtCoNumber: TevDBText
        Left = 465
        Top = 24
        Width = 104
        Height = 17
        Color = 4276545
        DataField = 'CUSTOM_COMPANY_NUMBER'
        DataSource = wwdsMaster
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object CompanyNameText: TevDBText
        Left = 571
        Top = 24
        Width = 155
        Height = 18
        Color = 4276545
        DataField = 'NAME'
        DataSource = wwdsMaster
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblComma: TevLabel
        Left = 104
        Top = 2
        Width = 7
        Height = 29
        Caption = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object lblSSN: TevLabel
        Left = 760
        Top = 5
        Width = 47
        Height = 13
        Caption = 'EE CODE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
    end
  end
  inherited PageControl1: TevPageControl
    Top = 87
    Width = 1220
    Height = 179
    TabOrder = 2
    TabStop = False
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1212
        Height = 150
        inherited pnlBorder: TevPanel
          Width = 1208
          Height = 146
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1208
          Height = 146
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 360
              Top = 76
            end
            inherited pnlSubbrowse: TevPanel
              Left = 369
              Top = 85
            end
            object sbEESunkenBrowse2: TScrollBox
              Left = 0
              Top = 0
              Width = 800
              Height = 600
              Align = alClient
              Color = clBtnFace
              ParentColor = False
              TabOrder = 2
              object pnlBrowseBorder: TevPanel
                Left = 0
                Top = 0
                Width = 796
                Height = 596
                Align = alClient
                BevelOuter = bvNone
                BorderWidth = 12
                ParentColor = True
                TabOrder = 0
                object evSplitterSunken2: TevSplitter
                  Left = 248
                  Top = 12
                  Width = 15
                  Height = 572
                end
                object pnlEEBrowseLeft2: TevPanel
                  Left = 36
                  Top = 396
                  Width = 275
                  Height = 537
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 0
                  Visible = False
                end
                object pnlEEBrowseRight2: TevPanel
                  Left = 15
                  Top = 200
                  Width = 769
                  Height = 537
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 1
                  Visible = False
                end
                object fpEEBrowseLeft2: TisUIFashionPanel
                  Left = 12
                  Top = 12
                  Width = 236
                  Height = 572
                  Align = alLeft
                  BevelOuter = bvNone
                  BorderWidth = 12
                  Color = 14737632
                  TabOrder = 2
                  OnResize = fpEEBrowseLeft2Resize
                  RoundRect = True
                  ShadowDepth = 8
                  ShadowSpace = 8
                  ShowShadow = True
                  ShadowColor = clSilver
                  TitleColor = clGrayText
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWhite
                  TitleFont.Height = -13
                  TitleFont.Name = 'Arial'
                  TitleFont.Style = [fsBold]
                  Title = 'Company'
                  LineWidth = 0
                  LineColor = clWhite
                  Theme = ttCustom
                  object pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 16
                    Top = 35
                    Width = 185
                    Height = 264
                    BevelOuter = bvNone
                    ParentColor = True
                    TabOrder = 0
                    Visible = False
                  end
                end
                object fpEEBrowseRight2: TisUIFashionPanel
                  Left = 263
                  Top = 12
                  Width = 521
                  Height = 572
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 12
                  Color = 14737632
                  TabOrder = 3
                  OnResize = fpEEBrowseRight2Resize
                  RoundRect = True
                  ShadowDepth = 8
                  ShadowSpace = 8
                  ShowShadow = True
                  ShadowColor = clSilver
                  TitleColor = clGrayText
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWhite
                  TitleFont.Height = -13
                  TitleFont.Name = 'Arial'
                  TitleFont.Style = [fsBold]
                  Title = 'Employee'
                  LineWidth = 0
                  LineColor = clWhite
                  Theme = ttCustom
                  object pnlFPEEBrowseRightBody2: TevPanel
                    Left = 16
                    Top = 35
                    Width = 185
                    Height = 264
                    BevelOuter = bvNone
                    ParentColor = True
                    TabOrder = 0
                    Visible = False
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 1160
            inherited BitBtn1: TevBitBtn
              Caption = 'Open company'
            end
            object bOpenFilteredByDBDTG: TevBitBtn
              Left = 153
              Top = 0
              Width = 150
              Height = 25
              Caption = 'Open By D/B/D/T/PG'
              TabOrder = 1
              OnClick = bOpenFilteredByDBDTGClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
                C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
                C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
                DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
                CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
              NumGlyphs = 2
              ParentColor = False
              Margin = 0
            end
            object bOpenFilteredByEe: TevBitBtn
              Left = 306
              Top = 0
              Width = 150
              Height = 25
              Caption = 'Open By EE#,SSN,Name'
              TabOrder = 2
              OnClick = bOpenFilteredByEeClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
                C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
                C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
                DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
                CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
              NumGlyphs = 2
              ParentColor = False
              Margin = 0
            end
            object cbShowRemovedEmployees: TevCheckBox
              Left = 459
              Top = 4
              Width = 117
              Height = 17
              Caption = 'Show Removed EEs'
              TabOrder = 3
              OnClick = cbShowRemovedEmployeesClick
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 1160
            Height = 39
            inherited Splitter1: TevSplitter
              Left = 315
              Height = 35
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 315
              Height = 35
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 265
                Height = 10
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'Fein'#9)
                IniAttributes.SectionName = 'TEDIT_EE_BASE\wwdbgridSelectClient'
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 318
              Width = 838
              Height = 35
              Title = 'Employee'
              object wwDBGrid4: TevDBGrid
                Left = 18
                Top = 48
                Width = 788
                Height = 10
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'CURRENT_TERMINATION_CODE;CustomEdit;cbEmployeeCode')
                Selected.Strings = (
                  'CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F'
                  'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
                  'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
                  'Employee_MI_Calculate'#9'2'#9'MI'#9'F'
                  'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
                  'CURRENT_TERMINATION_CODE_DESC'#9'40'#9'Status'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EE_BASE\wwDBGrid4'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsEmployee
                MultiSelectOptions = [msoAutoUnselect]
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnDblClick = wwDBGrid4DblClick
                OnKeyDown = wwDBGrid4KeyDown
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                OnFilterChange = wwDBGrid4FilterChange
                NoFire = False
              end
            end
          end
        end
      end
    end
  end
  object pnlEEChoice: TevPanel [2]
    Left = 0
    Top = 54
    Width = 1220
    Height = 33
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object dblkupEE_NBR: TevDBLookupCombo
      Left = 8
      Top = 6
      Width = 65
      Height = 19
      TabStop = False
      Ctl3D = False
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'Trim_NUMBER'#9'10'#9'Emp#'#9'F'
        'Employee_Name_Calculate'#9'40'#9'Name'#9'F')
      LookupTable = EEClone
      LookupField = 'EE_NBR'
      Style = csDropDownList
      Enabled = False
      ParentColor = True
      ParentCtl3D = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      OrderByDisplay = False
      PreciseEditRegion = False
      AllowClearKey = False
      ShowMatchText = True
      OnChange = dblkupEE_NBRChange
      OnDropDown = dblkupEE_NBRDropDown
      OnCloseUp = dbtxtnameCloseUp
    end
    object dbtxtname: TevDBLookupCombo
      Left = 76
      Top = 6
      Width = 129
      Height = 19
      TabStop = False
      Ctl3D = False
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'Employee_Name_Calculate'#9'40'#9'Name'#9'F')
      LookupTable = EEClone
      LookupField = 'EE_NBR'
      Style = csDropDownList
      Enabled = False
      ParentColor = True
      ParentCtl3D = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      ShowMatchText = True
      OnChange = dblkupEE_NBRChange
      OnCloseUp = dbtxtnameCloseUp
    end
    object butnPrior: TevBitBtn
      Left = 208
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Prior'
      TabOrder = 2
      OnClick = EEPriorExecute
      Color = clBlack
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFAFBFDD4EBF7B7DFF2B0DBF0C4E4F4E8F3FAFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBEBEBEBDF
        DFDFDBDBDBE3E3E3F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFDFEB9E1F358C1ED23B8EE0AB5F003B4F114B7F036BAED85CFEFE3F2
        F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C1C1C1B7B7B7B4
        B4B4B3B3B3B6B6B6BABABACECECEF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        F5FAFC7ACBEF0DAEF000B8FA00BFFE00C2FE00C3FE00C1FE00BEFD00B5F633B7
        EDC8E7F5FFFFFFFFFFFFFFFFFFFFFFFFFAFAFACBCBCBAEAEAEB7B7B7BEBEBEC1
        C1C1C2C2C2C0C0C0BEBEBEB4B4B4B6B6B6E7E7E7FFFFFFFFFFFFFFFFFFFBFDFE
        7CC9EF01A7F500B6FE00B9FD00BBFD00BBFD00BCFD00BDFD00BDFD00BDFE00B6
        FB27B3EED0EAF7FFFFFFFFFFFFFDFDFDC8C8C8A6A6A6B6B6B6B9B9B9BABABABB
        BBBBBBBBBBBCBCBCBCBCBCBDBDBDB5B5B5B2B2B2EAEAEAFFFFFFFFFFFFB9E0F3
        0BA0F000A9FE00B0FD00B3FD00B5FD00B6FD00B5FD00B1FD00B7FD00B9FD00B9
        FE00AFFA52BBEBF5FAFDFFFFFFDFDFDF9F9F9FA8A8A8AFAFAFB2B2B2B4B4B4B5
        B5B5B4B4B4B0B0B0B6B6B6B8B8B8B8B8B8AEAEAEBBBBBBFAFAFAF8FCFD58B9EC
        009EFB00A4FD00A6FD00A9FD00ACFD00AAFD15A7FD6DC9FE05B0FD00B3FD00B3
        FD00B3FE09A7F0BCE3F5FFFEFDB6B6B69D9D9DA3A3A3A5A5A5A8A8A8ABABABAA
        AAAAA6A6A6C8C8C8AFAFAFB2B2B2B2B2B2B2B2B2A6A6A6E2E2E2D5ECF622ABEE
        00A7FE00A5FD00A2FD00A2FD009FFD1299FDB5DCFEC6E9FE01A8FD00AFFD00B0
        FD00AFFE00A9F87DCCF0F6F2F1A7A7A7A5A5A5A4A4A4A0A0A0A0A0A09F9F9F98
        9898DCDCDCE9E9E9A7A7A7AEAEAEAEAEAEAEAEAEA8A8A8CBCBCBB8E0F109AFF1
        00ADFE00ACFD00AAFD00A4FD1697FCB1D6FEFFFFFFBDE4FE01A2FD00ABFD00AC
        FD00AAFD00AFFB56C3EEEBE7E6ACABABABABABABABABA8A8A8A2A2A2979797D6
        D6D6FFFFFFE4E4E4A0A0A0AAAAAAABABABA9A9A9AEAEAEC1C1C1B0DCF003B5F2
        00B1FE00B4FD00B4FD02B1FD82D4FEFFFFFFFFFFFFBCE6FE01A9FD00ACFD00AB
        FD00A9FD00B0FC4DC2EEE8E4E3B2B2B1B0B0B0B3B3B3B3B3B3B0B0B0D3D3D3FF
        FFFFFFFFFFE6E6E6A7A7A7ABABABA9A9A9A7A7A7AFAFAFC0C0C0C4E5F414B6F0
        00B7FE00BAFD00BDFD00BFFD16C3FDB1EAFEFFFFFFBDECFE01BDFD00BFFE00BA
        FD00AFFE00ACFA66C8EFF0EBEAB4B4B3B6B6B6B9B9B9BDBDBDBEBEBEC3C3C3EA
        EAEAFFFFFFEBEBEBBCBCBCBEBEBEB9B9B9AEAEAEAAAAAAC6C6C6E8F4FA37BAED
        00B9FD00C1FD00C6FD00C9FD00C9FD12CCFDB5F1FFC6F0FE01C5FD00C7FE00C5
        FE00C1FF00AFF49AD6F1FCF8F7B7B7B7B8B8B8C0C0C0C5C5C5C8C8C8C9C9C9CB
        CBCBF0F0F0F0F0F0C4C4C4C6C6C6C5C5C5C0C0C0AEAEAED5D5D5FFFFFF86D0EF
        00B6F600C8FE00CEFD00D1FD00D4FE00D4FE15D8FE6DE6FF05D2FE00D0FE00CB
        FE00C2FE23B8EDDDF0F8FFFFFFCECECEB5B5B5C7C7C7CDCDCDD0D0D0D3D3D3D4
        D4D4D7D7D7E6E6E6D1D1D1CFCFCFCBCBCBC1C1C1B7B7B7EFEFEFFFFFFFE3F2F9
        33BFED00C7FC00D5FE00D8FE00DBFE00DDFE00DDFE00DCFE00DCFE00D8FE00D2
        FF02BFF490D5F0FFFFFFFFFFFFF1F1F1BEBEBEC6C6C6D4D4D4D8D8D8DBDBDBDD
        DDDDDDDDDDDCDCDCDBDBDBD8D8D8D1D1D1BEBEBED4D4D4FFFFFFFFFFFFFFFFFF
        C8E8F628C3EE00D3FA00E1FF00E3FF00E3FE00E4FE00E4FE00E4FF00DEFE02C8
        F575CFEEF8FBFDFFFFFFFFFFFFFFFFFFE8E8E8C2C2C2D2D2D2E0E0E0E2E2E2E3
        E3E3E3E3E3E3E3E3E3E3E3DEDEDEC8C8C8CFCFCFFBFBFBFFFFFFFFFFFFFFFFFF
        FFFFFFD1EBF753C7EC09CDF100DDF800E6FC00E8FD00E3FB00D6F526C8ED90D6
        F0F9FAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEAC7C7C7CCCCCCDCDCDCE6
        E6E6E8E8E8E2E2E2D5D5D5C7C7C7D6D6D6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF7F9FCBDE3F47ED4EF58CCEC4FCAEB68D0ED9BDAF1DEEFF8FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9E3E3E3D3D3D3CB
        CBCBC9C9C9CFCFCFDADADAEEEEEEFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentColor = False
      Margin = 0
    end
    object butnNext: TevBitBtn
      Left = 286
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Next'
      TabOrder = 3
      OnClick = EENextExecute
      Color = clBlack
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFAFBFDD4EBF7B7DFF2B0DBF0C4E4F4E8F3FAFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBEBEBEBDF
        DFDFDBDBDBE3E3E3F3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFDFDFEB9E1F358C1ED23B8EE0AB5F003B4F114B7F036BAED85CFEFE3F2
        F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C1C1C1B7B7B7B4
        B4B4B3B3B3B6B6B6BABABACECECEF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        F5FAFC7ACBEF0DAEF000B8FA00BFFE00C2FE00C3FE00C1FE00BEFD00B5F633B7
        EDC8E7F5FFFFFFFFFFFFFFFFFFFFFFFFFAFAFACBCBCBAEAEAEB7B7B7BEBEBEC1
        C1C1C2C2C2C0C0C0BEBEBEB4B4B4B6B6B6E7E7E7FFFFFFFFFFFFFFFFFFFBFDFE
        7CC9EF01A7F500B6FE00B9FD00BBFD00BCFD00BCFD00BCFD00BDFD00BDFE00B6
        FB27B3EED0EAF7FFFFFFFFFFFFFDFDFDC8C8C8A6A6A6B6B6B6B9B9B9BABABABC
        BCBCBBBBBBBBBBBBBCBCBCBDBDBDB5B5B5B2B2B2EAEAEAFFFFFFFFFFFFB9E0F3
        0BA0F000A9FE00B0FD00B3FD00B4FD00AFFD00B5FD00B8FD00B8FD00B9FD00B9
        FE00AFFA52BBEBF5FAFDFFFFFFDFDFDF9F9F9FA8A8A8AFAFAFB2B2B2B3B3B3AE
        AEAEB4B4B4B7B7B7B7B7B7B8B8B8B8B8B8AEAEAEBBBBBBFAFAFAF8FCFD58B9EC
        009EFB00A4FD00A6FD00A9FD05A9FD6DC6FE15A7FD00AEFD00B3FD00B3FD00B3
        FD00B3FE09A7F0BCE3F5FFFEFDB6B6B69D9D9DA3A3A3A5A5A5A8A8A8A8A8A8C5
        C5C5A6A6A6ADADADB3B3B3B2B2B2B2B2B2B2B2B2A6A6A6E2E2E2D5ECF622ABEE
        00A7FE00A5FD00A3FD00A2FD019EFDC6E7FEB5DDFE129FFD00AAFD00AFFD00B0
        FD00AFFE00A9F87DCCF0F6F2F1A7A7A7A5A5A5A4A4A4A0A0A0A0A0A09C9C9CE7
        E7E7DCDCDC9E9E9EA9A9A9AEAEAEAEAEAEAEAEAEA8A8A8CBCBCBB8E0F109AFF1
        00ADFE00ACFD00A9FD00A5FD019BFDBDE1FEFFFFFFB1D9FE169FFC00A9FD00AC
        FD00AAFD00AFFB56C3EEEBE7E6ACABABABABABABABABA8A8A8A4A4A49A9A9AE1
        E1E1FFFFFFD8D8D89E9E9EA8A8A8ABABABA9A9A9AEAEAEC1C1C1B0DCF003B5F2
        00B1FE00B4FD00B4FD00B4FD01AEFDBCE7FEFFFFFFFFFFFF82D3FE02A9FD00AB
        FD00A9FD00B0FC4DC2EEE8E4E3B2B2B1B0B0B0B3B3B3B3B3B3B3B3B3ADADADE7
        E7E7FFFFFFFFFFFFD2D2D2A8A8A8A9A9A9A7A7A7AFAFAFC0C0C0C4E5F414B6F0
        00B7FE00BAFD00BDFD00C0FD01BCFDBDECFEFFFFFFB1EAFE16C3FD00BFFE00BA
        FD00AFFE00ACFA66C8EFF0EBEAB4B4B3B6B6B6B9B9B9BDBDBDBFBFBFBCBCBCEB
        EBEBFFFFFFEAEAEAC3C3C3BDBDBDB9B9B9AEAEAEAAAAAAC6C6C6E8F4FA37BAED
        00B9FD00C1FD00C6FD00C9FD01C7FDC6F1FEB5F0FF12CCFD00C8FD00C6FE00C5
        FE00C1FF00AFF49AD6F1FCF8F7B7B7B7B8B8B8C0C0C0C5C5C5C8C8C8C7C7C7F1
        F1F1F0F0F0CBCBCBC7C7C7C5C5C5C5C5C5C0C0C0AEAEAED5D5D5FFFFFF86D0EF
        00B6F600C8FE00CEFD00D1FD05D3FE6DE7FF15D8FE00D3FE00D3FE00D0FE00CB
        FE00C2FE23B8EDDDF0F8FFFFFFCECECEB5B5B5C7C7C7CDCDCDD0D0D0D2D2D2E7
        E7E7D7D7D7D3D3D3D2D2D2CFCFCFCBCBCBC1C1C1B7B7B7EFEFEFFFFFFFE3F2F9
        33BFED00C7FC00D5FE00D8FE00DBFE00DCFE00DDFE00DDFE00DCFE00D8FE00D2
        FF02BFF490D5F0FFFFFFFFFFFFF1F1F1BEBEBEC6C6C6D4D4D4D8D8D8DBDBDBDC
        DCDCDDDDDDDDDDDDDBDBDBD8D8D8D1D1D1BEBEBED4D4D4FFFFFFFFFFFFFFFFFF
        C8E8F628C3EE00D3FA00E1FF00E3FF00E3FE00E4FE00E4FE00E4FF00DEFE02C8
        F575CFEEF8FBFDFFFFFFFFFFFFFFFFFFE8E8E8C2C2C2D2D2D2E0E0E0E2E2E2E3
        E3E3E3E3E3E3E3E3E3E3E3DEDEDEC8C8C8CFCFCFFBFBFBFFFFFFFFFFFFFFFFFF
        FFFFFFD1EBF753C7EC09CDF100DDF800E6FC00E8FD00E3FB00D6F526C8ED90D6
        F0F9FAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEAC7C7C7CCCCCCDCDCDCE6
        E6E6E8E8E8E2E2E2D5D5D5C7C7C7D6D6D6FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF7F9FCBDE3F47ED4EF58CCEC4FCAEB68D0ED9BDAF1DEEFF8FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9E3E3E3D3D3D3CB
        CBCBC9C9C9CFCFCFDADADAEEEEEEFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentColor = False
      Layout = blGlyphRight
      Margin = 0
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_CO.CO
    Left = 144
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsEmployee
    Left = 174
    Top = 58
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 98
    Top = 42
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 305
    Top = 127
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 271
    Top = 87
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 336
    Top = 87
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 434
    Top = 90
  end
  object wwdsEmployee: TevDataSource
    DataSet = DM_EE.EE
    OnDataChange = wwdsEmployeeDataChange
    Left = 166
    Top = 80
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 424
    Top = 54
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_EE.EE
    Left = 462
    Top = 61
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_CL_PERSON.CL_PERSON
    OnDataChange = wwdsSubMaster2DataChange
    Left = 501
    Top = 42
  end
  object ActionList: TevActionList
    Left = 379
    Top = 75
    object EENext: TDataSetNext
      Category = 'Dataset'
      Caption = '&Next'
      Hint = 'Next'
      ImageIndex = 2
      ShortCut = 107
      OnExecute = EENextExecute
      DataSource = wwdsEmployee
    end
    object EEPrior: TDataSetPrior
      Category = 'Dataset'
      Caption = '&Prior'
      Hint = 'Prior'
      ImageIndex = 1
      ShortCut = 106
      OnExecute = EEPriorExecute
      DataSource = wwdsEmployee
    end
    object FocusNbr: TAction
      Caption = 'FocusNbr'
      ShortCut = 16506
      OnExecute = FocusNbrExecute
    end
    object FocusName: TAction
      Caption = 'FocusName'
      ShortCut = 16507
      OnExecute = FocusNameExecute
    end
    object Action1: TAction
      Caption = 'FocusMain'
      ShortCut = 16505
      OnExecute = Action1Execute
    end
  end
  object EEClone: TevClientDataSet
    AfterOpen = EECloneAfterOpen
    BeforeClose = EECloneBeforeClose
    Left = 546
    Top = 42
  end
end
