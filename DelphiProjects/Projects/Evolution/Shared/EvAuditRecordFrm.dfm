object evAuditRecord: TevAuditRecord
  Left = 0
  Top = 0
  Width = 702
  Height = 388
  AutoScroll = False
  TabOrder = 0
  object cbOperationType: TevDBComboBox
    Left = 128
    Top = 80
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Create'#9'I'
      'Update'#9'U'
      'Delete'#9'D')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 2
    UnboundDataType = wwDefault
  end
  object pnlParams: TevPanel
    Left = 0
    Top = 0
    Width = 702
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    Visible = False
    object btnShowBlob: TevSpeedButton
      Left = 568
      Top = 38
      Width = 18
      Height = 19
      Caption = '...'
      HideHint = True
      ParentFont = False
      AutoSize = False
      Visible = False
      OnClick = btnShowBlobClick
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Layout = blGlyphBottom
      Margin = 5
      ParentColor = False
      ShortCut = 0
    end
  end
  object grVersionAudit: TevDBGrid
    Left = 0
    Top = 32
    Width = 702
    Height = 356
    DisableThemesInTitle = False
    ControlType.Strings = (
      'change_type;CustomEdit;cbOperationType;F')
    Selected.Strings = (
      'change_date'#9'23'#9'Change Date'#9'F'
      'change_type'#9'9'#9'Operation'#9'F'
      'field_name'#9'25'#9'Field'#9'F'
      'old_value_text'#9'20'#9'Old Value'#9'F'
      'new_value_text'#9'19'#9'New Value'#9'F'
      'user_name'#9'20'#9'User'#9'F')
    IniAttributes.Enabled = False
    IniAttributes.SaveToRegistry = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TevAuditRecord\grVersionAudit'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    OnCellChanged = grVersionAuditCellChanged
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsrcVersionAudit
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = OnRecordInteract
    OnKeyDown = grVersionAuditKeyDown
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clGrayText
    DefaultSort = 'change_date'
    NoFire = False
    OnCreatingSortIndex = grVersionAuditCreatingSortIndex
    OnSortingChanged = grVersionAuditSortingChanged
  end
  object dsrcVersionAudit: TevDataSource
    AutoEdit = False
    Left = 236
    Top = 184
  end
end
