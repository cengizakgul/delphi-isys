unit EvAuditBlobFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  evBlobViewerFrm, ExtCtrls, isUIFashionPanel, Dialogs;

type
  TevAuditBlob = class(TForm)
    pnlOldValue: TisUIFashionPanel;
    frmOldValue: TevBlobViewer;
    pnlNewValue: TisUIFashionPanel;
    frmNewValue: TevBlobViewer;
    procedure FormResize(Sender: TObject);
  private
  public
    class procedure ShowBlob(const AOldData, ANewData: IisStream);
  end;

implementation

{$R *.dfm}


{ TevAuditBlob }

class procedure TevAuditBlob.ShowBlob(const AOldData, ANewData: IisStream);
var
  Frm: TevAuditBlob;
begin
  Frm := TevAuditBlob.Create(Application);
  try
    Frm.frmOldValue.ShowBlob(AOldData);
    Frm.frmNewValue.ShowBlob(ANewData);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

procedure TevAuditBlob.FormResize(Sender: TObject);
begin
  pnlOldValue.Width := ClientWidth div 2;
  pnlNewValue.Width := ClientWidth div 2;
end;

end.

