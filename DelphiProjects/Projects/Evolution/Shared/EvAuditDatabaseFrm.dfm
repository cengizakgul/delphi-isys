object evAuditDatabase: TevAuditDatabase
  Left = 0
  Top = 0
  Width = 781
  Height = 451
  AutoScroll = False
  TabOrder = 0
  object pnlParams: TevPanel
    Left = 0
    Top = 0
    Width = 781
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    Visible = False
  end
  object grVersionAudit: TevDBGrid
    Left = 0
    Top = 32
    Width = 781
    Height = 419
    DisableThemesInTitle = False
    Selected.Strings = (
      'last_change_date'#9'23'#9'Last Change Date'#9'F'
      'table_name'#9'40'#9'Table'#9'F')
    IniAttributes.Enabled = False
    IniAttributes.SaveToRegistry = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TevAuditRecord\grVersionAudit'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsrcVersionAudit
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = OnRecordInteract
    OnKeyDown = grVersionAuditKeyDown
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clGrayText
    DefaultSort = 'last_change_date'
    NoFire = False
    OnCreatingSortIndex = grVersionAuditCreatingSortIndex
    OnSortingChanged = grVersionAuditSortingChanged
  end
  object dsrcVersionAudit: TevDataSource
    AutoEdit = False
    Left = 236
    Top = 184
  end
end
