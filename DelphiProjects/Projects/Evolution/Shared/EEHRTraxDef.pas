unit EEHRTraxDef;

interface

uses IEMap, EvConsts, SFieldCodeValues;

const
  HRTraxMap: array[0..191] of TMapRecord =
  (
  (ITable: ''; IField: 'LAST NAME'; ETable: 'CL_PERSON'; EField: 'LAST_NAME'; MapType: mtDirect),
  (ITable: ''; IField: 'FIRST NAME'; ETable: 'CL_PERSON'; EField: 'FIRST_NAME'; MapType: mtCustom; FuncName: 'IEFirstNameMI'),
  (ITable: ''; IField: 'FIRST NAME'; ETable: 'CL_PERSON'; EField: 'MIDDLE_INITIAL'; MapType: mtCustom; FuncName: 'IEFirstNameMI'),
  (ITable: ''; IField: 'ADDRESS 1'; ETable: 'CL_PERSON'; EField: 'ADDRESS1'; MapType: mtDirect),
  (ITable: ''; IField: 'ADDRESS 2'; ETable: 'CL_PERSON'; EField: 'ADDRESS2'; MapType: mtDirect),
  (ITable: ''; IField: 'CITY'; ETable: 'CL_PERSON'; EField: 'CITY'; MapType: mtDirect),
  (ITable: ''; IField: 'STATE'; ETable: 'CL_PERSON'; EField: 'STATE'; MapType: mtCustom; FuncName: 'IECL_PERSON_STATE'),
  (ITable: ''; IField: 'ZIP'; ETable: 'CL_PERSON'; EField: 'ZIP_CODE'; MapType: mtDirect),
  (ITable: ''; IField: 'EMP COUNTY'; ETable: 'CL_PERSON'; EField: 'COUNTY'; MapType: mtDirect),
  (ITable: ''; IField: 'COUNTRY'; ETable: 'CL_PERSON'; EField: 'COUNTRY'; MapType: mtDirect),
  (ITable: ''; IField: 'HOME PHONE'; ETable: 'CL_PERSON'; EField: 'PHONE1'; MapType: mtDirect),
  (ITable: ''; IField: 'WORK PHONE'; ETable: 'CL_PERSON'; EField: 'PHONE2'; MapType: mtDirect),
  (ITable: ''; IField: 'E-MAIL'; ETable: 'CL_PERSON'; EField: 'E_MAIL_ADDRESS'; MapType: mtDirect),
  (ITable: ''; IField: 'SSN'; ETable: 'CL_PERSON'; EField: 'SOCIAL_SECURITY_NUMBER'; MapType: mtDirect),

  (ITable: ''; IField: 'W2 FIRST NAME'; ETable: 'CL_PERSON'; EField: 'W2_FIRST_NAME'; MapType: mtDirect),
  (ITable: ''; IField: 'W2 MIDDLE INITIAL'; ETable: 'CL_PERSON'; EField: 'W2_MIDDLE_NAME'; MapType: mtDirect),
  (ITable: ''; IField: 'W2 LAST NAME'; ETable: 'CL_PERSON'; EField: 'W2_LAST_NAME'; MapType: mtDirect),
  (ITable: ''; IField: 'W2 SUFFIX'; ETable: 'CL_PERSON'; EField: 'W2_NAME_SUFFIX'; MapType: mtDirect),
  (ITable: ''; IField: 'POSITION STATUS'; ETable: 'EE'; EField: 'POSITION_STATUS'; MapType: mtDirect),
  (ITable: ''; IField: 'STANDARD HOURS'; ETable: 'EE'; EField: 'STANDARD_HOURS'; MapType: mtDirect),

  (ITable: ''; IField: 'GROUP TERM'; ETable: 'EE'; EField: 'GROUP_TERM_POLICY_AMOUNT'; MapType: mtDirect),
  (ITable: ''; IField: 'W2 TYPE'; ETable: 'EE'; EField: 'W2_TYPE'; MapType: mtDirect),
  (ITable: ''; IField: 'PHONE3'; ETable: 'CL_PERSON'; EField: 'PHONE3'; MapType: mtDirect),
  (ITable: ''; IField: 'DESCRIPTION1'; ETable: 'CL_PERSON'; EField: 'DESCRIPTION1'; MapType: mtDirect),
  (ITable: ''; IField: 'DESCRIPTION2'; ETable: 'CL_PERSON'; EField: 'DESCRIPTION2'; MapType: mtDirect),
  (ITable: ''; IField: 'DESCRIPTION3'; ETable: 'CL_PERSON'; EField: 'DESCRIPTION3'; MapType: mtDirect),
  (ITable: ''; IField: 'EIN SSN'; ETable: 'CL_PERSON'; EField: 'EIN_OR_SOCIAL_SECURITY_NUMBER'; MapType: mtDirect),

  (ITable: ''; IField: 'PAY STATUS'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_CODE'; MapType: mtCustom; FuncName: 'IETermCode'),
  (ITable: ''; IField: 'HIRE DATE'; ETable: 'EE'; EField: 'CURRENT_HIRE_DATE'; MapType: mtDirect),
  (ITable: ''; IField: 'TERMINATION CODE'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_CODE'; MapType: mtCustom; FuncName: 'IETermCode'),
  (ITable: ''; IField: 'TERMINATION DATE'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_DATE'; MapType: mtDirect),
  (ITable: ''; IField: 'REHIRE DATE'; ETable: 'EE'; EField: 'CURRENT_HIRE_DATE'; MapType: mtDirect),
  (ITable: ''; IField: 'ORG HIRE DATE'; ETable: 'EE'; EField: 'ORIGINAL_HIRE_DATE'; MapType: mtDirect),
  (ITable: ''; IField: 'DIVISION'; ETable: 'EE'; EField: 'CO_DIVISION_NBR'; MapType: mtLookup; LookupTable: 'CO_DIVISION'; LookupField: 'CUSTOM_DIVISION_NUMBER'),
  (ITable: ''; IField: 'BRANCH'; ETable: 'EE'; EField: 'CO_BRANCH_NBR'; MapType: mtCustom; FuncName: 'IEBranch'),
  (ITable: ''; IField: 'DEPT.'; ETable: 'EE'; EField: 'CO_DEPARTMENT_NBR'; MapType: mtCustom; FuncName: 'IEDept'),
  (ITable: ''; IField: 'JOB'; ETable: 'EE_RATES'; EField: 'CO_JOBS_NBR'; KeyField: 'PRIMARY_RATE'; KeyValue: GROUP_BOX_YES; MapType: mtLookup; LookupTable: 'CO_JOBS'; LookupField: 'DESCRIPTION'),
//  (ITable: ''; IField: 'JOB DATE'; ETable: 'EE_RATES'; EField: 'EFFECTIVE_DATE'; KeyField: 'PRIMARY_RATE'; KeyValue: GROUP_BOX_YES; MapType: mtDirect),
  (ITable: ''; IField: 'EEO CODE'; ETable: 'EE'; EField: 'SY_HR_EEO_NBR'; MapType: mtMapLookup; MLookupTable: 'SY_HR_EEO'; MLookupField: 'DESCRIPTION';
                                   MMapValues: '"0=1.1 Exec/Sr Level Officials & Managers",' +
                                               '"1=1.2 First/Mid-Level Officials & Managers",' +
                                               '"2=2 Professional",' +
                                               '"3=3 Technicians",' +
                                               '"4=4 Sales Workers",' +
                                               '"5=5 Administrative Support Workers",' +
                                               '"6=6 Craft Workers - Skilled",' +
                                               '"7=7 Operatives - Semi-Skilled",' +
                                               '"8=8 Laborers - Unskilled",' +
                                               '"9=9 Service Workers"'),
  (ITable: ''; IField: 'POSITION TITLE'; ETable: 'EE'; EField: 'CO_HR_POSITIONS_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_POSITIONS'; LookupField: 'DESCRIPTION'),
  (ITable: ''; IField: 'WORKER COMP'; ETable: 'EE_RATES'; EField: 'CO_WORKERS_COMP_NBR'; KeyField: 'PRIMARY_RATE'; KeyValue: GROUP_BOX_YES; MapType: mtLookup; LookupTable: 'CO_WORKERS_COMP'; LookupField: 'WORKERS_COMP_CODE'),
  (ITable: ''; IField: 'UNION CODE'; ETable: 'EE'; EField: 'CO_UNIONS_NBR'; MapType: mtLookup; LookupTable: 'CO_UNIONS'; LookupField: 'CO_NBR;CL_UNION_NBR'; LookupTable1: 'CL_UNION'; LookupField1: 'NAME'),
  (ITable: ''; IField: 'CARD NO.'; ETable: 'EE'; EField: 'TIME_CLOCK_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'DATE OF BIRTH'; ETable: 'CL_PERSON'; EField: 'BIRTH_DATE'; MapType: mtDirect),
//  (ITable: ''; IField: 'MARITAL STATUS'; ETable: 'EE'; EField: 'FEDERAL_MARITAL_STATUS'; MapType: mtDirect),
  (ITable: ''; IField: 'SEX'; ETable: 'CL_PERSON'; EField: 'GENDER'; MapType: mtDirect),
  (ITable: ''; IField: 'ETHNICITY CODE'; ETable: 'CL_PERSON'; EField: 'ETHNICITY'; MapType: mtMap;
                                                        MapValues: '1=' + ETHNIC_AFRICAN + ',' +
                                                                   '2=' + ETHNIC_HISPANIC + ',' +
                                                                   '3=' + ETHNIC_ASIAN + ',' +
                                                                   '4=' + ETHNIC_INDIAN + ',' +
                                                                   '5=' + ETHNIC_CAUCASIAN + ',' +
                                                                   '6=' + ETHNIC_PACIFIC + ',' +
                                                                   '7=' + ETHNIC_MANY + ',' +
                                                                   'B=' + ETHNIC_AFRICAN + ',' +
                                                                   'H=' + ETHNIC_HISPANIC + ',' +
                                                                   'A=' + ETHNIC_ASIAN + ',' +
                                                                   'N=' + ETHNIC_INDIAN + ',' +
                                                                   'P=' + ETHNIC_PACIFIC + ',' +
                                                                   'T=' + ETHNIC_MANY + ',' +
                                                                   'C=' + ETHNIC_CAUCASIAN),
  (ITable: ''; IField: 'SMOKER'; ETable: 'CL_PERSON'; EField: 'SMOKER'; MapType: mtDirect),
  (ITable: ''; IField: 'EMP DECEASE'; ETable: 'EE'; EField: 'W2_DECEASED'; MapType: mtDirect),
  (ITable: ''; IField: 'MILITARY/VET CODE'; ETable: 'CL_PERSON'; EField: 'VIETNAM_VETERAN'; MapType: mtCustom; FuncName: 'IEVetCode'),
  (ITable: ''; IField: 'MILITARY/VET CODE'; ETable: 'CL_PERSON'; EField: 'DISABLED_VETERAN'; MapType: mtCustom; FuncName: 'IEVetCode'),
  (ITable: ''; IField: 'MILITARY/VET CODE'; ETable: 'CL_PERSON'; EField: 'MILITARY_RESERVE'; MapType: mtCustom; FuncName: 'IEVetCode'),
  (ITable: ''; IField: 'MILITARY/VET CODE'; ETable: 'CL_PERSON'; EField: 'VETERAN'; MapType: mtCustom; FuncName: 'IEVetCode'),
  (ITable: ''; IField: 'ALIEN #'; ETable: 'CL_PERSON'; EField: 'VISA_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'SEASONAL'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_CODE'; MapType: mtCustom; FuncName: 'IETermCode'),
  (ITable: ''; IField: 'STATUTORY'; ETable: 'EE'; EField: 'W2_STATUTORY_EMPLOYEE'; MapType: mtDirect),
  (ITable: ''; IField: 'PENSION CODE'; ETable: 'EE'; EField: 'W2_PENSION'; MapType: mtDirect),
  (ITable: ''; IField: 'DEF COMP'; ETable: 'EE'; EField: 'W2_DEFERRED_COMP'; MapType: mtDirect),
  (ITable: ''; IField: 'SALARY'; ETable: 'EE'; EField: 'SALARY_AMOUNT'; MapType: mtDirect),
  (ITable: ''; IField: 'HOURLY RATE 1'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '1'; MapType: mtDirect),
  (ITable: ''; IField: 'HOURLY RATE 2'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '2'; MapType: mtDirect),
  (ITable: ''; IField: 'HOURLY RATE 3'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '3'; MapType: mtDirect),
  (ITable: ''; IField: 'PAY FREQ'; ETable: 'EE'; EField: 'PAY_FREQUENCY'; MapType: mtMap;
                                                        MapValues: '52=' + FREQUENCY_TYPE_WEEKLY + ',' +
                                                                   '26=' + FREQUENCY_TYPE_BIWEEKLY + ',' +
                                                                   '24=' + FREQUENCY_TYPE_SEMI_MONTHLY + ',' +
                                                                   '12=' + FREQUENCY_TYPE_MONTHLY),
  (ITable: ''; IField: 'SHIFT DIFF'; ETable: 'EE'; EField: 'AUTOPAY_CO_SHIFTS_NBR'; MapType: mtCustom; FuncName: 'EEShift'),
  (ITable: ''; IField: 'SHIFT DIFF'; ETable: 'EE_WORK_SHIFTS'; EField: 'CO_SHIFTS_NBR'; MapType: mtCustom; FuncName: 'EEShift'),
  (ITable: ''; IField: 'GRADE POSITION'; ETable: 'EE_RATES'; EField: 'CO_HR_POSITIONS_NBR'; KeyField: 'PRIMARY_RATE'; KeyValue: GROUP_BOX_YES; MapType: mtLookup; LookupTable: 'CO_HR_POSITIONS'; LookupField: 'DESCRIPTION'),
  (ITable: ''; IField: 'GRADE'; ETable: 'EE_RATES'; EField: 'CO_HR_POSITION_GRADES_NBR'; KeyField: 'PRIMARY_RATE'; KeyValue: GROUP_BOX_YES; MapType: mtLookup; LookupTable: 'CO_HR_SALARY_GRADES'; LookupField: 'DESCRIPTION'),

  (ITable: ''; IField: 'EXEMPT CODE'; ETable: 'EE'; EField: 'FLSA_EXEMPT'; MapType: mtDirect),
  (ITable: ''; IField: 'HEALTHCARE COVERAGE'; ETable: 'EE'; EField: 'HEALTHCARE_COVERAGE'; MapType: mtDirect),
  (ITable: ''; IField: 'TIPPED'; ETable: 'EE'; EField: 'TIPPED_DIRECTLY'; MapType: mtMap;
                                                        MapValues: ' =' + GROUP_BOX_NO + ',' +
                                                                   'I=' + GROUP_BOX_YES + ',' +
                                                                   'D=' + GROUP_BOX_YES),
  (ITable: ''; IField: 'FED MARITAL'; ETable: 'EE'; EField: 'FEDERAL_MARITAL_STATUS'; MapType: mtDirect),
  (ITable: ''; IField: 'FED EXEM'; ETable: 'EE'; EField: 'NUMBER_OF_DEPENDENTS'; MapType: mtDirect),
  (ITable: ''; IField: 'ADDTL FED'; ETable: 'EE'; EField: 'OVERRIDE_FED_TAX_VALUE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'ADDTL % FED'; ETable: 'EE'; EField: 'OVERRIDE_FED_TAX_VALUE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'VOL FED'; ETable: 'EE'; EField: 'OVERRIDE_FED_TAX_VALUE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'VOL % FED'; ETable: 'EE'; EField: 'OVERRIDE_FED_TAX_VALUE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'ADDTL FED'; ETable: 'EE'; EField: 'OVERRIDE_FED_TAX_TYPE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'EIC CODE'; ETable: 'EE'; EField: 'EIC'; MapType: mtMap;
                                                        MapValues: '0=' + EIC_NONE + ',' +
                                                                   '1=' + EIC_SINGLE + ',' +
                                                                   '2=' + EIC_BOTH_SPOUSES),
  (ITable: ''; IField: 'TAX FORM'; ETable: 'EE'; EField: 'COMPANY_OR_INDIVIDUAL_NAME'; MapType: mtMap;
                                                        MapValues: 'W2=' + GROUP_BOX_INDIVIDUAL + ',' +
                                                                   '1099M=' + GROUP_BOX_COMPANY + ',' +
                                                                   '1099R=' + GROUP_BOX_COMPANY),
  (ITable: ''; IField: 'NEW HIRE REP'; ETable: 'EE'; EField: 'NEW_HIRE_REPORT_SENT'; MapType: mtDirect),
  //(ITable: ''; IField: 'STATE MARITAL'; ETable: 'EE_STATES'; EField: 'STATE_MARITAL_STATUS'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtDirect),
  (ITable: ''; IField: 'STATE MARITAL'; ETable: 'EE_STATES'; EField: 'STATE_MARITAL_STATUS'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'ValidateMaritalStatus'),
  (ITable: ''; IField: 'STATE RMS'; MapType: mtCustom; FuncName: 'RapidMaritalStatus'),
  (ITable: ''; IField: 'STATE EXEM'; ETable: 'EE_STATES'; EField: 'STATE_NUMBER_WITHHOLDING_ALLOW'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtDirect),
  (ITable: ''; IField: 'ADDTL STATE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_VALUE'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'ADDTL % STATE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_VALUE'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'VOL STATE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_VALUE'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'VOL % STATE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_VALUE'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'IETaxValue'),
  (ITable: ''; IField: 'ADDTL STATE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_TYPE'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'IETaxValue'),

  (ITable: ''; IField: 'SIT TAX CODE'; ETable: 'EE_STATES'; EField: 'CO_STATES_NBR'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'HomeTax'),
  (ITable: ''; IField: 'SUI TAX CODE'; ETable: 'EE_STATES'; EField: 'SUI_APPLY_CO_STATES_NBR'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'HomeSUI'),
  (ITable: ''; IField: 'SDI TAX CODE'; ETable: 'EE_STATES'; EField: 'SDI_APPLY_CO_STATES_NBR'; KeyField: 'CO_STATES_NBR'; KeyLookupTable: 'CO_STATES'; KeyLookupField: 'STATE'; MapType: mtCustom; FuncName: 'HomeSDI'),

  (ITable: ''; IField: 'ED TYPE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'CALCULATION_TYPE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtMap;
                                                        MapValues: ScheduledCalcMethod_ComboBoxChoices; MapDefault: CALC_METHOD_NONE),
  (ITable: ''; IField: 'ED STATUS'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'CALCULATION_TYPE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtMap;
                                                        MapValues: 'A=' + CALC_METHOD_FIXED + ',' +
                                                                   'I=' + CALC_METHOD_NONE),
  (ITable: ''; IField: 'ED START DATE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EFFECTIVE_START_DATE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED END DATE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EFFECTIVE_END_DATE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED AMOUNT'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'AMOUNT'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED PERCENT RATE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'PERCENTAGE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED METHOD'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'CALCULATION_TYPE'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED PRIORITY'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'PRIORITY_NUMBER'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED FREQ CODE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'FREQUENCY'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED ACTIVE WEEK'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EXCLUDE_WEEK_1'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEActiveWeeks'),
  (ITable: ''; IField: 'ED ACTIVE WEEK'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EXCLUDE_WEEK_2'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEActiveWeeks'),
  (ITable: ''; IField: 'ED ACTIVE WEEK'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EXCLUDE_WEEK_3'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEActiveWeeks'),
  (ITable: ''; IField: 'ED ACTIVE WEEK'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EXCLUDE_WEEK_4'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEActiveWeeks'),
  (ITable: ''; IField: 'ED ACTIVE WEEK'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'EXCLUDE_WEEK_5'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEActiveWeeks'),
  (ITable: ''; IField: 'ED TARGET'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'TARGET_AMOUNT'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtCustom; FuncName: 'IEEDTarget'),
  (ITable: ''; IField: 'ED MIN. PERIOD'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'MINIMUM_PAY_PERIOD_AMOUNT'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED MAX. PERIOD'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'MAXIMUM_PAY_PERIOD_AMOUNT'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED MAX. ANNUAL'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'ANNUAL_MAXIMUM_AMOUNT'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect),
  (ITable: ''; IField: 'ED BENEFIT REFERENCE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'CO_BENEFITS_NBR'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtLookup; LookupTable: 'CO_BENEFITS'; LookupField: 'BENEFIT_NAME'), // this field not posted in fact (see IEMap)
  (ITable: ''; IField: 'ED BENEFIT AMOUNT TYPE'; ETable: 'EE_SCHEDULED_E_DS'; EField: 'CO_BENEFIT_SUBTYPE_NBR'; KeyField: 'CL_E_DS_NBR'; KeyLookupTable: 'CL_E_DS'; KeyLookupField: 'CUSTOM_E_D_CODE_NUMBER'; MapType: mtDirect), // this field is lookup in fact (see IEMap)

  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'CONTACT_NAME'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'CONTACT_NAME'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'CONTACT_PRIORITY'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'CONTACT_PRIORITY'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'PRIMARY_PHONE'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'PRIMARY_PHONE'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'PAGER'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'PAGER'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'EMAIL_ADDRESS'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'EMAIL_ADDRESS'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'SECONDARY_PHONE'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'SECONDARY_PHONE'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'FAX'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'FAX'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),
  (ITable: 'EE_EMERGENCY_CONTACTS'; IField: 'ADDITIONAL_INFO'; ETable: 'EE_EMERGENCY_CONTACTS'; EField: 'ADDITIONAL_INFO'; KeyField: 'EE_EMERGENCY_CONTACTS_NBR'; MapType: mtDirect),

  (ITable: 'CL_HR_PERSON_SKILLS'; IField: 'SKILL'; ETable: 'CL_HR_PERSON_SKILLS'; EField: 'CL_HR_SKILLS_NBR'; KeyField: 'CL_HR_PERSON_SKILLS_NBR'; KeyLookupTable: 'CL_HR_SKILLS'; KeyLookupField: 'DESCRIPTION'; MapType: mtLookup; LookupTable: 'CL_HR_SKILLS'; LookupField: 'DESCRIPTION'),
  (ITable: 'CL_HR_PERSON_SKILLS'; IField: 'YEARS_EXPERIENCE'; ETable: 'CL_HR_PERSON_SKILLS'; EField: 'YEARS_EXPERIENCE'; KeyField: 'CL_HR_PERSON_SKILLS_NBR'; KeyLookupTable: 'CL_HR_SKILLS'; KeyLookupField: 'DESCRIPTION'; MapType: mtDirect;),
  (ITable: 'CL_HR_PERSON_SKILLS'; IField: 'NOTES'; ETable: 'CL_HR_PERSON_SKILLS'; EField: 'NOTES'; KeyField: 'CL_HR_PERSON_SKILLS_NBR'; KeyLookupTable: 'CL_HR_SKILLS'; KeyLookupField: 'DESCRIPTION'; MapType: mtDirect),


  (ITable: 'CL_HR_PERSON_HANDICAPS'; IField: 'HANDICAP'; ETable: 'CL_HR_PERSON_HANDICAPS'; EField: 'SY_HR_HANDICAPS_NBR'; KeyField: 'CL_HR_PERSON_HANDICAPS_NBR'; KeyLookupTable: 'SY_HR_HANDICAPS'; KeyLookupField: 'DESCRIPTION'; MapType: mtLookup; LookupTable: 'SY_HR_HANDICAPS'; LookupField: 'DESCRIPTION'),

  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'SCHOOL'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'CL_HR_SCHOOL_NBR'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtLookup; LookupTable: 'CL_HR_SCHOOL'; LookupField: 'NAME'),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'MAJOR'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'MAJOR'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'DEGREE_LEVEL'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'DEGREE_LEVEL'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'GPA'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'GPA'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'START_DATE'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'START_DATE'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'END_DATE'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'END_DATE'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'GRADUATION_DATE'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'GRADUATION_DATE'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'CL_HR_PERSON_EDUCATION'; IField: 'NOTES'; ETable: 'CL_HR_PERSON_EDUCATION'; EField: 'NOTES'; KeyField: 'CL_HR_PERSON_EDUCATION_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),

  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'ANATOMIC_CODE'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'SY_HR_OSHA_ANATOMIC_CODES_NBR'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtLookup; LookupTable: 'SY_HR_OSHA_ANATOMIC_CODES'; LookupField: 'DESCRIPTION'),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'WC_CODE'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'CO_WORKERS_COMP_NBR'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtLookup; LookupTable: 'CO_WORKERS_COMP'; LookupField: 'WORKER_COMP_CODE'),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'INJURY_CODE'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'SY_HR_INJURY_CODES_NBR'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtLookup; LookupTable: 'SY_HR_INJURY_CODES'; LookupField: 'DESCRIPTION'),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'CASE_NUMBER'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'CASE_NUMBER'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'CASE_STATUS'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'CASE_STATUS'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DATE_OCCURRED'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DATE_OCCURRED'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DATE_FILED'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DATE_FILED'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DAYS_AWAY'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DAYS_AWAY'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DAYS_RESTRICTED'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DAYS_RESTRICTED'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'WC_CLAIM_NUMBER'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'WC_CLAIM_NUMBER'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'WHERE_OCCURED'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'WHERE_OCCURED'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'FIRST_AID'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'FIRST_AID'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'EMERGENCY_ROOM'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'EMERGENCY_ROOM'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'OVERNIGHT_HOSPITALIZATION'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'OVERNIGHT_HOSPITALIZATION'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DEATH'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DEATH'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'DATE_OF_DEATH'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'DATE_OF_DEATH'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'EMPLOYEE_ACTIVITY'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'EMPLOYEE_ACTIVITY'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'LABOR_COST'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'LABOR_COST'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'MEDICAL_COST'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'MEDICAL_COST'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'OBJECT_DESCRIPTION'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'OBJECT_DESCRIPTION'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'PHYSICIAN_NAME'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'PHYSICIAN_NAME'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'HOSPITAL_NAME'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'HOSPITAL_NAME'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'ADDRESS1'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'ADDRESS1'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'ADDRESS2'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'ADDRESS2'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'CITY'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'CITY'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'STATE'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'STATE'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_INJURY_OCCURRENCE'; IField: 'ZIP_CODE'; ETable: 'EE_HR_INJURY_OCCURRENCE'; EField: 'ZIP_CODE'; KeyField: 'EE_HR_INJURY_OCCURRENCE_NBR'; MapType: mtDirect),

  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'SCHOOL'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'CL_HR_SCHOOL_NBR'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtLookup; LookupTable: 'CL_HR_SCHOOL'; LookupField: 'NAME'),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'COMPLETED'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'COMPLETED'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'TAXABLE'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'TAXABLE'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'COURSE'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'COURSE'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'DATE_START'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'DATE_START'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'DATE_FINISH'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'DATE_FINISH'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'COMPANY_COST'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'COMPANY_COST'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'NOTES'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'NOTES'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'CHECK_ISSUED_BY'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'CHECK_ISSUED_BY'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'CHECK_NUMBER'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'CHECK_NUMBER'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'DATE_ISSUED'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'DATE_ISSUED'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'RENEWAL_STATUS'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'RENEWAL_STATUS'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),
  (ITable: 'EE_HR_CO_PROVIDED_EDUCATN'; IField: 'LAST_REVIEW_DATE'; ETable: 'EE_HR_CO_PROVIDED_EDUCATN'; EField: 'LAST_REVIEW_DATE'; KeyField: 'EE_HR_CO_PROVIDED_EDUCATN_NBR'; KeyLookupTable: 'CL_HR_SCHOOL'; KeyLookupField: 'NAME'; MapType: mtDirect),

  (ITable: 'EE_HR_PROPERTY_TRACKING'; IField: 'PROPERTY'; ETable: 'EE_HR_PROPERTY_TRACKING'; EField: 'CO_HR_PROPERTY_NBR'; KeyField: 'EE_HR_PROPERTY_TRACKING_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_PROPERTY'; LookupField: 'PROPERTY_DESCRIPTION'),
  (ITable: 'EE_HR_PROPERTY_TRACKING'; IField: 'ISSUED_DATE'; ETable: 'EE_HR_PROPERTY_TRACKING'; EField: 'ISSUED_DATE'; KeyField: 'EE_HR_PROPERTY_TRACKING_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PROPERTY_TRACKING'; IField: 'RETURNED_DATE'; ETable: 'EE_HR_PROPERTY_TRACKING'; EField: 'RETURNED_DATE'; KeyField: 'EE_HR_PROPERTY_TRACKING_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PROPERTY_TRACKING'; IField: 'LOST_DATE'; ETable: 'EE_HR_PROPERTY_TRACKING'; EField: 'LOST_DATE'; KeyField: 'EE_HR_PROPERTY_TRACKING_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PROPERTY_TRACKING'; IField: 'RENEWAL_DATE'; ETable: 'EE_HR_PROPERTY_TRACKING'; EField: 'RENEWAL_DATE'; KeyField: 'EE_HR_PROPERTY_TRACKING_NBR'; MapType: mtDirect),

  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'RATING'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'CO_HR_PERFORMANCE_RATINGS_NBR'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_PERFORMANCE_RATINGS'; LookupField: 'DESCRIPTION'),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'REASON'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'CL_HR_REASON_CODES_NBR'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtLookup; LookupTable: 'CL_HR_REASON_CODES'; LookupField: 'NAME'),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'SUPERVISOR'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'CO_HR_SUPERVISORS_NBR'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_SUPERVISORS'; LookupField: 'SUPERVISOR_NAME'),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'INCREASE_TYPE'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'INCREASE_TYPE'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'INCREASE_AMOUNT'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'INCREASE_AMOUNT'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'INCREASE_EFFECTIVE_DATE'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'INCREASE_EFFECTIVE_DATE'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'OVERALL_RATING'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'OVERALL_RATING'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'REVIEW_DATE'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'REVIEW_DATE'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'RATE_NUMBER'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'RATE_NUMBER'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_PERFORMANCE_RATINGS'; IField: 'NOTES'; ETable: 'EE_HR_PERFORMANCE_RATINGS'; EField: 'NOTES'; KeyField: 'EE_HR_PERFORMANCE_RATINGS_NBR'; MapType: mtDirect),

  (ITable: 'EE_HR_CAR'; IField: 'LICENSE_PLATE'; ETable: 'EE_HR_CAR'; EField: 'CO_HR_CAR_NBR'; KeyField: 'EE_HR_CAR_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_CAR'; LookupField: 'LICENSE_PLATE'),

  (ITable: 'EE_HR_ATTENDANCE'; IField: 'ATTENDANCE_DESCRIPTION'; ETable: 'EE_HR_ATTENDANCE'; EField: 'CO_HR_ATTENDANCE_TYPES_NBR'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtLookup; LookupTable: 'CO_HR_ATTENDANCE_TYPES'; LookupField: 'ATTENDANCE_DESCRIPTION'),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'PERIOD_FROM'; ETable: 'EE_HR_ATTENDANCE'; EField: 'PERIOD_FROM'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'PERIOD_TO'; ETable: 'EE_HR_ATTENDANCE'; EField: 'PERIOD_TO'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'EXCUSED'; ETable: 'EE_HR_ATTENDANCE'; EField: 'EXCUSED'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'WARNING_ISSUED'; ETable: 'EE_HR_ATTENDANCE'; EField: 'WARNING_ISSUED'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'HOURS_USED'; ETable: 'EE_HR_ATTENDANCE'; EField: 'HOURS_USED'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect),
  (ITable: 'EE_HR_ATTENDANCE'; IField: 'NOTES'; ETable: 'EE_HR_ATTENDANCE'; EField: 'NOTES'; KeyField: 'EE_HR_ATTENDANCE_NBR'; MapType: mtDirect)

  );


implementation

end.

