// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvTaskResultFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  ComCtrls, Buttons, ExtCtrls, Menus, ISUtils,
  ISBasicClasses, ISErrorUtils, EvCommonInterfaces, EvStatisticsViewerFrm, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TTaskResultFrame = class(TFrame)
    PC: TevPageControl;
    tsResults: TTabSheet;
    tsExceptions: TTabSheet;
    memoExceptions: TevMemo;
    evPanel1: TevPanel;
    btnEMail: TevBitBtn;
    evBitBtn1: TevBitBtn;
    tsNotes: TTabSheet;
    memoNotes: TevMemo;
    evBitBtn2: TevBitBtn;
    zevPanel2: TevPanel;
    bWarningsEmail: TevBitBtn;
    bWarningsPrint: TevBitBtn;
    bWarningsClipboard: TevBitBtn;
    tsLog: TTabSheet;
    memoLog: TevMemo;
    tsWarnings: TTabSheet;
    memoWarnings: TevMemo;
    pWarnings: TevPanel;
    evBitBtn3: TevBitBtn;
    evBitBtn4: TevBitBtn;
    evBitBtn5: TevBitBtn;
    tsRunStatistics: TTabSheet;
    frmStatistics: TevStatisticsViewerFrm;
    procedure btnEMailClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
    procedure bWarningsEmailClick(Sender: TObject);
    procedure bWarningsPrintClick(Sender: TObject);
    procedure bWarningsClipboardClick(Sender: TObject);
    procedure evBitBtn3Click(Sender: TObject);
    procedure evBitBtn4Click(Sender: TObject);
    procedure evBitBtn5Click(Sender: TObject);
  private
    FTask:    IevTask;
  protected
    function  Task: IevTask;
    procedure PrintLines(const Lines: TStrings);
    procedure CheckExceptions;
    procedure CheckRunStatistics;
    procedure AddNoteToTask(s: string);
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); reintroduce; virtual;
    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); virtual;
  end;

  TTaskResultFrameClass = class of TTaskResultFrame;

implementation

uses EvSendMail, Printers, EvUtils, EvTypes, Clipbrd, StrUtils, EvContext, EvMainboard;

{$R *.DFM}

{ TTaskParamFrame }

constructor TTaskResultFrame.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited Create(AOwner);
  Parent := AParent;
  FTask := ATask;

  CheckRunStatistics;
  CheckExceptions;
end;


procedure TTaskResultFrame.CheckExceptions;

  procedure ParseAndAssignStrings(const Source, Dest: TStrings);
  var
    SourceIterator: Integer;
    CurrentSearchPos, NewSearchPos: Integer;
    OneSourceLine: string;
  begin
    for SourceIterator := 0 to Source.Count-1 do
    begin  
      OneSourceLine := StringReplace(Source[SourceIterator], #$D, '|', [rfReplaceAll]);
      CurrentSearchPos := 1;
      while True do
      begin
        NewSearchPos := PosEx('|', OneSourceLine, CurrentSearchPos);
        if NewSearchPos <> 0 then
        begin
          Dest.Append(Copy(OneSourceLine, CurrentSearchPos, NewSearchPos- CurrentSearchPos));
          CurrentSearchPos := NewSearchPos+ 1;
        end
        else
          Break;
      end;
      Dest.Append(Copy(OneSourceLine, CurrentSearchPos, MaxInt));
    end;
  end;
  
begin
  memoExceptions.Lines.BeginUpdate;
  try
    memoExceptions.Lines.Clear;
    memoExceptions.Lines.Text := FTask.Exceptions;
    //ParseAndAssignStrings(TBasicQueueTask(FTask).Exceptions, memoExceptions.Lines); // this overcomes performance problem in prev line
    tsExceptions.TabVisible := memoExceptions.Lines.Count > 0;
    memoExceptions.SelLength := 0;
    memoExceptions.SelStart := 0;
    memoExceptions.Perform(EM_SCROLLCARET, 0, 0);
  finally
    memoExceptions.Lines.EndUpdate;
  end;
  if tsExceptions.TabVisible then
    PC.ActivePage := tsExceptions
  else
    PC.ActivePage := tsResults;
  memoNotes.Lines.Text := FTask.Notes;
  tsNotes.TabVisible := memoNotes.Lines.Count > 0;
  memoNotes.SelLength := 0;
  memoNotes.SelStart := 0;
  memoNotes.Perform(EM_SCROLLCARET, 0, 0);
  //memoLog.Lines.Assign(TBasicQueueTask(FTask).ExecutionLog);
  memoLog.Lines.Text := FTask.Log;
  tsLog.TabVisible := memoLog.Lines.Count > 0;
  memoLog.SelLength := 0;
  memoLog.SelStart := 0;
  memoLog.Perform(EM_SCROLLCARET, 0, 0);
  memoWarnings.Lines.Text := FTask.Warnings;
  tsWarnings.TabVisible := memoWarnings.Lines.Count > 0;
  memoWarnings.SelLength := 0;
  memoWarnings.SelStart := 0;
  memoWarnings.Perform(EM_SCROLLCARET, 0, 0);
end;

procedure TTaskResultFrame.btnEMailClick(Sender: TObject);
begin
  MailTo(nil, nil, nil, nil, FTask.Caption, memoExceptions.Lines.Text);
end;

procedure TTaskResultFrame.evBitBtn1Click(Sender: TObject);
begin
  PrintLines(memoExceptions.Lines);
end;

procedure TTaskResultFrame.AddNoteToTask(s: string);
begin
  s := Context.UserAccount.User + ' '+ DateTimeToStr(Now)+ ' : '+ s;
  mb_TaskQueue.ModifyTaskProperty(FTask.ID, 'Notes', FTask.Notes + #13 + s);
  FTask.AddNotes(s);
  memoNotes.Lines.Append(s);
  tsNotes.TabVisible := True;
end;

procedure TTaskResultFrame.evBitBtn2Click(Sender: TObject);
begin
  ClipBoard.SetTextBuf(PChar(memoExceptions.Lines.Text));
end;

procedure TTaskResultFrame.bWarningsEmailClick(Sender: TObject);
begin
  MailTo(nil, nil, nil, nil, FTask.Caption, memoNotes.Lines.Text);
end;

procedure TTaskResultFrame.bWarningsPrintClick(Sender: TObject);
begin
  PrintLines(memoNotes.Lines);
end;

procedure TTaskResultFrame.bWarningsClipboardClick(Sender: TObject);
begin
  ClipBoard.SetTextBuf(PChar(memoNotes.Lines.Text));
end;

procedure TTaskResultFrame.evBitBtn3Click(Sender: TObject);
begin
  MailTo(nil, nil, nil, nil, FTask.Caption, memoWarnings.Lines.Text);
end;

procedure TTaskResultFrame.evBitBtn4Click(Sender: TObject);
begin
  PrintLines(memoWarnings.Lines);
end;

procedure TTaskResultFrame.evBitBtn5Click(Sender: TObject);
begin
  ClipBoard.SetTextBuf(PChar(memoWarnings.Lines.Text));
end;

procedure TTaskResultFrame.OnShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  Handled := False;
end;

procedure TTaskResultFrame.PrintLines(const Lines: TStrings);
var
  RichEdit: TevRichEdit;
begin
  RichEdit := TevRichEdit.Create(nil);
  try
    RichEdit.Visible := False;
    RichEdit.Parent := Parent;
    ctx_StartWait('Preparing for printing...');
    RichEdit.Lines.BeginUpdate;
    try
      RichEdit.Lines.Assign(Lines);
      RichEdit.Lines.Insert(0, '');
      RichEdit.Lines.Insert(0, 'Printed on '+ DateTimeToStr(Now));
      RichEdit.Lines.Insert(0, FTask.ProgressText);
      RichEdit.Lines.Insert(0, FTask.Caption);
      RichEdit.Lines.Insert(0, FTask.TaskType);
    finally
      RichEdit.Lines.EndUpdate;
      ctx_EndWait;
    end;
    RichEdit.Print(FTask.Caption);
  finally
    RichEdit.Free;
  end;
end;

function TTaskResultFrame.Task: IevTask;
begin
  Result := FTask;
end;

procedure TTaskResultFrame.CheckRunStatistics;
begin
  tsRunStatistics.TabVisible := Assigned(FTask.RunStatistics);
  frmStatistics.InitForEvent(FTask.RunStatistics);
end;

end.

