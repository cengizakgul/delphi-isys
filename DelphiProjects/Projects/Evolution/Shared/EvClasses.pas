unit EvClasses;

interface

uses Windows, SysUtils, Variants, isBaseClasses, EvStreamUtils, EvCommonInterfaces, TypInfo, OnGuard, OgUtil,
     isBasicUtils, SEncryptionRoutines, isLogFile, isSocket, evTypes, EvConsts, Printers, DateUtils,
     sSecurityClassDefs, isErrorUtils, EvBasicUtils, EvEventNotifier;

type
  TevUserAccount = class(TisInterfacedObject, IevUserAccount)
  protected
    FDomain:             String;
    FUserName:           String;
    FPassword:           String;
    FInternalNbr:        Integer;
    FAccountType:        TevUserAccountType;
    FAccountID:          String;
    FFirstName:          String;
    FLastName:           String;
    FEMail:              String;
    FSBAccountantNbr:    Integer;
    FPasswordChangeDate: TDateTime;

    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  AccountID: String;
    function  InternalNbr: Integer;
    function  Domain: String;
    function  User: String;
    function  Password: String;
    function  AccountType: TevUserAccountType;
    function  FirstName: String;
    function  LastName: String;
    function  EMail: String;
    function  SBAccountantNbr: Integer;
    function  PasswordChangeDate: TDateTime;
  public
    constructor Create(const ADomain, AUser, APassword: String); reintroduce;
    class function GetTypeID: String; override;
  end;



  TevSecAccountRights = class(TisCollection, IevSecAccountRights)
  protected
    FLevel: TevSecurityLevel;
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure ReadChildrenFromStream(const AStream: IEvDualStream); override;
    function  GetElementsByType(const AElementType: Char): IevSecElements;
    function  GetElementStateInfo(const AElementType: Char; const AElementName: String): IevSecStateInfo;
    function  AccountID: String;
    function  Level: TevSecurityLevel; virtual;
    function  Menus: IevSecElements; virtual;
    function  Screens: IevSecElements; virtual;
    function  Functions: IevSecElements; virtual;
    function  Clients: IevSecClients; virtual;
    function  Fields: IevSecFields; virtual;
    function  Records: IevSecRecords; virtual;
    function  GetElementState(const AElementType: Char; const AElementName: String): TevSecElementState;
    procedure SetElementState(const AElementType: Char; const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
  public
    class function GetTypeID: String; override;
    constructor Create(const AAccountID: String); reintroduce;
  end;


  TevSecElements = class(TisNamedObject, IevSecElements)
  private
    FList: IisStringList;
    FModified: Boolean;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  AddElement(const AElementName: String): Integer; virtual;
    procedure SetStateByIndex(const AIndex: Integer; const AState: TevSecElementState; const AFromGroup: Boolean);
    function  AccountID: String;
    function  Count: Integer;
    procedure ClearModified;
    function  Modified: Boolean;
    procedure Clear;
    procedure BeginChange;
    procedure EndChange;
    function  IndexOf(const AElementName: String): Integer;
    function  GetElementByIndex(const AIndex: Integer): String;
    function  GetStateInfo(const AIndex: Integer): IevSecStateInfo; overload;
    function  GetStateInfo(const AElementName: String): IevSecStateInfo; overload;
    function  GetState(const AElementName: String): TevSecElementState; virtual;
    procedure SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean); virtual;
  public
    class function GetTypeID: String; override;
  end;


  TevSecStateInfo = class(TisInterfacedObject, IevSecStateInfo)
  private
    FState: TevSecElementState;
    FFromGroup: Boolean;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetState: TevSecElementState;
    function  GetFromGroup: Boolean;
    procedure SetState(const AValue: TevSecElementState);
    procedure SetFromGroup(const AValue: Boolean);
  end;


  TevSecFieldInfo = class(TevSecStateInfo, IevSecFieldInfo)
  private
    FFieldType: String;
    FFieldSize: Integer;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetFieldType: String;
    function  GetFieldSize: Integer;
    procedure SetFieldType(const AValue: String);
    procedure SetFieldSize(const AValue: Integer);
  end;


  TevSecFields = class(TevSecElements, IevSecFields)
  protected
    procedure DoOnConstruction; override;
    function  AddElement(const AElementName: String): Integer; override;
    function  GetState(const AElementName: String): TevSecElementState; override;
    procedure SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean); override;
    function  GetSecFieldInfo(const ATableName, AFieldName: String): IevSecFieldInfo; overload;
    function  GetSecFieldInfo(const AIndex: Integer): IevSecFieldInfo; overload;
  public
    class function GetTypeID: String; override;
  end;


  TevSecClients = class(TevSecElements, IevSecClients)
  protected
    procedure DoOnConstruction; override;
    function  IsClientEnabled(const AClientID: Integer): Boolean;
  public
    class function GetTypeID: String; override;
  end;


  TevSecRecordInfo = class(TevSecStateInfo, IevSecRecordInfo)
  private
    FDBType: TevDBType;
    FClientID: Integer;
    FFilterType: String;
    FFilter: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetDBType: TevDBType;
    function  GetClientID: Integer;
    function  GetFilterType: String;
    function  GetFilter: String;
    procedure SetDBType(const AValue: TevDBType);
    procedure SetClientID(const AValue: Integer);
    procedure SetFilterType(const AValue: String);
    procedure SetFilter(const AValue: String);
  end;


  TevSecRecords = class(TevSecElements, IevSecRecords)
  private
    function  IsRecEnabled(const AClientID: Integer; const AType: String; const ANbr: Integer): Boolean;
  protected
    procedure DoOnConstruction; override;
    function  AddElement(const AElementName: String): Integer; override;
    function  GetState(const AElementName: String): TevSecElementState; override;
    procedure SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean); override;
    function  GetSecRecordInfo(const AIndex: Integer): IevSecRecordInfo;
    function  IsCompanyEnabled(const AClientID: Integer; const ACompanyNbr: Integer): Boolean;
    function  IsDivisionEnabled(const AClientID: Integer; const ADivisionNbr: Integer): Boolean;
    function  IsBranchEnabled(const AClientID: Integer; const ABranchNbr: Integer): Boolean;
    function  IsDepartmentEnabled(const AClientID: Integer; const ADepartmentNbr: Integer): Boolean;
    function  IsTeamEnabled(const AClientID: Integer; const ATeamNbr: Integer): Boolean;
  public
    class function GetTypeID: String; override;
  end;


  TevSecurityStructure = class(TisInterfacedObject, IevSecurityStructure)
  private
    // Legacy stuff. Some day we need to redo this
    FObj: TSecurityAtomCollection;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetObj: TSecurityAtomCollection;
    procedure SetObj(const AValue: TSecurityAtomCollection);
    function  Rebuild: IevDualStream; virtual;
  public
    class function GetTypeID: String; override;
    constructor Create(const ASecStructure: IevDualStream); reintroduce;
    destructor  Destroy; override;
  end;


  TevMachineInfo = class(TisInterfacedObject, IevMachineInfo)
  private
    FID: Cardinal;
    FComputerName: String;
    FIPaddress: String;
    FCipherKey: String;
    FPrinters:  IevPrintersList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  Name: String;
    function  ID: Cardinal;
    function  IPaddress: String;
    function  CipherKey: String;
    function  Printers:  IevPrintersList;
  public
    class function GetTypeID: String; override;
  end;


  TevTaskInfo = class(TisNamedObject, IevTaskInfo)
  private
    FTaskType: String;
    FNbr: Integer;
    FUser: String;
    FDomain: String;
    FPriority: Integer;
    FNotificationEmail: string;
    FSendEmailNotification: Boolean;
    FEmailSendRule: TevEmailSendRule;
    FState: TevTaskState;
    FLastUpdate: TDateTime;
    FCaption: String;
    FProgressText: string;
    FSeenByUser: Boolean;
    FDaysToStayInQueue: Integer;
    FFirstRunAt: TDateTime;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure AssignFromTask(const ATask: IevTask);

    function  GetID: TisGUID;
    function  GetNbr: Integer;
    function  GetTaskType: String;
    function  GetUser: String;
    function  GetDomain: String;
    function  GetPriority: Integer; virtual;
    function  GetNotificationEmail: string; virtual;
    function  GetSendEmailNotification: Boolean; virtual;
    function  GetEmailSendRule: TevEmailSendRule;
    function  GetLastUpdate: TDateTime; virtual;
    function  GetState: TevTaskState; virtual;
    function  GetCaption: String; virtual;
    function  GetProgressText: string; virtual;
    function  GetSeenByUser: Boolean; virtual;
    function  GetDaysToStayInQueue: Integer;
    function  GetFirstRunAt: TDateTime; virtual;
  public
    class function GetTypeID: String; override;
    constructor CreateFromTask(const ATask: IevTask); virtual;
    constructor CreateFromTaskFile(const AFileName: String); virtual;
  end;


  TevTaskRequestInfo = class(TisNamedObject, IevTaskRequestInfo)
  private
    FProgressText: String;
    FStatus: String;
    FStartedAt: TDateTime;
    FFinishedAt: TDateTime;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure AssignFromRequest(const ATaskRequest: IevTaskRequest);
    function  GetProgressText: String;
    function  GetStatus: String;
    function  GetStartedAt: TDateTime;
    function  GetFinishedAt: TDateTime;
  public
    class function GetTypeID: String; override;
    constructor CreateFromRequest(const ATaskRequest: IevTaskRequest); virtual;
  end;


  TevGlobalFlagInfo = class(TisNamedObject, IevGlobalFlagInfo)
  private
    FFlagType: String;
    FTag: String;
    FDomain: String;
    FUserName: String;
    FContextID: TisGUID;
    FLockTime: TDateTime;
    FUsageCount: Integer;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoOnConstruction; override;
    function  GetFlagName: String;
    function  GetFlagType: String;
    function  GetTag: String;
    function  GetDomain: String;
    function  GetUserName: String;
    function  GetContextID: TisGUID;
    function  GetLockTime: TDateTime;
    function  IncUsage: Integer;
    function  DecUsage: Integer;
  public
    class function GetTypeID: String; override;
    constructor Create(const AType: String; const ATag: String); reintroduce;
    constructor CreateLocked(const AType: String; const ATag: String; const Reserved: Boolean = False);  // C++ warning
  end;


  TevTaxPaymentsTaskParam = class(TisInterfacedObject, IevTaxPaymentsTaskParam)
  private
    FClNbr: Integer;
    FCustomClientNumber: string;    
    FFedTaxLiabilityList: string;
    FStateTaxLiabilityList: string;
    FSUITaxLiabilityList: string;
    FLocalTaxLiabilityList: string;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetClientID: Integer;
    procedure SetClientID(const AValue: Integer);
    function  GetCustomClientNumber: string;
    procedure SetCustomClientNumber(const AValue: String);
    function  GetFedTaxLiabilityList: string;
    procedure SetFedTaxLiabilityList(const AValue: string);
    function  GetStateTaxLiabilityList: string;
    procedure SetStateTaxLiabilityList(const AValue: string);
    function  GetSUITaxLiabilityList: string;
    procedure SetSUITaxLiabilityList(const AValue: string);
    function  GetLocalTaxLiabilityList: string;
    procedure SetLocalTaxLiabilityList(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


  TevTaxPaymentsTaskParams = class(TisCollection, IevTaxPaymentsTaskParams)
  protected
    function GetItem(AIndex: Integer): IevTaxPaymentsTaskParam;
    function Add: IevTaxPaymentsTaskParam;
    function Count: Integer;    
  public
    class function GetTypeID: String; override;
  end;


  TevACHOptions = class(TisInterfacedObject, IevACHOptions)
  private
    FBalanceBatches: Boolean;
    FBlockDebits: Boolean;
    FCombineTrans: Integer;
    FUseSBEIN: Boolean;
    FCTS: Boolean;
    FSunTrust: Boolean;
    FBlockSBCredit: Boolean;
    FFedReserve: Boolean;
    FIntercept: Boolean;
    FPayrollHeader: Boolean;
    FDescReversal: Boolean;
    FBankOne: Boolean;
    FEffectiveDate: Boolean;
    FDisplayCoNbrAndName: Boolean;
    FCombineThreshold: Currency;
    FCoId: string;
    FSBEINOverride: string;
    FHeader: string;
    FFooter: string;
    FPostProcessReportName: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  Revision: TisRevision; override;
    function  GetDisplayCoNbrAndName: Boolean;
    procedure SetDisplayCoNbrAndName(const AValue: Boolean);
    function  GetBalanceBatches: Boolean;
    procedure SetBalanceBatches(const AValue: Boolean);
    function  GetBlockDebits: Boolean;
    procedure SetBlockDebits(const AValue: Boolean);
    function  GetCombineTrans: Integer;
    procedure SetCombineTrans(const AValue: Integer);
    function  GetUseSBEIN: Boolean;
    procedure SetUseSBEIN(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetSunTrust: Boolean;
    procedure SetSunTrust(const AValue: Boolean);
    function  GetBlockSBCredit: Boolean;
    procedure SetBlockSBCredit(const AValue: Boolean);
    function  GetFedReserve: Boolean;
    procedure SetFedReserve(const AValue: Boolean);
    function  GetIntercept: Boolean;
    procedure SetIntercept(const AValue: Boolean);
    function  GetPayrollHeader: Boolean;
    procedure SetPayrollHeader(const AValue: Boolean);
    function  GetDescReversal: Boolean;
    procedure SetDescReversal(const AValue: Boolean);
    function  GetBankOne: Boolean;
    procedure SetBankOne(const AValue: Boolean);
    function  GetEffectiveDate: Boolean;
    procedure SetEffectiveDate(const AValue: Boolean);
    function  GetCombineThreshold: Currency;
    procedure SetCombineThreshold(const AValue: Currency);
    function  GetCoId: String;
    procedure SetCoId(const AValue: String);
    function  GetSBEINOverride: String;
    procedure SetSBEINOverride(const AValue: String);
    function  GetHeader: String;
    procedure SetHeader(const AValue: String);
    function  GetFooter: String;
    procedure SetFooter(const AValue: String);
    function  GetPostProcessReportName: String;
    procedure SetPostProcessReportName(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;


  TevPreprocessQuarterEndTaskFilterItem = class(TisInterfacedObject, IevPreprocessQuarterEndTaskFilterItem)
  private
    FConsolidation: Boolean;
    FClNbr: Integer;
    FCoNbr: Integer;
    FCaption: string;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetCaption: string;
    function  GetClNbr: Integer;
    function  GetCoNbr: Integer;
    function  GetConsolidation: Boolean;
    procedure SetCaption(const AValue: string);
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoNbr(const AValue: Integer);
    procedure SetConsolidation(const AValue: Boolean);
  public
    class function GetTypeID: String; override;
  end;


  TevPreprocessQuarterEndTaskFilter = class(TisCollection, IevPreprocessQuarterEndTaskFilter)
  protected
    function  GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskFilterItem;
    function  Add: IevPreprocessQuarterEndTaskFilterItem;
    procedure Delete(const AItem: IevPreprocessQuarterEndTaskFilterItem);
    procedure Clear;
    function  Count: Integer;
  public
    class function GetTypeID: String; override;
  end;


  TevPreprocessQuarterEndTaskResultItem = class(TisInterfacedObject, IevPreprocessQuarterEndTaskResultItem)
  private
    FConsolidation: Boolean;
    FCoCustomNumber: string;
    FCoName: string;
    FTimeStampStarted: TDateTime;
    FTimeStampFinished: TDateTime;
    FErrorMessage: string;
    FTaxAdjReports: IEvDualStream;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure SetCoCustomNumber(const AValue: string);
    procedure SetCoName(const AValue: string);
    procedure SetConsolidation(const AValue: Boolean);
    procedure SetTimeStampStarted(const AValue: TDateTime);
    procedure SetTimeStampFinished(const AValue: TDateTime);
    procedure SetErrorMessage(const AValue: string);
    procedure SetTaxAdjReports(const AValue: IEvDualStream);
    function  GetCoCustomNumber: string;
    function  GetCoName: string;
    function  GetConsolidation: Boolean;
    function  GetTimeStampStarted: TDateTime;
    function  GetTimeStampFinished: TDateTime;
    function  GetErrorMessage: string;
    function  GetTaxAdjReports: IEvDualStream;
  public
    class function GetTypeID: String; override;
  end;


  TevPreprocessQuarterEndTaskResult = class(TisCollection, IevPreprocessQuarterEndTaskResult)
  protected
    function GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskResultItem;
    function Add: IevPreprocessQuarterEndTaskResultItem;
    function Count: Integer;
  public
    class function GetTypeID: String; override;
  end;


  TevQECTaskFilterItem = class(TisInterfacedObject, IevQECTaskFilterItem)
  private
    FClNbr: Integer;
    FCoNbr: Integer;
    FCaption: string;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetCaption: string;
    function  GetClNbr: Integer;
    function  GetCoNbr: Integer;
    procedure SetCaption(const AValue: string);
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoNbr(const AValue: Integer);
  public
    class function GetTypeID: String; override;
  end;


  TevQECTaskFilter = class(TisCollection, IevQECTaskFilter)
  protected
    function GetItem(AIndex: Integer): IevQECTaskFilterItem;
    function Add: IevQECTaskFilterItem;
    function Count: Integer;
  public
    class function GetTypeID: String; override;
  end;


  TevQECTaskResultItem = class(TisInterfacedObject, IevQECTaskResultItem)
  private
    FQECReports: IEvDualStream;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure SetQECReports(const AValue: IEvDualStream);
    function  GetQECReports: IEvDualStream;
  public
    class function GetTypeID: String; override;
  end;

  
  TevQECTaskResult = class(TisCollection, IevQECTaskResult)
  protected
    function GetItem(AIndex: Integer): IevQECTaskResultItem;
    function Add: IevQECTaskResultItem;
    function Count: Integer;
  public
    class function GetTypeID: String; override;
  end;

  TevACATaskCompanyFilterItem = class(TisInterfacedObject, IevACATaskCompanyFilterItem)
  private
    FClNbr: Integer;
    FCoFilter: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetClNbr: Integer;
    function  GetCoFilter: String;
    procedure SetClNbr(const AValue: Integer);
    procedure SetCoFilter(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;

  TevACATaskCompanyFilter = class(TisCollection, IevACATaskCompanyFilter)
  protected
    function  GetItem(AIndex: Integer): IevACATaskCompanyFilterItem;
    function  Add: IevACATaskCompanyFilterItem;
    procedure Delete(const AItem: IevACATaskCompanyFilterItem);
    procedure Clear;
    function  Count: Integer;
  public
    class function GetTypeID: String; override;
  end;

  TevACATaskEEItem = class(TisInterfacedObject, IevACATaskEEItem)
  private
   FCO_NBR: Integer;
   FEE_NBR: Integer;
   FCUSTOM_EMPLOYEE_NUMBER: string;
   FFIRST_NAME: string;
   FLAST_NAME: string;
   FOLD_ACA_STATUS: string;
   FNEW_ACA_STATUS: string;
   FUpdateType: string;

  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetCO_NBR: Integer;
    function  GetEE_NBR: Integer;
    function  GetCUSTOM_EMPLOYEE_NUMBER: string;
    function  GetFIRST_NAME: string;
    function  GetLAST_NAME: string;
    function  GetOLD_ACA_STATUS: string;
    function  GetNEW_ACA_STATUS: string;
    function  GetUpdateType: string;

    procedure SetCO_NBR(const AValue: Integer);
    procedure SetEE_NBR(const AValue: Integer);
    procedure SetCUSTOM_EMPLOYEE_NUMBER(const AValue: string);
    procedure SetFIRST_NAME(const AValue: string);
    procedure SetLAST_NAME(const AValue: string);
    procedure SetOLD_ACA_STATUS(const AValue: string);
    procedure SetNEW_ACA_STATUS(const AValue: string);
    procedure SetUpdateType(const AValue: string);

  public
    class function GetTypeID: String; override;
  end;

  TevACATaskCompanyEE = class(TisCollection, IevACATaskCompanyEE)
  Private
    FUpdateDate: TDateTime;
    FClNbr: Integer;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetItem(AIndex: Integer): IevACATaskEEItem;
    function  Add: IevACATaskEEItem;
    procedure Delete(const AItem: IevACATaskEEItem);
    procedure Clear;
    function  Count: Integer;

    function  GetUpdateDate: TDateTime;
    function  GetClNbr: Integer;
    procedure SetUpdateDate(const AValue: TDateTime);
    procedure SetClNbr(const AValue: Integer);
  public
    class function GetTypeID: String; override;
  end;

  TevACATaskResult = class(TisCollection, IevACATaskResult)
  protected
    function  GetItem(AIndex: Integer): IevACATaskCompanyEE;
    function  Add: IevACATaskCompanyEE;
    procedure Delete(const AItem: IevACATaskCompanyEE);
    procedure Clear;
    function  Count: Integer;
  public
    class function GetTypeID: String; override;
  end;


  TevLogFile = class(TLogFile, IevLogFile)
  private
    FEmailNotifier: IEvEventNotifier;
  protected
    procedure AddContextEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String);
    function  GetEMailNotifierActive: Boolean;
    procedure SetEMailNotifierActive(const AValue: Boolean);
  public
    destructor Destroy; override;
  end;


  IevProgressInfo = interface
  ['{153F5BAF-0E7C-419B-BBDC-E17322C4467B}']
    function  GetProgressCurr: Integer;
    function  GetProgressMax: Integer;
    function  GetProgressText: String;
    procedure SetProgressCurr(const AValue: Integer);
    procedure SetProgressMax(const AValue: Integer);
    procedure SetProgressText(const AValue: String);
    property  ProgressMax: Integer read GetProgressMax write SetProgressMax;
    property  ProgressCurr: Integer read GetProgressCurr write SetProgressCurr;
    property  ProgressText: String read GetProgressText write SetProgressText;
  end;

  TevProgressInfo = class(TisInterfacedObject, IevProgressInfo)
  private
    FProgressMax: Integer;
    FProgressCurr: Integer;
    FProgressText: String;
    function  GetProgressCurr: Integer;
    function  GetProgressMax: Integer;
    function  GetProgressText: String;
    procedure SetProgressCurr(const AValue: Integer);
    procedure SetProgressMax(const AValue: Integer);
    procedure SetProgressText(const AValue: String);
  end;


  TevPrinterInfo = class(TisNamedObject, IevPrinterInfo)
  private
    FStorageID: String;
    FVOffset: Integer;
    FHOffset: Integer;
    function  GetVOffset: Integer;
    procedure SetVOffset(const AValue: Integer);
    function  GetHOffset: Integer;
    procedure SetHOffset(const AValue: Integer);
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    class function GetTypeID: String; override;
  end;


  TevPrintersList = class(TisCollection, IevPrintersList)
  private
    function  Count: Integer;
    function  GetItem(Index: Integer): IevPrinterInfo;
    function  FindPrinter(const AName: String): IevPrinterInfo;
    procedure Load;
    procedure Save;
  public
    class function GetTypeID: String; override;
  end;


  TevVMRPrinterBinInfo = class(TisNamedObject, IevVMRPrinterBinInfo)
  private
    FSbPaperNbr: Integer;
    FMediaType: string;
    FPaperWeight: Double;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetSbPaperNbr: Integer;
    procedure SetSbPaperNbr(const AValue: Integer);
    function  GetMediaType: string;
    procedure SetMediaType(const AValue: string);
    function  GetPaperWeight: Double;
    procedure SetPaperWeight(const AValue: Double);
  public
    class function GetTypeID: String; override;
  end;


  TevVMRPrinterInfo = class(TisCollection, IevVMRPrinterInfo)
  private
    FTag: String;
    FLocation: String;
    FActive: Boolean;
    FVOffset: Integer;
    FHOffset: Integer;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetTag: String;
    procedure SetTag(const AValue: String);
    function  GetLocation: String;
    procedure SetLocation(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetVOffset: Integer;
    procedure SetVOffset(const AValue: Integer);
    function  GetHOffset: Integer;
    procedure SetHOffset(const AValue: Integer);
    function  BinCount: Integer;
    function  FindBin(const AName: String): IevVMRPrinterBinInfo;
    function  FindBinByMediaType(const AMediaType: string): IevVMRPrinterBinInfo;
    function  FindBinNameByMediaType(const AMediaType: string): String;
    function  GetBin(Index: Integer): IevVMRPrinterBinInfo;
    function  AddBin: IevVMRPrinterBinInfo;
    procedure DeleteBin(const ABin: IevVMRPrinterBinInfo);
  public
    class function GetTypeID: String; override;
  end;


  TevVMRPrintersList = class(TisCollection, IevVMRPrintersList)
  protected
    function  Count: Integer;
    function  GetItem(Index: Integer): IevVMRPrinterInfo;
    function  FindPrinter(const AName: String): IevVMRPrinterInfo;
    function  Add: IevVMRPrinterInfo;
    procedure Delete(const AItem: IevVMRPrinterInfo);
  public
    class function GetTypeID: String; override;
  end;


  TevMailMessage = class(TisInterfacedObject, IevMailMessage)
  private
    FAddressFrom: String;
    FAddressTo: String;
    FAddressCC: String;
    FAddressBCC: String;
    FPriority: TisMailPriority;
    FConfirmation: Boolean;
    FSubject: String;
    FBody: String;
    FAttachments: IisListOfValues;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetAddressFrom: String;
    procedure SetAddressFrom(const AValue: String);
    function  GetAddressTo: String;
    procedure SetAddressTo(const AValue: String);
    function  GetAddressCC: String;
    procedure SetAddressCC(const AValue: String);
    function  GetAddressBCC: String;
    procedure SetAddressBCC(const AValue: String);
    function  GetSubject: String;
    procedure SetSubject(const AValue: String);
    function  GetPriority: TisMailPriority;
    procedure SetPriority(const AValue: TisMailPriority);
    function  GetConfirmation: Boolean;
    procedure SetConfirmation(const AValue: Boolean);
    function  GetBody: String;
    procedure SetBody(const AValue: String);
    procedure AttachFile(const AFileName: String);
    procedure AttachStream(const AName: String; const AStream: IisStream);
    function  Attachments: IisListOfValues;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBInfo = class(TisInterfacedObject, IevADRDBInfo)
  private
    FDatabase: String;
    FVersion: String;
    FLastTransactionNbr: Integer;
    FStatus: TevADRDBStatus;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure SetDatabase(const AValue: String);
    function  GetDatabase: String;
    procedure SetVersion(const AValue: String);
    function  GetVersion: String;
    procedure SetLastTransactionNbr(const AValue: Integer);
    function  GetLastTransactionNbr: Integer;
    procedure SetStatus(const AValue: TevADRDBStatus);
    function  GetStatus: TevADRDBStatus;
  public
    class function GetTypeID: String; override;
    constructor Create(const ADatabase: String); reintroduce;
  end;


  TevDataChangeItem = class(TisInterfacedObject, IevDataChangeItem)
  private
    FChangeType: TevDataChangeType;
    FDataSource: String;
    FKey: Integer;
    FFields: IisListOfValues;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  ChangeType: TevDataChangeType;
    function  DataSource: String;
    function  GetKey: Integer;
    procedure SetKey(const AValue: Integer);
    function  Fields: IisListOfValues;
  public
    class function GetTypeID: String; override;
    constructor Create(const AChangeType: TevDataChangeType; const ADataSource: String); reintroduce;
  end;


  TevDataChangePacket = class(TisCollection, IevDataChangePacket)
  private
    FAsOfDate: TDateTime;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  Revision: TisRevision; override;

    procedure SetAsOfDate(const AValue: TDateTime);
    function  GetAsOfDate: TDateTime;
    function  NewInsertRecord(const ADataSource: String): IevDataChangeItem;
    function  NewUpdateFields(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
    function  NewDeleteRecord(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
    procedure MergePacket(const APacket: IevDataChangePacket);
    procedure RemoveItem(const AItem: IevDataChangeItem);
    function  GetItem(Index: Integer): IevDataChangeItem;
    function  Count: Integer;
  public
    class function GetTypeID: String; override;
  end;


implementation

uses EvContext, EvMainboard;


{ TevUserAccount }

constructor TevUserAccount.Create(const ADomain, AUser, APassword: String);
begin
  inherited Create;
  FDomain := ADomain;
  FUserName := AUser;
  FPassword := APassword;
end;

function TevUserAccount.AccountType: TevUserAccountType;
begin
  Result := FAccountType;
end;

function TevUserAccount.Domain: String;
begin
  Result := FDomain;
end;

function TevUserAccount.AccountID: String;
begin
  Result := FAccountID;
end;

function TevUserAccount.Password: String;
begin
  Result := FPassword;
end;

function TevUserAccount.User: String;
begin
  Result := FUserName;
end;

function TevUserAccount.InternalNbr: Integer;
begin
  Result := FInternalNbr;
end;

function TevUserAccount.EMail: String;
begin
  Result := FEMail;
end;

function TevUserAccount.FirstName: String;
begin
  Result := FFirstName;
end;

function TevUserAccount.LastName: String;
begin
  Result := FLastName;
end;

function TevUserAccount.PasswordChangeDate: TDateTime;
begin
  Result := FPasswordChangeDate;
end;

function TevUserAccount.SBAccountantNbr: Integer;
begin
  Result := FSBAccountantNbr;
end;


procedure TevUserAccount.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FDomain := AStream.ReadShortString;
  FUserName := AStream.ReadShortString;
  FPassword := AStream.ReadShortString;
  FInternalNbr := AStream.ReadInteger;
  FAccountType := TevUserAccountType(AStream.ReadByte);
  FAccountID := AStream.ReadShortString;
  FFirstName := AStream.ReadShortString;
  FLastName := AStream.ReadShortString;
  FEMail := AStream.ReadShortString;
  FSBAccountantNbr := AStream.ReadInteger;
  FPasswordChangeDate := AStream.ReadDouble;
end;

procedure TevUserAccount.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FDomain);
  AStream.WriteShortString(FUserName);
  AStream.WriteShortString(FPassword);
  AStream.WriteInteger(FInternalNbr);
  AStream.WriteByte(Ord(FAccountType));
  AStream.WriteShortString(FAccountID);
  AStream.WriteShortString(FFirstName);
  AStream.WriteShortString(FLastName);
  AStream.WriteShortString(FEMail);
  AStream.WriteInteger(FSBAccountantNbr);
  AStream.WriteDouble(FPasswordChangeDate);
end;

class function TevUserAccount.GetTypeID: String;
begin
  Result := 'UserAccount';
end;

{ TevTaskInfo }

function TevTaskInfo.GetCaption: String;
begin
  Result := FCaption;
end;

constructor TevTaskInfo.CreateFromTask(const ATask: IevTask);
begin
  Create;
  AssignFromTask(ATask);
end;

constructor TevTaskInfo.CreateFromTaskFile(const AFileName: String);
var
  S: IevDualStream;
begin
  Create;
  S := TEvDualStreamHolder.CreateFromFile(AFileName);
  S.ReadByte;  // skip Revision info
  ReadFromStream(S);
end;

function TevTaskInfo.GetDomain: String;
begin
  Result := FDomain;
end;

function TevTaskInfo.GetID: TisGUID;
begin
  Result := GetName;
end;

function TevTaskInfo.GetNotificationEmail: string;
begin
  Result := FNotificationEmail;
end;

function TevTaskInfo.GetPriority: Integer;
begin
  Result := FPriority;
end;

function TevTaskInfo.GetProgressText: string;
begin
  Result := FProgressText;
end;

procedure TevTaskInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FNbr := AStream.ReadInteger;
  FTaskType := AStream.ReadShortString;
  FDomain := AStream.ReadShortString;
  FUser := AStream.ReadShortString;
  FState := TevTaskState(AStream.ReadByte);
  FPriority := AStream.ReadInteger;
  FNotificationEmail := AStream.ReadShortString;
  FSendEmailNotification := AStream.ReadBoolean;
  FCaption := AStream.ReadShortString;
  FProgressText := AStream.ReadString;
  FLastUpdate := AStream.ReadDouble;
  FSeenByUser := AStream.ReadBoolean;
  FDaysToStayInQueue := AStream.ReadInteger;
  FFirstRunAt := AStream.ReadDouble;
  FEmailSendRule := TevEmailSendRule(AStream.ReadByte);
end;

function TevTaskInfo.GetSendEmailNotification: Boolean;
begin
  Result := FSendEmailNotification;
end;

function TevTaskInfo.GetState: TevTaskState;
begin
  Result := FState;
end;

function TevTaskInfo.GetTaskType: String;
begin
  Result := FTaskType;
end;

function TevTaskInfo.GetUser: String;
begin
  Result := FUser;
end;

procedure TevTaskInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteInteger(FNbr);
  AStream.WriteShortString(FTaskType);
  AStream.WriteShortString(FDomain);
  AStream.WriteShortString(FUser);
  AStream.WriteByte(Ord(GetState));
  AStream.WriteInteger(GetPriority);
  AStream.WriteShortString(GetNotificationEmail);
  AStream.WriteBoolean(GetSendEmailNotification);
  AStream.WriteShortString(GetCaption);
  AStream.WriteString(GetProgressText);
  AStream.WriteDouble(GetLastUpdate);
  AStream.WriteBoolean(GetSeenByUser);
  AStream.WriteInteger(FDaysToStayInQueue);
  AStream.WriteDouble(FFirstRunAt);
  AStream.WriteByte(Ord(FEmailSendRule));
end;

procedure TevTaskInfo.AssignFromTask(const ATask: IevTask);
begin
  SetName(ATask.ID);
  FNbr := ATask.Nbr;
  FTaskType := ATask.TaskType;
  FUser := ATask.User;
  FDomain := ATask.Domain;
  FPriority := ATask.Priority;
  FNotificationEmail := ATask.NotificationEmail;
  FSendEmailNotification := ATask.SendEmailNotification;
  FEmailSendRule := ATask.EmailSendRule;
  FLastUpdate := ATask.LastUpdate;
  FState := ATask.State;
  FCaption := ATask.Caption;
  FProgressText := ATask.ProgressText;
  FSeenByUser := ATask.SeenByUser;
  FDaysToStayInQueue := ATask.DaysToStayInQueue;
  FFirstRunAt := ATask.FirstRunAt;
end;

function TevTaskInfo.GetNbr: Integer;
begin
  Result := FNbr;
end;

function TevTaskInfo.GetLastUpdate: TDateTime;
begin
  Result := FLastUpdate;
end;

function TevTaskInfo.GetSeenByUser: Boolean;
begin
  Result := FSeenByUser;
end;

class function TevTaskInfo.GetTypeID: String;
begin
  Result := 'TaskInfo';
end;


function TevTaskInfo.GetDaysToStayInQueue: Integer;
begin
  Result := FDaysToStayInQueue;
end;

function TevTaskInfo.GetFirstRunAt: TDateTime;
begin
  Result := FFirstRunAt;
end;

function TevTaskInfo.GetEmailSendRule: TevEmailSendRule;
begin
  Result := FEmailSendRule;
end;

{ TevTaskRequestInfo }

procedure TevTaskRequestInfo.AssignFromRequest(const ATaskRequest: IevTaskRequest);
begin
  SetName(ATaskRequest.RequestName);
  FProgressText := ATaskRequest.ProgressText;
  FStartedAt := ATaskRequest.StartedAt;
  FFinishedAt := ATaskRequest.FinishedAt;

  case ATaskRequest.State of
    rsWaiting:   FStatus := 'Waiting for resources';
    rsStarting:  FStatus := 'Starting';
    rsExecuting: FStatus := 'Executing';
    rsFinished:
      begin
        if ATaskRequest.GetExceptions <> '' then
          FStatus := 'Finished with exceptions'
        else if ATaskRequest.GetWarnings <> '' then
          FStatus := 'Finished with warnings'
        else
          FStatus := 'Finished successfully';
      end;
  end;
end;

constructor TevTaskRequestInfo.CreateFromRequest(const ATaskRequest: IevTaskRequest);
begin
  Create;
  AssignFromRequest(ATaskRequest);
end;

function TevTaskRequestInfo.GetFinishedAt: TDateTime;
begin
  Result := FFinishedAt;
end;

function TevTaskRequestInfo.GetProgressText: String;
begin
  Result := FProgressText;
end;

function TevTaskRequestInfo.GetStartedAt: TDateTime;
begin
  Result := FStartedAt;
end;

function TevTaskRequestInfo.GetStatus: String;
begin
  Result := FStatus;
end;

class function TevTaskRequestInfo.GetTypeID: String;
begin
  Result := 'TaskRequestInfo';
end;

procedure TevTaskRequestInfo.ReadSelfFromStream( const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FProgressText := AStream.ReadString;
  FStatus := AStream.ReadShortString;
  FStartedAt := AStream.ReadDouble;
  FFinishedAt := AStream.ReadDouble;
end;

procedure TevTaskRequestInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteString(FProgressText);
  AStream.WriteShortString(FStatus);
  AStream.WriteDouble(FStartedAt);
  AStream.WriteDouble(FFinishedAt);
end;

{ TevGlobalFlagInfo }

constructor TevGlobalFlagInfo.CreateLocked(const AType, ATag: String; const Reserved: Boolean = False);
begin
  Create(AType, ATag);
  FUserName := Context.UserAccount.User;
  FContextID := Context.GetRemoteID;
  FLockTime := Now;
end;

procedure TevGlobalFlagInfo.DoOnConstruction;
begin
  inherited;
  FDomain := Context.UserAccount.Domain;
end;

function TevGlobalFlagInfo.GetContextID: TisGUID;
begin
  Result := FContextID;
end;

function TevGlobalFlagInfo.GetDomain: String;
begin
  Result := FDomain;
end;

function TevGlobalFlagInfo.GetLockTime: TDateTime;
begin
  Result := FLockTime;
end;

function TevGlobalFlagInfo.GetFlagType: String;
begin
  Result := FFlagType;
end;

function TevGlobalFlagInfo.GetTag: String;
begin
  Result := FTag;
end;

class function TevGlobalFlagInfo.GetTypeID: String;
begin
  Result := 'GlobalFlagInfo';
end;

function TevGlobalFlagInfo.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TevGlobalFlagInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FFlagType := AStream.ReadShortString;
  FTag := AStream.ReadShortString;
  FDomain := AStream.ReadShortString;
  FUserName := AStream.ReadShortString;
  FContextID := AStream.ReadShortString;
  FLockTime := AStream.ReadDouble;
end;

procedure TevGlobalFlagInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetFlagName);
  AStream.WriteShortString(FFlagType);
  AStream.WriteShortString(FTag);
  AStream.WriteShortString(FDomain);
  AStream.WriteShortString(FUserName);
  AStream.WriteShortString(FContextID);
  AStream.WriteDouble(FLockTime);
end;

constructor TevGlobalFlagInfo.Create(const AType, ATag: String);
begin
  inherited Create;
  FFlagType := AType;
  FTag := ATag;
  SetName(AType + '.' + ATag);
end;

function TevGlobalFlagInfo.DecUsage: Integer;
begin
  Dec(FUsageCount);
  Result := FUsageCount;
end;

function TevGlobalFlagInfo.IncUsage: Integer;
begin
  Inc(FUsageCount);
  Result := FUsageCount;  
end;

function TevGlobalFlagInfo.GetFlagName: String;
begin
  Result := GetName;
end;

{ TevTaxPaymentsTaskParam }

function TevTaxPaymentsTaskParam.GetClientID: Integer;
begin
  Result := FClNbr;
end;

function TevTaxPaymentsTaskParam.GetCustomClientNumber: string;
begin
  Result := FCustomClientNumber;
end;

function TevTaxPaymentsTaskParam.GetFedTaxLiabilityList: string;
begin
  Result := FFedTaxLiabilityList;
end;

function TevTaxPaymentsTaskParam.GetLocalTaxLiabilityList: string;
begin
  Result := FLocalTaxLiabilityList;
end;

function TevTaxPaymentsTaskParam.GetStateTaxLiabilityList: string;
begin
  Result := FStateTaxLiabilityList;
end;

function TevTaxPaymentsTaskParam.GetSUITaxLiabilityList: string;
begin
  Result := FSUITaxLiabilityList;
end;

class function TevTaxPaymentsTaskParam.GetTypeID: String;
begin
  Result := 'TaxPaymentsTaskParam';
end;

procedure TevTaxPaymentsTaskParam.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCustomClientNumber := AStream.ReadString;
  FFedTaxLiabilityList := AStream.ReadString;
  FStateTaxLiabilityList := AStream.ReadString;
  FSUITaxLiabilityList := AStream.ReadString;
  FLocalTaxLiabilityList := AStream.ReadString;
end;

procedure TevTaxPaymentsTaskParam.SetClientID(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevTaxPaymentsTaskParam.SetCustomClientNumber(const AValue: String);
begin
  FCustomClientNumber := AValue;
end;

procedure TevTaxPaymentsTaskParam.SetFedTaxLiabilityList(const AValue: string);
begin
  FFedTaxLiabilityList := AValue;
end;

procedure TevTaxPaymentsTaskParam.SetLocalTaxLiabilityList(const AValue: string);
begin
  FLocalTaxLiabilityList := AValue;
end;

procedure TevTaxPaymentsTaskParam.SetStateTaxLiabilityList(const AValue: string);
begin
  FStateTaxLiabilityList := AValue;
end;

procedure TevTaxPaymentsTaskParam.SetSUITaxLiabilityList(const AValue: string);
begin
  FSUITaxLiabilityList := AValue;
end;

procedure TevTaxPaymentsTaskParam.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteString(FCustomClientNumber);
  AStream.WriteString(FFedTaxLiabilityList);
  AStream.WriteString(FStateTaxLiabilityList);
  AStream.WriteString(FSUITaxLiabilityList);
  AStream.WriteString(FLocalTaxLiabilityList);
end;

{ TevTaxPaymentsTaskParams }

function TevTaxPaymentsTaskParams.Add: IevTaxPaymentsTaskParam;
begin
  Result := TevTaxPaymentsTaskParam.Create(Self, False);
end;

function TevTaxPaymentsTaskParams.Count: Integer;
begin
  Result := ChildCount;
end;

function TevTaxPaymentsTaskParams.GetItem(AIndex: Integer): IevTaxPaymentsTaskParam;
begin
  Result := GetChild(AIndex) as IevTaxPaymentsTaskParam;
end;

class function TevTaxPaymentsTaskParams.GetTypeID: String;
begin
  Result := 'TaxPaymentsTaskParams';
end;

{ TevACHOptions }

function TevACHOptions.GetBalanceBatches: Boolean;
begin
  Result := FBalanceBatches;
end;

function TevACHOptions.GetBankOne: Boolean;
begin
  Result := FBankOne;
end;

function TevACHOptions.GetBlockDebits: Boolean;
begin
  Result := FBlockDebits;
end;

function TevACHOptions.GetBlockSBCredit: Boolean;
begin
  Result := FBlockSBCredit;
end;

function TevACHOptions.GetCoId: String;
begin
  Result := FCoId;
end;

function TevACHOptions.GetCombineTrans: Integer;
begin
  Result := FCombineTrans;
end;

function TevACHOptions.GetCombineThreshold: Currency;
begin
  Result := FCombineThreshold;
end;

function TevACHOptions.GetCTS: Boolean;
begin
  Result := FCTS;
end;

function TevACHOptions.GetDescReversal: Boolean;
begin
  Result := FDescReversal;
end;

function TevACHOptions.GetEffectiveDate: Boolean;
begin
  Result := FEffectiveDate;
end;

function TevACHOptions.GetFedReserve: Boolean;
begin
  Result := FFedReserve;
end;

function TevACHOptions.GetFooter: String;
begin
  Result := FFooter;
end;

function TevACHOptions.GetHeader: String;
begin
  Result := FHeader;
end;

function TevACHOptions.GetIntercept: Boolean;
begin
  Result := FIntercept;
end;

function TevACHOptions.GetPayrollHeader: Boolean;
begin
  Result := FPayrollHeader;
end;

function TevACHOptions.GetSBEINOverride: String;
begin
  Result := FSBEINOverride;
end;

function TevACHOptions.GetSunTrust: Boolean;
begin
  Result := FSunTrust;
end;

class function TevACHOptions.GetTypeID: String;
begin
  Result := 'ACHOptions';
end;

function TevACHOptions.GetUseSBEIN: Boolean;
begin
  Result := FUseSBEIN;
end;

procedure TevACHOptions.SetBalanceBatches(const AValue: Boolean);
begin
  FBalanceBatches := AValue;
end;

procedure TevACHOptions.SetBankOne(const AValue: Boolean);
begin
  FBankOne := AValue;
end;

procedure TevACHOptions.SetBlockDebits(const AValue: Boolean);
begin
  FBlockDebits := AValue;
end;

procedure TevACHOptions.SetBlockSBCredit(const AValue: Boolean);
begin
  FBlockSBCredit := AValue;
end;

procedure TevACHOptions.SetCoId(const AValue: String);
begin
  FCoId := AValue;
end;

procedure TevACHOptions.SetCombineTrans(const AValue: Integer);
begin
  FCombineTrans := AValue;
end;

procedure TevACHOptions.SetCombineThreshold(const AValue: Currency);
begin
  FCombineThreshold := AValue;
end;

procedure TevACHOptions.SetCTS(const AValue: Boolean);
begin
  FCTS := AValue;
end;

procedure TevACHOptions.SetDescReversal(const AValue: Boolean);
begin
  FDescReversal := AValue;
end;

procedure TevACHOptions.SetEffectiveDate(const AValue: Boolean);
begin
  FEffectiveDate := AValue;
end;

procedure TevACHOptions.SetFedReserve(const AValue: Boolean);
begin
  FFedReserve := AValue;
end;

procedure TevACHOptions.SetFooter(const AValue: String);
begin
  FFooter := AValue;
end;

procedure TevACHOptions.SetHeader(const AValue: String);
begin
  FHeader := AValue;
end;

procedure TevACHOptions.SetIntercept(const AValue: Boolean);
begin
  FIntercept := AValue;
end;

procedure TevACHOptions.SetPayrollHeader(const AValue: Boolean);
begin
  FPayrollHeader := AValue;
end;

procedure TevACHOptions.SetSBEINOverride(const AValue: String);
begin
  FSBEINOverride := AValue;
end;

procedure TevACHOptions.SetSunTrust(const AValue: Boolean);
begin
  FSunTrust := AValue;
end;

procedure TevACHOptions.SetUseSBEIN(const AValue: Boolean);
begin
  FUseSBEIN := AValue;
end;

procedure TevACHOptions.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FBalanceBatches := AStream.ReadBoolean;
  FBlockDebits := AStream.ReadBoolean;
  FCombineTrans := AStream.ReadInteger;
  FUseSBEIN := AStream.ReadBoolean;
  FCTS := AStream.ReadBoolean;
  FSunTrust := AStream.ReadBoolean;
  FBlockSBCredit := AStream.ReadBoolean;
  FFedReserve := AStream.ReadBoolean;
  FIntercept := AStream.ReadBoolean;
  FPayrollHeader := AStream.ReadBoolean;
  FDescReversal := AStream.ReadBoolean;
  FBankOne := AStream.ReadBoolean;
  FEffectiveDate := AStream.ReadBoolean;
  FCombineThreshold := AStream.ReadCurrency;
  FCoId := AStream.ReadString;
  FSBEINOverride := AStream.ReadString;
  FHeader := AStream.ReadString;
  FFooter := AStream.ReadString;
  FDisplayCoNbrAndName := AStream.ReadBoolean;
  FPostProcessReportName := AStream.ReadString;
end;

procedure TevACHOptions.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FBalanceBatches);
  AStream.WriteBoolean(FBlockDebits);
  AStream.WriteInteger(FCombineTrans);
  AStream.WriteBoolean(FUseSBEIN);
  AStream.WriteBoolean(FCTS);
  AStream.WriteBoolean(FSunTrust);
  AStream.WriteBoolean(FBlockSBCredit);
  AStream.WriteBoolean(FFedReserve);
  AStream.WriteBoolean(FIntercept);
  AStream.WriteBoolean(FPayrollHeader);
  AStream.WriteBoolean(FDescReversal);
  AStream.WriteBoolean(FBankOne);
  AStream.WriteBoolean(FEffectiveDate);
  AStream.WriteCurrency(FCombineThreshold);
  AStream.WriteString(FCoId);
  AStream.WriteString(FSBEINOverride);
  AStream.WriteString(FHeader);
  AStream.WriteString(FFooter);
  AStream.WriteBoolean(FDisplayCoNbrAndName);
  AStream.WriteString(FPostProcessReportName);
end;

function TevACHOptions.GetDisplayCoNbrAndName: Boolean;
begin
  Result := FDisplayCoNbrAndName;
end;

procedure TevACHOptions.SetDisplayCoNbrAndName(const AValue: Boolean);
begin
  FDisplayCoNbrAndName := AValue;
end;

function TevACHOptions.Revision: TisRevision;
begin
  Result := 1;
end;

function TevACHOptions.GetPostProcessReportName: String;
begin
  Result := FPostProcessReportName;
end;

procedure TevACHOptions.SetPostProcessReportName(const AValue: String);
begin
  FPostProcessReportName := AValue;
end;

{ TevMachineInfo }

function TevMachineInfo.CipherKey: String;
begin
  Result := FCipherKey;
end;

procedure TevMachineInfo.DoOnConstruction;
begin
  inherited;
  FID := Cardinal(CreateMachineID([midUser, midSystem, midNetwork, midDrives]));
  FComputerName := GetComputerNameString;
  FCipherKey := GetComputerCipherKey;
  FIPaddress := SocketLib.LookupHostAddr(FComputerName);
  FPrinters := TevPrintersList.Create(nil, True);
end;

class function TevMachineInfo.GetTypeID: String;
begin
  Result := 'MachineInfo';
end;

function TevMachineInfo.ID: Cardinal;
begin
  Result := FID;
end;

function TevMachineInfo.IPaddress: String;
begin
  Result := FIPaddress;
end;

function TevMachineInfo.Name: String;
begin
  Result := FComputerName;
end;

function TevMachineInfo.Printers: IevPrintersList;
begin
  Result := FPrinters;
end;

procedure TevMachineInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FID := AStream.ReadInteger;
  FComputerName := AStream.ReadShortString;
  FIPaddress := AStream.ReadShortString;
  FCipherKey := AStream.ReadString;
  (FPrinters as IisInterfacedObject).ReadFromStream(AStream);
end;

procedure TevMachineInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FID);
  AStream.WriteShortString(FComputerName);
  AStream.WriteShortString(FIPaddress);
  AStream.WriteString(FCipherKey);
  (FPrinters as IisInterfacedObject).WriteToStream(AStream);
end;


{ TevPreprocessQuarterEndTaskFilterItem }

function TevPreprocessQuarterEndTaskFilterItem.GetCaption: string;
begin
  Result := FCaption;
end;

function TevPreprocessQuarterEndTaskFilterItem.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevPreprocessQuarterEndTaskFilterItem.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevPreprocessQuarterEndTaskFilterItem.GetConsolidation: Boolean;
begin
  Result := FConsolidation;
end;

class function TevPreprocessQuarterEndTaskFilterItem.GetTypeID: String;
begin
  Result := 'PrepQETskFltItem';
end;

procedure TevPreprocessQuarterEndTaskFilterItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FConsolidation := AStream.ReadBoolean;
  FCaption := AStream.ReadShortString;
end;

procedure TevPreprocessQuarterEndTaskFilterItem.SetCaption(const AValue: string);
begin
  FCaption := AValue;
end;

procedure TevPreprocessQuarterEndTaskFilterItem.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevPreprocessQuarterEndTaskFilterItem.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevPreprocessQuarterEndTaskFilterItem.SetConsolidation(const AValue: Boolean);
begin
  FConsolidation := AValue;
end;

procedure TevPreprocessQuarterEndTaskFilterItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteBoolean(FConsolidation);
  AStream.WriteShortString(FCaption);
end;


{ TevPreprocessQuarterEndTaskFilter }

function TevPreprocessQuarterEndTaskFilter.Add: IevPreprocessQuarterEndTaskFilterItem;
begin
  Result := TevPreprocessQuarterEndTaskFilterItem.Create(Self, False);
end;

procedure TevPreprocessQuarterEndTaskFilter.Clear;
begin
  RemoveAllChildren;
end;

function TevPreprocessQuarterEndTaskFilter.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevPreprocessQuarterEndTaskFilter.Delete(
  const AItem: IevPreprocessQuarterEndTaskFilterItem);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevPreprocessQuarterEndTaskFilter.GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskFilterItem;
begin
  Result := GetChild(AIndex) as IevPreprocessQuarterEndTaskFilterItem;
end;

class function TevPreprocessQuarterEndTaskFilter.GetTypeID: String;
begin
  Result := 'PrepQETskFilter';
end;

{ TevPreprocessQuarterEndTaskResultItem }

function TevPreprocessQuarterEndTaskResultItem.GetCoCustomNumber: string;
begin
  Result := FCoCustomNumber;
end;

function TevPreprocessQuarterEndTaskResultItem.GetCoName: string;
begin
  Result := FCoName;
end;

function TevPreprocessQuarterEndTaskResultItem.GetConsolidation: Boolean;
begin
  Result := FConsolidation;
end;

function TevPreprocessQuarterEndTaskResultItem.GetErrorMessage: string;
begin
  Result := FErrorMessage;
end;

function TevPreprocessQuarterEndTaskResultItem.GetTaxAdjReports: IEvDualStream;
begin
  Result := FTaxAdjReports;
end;

function TevPreprocessQuarterEndTaskResultItem.GetTimeStampFinished: TDateTime;
begin
  Result := FTimeStampFinished;
end;

function TevPreprocessQuarterEndTaskResultItem.GetTimeStampStarted: TDateTime;
begin
  Result := FTimeStampStarted;
end;

class function TevPreprocessQuarterEndTaskResultItem.GetTypeID: String;
begin
  Result := 'PrepQETskResItem';
end;

procedure TevPreprocessQuarterEndTaskResultItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FCoCustomNumber := AStream.ReadShortString;
  FCoName := AStream.ReadShortString;
  FConsolidation := AStream.ReadBoolean;
  FTimeStampStarted := AStream.ReadDouble;
  FTimeStampFinished := AStream.ReadDouble;
  FErrorMessage := AStream.ReadString;
  FTaxAdjReports := AStream.ReadStream(nil, True);
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetCoCustomNumber(const AValue: string);
begin
  FCoCustomNumber := AValue;  
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetCoName(const AValue: string);
begin
  FCoName := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetConsolidation(const AValue: Boolean);
begin
  FConsolidation := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetErrorMessage(const AValue: string);
begin
  FErrorMessage := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetTaxAdjReports(const AValue: IEvDualStream);
begin
  FTaxAdjReports := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetTimeStampFinished(const AValue: TDateTime);
begin
  FTimeStampFinished := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.SetTimeStampStarted(const AValue: TDateTime);
begin
  FTimeStampStarted := AValue;
end;

procedure TevPreprocessQuarterEndTaskResultItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FCoCustomNumber);
  AStream.WriteShortString(FCoName);
  AStream.WriteBoolean(FConsolidation);
  AStream.WriteDouble(FTimeStampStarted);
  AStream.WriteDouble(FTimeStampFinished);
  AStream.WriteString(FErrorMessage);
  AStream.WriteStream(FTaxAdjReports);
end;


{ TevPreprocessQuarterEndTaskResult }

function TevPreprocessQuarterEndTaskResult.Add: IevPreprocessQuarterEndTaskResultItem;
begin
  Result := TevPreprocessQuarterEndTaskResultItem.Create(Self, False);
end;

function TevPreprocessQuarterEndTaskResult.Count: Integer;
begin
  Result := ChildCount;
end;

function TevPreprocessQuarterEndTaskResult.GetItem(AIndex: Integer): IevPreprocessQuarterEndTaskResultItem;
begin
  Result := GetChild(AIndex) as IevPreprocessQuarterEndTaskResultItem;
end;

class function TevPreprocessQuarterEndTaskResult.GetTypeID: String;
begin
  Result := 'PrepQETskResult';
end;


{ TevQECTaskFilterItem }

function TevQECTaskFilterItem.GetCaption: string;
begin
  Result := FCaption;
end;

function TevQECTaskFilterItem.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevQECTaskFilterItem.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

class function TevQECTaskFilterItem.GetTypeID: String;
begin
  Result := 'QECTaskFilterItem';
end;

procedure TevQECTaskFilterItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FCaption := AStream.ReadShortString;
end;

procedure TevQECTaskFilterItem.SetCaption(const AValue: string);
begin
  FCaption := AValue;
end;

procedure TevQECTaskFilterItem.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevQECTaskFilterItem.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevQECTaskFilterItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteShortString(FCaption);
end;

{ TevQECTaskFilter }

function TevQECTaskFilter.Add: IevQECTaskFilterItem;
begin
  Result := TevQECTaskFilterItem.Create(Self, False);
end;

function TevQECTaskFilter.Count: Integer;
begin
  Result := ChildCount;
end;

function TevQECTaskFilter.GetItem(AIndex: Integer): IevQECTaskFilterItem;
begin
  Result := GetChild(AIndex) as IevQECTaskFilterItem;
end;

class function TevQECTaskFilter.GetTypeID: String;
begin
  Result := 'QECTaskFilter';
end;

{ TevQECTaskResultItem }

function TevQECTaskResultItem.GetQECReports: IEvDualStream;
begin
 Result := FQECReports;
end;

class function TevQECTaskResultItem.GetTypeID: String;
begin
  Result := 'PrepQECTskResItem';
end;

procedure TevQECTaskResultItem.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FQECReports := AStream.ReadStream(nil, True);

end;

procedure TevQECTaskResultItem.SetQECReports(const AValue: IEvDualStream);
begin
  FQECReports := AValue;
end;

procedure TevQECTaskResultItem.WriteSelfToStream(
  const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteStream(FQECReports);
end;

{ TevQECTaskResult }

function TevQECTaskResult.Add: IevQECTaskResultItem;
begin
  Result := TevQECTaskResultItem.Create(Self, False);
end;

function TevQECTaskResult.Count: Integer;
begin
  Result := ChildCount;
end;

function TevQECTaskResult.GetItem(AIndex: Integer): IevQECTaskResultItem;
begin
  Result := GetChild(AIndex) as IevQECTaskResultItem;
end;

class function TevQECTaskResult.GetTypeID: String;
begin
  Result := 'PrepQECTskResult';
end;

{ TevLogFile }

procedure TevLogFile.AddContextEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String);
var
  sDetail: String;
  sText: String;
begin
  sText := '';
  if Context <> nil then
  begin
    sDetail := 'User: ' + EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
    if Context.UserAccount.Domain <> sDefaultDomain then
      sText := Context.UserAccount.Domain + ': ';
  end
  else
    sDetail := 'User: Undefined';

  AddStrValue(sDetail, ADetails, #13);
  if AEventType in [etError, etFatalError] then
    AddStrValue(sDetail, GetStack, #13);

  AddEvent(AEventType, AEventClass, sText + AText, sDetail);
end;


destructor TevLogFile.Destroy;
begin
  SetEMailNotifierActive(False);
  inherited;
end;

function TevLogFile.GetEMailNotifierActive: Boolean;
begin
  Result := Assigned(FEmailNotifier);
end;

procedure TevLogFile.SetEMailNotifierActive(const AValue: Boolean);
begin
  if AValue <> GetEMailNotifierActive then
    if AValue then
    begin
      FEmailNotifier := TevEventNotifier.Create;
      RegisterCallback(FEmailNotifier as ILogFileCallback);
      FEmailNotifier.Active := True;
    end
    else
    begin
      FEmailNotifier.Active := False;
      UnregisterCallback(FEmailNotifier as ILogFileCallback);
      FEmailNotifier := nil;
    end;
end;

{ TevSecAccountRights }

function TevSecAccountRights.AccountID: String;
begin
  Result := GetName;
end;

function TevSecAccountRights.Clients: IevSecClients;
begin
  Result := FindChildByName('Clients') as IevSecClients;
end;

constructor TevSecAccountRights.Create(const AAccountID: String);
begin
  inherited Create(nil, True);
  SetName(AAccountID);
end;

procedure TevSecAccountRights.DoOnConstruction;
var
  O: IisNamedObject;
begin
  inherited;
  O := TevSecElements.Create(Self, False);
  O.Name := 'Menus';

  O := TevSecElements.Create(Self, False);
  O.Name := 'Screens';

  O := TevSecElements.Create(Self, False);
  O.Name := 'Functions';

  O := TevSecClients.Create(Self, False);
  O := TevSecFields.Create(Self, False);
  O := TevSecRecords.Create(Self, False);
end;

function TevSecAccountRights.Fields: IevSecFields;
begin
  Result := FindChildByName('Fields') as IevSecFields;
end;

function TevSecAccountRights.Functions: IevSecElements;
begin
  Result := FindChildByName('Functions') as IevSecElements;
end;

function TevSecAccountRights.GetElementsByType(const AElementType: Char): IevSecElements;
begin
  case AElementType of
    etScreen:       Result := Screens;
    etFunc:         Result := Functions;
    etMenu:         Result := Menus;
    etTableField:   Result := Fields;
    etTableRecord:  Result := Records;
  else
    Result := nil;
  end;
end;

function TevSecAccountRights.GetElementState(const AElementType: Char; const AElementName: String): TevSecElementState;
var
  E: IevSecElements;
begin
  E := GetElementsByType(AElementType);
  if Assigned(E) then
    Result := E.GetState(AElementName)
  else
    Result := stDisabled;
end;

function TevSecAccountRights.GetElementStateInfo(const AElementType: Char; const AElementName: String): IevSecStateInfo;
var
  E: IevSecElements;
begin
  E := GetElementsByType(AElementType);
  if Assigned(E) then
    Result := E.GetStateInfo(AElementName)
  else
    Result := nil;
end;

class function TevSecAccountRights.GetTypeID: String;
begin
  Result := 'SecAccountRights';
end;

function TevSecAccountRights.Level: TevSecurityLevel;
begin
  Result := FLevel;
end;

function TevSecAccountRights.Menus: IevSecElements;
begin
  Result := FindChildByName('Menus') as IevSecElements;
end;

procedure TevSecAccountRights.ReadChildrenFromStream(const AStream: IEvDualStream);
begin
  RemoveAllChildren;
  inherited;
end;

procedure TevSecAccountRights.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FLevel := TevSecurityLevel(AStream.ReadByte);
end;

function TevSecAccountRights.Records: IevSecRecords;
begin
  Result := FindChildByName('Records') as IevSecRecords;
end;

function TevSecAccountRights.Screens: IevSecElements;
begin
  Result := FindChildByName('Screens') as IevSecElements;
end;

procedure TevSecAccountRights.SetElementState(const AElementType: Char;
  const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
var
  E: IevSecElements;
begin
  E := GetElementsByType(AElementType);
  if Assigned(E) then
    E.SetState(AElementName, AState, AFromGroup);
end;

procedure TevSecAccountRights.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteByte(Ord(FLevel));
end;


{ TevSecElements }

function TevSecElements.AccountID: String;
begin
  Result := (GetOwner as IevSecAccountRights).AccountID;
end;

function TevSecElements.AddElement(const AElementName: String): Integer;
var
  O: IInterface;
begin
  O := TevSecStateInfo.Create;
  Result := FList.AddObject(AElementName, O);
  FModified := True;
end;

procedure TevSecElements.BeginChange;
begin
  FList.Sorted := False;
end;

procedure TevSecElements.Clear;
begin
  FList.Clear;
end;

procedure TevSecElements.ClearModified;
begin
  FModified := False;
end;

function TevSecElements.Count: Integer;
begin
  Result := FList.Count;
end;

procedure TevSecElements.DoOnConstruction;
begin
  inherited;
  FList := TisStringList.CreateAsIndex;
end;

procedure TevSecElements.EndChange;
begin
  FList.Sorted := True;
end;

function TevSecElements.GetElementByIndex(const AIndex: Integer): String;
begin
  Result := FList[AIndex];
end;

function TevSecElements.GetState(const AElementName: String): TevSecElementState;
var
  i: Integer;
begin
  i := FList.IndexOf(AElementName);
  if i <> -1 then
    Result := GetStateInfo(i).State
  else
    Result := stDisabled;
end;

function TevSecElements.GetStateInfo(const AElementName: String): IevSecStateInfo;
var
  i: Integer;
begin
  i := FList.IndexOf(AElementName);
  if i <> -1 then
    Result := GetStateInfo(i)
  else
    Result := nil;
end;

function TevSecElements.GetStateInfo(const AIndex: Integer): IevSecStateInfo;
begin
  Result := FList.Objects[AIndex] as IevSecStateInfo;
end;

class function TevSecElements.GetTypeID: String;
begin
  Result := 'SecARElements';
end;

function TevSecElements.IndexOf(const AElementName: String): Integer;
begin
  Result := FList.IndexOf(AElementName);
end;

function TevSecElements.Modified: Boolean;
begin
  Result := FModified;
end;

procedure TevSecElements.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  i, j, n: Integer;
  StateInfo: IevSecStateInfo;
begin
  inherited;

  SetName(AStream.ReadShortString);

  n := AStream.ReadInteger;
  for i := 1 to n do
  begin
    j := AddElement(AStream.ReadShortString);
    StateInfo := GetStateInfo(j);
    (StateInfo as IisInterfacedObject).ReadFromStream(AStream);
  end;
end;

procedure TevSecElements.SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
var
  i: Integer;
begin
  i := FList.IndexOf(AElementName);
  if i = -1 then
    i := AddElement(AElementName);

  SetStateByIndex(i, AState, AFromGroup);
end;

procedure TevSecElements.SetStateByIndex(const AIndex: Integer;
  const AState: TevSecElementState; const AFromGroup: Boolean);
var
  StateInfo: IevSecStateInfo;
begin
  StateInfo := GetStateInfo(AIndex);

  if (StateInfo.State <> AState) or (StateInfo.FromGroup <> AFromGroup) then
    FModified := True;

  StateInfo.State := AState;
  StateInfo.FromGroup := AFromGroup;
end;

procedure TevSecElements.WriteSelfToStream(const AStream: IEvDualStream);
var
  i: Integer;
  StateInfo: IevSecStateInfo;
begin
  inherited;

  AStream.WriteShortString(GetName);
    
  AStream.WriteInteger(Count);
  for i := 0 to Count - 1 do
  begin
    StateInfo := GetStateInfo(i);
    AStream.WriteShortString(GetElementByIndex(i));
    (StateInfo as IisInterfacedObject).WriteToStream(AStream);
  end;
end;

{ TevSecStateInfo }

function TevSecStateInfo.GetFromGroup: Boolean;
begin
  Result := FFromGroup;
end;

function TevSecStateInfo.GetState: TevSecElementState;
begin
  Result := FState;
end;

procedure TevSecStateInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FState := TevSecElementState(AStream.ReadByte);
  FFromGroup := AStream.ReadBoolean;
end;

procedure TevSecStateInfo.SetFromGroup(const AValue: Boolean);
begin
  FFromGroup := AValue;
end;

procedure TevSecStateInfo.SetState(const AValue: TevSecElementState);
begin
  FState := AValue;
end;

procedure TevSecStateInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(Ord(FState));
  AStream.WriteBoolean(FFromGroup);
end;

{ TevSecFieldInfo }

function TevSecFieldInfo.GetFieldSize: Integer;
begin
  Result := FFieldSize;
end;

function TevSecFieldInfo.GetFieldType: String;
begin
  Result := FFieldType;
end;

procedure TevSecFieldInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FFieldType := AStream.ReadShortString;
  FFieldSize := AStream.ReadInteger;
end;

procedure TevSecFieldInfo.SetFieldSize(const AValue: Integer);
begin
  FFieldSize := AValue;
end;

procedure TevSecFieldInfo.SetFieldType(const AValue: String);
begin
  FFieldType := AValue;
end;


procedure TevSecFieldInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FFieldType);
  AStream.WriteInteger(FFieldSize);
end;

{ TevSecRecordInfo }

function TevSecRecordInfo.GetClientID: Integer;
begin
  Result := FClientID;
end;

function TevSecRecordInfo.GetDBType: TevDBType;
begin
  Result := FDBType;
end;

function TevSecRecordInfo.GetFilter: String;
begin
  Result := FFilter;
end;

function TevSecRecordInfo.GetFilterType: String;
begin
  Result := FFilterType;
end;

procedure TevSecRecordInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClientID := AStream.ReadInteger;
  FFilterType := AStream.ReadShortString;
  FFilter := AStream.ReadString;
end;

procedure TevSecRecordInfo.SetClientID(const AValue: Integer);
begin
  FClientID := AValue;
end;

procedure TevSecRecordInfo.SetDBType(const AValue: TevDBType);
begin
  FDBType := AValue;
end;

procedure TevSecRecordInfo.SetFilter(const AValue: String);
begin
  FFilter := AValue;
end;

procedure TevSecRecordInfo.SetFilterType(const AValue: String);
begin
  FFilterType := AValue;
end;


procedure TevSecRecordInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClientID);
  AStream.WriteShortString(FFilterType);
  AStream.WriteString(FFilter);
end;

{ TevSecFields }

function TevSecFields.AddElement(const AElementName: String): Integer;
var
  O: IInterface;
begin
  O := TevSecFieldInfo.Create;
  Result := FList.AddObject(AElementName, O);
end;

function TevSecFields.GetSecFieldInfo(const ATableName, AFieldName: String): IevSecFieldInfo;
var
  i: Integer;
begin
  i := FList.IndexOf(ATableName + '.' + AFieldName);
  if i = -1 then
    Result := nil
  else
    Result := GetSecFieldInfo(i);
end;

procedure TevSecFields.DoOnConstruction;
begin
  inherited;
  SetName('Fields');
end;

function TevSecFields.GetSecFieldInfo(const AIndex: Integer): IevSecFieldInfo;
begin
  Result := GetStateInfo(AIndex) as IevSecFieldInfo;
end;

function TevSecFields.GetState(const AElementName: String): TevSecElementState;
var
  i: Integer;
begin
  i := FList.IndexOf(AElementName);
  if i <> -1 then
    Result := GetStateInfo(i).State
  else
    Result := stEnabled;
end;

procedure TevSecFields.SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
begin
  // nothing
end;

class function TevSecFields.GetTypeID: String;
begin
  Result := 'SecARElementsFields';
end;

{ TevSecClients }

procedure TevSecClients.DoOnConstruction;
begin
  inherited;
  SetName('Clients');
end;

class function TevSecClients.GetTypeID: String;
begin
  Result := 'SecARElementsClients';
end;

function TevSecClients.IsClientEnabled(const AClientID: Integer): Boolean;
begin
  Result := GetState(IntToStr(AClientID)) = stEnabled;
end;

{ TevSecRecords }

function TevSecRecords.AddElement(const AElementName: String): Integer;
var
  O: IInterface;
begin
  O := TevSecRecordInfo.Create;
  Result := FList.AddObject(AElementName, O);
end;

procedure TevSecRecords.DoOnConstruction;
begin
  inherited;
  SetName('Records');
end;

function TevSecRecords.GetSecRecordInfo(const AIndex: Integer): IevSecRecordInfo;
begin
  Result := GetStateInfo(AIndex) as IevSecRecordInfo;
end;

function TevSecRecords.GetState(const AElementName: String): TevSecElementState;
var
  i: Integer;
begin
  i := FList.IndexOf(AElementName);
  if i <> -1 then
    Result := GetStateInfo(i).State
  else
    Result := stEnabled;
end;

class function TevSecRecords.GetTypeID: String;
begin
  Result := 'SecARElementsRecords';
end;

function TevSecRecords.IsBranchEnabled(const AClientID, ABranchNbr: Integer): Boolean;
begin
  Result := IsRecEnabled(AClientID, 'BR', ABranchNbr);
end;

function TevSecRecords.IsCompanyEnabled(const AClientID, ACompanyNbr: Integer): Boolean;
begin
  Result := IsRecEnabled(AClientID, 'CO', ACompanyNbr);
end;

function TevSecRecords.IsDepartmentEnabled(const AClientID, ADepartmentNbr: Integer): Boolean;
begin
  Result := IsRecEnabled(AClientID, 'DP', ADepartmentNbr);
end;

function TevSecRecords.IsDivisionEnabled(const AClientID, ADivisionNbr: Integer): Boolean;
begin
  Result := IsRecEnabled(AClientID, 'DV', ADivisionNbr);
end;

function TevSecRecords.IsRecEnabled(const AClientID: Integer;
  const AType: String; const ANbr: Integer): Boolean;
var
  i: Integer;
begin
  i := IndexOf('C:' + IntToStr(AClientID) + ':' + AType);
  if i <> -1 then
  begin
    if Contains(GetSecRecordInfo(i).Filter + ',', IntToStr(ANbr) + ',') then
      Result := GetSecRecordInfo(i).State = stEnabled
    else
      Result := GetSecRecordInfo(i).State = stDisabled;
  end
  else
    Result := True;
end;

function TevSecRecords.IsTeamEnabled(const AClientID, ATeamNbr: Integer): Boolean;
begin
  Result := IsRecEnabled(AClientID, 'TM', ATeamNbr);
end;

procedure TevSecRecords.SetState(const AElementName: String; const AState: TevSecElementState; const AFromGroup: Boolean);
begin
  // nothing
end;


{ TevSecurityStructure }

constructor TevSecurityStructure.Create(const ASecStructure: IevDualStream);
begin
  inherited Create;
  ASecStructure.Position := 0;
  FObj := TSecurityAtomCollection.CreateFromStream(ASecStructure);
end;

destructor TevSecurityStructure.Destroy;
begin
  FObj.Free;
  inherited;
end;

function TevSecurityStructure.GetObj: TSecurityAtomCollection;
begin
  Result := FObj;
end;

class function TevSecurityStructure.GetTypeID: String;
begin
  Result := 'SecStructureLegacy';
end;

procedure TevSecurityStructure.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FObj.Free;
  FObj := TSecurityAtomCollection.CreateFromStream(AStream);
  FObj.LoadContextsFromStream(AStream);
end;

function TevSecurityStructure.Rebuild: IevDualStream;
begin
  Result := nil;
end;

procedure TevSecurityStructure.SetObj(const AValue: TSecurityAtomCollection);
begin
  // Should be used only with care! Otherwith FObj may leak.
  FObj := AValue;
end;


procedure TevSecurityStructure.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  FObj.StreamStructureTo(AStream);
  FObj.SaveContextsToStream(AStream);
end;


{ TevProgressInfo }

function TevProgressInfo.GetProgressCurr: Integer;
begin
  Result := FProgressCurr;
end;

function TevProgressInfo.GetProgressMax: Integer;
begin
  Result := FProgressMax;
end;

function TevProgressInfo.GetProgressText: String;
begin
  Result := FProgressText;
end;

procedure TevProgressInfo.SetProgressCurr(const AValue: Integer);
begin
  FProgressCurr := AValue;
end;

procedure TevProgressInfo.SetProgressMax(const AValue: Integer);
begin
  FProgressMax := AValue;
end;

procedure TevProgressInfo.SetProgressText(const AValue: String);
begin
  FProgressText := AValue;
end;

{ TevPrinterInfo }

function TevPrinterInfo.GetHOffset: Integer;
begin
  Result := FHOffset;
end;

class function TevPrinterInfo.GetTypeID: String;
begin
  Result := 'PrinterInfo';
end;

function TevPrinterInfo.GetVOffset: Integer;
begin
  Result := FVOffset;
end;

procedure TevPrinterInfo.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FVOffset := AStream.ReadInteger;
  FHOffset := AStream.ReadInteger;
end;

procedure TevPrinterInfo.SetHOffset(const AValue: Integer);
begin
  FHOffset := AValue;
end;

procedure TevPrinterInfo.SetVOffset(const AValue: Integer);
begin
  FVOffset := AValue;
end;

procedure TevPrinterInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteInteger(FVOffset);
  AStream.WriteInteger(FHOffset);    
end;


{ TevPrintersList }

function TevPrintersList.Count: Integer;
begin
  Result := ChildCount;
end;

function TevPrintersList.FindPrinter(const AName: String): IevPrinterInfo;
begin
  Result := FindChildByName(AName) as IevPrinterInfo;
end;

function TevPrintersList.GetItem(Index: Integer): IevPrinterInfo;
begin
  Result := GetChild(Index) as IevPrinterInfo;
end;

class function TevPrintersList.GetTypeID: String;
begin
  Result := 'PrintersList';
end;

procedure TevPrintersList.Load;
var
  Prn: IevPrinterInfo;
  i: Integer;
  PrnList: IisStringListRO;
  sPrinter: String;
  Prnt: TPrinter;
begin
  Lock;
  try
    RemoveAllChildren;
    Prnt := TPrinter.Create;
    try
      for i := 0 to Prnt.Printers.Count - 1 do
      begin
        Prn := TevPrinterInfo.Create;
        (Prn as IisNamedObject).Name := Prnt.Printers[i];
         TevPrinterInfo((Prn as IisNamedObject).GetImplementation).FStorageID := GetUniqueID;
        AddChild(Prn as IisNamedObject);
      end;
    finally
      Prnt.Free;
    end;

    PrnList := mb_AppSettings.GetChildNodes('Settings\Printers\');
    for i := 0 to PrnList.Count - 1 do
    begin
      sPrinter := mb_AppSettings.AsString['Settings\Printers\' + PrnList[i] +'\PrinterName'];
      Prn := FindPrinter(sPrinter);
      if Assigned(Prn) then
      begin
        Prn.VOffset := mb_AppSettings.GetValue('Settings\Printers\' + PrnList[i] + '\VOffset', Integer(0));
        Prn.HOffset := mb_AppSettings.GetValue('Settings\Printers\' + PrnList[i] + '\HOffset', Integer(0));
        TevPrinterInfo((Prn as IisNamedObject).GetImplementation).FStorageID := PrnList[i];
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TevPrintersList.Save;
var
  Prn: IevPrinterInfo;
  i: Integer;
  sStorageID: String;
begin
  Lock;
  try
    mb_AppSettings.DeleteNode('Settings\Printers\');
    for i := 0 to Count - 1 do
    begin
      Prn := GetItem(i);
      sStorageID := TevPrinterInfo((Prn as IisNamedObject).GetImplementation).FStorageID;
      mb_AppSettings.AsString['Settings\Printers\' + sStorageID + '\PrinterName'] := Prn.Name;
      mb_AppSettings.AsInteger['Settings\Printers\' + sStorageID + '\VOffset'] := Prn.VOffset;
      mb_AppSettings.AsInteger['Settings\Printers\' + sStorageID + '\HOffset'] := Prn.HOffset;
    end;
  finally
    Unlock;
  end;
end;

{ TevMailMessage }

procedure TevMailMessage.AttachFile(const AFileName: String);
var
  S: IisStream;
begin
  S := TisStream.CreateFromFile(AFileName);
  AttachStream(ExtractFileName(AFileName), S);
end;

function TevMailMessage.Attachments: IisListOfValues;
begin
  Result := FAttachments;
end;

procedure TevMailMessage.AttachStream(const AName: String; const AStream: IisStream);
begin
  FAttachments.AddValue(AName, AStream);
end;

procedure TevMailMessage.DoOnConstruction;
begin
  inherited;
  FPriority := mpNormal;
  FAttachments := TisListOfValues.Create;
end;

function TevMailMessage.GetAddressBCC: String;
begin
  Result := FAddressBCC;
end;

function TevMailMessage.GetAddressCC: String;
begin
  Result := FAddressCC;
end;

function TevMailMessage.GetAddressFrom: String;
begin
  Result := FAddressFrom; 
end;

function TevMailMessage.GetAddressTo: String;
begin
  Result := FAddressTo;
end;

function TevMailMessage.GetBody: String;
begin
  Result := FBody;
end;

function TevMailMessage.GetConfirmation: Boolean;
begin
  Result := FConfirmation;
end;

function TevMailMessage.GetPriority: TisMailPriority;
begin
  Result := FPriority;
end;

function TevMailMessage.GetSubject: String;
begin
  Result := FSubject;
end;

class function TevMailMessage.GetTypeID: String;
begin
  Result := 'MailMessage';
end;

procedure TevMailMessage.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FAddressFrom := AStream.ReadString;
  FAddressTo := AStream.ReadString;
  FAddressCC := AStream.ReadString;
  FAddressBCC := AStream.ReadString;
  FPriority := TisMailPriority(AStream.ReadByte);
  FConfirmation := AStream.ReadBoolean;
  FSubject := AStream.ReadShortString;
  FBody := AStream.ReadString;
  FAttachments.ReadFromStream(AStream);
end;

procedure TevMailMessage.SetAddressBCC(const AValue: String);
begin
  FAddressBCC := AValue;
end;

procedure TevMailMessage.SetAddressCC(const AValue: String);
begin
  FAddressCC := AValue;
end;

procedure TevMailMessage.SetAddressFrom(const AValue: String);
begin
  FAddressFrom := AValue;
end;

procedure TevMailMessage.SetAddressTo(const AValue: String);
begin
  FAddressTo := AValue;
end;

procedure TevMailMessage.SetBody(const AValue: String);
begin
  FBody := AValue;
end;

procedure TevMailMessage.SetConfirmation(const AValue: Boolean);
begin
  FConfirmation := AValue;
end;

procedure TevMailMessage.SetPriority(const AValue: TisMailPriority);
begin
  FPriority := AValue;
end;

procedure TevMailMessage.SetSubject(const AValue: String);
begin
  FSubject := AValue;
end;

procedure TevMailMessage.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FAddressFrom);
  AStream.WriteString(FAddressTo);
  AStream.WriteString(FAddressCC);
  AStream.WriteString(FAddressBCC);
  AStream.WriteByte(Ord(FPriority));
  AStream.WriteBoolean(FConfirmation);
  AStream.WriteShortString(FSubject);
  AStream.WriteString(FBody);
  FAttachments.WriteToStream(AStream);
end;

{ TevVMRPrinterBinInfo }

function TevVMRPrinterBinInfo.GetMediaType: String;
begin
  Result := FMediaType;
end;

function TevVMRPrinterBinInfo.GetPaperWeight: Double;
begin
  Result := FPaperWeight;
end;

function TevVMRPrinterBinInfo.GetSbPaperNbr: Integer;
begin
  Result := FSbPaperNbr;
end;

class function TevVMRPrinterBinInfo.GetTypeID: String;
begin
  Result := 'VMRPrinterBinInfo';
end;

procedure TevVMRPrinterBinInfo.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FSbPaperNbr := AStream.ReadInteger;
  FMediaType := AStream.ReadShortString;
  FPaperWeight := AStream.ReadDouble;
end;


procedure TevVMRPrinterBinInfo.SetMediaType(const AValue: String);
begin
  FMediaType := AValue;
end;

procedure TevVMRPrinterBinInfo.SetPaperWeight(const AValue: Double);
begin
  FPaperWeight := AValue;
end;

procedure TevVMRPrinterBinInfo.SetSbPaperNbr(const AValue: Integer);
begin
  FSbPaperNbr := AValue;
end;

procedure TevVMRPrinterBinInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteInteger(FSbPaperNbr);
  AStream.WriteShortString(FMediaType);
  AStream.WriteDouble(FPaperWeight);
end;

{ TevVMRPrinterInfo }

function TevVMRPrinterInfo.AddBin: IevVMRPrinterBinInfo;
begin
  Result := TevVMRPrinterBinInfo.Create;
  AddChild(Result as IisNamedObject);
end;

function TevVMRPrinterInfo.BinCount: Integer;
begin
  Result := ChildCount;
end;

procedure TevVMRPrinterInfo.DeleteBin(const ABin: IevVMRPrinterBinInfo);
begin
  RemoveChild(ABin as IisInterfacedObject);
end;

function TevVMRPrinterInfo.FindBin(const AName: String): IevVMRPrinterBinInfo;
begin
  Result := FindChildByName(AName) as IevVMRPrinterBinInfo;
end;

function TevVMRPrinterInfo.FindBinByMediaType(const AMediaType: string): IevVMRPrinterBinInfo;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to BinCount - 1 do
    if GetBin(i).MediaType = AMediaType then
    begin
      Result := GetBin(i);
      break;
    end;
end;

function TevVMRPrinterInfo.FindBinNameByMediaType(const AMediaType: String): String;
var
  B: IevVMRPrinterBinInfo;
begin
  B := FindBinByMediaType(AMediaType);
  if Assigned(B) then
    Result := B.Name
  else
    Result := '';
end;

function TevVMRPrinterInfo.GetActive: Boolean;
begin
  Result := FActive;
end;

function TevVMRPrinterInfo.GetBin(Index: Integer): IevVMRPrinterBinInfo;
begin
  Result := GetChild(Index) as IevVMRPrinterBinInfo;
end;

function TevVMRPrinterInfo.GetHOffset: Integer;
begin
  Result := FHOffset;
end;

function TevVMRPrinterInfo.GetLocation: String;
begin
  Result := FLocation;
end;

function TevVMRPrinterInfo.GetTag: String;
begin
  Result := FTag;
end;

class function TevVMRPrinterInfo.GetTypeID: String;
begin
  Result := 'VMRPrinterInfo';
end;

function TevVMRPrinterInfo.GetVOffset: Integer;
begin
  Result := FVOffset;
end;

procedure TevVMRPrinterInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
  FLocation := AStream.ReadShortString;
  FActive   := AStream.ReadBoolean;
  FVOffset := AStream.ReadInteger;
  FHOffset := AStream.ReadInteger;
  FTag     :=  AStream.ReadShortString;
end;

procedure TevVMRPrinterInfo.SetActive(const AValue: Boolean);
begin
  FActive := AValue;
end;

procedure TevVMRPrinterInfo.SetHOffset(const AValue: Integer);
begin
  FHOffset := AValue;
end;

procedure TevVMRPrinterInfo.SetLocation(const AValue: String);
begin
  FLocation := AValue;
end;

procedure TevVMRPrinterInfo.SetTag(const AValue: String);
begin
  FTag := AValue;
end;

procedure TevVMRPrinterInfo.SetVOffset(const AValue: Integer);
begin
  FVOffset := AValue;
end;

procedure TevVMRPrinterInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetName);
  AStream.WriteShortString(FLocation);
  AStream.WriteBoolean(FActive);
  AStream.WriteInteger(FVOffset);
  AStream.WriteInteger(FHOffset);
  AStream.WriteShortString(FTag);
end;

{ TevVMRPrintersList }

function TevVMRPrintersList.Add: IevVMRPrinterInfo;
begin
  Result := TevVMRPrinterInfo.Create;
  AddChild(Result as IisNamedObject);
end;

function TevVMRPrintersList.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevVMRPrintersList.Delete(const AItem: IevVMRPrinterInfo);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevVMRPrintersList.FindPrinter(const AName: String): IevVMRPrinterInfo;
begin
  Result := FindChildByName(AName) as IevVMRPrinterInfo;
end;

function TevVMRPrintersList.GetItem(Index: Integer): IevVMRPrinterInfo;
begin
  Result := GetChild(Index) as IevVMRPrinterInfo;
end;

class function TevVMRPrintersList.GetTypeID: String;
begin
  Result := 'VMRPrintersList';
end;

{ TevDataChangeItem }

function TevDataChangeItem.ChangeType: TevDataChangeType;
begin
  Result := FChangeType;
end;

constructor TevDataChangeItem.Create(const AChangeType: TevDataChangeType; const ADataSource: String);
begin
  inherited Create;
  FChangeType := AChangeType;
  FDataSource := ADataSource;
end;

function TevDataChangeItem.DataSource: String;
begin
  Result := FDataSource;
end;

function TevDataChangeItem.Fields: IisListOfValues;
begin
  if not Assigned(FFields) then
    FFields := TisListOfValues.Create;
  Result := FFields;
end;

function TevDataChangeItem.GetKey: Integer;
var
  v: Variant;
begin
  if FChangeType = dctInsertRecord then
  begin
    v := Fields.TryGetValue(FDataSource + '_NBR', 0);
    if VarIsNull(v) then
      Result := 0
    else
      Result := v;
  end
  else
    Result := FKey;
end;

class function TevDataChangeItem.GetTypeID: String;
begin
  Result := 'DataChangeItem';
end;

procedure TevDataChangeItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FChangeType := TevDataChangeType(AStream.ReadByte);
  FDataSource := AStream.ReadShortString;

  if FChangeType in [dctUpdateFields, dctDeleteRecord] then
    FKey := AStream.ReadInteger
  else
    FKey := 0;

  if FChangeType in [dctUpdateFields, dctInsertRecord] then
    FFields := AStream.ReadObject(FFields) as IisListOfValues
  else
    FFields := nil;
end;

procedure TevDataChangeItem.SetKey(const AValue: Integer);
begin
  if FChangeType = dctInsertRecord then
    Fields.AddValue(FDataSource + '_NBR', AValue)
  else
    FKey := AValue;
end;

procedure TevDataChangeItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;

  AStream.WriteByte(Ord(FChangeType));
  AStream.WriteShortString(FDataSource);

  if FChangeType in [dctUpdateFields, dctDeleteRecord] then
    AStream.WriteInteger(FKey);

  if FChangeType in [dctUpdateFields, dctInsertRecord] then
  begin
    if FFields.Count = 0 then
      FFields := nil;
    AStream.WriteObject(FFields);
  end;
end;

{ TevDataChangePacket }

function TevDataChangePacket.Count: Integer;
begin
  Result := ChildCount;
end;

function TevDataChangePacket.GetAsOfDate: TDateTime;
begin
  Result := FAsOfDate;
end;

function TevDataChangePacket.GetItem(Index: Integer): IevDataChangeItem;
begin
  Result := GetChild(Index) as IevDataChangeItem;
end;

class function TevDataChangePacket.GetTypeID: String;
begin
  Result := 'DataChangePacket';
end;

procedure TevDataChangePacket.MergePacket(const APacket: IevDataChangePacket);
begin
  CheckCondition(DateOf(GetAsOfDate) = DateOf(APacket.AsOfDate), 'You cannot merge packages if one of them is AsOfDate change');
  while APacket.Count > 0 do
    (APacket[0] as IisInterfacedObject).Owner := Self;
end;

function TevDataChangePacket.NewDeleteRecord(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
begin
  Result := TevDataChangeItem.Create(dctDeleteRecord, ADataSource);
  Result.Key := AKey;
  AddChild(Result as IisInterfacedObject);
end;

function TevDataChangePacket.NewInsertRecord(const ADataSource: String): IevDataChangeItem;
begin
  Result := TevDataChangeItem.Create(dctInsertRecord, ADataSource);
  AddChild(Result as IisInterfacedObject);
end;

function TevDataChangePacket.NewUpdateFields(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
begin
  Result := TevDataChangeItem.Create(dctUpdateFields, ADataSource);
  Result.Key := AKey;
  AddChild(Result as IisInterfacedObject);
end;

procedure TevDataChangePacket.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  if ARevision > 0 then
    FAsOfDate := AStream.ReadDouble;
end;

procedure TevDataChangePacket.RemoveItem(const AItem: IevDataChangeItem);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevDataChangePacket.Revision: TisRevision;
begin
  Result := 1;
end;

procedure TevDataChangePacket.SetAsOfDate(const AValue: TDateTime);
begin
  FAsOfDate := DateOf(AValue);
end;

procedure TevDataChangePacket.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteDouble(FAsOfDate);
end;

{ TevADRDBInfo }

function TevADRDBInfo.GetDatabase: String;
begin
  Result := FDatabase;
end;

class function TevADRDBInfo.GetTypeID: String;
begin
  Result := 'ADRDBInfo';
end;

function TevADRDBInfo.GetLastTransactionNbr: Integer;
begin
  Result := FLastTransactionNbr;
end;

procedure TevADRDBInfo.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FDatabase := AStream.ReadShortString;
  FVersion := AStream.ReadShortString;
  FLastTransactionNbr := AStream.ReadInteger;
  FStatus := TevADRDBStatus(AStream.ReadByte);
end;

function TevADRDBInfo.GetStatus: TevADRDBStatus;
begin
  Result := FStatus;
end;

function TevADRDBInfo.GetVersion: String;
begin
  Result := FVersion;
end;

procedure TevADRDBInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FDatabase);
  AStream.WriteShortString(FVersion);
  AStream.WriteInteger(FLastTransactionNbr);
  AStream.WriteByte(Ord(FStatus));      
end;

procedure TevADRDBInfo.SetDatabase(const AValue: String);
begin
  FDatabase := AValue;
end;

procedure TevADRDBInfo.SetLastTransactionNbr(const AValue: Integer);
begin
  FLastTransactionNbr := AValue;
end;

procedure TevADRDBInfo.SetStatus(const AValue: TevADRDBStatus);
begin
  FStatus := AValue;
end;

procedure TevADRDBInfo.SetVersion(const AValue: String);
begin
  FVersion := AValue;
end;

constructor TevADRDBInfo.Create(const ADatabase: String);
begin
  inherited Create;
  FDatabase := ADatabase;
end;


{ TevTaskCompanyFilterItem }

function TevACATaskCompanyFilterItem.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevACATaskCompanyFilterItem.GetCoFilter: String;
begin
  Result := FCoFilter;
end;

class function TevACATaskCompanyFilterItem.GetTypeID: String;
begin
  Result := 'ACATskCoFltItem';
end;

procedure TevACATaskCompanyFilterItem.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoFilter := AStream.ReadString;
end;

procedure TevACATaskCompanyFilterItem.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevACATaskCompanyFilterItem.SetCoFilter(const AValue: String);
begin
  FCoFilter := AValue;
end;

procedure TevACATaskCompanyFilterItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteString(FCoFilter);
end;

{ TevTaskCompanyFilter }

function TevACATaskCompanyFilter.Add: IevACATaskCompanyFilterItem;
begin
  Result := TevACATaskCompanyFilterItem.Create(Self, False);
end;

procedure TevACATaskCompanyFilter.Clear;
begin
  RemoveAllChildren;
end;

function TevACATaskCompanyFilter.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevACATaskCompanyFilter.Delete(
  const AItem: IevACATaskCompanyFilterItem);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevACATaskCompanyFilter.GetItem(AIndex: Integer): IevACATaskCompanyFilterItem;
begin
  Result := GetChild(AIndex) as IevACATaskCompanyFilterItem;
end;

class function TevACATaskCompanyFilter.GetTypeID: String;
begin
  Result := 'ACATskCoFilter';
end;

{ TevACATaskEEItem }

function TevACATaskEEItem.GetCO_NBR: Integer;
begin
 Result:= FCO_NBR;
end;

function TevACATaskEEItem.GetCUSTOM_EMPLOYEE_NUMBER: string;
begin
 Result:= FCUSTOM_EMPLOYEE_NUMBER;
end;

function TevACATaskEEItem.GetEE_NBR: Integer;
begin
 Result:= FEE_NBR;
end;

function TevACATaskEEItem.GetFIRST_NAME: string;
begin
 Result:= FFIRST_NAME;
end;

function TevACATaskEEItem.GetLAST_NAME: string;
begin
 Result:= FLAST_NAME;
end;

function TevACATaskEEItem.GetNEW_ACA_STATUS: string;
begin
 Result:= FNEW_ACA_STATUS;
end;

function TevACATaskEEItem.GetOLD_ACA_STATUS: string;
begin
 Result:= FOLD_ACA_STATUS;
end;

class function TevACATaskEEItem.GetTypeID: String;
begin
  Result := 'ACATskEEITem';
end;

function TevACATaskEEItem.GetUpdateType: string;
begin
 Result:= FUpdateType;
end;

procedure TevACATaskEEItem.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FCO_NBR := AStream.ReadInteger;  
  FEE_NBR := AStream.ReadInteger;
  FCUSTOM_EMPLOYEE_NUMBER:= AStream.ReadShortString;
  FFIRST_NAME := AStream.ReadShortString;
  FLAST_NAME := AStream.ReadShortString;
  FOLD_ACA_STATUS := AStream.ReadShortString;
  FNEW_ACA_STATUS := AStream.ReadShortString;
  FUpdateType := AStream.ReadShortString;
end;

procedure TevACATaskEEItem.SetCO_NBR(const AValue: Integer);
begin
 FCO_NBR:= AValue;
end;

procedure TevACATaskEEItem.SetCUSTOM_EMPLOYEE_NUMBER(const AValue: string);
begin
 FCUSTOM_EMPLOYEE_NUMBER:= Avalue;
end;

procedure TevACATaskEEItem.SetEE_NBR(const AValue: Integer);
begin
 FEE_NBR:= AValue;
end;

procedure TevACATaskEEItem.SetFIRST_NAME(const AValue: string);
begin
 FFIRST_NAME:= AValue;
end;

procedure TevACATaskEEItem.SetLAST_NAME(const AValue: string);
begin
 FLAST_NAME:= AValue;
end;

procedure TevACATaskEEItem.SetNEW_ACA_STATUS(const AValue: string);
begin
 FNEW_ACA_STATUS:= Avalue;
end;

procedure TevACATaskEEItem.SetOLD_ACA_STATUS(const AValue: string);
begin
 FOLD_ACA_STATUS:= Avalue;
end;

procedure TevACATaskEEItem.SetUpdateType(const AValue: string);
begin
 FUpdateType := Avalue;
end;

procedure TevACATaskEEItem.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FCO_NBR);
  AStream.WriteInteger(FEE_NBR);
  AStream.WriteShortString(FCUSTOM_EMPLOYEE_NUMBER);
  AStream.WriteShortString(FFIRST_NAME);
  AStream.WriteShortString(FLAST_NAME);
  AStream.WriteShortString(FOLD_ACA_STATUS);
  AStream.WriteShortString(FNEW_ACA_STATUS);
  AStream.WriteShortString(FUpdateType);

end;

{ TevACATaskCompanyEE }

function TevACATaskCompanyEE.Add: IevACATaskEEItem;
begin
  Result := TevACATaskEEItem.Create(Self, False);
end;

procedure TevACATaskCompanyEE.Clear;
begin
   RemoveAllChildren;
end;

function TevACATaskCompanyEE.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevACATaskCompanyEE.Delete(const AItem: IevACATaskEEItem);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevACATaskCompanyEE.GetItem(AIndex: Integer): IevACATaskEEItem;
begin
  Result := GetChild(AIndex) as IevACATaskEEItem;
end;

class function TevACATaskCompanyEE.GetTypeID: String;
begin
  Result := 'ACATskCompanyEE';
end;

function TevACATaskCompanyEE.GetUpdateDate: TDateTime;
begin
 Result:= FUpdateDate
end;

procedure TevACATaskCompanyEE.SetUpdateDate(const AValue: TDateTime);
begin
 FUpdateDate:= AValue;
end;

procedure TevACATaskCompanyEE.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FUpdateDate := AStream.ReadDouble;
  FClNbr:= AStream.ReadInteger;
end;

procedure TevACATaskCompanyEE.WriteSelfToStream(
  const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteDouble(FUpdateDate);
  AStream.WriteInteger(FClNbr);
end;

function TevACATaskCompanyEE.GetClNbr: Integer;
begin
 Result:= FClNbr;
end;

procedure TevACATaskCompanyEE.SetClNbr(const AValue: Integer);
begin
 FClNbr:= Avalue;
end;

{ TevACATaskResult }


function TevACATaskResult.Add: IevACATaskCompanyEE;
begin
  Result := TevACATaskCompanyEE.Create(Self, False);
end;

procedure TevACATaskResult.Clear;
begin
   RemoveAllChildren;
end;

function TevACATaskResult.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevACATaskResult.Delete(const AItem: IevACATaskCompanyEE);
begin
  RemoveChild(AItem as IisInterfacedObject);
end;

function TevACATaskResult.GetItem(AIndex: Integer): IevACATaskCompanyEE;
begin
  Result := GetChild(AIndex) as IevACATaskCompanyEE;
end;

class function TevACATaskResult.GetTypeID: String;
begin
  Result := 'ACATskResult';
end;

initialization
  ObjectFactory.Register([TevTaskInfo, TevTaskRequestInfo, TevGlobalFlagInfo, TevTaxPaymentsTaskParam,
                          TevTaxPaymentsTaskParams, TevACHOptions, TevMachineInfo,
                          TevPreprocessQuarterEndTaskFilterItem, TevPreprocessQuarterEndTaskFilter,
                          TevPreprocessQuarterEndTaskResultItem, TevPreprocessQuarterEndTaskResult,
                          TevQECTaskFilterItem, TevQECTaskFilter,
                          TevQECTaskResultItem, TevQECTaskResult,
                          TevSecAccountRights, TevSecElements, TevSecClients, TevSecFields, TevSecRecords,
                          TevSecurityStructure, TevUserAccount, TevPrinterInfo, TevPrintersList,
                          TevMailMessage, TevVMRPrinterBinInfo, TevVMRPrinterInfo, TevVMRPrintersList,
                          TevDataChangeItem, TevDataChangePacket, TevADRDBInfo,
                          TevACATaskCompanyFilter, TevACATaskCompanyFilterItem,
                          TevACATaskResult, TevACATaskCompanyEE, TevACATaskEEItem]);


finalization
  SafeObjectFactoryUnRegister([TevTaskInfo, TevTaskRequestInfo, TevGlobalFlagInfo, TevTaxPaymentsTaskParam,
                            TevTaxPaymentsTaskParams, TevACHOptions, TevMachineInfo,
                            TevPreprocessQuarterEndTaskFilterItem, TevPreprocessQuarterEndTaskFilter,
                            TevPreprocessQuarterEndTaskResultItem, TevPreprocessQuarterEndTaskResult,
                            TevQECTaskFilterItem, TevQECTaskFilter,
                            TevQECTaskResultItem, TevQECTaskResult,
                            TevSecAccountRights, TevSecElements, TevSecClients, TevSecFields, TevSecRecords,
                            TevSecurityStructure, TevUserAccount, TevPrinterInfo, TevPrintersList,
                            TevMailMessage, TevVMRPrinterBinInfo, TevVMRPrinterInfo, TevVMRPrintersList,
                            TevDataChangeItem, TevDataChangePacket, TevADRDBInfo,
                            TevACATaskCompanyFilter, TevACATaskCompanyFilterItem,
                            TevACATaskResult, TevACATaskCompanyEE, TevACATaskEEItem]);



end.

