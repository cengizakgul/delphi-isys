unit EvBlobViewerFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  ExtCtrls, ComCtrls, ISZippingRoutines;

type
  TevBlobViewer = class(TFrame)
    imgBlob: TevImage;
    memBlob: TevMemo;
  private
    FData: IisStream;
    function  TryToDecompress(const AData: IisStream): IIsStream;
    function  TryAsBMP: Boolean;
    procedure ShowAsText;
  public
    procedure ShowBlob(const AData: IisStream);
  end;

implementation

{$R *.dfm}

{ TevBlobViewer }

procedure TevBlobViewer.ShowBlob(const AData: IisStream);
begin
  imgBlob.Hide;
  memBlob.Hide;

  if Assigned(AData) then
  begin
    FData := TryToDecompress(AData);
    if not TryAsBMP then
      ShowAsText;
  end
  else memBlob.Show;  //done in the event that the text is empty
end;

function TevBlobViewer.TryToDecompress(const AData: IisStream): IIsStream;
begin
  try
    Result := TisStream.Create(AData.Size);
    InflateStream(AData.RealStream, Result.RealStream);
  except
     Result := AData;
  end;
end;

function TevBlobViewer.TryAsBMP: Boolean;
begin
  Result := False;
  try
    FData.RealStream.Position := 0;
    imgBlob.Picture.Bitmap.LoadFromStream(FData.RealStream);
    imgBlob.Show;
    Result := True;
  except
  end;
end;

procedure TevBlobViewer.ShowAsText;
begin
  memBlob.Text := FData.AsString;
  memBlob.Show;
end;

end.

