// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SECURITY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ImgList, sSecurityClassDefs, Menus, 
  ExtCtrls, SDataStructure, wwdblook, Buttons, SDataDictbureau, SDDClasses,
  SDataDictsystem, ISBasicClasses, EvContext, EvConsts, EvCommonInterfaces,
  EvUtils, EvStreamUtils, EvUIUtils, EvUIComponents, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  LinkItem = record
    Atom: TSecurityAtom;
    Node: TTreeNode;
  end;
  TEDIT_SECURITY = class(TForm)
    TV: TevTreeView;
    btnSave: TevBitBtn;
    btnCancel: TevBitBtn;
    IL: TImageList;
    btnShowTopLevel: TevBitBtn;
    PopupMenu: TPopupMenu;
    PopupIL: TImageList;
    btnUndo: TevBitBtn;
    EvBevel1: TEvBevel;
    EvBevel2: TEvBevel;
    evGroupBox1: TevGroupBox;
    tabTopType: TevTabControl;
    EvBevel3: TEvBevel;
    EvBevel4: TEvBevel;
    cbBriefMode: TCheckBox;
    lpSystemTemplates: TevDBLookupCombo;
    evLabel1: TevLabel;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel2: TevLabel;
    lpSbTemplates: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    IL2: TImageList;
    StaticIL: TImageList;
    btnResetRights: TevBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnShowTopLevelClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
    procedure TVExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure TVCollapsed(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TVMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnUndoClick(Sender: TObject);
    procedure tabTopTypeChange(Sender: TObject);
    procedure lpSystemTemplatesChange(Sender: TObject);
    procedure cbBriefModeClick(Sender: TObject);
    procedure tabTopTypeGetImageIndex(Sender: TObject; TabIndex: Integer;
      var ImageIndex: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure TVAdvancedCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; Stage: TCustomDrawStage;
      var PaintImages, DefaultDraw: Boolean);
    procedure btnResetRightsClick(Sender: TObject);
  private
    { Private declarations }
    FSaveBtnEnabled: Boolean;
    Links: array of LinkItem;
    FReservCollection: TSecurityAtomCollection;
    FAccountGroupRights: IevSecAccountRights;
    procedure AddNode(const n: TTreeNode; const Atom: TSecurityAtom);
    procedure DeleteNode(const n: TTreeNode);
    procedure RefreshNode(const n: TTreeNode);
    procedure ReactOnPopupMenuItemClick(Sender: TObject);
    procedure ReactOnPopupGeneralMenuItemClick(Sender: TObject);
    procedure ReactOnPopupTakeOverGroupClick(Sender: TObject);
    procedure ReactOnAtomContextChange(const Atom: TSecurityAtom);
    procedure SaveACopy;
    procedure RestoreACopy;
    function  AtomIsAlreadyShown(const ParentNode: TTreeNode; const Atom: TSecurityAtom): Boolean;
    procedure TakeOverGroupRights(const a: TSecurityAtom);
  public
    { Public declarations }
    SBUserNbr: Integer;
    Collection: TSecurityAtomCollection;
  end;

var
  EDIT_SECURITY: TEDIT_SECURITY;

implementation

uses sSecurityInterface, Db;

type
  TSecTreeNode = class(TTreeNode)
  private
    FFromGroup: Boolean;
  end;

{$R *.DFM}

procedure TEDIT_SECURITY.FormShow(Sender: TObject);
begin
//  cbBriefMode.Visible := (GetAsyncKeyState(VK_CONTROL) and $8000) <> 0;    Very weird stuff...
  btnResetRights.Visible := SBUserNbr > 0;
  FSaveBtnEnabled := btnSave.Enabled;
  btnSave.Enabled := False;
  btnShowTopLevel.Click;
  FReservCollection := nil;
end;

procedure TEDIT_SECURITY.btnShowTopLevelClick(Sender: TObject);
var
  i: Integer;
  s: array [0..3] of ShortString;
begin
  s[0] := atMenu;
  s[1] := atScreen;
  s[2] := atFunc;
  s[3] := atDataset;
  ctx_StartWait;
  TV.Items.BeginUpdate;
  try
    for i := TV.Items.Count - 1 downto 0 do
      DeleteNode(TV.Items[i]);
    TV.Items.Clear;
    for i := 0 to Collection.AtomCount- 1 do
    begin
      if Collection.Atoms[i].AtomType = s[tabTopType.TabIndex] then
        AddNode(nil, Collection.Atoms[i]);
    end;
    TV.AlphaSort;
  finally
    TV.Items.EndUpdate;
    ctx_EndWait;
  end;
end;

procedure TEDIT_SECURITY.AddNode(const n: TTreeNode;
  const Atom: TSecurityAtom);
var
  o: TTreeNode;
  ic: TIcon;
  i: Integer;
begin
  if (Atom.ContextCount > 1)
  or ((Atom.ContextCount = 1) and (Atom.Contexts[0].Tag <> ctDisabled)) then
  begin
    o := TSecTreeNode.Create(TV.Items);
    TV.Items.AddNode(o, n, '', Atom, naAddChild);
    ic := TIcon.Create;
    try
      IL.GetIcon(0, ic);
      o.ImageIndex := IL.AddIcon(ic);
    finally
      ic.Free
    end;
    Atom.OnContextChange := ReactOnAtomContextChange;
    i := High(Links) + 1;
    SetLength(Links, i + 1);
    Links[i].Atom := Atom;
    Links[i].Node := o;
    RefreshNode(o);
  end;
end;

procedure TEDIT_SECURITY.RefreshNode(const n: TTreeNode);

  procedure AddStatusToString(var s: string; const StatusName: ShortString; const i: Integer);
  begin
    if i > 0 then
    begin
      if s <> '' then
        s := s+ '; ';
      s := s+ StatusName+ ': '+ IntToStr(i);
    end;
  end;

var
  o: TSecurityAtom;
  i: Integer;
  ic: TIcon;
  iEnabled, iDisabled, iReadOnly, iOther: Integer;
  bFromGroup: Boolean;
  sCurrentTag: ShortString;
  s: string;
begin
  o := TSecurityAtom(n.Data);
  n.Text := o.Caption + ' (' + o.CurrentContext.Caption + ')';
  i := n.ImageIndex;
  ic := TIcon.Create;
  try
    if o.CurrentContext.Tag = ctDisabled then
      IL.GetIcon(0, ic)
    else
    if o.CurrentContext.Tag = ctEnabled then
      IL.GetIcon(2, ic)
    else
      IL.GetIcon(1, ic);
    IL.ReplaceIcon(i, ic);
  finally
    ic.Free
  end;
  n.SelectedIndex := n.ImageIndex;
  n.HasChildren := False;
  iEnabled := 0;
  iDisabled := 0;
  iReadOnly := 0;
  iOther := 0;
  bFromGroup := o.CurrentContext.IsFromGroup;

  // TODO !!!
  // THERE IS A BUG IN SECURITY STRUCTURE!
  // Sometimes items returned by GetLinkedAtoms are already destroyed!
  with o.GetLinkedAtoms(not cbBriefMode.Checked) do
  try
    for i := 0 to AtomCount-1 do
      if not AtomIsAlreadyShown(n, Atoms[i]) and
         ((Atoms[i].ContextCount > 1) or ((Atoms[i].ContextCount = 1) and (Atoms[i].Contexts[0].Tag <> ctDisabled))) then
      begin
        n.HasChildren := True;
        try
          Atoms[i].ClassName;  // it's dead object
        except
          raise; // just to show up the problem
        end;
        sCurrentTag := Atoms[i].CurrentContext.Tag;
        if sCurrentTag = ctDisabled then
          Inc(iDisabled)
        else if sCurrentTag = ctEnabled then
          Inc(iEnabled)
        else if sCurrentTag = ctReadOnly then
          Inc(iReadOnly)
        else
          Inc(iOther);

        bFromGroup :=  bFromGroup and Atoms[i].CurrentContext.IsFromGroup;
      end;
  finally
    Free
  end;
  s := '';
  AddStatusToString(s, 'Enabled', iEnabled);
  AddStatusToString(s, 'Read-only', iReadOnly);
  AddStatusToString(s, 'Disabled', iDisabled);
  AddStatusToString(s, 'Other', iOther);

  TSecTreeNode(n).FFromGroup := bFromGroup;
  
  if s <> '' then
    n.Text := n.Text+ ' ('+ s+ ')';

  if o.CurrentContext.IsFromGroup then
    n.Text := n.Text + ' [FROM GROUP]'
  else if SBUserNbr > 0 then
    n.Text := n.Text + ' [FROM USER]';  

  if Assigned(n.Parent) then
    RefreshNode(n.Parent);
end;

procedure TEDIT_SECURITY.PopupMenuPopup(Sender: TObject);
var
  o: TTreeView;
  a: TSecurityAtom;
  i, j: Integer;
  e: TMenuItem;
  m: TPopupMenu;
  ic: TIcon;
begin
  o := TV;
  m := TPopupMenu(Sender);
  m.Items.Clear;
  m.Images.Clear;

  if o.Selected = nil then
    Exit;

  a := TSecurityAtom(o.Selected.Data);
  for i := 0 to a.ContextCount - 1 do
  begin
    e := TMenuItem.Create(m);
    e.Caption := a.Contexts[i].Caption;
    e.Default := a.Contexts[i] = a.CurrentContext;
    ic := TIcon.Create;
    try
      if a.Contexts[i].Tag = ctDisabled then
        IL.GetIcon(0, ic)
      else
      if a.Contexts[i].Tag = ctEnabled then
        IL.GetIcon(2, ic)
      else
        IL.GetIcon(1, ic);
      e.ImageIndex := m.Images.AddIcon(ic);
    finally
      ic.Free
    end;
    e.Tag := i;
    e.OnClick := ReactOnPopupMenuItemClick;
    if (m.Items.Count = 0)
    or (a.Contexts[m.Items[m.Items.Count-1].Tag].Priority >= a.Contexts[i].Priority) then
      m.Items.Add(e)
    else
      for j := 0 to m.Items.Count-1 do
        if (a.Contexts[m.Items[j].Tag].Priority < a.Contexts[i].Priority) then
        begin
          m.Items.Insert(j, e);
          Break;
        end;
  end;

  if Assigned(a.GetContextByTag(ctEnabled)) then
  begin
    e := TMenuItem.Create(m);
    e.Caption := '-';
    m.Items.Add(e);
    e := TMenuItem.Create(m);
    e.Caption := 'Give max rights to all child atoms';
    e.ImageIndex := m.Images.AddImage(StaticIL, 0);
    e.OnClick := ReactOnPopupGeneralMenuItemClick;
    m.Items.Add(e);

    if (SBUserNbr > 0) and not TSecTreeNode(o.Selected).FFromGroup then
    begin
      e := TMenuItem.Create(m);
      e.Caption := 'Reset rights to group settings';
      e.ImageIndex := m.Images.AddImage(StaticIL, 1);
      e.OnClick := ReactOnPopupTakeOverGroupClick;
      m.Items.Add(e);
    end;
  end;
end;

procedure TEDIT_SECURITY.ReactOnPopupMenuItemClick(Sender: TObject);
var
  a: TSecurityAtom;
begin
  a := TSecurityAtom(TV.Selected.Data);
  SaveACopy;
  try
    a.CurrentContext := a.Contexts[TMenuItem(Sender).Tag];
    a.CurrentContext.MarkFromGroup(False);

    RefreshNode(TV.Selected);
  except
    RestoreACopy;
    raise
  end;
end;

procedure TEDIT_SECURITY.TVExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var
  a: TSecurityAtom;
  i: Integer;
begin
  AllowExpansion := False;
  TV.Items.BeginUpdate;
  try
    a := TSecurityAtom(Node.Data);
    with a.GetLinkedAtoms(not cbBriefMode.Checked) do
    try
      for i := 0 to AtomCount - 1 do
      begin
        if not AtomIsAlreadyShown(Node, Atoms[i]) then
        begin
          AddNode(Node, Atoms[i]);
          AllowExpansion := True;
        end;
      end;
    finally
      Free
    end;
    Node.AlphaSort;
  finally
    TV.Items.EndUpdate;
  end;
end;

procedure TEDIT_SECURITY.DeleteNode(const n: TTreeNode);
var
  i, j: Integer;
begin
  for i := 0 to High(Links) do
    if Links[i].Node = n then
    begin
      for j := i to High(Links) - 1 do
        Links[j] := Links[j + 1];
      SetLength(Links, High(Links));
      Break;
    end;
  n.Delete;
end;

procedure TEDIT_SECURITY.TVCollapsed(Sender: TObject; Node: TTreeNode);
var
  i: Integer;
begin
  for i := Node.Count - 1 downto 0 do
    DeleteNode(Node.Item[i]);
  RefreshNode(Node);
end;

procedure TEDIT_SECURITY.ReactOnAtomContextChange(
  const Atom: TSecurityAtom);
var
  i: Integer;
begin
  for i := 0 to High(Links) do
    if Links[i].Atom = Atom then
      RefreshNode(Links[i].Node);
end;

procedure TEDIT_SECURITY.FormCreate(Sender: TObject);
begin
  SetLength(Links, 0);
  ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_SEC_TEMPLATES, DM_SERVICE_BUREAU.SB_SEC_TEMPLATES]);
end;

procedure TEDIT_SECURITY.FormDestroy(Sender: TObject);
begin
  Links := nil;
  if Assigned(FReservCollection) then
  begin
    FReservCollection.Free;
    FReservCollection := nil;
  end;
end;

procedure TEDIT_SECURITY.ReactOnPopupGeneralMenuItemClick(Sender: TObject);
  procedure AssignMaxContext(const a: TSecurityAtom);
  var
    i, j, k: Integer;
  begin
    j := -1;
    k := -1;
    for i := 0 to a.ContextCount-1 do
      if a.Contexts[i].Priority > j then
      begin
        j := a.Contexts[i].Priority;
        k := i;
      end;
    if k <> -1 then
    begin
      a.CurrentContext := a.Contexts[k];
      a.CurrentContext.MarkFromGroup(False);
    end;
  end;
var
  a: TSecurityAtom;
  i: Integer;
begin
  a := TSecurityAtom(TV.Selected.Data);
  SaveACopy;
  try
    AssignMaxContext(a);
    with a.GetLinkedAtoms do
    try
      for i := 0 to AtomCount - 1 do
        AssignMaxContext(Atoms[i]);
    finally
      Free
    end;
    RefreshNode(TV.Selected);
  except
    RestoreACopy;
    raise;
  end;
end;

procedure TEDIT_SECURITY.TVMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    TV.Selected := TV.GetNodeAt(X, Y);
end;

procedure TEDIT_SECURITY.RestoreACopy;
var
  t: IisStream;
begin
  if Assigned(FReservCollection) then
  begin
    t := TisStream.Create;
    FReservCollection.SaveContextsToStream(t);
    t.Position := 0;
    Collection.LoadContextsFromStream(t);
    btnUndo.Enabled := False;
  end;
end;

procedure TEDIT_SECURITY.SaveACopy;
var
  t: IisStream;
begin
  btnSave.Enabled := FSaveBtnEnabled;
  if not Assigned(FReservCollection) then
    FReservCollection := TSecurityAtomCollection.Create;
  t := TisStream.Create;
  Collection.SaveContextsToStream(t);
  t.Position := 0;
  FReservCollection.LoadContextsFromStream(t);
  btnUndo.Enabled := True;
end;

procedure TEDIT_SECURITY.btnUndoClick(Sender: TObject);
begin
  RestoreACopy;
end;

function TEDIT_SECURITY.AtomIsAlreadyShown(const ParentNode: TTreeNode;
  const Atom: TSecurityAtom): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to High(Links) do
    if (Links[i].Atom = Atom) and
       (Links[i].Node.Parent <> ParentNode) then
    begin
      Result := True;
      Break;
    end;
end;

procedure TEDIT_SECURITY.tabTopTypeChange(Sender: TObject);
begin
  tabTopType.UpdateTabImages;
  tabTopType.Update;
  btnShowTopLevel.Click;
end;

procedure TEDIT_SECURITY.lpSystemTemplatesChange(Sender: TObject);
var
  m: IisStream;
begin
  if (Tag = 0) and (EvMessage('Do you want to erase current settings and apply the template?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    m := TisStream.Create;
    Tag := 1;
    ctx_StartWait;
    try
      Collection.StreamStructureTo(m);
      m.Position := 0;
      Collection.Free;
      Collection := TSecurityAtomCollection.CreateFromStream(m);
      m.Clear;
      if Sender = lpSystemTemplates then
        m := DM_SYSTEM_MISC.SY_SEC_TEMPLATES.GetBlobData('DATA')
      else
      if Sender = lpSbTemplates then
        m := DM_SERVICE_BUREAU.SB_SEC_TEMPLATES.GetBlobData('TEMPLATE')
      else
        Assert(False);
      m.Position := 0;
      Collection.LoadContextsFromStream(m);
      TevDBLookupCombo(Sender).Clear;
      btnShowTopLevel.Click;
      btnSave.Enabled := FSaveBtnEnabled;
    finally
      ctx_EndWait;
      Tag := 0;
    end;
  end;
end;

procedure TEDIT_SECURITY.cbBriefModeClick(Sender: TObject);
begin
  btnShowTopLevel.Click;
end;

procedure TEDIT_SECURITY.tabTopTypeGetImageIndex(Sender: TObject;
  TabIndex: Integer; var ImageIndex: Integer);
begin
  if tabTopType.TabIndex = TabIndex then
    ImageIndex := TabIndex* 2
  else
    ImageIndex := TabIndex* 2+ 1;
end;

procedure TEDIT_SECURITY.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #27) and not btnSave.Enabled then
    Close;
end;

procedure TEDIT_SECURITY.ReactOnPopupTakeOverGroupClick(Sender: TObject);
var
  a: TSecurityAtom;
  i: Integer;
begin
  a := TSecurityAtom(TV.Selected.Data);
  SaveACopy;
  try
    TakeOverGroupRights(a);
    with a.GetLinkedAtoms do
    try
      for i := 0 to AtomCount - 1 do
        TakeOverGroupRights(Atoms[i]);
    finally
      Free
    end;
    RefreshNode(TV.Selected);
  except
    RestoreACopy;
    raise;
  end;
end;

procedure TEDIT_SECURITY.TVAdvancedCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; Stage: TCustomDrawStage;
  var PaintImages, DefaultDraw: Boolean);
begin
  if TSecTreeNode(Node).FFromGroup then
  begin
    if not (cdsSelected in State) then
      TV.Canvas.Font.Color := clGray;
  end;
end;

procedure TEDIT_SECURITY.btnResetRightsClick(Sender: TObject);
var
  i: Integer;
begin
  if EvMessage('All user rights will be restored from group level. Continue?',
    mtConfirmation, mbOKCancel, mbCancel) = mrOK then
  begin
    SaveACopy;
    try
      for i := 0 to Collection.AtomCount- 1 do
        TakeOverGroupRights(Collection.Atoms[i]);
    except
      RestoreACopy;
      raise;
    end;
  end;
end;

procedure TEDIT_SECURITY.TakeOverGroupRights(const a: TSecurityAtom);
var
  State: Char;
begin
  // IT WORKS ONLY FOR USER WHO HAS IT ENABLED FOR SELF
  if ctx_AccountRights.GetElementState(a.AtomType[1], a.Tag) = stEnabled then
  begin
    if not Assigned(FAccountGroupRights) then
      FAccountGroupRights := ctx_Security.GetUserGroupLevelRights(SBUserNbr);

    State := FAccountGroupRights.GetElementState(a.AtomType[1], a.Tag);
    a.CurrentContext.MarkFromGroup(False);
    a.CurrentContext := a.GetContextByTag(State);
    a.CurrentContext.MarkFromGroup(True);
    ReactOnAtomContextChange(a);
  end;  
end;

end.
