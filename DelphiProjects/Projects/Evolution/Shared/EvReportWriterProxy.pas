unit EvReportWriterProxy;

interface

uses Classes, Forms, Windows, Controls, SysUtils, Variants, Messages, Dialogs, ISErrorUtils, ActiveX,
     EvStreamUtils, isUtils, isBasicUtils, EvUtils, 
     SReportSettings, EvTypes, rwPreviewContainerFrm, rwModalInputFormFrm, rwInputFormContainerFrm, SDataStructure,
     DB, EvDataAccessComponents, EvContext, isBaseClasses, isThreadManager, EvMainboard, EvInitApp,
     EvTransportInterfaces, ISZippingRoutines, evBasicUtils, isLogFile, evTransportShared,
     evTransportDatagrams, rwConnection, rwConnectionInt, EvConsts;

type
  TevRWReportInfo = record
    ReportClassName: String;
    AncestorClassName: String;
    Description: String;
    UsedLibComponents: String;
  end;

  TevRWResultPageInfo = record
    PageNumber:   Integer;
    WidthMM:      Integer;
    HeightMM:     Integer;
    Duplexing:    Boolean;
  end;


  TevRWRunReportSettings = record
    ReturnParamsBack: WordBool;
    Duplexing: WordBool;
    ShowInputForm: WordBool;
    ReturnPageInfo: Boolean;
    ReportName: String;
  end;

  TevRWResultFormatType = (rwRTUnknown, rwRTRWA, rwRTASCII, rwRTPDF, rwRTHTML, rwRTXML, rwRTXLS);

  TevRWResultInfo = record
    Data: IEvDualStream;
    ResultType: TevRWResultFormatType;
    ReturnValue: Variant;
    DefaultFileName: String;
    PageInfo: array of TevRWResultPageInfo;
  end;

  TReportTypes = set of TReportType;


  IevRWPreview = interface
  ['{19F2C272-29A8-401F-98C6-6461C9659F3E}']
    function  GetBoundsRect: TRect;
    procedure SetBoundsRect(AValue: TRect);
    property  BoundsRect: TRect read GetBoundsRect write SetBoundsRect;
  end;


  IevReportWriterProxy = interface
  ['{A8AEE7AE-B2BD-4277-B537-2964697E9200}']
    function  GetReportInfo(const Data: IEvDualStream): TevRWReportInfo;
    function  ShowDesigner(const Data: IEvDualStream; const ReportName: String; const DefaultReportType: Integer = 0): Boolean;
    function  ShowQueryBuilder(const Data: IEvDualStream; ClientNbr, CoNbr: Integer; DescriptionView: Boolean): Boolean;
    procedure RunQueryBuilderQuery(Data: IEvDualStream; ClientNbr, CoNbr: Integer; Params: TParams; ResultDataSet: TEvBasicClientDataSet);
    procedure StartSession;
    procedure EndSession;
    procedure CompileReport(Data: IEvDualStream; var RepClassName: String; var AncestorClassName: String);
    procedure Preview(Results: TrwReportResults; Parent: TrwPreviewContainer; const SecuredPreview: Boolean = False);
    procedure Print(AResults: TrwReportResults; ReportTypes: TReportTypes; PrinterInfo: TevRWPrinterRec; const PrintJobName: String);
    function  ConvertRWAtoPDF(const RWAData: IEvDualStream; const PDFInfoRec: TrwPDFDocInfo): IEvDualStream;
    function  ShowInputForm(ReportData: IEvDualStream; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
    function  ShowAncestorInputForm(const AncestorClassName: String; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
    function  ShowModalInputForm(ReportData: IEvDualStream; Params: TrwReportParams; RunTime: Boolean): TModalResult;
    function  CloseInputForm(CloseResult: TModalResult; Params: TrwReportParams): Boolean;
    procedure PrepareReport(ReportData: IEvDualStream);
    function  RunReport(const ReportData: IEvDualStream; const Params: TrwReportParams; const RunSettings: TevRWRunReportSettings): TevRWResultInfo;
    function  GetInputFormBoundRect: TRect;
    function  ClassNameToDescr(AClassName: String): string;
    function  RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function  MakeSecureRWA(DataSource: IEvDualStream; const PreviewPassword, PrintPassword: String): IEvDualStream;
    function  MergeRWResult(const AReports: TrwReportResults; const AReportType: array of TReportType; const APrinterInfo: TevRWPrinterRec; const APrintJobName: String): IEvDualStream;
    procedure StopReportExecution;
    procedure SetLowerRWEnginePriority(const AValue: Boolean);
  end;

  function GetRWProxy: IevReportWriterProxy;

implementation

uses EvCommonInterfaces;

type
  TevReportWriterProxy = class;

  TevRWPreview = class(TisInterfacedObject, IevRWPreview)
  private
    FOwner: TevReportWriterProxy;
    FPreviewConnection: IrwPreviewApp;
  protected
    function  GetBoundsRect: TRect;
    procedure SetBoundsRect(AValue: TRect);
  public
    constructor Create(const AOwner: TevReportWriterProxy; const APreviewConnection: IrwPreviewApp);
    destructor  Destroy; override;
  end;


  TevReportWriterProxy = class(TisInterfacedObject, IevReportWriterProxy)
  private
    FCachedEngine: IrwEngineApp;
    FCachedEngineHWND: HWND;
    FInputFormContainer: TrwInputFormContainer;
    FInRWSession: Boolean;

    function  GetAdapterName: String;

    function  CachedEngineConnection: IrwEngineApp;
    function  GetDesignerConnection: IrwDesignerApp;
    function  GetPreviewConnection: IrwPreviewApp;

    procedure ReleaseRWConnections;
    procedure ReturnConnectionToCache(const AConnection: IInterface);

    function  ShowInputFormInternal(Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;

    function  EvLayersIntoOleLayers(ALayers: TrwPrintableLayers): OleVariant;
    function  GetRWEngineProcessID: Cardinal;
    procedure CloseActiveReport;

    function  ShowDesigner(const Data: IEvDualStream; const ReportName: String; const DefaultReportType: Integer = 0): Boolean;
    function  ShowQueryBuilder(const Data: IEvDualStream; ClientNbr, CoNbr: Integer; DescriptionView: Boolean): Boolean;
    procedure RunQueryBuilderQuery(Data: IEvDualStream; ClientNbr, CoNbr: Integer; Params: TParams; ResultDataSet: TEvBasicClientDataSet);
    function  GetReportInfo(const Data: IEvDualStream): TevRWReportInfo;
    procedure StartSession;
    procedure EndSession;
    procedure CompileReport(Data: IEvDualStream; var RepClassName: String; var AncestorClassName: String);
    procedure Preview(Results: TrwReportResults; Parent: TrwPreviewContainer; const SecuredPreview: Boolean = False);
    procedure Print(AResults: TrwReportResults; ReportTypes: TReportTypes; PrinterInfo: TevRWPrinterRec; const PrintJobName: String);
    function  ConvertRWAtoPDF(const RWAData: IEvDualStream; const PDFInfoRec: TrwPDFDocInfo): IEvDualStream;
    function  ShowInputForm(ReportData: IEvDualStream; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
    function  ShowAncestorInputForm(const AncestorClassName: String; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
    function  ShowModalInputForm(ReportData: IEvDualStream; Params: TrwReportParams; RunTime: Boolean): TModalResult;
    function  CloseInputForm(CloseResult: TModalResult; Params: TrwReportParams): Boolean;
    procedure PrepareReport(ReportData: IEvDualStream);
    function  RunReport(const ReportData: IEvDualStream; const Params: TrwReportParams; const RunSettings: TevRWRunReportSettings): TevRWResultInfo;
    function  GetInputFormBoundRect: TRect;
    function  ClassNameToDescr(AClassName: String): string;
    function  RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function  MakeSecureRWA(DataSource: IEvDualStream; const PreviewPassword, PrintPassword: String): IEvDualStream;
    function  MergeRWResult(const AReports: TrwReportResults; const AReportType: array of TReportType; const APrinterInfo: TevRWPrinterRec; const APrintJobName: String): IEvDualStream;
    procedure StopReportExecution;
    procedure SetLowerRWEnginePriority(const AValue: Boolean);
  public
    destructor  Destroy; override;
  end;


function GetRWProxy: IevReportWriterProxy;
begin
  Result := TevReportWriterProxy.Create;
end;

function AppCustomData: IisListOfValues;
var
  tmpGlobalSettings: IevDualStream;
begin
  Result := TisListOfValues.Create;
  Result.AddValue('User',  EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
  Result.AddValue('Password',  Context.UserAccount.Password);

  if Assigned(Mainboard.TCPServer) then
  begin
    // If RW is on server side
    Result.AddValue('Host', Mainboard.MachineInfo.IPAddress);
    Result.AddValue('Port', Mainboard.TCPServer.Port);
    Result.AddValue('EncryptionType', etNone);
    Result.AddValue('CompressionLevel', clNone);
  end
  else if Assigned(Mainboard.TCPClient) then
  begin
    // If RW is on client side
    Result.AddValue('Host', Mainboard.TCPClient.Host);
    Result.AddValue('Port', Mainboard.TCPClient.Port);
    Result.AddValue('EncryptionType', Mainboard.TCPClient.EncryptionType);
    Result.AddValue('CompressionLevel', Mainboard.TCPClient.CompressionLevel);
  end;
  
  if IsStandalone then
  begin
    tmpGlobalSettings := (Mainboard.GlobalSettings as IisInterfacedObject).AsStream;
    Result.AddValue('GlobalSettings', tmpGlobalSettings);
  end;
end;



{ TevReportWriterProxy }

function TevReportWriterProxy.GetDesignerConnection: IrwDesignerApp;
begin
  Result := RWConnectionPool.GetRWConnection('RW Designer') as IrwDesignerApp;
  Result.InitializeAppAdapter(GetAdapterName, AppCustomData);
end;

function TevReportWriterProxy.CachedEngineConnection: IrwEngineApp;
begin
  if not Assigned(FCachedEngine) or not RWConnectionPool.CheckRWConnection(FCachedEngine) then
  begin
    AbortIfCurrentThreadTaskTerminated;
    FCachedEngine := RWConnectionPool.GetRWConnection('RW Engine') as IrwEngineApp;
    FCachedEngine.InitializeAppAdapter(GetAdapterName, AppCustomData);
    FCachedEngineHWND := FCachedEngine.GetMainWindowHandle;
    if FInRWSession then
      StartSession;
  end;

  Result := FCachedEngine;
end;


function TevReportWriterProxy.ClassNameToDescr(AClassName: String): string;
var
  Cl: TrwClassInfo;
begin
  Cl := CachedEngineConnection.GetClassInfo(AClassName);
  Result := Cl.DisplayName;
end;

function TevReportWriterProxy.CloseInputForm(CloseResult: TModalResult; Params: TrwReportParams): Boolean;
var
  Pars: IEvDualStream;
begin
  Result := False;
  if Assigned(FCachedEngine) then
    if CachedEngineConnection.GetActiveReport <> nil then
    begin
      if CloseResult = mrOK then
      begin
        CachedEngineConnection.GetActiveReport.ReportInputForm.CloseOK;
        Pars := CachedEngineConnection.GetActiveReport.ReportParameters.ParamsToStream;
        Pars.Position := 0;
        Params.LoadFromStream(Pars.RealStream);
      end
      else
        CachedEngineConnection.GetActiveReport.ReportInputForm.CloseCancel;

      CloseActiveReport;
      Result := True;
    end;
end;

procedure TevReportWriterProxy.CompileReport(Data: IEvDualStream; var RepClassName, AncestorClassName: String);
var
  Rep: IrwReportStructure;
  RepInfo: TrwReportStructureInfo;
begin
  CachedEngineConnection.OpenReportFromStream(Data);
  Rep := CachedEngineConnection.GetActiveReport;
  Rep.Compile;
  RepInfo := Rep.StructureInfo;
  RepClassName := RepInfo.ReportClassName;
  AncestorClassName := RepInfo.AncestorClassName;
end;

function TevReportWriterProxy.ConvertRWAtoPDF(const RWAData: IEvDualStream; const PDFInfoRec: TrwPDFDocInfo): IEvDualStream;
const ProtectionFlags: array [pdfPrint..pdfModifyAnnotation] of Byte = ($01, $02, $04, $08);
var
  OlePDFInf: TrwPDFFileInfo;
  i: TrwPDFProtectionOption;
begin
  OlePDFInf.Author :=  PDFInfoRec.Author;
  OlePDFInf.Creator :=  PDFInfoRec.Creator;
  OlePDFInf.Subject :=  PDFInfoRec.Subject;
  OlePDFInf.Title :=  PDFInfoRec.Title;
  OlePDFInf.Compression :=  PDFInfoRec.Compression;
  OlePDFInf.OwnerPassword :=  PDFInfoRec.OwnerPassword;
  OlePDFInf.UserPassword :=  PDFInfoRec.UserPassword;

  OlePDFInf.ProtectionOptions := 0;
  for i := Low(ProtectionFlags) to High(ProtectionFlags) do
    if i in PDFInfoRec.ProtectionOptions then
      OlePDFInf.ProtectionOptions := OlePDFInf.ProtectionOptions or ProtectionFlags[i];

  Result := CachedEngineConnection.RWAtoPDF(RWAData, OlePDFInf);
end;

procedure TevReportWriterProxy.EndSession;
begin
  if CachedEngineConnection.GetActiveReport <> nil then
  begin
    CachedEngineConnection.GetActiveReport.ReportInputForm.CloseCancel;
    CloseActiveReport;
  end;

  if FCachedEngine <> nil then
    CachedEngineConnection.CallAppAdapterFunction('EndSession', '');
  FInRWSession := False;
end;

function TevReportWriterProxy.GetInputFormBoundRect: TRect;
var
  B: TrwBounds;
begin
  if CachedEngineConnection.GetActiveReport <> nil then
  begin
    B := CachedEngineConnection.GetActiveReport.ReportInputForm.FormBounds;
    Result.Left := B.Left;
    Result.Top := B.Top;
    Result.Right := Result.Left + B.Width;
    Result.Bottom := Result.Top + B.Height;
  end
  else
  begin
    Result.Left := 0;
    Result.Top := 0;
    Result.Right := 0;
    Result.Bottom := 0;
  end;
end;

function TevReportWriterProxy.GetReportInfo(const Data: IEvDualStream): TevRWReportInfo;
var
  ReportStructure: IrwReportStructure;
  RepStruct: TrwReportStructureInfo;
begin
  CachedEngineConnection.OpenReportFromStream(Data);
  ReportStructure := CachedEngineConnection.GetActiveReport;
  RepStruct := ReportStructure.StructureInfo;
  Result.ReportClassName := RepStruct.ReportClassName;
  Result.AncestorClassName := RepStruct.AncestorClassName;
  Result.Description := RepStruct.Description;
  Result.UsedLibComponents := RepStruct.UsedLibComponents;
end;

function TevReportWriterProxy.MakeSecureRWA(DataSource: IEvDualStream; const PreviewPassword, PrintPassword: String): IEvDualStream;
var
  NewInfo: TrwRWAFileInfo;
begin
  NewInfo.PreviewPassword := PreviewPassword;
  NewInfo.PrintPassword := PrintPassword;
  NewInfo.Compression := True;

  Result := CachedEngineConnection.ChangeRWAInfo(DataSource, '', NewInfo);
end;


procedure TevReportWriterProxy.PrepareReport(ReportData: IEvDualStream);
begin
  CachedEngineConnection.OpenReportFromStream(ReportData);
  CachedEngineConnection.GetActiveReport.PrepareForRunning;
  CloseActiveReport;
end;


function TevReportWriterProxy.MergeRWResult(const AReports: TrwReportResults; const AReportType: array of TReportType; const APrinterInfo: TevRWPrinterRec; const APrintJobName: String): IEvDualStream;
var
  i, j: Integer;
  c: Integer;
  RT: TReportTypes;
  PJ: TrwPrintJobInfo;
  RepInfo: TrwReportResultInfo;
  PreviewConnection: IrwPreviewApp;
begin
  Result := nil;

  PreviewConnection := GetPreviewConnection;
  try
    PreviewConnection.ClearGroup;

    RT := [];
    for i := Low(AReportType) to High(AReportType) do
      Include(RT, AReportType[i]);

    for i := 0 to AReports.Count - 1 do
      if Assigned(AReports[i].Data) and (AReports[i].Data.Size > 0) and ((rtUnknown in RT) and (AReports[i].ReportType <> rtCheck)  or
        (AReports[i].ReportType in RT) or (RT = [])) then
      begin
        if AReports[i].ReportType = rtASCIIFile then
          Continue;

        if rtUnknown in RT then
          c := AReports[i].Copies
        else
          c := 1;

        RepInfo.SecuredMode := False;
        RepInfo.PreviewPwd := '';
        RepInfo.PrintPwd := '';
        RepInfo.Name := AReports[i].ReportName;
        RepInfo.FormatType := rwRRTRWA;

        PJ.PrinterName := APrinterInfo.PrinterName;
        PJ.PrintJobName := APrintJobName;
        PJ.DuplexingMode := TrwDuplexingMode(APrinterInfo.Duplexing);
        PJ.PrinterVerticalOffset := APrinterInfo.PrinterVerticalOffset;
        PJ.PrinterHorizontalOffset := APrinterInfo.PrinterHorizontalOffset;
        PJ.Layers := EvLayersIntoOleLayers(AReports[i].Layers);
        PJ.TrayName := AReports[i].Tray;
        PJ.OutBinNbr := AReports[i].OutBinNbr;
        PJ.Copies := 1;
        PJ.FirstPage := 0;
        PJ.LastPage := 0;

        for j := 1 to c do
          PreviewConnection.AddToGroup(AReports[i].Data, RepInfo, PJ);
      end;

    if PreviewConnection.ReportsInGroup > 0 then
      Result := PreviewConnection.MergeGroup;

  finally
    ReturnConnectionToCache(PreviewConnection);
  end;
end;


procedure TevReportWriterProxy.Preview(Results: TrwReportResults; Parent: TrwPreviewContainer; const SecuredPreview: Boolean);
var
  i: Integer;
  PrintJob: TrwPrintJobInfo;
  RepInfo: TrwReportResultInfo;
  PreviewConnection: IrwPreviewApp;
begin
  PreviewConnection := GetPreviewConnection;
  try
    PreviewConnection.ClearGroup;

    for i := 0 to Results.Count - 1 do
    begin
      if (Results[i].ErrorMessage <> '') or not Assigned(Results[i].Data) or (Results[i].Data.Size = 0) then
        continue;

      RepInfo.PreviewPwd := '';
      RepInfo.PrintPwd := '';
      RepInfo.Name := Results[i].ReportName;

      if Results[i].ReportType = rtASCIIFile then
        RepInfo.FormatType := rwRRTASCII
      else
        RepInfo.FormatType := rwRRTRWA;

      RepInfo.SecuredMode := Results[i].SecurePreviewMode or SecuredPreview;

      PrintJob.PrintJobName := Results[i].ReportName;
      PrintJob.PrinterName := Results[i].PrinterInfo.PrinterName;
      PrintJob.DuplexingMode := TrwDuplexingMode(Results[i].PrinterInfo.Duplexing);
      PrintJob.PrinterVerticalOffset := Results[i].PrinterInfo.PrinterVerticalOffset;
      PrintJob.PrinterHorizontalOffset := Results[i].PrinterInfo.PrinterHorizontalOffset;
      PrintJob.TrayName := Results[i].Tray;
      PrintJob.OutBinNbr := Results[i].OutBinNbr;
      PrintJob.Layers := EvLayersIntoOleLayers(Results[i].Layers);
      PrintJob.Copies := Results[i].Copies;
      PrintJob.FirstPage := 0;
      PrintJob.LastPage := 0;

      if Results[i].ReportType = rtASCIIFile then
        PrintJob.PrintJobName := Results[i].FileName;

      PreviewConnection.AddToGroup(Results[i].Data, RepInfo, PrintJob);
    end;

    if Assigned(Parent) then
    begin
      PreviewConnection.PreviewGroup(Parent.Handle);
      Parent.HoldPreview(TevRWPreview.Create(Self, PreviewConnection));
    end
    else
      PreviewConnection.PreviewGroup(0);

  finally
    if not Assigned(Parent) then
      ReturnConnectionToCache(PreviewConnection);
  end;
end;


function TevReportWriterProxy.GetPreviewConnection: IrwPreviewApp;
begin
  Result := RWConnectionPool.GetRWConnection('RW Preview') as IrwPreviewApp;
end;

procedure TevReportWriterProxy.Print(AResults: TrwReportResults; ReportTypes: TReportTypes; PrinterInfo: TevRWPrinterRec; const PrintJobName: String);
var
 RepInfo: TrwReportResultInfo;
 PrintJob: TrwPrintJobInfo;

 procedure PrintGroup;
 var
   i: Integer;
   PreviewConnection: IrwPreviewApp;
 begin
   PreviewConnection := GetPreviewConnection;
   try
     PreviewConnection.ClearGroup;

     for i := 0 to AResults.Count - 1 do
       if (AResults[i].ErrorMessage = '') and Assigned(AResults[i].Data) and (AResults[i].Data.Size > 0) and
          ((ReportTypes = []) or (AResults[i].ReportType in ReportTypes)) then
       begin
         RepInfo.Name := AResults[i].ReportName;
         RepInfo.FormatType := rwRRTRWA;
         RepInfo.SecuredMode := False;
         RepInfo.PreviewPwd := '';
         RepInfo.PrintPwd := '';

         PrintJob.TrayName := AResults[i].Tray;
         PrintJob.OutBinNbr := AResults[i].OutBinNbr;
         PrintJob.Layers := EvLayersIntoOleLayers(AResults[i].Layers);
         PrintJob.Copies := AResults[i].Copies;

         PreviewConnection.AddToGroup(AResults[i].Data, RepInfo, PrintJob);
       end;

      if PreviewConnection.ReportsInGroup > 0 then
      begin
        PrintJob.TrayName := '';
        PrintJob.OutBinNbr := 0;
        PrintJob.Layers := '';
        PrintJob.Copies := 1;
        PreviewConnection.PrintGroup(PrintJob);
      end;
   finally
     ReturnConnectionToCache(PreviewConnection);
   end;
 end;

begin
  PrintJob.PrinterName := PrinterInfo.PrinterName;
  PrintJob.PrintJobName := PrintJobName;
  PrintJob.DuplexingMode := TrwDuplexingMode(PrinterInfo.Duplexing);
  PrintJob.TrayName := '';
  PrintJob.PrinterVerticalOffset := PrinterInfo.PrinterVerticalOffset;
  PrintJob.PrinterHorizontalOffset := PrinterInfo.PrinterHorizontalOffset;
  PrintJob.OutBinNbr := 0;
  PrintJob.FirstPage := 0;
  PrintJob.LastPage := 0;
  PrintJob.Layers := '';
  PrintJob.Copies := 1;

  PrintGroup;
end;

procedure TevReportWriterProxy.RunQueryBuilderQuery(Data: IEvDualStream; ClientNbr, CoNbr: Integer; Params: TParams; ResultDataSet: TEvBasicClientDataSet);
var
  ContextData: Variant;
  Prms: Variant;
  i, j: Integer;
begin
  ContextData := VarArrayOf([ClientNbr, CoNbr]);
  CachedEngineConnection.CallAppAdapterFunction('SetClientCompany', ContextData);

  if Assigned(Params) then
  begin
    Prms := VarArrayCreate([0, (Params.Count * 2) - 1], varVariant);
    for i := 0 to Params.Count - 1 do
    begin
      j := i * 2;
      Prms[j] := Params[i].Name;
      Prms[j + 1] := Params[i].Value;      
    end;
  end
  else
    Prms := VarArrayOf([]);

  ResultDataSet.Data := CachedEngineConnection.RunQuery(Data, Prms);
end;

function TevReportWriterProxy.RunReport(const ReportData: IEvDualStream; const Params: TrwReportParams; const RunSettings: TevRWRunReportSettings): TevRWResultInfo;
var
  Rep: IrwReportStructure;
  Prms: IEvDualStream;
  RunInfo: TrwRunReportSettings;
  Res: TrwReportResult;
  i: Integer;
  OleArray: Variant;
  h: String;
begin
  try
    Result.Data := nil;
    Result.ResultType := rwRTUnknown;
    Result.ReturnValue := Null;
    Result.DefaultFileName := '';
    Result.PageInfo := nil;

    CachedEngineConnection.OpenReportFromStream(ReportData);
    Rep := CachedEngineConnection.GetActiveReport;
    Prms := TEvDualStreamHolder.Create;
    Params.SaveToStream(Prms.RealStream);
    Rep.ReportParameters.ParamsFromStream(Prms);
    RunInfo.Duplexing := RunSettings.Duplexing;
    RunInfo.ShowInputForm := RunSettings.ShowInputForm;
    RunInfo.ReturnParamsBack := RunSettings.ReturnParamsBack;
    RunInfo.ReportName := RunSettings.ReportName;

    // In most cases __RUN_CONTROL exists, but it does not mean the report is MT one
    // So, here is logic, which controls ReturnParamsBack flag
    if Assigned(Params.ParamByName(__RUN_CONTROL))then
      if Rep.ReportParameters.IndexOf(__RUN_CONTROL) <> -1 then
        RunInfo.ReturnParamsBack := True    // return params
      else
        Params.ParamByName(__RUN_CONTROL).Free; // use default ReturnParamsBack value (may be True for some reports)



    CachedEngineConnection.CallAppAdapterFunction('BeforeRunReport', '');

    Res := CachedEngineConnection.RunActiveReport(RunInfo);

    if Assigned(Res.Data) then
    begin
      Result.Data := Res.Data;
      Result.ResultType := TevRWResultFormatType(Ord(Res.FormatType));

      if RunSettings.ReturnPageInfo and (Result.ResultType = rwRTRWA) then
      begin
        OleArray := Res.PageInfo;
        SetLength(Result.PageInfo, VarArrayHighBound(OleArray, 1) -  VarArrayLowBound(OleArray, 1) + 1);

        for i := VarArrayLowBound(OleArray, 1) to VarArrayHighBound(OleArray, 1) do
        begin
          h := OleArray[i];
          Result.PageInfo[i].PageNumber := StrToInt(GetNextStrValue(h, ','));
          Result.PageInfo[i].WidthMM := StrToInt(GetNextStrValue(h, ','));
          Result.PageInfo[i].HeightMM := StrToInt(GetNextStrValue(h, ','));
          Result.PageInfo[i].Duplexing := Boolean(StrToInt(GetNextStrValue(h, ',')));
        end;
      end;

      OleArray := Res.MiscInfo;
      Result.ReturnValue := OleArray[0];
      Result.DefaultFileName := OleArray[1];
    end;

    if RunInfo.ReturnParamsBack then
    begin
      Assert(Assigned(Rep));
      Assert(Assigned(Rep.ReportParameters));
      Prms := Rep.ReportParameters.ParamsToStream;
      Prms.Position := 0;
      Params.LoadFromStream(Prms.RealStream);
    end;
  finally
    CloseActiveReport;
  end;
end;

function TevReportWriterProxy.RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
begin
  Result := CachedEngineConnection.ClassInheritsFrom(AClassName, AAncestorClassName);
end;

function TevReportWriterProxy.ShowAncestorInputForm(const AncestorClassName: String; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
begin
  CloseActiveReport;
  CachedEngineConnection.OpenReportFromClass(AncestorClassName);
  Result := ShowInputFormInternal(Params, Parent, RunTime);
end;


function TevReportWriterProxy.ShowDesigner(const Data: IEvDualStream; const ReportName: String; const DefaultReportType: Integer = 0): Boolean;
var
  ContextData: OleVariant;
  ResData: IEvDualStream;
  Designer: IrwDesignerApp;
begin
  ctx_StartWait('Opening RW Designer...');
  try
    ContextData := VarArrayOf([ctx_DataAccess.ClientID, DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger]);

    Designer := GetDesignerConnection;
    try
      Designer.CallAppAdapterFunction('StartSession', AppCustomData);
      Designer.CallAppAdapterFunction('SetClientCompany', ContextData);

      if Assigned(Data) then
        Result := Designer.OpenDesigner2(Data, ReportName, DefaultReportType)
      else
        Result := Designer.OpenDesigner2(nil, ReportName, DefaultReportType);

      if Result then
      begin
        ctx_EndWait;
        Application.MainForm.Enabled := False;
        try
          while not Designer.IsDesignerFormClosed(ResData) do
          begin
            Application.ProcessMessages;
            if Application.Active then
              Application.MainForm.SendToBack;
            Sleep(500);
          end;

          if Assigned(ResData) and Assigned(Data) then
          begin
            Data.Size := 0;
            Data.CopyFrom(ResData, 0);
          end
          else
            Result := False;
        finally
          Application.MainForm.Enabled := True;
          Application.BringToFront;
        end;
      end;
    finally
      Designer.CallAppAdapterFunction('EndSession', '');
      ReturnConnectionToCache(Designer);
    end;
  finally
    ctx_EndWait;
  end;
end;

function TevReportWriterProxy.ShowInputForm(ReportData: IEvDualStream; Params: TrwReportParams; Parent: TrwInputFormContainer; RunTime: Boolean): Boolean;
begin
  CloseActiveReport;
  CachedEngineConnection.OpenReportFromStream(ReportData);
  Result := ShowInputFormInternal(Params, Parent, RunTime);
end;

function TevReportWriterProxy.ShowInputFormInternal(Params: TrwReportParams; Parent: TrwInputFormContainer;  RunTime: Boolean): Boolean;
var
  Pars: IEvDualStream;
begin
  if not Assigned(Parent) then
    raise  Exception.Create('Parent is required for RW Input Form');

  Pars := TEvDualStreamHolder.Create;
  Params.SaveToStream(Pars.RealStream);

  CachedEngineConnection.GetActiveReport.ReportParameters.ParamsFromStream(Pars);

  Result := CachedEngineConnection.GetActiveReport.ReportInputForm.Show(Parent.Handle, not RunTime);
  if Result then
  begin
    if Parent.Parent is TrwModalInputForm then
      TrwModalInputForm(Parent.Parent).SetInitialSize(GetInputFormBoundRect);

    FInputFormContainer := Parent;
    FInputFormContainer.HoldInputForm(CachedEngineConnection.GetActiveReport.ReportInputForm);
  end
  else
    CloseActiveReport;
end;

function TevReportWriterProxy.ShowQueryBuilder(const Data: IEvDualStream; ClientNbr, CoNbr: Integer; DescriptionView: Boolean): Boolean;
var
  ContextData: OleVariant;
  ResData: IevDualStream;
begin
  ctx_StartWait('Opening RW Query Builder...');
  try
    ContextData := VarArrayOf([ClientNbr, CoNbr]);
    CachedEngineConnection.CallAppAdapterFunction('SetClientCompany', ContextData);

    if Assigned(Data) then
      Result := CachedEngineConnection.OpenQueryBuilder2(Data, DescriptionView)
    else
      Result := CachedEngineConnection.OpenQueryBuilder2(nil, DescriptionView);

    if Result then
    begin
      ctx_EndWait;
      Application.MainForm.Enabled := False;
      try
        while not CachedEngineConnection.IsQueryBuilderFormClosed(ResData) do
        begin
          Application.ProcessMessages;
          if Application.Active then
            Application.MainForm.SendToBack;
          Sleep(500);
        end;

        if Assigned(ResData) then
        begin
          Data.Size := 0;
          Data.CopyFrom(ResData, 0);
          Result := True;
        end
        else
          Result := False;

      finally
        Application.MainForm.Enabled := True;
        Application.BringToFront;
      end;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevReportWriterProxy.StartSession;
begin
  CachedEngineConnection.CallAppAdapterFunction('StartSession', AppCustomData);
  FInRWSession := True;
end;

destructor TevReportWriterProxy.Destroy;
begin
  try
    ReleaseRWConnections;
  except
  end;

  inherited;
end;

function TevReportWriterProxy.ShowModalInputForm(ReportData: IEvDualStream; Params: TrwReportParams; RunTime: Boolean): TModalResult;
var
  Frm: TrwModalInputForm;
begin
  Result := mrNone;

  Frm := TrwModalInputForm.Create(nil);
  try
    if ShowInputForm(ReportData, Params, Frm.InputFormContainer, RunTime) then
    begin
      Frm.Params := Params;
      Result := Frm.ShowModal;
      Result := Frm.ModalResult;
    end;

  finally
    FreeAndNil(Frm);
  end;
end;

function TevReportWriterProxy.EvLayersIntoOleLayers(ALayers: TrwPrintableLayers): OleVariant;
var
  i: TrwLayerNumber;
begin
  Result := '';
  for i := Low(TrwLayerNumber) to High(TrwLayerNumber) do
    if i in ALayers then
    begin
      if Result <> '' then
        Result := Result + ',';
      Result := Result + IntToStr(Ord(i));
    end;
end;

procedure TevReportWriterProxy.StopReportExecution;
begin
  PostMessage(FCachedEngineHWND, WM_USER + 1000, 0, 0);
end;

procedure TevReportWriterProxy.ReleaseRWConnections;
begin
  try
    CloseInputForm(mrCancel, nil);
  except
  end;

  CloseActiveReport;

  if Assigned(FCachedEngine) then
  begin
    ReturnConnectionToCache(FCachedEngine);
    FCachedEngine := nil;
  end;
end;

procedure TevReportWriterProxy.ReturnConnectionToCache(const AConnection: IInterface);
begin
  if Supports(AConnection, IrwPreviewApp) then
  begin
    (AConnection as IrwPreviewApp).Preview(nil, 0);  // closing preview
    (AConnection as IrwPreviewApp).ClearGroup;
  end;

  RWConnectionPool.ReturnRWConnection(AConnection);
end;

function TevReportWriterProxy.GetRWEngineProcessID: Cardinal;
begin
  if FCachedEngineHWND <> 0 then
    GetWindowThreadProcessId(FCachedEngineHWND, Result)
  else
    Result := 0;
end;

procedure TevReportWriterProxy.SetLowerRWEnginePriority(const AValue: Boolean);
var
  ProcID: Cardinal;
  Proc: THandle;
begin
  ProcID := GetRWEngineProcessID;
  if ProcID <> 0 then
  begin
    Proc := OpenProcess(PROCESS_SET_INFORMATION, False, ProcID);
    if Proc <> 0 then
    begin
      if AValue then
        SetPriorityClass(Proc, IDLE_PRIORITY_CLASS)
      else
        SetPriorityClass(Proc, NORMAL_PRIORITY_CLASS);
      CloseHandle(Proc);
    end;
  end;
end;

procedure TevReportWriterProxy.CloseActiveReport;
begin
  if Assigned(FCachedEngine) then
    if FCachedEngine.GetActiveReport <> nil then
    begin
      if Assigned(FInputFormContainer) then
      begin
        FInputFormContainer.HoldInputForm(nil);
        FInputFormContainer := nil;
      end;
      FCachedEngine.GetActiveReport.ReportInputForm.CloseCancel;
      FCachedEngine.CloseActiveReport;
    end;
end;

function TevReportWriterProxy.GetAdapterName: String;
var
  s: String;
begin
  if (Assigned(Mainboard.TCPClient) or Assigned(Mainboard.TCPServer)) then
    s := 'EvRWAppAdapter.dll'
  else
    s := 'EvRWAppAdapterSA.dll';

  Result := AppDir + s;
end;

{ TevRWPreview }

constructor TevRWPreview.Create(const AOwner: TevReportWriterProxy; const APreviewConnection: IrwPreviewApp);
begin
  FOwner := AOwner;
  FPreviewConnection := APreviewConnection;
end;

destructor TevRWPreview.Destroy;
begin
  FOwner.ReturnConnectionToCache(FPreviewConnection);
  inherited;
end;

function TevRWPreview.GetBoundsRect: TRect;
var
  Bnd: TrwBounds;
begin
  Bnd := FPreviewConnection.FormBounds;
  Result := Rect(Bnd.Left, Bnd.Top, Bnd.Left + Bnd.Width, Bnd.Top + Bnd.Height);
end;

procedure TevRWPreview.SetBoundsRect(AValue: TRect);
var
  Bnd: TrwBounds;
begin
  Bnd.Left := AValue.Left;
  Bnd.Top := AValue.Top;
  Bnd.Width := AValue.Right - AValue.Left;
  Bnd.Height := AValue.Bottom - AValue.Top;
  FPreviewConnection.FormBounds := Bnd;
end;

end.
