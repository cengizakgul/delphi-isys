// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDataStructure;

interface

uses
  Classes, SysUtils, SSecurityInterface, SDDClasses, typinfo,
  EvSecElement,  EvUtils, StrUtils, SDataDictAdd,
  SDataDictSystem, SDataDictBureau, SDataDictClient, SDataDictTemp,
  EvConsts, evTypes, isBaseClasses;

type
  TDM_SYSTEM = class(TDM_SYSTEM_);

  TDM_BUREAU = class(TDM_BUREAU_);

  TDM_SERVICE_BUREAU = class(TDM_BUREAU);


  TDM_SYSTEM_LOCAL = class(TDM_SYSTEM);
  TDM_SYSTEM_STATE = class(TDM_SYSTEM);
  TDM_SYSTEM_MISC = class(TDM_SYSTEM);
  TDM_SYSTEM_FEDERAL = class(TDM_SYSTEM);
  TDM_SYSTEM_HR = class(TDM_SYSTEM);
  TDM_TEMPORARY = class(TDM_TEMPORARY_);

  TDM_CLIENT = class(TDM_CLIENT_)
  private
    function GetPR_CHECK_LINES_DISTRIBUTED: TPR_CHECK_LINES_DISTRIBUTED;
  published
    property PR_CHECK_LINES_DISTRIBUTED: TPR_CHECK_LINES_DISTRIBUTED read GetPR_CHECK_LINES_DISTRIBUTED;
  end;

  TDM_COMPANY = class(TDM_CLIENT);
  TDM_EMPLOYEE = class(TDM_CLIENT);
  TDM_PAYROLL = class(TDM_CLIENT);

procedure Register;

function DM_SERVICE_BUREAU: TDM_BUREAU;
function DM_SYSTEM: TDM_SYSTEM;
function DM_SYSTEM_LOCAL: TDM_SYSTEM;
function DM_SYSTEM_STATE: TDM_SYSTEM;
function DM_SYSTEM_MISC: TDM_SYSTEM;
function DM_SYSTEM_FEDERAL: TDM_SYSTEM;
function DM_SYSTEM_HR: TDM_SYSTEM;
function DM_TEMPORARY: TDM_TEMPORARY;
function DM_COMPANY: TDM_CLIENT;
function DM_CLIENT: TDM_CLIENT;
function DM_EMPLOYEE: TDM_CLIENT;
function DM_PAYROLL: TDM_CLIENT;

procedure CreateDataSetAtoms(const Owner: TComponent);

function GetDBTypeByTableName(const sName: string): TevDBType;
function GetddDatabaseByTableClass(TableClass: TddTableClass): TddDatabaseClass;
function GetddDatabaseByTableName(sName: string): TddDatabaseClass;
function GetddTableClassByName(sName: string): TddTableClass;
function GetddTableClassByTempTableClass(const TableName: TddTableClass): TddTableClass;

function ConvertCode(const TableName: TddTableClass; const FieldName: string; const S: string): string;

function IsValidTableName(sName: string): Boolean;

function TempTablesList: IisStringList;
function GetTableFieldList(const ATableName: String): IisStringList;

implementation

uses
  SFIeldCodeValues;

var
  DT_BUREAU: TDM_BUREAU;
  DT_SYSTEM: TDM_SYSTEM;
  DT_TEMPORARY: TDM_TEMPORARY;
  DT_CLIENT: TDM_CLIENT;

  vTempTablesList: IisStringList;


procedure Register;
begin
  RegisterComponents('EvDataModules',
      [TDM_SERVICE_BUREAU, TDM_SYSTEM_LOCAL, TDM_SYSTEM_STATE, TDM_SYSTEM_MISC,
       TDM_SYSTEM_FEDERAL,TDM_SYSTEM_HR,
       TDM_TEMPORARY, TDM_COMPANY, TDM_CLIENT, TDM_EMPLOYEE, TDM_PAYROLL]);
end;

type
  TDummyEventHandlerClass = class(TComponent)
  public
    procedure ProvideContexts(const Sender: ISecurityAtom; var ContextRecords: TAtomContextRecordArray);
  end;

function IsValidTableName(sName: string): Boolean;
begin
  Result := IsPublishedProp(TDM_CLIENT, sName) or
            IsPublishedProp(TDM_TEMPORARY, sName) or
            IsPublishedProp(TDM_BUREAU, sName) or
            IsPublishedProp(TDM_SYSTEM, sName);
end;


function TempTablesList: IisStringList;
begin
  Result := vTempTablesList;
end;

function GetTableFieldList(const ATableName: String): IisStringList;
var
  T: TddTableClass;
  FL: TddFieldList;
  i: Integer;
begin
  T := GetddTableClassByName(ATableName);
  Assert(Assigned(T));
  FL := T.GetFieldList;

  Result := TisStringList.CreateAsIndex;
  Result.Capacity := Length(FL);
  for i := Low(FL) to High(FL) do
    Result.Add(FL[i]);
end;

function GetddTableClassByName(sName: string): TddTableClass;
begin
  Result := TddTableClass(GetClass('T' + sName));
end;

function GetDBTypeByTableName(const sName: string): TevDBType;
begin
  Result := GetddDatabaseByTableName(sName).GetDBType;
end;

function GetddTableClassByTempTableClass(const TableName: TddTableClass): TddTableClass;
begin
  Result := GetddTableClassByName(RightStr(TableName.ClassName, Length(TableName.ClassName) - 5));
end;

function GetddDatabaseByTableClass(TableClass: TddTableClass): TddDatabaseClass;
begin
  Result := GetddDatabaseByTableName(RightStr(TableClass.ClassName, Pred(Length(TableClass.ClassName))));
end;

function GetddDatabaseByTableName(sName: string): TddDatabaseClass;
begin
  Result := nil;
  if IsPublishedProp(TDM_CLIENT, sName) or (AnsiCompareText(sName, 'CL_CUSTOM') = 0) then
    Result := TDM_CLIENT
  else if IsPublishedProp(TDM_TEMPORARY, sName) or (AnsiCompareText(sName, 'TEMP_CUSTOM') = 0) then
    Result := TDM_TEMPORARY
  else if IsPublishedProp(TDM_BUREAU, sName) or (AnsiCompareText(sName, 'SB_CUSTOM') = 0) then
    Result := TDM_BUREAU
  else if IsPublishedProp(TDM_SYSTEM, sName) or (AnsiCompareText(sName, 'SY_CUSTOM') = 0) then
    Result := TDM_SYSTEM;
end;


procedure CreateDataSetAtoms(const Owner: TComponent);
var
  DummyEventHandlerClass: TDummyEventHandlerClass;
  procedure Process(const DataModule: TStrEntry);
  var
    TypeData: PTypeData;
    PropList: PPropList;
    Count: Integer;
    i: Integer;
    a: TevSecAtom;
  begin
    TypeData := GetTypeData(DataModule.ClassInfo);
    if TypeData.PropCount <> 0 then
    begin
      GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
      try
        Count := GetPropList(DataModule.ClassInfo, [tkClass], PropList);
        for i := 0 to Pred(Count) do
        begin
          a := TevSecAtom.Create(Owner);
          with a do
          begin
            AtomType := atDataset;
            AtomTag := 'DS_'+ PropList[i]^.Name;
            AtomCaption := PropList[i]^.Name+ ' table';
            OnGetContextRecordArray := DummyEventHandlerClass.ProvideContexts;
          end;
          with TevSecElement.Create(Owner) do
          begin
            AtomComponent := a;
            ElementType := otDataset;
            ElementTag := PropList[i]^.Name;
          end;
        end;
      finally
        FreeMem(PropList);
      end;
    end;
  end;
begin
  DummyEventHandlerClass := TDummyEventHandlerClass.Create(Owner);
  Process(DM_SERVICE_BUREAU);
  Process(DM_SYSTEM);
  Process(DM_TEMPORARY);
  Process(DM_CLIENT);
end;

function DM_SERVICE_BUREAU: TDM_BUREAU;
begin
  Result := DT_BUREAU;
end;

function DM_SYSTEM_LOCAL: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;

function DM_SYSTEM: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;

function DM_SYSTEM_STATE: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;

function DM_SYSTEM_MISC: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;

function DM_SYSTEM_FEDERAL: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;

//begin gdy
function DM_SYSTEM_HR: TDM_SYSTEM;
begin
  Result := DT_SYSTEM;
end;
//end gdy

function DM_TEMPORARY: TDM_TEMPORARY;
begin
  Result := DT_TEMPORARY;
end;

function DM_COMPANY: TDM_CLIENT;
begin
  Result := DT_CLIENT;
end;

function DM_CLIENT: TDM_CLIENT;
begin
  Result := DT_CLIENT;
end;

function DM_EMPLOYEE: TDM_CLIENT;
begin
  Result := DT_CLIENT;
end;

function DM_PAYROLL: TDM_CLIENT;
begin
  Result := DT_CLIENT;
end;


{ TDummyEventHandlerClass }

procedure TDummyEventHandlerClass.ProvideContexts(
  const Sender: ISecurityAtom;
  var ContextRecords: TAtomContextRecordArray);
const
  secDsOff: TAtomContextRecord = (Tag: ctDisabled; Caption: 'Disabled'; IconName: ''; SelectedIconName: ''; Description: ''; Priority: 0);
  secDsOn: TAtomContextRecord = (Tag: ctEnabled; Caption: 'Enabled'; IconName: ''; SelectedIconName: ''; Description: ''; Priority: 255);
  secDsRO: TAtomContextRecord = (Tag: ctReadOnly; Caption: 'ReadOnly'; IconName: ''; SelectedIconName: ''; Description: ''; Priority: 128);
begin
  SetLength(ContextRecords, 3);
  ContextRecords[0] := secDsOff;
  ContextRecords[1] := secDsRO;
  ContextRecords[2] := secDsOn;
end;

{ TDMClient }

function ConvertCode(const TableName: TddTableClass; const FieldName: string; const S: string): string;
var
  J: integer;
  K: integer;
  ChoiceStringList, Choices: TStringList;
begin
  result := '';

  ChoiceStringList := TStringList.Create;
  Choices := TStringList.Create;

  for J := Low(FieldsToPopulate) to High(FieldsToPopulate) do
    if ((FieldsToPopulate[J].F = FieldName) and
      (FieldsToPopulate[J].T = TableName)) then
    begin
      Choices.Text := FieldsToPopulate[J].C;
      for K := 0 to Choices.Count - 1 do
      begin
        ChoiceStringList.Text := StringReplace(Choices[K], #9, #13, [rfReplaceAll]);
        if ChoiceStringList[1] = S then
        begin
          result := ChoiceStringList[0];
          break;
        end;
      end;
    end;

  ChoiceStringList.Destroy;
  Choices.Destroy;
end;

{ TDM_CLIENT }

function TDM_CLIENT.GetPR_CHECK_LINES_DISTRIBUTED: TPR_CHECK_LINES_DISTRIBUTED;
begin
  Result := GetDataSet(TPR_CHECK_LINES_DISTRIBUTED) as TPR_CHECK_LINES_DISTRIBUTED;
end;


procedure InitTempTablesList;
var
  TL: TddTableList;
  i: Integer;
begin
  TL := DT_TEMPORARY.GetTableList;
  vTempTablesList := TisStringList.CreateAsIndex;

  for i := Low(TL) to High(TL) do
    vTempTablesList.Add(TL[i]);
end;

initialization
  DT_SYSTEM := TDM_SYSTEM.Create(nil);
  DT_TEMPORARY := TDM_TEMPORARY.Create(nil);
  DT_CLIENT := TDM_CLIENT.Create(nil);
  DT_BUREAU := TDM_BUREAU.Create(nil);
  InitTempTablesList;

finalization
  FreeAndNil(DT_BUREAU);
  FreeAndNil(DT_SYSTEM);
  FreeAndNil(DT_TEMPORARY);
  FreeAndNil(DT_CLIENT);

end.
