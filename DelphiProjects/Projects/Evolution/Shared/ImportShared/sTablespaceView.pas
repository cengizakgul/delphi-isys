// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sTablespaceView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  Grids, Wwdbigrd, Wwdbgrid, StdCtrls, ExtCtrls,
  genericImport, ISBasicClasses, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TTableSpaceViewFrame = class(TFrame, ITableSpaceViewer )
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    cbTables: TevComboBox;
    evLabel1: TevLabel;
    dgView: TevDBGrid;
    dsView: TevDataSource;
    procedure cbTablesChange(Sender: TObject);
  private
    FTablespace: ITablespace;
    FCopies: TStringList;
    procedure ClearCopiesList;
    {ITableSpaceViewer}
    procedure SetTablespace( aTablespace: ITablespace );
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;

    { Public declarations }
  end;

implementation

uses
  evDataAccessComponents, isutils, evutils;
  
{$R *.DFM}



{ TTableSpaceViewFrame }

procedure TTableSpaceViewFrame.SetTablespace(aTablespace: ITablespace);
begin
  ClearCopiesList;
  FTablespace := aTablespace;
  cbTables.clear;

  if assigned( FTablespace ) then
    FTablespace.GetAvailableTables( cbTables.Items );
  cbTables.Enabled := cbTables.Items.Count > 0;
  if cbTables.Items.Count > 0 then
    cbTables.ItemIndex := 0
  else
    cbTables.ItemIndex := -1;
  cbTablesChange( cbTables );
end;

//copied from evUtils; changed argument type for DataSet from TevClientDataSet to TDataSet
procedure SetSelectedFromFields(const Grid: TevDBGrid; const DataSet: TDataSet; const HideFields: array of ShortString);
  function IsNotHidden(const FieldName: string): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := 0 to High(HideFields) do
      if UpperCase(HideFields[i]) = UpperCase(FieldName) then
      begin
        Result := False;
        Break;
      end;
  end;
  function Parse_(const s: string): string;
  var
    i: Integer;
  begin
    Result := s;
    for i := 1 to Length(Result) do
      if Result[i] = '_' then Result[i] := ' ';
  end;
var
  i: Integer;
  s: string;
begin
  Grid.Selected.Clear;
  for i := 0 to DataSet.Fields.Count-1 do
    begin
      if DataSet.Fields[i].DisplayName <> DataSet.Fields[i].FieldName then
        s := DataSet.Fields[i].DisplayName
      else
        s := Parse_(UpperCase(DataSet.Fields[i].FieldName[1])+ LowerCase(Copy(DataSet.Fields[i].FieldName, 2, 255)));
      Grid.Selected.Append(DataSet.Fields[i].FieldName+ #9+ IntToStr(DataSet.Fields[i].DisplayWidth)+ #9+ s+ #9'F');
    end;
  Grid.ApplySelected;
end;



type
  EAbortOperation = class(Exception);

//#RE 28072
function CopyDS(ds: TDataSet): TEvClientDataSet;
  procedure CloneFields;
  var
    i: integer;
    fcl: TFieldClass;
    aField: TField;
  begin
    for i := 0 to ds.Fields.Count-1 do
    begin
      fcl := DefaultFieldClasses[ds.Fields[i].DataType];
      if fcl.InheritsFrom(TWideStringField) then
        fcl := TStringField;
      aField := fcl.Create(Result);
      aField.FieldKind := fkData;
      aField.Size := ds.Fields[i].Size;
      aField.FieldName := ds.Fields[i].FieldName;
      aField.DataSet := Result;
      aField.Required := false;
      aField.Visible := true;

      aField.DisplayLabel := ds.Fields[i].DisplayLabel;
      aField.DisplayWidth := ds.Fields[i].DisplayWidth;

      if aField is TBCDField then
        TBCDField(aField).Precision := TBCDField(ds.Fields[i]).Precision;
      if ds.Fields[i].DataType = ftFixedChar then
        TStringField(aField).FixedChar := True;
    end;
  end;
  procedure CopyData;
  var
    map: array of TField;
    i: integer;
  begin
    //seems to be reduntant but I haven't checked if it is really so
    SetLength( map, Result.Fields.Count );
    for i := 0 to Result.Fields.Count-1 do
      map[i] := ds.FindField(Result.Fields[i].FieldName);
    ds.First;
    ctx_StartWait('Preparing data. Press Esc to abort', ds.RecordCount);
    try
      while not ds.Eof do
      begin
        Result.Append;
        for i := 0 to Result.Fields.Count-1 do
         // if assigned(map[i]) then
            Result.Fields[i].Value := map[i].Value;
{           if map[i].ClassType <> Result.Fields[i].ClassType then
              Result.Fields[i].AsString := map[i].AsString
           else
              Result.Fields[i].Value := map[i].Value;
}
        if (ds.RecNo mod 1000) = 0 then
        begin
          if CheckEscape and
            (EvMessage( 'You wouldn''t be able to sort and filter this table. Abort?', mtConfirmation, [mbYes, mbNo], mbNO ) = mrYes) then
              raise EAbortOperation.Create('User break.');
          ctx_UpdateWait('Preparing data. Press Esc to abort', ds.RecNo);
        end;
        ds.Next;
      end;
    finally
      ctx_EndWait;
    end;
    Result.First;
  end;
begin
  Result := TEvClientDataSet.CreateWithChecksDisabled(nil);
  try
    Result.LogChanges := false;
    CloneFields;
    Result.CreateDataSet;
    CopyData;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TTableSpaceViewFrame.cbTablesChange(Sender: TObject);
var
  ds: TDataSet;
  i: integer;
  newds: TDataSet;
begin
  newds := nil;
  if (cbTables.ItemIndex <> -1) and assigned(FTablespace) then
  begin
    ds := FTablespace.GetTable( cbTables.Items[cbTables.ItemIndex] );
    if ds is TEvBasicClientDataSet then
      newds := ds
    else
    begin
      i := FCopies.IndexOf(ds.Name);
      if i = -1 then
      begin
        try
          newds := CopyDS(ds);
          try
            FCopies.AddObject( ds.Name, newds );
          except
            FreeAndNil(newds);
            raise;
          end
        except
          on E: Exception do
            begin
              newds := ds;
              if not (E is EAbortOperation) then
                Application.HandleException( e );
            end
        end;
      end
      else
        newds := FCopies.Objects[i] as TDataSet;
    end;
  end;
  dsView.DataSet := newds;
  try
    if dsView.DataSet <> nil then
      SetSelectedFromFields( dgView, dsView.DataSet, [] );
  except
    dsView.DataSet := nil;
    raise;
  end;
  dgView.enabled := dsView.DataSet <> nil;
end;

constructor TTableSpaceViewFrame.Create(Owner: TComponent);
begin
  inherited;
  FCopies := TStringList.Create;
  FCopies.Duplicates := dupError;
  FCopies.Sorted := true;
end;

destructor TTableSpaceViewFrame.Destroy;
begin
  inherited;
  ClearCopiesList;
  FreeAndNil(FCopies);
end;

procedure TTableSpaceViewFrame.ClearCopiesList;
var
  i: integer;
begin
  if Assigned(FCopies) then
  begin
    for i := 0 to FCopies.Count-1 do
      FCopies.Objects[i].Free;
    FCopies.Clear;
  end;
end;

end.


