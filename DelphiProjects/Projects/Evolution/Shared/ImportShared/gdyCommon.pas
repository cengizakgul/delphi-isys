// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{$INCLUDE gdy.inc}
unit gdycommon;

interface

uses
  sysutils, classes, contnrs, windows, controls, forms, menus, buttons, graphics, actnlist, EvStreamUtils;

const
  endl: string = #13+#10;

function ShadeColor(InColor : TColor; ShadingDepth: Integer) : TColor; //completely wrong implementation by SEG

function GetAppDir: string;
function GetAppFilename: string;
function GetModuleName(Module: HMODULE): string;
function MySelectDirectory(const Caption: string;
  var Directory: string; const Root: WideString; parent: TwinControl; allowCreate: boolean): Boolean;

procedure StringToFile( s: string; fname: string );
function  FileToString( fname: string ): string;

procedure Log(const Fmt: string; const Args: array of const); overload;
procedure Log(const msg: string); overload;
procedure Log(const filename: string; const msg: string); overload;
procedure MB( const msg: string ); overload;
procedure MB( const msg: string; const Args: array of const); overload;

{$ifdef GDY_COMMON}
procedure ODS(const Fmt: string; const Args: array of const); overload;
procedure ODS(const msg: string); overload;
{$endif}

function IsCardinal( s: string ): boolean;
function IsInteger( s: string ): boolean;
function IsFloat( s: string ): boolean;

procedure SetTag( menu: TMenuItem; value: integer );
procedure DeleteWithTag( menu: TMenuItem; value: integer );


procedure FreeObjects( list: TStrings ); overload;
procedure FreeObjects( stack: TObjectStack ); overload;
//procedure FreeObjects( list: TObjectList ); overload;

function PopupMenuPos( btn: TControl; isLeft: boolean = false; istop: boolean = false): TPoint;

function WaitForKey( aMsgToWait: cardinal ): boolean;

function VersionInfoAsString: string;


function RandomInteger(iLow, iHigh: Integer): Integer;
function RandomString(iLength: Integer): String;


function BinToHex(s: string): string;
function HexToBin(s: string): string;


//type
//  TPluralFormsArray = array [0..3] of string;
function Plural( n: integer; forms: array of string ): string; overload;
function Plural( s: string ): string; overload;//not complete --doesnt handle -y
function Plural( s: string; count: integer ): string; overload;

procedure StreamToFile( aStream: TStream;  filename: string );
procedure FileToStream( filename: string; aStream: TStream );

function GetOwningForm( frame: TComponent ): TCustomForm;

function GetUserName: string;

function WithoutTrailingSlash( const s: string): string;

procedure EnableControl( control: TComponent; enable: boolean );
procedure CheckColor(const control: TControl; const grayed: Boolean);

function EnsureHasMenuItem( Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu;

//
function FileGetBasedTempName( const filename: string ): string;

procedure DrawProgressIndicator( aCanvas: TCanvas; aRect: TRect; aOperationName: string; aCurrent, aTotal: integer );
procedure RedrawIfNeeded;
function IndexOfText( sl: TStrings; w: string ): integer;
function IndexOfNameText( sl: TStrings; w: string ): integer;

function getForm( className: string ): TForm;
function CastToMethod( code: pointer; data: pointer = nil ): TMethod;
function PtInControls( pt: TPoint; const cc: array of TControl ): boolean;

implementation

uses shellapi, shlobj, activex, messages, typinfo, stdctrls,
  comctrls;

function IsCardinal( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := (code = 0) and (v >= 0);
end;

{$HINTS OFF}

function IsInteger( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := code = 0;
end;

function IsFloat( s: string ): boolean;
var
  V: extended;
begin
  Result := TextToFloat(PChar(S), V, fvExtended);
end;
{$HINTS ON}

{from implementation of sysutils}
function GetModuleName(Module: HMODULE): string;
var
  ModName: array[0..MAX_PATH] of Char;
begin
  SetString(Result, ModName, Windows.GetModuleFileName(Module, ModName, SizeOf(ModName)));
end;



function GetAppDir: string;
begin
  Result := ExtractFilePath( GetModuleName( 0 ) );
end;

function GetAppFilename: string;
begin
  Result := ExtractFileName( GetModuleName( 0 ) );
end;


function PathFromIDList( pidl: PItemIDList): string;
var
  Buffer: pchar;
  ShellMalloc: IMalloc;
begin
  Result := '';
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  try
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      ShGetPathFromIDList( pidl, Buffer );
      SetString( Result, Buffer, strlen(Buffer) );
    finally
      ShellMalloc.Free(Buffer);
    end;
  finally
  end;
end;




function MyBrowseCallBackProc(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
var
  s: string;
begin
  try
    if uMsg = BFFM_INITIALIZED then
      SendMessage( Wnd, BFFM_SETSELECTION, 1, lpData )
    else if uMsg = BFFM_SELCHANGED then
    begin
      SetString( s, pchar(lpdata), strlen(pchar(lpdata)) );
      SetLength( s, LastDelimiter('\:',s)-1 );
//      ODS( '%s %s',[PathFromIDList(PItemIDList(lParam)), s]);
      SendMessage( Wnd, BFFM_ENABLEOK, 0, ord( PathFromIDList(PItemIDList(lParam)) <> s )  );
    end;
  except
  end;
  Result := 0;
end;




function MySelectDirectory(  const Caption: string;  var Directory: string; const Root: WideString; parent: TwinControl; allowCreate: boolean ): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  ItemIDList, RootItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;
  initDir: string;
begin
  Result := False;
  initDir := Directory;
  Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(parent.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := parent.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_VALIDATE;
        if allowCreate then
          ulFlags := ulFlags or $40 ;// or BIF_EDITBOX;//
        lParam := integer(pchar(initDir));
        lpfn := @MyBrowseCallBackProc;
      end;
      WindowList := DisableTaskWindows(0);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

procedure StringToFile( s: string; fname: string );
var
  ostr: TEvFileStream;
begin
  ostr := TEvFileStream.Create( fname, fmCreate or fmShareExclusive );
  try
    ostr.Write( pchar(s)^, length(s) );
  finally
    FreeAndNil( ostr );
  end;
end;


function  FileToString( fname: string ): string;
var
  istr: TEvFileStream;
begin
  Result := '';
  istr := TEvFileStream.Create( fname, fmOpenRead or fmShareExclusive );
  try
    SetLength( Result, istr.Size );
    istr.Read( pchar(Result)^, Length( Result ) );
  finally
    FreeAndNil( istr );
  end;

end;

procedure MB( const msg: string );
begin
  MessageBox(0,pchar(msg),'MB',0);
end;

procedure MB( const msg: string; const Args: array of const);
begin
  MB( Format( msg, Args ) );
end;

procedure Log(const Fmt: string; const Args: array of const); overload;
begin
  Log( Format( Fmt, Args ) );
end;

procedure Log(const msg: string); overload;
begin
 Log( ChangeFileExt(GetModuleName(0),'.log'), msg );
end;

procedure Log(const filename: string; const msg: string); overload;
var
 log: THANDLE;
 written: DWORD;
begin
 log := CreateFile( pchar(filename),
                    GENERIC_WRITE,0{no share},
                    nil{secatt},
                    OPEN_ALWAYS,
                    FILE_ATTRIBUTE_NORMAL or FILE_FLAG_WRITE_THROUGH,0);
 SetFilePointer(log,0,nil,FILE_END);
 WriteFile(log,pchar(msg)^,length(msg), written,nil);
 CloseHandle(log);
end;

{$ifdef GDY_COMMON}
procedure ODS(const Fmt: string; const Args: array of const); overload;
begin
  ODS( Format( Fmt, Args ) );
end;

procedure ODS(const msg: string); overload;
var
  s: string;
begin
  s := msg + #13 + #10;
  OutputDebugString(pchar(s));
end;
{$endif}

procedure SetTag( menu: TMenuItem; value: integer );
var
  i: integer;
begin
  for i := 0 to menu.Count - 1 do
    menu.Items[i].Tag := maxint;
end;

procedure DeleteWithTag( menu: TMenuItem; value: integer );
var
  i: integer;
begin
  i := 0;
  while i < menu.Count do
    if menu.Items[i].Tag = maxint then
      menu.Delete(i)
    else
      inc(i);
end;

procedure FreeObjects( list: TStrings );
var
  i: integer;
begin
  if assigned( list )  then
    for i := 0 to list.count-1 do
    begin
      list.Objects[i].Free;
      list.Objects[i] := nil;
    end
end;

procedure FreeObjects( stack: TObjectStack );
begin
  if stack <> nil then
    while stack.Count > 0 do
      stack.Pop.Free;
end;

function PopupMenuPos( btn: TControl; isLeft: boolean; istop: boolean  ): TPoint;
var
  p: TPoint;
begin
  if istop then
    p.y := btn.Top
  else
    p.y := btn.Top + btn.Height;
  if isleft then
    p.X := btn.Left
  else
    p.X := btn.Left + btn.Width;

  Result := btn.Parent.ClientToScreen( p );
  if isleft then
  begin
    if (Result.x < 0 ) then
      p.x := btn.Left + btn.Width;
  end
  else
  begin
    Result := btn.Parent.ClientToScreen( p );
    if (Result.x >= Screen.Width) then
      p.x := btn.Left;
  end;
  if istop then
  begin
    if (Result.y < 0 ) then
      p.y := btn.Top + btn.Height;
  end
  else
  begin
    if (Result.y >= Screen.Height) then
      p.y := btn.Top;
  end;
  Result := btn.Parent.ClientToScreen( p );

end;

function WaitForKey( aMsgToWait: cardinal ): boolean;
var
  msg: TMsg;
begin
  Result := false;
  while PeekMessage( msg,0,WM_KEYFIRST, WM_KEYLAST, PM_REMOVE ) do
    if ( msg.message = aMsgToWait ) and (msg.wParam = VK_ESCAPE) then
      Result := true;
end;

function VersionInfoAsString: string;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
begin
  Result := '';
  FileName := GetModuleName(0);
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
        if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
          Result := Format( '%d.%d.%d ������ %d', [
          (FI.dwFileVersionMS shr 16) and $ffff, FI.dwFileVersionMS and $ffff,
          (FI.dwFileVersionLS shr 16) and $ffff, FI.dwFileVersionLS and $ffff ]);
    finally
      FreeMem(VerBuf);
    end;
  end;
end;

function RandomInteger(iLow, iHigh: Integer): Integer;
begin
  result := Trunc(Random(iHigh - iLow)) + iLow;
end;

function RandomString(iLength: Integer): String;
begin
  result := '';
  while Length(result) < iLength do
    result := result + IntToStr(RandomInteger(0, High(Integer)));
  if Length(result) > iLength then
    result := Copy(result, 1, iLength);
end;

function BinToHex(s: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length( S ) do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

function HexToBin(s: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length( S ) do
  begin
    if ((i mod 2) = 1) then
      Result := Result + Chr( StrToInt( '0x' + Copy( S, i, 2 )));
  end;
end;

function Plural( s: string ): string; //not complete --doesnt handle -y
const
  es_suffixes: array [0..4] of string = (
    'o','s','ch','sh','x'
  );
var
  i: integer;
begin
  for i := low(es_suffixes) to high(es_suffixes) do
    if copy( s, Length(s)-Length(es_suffixes[i])+1, Length(es_suffixes[i]) ) = es_suffixes[i] then
    begin
      Result := s + 'es';
      exit;
    end;
  Result := s + 's';
end;

function Plural( s: string; count: integer ): string; overload;
begin
  if count = 1 then
    Result := s
  else
    Result := Plural( s );
end;

procedure StreamToFile( aStream: TStream;  filename: string );
begin
  with TEvFileStream.Create( FileName, fmCreate ) do
  try
    CopyFrom( aStream, 0 );
  finally
    Free;
  end;
end;

procedure FileToStream( filename: string; aStream: TStream );
var
  fileStream: TEvFileStream;
begin
  fileStream := TEvFileStream.Create( FileName, fmOpenRead );
  try
    aStream.CopyFrom( fileStream, 0 );
  finally
    FreeAndNil( fileStream );
  end;
end;

function GetOwningForm( frame: TComponent ): TCustomForm;
var
  r: TComponent;
begin
  r := frame.Owner;
  while ( r <> nil ) and not (r is TCustomForm) do
    r := r.Owner;
  Result := r as TCustomForm;
end;

//SEG
function ShadeColor(InColor : TColor; ShadingDepth: Integer) : TColor;
begin
 Result := ColorToRGB(InColor);
 Result :=
  ((Result mod 256) * ShadingDepth div 6) +
  ((Result div 256 mod 256) * ShadingDepth div 6) * 256 +
  ((Result div 256 div 256 mod 256) * ShadingDepth div 6) * 256 * 256;
end;

function GetUserName: string;
const
//  EXTENDED_NAME_FORMAT = (
    NameUnknown = 0;
    NameFullyQualifiedDN = 1;
    NameSamCompatible = 2;
    NameDisplay = 3;
    NameUniqueId = 6;
    NameCanonical = 7;
    NameUserPrincipal = 8;
    NameCanonicalEx = 9;
    NameServicePrincipal = 10;

var
 GetUserNameExA: function ( NameFormat: dword; lpBuffer: PChar; var nSize: DWORD): BOOL; stdcall;

const
  UNLEN = 2048; // ;-)
var
  uName: array[0..UNLEN] of char;
  len: DWORD;
  hSecur32: THandle;
begin
  Result := '';
  hSecur32 := LoadLibrary( 'secur32.dll' );
  if hSecur32 <> 0 then
  begin
    GetUserNameExA := GetProcAddress( hSecur32, 'GetUserNameExA');
    if assigned(GetUserNameExA) then
    begin
      len := SizeOf(uName);
//      win32check(GetUserNameExA(NameDisplay, uName, len));
      if GetUserNameExA(NameDisplay, uName, len) then
        SetString(Result, uName, len);
    end;
  end;

  if Result = '' then
  begin
    len := SizeOf(uName);
    if Windows.GetUserName(uName, len) then
      SetString(Result, uName, len);
  end;
end;

function WithoutTrailingSlash( const s: string): string;
begin
  Result := s;
  if Length(Result) > 0 then
    if Result[Length(Result)] = '\' then
      SetLength( Result, Length(Result)-1 );
end;

procedure EnableControl( control: TComponent; enable: boolean );
begin
  if IsPublishedProp( control, 'ReadOnly' ) then
    if enable then
      SetEnumProp( control, 'ReadOnly', 'false' )
    else
      SetEnumProp( control, 'ReadOnly', 'true' )
  else
    if IsPublishedProp( control, 'Enabled' ) then
      if enable then
        SetEnumProp( control, 'Enabled', 'true' )
      else
        SetEnumProp( control, 'Enabled', 'false' );

  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) and ((control is TCustomEdit) or (control is TCustomComboBox) or (control is TCustomTreeView)) then
    if enable then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      SetEnumProp( control, 'ParentColor', 'true' );
    end;
end;


procedure CheckColor(const control: TControl; const grayed: Boolean);
begin
  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) then
    if not grayed then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      SetEnumProp( control, 'ParentColor', 'true' );
    end;
end;

function EnsureHasMenuItem( Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu;//gdy22
var
  mi: TMenuItem;
  i: integer;
const
  sPopupMenuName = 'DynamicPopupMenu';
begin
  Result := pm;
  if not Assigned( Result ) then begin
    Result := Self.FindComponent( sPopupMenuName ) as TPopupMenu;
    if not Assigned( Result ) then begin
      Result := TPopupMenu.Create(Self);
      Result.Name := sPopupMenuName;
    end;
  end;

  mi := nil;
  for i := 0 to Result.Items.Count-1 do
    if Result.Items[i].Action = actionInstance then
    begin
      mi := Result.Items[i];
      break;
    end;

  if not assigned( mi ) then begin
    mi := TMenuItem.Create(Self);
    mi.Action := actionInstance;
    Result.Items.Add(mi);
  end;
end;



// from jcl

function FileGetBasedTempName( const filename: string ): string;
var
  TempPath: string;
  R: Cardinal;
  sfx: integer;
  ext: string;
  fn: string;
begin
  Result := '';
  R := GetTempPath(0, nil);
  SetLength(TempPath, R-1); // R is buffer size, SetLength takes string length (i.e. number of characters)
  R := GetTempPath(R, PChar(TempPath));
  if R <> 0 then
  begin
    Result := TempPath + ExtractFileName( filename );
    sfx := 0;
    ext := ExtractFileExt( filename );
    fn := ExtractFileName( filename );
    while FileExists( Result ) do
    begin
      Result := TempPath + copy( fn, 1, length(fn)-length(ext) )+ '_'+inttostr(sfx)+ext;
      inc(sfx);
    end
  end
  else
{$IFNDEF VER130}
{$WARN SYMBOL_DEPRECATED OFF}
{$ENDIF}
    RaiseLastWin32Error;
{$IFNDEF VER130}
{$WARN SYMBOL_DEPRECATED ON}
{$ENDIF}
end;

procedure DrawProgressIndicator( aCanvas: TCanvas; aRect: TRect; aOperationName: string; aCurrent, aTotal: integer );
var
  oldBrushColor, oldFontColor: TColor;
  done: integer;
  percent: integer;
  r: TRect;
  s: string;
begin
  with aCanvas do
  begin
    if aTotal <> 0 then
    begin
      done := (aRect.right - aRect.Left)*aCurrent div aTotal;
      percent := aCurrent * 100 div aTotal;
    end
    else
    begin
      done := 0;
      percent := 0;
    end;
    s := Format(aOperationName,[aCurrent, aTotal, percent]);
//      s := FOperationName;
    r.Top := aRect.Top;
    r.Bottom := aRect.Bottom;

    oldBrushColor := Brush.Color;
    oldFontColor := Font.Color;

    Brush.Color := clActiveCaption;
    Font.Color := clCaptionText;
    r.Left := aRect.Left;
    r.Right := aRect.Left + done;
    TextRect(r,2,2, s); //!!hardcoded indent
    Brush.Color := oldBrushColor;
    Font.Color := oldFontColor;
    r.Left := aRect.Left+done+1;
    r.Right := aRect.Right;
    TextRect(r,2,2, s); //!!hardcoded indent
  end;
end;

procedure RedrawIfNeeded;
var
  Msg: TMsg;
begin
//  OldOnException := Application.OnException;
//  Application.OnException := DummyException;
    while PeekMessage(Msg, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
      DispatchMessage(Msg);
//  end;
end;

function IndexOfText( sl: TStrings; w: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to sl.count-1 do
    if AnsiSameText( trim(sl[i]), trim(w) ) then
    begin
      Result := i;
      break;
    end;
end;

function IndexOfNameText( sl: TStrings; w: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to sl.count-1 do
    if AnsiSameText( trim(sl.Names[i]), trim(w) ) then
    begin
      Result := i;
      break;
    end;
end;

function PluralIndex( n: integer ): integer;
begin
//n%100 / 10 ==1 ? 2 : n%10==1 ? 0: (n+9)%10 > 3 ? 2:1
  if ((n mod 100) div 10) = 1 then
    Result := 2
  else if (n mod 10) = 1 then
    Result := 0
  else if (( n + 9 ) mod 10) > 3 then
    Result := 2
  else
    Result := 1;
end;

function Plural( n: integer; forms: array of string ): string;
begin
  Result := forms[PluralIndex(n)];
end;

function GetForm( className: string ): TForm;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to screen.FormCount-1 do
    if screen.Forms[i].ClassName = className then
    begin
      Result := screen.forms[i];
      break;
    end;
end;

function CastToMethod( code, data: pointer ): TMethod;
begin
  Result.Code := code;
  Result.Data := data;
end;

function PtInControls( pt: TPoint; const cc: array of TControl ): boolean;
var
  i: integer;
begin
  Result := false;
  for i := low(cc) to high(cc) do
    if PtInRect( cc[i].BoundsRect, pt ) then
    begin
      Result := true;
      break;
    end;
end;


end.


