object ImportLoggerViewFrame: TImportLoggerViewFrame
  Left = 0
  Top = 0
  Width = 651
  Height = 536
  TabOrder = 0
  object pnlLog: TevPanel
    Left = 0
    Top = 0
    Width = 651
    Height = 536
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pgLogView: TevPageControl
      Left = 200
      Top = 272
      Width = 371
      Height = 264
      ActivePage = tbshUserView
      TabOrder = 0
      TabPosition = tpBottom
      Visible = False
      object tbshUserView: TTabSheet
        Caption = 'User Log'
      end
      object tbshDevView: TTabSheet
        Caption = 'Full Log'
        ImageIndex = 1
        object pnlDevView: TevPanel
          Left = 0
          Top = 0
          Width = 363
          Height = 238
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object mmDevView: TevMemo
            Left = 0
            Top = 0
            Width = 363
            Height = 238
            Align = alClient
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 0
            SpellcheckEnabled = False
          end
        end
      end
      object tbshUserTextView: TTabSheet
        Caption = 'User Log As Text'
        ImageIndex = 2
        object pnlUserTextView: TevPanel
          Left = 0
          Top = 0
          Width = 363
          Height = 238
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object mmUserTextView: TevMemo
            Left = 0
            Top = 0
            Width = 363
            Height = 238
            Align = alClient
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 0
            SpellcheckEnabled = False
          end
        end
      end
    end
    inline LoggerRichView: TImportLoggerRichViewFrame
      Left = 0
      Top = 0
      Width = 651
      Height = 536
      Align = alClient
      TabOrder = 1
      inherited pnlUserView: TevPanel
        Width = 651
        Height = 536
        inherited evSplitter1: TevSplitter
          Width = 651
        end
        inherited pnlTopPart: TevPanel
          Width = 651
          inherited lvMessages: TevListView
            Width = 651
          end
        end
        inherited pnlBottom: TevPanel
          Width = 651
          Height = 298
          inherited evSplitter2: TevSplitter
            Left = 299
            Height = 298
          end
          inherited pnlLeft: TevPanel
            Width = 299
            Height = 298
            inherited mmDetails: TevMemo
              Width = 299
              Height = 273
            end
            inherited evPanel1: TevPanel
              Width = 299
            end
          end
          inherited pnlRight: TevPanel
            Left = 304
            Height = 298
            inherited mmContext: TevMemo
              Height = 273
            end
          end
        end
      end
    end
  end
end
