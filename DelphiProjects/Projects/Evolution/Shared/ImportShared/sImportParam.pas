// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportParam;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  sImportVisualcontroller, EvBasicUtils,
  ISBasicClasses, EvUIComponents;

type
  TImportParamFrame = class(TFrame, IParamInputFrame)
    cbTerm: TevCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

implementation

{$R *.dfm}

{ TImportParamFrame }

procedure TImportParamFrame.ControlsToParams(aParams: TStrings);
begin
  aParams.Values[sEETermedParam] := BoolToYN(cbTerm.Checked);
end;

procedure TImportParamFrame.ParamsToControls(aParams: TStrings);
begin
  cbTerm.Checked := aParams.Values[sEETermedParam] = 'Y';
end;

end.
