// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit genericImportImpl;
{$include gdy.inc}
interface
uses
  iemap, classes, genericImport, EvTypes, EvCommonInterfaces, EvUIUtils;

type
  TTableImporter = class
  private
    FIEMap: IIeMap;
  protected
    FLogger: IImportLogger;
    FTablespace: ITableSpace;
    FIDef: IImportDefinition;
    FUpdateRecord: TUpdateRecordArray;
    FEffectiveDate: TDateTime;
    FTableName: string;
    procedure AddUpdateRecord( output: TFieldValues; aValueKeys: TFieldValues );
    function ImportValue( run: integer; aTableName, aFieldName: string; aFieldValue: Variant; aRow, aRowKeys: TFieldValues ): boolean;
    procedure DoPostUpdates;
  public
    constructor Create(const AIEMap: IIeMap; aLogger: IImportLogger; aTableSpace: ITablespace; aIDef: IImportDefinition );
    destructor Destroy; override;
    procedure ImportTable( tn: string; aTaskName: string; postAfterEachRecord: boolean );
  end;

implementation

uses
  db, sysutils, evutils, messages, dialogs, controls,
  math, windows{$ifdef D6_UP},variants{$endif}, dateutils, isutils, EvContext;


function DumpUpdateRecord( ur: TUpdateRecord ): string;
var
  i: integer;
begin
  with ur do
  begin
    Result := 'Keys are ('+Inttostr(Length(KeyTables))+'): '+#13#10;
    for i := 0 to Length(KeyTables)-1 do
      Result := Result + KeyTables[i]+'.'+KeyFields[i]+'='+VarToStr(KeyValues[i])+#13#10;
    Result := Result + 'Value: '+Table+'.'+Field+'=';
    if VarIsArray(Value) then
      Result := Result + 'Lookup ' + VarToStr(Value[0] + '.' + Value[1]) + ' where ' + VarToStr(Value[0] + '.' + Value[3]) + '=' + VarToStr(Value[2])
    else
      Result := Result + VarToStr(Value);
    Result := Result + #13#10+'Effective Date='+DateTimeToStr(EffectiveDate);
  end;
end;

{TTableImporterBase}
constructor TTableImporter.Create(const AIEMap: IIEMap; aLogger: IImportLogger; aTableSpace: ITablespace; aIDef: IImportDefinition );
begin
  FLogger := aLogger;
  FTableSpace := aTableSpace;
  FEffectiveDate := SysTime;
  FIDef := aIDef;
  FIEMap := AIEMap;
end;

function CheckEscape: boolean;
begin
  Result := isutils.CheckEscape;
  if result then
    Result := EvMessage( 'Do you really want to abort import?', mtConfirmation, [mbYes, mbNo], mbNO ) = mrYes;
end;

//maximum cardinality of one to many relationship in mapping table
function CalcMaxRunForTable( aMap: TMapRecords; aTableName: string ): integer;
  function GetRunSpecifier( aFieldName: string ): integer;
  var
    p: integer;
  begin
    p := Pos( '@', aFieldName );
    if p = 0 then
      Result := 0
    else
      Result := StrToInt( Copy( aFieldName, p+1, Length(aFieldName)-p) );
  end;
var
  i: integer;
begin
  Result := 0;
  for i := Low(aMap) to High(aMap) do
    if AnsiSameText(aMap[i].ITable, aTableName) then
      Result := max( Result, GetRunSpecifier(aMap[i].IField ));
end;

procedure TTableImporter.ImportTable( tn: string; aTaskName: string; postAfterEachRecord: boolean );
var
  prevRowKeys: TFieldValues;
var
  t: TDataSet;
  maxrun: integer;
  procedure ProcessRow;
  var
    row: TFieldValues;
    rowKeys: TFieldValues;

    procedure CustomImportRow;
      procedure ExecuteCustomImportRow( aFuncName: string );
      var
        output: TFieldValues;
        customRowKeys: TFieldValues;
      begin
        customRowKeys := TFieldValues.Create;
        try
          customRowKeys.Assign( rowKeys );
          output := TFieldValues.Create;
          try
            FIDef.ExecuteRowCustomFunc( aFuncName, row, customRowKeys, output );
            if output.Count <> 0 then
              FLogger.LogDebug( Format('Custom row procedure <%s> called', [aFuncName]), 'keys values are ' + customRowKeys._Dump );
            AddUpdateRecord( output, customRowKeys );
          finally
            FreeAndNil( output );
          end;
        finally
          FreeAndNil(customRowKeys);
        end;
      end;
    var
      i: integer;
    begin
      for i := low(FIDef.RowImport) to high(FIDef.RowImport) do
        if FIDef.RowImport[i].ExternalTable = FTableName then
          ExecuteCustomImportRow( FIDef.RowImport[i].CustomFunc );
    end;
    procedure ReadRow;
    var
      i: integer;
    begin
      for i := 0 to t.FieldCount-1 do
      begin
        FLogger.LogEntry( 'ReadValue' );
        try
          FLogger.LogContextItem( sciExternalField, FTableName + '.' + t.Fields[i].FieldName + ' = <' + VarToStr(t.Fields[i].Value) + '>' );
          try
            row.Add( FTableName, t.Fields[i].FieldName, FIDef.ConvertValue( FTableName, t.Fields[i].FieldName, t.Fields[i].Value ) );
          except
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
      end;
    end;
    procedure ImportRow;
    var
      i: integer;
      run: integer;
    begin
      FLogger.LogEntry( 'ImportRow' );
      try
        try
          FLogger.LogContextItem( sciRowKeys, rowKeys._Dump );
          for run := 0 to maxrun do
            for i := 0 to row.Count-1 do
              with row do
                ImportValue( run, Tables[i], Fields[i], Values[i], row, rowKeys );
          CustomImportRow;
        except
          FLogger.PassthroughException;
        end;
      finally
        FLogger.LogExit;
      end;
    end;
  begin
    rowKeys := TFieldValues.Create;
    try
      row := TFieldValues.Create;
      try
        try
          ReadRow;
          FIDef.GetRowKeys( FTablespace, row, rowkeys, FTableName );
          if postAfterEachRecord or not rowKeys.IsEqual( prevRowKeys ) then
          try
            FLogger.LogEntry( 'PostUpdates' );
            try
              try
                FLogger.LogContextItem( sciRowKeys, prevRowKeys._Dump );
                DoPostUpdates;
              except
                FLogger.PassthroughException;
              end;
            finally
              FLogger.LogExit;
            end;
          finally
            prevRowKeys.Assign( rowKeys );
          end;
          ImportRow;
        except
          FLogger.StopException;
        end;
      finally
        FreeAndNil( row );
      end;
    finally
      FreeAndNil( rowKeys )
    end;
  end;
var
  RecNo: Integer;
begin
  FTableName := tn;
  t := FTableSpace.GetTable( FTableName );
  maxrun := CalcMaxRunForTable( FIDef.Map, FTableName );
  prevRowKeys := TFieldValues.Create;
  try
    t.First;
    RecNo := 1;
    if aTaskName = '' then
      aTaskName := Format('Importing from %s table',[FTableName]);
    aTaskName := aTaskName + #13#10 + 'Press Esc to abort';
    ctx_StartWait( aTaskName, t.RecordCount );
    try
      try
        while not t.Eof do
        begin
          if CheckEscape then
            raise EAbortTableImport.Create('User break.');
          ProcessRow;
          ctx_UpdateWait( aTaskName, RecNo );
          t.Next;
          Inc(RecNo);
        end;
      finally
        DoPostUpdates;
      end;
    finally
      ctx_EndWait;
    end;
  finally
    FreeAndNil( prevRowKeys );
  end;
end;

function TTableImporter.ImportValue( run: integer; aTableName, aFieldName: string; aFieldValue: Variant; aRow, aRowKeys: TFieldValues ): boolean;
var
  output: TFieldValues;
  valueKeys: TFieldValues;
  fieldnameForMapValue: string;
  i: integer;
  nRowKeys: integer;
  UsedMapRecord: IEvImportMapRecord;
begin
  Result := false; // to make compiler happy
  valueKeys := TFieldValues.Create;
  try
    valueKeys.Assign( aRowKeys );
    if run <> 0 then
      fieldnameForMapValue := aFieldName + '@' + inttostr(run)
    else
      fieldnameForMapValue := aFieldName;
    nRowKeys := valueKeys.Count;
    FIDef.AddFieldKeys( FTablespace, aRow, valueKeys, aTableName, fieldnameForMapValue );
    FLogger.LogEntry( 'ImportValue' );
    try
      try
        if valueKeys.Count <> nRowKeys then
          FLogger.LogContextItem( sciFieldKeys, valueKeys.DumpLast( valueKeys.Count - nRowKeys ) );
        output := TFieldValues.Create;
        try
          Result := FIEMap.MapValue( true, aTableName, fieldnameForMapValue, aFieldValue, valueKeys.Tables, valueKeys.Fields, valueKeys.Values, output.Tables, output.Fields, output.Values, UsedMapRecord);
          if Result then
          begin
            FLogger.LogDebug( Format('ImportValue: %s.%s = <%s>', [aTableName, aFieldName, VarToStr(aFieldValue) ]), '' );
            AddUpdateRecord( output, valueKeys );
          end
          else
          begin
            for i := Low(FIDef.Map) to High(FIDef.Map) do
              if AnsiSameText(FIDef.Map[i].ITable, aTableName) and AnsiSameText(FIDef.Map[i].IField, fieldnameForMapValue)
                 and (FIDef.Map[i].MapType <> iemap.mtCustom)
                 then
              begin
                //!!is it reason to stop import of the current row or not?
                FLogger.LogError( Format('Cannot map %s.%s(%d)',[aTableName, aFieldName, run]));
                break;
              end;
          end;
        finally
          FreeAndNil( output );
        end;
      except
        FLogger.PassthroughException;
      end;
    finally
      FLogger.LogExit;
    end;
  finally
    FreeAndNil(valueKeys);
  end;
end;

procedure TTableImporter.AddUpdateRecord( output: TFieldValues; aValueKeys: TFieldValues );
var
  i, j: integer;
  ur: ^TUpdateRecord;
begin
  for i := 0 to output.Count-1 do
  begin
    SetLength(FUpdateRecord, Length(FUpdateRecord) + 1 );
    ur := @FUpdateRecord[High(FUpdateRecord)];
    SetLength(ur^.KeyTables, aValueKeys.Count );
    SetLength(ur^.KeyFields, aValueKeys.Count );
    SetLength(ur^.KeyValues, aValueKeys.Count );
    for j := 0 to aValueKeys.Count-1 do
    begin
      ur^.KeyTables[j] := aValueKeys.Tables[j];
      ur^.KeyFields[j] := aValueKeys.Fields[j];
      ur^.KeyValues[j] := aValueKeys.Values[j];
    end;
    ur^.Table := output.Tables[i];
    ur^.Field := output.Fields[i];
    ur^.Value := output.Values[i];
    ur^.EffectiveDate := FEffectiveDate;
  end;
end;

procedure TTableImporter.DoPostUpdates;
  function DumpUpdateRecords: string;
  var
    i: integer;
  begin
    Result := '';
    for i := 0 to Length(FUpdateRecord)-1 do
      Result := Result + #13#10 + DumpUpdateRecord(FUpdateRecord[i]);
  end;
begin
  FLogger.LogDebug('DoPostUpdates: '+DumpUpdateRecords);
  if Length( FUpdateRecord ) > 0 then
  try
    try
      FIEMap.PostUpdates( FUpdateRecord );
    except
      FLogger.StopException; //AndWarnFmt( 'Failed to post updates to server', [] );
    end;
  finally
    SetLength( FUpdateRecord, 0 );
  end;
end;

destructor TTableImporter.Destroy;
begin
  inherited;
  FIDef := nil;
end;


//          raise EevException.Create( 'test1'#13#10'test2' );

end.


