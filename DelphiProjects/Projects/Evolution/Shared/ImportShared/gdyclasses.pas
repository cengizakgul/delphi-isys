// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyClasses;
{$INCLUDE gdy.inc}
interface

uses
  controls, classes, comctrls, windows, evExceptions;

type
  IUsageLock = interface
    ['{E7184E40-066E-450E-964C-FCDDFE2680FD}']
    procedure Lock;
    procedure Unlock;
    function  IsLocked: boolean;
  end;

  IWaitIndicator = interface
  ['{0A29B7EB-0DC2-40A4-8F6F-B22B7490A31E}']
    procedure ctx_StartWait;
    procedure ctx_EndWait;
  end;

  IStringMapping = interface
  ['{DED328F9-B423-4BCD-9235-4A6AEA1CE85C}']
    function CreateMapping( s: string ): integer;
    function GetMapping( tag: integer ): string;
  end;

  IProgressIndicator = interface
  ['{3DD66A5F-EB4B-4D6F-BDF2-B57966721E8B}']
    procedure ctx_StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure ctx_EndWait;
  end;

  TUsageLock = class( TInterfacedObject, IUsageLock )
  private
    FLockCount: cardinal;
  public
    procedure Lock;
    procedure Unlock;
    function  IsLocked: boolean;
    procedure Reset;
    property LockCount: cardinal read FLockCount write FLockCount;
  end;

  TWaitIndicator = class( TInterfacedObject, IWaitIndicator )
  private
    FOldScreenCursor: TCursor;
    FCursorChangeCount: integer;
  public
    procedure ctx_StartWait;
    procedure ctx_EndWait;
  end;

{$ifdef GDYCOMMON}
  TStatusBarProgressIndicator = class( TInterfacedObject, IProgressIndicator )
  private
    FStatusBar: TStatusBar;
    FTotal, FCurrent: integer;
    FCaption: string;
    FOldDrawPanelHandler: TDrawPanelEvent;
    procedure ctx_UpdateWait;
    procedure StatusBarDrawPanel(
        StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);

  public
    constructor Create( sb: TStatusBar );
    procedure ctx_StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure ctx_EndWait;
  end;
{$endif}

  TStringMapping = class( TInterfacedObject, IStringMapping )
  private
    FMapping: TStringList;
  public
    constructor Create;
    destructor Destroy; override;

    {IStringMapping}
    function CreateMapping( s: string ): integer;
    function GetMapping( tag: integer ): string;
  end;

  TDummyUnknownImpl = class( TObject, IUnknown )
  public
    // mimics TComponent implementation of IUnknown
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

  TNexusEvent = procedure(Sender: TObject; AComponent: TComponent) of object;

  TNexus = class(TComponent)
  private
    FOnFreeNotify: TNexusEvent;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    property OnFreeNotify: TNexusEvent read FOnFreeNotify write FOnFreeNotify;
  end;

  IStr = interface
['{378231A5-8033-43A6-83F3-A6FBFAA7899F}']
    procedure SetDelimitedText( text: string; delim: string );
    procedure SetText( text: string );
    function Count: integer;
    function GetStr(Idx: integer): string;
    property Str[Idx: integer]: string read GetStr;
  end;

function SplitStr( text, delim: string ): IStr;
function ListAsStr( text: string ): IStr;

type

  TKeyPairList = class
  private
    FList: Variant;
    procedure IncLength;
    function GetLeftKey(i: integer): Variant;
    function GetRightKey(i: integer): Variant;
  public
//    constructor Create;
//    destructor Destroy; override;
    procedure AddPair( left, right: Variant );
    function Count: integer;
    property LeftKey[i: integer]: Variant read GetLeftKey;
    property RightKey[i: integer]: Variant read GetRightKey;
  end;



implementation

uses
  sysutils, 
  forms 
  {$ifdef GDYCOMMON}, gdycommon{$ENDIF}
  {$ifdef D6_UP}, variants{$endif};

{ TUsageLock }

function TUsageLock.IsLocked: boolean;
begin
  Result := FLockCount > 0;
end;

procedure TUsageLock.Lock;
begin
  inc( FLockCount );
end;

procedure TUsageLock.Reset;
begin
  LockCount := 0;
end;

procedure TUsageLock.Unlock;
begin
  if FLockCount > 0 then
    dec( FLockCount )
  else
    raise EevException.Create( 'Unlock called for non-locked object' );
end;


{ TWaitIndicator }

procedure TWaitIndicator.ctx_StartWait;
begin
  inc( FCursorChangeCount );
  if FCursorChangeCount = 1 then
  begin
    FOldScreenCursor := Screen.Cursor;
    Screen.Cursor := crHourGlass;
  end;
end;

procedure TWaitIndicator.ctx_EndWait;
begin
  if FCursorChangeCount > 0 then
  begin
    dec( FCursorChangeCount );
    if FCursorChangeCount = 0 then
      Screen.Cursor := FOldScreenCursor;
  end;
end;


{ TStringMapping }

constructor TStringMapping.Create;
begin
  FMapping := TStringList.Create;
end;

function TStringMapping.CreateMapping(s: string): integer;
begin
  Result := FMapping.IndexOf( s );
  if Result = -1 then
    Result := FMapping.Add( s );
end;

destructor TStringMapping.Destroy;
begin
  FreeAndNil( FMapping );
  inherited;
end;

function TStringMapping.GetMapping(tag: integer): string;
begin
  Result := FMapping[tag];
end;

function TDummyUnknownImpl.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;

function TDummyUnknownImpl._AddRef: Integer;
begin
  Result := -1   // -1 indicates no reference counting is taking place
end;

function TDummyUnknownImpl._Release: Integer;
begin
  Result := -1   // -1 indicates no reference counting is taking place
end;

{$ifdef GDYCOMMON}

{ TStatusBarProgressIndicator }

constructor TStatusBarProgressIndicator.Create(sb: TStatusBar);
begin
  FStatusBar := sb;
end;

procedure TStatusBarProgressIndicator.ctx_EndWait;
begin
  try
    FCaption := '';
    FCurrent := 0;
    FTotal := 0;
    FStatusBar.Repaint;
    FStatusBar.OnDrawPanel := FOldDrawPanelHandler;
  except
  end;
end;

procedure TStatusBarProgressIndicator.ctx_StartWait(name: string;
  total: integer);
begin
  try
    FCaption := name;
    FCurrent := 0;
    FTotal := total;
    ctx_UpdateWait;
    FOldDrawPanelHandler := FStatusBar.OnDrawPanel;
    FStatusBar.OnDrawPanel := StatusBarDrawPanel;
  except
  end;
end;

procedure TStatusBarProgressIndicator.StatusBarDrawPanel(
  StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin
  try
    if Panel.Index = 0 then
      DrawProgressIndicator( StatusBar.Canvas, Rect, FCaption, FCurrent, FTotal );
    if assigned(FOldDrawPanelHandler) then
      FOldDrawPanelHandler( StatusBar, Panel, Rect )
  except
  end;
end;

procedure TStatusBarProgressIndicator.Step(step: integer);
begin
  try
    FCurrent := step;
    ctx_UpdateWait;
  except
  end;
end;

procedure TStatusBarProgressIndicator.ctx_UpdateWait;
begin
  FStatusBar.Repaint;
  RedrawIfNeeded;
end;
{$endif}
{ TNexus }

procedure TNexus.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  if (Operation = opRemove) and Assigned(FOnFreeNotify) then
    FOnFreeNotify(Self, AComponent);
  inherited Notification(AComponent, Operation);
end;

type
  TStr = class (TInterfacedObject, IStr)
  private
    FSL: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetDelimitedText( text: string; delim: string );
    procedure SetText( text: string );
    function Count: integer;
    function GetStr(Idx: integer): string;
    property Str[Idx: integer]: string read GetStr;
  end;


{ TStr }

function TStr.Count: integer;
begin
  Result := FSL.Count;
end;

constructor TStr.Create;
begin
  FSL := TStringList.Create;
end;

destructor TStr.Destroy;
begin
  FreeAndNil( FSL );
  inherited;
end;

function TStr.GetStr(Idx: integer): string;
begin
  Result := FSL[idx];
end;
//!! shame on me; rewrite someday
procedure TStr.SetDelimitedText(text: string; delim: string);
begin
  FSL.CommaText := sysutils.StringReplace( text, delim, ',', [rfReplaceAll]);
end;

procedure TStr.SetText(text: string);
begin
  FSL.Text := text;
end;

function SplitStr( text, delim: string ): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetDelimitedText( text, delim );
end;

function ListAsStr( text: string ): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetText( text );
end;

{ TKeyPairList }

procedure TKeyPairList.AddPair(left, right: Variant);
var
  v: Variant;
begin
  IncLength;
  v := VarArrayCreate( [0,1], varVariant );
  v[0] := left;
  v[1] := right;
  FList[VarArrayHighBound(FList, 1)] := v;
end;

function TKeyPairList.Count: integer;
begin
  if VarIsNull( FList ) then
    Result := 0
  else
    Result := VarArrayHighBound(FList,1) - VarArrayLowBound(FList,1) + 1;
end;

function TKeyPairList.GetLeftKey(i: integer): Variant;
begin
  Result := FList[i][0];
end;

function TKeyPairList.GetRightKey(i: integer): Variant;
begin
  Result := FList[i][1];
end;

procedure TKeyPairList.IncLength;
begin
  if VarIsEmpty(FList) then
    FList := VarArrayCreate( [0,0], varVariant )
  else
    VarArrayRedim( FList, VarArrayHighBound(FList,1) + 1 );
end;



end.
