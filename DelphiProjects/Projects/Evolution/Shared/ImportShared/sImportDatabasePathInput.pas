// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportDatabasePathInput;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, sImportVisualController, ISBasicClasses, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TImportDatabasePathFrame = class(TFrame, IConnectionStringInputFrame )
    BitBtn1: TevBitBtn;
    lblSource: TevLabel;
    OpenDialog1: TOpenDialog;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FController: IImportVisualController;
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
  public
    { Public declarations }
  end;

implementation

uses
  evutils;
{$R *.DFM}

procedure TImportDatabasePathFrame.BitBtn1Click(Sender: TObject);
var
  dbpath: string;
  defpath: string;
begin
  if assigned( FController ) then
    defpath := FController.ConnectionString
  else
    defpath := '';
  dbpath := RunFolderDialog( 'Select database folder', defpath );
  if dbpath <> '' then
    FController.SetConnectionString( dbpath );
end;

procedure TImportDatabasePathFrame.ConnectionStringChanged;
begin
  if assigned(FController) and (trim(FController.ConnectionString) <> '') then
    lblSource.Caption := 'Importing from ' + FController.ConnectionString
  else
    lblSource.Caption := 'No database path specified';
end;

procedure TImportDatabasePathFrame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TImportDatabasePathFrame.SetDefaultConnectionString(aDefConnStr: string);
begin
//no disconnected state
end;

end.
