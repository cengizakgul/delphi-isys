// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyLogger;

interface

type
  TLogMessageKind = ( lmException, lmError, lmWarning, lmEvent, lmDebug );

  ILogger = interface
['{20812855-ED21-41E7-A523-8C24D984412E}']
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call
    procedure LogContextItem( tag: string; value: string );
    procedure LogMessage( aKind: TLogMessageKind; aText: string; aDetails: string );
  end;

  //all introduced methods must be called from except block only
  IExceptionLogger = interface (ILogger)
['{23EAD67D-CE16-4745-8173-568E65D06D6A}']
    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndLogMessage( kind: TLogMessageKind; s: string; details: string );
    procedure StopExceptionAndLogMessage( kind: TLogMessageKind; s: string; details: string );
  end;

function CreateExceptionLogger( aLogger: ILogger ): IExceptionLogger;

(*
usage pattern:
    FLogger.LogEntry( sText );
    try
      try
        FLogger.LogContextItem( ... );
        //do something useful here
        DoSomethingUsefulThere;
      except
        FLogger.PassthroughException;
        // or FLogger.PassthroughExceptionWarnFmt( 'important operation aborted', []);
        // or FLogger.StopException;
      end;
    finally
      FLogger.LogExit;
    end;

    DoSomethingUsefulThere;
    begin
      if somecondition then
        FLogger.LogError('fafa','lyalya');
      if anothercondition then
        raise EevException.Create('fafa-lyalya');
    end;
*)


implementation

uses
  sysutils;

type
  TExceptionLoggerImpl = class ( TInterfacedObject, IExceptionLogger )
  private
    FLogger: ILogger;
    FExceptAddr: Pointer;
    procedure DoLogException( msg: string );
    procedure PassthroughException_log;
    procedure PassthroughException_raise;

    procedure AssertContext;
  private
    {ILogger part}
    procedure LogEntry( blockname: string );
    procedure LogExit;
    procedure LogContextItem( tag: string; value: string );
    procedure LogMessage( aKind: TLogMessageKind; aText: string; aDetails: string );
  protected
    {IExceptionLogger}
    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndLogMessage( kind: TLogMessageKind; s: string; details: string );
    procedure StopExceptionAndLogMessage( kind: TLogMessageKind; s: string; details: string );
  public
    constructor Create( aLogger: ILogger );
    destructor Destroy; override;
  end;

function CreateExceptionLogger( aLogger: ILogger ): IExceptionLogger;
begin
  Result := TExceptionLoggerImpl.Create( aLogger );
end;
{ TExceptionLoggerImpl }

constructor TExceptionLoggerImpl.Create(aLogger: ILogger);
begin
  FExceptAddr := nil;
  FLogger := aLogger;
  FLogger.LogMessage( lmDebug, 'Exception logger created', '' );
end;

destructor TExceptionLoggerImpl.Destroy;
begin
  FLogger.LogMessage( lmDebug, 'Destroying exception logger', '' );
  inherited;
end;

procedure TExceptionLoggerImpl.DoLogException(msg: string);
  function RemoveCRLF( s: string ): string;
  begin
    Result := s;
    Result := StringReplace( Result, #13, ' ', [rfReplaceAll]);
    Result := StringReplace( Result, #10, ' ', [rfReplaceAll]);
  end;
var
  p: integer;
begin
  msg := AdjustLineBreaks( msg );
  p := Pos( #13#10#13#10, msg );
  if p = 0 then //one paragraph
    FLogger.LogMessage( lmException, RemoveCRLF(msg), '' )
  else
    FLogger.LogMessage( lmException, RemoveCRLF( copy( msg, 1, p-1) ), 'Exception text:' + #13#10 + msg );
end;

procedure TExceptionLoggerImpl.PassthroughException_log;
begin
  AssertContext;
  if ExceptAddr <> FExceptAddr then
  begin
    DoLogException( (ExceptObject as Exception).Message );
    FExceptAddr := ExceptAddr;
  end;
end;

procedure TExceptionLoggerImpl.PassthroughException_raise;
var
  LException: Exception;
begin
  LException := AcquireExceptionObject;
  raise LException at ExceptAddr;
end;

procedure TExceptionLoggerImpl.AssertContext;
begin
  Assert( (ExceptObject <> nil) and (ExceptObject is Exception) );
  Assert( ExceptAddr <> nil );
end;

procedure TExceptionLoggerImpl.PassthroughException;
begin
  PassthroughException_log;
  PassthroughException_raise;
end;

procedure TExceptionLoggerImpl.StopException;
begin
  AssertContext;
  if ExceptAddr <> FExceptAddr then
    DoLogException( (ExceptObject as Exception).Message );
  FExceptAddr := nil;
end;

procedure TExceptionLoggerImpl.PassthroughExceptionAndLogMessage( kind: TLogMessageKind; s, details: string);
begin
  PassthroughException_log;
  LogMessage( kind, s, details );
  PassthroughException_raise;
end;

procedure TExceptionLoggerImpl.StopExceptionAndLogMessage( kind: TLogMessageKind; s, details: string);
begin
  StopException;
  LogMessage( kind, s, details );
end;

///

procedure TExceptionLoggerImpl.LogEntry(blockname: string);
begin
  FExceptAddr := nil;
  FLogger.LogEntry( blockname );
end;

procedure TExceptionLoggerImpl.LogContextItem(tag, value: string);
begin
  FExceptAddr := nil;
  FLogger.LogContextItem( tag, value );
end;

procedure TExceptionLoggerImpl.LogExit;
begin
  FLogger.LogExit;
end;

procedure TExceptionLoggerImpl.LogMessage(aKind: TLogMessageKind; aText, aDetails: string);
begin
  FLogger.LogMessage( aKind, aText, aDetails );
end;

end.
