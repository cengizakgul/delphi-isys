// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyResourceManagerImpl;
{$INCLUDE gdy.inc}

interface
uses
  gdyResourceManager, classes, gdyResourceGraph, gdyResourceGraphImpl;

type
  TStatelessResourceAdapter = class(TInterfacedObject, IStatelessResourceAdapter)
  private
    FResourceAdapter: IResourceAdapter;
  protected
    {IStatelessResourceAdapter}
    function CanBeConstructed( aName: string ): boolean;
    procedure EnsureConstructed( aName: string );
    procedure EnsureDestructed( aName: string );
  public
    constructor Create( aResourceAdapter: IResourceAdapter );
  end;

  TResourceManager = class(TInterfacedObject, IResourceManager)
  private
    FNavigator: TGraphNavigator;
    FResourceAdapter: IStatelessResourceAdapter;
    FTempCanBeConstructed: boolean;
//    FTempCanBeDestructed: boolean;
    function hlpDestruct( aName: string ): boolean;
    function hlpConstruct( aName: string ): boolean;
    function hlpCanBeConstructed( aName: string ): boolean;
//    function hlpCanBeDestructed( aName: string ): boolean;
  protected
    function CanResourceBeConstructed(aName: string): boolean;
//    function CanResourceBeDestructed(aName: string): boolean;
    {IResourceManager}
    procedure DestructResource( aName: string );
    procedure DestructAllResources;
//    function AllResourcesCanBeDestructed: boolean;
    procedure TryConstructResource( aName: string );
  public
    constructor Create( rdg: IResourceDependencyGraph; aResourceAdapter: IStatelessResourceAdapter );
    destructor Destroy; override;
  end;

function ResourceManager( {const aResources: array of string; }aDepDefs: array of TDependencyDef; aResourceAdapter: IResourceAdapter ): IResourceManager;

implementation

uses
  sysutils;

function ResourceManager( {const aResources: array of string; }aDepDefs: array of TDependencyDef; aResourceAdapter: IResourceAdapter ): IResourceManager;
begin
  Result := TResourceManager.Create(
              TResourceDependencyGraph.Create( {aResources, }aDepDefs ) as IResourceDependencyGraph,
              TStatelessResourceAdapter.Create( aResourceAdapter )
            ) as IResourceManager;
end;

{ TResourceManager }

constructor TResourceManager.Create(rdg: IResourceDependencyGraph; aResourceAdapter: IStatelessResourceAdapter);
begin
  FNavigator := TGraphNavigator.Create( rdg );
  FResourceAdapter := aResourceAdapter;
end;

destructor TResourceManager.Destroy;
begin
  FreeAndNil( FNavigator );
  inherited;
end;

//consruction stuff
function TResourceManager.hlpConstruct(aName: string): boolean;
begin
  if CanResourceBeConstructed( aName ) then
    FResourceAdapter.EnsureConstructed( aName );
  Result := true;
end;

procedure TResourceManager.TryConstructResource(aName: string);
begin
  FNavigator.ForParentsR( aName, hlpConstruct );
end;

//can be constructed stuff
function TResourceManager.hlpCanBeConstructed(aName: string): boolean;
begin
  FTempCanBeConstructed := FResourceAdapter.CanBeConstructed( aName );
  Result := FTempCanBeConstructed;
end;

function TResourceManager.CanResourceBeConstructed(aName: string): boolean;
begin
  FTempCanBeConstructed := true;
  FNavigator.ForParents( aName, hlpCanBeConstructed );
  Result := FTempCanBeConstructed;
end;

//destruction stuff
function TResourceManager.hlpDestruct(aName: string): boolean;
begin
  FResourceAdapter.EnsureDestructed( aName );
  Result := true;
end;

procedure TResourceManager.DestructResource(aName: string);
begin
//  if CanResourceBeDestructed( aName ) then
    FNavigator.ForChildsR( aName, hlpDestruct )
//  else
//    AbortEx;
end;

procedure TResourceManager.DestructAllResources;
begin
//  if AllResourcesCanBeDestructed then
    FNavigator.ForRootsChildsR( hlpDestruct );
//  else
//    AbortEx;
end;

{
//can be destructed stuff
function TResourceManager.hlpCanBeDestructed(aName: string): boolean;
begin
  FTempCanBeDestructed := FResourceAdapter.CanBeDestructed( aName );
  Result := FTempCanBeDestructed;
end;

function TResourceManager.CanResourceBeDestructed(aName: string): boolean;
begin
  FTempCanBeDestructed := true;
  FNavigator.ForChildsR( aName, hlpCanBeDestructed ); //order is not significant
  Result := FTempCanBeDestructed;
end;

function TResourceManager.AllResourcesCanBeDestructed: boolean;
begin
  FTempCanBeDestructed := true;
  FNavigator.ForRootsChildsR( hlpCanBeDestructed ); //order is not significant
  Result := FTempCanBeDestructed
end;
}


{ TStatelessResourceAdapter }
function TStatelessResourceAdapter.CanBeConstructed(
  aName: string): boolean;
begin
  Result := FResourceAdapter.IsConstructed( aName ) or FResourceAdapter.CanBeConstructed( aName );
end;

procedure TStatelessResourceAdapter.EnsureConstructed(aName: string);
begin
  if not FResourceAdapter.IsConstructed( aName ) then
    FResourceAdapter.Construct( aName );
end;

procedure TStatelessResourceAdapter.EnsureDestructed(aName: string);
begin
  if FResourceAdapter.IsConstructed( aName ) then
    FResourceAdapter.Destruct( aName );
end;

constructor TStatelessResourceAdapter.Create(
  aResourceAdapter: IResourceAdapter);
begin
  FResourceAdapter := aResourceAdapter;
end;

end.
