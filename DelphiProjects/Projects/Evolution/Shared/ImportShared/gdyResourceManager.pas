// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyResourceManager;
{$INCLUDE gdy.inc}

interface

uses
  sysutils;

type
  ERMFailure = class(Exception);
  ERMNotFound = class(ERMFailure);
  ERMInvalidIndex = class(ERMFailure);
  ERMInvalidGraph = class(ERMFailure);
  ERMCycleFound = class(ERMInvalidGraph);
  ERMInvalidResourceName = class(ERMInvalidGraph);

  TDependencyDef = record
    Name: string;
    ParentName: string;
  end;

type
  IStatelessResourceAdapter = interface
  ['{B6BDAA10-2A67-4A03-9C3E-1B511DB77413}']
//    function CanBeDestructed( aName: string ): boolean;
    function CanBeConstructed( aName: string ): boolean;
    procedure EnsureConstructed( aName: string );
    procedure EnsureDestructed( aName: string );
  end;

  // ResourceState = (rsConstructed, rsDestructed)
  IResourceAdapter = interface
  ['{64EFA820-D2B9-4B2F-8183-CDE70EF779F8}']
    //PRE: State = rsDestructed
    function CanBeConstructed( aName: string ): boolean;
    //PRE: State = rsDestructed
    procedure Construct( aName: string );
    //PRE: State = rsConstructed
    procedure Destruct( aName: string );
    //POST: Result = State
    function IsConstructed( aName: string ): boolean;
  end;

  IResourceManager = interface
['{EF3FDFD7-9FEC-43AC-BFA0-2636F46BE7A0}']
    procedure DestructResource( aName: string );
    procedure TryConstructResource( aName: string );
    procedure DestructAllResources;
//    function  AllResourcesCanBeDestructed: boolean;
  end;

  function ResourceManager( {const aResources: array of string; }aDepDefs: array of TDependencyDef; aResourceAdapter: IResourceAdapter ): IResourceManager;

implementation
uses
  gdyResourceManagerImpl;

function ResourceManager( {const aResources: array of string; }aDepDefs: array of TDependencyDef; aResourceAdapter: IResourceAdapter ): IResourceManager;
begin
  Result := gdyResourceManagerImpl.ResourceManager( {aResources,} aDepDefs, aResourceAdapter );
end;

end.
