// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportDatabaseFileNameInput;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, sImportVisualController, ISBasicClasses, EvUIComponents;

type
  TImportDatabaseFileNameFrame = class(TFrame, IConnectionStringInputFrame )
    BitBtn1: TevBitBtn;
    lblSource: TevLabel;
    OpenDialog1: TOpenDialog;
    procedure BitBtn1Click(Sender: TObject);
  private
    FController: IImportVisualController;
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TImportDatabaseFileNameFrame.BitBtn1Click(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then
      FController.SetConnectionString( FileName );
end;

procedure TImportDatabaseFileNameFrame.ConnectionStringChanged;
begin
  if assigned(FController) and (trim(FController.ConnectionString) <> '') then
    lblSource.Caption := 'Importing from ' + FController.ConnectionString
  else
    lblSource.Caption := 'No database filename specified';
end;

procedure TImportDatabaseFileNameFrame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TImportDatabaseFileNameFrame.SetDefaultConnectionString( aDefConnStr: string);
begin
//no disconnected state
end;

end.
