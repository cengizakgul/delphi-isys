// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportQueue;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,  ExtCtrls, sImportVisualController, ActnList,
  ImgList, StdCtrls, Buttons, Menus, ISBasicClasses, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TImportQueueFrame = class(TFrame, IImportQueueFrame)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    lvQueue: TevListView;
    evImageList1: TevImageList;
    evActionList1: TevActionList;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    MoveUp: TAction;
    MoveDown: TAction;
    evBitBtn3: TevBitBtn;
    evBitBtn4: TevBitBtn;
    Remove: TAction;
    ShowLog: TAction;
    evPopupMenu1: TevPopupMenu;
    ViewLog1: TMenuItem;
    evBitBtn5: TevBitBtn;
    procedure RunQueuedImportsExecute(Sender: TObject);
    procedure RunQueuedImportsUpdate(Sender: TObject);
    procedure MoveUpUpdate(Sender: TObject);
    procedure MoveDownUpdate(Sender: TObject);
    procedure MoveDownExecute(Sender: TObject);
    procedure MoveUpExecute(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure RemoveExecute(Sender: TObject);
    procedure lvQueueMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ShowLogUpdate(Sender: TObject);
    procedure ShowLogExecute(Sender: TObject);
  private
    FController: IImportVisualController;
    {IImportQueueFrame}
    procedure QueueChanged;
    procedure QueueItemStatusChanged;
    procedure SetController( aController: IImportVisualController );
  public
  end;

implementation

uses
  gdyImportUIHelpers, ShellAPI;
{$R *.DFM}

{ TImportQueueFrame }

procedure TImportQueueFrame.QueueChanged;
var
  i: integer;
  ii: integer;
begin
  if assigned(lvQueue.Selected) then
    ii := lvQueue.Selected.Index
  else
    ii := -1;
  lvQueue.Items.BeginUpdate;
  try
    lvQueue.Items.Clear;
    if assigned( FController ) then
      for i := 0 to FController.ImportQueue.count -1 do
        with lvQueue.Items.Add do
        begin
          Data := pointer( i );
          Caption := FController.ImportQueue[i].CompanyName;
          Subitems.Add( FController.ImportQueue[i].ConnectionString );
          Subitems.Add( FController.ImportQueue[i].LogFolderOrFile );
        end;
    if ii > lvQueue.Items.Count-1 then
      ii := lvQueue.Items.Count-1;
    if (ii < 0) and (lvQueue.Items.Count > 0) then
      ii := 0;
    if ii = -1 then
      lvQueue.Selected := nil
    else
      lvQueue.Selected := lvQueue.Items[ii];
    QueueItemStatusChanged;
  finally
    lvQueue.Items.endUpdate;
  end;
end;

procedure TImportQueueFrame.QueueItemStatusChanged;
var
  i: integer;
begin
  if assigned( FController ) then
  begin
    for i := 0 to FController.ImportQueue.count -1 do
      with lvQueue.Items[i] do
      begin
        case FController.ImportQueue[i].Status of
          qisWaiting: ImageIndex := 0;
          qisCompleted: ImageIndex := 1;
          qisFailed: ImageIndex := 2;
          qisRunning: ImageIndex := 3;
        else
          ImageIndex := -1;
        end;
        Subitems[1] := FController.ImportQueue[i].LogFolderOrFile;
      end;
    lvQueue.Update;
  end;
end;

procedure TImportQueueFrame.SetController(aController: IImportVisualController);
begin
  FController := AController;
  QueueChanged;
end;

procedure TImportQueueFrame.RunQueuedImportsExecute(Sender: TObject);
begin
  FController.RunQueuedImports;
end;

procedure TImportQueueFrame.RunQueuedImportsUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController ) and FController.CanRunQueuedImports;
end;


procedure TImportQueueFrame.MoveUpUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController ) and assigned(lvQueue.Selected) and ( lvQueue.Selected.Index > 0 );
end;

procedure TImportQueueFrame.MoveDownUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController ) and assigned(lvQueue.Selected) and ( lvQueue.Selected.Index < FController.ImportQueue.Count-1) ;
end;

procedure TImportQueueFrame.MoveDownExecute(Sender: TObject);
begin                                                                                                                              
  lvQueue.Selected := lvQueue.Items[lvQueue.Selected.Index+1];
  FController.MoveQueuedImportDown( lvQueue.Selected.Index );
end;

procedure TImportQueueFrame.MoveUpExecute(Sender: TObject);
begin
  lvQueue.Selected := lvQueue.Items[lvQueue.Selected.Index-1];
  FController.MoveQueuedImportUp( lvQueue.Selected.Index );
end;

procedure TImportQueueFrame.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController ) and assigned(lvQueue.Selected) ;
end;

procedure TImportQueueFrame.RemoveExecute(Sender: TObject);
begin
  FController.RemoveQueuedImport( lvQueue.Selected.Index );
end;

procedure TImportQueueFrame.lvQueueMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (ssLeft in Shift) and (ssDouble in shift) and assigned(lvQueue.GetItemAt(x,y)) then
    ShowLog.Execute;
end;

procedure TImportQueueFrame.ShowLogUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController ) and assigned(lvQueue.Selected)
                                       and (FController.ImportQueue[lvQueue.Selected.Index].LogFile <> '');
end;

procedure TImportQueueFrame.ShowLogExecute(Sender: TObject);
begin
  ShellExecute(GetActiveWindow, 'open', PChar(FController.ImportQueue[lvQueue.Selected.Index].LogFile), nil, nil, SW_NORMAL);
end;

end.


