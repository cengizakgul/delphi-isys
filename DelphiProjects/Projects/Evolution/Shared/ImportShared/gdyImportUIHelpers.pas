// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyImportUIHelpers;

interface
uses
  classes, gdyBinder;

type
  TImportSetup = class
  private
    FFileName: string;
    FSectionName: string;
    FParams: TStringList;
    FOrigParams: TStringList;
    procedure SetParams(const Value: TStringList); //just not to create it every time it needed
  public
    constructor Create; overload; //NEVER use!!!
    constructor Create( aSectionName: string ); overload;
    constructor Create( aSectionName: string; aFileName: string ); overload;
    destructor Destroy; override;

    procedure SaveToFile( aFileName: string );
    procedure Assign( const Source: TImportSetup );
    function IsChanged: boolean;
    property FileName: string read FFileName write FFileName;
    property Params: TStringList read FParams write SetParams;
    property OrigParams: TStringList read FOrigParams;
  end;

  TQueuedImportStatus = ( qisWaiting, qisRunning, qisCompleted, qisFailed );

  TQueuedImportData = class(TCollectionItem)
  private
    FConnectionString: string;
    FLogFolder: string;
    FLogFile: string;
    FParams: TStringList;
    FMatchers: IMatchersList;
    FCompanyName: string;
    FStatus: TQueuedImportStatus;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    function Description: string;
    function LogFolderOrFile: string;
  //    procedure Assign(Source: TPersistent); override;
    procedure Init( aCompanyName: string; aConnectionString: string; aParams: TStringList; aMatchers{reference stored}: IMatchersList; aLogFolder: string );
    property CompanyName: string read FCompanyName;
    property ConnectionString: string read FConnectionString;
    property LogFolder: string read FLogFolder;
    property LogFile: string read FLogFile write FLogFile;
    property Params: TStringList read FParams;
    property Matchers: IMatchersList read FMatchers;
    property Status: TQueuedImportStatus read FStatus write FStatus;
  end;

  TImportQueue = class ( TCollection )
  private
    function GetItem(Index: Integer): TQueuedImportData;
    procedure SetItem(Index: Integer; const Value: TQueuedImportData);
  public
    constructor Create;
    function Add: TQueuedImportData;
    property Items[Index: Integer]: TQueuedImportData read GetItem write SetItem; default;
  end;

implementation

uses
  sysutils, inifiles;

function StringListsAreEqual( sl1, sl2: TStrings ): boolean;
var
  i: integer;
begin
  Result := sl1.Count = sl2.Count;
  if Result then
    for i := 0 to sl1.Count-1 do
      if sl2.IndexOf( sl1[i] ) = -1 then
      begin
        Result := false;
        break;
      end;
end;

{ TImportSetup }

constructor TImportSetup.Create;
begin
  Assert( false, 'Internal Error; wrong constructor is used' );
end;

procedure TImportSetup.Assign(const Source: TImportSetup);
begin
  Params := Source.Params;
  FileName := Source.FileName;
end;

constructor TImportSetup.Create( aSectionName: string  );
begin
  FParams := TStringList.Create;
  FOrigParams := TStringList.Create;
  FSectionName := aSectionName;
end;

constructor TImportSetup.Create( aSectionName: string; aFileName: string );
begin
  Create( aSectionName );
  FFileName := aFileName;
  if aFileName <> '' then
    with TIniFile.Create( aFileName ) do
    try
      ReadSectionValues( aSectionName,  FParams );
    finally
      Free;
    end;
  FOrigParams.Assign( FParams );
end;

destructor TImportSetup.Destroy;
begin
  inherited;
  FreeAndNil( FOrigParams );
  FreeAndNil( FParams );
end;

function TImportSetup.IsChanged: boolean;
begin
  Result := not StringListsAreEqual( FParams, FOrigParams );
end;

procedure TImportSetup.SaveToFile(aFileName: string);
var
  i: integer;
begin
  FileName := aFileName;
  with TIniFile.Create( FileName ) do
  try
    EraseSection( FSectionName );
    for i := 0 to Params.Count-1 do
      WriteString( FSectionName, Params.Names[i], Params.Values[Params.Names[i]]);
    FOrigParams.Assign( FParams );
  finally
    Free;
  end;
end;

procedure TImportSetup.SetParams(const Value: TStringList);
begin
  FParams.Assign( Value );
end;

{ TQueuedImportData }

constructor TQueuedImportData.Create(Collection: TCollection);
begin
  FParams := TstringList.Create;
  FStatus := qisWaiting;
  inherited;
end;


function TQueuedImportData.Description: string;
begin
  Result := CompanyName + ' from ' + ConnectionString;
end;


destructor TQueuedImportData.Destroy;
begin
  inherited;
  FreeAndNil( FParams );
end;

procedure TQueuedImportData.Init(aCompanyName: string; aConnectionString: string; aParams: TStringList; aMatchers: IMatchersList; aLogFolder: string);
begin
  FCompanyName := aCompanyName;
  FConnectionString := aConnectionString;
  FParams.Assign( aParams );
  FMatchers := aMatchers;
  FLogFolder := aLogFolder;
end;

{
procedure TQueuedImportData.Assign(Source: TPersistent);
begin
  if Source is TQueuedImportData then
  begin
    FConnectionString := TQueuedImportData(Source).ConnectionString;
    FLogFolder := TQueuedImportData(Source).LogFolder;
    FParams.Assign( TQueuedImportData(Source).Params );
    FMatchers := TQueuedImportData(Source).Matchers;
    FImportInstanceName := TQueuedImportData(Source).FImportInstanceName
  end
  else
    inherited;
end;
}
{ TImportQueued }

function TImportQueue.Add: TQueuedImportData;
begin
  Result := inherited Add as TQueuedImportData;
end;

constructor TImportQueue.Create;
begin
  inherited Create(TQueuedImportData);
end;


function TImportQueue.GetItem(Index: Integer): TQueuedImportData;
begin
  Result := inherited Items[ Index ] as TQueuedImportData;
end;

procedure TImportQueue.SetItem(Index: Integer; const Value: TQueuedImportData);
begin
  Items[Index] := Value;
end;


function TQueuedImportData.LogFolderOrFile: string;
begin
  if LogFile <> '' then
    Result := LogFile
  else
    Result := LogFolder;
end;

end.
