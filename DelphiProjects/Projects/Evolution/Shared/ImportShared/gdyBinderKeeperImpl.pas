// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyBinderKeeperImpl;

interface
uses
  gdyBinder, gdyResourceManager, db, evExceptions;

type
  TBindingKeeper = class (TInterfacedObject, IBindingKeeper, IResourceAdapter)
  private
    FRM: IResourceManager;
    FBindingDesc: TBindingDesc;
    FLeftTable, FRightTable: TDataSet;
    FCDSClass: TCDSBaseClass;
    FMatchTableFilename: string;
    FConnected: boolean;

    FMatchTableKeeper: IMatchTableKeeper;
    FBinder: IBinder;
    FVisualBinder: IVisualBinder;
    FAutoMapper: IAutoMapper;
    {IBindingKeeper}
    function BindingName: string;
    procedure SetTables( aLeftTable, aRightTable: TDataSet );
    procedure SetVisualBinder( aVisualBinder: IVisualBinder );
    procedure SetConnected( Value: boolean );
    procedure SetMatchTable( aFilename: string );
    procedure SetAutoMapper( aAutoMapper: IAutoMapper );

    procedure SaveMatchTable( aFilename: string );
    function IsMatchTableChanged: boolean;
    procedure CancelMatchTable;

    function CanCreateMatcher: boolean;
    function CreateMatcher: IMatcher;
  protected
    {IResourceAdapter}
    function CanBeConstructed( aName: string ): boolean;
    function IsConstructed( aName: string ): boolean;
    procedure Construct( aName: string );
    procedure Destruct( aName: string );
  public
    constructor Create( bd: TBindingDesc; cdsClass: TCDSBaseClass );
    destructor Destroy; override;

  end;

implementation

uses
  gdystrset, gdyBinderImpl, sysutils, dialogs, controls, {$ifndef GDYCOMMONN} evutils {$else} gdymsg {$endif};

{ TBindingKeeper }

const
  sRes_FieldDefs = 'field defs';
  sRes_Connection = 'opened tables';
  sRes_MatchTableKeeper = 'match table keeper';
  sRes_Binder = 'binder';
  sRes_VisualBinder = 'visual binder';
  sRes_VisualBinderLink = 'visual binder link';
  sRes_AutoMapper = 'auto mapper';
  sRes_All = sRes_VisualBinderLink;

  dep: array [0..6] of TDependencyDef = (
    (Name: sRes_MatchTableKeeper; ParentName: sRes_FieldDefs),
    (Name: sRes_Binder; ParentName: sRes_FieldDefs),
    (Name: sRes_Binder; ParentName: sRes_Connection),
    (Name: sRes_Binder; ParentName: sRes_MatchTableKeeper),
    (Name: sRes_VisualBinderLink; ParentName: sRes_VisualBinder),
    (Name: sRes_VisualBinderLink; ParentName: sRes_Binder),
    (Name: sRes_VisualBinderLink; ParentName: sRes_AutoMapper)

  );

  //for assertions only
  roots: array[0..4] of string = (sRes_Connection, sRes_FieldDefs, sRes_MatchTableKeeper, sRes_VisualBinder, sRes_AutoMapper);

constructor TBindingKeeper.Create(bd: TBindingDesc; cdsClass: TCDSBaseClass);
begin
  FBindingDesc := bd;
  FCDSClass := cdsClass;
  FRM := ResourceManager( dep, Self );
  SetAutoMapper( NullAutoMapper );
end;

destructor TBindingKeeper.Destroy;
begin
  inherited;
  FRM.DestructAllResources;
  FRM := nil;
end;

procedure TBindingKeeper.SetTables(aLeftTable, aRightTable: TDataSet);
begin
  if (FLeftTable <> aLeftTable) or (FRightTable <> aRightTable) then
  begin
    FRM.DestructResource( sRes_FieldDefs );
    FLeftTable := aLeftTable;
    FRightTable := aRightTable;
    FRM.TryConstructResource( sRes_All );
  end;
end;

procedure TBindingKeeper.SetConnected(Value: boolean);
begin
  if FConnected <> Value then
  begin
    FRM.DestructResource( sRes_Connection );
    FConnected := Value;
    FRM.TryConstructResource( sRes_All );
  end;
end;

procedure TBindingKeeper.SetMatchTable(aFilename: string);
begin
  //open or reopen
  FRM.DestructResource( sRes_MatchTableKeeper );
  FMatchTableFilename := aFilename;
  FRM.TryConstructResource( sRes_All );
end;

procedure TBindingKeeper.SetVisualBinder(aVisualBinder: IVisualBinder);
begin
  FRM.DestructResource( sRes_VisualBinder );
  FVisualBinder := aVisualBinder;
  FRM.TryConstructResource( sRes_All );
end;

procedure TBindingKeeper.SaveMatchTable(aFileName: string);
begin
  if IsConstructed( sRes_MatchTableKeeper ) then
  begin
    FMatchTableKeeper.Save( aFileName );
    FMatchTableFilename := aFileName;
  end;
end;

function TBindingKeeper.CanBeConstructed(aName: string): boolean;
begin
  if aName = sRes_Fielddefs then
    Result := assigned(FLeftTable) and assigned(FRightTable)
  else if aName = sRes_Connection then
    Result := FConnected
  else if aName = sRes_VisualBinder then
    Result := assigned(FVisualBinder)
  else if aName = sRes_AutoMapper then
    Result := assigned( FAutoMapper )
  else
    Result := true;
end;

function TBindingKeeper.IsConstructed(aName: string): boolean;
begin
  if aName = sRes_MatchTableKeeper then
    Result := assigned(FMatchTableKeeper)
  else if aName = sRes_Binder then
    Result := assigned(FBinder)
  else if aName = sRes_VisualBinderLink then
    Result := assigned(FVisualBinder) and assigned(FBinder) and (FVisualBinder.GetBinder = FBinder)
  else
  begin
    Assert( InSet(aName, roots ), 'Assert(InSet(aName, roots )) failed for '+ aName );
    Result := CanBeConstructed( aName );
  end
end;

procedure TBindingKeeper.Construct(aName: string);
begin
  Assert( CanBeConstructed( aName ) );
  Assert( InSet(aName, roots ) or not IsConstructed( aName ) );
  if aName = sRes_MatchTableKeeper then
    FMatchTableKeeper := TMatchTableKeeper.Create( FBindingDesc, FLeftTable, FRightTable, FMatchTableFilename, FCDSClass )
  else if aName = sRes_Binder then
    FBinder := TBinder.Create( FBindingDesc, FLeftTable, FRightTable, FMatchTableKeeper, FCDSClass )
  else if aName = sRes_VisualBinderLink then
    FVisualBinder.ActivateBinder( FBinder, FAutoMapper )
  else
    Assert( InSet(aName, roots ), 'Assert(InSet(aName, roots )) failed for '+ aName );
end;

procedure TBindingKeeper.Destruct(aName: string);
begin
  Assert( IsConstructed( aName ) );
  if aName = sRes_MatchTableKeeper then
    FMatchTableKeeper := nil
  else if aName = sRes_Binder then
    FBinder := nil
  else if aName = sRes_VisualBinderLink then
    FVisualBinder.ActivateBinder( nil, FAutoMapper )
  else
    Assert( InSet(aName, roots ), 'Assert(InSet(aName, roots )) failed for '+ aName );
end;

function TBindingKeeper.IsMatchTableChanged: boolean;
begin
  Result := IsConstructed( sRes_MatchTableKeeper ) and FMatchTableKeeper.IsChanged;
end;

procedure TBindingKeeper.CancelMatchTable;
begin
  FRM.DestructResource( sRes_MatchTableKeeper );
  FRM.TryConstructResource( sRes_MatchTableKeeper );
end;

function TBindingKeeper.BindingName: string;
begin
  Result := FBindingDesc.Name;
end;

function TBindingKeeper.CanCreateMatcher: boolean;
begin
  Result := IsConstructed( sRes_MatchTableKeeper );
end;

function TBindingKeeper.CreateMatcher: IMatcher;
begin
  if CanCreateMatcher then
    Result := FMatchTableKeeper.CreateMatcher
  else
    raise EevException.Create( 'Internal error: cannot create IMatcher')
end;

procedure TBindingKeeper.SetAutoMapper(aAutoMapper: IAutoMapper);
begin
  FRM.DestructResource( sRes_AutoMapper );
  FAutoMapper := aAutoMapper;
  FRM.TryConstructResource( sRes_All );
end;

end.
