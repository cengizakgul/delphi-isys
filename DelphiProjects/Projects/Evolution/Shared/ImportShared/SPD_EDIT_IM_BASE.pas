// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_BASE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db,  StdCtrls, Buttons,
  ExtCtrls, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, DBGrids,
  SDataStructure, genericImport, ComCtrls, ImgList, ActnList, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, gdyImportUIHelpers, sImportVisualController,
  sImportDatabasePathInput, sTablespaceView, sImportQueue,
  sImportLoggerView, ISBasicClasses, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_BASE = class(TFrameEntry, ISetupNameListener, ILogFolderListener )
    evActionList1: TevActionList;
    evImageList1: TevImageList;
    OpenConfig: TAction;
    SaveConfig: TAction;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    evPanel1: TevPanel;
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    evPanel4: TevPanel;
    evPanel5: TevPanel;
    SaveSetupAs: TAction;
    pnlConnectionInputHolder: TevPanel;
    RunImport: TAction;
    evPanel3: TevPanel;
    bbrun: TevBitBtn;
    TabSheet5: TTabSheet;
    viewerFrame: TTableSpaceViewFrame;
    SelectLogFolder: TAction;
    evPanel2: TevPanel;
    lblLogFolder: TevLabel;
    evBitBtn1: TevBitBtn;
    tbshQueue: TTabSheet;
    ImportQueueFrame: TImportQueueFrame;
    PutToQueue: TAction;
    evBitBtn4: TevBitBtn;
    NewSetup: TAction;
    pnlSetup: TevPanel;
    EvBevel1: TEvBevel;
    lblSetup: TevLabel;
    EvBevel2: TEvBevel;
    evBitBtn8: TevBitBtn;
    evBitBtn3: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evBitBtn5: TevBitBtn;
    pnlSetupControls: TevPanel;
    LogFrame: TImportLoggerViewFrame;
    cbDevLog: TevCheckBox;
    procedure SaveAsConfigExecute(Sender: TObject);
    procedure OpenConfigExecute(Sender: TObject);
    procedure EnableIfConfigured(Sender: TObject);
    procedure SaveConfigExecute(Sender: TObject);
    procedure RunImportExecute(Sender: TObject);
    procedure SelectLogFolderExecute(Sender: TObject);
    procedure EnableIfHasController(Sender: TObject);
    procedure PutToQueueExecute(Sender: TObject);
    procedure NewSetupExecute(Sender: TObject);
    procedure cbDevLogClick(Sender: TObject);
  private
    { Private declarations }
    FUserSaidDontSave: boolean;
    function SetupIsSaved: boolean;
    {ISetupNameListener}
    procedure SetupNameChanged;
    {ILogFolderListener}
    procedure LogFolderChanged;
    procedure DevLogEnabledChanged;
  protected
    FController: IImportVisualController;
    procedure SetController( aController: IImportVisualController ); virtual;
  public
    { Public declarations }
    function CanClose: Boolean; override;
    procedure Activate; override;
    procedure DeActivate; override;
  end;

implementation
uses
  evUtils, inifiles, sPAckageEntry;
{$R *.DFM}

{TEDIT_IM_BASE}
procedure TEDIT_IM_BASE.Activate;
begin
  inherited;
  FUserSaidDontSave := false;
end;

procedure TEDIT_IM_BASE.DeActivate;
begin
  if assigned(FController) then
  begin
    FController.Disconnect;
    if FController.DataWasImported then
      (Owner as TFramePackageTmpl).InternalRefresh;
    SetController( nil );
  end;
  inherited;
end;

procedure TEDIT_IM_BASE.SaveConfigExecute(Sender: TObject);
begin
  if FController.SetupName = '' then
    SaveSetupAs.Execute
  else
    FController.WriteSetup( FController.SetupName );
end;

procedure TEDIT_IM_BASE.SaveAsConfigExecute(Sender: TObject);
begin
  with SaveDialog1 do
  begin
    FileName := FController.SetupName;
    if Execute then
      FController.WriteSetup( FileName );
  end;
end;

procedure TEDIT_IM_BASE.OpenConfigExecute(Sender: TObject);
begin
  with OpenDialog2 do
    if Execute then
      if SetupIsSaved then
        FController.OpenSetup( FileName );
end;

procedure TEDIT_IM_BASE.EnableIfConfigured(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FController) and FController.CanRunImport;
  cbDevLog.Enabled := (Sender as TCustomAction).Enabled;
end;

function TEDIT_IM_BASE.CanClose: Boolean;
begin
  Result := FUserSaidDontSave or SetupIsSaved;
end;

function TEDIT_IM_BASE.SetupIsSaved: boolean;
var
  mr: TModalResult;
begin
  Result := true;
  if assigned( FController ) then
    if FController.SetupIsChanged then
    begin
      mr := EvMessage( 'Do you want to save import setup?', mtConfirmation, [mbYes, mbNo, mbCancel]);
      if mr = mrYes then
      begin
        SaveConfig.Execute;
        if FController.SetupIsChanged then
          mr := mrCancel;
      end
      else if mr = mrNo then
        FUserSaidDontSave := true;
      Result := mr <> mrCancel;
    end;
end;

procedure TEDIT_IM_BASE.RunImportExecute(Sender: TObject);
begin
  Assert( assigned(FController) );
  FController.RunImport;
end;

procedure TEDIT_IM_BASE.SetupNameChanged;
begin
  if assigned(FController) and (trim(FController.SetupName) <> '') then
    lblSetup.Caption := 'Using setup from '+ FController.SetupName
  else
    lblSetup.Caption := 'Using unnamed setup';
end;

procedure TEDIT_IM_BASE.SetController( aController: IImportVisualController );
begin
  if assigned(FController) then
  begin
    FController.AdviseSetupNameListener( nil );
    FController.AdviseLogFolderListener( nil );
    FController.SetImportQueueFrame( nil );
    FController.SetConnectionStringInputFrame( nil ); //set by subclasses
  end;
  FController := aController;
  if assigned(FController) then
  begin
    FController.AdviseSetupNameListener( Self );
    FController.AdviseLogFolderListener( Self );
    FController.SetImportQueueFrame( ImportQueueFrame );
  end;
  SetupNameChanged;
  LogFolderChanged;
  if assigned(FController)  then
    FController.LoadDefaults;
end;

procedure TEDIT_IM_BASE.SelectLogFolderExecute(Sender: TObject);
var
  selectedDir: string;
begin
  selectedDir := RunFolderDialog( 'Select folder for import log', FController.LogFolder );
  if selectedDir <> '' then
    FController.SetLogFolder( selectedDir );
end;

procedure TEDIT_IM_BASE.EnableIfHasController(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned( FController );
end;

procedure TEDIT_IM_BASE.LogFolderChanged;
begin
  if assigned(FController) and (trim(FController.LogFolder) <> '') then
    lblLogFolder.Caption := 'Import log will be saved in folder ' + FController.LogFolder
  else
    lblLogFolder.Caption := '';
end;

procedure TEDIT_IM_BASE.PutToQueueExecute(Sender: TObject);
begin
  FController.EnqueueImport;
end;

procedure TEDIT_IM_BASE.NewSetupExecute(Sender: TObject);
begin
  if SetupIsSaved then
    FController.NewSetup;
end;

procedure TEDIT_IM_BASE.cbDevLogClick(Sender: TObject);
begin
  FController.SetDevLogEnabled( cbDevLog.Checked );
end;

procedure TEDIT_IM_BASE.DevLogEnabledChanged;
begin
  cbDevLog.Checked := assigned(FController) and FController.DevLogEnabled;
end;

end.






