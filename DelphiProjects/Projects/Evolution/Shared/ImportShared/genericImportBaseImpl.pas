// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit genericImportBaseImpl;

interface

uses
  genericImport, classes, gdybinder, comobj, db, evExceptions, IEMap;

type
   //never try to create it!
  TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5 = class ( TContainedObject, IExternalDataModule )
  private
    FDataModuleInstance: TComponent;
    FDMImpl: IExternalDataModule;
    {IExternalDataModule}
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    procedure SetLogger( aLogger: IImportLogger );
    {ITablespace}
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  public
    constructor Create(Controller: IUnknown; aClass: TComponentClass );
    destructor Destroy; override;
  end;

  TImportBase = class (TInterfacedObject, IImport)
  private
    FDesc: TImportDesc;
    FDataModule: TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5;
    FMappingDescs: TMappingDescs;
    FLogger: IImportLogger;
    {IImport}
    procedure RunImport( aParams: TStrings; aMatchers: IMatchersList ); //template method that calls DoRunImport
    function ImportName: string;
    function MappingDesc( i: integer ): TBindingDesc;
    function MappingDescCount: integer;
  protected
    FImpEngine: IIEMap;
    function DataModule: IExternalDataModule;
    function DataModuleInstance: TComponent; //!! temp
  protected
    function Logger: IImportLogger;
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); virtual; abstract;
  public
    constructor Create(aLogger: IImportLogger; impdesc: TImportDesc; aMappingDescs: array of TBindingDesc );
    destructor Destroy; override;
  end;

implementation

uses
  sysutils, dialogs, evutils;

{ TImportBase }

constructor TImportBase.Create(aLogger: IImportLogger; impdesc: TImportDesc; aMappingDescs: array of TBindingDesc );
  procedure CopyRequiredMappingDescs;
  var
    i: integer;
    arm: TStringList;
  begin
    arm := TStringList.Create;
    try
      arm.Text := impdesc.RequiredMatchersNames;
      SetLength( FMappingDescs, 0 );
      for i := low(aMappingDescs) to high(aMappingDescs) do
        if arm.IndexOf(aMappingDescs[i].Name) <> -1 then
        begin
          SetLength( FMappingDescs, Length(FMappingDescs)+1 );
          FMappingDescs[high(FMappingDescs)] := aMappingDescs[i];
        end;
    finally
      FreeAndNil( arm );
    end;
  end;
begin
  FDesc := impdesc;
  FLogger := aLogger;
  FImpEngine := TIEMap.Create(FDesc.ImportName);
  FDataModule := TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.Create( Self as IUnknown, FDesc.ExternalDataModuleImplClass );
  FDataModule.SetLogger( aLogger );
  CopyRequiredMappingDescs;
end;

function TImportBase.DataModule: IExternalDataModule;
begin
  Result := FDataModule as IExternalDataModule;
end;

function TImportBase.DataModuleInstance: TComponent;
begin
  Result := FDataModule.FDataModuleInstance;
end;

destructor TImportBase.Destroy;
begin
  inherited;
  FreeAndNil( FDataModule );
end;

function TImportBase.ImportName: string;
begin
  Result := FDesc.ImportName;
end;

function TImportBase.Logger: IImportLogger;
begin
  Result := FLogger;
end;

function TImportBase.MappingDesc(i: integer): TBindingDesc;
begin
  Result := FMappingDescs[i];
end;

function TImportBase.MappingDescCount: integer;
begin
  Result := Length(FMappingDescs);
end;

procedure TImportBase.RunImport(aParams: TStrings; aMatchers: IMatchersList);
var
  importDefinition: IImportDefinition;
begin
  FLogger.LogEntry( 'RunImport' );
  try
    try
      FLogger.LogContextItem( sciCompanyCustomNumber, DataModule.CustomCompanyNumber );
      FLogger.LogEventFmt( '%s started', [ImportName] );
      try
        importDefinition := FDesc.ImportDefinitionImplClass.Create(FimpEngine) as IImportDefinition;
        importDefinition.SetLogger(FLogger);
        importDefinition.SetMatchers( aMatchers );
        DoRunImport( importDefinition, aParams );
        FLogger.LogEventFmt( '%s finished', [ImportName] );
      except
        FLogger.PassthroughExceptionAndWarnFmt( '%s aborted', [ImportName] );
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

{ TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5 }

constructor TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.Create(Controller: IUnknown; aClass: TComponentClass );
begin
  inherited Create( Controller );
  FDataModuleInstance := aClass.Create( nil );
  if not FDataModuleInstance.GetInterface(IExternalDataModule, FDMImpl) then
    raise EevException.CreateFmt( 'Cannot create datamodule: class %s doesn''t implements IExternalDataModule interface', [aClass.ClassName] )
end;

destructor TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.Destroy;
begin
  inherited;
  FDMImpl := nil;
  FreeAndNil( FDataModuleInstance );
//  showmessage( 'TExternalDataModule.Destroy');
end;

{TExternalDataModule.ITablespace}
procedure TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.GetAvailableTables(const list: TStrings);
begin
  FDMImpl.GetAvailableTables( list );
end;

function TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.GetTable(const name: string): TDataSet;
begin
  Result := FDMImpl.GetTable( name );
end;

{TExternalDataModule.IExternalDataModule}
procedure TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.OpenConnection(aConnectionString: string);
begin
  FDMImpl.OpenConnection( aConnectionString );
end;

procedure TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.CloseConnection;
begin
  FDMImpl.CloseConnection;
end;

function TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.Connected: boolean;
begin
  Result := FDMImpl.Connected;
end;

function TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.ConnectionString: string;
begin
  Result := FDMImpl.ConnectionString;
end;

function TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.CustomCompanyNumber: string;
begin
  Result := FDMImpl.CustomCompanyNumber;
end;

procedure TExternalDataModuleHolder_F07824FD_1C32_4E7A_BC97_A6F6332A23D5.SetLogger(
  aLogger: IImportLogger);
begin
  FDMImpl.SetLogger( aLogger );
end;

end.
