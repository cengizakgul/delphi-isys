object ImportSystemAndUserDBPathsInputFrame: TImportSystemAndUserDBPathsInputFrame
  Left = 0
  Top = 0
  Width = 503
  Height = 139
  TabOrder = 0
  object evPanel3: TevPanel
    Left = 0
    Top = 0
    Width = 503
    Height = 139
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object lblSystemDB: TevLabel
      Left = 184
      Top = 16
      Width = 195
      Height = 13
      Caption = 
        '                                                                ' +
        ' '
    end
    object lblClientDB: TevLabel
      Left = 184
      Top = 48
      Width = 195
      Height = 13
      Caption = 
        '                                                                ' +
        ' '
    end
    object bbSystemDB: TevBitBtn
      Left = 8
      Top = 8
      Width = 153
      Height = 25
      Caption = 'Select System Database'
      TabOrder = 0
      OnClick = bbSystemDBClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
        B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
        B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
        0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
        55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
        55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
        55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
        5555575FFF755555555557000075555555555577775555555555}
      NumGlyphs = 2
      Margin = 0
    end
    object bbClientDB: TevBitBtn
      Left = 8
      Top = 40
      Width = 153
      Height = 25
      Caption = 'Select Client Database'
      TabOrder = 1
      OnClick = bbClientDBClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
        B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
        B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
        0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
        55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
        55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
        55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
        5555575FFF755555555557000075555555555577775555555555}
      NumGlyphs = 2
      Margin = 0
    end
    object evBitBtn1: TevBitBtn
      Left = 8
      Top = 72
      Width = 153
      Height = 25
      Action = connect
      TabOrder = 2
      NumGlyphs = 2
      Margin = 0
    end
    object evBitBtn2: TevBitBtn
      Left = 8
      Top = 104
      Width = 153
      Height = 25
      Action = Disconnect
      TabOrder = 3
      NumGlyphs = 2
      Margin = 0
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '.mdb'
    FileName = 'E:\job\database\access\blit.MDB'
    Filter = 'Access database (*.mdb)|*.mdb'
    Options = [ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Select file to import from'
    Left = 464
    Top = 8
  end
  object evActionList1: TevActionList
    Left = 472
    Top = 24
    object connect: TAction
      Caption = 'Connect'
      OnExecute = connectExecute
      OnUpdate = connectUpdate
    end
    object Disconnect: TAction
      Caption = 'Disconnect'
      OnExecute = DisconnectExecute
      OnUpdate = DisconnectUpdate
    end
  end
end
