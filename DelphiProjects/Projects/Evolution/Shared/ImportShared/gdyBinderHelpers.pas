// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyBinderHelpers;

interface

uses
  gdybinder, classes, EvContext, evExceptions;

type
  TNamedInterfaceList = class ( TInterfacedObject )
  protected
    FList: IInterfaceList;
  public
    constructor Create;
  end;
                                                  
  TBindingKeepersList = class ( TNamedInterfaceList, IBindingKeepersList )
  public
    procedure Add( aBindingKeeper: IBindingKeeper );
    procedure Remove( aBindingKeeper: IBindingKeeper );
    function Get( aBindingName: string ): IBindingKeeper;
    function GetByIndex( i: integer ): IBindingKeeper;
    function Count: integer;
  end;

  IBindingKeepers = interface (IBindingKeepersList)
  ['{C467E5E5-D9A7-4234-9FF5-93B45AF8F7E7}']
    procedure Deactivating;
    procedure SetConnected( aConnected: boolean );
    procedure ApplySetup( aSetupFileName: string );
    procedure WriteSetup( aSetupFileName: string );
    function MatchersAvailable: boolean;
    function IsChanged: boolean;
  end;

  TBindingKeepers = class ( TBindingKeepersList, IBindingKeepers )
  private
    {IBindingKeepers}
    procedure Deactivating;
    procedure SetConnected( aConnected: boolean );
    procedure ApplySetup( aSetupFileName: string );
    procedure WriteSetup( aSetupFileName: string );
    function MatchersAvailable: boolean;
    function IsChanged: boolean;
  public
  end;

  TMatchersList = class ( TNamedInterfaceList, IMatchersList )
  public
    procedure Add( aMatch: IMatcher );
    procedure Remove( aMatch: IMatcher );
    function Get( aBindingName: string ): IMatcher;
  end;

  TBoundTableDescAdapter = class
  private
    FDesc: TBoundTableDesc;
    FPrefix: string;
    FPrefixedKeyFields: string;
  protected
    property KeyFields: string read FDesc.Keyfields;
  public
    constructor Create( btd: TBoundTableDesc; aPfx: string );
    function CopiedFields: string;
    property Prefix: string read FPrefix;
    property Unique: boolean read FDesc.Unique;
    property PrefixedKeyFields: string read FPrefixedKeyFields;
    function PrefixedCopiedFields: string;
  end;

function FieldListAddPfx( fieldlist: string; pfx: string ): string;
function FieldListConcat( fs: array of string ): string;

implementation

uses
  sysutils, gdyclasses, evutils; 

function FieldListConcat( fs: array of string ): string;
var
  i: integer;
begin
  Result := '';
  for i := low(fs) to high(fs) do
  begin
    if Result <> '' then
      Result := Result + ';';
    Result := Result + fs[i];
  end
end;

function FieldListAddPfx( fieldlist: string; pfx: string ): string;
var
  i: integer;
begin
  Result := '';
  with SplitStr( fieldlist, ';') do
    for i := 0 to Count-1 do
    begin
      if Result <> '' then
        Result := Result + ';';
      Result := Result + pfx + str[i];
    end;
end;

{ TMatches }

procedure TMatchersList.Add(aMatch: IMatcher);
begin
  FList.Add( aMatch as IUnknown );
end;

function TMatchersList.Get(aBindingName: string): IMatcher;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to FList.Count-1 do
    if (FList[i] as IMatcher).BindingName = aBindingName then
    begin
      Result := FList[i] as IMatcher;
      break;
    end;
  if Result = nil then
    raise EevException.CreateFmt( 'Cannot find mapping data for <%s>',[aBindingName]);
end;

procedure TMatchersList.Remove(aMatch: IMatcher);
begin
  FList.Remove( aMatch as IUnknown ); 
end;

{ TBoundTableDescAdapter }
constructor TBoundTableDescAdapter.Create(btd: TBoundTableDesc;
  aPfx: string);
begin
  FDesc := btd;
  FPrefix := aPfx;
  FPrefixedKeyFields := FieldListAddPfx( FDesc.KeyFields, FPrefix );
end;

function TBoundTableDescAdapter.CopiedFields: string;
begin
  Result := FieldListConcat( [FDesc.ListFields, FDesc.KeyFields] );
end;

function TBoundTableDescAdapter.PrefixedCopiedFields: string;
begin
  Result := FieldListAddPfx( CopiedFields, Prefix );
end;

{ IBindingKeepersList }

procedure TBindingKeepersList.Add(aBindingKeeper: IBindingKeeper);
begin
  FList.Add( aBindingKeeper as IUnknown );
end;

function TBindingKeepersList.Count: integer;
begin
  Result := FList.Count;
end;

function TBindingKeepersList.Get(aBindingName: string): IBindingKeeper;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to FList.Count-1 do
    if (FList[i] as IBindingKeeper).BindingName = aBindingName then
    begin
      Result := FList[i] as IBindingKeeper;
      break;
    end;
  if Result = nil then
    raise EevException.CreateFmt( 'Cannot find binding keeper with name <%s>',[aBindingName]);
end;

function TBindingKeepersList.GetByIndex(i: integer): IBindingKeeper;
begin
  Result := FList[i] as IBindingKeeper;
end;

procedure TBindingKeepersList.Remove(aBindingKeeper: IBindingKeeper);
begin
  FList.Remove( aBindingKeeper as IUnknown  ); 
end;

{ TNamedInterfaceList }

constructor TNamedInterfaceList.Create;
begin
  FList := TInterfaceList.Create;
end;

{ TBindingKeepers }

procedure TBindingKeepers.ApplySetup(aSetupFileName: string);
var
  i: integer;
begin
//  ctx_StartWait( 'Analyzing setup' );
//  try
    ctx_UpdateWait;
    inherited;
    for i := 0 to Count-1 do
      if trim(aSetupFileName) <> '' then
        GetByIndex(i).SetMatchTable( ComposeBindingFilename( aSetupFileName, GetByIndex(i).BindingName ) )
      else
        GetByIndex(i).SetMatchTable( '' );
//  finally
//    ctx_EndWait;
//  end;
end;

procedure TBindingKeepers.Deactivating;
var
  i: integer;
begin
  for i := 0 to Count-1 do
  begin
    GetByIndex(i).SetTables( nil, nil );
    GetByIndex(i).SetVisualBinder( nil );
  end;
end;

function TBindingKeepers.IsChanged: boolean;
var
  i: integer;
begin
  Result := false;
  for i := 0 to Count-1 do
    if GetByIndex(i).IsMatchTableChanged then
    begin
      Result := true;
      break;
    end;
end;

function TBindingKeepers.MatchersAvailable: boolean;
var
  i: integer;
begin
  Result := true;
  for i := 0 to Count-1 do
    if not GetByIndex(i).CanCreateMatcher then
    begin
      Result := false;
      break;
    end;
end;

procedure TBindingKeepers.SetConnected(aConnected: boolean);
var
  i: integer;
begin
  for i := 0 to Count-1 do
    GetByIndex(i).SetConnected( aConnected );
end;

procedure TBindingKeepers.WriteSetup(aSetupFileName: string);
var
  i: integer;
begin
  for i := 0 to Count-1 do
    GetByIndex(i).SaveMatchTable( ComposeBindingFilename( aSetupFileName, GetByIndex(i).BindingName ) );
end;

end.
