// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportHelpers;
{$include gdy.inc}

interface

uses
  controls, sFieldCodeValues, genericImport, evExceptions, IEMap;

function ConvertDBDTCustomNumber( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertDBDTCustomNumberAllowNull( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertEECustomNumber( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertToCardinal( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertSSN( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertPhone( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertFEIN( const aLogger: IImportLogger; const Value: Variant ): Variant; //empty or 9 digits
function ConvertName( const aLogger: IImportLogger; const Value: Variant ): Variant;
function ConvertToNotNullChar( const aLogger: IImportLogger; const Value: Variant ): Variant;
function Verbatim( const aLogger: IImportLogger; const Value: Variant ): Variant; //used to avoid default conversion for strings (trimming)
function DateAsString( const aLogger: IImportLogger; const Value: Variant ): Variant; 

function GetCustomBankAccountNumber(const AIEMap: IIEMap): string;

//this is a generalization of the PaySuite import logic
//if I will make changes in PS import then I will remove code duplication

type
  TPrDef = record
    Quarter: integer;
    IncludePrevQuarters: boolean;
    CheckDate: TDate;
    PeriodBeginDate: TDate;
    PeriodEndDate: TDate;
    CheckType: char;
  end;
  TPrDefs = array of TPrDef;

procedure CalcPayrollDefs( aCheckDate: TDate; var aPrDefs: TPrDefs );

implementation

uses
  sysutils, evutils,  math, variants, evconsts, dateutils;

function ConvertSpaces( Value: Variant ): Variant;
var
  val: string;
begin
  Result := VarToStr(Value);
  val := VarToStr(Value);
  if ( Length(val) > 0 ) and (trim(val) = '') then
    Result := StringReplace( val, ' ', '_', [rfReplaceAll]);
  if Result = '' then
    Result := '_';
end;

function ConvertDBDTCustomNumber( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := ConvertSpaces( Value );
  if VarToStr(Result) <> '' then
    Result := PadStringLeft( VarToStr( Result ), ' ', 20 );
end;

function ConvertDBDTCustomNumberAllowNull( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  if VarIsNull(Value) then
    Result := Null
  else
    Result := ConvertDBDTCustomNumber( aLogger, Value );
end;

function ConvertEECustomNumber( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := ConvertSpaces( Value );
  if VarToStr(Result) <> '' then
    Result := PadStringLeft( VarToStr( Result ), ' ', 9 );
end;

{$HINTS OFF}
{
function IsInteger( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := code = 0;
end;
}
{$HINTS ON}

function ConvertToInteger( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  if trim(VarToStr(Value)) = '' then
    Result := 0
  else
    Result := Value;
//  else if not IsInteger( VarToStr(Value) ) then
//    Result := Null;
end;

function ConvertToCardinal( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := ConvertToInteger( aLogger, Value );
  if Result < 0 then
    raise EevException.CreateFmt( 'Expected non negative integer value, but get <%s>',[ VarToStr(Value) ] );
end;

function ConvertSSN( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  val: string;
  s: string;
  i: integer;
begin
  if Value = 0 then
    Result := CreateFakeSSN
  else
  begin
    s := '';
    val := vartostr( Value );
    for i := 1 to Length(val) do
      if val[i] in ['0'..'9'] then
        s := s + val[i];
    s := PadStringLeft( s, '0', 9);
    Result := Format('%s%s%s-%s%s-%s%s%s%s',[s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8],s[9]]);
  end;
end;

function ConvertPhone( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  val: string;
begin
  if Value = 0 then
    Result := null
  else
  begin
    val := vartostr( Value );
    if Length(val) > 4 then
      Insert('-', val, Length(val) - 3);
    if Length(val) > 8 then
      Insert('-', val, Length(val) - 7);
    Result := val;
  end;
end;

function ConvertFEIN( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  val: string;
  s: string;
  i: integer;
begin
  s := '';
  val := vartostr( Value );
  if trim(val) <> '' then
  begin
    for i := 1 to Length(val) do
      if val[i] in ['0'..'9'] then
        s := s + val[i];
    if Length(s) < 9 then
      raise EevException.Create( 'Federal EIN must have at least 9 digits' )
    else
      Result := s;
  end
end;

function ConvertName( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  if not VarIsNull( Value ) then
    Result := CapitalizeWords(VarToStr(Value))
  else
    Result := Value;
end;

function ConvertToNotNullChar( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := Value;
  if VarIsNull( Value ) or (trim(VarToStr( Value )) = '') then
    raise EevException.Create( 'Field cannot have empty value' );
end;

function Verbatim( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := Value;
end;

function DateAsString( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  s: string;
begin
  Assert( VarIsType(Value, varDate));
  if not VarIsNull(Value) and not VarIsEmpty(Value) then
  begin
    DateTimeToString(s, ShortDateFormat, Value);
    Result := s;
  end
  else
    Result := Value;
end;

{TPaymentSerialNbrGen}
function GetCustomBankAccountNumber(const AIEMap: IIEMap): string;
var
  BankAcctNbr: integer;
begin
  BankAcctNbr := AIEMap.IDS('CO')['PAYROLL_CL_BANK_ACCOUNT_NBR'];
  AIEMap.CurrentDataRequired( AIEMap.IDS('CL_BANK_ACCOUNT') );
  Result := AIEMap.IDS('CL_BANK_ACCOUNT').Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
end;

//////////////

function DateIsIn1stQuarter( aDate: TDate ): boolean;
var
  y, m, d: word;
begin
  DecodeDate( aDate, y, m, d );
  Result := m in [1..3];
end;

function DaysPerMonth(AYear, AMonth: Integer): Integer;
const
  DaysInMonth: array[1..12] of Integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then Inc(Result); { leap-year Feb is special }
end;

function DateIsQuarterEnd( aDate: TDate ): boolean;
var
  y, m, d: word;
begin
  DecodeDate( aDate, y, m, d );
  Result := ( m in [3,6,9,12] ) and ( d = DaysPerMonth(y, m) );
end;

function QuarterFromMonth( m: Word ): integer;
begin
  Result := ((m-1) div 3) + 1;
end;

procedure  CalcPayrollDefs( aCheckDate: TDate; var aPrDefs: TPrDefs );
var
  y, m, d: Word;
  EndOfQuarterMonth: Word;
  i: integer;
begin
  DecodeDate( aCheckDate, y, m, d );
  if DateIsIn1stQuarter( aCheckDate ) or DateIsQuarterEnd( aCheckDate ) then // ytd only
  begin
    SetLength( aPrDefs, 1 );
    with aPrDefs[0] do
    begin
      IncludePrevQuarters := true;
      PeriodBeginDate := EncodeDate( y, 1, 1);
      PeriodEndDate := aCheckDate;
    end;
  end
  else
  begin
    SetLength( aPrDefs, 2 );
    EndOfQuarterMonth := ( ( (m-1) div 3 ) * 3 - 1 ) + 1; // we are not in the 1st quarter
    with aPrDefs[0] do
    begin
      IncludePrevQuarters := true;
      PeriodBeginDate := EncodeDate( y, 1, 1);
      PeriodEndDate := EncodeDate( y, EndOfQuarterMonth, DaysPerMonth(y, EndOfQuarterMonth) );
    end;
    with aPrDefs[1] do
    begin
      IncludePrevQuarters := false;
      PeriodBeginDate := EncodeDate( y, EndOfQuarterMonth + 1, 1);
      PeriodEndDate := aCheckDate;
    end;
  end;

  for i := low(aPrDefs) to high(aPrDefs) do
  begin
    aPrDefs[i].CheckType := CHECK_TYPE2_MANUAL;
    aPrDefs[i].CheckDate := aPrDefs[i].PeriodEndDate;
    aPrDefs[i].Quarter := QuarterFromMonth( MonthOf( aPrDefs[i].PeriodEndDate ) );
  end;
end;


end.
