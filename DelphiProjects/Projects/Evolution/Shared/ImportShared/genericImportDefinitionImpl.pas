// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit genericImportDefinitionImpl;
{$include gdy.inc}

interface

uses
  iemap, genericImport, gdyBinder,  classes, db, sysutils,
  webcontnrs, evExceptions, EvClientDataSet;

type

  //convert function must convert value or raise exception
  //e.g. if function converts SSN to Evo's format xxx-xx-xxxx then it should raise exception
  //for string which is empty, non-digit or length<>9
  TConvertFunc = function( const aLogger: IImportLogger; const Value: Variant ): Variant;
  TConvertDesc = record
    Table: string;
    Field: string;
    Func: TConvertFunc;
  end;
  TConvertDescs = array of TConvertDesc;

type
  TSetOfChar = set of char;


  TImportDefinition = class(TInterfacedObjectWithVirtualCtor, IImportDefinition )
  private
    FMap: TMapRecords;
    FConvertDescs: TConvertDescs;
    FKeySourceDescs: TKeySourceDescs;
    FRowImportDescs: TRowImportDescs;
    FMatchers: IMatchersList;
    FFixedKeys: TStringList;
    FLogger: IImportLogger;
    FParams: TNamedVariantsList;
    procedure CalcKey( aTableSpace: ITablespace; aRow, aKeys: TFieldValues; ksd: TKeySourceDesc; aTablename: string; var errmsg: string );
  private
    function GetMethod(aMethodName: string; out method: TMethod): boolean;
    procedure ResolveMethod(aMethodName: string; out method: TMethod);
    {IIEDefinition}
    function ExecuteCustomFunc( aMethodName: string; IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): boolean;
    procedure ExecuteRowCustomFunc( const aMethodName: string; const Input, Keys, Output: TFieldValues );
    procedure ExecuteAfterInsert(aMethodName: string; const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray);
    {IImportDefinition}
    procedure SetLogger( aLogger: IImportLogger );
    function ConvertValue( aTableName, aFieldName: string; const aValue: Variant): Variant;
    procedure GetRowKeys( aTableSpace: ITablespace; aRow, aKeys: TFieldValues; aTablename: string );
    procedure AddFieldKeys( aTableSpace: ITablespace; aRow, aKeys: TFieldValues; aTablename, aFieldName: string );
    function Map: TMapRecords;
    function RowImport: TRowImportDescs;
    procedure SetFixedKeys( aFixedKeys: string );
    procedure SetParam( ParamName: string; Value: Variant );
    procedure SetMatchers( aMatchers: IMatchersList );

  protected
    function GetSingleChar( const Value: Variant; userFriendlyFieldName: string; const allowedValues: TSetOfChar): char;

    procedure RaiseMissingParent( const msg: string; const args: array of const );
    procedure RaiseMissingValue( const msg: string; const args: array of const );
    procedure RaiseUnknownCode( const msg: string; const args: array of const ); //to be removed; superseded by GetSingleChar
    function  GetParam( ParamName: string ): Variant;
    function  Matchers: IMatchersList;
    property Logger: IImportLogger read FLogger;
  (*public*)
    procedure RegisterConvertDescs( aConvertDescs: array of TConvertDesc );
    procedure RegisterKeySourceDescs( aKeySourceDescs: array of TKeySourceDesc );
    procedure RegisterMap( aMap: array of TMapRecord );
    procedure RegisterRowImport( aRowImportDescs: array of TRowImportDesc );
  public
    constructor Create(const AIEMap: IIEMap); override;
    destructor Destroy; override;
  end;


//function IsSingleChar( const Value: Variant; out aChar: char ): boolean;

implementation

uses
  gdybinderHelpers{$ifdef D6_UP},variants{$endif};

{ TImportDefinition }

function TImportDefinition.ConvertValue(aTableName, aFieldName: string;
  const aValue: Variant): Variant;
var
  i: integer;
  explicitlyConverted: boolean;
begin
  explicitlyConverted := false;
  Result := aValue;
  for i := low(FConvertDescs) to high(FConvertDescs) do
    if (FConvertDescs[i].Table = aTableName) and (FConvertDescs[i].Field = aFieldName) then
    begin
      explicitlyConverted := true;
      Result := FConvertDescs[i].Func( Logger, aValue );
      break;
    end;
  if not explicitlyConverted and ( (VarType(aValue) = varString) or (VarType(aValue) = varOleStr) ) then
  begin
    Result := trim(VarToStr(aValue));
//    if VarToStr(aValue) <> trim(VarToStr(aValue)) then
//      ODS('trim: '+aTableName+'.'+aFieldName+': <'+VarToStr(aValue)+'> => <'+trim(VarToStr(aValue))+'>');
  end;
end;


type
  TImportCustomProc = procedure (const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues ) of object;
  TImportRecordCustomProc = procedure ( const Input, Keys, Output: TFieldValues ) of object;
  TAfterInsertCustomProc = procedure (const ds: TevClientDataSet; const Keys: TFieldValues ) of object;

function TImportDefinition.ExecuteCustomFunc(aMethodName: string;
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): boolean;
var
  cp: TImportCustomProc;
  keys: TFieldValues;
  output: TFieldValues;
begin
  Assert( IsImport );
  if IsImport then
  begin
    keys := TFieldValues.Create;
    try
      keys.Tables := KeyTables;
      keys.Fields := KeyFields;
      keys.Values := KeyValues;
      output := TFieldValues.Create;
      try
        ResolveMethod( aMethodName, TMethod(cp) );
        cp( Table, Field, Value, keys, output );
        Result := output.Count > 0;
        KeyTables := keys.Tables;
        KeyFields := keys.Fields;
        KeyValues := keys.Values;
        OTables := output.Tables;
        OFields := output.Fields;
        OValues := output.Values;
      finally
        FreeAndNil( output );
      end;
    finally
      FreeAndNil( keys );
    end;
  end
  else
    Result := false;
end;

procedure TImportDefinition.ExecuteRowCustomFunc( const aMethodName: string; const Input, Keys,
  Output: TFieldValues);
var
  rcp: TImportRecordCustomProc;
begin
  ResolveMethod( aMethodName, TMethod(rcp) );
  with Keys do
    FImpEngine.RestrictKeyTables( Tables, Fields, Values );
  rcp( Input, Keys, Output );
end;

procedure TImportDefinition.ExecuteAfterInsert(aMethodName: string;
  const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray;
  const KeyValues: TVariantArray);
var
  ai: TAfterInsertCustomProc;
  keys: TFieldValues;
begin
  keys := TFieldValues.Create;
  try
    keys.Tables := KeyTables;
    keys.Fields := KeyFields;
    keys.Values := KeyValues;
    if GetMethod( aMethodName, TMethod(ai) ) then
      ai( ds, keys );
  finally
    FreeAndNil( keys );
  end;
end;

function TImportDefinition.Map: TMapRecords;
begin
  Result := FMap;
end;

function TImportDefinition.Matchers: IMatchersList;
begin
  Result := FMatchers;
end;

procedure TImportDefinition.RaiseMissingParent( const msg: string;
  const args: array of const);
begin
  raise EevException.CreateFmt( msg, args );
end;

procedure TImportDefinition.RaiseMissingValue( const msg: string;
  const args: array of const);
begin
  raise EevException.CreateFmt( msg, args );
end;

procedure TImportDefinition.RaiseUnknownCode( const msg: string; const args: array of const);
begin
  raise EevException.CreateFmt( msg, args );
end;


function IsSingleChar( const Value: Variant; out aChar: char ): boolean;
var
  s: string;
begin
  s := trim(VarToStr( Value ));
  Result := length(s) = 1;
  if Result then
    aChar := s[1];
end;

function TImportDefinition.GetSingleChar( const Value: Variant; userFriendlyFieldName: string; const allowedValues: TSetOfChar ): char;
begin
  if not IsSingleChar( Value, Result ) then
     RaiseUnknownCode( '%s expected to be CHAR(1) NOT NULL but found: <%s>', [userFriendlyFieldName, VarToStr(Value)])
  else
    if (allowedValues <> []) and not (result in allowedValues) then
      RaiseUnknownCode( 'Unknown code in %s: <%s>', [userFriendlyFieldName, result]);
end;

procedure TImportDefinition.ResolveMethod(aMethodName: string;
  out method: TMethod);
begin
   if not GetMethod( aMethodName, method ) then
     raise EevException.CreateFmt( 'Cannot find method <%s> in <%s>',[aMethodName, ClassName]);
end;

function TImportDefinition.RowImport: TRowImportDescs;
begin
  Result := FRowImportDescs;
end;

procedure TImportDefinition.RegisterConvertDescs(
  aConvertDescs: array of TConvertDesc);
var
  i: integer;
  c: integer;
begin
  c := Length(FConvertDescs);
  SetLength( FConvertDescs, c + Length(aConvertDescs) );
  for i := low(aConvertDescs) to high(aConvertDescs) do
    FConvertDescs[c+i] := aConvertDescs[i];
end;

procedure TImportDefinition.RegisterKeySourceDescs(
  aKeySourceDescs: array of TKeySourceDesc);
var
  i: integer;
  c: integer;
begin
  c := Length(FKeySourceDescs);
  SetLength( FKeySourceDescs, c + Length(aKeySourceDescs) );
  for i := low(aKeySourceDescs) to high(aKeySourceDescs) do
    FKeySourceDescs[c+i] := aKeySourceDescs[i];
end;

procedure TImportDefinition.RegisterMap(aMap: array of TMapRecord);
var
  i: integer;
  c: integer;
begin
  c := Length(FMap);
  SetLength(FMap, c + Length(aMap));
  for i := low(aMap) to high(aMap) do
    FMap[c+i] := aMap[i];
end;

procedure TImportDefinition.RegisterRowImport(
  aRowImportDescs: array of TRowImportDesc);
var
  i: integer;
  c: integer;
begin
  c := Length(FRowImportDescs);
  SetLength(FRowImportDescs, c + Length(aRowImportDescs));
  for i := low(aRowImportDescs) to high(aRowImportDescs) do
    FRowImportDescs[c+i] := aRowImportDescs[i];
end;

function TImportDefinition.GetMethod(aMethodName: string;
  out method: TMethod): boolean;
begin
  //getmethodprop
  method.Data := Self;
  method.Code := MethodAddress( aMethodName );
  Result := assigned( method.Code );
end;

function TImportDefinition.GetParam(ParamName: string): Variant;
begin
  Result := FParams.Values[ParamName];
end;

procedure TImportDefinition.SetParam(ParamName: string; Value: Variant);
begin
  FParams.Values[ParamName] := Value;
end;

procedure TImportDefinition.SetMatchers(aMatchers: IMatchersList);
begin
  FMatchers := aMatchers;
end;

procedure TImportDefinition.CalcKey(aTableSpace: ITablespace; aRow, aKeys: TFieldValues; ksd: TKeySourceDesc; aTablename: string; var errmsg: string );
var
  Value: Variant;
  keySource: string;
begin
  keySource := '';
  if ksd.HowToGet = ksEmpty then
    Value := Unassigned
  else
  begin
    Value := Null;
    case ksd.HowToGet of
      ksFixedValue:
        Value := FFixedKeys.Values[ksd.EvoKeyTable+'.'+ksd.EvoKeyField];
      ksFromThisTable:
        begin
          Value := aTablespace.GetTable(aTableName)[ksd.ValueSourceField];
          try
            Value := ConvertValue( aTableName, ksd.ValueSourceField, Value );
            keySource := 'Value is taken from '+aTableName+'.'+ksd.ValueSourceField;
          except
            on E: Exception do
              begin
                keySource := 'Cannot convert value: '+aTableName+'.'+ksd.ValueSourceField + '=<' + VarToStr(Value) + '> ('+E.Message+')';
                Value := Null;
              end
          end;
        end;
      ksLookup:
        begin
          Value := aTablespace.GetTable(ksd.LookupTable).Lookup(ksd.LookupKeyField, aTablespace.GetTable(aTableName)[ksd.KeyField], ksd.ValueSourceField);
          try
            Value := ConvertValue( ksd.LookupTable, ksd.ValueSourceField, Value );
            keySource := 'Value is taken from lookup field: '+ksd.LookupTable+'.'+ksd.ValueSourceField +
            ' (lookup key value: '+aTableName+'.'+ksd.KeyField+'=<'+VarToStr(aTablespace.GetTable(aTableName)[ksd.KeyField])+'>'+
            ' lookup key: '+ksd.LookupKeyField+ ')';
          except
            on E: Exception do
              begin
                keySource := 'Cannot convert value: '+ksd.LookupTable+'.'+ksd.ValueSourceField + '=<' + VarToStr(Value) + '> ('+E.Message+')';
                Value := Null;
              end
          end;
        end;
      else
        keySource := 'Don''t know how to get key value';
    end;
    if VarIsEmpty(Value) then
      Value := Null;
  end;
  if not VarIsEmpty(Value) and (trim(VarToStr(Value)) = '') then
  begin
    if errmsg <> '' then
      errmsg := errmsg + #13#10;
    errMsg := errMsg  + 'Cannot get value for key field: '+ksd.EvoKeyTable+'.'+ksd.EvoKeyField+'. '+keySource;
  end;
  aKeys.Add( ksd.EvoKeyTable, ksd.EvoKeyField, Value );
end;

procedure TImportDefinition.GetRowKeys(aTableSpace: ITablespace; aRow, aKeys: TFieldValues; aTablename: string);
var
  i: integer;
  errMsg: string;
begin
  errMsg := '';
  for i := low(FKeySourceDescs) to high(FKeySourceDescs) do
    if AnsiSameText( FKeySourceDescs[i].ExternalTable, aTableName ) and ( FKeySourceDescs[i].ExternalField = '' ) then
      CalcKey( aTableSpace, aRow, aKeys, FKeySourceDescs[i], aTablename, errmsg );
  if errMsg <> '' then
    raise EevException.Create(errMsg);
end;

procedure TImportDefinition.AddFieldKeys(aTableSpace: ITablespace; aRow,
  aKeys: TFieldValues; aTablename, aFieldName: string);
var
  i: integer;
  errMsg: string;
begin
  errMsg := '';
  for i := low(FKeySourceDescs) to high(FKeySourceDescs) do
    if AnsiSameText( FKeySourceDescs[i].ExternalTable, aTableName ) and AnsiSameText( FKeySourceDescs[i].ExternalField, aFieldName ) then
      CalcKey( aTableSpace, aRow, aKeys, FKeySourceDescs[i], aTablename, errmsg );
  if errMsg <> '' then //!!
    raise EevException.Create(errMsg);
end;


procedure TImportDefinition.SetFixedKeys(aFixedKeys: string);
begin
  FFixedKeys.Text := aFixedKeys;
end;

constructor TImportDefinition.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);

  FFixedKeys := TStringList.Create;
  FParams := TNamedVariantsList.Create;
end;

destructor TImportDefinition.Destroy;
begin
  inherited;
  FreeAndNil( FParams ); 
  FreeAndNil( FFixedKeys );
end;

procedure TImportDefinition.SetLogger(aLogger: IImportLogger);
begin
  FLogger := aLogger;
end;

end.

