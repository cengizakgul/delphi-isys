// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyBinderImpl;
{$include gdy.inc}

interface

uses
  classes, gdybinder, db, gdyclasses, gdyBinderHelpers, gdyResourceManager, EvDataAccessComponents,
  isBasicUtils, EvStreamUtils, isBasicClasses, EvClientDataSet;

type

  TMatchTableKeeper = class( TInterfacedObject, IMatchTableKeeper )
  private
    FLeft: TBoundTableDescAdapter;
    FRight: TBoundTableDescAdapter;
    FMatchTable: TEvBasicClientDataSet;
    FBindingDesc: TBindingDesc;
  public
    constructor Create( bd: TBindingDesc; aLeftTable, aRightTable: TDataSet; aBindingTableFileName: string; cdsClass: TCDSBaseClass);
    destructor Destroy; override;
    {IMatchTableKeeper}
    function MatchTable: TDataSet;
    procedure Save(aFileName: string);
    function IsChanged: boolean;
    function CreateMatcher: IMatcher;
  end;

  TBoundTable = class (TBoundTableDescAdapter)
  private
    FDataSet: TDataSet;
    procedure InternalUpdateMatchIndicator(aMatch: TDataSet; aKeyValues: Variant);
    property DataSet: TDataSet read FDatasEt;
  public
    constructor Create( aCdsClass: TCDSBaseClass; aSrc: TDataSet; btd: TBoundTableDesc; aPfx: string);
    destructor Destroy; override;
    procedure SetMatchIndicator(isMatched: boolean); //may have duplicates
    procedure UpdateMatchIndicator(aMatch: TDataSet; aKeyValues: Variant);
    procedure UpdateMatchIndicators( aMatch: TDataSet );
    function KeyValues: Variant;
    function CopiedValues: Variant;
  end;

  TBinder = class (TInterfacedObject, IBinder)
  private
    FMatchTableKeeper: IMatchTableKeeper;
    FLeft: TBoundTable;
    FRight: TBoundTable;
    FBindingDesc_: TBindingDesc;
  protected
    {IBinder}
    function LeftTable: TDataSet;
    function RightTable: TDataSet;
    function MatchTable: TDataSet;
    function BindingDesc: TBindingDesc;

    function LeftKeyValues: Variant;
    function RightKeyValues: Variant;
    procedure AddMatch( aLeftKeys, aRightKeys: Variant );
    procedure RemoveMatch;
    procedure RemoveAllMatches;
  public
    constructor Create( bd: TBindingDesc; aLeftTable, aRightTable: TDataSet; aMatchTableKeeper: IMatchTableKeeper; cdsClass: TCDSBaseClass );
    destructor Destroy; override;
  end;


implementation

uses
  sysutils, evutils, gdystrset, forms,
  gdycommon {$ifdef D6_UP},variants{$endif}, typinfo;

type
  TMatcher = class(TInterfacedObject, IMatcher)
  private
    FLeft: TBoundTableDescAdapter;
    FRight: TBoundTableDescAdapter;
    FMatchTable: TEvBasicClientDataSet;
    FBindingDesc: TBindingDesc;
    function GetMatch( aBoundTable: TBoundTableDescAdapter; aKeyValues: Variant; aResultFields: string ): Variant;
  {IMatcher}
    function BindingName: string;
    function RightMatch( aLeftKeyValues: Variant ): Variant;
    function LeftMatch( aRightKeyValues: Variant ): Variant;
    function ExtraDataByLeftKey( aLeftKeyValues: Variant; aExtraFieldNames: string ): Variant; //may return VarArray or single Variant
    function ExtraDataByRightKey( aRightKeyValues: Variant; aExtraFieldNames: string ): Variant; //may return VarArray or single Variant
  public
    constructor Create( bd: TBindingDesc; aMatchTable: TCDSBase );
    destructor Destroy; override;
  end;


function VarArrayConcat( va: array of Variant): Variant;
var
  len, i, cur, j: integer;
begin
  len := 0;
  for i := low(va) to high(va) do
    if VarIsArray( va[i] ) then
      inc( len, VarArrayHighBound(va[i], 1) - VarArrayLowBound(va[i], 1) + 1 )
    else
      inc( len );
  Result := VarArrayCreate([0, len-1], varVariant);
  cur := 0;
  for i := low(va) to high(va) do
    if VarIsArray( va[i] ) then
      for j := VarArrayLowBound(va[i], 1) to VarArrayHighBound(va[i], 1) do
      begin
        Result[cur] := va[i][j];
        inc(cur);
      end
    else
    begin
      Result[cur] := va[i];
      inc(cur);
    end;
  Assert( cur = len );
end;


procedure AddCurrentRecord( cds: TDataSet; src: TDataSet );
var
  i: integer;
begin
  cds.Append;
  try
    for i := 0 to src.FieldCount-1 do
    begin
      cds[ src.Fields[i].FieldName ] := src.Fields[i].Value;
    end;
    cds.Post;
  except
    cds.Cancel;
    raise;
  end;
end;

procedure AddField( cds: TDataSet; aSrcField: TField; pfx: string );
var
  aField: TField;
type
  TFieldClass = class of TField;
begin
  if cds.FindField( pfx + aSrcField.FieldName ) = nil then
  begin
    if aSrcField.ClassType = TWideStringField then
      aField := TStringField.Create(cds)
    else
      aField := TFieldClass(aSrcField.ClassType).Create(cds);
    with aField do
    begin
      FieldKind := fkData;
      Visible := aSrcField.Visible;
      Size := aSrcField.Size;
      FieldName := pfx + aSrcField.FieldName;
      DataSet := cds;
      DisplayLabel := aSrcField.DisplayLabel;
      DisplayWidth := aSrcField.DisplayWidth;
    end;
    if aSrcField is TBCDField then
      TBCDField(aField).Precision := TBCDField(aSrcField).Precision;
    if aSrcField.DataType = ftFixedChar then
      TStringField(aField).FixedChar := True;
  end;
end;


{ TBoundTable }

constructor TBoundTable.Create(aCdsClass: TCDSBaseClass;
  aSrc: TDataSet; btd: TBoundTableDesc; aPfx: string);

  procedure InitFields;
  var
    i: integer;
  begin
    with TBooleanField.Create( FDataSet ) do
    begin
      FieldKind := fkData;
      FieldName := sMatchedFieldName;
      DataSet := FDataSet;
      DisplayLabel := 'Matched';
    end;
    for i := 0 to aSrc.FieldCount-1 do
      AddField( FDataSet, aSrc.Fields[i], '' );
  end;

  procedure PopulateRecords;
  var
    bmk: TBookmark;
  begin
    bmk := aSrc.GetBookMark;
    try
      try
        aSrc.DisableControls;
        try
          aSrc.First;
          while not aSrc.Eof do
          begin
            AddCurrentRecord( FDataSet, aSrc );
            aSrc.Next;
          end;
        finally
          aSrc.EnableControls;
        end;
      finally
        aSrc.GotoBookmark( bmk );
      end;
    finally
      aSrc.FreeBookmark( bmk );
    end;
  end;

begin
  inherited Create( btd, aPfx );
  FDataSet := aCdsClass.Create( nil );
  {$ifndef gdycommon}
  {$ifdef D7_UP}
  if FDataSet is TevClientDataSet then
    TevClientDataSet(FDataSet).SkipFieldCheck := True;
  {$endif}
  {$endif}
  InitFields;
  (FDataSet as TevBasicClientDataSet).CreateDataSet;
  (FDataSet as TevBasicClientDataSet).LogChanges := False;
  PopulateRecords;
  if Unique then
  begin
    FDataSet.Filter := Format('(%0:s<>True) or (%0:s is null)',[sMatchedFieldName]);
    FDataSet.Filtered := true;
  end;
end;

destructor TBoundTable.Destroy;
begin
  FreeAndNil( FDataSet );
end;

procedure TBoundTable.SetMatchIndicator( isMatched: boolean );
begin
  FDataSet.Edit;
  try
    FDataSet.FieldByName(sMatchedFieldName).AsBoolean := isMatched;
    FDataSet.Post;
  except
    FDataSet.Cancel;
    raise;
  end;
end;

procedure TBoundTable.UpdateMatchIndicator( aMatch: TDataSet; aKeyValues: Variant );
var
  saveFiltered: boolean;
begin
  FDataSet.DisableControls;
  try
    saveFiltered := FDataSet.Filtered;
    try
      FDataSet.filtered := false;
      InternalUpdateMatchIndicator( aMatch, aKeyValues );
    finally
      FDataSet.filtered := saveFiltered;
    end;
  finally
    FDataSet.EnableControls;
  end;
end;

procedure TBoundTable.InternalUpdateMatchIndicator( aMatch: TDataSet; aKeyValues: Variant );
begin
  if FDataSet.Locate( KeyFields, aKeyValues, []) then
    SetMatchIndicator( not VarIsNull( aMatch.Lookup( PrefixedKeyFields, aKeyValues, PrefixedKeyFields ) ) )
end;

procedure TBoundTable.UpdateMatchIndicators( aMatch: TDataSet );

  procedure DoUpdateMatchIndicators;
  begin
    aMatch.First;
    while not aMatch.Eof do
    begin
      UpdateMatchIndicator( aMatch, aMatch.FieldValues[PrefixedKeyFields] );
      aMatch.Next;
    end;
  end;

var
  bmkMatch: TBookmark;
  saveFiltered: boolean;
begin
  aMatch.DisableControls;
  try
    bmkMatch := aMatch.GetBookmark;
    try
      try
        FDataSet.DisableControls;
        try
          saveFiltered := FDataSet.Filtered;
          try
            FDataSet.filtered := false;
            DoUpdateMatchIndicators;
          finally
            FDataSet.filtered := saveFiltered;
          end;
        finally
          FDataSet.EnableControls;
        end;
      finally
        aMatch.GotoBookmark( bmkMatch )
      end;
    finally
      aMatch.FreeBookmark( bmkMatch );
    end;
  finally
    aMatch.EnableControls;
  end;
end;

function TBoundTable.KeyValues: Variant;
begin
  Result := DataSet.FieldValues[KeyFields];
end;

function TBoundTable.CopiedValues: Variant;
begin
  Result := DataSet.FieldValues[CopiedFields];
end;

{ TBinder }

constructor TBinder.Create( bd: TBindingDesc; aLeftTable, aRightTable: TDataSet; aMatchTableKeeper: IMatchTableKeeper; cdsClass: TCDSBaseClass);
begin
  inherited Create;
  FBindingDesc_ := bd;
  FLeft := TBoundTable.Create( cdsClass, aLeftTable, bd.LeftDesc, sLeftTablePfx );
  FRight := TBoundTable.Create( cdsClass, aRightTable, bd.RightDesc, sRightTablePfx );
  FMatchTableKeeper := aMatchTableKeeper;
  FLeft.UpdateMatchIndicators( MatchTable );
  FRight.UpdateMatchIndicators( MatchTable );
end;

destructor TBinder.Destroy;
begin
  inherited;
  FreeAndNil( FLeft );
  FreeAndNil( FRight );
end;

procedure TBinder.AddMatch( aLeftKeys, aRightKeys: Variant );
var
  MatchKeyFields: string;
begin
  Assert( FLeft.DataSet.RecordCount > 0 );
  Assert( FRight.DataSet.RecordCount > 0 );

  MatchKeyFields := FieldListConcat( [FLeft.PrefixedKeyFields, FRight.PrefixedKeyFields] );

  if VarIsNull( MatchTable.Lookup( MatchKeyFields, VarArrayConcat([aLeftKeys, aRightKeys]), MatchKeyFields ) ) then
    if FLeft.DataSet.Locate( FLeft.KeyFields, aLeftKeys, [] ) and
       FRight.DataSet.Locate( FRight.KeyFields, aRightKeys, [] ) then
    begin
      MatchTable.Append;
      try
        MatchTable.FieldValues[FieldListConcat([FLeft.PrefixedCopiedFields, FRight.PrefixedCopiedFields])] :=
          VarArrayConcat([FLeft.CopiedValues, FRight.CopiedValues]);
        MatchTable.Post;
      except
        MatchTable.Cancel;
        raise;
      end;
      FLeft.SetMatchIndicator( true );
      FRight.SetMatchIndicator( true );
    end
    else
      Assert(false,'addMatch: Cannot locate');
end;

function TBinder.BindingDesc: TBindingDesc;
begin
  Result := FBindingDesc_;
end;

function TBinder.LeftTable: TDataSet;
begin
  Result := FLeft.DataSet;
end;

function TBinder.MatchTable: TDataSet;
begin
  Result := FMatchTableKeeper.MatchTable;
end;

procedure TBinder.RemoveAllMatches;
begin
  MatchTable.DisableControls;
  try
    MatchTable.first;
    while not MatchTable.Eof do
      RemoveMatch;
  finally
    MatchTable.EnableControls;
  end;
end;

procedure TBinder.RemoveMatch;
var
  leftKeyValue, rightKeyValue: Variant;
begin
  Assert( MatchTable.RecordCount > 0 );
  leftKeyValue :=  MatchTable.FieldValues[FLeft.PrefixedKeyFields];
  rightKeyValue := MatchTable.FieldValues[FRight.PrefixedKeyFields];
  MatchTable.Delete;
  FLeft.UpdateMatchIndicator( MatchTable, leftKeyValue );
  FRight.UpdateMatchIndicator( MatchTable, rightKeyValue );
end;

function TBinder.RightTable: TDataSet;
begin
  Result := FRight.DataSet;
end;

function TBinder.LeftKeyValues: Variant;
begin
  Result := FLeft.KeyValues;
end;

function TBinder.RightKeyValues: Variant;
begin
  Result := FRight.KeyValues;
end;

{ TMatchTableKeeper }

constructor TMatchTableKeeper.Create(bd: TBindingDesc; aLeftTable,
  aRightTable: TDataSet; aBindingTableFileName: string;
  cdsClass: TCDSBaseClass);

  function StringToComponent(Value: string): TComponent;
  var
    StrStream:TStringStream;
    BinStream: IisStream;
  begin
    StrStream := TStringStream.Create(Value);
    try
      BinStream := TisStream.Create;
      ObjectTextToBinary(StrStream, BinStream.RealStream);
      BinStream.Position := 0;
      Result := BinStream.RealStream.ReadComponent(nil);
    finally
      StrStream.Free;
    end;
  end;

  procedure AddExtraFields( efs: string );
    procedure AddExtraField( ef: string );
    begin
      (StringToComponent( ef ) as TField).DataSet := FMatchTable;
    end;
  var
    i: integer;
  begin
    with ListAsStr( efs ) do
      for i := 0 to Count-1 do
        AddExtraField( Str[i] );
  end;

  procedure AddFieldsToMatch( aFieldList: string; aSrc: TDataSet; aPfx: string );
  var
    i: integer;
  begin
    with SplitStr( aFieldList, ';') do
      for i := 0 to Count-1 do
        AddField( FMatchTable, aSrc.FieldByName(Str[i]), aPfx );
  end;
var
  bLoaded: boolean;
begin
  FBindingDesc := bd;
  FLeft := TBoundTableDescAdapter.Create( bd.LeftDesc, sLeftTablePfx );
  FRight := TBoundTableDescAdapter.Create( bd.RightDesc, sRightTablePfx );
  FMatchTable := cdsClass.Create( nil );
  bLoaded := false;
  if FileExists( aBindingTableFileName ) then
  try
    FMatchTable.LoadFromFile( aBindingTableFileName );
    FMatchTable.MergeChangeLog;
    bLoaded := true;
  except
    on e: exception do
    begin
      bLoaded := false;
      Application.HandleException( e );
    end
  end;
  if not bLoaded then
  begin
    AddExtraFields( bd.ExtraFields );
    AddFieldsToMatch( FLeft.CopiedFields, aLeftTable, FLeft.Prefix );
    AddFieldsToMatch( FRight.CopiedFields, aRightTable, FRight.Prefix );
    FMatchTable.CreateDataSet;
  end;
end;

destructor TMatchTableKeeper.Destroy;
begin
  inherited;
  FreeAndNil( FMatchTable );
end;

function TMatchTableKeeper.MatchTable: TDataSet;
begin
  Result := FMatchTable;
end;

procedure TMatchTableKeeper.Save( aFileName: string );
begin
  FMatchTable.SaveToFile( aFileName );
  FMatchTable.MergeChangeLog;
end;

function TMatchTableKeeper.IsChanged: boolean;
begin
  Result := FMatchTable.ChangeCount > 0;
end;

function TMatchTableKeeper.CreateMatcher: IMatcher;
begin
  Result := TMatcher.Create( FBindingDesc, FMatchTable ) as IMatcher;
end;

{TMatcher}

function TMatcher.LeftMatch(aRightKeyValues: Variant): Variant;
begin
  Result := GetMatch( FRight, aRightKeyValues, FLeft.PrefixedKeyFields );
end;

function TMatcher.RightMatch(aLeftKeyValues: Variant): Variant;
begin
  Result := GetMatch( FLeft, aLeftKeyValues, FRight.PrefixedKeyFields );
end;

function TMatcher.BindingName: string;
begin
  Result := FBindingDesc.Name;
end;

function TMatcher.GetMatch(aBoundTable: TBoundTableDescAdapter; aKeyValues: Variant; aResultFields: string): Variant;

  function RecordEqual: Boolean;
  var
    s, sl: string;
    i: Integer;
  begin
    if not VarIsArray(aKeyValues) then
      Result := FMatchTable[aBoundTable.PrefixedKeyFields] = aKeyValues
    else
    begin
      Result := True;
      sl := aBoundTable.PrefixedKeyFields;
      for i := 0 to VarArrayHighBound(aKeyValues, 1) do
      begin
        s := Trim(GetNextStrValue(sl, ';'));
        if FMatchTable[s] <> aKeyValues[i] then
        begin
          Result := False;
          Break;
        end;
      end;
    end;
  end;
begin
  if aBoundTable.Unique then
    Result := FMatchTable.Lookup( aBoundTable.PrefixedKeyFields, aKeyValues, aResultFields )
  else
  begin
    Result := Null;
    FMatchTable.First;
    while not FMatchTable.Eof do
    begin
      if RecordEqual then
        if VarIsNull(Result) then
          Result := FMatchTable[aResultFields]
        else
        begin
          if not VarIsArray(Result) then
            Result := VarArrayOf([Result]);
          VarArrayRedim(Result, Succ(VarArrayHighBound(Result, 1)));
          Result[VarArrayHighBound(Result, 1)] := FMatchTable[aResultFields];
        end;
      FMatchTable.Next;
    end;
  end;
end;

function CopyCDS(const ds: TCDSBase ): TCDSBase;
var
  p: TisProvider;
begin
  Result := (TCDSBaseClass(ds.ClassType)).Create(nil);
  p := TisProvider.Create(nil);
  try
    ds.First;
    p.Dataset := ds;
    Result.Data := p.Data;
  finally
    p.Dataset := nil;
    p.Free;
  end;
end;

constructor TMatcher.Create(bd: TBindingDesc; aMatchTable: TCDSBase );
begin
  FBindingDesc := bd;
  FLeft := TBoundTableDescAdapter.Create( bd.LeftDesc, sLeftTablePfx );
  FRight := TBoundTableDescAdapter.Create( bd.RightDesc, sRightTablePfx );
  FMatchTable := CopyCDS(aMatchTable);
end;

destructor TMatcher.Destroy;
begin
  inherited;
  FreeAndNil( FMatchTable );
end;

function TMatcher.ExtraDataByLeftKey(aLeftKeyValues: Variant;
  aExtraFieldNames: string): Variant;
begin
  Result := GetMatch( FLeft, aLeftKeyValues, aExtraFieldNames );
end;

function TMatcher.ExtraDataByRightKey(aRightKeyValues: Variant;
  aExtraFieldNames: string): Variant;
begin
  Result := GetMatch( FRight, aRightKeyValues, aExtraFieldNames );
end;

end.
