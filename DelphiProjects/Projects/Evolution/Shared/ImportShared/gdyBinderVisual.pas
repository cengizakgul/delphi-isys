// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{$include gdy.inc}
unit gdyBinderVisual;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, gdyBinder, db, 
  ExtCtrls,  ActnList, StdCtrls, Buttons, ImgList, Grids, EvMainboard,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, DBCtrls, gdyclasses, ISBasicClasses, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type

  TBinderFrame = class(TFrame, IVisualBinder)
    pnltop: TevPanel;
    pnlTopLeft: TevPanel;
    pnlTopRight: TevPanel;
    evSplitter1: TevSplitter;
    evActionList1: TevActionList;
    AddMatch: TAction;
    RemoveMatch: TAction;
    RemoveAllMatches: TAction;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    dgLeft: TevDBGrid;
    dgRight: TevDBGrid;
    evSplitter2: TevSplitter;
    evPanel1: TevPanel;
    pnlbottom: TevPanel;
    evPanel5: TevPanel;
    dgBottom: TevDBGrid;
    pnlMiddle: TevPanel;
    pnlControl: TevPanel;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evBitBtn3: TevBitBtn;
    lblLeft: TevLabel;
    lblRight: TevLabel;
    lblBottom: TevLabel;
    dsLeft: TevDataSource;
    dsRight: TevDataSource;
    dsBottom: TevDataSource;
    evLabel1: TevLabel;
    procedure pnlMiddleResize(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure AddMatchUpdate(Sender: TObject);
    procedure AddMatchExecute(Sender: TObject);
    procedure RemoveMatchUpdate(Sender: TObject);
    procedure RemoveMatchExecute(Sender: TObject);
    procedure dsLeftDataChange(Sender: TObject; Field: TField);
    procedure RemoveAllMatchesExecute(Sender: TObject);
    procedure dsBottomDataChange(Sender: TObject; Field: TField);
    procedure dgBottomDblClick(Sender: TObject);
    procedure dgLeftRowChanged(Sender: TObject);
    procedure dgLeftMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FBinder: IBinder;
    FAutoMapper: IAutoMapper;
    procedure AutoMap;
//    function HandleBottomSelected(ds: TEvClientDataSet): boolean;
  protected
    procedure ActivateBinder( aBinder: IBinder; aAutoMapper: IAutoMapper );
    function GetBinder: IBinder;
  public
    { Public declarations }
  end;

implementation

uses
  evutils, typinfo, genericImport{$ifdef D6_UP},variants{$endif};
{$R *.DFM}

type
  TKeyPairListBuilder = class(TKeyPairList)
  private
    FCurLeft: Variant;
    FRightGrid: TevDBGrid;
    FBinder: IBinder;
  public
    constructor Create( aBinder: IBinder; aLeftGrid, aRightGrid: TevDbGrid );
    function HandleLeftSelected( ds: TEvClientDataSet ): boolean;
    function HandleRightSelected( ds: TEvClientDataSet ): boolean;
  end;

{ TKeyPairListBuilder }

constructor TKeyPairListBuilder.Create( aBinder: IBinder; aLeftGrid, aRightGrid: TevDbGrid);
begin
  FBinder := aBinder;
  FRightGrid := aRightGrid;
  ForSelected( aLeftGrid, HandleLeftSelected, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty] )
end;

function TKeyPairListBuilder.HandleLeftSelected(
  ds: TEvClientDataSet): boolean;
begin
  FCurLeft := FBinder.LeftKeyValues;
  Result := ForSelected( FRightGrid, HandleRightSelected, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty] )
end;

function TKeyPairListBuilder.HandleRightSelected(
  ds: TEvClientDataSet): boolean;
begin
  AddPair( FCurLeft, FBinder.RightKeyValues );
  Result := true;
end;


{ TBinderFrame }

procedure TBinderFrame.pnlMiddleResize(Sender: TObject);
begin
//
//  pnlControl.Left := (Width - pnlControl.Width) div 2;
end;

procedure TBinderFrame.FrameResize(Sender: TObject);
begin
  pnlTopLeft.Width := Width div 2;
end;

function AddSelected( selected: TStrings; lst: string; ds: TDataSet; pfx: string ): string;
var
  i: integer;
  s: string;
  w: integer;
begin
  with SplitStr( lst, ';') do
    for i := 0 to Count-1 do
    begin
      w := ds.FieldByName(Str[i]).DisplayWidth;
      if w > 30 then
        w := 30;
      s := pfx + Str[i] + #9 + inttostr(w) + #9 + ds.FieldByName(Str[i]).DisplayLabel + #9 + 'F';
      if selected.IndexOf( s ) = -1 then
        selected.Append( s );
    end
end;

function GetHostFrameClassName( control: TComponent; hostBaseClass: string ): string;
  function InheritsFrom(cl: TClass): boolean;
  begin
    Result := assigned(cl) and ( SameText( cl.CLassName, hostBaseClass ) or InheritsFrom( cl.ClassParent ) );
  end;
begin
  while (control <> nil) and not InheritsFrom(control.ClassType) do
    control := control.Owner as TComponent;
  if assigned(control) then
    Result := control.ClassName
  else
    Result := '';
end;

procedure SetIniAttributes( aGrid: TevDBGrid; aBindingName: string );
begin
  try
    aGrid.SaveToIniFile;
    if aBindingName <> '' then
      with aGrid.IniAttributes do
      begin
        Enabled := true;
        FileName := mb_AppSettings.TopNodeName + '\' + EVOLUTION_IMPORT_SETTINGS + GetHostFrameClassName(aGrid, 'TFrameEntry') + '\Mapping\'+aBindingName+'\';
        Delimiter := ';;';
        SaveToRegistry := true;
//        SectionName := '';
//        if aGrid.Owner <> nil then
//          SectionName := aGrid.Owner.Name + '.';
        SectionName := aGrid.Name;
      end
    else
      with aGrid.IniAttributes do
        Enabled := false;
    aGrid.LoadFromIniFile;
  except
    on e: Exception do Application.HandleException( e );
  end
end;

procedure TBinderFrame.ActivateBinder( aBinder: IBinder; aAutoMapper: IAutoMapper );
var
  i: integer;
begin
  lblLeft.Caption := '';
  lblRight.Caption := '';
  lblBottom.Caption := '';
  dsLeft.DataSet := nil;
  dsRight.DataSet := nil;
  dsBottom.DataSet := nil;
  SetIniAttributes( dgLeft, '' );
  SetIniAttributes( dgRight, '' );
  SetIniAttributes( dgBottom, '' );

  FBinder := aBinder;
  FAutoMapper := aAutoMapper;
  if FBinder <> nil then
  begin
    lblLeft.Caption := FBinder.BindingDesc.LeftDesc.Caption;
    lblRight.Caption := FBinder.BindingDesc.rightDesc.Caption;
    lblBottom.Caption := FBinder.BindingDesc.Caption;

    dgLeft.Selected.Clear;
    AddSelected( dgLeft.Selected, FBinder.BindingDesc.LeftDesc.ListFields, FBinder.leftTable, '' );
    dgLeft.ApplySelected;
    if FBinder.BindingDesc.RightDesc.Unique then
      dgLeft.Options := dgLeft.Options - [wwdbigrd.dgMultiSelect]
    else
      dgLeft.Options := dgLeft.Options + [wwdbigrd.dgMultiSelect];

    dgRight.Selected.Clear;
    AddSelected( dgRight.Selected, FBinder.BindingDesc.RightDesc.ListFields, FBinder.rightTable, '' );
    dgRight.ApplySelected;
    if FBinder.BindingDesc.LeftDesc.Unique then
      dgRight.Options := dgRight.Options - [wwdbigrd.dgMultiSelect]
    else
      dgRight.Options := dgRight.Options + [wwdbigrd.dgMultiSelect];

    dgBottom.Selected.Clear;
    for i := 0 to FBinder.MatchTable.FieldCount-1 do
      if (Pos( sLeftTablePfx, FBinder.MatchTable.Fields[i].FieldName) <> 1) and
         (Pos( sRightTablePfx, FBinder.MatchTable.Fields[i].FieldName) <> 1) then
         AddSelected( dgBottom.Selected, FBinder.MatchTable.Fields[i].FieldName, FBinder.MatchTable, '' );

    AddSelected( dgBottom.Selected, FBinder.BindingDesc.LeftDesc.ListFields, FBinder.leftTable, sLeftTablePfx );
    AddSelected( dgBottom.Selected, FBinder.BindingDesc.LeftDesc.KeyFields, FBinder.leftTable, sLeftTablePfx );//
    AddSelected( dgBottom.Selected, FBinder.BindingDesc.RightDesc.ListFields, FBinder.rightTable, sRightTablePfx );
    AddSelected( dgBottom.Selected, FBinder.BindingDesc.RightDesc.KeyFields, FBinder.rightTable, sRightTablePfx ); //
    if FBinder.BindingDesc.IndicatorField <> '' then
      dgBottom.SetControlType( FBinder.BindingDesc.IndicatorField, fctCheckBox, 'Y;N' );
    dgBottom.ApplySelected;


    dsLeft.DataSet := FBinder.leftTable;
    dsRight.DataSet := FBinder.rightTable;
    dsBottom.DataSet := FBinder.MatchTable;

    SetIniAttributes( dgLeft, FBinder.BindingDesc.Name );
    SetIniAttributes( dgRight, FBinder.BindingDesc.Name );
    SetIniAttributes( dgBottom, FBinder.BindingDesc.Name );
  end;
end;

function HasSelection( dg: TevDBGrid ): boolean;
begin
  Result := assigned(dg) and assigned(dg.DataSource) and assigned(dg.DataSource.DataSet) and dg.DataSource.DataSet.Active and (dg.DataSource.DataSet.RecordCount > 0);
end;

procedure TBinderFrame.AddMatchUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FBinder) and HasSelection(dgLeft) and HasSelection(dgRight);
end;

procedure TBinderFrame.AddMatchExecute(Sender: TObject);
var
  i: integer;
begin
  with TKeyPairListBuilder.Create( FBinder, dgLeft, dgRight) do
  try
    dgLeft.UnselectAll;
    dgRight.UnselectAll;
    for i := 0 to Count-1 do
      FBinder.AddMatch( LeftKey[i], RightKey[i] );
  finally
    Free;
  end;
end;

procedure TBinderFrame.RemoveMatchUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FBinder) and HasSelection(dgBottom);
end;

procedure TBinderFrame.RemoveMatchExecute(Sender: TObject);
begin
  FBinder.RemoveMatch;
//  ForSelected( dgBottom, HandleBottomSelected, [fsDisableControls, fsForCurrentIfListIsEmpty] )
end;

{
function TBinderFrame.HandleBottomSelected(ds: TEvClientDataSet): boolean;
begin
  Result := true;
  FBinder.RemoveMatch;
end;
}
procedure TBinderFrame.dsLeftDataChange(Sender: TObject; Field: TField);
begin
//ODS( GetEnumName( TypeInfo(boolean), ord(dgLeft.DataSource.DataSet.filtered))+' '+dgLeft.DataSource.DataSet.Filter);
end;

procedure TBinderFrame.RemoveAllMatchesExecute(Sender: TObject);
begin
  if EvMessage( 'Remove all matches?', mtConfirmation, [mbYes, mbNo], mbNo ) = mrYes then
    FBinder.RemoveAllMatches;
end;

function TBinderFrame.GetBinder: IBinder;
begin
  Result := FBinder;
end;

procedure TBinderFrame.dsBottomDataChange(Sender: TObject; Field: TField);
begin
//  with (dsBottom.DataSet as TClientDataSet) do
//    evlabel1.Caption := inttostr(ChangeCount) + ' ' + GetEnumName( typeinfo(TUpdateStatus), ord(UpdateStatus) );
end;

procedure TBinderFrame.dgBottomDblClick(Sender: TObject);
begin
  if (FBinder.BindingDesc.IndicatorField <> '') and not FBinder.MatchTable.IsEmpty  then
  with FBinder.MatchTable do
  begin
    Edit;
    try
      if FieldByName(FBinder.BindingDesc.IndicatorField).AsString = 'Y' then
        FieldByName(FBinder.BindingDesc.IndicatorField).AsString := 'N'
      else
        FieldByName(FBinder.BindingDesc.IndicatorField).AsString := 'Y';
      Post;
    except
      Cancel;
      raise;
    end;
  end;
end;

function ActiveDS( dg: TevDBGrid ): boolean;
begin
  Result :=  assigned(dg) and assigned(dg.DataSource) and assigned(dg.DataSource.DataSet) and dg.DataSource.DataSet.Active;
end;

procedure TBinderFrame.dgLeftRowChanged(Sender: TObject);
begin
  AutoMap;
end;

procedure TBinderFrame.dgLeftMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  gc: TGridCoord;
begin
  gc := dgLeft.MouseCoord( X, Y );
  if (gc.X <> 0) and (gc.Y <> 0) then
    AutoMap;
end;

procedure TBinderFrame.AutoMap;
var
  rightKeys: Variant;
begin
  if assigned(FBinder) and ActiveDS(dgLeft) and ActiveDS(dgRight) then
  begin
    rightKeys := FAutoMapper.MapToRightKeys( dgLeft.DataSource.DataSet, FBinder.BindingDesc, dgRight.DataSource.DataSet );
    if not VarIsNull(rightKeys) then
    begin
      dgRight.DataSource.DataSet.Locate( FBinder.BindingDesc.RightDesc.KeyFields, rightKeys, []);
    end;
  end;
end;

end.


