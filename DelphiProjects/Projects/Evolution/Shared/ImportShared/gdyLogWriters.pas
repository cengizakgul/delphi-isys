// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdyLogWriters;

interface

uses
  classes, gdyLoggerImpl;

type
  TLoggerEventSink = class ( TInterfacedObject )
  protected
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  end;

  ILogOutput = interface
['{4D78D2D1-60F9-4819-B939-F8B72275FBE7}']
    procedure Put( s: string );
  end;

  TFileLogOutput = class ( TInterfacedObject, ILogOutput)
  private
    FLog: Text;
    FFilename: string;
    {ILogOutput}
    procedure Put( s: string );
  public
    constructor Create( filename: string );
    destructor Destroy; override;
    function Filename: string;
  end;


  TStringsLogOutput = class ( TInterfacedObject, ILogOutput)
  private
    FStrings: TStrings;
    {ILogOutput}
    procedure Put( s: string );
  public
    constructor Create( aStrings: TStrings );
  end;

  TPlainTextLogWriterBase = class ( TLoggerEventSink )
  protected
    FOut: ILogOutput;
  public
    constructor Create( aOut: ILogOutput );
  end;

  TPlainTextDevLogWriter = class ( TPlainTextLogWriterBase, ILoggerEventSink )
  private
    procedure put( aLogState: TLogState; s: string );
    {ILoggerEventSink}
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  public
  end;

  TPlainTextUserLogWriter = class ( TPlainTextLogWriterBase, ILoggerEventSink )
  private
    {ILoggerEventSink}
    procedure MessageLogged( aLogState: TLogState );
  public
  end;

function FormatCtxVal( s: string ): string;

implementation

uses
  typinfo, sysutils, gdyLogger;

function FormatCtxVal( s: string ): string;
begin
  if Pos( #13, s ) > 0 then
    Result := #13#10 + Indent( s, 1 )
  else
    Result := s;
end;

function GenUserLogEntry( ls: TLogState ): string;
  function DumpLogBlock( lb: TLogBlock ): string;
  var
    i: integer;
  begin
    Result := '';
    if lb.Context.Count > 0 then
    begin
      Result := '';//lb.BlockName + #13#10;
      for i := 0 to lb.Context.Count-1 do
        Result := Result + lb.Context[i].ContextTag + ': ' + FormatCtxVal(lb.Context[i].ContextValue) + #13#10;
    end;
  end;
var
  i: integer;
begin
  with ls.Last.Messages.Last do
    Result := KindAsStr + ': ' + Text + #13#10 + Details + #13#10 + 'Context:' + #13#10;
  for i := 0 to ls.BlockStack.Count-1 do
    Result := Result + DumpLogBlock( ls.BlockStack[i] );
end;

{ TWriterBase }

procedure TLoggerEventSink.BlockEntered(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.BlockLeaving(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.ContextItemLogged(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.MessageLogged(aLogState: TLogState);
begin
//do nothing
end;

{ TFileLogOutput }

constructor TFileLogOutput.Create(filename: string);
begin
  FFilename := ChangeFileExt( filename, '.txt' );
  try
    system.assignfile( FLog, FFilename );
    if FileExists(FFilename) then
      system.Append( FLog )
    else
      system.Rewrite( FLog );
  except
    //do nothing
  end;
end;

destructor TFileLogOutput.Destroy;
begin
  inherited;
  try
    system.Flush( FLog );
    system.CloseFile( FLog );
  except
    //do nothing
  end;
end;

function TFileLogOutput.Filename: string;
begin
  Result := FFilename;
end;

procedure TFileLogOutput.put(s: string);
begin
  try
    system.Writeln( FLog, s );
  except
    //do nothing
  end;
end;

{ TPlainTextDevLogWriter }

procedure TPlainTextDevLogWriter.BlockEntered(aLogState: TLogState);
begin
  put( aLogState, 'enter ' + aLogState.Last.BlockName );
end;

procedure TPlainTextDevLogWriter.BlockLeaving(aLogState: TLogState);
begin
  put( aLogState, 'leave ' + aLogState.Last.BlockName );
end;

procedure TPlainTextDevLogWriter.ContextItemLogged(aLogState: TLogState);
begin
  put( aLogState, aLogState.Last.Context.Last.RenderAsString  );
end;

procedure TPlainTextDevLogWriter.MessageLogged(aLogState: TLogState);
begin
  put( aLogState, aLogState.Last.Messages.Last.RenderAsString);
end;

procedure TPlainTextDevLogWriter.put(aLogState: TLogState; s: string);
begin
  FOut.Put( Indent( s, aLogState.BlockStack.Count-1 ) );
end;

{ TPlainTextUserLogWriter }

procedure TPlainTextUserLogWriter.MessageLogged(aLogState: TLogState);
begin
  if not (aLogState.Last.Messages.Last.Kind in [lmDebug]) then
    FOut.Put( GenUserLogEntry( aLogState ) );
end;

{ TPlainTextLogWriterBase }

constructor TPlainTextLogWriterBase.Create(aOut: ILogOutput);
begin
  FOut := aOut;
end;

{ TStringsLogOutput }

constructor TStringsLogOutput.Create(aStrings: TStrings);
begin
  FStrings := aStrings;
end;

procedure TStringsLogOutput.Put(s: string);
begin
  FStrings.Add( s );
end;



end.
