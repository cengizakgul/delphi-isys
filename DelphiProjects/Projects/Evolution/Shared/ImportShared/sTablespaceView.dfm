object TableSpaceViewFrame: TTableSpaceViewFrame
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 320
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 8
      Top = 16
      Width = 27
      Height = 13
      Caption = 'Table'
    end
    object cbTables: TevComboBox
      Left = 48
      Top = 8
      Width = 185
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      OnChange = cbTablesChange
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 41
    Width = 320
    Height = 199
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dgView: TevDBGrid
      Left = 0
      Top = 0
      Width = 320
      Height = 199
      DisableThemesInTitle = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TTableSpaceViewFrame\dgView'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsView
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object dsView: TevDataSource
    Left = 120
    Top = 113
  end
end
