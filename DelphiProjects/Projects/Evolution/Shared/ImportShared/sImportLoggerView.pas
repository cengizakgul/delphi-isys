// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportLoggerView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyLoggerImpl, StdCtrls,  ExtCtrls, genericImport,
  ComCtrls, gdyLogWriters, sImportLoggerRichView, gdyLogger, ISBasicClasses, EvUIComponents;

type
  TImportLoggerKeeper = class( TInterfacedObject, IImportLogger, IImportLoggerKeeper )
  private
    FLogger: IExceptionLogger;
    FStatefulLogger: IStatefulLogger;

    FDevLogWriter: ILoggerEventSink;
    FUserLogWriter: ILoggerEventSink;
    FUserLogFileName: string;

    {IImportLogger}
    procedure LogEntry( blockname: string );
    procedure LogExit;
    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string );
    procedure LogWarning( s: string );
    procedure LogWarningFmt( s: string; const args: array of const );
    procedure LogEventFmt( s: string; const args: array of const );
    procedure LogError( s: string );
    procedure LogErrorFmt( s: string; const Args: array of const );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopMostExceptionsAndWarnFmt( s: string; const args: array of const ); //EAbortTableImport is passed further

    {IImportLoggerKeeper}
    procedure StartDevLoggingToFile( filename: string );
    procedure StopDevLoggingToFile;
    procedure StartLoggingToFile( filename: string );
    procedure StopLoggingToFile;
    function Logger: IImportLogger;
    function LogFileName: string;
//  protected
//    property Logger: IStatefulLogger read FLogger implements IImportLogger; //just because IImportLogger = ILogger for now; need manual delegation otherwise
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TImportLoggerViewFrame = class(TFrame )
    pnlLog: TevPanel;
    pgLogView: TevPageControl;
    tbshUserView: TTabSheet;
    tbshDevView: TTabSheet;
    pnlDevView: TevPanel;
    mmDevView: TevMemo;
    tbshUserTextView: TTabSheet;
    pnlUserTextView: TevPanel;
    mmUserTextView: TevMemo;
    LoggerRichView: TImportLoggerRichViewFrame;
  private
    FLoggerInstance: TImportLoggerKeeper;
    FLoggerKeeper: IImportLoggerKeeper;
//    FDevLogWriter: ILoggerEventSink;
//    FUserLogWriter: ILoggerEventSink;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    property LoggerKeeper: IImportLoggerKeeper read FLoggerKeeper;
  end;

implementation

uses
  typinfo, gdycommon;
{$R *.dfm}

{ TImportLogger }

constructor TImportLoggerKeeper.Create;
begin
  FStatefulLogger := CreateStatefulLogger;
  FLogger := CreateExceptionLogger( FStatefulLogger as ILogger );
end;

destructor TImportLoggerKeeper.Destroy;
begin
  LogDebug( 'Stopping to write developer log to file' );
  FStatefulLogger.UnAdvise( FDevLogWriter );
  FDevLogWriter := nil;
  LogDebug( 'Stopped to write developer log to file' );
  StopLoggingToFile;
  inherited;
end;

procedure TImportLoggerKeeper.PassthroughException;
begin
  FLogger.PassthroughException;
end;

procedure TImportLoggerKeeper.StopException;
begin
  FLogger.StopException;
end;

procedure TImportLoggerKeeper.PassthroughExceptionAndWarnFmt(s: string; const args: array of const);
begin
  FLogger.PassthroughExceptionAndLogMessage( lmWarning, Format( s, args), '' ); //look at LogWarning impl
end;

procedure TImportLoggerKeeper.StopExceptionAndWarnFmt(s: string; const args: array of const);
begin
  FLogger.StopExceptionAndLogMessage( lmWarning, Format( s, args), '' ); //look at LogWarning impl
end;

procedure TImportLoggerKeeper.StopMostExceptionsAndWarnFmt(s: string; const args: array of const);
begin
  if ExceptObject is EAbortTableImport then
    PassthroughExceptionAndWarnFmt( s, args )
  else
    StopExceptionAndWarnFmt( s, args );
end;

procedure TImportLoggerKeeper.LogContextItem(tag, value: string);
begin
  FLogger.LogContextItem( tag, value );
end;

procedure TImportLoggerKeeper.LogDebug(s, fulltext: string);
begin
  FLogger.LogMessage( lmDebug, s, fulltext );
end;

procedure TImportLoggerKeeper.LogEntry(blockname: string);
begin
  FLogger.LogEntry( blockname );
end;

procedure TImportLoggerKeeper.LogError(s: string);
begin
  FLogger.LogMessage( lmError, s, '' );
end;

procedure TImportLoggerKeeper.LogErrorFmt(s: string; const Args: array of const);
begin
  LogError( Format( s, args ) );
end;


procedure TImportLoggerKeeper.LogEvent(s: string);
begin
  FLogger.LogMessage( lmEvent, s, '' );
end;

procedure TImportLoggerKeeper.LogEventFmt(s: string;
  const args: array of const);
begin
  LogEvent( Format( s, args ) );
end;

procedure TImportLoggerKeeper.LogExit;
begin
  FLogger.LogExit;
end;

function TImportLoggerKeeper.LogFileName: string;
begin
  Result := FUserLogFileName;
end;

function TImportLoggerKeeper.Logger: IImportLogger;
begin
  Result := Self;
end;

procedure TImportLoggerKeeper.LogWarning(s: string);
begin
  FLogger.LogMessage( lmWarning, s, '' );
end;

procedure TImportLoggerKeeper.LogWarningFmt(s: string; const args: array of const);
begin
  LogWarning( Format( s, args ) );
end;

procedure TImportLoggerKeeper.StartDevLoggingToFile(filename: string);
begin
  LogDebug( 'Starting to write developer''s log to file <' + filename + '>' );
  FStatefulLogger.UnAdvise( FDevLogWriter );
  FDevLogWriter := TPlainTextDevLogWriter.Create( TFileLogOutput.Create(filename) );
  FStatefulLogger.Advise( FDevLogWriter );
  LogDebug( 'Started to write developer''s log to file <' + filename + '>' );
end;

procedure TImportLoggerKeeper.StopDevLoggingToFile;
begin
  LogDebug( 'Stopping to write developer''s log to file' );
  FStatefulLogger.UnAdvise( FDevLogWriter );
  FDevLogWriter := nil;
  LogDebug( 'Stopped to write developer''s log to file' );
end;

procedure TImportLoggerKeeper.StartLoggingToFile(filename: string);
var
  flg: TFileLogOutput;
begin
  LogDebug( 'Starting to write user log to file <' + filename + '>' );
  FUserLogFileName := '';
  FStatefulLogger.UnAdvise( FUserLogWriter );
  flg := TFileLogOutput.Create(filename);
  FUserLogFileName := flg.Filename;
  FUserLogWriter := TPlainTextUserLogWriter.Create( flg );
  FStatefulLogger.Advise( FUserLogWriter );
  LogDebug( 'Started to write user log to file <' + filename + '>' );
end;

procedure TImportLoggerKeeper.StopLoggingToFile;
begin
  LogDebug( 'Stopping to write user log to file' );
  FUserLogFileName := '';
  FStatefulLogger.UnAdvise( FUserLogWriter );
  FUserLogWriter := nil;
  LogDebug( 'Stopped to write user log to file' );
end;

{ TImportLoggerViewFrame }

constructor TImportLoggerViewFrame.Create(Owner: TComponent);
begin
  FLoggerInstance := TImportLoggerKeeper.Create;
  FLoggerKeeper := FLoggerInstance;
  inherited;
  FLoggerInstance.FStatefulLogger.Advise( LoggerRichView as ILoggerEventSink );
//  FDevLogWriter := TPlainTextDevLogWriter.Create( TStringsLogOutput.Create( mmDevView.Lines ) );
//  FLoggerInstance.FLogger.Advise( FDevLogWriter );
//  FUserLogWriter := TPlainTextUserLogWriter.Create( TStringsLogOutput.Create( mmUserTextView.Lines ) );
//  FLoggerInstance.FLogger.Advise( FUserLogWriter );
end;

destructor TImportLoggerViewFrame.Destroy;
  procedure Finish;
  begin
    FLoggerInstance.FStatefulLogger.UnAdvise( LoggerRichView as ILoggerEventSink );
  end;
begin
  Finish;
//  FLoggerInstance.FLogger.UnAdvise( FDevLogWriter );
//  FLoggerInstance.FLogger.UnAdvise( FUserLogWriter );
  FLoggerKeeper := nil;
  inherited;
end;


end.
