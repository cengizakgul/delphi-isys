// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportLoggerRichView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, gdyLoggerImpl, StdCtrls,  ComCtrls, ExtCtrls,
  ImgList, ISBasicClasses, EvUIComponents;

type
  TImportLoggerRichViewFrame = class(TFrame, ILoggerEventSink)
    pnlUserView: TevPanel;
    evSplitter1: TevSplitter;
    pnlTopPart: TevPanel;
    lvMessages: TevListView;
    pnlBottom: TevPanel;
    evSplitter2: TevSplitter;
    pnlLeft: TevPanel;
    mmDetails: TevMemo;
    evPanel1: TevPanel;
    pnlRight: TevPanel;
    evPanel2: TevPanel;
    mmContext: TevMemo;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evImageList1: TevImageList;
    procedure lvMessagesChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
  private
    {ILoggerEventSink}
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  public
//    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
  end;

implementation

uses
  gdyLogWriters, gdyLogger;

{$R *.dfm}

{ TImportLoggerRichViewFrame }

procedure TImportLoggerRichViewFrame.BlockEntered(aLogState: TLogState);
begin
end;

procedure TImportLoggerRichViewFrame.BlockLeaving(aLogState: TLogState);
begin
end;

procedure TImportLoggerRichViewFrame.ContextItemLogged(aLogState: TLogState);
begin
end;

procedure TImportLoggerRichViewFrame.MessageLogged(aLogState: TLogState);
begin
  if not (aLogState.Last.Messages.Last.Kind in [lmDebug]) then
  begin
    with lvMessages.Items.Add do
    begin
      Caption := aLogState.Last.Messages.Last.Text;
      Data := aLogState.Clone;
      case aLogState.Last.Messages.Last.Kind of
        lmEvent: ImageIndex := 2;
        lmWarning: ImageIndex := 1;
        lmError, lmException: ImageIndex := 0;
      else
        ImageIndex := -1;
      end;
    end;
  end;

  Update;
end;

procedure TImportLoggerRichViewFrame.lvMessagesChange(Sender: TObject; Item: TListItem; Change: TItemChange);
var
  i: integer;
  j: integer;
  ls: TLogState;
begin
  if Change = ctstate then
  begin
    mmDetails.Lines.Clear;
    mmContext.Lines.Clear;
    if lvMessages.Selected <> nil then
    begin
      ls := TLogState(lvMessages.Selected.Data);
      if assigned(ls) then
      begin
        mmDetails.Lines.Add( ls.Last.Messages.Last.Text );
        mmDetails.Lines.Add( ls.Last.Messages.Last.Details );
        for i := 0 to ls.BlockStack.Count-1 do
          for j := 0 to ls.BlockStack[i].Context.Count-1 do
            mmContext.Lines.Add( ls.BlockStack[i].Context[j].ContextTag + ': ' + FormatCtxVal(ls.BlockStack[i].Context[j].ContextValue) );
        mmDetails.ScrollBy(-100000,-100000)
{        mmDetails.SelStart := 0;
        mmContext.SelStart := 0;}
//        mmDetails.Lines.Add( 'Dump:' );
//        mmDetails.Lines.Add( ls.Dump );
      end;
    end;
  end;
end;

destructor TImportLoggerRichViewFrame.Destroy;
var
  i: integer;
begin
  for i := 0 to lvMessages.Items.Count-1 do
  begin
    TLogState(lvMessages.Items[i].Data).Free;
    lvMessages.Items[i].Data := nil;
  end;
  inherited;
end;

end.
