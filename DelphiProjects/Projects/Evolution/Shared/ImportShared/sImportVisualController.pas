// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportVisualController;
//todo:  rename to mediator
interface

uses
  genericImport, gdyImportUIHelpers, gdyBinder, gdyBinderHelpers, classes, EvMainboard, EvClientDataSet;

const
  sEETermedParam = 'EETermed';

type
  IImportVisualController = interface;

  IParamInputFrame = interface
['{00A846F5-53CE-4103-894A-C6BFA9DD5368}']
    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

  IConnectionStringInputFrame = interface
['{3ADD931F-9014-439C-881B-498FF0F363C0}']
    procedure ConnectionStringChanged;   //clumsy; explicit connected and disconnected events are better
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
  end;

  IImportQueueFrame = interface
['{A55D321E-D50D-4AB5-A5C4-FD2E583D4A50}']
    procedure QueueChanged;
    procedure QueueItemStatusChanged;
    procedure SetController( aController: IImportVisualController );
  end;

  ISetupNameListener = interface
['{3ADD931F-9014-439C-881B-498FF0F363C0}']
    procedure SetupNameChanged;
  end;

  IConnectionStatusListener = interface
['{65DCC0D3-4FB3-425B-B8C4-77B6A3E6BAEF}']
    procedure ConnectionChanged( DataModule: IExternalDataModule );
  end;

  ILogFolderListener = interface
['{84B72010-4B09-40D0-A1D3-D225EBF8E792}']
    procedure LogFolderChanged;
    procedure DevLogEnabledChanged;
  end;

  IImportVisualController = interface
['{367F1208-CB72-4E9A-8AA1-7E88D0E3961D}']
    procedure RunImport;
    procedure EnqueueImport;
    procedure RunQueuedImports;
    function CanRunQueuedImports: boolean;
    procedure SetConnectionString( aConnectionString: string );
    procedure Disconnect;
    function CanRunImport: boolean;
    function DataWasImported: boolean;
    procedure AdviseSetupNameListener( aSetupNameListener: ISetupNameListener ); //one slot; set to nil to unadvise
    procedure AdviseConnectionStatusListener( aConnectionStatusListener: IConnectionStatusListener ); //one slot; set to nil to unadvise
    function SetupName: string;
    procedure WriteSetup( aSetupName: string );
    procedure OpenSetup( aSetupName: string );
    procedure NewSetup;
    function SetupIsChanged: boolean;
    procedure LoadDefaults;
    function ConnectionString: string;
    function BindingKeeper( aBindingName: string ): IBindingKeeper;
    procedure AdviseLogFolderListener( aLogFolderListener: ILogFolderListener ); //one slot; set to nil to unadvise
    function ImportQueue: TImportQueue;
    procedure SetImportQueueFrame( aImportQueueFrame: IImportQueueFrame );
    procedure SetConnectionStringInputFrame( aConnectionStringInputFrame: IConnectionStringInputFrame );
    procedure MoveQueuedImportUp( index: integer );
    procedure MoveQueuedImportDown( index: integer );
    procedure RemoveQueuedImport( index: integer );
    function IsConnected: boolean; //for 2 db path input frame

    procedure SetLogFolder( aLogFolder: string );
    function LogFolder: string;
    procedure SetDevLogEnabled( enabled: boolean );
    function DevLogEnabled: boolean;
   end;


  TImportVisualController = class ( TInterfacedObject, IImportVisualController )
  private
    FImportQueue: TImportQueue;

    FLoggerKeeper: IImportLoggerKeeper;
    FImport: IImport;
    FParamInput: IParamInputFrame;
    FViewer: ITablespaceViewer;
    FConnectionStringInputFrame: IConnectionStringInputFrame;
    FImportQueueFrame: IImportQueueFrame;

    FDataWasImported: boolean;
    FImportSetup: TImportSetup;
    FBindingKeepers: IBindingKeepers;
    FLogFolder: string;
    FDevLogEnabled: boolean;

    FSetupNameListener: ISetupNameListener;
    FLogFolderListener: ILogFolderListener;
    FConnectionStatusListener: IConnectionStatusListener;

    function MakeMatchersList: IMatchersList;
    procedure FireConnectionStringChanged;
    procedure FireSetupNameChanged;
    procedure FireLogFolderChanged;
    procedure FireDevLogEnabledChanged;
    procedure FireImportQueueChanged;
    procedure FireQueueItemStatusChanged;
    procedure DoConnect(const aConnectionString: string);
    procedure SetSetup( aNewSetup: TImportSetup );
    procedure RunQueuedImport(qid: TQueuedImportData);
  public
    constructor Create( aLoggerKeeper: IImportLoggerKeeper; aImport: IImport; aParamInput: IParamInputFrame; aViewer: ITablespaceViewer );
    destructor Destroy; override;

    {IImportVisualController}
    procedure RunImport;
    procedure EnqueueImport;
    procedure RunQueuedImports;
    function CanRunQueuedImports: boolean;

    procedure SetConnectionString( aConnectionString: string );
    procedure Disconnect;
    function  CanRunImport: boolean;
    function DataWasImported: boolean;
    procedure AdviseSetupNameListener( aSetupNameListener: ISetupNameListener );
    procedure AdviseConnectionStatusListener( aConnectionStatusListener: IConnectionStatusListener ); //one slot; set to nil to unadvise
    function SetupName: string;
    procedure WriteSetup( aSetupName: string );
    procedure OpenSetup( aSetupName: string );
    procedure NewSetup;
    function SetupIsChanged: boolean;
    procedure LoadDefaults;
    function ConnectionString: string;
    function BindingKeeper( aBindingName: string ): IBindingKeeper;
    procedure AdviseLogFolderListener( aLogFolderListener: ILogFolderListener ); //one slot; set to nil to unadvise
    function ImportQueue: TImportQueue;
    procedure SetImportQueueFrame( aImportQueueFrame: IImportQueueFrame );
    procedure SetConnectionStringInputFrame( aConnectionStringInputFrame: IConnectionStringInputFrame );
    procedure MoveQueuedImportUp( index: integer );
    procedure MoveQueuedImportDown( index: integer );
    procedure RemoveQueuedImport( index: integer );
    function IsConnected: boolean;
    procedure SetLogFolder( aLogFolder: string );
    function LogFolder: string;
    procedure SetDevLogEnabled( enabled: boolean );
    function DevLogEnabled: boolean;
  end;

function CreateNullParamInput: IParamInputFrame;

implementation

uses
  sysutils,  forms, evutils, comobj,
  typinfo;

const
  sConnectionString = 'ConnectionString'; 
  sSetupFilename = 'SetupFilename';
  sLogFolder = 'LogFolder';
  sDevLogEnabled = 'DevLogEnabled';

  EVOLUTION_IMPORT_SETUP_SETTINGS = EVOLUTION_IMPORT_SETTINGS + 'Setup\';

{ TNullParamInputFrame }
type
  TNullParamInputFrame = class( TInterfacedObject, IParamInputFrame )
  private
    {IParamInputFrame}
    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

function CreateNullParamInput: IParamInputFrame;
begin
  Result := TNullParamInputFrame.Create;
end;

procedure TNullParamInputFrame.ControlsToParams(aParams: TStrings);
begin
end;

procedure TNullParamInputFrame.ParamsToControls(aParams: TStrings);
begin
end;

{ TImportVisualController }

constructor TImportVisualController.Create(aLoggerKeeper: IImportLoggerKeeper; aImport: IImport; aParamInput: IParamInputFrame; aViewer: ITablespaceViewer );
var
  i: integer;
begin
  FImportQueue := TImportQueue.Create;
  FLoggerKeeper := aLoggerKeeper;
  FImport := aImport;
  FParamInput := aParamInput;
  FViewer := aViewer;

  FDataWasImported := false;
  FBindingKeepers := TBindingKeepers.Create;
  SetSetup( TImportSetup.Create( FImport.ImportName + ' Setup' ) );

  for i := 0 to FImport.MappingDescCount-1 do
    FBindingKeepers.Add( gdyBinder.CreateBindingKeeper( FImport.MappingDesc(i), TevClientDataSet ) );
end;

procedure TImportVisualController.DoConnect(const aConnectionString: string);
begin
  FImport.DataModule.OpenConnection( aConnectionString );
  FireConnectionStringChanged;
  FViewer.SetTablespace( FImport.DataModule );
  FBindingKeepers.SetConnected( true );
  mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sConnectionString] := aConnectionString;
end;

procedure TImportVisualController.Disconnect;
begin
  FBindingKeepers.SetConnected( false );
  FViewer.SetTablespace( nil );
  FImport.DataModule.CloseConnection;
  FireConnectionStringChanged;
end;

function NowAsStr: string;
begin
  Result := FormatDateTime( 'mm-dd-yyyy (hh-nn-ss)', Now );
end;

procedure TImportVisualController.EnqueueImport;
begin
  FParamInput.ControlsToParams( FImportSetup.Params );
  FImportQueue.Add.Init( FImport.DataModule.CustomCompanyNumber, FImport.DataModule.ConnectionString, FImportSetup.Params, MakeMatchersList, FLogFolder );
  FireImportQueueChanged;
end;

procedure TImportVisualController.RunImport;
begin
  try
    EnqueueImport;
    RunQueuedImport( FImportQueue[FImportQueue.Count-1] );
  except
    FLoggerKeeper.Logger.StopException; //AndWarnFmt( '%s aborted.', [FImport.ImportName] );
  end;
end;

procedure TImportVisualController.SetConnectionString(aConnectionString: string);
begin
  Disconnect;
  DoConnect( aConnectionString );
end;

procedure TImportVisualController.FireConnectionStringChanged;
begin
  if assigned(FConnectionStringInputFrame) then
    FConnectionStringInputFrame.ConnectionStringChanged;
  if assigned(FConnectionStatusListener) then
    FConnectionStatusListener.ConnectionChanged( FImport.DataModule );
end;

function TImportVisualController.CanRunImport: boolean;
begin
  Result := FImport.DataModule.Connected and FBindingKeepers.MatchersAvailable;
end;

function TImportVisualController.DataWasImported: boolean;
begin
  Result := FDataWasImported;
end;

destructor TImportVisualController.Destroy;
begin
  inherited;
  if FImport.DataModule.Connected then
    Disconnect;
//  FConnectionStringInputFrame.SetController( nil ); //we cannot get here if FConnectionStringInputFrame holds reference to us 
  FBindingKeepers.Deactivating;
  FreeAndNil( FImportSetup );
  FreeAndNil( FImportQueue );
end;

procedure TImportVisualController.FireSetupNameChanged;
begin
  if assigned(FSetupNameListener) then
    FSetupNameListener.SetupNameChanged;
end;

procedure TImportVisualController.AdviseSetupNameListener(
  aSetupNameListener: ISetupNameListener);
begin
  FSetupNameListener := aSetupNameListener;
end;

function TImportVisualController.SetupName: string;
begin
  Result := FImportSetup.FileName;
end;

procedure TImportVisualController.WriteSetup(aSetupName: string);
begin
  FParamInput.ControlsToParams( FImportSetup.Params );
  FImportSetup.SaveToFile( aSetupName );
  FBindingKeepers.WriteSetup( FImportSetup.Filename );
  FireSetupNameChanged;
end;

procedure TImportVisualController.OpenSetup(aSetupName: string);
begin
  SetSetup( TImportSetup.Create( FImport.ImportName + ' Setup', aSetupName ) );
end;

function TImportVisualController.SetupIsChanged: boolean;
begin
  FParamInput.ControlsToParams( FImportSetup.Params );
  Result := FImportSetup.IsChanged or FBindingKeepers.IsChanged;
end;

procedure TImportVisualController.LoadDefaults;
var
  constr: string;
  logfold: string;
  dle: string;
  setupFilename: string;
begin
  try
    constr := mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sConnectionString];
    if constr <> '' then
    begin
      if assigned(FConnectionStringInputFrame) then
        FConnectionStringInputFrame.SetDefaultConnectionString( constr );
//      if DebugMode then
//        SetConnectionString( constr )
    end
  except
    //do nothing
  end;

  try
    setupFilename := mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sSetupFilename];
    if setupFilename <> '' then
      OpenSetup( setupFilename );
  except
    //do nothing
  end;
  try
    logfold := mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sLogFolder];
    if trim(logfold) = '' then
      logfold := 'c:\';
    SetLogFolder( logfold );
  except
    //do nothing
  end;
  try
    dle := mb_AppSettings.GetValue(EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sDevLogEnabled, 'false' );
    if SameText(dle, BooleanIdents[False]) or SameText(dle, BooleanIdents[true]) then
      SetDevLogEnabled(  boolean(GetEnumValue(typeinfo(boolean), dle)) );
  except
    //do nothing
  end;
end;

function TImportVisualController.ConnectionString: string;
begin
  Result := FImport.DataModule.ConnectionString;
end;

function TImportVisualController.BindingKeeper(
  aBindingName: string): IBindingKeeper;
begin
  Result := FBindingKeepers.Get( aBindingName );
end;


function TImportVisualController.MakeMatchersList: IMatchersList;
var
  i: integer;
begin
  Result := TMatchersList.Create;
  for i := 0 to FImport.MappingDescCount-1 do
    Result.Add( BindingKeeper(FImport.MappingDesc(i).Name).CreateMatcher );
end;

procedure TImportVisualController.RunQueuedImport( qid: TQueuedImportData );
begin
  qid.Status := qisRunning;
  FireQueueItemStatusChanged;
  try
    try
      FLoggerKeeper.StartLoggingToFile( LogFolder + FImport.DataModule.CustomCompanyNumber + ' ' + FImport.ImportName + ' ' + NowAsStr );
      try
        FImport.DataModule.OpenConnection( qid.ConnectionString );
        qid.LogFile := FLoggerKeeper.LogFileName;
        FDataWasImported := true;
        FImport.RunImport( qid.Params, qid.Matchers );
      finally
        FLoggerKeeper.StopLoggingToFile;
      end;
      qid.Status := qisCompleted;
    except
      on e: Exception do
        begin
          qid.Status := qisFailed;
          raise;
        end
    end;
  finally
    FireQueueItemStatusChanged;
  end;
end;

procedure TImportVisualController.RunQueuedImports;
var
  i: integer;
begin
  try
    for i := 0 to FImportQueue.Count - 1 do
      if FImportQueue[i].Status = qisWaiting then
      try
        RunQueuedImport( FImportQueue[i] );
      except
        FLoggerKeeper.Logger.StopMostExceptionsAndWarnFmt( 'Queued import execution aborted: <%s>', [FImportQueue[i].Description] );
      end;
  except
    FLoggerKeeper.Logger.StopException;
  end;
end;

function TImportVisualController.CanRunQueuedImports: boolean;
var
  i: integer;
begin
  Result := false;
  for i := 0 to FImportQueue.Count - 1 do
    if FImportQueue[i].Status = qisWaiting then
    begin
      Result := true;
      break;
    end;
end;

function TImportVisualController.ImportQueue: TImportQueue;
begin
  Result := FImportQueue;
end;

procedure TImportVisualController.SetImportQueueFrame( aImportQueueFrame: IImportQueueFrame);
begin
  if Assigned( FImportQueueFrame ) then
    FImportQueueFrame.SetController( nil );
  FImportQueueFrame := aImportQueueFrame;
  if Assigned( FImportQueueFrame ) then
    FImportQueueFrame.SetController( Self );
end;

procedure TImportVisualController.SetConnectionStringInputFrame(
  aConnectionStringInputFrame: IConnectionStringInputFrame);
begin
  if Assigned( FConnectionStringInputFrame ) then
    FConnectionStringInputFrame.SetController( nil );
  FConnectionStringInputFrame := aConnectionStringInputFrame;
  if Assigned( FConnectionStringInputFrame ) then
    FConnectionStringInputFrame.SetController( Self );
end;


procedure TImportVisualController.FireImportQueueChanged;
begin
  if assigned(FImportQueueFrame) then
    FImportQueueFrame.QueueChanged();
end;

procedure TImportVisualController.FireQueueItemStatusChanged;
begin
  if assigned(FImportQueueFrame) then
    FImportQueueFrame.QueueItemStatusChanged();
end;

procedure TImportVisualController.MoveQueuedImportDown( index: integer );
begin
  FImportQueue.Items[index].Index := index-1;
  FireImportQueueChanged;
end;

procedure TImportVisualController.MoveQueuedImportUp( index: integer );
begin
  FImportQueue.Items[index].Index := index+1;
  FireImportQueueChanged;
end;

procedure TImportVisualController.RemoveQueuedImport(index: integer);
begin
  FImportQueue.Delete( index );
  FireImportQueueChanged;
end;

function TImportVisualController.IsConnected: boolean;
begin
  Result := FImport.DataModule.Connected;
end;

procedure TImportVisualController.NewSetup;
begin
  SetSetup( TImportSetup.Create( FImport.ImportName + ' Setup' ) );
end;

procedure TImportVisualController.SetSetup(aNewSetup: TImportSetup);
begin
  FreeAndNil( FImportSetup );
  FImportSetup := anewSetup;

  FParamInput.ParamsToControls( FImportSetup.Params );
  FParamInput.ControlsToParams( FImportSetup.OrigParams ); //!!ugly way to get default values which are generated by UI controls now
  FireSetupNameChanged;
  FBindingKeepers.ApplySetup( FImportSetup.FileName );
  if FImportSetup.FileName <> '' then
    mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sSetupFilename] := FImportSetup.FileName;
end;

function TImportVisualController.DevLogEnabled: boolean;
begin
  Result := FDevLogEnabled;
end;

procedure TImportVisualController.SetDevLogEnabled(enabled: boolean);
begin
  if enabled <> FDevLogEnabled then
  begin
    FDevLogEnabled := enabled;
    try
      mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName + '\' + sDevLogEnabled] := GetEnumName(typeinfo(boolean), ord(FDevLogEnabled));
      if FDevLogEnabled then
        FLoggerKeeper.StartDevLoggingToFile( FLogFolder + FImport.ImportName + ' ' + NowAsStr + ' - dev' )
      else
        FLoggerKeeper.StopDevLoggingToFile;
    finally
      FireDevLogEnabledChanged;
    end;
  end;
end;

procedure TImportVisualController.FireDevLogEnabledChanged;
begin
  if assigned(FLogFolderListener) then
    FLogFolderListener.DevLogEnabledChanged;
end;

procedure TImportVisualController.SetLogFolder(aLogFolder: string);
var
  lf: string;
begin
  lf := trim(aLogFolder);
  if lf[Length(lf)] <> '\' then
    lf := lf + '\';
  if FLogFolder <> lf then
  begin
    FLogFolder := lf;
    try
      mb_AppSettings[EVOLUTION_IMPORT_SETUP_SETTINGS + FImport.ImportName +'\' + sLogFolder] := FLogFolder;
      if FDevLogEnabled then
        FLoggerKeeper.StartDevLoggingToFile( FLogFolder + FImport.ImportName + ' ' + NowAsStr + ' - dev' );
    finally
      FireLogFolderChanged;
    end;
  end;
end;

procedure TImportVisualController.FireLogFolderChanged;
begin
  if assigned(FLogFolderListener) then
    FLogFolderListener.LogFolderChanged;
end;

function TImportVisualController.LogFolder: string;
begin
  Result := FLogFolder;
end;

procedure TImportVisualController.AdviseLogFolderListener(aLogFolderListener: ILogFolderListener);
begin
  FLogFolderListener := aLogFolderListener;
end;

procedure TImportVisualController.AdviseConnectionStatusListener(
  aConnectionStatusListener: IConnectionStatusListener);
begin
  FConnectionStatusListener := aConnectionStatusListener;
end;

end.
