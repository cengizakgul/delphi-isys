// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit genericImport;
{$include gdy.inc}
interface

uses
  db, classes, iemap, gdybinder{IMatchers}, gdylogger{ILogger}, sysutils, evExceptions;


type
  TImportLocation = record
    ExternalTable: string;
    ExternalRow: string;
    KeyValues: string;
  end;

  IImportLogger = interface
['{DEBA6A21-A44C-4FE3-86B8-A9D0BDA288BF}']
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call

    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string );
    procedure LogEventFmt( s: string; const args: array of const );
    procedure LogWarning( s: string );
    procedure LogWarningFmt( s: string; const args: array of const );
    procedure LogError( s: string );
    procedure LogErrorFmt( s: string; const args: array of const );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopMostExceptionsAndWarnFmt( s: string; const args: array of const ); //EAbortTableImport is passed further
  end;

  IImportLoggerKeeper = interface
['{99CA2ED0-41B7-4BFF-8E10-CCF26ED82646}']
    procedure StartDevLoggingToFile( filename: string );
    procedure StopDevLoggingToFile;
    procedure StartLoggingToFile( filename: string );
    procedure StopLoggingToFile;
    function Logger: IImportLogger;
    function LogFileName: string;
  end;

  ITablespace = interface
['{B8FB4176-6380-4B6E-B38A-C8172A4433DF}']
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  end;

  IExternalDataModule = interface (ITablespace)
['{5D0222AC-CDD1-4F28-BDAF-8C08B54673DC}']
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    procedure SetLogger( aLogger: IImportLogger ); //should be set once after construction; it is safe now to change it during lifecycle but there is no need for this
  end;

  TKeySourceType = (ksFixedValue, ksFromThisTable, ksLookup, ksEmpty );
  //we need ksEmpty if we want to add a table to key tables without specifing additional keys (parent tables' nbr is enough)
  TKeySourceDesc = record
    ExternalTable: string;
    ExternalField: string; // empty string for row level key
    EvoKeyTable: string;
    EvoKeyField: string;
    HowToGet: TKeySourceType;
    ValueSourceField: string; // ksFromThisTable or ksLookup
    //only needed when HowToGet = ksLookup
    KeyField: string;
    LookupTable: string;
    LookupKeyField: string;
  end;
  TKeySourceDescs = array of TKeySourceDesc;

  TRowImportDesc = record
    ExternalTable: string;
    CustomFunc: string;
  end;
  TRowImportDescs = array of TRowImportDesc;

  TExternalTableRowDesc = record
    ExternalTable: string;
    FormatString: string;
    Fields: string;
  end;

  TMapRecords = array of TMapRecord;

  TFieldValues = class
  private
    function GetCount: integer;
    procedure SetCount( Value: integer );
    function GetValue(key: string): Variant;

  public
    Tables, Fields: TStringArray;
    Values: TVariantArray;
    procedure AddNotNull( const aTable, aField: string; const aValue: Variant );
    procedure Add( const aTable, aField: string; const aValue: Variant );
    procedure AddFirst(const aTable, aField: string; const aValue: Variant); //ugly workaround for absence of keytables sorting in iemap
    property Count: integer read GetCount;
    function _Dump: string;
    function DumpLast( n: integer ): string;
//    procedure Clear;
    function IsEqual( const ka: TFieldValues ): boolean;
    procedure Assign( const ka: TFieldValues );
    property Value[key: string]: Variant read GetValue; default;
    function IsEmpty: boolean;
    function HasValue(key: string): boolean;
  end;

  IImportDefinition = interface (IIEDefinition)
['{34F6FAEB-82C2-42F8-B4A3-1A7E6627E3C0}']
    procedure SetLogger( aLogger: IImportLogger ); //should be set once after construction; it is safe now to change it during lifecycle but there is no need for this

    function ConvertValue( TableName, FieldName: string; const Value: Variant): Variant; //throws exceptions
    procedure GetRowKeys( aTableSpace: ITablespace; aRow, aKeys: TFieldValues; aTablename: string ); //throws exceptions
    procedure AddFieldKeys( aTableSpace: ITablespace; aRow, aKeys: TFieldValues; aTablename, aFieldName: string ); //throws exceptions
    function Map: TMapRecords;
    function RowImport: TRowImportDescs;
    procedure ExecuteRowCustomFunc( const aMethodName: string; const Input, Keys, Output: TFieldValues );
    //external context setters
    procedure SetFixedKeys( aFixedKeys: string );
    procedure SetParam( ParamName: string; Value: Variant );
    procedure SetMatchers( aMatchers: IMatchersList );
    function  Matchers: IMatchersList;
  end;

  TInterfacedObjectWithVirtualCtor = class (TInterfacedObject)
  protected
    FImpEngine: IIEMap;
  public
    constructor Create(const AIEMap: IIEMap); virtual;
  end;

  TComponentClass = class of TComponent;
  TInterfacedObjectWithVirtualCtorClass = class of TInterfacedObjectWithVirtualCtor;

  TImportDesc = record
    ImportName: string;
    RequiredMatchersNames: string;
    ExternalDataModuleImplClass: TComponentClass;
    ImportDefinitionImplClass: TInterfacedObjectWithVirtualCtorClass;
  end;

  IImport = interface
['{F6BC6840-1B44-4AA1-BD80-12467EAEE0D1}']
    procedure RunImport( aParams: TStrings; aMatchers: IMatchersList );
    function ImportName: string;
    function DataModule: IExternalDataModule;
    function MappingDescCount: integer;
    function MappingDesc( i: integer ): TBindingDesc;
  end;

  ITablespaceViewer = interface
['{AAD2B6BA-0955-4D6B-A1AE-DE86119FCEC7}']
    procedure SetTablespace( aTablespace: ITablespace );
  end;

{  TBindingSourceDesc = record
    BindingName: string;
    ExternalTable: string;
    EvoTable: string;
  end;
}

//clumsy
  TSystemAndClientDBPaths = record
    SystemDBPath: string;
    ClientDBPath: string;
  end;
function ParseConnectionString( aConnStr: string ): TSystemAndClientDBPaths;
function ComposeConnectionString( paths: TSystemAndClientDBPaths ): string;

type
  EAbortTableImport = class(Exception);

//doesn't rise exceptions but EAbortTableImport; puts everything to log
procedure ImportTable(const AIIEMap: IIEMap; aLogger: IImportLogger; aTablespace: ITablespace; aIDef: IImportDefinition; tablename: string; aTaskName: string; postAfterEachRecord: boolean = false );

const
  EVOLUTION_IMPORT_SETTINGS = 'Import\';

const
  //Import log context items
  sciExtDatabase = 'External Database';
  sciExtSysDatabase = 'External System Database';
  sciCompanyCustomNumber = 'Company Custom Number';
  sciExtTableName = 'External Table';
  sciRowKeys = 'Row Keys';  
  sciExternalField = 'External Field';
  sciFieldKeys = 'Field Keys';

const
  sImportFailedToOpenConnection = 'Failed to initialize data source';

implementation

uses
  genericImportImpl{$ifdef D6_UP},variants{$endif}, gdyclasses, comobj;


{ TFieldValues }

procedure TFieldValues.Add(const aTable: string; const aField: string; const aValue: Variant);
begin
  SetCount( Count + 1 );
  tables[High(tables)] := aTable;
  fields[High(fields)] := aField;
  values[High(values)] := aValue;
end;

procedure TFieldValues.AddFirst( const aTable, aField: string; const aValue: Variant );
var
  i: integer;
begin
  SetCount( Count + 1 );
  for i := Count-1 downto 1 do
  begin
    Tables[i] := Tables[i-1];
    Fields[i] := Fields[i-1];
    Values[i] := Values[i-1];
  end;
  Tables[0] := aTable;
  Fields[0] := aField;
  Values[0] := aValue;
end;

function TFieldValues.GetCount: integer;
begin
  Result := Length(Tables);
end;

function TFieldValues._Dump: string;
begin
  Result := DumpLast( Count );
end;

function TFieldValues.DumpLast(n: integer): string;
var
  i: integer;
begin
  Assert( n <= Count );
  Result := '';
  for i := Count-n to Count-1 do
  begin
    if Result <> '' then
      Result := Result + #13#10;
    Result := Result + Tables[i] + '.' + Fields[i]+' = <'+VarToStr(Values[i])+'>';
  end
end;

{
procedure TKeysArray.Clear;
begin
  SetCount(0);
end;
}
procedure TFieldValues.SetCount(Value: integer);
begin
  SetLength( tables, Value );
  SetLength( fields, Value );
  SetLength( values, Value );
end;

function TFieldValues.IsEqual( const ka: TFieldValues): boolean;
  function VarEqual( const v1: Variant; const v2: Variant ): boolean;
  begin
    if VarIsEmpty(v1) or VarIsEmpty(v2) then
      Result := VarIsEmpty(v1) and VarIsEmpty(v2)
    else
      Result := v1 = v2;
  end;
var
  i: integer;
begin
  Result := Count = ka.Count;
  if Result then
    for i := 0 to Count-1 do
      if not ( SameText(Tables[i], ka.Tables[i]) and SameText(Fields[i], ka.Fields[i]) and VarEqual(Values[i], ka.Values[i]) ) then
      begin
        Result := false;
        break;
      end;
end;

procedure TFieldValues.Assign( const ka: TFieldValues);
var
  i: integer;
begin
  SetCount( ka.Count );
  for i := 0 to Count-1 do
  begin
    tables[i] := ka.tables[i];
    fields[i] := ka.fields[i];
    values[i] := ka.values[i];
  end;
end;

function TFieldValues.GetValue(key: string): Variant;
var
  i: integer;
  bFound: boolean;
begin
  bFound := false;
  key := UpperCase( key );
  for i := low(Tables) to high(Tables) do
    if key = UpperCase(Tables[i] + '.' + Fields[i]) then
    begin
      Result := Values[i];
      bFound := true;
      break;
    end;
  if not bFound then
    raise EevException.CreateFmt( 'Cannot find field value: <%s>', [key]);
end;


procedure ImportTable(const AIIEMap: IIEMap; aLogger: IImportLogger; aTablespace: ITablespace; aIDef: IImportDefinition; tablename: string; aTaskName: string; postAfterEachRecord: boolean = false );
begin
  aLogger.LogEntry( 'ImportTable' );
  try
    try
      aLogger.LogContextItem( sciExtTableName, tableName );
      if aTablespace.GetTable(tablename).Active then  //7902
      begin
        AIIEMap.StartMapping( aIDef.Map, aIDef, false );
        try
          with TTableImporter.Create(AIIEMap, aLogger, aTablespace, aIDef ) do
          try
            ImportTable( tablename, aTaskName, postAfterEachRecord );
          finally
            Free;
          end;
        finally
          AIIEMap.EndMapping;
        end;
      end
      else
        raise EevException.CreateFmt('Failed to open %s table. Task <%s> cannot be executed.',[tablename, aTaskName]);
    except
      aLogger.StopMostExceptionsAndWarnFmt( 'Import of %s table aborted.', [tablename] );
    end;
  finally
    aLogger.LogExit;
  end;
end;

const
  sSystemDBPathAlias = 'SystemDB';
  sClientDBPathAlias = 'ClientDB';

function ParseConnectionString( aConnStr: string ): TSystemAndClientDBPaths;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    Result.SystemDBPath := '';
    Result.ClientDBPath := '';
    try
      sl.Text := StringReplace( aConnStr, ';', #13, [rfReplaceAll]);
      Result.SystemDBPath := sl.Values[sSystemDBPathAlias];
      Result.ClientDBPath := sl.Values[sClientDBPathAlias];
    except
    end;
  finally
    FreeAndNil( sl );
  end;
end;

function ComposeConnectionString( paths: TSystemAndClientDBPaths ): string;
begin
  Result := '';
  if trim(paths.SystemDBPath) <> '' then
    Result := sSystemDBPathAlias + '=' + trim(paths.SystemDBPath);
  if trim(paths.ClientDBPath) <> '' then
  begin
    if Result <> '' then
      Result := Result + ';';
    Result := Result + sClientDBPathAlias + '=' + trim(paths.ClientDBPath);
  end
end;

procedure TFieldValues.AddNotNull(const aTable, aField: string;
  const aValue: Variant);
begin
  if not VarIsNull(aValue) then
    Add(aTable, aField, aValue);
end;

function TFieldValues.IsEmpty: boolean;
begin
  Result := Count = 0;
end;

function TFieldValues.HasValue(key: string): boolean;
var
  i: integer;
begin
  Result := false;
  key := UpperCase( key );
  for i := low(Tables) to high(Tables) do
    if key = UpperCase(Tables[i] + '.' + Fields[i]) then
    begin
      Result := true;
      break;
    end;
end;

{ TInterfacedObjectWithVirtualCtor }

constructor TInterfacedObjectWithVirtualCtor.Create(const AIEMap: IIEMap);
begin
  inherited Create;

  FImpEngine := AIEMap;
end;

end.
