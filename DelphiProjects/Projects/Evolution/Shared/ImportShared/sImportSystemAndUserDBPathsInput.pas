// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportSystemAndUserDBPathsInput;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, sImportVisualController, ActnList,
  ExtCtrls, genericImport, ISBasicClasses, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type

  TImportSystemAndUserDBPathsInputFrame = class(TFrame, IConnectionStringInputFrame )
    OpenDialog1: TOpenDialog;
    evPanel3: TevPanel;
    lblSystemDB: TevLabel;
    lblClientDB: TevLabel;
    bbSystemDB: TevBitBtn;
    bbClientDB: TevBitBtn;
    evBitBtn1: TevBitBtn;
    evActionList1: TevActionList;
    connect: TAction;
    Disconnect: TAction;
    evBitBtn2: TevBitBtn;
    procedure connectUpdate(Sender: TObject);
    procedure DisconnectUpdate(Sender: TObject);
    procedure connectExecute(Sender: TObject);
    procedure DisconnectExecute(Sender: TObject);
    procedure bbSystemDBClick(Sender: TObject);
    procedure bbClientDBClick(Sender: TObject);
  private
    { Private declarations }
    FController: IImportVisualController;
    FPaths: TSystemAndClientDBPaths;
    procedure PathsChanged;
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
  public
    { Public declarations }
  end;

implementation

uses
  evutils;
{$R *.DFM}

procedure TImportSystemAndUserDBPathsInputFrame.bbSystemDBClick(
  Sender: TObject);
begin
  FPaths.SystemDBPath := RunFolderDialog( 'Select system database folder (where S10_DB.dat is located)', FPaths.SystemDBPath );
  PathsChanged;
  Disconnect.Execute;
end;

procedure TImportSystemAndUserDBPathsInputFrame.bbClientDBClick(
  Sender: TObject);
begin
  FPaths.ClientDBPath := RunFolderDialog( 'Select client database folder', FPaths.ClientDBPath );
  PathsChanged;
  Disconnect.Execute;
end;

procedure TImportSystemAndUserDBPathsInputFrame.ConnectionStringChanged;
begin
  if assigned(FController) and (trim(FController.ConnectionString)<>'') then
    FPaths := ParseConnectionString( FController.ConnectionString );
//  else
//    FPaths := ParseConnectionString( '' );
  PathsChanged;
end;

procedure TImportSystemAndUserDBPathsInputFrame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TImportSystemAndUserDBPathsInputFrame.connectUpdate(
  Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FController) and not FController.IsConnected and (FPaths.SystemDBPath <> '') and (FPaths.ClientDBPath <> '');
end;

procedure TImportSystemAndUserDBPathsInputFrame.DisconnectUpdate(
  Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FController) and FController.IsConnected;
end;

procedure TImportSystemAndUserDBPathsInputFrame.connectExecute(
  Sender: TObject);
begin
  FController.SetConnectionString( ComposeConnectionString( FPaths ) );
end;

procedure TImportSystemAndUserDBPathsInputFrame.DisconnectExecute(
  Sender: TObject);
begin
  FController.Disconnect;
end;

procedure TImportSystemAndUserDBPathsInputFrame.PathsChanged;
begin
  if FPaths.SystemDBPath <> '' then
    lblSystemDB.Caption := 'Using system database ' + FPaths.SystemDBPath
  else
    lblSystemDB.Caption := 'No database path specified';
  if FPaths.ClientDBPath <> '' then
    lblClientDB.Caption := 'Importing from ' + FPaths.ClientDBPath
  else
    lblClientDB.Caption := 'No database path specified';
end;

procedure TImportSystemAndUserDBPathsInputFrame.SetDefaultConnectionString(aDefConnStr: string);
begin
  if not FController.IsConnected then
  begin
    FPaths := ParseConnectionString( aDefConnStr );
    PathsChanged;
  end;
end;

end.
