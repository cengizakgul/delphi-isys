unit EvVersionedCLCOConsFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem, SDataDictclient;

type
  TevVersionedCLCOCons = class(TevVersionedFieldBase)
    lValue: TevLabel;
    cbClCoCons: TevDBLookupCombo;
    DM_CLIENT: TDM_CLIENT;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedCLCOCons }

procedure TevVersionedCLCOCons.FormShow(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CL_CO_CONSOLIDATION.DataRequired('ALL');
  DM_CLIENT.CL_CO_CONSOLIDATION.Filter:= 'CONSOLIDATION_TYPE <> ''C''';
  DM_CLIENT.CL_CO_CONSOLIDATION.Filtered := true;
end;

end.

