unit EvImportMapRecord;

interface

uses Classes, Windows, SysUtils, EvCommonInterfaces, isBaseClasses, isBasicUtils, IEMap,
  EvStreamUtils, EvExchangeUtils, EvExchangeConsts;

type

  TEvImportMapRecord = class(TisInterfacedObject, IEvImportMapRecord)
  private
    FITable: string;
    FIField: string;
    FETable: string;
    FEField: string;
    FKeyField: string;
    FKeyLookupTable: string;
    FKeyLookupField: string;
    FKeyValue: string;
    FGroupName: string;
    FRecordNumber: integer;

    FMapType: TEvImportMapType;

    FLookupField1: String;
    FMMapDefault: String;
    FMMapValues: String;
    FLookupTable1: String;
    FFuncName: String;
    FMLookupTable: String;
    FMapValues: String;
    FLookupTable: String;
    FLookupField: String;
    FMapDefault: String;
    FMLookupField: String;

    FDescription: String;
    FMapRecordName: String;
    FVisibleToUser: boolean;
  protected
    procedure DoOnConstruction; override;
    procedure EmptyMapRecord;
    procedure EmptyVariablePart;

    function GetEField: string;
    function GetETable: string;
    function GetFuncName: String;
    function GetIField: string;
    function GetITable: string;
    function GetKeyField: string;
    function GetKeyLookupField: string;
    function GetKeyLookupTable: string;
    function GetKeyValue: string;
    function GetGroupName: string;
    function GetRecordNumber: integer;
    function GetLookupTable: String;
    function GetLookupField: String;
    function GetLookupField1: String;
    function GetLookupTable1: String;
    function GetMapDefault: String;
    function GetMapType: TEvImportMapType;
    function GetMapTypeName: String;
    function GetMapValues: String;
    function GetMLookupField: String;
    function GetMLookupTable: String;
    function GetMMapDefault: String;
    function GetMMapValues: String;
    function GetDescription: String;
    function GetMapRecordName: String;
    function GetXML: String;

    function IsVisibleToUser: boolean;

    procedure SetEField(const Value: string);
    procedure SetETable(const Value: string);
    procedure SetFuncName(const Value: String);
    procedure SetIField(const Value: string);
    procedure SetITable(const Value: string);
    procedure SetKeyField(const Value: string);
    procedure SetKeyLookupField(const Value: string);
    procedure SetKeyLookupTable(const Value: string);
    procedure SetKeyValue(const Value: string);
    procedure SetGroupName(const Value: string);
    procedure SetRecordNumber(const Value: integer);
    procedure SetLookupTable(const Value: String);
    procedure SetLookupField(const Value: String);
    procedure SetLookupField1(const Value: String);
    procedure SetLookupTable1(const Value: String);
    procedure SetMapDefault(const Value: String);
    procedure SetMapType(const Value: TEvImportMapType);
    procedure SetMapValues(const Value: String);
    procedure SetMLookupField(const Value: String);
    procedure SetMLookupTable(const Value: String);
    procedure SetMMapDefault(const Value: String);
    procedure SetMMapValues(const Value: String);
    procedure SetDescription(const Value: String);
    procedure SetMapRecordName(const Value: String);
    procedure SetVisibleToUser(const Value: boolean);
  public
    property ITable: string read GetITable write SetITable;
    property IField: string read GetIField write SetIField;
    property ETable: string read GetETable write SetETable;
    property EField: string read GetEField write SetEField;
    property KeyField: string read GetKeyField write SetKeyField;
    property KeyLookupTable: string read GetKeyLookupTable write SetKeyLookupTable;
    property KeyLookupField: string read GetKeyLookupField write SetKeyLookupField;
    property KeyValue: string read GetKeyValue write SetKeyValue;
    property GroupName: string read GetGroupName write SetGroupName;
    property RecordNumber: integer read GetRecordNumber write SetRecordNumber;

    property MapType: TEvImportMapType read GetMapType write SetMapType;
    // MapType = mtpCustom
    property FuncName: String read GetFuncName write SetFuncName;
    // MapType = mtpMap
    property MapValues: String read GetMapValues write SetMapValues;
    property MapDefault: String read GetMapDefault write SetMapDefault;
    // MapType = mtpLookup
    property LookupTable: String read GetLookupTable write SetLookupTable;
    property LookupField: String read GetLookupField write SetLookupField;
    property LookupTable1: String read GetLookupTable1 write SetLookupTable1;
    property LookupField1: String read GetLookupField1 write SetLookupField1;
    // MapType = mtpMapLookup
    property MLookupTable: String read GetMLookupTable write SetMLookupTable;
    property MLookupField: String read GetMLookupField write SetMLookupField;
    property MMapValues: String read GetMMapValues write SetMMapValues;
    property MMapDefault: String read GetMMapDefault write SetMMapDefault;

    property Description: String read GetDescription write SetDescription;

    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    constructor Create(const AMaprecord: TMapRecord); reintroduce;
    constructor CreateEmpty;

    class function GetTypeID: String; override;
  end;

implementation

{ TEvImportMapRecord }

constructor TEvImportMapRecord.Create(const AMaprecord: TMaprecord);
begin
  CreateEmpty;

  ITable := AMaprecord.ITable;
  IField := AMaprecord.IField;
  ETable := AMaprecord.ETable;
  EField := AMaprecord.EField;
  KeyField := AMaprecord.KeyField;
  KeyLookupTable := AMaprecord.KeyLookupTable;
  KeyLookupField := AMaprecord.KeyLookupField;
  KeyValue := AMaprecord.KeyValue;
  GroupName := AMaprecord.GroupName;
  RecordNumber := AMaprecord.RecordNumber;

  case AMaprecord.MapType of
  mtDirect: MapType := mtpDirect;
  mtLookup: begin
              MapType := mtpLookup;
              LookupTable := AMaprecord.LookupTable;
              LookupField := AMapRecord.LookupField;
              LookupTable1 := AMaprecord.LookupTable1;
              LookupField1 := AMapRecord.LookupField1;
            end;
  mtMap:    begin
              MapType := mtpMap;
              MapValues := AMapRecord.MapValues;
              MapDefault := AMaprecord.MapDefault;
            end;
  mtCustom: begin
              MapType := mtpCustom;
              FuncName := AMapRecord.FuncName;
            end;
  mtMapLookup:
            begin
              MapType := mtpMapLookup;
              MMapDefault := AMaprecord.MMapDefault;
              MMapValues := AMaprecord.MMapValues;
              MLookupTable := AMapRecord.MLookupTable;
              MLookupField := AMapRecord.MLookupField;
            end;
  else
    Assert(false, 'Unknown map type: ' + IntToStr(Integer(AMaprecord.MapType)));
  end;
end;

constructor TEvImportMapRecord.CreateEmpty;
begin
  inherited Create;  
end;

procedure TEvImportMapRecord.DoOnConstruction;
begin
  inherited;
  EmptyMapRecord;
end;

procedure TEvImportMapRecord.EmptyMaprecord;
begin
  FMapType := mtpDirect;
  FITable :='';
  FIField :='';
  FETable :='';
  FEField :='';
  FKeyField :='';
  FKeyLookupTable :='';
  FKeyLookupField :='';
  FKeyValue :='';
  FDescription := '';
  FVisibleToUser := true;
  
  EmptyVariablePart;
end;

procedure TEvImportMapRecord.EmptyVariablePart;
begin
  FLookupField1 :='';
  FMMapDefault :='';
  FMMapValues :='';
  FLookupTable1 :='';
  FLookupTable := '';
  FFuncName :='';
  FMLookupTable :='';
  FMapValues :='';
  FLookupField :='';
  FMapDefault :='';
  FMLookupField :='';
end;

function TEvImportMapRecord.GetDescription: String;
begin
  Result := FDescription;
end;

function TEvImportMapRecord.GetEField: string;
begin
  Result := FEField;
end;

function TEvImportMapRecord.GetETable: string;
begin
  Result := FETable;
end;

function TEvImportMapRecord.GetFuncName: String;
begin
  CheckCondition((FMapType = mtpCustom) or (FMapType = mtpMapCustom), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FFuncName;
end;

function TEvImportMapRecord.GetGroupName: string;
begin
  Result := FGroupName;
end;

function TEvImportMapRecord.GetIField: string;
begin
  Result := FIField;
end;

function TEvImportMapRecord.GetITable: string;
begin
  Result := FITable;
end;

function TEvImportMapRecord.GetKeyField: string;
begin
  Result := FKeyField;
end;

function TEvImportMapRecord.GetKeyLookupField: string;
begin
  Result := FKeyLookupField;
end;

function TEvImportMapRecord.GetKeyLookupTable: string;
begin
  Result := FKeyLookupTable;
end;

function TEvImportMapRecord.GetKeyValue: string;
begin
  Result := FKeyValue;
end;

function TEvImportMapRecord.GetLookupField: String;
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FLookupField;
end;

function TEvImportMapRecord.GetLookupField1: String;
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FLookupField1;
end;

function TEvImportMapRecord.GetLookupTable: String;
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FLookupTable;
end;

function TEvImportMapRecord.GetLookupTable1: String;
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FLookupTable1;
end;

function TEvImportMapRecord.GetMapDefault: String;
begin
  CheckCondition((FMapType = mtpMap) or ((FMapType = mtpMapCustom)), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMapDefault;
end;

function TEvImportMapRecord.GetMapRecordName: String;
begin
  Result := FMapRecordName;
end;

function TEvImportMapRecord.GetMapType: TEvImportMapType;
begin
  Result := FMapType;
end;

function TEvImportMapRecord.GetMapTypeName: String;
begin
  Result := 'Unknown';
  case FMapType of
    mtpDirect: Result := 'Direct';
    mtpLookup: Result := 'Lookup';
    mtpMap: Result := 'Map';
    mtpCustom: Result := 'Custom function';
    mtpMapLookup: Result := 'Map&Lookup';
    mtpMapCustom: Result := 'Map&Custom function';
  end;
end;

function TEvImportMapRecord.GetMapValues: String;
begin
  CheckCondition((FMapType = mtpMap) or (FMapType = mtpMapCustom), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMapValues;
end;

function TEvImportMapRecord.GetMLookupField: String;
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMLookupField;
end;

function TEvImportMapRecord.GetMLookupTable: String;
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMLookupTable;
end;

function TEvImportMapRecord.GetMMapDefault: String;
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMMapDefault;
end;

function TEvImportMapRecord.GetMMapValues: String;
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  Result := FMMapValues;
end;

function TEvImportMapRecord.GetRecordNumber: integer;
begin
  Result := FRecordNumber;
end;

class function TEvImportMapRecord.GetTypeID: String;
begin
  Result := 'evImportMapRecord';
end;

function TEvImportMapRecord.GetXML: String;
begin
  Result := StartTag(EvoXmapRecordTag) + #13#10;

  Result := Result + InsertTag(EvoXmapRecordNameTag, AddCDataTag(FMapRecordName));

  if FVisibleToUser then
    Result := Result + InsertTag(EvoXmapRecordVisibleToUserTag, '1')
  else
    Result := Result + InsertTag(EvoXmapRecordVisibleToUserTag, '0');

  if FDescription <> '' then
    Result := Result + InsertTag(EvoXmapRecordDescriptionTag, AddCDataTag(FDescription));

  case FMapType of
  mtpDirect:    begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpDirect');
                end;
  mtpLookup:    begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpLookup');
                  Result := Result + InsertTag(EvoXmapLookupTableTag, FLookupTable);
                  Result := Result + InsertTag(EvoXmapLookupFieldTag, FLookupField);
                  Result := Result + InsertTag(EvoXmapLookupTable1Tag, FLookupTable1);
                  Result := Result + InsertTag(EvoXmapLookupField1Tag, FLookupField1);
                end;
  mtpMap:       begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpMap');
                  Result := Result + InsertTag(EvoXmapValuesTag, AddCDataTag(FMapValues));
                  Result := Result + InsertTag(EvoXmapDefaultTag, AddCDataTag(FMapDefault));
                end;
  mtpCustom:    begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpCustom');
                  Result := Result + InsertTag(EvoXmapFuncNameTag, FFuncName);
                end;
  mtpMapLookup: begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpMapLookup');
                  Result := Result + InsertTag(EvoXmapLookupTableTag, FMLookupTable);
                  Result := Result + InsertTag(EvoXmapLookupFieldTag, FMLookupField);
                  Result := Result + InsertTag(EvoXmapValuesTag, AddCDataTag(FMMapValues));
                  Result := Result + InsertTag(EvoXmapDefaultTag, AddCDataTag(FMMapDefault));
                end;
  mtpMapCustom: begin
                  Result := Result + InsertTag(EvoXmapTypeTag, 'mtpMapCustom');
                  Result := Result + InsertTag(EvoXmapValuesTag, AddCDataTag(FMapValues));
                  Result := Result + InsertTag(EvoXmapDefaultTag, AddCDataTag(FMapDefault));
                  Result := Result + InsertTag(EvoXmapFuncNameTag, FFuncName);
                end;
  end;
  Result := Result + #13#10 + EndTag(EvoXmapRecordTag) + #13#10;
end;

function TEvImportMapRecord.IsVisibleToUser: boolean;
begin
  Result := FVisibleToUser;
end;

procedure TEvImportMapRecord.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FITable := AStream.ReadString;
  FIField := AStream.ReadString;
  FETable := AStream.ReadString;
  FEField := AStream.ReadString;
  FKeyField := AStream.ReadString;
  FKeyLookupTable := AStream.ReadString;
  FKeyLookupField := AStream.ReadString;
  FKeyValue := AStream.ReadString;

  FMapType := TEvImportMapType(AStream.ReadInteger);

  FLookupField1 := AStream.ReadString;
  FMMapDefault := AStream.ReadString;
  FMMapValues :=AStream.ReadString;
  FLookupTable1 := AStream.ReadString;
  FFuncName := AStream.ReadString;
  FMLookupTable := AStream.ReadString;
  FMapValues := AStream.ReadString;
  FLookupTable := AStream.ReadString;
  FLookupField := AStream.ReadString;
  FMapDefault := AStream.ReadString;
  FMLookupField := AStream.ReadString;

  FDescription := AStream.ReadString;
  FMapRecordName := AStream.ReadString;
  FVisibleToUser := AStream.ReadBoolean;
end;

procedure TEvImportMapRecord.SetDescription(const Value: String);
begin
  FDescription := Value;
end;

procedure TEvImportMapRecord.SetEField(const Value: string);
begin
  FEField := Value;
end;

procedure TEvImportMapRecord.SetETable(const Value: string);
begin
  FETable := Value;
end;

procedure TEvImportMapRecord.SetFuncName(const Value: String);
begin
  CheckCondition((FMapType = mtpCustom) or (FMapType = mtpMapCustom), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FFuncName := Value;
end;

procedure TEvImportMapRecord.SetGroupName(const Value: string);
begin
  FGroupName := Value;
end;

procedure TEvImportMapRecord.SetIField(const Value: string);
begin
  FIField := Value;
end;

procedure TEvImportMapRecord.SetITable(const Value: string);
begin
  FITable := Value;
end;

procedure TEvImportMapRecord.SetKeyField(const Value: string);
begin
  FKeyField := Value;
end;

procedure TEvImportMapRecord.SetKeyLookupField(const Value: string);
begin
  FKeyLookupField := Value;
end;

procedure TEvImportMapRecord.SetKeyLookupTable(const Value: string);
begin
  FKeyLookupTable := Value;
end;

procedure TEvImportMapRecord.SetKeyValue(const Value: string);
begin
  FKeyValue := Value;
end;

procedure TEvImportMapRecord.SetLookupField(const Value: String);
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FLookupField := Value;
end;

procedure TEvImportMapRecord.SetLookupField1(const Value: String);
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FLookupField1 := Value;
end;

procedure TEvImportMapRecord.SetLookupTable(const Value: String);
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FLookupTable := Value;
end;

procedure TEvImportMapRecord.SetLookupTable1(const Value: String);
begin
  CheckCondition(FMapType = mtpLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FLookupTable1 := Value;
end;

procedure TEvImportMapRecord.SetMapDefault(const Value: String);
begin
  CheckCondition((FMapType = mtpMap) or (FMapType = mtpMapCustom), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMapDefault := Value;
end;

procedure TEvImportMapRecord.SetMapRecordName(const Value: String);
begin
  FMapRecordName := Value;
end;

procedure TEvImportMapRecord.SetMapType(const Value: TEvImportMapType);
begin
  if FMapType <> Value then
  begin
    EmptyVariablePart;
    FMapType := Value;
  end;  
end;

procedure TEvImportMapRecord.SetMapValues(const Value: String);
begin
  CheckCondition((FMapType = mtpMap) or (FMapType = mtpMapCustom), 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMapValues := Value;
end;

procedure TEvImportMapRecord.SetMLookupField(const Value: String);
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMLookupField := Value;
end;

procedure TEvImportMapRecord.SetMLookupTable(const Value: String);
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMLookupTable := Value;
end;

procedure TEvImportMapRecord.SetMMapDefault(const Value: String);
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMMapDefault := Value;
end;

procedure TEvImportMapRecord.SetMMapValues(const Value: String);
begin
  CheckCondition(FMapType = mtpMapLookup, 'Field is not avaliable for this map type: ' + IntToStr(Integer(FMaptype)));
  FMMapValues := Value;
end;

procedure TEvImportMapRecord.SetRecordNumber(const Value: integer);
begin
  FRecordNumber := Value;
end;

procedure TEvImportMapRecord.SetVisibleToUser(const Value: boolean);
begin
  FVisibleToUser := Value;
end;

procedure TEvImportMapRecord.WriteSelfToStream(
  const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FITable);
  AStream.WriteString(FIField);
  AStream.WriteString(FETable);
  AStream.WriteString(FEField);
  AStream.WriteString(FKeyField);
  AStream.WriteString(FKeyLookupTable);
  AStream.WriteString(FKeyLookupField);
  AStream.WriteString(FKeyValue);

  AStream.WriteInteger(Integer(FMapType));

  AStream.WriteString(FLookupField1);
  AStream.WriteString(FMMapDefault);
  AStream.WriteString(FMMapValues);
  AStream.WriteString(FLookupTable1);
  AStream.WriteString(FFuncName);
  AStream.WriteString(FMLookupTable);
  AStream.WriteString(FMapValues);
  AStream.WriteString(FLookupTable);
  AStream.WriteString(FLookupField);
  AStream.WriteString(FMapDefault);
  AStream.WriteString(FMLookupField);

  AStream.WriteString(FDescription);
  AStream.WriteString(FMapRecordName);
  AStream.WriteBoolean(FVisibleToUser);
end;

initialization
  ObjectFactory.Register([TEvImportMapRecord]);

finalization
  SafeObjectFactoryUnRegister([TEvImportMapRecord]);
end.
