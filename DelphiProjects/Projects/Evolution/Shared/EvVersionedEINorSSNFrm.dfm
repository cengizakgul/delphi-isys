inherited evVersionedEINorSSN: TevVersionedEINorSSN
  Left = 433
  Top = 255
  ClientHeight = 471
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 331
    inherited grFieldData: TevDBGrid
      Height = 204
    end
    inherited pnlEdit: TevPanel
      Top = 248
      inherited pnlButtons: TevPanel
        Top = 19
        Height = 28
      end
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 331
    Width = 791
    Height = 140
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 140
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'W2 Information'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lablW2_Social_Security_Number: TevLabel
        Left = 20
        Top = 75
        Width = 119
        Height = 16
        Caption = '~Social Security Number'
        FocusControl = edSocial_Security_Number
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edSocial_Security_Number: TevDBEdit
        Left = 20
        Top = 90
        Width = 265
        Height = 21
        Color = clBtnFace
        DataField = 'SOCIAL_SECURITY_NUMBER'
        DataSource = dsFieldData
        Enabled = False
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object rg_SSN_EIN: TevDBRadioGroup
        Left = 20
        Top = 31
        Width = 128
        Height = 36
        HelpContext = 17720
        Caption = '~EIN or SSN'
        Columns = 2
        DataField = 'EIN_OR_SOCIAL_SECURITY_NUMBER'
        DataSource = dsFieldData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'EIN'
          'SSN')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'E'
          'S')
        OnChange = rg_SSN_EINChange
      end
    end
  end
end
