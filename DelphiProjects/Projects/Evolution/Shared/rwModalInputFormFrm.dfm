object rwModalInputForm: TrwModalInputForm
  Left = 434
  Top = 231
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Report Parameters'
  ClientHeight = 237
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 200
    Width = 358
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      358
      37)
    object btnOK: TButton
      Left = 186
      Top = 9
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 275
      Top = 9
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  inline InputFormContainer: TrwInputFormContainer
    Left = 0
    Top = 0
    Width = 358
    Height = 200
    Align = alClient
    AutoScroll = False
    TabOrder = 1
    OnResize = InputFormContainerResize
  end
end
