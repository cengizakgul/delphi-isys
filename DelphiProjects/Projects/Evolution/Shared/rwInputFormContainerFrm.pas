unit rwInputFormContainerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, rwConnectionInt;

type
  TrwInputFormContainer = class(TFrame)
    procedure FrameResize(Sender: TObject);
  private
    FInputForm: IrwReportInputForm;
    procedure SetContentSizeByContainer;    
  public
    procedure HoldInputForm(const InputForm: IrwReportInputForm);
  end;

implementation

{$R *.dfm}

procedure TrwInputFormContainer.FrameResize(Sender: TObject);
begin
  SetContentSizeByContainer;
end;

procedure TrwInputFormContainer.HoldInputForm(const InputForm: IrwReportInputForm);
begin
  FInputForm := InputForm;
  SetContentSizeByContainer;
end;

procedure TrwInputFormContainer.SetContentSizeByContainer;
var
  B: TrwBounds;
begin
  if Assigned(FInputForm) then
  begin
    B.Left := 0;
    B.Top := 0;
    B.Width := ClientWidth;
    B.Height := ClientHeight;
    FInputForm.FormBounds := B;
  end;
end;

end.
 