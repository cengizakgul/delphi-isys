unit EvUIComponents;

interface

uses Windows, Messages, Classes, Controls, StdCtrls, Types, Menus, DB, Graphics,
     Grids, TypInfo, StdActns, ActnList, ExtDlgs, SysUtils, CommCtrl, DBCtrls,
     wwDBLook, wwDBIGrd, wwPict, wwStr, CoolTrayIcon,
     isBasicClasses, isUILMDButton, IsTypes, isBaseClasses,
     SSecurityInterface, EvClientDataSet, EvStreamUtils, dialogs, wwdbdatetimepicker,
     Forms, wwdbedit;

const
  WM_ACTIVATE_PACKAGE = WM_USER + 222;
  WM_ACTIVATE_FRAME = WM_USER + 223;
  WM_ACTIVATE_FRAME_PARAMS = WM_USER + 224;
  WM_RECREATE_MENUS = WM_USER + 225;
  CM_SHOW_EFFECTIVE_DATE_DIALOG = WM_USER + 226;
  
  ControlGlowColor = $00FF9933;
  //VersionedControlColor = $00CCFFFF;   //original soft yellow
  VersionedControlColor = $0095CAFF;

procedure CtrlShowVersionedFieldEditor(const AControl: TControl; const ADataSet: TDataSet; const AField: String);  

type
  IevGlobalLookupHandler = interface
  ['{7935E125-F7EA-4F09-ACEF-4E5169267377}']
    procedure HandleGlobalLookupRequest( frameclass: TClass );
  end;

  IevDBFieldControl = interface(IisDBFieldControl)
  ['{060AB54A-1C07-49A5-A0BC-0A51C6B912E6}']
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;
  end;


  TFrameActivateParamsRec = record
    FramePath: String;
    Context: Integer;
    Params: array of variant;
  end;

  PTFrameActivateParamsRec = ^TFrameActivateParamsRec;

  EOnSetSecurityRO = procedure (const Control: TControl; var ReadOnly: Boolean; var Accept: Boolean) of object;


  TevIniAttributes = class(TISIniAttributes)
  public
    constructor Create;
  published
    property Enabled default True;
  end;

  TevStatusBar = class(TISStatusBar);

  TevToolBar = class(TISToolBar);

  TevToolButton = class(TISToolButton);

  TevCheckListBox = class(TISCheckListBox)
  private
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;

  TevListBox = class(TListBox)
  private
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
  end;

  TevImageList = class(TISImageList);

  TevImage = class(TISImage);

  TevActionList = class(TISActionList);

  TevListView = class(TISListView);

  TevDBTreeView = class(TISDBTreeView);

  TEvBevel = class(TISBevel);

  TevSplitter = class(TISSplitter);

  TevPopupMenu = class(TISPopupMenu);

  TevDBCheckGrid = class(TISDBCheckGrid);

  TevPageControl = class(TISPageControl);
 
  TevDBText = class(TISDBText);

  TevTabControl = class(TISTabControl);

  TevPanel = class(TISPanel);


  TevRadioButton = class(TISRadioButton)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
  protected
    function  GetEnabled: Boolean; override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevStringGrid = class(TISStringGrid)
  private
    FSecurityRO: Boolean;
    FReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    function  GetReadOnly: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
  published
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly stored False;
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevMemo = class(TISMemo)
  private
    FSecurityRO: Boolean;
    FSpellcheckEnabled: boolean;
    FPopupEditorEnabled: boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
  protected
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    function GetPopupMenu: TPopupMenu; override;
    procedure CreateHandle; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property SpellcheckEnabled: boolean read FSpellcheckEnabled write FSpellcheckEnabled default true;
    property PopupEditorEnabled: boolean read FPopupEditorEnabled write FPopupEditorEnabled default true;
  end;


  TevRichEdit = class(TISRichEdit)
  private
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
  protected
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevDBRichEdit = class(TISDBRichEdit, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure CreateHandle; override;
    function GetReadOnly: Boolean; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevSpinEdit = class(TISSpinEdit)
  private
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
  protected
    procedure UpClick (Sender: TObject); override;
    procedure DownClick (Sender: TObject); override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevRadioGroup = class(TISRadioGroup)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
  protected
    procedure Paint; override;
    function  GetEnabled: Boolean; override;
  public
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevDBImage = class(TISDBImage, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    FContextMenu: TPopupMenu;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
    procedure miLoadFromFile(Sender: TObject);
    procedure miSaveToFile(Sender: TObject);
    procedure miClear(Sender: TObject);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure SetAutoDisplay(Value: Boolean); override;
    function  GetAutoDisplay: Boolean; override;
    function  FieldIsBlob: Boolean; override;
    function  GetFieldData: IisStream; override;
    procedure SetFieldData(const AValue: IisStream); override;
    function  GetNotLoadedText: String; override;

    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;

    procedure ContexMenu_LoadFromFile;
    procedure ContexMenu_SaveToFileDialog;
    procedure ContexMenu_Clear;
    procedure CutToClipboard; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevTreeView = class(TISTreeView)
  private
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure WMStyleChanging(var Message: TMessage); message WM_STYLECHANGING;
  protected
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevEdit = class(TISEdit)
  private
    FSecurityRO: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
  protected
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevDBSpinEdit = class(TISDBSpinEdit, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure UpClick (Sender: TObject); override;
    procedure DownClick (Sender: TObject); override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TEvDBMemo = class(TISDBMemo, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FSpellcheckEnabled: boolean;
    FPopupEditorEnabled: boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
    procedure SyncState;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure SetAutoDisplay(Value: Boolean); override;
    function  GetAutoDisplay: Boolean; override;
    function  FieldIsBlob: Boolean; override;
    function  GetFieldText: String; override;
    procedure SetFieldText(const AValue: String); override;
    function  GetNotLoadedText: String; override;

    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    function  GetPopupMenu: TPopupMenu; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure CreateHandle; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property SpellcheckEnabled: boolean read FSpellcheckEnabled write FSpellcheckEnabled default true;
    property PopupEditorEnabled: boolean read FPopupEditorEnabled write FPopupEditorEnabled default true;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevCheckBox = class(TISCheckBox)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
  protected
    function  GetEnabled: Boolean; override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevComboBox = class(TISComboBox)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
    function  GetStyle: TComboBoxStyle;
    procedure SetMyStyle(const Value: TComboBoxStyle);
  protected
    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property Style: TComboBoxStyle read GetStyle write SetMyStyle;
  end;


  TevDBComboDlg = class(TISDBComboDlg, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FOnSetSecurityRO: EOnSetSecurityRO;
    FAutoPopupEffectiveDateDialog: Boolean;
    FAutoPopupForceQuartersOnly : Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure ActiveChange(Sender: TObject);
    procedure SetSecurityRO(Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  public
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure DropDown; override;
    constructor Create(AOwner: TComponent); override;
    procedure ShowCategorizedComboDialog;
    procedure CreateHandle; override;
    procedure MouseDown(Button:TMouseButton; Shift:TShiftState; X, Y: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property OnSetSecurityRO: EOnSetSecurityRO read FOnSetSecurityRO write FOnSetSecurityRO;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property AutoPopupForceQuartersOnly: Boolean read FAutoPopupForceQuartersOnly write FAutoPopupForceQuartersOnly default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevDBComboBox = class(TISDBComboBox, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FDontAutoPopupEffectiveDateDialog: Boolean;
    FAutoPopupForceQuartersOnly: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    FAlreadyFired : Boolean;
    procedure ActiveChange(Sender: TObject);
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    function GetAutoDropDown: Boolean;
    function GetStyle: TComboBoxStyle;
    procedure SetAutoDropDown(const Value: Boolean);
    procedure SetStyle(const Value: TComboBoxStyle);
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;

  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    function  GetPopupMenu: TPopupMenu; override;
    function  GetEnabled: Boolean; override;
    procedure CreateHandle; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure MouseDown(Button:TMouseButton; Shift:TShiftState; X, Y: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DropDown; override;
    procedure CloseUp(Accept: Boolean); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property Style: TComboBoxStyle read GetStyle write SetStyle;
    property AutoDropDown: Boolean read GetAutoDropDown write SetAutoDropDown;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property DontAutoPopupEffectiveDateDialog: Boolean read FDontAutoPopupEffectiveDateDialog write FDontAutoPopupEffectiveDateDialog default False;
    property AutoPopupForceQuartersOnly: Boolean read FAutoPopupForceQuartersOnly write FAutoPopupForceQuartersOnly default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevDBLookupComboDlg = class(TISDBLookupComboDlg, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;
  public
    constructor Create(AOwner: TComponent); override;
    procedure DropDown; override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property UseTFields default False;
    property ControlInfoInDataSet default False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevDBCheckBox = class(TISDBCheckBox, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevDataSource = class(TISDataSource)
  private
    FMasterDataSource: TevDataSource;
    FChildList: TList;
    FOnDataChange: TDataChangeEvent;
    FKeyValue: Integer;
    FOnBeforeChildrenDataChange: TDataChangeEvent;
    FPriorState: TDataSetState;
    FOnStateChange: TNotifyEvent;
    FDataChangedOnLoading: Boolean;
    procedure SetMasterDataSource(const Value: TevDataSource);
    function GetActive: Boolean;
    procedure SetActive(const Value: Boolean);
    function GetKeyValue: Integer;
    procedure SetDataSet(const Value: TDataSet);
    procedure DoStateChange(Sender: TObject);
  protected
    procedure Loaded; override;
    procedure AddChild(Value: TevDataSource); virtual;
    procedure DeleteChild(Value: TevDataSource); virtual;
    property ChildList: TList read FChildList;
    procedure Notification(AComponent: TComponent;
      Operation: TOperation); override;
  published
    property DataSet write SetDataSet;
    property Active: Boolean read GetActive write SetActive stored False;
    property MasterDataSource: TevDataSource read FMasterDataSource write SetMasterDataSource;
    property OnDataChangeBeforeChildren: TDataChangeEvent read FOnBeforeChildrenDataChange write FOnBeforeChildrenDataChange;
    property OnDataChange: TDataChangeEvent read FOnDataChange write FOnDataChange;
    property OnStateChange: TNotifyEvent read FOnStateChange write FOnStateChange;
  public
    procedure DoDataChange(Sender: TObject; Field: TField); virtual;
    property KeyValue: Integer read GetKeyValue;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


  TevDBLookupCombo = class(TISDBLookupCombo, IevDBFieldControl)
  private
    FDataField: string;
    FSecurityRO: Boolean;
    FPainting: Boolean;
    FLookupTableEditorEnabled: boolean;
    FLookupTableEditorCaption: string;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    FForceVersioning: Boolean;
    FForceVersioningAlternateControl: TControl;
    function GetDataField: string;
    procedure SetDataField(const Value: string);
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
    function GetStyle: TwwDBLookupComboStyle;
    function GetAutoDropDown: Boolean;
    procedure SetAutoDropDown(const Value: Boolean);
    procedure SetStyle(const Value: TwwDBLookupComboStyle);
    procedure EditLookupTable( Sender: TObject );
    function GetLookupTableEditorCaption: string;
    function GetLookupTableEditorFrame: TCLass;
    function OldDisplayValue: string;
    function OldLookupValue: string;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure DoEnter; override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    function  GetPopupMenu: TPopupMenu; override;
    function  GetEnabled: Boolean; override;
    procedure CreateHandle; override;
  public
    procedure CloseUp (modified: boolean); override;
    constructor Create(AOwner: TComponent); override;
    property OldLookup: string read OldLookupValue;
    property OldDisplay: string read OldDisplayValue;
  published
    property DataField read GetDataField write SetDataField;
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property UseTFields default False;
    property ControlInfoInDataSet default False;
    property Style: TwwDBLookupComboStyle read GetStyle write SetStyle;
    property AutoDropDown: Boolean read GetAutoDropDown write SetAutoDropDown;
    property LookupTableEditorEnabled: boolean read FLookupTableEditorEnabled write FLookupTableEditorEnabled default true;
    property LookupTableEditorCaption: string read GetLookupTableEditorCaption write FLookupTableEditorCaption;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
    property ForceVersioning: Boolean read FForceVersioning write FForceVersioning default False;
    property ForceVersioningAlternateControl: TControl read FForceVersioningAlternateControl write FForceVersioningAlternateControl default nil;
  end;


  TevDBRadioGroup = class(TISDBRadioGroup, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;
    FOnSetSecurityRO: EOnSetSecurityRO;
    FGlowing: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    FAutoPopupForceQuartersOnly :Boolean;
    FHasFocus : Boolean;
    procedure SetGlowing(Value: Boolean);
    procedure ActiveChange(Sender: TObject);
    procedure SetSecurityRO(Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    procedure Paint; override;
    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
    procedure SetParent(AParent: TWinControl); override;
    procedure Change; override;
    procedure DoExit; override;
  public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Invalidate; override;
    constructor Create(AOwner: TComponent); override;
    procedure SetMouseUpEventForButtons(proc: TMouseEvent);
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property OnSetSecurityRO: EOnSetSecurityRO read FOnSetSecurityRO write FOnSetSecurityRO;
    property Glowing : Boolean read FGlowing write SetGlowing default False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property AutoPopupForceQuartersOnly: Boolean read FAutoPopupForceQuartersOnly write FAutoPopupForceQuartersOnly default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevGroupBox = class(TISGroupBox)
  protected
    procedure Paint; override;
  end;


  TevDBEdit = class(TISDBEdit, ISecurityElement, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FOldDataChange: TNotifyEvent;
    FAutoPopupEffectiveDateDialog: Boolean;
    FAutoPopupForceQuartersOnly: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure SyncState;
    function  CanHaveMask:boolean;
    function  MatchMask:boolean;
    procedure ToggleMask(Sender: TObject );
    procedure DataChange(Sender: TObject);
    function Component: TComponent;
    function Atom: ISecurityAtom;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetStates: TElementStatesArray;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray;
    procedure SSetCurrentSecurityState(const State: ShortString);
    function SecurityState: ShortString;

    procedure EMSetReadOnly(var Message: TMessage); message EM_SETREADONLY;

    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;

    function  GetPopupMenu: TPopupMenu; override;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure KeyPress(var Key: Char); override;
    function  PreventEdit: boolean; override;
    procedure CreateHandle; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure MouseDown(Button:TMouseButton; Shift:TShiftState; X, Y: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property AutoPopupForceQuartersOnly: Boolean read FAutoPopupForceQuartersOnly write FAutoPopupForceQuartersOnly default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevBitBtn = class(TISBitBtn)
  private
    FSecurityRO: Boolean;
    FOnSetSecurityRO: EOnSetSecurityRO;
    FPainting: Boolean;
    procedure SetSecurityRO(Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
  protected
    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property OnSetSecurityRO: EOnSetSecurityRO read FOnSetSecurityRO write FOnSetSecurityRO;
  end;


  TevSpeedButton = class(TISSpeedButton)
  private
    FSecurityRO: Boolean;
    FEnabled: Boolean;
    FEvInternalRead: Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure SyncState;
  protected
    function  GetEnabled: Boolean; override;
    procedure SetEnabled(Value: Boolean); override;
    procedure Paint; override;
  public
    procedure AfterConstruction; override;
    constructor Create(AOwner: TComponent); override;
    procedure MouseDown(Button:TMouseButton; Shift:TShiftState; X, Y: Integer); override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevDateTimePicker = class(TISDateTimePicker)
  private
    FSecurityRO: Boolean;
    Fpainting: boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure SetBorder(AColor: TColor);
  protected
    function  GetEnabled: Boolean; override;
    procedure CreateHandle; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevLabel = class(TISLabel)
  protected
    procedure Paint; override;
    function  GetLabelText: string; override;
    procedure AdjustBounds; override;
  end;


  TevDBDateTimePicker = class(TISDBDateTimePicker, IevDBFieldControl)
  private
    FSecurityRO: Boolean;
    FUseQuarters: Boolean;
    FUseQuartersOnly: Boolean;
    FAutoPopupEffectiveDateDialog: Boolean;
    FBlockEffectiveDateEditor: Boolean;
    //FDisplayQuarters : Boolean;
    procedure SetSecurityRO(const Value: Boolean);
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure SetUseQuarters(Value: Boolean);
    function GetUseQuarters: Boolean;
    procedure SetUseQuartersOnly(Value: Boolean);
    function GetUseQuartersOnly: Boolean;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMShowEffectiveDateDialog(var Message: TMessage); message CM_SHOW_EFFECTIVE_DATE_DIALOG;
  protected
    //IevDBFieldControl
    function  IsVersioned: Boolean;
    procedure PrepareVersionedLook;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DropDown; override;
    procedure CloseUp(modified: boolean); override;
    procedure ShowQuarterlyDialog;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property UseQuarters: Boolean read GetUseQuarters write SetUseQuarters default False;
    property UseQuartersOnly: Boolean read GetUseQuartersOnly write SetUseQuartersOnly default False;
    property AutoPopupEffectiveDateDialog: Boolean read FAutoPopupEffectiveDateDialog write FAutoPopupEffectiveDateDialog default False;
    property BlockEffectiveDateEditor: Boolean read FBlockEffectiveDateEditor write FBlockEffectiveDateEditor default False;
  end;


  TevButton = class(TISButton)
  private
    FSecurityRO: Boolean;
    FPainting: Boolean;

    procedure SetSecurityRO(const Value: Boolean);
    procedure WMEnable(var Message: TMessage); message WM_ENABLE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure SyncState;
  protected
    procedure CreateHandle; override;
    function  GetEnabled: Boolean; override;
  published
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
  end;


  TevDBGrid = class(TISDBGrid, ISecurityElement)
  private
    FTempKeyOptions: TwwDBGridKeyOptions;
    FSecurityRO: Boolean;
    FSecurityElementsCreated: Boolean;
    FReadOnly: Boolean;
    function GetReadOnly: Boolean;
    function GetKeyOptions: TwwDBGridKeyOptions;
    function IsKeyOptions: boolean;
    procedure SetReadOnly(const Value: Boolean);
    procedure SetKeyOptions(const Value: TwwDBGridKeyOptions);
    procedure SetSecurityRO(const Value: Boolean);

    function Component: TComponent;
    function Atom: ISecurityAtom;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetStates: TElementStatesArray;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray;
    procedure SSetCurrentSecurityState(const State: ShortString);
    function SecurityState: ShortString;
  protected
    function CanEditModify: Boolean; override;
    procedure Loaded; override;

  public
    procedure ForceKeyOptions(const Value: TwwDBGridKeyOptions);
    constructor Create(AOwner: TComponent); override;
    procedure DoCalcCellColors(Field: TField; State: TGridDrawState;
	     highlight: boolean; AFont: TFont; ABrush: TBrush); override;
  published
    property ReadOnly read GetReadOnly write SetReadOnly default True;
    property SecurityRO: Boolean read FSecurityRO write SetSecurityRO stored False;
    property KeyOptions read GetKeyOptions write SetKeyOptions stored IsKeyOptions;
    property UseTFields default False;
    property ControlInfoInDataSet default False;
  end;


  procedure Register;


implementation

uses EvUtils, SDDClasses, SPD_MemoEditor, EvSpellCheck, SFieldCodeValues,  SDataStructure,
     SPD_CategorizedComboDialog, SPD_QuarterlyDateDialog,EvUIUtils, EvConsts, EvSecElement, EvMainboard, rwCallback,
     EvDataAccessComponents, EvVersionedFieldFrm, isUIDBImage, ComCtrls,
  ISKbmMemDataSet;

const
  Cap1Letter: string = '*{&*?[{ ,*'#39'*~}],@}';

type
  TDataSetStates = set of TDataSetState;

  TEditSelectAllFixed = class(TEditSelectAll)
  public
    procedure UpdateTarget(Target: TObject); override;
  end;


  TevVersionedFieldPopupMenu = class(TPopupMenu)
  private
    procedure ShowFieldVersions(Sender: TObject);
    function  CreateFieldVersionsItem(const AOwner: TComponent): TMenuItem;
  public
    procedure AfterConstruction; override;
  end;

var
  FEditUndo: TEditUndo;
  FEditCopy: TEditCopy;
  FEditCut: TEditCut;
  FEditPaste: TEditPaste;
  FEditDelete: TEditDelete;
  FEditSelectAll: TEditSelectAll;
  FVersionedFieldPopupMenu: TevVersionedFieldPopupMenu;
  FLastEffectiveDateDialogFields: IisListOfValues;


procedure Register;
begin
  rwCallBack.Register;
  ISBasicClasses.Register;
  CoolTrayIcon.Register;

  RegisterComponents('Evolution',
      [TEvSpeedButton, TEvButton, TEvLabel, TEvTabControl, TEvPanel, TEvDateTimePicker,
       TEvBitBtn, TEvPageControl, TEvDBText, TEvDBGrid, TevDBCheckGrid, TEvDBEdit, TEvPopupMenu,
       TEvGroupBox, TEvDBRadioGroup, TevDBLookupComboDlg, TevDBLookupCombo,
       TevClientDataSet, TevDataSource, TevDBComboBox, TevDBCheckBox,
       TevDBComboDlg, TevDBDateTimePicker, TevSplitter, TEvBevel, TevCheckBox, TevComboBox,
       TEvDBMemo, TevDBSpinEdit, TevEdit, TevTreeView, TevDBImage, TevActionList,
       TisProvider, TevImage, TevRadioGroup, TevSpinEdit, TevRichEdit, TevDBRichEdit, TevMemo, TevStringGrid,
       TevRadioButton, TevListView, TevCheckListBox, TevToolBar, TevImageList, TevStatusBar, TevDbTreeView,
       TEvBasicClientDataSet,
       TevProxyDataSet,
       TEvQueryBuilderQuery,
       TevListBox
       ]);
  RegisterClass(TevToolButton);
end;


procedure CreateGlobalObjects;
begin
  FEditUndo := TEditUndo.Create(nil);
  FEditUndo.Caption := '&Undo';
  FEditCopy := TEditCopy.Create(nil);
  FEditCopy.Caption := '&Copy';
  FEditCut := TEditCut.Create(nil);
  FEditCut.Caption := 'Cu&t';
  FEditPaste := TEditPaste.Create(nil);
  FEditPaste.Caption := '&Paste';
  FEditDelete := TEditDelete.Create(nil);
  FEditDelete.Caption := '&Delete';
  FEditSelectAll := TEditSelectAllFixed.Create(nil);
  FEditSelectAll.Caption := 'Select &all';

  FVersionedFieldPopupMenu := TevVersionedFieldPopupMenu.Create(nil);
  FLastEffectiveDateDialogFields := TisListOfValues.Create;
end;

procedure FreeGlobalObjects;
begin
  FLastEffectiveDateDialogFields := nil;
  FVersionedFieldPopupMenu.Free;

  FEditUndo.Free;
  FEditCopy.Free;
  FEditCut.Free;
  FEditPaste.Free;
  FEditDelete.Free;
  FEditSelectAll.Free;
end;

procedure CheckColor(const c: TControl; const SecurityRO: Boolean);
var
  NewColor: TColor;
begin
  if SecurityRO then
    NewColor := clBtnFace
  else
    NewColor := clWindow;

  if Supports(c, IevDBFieldControl) and (c as IevDBFieldControl).IsVersioned then
    begin
      if mb_AppSettings['Settings\ShowEffectivePeriods'] = 'Y' then
        NewColor := VersionedControlColor;

      //turned off, as blue version ring not being used now,
      //BUT, may be used later.
      {if IsPublishedProp(c,'Glowing') then
        SetOrdProp(c,'Glowing',1);  }
    end;

  if not (C is TButtonControl) and not (C is TCustomGroupBox) and IsPublishedProp(c, 'Color') then
    SetPropValue(C, 'Color', NewColor);
end;


procedure AddDefaultEditMenu(menu: TPopupMenu);
var
  mi: TMenuItem;

  procedure AddAction(a: TAction);
  begin
    mi := TMenuItem.Create(menu);
    mi.Action := a;
    menu.Items.Add(mi);
  end;

begin
 if assigned(menu) then
   if not assigned(menu.Items.Find( FEditSelectAll.Caption ) ) then
   begin
     menu.Items.NewBottomLine;
     AddAction(FEditUndo);
     menu.Items.NewBottomLine;
     AddAction(FEditCut);
     AddAction(FEditCopy);
     AddAction(FEditPaste);
     AddAction(FEditDelete);
     menu.Items.NewBottomLine;
     AddAction(FEditSelectAll);
     menu.Items.RethinkLines;
   end;
end;


function IsVersionedDBFieldControl(const AControl: TControl): Boolean;
var
  table: String;
  tableClass: TddTableClass;
begin
  Result := False;

  if Supports(AControl, IisDBFieldControl) then
    if (AControl as IisDBFieldControl).DataSource <> nil then
      if (AControl as IisDBFieldControl).DataSource.DataSet is TevClientDataset then
      begin
        table := TevClientDataset((AControl as IisDBFieldControl).DataSource.DataSet).TableName;
        tableClass := GetddTableClassByName(table);
        Result :=  Assigned(tableClass) and tableClass.IsVersionedTable and
           tableClass.IsVersionedField((AControl as IisDBFieldControl).FieldName);
      end;
end;

function CheckEvDSState(const ADataSource: TDataSource; const AState: TDataSetStates): Boolean;
begin
  Result := Assigned(ADataSource) and Assigned(ADataSource.DataSet) and
            (ADataSource.DataSet is TevClientDataSet) and (ADataSource.DataSet.State in AState);
end;

procedure CtrlShowVersionedFieldEditor(const AControl: TControl; const ADataSet: TDataSet; const AField: String);
var
  L: IisStringList;
begin
  if FLastEffectiveDateDialogFields.TryGetValue('DataSet', 0) = Integer(Pointer(ADataSet)) then
  begin
    L := IInterface(FLastEffectiveDateDialogFields.Value['Fields']) as IisStringList;
    if L.IndexOf(AField) <> -1 then
      Exit;
  end;

  ShowVersionedFieldEditor(AControl);
end;


procedure SetControlVersionedPopupMenu(const AControl: TControl);
var
  pm: TPopupMenu;
  mi: TMenuItem;
begin
  if IsPublishedProp(AControl, 'PopupMenu') then
  begin
    pm := TPopupMenu(Pointer(GetOrdProp(AControl, 'PopupMenu')));

    if not Assigned(pm) then
      SetOrdProp(AControl, 'PopupMenu', Integer(Pointer(TevVersionedFieldPopupMenu.Create(aControl))))
    else
    begin
      mi := FVersionedFieldPopupMenu.CreateFieldVersionsItem(pm);
      if mi <> nil then
        pm.Items.Add(mi);
    end;
  end;
end;


procedure SetControlVersionedLook(const AControl: TControl);
begin
    if mb_AppSettings['Settings\ShowEffectivePeriods'] = 'Y' then
      SetOrdProp(AControl, 'Color', VersionedControlColor);
    if Supports(AControl, IevDBFieldControl) and (AControl as IevDBFieldControl).IsVersioned then
    begin
       //NewColor := VersionedControlColor;
      //Keep the following. It may be used in the future.
      {if IsPublishedProp(AControl,'Glowing') then
        SetOrdProp(AControl,'Glowing',1);  }
    end;

  if not IsPublishedProp(AControl, 'BlockEffectiveDateEditor') or (GetOrdProp(AControl, 'BlockEffectiveDateEditor') = 0) then
    SetControlVersionedPopupMenu(AControl);
end;

function IsCaptionWithAsterisk(const AText: String): Boolean;
begin
  Result := (AText <> '' ) and ((AText[1] = '~') or (AText[1] = '*'));
end;

procedure DrawCaptionWithAsterisk(const ACanvas: TCanvas; AText: String; const X, Y: Integer);
var
  TextR: TRect;
  Font: TFont;
begin
  Delete(AText, 1, 1);

  // Calculate caption area rectangle
  TextR := Rect(X, Y, 0, 0);
  DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE or DT_CALCRECT);
  Inc(TextR.Right, 2); //3     reduced to match the * spacing from PERU

  Font := TFont.Create;
  try
    Font.Assign(ACanvas.Font);

    // Draw caption
    ACanvas.Brush.Style := bsClear;
    DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE);

    // Draw asterisk
    AText := '*';
    ACanvas.Font.color := clRed;
    ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
    ACanvas.Font.Size := ACanvas.Font.Size + 2;  //make the asterisk bigger
    TextR.Left := TextR.Right;
    DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE or DT_CALCRECT);
    DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE);

    ACanvas.Font.Assign(Font);
  finally
    Font.Free;
  end;
end;

procedure DrawCaptionWithoutAsterisk(const ACanvas: TCanvas; AText: String; const X, Y: Integer);
var
  TextR: TRect;
  Font: TFont;
begin
  
  // Calculate caption area rectangle
  TextR := Rect(X, Y, 0, 0);
  DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE or DT_CALCRECT);
  Inc(TextR.Right, 3);

  Font := TFont.Create;
  try
    Font.Assign(ACanvas.Font);

    // Draw caption
    ACanvas.Brush.Style := bsClear;
    DrawText(ACanvas.Handle, PChar(AText), Length(AText), TextR, DT_SINGLELINE);

    ACanvas.Font.Assign(Font);
  finally
    Font.Free;
  end;
end;

{ TEditSelectAllFixed }

procedure TEditSelectAllFixed.UpdateTarget(Target: TObject);
begin
  inherited;
  Enabled := Enabled and ( GetControl(Target).SelText <> GetControl(Target).Text );
end;


{ TevRadioButton }

procedure TevRadioButton.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

function TevRadioButton.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevRadioButton.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevRadioButton.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevRadioButton.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevRadioButton.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;

{ TevRadioGroup }

procedure TevRadioGroup.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevRadioGroup.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevRadioGroup.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevRadioGroup.Paint;
var
  OrigTextWidth : Integer;
  TopRect : TRect;
  InnerBodyRect : TRect;
begin
  inherited;

  if IsCaptionWithAsterisk(Caption) then
  begin
    OrigTextWidth := Canvas.TextWidth(Caption);
    //first paint over the top section of the control with the parent's color
    TopRect := ClientRect;
    TopRect.Bottom := TopRect.Top + 6;
    TopRect.Right := TopRect.Right + 1;
    Canvas.Brush.Color := Parent.Brush.Color;
    Canvas.Pen.Style := psClear;
    Canvas.Rectangle(TopRect);

    //Since the innerbody is getting TopRect values, we need a snapshot now
    //as it changes below
    InnerBodyRect.Top := TopRect.Top + 6;
    InnerBodyRect.Right := TopRect.Right - 2;
    InnerBodyRect.Left := TopRect.Left + 2;
    InnerBodyRect.Bottom := TopRect.Top + 7;

    //Do the thin line of parent color, we want the gap between the
    //radio group's frame to be gray, not yellow
    TopRect.Right := TopRect.Left + 8 + OrigTextWidth;
    TopRect.Left := TopRect.Left + 8;  //typical x position starting point for group label
    TopRect.Top := TopRect.Bottom - 2;
    TopRect.Bottom := TopRect.Bottom + 2;
    Canvas.Rectangle(TopRect);

    //now do the body colored section, don't forget to change the brush color
    Canvas.Brush.Color := Color;
    InnerBodyRect.Bottom := InnerBodyRect.Bottom + 7;
    Canvas.Rectangle(InnerBodyRect);

    DrawCaptionWithAsterisk(Canvas, Caption, 8, ClientRect.Top);
  end;
end;

procedure TevRadioGroup.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevRadioGroup.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevRadioGroup.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TevStringGrid }

function TevStringGrid.GetReadOnly: Boolean;
begin
  Result := FReadOnly;
end;

procedure TevStringGrid.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  SyncState;
end;

procedure TevStringGrid.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

procedure TevStringGrid.SyncState;
var
  Value: TGridOptions;
begin
  Value := Options;

  if FReadOnly or FSecurityRO and (goEditing in Value) then
      Exclude(Value, goEditing)
  else
    if (not FSecurityRO) and (not FReadOnly) and (not (goEditing in Value)) then
      Include(Value, goEditing);

  Options := Value;
end;


{ TevMemo }

constructor TevMemo.Create(AOwner: TComponent);
begin
  inherited;
  FSecurityRO := False;
  FSpellcheckEnabled := true;
  FPopupEditorEnabled := true;
end;

procedure TevMemo.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevMemo.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if GetPopupMenu <> nil then
    SetFocus;
  inherited;
end;

procedure TevMemo.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

function TevMemo.GetPopupMenu: TPopupMenu;
begin
  Result := inherited GetPopupMenu;
  Result := EnsureHasSpellcheckItem( FSpellcheckEnabled, Self, Result );
  Result := EnsureHasEditMemoItem( FPopupEditorEnabled, Self, Result );
  AddDefaultEditMenu( Result );
end;

procedure TevMemo.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevMemo.SyncState;
begin
  if HandleAllocated then
    SendMessage(Handle, EM_SETREADONLY, 0, 0);
end;


{ TevRichEdit }

procedure TevRichEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevRichEdit.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

procedure TevRichEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevRichEdit.SyncState;
begin
  if HandleAllocated then
    SendMessage(Handle, EM_SETREADONLY, 0, 0);
end;


{ TevDBRichEdit }

procedure TevDBRichEdit.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBRichEdit.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBRichEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBRichEdit.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

function TevDBRichEdit.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly;
  if FSecurityRO then
    Result := true;
end;

function TevDBRichEdit.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBRichEdit.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBRichEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevDBRichEdit.SyncState;
begin
  if HandleAllocated then
    SendMessage(Handle, EM_SETREADONLY, 0, 0);
end;


{ TevSpinEdit }

procedure TevSpinEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevSpinEdit.DownClick(Sender: TObject);
begin
  if ReadOnly or FSecurityRO then MessageBeep(0)
  else inherited;
end;

procedure TevSpinEdit.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

procedure TevSpinEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevSpinEdit.SyncState;
begin
  if HandleAllocated then
    SendMessage(Handle, EM_SETREADONLY, 0, 0);
end;

procedure TevSpinEdit.UpClick(Sender: TObject);
begin
  if ReadOnly or FSecurityRO then MessageBeep(0)
  else inherited;
end;


{ TevDBImage }

procedure TevDBImage.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

constructor TevDBImage.Create(AOwner: TComponent);
var
  MI: TMenuItem;
begin
  inherited;
  FContextMenu := TPopupMenu.Create(Self);
  MI := TMenuItem.Create(Self);
  MI.Caption := 'Load From File...';
  MI.OnClick := miLoadFromFile;

  FContextMenu.Items.Add(MI);
  MI := TMenuItem.Create(Self);
  MI.Caption := 'Save To File...';
  MI.OnClick := miSaveToFile;
  FContextMenu.Items.Add(MI);

  MI := TMenuItem.Create(Self);
  MI.Caption := 'Clear';
  MI.OnClick := miClear;
  FContextMenu.Items.Add(MI);
end;

procedure TevDBImage.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevDBImage.FieldIsBlob: Boolean;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) then
    Result := TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName)
  else
    Result := inherited FieldIsBlob;
end;

function TevDBImage.GetAutoDisplay: Boolean;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
  begin
    if TevClientDataSet(Field.DataSet).BlobsLoadMode = blmPreload then
      Result := True
    else
      Result := TevClientDataSet(Field.DataSet).IsBlobLoaded(Field.FieldName)
  end
  else
    Result := True;
end;

function TevDBImage.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

function TevDBImage.GetFieldData: IisStream;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
    Result := TevClientDataSet(Field.DataSet).GetBlobData(Field.FieldName)
  else
    Result := inherited GetFieldData;
end;

procedure TevDBImage.CutToClipboard;
begin
  if Picture.Graphic <> nil then
    if FDataLink.Edit then
    begin
      CopyToClipboard;
      Picture.Graphic := nil;

      if Assigned(Field) and
         (Field.DataSet is TevClientDataSet) and
           TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
      TevClientDataSet(Field.DataSet).DeleteFromRefBlobData(Field.FieldName)
    end;
end;

function TevDBImage.GetNotLoadedText: String;
begin
  Result := 'Please DOUBLE-CLICK to load data';
end;

function TevDBImage.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBImage.miLoadFromFile(Sender: TObject);
begin
  ContexMenu_LoadFromFile;
end;

procedure TevDBImage.miClear(Sender: TObject);
begin
  ContexMenu_Clear;
end;

procedure TevDBImage.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
begin
  inherited;

  if not ReadOnly and not FSecurityRO and (Button = mbRight) and
     not Assigned(PopupMenu) and Assigned(Field) and
     Assigned(DataSource) and Assigned(DataSource.DataSet) and
     DataSource.DataSet.Active then
  begin
    P := ClientToScreen(Point(X, Y));
    FContextMenu.PopUp(P.X, P.Y);
  end;
end;

procedure TevDBImage.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBImage.miSaveToFile(Sender: TObject);
begin
  ContexMenu_SaveToFileDialog;
end;

procedure TevDBImage.SetAutoDisplay(Value: Boolean);
begin
end;

procedure TevDBImage.SetFieldData(const AValue: IisStream);
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
    TevClientDataSet(Field.DataSet).UpdateBlobData(Field.FieldName, AValue)
  else
    inherited SetFieldData(AValue);
end;

procedure TevDBImage.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

procedure TevDBImage.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDBImage.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


procedure TevDBImage.ContexMenu_Clear;
begin
  if DataSource.DataSet.State = dsBrowse then
    DataSource.DataSet.Edit;

  SetFieldData(nil);
end;

procedure TevDBImage.ContexMenu_LoadFromFile;
var
  PD: TOpenPictureDialog;
  S: IisStream;
begin
  PD := TOpenPictureDialog.Create(nil);
  try
    PD.Filter := 'Bitmap files (*.bmp)|*.BMP';
    if PD.Execute then
    begin
      CheckFileSize(PD.FileName, 700);
      if DataSource.DataSet.State = dsBrowse then
        DataSource.DataSet.Edit;

      S := TisStream.CreateFromFile(PD.FileName);
      SetFieldData(S);
    end;
  finally
    PD.Free;
  end;
end;

procedure TevDBImage.ContexMenu_SaveToFileDialog;
var
  PD: TSavePictureDialog;
begin
  PD := TSavePictureDialog.Create(nil);
  try
    PD.Filter := 'Bitmap files (*.bmp)|*.BMP';
    if PD.Execute then
      Picture.SaveToFile(ChangeFileExt(PD.FileName, '.bmp'));
  finally
    PD.Free;
  end;
end;

procedure TevDBImage.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBImage.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

{ TevTreeView }

procedure TevTreeView.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevTreeView.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SynCState;
  CheckColor(Self, Value);
end;

procedure TevTreeView.SyncState;
begin
  ReadOnly := not ReadOnly;
  ReadOnly := not ReadOnly;
end;

procedure TevTreeView.WMStyleChanging(var Message: TMessage);
var
  StyleStruct: PStyleStruct;
begin
  // check if it is ReadOnly change
  if FSecurityRO then
    if Message.WParam = GWL_STYLE then
    begin
      StyleStruct := PStyleStruct(Pointer(Message.lParam));
      if (StyleStruct.styleOld and TVS_EDITLABELS) <> (StyleStruct.styleNew and TVS_EDITLABELS) then
        StyleStruct.styleNew := StyleStruct.styleNew and not TVS_EDITLABELS;
    end;

  inherited;
end;


{ TevEdit }

procedure TevEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevEdit.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

procedure TevEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevEdit.SyncState;
begin
  if HandleAllocated then
    SendMessage(Handle, EM_SETREADONLY, 0, 0);
end;


{ TevDBSpinEdit }

procedure TevDBSpinEdit.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBSpinEdit.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBSpinEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBSpinEdit.DownClick(Sender: TObject);
begin
  if not FSecurityRO then
    inherited;
end;

procedure TevDBSpinEdit.EMSetReadOnly(var Message: TMessage);
begin
  Message.WParam := Ord(FSecurityRO or ReadOnly);
  inherited;
end;

function TevDBSpinEdit.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBSpinEdit.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBSpinEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SynCState;
end;

procedure TevDBSpinEdit.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDBSpinEdit.UpClick(Sender: TObject);
begin
  if not FSecurityRO then
    inherited;
end;

procedure TevDBSpinEdit.WMPaste(var Message: TMessage);
begin
  if not FSecurityRO then
    inherited;
end;


{ TEvDBMemo }

procedure TEvDBMemo.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TEvDBMemo.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

constructor TEvDBMemo.Create(AOwner: TComponent);
begin
  inherited;
  FSpellcheckEnabled := true;
  FPopupEditorEnabled := true;
end;

procedure TEvDBMemo.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TEvDBMemo.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if GetPopupMenu <> nil then
    SetFocus;
  inherited;
end;

procedure TEvDBMemo.EMSetReadOnly(var Message: TMessage);
begin
  if FSecurityRO then
    Message.WParam := 1;
  inherited;
end;

function TEvDBMemo.FieldIsBlob: Boolean;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) then
    Result := TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName)
  else
    Result := inherited FieldIsBlob;
end;

function TEvDBMemo.GetAutoDisplay: Boolean;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
  begin
    if TevClientDataSet(Field.DataSet).BlobsLoadMode = blmPreload then
      Result := True
    else
      Result := TevClientDataSet(Field.DataSet).IsBlobLoaded(Field.FieldName)
  end
  else
    Result := True;
end;

function TEvDBMemo.GetFieldText: String;
var
  S: IisStream;
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
  begin
    S := TevClientDataSet(Field.DataSet).GetBlobData(Field.FieldName);
    if Assigned(S) then
      Result := S.AsString
    else
      Result := '';
  end
  else
    Result := inherited GetFieldText;
end;

function TEvDBMemo.GetNotLoadedText: String;
begin
  Result := 'Please DOUBLE-CLICK to load data';
end;

function TEvDBMemo.GetPopupMenu: TPopupMenu;
begin
  Result := inherited GetPopupMenu;
  Result := EnsureHasSpellcheckItem( FSpellcheckEnabled, Self, Result );
  Result := EnsureHasEditMemoItem( FPopupEditorEnabled, Self, Result );
  AddDefaultEditMenu( Result );
end;

function TEvDBMemo.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TEvDBMemo.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if FSecurityRO and ((Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift))) then
    Key := 0;

  inherited;
end;

procedure TEvDBMemo.KeyPress(var Key: Char);
begin
  if FSecurityRO and (Key in [^H, ^I, ^J, ^M, ^V, ^X, #32..#255]) then
    Key := #0;

  inherited;
end;

procedure TEvDBMemo.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TEvDBMemo.SetAutoDisplay(Value: Boolean);
begin
end;

procedure TEvDBMemo.SetFieldText(const AValue: String);
begin
  if Assigned(Field) and (Field.DataSet is TevClientDataSet) and TevClientDataSet(Field.DataSet).IsEvBlobField(Field.FieldName) then
  begin
    TevClientDataSet(Field.DataSet).UpdateBlobData(Field.FieldName, AValue);
  end
  else
    inherited SetFieldText(AValue);
end;

procedure TEvDBMemo.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TEvDBMemo.SyncState;
begin
  if HandleAllocated then
    if FSecurityRO then
      SendMessage(Handle, EM_SETREADONLY, 1, 0)
    else
      SendMessage(Handle, EM_SETREADONLY, Ord(ReadOnly), 0);
end;


{ TevCheckBox }

procedure TevCheckBox.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevCheckBox.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevCheckBox.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevCheckBox.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevCheckBox.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevCheckBox.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TevComboBox }

constructor TevComboBox.Create(AOwner: TComponent);
begin
  inherited;
  inherited Style := stdctrls.csDropDownList;
  inherited BevelKind := bkFlat;
end;

function TevComboBox.GetStyle: TComboBoxStyle;
begin
  Result := inherited Style;
end;

procedure TevComboBox.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

procedure TevComboBox.SetMyStyle(const Value: TComboBoxStyle);
begin
  //empty!!
end;

function TevComboBox.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevComboBox.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevComboBox.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevComboBox.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevComboBox.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TevDBComboDlg }

procedure TevDBComboDlg.ActiveChange(Sender: TObject);
var
  i: Integer;

  function CreateIPPictureMask(const ComboChoices: string): string;
  var
    t: TStringList;
    i: Integer;
  begin
    t := TStringList.Create;
    try
      t.Text := ComboChoices;
      Result := '';
      for i := 0 to Pred(t.Count) do
      begin
        Result := Result + Copy(t[i], Succ(Pos(#9, t[i])), Length(t[i]));
        if i <> Pred(t.Count) then
          Result := Result + ',';
      end;
      result := '[' + Result + ']';
    finally
      t.Free;
    end;
  end;

begin
  if (DataLink.DataSet is TddTable) and
     DataLink.DataSet.Active then
    for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      if (FieldsToPopulate[i].F = DataField) and
         ((FieldsToPopulate[i].T = DataLink.DataSet.ClassType) or
          (Copy(TevClientDataSet(DataLink.DataSet).TableName, 1, 4) = 'TMP_') and
          (FieldsToPopulate[i].T = GetddTableClassByTempTableClass(TddTableClass(DataLink.DataSet.ClassType)))) then
    begin
      Picture.PictureMask := CreateIPPictureMask(FieldsToPopulate[i].C);
      Picture.AutoFill := False;
      Break;
    end;
end;

constructor TevDBComboDlg.Create(AOwner: TComponent);
begin
  inherited;
  DataLink.OnActiveChange := ActiveChange;
  Picture.PictureMaskFromDataSet := False;
end;

procedure TevDBComboDlg.DropDown;
begin
  if ReadOnly or FSecurityRO then
    Exit;

  if Assigned(DataLink.DataSet) and
     (DataLink.DataSet is TevClientDataSet) and
     DataLink.DataSet.Active and
     (DataLink.DataSet.State = dsBrowse) then
    DataLink.DataSet.Edit;
  if Assigned(OnCustomDlg) then
    inherited
  else
    ShowCategorizedComboDialog;
end;

procedure TevDBComboDlg.ShowCategorizedComboDialog;
var
  i: Integer;
begin
  if Assigned(DataLink.DataSet) and
     (DataLink.DataSet is TddTable) and
     DataLink.DataSet.Active then
    for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      if (FieldsToPopulate[i].F = DataField) and
         ((FieldsToPopulate[i].T = DataLink.DataSet.ClassType) or
          (Copy(TevClientDataSet(DataLink.DataSet).TableName, 1, 4) = 'TMP_') and
          (FieldsToPopulate[i].T = GetddTableClassByTempTableClass(TddTableClass(DataLink.DataSet.ClassType)))) then
      begin
        with TCategorizedComboDialog.Create(self) do
        begin
          Setup(FieldsToPopulate[i].c, FieldsToPopulate[i].g, Text);
          if ShowModal = idOk then
          begin
            Text := SelectedCode;
            if DataLink.DataSet.State = dsBrowse then
              DataLink.DataSet.Edit;
            DataLink.Field.AsString := Text;
          end;
          Free;
        end;
        SetFocus;
        Break;
      end;
end;

procedure TevDBComboDlg.SetSecurityRO(Value: Boolean);
var
  Accept: Boolean;
begin
  if Assigned(FOnSetSecurityRO) then
  begin
    Accept := True;
    FOnSetSecurityRO(Self, Value, Accept);
    if not Accept then Exit;
  end;
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevDBComboDlg.SyncState;
begin
  if HandleAllocated then
    if FSecurityRO then
      SendMessage(Handle, EM_SETREADONLY, 1, 0)
    else
      SendMessage(Handle, EM_SETREADONLY, Ord(ReadOnly), 0);
end;

procedure TevDBComboDlg.EMSetReadOnly(var Message: TMessage);
begin
  if FSecurityRO then
    Message.WParam := 1;
  inherited;
end;

procedure TevDBComboDlg.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBComboDlg.WMPaste(var Message: TMessage);
begin
  if not FSecurityRO then
    inherited;
end;


function TevDBComboDlg.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBComboDlg.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBComboDlg.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBComboDlg.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBComboDlg.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;

end;

procedure TevDBComboDlg.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;

end;

{ TevDBComboBox }

constructor TevDBComboBox.Create(AOwner: TComponent);
begin
  inherited;
  DataLink.OnActiveChange := ActiveChange;
  inherited AutoDropDown := True;
  inherited Style := stdctrls.csDropDownList;
  Picture.PictureMaskFromDataSet := False;
end;

procedure TevDBComboBox.ActiveChange(Sender: TObject);
var
  i: Integer;
begin
  if (DataLink.DataSet is TddTable) and
     DataLink.DataSet.Active then
    for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      if (FieldsToPopulate[i].F = DataField) and
         ((FieldsToPopulate[i].T = DataLink.DataSet.ClassType) or
          (Copy(TevClientDataSet(DataLink.DataSet).TableName, 1, 4) = 'TMP_') and
          (FieldsToPopulate[i].T = GetddTableClassByTempTableClass(TddTableClass(DataLink.DataSet.ClassType)))) then
    begin
      AllowClearKey := not DataLink.DataSet.FieldByName(DataField).Required;
      Items.Text := FieldsToPopulate[i].C;
      MapList := True;
      DataLink.OnDataChange(Self);
      Break;
    end;
end;

procedure TevDBComboBox.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

function TevDBComboBox.GetPopupMenu: TPopupMenu;
begin
  Result := inherited GetPopupMenu;
  AddDefaultEditMenu( Result );
end;

function TevDBComboBox.GetAutoDropDown: Boolean;
begin
  Result := inherited AutoDropDown;
end;

function TevDBComboBox.GetStyle: TComboBoxStyle;
begin
  Result := inherited Style;
end;

procedure TevDBComboBox.SetAutoDropDown(const Value: Boolean);
begin
  //empty!!!
end;

procedure TevDBComboBox.SetStyle(const Value: TComboBoxStyle);
begin
  inherited Style := Value;
end;

procedure TevDBComboBox.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

function TevDBComboBox.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevDBComboBox.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevDBComboBox.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;

procedure TevDBComboBox.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBComboBox.WMPaste(var Message: TMessage);
begin
  if not FSecurityRO then
    inherited;
end;


function TevDBComboBox.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBComboBox.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBComboBox.CMEnter(var Message: TCMEnter);
begin
  inherited;

 { if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear; }
end;

procedure TevDBComboBox.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if FAlreadyFired then exit;
  if DontAutoPopupEffectiveDateDialog then exit;
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
  begin
    if  ssShift in Shift then exit;
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0);
    FAlreadyFired := True;
  end
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;


end;



procedure TevDBComboBox.DropDown;
begin
  if FAlreadyFired then exit;
  if DontAutoPopupEffectiveDateDialog then exit;
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
  begin
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0);
    FAlreadyFired := True;
  end
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;

end;

procedure TevDBComboBox.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if FAlreadyFired then exit;
  if DontAutoPopupEffectiveDateDialog then exit;
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
  begin
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0);
    FAlreadyFired := True;
  end
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;

end;



procedure TevDBComboBox.CloseUp(Accept: Boolean);
begin
  inherited;
  FAlreadyFired := False;
end;


{ TevDBLookupComboDlg }

procedure TevDBLookupComboDlg.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBLookupComboDlg.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

constructor TevDBLookupComboDlg.Create(AOwner: TComponent);
begin
  inherited;
  UseTFields := False;
  ControlInfoInDataSet := False;
end;

procedure TevDBLookupComboDlg.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBLookupComboDlg.DropDown;
begin
  if not FSecurityRO then
    inherited;
end;

procedure TevDBLookupComboDlg.EMSetReadOnly(var Message: TMessage);
begin
  if FSecurityRO then
    Message.WParam := 1;
  inherited;
end;

function TevDBLookupComboDlg.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBLookupComboDlg.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBLookupComboDlg.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

procedure TevDBLookupComboDlg.SyncState;
begin
  if HandleAllocated then
    if FSecurityRO then
      SendMessage(Handle, EM_SETREADONLY, 1, 0)
    else
      SendMessage(Handle, EM_SETREADONLY, Ord(ReadOnly), 0);
end;

procedure TevDBLookupComboDlg.WMPaste(var Message: TMessage);
begin
  if not FSecurityRO then
    inherited;
end;


{ TevDBCheckBox }

procedure TevDBCheckBox.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBCheckBox.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBCheckBox.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevDBCheckBox.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevDBCheckBox.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

function TevDBCheckBox.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBCheckBox.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBCheckBox.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevDBCheckBox.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDBCheckBox.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TevDataSource }

procedure TevDataSource.AddChild(Value: TevDataSource);
begin
  if Assigned(FChildList) then
    if FChildList.IndexOf(Value) = -1 then
    begin
      FChildList.Add(Value);
      if Assigned(DataSet) then
        DoDataChange(Self, nil);
    end;
end;

constructor TevDataSource.Create(AOwner: TComponent);
begin
  inherited;
  FPriorState := State;
  FChildList := TList.Create;
  inherited OnDataChange := DoDataChange;
  inherited OnStateChange := DoStateChange;
  FKeyValue := 0;
end;

procedure TevDataSource.DeleteChild(Value: TevDataSource);
begin
  if Assigned(FChildList) then
    if FChildList.IndexOf(Value) >= 0 then
      FChildList.Delete(FChildList.IndexOf(Value));
end;

destructor TevDataSource.Destroy;
begin
  if Assigned(MasterDataSource) and
     Assigned(DataSet) and
     (DataSet is TevClientDataSet) and
     not (csDestroying in DataSet.ComponentState) then
    TevClientDataSet(DataSet).RetrieveCondition := '';

  FChildList.Free;
  FChildList := nil;
  inherited;
end;

procedure TevDataSource.DoDataChange(Sender: TObject; Field: TField);
var
  i: Integer;
  sTableName: string;
begin
  if csDestroying in ComponentState then
    Exit;
  if csDesigning in ComponentState then
    Exit;

  if (csLoading in ComponentState) and not Assigned(Field) then
  begin
    FDataChangedOnLoading := True;
    Exit;
  end;

  if not (csDesigning in ComponentState) and
     Assigned(FOnBeforeChildrenDataChange) and
     Assigned(DataSet) and
     DataSet.Active then
    FOnBeforeChildrenDataChange(Sender, Field);

  sTableName := '';
  if Assigned(DataSet) and
     (DataSet is TevClientDataSet) then
    if Copy(TevClientDataSet(DataSet).TableName, 1, 4) = 'TMP_' then
      sTableName := Copy(TevClientDataSet(DataSet).TableName, 5, Length(TevClientDataSet(DataSet).TableName))
    else
      sTableName := TevClientDataSet(DataSet).TableName;

  if (sTableName <> '') and
     (not Assigned(Field) or
      (UpperCase(sTableName + '_NBR') = UpperCase(Field.FieldName))) then
  begin
    if DataSet.Active and
       Assigned(DataSet.FindField(sTableName + '_NBR')) then
      FKeyValue := DataSet.FindField(sTableName + '_NBR').AsInteger;
    for i := 0 to Pred(FChildList.Count) do
      with TevDataSource(FChildList[i]) do
        if Assigned(DataSet) and
           not (csDestroying in DataSet.ComponentState) and
           (DataSet is TevClientDataSet) then
        begin
          TevClientDataSet(DataSet).ClientID := TevClientDataSet(Self.DataSet).ClientID;
          if Self.DataSet.Active and
             Assigned(Self.DataSet.FindField(sTableName + '_NBR')) and
             Assigned(DataSet.FindField(sTableName + '_NBR')) then
            TevClientDataSet(DataSet).RetrieveCondition := sTableName + '_NBR = ' + IntToStr(Self.KeyValue);
        end;
  end;
  if not (csDesigning in ComponentState) and
     Assigned(FOnDataChange) and
     Assigned(DataSet) and
     DataSet.Active then
    FOnDataChange(Sender, Field);

  for I := DataLinks.Count - 1 downto 0 do
    if TObject(DataLinks[i]) is TFieldDataLink then
      with TFieldDataLink(DataLinks[I]) do
        if Control is TevDBLookupCombo then
          OnDataChange(Sender);
end;

procedure TevDataSource.DoStateChange(Sender: TObject);
begin
  if (FPriorState = dsInactive) and
     (State <> dsInactive) and
     not (csDesigning in ComponentState) and
     Assigned(DataSet) and
     Assigned(MasterDataSource) and
     Assigned(MasterDataSource.DataSet) and
     not (csDestroying in MasterDataSource.ComponentState) then
    MasterDataSource.DoDataChange(Self, nil);
  FPriorState := State;
  if Assigned(FOnStateChange) then
    FOnStateChange(Sender);
end;

function TevDataSource.GetActive: Boolean;
begin
  Result := Assigned(DataSet) and DataSet.Active;
end;

function TevDataSource.GetKeyValue: Integer;
begin
  if Active then
    Result := FKeyValue
  else
    Result := 0;
end;

procedure TevDataSource.Loaded;
begin
  inherited;

  if FDataChangedOnLoading then
  begin
    FDataChangedOnLoading := False;
    DoDataChange(Self, nil);
  end;
end;

procedure TevDataSource.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (AComponent = FMasterDataSource) and
     (Operation = opRemove) then
  begin
    FMasterDataSource := nil;
    if Assigned(DataSet) and
       (DataSet is TevClientDataSet) and
       not (csDestroying in DataSet.ComponentState) then
      TevClientDataSet(DataSet).RetrieveCondition := '';
  end;
  if (AComponent is TevDataSource) and
     (Operation = opRemove) then
    DeleteChild(AComponent as TevDataSource);
end;

procedure TevDataSource.SetActive(const Value: Boolean);
begin
  if Assigned(DataSet) then
    DataSet.Active := Value;
end;

procedure TevDataSource.SetDataSet(const Value: TDataSet);
begin
  inherited DataSet := Value;
  if not (csDesigning in ComponentState) and
     Assigned(Value) and
     Assigned(MasterDataSource) and
     Assigned(MasterDataSource.DataSet) then
    MasterDataSource.DoDataChange(Self, nil);
end;

procedure TevDataSource.SetMasterDataSource(const Value: TevDataSource);
begin
  if FMasterDataSource <> Value then
  begin
    if not (csDesigning in ComponentState) and Assigned(FMasterDataSource) and
       not (csDestroying in MasterDataSource.ComponentState) then
      FMasterDataSource.DeleteChild(Self);
    if not (csDesigning in ComponentState) and not Assigned(Value) and Assigned(DataSet) and
       not (csDestroying in DataSet.ComponentState) then
      TevClientDataSet(DataSet).RetrieveCondition := '';
    FMasterDataSource := Value;
    if not (csDesigning in ComponentState) and Assigned(Value) then
      Value.AddChild(Self);
  end;
end;


{ TevDBLookupCombo }

constructor TevDBLookupCombo.Create(AOwner: TComponent);
begin
  inherited;
  inherited Style := csDropDownList;
  inherited AutoDropDown := True;
  UseTFields := False;
  ControlInfoInDataSet := False;
  FLookupTableEditorEnabled := true;
end;

function TevDBLookupCombo.GetDataField: string;
begin
  if not (Parent is TevDBGrid) then
    Result := inherited DataField
  else
    Result := FDataField;
end;

procedure TevDBLookupCombo.SetDataField(const Value: string);
begin
  if not (Parent is TevDBGrid) then
    inherited DataField := Value
  else
    FDataField := Value;
end;

procedure TevDBLookupCombo.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

function TevDBLookupCombo.GetStyle: TwwDBLookupComboStyle;
begin
  Result := inherited Style;
end;

function TevDBLookupCombo.GetAutoDropDown: Boolean;
begin
  Result := inherited AutoDropDown;
end;

procedure TevDBLookupCombo.SetAutoDropDown(const Value: Boolean);
begin
  //empty!!!
end;

procedure TevDBLookupCombo.SetStyle(const Value: TwwDBLookupComboStyle);
begin
  //empty!!!
end;

procedure TevDBLookupCombo.DoEnter;
begin
  if Assigned(DataSource) and Assigned(DataSource.DataSet) then
    AllowClearKey := not DataSource.DataSet.FieldByName(DataField).Required;
  inherited;
end;

function TevDBLookupCombo.GetPopupMenu: TPopupMenu;
begin
  Result := inherited GetPopupMenu;
  Result := EnsureHasMenuItem( FLookupTableEditorEnabled and ( GetLookupTableEditorFrame <> nil ), Self, Result, EditLookupTable, LookupTableEditorCaption, '', false, true );
  AddDefaultEditMenu( Result );
end;

procedure TevDBLookupCombo.DoContextPopup(MousePos: TPoint;
  var Handled: Boolean);
begin
  if GetPopupMenu <> nil then
    SetFocus;
  inherited;
end;

function TevDBLookupCombo.GetLookupTableEditorCaption: string;
begin
  Result := FLookupTableEditorCaption;
  if (Result = '') and not (csDesigning in ComponentState ) then
    Result := 'Edit lookup table';
end;


procedure TevDBLookupCombo.EditLookupTable(Sender: TObject);
var
  comp: TComponent;
  handler: IevGlobalLookupHandler;
begin
  comp := Self;
  handler := nil;
  while assigned(comp) do
  begin
    if comp.GetInterface( IevGlobalLookupHandler, handler ) then
      break;
    comp := comp.Owner;
  end;
  Assert( assigned(handler), 'IevGlobalLookupHandler implementation not found in owners chain' );
  if assigned(handler) then
    handler.HandleGlobalLookupRequest( GetLookupTableEditorFrame )
end;

function TevDBLookupCombo.GetLookupTableEditorFrame: TClass;
var
  lf: string;
begin
  Result := nil;
  if assigned(LookupTable) then
    Result := GetFrameClassForTable(LookupTable.Name);
  if Result = nil then
  begin
    lf := trim(LookupField);
    if ( Pos( '_NBR', lf) = Length(lf)-3) then
      Result := GetFrameClassForTable( Copy( lf, 1, Pos('_NBR', lf)-1) );
  end
end;

function TevDBLookupCombo.OldDisplayValue: string;
begin
end;

function TevDBLookupCombo.OldLookupValue: string;
begin
end;

procedure TevDBLookupCombo.CloseUp(modified: boolean);
begin
  inherited;
  if (Parent is TevDBGrid) and
     Assigned(DataSource) and
     Assigned(DataSource.DataSet) and
     Assigned(DataSource.DataSet.FindField(DataField)) then
    Text := DataSource.DataSet.FindField(DataField).AsString;
end;

function TevDBLookupCombo.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevDBLookupCombo.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDBLookupCombo.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevDBLookupCombo.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBLookupCombo.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


function TevDBLookupCombo.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and (IsVersionedDBFieldControl(Self) or FForceVersioning);
end;

procedure TevDBLookupCombo.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBLookupCombo.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBLookupCombo.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

{ TEvDBRadioGroup }

procedure TevDBRadioGroup.ActiveChange(Sender: TObject);
var
  i, j: Integer;
  t: TStringList;
begin
  if (DataLink.DataSet is TddTable) and
     DataLink.DataSet.Active then
    for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
      if (FieldsToPopulate[i].F = DataField) and
         ((FieldsToPopulate[i].T = DataLink.DataSet.ClassType) or
          (Copy(TevClientDataSet(DataLink.DataSet).TableName, 1, 4) = 'TMP_') and
          (FieldsToPopulate[i].T = GetddTableClassByTempTableClass(TddTableClass(DataLink.DataSet.ClassType)))) then
    begin
      t := TStringList.Create;
      try
        t.Text := FieldsToPopulate[i].C;
        Items.Clear;
        Values.Clear;
        for j := 0 to Pred(t.Count) do
        begin
          Items.Add(Copy(t[j], 1, Pred(Pos(#9, t[j]))));
          Values.Add(Copy(t[j], Succ(Pos(#9, t[j])), Length(t[j])));
        end;
        ItemIndex := Values.IndexOf(DataLink.DataSet.FieldByName(DataField).AsString);
      finally
        t.Free;
      end;
      Break;
    end;
end;

procedure TevDBRadioGroup.Change;
begin
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsEdit]) and FHasFocus then
  begin
    Self.DataSource.DataSet.Cancel;
    CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
  end
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    inherited;
  end;

end;

procedure TevDBRadioGroup.CMEnter(var Message: TCMEnter);
begin
  inherited;
  FHasFocus := True;
end;

procedure TevDBRadioGroup.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBRadioGroup.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

constructor TEvDBRadioGroup.Create(AOwner: TComponent);
begin
  inherited;
  DataLink.OnActiveChange := ActiveChange;
  FGlowing := False;
end;

procedure TevDBRadioGroup.CreateHandle;
begin
  inherited;
  SyncState;
end;

procedure TevDBRadioGroup.DoExit;
begin
  inherited;
  FHasFocus := False;
end;

function TevDBRadioGroup.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevDBRadioGroup.Invalidate;
begin
  TisGlowBorder.Erase(Self);
  inherited;
end;

function TevDBRadioGroup.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TEvDBRadioGroup.Paint;
var
  OrigTextWidth : Integer;
  TopRect : TRect;
  InnerBodyRect : TRect;
begin
  inherited;
  if IsCaptionWithAsterisk(Caption) then
  begin
    OrigTextWidth := Canvas.TextWidth(Caption);
    //first paint over the top section of the control with the parent's color
    TopRect := ClientRect;
    TopRect.Bottom := TopRect.Top + 6;
    TopRect.Right := TopRect.Right + 1;
    Canvas.Brush.Color := Parent.Brush.Color;
    Canvas.Pen.Style := psClear;
    Canvas.Rectangle(TopRect);

    //Since the innerbody is getting TopRect values, we need a snapshot now
    //as it changes below
    InnerBodyRect.Top := TopRect.Top + 6;
    InnerBodyRect.Right := TopRect.Right - 2;
    InnerBodyRect.Left := TopRect.Left + 2;
    InnerBodyRect.Bottom := TopRect.Top + 7;

    //Do the thin line of parent color, we want the gap between the
    //radio group's frame to be gray, not yellow

    TopRect.Right := TopRect.Left + 8 + OrigTextWidth;
    TopRect.Left := TopRect.Left + 8;  //typical x position starting point for group label
    TopRect.Top := TopRect.Bottom - 2;
    TopRect.Bottom := TopRect.Bottom + 2;
    Canvas.Rectangle(TopRect);



    //now do the body colored section, don't forget to change the brush color
    Canvas.Brush.Color := color;
    //increased the top by 1 because we were blocking the thin white inside line.
    InnerBodyRect.Top := InnerBodyRect.Top + 1;    
    InnerBodyRect.Bottom := InnerBodyRect.Bottom + 7;
    Canvas.Rectangle(InnerBodyRect);

    //now we need the white inside line back

    DrawCaptionWithAsterisk(Canvas, Caption, 8, ClientRect.Top);
    if FGlowing then
    begin
      //draw the blue glowing line
      Canvas.Pen.Color := ControlGlowColor;
      Canvas.Pen.Style := psSolid;
      Canvas.MoveTo(ClientRect.Left, TopRect.Top - 1);
      Canvas.LineTo(ClientRect.Left + 8 , TopRect.Top - 1);

      Canvas.MoveTo(TopRect.Right + 1, TopRect.Top - 1);
      Canvas.LineTo(ClientRect.BottomRight.x , TopRect.Top - 1);
    end;
  end
  else
  begin
    OrigTextWidth := Canvas.TextWidth(Caption);
    //first paint over the top section of the control with the parent's color
    TopRect := ClientRect;
    TopRect.Bottom := TopRect.Top + 6;
    TopRect.Right := TopRect.Right + 1;
    Canvas.Brush.Color := Parent.Brush.Color;
    Canvas.Pen.Style := psClear;
    Canvas.Rectangle(TopRect);

    //Since the innerbody is getting TopRect values, we need a snapshot now
    //as it changes below
    InnerBodyRect.Top := TopRect.Top + 6;
    InnerBodyRect.Right := TopRect.Right - 2;
    InnerBodyRect.Left := TopRect.Left + 2;
    InnerBodyRect.Bottom := TopRect.Top + 7;

    //Do the thin line of parent color, we want the gap between the
    //radio group's frame to be gray, not yellow
    TopRect.Right := TopRect.Left + 8 + OrigTextWidth;
    TopRect.Left := TopRect.Left + 8;  //typical x position starting point for group label
    TopRect.Top := TopRect.Bottom - 2;
    TopRect.Bottom := TopRect.Bottom + 2;
    Canvas.Rectangle(TopRect);



    //now do the body colored section, don't forget to change the brush color
    Canvas.Brush.Color := Color;
    //increased the top by 1 because we were blocking the thin white inside line.
    InnerBodyRect.Top := InnerBodyRect.Top + 1; 
    InnerBodyRect.Bottom := InnerBodyRect.Bottom + 7;
    Canvas.Rectangle(InnerBodyRect);

    DrawCaptionWithoutAsterisk(Canvas, Caption, 8, ClientRect.Top);
    if FGlowing then
    begin
      //draw the blue glowing line
      Canvas.Pen.Color := ControlGlowColor;
      Canvas.Pen.Style := psSolid;
      Canvas.MoveTo(ClientRect.Left, TopRect.Top - 1);
      Canvas.LineTo(ClientRect.Left + 8 , TopRect.Top - 1);

      Canvas.MoveTo(TopRect.Right + 1, TopRect.Top - 1);
      Canvas.LineTo(ClientRect.BottomRight.x , TopRect.Top - 1);
    end;  
  end;
  if FGlowing then
  begin
    if ((Self.Color <> VersionedControlColor)) then
      Self.ParentColor := True;
    TisGlowBorder.DrawRadioGroup(self, TopRect.Left + 10 + OrigTextWidth);
  end;

end;

procedure TevDBRadioGroup.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBRadioGroup.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  // Erase previous position border
  if FGlowing then
    TisGlowBorder.Erase(Self);
  inherited;
  Invalidate;
end;

procedure TevDBRadioGroup.SetGlowing(Value: Boolean);
begin
  if FGlowing <> Value then
  begin
    FGlowing := Value;
    Invalidate;
  end;
end;

procedure TevDBRadioGroup.SetMouseUpEventForButtons(proc: TMouseEvent);
var
  i: Integer;
begin
  for i := 0 to Pred(ControlCount) do
    if Controls[i] is TRadioButton then
      TRadioButton(Controls[i]).OnMouseUp := proc;
end;

procedure TevDBRadioGroup.SetParent(AParent: TWinControl);
begin
  if (Parent <> AParent) and FGlowing then
    TisGlowBorder.Erase(Self);
    
  inherited;
end;

procedure TevDBRadioGroup.SetSecurityRO(Value: Boolean);
var
  Accept: Boolean;
begin
  if Assigned(FOnSetSecurityRO) then
  begin
    Accept := True;
    FOnSetSecurityRO(Self, Value, Accept);
    if not Accept then Exit;
  end;
  FSecurityRO := Value;
  SyncState;
end;

procedure TevDBRadioGroup.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDBRadioGroup.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TEvGroupBox }

procedure TEvGroupBox.Paint;
var
  OrigTextWidth : Integer;
  TopRect : TRect;
  InnerBodyRect : TRect;
begin
  inherited;

  if IsCaptionWithAsterisk(Caption) then
  begin
    OrigTextWidth := Canvas.TextWidth(Caption);
    //first paint over the top section of the control with the parent's color
    TopRect := ClientRect;
    TopRect.Bottom := TopRect.Top + 6;
    TopRect.Right := TopRect.Right + 1;
    Canvas.Brush.Color := Parent.Brush.Color;
    Canvas.Pen.Style := psClear;
    Canvas.Rectangle(TopRect);

    //Since the innerbody is getting TopRect values, we need a snapshot now
    //as it changes below
    InnerBodyRect.Top := TopRect.Top + 6;
    InnerBodyRect.Right := TopRect.Right - 2;
    InnerBodyRect.Left := TopRect.Left + 2;
    InnerBodyRect.Bottom := TopRect.Top + 7;

    //Do the thin line of parent color, we want the gap between the
    //radio group's frame to be gray, not yellow
    TopRect.Right := TopRect.Left + 8 + OrigTextWidth;
    TopRect.Left := TopRect.Left + 8;  //typical x position starting point for group label
    TopRect.Top := TopRect.Bottom - 2;
    TopRect.Bottom := TopRect.Bottom + 2;
    Canvas.Rectangle(TopRect);

    //now do the body colored section, don't forget to change the brush color
    Canvas.Brush.Color := Color;
    InnerBodyRect.Bottom := InnerBodyRect.Bottom + 7;
    Canvas.Rectangle(InnerBodyRect);

    DrawCaptionWithAsterisk(Canvas, Caption, 8, ClientRect.Top);
  end;
end;

{ TevDBEdit }

const
  sAutoCapitalize = 'Capitalize first letter';

procedure TevDBEdit.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
begin
  if GetPopupMenu <> nil then
    SetFocus;
  inherited;
end;

function TevDBEdit.GetPopupMenu: TPopupMenu;
begin
  Result := inherited GetPopupMenu;
  if not FSecurityRO then
  begin
    Result := EnsureHasMenuItem( CanHaveMask and ( (Picture.PictureMask = '') or (Picture.PictureMask = Cap1Letter) ),
                                 Self, Result, ToggleMask, sAutoCapitalize, 'Ctrl+T', Picture.PictureMask = Cap1Letter, true );
    AddDefaultEditMenu( Result );
  end
end;

procedure TevDBEdit.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
  SyncState;
end;

function TevDBEdit.CanHaveMask: boolean;
begin
  Result := Assigned(Field) and ( (Field is TStringField) or (Field is TMemoField) );
end;

//exact match without case conversion
function TevDBEdit.MatchMask: boolean;
var
  s: string;
begin
  if Text = '' then
    Result := true
  else
    with TwwPictureValidator.create(Cap1Letter, false) do
    try
      s := Text;
      Result := (Picture(s, False) = prComplete) and (s = Text);
    finally
      Free;
    end;
end;


procedure TevDBEdit.ToggleMask(Sender: TObject);
  procedure SetMask;
  begin
    Picture.PictureMaskFromDataset := False;
    Picture.PictureMask := Cap1Letter;
    if not MatchMask then
    begin
      // if linked dataset isn't in edit state
      // then tdataset.edit will fire deRecordChange event which causes
      // DataLink.OnDataChange to be called
      DataLink.OnDataChange := FOldDataChange;
      try
        DataLink.Edit;
      finally
        DataLink.OnDataChange := DataChange;
      end;
      ApplyMask;
      SetModified( true );
      if not Focused then
        UpdateRecord;
    end;
  end;

  procedure ResetMask;
  var
    newText: string;
  begin
    Picture.PictureMask := '';
    newText := LowerCase( Text );
    if newText <> Text then
    begin
      DataLink.OnDataChange := FOldDataChange;
      try
        DataLink.Edit;
      finally
        DataLink.OnDataChange := DataChange;
      end;
      InitText( newText );
      if not Focused then
        UpdateRecord;
    end;
  end;
begin
  if CanHaveMask then
    if Picture.PictureMask = '' then
      SetMask
    else if Picture.PictureMask = Cap1Letter then
      ResetMask;
end;

procedure TevDBEdit.DataChange(Sender: TObject);
begin
  if assigned( FOldDataChange ) then
    FOldDataChange( Sender );

  if CanHaveMask then
    if MatchMask then
    begin
      if Picture.PictureMask = '' then begin
        Picture.PictureMaskFromDataset := False;
        Picture.PictureMask := Cap1Letter;
      end;
    end
    else
      if Picture.PictureMask = Cap1Letter then
        Picture.PictureMask := '';
end;

constructor TevDBEdit.Create(AOwner: TComponent);
begin
  inherited;
// this works only while TwwDBEdit doesn't change DataLink.OnDataChange
// after construction,
// but there exist no virtual function called from TwwDbEdit.DataChange :(
  FOldDataChange := DataLink.OnDataChange;
  DataLink.OnDataChange := DataChange;
end;

function TevDBEdit.Atom: ISecurityAtom;
begin
  Result := nil;
end;

function TevDBEdit.Component: TComponent;
begin
  Result := Self;
end;

function TevDBEdit.SGetAtomElementRelationsArray: TAtomElementRelationsArray;
begin
  SetLength(Result, 0);
end;

function TevDBEdit.SGetStates: TElementStatesArray;
begin
  SetLength(Result, 0);
end;

function TevDBEdit.SGetTag: ShortString;
begin
  if Assigned(DataSource) and Assigned(DataSource.DataSet) then
    Result := DataSource.DataSet.Name+ ':'+ DataField
  else
    Result := '';
end;

function TevDBEdit.SGetType: ShortString;
begin
  Result := etDataField;
end;

procedure TevDBEdit.SSetCurrentSecurityState(const State: ShortString);
begin
  SecurityRO := State = stReadOnly;
end;


function TevDBEdit.SecurityState: ShortString;
begin
  Result := '';
end;

procedure TevDBEdit.EMSetReadOnly(var Message: TMessage);
begin
  if FSecurityRO then
    Message.WParam := 1;
  inherited;
end;

procedure TevDBEdit.SyncState;
begin
  if HandleAllocated then
    if FSecurityRO then
      SendMessage(Handle, EM_SETREADONLY, 1, 0)
    else
      SendMessage(Handle, EM_SETREADONLY, Ord(ReadOnly), 0);
end;

procedure TevDBEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
  begin
    if ssShift in Shift then exit;
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0);
    Key := 0;
  end
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    if FSecurityRO and ((Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift))) then
      Key := 0;

   inherited;
  end;


end;

procedure TevDBEdit.KeyPress(var Key: Char);
begin
  if AutoPopupEffectiveDateDialog and (datasource.dataset.state <> dsInsert) then
  begin
    Key := #0;
    exit;      //if we don't do this, it'll go into dsEdit and the popup will be incorrect.
  end;
  if FSecurityRO and ((Key in [^H, ^I, ^J, ^M, ^V, ^X, #13, #32..#255]) or (ord(key)=vk_back)) then
    Key := #0;

  inherited;
end;

function TevDBEdit.PreventEdit: boolean;
begin
  if FSecurityRO then
    Result := true
  else
    Result := inherited PreventEdit;
end;

procedure TevDBEdit.CreateHandle;
begin
  inherited;
  SyncState;
end;


function TevDBEdit.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBEdit.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBEdit.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

procedure TevDBEdit.CMEnter(var Message: TCMEnter);
begin
  inherited;

{  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;}
end;

procedure TevDBEdit.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
  begin
    FLastEffectiveDateDialogFields.Clear;
    Inherited;
  end;

end;

{ TevBitBtn }

procedure TevBitBtn.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevBitBtn.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevBitBtn.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevBitBtn.SetSecurityRO(Value: Boolean);
var
  Accept: Boolean;
begin
  if Assigned(FOnSetSecurityRO) then
  begin
    Accept := True;
    FOnSetSecurityRO(Self, Value, Accept);
    if not Accept then Exit;
  end;

  FSecurityRO := Value;
  SyncState;
end;

procedure TevBitBtn.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevBitBtn.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TevSpeedButton }

procedure TevSpeedButton.AfterConstruction;
begin
  inherited;
  FEnabled := inherited GetEnabled;
end;


procedure TevSpeedButton.CMEnabledChanged(var Message: TMessage);
begin
  FEvInternalRead := true;
  try
    inherited;
  finally
    FEvInternalRead := False;
  end;
end;

constructor TevSpeedButton.Create(AOwner: TComponent);
begin
  FEnabled := true;
  inherited;
end;

function TevSpeedButton.GetEnabled: Boolean;
begin
  if FEvInternalRead then
    Result := inherited GetEnabled
  else
    Result := FEnabled;
end;

procedure TevSpeedButton.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not FSecurityRO then
    inherited;

end;

procedure TevSpeedButton.Paint;
begin
  FEvInternalRead := true;
  try
    inherited;
  finally
    FEvInternalRead := False;
  end;
end;

procedure TevSpeedButton.SetEnabled(Value: Boolean);
begin
  FEnabled := Value;
  SyncState;
end;

procedure TevSpeedButton.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevSpeedButton.SyncState;
begin
  if FSecurityRO then
    inherited SetEnabled(false)
  else
    inherited SetEnabled(FEnabled);
end;


{ TEvDateTimePicker }

procedure TevDateTimePicker.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevDateTimePicker.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevDateTimePicker.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevDateTimePicker.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
  Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
    Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
    Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

procedure TevDateTimePicker.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
  CheckColor(Self, Value);
end;

procedure TevDateTimePicker.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevDateTimePicker.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;

procedure TevDateTimePicker.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
end;

{ TEvLabel }

procedure TevLabel.AdjustBounds;
var
  TextR: TRect;
  Font: TFont;
  Text: String;
begin
  if not (csReading in ComponentState) and AutoSize and IsCaptionWithAsterisk(Caption) then
  begin
    Canvas.Handle := GetDC(0);
    try
      Text := Caption;
      Delete(Text, 1, 1);

      TextR := Rect(0, 0, 0, 0);
      TextR.Right := Canvas.TextWidth(Text);

      Inc(TextR.Right, 3);

      Font := TFont.Create;
      try
        Font.Assign(Canvas.Font);

        Canvas.Font.Style := Canvas.Font.Style + [fsBold];
        Canvas.Font.Size := Canvas.Font.Size + 2;  //make the asterisk bigger

        TextR.Right := TextR.Right + Canvas.TextWidth('*');
        TextR.Bottom  := Canvas.TextHeight('*');

        Canvas.Font.Assign(Font);
      finally
        Font.Free;
      end;
    finally
      ReleaseDC(0, Canvas.Handle);
      Canvas.Handle := 0;      
    end;

    SetBounds(Left, Top, TextR.Right, TextR.Bottom);
  end
  else
    inherited;
end;

function TEvLabel.GetLabelText: string;
begin
  if Copy(Caption, 1, 1) = '~' then
    Result := '*' + Copy(Caption, 2, Length(Caption))
  else
    Result := inherited GetLabelText;
end;

procedure TEvLabel.Paint;
begin
  if IsCaptionWithAsterisk(Caption) then
    DrawCaptionWithAsterisk(Canvas, Caption, 0, 0)
  else
    inherited;
end;


{ TevDBDateTimePicker }

procedure TevDBDateTimePicker.CloseUp(modified: boolean);
begin
  inherited;
end;

procedure TevDBDateTimePicker.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if AutoPopupEffectiveDateDialog and not SecurityRO and CheckEvDSState(DataSource, [dsBrowse]) then
    PostMessage(Handle, CM_SHOW_EFFECTIVE_DATE_DIALOG, 0, 0)
  else
    FLastEffectiveDateDialogFields.Clear;
end;

procedure TevDBDateTimePicker.CMShowEffectiveDateDialog(var Message: TMessage);
begin
  CtrlShowVersionedFieldEditor(Self, DataSource.DataSet, DataField);
end;

constructor TevDBDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  Time := 0;
end;

destructor TevDBDateTimePicker.Destroy;
begin
  inherited;
end;

procedure TevDBDateTimePicker.DropDown;
begin
  if not FSecurityRO then
    if FUseQuarters then
      ShowQuarterlyDialog
    else
      inherited;


end;




function TevDBDateTimePicker.GetUseQuarters: Boolean;
begin
  Result := FUseQuarters;
end;

function TevDBDateTimePicker.GetUseQuartersOnly: Boolean;
begin
  Result := FUseQuartersOnly;
end;

function TevDBDateTimePicker.IsVersioned: Boolean;
begin
  Result := not BlockEffectiveDateEditor and IsVersionedDBFieldControl(Self);
end;

procedure TevDBDateTimePicker.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if FSecurityRO and ((Key = VK_DELETE) or (Key = VK_INSERT) or (Key in [32..255])) then
    Key := 0;
  inherited;
end;

procedure TevDBDateTimePicker.PrepareVersionedLook;
begin
  SetControlVersionedLook(Self);
end;

procedure TevDBDateTimePicker.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  CheckColor(Self, Value);
end;

procedure TevDBDateTimePicker.SetUseQuarters(Value: Boolean);
begin
  FUseQuarters := Value;
  if not Value then SetUseQuartersOnly(False);
end;

procedure TevDBDateTimePicker.SetUseQuartersOnly(Value: Boolean);
begin
  FUseQuartersOnly := Value;
  if Value then SetUseQuarters(Value);
end;

procedure TevDBDateTimePicker.ShowQuarterlyDialog;
begin

    
  with TQuarterlyDateDialog.Create(self) do
  begin
    PreventCustomDates := FUseQuartersOnly;
    qdsCalendar.Date := Date;

    if ShowModal = idOk then
    begin
      DataSource.DataSet.Edit;
      DataSource.DataSet.FieldByName(DataField).Value := qdsCalendar.Date;
      DateTime := qdsCalendar.Date;
    end;
    Free;
  end;
end;

procedure TevDBDateTimePicker.WMPaste(var Message: TMessage);
begin
  if not FSecurityRO then
    inherited;
end;


{ TevButton }

procedure TevButton.CNDrawItem(var Message: TWMDrawItem);
begin
  FPainting := True;
  try
    inherited;
  finally
    FPainting := False;
  end;
end;

procedure TevButton.CreateHandle;
begin
  inherited;
  SyncState;
end;

function TevButton.GetEnabled: Boolean;
begin
  Result := inherited GetEnabled;
  if FPainting then
    Result := not FSecurityRO and Result;
end;

procedure TevButton.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevButton.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end;
end;

procedure TevButton.WMEnable(var Message: TMessage);
begin
  if LongBool(Message.WParam) <> LongBool(not FSecurityRO and Enabled) then
    SyncState
  else
    inherited;
end;


{ TEvDBGrid }

constructor TEvDBGrid.Create(AOwner: TComponent);
begin
  inherited;
  IniAttributes.Free;
  IniAttributes := TevIniAttributes.Create;
  with IniAttributes do begin
    FileName := '';
    SectionName := '';
    Delimiter := ';;';
    Enabled := True;
    Owner := self;
    IniAttributes.SaveToRegistry := True;
    IniAttributes.CheckNewFields := True;
  end;

  FSecurityRO := False;
  FTempKeyOptions := [dgAllowInsert, dgAllowDelete, dgEnterToTab];
  inherited KeyOptions := [dgEnterToTab];
  inherited ReadOnly := True;
  FReadOnly := True;
  UseTFields := False;
  ControlInfoInDataSet := False;
end;

procedure TEvDBGrid.DoCalcCellColors(Field: TField; State: TGridDrawState;
	     highlight: boolean; AFont: TFont; ABrush: TBrush);
var
  e: ISecurityElement;
begin
  inherited;
  if Assigned(Field) then // just a walkarount for strange bug // Sergei
  begin
    e := FindSecurityElement(Self, UpperCase(Field.DataSet.Name+ ':'+ Field.FieldName));
    if (Assigned(e) and (e.SecurityState = stReadOnly)) then
      ABrush.Color := clBtnFace;
  end;
end;

procedure TEvDBGrid.SetKeyOptions(const Value: TwwDBGridKeyOptions);
begin
  if inherited KeyOptions <> Value then
  begin
    if not ReadOnly then
      inherited KeyOptions := Value;
  end;
  FTempKeyOptions := Value;
end;

procedure TEvDBGrid.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  inherited ReadOnly := Value or FSecurityRO;
  if inherited ReadOnly then
    inherited KeyOptions := inherited KeyOptions - [dgAllowDelete, dgAllowInsert]
  else
    inherited KeyOptions := FTempKeyOptions;
end;

function TevDBGrid.GetKeyOptions: TwwDBGridKeyOptions;
begin
  Result := inherited KeyOptions;
end;

function TevDBGrid.GetReadOnly: Boolean;
begin
  Result := inherited ReadOnly;
end;

function TevDBGrid.IsKeyOptions: boolean;
begin
  Result := not (ReadOnly and (KeyOptions = [dgEnterToTab]) or
                 not ReadOnly and (KeyOptions = [dgAllowDelete, dgAllowInsert, dgEnterToTab]));
end;

procedure TevDBGrid.ForceKeyOptions(const Value: TwwDBGridKeyOptions);
begin
  inherited KeyOptions := Value;
  FTempKeyOptions := Value;
end;

procedure TevDBGrid.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  inherited ReadOnly := FReadOnly or FSecurityRO;
  if inherited ReadOnly then
    inherited KeyOptions := inherited KeyOptions - [dgAllowDelete, dgAllowInsert]
  else
    inherited KeyOptions := FTempKeyOptions;
  CheckColor(Self, Value);
end;

procedure TevDBGrid.Loaded;
begin
  inherited;
  IniAttributes.FileName := mb_AppSettings.TopNodeName + '\Misc\';
end;

function TevDBGrid.Atom: ISecurityAtom;
begin
  Result := nil;
end;

function TevDBGrid.Component: TComponent;
begin
  Result := Self;
end;

function TevDBGrid.SGetAtomElementRelationsArray: TAtomElementRelationsArray;
begin
  SetLength(Result, 0);
end;

function TevDBGrid.SGetStates: TElementStatesArray;
begin
  SetLength(Result, 0);
end;

function TevDBGrid.SGetTag: ShortString;
var
  s: string;
  i, Apos: Integer;
  e: TevSecElement;
begin
  Result := '';
  if not FSecurityElementsCreated and Assigned(DataSource) and Assigned(DataSource.DataSet) then
  begin
    for i := 0 to Selected.Count-1 do
    begin
      APos:= 1;
      s := strGetToken(Selected[i], #9, APos);
      e := TevSecElement.Create(Self);
      e.AtomComponent := nil;
      e.ElementType := otDataField;
      e.ElementTag := UpperCase(DataSource.DataSet.Name + ':'+ s);
    end;
    FSecurityElementsCreated := True;
  end;
end;

function TevDBGrid.SGetType: ShortString;
begin
  Result := '';
end;

procedure TevDBGrid.SSetCurrentSecurityState(const State: ShortString);
begin
end;

function TevDBGrid.CanEditModify: Boolean;
var
  e: ISecurityElement;
begin
  Result := inherited CanEditModify;
  if Assigned(DataSource) and Assigned(DataSource.DataSet) then
  begin
    e := FindSecurityElement(Self, UpperCase(DataSource.DataSet.Name+ ':'+ Fields[SelectedIndex].FieldName));
    if Assigned(e) then
      Result := Result and (e.SecurityState <> stReadOnly);
  end;
end;

function TevDBGrid.SecurityState: ShortString;
begin
  Result := '';
end;


{ TevIniAttributes }

constructor TevIniAttributes.Create;
begin
  inherited;
  Enabled := True;
end;

{ TevVersionedFieldPopupMenu }

procedure TevVersionedFieldPopupMenu.AfterConstruction;
begin
  inherited;
  Items.Add(CreateFieldVersionsItem(Self));
end;

function TevVersionedFieldPopupMenu.CreateFieldVersionsItem(const AOwner: TComponent): TMenuItem;
var
  i : Integer;
begin
  //if 2 or more versioned controls share the same popup menu, don't let them
  //be duplicated here.
  Result := nil;
  if AOwner is TPopupMenu then
  begin
    for i := 0 to (AOwner as TPopupMenu).Items.Count - 1 do
    begin
      if (AOwner as TPopupMenu).Items[i].Caption = 'Effective Period' then exit;
    end;
  end;

  Result := TMenuItem.Create(AOwner);
  Result.Caption := 'Effective Period';
  Result.OnClick := ShowFieldVersions;
end;

procedure TevVersionedFieldPopupMenu.ShowFieldVersions(Sender: TObject);
var
  c: TComponent;
  fc: IevDBFieldControl;
  AffectedFields: TisCommaDilimitedString;
  L: IisStringList;
  dsROMode: Boolean;
  DefaultToQuarters: Boolean;
  ForceQuarters: Boolean;
  Pict: TwwDBPicture;
  Options: IisListOfValues;
  Fld: TField;
  Obj : TObject;
  AltFieldName : String;
begin
  c := nil;
  DefaultToQuarters := False;
  ForceQuarters := False;
  if Sender is TMenuItem then
    if TMenuItem(Sender).Owner is TPopupMenu then
    begin
      c := TPopupMenu(TMenuItem(Sender).Owner).PopupComponent;
      if Supports(c, IevDBFieldControl) then
        fc := c as IevDBFieldControl;
    end;

  if Assigned(fc) and CheckEvDSState(fc.DataSource, [dsBrowse, dsEdit]) then
  begin
    dsROMode := TevClientDataSet(fc.DataSource.DataSet).ReadOnly;
    if IsPublishedProp(c, 'SecurityRO') and (GetOrdProp(c, 'SecurityRO') <> 0) then
      TevClientDataSet(fc.DataSource.DataSet).ReadOnly := True;

    if IsPublishedProp(c, 'AutoPopupEffectiveDateDialog') and (GetOrdProp(c, 'AutoPopupEffectiveDateDialog') <> 0) then
      DefaultToQuarters := True;

    if IsPublishedProp(c, 'AutoPopupForceQuartersOnly') and (GetOrdProp(c, 'AutoPopupForceQuartersOnly') <> 0) then
      ForceQuarters := True;

    Options := TisListOfValues.Create;

    Fld := fc.DataSource.DataSet.FieldByName(fc.FieldName);
    if IsPublishedProp(Fld, 'DisplayFormat') then
      Options.AddValue('DisplayFormat', GetStrProp(Fld, 'DisplayFormat'));

    if IsPublishedProp(c, 'Picture') then
    begin
      Pict := GetObjectProp(c, 'Picture', TwwDBPicture) as TwwDBPicture;
      if Assigned(Pict) then
        Options.AddValue('PictureMask', Pict.PictureMask);
    end;
      //added to support non-db controls to popup as a group with db controls
  if IsPublishedProp(c, 'ForceVersioningAlternateControl') and (GetObjectProp(c, 'ForceVersioningAlternateControl') <> nil) then
  begin
     Obj := GetObjectProp(c, 'ForceVersioningAlternateControl');
     if Obj is TObject then
     begin
       if IsPublishedProp(Obj, 'DataField') then
         AltFieldName := GetStrProp(Obj,'DataField')
       else
         AltFieldName := '';
     end;
  end;
  if (Assigned(fc) and (fc <> nil)) then
    try
      if AltFieldName <> '' then
        ShowVersionedFieldEditor(TevClientDataSet(fc.DataSource.DataSet), AltFieldName, AffectedFields, DefaultToQuarters, ForceQuarters, Options)
      else
        ShowVersionedFieldEditor(TevClientDataSet(fc.DataSource.DataSet), fc.FieldName, AffectedFields, DefaultToQuarters, ForceQuarters, Options);
    finally
      TevClientDataSet(fc.DataSource.DataSet).ReadOnly := dsROMode;
    end;

    // Store information about last versioned fields
    FLastEffectiveDateDialogFields.Clear;
    FLastEffectiveDateDialogFields.AddValue('DataSet', Integer(Pointer(fc.DataSource.DataSet)));
    L := TisStringList.CreateAsIndex;
    L.CommaText := AffectedFields;
    FLastEffectiveDateDialogFields.AddValue('Fields', L);
  end;
end;

{ TevListBox }

procedure TevListBox.SetBorder(AColor: TColor);
begin
  Canvas.Pen.Style := psSolid;
  Canvas.Pen.Color := AColor;
  Canvas.Brush.Style := bsClear;
  Canvas.Rectangle(0, 0, Width, Height);
  Canvas.Pen.Color := clWindow;                 //Draw the outer rectangle
  Canvas.Rectangle(1,1,Width - 1, Height - 1);  //wipe out the existing 3D
end;

procedure TevListBox.WMPaint(var Message: TWMPaint);
begin
  inherited;
  SetBorder(clBtnShadow);
end;

{ TevCheckListBox }

procedure TevCheckListBox.SetSecurityRO(const Value: Boolean);
begin
  FSecurityRO := Value;
  SyncState;
end;

procedure TevCheckListBox.SyncState;
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
  begin
    EnableWindow(Handle, not FSecurityRO and Enabled);
    Invalidate;
  end; 

end;

initialization
  CreateGlobalObjects;


finalization
  FreeGlobalObjects;

end.


