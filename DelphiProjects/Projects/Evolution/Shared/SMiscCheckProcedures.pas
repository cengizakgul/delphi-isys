// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SMiscCheckProcedures;

interface

procedure NextCompanyAvailableCheckNumber(CO_BankField: string; var SerialNumber: integer; var BankAccount: string; var MiscCheckType:
  string; const DontCutCheck: Boolean = False);
function GetGlobalAgencyFieldOffice(Global_Agency_Nbr: integer): integer;
procedure CreateMiscCheck(aPR_NBR: Integer; MiscCheckType: string; Amount: Currency; SerialNumber: Variant;
  BankAccount: string; strTaxDepositId: string; Global_Agency_Nbr: Integer = -1; const FieldOfficeNbr: Integer = -1;
  const TaxCouponSyReportNbr: Integer = -1);

implementation

uses SDataStructure, SysUtils, Db,  EvConsts, EvUtils, EvTypes, EvExceptions, EvClientDataSet;

function GetGlobalAgencyFieldOffice(Global_Agency_Nbr: integer): integer;
begin
  with DM_SYSTEM_MISC.SY_GLOBAL_AGENCY do
  begin
    if not Locate('SY_GLOBAL_AGENCY_NBR', Global_Agency_Nbr, []) then
      raise EInconsistentData.CreateHelp('(' + IntToStr(Global_Agency_Nbr) + ') Global agency was not found', IDH_InconsistentData);

    if FieldByName('TAX_PAYMENT_FIELD_OFFICE_NBR').IsNull then
      raise EInconsistentData.CreateHelp('(' + IntToStr(Global_Agency_Nbr) + ') ' + FieldByName('AGENCY_NAME').AsString +
        ':  Tax payment field office was not initialized.', IDH_InconsistentData);

    result := FieldByName('TAX_PAYMENT_FIELD_OFFICE_NBR').AsInteger;
  end;
end;

procedure NextCompanyAvailableCheckNumber(CO_BankField: string; var SerialNumber: integer; var BankAccount: string; var MiscCheckType:
  string; const DontCutCheck: Boolean = False);
var
  SavePlace: TBookmark;
  aDataSet: TevClientDataSet;
  CheckNumberField, KeyFieldName: string;
begin
  SerialNumber := -1;

  aDataSet := nil;
  if (CO_BankField = 'TAX_SB_BANK_ACCOUNT_NBR') or
    (CO_BankField = 'BILLING_SB_BANK_ACCOUNT_NBR') then
  begin
    aDataSet := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS;
    CheckNumberField := 'NEXT_AVAILABLE_CHECK_NUMBER';
    KeyFieldName := 'SB_BANK_ACCOUNTS_NBR';
  end
  else
    if (CO_BankField = 'TAX_CL_BANK_ACCOUNT_NBR') or
    (CO_BankField = 'BILLING_CL_BANK_ACCOUNT_NBR') then
  begin
    aDataSet := DM_CLIENT.CL_BANK_ACCOUNT;
    CheckNumberField := 'NEXT_CHECK_NUMBER';
    KeyFieldName := 'CL_BANK_ACCOUNT_NBR';
  end
  else
    Assert(False, 'Unrecognized bank account field for CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

  if MiscCheckType = '' then
  begin
    if (CO_BankField = 'BILLING_SB_BANK_ACCOUNT_NBR') or
      (CO_BankField = 'BILLING_CL_BANK_ACCOUNT_NBR') then
      MiscCheckType := MISC_CHECK_TYPE_BILLING;

    if (CO_BankField = 'TAX_SB_BANK_ACCOUNT_NBR') or
      (CO_BankField = 'TAX_CL_BANK_ACCOUNT_NBR') then
      MiscCheckType := MISC_CHECK_TYPE_TAX;
  end;

  Assert(MiscCheckType <> '', 'Unresolvable misc check type for CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

  with aDataSet do
  begin
    SavePlace := GetBookmark;

    if Locate(KeyFieldName, DM_COMPANY.CO.FieldByName(CO_BankField).AsInteger, []) then
    begin
      Edit;
      SerialNumber := FieldByName(CheckNumberField).AsInteger;
      FieldByName(CheckNumberField).AsInteger := SerialNumber + 1;
      BankAccount := FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
      if DontCutCheck then
        Cancel
      else
        Post;
    end
    else
      raise EInconsistentData.CreateHelp(CO_BankField + ' account not found CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString, IDH_InconsistentData);

    GotoBookmark(SavePlace);
    FreeBookmark(SavePlace);
  end;

  if SerialNumber = -1 then
    raise EInconsistentData.CreateHelp('Payroll check number not assigned for CO=NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString, IDH_InconsistentData);
end;

function GetFieldOfficeAgency(FieldOfficeNbr: integer): integer;
begin
  with DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE do
  begin
    if not Locate('SY_GL_AGENCY_FIELD_OFFICE_NBR', FieldOfficeNbr, []) then
      raise EInconsistentData.CreateHelp('(' + IntToStr(FieldOfficeNbr) + ') Field office was not found', IDH_InconsistentData);
    Result := FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
  end;
end;

procedure CreateMiscCheck(aPR_NBR: Integer; MiscCheckType: string; Amount: Currency; SerialNumber: Variant;
  BankAccount: string; strTaxDepositId: string; Global_Agency_Nbr: Integer = -1; const FieldOfficeNbr: Integer = -1;
  const TaxCouponSyReportNbr: Integer = -1);
begin
  Assert(DM_PAYROLL.PR.Locate('PR_NBR', aPR_NBR, []));
  if not DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.AsInteger, []) then
  begin
    DM_COMPANY.CO.DataRequired('ALL');
    Assert(DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.AsInteger, []));
  end;
  with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
  begin
    if not Active then
      Open;
    Append;
    FieldByName('PR_NBR').AsInteger := aPR_NBR;
    FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency := Amount;
    FieldByName('PAYMENT_SERIAL_NUMBER').Value := SerialNumber;
    FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString := MiscCheckType;
    FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
    FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := BankAccount;
    FieldByName('CO_TAX_DEPOSITS_NBR').AsString := strTaxDepositId;

    if not (FieldOfficeNbr = -1) then
    begin
      FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := FieldOfficeNbr;
      FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger := GetFieldOfficeAgency(FieldOfficeNbr);
    end
    else
    if not (Global_Agency_Nbr = -1) then
    begin
      GetGlobalAgencyFieldOffice(Global_Agency_Nbr); // Make sure field office has been initialized
                                                      // function will raise exception if not
      FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger := Global_Agency_Nbr;
    end;
    if TaxCouponSyReportNbr <> -1 then
      FieldByName('TAX_COUPON_SY_REPORTS_NBR').AsInteger := TaxCouponSyReportNbr;

    Post;
  end;
end;

end.
