// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPackageEntry;

interface

uses
  Forms, ComCtrls,  StdCtrls, SysUtils, menus, SFrameEntry, Variants,
  Controls, Buttons, ExtCtrls, EvCommonInterfaces, Db, Wwdatsrc,
  Dialogs, SSecurityInterface, windows, EvUtils, Messages, Grids, DateUtils,
  ToolWin, ImgList, Classes, EvTypes, ActnList, Graphics, contnrs, EvBasicUtils,
  ISBasicClasses, EvDataAccessComponents, isVCLBugFix, evContext, EvExceptions,
  EvUIUtils, EvUIComponents, EvClientDataSet, EvConsts, isTypes, TypInfo,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;


type
  TFrameClass = class of TFrameEntry;

  TFramePackageTmpl = class(TFrame, IevScreenPackage, IevGlobalLookupHandler)
    WorkPanel: TEvPanel;
    BackDDMenu: TevPopupMenu;
    ForwardDDMenu: TevPopupMenu;
    Panel: TevPanel;
    pnlNavigationButtons: TevPanel;
    btnNavFirst: TevSpeedButton;
    btnNavPrevious: TevSpeedButton;
    btnNavNext: TevSpeedButton;
    btnNavLast: TevSpeedButton;
    btnNavInsert: TevSpeedButton;
    btnNavDelete: TevSpeedButton;
    btnNavOk: TevSpeedButton;
    btnNavCancel: TevSpeedButton;
    dtpAsOfDate: TevDateTimePicker;
    HistoryToolBar: TevToolBar;
    btnNavBackScreen: TevToolButton;
    btnNavForwardScreen: TevToolButton;
    AutoSaveTimer: TTimer;
    pnlCommit: TevPanel;
    btnNavCommit: TevSpeedButton;
    btnNavAbort: TevSpeedButton;
    btnRefresh: TevSpeedButton;
    pmAudit: TevPopupMenu;
    miFieldAudit: TMenuItem;
    miRecordAudit: TMenuItem;
    alAudit: TevActionList;
    aRecordAudit: TAction;
    MenuImageList: TevImageList;
    btnNavHistory: TevSpeedButton;
    Viewtableaudithistory1: TMenuItem;
    aTableAudit: TAction;
    aDatabaseAudit: TAction;
    Viewdatabaseaudithistory1: TMenuItem;
    procedure btnNavFirstClick(Sender: TObject);
    procedure dtpAsOfDateChange(Sender: TObject);
    procedure btnNavBackScreenClick(Sender: TObject);
    procedure btnNavForwardScreenClick(Sender: TObject);
    procedure BackDDMenuPopup(Sender: TObject);
    procedure ForwardDDMenuPopup(Sender: TObject);
    procedure AutoSaveTimerTimer(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure miFieldAuditClick(Sender: TObject);
    procedure aRecordAuditExecute(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure aTableAuditExecute(Sender: TObject);
    procedure aDatabaseAuditExecute(Sender: TObject);
  private
    FSupportDataSets: TArrayDS;
    FInsertDataSets: TArrayDS;
    FEditDataSets: TArrayDS;
    FDeleteDataSet: TevClientDataSet;
    FNavigationDataSet: TevClientDataSet;
    FCommiting: Boolean;
    FImages: TObjectList;
    FInserting: Boolean;
    FActiveFrame: TFrameEntry;

    procedure FillImages;
    procedure HistoryMenuClick(Sender: TObject);
    procedure SetDeleteDataSet(const Value: TevClientDataSet);
    procedure SetEditDataSets(const Value: TArrayDS);
    procedure SetInsertDataSets(const Value: TArrayDS);
    procedure SetNavigationDataSet(const Value: TevClientDataSet);
    procedure SetSupportDataSets(const Value: TArrayDS);
    procedure DoInsertDataSets(DataSet: TDataSet);
    function  ButtonByKind(Kind: Integer): TControl;
    function  GetFullCaption(Item: TMenuItem): string;
    procedure AddScreenHistory(sCaption: string);
    procedure CheckButtons;
    procedure GotoScreen(iPos: Integer); overload;
    procedure GotoScreen(sName: string); overload;
    procedure DoAbortDataSets;
    procedure SyncAsOfDate;
    procedure SyncAsOfDateColor;
    procedure ShowAudit(const AType: Char);
    procedure DataSetPost(DataSet: TDataSet);
    procedure DataSetCancel(DataSet: TDataSet);
    procedure DataSetScroll(DataSet: TDataSet);
    procedure DataSetInsert(DataSet: TDataSet);
    procedure DataSetOpen(DataSet: TDataSet);
    procedure DataSetClose(DataSet: TDataSet);
    procedure DataSetEdit(DataSet: TDataSet);
    procedure DataSetDelete(DataSet: TDataSet);
    procedure DataSetCommit(DataSet: TDataSet);
    procedure DataSetAbort(DataSet: TDataSet);
    procedure DataSetFilter(DataSet: TDataSet);
    procedure DataSetAssign(DataSet: TDataSet);
    procedure DataSetCustomSet(DataSet: TevClientDataSet);
    function  IsNavigationDataSet(DataSet: TDataSet): Boolean;
    function  IsInsertDataSet(DataSet: TDataSet): Boolean;
    function  IsEditDataSet(DataSet: TDataSet): Boolean;
    function  IsDeleteDataSet(DataSet: TDataSet): Boolean;
    function  IsPostDataSet(DataSet: TDataSet): Boolean;
    function  IsAllBrowse(aDS: TArrayDS): Boolean;
    function  IsSomeEdit(aDS: TArrayDS): Boolean;
    procedure DoPostDataSets;
    procedure DoCancelDataSets;
    procedure DoDeleteDataSets;
    procedure DoCommitDataSets;
    procedure HandleGlobalLookupRequest( frameclass: TClass );
    procedure SyncButtonsState;
    function  IsAllCommitted: Boolean;
    procedure CommitDataSets;
    procedure PrepareVersionedFields;
  protected
    FInitialized: Boolean;
    FActivated: Boolean;

    procedure AddStructure(Path: string; ClassToCall: String = ''; Default: Boolean = False; Bitmap: TBitmap = nil;ForceAdd:boolean=false);
    function  GetBitmap( index: integer ): TBitmap;
    function  CanClose: Boolean; virtual;
    function  PackageCaption: string; virtual;
    function  PackageSortPosition: string; virtual;
    function  InitPackage: Boolean; virtual;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean; virtual;
    function  DeactivatePackage: Boolean; virtual;
    function  UninitPackage: Boolean; virtual;
    procedure UserPackageStructure; virtual;
    function  PackageBitmap: TBitmap; virtual;
    function  AskCommitChanges: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function  IsModified: Boolean;
    function  ActivateFrame(FClass: Pointer; Path: string): Boolean;
    procedure OpenFrame(FClass: string);
    procedure AutoSave(Minutes: Integer);
    procedure InternalRefresh;
    procedure StoreEditDSForswitchScreen( frameclass: TClass );
    procedure AddPostDataSet(DataSet: TDataSet);
    procedure SetPostingDataSetLast(DataSet: TDataSet);
    procedure SetButton(Kind: Integer; Value: Boolean);
    function  ButtonEnabled(Kind: Integer): Boolean;
    procedure ClickButton(Kind: Integer); virtual;
    procedure HideToolBar;
    procedure ShowToolBar;

    property  ActiveFrame: TFrameEntry read FActiveFrame;
    property  NavigationDataSet: TevClientDataSet read FNavigationDataSet write SetNavigationDataSet;
    property  InsertDataSets: TArrayDS read FInsertDataSets write SetInsertDataSets;
    property  EditDataSets: TArrayDS read FEditDataSets write SetEditDataSets;
    property  DeleteDataSet: TevClientDataSet read FDeleteDataSet write SetDeleteDataSet;
    property  SupportDataSets: TArrayDS read FSupportDataSets write SetSupportDataSets;
  end;


  TStrItem = record
    Path: string;
    Position: string;
    PackageID: String;
    PClass: String;
    Default: Boolean;
    Bitmap: TBitmap;
  end;

var
  Structure: array of TStrItem;

const
  NavFirst = 1;
  NavPrevious = 2;
  NavNext = 3;
  NavLast = 4;
  NavInsert = 5;
  NavDelete = 6;
  NavBack = 7;
  NavForward = 8;
  NavHistory = 9;
  NavOK = 10;
  NavCancel = 11;
  NavCommit = 12;
  NavAbort = 13;
  NavRefresh = 14;

implementation

uses EvSecElement, EvSecurityUtils, SShowLogo, sDSMemento, EvAuditViewFrm;

type
  TevChildTableRec = record
    TableName: String;
    RefField: string;
  end;

  TevMasterTableRec = record
    TableName: String;
    RefTables: array of TevChildTableRec;
  end;

var
  HistoryList: TStringList;
  HistoryListPosition: Integer = -1;
  OldHistPos: Integer = -1;
  HistoryMoving: Boolean = False;
  _DSMementoManager: IDSMementoManager;
  FDataSetsToPost: TArrayDS;

{$R *.DFM}

{ TFramePackageTmpl }

procedure TFramePackageTmpl.AddStructure(Path: string; ClassToCall: String = ''; Default: Boolean = False; Bitmap: TBitmap = nil;ForceAdd:boolean=false);
begin
  if ((ClassToCall <> '') and (GetClass(ClassToCall) = nil)) and  (not ForceAdd) then
    Exit;

  SetLength(Structure, Succ(Length(Structure)));
  if Pos('\', Path) <> 1 then
  begin
    Assert(PackageCaption <> '');
    Path := '\' + PackageCaption + '\' + Path;
  end;

  if Pos('|', Path) = 0 then
  begin
    Structure[Pred(Length(Structure))].Path := Path;
    Structure[Pred(Length(Structure))].Position := '';
  end
  else
  begin
    Structure[Pred(Length(Structure))].Path := Copy(Path, 1, Pred(Pos('|', Path)));
    Structure[Pred(Length(Structure))].Position := Copy(Path, Succ(Pos('|', Path)), Length(Path));
  end;
  Structure[Pred(Length(Structure))].PClass := ClassToCall;
  Structure[Pred(Length(Structure))].Default := Default;
  Structure[Pred(Length(Structure))].Bitmap := Bitmap;
end;


function TFramePackageTmpl.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := True;
  if not FActivated then
  begin
    Parent := WorkPlace;
    LogoPresenter.ShowLogo(WorkPanel);
    SyncAsOfDate;
    Visible := True;
    FActivated := True;
  end;
end;

function TFramePackageTmpl.DeactivatePackage: Boolean;
begin
  Result := True;
  if FActivated then
  begin
    LogoPresenter.ShowLogo(Parent);
    Result := CanClose and ActivateFrame(nil, '');
    if Result then
    begin
      Visible := False;
      Parent := nil;
      FActivated := False;
    end;
  end;
end;

function TFramePackageTmpl.UninitPackage: Boolean;
begin
  Result := True;
  if FInitialized then
  begin
    Result := not FActivated or DeactivatePackage;
    if Result then
    begin
      FreeAndNil( FImages );
      FInitialized := False;
    end;
  end;
end;

function TFramePackageTmpl.InitPackage: Boolean;
begin
  Result := True;
  if not FInitialized then
  begin
    FActiveFrame := nil;
    FImages := TObjectList.Create( true );
    FillImages;
    FInitialized := True;
  end;
end;

function TFramePackageTmpl.PackageCaption: string;
begin
  Result := '';
end;

function TFramePackageTmpl.PackageSortPosition: string;
begin
  Result := '';
end;

procedure TFramePackageTmpl.UserPackageStructure;
begin
end;

function TFramePackageTmpl.ActivateFrame(FClass: Pointer; Path: string): Boolean;
var
  f: TFrameEntry;
  nds, dds: TevClientDataSet;
  ids, eds, sds, EmptyArrayDS: TArrayDS;
begin
  Update;
  Result := not Assigned(ActiveFrame) or ActiveFrame.CanClose;
  if Result then
  begin
    if Assigned(ActiveFrame) then
    begin
      if ActiveFrame.ClassType = FClass then
        Exit;

      if ctx_Statistics.Enabled then
        ctx_Statistics.BeginEvent('Deactivate');

      ActiveFrame.Visible := False;
      ActiveFrame.Parent := nil;
      NavigationDataSet := nil;
      SetLength(EmptyArrayDS, 0);
      InsertDataSets := EmptyArrayDS;
      EditDataSets := EmptyArrayDS;
      DeleteDataSet := nil;
      SupportDataSets := EmptyArrayDS;
      ActiveFrame.Deactivate;
      ActiveFrame.Free;
      FActiveFrame := nil;
      btnNavHistory.Enabled := False;
      btnRefresh.Enabled := False;

      if ctx_Statistics.ActiveEvent <> nil then
      begin
        ctx_Statistics.EndEvent;
        ctx_Statistics.EndEvent;
      end;
    end;

    if Assigned(FClass) then
    begin
      if (HistoryList.Count = 0) or HistoryMoving or (HistoryList[HistoryListPosition] <> Path) then
        AddScreenHistory(Path);

      ctx_AccountRights;


      f := TFrameClass(FClass).Create(Self);
      if f.CanOpen then
      begin
        f.Parent := WorkPanel;
        if Assigned(f.Parent) then
        begin
          if ctx_Statistics.Enabled then
          begin
            ctx_Statistics.BeginEvent('Screen ' + f.ClassName);
            ctx_Statistics.BeginEvent('Activate');
          end;

          FActiveFrame := f;
          nds := nil;
          ids := EmptyArrayDS;
          eds := EmptyArrayDS;
          dds := nil;
          sds := EmptyArrayDS;
          ActiveFrame.RetrieveDataSets(nds, ids, eds, dds, sds);
          NavigationDataSet := nds;
          InsertDataSets := ids;
          EditDataSets := eds;
          DeleteDataSet := dds;
          SupportDataSets := sds;

          AddDS(nds, sds);
          AddDS(ids, sds);
          AddDS(eds, sds);
          AddDS(dds, sds);

          try
            ctx_DataAccess.OpenDataSets(sds);
          except
            on E: ENoRightsToClient do
            begin
              EvErrMessage(E.Message);
              ctx_DataAccess.OpenClient(0);
              ctx_DataAccess.OpenDataSets(sds);
            end
            else
              raise;
          end;

          btnNavHistory.Enabled := Length(sds) > 0;
          btnRefresh.Enabled := btnNavHistory.Enabled;
          dtpAsOfDate.Enabled := btnNavHistory.Enabled;
          ActiveFrame.Activate;

          PrepareVersionedFields;

          f.Visible := True;
          PutFocus(f);
          _DSMementoManager.ActivatedFrame( ActiveFrame.ClassName, EditDataSets );

          if ctx_Statistics.Enabled then
            ctx_Statistics.EndEvent;
        end
        else
          f.Free;
      end
      else
        f.Free;
    end;
  end;
  CheckButtons;
  SyncAsOfDate;
end;

function TFramePackageTmpl.CanClose: Boolean;
begin
  Result := not FActivated or not Assigned(ActiveFrame) or ActiveFrame.CanClose;
end;

constructor TFramePackageTmpl.Create(AOwner: TComponent);
var
  Component: TevSecElement;
begin
  inherited;
  FCommiting := False;
  FInserting := False;
  if not Assigned(FindSecurityElement(Self, PackageCaption)) then
  begin
    Component := TevSecElement.Create(Self);
    Component.AtomComponent := Self;
    Component.ElementTag := PackageCaption;
    Component.ElementType := otMenu;
  end;
end;

procedure TFramePackageTmpl.SetDeleteDataSet(
  const Value: TevClientDataSet);
var
  DS: TevClientDataSet;
begin
  if FDeleteDataSet <> Value then
  begin
    DS := FDeleteDataSet;
    FDeleteDataSet := Value;
    DataSetCustomSet(DS);
    if Value <> nil then
      DataSetAssign(Value)
    else
      SyncButtonsState;
  end;
end;

procedure TFramePackageTmpl.SetEditDataSets(const Value: TArrayDS);
var
  i: Integer;
  DS: TArrayDS;
begin
  DS := FEditDataSets;
  if FEditDataSets <> Value then
  begin
    FEditDataSets := Value;
    for i := Low(DS) to High(DS) do
      DataSetCustomSet(DS[i]);
    if Length(Value) <> 0 then
      for i := Low(Value) to High(Value) do
        DataSetAssign(Value[i])
    else
      SyncButtonsState;
  end;
end;

procedure TFramePackageTmpl.SetInsertDataSets(const Value: TArrayDS);
var
  i: Integer;
  DS: TArrayDS;
begin
  DS := FInsertDataSets;
  if FInsertDataSets <> Value then
  begin
    FInsertDataSets := Value;
    for i := Low(DS) to High(DS) do
      DataSetCustomSet(DS[i]);
    if Length(Value) <> 0 then
      for i := Low(Value) to High(Value) do
        DataSetAssign(Value[i])
    else
      SyncButtonsState;
  end;
end;

procedure TFramePackageTmpl.SetNavigationDataSet(
  const Value: TevClientDataSet);
var
  DS: TevClientDataSet;
begin
  if FNavigationDataSet <> Value then
  begin
    DS := FNavigationDataSet;
    FNavigationDataSet := Value;
    DataSetCustomSet(DS);
    if Value <> nil then
      DataSetAssign(Value)
    else
      SyncButtonsState;
  end;
end;

procedure TFramePackageTmpl.SetSupportDataSets(const Value: TArrayDS);
begin
  FSupportDataSets := Value;
end;


procedure TFramePackageTmpl.btnNavFirstClick(Sender: TObject);
var
  c: TWinControl;

  function FindDSControl(wc: TWinControl; TableName, FieldName: String): TWinControl;
  var
    j: Integer;
    ds: TDataSource;
    df: string;
  begin
    Result := nil;

    for j := 0 to Pred(wc.ControlCount) do
    begin
      if wc.Controls[j] is TWinControl then
      begin
        if IsPublishedProp(wc.Controls[j], 'DataSource') and
           IsPublishedProp(wc.Controls[j], 'DataField') then
        begin
          ds := TDataSource(GetObjectProp(wc.Controls[j], 'DataSource'));
          df := GetStrProp(wc.Controls[j], 'DataField');
          if Assigned(ds) and Assigned(ds.DataSet) and
             (ds.DataSet.Name = TableName) and
             (df = FieldName) then
          begin
            Result := TWinControl(wc.Controls[j]);
            Exit;
          end;
        end
        else
        begin
          Result := FindDSControl(TWinControl(wc.Controls[j]), TableName, FieldName);
          if Assigned(Result) then
            Exit;
        end;
      end;
    end;
  end;

begin
  try
    ClickButton(TComponent(Sender).Tag);
  except
    on E: EInvalidRecord do
    begin
      c := FindDSControl(Self, E.TableName, E.FieldName);
      if Assigned(c) then
      begin
        c.Show;
        if c.CanFocus then
          c.SetFocus;
      end;
      raise;
    end
    else
      raise;
  end;
end;

procedure TFramePackageTmpl.DataSetScroll(DataSet: TDataSet);
begin
  SyncButtonsState;
end;

procedure TFramePackageTmpl.DoInsertDataSets(DataSet: TDataSet);
var
  i: Integer;
  a: array of TDataSetNotifyEvent;
begin
  FInserting := True;
  try
    SetLength(a, Length(FInsertDataSets));
    for i := Low(FInsertDataSets) to High(FInsertDataSets) do
      if FInsertDataSets[i] <> DataSet then
      begin
        a[i] := FInsertDataSets[i].CustomBeforeInsert;
        FInsertDataSets[i].CustomBeforeInsert := nil;
      end;
    try
      for i := Low(FInsertDataSets) to High(FInsertDataSets) do
        if FInsertDataSets[i] <> DataSet then
        begin
          if not FInsertDataSets[i].Active then
            FInsertDataSets[i].Open;
          if FInsertDataSets[i].State <> dsInsert then
            FInsertDataSets[i].Insert;
        end;
    finally
      for i := Low(FInsertDataSets) to High(FInsertDataSets) do
        if FInsertDataSets[i] <> DataSet then
          FInsertDataSets[i].CustomBeforeInsert := a[i];
    end;
  finally
    FInserting := False;
  end;
end;

procedure TFramePackageTmpl.DataSetInsert(DataSet: TDataSet);
begin
  if not FInserting and IsInsertDataSet(DataSet) then
    if Length(InsertDataSets) > 1 then
    begin
      DoInsertDataSets(nil);
      AbortEx;
    end
    else
      DoInsertDataSets(DataSet);
end;

procedure TFramePackageTmpl.DataSetClose(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetCommit(DataSet: TDataSet);
begin
  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetDelete(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  if IsDeleteDataSet(DataSet) or IsEditDataSet(DataSet) then
    AddPostDataSet(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetEdit(DataSet: TDataSet);
begin
  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetOpen(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  SyncButtonsState;
end;

function TFramePackageTmpl.IsDeleteDataSet(DataSet: TDataSet): Boolean;
begin
  Result := DataSet = DeleteDataSet;
end;

function TFramePackageTmpl.IsEditDataSet(DataSet: TDataSet): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(EditDataSets) to High(EditDataSets) do
    if EditDataSets[i] = DataSet then
    begin
      Result := True;
      Exit;
    end;
end;

function TFramePackageTmpl.IsInsertDataSet(DataSet: TDataSet): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(InsertDataSets) to High(InsertDataSets) do
    if InsertDataSets[i] = DataSet then
    begin
      Result := True;
      Exit;
    end;
end;

function TFramePackageTmpl.IsNavigationDataSet(DataSet: TDataSet): Boolean;
begin
  Result := DataSet = NavigationDataSet;
end;

procedure TFramePackageTmpl.DataSetCancel(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetPost(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  if IsInsertDataSet(DataSet) or IsEditDataSet(DataSet) then
    AddPostDataSet(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.SetButton(Kind: Integer; Value: Boolean);
begin
  if Assigned(ActiveFrame) then
    ActiveFrame.OnSetButton(Kind, Value);
  SetOrdProp(ButtonByKind(Kind), 'Enabled', Ord(Value));
end;

function TFramePackageTmpl.IsPostDataSet(DataSet: TDataSet): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(FDataSetsToPost) to High(FDataSetsToPost) do
    if FDataSetsToPost[i] = DataSet then
    begin
      Result := True;
      Exit;
    end;
end;

function TFramePackageTmpl.IsAllBrowse(aDS: TArrayDS): Boolean;
var
  i: Integer;
begin
  if Length(aDS) = 0 then
  begin
    Result := False;
    Exit;
  end;

  Result := True;
  for i := Low(aDS) to High(aDS) do
    if not aDS[i].Active or (aDS[i].State <> dsBrowse) then
    begin
      Result := False;
      Exit;
    end;
end;

function TFramePackageTmpl.ButtonByKind(Kind: Integer): TControl;
begin
  case Kind of
    NavFirst:     Result := btnNavFirst;
    NavPrevious:  Result := btnNavPrevious;
    NavNext:      Result := btnNavNext;
    NavLast:      Result := btnNavLast;
    NavInsert:    Result := btnNavInsert;
    NavDelete:    Result := btnNavDelete;
    NavHistory:   Result := btnNavHistory;
    NavOK:        Result := btnNavOk;
    NavCancel:    Result := btnNavCancel;
    NavCommit:    Result := btnNavCommit;
    NavAbort:     Result := btnNavAbort;
    NavRefresh:   Result := btnRefresh;
  else
    Result := nil;
  end;
end;

procedure TFramePackageTmpl.DataSetAssign(DataSet: TDataSet);
begin
  DataSetCustomSet(DataSet as TevClientDataSet);

  if DataSet.Active then
    DataSetOpen(DataSet)
  else
    DataSetClose(DataSet);
end;

procedure TFramePackageTmpl.DoCancelDataSets;
var
  i: Integer;
begin
  for i := Low(EditDataSets) to High(EditDataSets) do
    EditDataSets[i].DisableControls;
  try
    for i := Low(EditDataSets) to High(EditDataSets) do
      if EditDataSets[i].Active and (EditDataSets[i].State <> dsBrowse) then
        EditDataSets[i].Cancel;

    ActiveFrame.DoOnCancel;        
  finally
    for i := Low(EditDataSets) to High(EditDataSets) do
      EditDataSets[i].EnableControls;
  end;
end;

procedure TFramePackageTmpl.DoPostDataSets;
var
  i: Integer;
  b: Boolean;
begin
  b := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    ActiveFrame.DoBeforePost;

    for i := Low(EditDataSets) to High(EditDataSets) do
    begin
      if EditDataSets[i].Active and (EditDataSets[i].State <> dsBrowse) then
        EditDataSets[i].CheckBrowseMode;
      if FCommiting then
        AddPostDataSet(EditDataSets[i]);
    end;

    ActiveFrame.DoAfterPost;
  finally
    ctx_DataAccess.TempCommitDisable := b;
  end;

  if not ctx_DataAccess.DelayedCommit then
    ClickButton(NavCommit);
end;

procedure TFramePackageTmpl.AddPostDataSet(DataSet: TDataSet);
begin
  if not IsPostDataSet(DataSet) and ((DataSet as TEvBasicClientDataSet).ChangeCount <> 0) then
  begin
    AddDS(DataSet as TevClientDataSet, FDataSetsToPost);
    SyncButtonsState;
  end;

  if not ctx_DataAccess.DelayedCommit then
    ClickButton(NavCommit);
end;

procedure TFramePackageTmpl.DataSetCustomSet(DataSet: TevClientDataSet);
begin
  if not Assigned(DataSet) then
    Exit;
  if IsDeleteDataSet(DataSet) or IsNavigationDataSet(DataSet) then
    DataSet.CustomAfterFiltering := DataSetFilter
  else
    DataSet.CustomAfterFiltering := nil;
  if IsPostDataSet(DataSet) then
    DataSet.CustomAfterCommit := DataSetCommit
  else
    DataSet.CustomAfterCommit := nil;
  if IsPostDataSet(DataSet) then
    DataSet.CustomAfterAbort := DataSetAbort
  else
    DataSet.CustomAfterAbort := nil;
  if IsPostDataSet(DataSet) or IsEditDataSet(DataSet) then
    DataSet.CustomAfterPost := DataSetPost
  else
    DataSet.CustomAfterPost := nil;
  if IsPostDataSet(DataSet) or IsEditDataSet(DataSet) or IsDeleteDataSet(DataSet) or IsNavigationDataSet(DataSet) then
    DataSet.CustomAfterDelete := DataSetDelete
  else
    DataSet.CustomAfterDelete := nil;
  if IsPostDataSet(DataSet) or IsDeleteDataSet(DataSet) or IsEditDataSet(DataSet) or IsNavigationDataSet(DataSet) then
    DataSet.CustomAfterOpen := DataSetOpen
  else
    DataSet.CustomAfterOpen := nil;
  if IsPostDataSet(DataSet) or IsDeleteDataSet(DataSet) or IsEditDataSet(DataSet) or IsNavigationDataSet(DataSet) then
    DataSet.CustomAfterClose := DataSetClose
  else
    DataSet.CustomAfterClose := nil;
  if IsEditDataSet(DataSet) then
    DataSet.CustomAfterEdit := DataSetEdit
  else
    DataSet.CustomAfterEdit := nil;
  if IsEditDataSet(DataSet) then
    DataSet.CustomAfterCancel := DataSetCancel
  else
    DataSet.CustomAfterCancel := nil;
  if IsInsertDataSet(DataSet) then
    DataSet.CustomBeforeInsert := DataSetInsert
  else
    DataSet.CustomBeforeInsert := nil;
  if IsNavigationDataSet(DataSet) then
    DataSet.CustomAfterScroll := DataSetScroll
  else
    DataSet.CustomAfterScroll := nil;
end;

function TFramePackageTmpl.GetFullCaption(Item: TMenuItem): string;
begin
  Result := '';
  if Assigned(Item.Parent) then
    Result := GetFullCaption(Item.Parent);
  if Result = '' then
    Result := Item.Caption
  else
    Result := Result +  ' - ' + Item.Caption;
  Result := StringReplace(Result, '&', '', [rfReplaceAll]);
end;

procedure TFramePackageTmpl.DoAbortDataSets;
var
  i: Integer;
begin
  for i := Low(FDataSetsToPost) to High(FDataSetsToPost) do
    if FDataSetsToPost[i].Active then
      FDataSetsToPost[i].CancelUpdates;
  SetLength(FDataSetsToPost, 0);
  SyncButtonsState;  
end;

procedure TFramePackageTmpl.DataSetFilter(DataSet: TDataSet);
begin
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.DataSetAbort(DataSet: TDataSet);
begin
  DoCancelDataSets;
  if IsNavigationDataSet(DataSet) then
    DataSetScroll(DataSet);

  SyncButtonsState;
end;

procedure TFramePackageTmpl.ClickButton(Kind: Integer);
var
  Handled: Boolean;
begin
  if not ButtonEnabled(Kind) then
    Exit;

  if (Kind <> NavHistory) and
     (Application.MainForm.ActiveControl is TevDBLookupCombo) then
    WorkPanel.SetFocus;

  Handled := False;
  if Assigned(ActiveFrame) then
    ActiveFrame.ButtonClicked(Kind, Handled);

  if Handled then
    Exit;

  if (Kind in [NavFirst, NavPrevious, NavNext, NavLast]) and
     (btnNavOk.Enabled or btnNavCancel.Enabled) then
    ClickButton(NavOK);

  if Kind = NavFirst then
    NavigationDataSet.First;

  if Kind = NavPrevious then
    NavigationDataSet.Prior;

  if Kind = NavNext then
    NavigationDataSet.Next;

  if Kind = NavLast then
    NavigationDataSet.Last;

  if Kind = NavInsert then
  begin
    if Assigned(ActiveFrame) and
       (ActiveFrame.GetInsertControl <> nil) then
      with ActiveFrame.GetInsertControl do
      begin
        Show;
        if CanFocus then
          SetFocus;
      end;
    DoInsertDataSets(nil);
  end;

  if Kind = NavDelete then
    if ActiveFrame.DeleteMessage = mrOK then
      DoDeleteDataSets;

  if Kind = NavOK then
    DoPostDataSets;

  if Kind = NavCancel then
    DoCancelDataSets;

  if Kind = NavCommit then
    DoCommitDataSets;

  if Kind = NavAbort then
    DoAbortDataSets;

  if Kind = NavHistory then
    ShowAudit('F');

  if not FCommiting and Assigned(ActiveFrame) then
    ActiveFrame.AfterClick(Kind);
end;

function TFramePackageTmpl.ButtonEnabled(Kind: Integer): Boolean;
begin
  Result := Boolean(GetOrdProp(ButtonByKind(Kind), 'Enabled'));
end;

function TFramePackageTmpl.IsModified: Boolean;
begin
  Result := not IsAllCommitted or not IsAllBrowse(EditDataSets);
end;

procedure TFramePackageTmpl.OpenFrame(FClass: string);
begin
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_FRAME, -1, Integer(Pointer(FindClass(FClass))));
end;

procedure TFramePackageTmpl.dtpAsOfDateChange(Sender: TObject);
begin
  SyncAsOfDateColor;
end;

procedure TFramePackageTmpl.btnNavBackScreenClick(Sender: TObject);
begin
  Dec(HistoryListPosition);
  GotoScreen(HistoryListPosition);
end;

procedure TFramePackageTmpl.btnNavForwardScreenClick(Sender: TObject);
begin
  Inc(HistoryListPosition);
  GotoScreen(HistoryListPosition);
end;

procedure TFramePackageTmpl.CheckButtons;
begin
  btnNavForwardScreen.Enabled := HistoryListPosition < Pred(HistoryList.Count);
  btnNavBackScreen.Enabled := HistoryListPosition > 0;

  if btnNavForwardScreen.Enabled then
    btnNavForwardScreen.Hint := 'Forward to ' + HistoryList[Succ(HistoryListPosition)] + ' (' + ShortCutToText(btnNavForwardScreen.ShortCut) + ')'
  else
    btnNavForwardScreen.Hint := '';
  if btnNavBackScreen.Enabled then
    btnNavBackScreen.Hint := 'Back to ' + HistoryList[Pred(HistoryListPosition)] + ' (' + ShortCutToText(btnNavBackScreen.ShortCut) + ')'
  else
    btnNavBackScreen.Hint := '';
end;

procedure TFramePackageTmpl.AddScreenHistory(sCaption: string);
begin
  if not HistoryMoving then
  begin
    while HistoryListPosition < Pred(HistoryList.Count) do
      HistoryList.Delete(Pred(HistoryList.Count));
    HistoryList.Add(sCaption);
    Inc(HistoryListPosition);
    OldHistPos := HistoryListPosition;
  end;
  HistoryMoving := False;
  CheckButtons;
end;

procedure TFramePackageTmpl.HistoryMenuClick(Sender: TObject);
begin
  with TMenuItem(Sender) do
    if GetParentMenu = BackDDMenu then
      Dec(HistoryListPosition, Succ(MenuIndex))
    else
      Inc(HistoryListPosition, Succ(MenuIndex));
  GotoScreen(HistoryListPosition);
end;

procedure TFramePackageTmpl.BackDDMenuPopup(Sender: TObject);
var
  i: Integer;
begin
  while BackDDMenu.Items.Count > 0 do
    BackDDMenu.Items[0].Free;

  for i := 1 to 7 do
  begin
    if HistoryListPosition - i < 0 then
      Break;
    BackDDMenu.Items.Add(NewItem(HistoryList[HistoryListPosition - i], 0, False, True, HistoryMenuClick, 0, ''));
  end;
end;

procedure TFramePackageTmpl.ForwardDDMenuPopup(Sender: TObject);
var
  i: Integer;
begin
  while ForwardDDMenu.Items.Count > 0 do
    ForwardDDMenu.Items[0].Free;

  for i := 1 to 7 do
  begin
    if HistoryListPosition + i >= HistoryList.Count then
      Break;
    ForwardDDMenu.Items.Add(NewItem(HistoryList[HistoryListPosition + i], 0, False, True, HistoryMenuClick, 0, ''));
  end;;
end;

procedure TFramePackageTmpl.GotoScreen(iPos: Integer);
begin
  HistoryMoving := True;
  if SendMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar(HistoryList[HistoryListPosition] + #0)), 0) = 1 then
    OldHistPos := HistoryListPosition
  else
    HistoryListPosition := OldHistPos;
  CheckButtons;
end;

procedure TFramePackageTmpl.GotoScreen(sName: string);
begin
  GotoScreen(HistoryList.IndexOf(sName));
end;

procedure TFramePackageTmpl.AutoSave(Minutes: Integer);
begin
  if Minutes <= 0 then
    AutoSaveTimer.Enabled := False
  else
  begin
    AutoSaveTimer.Interval := Minutes * 60000;
    AutoSaveTimer.Enabled := True;
  end;
end;

procedure TFramePackageTmpl.AutoSaveTimerTimer(Sender: TObject);
begin
  if btnNavCommit.Enabled then
  try
    ClickButton(NavCommit);
  except
  end;
end;

procedure TFramePackageTmpl.DoCommitDataSets;
begin
  if FCommiting then
    Exit;

  FCommiting := True;
  try
    DoPostDataSets;
  finally
    FCommiting := False;
  end;

  CommitDataSets;
end;

procedure TFramePackageTmpl.btnRefreshClick(Sender: TObject);
var
  sFrameClass: string;
  Handled: Boolean;
begin
  SetFocus;
  Handled := False;
  if Assigned(ActiveFrame) then
    ActiveFrame.ButtonClicked(NavRefresh, Handled);
  if Handled then
    Exit;

  if CanClose and AskCommitChanges then
  begin
    InternalRefresh;

    sFrameClass := '';
    if Assigned(ActiveFrame) then
      sFrameClass := ActiveFrame.ClassName;
    ActivateFrame(nil, '');
    if sFrameClass <> '' then
      OpenFrame(sFrameClass);
  end;
end;

procedure TFramePackageTmpl.miFieldAuditClick(Sender: TObject);
begin
  ShowAudit('F');
end;

procedure TFramePackageTmpl.aRecordAuditExecute(Sender: TObject);
begin
  ShowAudit('R');
end;

procedure TFramePackageTmpl.FillImages;
var
  i: integer;
  bitmap: TBitmap;
begin
  for i := 0 to MenuImageList.count-1 do
  begin
    bitmap := TBitmap.Create;
    MenuImageList.GetBitmap( i, bitmap );
    bitmap.Transparent := true;
    bitmap.TransparentMode := tmAuto;
    FImages.Add( bitmap );
  end;
end;

function TFramePackageTmpl.GetBitmap( index: integer ): TBitmap;
begin
  if Assigned(FImages) then
    Result := FImages[index] as TBitmap
  else
    Result := nil;
end;

function TFramePackageTmpl.PackageBitmap: TBitmap;
begin
  Result := nil;
end;


procedure TFramePackageTmpl.FrameResize(Sender: TObject);
begin
  LogoPresenter.AlignLogo;
end;

procedure TFramePackageTmpl.InternalRefresh;
begin
  if CanClose and AskCommitChanges then
  begin
    if Trunc(dtpAsOfDate.Date) = SysDate then
      ctx_DataAccess.AsOfDate := EmptyDate
    else
      ctx_DataAccess.AsOfDate := Trunc(dtpAsOfDate.Date);

    ctx_DataAccess.ClearCache();
  end;
end;

procedure TFramePackageTmpl.HandleGlobalLookupRequest(frameclass: TClass);
begin
  _DSMementoManager.ActivatingFrameDueToGlobalLookup( ActiveFrame.ClassName, EditDataSets, frameclass.ClassName );
  if IsAllCommitted and not IsAllBrowse(EditDataSets) then
    ClickButton( NavCancel );
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_FRAME, -2, Integer(Pointer(frameclass)));
end;

procedure TFramePackageTmpl.StoreEditDSForswitchScreen(frameclass: TClass);
begin
  _DSMementoManager.ActivatingFrameDueToGlobalLookup( ActiveFrame.ClassName, EditDataSets, frameclass.ClassName );
  if IsAllCommitted and not IsAllBrowse(EditDataSets) then
    ClickButton( NavCancel );
end;

destructor TFramePackageTmpl.Destroy;
begin
  UninitPackage;
  inherited;
end;

procedure TFramePackageTmpl.SyncAsOfDate;
begin
  dtpAsOfDate.Date := ctx_DataAccess.AsOfDate;
  SyncAsOfDateColor;
end;

procedure TFramePackageTmpl.SyncAsOfDateColor;
begin
  if Trunc(dtpAsOfDate.Date) <> SysDate then
    dtpAsOfDate.Color := clYellow
  else
    dtpAsOfDate.Color := clWindow;
end;

procedure TFramePackageTmpl.ShowAudit(const AType: Char);
var
  ClientDataSet: TevClientDataSet;
  Src: TevDataSource;
  sTableName: string;
  sDataField: string;
  Fld,FldCalc: TField;
  C: TWinControl;
  DBType: TevDBType;
begin
  C := FindControl(GetFocus);
  if Assigned(C) then
    if (C.Owner is TCustomGrid) or (C.Owner is TCustomRadioGroup) then
      C := C.Owner as TWinControl;

  if not Assigned(c) or not IsPublishedProp(C, 'DataSource') then
    Exit;

  Src := GetObjectProp(C, 'DataSource') as TevDataSource;
  if not Assigned(Src) then
    Exit;

  if not (Src.DataSet is TevClientDataSet) then
     exit;

  ClientDataSet := Src.DataSet as TevClientDataSet;
  if not Assigned(ClientDataSet) then
    Exit;

  sTableName := ClientDataSet.TableName;
  if sTableName = '' then
    Exit;

  if AType = 'D' then
  begin
    DBType := GetDBTypeByTable(sTableName);
    if DBType in [dbtSystem, dbtBureau, dbtClient] then
      TevAuditView.ShowDatabaseAudit(DBType, Trunc(IncMonth(SysDate, -1)));
    Exit;
  end;

  if AType = 'T' then
  begin
    TevAuditView.ShowTableAudit(sTableName, EmptyDate);
    Exit;
  end;

  if ClientDataSet.RecordCount > 0 then
  begin
    Fld := ClientDataSet.FindField(sTableName + '_NBR');
    if not Assigned(Fld) or (Fld.FieldKind <> fkData) then
      Exit;
  end
  else
    Exit;

  sDataField := '';
  if AType = 'F' then
    if IsPublishedProp(C, 'DataField') then
      sDataField := GetStrProp(C, 'DataField');

  if sDataField = 'SOCCodeMask' then
    sDataField := 'SOC_CODE'; // SOCCodeMask is a calc field to display SOC_CODE value with dash in the 2nd position. Need to show SOC_CODE audit history

  if sDataField <> '' then
  begin
    FldCalc := ClientDataSet.FindField(sDataField);
    if not Assigned(FldCalc) or (FldCalc.FieldKind <> fkData) then
    begin
      evmessage('Field Audit History is not available for this field. ');
      Exit;
    end;

    TevAuditView.ShowFieldAudit(sTableName, sDataField, Fld.AsInteger, EmptyDate);
  end
  else
    TevAuditView.ShowRecordAudit(sTableName, Fld.AsInteger, EmptyDate);
end;

function TFramePackageTmpl.AskCommitChanges: Boolean;
begin
  Result := True;

  if btnNavOk.Enabled or btnNavCancel.Enabled then
  begin
    EvErrMessage('You must post or cancel changes.');
    Result := False;
    Exit;
  end;

  if not IsAllCommitted then
  begin
    case EvMessage('Do you want to commit changes?', mtConfirmation, mbYesNoCancel) of
    mrYes:
      CommitDataSets;
    mrNo:
      if EvMessage('YOU WILL LOSE YOUR CHANGES!!!', mtConfirmation, [mbIgnore, mbCancel]) = mrIgnore then
        DoAbortDataSets
      else
        Result := False;
    else
      Result := False;
    end;
  end;
end;

function TFramePackageTmpl.IsAllCommitted: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := Low(FDataSetsToPost) to High(FDataSetsToPost) do
    if FDataSetsToPost[i].Active and (FDataSetsToPost[i].ChangeCount > 0) then
    begin
      Result := False;
      Exit;
    end;
end;

function TFramePackageTmpl.IsSomeEdit(aDS: TArrayDS): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(aDS) to High(aDS) do
    if aDS[i].State in [dsEdit, dsInsert] then
    begin
      Result := True;
      Exit;
    end;
end;

procedure TFramePackageTmpl.CommitDataSets;
var
  err: Boolean;
begin
  err := False;
  ActiveFrame.DoBeforeCommit;
  try
    try
      ctx_DataAccess.PostDataSets(FDataSetsToPost, True);
    except
      err := True;
      raise;
    end;
  finally
    ActiveFrame.DoAfterCommit(err);
  end;

  SetLength(FDataSetsToPost, 0);
  SyncButtonsState;
end;

procedure TFramePackageTmpl.SyncButtonsState;
var
  b: Boolean;
begin
  if Assigned(NavigationDataSet) and NavigationDataSet.Active then
  begin
    SetButton(NavFirst, NavigationDataSet.RecNo > 1);
    SetButton(NavPrevious, NavigationDataSet.RecNo > 1);
    SetButton(NavNext, not NavigationDataSet.IsEmpty and (NavigationDataSet.RecNo < NavigationDataSet.RecordCount));
    SetButton(NavLast, not NavigationDataSet.IsEmpty and (NavigationDataSet.RecNo < NavigationDataSet.RecordCount));
  end
  else
  begin
    SetButton(NavFirst, False);
    SetButton(NavPrevious, False);
    SetButton(NavNext, False);
    SetButton(NavLast, False);
  end;

  b := IsSomeEdit(InsertDataSets) or IsSomeEdit(EditDataSets);

  SetButton(NavInsert, IsAllBrowse(InsertDataSets));
  SetButton(NavDelete, not b and Assigned(DeleteDataSet) and (DeleteDataSet.State = dsBrowse) and (DeleteDataSet.RecordCount > 0));

  SetButton(NavOK, b);
  SetButton(NavCancel, b);

  b := not IsAllCommitted;
  SetButton(NavCommit, b);
  SetButton(NavAbort, b);
end;

procedure TFramePackageTmpl.PrepareVersionedFields;

  procedure CheckParent(const AParent: TWinControl);
  var
    i: Integer;
    C: TControl;
  begin
    for i := 0 to AParent.ControlCount - 1 do
    begin
      C := AParent.Controls[i];

      if Supports(C, IevDBFieldControl) then
        if (C as IevDBFieldControl).IsVersioned then
          (C as IevDBFieldControl).PrepareVersionedLook;

      if C is TWinControl then
        CheckParent(TWinControl(C));
    end;
  end;

begin
  if Assigned(ActiveFrame) then
    CheckParent(ActiveFrame);

  //Insert button incorrectly activating for some screens, when it should not.
  SyncButtonsState;
end;

procedure TFramePackageTmpl.DoDeleteDataSets;
var
  i: Integer;
  b: Boolean;
begin
  b := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    ActiveFrame.DoBeforeDelete;

    for i := Low(EditDataSets) to High(EditDataSets) do
    begin
      if EditDataSets[i].Active and (EditDataSets[i].State <> dsBrowse) then
        EditDataSets[i].CheckBrowseMode;
      if FCommiting then
        AddPostDataSet(EditDataSets[i]);
    end;

    DeleteDataSet.CheckBrowseMode;
    DeleteDataSet.Delete;

    ActiveFrame.DoAfterDelete;
  finally
    ctx_DataAccess.TempCommitDisable := b;
  end;

  if not ctx_DataAccess.DelayedCommit then
    ClickButton(NavCommit);
end;

procedure TFramePackageTmpl.aTableAuditExecute(Sender: TObject);
begin
  ShowAudit('T');
end;

procedure TFramePackageTmpl.aDatabaseAuditExecute(Sender: TObject);
begin
  ShowAudit('D');
end;

procedure TFramePackageTmpl.SetPostingDataSetLast(DataSet: TDataSet);
begin
  if IsPostDataSet(DataSet) then
  begin
    RemoveDS(DataSet as TevClientDataSet, FDataSetsToPost);
    AddDS(DataSet as TevClientDataSet, FDataSetsToPost);
  end;
end;

procedure TFramePackageTmpl.HideToolBar;
begin
  Panel.Hide;
end;

procedure TFramePackageTmpl.ShowToolBar;
begin
  Panel.Show;
end;

initialization
  HistoryList := TStringList.Create;
  _DSMementoManager := CreateDSMementoManager;

finalization
  HistoryList.Free;
  _DSMementoManager := nil;

end.
