unit EvVersionedW2PersonNameFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, EvVersionedPersonNameFrm;

type
  TevVersionedW2PersonName = class(TevVersionedPersonName)
    lNameSuffix: TevLabel;
    edNameSuffix: TevDBEdit;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}

procedure TevVersionedW2PersonName.FormShow(Sender: TObject);
begin
  inherited;

  Fields[1].AddValue('Width', 18);

  Fields[3].AddValue('Title', lNameSuffix.Caption);
  Fields[3].AddValue('Width', 18);
  lNameSuffix.Caption := Iff(Fields[3].Value['Required'], '~', '') + lNameSuffix.Caption;
  edNameSuffix.DataField := Fields.ParamName(3);
end;

end.

