// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit gdystrset;

interface

type
  TStringSet = array of string;

function InSet( const s: string; const ss: array of string ): boolean;
procedure SetInclude( var ss: TStringSet; const s: string );
procedure SetExclude( var ss: TStringSet; const s: string );
procedure SetAssign( var ss: TStringSet; const ass: array of string );
function SetEqual( const ss1: array of string; const ss2: array of string ): boolean;
procedure SetClear(var ss: TStringSet);
function SetToStr( const ss: array of string; const separator: string = ';' ): string;
function SetDiff( const ss1: array of string; const ss2: array of string ): TStringSet;
function SetValid( const ss: array of string ): boolean;
function SetIntersects( const ss1: array of string; const ss2: array of string ): boolean;
function SetUnion( const ss1: array of string; const ss2: array of string ): TStringSet;

function InList(const s: integer; const ss: array of integer ): boolean; overload;
function InList(const s: string; const ss: array of string ): boolean; overload;

implementation

function InList(const s: integer; const ss: array of integer ): boolean;
var
  i: integer;
begin
  Result := false;
  for i := low(ss) to high(ss) do
    if s = ss[i] then
    begin
      Result := true;
      break;
    end;
end;

function InList(const s: string; const ss: array of string ): boolean; 
var
  i: integer;
begin
  Result := false;
  for i := low(ss) to high(ss) do
    if s = ss[i] then
    begin
      Result := true;
      break;
    end;
end;

function InSet( const s: string; const ss: array of string ): boolean;
begin
  Result := InList(s, ss);
end;

procedure SetInclude( var ss: TStringSet; const s: string );
begin
  if not InSet(s, ss) then
  begin
    SetLength( ss, Length(ss)+1 );
    ss[high(ss)] := s;
  end;
end;

procedure SetExclude( var ss: TStringSet; const s: string );
var
  i: integer;
  disp: integer;
begin
  disp := 0;
  for i := low(ss) to high(ss) do
    if s = ss[i] then
      disp := 1
    else
      if disp <> 0 then
        ss[i-disp] := ss[i];
  if disp <> 0 then
    SetLength( ss, Length(ss) - disp );
end;

procedure SetAssign( var ss: TStringSet; const ass: array of string );
var
  i: integer;
begin
  SetLength( ss, Length(ass) );
  for i := low(ass) to high(ass) do
    ss[i] := ass[i];
end;

function SetEqual( const ss1: array of string; const ss2: array of string ): boolean;
var
  i: integer;
begin
  Result := Length(ss1) = Length(ss2);
  if Result then
    for i := low(ss1) to high(ss1) do
      if not InSet( ss1[i], ss2 ) then
      begin
        Result := false;
        break;
      end;
end;

procedure SetClear(var ss: TStringSet);
begin
  SetLength(ss, 0);
end;

function SetToStr( const ss: array of string; const separator: string ): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to Length(ss)-1 do
  begin
    if Result <> '' then
      Result := Result + separator;
    Result := Result + ss[i];
  end;
end;

function SetDiff( const ss1: array of string; const ss2: array of string ): TStringSet;
var
  i: integer;
begin
  SetAssign( Result, ss1 );
  for i := low(ss2) to high(ss2) do
    if InSet( ss2[i], Result ) then
      SetExclude( Result, ss2[i] );
end;

function SetUnion( const ss1: array of string; const ss2: array of string ): TStringSet;
var
  i: integer;
  p: integer;
begin
  SetAssign( Result, ss1 );
  p := Length(ss1);
  SetLength( Result, Length(ss1) + Length(ss2));

  for i := low(ss2) to high(ss2) do
    if not InSet( ss2[i], ss1 ) then
    begin
      Result[p] := ss2[i];
      inc(p);
    end;
  SetLength(Result, p);
end;

function SetIntersects( const ss1: array of string; const ss2: array of string ): boolean;
var
  i: integer;
begin
  Result := false;
  for i := low(ss1) to high(ss1) do
    if InSet( ss1[i], ss2 ) then
    begin
      Result := true;
      break;
    end
end;

function SetValid( const ss: array of string ): boolean;
var
  i, j: integer;
begin
  Result := true;
  for i := low(ss) to high(ss) do
    for j := i+1 to high(ss) do
      if ss[i] = ss[j] then
      begin
        Result := false;
        break;
      end
end;

end.
