inherited evVersionedCLCOCons: TevVersionedCLCOCons
  Left = 468
  Top = 317
  ClientHeight = 366
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 366
    inherited grFieldData: TevDBGrid
      Height = 239
    end
    inherited pnlEdit: TevPanel
      Top = 283
      object lValue: TevLabel [2]
        Left = 243
        Top = 11
        Width = 110
        Height = 13
        Caption = 'Consolidated Reporting'
      end
      inherited pnlButtons: TevPanel
        Top = 19
      end
      object cbClCoCons: TevDBLookupCombo
        Left = 243
        Top = 26
        Width = 184
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'20'#9'Description'#9'F')
        DataField = 'CL_CO_CONSOLIDATION_NBR'
        DataSource = dsFieldData
        LookupTable = DM_CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION
        LookupField = 'CL_CO_CONSOLIDATION_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 376
    Top = 64
  end
end
