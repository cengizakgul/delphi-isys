unit rwModalPreviewFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rwPreviewContainerFrm;

type
  TrwModalPreviewForm = class(TForm)
    rwPreviewContainer: TrwPreviewContainer;
    procedure rwPreviewContainerResize(Sender: TObject);
  private
  public
  end;

implementation

{$R *.dfm}

procedure TrwModalPreviewForm.rwPreviewContainerResize(Sender: TObject);
begin
  rwPreviewContainer.FrameResize(Sender);
end;

end.
