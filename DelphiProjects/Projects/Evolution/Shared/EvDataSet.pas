unit EvDataset;

interface

uses SysUtils, DB,
     isBaseClasses, isDataSet, isBasicUtils, EvCommonInterfaces, EvStreamUtils, EvContext,
     isTypes, evClientDataSet, ISKbmMemDataSet, EvTypes;

type
  TevDataSet = class(TisDataSet, IevDataSet)
  protected
    function  CreateVCLDataSet: TisKbmMemDataSet; override;
  public
    class function  GetTypeID: String; override;
  end;

  
  TevQuery = class(TisInterfacedObject, IevQuery)
  private
    FSelect: Boolean;
    FExecuted: Boolean;
    FResult: IevDataSet;
    FQueryName: String;
    FParams: IisListOfValues;
    FMacros: IisListOfValues;
    FLoadBlobs: Boolean;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  QueryName: String;
    function  GetParams: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    function  GetMacros: IisListOfValues;
    procedure SetMacros(const AValue: IisListOfValues);
    procedure Execute;
    function  Result: IEvDataSet;
    procedure SetParam(Index: String; Value: Variant);
    procedure SetMacro(Index: String; Value: Variant);
    function  GetLoadBlobs: Boolean;
    procedure SetLoadBlobs(const AValue: Boolean);
  public
    class function GetTypeID: String; override;
    constructor Create(const AQueryName: String; const ASelectStatement: Boolean = True); reintroduce;
    constructor CreateFmt(const AQueryName: String; const AMacros: array of const; const ASelectStatement: Boolean = True);
    constructor CreateAsOf(const AQueryName: String; const CheckDate: TDateTime);
  end;


  TevStoredProc = class(TisInterfacedObject, IevStoredProc)
  private
    FResult: IisListOfValues;
    FProcName: String;
    FParams: IisListOfValues;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  ProcName: String;
    function  GetParams: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    procedure Execute;
    function  Result: IisListOfValues;
  public
    class function GetTypeID: String; override;
    constructor Create(const AProcName: String); reintroduce;
  end;


  TevTable = class(TisInterfacedObject, IevTable)
  private
    FTableName: String;
    FFieldList: TisCommaDilimitedString;
    FCondition: String;
    FAsOfDate:  TisDate;
    FDataSet: IevDataSet;
    function  DataSet: IevDataSet;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure Open;
    function  GetChangePacket: IevDataChangePacket;
    function  GetData: IisStream;
    procedure SetData(const AData: IisStream);
    function  TableName: String;
    function  FieldList: String;
    function  AsOfDate: TisDate;
    procedure SaveChanges;
    function  GetFields: TFields;
    function  GetBof: Boolean;
    function  GetEof: Boolean;
    function  GetFieldCount: Integer;
    function  GetRecordCount: Integer;
    function  GetRecNo: Integer;
    procedure SetRecNo(const AValue: Integer);
    function  GetFieldValue(const Index: string): Variant;
    procedure SetFieldValue(const Index: string; const AValue: Variant);
    function  GetIndexFieldNames: String;
    procedure SetIndexFieldNames(const AValue: String);
    function  GetFilter: String;
    procedure SetFilter(const AValue: String);
    function  GetFiltered: Boolean;
    procedure SetFiltered(const AValue: Boolean);
    function  GetBlobsLoadMode: TevBlobsLoadMode;
    procedure SetBlobsLoadMode(const AValue: TevBlobsLoadMode);
    procedure First;
    procedure Last;
    procedure Next;
    procedure Prior;
    procedure AppendRecord(const AValues: array of const);
    procedure Append;
    procedure Insert;
    procedure Delete;
    procedure DeleteAll;    
    procedure Edit;
    procedure Post;
    procedure Cancel;
    function  Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
    function  FindKey(const KeyValues:array of const): Boolean;
    function  FieldByName(const AFieldName: String): TField;
    procedure SetRange(const StartValues, EndValues:array of const);
    procedure CancelRange;
    function  IsEvBlobField(const AFieldName: String): Boolean;
    function  IsBlobLoaded(const AFieldName: String): Boolean;
    function  GetBlobData(const AFieldName: String): IisStream;
    procedure UpdateBlobData(const AFieldName: String; const AData: IisStream); overload;
    procedure UpdateBlobData(const AFieldName: String; const AData: String); overload;


  public
    class function GetTypeID: String; override;
    constructor Create(const aTableName: String;
                       const aFieldList: TisCommaDilimitedString = '';
                       const aCondition: String = '';
                       const aAsOfDate: TisDate = 0); reintroduce;

    constructor CreateAsQuery(const aTableName: String;
                       const aQuery: String;
                       const aAsOfDate: TisDate = 0);
  end;


implementation

uses EvDataAccessComponents, EvUtils;

{ TevTable }

procedure TevTable.Append;
begin
  DataSet.Append;
end;

procedure TevTable.AppendRecord(const AValues: array of const);
begin
  DataSet.AppendRecord(AValues);
end;

function TevTable.AsOfDate: TisDate;
begin
  if Assigned(FDataSet) then
    Result := TevClientDataSet(FDataSet.vclDataSet).AsOfDate  
  else
    Result := FAsOfDate;
end;

procedure TevTable.Cancel;
begin
  DataSet.Cancel;
end;

procedure TevTable.CancelRange;
begin
  DataSet.CancelRange;
end;

constructor TevTable.Create(const aTableName: String;
 const aFieldList: TisCommaDilimitedString; const aCondition: String; const aAsOfDate: TisDate);
begin
  inherited Create;
  FTableName := aTableName;
  FAsOfDate := aAsOfDate;
  FFieldList := aFieldList;
  FCondition := aCondition;
end;

constructor TevTable.CreateAsQuery(const aTableName, aQuery: String; const aAsOfDate: TisDate);
begin
  Create(aTableName, '', aQuery, aAsOfDate);
end;

function TevTable.DataSet: IevDataSet;
begin
  Open;
  Result := FDataSet;
end;

procedure TevTable.Delete;
begin
  DataSet.Delete;
end;

procedure TevTable.DeleteAll;
begin
  DataSet.DeleteAll;
end;

procedure TevTable.Edit;
begin
  DataSet.Edit;
end;

function TevTable.FieldByName(const AFieldName: String): TField;
begin
  Result := DataSet.FieldByName(AFieldName);
end;

function TevTable.FieldList: String;
begin
  Result := FFieldList;
end;

function TevTable.FindKey(const KeyValues: array of const): Boolean;
begin
  Result := DataSet.FindKey(KeyValues);
end;

procedure TevTable.First;
begin
  DataSet.First;
end;

function TevTable.GetBlobsLoadMode: TevBlobsLoadMode;
begin
  Result := TevClientDataSet(DataSet.vclDataSet).BlobsLoadMode;
end;

function TevTable.GetBof: Boolean;
begin
  Result := DataSet.GetBof;
end;

function TevTable.GetBlobData(const AFieldName: String): IisStream;
begin
  Result := TevClientDataSet(DataSet.vclDataSet).GetBlobData(AFieldName);
end;

function TevTable.GetEof: Boolean;
begin
  Result := DataSet.GetEof;
end;

function TevTable.GetFieldCount: Integer;
begin
  Result := DataSet.GetFieldCount;
end;

function TevTable.GetFields: TFields;
begin
  Result := DataSet.GetFields;
end;

function TevTable.GetFieldValue(const Index: string): Variant;
begin
  Result := DataSet.GetFieldValue(Index);
end;

function TevTable.GetFilter: String;
begin
  Result := DataSet.GetFilter;
end;

function TevTable.GetFiltered: Boolean;
begin
  Result := DataSet.GetFiltered;
end;

function TevTable.GetIndexFieldNames: String;
begin
  Result := DataSet.GetIndexFieldNames;
end;

function TevTable.GetRecNo: Integer;
begin
  Result := DataSet.GetRecNo;
end;

function TevTable.GetRecordCount: Integer;
begin
  Result := DataSet.GetRecordCount;
end;

class function TevTable.GetTypeID: String;
begin
  Result := 'evTable';
end;

procedure TevTable.Insert;
begin
  DataSet.Insert;
end;

procedure TevTable.Last;
begin
  DataSet.Last;
end;

function TevTable.Locate(const KeyFields: string; const KeyValues: Variant;
  Options: TLocateOptions): Boolean;
begin
  Result := DataSet.Locate(KeyFields, KeyValues, Options);
end;

procedure TevTable.Next;
begin
  DataSet.Next;
end;

procedure TevTable.Open;
var
  DS: TevClientDataSet;
begin
  if not Assigned(FDataSet) then
  begin
    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := AnsiUpperCase(FTableName) + '_PROV';
      DS.AsOfDate := FAsOfDate;
      DS.DataRequired(FFieldList, FCondition);
      FDataSet := TevDataSet.CreateForDataSet(DS);
    except
      DS.Free;
      raise;
    end;
  end;
end;

procedure TevTable.Post;
begin
  DataSet.Post;
end;

procedure TevTable.Prior;
begin
  DataSet.Prior;
end;

procedure TevTable.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FTableName := AStream.ReadShortString;
  FFieldList := AStream.ReadString;
  FCondition := AStream.ReadString;
  FAsOfDate := AStream.ReadInteger;
end;

procedure TevTable.SaveChanges;
begin
  if Assigned(FDataSet) then
    ctx_DataAccess.PostDataSets([TevClientDataSet(FDataSet.vclDataSet)]);
end;

procedure TevTable.SetBlobsLoadMode(const AValue: TevBlobsLoadMode);
begin
  TevClientDataSet(DataSet.vclDataSet).BlobsLoadMode := AValue;
end;

procedure TevTable.SetFieldValue(const Index: string; const AValue: Variant);
begin
  DataSet.SetFieldValue(Index, AValue);
end;

procedure TevTable.SetFilter(const AValue: String);
begin
  DataSet.SetFilter(AValue);
end;

procedure TevTable.SetFiltered(const AValue: Boolean);
begin
  DataSet.SetFiltered(AValue);
end;

procedure TevTable.SetIndexFieldNames(const AValue: String);
begin
  DataSet.SetIndexFieldNames(AValue);
end;

procedure TevTable.SetRange(const StartValues, EndValues: array of const);
begin
  DataSet.SetRange(StartValues, EndValues);
end;

procedure TevTable.SetRecNo(const AValue: Integer);
begin
  DataSet.SetRecNo(AValue);
end;

function TevTable.TableName: String;
begin
  Result := FTableName;
end;


procedure TevTable.UpdateBlobData(const AFieldName: String; const AData: IisStream);
begin
  TevClientDataSet(DataSet.vclDataSet).UpdateBlobData(AFieldName, AData);
end;

procedure TevTable.UpdateBlobData(const AFieldName, AData: String);
begin
  TevClientDataSet(DataSet.vclDataSet).UpdateBlobData(AFieldName, AData);
end;

procedure TevTable.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FTableName);
  AStream.WriteString(FFieldList);
  AStream.WriteString(FCondition);
  AStream.WriteInteger(FAsOfDate);
end;

function TevTable.IsBlobLoaded(const AFieldName: String): Boolean;
begin
  Result := TevClientDataSet(DataSet.vclDataSet).IsBlobLoaded(AFieldName);
end;

function TevTable.IsEvBlobField(const AFieldName: String): Boolean;
begin
  Result := TevClientDataSet(DataSet.vclDataSet).IsEvBlobField(AFieldName);
end;

function TevTable.GetData: IisStream;
begin
  if not Assigned(FDataset) then
    Open;
  Result := FDataset.Data;
end;

procedure TevTable.SetData(const AData: IisStream);
begin
  if not Assigned(FDataset) then
    Open;
  FDataset.Data := AData;
end;

function TevTable.GetChangePacket: IevDataChangePacket;
begin
  Result := TevClientDataset(FDataset.vclDataset).GetChangePacket;
end;

{ TevDataSet }

function TevDataSet.CreateVCLDataSet: TisKbmMemDataSet;
begin
  Result := TEvBasicClientDataSet.Create(nil);
end;

class function TevDataSet.GetTypeID: String;
begin
  Result := 'evDataSet';
end;

{ TevQuery }

function TevQuery.QueryName: String;
begin
  Result := FQueryName;
end;

class function TevQuery.GetTypeID: String;
begin
  Result := 'evQuery';
end;

function TevQuery.GetParams: IisListOfValues;
begin
  Result := FParams;
end;

procedure TevQuery.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FQueryName := AStream.ReadString;
  FParams.ReadFromStream(AStream);
  FMacros.ReadFromStream(AStream);
  FLoadBlobs := AStream.ReadBoolean;
end;

procedure TevQuery.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FQueryName);
  FParams.WriteToStream(AStream);
  FMacros.WriteToStream(AStream);
  AStream.WriteBoolean(FLoadBlobs);
end;

procedure TevQuery.DoOnConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
  FMacros := TisListOfValues.Create;
  FLoadBlobs := True;
end;

constructor TevQuery.Create(const AQueryName: String; const ASelectStatement: Boolean = True);
begin
  inherited Create;
  FSelect := ASelectStatement;
  FQueryName := AQueryName;
end;

constructor TevQuery.CreateAsOf(const AQueryName: String; const CheckDate: TDateTime);
begin
  inherited Create;
  FSelect := True;
  FQueryName := AQueryName;
  if CheckDate = 0 then
    FParams.AddValue('StatusDate', SysDate)
  else
    FParams.AddValue('StatusDate', CheckDate);
end;

procedure TevQuery.Execute;
begin
  if FSelect then
    FResult := ctx_DBAccess.OpenQuery(FQueryName, FParams, FMacros, GetLoadBlobs)
  else
    ctx_DBAccess.ExecQuery(FQueryName, FParams, FMacros);

  FExecuted := True;  
end;

function TevQuery.Result: IEvDataSet;
begin
  if not FExecuted then
    Execute;
  Result := FResult;
end;

function TevQuery.GetMacros: IisListOfValues;
begin
  Result := FMacros;
end;

procedure TevQuery.SetMacros(const AValue: IisListOfValues);
begin
  if Assigned(AValue) then
    FMacros := AValue
  else
    FMacros.Clear;
end;

procedure TevQuery.SetParams(const AValue: IisListOfValues);
begin
  if Assigned(AValue) then
    FParams := AValue
  else
    FParams.Clear;
end;

constructor TevQuery.CreateFmt(const AQueryName: String;
  const AMacros: array of const; const ASelectStatement: Boolean);
begin
  Create(Format(AQueryName, AMacros), ASelectStatement);
end;

procedure TevQuery.SetParam(Index: String; Value: Variant);
begin
  FParams.AddValue(Index, Value);
end;

procedure TevQuery.SetMacro(Index: String; Value: Variant);
begin
  FMacros.AddValue(Index, Value);
end;

function TevQuery.GetLoadBlobs: Boolean;
begin
  Result := FLoadBlobs;
end;

procedure TevQuery.SetLoadBlobs(const AValue: Boolean);
begin
  FLoadBlobs := AValue;
end;

{ TevStoredProc }

constructor TevStoredProc.Create(const AProcName: String);
begin
  inherited Create;
  FProcName := AProcName;
end;

procedure TevStoredProc.DoOnConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
end;

procedure TevStoredProc.Execute;
begin
  FResult := ctx_DBAccess.ExecStoredProc(FProcName, FParams);
end;

function TevStoredProc.GetParams: IisListOfValues;
begin
  Result := FParams;
end;

class function TevStoredProc.GetTypeID: String;
begin
  Result := 'evStoredProc';
end;

function TevStoredProc.ProcName: String;
begin
  Result := FProcName;
end;

procedure TevStoredProc.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FProcName := AStream.ReadShortString;
  FParams.ReadFromStream(AStream);
end;

function TevStoredProc.Result: IisListOfValues;
begin
  if not Assigned(FResult) then
    Execute;
  Result := FResult;
end;

procedure TevStoredProc.SetParams(const AValue: IisListOfValues);
begin
  if Assigned(AValue) then
    FParams := AValue
  else
    FParams.Clear;
end;

procedure TevStoredProc.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FProcName);
  FParams.WriteToStream(AStream);
end;

initialization
  ObjectFactory.Register([TevDataSet, TevTable, TevQuery, TevStoredProc]);

finalization
  SafeObjectFactoryUnRegister([TevDataSet, TevTable, TevQuery, TevStoredProc]);

end.

