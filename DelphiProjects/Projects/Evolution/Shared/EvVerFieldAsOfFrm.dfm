object evVerFieldAsOf: TevVerFieldAsOf
  Left = 0
  Top = 0
  Width = 591
  Height = 595
  AutoScroll = False
  TabOrder = 0
  object fpChange: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Caption = 'fpChange'
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Title'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    object grFieldData: TevDBGrid
      Left = 12
      Top = 36
      Width = 559
      Height = 539
      DisableThemesInTitle = False
      Selected.Strings = (
        'begin_date'#9'9'#9'Period Begin'#9'F'
        'end_date'#9'9'#9'Period End'#9'F'
        'text_value'#9'15'#9'Display Value'#9'F'
        'value'#9'15'#9'Value'#9'F')
      IniAttributes.Enabled = False
      IniAttributes.SaveToRegistry = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.Delimiter = ';;'
      IniAttributes.CheckNewFields = False
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsFieldData
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      OnCalcCellColors = grFieldDataCalcCellColors
      PaintOptions.AlternatingRowColor = 14544093
      PaintOptions.ActiveRecordColor = clBlack
      DefaultSort = '-'
      Sorting = False
      NoFire = False
    end
  end
  object dsFieldData: TevDataSource
    Left = 88
    Top = 64
  end
end
