// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvClientDataSet;

interface

uses
  Classes, EvStreamUtils, DB, Types, ISKbmMemDataSet, EvBasicUtils, SysUtils, EvTypes, EvConsts,
  DBConsts, Variants, StrUtils, EvDataAccessComponents, isBaseClasses, isTypes, kbmMemTable,
  isBasicUtils, Math, EvExceptions;

type
  TevClientDataSet = class;

  TArrayDS = array of TevClientDataSet;

  TDSSavedState = record
    OpenCondition: string;
    RetrieveCondition: string;
    ClientID: Integer;
    Filter: string;
    UserFilter: string;
    UserFiltered: Boolean;
    Filtered: Boolean;
    Nbr: Integer;
    Active: Boolean;
    IndexName: string;
    IndexFieldNames: string;
    BrowseState: TDataSetState;
    Data: IEvDualStream;
    GenerationNumber: Integer;
    LastTimeModified: TDateTime;
    LastTimeOpened: TDateTime;
  end;

  TLookupIntegerPair = record
    Value: string;
    Key: Integer;
  end;

  TLookupStringPair = record
    Value: string;
    Key: String;
  end;

  TLookupRecord = record
    DataSet: TevClientDataSet;
    sKeyField: string;
    sLookupResultField: string;
    tInteger: Boolean;
    IntegerValues: array of TLookupIntegerPair;
    StringValues: array of TLookupStringPair;
    bDone: Boolean;
  end;

  TLookupArray = array of TLookupRecord;

  TPrepareLookupsEvent = procedure(var Lookups: TLookupArray) of object;
  TAddLookupsEvent = procedure(var A: TArrayDS) of object;
  TDataLookupsEvent = procedure of object;

  TevShadowClientDataSet = class(TEvBasicClientDataSet)
  private
    FLastKeyValue: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    function CachedFindKey(const KeyValue: Integer): Boolean;
    function CachedLookup(const KeyValue: Integer; const LookupFieldName: string): string;
  end;

  TEvDefaultLookupFieldRecordArray = array of TField;

  TEvLookupFieldRecord = record
    ValueField: TField;
    KeyField1, KeyField2: TField;
    LookupDataSet: TEvClientDataSet;
    LookupValueFieldIndex: Integer;
    CachedLookupArrayIndex: Integer;
  end;

  TEvLookupFieldRecordArray = array of TEvLookupFieldRecord;

  IevDataSetHolder = interface
  ['{54505F92-EB93-4BD4-9B12-6C201B336BAA}']
    function DataSet: TevClientDataSet;
  end;

  TevDataSetHolder = class(TisInterfacedObject, IevDataSetHolder)
  private
    FDataSet: TevClientDataSet;
  protected
    function DataSet: TevClientDataSet;
  public
    constructor Create(const ADataSet: TevClientDataSet = nil); reintroduce;
    constructor CreateCloneAndRetrieveData(const ASourceDataSet: TevClientDataSet; const ACondition: string);
    destructor  Destroy; override;
  end;

  TevBlobsLoadMode = (blmPreload, blmLazyLoad, blmManualLoad);

  TevClientDataSet = class(TEvBasicClientDataSet)
  private
    FGenerationNumber: Integer;
    FAlwaysCallCalcFields: Boolean;
    FDelayedAfterPost: Boolean;
    FRetrieveCondition: string;
    FClientID: Integer;
    FInternalFilter: string;
    FFilter: string;
    FFiltered: Boolean;
    FOpenWithLookups: Boolean;
    FDoLookupOpen: Boolean;
    FOpenCondition: string;
    FOpenFieldList: TisCommaDilimitedString;
    FDeleteChildren: Boolean;
    FSavedStates: array of TDSSavedState;
    FNamedSavedStates: array of record Name: string; SavedState: TDSSavedState; end;
    FCustomName: string;
    FAfterCommit: TDataSetNotifyEvent;
    FBeforeValidateRecord: TDataSetNotifyEvent;
    aLookupValues: TLookupArray;
    FOnPrepareLookups: TPrepareLookupsEvent;
    FAddLookupsEvent: TAddLookupsEvent;
    FShadowDataSet: TevShadowClientDataSet;
    FOnUnPrepareLookups: TDataLookupsEvent;
    FLookupInfoArray: TEvLookupFieldRecordArray;
    FLookupInfoArrayHigh: Integer;
    FDefaultLookupInfoArray: TEvDefaultLookupFieldRecordArray;
    FDefaultLookupInfoArrayHigh: Integer;
    FLoadValuesFieldList: TStringList;
    FInSetActive: Boolean;
    FLookupsEnabled: Boolean;
    FLastTimeModified: TDateTime;
    FLastTimeOpened: TDateTime;
    FRefBlobTable: String;
    FRefBlobData: IisParamsCollection;
    FBlobRefCurrentChanges: IisInterfaceList;

    FBlobsLoadMode: TevBlobsLoadMode;
    FConstructingChangePacket: Boolean;
    FCurrentBlobStream: TkbmBlobStream;
    FRefreshingData: Boolean;

    procedure SetRetrieveCondition(const Value: string);
    procedure SetEvFilter(const Value: string);
    procedure SetEvFiltered(const Value: Boolean);
    procedure SetInheritedFilter;
    procedure SetInternalFilter(const Value: string);
    procedure FillLookupAndCalcFields;
    function  GetShadowDataSet: TevShadowClientDataSet;
    procedure SetLookupsEnabled(const Value: Boolean);
    procedure InternalSaveState(var SavedState: TDSSavedState; const SaveData: Boolean);
    procedure InternalLoadState(const SavedState: TDSSavedState);
    function  GetTableName: string;
    function  GetKeyFieldName: String;
    procedure SetBlobsLoadMode(const Value: TevBlobsLoadMode);

    procedure InitRefBlobList;
    procedure PostPendingRefBlobsChanges;
    procedure CancelPendingRefBlobsChanges;
    procedure InitBlobRawData(AField: TBlobField);
    procedure DeinitBlobRawData;
    function  CheckIfBlobLoaded: Boolean;
    procedure LoadBlob(const AFieldName: String);

    procedure RefreshFieldsOfRecord(const AData: TDataSet; const ANbr: Integer);

  protected
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure DoBeforePost; override;
    procedure SetClientID(const Value: Integer); virtual;
    procedure DoOnCalcFields; override;
    procedure CalculateFields(Buffer: PChar); override;
    procedure SetFiltered(Value: Boolean); override;
    procedure SetFilterText(const Value: string); override;
    procedure OpenCursor(InfoQuery: Boolean); override;
    procedure CloseCursor; override;
    procedure DoOnNewRecord; override;
    procedure DoBeforeInsert; override;
    procedure DoAfterInsert; override;
    procedure DoBeforeOpen; override;
    procedure DoAfterOpen; override;
    procedure DoCustomAfterOpen; override;
    procedure DoBeforeClose; override;
    procedure DoAfterClose; override;
    procedure DoAfterDelete; override;
    procedure DoAfterScroll; override;
    procedure DoAfterPost; override;
    procedure DoAfterCancel; override;
    procedure DoAfterEdit; override;
    procedure DoAfterCommit; virtual;
    procedure DoAfterAbort; virtual;
    procedure SetActive(Value: Boolean); override;
    procedure SetDelta(const Value: IEvDualStream); override;
    procedure CustomReconcile(const MapList: IisValueMap); override;
  public
    CustomAfterScroll: TDataSetNotifyEvent;
    CustomBeforeInsert: TDataSetNotifyEvent;
    CustomAfterCommit: TDataSetNotifyEvent;
    CustomAfterAbort: TDataSetNotifyEvent;
    CustomAfterPost: TDataSetNotifyEvent;
    CustomAfterDelete: TDataSetNotifyEvent;
    CustomAfterOpen: TDataSetNotifyEvent;
    CustomAfterClose: TDataSetNotifyEvent;
    CustomAfterCancel: TDataSetNotifyEvent;
    CustomAfterEdit: TDataSetNotifyEvent;
    CustomAfterFiltering: TDataSetNotifyEvent;
    CustomDataRequest: IExecDSWrapper;
    UseLookupCache: Boolean;
    SkipFieldCheck: Boolean;

    constructor Create(AOwner: TComponent); override;
    constructor CreateWithChecksDisabled(AOwner: TComponent; const Dummy: Boolean = False);
    destructor  Destroy; override;

    function  CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    procedure AddLookupDS(var DS: TArrayDS);
    function  FindCachedLookupValue(const sValue: string; const Index: Integer): string; overload;
    function  FindCachedLookupValue(const sValue: string; const DS: TevClientDataSet;
                                   const sKeyField, sLookupResultField: string): string; overload;
    procedure ValidateRecord;
    procedure PrepareLookups;
    procedure UnprepareLookups;
    procedure CancelUpdates;
    procedure EnableControls;
    procedure DataRequest(Data: IExecDSWrapper); reintroduce; overload;
    procedure DataRequest(const s: string); reintroduce; overload;
    procedure SetOpenCondition(sCond: string);
    procedure SaveState(const SaveData: Boolean = False);
    procedure SaveNamedState(const Name: string; const SaveData: Boolean = False);
    procedure LoadNamedState(const Name: string);
    procedure InitNamedState(const Name: string);
    procedure SaveStateAndReset;
    procedure SaveStateAndSet(const NewFiltered: Boolean; const NewFilter, NewIndexName, NewIndexFieldNames: string);
    procedure LoadState;
    procedure CloneCursor(Source :TISKbmMemDataSet; Reset: Boolean; KeepSettings: Boolean = False); override;
    procedure Open;
    function  NewLocate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
    function  NewLookup(const KeyFields: string; const KeyValues: Variant; const ResultField: string): Variant;
    procedure SetLookupFieldTag(const f: TField; const FieldName: string; const LookupDataSet: TEvClientDataSet); overload;
    procedure SetLookupFieldTag(const f: TField; const LookupDataSet: TEvClientDataSet); overload;
    procedure AssignDisplayFormatPictureMasks;
    procedure CreateDataSet; override;
    procedure LoadFromUniversalStream(const ms: IEvDualStream); override;
    procedure LoadFromFile(const FileName: string); override;
    procedure LoadFromStream(Stream:TStream); override;
    procedure AssignDataFrom(const ds: TISKbmMemDataSet); override;
    function  GetClone(const CloneName: string): TevClientDataSet;
    procedure SetLockedIndexFieldNames(const pIndexFieldNames: string);
    procedure ResetLockedIndexFieldNames(const pIndexFieldNames: string);
    procedure ResetNamedSavedStates;
    procedure DoAfterChangesApply;
    procedure Activate;
    procedure DataRequired(aCondition: string = ''); overload;
    procedure DataRequired(const aFieldList: TisCommaDilimitedString; const aCondition: string); overload;
    procedure EnsureDSCondition(const aCondition: string);
    procedure EnsureHasMetadata;
    function  GetChangePacket: IevDataChangePacket;
    function  IsEvBlobField(const AFieldName: String): Boolean;
    function  IsBlobLoaded(const AFieldName: String): Boolean;
    function  GetBlobData(const AFieldName: String): IisStream;
    Procedure DeleteFromRefBlobData(const AFieldName: String);    
    procedure UpdateBlobData(const AFieldName: String; const AData: IisStream); overload;
    procedure UpdateBlobData(const AFieldName: String; const AData: String); overload;
    procedure CheckDSCondition(const ACheckCondition: string; Unfilter: Boolean = True);
    procedure CheckDSConditionAndClient(const AClientID: Integer; const ACheckCondition: string; Unfilter: Boolean = True);
    procedure CheckChildDSAndClient(const AClientID: Integer; const UpLinkFieldName: string; const UpLinkDataSet: TevClientDataSet; const ExtraCheckCondition: string = '');

    property  CustomProviderTableName: string read FCustomName write FCustomName;
    property  LookupsEnabled: Boolean read FLookupsEnabled write SetLookupsEnabled;
    property  LoadValuesFieldList: TStringList read FLoadValuesFieldList;
    property  GenerationNumber: Integer read FGenerationNumber;
    property  LastTimeModified: TDateTime read FLastTimeModified;
    property  LastTimeOpened: TDateTime read FLastTimeOpened;
    property  ShadowDataSet: TevShadowClientDataSet read GetShadowDataSet;
    property  TableName: string read GetTableName;
    property  KeyFieldName: string read GetKeyFieldName;
    property  OpenCondition: string read FOpenCondition;
    property  OpenFieldList: TisCommaDilimitedString read FOpenFieldList;
    procedure RefreshFieldValue(const AFields: TisCommaDilimitedString);
    procedure RefreshRecord(const ANbr: Integer);
  published
    property AlwaysCallCalcFields: Boolean read FAlwaysCallCalcFields write FAlwaysCallCalcFields default False;
    property OpenWithLookups: Boolean read FOpenWithLookups write FOpenWithLookups default True;
    property Filter: string read FFilter write SetEvFilter;
    property Filtered: Boolean read FFiltered write SetEvFiltered default False;
    property RetrieveCondition: string read FRetrieveCondition write SetRetrieveCondition stored False;
    property ClientID: Integer read FClientID write SetClientID default 0;
    property DeleteChildren: Boolean read FDeleteChildren write FDeleteChildren default False;
    property AfterCommit: TDataSetNotifyEvent read FAfterCommit write FAfterCommit;
    property BeforeValidateRecord: TDatasetNotifyEvent read FBeforeValidateRecord write FBeforeValidateRecord;
    property BlobsLoadMode: TevBlobsLoadMode read FBlobsLoadMode write SetBlobsLoadMode default blmPreload;
    property OnPrepareLookups: TPrepareLookupsEvent read FOnPrepareLookups write FOnPrepareLookups;
    property OnAddLookups: TAddLookupsEvent read FAddLookupsEvent write FAddLookupsEvent;
    property OnUnPrepareLookups: TDataLookupsEvent read FOnUnPrepareLookups write FOnUnPrepareLookups;
  end;

  TevProxyDataSet = class(TComponent)
  private
    FDataSet: TDataSet;
    FBeforeOpen: TDataSetNotifyEvent;
    FAfterOpen: TDataSetNotifyEvent;
    FBeforeClose: TDataSetNotifyEvent;
    FAfterClose: TDataSetNotifyEvent;
    FBeforeInsert: TDataSetNotifyEvent;
    FAfterInsert: TDataSetNotifyEvent;
    FBeforeEdit: TDataSetNotifyEvent;
    FAfterEdit: TDataSetNotifyEvent;
    FBeforePost: TDataSetNotifyEvent;
    FAfterPost: TDataSetNotifyEvent;
    FBeforeCancel: TDataSetNotifyEvent;
    FAfterCancel: TDataSetNotifyEvent;
    FBeforeDelete: TDataSetNotifyEvent;
    FAfterDelete: TDataSetNotifyEvent;
    FBeforeRefresh: TDataSetNotifyEvent;
    FAfterRefresh: TDataSetNotifyEvent;
    FBeforeScroll: TDataSetNotifyEvent;
    FAfterScroll: TDataSetNotifyEvent;
    FOnNewRecord: TDataSetNotifyEvent;
    FOnCalcFields: TDataSetNotifyEvent;
    FOnEditError: TDataSetErrorEvent;
    FOnPostError: TDataSetErrorEvent;
    FOnDeleteError: TDataSetErrorEvent;
    FOnFilterRecord: TFilterRecordEvent;

{ TDataset Old Events }
    FOldBeforeOpen: TDataSetNotifyEvent;
    FOldAfterOpen: TDataSetNotifyEvent;
    FOldBeforeClose: TDataSetNotifyEvent;
    FOldAfterClose: TDataSetNotifyEvent;
    FOldBeforeInsert: TDataSetNotifyEvent;
    FOldAfterInsert: TDataSetNotifyEvent;
    FOldBeforeEdit: TDataSetNotifyEvent;
    FOldAfterEdit: TDataSetNotifyEvent;
    FOldBeforePost: TDataSetNotifyEvent;
    FOldAfterPost: TDataSetNotifyEvent;
    FOldBeforeCancel: TDataSetNotifyEvent;
    FOldAfterCancel: TDataSetNotifyEvent;
    FOldBeforeDelete: TDataSetNotifyEvent;
    FOldAfterDelete: TDataSetNotifyEvent;
    FOldBeforeRefresh: TDataSetNotifyEvent;
    FOldAfterRefresh: TDataSetNotifyEvent;
    FOldBeforeScroll: TDataSetNotifyEvent;
    FOldAfterScroll: TDataSetNotifyEvent;
    FOldOnNewRecord: TDataSetNotifyEvent;
    FOldOnCalcFields: TDataSetNotifyEvent;
    FOldOnEditError: TDataSetErrorEvent;
    FOldOnPostError: TDataSetErrorEvent;
    FOldOnDeleteError: TDataSetErrorEvent;
    FOldOnFilterRecord: TFilterRecordEvent;

{ TDataset New Events }
    FNewBeforeOpen: TDataSetNotifyEvent;
    FNewAfterOpen: TDataSetNotifyEvent;
    FNewBeforeClose: TDataSetNotifyEvent;
    FNewAfterClose: TDataSetNotifyEvent;
    FNewBeforeInsert: TDataSetNotifyEvent;
    FNewAfterInsert: TDataSetNotifyEvent;
    FNewBeforeEdit: TDataSetNotifyEvent;
    FNewAfterEdit: TDataSetNotifyEvent;
    FNewBeforePost: TDataSetNotifyEvent;
    FNewAfterPost: TDataSetNotifyEvent;
    FNewBeforeCancel: TDataSetNotifyEvent;
    FNewAfterCancel: TDataSetNotifyEvent;
    FNewBeforeDelete: TDataSetNotifyEvent;
    FNewAfterDelete: TDataSetNotifyEvent;
    FNewBeforeRefresh: TDataSetNotifyEvent;
    FNewAfterRefresh: TDataSetNotifyEvent;
    FNewBeforeScroll: TDataSetNotifyEvent;
    FNewAfterScroll: TDataSetNotifyEvent;
    FNewOnNewRecord: TDataSetNotifyEvent;
    FNewOnCalcFields: TDataSetNotifyEvent;
    FNewOnEditError: TDataSetErrorEvent;
    FNewOnPostError: TDataSetErrorEvent;
    FNewOnDeleteError: TDataSetErrorEvent;
    FNewOnFilterRecord: TFilterRecordEvent;
    procedure SetDataSet(Value: TDataSet);
  protected
    procedure Notification (AComponent :TComponent;
                             Operation :TOperation); override;
  public
    procedure Loaded; override;
    destructor Destroy; override;

    procedure AssignPropsToDataSet; virtual;

    procedure GetNewEvents; virtual;
    procedure GetOldEvents; virtual;

    procedure SetAllEvents; virtual;
    procedure RevertToOldEvents; virtual;

{ TDataSet AllEvents}
    procedure AllBeforeOpen(DataSet: TDataSet); virtual;
    procedure AllAfterOpen(DataSet: TDataSet); virtual;
    procedure AllBeforeClose(DataSet: TDataSet); virtual;
    procedure AllAfterClose(DataSet: TDataSet); virtual;
    procedure AllBeforeInsert(DataSet: TDataSet); virtual;
    procedure AllAfterInsert(DataSet: TDataSet); virtual;
    procedure AllBeforeEdit(DataSet: TDataSet); virtual;
    procedure AllAfterEdit(DataSet: TDataSet); virtual;
    procedure AllBeforePost(DataSet: TDataSet); virtual;
    procedure AllAfterPost(DataSet: TDataSet); virtual;
    procedure AllBeforeCancel(DataSet: TDataSet); virtual;
    procedure AllAfterCancel(DataSet: TDataSet); virtual;
    procedure AllBeforeDelete(DataSet: TDataSet); virtual;
    procedure AllAfterDelete(DataSet: TDataSet); virtual;
    procedure AllBeforeScroll(DataSet: TDataSet); virtual;
    procedure AllAfterScroll(DataSet: TDataSet); virtual;
    procedure AllBeforeRefresh(DataSet: TDataSet); virtual;
    procedure AllAfterRefresh(DataSet: TDataSet); virtual;
    procedure AllOnCalcFields(DataSet: TDataSet); virtual;
    procedure AllOnDeleteError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction); virtual;
    procedure AllOnEditError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction); virtual;
    procedure AllOnFilterRecord(DataSet: TDataSet;
      var Accept: Boolean); virtual;
    procedure AllOnNewRecord(DataSet: TDataSet); virtual;
    procedure AllOnPostError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction); virtual;

{OLD EVENTS}
    property OldBeforeOpen: TDataSetNotifyEvent read FOldBeforeOpen write FOldBeforeOpen;
    property OldAfterOpen: TDataSetNotifyEvent read FOldAfterOpen write FOldAfterOpen;
    property OldBeforeClose: TDataSetNotifyEvent read FOldBeforeClose write FOldBeforeClose;
    property OldAfterClose: TDataSetNotifyEvent read FOldAfterClose write FOldAfterClose;
    property OldBeforeInsert: TDataSetNotifyEvent read FOldBeforeInsert write FOldBeforeInsert;
    property OldAfterInsert: TDataSetNotifyEvent read FOldAfterInsert write FOldAfterInsert;
    property OldBeforeEdit: TDataSetNotifyEvent read FOldBeforeEdit write FOldBeforeEdit;
    property OldAfterEdit: TDataSetNotifyEvent read FOldAfterEdit write FOldAfterEdit;
    property OldBeforePost: TDataSetNotifyEvent read FOldBeforePost write FOldBeforePost;
    property OldAfterPost: TDataSetNotifyEvent read FOldAfterPost write FOldAfterPost;
    property OldBeforeCancel: TDataSetNotifyEvent read FOldBeforeCancel write FOldBeforeCancel;
    property OldAfterCancel: TDataSetNotifyEvent read FOldAfterCancel write FOldAfterCancel;
    property OldBeforeDelete: TDataSetNotifyEvent read FOldBeforeDelete write FOldBeforeDelete;
    property OldAfterDelete: TDataSetNotifyEvent read FOldAfterDelete write FOldAfterDelete;
    property OldBeforeScroll: TDataSetNotifyEvent read FOldBeforeScroll write FOldBeforeScroll;
    property OldAfterScroll: TDataSetNotifyEvent read FOldAfterScroll write FOldAfterScroll;
    property OldBeforeRefresh: TDataSetNotifyEvent read FOldBeforeRefresh write FOldBeforeRefresh;
    property OldAfterRefresh: TDataSetNotifyEvent read FOldAfterRefresh write FOldAfterRefresh;
    property OldOnCalcFields: TDataSetNotifyEvent read FOldOnCalcFields write FOldOnCalcFields;
    property OldOnDeleteError: TDataSetErrorEvent read FOldOnDeleteError write FOldOnDeleteError;
    property OldOnEditError: TDataSetErrorEvent read FOldOnEditError write FOldOnEditError;
    property OldOnFilterRecord: TFilterRecordEvent read FOldOnFilterRecord write FOldOnFilterRecord;
    property OldOnNewRecord: TDataSetNotifyEvent read FOldOnNewRecord write FOldOnNewRecord;
    property OldOnPostError: TDataSetErrorEvent read FOldOnPostError write FOldOnPostError;
{NEW EVENTS}
    property NewBeforeOpen: TDataSetNotifyEvent read FNewBeforeOpen write FNewBeforeOpen;
    property NewAfterOpen: TDataSetNotifyEvent read FNewAfterOpen write FNewAfterOpen;
    property NewBeforeClose: TDataSetNotifyEvent read FNewBeforeClose write FNewBeforeClose;
    property NewAfterClose: TDataSetNotifyEvent read FNewAfterClose write FNewAfterClose;
    property NewBeforeInsert: TDataSetNotifyEvent read FNewBeforeInsert write FNewBeforeInsert;
    property NewAfterInsert: TDataSetNotifyEvent read FNewAfterInsert write FNewAfterInsert;
    property NewBeforeEdit: TDataSetNotifyEvent read FNewBeforeEdit write FNewBeforeEdit;
    property NewAfterEdit: TDataSetNotifyEvent read FNewAfterEdit write FNewAfterEdit;
    property NewBeforePost: TDataSetNotifyEvent read FNewBeforePost write FNewBeforePost;
    property NewAfterPost: TDataSetNotifyEvent read FNewAfterPost write FNewAfterPost;
    property NewBeforeCancel: TDataSetNotifyEvent read FNewBeforeCancel write FNewBeforeCancel;
    property NewAfterCancel: TDataSetNotifyEvent read FNewAfterCancel write FNewAfterCancel;
    property NewBeforeDelete: TDataSetNotifyEvent read FNewBeforeDelete write FNewBeforeDelete;
    property NewAfterDelete: TDataSetNotifyEvent read FNewAfterDelete write FNewAfterDelete;
    property NewBeforeScroll: TDataSetNotifyEvent read FNewBeforeScroll write FNewBeforeScroll;
    property NewAfterScroll: TDataSetNotifyEvent read FNewAfterScroll write FNewAfterScroll;
    property NewBeforeRefresh: TDataSetNotifyEvent read FNewBeforeRefresh write FNewBeforeRefresh;
    property NewAfterRefresh: TDataSetNotifyEvent read FNewAfterRefresh write FNewAfterRefresh;
    property NewOnCalcFields: TDataSetNotifyEvent read FNewOnCalcFields write FNewOnCalcFields;
    property NewOnDeleteError: TDataSetErrorEvent read FNewOnDeleteError write FNewOnDeleteError;
    property NewOnEditError: TDataSetErrorEvent read FNewOnEditError write FNewOnEditError;
    property NewOnFilterRecord: TFilterRecordEvent read FNewOnFilterRecord write FNewOnFilterRecord;
    property NewOnNewRecord: TDataSetNotifyEvent read FNewOnNewRecord write FNewOnNewRecord;
    property NewOnPostError: TDataSetErrorEvent read FNewOnPostError write FNewOnPostError;
  published
    property Dataset :TDataset read FDataset write SetDataset;
{Events}
    property BeforeOpen: TDataSetNotifyEvent read FBeforeOpen write FBeforeOpen;
    property AfterOpen: TDataSetNotifyEvent read FAfterOpen write FAfterOpen;
    property BeforeClose: TDataSetNotifyEvent read FBeforeClose write FBeforeClose;
    property AfterClose: TDataSetNotifyEvent read FAfterClose write FAfterClose;
    property BeforeInsert: TDataSetNotifyEvent read FBeforeInsert write FBeforeInsert;
    property AfterInsert: TDataSetNotifyEvent read FAfterInsert write FAfterInsert;
    property BeforeEdit: TDataSetNotifyEvent read FBeforeEdit write FBeforeEdit;
    property AfterEdit: TDataSetNotifyEvent read FAfterEdit write FAfterEdit;
    property BeforePost: TDataSetNotifyEvent read FBeforePost write FBeforePost;
    property AfterPost: TDataSetNotifyEvent read FAfterPost write FAfterPost;
    property BeforeCancel: TDataSetNotifyEvent read FBeforeCancel write FBeforeCancel;
    property AfterCancel: TDataSetNotifyEvent read FAfterCancel write FAfterCancel;
    property BeforeDelete: TDataSetNotifyEvent read FBeforeDelete write FBeforeDelete;
    property AfterDelete: TDataSetNotifyEvent read FAfterDelete write FAfterDelete;
    property BeforeScroll: TDataSetNotifyEvent read FBeforeScroll write FBeforeScroll;
    property AfterScroll: TDataSetNotifyEvent read FAfterScroll write FAfterScroll;
    property BeforeRefresh: TDataSetNotifyEvent read FBeforeRefresh write FBeforeRefresh;
    property AfterRefresh: TDataSetNotifyEvent read FAfterRefresh write FAfterRefresh;
    property OnCalcFields: TDataSetNotifyEvent read FOnCalcFields write FOnCalcFields;
    property OnDeleteError: TDataSetErrorEvent read FOnDeleteError write FOnDeleteError;
    property OnEditError: TDataSetErrorEvent read FOnEditError write FOnEditError;
    property OnFilterRecord: TFilterRecordEvent read FOnFilterRecord write FOnFilterRecord;
    property OnNewRecord: TDataSetNotifyEvent read FOnNewRecord write FOnNewRecord;
    property OnPostError: TDataSetErrorEvent read FOnPostError write FOnPostError;
  end;


  TEvQueryBuilderInfo = class(TPersistent)
  private
    FData: IEvDualStream;
    procedure ReadData(AStream: TStream);
    procedure WriteData(AStream: TStream);
  protected
    procedure AssignTo(Dest: TPersistent); override;
    procedure DefineProperties(Filer: TFiler); override;
  public
    constructor Create;
    property Data: IEvDualStream read FData;
  end;


  TEvQueryBuilderQuery = class(TEvBasicClientDataSet)
  private
    FQueryBuilderInfo: TEvQueryBuilderInfo;
    FCoNbr: Integer;
    procedure SetQueryBuilderInfo(const AValue: TEvQueryBuilderInfo);
  protected
    procedure  DoRunQuery;
  public
    constructor Create(Owner: TComponent); override;
    destructor  Destroy; override;
    procedure   OpenQuery;
    property    CoNbr: Integer read FCoNbr write FCoNbr;

  published
    property QueryBuilderInfo: TEvQueryBuilderInfo read FQueryBuilderInfo write SetQueryBuilderInfo;
    property Params stored True;
  end;


  TTransactionManager = class(TComponent)
  private
    FDS: TArrayDS;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Initialize(const DS: array of TevClientDataset);
    procedure ApplyUpdates(const AResolveCircularReferences: Boolean = False);
    procedure CancelUpdates;
    function IsModified: Boolean;
  end;

implementation

uses
  SDataStructure, SDDClasses, EvUtils, EvReportWriterProxy, EvContext, EvDataSet, evClasses,
  EvCommonInterfaces, isDataSet;

const
  ClonePrefix = 'Cloned_';


{ TevClientDataSet }

procedure TevClientDataSet.AddLookupDS(var DS: TArrayDS);
var
  i: Integer;
  aAddDS: TArrayDS;
begin
  if not LookupsEnabled then
    Exit;

  if FDoLookupOpen then
  begin
    FDoLookupOpen := False;
    Assert(False, 'You can''t do cross references for lookup fields');
  end;

  FDoLookupOpen := True;
  try
    AddDS(Self, DS);

    for i := 0 to FieldCount - 1 do
      with Fields[i] do
        if ((FieldKind = fkLookup) or (FieldKind = fkInternalCalc) and (FLookupFields.IndexOf(FieldName) >= 0)) and
           Assigned(LookupDataSet) and (LookupDataSet is TevClientDataSet) then
        begin
          if not LookupDataSet.Active or
             ((LookupDataSet is TddClienDBTable) and (TevClientDataSet(LookupDataSet).ClientID >= 0) and (TevClientDataSet(LookupDataSet).ClientID <> Self.ClientID)) or
             (not (LookupDataSet is TddTMPTable) and (TevClientDataSet(LookupDataSet).AsOfDate <> Self.AsOfDate) and not Self.UsingDefaultAsOfDate) then
             with TevClientDataSet(LookupDataSet) do
             begin
               if Active then
                 RetrieveCondition := '';
               Close;
               if ClientID >= 0 then
                 ClientID := Self.ClientID;
             end;
          TevClientDataSet(LookupDataSet).AddLookupDS(DS);
        end;

    if Assigned(FAddLookupsEvent) then
    begin
      FAddLookupsEvent(aAddDS);
      for i := Low(aAddDS) to High(aAddDS) do
      begin
        if (not aAddDS[i].Active or
           (aAddDS[i] is TddClienDBTable) and (aAddDS[i].ClientID >= 0) and (aAddDS[i].ClientID <> Self.ClientID) or
           not (aAddDS[i] is TddTMPTable) and (aAddDS[i].AsOfDate <> Self.AsOfDate)) then
           with aAddDS[i] as TevClientDataSet do
           begin
             if Active then
               RetrieveCondition := '';
             Close;
             if ClientID >= 0 then
               ClientID := Self.ClientID;
          end;
        aAddDS[i].AddLookupDS(DS);
      end;
    end;
  finally
    FDoLookupOpen := False;
  end;
end;

constructor TevClientDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FGenerationNumber := 1;
  FLastTimeModified := 0;
  FLastTimeOpened := 0;
  FLoadValuesFieldList := TStringList.Create;
  FLoadValuesFieldList.Sorted := True;
  FLoadValuesFieldList.Duplicates := dupIgnore;
  LookupsEnabled := True;
  FOpenWithLookups := True;
  FLookupInfoArrayHigh := -1;
  FDefaultLookupInfoArrayHigh := -1;
  SetLength(FNamedSavedStates, 0);
end;

procedure TevClientDataSet.DoAfterScroll;
begin
  inherited;
  if not ControlsDisabled then
    if Assigned(CustomAfterScroll) then
      CustomAfterScroll(Self);
end;

procedure TevClientDataSet.DoAfterInsert;
begin
  inherited;
  if not ControlsDisabled then
    if Assigned(CustomAfterEdit) then
      CustomAfterEdit(Self);
end;

procedure TevClientDataSet.DoOnNewRecord;
var
  Fld: TField;
  CurrClientID: Integer;
begin
  if IsValidTableName(TableName) then
  begin
    Fld := FindField(TableName + '_NBR');
    if Assigned(Fld) then
    begin
      CurrClientID := ctx_DBAccess.CurrentClientNbr;
      try
        if ClientID > 0 then
          ctx_DBAccess.CurrentClientNbr := ctx_DataAccess.ClientID;
        Fld.AsInteger := ctx_DBAccess.GetNewKeyValue(TableName, 1);
      finally
        ctx_DBAccess.CurrentClientNbr := CurrClientID;
      end;
    end;
  end;

  inherited;
end;

function TevClientDataSet.GetTableName: string;
begin
  if CustomProviderTableName <> '' then
    Result := CustomProviderTableName
  else if ProviderName <> '' then
    Result := Copy(ProviderName, 1, Length(ProviderName) - 5)
  else
    Result := Name;
end;

procedure TevClientDataSet.OpenCursor(InfoQuery: Boolean);
begin
  SetInternalFilter('');
  inherited;
end;

procedure TevClientDataSet.SetClientID(const Value: Integer);
begin
  if FClientID <> Value then
  begin
    FClientID := Value;
    if not (csReading in ComponentState) and (Value > 0) and Active then
    begin
      Close;
      FRetrieveCondition := '';
    end;
  end;
end;

procedure TevClientDataSet.SetEvFilter(const Value: string);
begin
  if FFilter <> Value then
  begin
    FFilter := Value;
    SetInheritedFilter;
  end;
end;

procedure TevClientDataSet.SetEvFiltered(const Value: Boolean);
begin
  if FFiltered <> Value then
  begin
    FFiltered := Value;
    SetInheritedFilter;
  end;
end;

procedure TevClientDataSet.SetInternalFilter(const Value: string);
begin
  if FInternalFilter <> Value then
  begin
    FInternalFilter := Value;
    SetInheritedFilter;
  end;
end;

procedure TevClientDataSet.SetRetrieveCondition(const Value: string);
var
  Nbr: Integer;
begin
  if UpperCase(StringReplace(FRetrieveCondition, ' ', '', [rfReplaceAll])) <> UpperCase(StringReplace(Value, ' ', '', [rfReplaceAll])) then
  begin
    FRetrieveCondition := Value;
    if Active then
    begin
      if Assigned(FindField(TableName + '_NBR')) then
        Nbr := FindField(TableName + '_NBR').AsInteger
      else
        Nbr := 0;
      DisableControls;
      try
        SetInternalFilter(Value);
        if Nbr <> 0 then
          Locate(TableName + '_NBR', Nbr, []);
      finally
        EnableControls;
      end;
      if not ControlsDisabled then
        if Assigned(CustomAfterScroll) then
          CustomAfterScroll(Self);
    end;
  end;
end;

procedure TevClientDataSet.DoBeforeInsert;
begin
  inherited;
  if not ControlsDisabled then
    if Assigned(CustomBeforeInsert) then
      CustomBeforeInsert(Self);
end;

procedure TevClientDataSet.DoAfterCommit;
begin
  if Assigned(FAfterCommit) then
    FAfterCommit(Self);
  if Assigned(CustomAfterCommit) then
    CustomAfterCommit(Self);
end;

procedure TevClientDataSet.DoAfterPost;
begin
  FLastTimeModified := Now;
  inherited;
  PostPendingRefBlobsChanges;  
  if not ControlsDisabled then
  begin
    if Assigned(CustomAfterPost) then
      CustomAfterPost(Self);
  end
  else
    FDelayedAfterPost := True;
end;

procedure TevClientDataSet.DoAfterDelete;
begin
  FLastTimeModified := Now;
  inherited;

  if not ControlsDisabled then
  begin
    if Assigned(CustomAfterDelete) then
      CustomAfterDelete(Self);
  end
  else
    FDelayedAfterPost := True;
end;

procedure TevClientDataSet.DoAfterClose;
begin
  inherited;
  FLookupInfoArrayHigh := -1;
  SetLength(FLookupInfoArray, 0);
  FDefaultLookupInfoArrayHigh := -1;
  SetLength(FDefaultLookupInfoArray, 0);
  FRefBlobTable := '';
  FRefBlobData := nil;

  FOpenCondition := '';
  FOpenFieldList := '';
  if Assigned(CustomAfterClose) then
    CustomAfterClose(Self);
  Filtered := False;
  Inc(FGenerationNumber);
end;

procedure TevClientDataSet.DoAfterOpen;
begin
  FLastTimeOpened := Now;
  RecalcOnFetch := False;

  if not SkipFieldCheck then
    AssignDisplayFormatPictureMasks
  else
    PictureMasks.Clear;

  InitRefBlobList;

  inherited;

  FOpenCondition := FRetrieveCondition;
  if Assigned(CustomAfterOpen) then
    CustomAfterOpen(Self);
end;

procedure TevClientDataSet.DoAfterCancel;
begin
  inherited;
  CancelPendingRefBlobsChanges;  
  if Assigned(CustomAfterCancel) then
    CustomAfterCancel(Self);
end;

procedure TevClientDataSet.DoAfterEdit;
begin
  inherited;
  if not ControlsDisabled then
    if Assigned(CustomAfterEdit) then
      CustomAfterEdit(Self);
end;

procedure TevClientDataSet.SetFilterText(const Value: string);
begin
  inherited;
  if inherited Filtered and
     not ControlsDisabled and
     Assigned(CustomAfterFiltering) then
    CustomAfterFiltering(Self);
end;

procedure TevClientDataSet.SetFiltered(Value: Boolean);
begin
  inherited;
  if Assigned(CustomAfterFiltering) and
     not ControlsDisabled then
    CustomAfterFiltering(Self);
end;

procedure TevClientDataSet.DoAfterAbort;
begin
  if Assigned(CustomAfterAbort) then
    CustomAfterAbort(Self);
end;

procedure TevClientDataSet.CancelUpdates;
begin
  inherited;
  DoAfterAbort;
end;

procedure TevClientDataSet.EnableControls;
var
  b: Boolean;
begin
  inherited;
  if not ControlsDisabled then
  begin
    b := FDelayedAfterPost;
    FDelayedAfterPost := False;
    if Assigned(CustomAfterPost) and b then
      CustomAfterPost(Self);
  end;
end;

procedure TevClientDataSet.DoBeforeOpen;
begin
  FillLookupAndCalcFields;
  inherited;
end;

destructor TevClientDataSet.Destroy;
begin
  FLoadValuesFieldList.Free;
  FreeAndNil(FShadowDataSet);
  inherited;
end;

function TevClientDataSet.FindCachedLookupValue(const sValue: string; const DS: TevClientDataSet; const sKeyField, sLookupResultField: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := Low(aLookupValues) to High(aLookupValues) do
    if (aLookupValues[i].DataSet = DS) and
       (AnsiCompareText(aLookupValues[i].sKeyField, sKeyField) = 0) and
       (AnsiCompareText(aLookupValues[i].sLookupResultField, sLookupResultField) = 0) then
    begin
      Result := FindCachedLookupValue(sValue, i);
      Break;
    end;
end;

function TevClientDataSet.FindCachedLookupValue(const sValue: string; const Index: Integer): string;
var
  iPos: Integer;
  I: Integer;
  Value: Integer;

  function FindPos(const A: TLookupRecord; const L, R: Integer): Integer;
  var
    M: Integer;
  begin
    if aLookupValues[Index].tInteger then
      if A.IntegerValues[L].Key = Value then
        Result := L
      else if A.IntegerValues[R].Key = Value then
        Result := R
      else
      begin
        M := (L + R) shr 1;
        if A.IntegerValues[M].Key = Value then
          Result := M
        else if (M <= L + 1) and (M >= R - 1) then
          Result := -1
        else if A.IntegerValues[M].Key > Value then
          Result := FindPos(A, L, M)
        else
          Result := FindPos(A, M, R)
      end
    else
      if A.StringValues[L].Key = sValue then
        Result := L
      else if A.StringValues[R].Key = sValue then
        Result := R
      else
      begin
        M := (L + R) shr 1;
        if A.StringValues[M].Key = sValue then
          Result := M
        else if (M <= L + 1) and (M >= R - 1) then
          Result := -1
        else if A.StringValues[M].Key > sValue then
          Result := FindPos(A, L, M)
        else
          Result := FindPos(A, M, R)
      end
  end;
begin
  Result := '';
  if Trim(sValue) = '' then
    Exit;
  if aLookupValues[Index].tInteger then
  begin
    I := High(aLookupValues[Index].IntegerValues);
    Value := StrToInt(sValue)
  end
  else
    I := High(aLookupValues[Index].StringValues);
  if I = -1 then
    Exit;
  iPos := FindPos(aLookupValues[Index], 0, I);
  if iPos = -1 then
  else if aLookupValues[Index].tInteger then
    Result := aLookupValues[Index].IntegerValues[iPos].Value
  else
    Result := aLookupValues[Index].StringValues[iPos].Value
end;

procedure TevClientDataSet.CalculateFields(Buffer: PChar);
var
  i: Integer;
  s: string;
begin
  inherited;
  if LookupsEnabled then
  begin
    if UseLookupCache then
    begin
      for i := 0 to FLookupInfoArrayHigh do
        with FLookupInfoArray[i] do
        begin
          if not Assigned(KeyField2) then
          begin
            if CachedLookupArrayIndex >= 0 then
              s := FindCachedLookupValue(KeyField1.AsString, CachedLookupArrayIndex)
            else
              s := FindCachedLookupValue(KeyField1.AsString, LookupDataSet, ValueField.KeyFields, ValueField.LookupResultField);
          end
          else
          begin
            if KeyField1.IsNull or KeyField2.IsNull then
              s := ''
            else
            if not LookupDataSet.ShadowDataSet.FindKey([KeyField1.AsInteger, KeyField2.AsInteger]) then
              s := ''
            else
              s := LookupDataSet.ShadowDataSet.Fields[LookupValueFieldIndex].AsString;
          end;
          if s = '' then
            ValueField.Clear
          else
            ValueField.AsString := s;
        end;
    end
    else
    begin
      for i := 0 to FLookupInfoArrayHigh do
        with FLookupInfoArray[i] do
        begin
          if not Assigned(KeyField2) then
          begin
            if KeyField1.IsNull or not LookupDataSet.Active then
              s := ''
            else
            if not LookupDataSet.ShadowDataSet.CachedFindKey(KeyField1.AsInteger) then
              s := ''
            else
              s := LookupDataSet.ShadowDataSet.Fields[LookupValueFieldIndex].AsString;
          end
          else
          begin
            if KeyField1.IsNull or KeyField2.IsNull or not LookupDataSet.Active then
              s := ''
            else
            if not LookupDataSet.ShadowDataSet.FindKey([KeyField1.AsInteger, KeyField2.AsInteger]) then
              s := ''
            else
              s := LookupDataSet.ShadowDataSet.Fields[LookupValueFieldIndex].AsString;
          end;
          if s = '' then
            ValueField.Clear
          else
            ValueField.AsString := s;
        end;
    end;
    for i := 0 to FDefaultLookupInfoArrayHigh do
      with FDefaultLookupInfoArray[i] do
        Value := LookupDataSet.Lookup(LookupKeyFields, FieldValues[KeyFields], LookupResultField);
  end;
  if LookupsEnabled or AlwaysCallCalcFields then
    DoInheritedCalcFields;
end;

procedure TevClientDataSet.SetOpenCondition(sCond: string);
begin
  FOpenCondition := sCond;
end;

procedure TevClientDataSet.LoadState;
begin
  InternalLoadState(FSavedStates[High(FSavedStates)]);
  SetLength(FSavedStates, Pred(Length(FSavedStates)));
  EnableControls;
end;

procedure TevClientDataSet.SaveState(const SaveData: Boolean = False);
begin
  SetLength(FSavedStates, Succ(Length(FSavedStates)));
  InternalSaveState(FSavedStates[High(FSavedStates)], SaveData);
  DisableControls;
end;

procedure TevClientDataSet.CloneCursor(Source :TISKbmMemDataSet; Reset: Boolean; KeepSettings: Boolean = False);
begin
  FInSetActive := True;
  try
    inherited CloneCursor(Source, Reset, KeepSettings);
  finally
    FInSetActive := False;
  end;

  if Reset then
  begin
    FFilter := '';
    FFiltered := False;
    FInternalFilter := '';
  end
  else if not KeepSettings and (Source is TevClientDataSet) then
  begin
    FFilter := TevClientDataSet(Source).FFilter;
    FFiltered := TevClientDataSet(Source).FFiltered;
    FInternalFilter := TevClientDataSet(Source).FInternalFilter;
  end;
end;

function TevClientDataSet.NewLocate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;

  function RecordMatches: Boolean;
  var
    aFieldName, FieldsLeft: string;
    Position, Count: Integer;
    aValue: Variant;
  begin
    Result := True;
    FieldsLeft := KeyFields;
    Count := 0;
    repeat
      Position := Pos(';', FieldsLeft);
      if Position = 0 then
      begin
        aFieldName := FieldsLeft;
        FieldsLeft := '';
      end
      else
      begin
        aFieldName := Copy(FieldsLeft, 1, Position - 1);
        System.Delete(FieldsLeft, 1, Position);
      end;

      if VarIsArray(KeyValues) then
        aValue := KeyValues[Count]
      else
        aValue := KeyValues;

      if FieldByName(aFieldName).Value <> aValue then
      begin
        Result := False;
        Break;
      end;
      Inc(Count);
    until FieldsLeft = '';
  end;

begin
  CheckBrowseMode;
  if RecordMatches then
  begin
    Result := True;
    Exit;
  end;
  Result := Locate(KeyFields, KeyValues, Options);
end;

function TevClientDataSet.NewLookup(const KeyFields: string; const KeyValues: Variant; const ResultField: string): Variant;
begin
  Result := Null;
  if NewLocate(KeyFields, KeyValues, []) then
    Result := FieldByName(ResultField).Value;
end;

procedure aQuickSort(var A: TLookupRecord; L, R: Integer);
var
  I, J, P: Integer;

  function SCompare(const L, R: Integer): Integer;
  begin
    if A.tInteger then
      if A.IntegerValues[L].Key < A.IntegerValues[R].Key then
        Result := -1
      else if A.IntegerValues[L].Key = A.IntegerValues[R].Key then
        Result := 0
      else
        Result := 1
    else
      if A.StringValues[L].Key < A.StringValues[R].Key then
        Result := -1
      else if A.StringValues[L].Key = A.StringValues[R].Key then
        Result := 0
      else
        Result := 1
  end;

  procedure ExchangeItems(const I, J: Integer);
  var
    ip: TLookupIntegerPair;
    sp: TLookupStringPair;
  begin
    if A.tInteger then
    begin
      ip := A.IntegerValues[J];
      A.IntegerValues[J] := A.IntegerValues[I];
      A.IntegerValues[I] := ip
    end
    else
    begin
      sp := A.StringValues[J];
      A.StringValues[J] := A.StringValues[I];
      A.StringValues[I] := sp
    end
  end;
begin
  if L > R then
    Exit;
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while SCompare(I, P) < 0 do Inc(I);
      while SCompare(J, P) > 0 do Dec(J);
      if I <= J then
      begin
        ExchangeItems(I, J);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then aQuickSort(A, L, J);
    L := I;
  until I >= R;
end;

procedure TevClientDataSet.PrepareLookups;
var
  b: Boolean;
  Mark: TBookmarkStr;
  ds: TevClientDataSet;
  i, j: Integer;
  t: TStringList;
  s: string;
  p1, p2: array of Integer;
const
  BufSize = 64 * 1024;
begin
  if not LookupsEnabled then
    Exit;
  t := TStringList.Create;
  t.Sorted := True;
  ctx_StartWait;
  s := RetrieveCondition;
  try
    if (FLookupFields.Count > 0) or Active then
      t.Assign(FLookupFields)
    else
      for i := 0 to Pred(FieldCount) do
        if Fields[i].FieldKind = fkLookup then
          t.Add(Fields[i].FieldName);

    SetLength(aLookupValues, t.Count);
    for i := Low(aLookupValues) to High(aLookupValues) do
    begin
      aLookupValues[i].DataSet := FieldByName(t[i]).LookupDataSet as TevClientDataSet;
      aLookupValues[i].sKeyField := FieldByName(t[i]).LookupKeyFields;
      aLookupValues[i].sLookupResultField := FieldByName(t[i]).LookupResultField;
    end;

    if Assigned(FOnPrepareLookups) then
      FOnPrepareLookups(aLookupValues);

    for i := Low(aLookupValues) to High(aLookupValues) do
    begin
      SetLength(aLookupValues[i].IntegerValues, 0);
      SetLength(aLookupValues[i].StringValues, 0);
      aLookupValues[i].bDone := False;
    end;
    for i := Low(aLookupValues) to High(aLookupValues) do
      if not aLookupValues[i].bDone and
         (Pos(';', aLookupValues[i].sKeyField) = 0) then
      begin
        ds := aLookupValues[i].DataSet;
        if Assigned(ds) and ds.Active then
        begin
          Mark := ds.Bookmark;
          b := ds.LookupsEnabled;
          ds.DisableControls;
          try
            ds.LookupsEnabled := False;
            ds.First;
            SetLength(p1, Length(aLookupValues));
            SetLength(p2, Length(aLookupValues));
            for j := Low(aLookupValues) to High(aLookupValues) do
              if not aLookupValues[j].bDone and
                 (aLookupValues[j].DataSet = ds) then
              begin
                p1[j] := ds.FieldByName(aLookupValues[j].sKeyField).Index;
                p2[j] := ds.FieldByName(aLookupValues[j].sLookupResultField).Index;
                aLookupValues[j].tInteger := ds.FieldByName(aLookupValues[j].sKeyField) is TIntegerField;
                if aLookupValues[j].tInteger then
                  SetLength(aLookupValues[j].IntegerValues, DS.RecordCount)
                else
                  SetLength(aLookupValues[j].StringValues, DS.RecordCount)
              end;
            while not ds.Eof do
            begin
              for j := Low(aLookupValues) to High(aLookupValues) do
                if not aLookupValues[j].bDone and
                   (aLookupValues[j].DataSet = ds) then
                  if aLookupValues[j].tInteger then
                    with aLookupValues[j].IntegerValues[Pred(ds.Recno)] do
                    begin
                      Key := ds.Fields[p1[j]].AsInteger;
                      Value := ds.Fields[p2[j]].AsString;
                    end
                  else
                    with aLookupValues[j].StringValues[Pred(ds.Recno)] do
                    begin
                      Key := ds.Fields[p1[j]].AsString;
                      Value := ds.Fields[p2[j]].AsString;
                    end;
              ds.Next;
            end;
          finally
            ds.Bookmark := Mark;
            ds.LookupsEnabled := b;
            ds.EnableControls;
          end;
          for j := Low(aLookupValues) to High(aLookupValues) do
            if not aLookupValues[j].bDone and
               (aLookupValues[j].DataSet = ds) then
            begin
              aLookupValues[j].bDone := True;
              aQuickSort(aLookupValues[j], 0, Pred(ds.RecordCount));
            end;
        end;
      end;
    UseLookupCache := True;
  finally
    t.Free;
    RetrieveCondition := s;
    ctx_EndWait;
  end;
end;

procedure TevClientDataSet.UnprepareLookups;
begin
  UseLookupCache := False;
  SetLength(aLookupValues, 0);
  if not LookupsEnabled then
    Exit;
  if Assigned(FOnUnPrepareLookups) then
    FOnUnPrepareLookups;
end;

procedure TevClientDataSet.FillLookupAndCalcFields;
  function CharCount(const s: string; const c: char): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := 1 to Length(s) do
      if s[i] = c then
        Inc(Result);
  end;
var
  i, j: Integer;
  f: TField;
  s: string;
begin
  for i := 0 to Fields.Count-1 do
  begin
    f := Fields[i];
    if (f.FieldKind = fkLookup)
    and Assigned(f.LookupDataSet)
    and f.LookupDataSet.Active
    and (f.LookupDataSet is TevClientDataSet)
    and (CharCount(f.KeyFields, ';') <= 1)
    and (TevClientDataSet(f.LookupDataSet).ShadowDataSet.IndexFieldNames = f.LookupKeyFields) then
    begin
      SetLength(FLookupInfoArray, Length(FLookupInfoArray)+1);
      Inc(FLookupInfoArrayHigh);
      with FLookupInfoArray[FLookupInfoArrayHigh] do
      begin
        ValueField := f;
        LookupDataSet := f.LookupDataSet as TevClientDataSet;
        LookupValueFieldIndex := LookupDataSet.ShadowDataSet.FieldByName(f.LookupResultField).Index;
        CachedLookupArrayIndex := -1;
        if CharCount(f.KeyFields, ';') = 0 then
        begin
          KeyField1 := FieldByName(f.KeyFields);
          KeyField2 := nil;
          for j := Low(aLookupValues) to High(aLookupValues) do
            if (aLookupValues[j].DataSet = LookupDataSet) and
               (AnsiCompareText(aLookupValues[j].sKeyField, f.LookupKeyFields) = 0) and
               (AnsiCompareText(aLookupValues[j].sLookupResultField, f.LookupResultField) = 0) then
            begin
              CachedLookupArrayIndex := j;
              Break;
            end;
        end
        else
        begin
          s := f.KeyFields;
          KeyField1 := FieldByName(Fetch(s, ';'));
          KeyField2 := FieldByName(Fetch(s, ';'));
        end;
      end;
    end
    else
    if (f.FieldKind = fkLookup)
    and Assigned(f.LookupDataSet)
    and f.LookupDataSet.Active
    and (f.LookupDataSet is TevClientDataSet) then
    begin
      SetLength(FDefaultLookupInfoArray, Length(FDefaultLookupInfoArray)+1);
      Inc(FDefaultLookupInfoArrayHigh);
      FDefaultLookupInfoArray[FDefaultLookupInfoArrayHigh] := f;
    end;
  end;
end;


procedure TevClientDataSet.CloseCursor;
begin
  SetState(dsInactive);
  inherited;
end;

procedure TevClientDataSet.SetActive(Value: Boolean);
begin
  // need to add design time data acess...

  if [csLoading, csDesigning] * ComponentState = [] then
  begin
    if FInSetActive or not Value then
      inherited
    else
      ctx_DataAccess.OpenDataSets([Self])
  end;
end;


procedure TevClientDataSet.SetInheritedFilter;
var
  s: string;
begin
  if (Trim(Filter) <> '') and Filtered then
    s := '(' + Filter + ')'
  else
    s := '';
  if Trim(FInternalFilter) <> '' then
    if s <> '' then
      s := s + ' and (' + FInternalFilter + ')'
    else
      s := '(' + FInternalFilter + ')';
  inherited Filtered := Filtered or (Trim(FInternalFilter) <> '');
  inherited Filter := s;
end;

procedure TevClientDataSet.DataRequest(Data: IExecDSWrapper);
var
  s: string;
begin
  s := Data.QueryText;
  CustomDataRequest := Data;
  if Copy(s, 1, 1) = ';' then
  begin
    Close;
    ctx_DataAccess.OpenDataSets([Self]);
  end;
  if Pos(';', s) > 1 then
    FCustomName := UpperCase(Copy(s, 1, Pos(';', s) - 1));
end;

procedure TevClientDataSet.DataRequest(const s: string);
begin
  DataRequest(TExecDSWrapper.Create(s));
end;

function TevClientDataSet.GetShadowDataSet: TevShadowClientDataSet;
var
  s: string;
begin
  if not Assigned(FShadowDataSet) then
  begin
    CheckActive;
    FShadowDataSet := TevShadowClientDataSet.Create(nil);
    FShadowDataSet.CloneCursor(Self, True, True);
    if Copy(ProviderName, Length(ProviderName)-4, 5) = '_PROV' then
      s := Copy(ProviderName, 1, Length(ProviderName)-5)
    else
      s := Name;
    if s <> '' then
    begin
      if FShadowDataSet.FindField(s+ '_NBR') <> nil then
        FShadowDataSet.IndexFieldNames := s+ '_NBR'
      else
      if s = 'TMP_CL' then
        FShadowDataSet.IndexFieldNames := 'CL_NBR'
      else
      if Copy(s, 1, 4) = 'TMP_' then
        FShadowDataSet.IndexFieldNames := 'CL_NBR;'+ Copy(s, 5, 255)+ '_NBR';
    end;
  end;
  Result := FShadowDataSet;
end;

procedure TevClientDataSet.DoBeforeClose;
var
  i: Integer;
begin
  FreeAndNil(FShadowDataSet);
  for i := ComponentCount-1 downto 0 do
    if (Components[i] is TevClientDataSet)
    and (Copy(Components[i].Name, 1, Length(ClonePrefix)) = ClonePrefix) then
      Components[i].Free;
  inherited;
end;

procedure TevClientDataSet.SetLookupFieldTag(const f: TField;
  const FieldName: string; const LookupDataSet: TEvClientDataSet);
begin
  if LookupDataSet.Active then
    f.Tag := LookupDataSet.ShadowDataSet.FieldByName(FieldName).Index;
end;

procedure TevClientDataSet.SetLookupFieldTag(const f: TField;
  const LookupDataSet: TEvClientDataSet);
begin
  SetLookupFieldTag(f, f.FieldName, LookupDataSet);
end;

procedure TevClientDataSet.DoOnCalcFields;
begin
end;

procedure TevClientDataSet.SaveStateAndReset;
begin
  SaveState;
  Filter := '';
  UserFilter := '';
  UserFiltered := False;
  Filtered := False;
  IndexName := '';
  IndexFieldNames := '';
end;

procedure TevClientDataSet.SaveStateAndSet(const NewFiltered: Boolean;
  const NewFilter, NewIndexName, NewIndexFieldNames: string);
begin
  SaveState;
  Filter := NewFilter;
  Filtered := NewFiltered;
  if NewIndexName <> '' then
    IndexName := NewIndexName
  else if NewIndexFieldNames <> '' then
    IndexFieldNames := NewIndexFieldNames
  else
    IndexFieldNames := '';
end;

procedure TevClientDataSet.AssignDisplayFormatPictureMasks;
var
  i, j, ind: Integer;
  sMask: string;
  Digits: Integer;
begin
  for i := 0 to Pred(FieldCount) do
    if Fields[i] is TFloatField then
    begin
      if TFloatField(Fields[i]).DisplayFormat = '' then
        TFloatField(Fields[i]).DisplayFormat := '#,##0.00';
      if TFloatField(Fields[i]).EditFormat = '' then
        TFloatField(Fields[i]).EditFormat := StringReplace(TFloatField(Fields[i]).DisplayFormat, ',', '', [rfReplaceAll]);
      if not SkipFieldCheck then
      begin
        Digits := Pos('.', TFloatField(Fields[i]).DisplayFormat);
        if Digits > 0 then
        begin
          Digits := Length(TFloatField(Fields[i]).DisplayFormat) - Digits;
          if Digits > 1 then
            sMask := Format('[-]{{#[#][#]{{;,###*[;,###]},*#}[.*%d[#]]},.#*%d[#]}', [Digits, Pred(Digits)])
          else
            sMask := '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*1[#]]},.#}';
          ind := -1;
          for j := 0 to Pred(PictureMasks.Count) do
            if LeftStr(PictureMasks[j], Pred(Pos(#9, PictureMasks[j]))) = Fields[i].FieldName then
            begin
              ind := j;
              Break;
            end;
          if ind = -1 then
            PictureMasks.Add(Fields[i].FieldName + #9 + sMask + #9'T'#9'F')
          else
            PictureMasks[ind] := Fields[i].FieldName + #9 + sMask + #9'T'#9'F';
        end;
      end;
    end;
end;

constructor TevClientDataSet.CreateWithChecksDisabled(AOwner: TComponent; const Dummy: Boolean = False);
begin
  Create(AOwner);
  SkipFieldCheck := True;
end;

procedure TevClientDataSet.Open;
begin
  Active := True;
end;

procedure TevClientDataSet.CreateDataSet;
begin
  FInSetActive := True;
  try
    inherited;
  finally
    FInSetActive := False;
  end;
  SetInternalFilter(RetrieveCondition);
end;

procedure TevClientDataSet.LoadFromUniversalStream(const ms: IEvDualStream);
begin
  FInSetActive := True;
  try
    inherited;
  finally
    FInSetActive := False;
  end;
end;

procedure TevClientDataSet.SetLookupsEnabled(const Value: Boolean);
begin
  if Value and (FLookupsEnabled <> Value) then
    RecalcOnFetch := True;
  FLookupsEnabled := Value;
end;

procedure TevClientDataSet.SetDelta(const Value: IEvDualStream);
begin
  FInSetActive := True;
  try
    inherited;
  finally
    FInSetActive := False;
  end;
end;

procedure TevClientDataSet.LoadFromFile(const FileName: string);
begin
  FInSetActive := True;
  try
    inherited;
  finally
    FInSetActive := False;
  end;
  SetInternalFilter(RetrieveCondition);
end;

procedure TevClientDataSet.LoadFromStream(Stream: TStream);
begin
  FInSetActive := True;
  try
    inherited;
  finally
    FInSetActive := False;
  end;
  SetInternalFilter(RetrieveCondition);
end;

function TevClientDataSet.GetClone(
  const CloneName: string): TevClientDataSet;
begin
  Result := FindComponent(ClonePrefix+ CloneName) as TevClientDataSet;
  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(Self);
    Result.Name := ClonePrefix+ CloneName;
    Result.CloneCursor(Self, False);
  end;
end;

procedure TevClientDataSet.ResetLockedIndexFieldNames(
  const pIndexFieldNames: string);
begin
  FFilterIndexIsLocked := False;
  if pIndexFieldNames = '' then
  try
    IndexFieldNames := pIndexFieldNames;
  except
  end
  else
    IndexFieldNames := pIndexFieldNames;
end;

procedure TevClientDataSet.SetLockedIndexFieldNames(
  const pIndexFieldNames: string);
begin
  IndexFieldNames := pIndexFieldNames;
  FFilterIndexIsLocked := False;
end;

procedure TevClientDataSet.SaveNamedState(const Name: string;
  const SaveData: Boolean);
var
  i: Integer;
begin
  for i := 0 to High(FNamedSavedStates) do
    if FNamedSavedStates[i].Name = Name then
    begin
      InternalSaveState(FNamedSavedStates[i].SavedState, SaveData);
      Exit;
    end;
  SetLength(FNamedSavedStates, Length(FNamedSavedStates)+1);
  FNamedSavedStates[High(FNamedSavedStates)].Name := Name;
  InternalSaveState(FNamedSavedStates[High(FNamedSavedStates)].SavedState, SaveData);
end;

procedure TevClientDataSet.InternalLoadState(
  const SavedState: TDSSavedState);
begin
  ClientID := SavedState.ClientID;
  if Assigned(SavedState.Data) then
    Close;
  CheckDSCondition(SavedState.OpenCondition);
  if Assigned(SavedState.Data) then
    LoadFromUniversalStream(SavedState.Data);
  if Active <> SavedState.Active then
    Active := SavedState.Active;
  if (State = dsBrowse) or not SavedState.Active then
  begin
    IndexName := SavedState.IndexName;
    IndexFieldNames := SavedState.IndexFieldNames;
    if RetrieveCondition <> SavedState.RetrieveCondition then
      RetrieveCondition := SavedState.RetrieveCondition;
    Filter := SavedState.Filter;
    UserFilter := SavedState.UserFilter;
    UserFiltered := SavedState.UserFiltered;
    Filtered := SavedState.Filtered;
    if SavedState.Nbr <> 0 then
      Locate(TableName + '_NBR', SavedState.Nbr, []);
    if SavedState.BrowseState = dsEdit then
      Edit
    else if SavedState.BrowseState = dsInsert then
      Insert;
    FGenerationNumber := SavedState.GenerationNumber;
    FLastTimeModified := SavedState.LastTimeModified;
    FLastTimeOpened := SavedState.LastTimeOpened;
  end;
end;

procedure TevClientDataSet.InternalSaveState(
  var SavedState: TDSSavedState; const SaveData: Boolean);
begin
  with SavedState do
  begin
    BrowseState := State;
    OpenCondition := Self.OpenCondition;
    RetrieveCondition := Self.RetrieveCondition;
    ClientID := Self.ClientID;
    Filter := Self.Filter;
    UserFilter := Self.UserFilter;
    UserFiltered := Self.UserFiltered;
    Filtered := Self.Filtered;
    if Self.Active and Assigned(FindField(TableName + '_NBR')) then
      Nbr := FindField(TableName + '_NBR').AsInteger
    else
      Nbr := 0;
    Active := Self.Active;
    IndexName := Self.IndexName;
    IndexFieldNames := Self.IndexFieldNames;
    if SaveData and Self.Active then
      Data := GetDataStream(True, True)
    else
      Data := nil;
    GenerationNumber := Self.FGenerationNumber;
    LastTimeModified := Self.FLastTimeModified;
    LastTimeOpened := Self.FLastTimeOpened;
  end;
end;

procedure TevClientDataSet.LoadNamedState(const Name: string);
var
  i: Integer;
begin
  for i := 0 to High(FNamedSavedStates) do
    if FNamedSavedStates[i].Name = Name then
    begin
      InternalLoadState(FNamedSavedStates[i].SavedState);
      Break;
    end;
end;

procedure TevClientDataSet.InitNamedState(const Name: string);
var
  i, j: Integer;
begin
  for i := 0 to High(FNamedSavedStates) do
    if FNamedSavedStates[i].Name = Name then
    begin
      for j := i+1 to High(FNamedSavedStates) do
        FNamedSavedStates[j-1] := FNamedSavedStates[j];
      SetLength(FNamedSavedStates, Length(FNamedSavedStates)-1);
      Break;
    end;
end;

procedure TevClientDataSet.ResetNamedSavedStates;
begin
  SetLength(FNamedSavedStates, 0);
end;

procedure TevClientDataSet.AssignDataFrom(const ds: TISKbmMemDataSet);
begin
  inherited;
  SetInternalFilter(RetrieveCondition);
end;

procedure TevClientDataSet.DoCustomAfterOpen;
begin
end;

procedure TevClientDataSet.DoAfterChangesApply;
begin
  MergeChangeLog;
  DoAfterCommit;
end;

procedure TevClientDataSet.Activate;
begin
  if not Active then
    Self.DataRequired('ALL');
end;

procedure TevClientDataSet.DataRequired(aCondition: string);
begin
  if (Self.ClientID >= 0) and
     (Self.ClientID <> ClientID) then
    Self.ClientID := ctx_DataAccess.ClientID;
  if AnsiSameText(aCondition, 'ALL') then
    aCondition := '';
  CheckDSCondition(aCondition);
  ctx_DataAccess.OpenDataSets([Self]);
  Self.RetrieveCondition := aCondition;
end;

procedure TevClientDataSet.EnsureDSCondition(const aCondition: string);
begin
  if not DSConditionsEqual(RetrieveCondition, aCondition) or not Active then
    DataRequired(aCondition);
end;

procedure TevClientDataSet.EnsureHasMetadata;
begin
  if FieldCount = 0 then
    DataRequired(AlwaysFalseCond);
end;

procedure TevProxyDataSet.Loaded;
begin
  inherited;
  GetNewEvents;
  GetOldEvents;
  SetAllEvents;
end;

destructor TevProxyDataSet.Destroy;
begin
  RevertToOldEvents;
  inherited;
end;

procedure TevProxyDataSet.Notification (AComponent :TComponent;
                                 Operation :TOperation);
begin
  inherited Notification (AComponent, Operation);
  if ( Operation = opRemove ) and ( AComponent = FDataset ) then
    FDataset := Nil;
end;

procedure TevProxyDataSet.SetDataset (Value :TDataSet);
begin
  if Value <> FDataset then
    FDataset := Value;
end;

procedure TevProxyDataSet.AssignPropsToDataSet;
begin
end;

procedure TevProxyDataSet.GetNewEvents;
begin
  NewBeforeOpen := BeforeOpen;
  NewAfterOpen := AfterOpen;
  NewBeforeClose := BeforeClose;
  NewAfterClose := AfterClose;
  NewBeforeInsert := BeforeInsert;
  NewAfterInsert := AfterInsert;
  NewBeforeEdit := BeforeEdit;
  NewAfterEdit := AfterEdit;
  NewBeforePost := BeforePost;
  NewAfterPost := AfterPost;
  NewBeforeCancel := BeforeCancel;
  NewAfterCancel := AfterCancel;
  NewBeforeDelete := BeforeDelete;
  NewAfterDelete := AfterDelete;
  NewBeforeRefresh := BeforeRefresh;
  NewAfterRefresh := AfterRefresh;
  NewBeforeScroll := BeforeScroll;
  NewAfterScroll := AfterScroll;
  NewOnNewRecord := OnNewRecord;
  NewOnCalcFields := OnCalcFields;
  NewOnEditError := OnEditError;
  NewOnPostError := OnPostError;
  NewOnDeleteError := OnDeleteError;
  NewOnFilterRecord := OnFilterRecord;
end;

procedure TevProxyDataSet.GetOldEvents;
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
  with (DataSet as TEvBasicClientDataSet) do
  begin
    OldBeforeOpen := BeforeOpen;
    OldAfterOpen := AfterOpen;
    OldBeforeClose := BeforeClose;
    OldAfterClose := AfterClose;
    OldBeforeInsert := BeforeInsert;
    OldAfterInsert := AfterInsert;
    OldBeforeEdit := BeforeEdit;
    OldAfterEdit := AfterEdit;
    OldBeforePost := BeforePost;
    OldAfterPost := AfterPost;
    OldBeforeCancel := BeforeCancel;
    OldAfterCancel := AfterCancel;
    OldBeforeDelete := BeforeDelete;
    OldAfterDelete := AfterDelete;
    OldBeforeRefresh := BeforeRefresh;
    OldAfterRefresh := AfterRefresh;
    OldBeforeScroll := BeforeScroll;
    OldAfterScroll := AfterScroll;
    OldOnNewRecord := OnNewRecord;
    OldOnCalcFields := OnCalcFields;
    OldOnEditError := OnEditError;
    OldOnPostError := OnPostError;
    OldOnDeleteError := OnDeleteError;
    OldOnFilterRecord := OnFilterRecord;
  end;
end;

procedure TevProxyDataSet.AllBeforeOpen(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeOpen) then
        OldBeforeOpen(DataSet);
      if Assigned(NewBeforeOpen) then
        NewBeforeOpen(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterOpen(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterOpen) then
        OldAfterOpen(DataSet);
      if Assigned(NewAfterOpen) then
        NewAfterOpen(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeClose(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeClose) then
        OldBeforeClose(DataSet);
      if Assigned(NewBeforeClose) then
        NewBeforeClose(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterClose(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterClose) then
        OldAfterClose(DataSet);
      if Assigned(NewAfterClose) then
        NewAfterClose(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeInsert(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeInsert) then
        OldBeforeInsert(DataSet);
      if Assigned(NewBeforeInsert) then
        NewBeforeInsert(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterInsert(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterInsert) then
        OldAfterInsert(DataSet);
      if Assigned(NewAfterInsert) then
        NewAfterInsert(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeEdit(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeEdit) then
        OldBeforeEdit(DataSet);
      if Assigned(NewBeforeEdit) then
        NewBeforeEdit(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterEdit(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterEdit) then
        OldAfterEdit(DataSet);
      if Assigned(NewAfterEdit) then
        NewAfterEdit(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforePost(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforePost) then
        OldBeforePost(DataSet);
      if Assigned(NewBeforePost) then
        NewBeforePost(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterPost(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterPost) then
        OldAfterPost(DataSet);
      if Assigned(NewAfterPost) then
        NewAfterPost(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeCancel(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeCancel) then
        OldBeforeCancel(DataSet);
      if Assigned(NewBeforeCancel) then
        NewBeforeCancel(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterCancel(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterCancel) then
        OldAfterCancel(DataSet);
      if Assigned(NewAfterCancel) then
        NewAfterCancel(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeDelete(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeDelete) then
        OldBeforeDelete(DataSet);
      if Assigned(NewBeforeDelete) then
        NewBeforeDelete(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterDelete(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterDelete) then
        OldAfterDelete(DataSet);
      if Assigned(NewAfterDelete) then
        NewAfterDelete(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeRefresh(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeRefresh) then
        OldBeforeRefresh(DataSet);
      if Assigned(NewBeforeRefresh) then
        NewBeforeRefresh(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterRefresh(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterRefresh) then
        OldAfterRefresh(DataSet);
      if Assigned(NewAfterRefresh) then
        NewAfterRefresh(DataSet);
    end;
end;

procedure TevProxyDataSet.AllBeforeScroll(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldBeforeScroll) then
        OldBeforeScroll(DataSet);
      if Assigned(NewBeforeScroll) then
        NewBeforeScroll(DataSet);
    end;
end;

procedure TevProxyDataSet.AllAfterScroll(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldAfterScroll) then
        OldAfterScroll(DataSet);
      if Assigned(NewAfterScroll) then
        NewAfterScroll(DataSet);
    end;
end;

procedure TevProxyDataSet.AllOnNewRecord(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnNewRecord) then
        OldOnNewRecord(DataSet);
      if Assigned(NewOnNewRecord) then
        NewOnNewRecord(DataSet);
    end;
end;

procedure TevProxyDataSet.AllOnCalcFields(DataSet: TDataSet);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnCalcFields) then
        OldOnCalcFields(DataSet);
      if Assigned(NewOnCalcFields) then
        NewOnCalcFields(DataSet);
    end;
end;

procedure TevProxyDataSet.AllOnEditError(DataSet: TDataSet;
         E: EDatabaseError; var Action: TDataAction);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnEditError) then
        OldOnEditError(DataSet, E, Action);
      if Assigned(NewOnEditError) then
        NewOnEditError(DataSet, E, Action);
    end;
end;

procedure TevProxyDataSet.AllOnPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnPostError) then
        OldOnPostError(DataSet, E, Action);
      if Assigned(NewOnPostError) then
        NewOnPostError(DataSet, E, Action);
    end;
end;

procedure TevProxyDataSet.AllOnDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnDeleteError) then
        OldOnDeleteError(DataSet, E, Action);
      if Assigned(NewOnDeleteError) then
        NewOnDeleteError(DataSet, E, Action);
    end;
end;

procedure TevProxyDataSet.AllOnFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      if Assigned(OldOnFilterRecord) then
        OldOnFilterRecord(DataSet, Accept);
      if Assigned(NewOnFilterRecord) then
        NewOnFilterRecord(DataSet, Accept);
    end;
end;

procedure TevProxyDataSet.SetAllEvents;
begin
  if Assigned(Dataset) then
  begin
    if Assigned(BeforeOpen) then Dataset.BeforeOpen := AllBeforeOpen;
    if Assigned(AfterOpen) then Dataset.AfterOpen := AllAfterOpen;
    if Assigned(BeforeClose) then Dataset.BeforeClose := AllBeforeClose;
    if Assigned(AfterClose) then Dataset.AfterClose := AllAfterClose;
    if Assigned(BeforeInsert) then Dataset.BeforeInsert := AllBeforeInsert;
    if Assigned(AfterInsert) then Dataset.AfterInsert := AllAfterInsert;
    if Assigned(BeforeEdit) then Dataset.BeforeEdit := AllBeforeEdit;
    if Assigned(AfterEdit) then Dataset.AfterEdit := AllAfterEdit;
    if Assigned(BeforePost) then Dataset.BeforePost := AllBeforePost;
    if Assigned(AfterPost) then Dataset.AfterPost := AllAfterPost;
    if Assigned(BeforeCancel) then Dataset.BeforeCancel := AllBeforeCancel;
    if Assigned(AfterCancel) then Dataset.AfterCancel := AllAfterCancel;
    if Assigned(BeforeDelete) then Dataset.BeforeDelete := AllBeforeDelete;
    if Assigned(AfterDelete) then Dataset.AfterDelete := AllAfterDelete;
    if Assigned(BeforeRefresh) then Dataset.BeforeRefresh := AllBeforeRefresh;
    if Assigned(AfterRefresh) then Dataset.AfterRefresh := AllAfterRefresh;
    if Assigned(BeforeScroll) then Dataset.BeforeScroll := AllBeforeScroll;
    if Assigned(AfterScroll) then DataSet.AfterScroll := AllAfterScroll;
    if Assigned(OnNewRecord) then Dataset.OnNewRecord := AllOnNewRecord;
    if Assigned(OnCalcFields) then Dataset.OnCalcFields := AllOnCalcFields;
    if Assigned(OnEditError) then Dataset.OnEditError := AllOnEditError;
    if Assigned(OnPostError) then Dataset.OnPostError := AllOnPostError;
    if Assigned(OnDeleteError) then Dataset.OnDeleteError := AllOnDeleteError;
  end;
end;

procedure TevProxyDataSet.RevertToOldEvents;
begin
  if Assigned(DataSet) and (DataSet is TEvBasicClientDataSet) then
    with (DataSet as TEvBasicClientDataSet) do
    begin
      BeforeOpen := OldBeforeOpen;
      AfterOpen := OldAfterOpen;
      BeforeClose := OldBeforeClose;
      AfterClose := OldAfterClose;
      BeforeInsert := OldBeforeInsert;
      AfterInsert := OldAfterInsert;
      BeforeEdit := OldBeforeEdit;
      AfterEdit := OldAfterEdit;
      BeforePost := OldBeforePost;
      AfterPost := OldAfterPost;
      BeforeCancel := OldBeforeCancel;
      AfterCancel := OldAfterCancel;
      BeforeDelete := OldBeforeDelete;
      AfterDelete := OldAfterDelete;
      BeforeRefresh := OldBeforeRefresh;
      AfterRefresh := OldAfterRefresh;
      BeforeScroll := OldBeforeScroll;
      AfterScroll := OldAfterScroll;
      OnNewRecord := OldOnNewRecord;
      OnCalcFields := OldOnCalcFields;
      OnEditError := OldOnEditError;
      OnPostError := OldOnPostError;
      OnDeleteError := OldOnDeleteError;
    end;
end;


procedure TevClientDataSet.DataRequired(const aFieldList: TisCommaDilimitedString; const aCondition: string);
var
  sCond: String;
  sFld: TisCommaDilimitedString;
begin
  if (Self.ClientID >= 0) and
     (Self.ClientID <> ClientID) then
    Self.ClientID := ctx_DataAccess.ClientID;

  if AnsiSameText(aCondition, 'ALL') then
    sCond := ''
  else
    sCond := aCondition;

  if FOpenFieldList <> aFieldList then
  begin
    Close;
    sFld := aFieldList;
  end
  else
    sFld := FOpenFieldList;

  CheckDSCondition(sCond);

  FOpenFieldList := sFld;
  ctx_DataAccess.OpenDataSets([Self]);

  Self.RetrieveCondition := aCondition;
end;

function TevClientDataSet.GetBlobData(const AFieldName: String): IisStream;
var
  Q: IevQuery;
  KeyNbr: String;
  ValItem: IisListOfValues;
  Val: Variant;
  Fld: TField;
  Data: IisStream;
begin
  Result := nil;

  Fld := FindField(AFieldName);
  if not Assigned(Fld) then
    Exit;

  if Fld.IsBlob then
  begin
    if BlobsLoadMode = blmManualLoad then
    begin
      InitBlobRawData(TBlobField(Fld));
      try
        if not CheckIfBlobLoaded then
          LoadBlob(Fld.FieldName);
      finally
        DeinitBlobRawData;
      end;
    end;

    Result := TisStream.Create(TBlobField(Fld).Size);
    TBlobField(Fld).SaveToStream(Result.RealStream);
    Result.Position := 0;

    Exit;
  end;

  ValItem := FRefBlobData.ParamsByName(Fld.FieldName);
  if not Assigned(ValItem) then
    Exit;

  KeyNbr := FieldByName(KeyFieldName).AsString;
  if KeyNbr = '' then
    Exit;

  Val := ValItem.TryGetValue(KeyNbr, Null);

  if VarIsNull(Val) then
  begin
    if not Fld.IsNull then
    begin
      Q := TevQuery.CreateFmt('SELECT t.data FROM %s t WHERE t.%s_nbr = :nbr AND {AsOfDate<t>}', [FRefBlobTable, FRefBlobTable]);
      Q.Params.AddValue('nbr', Fld.AsInteger);
      Q.Params.AddValue('StatusDate', AsOfDate);
      Q.Execute;
      if Q.Result.RecordCount = 1 then
      begin
        Result := TisStream.Create;
        TBlobField(Q.Result.Fields[0]).SaveToStream(Result.RealStream);
        if Result.Size = 0 then
          Result := nil;
      end;
    end;

    Val := VarArrayCreate([0, 2], varVariant);
    Val[0] := Result;  // Value
    Val[1] := False;   // Modified flag
    Val[2] := Result;  // Old value
    ValItem.AddValue(KeyNbr, Val);
  end;

  Data := IInterface(Val[0]) as IisStream;

  if Assigned(Data) then
  begin
    Result := Data.GetClone;
    Result.Position := 0;
  end;
end;

Procedure TevClientDataSet.DeleteFromRefBlobData(const AFieldName: String);
var
  KeyNbr: String;
  ValItem: IisListOfValues;
  Val: Variant;
  Fld: TField;
begin

  Fld := FindField(AFieldName);
  if not Assigned(Fld) then
    Exit;

  if Fld.IsBlob then
    Exit;

  ValItem := FRefBlobData.ParamsByName(Fld.FieldName);
  if not Assigned(ValItem) then
    Exit;

  KeyNbr := FieldByName(KeyFieldName).AsString;
  if KeyNbr = '' then
    Exit;

  Val := ValItem.TryGetValue(KeyNbr, Null);

  if not VarIsNull(Val) then
    ValItem.DeleteValue(KeyNbr);

end;


procedure TevClientDataSet.UpdateBlobData(const AFieldName: String; const AData: IisStream);
var
  Fld: TField;
  ValItem: IisNamedValue;
  Val: Variant;
begin
  Fld := FindField(AFieldName);
  if not Assigned(Fld) then
    Exit;

  if not (State in dsEditModes) then DatabaseError(SNotEditing, Self);

  if Fld.IsBlob then
  begin
    if (AData = nil) or (AData.Size = 0) then
      TBlobField(Fld).Clear
    else
      TBlobField(Fld).LoadFromStream(AData.RealStream);
    Exit;
  end;

  if (AData = nil) or (AData.Size = 0) then
  begin
    GetBlobData(AFieldName); // Cache blob data
    Fld.Clear;
  end
  else if Fld.IsNull then
  begin
    Fld.AsInteger := ctx_DBAccess.GetNewKeyValue(FRefBlobTable);
    GetBlobData(AFieldName); // Cache blob data
  end
  else
    GetBlobData(AFieldName); // Cache blob data

    
  ValItem := FRefBlobData.ParamsByName(Fld.FieldName).FindValue(FieldByName(KeyFieldName).AsString);

  Val := ValItem.Value;
  if Assigned(AData) then
    Val[0] := AData.GetClone
  else
    Val[0] := AData;
  ValItem.Value := Val;

  if not Assigned(FBlobRefCurrentChanges) then
    FBlobRefCurrentChanges := TisInterfaceList.Create;

  FBlobRefCurrentChanges.Add(ValItem);

  // This code will send notifications to DataLink
  if Fld.IsNull then
    Fld.Clear
  else
    Fld.AsInteger := Fld.AsInteger;
end;

function TevClientDataSet.GetKeyFieldName: String;
begin
  Result := TableName + '_NBR';
end;

procedure TevClientDataSet.UpdateBlobData(const AFieldName, AData: String);
var
  S: IisStream;
begin
  if AData = '' then
    S := nil
  else
  begin
    S := TisStream.Create;
    S.AsString := AData;
  end;

  UpdateBlobData(AFieldName, S);
end;


function TevClientDataSet.GetChangePacket: IevDataChangePacket;
var
  Changes: TEvBasicClientDataSet;
  ChangePacket: IevDataChangePacket;
  ChangeItem: IevDataChangeItem;
  KeyField, Fld: TField;
  i: Integer;
  refFields: array of TField;

  procedure ProcessNewAndUpdatedRefBlobChanges;
  var
    Item: IisListOfValues;
    i: Integer;
    fld: TField;
    RecVal: Variant;
  begin
    if Changes.CompleteUpdateStatus in [usInserted, usModified] then
    begin
      for i := 0 to High(refFields) do
      begin
         fld := refFields[i];
         if fld <> nil then
         begin
           Item := FRefBlobData[i];
           RecVal := Item.TryGetValue(KeyField.AsString, Null);

           if not VarIsNull(RecVal) and not VarIsNull(fld.NewValue) then
             if Boolean(RecVal[1]) then
             begin
               if (Changes.CompleteUpdateStatus = usInserted) or VarIsNull(fld.OldValue) then
               begin
                 ChangeItem := ChangePacket.NewInsertRecord(FRefBlobTable);
                 ChangeItem.Key := fld.NewValue;
                 ChangeItem.Fields.AddValue('DATA', IInterface(RecVal[0]) as IisStream);
               end
               else if not VarIsNull(fld.OldValue) then
               begin
                 ChangeItem := ChangePacket.NewUpdateFields(FRefBlobTable, fld.NewValue);
                 ChangeItem.Fields.AddValue('DATA', IInterface(RecVal[0]) as IisStream);
               end;
             end;
         end;
      end;
    end;
  end;

  procedure ProcessDeletedRefBlobs;
  var
    i: Integer;
    fld: TField;
  begin
    if Changes.CompleteUpdateStatus in [usModified, usDeleted] then
    begin
      for i := 0 to High(refFields) do
      begin
         fld := refFields[i];
         if Assigned(fld) and not VarIsNull(fld.OldValue) and
           ((Changes.CompleteUpdateStatus = usDeleted) or
             not VarSameValue(fld.OldValue, fld.NewValue)) then
         begin
           ChangePacket.NewDeleteRecord(FRefBlobTable, fld.OldValue);
         end;
      end;
    end;
  end;

begin
  FConstructingChangePacket := True;
  try
    if Active and (ChangeCount > 0) and (TableName <> '') then
    begin
      ChangePacket := TevDataChangePacket.Create;
      if OpenedAsOfToday then
        ChangePacket.AsOfDate := EmptyDate
      else
        ChangePacket.AsOfDate := AsOfDate;

      Changes := TEvBasicClientDataSet.Create(nil);
      try
        Changes.Delta := Delta;

        KeyField := Changes.FieldByName(KeyFieldName);

        if Assigned(FRefBlobData) then
        begin
          SetLength(refFields, FRefBlobData.Count);
          for i := 0 to FRefBlobData.Count - 1 do
            refFields[i] := Changes.FieldByName(FRefBlobData.ParamName(i));
        end
        else
          SetLength(refFields, 0);

        Changes.First;
        while not Changes.Eof do
        begin
          ProcessNewAndUpdatedRefBlobChanges;

          case Changes.CompleteUpdateStatus of
            usInserted:
            begin
              ChangeItem := ChangePacket.NewInsertRecord(TableName);

              for i := 0 to Changes.FieldCount - 1 do
              begin
                Fld := Changes.Fields[i];
                if Fld.FieldKind = fkData then
                  if not Fld.IsNull then
                    ChangeItem.Fields.AddValue(AnsiUpperCase(Fld.FieldName), Fld.Value);
              end;
            end;

            usModified:
            begin
              ChangeItem := ChangePacket.NewUpdateFields(TableName, KeyField.AsInteger);

              for i := 0 to Changes.FieldCount - 1 do
              begin
                Fld := Changes.Fields[i];
                if Fld.FieldKind = fkData then
                begin
                  if not VarSameValue(Fld.OldValue, Fld.NewValue) then
                    ChangeItem.Fields.AddValue(AnsiUpperCase(Fld.FieldName), Fld.Value);
                end;
              end;

              if ChangeItem.Fields.Count = 0 then
                ChangePacket.RemoveItem(ChangeItem);
            end;

            usDeleted:
            begin
              ChangeItem := ChangePacket.NewDeleteRecord(TableName, KeyField.AsInteger);
            end;
          else
            Assert(False);
          end;

          ProcessDeletedRefBlobs;

          Changes.Next;
        end;

      finally
        FreeAndNil(Changes);
      end;

      if ChangePacket.Count = 0 then
        ChangePacket := nil;
    end
    else
      ChangePacket := nil;

    Result := ChangePacket;
  finally
    FConstructingChangePacket := False;
  end;
end;

procedure TevClientDataSet.SetBlobsLoadMode(const Value: TevBlobsLoadMode);
begin
  if not Active then
    FBlobsLoadMode := Value;
end;

function TevClientDataSet.CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream;
var
  alreadyLoaded: Boolean;
  KeyFld: TField;
begin
  Result := nil;

  if not FConstructingChangePacket and not TBlobField(Field).Modified and not Field.IsNull then
  begin
    KeyFld := FindField(KeyFieldName);
    if Assigned(KeyFld) and (KeyFld.AsInteger > 0) then
    begin
      // Perform blob data lazy load
      alreadyLoaded := False;
      InitBlobRawData(TBlobField(Field));
      try
        alreadyLoaded := CheckIfBlobLoaded;
        if not alreadyLoaded then
        begin
          if BlobsLoadMode = blmManualLoad then
          begin
            FCurrentBlobStream.Clear;
            alreadyLoaded := True;
          end
          else
            LoadBlob(Field.FieldName);
        end
      finally
        if not alreadyLoaded or (Mode <> bmRead) then
          DeinitBlobRawData
        else
        begin
          Result := FCurrentBlobStream;
          FCurrentBlobStream := nil;
        end;
      end;
    end;
  end;

  if not Assigned(Result) then
    Result := inherited CreateBlobStream(Field, Mode);
end;

function TevClientDataSet.IsBlobLoaded(const AFieldName: String): Boolean;
var
  KeyNbr: String;
  ValItem: IisListOfValues;
  Val: Variant;
  Fld: TField;
begin
  Result := False;

  Fld := FindField(AFieldName);
  if not Assigned(Fld) then
    Exit;

  if Fld.IsBlob then
  begin
    if not assigned(FCurrentBlobStream) then
    begin
      InitBlobRawData(TBlobField(Fld));
      try
        Result := CheckIfBlobLoaded;
      finally
        DeinitBlobRawData;
      end;
    end
    else
      Result := CheckIfBlobLoaded;
    Exit;
  end;

  if Fld.IsNull then
  begin
    Result := True;
    Exit;
  end;

  ValItem := FRefBlobData.ParamsByName(Fld.FieldName);
  if not Assigned(ValItem) then
    Exit;

  KeyNbr := FieldByName(KeyFieldName).AsString;
  Val := ValItem.TryGetValue(KeyNbr, Null);

  Result := not VarIsNull(Val);
end;

procedure TevClientDataSet.InitBlobRawData(AField: TBlobField);
begin
  FCurrentBlobStream := TkbmBlobStream(inherited CreateBlobStream(AField, bmRead));
end;

function TevClientDataSet.CheckIfBlobLoaded: Boolean;
var
  s: String;
begin
  Result := True;

  if FCurrentBlobStream.Size = Length(LAZY_BLOB) then
  begin
    SetLength(s, FCurrentBlobStream.Size);
    Move(FCurrentBlobStream.Memory^, s[1], FCurrentBlobStream.Size);
    Result := s <> LAZY_BLOB;
  end;
end;

procedure TevClientDataSet.DeinitBlobRawData;
begin
  FreeAndNil(FCurrentBlobStream);
end;

procedure TevClientDataSet.LoadBlob(const AFieldName: String);
var
  Q: IevQuery;
begin
  Q := TevQuery.Create('SELECT t.' + AFieldName + ' FROM ' + TableName + ' t WHERE t.' + KeyFieldName + ' = :nbr AND {AsOfDate<t>}');
  Q.Params.AddValue('nbr', FieldByName(KeyFieldName).AsInteger);
  Q.Params.AddValue('StatusDate', AsOfDate);
  Q.Execute;

  if Q.Result.RecordCount = 1 then
    FCurrentBlobStream.SetLazyData(TBlobField(Q.Result.Fields[0]))
  else
    FCurrentBlobStream.SetLazyData(nil);
end;

procedure TevClientDataSet.InitRefBlobList;
var
  i: Integer;
  TblClass, RefTblClass: TddTableClass;
  RefFlds: TddReferencesDesc;
begin
  FRefBlobTable := '';
  FRefBlobData := nil;
  FBlobRefCurrentChanges := nil;

  TblClass := GetddTableClassByName(TableName);
  if not Assigned(TblClass) then
    Exit;

  RefTblClass := TblClass.GetRefBlobTableClass;
  if not Assigned(RefTblClass) then
    Exit;

  FRefBlobTable := RefTblClass.GetTableName;

  RefFlds := TblClass.GetRefBlobFieldList;
  FRefBlobData := TisParamsCollection.Create;
  for i := 0 to High(RefFlds) do
    if FindField(RefFlds[i].SrcFields[0]) <> nil then
      FRefBlobData.AddParams(RefFlds[i].SrcFields[0]);

  if FRefBlobData.Count = 0 then
  begin
    FRefBlobData := nil;
    FRefBlobTable := '';
  end
end;

procedure TevClientDataSet.PostPendingRefBlobsChanges;
var
  i: Integer;
  Item: IisNamedValue;
  val: Variant;
begin
  if Assigned(FBlobRefCurrentChanges) then
  begin
    for i := 0 to FBlobRefCurrentChanges.Count - 1 do
    begin
      Item :=  FBlobRefCurrentChanges[i] as IisNamedValue;
      val := Item.Value;
      val[2] := Val[0];
      val[1] := True;
      Item.Value := val;
    end;
  end;

  FBlobRefCurrentChanges := nil;
end;

procedure TevClientDataSet.CancelPendingRefBlobsChanges;
var
  i: Integer;
  Item: IisNamedValue;
  val: Variant;
begin
  if Assigned(FBlobRefCurrentChanges) then
  begin
    for i := 0 to FBlobRefCurrentChanges.Count - 1 do
    begin
      Item :=  FBlobRefCurrentChanges[i] as IisNamedValue;
      val := Item.Value;
      val[0] := Val[2];
      Item.Value := val;
    end;
  end;

  FBlobRefCurrentChanges := nil;
end;

procedure TevClientDataSet.CustomReconcile(const MapList: IisValueMap);
var
  i, j: Integer;
  k: Variant;
  Recs: IisListOfValues;
  val: Variant;
begin
  inherited;

  if Assigned(FRefBlobData) then
  begin
    for i := 0 to FRefBlobData.Count - 1 do
    begin
      Recs := FRefBlobData[i];
      for j := Recs.Count - 1 downto 0 do
      begin
        val := Recs[j].Value;
        val[1] := False;
        Recs[j].Value := val;
        if Recs[j].Name[1] = '-' then
          if MapList.TryDecodeValue(Recs[j].Name, k) then
          begin
            (Recs[j] as IisNamedObject).Name := IntToStr(k)
          end
          else
            Recs.Delete(j);
      end;
    end;
  end;
end;

function TevClientDataSet.IsEvBlobField(const AFieldName: String): Boolean;
var
  ValItem: IisListOfValues;
  Fld: TField;
begin
  Result := False;

  Fld := FindField(AFieldName);
  if not Assigned(Fld) then
    Exit;

  Result := Fld.IsBlob;
  if not Result and Assigned(FRefBlobData) then
  begin
    ValItem := FRefBlobData.ParamsByName(Fld.FieldName);
    Result := Assigned(ValItem);
  end;
end;

procedure TevClientDataSet.RefreshRecord(const ANbr: Integer);
var
  Q: IevQuery;
  Flds: String;
  i: Integer;
begin
  if (TableName = '') or (KeyFieldName = '') then
    Exit;

  CheckCondition(State = dsBrowse, 'Dataset is not in browse state');

  Flds := '';
  for i := 0 to Fields.Count - 1 do
    if Fields[i].FieldKind = fkData then
      AddStrValue(Flds, Fields[i].FieldName, ',');

  Q := TevQuery.CreateFmt('SELECT %s FROM %s WHERE %s = :nbr AND {AsOfDate<%s>}', [Flds, TableName, KeyFieldName, TableName]);
  Q.Params.AddValue('nbr', ANbr);
  Q.Params.AddValue('StatusDate', AsOfDate);
  Q.Execute;

  RefreshFieldsOfRecord(Q.Result.vclDataSet, ANbr);
end;

procedure TevClientDataSet.RefreshFieldValue(const AFields: TisCommaDilimitedString);
var
  Q: IevQuery;
  Fld: TField;
  L: IisStringList;
  i: Integer;
begin
  if (TableName = '') or (KeyFieldName = '') or (AFields = '') then
    Exit;

  CheckCondition(State = dsBrowse, 'Dataset is not in browse state');

  L := TisStringList.Create;
  L.CommaText := AFields;

  for i := L.Count - 1 downto 0 do
  begin
    Fld := FindField(L[i]);
    if Assigned(Fld) then
      CheckCondition(VarSameValue(Fld.OldValue, Fld.NewValue), 'Field value is modified')
    else
      L.Delete(i);
  end;

  Q := TevQuery.CreateFmt('SELECT %s FROM %s WHERE %s = :nbr AND {AsOfDate<%s>}', [L.CommaText, TableName, KeyFieldName, TableName]);
  Q.Params.AddValue('nbr', FieldByName(KeyFieldName).AsInteger);
  Q.Params.AddValue('StatusDate', AsOfDate);
  Q.Execute;

  RefreshFieldsOfRecord(Q.Result.vclDataSet, FieldByName(KeyFieldName).AsInteger);
end;

procedure TevClientDataSet.CheckDSCondition(const ACheckCondition: string; Unfilter: Boolean = True);
var
  s: string;
begin
  if AsOfDate <> ctx_DataAccess.AsOfDate then
    AsOfDate := 0;

  if UpperCase(ACheckCondition) = 'ALL' then
    s := ''
  else
    s := ACheckCondition;
  if UpperCase(StringReplace(OpenCondition, ' ', '', [rfReplaceAll])) <>
     UpperCase(StringReplace(s, ' ', '', [rfReplaceAll])) then
    Close;
  if not Active then
  begin
    RetrieveCondition := s;
    Filtered := False;
    UserFiltered := False;
  end
  else
  if Unfilter then
  begin
    Filtered := False;
    UserFiltered := False;
  end;
end;

procedure TevClientDataSet.CheckDSConditionAndClient(const AClientID: Integer; const ACheckCondition: string; Unfilter: Boolean = True);
begin
  if ClientID <> AClientID then
    ClientID := AClientID;
  CheckDSCondition(ACheckCondition, Unfilter);
end;

procedure TevClientDataSet.CheckChildDSAndClient(const AClientID: Integer; const UpLinkFieldName: string; const UpLinkDataSet: TevClientDataSet; const ExtraCheckCondition: string = '');
var
  s: string;
begin
  Assert(UpLinkDataSet is TddTable);
  s := UplinkRetrieveCondition(UpLinkFieldName, TddTable(UpLinkDataSet), ExtraCheckCondition);
  CheckDSConditionAndClient(AClientID, s);
end;

procedure TevClientDataSet.DoBeforePost;
var
  Digits, i: Integer;

  RoundVal: Double;

  function ThresholdOf(const ADigits: Integer): Double;
  const
    ArThreshold: array [0..9] of Double = (0.9, 0.09, 0.009, 0.0009, 0.00009, 0.000009, 0.0000009, 0.00000009, 0.000000009, 0.0000000009);
  begin
    if ADigits > High(ArThreshold) then
      Result := 9 * Power(0.1, ADigits + 1)
    else
      Result := ArThreshold[ADigits];
  end;


begin
  inherited;
  if not SkipFieldCheck then
  begin
    if Assigned(FBeforeValidateRecord) then
      FBeforeValidateRecord(Self);

    for i := 0 to Fields.Count - 1 do
    begin
      if not ReadOnly and (Fields[i] is TFloatField) and not Fields[i].IsNull then
      begin
        Digits := Pos('.', TFloatField(Fields[i]).DisplayFormat);
        if Digits > 0 then
        begin
          Digits := Length(TFloatField(Fields[i]).DisplayFormat) - Digits;
          RoundVal := RoundAll(Fields[i].Value, Digits);
          if Abs(Fields[i].Value - RoundVal) > ThresholdOf(Digits)  then
            Fields[i].Value := RoundVal;
        end;
      end;
    end;
    ValidateRecord;
  end;
end;

procedure TevClientDataSet.ValidateRecord;
var
  i: Integer;
begin
  for i := 0 to Fields.Count - 1 do
  begin
    if Fields[i].Required
    and not Fields[i].ReadOnly
    and (Fields[i].FieldKind = fkData)
    and (Fields[i].IsNull or (Fields[i].DataType = ftString) and (Fields[i].AsString = '')) then
    begin
      raise EInvalidRecord.Create('Field '+Fields[i].DisplayName+' must have a value.', Name, Fields[i].FieldName, True);
    end;
  end;
end;

procedure TevClientDataSet.RefreshFieldsOfRecord(const AData: TDataSet; const ANbr: Integer);
var
  Fld: TField;
  bOldLogChanges: Boolean;
  bOldDataModified: Boolean;
  bOldFiltered, bOldUserFiltered: Boolean;
  i, OldNbr: Integer;
  bIns: Boolean;
  BeforeInsertEvt, OnNewRecordEvt, BeforeEditEvt,
  BeforePostEvt, AfterInsertEvt, AfterEditEvt, AfterPostEvt: TDataSetNotifyEvent;
begin
  BeforeInsertEvt := BeforeInsert;
  OnNewRecordEvt := OnNewRecord;
  BeforeEditEvt := BeforeEdit;
  BeforePostEvt := BeforePost;
  AfterInsertEvt := AfterInsert;  
  AfterEditEvt := AfterEdit;
  AfterPostEvt := AfterPost;
  try
    BeforeInsert := nil;
    OnNewRecord := nil;
    BeforeEdit := nil;
    BeforePost := nil;
    AfterInsert := nil;
    AfterEdit := nil;
    AfterPost := nil;

    OldNbr := FieldByName(KeyFieldName).AsInteger;
    bOldLogChanges := LogChanges;
    bOldDataModified := Common.IsDataModified;
    bOldFiltered := Filtered;
    bOldUserFiltered := UserFiltered;
    DisableControls;
    try
      LogChanges := False;
      Filtered := False;
      UserFiltered := False;

      if FieldByName(KeyFieldName).AsInteger <> ANbr then
        bIns := not Locate(KeyFieldName, ANbr, [])
      else
        bIns := False;

      FRefreshingData := True;
      if bIns then
      begin
        if AData.RecordCount > 0 then
          Append
      end
      else
      begin
        if AData.RecordCount > 0 then
          Edit
        else
          Delete;
      end;

      if AData.RecordCount > 0 then
      begin
        for i := 0 to AData.Fields.Count - 1 do
        begin
          Fld := FindField(AData.Fields[i].FieldName);
          if Assigned(Fld) then
            Fld.Value := AData.Fields[i].Value;
        end;
        Post;
      end;
    finally
      FRefreshingData := False;
      Filtered := bOldFiltered;
      UserFiltered := bOldUserFiltered;
      if (OldNbr <> 0) and (FieldByName(KeyFieldName).AsInteger <> OldNbr) then
        Locate(KeyFieldName, OldNbr, []);

      Common.IsDataModified := bOldDataModified;
      LogChanges := bOldLogChanges;
      EnableControls;
    end;
  finally
    BeforeInsert := BeforeInsertEvt;
    OnNewRecord := OnNewRecordEvt;  
    BeforeEdit := BeforeEditEvt;
    BeforePost := BeforePostEvt;
    AfterInsert := AfterInsertEvt;    
    AfterEdit := AfterEditEvt;
    AfterPost := AfterPostEvt;
  end;
end;

procedure TevClientDataSet.DataEvent(Event: TDataEvent; Info: Integer);
begin
  if not FRefreshingData then
    inherited;
end;

{ TevShadowClientDataSet }

function TevShadowClientDataSet.CachedFindKey(
  const KeyValue: Integer): Boolean;
begin
  Result := KeyValue = FLastKeyValue;
  if not Result then
  begin
    Result := FindKey([KeyValue]);
    if Result then
      FLastKeyValue := KeyValue;
  end;
end;

function TevShadowClientDataSet.CachedLookup(const KeyValue: Integer;
  const LookupFieldName: string): string;
begin
  if CachedFindKey(KeyValue) then
    Result := FieldByName(LookupFieldName).AsString
  else
    Result := '';
end;

constructor TevShadowClientDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FLastKeyValue := -1;
end;


{ TEvQueryBuilderQuery }

constructor TEvQueryBuilderQuery.Create(Owner: TComponent);
begin
  inherited;
  FQueryBuilderInfo := TEvQueryBuilderInfo.Create;
end;

destructor TEvQueryBuilderQuery.Destroy;
begin
  inherited;
  FreeAndNil(FQueryBuilderInfo);
end;

procedure TEvQueryBuilderQuery.DoRunQuery;
begin
  if Active then
    Close;
    
  ctx_RWLocalEngine.RunQBQuery(FQueryBuilderInfo.Data, ctx_DataAccess.ClientID, FCoNbr, Params, Self);
end;

procedure TEvQueryBuilderQuery.OpenQuery;
begin
  DoRunQuery;
end;

procedure TEvQueryBuilderQuery.SetQueryBuilderInfo(const AValue: TEvQueryBuilderInfo);
begin
  FQueryBuilderInfo.Assign(AValue);
end;


{ TEvQueryBuilderInfo }

procedure TEvQueryBuilderInfo.AssignTo(Dest: TPersistent);
begin
  (Dest as TEvQueryBuilderInfo).Data.Size := 0;
  (Dest as TEvQueryBuilderInfo).Data.RealStream.CopyFrom(Data.RealStream, 0);
end;

constructor TEvQueryBuilderInfo.Create;
begin
  FData := TEvDualStreamHolder.Create;
end;

procedure TEvQueryBuilderInfo.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('Data', ReadData, WriteData, FData.Size > 0);
end;


procedure TEvQueryBuilderInfo.ReadData(AStream: TStream);
begin
  FData.RealStream.Size := 0;
  FData.RealStream.CopyFrom(AStream, 0);
end;

procedure TEvQueryBuilderInfo.WriteData(AStream: TStream);
begin
  AStream.CopyFrom(FData.RealStream, 0);
end;

{ TevDataSetHolder }

constructor TevDataSetHolder.Create(const ADataSet: TevClientDataSet);
begin
  inherited Create;

  if ADataSet = nil then
    FDataSet := TevClientDataSet.Create(nil)  
  else
    FDataSet := ADataSet;
end;

constructor TevDataSetHolder.CreateCloneAndRetrieveData(
  const ASourceDataSet: TevClientDataSet; const ACondition: string);
begin
  Create(nil);
  Dataset.ClientID := ASourceDataSet.ClientID;
  DataSet.ProviderName := ASourceDataSet.ProviderName;
  DataSet.DataRequired(ACondition);
end;

function TevDataSetHolder.DataSet: TevClientDataSet;
begin
  Result := FDataSet;
end;

destructor TevDataSetHolder.Destroy;
begin
  if FDataSet.Owner = nil then
    FreeAndNil(FDataSet);
  inherited;
end;


{ TTransactionManager }

procedure TTransactionManager.ApplyUpdates(const AResolveCircularReferences: Boolean = False);
begin
  if IsModified then
    ctx_DataAccess.PostDataSets(FDS, AResolveCircularReferences);
end;

procedure TTransactionManager.CancelUpdates;
begin
  if IsModified then
    ctx_DataAccess.CancelDataSets(FDS);
end;

constructor TTransactionManager.Create(AOwner: TComponent);
begin
  inherited;
  SetLength(FDS, 0);
end;

procedure TTransactionManager.Initialize(const DS: array of TevClientDataset);
begin
  FDS := MakeArrayDS(DS);
end;

function TTransactionManager.IsModified: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(FDS) to High(FDS) do
  begin
    Result := FDS[i].ChangeCount > 0;
    if Result then
      Break;
  end;
end;

initialization
  RegisterClasses([
      TStringField,
      TDateTimeField,
      TFloatField,
      TAutoIncField,
      TBytesField,
      TCurrencyField,
      TBooleanField,
      TIntegerField,
      TNumericField,
      TWordField,
      TSmallIntField
      ] );

finalization
  UnRegisterClasses([
      TStringField,
      TDateTimeField,
      TFloatField,
      TAutoIncField,
      TBytesField,
      TCurrencyField,
      TBooleanField,
      TIntegerField,
      TNumericField,
      TWordField,
      TSmallIntField
      ] );

end.
