// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDFields;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, CheckLst,  EvUtils, ISBasicClasses, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDFields = class(TForm)
    clbFields: TevCheckListBox;
    evBitBtn2: TevBitBtn;
    evBitBtn1: TevBitBtn;
    procedure clbFieldsClickCheck(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TEDFields.clbFieldsClickCheck(Sender: TObject);
var
  b: Boolean;
  i: Integer;
begin
  b := False;
  for i := 0 to Pred(clbFields.Items.Count) do
    if clbFields.Checked[i] then
    begin
      b := True;
      Break;
    end;
  evBitBtn2.Enabled := b;
end;

end.
