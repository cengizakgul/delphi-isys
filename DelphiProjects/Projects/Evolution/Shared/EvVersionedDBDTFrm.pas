//copyright 2013 iSystems, LLC
unit EvVersionedDBDTFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, SPD_DBDTSelectionListFiltr;

type
  TevVersionedDBDT = class(TevVersionedFieldBase)
    btnSelectDBTD: TevSpeedButton;
    procedure btnSelectDBTDClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    DBDTSelectionList: TDBDTSelectionListFiltr;
    DBDTLevel: Char;    
  protected
    function  GetFieldSettings: IisListOfValues; override;
    procedure DoAfterDataBuild; override;
  end;

implementation


{$R *.dfm}


{ TevVersionedDBDT }

procedure TevVersionedDBDT.btnSelectDBTDClick(Sender: TObject);
begin
  if DBDTSelectionList.ShowSelection(Data[Fields.ParamName(0)], Data[Fields.ParamName(1)], Data[Fields.ParamName(2)], Data[Fields.ParamName(3)]) then
  begin
    if not (Data.State in [dsEdit, dsInsert]) then
      Data.Edit;

    Data[Fields.ParamName(0)] := DBDTSelectionList.wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
    Data[Fields.ParamName(1)] := DBDTSelectionList.wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
    Data[Fields.ParamName(2)] := DBDTSelectionList.wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
    Data[Fields.ParamName(3)] := DBDTSelectionList.wwcsTempDBDT.FieldByName('TEAM_NBR').Value;

    Data['Division_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Division_Name').Value;
    Data['Branch_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Branch_Name').Value;
    Data['Department_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Department_Name').Value;
    Data['Team_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Team_Name').Value;
    Data['Custom_DBDT_Number'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('DBDT_CUSTOM_NUMBERS').Value;
  end;
end;

function TevVersionedDBDT.GetFieldSettings: IisListOfValues;
begin
  Result := TisListOfValues.Create;
  Result.AddValue(Fields.ParamName(0) + '\ReferenceAsText', False);
  Result.AddValue(Fields.ParamName(1) + '\ReferenceAsText', False);
  Result.AddValue(Fields.ParamName(2) + '\ReferenceAsText', False);
  Result.AddValue(Fields.ParamName(3) + '\ReferenceAsText', False);
end;

procedure TevVersionedDBDT.FormShow(Sender: TObject);
var
  Q: IevQuery;
  CO_DIVISION: IevDataSet;
  CO_BRANCH: IevDataSet;
  CO_DEPARTMENT: IevDataSet;
  CO_TEAM: IevDataSet;
  CoNbr: Integer;
begin
  if AnsiSameText(Table, 'EE') then
    Q := TevQuery.CreateFmt('SELECT t.co_nbr FROM %s t WHERE %s_nbr = :nbr AND {AsOfNow<t>}', [Table, Table])
  else
    Assert(False);

  Q.Params.AddValue('nbr', Nbr);
  CoNbr := Q.Result.Fields[0].AsInteger;

  Q := TevQuery.Create('SELECT t.dbdt_level FROM co t WHERE co_nbr = :co_nbr AND {AsOfNow<t>}');
  Q.Params.AddValue('co_nbr', CoNbr);
  Q.Execute;
  Assert(Q.Result.RecordCount = 1);
  DBDTLevel := Q.Result.Fields[0].AsString[1];

  Q := TevQuery.Create('SELECT t.* FROM co_division t WHERE co_nbr = :co_nbr AND {AsOfNow<t>}');
  Q.Params.AddValue('co_nbr', CoNbr);
  CO_DIVISION := Q.Result;

  Q := TevQuery.Create('SELECT t.* FROM co_branch t WHERE {AsOfNow<t>}');
  CO_BRANCH := Q.Result;

  Q := TevQuery.Create('SELECT t.* FROM co_department t WHERE {AsOfNow<t>}');
  CO_DEPARTMENT := Q.Result;

  Q := TevQuery.Create('SELECT t.* FROM co_team t WHERE {AsOfNow<t>}');
  CO_TEAM := Q.Result;


  DBDTSelectionList := TDBDTSelectionListFiltr.Create(Self);
  DBDTSelectionList.Setup(
    CO_DIVISION.vclDataSet as TevBasicClientDataSet, CO_BRANCH.vclDataSet as TevBasicClientDataSet,
    CO_DEPARTMENT.vclDataSet as TevBasicClientDataSet, CO_TEAM.vclDataSet as TevBasicClientDataSet,
    Null, Null, Null, Null, DBDTLevel);

  inherited;
end;

procedure TevVersionedDBDT.DoAfterDataBuild;
Type
  TLocateArray= array of Variant;

var
  DSCont: IisStream;
  LocateFields, sDBDT_CUSTOM_NUMBERS, sSplitter: string;
  LocateArray: Variant;
  ShowCustom:boolean;

  function LocateVarArrayOf:TLocateArray;
  begin
    SetLength(result, 1);
    result[0] := Data[Fields.ParamName(0)];
    LocateFields:= 'DIVISION_NBR';
    ShowCustom:= True;
    if StrToInt(DBDTLevel) >= 2 then
    begin
     if not VarIsNull(Data[Fields.ParamName(1)]) then
     begin
      SetLength(result, 2);
      result[1] := Data[Fields.ParamName(1)];
      LocateFields:= LocateFields + ';BRANCH_NBR';
     end
     else
     begin
       ShowCustom:=false;
       exit;
     end;
    end;
    if StrToInt(DBDTLevel) >= 3 then
    begin
     if not VarIsNull(Data[Fields.ParamName(2)]) then
     begin
      SetLength(result, 3);
      result[2] := Data[Fields.ParamName(2)];
      LocateFields:= LocateFields + ';DEPARTMENT_NBR';
     end
     else
     begin
       ShowCustom:=false;
       exit;
     end;
    end;
    if StrToInt(DBDTLevel) >= 4 then
    begin
     if not VarIsNull(Data[Fields.ParamName(3)]) then
     begin
      SetLength(result, 4);
      result[3] := Data[Fields.ParamName(3)];
      LocateFields:= LocateFields + ';TEAM_NBR';
     end
     else
     begin
       ShowCustom:=false;
       exit;
     end;
    end;
  end;

begin
  inherited;

  DSCont := Data.Data;
  Data.vclDataSet.SetDefaultFields(False);
  Data.vclDataSet.Close;
  Data.vclDataSet.FieldDefs.Update;
  Data.vclDataSet.FieldDefs.Add('Division_Name', ftString, 40);
  Data.vclDataSet.FieldDefs.Add('Branch_Name', ftString, 40);
  Data.vclDataSet.FieldDefs.Add('Department_Name', ftString, 40);
  Data.vclDataSet.FieldDefs.Add('Team_Name', ftString, 40);
  Data.vclDataSet.FieldDefs.Add('Custom_DBDT_Number', ftString, 64);
  Data.Data := DSCont;

  Data.First;
  while not Data.Eof do
  begin
      LocateArray:= LocateVarArrayOf;
      if DBDTSelectionList.wwcsTempDBDT.Locate(LocateFields,LocateArray,[]) then
      begin
        sDBDT_CUSTOM_NUMBERS:= '';
        sSplitter:='';
        Data.Edit;
        if Contains(LocateFields, 'DIVISION_NBR') then
        begin
           Data['Division_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Division_Name').Value;
           sDBDT_CUSTOM_NUMBERS :=  Trim(DBDTSelectionList.wwcsTempDBDT.FieldByName('Division_Name').AsString);
           sSplitter:=' | ' ;
        end;
        if Contains(LocateFields, 'BRANCH_NBR') then
        begin
           Data['Branch_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Branch_Name').Value;
           sDBDT_CUSTOM_NUMBERS := sDBDT_CUSTOM_NUMBERS + sSplitter + Trim(DBDTSelectionList.wwcsTempDBDT.FieldByName('Branch_Name').AsString);
           sSplitter:=' | ' ;
        end;
        if Contains(LocateFields, 'DEPARTMENT_NBR') then
        begin
           Data['Department_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Department_Name').Value;
           sDBDT_CUSTOM_NUMBERS := sDBDT_CUSTOM_NUMBERS + sSplitter + Trim(DBDTSelectionList.wwcsTempDBDT.FieldByName('Department_Name').AsString);
           sSplitter:=' | ' ;
        end;
        if Contains(LocateFields, 'TEAM_NBR') then
        begin
           Data['Team_Name'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('Team_Name').Value;
           sDBDT_CUSTOM_NUMBERS := sDBDT_CUSTOM_NUMBERS + sSplitter + Trim(DBDTSelectionList.wwcsTempDBDT.FieldByName('Team_Name').AsString);
        end;
        if  ShowCustom then
           Data['Custom_DBDT_Number'] := DBDTSelectionList.wwcsTempDBDT.FieldByName('DBDT_CUSTOM_NUMBERS').Value
        else
           Data['Custom_DBDT_Number'] := sDBDT_CUSTOM_NUMBERS;
        Data.Post;
      end;
    Data.Next;
  end;

  grFieldData.Selected.Text := 'begin_date'#9'21'#9'Begin Effective Date'#9'F'#13 +
                               'end_date'#9'21'#9'End Effective Date'#9'F'#13 +
                               'Division_Name' + #9'15'#9'Division Name'#9'F'#13 +
                               'Branch_Name' + #9'15'#9'Branch Name'#9'F'#13 +
                               'Department_Name' + #9'15'#9'Department Name'#9'F'#13 +
                               'Team_Name' + #9'15'#9'Team Name'#9'F'#13 +
                               'Custom_DBDT_Number' + #9'25'#9'Custom D/B/D/T Numbers'#9'F';
end;

end.

