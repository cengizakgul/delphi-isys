// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AskGlobalUpdateParamsBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList,  StdCtrls, Buttons, ExtCtrls,
  ISBasicClasses, EvUIUtils, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TAskGlobalUpdateParamsBase = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    EvBevel1: TEvBevel;
    evPanel4: TevPanel;
    evBitBtn2: TevBitBtn;
    evBitBtn1: TevBitBtn;
    evActionList1: TevActionList;
    OK: TAction;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//procedure GrayOut(const control: TControl; const grayed: Boolean);
//procedure GrayOutControl( control: TControl; grayed: boolean );
function IsInteger( s: string ): boolean;
function IsFloat( s: string ): boolean;

implementation

uses
  typinfo, db;
{$R *.dfm}


{$HINTS OFF}
function IsInteger( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := code = 0;
end;

function IsFloat( s: string ): boolean;
var
  V: extended;
begin
  Result := TextToFloat(PChar(S), V, fvExtended);
end;
{$HINTS ON}

end.
