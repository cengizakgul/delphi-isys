inherited EDIT_GLOBAL_CO_BASE: TEDIT_GLOBAL_CO_BASE
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 270
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Company list'
      object Panel2: TevPanel
        Left = 0
        Top = 41
        Width = 435
        Height = 201
        Align = alClient
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 321
          Top = 1
          Height = 199
        end
        object wwdbgridSelectClient: TevDBGrid
          Left = 1
          Top = 1
          Width = 320
          Height = 199
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'27'#9'Number'#9'F'
            'NAME'#9'25'#9'Name'#9'F'
            'FEIN'#9'9'#9'Fein'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
          IniAttributes.SectionName = 'TEDIT_GLOBAL_CO_BASE\wwdbgridSelectClient'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsTempTable
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          OnDblClick = wwdbgridSelectClientDblClick
          PaintOptions.AlternatingRowColor = clCream
        end
        object pnlSubbrowse: TevPanel
          Left = 324
          Top = 1
          Width = 110
          Height = 199
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel4: TevPanel
            Left = 0
            Top = 0
            Width = 110
            Height = 199
            Align = alClient
            TabOrder = 0
            object wwDBGrid1: TevDBGrid
              Left = 1
              Top = 1
              Width = 108
              Height = 197
              Selected.Strings = (
                'CUSTOM_COMPANY_NUMBER'#9'10'#9'Company #'#9'F'
                'NAME'#9'40'#9'Company Name'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
              IniAttributes.SectionName = 'TEDIT_GLOBAL_CO_BASE\wwDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsSelectedCompanies
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = wwDBGrid1DblClick
              PaintOptions.AlternatingRowColor = clCream
            end
          end
        end
      end
      object Panel3: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object bbtnCopyAll: TevBitBtn
          Left = 8
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Copy All'
          TabOrder = 0
          OnClick = bbtnCopyAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD4444444
            4444DDDDD88888888888DDDDD4FFFFFFFFF4DDDDD8DDDDDDDDD8D8888477777F
            FFF4DFFFFFFFFFFDDDD800000000007FFFF48888888888FDDDD80FFFF7FFF07F
            FFF48DDDDDDDD8FDDDD80F444444F007FFF48D888888D88FDDD80FFFF7FFF0F0
            7FF48DDDDDDDD8D8FDD80F444444FF0F07F48D888888DD8D8FD80FFFF7FFFFF0
            7FF48DDDDDDDDDD8DDD80F444444FFFF07F48D888888DDDD8DD80FFFF7FFFFF0
            7FF48DDDDDDDDDD8DDD80F444444FF07FFF48D888888DD8DDDD80FFFF7FFF07F
            FFF48DDDDDDDD8FDDDD80F444444704444448D888888D8F888880FFFFFFFF08D
            DDDD8DDDDDDDD8FDDDDD0000000000DDDDDD8888888888DDDDDD}
          NumGlyphs = 2
        end
        object bbtnRemoveAll: TevBitBtn
          Left = 104
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Remove All'
          TabOrder = 1
          OnClick = bbtnRemoveAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD8888888
            8888DDDDDFFFFFFFFFFFDDDD000000000008DDDD88888888888FDDDD0FFFFFFF
            FF08DDDD8DDDDDDDDD8FDDDD0FFFFFFFFF08DDDD8DDDDDDDDD8F88DD0FF88FFF
            FF08FFDD8DDFFDDDDD8F998D0F899FFFFF0888FD8DF88DDDDD8F999808999FFF
            FF08888F8F888DDDDD8FD9998999FFFFFF08D888F888DDDDDD8FDD99999FFFFF
            FF08DD88888DDDDDDD8FDD89998FFFFFFF08DDF888FDDDDDDD8FD8999998FFFF
            FF08DF88888FDDDDDD8F899909998FFFFF08F888D888FDDDDD8F999D0F999FFF
            FF08888D8D888DDDDD8F99DD0FF99FFFFF0888DD8DD88DDDDD8FDDDD0FFFFFFF
            FF08DDDD8DDDDDDDDD8FDDDD00000000000DDDDD88888888888D}
          NumGlyphs = 2
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 453
    Top = 0
  end
  inherited wwdsDetail: TevDataSource
    Left = 486
    Top = 0
  end
  inherited wwdsList: TevDataSource
    Left = 418
    Top = 2
  end
  object SelectedCompanies: TevClientDataSet
    Left = 308
    Top = 65529
    object SelectedCompaniesCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object SelectedCompaniesCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object SelectedCompaniesCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Company Nbr'
      DisplayWidth = 10
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object SelectedCompaniesNAME: TStringField
      DisplayLabel = 'Company Name'
      FieldName = 'NAME'
      Size = 40
    end
  end
  object wwdsSelectedCompanies: TevDataSource
    DataSet = SelectedCompanies
    Left = 344
    Top = 65528
  end
  object wwdsTempTable: TevDataSource
    Left = 544
    Top = 8
  end
end
