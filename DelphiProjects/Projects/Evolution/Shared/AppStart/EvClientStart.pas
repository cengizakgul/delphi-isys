unit EvClientStart;

interface

uses SysUtils, Forms, EvMainboard, EvContext, isBasicUtils, EvUtils, EvTypes, isThreadManager, SSettingsDialog,
     evExceptions, EvCommonInterfaces,
     isBaseClasses, EvStreamUtils, SEncryptionRoutines, EvInitApp, IsZippingRoutines, isErrorUtils, EvConsts,
     EvTransportInterfaces, isVCLBugFix, evBasicUtils, isExceptions, isGUIApp, EvUIUtils;

  procedure RunEvClient(const AppTitle, AppID: String);
  function  InitGUIContext: Boolean;

implementation

uses SLoginDialog, SPD_Change_Password, EvSecurityQuestionsSetupFrm, EvSecurityQuestionsFrm,
  isSettings;

procedure GetSecuritySchema;
var
  s, f: IevDualStream;
begin
  InitGUIContext;
  ctx_StartWait('Rebuilding security schema');
  try
    s := ctx_Security.GetSecStructure.Rebuild;
    f := TEvDualStreamHolder.CreateFromNewFile(AppDir +  'EvSecuritySchema.dat');
    s.Position := 0;
    f.CopyFrom(s, S.Size);
  finally
    ctx_EndWait;
  end;
end;


procedure RunEvClient(const AppTitle, AppID: String);

  procedure _Init;
  var
    s, pw: String;
  begin
    InitializeEvoApp;

    if IsStandalone and AppSwitches.ValueExists('CONFIG') then
      ShowSettingsDialog  // Show configuration GUI

    else if IsStandalone and AppSwitches.ValueExists('GETSEC') then
      GetSecuritySchema

    else if IsDebugMode and AppSwitches.ValueExists('ADMINRESET') then
    begin
      Mainboard.ContextManager.CreateThreadContext(
        EncodeUserAtDomain(ctx_DomainInfo.SystemAccountInfo.UserName, ctx_DomainInfo.DomainName),
        ctx_DomainInfo.SystemAccountInfo.Password);

      ctx_Security.ResetPassword(ctx_DomainInfo.AdminAccount, nil, AppSwitches.Value['ADMINRESET']);
    end

    else if IsDebugMode and AppSwitches.ValueExists('CHSYSDBA') then
    begin
      Mainboard.ContextManager.CreateThreadContext(
        EncodeUserAtDomain(ctx_DomainInfo.SystemAccountInfo.UserName, ctx_DomainInfo.DomainName),
        ctx_DomainInfo.SystemAccountInfo.Password);

      s := AppSwitches.Value['CHSYSDBA'];
      pw := GetNextStrValue(s, #13);
      ctx_DBAccess.ChangeFBAdminPassword(pw, s);
      mb_GlobalSettings.DBAdminPassword := s;
      mb_GlobalSettings.Save;
    end

    else if IsDebugMode and AppSwitches.ValueExists('CHEUSER') then
    begin
      Mainboard.ContextManager.CreateThreadContext(
        EncodeUserAtDomain(ctx_DomainInfo.SystemAccountInfo.UserName, ctx_DomainInfo.DomainName),
        ctx_DomainInfo.SystemAccountInfo.Password);

      s := AppSwitches.Value['CHEUSER'];
      ctx_DBAccess.ChangeFBUserPassword(mb_GlobalSettings.DBAdminPassword, s);
      mb_GlobalSettings.DBUserPassword := s;
      mb_GlobalSettings.Save;
    end

    else
      // If authorized then show main application form, otherwise terminate process
      if InitGUIContext then
      begin
        ctx_StartWait('Please wait while Evolution is loading ...');
        try
          ctx_EvolutionGUI.CreateMainForm;
          Mainboard.ActivateModules;
        finally
          ctx_EndWait;
        end;
      end;
  end;

  procedure _Deinit;
  begin
    Mainboard.DeactivateModules;
    ctx_EvolutionGUI.DestroyMainForm;
    UninitializeEvoApp;
  end;

begin
  RunISGUI(AppTitle, AppID, @_Init, @_Deinit);
end;


function InitGUIContext: Boolean;
var
  sUser, sPassword, sHost, sPort: String;
  EncryptionType: TevEncryptionType;
  CompressionLevel: TisCompressionLevel;
  LoginDialogOper: TevLoginDialogOperation;
  bPwdChangeRequire: Boolean;
  bSecQuestionsSetRequires: Boolean;
  bExitForUpdate: Boolean;
  bAccountLocked: Boolean;

  function InitContext: Boolean;
  var
    bNeedToExit: Boolean;
    sHashedPassword: String;
    sU: String;
    D: IevDomainInfo;
    OldCtx: IevContext;
  begin
    sHashedPassword := HashPassword(sPassword);

    Result := False;
    bAccountLocked := False;
    OldCtx := Context;
    try
      if not IsStandalone then
      begin
        Mainboard.TCPClient.Logoff;

        Mainboard.TCPClient.Host := sHost;
        Mainboard.TCPClient.Port := sPort;
        Mainboard.TCPClient.EncryptionType := EncryptionType;
        Mainboard.TCPClient.CompressionLevel := CompressionLevel;
        Mainboard.TCPClient.ArtificialLatency := mb_AppSettings.GetValue('Settings\NetworkLatency', Integer(0));
      end;

      if not IsDebugMode then
        if Mainboard.VersionUpdateLocal <> nil then
        begin
          bNeedToExit := Mainboard.VersionUpdateLocal.PrepareUpdateIfNeeds(False);

          if not IsStandalone then
            Mainboard.TCPClient.Logoff;  // in order to get rid of Guest connection

          if bNeedToExit then
          begin
            bExitForUpdate := True;
            Exit; //exit for autoupdate
          end;
        end;

      sU := sUser;
      if not Contains(sUser, '@') then
        sU := EncodeUserAtDomain(sU, Context.UserAccount.Domain); // should be Guest context

      Mainboard.ContextManager.CreateThreadContext(sU, sHashedPassword);

      // Add dummy domain info it's absent
      if not IsStandalone then
      begin
        D := mb_GlobalSettings.DomainInfoList.GetDomainInfo(Context.UserAccount.Domain);
        if not Assigned(D) then
        begin
          D := mb_GlobalSettings.DomainInfoList.Add;
          D.DomainName := Context.UserAccount.Domain;
          D.DomainDescription := Context.UserAccount.Domain;
        end;
        ctx_DomainInfo.LicenseInfo.SerialNumber := Context.License.GetLicenseInfo.Value['SerialNumber'];
      end;

      // fix up system account if it's empty
      if ctx_DomainInfo.SystemAccountInfo.UserName = '' then
      begin
        if IsStandalone then
          raise EisException.Create('Evolution system account is not set up.');

        ctx_AccountRights;
        ctx_DomainInfo.SystemAccountInfo.UserName := Context.UserAccount.User;
      end;

      Result := True;
    except
      on E: Exception do
      begin
        Mainboard.ContextManager.SetThreadContext(OldCtx);

        if E is ELockedAccount then
          bAccountLocked := True;

        if E is EisException then
          EisException(E).StackInfo := '';

        EvExceptMessage(E);
      end;
    end;
  end;


  function SecurityQuestionsNotSet: Boolean;
  var
    Questions: IisStringList;
  begin
    Questions := ctx_Security.GetQuestions(Context.UserAccount.User);
    Result := Questions.Count = 0;
  end;


  function SetSercurityQuestions: Boolean;
  var
    Questions: IisStringList;
    QuestionAnswerList: IisListOfValues;
  begin
    Questions := ctx_Security.GetAvailableQuestions(Context.UserAccount.User);
    Result := SecurityQuestionSetup(Questions, QuestionAnswerList);
    if Result then
      ctx_Security.SetQuestions(QuestionAnswerList)
    else
      EvMessage('You will not be able to use Evolution until you setup security questions.');
  end;


  function ResetPassword: Boolean;
  var
    Questions: IisStringList;
    QuestionAnswerList: IisListOfValues;
    bAccountBlocked: Boolean;
    sU, sD, sPassword: String;
  begin
    Result := False;
    bAccountBlocked := False;

    if not IsStandalone then
    begin
      Mainboard.TCPClient.Logoff;

      Mainboard.TCPClient.Host := sHost;
      Mainboard.TCPClient.Port := sPort;
      Mainboard.TCPClient.EncryptionType := EncryptionType;
      Mainboard.TCPClient.CompressionLevel := CompressionLevel;
    end;

    Mainboard.ContextManager.StoreThreadContext;
    try
      try
        DecodeUserAtDomain(sUser, sU, sD);
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sD), '');

        Questions := ctx_Security.GetQuestions(sU);
        CheckCondition(Questions.Count >= 3, 'Security questions are not set up');

        while not bAccountBlocked and AskSecurityQuestions(Questions, QuestionAnswerList, sPassword) do
        begin
          try
            ctx_Security.ResetPassword(sU, QuestionAnswerList, sPassword);
            Result := True;
            break;
          except
            on E: Exception do
            begin
              if E is EisException then
                EisException(E).StackInfo := '';
              EvExceptMessage(E);                
              if E is EBlockedAccount then
                bAccountBlocked := True;
            end;
          end;
        end;
      except
        on E: Exception do
          EvExceptMessage(E);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

begin
  Result := False;

  if (Mainboard.VersionUpdateLocal <> nil) and Mainboard.VersionUpdateLocal.PerformUpdateIfNeeds then
    Exit; //restart after autoupdate

  if AppSwitches.ValueExists('USER') then
    sUser := AppSwitches.Value['USER']
  else
    sUser := mb_AppSettings['UserName'];

  if AppSwitches.ValueExists('PASSWORD') then
    sPassword := AppSwitches.Value['PASSWORD']
  else
    sPassword := '';

  sHost := '';
  sPort := '';
  EncryptionType := etNone;
  CompressionLevel := clNone;
  if not IsStandalone then
  begin
    if AppSwitches.ValueExists('HOST') then
      sHost := AppSwitches.Value['HOST']
    else if AppSwitches.ValueExists('HOSTNAME') then
      sHost := AppSwitches.Value['HOSTNAME']
    else
      sHost := mb_AppSettings['Hosts'];

    if AppSwitches.ValueExists('PORT') then
      sPort := AppSwitches.Value['PORT']
    else
    begin
      sPort := mb_AppSettings.GetValue('DefaultPort', IntToStr(TCPClient_PortSSL1));
      sPort := IntToStr(StrToIntDef(sPort, TCPClient_PortSSL1));
    end;

    if AppSwitches.ValueExists('ENCRYPTION') then
      EncryptionType := AppSwitches.Value['ENCRYPTION']
    else
      EncryptionType := TevEncryptionType(mb_AppSettings.AsInteger['EncryptionType']);

    if AppSwitches.ValueExists('COMPRESSION') then
      CompressionLevel := AppSwitches.Value['COMPRESSION']
    else
      CompressionLevel := TisCompressionLevel(mb_AppSettings.AsInteger['CompressionLevel']);
  end;

  if IsStandalone and (sUser <> '') and (sPassword <> '')  or
     not IsStandalone and (sUser <> '') and (sPassword <> '') and (sHost <> '') then
    Result := InitContext;

  LoginDialogOper := loLogin;
  bPwdChangeRequire := False;

  // Login loop
  bExitForUpdate := False;
  while not bExitForUpdate and not Result and
        ShowLoginDialog(sUser, sPassword, sHost, sPort, EncryptionType, CompressionLevel, LoginDialogOper) do
  begin
    if LoginDialogOper in [loLogin, loLoginWithPasswordChange] then
    begin
      bPwdChangeRequire := LoginDialogOper = loLoginWithPasswordChange;
      Result := InitContext;
      if bAccountLocked then
        if ResetPassword then
        begin
          sPassword := '';
          LoginDialogOper := loLogin;
        end
        else
          break
    end
    else if LoginDialogOper = loForgotPassword then
    begin
      if ResetPassword then
      begin
        sPassword := '';
        LoginDialogOper := loLogin;
      end;  
    end;
  end;

  if Result then
  begin
    // check security questions
    bSecQuestionsSetRequires := SecurityQuestionsNotSet;
    if bSecQuestionsSetRequires then
    begin
      Result := SetSercurityQuestions;
      bPwdChangeRequire := False;
    end;

    if Result then
    begin
      Result := CheckPasswordChange(bPwdChangeRequire); // check password expiration
      if bPwdChangeRequire or Result then
      begin
        Result := True;

        // store these settings if login was successful
        mb_AppSettings['UserName'] := sUser;
        if not IsStandalone then
        begin
          mb_AppSettings['Hosts'] := sHost;
          mb_AppSettings['DefaultPort'] := sPort;
          mb_AppSettings.AsInteger['CompressionLevel'] := Ord(CompressionLevel);
          mb_AppSettings.AsInteger['EncryptionType'] := Ord(EncryptionType);
        end;
      end;
    end;
  end;
end;


end.
