unit SSettingsDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses,  ComCtrls, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, EvMainboard, EvCommonInterfaces, ISBasicUtils,
  DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, OgUtil,
  EvDataAccessComponents, Wwdatsrc, EvSettings, Mask, wwdbedit, EvTypes,
  EvUIComponents, EvClientDataSet, isUIwwDBEdit, LMDCustomButton,
  LMDButton, isUILMDButton, isUIEdit;

type
  TEvSettingsDialog = class(TForm)
    ISPageControl1: TISPageControl;
    btnOk: TevBitBtn;
    btnCancel: TevBitBtn;
    DBLocationsTabSheet: TTabSheet;
    TempFolderLbl: TevLabel;
    SystemDBpathLbl: TevLabel;
    BureauDBpathLbl: TevLabel;
    TempDBpathLbl: TevLabel;
    DefaultDBPathEdit: TevEdit;
    SystemDBpathEdit: TevEdit;
    BureauDBpathEdit: TevEdit;
    TempDBpathEdit: TevEdit;
    ClientDBSrc: TevDataSource;
    ClientDBGrid: TevDBGrid;
    Label1: TLabel;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    edLicSerialNumber: TevEdit;
    Label3: TLabel;
    Label4: TLabel;
    edMachineID: TevEdit;
    chbDBChangeBroadcast: TevCheckBox;
    chbAPI: TevCheckBox;
    Label5: TLabel;
    edEnvID: TevEdit;
    Label6: TLabel;
    edTmpFolder: TevEdit;
    Label7: TLabel;
    edTaskQueueFolder: TevEdit;
    Label8: TLabel;
    edVMRFolder: TevEdit;
    chbTaskQueue: TevCheckBox;
    chbTaskScheduler: TevCheckBox;
    memLicKey: TevMemo;
    Label9: TLabel;
    edSUname: TevEdit;
    Label10: TLabel;
    edSysdbaPassword: TevEdit;
    ClientDbTmp: TevClientDataSet;
    Label11: TLabel;
    edEuserPassword: TevEdit;
    Label12: TLabel;
    edAdminUserName: TevEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FDomainInfo: IevDomainInfo;
    procedure LoadSettings;
    procedure SaveSettings;
  end;

function ShowSettingsDialog: Boolean;

implementation

{$R *.dfm}

function ShowSettingsDialog: Boolean;
var
  D: TEvSettingsDialog;
begin
  D := TEvSettingsDialog.Create(nil);
  try
    Result := D.ShowModal = mrOK;
  finally
    D.Free;
  end;
end;

procedure TEvSettingsDialog.LoadSettings;

  function GetDBLocation(const ADBType: TevDBType): String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 0 to FDomainInfo.DBLocationList.Count - 1 do
    begin
      if FDomainInfo.DBLocationList.GetItem(i).DBType = ADBType then
      begin
        Result := FDomainInfo.DBLocationList.GetItem(i).GetPath;
        break;
      end;
    end;
  end;

var
  i: Integer;
  N: Cardinal;
  s: String;
begin
  FDomainInfo := mb_GlobalSettings.DomainInfoList.Items[0];

  DefaultDBpAthEdit.Text := GetDBLocation(dbtUnspecified);
  SystemDBpathEdit.Text := GetDBLocation(dbtSystem);
  BureauDBpathEdit.Text := GetDBLocation(dbtBureau);
  TempDBpathEdit.Text := GetDBLocation(dbtTemporary);

  ClientDbTmp.FieldDefs.Add('PATH', ftString, 200);
  ClientDbTmp.FieldDefs.Add('START', ftInteger);
  ClientDbTmp.FieldDefs.Add('END', ftInteger);
  ClientDbTmp.CreateDataset;
  ClientDbTmp.Open;

  ClientDbTmp.DisableControls;
  try
    for i := 0 to FDomainInfo.DBLocationList.Count - 1 do
    begin
      if FDomainInfo.DBLocationList.GetItem(i).DBType = dbtClient then
      begin
        ClientDbTmp.Append;
        ClientDbTmp.FieldByName('PATH').AsString := FDomainInfo.DBLocationList.GetItem(i).Path;
        ClientDbTmp.FieldByName('START').AsInteger := FDomainInfo.DBLocationList.GetItem(i).BeginRangeNbr;
        ClientDbTmp.FieldByName('END').AsInteger := FDomainInfo.DBLocationList.GetItem(i).EndRangeNbr;
        ClientDbTmp.Post;
      end;
    end;
  finally
    ClientDbTmp.EnableControls;
  end;

  ClientDBGrid.AddField('PATH');
  ClientDBGrid.AddField('START');
  ClientDBGrid.AddField('END');

  edSysdbaPassword.Text := mb_GlobalSettings.DBAdminPassword;
  edEuserPassword.Text := mb_GlobalSettings.DBUserPassword;

  edLicSerialNumber.Text := FDomainInfo.LicenseInfo.SerialNumber;
  memLicKey.Text := FDomainInfo.LicenseInfo.Code;

  N := Mainboard.MachineInfo.ID;
  edMachineID.Text := BufferToHex(N, SizeOf(N));

  edEnvID.Text := mb_GlobalSettings.EnvironmentID;
  chbDBChangeBroadcast.Checked := FDomainInfo.DBChangesBroadcast;
  chbAPI.Checked := mb_AppConfiguration.AsBoolean['General\API'];
  chbTaskQueue.Checked := mb_AppConfiguration.AsBoolean['General\TaskQueue'];
  chbTaskScheduler.Checked := mb_AppConfiguration.AsBoolean['General\TaskScheduler'];

  s := mb_AppConfiguration.AsString['General\TempFolder'];
  if s = '' then
  begin
    s := AppTempFolder;
    s := DenormalizePath(s);
    GetPrevStrValue(s, '\');
  end;

  edTmpFolder.Text := s;
  edTaskQueueFolder.Text := FDomainInfo.FileFolders.TaskQueueFolder;
  edVMRFolder.Text := FDomainInfo.FileFolders.VMRFolder;

  edSUname.Text := FDomainInfo.SystemAccountInfo.UserName;
  edAdminUserName.Text := FDomainInfo.AdminAccount;
end;

procedure TEvSettingsDialog.SaveSettings;
var
  DBLocation: IevDBLocation;
begin
  FDomainInfo.DBLocationList.Clear;

  if DefaultDBPathEdit.Text <> '' then
  begin
    DBLocation := FDomainInfo.DBLocationList.Add;
    DBLocation.SetPath(DefaultDBPathEdit.Text);
    DBLocation.DBType := dbtUnspecified;
  end;

  if SystemDBPathEdit.Text <> '' then
  begin
    DBLocation := FDomainInfo.DBLocationList.Add;
    DBLocation.SetPath(SystemDBPathEdit.Text);
    DBLocation.DBType := dbtSystem;
  end;

  if BureauDBPathEdit.Text <> '' then
  begin
    DBLocation := FDomainInfo.DBLocationList.Add;
    DBLocation.SetPath(BureauDBPathEdit.Text);
    DBLocation.DBType := dbtBureau;
  end;

  if TempDBPathEdit.Text <> '' then
  begin
    DBLocation := FDomainInfo.DBLocationList.Add;
    DBLocation.SetPath(TempDBPathEdit.Text);
    DBLocation.DBType := dbtTemporary;
  end;

  ClientDbTmp.DisableControls;
  try
    ClientDbTmp.First;
    while not ClientDbTmp.EOF do
    begin
      DBLocation := FDomainInfo.DBLocationList.Add;
      DBLocation.DBType := dbtClient;
      DBLocation.Path := ClientDbTmp.FieldByName('PATH').AsString;
      DBLocation.BeginRangeNbr := ClientDbTmp.FieldByName('START').AsInteger;
      DBLocation.EndRangeNbr := ClientDbTmp.FieldByName('END').AsInteger;

      ClientDbTmp.Next;
    end;
  finally
    ClientDbTmp.EnableControls;
  end;

  mb_GlobalSettings.DBAdminPassword := edSysdbaPassword.Text;
  mb_GlobalSettings.DBUserPassword := edEuserPassword.Text;

  FDomainInfo.LicenseInfo.SerialNumber := edLicSerialNumber.Text;
  FDomainInfo.LicenseInfo.Code := memLicKey.Text;
  mb_GlobalSettings.EnvironmentID := edEnvID.Text;
  FDomainInfo.DBChangesBroadcast := chbDBChangeBroadcast.Checked;
  mb_AppConfiguration.AsBoolean['General\API'] := chbAPI.Checked;
  mb_AppConfiguration.AsBoolean['General\TaskQueue'] := chbTaskQueue.Checked;
  mb_AppConfiguration.AsBoolean['General\TaskScheduler'] := chbTaskScheduler.Checked;

  if edTmpFolder.Text <> AppTempFolder then
    mb_AppConfiguration.AsString['General\TempFolder'] := edTmpFolder.Text;
  FDomainInfo.FileFolders.TaskQueueFolder := edTaskQueueFolder.Text;
  FDomainInfo.FileFolders.VMRFolder := edVMRFolder.Text;

  FDomainInfo.SystemAccountInfo.UserName := edSUname.Text;
  FDomainInfo.AdminAccount := edAdminUserName.Text;  

  mb_GlobalSettings.Save;
end;

procedure TEvSettingsDialog.FormShow(Sender: TObject);
begin
  LoadSettings;

  ClientDBGrid.Columns[0].DisplayWidth := 60;
  ClientDBGrid.Columns[1].DisplayWidth := 10;
  ClientDBGrid.Columns[2].DisplayWidth := 10;
end;

procedure TEvSettingsDialog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOk then
    SaveSettings;
end;

end.
