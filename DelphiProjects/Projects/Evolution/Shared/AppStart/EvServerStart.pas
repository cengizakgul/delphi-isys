unit EvServerStart;

interface

uses EvInitApp, EvMainboard, isServerServiceFrm, isBasicUtils;

  procedure RunEvoServer(const AAppTitle, AAppID: String);


implementation

procedure RunEvoServer(const AAppTitle, AAppID: String);

  procedure _Init;
  begin
    InitializeEvoApp;
    Mainboard.ActivateModules;
  end;

  procedure _Deinit;
  begin
    Mainboard.DeactivateModules;
    UninitializeEvoApp;
  end;

begin
  RunISServer(AAppTitle, AAppTitle, AAppID, @_Init, @_Deinit, nil);
end;


end.
