object EvSettingsDialog: TEvSettingsDialog
  Left = 642
  Top = 250
  BorderStyle = bsDialog
  Caption = 'Evolution Settings'
  ClientHeight = 442
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    686
    442)
  PixelsPerInch = 96
  TextHeight = 13
  object ISPageControl1: TISPageControl
    Left = 0
    Top = 0
    Width = 686
    Height = 392
    ActivePage = DBLocationsTabSheet
    Align = alTop
    TabOrder = 0
    object DBLocationsTabSheet: TTabSheet
      Caption = 'Database Access'
      object TempFolderLbl: TevLabel
        Left = 16
        Top = 8
        Width = 76
        Height = 13
        Caption = 'Default DB path'
        FocusControl = DefaultDBPathEdit
      end
      object SystemDBpathLbl: TevLabel
        Left = 336
        Top = 8
        Width = 117
        Height = 13
        Caption = 'System DB path override'
        FocusControl = SystemDBpathEdit
      end
      object BureauDBpathLbl: TevLabel
        Left = 16
        Top = 56
        Width = 156
        Height = 13
        Caption = 'Service Bureau DB path override'
        FocusControl = BureauDBpathEdit
      end
      object TempDBpathLbl: TevLabel
        Left = 336
        Top = 56
        Width = 133
        Height = 13
        Caption = 'Temporary DB path override'
        FocusControl = TempDBpathEdit
      end
      object Label1: TLabel
        Left = 16
        Top = 104
        Width = 78
        Height = 13
        Caption = 'Client databases'
      end
      object Label10: TLabel
        Left = 16
        Top = 334
        Width = 92
        Height = 13
        Caption = 'SYSDBA Password'
      end
      object Label11: TLabel
        Left = 304
        Top = 334
        Width = 86
        Height = 13
        Caption = 'EUSER Password'
      end
      object DefaultDBPathEdit: TevEdit
        Left = 16
        Top = 24
        Width = 305
        Height = 21
        TabOrder = 0
      end
      object SystemDBpathEdit: TevEdit
        Left = 336
        Top = 24
        Width = 321
        Height = 21
        TabOrder = 1
      end
      object BureauDBpathEdit: TevEdit
        Left = 16
        Top = 72
        Width = 305
        Height = 21
        TabOrder = 2
      end
      object TempDBpathEdit: TevEdit
        Left = 336
        Top = 72
        Width = 321
        Height = 21
        TabOrder = 3
      end
      object ClientDBGrid: TevDBGrid
        Left = 16
        Top = 120
        Width = 642
        Height = 197
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEvSettingsDialog\ClientDBGrid'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = ClientDBSrc
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = False
        TabOrder = 4
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object edSysdbaPassword: TevEdit
        Left = 120
        Top = 331
        Width = 145
        Height = 21
        PasswordChar = '*'
        TabOrder = 5
      end
      object edEuserPassword: TevEdit
        Left = 408
        Top = 331
        Width = 145
        Height = 21
        PasswordChar = '*'
        TabOrder = 6
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Misc'
      ImageIndex = 2
      object Label5: TLabel
        Left = 408
        Top = 288
        Width = 73
        Height = 13
        Caption = 'Environment ID'
      end
      object Label6: TLabel
        Left = 16
        Top = 215
        Width = 59
        Height = 13
        Caption = 'Temp Folder'
      end
      object Label7: TLabel
        Left = 16
        Top = 252
        Width = 59
        Height = 13
        Caption = 'Task Queue'
      end
      object Label8: TLabel
        Left = 16
        Top = 288
        Width = 56
        Height = 13
        Caption = 'VMR Folder'
      end
      object Label9: TLabel
        Left = 408
        Top = 215
        Width = 77
        Height = 13
        Caption = 'System Account'
      end
      object Label12: TLabel
        Left = 408
        Top = 252
        Width = 72
        Height = 13
        Caption = 'Admin Account'
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 16
        Width = 647
        Height = 161
        Caption = 'Licence Information'
        TabOrder = 0
        object Label2: TLabel
          Left = 16
          Top = 29
          Width = 66
          Height = 13
          Caption = 'Serial Number'
        end
        object Label3: TLabel
          Left = 16
          Top = 53
          Width = 18
          Height = 13
          Caption = 'Key'
        end
        object Label4: TLabel
          Left = 391
          Top = 29
          Width = 55
          Height = 13
          Caption = 'Machine ID'
        end
        object edLicSerialNumber: TevEdit
          Left = 102
          Top = 26
          Width = 225
          Height = 21
          TabOrder = 0
        end
        object edMachineID: TevEdit
          Left = 454
          Top = 26
          Width = 176
          Height = 21
          Color = clInactiveBorder
          ReadOnly = True
          TabOrder = 1
        end
        object memLicKey: TevMemo
          Left = 16
          Top = 72
          Width = 615
          Height = 77
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 2
          WantReturns = False
        end
      end
      object chbDBChangeBroadcast: TevCheckBox
        Left = 499
        Top = 328
        Width = 165
        Height = 17
        Caption = 'ADR DB Change Notification'
        TabOrder = 7
      end
      object chbAPI: TevCheckBox
        Left = 280
        Top = 328
        Width = 97
        Height = 17
        Caption = 'Evolution API'
        TabOrder = 10
      end
      object edEnvID: TevEdit
        Left = 499
        Top = 285
        Width = 150
        Height = 21
        TabOrder = 6
      end
      object edTmpFolder: TevEdit
        Left = 94
        Top = 212
        Width = 277
        Height = 21
        TabOrder = 1
      end
      object edTaskQueueFolder: TevEdit
        Left = 94
        Top = 249
        Width = 277
        Height = 21
        TabOrder = 2
      end
      object edVMRFolder: TevEdit
        Left = 94
        Top = 285
        Width = 277
        Height = 21
        TabOrder = 3
      end
      object chbTaskQueue: TevCheckBox
        Left = 148
        Top = 328
        Width = 97
        Height = 17
        Caption = 'Task Queue'
        TabOrder = 9
      end
      object chbTaskScheduler: TevCheckBox
        Left = 16
        Top = 328
        Width = 111
        Height = 17
        Caption = 'Task Scheduler'
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 8
      end
      object edSUname: TevEdit
        Left = 499
        Top = 212
        Width = 150
        Height = 21
        TabOrder = 4
      end
      object edAdminUserName: TevEdit
        Left = 499
        Top = 249
        Width = 150
        Height = 21
        TabOrder = 5
      end
    end
  end
  object btnOk: TevBitBtn
    Left = 458
    Top = 406
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    Color = clBlack
    Margin = 0
  end
  object btnCancel: TevBitBtn
    Left = 573
    Top = 406
    Width = 96
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Color = clBlack
    Margin = 0
  end
  object ClientDBSrc: TevDataSource
    DataSet = ClientDbTmp
    Left = 92
    Top = 400
  end
  object ClientDbTmp: TevClientDataSet
    Left = 124
    Top = 400
  end
end
