// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SLoginDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,  ISBasicClasses, isBasicUtils,
  EvMainboard, EvTypes, SPD_RASConnection, EvContext, SShowLogo, isZippingRoutines,
  EvCommonInterfaces, EvTransportInterfaces, EvInitApp, EvConsts, evUtils, EvExceptions,
  EvUIComponents, isUIEdit, LMDCustomButton, LMDButton, isUILMDButton;

type
  TMyHintWindow = class(THintWindow)
  private
    FControl: TControl;
  public
    procedure   InitializeHint(AControl: TControl; const AText: String);
    procedure   UpdateHintPosition;
  end;

  TevLoginDialogOperation = (loLogin, loLoginWithPasswordChange, loForgotPassword);

  TevLoginDialog = class(TForm)
    pnlLogin: TevPanel;
    Image1: TevImage;
    btnLoginOk: TevBitBtn;
    btnLoginCancel: TevBitBtn;
    Panel1: TevPanel;
    lPassword: TevLabel;
    lLoginID: TevLabel;
    lServername: TevLabel;
    edtLoginid: TevEdit;
    edtPassword: TevEdit;
    edtServerName: TevEdit;
    BtnSettings: TevBitBtn;
    edtSpeed: TevComboBox;
    lCompression: TevLabel;
    chbChangePassword: TCheckBox;
    chbForgotPass: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure BtnSettingsClick(Sender: TObject);
    procedure edtLoginidChange(Sender: TObject);
    procedure edtPasswordEnter(Sender: TObject);
    procedure edtPasswordExit(Sender: TObject);
    procedure edtPasswordKeyUp(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure chbForgotPassClick(Sender: TObject);
  private
    FHintWindow: TMyHintWindow;
    FPort: String;
    FEncryption: Boolean;
    procedure CheckOKButton;
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged); message WM_WINDOWPOSCHANGED;
    procedure WMActivate(var Message: TWMActivate); message WM_ACTIVATE;
    procedure CheckCaps(AControl: TControl);
    procedure HideCapsHint;
    procedure UpdateCapsHint;
    function  GetLogin: string;
    procedure SetLogin(const Value: string);
    function  GetPassword: string;
    function  GetHost: string;
    procedure SetPassword(const Value: string);
    procedure SetHost(const Value: string);
    function  GetConnectionType: String;
    procedure SetConnectionType(const AValue: String);
    function  GetPort: string;
    procedure SetPort(const AValue: string);
    function  GetEncryption: Boolean;
    procedure SetEncryption(const AValue: Boolean);
  public
    constructor Create(const AConnectionTypes: array of string); reintroduce;
    property  Login: string read GetLogin write SetLogin;
    property  Password: string read GetPassword write SetPassword;
    property  Host: string read GetHost write SetHost;
    property  Port: string read GetPort write SetPort;
    property  ConnectionType: String read GetConnectionType write SetConnectionType;
    property  Encryption: Boolean read GetEncryption write SetEncryption;
  end;

function ShowLoginDialog(var ALogin, APassword, AHost, APort: String;
  var AEncryptionType: TevEncryptionType; var ACompressionLevel: TisCompressionLevel;
  var ALoginOperation: TevLoginDialogOperation): Boolean;

procedure DrawRounded(Control: TWinControl) ;

implementation

{$R *.DFM}

function ShowLoginDialog(var ALogin, APassword, AHost, APort: String;
  var AEncryptionType: TevEncryptionType; var ACompressionLevel: TisCompressionLevel;
  var ALoginOperation: TevLoginDialogOperation): Boolean;
const
  Compressions: array [Low(TisCompressionLevel)..High(TisCompressionLevel)] of String =
    ('NONE', 'T1', 'CABLE/DSL', 'MODEM');
var
  LoginDlg: TevLoginDialog;
  i: TisCompressionLevel;
begin
  LoginDlg := TevLoginDialog.Create(Compressions);
  try
    LoginDlg.Host := AHost;
    LoginDlg.Port := APort;
    LoginDlg.Login := ALogin;
    LoginDlg.Password := APassword;
    LoginDlg.ConnectionType := Compressions[ACompressionLevel];
    LoginDlg.Encryption := AEncryptionType <> etNone;

    case ALoginOperation of
      loLoginWithPasswordChange:  LoginDlg.chbChangePassword.Checked := True;
      loForgotPassword:           LoginDlg.chbForgotPass.Checked := True;
    end;

    Result := LoginDlg.ShowModal = mrOK;

    if Result then
    begin
      AHost := Trim(LoginDlg.Host);
      APort := LoginDlg.Port;
      ALogin := LoginDlg.Login;
      APassword := LoginDlg.Password;

      if LoginDlg.chbChangePassword.Checked then
        ALoginOperation := loLoginWithPasswordChange
      else if LoginDlg.chbForgotPass.Checked then
        ALoginOperation := loForgotPassword
      else
        ALoginOperation := loLogin;

      if LoginDlg.Encryption then
        AEncryptionType := etProprietary
      else
        AEncryptionType := etNone;

      if APort <> IntToStr(TCPClient_Port) then
        AEncryptionType := etSSL;

      ACompressionLevel := clNone;
      for i := Low(Compressions) to High(Compressions) do
        if Compressions[i] = LoginDlg.ConnectionType then
        begin
          ACompressionLevel := i;
          break;
        end;
    end;

  finally
    LoginDlg.Free;
  end;
end;


{ TLoginDlg }

constructor TevLoginDialog.Create(const AConnectionTypes: array of string);
var
  i: Integer;
begin
  inherited Create(nil);

  edtSpeed.Items.Clear;
  for i := Low(AConnectionTypes) to High(AConnectionTypes) do
    edtSpeed.AddItem(AConnectionTypes[i], nil);

  if IsStandalone then
  begin
    edtSpeed.AddItem('N/A', nil);
    edtSpeed.ItemIndex := edtSpeed.Items.Count - 1;
  end;
end;


function TevLoginDialog.GetPassword: string;
begin
  Result := edtPassword.Text;
end;

function TevLoginDialog.GetHost: string;
begin
  Result := edtServerName.Text;
end;

function TevLoginDialog.GetLogin: string;
begin
  Result := edtLoginid.Text;
end;

procedure TevLoginDialog.SetPassword(const Value: string);
begin
  edtPassword.Text := Value;
end;

procedure TevLoginDialog.SetHost(const Value: string);
begin
  if not IsStandalone then
    edtServerName.Text := Value;
end;

procedure TevLoginDialog.SetLogin(const Value: string);
begin
  edtLoginid.Text := Value;
end;

procedure TevLoginDialog.FormShow(Sender: TObject);
begin
  if edtLoginid.Enabled and (Login = '') then
    edtLoginid.SetFocus
  else if edtPassword.Enabled then
    edtPassword.SetFocus;

  CheckOKButton;
end;

procedure TevLoginDialog.BtnSettingsClick(Sender: TObject);
var
  Frm: TRASConnectionForm;
begin
  CheckCondition(Mainboard.RASAccess <> nil, 'You don''t have RAS installed', ENoRASInstalled, IDH_NoRASInstalled);

  Frm := TRASConnectionForm.Create(Self);
  try
    Frm.Port := FPort;
    Frm.Encryption := FEncryption;
    if Frm.ShowModal = mrOK then
    begin
      FPort := Frm.Port;
      FEncryption := Frm.Encryption;
    end;
  finally
    Frm.Free;
  end;

  if edtPassword.Enabled then
    edtPassword.SetFocus;
end;

procedure TevLoginDialog.edtLoginidChange(Sender: TObject);
begin
  CheckOKButton;
end;

function TevLoginDialog.GetConnectionType: String;
begin
  Result := edtSpeed.Text;
end;

procedure TevLoginDialog.SetConnectionType(const AValue: String);
begin
  if not IsStandalone then
    edtSpeed.ItemIndex := edtSpeed.Items.IndexOf(AValue);
end;

procedure TevLoginDialog.CheckCaps(AControl: TControl);
begin
  if GetKeyState(VK_CAPITAL) = 1 then
  begin
    if not Assigned(FHintWindow) then
    begin
      FHintWindow := TMyHintWindow.Create(Self);
      FHintWindow.InitializeHint(AControl, 'Caps Lock is ON');
    end;
  end
  else
    HideCapsHint;
end;

procedure TevLoginDialog.HideCapsHint;
begin
  FreeandNil(FHintWindow);
end;

procedure TevLoginDialog.UpdateCapsHint;
begin
  if Assigned(FHintWindow) then
    FHintWindow.UpdateHintPosition
end;

procedure TevLoginDialog.WMActivate(var Message: TWMActivate);
begin
  inherited;

  if (Message.Active <> WA_INACTIVE) AND edtPassword.Focused then
    CheckCaps(edtPassword)
  else
    HideCapsHint;
end;

procedure TevLoginDialog.WMWindowPosChanged(var Message: TWMWindowPosChanged);
begin
  inherited;
  UpdateCapsHint;
end;


{ TMyHintWindow }

procedure TMyHintWindow.InitializeHint(AControl: TControl;
  const AText: String);
begin
  FControl := AControl;
  Caption := AText;
  UpdateHintPosition;
  Parent := GetParentForm(FControl);
  ActivateHint(BoundsRect, AText);
end;

procedure TMyHintWindow.UpdateHintPosition;
var
  P: TPoint;
begin
  P := FControl.ClientToScreen(Point(0, FControl.Height));
  Top := P.Y;
  Left := P.X;
end;

procedure TevLoginDialog.edtPasswordEnter(Sender: TObject);
begin
  CheckCaps(edtPassword);
end;

procedure TevLoginDialog.edtPasswordExit(Sender: TObject);
begin
  HideCapsHint;
end;

procedure TevLoginDialog.edtPasswordKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CheckCaps(edtPassword);
end;

procedure TevLoginDialog.FormCreate(Sender: TObject);
var
  AppVer: String;
  v1, v2, v3, v4: String;
begin
  AppVer := ModuleVersion;
  v1 := PadLeft(copy(AppVer, 1, pos('.', AppVer)-1),'0',2);
  AppVer := copy(AppVer, pos('.', AppVer)+1, 100);
  v2 := PadLeft(copy(AppVer, 1, pos('.', AppVer)-1),'0',2);
  AppVer := copy(AppVer, pos('.', AppVer)+1, 100);
  v3 := PadLeft(copy(AppVer, 1, pos('.', AppVer)-1),'0',2);
  v4 := PadLeft(copy(AppVer, pos('.', AppVer)+1, 100), '0', 2);

  Caption := 'Evolution Login (v ' + v1+'.'+v2+'.'+v3+'.'+v4 + ')';

  if Assigned(ctx_DomainInfo.BrandInfo.LogoPicture.Data) then
    TevLogoPresenter.AssignGraphicData(Image1.Picture, ctx_DomainInfo.BrandInfo.LogoPicture)
  else
    pnlLogin.Color := clWhite;

  if IsStandalone then
  begin
    lServername.Enabled := False;
    lCompression.Enabled := False;
    edtServerName.Enabled := False;
    edtServerName.Text := 'N/A';
    edtSpeed.Enabled := False;
  end;
  //Added 7/25/2012 for GUI 2.0 enhancement
  DrawRounded(Panel1);
  BtnSettings.Enabled := Mainboard.ModuleRegister.GetModuleInfo(IevRASAccess) <> nil;
end;

function TevLoginDialog.GetPort: string;
begin
  Result := FPort;
end;

procedure TevLoginDialog.SetPort(const AValue: string);
begin
  FPort := AValue;
end;

function TevLoginDialog.GetEncryption: Boolean;
begin
  Result := FEncryption;
end;

procedure TevLoginDialog.SetEncryption(const AValue: Boolean);
begin
  FEncryption := AValue;
end;

procedure TevLoginDialog.CheckOKButton;
begin
  btnLoginOk.Enabled := (edtLoginid.Text <> '') and (Pos(' ', edtLoginid.Text) = 0) and
                        ((edtPassword.Text <> '') or chbForgotPass.Checked) and
                        (not edtServerName.Enabled or (edtServerName.Text <> '')) and
                        (not edtSpeed.Enabled or (edtSpeed.ItemIndex <> -1));

end;

procedure TevLoginDialog.chbForgotPassClick(Sender: TObject);
begin
  if chbForgotPass.Checked then
    chbChangePassword.Checked := False;

  if chbChangePassword.Checked then
    chbForgotPass.Checked := False;

  chbChangePassword.Enabled := not chbForgotPass.Checked;
  lPassword.Enabled := not chbForgotPass.Checked;
  edtPassword.Enabled := not chbForgotPass.Checked;
  CheckOKButton;
end;

procedure DrawRounded(Control: TWinControl) ;
var
   R: TRect;
   Rgn: HRGN;
begin
  //allows us to round the TPanel for a more organic shape.
   with Control do
   begin
     R := ClientRect;
     rgn := CreateRoundRectRgn(R.Left, R.Top, R.Right, R.Bottom, 8, 8) ;
     Perform(EM_GETRECT, 0, lParam(@r)) ;
     InflateRect(r, - 4, - 4) ;
     Perform(EM_SETRECTNP, 0, lParam(@r)) ;
     SetWindowRgn(Handle, rgn, True) ;
     Invalidate;
   end;
end;

end.
