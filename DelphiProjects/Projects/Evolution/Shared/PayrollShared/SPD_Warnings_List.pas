// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Warnings_List;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons,  Menus, Printers, EvUIComponents;

type
  TWarnings_List = class(TForm)
    WarningsListBox: TListBox;
    Panel1: TevPanel;
    BitBtn1: TevBitBtn;
    BitBtn2: TevBitBtn;
    PopupMenu1: TevPopupMenu;
    PrintResults1: TMenuItem;
    PrintDialog1: TPrintDialog;
    evBitBtn1: TevBitBtn;
    memoError: TevMemo;
    procedure PrintResults1Click(Sender: TObject);
    procedure WarningsListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Warnings_List: TWarnings_List;

implementation

{$R *.DFM}

procedure TWarnings_List.PrintResults1Click(Sender: TObject);
var
  I: Integer;
  rtDest: TRect;
begin
  if WarningsListBox.Items.Count = 0 then
    Exit;

  if PrintDialog1.Execute then
  begin
    Printer.BeginDoc;
    Printer.Canvas.Font := WarningsListBox.Font;
    for I := 0 to WarningsListBox.Items.Count - 1 do
    begin
      if I <> 0 then
        Printer.NewPage;
      rtDest := Rect(100, 100 * (I + 2), 200, 100 * (I + 3));
      DrawText(Printer.Canvas.Handle, PChar(WarningsListBox.Items[I]), Length(WarningsListBox.Items[I]), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    end;
    Printer.EndDoc;
  end;
end;

procedure TWarnings_List.WarningsListBoxClick(Sender: TObject);
begin
  if (WarningsListBox.Items.Count > 0) and (WarningsListBox.ItemIndex > -1) then
    memoError.Text := WarningsListBox.Items[WarningsListBox.ItemIndex]
  else
    memoError.Text := '';
end;

initialization
  RegisterClass(TWARNINGS_LIST);

end.
