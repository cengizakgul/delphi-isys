object Misc_CheckFinder: TMisc_CheckFinder
  Left = 299
  Top = 145
  Width = 677
  Height = 480
  BorderIcons = [biSystemMenu]
  Caption = 'Miscellaneous Check Finder'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object wwdgFinder: TevDBGrid
    Left = 0
    Top = 65
    Width = 661
    Height = 377
    TabStop = False
    DisableThemesInTitle = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TMisc_CheckFinder\wwdgFinder'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsFinder
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    UseTFields = True
    PaintOptions.AlternatingRowColor = clCream
  end
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 661
    Height = 65
    Align = alTop
    TabOrder = 1
    object Label1: TevLabel
      Left = 160
      Top = 8
      Width = 57
      Height = 13
      Caption = 'Check Date'
    end
    object Label2: TevLabel
      Left = 272
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Run #'
    end
    object Label4: TevLabel
      Left = 320
      Top = 8
      Width = 70
      Height = 13
      Caption = 'Check Serial #'
    end
    object editRunNbr: TevEdit
      Left = 272
      Top = 24
      Width = 33
      Height = 21
      TabOrder = 1
    end
    object bbtnFind: TevBitBtn
      Left = 408
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Show'
      Default = True
      TabOrder = 3
      OnClick = bbtnFindClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D444444444DD
        DDDDD888888888DDDDDD47FFFFFFF4DDDDDD88DDDDDDD8DDDDDD47FFFFFFF488
        8DDD88DDDDDDD8888DDD4444444444DD8DDD8888888888DD8DDD47F777777488
        888D88DFFFFFFFFFFFFD4444000000000000888F88888888888847F70BBBBBBB
        BBB088DF88888888888844480B33333333B0888F88DDDDDDDD8847F70BBBBBBB
        BBB088DF88888888888844440B33333333B0888F88DDDDDDDD8847F70BBBBBBB
        BBB088DF88888888888844440BBBBB3333B0888F888888DDDD8847F70BBBBBBB
        BBB088DF8888888888884444000000000000888D88888888888847FFFFFFF4DD
        DDDD88DDDDDDDDDDDDDD4444444444DDDDDD8888888888DDDDDD}
      NumGlyphs = 2
    end
    object bbtnOK: TevBitBtn
      Left = 496
      Top = 24
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 4
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
    end
    object bbtnCancel: TevBitBtn
      Left = 584
      Top = 24
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 5
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
    end
    object rgrpType: TevRadioGroup
      Left = 8
      Top = 8
      Width = 137
      Height = 41
      Caption = 'Type'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Agn'
        'Tax'
        'Bill')
      TabOrder = 0
    end
    object editCheckSerialNbr: TevEdit
      Left = 320
      Top = 24
      Width = 73
      Height = 21
      TabOrder = 2
    end
    object dtpkCheckDate: TevDateTimePicker
      Left = 160
      Top = 24
      Width = 97
      Height = 21
      Date = 36795.604308101900000000
      Time = 36795.604308101900000000
      ShowCheckbox = True
      Checked = False
      TabOrder = 6
    end
  end
  object wwdsFinder: TevDataSource
    DataSet = wwcsFinder
    Left = 88
    Top = 80
  end
  object wwcsFinder: TevClientDataSet
    OnCalcFields = wwcsFinderCalcFields
    Left = 32
    Top = 80
  end
end
