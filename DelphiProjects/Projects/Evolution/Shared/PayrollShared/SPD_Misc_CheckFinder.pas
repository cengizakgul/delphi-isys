// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Misc_CheckFinder;

interface

uses
   Db,  Wwdatsrc, ComCtrls, StdCtrls, Forms, DateUtils,
  ExtCtrls, Buttons, Controls, Classes, Grids, Wwdbigrd, Wwdbgrid, EvUtils,
  SysUtils, SDataStructure, SFieldCodeValues, EvConsts, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, ISDataAccessComponents, EvDataAccessComponents, isBasicUtils,
  EvContext, EvUIComponents, EvClientDataSet;

type
  TMisc_CheckFinder = class(TForm)
    wwdsFinder: TevDataSource;
    wwcsFinder: TevClientDataSet;
    wwdgFinder: TevDBGrid;
    Panel1: TevPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    editRunNbr: TevEdit;
    bbtnFind: TevBitBtn;
    bbtnOK: TevBitBtn;
    bbtnCancel: TevBitBtn;
    rgrpType: TevRadioGroup;
    Label4: TevLabel;
    editCheckSerialNbr: TevEdit;
    dtpkCheckDate: TevDateTimePicker;
    procedure FormCreate(Sender: TObject);
    procedure bbtnFindClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure wwcsFinderCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    CheckNumber: Integer;
    CheckType: Char;
    AdditionalFilter: string;
  end;

implementation

{$R *.DFM}

procedure TMisc_CheckFinder.FormCreate(Sender: TObject);
var
  StringField: TStringField;
begin
  StringField := TStringField.Create(Self);
  with StringField do
  begin
    FieldName := 'Status_Calculate';
    Size := 12;
    DisplayLabel := 'CHECK STATUS';
    DataSet := wwcsFinder;
    FieldKind := fkCalculated;
  end;

  dtpkCheckDate.Date := Date;
  dtpkCheckDate.Checked := False;
end;

procedure TMisc_CheckFinder.bbtnFindClick(Sender: TObject);
var
  q: TExecDSWrapper;
  sFilter: String;
begin
  q := TExecDSWrapper.Create('SelectMiscCheckList');
  q.SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
  if rgrpType.Items[rgrpType.ItemIndex] = 'Agn' then
    CheckType := MISC_CHECK_TYPE_AGENCY
  else
    if rgrpType.Items[rgrpType.ItemIndex] = 'Tax' then
    CheckType := MISC_CHECK_TYPE_TAX
  else
    if rgrpType.Items[rgrpType.ItemIndex] = 'Bill' then
    CheckType := MISC_CHECK_TYPE_BILLING;
  q.SetParam('CheckType', CheckType);

  sFilter := '';
  if dtpkCheckDate.Checked then
  begin
    AddStrValue(sFilter, 'P.CHECK_DATE = :FLT_CHECK_DATE' , ' and ');
    q.SetParam('FLT_CHECK_DATE', DateOf(dtpkCheckDate.Date));
  end;

  if editRunNbr.Text <> '' then
  begin
    AddStrValue(sFilter, 'P.RUN_NUMBER = :FLT_RUN_NUMBER' , ' and ');
    q.SetParam('FLT_RUN_NUMBER', StrToInt(editRunNbr.Text));
  end;

  if editCheckSerialNbr.Text <> '' then
  begin
    AddStrValue(sFilter, 'M.PAYMENT_SERIAL_NUMBER = :FLT_PAYMENT_SERIAL_NUMBER' , ' and ');
    q.SetParam('FLT_PAYMENT_SERIAL_NUMBER', editCheckSerialNbr.Text);
  end;

  if sFilter <> '' then
    sFilter := ' and ' + sFilter;
  q.SetMacro('FilterCondition', sFilter);

  sFilter := AdditionalFilter;
  if sFilter <> '' then
    sFilter := ' and ' + sFilter;
  q.SetMacro('AdditionalFilter', sFilter);


  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    DataRequest(q.AsVariant);
    Open;
    CreateNewClientDataSet(Self, wwcsFinder, ctx_DataAccess.CUSTOM_VIEW);
    wwcsFinder.Data := Data;
    Close;
    wwcsFinder.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Visible := False;
    wwcsFinder.FieldByName('PR_NBR').Visible := False;
    wwcsFinder.FieldByName('CHECK_STATUS').Visible := False;
  end;
end;

procedure TMisc_CheckFinder.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then
  begin
    if (not wwcsFinder.Active) or (wwcsFinder.BOF and wwcsFinder.EOF) then
      CanClose := False
    else
      CheckNumber := wwcsFinder.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsInteger;
  end;
end;

procedure TMisc_CheckFinder.wwcsFinderCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('Status_Calculate').Value := ReturnDescription(MiscCheckStatus_ComboChoices,
    DataSet.FieldByName('CHECK_STATUS').AsString[1]);
end;

end.
