// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SProcessTCImport;

interface

uses
  DB, EvUtils, EvConsts, SysUtils, Classes, EvContext, EvBasicUtils,
  EvTypes, Variants, EvStreamUtils, SDataStructure,Dialogs,Controls, isVCLBugFix,
  EvExceptions, EvUIUtils, EvClientDataSet;

type
  TProcessBarCallback = procedure (const Position, Count: Integer) of object;

function ProcessTCImport(const FileIn, FileOut: string;
  const LookupOption: TCImportLookupOption;
  const DBDTOption: TCImportDBDTOption; const FourDigitYear: Boolean;
  const FileFormat: TCFileFormatOption; const AutoImportJobCodes: Boolean;
  const UseEmployeePayRates: boolean; AutoCreateBatch : boolean = false;
  ScheduledDDOnly: boolean = False;ImportToBatch:integer = -1) : integer; overload;
function ProcessTCImport(const FileIn, FileOut: IisStream;
  const LookupOption: TCImportLookupOption;
  const DBDTOption: TCImportDBDTOption; const FourDigitYear: Boolean;
  const FileFormat: TCFileFormatOption; const AutoImportJobCodes: Boolean;
  const UseEmployeePayRates: boolean; AutoCreateBatch : boolean = false;
  ScheduledDDOnly: boolean = False;ImportToBatch:integer = -1) : integer; overload;

  procedure EXternLogEvent(employeeNumber:string;employeeName:string;TypeOfEvent:string);
type
  TForEEAnswer = class(TCollectionItem)
  public
    EE_nbr:string;
  end;

  TForEEAnswers = class (TCollection)
    function GetItem(Index: Integer): TForEEAnswer;
    procedure SetItem(Index: Integer; const Value: TForEEAnswer);
  public
    constructor Create;
    function  Add: TForEEAnswer;
    procedure AddEe(EE_NBR:string);
    property  Items[Index: Integer]: TForEEAnswer read GetItem write SetItem; default;
    function  GetLastEEAnswer(EE_nbr:string): TModalResult;
  end;


var ExtLogStrings:TStringList;
    EXternLogEventLastAnswer:TModalResult;
    AnswersForEachEE:TForEEAnswers;


implementation

uses  SDataDictclient, DateUtils, EvCommonInterfaces, EvDataset, isBasicUtils, EvDataAccessComponents;

///////////////////////////////////////////////////////////////////////////////
function TForEEAnswers.Add: TForEEAnswer;
begin
  Result := inherited Add as TForEEAnswer;
end;

constructor TForEEAnswers.Create;
begin
  inherited Create( TForEEAnswer );
end;

function TForEEAnswers.GetItem(Index: Integer): TForEEAnswer;
begin
  Result := inherited Items[ Index ] as TForEEAnswer;
end;


procedure TForEEAnswers.SetItem(Index: Integer; const Value: TForEEAnswer);
begin
  Items[Index] := Value;
end;

procedure TForEEAnswers.AddEE(EE_NBR: string);
var
   EE_Answer:TForEEAnswer;
begin
   if GetLastEEAnswer(EE_NBR)<>mrNo then
   begin
      EE_Answer := Add();
      EE_Answer.EE_nbr := EE_NBR;
   end;
end;

function TForEEAnswers.GetLastEEAnswer(EE_nbr: string): TModalResult;
var i:integer;
begin
    result := mrYes;
    for i:= 0 to Self.Count-1 do
    begin
       if(Self.Items[i].EE_nbr = EE_nbr) then
       begin
          Result := mrNo;
          break;
       end;
    end;
end;

/////////////////////////////////////////////////////////////////////////////////////
procedure EXternLogEvent(employeeNumber:string;employeeName:string;TypeOfEvent:string);
    var Msg:string;
        ImpMsg:string;
        EeAnswer:TModalResult;
        EEnbr:string;
  begin
    if TypeOfEvent = 'hours' then
    begin
       Msg := employeeNumber +': ' +employeeName+ #13 +'Total hours exceeds the maximum.'+#13+'Would you like to import this check line?';
       ImpMsg :='Total hours exceeds the maximum.';
    end
    else if TypeOfEvent = 'amounts' then
    begin
       Msg := employeeNumber +': ' +employeeName+ #13 +'Total amount exceeds the maximum. '+#13+'Would you like to import this check line?';
       ImpMsg :='Total amounts exceeds the maximum.';
    end
    else if TypeOfEvent = 'amounts&hours' then
    begin
       Msg := employeeNumber +': ' +employeeName+ #13 +'Total hours exceeds the maximum.'+ #13
                                  +'Total amount exceeds the maximum.'+ #13
                                  +'Would you like to import this check line?';
       ImpMsg :='Total amounts and hours exceeds the maximum.';
    end
    else
    begin
       Msg := employeeNumber +': ' +employeeName+ #13 +'Unknown error. '+#13+'Would you like to continue?';
       ImpMsg :='Unknown error.';
    end;

    ////////////////////////////////////////////////////////////////////////////////////////////
    EEnbr := employeeNumber;
    EeAnswer := AnswersForEachEE.GetLastEEAnswer(EEnbr);
    if (EXternLogEventLastAnswer <> mrYesToAll) and
       (EXternLogEventLastAnswer <> mrNoToAll)  and
       (EeAnswer <> mrNo) then
    begin
        EXternLogEventLastAnswer :=EvMessage(Msg, mtConfirmation, [mbYes, mbNo,mbYesToAll,mbNoToAll,mbAbort]);
    end;
    if EXternLogEventLastAnswer in  [mrNo,mrNoToAll] then
       AnswersForEachEE.AddEe(EEnbr);
    ExtLogStrings.Add(ImpMsg);
    if (EXternLogEventLastAnswer <> mrYes) and (EXternLogEventLastAnswer <> mrYesToAll) then
    begin
      AbortEx;
    end;
end;

// function returns amount of rejected records
function ProcessTCImport(const FileIn, FileOut: string;
  const LookupOption: TCImportLookupOption;
  const DBDTOption: TCImportDBDTOption;  const FourDigitYear: Boolean;
  const FileFormat: TCFileFormatOption; const AutoImportJobCodes: Boolean;
  const UseEmployeePayRates: boolean; AutoCreateBatch : boolean = false;
  ScheduledDDOnly: boolean = False;ImportToBatch:integer = -1) : integer; overload;
var
  sIn, sOut: IisStream;
begin
  result := 0;
// ca   EXternLogEventLastAnswer := mrYes;
// ca   AnswersForEachEE := TForEEAnswers.Create();
  if FileIn = '' then
    raise EInvalidParameters.CreateHelp('Import file name is required', IDH_InvalidParameters);
  ForceDirectories(ExtractFilePath(FileOut));
  sIn := TEvDualStreamHolder.CreateFromFile(FileIn);
  sOut := TEvDualStreamHolder.Create();
  try
    try
      sOut.SaveToFile(FileOut);
    except
      raise EInvalidParameters.CreateHelp('Output file name is wrong. Can not create such file.', IDH_InvalidParameters);
    end;
    result := ProcessTCImport(sIn, sOut, LookupOption, DBDTOption, FourDigitYear, FileFormat, AutoImportJobCodes, UseEmployeePayRates, AutoCreateBatch, ScheduledDDOnly,ImportToBatch);
  finally
    try
      sOut.SaveToFile(FileOut);
// ca      AnswersForEachEE.Destroy;
    except
    end;
  end;
end;

function PadField(const s: string; ds: TDataset; const sFieldName: string): string;
begin
  Result := PadStringLeft(s, ' ', ds.FieldByName(sFieldName).Size);
end;

// function returns amount of rejected records
function ProcessTCImport(const FileIn, FileOut: IisStream;
  const LookupOption: TCImportLookupOption;
  const DBDTOption: TCImportDBDTOption; const FourDigitYear: Boolean;
  const FileFormat: TCFileFormatOption; const AutoImportJobCodes: Boolean;
  const UseEmployeePayRates: boolean; AutoCreateBatch : boolean = false;
  ScheduledDDOnly: boolean = False;ImportToBatch:integer = -1) : integer;
var
  S: string;
  lCsfList: TStrings;
  EeDbCustN: string;
  iLineNumber, iSeqNumber: Integer;
  lMatrix, lWorkComp: TevClientDataset;
  Positions1: array [1..314] of Integer;
  iEEStatesRecordCount, pSSN, lSSN: integer;

  listDiv, listBr, listDep, listTm : TStringList;

const
  fOutFormat = '| %5u | %6s | %60s | %12s |';
  sRejected = 'Rejected';
  sImported = 'Imported';
  Positions2: array [0..32] of Integer = (1, 7, 32, 38, 50, 51, 52, 54, 63, 71, 73, 75, 77, 79, 81, 90, 91, 97, 103, 105, 176, 186, 196, 202, 211, 217, 221, 235, 196, 249, 309, 311, 313);
  TC_COBRACREDITTYPE = 1;

  procedure AppendToLog(s: string = '');
  begin
    s := s+ #13#10;
    FileOut.WriteBuffer(s[1], Length(s));
  end;

  function SelectWhichOne(const b: Boolean; const v1, v2: Variant): Variant;
  begin
    if b then
      Result := v1
    else
      Result := v2;
  end;

  function Copy2(const s: string; const pos, len: Integer; const CommaDelimitedLen : integer = -1): string;
  var
   nPos: integer;
  begin

    nPos:= pos;
    if (pSSN > 0) and (pos > 202) then
      nPos:= pos + pSSN;

    if FileFormat = loFixed then
    begin
      if FourDigitYear and (pos >= 311) then
        Result := Copy(s, nPos+4, len)
      else
      if FourDigitYear and (pos = 309) then
        Result := Copy(s, nPos+2, 4)
      else
      if FourDigitYear and (pos >= 73) then
        Result := Copy(s, nPos+2, len)
      else
      if FourDigitYear and (pos = 71) then
        Result := Copy(s, nPos, 4)
      else
      if not FourDigitYear and ((pos = 71) or (pos = 309)) then
        Result := '20'+ Copy(s, nPos, 2)
      else
        Result := Copy(s, nPos, len)
    end
    else
    begin
      Result := '';
      if Positions1[pos] < lCsfList.Count then
        if CommaDelimitedLen = -1 then
          Result := lCsfList[Positions1[pos]]
        else
          Result := Copy(lCsfList[Positions1[pos]], 1, CommaDelimitedLen);
      if not FourDigitYear and ((pos = 71) or (pos = 309)) and (Length(Trim(Result)) = 2)  then
        Result := '20'+ Result;
    end;
  end;

  //Get punch info from the import file
  function GetPunchDetail(AString: String; var ADateTime: TDateTime): Boolean;
  var
    AYear, AMonth, ADay: Integer;
    AHour, AMinute, ASecond: Integer;
  begin
    //Format in AString  =  yyyymmddhhnnss

    Result := False;

    if Length(AString) <> 14 then
    begin
      Exit;
    end;

    try
      AYear := StrToInt(Copy(AString, 1, 4));
      AMonth := StrToInt(copy(AString, 5, 2));
      ADay := StrToInt(copy(AString, 7, 2));
      AHour := StrToInt(copy(AString, 9, 2));
      AMinute := StrToInt(copy(AString, 11, 2));
      ASecond := StrToInt(copy(AString, 13, 2));
      ADateTime := EncodeDateTime(AYear, AMonth, ADay, AHour, AMinute, ASecond, 0);
      Result := True;
    except
      //Eat Exceptions and return back with false to ignore the time stamp.
    end;

  end;

  procedure OverRideLocal(const ACoStatesNbr: integer);
  var
    sOverrideLocal: String;
    iSysStatesNbr: integer;
    bFound: boolean;
    Q: IevQuery;
  begin
          sOverrideLocal := Trim(Copy2(s, 105,40));
          if sOverrideLocal <> '' then    //    OVERRIDE LOCAL
          begin
             if ACoStatesNbr > 0 then
             begin
               Q := TevQuery.Create('select SY_STATES_NBR from CO_STATES a where {AsOfNow<a>} and a.CO_STATES_NBR=' + IntToStr(ACoStatesNbr));
               Q.Execute;
               CheckCondition(Q.Result.RecordCount = 1, 'State not found in CO_STATES table. CO_STATES_NBR=' + IntToStr(ACoStatesNbr));
               iSysStatesNbr := Q.Result['SY_STATES_NBR'];
               bFound := DM_COMPANY.CO_LOCAL_TAX.Locate('CO_NBR;SY_LOCALS_NBR', VarArrayOf([DM_COMPANY.CO['CO_NBR'],
                                           DM_SYSTEM.SY_LOCALS.Lookup('NAME; SY_STATES_NBR', VarArrayOf([sOverrideLocal, iSysStatesNbr]), 'SY_LOCALS_NBR')]), []);
             end
             else
               bFound := DM_COMPANY.CO_LOCAL_TAX.Locate('CO_NBR;SY_LOCALS_NBR', VarArrayOf([DM_COMPANY.CO['CO_NBR'],
                                           DM_SYSTEM.SY_LOCALS.Lookup('NAME', sOverrideLocal, 'SY_LOCALS_NBR')]), []);

             if not bFound then
             begin
               AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown CO Local code: ' + Trim(Copy2(s, 105,40)), sRejected]));
               Inc(result);
               AbortEx;
             end
             else
             begin
                if not DM_COMPANY.EE_LOCALS.active then
                   DM_COMPANY.EE_LOCALS.Open;
                if not DM_COMPANY.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR',
                                          VarArrayOf([DM_EMPLOYEE.EE['EE_NBR'], DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']]), []) then
                begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown EE Local code: ' + Trim(Copy2(s, 105,40)), sRejected]));
                 Inc(result);
                 AbortEx;
                end
                else
                begin
                 DM_PAYROLL.PR_CHECK_LINE_LOCALS.Insert;
                 Try
                  DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
                  DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] :=  DM_COMPANY.EE_LOCALS['EE_LOCALS_NBR'];
                  DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := 'I';

                  DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
                 except
                  DM_PAYROLL.PR_CHECK_LINE_LOCALS.Cancel;
                  raise;
                 end;
                end;
             end;
          end;
  end;

  procedure DBDTPayrateControl;
  var
    V: variant;
  begin
             if not DM_PAYROLL.PR_CHECK_LINES.CO_TEAM_NBR.IsNull then
             begin
              if DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'],'OVERRIDE_EE_RATE_NUMBER') > 0 then
              begin
                V := DM_EMPLOYEE.EE_RATES.Lookup('EE_NBR;RATE_NUMBER',
                         VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'],
                         DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'],'OVERRIDE_EE_RATE_NUMBER')]),
                         'RATE_NUMBER');
                if VarIsNull(V) then
                begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Team Rate number is not setup for employee # ', sRejected]));
                 Inc(result);
                 AbortEx;
                end;
              end;
             end
             else
             if not DM_PAYROLL.PR_CHECK_LINES.CO_DEPARTMENT_NBR.IsNull then
             begin
              if DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'],'OVERRIDE_EE_RATE_NUMBER') > 0 then
              begin
                V := DM_EMPLOYEE.EE_RATES.Lookup('EE_NBR;RATE_NUMBER',
                         VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'],
                         DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'],'OVERRIDE_EE_RATE_NUMBER')]),
                         'RATE_NUMBER');
                if VarIsNull(V) then
                begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Department Rate number is not setup for employee # ', sRejected]));
                 Inc(result);
                 AbortEx;
                end;
              end;
             end
             else
             if not DM_PAYROLL.PR_CHECK_LINES.CO_BRANCH_NBR.IsNull then
             begin
              if DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'],'OVERRIDE_EE_RATE_NUMBER') > 0 then
              begin
                V := DM_EMPLOYEE.EE_RATES.Lookup('EE_NBR;RATE_NUMBER',
                         VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'],
                         DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'],'OVERRIDE_EE_RATE_NUMBER')]),
                         'RATE_NUMBER');
                if VarIsNull(V) then
                begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Branch Rate number is not setup for employee #  ', sRejected]));
                 Inc(result);
                 AbortEx;
                end;
              end;
             end
             else
             if not DM_PAYROLL.PR_CHECK_LINES.CO_DIVISION_NBR.IsNull then
             begin
              if DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'],'OVERRIDE_EE_RATE_NUMBER') > 0 then
              begin
                V := DM_EMPLOYEE.EE_RATES.Lookup('EE_NBR;RATE_NUMBER',
                         VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'],
                         DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR',DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'],'OVERRIDE_EE_RATE_NUMBER')]),
                         'RATE_NUMBER');
                if VarIsNull(V) then
                begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Division Rate number is not setup for employee #  ', sRejected]));
                 Inc(result);
                 AbortEx;
                end;
              end;
             end;
  end;

  procedure AddLine(const pRate, pHours, pAmount, pRateNumber: string;RunNbr:integer);
  var
    EdCode: string[20];
    WC: String[6];
    WC2: String[4];
    WCState: String[2];
    s2, s3, sFilter: string;
    V: Variant;
    PunchIn, PunchOut: TDateTime;
    i:integer;
    iCoStatesNbr: integer;
  begin
    if (AnswersForEachEE.GetLastEEAnswer(EeDbCustN) = mrNo) then
      exit;

    iCoStatesNbr := -1;
    with DM_PAYROLL, DM_CLIENT, DM_EMPLOYEE, DM_COMPANY do
    try
      PR_CHECK_LINES.Insert;
      try
        PR_CHECK_LINES['PR_CHECK_NBR'] := PR_CHECK['PR_CHECK_NBR'];
        PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
        EdCode := Trim(Copy2(s, 51, 1)) + Trim(Copy2(s, 52, 2));
        if not CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', EdCode, [loCaseInsensitive]) then
          if (Length(Trim(EdCode)) <> 2)
          or not CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', EdCode[1]+ '0'+ EdCode[2], [loCaseInsensitive]) then
          begin
            if RunNbr = 1 then
            begin
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown E/D code: ' + EdCode, sRejected]));
              Inc(result);
            end;
            AbortEx;
          end;
        PR_CHECK_LINES['CL_E_DS_NBR'] := CL_E_DS['CL_E_DS_NBR'];


        if pRate <> '' then
        begin
          if StrToFloat(pRate) <> 0 then
            PR_CHECK_LINES['RATE_OF_PAY'] := StrToFloat(pRate)
          else
            PR_CHECK_LINES['RATE_NUMBER'] := -1;
        end;
        if pRateNumber <> '' then
          PR_CHECK_LINES['RATE_NUMBER'] := StrToFloat(pRateNumber);
        if pHours <> '' then
          PR_CHECK_LINES['HOURS_OR_PIECES'] := StrToFloat(pHours);
        if pAmount <> '' then
          PR_CHECK_LINES['AMOUNT'] := StrToFloat(pAmount);
        if (Trim(Copy2(s, 73, 2)) <> '') and (Trim(Copy2(s, 75, 2)) <> '') and (Trim(Copy2(s, 71, 2)) <> '') then
          if (FourDigitYear and (Trim(Copy2(s, 71, 4)) <> ''))
          or (not FourDigitYear and (Trim(Copy2(s, 71, 2)) <> '')) then
          try
            PR_CHECK_LINES['LINE_ITEM_DATE'] := EncodeDate(StrToInt(Trim(Copy2(s, 71, 2))),
              StrToInt(Trim(Copy2(s, 73, 2))), StrToInt(Trim(Copy2(s, 75, 2))))
          except
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Wrong date provided: ' + Trim(Copy2(s, 73, 2))+ '/'+ Trim(Copy2(s, 75, 2))+ '/'+ Trim(Copy2(s, 71, 2)), sRejected]));
            Inc(result);
            AbortEx;
          end;
        if (Trim(Copy2(s, 311, 2)) <> '') and (Trim(Copy2(s, 313, 2)) <> '') and (Trim(Copy2(s, 309, 2)) <> '') then
          if (FourDigitYear and (Trim(Copy2(s, 309, 4)) <> ''))
          or (not FourDigitYear and (Trim(Copy2(s, 309, 2)) <> '')) then
          try
            PR_CHECK_LINES['LINE_ITEM_END_DATE'] := EncodeDate(StrToInt(Trim(Copy2(s, 309, 2))),
              StrToInt(Trim(Copy2(s, 311, 2))), StrToInt(Trim(Copy2(s, 313, 2))))
          except
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Wrong date provided: ' + Trim(Copy2(s, 311, 2))+ '/'+ Trim(Copy2(s, 313, 2))+ '/'+ Trim(Copy2(s, 309, 2)), sRejected]));
            Inc(result);
            AbortEx;
          end;

        if Trim(Copy2(s, 103, 2)) <> '' then // EE state
        begin
          if not EE_STATES.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([EE['EE_NBR'], CO_STATES.Lookup('STATE', Trim(Copy2(s, 103, 2)),
            'CO_STATES_NBR')]), []) then
          begin
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown state code: ' + Trim(Copy2(s, 103, 2)), sRejected]));
            Inc(result);
            AbortEx;
          end
          else
          begin
            PR_CHECK_LINES['EE_STATES_NBR'] := EE_STATES['EE_STATES_NBR'];
            iCoStatesNbr := EE_STATES['CO_STATES_NBR']
          end;
        end;
        if Trim(Copy2(s, 38, 12)) <> '' then // Co Jobs
        begin
          if not CO_JOBS.Locate('DESCRIPTION', Trim(Copy2(s, 38, 12)), []) then
          begin
            if not AutoImportJobCodes then
            begin
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown job code: ' + Trim(Copy2(s, 38, 12)), sRejected]));
              Inc(result);
              AbortEx;
            end
            else
            begin
              CO_JOBS.Append;
              CO_JOBS['DESCRIPTION'] := Trim(Copy2(s, 38, 12));
              CO_JOBS['TRUE_DESCRIPTION'] := Trim(Copy2(s, 38, 12));
              CO_JOBS['JOB_ACTIVE'] := GROUP_BOX_YES;
              CO_JOBS['CERTIFIED'] := GROUP_BOX_NO;
              CO_JOBS['STATE_CERTIFIED'] := GROUP_BOX_NO;
              CO_JOBS['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
              CO_JOBS.Post;
              PR_CHECK_LINES['CO_JOBS_NBR'] := CO_JOBS['CO_JOBS_NBR'];
            end;
          end
          else
            PR_CHECK_LINES['CO_JOBS_NBR'] := CO_JOBS['CO_JOBS_NBR'];
        end;

        WC := Trim(Copy2(s, 211, 6));
        WC2 := Trim(Copy(WC, 1, 4));
        WCState := Trim(Copy(WC, 5, 2));
        if WC <> '' then // Work comp code
        begin
          //Get valid CO_STATES_NBR
          if WCState <> '' then
          begin
            V := CO_STATES.Lookup('STATE', WCState, 'CO_STATES_NBR');
            if VarIsNull(V) then
            begin
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown state for the current workers comp code: ' + WC, sRejected]));
              Inc(result);
              AbortEx;
            end;
          end;

          if lWorkComp.Locate('CO_STATES_NBR;WORKERS_COMP_CODE', VarArrayOf([V, WC2]), []) then
          begin
            PR_CHECK_LINES['CO_WORKERS_COMP_NBR'] := lWorkComp['CO_WORKERS_COMP_NBR'];
          end
          else if lWorkComp.Locate('WORKERS_COMP_CODE', WC, []) then
          begin
             PR_CHECK_LINES['CO_WORKERS_COMP_NBR'] := lWorkComp['CO_WORKERS_COMP_NBR'];
          end
          else
          begin
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown workers comp code: ' + WC, sRejected]));
            Inc(result);
            AbortEx;
          end;
        end;

        if Trim(Copy2(s, 50, 1)) <> '' then // shift
        begin
          if CO_SHIFTS.Locate('NAME', Trim(Copy2(s, 50, 1)), [loPartialKey]) then
            PR_CHECK_LINES['CO_SHIFTS_NBR'] := CO_SHIFTS['CO_SHIFTS_NBR']
          else
          begin
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown shift code: ' + Trim(Copy2(s, 50, 1)), sRejected]));
            Inc(result);
            AbortEx;
          end
        end;

        if (Trim(Copy2(s, 32, 6)) = '') and
          (Trim(Copy2(s, 91, 6)) = '') and
          (Trim(Copy2(s, 97, 6)) = '') and
          (Trim(Copy2(s, 196, 6)) = '')
          then
        begin
          if UseEmployeePayRates and (pHours <> '') then
          begin
            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Missing DBDT information', sRejected]));
            Inc(result);
            AbortEx;
          end;
        end
        else
        begin
          sFilter := '';

          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsInteger >= 1 then
          begin
            s2 := Trim(Copy2(s, 91, 6));
            if (s2 <> '') then
            begin
              s3 := PadField(s2, CO_DIVISION, 'CUSTOM_DIVISION_NUMBER');
              sFilter := 'CUSTOM_DIVISION_NUMBER = '+ QuotedStr(s3);
              if (DBDTOption = loFull) and (listDiv.IndexOf(s3) = -1) then
              begin
                  AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown division code: ' + s2, sRejected]));
                  Inc(result);
                  AbortEx;
              end;
            end
            else
            if DBDTOption = loFull then
            begin
                //  if only onre record..
                AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Missing division code or setup', sRejected]));
                Inc(result);
                AbortEx;
            end;
          end;
          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsInteger >= 2 then
          begin
            s2 := Trim(Copy2(s, 97, 6));
            if (s2 <> '') then
            begin
              if sFilter <> '' then
                sFilter := sFilter+ ' and ';
              s3 := PadField(s2, CO_BRANCH, 'CUSTOM_BRANCH_NUMBER');
              sFilter := sFilter+ 'CUSTOM_BRANCH_NUMBER = '+ QuotedStr(s3);
              if (DBDTOption = loFull) and (listBr.IndexOf(s3) = -1) then
              begin
                  AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown branch code: ' + s2, sRejected]));
                  Inc(result);
                  AbortEx;
              end;
            end
            else
            if DBDTOption = loFull then
            begin
                AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Missing branch code or setup', sRejected]));
                Inc(result);
                AbortEx;
            end;
          end;

          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsInteger >= 3 then
          begin
            s2 := Trim(Copy2(s, 32, 6));
            if (s2 <> '') then
            begin
              if sFilter <> '' then
                sFilter := sFilter+ ' and ';
              s3 := PadField(s2, CO_DEPARTMENT, 'CUSTOM_DEPARTMENT_NUMBER');
              sFilter := sFilter+ 'CUSTOM_DEPARTMENT_NUMBER = '+ QuotedStr(s3);
              if (DBDTOption = loFull) and (listDep.IndexOf(s3) = -1 ) then
              begin
                  AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown dept. code: ' + s2, sRejected]));
                  Inc(result);
                  AbortEx;
              end;
            end
            else
            if DBDTOption = loFull then
            begin
                AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Missing dept. code or setup', sRejected]));
                Inc(result);
                AbortEx;
            end;
          end;

          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsInteger >= 4 then
          begin
            s2 := Trim(Copy2(s, 196, 6));
            if (s2 <> '') then
            begin
              if sFilter <> '' then
                sFilter := sFilter+ ' and ';
              s3 := PadField(s2, CO_TEAM, 'CUSTOM_TEAM_NUMBER');
              sFilter := sFilter+ 'CUSTOM_TEAM_NUMBER = '+ QuotedStr(s3);
              if (DBDTOption = loFull) and (listTm.IndexOf(s3) = -1 ) then
              begin
                  AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Unknown team code: ' + s2, sRejected]));
                  Inc(result);
                  AbortEx;
              end;
            end
            else
            if DBDTOption = loFull then
            begin
                AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Missing dept. code or setup', sRejected]));
                Inc(result);
                AbortEx;
            end;
          end;

          if sFilter <> '' then
            lMatrix.Filter := 'DBDT_LEVEL = ' + QuotedStr(DM_COMPANY.CO['DBDT_LEVEL']) + ' and ' + sFilter;

          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsInteger <> 0 then
           if lMatrix.RecordCount <> 1 then
           begin
            if lMatrix.RecordCount = 0 then
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Could not match DBDT codes', sRejected]))
            else
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'DBDT codes are not unique', sRejected]));
            Inc(result);
            AbortEx;
           end;

          PR_CHECK_LINES['CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR'] := lMatrix['CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR'];

          if UseEmployeePayRates and (PR_CHECK_LINES.RATE_OF_PAY.IsNull) then
          begin
            V := DM_EMPLOYEE.EE_RATES.Lookup(
              'EE_NBR;CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR',
              VarArrayOf([PR_CHECK['EE_NBR'], PR_CHECK_LINES['CO_DIVISION_NBR'],
              PR_CHECK_LINES['CO_BRANCH_NBR'], PR_CHECK_LINES['CO_DEPARTMENT_NBR'],
              PR_CHECK_LINES['CO_TEAM_NBR']]), 'RATE_NUMBER;RATE_AMOUNT');

            if VarType(V) = varNull then
              V := DM_EMPLOYEE.EE_RATES.Lookup('EE_NBR;PRIMARY_RATE',
                          VarArrayOf([PR_CHECK['EE_NBR'], 'Y']), 'RATE_NUMBER;RATE_AMOUNT')
            else
              PR_CHECK_LINES['RATE_NUMBER'] := V[0];

            PR_CHECK_LINES['RATE_OF_PAY'] := V[1];
          end
          else
          if PR_CHECK_LINES.RATE_OF_PAY.IsNull then
             DBDTPayrateControl;
        end;

        //Punch-In
        if GetPunchDetail(Trim(Copy2(s, 221, 14)), PunchIn) then
          PR_CHECK_LINES['PUNCH_IN'] := PunchIn;
        //Punch-Out
        if GetPunchDetail(Trim(Copy2(s, 235, 14)), PunchOut) then
          PR_CHECK_LINES['PUNCH_OUT'] := PunchOut;

        try
          // iEEStatesRecordCount is a stupid fix for #69879, but what to do :(
          // PR_CHECK_LINES.Post set up filter for EE_STATES for some records
          try
            iEEStatesRecordCount := -1;
            if DM_EMPLOYEE.EE_STATES.Active then
              iEEStatesRecordCount := DM_EMPLOYEE.EE_STATES.RecordCount;
            PR_CHECK_LINES.Post;
          finally
            if (iEEStatesRecordCount <> -1) and (iEEStatesRecordCount <> DM_EMPLOYEE.EE_STATES.RecordCount) then
            begin
              DM_EMPLOYEE.EE_STATES.Close;
              DM_EMPLOYEE.EE_STATES.DataRequired('ALL');
            end;
          end;

          for i:=0 to ExtLogStrings.Count-1 do
          begin
             AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN,ExtLogStrings[i] , sImported]));
          end;

          OverRideLocal(iCoStatesNbr);  //    OVERRIDE LOCAL

          ExtLogStrings.Clear;
        except
          on EAbort do
          begin
            for i:=0 to ExtLogStrings.Count-1 do
            begin
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN,ExtLogStrings[i] , sRejected]));;
              Inc(result);
            end;
            ExtLogStrings.Clear;
            //AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Failure CheckLine.Post', sRejected]));;
            //PR_CHECK_LINES.Cancel;
            raise;
          end;
        end;
      except
        PR_CHECK_LINES.Cancel;
        raise;
      end;
    except
      on EAbort do
      begin
         if EXternLogEventLastAnswer = mrAbort then
           raise;
      end;
    else
      raise;
    end;
  end;

  procedure ClearOtherCoRecords(const cd1, cd2: TevClientDataSet; const fname: string);
  begin
    cd1.LogChanges := False;
    cd1.First;
    while not cd1.Eof do
    begin
      if cd2.Locate(fname, cd1[fname], []) then
        cd1.Next
      else
        cd1.Delete;
    end;
  end;

  function DeleteStartingZeroes(const s: string): string;
  var
    i, j: Integer;
  begin
    i := 1;
    j := Length(s);
    while (i < j) and (s[i] = '0') do
      Inc(i);
    Result := Copy(s, i, 255);
  end;


var
  bEeFound: Boolean;
  sFirstName, sLastName, sSSN, sSecN: string;
  lList: TStringList;
  i: Integer;
  cCheckType: Char;

  function StripLeadingZeroes(const s: string): string;
  begin
    Result := s;
    while (Length(Result) > 0) and (Result[1] in [' ', '0']) do
      Result := Copy(Result, 2, 1024);
  end;

var
  Rate, ARate, Hours, Amount, RateNumber: string;
  LogSetter :IPrCheckLinesLoggerSetter;
  BatchFound, CheckFound, EDCodeFound : boolean;
  loop : integer;
  lEdCode: string[20];
  bpost: TDataSetNotifyEvent;
begin
  result := 0;

  EXternLogEventLastAnswer := mrYes;
  AnswersForEachEE := TForEEAnswers.Create();


  if DM_PAYROLL.PR_CHECK_LINES.Owner.GetInterface(IPrCheckLinesLoggerSetter,LogSetter) then
  begin
    LogSetter.SetLogger(EXternLogEvent);
  end  else
    assert(false);
  ExtLogStrings := TStringList.Create;
  for i := Low(Positions1) to High(Positions1) do
    Positions1[i] := 0;
  for i := Low(Positions2) to High(Positions2) do
    Positions1[Positions2[i]] := i;

  lList := TStringList.Create;
  lCsfList := TStringList.Create;

  lMatrix := TevClientDataSet.Create(nil);
  lWorkComp := TevClientDataSet.Create(nil);
  SaveDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS]);

  try
    with TExecDSWrapper.Create('TC_SelectDBDT') do
    begin
        SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
        ctx_DataAccess.GetCustomData(lMatrix, AsVariant);
        lMatrix.Filter := 'DBDT_LEVEL = ' + QuotedStr(DM_COMPANY.CO['DBDT_LEVEL']);
        lMatrix.Filtered := True;
    end;

    ListDiv := lMatrix.GetFieldValueList('CUSTOM_DIVISION_NUMBER',True);
    listBr  := lMatrix.GetFieldValueList('CUSTOM_BRANCH_NUMBER',True);
    listDep := lMatrix.GetFieldValueList('CUSTOM_DEPARTMENT_NUMBER',True);
    listTm  := lMatrix.GetFieldValueList('CUSTOM_TEAM_NUMBER',True);

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'CO_WORKERS_COMP_NBR, WORKERS_COMP_CODE, CO_STATES_NBR');
      SetMacro('TABLENAME', 'CO_WORKERS_COMP');
      SetMacro('CONDITION', 'CO_NBR = :CONBR');
      SetParam('CONBR', DM_COMPANY.CO['CO_NBR']);
      ctx_DataAccess.GetCustomData(lWorkComp, AsVariant);
    end;
    lList.LoadFromStream(FileIn.RealStream);

    pSSN:= 0;
    lSSN:= 9;
    if lList.Count > 0 then
    begin
       s := lList[0];
       if FileFormat = loCommaDelimited then
       begin
          lCsfList.CommaText := StringReplace(s, ' ', '|', [rfReplaceAll]);
          lCsfList.Text := StringReplace(lCsfList.Text, '|', ' ', [rfReplaceAll]);
       end;
       s := Trim(Copy2(s, 202, lSSN));
       if (Pos('-', s) > 0) then
       begin
          pSSN:= 2;
          lSSN:= 11;
       end;
     end;

    AppendToLog(Format('Timeclock import exception report. Started at %18s', [DateTimeToStr(Now)]));
    AppendToLog();
    SetLength(s, 95);
    FillChar(s[1], 95, '=');
    AppendToLog(s);
    AppendToLog(Format(fOutFormat, [0, 'EE #', 'Reason', 'Line status']));
    AppendToLog(s);
    iLineNumber := 0;
    ctx_StartWait('Processing...' + #13#13 +'Company:  #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '  "' + DM_COMPANY.CO.FieldByName('NAME').AsString + '"', lList.Count);
    DM_PAYROLL.PR_CHECK_LINES.DisableControls;
    try
      DM_PAYROLL.PR_CHECK.SaveStateAndSet(False, '' , '', 'EE_NBR;CHECK_TYPE;SortTag');
      DM_EMPLOYEE.EE.SaveStateAndReset;
      case LookupOption of
      loByNumber:
        DM_EMPLOYEE.EE.IndexFieldNames := 'CUSTOM_EMPLOYEE_NUMBER';
      loByName,
      loBySSN:
        DM_EMPLOYEE.EE.IndexFieldNames := 'CL_PERSON_NBR;CURRENT_TERMINATION_CODE';
      else
        Assert(False);
      end;
      with DM_PAYROLL, DM_EMPLOYEE, DM_CLIENT do
      try
        if PR['PAYROLL_TYPE'] = PAYROLL_TYPE_SETUP then
          cCheckType := CHECK_TYPE2_YTD
        else
          cCheckType := CHECK_TYPE2_REGULAR;
        Assert(EE.Active and CL_PERSON.Active);

//   loop = 0    for all cl_e_ds's
//   loop = 1   only for MC type's

        if ImportToBatch > 0  then
        begin
           AutoCreateBatch := false;
           if not PR_BATCH.Locate('PR_BATCH_NBR',ImportToBatch,[]) then
              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Wrong Batch ' , sRejected]));
           PR_CHECK.Filter := 'PR_BATCH_NBR = '+ IntToStr(ImportToBatch);
           PR_CHECK.Filtered := true;
        end;

        for loop := 0 to 1 do
        begin

                         //   to create only cobra check type...
          if loop = TC_COBRACREDITTYPE then
            cCheckType := CHECK_TYPE2_COBRA_CREDIT;

          for i := 0 to lList.Count-1 do
          begin
            s := lList[i];

            if FileFormat = loCommaDelimited then
            begin
              lCsfList.CommaText := StringReplace(s, ' ', '|', [rfReplaceAll]);
              lCsfList.Text := StringReplace(lCsfList.Text, '|', ' ', [rfReplaceAll]);
            end;
            Inc(iLineNumber);

            lEdCode := Trim(Copy2(s, 51, 1)) + Trim(Copy2(s, 52, 2));
            EDCodeFound := CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', lEdCode, [loCaseInsensitive]);

            if not EDCodeFound then
              EDCodeFound := (Length(Trim(lEdCode)) = 2) and
                CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', lEdCode[1]+ '0'+ lEdCode[2], [loCaseInsensitive]);

            if EDCodeFound then
            begin

              if not CO_E_D_CODES.Active then
                CO_E_D_CODES.Activate;

              if not CO_E_D_CODES.Locate('CL_E_DS_NBR;CO_NBR', VarArrayOf([DM_CLIENT.CL_E_DS['CL_E_DS_NBR'], DM_CLIENT.CO['CO_NBR']]), []) then
              begin
                 AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Client E/D is not set up as a Company E/D', sRejected]));
                 Inc(result);
                 AbortEx;
              end;

              if ((DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_MEMO_SIMPLE) or (DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_MEMO_ER_INSURANCE_PREMIUM)) then
              begin
               if pos('COBRA',UpperCase(DM_CLIENT.CL_E_DS['DESCRIPTION'])) > 0 then
               begin
                if loop <> TC_COBRACREDITTYPE then
                   continue;
               end
               else
                if loop = TC_COBRACREDITTYPE then
                   continue;
              end
              else
              begin
                if (loop <> TC_COBRACREDITTYPE) and (DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_MEMO_COBRA_CREDIT) then
                  continue;
                if (loop = TC_COBRACREDITTYPE) and (DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] <> ED_MEMO_COBRA_CREDIT) then
                  continue;
              end
            end
            else
              if loop = TC_COBRACREDITTYPE then
                Continue;

            ctx_UpdateWait('Processing...' + #13#13 +'Company:  #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '  "' + DM_COMPANY.CO.FieldByName('NAME').AsString + '"', i);
            if LookupOption = loByNumber then
            begin
              bEeFound := EE.FindKey([PadField(Trim(Copy2(s, 1, 6)), EE, 'CUSTOM_EMPLOYEE_NUMBER')]);
              if not bEeFound then
                bEeFound := EE.FindKey([PadField(StripLeadingZeroes(Trim(Copy2(s, 1, 6))), EE, 'CUSTOM_EMPLOYEE_NUMBER')]);
            end
            else
            if LookupOption = loByName then
            begin
              sLastName := Trim(Copy2(s, 7, 25));
              sFirstName := Trim(Fetch(sLastName));
              sLastName := Trim(sLastName);
              bEeFound := EE.FindKey([CL_PERSON.Lookup('FIRST_NAME;LAST_NAME', VarArrayOf([sFirstName, sLastName]), 'CL_PERSON_NBR'), EE_TERM_ACTIVE]);
              if not bEeFound then
                bEeFound := EE.FindKey([CL_PERSON.Lookup('FIRST_NAME;LAST_NAME', VarArrayOf([sFirstName, sLastName]), 'CL_PERSON_NBR')]);
            end
            else
            if LookupOption = loBySSN then
            begin
              sSSN := Trim(Copy2(s, 202, lSSN));
              if pSSN = 0 then
                 sSSN := Copy(sSSN, 1, 3)+ '-'+ Copy(sSSN, 4, 2)+ '-'+ Copy(sSSN, 6, 4);
              bEeFound := EE.FindKey([CL_PERSON.Lookup('SOCIAL_SECURITY_NUMBER', sSSN, 'CL_PERSON_NBR'), EE_TERM_ACTIVE]);
              if not bEeFound then
                bEeFound := EE.FindKey([CL_PERSON.Lookup('SOCIAL_SECURITY_NUMBER', sSSN, 'CL_PERSON_NBR')]);
            end
            else
            begin
              raise EInconsistentData.CreateHelp('Unexpected locate method', IDH_InconsistentData);
              bEeFound := False;
            end;
            if bEeFound then
            begin // EE's found
             if (loop <> TC_COBRACREDITTYPE) or
                ((loop = TC_COBRACREDITTYPE) and  (EE['CURRENT_TERMINATION_CODE'] = EE_TERM_INV_LAYOFF)) then
             begin
                    EeDbCustN := Trim(ConvertNull(EE['CUSTOM_EMPLOYEE_NUMBER'], '--'));
                    if (EE['PAY_FREQUENCY'] <> PR_BATCH['FREQUENCY']) and (not AutoCreateBatch) then
                    begin
                      AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Wrong Pay Frequency EE ' + Trim(Copy2(s, 7, 25)), sRejected]));
                      Inc(result);
                    end
                    else
                    begin
                      // trying to find suitable batch or create it
                      if (EE['PAY_FREQUENCY'] <> PR_BATCH['FREQUENCY']) and (AutoCreateBatch) then
                      begin
                        BatchFound := false;
                        PR_BATCH.First;
                        while not PR_BATCH.eof do
                        begin
                          if EE['PAY_FREQUENCY'] = PR_BATCH['FREQUENCY'] then
                          begin
                            BatchFound := true;
                            break;
                          end;
                          PR_BATCH.Next;
                        end;

                        // create a new batch if didn't find
                        if not BatchFound then
                        begin
                          PR_BATCH.Insert;
                          PR_BATCH.FieldByName('FREQUENCY').Value := EE.FieldByName('PAY_FREQUENCY').Value;
                          PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
                          PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
                          PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := ctx_PayrollCalculation.NextPeriodBeginDate;
                          PR_BATCH.FieldByName('PERIOD_END_DATE').Value := ctx_PayrollCalculation.NextPeriodEndDate;
                          PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
                          PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
                          PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'N';
                          PR_BATCH.Post;
                          AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'EE does not have any batches. One has been created', sImported]))
                        end;
                      end;

                      PR_CHECK.SetRange([EE['EE_NBR'], cCheckType], [EE['EE_NBR'], cCheckType]);
                      if PR_CHECK.RecordCount = 0 then
                      begin
                        PR_CHECK.Insert;
                        PR_CHECK['PR_NBR'] := PR['PR_NBR'];
                        PR_CHECK['CHECK_TYPE'] := cCheckType;
                        PR_CHECK['EE_NBR'] := EE['EE_NBR'];
                        ctx_PayrollCalculation.AssignCheckDefaults(0, 0);
                        PR_CHECK['CUSTOM_PR_BANK_ACCT_NUMBER'] := ctx_PayrollCalculation.GetCustomBankAccountNumber(PR_CHECK);
                        PR_CHECK['ABA_NUMBER'] := ctx_PayrollCalculation.GetABANumber;
                        PR_CHECK.UpdateBlobData('NOTES_NBR', Trim(Copy2(s, 249, 60, 60)));

                        PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
                        PR_CHECK.Post;
                        AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'EE does not have any checks. One has been created', sImported]))
                      end
                      else
                      begin
                        sSecN := Trim(Copy2(s, 90, 1));
                        if sSecN <> '' then
                        begin
                          if not (TryStrToInt(sSecN, iSeqNumber) and (iSeqNumber >= 1) and (iSeqNumber <= 99)) then
                          begin
                            AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Seq number is not valid ('+ sSecN+ ')', sRejected]));
                            Inc(result);
                          end
                          else
                          begin
                            CheckFound := True;
                            while PR_CHECK.RecordCount < iSeqNumber do
                            begin
                              CheckFound := False;
                              PR_CHECK.Append;
                              PR_CHECK['PR_NBR'] := PR['PR_NBR'];
                              PR_CHECK['CHECK_TYPE'] := cCheckType;
                              PR_CHECK['EE_NBR'] := EE['EE_NBR'];
                              ctx_PayrollCalculation.AssignCheckDefaults(0, 0);
                              PR_CHECK['CUSTOM_PR_BANK_ACCT_NUMBER'] := ctx_PayrollCalculation.GetCustomBankAccountNumber(PR_CHECK);
                              PR_CHECK['ABA_NUMBER'] := ctx_PayrollCalculation.GetABANumber;
                              //CHECK_COMMENTS
                              PR_CHECK.UpdateBlobData('NOTES_NBR', Trim(Copy2(s, 249, 60, 60)));

                              PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;

                              if ScheduledDDOnly then
                                PR_CHECK.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_YES;

                              PR_CHECK.Post;
                            end;
                            PR_CHECK.GotoSeqRecordNumber(iSeqNumber);

                            if CheckFound and (Trim(Copy2(s, 249, 60, 60)) <> '') then
                            begin
                              bpost:= PR_CHECK.BeforePost;
                              try
                                PR_CHECK.BeforePost := nil;
                                PR_CHECK.Edit;
                                PR_CHECK.UpdateBlobData('NOTES_NBR', Trim(Copy2(s, 249, 60, 60)));
                                PR_CHECK.Post;
                              finally
                                PR_CHECK.BeforePost := bpost;
                              end;
                              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Check comments have been updated', sImported]));
                            end;

                          end;
                        end
                        else
                        begin
                          PR_CHECK.GotoSeqRecordNumber(1);
                          if Trim(Copy2(s, 249, 60, 60)) <> '' then
                          begin
                              bpost:= PR_CHECK.BeforePost;
                              try
                                PR_CHECK.BeforePost := nil;
                                PR_CHECK.Edit;
                                PR_CHECK.UpdateBlobData('NOTES_NBR', Trim(Copy2(s, 249, 60, 60)));
                                PR_CHECK.Post;
                              finally
                                PR_CHECK.BeforePost := bpost;
                              end;
                              AppendToLog(Format(fOutFormat, [iLineNumber, EeDbCustN, 'Check comments have been updated', sImported]));
                          end;
                        end;
                      end;

                      Rate := DeleteStartingZeroes(Trim(Copy2(s, 54, 9)));
                      Hours := DeleteStartingZeroes(Trim(Copy2(s, 63, 8)));
                      Amount := DeleteStartingZeroes(Trim(Copy2(s, 81, 9)));
                      RateNumber:= DeleteStartingZeroes(Trim(Copy2(s, 217, 4)));
                      if (Hours <> '') and (StrToFloat(Hours) <> 0) then
                          AddLine(Rate, Hours, '', RateNumber,1);
                      if (Rate <> '') and (StrToFloat(Rate) <> 0) then
                          ARate := Rate
                      else
                          ARate := '';
                      if (Amount <> '') and (StrToFloat(Amount) <> 0) then
                          AddLine(ARate, '', Amount, '',2);
                    end
             end
             else
                AppendToLog(Format(fOutFormat, [iLineNumber, '--', ' EE does not have Involuntary Status # ' + Trim(Copy2(s, 1, 6)), sRejected]))
            end
            else
            begin
              Inc(result);
              if LookupOption = loByNumber then
                AppendToLog(Format(fOutFormat, [iLineNumber, '--', 'Could not find EE # ' + Trim(Copy2(s, 1, 6)), sRejected]))
              else
              if LookupOption = loByName then
                AppendToLog(Format(fOutFormat, [iLineNumber, '--', 'Could not find EE ' + Trim(Copy2(s, 7, 25)), sRejected]))
              else
                AppendToLog(Format(fOutFormat, [iLineNumber, '--', 'Could not find EE with SSN ' + Trim(Copy2(s, 202, lSSN)), sRejected]));
            end;
          end;
        end;
      finally
        PR_CHECK.CancelRange;
        PR_CHECK.Filtered := false;
        PR_CHECK.Filter := '';
        PR_CHECK.LoadState;
        EE.LoadState;
      end;
    finally
      DM_PAYROLL.PR_CHECK_LINES.EnableControls;
      ctx_EndWait;
    end;
  finally
     lMatrix.Free;
     listDiv.Free;
     listBr.Free;
     listDep.Free;
     listTm.Free;

    lWorkComp.Free;
    lList.Free;
    lCsfList.Free;
    LoadDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
    LogSetter.SetLogger(nil);
    ExtLogStrings.Destroy;

    AnswersForEachEE.Destroy;

  end;
end;

end.

