// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Import_Options;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  ExtCtrls, EvUtils, ISBasicClasses, EvUIUtils, EvUIComponents;

type
  TImport_Options = class(TForm)
    grpTwImport: TevGroupBox;
    spbImportSourceFile: TevSpeedButton;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    editImportFolder: TevEdit;
    Panel8: TevPanel;
    radioImportLookByNumber: TevRadioButton;
    radioImportLookByName: TevRadioButton;
    radioImportLookBySSN: TevRadioButton;
    evPanel2: TevPanel;
    radioImportMatchDbdtFull: TevRadioButton;
    radioImportMatchDbdtPartial: TevRadioButton;
    cbFourDigits: TevCheckBox;
    evPanel3: TevPanel;
    radioImportFormatFFF: TevRadioButton;
    radioImportFormatCSF: TevRadioButton;
    evBitBtn1: TevBitBtn;
    cbAutoImportJobCode: TevCheckBox;
    cbUseEmployeePayRates: TevCheckBox;
    procedure spbImportSourceFileClick(Sender: TObject);
  private
  public
  end;

var
  Import_Options: TImport_Options;

implementation

{$R *.DFM}

procedure TImport_Options.spbImportSourceFileClick(Sender: TObject);
begin
  editImportFolder.Text := RunFolderDialog('Please, select Import Files Folder.', editImportFolder.Text);
  BringToFront;
end;

end.
