// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CheckFinder;

interface

uses
  Windows,  Db,  Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, StdCtrls, Buttons, Controls, Classes, ExtCtrls, EvBasicUtils,
  Forms, SysUtils, EvUtils, SDataStructure, SFieldCodeValues, EvTypes,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, ISBasicClasses, isBasicUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit, isUIFashionPanel, EvCommonInterfaces,
  EvDataset, EvConsts, EvUIUtils, Dialogs;

type
  TCheckFinder = class(TForm)
    Panel1: TevPanel;
    wwdsFinder: TevDataSource;
    wwcsFinder: TevClientDataSet;
    wwdgFinder: TevDBGrid;
    editRunNbr: TevEdit;
    editEENbr: TevEdit;
    editCheckSerialNbr: TevEdit;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    bbtnFind: TevBitBtn;
    bbtnOK: TevBitBtn;
    bbtnCancel: TevBitBtn;
    dtpkBeginCheckDate: TevDateTimePicker;
    bbtnShow: TevBitBtn;
    evLabel1: TevLabel;
    dtpkEndCheckDate: TevDateTimePicker;
    pnlSearchCriteria: TisUIFashionPanel;
    pnlSearchResults: TisUIFashionPanel;
    procedure FormCreate(Sender: TObject);
    procedure bbtnFindClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure wwdgFinderDblClick(Sender: TObject);
    procedure wwdgFinderKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwdgFinderEnter(Sender: TObject);
    procedure wwdgFinderExit(Sender: TObject);
    procedure bbtnShowClick(Sender: TObject);
    procedure dtpkBeginCheckDateChange(Sender: TObject);
    procedure dtpkEndCheckDateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    CheckNumber: Integer;
    CheckDate: TDateTime;
    AdditionalFilter: string;
    DisableShowButton: Boolean;
    DisableOKButton: Boolean;
    VoidCheckControl: Boolean;
    ChecksList: TStringList;
  end;

var
  CheckFinder: TCheckFinder;

implementation

uses SPD_Check_Preview;

{$R *.DFM}

procedure TCheckFinder.FormCreate(Sender: TObject);
begin
  dtpkBeginCheckDate.Date := Date;
  dtpkBeginCheckDate.Checked := False;
  dtpkEndCheckDate.Date := Date;
  dtpkEndCheckDate.Checked := False;
  DisableShowButton := False;
  DisableOKButton := False;
end;

procedure TCheckFinder.bbtnFindClick(Sender: TObject);
var
  CoNbr: Integer;
  q: TExecDSWrapper;
  sFilter: String;
begin
  wwcsFinder.DisableControls;
  try
    CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
    q := TExecDSWrapper.Create('SelectCheckList');
    q.SetParam('CoNbr', CoNbr);

    sFilter := '';

    if editRunNbr.Text <> '' then
    begin
      AddStrValue(sFilter, 'P.RUN_NUMBER = :FLT_RUN_NUMBER', ' and ');
      q.SetParam('FLT_RUN_NUMBER', StrToInt(editRunNbr.Text));
    end;

    if editEENbr.Text <> '' then
    begin
      AddStrValue(sFilter, 'E.CUSTOM_EMPLOYEE_NUMBER = :FLT_CUSTOM_EMPLOYEE_NUMBER', ' and ');
      q.SetParam('FLT_CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(editEENbr.Text, ' ', 9));
    end;

    if editCheckSerialNbr.Text <> '' then
    begin
      AddStrValue(sFilter, 'C.PAYMENT_SERIAL_NUMBER = :FLT_PAYMENT_SERIAL_NUMBER', ' and ');
      q.SetParam('FLT_PAYMENT_SERIAL_NUMBER', editCheckSerialNbr.Text);
    end;

    if sFilter <> '' then
      sFilter := ' and ' + sFilter;
    q.SetMacro('FilterCondition', sFilter);

    sFilter := AdditionalFilter;
    if dtpkBeginCheckDate.Checked then
    begin
      AddStrValue(sFilter, 'P.CHECK_DATE between :FLT_CHECK_DATE_B and :FLT_CHECK_DATE_E', ' and ');
      q.SetParam('FLT_CHECK_DATE_B', dtpkBeginCheckDate.Date);
      q.SetParam('FLT_CHECK_DATE_E', dtpkEndCheckDate.Date);
    end
    else
    begin
      AddStrValue(sFilter, 'P.CHECK_DATE >= :FLT_CHECK_DATE', ' and ');
      q.SetParam('FLT_CHECK_DATE', GetBeginYear(Now));
    end;

    if sFilter <> '' then
      sFilter := ' and ' + sFilter;
    q.SetMacro('AdditionalFilter', sFilter);

    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      DataRequest(q.AsVariant);
      Open;
      CreateNewClientDataSet(Self, wwcsFinder, ctx_DataAccess.CUSTOM_VIEW);
      wwcsFinder.Data := Data;
      Close;
    end;
    wwcsFinder.FieldByName('PR_CHECK_NBR').Visible := False;
    wwcsFinder.FieldByName('PR_BATCH_NBR').Visible := False;
    wwcsFinder.FieldByName('PR_NBR').Visible := False;
    wwcsFinder.FieldByName('CHECK_STATUS').Visible := False;
    wwcsFinder.Last;
    while not wwcsFinder.Bof do
    begin
      wwcsFinder.Edit;
      wwcsFinder.FieldByName('Status_Calculate').Value := ReturnDescription(PayrollCheckStatus_ComboChoices,
        wwcsFinder.FieldByName('CHECK_STATUS').AsString[1]);
      wwcsFinder.Post;
      wwcsFinder.Prior;
    end;
    wwdgFinder.SetFocus;
  finally
    wwcsFinder.EnableControls;
    if not DisableOKButton then
      bbtnOK.Enabled := wwcsFinder.Active and (wwcsFinder.RecordCount > 0);
    if not DisableShowButton then
      bbtnShow.Enabled := wwcsFinder.Active and (wwcsFinder.RecordCount > 0);
  end;
end;

procedure TCheckFinder.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  s : String;
  Q : IevQuery;
begin
  if ModalResult = mrOK then
  begin
    if (not wwcsFinder.Active) or (wwcsFinder.BOF and wwcsFinder.EOF) then
      CanClose := False
    else
    begin
      CheckNumber := wwcsFinder.FieldByName('PR_CHECK_NBR').AsInteger;
      CheckDate := wwcsFinder.FieldByName('CHECK_DATE').AsDateTime;
      if VoidCheckControl and
        (ctx_AccountRights.Functions.GetState('BLOCK_VOIDING_CHECKS') = stEnabled) then
      begin
        s := 'select count(c.EE_DIRECT_DEPOSIT_NBR) from ' +
                'CL_E_DS a, PR_CHECK_LINES b, EE_SCHEDULED_E_DS c ' +
                   'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                      'a.SKIP_HOURS = ''N'' and a.CL_E_DS_NBR = b.CL_E_DS_NBR and ' +
                      'b.EE_SCHEDULED_E_DS_NBR = c.EE_SCHEDULED_E_DS_NBR and ' +
                      'c.EE_DIRECT_DEPOSIT_NBR is not null ' +
                      'and b.PR_CHECK_NBR = :PrCheckNbr ';

        Q := TevQuery.Create(s);
        Q.Params.AddValue('PrCheckNbr', CheckNumber);
        Q.Execute;
        if (Q.Result.Fields[0].AsInteger > 0) then
        begin
            EvMessage('This check contains direct deposits and you do not have privileges to void these checks.', mtWarning, [mbOk]);
            CanClose := False;
            exit;
        end;
      end;
      ChecksList := TStringList.Create;
      wwcsFinder.First;
      while not(wwcsFinder.Eof) do
      begin
        ChecksList.Add(wwcsFinder.FieldByName('PR_CHECK_NBR').AsString);
        wwcsFinder.Next;
      end;
    end;
  end;
end;

procedure TCheckFinder.wwdgFinderDblClick(Sender: TObject);
begin
  if bbtnOK.Enabled then
    bbtnOK.Click;
end;

procedure TCheckFinder.wwdgFinderKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    Key := 0;
    if bbtnOK.Enabled then
      bbtnOK.Click;
  end;
end;

procedure TCheckFinder.wwdgFinderEnter(Sender: TObject);
begin
  bbtnFind.Default := False;
end;

procedure TCheckFinder.wwdgFinderExit(Sender: TObject);
begin
  bbtnFind.Default := True;
end;

procedure TCheckFinder.bbtnShowClick(Sender: TObject);
var
  OldCheckLineNbr, OldCheckNbr, OldPrNbr: Integer;
  OldCheckLineRetrieveCondition, OldCheckRetrieveCondition, OldPrRetrieveCondition: String;
begin
  DM_PAYROLL.PR.DisableControls;
  DM_PAYROLL.PR_CHECK.DisableControls;
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  try
    ctx_StartWait;
    try
      OldCheckLineNbr := 0;
      OldCheckLineRetrieveCondition := '';
      if DM_PAYROLL.PR_CHECK_LINES.Active then
      begin
        if DM_PAYROLL.PR_CHECK_LINES.RecordCount > 0 then
          OldCheckLineNbr := DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
        OldCheckLineRetrieveCondition := DM_PAYROLL.PR_CHECK_LINES.OpenCondition;
      end;
      OldCheckNbr := 0;
      OldCheckRetrieveCondition := '';
      if DM_PAYROLL.PR_CHECK.Active then
      begin
        if DM_PAYROLL.PR_CHECK.RecordCount > 0 then
          OldCheckNbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
        OldCheckRetrieveCondition := DM_PAYROLL.PR_CHECK.OpenCondition;
      end;
      DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=' + wwcsFinder.FieldByName('PR_CHECK_NBR').AsString);
      DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
      DM_PAYROLL.PR.Activate;
      OldPrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
      OldPrRetrieveCondition := DM_PAYROLL.PR.OpenCondition;
      if not DM_PAYROLL.PR.Locate('PR_NBR', DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').Value, []) then
        DM_PAYROLL.PR.DataRequired('PR_NBR=' + wwcsFinder.FieldByName('PR_NBR').AsString);
    finally
      ctx_EndWait;
    end;
    Check_Preview := TCheck_Preview.Create(Self);
    try
      Check_Preview.ShowModal;
    finally
      Check_Preview.Free;
    end;
    if (OldPrNbr <> 0) or (OldPrRetrieveCondition <> '') then
    begin
      DM_PAYROLL.PR.CheckDSCondition(OldPrRetrieveCondition);
      if not DM_PAYROLL.PR.Active then
        DM_PAYROLL.PR.Open;
      DM_PAYROLL.PR.Locate('PR_NBR', OldPrNbr, []);
    end;
    if (OldCheckNbr <> 0) or (OldCheckRetrieveCondition <> '') then
    begin
      DM_PAYROLL.PR_CHECK.CheckDSCondition(OldCheckRetrieveCondition);
      if not DM_PAYROLL.PR_CHECK.Active then
        if Pos('CO_NBR', OldCheckRetrieveCondition) = 0 then  // because PR_CHECK doesn't have CO_NBR field we should select the right way to open PR_CHECK datase
          DM_PAYROLL.PR_CHECK.Open
        else
        begin
          DM_PAYROLL.PR_CHECK.PrepareLookups;
          ctx_DataAccess.OpenEEDataSetForCompany(DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR['CO_NBR'], 'PR_NBR = ' + IntToStr(OldPrNbr));
          DM_PAYROLL.PR_CHECK.UnPrepareLookups;
        end;
      if OldCheckNbr <> 0 then
        DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', OldCheckNbr, []);
    end;
    if (OldCheckLineNbr <> 0) or (OldCheckLineRetrieveCondition <> '') then
    begin
      DM_PAYROLL.PR_CHECK_LINES.CheckDSCondition(OldCheckLineRetrieveCondition);
      if not DM_PAYROLL.PR_CHECK_LINES.Active then
        DM_PAYROLL.PR_CHECK_LINES.Open;
      if OldCheckLineNbr <> 0 then
        DM_PAYROLL.PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', OldCheckLineNbr, []);
    end;
  finally
    DM_PAYROLL.PR.EnableControls;
    DM_PAYROLL.PR_CHECK.EnableControls;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
  end;
end;

procedure TCheckFinder.dtpkBeginCheckDateChange(Sender: TObject);
begin
  if dtpkBeginCheckDate.Checked then
  begin
    dtpkEndCheckDate.Enabled := True;
    if not dtpkEndCheckDate.Checked then
    begin
      dtpkEndCheckDate.Date := dtpkBeginCheckDate.Date;
      dtpkEndCheckDate.Checked := False;
    end
    else
    if dtpkEndCheckDate.Date < dtpkBeginCheckDate.Date then
      dtpkEndCheckDate.Date := dtpkBeginCheckDate.Date;
  end
  else
  begin
    dtpkEndCheckDate.Checked := False;
    dtpkEndCheckDate.Enabled := False;
  end;
end;

procedure TCheckFinder.dtpkEndCheckDateChange(Sender: TObject);
begin
  if not dtpkEndCheckDate.Checked then
  begin
    dtpkEndCheckDate.Date := dtpkBeginCheckDate.Date;
    dtpkEndCheckDate.Checked := False;
  end;
end;

initialization
  RegisterClass(TCHECKFINDER);

end.
