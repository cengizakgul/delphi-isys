object Import_Options: TImport_Options
  Left = 197
  Top = 229
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Please, Select Timeclock Import Options'
  ClientHeight = 222
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    602
    222)
  PixelsPerInch = 96
  TextHeight = 13
  object grpTwImport: TevGroupBox
    Left = 10
    Top = 8
    Width = 580
    Height = 161
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Timeclock import'
    TabOrder = 0
    object spbImportSourceFile: TevSpeedButton
      Left = 391
      Top = 24
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
        7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
        7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      OnClick = spbImportSourceFileClick
      ShortCut = 0
    end
    object evLabel1: TevLabel
      Left = 8
      Top = 24
      Width = 66
      Height = 13
      Caption = 'Imports Folder'
    end
    object evLabel3: TevLabel
      Left = 8
      Top = 56
      Width = 70
      Height = 13
      Caption = 'Look EE up by'
    end
    object evLabel4: TevLabel
      Left = 8
      Top = 80
      Width = 62
      Height = 13
      Caption = 'DBDT match'
    end
    object evLabel5: TevLabel
      Left = 8
      Top = 104
      Width = 48
      Height = 13
      Caption = 'File format'
    end
    object editImportFolder: TevEdit
      Left = 88
      Top = 24
      Width = 297
      Height = 21
      TabOrder = 0
    end
    object Panel8: TevPanel
      Left = 152
      Top = 56
      Width = 265
      Height = 17
      BevelOuter = bvNone
      TabOrder = 1
      object radioImportLookByNumber: TevRadioButton
        Left = 8
        Top = 0
        Width = 113
        Height = 17
        Caption = 'Custom #'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object radioImportLookByName: TevRadioButton
        Left = 120
        Top = 0
        Width = 89
        Height = 17
        Caption = 'Name'
        TabOrder = 1
      end
      object radioImportLookBySSN: TevRadioButton
        Left = 224
        Top = 0
        Width = 41
        Height = 17
        Caption = 'SSN'
        TabOrder = 2
      end
    end
    object evPanel2: TevPanel
      Left = 152
      Top = 80
      Width = 249
      Height = 17
      BevelOuter = bvNone
      TabOrder = 2
      object radioImportMatchDbdtFull: TevRadioButton
        Left = 8
        Top = 0
        Width = 113
        Height = 17
        Caption = 'Full (faster)'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object radioImportMatchDbdtPartial: TevRadioButton
        Left = 120
        Top = 0
        Width = 89
        Height = 17
        Caption = 'Partial (slower)'
        TabOrder = 1
      end
    end
    object cbFourDigits: TevCheckBox
      Left = 8
      Top = 128
      Width = 153
      Height = 17
      Caption = 'Use four digits for year'
      TabOrder = 4
    end
    object evPanel3: TevPanel
      Left = 152
      Top = 104
      Width = 265
      Height = 17
      BevelOuter = bvNone
      TabOrder = 3
      object radioImportFormatFFF: TevRadioButton
        Left = 8
        Top = 0
        Width = 113
        Height = 17
        Caption = 'Fixed positions'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object radioImportFormatCSF: TevRadioButton
        Left = 120
        Top = 0
        Width = 113
        Height = 17
        Caption = 'Comma delimited'
        TabOrder = 1
      end
    end
    object cbAutoImportJobCode: TevCheckBox
      Left = 224
      Top = 128
      Width = 183
      Height = 17
      Caption = 'Auto import job codes'
      TabOrder = 5
    end
    object cbUseEmployeePayRates: TevCheckBox
      Left = 440
      Top = 128
      Width = 137
      Height = 17
      Caption = 'Use employee pay rates'
      TabOrder = 6
    end
  end
  object evBitBtn1: TevBitBtn
    Left = 488
    Top = 184
    Width = 99
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
  end
end
