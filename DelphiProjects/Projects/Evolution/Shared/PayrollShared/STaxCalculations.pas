 // Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STaxCalculations;

interface

uses
  SysUtils,  SDataStructure, EvConsts, EvTypes, Variants, EvBasicUtils,
  EvExceptions, IsBasicUtils, EvDataset, EvCommonInterfaces, IsBaseClasses, evContext, DB, EvClientDataSet;

procedure CalcSUITaxes(CheckNumber, COSUINumber: Integer; SUI_Type_ER: Boolean; var SUITax, SUITaxWages, SUIGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI: TevClientDataSet; DisableYTDs: Boolean; DisableShortfalls: Boolean;
  APR_CHECK_2: TevClientDataSet);

procedure CalcLocalTax(CheckNumber, COLocalNumber: Integer; Local_Type_ER: Boolean; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean; DisableYTDs: Boolean; DisableShortfalls: Boolean);
function CheckHasTaxOverrides(CheckNbr: Integer): Boolean;

procedure CalcEmployeeLocalTax(CheckNumber, COLocalNumber: Integer; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean = False; DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False);

procedure CalcEmployerLocalTax(CheckNumber, COLocalNumber: Integer; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean = False; DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False);

procedure PreparePaTax(CheckNumber, COLocalNumber: Integer;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS: TDataSet;
  DisableYTDs: Boolean);

implementation

uses
  EvUtils, Math, SPD_DM_HISTORICAL_TAXES, ISKbmMemDataSet, SDataDictclient, EvDataAccessComponents, DateUtils, Types;

procedure GetEELocalTaxes(EENbr, LocalNbr, PrNbr: Integer; StartDate, EndDate: TDateTime; var Wages, Tax: Double);
var
  Q: IevQuery;
  SQL: String;
begin

//  SQL = 'select sum(L.LOCAL_TAXABLE_WAGE) TAX_WAGE, sum(L.LOCAL_TAX) TAX ' +

  SQL := 'select sum(L.LOCAL_TAXABLE_WAGE - '+

      'CONVERTNULLDOUBLE((select SUM(z.AMOUNT) from PR_CHECK_LINES z '+
      'join PR_CHECK_LINE_LOCALS x on x.PR_CHECK_LINES_NBR=z.PR_CHECK_LINES_NBR and x.EE_LOCALS_NBR=L.EE_LOCALS_NBR and x.EXEMPT_EXCLUDE=''C'' '+
      'where {AsOfNow<z>} and z.PR_CHECK_NBR = C.PR_CHECK_NBR),0) '+

      ') TAX_WAGE, sum(L.LOCAL_TAX) TAX '+

      'from PR_CHECK_LOCALS L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR and P.STATUS in (#STATUS) ' +
      'where C.EE_NBR=:EENbr and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
      'and L.EE_LOCALS_NBR=:RecordNbr and {AsOfNow<P>} and {AsOfNow<C>} ' +
      'and {AsOfNow<L>} ';

  Q := TevQuery.Create(SQL);

  Q.Params.AddValue('EENbr', EENbr);
  Q.Params.AddValue('RecordNbr', LocalNbr);
  Q.Params.AddValue('PayrollNbr', PrNbr);
  Q.Params.AddValue('BeginDate', StartDate);
  Q.Params.AddValue('EndDate', EndDate);
  Q.Macros.AddValue('Status', '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''');
  Wages := ConvertNull(Q.Result['TAX_WAGE'], 0);
  Tax := ConvertNull(Q.Result['TAX'], 0);

end;

procedure GetEELocalTaxesCalc(EENbr, EELocalNbr, COLocalNbr, SYLocalNbr, PrNbr, CheckNbr: Integer; StartDate, EndDate: TDateTime; var Wages, Tax, CalculatedTax: Double);
var
  Q, Q1, Q2, Q3: IevQuery;
  D: String;
  Additional: Double;
const
  LocalTemplateDetails = 'select C.PR_CHECK_NBR, sum(L.LOCAL_TAXABLE_WAGE - '+

      'CONVERTNULLDOUBLE((select SUM(z.AMOUNT) from PR_CHECK_LINES z '+
      'join PR_CHECK_LINE_LOCALS x on x.PR_CHECK_LINES_NBR=z.PR_CHECK_LINES_NBR and x.EE_LOCALS_NBR=L.EE_LOCALS_NBR and x.EXEMPT_EXCLUDE=''C'' '+
      'where {AsOfNow<z>} and {AsOfNow<x>} and z.PR_CHECK_NBR = C.PR_CHECK_NBR and T.SY_LOCALS_NBR not in (921, 922)),0) '+

      ') TAX_WAGE, sum(L.LOCAL_TAX) TAX, P.CHECK_DATE ' +
      'from PR_CHECK_LOCALS L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR and P.STATUS in (#STATUS) ' +
      'join EE_LOCALS E on E.EE_LOCALS_NBR=L.EE_LOCALS_NBR ' +
      'join CO_LOCAL_TAX T on T.CO_LOCAL_TAX_NBR=E.CO_LOCAL_TAX_NBR ' +
      'where C.EE_NBR=:EENbr and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
      'and L.EE_LOCALS_NBR=:RecordNbr and {AsOfNow<P>} and {AsOfNow<E>} and {AsOfNow<T>} and {AsOfNow<C>} and {AsOfNow<L>} and L.REPORTABLE=''Y'' ' +
      'and C.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
      'group by P.CHECK_DATE, C.PR_CHECK_NBR order by P.CHECK_DATE, C.PR_CHECK_NBR';
begin
  Wages := 0;
  Tax := 0;
  CalculatedTax := 0;

  Q := TevQuery.Create(LocalTemplateDetails);
  Q.Params.AddValue('EENbr', EENbr);
  Q.Params.AddValue('RecordNbr', EELocalNbr);
  Q.Params.AddValue('PayrollNbr', PrNbr);
  Q.Params.AddValue('BeginDate', StartDate);
  Q.Params.AddValue('EndDate', EndDate);
  Q.Params.AddValue('CheckNbr', CheckNbr);
  Q.Macros.AddValue('Status', '''' + PAYROLL_STATUS_PROCESSED + '''');
  Q.Result.First;
  while not Q.Result.Eof do
  begin

    D := Q.Result.FieldByName('CHECK_DATE').AsString;
    Wages := Wages + ConvertNull(Q.Result.FieldByName('TAX_WAGE').Value, 0);
    Tax := Tax + ConvertNull(Q.Result.FieldByName('TAX').Value, 0);

    Q1 := TevQuery.Create(
            ' select s.OVERRIDE_LOCAL_TAX_VALUE, s.PERCENTAGE_OF_TAX_WAGES, s.OVERRIDE_LOCAL_TAX_TYPE '+
            ' from EE_LOCALS s where s.EE_LOCALS_NBR = '+IntToStr(EELocalNbr) + ' and {AsOfDate<s>} '+
            ' and s.OVERRIDE_LOCAL_TAX_TYPE<>'''+OVERRIDE_VALUE_TYPE_NONE+''' and s.OVERRIDE_LOCAL_TAX_TYPE<>'''+OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT+''' '+
            ' and s.OVERRIDE_LOCAL_TAX_TYPE<>'''+OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT+''' ');
    Q1.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

    Q2 := TevQuery.Create('select s.SELF_ADJUST_TAX '+
            ' from CO_LOCAL_TAX s where s.CO_LOCAL_TAX_NBR = '+IntToStr(COLocalNbr)+' and {AsOfDate<s>}');
    Q2.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

    if Q2.Result.FieldByName('SELF_ADJUST_TAX').AsString = 'N' then
    begin
      CalculatedTax := CalculatedTax + ConvertNull(Q.Result.FieldByName('TAX').Value, 0);
    end
    else
    begin
      if Q1.Result.RecordCount <> 0 then
      begin
        if Q1.Result.FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT then
          CalculatedTax := CalculatedTax + ConvertNull(Q1.Result.FieldByName('OVERRIDE_LOCAL_TAX_VALUE').Value, 0)
        else if Q1.Result.FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT then
          CalculatedTax := CalculatedTax + ConvertNull(Q.Result.FieldByName('TAX_WAGE').Value, 0) * ConvertNull(Q1.Result.FieldByName('OVERRIDE_LOCAL_TAX_VALUE').Value, 0);
      end
      else
      begin
        Additional := 0;

        Q3 := TevQuery.Create(
                ' select s.OVERRIDE_LOCAL_TAX_VALUE '+
                ' from EE_LOCALS s where s.EE_LOCALS_NBR = '+IntToStr(EELocalNbr) + ' and {AsOfDate<s>} '+
                ' and s.OVERRIDE_LOCAL_TAX_TYPE='''+OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT+'''');
        Q3.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

        if Q3.Result.RecordCount > 0 then
          Additional := ConvertNull(Q3.Result['OVERRIDE_LOCAL_TAX_VALUE'], 0);

        Q3 := TevQuery.Create(
                ' select s.OVERRIDE_LOCAL_TAX_VALUE '+
                ' from EE_LOCALS s where s.EE_LOCALS_NBR = '+IntToStr(EELocalNbr) + ' and {AsOfDate<s>} '+
                ' and s.OVERRIDE_LOCAL_TAX_TYPE='''+OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT+'''');
        Q3.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

        if Q3.Result.RecordCount > 0 then
          Additional := Additional + ConvertNull(Q.Result.FieldByName('TAX_WAGE').Value, 0) * ConvertNull(Q3.Result['OVERRIDE_LOCAL_TAX_VALUE'], 0)/100;

        Q1 := TevQuery.Create('select s.TAX_AMOUNT, s.TAX_RATE '+
                ' from CO_LOCAL_TAX s where s.CO_LOCAL_TAX_NBR = '+IntToStr(COLocalNbr)+' and {AsOfDate<s>}');
        Q1.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

        if (Q1.Result.RecordCount <> 0) and ( not Q1.Result.FieldByName('TAX_AMOUNT').IsNull or not Q1.Result.FieldByName('TAX_RATE').IsNull) then
        begin
          if not Q1.Result.FieldByName('TAX_AMOUNT').IsNull then
            CalculatedTax := CalculatedTax + Q1.Result.FieldByName('TAX_AMOUNT').Value
          else if not Q1.Result.FieldByName('TAX_RATE').IsNull then
            CalculatedTax := CalculatedTax + Additional + ConvertNull(Q.Result.FieldByName('TAX_WAGE').Value, 0) * Q1.Result.FieldByName('TAX_RATE').Value;
        end
        else
        begin
          Q1 := TevQuery.Create('select s.TAX_RATE from SY_LOCALS s where s.SY_LOCALS_NBR = '+IntToStr(SYLocalNbr)+' and {AsOfDate<s>}');
          Q1.Params.AddValue('StatusDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);

          if Q1.Result.RecordCOunt <> 0 then
            CalculatedTax := CalculatedTax + ConvertNull(Q.Result.FieldByName('TAX_WAGE').Value, 0) * ConvertNull(Q1.Result.FieldByName('TAX_RATE').Value, 0);

          CalculatedTax := CalculatedTax + Additional;

        end;

      end;
    end;

    Q.Result.Next;
  end;
end;

function EndOfDay(const Value: TDateTime): TDateTime;
begin
  Result := Value + 1 - 1 / 1440;
end;

function GetEEStateNbr(SYStateNbr, EENbr, CONbr: Integer): Integer;
var
  State: string;
  COStateNbr: Integer;
begin
  State := HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'STATE');

  if not DM_COMPANY.CO_STATES.Active then
    DM_COMPANY.CO_STATES.Open;

  COStateNbr := DM_COMPANY.CO_STATES.Lookup('CO_NBR;STATE', VarArrayOf([CONbr, State]), 'CO_STATES_NBR');
  Result := DM_HISTORICAL_TAXES.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EENbr, COStateNbr]), 'EE_STATES_NBR');
end;

procedure CalcSUITaxes(CheckNumber, COSUINumber: Integer; SUI_Type_ER: Boolean; var SUITax, SUITaxWages, SUIGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI: TevClientDataSet; DisableYTDs: Boolean; DisableShortfalls: Boolean;
  APR_CHECK_2: TevClientDataSet);
var
  YTDTaxWages, YTDGrossWagesNonRecip, YTDTaxWagesNonRecip, YTDTax, TempTaxWages, YTDTaxOwed, MaxWage, Temp: Real;
  SYSUINumber, SYStateNumber, PRNumber, EENumber, CLPersonNbr, EEStateNumber, COStateNumber, HrsWorkedGroupNbr: Integer;
  SUIType, State, EDCodeType: String;
  CheckDate: TDateTime;
  PR_CHECK_SUI_2, PR_CHECK_2: TevClientDataSet;
  ees: array of Integer;
  iCount: Integer;
  Q, CS, SS: IevQuery;
  view: IevDataSet;

  function isEEinList(const Nbr: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(ees) do
      if ees[i] >= Nbr then
      begin
        Result := ees[i] = Nbr;
        Break;
      end;
  end;

  function CalcReciprocalWages(SUI_Type_ER: Boolean): Real;
  begin
    Result := 0;
    ctx_DataAccess.CUSTOM_VIEW.First;
    while not ctx_DataAccess.CUSTOM_VIEW.EOF do
    begin
      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').Value = COSUINumber then
      begin
        Result := Result + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_GROSS_WAGES').Value, 0);
        YTDGrossWagesNonRecip := YTDGrossWagesNonRecip + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_GROSS_WAGES').Value, 0);
        YTDTaxWagesNonRecip := YTDTaxWagesNonRecip + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
      end
      else
      if (PR_CHECK.FieldByName('RECIPROCATE_SUI').Value = 'Y') and (DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'), 'SY_STATES_NBR') <> SYStateNumber) then
      begin
        if (DM_CLIENT.CL.FieldByName('RECIPROCATE_SUI').Value = 'Y') and (DM_HISTORICAL_TAXES.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'SUI_RECIPROCATE') = RECIPROCATE_YES) then
        begin
          if not DM_HISTORICAL_TAXES.CO_SUI.Locate('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').Value, []) then
            raise EInconsistentData.CreateHelp('Failed to find CO_SUI with NBR= '+ ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').AsString, IDH_InconsistentData);
          if not DM_HISTORICAL_TAXES.SY_SUI.Locate('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI['SY_SUI_NBR'], []) then
            raise EInconsistentData.CreateHelp('Failed to find SY_SUI with NBR= '+ DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').AsString, IDH_InconsistentData);
          SUIType := DM_HISTORICAL_TAXES.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'];
          if ((SUIType = GROUP_BOX_ER) and (SUI_Type_ER)) or ((SUIType = GROUP_BOX_EE) and (not SUI_Type_ER)) then
            Result := Result + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
        end
        else
        if (DM_CLIENT.CL.FieldByName('RECIPROCATE_SUI').Value = 'Y') and (DM_HISTORICAL_TAXES.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber,
        'SUI_RECIPROCATE') = RECIPROCATE_WITH_REC_STATES) then
        begin
          if not DM_HISTORICAL_TAXES.CO_SUI.Locate('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').Value, []) then
            raise EInconsistentData.CreateHelp('Failed to find CO_SUI with NBR= '+ ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').AsString, IDH_InconsistentData);
          if not DM_HISTORICAL_TAXES.SY_SUI.Locate('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI['SY_SUI_NBR'], []) then
            raise EInconsistentData.CreateHelp('Failed to find SY_SUI with NBR= '+ DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').AsString, IDH_InconsistentData);
          SUIType := DM_HISTORICAL_TAXES.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'];
          if (((SUIType = GROUP_BOX_ER) and (SUI_Type_ER)) or ((SUIType = GROUP_BOX_EE) and (not SUI_Type_ER)))
          and (DM_HISTORICAL_TAXES.SY_STATES.Lookup('SY_STATES_NBR', DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'),
          'SY_STATES_NBR'), 'SUI_RECIPROCATE') <> 'N') then
            Result := Result + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
        end;
      end;
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

var
  RoundToNearestDollar: String;

const

  EESUITaxes = 'select #Columns ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where C.EE_NBR=:EENbr and ' +
      '#Filter ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      'P.PR_NBR<>:PrNbr and ' +
      'P.{PayrollProcessedClause} ' +
      'and {AsOfNow<S>} ' +
      'and {AsOfNow<C>} ' +
      'and {AsOfNow<P>} ' +
      '#Order';

  SuiTemplate = 'select sum(S.SUI_TAXABLE_WAGES) TAX_WAGE, sum(S.SUI_TAX) TAX, sum(S.SUI_GROSS_WAGES) GROSS_WAGE ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR and {AsOfNow<C>} ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'and {AsOfNow<P>} and P.STATUS in (#STATUS) ' +
      'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
      'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
      'join CO_SUI CS on S.CO_SUI_NBR=CS.CO_SUI_NBR and {AsOfNow<CS>} ' +
      'and CS.SY_SUI_NBR=(select S.SY_SUI_NBR from CO_SUI S where S.CO_SUI_NBR=:RecordNbr and {AsOfNow<S>}) ' +
      'join CO CO on CO.CO_NBR=CS.CO_NBR and (CO.CL_COMMON_PAYMASTER_NBR=(' +
      'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbrForPM and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
      'and {AsOfNow<M>}) and P.CO_NBR<>:CONbr or (P.CO_NBR=:CONbr and (C.EE_NBR=:EENbr and :Act=1 or C.EE_NBR<>:EENbr and :Act=2 or :Act=3))) ' +
      'and {AsOfNow<CO>} where S.PR_NBR=P.PR_NBR and #Filter {AsOfNow<S>}';

begin
  SUITax := 0;
  SUITaxWages := 0;
  SUIGrossWages := 0;
  with DM_HISTORICAL_TAXES do
  begin
    try
      SYSUINumber := CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'SY_SUI_NBR');
    except
      raise EInconsistentData.CreateHelp('Company SUI ' + DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'DESCRIPTION') +
        ' Effective Date is later than Check Date.', IDH_InconsistentData);
    end;
    SY_SUI.Locate('SY_SUI_NBR', SYSUINumber, []);
    try
      if Is_ER_SUI(SY_SUI.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').Value) <> SUI_Type_ER then
        Exit;
    except
      raise EInconsistentData.CreateHelp('Company SUI ' + CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'DESCRIPTION') +
        ' doesn''t have a reference to system SUI.', IDH_InconsistentData);
    end;

    COStateNumber := CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'CO_STATES_NBR');
// Get SY_STATE_NBR based on CO_STATE_NBR
    State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'STATE');
    SYStateNumber := SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');
// --------------------------------------
    CheckDate := PR.Lookup('PR_NBR', PR_CHECK.FieldByName('PR_NBR').Value, 'CHECK_DATE');
    PRNumber := PR_CHECK.FieldByName('PR_NBR').Value;
    EENumber := PR_CHECK.FieldByName('EE_NBR').Value;
    try
      EEStateNumber := EE_STATES.Lookup('EE_NBR;SUI_APPLY_CO_STATES_NBR', VarArrayOf([EENumber, COStateNumber]), 'EE_STATES_NBR');
    except
      raise EInconsistentData.CreateHelp('Employee #' + Trim(VarToStr(EE.Lookup('EE_NBR', EENumber, 'CUSTOM_EMPLOYEE_NUMBER'))) + ' does not have ' +
        State + ' state setup. Can not calculate SUI.', IDH_InconsistentData);
    end;

    if CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'SUI_EXEMPT') = 'Y' then
      Exit;
    if SY_SUI.FieldByName('SUI_ACTIVE').Value = 'N' then
      Exit;
    if CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'FINAL_TAX_RETURN') = 'Y' then  // Really means "SUI Inactive"
    begin
      Exit;
//      raise EInconsistentData.CreateHelp('Employee #' + Trim(VarToStr(EE.Lookup('EE_NBR', EENumber, 'CUSTOM_EMPLOYEE_NUMBER'))) + ' has the ' +
//        State + ' SUI setup which is not active on company level.', IDH_InconsistentData);
    end;

    if SUI_Type_ER then
    begin
      if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SUI_EXEMPT_EXCLUDE') = 'E' then
        Exit;
      if (SYSUINumber = 90) and (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SDI_EXEMPT_EXCLUDE') = 'E') then
        Exit;
    end
    else
    begin
      if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SUI_EXEMPT_EXCLUDE') = 'E' then
        Exit;
      if (SYSUINumber = 89) and (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') = 'E') then
        Exit;
    end;

    CLPersonNbr := StrToInt(DM_EMPLOYEE.EE.ShadowDataSet.CachedLookup(EENumber, 'CL_PERSON_NBR'));
    try
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'CL_PERSON_NBR;EE_NBR';
      DM_EMPLOYEE.EE.ShadowDataSet.SetRange([CLPersonNbr], [CLPersonNbr]);
      SetLength(ees, DM_EMPLOYEE.EE.ShadowDataSet.RecordCount);
      iCount := 0;
      while not DM_EMPLOYEE.EE.ShadowDataSet.Eof do
      begin
        ees[iCount] := DM_EMPLOYEE.EE.ShadowDataSet.FieldByName('EE_NBR').AsInteger;
        Inc(iCount);
        DM_EMPLOYEE.EE.ShadowDataSet.Next;
      end;
    finally
      DM_EMPLOYEE.EE.ShadowDataSet.CancelRange;
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'EE_NBR';
    end;

    HrsWorkedGroupNbr := 0;
    if SY_SUI.FieldByName('USE_MISC_TAX_RETURN_CODE').Value = 'Y' then // Base on Hours Worked
    begin
      DM_CLIENT.CL_E_D_GROUPS.Activate;
      HrsWorkedGroupNbr := ConvertNull(DM_CLIENT.CL_E_D_GROUPS.Lookup('NAME', 'Oregon', 'CL_E_D_GROUPS_NBR'), 0);
      DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
    end;

    PR_CHECK_LINES.First;
    with PR_CHECK_LINES do
    while not EOF do
    begin
      if (StrToIntDef(EE_STATES.ShadowDataSet.CachedLookup(FieldByName('EE_SUI_STATES_NBR').AsInteger, 'CO_STATES_NBR'),0) = COStateNumber)
      or (FieldByName('EE_SUI_STATES_NBR').IsNull and (StrToIntDef(EE_STATES.ShadowDataSet.CachedLookup(FieldByName('EE_STATES_NBR').AsInteger, 'SUI_APPLY_CO_STATES_NBR'),0) = COStateNumber))
      or (FieldByName('EE_SUI_STATES_NBR').IsNull and FieldByName('EE_STATES_NBR').IsNull
      and (StrToIntDef(EE_STATES.ShadowDataSet.CachedLookup(EE.Lookup('EE_NBR', PR_CHECK.FieldByName('EE_NBR').Value, 'HOME_TAX_EE_STATES_NBR'), 'SUI_APPLY_CO_STATES_NBR'),0) = COStateNumber)) then
      begin
        EDCodeType := CL_E_DS.ShadowDataSet.CachedLookup(PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType)
        or ((SYSUINumber = 118) and ((EDCodeType = 'M1') or (EDCodeType = 'M4')) and (CL_E_DS.ShadowDataSet.CachedLookup(PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION') = 'ERNEV')) then
        begin
          if ((SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([SYStateNumber, EDCodeType]), 'EXEMPT_EMPLOYEE_SUI') <> 'Y') and (not SUI_Type_ER))
          or ((SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([SYStateNumber, EDCodeType]), 'EXEMPT_EMPLOYER_SUI') <> 'Y') and (SUI_Type_ER)) then
            if ((CL_E_D_STATE_EXMPT_EXCLD.Lookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
            PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), 'EMPLOYEE_EXEMPT_EXCLUDE_SUI') <> GROUP_BOX_EXE) and (not SUI_Type_ER))
            or ((CL_E_D_STATE_EXMPT_EXCLD.Lookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
            PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), 'EMPLOYER_EXEMPT_EXCLUDE_SUI') <> GROUP_BOX_EXE) and (SUI_Type_ER))
            or (((SYSUINumber = 118) or (SYSUINumber = 223)) and (EDCodeType = 'E7') and (CL_E_DS.ShadowDataSet.CachedLookup(PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION') = 'ERNEV')) then
            begin
              if (EDCodeType = 'M1') or (EDCodeType = 'M4') then
                SUIGrossWages := SUIGrossWages - ConvertNull(FieldByName('AMOUNT').Value, 0)
              else
              if SY_SUI.FieldByName('USE_MISC_TAX_RETURN_CODE').Value = 'Y' then // Base on Hours Worked
              begin
                if (HrsWorkedGroupNbr = 0) and ((EDCodeType = ED_OEARN_SALARY) or (EDCodeType = ED_OEARN_REGULAR)
                or (EDCodeType = ED_OEARN_OVERTIME) or (EDCodeType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDCodeType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)
                or (EDCodeType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDCodeType = ED_OEARN_AVG_OVERTIME) or (EDCodeType = ED_OEARN_AVG_MULT_OVERTIME)
                or (EDCodeType = ED_OEARN_SEVERANCE_PAY)) or (HrsWorkedGroupNbr <> 0)
                and DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([HrsWorkedGroupNbr,
                PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), []) then
                  SUIGrossWages := SUIGrossWages + ConvertDeduction(EDCodeType, ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0))
              end
              else
                SUIGrossWages := SUIGrossWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
            end;
        end;
      end;
      Next;
    end;

    YTDTaxWages := 0;
    YTDGrossWagesNonRecip := 0;
    YTDTaxWagesNonRecip := 0;

    if (CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'TAX_RETURN_CODE') = 'N')
    or (CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'SUI_REIMBURSER') = GROUP_BOX_YES) then
      MaxWage := MaxInt
    else
    begin
      MaxWage := ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'MAXIMUM_WAGE'), 0);
      if CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'ALTERNATE_WAGE') = 'Y' then
        MaxWage := ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'ALTERNATE_TAXABLE_WAGE_BASE'), MaxWage);
    end;

    if not DisableYTDs then
    begin
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then
          Close;
        with TExecDSWrapper.Create(EESUITaxes) do
        begin
          SetMacro('Columns', 'S.PR_CHECK_SUI_NBR, S.CO_SUI_NBR, S.SUI_GROSS_WAGES, S.SUI_TAXABLE_WAGES, S.SUI_TAX, P.CHECK_DATE, P.PR_NBR');
          SetMacro('Order', 'order by S.PR_CHECK_SUI_NBR');
          SetMacro('Filter', '');
          SetParam('EENbr', EENumber);
          SetParam('PrNbr', PrNumber);
          SetParam('StartDate', GetBeginYear(CheckDate));
          SetParam('EndDate', GetEndYear(CheckDate));
          DataRequest(AsVariant);
        end;
        Open;
        Temp := CalcReciprocalWages(SUI_Type_ER);
        if (Temp = 0) and (not SUI_Type_ER) then
          Temp := CalcReciprocalWages(True);
        YTDTaxWages := YTDTaxWages + Temp;
      end;

  // This piece is to take into consideration multiple checks for one Person within the same payroll
      PR_CHECK_SUI_2 := TevClientDataSet.Create(nil);
      PR_CHECK_2 := TevClientDataSet.Create(nil);
      try
        PR_CHECK_SUI_2.CloneCursor(PR_CHECK_SUI, False);
        PR_CHECK_SUI_2.IndexFieldNames := 'PR_CHECK_NBR';
        PR_CHECK_2.CloneCursor(APR_CHECK_2, False);
        PR_CHECK_2.First;
        while (not PR_CHECK_2.EOF) and (PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNumber) do
        begin
          if (PR_CHECK_2.FieldByName('PR_NBR').AsInteger = PRNumber) and isEEinList(PR_CHECK_2.FieldByName('EE_NBR').AsInteger) then
          begin
            PR_CHECK_SUI_2.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);
            PR_CHECK_SUI_2.First;
            while not PR_CHECK_SUI_2.EOF do
            begin
              if (Is_ER_SUI(SY_SUI.Lookup('SY_SUI_NBR', CO_SUI.Lookup('CO_SUI_NBR', PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value,
              'SY_SUI_NBR'), 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) = SUI_Type_ER) and
              ((PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value = COSUINumber) or
              ((PR_CHECK_2.FieldByName('RECIPROCATE_SUI').Value = 'Y') and
              (DM_CLIENT.CL.FieldByName('RECIPROCATE_SUI').Value = 'Y') and
              (SY_SUI.Lookup('SY_SUI_NBR', CO_SUI.Lookup('CO_SUI_NBR', PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'), 'SY_STATES_NBR') <> SYStateNumber) and
              ((SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'SUI_RECIPROCATE') = RECIPROCATE_YES) or
              ((SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'SUI_RECIPROCATE') = RECIPROCATE_WITH_REC_STATES) and
              (SY_STATES.Lookup('SY_STATES_NBR', SY_SUI.Lookup('SY_SUI_NBR', CO_SUI.Lookup('CO_SUI_NBR',
              PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'), 'SY_STATES_NBR'), 'SUI_RECIPROCATE') <> 'N'))))) then
                YTDTaxWages := YTDTaxWages + ConvertNull(PR_CHECK_SUI_2.FieldByName('SUI_GROSS_WAGES').Value, 0);

              if (Is_ER_SUI(SY_SUI.Lookup('SY_SUI_NBR', CO_SUI.Lookup('CO_SUI_NBR', PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value,
              'SY_SUI_NBR'), 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) = SUI_Type_ER) then
              begin
                if PR_CHECK_SUI_2.FieldByName('CO_SUI_NBR').Value = COSUINumber then
                begin
                  YTDGrossWagesNonRecip := YTDGrossWagesNonRecip + ConvertNull(PR_CHECK_SUI_2.FieldByName('SUI_GROSS_WAGES').Value, 0);
                  YTDTaxWagesNonRecip := YTDTaxWagesNonRecip + ConvertNull(PR_CHECK_SUI_2.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
                end
              end;

              PR_CHECK_SUI_2.Next;
            end;
          end;
          PR_CHECK_2.Next;
        end;
      finally
        PR_CHECK_SUI_2.Free;
        PR_CHECK_2.Free;
      end;
  // End of multiple checks piece...

  // Get SUI tax wages for other companies that belong to the same Common Paymaster
      with TExecDSWrapper.Create(SuiTemplate) do
      begin
        SetMacro('Filter', '');
        SetParam('CONbr', PR.Lookup('PR_NBR', PRNumber, 'CO_NBR'));
        SetParam('CONbrForPM', PR.Lookup('PR_NBR', PRNumber, 'CO_NBR'));
        SetParam('CLPersonNbr', EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'));
        SetParam('RecordNbr', COSUINumber);
        SetParam('EENbr', EENumber);
        SetParam('Act', 2);
        SetParam('PayrollNbr', PRNumber);
        SetParam('BeginDate', GetBeginYear(CheckDate));
        SetParam('EndDate', GetEndYear(CheckDate));
        SetMacro('Status', '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''');

        if ctx_DataAccess.CUSTOM_VIEW.Active then
          ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;

        YTDTaxWages := YTDTaxWages + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('GROSS_WAGE').Value, 0);
      end;
    end;

    SUITaxWages := SUIGrossWages;                  
    if (YTDGrossWagesNonRecip > YTDTaxWagesNonRecip) and DoubleCompare(YTDGrossWagesNonRecip, coGreaterOrEqual, MaxWage) then
    begin
      if YTDGrossWagesNonRecip + SUITaxWages < YTDTaxWagesNonRecip then
        SUITaxWages := YTDGrossWagesNonRecip - YTDTaxWagesNonRecip + SUITaxWages
      else
        SUITaxWages := 0;
    end
    else
    if SUITaxWages + YTDTaxWages > MaxWage then
    begin
      if YTDTaxWages >= MaxWage then
        SUITaxWages := 0
      else
        SUITaxWages := MaxWage - YTDTaxWages;
    end;
    if Abs(SUITaxWages) > MaxWage then
      SUITaxWages := MaxWage * Sign(SUITaxWages);
    if not VarIsNull(SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'GLOBAL_RATE')) then
      SUITax := SUITaxWages * SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'GLOBAL_RATE')
    else
    try
      SUITax := SUITaxWages * CO_SUI.Lookup('CO_SUI_NBR', COSUINumber, 'RATE');
    except
      raise EInconsistentData.CreateHelp('SUI rate is not setup!', IDH_InconsistentData);
    end;
    if SUITax <> 0 then
      SUITax := SUITax + SUITax * ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'FUTURE_DEFAULT_RATE'{Additional Assessment %}), 0)
      + ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'FUTURE_MAXIMUM_WAGE'{Additional Assessment Amount}), 0);
    if PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_SUI') = 'Y' then
    begin
      SUITax := 0;
      Exit;
    end;
    if SUI_Type_ER then
    begin
      if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SUI_EXEMPT_EXCLUDE') = 'X' then
      begin
        SUITax := 0;
        Exit;
      end;
      if (SYSUINumber = 90) and (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SDI_EXEMPT_EXCLUDE') = 'X') then
      begin
        SUITax := 0;
        Exit;
      end;
    end
    else
    begin
      if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SUI_EXEMPT_EXCLUDE') = 'X' then
      begin
        SUITax := 0;
        Exit;
      end;
      if (SYSUINumber = 89) and (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') = 'X') then
      begin
        SUITax := 0;
        Exit;
      end;
    end;
    if not PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').IsNull then
      SUITax := PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value
    else
// Self-correcting SUI for non-reciprocal clients
    if (DM_CLIENT.CL.FieldByName('RECIPROCATE_SUI').Value = 'N') and not SUI_Type_ER then
    begin
      YTDTax := 0;
      YTDTaxOwed := 0;
      YTDTaxWages := 0;

      Q := TevQuery.Create(EESUITaxes);
      Q.Macros.AddValue('Columns', 'S.SUI_TAXABLE_WAGES, S.SUI_TAX, P.CHECK_DATE');
      Q.Params.AddValue('EENbr', EENumber);
      Q.Params.AddValue('PrNbr', PrNumber);
      Q.Params.AddValue('StartDate', GetBeginYear(CheckDate));
      Q.Params.AddValue('EndDate', GetEndYear(CheckDate));
      Q.Macros.AddValue('Order', '');
      Q.Macros.AddValue('Filter', 'S.CO_SUI_NBR=' + IntToStr(COSUINumber) + ' and');

      view := Q.Result;

      while not view.Eof do
      begin

        CS := TevQuery.Create('select RATE from CO_SUI where CO_SUI_NBR=' + IntToStr(COSUINumber)+' and {AsOfDate<CO_SUI>}');
        CS.Params.AddValue('StatusDate', view.FieldByName('CHECK_DATE').AsDateTime);

        SS := TevQuery.Create(' select GLOBAL_RATE,MAXIMUM_WAGE,FUTURE_DEFAULT_RATE,FUTURE_MAXIMUM_WAGE '+
                                  ' from SY_SUI where SY_SUI_NBR='+ IntToStr(SYSUINumber)+' and {AsOfDate<SY_SUI>}');
        SS.Params.AddValue('StatusDate', view.FieldByName('CHECK_DATE').AsDateTime);

        TempTaxWages := YTDTaxWages;
        YTDTaxWages := YTDTaxWages + ConvertNull(view.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
        YTDTax := YTDTax + ConvertNull(view.FieldByName('SUI_TAX').Value, 0);
        if not SY_SUI.FieldByName('GLOBAL_RATE').IsNull then
          Temp := SS.Result.FieldByName('GLOBAL_RATE').Value
        else
          Temp := ConvertNull(CS.Result.FieldByName('RATE').Value, 0);

        if (ConvertNull(SS.Result.FieldByName('MAXIMUM_WAGE').Value, 0) > 0)
        and (YTDTaxWages >= SS.Result.FieldByName('MAXIMUM_WAGE').Value) then
        begin
          YTDTaxWages := SS.Result.FieldByName('MAXIMUM_WAGE').Value;
          Temp := RoundTwo((YTDTaxWages - TempTaxWages) * Temp);
        end
        else
          Temp := RoundTwo(ConvertNull(view.FieldByName('SUI_TAXABLE_WAGES').Value, 0) * Temp);

        if Temp <> 0 then
          Temp := Temp + Temp * ConvertNull(SS.Result.FieldByName('FUTURE_DEFAULT_RATE'{Additional Assessment %}).Value, 0)
          + ConvertNull(SS.Result.FieldByName('FUTURE_MAXIMUM_WAGE'{Additional Assessment Amount}).Value, 0);
        YTDTaxOwed := YTDTaxOwed + Temp;
        view.Next;
      end;

      if not DisableShortfalls and (Abs(YTDTaxOwed - YTDTax) > TaxShortfallPickupLimit) then
        SUITax := SUITax + YTDTaxOwed - YTDTax;

    end;
  end;

  RoundToNearestDollar := ConvertNull(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', SYSUINumber, 'ROUND_TO_NEAREST_DOLLAR'), 'N');

  if (RoundToNearestDollar = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
    SUITax := RoundAll(SUITax, 0);

end;

{SR-200, SR-200.10}

procedure CalcLocalTax(CheckNumber, COLocalNumber: Integer; Local_Type_ER: Boolean; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean; DisableYTDs: Boolean; DisableShortfalls: Boolean);
var
  AdjLocalTaxWages, MiscAmount, YTDLocalTax, YTDLocalTaxWages, YTDCalculatedTax, TaxableAmount, TempTax,
  TaxCredit, TempYTDWages, TempYTDTax, TempYTDCalculatedTax, WageMinimum: Double;
  TotalOPTTaxes, PayrollTax, PeriodWage: Double;
  EDCodeType, EEStateMaritalStatus: String;
  CalcMethod: Char;
  TaxFreq, EELocalNumber, EELocalNumber2, SYLocalNumber, SYLocalMarStatusNbr,
  CONumber, CLPersonNbr, EENumber, PRNumber, EntityNbr, TmpNBR: Integer;
  EEStateNumber, SYStateNumber, SYStateMarStatusNbr: Integer;
  CheckDate: TDateTime;
  PR_CHECK_LOCALS_2, PR_CHECK_2: TevClientDataSet;
  ees: array of Integer;
  iCount: Integer;
  Query: IevQuery;
  Locations: IevDataSet;

  procedure GetTaxableAmount;
  var
    StandardDeduction: Real;
    PersonalExemptions,
    StdExemptionsAllow,
    WithholdingAllow: Integer;
  begin
    if ForTaxCalculator then
      EEStateMaritalStatus := PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_MARITAL_STATUS')
    else
      EEStateMaritalStatus := DM_HISTORICAL_TAXES.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'STATE_MARITAL_STATUS');

    SYStateMarStatusNbr := DM_HISTORICAL_TAXES.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), 'SY_STATE_MARITAL_STATUS_NBR');
    SYLocalMarStatusNbr := DM_HISTORICAL_TAXES.SY_LOCAL_MARITAL_STATUS.Lookup('SY_LOCALS_NBR;SY_STATE_MARITAL_STATUS_NBR', VarArrayOf([SYLocalNumber, SYStateMarStatusNbr]), 'SY_LOCAL_MARITAL_STATUS_NBR');

    StandardDeduction   := ConvertNull(DM_HISTORICAL_TAXES.SY_LOCAL_MARITAL_STATUS.Lookup('SY_LOCAL_MARITAL_STATUS_NBR', SYLocalMarStatusNbr, 'STANDARD_DEDUCTION_AMOUNT'), 0);
    PersonalExemptions  := ConvertNull(DM_HISTORICAL_TAXES.SY_STATE_MARITAL_STATUS.NewLookup('SY_STATE_MARITAL_STATUS_NBR', SYStateMarStatusNbr, 'PERSONAL_EXEMPTIONS'), 0);
    StdExemptionsAllow  := ConvertNull(DM_HISTORICAL_TAXES.SY_LOCAL_MARITAL_STATUS.Lookup('SY_LOCAL_MARITAL_STATUS_NBR', SYLocalMarStatusNbr, 'STANDARD_EXEMPTION_ALLOW'), 0);

    if ForTaxCalculator then
      WithholdingAllow := ConvertNull(PR_CHECK_STATES.NewLookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0)
    else
      WithholdingAllow := ConvertNull(DM_HISTORICAL_TAXES.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0);

    if SYStateNumber = 17 then // IN
    begin
      WithholdingAllow := ConvertNull(DM_HISTORICAL_TAXES.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0);    
      TaxableAmount := AdjLocalTaxWages * TaxFreq - StdExemptionsAllow - StandardDeduction * WithholdingAllow;
    end
    else
      TaxableAmount := AdjLocalTaxWages * TaxFreq - StandardDeduction * (WithholdingAllow - PersonalExemptions) - StdExemptionsAllow * PersonalExemptions;
  end;

  function IsSamePerson(const Nbr: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(ees) do
      if ees[i] >= Nbr then
      begin
        Result := ees[i] = Nbr;
        Break;
      end;
  end;

  procedure GetEeNumbersForPerson;
  begin
    try
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'CL_PERSON_NBR;EE_NBR';
      DM_EMPLOYEE.EE.ShadowDataSet.SetRange([CLPersonNbr], [CLPersonNbr]);
      SetLength(ees, DM_EMPLOYEE.EE.ShadowDataSet.RecordCount);
      iCount := 0;
      while not DM_EMPLOYEE.EE.ShadowDataSet.Eof do
      begin
        ees[iCount] := DM_EMPLOYEE.EE.ShadowDataSet.FieldByName('EE_NBR').AsInteger;
        Inc(iCount);
        DM_EMPLOYEE.EE.ShadowDataSet.Next;
      end;
    finally
      DM_EMPLOYEE.EE.ShadowDataSet.CancelRange;
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'EE_NBR';
    end;
  end;

  function GetLocalTaxWagesSameDayOtherPayrolls(const ACheckDate: TDateTime; const AEENumber, APRNumber, EELocalNumber: Integer): Currency;
  var
    Query: IevQuery;
  begin
    Query := TevQuery.Create(
      'select sum(L.LOCAL_TAXABLE_WAGE) '+
      'from PR_CHECK_LOCALS L '+
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR '+
      'join PR P on C.PR_NBR=P.PR_NBR '+
      'where C.EE_NBR=:EE_NBR and P.CHECK_DATE=:CHECK_DATE and P.PR_NBR<>:PR_NBR and L.EE_LOCALS_NBR=:EE_LOCALS_NBR '+
      'and {AsOfNow<C>} '+
      'and {AsOfNow<P>} '+
      'and {AsOfNow<L>} ');
    Query.Params.AddValue('EE_NBR', AEENumber);
    Query.Params.AddValue('CHECK_DATE', ACheckDate);
    Query.Params.AddValue('PR_NBR', APRNumber);
    Query.Params.AddValue('EE_LOCALS_NBR', EELocalNumber);
    Query.Execute;
    Result := Query.Result.Fields[0].AsCurrency;
  end;

  function GetLocalShortfall(const AEELocalNumber, ACheckNumber: Integer): Real;
  var
    Query: IevQuery;
  begin
    Query := TevQuery.Create('LocalShortfall');
    Query.Params.AddValue('EELocalNbr', AEELocalNumber);
    Query.Params.AddValue('CheckNbr', ACheckNumber);
    Query.Params.AddValue('CheckType', CHECK_TYPE2_REGULAR);
    Query.Result.Last;
    Result := ConvertNull(Query.Result.FieldByName('LOCAL_SHORTFALL').Value, 0);
  end;

  function PaExcluded(const CheckLineNbr, LocalNumber: Integer): Boolean;
  begin
    Result := False;
    if Locations.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([CheckLineNbr, LocalNumber]), []) then
      Result := Locations['EXCLUDED'] = 'Y';
  end;

  function GetGrossWages(const CheckLineNbr, LocalNumber: Integer): Double;
  begin
    if EDCodeType[1] = 'D' then
      Result := 0
    else
    begin
      Result := ConvertDeduction(EDCodeType, PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat);
    end;
  end;

  function GetWages(const CheckLineNbr, LocalNumber: Integer): Double;
  var
    Q: IevQuery;
  begin
    if EDCodeType[1] = 'D' then
    begin
        Q := TevQuery.Create('select 1 '+
        ' from SY_LOCALS l '+
        ' join SY_STATES s on s.SY_STATES_NBR=l.SY_STATES_NBR and s.STATE=''PA'' ' +
        ' where {AsOfNow<s>} and {AsOfNow<l>} and l.LOCAL_TYPE in (''R'',''N'') and SY_LOCALS_NBR='+IntToStr(SYLocalNumber));
        if Q.Result.Eof then
          Result := ConvertDeduction(EDCodeType, PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat)
        else
          Result := 0;
    end
    else
    begin
      Locations.IndexFieldNames := 'PR_CHECK_LINES_NBR;EE_LOCALS_NBR';
      Locations.First;
      if Locations.FindKey([CheckLineNbr, LocalNumber]) then
        Result := Locations.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat
      else
      begin
        Q := TevQuery.Create('select 1 '+
        ' from SY_LOCALS l '+
        ' join SY_STATES s on s.SY_STATES_NBR=l.SY_STATES_NBR and s.STATE=''PA'' ' +
        ' where {AsOfNow<s>} and {AsOfNow<l>} and l.LOCAL_TYPE in (''R'',''N'') and SY_LOCALS_NBR='+IntToStr(SYLocalNumber));
        if Q.Result.Eof then
          Result := ConvertDeduction(EDCodeType, PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat)
        else
          Result := 0;
      end;
    end;
  end;

var
  CL_E_DS_NBR, PR_CHECK_LINES_NBR: Integer;
  StartDate: TDateTime;
  OverrideAmount: Variant;
  WageMaximum: Real;
  TaxCap: Real;
  {TaxMax, TaxAll: Real;
  SALocals: IisListOfValues;
  SAF: Boolean;
  i: Integer;}
begin
// SR-200

  WageMinimum := 0;
  LocalTax := 0;
  LocalTaxWages := 0;
  LocalGrossWages := 0;
  AdjLocalTaxWages := 0;
  TaxCredit := 0;
  Locations := ctx_DataAccess.PrCheckLineLocations;
  Locations.CancelRange;
  Locations.First;

{  SALocals := TisListOfValues.Create;}

  EENumber := PR_CHECK.FieldByName('EE_NBR').AsInteger;

  SYLocalNumber := HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'SY_LOCALS_NBR');

{  SAF := HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'SELF_ADJUST_TAX') = 'Y';}

  if HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'EXEMPT') = 'Y' then
    Exit;

  if HT.IsEmployerLocalTax(SYLocalNumber) <> Local_Type_ER then
    Exit;

  EELocalNumber := HT.EE_LOCALS.Lookup('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EENumber, COLocalNumber]), 'EE_LOCALS_NBR');

  if HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'EXEMPT_EXCLUDE') = 'E' then
    Exit;

  CheckDate := EndOfDay(PR.FieldByName('CHECK_DATE').AsDateTime);
  TaxFreq := GetTaxFreq(PR_CHECK['TAX_FREQUENCY']);

  if not DisableYTDs then
  begin
    if HT.MinimumHoursWorked(SYLocalNumber) > 0 then
    begin
      case HT.MinimumHoursWorkedPer(SYLocalNumber) of
        FREQUENCY_TYPE_ANNUAL:
          if Get_EE_YTD(EENumber, GetBeginYear(CheckDate), CheckDate, 'H', 0) <= HT.MinimumHoursWorked(SYLocalNumber) then
            Exit;
        FREQUENCY_TYPE_QUARTERLY:
          if Get_EE_YTD(EENumber, GetBeginQuarter(CheckDate), CheckDate, 'H', 0) <= HT.MinimumHoursWorked(SYLocalNumber) then
            Exit;
        FREQUENCY_TYPE_MONTHLY:
          if Get_EE_YTD(EENumber, GetBeginMonth(CheckDate), CheckDate, 'H', 0) <= HT.MinimumHoursWorked(SYLocalNumber) then
            Exit;
        FREQUENCY_TYPE_WEEKLY:
          if Get_EE_YTD(EENumber, CheckDate, CheckDate, 'H', 0)  <= HT.MinimumHoursWorked(SYLocalNumber) * TaxFreq then
            Exit;
      end;
    end;
  end;

  PRNumber := PR_CHECK.FieldByName('PR_NBR').AsInteger;
  CONumber := PR.FieldByName('CO_NBR').AsInteger;
  
  DM_EMPLOYEE.EE.Locate('EE_NBR', EENumber, []);

  CLPersonNbr := StrToInt(DM_EMPLOYEE.EE.ShadowDataSet.CachedLookup(EENumber, 'CL_PERSON_NBR'));

  GetEeNumbersForPerson;

  try
    EEStateNumber := GetEEStateNbr(HT.LocalStatesNbr(SYLocalNumber), EENumber, CONumber);
  except
    raise EInconsistentData.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) +
        ' has a local tax, but does not have a corresponding state setup!', IDH_InconsistentData);
  end;

  SYStateNumber := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'CO_STATES_NBR'), 'SY_STATES_NBR');

  PR_CHECK_LINES.First;
  while not PR_CHECK_LINES.EOF do
  begin
    EntityNbr := 0;
    CL_E_DS_NBR := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger;
    PR_CHECK_LINES_NBR := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    EDCodeType := HT.CL_E_DS.ShadowDataSet.CachedLookup(CL_E_DS_NBR, 'E_D_CODE_TYPE');

    if HT.CL_E_DS.NewLookup('CL_E_DS_NBR', CL_E_DS_NBR, 'SY_STATE_NBR') = SYStateNumber then
      TaxCredit := TaxCredit + ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);

    if TypeIsTaxable(EDCodeType)
    and not PaExcluded(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger, EELocalNumber)
    and not (TypeIsSpecTaxedDed(EDCodeType) and not HT.IncludeInPretax(EELocalNumber))
    and not (HT.LocalSyExempt(SYLocalNumber, EDCodeType) and not HT.LocalClInclude(SYLocalNumber, CL_E_DS_NBR))
    and not HT.LocalClExempt(SYLocalNumber, CL_E_DS_NBR) then
    begin

      if (HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'DEDUCT') = DEDUCT_NEVER)
      and not PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES_NBR, EELocalNumber]), []) then
      begin

        if (not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull) or ( not DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').Isnull) then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull then
             TmpNBR := PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').AsInteger
          else
             TmpNBR := DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').AsInteger;

          if DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_NBR;CO_LOCAL_TAX_NBR',VarArrayOf([TmpNBR, COLocalNumber]), []) then
            EntityNbr := TmpNBR;
        end;

        if (EntityNbr = 0) and ((not PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').IsNull) or (not DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull)) then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').IsNull then
            TmpNBR := PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').AsInteger
          else
            TmpNBR := DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').AsInteger;

          if DM_COMPANY.CO_TEAM_LOCALS.Locate('CO_TEAM_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([TmpNBR, COLocalNumber]), []) then
            EntityNbr := TmpNBR;
        end;

        if (EntityNbr = 0) and ((not PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').IsNull) or (not DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull)) then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').IsNull then
            TmpNBR := PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').AsInteger
          else
            TmpNBR := DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').AsInteger;

          if DM_COMPANY.CO_DEPARTMENT_LOCALS.Locate('CO_DEPARTMENT_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([TmpNBR, COLocalNumber]), []) then
            EntityNbr := TmpNBR;
        end;

        if (EntityNbr = 0) and ((not PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').IsNull) or (not DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull)) then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').IsNull then
            TmpNBR := PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').AsInteger
          else
            TmpNBR := DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').AsInteger;

          if DM_COMPANY.CO_BRANCH_LOCALS.Locate('CO_BRANCH_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([TmpNBR, COLocalNumber]), []) then
            EntityNbr := TmpNBR;
        end;

        if (EntityNbr = 0) and ((not PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').IsNull) or (not DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull)) then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').IsNull then
            EntityNbr := PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').AsInteger
          else
            EntityNbr := DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').AsInteger;

          if not DM_COMPANY.CO_DIVISION_LOCALS.Locate('CO_DIVISION_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EntityNbr, COLocalNumber]), []) then
          begin
            PR_CHECK_LINES.Next;
            Continue;
          end;
        end;

        if DM_EMPLOYEE.EE.CO_JOBS_NBR.IsNull and
           PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull and
           PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').IsNull and
           DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull and
           PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').IsNull and
           DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull and
           PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').IsNull and
           DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull and
           PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').IsNull and
           DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull
        then begin
          PR_CHECK_LINES.Next;
          Continue;
        end;
      end
      else
      if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR',  PR_CHECK_LINES_NBR, []) and (HT.DeductEeLocal(EELocalNumber) = DEDUCT_NO_OVERRIDES) then
      begin
        PR_CHECK_LINES.Next;
        Continue;
      end;

      LocalGrossWages := LocalGrossWages + GetGrossWages(PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber);

      if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES_NBR, EELocalNumber]), []) then
      begin
        if PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString <> GROUP_BOX_EXEMPT then
        begin
          LocalTaxWages := LocalTaxWages + GetWages(PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber);
        end;
      end
      else
      begin
        LocalTaxWages := LocalTaxWages + GetWages(PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber);
      end;

      if (not HT.LocalSyExclude(SYLocalNumber, EDCodeType) or HT.LocalClInclude(SYLocalNumber, CL_E_DS_NBR)) and not HT.LocalClExclude(SYLocalNumber, CL_E_DS_NBR) then
      begin
        if not PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES_NBR, EELocalNumber]), []) then
        begin
          AdjLocalTaxWages := AdjLocalTaxWages + GetWages(PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber);
        end
        else
        begin
          if PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString = GROUP_BOX_INCLUDE then
            AdjLocalTaxWages := AdjLocalTaxWages + GetWages(PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber);
        end;
      end;
    end;
    PR_CHECK_LINES.Next;
  end;

  if HT.PercentageOfLocalTaxWages(EELocalNumber) <> 0 then
  begin
    LocalTaxWages := LocalTaxWages * HT.PercentageOfLocalTaxWages(EELocalNumber) * 0.01;
    AdjLocalTaxWages := AdjLocalTaxWages * HT.PercentageOfLocalTaxWages(EELocalNumber) * 0.01;
  end;
  

  PR_CHECK_LOCALS_2 := TevClientDataSet.Create(nil);
  try
    PR_CHECK_LOCALS_2.CloneCursor(PR_CHECK_LOCALS, False);
    
    if (PR_CHECK_LOCALS_2.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalNumber]),[])
        and (PR_CHECK_LOCALS_2.FieldByName('EXCLUDE_LOCAL').AsString = 'Y'))
        or HT.LocalExcludedOnEmployeeLevel(EELocalNumber) then
    begin
      LocalTax := 0;
      Exit;
    end;
  finally
    PR_CHECK_LOCALS_2.Free;
  end;

  if (not DisableYTDs) and (HT.WageMinimum(SYLocalNumber) > 0) then
  begin
    PeriodWage := 0;
    case HT.MinimumHoursWorkedPer(SYLocalNumber) of
      FREQUENCY_TYPE_ANNUAL:    PeriodWage := Get_EE_YTD(EENumber, GetBeginYear(CheckDate)   , CheckDate - 1, 'LW', EELocalNumber);
      FREQUENCY_TYPE_QUARTERLY: PeriodWage := Get_EE_YTD(EENumber, GetBeginQuarter(CheckDate), CheckDate - 1, 'LW', EELocalNumber);
      FREQUENCY_TYPE_MONTHLY:   PeriodWage := Get_EE_YTD(EENumber, GetBeginMonth(CheckDate)  , CheckDate - 1, 'LW', EELocalNumber);
      FREQUENCY_TYPE_WEEKLY:    PeriodWage := Get_EE_YTD(EENumber, CheckDate, CheckDate - 1  , 'LW', EELocalNumber) / TaxFreq;
    end;

    if HT.MinimumHoursWorkedPer(SYLocalNumber) in [FREQUENCY_TYPE_ANNUAL, FREQUENCY_TYPE_QUARTERLY, FREQUENCY_TYPE_MONTHLY, FREQUENCY_TYPE_WEEKLY] then
      PeriodWage := PeriodWage + GetLocalTaxWagesSameDayOtherPayrolls(CheckDate, EENumber, PRNumber, EELocalNumber);

    // This piece is to take into consideration multiple checks for one Person within the same payroll
    PR_CHECK_LOCALS_2 := TevClientDataSet.Create(nil);
    PR_CHECK_2 := TevClientDataSet.Create(nil);
    try
      PR_CHECK_LOCALS_2.CloneCursor(PR_CHECK_LOCALS, False);
      PR_CHECK_LOCALS_2.IndexFieldNames := 'PR_CHECK_NBR';
      PR_CHECK_2.CloneCursor(PR_CHECK, False);
      PR_CHECK_2.First;
      while (not PR_CHECK_2.EOF) and (PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNumber) do
      begin
        if (PR_CHECK_2.FieldByName('PR_NBR').AsInteger = PRNumber) and IsSamePerson(PR_CHECK_2.FieldByName('EE_NBR').AsInteger) then
        begin
          PR_CHECK_LOCALS_2.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);
          PR_CHECK_LOCALS_2.First;
          while not PR_CHECK_LOCALS_2.EOF do
          begin
            if COLocalNumber = HT.CoLocalTaxNbr(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').Value) then
            begin
              PeriodWage := PeriodWage + ConvertNull(PR_CHECK_LOCALS_2.FieldByName('LOCAL_TAXABLE_WAGE').Value, 0);
            end;
            PR_CHECK_LOCALS_2.Next;
          end;
        end;
        PR_CHECK_2.Next;
      end;
    finally
      PR_CHECK_LOCALS_2.Free;
      PR_CHECK_2.Free;
    end;
    // End of multiple checks piece...

    WageMinimum := HT.WageMinimum(SYLocalNumber);

    if PeriodWage + LocalTaxWages < WageMinimum then
    begin
      LocalTax := 0;
      Exit;
    end
    else
    begin
      if LocalTaxWages > PeriodWage + LocalTaxWages - WageMinimum then
      begin
        AdjLocalTaxWages := PeriodWage + LocalTaxWages - WageMinimum;
      end;
    end;

  end;

  YTDLocalTaxWages := 0;
  YTDLocalTax := 0;
  YTDCalculatedTax := 0;
  if not DisableYTDs then
  for iCount := 0 to High(ees) do
  begin
    EELocalNumber2 := ConvertNull(HT.EE_LOCALS.Lookup('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([ees[iCount], COLocalNumber]), 'EE_LOCALS_NBR'), 0);

    if HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'PAY_WITH_STATE'{Monthly Maximums}) = 'Y' then
      StartDate := GetBeginMonth(CheckDate)
    else
      StartDate := GetBeginYear(CheckDate);

    if HT.SelfAdjustLocalTax(COLocalNumber) then
      GetEELocalTaxesCalc(ees[iCount], EELocalNumber2, COLocalNumber, SYLocalNumber, PRNumber, CheckNumber, StartDate, CheckDate, TempYTDWages, TempYTDTax, TempYTDCalculatedTax)
    else
      GetEELocalTaxes(ees[iCount], EELocalNumber2, PRNumber, StartDate, CheckDate, TempYTDWages, TempYTDTax);
    YTDLocalTaxWages := YTDLocalTaxWages + TempYTDWages;
    YTDLocalTax := YTDLocalTax + TempYTDTax;
    YTDCalculatedTax := YTDCalculatedTax + TempYTDCalculatedTax;
  end;

// This piece is to take into consideration multiple checks for one Person within the same payroll
  PayrollTax := 0;
  PR_CHECK_LOCALS_2 := TevClientDataSet.Create(nil);
  PR_CHECK_2 := TevClientDataSet.Create(nil);
  try
    PR_CHECK_LOCALS_2.CloneCursor(PR_CHECK_LOCALS, False);
    PR_CHECK_LOCALS_2.IndexFieldNames := 'PR_CHECK_NBR';
    PR_CHECK_2.CloneCursor(PR_CHECK, False);
    PR_CHECK_2.First;
    while (not PR_CHECK_2.EOF) and (PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNumber) do
    begin
      if (PR_CHECK_2.FieldByName('PR_NBR').AsInteger = PRNumber) and IsSamePerson(PR_CHECK_2.FieldByName('EE_NBR').AsInteger) then
      begin
        PR_CHECK_LOCALS_2.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);
        PR_CHECK_LOCALS_2.First;
        while not PR_CHECK_LOCALS_2.EOF do
        begin
          if COLocalNumber = HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').Value, 'CO_LOCAL_TAX_NBR') then
          begin

            YTDLocalTaxWages := YTDLocalTaxWages + ConvertNull(PR_CHECK_LOCALS_2.FieldByName('LOCAL_TAXABLE_WAGE').Value, 0);
            YTDLocalTax := YTDLocalTax + ConvertNull(PR_CHECK_LOCALS_2.FieldByName('LOCAL_TAX').Value, 0);
            PayrollTax := PayrollTax + ConvertNull(PR_CHECK_LOCALS_2.FieldByName('LOCAL_TAX').Value, 0);

            YTDCalculatedTax := YTDCalculatedTax + ConvertNull(PR_CHECK_LOCALS_2.FieldByName('LOCAL_TAX').Value, 0);

          end;
          PR_CHECK_LOCALS_2.Next;
        end;
      end;
      PR_CHECK_2.Next;
    end;
  finally
    PR_CHECK_LOCALS_2.Free;
    PR_CHECK_2.Free;
  end;
  
// End of multiple checks piece...

  OverrideAmount := PR_CHECK_LOCALS.Lookup('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalNumber]), 'OVERRIDE_AMOUNT');
  if not VarIsNull(OverrideAmount) then
    LocalTax := OverrideAmount
  else
  if not VarIsNull(HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE'))
  and (HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
    LocalTax := HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE')
  else
  if ConvertNull(HT.CO_LOCAL_TAX.NewLookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_AMOUNT'), 0) <> 0 then
    LocalTax := HT.CO_LOCAL_TAX.NewLookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_AMOUNT')
  else
  begin
    if not VarIsNull(HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE'))
    and (HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT) then
      TaxRate := HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE')
    else
    if not VarIsNull(HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_RATE')) then
      TaxRate := HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_RATE')
    else
      TaxRate := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_RATE'), 0);
        
    CalcMethod := PadRight(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'CALCULATION_METHOD'), ' ', 1)[1];

    // The crazy PA OPT LST calculation
    if (HT.LocalType(SYLocalNumber) = LOCAL_TYPE_OCCUPATIONAL)
      and (DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'CO_STATES_NBR'), 'STATE') = 'PA') then
    begin
      if (PayrollTax <> 0) or ((UpperCase(ConvertNull(HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'EE_TAX_CODE'), '')) = 'X')
      and (YTDLocalTaxWages + LocalTaxWages < ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'WAGE_MAXIMUM'), 0))) then
        LocalTax := 0
      else
      begin
        if TaxRate > 10 then
        begin
          LocalTax := TaxRate / TaxFreq;
          if YTDLocalTax = 0 then
          begin
            // Find out if the tax was missed in any prior scheduled payrolls
            ctx_DataAccess.CUSTOM_VIEW.Close;
            with TExecDSWrapper.Create('CountMissedOPTPayrolls') do
            begin
              SetParam('EELocalNbr', EELocalNumber);
              SetParam('BeginDate', GetBeginYear(CheckDate));
              SetParam('EndDate', CheckDate);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            ctx_DataAccess.CUSTOM_VIEW.First;
            LocalTax := LocalTax * (ctx_DataAccess.CUSTOM_VIEW.FieldByName('PAYROLL_COUNT').AsInteger + 1{for current payroll});
          end
          else
          if YTDLocalTax + LocalTax >= TaxRate then
            LocalTax := TaxRate - YTDLocalTax;
        end
        else
          LocalTax := TaxRate;
        // See how much was paid for other OPT taxes for this EE during the year and make sure we don't exceed the total maximum
        TotalOPTTaxes := 0;
        HT.EE_LOCALS.First;
        while not HT.EE_LOCALS.EOF do
        begin
          if (HT.EE_LOCALS.FieldByName('EE_NBR').Value = PR_CHECK.FieldByName('EE_NBR').Value)
          and (HT.SY_STATES.Lookup('SY_STATES_NBR', HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',
            HT.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').Value, 'SY_STATES_NBR'), 'STATE') = 'PA')
          and (HT.LocalType(HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', HT.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').Value, 'SY_LOCALS_NBR')) = LOCAL_TYPE_OCCUPATIONAL) then
          begin
            GetEELocalTaxes(PR_CHECK.FieldByName('EE_NBR').AsInteger, HT.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger, PRNumber, GetBeginYear(CheckDate), CheckDate, TempYTDWages, TempYTDTax);
            TotalOPTTaxes := TotalOPTTaxes + TempYTDTax;
            if (TaxRate <= 10) and (HT.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value = EELocalNumber) and (TempYTDTax > 0) then
              LocalTax := LocalTax - TempYTDTax;
          end;
          HT.EE_LOCALS.Next;
        end;

        if TotalOPTTaxes + LocalTax > ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_MAXIMUM'), 0) then
          LocalTax := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_MAXIMUM'), 0) - TotalOPTTaxes;
      end
    end
    else
    if CalcMethod = CALC_METHOD_PERC_STATE then
    begin
      if DM_COMPANY.CO.FieldByName('CALCULATE_LOCALS_FIRST').Value = 'Y' then
        raise EInconsistentData.CreateHelp('You can not use local tax that is a percentage of state for a company that needs to calculate locals first!', IDH_InconsistentData);
      LocalTax := TaxRate * ConvertNull(PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_TAX'), 0);
    end
    else
    if CalcMethod = CALC_METHOD_PERC_MEDICARE_WAGES then
    begin
      if DM_COMPANY.CO.FieldByName('CALCULATE_LOCALS_FIRST').Value = 'Y' then
        raise EInconsistentData.CreateHelp('You can not use local tax that is a percentage of Medicare wages for a company that needs to calculate locals first!', IDH_InconsistentData);
      LocalTax := TaxRate * ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').Value, 0);
    end
    else
    if CalcMethod in [CALC_METHOD_GROSS, CALC_METHOD_GROSS_ADJ, CALC_METHOD_GROSS_MINUS_EXEMPTIONS] then
    begin
      case CalcMethod of
      CALC_METHOD_GROSS: LocalTax := TaxRate * AdjLocalTaxWages;
      CALC_METHOD_GROSS_ADJ:
        begin
          if ConvertNull(HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'MISCELLANEOUS_AMOUNT'), 0) <> 0 then
            MiscAmount := HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'MISCELLANEOUS_AMOUNT')
          else
          if ConvertNull(HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'MISCELLANEOUS_AMOUNT'), 0) <> 0 then
            MiscAmount := HT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'MISCELLANEOUS_AMOUNT')
          else
            MiscAmount := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'MISCELLANEOUS_AMOUNT'), 0);
          if ForTaxCalculator then
            LocalTax := TaxRate * (AdjLocalTaxWages - MiscAmount * ConvertNull(PR_CHECK_STATES.NewLookup('PR_CHECK_NBR;EE_STATES_NBR',
              VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0))
          else
            LocalTax := TaxRate * (AdjLocalTaxWages - MiscAmount *
              ConvertNull(HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0));
          if LocalTax < 0 then
            LocalTax := 0;
        end;
      CALC_METHOD_GROSS_MINUS_EXEMPTIONS:
        begin
          GetTaxableAmount;
          LocalTax := TaxRate * TaxableAmount / TaxFreq;
          if LocalTax < 0 then
            LocalTax := 0;
        end;
      end;
      // Logic for automatic adjustment of locals
      if HT.SelfAdjustLocalTax(COLocalNumber) and not DisableShortfalls and (YTDLocalTaxWages > 0) then
      begin

        if ((YTDLocalTaxWages - WageMinimum) > 0) and (YTDLocalTax <> YTDCalculatedTax) then
          LocalTax := LocalTax + YTDCalculatedTax - YTDLocalTax;

        if LocalTax < 0 then
          LocalTax := 0;
      end;
    end
    else
    if CalcMethod in [CALC_METHOD_TAX_TABLES, CALC_METHOD_TAX_TABLES_REVERSED] then
    begin
      GetTaxableAmount;
      TempTax := 0;
      if TaxableAmount < 0 then
        TaxableAmount := 0;
      if (ConvertNull(HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE'), 0) <> 0)
      and (HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT) then
        LocalTax := TaxRate * AdjLocalTaxWages
      else
      begin
        HT.SY_LOCAL_TAX_CHART.IndexFieldNames := 'SY_LOCAL_MARITAL_STATUS_NBR;MAXIMUM';
        HT.SY_LOCAL_TAX_CHART.SetRange([SYLocalMarStatusNbr], [SYLocalMarStatusNbr]);
        try
          HT.SY_LOCAL_TAX_CHART.First;
          while not HT.SY_LOCAL_TAX_CHART.EOF do
          begin
            if TaxableAmount <= HT.SY_LOCAL_TAX_CHART.FieldByName('MAXIMUM').Value then
            begin
              if CalcMethod = CALC_METHOD_TAX_TABLES then
                TempTax := TempTax + (TaxableAmount - HT.SY_LOCAL_TAX_CHART.FieldByName('MINIMUM').Value)
                  * HT.SY_LOCAL_TAX_CHART.FieldByName('PERCENTAGE').Value
              else
                TempTax := HT.SY_LOCAL_TAX_CHART.FieldByName('PERCENTAGE').Value;
              Break;
            end;
            if CalcMethod = CALC_METHOD_TAX_TABLES then
              TempTax := TempTax + (HT.SY_LOCAL_TAX_CHART.FieldByName('MAXIMUM').Value -
                HT.SY_LOCAL_TAX_CHART.FieldByName('MINIMUM').Value)
                * HT.SY_LOCAL_TAX_CHART.FieldByName('PERCENTAGE').Value
            else
              TempTax := HT.SY_LOCAL_TAX_CHART.FieldByName('PERCENTAGE').Value;
            HT.SY_LOCAL_TAX_CHART.Next;
          end;
        finally
          HT.SY_LOCAL_TAX_CHART.CancelRange;
          HT.SY_LOCAL_TAX_CHART.IndexFieldNames := '';
        end;

        if (SYLocalNumber = 215) and (CompareDate(CheckDate, EncodeDate(2015, 5, 31)) = GreaterThanValue) then // hardcoded logic for New York City Res
        begin
          if TaxableAmount > 500000 then
          begin
            if DM_HISTORICAL_TAXES.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), 'STATUS_TYPE') = 'M' then
              TempTax := TempTax + 1127.61
            else
            if DM_HISTORICAL_TAXES.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), 'STATUS_TYPE') = 'S' then
              TempTax := TempTax + 1133.31;
          end;
        end;
         
        if CalcMethod = CALC_METHOD_TAX_TABLES then
          LocalTax := TempTax / TaxFreq
        else
        begin
          if AdjLocalTaxWages * TaxFreq - TempTax > 0 then
            LocalTax := TaxRate * (AdjLocalTaxWages * TaxFreq - TempTax) / TaxFreq;
        end;
      end;
    end
    else // CALC_METHOD_FIXED
    if LocalTaxWages <> 0 then
    begin
      LocalTax := TaxRate;
      if ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'LOCAL_MINIMUM_WAGE'), 0) = 0 then
        LocalTaxWages := 0;
    end;
  end;

  if (not DisableYTDs)
  and (HT.MinimumHoursWorkedPer(SYLocalNumber) in [FREQUENCY_TYPE_ANNUAL, FREQUENCY_TYPE_QUARTERLY, FREQUENCY_TYPE_MONTHLY])
  and (ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'LOCAL_MINIMUM_WAGE'), 0) > 0) then
  begin
    StartDate := CheckDate;
    case HT.MinimumHoursWorkedPer(SYLocalNumber) of
      FREQUENCY_TYPE_ANNUAL:    StartDate := GetBeginYear(CheckDate);
      FREQUENCY_TYPE_QUARTERLY: StartDate := GetBeginQuarter(CheckDate);
      FREQUENCY_TYPE_MONTHLY:   StartDate := GetBeginMonth(CheckDate);
    end;
    GetEELocalTaxes(PR_CHECK.FieldByName('EE_NBR').Value, EELocalNumber, PRNumber, StartDate, CheckDate, TempYTDWages, TempYTDTax);
    if (TempYTDWages + LocalTaxWages <= HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'LOCAL_MINIMUM_WAGE')) or (TempYTDTax > EELocalNumber) then
    begin
      LocalTax := 0;
      OverrideAmount := PR_CHECK_LOCALS.Lookup('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalNumber]), 'OVERRIDE_AMOUNT');
      LocalTax := ConvertNull(OverrideAmount, LocalTax);
      Exit;
    end;
  end;

  if ConvertNull(HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE'), 0) <> 0 then
  begin
    if HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT then
      LocalTax := LocalTax + HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE')
    else
    if HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT then
      LocalTax := LocalTax + HT.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE') * AdjLocalTaxWages * 0.01;
  end;

  if (HT.LocalType(SYLocalNumber) <> LOCAL_TYPE_OCCUPATIONAL)
    or (DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'CO_STATES_NBR'), 'STATE') <> 'PA') then
  begin
    WageMaximum := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'WAGE_MAXIMUM'), 0);
    if (WageMaximum <> 0) and (YTDLocalTaxWages + LocalTaxWages > WageMaximum) then
    begin
      if LocalTaxWages <> 0 then
        LocalTax := LocalTax * (WageMaximum - YTDLocalTaxWages) / LocalTaxWages
      else
        LocalTax := 0;
      LocalTaxWages := WageMaximum - YTDLocalTaxWages;
    end
    else
    begin
      if (YTDLocalTax + LocalTax > ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_MAXIMUM'), 0))
      and (ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_MAXIMUM'), 0) <> 0) then
      begin
        LocalTax := HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_MAXIMUM') - YTDLocalTax;
        Exit;
      end;
      TaxCap := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'WEEKLY_TAX_CAP'), 0);
      if (TaxCap <> 0) and (LocalTax > TaxCap * TaxFreq) then
        LocalTax := TaxCap * TaxFreq;
    end;
  end;

  if PR_CHECK_LOCALS.Lookup('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalNumber]), 'EXCLUDE_LOCAL') = 'Y' then
  begin
    LocalTax := 0;
    Exit;
  end;

  if not DisableShortfalls and (DM_COMPANY.CO.FieldByName('MAKE_UP_TAX_DEDUCT_SHORTFALLS').Value = 'Y') then
    LocalTax := LocalTax + GetLocalShortfall(EELocalNumber, CheckNumber);

{  if SAF and (SALocals.Count > 0) then
  begin
    TaxAll := 0;
    TaxMax := 0;
    for i := 0 to SALocals.Count - 1 do
    begin
      TaxAll := TaxAll + SALocals.Values[i].Value;
      if SALocals.Values[i].Value > TaxMax then
        TaxMax := SALocals.Values[i].Value;
    end;
    if TaxMax < LocalTax then
      TaxMax := LocalTax;
    if (TaxAll + LocalTax) > TaxMax then
    begin
      LocalTax := TaxMax - TaxAll;
      if LocalTax < 0 then
        LocalTax := 0;
    end;
  end;
}

end;

{SR-60, SR-60.10, SR-210, SR-220}

function CheckHasTaxOverrides(CheckNbr: Integer): Boolean;
begin
  Result := False;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
// Look at check
    if Active then Close;
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'OR_CHECK_FEDERAL_VALUE, OR_CHECK_OASDI, OR_CHECK_MEDICARE, OR_CHECK_EIC, '
        + 'OR_CHECK_BACK_UP_WITHHOLDING, FEDERAL_SHORTFALL, OR_CHECK_FEDERAL_TYPE');
      SetMacro('TableName', 'PR_CHECK');
      SetMacro('NbrField', 'PR_CHECK');
      SetParam('RecordNbr', CheckNbr);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    if (not FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull and (FieldByName('OR_CHECK_FEDERAL_TYPE').AsString[1]
      in [OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT]))
      or (not FieldByName('OR_CHECK_OASDI').IsNull) or (not FieldByName('OR_CHECK_MEDICARE').IsNull)
      or (not FieldByName('OR_CHECK_EIC').IsNull) or (not FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').IsNull) then
    begin
      if ConvertNull(FieldByName('FEDERAL_SHORTFALL').Value, 0) > 0 then
        Result := False
      else
        Result := True;
      Exit;
    end;
    Close;

// Look at check states
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'STATE_OVERRIDE_VALUE, STATE_OVERRIDE_TYPE, STATE_SHORTFALL, EE_SDI_OVERRIDE, EE_SDI_SHORTFALL');
      SetMacro('TableName', 'PR_CHECK_STATES');
      SetMacro('NbrField', 'PR_CHECK');
      SetParam('RecordNbr', CheckNbr);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not EOF do
    begin
      if (not FieldByName('STATE_OVERRIDE_VALUE').IsNull and (FieldByName('STATE_OVERRIDE_TYPE').AsString[1]
        in [OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT]))
        or (not FieldByName('EE_SDI_OVERRIDE').IsNull) then
      begin
        if (ConvertNull(FieldByName('STATE_SHORTFALL').Value, 0) > 0) or (ConvertNull(FieldByName('EE_SDI_SHORTFALL').Value, 0) > 0) then
          Result := False
        else
          Result := True;
        Exit;
      end;
      Next;
    end;
    Close;

// Look at check SUI
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'OVERRIDE_AMOUNT');
      SetMacro('TableName', 'PR_CHECK_SUI');
      SetMacro('NbrField', 'PR_CHECK');
      SetParam('RecordNbr', CheckNbr);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not EOF do
    begin
      if not FieldByName('OVERRIDE_AMOUNT').IsNull then
      begin
        Result := True;
        Exit;
      end;
      Next;
    end;
    Close;

// Look at check locals
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'OVERRIDE_AMOUNT, LOCAL_SHORTFALL');
      SetMacro('TableName', 'PR_CHECK_LOCALS');
      SetMacro('NbrField', 'PR_CHECK');
      SetParam('RecordNbr', CheckNbr);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not EOF do
    begin
      if not FieldByName('OVERRIDE_AMOUNT').IsNull then
      begin
        if ConvertNull(FieldByName('LOCAL_SHORTFALL').Value, 0) > 0 then
          Result := False
        else
          Result := True;
        Exit;
      end;
      Next;
    end;
    Close;
  end;
end;


procedure PreparePaTax(CheckNumber, COLocalNumber: Integer;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS: TDataSet;
  DisableYTDs: Boolean);
var
  TaxRate: Real;
  EDCodeType, LocalType: String;
  TaxFreq, EELocalNumber, SYLocalNumber,
  CONumber, EENumber, EntityNbr: Integer;
  EEStateNumber, SYStateNumber, CoStatesNumber: Integer;
  CheckDate: TDateTime;
  MinimumHoursWorkedPer: AnsiChar;
  CL_E_DS_NBR, PR_CHECK_LINES_NBR: Integer;
  MinimumHoursWorked: Real;
  Source: Integer;
  Q1, Q2, Q3, Q4, Q5: IevQuery;
  LocationLevel: String;
  DbdtNbr, no, LocationNbr: Integer;
  IsResidential: Boolean;
  Deduct, Deductions, TaxableWages, TotalDeductions: Double;

  function SYExempt: Boolean;
  begin
    if DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'PRINT_RETURN_IF_ZERO' {Follow State E/D exemptions}) = 'Y' then
      Result := (DM_HISTORICAL_TAXES.SY_STATE_EXEMPTIONS.NewLookup('SY_STATES_NBR;E_D_CODE_TYPE',
        VarArrayOf([DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR',
        SYLocalNumber, 'SY_STATES_NBR'), EDCodeType]), 'EXEMPT_STATE') = 'E')
    else
      Result := (DM_HISTORICAL_TAXES.SY_LOCAL_EXEMPTIONS.Lookup('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([SYLocalNumber, EDCodeType]), 'EXEMPT') = 'E');
  end;

var
  Locations, LocationsM: IevDataset;
  Flag: Boolean;
  CO_DIVISION_NBR, CO_BRANCH_NBR, CO_DEPARTMENT_NBR, CO_TEAM_NBR, CO_JOBS_NBR: Variant;
  WAO: String;
begin

  Deductions := 0;
  TaxableWages := 0;

  LocationsM := ctx_DataAccess.PrCheckLineLocationsM;
  LocationsM.CancelRange;
  LocationsM.First;

  LocationsM.IndexFieldNames := 'PR_CHECK_NBR;CO_LOCAL_TAX_NBR';
  LocationsM.First;

  Locations := ctx_DataAccess.PrCheckLineLocations;
  Locations.CancelRange;
  Locations.First;

  Locations.IndexFieldNames := 'PR_CHECK_NBR;CO_LOCAL_TAX_NBR';
  Locations.First;

  if not Assigned(PR_CHECK) then
  begin
    Q1 := TevQuery.Create('select a.* from PR_CHECK a where {AsOfNow<a>} and a.PR_CHECK_NBR=:CheckNumber');
    Q1.Params.AddValue('CheckNumber', CheckNumber);
    PR_CHECK := Q1.Result.VCLDataset;

    Q2 := TevQuery.Create('select a.* from PR a where {AsOfNow<a>} and a.PR_NBR=:PrNbr');
    Q2.Params.AddValue('PrNbr', PR_CHECK['PR_NBR']);
    PR := Q2.Result.VCLDataset;

    Q3 := TevQuery.Create('select a.* from PR_CHECK_LINES a where {AsOfNow<a>} and a.PR_CHECK_NBR=:CheckNumber');
    Q3.Params.AddValue('CheckNumber', CheckNumber);
    PR_CHECK_LINES := Q3.Result.VCLDataset;

    Q4 := TevQuery.Create('select l.* from PR_CHECK_LINES c join PR_CHECK_LINE_LOCALS l on l.PR_CHECK_LINES_NBR=c.PR_CHECK_LINES_NBR where {AsOfNow<c>} and c.PR_CHECK_NBR=:CheckNumber and {AsOfNow<l>} ');
    Q4.Params.AddValue('CheckNumber', CheckNumber);
    PR_CHECK_LINE_LOCALS := Q4.Result.VCLDataset;

    DM_COMPANY.CO_DIVISION_LOCALS.DataRequired('ALL');
    DM_COMPANY.CO_BRANCH_LOCALS.DataRequired('ALL');
    DM_COMPANY.CO_DEPARTMENT_LOCALS.DataRequired('ALL');
    DM_COMPANY.CO_TEAM_LOCALS.DataRequired('ALL');
    DM_COMPANY.CO_JOBS_LOCALS.DataRequired('ALL');

  end;

  EENumber := PR_CHECK.FieldByName('EE_NBR').AsInteger;

  SYLocalNumber := DM_HISTORICAL_TAXES.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'SY_LOCALS_NBR');

  if DM_HISTORICAL_TAXES.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'EXEMPT') = 'Y' then
    Exit;

  if HT.LocalTaxType(SYLocalNumber) = GROUP_BOX_ER then
    Exit;

  EELocalNumber := DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EENumber, COLocalNumber]),
    'EE_LOCALS_NBR');

  if DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'EXEMPT_EXCLUDE') = 'E' then
    Exit;

  CheckDate := EndOfDay(PR.FieldByName('CHECK_DATE').AsDateTime);
  TaxFreq := GetTaxFreq(PR_CHECK['TAX_FREQUENCY']);

  MinimumHoursWorkedPer := String(ConvertNull(DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'MINIMUM_HOURS_WORKED_PER'), ' '))[1];

  if not DisableYTDs then
  begin
    MinimumHoursWorked := ConvertNull(DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'MINIMUM_HOURS_WORKED'), 0);
    if MinimumHoursWorked > 0 then
    begin
      case MinimumHoursWorkedPer of
        FREQUENCY_TYPE_ANNUAL:
          if Get_EE_YTD(EENumber, GetBeginYear(CheckDate), CheckDate, 'H', 0) <= MinimumHoursWorked then
            Exit;
        FREQUENCY_TYPE_QUARTERLY:
          if Get_EE_YTD(EENumber, GetBeginQuarter(CheckDate), CheckDate, 'H', 0) <= MinimumHoursWorked then
            Exit;
        FREQUENCY_TYPE_MONTHLY:
          if Get_EE_YTD(EENumber, GetBeginMonth(CheckDate), CheckDate, 'H', 0) <= MinimumHoursWorked then
            Exit;
        FREQUENCY_TYPE_WEEKLY:
          if Get_EE_YTD(EENumber, CheckDate, CheckDate, 'H', 0)  <= MinimumHoursWorked * TaxFreq then
            Exit;
      end;
    end;
  end;

  CONumber := PR.FieldByName('CO_NBR').AsInteger;

  DM_EMPLOYEE.EE.Locate('EE_NBR', EENumber, []);

  try
    EEStateNumber := GetEEStateNbr(HT.LocalStatesNbr(SYLocalNumber), EENumber, CONumber);
  except
    raise EInconsistentData.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) +
        ' has a local tax, but does not have a corresponding state setup!', IDH_InconsistentData);
  end;

  CoStatesNumber := DM_HISTORICAL_TAXES.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'CO_STATES_NBR');

  SYStateNumber := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNumber, 'SY_STATES_NBR');

  if DM_HISTORICAL_TAXES.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'STATE') <> 'PA' then
    Exit;

  LocalType := DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'LOCAL_TYPE');

  if (LocalType <> LOCAL_TYPE_SCHOOL)
  and (LocalType <> LOCAL_TYPE_INC_RESIDENTIAL)
  and (LocalType <> LOCAL_TYPE_INC_NON_RESIDENTIAL) then
    Exit;

  IsResidential := LocalType <> LOCAL_TYPE_INC_NON_RESIDENTIAL;

  if not VarIsNull(DM_HISTORICAL_TAXES.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE'))
  and (DM_HISTORICAL_TAXES.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_TYPE') = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT) then
    TaxRate := DM_HISTORICAL_TAXES.EE_LOCALS.NewLookup('EE_LOCALS_NBR', EELocalNumber, 'OVERRIDE_LOCAL_TAX_VALUE')
  else
  if not VarIsNull(DM_HISTORICAL_TAXES.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_RATE')) then
    TaxRate := DM_HISTORICAL_TAXES.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COLocalNumber, 'TAX_RATE')
  else
    TaxRate := ConvertNull(DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SYLocalNumber, 'TAX_RATE'), 0);

  PR_CHECK_LINES.First;
  while not PR_CHECK_LINES.EOF do
  begin
    Source := 0;
    LocationLevel := 'C';
    DbdtNbr := 0;
    LocationNbr := 0;
    CL_E_DS_NBR := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger;
    PR_CHECK_LINES_NBR := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    EDCodeType := DM_HISTORICAL_TAXES.CL_E_DS.ShadowDataSet.CachedLookup(CL_E_DS_NBR, 'E_D_CODE_TYPE');

    CO_DIVISION_NBR   := PR_CHECK_LINES['CO_DIVISION_NBR'];
    CO_BRANCH_NBR     := PR_CHECK_LINES['CO_BRANCH_NBR'];
    CO_DEPARTMENT_NBR := PR_CHECK_LINES['CO_DEPARTMENT_NBR'];
    CO_TEAM_NBR       := PR_CHECK_LINES['CO_TEAM_NBR'];
    CO_JOBS_NBR       := PR_CHECK_LINES['CO_JOBS_NBR'];

    if VarIsNull(CO_DIVISION_NBR) and not DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull then
      CO_DIVISION_NBR := DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').Value;

    if VarIsNull(CO_BRANCH_NBR) and not DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull then
      CO_BRANCH_NBR := DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').Value;

    if VarIsNull(CO_DEPARTMENT_NBR) and not DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull then
      CO_DEPARTMENT_NBR := DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').Value;

    if VarIsNull(CO_TEAM_NBR) and not DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull then
      CO_TEAM_NBR := DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').Value;

    if VarIsNull(CO_JOBS_NBR) and not DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').IsNull then
      CO_JOBS_NBR := DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').Value;

    if EDCodeType[1] <> 'D' then
    begin

      Flag := True;

      if (PR_CHECK_LINES.FieldByName('RATE_NUMBER').AsInteger <= 0) and PR_CHECK_LINES.FieldByName('RATE_OF_PAY').IsNull then
      begin
        if (PR_CHECK_LINES.FieldByName('RATE_NUMBER').AsInteger <= 0) then
          Q5 := TevQuery.Create('select * from EE_RATES where {AsOfNow<EE_RATES>} and EE_NBR='+
            DM_EMPLOYEE.EE.EE_NBR.AsString+' and PRIMARY_RATE=''Y''')
        else
          Q5 := TevQuery.Create('select * from EE_RATES where {AsOfNow<EE_RATES>} and EE_NBR='+
            DM_EMPLOYEE.EE.EE_NBR.AsString+' and RATE_NUMBER='+PR_CHECK_LINES.FieldByName('RATE_NUMBER').AsString);

        if not Q5.Result.Eof then
        begin
          if (not Q5.Result.FieldByName('CO_DIVISION_NBR').IsNull
          or not Q5.Result.FieldByName('CO_BRANCH_NBR').IsNull
          or not Q5.Result.FieldByName('CO_DEPARTMENT_NBR').IsNull
          or not Q5.Result.FieldByName('CO_TEAM_NBR').IsNull
          or not Q5.Result.FieldByName('CO_JOBS_NBR').IsNull)
          and VarIsNull(CO_JOBS_NBR)
          then
          begin
            CO_DIVISION_NBR   := Q5.Result['CO_DIVISION_NBR'];
            CO_BRANCH_NBR     := Q5.Result['CO_BRANCH_NBR'];
            CO_DEPARTMENT_NBR := Q5.Result['CO_DEPARTMENT_NBR'];
            CO_TEAM_NBR       := Q5.Result['CO_TEAM_NBR'];
            CO_JOBS_NBR       := Q5.Result['CO_JOBS_NBR'];
          end;
        end;
      end;

      if TypeIsTaxable(EDCodeType)
      and not (TypeIsSpecTaxedDed(EDCodeType)
      and (DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'INCLUDE_IN_PRETAX') = 'N'))
      and not SYExempt
      and (DM_HISTORICAL_TAXES.CL_E_D_LOCAL_EXMPT_EXCLD.Lookup('SY_LOCALS_NBR;CL_E_DS_NBR', VarArrayOf([SYLocalNumber, CL_E_DS_NBR]), 'EXEMPT_EXCLUDE') <> 'E') then
      begin

        TaxableWages := TaxableWages + ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);

        if not VarIsNull(CO_JOBS_NBR) then
        begin
          if not DM_COMPANY.CO_JOBS.Active then
            DM_COMPANY.CO_JOBS.Open;
          DM_COMPANY.CO_JOBS.Locate('CO_JOBS_NBR', CO_JOBS_NBR, []);
          if not DM_COMPANY.CO_JOBS.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationLevel := '5';
            LocationNbr := DM_COMPANY.CO_JOBS.CO_LOCATIONS_NBR.AsInteger;
            DbdtNbr := CO_JOBS_NBR;
          end;
        end;

        if (LocationLevel = 'C') and not VarIsNull(CO_TEAM_NBR) then
        begin
          if not DM_COMPANY.CO_TEAM.Active then
            DM_COMPANY.CO_TEAM.Open;
          DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', CO_TEAM_NBR, []);
          if not DM_COMPANY.CO_TEAM.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationLevel := '4';
            LocationNbr := DM_COMPANY.CO_TEAM.CO_LOCATIONS_NBR.AsInteger;
            DbdtNbr := CO_TEAM_NBR;
          end;
        end;

        if (LocationLevel = 'C') and not VarIsNull(CO_DEPARTMENT_NBR) then
        begin
          if not DM_COMPANY.CO_DEPARTMENT.Active then
            DM_COMPANY.CO_DEPARTMENT.Open;
          DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', CO_DEPARTMENT_NBR, []);
          if not DM_COMPANY.CO_DEPARTMENT.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationLevel := '3';
            LocationNbr := DM_COMPANY.CO_DEPARTMENT.CO_LOCATIONS_NBR.AsInteger;
            DbdtNbr := CO_DEPARTMENT_NBR;
          end;
        end;

        if (LocationLevel = 'C') and not VarIsNull(CO_BRANCH_NBR) then
        begin
          if not DM_COMPANY.CO_BRANCH.Active then
            DM_COMPANY.CO_BRANCH.Open;
          DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', CO_BRANCH_NBR, []);
          if not DM_COMPANY.CO_BRANCH.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationLevel := '2';
            LocationNbr := DM_COMPANY.CO_BRANCH.CO_LOCATIONS_NBR.AsInteger;
            DbdtNbr := CO_BRANCH_NBR;
          end;
        end;

        if (LocationLevel = 'C') and not VarIsNull(CO_DIVISION_NBR) then
        begin
          if not DM_COMPANY.CO_DIVISION.Active then
            DM_COMPANY.CO_DIVISION.Open;
          DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', CO_DIVISION_NBR, []);
          if not DM_COMPANY.CO_DIVISION.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationLevel := '1';
            LocationNbr := DM_COMPANY.CO_DIVISION.CO_LOCATIONS_NBR.AsInteger;
            DbdtNbr := CO_DIVISION_NBR;
          end;
        end;

        if LocationLevel = 'C' then
        begin
          if not DM_COMPANY.CO.CO_LOCATIONS_NBR.IsNull then
          begin
            LocationNbr := DM_COMPANY.CO.CO_LOCATIONS_NBR.AsInteger;
          end;
        end;

//        if (DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'DEDUCT') = DEDUCT_NEVER) then
        if PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString = 'Y' then
        begin
          Source := 1;
          LocationLevel := 'E';
          LocationNbr := ConvertNull(DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'CO_LOCATIONS_NBR'),0);
        end
        else
        if True then
        begin

          if not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull then
          begin
            if not DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_NBR;CO_LOCAL_TAX_NBR',
              VarArrayOf([CO_JOBS_NBR, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              Source := 1;
              LocationLevel := '5';
              DbdtNbr := CO_JOBS_NBR;
            end;
          end
          else
          if LocationLevel = '5' then
          begin
            if not DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_NBR;CO_LOCAL_TAX_NBR',
              VarArrayOf([CO_JOBS_NBR, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              Source := 1;
              LocationLevel := '5';
              DbdtNbr := CO_JOBS_NBR;
            end;
          end;

          if not Flag and not DM_EMPLOYEE.EE.CO_JOBS_NBR.IsNull then
          begin
            if not DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_NBR;CO_LOCAL_TAX_NBR',
              VarArrayOf([DM_EMPLOYEE.EE.CO_JOBS_NBR.Value, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              Source := 1;
              LocationLevel := '5';
              DbdtNbr := DM_EMPLOYEE.EE.CO_JOBS_NBR.Value;
            end;
          end;

          if (LocationLevel <> '5') and (not Flag and (not VarIsNull(CO_TEAM_NBR)) or (not DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull)) then
          begin
            if not VarIsNull(CO_TEAM_NBR) then
              EntityNbr := CO_TEAM_NBR
            else
              EntityNbr := DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').AsInteger;

            if not DM_COMPANY.CO_TEAM_LOCALS.Locate('CO_TEAM_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EntityNbr, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              if not VarIsNull(CO_TEAM_NBR) then
                if PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString = 'N' then
                begin
                  Source := 1;
                  LocationLevel := '4';
                  DbdtNbr := EntityNbr;
                end;
            end;
          end;

          if (LocationLevel <> '5') and (LocationLevel <> '4') and (not Flag and (not VarIsNull(CO_DEPARTMENT_NBR)) or (not DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull)) then
          begin
            if not VarIsNull(CO_DEPARTMENT_NBR) then
              EntityNbr := CO_DEPARTMENT_NBR
            else
              EntityNbr := DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').AsInteger;

            if not DM_COMPANY.CO_DEPARTMENT_LOCALS.Locate('CO_DEPARTMENT_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EntityNbr, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              if not VarIsNull(CO_DEPARTMENT_NBR) then
                if PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString = 'N' then
                begin
                  Source := 1;
                  LocationLevel := '3';
                  DbdtNbr := EntityNbr;
                end;
            end;
          end;

          if (LocationLevel <> '5') and (LocationLevel <> '4') and (LocationLevel <> '3') and (not Flag and (not VarIsNull(CO_BRANCH_NBR)) or (not DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull)) then
          begin
            if not VarIsNull(CO_BRANCH_NBR) then
              EntityNbr := CO_BRANCH_NBR
            else
              EntityNbr := DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').AsInteger;

            if not DM_COMPANY.CO_BRANCH_LOCALS.Locate('CO_BRANCH_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EntityNbr, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              if not VarIsNull(CO_BRANCH_NBR) then
                if PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString = 'N' then
                begin
                  Source := 1;
                  LocationLevel := '2';
                  DbdtNbr := EntityNbr;
                end;
            end;
          end;

          if (LocationLevel <> '5') and (LocationLevel <> '4') and (LocationLevel <> '3') and (LocationLevel <> '2') and (not Flag and (not VarIsNull(CO_DIVISION_NBR)) or (not DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull)) then
          begin
            if not VarIsNull(CO_DIVISION_NBR) then
              EntityNbr := CO_DIVISION_NBR
            else
              EntityNbr := DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').AsInteger;

            if not DM_COMPANY.CO_DIVISION_LOCALS.Locate('CO_DIVISION_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EntityNbr, COLocalNumber]), []) then
            begin
              Flag := False;
            end
            else
            begin
              if not VarIsNull(CO_DIVISION_NBR) then
                if PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString = 'N' then
                begin
                  Source := 1;
                  LocationLevel := '1';
                  DbdtNbr := EntityNbr;
                end;
            end;
          end;

          if DM_EMPLOYEE.EE.CO_JOBS_NBR.IsNull and
             VarIsNull(CO_JOBS_NBR) and
             VarIsNull(CO_TEAM_NBR) and
             DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull and
             VarIsNull(CO_DEPARTMENT_NBR) and
             DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull and
             VarIsNull(CO_BRANCH_NBR) and
             DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull and
             VarIsNull(CO_DIVISION_NBR) and
             DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull
          then
            Flag := False;


          if DbdtNbr > 0
            then Flag := True;

          if not Flag
          and PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES_NBR, EELocalNumber]), [])
          and (PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] = GROUP_BOX_INCLUDE)
          then
          begin
            LocationsM.Insert;
            LocationsM['PR_NBR'] := PR_CHECK['PR_NBR'];
            LocationsM['EE_NBR'] := PR_CHECK['EE_NBR'];
            LocationsM['PR_CHECK_NBR'] := CheckNumber;
            LocationsM['PR_CHECK_LINES_NBR'] := PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
            LocationsM['EE_LOCALS_NBR'] := EELocalNumber;
            LocationsM['CO_LOCAL_TAX_NBR'] := CoLocalNumber;
            LocationsM['SY_LOCALS_NBR'] := SYLocalNumber;
            LocationsM['RATE'] := TaxRate;
            LocationsM['BLOCKED'] := 'N';
            LocationsM['ISLOSER'] := 'N';
            LocationsM['EXCLUDED'] := 'N';
            LocationsM['AMOUNT'] := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
            if IsResidential then
              LocationsM['ISRES'] := 'Y'
            else
              LocationsM['ISRES'] := 'N';
            if Source = 1 then
              LocationsM['DBDT_ATTACHED'] := 'Y'
            else
              LocationsM['DBDT_ATTACHED'] := 'N';

            LocationsM['WORK_ADDRESS_OVR'] := PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString;

            LocationsM['LOCATION_LEVEL'] := LocationLevel;
            if LocationNbr <> 0 then
              LocationsM['CO_LOCATIONS_NBR'] := LocationNbr;

            if LocationLevel = '1' then
              LocationsM['DIVISION_NBR'] := DbdtNbr
            else
            if LocationLevel = '2' then
            begin
              LocationsM['BRANCH_NBR'] := DbdtNbr;
              if not DM_COMPANY.CO_BRANCH.Active then
                DM_COMPANY.CO_BRANCH.Open;
              Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DbdtNbr, []));
              LocationsM['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
            end
            else
            if LocationLevel = '3' then
            begin
              LocationsM['DEPARTMENT_NBR'] := DbdtNbr;
              if not DM_COMPANY.CO_DEPARTMENT.Active then
                DM_COMPANY.CO_DEPARTMENT.Open;
              Assert(DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DbdtNbr, []));
              LocationsM['BRANCH_NBR'] := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
              if not DM_COMPANY.CO_BRANCH.Active then
                DM_COMPANY.CO_BRANCH.Open;
              Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', LocationsM['BRANCH_NBR'], []));
              LocationsM['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
            end
            else
            if LocationLevel = '4' then
            begin
              LocationsM['TEAM_NBR'] := DbdtNbr;
              if not DM_COMPANY.CO_TEAM.Active then
                DM_COMPANY.CO_TEAM.Open;
              Assert(DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DbdtNbr, []));
              LocationsM['DEPARTMENT_NBR'] := DM_COMPANY.CO_TEAM['CO_DEPARTMENT_NBR'];
              if not DM_COMPANY.CO_DEPARTMENT.Active then
                DM_COMPANY.CO_DEPARTMENT.Open;
              Assert(DM_COMPANY.CO_DEPARTMENT.Locate('CO_BRANCH_NBR', LocationsM['DEPARTMENT_NBR'], []));
              LocationsM['BRANCH_NBR'] := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
              if not DM_COMPANY.CO_BRANCH.Active then
                DM_COMPANY.CO_BRANCH.Open;
              Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', LocationsM['BRANCH_NBR'], []));
              LocationsM['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
            end
            else
            if LocationLevel = '5' then
            begin
              LocationsM['JOBS_NBR'] := DbdtNbr;
            end;

            LocationsM.Post;

          end;


        end
        else
        if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR',  PR_CHECK_LINES_NBR, [])
        and (DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'DEDUCT') = DEDUCT_NO_OVERRIDES) then
        begin
          Flag := False;
        end;

        Locations.Insert;
        Locations['PR_NBR'] := PR_CHECK['PR_NBR'];
        Locations['EE_NBR'] := PR_CHECK['EE_NBR'];
        Locations['PR_CHECK_NBR'] := CheckNumber;
        Locations['PR_CHECK_LINES_NBR'] := PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
        Locations['EE_LOCALS_NBR'] := EELocalNumber;
        Locations['CO_LOCAL_TAX_NBR'] := CoLocalNumber;
        Locations['SY_LOCALS_NBR'] := SYLocalNumber;
        Locations['RATE'] := TaxRate;
        Locations['BLOCKED'] := 'N';
        Locations['ISLOSER'] := 'N';

        Locations['EXCLUDED'] := 'N';

        Locations['AMOUNT'] := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);

        if IsResidential then
          Locations['ISRES'] := 'Y'
        else
          Locations['ISRES'] := 'N';
        if Source = 1 then
          Locations['DBDT_ATTACHED'] := 'Y'
        else
          Locations['DBDT_ATTACHED'] := 'N';

        Locations['LOCATION_LEVEL'] := LocationLevel;
        if LocationNbr <> 0 then
          Locations['CO_LOCATIONS_NBR'] := LocationNbr;
        Locations['WORK_ADDRESS_OVR'] := DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'WORK_ADDRESS_OVR');//PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString;

        if LocationLevel = '1' then
          Locations['DIVISION_NBR'] := DbdtNbr
        else
        if LocationLevel = '2' then
        begin
          Locations['BRANCH_NBR'] := DbdtNbr;
          if not DM_COMPANY.CO_BRANCH.Active then
            DM_COMPANY.CO_BRANCH.Open;
          Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DbdtNbr, []));
          Locations['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
        end
        else
        if LocationLevel = '3' then
        begin
          Locations['DEPARTMENT_NBR'] := DbdtNbr;
          if not DM_COMPANY.CO_DEPARTMENT.Active then
            DM_COMPANY.CO_DEPARTMENT.Open;
          Assert(DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DbdtNbr, []));
          Locations['BRANCH_NBR'] := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
          if not DM_COMPANY.CO_BRANCH.Active then
            DM_COMPANY.CO_BRANCH.Open;
          Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', Locations['BRANCH_NBR'], []));
          Locations['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
        end
        else
        if LocationLevel = '4' then
        begin
          Locations['TEAM_NBR'] := DbdtNbr;
          if not DM_COMPANY.CO_TEAM.Active then
            DM_COMPANY.CO_TEAM.Open;
          Assert(DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DbdtNbr, []));
          Locations['DEPARTMENT_NBR'] := DM_COMPANY.CO_TEAM['CO_DEPARTMENT_NBR'];
          if not DM_COMPANY.CO_DEPARTMENT.Active then
            DM_COMPANY.CO_DEPARTMENT.Open;
          Assert(DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', Locations['DEPARTMENT_NBR'], []));
          Locations['BRANCH_NBR'] := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
          if not DM_COMPANY.CO_BRANCH.Active then
            DM_COMPANY.CO_BRANCH.Open;
          Assert(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', Locations['BRANCH_NBR'], []));
          Locations['DIVISION_NBR'] := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
        end
        else
        if LocationLevel = '5' then
        begin
          Locations['JOBS_NBR'] := DbdtNbr;
        end;

        Locations.Post;
      end;
    end
    else
    begin
      if TypeIsTaxable(EDCodeType)
      and not (TypeIsSpecTaxedDed(EDCodeType)
      and (DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalNumber, 'INCLUDE_IN_PRETAX') = 'N'))
      and not SYExempt
      and (DM_HISTORICAL_TAXES.CL_E_D_LOCAL_EXMPT_EXCLD.Lookup('SY_LOCALS_NBR;CL_E_DS_NBR', VarArrayOf([SYLocalNumber, CL_E_DS_NBR]), 'EXEMPT_EXCLUDE') <> 'E') then
        Deductions := Deductions + PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
    end;
    PR_CHECK_LINES.Next;
  end;

//  if abs(Deductions) > 0.005 then
  begin
    Locations.CancelRange;

    Locations.IndexFieldNames := 'PR_CHECK_NBR;CO_LOCAL_TAX_NBR';
    Locations.SetRange([CheckNumber, COLocalNumber], [CheckNumber, COLocalNumber]);
    Locations.First;
    no := 0;
    TotalDeductions := Deductions;
    while not Locations.Eof do
    begin
      Inc(no);
      if Abs(TaxableWages) > 0.005 then
        Deduct := RoundTwo(Deductions * Locations['AMOUNT']/TaxableWages)
      else
        Deduct := 0;  

      if (no = Locations.RecordCount) and (Abs(Deduct - TotalDeductions) < 0.03) then
        Deduct := TotalDeductions
      else
        TotalDeductions := TotalDeductions - Deduct;

      Locations.Edit;
      Locations['LOCAL_TAXABLE_WAGES'] := Locations['AMOUNT'] - Deduct;

      if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
        VarArrayOf([Locations['PR_CHECK_LINES_NBR'], Locations['EE_LOCALS_NBR']]), [])
          and (PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] = GROUP_BOX_EXEMPT) then
      begin
        Locations['LOCAL_TAXABLE_WAGES'] := 0;
      end;



      Locations.Post;
      Locations.Next;
    end;
  end;

end;

procedure CalcEmployeeLocalTax(CheckNumber, COLocalNumber: Integer; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean = False; DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False);
begin
  CalcLocalTax(CheckNumber, COLocalNumber, False, TaxRate, LocalTax, LocalTaxWages, LocalGrossWages,
    PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS,
    ForTaxCalculator, DisableYTDs, DisableShortfalls);
end;

procedure CalcEmployerLocalTax(CheckNumber, COLocalNumber: Integer; var TaxRate, LocalTax, LocalTaxWages, LocalGrossWages: Real;
  PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS: TevClientDataSet;
  ForTaxCalculator: Boolean = False; DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False);
begin
  CalcLocalTax(CheckNumber, COLocalNumber, True, TaxRate, LocalTax, LocalTaxWages, LocalGrossWages,
    PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS,
    ForTaxCalculator, DisableYTDs, DisableShortfalls);
end;

end.

