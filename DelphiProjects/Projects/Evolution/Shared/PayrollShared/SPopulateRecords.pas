// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPopulateRecords;

interface

uses
  Controls,  EvTypes, classes, SPD_Payroll_Expert, EvUtils, EvConsts,
  SFieldCodeValues, Dialogs, SDataStructure, SysUtils, SPD_Warnings_List,
  DB, SPD_CheckFinder, SPD_Misc_CheckFinder, Variants, isVCLBugFix,
  EvStreamUtils, EvBasicUtils, EvContext, EvMainboard, EvExceptions, EvUIUtils,
  EvUIComponents, EvClientDataSet, isTypes, IsBaseClasses;

procedure Generic_PopulatePRRecord(iClientNbr, iCoNumber: Integer; PayrollNotes: string);
procedure Generic_PopulatePRBatchRecord(CobraType: boolean = False);
function Generic_PopulatePRCheckRecord(EmployeeNumber: String): Integer; // return check template or zero
procedure PopulatePRMiscCheckRecord;
procedure PopulateStringGrid(MyStringGrid: TevStringGrid; OLD_PR_CHECK, OLD_PR_CHECK_LINES, OLD_PR_CHECK_STATES,
  OLD_PR_CHECK_SUI, OLD_PR_CHECK_LOCALS: TevClientDataSet);
procedure AutoCreateCompanies(CompanyList: TStringList);
procedure CheckEEStatesForSUI;
function GetCobraWizardEEs(var V: variant; WithScheduledED: boolean = True): string;

implementation

uses
  SSecurityInterface, SProcessTCImport,
  SPD_Import_Options, Windows, SPD_DBDTP_Filter, SDDClasses,
  EvCommonInterfaces, SDataDictclient, EvDataAccessComponents;

function GetCobraWizardEEs(var V: variant; WithScheduledED: boolean = True): string;
var
  CoNbr: string;
  i: integer;
begin
  // returns comma delimited list of employees with the status of 'Involuntary Layoff'
  // and have the 'Cobra Credit' code type as an active Scheduled ED
  Result := '-1';
  V := Null;
  CoNbr := Trim(VarToStr(DM_COMPANY.CO['CO_NBR']));
  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;

  if WithScheduledED then
  with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
  begin
    SetMacro('COLUMNS', 'T1.EE_NBR');
    SetMacro('TABLE1', 'EE');
    SetMacro('TABLE2', 'EE_SCHEDULED_E_DS');
    SetMacro('TABLE3', 'CL_E_DS');
    SetMacro('JOINFIELD', 'EE');
    SetMacro('JOINFIELD2', 'CL_E_DS');
    SetMacro('CONDITION', 'T1.CURRENT_TERMINATION_CODE=''' + EE_TERM_INV_LAYOFF + ''' AND T1.CO_NBR=' + CoNbr +
      ' AND T3.E_D_CODE_TYPE=''' + ED_MEMO_COBRA_CREDIT + '''');

    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end
  else with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('COLUMNS', 'EE_NBR');
    SetMacro('TABLENAME', 'EE');
    SetMacro('CONDITION', 'CURRENT_TERMINATION_CODE=''' + EE_TERM_INV_LAYOFF + '''');

    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  
  ctx_DataAccess.CUSTOM_VIEW.Open;
  i := 0;
  if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
    V := VarArrayCreate([0, ctx_DataAccess.CUSTOM_VIEW.RecordCount - 1], varInteger);
  while not (ctx_DataAccess.CUSTOM_VIEW.Eof) do
  begin
    Result := Result + ',' +VarToStr(ctx_DataAccess.CUSTOM_VIEW['EE_NBR']);
    V[i] := VarToInt(ctx_DataAccess.CUSTOM_VIEW['EE_NBR']);
    Inc(i);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
  ctx_DataAccess.CUSTOM_VIEW.Close;
end;

procedure CheckEEStatesForSUI;
var
  bMultipleCompanies: Boolean;
  d: TisDate;
begin
  if DM_PAYROLL.PR.Active then
  begin
    d := Trunc(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime + 1 - 1 / 1440);
    DM_SYSTEM_STATE.SY_STATES.AsOfDate := d;
    DM_COMPANY.CO_STATES.AsOfDate := d;
    DM_EMPLOYEE.EE_STATES.AsOfDate := d;
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.AsOfDate := d;
    DM_SYSTEM_LOCAL.SY_COUNTY.AsOfDate := d;
    DM_SYSTEM_LOCAL.SY_LOCALS.AsOfDate := d;
    DM_COMPANY.CO_LOCAL_TAX.AsOfDate := d;
    DM_EMPLOYEE.EE_LOCALS.AsOfDate := d;
  end
  else
  begin
    DM_SYSTEM_STATE.SY_STATES.AsOfDate := 0;
    DM_COMPANY.CO_STATES.AsOfDate := 0;
    DM_EMPLOYEE.EE_STATES.AsOfDate := 0;
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.AsOfDate := 0;
    DM_SYSTEM_LOCAL.SY_COUNTY.AsOfDate := 0;
    DM_SYSTEM_LOCAL.SY_LOCALS.AsOfDate := 0;
    DM_COMPANY.CO_LOCAL_TAX.AsOfDate := 0;
    DM_EMPLOYEE.EE_LOCALS.AsOfDate := 0;
  end;

  with DM_TEMPORARY.TMP_CO.ShadowDataSet do
  try
    SetRange([ctx_DataAccess.ClientID], [ctx_DataAccess.ClientID]);
    bMultipleCompanies := RecordCount > 1;
  finally
    CancelRange;
  end;
  if not DM_EMPLOYEE.EE_STATES.Active then
    if bMultipleCompanies or (ctx_PayrollCalculation.GetEeFilter <> '') then
      DM_EMPLOYEE.EE_STATES.RetrieveCondition := UplinkRetrieveCondition('EE_NBR', DM_EMPLOYEE.EE)
    else
      DM_EMPLOYEE.EE_STATES.RetrieveCondition := '';
  if not DM_EMPLOYEE.EE_LOCALS.Active then
    if bMultipleCompanies or (ctx_PayrollCalculation.GetEeFilter <> '') then
      DM_EMPLOYEE.EE_LOCALS.RetrieveCondition := UplinkRetrieveCondition('EE_NBR', DM_EMPLOYEE.EE)
    else
      DM_EMPLOYEE.EE_LOCALS.RetrieveCondition := '';
  ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS]);
end;

procedure Generic_PopulatePRRecord(iClientNbr, iCoNumber: Integer; PayrollNotes: string);
var
  TempDate: TDateTime;
  TempStr: string;
  WarningList: TStringList;
  ScheduledCheckDate: TDateTime;
  s: string;
  InSbMode: Boolean;
  ds, ds2: TevClientDataSet;

  Function PrepareItemList(Items : String; Parameters: array of Variant ):String;
  var
  J, K: integer;
  Choices: TStringList;
  begin
     result := '';
     Choices := TStringList.Create;
     try
       Choices.Text := Items;
       for J := 0 to High(Parameters) do
         for K := 0 to Choices.Count - 1 do
          if Pos(#9 + Parameters[J], Choices[K]) > 0 then
          begin
            Choices.Delete(K);
            break;
          end;
      finally
        result := Choices.Text;
        Choices.Destroy;
      end;
  End;

begin
  InSbMode := ctx_AccountRights.Functions.GetState('PR_CREATION_WIZARD2') = stEnabled;
// 4-10
  Payroll_Expert := TPayroll_Expert.Create(Nil);

  with Payroll_Expert, ctx_PayrollCalculation do
  try
    ScheduledCheckDate := NextCheckDate(Date, True);
    wwdsTemp.DataSet := DM_PAYROLL.PR;

// Show Payroll notes to a user
    if PayrollNotes = '' then
      DisplayExpert(crNone, 'No payroll notes.', nil, Null)
    else
      DisplayExpert(crNone, PayrollNotes, nil, Null);

// Get check comments from a user
    DisplayExpert(crMemo, 'Please, enter payroll check comments below.', nil, VarArrayOf(['PAYROLL_COMMENTS']));
// Get payroll type from a user
    DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_REGULAR;
    DisplayExpert(crComboBox, 'Please, choose payroll type.', nil, VarArrayOf(['PAYROLL_TYPE', PrepareItemList(PayrollType_ComboChoices, [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_TAX_ADJUSTMENT])]));

    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_REGULAR, PAYROLL_TYPE_SETUP] then
    begin
// Get check date from a user
      DM_PAYROLL.PR.FieldByName('SCHEDULED').Value := 'Y';
      DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value := ScheduledCheckDate;
      DisplayExpert(crDateEdit, 'This is the current scheduled check date. Please, make necessary changes.', nil,
        VarArrayOf(['CHECK_DATE']));
      s := HolidayDay(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);
      while (s <> '') or DayIsWeekend(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) do
      begin
        TempDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
        if s <> '' then
          DisplayExpert(crDateEdit, 'This date is the ' + s +
            '. Please, select another date.', nil, VarArrayOf(['CHECK_DATE']))
        else if DayIsWeekend(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) then
          DisplayExpert(crDateEdit, 'This date falls on a weekend. Please, select another date.', nil, VarArrayOf(['CHECK_DATE']));
        if DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value = TempDate then Break;
        s := HolidayDay(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);
      end;

      DM_PAYROLL.PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_NORMAL;
{        if ClientConfiguration.ClientType = ctRemote then
        DM_PAYROLL.PR.FieldByName('SCHEDULED_DATE_CHANGE').AsString := 'N';}
(*      if ScheduledCheckDate <> DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value then
// Is it a new scheduled date?
{          if ClientConfiguration.ClientType = ctRemote then
        begin
          DisplayExpert(crRadioGroup, 'You have chosen a check date of ' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString
            + '. Should it be put in the calendar?', nil, VarArrayOf(['SCHEDULED_DATE_CHANGE', 3, VarArrayOf(['Add', 'Replace', 'No'])
            , VarArrayOf(['A', 'R', 'N'])]));
          if DM_PAYROLL.PR.FieldByName('SCHEDULED_DATE_CHANGE').AsString = 'N' then
          begin
            DM_PAYROLL.PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
            DM_PAYROLL.PR.FieldByName('SCHEDULED').Value := 'N';
          end;
        end
        else}
        begin
          DisplayExpert(crRadioGroup, 'You have chosen a check date of ' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString
            + '. Is this a new scheduled date?', nil, VarArrayOf(['CHECK_DATE_STATUS', 2, VarArrayOf(['Yes', 'No'])
            , VarArrayOf([DATE_STATUS_NORMAL, DATE_STATUS_IGNORE])]));
          if DM_PAYROLL.PR.FieldByName('CHECK_DATE_STATUS').Value = DATE_STATUS_IGNORE then
            DM_PAYROLL.PR.FieldByName('SCHEDULED').Value := 'N';
        end;*)
    end
    else
    begin
      DM_PAYROLL.PR.FieldByName('SCHEDULED').Value := 'N';
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_REVERSAL] then
      begin
        EvMessage('Reversal payroll type is supported through voiding of a payroll.', mtInformation, [mbOK]);
        DM_PAYROLL.PR.Cancel;
        AbortEx;
      end;

      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION] then
// Get prior payroll from a user
        with ctx_DataAccess.CUSTOM_VIEW do
        begin
          if Active then Close;
          with TExecDSWrapper.Create('GenericSelectCurrentNBROrdered') do
          begin
            SetMacro('Columns', 'CHECK_DATE, RUN_NUMBER, PR_NBR');
            SetMacro('TableName', 'PR');
            SetMacro('NbrField', 'CO');
            SetParam('RecordNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
            SetMacro('Order', 'CHECK_DATE, RUN_NUMBER');
            DataRequest(AsVariant);
          end;
          Open;
          repeat
            TempStr := DisplayExpert(crLookupCombo, 'Please, select a prior payroll you want to correct.',
              ctx_DataAccess.CUSTOM_VIEW,
              VarArrayOf(['PR_NBR', 'CHECK_DATE' + #9 + '10' + #9 + 'CHECK_DATE' + #13 + 'RUN_NUMBER' + #9 + '5' + #9 + 'RUN_NUMBER']));
          until TempStr <> '';
          DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value := Lookup('PR_NBR', TempStr, 'CHECK_DATE');
          DM_PAYROLL.PR.FieldByName('FILLER').Value := PutIntoFiller(ConvertNull(DM_PAYROLL.PR.FieldByName('FILLER').Value, ''), TempStr, 1, 8);
          Close;
        end;
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_SUPPLEMENTAL, PAYROLL_TYPE_WAGE_ADJUSTMENT_QTR,
      PAYROLL_TYPE_WAGE_ADJUSTMENT_NO_QTR, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT, PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_BACKDATED] then
      begin
// Get check date from a user
        while DM_PAYROLL.PR.FieldByName('CHECK_DATE').IsNull do
        begin
          DisplayExpert(crDateEdit, 'Please, select a check date.', nil, VarArrayOf(['CHECK_DATE']));
        end;
      end;
// 4-30 Begin
      s := HolidayDay(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);
      while (s <> '') or DayIsWeekend(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) do
      begin
        TempDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
        if s <> '' then
          DisplayExpert(crDateEdit, 'This date is the ' + s
            + '. Please, select another date or proceed.', nil, VarArrayOf(['CHECK_DATE']))
        else
          if DayIsWeekend(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) then
          DisplayExpert(crDateEdit, 'This date falls on a weekend. Please, select another date or proceed.'
            , nil, VarArrayOf(['CHECK_DATE']));
        if DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value = TempDate then Break;
        s := HolidayDay(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);
      end;

      if DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value < Date then
      begin
        WarningList := TStringList.Create;
        WarningList.Text := ListBackdatedPayrollProblems(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime);
        if WarningList.Count > 0 then
        begin
          Warnings_List := TWarnings_List.Create(Nil);
          try
            Warnings_List.WarningsListBox.Items.Assign(WarningList);
            if Warnings_List.ShowModal <> mrOK then
            begin
              DM_PAYROLL.PR.Cancel;
              AbortEx;
            end;
          finally
            Warnings_List.Free;
          end;
        end;
        WarningList.Free;
      end;
// 4-30 End
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION,
        PAYROLL_TYPE_WAGE_ADJUSTMENT_QTR, PAYROLL_TYPE_WAGE_ADJUSTMENT_NO_QTR, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT,
        PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_SUPPLEMENTAL] then
      begin
        DM_PAYROLL.PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      end;
    end;

// Assign Billing Company Services info
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y'
    else
// Get Exclude All Agencies
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value := 'N';
      DisplayExpert(crRadioGroup, 'Block all agencies?', nil, VarArrayOf(['EXCLUDE_AGENCY', 2, VarArrayOf(['Yes', 'No']),
        VarArrayOf(['Y',
          'N'])]));
    end;

// Get Exclude 401K agencies only
    {DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
    if DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value = 'N' then
    begin
      DisplayExpert(crRadioGroup, 'Block 401K agencies only?', nil, VarArrayOf(['MARK_LIABS_PAID_DEFAULT', 2, VarArrayOf(['Yes', 'No']),
        VarArrayOf(['Y', 'N'])]));
    end;
    }
// Get Exclude ACH
   if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
     DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value := 'Y'
   else
   begin
     DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value := 'N';
     if InSbMode then
       DisplayExpert(crRadioGroup, 'Block All ACH Transactions?', nil, VarArrayOf(['EXCLUDE_ACH', 2, VarArrayOf(['Yes', 'No']), VarArrayOf(['Y', 'N'])]));
   end;

   if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := 'Y'
    else
// Get Exclude Billing (should be available to SB only)
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := 'N';
      if InSbMode then
        DisplayExpert(crRadioGroup, 'Block billing?', nil, VarArrayOf(['EXCLUDE_BILLING', 2, VarArrayOf(['Yes', 'No']), VarArrayOf(['Y', 'N'])]));
    end;

    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'B'
    else
// Get Exclude Tax Deposits
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
      if InSbMode then
        DisplayExpert(crRadioGroup, 'Block tax deposits?', nil, VarArrayOf(['EXCLUDE_TAX_DEPOSITS', 2
          , VarArrayOf(['Deposits', 'Liabilities', 'Both', 'None']), VarArrayOf(['D', 'L', 'B', 'N'])]));
    end;

    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := 'B'
    else
// Get exclude REPORTS/CHECKS from a user
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').Value;
      DisplayExpert(crRadioGroup, 'Block Checks/Reports?', nil, VarArrayOf(['EXCLUDE_R_C_B_0R_N', 2
        , VarArrayOf(['Reports', 'Checks', 'Both', 'None']), VarArrayOf(['R', 'C', 'B', 'N'])]));
    end;

    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_NONE
    else
// Get exclude REPORTS/CHECKS from a user
    begin
      ds := TevClientDataSet.Create(nil);
      ds2 := TEvClientDataSet(wwdsTemp.DataSet);
      wwdsTemp.DataSet := ds;
      try
        ds.FieldDefs.Add('TO', ftString, 1);
        ds.CreateDataSet;
        ds.Insert;
        ds.FieldByName('TO').AsString := TIME_OFF_EXCLUDE_ACCRUAL_NONE;
        DisplayExpert(crRadioGroup, 'Block Time Off Accrual?', ds, VarArrayOf(['TO', 2
          , VarArrayOf(['All', 'Accrual', 'None']), VarArrayOf([TIME_OFF_EXCLUDE_ACCRUAL_ALL, TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL, TIME_OFF_EXCLUDE_ACCRUAL_NONE])]));
        DM_PAYROLL.PR.FieldByName('EXCLUDE_TIME_OFF').AsString := ds.FieldByName('TO').AsString;
      finally
        wwdsTemp.DataSet := ds2;
        ds.Free;
      end;
    end;
  finally
    Free;
    Payroll_Expert := nil;
  end;
end;

procedure Generic_PopulatePRBatchRecord(CobraType: boolean = False);
var
  ScheduledDate: TDateTime;
  TempDataSet: TevClientDataSet;
  TempStr: string;
  b: Boolean;
  Template: String;
begin
  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT then
  begin
    DM_PAYROLL.PR_BATCH.Cancel;
    raise ECanNotPerformOperation.CreateHelp('You can not create batch and regular checks on Miscellaneous Check Adjustment Payroll!', IDH_CanNotPerformOperation);
  end;

// 4-10.10
  Payroll_Expert := TPayroll_Expert.Create(nil);
  with Payroll_Expert, ctx_PayrollCalculation do
  try
    wwdsTemp.DataSet := DM_PAYROLL.PR_BATCH;

// Get frequency from a user
    DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);
    if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_REGULAR) or (DM_PAYROLL.PR.FieldByName('SCHEDULED').Value <> 'Y') then
      DisplayExpert(crComboBox, 'Please, select frequency.', nil, VarArrayOf(['FREQUENCY', GetCompanyPayFrequencies]));

    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_REGULAR then
    begin
// Get period begin date from a user
      b := False;
      if DM_PAYROLL.PR.FieldByName('SCHEDULED').Value = 'Y' then
      begin
        FindNextPeriodBeginEndDate;
        if DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount > 0 then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
          with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
          try
            DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Strings[0];
          finally
            Free;
          end;
        end;
        DisplayExpert(crComboBox, 'Please, select frequency.', nil, VarArrayOf(['FREQUENCY', GetCompanyPayFrequencies]));
        b := FindNextPeriodBeginEndDate;
        if not b then
          ScheduledDate := NextPeriodBeginDate
        else
          ScheduledDate := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE'];
      end
      else
        ScheduledDate := NextPeriodBeginDate;

      if CobraType then
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
      else
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := ScheduledDate;
      DisplayExpert(crDateEdit, 'This is the current scheduled period begin date. Please, make necessary changes.'
        , nil, VarArrayOf(['PERIOD_BEGIN_DATE']));
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      if not CobraType and (ScheduledDate <> DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value) then
      begin
// Is it a new scheduled date?
        DisplayExpert(crRadioGroup, 'You have chosen a period begin date of ' + DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsString
          + '. Is this a new scheduled date?', nil, VarArrayOf(['PERIOD_BEGIN_DATE_STATUS', 2, VarArrayOf(['Yes', 'No'])
          , VarArrayOf([DATE_STATUS_NORMAL, DATE_STATUS_IGNORE])]));
      end;

// Get period end date from a user
      if DM_PAYROLL.PR.FieldByName('SCHEDULED').Value = 'Y' then
      begin
        if not b then
          ScheduledDate := NextPeriodEndDate
        else
          ScheduledDate := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE'];
      end
      else
        ScheduledDate := NextPeriodEndDate;

      if CobraType then
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
      else
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := ScheduledDate;
      DisplayExpert(crDateEdit, 'This is the current scheduled period end date. Please, make necessary changes.'
        , nil, VarArrayOf(['PERIOD_END_DATE']));
      if DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value > DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value then
      begin
        DisplayExpert(crDateEdit, 'The period end date you have chosen is greater than the check date of '
          + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + '. Please, doublecheck it.', nil, VarArrayOf(['PERIOD_END_DATE']));
      end;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      if not CobraType and (ScheduledDate <> DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value) then
      begin
// Is it a new scheduled date?
        DisplayExpert(crRadioGroup, 'You have chosen a period end date of ' + DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').AsString
          + '. Is this a new scheduled date?', nil, VarArrayOf(['PERIOD_END_DATE_STATUS', 2, VarArrayOf(['Yes', 'No'])
          , VarArrayOf([DATE_STATUS_NORMAL, DATE_STATUS_IGNORE])]));
      end;
{
      if DM_PAYROLL.PR.FieldByName('SCHEDULED').Value = 'Y' then
      with DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH do
      begin
        if Locate('FREQUENCY', DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value, []) then
        begin
          Edit;
        end
        else
        begin
          Insert;
          FieldByName('PR_SCHEDULED_EVENT_NBR').Value := DM_PAYROLL.PR_SCHEDULED_EVENT.Lookup('PR_NBR', DM_PAYROLL.PR.FieldByName('PR_NBR').Value, 'PR_SCHEDULED_EVENT_NBR');
          FieldByName('SCHEDULED_CALL_IN_DATE').Value := DM_PAYROLL.PR.FieldByName('SCHEDULED_CALL_IN_DATE').Value;
          FieldByName('FREQUENCY').Value := DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value;
        end;
        FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value;
        FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value;
        Post;
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH]);
      end;}
    end
    else if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION] then
    begin
// Find all batches for the payroll we are correcting
      TempDataSet := TevClientDataSet.Create(nil);
      with ctx_DataAccess.CUSTOM_VIEW do
      try
        TempDataSet.FieldDefs.Add('PERIOD_BEGIN_DATE', ftDateTime, 0, False);
        TempDataSet.FieldDefs.Add('PERIOD_END_DATE', ftDateTime, 0, False);
        TempDataSet.FieldDefs.Add('FREQUENCY', ftString, 12, False);
        TempDataSet.FieldDefs.Add('PR_BATCH_NBR', ftInteger, 0, False);
        TempDataSet.CreateDataSet;
        if Active then Close;
        with TExecDSWrapper.Create('GenericSelectCurrentNBROrdered') do
        begin
          SetMacro('Columns', 'PERIOD_BEGIN_DATE, PERIOD_END_DATE, FREQUENCY, PR_BATCH_NBR');
          SetMacro('TableName', 'PR_BATCH');
          SetMacro('NbrField', 'PR');
          SetParam('RecordNbr', StrToInt(ExtractFromFiller(DM_PAYROLL.PR.FieldByName('FILLER').AsString, 1, 8)));
          SetMacro('Order', 'PERIOD_BEGIN_DATE, PERIOD_END_DATE');
          DataRequest(AsVariant);
        end;
        Open;
        First;
        while not EOF do
        begin
          TempDataSet.Edit;
          TempDataSet.FieldByName('PERIOD_BEGIN_DATE').Value := FieldByName('PERIOD_BEGIN_DATE').Value;
          TempDataSet.FieldByName('PERIOD_END_DATE').Value := FieldByName('PERIOD_END_DATE').Value;
          TempDataSet.FieldByName('PR_BATCH_NBR').Value := FieldByName('PR_BATCH_NBR').Value;
          case FieldByName('FREQUENCY').AsString[1] of
            FREQUENCY_TYPE_DAILY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Daily';
            FREQUENCY_TYPE_WEEKLY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Weekly';
            FREQUENCY_TYPE_BIWEEKLY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Bi-weekly';
            FREQUENCY_TYPE_SEMI_MONTHLY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Semi-monthly';
            FREQUENCY_TYPE_MONTHLY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Monthly';
            FREQUENCY_TYPE_QUARTERLY:
              TempDataSet.FieldByName('FREQUENCY').AsString := 'Quarterly';
          end;
          TempDataSet.Post;
          Next;
        end;
// Get a batch from a user
        TempStr := DisplayExpert(crLookupCombo, 'Please, select a prior batch you want to correct.', TempDataSet,
          VarArrayOf(['PR_BATCH_NBR', 'PERIOD_BEGIN_DATE' + #9 + '10' + #9 + 'PERIOD_BEGIN_DATE' + #13 +
          'PERIOD_END_DATE' + #9 + '10' + #9 + 'PERIOD_END_DATE' + #13 +
            'FREQUENCY' + #9 + '12' + #9 + 'FREQUENCY']));
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := TempDataSet.Lookup('PR_BATCH_NBR', TempStr, 'PERIOD_BEGIN_DATE');
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := TempDataSet.Lookup('PR_BATCH_NBR', TempStr, 'PERIOD_END_DATE');
        DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Lookup('PR_BATCH_NBR', TempStr, 'FREQUENCY');
        DM_PAYROLL.PR_BATCH.FieldByName('FILLER').Value := PutIntoFiller(ConvertNull(DM_PAYROLL.PR_BATCH.FieldByName('FILLER').Value, ''), TempStr, 1, 8);
        Close;
      finally
        TempDataSet.Free;
      end;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_IGNORE;
    end
    else
    begin
// Get period begin date from a user
      if CobraType then
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
      else
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := Date;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      DisplayExpert(crDateEdit, 'Please, enter period begin date.', nil, VarArrayOf(['PERIOD_BEGIN_DATE']));
// Get period end date from a user
      if CobraType then
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
      else
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := Date;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      DisplayExpert(crDateEdit, 'Please, enter period end date.', nil, VarArrayOf(['PERIOD_END_DATE']));
    end;

    if not CobraType then
    begin
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').Value := 'N'
      else
  // Get Pay Salary from a user
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').Value := 'Y';
        DisplayExpert(crRadioGroup, 'Pay Salary?', nil, VarArrayOf(['PAY_SALARY', 2, VarArrayOf(['Yes', 'No']), VarArrayOf(['Y', 'N'])]));
      end;

      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N'
      else
  // Get Pay standard hours from a user
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'Y';
        DisplayExpert(crRadioGroup, 'Pay Standard hours?', nil, VarArrayOf(['PAY_STANDARD_HOURS', 2, VarArrayOf(['Yes', 'No'])
          , VarArrayOf(['Y', 'N'])]));
      end;

  // Get Load DBDT defaults from a user
      DM_PAYROLL.PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'Y';
      DisplayExpert(crRadioGroup, 'Load payroll defaults?', nil
        , VarArrayOf(['LOAD_DBDT_DEFAULTS', 2, VarArrayOf(['Yes', 'No']), VarArrayOf(['Y', 'N'])]));

  // Get a default Check Template from a user
      Template := DisplayExpert(crLookupCombo, 'Please, select a default check template for this batch or leave it blank.'
        , DM_COMPANY.CO_PR_CHECK_TEMPLATES, VarArrayOf(['CO_PR_CHECK_TEMPLATES_NBR', 'NAME' + #9 + '40' + #9 + 'NAME'
        , 'CO_PR_CHECK_TEMPLATES_NBR']));

      if (Trim(Template) <> '') and (StrToIntDef(Trim(Template), 0) <> 0) then
        DM_PAYROLL.PR_BATCH.CO_PR_CHECK_TEMPLATES_NBR.AsInteger := StrToIntDef(Trim(Template), 0);

  // Get a Payroll Filter from a user
      DisplayExpert(crLookupCombo, 'Please, select a payroll filter for this batch or leave it blank.'
        , DM_COMPANY.CO_PR_FILTERS, VarArrayOf(['CO_PR_FILTERS_NBR', 'NAME' + #9 + '40' + #9 + 'NAME'
        , 'CO_PR_FILTERS_NBR']));
    end;    
  finally
    Free;
  end;
end;

function Generic_PopulatePRCheckRecord(EmployeeNumber: String): Integer;
var
  CheckToVoid: Integer;
  TempStr: string;
  s: string;
  bFilt: Boolean;
  OnFR: TFilterRecordEvent;
  QtrOfVoided, QtrOfVoiding: Integer;
  V: variant;
begin
// Initialization part
  Result := 0;
  Payroll_Expert := TPayroll_Expert.Create(nil);
  with Payroll_Expert, ctx_PayrollCalculation do
  try
    wwdsTemp.DataSet := DM_PAYROLL.PR_CHECK;
// Get check type from a user
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_TAX_ADJUSTMENT then
    begin
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
    end
    else
    begin
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_REGULAR;
      if ctx_AccountRights.Functions.GetState('USER_CAN_VOID_CHECK') = stEnabled then
        DisplayExpert(crComboBox, 'Please, choose check type.', nil, VarArrayOf(['CHECK_TYPE', CheckType2_ComboChoices]))
      else
        DisplayExpert(crComboBox, 'Please, choose check type.', nil, VarArrayOf(['CHECK_TYPE', CheckType3_ComboChoices]));
    end;

    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
       DM_PAYROLL.PR_CHECK.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;

    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL] then
    begin
      // Get Update_Balance from a user
      DisplayExpert(crRadioGroup, 'Do you want the balance of the scheduled EDs for the manual check to be updated?', nil, VarArrayOf(['UPDATE_BALANCE', 2, VarArrayOf(['Yes', 'No']),
        VarArrayOf(['Y', 'N'])]));

    end;

    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR] then
    begin
// Get EE Number from a user
      s := DM_EMPLOYEE.EE.Filter;
      bFilt := DM_EMPLOYEE.EE.Filtered;
      OnFR := DM_EMPLOYEE.EE.OnFilterRecord;
      try
        DM_EMPLOYEE.EE.OnFilterRecord := nil;
        DM_EMPLOYEE.EE.Filter := 'Pay_Frequency=''' + DM_PAYROLL.PR_BATCH.FieldByName('Frequency').AsString + ''' and EE_ENABLED = ''Y''';
        DM_EMPLOYEE.EE.Filtered := True;
        repeat
          TempStr := DisplaySortEmployee(DM_EMPLOYEE.EE, DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
        until TempStr <> '';
      finally
        DM_EMPLOYEE.EE.OnFilterRecord := OnFR;
        DM_EMPLOYEE.EE.Filter := s;
        DM_EMPLOYEE.EE.Filtered := bFilt;
      end;

// Get a Check Template from a user
      DM_COMPANY.CO_PR_CHECK_TEMPLATES.Activate;
      TempStr := DisplayExpert(crLookupCombo, 'Please, select a check template for the check or leave it blank.'
        , DM_COMPANY.CO_PR_CHECK_TEMPLATES, VarArrayOf(['CO_PR_CHECK_TEMPLATES_NBR', 'NAME' + #9 + '40' + #9 + 'NAME'
        , DM_PAYROLL.PR_BATCH.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').AsString,
          ConvertNull(DM_COMPANY.CO_PR_CHECK_TEMPLATES.Lookup('CO_PR_CHECK_TEMPLATES_NBR'
          , DM_PAYROLL.PR_BATCH.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').Value, 'NAME'), '')]));
      if TempStr <> '' then
      begin
        ctx_DataAccess.TemplateNumber := StrToInt(TempStr);
        Result := ctx_DataAccess.TemplateNumber;
        AssignCheckDefaults(StrToInt(TempStr), GetNextPaymentSerialNbr);
        ctx_DataAccess.TemplateNumber := 0;
      end
      else
      begin
        ctx_DataAccess.TemplateNumber := 0;      
        AssignCheckDefaults(0, GetNextPaymentSerialNbr);
      end;

      DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value, []);
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_REGULAR;
      DM_CLIENT.CL_E_DS.DataRequired('ALL');
// This line is to fix "empty earnings" bug. Could not figure out any other way yet.
  // SR-10
      DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
      DM_PAYROLL.PR_CHECK.FieldByName('ABA_NUMBER').Value := GetABANumber;
      DM_PAYROLL.PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);

  // SR-20 End
    end
    else if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_COBRA_CREDIT] then
    begin
      if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_COBRA_CREDIT] then
      begin
// Get EE Number from a user
        s := DM_EMPLOYEE.EE.Filter;
        bFilt := DM_EMPLOYEE.EE.Filtered;
        OnFR := DM_EMPLOYEE.EE.OnFilterRecord;
        try
          DM_EMPLOYEE.EE.OnFilterRecord := nil;
          if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] = CHECK_TYPE2_COBRA_CREDIT then
            DM_EMPLOYEE.EE.Filter := 'EE_NBR in (' + GetCobraWizardEEs(V, False) + ')'
          else
            DM_EMPLOYEE.EE.Filter := 'Pay_Frequency=''' + DM_PAYROLL.PR_BATCH.FieldByName('Frequency').AsString + ''' and EE_ENABLED = ''Y''';
          DM_EMPLOYEE.EE.Filtered := True;
          repeat
            TempStr := DisplaySortEmployee(DM_EMPLOYEE.EE, DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
          until TempStr <> '';
        finally
          DM_EMPLOYEE.EE.OnFilterRecord := OnFR;
          DM_EMPLOYEE.EE.Filter := s;
          DM_EMPLOYEE.EE.Filtered := bFilt;
        end;

        if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
          DisplayExpert(crEdit, 'Please, enter check number', nil, VarArrayOf(['PAYMENT_SERIAL_NUMBER']))
        else
          while Trim(DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString) = '' do
            DisplayExpert(crEdit, 'Please, enter check number', nil, VarArrayOf(['PAYMENT_SERIAL_NUMBER']));

        if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_COBRA_CREDIT] then
          AssignCheckDefaults(0, 0)
        else
        begin
          AssignCheckDefaults(0, GetNextPaymentSerialNbr);
          ctx_DataAccess.TemplateNumber := 0;
        end;
      end;

      DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
      DM_PAYROLL.PR_CHECK.FieldByName('ABA_NUMBER').Value := GetABANumber;
    end
    else if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        CheckToVoid := 0;
        CheckFinder := TCheckFinder.Create(nil);
        with CheckFinder, DM_PAYROLL do
        try
          CheckFinder.editEENbr.Text := EmployeeNumber;
          CheckFinder.DisableShowButton := True;
          CheckFinder.VoidCheckControl := True;            
          AdditionalFilter := '((C.CHECK_STATUS=''' + CHECK_STATUS_OUTSTANDING + ''') or (C.CHECK_STATUS=''' + CHECK_STATUS_OBC_SENT
            + ''')) and (P.CHECK_DATE<=''' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString
            + ''') and (C.CHECK_TYPE<>''' + CHECK_TYPE2_3RD_PARTY + ''')';
          if ShowModal = mrOK then
            CheckToVoid := CheckNumber;
        finally
          CheckFinder.Free;
        end;
        if CheckToVoid = 0 then
        begin
          DM_PAYROLL.PR_CHECK.Cancel;
          AbortEx;
        end;

        if Active then Close;
        with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
        begin
          SetMacro('Columns', 'CHECK_DATE');
          SetMacro('Table1', 'PR');
          SetMacro('Table2', 'PR_CHECK');
          SetMacro('NbrField', 'PR_CHECK');
          SetParam('RecordNbr', CheckToVoid);
          SetMacro('JoinField', 'PR');
          DataRequest(AsVariant);
        end;
        Open;
        First;
        if GetYear(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) <> GetYear(FieldByName('CHECK_DATE').Value) then
          raise ECanNotPerformOperation.CreateHelp('Voided check date year is not the same as your payroll check date year.', IDH_CanNotPerformOperation);

        QtrOfVoided := GetQuarterNumber(FieldByName('CHECK_DATE').Value);
        QtrOfVoiding := GetQuarterNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);

        if (QtrOfVoided < QtrOfVoiding) and (ctx_AccountRights.Functions.GetState('USER_CAN_VOID_PREV_QTR_CHECK') <> stEnabled) then
        begin
          EvMessage('This check is from a previous quarter and you do not have privileges to void checks from previous quarters', mtWarning, [mbOk]);
          DM_PAYROLL.PR_CHECK.Cancel;
          AbortEx;
        end;

        if QtrOfVoiding <> QtrOfVoided then
          if EvMessage('Voided check date quarter is not the same as your payroll check date quarter.' + #10 + #13
            + 'Would you like to continue?', mtWarning, [mbYes, mbNo]) <> mrYes then
          begin
            DM_PAYROLL.PR_CHECK.Cancel;
            AbortEx;
          end;
        Close;
        DM_PAYROLL.PR_CHECK.FieldByName('FILLER').Value := PutIntoFiller(ConvertNull(DM_PAYROLL.PR_CHECK.FieldByName('FILLER').Value, ''), IntToStr(CheckToVoid), 1, 8);
      end
    else
// Souldn't get here since we're dealing with all check types prior to this point
    begin
      EvErrMessage('The check type you are trying to create is not available yet!');
      DM_PAYROLL.PR_CHECK.Cancel;
    end;
  finally
    Free;
  end;
end;

procedure PopulateStringGrid(MyStringGrid: TevStringGrid; OLD_PR_CHECK, OLD_PR_CHECK_LINES, OLD_PR_CHECK_STATES,
  OLD_PR_CHECK_SUI, OLD_PR_CHECK_LOCALS: TevClientDataSet);
var
  CheckNumber, DRow, ERow: Integer;
  LastCode, LastName, TypeOfLastCode, TempStr: string;
  ItemTotal, ItemTotalHours, ETotal, DTotal, TTotal, TaxMTotal: Real;
  MyStringList: TStringList;
  I: integer;
  PR_CHECK_STATES: TevClientDataSet;
  PR_CHECK_LINES: TevClientDataSet;
  PR_CHECK_SUI: TevClientDataSet;
  PR_CHECK_LOCALS: TevClientDataSet;
  PR_CHECK: TevClientDataSet;
  LocalTaxes: IisParamsCollection;

  procedure IncDRow;
  begin
    if DRow = MyStringGrid.RowCount then MyStringGrid.RowCount := MyStringGrid.RowCount + 1;
    Inc(DRow);
  end;

  procedure CopyStringList;
  var
    I: Integer;
  begin
    for I := 0 to MyStringList.Count - 1 do
    begin
      IncDRow;
      MyStringGrid.Cells[1, DRow - 1] := MyStringList.Strings[I];
    end;
    MyStringList.Clear;
  end;

begin
  PR_CHECK_STATES := TevClientDataSet.Create(nil);
  PR_CHECK_LINES := TevClientDataSet.Create(nil);
  PR_CHECK_SUI := TevClientDataSet.Create(nil);
  PR_CHECK_LOCALS := TevClientDataSet.Create(nil);
  PR_CHECK := TevClientDataSet.Create(nil);

  ctx_StartWait;
  try
    PR_CHECK_STATES.CloneCursor(OLD_PR_CHECK_STATES, False);
    PR_CHECK_LINES.CloneCursor(OLD_PR_CHECK_LINES, False);
    PR_CHECK_SUI.CloneCursor(OLD_PR_CHECK_SUI, False);
    PR_CHECK_LOCALS.CloneCursor(OLD_PR_CHECK_LOCALS, False);
    PR_CHECK.CloneCursor(OLD_PR_CHECK, False);

    with MyStringGrid do
    begin
      for I := 0 to RowCount - 1 do
      begin
        Cells[0, I] := '';
        Cells[1, I] := '';
      end;

      RowCount := 2;
      CheckNumber := OLD_PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
      PR_CHECK.Locate('PR_CHECK_NBR', CheckNumber, []);
      Cells[0, RowCount - 1] := 'EARNINGS:';
      Cells[1, RowCount - 1] := 'DEDUCTIONS:';
      RowCount := RowCount + 2;
      ERow := RowCount;
      DRow := RowCount;

      PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
      PR_CHECK_LINES.IndexFieldNames := 'CL_E_DS_NBR';
      PR_CHECK_LINES.Filtered := True;
      PR_CHECK_LINES.First;
      ItemTotal := 0;
      ItemTotalHours := 0;
      ETotal := 0;
      TaxMTotal := 0;
      DTotal := 0;
  (*    LastCode:=PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString;
      TypeOfLastCode:={DM_CLIENT.}CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
      LastName:=PR_CHECK_LINES.FieldByName('E_D_Description_Lookup').AsString;*)
      LastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER'));
      TypeOfLastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'));
      LastName := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION'));
      while not PR_CHECK_LINES.EOF do
      begin
        if LastCode <> {PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString} VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR',
          PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER')) then
        begin
          if (ItemTotal <> 0) or (ItemTotalHours <> 0) then
          begin
            if Copy(TypeOfLastCode, 1, 1) = 'D' then
            begin
              Cells[1, DRow - 1] := LastCode + ' ' + LastName + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
              if DRow = RowCount then RowCount := RowCount + 1;
              Inc(DRow);
              DTotal := DTotal + ItemTotal;
            end;
            if Copy(TypeOfLastCode, 1, 1) = 'E' then
            begin
              Cells[0, ERow - 1] := LastCode + ' ' + LastName;
              if ItemTotalHours <> 0 then
                Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' (' + FloatToStr(ItemTotalHours) + ')';
              Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
              if TypeIsTaxableMemo(TypeOfLastCode) then
              begin
                Cells[0, ERow - 1] := '*' + Cells[0, ERow - 1];
                TaxMTotal := TaxMTotal + ItemTotal;
              end;
              if ERow = RowCount then RowCount := RowCount + 1;
              Inc(ERow);
              ETotal := ETotal + ItemTotal;
            end;
          end;
          ItemTotal := ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
          ItemTotalHours := ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
  (*        LastCode:=PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString;
          TypeOfLastCode:={DM_CLIENT.}CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
          LastName:=PR_CHECK_LINES.FieldByName('E_D_Description_Lookup').AsString;*)
          LastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER'));
          TypeOfLastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'));
          LastName := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION'));
        end
        else
        begin
          ItemTotal := ItemTotal + ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
          ItemTotalHours := ItemTotalHours + ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
        end;
        PR_CHECK_LINES.Next;
      end;
      if Copy(TypeOfLastCode, 1, 1) = 'D' then
      begin
        Cells[1, DRow - 1] := LastCode + ' ' + LastName + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
        if DRow = RowCount then RowCount := RowCount + 1;
        Inc(DRow);
        DTotal := DTotal + ItemTotal;
      end;
      if Copy(TypeOfLastCode, 1, 1) = 'E' then
      begin
        Cells[0, ERow - 1] := LastCode + ' ' + LastName;
        if ItemTotalHours <> 0 then
          Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' (' + FloatToStr(ItemTotalHours) + ')';
        Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
        if TypeIsTaxableMemo(TypeOfLastCode) then
        begin
          Cells[0, ERow - 1] := '*' + Cells[0, ERow - 1];
          TaxMTotal := TaxMTotal + ItemTotal;
        end;
        if ERow = RowCount then RowCount := RowCount + 1;
        Inc(ERow);
        ETotal := ETotal + ItemTotal;
      end;
      if DTotal <> 0 then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'Deductions SubTotal = ' + FloatToStrF(DTotal, ffCurrency, 15, 2);
        IncDRow;
        IncDRow;
      end;
      if (ETotal <> 0) or (TaxMTotal <> 0) then
      begin
        if ERow = RowCount then RowCount := RowCount + 1;
        Inc(ERow);
        if TaxMTotal = 0 then
          Cells[0, ERow - 1] := 'Earnings SubTotal = ' + FloatToStrF(ETotal, ffCurrency, 15, 2)
        else
          Cells[0, ERow - 1] := 'Earnings SubTotal = ' + FloatToStrF(ETotal, ffCurrency, 15, 2) + ' - ' +
            FloatToStrF(TaxMTotal, ffCurrency, 15, 2) + ' = ' + FloatToStrF(ETotal - TaxMTotal, ffCurrency, 15, 2);
      end;

      if ERow = RowCount then RowCount := RowCount + 1;
      Inc(ERow);
      if ERow = RowCount then RowCount := RowCount + 1;
      Inc(ERow);
      Cells[0, ERow - 1] := 'MEMOS:';
      if ERow = RowCount then RowCount := RowCount + 1;
      Inc(ERow);
      if ERow = RowCount then RowCount := RowCount + 1;
      Inc(ERow);
  //------------------------------------------------------
      PR_CHECK_LINES.First;
      ItemTotal := 0;
      ItemTotalHours := 0;
      ETotal := 0;
  (*    LastCode:=PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString;
      TypeOfLastCode:={DM_CLIENT.}CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
      LastName:=PR_CHECK_LINES.FieldByName('E_D_Description_Lookup').AsString;*)
      LastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER'));
      TypeOfLastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'));
      LastName := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION'));
      while not PR_CHECK_LINES.EOF do
      begin
        if LastCode <> {PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString} VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR',
          PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER')) then
        begin
          if (ItemTotal <> 0) or (ItemTotalHours <> 0) then
          begin
            if Copy(TypeOfLastCode, 1, 1) = 'M' then
            begin
              Cells[0, ERow - 1] := LastCode + ' ' + LastName;
              if ItemTotalHours <> 0 then
                Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' (' + FloatToStr(ItemTotalHours) + ')';
              Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
              if ERow = RowCount then RowCount := RowCount + 1;
              Inc(ERow);
              ETotal := ETotal + ItemTotal;
            end;
          end;
          ItemTotal := ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
          ItemTotalHours := ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
  (*        LastCode:=PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString;
          TypeOfLastCode:={DM_CLIENT.}CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
          LastName:=PR_CHECK_LINES.FieldByName('E_D_Description_Lookup').AsString;*)
          LastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER'));
          TypeOfLastCode := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'));
          LastName := VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION'));
        end
        else
        begin
          ItemTotal := ItemTotal + ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
          ItemTotalHours := ItemTotalHours + ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
        end;
        PR_CHECK_LINES.Next;
      end;
      if Copy(TypeOfLastCode, 1, 1) = 'M' then
      begin
        Cells[0, ERow - 1] := LastCode + ' ' + LastName;
        if ItemTotalHours <> 0 then
          Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' (' + FloatToStr(ItemTotalHours) + ')';
        Cells[0, ERow - 1] := Cells[0, ERow - 1] + ' = ' + FloatToStrF(ItemTotal, ffCurrency, 15, 2);
        if ERow = RowCount then RowCount := RowCount + 1;
        Inc(ERow);
        ETotal := ETotal + ItemTotal;
      end;
      if ETotal <> 0 then
      begin
        if ERow = RowCount then RowCount := RowCount + 1;
        Inc(ERow);
        Cells[0, ERow - 1] := 'Memos SubTotal = ' + FloatToStrF(ETotal, ffCurrency, 15, 2);
      end;
  //------------------------------------------------------

      PR_CHECK_LINES.Filtered := False;
      PR_CHECK_LINES.IndexFieldNames := '';

      TTotal := 0;
      Cells[1, DRow - 1] := 'FEDERAL TAXES:';
      IncDRow;
      if ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0) <> 0 then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'OASDI Tax = ' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0), ffCurrency, 15, 2) +
          ' ('
          + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value, 0), ffCurrency, 15, 2) + '/'
          + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value, 0), ffCurrency, 15, 2) + ')';
        TTotal := TTotal + ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);
      end;
      if ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0) <> 0 then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'Medicare Tax = ' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0), ffCurrency,
          15, 2) + ' ('
          + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').Value, 0), ffCurrency, 15, 2) + ')';
        TTotal := TTotal + ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);
      end;
      if (ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) <> 0) or (ConvertNull(PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value,
        0) <> 0) then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'Federal Tax = ' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) +
          ConvertNull(PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value, 0), ffCurrency, 15, 2);
        if ConvertNull(PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value, 0) <> 0 then
          Cells[1, DRow - 1] := Cells[1, DRow - 1] + '-' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value, 0),
            ffCurrency, 15, 2);
        Cells[1, DRow - 1] := Cells[1, DRow - 1] + ' (' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value, 0),
          ffCurrency, 15, 2) + ')';
        TTotal := TTotal + ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0);
      end;
      if ConvertNull(PR_CHECK.FieldByName('EE_EIC_TAX').Value, 0) <> 0 then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'EIC Tax = ' + FloatToStrF(PR_CHECK.FieldByName('EE_EIC_TAX').Value * (-1), ffCurrency, 15, 2);
        TTotal := TTotal - ConvertNull(PR_CHECK.FieldByName('EE_EIC_TAX').Value, 0);
      end;
      if ConvertNull(PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value, 0) <> 0 then
      begin
        IncDRow;
        Cells[1, DRow - 1] := 'Backup Withholding = ' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value,
          0), ffCurrency, 15, 2);
        TTotal := TTotal + ConvertNull(PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value, 0);
      end;

      MyStringList := TStringList.Create;
      MyStringList.Duplicates := dupAccept;
      with MyStringList do
      try
        Sorted := True;

        PR_CHECK_STATES.First;
        while not PR_CHECK_STATES.EOF do
        begin
          if (ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0) <> 0) or
            (ConvertNull(PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value, 0) <> 0) then
          begin
            TempStr := {PR_CHECK_STATES.FieldByName('State_Lookup').AsString} VarToStr(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR',
              DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value, 'CO_STATES_NBR'), 'STATE')) +
              ' State Tax = '
              + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0) +
                ConvertNull(PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value, 0), ffCurrency, 15, 2);
            if ConvertNull(PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value, 0) <> 0 then
              TempStr := TempStr + '-' + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value, 0), ffCurrency, 15,
                2);
            TempStr := TempStr + ' (' + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value, 0), ffCurrency,
              15, 2) + ')';
            TTotal := TTotal + ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0);
            Add(TempStr);
          end;
          if (ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, 0) <> 0) or
            (ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').Value, 0) <> 0) then
          begin
            TempStr := {PR_CHECK_STATES.FieldByName('State_Lookup').AsString} VarToStr(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR',
              DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value, 'CO_STATES_NBR'), 'STATE')) +
              ' SDI Tax = '
              + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, 0) +
                ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').Value, 0), ffCurrency, 15, 2);
            if ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').Value, 0) <> 0 then
              TempStr := TempStr + '-' + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').Value, 0), ffCurrency, 15,
                2);
            TempStr := TempStr + ' (' + FloatToStrF(ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0), ffCurrency,
              15, 2) + ')';
            TTotal := TTotal + ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, 0);
            Add(TempStr);
          end;
          PR_CHECK_STATES.Next;
        end;
        if Count <> 0 then
        begin
          IncDRow;
          IncDRow;
          Cells[1, DRow - 1] := 'STATE TAXES:';
          IncDRow;
          CopyStringList;
        end;

        DM_SYSTEM_STATE.SY_SUI.Activate;
        PR_CHECK_SUI.First;
        while not PR_CHECK_SUI.EOF do
        begin
          if (ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAX').Value, 0) <> 0) then
          begin
            if not Is_ER_SUI(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR',
            PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'),
            'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) then
            begin
              Add(DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', PR_CHECK_SUI.FieldByName('CO_SUI_NBR').AsString, 'Description') + ' = ' +
                FloatToStrF(ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAX').Value, 0), ffCurrency, 15, 2) + ' ('
                + FloatToStrF(ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value, 0), ffCurrency, 15, 2) + ')');
              TTotal := TTotal + ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAX').Value, 0);
            end;
          end;
          PR_CHECK_SUI.Next;
        end;
        if Count <> 0 then
        begin
          IncDRow;
          IncDRow;
          Cells[1, DRow - 1] := 'SUI TAXES:';
          IncDRow;
          CopyStringList;
        end;

        LocalTaxes := TisParamsCollection.Create;
        PR_CHECK_LOCALS.First;
        while not PR_CHECK_LOCALS.EOF do
        begin
          if (ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value, 0) <> 0) or
          (ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value, 0) <> 0) then
          begin
            if not Assigned(LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString)) then
            begin
              LocalTaxes.AddParams(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString);
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).AddValue('LOCAL_TAX', ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value, 0));
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).AddValue('LOCAL_SHORTFALL', ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value, 0));
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).AddValue('LOCAL_TAXABLE_WAGE', ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value, 0));
            end
            else
            begin
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_TAX'] :=
                LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_TAX'] + ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value, 0);
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_SHORTFALL'] :=
                LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_SHORTFALL'] + ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value, 0);
              LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_TAXABLE_WAGE'] :=
                LocalTaxes.ParamsByName(PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsString).Value['LOCAL_TAXABLE_WAGE'] + ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value, 0);
            end;
          end;
          PR_CHECK_LOCALS.Next;
        end;


        for I := 0 to LocalTaxes.Count - 1 do
        begin
          if DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',
          DM_EMPLOYEE.EE_LOCALS.Lookup('EE_LOCALS_NBR',
          LocalTaxes.ParamName(I),
          'CO_LOCAL_TAX_NBR'), 'SY_LOCALS_NBR'), 'TAX_TYPE') = GROUP_BOX_EE then
          begin
            TempStr := VarToStr(DM_EMPLOYEE.EE_LOCALS.Lookup('EE_LOCALS_NBR', LocalTaxes.ParamName(I), 'Local_Name_Lookup')) + ' = '
              + FloatToStrF(LocalTaxes.Params[I].Value['LOCAL_TAX'] + LocalTaxes.Params[I].Value['LOCAL_SHORTFALL'], ffCurrency, 15, 2);
            if LocalTaxes.Params[I].Value['LOCAL_SHORTFALL'] <> 0 then
              TempStr := TempStr + '-' + FloatToStrF(LocalTaxes.Params[I].Value['LOCAL_SHORTFALL'], ffCurrency, 15, 2);
            TempStr := TempStr + ' (' + FloatToStrF(LocalTaxes.Params[I].Value['LOCAL_TAXABLE_WAGE'], ffCurrency,
              15, 2) + ')';
            TTotal := TTotal + LocalTaxes.Params[I].Value['LOCAL_TAX'];
            Add(TempStr);
          end;
        end;
        if LocalTaxes.Count <> 0 then
        begin
          IncDRow;
          IncDRow;
          Cells[1, DRow - 1] := 'LOCAL TAXES:';
          IncDRow;
          CopyStringList;
        end;

      finally
        MyStringList.Free;
      end;

      IncDRow;
      IncDRow;
      Cells[1, DRow - 1] := 'Taxes SubTotal = ' + FloatToStrF(TTotal, ffCurrency, 15, 2);
      IncDRow;
      IncDRow;
      Cells[1, DRow - 1] := 'Net Amount = ' + FloatToStrF(ConvertNull(PR_CHECK.FieldByName('NET_WAGES').Value, 0), ffCurrency, 15, 2);
    end;

  finally
    PR_CHECK_STATES.Free;
    PR_CHECK_LINES.Free;
    PR_CHECK_SUI.Free;
    PR_CHECK_LOCALS.Free;
    PR_CHECK.Free;
    ctx_EndWait;
  end;
end;

procedure PopulatePRMiscCheckRecord;
var
  CheckToVoid: Integer;
  CheckTypeToVoid: Char;
  DataSet: TDataSet;
  Misc_CheckFinder: TMisc_CheckFinder;
begin
  DataSet := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS;
  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_TAX_ADJUSTMENT then
  begin
    DataSet.Cancel;
    raise ECanNotInsertMiscCheck.CreateHelp('You can not create misc. checks on Tax Adjustment Payroll!', IDH_CanNotPerformOperation);
  end;

  Payroll_Expert := TPayroll_Expert.Create(nil);
  with Payroll_Expert do
  try
    wwdsTemp.DataSet := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS;
// Get misc. check type from a user
    DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY;
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT then
    begin
      DisplayExpert(crComboBox, 'Please, choose miscellaneous check type.', nil, VarArrayOf(['MISCELLANEOUS_CHECK_TYPE',
        'Agency' + #9 + MISC_CHECK_TYPE_AGENCY + #13 + 'Tax' + #9 + MISC_CHECK_TYPE_TAX + #13 + 'Billing' + #9 + MISC_CHECK_TYPE_BILLING +
          #13 + 'Void' + #9 + MISC_CHECK_TYPE_AGENCY_VOID]));
    end
    else
    begin
      DisplayExpert(crComboBox, 'Please, choose miscellaneous check type.', nil, VarArrayOf(['MISCELLANEOUS_CHECK_TYPE',
        'Agency' + #9 + MISC_CHECK_TYPE_AGENCY + #13 + 'Void' + #9 + MISC_CHECK_TYPE_AGENCY_VOID]));
    end;

    if DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value = MISC_CHECK_TYPE_AGENCY_VOID then
    begin
      CheckToVoid := 0;
      CheckTypeToVoid := ' ';
      Misc_CheckFinder := TMisc_CheckFinder.Create(nil);
      with Misc_CheckFinder, DM_PAYROLL do
      try
        AdditionalFilter := '((M.CHECK_STATUS=''' + MISC_CHECK_STATUS_OUTSTANDING + ''') or (M.CHECK_STATUS=''' + MISC_CHECK_STATUS_PENDING_FOR_ACH
          + ''')) and (P.CHECK_DATE<=''' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + ''')';
        if ShowModal = mrOK then
        begin
          CheckToVoid := CheckNumber;
          CheckTypeToVoid := CheckType;
        end;
      finally
        Misc_CheckFinder.Free;
      end;
      if CheckToVoid = 0 then
      begin
        DataSet.Cancel;
        AbortEx;
      end;
      DataSet.FieldByName('FILLER').Value := PutIntoFiller(ConvertNull(DataSet.FieldByName('FILLER').Value, ''), IntToStr(CheckToVoid), 1, 8);
      case CheckTypeToVoid of
        MISC_CHECK_TYPE_AGENCY:
          DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY_VOID;
        MISC_CHECK_TYPE_TAX:
          DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_TAX_VOID;
        MISC_CHECK_TYPE_BILLING:
          DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_BILLING_VOID;
      end;
    end
    else
    if DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value = MISC_CHECK_TYPE_BILLING then
    begin
      DataSet.FieldByName('EFTPS_TAX_TYPE').Value := 'N';
      DisplayExpert(crComboBox, 'Please, choose billing check type.', nil, VarArrayOf(['EFTPS_TAX_TYPE',
        'Regular' + #9 + 'N' + #13 + 'Tax Impound' + #9 + 'T' + #13 + 'Trust Impound' + #9 + 'R']));
      if DataSet.FieldByName('EFTPS_TAX_TYPE').Value = 'T' then
        DataSet.FieldByName('OVERRIDE_INFORMATION').AsString := 'Tax Impound Check';
      if DataSet.FieldByName('EFTPS_TAX_TYPE').Value = 'R' then
        DataSet.FieldByName('OVERRIDE_INFORMATION').AsString := 'Trust Impound Check';
    end;
  finally
    Free;
  end;
end;

procedure AutoCreateCompanies(CompanyList: TStringList);
var
  DoImport, DoProcessing, FourDigitYear, AutoImportJobCodes, ErrorEncountered: Boolean;
  ImportFolder, S: String;
  I, PrNbr: Integer;
  j: LongInt;
  ErrorsList: TStringList;
  ImportLookupOption: TCImportLookupOption;
  DBDTOption: TCImportDBDTOption;
  FileFormatOption: TCFileFormatOption;
  UseEmployeePayRates: boolean;
  NextCheckDate: TDateTime;
  aStream: IisStream;
  Log: string;
begin
  if CompanyList.Count = 0 then
    Exit;
  DoImport := (EvMessage('Would you like to run time clock imports after payrolls are created?' + #13 +
    '(This will require selecting a folder with import files for each payroll you intend to import.' + #13 +
    'These files should have .imp or .txt extension and have the same name as a company number you are importing.' + #13 +
    'All exceptions will be reported in .exp files.)',
    mtConfirmation, [mbYes, mbNo], mbNo) = mrYes);
  ImportLookupOption := loBySSN;
  DBDTOption := loSmart;
  FourDigitYear := False;
  AutoImportJobCodes := False;
  FileFormatOption := loCommaDelimited;
  UseEmployeePayRates := False;
  if DoImport then
  begin
    Import_Options := TImport_Options.Create(Nil);
    try
      ImportFolder := mb_AppSettings['ImportFolder'];
      Import_Options.editImportFolder.Text := ImportFolder;
      Import_Options.ShowModal;
      ImportFolder := Import_Options.editImportFolder.Text;
      FourDigitYear := Import_Options.cbFourDigits.Checked;
      AutoImportJobCodes := Import_Options.cbAutoImportJobCode.Checked;
      if Import_Options.radioImportLookByNumber.Checked then
        ImportLookupOption := loByNumber;
      if Import_Options.radioImportLookByName.Checked then
        ImportLookupOption := loByName;
      if Import_Options.radioImportMatchDbdtFull.Checked then
        DBDTOption := loFull;
      if Import_Options.radioImportFormatFFF.Checked then
        FileFormatOption := loFixed;
      UseEmployeePayRates := Import_Options.cbUseEmployeePayRates.Checked;
    finally
      Import_Options.Free;
    end;
    if ImportFolder[Length(ImportFolder)] <> '\' then
      ImportFolder := ImportFolder + '\';
    mb_AppSettings['ImportFolder'] := ImportFolder;
  end;
  DoProcessing := (EvMessage('Would you like to auto-process created payrolls?', mtConfirmation, [mbYes, mbNo], mbNo) = mrYes);

  if EvMessage('Are you sure you want to auto-create payrolls for selected companies?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  ErrorsList := TStringList.Create;
  with DM_PAYROLL do
  try
    for I := 0 to CompanyList.Count - 1 do
    begin
      ErrorEncountered := False;
      ctx_StartWait('Preparing...');
      try
        ctx_DataAccess.OpenClient(StrToInt(CompanyList.Names[I]));
        S := CompanyList[I];
        DM_COMPANY.CO.DataRequired('CO_NBR' + Copy(S, Pos('=', S), Length(S)));
        NextCheckDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
        DM_PAYROLL.PR_SCHEDULED_EVENT.Close;
      finally
        ctx_EndWait;
      end;
      if NextCheckDate = 0  then
      begin
        ErrorsList.Add('Creating PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': There are no more check dates available in the calendar.');
        Continue;
      end;

      PrNbr := 0;
      try
        PrNbr := ctx_PayrollProcessing.CreatePR(StrToInt(Copy(S, Pos('=', S) + 1, Length(S))), NextCheckDate);
      except
        on E: Exception do
        begin
          ErrorsList.Add('Creating PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': ' + E.Message);
          ErrorEncountered := True;
        end;
      end;

      if not ErrorEncountered and (DoImport or DoProcessing) then
        DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(PrNbr));

      if not ErrorEncountered and DoImport then
      begin
        if FileExists(ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.imp')
        or FileExists(ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.txt') then
        try
          PopulateDataSets([DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
            DM_CLIENT.CL_PERSON, DM_EMPLOYEE.EE_SCHEDULED_E_DS], '');
          ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
            DM_CLIENT.CL_PERSON, DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
          DM_EMPLOYEE.EE.DataRequired('CO_NBR='+ IntToStr(DM_COMPANY.CO['CO_NBR']));
          DM_PAYROLL.PR_BATCH.DataRequired('PR_NBR='+ IntToStr(PrNbr));
          DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+ IntToStr(PrNbr));
          DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=-1');
          try
            if FileExists(ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.imp') then
              ProcessTCImport(ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.imp',
                ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.exp',
                ImportLookupOption, DBDTOption, FourDigitYear, FileFormatOption, AutoImportJobCodes,
                UseEmployeePayRates)
            else
              ProcessTCImport(ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.txt',
                ImportFolder + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '.exp',
                ImportLookupOption, DBDTOption, FourDigitYear, FileFormatOption, AutoImportJobCodes,
                UseEmployeePayRates);
            ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES]);
          except
            ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES]);
            raise;
          end;
        except
          on E: Exception do
          begin
            ErrorsList.Add('Importing PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': ' + E.Message);
            ErrorEncountered := True;
          end;
        end
        else
          ErrorsList.Add('Importing PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': No import file found.');
      end;

      if not ErrorEncountered and DoProcessing then
      begin
        try
          PR.Edit;
          PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
          PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString:=DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').AsString;
          PR.Post;
          ctx_DataAccess.PostDataSets([PR]);
          Sleep(1000);
          ctx_PayrollProcessing.ProcessPayroll(PR.FieldByName('PR_NBR').Value, False, Log, True);
          aStream := ctx_PayrollProcessing.PrintPayroll(PR.FieldByName('PR_NBR').Value, True);
          aStream.Position := aStream.Size- SizeOf(j);
          j := aStream.ReadInteger;
          aStream.Position := j;
          S := aStream.ReadString;
          S := aStream.ReadString;
          aStream.Position := 0;
          if ctx_VmrEngine <> nil then
            if ctx_VmrEngine.VmrPostProcessReportResult(aStream) then
              ErrorsList.Add('Some of the reports for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+ ' have been submitted to mail room');
          ctx_RWLocalEngine.Print(aStream);
        except
          on E: Exception do
          begin
            ErrorsList.Add('Processing PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': ' + E.Message);
            if PR.State in [dsInsert, dsEdit] then
              PR.Cancel;
            ctx_DataAccess.CancelDataSets([PR]);
          end;
        end;
      end;
    end;

    if ErrorsList.Count > 0 then
    begin
      Warnings_List := TWarnings_List.Create(Nil);
      try
        Warnings_List.WarningsListBox.Items.Assign(ErrorsList);
        if Warnings_List.ShowModal <> mrOK then
          ShowMessage('You can not cancel this action!');
      finally
        Warnings_List.Free;
      end;
    end
    else
    begin
      SysUtils.Beep;
      ShowMessage('Success!');
    end;
  finally
    ErrorsList.Free;
  end;
end;

end.

