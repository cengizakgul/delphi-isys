object TaxCalculator: TTaxCalculator
  Left = 547
  Top = 151
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Tax and Check Line Calculator'
  ClientHeight = 674
  ClientWidth = 870
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TevPageControl
    Left = 0
    Top = 0
    Width = 870
    Height = 674
    ActivePage = TabSheet1
    Align = alClient
    Images = PageControlImages
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Tax'
      OnShow = TabSheet1Show
      object fpStates: TisUIFashionPanel
        Left = 475
        Top = 8
        Width = 380
        Height = 147
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpStates'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'States'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evDBGrid2: TevDBGrid
          Left = 12
          Top = 35
          Width = 345
          Height = 90
          DisableThemesInTitle = False
          ControlType.Strings = (
            'State_Lookup;CustomEdit;lcStateState'
            'STATE_MARITAL_STATUS;CustomEdit;lcStateMaritalStatus'
            'STATE_OVERRIDE_TYPE;CustomEdit;cbStateORType'
            'EXCLUDE_STATE;CheckBox;Y;N'
            'EXCLUDE_ADDITIONAL_STATE;CheckBox;Y;N'
            'TAX_AT_SUPPLEMENTAL_RATE;CheckBox;Y;N'
            'EXCLUDE_SDI;CheckBox;Y;N'
            'EXCLUDE_SUI;CheckBox;Y;N')
          Selected.Strings = (
            'State_Lookup'#9'6'#9'State'#9'F'
            'STATE_MARITAL_STATUS'#9'2'#9'Marital St'#9'F'
            'STATE_NUMBER_WITHHOLDING_ALLOW'#9'8'#9'With Allow'#9'F'
            'STATE_TAXABLE_WAGES'#9'10'#9'State Tax Wages'#9'F'
            'STATE_TAX'#9'10'#9'State Tax'#9'F'
            'EE_SDI_TAXABLE_WAGES'#9'10'#9'SDI Tax Wages'#9'F'
            'EE_SDI_TAX'#9'10'#9'SDI Tax'#9'F'
            'STATE_OVERRIDE_TYPE'#9'1'#9'State OR Type'#9'F'
            'STATE_OVERRIDE_VALUE'#9'10'#9'State OR Value'#9'F'
            'EXCLUDE_STATE'#9'1'#9'Blk State'#9'F'
            'EXCLUDE_ADDITIONAL_STATE'#9'1'#9'Blk Add State'#9'F'
            'TAX_AT_SUPPLEMENTAL_RATE'#9'1'#9'Tax at Supp Rate'#9'F'
            'EE_SDI_OVERRIDE'#9'10'#9'SDI Override'#9'F'
            'EXCLUDE_SDI'#9'1'#9'Blk SDI'#9'F'
            'EXCLUDE_SUI'#9'1'#9'Blk SUI'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TTaxCalculator\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsCHECK_STATES
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object lcStateMaritalStatus: TevDBLookupCombo
          Left = 216
          Top = 100
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'STATUS_TYPE'#9'4'#9'STATUS_TYPE'#9'F'
            'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION'#9'F')
          DataField = 'STATE_MARITAL_STATUS'
          DataSource = dsCHECK_STATES
          LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
          LookupField = 'STATUS_TYPE'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object lcStateState: TevDBLookupCombo
          Left = 80
          Top = 68
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'State_Lookup'#9'2'#9'State_Lookup'#9'F')
          DataField = 'EE_STATES_NBR'
          DataSource = dsCHECK_STATES
          LookupTable = DM_EE_STATES.EE_STATES
          LookupField = 'EE_STATES_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object cbStateORType: TevDBComboBox
          Left = 80
          Top = 100
          Width = 121
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'STATE_OVERRIDE_TYPE'
          DataSource = dsCHECK_STATES
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'None'#9'N'
            'Reg Amt'#9'A'
            'Reg %'#9'P'
            'Add Amt'#9'M'
            'Add %'#9'E')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 3
          UnboundDataType = wwDefault
        end
      end
      object fpSUI: TisUIFashionPanel
        Left = 475
        Top = 162
        Width = 380
        Height = 147
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpSUI'
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'SUI'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evDBGrid3: TevDBGrid
          Left = 12
          Top = 35
          Width = 345
          Height = 90
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Description_Lookup;CustomEdit;lcCoSUI')
          Selected.Strings = (
            'Description_Lookup'#9'20'#9'SUI'#9'F'
            'SUI_TAXABLE_WAGES'#9'10'#9'SUI Tax Wages'#9'F'
            'SUI_TAX'#9'10'#9'SUI Tax'#9'F'
            'SUI_GROSS_WAGES'#9'10'#9'SUI Gross Wages'#9'F'
            'OVERRIDE_AMOUNT'#9'10'#9'OR Amount'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TTaxCalculator\evDBGrid3'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsCHECK_SUI
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object lcCoSUI: TevDBLookupCombo
          Left = 183
          Top = 101
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'20'#9'DESCRIPTION'#9'F')
          DataField = 'CO_SUI_NBR'
          DataSource = dsCHECK_SUI
          LookupTable = DM_CO_SUI.CO_SUI
          LookupField = 'CO_SUI_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object fpLocals: TisUIFashionPanel
        Left = 475
        Top = 317
        Width = 380
        Height = 147
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 2
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Locals'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evDBGrid4: TevDBGrid
          Left = 12
          Top = 35
          Width = 345
          Height = 90
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Local_Name_Lookup;CustomEdit;lcLocal'
            'EXCLUDE_LOCAL;CheckBox;Y;N')
          Selected.Strings = (
            'Local_Name_Lookup'#9'20'#9'Local Name'#9'F'
            'LOCAL_TAXABLE_WAGE'#9'10'#9'Local Tax Wages'#9'F'
            'LOCAL_TAX'#9'10'#9'Local Tax'#9'F'
            'OVERRIDE_AMOUNT'#9'10'#9'OR Amount'#9'F'
            'EXCLUDE_LOCAL'#9'1'#9'Blk Local'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TTaxCalculator\evDBGrid4'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsCHECK_LOCALS
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object lcLocal: TevDBLookupCombo
          Left = 231
          Top = 105
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
          DataField = 'EE_LOCALS_NBR'
          DataSource = dsCHECK_LOCALS
          LookupTable = DM_EE_LOCALS.EE_LOCALS
          LookupField = 'EE_LOCALS_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object fpControls: TisUIFashionPanel
        Left = 475
        Top = 472
        Width = 380
        Height = 165
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpControls'
        Color = 14737632
        TabOrder = 3
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Controls'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object cbDisableYTDs: TevCheckBox
          Left = 12
          Top = 35
          Width = 89
          Height = 17
          Caption = 'Disable YTDs'
          TabOrder = 0
        end
        object bbtnCalculate: TevBitBtn
          Left = 148
          Top = 35
          Width = 210
          Height = 49
          Caption = '&Calculate check (F5)'
          TabOrder = 1
          OnClick = bbtnCalculateClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFDCDCDC
            CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            CCCCCCCCD3D3D3FFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
            CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCD4D4D4FFFFFFFFFFFFB0B0B0
            9F9F9F9E9E9E9E9E9E9E9F9F9E9E9E9E9E9E9E9F9F9E9E9E9D9E9F9D9E9F9D9E
            9E9F9F9FAAAAAAFFFFFFFFFFFFB1B0B0A09F9F9F9F9E9F9F9EA09F9F9F9F9E9F
            9F9EA09F9F9F9F9E9F9F9E9F9F9E9F9F9EA09F9FAAAAAAFFFFFFFFFFFF9F9F9F
            FFFEFEFBFAFAFEFDFDFEFDFDFDFCFCFEFDFDFEFDFEFBFCFDF9FCFFF9FCFFF9FA
            FCFFFEFE9F9F9FFFFFFFFFFFFFA09F9FFFFFFFFCFCFBFEFEFEFEFEFEFEFDFDFE
            FEFEFFFFFFFEFDFDFEFEFEFEFEFEFCFCFBFFFFFFA09F9FFFFFFFFFFFFF9E9E9E
            F7F6F6F2F1F08D8A886D6B68F7F6F58D8B896C6B69F2F6F9E49F55E49F55EEF2
            F5F7F6F79E9E9EFFFFFFFFFFFF9F9F9EF7F7F7F3F2F28A8A8A6A6A6AF7F7F78B
            8B8B6B6B6BF8F8F8999999999999F4F4F4F8F8F89F9F9EFFFFFFFFFFFF9D9E9E
            F4F3F2EDECEBF2F1F0F3F3F2F0F0EFF3F2F1F3F3F3EDF1F6EEAD66E4A25BE9ED
            F2F4F4F39D9E9EFFFFFFFFFFFF9F9F9EF4F4F4EDEDEDF3F2F2F4F4F4F1F1F1F3
            F3F3F4F4F4F3F3F3A7A7A79C9C9CF0EFEFF5F5F59F9F9EFFFFFFFFFFFF9E9E9E
            F3F2F0E9E8E78E8C8A6E6C6AEDECEB8F8D8A6E6C6BE9ECF0F0AC62F0AC62E5E8
            ECF2F2F19E9E9EFFFFFFFFFFFF9F9F9EF3F3F3E9E9E98C8C8C6C6C6CEDEDED8D
            8D8D6C6C6CEEEEEEA6A6A6A6A6A6EAEAEAF3F3F39F9F9EFFFFFFFFFFFF9E9E9E
            F0EEEEE4E2E2E9E7E7EAE8E9E8E6E6E9E7E8EAE8E9E6E5E7E5E6EBE4E6EBE2E2
            E4F0EEEE9E9E9EFFFFFFFFFFFF9F9F9EF0EFEFE4E3E3E8E8E8EAEAEAE8E7E7E8
            E8E8EAEAEAE7E7E6E8E8E8E8E8E8E4E3E3F0EFEF9F9F9EFFFFFFFFFFFF9E9F9F
            EEEDEDE0DEDD918E8A716F6BE5E2E2918F8C706E6CE4E2E2918F8C706E6AE1DF
            DEEEEDEE9E9F9FFFFFFFFFFFFFA09F9FEFEEEEE0DFDF8E8E8E6E6E6EE4E3E38F
            8F8F6D6D6DE4E3E38F8F8F6D6D6DE0E0E0EFEEEEA09F9FFFFFFFFFFFFF9F9F9F
            EEECECDDD9D5E4DED6E5DFD7E0DDD9E0DEDDE1DFDEE1DDD9E5DFD7E5DFD6DDD9
            D5EEEDEC9F9F9FFFFFFFFFFFFFA09F9FEDEDEDDADADADEDEDEDFDFDEDEDEDEE0
            DFDFE0E0E0DEDEDEDFDFDEDFDFDEDADADAEEEEEEA09F9FFFFFFFFFFFFF9F9F9F
            EDECEADAD5CE1F7FFF2266FFDDD8D1928F8C716F6CDED8D22080FF2265FFDAD5
            CEEEECEA9F9F9FFFFFFFFFFFFFA09F9FEDEDEDD5D5D5A6A6A69A9A9AD8D8D88F
            8F8F6E6E6ED8D8D8A6A6A6999999D5D5D5EDEDEDA09F9FFFFFFFFFFFFFA0A1A1
            F1F0F0F1EFEDF6F1ECF6F2ECF2F1EFF1F1F2F2F2F3F2F1EFF6F2ECF6F2ECF1F0
            EDF1F0F0A0A1A1FFFFFFFFFFFFA1A1A1F2F2F1F0F0F0F2F2F1F3F2F2F2F2F1F3
            F2F2F3F3F3F2F2F1F3F2F2F3F2F2F1F1F1F2F2F1A1A1A1FFFFFFFFFFFFA2A3A5
            7C7E7F7B7D807C7E807C7E817B7E817B7E827B7E827B7E817C7E817C7E807B7D
            807C7E7FA2A3A5FFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E80
            807F80807F7F7E7E7F7E7E7F7E7E7E7E7D7F7E7EA4A4A4FFFFFFFFFFFFA0A3A7
            FFCA8BF9C27EF9C27FF9C280F9C280F9C280F9C280F9C280F9C280F9C27FF9C2
            7EFFCA8BA0A3A7FFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBB
            BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFF9EA2A6
            F9D0A4E8A760E7A762E7A863E7A863E7A863E7A863E7A863E7A863E7A762E8A7
            60F9D0A49EA2A6FFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1
            A1A1A1A1A1A1A1A1A1A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFF9FA2A5
            FDDBB6F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DA
            B8FDDBB69FA2A5FFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7
            D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFA9AAAB
            9FA2A59EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1
            A59FA2A5A9AAABFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2
            A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFF}
          NumGlyphs = 2
          Margin = 0
        end
        object bbtnDone: TevBitBtn
          Left = 275
          Top = 92
          Width = 83
          Height = 49
          Cancel = True
          Caption = 'C&lose (F2)'
          ModalResult = 2
          TabOrder = 2
          OnClick = bbtnDoneClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCC
            CCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            EDEDED9499C82C3CC02B3BBE2B3ABE2B3ABE2B3ABE2B3BBE2C3CC09499C8EDED
            EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDA5A5A56C6C6C6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6C6C6CA5A5A5EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFEDEDED
            969BC92F3EC35F71F9697DFF697CFF697CFF697CFF697DFF5F71F92F3EC3969B
            C9EDEDEDFFFFFFFFFFFFFFFFFFEDEDEDA7A7A76E6E6E9E9E9EA6A6A6A6A6A6A6
            A6A6A6A6A6A6A6A69E9E9E6E6E6EA7A7A7EDEDEDFFFFFFFFFFFFEDEDED969BC9
            2F3EC2586BF65F74FF5D72FE5E72FD5E73FD5E72FD5D72FE5F74FF586BF62F3E
            C2969BC9EDEDEDFFFFFFEDEDEDA7A7A76E6E6E999999A1A1A1A09F9FA09F9FA0
            9F9FA09F9FA09F9FA1A1A19999996E6E6EA7A7A7EDEDEDFFFFFF9499C8303FC2
            5568F3586CFC4E64F94D63F85468F9576BF95468F94D63F84E64F9586CFC5568
            F3303FC29499C8FFFFFFA5A5A56E6E6E9796969C9C9C9797979796969999999A
            9A9A9999999796969797979C9C9C9796966E6E6EA5A5A5FFFFFF2D3DC05367F2
            556BFA4960F7FFFFFFFFFFFF3E56F6475EF63E56F6FFFFFFFFFFFF4960F7556B
            FA5166F22D3DC0FFFFFF6D6D6D9695959B9B9B959594FFFFFFFFFFFF90909093
            9393909090FFFFFFFFFFFF9595949B9B9B9595946D6D6DFFFFFF2B3BBF6276FC
            4D64F64259F4FFFFFFFFFFFFFFFFFF2C46F3FFFFFFFFFFFFFFFFFF4259F44E64
            F65F75FC2C3BBFFFFFFF6B6B6BA1A1A1969595909090FFFFFFFFFFFFFFFFFF88
            8888FFFFFFFFFFFFFFFFFF909090969595A1A0A06B6B6BFFFFFF2A3ABF7386FA
            495FF3435AF36E80F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E80F6435AF3495F
            F36E81FA2B3ABFFFFFFF6B6B6BAAA9A9929292909090A4A4A4FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFA4A4A4909090929292A7A7A76B6B6BFFFFFF2939BF8696FB
            425AF14259F1354EF05B70F2FFFFFFFFFFFFFFFFFF5B70F2354EF04259F1435B
            F17D90F92A39BFFFFFFF6B6B6BB3B3B38F8F8F8F8F8F8989899A9A9AFFFFFFFF
            FFFFFFFFFF9A9A9A8989898F8F8F909090AFAFAF6B6B6BFFFFFF2737BF9AA8FB
            3A55EF3953EE2844EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2844ED3953EE3B55
            EF8E9DFA2838BFFFFFFF6A6A6ABFBFBF8C8C8C8A8A8A858484FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF8584848A8A8A8C8C8CB8B7B76A6A6AFFFFFF2637BF9FABF1
            314CED2B47EBFFFFFFFFFFFFFFFFFF5369EFFFFFFFFFFFFFFFFFFF2C47EB314C
            ED9FABF12737BFFFFFFF6A6A6ABEBDBD878787858484FFFFFFFFFFFFFFFFFF96
            9595FFFFFFFFFFFFFFFFFF858484878787BEBDBD6A6A6AFFFFFF2838C19FABF1
            8091F4213EE8FFFFFFFFFFFF5D72EE2340E85D72EEFFFFFFFFFFFF213EE88091
            F49FABF12838C1FFFFFF6B6B6BBEBDBDAEAEAE818080FFFFFFFFFFFF99999981
            8080999999FFFFFFFFFFFF818080AEAEAEBEBDBD6B6B6BFFFFFFB4BAEA2E3EC3
            97A5EF778AF25B71EE6074EE2643E62C48E72643E66074EE5B71EE778AF297A5
            EF2E3EC3B4BAEAFFFFFFC6C6C66E6E6EB9B9B9A9A8A89999999A9A9A81808083
            83838180809A9A9A999999A9A8A8B9B9B96E6E6EC6C6C6FFFFFFFFFFFFB6BCEA
            2E3EC295A2EE7688F01E3BE42340E52541E52340E51E3BE47688F095A2EE2E3E
            C2B6BCEAFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB7B6B6A7A7A77E7E7D80807F80
            807F80807F7E7E7DA7A7A7B7B6B66E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFF
            B6BCEA2F3DC394A0EFADB9F8ADB8F7ADB9F7ADB8F7ADB9F894A0EF2F3DC3B6BC
            EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB6B6B5CAC9C9C9C8C8CA
            C9C9C9C8C8CAC9C9B6B6B56E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFB4BAEA303FC44555CE4454CD4354CD4454CD4555CE303FC4B4BAEAFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C66F6F6F7E7E7D7D7C7C7D
            7C7C7D7C7C7E7E7D6F6F6FC6C6C6FFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          Margin = 0
        end
        object cbCheckPreview: TevCheckBox
          Left = 12
          Top = 70
          Width = 93
          Height = 17
          Caption = 'Check Preview'
          TabOrder = 3
        end
        object cbDisableShortfalls: TevCheckBox
          Left = 12
          Top = 52
          Width = 103
          Height = 17
          Caption = 'Disable Shortfalls'
          TabOrder = 4
        end
        object bbtnCopyPayroll: TevBitBtn
          Left = 148
          Top = 92
          Width = 119
          Height = 49
          Caption = 'Copy to payroll (F8)'
          TabOrder = 5
          OnClick = bbtnCopyPayrollClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFCFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            CCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFCC
            CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFBB8B36B68124B57F20B57F1FB57F1FB57F1FB57F
            1FB57F20B68124BB8B36FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF82828279
            7979777676777676777676777676777676777676797979828282FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFB68225FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFB68124FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7979FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979CFCFCFCCCCCC
            CCCCCCCCCCCCCCCCCCCCCCCCB58022FFFFFFEADDBAEADCB8E8D9B3E7D7B1E6D6
            AFE6D6AEFFFFFFB57F1FCFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC787877FF
            FFFFD8D8D8D7D6D6D3D3D3D2D2D1D0D0D0D0D0D0FFFFFF777676BB8B36B68124
            B57F20B57F1FB57F1FB47E1EB37B1AB07712B07612BB8A32EADCB8E7D7B1E6D6
            AFE5D5ACFFFFFFB47E1E8282827979797776767776767776767676757473736F
            6F6F6E6E6E828181D7D6D6D2D2D1D0D0D0CFCFCFFFFFFF767675B68124FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB07610FCFBF4F9F4E9F8F2
            E6F8F2E5FFFFFFB47E1E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF6E6E6EFBFBFBF3F3F3F1F1F1F1F1F1FFFFFF767675B57F1FFFFFFF
            E6D6AEE6D6AFE6D7B0E6D7B0E6D6AFE6D6AEFFFFFFAE740EF8F5E7F4EEDCF3EC
            D9F3ECD8FFFFFFB47E1E777676FFFFFFD0D0D0D0D0D0D1D1D1D1D1D1D0D0D0D0
            D0D0FFFFFF6C6C6CF3F3F3ECEBEBEAEAEAE9E9E9FFFFFF767675B47E1EFFFFFF
            E5D5ACE6D6AFE6D6AFE6D6AFE6D6AFE5D5ACFFFFFFAE740EF6F0DEF1E8D1F0E6
            CDF0E7CDFFFFFFB47E1E767675FFFFFFCFCFCFD0D0D0D0D0D0D0D0D0D0D0D0CF
            CFCFFFFFFF6C6C6CEEEEEEE5E5E5E3E3E2E4E3E3FFFFFF767675B47E1EFFFFFF
            F8F2E5F8F2E6F8F3E7F8F3E7F8F2E6F8F2E5FFFFFFAE740EF1EAD3F7F1E4FFFF
            FFFFFFFFFFFFFFB47F1E767675FFFFFFF1F1F1F1F1F1F2F2F1F2F2F1F1F1F1F1
            F1F1FFFFFF6C6C6CE7E7E6F0F0F0FFFFFFFFFFFFFFFFFF777676B47E1EFFFFFF
            F3ECD9F3ECD9F3ECDAF3ECD9F3ECD9F3ECD8FFFFFFAE740EEFE4C7FFFFFFCFAB
            6DAC7005FFFFFFB58022767675FFFFFFEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE9
            E9E9FFFFFF6C6C6CE0DFDFFFFFFFA3A3A3696969FFFFFF787877B47E1EFFFFFF
            F1E8CFF1E8D0F1E8D0F0E7CFF0E6CDF0E7CDFFFFFFAE740EEBDEBCFFFFFFAC70
            05FFFFFFECDCC4CFAC6D767675FFFFFFE4E4E4E5E5E5E5E5E5E4E3E3E3E3E2E4
            E3E3FFFFFF6C6C6CD9D9D9FFFFFF696969FFFFFFD9D9D9A3A3A3B47E1EFFFFFF
            EDE3C7EDE3C8EDE3C7F6F0E1FFFFFFFFFFFFFFFFFFB07611FFFFFFFFFFFFFFFF
            FFECDCC4CFAC6EFFFFFF767675FFFFFFDFDFDEE0DFDFDFDFDEEFEEEEFFFFFFFF
            FFFFFFFFFF6E6E6EFFFFFFFFFFFFFFFFFFD9D9D9A4A4A4FFFFFFB47E1EFFFFFF
            EBDDBCEBDDBDEBDCBBFFFFFFCFAB6DAC7005FFFFFFB37D1CB58021B57F20B580
            22CFAC6DFFFFFFFFFFFF767675FFFFFFD8D8D8D8D8D8D7D7D7FFFFFFA3A3A369
            6969FFFFFF757474787877777676787877A3A3A3FFFFFFFFFFFFB57F1FFFFFFF
            E7D8B1E7D8B1E7D7B0FFFFFFAC7005FFFFFFECDCC3D0AF73FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF777676FFFFFFD3D2D2D3D2D2D2D2D1FFFFFF696969FF
            FFFFD9D9D9A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB68124FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECDCC4CFAC6EFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
            D9D9A4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE8F3AB68124
            B57F1FB47E1EB47E1EB47F1FB58022C3984AFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF8686867979797776767676757676757776767878778E
            8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          Margin = 0
        end
        object evDBRadioGroup2: TevDBRadioGroup
          Left = 12
          Top = 105
          Width = 128
          Height = 36
          Caption = 'Check Type'
          Columns = 2
          DataField = 'CHECK_TYPE'
          DataSource = dsCHECK
          Items.Strings = (
            'Manual'
            'Regular')
          TabOrder = 6
          Values.Strings = (
            'M'
            'R')
          OnClick = evDBRadioGroup2Click
        end
        object cbPlugTaxes: TevCheckBox
          Left = 12
          Top = 88
          Width = 93
          Height = 17
          Caption = 'Plug Taxes'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
      end
      object fpEmployee: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 459
        Height = 456
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpEmployee'
        Color = 14737632
        TabOrder = 4
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Employee'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evLabel1: TevLabel
          Left = 12
          Top = 35
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object Label13: TevLabel
          Left = 332
          Top = 113
          Width = 91
          Height = 13
          Caption = 'Override Fed Value'
          FocusControl = DBEdit12
        end
        object Label18: TevLabel
          Left = 332
          Top = 269
          Width = 96
          Height = 13
          Caption = 'Backup Withholding'
          FocusControl = DBEdit17
        end
        object Label17: TevLabel
          Left = 332
          Top = 230
          Width = 60
          Height = 13
          Caption = 'Override EIC'
          FocusControl = DBEdit16
        end
        object Label16: TevLabel
          Left = 332
          Top = 191
          Width = 87
          Height = 13
          Caption = 'Override Medicare'
          FocusControl = DBEdit15
        end
        object Label15: TevLabel
          Left = 332
          Top = 152
          Width = 76
          Height = 13
          Caption = 'Override OASDI'
          FocusControl = DBEdit14
        end
        object Label28: TevLabel
          Left = 332
          Top = 35
          Width = 71
          Height = 13
          Caption = 'Tax Frequency'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TevLabel
          Left = 332
          Top = 74
          Width = 88
          Height = 13
          Caption = 'Override Fed Type'
        end
        object evLabel11: TevLabel
          Left = 196
          Top = 398
          Width = 131
          Height = 13
          Caption = 'Fed Number of Dependents'
        end
        object evDBText11: TevDBText
          Left = 132
          Top = 54
          Width = 71
          Height = 13
          AutoSize = True
          DataField = 'Employee_Name_Calculate'
          DataSource = dsEE
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EvBevel1: TEvBevel
          Left = 12
          Top = 322
          Width = 425
          Height = 3
          Shape = bsTopLine
        end
        object lcEmployee: TevDBLookupCombo
          Left = 12
          Top = 50
          Width = 97
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Trim_Number'#9'9'#9'Aligned EE Code'#9'F'
            'Employee_Name_Calculate'#9'50'#9'Employee_Name_Calculate'#9'F')
          LookupTable = DM_EE.EE
          LookupField = 'EE_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = lcEmployeeChange
          OnCloseUp = lcEmployeeCloseUp
        end
        object evDBGrid1: TevDBGrid
          Left = 12
          Top = 76
          Width = 297
          Height = 190
          DisableThemesInTitle = False
          ControlType.Strings = (
            'CL_E_DS_NBR;CustomEdit;lcEDCode'
            'EE_STATES_NBR;CustomEdit;lcState'
            'EE_SUI_STATES_NBR;CustomEdit;lcSUIState'
            'E_D_Code_Lookup;CustomEdit;lcEDCode;F'
            'State_Lookup;CustomEdit;lcState'
            'SUI_State_Lookup;CustomEdit;lcSUIState;F')
          Selected.Strings = (
            'E_D_Code_Lookup'#9'10'#9'E/D Code'#9'F'
            'AMOUNT'#9'12'#9'Amount'#9'F'
            'State_Lookup'#9'8'#9'State'#9'F'
            'SUI_State_Lookup'#9'8'#9'SUI State'#9'F'
            'HOURS_OR_PIECES'#9'10'#9'Hours'#9'F'
            'RATE_OF_PAY'#9'10'#9'Rate'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TTaxCalculator\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsCHECK_LINES
          ReadOnly = False
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object bbtnRefresh: TevBitBtn
          Left = 12
          Top = 280
          Width = 297
          Height = 25
          Caption = '&Refresh Scheduled E/Ds (F3)'
          TabOrder = 2
          OnClick = bbtnRefreshClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            CCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDDDDFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCD0D0D0FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            329EDE3CA2DDD2D2D2FFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E72DEDE
            DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFACACACAFAFAFD3D3D3FFFFFFFFFFFFFF
            FFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            61B2E1329DDE43A2E0CCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE8046A0
            75DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFBCBCBBABABABB0AFAFCDCCCCCDCCCCCD
            CCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFFFFFFFFFFFFF
            B7DDF4329CDE6CC9F70E907C00884000884600874600834200D9A100D8A000BC
            8046A075DDDDDDFFFFFFFFFFFFFFFFFFE2E2E2ABABABD2D2D188888879797979
            7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFFFFFFFFFFFFF
            FFFFFF339BDD8DDCFF00843440E7BE00D7A000D7A000D59F00D09C00D09C00D3
            9F00B98142A074FFFFFFFFFFFFFFFFFFFFFFFFAAAAAAE2E2E2747373DADADAC7
            C7C7C7C7C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFFFFFFFFFFFFF
            FFFFFF6AB4E465C5FA0083346DE7CD00C89900C89900C89900C79700C89800CA
            9A63E6CD008A47FFFFFFFFFFFFFFFFFFFFFFFFBEBDBDD0D0D0737373DEDEDEBA
            BABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFFFFFFFCCCCCC
            CCCCCCA3BFD150B0F000833295EDDE4CE7D24FE7D24DE6D193E8D700C39760E0
            C700B28153B184FFFFFFFFFFFFCDCCCCCDCCCCC3C2C2BEBEBE737373E7E7E6DE
            DEDEDEDEDEDDDDDDE1E1E1B6B6B5D7D6D6A4A4A4A5A5A5FFFFFFFFFFFF389BDC
            48ACE446AAE448AAEA3DB082007F30007F32008035007F3681E4D559DAC300AD
            7D58B285FFFFFFFFFFFFFFFFFFAAA9A9B8B7B7B7B6B6B8B8B8A3A3A36F6F6F6F
            6F6F7170706F6F6FDDDDDDD1D1D1A09F9FA6A6A6FFFFFFFFFFFFFFFFFF3799DB
            9EF1FD81E5FA77E2FC78E3FF78E3FF7AE5FF81E9FF00823677E2D200A97756A9
            79FFFFFFFFFFFFFFFFFFFFFFFFA8A8A7F1F1F1E7E7E6E5E5E5E7E7E6E7E7E6E8
            E7E7EBEBEA737373DBDADA9B9B9B9D9D9DFFFFFFFFFFFFFFFFFFFFFFFF5AA7DE
            85DCF589E5F869DAF769DBF8BBF2FFBCF4FFBFF9FF00842E00A87215947977B0
            E3FFFFFFFFFFFFFFFFFFFFFFFFB3B3B3E0DFDFE7E7E6DEDEDEDFDFDEF3F3F3F5
            F5F5F9F9F87473739999998B8B8BBBBBBBFFFFFFFFFFFFFFFFFFFFFFFF98C7E8
            6DC5EC9CE8FA5FD6F461D7F446B4E63292D93998DE429CE5459DE6449EE5429E
            E1FFFFFFFFFFFFFFFFFFFFFFFFCECDCDCCCCCCEAEAEADADADADBDADABEBDBDA3
            A3A3A9A8A8ADADADAFAFAFAFAFAFADADADFFFFFFFFFFFFFFFFFFFFFFFFDAECF9
            4CAEE4B0EEFB66D8F457D3F3ACEDFB63B5E58FB6D3FEFEFEFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF0EFEFB9B9B9F0EFEFDCDBDBD7D7D7EFEEEEBE
            BEBEBCBCBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            3B9ADAACEEFB82DDF54CCEF182DFF6ABE6F83996D9D4D5D7FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A8A8F0EFEFE0DFDFD3D2D2E2E2E2E8
            E8E8A6A6A6D7D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            5EA7DD8BDAF3ACEAF93FCBF045CDF0BBF0FB6DBAE758A3D7F1F1F1FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B3DDDDDDECEBEBD0D0D0D2D2D1F1
            F1F1C3C2C2AEAEAEF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            99C6E76CC5EBD7FAFFCDF6FDC3F3FDD2F8FFC6F1FB409ADAA5BED0FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCCCCCCFAFAFAF7F6F6F4F4F4F9
            F9F8F3F2F2A9A8A8C2C2C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            D5E9F8439ADA3F97D93E96D93E96D93E97D94099DA459CDB489EDCFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECECA9A8A8A6A6A6A6A6A6A6A6A6A6
            A6A6A8A8A7AAAAAAACACACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          Margin = 0
        end
        object DBEdit12: TevDBEdit
          Left = 332
          Top = 128
          Width = 105
          Height = 21
          DataField = 'OR_CHECK_FEDERAL_VALUE'
          DataSource = dsCHECK
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object DBEdit14: TevDBEdit
          Left = 332
          Top = 167
          Width = 105
          Height = 21
          DataField = 'OR_CHECK_OASDI'
          DataSource = dsCHECK
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object DBEdit15: TevDBEdit
          Left = 332
          Top = 206
          Width = 105
          Height = 21
          DataField = 'OR_CHECK_MEDICARE'
          DataSource = dsCHECK
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object DBEdit16: TevDBEdit
          Left = 333
          Top = 245
          Width = 104
          Height = 21
          DataField = 'OR_CHECK_EIC'
          DataSource = dsCHECK
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object DBEdit17: TevDBEdit
          Left = 332
          Top = 284
          Width = 105
          Height = 21
          DataField = 'OR_CHECK_BACK_UP_WITHHOLDING'
          DataSource = dsCHECK
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwDBComboBox1: TevDBComboBox
          Left = 332
          Top = 51
          Width = 105
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'TAX_FREQUENCY'
          DataSource = dsCHECK
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'Weekly'#9'W'
            'Bi-Weekly'#9'B'
            'Semi-Monthly'#9'S'
            'Monthly'#9'M'
            'Quarterly'#9'Q'
            'Semi-Annual'#9'N'
            'Annual'#9'A')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 8
          UnboundDataType = wwDefault
        end
        object wwDBComboBox3: TevDBComboBox
          Left = 332
          Top = 89
          Width = 105
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'OR_CHECK_FEDERAL_TYPE'
          DataSource = dsCHECK
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'None'#9'N'
            'Regular Amount'#9'A'
            'Regular Percent'#9'P'
            'Additional Amount'#9'M'
            'Additional Percent'#9'E')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 9
          UnboundDataType = wwDefault
        end
        object lcEDCode: TevDBLookupCombo
          Left = 12
          Top = 196
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'ED_Lookup'#9'20'#9'ED_Lookup'#9'F'
            'CodeDescription'#9'40'#9'CodeDescription'#9'F')
          DataField = 'CL_E_DS_NBR'
          DataSource = dsCHECK_LINES
          LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
          LookupField = 'CL_E_DS_NBR'
          Style = csDropDownList
          TabOrder = 10
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object lcState: TevDBLookupCombo
          Left = 12
          Top = 228
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'State_Lookup'#9'2'#9'State_Lookup'#9'F')
          DataField = 'EE_STATES_NBR'
          DataSource = dsCHECK_LINES
          LookupTable = DM_EE_STATES.EE_STATES
          LookupField = 'EE_STATES_NBR'
          Style = csDropDownList
          TabOrder = 11
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object lcSUIState: TevDBLookupCombo
          Left = 12
          Top = 260
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'State_Lookup'#9'2'#9'State_Lookup'#9'F')
          DataField = 'EE_SUI_STATES_NBR'
          DataSource = dsCHECK_LINES
          LookupTable = DM_EE_STATES.EE_STATES
          LookupField = 'EE_STATES_NBR'
          Style = csDropDownList
          TabOrder = 12
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBCheckBox1: TevDBCheckBox
          Left = 12
          Top = 340
          Width = 105
          Height = 17
          Caption = 'Block Federal'
          DataField = 'EXCLUDE_FEDERAL'
          DataSource = dsCHECK
          TabOrder = 13
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBCheckBox2: TevDBCheckBox
          Left = 139
          Top = 340
          Width = 137
          Height = 17
          Caption = 'Block Additional Federal'
          DataField = 'EXCLUDE_ADDITIONAL_FEDERAL'
          DataSource = dsCHECK
          TabOrder = 14
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBCheckBox3: TevDBCheckBox
          Left = 12
          Top = 364
          Width = 113
          Height = 17
          Caption = 'Block EE OASDI'
          DataField = 'EXCLUDE_EMPLOYEE_OASDI'
          DataSource = dsCHECK
          TabOrder = 15
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBCheckBox4: TevDBCheckBox
          Left = 139
          Top = 364
          Width = 129
          Height = 17
          Caption = 'Block EE Medicare'
          DataField = 'EXCLUDE_EMPLOYEE_MEDICARE'
          DataSource = dsCHECK
          TabOrder = 16
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBCheckBox5: TevDBCheckBox
          Left = 293
          Top = 364
          Width = 65
          Height = 17
          Caption = 'Block EIC'
          DataField = 'EXCLUDE_EMPLOYEE_EIC'
          DataSource = dsCHECK
          TabOrder = 17
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBCheckBox6: TevDBCheckBox
          Left = 293
          Top = 340
          Width = 145
          Height = 17
          Caption = 'Tax at Supplemental Rate'
          DataField = 'TAX_AT_SUPPLEMENTAL_RATE'
          DataSource = dsCHECK
          TabOrder = 18
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBRadioGroup1: TevDBRadioGroup
          Left = 12
          Top = 399
          Width = 176
          Height = 36
          Caption = 'Federal Marital Status'
          Columns = 2
          DataField = 'FEDERAL_MARITAL_STATUS'
          DataSource = dsCHECK
          Items.Strings = (
            'Single'
            'Married')
          TabOrder = 19
          Values.Strings = (
            'S'
            'M')
        end
        object evDBSpinEdit1: TevDBSpinEdit
          Left = 196
          Top = 414
          Width = 131
          Height = 21
          Increment = 1.000000000000000000
          MaxValue = 100.000000000000000000
          DataField = 'NUMBER_OF_DEPENDENTS'
          DataSource = dsCHECK
          TabOrder = 20
          UnboundDataType = wwDefault
        end
        object cbNetToGross: TevCheckBox
          Left = 335
          Top = 396
          Width = 81
          Height = 17
          Caption = 'Net To Gross'
          TabOrder = 21
          OnClick = cbNetToGrossClick
        end
        object edtNetToGross: TevEdit
          Left = 335
          Top = 414
          Width = 102
          Height = 21
          Enabled = False
          TabOrder = 22
        end
      end
      object fpTaxes: TisUIFashionPanel
        Left = 8
        Top = 472
        Width = 459
        Height = 165
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpTaxes'
        Color = 14737632
        TabOrder = 5
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Taxes'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablGross: TevLabel
          Left = 12
          Top = 129
          Width = 39
          Height = 16
          Caption = 'Gross:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablNet: TevLabel
          Left = 240
          Top = 129
          Width = 24
          Height = 16
          Caption = 'Net:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel5: TevLabel
          Left = 12
          Top = 35
          Width = 96
          Height = 13
          Caption = 'Federal Tax Wages:'
        end
        object evLabel6: TevLabel
          Left = 12
          Top = 58
          Width = 114
          Height = 13
          Caption = 'OASDI Taxable Wages:'
        end
        object evLabel7: TevLabel
          Left = 12
          Top = 105
          Width = 125
          Height = 13
          Caption = 'Medicare Taxable Wages:'
        end
        object evLabel8: TevLabel
          Left = 240
          Top = 35
          Width = 59
          Height = 13
          Caption = 'Federal Tax:'
        end
        object evLabel9: TevLabel
          Left = 12
          Top = 82
          Width = 100
          Height = 13
          Caption = 'OASDI Taxable Tips:'
        end
        object evLabel10: TevLabel
          Left = 240
          Top = 58
          Width = 57
          Height = 13
          Caption = 'OASDI Tax:'
        end
        object evLabel12: TevLabel
          Left = 240
          Top = 82
          Width = 68
          Height = 13
          Caption = 'Medicare Tax:'
        end
        object evLabel13: TevLabel
          Left = 240
          Top = 105
          Width = 41
          Height = 13
          Caption = 'EIC Tax:'
        end
        object evDBText1: TevDBText
          Left = 116
          Top = 35
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'FEDERAL_TAXABLE_WAGES'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText2: TevDBText
          Left = 132
          Top = 58
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_OASDI_TAXABLE_WAGES'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText3: TevDBText
          Left = 116
          Top = 82
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_OASDI_TAXABLE_TIPS'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText4: TevDBText
          Left = 140
          Top = 105
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_MEDICARE_TAXABLE_WAGES'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText5: TevDBText
          Left = 304
          Top = 35
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'FEDERAL_TAX'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText6: TevDBText
          Left = 304
          Top = 58
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_OASDI_TAX'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText7: TevDBText
          Left = 312
          Top = 82
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_MEDICARE_TAX'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText8: TevDBText
          Left = 288
          Top = 105
          Width = 54
          Height = 13
          AutoSize = True
          DataField = 'EE_EIC_TAX'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText9: TevDBText
          Left = 60
          Top = 129
          Width = 67
          Height = 16
          AutoSize = True
          DataField = 'GROSS_WAGES'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText10: TevDBText
          Left = 272
          Top = 129
          Width = 74
          Height = 16
          AutoSize = True
          DataField = 'NET_WAGES'
          DataSource = dsCHECK
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Check Preview'
      ImageIndex = 1
      object evDBText12: TevDBText
        Left = 8
        Top = 0
        Width = 65
        Height = 17
        DataField = 'Trim_Number'
        DataSource = dsEE
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evDBText13: TevDBText
        Left = 80
        Top = 0
        Width = 71
        Height = 13
        AutoSize = True
        DataField = 'Employee_Name_Calculate'
        DataSource = dsEE
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object sbCheckPreview: TScrollBox
        Left = 0
        Top = 0
        Width = 862
        Height = 645
        Align = alClient
        TabOrder = 0
        object pnlCheckPreviewBorder: TevPanel
          Left = 0
          Top = 0
          Width = 858
          Height = 641
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object fpCheckPreview: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 842
            Height = 625
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            OnResize = fpCheckPreviewResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Employee'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlFPCheckPreviewBody: TevPanel
              Left = 12
              Top = 35
              Width = 801
              Height = 542
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object strPreviewGrid: TevStringGrid
                Left = 0
                Top = 0
                Width = 801
                Height = 542
                Align = alClient
                ColCount = 2
                DefaultColWidth = 380
                DefaultRowHeight = 14
                FixedCols = 0
                RowCount = 2
                PopupMenu = evPopupMenu1
                TabOrder = 0
                SortOptions.CanSort = False
                SortOptions.SortStyle = ssNormal
                SortOptions.SortCaseSensitive = False
                SortOptions.SortCol = -1
                SortOptions.SortDirection = sdAscending
                ColWidths = (
                  403
                  391)
              end
            end
          end
        end
      end
    end
    object tsPayrolls: TTabSheet
      Caption = 'Payrolls'
      ImageIndex = 2
      object sbPayrolls: TScrollBox
        Left = 0
        Top = 0
        Width = 862
        Height = 645
        Align = alClient
        TabOrder = 0
        object pnlPayrollBorder: TevPanel
          Left = 0
          Top = 0
          Width = 858
          Height = 539
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 4
          TabOrder = 0
          object evSplitter1: TevSplitter
            Left = 423
            Top = 4
            Height = 531
          end
          object pnlPayrollLeft: TevPanel
            Left = 4
            Top = 4
            Width = 419
            Height = 531
            Align = alLeft
            BevelOuter = bvNone
            BorderWidth = 4
            TabOrder = 0
            object fpPayrollLeft: TisUIFashionPanel
              Left = 4
              Top = 4
              Width = 411
              Height = 523
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Payroll'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object pnlFPLeftBody: TevPanel
                Left = 12
                Top = 35
                Width = 361
                Height = 394
                BevelOuter = bvNone
                ParentColor = True
                TabOrder = 0
                object PRGrid: TevDBGrid
                  Left = 0
                  Top = 0
                  Width = 361
                  Height = 394
                  DisableThemesInTitle = False
                  Selected.Strings = (
                    'CHECK_DATE'#9'10'#9'Check Date'#9
                    'RUN_NUMBER'#9'5'#9'Run Number'#9
                    'SCHEDULED'#9'1'#9'Scheduled'#9
                    'PAYROLL_TYPE'#9'1'#9'Type'#9
                    'STATUS'#9'1'#9'Status'#9)
                  IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                  IniAttributes.SectionName = 'TTaxCalculator\PRGrid'
                  IniAttributes.Delimiter = ';;'
                  ExportOptions.ExportType = wwgetSYLK
                  ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                  TitleColor = clBtnFace
                  FixedCols = 0
                  ShowHorzScrollBar = True
                  Align = alClient
                  DataSource = wwdsSubMaster
                  MultiSelectOptions = [msoAutoUnselect]
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                  TabOrder = 0
                  TitleAlignment = taLeftJustify
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  TitleLines = 1
                  PaintOptions.AlternatingRowColor = 14544093
                  PaintOptions.ActiveRecordColor = clBlack
                  DefaultSort = 'CHECK_DATE;RUN_NUMBER'
                  NoFire = False
                end
              end
            end
          end
          object pnlPayrollRight: TevPanel
            Left = 426
            Top = 4
            Width = 428
            Height = 531
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 4
            TabOrder = 1
            object fpPayrollRight: TisUIFashionPanel
              Left = 4
              Top = 4
              Width = 420
              Height = 523
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Batch'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object pnlFPRightBody: TevPanel
                Left = 12
                Top = 35
                Width = 373
                Height = 454
                BevelOuter = bvNone
                ParentColor = True
                TabOrder = 0
                object wwDBGrid2: TevDBGrid
                  Left = 0
                  Top = 0
                  Width = 373
                  Height = 454
                  TabStop = False
                  DisableThemesInTitle = False
                  Selected.Strings = (
                    'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
                    'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
                    'FREQUENCY'#9'1'#9'Frequency'#9'No')
                  IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                  IniAttributes.SectionName = 'TTaxCalculator\wwDBGrid2'
                  IniAttributes.Delimiter = ';;'
                  ExportOptions.ExportType = wwgetSYLK
                  ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                  TitleColor = clBtnFace
                  FixedCols = 0
                  ShowHorzScrollBar = True
                  Align = alClient
                  DataSource = wwdsSubMaster1
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                  TabOrder = 0
                  TitleAlignment = taLeftJustify
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  TitleLines = 1
                  PaintOptions.AlternatingRowColor = 14544093
                  PaintOptions.ActiveRecordColor = clBlack
                  NoFire = False
                end
              end
            end
          end
        end
        object evPanel2: TevPanel
          Left = 0
          Top = 539
          Width = 858
          Height = 102
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object fpPayrollControls: TisUIFashionPanel
            Left = 8
            Top = 0
            Width = 843
            Height = 96
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpPayrollControls'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Controls'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evBitBtn3: TevBitBtn
              Left = 283
              Top = 35
              Width = 265
              Height = 38
              Caption = 'Attach to New Batch'
              TabOrder = 0
              OnClick = evBitBtn3Click
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
                CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCCCCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E253B28C009E5E009D
                5D009E5E53B28CE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFE2E2E2A7A7A78E8E8E8D8D8D8E8E8EA6A6A6E1E1E1FFFFFFFEFEFE
                ECECECCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC59B48D00A66900BA8677DF
                C400BA8600A66A53B28CFFFFFFFEFEFEECECECCDCCCCCCCCCCCCCCCCCCCCCCCC
                CCCCCCCCCCA9A8A8979696ABABABD6D6D6ABABAB979696A6A6A6FFFFFFF2F2F2
                B5B5B588888885868686868686868789868896868C009E5800C08B00BB82FFFF
                FF00BB8200C08C009E5EFFFFFFF3F2F2B5B5B588888886868686868686868687
                87878989898E8E8EB2B1B1ABABABFFFFFFABABABB2B1B18E8E8EFFFFFFD5D5D5
                9B9C9CF1F0EEF0EEEFDDDAD9DEDCDAE1DCDCF1DEE3009B5374E5CBFFFFFFFFFF
                FFFFFFFF77E5CC009C5CFFFFFFD6D6D69D9D9DF1F1F1F0EFEFDCDBDBDDDDDDDE
                DEDEE2E2E28A8A8ADCDBDBFFFFFFFFFFFFFFFFFFDCDCDC8C8C8CFFFFFFBDBDBD
                AEADADEDECEA888483D8D6D48A8786DBD7D69C898E009B5300CC9600C88FFFFF
                FF00C88F00CC98009D5DFFFFFFBDBCBCAEAEAEEDEDED858484D7D6D6878787D8
                D8D88C8C8C8A8A8ABDBCBCB8B8B8FFFFFFB8B8B8BDBCBC8D8D8DFFFFFFA9A9A9
                BBB8B7D8D5D4D1CFCD8E8C8AD4D2D0918D8CDFD3D539977000AE6C00D39C74ED
                D300D49D00AE7268C6A1FFFFFFAAA9A9B9B9B9D6D6D6D0D0D08C8C8CD3D2D28E
                8E8ED5D5D58C8C8C9E9E9EC4C3C3E4E3E3C4C4C49F9F9EBBBBBBFFFFFF969696
                C5C3C2CDC9C891908ECFCCCB93918FD0CCCC979191DFCCD13A9871009B54009B
                54009D586FC9A4FFFFFFFFFFFF979696C4C4C4CACACA909090CDCCCC919191CE
                CDCD929292CFCFCF8D8D8D8A8A8A8A8A8A8D8D8DBEBEBEFFFFFFFFFFFF888888
                CAC8C6959391C8C6C4969492C8C6C4969492C9C6C59A9493D3C7C9A69598DDC9
                CD9E868FFFFFFFFFFFFFFFFFFF888888C9C8C8949393C7C7C7959594C7C7C795
                9594C7C7C7959594CAC9C9979797CCCCCC8A8A8AFFFFFFFFFFFFD1D1D1858585
                A8A5A4A6A4A3A6A3A2A6A3A1A6A3A1A6A3A1A6A3A1A7A3A1A8A3A3A9A4A4ABA5
                A58B8487D1D1D1FFFFFFD1D1D1868585A6A6A6A5A5A5A4A4A4A3A3A3A3A3A3A3
                A3A3A3A3A3A3A3A3A4A4A4A5A5A5A6A6A6868585D1D1D1FFFFFF8D8D8EE9E8E9
                D9DCDFD9DCE2D9DBDDDAD9D9DAD9D7DAD9D7DAD9D7DAD9D9D9DBDDD9DCE2D9DC
                DFE9E8EA908E8FFFFFFF8E8E8EEAEAEADEDEDEDFDFDEDCDCDCDADADADADADADA
                DADADADADADADADADCDCDCDFDFDEDEDEDEEAEAEA8F8F8FFFFFFF858687E7E9ED
                D4AA66C66E00C1C7D3C1C2C5C2C0BFC2C0BEC2C0BFC1C2C5C1C7D3C66E00D4AA
                66E7E9ED858687FFFFFF868686ECEBEBA2A2A26B6B6BCACACAC4C3C3C1C1C1C1
                C1C1C1C1C1C4C3C3CACACA6B6B6BA2A2A2ECEBEB868686FFFFFFAEAEAFE6E8EA
                FFEDABDD9C33C57000DFE7F3E2E4E7E3E3E3E2E4E7DFE7F3C57000DD9C33FFED
                ABE6E8EA919192FFFFFFAFAFAFEAEAEAE3E3E29393936C6C6CEBEBEAE6E6E6E4
                E4E4E6E6E6EBEBEA6C6C6C939393E3E3E2EAEAEA929292FFFFFFFFFFFFADAEB0
                7C828CFFEDB2DA9B35C772007C859580858F7C8595C77200DA9B35FFEDB27C82
                8CADAFB0FFFFFFFFFFFFFFFFFFAFAFAF858484E4E4E49292926E6E6E89898987
                87878989896E6E6E929292E4E4E4858484AFAFAFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFE8B1DB9C37CB7600FFFFFFCB7600DB9C37FFE8B1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E0939393727171FF
                FFFF727171939393E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE9B3DB9E3BFFFFFFDB9E3BFFE9B3FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1959594FF
                FFFF959594E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object evBitBtn2: TevBitBtn
              Left = 12
              Top = 35
              Width = 265
              Height = 38
              Caption = 'Attach to New Payroll'
              TabOrder = 1
              OnClick = evBitBtn2Click
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
                CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE2E2E2CDCCCCCDCCCCCDCCCCE2E2E2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E153A882008C4C008B
                4A008C4C53A882E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFE2E2E29E9E9E7E7E7D7C7B7B7E7E7D9E9E9EE2E2E2FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF54A88100995B00BB8677E0
                C600BB8600995C53A882FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF9D9D9D8A8A8AACACACD7D6D6ACACAC8A8A8A9E9E9EDCDCDCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00894600C18D00BC83FFFF
                FF00BC8300C18D008C4CDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCC7A7979B2B2B2ACACACFFFFFFACACACB2B2B27E7E7D8CBAA36DB08F
                6EB18F6FB18F6EB18F6EB08E6EB18E70B1907AB49500864175E6CDFFFFFFFFFF
                FFFFFFFF77E7CE008A49B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
                A8A7ABABAB777676DDDDDDFFFFFFFFFFFFFFFFFFDEDEDE7B7B7B6AAD8BCBF4DB
                8CC7B583BEA793CBBB92CAB992CBBA95CCBCA0CFC200854000CB9700C990FFFF
                FF00C99000CC98008A49A3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C7
                C7C7CBCBCB767675BCBCBBB9B9B9FFFFFFB9B9B9BDBCBC7B7B7B65A986CAF3DC
                5FA27C4E9368B8EBCFB7E9CD98D1B288C4A391C8A92C9B67009F6000D39D74EE
                D400D39C00A061309B68A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
                BBBBC0C0C08E8E8E909090C4C3C3E4E4E4C4C3C39191918E8E8E62A581CDF2E0
                73B593B3E7CDB0E4CAB0E4CA6BAC89B3E4C7B6E5C97AB2924AB28200843F0083
                3E00833E56B78A72AB8A9B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
                DCDCDEDEDEAAA9A9A5A5A5757474747373747373AAAAAAA2A2A25FA37ED0F2E4
                6CAD8BABE2C851946DABE2C86DAC88B0E2C6B1E2C671AE8AB5E6CF609B76BAE8
                D179B292DAF6E963A580999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
                DADADBDADAA4A4A4E0DFDF929292E2E2E2AAA9A9F3F3F39B9B9B5CA07AD8F5E8
                67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA7DFC6A7DFC6A9E1
                C869AB86D9F5E95DA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
                DBDBDCDBDB989898D8D8D8D8D8D8DADADAA1A1A1F2F2F1969595599D75DFF7EF
                549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
                7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
                AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
                448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
                61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
                959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
                E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
                F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
                F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
                B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
                CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
                DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
                5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
                685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
                8686868686868686868686868686868686878787B0AFAFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object evBitBtn1: TevBitBtn
              Left = 554
              Top = 35
              Width = 265
              Height = 38
              Caption = 'Attach to Selected Batch'
              TabOrder = 2
              OnClick = evBitBtn1Click
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDD
                DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E
                72DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFEFEFE
                ECECECCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE
                8046A075DEDEDEFFFFFFFFFFFFFFFFFFEDEDEDCECECECDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFF2F2F2
                B5B5B589888993878C308A6100894600884700884700874600834200D9A100D8
                A000BC8046A075DDDDDDFFFFFFF3F3F3B6B6B58989898989897F7E7E7A79797A
                79797A7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFD5D5D5
                9B9C9CF4F0F0FFF2F800853B3FE8C000D7A000D7A100D7A100D59F00D09C00D0
                9C00D39F00B98142A074FFFFFFD6D6D69D9D9DF2F2F1F6F6F5767675DBDADAC7
                C7C7C8C8C7C8C8C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFBDBDBD
                AEADADF0EDEC9A888D0083396CE8CE00C89900C89900C89A00C89900C79700C8
                9800CA9A63E6CD008A47FFFFFFBEBDBDAEAEAEEFEEEE8B8B8B747373DFDFDEBA
                BABABABABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFA9A9A9
                BBB8B7DAD6D6E1D1D500843992EEE04BE8D34FE8D34FE8D34DE6D193E8D800C3
                9760E0C600B28153B184FFFFFFAAA9A9B9B9B9D7D7D7D4D4D4757474E8E8E8DF
                DFDEDFDFDEDFDFDEDDDDDDE2E2E2B6B6B5D7D6D6A4A4A4A5A5A5FFFFFF969696
                C5C3C2CECAC99C91923EA07300853900843A00843A00843C00813981E4D557DA
                C300AD7D5BB387FFFFFFFFFFFF979696C4C4C4CBCBCB93939394939376767575
                7474757474757474727171DDDDDDD1D1D1A09F9FA7A7A7FFFFFFFFFFFF888888
                CAC8C6959391CCC7C6A09697D8C8CCA7969ADCC9CEB3989F00863C73E2D400A9
                7934875EFFFFFFFFFFFFFFFFFF888888C9C8C8949393C9C8C8979797CBCBCB98
                9898CDCCCC9C9C9C777676DCDBDB9B9B9B7D7C7CFFFFFFFFFFFFD1D1D1858585
                A8A5A4A6A4A3A7A3A2A8A4A2A9A3A2A9A4A3ABA4A4B6A6A900843700A5783290
                6297858DD1D1D1FFFFFFD3D2D2868585A6A6A6A5A5A5A4A4A4A4A4A4A4A4A4A5
                A5A5A5A5A5A9A8A8757474989898858484888888D3D2D2FFFFFF8D8D8EE9E8E9
                D9DCDFD9DCE2D9DBDDDAD9D9DAD9D7DAD9D7DAD9D7DFDADBE6DCE2E8DDE7E4DC
                E3EEE9EC918E8FFFFFFF8E8E8EEAEAEADEDEDEDFDFDEDCDCDCDADADADADADADA
                DADADADADADCDBDBE0DFDFE1E1E1E0DFDFECEBEB8F8F8FFFFFFF858687E7E9ED
                D4AA66C66E00C1C7D3C1C2C5C2C0BFC2C0BEC2C0BFC2C2C6C3C7D3C86E00D6AA
                67E8E9ED858687FFFFFF868686ECEBEBA2A2A26B6B6BCACACAC4C3C3C1C1C1C1
                C1C1C1C1C1C4C3C3CACACA6B6B6BA2A2A2ECEBEB868686FFFFFFAEAEAFE6E8EA
                FFEDABDD9C33C57000DFE7F3E2E4E7E3E3E3E2E4E7DFE7F3C57000DD9C33FFED
                ABE6E8EA919192FFFFFFAFAFAFEAEAEAE3E3E29393936C6C6CEBEBEAE6E6E6E4
                E4E4E6E6E6EBEBEA6C6C6C939393E3E3E2EAEAEA929292FFFFFFFFFFFFADAEB0
                7C828CFFEDB2DA9B35C772007C859580858F7C8595C77200DA9B35FFEDB27C82
                8CADAFB0FFFFFFFFFFFFFFFFFFAFAFAF858484E4E4E49292926E6E6E89898987
                87878989896E6E6E929292E4E4E4858484B0AFAFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFE8B1DB9C37CB7600FFFFFFCB7600DB9C37FFE8B1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E0939393727171FF
                FFFF727171939393E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE9B3DB9E3BFFFFFFDB9E3BFFE9B3FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1959594FF
                FFFF959594E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
      end
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 332
  end
  object dsCHECK: TevDataSource
    DataSet = PR_CHECK
    Left = 604
    Top = 4
  end
  object dsCHECK_LINES: TevDataSource
    DataSet = PR_CHECK_LINES
    OnDataChange = dsCHECK_LINESDataChange
    Left = 652
    Top = 4
  end
  object dsCHECK_STATES: TevDataSource
    DataSet = PR_CHECK_STATES
    OnDataChange = dsCHECK_STATESDataChange
    Left = 700
    Top = 4
  end
  object dsCHECK_SUI: TevDataSource
    DataSet = PR_CHECK_SUI
    Left = 748
    Top = 4
  end
  object dsCHECK_LOCALS: TevDataSource
    DataSet = PR_CHECK_LOCALS
    Left = 796
    Top = 4
  end
  object PR_CHECK: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CHECK_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_EIC_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_OASDI_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_TAXABLE_TIPS'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EXCLUDE_ADDITIONAL_FEDERAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_ALL_SCHED_E_D_CODES'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_AUTO_DISTRIBUTION'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_DD'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_DD_EXCEPT_NET'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_EIC'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_MEDICARE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_OASDI'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_FEDERAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_FROM_AGENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_TIME_OFF_ACCURAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_SHORTFALL'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_TAX'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'NET_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_BACK_UP_WITHHOLDING'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_EIC'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_FEDERAL_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OR_CHECK_FEDERAL_VALUE'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_MEDICARE'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_OASDI'
        DataType = ftFloat
      end
      item
        Name = 'PR_BATCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SALARY'
        DataType = ftFloat
      end
      item
        Name = 'TAX_AT_SUPPLEMENTAL_RATE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TAX_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_MARITAL_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMBER_OF_DEPENDENTS'
        DataType = ftInteger
      end
      item
        Name = 'PAYMENT_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'FEDERAL_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_MEDICARE_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'CALCULATE_OVERRIDE_TAXES'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RECIPROCATE_SUI'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DISABLE_SHORTFALLS'
        DataType = ftString
        Size = 1
      end>
    Left = 372
    object PR_CHECKCHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Size = 1
    end
    object PR_CHECKEE_EIC_TAX: TFloatField
      FieldName = 'EE_EIC_TAX'
    end
    object PR_CHECKEE_MEDICARE_TAX: TFloatField
      FieldName = 'EE_MEDICARE_TAX'
    end
    object PR_CHECKEE_MEDICARE_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_MEDICARE_TAXABLE_WAGES'
    end
    object PR_CHECKEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object PR_CHECKEE_OASDI_TAX: TFloatField
      FieldName = 'EE_OASDI_TAX'
    end
    object PR_CHECKEE_OASDI_TAXABLE_TIPS: TFloatField
      FieldName = 'EE_OASDI_TAXABLE_TIPS'
    end
    object PR_CHECKEE_OASDI_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_OASDI_TAXABLE_WAGES'
    end
    object PR_CHECKEXCLUDE_ADDITIONAL_FEDERAL: TStringField
      FieldName = 'EXCLUDE_ADDITIONAL_FEDERAL'
      Size = 1
    end
    object PR_CHECKEXCLUDE_ALL_SCHED_E_D_CODES: TStringField
      FieldName = 'EXCLUDE_ALL_SCHED_E_D_CODES'
      Size = 1
    end
    object PR_CHECKEXCLUDE_AUTO_DISTRIBUTION: TStringField
      FieldName = 'EXCLUDE_AUTO_DISTRIBUTION'
      Size = 1
    end
    object PR_CHECKEXCLUDE_DD: TStringField
      FieldName = 'EXCLUDE_DD'
      Size = 1
    end
    object PR_CHECKEXCLUDE_DD_EXCEPT_NET: TStringField
      FieldName = 'EXCLUDE_DD_EXCEPT_NET'
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_EIC: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_EIC'
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_MEDICARE: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_MEDICARE'
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_OASDI: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_OASDI'
      Size = 1
    end
    object PR_CHECKEXCLUDE_FEDERAL: TStringField
      FieldName = 'EXCLUDE_FEDERAL'
      Size = 1
    end
    object PR_CHECKEXCLUDE_FROM_AGENCY: TStringField
      FieldName = 'EXCLUDE_FROM_AGENCY'
      Size = 1
    end
    object PR_CHECKEXCLUDE_SCH_E_D_EXCEPT_PENSION: TStringField
      FieldName = 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'
      Size = 1
    end
    object PR_CHECKEXCLUDE_SCH_E_D_FROM_AGCY_CHK: TStringField
      FieldName = 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'
      Size = 1
    end
    object PR_CHECKEXCLUDE_TIME_OFF_ACCURAL: TStringField
      FieldName = 'EXCLUDE_TIME_OFF_ACCURAL'
      Size = 1
    end
    object PR_CHECKFEDERAL_SHORTFALL: TFloatField
      FieldName = 'FEDERAL_SHORTFALL'
    end
    object PR_CHECKFEDERAL_TAX: TFloatField
      FieldName = 'FEDERAL_TAX'
    end
    object PR_CHECKFEDERAL_TAXABLE_WAGES: TFloatField
      FieldName = 'FEDERAL_TAXABLE_WAGES'
    end
    object PR_CHECKGROSS_WAGES: TFloatField
      FieldName = 'GROSS_WAGES'
    end
    object PR_CHECKNET_WAGES: TFloatField
      FieldName = 'NET_WAGES'
    end
    object PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING: TFloatField
      FieldName = 'OR_CHECK_BACK_UP_WITHHOLDING'
    end
    object PR_CHECKOR_CHECK_EIC: TFloatField
      FieldName = 'OR_CHECK_EIC'
    end
    object PR_CHECKOR_CHECK_FEDERAL_TYPE: TStringField
      FieldName = 'OR_CHECK_FEDERAL_TYPE'
      Size = 1
    end
    object PR_CHECKOR_CHECK_FEDERAL_VALUE: TFloatField
      FieldName = 'OR_CHECK_FEDERAL_VALUE'
    end
    object PR_CHECKOR_CHECK_MEDICARE: TFloatField
      FieldName = 'OR_CHECK_MEDICARE'
    end
    object PR_CHECKOR_CHECK_OASDI: TFloatField
      FieldName = 'OR_CHECK_OASDI'
    end
    object PR_CHECKPR_BATCH_NBR: TIntegerField
      FieldName = 'PR_BATCH_NBR'
    end
    object PR_CHECKPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object PR_CHECKPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_CHECKSALARY: TFloatField
      FieldName = 'SALARY'
    end
    object PR_CHECKTAX_AT_SUPPLEMENTAL_RATE: TStringField
      FieldName = 'TAX_AT_SUPPLEMENTAL_RATE'
      Size = 1
    end
    object PR_CHECKTAX_FREQUENCY: TStringField
      FieldName = 'TAX_FREQUENCY'
      Size = 1
    end
    object PR_CHECKFEDERAL_MARITAL_STATUS: TStringField
      FieldName = 'FEDERAL_MARITAL_STATUS'
      Size = 1
    end
    object PR_CHECKNUMBER_OF_DEPENDENTS: TIntegerField
      FieldName = 'NUMBER_OF_DEPENDENTS'
    end
    object PR_CHECKPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object PR_CHECKFEDERAL_GROSS_WAGES: TFloatField
      FieldName = 'FEDERAL_GROSS_WAGES'
    end
    object PR_CHECKEE_OASDI_GROSS_WAGES: TFloatField
      FieldName = 'EE_OASDI_GROSS_WAGES'
    end
    object PR_CHECKER_OASDI_GROSS_WAGES: TFloatField
      FieldName = 'ER_OASDI_GROSS_WAGES'
    end
    object PR_CHECKEE_MEDICARE_GROSS_WAGES: TFloatField
      FieldName = 'EE_MEDICARE_GROSS_WAGES'
    end
    object PR_CHECKER_MEDICARE_GROSS_WAGES: TFloatField
      FieldName = 'ER_MEDICARE_GROSS_WAGES'
    end
    object PR_CHECKCALCULATE_OVERRIDE_TAXES: TStringField
      FieldName = 'CALCULATE_OVERRIDE_TAXES'
      Size = 1
    end
    object PR_CHECKRECIPROCATE_SUI: TStringField
      FieldName = 'RECIPROCATE_SUI'
      Size = 1
    end
    object PR_CHECKDISABLE_SHORTFALLS: TStringField
      FieldName = 'DISABLE_SHORTFALLS'
      Size = 1
    end
    object PR_CHECKUPDATE_BALANCE: TStringField
      FieldName = 'UPDATE_BALANCE'
      Size = 1
    end
  end
  object PR_CHECK_LINES: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'AGENCY_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_PIECES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_BRANCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DEPARTMENT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DIVISION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_JOBS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_SHIFTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DEDUCTION_SHORTFALL_CARRYOVER'
        DataType = ftFloat
      end
      item
        Name = 'EE_SCHEDULED_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_SUI_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'E_D_DIFFERENTIAL_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'HOURS_OR_PIECES'
        DataType = ftFloat
      end
      item
        Name = 'LINE_ITEM_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'LINE_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PR_CHECK_LINES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'RATE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'RATE_OF_PAY'
        DataType = ftFloat
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'USER_OVERRIDE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'REDUCED_HOURS'
        DataType = ftString
        Size = 1
      end>
    BeforePost = PR_CHECK_LINESBeforePost
    OnNewRecord = PR_CHECK_LINESNewRecord
    Left = 420
    object PR_CHECK_LINESCL_AGENCY_NBR: TIntegerField
      FieldName = 'CL_AGENCY_NBR'
      Origin = '"PR_CHECK_LINES"."CL_AGENCY_NBR"'
    end
    object PR_CHECK_LINESAGENCY_STATUS: TStringField
      FieldName = 'AGENCY_STATUS'
      Origin = '"PR_CHECK_LINES"."AGENCY_STATUS"'
      Size = 1
    end
    object PR_CHECK_LINESAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LINESCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
      OnChange = PR_CHECK_LINESCL_E_DS_NBRChange
    end
    object PR_CHECK_LINESCL_PIECES_NBR: TIntegerField
      FieldName = 'CL_PIECES_NBR'
    end
    object PR_CHECK_LINESCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object PR_CHECK_LINESCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object PR_CHECK_LINESCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object PR_CHECK_LINESCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
    object PR_CHECK_LINESCO_SHIFTS_NBR: TIntegerField
      FieldName = 'CO_SHIFTS_NBR'
    end
    object PR_CHECK_LINESCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object PR_CHECK_LINESCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
    end
    object PR_CHECK_LINESDEDUCTION_SHORTFALL_CARRYOVER: TFloatField
      FieldName = 'DEDUCTION_SHORTFALL_CARRYOVER'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LINESEE_SCHEDULED_E_DS_NBR: TIntegerField
      FieldName = 'EE_SCHEDULED_E_DS_NBR'
    end
    object PR_CHECK_LINESEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
    end
    object PR_CHECK_LINESEE_SUI_STATES_NBR: TIntegerField
      FieldName = 'EE_SUI_STATES_NBR'
    end
    object PR_CHECK_LINESE_D_DIFFERENTIAL_AMOUNT: TFloatField
      FieldName = 'E_D_DIFFERENTIAL_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LINESHOURS_OR_PIECES: TFloatField
      FieldName = 'HOURS_OR_PIECES'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LINESLINE_ITEM_DATE: TDateTimeField
      FieldName = 'LINE_ITEM_DATE'
    end
    object PR_CHECK_LINESLINE_TYPE: TStringField
      FieldName = 'LINE_TYPE'
      Size = 1
    end
    object PR_CHECK_LINESPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object PR_CHECK_LINESPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object PR_CHECK_LINESRATE_NUMBER: TIntegerField
      FieldName = 'RATE_NUMBER'
    end
    object PR_CHECK_LINESRATE_OF_PAY: TFloatField
      FieldName = 'RATE_OF_PAY'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LINESE_D_Code_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_Code_Lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object PR_CHECK_LINESState_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'State_Lookup'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object PR_CHECK_LINESSUI_State_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_State_Lookup'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_SUI_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object PR_CHECK_LINESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_CHECK_LINESUSER_OVERRIDE: TStringField
      FieldName = 'USER_OVERRIDE'
      Size = 1
    end
    object PR_CHECK_LINESREDUCED_HOURS: TStringField
      FieldName = 'REDUCED_HOURS'
      Size = 1
    end
    object PR_CHECK_LINESNONRES_EE_LOCALS_NBR: TIntegerField
      FieldName = 'NONRES_EE_LOCALS_NBR'
    end
    object PR_CHECK_LINESWORK_ADDRESS_OVR: TStringField
      FieldName = 'WORK_ADDRESS_OVR'
      Size = 1
    end
  end
  object PR_CHECK_STATES: TevClientDataSet
    BeforePost = PR_CHECK_STATESBeforePost
    Left = 468
    object PR_CHECK_STATESEE_SDI_OVERRIDE: TFloatField
      FieldName = 'EE_SDI_OVERRIDE'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESEE_SDI_SHORTFALL: TFloatField
      FieldName = 'EE_SDI_SHORTFALL'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESEE_SDI_TAX: TFloatField
      FieldName = 'EE_SDI_TAX'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESEE_SDI_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_SDI_TAXABLE_WAGES'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
    end
    object PR_CHECK_STATESEXCLUDE_ADDITIONAL_STATE: TStringField
      FieldName = 'EXCLUDE_ADDITIONAL_STATE'
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_SDI: TStringField
      FieldName = 'EXCLUDE_SDI'
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_STATE: TStringField
      FieldName = 'EXCLUDE_STATE'
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_SUI: TStringField
      FieldName = 'EXCLUDE_SUI'
      Size = 1
    end
    object PR_CHECK_STATESPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object PR_CHECK_STATESPR_CHECK_STATES_NBR: TIntegerField
      FieldName = 'PR_CHECK_STATES_NBR'
    end
    object PR_CHECK_STATESSTATE_OVERRIDE_TYPE: TStringField
      FieldName = 'STATE_OVERRIDE_TYPE'
      Size = 1
    end
    object PR_CHECK_STATESSTATE_OVERRIDE_VALUE: TFloatField
      FieldName = 'STATE_OVERRIDE_VALUE'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESSTATE_SHORTFALL: TFloatField
      FieldName = 'STATE_SHORTFALL'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESSTATE_TAX: TFloatField
      FieldName = 'STATE_TAX'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESSTATE_TAXABLE_WAGES: TFloatField
      FieldName = 'STATE_TAXABLE_WAGES'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_STATESTAX_AT_SUPPLEMENTAL_RATE: TStringField
      FieldName = 'TAX_AT_SUPPLEMENTAL_RATE'
      Size = 1
    end
    object PR_CHECK_STATESSTATE_MARITAL_STATUS: TStringField
      FieldName = 'STATE_MARITAL_STATUS'
      Size = 4
    end
    object PR_CHECK_STATESSTATE_NUMBER_WITHHOLDING_ALLOW: TIntegerField
      FieldName = 'STATE_NUMBER_WITHHOLDING_ALLOW'
    end
    object PR_CHECK_STATESState_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'State_Lookup'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object PR_CHECK_STATESSTATE_GROSS_WAGES: TFloatField
      FieldName = 'STATE_GROSS_WAGES'
    end
    object PR_CHECK_STATESEE_SDI_GROSS_WAGES: TFloatField
      FieldName = 'EE_SDI_GROSS_WAGES'
    end
    object PR_CHECK_STATESER_SDI_GROSS_WAGES: TFloatField
      FieldName = 'ER_SDI_GROSS_WAGES'
    end
  end
  object PR_CHECK_SUI: TevClientDataSet
    BeforePost = PR_CHECK_SUIBeforePost
    Left = 516
    object PR_CHECK_SUICO_SUI_NBR: TIntegerField
      FieldName = 'CO_SUI_NBR'
    end
    object PR_CHECK_SUIOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_SUIPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object PR_CHECK_SUIPR_CHECK_SUI_NBR: TIntegerField
      FieldName = 'PR_CHECK_SUI_NBR'
    end
    object PR_CHECK_SUISUI_GROSS_WAGES: TFloatField
      FieldName = 'SUI_GROSS_WAGES'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_SUISUI_TAX: TFloatField
      FieldName = 'SUI_TAX'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_SUISUI_TAXABLE_WAGES: TFloatField
      FieldName = 'SUI_TAXABLE_WAGES'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_SUIDescription_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Description_Lookup'
      LookupDataSet = DM_CO_SUI.CO_SUI
      LookupKeyFields = 'CO_SUI_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_SUI_NBR'
      Lookup = True
    end
  end
  object PR_CHECK_LOCALS: TevClientDataSet
    BeforePost = PR_CHECK_LOCALSBeforePost
    Left = 564
    object PR_CHECK_LOCALSEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object PR_CHECK_LOCALSEXCLUDE_LOCAL: TStringField
      FieldName = 'EXCLUDE_LOCAL'
      Size = 1
    end
    object PR_CHECK_LOCALSLOCAL_SHORTFALL: TFloatField
      FieldName = 'LOCAL_SHORTFALL'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LOCALSLOCAL_TAX: TFloatField
      FieldName = 'LOCAL_TAX'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LOCALSLOCAL_TAXABLE_WAGE: TFloatField
      FieldName = 'LOCAL_TAXABLE_WAGE'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LOCALSOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECK_LOCALSPR_CHECK_LOCALS_NBR: TIntegerField
      FieldName = 'PR_CHECK_LOCALS_NBR'
    end
    object PR_CHECK_LOCALSPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object PR_CHECK_LOCALSLocal_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name_Lookup'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'EE_LOCALS_NBR'
      Lookup = True
    end
    object PR_CHECK_LOCALSLOCAL_GROSS_WAGES: TFloatField
      FieldName = 'LOCAL_GROSS_WAGES'
    end
    object PR_CHECK_LOCALSREPORTABLE: TStringField
      FieldName = 'REPORTABLE'
      Size = 1
    end
    object PR_CHECK_LOCALSCO_LOCATIONS_NBR: TIntegerField
      FieldName = 'CO_LOCATIONS_NBR'
    end
    object PR_CHECK_LOCALSNONRES_EE_LOCALS_NBR: TIntegerField
      FieldName = 'NONRES_EE_LOCALS_NBR'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 268
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 300
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 236
  end
  object PR: TevClientDataSet
    FieldDefs = <
      item
        Name = 'ACTUAL_CALL_IN_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'CHECK_DATE_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'MARK_LIABS_PAID_DEFAULT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_ACH'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_AGENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_BILLING'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_R_C_B_0R_N'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_TAX_DEPOSITS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'INVOICE_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAYROLL_COMMENTS'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'PROCESS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'RUN_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SCHEDULED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SCHEDULED_CALL_IN_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SCHEDULED_CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SCHEDULED_DELIVERY_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SCHEDULED_PROCESS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'PAYROLL_TYPE'
        DataType = ftString
        Size = 1
      end>
    Left = 492
    Top = 56
    object PRACTUAL_CALL_IN_DATE: TDateTimeField
      FieldName = 'ACTUAL_CALL_IN_DATE'
    end
    object PRCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object PRCHECK_DATE_STATUS: TStringField
      FieldName = 'CHECK_DATE_STATUS'
      Size = 1
    end
    object PRCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object PRMARK_LIABS_PAID_DEFAULT: TStringField
      FieldName = 'MARK_LIABS_PAID_DEFAULT'
      Size = 1
    end
    object PREXCLUDE_ACH: TStringField
      FieldName = 'EXCLUDE_ACH'
      Size = 1
    end
    object PREXCLUDE_AGENCY: TStringField
      FieldName = 'EXCLUDE_AGENCY'
      Size = 1
    end
    object PREXCLUDE_BILLING: TStringField
      FieldName = 'EXCLUDE_BILLING'
      Size = 1
    end
    object PREXCLUDE_R_C_B_0R_N: TStringField
      FieldName = 'EXCLUDE_R_C_B_0R_N'
      Size = 1
    end
    object PREXCLUDE_TAX_DEPOSITS: TStringField
      FieldName = 'EXCLUDE_TAX_DEPOSITS'
      Size = 1
    end
    object PRFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object PRINVOICE_PRINTED: TStringField
      FieldName = 'INVOICE_PRINTED'
      Size = 1
    end
    object PRPAYROLL_COMMENTS: TBlobField
      FieldName = 'PAYROLL_COMMENTS'
      Size = 8
    end
    object PRPROCESS_DATE: TDateTimeField
      FieldName = 'PROCESS_DATE'
    end
    object PRPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PRRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object PRSCHEDULED: TStringField
      FieldName = 'SCHEDULED'
      Size = 1
    end
    object PRSCHEDULED_CALL_IN_DATE: TDateTimeField
      FieldName = 'SCHEDULED_CALL_IN_DATE'
    end
    object PRSCHEDULED_CHECK_DATE: TDateTimeField
      FieldName = 'SCHEDULED_CHECK_DATE'
    end
    object PRSCHEDULED_DELIVERY_DATE: TDateTimeField
      FieldName = 'SCHEDULED_DELIVERY_DATE'
    end
    object PRSCHEDULED_PROCESS_DATE: TDateTimeField
      FieldName = 'SCHEDULED_PROCESS_DATE'
    end
    object PRSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object PRSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object PRTYPE: TStringField
      FieldName = 'PAYROLL_TYPE'
      Size = 1
    end
    object PRTAX_IMPOUND: TStringField
      FieldName = 'TAX_IMPOUND'
      Size = 1
    end
    object PRDD_IMPOUND: TStringField
      FieldName = 'DD_IMPOUND'
      Size = 1
    end
    object PRWC_IMPOUND: TStringField
      FieldName = 'WC_IMPOUND'
      Size = 1
    end
    object PRTRUST_IMPOUND: TStringField
      FieldName = 'TRUST_IMPOUND'
      Size = 1
    end
    object PRBILLING_IMPOUND: TStringField
      FieldName = 'BILLING_IMPOUND'
      Size = 1
    end
  end
  object PR_BATCH: TevClientDataSet
    Left = 548
    Top = 56
    object PR_BATCHCO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
    end
    object PR_BATCHCO_PR_FILTERS_NBR: TIntegerField
      FieldName = 'CO_PR_FILTERS_NBR'
    end
    object PR_BATCHFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object PR_BATCHFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Size = 1
    end
    object PR_BATCHLOAD_DBDT_DEFAULTS: TStringField
      FieldName = 'LOAD_DBDT_DEFAULTS'
      Size = 1
    end
    object PR_BATCHPAY_SALARY: TStringField
      FieldName = 'PAY_SALARY'
      Size = 1
    end
    object PR_BATCHPAY_STANDARD_HOURS: TStringField
      FieldName = 'PAY_STANDARD_HOURS'
      Size = 1
    end
    object PR_BATCHPERIOD_BEGIN_DATE: TDateTimeField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object PR_BATCHPERIOD_BEGIN_DATE_STATUS: TStringField
      FieldName = 'PERIOD_BEGIN_DATE_STATUS'
      Size = 1
    end
    object PR_BATCHPERIOD_END_DATE: TDateTimeField
      FieldName = 'PERIOD_END_DATE'
    end
    object PR_BATCHPERIOD_END_DATE_STATUS: TStringField
      FieldName = 'PERIOD_END_DATE_STATUS'
      Size = 1
    end
    object PR_BATCHPR_BATCH_NBR: TIntegerField
      FieldName = 'PR_BATCH_NBR'
    end
    object PR_BATCHPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
  end
  object PR_CHECK_LINE_LOCALS: TevClientDataSet
    Left = 612
    Top = 56
    object PR_CHECK_LINE_LOCALSEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object PR_CHECK_LINE_LOCALSEXEMPT_EXCLUDE: TStringField
      FieldName = 'EXEMPT_EXCLUDE'
      Size = 1
    end
    object PR_CHECK_LINE_LOCALSPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object PR_CHECK_LINE_LOCALSPR_CHECK_LINE_LOCALS_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINE_LOCALS_NBR'
    end
  end
  object evPopupMenu1: TevPopupMenu
    Left = 716
    Top = 56
    object Print1: TMenuItem
      Caption = '&Print Results (F7)'
      OnClick = Print1Click
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 716
    Top = 104
  end
  object dsEE: TevDataSource
    DataSet = DM_EE.EE
    OnDataChange = dsEEDataChange
    Left = 832
    Top = 4
  end
  object wwdsSubMaster: TevDataSource
    DataSet = PR_LIST
    Left = 525
    Top = 113
  end
  object wwdsSubMaster1: TevDataSource
    DataSet = PR_BATCH_LIST
    MasterDataSource = wwdsSubMaster
    Left = 589
    Top = 112
  end
  object PR_LIST: TevClientDataSet
    ProviderName = 'PR_PROV'
    Left = 524
    Top = 144
  end
  object PR_BATCH_LIST: TevClientDataSet
    ProviderName = 'PR_BATCH_PROV'
    Left = 588
    Top = 144
  end
  object PageControlImages: TevImageList
    Left = 868
    Top = 4
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CECECE00CECECE00CECECE00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00D6D6D600CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE004A84AD004A84AD004A84AD00B5BDC6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000808DE000000DE000000DE000000
      DE000000DE000000DE000000DE000000DE000000DE000000DE000000DE000000
      DE000000DE000000DE000000DE000808DE00EFBD7B00EFB57300EFB57300EFB5
      7300EFB57300EFB57300EFBD7300F7BD7300F7BD7300F7BD7300FFC67300317B
      B500739CBD0021ADFF0094CEEF004294C600DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFF700DEDEB500EFB57300FFEFCE00FFEFC600FFEF
      C600FFEFC600FFF7CE00C6B5A5007B7B7B0073737B0073737B0073737B008C8C
      8400B5A59C009CC6D600ADEFFF004294C6008CBAA3006DB08F006EB18F006FB1
      8F006EB18F006EB08E006EB18E006EB18E006EB18E006EB18E006EB08E006EB1
      8F006FB190006EB18F006DB08F008CBAA3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E700184AFF000031FF000031
      FF000031FF000031FF000031FF000031FF000031FF000031FF000031FF000031
      FF000031FF000031FF00184AFF000000E700EFB57300FFEFCE00FFF7E700FFF7
      E700FFEFBD008C8C840094949C00D6D6DE00E7E7E700E7E7E700D6D6DE009C9C
      9C008C848400F7EFE700BDDEEF004294CE006AAD8B00CBF4DB008CC7B50083BE
      A70093CBBB0092CAB90092CBBA0093CBBB0093CBBB0092CBBA0092CAB90093CB
      BB007DBBA00085C1AC00CBF4DC006AAE8B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B500EFB57300FFEFCE00FFF7DE00FFF7
      E700C6AD9C009C9C9C00E7E7E700EFC69400F7CE8400F7DE8C00F7E7AD00E7E7
      E7009C9C9C00B5ADAD003994CE000000000065A98600CAF3DC005FA27C004E93
      6800B8EBCF00B7E9CD0098D1B20087C4A30087C4A30098D1B200B7E9CD00B8EB
      CF004F926900599F7700CAF3DD0065A986000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F7003963FF000021FF000021
      FF000021FF000021FF000021FF000021FF000021FF000021FF000021FF000021
      FF000021FF000021FF003963FF000000E700EFB57300FFEFD600FFDEB500FFE7
      B50084848400E7E7E700E7BD8C00EFCE8C00F7D68C00FFE79C00FFEFA500F7E7
      AD00E7E7E7008C847B00000000000000000062A58100CDF2E00073B59300B3E7
      CD00B0E4CA00B0E4CA006BAC8900B2E3C700B2E3C7006BAC8900B0E4CA00B0E4
      CA00B3E7CD0074B59300CDF2E00062A581000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A55A0000FFFFE700E76B0000FFF7
      C600E76B0000FFF7C600E76B0000FFF7C600EF6B0000FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B500EFB57300FFEFD600FFDEAD00FFE7
      B5007B7B7B00F7F7FF00E7B57300F7DEB500F7DEA500F7DE9C00FFE79C00F7DE
      8C00F7F7FF008C84840000000000000000005FA37E00D0F2E4006CAD8B00ABE2
      C80051946D00ABE2C8006DAC8800B0E2C600B0E2C6006DAC8800ABE2C8005194
      6D00ABE2C8006CAD8B00D0F2E4005FA37E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000945A0000EFA55200FFE7CE00CE5A
      0000FFEFD600CE5A0000FFEFD600CE5A0000FFEFC6000010FF000010FF000010
      FF000010FF000010FF006373FF000000E700EFB57300FFEFD600FFEFDE00FFF7
      DE00847B7B00FFFFFF00E7B57300F7EFD600F7DEB500F7DEA500F7D68C00EFCE
      7B00FFFFFF008C84840000000000000000005CA07A00D8F5E80067AA8500A7E0
      C600A5DEC400A5DEC40061A17C00B2E3C600B2E3C60061A27C00A5DEC400A5DE
      C400A7E0C60067AA8500D8F5E8005CA07A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094520000FFFFFF00BD520000F7E7
      D600BD520000F7E7D600BD520000F7E7D600C6520000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00D6D6B500EFB57300FFEFDE00FFEFD600FFF7
      DE008C848400EFF7F700E7B58400FFEFE700F7EFD600F7DEB500EFCE9400EFCE
      9400EFF7F700948C8C000000000000000000599D7500DFF7EF00549A70005EA2
      7D009FDBC3009FDBC20080BFA0007EB8940078B38F0080BFA0009FDBC2009FDB
      C3005EA27D00559B7300DFF7EF00599D75000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094520000E7B58400F7E7CE00B54A
      0000F7E7D600B54A0000F7E7D600B54A0000FFE7C6000000FF000008FF000008
      FF000008FF000000FF00848CFF000000E700EFB57300FFF7E700FFD6A500FFD6
      A500C6BDB500ADADAD0000000000E7B58400E7BD8400E7BD8400EFC694000000
      0000ADADB500C6A584000000000000000000569B7200E5F9F400448B5E00468C
      61005A9E76005A9D75005A9E76005B9F77005B9F77005A9E76005A9D75005A9E
      7600468C6100448B5E00E5F9F400569B72000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009452000000000000A5420000F7DE
      CE00A5420000F7DECE00A5420000F7DECE00AD420000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DEDEB500EFB56B00FFF7E700FFD69C00FFD6
      9C00FFF7DE00A59C9C00B5B5B500FFFFFF000000000000000000FFFFFF00B5B5
      B500A5A5A500F7BD7300000000000000000054976F00E7FAF500E3F6F100E3F6
      F100E3F5F000E2F5F000E2F5F000E2F5F000E2F5F000E2F5F000E2F5F000E3F5
      F000E3F6F100E3F6F100E7FAF50054976F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000945A0000E7C6AD0000000000E7C6
      A50000000000E7C6A50000000000E7C6A500FFFFFF009CADFF00A5ADFF00A5AD
      FF00A5ADFF00ADADFF00ADB5FF000000DE00EFB56B00FFF7EF00FFEFCE00FFEF
      D600FFCE9C00FFCE9C00CEBDB500949494008C8C8C008C8C8C0094949400CEBD
      AD00FFFFEF00EFB5730000000000000000008CB99E0089C6A600B0E7CC00AEE5
      CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5
      CA00AEE5CA00B0E7CC0089C6A6008CB99E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BD944A00945A0000945200009452
      0000945200009452000094520000945A0000A55A00000000EF000000DE000000
      DE000000DE000000DE000000DE003939E700EFB56B00FFFFF700FFE7CE00FFE7
      CE00FFCE9400FFCE9400FFEFCE00FFEFCE00FFCE9400FFCE9400FFEFCE00FFEF
      CE00FFFFF700EFB56B000000000000000000000000008AB79B00509269004F91
      68004F9168004F9168004F9168004F9168004F9168004F9168004F9168004F91
      68004F916800509269008AB79B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFB57300FFFFFF00FFFFF700FFFF
      F700FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFFF00FFFFF700FFFF
      F700FFFFFF00EFB5730000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFBD8400EFB57300EFB56B00EFB5
      6B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB5
      6B00EFB57300EFBD840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFF0FFFF000000000000FFFF0000
      0000000000000000000000000000000000000000000000000000000100000000
      0000000300000000000000030000000000000003000000000000000300000000
      0000021300000000400000C3000000002A000003000000000000000380010000
      FFFF0003FFFF0000FFFF0003FFFF000000000000000000000000000000000000
      000000000000}
  end
end
