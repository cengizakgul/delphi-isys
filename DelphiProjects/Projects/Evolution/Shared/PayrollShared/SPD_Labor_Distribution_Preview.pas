// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Labor_Distribution_Preview;

interface

uses
   Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, Classes, Controls, ExtCtrls, Forms,
  SysUtils, EvUtils, EvTypes, Variants, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvExceptions, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel;

type
  TLabor_Distribution_Preview = class(TForm)
    Panel1: TevPanel;
    Button2: TevBitBtn;
    wwDBGrid1: TevDBGrid;
    wwClientDataSet1: TevClientDataSet;
    wwDataSource1: TevDataSource;
    wwClientDataSet1Earning_Code: TStringField;
    wwClientDataSet1Hours: TFloatField;
    wwClientDataSet1Amount: TCurrencyField;
    wwClientDataSet1Earning_Description: TStringField;
    wwClientDataSet1DBDT: TStringField;
    wwClientDataSet1Job: TStringField;
    wwClientDataSet1WComp: TStringField;
    fpPreview: TisUIFashionPanel;
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ShowDistribution(EmployeeNbr: Integer; PR_CHECK_LINES, EE, EE_AUTOLABOR_DISTRIBUTION,
      CL_E_DS, CL_E_D_GROUP_CODES, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS, CO_WORKERS_COMP: TevClientDataSet);
  end;

var
  Labor_Distribution_Preview: TLabor_Distribution_Preview;

implementation

{$R *.DFM}

procedure TLabor_Distribution_Preview.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TLabor_Distribution_Preview.ShowDistribution(EmployeeNbr: Integer; PR_CHECK_LINES, EE, EE_AUTOLABOR_DISTRIBUTION,
  CL_E_DS, CL_E_D_GROUP_CODES, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS, CO_WORKERS_COMP: TevClientDataSet);
var
  ALDGroupNbr: Integer;
  Amount, Hours, Percentage: Double;
  EDCodeType, MyString: string;
begin
  EE.Locate('EE_NBR', EmployeeNbr, []);
  if EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').IsNull then
    raise ECanNotPerformOperation.CreateHelp('Employee does not have Auto-Labor Distribution group setup!', IDH_CanNotPerformOperation);
  ALDGroupNbr := EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').AsInteger;

  EE_AUTOLABOR_DISTRIBUTION.Filter := 'EE_NBR=' + IntToStr(EmployeeNbr);
  EE_AUTOLABOR_DISTRIBUTION.Filtered := True;

  // Test Auto Labor percentages for EE
  Percentage := 0;
  EE_AUTOLABOR_DISTRIBUTION.First;
  while not EE_AUTOLABOR_DISTRIBUTION.EOF do
  begin
    Percentage := Percentage + ConvertNull(EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0);
    EE_AUTOLABOR_DISTRIBUTION.Next;
  end;
  if RoundAll(Percentage, 2) <> 100 then
    raise EWrongALDPrecentages.CreateHelp('A sum of Auto-Labor Distribution percentages for EE does not equal 100%!', IDH_InconsistentData);

  wwClientDataSet1.CreateDataSet;

  with PR_CHECK_LINES do
  begin
    First;
    while not EOF do
    begin
      EDCodeType := CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
      if ((EDCodeType[1] = 'E') or TypeIsDistributedMemo(EDCodeType))
      and (FieldByName('CO_DIVISION_NBR').IsNull) and (FieldByName('CO_BRANCH_NBR').IsNull)
      and (FieldByName('CO_DEPARTMENT_NBR').IsNull) and (FieldByName('CO_TEAM_NBR').IsNull)
      and (FieldByName('CO_JOBS_NBR').IsNull) and (FieldByName('CO_WORKERS_COMP_NBR').IsNull) then
      begin
        if CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([ALDGroupNbr, FieldByName('CL_E_DS_NBR').Value]), []) then
        begin
          Amount := 0;
          Hours := 0;
          Percentage := 0;
          EE_AUTOLABOR_DISTRIBUTION.First;
          while not EE_AUTOLABOR_DISTRIBUTION.EOF do
          begin
            wwClientDataSet1.Insert;
            wwClientDataSet1.FieldByName('Earning_Code').Value := CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value,
              'CUSTOM_E_D_CODE_NUMBER');
            wwClientDataSet1.FieldByName('Earning_Description').Value := CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value,
              'DESCRIPTION');
            Percentage := Percentage + ConvertNull(EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0);
            wwClientDataSet1.FieldByName('Amount').Value := RoundTwo(ConvertNull(FieldByName('AMOUNT').Value *
              ConvertNull(EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0) * 0.01, 0));
            Amount := Amount + wwClientDataSet1.FieldByName('Amount').Value;
            wwClientDataSet1.FieldByName('Hours').Value := RoundTwo(ConvertNull(FieldByName('HOURS_OR_PIECES').Value *
              ConvertNull(EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0) * 0.01, 0));
            Hours := Hours + wwClientDataSet1.FieldByName('Hours').Value;
            if RoundAll(Percentage, 2) = 100 then
            begin
              wwClientDataSet1.FieldByName('Amount').Value := RoundTwo(ConvertNull(wwClientDataSet1.FieldByName('Amount').Value +
                (FieldByName('AMOUNT').Value - Amount), 0));
              wwClientDataSet1.FieldByName('Hours').Value := RoundTwo(ConvertNull(wwClientDataSet1.FieldByName('Hours').Value +
                (FieldByName('HOURS_OR_PIECES').Value - Hours), 0));
            end;

            MyString := Trim(VarToStr(CO_TEAM.Lookup('CO_TEAM_NBR', EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_TEAM_NBR').Value,
              'CUSTOM_TEAM_NUMBER')));
            wwClientDataSet1.FieldByName('DBDT').Value := MyString;

            MyString := Trim(VarToStr(CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR',
              EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_DEPARTMENT_NBR').Value, 'CUSTOM_DEPARTMENT_NUMBER')));
            if wwClientDataSet1.FieldByName('DBDT').AsString = '' then
              wwClientDataSet1.FieldByName('DBDT').Value := MyString
            else
              wwClientDataSet1.FieldByName('DBDT').Value := MyString + ' | ' + wwClientDataSet1.FieldByName('DBDT').AsString;

            MyString := Trim(VarToStr(CO_BRANCH.Lookup('CO_BRANCH_NBR', EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_BRANCH_NBR').Value,
              'CUSTOM_BRANCH_NUMBER')));
            if wwClientDataSet1.FieldByName('DBDT').AsString = '' then
              wwClientDataSet1.FieldByName('DBDT').Value := MyString
            else
              wwClientDataSet1.FieldByName('DBDT').Value := MyString + ' | ' + wwClientDataSet1.FieldByName('DBDT').AsString;

            MyString := Trim(VarToStr(CO_DIVISION.Lookup('CO_DIVISION_NBR', EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_DIVISION_NBR').Value,
              'CUSTOM_DIVISION_NUMBER')));
            if wwClientDataSet1.FieldByName('DBDT').AsString = '' then
              wwClientDataSet1.FieldByName('DBDT').Value := MyString
            else
              wwClientDataSet1.FieldByName('DBDT').Value := MyString + ' | ' + wwClientDataSet1.FieldByName('DBDT').AsString;

            wwClientDataSet1.FieldByName('Job').Value := CO_JOBS.Lookup('CO_JOBS_NBR',
              EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_JOBS_NBR').Value, 'DESCRIPTION');
            wwClientDataSet1.FieldByName('WComp').Value := CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR',
              EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_WORKERS_COMP_NBR').Value, 'WORKERS_COMP_CODE');

            wwClientDataSet1.Post;
            EE_AUTOLABOR_DISTRIBUTION.Next;
          end;
        end;
      end;
      Next;
    end;
  end;
  EE_AUTOLABOR_DISTRIBUTION.Filtered := False;

  wwClientDataSet1.IndexFieldNames := 'Earning_Code';
  ShowModal;
end;

initialization
  RegisterClass(TLABOR_DISTRIBUTION_PREVIEW);

end.
