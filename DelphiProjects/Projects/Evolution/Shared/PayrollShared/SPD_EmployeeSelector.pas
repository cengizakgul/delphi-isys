// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EmployeeSelector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, StdCtrls, Buttons,  Db, Wwdatsrc, Grids,
  Wwdbigrd, Wwdbgrid, Variants, SDDClasses, ISBasicClasses, SDataDictclient, EvUIComponents;

type
  TEmployeeSelector = class(TForm)
    evDBGrid1: TevDBGrid;
    dsEmployee: TevDataSource;
    bbtnOK: TevBitBtn;
    bbtnCancel: TevBitBtn;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    procedure bbtnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    EENumbers: Variant;
  end;

var
  EmployeeSelector: TEmployeeSelector;

implementation

{$R *.DFM}

procedure TEmployeeSelector.bbtnOKClick(Sender: TObject);
var
  I: Integer;
begin
  if evDBGrid1.SelectedList.Count = 0 then
    EENumbers := VarArrayOf([dsEmployee.DataSet.FieldByName('EE_NBR').AsInteger])
  else
  begin
    EENumbers := VarArrayCreate([0, evDBGrid1.SelectedList.Count - 1], varInteger);
    dsEmployee.DataSet.DisableControls;
    for I := 0 to evDBGrid1.SelectedList.Count - 1 do
    begin
      dsEmployee.DataSet.GotoBookmark(evDBGrid1.SelectedList.Items[I]);
      EENumbers[I] := dsEmployee.DataSet.FieldByName('EE_NBR').AsInteger;
    end;
    dsEmployee.DataSet.EnableControls;
  end;
end;

end.
