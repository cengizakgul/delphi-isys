// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Check_Preview;

interface

uses
   Grids, StdCtrls, Buttons, Classes, Controls, ExtCtrls, Forms,
  SDataStructure, SLogCheckReprint, EvUtils, EvTypes, SysUtils, EvConsts,
  SPopulateRecords, EvStreamUtils, ISBasicClasses, EvContext, EvInitApp, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TCheck_Preview = class(TForm)
    Panel1: TevPanel;
    bPrintCheck: TevBitBtn;
    Button2: TevBitBtn;
    MyStringGrid: TevStringGrid;
    bPreviewCheck: TevBitBtn;
    procedure bPrintCheckClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bPreviewCheckClick(Sender: TObject);
  private
    { Private declarations }
    DisabledControls: Boolean;
    procedure PrintCheck(Preview: Boolean);
  public
    { Public declarations }
  end;

var
  Check_Preview: TCheck_Preview;

implementation

uses SSecurityInterface;

{$R *.DFM}

procedure TCheck_Preview.bPrintCheckClick(Sender: TObject);
begin
  PrintCheck(False);
end;

procedure TCheck_Preview.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TCheck_Preview.FormShow(Sender: TObject);
begin
  DisabledControls := False;
  if not DM_PAYROLL.PR_CHECK_LINES.ControlsDisabled then
  begin
    DM_PAYROLL.PR_CHECK_LINES.DisableControls;
    DisabledControls := True;
  end;

  with DM_PAYROLL do
  begin
    PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + IntToStr(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger));
    if not PR_CHECK_SUI.EOF then
    begin
      DM_COMPANY.CO_SUI.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
    end;
    PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + IntToStr(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger));
    PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + IntToStr(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger));

    PopulateStringGrid(MyStringGrid, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS);
  end;
  bPreviewCheck.Enabled := ctx_AccountRights.Functions.GetState('FIELDS_WAGES') = stEnabled;
  bPrintCheck.Enabled := bPreviewCheck.Enabled;
  if DM_PAYROLL.PR.Active then
  begin
    if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED then
      bPrintCheck.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') = stEnabled
    else
      bPrintCheck.Enabled := False;
  end;
end;

procedure TCheck_Preview.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DisabledControls then
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
end;

procedure TCheck_Preview.PrintCheck(Preview: Boolean);
var
  TempList: TStringList;
  PrintJob: IisStream;
  sReason, sWarnings, sErrors: String;
begin
  try
    TempList := TStringList.Create;
    try
      TempList.Add(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
      if not Preview then
        sReason := AskForCheckReprintReason;
//        LogCheckReprint(TempList, False, DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);

      ctx_StartWait;
      try
        DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString);
        PrintJob := ctx_PayrollProcessing.ReprintPayroll(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger,
          False, False, False, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString, '', '', '',
          DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString,
                                    0, False, False, False, False, True, 0, sReason, sWarnings, sErrors);
        if Preview then
          ctx_RWLocalEngine.Preview(PrintJob)
        else
          ctx_RWLocalEngine.Print(PrintJob);
      finally
        ctx_EndWait;
      end;
    finally
      TempList.Free;
    end;
  finally
    if isStandalone then  //TODO: WTF?
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
  end;
end;

procedure TCheck_Preview.bPreviewCheckClick(Sender: TObject);
begin
  PrintCheck(True);
end;

initialization
  RegisterClass(TCHECK_PREVIEW);

finalization
  UnregisterClass(TCHECK_PREVIEW);

end.
