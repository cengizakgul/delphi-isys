// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_TaxCalculator;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Wwdotdot, Wwdbcomb,  StdCtrls, ExtCtrls, DBCtrls, Mask,
  wwdbedit, Buttons, Grids, Wwdbigrd, Wwdbgrid, wwdblook, ComCtrls, Db,
   Wwdatsrc, SDataStructure, evUtils, EvConsts, SFieldCodeValues,
  Wwdbspin, Menus, Printers, Variants, EvTypes, SDDClasses, EvContext,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, SDataDictsystem, SDataDictclient, EvExceptions,
  EvUIUtils, EvUIComponents, EvClientDataSet, isUIEdit, isUIwwDBComboBox,
  isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIwwDBLookupCombo, ImgList, isUIFashionPanel, EvStreamUtils;

type
  TTaxCalculator = class(TForm)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    dsCHECK: TevDataSource;
    dsCHECK_LINES: TevDataSource;
    dsCHECK_STATES: TevDataSource;
    dsCHECK_SUI: TevDataSource;
    dsCHECK_LOCALS: TevDataSource;
    PR_CHECK: TevClientDataSet;
    PR_CHECK_LINES: TevClientDataSet;
    PR_CHECK_STATES: TevClientDataSet;
    PR_CHECK_SUI: TevClientDataSet;
    PR_CHECK_LOCALS: TevClientDataSet;
    PR_CHECKCHECK_TYPE: TStringField;
    PR_CHECKEE_EIC_TAX: TFloatField;
    PR_CHECKEE_MEDICARE_TAX: TFloatField;
    PR_CHECKEE_MEDICARE_TAXABLE_WAGES: TFloatField;
    PR_CHECKEE_NBR: TIntegerField;
    PR_CHECKEE_OASDI_TAX: TFloatField;
    PR_CHECKEE_OASDI_TAXABLE_TIPS: TFloatField;
    PR_CHECKEE_OASDI_TAXABLE_WAGES: TFloatField;
    PR_CHECKEXCLUDE_ADDITIONAL_FEDERAL: TStringField;
    PR_CHECKEXCLUDE_ALL_SCHED_E_D_CODES: TStringField;
    PR_CHECKEXCLUDE_AUTO_DISTRIBUTION: TStringField;
    PR_CHECKEXCLUDE_DD: TStringField;
    PR_CHECKEXCLUDE_DD_EXCEPT_NET: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_EIC: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_MEDICARE: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_OASDI: TStringField;
    PR_CHECKEXCLUDE_FEDERAL: TStringField;
    PR_CHECKEXCLUDE_FROM_AGENCY: TStringField;
    PR_CHECKEXCLUDE_SCH_E_D_EXCEPT_PENSION: TStringField;
    PR_CHECKEXCLUDE_SCH_E_D_FROM_AGCY_CHK: TStringField;
    PR_CHECKEXCLUDE_TIME_OFF_ACCURAL: TStringField;
    PR_CHECKFEDERAL_SHORTFALL: TFloatField;
    PR_CHECKFEDERAL_TAX: TFloatField;
    PR_CHECKFEDERAL_TAXABLE_WAGES: TFloatField;
    PR_CHECKGROSS_WAGES: TFloatField;
    PR_CHECKNET_WAGES: TFloatField;
    PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING: TFloatField;
    PR_CHECKOR_CHECK_EIC: TFloatField;
    PR_CHECKOR_CHECK_FEDERAL_TYPE: TStringField;
    PR_CHECKOR_CHECK_FEDERAL_VALUE: TFloatField;
    PR_CHECKOR_CHECK_MEDICARE: TFloatField;
    PR_CHECKOR_CHECK_OASDI: TFloatField;
    PR_CHECKPR_BATCH_NBR: TIntegerField;
    PR_CHECKPR_CHECK_NBR: TIntegerField;
    PR_CHECKPR_NBR: TIntegerField;
    PR_CHECKSALARY: TFloatField;
    PR_CHECKTAX_AT_SUPPLEMENTAL_RATE: TStringField;
    PR_CHECKTAX_FREQUENCY: TStringField;
    PR_CHECK_LINESAMOUNT: TFloatField;
    PR_CHECK_LINESCL_E_DS_NBR: TIntegerField;
    PR_CHECK_LINESCL_PIECES_NBR: TIntegerField;
    PR_CHECK_LINESCO_BRANCH_NBR: TIntegerField;
    PR_CHECK_LINESCO_DEPARTMENT_NBR: TIntegerField;
    PR_CHECK_LINESCO_DIVISION_NBR: TIntegerField;
    PR_CHECK_LINESCO_JOBS_NBR: TIntegerField;
    PR_CHECK_LINESCO_SHIFTS_NBR: TIntegerField;
    PR_CHECK_LINESCO_TEAM_NBR: TIntegerField;
    PR_CHECK_LINESCO_WORKERS_COMP_NBR: TIntegerField;
    PR_CHECK_LINESDEDUCTION_SHORTFALL_CARRYOVER: TFloatField;
    PR_CHECK_LINESEE_SCHEDULED_E_DS_NBR: TIntegerField;
    PR_CHECK_LINESEE_STATES_NBR: TIntegerField;
    PR_CHECK_LINESEE_SUI_STATES_NBR: TIntegerField;
    PR_CHECK_LINESE_D_DIFFERENTIAL_AMOUNT: TFloatField;
    PR_CHECK_LINESHOURS_OR_PIECES: TFloatField;
    PR_CHECK_LINESLINE_ITEM_DATE: TDateTimeField;
    PR_CHECK_LINESLINE_TYPE: TStringField;
    PR_CHECK_LINESPR_CHECK_LINES_NBR: TIntegerField;
    PR_CHECK_LINESPR_CHECK_NBR: TIntegerField;
    PR_CHECK_LINESRATE_NUMBER: TIntegerField;
    PR_CHECK_LINESRATE_OF_PAY: TFloatField;
    PR_CHECK_STATESEE_SDI_OVERRIDE: TFloatField;
    PR_CHECK_STATESEE_SDI_SHORTFALL: TFloatField;
    PR_CHECK_STATESEE_SDI_TAX: TFloatField;
    PR_CHECK_STATESEE_SDI_TAXABLE_WAGES: TFloatField;
    PR_CHECK_STATESEE_STATES_NBR: TIntegerField;
    PR_CHECK_STATESEXCLUDE_ADDITIONAL_STATE: TStringField;
    PR_CHECK_STATESEXCLUDE_SDI: TStringField;
    PR_CHECK_STATESEXCLUDE_STATE: TStringField;
    PR_CHECK_STATESEXCLUDE_SUI: TStringField;
    PR_CHECK_STATESPR_CHECK_NBR: TIntegerField;
    PR_CHECK_STATESPR_CHECK_STATES_NBR: TIntegerField;
    PR_CHECK_STATESSTATE_OVERRIDE_TYPE: TStringField;
    PR_CHECK_STATESSTATE_OVERRIDE_VALUE: TFloatField;
    PR_CHECK_STATESSTATE_SHORTFALL: TFloatField;
    PR_CHECK_STATESSTATE_TAX: TFloatField;
    PR_CHECK_STATESSTATE_TAXABLE_WAGES: TFloatField;
    PR_CHECK_STATESTAX_AT_SUPPLEMENTAL_RATE: TStringField;
    PR_CHECK_SUICO_SUI_NBR: TIntegerField;
    PR_CHECK_SUIOVERRIDE_AMOUNT: TFloatField;
    PR_CHECK_SUIPR_CHECK_NBR: TIntegerField;
    PR_CHECK_SUIPR_CHECK_SUI_NBR: TIntegerField;
    PR_CHECK_SUISUI_GROSS_WAGES: TFloatField;
    PR_CHECK_SUISUI_TAX: TFloatField;
    PR_CHECK_SUISUI_TAXABLE_WAGES: TFloatField;
    PR_CHECK_LOCALSEE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSEXCLUDE_LOCAL: TStringField;
    PR_CHECK_LOCALSLOCAL_SHORTFALL: TFloatField;
    PR_CHECK_LOCALSLOCAL_TAX: TFloatField;
    PR_CHECK_LOCALSLOCAL_TAXABLE_WAGE: TFloatField;
    PR_CHECK_LOCALSOVERRIDE_AMOUNT: TFloatField;
    PR_CHECK_LOCALSPR_CHECK_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSPR_CHECK_NBR: TIntegerField;
    DM_CLIENT: TDM_CLIENT;
    PR_CHECK_LINESE_D_Code_Lookup: TStringField;
    PR_CHECK_LINESState_Lookup: TStringField;
    PR_CHECK_LINESSUI_State_Lookup: TStringField;
    PR_CHECK_STATESState_Lookup: TStringField;
    lcStateState: TevDBLookupCombo;
    DM_COMPANY: TDM_COMPANY;
    PR_CHECK_SUIDescription_Lookup: TStringField;
    PR_CHECK_LOCALSLocal_Name_Lookup: TStringField;
    cbStateORType: TevDBComboBox;
    PR_CHECK_STATESSTATE_MARITAL_STATUS: TStringField;
    PR_CHECK_STATESSTATE_NUMBER_WITHHOLDING_ALLOW: TIntegerField;
    PR_CHECKFEDERAL_MARITAL_STATUS: TStringField;
    PR_CHECKNUMBER_OF_DEPENDENTS: TIntegerField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    lcStateMaritalStatus: TevDBLookupCombo;
    PR: TevClientDataSet;
    PR_BATCH: TevClientDataSet;
    PR_CHECK_LINE_LOCALS: TevClientDataSet;
    PRACTUAL_CALL_IN_DATE: TDateTimeField;
    PRCHECK_DATE: TDateTimeField;
    PRCHECK_DATE_STATUS: TStringField;
    PRCO_NBR: TIntegerField;
    PRMARK_LIABS_PAID_DEFAULT: TStringField;
    PREXCLUDE_ACH: TStringField;
    PREXCLUDE_AGENCY: TStringField;
    PREXCLUDE_BILLING: TStringField;
    PREXCLUDE_R_C_B_0R_N: TStringField;
    PREXCLUDE_TAX_DEPOSITS: TStringField;
    PRFILLER: TStringField;
    PRINVOICE_PRINTED: TStringField;
    PRPAYROLL_COMMENTS: TBlobField;
    PRPROCESS_DATE: TDateTimeField;
    PRPR_NBR: TIntegerField;
    PRRUN_NUMBER: TIntegerField;
    PRSCHEDULED: TStringField;
    PRSCHEDULED_CALL_IN_DATE: TDateTimeField;
    PRSCHEDULED_CHECK_DATE: TDateTimeField;
    PRSCHEDULED_DELIVERY_DATE: TDateTimeField;
    PRSCHEDULED_PROCESS_DATE: TDateTimeField;
    PRSTATUS: TStringField;
    PRSTATUS_DATE: TDateTimeField;
    PRTYPE: TStringField;
    PR_BATCHCO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    PR_BATCHCO_PR_FILTERS_NBR: TIntegerField;
    PR_BATCHFILLER: TStringField;
    PR_BATCHFREQUENCY: TStringField;
    PR_BATCHLOAD_DBDT_DEFAULTS: TStringField;
    PR_BATCHPAY_SALARY: TStringField;
    PR_BATCHPAY_STANDARD_HOURS: TStringField;
    PR_BATCHPERIOD_BEGIN_DATE: TDateTimeField;
    PR_BATCHPERIOD_BEGIN_DATE_STATUS: TStringField;
    PR_BATCHPERIOD_END_DATE: TDateTimeField;
    PR_BATCHPERIOD_END_DATE_STATUS: TStringField;
    PR_BATCHPR_BATCH_NBR: TIntegerField;
    PR_BATCHPR_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSEE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSEXEMPT_EXCLUDE: TStringField;
    PR_CHECK_LINE_LOCALSPR_CHECK_LINES_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSPR_CHECK_LINE_LOCALS_NBR: TIntegerField;
    strPreviewGrid: TevStringGrid;
    evPopupMenu1: TevPopupMenu;
    Print1: TMenuItem;
    PrintDialog1: TPrintDialog;
    dsEE: TevDataSource;
    evDBText12: TevDBText;
    evDBText13: TevDBText;
    PR_CHECKPAYMENT_SERIAL_NUMBER: TIntegerField;
    PR_CHECKFEDERAL_GROSS_WAGES: TFloatField;
    PR_CHECKEE_OASDI_GROSS_WAGES: TFloatField;
    PR_CHECKER_OASDI_GROSS_WAGES: TFloatField;
    PR_CHECKEE_MEDICARE_GROSS_WAGES: TFloatField;
    PR_CHECKER_MEDICARE_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESSTATE_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESEE_SDI_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESER_SDI_GROSS_WAGES: TFloatField;
    PR_CHECK_LOCALSLOCAL_GROSS_WAGES: TFloatField;
    PR_CHECK_LINESAGENCY_STATUS: TStringField;
    PR_CHECK_LINESCL_AGENCY_NBR: TIntegerField;
    tsPayrolls: TTabSheet;
    PRGrid: TevDBGrid;
    evSplitter1: TevSplitter;
    wwDBGrid2: TevDBGrid;
    evPanel2: TevPanel;
    wwdsSubMaster: TevDataSource;
    wwdsSubMaster1: TevDataSource;
    PR_LIST: TevClientDataSet;
    PR_BATCH_LIST: TevClientDataSet;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evBitBtn3: TevBitBtn;
    PR_CHECKCALCULATE_OVERRIDE_TAXES: TStringField;
    PR_CHECKRECIPROCATE_SUI: TStringField;
    PR_CHECK_LINESPR_NBR: TIntegerField;
    PR_CHECK_LINESUSER_OVERRIDE: TStringField;
    PR_CHECKDISABLE_SHORTFALLS: TStringField;
    PR_CHECK_LINESREDUCED_HOURS: TStringField;
    PR_CHECKUPDATE_BALANCE: TStringField;
    PRTAX_IMPOUND: TStringField;
    PRDD_IMPOUND: TStringField;
    PRWC_IMPOUND: TStringField;
    PRTRUST_IMPOUND: TStringField;
    PRBILLING_IMPOUND: TStringField;
    PageControlImages: TevImageList;
    fpStates: TisUIFashionPanel;
    evDBGrid2: TevDBGrid;
    fpSUI: TisUIFashionPanel;
    fpLocals: TisUIFashionPanel;
    fpControls: TisUIFashionPanel;
    fpEmployee: TisUIFashionPanel;
    evDBGrid3: TevDBGrid;
    lcCoSUI: TevDBLookupCombo;
    evDBGrid4: TevDBGrid;
    lcLocal: TevDBLookupCombo;
    cbDisableYTDs: TevCheckBox;
    bbtnCalculate: TevBitBtn;
    bbtnDone: TevBitBtn;
    cbCheckPreview: TevCheckBox;
    cbDisableShortfalls: TevCheckBox;
    bbtnCopyPayroll: TevBitBtn;
    evDBRadioGroup2: TevDBRadioGroup;
    cbPlugTaxes: TevCheckBox;
    evLabel1: TevLabel;
    Label13: TevLabel;
    Label18: TevLabel;
    Label17: TevLabel;
    Label16: TevLabel;
    Label15: TevLabel;
    Label28: TevLabel;
    Label9: TevLabel;
    evLabel11: TevLabel;
    evDBText11: TevDBText;
    lcEmployee: TevDBLookupCombo;
    evDBGrid1: TevDBGrid;
    bbtnRefresh: TevBitBtn;
    DBEdit12: TevDBEdit;
    DBEdit14: TevDBEdit;
    DBEdit15: TevDBEdit;
    DBEdit16: TevDBEdit;
    DBEdit17: TevDBEdit;
    wwDBComboBox1: TevDBComboBox;
    wwDBComboBox3: TevDBComboBox;
    lcEDCode: TevDBLookupCombo;
    lcState: TevDBLookupCombo;
    lcSUIState: TevDBLookupCombo;
    evDBCheckBox1: TevDBCheckBox;
    evDBCheckBox2: TevDBCheckBox;
    evDBCheckBox3: TevDBCheckBox;
    evDBCheckBox4: TevDBCheckBox;
    evDBCheckBox5: TevDBCheckBox;
    evDBCheckBox6: TevDBCheckBox;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBSpinEdit1: TevDBSpinEdit;
    cbNetToGross: TevCheckBox;
    edtNetToGross: TevEdit;
    fpTaxes: TisUIFashionPanel;
    lablGross: TevLabel;
    lablNet: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    evLabel12: TevLabel;
    evLabel13: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evDBText3: TevDBText;
    evDBText4: TevDBText;
    evDBText5: TevDBText;
    evDBText6: TevDBText;
    evDBText7: TevDBText;
    evDBText8: TevDBText;
    evDBText9: TevDBText;
    evDBText10: TevDBText;
    sbPayrolls: TScrollBox;
    pnlPayrollBorder: TevPanel;
    pnlPayrollLeft: TevPanel;
    fpPayrollLeft: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    pnlPayrollRight: TevPanel;
    fpPayrollRight: TisUIFashionPanel;
    pnlFPRightBody: TevPanel;
    fpPayrollControls: TisUIFashionPanel;
    sbCheckPreview: TScrollBox;
    pnlCheckPreviewBorder: TevPanel;
    fpCheckPreview: TisUIFashionPanel;
    pnlFPCheckPreviewBody: TevPanel;
    EvBevel1: TEvBevel;
    PR_CHECK_LINESWORK_ADDRESS_OVR: TStringField;
    PR_CHECK_LINESNONRES_EE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSNONRES_EE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSCO_LOCATIONS_NBR: TIntegerField;
    PR_CHECK_LOCALSREPORTABLE: TStringField;
    procedure bbtnRefreshClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bbtnDoneClick(Sender: TObject);
    procedure bbtnCalculateClick(Sender: TObject);
    procedure dsCHECK_STATESDataChange(Sender: TObject; Field: TField);
    procedure FormDestroy(Sender: TObject);
    procedure PR_CHECK_LINESBeforePost(DataSet: TDataSet);
    procedure PR_CHECK_STATESBeforePost(DataSet: TDataSet);
    procedure PR_CHECK_SUIBeforePost(DataSet: TDataSet);
    procedure PR_CHECK_LOCALSBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Print1Click(Sender: TObject);
    procedure lcEmployeeCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure cbNetToGrossClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbtnCopyPayrollClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn3Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
    procedure PR_CHECK_LINESNewRecord(DataSet: TDataSet);
    procedure PR_CHECK_LINESCL_E_DS_NBRChange(Sender: TField);
    procedure TabSheet1Show(Sender: TObject);
    procedure evDBRadioGroup2Click(Sender: TObject);
    procedure dsCHECK_LINESDataChange(Sender: TObject; Field: TField);
    procedure lcEmployeeChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure fpCheckPreviewResize(Sender: TObject);
    procedure dsEEDataChange(Sender: TObject; Field: TField);
  private
    FNeedRecalc: Boolean;
    procedure PrintResults;
    procedure PopulateEEInfo;
    procedure EmptyDataSets;
    procedure CopyToBatch(Pr, PrBatch: Integer);
    procedure CopyRecord(Source, Dest: TevClientDataSet);
    procedure SetBatchDefaults;
    procedure CalculateCheckNew;
    procedure CheckNet;
  public
    PrNbr, PrBatchNbr: Integer;
    NewCheckAdded: Boolean;
    ModifiedNbr: array of record
      PrNbr: Integer;
      PrBatchNbr: Integer;
    end;
  end;

var
  TaxCalculator: TTaxCalculator;

procedure PopulateScheduledEDs(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, pr_check_line_locals: TevClientDataSet);

implementation

uses SPopulateRecords;

{$R *.DFM}

procedure DeleteERSUI;
var
  V: Variant;
begin
  with DM_COMPANY.CO_SUI do
  begin
    First;
    while not EOF do
    begin
      V := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', FieldByName('SY_SUI_NBR').Value, 'EE_ER_OR_EE_OTHER_OR_ER_OTHER');
      if VarIsNull(V) or Is_ER_SUI(V) then
        Delete
      else
        Next;
    end;
  end;
end;

procedure TTaxCalculator.bbtnRefreshClick(Sender: TObject);
begin
  PR_CHECK_LINES.DisableControls;
  try
    PopulateScheduledEDs(pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, pr_check_line_locals);
  finally
    PR_CHECK_LINES.EnableControls;
  end;
end;

procedure PopulateScheduledEDs(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, pr_check_line_locals: TevClientDataSet);
var
  i: Integer;
begin
  ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
  try
    PR_CHECK_LINES.First;
    while not PR_CHECK_LINES.EOF do
    begin
      if PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED then
      begin
        PR_CHECK_LINES.Delete;
        i := PR_CHECK_LINES.RecNo;
        PR_CHECK_LINES.ReSync([]);
        PR_CHECK_LINES.RecNo := i;
      end
      else
        PR_CHECK_LINES.Next;
    end;

    ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
      PR,
      PR_BATCH,
      PR_CHECK,
      PR_CHECK_LINES,
      PR_CHECK_STATES,
      PR_CHECK_SUI,
      PR_CHECK_LOCALS,
      PR_CHECK_LINE_LOCALS,
      DM_PAYROLL.PR_SCHEDULED_E_DS,
      DM_CLIENT.CL_E_DS,
      DM_CLIENT.CL_PERSON,
      DM_CLIENT.CL_PENSION,
      DM_COMPANY.CO,
      DM_COMPANY.CO_E_D_CODES,
      DM_COMPANY.CO_STATES,
      DM_EMPLOYEE.EE,
      DM_EMPLOYEE.EE_SCHEDULED_E_DS,
      DM_EMPLOYEE.EE_STATES,
      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
      DM_SYSTEM_STATE.SY_STATES,
      False, False, False, True
    );
  finally
    if DM_PAYROLL.PR_CHECK_LINES.Active then
      ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
    else
      ctx_PayrollCalculation.ReleaseTempCheckLines;
  end;
end;

procedure TTaxCalculator.FormCreate(Sender: TObject);
begin
  DM_COMPANY.CO_SUI.SaveState;
  DM_SYSTEM_STATE.SY_STATES.CheckDSCondition('');
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.CheckDSCondition('');
  DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.CheckDSCondition('');
  DM_SYSTEM_STATE.SY_SUI.CheckDSCondition('');
  DM_CLIENT.CL_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_COMPANY.CO_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_LOCAL_TAX.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  if ctx_PayrollCalculation.GetEeFilter <> '' then
     DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and ' + ctx_PayrollCalculation.GetEeFilter, False)
  else
     DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString, False);
  PR_LIST.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and STATUS=''' + PAYROLL_STATUS_PENDING + '''');
  PR_BATCH_LIST.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');

  DM_CLIENT.CL_PERSON.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_CLIENT.CL_PENSION.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_COMPANY.CO_E_D_CODES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_EMPLOYEE.EE_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');

  ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES, DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, DM_SYSTEM_STATE.SY_SUI,
    DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PERSON, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_E_D_CODES,
    DM_COMPANY.CO_STATES, DM_COMPANY.CO_SUI, DM_COMPANY.CO_LOCAL_TAX,
    DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_STATES,
    PR_LIST, PR_BATCH_LIST]);
  if PR_LIST.Locate('PR_NBR', PrNbr, []) then
    PR_BATCH_LIST.Locate('PR_BATCH_NBR', PrBatchNbr, []);
  DeleteERSUI;

  PR.CreateDataSet;
  PR_BATCH.CreateDataSet;
  PR_CHECK.CreateDataSet;
  PR_CHECK_LINES.CreateDataSet;
  PR_CHECK_STATES.CreateDataSet;
  PR_CHECK_SUI.CreateDataSet;
  PR_CHECK_LOCALS.CreateDataSet;
  PR_CHECK_LINE_LOCALS.CreateDataSet;

  PR.Insert;
  PR.FieldByName('TAX_IMPOUND').Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
  PR.FieldByName('TRUST_IMPOUND').Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
  PR.FieldByName('BILLING_IMPOUND').Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
  PR.FieldByName('DD_IMPOUND').Value := DM_COMPANY.CO.DD_IMPOUND.Value;
  PR.FieldByName('WC_IMPOUND').Value := DM_COMPANY.CO.WC_IMPOUND.Value;
  PR.FieldByName('CHECK_DATE').Value := Date;
  PR.FieldByName('RUN_NUMBER').Value := 1;
  PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_REGULAR;
  PR.FieldByName('CO_NBR').Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
  PR.Post;

  PR_BATCH.Insert;
  PR_BATCH.FieldByName('PR_NBR').Value := PR.FieldByName('PR_NBR').Value;
  with ctx_PayrollCalculation do
  begin
    PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);//FREQUENCY_TYPE_WEEKLY;
    PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := GetNextPeriodBeginDate(PR_BATCH.FieldByName('FREQUENCY').AsString[1]);
    PR_BATCH.FieldByName('PERIOD_END_DATE').Value := GetNextPeriodEndDate(PR_BATCH.FieldByName('FREQUENCY').AsString[1], PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value);
  end;
  PR_BATCH.Post;

  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Filtered := True;
  PageControl1.ActivatePage(TabSheet1);
end;

procedure TTaxCalculator.bbtnDoneClick(Sender: TObject);
begin
  Close;
end;

procedure TTaxCalculator.bbtnCalculateClick(Sender: TObject);
begin
  CalculateCheckNew;
end;

procedure TTaxCalculator.CalculateCheckNew;
var
  aPR, aPR_BATCH, aPR_CHECK, aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI, aPR_CHECK_LOCALS: IisStream;
begin

  PR_CHECK.CheckBrowseMode;
  PR_CHECK_LINES.CheckBrowseMode;
  PR_CHECK_LINE_LOCALS.CheckBrowseMode;
  PR_CHECK_STATES.CheckBrowseMode;
  PR_CHECK_SUI.CheckBrowseMode;
  PR_CHECK_LOCALS.CheckBrowseMode;

  if cbNetToGross.Checked then
  begin
    PR_CHECK.Edit;
    try
      PR_CHECK.FieldByName('NET_WAGES').Value := StrToFloat(edtNetToGross.Text);
      PR_CHECK.Post;
    except
      on E: Exception do
      begin
        PR_CHECK.Cancel;
        raise;
      end;
    end;
  end;

  aPR := PR.GetDataStream(True, True);
  aPR_BATCH := PR_BATCH.GetDataStream(True, True);
  aPR_CHECK := PR_CHECK.GetDataStream(True, True);
  aPR_CHECK_LINES := PR_CHECK_LINES.GetDataStream(True, True);
  aPR_CHECK_LINE_LOCALS := PR_CHECK_LINE_LOCALS.GetDataStream(True, True);
  aPR_CHECK_STATES := PR_CHECK_STATES.GetDataStream(True, True);
  aPR_CHECK_SUI := PR_CHECK_SUI.GetDataStream(True, True);
  aPR_CHECK_LOCALS := PR_CHECK_LOCALS.GetDataStream(True, True);

  ctx_PayrollProcessing.TaxCalculator(aPR, aPR_BATCH, aPR_CHECK, aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI, aPR_CHECK_LOCALS,
    cbDisableYTDs.Checked, cbDisableShortfalls.Checked, cbNetToGross.Checked);

  PR_CHECK.Data := aPR_CHECK;
  PR_CHECK_LINES.Data := aPR_CHECK_LINES;
  PR_CHECK_LINE_LOCALS.Data := aPR_CHECK_LINE_LOCALS;
  PR_CHECK_STATES.Data := aPR_CHECK_STATES;
  PR_CHECK_SUI.Data := aPR_CHECK_SUI;
  PR_CHECK_LOCALS.Data := aPR_CHECK_LOCALS;

  PopulateStringGrid(strPreviewGrid, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS);
  if cbCheckPreview.Checked then
    PageControl1.ActivePageIndex := 1;

  DM_EMPLOYEE.EE_LOCALS.Filter := ' LOCAL_ENABLED = ''Y'' ';
  DM_EMPLOYEE.EE_LOCALS.Filtered := True;
end;

procedure TTaxCalculator.dsCHECK_STATESDataChange(Sender: TObject;
  Field: TField);
begin
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Filter := 'SY_STATES_NBR='
    + IntToStr(ConvertNull(DM_SYSTEM_STATE.SY_STATES.Lookup('STATE',
    PR_CHECK_STATES.FieldByName('State_Lookup').Value, 'SY_STATES_NBR'), 0));
end;

procedure TTaxCalculator.FormDestroy(Sender: TObject);
begin
  DM_EMPLOYEE.EE_LOCALS.Filter := '';
  DM_EMPLOYEE.EE_LOCALS.Filtered := False;
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Filtered := False;
  DM_COMPANY.CO_SUI.LoadState;
end;

procedure TTaxCalculator.PR_CHECK_LINESBeforePost(DataSet: TDataSet);
begin
  DataSet.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
  if (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DED_REIMBURSEMENT) and (DataSet.State = dsInsert) then
    DataSet.FieldByName('AMOUNT').Value := -DataSet.FieldByName('AMOUNT').Value;
end;

procedure TTaxCalculator.PR_CHECK_STATESBeforePost(DataSet: TDataSet);
begin
  DataSet.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
end;

procedure TTaxCalculator.PR_CHECK_SUIBeforePost(DataSet: TDataSet);
begin
  DataSet.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
end;

procedure TTaxCalculator.PR_CHECK_LOCALSBeforePost(DataSet: TDataSet);
begin
  DataSet.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
end;

procedure TTaxCalculator.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    bbtnRefresh.Click;
//  if Key = VK_F6 then
//    bbtnCalculateCL.Click;
  if Key = VK_F5 then
    bbtnCalculate.Click;
  if Key = VK_F7 then
    PrintResults;
  if Key = VK_F8 then
    bbtnCopyPayroll.Click;
  if Key = VK_F2 then
    bbtnDone.Click;
end;

procedure TTaxCalculator.Print1Click(Sender: TObject);
begin
  PrintResults;
end;

procedure TTaxCalculator.PrintResults;
var
  I: Integer;
  S: String;
  rtDest: TRect;
begin
  if PrintDialog1.Execute then
  begin
    Printer.BeginDoc;
    Printer.Canvas.Font := strPreviewGrid.Font;
    Printer.Canvas.Font.Style := Printer.Canvas.Font.Style + [fsBold];
    rtDest := Rect(100, 200, 200, 300);
    S := 'Company #' + Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString) + ' ' + DM_COMPANY.CO.FieldByName('NAME').AsString;
    DrawText(Printer.Canvas.Handle, PChar(S), Length(S), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    rtDest := Rect(100, 300, 200, 400);
    S := 'EE #' + DM_EMPLOYEE.EE.FieldByName('Trim_Number').AsString + ' ' + DM_EMPLOYEE.EE.FieldByName('Employee_Name_Calculate').AsString;
    DrawText(Printer.Canvas.Handle, PChar(S), Length(S), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    Printer.Canvas.Font.Style := Printer.Canvas.Font.Style - [fsBold];
    for I := 1 to strPreviewGrid.RowCount - 1 do
    begin
      rtDest := Rect(100, 100 * (I + 5), 200, 100 * (I + 6));
      DrawText(Printer.Canvas.Handle, PChar(strPreviewGrid.Cells[0, I]), Length(strPreviewGrid.Cells[0, I]), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
      rtDest := Rect(2300, 100 * (I + 5), 2400, 100 * (I + 6));
      DrawText(Printer.Canvas.Handle, PChar(strPreviewGrid.Cells[1, I]), Length(strPreviewGrid.Cells[1, I]), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    end;
    Printer.EndDoc;
  end;
end;

procedure TTaxCalculator.lcEmployeeCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  if Modified then
    PopulateEEInfo;
end;

procedure TTaxCalculator.cbNetToGrossClick(Sender: TObject);
begin
  edtNetToGross.Enabled := cbNetToGross.Checked;
  if cbNetToGross.Checked then
    EvMessage('The Net to Gross option does not calculate %-based E/Ds', mtInformation, [mbOK]);
end;

procedure TTaxCalculator.FormShow(Sender: TObject);
begin
  NewCheckAdded := False;
  if DM_EMPLOYEE.EE.RecordCount > 0 then
  begin
    lcEmployee.LookupValue := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value;
    try
      ctx_DataAccess.IsForTaxCalculator := True;
      PopulateEEInfo;
    finally
      ctx_DataAccess.IsForTaxCalculator := False;
    end;
  end;
end;

procedure TTaxCalculator.EmptyDataSets;
begin
  PR_CHECK.DisableControls;
  PR_CHECK_LINES.DisableControls;
  PR_CHECK_STATES.DisableControls;
  PR_CHECK_SUI.DisableControls;
  PR_CHECK_LOCALS.DisableControls;
  try
    PR_CHECK.EmptyDataSet;
    PR_CHECK_LINES.EmptyDataSet;
    PR_CHECK_STATES.EmptyDataSet;
    PR_CHECK_SUI.EmptyDataSet;
    PR_CHECK_LOCALS.EmptyDataSet;
  finally
    PR_CHECK.EnableControls;
    PR_CHECK_LINES.EnableControls;
    PR_CHECK_STATES.EnableControls;
    PR_CHECK_SUI.EnableControls;
    PR_CHECK_LOCALS.EnableControls;
  end;
end;

procedure TTaxCalculator.PopulateEEInfo;
begin
  EmptyDataSets;
  PR_CHECK.DisableControls;
  PR_CHECK_LINES.DisableControls;
  PR_CHECK_STATES.DisableControls;
  PR_CHECK_SUI.DisableControls;
  PR_CHECK_LOCALS.DisableControls;
  try
    PR_CHECK.Insert;
    PR_CHECK.FieldByName('PR_NBR').Value := PR.FieldByName('PR_NBR').Value;
    PR_CHECK.FieldByName('PR_BATCH_NBR').Value := PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
    PR_CHECK.FieldByName('EE_NBR').Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value;
    PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := 1;
    PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
    PR_CHECK.FieldByName('FEDERAL_MARITAL_STATUS').Value := DM_EMPLOYEE.EE.FieldByName('FEDERAL_MARITAL_STATUS').Value;
    PR_CHECK.FieldByName('NUMBER_OF_DEPENDENTS').Value := DM_EMPLOYEE.EE.FieldByName('NUMBER_OF_DEPENDENTS').Value;
    PR_CHECK.FieldByName('TAX_FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;
    PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := 'N';
    PR_CHECK.FieldByName('EXCLUDE_FEDERAL').Value := 'N';
    PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value := 'N';
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_OASDI').Value := 'N';
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').Value := 'N';
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_EIC').Value := 'N';
    PR_CHECK.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
    PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := DM_EMPLOYEE.EE['OVERRIDE_FED_TAX_TYPE'];
    PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := DM_EMPLOYEE.EE['OVERRIDE_FED_TAX_VALUE'];    
    PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value := 'N';
    PR_CHECK.FieldByName('UPDATE_BALANCE').Value := GROUP_BOX_NO;

    PR_CHECK.Post;

    DM_EMPLOYEE.EE_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'EE_NBR=' + DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString);
    DM_EMPLOYEE.EE_LOCALS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'EE_NBR=' + DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString);
    ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS]);

    DM_EMPLOYEE.EE_STATES.First;
    while not DM_EMPLOYEE.EE_STATES.EOF do
    begin
      PR_CHECK_STATES.Insert;
      PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
      PR_CHECK_STATES.FieldByName('STATE_MARITAL_STATUS').Value := DM_EMPLOYEE.EE_STATES['STATE_MARITAL_STATUS'];
      PR_CHECK_STATES.FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').Value := DM_EMPLOYEE.EE_STATES['STATE_NUMBER_WITHHOLDING_ALLOW'];
      PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := DM_EMPLOYEE.EE_STATES['OVERRIDE_STATE_TAX_TYPE'];
      PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := DM_EMPLOYEE.EE_STATES['OVERRIDE_STATE_TAX_VALUE'];
      PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
      PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := 'N';
      PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
      PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
      PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
      PR_CHECK_STATES.Post;

      DM_COMPANY.CO_SUI.Filter := 'CO_STATES_NBR=' + DM_EMPLOYEE.EE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsString;
      DM_COMPANY.CO_SUI.Filtered := True;
      DM_COMPANY.CO_SUI.First;
      while not DM_COMPANY.CO_SUI.EOF do
      begin
        if not PR_CHECK_SUI.Locate('CO_SUI_NBR', DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').Value, []) then
        begin
          PR_CHECK_SUI.Insert;
          PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value := DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').Value;
          PR_CHECK_SUI.Post;
        end;
        DM_COMPANY.CO_SUI.Next;
      end;
      DM_COMPANY.CO_SUI.Filtered := False;

      DM_EMPLOYEE.EE_STATES.Next;
    end;

    DM_EMPLOYEE.EE_LOCALS.Filter := ' LOCAL_ENABLED = ''Y'' ';
    DM_EMPLOYEE.EE_LOCALS.Filtered := True;
    DM_EMPLOYEE.EE_LOCALS.First;
    while not DM_EMPLOYEE.EE_LOCALS.EOF do
    begin
      PR_CHECK_LOCALS.Insert;
      PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
      PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
      PR_CHECK_LOCALS.Post;
      DM_EMPLOYEE.EE_LOCALS.Next;
    end;

    PopulateScheduledEDs(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS);

  finally
    PR_CHECK.EnableControls;
    PR_CHECK_LINES.EnableControls;
    PR_CHECK_STATES.EnableControls;
    PR_CHECK_SUI.EnableControls;
    PR_CHECK_LOCALS.EnableControls;
  end;
end;

procedure TTaxCalculator.bbtnCopyPayrollClick(Sender: TObject);
begin
  if FNeedRecalc then
    CalculateCheckNew;
  PageControl1.ActivatePage(tsPayrolls);
end;

procedure TTaxCalculator.evBitBtn1Click(Sender: TObject);
var
 bTemp : boolean;
begin
  CheckNet;
  if PR_BATCH_LIST.FieldByName('FREQUENCY').AsString <> DM_EMPLOYEE.EE.PAY_FREQUENCY.Value then
    raise EInvalidOperation.Create('This batch has different frequency from employee. You need to create a new batch');
  bTemp := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    try
      CopyToBatch(PR_LIST.FieldByName('PR_NBR').AsInteger, PR_BATCH_LIST.FieldByName('PR_BATCH_NBR').AsInteger);
    except
      ctx_DataAccess.CancelDataSets([PR_LIST, PR_BATCH_LIST, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES,
        DM_PAYROLL.PR_CHECK_STATES, DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_LOCALS, DM_PAYROLL.PR_SCHEDULED_EVENT]);
      raise;
    end;
  finally
    ctx_DataAccess.TempCommitDisable := bTemp;
  end;
end;

procedure TTaxCalculator.CheckNet;
begin
  if PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL then
  begin
    if PR_CHECK.FieldByName('NET_WAGES').AsFloat < -0.009 then
      raise EInconsistentData.CreateHelp('Can not create a negative regular check.', IDH_InconsistentData);
  end;
end;

procedure TTaxCalculator.evBitBtn3Click(Sender: TObject);
var
 bTemp  : boolean;
begin
  CheckNet;
  bTemp := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    CopyRecord(PR_BATCH, PR_BATCH_LIST);
    PR_BATCH_LIST['PR_NBR'] := PR_LIST['PR_NBR'];
    SetBatchDefaults;
    if PR_BATCH_LIST.FieldByName('FREQUENCY').AsString <> DM_EMPLOYEE.EE.PAY_FREQUENCY.Value then
      with ctx_PayrollCalculation do
      begin
        PR_BATCH_LIST.FieldByName('FREQUENCY').Value := DM_EMPLOYEE.EE.PAY_FREQUENCY.Value;
        PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').Value := GetNextPeriodBeginDate(PR_BATCH_LIST.FieldByName('FREQUENCY').AsString[1]);
        PR_BATCH_LIST.FieldByName('PERIOD_END_DATE').Value := GetNextPeriodEndDate(PR_BATCH_LIST.FieldByName('FREQUENCY').AsString[1], PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').Value);
      end;
    PR_BATCH_LIST.Post;
    CopyToBatch(PR_LIST.FieldByName('PR_NBR').AsInteger, PR_BATCH_LIST.FieldByName('PR_BATCH_NBR').AsInteger);
  finally
    ctx_DataAccess.TempCommitDisable := bTemp;
  end;
end;

procedure TTaxCalculator.evBitBtn2Click(Sender: TObject);
var
  i, j: Integer;
  F: TField;
  FCheckValues: TevFieldValues;
  RV: TevFieldValuesRec;
  CheckDate, PeriodBeginDate, PeriodEndDate: String;
  ScheduledCheckDate: TDateTime;
  AskDates, b: Boolean;
  bTemp : boolean;
begin
  CheckNet;
  bTemp := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    CopyRecord(PR, PR_LIST);
    FCheckValues := FieldCodeValues.FindFields(TPR);
    with PR_LIST do
    for i := 0 to Fields.Count - 1 do
    begin
      F := Fields[i];
      j := FCheckValues.FieldIndex(F.FieldName);
      if j <> -1 then
      begin
        RV := FCheckValues[j];
        if Length(RV.DefValue) > 0 then
          F.Value := RV.DefValue
      end;
    end;

    AskDates := True;
    ScheduledCheckDate := PR_LIST.FieldByName('CHECK_DATE').AsDateTime;
    if EvMessage('Would you like to attach to the next scheduled payroll?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      ScheduledCheckDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
      if EvMessage('The next scheduled payroll check date is ' + DateToStr(ScheduledCheckDate) + '. Would you like to use it?', mtConfirmation, [mbYes, mbNo]) = mrYes then
        AskDates := False;
      PR_LIST.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_NORMAL;
      PR_LIST.FieldByName('SCHEDULED').AsString := GROUP_BOX_YES;
      PR_LIST.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_NONE;
    end
    else
    begin
      PR_LIST.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
      PR_LIST.FieldByName('SCHEDULED').AsString := GROUP_BOX_NO;
      PR_LIST.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    end;

    if AskDates then
    begin
      if not EvDialog('Input required', 'Please, enter a Check Date', CheckDate) then
      begin
        PR_LIST.Cancel;
        Exit;
      end;
      if not EvDialog('Input required', 'Please, enter a Period Begin Date', PeriodBeginDate) then
      begin
        PR_LIST.Cancel;
        Exit;
      end;
      if not EvDialog('Input required', 'Please, enter a Period End Date', PeriodEndDate) then
      begin
        PR_LIST.Cancel;
        Exit;
      end;
      PR_LIST.FieldByName('CHECK_DATE').AsString := CheckDate;
    end
    else
      PR_LIST.FieldByName('CHECK_DATE').AsDateTime := ScheduledCheckDate;

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', 'max(RUN_NUMBER)');
      SetMacro('TableName', 'PR');
      SetMacro('NbrField', 'CO');
      SetMacro('Condition', 'CHECK_DATE = :CheckDate');
      SetParam('RecordNbr', DM_COMPANY.CO.CO_NBR.Value);
      SetParam('CheckDate', PR_LIST.FieldByName('CHECK_DATE').Value);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    PR_LIST.FieldByName('RUN_NUMBER').AsInteger := Succ(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger);

    PR_LIST.FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING;
    PR_LIST.FieldByName('STATUS_DATE').AsDateTime := SysTime;
    PR_LIST.FieldByName('EXCLUDE_ACH').AsString := GROUP_BOX_NO;
    PR_LIST.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_NO;
    PR_LIST.FieldByName('EXCLUDE_AGENCY').AsString := GROUP_BOX_NO;
    PR_LIST.FieldByName('MARK_LIABS_PAID_DEFAULT').AsString := GROUP_BOX_NO;
    PR_LIST.FieldByName('EXCLUDE_BILLING').AsString := GROUP_BOX_NO;
    PR_LIST.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_NONE;
    PR_LIST.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;
    PR_LIST.FieldByName('SCHEDULED_CALL_IN_DATE').Value := Date;
    PR_LIST.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := Date;
    PR_LIST.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := Date;

    PR_LIST.FieldByName('TAX_IMPOUND').Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
    PR_LIST.FieldByName('TRUST_IMPOUND').Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
    PR_LIST.FieldByName('BILLING_IMPOUND').Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
    PR_LIST.FieldByName('DD_IMPOUND').Value := DM_COMPANY.CO.DD_IMPOUND.Value;
    PR_LIST.FieldByName('WC_IMPOUND').Value := DM_COMPANY.CO.WC_IMPOUND.Value;


    ctx_PayrollCalculation.AttachPayrollToCalendar(PR_LIST);
    PR_LIST.Post;
    CopyRecord(PR_BATCH, PR_BATCH_LIST);
    PR_BATCH_LIST['PR_NBR'] := PR_LIST['PR_NBR'];
    SetBatchDefaults;
    PR_BATCH_LIST.FieldByName('FREQUENCY').Value := DM_EMPLOYEE.EE.PAY_FREQUENCY.Value;
    if not AskDates then
    with ctx_PayrollCalculation do
    begin
      b := FindNextPeriodBeginEndDateExt(PR_LIST, PR_BATCH_LIST);
      if b then
        PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value
      else
        PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').Value := GetNextPeriodBeginDate(PR_BATCH_LIST.FieldByName('FREQUENCY').AsString[1]);
      if b then
        PR_BATCH_LIST.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_END_DATE').Value
      else
        PR_BATCH_LIST.FieldByName('PERIOD_END_DATE').Value := GetNextPeriodEndDate(PR_BATCH_LIST.FieldByName('FREQUENCY').AsString[1], PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').Value);
    end
    else
    begin
      PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE').AsString := PeriodBeginDate;
      PR_BATCH_LIST.FieldByName('PERIOD_END_DATE').AsString := PeriodEndDate;
    end;
    PR_BATCH_LIST.Post;
    CopyToBatch(PR_LIST.FieldByName('PR_NBR').AsInteger, PR_BATCH_LIST.FieldByName('PR_BATCH_NBR').AsInteger);
  finally
    ctx_DataAccess.TempCommitDisable := bTemp;
  end;
end;

procedure TTaxCalculator.CopyToBatch(Pr, PrBatch: Integer);
var
  i: Integer;
  b: Boolean;
  ap, bp: TDataSetNotifyEvent;
  StrNbr: String;
begin
  if (Pr = 0) or (PrBatch = 0) then
    raise EInconsistentData.Create('Please, choose payroll and batch to attach to');
  if PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL then
    if not EvDialog('Input required', 'Please, enter a Manual Check Number', StrNbr) then
      Exit;

  ctx_DataAccess.UnlockPRSimple;
  ctx_DataAccess.SkipPrCheckPostCheck := True;
  DM_PAYROLL.PR_CHECK.DisableControls;
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  DM_PAYROLL.PR_CHECK_STATES.DisableControls;
  DM_PAYROLL.PR_CHECK_SUI.DisableControls;
  DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
  try
    try
      if not DM_PAYROLL.PR_CHECK.Active then
        DM_PAYROLL.PR_CHECK.DataRequired('PR_BATCH_NBR=' + IntToStr(PrBatch));
      CopyRecord(PR_CHECK, DM_PAYROLL.PR_CHECK);

      if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL then
      begin
        DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString := StrNbr;
        if EvMessage('Do you want the balance of the scheduled EDs for the manual check to be updated?', mtConfirmation, [mbYes,mbNo]) = mrYes then
          DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_YES
        else
          DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_NO;
      end;

      DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := ctx_PayrollCalculation.GetCustomBankAccountNumber(PR_CHECK);
      DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsInteger := Pr;
      DM_PAYROLL.PR_CHECK.FieldByName('PR_BATCH_NBR').AsInteger := PrBatch;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_NONE;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
      DM_PAYROLL.PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
      DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
      DM_PAYROLL.PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0.0;
      DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value := 0.0;
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK.RECIPROCATE_SUI.Value := GROUP_BOX_YES;
      DM_PAYROLL.PR_CHECK.DISABLE_SHORTFALLS.Value := GROUP_BOX_NO;

      if (cbPlugTaxes.Checked) and (evDBRadioGroup2.ItemIndex = 0) then
      begin
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := PR_CHECK.FieldByName('FEDERAL_TAX').Value;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := PR_CHECK.FieldByName('EE_OASDI_TAX').Value;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').Value := PR_CHECK.FieldByName('EE_EIC_TAX').Value;
      end;

      DM_PAYROLL.PR_CHECK['CUSTOM_PR_BANK_ACCT_NUMBER'] := ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
      DM_PAYROLL.PR_CHECK['ABA_NUMBER'] := ctx_PayrollCalculation.GetABANumber;

      DM_PAYROLL.PR_CHECK.Post;

      if not DM_PAYROLL.PR_CHECK_LINES.Active then
        DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_NBR=' + IntToStr(Pr));
      bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
      ap := DM_PAYROLL.PR_CHECK_LINES.AfterPost;
      DM_PAYROLL.PR_CHECK_LINES.BeforePost := nil;
      DM_PAYROLL.PR_CHECK_LINES.AfterPost := nil;
      try
        PR_CHECK_LINES.Last;
        while not PR_CHECK_LINES.Bof do
        begin
          CopyRecord(PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINES);
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_NBR').AsInteger := Pr;
          if (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').IsNull)  then
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_USER;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('USER_OVERRIDE').Value := GROUP_BOX_NO;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('REDUCED_HOURS').Value := GROUP_BOX_NO;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').Value := GROUP_BOX_NO;
          DM_PAYROLL.PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_PENDING;
          DM_PAYROLL.PR_CHECK_LINES.Post;
          PR_CHECK_LINES.Prior;
        end;
      finally
        DM_PAYROLL.PR_CHECK_LINES.BeforePost := bp;
        DM_PAYROLL.PR_CHECK_LINES.AfterPost := ap;
      end;

      if not DM_PAYROLL.PR_CHECK_STATES.Active then
        DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_NBR=' + IntToStr(Pr));
      PR_CHECK_STATES.Last;
      while not PR_CHECK_STATES.Bof do
      begin
        CopyRecord(PR_CHECK_STATES, DM_PAYROLL.PR_CHECK_STATES);
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_NBR').AsInteger := Pr;
        if cbPlugTaxes.Checked then
        begin
          DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
          DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := PR_CHECK_STATES.FieldByName('STATE_TAX').Value;
          DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').Value := PR_CHECK_STATES.FieldByName('STATE_TAX').Value;
          DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value := PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value;
        end;
        DM_PAYROLL.PR_CHECK_STATES.Post;
        PR_CHECK_STATES.Prior;
      end;

      if not DM_PAYROLL.PR_CHECK_SUI.Active then
        DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_NBR=' + IntToStr(Pr));
      PR_CHECK_SUI.Last;
      while not PR_CHECK_SUI.Bof do
      begin
        CopyRecord(PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_SUI);
        DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_NBR').AsInteger := Pr;
        if cbPlugTaxes.Checked then
          DM_PAYROLL.PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_SUI.FieldByName('SUI_TAX').Value;
        DM_PAYROLL.PR_CHECK_SUI.Post;
        PR_CHECK_SUI.Prior;
      end;

      if not DM_PAYROLL.PR_CHECK_LOCALS.Active then
        DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_NBR=' + IntToStr(Pr));
      PR_CHECK_LOCALS.Last;
      while not PR_CHECK_LOCALS.Bof do
      begin
        if (PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsFloat <> 0)
        or (PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').AsFloat <> 0)
        or (PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').AsFloat <> 0) then
        begin
          CopyRecord(PR_CHECK_LOCALS, DM_PAYROLL.PR_CHECK_LOCALS);
          if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('REPORTABLE').AsString = '' then
            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('REPORTABLE').AsString := 'Y';
          DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_NBR').AsInteger := Pr;
          if cbPlugTaxes.Checked then
            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;
          DM_PAYROLL.PR_CHECK_LOCALS.Post;
        end;
        PR_CHECK_LOCALS.Prior;
      end;

      ctx_DataAccess.PostDataSets([PR_LIST, PR_BATCH_LIST, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES,
        DM_PAYROLL.PR_CHECK_STATES, DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_LOCALS, DM_PAYROLL.PR_SCHEDULED_EVENT]);

      if not DM_PAYROLL.PR_CHECK_STATES.Active then
        DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_NBR=' + IntToStr(Pr));

      b := False;
      for i := 0 to High(ModifiedNbr) do
        if (ModifiedNbr[i].PrNbr = PR_LIST.FieldByName('PR_NBR').AsInteger) and
           (ModifiedNbr[i].PrBatchNbr = PR_BATCH_LIST.FieldByName('PR_BATCH_NBR').AsInteger) then
        begin
          b := True;
          Break;
        end;
      if not b then
      begin
        SetLength(ModifiedNbr, Succ(Length(ModifiedNbr)));
        ModifiedNbr[High(ModifiedNbr)].PrNbr := PR_LIST.FieldByName('PR_NBR').AsInteger;
        ModifiedNbr[High(ModifiedNbr)].PrBatchNbr := PR_BATCH_LIST.FieldByName('PR_BATCH_NBR').AsInteger;
      end;
      PageControl1.ActivatePage(TabSheet1);
      NewCheckAdded := True;
    except
      ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK,
                      DM_PAYROLL.PR_CHECK_LINES,
                      DM_PAYROLL.PR_CHECK_STATES,
                      DM_PAYROLL.PR_CHECK_SUI,
                      DM_PAYROLL.PR_CHECK_LOCALS]);
      raise;
    end;
  finally
    ctx_DataAccess.SkipPrCheckPostCheck := False;
    ctx_DataAccess.LockPRSimple;
    DM_PAYROLL.PR_CHECK.EnableControls;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    DM_PAYROLL.PR_CHECK_STATES.EnableControls;
    DM_PAYROLL.PR_CHECK_SUI.EnableControls;
    DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
  end;
end;

procedure TTaxCalculator.CopyRecord(Source, Dest: TevClientDataSet);
var
  i: Integer;
  f: TField;
begin
  Dest.Append;
  for i := 0 to Pred(Source.FieldCount) do
  begin
    f := Dest.FindField(Source.Fields[i].FieldName);
    if Assigned(f) then
      f.Assign(Source.Fields[i]);
  end;
end;

procedure TTaxCalculator.SetBatchDefaults;
begin
  PR_BATCH_LIST.FieldByName('PERIOD_BEGIN_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  PR_BATCH_LIST.FieldByName('PERIOD_END_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  PR_BATCH_LIST.FieldByName('PAY_SALARY').AsString := GROUP_BOX_NO;
  PR_BATCH_LIST.FieldByName('PAY_STANDARD_HOURS').AsString := GROUP_BOX_NO;
  PR_BATCH_LIST.FieldByName('LOAD_DBDT_DEFAULTS').AsString := GROUP_BOX_NO;
  PR_BATCH_LIST.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
end;

procedure TTaxCalculator.PR_CHECK_LINESNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('LINE_TYPE').AsString := CHECK_LINE_TYPE_USER;
end;

procedure TTaxCalculator.PR_CHECK_LINESCL_E_DS_NBRChange(Sender: TField);
begin
  PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Clear;
  PR_CHECK_LINES.FieldByName('LINE_TYPE').Clear;
  ctx_PayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS);
end;

procedure TTaxCalculator.TabSheet1Show(Sender: TObject);
begin
  if PR_CHECK.State = dsInsert then
    evDBRadioGroup2.ItemIndex := 0 ;
end;

procedure TTaxCalculator.evDBRadioGroup2Click(Sender: TObject);
begin
  if evDBRadioGroup2.ItemIndex = 1  then
  with ctx_PayrollCalculation do
    PR_Check.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GetNextPaymentSerialNbr;//NULL;
end;

procedure TTaxCalculator.dsCHECK_LINESDataChange(Sender: TObject;
  Field: TField);
begin
  if Assigned(Field) and (Field.Name = 'PR_CHECK_LINESRATE_OF_PAY') and not Field.IsNull then
    PR_CHECK_LINES.FieldByName('RATE_NUMBER').Value := Null;
end;

procedure TTaxCalculator.lcEmployeeChange(Sender: TObject);
begin
  FNeedRecalc := True;
end;

procedure TTaxCalculator.PageControl1Change(Sender: TObject);
begin
  //size the Fashion Body internal Panels
  pnlFPLeftBody.Width  := fpPayrollLeft.Width - 40;
  pnlFPLeftBody.Height := fpPayrollLeft.Height - 65;

  pnlFPRightBody.Width  := fpPayrollRight.Width - 40;
  pnlFPRightBody.Height := fpPayrollRight.Height - 65;

  //adjust the column widths no the Payrolls tab.
  PRGrid.Columns[0].DisplayWidth := 10;
  PRGrid.Columns[1].DisplayWidth := 5;
  PRGrid.Columns[2].DisplayWidth := 5;
  PRGrid.Columns[3].DisplayWidth := 5;
  PRGrid.Columns[4].DisplayWidth := 5;
end;

procedure TTaxCalculator.fpCheckPreviewResize(Sender: TObject);
begin
  //GUI 2.0 Stuff, Resize the Fashion Panel Body
  pnlFPCheckPreviewBody.Width  := fpCheckPreview.Width - 40;
  pnlFPCheckPreviewBody.Height := fpCheckPreview.Height - 65;

end;

procedure TTaxCalculator.dsEEDataChange(Sender: TObject; Field: TField);
begin
  //GUI 2.0, put the Employee Name in the Fashion Panel Title.
  fpCheckPreview.Title := evDBText12.Caption + ' ' +evDBText13.Caption;

end;


end.
