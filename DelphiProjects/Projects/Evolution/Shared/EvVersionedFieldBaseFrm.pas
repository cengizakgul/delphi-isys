//Copyright iSystems, LLC. 2013
unit EvVersionedFieldBaseFrm;

interface

uses
  Windows, Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  isUIwwDBEdit;


type
  TevVersionedFieldBaseClass = class of TevVersionedFieldBase;

  TevVersionedFieldBase = class(TForm)
    dsFieldData: TevDataSource;
    evPanel2: TevPanel;
    fpEffectivePeriod: TisUIFashionPanel;
    grFieldData: TevDBGrid;
    pnlEdit: TevPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    deBeginDate: TevDBDateTimePicker;
    pnlButtons: TevPanel;
    btnNavInsert: TevSpeedButton;
    btnNavOk: TevSpeedButton;
    btnNavCancel: TevSpeedButton;
    btnNavDelete: TevSpeedButton;
    deEndDate: TevDBDateTimePicker;
    procedure FormShow(Sender: TObject);
    procedure dsFieldDataStateChange(Sender: TObject);
    procedure btnNavCancelClick(Sender: TObject);
    procedure btnNavOkClick(Sender: TObject);
    procedure btnNavInsertClick(Sender: TObject);
    procedure dsFieldDataDataChange(Sender: TObject; Field: TField);
    procedure grFieldDataCalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure btnNavDeleteClick(Sender: TObject);
    procedure UpdateFieldOnChange(Sender: TObject);
    procedure deBeginDateDropDown(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FTable: String;
    FFields: IisParamsCollection;
    FNbr: Integer;
    FData: IevDataSet;
    FReadOnly: Boolean;
    FInDeletion: Boolean;
    FAllDates: IevDataSet;
    FModified: Boolean;
    FDefaultToQuarters: Boolean;
    FForceQuarters: Boolean;
    procedure BuildVersionData(const AParams: TTaskParamList);
    procedure SetBeginDateConstraints;
    procedure ResetBeginDateConstraints;
  protected
    procedure SyncControlsState; virtual;
    procedure RefreshData;
    procedure DoBeforeDataBuild; virtual;
    procedure DoAfterDataBuild; virtual;
    procedure DoOnSave; virtual;
    procedure DoAfterSave; virtual;
    function  ValidInput: Boolean; virtual;
    function  GetFieldSettings: IisListOfValues; virtual;
    procedure DisplayOrOverrideNavInsertMessage; virtual;

    property DefaultToQuarters: Boolean read FDefaultToQuarters write FDefaultToQuarters;
    property ForceQuarters: Boolean read FForceQuarters write FForceQuarters;
    property Table: String read FTable;
    property Fields: IisParamsCollection read FFields;
    property Nbr: Integer read FNbr;
    property Data: IevDataSet read FData;
    property ReadOnly: Boolean read FReadOnly;
  public
    procedure AfterConstruction; override;
    class function ShowField(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer;
                             const AReadOnly, DefaultToQuarters, ForceQuarters: Boolean; const AFieldsCustomOptions: IisParamsCollection = nil): Boolean;
    class function GetEditorClass(const ATable, AField: String; out AFieldsInGroup: TisCommaDilimitedString): TevVersionedFieldBaseClass;
  end;

implementation

uses evVersionedFieldFrm, evVersionedPersonNameFrm, evVersionedDBDTFrm, EvVersionedAddressFrm, evVersionedW2PersonNameFrm,
     evVersionedCoAddressFrm, EvVersionedCoLegalAddressFrm, EvVersionedEELocalFrm, EvVersionedEERateFrm, EvVersionedEELocalOverrideTaxFrm,
     EvVersionedSYFedTaxTableBracketsFrm, TypInfo, EvVersionedCOStatesFrm, EvVersionedVirtualReportNameFrm, EvVersionedCLCOConsFrm, EvVersionedCOTierFrm,
     EvVersionedACALowestCostFrm, EvVersionedEINorSSNFrm, EvVersionedE_D_CODE_TYPEFrm, EvVersionedCL_E_DS_EXEMPT_EXCLUDE, EvVersionedEnableHRFrm;

{$R *.dfm}

type
  TevFieldGroupInfo = record
    Table: String;
    Fields: TisCommaDilimitedString;
    Editor: TevVersionedFieldBaseClass;
  end;

var FieldGroupRegistry: array [0..33] of TevFieldGroupInfo = (
    (Table: 'CO'; Fields: 'ADDRESS1,ADDRESS2,CITY,STATE,ZIP_CODE,NAME,DBA'; Editor: TevVersionedCoAddress),
    (Table: 'CO'; Fields: 'LEGAL_ADDRESS1,LEGAL_ADDRESS2,LEGAL_CITY,LEGAL_STATE,LEGAL_ZIP_CODE,LEGAL_NAME'; Editor: TevVersionedCoLegalAddress),
    (Table: 'CO'; Fields: 'CL_CO_CONSOLIDATION_NBR'; Editor: TevVersionedCLCOCons),
    (Table: 'CO'; Fields: 'ENABLE_HR'; Editor: TEvVersionedEnableHR),
    (Table: 'CO'; Fields: 'SY_ANALYTICS_TIER_NBR'; Editor: TevVersionedCOTier),
    (Table: 'CL_PERSON'; Fields: 'FIRST_NAME,MIDDLE_INITIAL,LAST_NAME'; Editor: TevVersionedPersonName),
    (Table: 'CL_PERSON'; Fields: 'ADDRESS1,ADDRESS2,CITY,STATE,ZIP_CODE'; Editor: TevVersionedAddress),
    (Table: 'CL_PERSON'; Fields: 'W2_FIRST_NAME,W2_MIDDLE_NAME,W2_LAST_NAME,W2_NAME_SUFFIX'; Editor: TevVersionedW2PersonName),
    (Table: 'CL_PERSON'; Fields: 'EIN_OR_SOCIAL_SECURITY_NUMBER,SOCIAL_SECURITY_NUMBER'; Editor: TevVersionedEINorSSN ),
    (Table: 'EE'; Fields: 'CO_DIVISION_NBR,CO_BRANCH_NBR,CO_DEPARTMENT_NBR,CO_TEAM_NBR'; Editor: TevVersionedDBDT),
    (Table: 'EE'; Fields: 'ACA_CO_BENEFIT_SUBTYPE_NBR'; Editor: TEvVersionedACALowestCost),
    (Table: 'EE_LOCALS'; Fields: 'LOCAL_ENABLED,DEDUCT'; Editor: TEvVersionedEELocal),
    (Table: 'EE_RATES'; Fields: 'PRIMARY_RATE,RATE_NUMBER,RATE_AMOUNT'; Editor: TEvVersionedEERate),
    (Table: 'EE_LOCALS'; Fields: 'OVERRIDE_LOCAL_TAX_TYPE,OVERRIDE_LOCAL_TAX_VALUE'; Editor: TEvVersionedEELocalOverrideTax),
    (Table: 'SY_FED_TAX_TABLE_BRACKETS'; Fields: 'GREATER_THAN_VALUE,LESS_THAN_VALUE,PERCENTAGE'; Editor: TEvVersionedSYFedTaxTableBrackets),
    (Table: 'SY_STATE_TAX_CHART'; Fields: 'MINIMUM,MAXIMUM,PERCENTAGE'; Editor: TEvVersionedSYFedTaxTableBrackets),
    (Table: 'SY_LOCAL_TAX_CHART'; Fields: 'MINIMUM,MAXIMUM,PERCENTAGE'; Editor: TEvVersionedSYFedTaxTableBrackets),
    (Table: 'CO_STATES'; Fields: 'TCD_DEPOSIT_FREQUENCY_NBR'; Editor: TEvVersionedCOStates),
    (Table: 'SY_LOCAL_DEPOSIT_FREQ'; Fields: 'SY_REPORTS_GROUP_NBR'; Editor: TevVersionedVirtualReportName),
    (Table: 'SY_STATE_DEPOSIT_FREQ'; Fields: 'SY_REPORTS_GROUP_NBR'; Editor: TevVersionedVirtualReportName ),
    (Table: 'CL_E_DS'; Fields: 'E_D_CODE_TYPE'; Editor: TevVersionedE_D_CODE_TYPE ),
    (Table: 'CL_E_DS'; Fields: 'EE_EXEMPT_EXCLUDE_FEDERAL'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'EE_EXEMPT_EXCLUDE_OASDI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'ER_EXEMPT_EXCLUDE_OASDI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'EE_EXEMPT_EXCLUDE_MEDICARE'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'EE_EXEMPT_EXCLUDE_EIC'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'ER_EXEMPT_EXCLUDE_MEDICARE'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_DS'; Fields: 'ER_EXEMPT_EXCLUDE_FUI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_STATE_EXMPT_EXCLD'; Fields: 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_STATE_EXMPT_EXCLD'; Fields: 'EMPLOYEE_EXEMPT_EXCLUDE_SDI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_STATE_EXMPT_EXCLD'; Fields: 'EMPLOYEE_EXEMPT_EXCLUDE_SUI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_STATE_EXMPT_EXCLD'; Fields: 'EMPLOYER_EXEMPT_EXCLUDE_SDI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_STATE_EXMPT_EXCLD'; Fields: 'EMPLOYER_EXEMPT_EXCLUDE_SUI'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE),
    (Table: 'CL_E_D_LOCAL_EXMPT_EXCLD'; Fields: 'EXEMPT_EXCLUDE'; Editor: TevVersionedCL_E_DS_EXEMPT_EXCLUDE)
   );


{ TevVersionedField }

procedure TevVersionedFieldBase.BuildVersionData(const AParams: TTaskParamList);
const
    AllDatesStruct: array [0..1] of TDSFieldDef = (
      (FieldName: 'begin_date';  DataType: ftDate;  Size: 0;  Required: False),
      (FieldName: 'end_date';  DataType: ftDate;  Size: 0;  Required: False));

var
  bm: TDateTime;
  Flds: String;
  i: Integer;
  sFieldNames : String;
  Fld: TField;
begin
  DoBeforeDataBuild;

  if Assigned(FData) then
    bm := FData['begin_date']
  else
    bm := 0;

  if Fields.Count = 1 then
    sFieldNames := Fields.ParamName(0)
  else
    sFieldNames := '[MULTIPLE FIELDS]';

  Caption := Format('As-Of-Date values of %s.%s (nbr=%d)', [Table, sFieldNames, Nbr]);
  dsFieldData.DataSet := nil;
  grFieldData.Selected.Clear;

  Flds := '';
  for i := 0 to Fields.Count - 1 do
    AddStrValue(Flds, Fields.ParamName(i), ',');

  FData := ctx_DBAccess.GetFieldVersions(Table, Flds, Nbr, GetFieldSettings);
  FData.LogChanges := True;

  DoAfterDataBuild;

  FAllDates := TevDataSet.Create(FData.vclDataSet);
  FAllDates.IndexFieldNames := 'begin_date';

  if grFieldData.Selected.Count = 0 then
  begin
    grFieldData.Selected.Add('begin_date'#9'21'#9'Begin Effective Date'#9'F');
    grFieldData.Selected.Add('end_date'#9'21'#9'End Effective Date'#9'F');
    for i := 0 to Fields.Count - 1 do
      grFieldData.Selected.Add('');

    Flds := '';
    for i := 0 to Fields.Count - 1 do
    begin
      Flds := Fields.ParamName(i) + #9 + IntToStr(Fields[i].Value['Width']) + #9 + Fields[i].Value['Title'] + #9'F';
      if FData.Fields.FindField('$' + Fields.ParamName(i)) <> nil then
        Flds := '$' + Flds;

      grFieldData.Selected[Fields[i].Value['DisplayIndex'] + 2] := Flds;
    end;
  end;

  FData.vclDataSet.PictureMasks.Clear;
  for i := 0 to Fields.Count - 1 do
  begin
    Fld := FData.Fields.FieldByName(Fields.ParamName(i));
    if Fields[i].ValueExists('DisplayFormat') and IsPublishedProp(Fld, 'DisplayFormat') then
      SetStrProp(Fld, 'DisplayFormat', Fields[i].Value['DisplayFormat']);

    if Fields[i].ValueExists('Items') and IsPublishedProp(Fld, 'Items') then
      SetStrProp(Fld, 'DisplayFormat', Fields[i].Value['DisplayFormat']);

    if Fields[i].ValueExists('PictureMask') then
      FData.vclDataSet.PictureMasks.Add(Fld.FieldName + #9 + Fields[i].Value['PictureMask'] + #9'T'#9'F')
  end; 

  dsFieldData.DataSet := FData.vclDataSet;

  if bm <> 0 then
    FData.Locate('begin_date', bm, [])
  else
    FData.Last;

  grFieldData.SetFocus;
  SyncControlsState;
end;

class function TevVersionedFieldBase.ShowField(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer;
  const AReadOnly, DefaultToQuarters, ForceQuarters: Boolean; const AFieldsCustomOptions: IisParamsCollection = nil): Boolean;
var
  Frm: TevVersionedFieldBase;
  Cl: TddTableClass;
  L: IisStringList;
  i, j: Integer;
  LV: IisListOfValues;
begin
  Frm := Self.Create(Application);
  try
    Frm.FTable := ATable;
    Frm.FNbr := ANbr;
    Frm.FReadOnly := AReadOnly;
    Frm.DefaultToQuarters := DefaultToQuarters;
    Frm.ForceQuarters := ForceQuarters;
    Frm.deBeginDate.UseQuartersOnly := ForceQuarters;
    Frm.deEndDate.UseQuartersOnly := ForceQuarters;

    Cl := GetddTableClassByName(Frm.FTable);
    L := TisStringList.Create;
    L.CommaText := AFields;
    for i := 0 to L.Count - 1 do
    begin
      LV := Frm.FFields.AddParams(L[i]);
      LV.AddValue('Required', Cl.GetIsRequiredField(L[i]) or Contains(CUSTOM_DYNAMIC_REQUIRED_FIELDS, ATable +'.'+ AFields + ','));
      LV.AddValue('Reference', Cl.GetParentTable(L[i]) <> nil);
      LV.AddValue('Title', Cl.GetFieldDisplayLabel(L[i]));
      LV.AddValue('Width', 43);
      LV.AddValue('DisplayIndex', i);

      if Assigned(AFieldsCustomOptions) and (i < AFieldsCustomOptions.Count) then
        for j := 0 to AFieldsCustomOptions[i].Count - 1 do
          LV.AddValue(AFieldsCustomOptions[i][j].Name, AFieldsCustomOptions[i][j].Value);
    end;

    Frm.ShowModal;
    Result := Frm.FModified;
  finally
    Frm.Free;
  end;
end;

procedure TevVersionedFieldBase.FormShow(Sender: TObject);
var
  i: Integer;
begin
  if FReadOnly then
    for i := 0 to ControlCount - 1 do
      if Controls[i] <> fpEffectivePeriod then
        Controls[i].Visible := False;

  pnlEdit.Visible := not FReadOnly;
  grFieldData.SecurityRO := FReadOnly;
  RefreshData;
end;

procedure TevVersionedFieldBase.dsFieldDataStateChange(Sender: TObject);
begin
  SyncControlsState;

  if Assigned(dsFieldData.DataSet) and (dsFieldData.DataSet.State = dsEdit) then
    SetBeginDateConstraints
  else
    ResetBeginDateConstraints;

  grFieldData.Invalidate;
end;

procedure TevVersionedFieldBase.btnNavCancelClick(Sender: TObject);
begin
  Data.Cancel;
  ActiveControl := grFieldData;
end;

procedure TevVersionedFieldBase.btnNavOkClick(Sender: TObject);
begin
  ActiveControl:= nil;

  // In case if validation OnExit has failed it's not gonna be nil 
  if ActiveControl = nil then
  begin
    Assert(not ctx_DBAccess.InTransaction);
    ctx_DBAccess.StartTransaction([GetDBTypeByTableName(Table)]);
    try
      DoOnSave;
      ctx_DBAccess.CommitTransaction;
      FModified := True;
      DoAfterSave;      
    except
      on E: Exception do
      begin
        ctx_DBAccess.RollbackTransaction;
        if not(E is EAbort) then
          RefreshData;
        raise;
      end;
    end;
    RefreshData;
    if grFieldData.CanFocus then
      ActiveControl:= grFieldData;
  end;
end;

procedure TevVersionedFieldBase.btnNavInsertClick(Sender: TObject);
var
  d1, d2: TDateTime;
  Year, Month, Day : Word;

begin
  Data.vclDataSet.DisableControls;
  try
    d1 := Data['begin_date'];
    d2 := Data['end_date'];

    if Data.RecNo = Data.RecordCount then
    begin
      if d1 >= SysDate then
        d1 := IncDay(d1, 1)
      else
        d1 := SysDate;

      if DefaultToQuarters then
      begin
        //first test to see if the date is already a quarter begin date
        DecodeDate(d1,Year,Month,Day);
        Day := 1;
        case Month of
          1..3: Month := 1;
          4..6: Month := 4;
          7..9: Month := 7;
          10..12: Month := 10;
        end;
        DisplayOrOverrideNavInsertMessage;

// I believe it should be like this. Someday someone will realize it.  -Aleksey
//        if EncodeDate(Year,Month,Day) > Data['begin_date'] then
          d1 := EncodeDate(Year,Month,Day);
      end;
      Data.Append;
    end
    else
    begin
      if d1 = BeginOfTime then
        d1 := IncYear(d2, -1);
      d1 := d1 + Trunc(d2 - d1) div 2;
      Data.Next;
      Data.Insert;
    end;

    if d1 > d2 then
      d2 := d1;

    Data['begin_date'] := d1;
    Data['end_date'] := d2;
  finally
    Data.vclDataSet.EnableControls;
  end;

  SetBeginDateConstraints;
end;

procedure TevVersionedFieldBase.dsFieldDataDataChange(Sender: TObject; Field: TField);
begin
  if not Assigned(Field) or
     AnsiSameText(Field.FieldName, 'begin_date') or
     (Fields.ParamsByName(Field.FieldName) <> nil) then
    SyncControlsState;
end;

procedure TevVersionedFieldBase.grFieldDataCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if not Assigned(dsFieldData.DataSet) or (dsFieldData.DataSet.State in [dsEdit, dsInsert]) then
  begin
    if Highlight then
      ABrush.Color := clBlue
    else
      AFont.Color := clGrayText;
  end
  else if FInDeletion and Highlight then
    ABrush.Color := clRed;
end;

procedure TevVersionedFieldBase.RefreshData;
begin
  GlobalThreadManager.RunInMainThread(Self, BuildVersionData, EmptyParams, False);
end;

procedure TevVersionedFieldBase.btnNavDeleteClick(Sender: TObject);
var
  BeginDate, EndDate: TDate;
  LV: IisListOfValues;
  i: Integer;
begin
  FInDeletion := True;
  try
    grFieldData.Invalidate;
    if EvMessage('Do you want to delete this effective period?', mtConfirmation, [mbYes, mbNo], mbNo) <> mrYes then
      Exit;

    LV := TisListOfValues.Create;

    Data.vclDataSet.DisableControls;
    try
      EndDate := Data['end_date'];
      Data.Prior;

      for i := 0 to Fields.Count - 1 do
        LV.AddValue(Fields.ParamName(i), Data[Fields.ParamName(i)]);

      BeginDate := Data['begin_date'];
      Data.Next;
      Data.Delete;
    finally
      Data.vclDataSet.EnableControls;
    end;

    try
      ctx_DBAccess.UpdateFieldVersion(Table, Nbr, BeginDate, EndDate, BeginDate, EndDate, LV);
      FModified := True;
    finally
      RefreshData;
    end;
  finally
    FInDeletion := False;
    grFieldData.Invalidate;
  end;
end;

procedure TevVersionedFieldBase.SetBeginDateConstraints;
begin
  if FAllDates.FindKey([deBeginDate.Date]) then
  begin
    if FAllDates.RecNo > 1 then
    begin
      FAllDates.Prior;
      deBeginDate.MinDate := IncDay(FAllDates.FieldByName('begin_date').AsDateTime, 1);
      FAllDates.Next;
    end;

    if FAllDates.RecNo < FAllDates.RecordCount then
    begin
      FAllDates.Next;
      deBeginDate.MaxDate := IncDay(FAllDates.FieldByName('begin_date').AsDateTime, -1);
    end;
  end

  else if FAllDates.FindNearest([deBeginDate.Date]) then
  begin
    if deBeginDate.Date > FAllDates.FieldByName('begin_date').AsDateTime then
      deBeginDate.MinDate := IncDay(FAllDates.FieldByName('begin_date').AsDateTime, 1)
    else
    begin
      FAllDates.Prior;
      deBeginDate.MinDate := IncDay(FAllDates.FieldByName('begin_date').AsDateTime, 1);
      FAllDates.Next;
      deBeginDate.MaxDate := IncDay(FAllDates.FieldByName('begin_date').AsDateTime, -1);
    end;
  end;
end;

procedure TevVersionedFieldBase.ResetBeginDateConstraints;
begin
  deBeginDate.MinDate := BeginOfTime;
  deBeginDate.MaxDate := DayBeforeEndOfTime;  
end;

procedure TevVersionedFieldBase.SyncControlsState;
begin
  if Data.State in [dsEdit, dsInsert] then
  begin
    btnNavInsert.Enabled := False;
    btnNavDelete.Enabled := False;
    btnNavOk.Enabled := ValidInput;
    btnNavCancel.Enabled := True;
    grFieldData.Enabled := False;
  end
  else
  begin
    grFieldData.Enabled := True;
    btnNavInsert.Enabled := True;
    btnNavDelete.Enabled := Data.RecNo > 1;
    btnNavOk.Enabled := False;
    btnNavCancel.Enabled := False;
  end;

  deBeginDate.Enabled := (Data.RecNo > 1) or (Data.State = dsInsert);
end;

procedure TevVersionedFieldBase.DoAfterDataBuild;
begin
end;

function TevVersionedFieldBase.ValidInput: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to Fields.Count - 1 do
  begin
    if Fields[i].Value['Required'] then
      Result := VarToStr(Data[Fields.ParamName(i)]) <> '';
  end;
end;

procedure TevVersionedFieldBase.AfterConstruction;
begin
  inherited;
  FFields := TisParamsCollection.Create;
end;

class function TevVersionedFieldBase.GetEditorClass(const ATable, AField: String; out AFieldsInGroup: TisCommaDilimitedString): TevVersionedFieldBaseClass;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to High(FieldGroupRegistry) do
  begin
    if AnsiSameText(FieldGroupRegistry[i].Table, ATable) then
      if Contains(FieldGroupRegistry[i].Fields + ',', AField + ',') and
         (AField <> 'SOCIAL_SECURITY_NUMBER') then
      begin
        AFieldsInGroup := FieldGroupRegistry[i].Fields;
        Result := FieldGroupRegistry[i].Editor;
        break;
      end;
  end;

  if not Assigned(Result) then
  begin
    Result := TevVersionedField;
    AFieldsInGroup := AField;
  end;
end;

function TevVersionedFieldBase.GetFieldSettings: IisListOfValues;
begin
  Result := nil;
end;

procedure TevVersionedFieldBase.DoBeforeDataBuild;
begin
end;

procedure TevVersionedFieldBase.UpdateFieldOnChange(Sender: TObject);
begin
  if Sender is TevDBEdit then
  begin
    if TevDBEdit(Sender).Picture.PictureMask = '' then
      TevDBEdit(Sender).UpdateRecord
  end

  else if Sender is TevDBDateTimePicker then
  begin
    TevDBDateTimePicker(Sender).UpdateRecord
  end

  else if Sender is TevDBRadioGroup then
  begin
    TevDBRadioGroup(Sender).UpdateRecord;
    if (Data.State in [dsEdit, dsInsert]) and (Data.Fields.FindField('$' + TevDBRadioGroup(Sender).DataField) <> nil) then
      if TevDBRadioGroup(Sender).ItemIndex = -1 then
        Data['$' + TevDBRadioGroup(Sender).DataField] := Null
      else
        Data['$' + TevDBRadioGroup(Sender).DataField] := TevDBRadioGroup(Sender).Items[TevDBRadioGroup(Sender).ItemIndex];
  end

  else if Sender is TevDBComboBox then
  begin
    TevDBComboBox(Sender).UpdateRecord;
    if (Data.State in [dsEdit, dsInsert]) and (Data.Fields.FindField('$' + (Sender as TevDBComboBox).DataField) <> nil) then
      Data['$' + (Sender as TevDBComboBox).DataField] := (Sender as TevDBComboBox).Text;
  end
  else if Sender is TevDBComboDlg then
  begin
    if (TevDBComboDlg(Sender).Picture.PictureMask = '') or
       TevDBComboDlg(Sender).IsValidPictureValue(TevDBComboDlg(Sender).Text) then
         TevDBComboDlg(Sender).UpdateRecord;
  end;

end;

procedure TevVersionedFieldBase.deBeginDateDropDown(Sender: TObject);
begin
  if not (dsFieldData.DataSet.State in [dsEdit, dsInsert]) then
    Data.Edit;
end;

procedure TevVersionedFieldBase.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) and (Shift = []) and ((ActiveControl = nil) or (ActiveControl = grFieldData)) then
    Close;
end;

procedure TevVersionedFieldBase.DisplayOrOverrideNavInsertMessage;
begin

  ShowMessage('Begin Effective Date for this field defaults to the 1st day of the current quarter.'+chr(13)+
                    'Changes should be made as of the beginning of a quarter.');
end;

procedure TevVersionedFieldBase.DoOnSave;
var
  OldBeginDate, OldEndDate: TDateTime;
  LV: IisListOfValues;
  i: Integer;
begin
  case Data.vclDataSet.UpdateStatus of
  usModified:
    begin
      OldBeginDate := Data.Fields.FieldByName('begin_date').OldValue;
      OldEndDate := Data.Fields.FieldByName('end_date').OldValue;
    end;

  else
    begin
      OldBeginDate := 0;
      OldEndDate := 0;
    end;
  end;

  LV := TisListOfValues.Create;
  for i := 0 to Fields.Count - 1 do
    LV.AddValue(Fields.ParamName(i), Data[Fields.ParamName(i)]);

  ctx_DBAccess.UpdateFieldVersion(Table, Nbr, OldBeginDate, OldEndDate, Data['begin_date'], Data['end_date'], LV);
end;

procedure TevVersionedFieldBase.DoAfterSave;
begin
end;

end.

