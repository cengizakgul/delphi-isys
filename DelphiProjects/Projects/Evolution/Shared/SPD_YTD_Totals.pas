// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_YTD_Totals;

interface

uses
   Db, Wwdatsrc,  Controls, Grids, SReportSettings, EvExceptions,
  Wwdbigrd, Wwdbgrid, Classes, StdCtrls, EvUtils, Forms, SysUtils, Spin,
  Buttons, ISBasicClasses, kbmMemTable, ISKbmMemDataSet, EvContext, Variants,
  ISDataAccessComponents, EvDataAccessComponents, SDataStructure, EvConsts,
  EvTypes, EvUIComponents, EvClientDataSet, LMDCustomButton, LMDButton,
  isUILMDButton, ExtCtrls, isUIFashionPanel;

type
  TYTD_Totals = class(TForm)
    EDataSet: TevClientDataSet;
    DDataSet: TevClientDataSet;
    XDataSet: TevClientDataSet;
    EDataSource: TevDataSource;
    DDataSource: TevDataSource;
    XDataSource: TevDataSource;
    grpYTD: TevGroupBox;
    EGrid: TevDBGrid;
    DGrid: TevDBGrid;
    XGrid: TevDBGrid;
    lblEarnings: TevLabel;
    lblDeductions: TevLabel;
    lblTaxes: TevLabel;
    fpYTD: TisUIFashionPanel;
    pnlFPYTDBody: TevPanel;
    sbYTD: TScrollBox;
    fpEarnings: TisUIFashionPanel;
    fpDeductions: TisUIFashionPanel;
    fpTaxes: TisUIFashionPanel;
    fpControls: TisUIFashionPanel;
    lblYear: TevLabel;
    edtYear: TevSpinEdit;
    CloseBtn: TevBitBtn;
    btnYear: TevBitBtn;
    bPrintCheck: TevBitBtn;
    bPreviewCheck: TevBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnYearClick(Sender: TObject);
    procedure EDataSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DDataSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure XDataSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure bPrintCheckClick(Sender: TObject);
    procedure bPreviewCheckClick(Sender: TObject);
  private
    CurClNbr: integer;
    CurCoNbr: integer;
    CurEeNbr: integer;
    CYear: Word;
  public
    ClNbr: integer;
    CoNbr: integer;
    EeNbr: integer;

    procedure PrintResult(aPrint: boolean);
  end;

function CreateCoYTD: TYTD_Totals;
procedure FreeCoYTD;

implementation

{$R *.DFM}

var
  YTD_Totals: TYTD_Totals;
  CurYear: Word;

function CreateCoYTD: TYTD_Totals;
var
  Month, Day: Word;
begin
  if not Assigned(YTD_Totals) then
  begin
    YTD_Totals := TYTD_Totals.Create(nil);
    DecodeDate(Date, CurYear, Month, Day);
  end;
  Result := YTD_Totals;
end;

procedure FreeCoYTD;
begin
  YTD_Totals.Free;
end;

procedure TYTD_Totals.FormShow(Sender: TObject);
var
  v: OleVariant;
begin
  ctx_StartWait('Preparing...Please wait...');
  try
    edtYear.Value := CurYear;
    if (ClNbr <> CurClNbr) or
       (CoNbr <> CurCoNbr) or
       (CYear <> CurYear) or
       (EeNbr <> CurEeNbr) then
    begin
      v := ctx_PayrollProcessing{(NotQueued)}.LoadYTD(ClNbr, CoNbr, CurYear, EeNbr);
      EDataSet.Data := v[0];
      DDataSet.Data := v[0];
      XDataSet.Data := v[1];
      CurClNbr := ClNbr;
      CurCoNbr := CoNbr;
      CurEeNbr := EeNbr;      
      CYear := CurYear;
    end;

    EDataSet.Filtered := False;
    EDataSet.Filtered := True;
    DDataSet.Filtered := False;
    DDataSet.Filtered := True;
    XDataSet.Filtered := False;
    XDataSet.Filtered := True;

    with EDataSet.Aggregates.Add do
    begin
      AggregateName := 'ASum';
      Expression := 'Sum(Amount)';
      Active := True;
    end;
    with EDataSet.Aggregates.Add do
    begin
      AggregateName := 'HSum';
      Expression := 'sum(hours_or_pieces)';
      Active := True;
    end;

    with DDataSet.Aggregates.Add do
    begin
      AggregateName := 'ASum';
      Expression := 'Sum(Amount)';
      Active := True;
    end;
    with DDataSet.Aggregates.Add do
    begin
      AggregateName := 'HSum';
      Expression := 'sum(hours_or_pieces)';
      Active := True;
    end;

    with XDataSet.Aggregates.Add do
    begin
      AggregateName := 'ASum';
      Expression := 'Sum(Amount)';
      Active := True;
    end;

    EDataSet.AggregatesActive := True;
    DDataSet.AggregatesActive := True;
    XDataSet.AggregatesActive := True;

    EGrid.ColumnByName('Hours_or_Pieces').FooterValue := FormatFloat('#,##0.##', ConvertNull(EDataSet.Aggregates.Find('HSum').Value, 0));
    EGrid.ColumnByName('Amount').FooterValue := FormatFloat('#,##0.00', ConvertNull(EDataSet.Aggregates.Find('ASum').Value, 0));

    DGrid.ColumnByName('Hours_or_Pieces').FooterValue := FormatFloat('#,##0.##', ConvertNull(DDataSet.Aggregates.Find('HSum').Value, 0));
    DGrid.ColumnByName('Amount').FooterValue := FormatFloat('#,##0.00', ConvertNull(DDataSet.Aggregates.Find('ASum').Value, 0));

    XGrid.ColumnByName('Amount').FooterValue := FormatFloat('#,##0.00', ConvertNull(XDataSet.Aggregates.Find('ASum').Value, 0));
    //GUI 2.0
    XGrid.ColumnByName('Amount').DisplayWidth := 12;
    XGrid.Columns[0].DisplayWidth := 20;
  finally
    ctx_EndWait;
  end;
end;

procedure TYTD_Totals.FormCreate(Sender: TObject);
begin
  ClNbr := 0;
  CoNbr := 0;
  EeNbr := 0;
  CurClNbr := 0;
  CurCoNbr := 0;
  CurEeNbr := 0;  
  CYear := 0;
end;

procedure TYTD_Totals.FormDestroy(Sender: TObject);
begin
  YTD_Totals := nil;
end;

procedure TYTD_Totals.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  EDataSet.AggregatesActive := False;
  DDataSet.AggregatesActive := False;
  XDataSet.AggregatesActive := False;

  EDataSet.Aggregates.Clear;
  DDataSet.Aggregates.Clear;
  XDataSet.Aggregates.Clear;

  EDataSet.Filtered := False;
  DDataSet.Filtered := False;
  XDataSet.Filtered := False;
end;

procedure TYTD_Totals.btnYearClick(Sender: TObject);
var
  Action: TCloseAction;
  ds1, ds2, ds3: TDataSource;
begin
  CurYear := edtYear.Value;
  ds1 := EGrid.DataSource;
  ds2 := DGrid.DataSource;
  ds3 := XGrid.DataSource;
  try
    EGrid.DataSource := nil;
    DGrid.DataSource := nil;
    XGrid.DataSource := nil;
    FormClose(Self, Action);
    FormShow(Self);
  finally
    EGrid.DataSource := ds1;
    DGrid.DataSource := ds2;
    XGrid.DataSource := ds3;
  end;
end;

procedure TYTD_Totals.EDataSetFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('e_d_code_type').AsString, 1, 1)[1] in ['E', 'M']) and (DataSet.FieldByName('ee_nbr').AsInteger = EeNbr);
end;

procedure TYTD_Totals.DDataSetFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('e_d_code_type').AsString, 1, 1) = 'D') and (DataSet.FieldByName('ee_nbr').AsInteger = EeNbr);
end;

procedure TYTD_Totals.XDataSetFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := DataSet.FieldByName('ee_nbr').AsInteger = EeNbr;
end;

procedure TYTD_Totals.PrintResult(aPrint: boolean);
var
  ReportParams: TrwReportParams;
  ReportResult: TrwReportResults;
begin
  ReportParams := TrwReportParams.Create;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''Employee YTD Amounts''');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Employee YTD Amounts', [loCaseInsensitive]) then
      raise EMissingReport.CreateHelp('"Employee YTD Amounts" report does not exist in the database!', IDH_MissingReport);

    ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('Companies', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger]));
    ReportParams.Add('Dat', EncodeDate(edtYear.Value, 12, 31));
    ReportParams.Add('EeNbr', DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger);

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
        CH_DATABASE_SYSTEM, ReportParams);
    finally
      ReportResult := TrwReportResults.Create(ctx_RWLocalEngine.EndGroup(rdtNone));
      if aPrint then
        ctx_RWLocalEngine.Print(ReportResult)
      else
        ctx_RWLocalEngine.Preview(ReportResult);
    end;
  finally
    ReportParams.Free;
  end;
end;

procedure TYTD_Totals.bPrintCheckClick(Sender: TObject);
begin
  PrintResult(True);
end;

procedure TYTD_Totals.bPreviewCheckClick(Sender: TObject);
begin
  PrintResult(False);
end;

initialization
  RegisterClass(TYTD_TOTALS);

end.
