// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SafeChoiceFromListQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_ChoiceFromListQueryEx, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls,  ActnList, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvUIComponents, EvClientDataSet, LMDCustomButton,
  LMDButton, isUILMDButton, ComCtrls;

type
  TSafeChoiceFromListQuery = class(TChoiceFromListQueryEx)
    cdsChoiceList: TevClientDataSet;
    pnlEffectiveDate: TevPanel;
    evLabel1: TevLabel;
    dtBeginEffective: TevDateTimePicker;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

end.
 