inherited evVersionedW2PersonName: TevVersionedW2PersonName
  Left = 721
  Top = 332
  ClientWidth = 799
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 799
    Height = 286
    inherited grFieldData: TevDBGrid
      Width = 751
      Height = 159
    end
    inherited pnlEdit: TevPanel
      Top = 203
      Width = 751
      inherited pnlButtons: TevPanel
        Left = 442
      end
      inherited deBeginDate: TevDBDateTimePicker
        UseQuartersOnly = False
      end
    end
  end
  inherited pnlBottom: TevPanel
    Top = 286
    Width = 799
    Height = 218
    inherited isUIFashionPanel1: TisUIFashionPanel
      Height = 218
      Caption = ''
      Title = 'W2 Name'
      inherited lMiddleName: TevLabel
        Left = 20
        Top = 74
        Width = 62
        Caption = 'Middle Name'
      end
      inherited lLastName: TevLabel
        Top = 113
      end
      object lNameSuffix: TevLabel [3]
        Left = 20
        Top = 152
        Width = 57
        Height = 13
        Caption = 'Name Suffix'
        FocusControl = edNameSuffix
      end
      inherited edFirstName: TevDBEdit
        Width = 270
      end
      inherited edMiddleName: TevDBEdit
        Left = 20
        Top = 89
        Width = 270
      end
      inherited edLastName: TevDBEdit
        Top = 128
      end
      object edNameSuffix: TevDBEdit
        Left = 20
        Top = 167
        Width = 270
        Height = 21
        DataSource = dsFieldData
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
