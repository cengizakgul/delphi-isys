unit sPickupSheetsXMLData;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLPickupDataType = interface;
  IXMLPrinterQueueType = interface;
  IXMLPrinterType = interface;
  IXMLVMRBoxesType = interface;
  IXMLBOXType = interface;

{ IXMLPickupDataType }

  IXMLPickupDataType = interface(IXMLNode)
    ['{AE2E166D-D48C-42BD-B380-D5DE39A6B2AA}']
    { Property Accessors }
    function Get_PrinterQueue: IXMLPrinterQueueType;
    function Get_VMRBoxes: IXMLVMRBoxesType;
    { Methods & Properties }
    property PrinterQueue: IXMLPrinterQueueType read Get_PrinterQueue;
    property VMRBoxes: IXMLVMRBoxesType read Get_VMRBoxes;
  end;

{ IXMLPrinterQueueType }

  IXMLPrinterQueueType = interface(IXMLNodeCollection)
    ['{30817C73-8720-4FBE-968E-4DA2031F84D7}']
    { Property Accessors }
    function Get_Printer(Index: Integer): IXMLPrinterType;
    { Methods & Properties }
    function Add: IXMLPrinterType;
    function Insert(const Index: Integer): IXMLPrinterType;
    property Printer[Index: Integer]: IXMLPrinterType read Get_Printer; default;
  end;

{ IXMLPrinterType }

  IXMLPrinterType = interface(IXMLNode)
    ['{801008FF-EDF1-4436-9379-8BB91F25323F}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_LastScanned: WideString;
    function Get_Data: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_LastScanned(Value: WideString);
    procedure Set_Data(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
    property LastScanned: WideString read Get_LastScanned write Set_LastScanned;
    property Data: WideString read Get_Data write Set_Data;
  end;

{ IXMLVMRBoxesType }

  IXMLVMRBoxesType = interface(IXMLNodeCollection)
    ['{E0D83EBE-AD26-4539-B859-893AC8719AB0}']
    { Property Accessors }
    function Get_BOX(Index: Integer): IXMLBOXType;
    { Methods & Properties }
    function Add: IXMLBOXType;
    function Insert(const Index: Integer): IXMLBOXType;
    property BOX[Index: Integer]: IXMLBOXType read Get_BOX; default;
    function FindInBoxes(const BarCode:string):boolean;
  end;

{ IXMLBOXType }

  IXMLBOXType = interface(IXMLNode)
    ['{49876E55-03AA-4D97-A8D2-E52AE30BAC06}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_Data: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Data(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
    property Data: WideString read Get_Data write Set_Data;
  end;

{ Forward Decls }

  TXMLPickupDataType = class;
  TXMLPrinterQueueType = class;
  TXMLPrinterType = class;
  TXMLVMRBoxesType = class;
  TXMLBOXType = class;

{ TXMLPickupDataType }

  TXMLPickupDataType = class(TXMLNode, IXMLPickupDataType)
  protected
    { IXMLPickupDataType }
    function Get_PrinterQueue: IXMLPrinterQueueType;
    function Get_VMRBoxes: IXMLVMRBoxesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPrinterQueueType }

  TXMLPrinterQueueType = class(TXMLNodeCollection, IXMLPrinterQueueType)
  protected
    { IXMLPrinterQueueType }
    function Get_Printer(Index: Integer): IXMLPrinterType;
    function Add: IXMLPrinterType;
    function Insert(const Index: Integer): IXMLPrinterType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPrinterType }

  TXMLPrinterType = class(TXMLNode, IXMLPrinterType)
  protected
    { IXMLPrinterType }
    function Get_Name: WideString;
    function Get_LastScanned: WideString;
    function Get_Data: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_LastScanned(Value: WideString);
    procedure Set_Data(Value: WideString);
  end;

{ TXMLVMRBoxesType }

  TXMLVMRBoxesType = class(TXMLNodeCollection, IXMLVMRBoxesType)
  protected
    { IXMLVMRBoxesType }
    function Get_BOX(Index: Integer): IXMLBOXType;
    function Add: IXMLBOXType;
    function Insert(const Index: Integer): IXMLBOXType;
    function FindInBoxes(const BarCode:string):boolean;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBOXType }

  TXMLBOXType = class(TXMLNode, IXMLBOXType)
  protected
    { IXMLBOXType }
    function Get_Name: WideString;
    function Get_Data: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Data(Value: WideString);
  end;

{ Global Functions }

function GetPickupData(Doc: IXMLDocument): IXMLPickupDataType;
function LoadPickupData(const FileName: WideString): IXMLPickupDataType;
function NewPickupData: IXMLPickupDataType;

const
  TargetNamespace = '';

implementation

uses Classes,SysUtils;

{ Global Functions }

function GetPickupData(Doc: IXMLDocument): IXMLPickupDataType;
begin
  Result := Doc.GetDocBinding('PickupData', TXMLPickupDataType, TargetNamespace) as IXMLPickupDataType;
end;

function LoadPickupData(const FileName: WideString): IXMLPickupDataType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('PickupData', TXMLPickupDataType, TargetNamespace) as IXMLPickupDataType;
end;

function NewPickupData: IXMLPickupDataType;
begin
  Result := NewXMLDocument.GetDocBinding('PickupData', TXMLPickupDataType, TargetNamespace) as IXMLPickupDataType;
end;

{ TXMLPickupDataType }

procedure TXMLPickupDataType.AfterConstruction;
begin
  RegisterChildNode('PrinterQueue', TXMLPrinterQueueType);
  RegisterChildNode('VMRBoxes', TXMLVMRBoxesType);
  inherited;
end;

function TXMLPickupDataType.Get_PrinterQueue: IXMLPrinterQueueType;
begin
  Result := ChildNodes['PrinterQueue'] as IXMLPrinterQueueType;
end;

function TXMLPickupDataType.Get_VMRBoxes: IXMLVMRBoxesType;
begin
  Result := ChildNodes['VMRBoxes'] as IXMLVMRBoxesType;
end;

{ TXMLPrinterQueueType }

procedure TXMLPrinterQueueType.AfterConstruction;
begin
  RegisterChildNode('Printer', TXMLPrinterType);
  ItemTag := 'Printer';
  ItemInterface := IXMLPrinterType;
  inherited;
end;

function TXMLPrinterQueueType.Get_Printer(Index: Integer): IXMLPrinterType;
begin
  Result := List[Index] as IXMLPrinterType;
end;

function TXMLPrinterQueueType.Add: IXMLPrinterType;
begin
  Result := AddItem(-1) as IXMLPrinterType;
end;

function TXMLPrinterQueueType.Insert(const Index: Integer): IXMLPrinterType;
begin
  Result := AddItem(Index) as IXMLPrinterType;
end;

{ TXMLPrinterType }

function TXMLPrinterType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLPrinterType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLPrinterType.Get_LastScanned: WideString;
begin
  Result := AttributeNodes['LastScanned'].Text;
end;

procedure TXMLPrinterType.Set_LastScanned(Value: WideString);
begin
  SetAttribute('LastScanned', Value);
end;

function TXMLPrinterType.Get_Data: WideString;
begin
  Result := AttributeNodes['Data'].Text;
end;

procedure TXMLPrinterType.Set_Data(Value: WideString);
begin
  SetAttribute('Data', Value);
end;

{ TXMLVMRBoxesType }

procedure TXMLVMRBoxesType.AfterConstruction;
begin
  RegisterChildNode('BOX', TXMLBOXType);
  ItemTag := 'BOX';
  ItemInterface := IXMLBOXType;
  inherited;
end;

function TXMLVMRBoxesType.Get_BOX(Index: Integer): IXMLBOXType;
begin
  Result := List[Index] as IXMLBOXType;
end;

function TXMLVMRBoxesType.Add: IXMLBOXType;
begin
  Result := AddItem(-1) as IXMLBOXType;
end;

function TXMLVMRBoxesType.Insert(const Index: Integer): IXMLBOXType;
begin
  Result := AddItem(Index) as IXMLBOXType;
end;

function TXMLVMRBoxesType.FindInBoxes(const BarCode: string): boolean;
var
 i:integer;
begin
   result := false;

   for i:= 0 to Count -1 do
   begin
     if Pos(';'+Trim(BarCode)+ ';', ';'+Get_BOX(i).Data+ ';') <> 0 then
     begin
       result := true;
       break;
     end;
   end;
end;

{ TXMLBOXType }

function TXMLBOXType.Get_Name: WideString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLBOXType.Set_Name(Value: WideString);
begin
  SetAttribute('Name', Value);
end;

function TXMLBOXType.Get_Data: WideString;
begin
  Result := AttributeNodes['Data'].Text;
end;

procedure TXMLBOXType.Set_Data(Value: WideString);
begin
  SetAttribute('Data', Value);
end;

end. 