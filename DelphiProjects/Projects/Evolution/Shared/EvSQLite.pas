unit EvSQLite;

interface

uses isBaseClasses, isDataSet, isBasicUtils, EvCommonInterfaces, EvStreamUtils, EvContext,
     DISQLite3Database, DISQLite3Api, DISQLite3DataSet, DB, SysUtils;

type
  TSQLite = class(TInterfacedObject, ISQLite)
  private
    FSQLite: TDISQLite3Database;
  protected
    procedure Execute(const SQL: String);
    function Select(const SQL: String; const Params: array of Variant): IevDataset;
    procedure Insert(const TableName: String; const Dataset: TDataset; const CheckDate: TDateTime = 0; const Condition: String = ''; Order: Boolean = False);
    procedure InsertRecord(const TableName: String; const Dataset: TDataset);
    function Value(const SQL: String; const Params: array of Variant): Variant;
    procedure CreateIndex(const Table, Index, Fields: String);
    procedure StartTransaction;
    procedure Commit;
    procedure Backup(const FileName: String);
    procedure Restore(const FileName: String);
  public
    constructor Create(const DatabaseName: String);
    destructor Destroy; override;
  end;


implementation

uses EvDataSet;

{ TSQLite }

function backupDb(const DbSource: sqlite3_ptr; const DbDest: sqlite3_ptr): Integer;
var
  pBackup: sqlite3_backup_ptr;
begin
  { Open the sqlite3_backup object used to accomplish the transfer }
  pBackup := sqlite3_backup_init(DbDest, 'main', DbSource, 'main');
  if Assigned(pBackup) then
    try
      repeat
        Result := sqlite3_backup_step(pBackup, 50);
        if not (Result in [SQLITE_OK, SQLITE_BUSY, SQLITE_LOCKED]) then
          Break;
//        sqlite3_sleep(50);
      until False;
    finally
      { Release resources allocated by backup_init(). }
      Result := sqlite3_backup_finish(pBackup);
    end
  else
    Result := SQLITE_ERROR;
end;

procedure TSQLite.Backup(const FileName: String);
var
  FileDB: TDISQLite3Database;
begin
  Set8087CW($133F); {$IFDEF CPUX64}SetMXCSR($1F80); {$ENDIF CPUX64}
  FSQLite.Commit;
  FileDB := TDISQLite3Database.Create(nil);
  try
    FileDB.DatabaseName := FileName;
    FileDB.CreateDatabase;
    backupDb(FSQLite.Handle, FileDB.Handle);
  finally
    FileDB.Free;
  end;
end;

procedure TSQLite.Commit;
begin
  FSQLite.Commit;
end;

procedure TSQLite.CreateIndex(const Table, Index, Fields: String);
begin
  FSQLite.Execute('create index if not exists '+Table+' on '+Table+'('+Fields+')');
end;

procedure TSQLite.Restore(const FileName: String);
var
  FileDB: TDISQLite3Database;
begin
  if Assigned(FSQLite) then
    FSQLite.Free;
  FSQLite := TDISQLite3Database.Create(nil);
  FSQLite.DatabaseName := ':memory:';
  FSQLite.CreateDatabase;
  if FileName <> ':memory:' then
  begin
    FileDB := TDISQLite3Database.Create(nil);
    try
      FileDB.DatabaseName := FileName;
      FileDB.Open;
      backupDb(FileDB.Handle, FSQLite.Handle);
    finally
      FileDB.Free;
    end;
  end;
end;

constructor TSQLite.Create(const DatabaseName: String);
begin
  inherited Create;
  Set8087CW($133F); {$IFDEF CPUX64}SetMXCSR($1F80); {$ENDIF CPUX64}  
  FSQLite := nil;
  Restore(DatabaseName);
end;

destructor TSQLite.Destroy;
begin
  FSQLite.Free;
  inherited;
end;

procedure TSQLite.Execute(const SQL: String);
begin
  FSQLite.Execute(SQL);
end;

procedure TSQLite.InsertRecord(const TableName: String; const Dataset: TDataset);
var
  CREATE_SQL, INSERT_SQL: String;
  Q: TDISQLite3Statement;
  i: Integer;
begin
  CREATE_SQL := 'CREATE TABLE IF NOT EXISTS ' + TableName + ' (';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    case Dataset.Fields[i].DataType of
    ftInteger, ftSmallint, ftWord: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' INTEGER';
    ftDateTime, ftDate, ftTime: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' DATETIME';
    ftFloat, ftCurrency: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' REAL';
    ftString: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName+ ' CHAR('+IntToStr(Dataset.Fields[i].DataSize)+')';
    end;
    if i < (Dataset.Fields.Count - 1) then
      CREATE_SQL := CREATE_SQL + ','
    else
      CREATE_SQL := CREATE_SQL + ')';
  end;
  INSERT_SQL := 'INSERT INTO ' + TableName + ' (';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    INSERT_SQL := INSERT_SQL + Dataset.Fields[i].FieldName;
    if i < (Dataset.Fields.Count - 1) then
      INSERT_SQL := INSERT_SQL + ','
    else
      INSERT_SQL := INSERT_SQL + ')';
  end;
  INSERT_SQL := INSERT_SQL + 'SELECT ';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    INSERT_SQL := INSERT_SQL + ':' + Dataset.Fields[i].FieldName;
    if i < (Dataset.Fields.Count - 1) then
      INSERT_SQL := INSERT_SQL + ','
    else
      INSERT_SQL := INSERT_SQL + ' ';
  end;

  FSQLite.Execute(CREATE_SQL);

  Q := FSQLite.Prepare(INSERT_SQL);
  StartTransaction;
  try
    for i := 0 to Dataset.FieldCount - 1 do
    begin
      if Dataset.Fields[i].DataType = ftInteger then
        Q.Bind_Int_By_Name(':'+Dataset.Fields[i].FieldName, Dataset.Fields[i].AsInteger)
      else
        Q.Bind_Variant_By_Name(':'+Dataset.Fields[i].FieldName, Dataset.Fields[i].Value);
    end;
    Q.StepAndReset;
  finally
    Q.Free;
    Commit;
  end;
end;

procedure TSQLite.Insert(const TableName: String; const Dataset: TDataset;
  const CheckDate: TDateTime; const Condition: String; Order: Boolean);
var
  CREATE_SQL, INSERT_SQL: String;
  Q: TDISQLite3Statement;
  i, n: Integer;
begin
  CREATE_SQL := 'CREATE TABLE IF NOT EXISTS ' + TableName + ' (';
  if CheckDate <> 0 then
    CREATE_SQL := CREATE_SQL + 'CHECK_DATE DATETIME,';
  if Order then
    CREATE_SQL := CREATE_SQL + 'NBR,';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    case Dataset.Fields[i].DataType of
    ftInteger, ftSmallint, ftWord: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' INTEGER';
    ftDateTime, ftDate, ftTime: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' DATETIME';
    ftFloat, ftCurrency: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName + ' REAL';
    ftString: CREATE_SQL := CREATE_SQL + Dataset.Fields[i].FieldName+ ' CHAR('+IntToStr(Dataset.Fields[i].DataSize)+')';
    end;
    if i < (Dataset.Fields.Count - 1) then
      CREATE_SQL := CREATE_SQL + ','
    else
      CREATE_SQL := CREATE_SQL + ')';
  end;
  INSERT_SQL := 'INSERT INTO ' + TableName + ' (';
  if CheckDate <> 0 then
    INSERT_SQL := INSERT_SQL + 'CHECK_DATE,';
  if Order then
    INSERT_SQL := INSERT_SQL + 'NBR,';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    INSERT_SQL := INSERT_SQL + Dataset.Fields[i].FieldName;
    if i < (Dataset.Fields.Count - 1) then
      INSERT_SQL := INSERT_SQL + ','
    else
      INSERT_SQL := INSERT_SQL + ')';
  end;
  INSERT_SQL := INSERT_SQL + 'SELECT ';
  if CheckDate <> 0 then
    INSERT_SQL := INSERT_SQL + ':CHECK_DATE,';
  if Order then
    INSERT_SQL := INSERT_SQL + ':NBR,';
  for i := 0 to Dataset.Fields.Count - 1 do
  begin
    INSERT_SQL := INSERT_SQL + ':' + Dataset.Fields[i].FieldName;
    if i < (Dataset.Fields.Count - 1) then
      INSERT_SQL := INSERT_SQL + ','
    else
      INSERT_SQL := INSERT_SQL + ' ';
  end;
  INSERT_SQL := INSERT_SQL + ' '+Condition;

  FSQLite.Execute(CREATE_SQL);

  Q := FSQLite.Prepare(INSERT_SQL);
  StartTransaction;
  try
    n := 0;
    Dataset.First;
    while not Dataset.Eof do
    begin
      inc(n);
      for i := 0 to Dataset.FieldCount - 1 do
      begin
        if Dataset.Fields[i].DataType = ftInteger then
          Q.Bind_Int_By_Name(':'+Dataset.Fields[i].FieldName, Dataset.Fields[i].AsInteger)
        else
          Q.Bind_Variant_By_Name(':'+Dataset.Fields[i].FieldName, Dataset.Fields[i].Value);
      end;
      if CheckDate <> 0 then
        Q.Bind_Variant_By_Name(':CHECK_DATE', CheckDate);

      if Order then
        Q.Bind_Int_By_Name(':NBR', n);

      Q.StepAndReset;
      Dataset.Next;
    end;
  finally
    Q.Free;
    Commit;
  end;
end;

function TSQLite.Select(const SQL: String;
  const Params: array of Variant): IevDataset;
var
  Q: TDISQLite3UniDirQuery;
  i: Integer;
  Value: String;
begin
  Q := TDISQLite3UniDirQuery.Create(nil);
  Q.Database := FSQLite;
  try
    Q.SelectSQL := SQL;
    for i := 0 to High(Params) do
      Q.Params[i].Value := Params[i];
    Q.Active := True;
    Result := TevDataset.Create;
    for i := 0 to Q.Fields.Count - 1 do
    begin
      if (Q.Fields[i].DataType = ftLargeInt) then
        Result.VCLDataset.FieldDefs.Add(Q.Fields[i].FieldName, ftInteger)
      else
      if (Q.Fields[i].DataType = ftString) or (Q.Fields[i].DataType = ftWideString) then
        Result.VCLDataset.FieldDefs.Add(Q.Fields[i].FieldName, ftString, 512)
      else
        Result.VCLDataset.FieldDefs.Add(Q.Fields[i].FieldName, Q.Fields[i].DataType);
    end;
    Result.VCLDataset.Open;
    while not Q.Eof do
    begin
      Result.Append;
      for i := 0 to Q.Fields.Count - 1 do
      begin
        if (Q.Fields[i].DataType = ftLargeInt) then
          Result.Fields[i].AsInteger := Q.Fields[i].AsInteger
        else
        if (Q.Fields[i].DataType = ftWideString) then
        begin
          Value := Q.Fields[i].AsString;
          Result.Fields[i].AsString := Value;
        end
        else
          Result.Fields[i].Value := Q.Fields[i].Value;
      end;
      Result.Post;
      Q.Next;
    end;
    Result.First;
  finally
    Q.Free;
  end;
end;

procedure TSQLite.StartTransaction;
begin
  FSQLite.StartTransaction;
end;

function TSQLite.Value(const SQL: String;
  const Params: array of Variant): Variant;
var
  Q: TDISQLite3UniDirQuery;
  i: Integer;
begin
  Q := TDISQLite3UniDirQuery.Create(nil);
  Q.Database := FSQLite;
  try
    Q.SelectSQL := SQL;
    for i := 0 to High(Params) do
      Q.Params[i].Value := Params[i];
    Q.Active := True;
    Result := Q.Fields[0].Value;
  finally
    Q.Free;
  end;
end;

end.
