// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EFTPS_Payment;

interface

uses
  Classes, SysUtils, Dialogs, Controls,  Forms, EvTypes, EvComponentsExt, EvBasicUtils,
  EvContext, EvExceptions;

type
  TEFTPS_Payment = class(TComponent, IEFTPS_Payment)
  private
    PaymentList: TStringList;
    FBackUp: TStringList;
    FMySaveDialog: TSaveDialog;
    FFileDateTime: string;
    FPaymentCount : Integer;
    function GetCount: Integer;
    function GetPaymentCount: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddPayment(SBBankAccountNbr: Integer; CompanyFEIN, CompanyPIN, TaxName: string; DueDate, CheckDate: TDateTime; Amount: Real): string;
    procedure FillStringList(const ResultList: TStringList);
    procedure Backup;
    procedure Restore;
    property Count: Integer read GetCount;
    property PaymentCount: Integer read GetPaymentCount;
    property FileDateTime: string read FFileDateTime write FFileDateTime;
  end;

function GetEFTPSTaxCode(TaxName: string): string;
function GetEFTPSBatchTaxCode(TaxName: string): string;

implementation

uses
  EvUtils, EvConsts, SDataStructure;

function GetBatchFilerID(SBBankAccountNbr: Integer): string;
begin
  Result := ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', SBBankAccountNbr, 'BATCH_FILER_ID'), '');
  if Result = '' then
    raise EInconsistentData.CreateHelp('Batch Filer ID has to be setup on the Service Bureau Bank Account level.', IDH_InconsistentData);
  Result := PadStringLeft(Result, ' ', 9);
end;

function GetEFTPSTaxCode(TaxName: string): string;
begin
  Result := '';
  if Pos('1042', TaxName) <> 0 then
    Result := '10425'
  else
    if Pos('1120', TaxName) <> 0 then
    Result := '11206'
  else
    if Pos('720', TaxName) <> 0 then
    Result := '72005'
  else
    if Pos('940', TaxName) <> 0 then
    Result := '09405'
  else
    if Pos('941', TaxName) <> 0 then
    Result := '94105'
  else
    if Pos('943', TaxName) <> 0 then
    Result := '09435'
  else
    if Pos('944', TaxName) <> 0 then
    Result := '94405'
  else
    if Pos('945', TaxName) <> 0 then
    Result := '09455'
  else
    if Pos('990C', TaxName) <> 0 then
    Result := '99026'
  else
    if Pos('990PF', TaxName) <> 0 then
    Result := '99036'
  else
    if Pos('990T', TaxName) <> 0 then
    Result := '99046'
  else
    if Pos('CT-1', TaxName) <> 0 then
    Result := '10005';
end;

function GetEFTPSBatchTaxCode(TaxName: string): string;
begin
  if Pos('940', TaxName) <> 0 then
    Result := '0'//'09405'
  else
    if Pos('941', TaxName) <> 0 then
    Result := '1'//'94105'
  else
    if Pos('943', TaxName) <> 0 then
    Result := '3'//'09435'
  else
    if Pos('944', TaxName) <> 0 then
    Result := '4'//'09445'
  else
    if Pos('945', TaxName) <> 0 then
    Result := '5';//'09455'
end;

{ TEFTPS_Payment }

function TEFTPS_Payment.AddPayment(SBBankAccountNbr: Integer; CompanyFEIN, CompanyPIN, TaxName: string;
  DueDate, CheckDate: TDateTime; Amount: Real): string;
  function GetFileDateTime: string;
  var
    sFileDate, sSeqNumber: string;
  begin
    sFileDate := Copy(FileDateTime, 1, 8);
    sSeqNumber := Copy(FileDateTime, 9, 3);
    sSeqNumber := PadLeft(IntToStr(StrToInt(sSeqNumber)+ Count div 750), '0', 3);
    Result := sFileDate+ sSeqNumber;
  end;
  function CompressAmount(aString: String): String;
  var
    AmountAsInt: Int64;
  begin
    AmountAsInt := StrToInt64(aString);
    Result := IntToHex(AmountAsInt, 9);
  end;
var
  TaxCode: string;
  QtrEndDate, RealDueDate: TDateTime;
  TmpStr: String;
begin
  Result := '';

  if Amount = 0 then
    Exit;

  TaxCode := GetEFTPSTaxCode(TaxName);
  if TaxCode = '' then
    Exit;

  QtrEndDate := GetEndQuarter(CheckDate);
  RealDueDate := DueDate;
  if RealDueDate <= Date then
    RealDueDate := Date + 1;

  while ctx_PayrollCalculation.IsNonBusinessDay(RealDueDate) do
      RealDueDate := RealDueDate + 1;

  TmpStr := GetFileDateTime + PadStringLeft(IntToStr(GetPaymentCount),'0', 4);

  if DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString[1] <> GROUP_BOX_BATCH_PROVIDER then
    Result := TmpStr
  else
    Result := Copy(FileDateTime, 3, 6) + FormatDateTime('YYMM', QtrEndDate) + GetEFTPSBatchTaxCode(TaxCode) + CompressAmount(StringReplace(FloatToStrF(Amount, ffFixed, 11, 2), '.', '', []));
  //Above need to include 2 digit year and compress amount using HEX.
  case DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString[1] of
  GROUP_BOX_CHICAGO:
    PaymentList.Add(GetBatchFilerID(SBBankAccountNbr) + GetMasterInquiryPIN(SBBankAccountNbr) + TmpStr+ 'P'
      + PadStringRight(CompanyFEIN, ' ', 9) + PadStringRight(CompanyPIN, ' ', 4) + 'B' + TaxCode + FormatDateTime('YYYYMM', QtrEndDate)
      + FormatDateTime('YYYYMMDD', RealDueDate) + PadStringLeft(FloatToStrF(Amount, ffFixed, 15, 2), ' ', 15));
  GROUP_BOX_NATIONS, GROUP_BOX_BATCH_PROVIDER:
    PaymentList.Add(PadStringRight(GetBatchFilerID(SBBankAccountNbr) + GetMasterInquiryPIN(SBBankAccountNbr) + TmpStr+ 'P'
      + PadStringRight(CompanyFEIN, ' ', 9) + PadStringRight(CompanyPIN, ' ', 4) + 'B' + TaxCode + FormatDateTime('YYYYMM', QtrEndDate)
      + FormatDateTime('YYYYMMDD', RealDueDate) + PadStringLeft(StringReplace(FloatToStrF(Amount, ffFixed, 15, 2), '.', '', []), '0', 15), ' ', 167));

  end;
end;

procedure TEFTPS_Payment.Backup;
begin
  FBackUp.Assign(PaymentList);
end;

constructor TEFTPS_Payment.Create;
begin
  inherited;
  FMySaveDialog := nil;
  FBackUp := TStringList.Create;
  PaymentList := TStringList.Create;
  FFileDateTime := FormatDateTime('yyyymmddhhmm', Now);
  Delete(FFileDateTime, 9, 1);
  FPaymentCount := 0;
  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');
end;

destructor TEFTPS_Payment.Destroy;
begin
  FBackUp.Free;
  PaymentList.Free;
  FMySaveDialog.Free;
  inherited;
end;

procedure TEFTPS_Payment.FillStringList(const ResultList: TStringList);
begin
  ResultList.Assign(PaymentList);
end;

function TEFTPS_Payment.GetCount: Integer;
begin
  Result := PaymentList.Count;
end;

function TEFTPS_Payment.GetPaymentCount: Integer;
begin
  inc(FPaymentCount);
  if FPaymentCount > 9999 then
     FPaymentCount := 1;
  Result := FPaymentCount;
end;

procedure TEFTPS_Payment.Restore;
begin
  PaymentList.Assign(FBackUp);
end;

end.
