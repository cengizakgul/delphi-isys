// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PPrQueueList;

interface

uses
  Windows, EvTaskParamFrame, Classes, Controls, Grids, Wwdbigrd, Wwdbgrid, SysUtils,
   EvUtils, SDataStructure, EvConsts, Db, Wwdatsrc, EvTypes,
  StdCtrls, Spin, ExtCtrls, ISBasicClasses, SDDClasses, SDataDicttemp,
  EvCommonInterfaces, EvExceptions, EvUIComponents;

type
  TpfPrQueueList = class(TTaskParamFrame)
    DM_TEMPORARY: TDM_TEMPORARY;
    dsTMP_PR: TevDataSource;
    pGrid: TevPanel;
    gList: TevDBGrid;
    pSelect: TevPanel;
    rbList: TevRadioButton;
    rbAll: TevRadioButton;
    pAge: TevPanel;
    pAge1: TevPanel;
    lAge: TevLabel;
    lHours: TevLabel;
    lMinutes: TevLabel;
    seHours: TevSpinEdit;
    seMinutes: TevSpinEdit;
    lFor: TevLabel;
    rbAllCompanies: TevRadioButton;
    rbSelectedCompanies: TevRadioButton;
    gSelectCompanies: TevDBGrid;
    dsTMP_CO: TevDataSource;
    procedure rbListClick(Sender: TObject);
    procedure rbAllCompaniesClick(Sender: TObject);
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
  end;

implementation

{$R *.DFM}

{ TpfCompanyList }

procedure TpfPrQueueList.ApplyUpdates;
var
  r: Integer;
  s: string;
  sc: string;
begin
  inherited;
  (FTask as IevProcessPayrollTask).AllPayrolls := rbAll.Checked;
  if rbAll.Checked then
  begin
    (FTask as IevProcessPayrollTask).PayrollAge := EncodeTime(seHours.Value, seMinutes.Value, 0, 0);
    (FTask as IevProcessPayrollTask).Caption := 'All payrolls, being completed more than ' +
                     IntToStr(seHours.Value) + ' hour(s) and ' + IntToStr(seMinutes.Value) + ' minute(s)';

    s := '';
    sc := '';
    if rbSelectedCompanies.Checked then
    begin
      r := DM_TEMPORARY.TMP_CO.RecNo;
      DM_TEMPORARY.TMP_CO.DisableControls;
      DM_TEMPORARY.TMP_CO.LookupsEnabled := False;
      try
        DM_TEMPORARY.TMP_CO.First;
        while not DM_TEMPORARY.TMP_CO.Eof do
        begin
          if DM_TEMPORARY.TMP_CO['SELECTED'] = 'Y' then
          begin
            s := s + DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsString + ';' +
                     DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString + ' ';
            sc := sc + Trim(DM_TEMPORARY.TMP_CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString) + ',';
          end;
          DM_TEMPORARY.TMP_CO.Next;
        end;
        sc := Copy(sc, 1, Pred(Length(sc)));
      finally
        DM_TEMPORARY.TMP_CO.RecNo := r;
        DM_TEMPORARY.TMP_CO.LookupsEnabled := True;
        DM_TEMPORARY.TMP_CO.EnableControls;
      end;
    end;
    (FTask as IevProcessPayrollTask).CoList := s;

    if sc <> '' then
      (FTask as IevProcessPayrollTask).Caption := (FTask as IevProcessPayrollTask).Caption + ' for ' + sc + ' companies'
    else
      (FTask as IevProcessPayrollTask).Caption := (FTask as IevProcessPayrollTask).Caption + ' for all companies';
  end

  else
  begin
    r := DM_TEMPORARY.TMP_PR.RecNo;
    DM_TEMPORARY.TMP_PR.DisableControls;
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    try
      s := '';
      sc := '';
      DM_TEMPORARY.TMP_PR.First;
      while not DM_TEMPORARY.TMP_PR.Eof do
      begin
        if DM_TEMPORARY.TMP_PR['SELECTED'] = 'Y' then
        begin
          s := s + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' +
                   DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').AsString + ' ';
          sc := sc + 'Co:' + Trim(DM_TEMPORARY.TMP_PR.FieldbyName('COMPANY_NUMBER_Lookup').AsString) +
                    ' Pr:' + DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').AsString + '-' + DM_TEMPORARY.TMP_PR.FieldByName('RUN_NUMBER').AsString + '  ';
        end;
        DM_TEMPORARY.TMP_PR.Next;
      end;
      if s = '' then
        raise EInvalidParameters.CreateHelp('No payrolls selected', IDH_InvalidParameters);
      (FTask as IevProcessPayrollTask).PrList := s;
      (FTask as IevProcessPayrollTask).Caption := sc;
    finally
      DM_TEMPORARY.TMP_PR.RecNo := r;
      DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
      DM_TEMPORARY.TMP_PR.EnableControls;
    end;
  end;
end;

constructor TpfPrQueueList.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  Hours, Min, Dummy: Word;
begin
  inherited;
  DM_TEMPORARY.TMP_CO.Activate;
  DM_TEMPORARY.TMP_PR.DataRequired('STATUS=''' + PAYROLL_STATUS_COMPLETED + '''');

  if (FTask as IevProcessPayrollTask).AllPayrolls then
    rbAll.Checked := True
  else
    rbList.Checked := True;

  if rbAll.Checked then
  begin
    DecodeTime((FTask as IevProcessPayrollTask).PayrollAge, Hours, Min, Dummy, Dummy);
    seHours.Value := Hours;
    seMinutes.Value := Min;

    if (FTask as IevProcessPayrollTask).CoList <> '' then
    begin
      rbSelectedCompanies.Checked := True;
      DM_TEMPORARY.TMP_CO.DisableControls;
      DM_TEMPORARY.TMP_CO.LookupsEnabled := False;
      try
        DM_TEMPORARY.TMP_CO.Last;
        while not DM_TEMPORARY.TMP_CO.Bof do
        begin
          if Pos(' ' + DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsString + ';' +
                       DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString + ' ',
                 ' ' + (FTask as IevProcessPayrollTask).CoList + ' ') > 0 then
          begin
            DM_TEMPORARY.TMP_CO.Edit;
            DM_TEMPORARY.TMP_CO['SELECTED'] := 'Y';
            DM_TEMPORARY.TMP_CO.Post;
          end
          else if DM_TEMPORARY.TMP_CO['SELECTED'] = 'Y' then
          begin
            DM_TEMPORARY.TMP_CO.Edit;
            DM_TEMPORARY.TMP_CO['SELECTED'] := 'N';
            DM_TEMPORARY.TMP_CO.Post;
          end;
          DM_TEMPORARY.TMP_CO.Prior;
        end;
      finally
        DM_TEMPORARY.TMP_CO.LookupsEnabled := True;
        DM_TEMPORARY.TMP_CO.EnableControls;
      end;
    end
    else
      rbAllCompanies.Checked := True;
  end
  else
  begin
    DM_TEMPORARY.TMP_PR.DisableControls;
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    try
      DM_TEMPORARY.TMP_PR.Last;
      while not DM_TEMPORARY.TMP_PR.Bof do
      begin
        if Pos(' ' + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' +
                     DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').AsString + ' ',
               ' ' + (FTask as IevProcessPayrollTask).PrList + ' ') > 0 then
        begin
          DM_TEMPORARY.TMP_PR.Edit;
          DM_TEMPORARY.TMP_PR['SELECTED'] := 'Y';
          DM_TEMPORARY.TMP_PR.Post;
        end
        else if DM_TEMPORARY.TMP_PR['SELECTED'] = 'Y' then
        begin
          DM_TEMPORARY.TMP_PR.Edit;
          DM_TEMPORARY.TMP_PR['SELECTED'] := 'N';
          DM_TEMPORARY.TMP_PR.Post;
        end;
        DM_TEMPORARY.TMP_PR.Prior;
      end;
    finally
      DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
      DM_TEMPORARY.TMP_PR.EnableControls;
    end;
  end;
end;

procedure TpfPrQueueList.rbListClick(Sender: TObject);
begin
  inherited;
  pAge.Visible := rbAll.Checked;
  pGrid.Visible := not pAge.Visible;
end;

procedure TpfPrQueueList.rbAllCompaniesClick(Sender: TObject);
begin
  gSelectCompanies.Visible := rbSelectedCompanies.Checked;
end;

end.
