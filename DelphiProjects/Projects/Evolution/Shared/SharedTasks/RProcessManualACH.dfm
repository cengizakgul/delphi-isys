inherited rfProcessManualACH: TrfProcessManualACH
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      inline rwPreviewContainer1: TrwPreviewContainer
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        Align = alClient
        AutoScroll = False
        TabOrder = 0
      end
      object PageControl: TevPageControl
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        ActivePage = tsAchFile
        Align = alClient
        MultiLine = True
        TabOrder = 1
        TabPosition = tpRight
        object tsAchFile: TTabSheet
          Caption = 'Ach File'
          ImageIndex = 1
          inline ACHFilePreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 381
            Height = 232
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsAchReport: TTabSheet
          Caption = 'Ach Report'
          ImageIndex = 4
          inline ACHRegularReportPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 408
            Height = 243
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsAchDetailedReport: TTabSheet
          Caption = 'Detailed Report'
          ImageIndex = 2
          inline ACHDetailedReportPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 408
            Height = 243
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsSave: TTabSheet
          Caption = 'Save File'
          ImageIndex = 3
        end
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Height = 210
      end
      inherited pWarnings: TevPanel
        Top = 210
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Height = 251
      end
    end
  end
end
