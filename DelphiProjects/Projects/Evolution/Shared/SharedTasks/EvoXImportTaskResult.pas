// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvoXImportTaskResult;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils,
  Buttons, ExtCtrls, Menus, EvTypes, ISBasicClasses, EvContext, EvCommonInterfaces,
  Grids, Wwdbigrd, Wwdbgrid, DB, EvDataSet, EvStatisticsViewerFrm, EvExchangeResultLog, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TrfEvoXImportTask = class(TTaskResultFrame)
    evPanel2: TevPanel;
    ResultsListGrid: TevDBGrid;
    ErrorsLogDataSource: TDataSource;
    ResultsDataSource: TDataSource;
    evSplitter1: TevSplitter;
    evPanel3: TevPanel;
    ResultDetailsGrid: TevDBGrid;
    procedure ResultDetailsGridRowChanged(Sender: TObject);
    procedure ResultDetailsGridCalcCellColors(Sender: TObject;
      Field: TField; State: TGridDrawState; Highlight: Boolean;
      AFont: TFont; ABrush: TBrush);
  private
    FBrokenData: boolean;
    FdsErrorsLog: IevDataSet;
    FdsResults: IevDataSet;
    FTask: IevEvoXImportTask;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.DFM}

uses
  EvStreamUtils, IsBaseClasses, EvExchangeConsts, Variants, isLogFile;

const
  ErrorMessage_length = 1000;
  FileName_length = 500;
  ResultMessage_length = 300;

{ TEvoXImportTaskResult }

constructor TrfEvoXImportTask.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  i, iErrorsCount: integer;
  InputDataSets, ResultsList: IisListOfValues;
  iAllRows, iImportedRows: integer;
begin
  inherited;

  FTask := Task as IevEvoXImportTask;

  FdsErrorsLog := TevDataSet.Create;
  FdsErrorsLog.vclDataSet.FieldDefs.Add('Error#', ftInteger, 0, false);
  FdsErrorsLog.vclDataSet.FieldDefs.Add('Error message', ftString, ErrorMessage_length, false);
  FdsErrorsLog.vclDataSet.FieldDefs.Add('ErrorType', ftInteger, 0, false);
  ErrorsLogDataSource.DataSet := FdsErrorsLog.vclDataSet;

  FdsResults := TevDataSet.Create;
  FdsResults.vclDataSet.FieldDefs.Add('File Name', ftString, FileName_length, false);
  FdsResults.vclDataSet.FieldDefs.Add('Result Message', ftString, ResultMessage_length, false);
  ResultsDataSource.DataSet := FdsResults.vclDataSet;
  FdsResults.vclDataSet.Active := true;
  FdsErrorsLog.vclDataSet.Active := true;

  InputDataSets := FTask.InputDatasets;
  ResultsList := FTask.Results;
  FdsResults.vclDataSet.DisableControls;
  try
    if (ResultsList.Count > 0) and (InputDataSets.Count <> ResultsList.Count) then
    begin
      FBrokenData := true;
      FdsResults.Append;
      FdsResults['File Name'] := 'Error reading task results!';
      FdsResults['Result Message'] := Copy('Amount of input files is different than amount of result: ' +
        IntToStr(InputDataSets.Count) + '/' + IntToStr(ResultsList.Count), 1, ResultMessage_length);
      FdsResults.Post;
    end
    else if ResultsList.Count = 0 then
    begin
      FBrokenData := true;
    end
    else
    begin
      for i := 0 to InputDataSets.Count - 1 do
      begin
        FdsResults.Append;

        FdsResults['File Name'] := InputDataSets[i].Name;
        iErrorsCount := (IInterface(ResultsList[i].Value) as IisListOfValues).Value[EvoXAllMessagesAmount];
        if iErrorsCount = 0 then
        begin
          iAllRows := (IInterface(ResultsList[i].Value) as IisListOfValues).Value[EvoXInputRowsAmount];
          iImportedRows := (IInterface(ResultsList[i].Value) as IisListOfValues).Value[EvoXImportedRowsAmount];
          FdsResults['Result Message'] := 'Import finished successfully. Rows processed: ' + IntToStr(iAllRows) +
            '. Imported rows: ' + IntToStr(iImportedRows);
        end
        else
          FdsResults['Result Message'] := 'Import finished with errors. Found: ' + intToStr(iErrorsCount) + 'errors.';

        FdsResults.Post;
      end;  // for;
    end;
  finally
    FdsResults.vclDataSet.EnableControls;
  end;
  FdsResults.First;
end;

destructor TrfEvoXImportTask.Destroy;
begin
  FdsErrorsLog.vclDataSet.Active := false;
  FdsResults.vclDataSet.Active := false;
  ErrorsLogDataSource.DataSet := nil;
  ResultsDataSource.DataSet := nil;

  inherited;
end;

procedure TrfEvoXImportTask.ResultDetailsGridRowChanged(Sender: TObject);
var
  ResultData: IisListOfValues;
  OldErrorsList: IisStringList;
  ExtErrorsList: IEvExchangeResultLog;
  i: integer;
begin
  inherited;
  if not FBrokenData then
    try
      FdsErrorsLog.vclDataSet.DisableControls;
      FdsErrorsLog.Clear;

      if (not FdsResults.eof) and (FdsResults['File Name'] <> null) then
      begin
        if FTask.Results.ValueExists(FdsResults['File Name']) then
        begin
          ResultData := IInterface(FTask.Results.Value[FdsResults['File Name']]) as IisListOfValues;
          if ResultData.Value[EvoXAllMessagesAmount] > 0 then
          begin
            if ResultData.ValueExists(EvoXStringErrorsList) then
            begin
              OldErrorsList :=  IInterface(ResultData.Value[EvoXStringErrorsList]) as IisStringList;
              ExtErrorsList := TEvExchangeResultLog.Create;
              ExtErrorsList.LoadFromStringList(OldErrorsList);
            end
            else
              ExtErrorsList :=  IInterface(ResultData.Value[EvoXExtErrorsList]) as IEvExchangeResultLog;

            for i := 0 to ExtErrorsList.Count - 1 do
            begin
              FdsErrorsLog.Append;
              FdsErrorsLog['Error#'] := i + 1;
              FdsErrorslog['Error message'] := Copy(ExtErrorsList.Items[i].GetText,1, ErrorMessage_length);
              FdsErrorsLog['ErrorType'] := Integer(ExtErrorsList.Items[i].GetEventType);
              FdsErrorsLog.Post;
            end;
          end;
        end
        else
        begin
         FdsErrorsLog.Append;
         FdsErrorsLog['Error#'] := 1;
         FdsErrorsLog['Error message'] := 'Error reading results. Cannot find result for file "' + FdsResults['File Name'] + '"';
         FdsErrorsLog['ErrorType'] := etInformation;
         FdsErrorsLog.Post;
        end;
      end;
    finally
      FdsErrorsLog.vclDataSet.EnableControls;
    end;
end;

procedure TrfEvoXImportTask.ResultDetailsGridCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
  EventType: TLogEventType;
begin
  inherited;
  if (FdsErrorsLog = nil) or (not FdsErrorsLog.vclDataSet.Active) or (FdsErrorsLog.RecordCount = 0) then
    exit;

  if (Field.FieldName = 'Error#') and (FdsErrorsLog['ErrorType'] <> null) then
  begin
    EventType := TLogEventType(FdsErrorsLog['ErrorType']);
    if (EventType = etError) or (EventType = etFatalError) then
      ABrush.Color := clRed
    else if (EventType = etWarning) then
      ABrush.Color := clYellow;
  end;
end;

end.
