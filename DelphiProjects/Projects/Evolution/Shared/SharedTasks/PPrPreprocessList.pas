// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PPrPreprocessList;

interface

uses
  Windows, EvTaskParamFrame, Classes, Controls, Grids, Wwdbigrd, Wwdbgrid, SysUtils,
   EvUtils, SDataStructure, EvConsts, Db, Wwdatsrc, EvTypes,
  SDDClasses, ISBasicClasses, SDataDicttemp, EvCommonInterfaces, StdCtrls, EvExceptions, EvUIComponents;

type
  TpfPrPreprocessList = class(TTaskParamFrame)
    gList: TevDBGrid;
    dsTMP_PR: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
  end;

implementation

{$R *.DFM}


{ TpfCompanyList }

procedure TpfPrPreprocessList.ApplyUpdates;
var
  r: Integer;
  s: string;
  sc: string;
begin
  inherited;
  r := DM_TEMPORARY.TMP_PR.RecNo;
  DM_TEMPORARY.TMP_PR.DisableControls;
  DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
  try
    s := '';
    sc := '';
    DM_TEMPORARY.TMP_PR.First;
    while not DM_TEMPORARY.TMP_PR.Eof do
    begin
      if DM_TEMPORARY.TMP_PR['SELECTED'] = 'Y' then
      begin
        s := s + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' +
                 DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').AsString + ' ';
        sc := sc + 'Co:' + Trim(DM_TEMPORARY.TMP_PR.FieldbyName('COMPANY_NUMBER_Lookup').AsString) +
                  ' Pr:' + DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').AsString + '-' + DM_TEMPORARY.TMP_PR.FieldByName('RUN_NUMBER').AsString + '  ';
      end;
      DM_TEMPORARY.TMP_PR.Next;
    end;
    if s = '' then
      raise EInvalidParameters.CreateHelp('No payrolls selected', IDH_InvalidParameters);
    (FTask as IevPreprocessPayrollTask).PrList := s;
    (FTask as IevPreprocessPayrollTask).Caption := sc;
  finally
    DM_TEMPORARY.TMP_PR.RecNo := r;
    DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    DM_TEMPORARY.TMP_PR.EnableControls;
  end;
end;

constructor TpfPrPreprocessList.Create(const AOwner: TComponent;
  const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  DM_TEMPORARY.TMP_PR.DataRequired('STATUS=''' + PAYROLL_STATUS_PENDING + '''');
  DM_TEMPORARY.TMP_PR.DisableControls;
  DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
  try
    DM_TEMPORARY.TMP_PR.Last;
    while not DM_TEMPORARY.TMP_PR.Bof do
    begin
      if Pos(' ' + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' +
                   DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').AsString + ' ',
             ' ' + (FTask as IevPreprocessPayrollTask).PrList + ' ') > 0 then
      begin
        DM_TEMPORARY.TMP_PR.Edit;
        DM_TEMPORARY.TMP_PR['SELECTED'] := 'Y';
        DM_TEMPORARY.TMP_PR.Post;
      end
      else if DM_TEMPORARY.TMP_PR['SELECTED'] = 'Y' then
      begin
        DM_TEMPORARY.TMP_PR.Edit;
        DM_TEMPORARY.TMP_PR['SELECTED'] := 'N';
        DM_TEMPORARY.TMP_PR.Post;
      end;
      DM_TEMPORARY.TMP_PR.Prior;
    end;
  finally
    DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    DM_TEMPORARY.TMP_PR.EnableControls;
  end;
end;

end.
