inherited pfPrPreprocessList: TpfPrPreprocessList
  object gList: TevDBGrid
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    DisableThemesInTitle = False
    ControlType.Strings = (
      'Selected;CheckBox;Y;N')
    Selected.Strings = (
      'SELECTED'#9'8'#9'Selected'#9'F'
      'CLIENT_NUMBER_Lookup'#9'17'#9'Client Number'
      'COMPANY_NUMBER_Lookup'#9'17'#9'Company Number'
      'COMPANY_NAME_Lookup'#9'37'#9'Company Name'
      'CHECK_DATE'#9'10'#9'Check Date'
      'PAYROLL_TYPE'#9'4'#9'Type'
      'RUN_NUMBER'#9'5'#9'Run #'
      'PROCESS_PRIORITY'#9'4'#9'Prty')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TpfPrPreprocessList\gList'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = dsTMP_PR
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    DefaultSort = 'PROCESS_PRIORITY;CHECK_DATE;RUN_NUMBER'
  end
  object dsTMP_PR: TevDataSource
    DataSet = DM_TMP_PR.TMP_PR
    Left = 345
    Top = 69
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 381
    Top = 69
  end
end
