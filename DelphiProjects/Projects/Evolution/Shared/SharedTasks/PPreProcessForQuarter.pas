// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PPreProcessForQuarter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskParamFrame, StdCtrls, ExtCtrls, Variants,  EvBasicUtils, EvUtils, DateUtils,
  ISBasicClasses, EvCommonInterfaces, evExceptions, Grids, Wwdbigrd,
  Wwdbgrid, SDDClasses, SDataDicttemp, SDataStructure, DB, Wwdatsrc,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvConsts, EvContext, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit;

type
  TpfPreProcessForQuarterParams = class(TTaskParamFrame)
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evComboBox1: TevComboBox;
    edQecLimit: TevEdit;
    rgTaxServiceFilter: TevRadioGroup;
    evLabel2: TevLabel;
    chbPrintReports: TevCheckBox;
    lblTaxAdjLimit: TevLabel;
    edTaxAdjLimit: TevEdit;
    dsTMP_CO: TevDataSource;
    evPanel3: TevPanel;
    evPanel13: TevPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evLabel14: TevLabel;
    evLabel13: TevLabel;
    edCusClNbr: TevEdit;
    edClName: TevEdit;
    edCusCoNbr: TevEdit;
    edCoName: TevEdit;
    btnCompanyFilter: TevButton;
    tsht2CompFilterGroup: TevGroupBox;
    rbAllCompanies: TevRadioButton;
    rbSelectedCompanies: TevRadioButton;
    grColist: TevDBCheckGrid;
    cdQueue: TevClientDataSet;
    cdQueueCL_NBR: TIntegerField;
    cdQueueCO_NBR: TIntegerField;
    cdQueueCO_NAME: TStringField;
    cdQueueCUSTOM_COMPANY_NUMBER: TStringField;
    cdQueueCUSTOM_CLIENT_NUMBER: TStringField;
    procedure chbPrintReportsClick(Sender: TObject);
    procedure edTaxAdjLimitChange(Sender: TObject);
    procedure grColistMultiSelectRecord(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure rbAllCompaniesClick(Sender: TObject);
    procedure FtaskToScreen;
    procedure btnCompanyFilterClick(Sender: TObject);
    procedure PrepareCompanyList;
    procedure rbSelectedCompaniesClick(Sender: TObject);
    procedure evComboBox1Change(Sender: TObject);
    procedure SetGridColor;
  private
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
    procedure CancelUpdates; override;
  end;

implementation

{$R *.DFM}

{ TPPreProcessForQuarterParamFrame }

procedure TpfPreProcessForQuarterParams.ApplyUpdates;
var
  t: IevPreprocessQuarterEndTask;
  d: TDateTime;
  i : integer;
begin
  inherited;

  t := FTask as IevPreprocessQuarterEndTask;
  t.QecAdjustmentLimit := StrToCurr(edQecLimit.Text);
  d := Integer(evComboBox1.Items.Objects[evComboBox1.ItemIndex]);
  if d = 0 then
    raise EevException.Create('You have to select quarter first');
  t.EndDate := d;
  t.BegDate := GetBeginQuarter(d);
  case rgTaxServiceFilter.ItemIndex of
  0: t.TaxServiceFilter := tsAll;
  1: t.TaxServiceFilter := tsTax;
  2: t.TaxServiceFilter := tsNonTax;
  else
    Assert(False);
  end;
  t.QecProduceReports := chbPrintReports.Checked;
  t.TaxAdjustmentLimit := StrToCurr(edTaxAdjLimit.Text);
  t.Filter.Clear;
  if rbAllCompanies.Checked then
     grColist.UnSelectAll;

  for i := 0 to grColist.SelectedList.Count - 1 do
  Begin
   cdQueue.GotoBookmark(grColist.SelectedList[i]);
   with t.Filter.Add do
   begin
    ClNbr := cdQueue['CL_NBR'];
    CoNbr := cdQueue['CO_NBR'];
    Consolidation := False;
    Caption := cdQueue['CUSTOM_COMPANY_NUMBER'];
   end;
  end;

end;

procedure TpfPreProcessForQuarterParams.CancelUpdates;
begin
  inherited;
  FtaskToScreen;
end;

procedure TpfPreProcessForQuarterParams.FtaskToScreen;
var
  t: IevPreprocessQuarterEndTask;
  i, j: Integer;
  d: TDateTime;
  s: string;
begin
  inherited;
  evComboBox1.Clear;
  t := FTask as IevPreprocessQuarterEndTask;

  for i := -4 to 4 do
  begin
    d := GetEndQuarter(Now);
    for j := i to -1 do
      d := GetBeginQuarter(d)- 1;
    for j := 1 to i do
      d := GetEndQuarter(d+ 1);
    case GetQuarterNumber(d) of
    1: s := '1''st';
    2: s := '2''nd';
    3: s := '3''rd';
    4: s := '4''th';
    end;
    evComboBox1.Items.AddObject(s+ ' quarter '+ IntToStr(GetYear(d)), Pointer(Trunc(d)));
  end;
  if t.EndDate = 0 then
  begin
    evComboBox1.Items.InsertObject(0, '-Unassigned-', nil);
    evComboBox1.ItemIndex := 0;
  end
  else
  begin
    evComboBox1.ItemIndex := 0;
    for i := 0 to evComboBox1.Items.Count-1 do
    begin
      d := Integer(evComboBox1.Items.Objects[i]);
      if d = t.EndDate then
      begin
        evComboBox1.ItemIndex := i;
        Break;
      end;
    end;
    if evComboBox1.ItemIndex = 0 then
      evComboBox1.Items.InsertObject(0, DateToStr(t.EndDate), Pointer(Trunc(t.EndDate)));
  end;

  if not ((SecondsBetween(Now, t.LastUpdate) < 10) and (t.QecAdjustmentLimit = 0)) then
    edQecLimit.Text := CurrToStr(t.QecAdjustmentLimit);
  case t.TaxServiceFilter of
  tsAll: rgTaxServiceFilter.ItemIndex := 0;
  tsTax: rgTaxServiceFilter.ItemIndex := 1;
  tsNonTax: rgTaxServiceFilter.ItemIndex := 2;
  else
    Assert(False);
  end;
  chbPrintReports.Checked := t.QecProduceReports;
  edTaxAdjLimit.Text := CurrToStr(t.TaxAdjustmentLimit);

  if not cdQueue.Active and (evComboBox1.ItemIndex > 0) then
     PrepareCompanyList;
  grCoList.UnSelectAll;
  if t.Filter.Count > 0 then
  begin
    for i := 0 to t.Filter.Count -1 do
    begin
      if cdQueue.Locate('CL_NBR;CO_NBR', VarArrayOf([t.Filter[i].ClNbr, t.Filter[i].CoNbr]), []) then
      begin
       grCoList.SelectedList.Add(cdQueue.GetBookmark);
      end;
    end;
   rbSelectedCompanies.Checked := True;
  end
  else
  begin
   rbAllCompanies.Checked := True;
   grCoList.Enabled := cdQueue.Active and (cdQueue.RecordCount > 0);
  end;

  chbPrintReports.OnClick(Self);
end;

constructor TpfPreProcessForQuarterParams.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FtaskToScreen;
end;

procedure TpfPreProcessForQuarterParams.chbPrintReportsClick(
  Sender: TObject);
begin
  inherited;
  lblTaxAdjLimit.Enabled := chbPrintReports.Checked;
  edTaxAdjLimit.Enabled := chbPrintReports.Checked;
end;

procedure TpfPreProcessForQuarterParams.edTaxAdjLimitChange(
  Sender: TObject);
begin
  inherited;
  try
    StrToFloat(edTaxAdjLimit.Text);
  except
    edTaxAdjLimit.Text := '0.00';
  end;
end;

procedure TpfPreProcessForQuarterParams.grColistMultiSelectRecord(
  Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_TASK.State = dsBrowse then
    DM_SERVICE_BUREAU.SB_TASK.Edit;
end;

procedure TpfPreProcessForQuarterParams.rbAllCompaniesClick(
  Sender: TObject);
begin
  inherited;
  SetGridColor;
end;

procedure TpfPreProcessForQuarterParams.rbSelectedCompaniesClick(
  Sender: TObject);
begin
  inherited;
  if not cdQueue.Active then
     PrepareCompanyList;
  SetGridColor;
end;


procedure TpfPreProcessForQuarterParams.PrepareCompanyList;
var
 Condition, CheckAnd, ReturnsQueue, s : String;
 d: TDateTime;
Begin
  Condition :='';
  CheckAnd := '';
  if Length(Trim(edCusClNbr.Text))> 0 then
     begin
      Condition := Condition + ' LOWER(t1.Custom_client_Number) like LOWER('''+'%'+Trim(edCusClNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edClName.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t1.Name) like LOWER('''+'%'+Trim(edClName.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edCusCoNbr.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t2.Custom_company_Number) like LOWER('''+'%'+Trim(edCusCoNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edCoName.Text))> 0 then
      Condition := Condition + CheckAnd + ' LOWER(t2.Name) like LOWER('''+'%'+Trim(edCoName.Text)+ '%'''+')';
  if Length(Trim(Condition))> 0 then
         Condition := 'and ' + Condition;
  if Length(Trim(Condition))= 0 then
         Condition := 'and  t1.Custom_client_Number like ''%''';

  ReturnsQueue := 'select '+
      'distinct q.cl_nbr, q.co_nbr, '+
      't2.name co_name, t2.CUSTOM_COMPANY_NUMBER, '+
      't1.CUSTOM_CLIENT_NUMBER '+
    'from TMP_CO_TAX_RETURN_QUEUE q '+
    'join TMP_CO t2 on t2.cl_nbr = q.cl_nbr and t2.co_nbr = q.co_nbr '+
    'join TMP_CL t1 on t1.cl_nbr = q.cl_nbr '+
    'left outer join TMP_CL_CO_CONSOLIDATION n on n.cl_nbr=q.cl_nbr and n.CL_CO_CONSOLIDATION_NBR=q.CL_CO_CONSOLIDATION_NBR and SCOPE='''+ CONSOLIDATION_SCOPE_ALL+ ''' '+
    'where q.PERIOD_END_DATE between :BegDate and :EndDate '+
      '#Cond '+
    'order by CUSTOM_COMPANY_NUMBER ';

    with TExecDSWrapper.Create(ReturnsQueue) do
    begin
      s := '';
      case rgTaxServiceFilter.ItemIndex of
        0: s := '';
        1: s := ' and TAX_SERVICE='+ QuotedStr(GROUP_BOX_YES);
        2: s := ' and TAX_SERVICE='+ QuotedStr(GROUP_BOX_NO);
      else
       Assert(False);
      end;
      if Length(Trim(Condition))> 0 then
             Condition := Condition + s;

      SetMacro('COND', Condition);
      d := Integer(evComboBox1.Items.Objects[evComboBox1.ItemIndex]);
      if d = 0 then
      raise EevException.Create('You have to select quarter first');
      SetParam('ENDDATE', d);
      SetParam('BEGDATE', GetBeginQuarter(d));
      cdQueue.Close;
      ctx_DataAccess.GetTmpCustomData(cdQueue, AsVariant);
    end;

    cdQueue.LogChanges := False;
    SetGridColor;
End;

procedure TpfPreProcessForQuarterParams.SetGridColor;
begin
    grColist.Enabled := rbSelectedCompanies.Checked and cdQueue.Active and (cdQueue.RecordCount > 0);
    if grColist.Enabled then
      grColist.Color := clWindow
    else
      grColist.Color := clBtnFace;
end;

procedure TpfPreProcessForQuarterParams.evComboBox1Change(Sender: TObject);
begin
  inherited;
  PrepareCompanyList;
  SetGridColor;
end;

procedure TpfPreProcessForQuarterParams.btnCompanyFilterClick(Sender: TObject);
begin
  PrepareCompanyList;
end;


end.
