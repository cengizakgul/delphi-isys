// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RPreProcessForQuarter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls, ExtCtrls,  Db, Wwdatsrc,
   Grids, Wwdbigrd, Wwdbgrid, DBCtrls, ComCtrls, Buttons, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SReportSettings,
  EvDataAccessComponents, EvUtils, rwPreviewContainerFrm, EvStreamUtils,
  EvContext, EvCommonInterfaces, EvStatisticsViewerFrm, EvUIComponents,
  evClientDataSet, isUIDBMemo, LMDCustomButton, LMDButton, isUILMDButton;

type
  TrfPreProcessForQuarter = class(TTaskResultFrame)
    evDBGrid1: TevDBGrid;
    evClientDataSet1: TevClientDataSet;
    evDataSource1: TevDataSource;
    evClientDataSet1CoCustomNumber: TStringField;
    evClientDataSet1CoName: TStringField;
    evClientDataSet1Consolidation: TBooleanField;
    evClientDataSet1TimeStampStarted: TDateTimeField;
    evClientDataSet1TimeStampFinished: TDateTimeField;
    evClientDataSet1Message: TStringField;
    EvDBMemo1: TEvDBMemo;
    tsReports: TTabSheet;
    PreviewContainer: TrwPreviewContainer;
    procedure PreviewContainerResize(Sender: TObject);
  private
    FReports: TrwReportResults;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.DFM}

{ TPreProcessForQuarterResultFrame }

constructor TrfPreProcessForQuarter.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  i: Integer;
  FReports: TrwReportResults;
begin
  inherited;

  with Task as IevPreprocessQuarterEndTask do
  begin
    FReports := TrwReportResults.Create;

    if ReportResults <> nil then
    begin
      ReportResults.Position := 0;
      FReports.SetFromStream(ReportResults);
    end;

    evClientDataSet1.CreateDataSet;
    if Assigned(Results) then
      for i := 0 to Results.Count-1 do
      begin
        evClientDataSet1.AppendRecord([Results[i].CoCustomNumber, Results[i].CoName, Results[i].Consolidation,
          Results[i].TimeStampStarted, Results[i].TimeStampFinished, Results[i].ErrorMessage]);
      end;
  end;

  ctx_RWLocalEngine.Preview(FReports, PreviewContainer);
end;

destructor TrfPreProcessForQuarter.Destroy;
begin
  FReports.Free;
  inherited;
end;

procedure TrfPreProcessForQuarter.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

end.
