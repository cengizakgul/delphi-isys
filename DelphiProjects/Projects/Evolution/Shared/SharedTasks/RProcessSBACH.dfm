inherited rfProcessSBACH: TrfProcessSBACH
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      OnShow = tsResultsShow
      inline rwPreviewContainer1: TrwPreviewContainer
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        Align = alClient
        AutoScroll = False
        TabOrder = 0
      end
      object PageControl: TevPageControl
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        ActivePage = tsAchFile
        Align = alClient
        MultiLine = True
        TabOrder = 1
        TabPosition = tpRight
        object tsAchFile: TTabSheet
          Caption = 'Ach File'
          ImageIndex = 1
          OnShow = tsAchFileShow
          inline ACHFilePreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 381
            Height = 232
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsAchReport: TTabSheet
          Caption = 'Ach Report'
          ImageIndex = 4
          OnShow = tsAchReportShow
          inline ACHRegularReportPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 485
            Height = 304
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsAchDetailedReport: TTabSheet
          Caption = 'Detailed Report'
          ImageIndex = 2
          OnShow = tsAchDetailedReportShow
          inline ACHDetailedReportPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 485
            Height = 304
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsSave: TTabSheet
          Caption = 'Save File'
          ImageIndex = 3
        end
        object tsNonAchReport: TTabSheet
          Caption = 'Non Ach Report'
          ImageIndex = 4
          OnShow = tsNonAchReportShow
          inline NonAchReportPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 485
            Height = 304
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
        object tsWTFile: TTabSheet
          Caption = 'W/T ASCII'
          ImageIndex = 5
          OnShow = tsWTFileShow
          inline WTFilePreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 485
            Height = 304
            Align = alClient
            AutoScroll = False
            TabOrder = 0
          end
        end
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Width = 531
        Height = 271
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Courier New'
        ParentFont = False
      end
      inherited pWarnings: TevPanel
        Top = 271
        Width = 531
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Height = 251
      end
    end
    inherited tsRunStatistics: TTabSheet
      inherited frmStatistics: TevStatisticsViewerFrm
        inherited tvEvents: TTreeView
          Width = 73
        end
        inherited pnlChart: TPanel
          inherited Chart: TChart
            Height = 179
          end
        end
      end
    end
  end
end
