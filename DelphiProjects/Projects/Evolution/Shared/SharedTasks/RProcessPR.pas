// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RProcessPR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, SDataStructure, SLogCheckReprint, EvTypes, EvBasicUtils,
  rwPreviewContainerFrm, ISBasicClasses, EvConsts, EvContext, EvCommonInterfaces, EvMainboard,
  EvStatisticsViewerFrm, EvUIUtils, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TrfProcessPR = class(TTaskResultFrame)
    tsMessages: TTabSheet;
    memoMessages: TevRichEdit;
    PreviewContainer: TrwPreviewContainer;
    Panel1: TPanel;
    ReprintChecksBtn: TButton;
    procedure PCChange(Sender: TObject);
    procedure ReprintChecks1Click(Sender: TObject);
    procedure PreviewContainerResize(Sender: TObject);
    procedure ReprintChecksBtnClick(Sender: TObject);
  private
    FPreviewed: Boolean;
    FResult: TrwReportResults;
    FPrList: String;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
//    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

{ TrfReportPreview }

constructor TrfProcessPR.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  i: Integer;
begin
  inherited;
  ReprintChecksBtn.Enabled :=  ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') = stEnabled;
  memoMessages.Lines.Text := (ATask as IevProcessPayrollTask).PRMessage;
  FPreviewed := False;
  FResult := TrwReportResults.Create;

  if ((ATask as IevProcessPayrollTask).ReportResults <> nil) and
     ((ATask as IevProcessPayrollTask).ReportResults.Size > 0) then
  begin
    (ATask as IevProcessPayrollTask).ReportResults.Position := 0;
    FResult.SetFromStream((ATask as IevProcessPayrollTask).ReportResults);
  end;

  FPrList := (ATask as IevProcessPayrollTask).PrList;

  for i := 0 to FResult.Count - 1 do
  begin
    if Length(FResult[i].ErrorMessage) > 0 then
      (ATask as IevProcessPayrollTask).AddException(FResult[i].ReportName + ':  ' + FResult[i].ErrorMessage);
  end;

  CheckExceptions;

  Align := alClient;
  if memoExceptions.Text <> '' then
    PC.ActivePage := tsExceptions
  else
    PC.ActivePage := tsMessages;
end;

destructor TrfProcessPR.Destroy;
begin
  FResult.Free;
  inherited;
end;

{
procedure TrfProcessPR.OnShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if tsResults.ControlCount > 0 then
    RWEngineInterface.PreviewShortCutNotify(TWinControl(tsResults.Controls[0]), Msg, Handled);
end;
}
procedure TrfProcessPR.PCChange(Sender: TObject);
begin
  inherited;
  if (PC.ActivePage = tsResults) and not FPreviewed then
  begin
    ctx_RWLocalEngine.Preview(FResult, PreviewContainer);
    FPreviewed := True;
  end;
end;

procedure TrfProcessPR.ReprintChecks1Click(Sender: TObject);
var
  Res: TrwReportResults;
  I, ClId: Integer;
  ReprintReason, S: String;
  slNormalChecks, slMiscChecks: TStringList;
begin
  inherited;
  if EvMessage('Are you sure you would like to reprint all checks in this task?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  if Trim(FPrList) = '' then
    Exit;
  ReprintReason := '';
  repeat
    S := Fetch(FPrList);
    ClId := StrToInt(Fetch(s, ';'));
    ctx_DataAccess.OpenClient(ClId);
    slNormalChecks := TStringList.Create;
    slMiscChecks := TStringList.Create;
    try
      DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + S);
      DM_PAYROLL.PR_CHECK.First;
      while not DM_PAYROLL.PR_CHECK.EOF do
      begin
        slNormalChecks.Add(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        DM_PAYROLL.PR_CHECK.Next;
      end;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=' + S);
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.First;
      while not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.EOF do
      begin
        slMiscChecks.Add(DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Next;
      end;

      if slNormalChecks.Count > 0 then
      begin
        slNormalChecks.Sort;
        ReprintReason := LogCheckReprint(slNormalChecks, False, StrToInt(S), ReprintReason);
      end;
      if slMiscChecks.Count > 0 then
      begin
        slMiscChecks.Sort;
        ReprintReason := LogCheckReprint(slMiscChecks, True, StrToInt(S), ReprintReason);
      end;
    finally
      slNormalChecks.Free;
      slMiscChecks.Free;
    end;
  until Trim(FPrList) = '';

  Res := TrwReportResults.Create;
  try
    Res.Assign(FResult);
    for I := Res.Count - 1 downto 0 do
      if Res.Items[I].ReportType <> rtCheck then
        Res.Items[I].Destroy;
    ctx_RWLocalEngine.Print(Res);
  finally
    Res.Free;
  end;
end;

procedure TrfProcessPR.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

procedure TrfProcessPR.ReprintChecksBtnClick(Sender: TObject);
var
  Res: TrwReportResults;
  I, ClId: Integer;
  ReprintReason, S: String;
  slNormalChecks, slMiscChecks: TStringList;
  Ctx: IevContext;
begin
  inherited;
  if EvMessage('Are you sure you would like to reprint all checks in this task?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  if Trim(FPrList) = '' then
    Exit;
  ReprintReason := '';
  repeat
    S := Fetch(FPrList);
    ClId := StrToInt(Fetch(s, ';'));
    ctx_DataAccess.OpenClient(ClId);
    slNormalChecks := TStringList.Create;
    slMiscChecks := TStringList.Create;
    try
      DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + S);
      DM_PAYROLL.PR_CHECK.First;
      while not DM_PAYROLL.PR_CHECK.EOF do
      begin
        slNormalChecks.Add(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        DM_PAYROLL.PR_CHECK.Next;
      end;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=' + S);
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.First;
      while not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.EOF do
      begin
        slMiscChecks.Add(DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Next;
      end;

      if slNormalChecks.Count > 0 then
      begin
        slNormalChecks.Sort;
        ReprintReason := LogCheckReprint(slNormalChecks, False, StrToInt(S), ReprintReason);
      end;
      if slMiscChecks.Count > 0 then
      begin
        slMiscChecks.Sort;
        ReprintReason := LogCheckReprint(slMiscChecks, True, StrToInt(S), ReprintReason);
      end;
    finally
      slNormalChecks.Free;
      slMiscChecks.Free;
    end;
  until Trim(FPrList) = '';

  Res := TrwReportResults.Create;
  try
    Res.Assign(FResult);
    for I := Res.Count - 1 downto 0 do
      if Res.Items[I].ReportType <> rtCheck then
        Res.Items[I].Destroy;

    Mainboard.ContextManager.StoreThreadContext;
    try
      Ctx := Mainboard.ContextManager.CopyContext(Context);
      Mainboard.ContextManager.SetThreadContext(Ctx);
      ctx_RWLocalEngine.Print(Res);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  finally
    Res.Free;
  end;
end;

end.
