// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PReportParams;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskParamFrame, SDataStructure, StdCtrls, wwdblook, 
  ExtCtrls, DB,  EvConsts, Wwdatsrc, DBCtrls, EvContext,
  SReportSettings, evTypes, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  SDDClasses, ISDataAccessComponents, EvCommonInterfaces,
  EvDataAccessComponents, SDataDictsystem, SDataDictbureau, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIDBMemo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIwwDBLookupCombo;

type
  TpfReportParams = class(TTaskParamFrame)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel1: TevLabel;
    cbReports: TevDBLookupCombo;
    btnInpForm: TevButton;
    csReports: TevClientDataSet;
    memNote: TEvDBMemo;
    evDataSource1: TevDataSource;
    chbPrint: TevCheckBox;
    edFileName: TevEdit;
    evLabel2: TevLabel;
    procedure btnInpFormClick(Sender: TObject);
    procedure edFileNameChange(Sender: TObject);
    procedure cbReportsCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  private
    FInParams: TrwReportParams;
    FReports: TrwReportList;

    procedure CreateReportSourceDataSet;
    procedure UpdateScreenStatus;

  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
    procedure   ApplyUpdates; override;
  end;

implementation

uses
  EvUtils;

{$R *.DFM}


{ TpfReportParams }

constructor TpfReportParams.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FReports := TrwReportList.Create;

  ctx_RWLocalEngine.BeginWork;

  CreateReportSourceDataSet;

  if Assigned((FTask as IevRunReportTask).Reports) and ((FTask as IevRunReportTask).Reports.Size > 0) then
  begin
    (FTask as IevRunReportTask).Reports.Position := 0;
    FReports.SetFromStream((FTask as IevRunReportTask).Reports);
    chbPrint.Checked := ((FTask as IevRunReportTask).Destination = rdtRemotePrinter);
    cbReports.LookupValue := IntToStr(FReports[0].NBR) + FReports[0].Level;
    edFileName.Text := FReports[0].Params.FileName;
  end
  else
    FReports.AddReport;

  FInParams := FReports[0].Params;

  UpdateScreenStatus;
end;


procedure TpfReportParams.CreateReportSourceDataSet;
var
  SY_DS: TevClientDataSet;
  SB_DS: TevClientDataSet;


  procedure AddDBReports(ALevel: String);
  var
    KeyFld, h: String;
    DS: TevClientDataSet;
  begin
    KeyFld := '_REPORT_WRITER_REPORTS_NBR';
    case ALevel[1] of
      CH_DATABASE_SYSTEM:  begin
              KeyFld := 'SY' + KeyFld;
              DS := SY_DS;
            end;

      CH_DATABASE_SERVICE_BUREAU:
            begin
              KeyFld := 'SB' + KeyFld;
              DS := SB_DS;
            end;
    else
      DS := nil;
    end;

    DS.First;
    while not DS.Eof do
    begin
      h := DS.FieldByName('report_description').AsString;

      if DS.FieldByName('REPORT_TYPE').AsString[1] = SYSTEM_REPORT_TYPE_MULTICLIENT then
      begin
        csReports.Append;
        csReports.FieldByName('nbr').AsInteger := DS.FieldByName(KeyFld).AsInteger;
        csReports.FieldByName('level').AsString := ALevel;
        csReports.FieldByName('Key').AsString := DS.FieldByName(KeyFld).AsString + ALevel;
        csReports.FieldByName('name').AsString := h;
        csReports.FieldByName('notes').Value := DS.FieldByName('notes').Value;
        csReports.FieldByName('Type').AsString := DS.FieldByName('REPORT_TYPE').AsString[1];
        csReports.Post;
      end;

      DS.Next;
    end;
  end;

begin
  if csReports.Active then
    Exit;

  DM_SERVICE_BUREAU.SB_REPORTS.CheckDSCondition('');
  ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_REPORTS]);

  csReports.DisableControls;
  csReports.Close;
  csReports.CreateDataSet;

  SY_DS := TevClientDataSet.Create(nil);
  SB_DS := TevClientDataSet.Create(nil);

  try
    if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrent') do
    begin
      SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES');
      SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
      ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      SY_DS.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;
    end;

    if ctx_DataAccess.SB_CUSTOM_VIEW.Active then
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrent') do
    begin
      SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES');
      SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      SB_DS.Data := ctx_DataAccess.SB_CUSTOM_VIEW.Data;
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    end;

    AddDBReports(CH_DATABASE_SYSTEM);
    AddDBReports(CH_DATABASE_SERVICE_BUREAU);

  finally
    SY_DS.Free;
    SB_DS.Free;
  end;

  csReports.First;
  csReports.EnableControls;
  cbReports.LookupValue := csReports.FieldByName('Key').AsString;
end;

procedure TpfReportParams.btnInpFormClick(Sender: TObject);
var
  Res: TModalResult;
begin
  try
    FInParams.Add('SetupMode', False);
    FInParams.Add('ReportName', csReports.FieldByName('Name').AsString);

    Res := ctx_RWLocalEngine.ShowModalInputForm(csReports.FieldByName('NBR').AsInteger,
      csReports.FieldByName('LEVEL').AsString, FInParams, False);
  finally
    if not DM_COMPANY.CO.Active then
      DM_COMPANY.CO.Active := True;
  end;

  if (Res = mrOk) then
    if DM_SERVICE_BUREAU.SB_TASK.State = dsBrowse then
      DM_SERVICE_BUREAU.SB_TASK.Edit;
end;

destructor TpfReportParams.Destroy;
begin
  FReports.Free;
  ctx_RWLocalEngine.EndWork;
  inherited;
end;

procedure TpfReportParams.ApplyUpdates;
begin
  with FTask as IevRunReportTask do
  begin
    if edFileName.Text <> '' then
      Destination := rdtRemoteStorage
    else
      if chbPrint.Checked then
        Destination := rdtRemotePrinter
      else
        Destination := rdtPreview;

    FReports[0].NBR := csReports.FieldByName('nbr').AsInteger;
    FReports[0].Level := csReports.FieldByName('LEVEL').AsString;
    FReports[0].Params.FileName := edFileName.Text;
    Caption := csReports.FieldByName('name').AsString;

    Reports := FReports.GetAsStream;
  end;

  inherited;
end;

procedure TpfReportParams.edFileNameChange(Sender: TObject);
begin
  UpdateScreenStatus;
end;

procedure TpfReportParams.UpdateScreenStatus;
begin
  if edFileName.Text <> '' then
  begin
    chbPrint.Checked := False;
    chbPrint.Enabled := False;
  end
  else
    chbPrint.Enabled := True;
end;

procedure TpfReportParams.cbReportsCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (DM_SERVICE_BUREAU.SB_TASK.State = dsEdit) then
     EvMessage('You have changed the report.  Be sure to update the Report Parameters to ensure that you get accurate results for your report.', mtInformation, [mbOK]);

end;

end.
