// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RReprintPR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, EvTypes, SLogCheckReprint, EvConsts, EvContext,
  rwPreviewContainerFrm, ISBasicClasses, EvCommonInterfaces, EvMainboard,
  EvStatisticsViewerFrm, EvUIUtils, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TrfReprintPR = class(TTaskResultFrame)
    PreviewContainer: TrwPreviewContainer;
    Panel1: TPanel;
    ReprintChecksBtn: TButton;
    procedure ReprintChecks1Click(Sender: TObject);
    procedure PreviewContainerResize(Sender: TObject);
    procedure ReprintChecksBtnClick(Sender: TObject);
  private
    FResult: TrwReportResults;
    FNormalChecks: TStringList;
    FMiscChecks: TStringList;
    FPrNbr: Integer;
    FClientId: Integer;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
//    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); override;
  end;

implementation

uses
   SSecurityInterface;

{$R *.DFM}

{ TrfReprintPR }

constructor TrfReprintPR.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  ReprintChecksBtn.Enabled :=  ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') = stEnabled;
  FResult := TrwReportResults.Create;
  FNormalChecks := TStringList.Create;
  FMiscChecks := TStringList.Create;

  if (Task as IevReprintPayrollTask).ReportResults <> nil then
  begin
    (Task as IevReprintPayrollTask).ReportResults.Position := 0;
    FResult.SetFromStream((Task as IevReprintPayrollTask).ReportResults);
  end;
    
  FNormalChecks.CommaText := (Task as IevReprintPayrollTask).PayrollChecks;
  FMiscChecks.CommaText := (Task as IevReprintPayrollTask).AgencyChecks;
  if FMiscChecks.Count > 0 then
    FMiscChecks.CommaText := FMiscChecks.CommaText + ',' + (Task as IevReprintPayrollTask).TaxChecks
  else
    FMiscChecks.CommaText := (Task as IevReprintPayrollTask).TaxChecks;
  if FMiscChecks.Count > 0 then
    FMiscChecks.CommaText := FMiscChecks.CommaText + ',' + (Task as IevReprintPayrollTask).BillingChecks
  else
    FMiscChecks.CommaText := (Task as IevReprintPayrollTask).BillingChecks;
  FPrNbr := (Task as IevReprintPayrollTask).PrNbr;
  FClientId := (Task as IevReprintPayrollTask).ClientId;

  memoWarnings.Text := (Task as IevReprintPayrollTask).Warnings;
  tsWarnings.TabVisible := Length(memoWarnings.Text) > 0;

  CheckExceptions;

  Align := alClient;
  ctx_RWLocalEngine.Preview(FResult, PreviewContainer);
end;


destructor TrfReprintPR.Destroy;
begin
  FResult.Free;
  FNormalChecks.Free;
  FMiscChecks.Free;
  inherited;
end;

procedure TrfReprintPR.ReprintChecks1Click(Sender: TObject);
var
  Res: TrwReportResults;
  I: Integer;
  ReprintReason: String;
  Ctx: IevContext;
begin
  inherited;
  if EvMessage('Are you sure you would like to reprint all checks in this task?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ReprintReason := '';
  ctx_DataAccess.OpenClient(FClientId);
  if FNormalChecks.Count > 0 then
  begin
    FNormalChecks.Sort;
    ReprintReason := LogCheckReprint(FNormalChecks, False, FPrNbr);
  end;
  if FMiscChecks.Count > 0 then
  begin
    FMiscChecks.Sort;
    LogCheckReprint(FMiscChecks, True, FPrNbr, ReprintReason);
  end;

  Res := TrwReportResults.Create;
  try
    Res.Assign(FResult);
    for I := Res.Count - 1 downto 0 do
      if Res.Items[I].ReportType <> rtCheck then
        Res.Items[I].Destroy;

    Mainboard.ContextManager.StoreThreadContext;
    try
      Ctx := Mainboard.ContextManager.CopyContext(Context);
      Mainboard.ContextManager.SetThreadContext(Ctx);
      ctx_RWLocalEngine.Print(Res);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  finally
    Res.Free;
  end;
end;

procedure TrfReprintPR.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

procedure TrfReprintPR.ReprintChecksBtnClick(Sender: TObject);
var
  Res: TrwReportResults;
  I: Integer;
  ReprintReason: String;
  Ctx: IevContext;
begin
  inherited;
  if EvMessage('Are you sure you would like to reprint all checks in this task?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ReprintReason := '';
  ctx_DataAccess.OpenClient(FClientId);
  if FNormalChecks.Count > 0 then
  begin
    FNormalChecks.Sort;
    ReprintReason := LogCheckReprint(FNormalChecks, False, FPrNbr);
  end;
  if FMiscChecks.Count > 0 then
  begin
    FMiscChecks.Sort;
    LogCheckReprint(FMiscChecks, True, FPrNbr, ReprintReason);
  end;

  Res := TrwReportResults.Create;
  try
    Res.Assign(FResult);
    for I := Res.Count - 1 downto 0 do
      if Res.Items[I].ReportType <> rtCheck then
        Res.Items[I].Destroy;

    Mainboard.ContextManager.StoreThreadContext;
    try
      Ctx := Mainboard.ContextManager.CopyContext(Context);
      Mainboard.ContextManager.SetThreadContext(Ctx);
      ctx_RWLocalEngine.Print(Res);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  finally
    Res.Free;
  end;
end;

end.
