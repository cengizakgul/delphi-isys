// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RAcaUpdate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, Buttons, ExtCtrls,
  EvCommonInterfaces, ISBasicClasses, EvStatisticsViewerFrm, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, DB, Grids, Wwdbigrd, Wwdbgrid,
  Wwdatsrc, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, EvUtils,
  SFieldCodeValues, EvDataAccessComponents, EvClientDataSet, Menus, EvContext,
  SDDClasses, SDataDictclient, SDataStructure, ISBasicUtils, EvConsts, EvTypes, EvDataset, Variants;

type
  TrfAcaUpdate = class(TTaskResultFrame)
    cdCoList: TevClientDataSet;
    cdEEList: TevClientDataSet;
    dsCoList: TevDataSource;
    dsEEList: TevDataSource;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    grdEEList: TevDBGrid;
    grdCoList: TevDBGrid;
    mnRollback: TevPopupMenu;
    Rollback1: TMenuItem;
    DM_COMPANY: TDM_COMPANY;
    RollbackAllCompanies1: TMenuItem;
    procedure dsCoListDataChange(Sender: TObject; Field: TField);
    procedure Rollback1Click(Sender: TObject);
    procedure RollbackAllCompanies1Click(Sender: TObject);
    procedure Rollback;
    Procedure SetcdEEListFilter;        
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
  end;

implementation

{$R *.DFM}

{ TrfPAcaUpdate }

constructor TrfAcaUpdate.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  t: IevACAUpdateTask;
  i,j: integer;
  QCoList: IevQuery;
  sName: string;
begin
  inherited;
  QCoList := TevQuery.Create(' SELECT CL_NBR, CO_NBR, CUSTOM_COMPANY_NUMBER FROM TMP_CO order by CL_NBR,CO_NBR' );
  QCoList.Execute;
  QCoList.Result.IndexFieldNames:= 'CL_NBR;CO_NBR';

  t := Task as IevACAUpdateTask;

  cdEEList.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
  cdEEList.FieldDefs.Add('CO_NBR', ftInteger, 0, True);
  cdEEList.FieldDefs.Add('EE_NBR', ftInteger, 0, True);
  cdEEList.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 9, True);
  cdEEList.FieldDefs.Add('FIRST_NAME', ftString, 20, True);
  cdEEList.FieldDefs.Add('LAST_NAME', ftString, 30, True);
  cdEEList.FieldDefs.Add('OLD_ACA_STATUS', ftString, 1, false);
  cdEEList.FieldDefs.Add('OLD_ACA_STATUS_DESC', ftString,20, false);
  cdEEList.FieldDefs.Add('NEW_ACA_STATUS', ftString, 1, false);
  cdEEList.FieldDefs.Add('NEW_ACA_STATUS_DESC', ftString,20, false);
  cdEEList.FieldDefs.Add('UpdateType', ftString, 1, false);
  cdEEList.FieldDefs.Add('Report', ftString,40, false);
  cdEEList.CreateDataSet;
  cdEEList.LogChanges := False;
  cdEEList.IndexFieldNames:= 'CUSTOM_EMPLOYEE_NUMBER';

  cdCoList.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
  cdCoList.FieldDefs.Add('CO_NBR', ftInteger, 0, True);
  cdCoList.FieldDefs.Add('CoName', ftString, 20, True);
  cdCoList.FieldDefs.Add('RolledBack', ftString, 2, false);
  cdCoList.FieldDefs.Add('UpdateDate', ftDateTime, 0, False);
  cdCoList.CreateDataSet;
  cdCoList.LogChanges := False;
  cdCoList.IndexFieldNames:= 'CL_NBR;CO_NBR';

  grdCoList.Selected.Text :=
      'CoName' + #9 + '30' + #9 + 'Company Name' + #9 + 'F';
  grdEEList.Selected.Text :=
      'CUSTOM_EMPLOYEE_NUMBER' + #9 + '10' + #9 + 'EE Number' + #9 + 'F' + #13 + #10 +
      'FIRST_NAME' + #9 + '30' + #9 + 'First Name' + #9 + 'F' + #13 + #10 +
      'LAST_NAME' + #9 + '30' + #9 + 'Last Name' + #9 + 'F' + #13 + #10 +
      'OLD_ACA_STATUS_DESC' + #9 + '20' + #9 + 'Pre ACA Status' + #9 + 'F' + #13 + #10 +
      'NEW_ACA_STATUS_DESC' + #9 + '20' + #9 + 'New ACA Status' + #9 + 'F' + #13 + #10 +
      'Report' + #9 + '40' + #9 + 'Report' + #9 + 'F';


  grdCoList.ApplySelected;
  grdEEList.ApplySelected;

  cdCoList.DisableControls;
  cdEEList.DisableControls;
  cdCoList.Filtered:= false;
  Try
    if t.Results.Count > 0 then
      for i := 0 to t.Results.Count - 1 do
      begin
        if t.Results.Items[i].Count> 0 then
        begin
            for j := 0 to  t.Results.Items[i].Count - 1 do
            begin
             if (not cdCoList.Locate('CL_NBR;CO_NBR', VarArrayOf([t.Results.Items[i].ClNbr, t.Results.Items[i].Items[j].CO_NBR]), [])) and
                      QCoList.Result.Locate('CL_NBR;CO_NBR', VarArrayOf([t.Results.Items[i].ClNbr, t.Results.Items[i].Items[j].CO_NBR]), []) then
             begin
              if not Contains(t.Notes, QCoList.Result.fieldByName('CUSTOM_COMPANY_NUMBER').AsString  + ' - ' +  'ACA Status updates rolled back') then
                cdCoList.AppendRecord([ t.Results.Items[i].ClNbr,
                                        t.Results.Items[i].Items[j].CO_NBR,
                                        QCoList.Result.fieldByName('CUSTOM_COMPANY_NUMBER').AsString,
                                        'N',
                                        t.Results.Items[i].UpdateDate])
                else
                 cdCoList.AppendRecord([t.Results.Items[i].ClNbr,
                                        t.Results.Items[i].Items[j].CO_NBR,
                                        QCoList.Result.fieldByName('CUSTOM_COMPANY_NUMBER').AsString,
                                        'Y',
                                        t.Results.Items[i].UpdateDate]);
             end;

             case t.Results.Items[i].Items[j].UpdateType[1] of
              '1' : sName:='Inactive Employee Report (S2731)';
              '2' : sName:='ACA Rule Of Parity (S2834)';
              else  sName:='ACA Eligibility Analysis Report (S2713)';
             end;
             
             cdEEList.AppendRecord([ t.Results.Items[i].ClNbr,
                                      t.Results.Items[i].Items[j].CO_NBR,
                                      t.Results.Items[i].Items[j].EE_NBR,
                                      t.Results.Items[i].Items[j].CUSTOM_EMPLOYEE_NUMBER,
                                      t.Results.Items[i].Items[j].FIRST_NAME, t.Results.Items[i].Items[j].LAST_NAME,
                                      t.Results.Items[i].Items[j].OLD_ACA_STATUS,
                                      ReturnDescription(ACAStatus_ComboChoices, t.Results.Items[i].Items[j].OLD_ACA_STATUS),
                                      t.Results.Items[i].Items[j].NEW_ACA_STATUS,
                                      ReturnDescription(ACAStatus_ComboChoices, t.Results.Items[i].Items[j].NEW_ACA_STATUS),
                                      t.Results.Items[i].Items[j].UpdateType, sName]);
            end;
        end;
      end;
  finally
   cdCoList.Filter:= 'RolledBack=''N''';
   cdCoList.Filtered:= True;
   cdCoList.First;
   SetcdEEListFilter;
   cdEEList.First;
   cdEEList.EnableControls;
   cdCoList.EnableControls;
  end;
end;

Procedure TrfAcaUpdate.SetcdEEListFilter;
Begin
  if cdEEList.Active then
  begin
    if (cdCoList.RecordCount > 0) and (not cdCoList.FieldByName('CL_NBR').isNull ) then
       cdEEList.Filter := 'CL_NBR=' + cdCoList.FieldByName('CL_NBR').AsString +
                          ' and CO_NBR = ' + cdCoList.FieldByName('CO_NBR').AsString
    else
       cdEEList.Filter := 'CL_NBR= -1 and CO_NBR = -1';
    cdEEList.Filtered:= True;
  end;
End;

procedure TrfAcaUpdate.dsCoListDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  SetcdEEListFilter;
end;

procedure TrfAcaUpdate.Rollback;
var
 I, tmpClient: integer;
 bAddNote: Boolean;
begin
  ctx_StartWait;
  tmpClient:= ctx_DataAccess.ClientID;
  try
    dsCoList.DataSet.DisableControls;
    cdEEList.DisableControls;
    cdEEList.Filter := '';
    cdEEList.Filtered:= false;
    cdEEList.IndexFieldNames:= 'CL_NBR;CO_NBR';
    try
      for I := 0 to grdCoList.SelectedList.Count - 1 do
      begin
        dsCoList.DataSet.GotoBookmark(grdCoList.SelectedList.Items[I]);
        ctx_DataAccess.OpenClient(dsCoList.DataSet.FieldByName('CL_NBR').AsInteger);

        cdEEList.SetRange([dsCoList.DataSet['CL_NBR'], dsCoList.DataSet['CO_NBR']],
                          [dsCoList.DataSet['CL_NBR'], dsCoList.DataSet['CO_NBR']]);

        bAddNote:= false;
        try
          ctx_DBAccess.StartTransaction([dbtClient]);
          try
            cdEEList.First;
            while not cdEEList.Eof do
            begin
              bAddNote:= true;
              ctx_DBAccess.UpdateFieldValue('EE','ACA_STATUS', cdEEList.FieldByName('EE_NBR').asInteger,
                               dsCoList.DataSet.FieldByName('UpdateDate').asDatetime, DayBeforeEndOfTime,
                               cdEEList.FieldByName('OLD_ACA_STATUS').asVariant);

              cdEEList.Next;
            end;

            ctx_DBAccess.CommitTransaction;
          except
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        finally
         cdEEList.CancelRange;
         if bAddNote then
         begin
           AddNoteToTask(dsCoList.DataSet.FieldByName('CoName').AsString + ' - ' +  'ACA Status updates rolled back');
           cdCoList.Edit;
           cdCoList['RolledBack']:= 'Y';
           cdCoList.post;
         end;
        end;
      end;
    finally
      cdCoList.First;
      cdEEList.IndexFieldNames:= 'CUSTOM_EMPLOYEE_NUMBER';
      SetcdEEListFilter;
      cdEEList.EnableControls;
      dsCoList.DataSet.EnableControls;
    end;
  finally
    ctx_DataAccess.OpenClient(tmpClient);
    ctx_EndWait;
  end;
end;

procedure TrfAcaUpdate.Rollback1Click(Sender: TObject);
begin
  inherited;
  Rollback;
end;

procedure TrfAcaUpdate.RollbackAllCompanies1Click(Sender: TObject);
begin
  inherited;
  grdCoList.SelectAll;
  Rollback;
end;

end.

