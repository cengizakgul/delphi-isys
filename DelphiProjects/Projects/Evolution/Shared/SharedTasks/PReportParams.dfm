inherited pfReportParams: TpfReportParams
  AutoScroll = False
  object evLabel1: TevLabel
    Left = 6
    Top = 9
    Width = 32
    Height = 13
    Caption = 'Report'
  end
  object evLabel2: TevLabel
    Left = 8
    Top = 192
    Width = 118
    Height = 13
    Caption = 'File name for ASCII result'
  end
  object cbReports: TevDBLookupCombo
    Left = 48
    Top = 7
    Width = 289
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME'#9'64'#9'NAME'#9'F')
    LookupTable = csReports
    LookupField = 'KEY'
    Style = csDropDownList
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnCloseUp = cbReportsCloseUp
  end
  object btnInpForm: TevButton
    Left = 361
    Top = 4
    Width = 140
    Height = 25
    Caption = 'Report Parameters'
    TabOrder = 1
    OnClick = btnInpFormClick
    Margin = 0
  end
  object memNote: TEvDBMemo
    Left = 8
    Top = 37
    Width = 329
    Height = 109
    DataField = 'notes'
    DataSource = evDataSource1
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object chbPrint: TevCheckBox
    Left = 8
    Top = 162
    Width = 185
    Height = 17
    Caption = 'Print on Request Broker'
    TabOrder = 3
  end
  object edFileName: TevEdit
    Left = 136
    Top = 192
    Width = 361
    Height = 21
    TabOrder = 4
    OnChange = edFileNameChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 269
    Top = 47
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 269
    Top = 79
  end
  object csReports: TevClientDataSet
    IndexFieldNames = 'name'
    ControlType.Strings = (
      'Select;CheckBox;Yes;No')
    FieldDefs = <
      item
        Name = 'NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'LEVEL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOTES'
        DataType = ftMemo
      end
      item
        Name = 'KEY'
        DataType = ftString
        Size = 16
      end
      item
        Name = 'Type'
        DataType = ftString
        Size = 1
      end>
    Left = 180
    Top = 153
  end
  object evDataSource1: TevDataSource
    AutoEdit = False
    DataSet = csReports
    Left = 216
    Top = 152
  end
end
