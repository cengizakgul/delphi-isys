inherited rfPreProcessForQuarter: TrfPreProcessForQuarter
  inherited PC: TevPageControl
    ActivePage = tsReports
    inherited tsResults: TTabSheet
      object evDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 427
        Height = 176
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Consolidation;CheckBox;True;False')
        Selected.Strings = (
          'CoCustomNumber'#9'10'#9'Custom#'#9'F'
          'CoName'#9'40'#9'Name'#9'F'
          'Consolidation'#9'5'#9'Con'#9'F'
          'TimeStampStarted'#9'18'#9'Started'#9'F'
          'TimeStampFinished'#9'18'#9'Finished'#9'F'
          'Message'#9'100'#9'Message'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TrfPreProcessForQuarter\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = evDataSource1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object EvDBMemo1: TEvDBMemo
        Left = 0
        Top = 176
        Width = 427
        Height = 64
        Align = alBottom
        DataField = 'Message'
        DataSource = evDataSource1
        ReadOnly = True
        TabOrder = 1
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Width = 539
        Height = 288
      end
      inherited pWarnings: TevPanel
        Top = 288
        Width = 539
      end
    end
    inherited tsNotes: TTabSheet
      inherited memoNotes: TevMemo
        Width = 539
        Height = 288
      end
      inherited zevPanel2: TevPanel
        Top = 288
        Width = 539
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Height = 251
      end
    end
    object tsReports: TTabSheet
      Caption = 'Reports'
      ImageIndex = 5
      inline PreviewContainer: TrwPreviewContainer
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        OnResize = PreviewContainerResize
      end
    end
  end
  object evClientDataSet1: TevClientDataSet
    Left = 608
    Top = 24
    object evClientDataSet1CoCustomNumber: TStringField
      FieldName = 'CoCustomNumber'
      Size = 10
    end
    object evClientDataSet1CoName: TStringField
      FieldName = 'CoName'
      Size = 40
    end
    object evClientDataSet1Consolidation: TBooleanField
      FieldName = 'Consolidation'
    end
    object evClientDataSet1TimeStampStarted: TDateTimeField
      FieldName = 'TimeStampStarted'
    end
    object evClientDataSet1TimeStampFinished: TDateTimeField
      FieldName = 'TimeStampFinished'
    end
    object evClientDataSet1Message: TStringField
      FieldName = 'Message'
      Size = 1024
    end
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    Left = 640
    Top = 24
  end
end
