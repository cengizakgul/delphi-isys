// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RQEC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, EvTypes, SLogCheckReprint, EvConsts, EvContext,
  rwPreviewContainerFrm, ISBasicClasses, EvCommonInterfaces,
  EvStatisticsViewerFrm, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TrfQEC = class(TTaskResultFrame)
    PreviewContainer: TrwPreviewContainer;
    procedure PreviewContainerResize(Sender: TObject);
  private
    FResult: TrwReportResults;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
//    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); override;
  end;

implementation


{$R *.DFM}

{ TrfQEC }

constructor TrfQEC.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FResult := TrwReportResults.Create;

  if (Task as IevQECTask).ReportResults <> nil then
  begin
    (Task as IevQECTask).ReportResults.Position := 0;
    FResult.SetFromStream((Task as IevQECTask).ReportResults);
  end;

  memoWarnings.Text := (Task as IevQECTask).Warnings;
  tsWarnings.TabVisible := Length(memoWarnings.Text) > 0;

  CheckExceptions;

  Align := alClient;
  ctx_RWLocalEngine.Preview(FResult, PreviewContainer);
end;


destructor TrfQEC.Destroy;
begin
  FResult.Free;
  inherited;
end;

{
procedure TrfQEC.OnShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if tsResults.ControlCount > 0 then
    RWEngineInterface.PreviewShortCutNotify(TWinControl(tsResults.Controls[0]), Msg, Handled);
end;
}
procedure TrfQEC.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

end.
