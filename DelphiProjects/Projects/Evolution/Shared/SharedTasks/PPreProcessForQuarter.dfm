inherited pfPreProcessForQuarterParams: TpfPreProcessForQuarterParams
  inherited lNoParams: TLabel
    Top = 114
    Height = 152
  end
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 435
    Height = 114
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 16
      Top = 16
      Width = 142
      Height = 13
      Caption = 'Pick quarter to preprocess for:'
    end
    object evLabel2: TevLabel
      Left = 16
      Top = 44
      Width = 99
      Height = 13
      Caption = 'QEC adjustment limit:'
    end
    object lblTaxAdjLimit: TevLabel
      Left = 16
      Top = 89
      Width = 123
      Height = 13
      Caption = 'Tax Adjustment Threshold'
    end
    object evComboBox1: TevComboBox
      Left = 176
      Top = 16
      Width = 233
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = evComboBox1Change
    end
    object edQecLimit: TevEdit
      Left = 176
      Top = 44
      Width = 97
      Height = 21
      TabOrder = 1
      Text = '0.10'
    end
    object rgTaxServiceFilter: TevRadioGroup
      Left = 435
      Top = 16
      Width = 161
      Height = 80
      Caption = 'Tax Service Filter'
      Items.Strings = (
        'All'
        'Tax Companies Only'
        'Non-Tax Companies Only')
      TabOrder = 2
    end
    object chbPrintReports: TevCheckBox
      Left = 16
      Top = 66
      Width = 97
      Height = 17
      Caption = 'Print reports'
      TabOrder = 3
      OnClick = chbPrintReportsClick
    end
    object edTaxAdjLimit: TevEdit
      Left = 176
      Top = 89
      Width = 97
      Height = 21
      TabOrder = 4
      Text = '0.00'
      OnChange = edTaxAdjLimitChange
    end
  end
  object evPanel3: TevPanel
    Left = 0
    Top = 114
    Width = 435
    Height = 152
    Align = alClient
    Caption = 'evPanel3'
    TabOrder = 1
    object evPanel13: TevPanel
      Left = 1
      Top = 1
      Width = 433
      Height = 56
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      Visible = False
      object evLabel10: TevLabel
        Left = 5
        Top = 11
        Width = 104
        Height = 13
        Caption = 'Custom Client Number'
      end
      object evLabel11: TevLabel
        Left = 5
        Top = 35
        Width = 60
        Height = 13
        Caption = 'Client Name '
      end
      object evLabel14: TevLabel
        Left = 319
        Top = 35
        Width = 78
        Height = 13
        Caption = 'Company Name '
      end
      object evLabel13: TevLabel
        Left = 319
        Top = 11
        Width = 122
        Height = 13
        Caption = 'Custom Company Number'
      end
      object edCusClNbr: TevEdit
        Left = 123
        Top = 4
        Width = 185
        Height = 21
        TabOrder = 0
      end
      object edClName: TevEdit
        Left = 123
        Top = 27
        Width = 185
        Height = 21
        TabOrder = 1
      end
      object edCusCoNbr: TevEdit
        Left = 455
        Top = 5
        Width = 185
        Height = 21
        TabOrder = 2
      end
      object edCoName: TevEdit
        Left = 455
        Top = 29
        Width = 185
        Height = 21
        TabOrder = 3
      end
      object btnCompanyFilter: TevButton
        Left = 650
        Top = 24
        Width = 97
        Height = 25
        Caption = 'Company Filter'
        TabOrder = 4
        OnClick = btnCompanyFilterClick
        Color = clBlack
        Margin = 0
      end
    end
    object tsht2CompFilterGroup: TevGroupBox
      Left = 1
      Top = 57
      Width = 433
      Height = 27
      Align = alTop
      TabOrder = 1
      object rbAllCompanies: TevRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'All Companies'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbAllCompaniesClick
      end
      object rbSelectedCompanies: TevRadioButton
        Left = 208
        Top = 8
        Width = 121
        Height = 17
        Caption = 'Selected Companies'
        TabOrder = 1
        OnClick = rbSelectedCompaniesClick
      end
    end
    object grColist: TevDBCheckGrid
      Left = 1
      Top = 84
      Width = 433
      Height = 67
      DisableThemesInTitle = False
      Selected.Strings = (
        'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9'F'#9
        'CO_NAME'#9'40'#9'Company Name'#9'F'#9
        'CUSTOM_CLIENT_NUMBER'#9'20'#9'Custom Client Number'#9'F'#9
        'CL_NBR'#9'10'#9'Cl Nbr'#9'F'#9
        'CO_NBR'#9'10'#9'Co Nbr'#9'F'#9)
      IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
      IniAttributes.SectionName = 'TpfPreProcessForQuarterParams\grColist'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      OnMultiSelectRecord = grColistMultiSelectRecord
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsTMP_CO
      MultiSelectOptions = [msoShiftSelect]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      ReadOnly = True
      TabOrder = 2
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
  end
  object dsTMP_CO: TevDataSource
    DataSet = cdQueue
    Left = 682
    Top = 29
  end
  object cdQueue: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_RETURN_QUEUE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SYSTEM_TAX_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PERIOD_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'DUE_DATE'
        DataType = ftDate
      end
      item
        Name = 'RETURN_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SB_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CLIENT_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AGENCY_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SY_GLOBAL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_GL_AGENCY_REPORT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORTS_DESC'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'HIDDEN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRIORITY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'taken'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UserNumber'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_CLIENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONSOLIDATION_DESCR'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CONSOLIDATION_DESCR2'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CONSOLIDATION'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'KEEP_ONHOLD'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRIORITY2'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'HAVE_ACOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HAVE_CCOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HAVE_SCOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_REPORTS_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_CO_CONSOLIDATION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PRODUCE_ASCII_FILE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GlAgencyFOName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'HOLD_RETURN_QUEUE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_REPORT_WRITER_REPORTS_NBR'
        DataType = ftInteger
      end>
    Left = 696
    Top = 80
    object cdQueueCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Custom Company Number'
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdQueueCO_NAME: TStringField
      DisplayLabel = 'Company Name'
      DisplayWidth = 40
      FieldName = 'CO_NAME'
      Size = 40
    end
    object cdQueueCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Custom Client Number'
      DisplayWidth = 20
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdQueueCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdQueueCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
  end
end
