inherited rfProcessPR: TrfProcessPR
  inherited PC: TevPageControl
    OnChange = PCChange
    object tsMessages: TTabSheet [0]
      Caption = 'Messages'
      ImageIndex = 2
      object memoMessages: TevRichEdit
        Left = 0
        Top = 0
        Width = 435
        Height = 244
        Align = alClient
        ReadOnly = True
        TabOrder = 0
      end
    end
    inherited tsResults: TTabSheet
      inline PreviewContainer: TrwPreviewContainer
        Left = 0
        Top = 0
        Width = 427
        Height = 209
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        OnResize = PreviewContainerResize
      end
      object Panel1: TPanel
        Left = 0
        Top = 209
        Width = 427
        Height = 31
        Align = alBottom
        TabOrder = 1
        object ReprintChecksBtn: TButton
          Left = 1
          Top = 4
          Width = 89
          Height = 25
          Caption = 'Reprint checks'
          TabOrder = 0
          OnClick = ReprintChecksBtnClick
        end
      end
    end
  end
end
