// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RProcessManualACH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, SDataStructure, SLogCheckReprint, EvTypes,
  rwPreviewContainerFrm, ISBasicClasses, EvContext, EvCommonInterfaces,
  EvStatisticsViewerFrm, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type                                                      
  TrfProcessManualACH = class(TTaskResultFrame)
    rwPreviewContainer1: TrwPreviewContainer;
    PageControl: TevPageControl;
    tsAchFile: TTabSheet;
    tsAchReport: TTabSheet;
    tsAchDetailedReport: TTabSheet;
    ACHFilePreviewContainer: TrwPreviewContainer;
    ACHRegularReportPreviewContainer: TrwPreviewContainer;
    ACHDetailedReportPreviewContainer: TrwPreviewContainer;
    tsSave: TTabSheet;
  private
    FAchFileRwResult, FAchRegularReportRwResult, FAchDetailedReportRwResult: TrwReportResults;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.DFM}

uses
  EvStreamUtils, AscFileViewerFrame;

{ TrfACHBase }

constructor TrfProcessManualACH.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  t: IevProcessManualACHTask;
begin
  inherited;

  t := Task as IevProcessManualACHTask;

  if not StreamIsAssigned(t.AchFile) then
  begin
    FAchFileRwResult := nil;
    tsAchFile.TabVisible := False;
  end
  else
  begin
    FAchFileRwResult := TrwReportResults.Create;
    t.AchFile.Position := 0;
    FAchFileRwResult.SetFromStream(t.AchFile);
    if FAchFileRwResult.Count > 0 then
    begin
      ctx_RWLocalEngine.Preview(FAchFileRwResult, ACHFilePreviewContainer);
    end
    else
    begin
      FreeAndNil(FAchFileRwResult);
      tsAchFile.TabVisible := False;
    end;
  end;

  if not StreamIsAssigned(t.AchRegularReport) then
  begin
    FAchRegularReportRwResult := nil;
    tsAchReport.TabVisible := False;
  end
  else
  begin
    FAchRegularReportRwResult := TrwReportResults.Create;
    t.AchRegularReport.Position := 0;
    FAchRegularReportRwResult.SetFromStream(t.AchRegularReport);
    if FAchRegularReportRwResult.Count > 0 then
    begin
      ctx_RWLocalEngine.Preview(FAchRegularReportRwResult, ACHRegularReportPreviewContainer);
    end
    else
    begin
      FreeAndNil(FAchRegularReportRwResult);
      tsAchReport.TabVisible := False;
    end;
  end;

  if not StreamIsAssigned(t.AchDetailedReport) then
  begin
    FAchDetailedReportRwResult := nil;
    tsAchDetailedReport.TabVisible := False;
  end
  else
  begin
    FAchDetailedReportRwResult := TrwReportResults.Create;
    t.AchDetailedReport.Position := 0;
    FAchDetailedReportRwResult.SetFromStream(t.AchDetailedReport);
    if FAchDetailedReportRwResult.Count > 0 then
    begin
      ctx_RWLocalEngine.Preview(FAchDetailedReportRwResult, ACHDetailedReportPreviewContainer);
    end
    else
    begin
      FreeAndNil(FAchDetailedReportRwResult);
      tsAchDetailedReport.TabVisible := False;
    end;
  end;

  if not StreamIsAssigned(t.AchSave) then
    tsSave.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsSave, tsSave, t.AchSave);

  CheckExceptions;
  if memoExceptions.Text <> '' then
    PC.ActivePage := tsExceptions;
end;

destructor TrfProcessManualACH.Destroy;
begin
  if Assigned(FAchDetailedReportRwResult) then
    FreeAndNil(FAchDetailedReportRwResult);
  if Assigned(FAchFileRwResult) then
    FreeAndNil(FAchFileRwResult);
  if Assigned(FAchRegularReportRwResult) then
    FreeAndNil(FAchRegularReportRwResult);
  inherited;
end;

end.
