inherited ProcessQuarterReturnsParamFrame: TProcessQuarterReturnsParamFrame
  inherited lNoParams: TLabel
    Width = 577
    Height = 249
  end
  object evLabel1: TevLabel
    Left = 8
    Top = 16
    Width = 25
    Height = 13
    Caption = 'Year:'
  end
  object evRadioGroup1: TevRadioGroup
    Left = 8
    Top = 144
    Width = 185
    Height = 104
    Caption = 'Process Filter Options'
    Items.Strings = (
      'All'
      'Tax Companies Only'
      'Non-Tax Companies Only'
      'Tax Direct Companies Only')
    TabOrder = 0
    OnClick = SetModifiedTrue
  end
  object cbEeSort: TevRadioGroup
    Left = 392
    Top = 144
    Width = 185
    Height = 73
    Caption = 'Sort EE by'
    ItemIndex = 0
    Items.Strings = (
      'Last name'
      'SSN')
    TabOrder = 1
    OnClick = SetModifiedTrue
  end
  object cbSettingsCredit: TevRadioGroup
    Left = 200
    Top = 144
    Width = 185
    Height = 73
    Caption = 'If you have tax credit'
    ItemIndex = 0
    Items.Strings = (
      'Apply to next qtr.'
      'Ask for refund')
    TabOrder = 2
    OnClick = SetModifiedTrue
  end
  object evSpinEdit1: TevSpinEdit
    Left = 40
    Top = 16
    Width = 73
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 0
    OnChange = SetModifiedTrue
  end
  object evRadioGroup2: TevRadioGroup
    Left = 8
    Top = 48
    Width = 201
    Height = 73
    Caption = 'Quarter'
    Columns = 2
    Items.Strings = (
      'First'
      'Second'
      'Third'
      'Fourth/Annual')
    TabOrder = 4
    OnClick = SetModifiedTrue
  end
  object cbHideSSN: TevCheckBox
    Left = 208
    Top = 232
    Width = 137
    Height = 17
    Caption = 'Hide SSN'
    TabOrder = 5
  end
end
