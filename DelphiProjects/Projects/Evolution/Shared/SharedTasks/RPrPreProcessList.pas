// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RPrPreProcessList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, Buttons, ExtCtrls,
  EvCommonInterfaces, ISBasicClasses, EvStatisticsViewerFrm, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TrfPrPreprocessList = class(TTaskResultFrame)
    mResult: TevMemo;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
  end;

implementation

{$R *.DFM}

{ TrfPrQueueList }

constructor TrfPrPreprocessList.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  mResult.Lines.Text := (ATask as IevPreprocessPayrollTask).PRMessage;
end;

end.
 