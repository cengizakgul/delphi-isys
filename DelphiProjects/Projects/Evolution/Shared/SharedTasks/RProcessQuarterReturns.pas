// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RProcessQuarterReturns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, Db, Grids, Wwdbigrd,
  Wwdbgrid, Wwdatsrc,  Buttons, ExtCtrls, EvCommonInterfaces,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvStatisticsViewerFrm, EvUIComponents,
  EvClientDataSet, LMDCustomButton, LMDButton, isUILMDButton;

type
  TrfProcessTaxReturns = class(TTaskResultFrame)
    evClientDataSet1: TevClientDataSet;
    evDataSource1: TevDataSource;
    evDBGrid1: TevDBGrid;
    evClientDataSet1CompanyDesc: TStringField;
    evClientDataSet1ReportDesc: TStringField;
    evClientDataSet1Msg: TStringField;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
  end;

implementation


{$R *.DFM}

{ TrfProcessTaxReturns }

constructor TrfProcessTaxReturns.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  t: IevProcessTaxReturnsTask;
  i: Integer;
begin
  inherited;
  t := Task as IevProcessTaxReturnsTask;
  evClientDataSet1.CreateDataSet;
  evClientDataSet1.LogChanges := False;
  //evClientDataSet1.AddIndex('SORT', 'COMPDESC;REPORTDESC', []);
  //evClientDataSet1.IndexName := 'SORT';

  if t.Results <> nil then
    for i := 0 to t.Results.Count-1 do
      if t.Results.ValueExists('CompanyDesc') then
        evClientDataSet1.AppendRecord([t.Results.Value['CompanyDesc'], t.Results.Value['ReportDesc'],
          t.Results.Value['Message']]);

end;

end.
