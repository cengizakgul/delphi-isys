// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PPrCreateParams;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskParamFrame, StdCtrls,  SDataStructure, wwdblook,
  ExtCtrls, Variants, Buttons, DBCtrls, EvTypes, SDDClasses,
  ISBasicClasses, SDataDictclient, SDataDicttemp, EvContext, EvCommonInterfaces,
  EvUIUtils, EvUIComponents, isUIEdit, LMDCustomButton, LMDButton,
  isUILMDButton, isUIwwDBLookupCombo;

type
  TpfPrCreateParams = class(TTaskParamFrame)
    lckpCompany: TevDBLookupCombo;
    DM_TEMPORARY: TDM_TEMPORARY;
    lablCompany: TevLabel;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    memoComments: TevMemo;
    rgrpBlockAgencies: TevRadioGroup;
    rgrpBlockACH: TevRadioGroup;
    rgrpBlockBilling: TevRadioGroup;
    rgrpBlockTaxDeposits: TevRadioGroup;
    rgrpBlockChecks: TevRadioGroup;
    rgrpBlockTimeOff: TevRadioGroup;
    evGroupBox2: TevGroupBox;
    evLabel2: TevLabel;
    cbFrequencies: TevComboBox;
    rgrpPaySalary: TevRadioGroup;
    rgrpPayHours: TevRadioGroup;
    rgrpLoadDefaults: TevRadioGroup;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    lckpTemplate: TevDBLookupCombo;
    lckpFilter: TevDBLookupCombo;
    DM_COMPANY: TDM_COMPANY;
    cbAutoCommit: TevCheckBox;
    evGroupBox3: TevGroupBox;
    evGroupBox4: TevGroupBox;
    rbFileFromCommonDir: TevRadioButton;
    rbFileSpecified: TevRadioButton;
    edFileSource: TevEdit;
    evSpeedButton1: TevBitBtn;
    dFile: TOpenDialog;
    rgTcLookup: TevRadioGroup;
    rgTcDbdt: TevRadioGroup;
    rgTcFileFormat: TevRadioGroup;
    cbTcFourDigits: TevCheckBox;
    cbTcAutoImportJobCodes: TevCheckBox;
    cbtcUseEmployeePayRates: TevCheckBox;
    cbIncludeTimeOffRequests: TevCheckBox;
    procedure lckpCompanyChange(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure PopulateFrequency;
  public
    { Public declarations }
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
  end;

implementation

uses
  EvUtils, EvConsts, SFieldCodeValues, EvStreamUtils;

{$R *.DFM}

procedure TpfPrCreateParams.ApplyUpdates;
begin
  inherited;
  if lckpCompany.Text <> '' then
  begin
    (FTask as IevCreatePayrollTask).CLNbr := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger;
    (FTask as IevCreatePayrollTask).CONbr := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger;
    (FTask as IevCreatePayrollTask).CheckComments := memoComments.Text;
    (FTask as IevCreatePayrollTask).BlockAgencies := rgrpBlockAgencies.ItemIndex;

    //Issue#49987
    //If it is created a payroll in this module it will be regular type
    //Block401K(Mark Lia as Paid) is applicable  only Backdated Setup payroll type
    //So the value of Block401K(Mark Liab as Paid) should be a default value which is "NO"(radiogroupbox.itemindex = 1)
    if (FTask as IevCreatePayrollTask).Block401K = 0 then
      (FTask as IevCreatePayrollTask).Block401K := 1;

    (FTask as IevCreatePayrollTask).BlockACH := rgrpBlockACH.ItemIndex;
    (FTask as IevCreatePayrollTask).BlockBilling := rgrpBlockBilling.ItemIndex;
    (FTask as IevCreatePayrollTask).BlockTaxDeposits := rgrpBlockTaxDeposits.ItemIndex;
    (FTask as IevCreatePayrollTask).BlockChecks := rgrpBlockChecks.ItemIndex;
    (FTask as IevCreatePayrollTask).BlockTimeOff := rgrpBlockTimeOff.ItemIndex;
    (FTask as IevCreatePayrollTask).IncludeTimeOffRequests := cbIncludeTimeOffRequests.Checked;
    (FTask as IevCreatePayrollTask).CheckFreq := cbFrequencies.Text;
    (FTask as IevCreatePayrollTask).PaySalary := rgrpPaySalary.ItemIndex;
    (FTask as IevCreatePayrollTask).PayHours := rgrpPayHours.ItemIndex;
    (FTask as IevCreatePayrollTask).LoadDefaults := rgrpLoadDefaults.ItemIndex;
    (FTask as IevCreatePayrollTask).AutoCommit := cbAutoCommit.Checked;
    if lckpTemplate.Text <> '' then
      (FTask as IevCreatePayrollTask).PRTemplateNbr := DM_COMPANY.CO_PR_CHECK_TEMPLATES.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').AsInteger
    else
      (FTask as IevCreatePayrollTask).PRTemplateNbr := 0;
    if lckpFilter.Text <> '' then
      (FTask as IevCreatePayrollTask).PRFilterNbr := DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_PR_FILTERS_NBR').AsInteger
    else
      (FTask as IevCreatePayrollTask).PRFilterNbr := 0;
    (FTask as IevCreatePayrollTask).Caption := 'Create payroll for CO #' + DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString
      + ' - ' + DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString;
    if rbFileSpecified.Checked then
    begin
      if edFileSource.Text <> '' then
        (FTask as IevCreatePayrollTask).TcImportSourceFile := TEvDualStreamHolder.CreateFromFile(edFileSource.Text);
      (FTask as IevCreatePayrollTask).TcImportSource := '';
    end
    else
    begin
      (FTask as IevCreatePayrollTask).TcImportSourceFile := nil;
      (FTask as IevCreatePayrollTask).TcImportSource := edFileSource.Text;
    end;

    with (FTask as IevCreatePayrollTask) do
      case rgTcLookup.ItemIndex of
    0:
      TcLookupOption := loByNumber;
    1:
      TcLookupOption := loByName;
    2:
      TcLookupOption := loBySSN;
    end;
    (FTask as IevCreatePayrollTask).TcDBDTOption := TCImportDBDTOption(rgTcDbdt.ItemIndex);
    (FTask as IevCreatePayrollTask).TcFileFormat := TCFileFormatOption(rgTcFileFormat.ItemIndex);
    (FTask as IevCreatePayrollTask).TcFourDigitYear := cbTcFourDigits.Checked;
    (FTask as IevCreatePayrollTask).TcAutoImportJobCodes := cbTcAutoImportJobCodes.Checked;
    (FTask as IevCreatePayrollTask).TcUseEmployeePayRates := cbTcUseEmployeePayRates.Checked;
  end;
end;

constructor TpfPrCreateParams.Create(const AOwner: TComponent;
  const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  rbFileSpecified.Checked := Assigned((FTask as IevCreatePayrollTask).TcImportSourceFile);
  if rbFileSpecified.Checked then
    rbFileSpecified.Caption := rbFileSpecified.Caption+ ' (already loaded)';
  rbFileFromCommonDir.Checked := not rbFileSpecified.Checked;
  edFileSource.Text := (FTask as IevCreatePayrollTask).TcImportSource;
  with rgTcLookup do
    case (FTask as IevCreatePayrollTask).TcLookupOption of
    loByName:
      ItemIndex := 1;
    loByNumber:
      ItemIndex := 0;
    loBySSN:
      ItemIndex := 2;
    end;
  rgTcDbdt.ItemIndex := Integer((FTask as IevCreatePayrollTask).TcDBDTOption);
  rgTcFileFormat.ItemIndex := Integer((FTask as IevCreatePayrollTask).TcFileFormat);
  cbTcFourDigits.Checked := (FTask as IevCreatePayrollTask).TcFourDigitYear;
  cbTcAutoImportJobCodes.Checked := (FTask as IevCreatePayrollTask).TcAutoImportJobCodes;
  cbTcUseEmployeePayRates.Checked := (FTask as IevCreatePayrollTask).TcUseEmployeePayRates;
  //
  DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
  if DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([(FTask as IevCreatePayrollTask).CLNbr, (FTask as IevCreatePayrollTask).CoNbr]), []) then
  begin
    ctx_DataAccess.OpenClient((FTask as IevCreatePayrollTask).CLNbr);
    PopulateDataSets([DM_COMPANY.CO_PR_CHECK_TEMPLATES, DM_COMPANY.CO_PR_FILTERS]);
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_PR_CHECK_TEMPLATES, DM_COMPANY.CO_PR_FILTERS]);
    PopulateFrequency;
    lckpCompany.Text := DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString;
    memoComments.Text := (FTask as IevCreatePayrollTask).CheckComments;
    rgrpBlockAgencies.ItemIndex := (FTask as IevCreatePayrollTask).BlockAgencies;
    rgrpBlockACH.ItemIndex := (FTask as IevCreatePayrollTask).BlockACH;
    rgrpBlockBilling.ItemIndex := (FTask as IevCreatePayrollTask).BlockBilling;
    rgrpBlockTaxDeposits.ItemIndex := (FTask as IevCreatePayrollTask).BlockTaxDeposits;
    rgrpBlockChecks.ItemIndex := (FTask as IevCreatePayrollTask).BlockChecks;
    rgrpBlockTimeOff.ItemIndex := (FTask as IevCreatePayrollTask).BlockTimeOff;
    cbFrequencies.ItemIndex := cbFrequencies.Items.IndexOf((FTask as IevCreatePayrollTask).CheckFreq);
    rgrpPaySalary.ItemIndex := (FTask as IevCreatePayrollTask).PaySalary;
    cbIncludeTimeOffRequests.Checked := (FTask as IevCreatePayrollTask).IncludeTimeOffRequests;
    rgrpPayHours.ItemIndex := (FTask as IevCreatePayrollTask).PayHours;
    rgrpLoadDefaults.ItemIndex := (FTask as IevCreatePayrollTask).LoadDefaults;
    cbAutoCommit.Checked := (FTask as IevCreatePayrollTask).AutoCommit;
    if DM_COMPANY.CO_PR_CHECK_TEMPLATES.Locate('CO_PR_CHECK_TEMPLATES_NBR', (FTask as IevCreatePayrollTask).PRTemplateNbr, []) then
      lckpTemplate.Text := DM_COMPANY.CO_PR_CHECK_TEMPLATES.FieldByName('NAME').AsString;
    if DM_COMPANY.CO_PR_FILTERS.Locate('CO_PR_FILTERS_NBR', (FTask as IevCreatePayrollTask).PRFilterNbr, []) then
      lckpFilter.Text := DM_COMPANY.CO_PR_FILTERS.FieldByName('NAME').AsString;
  end;
end;

procedure TpfPrCreateParams.lckpCompanyChange(Sender: TObject);
begin
  inherited;
  PopulateFrequency;
end;

procedure TpfPrCreateParams.evSpeedButton1Click(Sender: TObject);
begin
  inherited;
  if rbFileFromCommonDir.Checked then
    edFileSource.Text := RunFolderDialog('Select common directory location', '')
  else
  if rbFileSpecified.Checked then
  begin
    if dFile.Execute then
      edFileSource.Text := dFile.FileName;
  end
  else
    Assert(False);
end;

procedure TpfPrCreateParams.Button1Click(Sender: TObject);
begin
  //
  inherited;
end;

procedure TpfPrCreateParams.PopulateFrequency;
var
  MyStrings: TStringList;
  I: Integer;
  Descr, CoFreq, Freq: String;
begin
  ctx_StartWait;
  with DM_TEMPORARY, DM_COMPANY do
  try
    ctx_DataAccess.OpenClient(TMP_CO.FieldByName('CL_NBR').AsInteger);
    CO.DataRequired('CO_NBR=' + TMP_CO.FieldByName('CO_NBR').AsString);
    CO_PR_CHECK_TEMPLATES.DataRequired('CO_NBR=' + TMP_CO.FieldByName('CO_NBR').AsString);
    CO_PR_FILTERS.DataRequired('CO_NBR=' + TMP_CO.FieldByName('CO_NBR').AsString);
    CoFreq := CO.FieldByName('PAY_FREQUENCIES').AsString;

    cbFrequencies.Items.Clear;
    if CoFreq = CO_FREQ_DAILY then
      cbFrequencies.Items.Add('Daily')
    else
    if CoFreq = CO_FREQ_QUARTERLY then
      cbFrequencies.Items.Add('Quarterly')
    else
    begin
      MyStrings := TStringList.Create;
      try
        MyStrings.Text := PayFrequencies_ComboChoices;
        for I := 0 to MyStrings.Count - 1 do
        begin
          Freq := MyStrings[I][Length(MyStrings[I])];
          if (FrequencyIn(FREQUENCY_TYPE_WEEKLY, Freq) and FrequencyIn(FREQUENCY_TYPE_WEEKLY, CoFreq) or not FrequencyIn(FREQUENCY_TYPE_WEEKLY, Freq)) and
          (FrequencyIn(FREQUENCY_TYPE_BIWEEKLY, Freq) and FrequencyIn(FREQUENCY_TYPE_BIWEEKLY, CoFreq) or not FrequencyIn(FREQUENCY_TYPE_BIWEEKLY, Freq)) and
          (FrequencyIn(FREQUENCY_TYPE_SEMI_MONTHLY, Freq) and FrequencyIn(FREQUENCY_TYPE_SEMI_MONTHLY, CoFreq) or not FrequencyIn(FREQUENCY_TYPE_SEMI_MONTHLY, Freq)) and
          (FrequencyIn(FREQUENCY_TYPE_MONTHLY, Freq) and FrequencyIn(FREQUENCY_TYPE_MONTHLY, CoFreq) or not FrequencyIn(FREQUENCY_TYPE_MONTHLY, Freq)) then
          begin
            Descr := ReturnDescription(PayFrequencies_ComboChoices, Freq);
            if cbFrequencies.Items.IndexOf(Descr) = -1 then
              cbFrequencies.Items.Add(Descr);
          end;
        end;
      finally
        MyStrings.Free;
      end;
    end;
    if cbFrequencies.Items.Count > 0 then
      cbFrequencies.ItemIndex := 0;
  finally
    ctx_EndWait;
  end;
end;

end.
