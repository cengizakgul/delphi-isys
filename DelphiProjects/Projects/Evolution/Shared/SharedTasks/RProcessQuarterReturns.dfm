inherited rfProcessTaxReturns: TrfProcessTaxReturns
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      object evDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        DisableThemesInTitle = False
        Selected.Strings = (
          'CompanyDesc'#9'16'#9'Companydesc'#9'F'
          'ReportDesc'#9'48'#9'Return'#9'F'
          'Msg'#9'256'#9'Message'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TrfProcessTaxReturns\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = evDataSource1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Width = 498
      end
      inherited pWarnings: TevPanel
        Width = 498
      end
    end
    inherited tsNotes: TTabSheet
      inherited memoNotes: TevMemo
        Width = 498
        Height = 203
      end
      inherited zevPanel2: TevPanel
        Top = 203
        Width = 498
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Width = 498
      end
    end
  end
  object evClientDataSet1: TevClientDataSet
    IndexName = 'SORT'
    IndexDefs = <
      item
        Name = 'SORT'
        Fields = 'COMPANYDESC;REPORTDESC'
      end>
    Left = 492
    Top = 14
    object evClientDataSet1CompanyDesc: TStringField
      DisplayWidth = 16
      FieldName = 'CompanyDesc'
      Size = 16
    end
    object evClientDataSet1ReportDesc: TStringField
      DisplayWidth = 64
      FieldName = 'ReportDesc'
      Size = 64
    end
    object evClientDataSet1Msg: TStringField
      DisplayWidth = 256
      FieldName = 'Msg'
      Size = 256
    end
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    Left = 492
    Top = 46
  end
end
