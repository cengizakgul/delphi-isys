inherited pfRebuildTempTables: TpfRebuildTempTables
  inherited lNoParams: TLabel
    Width = 481
    Height = 249
  end
  object Bevel1: TBevel
    Left = 16
    Top = 16
    Width = 465
    Height = 105
  end
  object lbTo: TevLabel
    Left = 232
    Top = 60
    Width = 9
    Height = 13
    Caption = 'to'
  end
  object cbFullRebuild: TevCheckBox
    Left = 24
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Full Rebuild'
    TabOrder = 0
    OnClick = cbFullRebuildClick
  end
  object cbUseRange: TevCheckBox
    Left = 24
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Use Range'
    TabOrder = 1
  end
  object cbUseNumbers: TevCheckBox
    Left = 24
    Top = 88
    Width = 121
    Height = 17
    Caption = 'Use Client Numbers'
    TabOrder = 2
  end
  object seFromClient: TevSpinEdit
    Left = 152
    Top = 56
    Width = 73
    Height = 22
    MaxValue = 10000000
    MinValue = 1
    TabOrder = 3
    Value = 1
  end
  object seToClient: TevSpinEdit
    Left = 248
    Top = 56
    Width = 73
    Height = 22
    MaxValue = 10000000
    MinValue = 1
    TabOrder = 4
    Value = 1
  end
  object edClientNumbers: TevEdit
    Left = 152
    Top = 88
    Width = 321
    Height = 21
    TabOrder = 5
  end
end
