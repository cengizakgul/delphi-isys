// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RProcessSBACH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, SDataStructure, SLogCheckReprint, EvTypes,
  rwPreviewContainerFrm, ISBasicClasses, EvContext, EvCommonInterfaces,
  EvStatisticsViewerFrm, evConsts, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TrfProcessSBACH = class(TTaskResultFrame)
    rwPreviewContainer1: TrwPreviewContainer;
    PageControl: TevPageControl;
    tsAchFile: TTabSheet;
    tsAchReport: TTabSheet;
    tsAchDetailedReport: TTabSheet;
    ACHFilePreviewContainer: TrwPreviewContainer;
    ACHRegularReportPreviewContainer: TrwPreviewContainer;
    ACHDetailedReportPreviewContainer: TrwPreviewContainer;
    tsSave: TTabSheet;
    NonAchReportPreviewContainer: TrwPreviewContainer;
    tsNonAchReport: TTabSheet;
    tsWTFile: TTabSheet;
    WTFilePreviewContainer: TrwPreviewContainer;
    procedure tsAchReportShow(Sender: TObject);
    procedure tsNonAchReportShow(Sender: TObject);
    procedure tsWTFileShow(Sender: TObject);
    procedure tsAchDetailedReportShow(Sender: TObject);
    procedure tsAchFileShow(Sender: TObject);
    procedure tsResultsShow(Sender: TObject);
  private
    FAchFileVisible: Boolean;
    FAchDetailedReportVisible: Boolean;
    FWTFileVisible: Boolean;
    FNonAchReportVisible: Boolean;
    FAchRegularReportVisible: Boolean;
    FAchFileRwResult, FAchRegularReportRwResult, FAchDetailedReportRwResult,
    FNonAchReportRwResult, FWTFileRwResult: TrwReportResults;
    FTask: IevCashManagementTask;    
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.DFM}

uses
  EvStreamUtils, AscFileViewerFrame;

{ TrfACHBase }

constructor TrfProcessSBACH.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;

  FTask := Task as IevCashManagementTask;

  FAchFileVisible := False;
  FAchDetailedReportVisible := False;
  FWTFileVisible := False;
  FNonAchReportVisible := False;
  FAchRegularReportVisible := False;

  if not StreamIsAssigned(FTask.AchFile) then
  begin
    FAchFileRwResult := nil;
    tsAchFile.TabVisible := False;
  end;

  if not StreamIsAssigned(FTask.AchRegularReport) then
  begin
    FAchRegularReportRwResult := nil;
    tsAchReport.TabVisible := False;
  end;

  if not StreamIsAssigned(FTask.NonAchReport) then
  begin
    FNonAchReportRwResult := nil;
    tsNonAchReport.TabVisible := False;
  end;

  if not StreamIsAssigned(FTask.WTFile) then
  begin
    FWTFileRwResult := nil;
    tsWTFile.TabVisible := False;
  end;

  if not StreamIsAssigned(FTask.AchDetailedReport) then
  begin
    FAchDetailedReportRwResult := nil;
    tsAchDetailedReport.TabVisible := False;
  end;

  if not StreamIsAssigned(FTask.AchSave) then
    tsSave.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsSave, tsSave, FTask.AchSave);

  CheckExceptions;
  if memoExceptions.Text <> '' then
    PC.ActivePage := tsExceptions
  else
  if memoWarnings.Text <> '' then
    PC.ActivePage := tsWarnings
  else
    tsAchFileShow(nil);

end;

destructor TrfProcessSBACH.Destroy;
begin
  if Assigned(FAchFileRwResult) then
    FreeAndNil(FAchFileRwResult);

  if Assigned(FAchRegularReportRwResult) then
    FreeAndNil(FAchRegularReportRwResult);

  if Assigned(FAchDetailedReportRwResult) then
    FreeAndNil(FAchDetailedReportRwResult);

  if Assigned(FNonAchReportRwResult) then
    FreeAndNil(FNonAchReportRwResult);

  if Assigned(FWTFileRwResult) then
    FreeAndNil(FWTFileRwResult);

  inherited;
end;

procedure TrfProcessSBACH.tsAchReportShow(Sender: TObject);
begin
  inherited;
  if Assigned(FTask) then
  begin
    if StreamIsAssigned(FTask.AchRegularReport) and not FAchRegularReportVisible then
    begin
      FAchRegularReportVisible := True;
      FAchRegularReportRwResult := TrwReportResults.Create;
      FTask.AchRegularReport.Position := 0;
      FAchRegularReportRwResult.SetFromStream(FTask.AchRegularReport);
      if FAchRegularReportRwResult.Count > 0 then
      begin
        ctx_RWLocalEngine.Preview(FAchRegularReportRwResult, ACHRegularReportPreviewContainer);
      end
      else
      begin
        FreeAndNil(FAchRegularReportRwResult);
        tsAchReport.TabVisible := False;
      end;
    end;
  end;
end;

procedure TrfProcessSBACH.tsNonAchReportShow(Sender: TObject);
begin
  inherited;
  if Assigned(FTask) then
  begin
    if StreamIsAssigned(FTask.NonAchReport) and not FNonAchReportVisible then
    begin
      FNonAchReportVisible := True;
      FNonAchReportRwResult := TrwReportResults.Create;
      FTask.NonAchReport.Position := 0;
      FNonAchReportRwResult.SetFromStream(FTask.NonAchReport);
      if FNonAchReportRwResult.Count > 0 then
      begin
        ctx_RWLocalEngine.Preview(FNonAchReportRwResult, NonAchReportPreviewContainer);
      end
      else
      begin
        FreeAndNil(FNonAchReportRwResult);
        tsNonAchReport.TabVisible := False;
      end;
    end;
  end;
end;

procedure TrfProcessSBACH.tsWTFileShow(Sender: TObject);
begin
  inherited;
  if Assigned(FTask) then
  begin
    if StreamIsAssigned(FTask.WTFile) and not FWTFileVisible then
    begin
      FWTFileVisible := True;
      FWTFileRwResult := TrwReportResults.Create;
      FTask.WTFile.Position := 0;
      FWTFileRwResult.SetFromStream(FTask.WTFile);
      if FWTFileRwResult.Count > 0 then
      begin
        ctx_RWLocalEngine.Preview(FWTFileRwResult, WTFilePreviewContainer);
      end
      else
      begin
        FreeAndNil(FWTFileRwResult);
        tsWTFile.TabVisible := False;
      end;
    end;
  end;
end;

procedure TrfProcessSBACH.tsAchDetailedReportShow(Sender: TObject);
begin
  inherited;
  if Assigned(FTask) then
  begin
    if StreamIsAssigned(FTask.AchDetailedReport) and not FAchDetailedReportVisible then
    begin
      FAchDetailedReportVisible := True;
      FAchDetailedReportRwResult := TrwReportResults.Create;
      FTask.AchDetailedReport.Position := 0;
      FAchDetailedReportRwResult.SetFromStream(FTask.AchDetailedReport);
      if FAchDetailedReportRwResult.Count > 0 then
      begin
        ctx_RWLocalEngine.Preview(FAchDetailedReportRwResult, ACHDetailedReportPreviewContainer);
      end
      else
      begin
        FreeAndNil(FAchDetailedReportRwResult);
        tsAchDetailedReport.TabVisible := False;
      end;
    end;
  end;
end;

procedure TrfProcessSBACH.tsAchFileShow(Sender: TObject);
begin
  inherited;
  if Assigned(FTask) then
  begin
    if StreamIsAssigned(FTask.AchFile) and not FAchFileVisible then
    begin
      FAchFileVisible := True;
      FAchFileRwResult := TrwReportResults.Create;
      FTask.AchFile.Position := 0;
      FAchFileRwResult.SetFromStream(FTask.AchFile);
      if FAchFileRwResult.Count > 0 then
      begin
        ctx_RWLocalEngine.Preview(FAchFileRwResult, ACHFilePreviewContainer);
      end
      else
      begin
        FreeAndNil(FAchFileRwResult);
        tsAchFile.TabVisible := False;
      end;
    end;
  end;
end;

procedure TrfProcessSBACH.tsResultsShow(Sender: TObject);
begin
  inherited;
  if not Assigned(PageControl.ActivePage) then
    PageControl.ActivePageIndex := 0;
  if Assigned(PageControl.ActivePage.OnShow) then
    PageControl.ActivePage.OnShow(Self);
end;

end.
