// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RPayTaxes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, SReportSettings,
  Buttons, ExtCtrls, EvExceptions, EvStreamUtils, ISBasicClasses, EvContext,
  rwPreviewContainerFrm, EvCommonInterfaces, isVCLBugFix, EvTypes,
  EvStatisticsViewerFrm, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, EvDataset;

type
  TPayTaxesReturnsResultFrame = class(TTaskResultFrame)
    PageControl: TevPageControl;
    tsChecks: TTabSheet;
    tsAch: TTabSheet;
    tsEFTP: TTabSheet;
    tsNyDebit: TTabSheet;
    evPanel2: TevPanel;
    bPrint: TevButton;
    tsAchReport: TTabSheet;
    tsUtils: TTabSheet;
    Memo1: TMemo;
    bRollback: TevButton;
    tsMaDebit: TTabSheet;
    tsCaDebit: TTabSheet;
    bMarkPaid: TevButton;
    CheckPreviewContainer: TrwPreviewContainer;
    ACHPreviewContainer: TrwPreviewContainer;
    procedure bPrintClick(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure bRollbackClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure bMarkPaidClick(Sender: TObject);
    procedure CheckPreviewContainerResize(Sender: TObject);
    procedure ACHPreviewContainerResize(Sender: TObject);
  private
    { Private declarations }
    FChecksRwResult, FAchRwResult: TrwReportResults;
    procedure RollBack;
    procedure OnEftpsSaveToFile(const Strings: TStringList; const FileName: string);
  public
    { Public declarations }
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
  end;

implementation

{$R *.DFM}

uses EvUtils, AscFileViewerFrame, SSecurityInterface, Db, EvConsts, SDataStructure, DateUtils,
     EvDataAccessComponents;

const
  RollbackCompletedMsg = 'This payment is completely rolled back';

{ TPayTaxesReturnsResultFrame }

constructor TPayTaxesReturnsResultFrame.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
var
  i, j: Integer;
  t: IevProcessTaxPaymentsTask;
  l, l2: TStringList;
  ts: TTabSheet;
  sql: String;
  Q: IevQuery;
begin
  inherited;

  t := Task as IevProcessTaxPaymentsTask;
  if StreamIsAssigned(t.Checks) or StreamIsAssigned(t.OneMiscCheck) or StreamIsAssigned(t.OneMiscCheckReport)then
  begin
    FChecksRwResult := TrwReportResults.Create;

    if StreamIsAssigned(t.Checks) then
    begin
     t.Checks.Position := 0;
     FChecksRwResult.SetFromStream(t.Checks);
    end;
    if StreamIsAssigned(t.OneMiscCheck) then
    begin
     t.OneMiscCheck.Position := 0;
     if FChecksRwResult.Count > 0 then
       FChecksRwResult.AddReportResults(t.OneMiscCheck)
     else
       FChecksRwResult.SetFromStream(t.OneMiscCheck);
    end;
    if StreamIsAssigned(t.OneMiscCheckReport) then
    begin
     t.OneMiscCheckReport.Position := 0;
     if FChecksRwResult.Count > 0 then
       FChecksRwResult.AddReportResults(t.OneMiscCheckReport)
     else
       FChecksRwResult.SetFromStream(t.OneMiscCheckReport);
    end;

    if FChecksRwResult.Count > 0 then
    begin
      sql :='select a.SY_REPORT_WRITER_REPORTS_NBR, c.NAME ' +
                  'from SY_REPORT_WRITER_REPORTS a, SY_REPORTS b, SY_REPORTS_GROUP c ' +
                      'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                           'a.REPORT_TYPE = ''T'' and ' +
                           'a.SY_REPORT_WRITER_REPORTS_NBR = b.SY_REPORT_WRITER_REPORTS_NBR and ' +
                           'b.SY_REPORTS_GROUP_NBR  = c.SY_REPORTS_GROUP_NBR ' +
                           'order by a.SY_REPORT_WRITER_REPORTS_NBR ';
      Q := TevQuery.Create(sql);
      Q.Execute;
      Q.Result.IndexFieldNames := 'SY_REPORT_WRITER_REPORTS_NBR';

      for i := 0 to FChecksRwResult.Count - 1 do
        if Q.Result.Locate('SY_REPORT_WRITER_REPORTS_NBR', FChecksRwResult.Items[i].NBR, []) then
           FChecksRwResult.Items[i].ReportName := Q.Result.FieldByName('NAME').AsString;

      ctx_RWLocalEngine.Preview(FChecksRwResult, CheckPreviewContainer);
    end
    else
    begin
      FreeAndNil(FChecksRwResult);
      tsChecks.HandleNeeded;
      tsChecks.TabVisible := False;
    end;
  end
  else
  begin
    FChecksRwResult := nil;
    tsChecks.HandleNeeded;
    tsChecks.TabVisible := False;
  end;

  if StreamIsAssigned(t.UnivDebitFile) then
  begin
    l := TStringList.Create;
    l2 := TStringList.Create;
    try
      l.LoadFromStream(t.UnivDebitFile.RealStream);
      i := 0;
      while i < l.Count do
      begin
        ts := TTabSheet.Create(PageControl);
        ts.PageControl := PageControl;
        ts.Caption := l[i];
        ts.PageIndex := 0;
        ts.TabVisible := True;
        Inc(i);
        Inc(i); // skipping report description
        j := StrToInt(l[i]);
        Inc(i);
        l2.Clear;
        while j > 0 do
        begin
          l2.Append(l[i]);
          Inc(i);
          Dec(j);
        end;
        TAscFileViewer.CreateExt(ts, ts, l2);
      end;
    finally
      l.Free;
      l2.Free;
    end;
  end;
  if not StreamIsAssigned(t.AchReport) then
  begin
    FAchRwResult := nil;
    tsAchReport.TabVisible := False;
  end
  else
  begin
    FAchRwResult := TrwReportResults.Create;
    t.AchReport.Position := 0;
    FAchRwResult.SetFromStream(t.AchReport);
    if FAchRwResult.Count > 0 then
    begin
      ctx_RWLocalEngine.Preview(FAchRwResult, ACHPreviewContainer);
    end
    else
    begin
      FreeAndNil(FAchRwResult);
      tsAchReport.TabVisible := False;
    end;
  end;
  if not StreamIsAssigned(t.AchFile) then
    tsAch.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsAch, tsAch, t.AchFile);
  if not StreamIsAssigned(t.EftFile) then
    tsEFTP.TabVisible := False
  else
    with TAscFileViewer.CreateExt(tsEFTP, tsEFTP, t.EftFile) do
      OnSaveToFile := OnEftpsSaveToFile;
  if not StreamIsAssigned(t.NyDebitFile) then
    tsNyDebit.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsNyDebit, tsNyDebit, t.NyDebitFile);
  if not StreamIsAssigned(t.MaDebitFile) then
    tsMaDebit.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsMaDebit, tsMaDebit, t.MaDebitFile);
  if not StreamIsAssigned(t.CaDebitFile) then
    tsCaDebit.TabVisible := False
  else
    TAscFileViewer.CreateExt(tsCaDebit, tsCaDebit, t.CaDebitFile);
  for i := 0 to PageControl.PageCount-1 do
    if PageControl.Pages[i].TabVisible then
    begin
      PageControl.ActivePage := PageControl.Pages[i];
      Break;
    end;

  if PageControl.ActivePage = tsUtils then
    bRollback.Enabled := (ctx_AccountRights.Functions.GetState('ROLLBACK_TAX_PAYMENTS') = stEnabled) and
                          ctx_DataAccess.CheckSbTaxPaymentstatus((Task as IevProcessTaxPaymentsTask).SbTaxPaymentNbr);
end;


destructor TPayTaxesReturnsResultFrame.Destroy;
begin
  inherited;
  FChecksRwResult.Free;
end;

procedure TPayTaxesReturnsResultFrame.bPrintClick(Sender: TObject);
const
  PrintMessageText = 'Checks has been printed';
var
  lResults: TrwReportResults;
begin
  inherited;
  if Assigned(FChecksRwResult) then
  begin
    lResults := TrwReportResults.Create;
    try
      lResults.Assign(FChecksRwResult);
      if Pos(PrintMessageText, (Task as IevProcessTaxPaymentsTask).Notes) > 0 then
      begin
        if  ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_MISC_CHECKS') = stEnabled then
        begin
          ctx_RWLocalEngine.Print(lResults);
          AddNoteToTask('Checks has been reprinted');
        end
        else
          raise ENoRights.CreateHelp('You do not have rights to reprint misc. checks', IDH_SecurityViolation);
      end
      else
      begin
        ctx_RWLocalEngine.Print(lResults);
        AddNoteToTask(PrintMessageText);
      end;
    finally
      lResults.Free;
    end
  end;
end;

procedure TPayTaxesReturnsResultFrame.evButton1Click(Sender: TObject);
begin
  inherited;
  ;
end;

procedure TPayTaxesReturnsResultFrame.bRollbackClick(Sender: TObject);
begin
  inherited;
  RollBack;
end;

procedure TPayTaxesReturnsResultFrame.PageControlChange(Sender: TObject);
begin
  inherited;
    if PageControl.ActivePage = tsUtils then
    bRollback.Enabled := (ctx_AccountRights.Functions.GetState('ROLLBACK_TAX_PAYMENTS') = stEnabled) and
                          ctx_DataAccess.CheckSbTaxPaymentstatus((Task as IevProcessTaxPaymentsTask).SbTaxPaymentNbr);
end;

procedure TPayTaxesReturnsResultFrame.RollBack;
const
  woMessage = 'Rolling back. Please wait...';

begin
  EvMessage('Confirm due dates for threshold based deposits. The liability''s due date and period end dates may need to be adjusted.');
  ctx_StartWait(woMessage, (Task as IevProcessTaxPaymentsTask).Params.Count);
  ctx_DataAccess.UnlockPRSimple;
  try
    try
       ctx_UpdateWait(woMessage, 0);
       if (Task as IevProcessTaxPaymentsTask).SbTaxPaymentNbr > 0 then
         bRollback.Enabled := not ctx_DataAccess.TaxPaymentRollback((Task as IevProcessTaxPaymentsTask).SbTaxPaymentNbr, 0);
    except
      AddNoteToTask('This payment may be partially rolled back');
      raise;
    end
  finally
    ctx_EndWait;
    ctx_DataAccess.LockPRSimple;
  end;
end;

procedure TPayTaxesReturnsResultFrame.bMarkPaidClick(Sender: TObject);
  function GetDepositNbr: Integer;
  begin
    if DM_COMPANY.CO_TAX_DEPOSITS.RecordCount = 0 then
    begin
      DM_COMPANY.CO_TAX_DEPOSITS.Append;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := 'Fix for v51 problem';
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('PR_NBR').Clear;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('DEPOSIT_TYPE').AsString := TAX_DEPOSIT_TYPE_NOTICE;
      DM_COMPANY.CO_TAX_DEPOSITS.Post;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_DEPOSITS]);
    end;
    Result := DM_COMPANY.CO_TAX_DEPOSITS.CO_TAX_DEPOSITS_NBR.AsInteger;
  end;
  procedure Process(const ds: TevClientDataSet; const Filter: string);
  var
    fs, fd: TField;
  begin
    if Filter <> '' then
    begin
      ds.DisableControls;
      try
        ds.Close;
        ds.Filtered := False;
        ds.IndexName := '';
        ds.DataRequired(Filter);
        if ds.RecordCount > 0 then
        begin
          fs := ds.FieldByName('STATUS');
          fd := ds.FieldByName('CO_TAX_DEPOSITS_NBR');
          ds.First;
          while not ds.Eof do
          begin
            if (fs.AsString <> TAX_DEPOSIT_STATUS_PAID)
            and (fs.AsString <> TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
            and (fs.AsString <> TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE) then
            begin
              ds.Edit;
              fs.AsString := TAX_DEPOSIT_STATUS_PAID;
              ds.Post;
            end;
            if fd.IsNull then
            begin
              ds.Edit;
              fd.AsInteger := GetDepositNbr;
              ds.Post;
            end;
            ds.Next;
          end;
          ctx_DataAccess.PostDataSets([ds]);
        end;
      finally
        ds.EnableControls;
      end;
    end;
  end;
const
  woMessage = 'Marking as Paid. Please wait...';
var
  s: string;
  i: Integer;
begin
  inherited;
  if EvDialog('This function is password protected', 'Password:', s, True) then
    if UpperCase(s) <> 'LONDON1959' then
      raise EevException.Create('Incorrect password')
    else
    begin
      ctx_StartWait(woMessage, (Task as IevProcessTaxPaymentsTask).Params.Count);
      try
        DM_TEMPORARY.TMP_CL.DataRequired('');
        DM_TEMPORARY.TMP_CL.IndexFieldNames := 'CL_NBR';
        for i := 0 to (Task as IevProcessTaxPaymentsTask).Params.Count-1 do
        begin
          Assert(DM_TEMPORARY.TMP_CL.FindKey([(Task as IevProcessTaxPaymentsTask).Params[i].ClientID]));
          if Pos('Client '+ DM_TEMPORARY.TMP_CL.CUSTOM_CLIENT_NUMBER.AsString+ ':', (Task as IevProcessTaxPaymentsTask).Exceptions) > 0 then
            EvMessage('Client '+ DM_TEMPORARY.TMP_CL.CUSTOM_CLIENT_NUMBER.AsString+ ' has been skipped as it has errors in exceptions log')
          else
          begin
            ctx_UpdateWait(woMessage, i);
            ctx_DataAccess.OpenClient((Task as IevProcessTaxPaymentsTask).Params[i].ClientID);
            ctx_DataAccess.StartNestedTransaction([dbtClient]);
            try
              DM_COMPANY.CO_TAX_DEPOSITS.Close;
              DM_COMPANY.CO_TAX_DEPOSITS.DataRequired('CO_TAX_DEPOSITS_NBR=-1');
              Process(DM_COMPANY.CO_FED_TAX_LIABILITIES, (Task as IevProcessTaxPaymentsTask).Params[i].FedTaxLiabilityList);
              Process(DM_COMPANY.CO_STATE_TAX_LIABILITIES, (Task as IevProcessTaxPaymentsTask).Params[i].StateTaxLiabilityList);
              Process(DM_COMPANY.CO_SUI_LIABILITIES, (Task as IevProcessTaxPaymentsTask).Params[i].SUITaxLiabilityList);
              Process(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, (Task as IevProcessTaxPaymentsTask).Params[i].LocalTaxLiabilityList);
              ctx_DataAccess.CommitNestedTransaction;
            except
              ctx_DataAccess.RollbackNestedTransaction;
              raise;
            end;
          end;
        end;
      finally
        ctx_EndWait;
      end;
    end;
end;

procedure TPayTaxesReturnsResultFrame.OnEftpsSaveToFile(
  const Strings: TStringList; const FileName: string);
var
  i: Integer;
  sSeqNumber: string;
  sTempFileName: string;
  TargetFile: System.Text;
begin
  if Strings.Count > 750 then
  begin
    EvMessage('Your EFTP file is greater than 750 lines.'#13'It will be split into several files with Filer Sequence Number added to file names.');
    sSeqNumber := '';
    for i := 0 to Strings.Count-1 do
    begin
      if sSeqNumber <> Copy(Strings[i], 22, 3) then
      begin
        if sSeqNumber <> '' then
          CloseFile(TargetFile);
        sSeqNumber := Copy(Strings[i], 22, 3);
        sTempFileName := ChangeFileExt(FileName, '_'+ sSeqNumber+ ExtractFileExt(FileName));
        if FileExists(sTempFileName) then
          if EvMessage('File '+ sTempFileName+ ' already exists and will be overwritten. Continue?',
            mtConfirmation,  mbOKCancel, mbCancel) <> mrOK then
            AbortEx;
        AssignFile(TargetFile, sTempFileName);
        Rewrite(TargetFile);
      end;
      Writeln(TargetFile, Strings[i]);
    end;
    if sSeqNumber <> '' then
      CloseFile(TargetFile);
  end
  else
    Strings.SaveToFile(FileName);
end;

procedure TPayTaxesReturnsResultFrame.CheckPreviewContainerResize(Sender: TObject);
begin
  inherited;
  CheckPreviewContainer.FrameResize(Sender);
end;

procedure TPayTaxesReturnsResultFrame.ACHPreviewContainerResize(
  Sender: TObject);
begin
  inherited;
  ACHPreviewContainer.FrameResize(Sender);
end;

end.
