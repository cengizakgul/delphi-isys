inherited rfPAMC: TrfPAMC
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      inline PreviewContainer: TrwPreviewContainer
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        OnResize = PreviewContainerResize
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Width = 516
        Height = 243
      end
      inherited pWarnings: TevPanel
        Top = 243
        Width = 516
      end
    end
    inherited tsNotes: TTabSheet
      inherited memoNotes: TevMemo
        Width = 516
        Height = 243
      end
      inherited zevPanel2: TevPanel
        Top = 243
        Width = 516
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Width = 516
        Height = 284
      end
    end
  end
end
