// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PAcaUpdate;

interface

uses
  Windows, EvTaskParamFrame, Classes, Controls, Grids, Wwdbigrd, Wwdbgrid, SysUtils,
   EvUtils, SDataStructure, EvConsts, Db, Wwdatsrc, EvTypes,
  SDDClasses, ISBasicClasses, SDataDicttemp, EvCommonInterfaces, StdCtrls, EvExceptions, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, ExtCtrls, isUIEdit,

  EvBasicUtils,Variants, EvContext, Graphics,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, isBaseClasses;

type
  TpfAcaUpdate = class(TTaskParamFrame)
    evPanel3: TevPanel;
    evPanel13: TevPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evLabel14: TevLabel;
    evLabel13: TevLabel;
    edCusClNbr: TevEdit;
    edClName: TevEdit;
    edCusCoNbr: TevEdit;
    edCoName: TevEdit;
    btnCompanyFilter: TevButton;
    tsht2CompFilterGroup: TevGroupBox;
    rbAllCompanies: TevRadioButton;
    rbSelectedCompanies: TevRadioButton;
    grColist: TevDBCheckGrid;
    cdQueue: TevClientDataSet;
    cdQueueCUSTOM_COMPANY_NUMBER: TStringField;
    cdQueueCO_NAME: TStringField;
    cdQueueCUSTOM_CLIENT_NUMBER: TStringField;
    cdQueueCL_NBR: TIntegerField;
    cdQueueCO_NBR: TIntegerField;
    dsTMP_CO: TevDataSource;
    procedure grColistMultiSelectRecord(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure rbAllCompaniesClick(Sender: TObject);
    procedure FtaskToScreen;
    procedure btnCompanyFilterClick(Sender: TObject);
    procedure PrepareCompanyList;
    procedure rbSelectedCompaniesClick(Sender: TObject);
    procedure SetGridColor;

  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
    procedure CancelUpdates; override;
  end;

implementation

{$R *.DFM}

{ TpfAcaUpdate }

procedure TpfAcaUpdate.ApplyUpdates;
var
  t: IevACAUpdateTask;
  i : integer;
  cd: TEvClientDataSet;
  tmpClNbr: integer;
  cfilter : IisStringList;
begin
  inherited;

  t := FTask as IevACAUpdateTask;
  t.Filter.Clear;

  if rbAllCompanies.Checked then
     grColist.UnSelectAll;

  cd := TevClientDataSet.Create(Self);
  try
    cd.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
    cd.FieldDefs.Add('CO_NBR', ftInteger, 0, True);
    cd.CreateDataSet;
    cd.LogChanges := False;
    cd.IndexFieldNames:= 'CL_NBR;CO_NBR';
    for i := 0 to grColist.SelectedList.Count - 1 do
    Begin
     cdQueue.GotoBookmark(grColist.SelectedList[i]);
     cd.AppendRecord([cdQueue['CL_NBR'],  cdQueue['CO_NBR']]);
    end;

    cfilter:= TisStringList.Create;

    cd.First;
    tmpClNbr:=cd.FieldByName('CL_NBR').AsInteger;
    while not cd.Eof do
    begin
     if tmpClNbr <> cd.FieldByName('CL_NBR').AsInteger then
     begin
      if trim(cfilter.CommaText) <> '' then
        with t.Filter.Add do
        begin
         ClNbr := tmpClNbr;
         CoFilter := cfilter.CommaText;
        end;
      cfilter.Clear;
      tmpClNbr:=cd['CL_NBR'];
      cfilter.Add(cd.FieldByName('CO_NBR').AsString);
     end
     else
       cfilter.Add(cd.FieldByName('CO_NBR').AsString);

      cd.Next;
    end;

    if tmpClNbr = cd.FieldByName('CL_NBR').AsInteger then
    begin
     if trim(cfilter.CommaText) <> '' then
       with t.Filter.Add do
       begin
        ClNbr := tmpClNbr;
        CoFilter := cfilter.CommaText;
       end;
    end;

  finally
    cd.Free;
  end;

end;

procedure TpfAcaUpdate.CancelUpdates;
begin
  inherited;
  FtaskToScreen;
end;

procedure TpfAcaUpdate.FtaskToScreen;
var
  t: IevACAUpdateTask;
  i,j: Integer;
  cfilter : IisStringList;
begin
  inherited;
  t := FTask as IevACAUpdateTask;
  cfilter:= TisStringList.Create;

  if not cdQueue.Active then
     PrepareCompanyList;
  grCoList.UnSelectAll;
  if t.Filter.Count > 0 then
  begin

    for i := 0 to t.Filter.Count -1 do
    begin
      cfilter.CommaText := t.Filter[i].CoFilter;

      for j := 0 to cfilter.Count -1 do
        if cdQueue.Locate('CL_NBR;CO_NBR', VarArrayOf([t.Filter[i].ClNbr, cfilter[j]]), []) then
         grCoList.SelectedList.Add(cdQueue.GetBookmark);


    end;
   rbSelectedCompanies.Checked := True;
  end
  else
  begin
   rbAllCompanies.Checked := True;
   grCoList.Enabled := cdQueue.Active and (cdQueue.RecordCount > 0);
  end;
end;

constructor TpfAcaUpdate.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FtaskToScreen;
end;

procedure TpfAcaUpdate.grColistMultiSelectRecord(
  Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_TASK.State = dsBrowse then
    DM_SERVICE_BUREAU.SB_TASK.Edit;
end;

procedure TpfAcaUpdate.rbAllCompaniesClick(
  Sender: TObject);
begin
  inherited;
  SetGridColor;
end;

procedure TpfAcaUpdate.rbSelectedCompaniesClick(
  Sender: TObject);
begin
  inherited;
  if not cdQueue.Active then
     PrepareCompanyList;
  SetGridColor;
end;

procedure TpfAcaUpdate.PrepareCompanyList;
var
 Condition, CheckAnd, ReturnsQueue : String;
Begin
  Condition :='';
  CheckAnd := '';
  if Length(Trim(edCusClNbr.Text))> 0 then
     begin
      Condition := Condition + ' LOWER(t1.Custom_client_Number) like LOWER('''+'%'+Trim(edCusClNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edClName.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t1.Name) like LOWER('''+'%'+Trim(edClName.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edCusCoNbr.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t2.Custom_company_Number) like LOWER('''+'%'+Trim(edCusCoNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;
  if Length(Trim(edCoName.Text))> 0 then
      Condition := Condition + CheckAnd + ' LOWER(t2.Name) like LOWER('''+'%'+Trim(edCoName.Text)+ '%'''+')';

  ReturnsQueue :=
     ' select distinct t2.cl_nbr, t2.co_nbr,  t2.name co_name, t2.CUSTOM_COMPANY_NUMBER,  t1.CUSTOM_CLIENT_NUMBER ' +
     ' from TMP_CO t2 ' +
     ' join TMP_CL t1 on t1.cl_nbr = t2.cl_nbr ' +
     ' #COND ' +
     ' order by CUSTOM_COMPANY_NUMBER ';

    with TExecDSWrapper.Create(ReturnsQueue) do
    begin
      if Length(Trim(Condition))> 0 then
             Condition := ' where ' + Condition;
      SetMacro('COND', Condition);
      cdQueue.Close;
      ctx_DataAccess.GetTmpCustomData(cdQueue, AsVariant);
      cdQueue.IndexFieldNames:='CUSTOM_COMPANY_NUMBER';
    end;

    cdQueue.LogChanges := False;
    SetGridColor;
End;

procedure TpfAcaUpdate.SetGridColor;
begin
    grColist.Enabled := rbSelectedCompanies.Checked and cdQueue.Active and (cdQueue.RecordCount > 0);
    if grColist.Enabled then
      grColist.Color := clWindow
    else
      grColist.Color := clBtnFace;
end;

procedure TpfAcaUpdate.btnCompanyFilterClick(Sender: TObject);
begin
  PrepareCompanyList;
end;

end.
