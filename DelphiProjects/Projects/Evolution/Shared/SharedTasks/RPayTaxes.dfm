inherited PayTaxesReturnsResultFrame: TPayTaxesReturnsResultFrame
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      object PageControl: TevPageControl
        Left = 0
        Top = 0
        Width = 427
        Height = 240
        ActivePage = tsChecks
        Align = alClient
        MultiLine = True
        TabOrder = 0
        TabPosition = tpRight
        OnChange = PageControlChange
        object tsChecks: TTabSheet
          Caption = 'Checks'
          object evPanel2: TevPanel
            Left = 0
            Top = 201
            Width = 381
            Height = 31
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object bPrint: TevButton
              Left = 8
              Top = 4
              Width = 153
              Height = 25
              Caption = 'Print Checks And Coupons'
              TabOrder = 0
              OnClick = bPrintClick
              Margin = 0
            end
          end
          inline CheckPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 381
            Height = 201
            Align = alClient
            AutoScroll = False
            TabOrder = 1
            OnResize = CheckPreviewContainerResize
          end
        end
        object tsAch: TTabSheet
          Caption = 'Ach'
          ImageIndex = 1
        end
        object tsAchReport: TTabSheet
          Caption = 'Ach Report'
          ImageIndex = 4
          inline ACHPreviewContainer: TrwPreviewContainer
            Left = 0
            Top = 0
            Width = 646
            Height = 376
            Align = alClient
            AutoScroll = False
            TabOrder = 0
            OnResize = ACHPreviewContainerResize
          end
        end
        object tsEFTP: TTabSheet
          Caption = 'EFTP'
          ImageIndex = 2
        end
        object tsNyDebit: TTabSheet
          Caption = 'Ny Debit'
          ImageIndex = 3
        end
        object tsMaDebit: TTabSheet
          Caption = 'MA Debit'
          ImageIndex = 6
        end
        object tsCaDebit: TTabSheet
          Caption = 'CA Debit'
          ImageIndex = 7
        end
        object tsUtils: TTabSheet
          Caption = 'Utils'
          ImageIndex = 5
          object Memo1: TMemo
            Left = 8
            Top = 8
            Width = 353
            Height = 41
            TabStop = False
            BorderStyle = bsNone
            Color = clBtnFace
            Lines.Strings = (
              
                'You can rollback this payment completely. This operation is irre' +
                'versible.'
              
                'Once you do you can pay these taxes again with or without change' +
                's.'
              
                'This operation is only available if you have security rights for' +
                ' it.')
            ReadOnly = True
            TabOrder = 0
          end
          object bRollback: TevButton
            Left = 8
            Top = 64
            Width = 137
            Height = 25
            Caption = 'Rollback This Payment'
            TabOrder = 1
            OnClick = bRollbackClick
            Margin = 0
          end
          object bMarkPaid: TevButton
            Left = 8
            Top = 104
            Width = 137
            Height = 25
            Caption = 'Mark Liabs Paid'
            TabOrder = 2
            Visible = False
            OnClick = bMarkPaidClick
            Margin = 0
          end
        end
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Width = 692
        Height = 343
      end
      inherited pWarnings: TevPanel
        Top = 343
        Width = 692
      end
    end
    inherited tsNotes: TTabSheet
      inherited memoNotes: TevMemo
        Width = 692
        Height = 343
      end
      inherited zevPanel2: TevPanel
        Top = 343
        Width = 692
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Width = 692
        Height = 384
      end
    end
    inherited tsRunStatistics: TTabSheet
      inherited frmStatistics: TevStatisticsViewerFrm
        inherited tvEvents: TTreeView
          Width = 234
        end
        inherited pnlChart: TPanel
          inherited Chart: TChart
            Height = 55
          end
        end
      end
    end
  end
end
