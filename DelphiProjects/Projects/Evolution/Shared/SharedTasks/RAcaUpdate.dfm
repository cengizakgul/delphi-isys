inherited rfAcaUpdate: TrfAcaUpdate
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      object evPanel2: TevPanel
        Left = 0
        Top = 0
        Width = 226
        Height = 240
        Align = alLeft
        BevelOuter = bvLowered
        BevelWidth = 2
        Caption = 'evPanel2'
        TabOrder = 0
        object grdCoList: TevDBGrid
          Left = 2
          Top = 2
          Width = 222
          Height = 236
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TrfAcaUpdate\grdCoList'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dsCoList
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          PopupMenu = mnRollback
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object evPanel3: TevPanel
        Left = 226
        Top = 0
        Width = 201
        Height = 240
        Align = alClient
        BevelOuter = bvLowered
        BevelWidth = 2
        Caption = 'evPanel3'
        TabOrder = 1
        object grdEEList: TevDBGrid
          Left = 2
          Top = 2
          Width = 197
          Height = 236
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TrfAcaUpdate\grdEEList'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dsEEList
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
    inherited tsRunStatistics: TTabSheet
      inherited frmStatistics: TevStatisticsViewerFrm
        inherited Splitter1: TSplitter
          Left = 0
          Top = 98
          Width = 427
          Height = 7
          Cursor = crVSplit
          Align = alBottom
        end
        inherited Splitter2: TSplitter
          Left = -31
          Top = 60
          Width = 9
          Height = 38
          Align = alRight
        end
        inherited Label3: TLabel
          Left = 0
          Top = 42
          Width = 427
          Height = 18
          Align = alTop
          AutoSize = False
          Caption = 'Events'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        inherited pnlFileList: TPanel
          Left = 0
          Top = 0
          Width = 427
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          inherited Label1: TLabel
            Left = 0
            Top = 14
            Width = 71
            Height = 13
            Caption = 'Statistic File'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          inherited cbDates: TevDBComboBox
            Left = 81
            Top = 12
            Width = 247
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = ()
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
        end
        inherited tvEvents: TTreeView
          Left = 0
          Top = 60
          Width = 47
          Height = 38
          Align = alClient
          HideSelection = False
          Indent = 19
          ReadOnly = True
          TabOrder = 1
        end
        inherited Panel2: TPanel
          Left = 0
          Top = 105
          Width = 427
          Height = 135
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          inherited Label2: TLabel
            Left = 0
            Top = 0
            Width = 427
            Height = 17
            Align = alTop
            AutoSize = False
            Caption = 'Event Info'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          inherited mEventInfo: TMemo
            Left = 0
            Top = 17
            Width = 427
            Height = 118
            Align = alClient
            Lines.Strings = ()
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        inherited pnlChart: TPanel
          Left = -22
          Top = 60
          Width = 449
          Height = 38
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          inherited Splitter3: TSplitter
            Left = 0
            Top = -89
            Width = 449
            Height = 7
            Cursor = crVSplit
            Align = alBottom
          end
          inherited Chart: TChart
            Left = 0
            Top = 0
            Width = 449
            Height = 43
            AllowPanning = pmNone
            AllowZoom = False
            BackImage.Data = {00}
            BackWall.Brush.Color = clWhite
            BackWall.Brush.Style = bsClear
            BackWall.Pen.Visible = False
            Foot.Text.Strings = ()
            Title.Text.Strings = (
              'Event summary')
            AxisVisible = False
            ClipPoints = False
            Frame.Visible = False
            Legend.Alignment = laBottom
            View3D = False
            View3DOptions.Elevation = 315
            View3DOptions.Orthogonal = False
            View3DOptions.Perspective = 0
            View3DOptions.Rotation = 360
            View3DWalls = False
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            inherited Series1: TPieSeries
              Marks.ArrowLength = 8
              Marks.Visible = False
              SeriesColor = clRed
              Title = 'Statistics'
              Circled = True
              OtherSlice.Text = 'Other'
              PieValues.DateTime = False
              PieValues.Name = 'Pie'
              PieValues.Multiplier = 1.000000000000000000
              PieValues.Order = loNone
            end
          end
          inherited lvSummary: TListView
            Left = 0
            Top = -82
            Width = 449
            Height = 120
            Align = alBottom
            Columns = <
              item
                Caption = 'Item'
                Width = 150
              end
              item
                AutoSize = True
                Caption = 'Value'
              end>
            ColumnClick = False
            GridLines = True
            ReadOnly = True
            RowSelect = True
            ShowColumnHeaders = False
            TabOrder = 1
            ViewStyle = vsReport
          end
        end
      end
    end
  end
  object cdCoList: TevClientDataSet
    Left = 584
    Top = 88
  end
  object cdEEList: TevClientDataSet
    Left = 584
    Top = 120
  end
  object dsCoList: TevDataSource
    DataSet = cdCoList
    OnDataChange = dsCoListDataChange
    Left = 624
    Top = 88
  end
  object dsEEList: TevDataSource
    DataSet = cdEEList
    Left = 624
    Top = 120
  end
  object mnRollback: TevPopupMenu
    Left = 488
    Top = 40
    object Rollback1: TMenuItem
      Caption = 'Rollback Selected Companies'
      OnClick = Rollback1Click
    end
    object RollbackAllCompanies1: TMenuItem
      Caption = 'Rollback All Companies'
      OnClick = RollbackAllCompanies1Click
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 672
    Top = 48
  end
end
