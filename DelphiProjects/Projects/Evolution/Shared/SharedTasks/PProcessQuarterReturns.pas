// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PProcessQuarterReturns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskParamFrame, StdCtrls, ExtCtrls,  Spin, EvBasicUtils, EvCommonInterfaces,
  ISBasicClasses, EvUIComponents;

type
  TProcessQuarterReturnsParamFrame = class(TTaskParamFrame)
    evRadioGroup1: TevRadioGroup;
    cbEeSort: TevRadioGroup;
    cbSettingsCredit: TevRadioGroup;
    evLabel1: TevLabel;
    evSpinEdit1: TevSpinEdit;
    evRadioGroup2: TevRadioGroup;
    cbHideSSN: TevCheckBox;
    procedure SetModifiedTrue(Sender: TObject);
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure   ApplyUpdates; override;
  end;

implementation

uses EvUtils;

{$R *.DFM}

{ TProcessQuarterReturnsParamFrame }

procedure TProcessQuarterReturnsParamFrame.ApplyUpdates;
begin
  (FTask as IevProcessTaxReturnsTask).TaxFilter := TProcessTaxReturnsFilterSelections(evRadioGroup1.ItemIndex);

  case cbEeSort.ItemIndex of
    0: (FTask as IevProcessTaxReturnsTask).EeSortMode := eesLastName;
    1: (FTask as IevProcessTaxReturnsTask).EeSortMode := eesSSN;
  else
    Assert(False);
  end;

  (FTask as IevProcessTaxReturnsTask).AskForRefund := cbSettingsCredit.ItemIndex = 1;
  case evRadioGroup2.ItemIndex of
    0: (FTask as IevProcessTaxReturnsTask).EndDate := EncodeDate(evSpinEdit1.Value, 3, 31);
    1: (FTask as IevProcessTaxReturnsTask).EndDate := EncodeDate(evSpinEdit1.Value, 6, 30);
    2: (FTask as IevProcessTaxReturnsTask).EndDate := EncodeDate(evSpinEdit1.Value, 9, 30);
    3: (FTask as IevProcessTaxReturnsTask).EndDate := EncodeDate(evSpinEdit1.Value, 12, 31);
  else
    Assert(False);
  end;
  (FTask as IevProcessTaxReturnsTask).SecureReport := cbHideSSN.Checked;
end;

constructor TProcessQuarterReturnsParamFrame.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;

  evSpinEdit1.Value := GetYear((ATask as IevProcessTaxReturnsTask).EndDate);
  evRadioGroup2.ItemIndex := GetQuarterNumber((ATask as IevProcessTaxReturnsTask).EndDate)- 1;
  evRadioGroup1.ItemIndex := Ord((ATask as IevProcessTaxReturnsTask).TaxFilter);

  case (ATask as IevProcessTaxReturnsTask).EeSortMode of
    eesLastName: cbEeSort.ItemIndex := 0;
    eesSSN:      cbEeSort.ItemIndex := 1;
  else
    Assert(False);
  end;

  if (ATask as IevProcessTaxReturnsTask).AskForRefund then
    cbSettingsCredit.ItemIndex := 1
  else
    cbSettingsCredit.ItemIndex := 0;

  cbHideSSN.Checked := (ATask as IevProcessTaxReturnsTask).SecureReport;

  FModified := False;
end;

procedure TProcessQuarterReturnsParamFrame.SetModifiedTrue(Sender: TObject);
begin
  inherited;
  FModified := True;
end;

end.
