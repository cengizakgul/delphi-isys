inherited rfEvoXImportTask: TrfEvoXImportTask
  inherited PC: TevPageControl
    inherited tsResults: TTabSheet
      object evSplitter1: TevSplitter
        Left = 0
        Top = 113
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 113
        Align = alTop
        TabOrder = 0
        object ResultsListGrid: TevDBGrid
          Left = 1
          Top = 1
          Width = 425
          Height = 111
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TrfEvoXImportTask\ResultsListGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          OnRowChanged = ResultDetailsGridRowChanged
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = ResultsDataSource
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object evPanel3: TevPanel
        Left = 0
        Top = 116
        Width = 427
        Height = 124
        Align = alClient
        TabOrder = 1
        object ResultDetailsGrid: TevDBGrid
          Left = 1
          Top = 1
          Width = 425
          Height = 122
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TrfEvoXImportTask\ResultDetailsGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = ErrorsLogDataSource
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          OnCalcCellColors = ResultDetailsGridCalcCellColors
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
    inherited tsWarnings: TTabSheet
      inherited memoWarnings: TevMemo
        Height = 210
      end
      inherited pWarnings: TevPanel
        Top = 210
      end
    end
    inherited tsLog: TTabSheet
      inherited memoLog: TevMemo
        Width = 668
        Height = 468
      end
    end
    inherited tsRunStatistics: TTabSheet
      inherited frmStatistics: TevStatisticsViewerFrm
        inherited tvEvents: TTreeView
          Width = 98
        end
        inherited pnlChart: TPanel
          inherited Chart: TChart
            Height = 24
          end
        end
      end
    end
  end
  object ErrorsLogDataSource: TDataSource
    Left = 104
    Top = 16
  end
  object ResultsDataSource: TDataSource
    Left = 136
    Top = 16
  end
end
