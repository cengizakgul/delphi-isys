inherited pfPrQueueList: TpfPrQueueList
  inherited lNoParams: TLabel
    Top = 37
    Height = 229
  end
  object pGrid: TevPanel
    Left = 0
    Top = 37
    Width = 435
    Height = 229
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pGrid'
    TabOrder = 1
    Visible = False
    object gList: TevDBGrid
      Left = 0
      Top = 0
      Width = 435
      Height = 229
      DisableThemesInTitle = False
      ControlType.Strings = (
        'Selected;CheckBox;Y;N')
      Selected.Strings = (
        'SELECTED'#9'8'#9'Selected'#9'F'
        'CLIENT_NUMBER_Lookup'#9'17'#9'Client Number'
        'COMPANY_NUMBER_Lookup'#9'17'#9'Company Number'
        'COMPANY_NAME_Lookup'#9'37'#9'Company Name'
        'CHECK_DATE'#9'10'#9'Check Date'
        'PAYROLL_TYPE'#9'4'#9'Type'
        'RUN_NUMBER'#9'5'#9'Run #'
        'PROCESS_PRIORITY'#9'4'#9'Prty')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TpfPrQueueList\gList'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = dsTMP_PR
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      ReadOnly = False
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      DefaultSort = 'PROCESS_PRIORITY;CHECK_DATE;RUN_NUMBER'
    end
  end
  object pSelect: TevPanel
    Left = 0
    Top = 0
    Width = 435
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object rbList: TevRadioButton
      Left = 150
      Top = 12
      Width = 103
      Height = 17
      Caption = 'Payrolls From List'
      TabOrder = 1
      OnClick = rbListClick
    end
    object rbAll: TevRadioButton
      Left = 12
      Top = 12
      Width = 130
      Height = 17
      Caption = 'All Completed Payrolls'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbListClick
    end
  end
  object pAge: TevPanel
    Left = 0
    Top = 37
    Width = 435
    Height = 229
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object pAge1: TevPanel
      Left = 1
      Top = 1
      Width = 433
      Height = 117
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lAge: TevLabel
        Left = 12
        Top = 12
        Width = 170
        Height = 13
        Caption = 'Payrolls, being completed more than'
      end
      object lHours: TevLabel
        Left = 81
        Top = 36
        Width = 32
        Height = 13
        Caption = 'hour(s)'
      end
      object lMinutes: TevLabel
        Left = 81
        Top = 63
        Width = 42
        Height = 13
        Caption = 'minute(s)'
      end
      object lFor: TevLabel
        Left = 12
        Top = 91
        Width = 12
        Height = 13
        Caption = 'for'
      end
      object seHours: TevSpinEdit
        Left = 12
        Top = 33
        Width = 60
        Height = 22
        MaxValue = 23
        MinValue = 0
        TabOrder = 0
        Value = 0
      end
      object seMinutes: TevSpinEdit
        Left = 12
        Top = 60
        Width = 60
        Height = 22
        MaxValue = 59
        MinValue = 0
        TabOrder = 1
        Value = 0
      end
      object rbAllCompanies: TevRadioButton
        Left = 33
        Top = 90
        Width = 88
        Height = 17
        Caption = 'all companies'
        Checked = True
        TabOrder = 2
        TabStop = True
        OnClick = rbAllCompaniesClick
      end
      object rbSelectedCompanies: TevRadioButton
        Left = 126
        Top = 90
        Width = 127
        Height = 17
        Caption = 'selected companies'
        TabOrder = 3
        OnClick = rbAllCompaniesClick
      end
    end
    object gSelectCompanies: TevDBGrid
      Left = 1
      Top = 118
      Width = 433
      Height = 110
      DisableThemesInTitle = False
      ControlType.Strings = (
        'Selected;CheckBox;Y;N')
      Selected.Strings = (
        'SELECTED'#9'8'#9'Selected'#9'F'
        'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
        'NAME'#9'25'#9'Name'#9'F'
        'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
        'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
        'FEIN'#9'9'#9'Fein'#9
        'DBA'#9'40'#9'Dba'#9'F'
        'UserFirstName'#9'20'#9'CSR First Name'#9'F'
        'UserLastName'#9'30'#9'CSR Last Name'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TpfPrQueueList\gSelectCompanies'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      DataSource = dsTMP_CO
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      Visible = False
      PaintOptions.AlternatingRowColor = clCream
      DefaultSort = 'CUSTOM_COMPANY_NUMBER'
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 381
    Top = 72
  end
  object dsTMP_PR: TevDataSource
    DataSet = DM_TMP_PR.TMP_PR
    Left = 345
    Top = 69
  end
  object dsTMP_CO: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 346
    Top = 101
  end
end
