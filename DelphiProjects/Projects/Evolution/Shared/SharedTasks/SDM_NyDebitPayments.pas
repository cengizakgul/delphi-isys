// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_NyDebitPayments;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, EvComponentsExt, Variants, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses,  EvDataAccessComponents, EvExceptions, EvClientDataSet;

type
  TNyDebitPayments = class(TComponent, INyDebitFile)
  private
    { Private declarations }
    FSbEIN,
    FSbName,
    FSbAddress,
    FSbCity,
    FSbState,
    FSbZip: string;
    cdMain: TevClientDataSet;
    function CheckDigit(EIN: string): Char;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Backup;
    procedure Restore;
    procedure FillStringList(const ResultList: TStringList);
    procedure AddPayment(const EIN: string; const LastPrInPeriod: TDateTime; const PayrollsInPeriod: Integer;
      const NyStateTax, NyCityTax, CityOfYorkersTax: Currency);
  end;

implementation

uses EvUtils, SDataStructure, EvTypes;

const
  TaxPaymentsKey = 'TaxPayments';
  TaxFilePathField = 'TaxNyDebitFilePathField';

{ TNyDebitPayments }

procedure TNyDebitPayments.AddPayment(const EIN: string;
  const LastPrInPeriod: TDateTime; const PayrollsInPeriod: Integer;
  const NyStateTax, NyCityTax, CityOfYorkersTax: Currency);
begin
  cdMain.AppendRecord([PadStringRight(EIN, ' ', 11)+ CheckDigit(EIN), LastPrInPeriod, PayrollsInPeriod, NyStateTax, NyCityTax, CityOfYorkersTax]);
end;

procedure TNyDebitPayments.Backup;
begin
  cdMain.MergeChangeLog;
end;

procedure TNyDebitPayments.FillStringList(const ResultList: TStringList);
  function MyFormatCurr(const c: Currency; const Size: Integer): string;
  var
    f: Integer;
  begin
    Assert(c >= 0);
    Result := IntToStr(Trunc(c));
    if Length(Result) > (Size-2) then
      raise EDataLostException.CreateHelp('Withheld tax is longer than '+ IntToStr(Size)+ ' characters', IDH_DataLostException)
      //Delete(Result, 1, Length(Result)- (Size-2))
    else
      Result := PadStringLeft(Result, '0', Size-2);
    f := Trunc((c- Trunc(c))* 100);
    if f >= 10 then
      Result := Result+ IntToStr(f)
    else
      Result := Result+ '0'+ IntToStr(f);
  end;
  function RemovePunctuation(const s: string): string;
  begin
    Result := StringReplace(s, '.', '', [rfReplaceAll]);
    Result := StringReplace(Result, ',', '', [rfReplaceAll]);
    Result := StringReplace(Result, '-', '', [rfReplaceAll]);
  end;
var
  s: string;
  LineTotal: Currency;
  HashTax: Currency;
  HashCounter: Integer;
begin
  ResultList.Clear;
  if cdMain.RecordCount <> 0 then
  begin
    {if SaveDialog.Tag = 0 then
    begin
      SaveDialog.FileName := IncludeTrailingBackslash(mb_AppSettings.GetValue(TaxPaymentsKey + '\' + TaxFilePathField, AppDir))+ 'NyDebit-'+ FormatDateTime('MM-DD-YY', Now);
      SaveDialog.InitialDir := ExtractFilePath(SaveDialog.FileName);
      while not SaveDialog.Execute do;
      mb_AppSettings[TaxPaymentsKey + '\' + TaxFilePathField] := ExtractFilePath(SaveDialog.FileName);
      SaveDialog.Tag := 1;
    end;
    AssignFile(f, SaveDialog.FileName);}
    s := '1HDR';
    s := PutIntoFiller(s, '000000', 5, 6);
    s := PutIntoFiller(s, FormatDateTime('MMDDYY', Now), 21, 6);
    s := PutIntoFiller(s, '000', 49, 3);
    s := PutIntoFiller(s, 'WT-1', 52, 4);
    s := PutIntoFiller(s, FSbEIN, 59, 12);
    s := PutIntoFiller(s, RemovePunctuation(FSbName), 71, 40);
    s := PutIntoFiller(s, RemovePunctuation(FSbAddress), 111, 30);
    s := PutIntoFiller(s, RemovePunctuation(FSbCity), 141, 25);
    s := PutIntoFiller(s, FSbState, 166, 2);
    s := PutIntoFiller(s, RemovePunctuation(FSbZip), 168, 9);
    s := PadStringRight(s, ' ', 279);
    ResultList.Append(s);
    HashTax := 0;
    HashCounter := 0;
    cdMain.First;
    while not cdMain.Eof do
    begin
      LineTotal := cdMain['NyStateTax']+ cdMain['NycCityTax']+ cdMain['CoyCityTax'];
      HashTax := HashTax+ LineTotal;
      Inc(HashCounter);
      s := cdMain['EIN'];
      s := PutIntoFiller(s, FormatDateTime('MMDDYY', cdMain['LastPayroll']), 36, 6);
      s := PutIntoFiller(s, '000', 42, 3);
      s := PutIntoFiller(s, MyFormatCurr(cdMain['NyStateTax'], 11), 45, 11);
      s := PutIntoFiller(s, MyFormatCurr(cdMain['NycCityTax'], 11), 56, 11);
      s := PutIntoFiller(s, MyFormatCurr(cdMain['CoyCityTax'], 11), 67, 11);
      s := PutIntoFiller(s, MyFormatCurr(0, 11), 111, 11);
      s := PutIntoFiller(s, MyFormatCurr(LineTotal, 11), 122, 11);
      s := PutIntoFiller(s, MyFormatCurr(LineTotal, 11), 133, 11);
      s := PutIntoFiller(s, '00', 150, 2);
      s := PadStringRight(s, ' ', 279);
      ResultList.Append(s);
      cdMain.Next;
    end;
    s := 'HASH TOTAL';
    s := PutIntoFiller(s, MyFormatCurr(HashTax, 13), 12, 13);
    s := PutIntoFiller(s, PadStringLeft(IntToStr(HashCounter), '0', 6), 27, 6);
    s := PadStringRight(s, ' ', 279);
    ResultList.Append(s);
    s := '1EOF';
    s := PutIntoFiller(s, PadStringLeft(IntToStr(HashCounter+ 1), '0', 7), 6, 7);
    s := PadStringRight(s, ' ', 279);
    ResultList.Append(s);
  end;
end;

procedure TNyDebitPayments.Restore;
begin
  cdMain.CancelUpdates;
end;

function TNyDebitPayments.CheckDigit(EIN: string): Char;
const
  EinSize = 11;
var
  i, c: Integer;
begin
  EIN := PadStringRight(EIN, '0', EinSize);
  c := 0;
  for i := 1 to EinSize do
    case EIN[i] of
    ' ', '0': ;
    '1'..'9':                               
      c := c+ i* (Ord(EIN[i])- Ord('0'));
    'A'..'Z':
      c := c+ i* (Ord(EIN[i])- Ord('A')+ 10);
    else
      Assert(False, 'Illegal characters in state EIN. Legal characters are space, 0-9, A-Z');
    end;
  Result := Chr(Ord('0')+ (9- (c mod 9)));
end;

constructor TNyDebitPayments.Create;
begin
  cdMain := TevClientDataSet.Create(Self);
  cdMain.FieldDefs.Add('EIN', ftString, 12);
  cdMain.FieldDefs.Add('LastPayroll', ftDateTime);
  cdMain.FieldDefs.Add('PayrollsTotal', ftInteger);
  cdMain.FieldDefs.Add('NyStateTax', ftCurrency);
  cdMain.FieldDefs.Add('NycCityTax', ftCurrency);
  cdMain.FieldDefs.Add('CoyCityTax', ftCurrency);
  cdMain.CreateDataSet;
  cdMain.LogChanges := True;
  with DM_SERVICE_BUREAU do
  begin
    SB.Activate;
    FSbEIN := SB['EIN_NUMBER'];
    FSbEIN := PadStringRight(FSbEIN, ' ', 11)+ CheckDigit(FSbEIN);
    FSbName := SB['SB_NAME'];
    FSbAddress := ConvertNull(SB['ADDRESS1'], '');
    if not VarIsNull(SB['ADDRESS2']) then
      FSbAddress := FSbAddress+ SB['ADDRESS2'];
    FSbCity := SB['CITY'];
    FSbState := SB['STATE'];
    FSbZip := SB['ZIP_CODE'];
  end;
end;

end.
