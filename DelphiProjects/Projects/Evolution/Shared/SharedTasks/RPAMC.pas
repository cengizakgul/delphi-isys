// Copyright � 2000-2012 iSystems LLC. All rights reserved.
unit RPAMC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls, ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, Menus, EvTypes, SLogCheckReprint, EvConsts, EvContext,
  rwPreviewContainerFrm, ISBasicClasses, EvCommonInterfaces,
  EvStatisticsViewerFrm, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TrfPAMC = class(TTaskResultFrame)
    PreviewContainer: TrwPreviewContainer;
    procedure PreviewContainerResize(Sender: TObject);
  private
    FResult: TrwReportResults;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
//    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); override;
  end;

implementation


{$R *.DFM}

{ TrfPAMC }

constructor TrfPAMC.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FResult := TrwReportResults.Create;

  if (Task as IevPAMCTask).ReportResults <> nil then
  begin
    (Task as IevPAMCTask).ReportResults.Position := 0;
    FResult.SetFromStream((Task as IevPAMCTask).ReportResults);
  end;

  memoWarnings.Text := (Task as IevPAMCTask).Warnings;
  tsWarnings.TabVisible := Length(memoWarnings.Text) > 0;

  CheckExceptions;

  Align := alClient;
  ctx_RWLocalEngine.Preview(FResult, PreviewContainer);
end;


destructor TrfPAMC.Destroy;
begin
  FResult.Free;
  inherited;
end;

procedure TrfPAMC.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

end.
