// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RReportPreview;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskResultFrame, StdCtrls,  ComCtrls, EvUtils, SReportSettings,
  Buttons, ExtCtrls, ISBasicClasses, rwPreviewContainerFrm, SFrameEntry, EvContext,
  EvCommonInterfaces, EvStatisticsViewerFrm, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TrfReportPreview = class(TTaskResultFrame)
    PreviewContainer: TrwPreviewContainer;
    procedure PreviewContainerResize(Sender: TObject);
  private
    FResult: TrwReportResults;
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    destructor  Destroy; override;
//    procedure   OnShortCut(var Msg: TWMKey; var Handled: Boolean); override;    
  end;

implementation

{$R *.DFM}

{ TrfReportPreview }

constructor TrfReportPreview.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  FResult := TrwReportResults.Create;

  if (ATask as IevRunReportTask).ReportResults <> nil then
  begin
    (ATask as IevRunReportTask).ReportResults.Position := 0;
    FResult.SetFromStream((ATask as IevRunReportTask).ReportResults);
  end;  

  memoWarnings.Lines.Text := ATask.Warnings;
  tsWarnings.TabVisible := Length(memoWarnings.Text) > 0;
  CheckExceptions;

  Align := alClient;
  Update;
  ctx_RWLocalEngine.Preview(FResult, PreviewContainer);
end;


destructor TrfReportPreview.Destroy;
begin
  FResult.Free;
  inherited;
end;


{procedure TrfReportPreview.OnShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if tsResults.ControlCount > 0 then
    RWEngineInterface.PreviewShortCutNotify(TWinControl(tsResults.Controls[0]), Msg, Handled);
end;
}
procedure TrfReportPreview.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  PreviewContainer.FrameResize(Sender);
end;

end.
