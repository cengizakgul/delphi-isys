// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PRebuildTempTables;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EvTaskParamFrame, StdCtrls, wwdblook, ExtCtrls, Variants, 
  Spin, ComCtrls, SDataStructure, EvUtils, ISBasicClasses, EvCommonInterfaces, EvUIComponents,
  isUIEdit;

type
  TpfRebuildTempTables = class(TTaskParamFrame)
    cbFullRebuild: TevCheckBox;
    cbUseRange: TevCheckBox;
    cbUseNumbers: TevCheckBox;
    seFromClient: TevSpinEdit;
    seToClient: TevSpinEdit;
    edClientNumbers: TevEdit;
    lbTo: TevLabel;
    Bevel1: TBevel;
    procedure cbFullRebuildClick(Sender: TObject);
  private
    { Private declarations }
    procedure UpdateStates;
  public
    { Public declarations }
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask); override;
    procedure ApplyUpdates; override;
  end;

implementation


{$R *.DFM}

procedure TpfRebuildTempTables.ApplyUpdates;
begin
  inherited;
  (FTask as IevRebuildTempTablesTask).FullRebuild := cbFullRebuild.Checked;
  (FTask as IevRebuildTempTablesTask).UseRange := cbUseRange.Checked;
  (FTask as IevRebuildTempTablesTask).UseNumbers := cbUseNumbers.Checked;
  (FTask as IevRebuildTempTablesTask).FromClient := seFromClient.Value;
  (FTask as IevRebuildTempTablesTask).ToClient := seToClient.Value;
  (FTask as IevRebuildTempTablesTask).ClientNumbers := edClientNumbers.Text;

  if cbFullRebuild.Checked then
    (FTask as IevRebuildTempTablesTask).Caption := 'Rebuild all clients'
  else if cbUseRange.Checked then
  begin
    (FTask as IevRebuildTempTablesTask).Caption := 'Rebuild clients from ' + IntToStr(seFromClient.Value) + ' to ' + IntToStr(seToClient.Value);
    if cbUseNumbers.Checked then
      (FTask as IevRebuildTempTablesTask).Caption := (FTask as IevRebuildTempTablesTask).Caption + ' and ' + edClientNumbers.Text;
  end
  else if cbUseNumbers.Checked then
  begin
    if Pos(',', edClientNumbers.Text) = 0 then
      (FTask as IevRebuildTempTablesTask).Caption := 'Rebuild client ' + edClientNumbers.Text
    else
      (FTask as IevRebuildTempTablesTask).Caption := 'Rebuild clients ' + edClientNumbers.Text;
  end;
  if (FTask as IevRebuildTempTablesTask).Caption <> '' then
  begin
    DM_SERVICE_BUREAU.SB.Activate;
    (FTask as IevRebuildTempTablesTask).Caption := (FTask as IevRebuildTempTablesTask).Caption + ' for ' + DM_SERVICE_BUREAU.SB.SB_NAME.Value;
  end
end;

procedure TpfRebuildTempTables.cbFullRebuildClick(Sender: TObject);
begin
  inherited;
  UpdateStates;
end;

constructor TpfRebuildTempTables.Create(const AOwner: TComponent; const AParent: TWinControl; const ATask: IevTask);
begin
  inherited;
  cbFullRebuild.Checked := (FTask as IevRebuildTempTablesTask).FullRebuild;
  cbUseRange.Checked := (FTask as IevRebuildTempTablesTask).UseRange;
  cbUseNumbers.Checked := (FTask as IevRebuildTempTablesTask).UseNumbers;
  seFromClient.Value := (FTask as IevRebuildTempTablesTask).FromClient;
  seToClient.Value := (FTask as IevRebuildTempTablesTask).ToClient;
  edClientNumbers.Text := (FTask as IevRebuildTempTablesTask).ClientNumbers;
  UpdateStates;
end;

procedure TpfRebuildTempTables.UpdateStates;
begin
  if cbFullRebuild.Checked then
  begin
    cbUseRange.Enabled := False;
    cbUseNumbers.Enabled := False;
    seFromClient.Enabled := False;
    seToClient.Enabled := False;
    edClientNumbers.Enabled := False;
    lbTo.Enabled := False;
  end
  else
  begin
    cbUseRange.Enabled := True;
    cbUseNumbers.Enabled := True;
    seFromClient.Enabled := True;
    seToClient.Enabled := True;
    edClientNumbers.Enabled := True;
    lbTo.Enabled := True;
  end;
end;

end.
