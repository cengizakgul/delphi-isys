inherited pfPrCreateParams: TpfPrCreateParams
  inherited lNoParams: TLabel
    Width = 681
    Height = 441
  end
  object lablCompany: TevLabel
    Left = 8
    Top = 8
    Width = 44
    Height = 13
    Caption = 'Company'
  end
  object lckpCompany: TevDBLookupCombo
    Left = 8
    Top = 24
    Width = 337
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
      'NAME'#9'25'#9'Name'#9'F')
    LookupTable = DM_TMP_CO.TMP_CO
    LookupField = 'CUSTOM_COMPANY_NUMBER'
    Style = csDropDownList
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnExit = lckpCompanyChange
  end
  object evGroupBox1: TevGroupBox
    Left = 8
    Top = 48
    Width = 337
    Height = 273
    Caption = 'Payroll'
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 16
      Top = 16
      Width = 83
      Height = 13
      Caption = 'Check Comments'
    end
    object memoComments: TevMemo
      Left = 16
      Top = 32
      Width = 305
      Height = 41
      TabOrder = 0
    end
    object rgrpBlockAgencies: TevRadioGroup
      Left = 16
      Top = 80
      Width = 145
      Height = 33
      Caption = 'Block Agencies'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 1
    end
    object rgrpBlockACH: TevRadioGroup
      Left = 16
      Top = 120
      Width = 145
      Height = 33
      Caption = 'Block ACH'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 2
    end
    object rgrpBlockBilling: TevRadioGroup
      Left = 176
      Top = 80
      Width = 145
      Height = 33
      Caption = 'Block Billing'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 3
    end
    object rgrpBlockTaxDeposits: TevRadioGroup
      Left = 16
      Top = 160
      Width = 145
      Height = 49
      Caption = 'Block Tax Deposits'
      Columns = 2
      ItemIndex = 3
      Items.Strings = (
        'Deposits'
        'Liabilities'
        'Both'
        'None')
      TabOrder = 4
    end
    object rgrpBlockChecks: TevRadioGroup
      Left = 176
      Top = 120
      Width = 145
      Height = 49
      Caption = 'Block Checks/Reports'
      Columns = 2
      ItemIndex = 3
      Items.Strings = (
        'Reports'
        'Checks'
        'Both'
        'None')
      TabOrder = 5
    end
    object rgrpBlockTimeOff: TevRadioGroup
      Left = 16
      Top = 216
      Width = 145
      Height = 49
      Caption = 'Block Time Off Accrual'
      Columns = 2
      ItemIndex = 2
      Items.Strings = (
        'All'
        'Accrual'
        'None')
      TabOrder = 6
    end
  end
  object evGroupBox2: TevGroupBox
    Left = 360
    Top = 48
    Width = 321
    Height = 273
    Caption = 'Batches'
    TabOrder = 2
    object evLabel2: TevLabel
      Left = 16
      Top = 16
      Width = 79
      Height = 13
      Caption = 'Pay Frequencies'
    end
    object evLabel3: TevLabel
      Left = 16
      Top = 182
      Width = 112
      Height = 13
      Caption = 'Payroll Check Template'
    end
    object evLabel4: TevLabel
      Left = 16
      Top = 224
      Width = 56
      Height = 13
      Caption = 'Payroll Filter'
    end
    object cbFrequencies: TevComboBox
      Left = 16
      Top = 32
      Width = 289
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object rgrpPaySalary: TevRadioGroup
      Left = 16
      Top = 64
      Width = 137
      Height = 41
      Caption = 'Pay Salary'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 1
    end
    object rgrpPayHours: TevRadioGroup
      Left = 168
      Top = 64
      Width = 137
      Height = 41
      Caption = 'Pay Standard Hours'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 2
    end
    object rgrpLoadDefaults: TevRadioGroup
      Left = 16
      Top = 110
      Width = 137
      Height = 41
      Caption = 'Load Payroll Defaults'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 3
    end
    object lckpTemplate: TevDBLookupCombo
      Left = 16
      Top = 198
      Width = 289
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'40'#9'NAME')
      LookupTable = DM_CO_PR_CHECK_TEMPLATES.CO_PR_CHECK_TEMPLATES
      LookupField = 'CO_PR_CHECK_TEMPLATES_NBR'
      Style = csDropDownList
      TabOrder = 4
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object lckpFilter: TevDBLookupCombo
      Left = 16
      Top = 240
      Width = 289
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'40'#9'NAME')
      LookupTable = DM_CO_PR_FILTERS.CO_PR_FILTERS
      LookupField = 'CO_PR_FILTERS_NBR'
      Style = csDropDownList
      TabOrder = 5
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object cbIncludeTimeOffRequests: TevCheckBox
      Left = 17
      Top = 159
      Width = 145
      Height = 17
      Caption = 'Include Time Off Requests'
      TabOrder = 6
    end
  end
  object cbAutoCommit: TevCheckBox
    Left = 360
    Top = 24
    Width = 185
    Height = 17
    Caption = 'Automatically Commit Payroll'
    TabOrder = 3
  end
  object evGroupBox3: TevGroupBox
    Left = 8
    Top = 328
    Width = 673
    Height = 113
    Caption = 'Time Clock Import'
    TabOrder = 4
    object evGroupBox4: TevGroupBox
      Left = 16
      Top = 16
      Width = 321
      Height = 89
      Caption = 'File Source'
      TabOrder = 0
      object evSpeedButton1: TevBitBtn
        Left = 288
        Top = 56
        Width = 23
        Height = 22
        Caption = '...'
        TabOrder = 3
        OnClick = evSpeedButton1Click
        Margin = 0
      end
      object rbFileFromCommonDir: TevRadioButton
        Left = 8
        Top = 16
        Width = 273
        Height = 17
        Caption = 'Auto locate in common directory'
        TabOrder = 0
      end
      object rbFileSpecified: TevRadioButton
        Left = 8
        Top = 32
        Width = 281
        Height = 17
        Caption = 'Use specified file'
        TabOrder = 1
      end
      object edFileSource: TevEdit
        Left = 8
        Top = 56
        Width = 281
        Height = 21
        TabOrder = 2
        Text = 'edFileSource'
      end
    end
    object rgTcLookup: TevRadioGroup
      Left = 352
      Top = 16
      Width = 89
      Height = 57
      Caption = 'Look EE up by'
      Items.Strings = (
        'Custom#'
        'Name'
        'SSN')
      TabOrder = 1
    end
    object rgTcDbdt: TevRadioGroup
      Left = 448
      Top = 16
      Width = 81
      Height = 57
      Caption = 'DBDT match'
      Items.Strings = (
        'Full'
        'Partial')
      TabOrder = 2
    end
    object rgTcFileFormat: TevRadioGroup
      Left = 536
      Top = 16
      Width = 121
      Height = 57
      Caption = 'File format'
      Items.Strings = (
        'Fixed Position'
        'Comma delimited')
      TabOrder = 3
    end
    object cbTcFourDigits: TevCheckBox
      Left = 352
      Top = 75
      Width = 145
      Height = 17
      Caption = 'Use four digits for year'
      TabOrder = 4
    end
    object cbTcAutoImportJobCodes: TevCheckBox
      Left = 512
      Top = 75
      Width = 145
      Height = 17
      Caption = 'Auto import job codes'
      TabOrder = 5
    end
    object cbtcUseEmployeePayRates: TevCheckBox
      Left = 352
      Top = 91
      Width = 145
      Height = 17
      Caption = 'Use Employee Pay Rates'
      TabOrder = 6
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 456
    Top = 16
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 528
    Top = 16
  end
  object dFile: TOpenDialog
    Title = 'Select import source file'
    Left = 256
    Top = 352
  end
end
