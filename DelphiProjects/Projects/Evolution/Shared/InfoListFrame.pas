// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit InfoListFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, EvBasicUtils,
  StdCtrls,  ExtCtrls, ComCtrls, SDataStructure, EvConsts, EvContext,
  EvUtils, EvSendMail, Registry, Printers, Grids, ISBasicClasses, EvMainboard,
  isBaseClasses, isBasicUtils, evTypes, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton, evDataSet;

type
  TInfoList = class(TFrame)
    Panel: TevPanel;
    Grid: TevStringGrid;
    evPanel1: TevPanel;
    btnSend: TevButton;
    procedure btnSendClick(Sender: TObject);
  public
    function AddLine: TStrings;
    procedure Activate(VersionsOnly: Boolean = False);
  end;

implementation

uses EvCommonInterfaces;

{$R *.DFM}

{ TInfoList }

function ENum(HInstance: Integer; Data: Pointer): Boolean;
var
  s: PChar;
  Buffer: array [1..1024] of Char;
  PInfo: Pointer;
  L: Cardinal;
begin
  Result := True;
  GetMem(s, 2048);
  if GetModuleFileName(HInstance, s, 2048) <> 0 then
    with TInfoList(Data).AddLine do
    begin
      Strings[0] := LowerCase(ExtractFileName(s));
      Strings[1] := LowerCase(ExtractFilePath(s));
      if GetFileVersionInfo(s, 0, 1024, @Buffer) and
         VerQueryValue(@Buffer, '\', PInfo, L) then
        Strings[2] := IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS div $10000) + '.' +
                     IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS mod $10000) + '.' +
                     IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS div $10000) + '.' +
                     IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS mod $10000);
    end;
  FreeMem(s);
end;

procedure TInfoList.Activate(VersionsOnly: Boolean);
var
  i: Integer;
  clinfo: TIsStringGridHwInfo;
  LoadedModules: IisList;
  ModuleList: IisStringList;
  ModuleInstance: IInterface;
  Q: IevQuery;
  s: String;
begin
  with AddLine do
  begin
    Strings[0] := 'Generated';
    Strings[1] := DateTimeToStr(Now);
  end;

  if not VersionsOnly then
  begin
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB]);

    with AddLine do
    begin
      Strings[0] := 'Service Bureau';
      Strings[1] := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;
    end;

    with AddLine do
    begin
      Strings[0] := 'User name';
      Strings[1] := Context.UserAccount.User;
    end;

    with AddLine do
    begin
      Strings[0] := 'System Database Version';

      Q := TevQuery.Create('SELECT t.tag FROM sy_storage t WHERE t.tag LIKE ''VER %''');
      s := Q.Result.Fields[0].AsString;
      System.Delete(s, 1, 4);
      Strings[1] := s;
    end;
  end;

  AddLine;
  clinfo := TIsStringGridHwInfo.Create(nil);
  try
    clinfo.Initialize;
    clinfo.ReportToStringGrid(Grid);
  finally
    clinfo.Free;
  end;

  AddLine;

  ENumModules(ENum, Self);


  // Create Evo module info
  LoadedModules := Mainboard.ModuleRegister.GetModules;
  ModuleList := TisStringList.CreateAsIndex;

  for i := 0 to LoadedModules.Count - 1 do
    ModuleList.AddObject((LoadedModules[i] as IevModuleDescriptor).ModuleName, LoadedModules[i]);

  AddLine;
  with AddLine do
    Strings[0] := 'Evolution Modules:';

  for i := 0 to ModuleList.Count - 1 do
  begin
    ModuleInstance := Mainboard.ModuleRegister.CreateModuleInstance((ModuleList.Objects[i] as IevModuleDescriptor).InstanceType);
    if Assigned(ModuleInstance) then
      with AddLine do
      begin
        Strings[0] := ModuleList[i];
        if Mainboard.ModuleRegister.IsProxyModule(ModuleInstance) then
          Strings[1] := 'Proxy';
      end;
    ModuleInstance := nil;
  end;


end;

function TInfoList.AddLine: TStrings;
begin
  Result := Grid.AddLine;
end;

procedure TInfoList.btnSendClick(Sender: TObject);
var
  FileName: string;
  F: TextFile;
  i: Integer;
  t: TStringList;
begin
  inherited;
  FileName := AppTempFolder + '_SysInfo.txt';
  AssignFile(F, FileName);
  Rewrite(F);
  try
    for i := 0 to Pred(Grid.RowCount) do
      Writeln(F, StringReplace(Grid.Rows[i].Text, #13, #9, [rfReplaceAll]));
    CloseFile(F);
    t := TStringList.Create;
    t.Add(FileName);
    try
      MailTo(nil, nil, nil, t, '', '');
    finally
      t.Free;
    end;
  finally
    DeleteFile(FileName);
  end;
end;

end.
