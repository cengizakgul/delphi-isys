inherited ChoiceFromListQueryEx: TChoiceFromListQueryEx
  Caption = 'ChoiceFromListQueryEx'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    inherited btnYes: TevBitBtn
      Action = HandleSelected
    end
    object btnAll: TevBitBtn
      Left = 64
      Top = 8
      Width = 113
      Height = 25
      Action = HandleAll
      Anchors = [akRight, akBottom]
      Default = True
      TabOrder = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD2222DDDD
        DDDDDDDDFFFFDDDDDDDDDDDDAAA2D2DDDDDDDDDD888FD8DDDDDDDDDDAAA2D2D2
        DDDDDDDD888FD8D8DDDDDDDDAAA2DDDDDDDDDDDD888FDDDDDDDD2222AAA22222
        DDDDFFFF888FFFFFDDDDAAAAAAAAAAA2D2DD88888888888FD8DDAAAAAAAAAAA2
        D2D288888888888FD8D8AAAAAAAAAAA2D2D288888888888FD8D8DDDDAAA2DDDD
        D2D2DDDD888FDDDDD8D8DD88AAA2D2DDDDD2DD8D888FD8DDDDD8DDDDAAA2D2D2
        DDDDDDDD888FD8D8DDDDDDDDAAA2D2D2DDDDDDDD888FD8D8DDDDDDDDDDDDD2D2
        DDDDDDDDDDDDD8D8DDDDDDDDDDDDDDD2DDDDDDDDDDDDDDD8DDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  inherited dgChoiceList: TevDBGrid
    IniAttributes.SectionName = 'TChoiceFromListQueryEx\dgChoiceList'
  end
  object ActionList1: TActionList
    Left = 248
    Top = 88
    object HandleSelected: TAction
      Caption = 'Yes'
      OnExecute = HandleSelectedExecute
      OnUpdate = HandleSelectedUpdate
    end
    object HandleAll: TAction
      Caption = 'All'
      OnExecute = HandleAllExecute
      OnUpdate = HandleAllUpdate
    end
  end
end
