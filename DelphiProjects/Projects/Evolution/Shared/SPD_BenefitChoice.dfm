inherited BenefitChoice: TBenefitChoice
  Left = 466
  Width = 405
  Height = 457
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 378
    Width = 389
    inherited btnYes: TevBitBtn
      Left = 132
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 258
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 21
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 389
    Height = 343
    Selected.Strings = (
      'BENEFIT_NAME'#9'44'#9'Benefit Name'#9'F'
      'CO_BENEFITS_NBR'#9'10'#9'CO Benefit NBR'#9#9)
    IniAttributes.SectionName = 'TBenefitChoice\dgChoiceList'
  end
  inherited pnlEffectiveDate: TevPanel
    Top = 343
    Width = 389
  end
  inherited dsChoiceList: TevDataSource
    Left = 192
    Top = 40
  end
  inherited cdsChoiceList: TevClientDataSet
    Left = 344
    Top = 80
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 184
    Top = 168
  end
end
