object HistoryViewDialog: THistoryViewDialog
  Left = 419
  Top = 221
  Width = 662
  Height = 509
  Caption = 'Audit'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    646
    471)
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TevBitBtn
    Left = 476
    Top = 444
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '&Close'
    Default = True
    TabOrder = 0
    OnClick = BitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
  end
  object BitBtn2: TevBitBtn
    Left = 564
    Top = 444
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Help'
    TabOrder = 1
    OnClick = BitBtn2Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD3333DD
      DDDDDDDDDDDFFFDDDDDDDDDDDFBBB3DDDDDDDDDDDD888FDDDDDDDDDDDFBBBDDD
      DDDDDDDDDD888DDDDDDDDDDDDD333DDDDDDDDDDDDDDFFDDDDDDDDDDDDFBB3DDD
      DDDDDDDDDD88FDDDDDDDDDDDDFBB3DDDDDDDDDDDDD88FDDDDDDDDDDDDFBBB33D
      DDDDDDDDDD888FFDDDDDDDDDDDFBBBB3DDDDDDDDDDD8888FDDDDDDDDDDDDFBBB
      3DDDDDDDDDDDD888FDDDDDDDDDDDDFBB73DDDDDDDDDDDD888FDDDDDD3DDDDFBB
      B3DDDDDDFDDDDF888FDDDDDF73337BBBB3DDDDDD8FFFF8888FDDDDDFBBBBBBBB
      7DDDDDDD888888888DDDDDDFBBBBBBBBDDDDDDDD88888888DDDDDDDDFBBBBBDD
      DDDDDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
    NumGlyphs = 2
  end
  object DBGrid1: TevDBGrid
    Left = 11
    Top = 10
    Width = 633
    Height = 421
    DisableThemesInTitle = False
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly]
    IniAttributes.Enabled = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'THistoryViewDialog\DBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource1
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = True
    PaintOptions.AlternatingRowColor = clCream
    Sorting = False
  end
  object bUndelete: TevBitBtn
    Left = 18
    Top = 444
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Undelete'
    TabOrder = 3
    Visible = False
    OnClick = bUndeleteClick
  end
  object DataSource1: TevDataSource
    Left = 232
    Top = 444
  end
end
