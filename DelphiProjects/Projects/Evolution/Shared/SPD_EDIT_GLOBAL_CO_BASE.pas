// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_GLOBAL_CO_BASE;

interface

uses
   SFrameEntry, Db,  StdCtrls, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, Classes, Wwdatsrc,
  ComCtrls, Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, EvUIComponents, EvClientDataSet;

type
  TEDIT_GLOBAL_CO_BASE = class(TFrameEntry)
    SelectedCompanies: TevClientDataSet;
    SelectedCompaniesCL_NBR: TIntegerField;
    SelectedCompaniesCO_NBR: TIntegerField;
    SelectedCompaniesCUSTOM_COMPANY_NUMBER: TStringField;
    SelectedCompaniesNAME: TStringField;
    wwdsSelectedCompanies: TevDataSource;
    Panel2: TevPanel;
    Splitter1: TevSplitter;
    wwdbgridSelectClient: TevDBGrid;
    pnlSubbrowse: TevPanel;
    Panel4: TevPanel;
    wwDBGrid1: TevDBGrid;
    Panel3: TevPanel;
    bbtnCopyAll: TevBitBtn;
    bbtnRemoveAll: TevBitBtn;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    wwdsTempTable: TevDataSource;
    procedure bbtnCopyAllClick(Sender: TObject);
    procedure bbtnRemoveAllClick(Sender: TObject);
    procedure wwdbgridSelectClientDblClick(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CopyAllCompanies;
    procedure CopyCompany; virtual;
  public
    { Public declarations }
    //procedure UpdateStates(Sender: TObject); override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_GLOBAL_CO_BASE: TEDIT_GLOBAL_CO_BASE;

implementation

{$R *.DFM}

uses EvUtils, SDataStructure;

procedure TEDIT_GLOBAL_CO_BASE.bbtnCopyAllClick(Sender: TObject);
begin
  inherited;
  CopyAllCompanies;
end;

procedure TEDIT_GLOBAL_CO_BASE.bbtnRemoveAllClick(Sender: TObject);
begin
  inherited;
  SelectedCompanies.Close;
  SelectedCompanies.CreateDataSet;
end;

procedure TEDIT_GLOBAL_CO_BASE.CopyAllCompanies;
begin
  with wwdsTempTable.DataSet do
  begin
    SelectedCompanies.DisableControls;
    DisableControls;
    First;
    while not EOF do
    begin
      CopyCompany;
      Next;
    end;
    EnableControls;
    SelectedCompanies.EnableControls;
  end;
end;

procedure TEDIT_GLOBAL_CO_BASE.CopyCompany;
var
  I: Integer;
begin
  with wwdsTempTable.DataSet do
    if not SelectedCompanies.Locate('CL_NBR;CO_NBR', VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), []) then
    begin
      SelectedCompanies.Append;
      for I := 0 to SelectedCompanies.FieldCount - 1 do
        SelectedCompanies.Fields[I].Value := FieldByName(SelectedCompanies.Fields[I].FieldName).Value;
      SelectedCompanies.Post;
    end;
end;

procedure TEDIT_GLOBAL_CO_BASE.wwdbgridSelectClientDblClick(
  Sender: TObject);
begin
  inherited;
  CopyCompany;
end;

procedure TEDIT_GLOBAL_CO_BASE.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if SelectedCompanies.RecordCount > 0 then
    SelectedCompanies.Delete;
end;

{procedure TEDIT_GLOBAL_CO_BASE.UpdateStates(Sender: TObject);
begin
  inherited;
  pnlSubbrowse.Visible := True;
end;}

procedure TEDIT_GLOBAL_CO_BASE.Activate;
begin
  inherited;
  wwdsTempTable.DataSet := DM_TEMPORARY.TMP_CO;
  SelectedCompanies.CreateDataSet;
end;

procedure TEDIT_GLOBAL_CO_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_TEMPORARY.TMP_CO, SupportDataSets, GetReadOnlyClintCompanyListFilter(false));
end;

end.
