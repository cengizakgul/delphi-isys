inherited evVersionedCL_E_DS_EXEMPT_EXCLUDE: TevVersionedCL_E_DS_EXEMPT_EXCLUDE
  Left = 762
  Top = 250
  ClientHeight = 503
  ClientWidth = 808
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 808
    Height = 503
    inherited grFieldData: TevDBGrid
      Width = 760
      Height = 372
    end
    inherited pnlEdit: TevPanel
      Top = 416
      Width = 760
      Height = 59
      inherited evLabel2: TevLabel
        Left = 118
      end
      inherited pnlButtons: TevPanel
        Left = 451
        Top = 19
        inherited btnNavInsert: TevSpeedButton
          Left = 12
        end
        inherited btnNavDelete: TevSpeedButton
          Left = 77
        end
      end
      inherited deEndDate: TevDBDateTimePicker
        Left = 118
        Width = 110
      end
      inherited deBeginDate: TevDBDateTimePicker
        Width = 110
      end
      object drgpExemptExclude: TevDBRadioGroup
        Left = 241
        Top = 15
        Width = 205
        Height = 44
        HelpContext = 8142
        Caption = '~EE EIC'
        Columns = 3
        DataSource = dsFieldData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Block'
          'Include')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'E'
          'X'
          'I')
      end
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 376
    Top = 64
  end
end
