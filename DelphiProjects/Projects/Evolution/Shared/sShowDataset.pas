// Copyright � 2000-2005 iSystems LLC. All rights reserved.
unit sShowDataset;
////////////////////////////////////////////////////////////////////////////////
// Include this unit in the uses clause, then call the ShowDataSet procedure
// with a dataset similar to the way you would call ShowMessage with a string.
////////////////////////////////////////////////////////////////////////////////
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ExtCtrls, StdCtrls, ComCtrls;

type
  TShowDataSetForm = class(TForm)
    BottomPanel: TPanel;
    DataSource: TDataSource;
    DBGrid: TDBGrid;
    OKButton: TButton;
    StatusBar: TStatusBar;
  end;

procedure ShowDataSet(DataSet: TDataset);

implementation
{$R *.dfm}
uses
  SDataStructure;

procedure ShowDataSet(DataSet: TDataset);
var
  ShowDataSetForm: TShowDataSetForm;
begin
  ShowDataSetForm := TShowDataSetForm.Create(nil);
  try
    ShowDataSetForm.DataSource.DataSet := DataSet;
    ShowDataSetForm.Caption := DataSet.Name;
    DM_CLIENT.CL.Open;
    ShowDataSetForm.StatusBar.Panels[0].Text :=
      'Client: ' + DM_CLIENT.CL.CUSTOM_CLIENT_NUMBER.AsString;
    ShowDataSetForm.StatusBar.Panels[1].Text :=
      'Client Database: CL_' + IntToStr(DM_CLIENT.CL.CL_NBR.AsInteger);
    ShowDataSetForm.ShowModal;
  finally
    FreeAndNil(ShowDataSetForm);
  end;
end;

end.

