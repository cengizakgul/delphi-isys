// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SReportSettings;

interface

uses Classes, SysUtils, SyncObjs, EvStreamUtils, DB, EvTypes, Variants, EvConsts, isBasicUtils,
     isExceptions, Math, EvClientDataSet;


type
  TReportParamArrayType = array of record ParamName: string; ParamValue: Variant end;

  TrwParamCollection = class(TCollection)
  protected
    function GetEstimatedMemorySize: LongInt; virtual;
  public
    function  GetAsStream: IisStream; virtual;
    procedure SetFromStream(const Value: IisStream); virtual;
  end;


  //Report parameters

  TrwReportParam = class (TCollectionItem)
  private
    FValue: Variant;
    FName: string;
    FVarType: Integer;
    FStore: Boolean;

    procedure SetValue(const AValue: Variant);
    procedure SetVarType(const Value: Integer);
    procedure ReadValue(Reader: TReader);
    procedure WriteValue(Writer: TWriter);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
    procedure SetArrItemValue(AIndex: Integer; AValue: Variant);

  published
    property Name: string read FName write FName;
    property VarType: Integer read FVarType write SetVarType;
    property Value: Variant read FValue write SetValue stored False;
    property Store: Boolean read FStore write FStore default True;
  end;


  TrwReportParams = class(TCollection)
  private
    FState: TComponentState;
    FFileName: String;
    FAddToExistFile: Boolean;
    FDuplexing: Boolean;
    FCopies: Byte;
    FLayers: TrwPrintableLayers;

    function  GetItem(Index: Integer): TrwReportParam;
    procedure SetItem(Index: Integer; const Value: TrwReportParam);
    function  FileNameIsNotEmpty: Boolean;
    procedure SetFileName(const Value: String);
    procedure SetAddToExistFile(const Value: Boolean);

  public
    constructor Create;
    procedure Assign(Source: TPersistent); override;
    function  Add(const AName: String; const AValue: Variant; const AStore: Boolean = False): TrwReportParam;
    function  IndexOf(AName: String): Integer;
    function  SetValueIfParamExists(AName: String; AValue: Variant): Boolean;
    function  ParamByName(AName: String): TrwReportParam;
    procedure SaveToStream(AStream: TStream);
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToFile(const AFileName: String);
    procedure LoadFromFile(const AFileName: String);
    procedure SaveToBlobField(AField: TBlobField);
    procedure ReadFromBlobField(AField: TBlobField);
    property  Items[Index: Integer]: TrwReportParam read GetItem write SetItem; default;
    property  AddToExistFile: Boolean read FAddToExistFile write SetAddToExistFile;
    procedure Clear; reintroduce;
    procedure ClearItemsOnly;
    procedure LoadFromParamArray(const a: TReportParamArrayType);
  published
    property FileName: String read FFileName write SetFileName stored FileNameIsNotEmpty;
    property Duplexing: Boolean read FDuplexing write FDuplexing default False;
    property Copies: Byte read FCopies write FCopies default 1;
    property Layers: TrwPrintableLayers read FLayers write FLayers default [];
  end;



  //Group of reports

  TrwRepParams = class(TCollectionItem)
  private
    FParams: TrwReportParams;
    FReturnParams: Boolean;
    FNBR: Integer;
    FLevel: String;
    FTag: String;
    FCaption: String;
    FVmrTag: String;
    FOverrideVmrMbGroupNbr: Integer;
    FOverrideVmrMiscTaxCheckType: TCheckType;
    FVmrPrNbr: Integer;
    FVmrCoNbr: Integer;
    FVmrEventDate: TDateTime;
    procedure SetParams(const Value: TrwReportParams);
    function  TagIsNotEmpty: Boolean;
    procedure ReadParams(Reader: TReader);
    procedure WriteParams(Writer: TWriter);
    function  CaptionIsNotEmpty: Boolean;

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    property Params: TrwReportParams read FParams write SetParams;
    property OverrideVmrMbGroupNbr: Integer read FOverrideVmrMbGroupNbr write FOverrideVmrMbGroupNbr;
    property OverrideVmrMiscTaxCheckType: TCheckType read FOverrideVmrMiscTaxCheckType write FOverrideVmrMiscTaxCheckType;

    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

  published
    property NBR: Integer read FNBR write FNBR;
    property Level: String read FLevel write FLevel;
    property Tag: String read FTag write FTag stored TagIsNotEmpty;
    property VmrTag: String read FVmrTag write FVmrTag;
    property VmrCoNbr: Integer read FVmrCoNbr write FVmrCoNbr;
    property VmrPrNbr: Integer read FVmrPrNbr write FVmrPrNbr;
    property VmrEventDate: TDateTime read FVmrEventDate write FVmrEventDate;
    property Caption: String read FCaption write FCaption stored CaptionIsNotEmpty;
    property ReturnParams: Boolean read FReturnParams write FReturnParams default False;
  end;


  TrwReportList = class(TrwParamCollection)
  private
    FDescr: string;
    function GetItem(Index: Integer): TrwRepParams;
    procedure SetParamsState(AState: TComponentState);

  public
    property Items[Index: Integer]: TrwRepParams read GetItem; default;
    constructor Create;
    function  GetAsStream: IisStream; override;
    procedure SetFromStream(const Value: IisStream); override;
    procedure   Assign(Source: TPersistent); override;
    function  AddReport: TrwRepParams; overload;
    function  AddReport(ANBR: Integer; ALevel: String; AParams: TrwReportParams; ATag: String = ''): TrwRepParams; overload;
    function  FindReport(const ANBR: Integer; const ALevel: String): TrwRepParams; overload;
    function  FindReport(const ATag: String): TrwRepParams; overload;
  published
    property  Descr: string read FDescr write FDescr;
  end;


  //Results of report group

  TrwResPageInfo = class(TCollectionItem)
  private
    FSimplexPageCount: Integer;
    FDuplexPageCount: Integer;
    FWidthMM: Integer;
    FHeightMM: Integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property WidthMM: Integer read FWidthMM write FWidthMM;
    property HeightMM: Integer read FHeightMM write FHeightMM;
    property SimplexPageCount: Integer read FSimplexPageCount write FSimplexPageCount default 0;
    property DuplexPageCount: Integer read FDuplexPageCount write FDuplexPageCount default 0;
  end;


  TrwResPagesInfo = class(TCollection)
  private
    function GetItems(Index: Integer): TrwResPageInfo;
  public
    property Items[Index: Integer]: TrwResPageInfo read GetItems; default;
    function PageIndexBySize(const AWidth, AHeight: Integer): Integer;
    function AddPageInfo(const AWidth, AHeight: Integer; const DuplexPage: Boolean): TrwResPageInfo;
    procedure CorrectPageCounts;
  end;


  TrwDuplexPrintType = (rdpDefault, rdpDuplex, rdpSimplex);

  TevRWPrinterRec = record
    Duplexing:               TrwDuplexPrintType;
    PrinterVerticalOffset:   Integer;
    PrinterHorizontalOffset: Integer;
    PrinterName:             String;
  end;


  TrwReportResult = class(TCollectionItem)
  private
    FNBR: Integer;
    FLevel: String;
    FReportName: String;
    FReportType: TReportType;
    FData: IEvDualStream;
    FErrorMessage: String;
    FWarningMessage: String;
    FTag: String;
    FTray: String;
    FOutBinNbr: Integer;
    FLayers: TrwPrintableLayers;
    FCopies: Byte;
    FFileName: String;
    FVmrTag: String;
    FMediaType: String;
    FPagesInfo: TrwResPagesInfo;
    FTaxReturnVmrTag: string;
    FVmrCoNbr: Integer;
    FVmrPrNbr: Integer;
    FVmrJobDescr: String;
    FVmrFileName: string;
    FVmrEventDate: TDateTime;
    FVmrEeNbr: Integer;
    FReturnValues: TrwReportParams;
    FPrinterInfo: TevRWPrinterRec;
    FSecurePreviewMode: Boolean;
    FVmrConsolidation: boolean;

    procedure ReadParams(Reader: TReader);
    procedure WriteParams(Writer: TWriter);
    function TagIsNotEmpty: Boolean;
    function FileNameIsNotEmpty: Boolean;
    function TrayIsNotEmpty: Boolean;
    procedure SetPagesInfo(const Value: TrwResPagesInfo);
    procedure SetReturnValues(const Value: TrwReportParams);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    property Data: IEvDualStream read FData write FData;
    property TaxReturnVmrTag: string read FTaxReturnVmrTag write FTaxReturnVmrTag;
    property VmrFileName: string read FVmrFileName write FVmrFileName;
    property PrinterInfo: TevRWPrinterRec read FPrinterInfo write FPrinterInfo;
    property SecurePreviewMode: Boolean read FSecurePreviewMode write FSecurePreviewMode;
    property VmrConsolidation: boolean read FVmrConsolidation write FVmrConsolidation;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure   Assign(Source: TPersistent); override;

  published
    property NBR: Integer read FNBR write FNBR;
    property Level: String read FLevel write FLevel;
    property Tag: String read FTag write FTag stored TagIsNotEmpty;
    property VmrTag: String read FVmrTag write FVmrTag;
    property VmrCoNbr: Integer read FVmrCoNbr write FVmrCoNbr;
    property VmrPrNbr: Integer read FVmrPrNbr write FVmrPrNbr;
    property VmrEeNbr: Integer read FVmrEeNbr write FVmrEeNbr;
    property VmrEventDate: TDateTime read FVmrEventDate write FVmrEventDate;
    property VmrJobDescr: String read FVmrJobDescr write FVmrJobDescr;
    property MediaType: String read FMediaType write FMediaType;
    property PagesInfo: TrwResPagesInfo read FPagesInfo write SetPagesInfo;
    property Layers: TrwPrintableLayers read FLayers write FLayers;
    property Copies: Byte read FCopies write FCopies default 1;
    property Tray: String read FTray write FTray stored TrayIsNotEmpty;
    property OutBinNbr: Integer read FOutBinNbr write FOutBinNbr default 0;
    property FileName: String read FFileName write FFileName stored FileNameIsNotEmpty;
    property ReportName: String read FReportName write FReportName;
    property ReportType: TReportType read FReportType write FReportType;
    property ErrorMessage: String read FErrorMessage write FErrorMessage;
    property WarningMessage: String read FWarningMessage write FWarningMessage;
    property ReturnValues: TrwReportParams read FReturnValues write SetReturnValues;

  end;


  TrwReportResults = class(TrwParamCollection)
  private
    FLocation : string;
    FPrinterName: String;
    FDescr: string;
    FPrReprintHistoryNBR: Integer;
    function GetItems(Index: Integer): TrwReportResult;
    function PrinterNameNotEmpty: Boolean;
  protected
    function GetEstimatedMemorySize: LongInt; override;
  public
    property  Items[Index: Integer]: TrwReportResult read GetItems; default;
    constructor Create; overload;
    constructor Create(const ms: IisStream); overload;
    function  Add: TrwReportResult; reintroduce;
    function  AddReportResult(const AReportName: String; const AReportType: TReportType; const AData: IEvDualStream): TrwReportResult;
    function  Insert(AIndex: Integer): TrwReportResult; reintroduce;
    function  InsertReportResult(const AIndex: Integer; const AReportName: String; const AReportType: TReportType; const AData: IEvDualStream): TrwReportResult;
    function  FindReport(const ANBR: Integer; const ALevel: String): TrwReportResult; overload;
    function  FindReport(const ATag: String): TrwReportResult; overload;
    procedure  AddReportResults(const r: TrwReportResults); overload;
    procedure  AddReportResults(const m: IisStream); overload;
    procedure  Assign(Source: TPersistent); override;
    procedure MoveTo(const Target: TrwReportResults);
    function  GetAllExceptions: String;
    function  GetAllWarnings: String;
  published
    property Location : String read FLocation write FLocation;
    property PrinterName: String read FPrinterName write FPrinterName stored PrinterNameNotEmpty;
    property Descr: string read FDescr write FDescr;
    property PrReprintHistoryNBR: Integer read FPrReprintHistoryNBR write FPrReprintHistoryNBR;

  end;


implementation

const
  BuffSize = 4 * 1024;

type

  //Workaround
  //Feature-bug in Delphi. There is no ability to write/read published properties of TCollection

  TrwWriter = class(TWriter)
  public
    procedure WriteCollection(Value: TCollection); reintroduce;
  end;


  TrwReader = class(TReader)
  public
    procedure ReadCollection(Collection: TCollection); reintroduce;
  end;


  TevVariantToStreamHolder = class
  private
    FStream: TStream;
    function InternalTypeToVarType(AIntType: Byte): Integer;
    function ReadArray: Variant;
    function ReadElement(AType: Byte): Variant;
  public
    function StreamToVarArray(const AStream: TStream): Variant;
  end;


procedure StreamToVariant(const FStream: TStream; var Result: Variant); overload;
var
  VarType: Integer;
  I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
  StrLen: Integer;
  ArrayData: Pointer;
  ArrayDims: array of Integer;
  IntStream: IEvDualStream;
begin
  FStream.ReadBuffer(VarType, SizeOf(Integer));
  if (VarType and varArray) <> 0 then
  begin
    FStream.ReadBuffer(DimCount, SizeOf(DimCount));
    SetLength(ArrayDims, DimCount * 2);
    ElemCount:= 1;
    for I:= 0 to DimCount - 1 do
    begin
      FStream.ReadBuffer(DimElemCount , SizeOf(DimElemCount));
      ElemCount:= ElemCount * DimElemCount;
      ArrayDims[I * 2]:= 0;
      ArrayDims[I * 2 + 1]:= DimElemCount - 1;
    end;
    Result:= VarArrayCreate(ArrayDims, VarType and varTypeMask);
    try
      ArrayData:= VarArrayLock(Result);
      for I:= 0 to ElemCount - 1 do
      begin
        case VarType and varTypeMask of
          varShortInt, varByte:
          begin
            ElemSize:= SizeOf(Byte);
            FStream.ReadBuffer(PByte(ArrayData)^, ElemSize);
          end;
          varSmallint, varWord:
          begin
            ElemSize:= SizeOf(SmallInt);
            FStream.ReadBuffer(PSmallInt(ArrayData)^, ElemSize);
          end;
          varInteger, varLongWord:
          begin
            ElemSize:= SizeOf(Integer);
            FStream.ReadBuffer(PInteger(ArrayData)^, ElemSize);
          end;
          varSingle:
          begin
            ElemSize:= SizeOf(Single);
            FStream.ReadBuffer(PSingle(ArrayData)^, ElemSize);
          end;
          varDouble:
          begin
            ElemSize:= SizeOf(Double);
            FStream.ReadBuffer(PDouble(ArrayData)^, ElemSize);
          end;
          varCurrency:
          begin
            ElemSize:= SizeOf(Currency);
            FStream.ReadBuffer(Currency(ArrayData^), ElemSize);
          end;
          varDate:
          begin
            ElemSize:= SizeOf(TDateTime);
            FStream.ReadBuffer(TDateTime(ArrayData^), ElemSize);
          end;
          varOleStr:
          begin
            ElemSize:= SizeOf(PWideChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            PWideString(ArrayData)^ := WideString(StringOfChar(' ', Len));
            FStream.ReadBuffer(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
          end;
{         varString:    This array item type is not supported by Delphi!
          begin
            ElemSize:= SizeOf(PChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            string(ArrayData^) := StringOfChar(' ', Len);
            FStream.Read(PChar(ArrayData)^, Len * SizeOf(Char));
          end;
}
          varBoolean:
          begin
            ElemSize:= SizeOf(WordBool);
            FStream.ReadBuffer(WordBool(ArrayData^), ElemSize);
          end;
          varVariant:
          begin
            ElemSize:= SizeOf(TVarData);
            Variant(PVarData(ArrayData)^) := Unassigned;
            StreamToVariant(FStream, Variant(PVarData(ArrayData)^));
          end;
        else
          raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
          ElemSize := 0;
        end;
        ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
      end;
    finally
      VarArrayUnlock(Result);
    end;
  end
  else
  begin
    case VarType and varTypeMask of
    varEmpty:
      Result := Unassigned;
    varNull:
      Result := null;
    varSmallint:
      FStream.ReadBuffer(TVarData(Result).VSmallint , SizeOf(TVarData(Result).VSmallint));
    varShortint:
      FStream.ReadBuffer(TVarData(Result).VShortint , SizeOf(TVarData(Result).VShortint));
    varInteger:
      FStream.ReadBuffer(TVarData(Result).VInteger , SizeOf(TVarData(Result).VInteger));
    varInt64:
      FStream.ReadBuffer(TVarData(Result).VInt64 , SizeOf(TVarData(Result).VInt64));
    varSingle:
      FStream.ReadBuffer(TVarData(Result).VSingle, SizeOf(TVarData(Result).VSingle));
    varDouble:
      FStream.ReadBuffer(TVarData(Result).VDouble, SizeOf(TVarData(Result).VDouble));
    varCurrency:
      FStream.ReadBuffer(TVarData(Result).VCurrency, SizeOf(TVarData(Result).VCurrency));
    varDate:
      FStream.ReadBuffer(TVarData(Result).VDate, SizeOf(TVarData(Result).VDate));
    varBoolean:
      FStream.ReadBuffer(TVarData(Result).VBoolean, SizeOf(TVarData(Result).VBoolean));
    varByte:
      FStream.ReadBuffer(TVarData(Result).VByte, SizeOf(TVarData(Result).VByte));
    varWord:
      FStream.ReadBuffer(TVarData(Result).VWord, SizeOf(TVarData(Result).VWord));
    varLongWord:
      FStream.ReadBuffer(TVarData(Result).VLongWord, SizeOf(TVarData(Result).VLongWord));
    varOleStr:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := WideString(StringOfChar(' ', StrLen));
      FStream.Read(TVarData(Result).VOleStr^, StrLen*SizeOf(WideChar));
    end;
    varString:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := StringOfChar(' ', StrLen);
      FStream.Read(TVarData(Result).VString^, StrLen*SizeOf(Char));
    end;
    varUnknown:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      if StrLen = -1 then
        IntStream := nil
      else
      begin
        IntStream := TEvDualStreamHolder.Create(StrLen);
        if StrLen > 0 then
        begin
          IntStream.RealStream.CopyFrom(FStream, StrLen);
          IntStream.Position := 0;
        end;
      end;

      Result := IntStream;
      IntStream := nil;
    end;

    else
      raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
    end;
    TVarData(Result).VType := VarType;
  end;
end;


function StreamToVariant(const FStream: TStream): Variant; overload;
var
  ver: byte;
begin
  FStream.ReadBuffer(ver, SizeOf(ver));
  Assert(ver = $FF);
  StreamToVariant(FStream, Result);
end;


function StreamToVarArray(const AStream: TStream): Variant;
var
  i: Integer;
  ver: byte;
begin
  i := AStream.Position;
  AStream.ReadBuffer(ver, SizeOf(ver));
  AStream.Position := i;
  if ver = $FF then
    Result := StreamToVariant(AStream)
  else
    with TevVariantToStreamHolder.Create do
    try
      Result := StreamToVarArray(AStream);
    finally
      Free;
    end;
end;


function VariantToStream(const AValue: Variant; const FStream: TStream = nil): TStream;

  procedure VarToStream(const AValue: Variant; const FStream: TStream; const FUseChunksForWriting: Boolean);
  var
    I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
    ArrayData: Pointer;

    procedure WriteData(const Buffer; const Count: Longint);
    begin
      if (FStream.Position + Count >= FStream.Size) and FUseChunksForWriting then
        FStream.Size := FStream.Size + 1024;
      FStream.WriteBuffer(Buffer, Count);
    end;

    procedure WriteInterfacedData(const AInt: IInterface);
    var
      S: IEvDualStream;
      n: Integer;
    begin
      S := AInt as IEvDualStream;
      if Assigned(S) then
      begin
        S.Position := 0;
        n := S.Size;
        FStream.WriteBuffer(n, SizeOf(n));
        FStream.CopyFrom(S.RealStream, n);
      end
      else
      begin
        n := -1;
        FStream.WriteBuffer(n, SizeOf(n));
      end;
    end;

  begin
    i := VarType(AValue);
    WriteData(i, SizeOf(i));
    if VarIsArray(AValue) then
    begin
      DimCount:= VarArrayDimCount(AValue);
      WriteData(DimCount, SizeOf(DimCount));
      ElemCount:= 1;
      for I:= 0 to DimCount - 1 do
      begin
        DimElemCount:= VarArrayHighBound(AValue, I + 1) - VarArrayLowBound(AValue, I + 1) + 1;
        ElemCount:= ElemCount * DimElemCount;
        WriteData(DimElemCount, SizeOf(DimElemCount));
      end;
      try
        ArrayData:= VarArrayLock(AValue);
        for I:= 0 to ElemCount - 1 do
        begin
          case VarType(AValue) and varTypeMask of
            varShortInt, varByte:
            begin
              ElemSize:= SizeOf(Byte);
              WriteData(PByte(ArrayData)^, ElemSize);
            end;
            varSmallint, varWord:
            begin
              ElemSize:= SizeOf(SmallInt);
              WriteData(PSmallInt(ArrayData)^, ElemSize);
            end;
            varInteger, varLongWord:
            begin
              ElemSize:= SizeOf(Integer);
              WriteData(PInteger(ArrayData)^, ElemSize);
            end;
            varSingle:
            begin
              ElemSize:= SizeOf(Single);
              WriteData(PSingle(ArrayData)^, ElemSize);
            end;
            varDouble:
            begin
              ElemSize:= SizeOf(Double);
              WriteData(PDouble(ArrayData)^, ElemSize);
            end;
            varCurrency:
            begin
              ElemSize:= SizeOf(Currency);
              WriteData(PCurrency(ArrayData)^, ElemSize);
            end;
            varDate:
            begin
              ElemSize:= SizeOf(TDateTime);
              WriteData(PDateTime(ArrayData)^, ElemSize);
            end;
            varOleStr:
              begin
                ElemSize:= SizeOf(PWideChar);
                Len := Length(PWideString(ArrayData)^);
                WriteData(Len, SizeOf(Len));
                WriteData(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
              end;
{           varString:  This array item type is not supported by Delphi!
              begin
                ElemSize:= SizeOf(PChar);
                Len := Length(PChar(ArrayData));
                WriteData(StrLen, SizeOf(Len));
                WriteData(String(PChar(ArrayData))[1], Len * SizeOf(Char));
              end;
}
            varBoolean:
            begin
              ElemSize:= SizeOf(WordBool);
              WriteData(PWordBool(ArrayData)^, ElemSize);
            end;
            varVariant:
            begin
              ElemSize:= SizeOf(TVarData);
              VarToStream(Variant(PVarData(ArrayData)^), FStream, FUseChunksForWriting);
            end;
          else
            raise EisException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
            ElemSize := 0;
          end;
          ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
        end;
      finally
        VarArrayUnlock(AValue);
      end;
    end
    else
    begin
      case VarType(AValue) of
        varEmpty, varNull:;
        varShortInt, varByte:
          WriteData(TVarData(AValue).VByte, SizeOf(TVarData(AValue).VByte));
        varSmallint, varWord:
          WriteData(TVarData(AValue).VSmallInt, SizeOf(TVarData(AValue).VSmallInt));
        varInteger, varLongWord:
          WriteData(TVarData(AValue).VInteger, SizeOf(TVarData(AValue).VInteger));
        varInt64:
          WriteData(TVarData(AValue).VInt64, SizeOf(TVarData(AValue).VInt64));
        varSingle:
          WriteData(TVarData(AValue).VSingle, SizeOf(TVarData(AValue).VSingle));
        varDouble:
          WriteData(TVarData(AValue).VDouble, SizeOf(TVarData(AValue).VDouble));
        varCurrency:
          WriteData(TVarData(AValue).VCurrency, SizeOf(TVarData(AValue).VCurrency));
        varDate:
          WriteData(TVarData(AValue).VDate, SizeOf(TVarData(AValue).VDate));
        varOleStr:
          begin
            i := Length(TVarData(AValue).VOleStr);
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VOleStr^, i*SizeOf(WideChar));
          end;
        varString:
          begin
            i := Length(PChar(TVarData(AValue).VString));
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VString^, i*SizeOf(Char));
          end;
        varBoolean:
          WriteData(TVarData(AValue).VBoolean, SizeOf(TVarData(AValue).VBoolean));
        varUnknown:
          WriteInterfacedData(IInterface(TVarData(AValue).VUnknown));
      else
        raise EisException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
      end;
    end;
  end;
var
  ver: Byte;
begin
  if Assigned(FStream) then
    Result := FStream
  else
    Result := TisMemoryStream.Create;
  ver := $FF;
  Result.WriteBuffer(ver, SizeOf(ver));
  VarToStream(AValue, Result, VarIsArray(AValue));
  Result.Size := Result.Position;
end;


function VarArrayToStream(const vData: Variant; const AStream: TStream = nil; const AOleStrToStr: Boolean = False): TStream;
begin
  Result := VariantToStream(vData, AStream);
end;

procedure BoolToStream(const b: Boolean; const str: TStream);
begin
  str.WriteBuffer(b, SizeOf(b));
end;

function StreamToBool(const str: TStream): Boolean;
begin
  str.ReadBuffer(Result, SizeOf(Result));
end;

procedure WriteStreamToWriter(const Writer: TWriter; const Str: IEvDualStream);
var
  Count: Integer;
  p: ^Byte;
  i: Integer;
begin
  if Assigned(Str) then
    Count := Str.Size
  else
    Count := 0;
  Writer.Write(Count, SizeOf(Count));
  if Count > 0 then
    if Assigned(Str.Memory) then
      Writer.Write(Str.Memory^, Count)
    else
    begin
      Str.Position := 0;
      GetMem(p, BuffSize);
      try
        while Count > 0 do
        begin
          i := Min(Count, BuffSize);
          Str.ReadBuffer(p^, i);
          Writer.Write(p^, i);
          Dec(Count, i);
        end;
      finally
        FreeMem(p);
      end;
    end;
end;


function ReadStreamFromReader(const Reader: TReader; const Str: IisStream = nil; const CreateIfEmpty: Boolean = True): IisStream;
var
  Count: Integer;
  p: ^Byte;
  i: Integer;
begin
  Result := Str;
  Reader.Read(Count, SizeOf(Count));
  if (Count > 0) then
  begin
    if not Assigned(Result) then
      Result := TisStream.Create(Count, True);
    Result.Size := Count;
    Result.Position := 0;
    if Assigned(Result.Memory) then
      Reader.Read(Result.Memory^, Count)
    else
    begin
      GetMem(p, BuffSize);
      try
        while Count > 0 do
        begin
          i := Min(Count, BuffSize);
          Reader.Read(p^, i);
          Result.WriteBuffer(p^, i);
          Dec(Count, i);
        end;
      finally
        FreeMem(p);
      end;
    end;
  end
  else
  if Assigned(Result) then
    Result.Size := Count
  else
  if CreateIfEmpty then
    Result := TisStream.Create;
  if Assigned(Result) then
    Result.Position := 0;
end;



{ TrwReportParam }

constructor TrwReportParam.Create(Collection: TCollection);
begin
  inherited;
  FStore := True;
end;

procedure TrwReportParam.Assign(Source: TPersistent);
begin
  FName := TrwReportParam(Source).Name;
  Store := TrwReportParam(Source).Store;
  Value := TrwReportParam(Source).Value;
end;

procedure TrwReportParam.SetArrItemValue(AIndex: Integer; AValue: Variant);
begin
  FValue[AIndex] := AValue;
end;

procedure TrwReportParam.SetValue(const AValue: Variant);
var
  MS: TStringStream;
  S: String;
begin
  if (FVarType and varArray) <> 0 then
    if (Variants.VarType(AValue) = varString) or (Variants.VarType(AValue) = varOleStr) then
    begin
      S := AValue;
      if Length(S) = 0 then
        FValue := varArrayCreate([0, -1], varVariant)
      else
      begin
        MS := TStringStream.Create(S);
        try
          FValue := StreamToVarArray(MS);
        finally
          MS.Free;
        end;
        S := '';
      end;
    end
    else
      FValue := AValue

  else
  begin
    if TrwReportParams(Collection).FState = [csReading] then
      FValue := VarAsType(AValue, FVarType)
    else
      FValue := AValue;
  end;

  FVarType := Variants.varType(FValue);
end;


procedure TrwReportParam.SetVarType(const Value: Integer);
begin
  if FVarType <> Value then
  begin
    FValue := Unassigned;
    FVarType := Value;
  end;
end;

procedure TrwReportParam.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('VarValue', ReadValue, WriteValue, True);
end;

procedure TrwReportParam.ReadValue(Reader: TReader);
var
  MS: TMemoryStream;
  l: Integer;
begin
  if (FVarType and varArray) <> 0 then
  begin
    l := 0;
    if Reader.ReadValue = vaBinary then
      Reader.Read(l, SizeOf(Integer))
    else
      Assert(False, 'Error of reading array data');

    MS := TisMemoryStream.Create;
    try
      MS.SetSize(l);
      Reader.Read(MS.Memory^, l);
      FValue := StreamToVarArray(MS);
    finally
      MS.Free;
    end;
  end
  else
    Value := Reader.ReadVariant;
end;


procedure TrwReportParam.WriteValue(Writer: TWriter);
var
  l: Integer;
  t: TValueType;
  MS: TMemoryStream;
begin
  if (FVarType and varArray) <> 0 then
  begin
    MS := TMemoryStream(VarArrayToStream(FValue, nil, True));
    try
      l := MS.Size;
      t := vaBinary;
      Writer.Write(t, SizeOf(t));
      Writer.Write(l, SizeOf(Integer));
      Writer.Write(MS.Memory^, l);
    finally
      MS.Free;
    end;
  end
  else
    Writer.WriteVariant(FValue);
end;


{ TrwReportParams }

function TrwReportParams.Add(const AName: String; const AValue: Variant; const AStore: Boolean = False): TrwReportParam;
var
  i: Integer;
begin
  i := IndexOf(AName);
  if i = -1 then
  begin
    Result := TrwReportParam(inherited Add);
    Result.Name := AName;
  end
  else
    Result := Items[i];

  REsult.Store := AStore;
  Result.Value := AValue;
end;


procedure TrwReportParams.Assign(Source: TPersistent);
begin
  inherited;

  FileName := TrwReportParams(Source).FFileName;
  Duplexing := TrwReportParams(Source).Duplexing;
  Copies := TrwReportParams(Source).Copies;
  Layers := TrwReportParams(Source).Layers;
end;

procedure TrwReportParams.Clear;
begin
  inherited Clear;
  FileName := '';
  FAddToExistFile := False;
  Duplexing := False;
  FCopies := 1;
  FLayers := [];
end;

procedure TrwReportParams.ClearItemsOnly;
begin
  inherited Clear;
end;

constructor TrwReportParams.Create;
begin
  inherited Create(TrwReportParam);
  FState := [];
  Clear;
end;


function TrwReportParams.FileNameIsNotEmpty: Boolean;
begin
  Result := Length(FFIleName) > 0;
end;

function TrwReportParams.GetItem(Index: Integer): TrwReportParam;
begin
  Result := TrwReportParam(inherited GetItem(Index));
end;

function TrwReportParams.IndexOf(AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AName := AnsiUpperCase(AName);

  for i := 0 to Count-1 do
    if AnsiSameStr(UpperCase(Items[i].Name), AName) then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwReportParams.LoadFromFile(const AFileName: String);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TrwReportParams.LoadFromParamArray(
  const a: TReportParamArrayType);
var
  i: Integer;
begin
  for i := 0 to High(a) do
    Add(a[i].ParamName, a[i].ParamValue);
end;

procedure TrwReportParams.LoadFromStream(AStream: TStream);
var
  Reader: TrwReader;
begin
  FState := [csReading];
  Reader := TrwReader.Create(AStream, 1024);
  try
    Reader.ReadCollection(Self);
  finally
    Reader.Free;
    FState := [];
  end;
end;

function TrwReportParams.ParamByName(AName: String): TrwReportParam;
var
  i: Integer;
begin
  i := IndexOf(AName);
  if i = -1 then
    Result := nil
  else
    Result := Items[i];  
end;

procedure TrwReportParams.ReadFromBlobField(AField: TBlobField);
var
  MS: IisStream;
begin
  Clear;
  if AField.DataSet is TevClientDataSet then
    MS :=  TevClientDataSet(AField.DataSet).GetBlobData(AField.FieldName)
  else begin
    MS := TisStream.Create;
    AField.SaveToStream(MS.RealStream);
    MS.Position := 0;
  end;
  LoadFromStream(MS.RealStream);
end;

procedure TrwReportParams.SaveToBlobField(AField: TBlobField);
var
  MS: IEvDualStream;
  SavePar: TrwReportParams;
  i: Integer;
begin
  SavePar := TrwReportParams.Create;
  try
    SavePar.Assign(Self);
    i := 0;
    while i < SavePar.Count do
      if SavePar[i].Store then
        Inc(i)
      else
        SavePar[i].Free;

    MS := TEvDualStreamHolder.Create;
    SavePar.SaveToStream(MS.RealStream);
    MS.Position := 0;
    AField.LoadFromStream(MS.RealStream);

  finally
    SavePar.Free;
  end;
end;

procedure TrwReportParams.SaveToFile(const AFileName: String);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TrwReportParams.SaveToStream(AStream: TStream);
var
  Writer: TrwWriter;
begin
  FState := [csWriting];
  Writer := TrwWriter.Create(AStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
    FState := [];
  end;
end;


procedure TrwReportParams.SetAddToExistFile(const Value: Boolean);
begin
  if Length(FFileName) = 0 then
    Exit;

  FAddToExistFile := Value;

  if FFileName[Length(FFileName)] = '+' then
    System.Delete(FFileName, Length(FFileName), 1);

  if Value then
    FFileName := FFileName + '+';
end;

procedure TrwReportParams.SetFileName(const Value: String);
begin
  FFileName := Trim(Value);

  if FileNameIsNotEmpty then
    FAddToExistFile := (FFileName[Length(FFileName)] = '+')
  else
    FAddToExistFile := False;
end;

procedure TrwReportParams.SetItem(Index: Integer; const Value: TrwReportParam);
begin
  inherited SetItem(Index, Value);
end;


function TrwReportParams.SetValueIfParamExists(AName: String; AValue: Variant): Boolean;
var
  i: Integer;
begin
  i := IndexOf(AName);

  if i = -1 then
    Result := False
  else
  begin
    Items[i].Value := AValue;
    Result := True;
  end;
end;



{ TrwReportResults }

function TrwReportResults.Add: TrwReportResult;
begin
  Result := TrwReportResult(inherited Add);
end;

function TrwReportResults.AddReportResult(const AReportName: String;
  const AReportType: TReportType; const AData: IEvDualStream): TrwReportResult;
begin
  Result := Add;
  Result.ReportName := AReportName;
  Result.ReportType := AReportType;
  Result.Data := AData;
end;


procedure TrwReportResults.AddReportResults(const r: TrwReportResults);
var
  i: Integer;
begin
  for i := 0 to r.Count-1 do
    Add.Assign(r[i]);
end;

procedure TrwReportResults.AddReportResults(const m: IisStream);
var
  r: TrwReportResults;
begin
  if StreamIsAssigned(m) then
  begin
    m.Position := 0;
    r := TrwReportResults.Create;
    try
      r.SetFromStream(m);
      AddReportResults(r);
    finally
      r.Free;
    end;
  end;
end;

procedure TrwReportResults.Assign(Source: TPersistent);
begin
  inherited;
  FPrinterName := TrwReportResults(Source).PrinterName;
  FDescr := TrwReportResults(Source).Descr;
end;

constructor TrwReportResults.Create;
begin
  inherited Create(TrwReportResult);
end;


function TrwReportResults.FindReport(const ANBR: Integer; const ALevel: String): TrwReportResult;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if (Items[i].NBR = ANBR) and AnsiSameStr(Items[i].Level, ALevel) then
    begin
      Result := Items[i];
      break;
    end;
end;


constructor TrwReportResults.Create(const ms: IisStream);
begin
  Create;
  SetFromStream(ms);
end;

function TrwReportResults.FindReport(const ATag: String): TrwReportResult;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if AnsiSameStr(Items[i].Tag, ATag) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrwReportResults.GetEstimatedMemorySize: LongInt;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count- 1 do
    with Items[i] do
    begin
      Inc(Result, Length(Tag)+ Length(ErrorMessage)+ Length(WarningMessage));
      if Assigned(Data) then
        Inc(Result, Data.Size+ 4);
    end;
end;


function TrwReportResults.GetItems(Index: Integer): TrwReportResult;
begin
  Result := TrwReportResult(inherited Items[Index]);
end;




function TrwReportResults.Insert(AIndex: Integer): TrwReportResult;
begin
  Result := TrwReportResult(inherited Insert(AIndex));
end;

function TrwReportResults.InsertReportResult(const AIndex: Integer;
  const AReportName: String; const AReportType: TReportType;
  const AData: IEvDualStream): TrwReportResult;
begin
  Result := Insert(AIndex);
  Result.ReportName := AReportName;
  Result.ReportType := AReportType;
  Result.Data := AData;
end;

function TrwReportResults.PrinterNameNotEmpty: Boolean;
begin
  Result := Length(FPrinterName) > 0;
end;


procedure TrwReportResults.MoveTo(const Target: TrwReportResults);
var
  i: Integer;
  L: Tlist;
begin
  L := TList.Create;
  try
    for i := 0 to Count - 1 do
      L.Add(Items[i]);

    for i := 0 to L.Count - 1 do
      TCollectionItem(L[i]).Collection := Target;
  finally
    L.Free;
  end;
end;

function TrwReportResults.GetAllExceptions: String;
var
  i: Integer;
begin
  Result := '';

  for i := 0 to Count - 1 do
  begin
    if Items[i].ErrorMessage <> '' then
      AddStrValue(Result, Items[i].ReportName + ': ' + Items[i].ErrorMessage, #13#13);
  end;
end;

function TrwReportResults.GetAllWarnings: String;
var
  i: Integer;
begin
  Result := '';

  for i := 0 to Count - 1 do
  begin
    if Items[i].WarningMessage <> '' then
      AddStrValue(Result, Items[i].ReportName + ': ' + Items[i].WarningMessage, #13#13);
  end;
end;

{ TrwReportResult }


procedure TrwReportResult.Assign(Source: TPersistent);
begin
  NBR := (Source as TrwReportResult).Nbr;
  Level := TrwReportResult(Source).Level;
  Tag := TrwReportResult(Source).Tag;
  VmrTag := TrwReportResult(Source).VmrTag;
  VmrCoNbr := TrwReportResult(Source).VmrCoNbr;
  VmrPrNbr := TrwReportResult(Source).VmrPrNbr;
  VmrEeNbr := TrwReportResult(Source).VmrEeNbr;
  VmrEventDate := TrwReportResult(Source).VmrEventDate;
  MediaType := TrwReportResult(Source).MediaType;
  Layers := TrwReportResult(Source).Layers;
  Copies := TrwReportResult(Source).Copies;
  Tray := TrwReportResult(Source).Tray;
  OutBinNbr := TrwReportResult(Source).OutBinNbr;
  FileName := TrwReportResult(Source).FileName;
  ReportName := TrwReportResult(Source).ReportName;
  ReportType := TrwReportResult(Source).ReportType;
  ErrorMessage := TrwReportResult(Source).ErrorMessage;
  WarningMessage := TrwReportResult(Source).WarningMessage;
  Data := TrwReportResult(Source).Data;
  FPagesInfo.Assign(TrwReportResult(Source).PagesInfo);
  FReturnValues.Assign(TrwReportResult(Source).ReturnValues);
  FPrinterInfo := TrwReportResult(Source).PrinterInfo;
  FSecurePreviewMode := TrwReportResult(Source).SecurePreviewMode;
end;

constructor TrwReportResult.Create(Collection: TCollection);
begin
  inherited;
  FData := nil;
  FTag := '';
  FCopies := 1;
  FTray := '';
  FOutBinNbr := 0;
  FFileName := '';
  FErrorMessage := '';
  FWarningMessage := '';
  FVmrJobDescr := '';
  FVmrTag := '';
  FVmrCoNbr := 0;
  FVmrPrNbr := 0;
  FVmrEeNbr := 0;
  FVmrEventDate := 0;
  FMediaType := MEDIA_TYPE_PLAIN_LETTER;
  FPagesInfo := TrwResPagesInfo.Create(TrwResPageInfo);
  FReturnValues := TrwReportParams.Create;
end;

destructor TrwReportResult.Destroy;
begin
  FPagesInfo.Free;
  FReturnValues.Free;
// ticket 103738 - added explicit release of data stream
  FData := nil;

  inherited;
end;

procedure TrwReportResult.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Params2', ReadParams, WriteParams, True);
end;

function TrwReportResult.FileNameIsNotEmpty: Boolean;
begin
  Result := Length(FFileName) > 0;
end;

procedure TrwReportResult.ReadParams(Reader: TReader);
begin
  if Reader.ReadBoolean then
    FData := ReadStreamFromReader(Reader, nil, False);
end;

procedure TrwReportResult.SetPagesInfo(const Value: TrwResPagesInfo);
begin
  FPagesInfo.Assign(Value);
end;


function TrwReportResult.TagIsNotEmpty: Boolean;
begin
  Result := Length(Tag) > 0;
end;



{ TrwRepRes }

procedure TrwRepParams.Assign(Source: TPersistent);
begin
  NBR := TrwRepParams(Source).NBR;
  Level := TrwRepParams(Source).Level;
  Tag := TrwRepParams(Source).Tag;
  Caption := TrwRepParams(Source).Caption;
  Params := TrwRepParams(Source).Params;
  VmrTag := TrwRepParams(Source).VmrTag;
  VmrCoNbr := TrwRepParams(Source).VmrCoNbr;
  VmrPrNbr := TrwRepParams(Source).VmrPrNbr;
  VmrEventDate := TrwRepParams(Source).VmrEventDate;
  OverrideVmrMbGroupNbr := TrwRepParams(Source).OverrideVmrMbGroupNbr;
  OverrideVmrMiscTaxCheckType := TrwRepParams(Source).OverrideVmrMiscTaxCheckType;
  ReturnParams := TrwRepParams(Source).ReturnParams;
end;

function TrwRepParams.CaptionIsNotEmpty: Boolean;
begin
  Result := Length(Caption) > 0;
end;

constructor TrwRepParams.Create(Collection: TCollection);
begin
  inherited;
  FParams := TrwReportParams.Create;
  FTag := '';
  Caption := '';
  FVmrTag := '';
  FVmrCoNbr := 0;
  FVmrPrNbr := 0;
  FVmrEventDate := 0;
  FOverrideVmrMbGroupNbr := 0;
  FOverrideVmrMiscTaxCheckType := ctRegular;
end;


procedure TrwRepParams.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Params', ReadParams, WriteParams, True);
end;

destructor TrwRepParams.Destroy;
begin
  FParams.Free;
  inherited;
end;


procedure TrwRepParams.ReadParams(Reader: TReader);
begin
  Params.Clear;
  TrwReader(Reader).ReadCollection(Params);
end;

procedure TrwRepParams.SetParams(const Value: TrwReportParams);
begin
  FParams.Assign(Value);
end;



{ TrwRepResList }

function TrwReportList.AddReport(ANBR: Integer; ALevel: String; AParams: TrwReportParams; ATag: String = ''): TrwRepParams;
begin
  Result := TrwRepParams(Add);
  Result.NBR := ANBR;
  Result.Level := ALevel;
  Result.Tag := ATag;
  Result.Params := AParams;
end;


function TrwReportList.AddReport: TrwRepParams;
begin
  Result := TrwRepParams(Add);
end;


procedure TrwReportList.Assign(Source: TPersistent);
begin
  inherited;
  FDescr := TrwReportList(Source).Descr;
end;

constructor TrwReportList.Create;
begin
  inherited Create(TrwRepParams);
end;

function TrwReportList.FindReport(const ANBR: Integer; const ALevel: String): TrwRepParams;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if (Items[i].NBR = ANBR) and AnsiSameStr(Items[i].Level, ALevel) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrwReportList.FindReport(const ATag: String): TrwRepParams;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if AnsiSameStr(Items[i].Tag, ATag) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrwReportList.GetAsStream: IisStream;
begin
  SetParamsState([csWriting]);
  Result := inherited GetAsStream;
  SetParamsState([]);
end;


function TrwReportList.GetItem(Index: Integer): TrwRepParams;
begin
  Result := TrwRepParams(inherited Items[Index]);
end;


procedure TrwReportResult.WriteParams(Writer: TWriter);
begin
  if Assigned(FData) then
  begin
    Writer.WriteBoolean(True);
    WriteStreamToWriter(Writer, FData);
  end
  else
    Writer.WriteBoolean(False);
end;


function TrwReportResult.TrayIsNotEmpty: Boolean;
begin
  Result := Length(FTray) > 0;
end;



procedure TrwReportResult.SetReturnValues(const Value: TrwReportParams);
begin
  FReturnValues.Assign(Value);
end;

{ TrwParamCollection }

function TrwParamCollection.GetAsStream: IisStream;
var
  Writer: TrwWriter;
begin
  Result := TisStream.Create(GetEstimatedMemorySize);
  Writer := TrwWriter.Create(Result.RealStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
  end;
  Result.Size := Result.Position;
end;


procedure TrwReportList.SetFromStream(const Value: IisStream);
begin
  SetParamsState([csReading]);
  inherited;
  SetParamsState([]);
end;


procedure TrwReportList.SetParamsState(AState: TComponentState);
var
  i: Integer;
begin
  for i := 0 to Count -1 do
    Items[i].FParams.FState := AState;
end;

function TrwParamCollection.GetEstimatedMemorySize: LongInt;
begin
  Result := -1;
end;

procedure TrwParamCollection.SetFromStream(const Value: IisStream);
var
  Reader: TrwReader;
begin
  Clear;
  if StreamIsAssigned(Value) then
  begin
    Reader := TrwReader.Create(Value.RealStream, 1024);
    try
      Reader.ReadCollection(Self);
    finally
      Reader.Free;
    end;
  end;
end;

function TrwRepParams.TagIsNotEmpty: Boolean;
begin
  Result := Length(Tag) > 0;
end;



{ TrwWriter }

procedure TrwWriter.WriteCollection(Value: TCollection);
var
  I: Integer;
begin
  if Value <> nil then
  begin
    WriteListBegin;
    WriteProperties(Value);
    WriteListEnd;
  end;

  WriteValue(vaCollection);
  if Value <> nil then
  begin
    for I := 0 to Value.Count - 1 do
    begin
      WriteListBegin;
      WriteProperties(Value.Items[I]);
      WriteListEnd;
    end;
  end;
  WriteListEnd;
end;


{ TrwReader }

procedure TrwReader.ReadCollection(Collection: TCollection);
var
  Item: TPersistent;
begin
  ReadListBegin;
  while not EndOfList do
    ReadProperty(Collection);
  ReadValue;
  ReadValue;

  Collection.BeginUpdate;
  try
    if not EndOfList then
      Collection.Clear;

    while not EndOfList do
    begin
      if NextValue in [vaInt8, vaInt16, vaInt32] then
        ReadInteger;
      Item := Collection.Add;
      ReadListBegin;
      while not EndOfList do
        ReadProperty(Item);
      ReadListEnd;
    end;
    ReadListEnd;

  finally
    Collection.EndUpdate;
  end;
end;

procedure TrwRepParams.WriteParams(Writer: TWriter);
begin
  TrwWriter(Writer).WriteCollection(Params);
end;



{ TrwResPageInfo }

procedure TrwResPageInfo.Assign(Source: TPersistent);
begin
  FWidthMM := TrwResPageInfo(Source).WidthMM;
  FHeightMM := TrwResPageInfo(Source).HeightMM;
  FSimplexPageCount := TrwResPageInfo(Source).SimplexPageCount;
  FDuplexPageCount := TrwResPageInfo(Source).DuplexPageCount;
end;



{ TrwResPagesInfo }

function TrwResPagesInfo.AddPageInfo(const AWidth, AHeight: Integer; const DuplexPage: Boolean): TrwResPageInfo;
var
  i: Integer;
begin
  i := PageIndexBySize(AWidth, AHeight);
  if i = -1 then
  begin
    Result := TrwResPageInfo(Add);
    Result.WidthMM := AWidth;
    Result.HeightMM := AHeight;
  end
  else
    Result := Items[i];

  if DuplexPage then
    Inc(Result.FDuplexPageCount)
  else
  begin
    Inc(Result.FSimplexPageCount);

    if (Result.FDuplexPageCount mod 2) <> 0 then
    begin
      Dec(Result.FDuplexPageCount);
      Inc(Result.FSimplexPageCount);
    end;
  end;
end;

procedure TrwResPagesInfo.CorrectPageCounts;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if (Items[i].DuplexPageCount mod 2) <> 0 then
    begin
      Items[i].DuplexPageCount := Items[i].DuplexPageCount - 1;
      Items[i].SimplexPageCount := Items[i].SimplexPageCount + 1;
    end;
end;

function TrwResPagesInfo.GetItems(Index: Integer): TrwResPageInfo;
begin
  Result := TrwResPageInfo(inherited Items[Index]);
end;


function TrwResPagesInfo.PageIndexBySize(const AWidth,  AHeight: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to Count -1 do
    if (Items[i].WidthMM = AWidth) and (Items[i].HeightMM = AHeight) or
       (Items[i].WidthMM = AHeight) and (Items[i].HeightMM = AWidth) then
    begin
      Result := i;
      break;
    end;
end;


function TevVariantToStreamHolder.InternalTypeToVarType(AIntType: Byte): Integer;
var
  fArray: Boolean;
begin
  fArray := (AIntType and 128) <> 0;
  if fArray then
    AIntType := AIntType and 127;

  case AIntType of
    0:  Result := varEmpty;
    1:  Result := varNull;
    2:  Result := varSmallint;
    3:  Result := varInteger;
    4:  Result := varSingle;
    5:  Result := varDouble;
    6:  Result := varCurrency;
    7:  Result := varDate;
    8:  Result := varOleStr;
    9:  Result := varBoolean;
    10: Result := varVariant;
    11: Result := varByte;
    12: Result := varString;
    13: Result := varShortint;
  else
    raise EisException.Create('Variant type is not applicable');
  end;

  if fArray then
    Result := Result or varArray;
end;

function TevVariantToStreamHolder.ReadArray: Variant;
var
  i, n, m: Integer;
  t: Byte;
  P: Pointer;
begin
  FStream.Read(t, SizeOf(t));
  if (t and 128) = 0 then
    raise EisException.Create('Array type expected');
  FStream.Read(n, SizeOf(n));
  FStream.Read(m, SizeOf(m));
  t := t and 127;
  Result := VarArrayCreate([n, m], InternalTypeToVarType(t));

  if t = 11 then
  begin
    P := VarArrayLock(Result);
    try
      FStream.ReadBuffer(P^, m - n + 1);
    finally
      VarArrayUnLock(Result);
    end;
  end

  else
  begin
    if t = 10 then
      t := 0;
    for i := n to m do
      Result[i] := ReadElement(t);
  end;
end;

function TevVariantToStreamHolder.ReadElement(AType: Byte): Variant;
var
  vt: Integer;
  i: Integer;
  t: Byte;
begin
  i := FStream.Position;
  if AType = 0 then
    FStream.Read(t, SizeOf(t))
  else
    t := AType;
  vt := InternalTypeToVarType(t);
  Result := Unassigned;
  if (vt and varArray) = 0 then
    Result := VarAsType(Result, vt)
  else
  begin
    FStream.Position := i;
    Result := ReadArray;
    Exit;
  end;

  case vt of
    varEmpty:    Result := Unassigned;

    varNull:     VarAsType(Result, varNull);

    varSmallint: FStream.Read(TVarData(Result).VSmallint , SizeOf(TVarData(Result).VSmallint));

    varShortint: FStream.Read(TVarData(Result).VShortint , SizeOf(TVarData(Result).VShortint));

    varInteger:  FStream.Read(TVarData(Result).VInteger , SizeOf(TVarData(Result).VInteger));

    varInt64:    FStream.Read(TVarData(Result).VInt64 , SizeOf(TVarData(Result).VInt64));

    varSingle:   FStream.Read(TVarData(Result).VSingle, SizeOf(TVarData(Result).VSingle));

    varDouble:   FStream.Read(TVarData(Result).VDouble, SizeOf(TVarData(Result).VDouble));

    varCurrency: FStream.Read(TVarData(Result).VCurrency, SizeOf(TVarData(Result).VCurrency));

    varDate:     FStream.Read(TVarData(Result).VDate, SizeOf(TVarData(Result).VDate));

    varBoolean:  FStream.Read(TVarData(Result).VBoolean, SizeOf(TVarData(Result).VBoolean));

    varByte:     FStream.Read(TVarData(Result).VByte, SizeOf(TVarData(Result).VByte));

    varOleStr:   begin
                   FStream.Read(i, SizeOf(i));
                   Result := VarAsType(StringOfChar(' ', i), varOleStr);
                   FStream.Read(TVarData(Result).VOleStr^, i*SizeOf(WideChar));
                 end;

    varString:   begin
                   FStream.Read(i, SizeOf(i));
                   Result := VarAsType(StringOfChar(' ', i), varString);
                   FStream.Read(TVarData(Result).VString^, i*SizeOf(Char));
                 end;
  else
    raise EisException.Create('Variant type is not applicable');
  end;
end;

function TevVariantToStreamHolder.StreamToVarArray(const AStream: TStream): Variant;
begin
  FStream := AStream;
  try
    Result := ReadArray;
  finally
    FStream := nil;
  end;
end;



end.
