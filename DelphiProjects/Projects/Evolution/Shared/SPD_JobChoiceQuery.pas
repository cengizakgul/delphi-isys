// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_JobChoiceQuery;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_SafeChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure, 
  SPD_ChoiceFromListQueryEx, ActnList, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDicttemp, ISBasicClasses, SDataDictclient, EvClientDataSet,
  EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TJobChoiceQuery = class(TSafeChoiceFromListQuery)
    DM_COMPANY: TDM_COMPANY;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilter( flt: string );
  end;


implementation

uses
  evutils;
{$R *.DFM}

{ TJobChoiceQuery }

procedure TJobChoiceQuery.SetFilter(flt: string);
begin
  dgChoiceList.DataSource := nil;
  cdsChoiceList.Data := DM_COMPANY.CO_JOBS.Data;
  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
  dgChoiceList.DataSource := dsChoiceList;
end;

end.
