// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sDSMemento;

interface

uses
   SDDClasses, EvClientDataSet;

type
  IDSMementoManager = interface
['{4954D7A2-A5BA-499B-9022-6998A1C8C417}']
    procedure ActivatingFrameDueToGlobalLookup( frameClassName: string; ds: TArrayDS; newFrameClassName: string );
    procedure ActivatedFrame( frameClassName: string; ds: TArrayDS );
  end;

function CreateDSMementoManager: IDSMementoManager;

implementation

uses
  db, sysutils, typinfo, evutils, variants, sDataStructure, contnrs;

type
  TFieldValueRec = record
    Name: string;
    Value: Variant;
  end;

  TDSMementoRec = record
    Name: string;
    State: TDataSetState;
    Values: array of TFieldValueRec;
    OldNbr: Variant;
    NewNbr: Variant;
  end;

  TDSMemento = class
  private
    FDSMementos: array of TDSMementoRec;
    FSourceClassName: string;
    procedure StoreDS( ds: TEvClientDataSet );
    procedure Store( ads: TArrayDS );
//    function Dump: string;
  public
    constructor Create( ads: TArrayDS; aSourceClassName: string ); overload;
//    destructor Destroy; override;
    procedure Restore( ads: TArrayDS );
  end;

  TDSMementoManager = class( TInterfacedObject, IDSMementoManager )
  private
    FMementos: TObjectList;
    FGlobalLookupFrameClassName: string;
    function LastMemento: TDSMemento;
    procedure Start( frameClassName: string; ds: TArrayDS; newFrameClassName: string );
    procedure Rollback;
    procedure Commit;
    {IDSMementoManager}
    procedure ActivatingFrameDueToGlobalLookup( frameClassName: string; ds: TArrayDS; newFrameClassName: string );
    procedure ActivatedFrame( frameClassName: string; ds: TArrayDS );
  public
    constructor Create;
    destructor Destroy; override;
  end;

function CreateDSMementoManager: IDSMementoManager;
begin
  Result := TDSMementoManager.Create;
end;

{ TDSMemento }

constructor TDSMemento.Create(ads: TArrayDS; aSourceClassName: string);
var
  ds: TArrayDS;
  i: integer;
begin
  for i := low(ads) to high(ads) do //in order to sort using AddDSEx (not AddDS)
    AddDS( ads[i], ds );
  Store( ds );
  FSourceClassName := aSourceClassName;
  //ODS(Dump);
end;
{
function TDSMemento.Dump: string;
var
  i: integer;
  m: ^TDSMementoRec;
  j: integer;
begin
  Result := '';
  for i := low(FDSMementos) to high(FDSMementos) do
  begin
    m := @FDSMementos[i];
    Result := Result + Format( '%s: %s'#13#10, [m.Name, getenumname( typeinfo(TDataSetState), ord(m.State) ) ] );
    for j := low(m.Values) to high(m.Values) do
      with m.Values[j] do
        Result := Result + Format( '%s=<%s>'#13#10, [Name, Iff( VarIsNull(Value), 'Null', VarToStr(Value) )] );
    Result := Result + #13#10;
  end
end;
}

procedure TDSMemento.Restore(ads: TArrayDS);
  function DSByName( aName: string ): TEvClientDataSet;
  var
    i: integer;
  begin
    Result := nil;
    for i := low(ads) to high(ads) do
      if ads[i].Name = aname then
        Result := ads[i];
    Assert( assigned(Result) );
  end;

var
  ds: TevClientDataSet;

  procedure AssignFieldVal( fname: string; fvalue: Variant );
  begin
    if ds[fname] <> fvalue then
      ds[fname] := fvalue;
    //ODS( '%s.%s := <%s>', [ds.Name, fname, vartostr(fvalue)])
  end;
var
  i: integer;

  function GetNewNbrValue( table: string; vr: TFieldValueRec ): Variant;
  var
    k, j: integer;
    reffered_table: string;
    Parents: TddReferencesDesc;
  begin
    Result := Null;
    reffered_table := '';
    Parents := GetddTableClassByName(table).GetParentsDesc;
    for j := 0 to High(Parents) do
      if Parents[j].SrcFields[0] = vr.Name then
        reffered_table := Parents[j].Table.ClassName;
    for k := low(FDSMementos) to i-1 do
      if ('T' + FDSMementos[k].Name = reffered_table) and (FDSMementos[k].OldNbr = vr.Value) then
      begin
        Result := FDSMementos[k].NewNbr;
        break;
      end
  end;
var
  m: ^TDSMementoRec;
  j: integer;
  newNbrVal: Variant;
  savedInsertHandler: TDataSetNotifyEvent;
begin
  for i := low(FDSMementos) to high(FDSMementos) do
  begin
    m := @FDSMementos[i];
    //ODS( 'restoring %s', [m.Name] );
    ds := DSByName( m.Name );
    case m.State of
      dsInsert: begin
                  savedInsertHandler := ds.CustomBeforeInsert;
                  ds.CustomBeforeInsert := nil;
                  try
                    ds.Insert;
                  finally
                    ds.CustomBeforeInsert := savedInsertHandler;
                  end;
                end;
      dsEdit: ds.Edit;
    else
      Assert( false );
    end;
    m.NewNbr := ds[ m.Name + '_NBR' ];
    //ODS( 'old nbr = <%s> new nbr = <%s>', [VarToStr(m.OldNbr), VarToStr(m.NewNbr)] );
    for j := low(m.Values) to high(m.Values) do
      if m.Values[j].Name <> m.Name + '_NBR' then
        if (Copy( m.Values[j].Name, Length(m.Values[j].Name)-4, 4 ) = '_NBR') then
        begin
          newNbrVal := GetNewNbrValue( m.Name, m.Values[j] );
          if not VarIsNull( newNbrVal ) then
            AssignFieldVal( m.Values[j].Name, newNbrVal )
          else
            AssignFieldVal( m.Values[j].Name, m.Values[j].Value )
        end
        else
          AssignFieldVal( m.Values[j].Name, m.Values[j].Value )
  end;
end;

procedure TDSMemento.Store(ads: TArrayDS);
var
  i: integer;
begin
  for i := low(ads) to high(ads) do
    StoreDS( ads[i] );
end;

procedure TDSMemento.StoreDS(ds: TEvClientDataSet);
var
  i: integer;
  m: ^TDSMementoRec;
begin
  if ds.State in [dsInsert, dsEdit] then
  begin
    SetLength( FDSMementos, Length(FDSMementos) + 1 );
    m := @FDSMementos[high(FDSMementos)];
    m.Name := ds.Name;
    m.State := ds.State;
    m.OldNbr := ds[ds.Name + '_NBR'];
    for i := 0 to ds.Fields.Count-1 do
      if ds.Fields[i].FieldKind = fkData then
      begin
        SetLength( m.Values, Length(m.Values) + 1 );
        m.Values[high(m.Values)].Name := ds.Fields[i].FieldName;
        m.Values[high(m.Values)].Value := ds.Fields[i].Value;
      end
  end
end;

{ TDSMementoManager }

procedure TDSMementoManager.ActivatedFrame(frameClassName: string; ds: TArrayDS);
  function RestoreIfPossible: boolean;
  var
    i: integer;
    idx: integer;
  begin
    idx := -1;
    for i := FMementos.Count - 1 downto 0 do
      if (FMementos[i] as TDSMemento).FSourceClassName = frameClassName then
      begin
        idx := i;
        break;
      end;
    Result := idx <> -1;
    if Result then
      try
        (FMementos[idx] as TDSMemento).Restore( ds );
      finally
        while FMementos.Count > idx do
          FMementos.Delete( idx );
        FGlobalLookupFrameClassName := '';
      end;
  end;
var
  brokenGlobalLookupChain: boolean;
begin
  brokenGlobalLookupChain := not RestoreIfPossible;
  if FGlobalLookupFrameClassName <> '' then
    if frameClassName = FGlobalLookupFrameClassName then //successful Global Lookup
    begin
      Commit;
      brokenGlobalLookupChain := false;
    end
    else
      Rollback;
  if brokenGlobalLookupChain then
    FMementos.Clear;
end;

procedure TDSMementoManager.ActivatingFrameDueToGlobalLookup( frameClassName: string; ds: TArrayDS; newFrameClassName: string );
begin
  Assert( newFrameClassName <> '' );
  Assert( newFrameClassName <> frameClassName );
  if FGlobalLookupFrameClassName <> '' then //this means that we failed to activate frame requested by previous GlobalLookup
  begin
    if LastMemento.FSourceClassName = frameClassName then
      Rollback
    else //this means that we missed ActivatedFrame call or get this call too early
      Assert( false );
  end;
  Start( frameClassName, ds, newFrameClassName );
end;

procedure TDSMementoManager.Start( frameClassName: string; ds: TArrayDS; newFrameClassName: string );
begin
  FMementos.Add( TDSMemento.Create( ds, frameClassName ) );
  FGlobalLookupFrameClassName := newFrameClassName;
end;

procedure TDSMementoManager.Rollback;
begin
  FMementos.Delete( FMementos.Count-1 );
  FGlobalLookupFrameClassName := '';
end;

procedure TDSMementoManager.Commit;
begin
  FGlobalLookupFrameClassName := ''
end;

constructor TDSMementoManager.Create;
begin
  FMementos := TObjectList.Create( true );
end;

destructor TDSMementoManager.Destroy;
begin
  inherited;
  FreeAndNil( FMementos );
end;

function TDSMementoManager.LastMemento: TDSMemento;
begin
  Assert( FMementos.Count > 0 );
  Result := FMementos[FMementos.Count-1] as TDSMemento;
end;

end.
