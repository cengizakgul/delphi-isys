// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DBDTSelectionListFiltr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,   EvConsts, Variants,
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, Mask, wwdbedit, ActnList,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvUIComponents, EvClientDataSet, isUIwwDBEdit,
  LMDCustomButton, LMDButton, isUILMDButton, ExtCtrls, isUIFashionPanel;

type
  TDBDTSelectionListFiltr = class(TForm)
    Label2: TevLabel;
    wwDBGrid1: TevDBGrid;
    BitBtn1: TevBitBtn;
    BitBtn2: TevBitBtn;
    wwcsTempDBDT: TevClientDataSet;
    wwdsTempDBDT: TevDataSource;
    wwcsTempDBDTDIVISION_NBR: TIntegerField;
    wwcsTempDBDTDIVISION_NAME: TStringField;
    wwcsTempDBDTBRANCH_NBR: TIntegerField;
    wwcsTempDBDTBRANCH_NAME: TStringField;
    wwcsTempDBDTDEPARTMENT_NBR: TIntegerField;
    wwcsTempDBDTDEPARTMENT_NAME: TStringField;
    wwcsTempDBDTTEAM_NBR: TIntegerField;
    wwcsTempDBDTTEAM_NAME: TStringField;
    wwcsTempDBDTDBDT_CUSTOM_NUMBERS: TStringField;
    edtFiltr: TevDBEdit;
    evActionList1: TevActionList;
    Select: TAction;
    fpFilterCriteria: TisUIFashionPanel;
    fpSearchResults: TisUIFashionPanel;
    procedure wwDBGrid1TitleButtonClick(Sender: TObject;
      AFieldName: string);
    procedure wwDBGrid1CalcTitleAttributes(Sender: TObject;
      AFieldName: string; AFont: TFont; ABrush: TBrush;
      var ATitleAlignment: TAlignment);
    procedure FormShow(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtFiltrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFiltrChange(Sender: TObject);
    procedure wwcsTempDBDTFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure wwDBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure SelectUpdate(Sender: TObject);
  private
    FIndexFieldName: string;
    procedure SortGrid(aFieldName: string);
  public
    procedure Setup(const aDivDataSet, aBrchDataSet, aDeptDataSet, aTeamDataSet: TevBasicClientDataSet;
                    const DivValue, BrchValue, DeptValue, TeamValue: Variant; const DBDTLevel: Char);
    function  SetCurrent(const DivValue, BrchValue, DeptValue, TeamValue: Variant): Boolean;
    function  ShowSelection(const DivValue, BrchValue, DeptValue, TeamValue: Variant): Boolean;
  end;

procedure SetupDBDT_DS(var wwcsTempDBDT: TevClientDataSet; aDivDataSet, aBrchDataSet, aDeptDataSet,
  aTeamDataSet: TevBasicClientDataSet; DBDTLevel: Char;  AlwaysDisplay : boolean = false);

implementation

{$R *.DFM}

procedure TDBDTSelectionListFiltr.edtFiltrKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
  VK_UP :
  begin
    wwcsTempDBDT.FindPrior;
    Key := 0;
  end;
  VK_DOWN :
  begin
    wwcsTempDBDT.FindNext;
    Key := 0;
  end;
  VK_END :
  begin
    if ssCtrl in Shift then
    begin
      wwcsTempDBDT.FindLast;
      Key := 0;
    end
  end;
  VK_HOME :
  begin
    if ssCtrl in Shift then
    begin
      wwcsTempDBDT.FindFirst;
      Key := 0;
    end;
  end;
  VK_PRIOR :
    wwcsTempDBDT.MoveBy(-19);
  VK_NEXT :
    wwcsTempDBDT.MoveBy(19);
  end;

  inherited;
end;

procedure TDBDTSelectionListFiltr.edtFiltrChange(Sender: TObject);
begin
  inherited;
  wwcsTempDBDT.Filtered:=false;
  wwcsTempDBDT.Filtered:=true;
end;

procedure TDBDTSelectionListFiltr.wwcsTempDBDTFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;

  if edtFiltr.Text = '' then
    Accept := true
  else
    Accept := pos(AnsiUpperCase(edtFiltr.Text),
                  AnsiUpperCase(StringReplace(DataSet.FindField('DBDT_CUSTOM_NUMBERS').AsString, ' | ', '', [rfReplaceAll]))) > 0;

  Accept := Accept and not DataSet.FindField('DIVISION_NBR').IsNull;
end;

procedure TDBDTSelectionListFiltr.wwDBGrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if wwDBGrid1.Focused then
  begin
    edtFiltr.SetFocus;
    SendMessage(edtFiltr.Handle, WM_KEYDOWN, VK_END, 0);
    SendMessage(edtFiltr.Handle, WM_Char, word(Key), 0);
    Key := #0;
  end;
end;

procedure TDBDTSelectionListFiltr.SelectUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := wwDBGrid1.DataSource.DataSet.RecordCount > 0;
end;

procedure SetupDBDT_DS(var wwcsTempDBDT: TevClientDataSet; aDivDataSet, aBrchDataSet, aDeptDataSet,
  aTeamDataSet: TevBasicClientDataSet; DBDTLevel: Char; AlwaysDisplay : boolean = false);
var
  DivDataSet, BrchDataSet, DeptDataSet, TeamDataSet: TevClientDataSet;

  procedure PopulateTeamInfo;
  begin
    with wwcsTempDBDT do
    begin
      FieldByName('TEAM_NBR').Value := TeamDataSet.FieldByName('CO_TEAM_NBR').Value;
      if AlwaysDisplay or (TeamDataSet.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        FieldByName('TEAM_NAME').Value := TeamDataSet.FieldByName('NAME').Value;
        if FieldByName('DBDT_CUSTOM_NUMBERS').IsNull then
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(TeamDataSet.FieldByName('CUSTOM_TEAM_NUMBER').AsString)
        else
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(TeamDataSet.FieldByName('CUSTOM_TEAM_NUMBER').AsString) + ' | ' +
            FieldByName('DBDT_CUSTOM_NUMBERS').AsString;
      end;
    end;
  end;

  procedure PopulateDeptInfo;
  begin
    with wwcsTempDBDT do
    begin
      FieldByName('DEPARTMENT_NBR').Value := DeptDataSet.FieldByName('CO_DEPARTMENT_NBR').Value;
      if AlwaysDisplay or (DeptDataSet.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        FieldByName('DEPARTMENT_NAME').Value := DeptDataSet.FieldByName('NAME').Value;
        if FieldByName('DBDT_CUSTOM_NUMBERS').IsNull then
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(DeptDataSet.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString)
        else
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(DeptDataSet.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString) + ' | ' +
            FieldByName('DBDT_CUSTOM_NUMBERS').AsString;
      end;
    end;
  end;

  procedure PopulateBrchInfo;
  begin
    with wwcsTempDBDT do
    begin
      FieldByName('BRANCH_NBR').Value := BrchDataSet.FieldByName('CO_BRANCH_NBR').Value;
      if AlwaysDisplay or (BrchDataSet.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        FieldByName('BRANCH_NAME').Value := BrchDataSet.FieldByName('NAME').Value;
        if FieldByName('DBDT_CUSTOM_NUMBERS').IsNull then
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(BrchDataSet.FieldByName('CUSTOM_BRANCH_NUMBER').AsString)
        else
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(BrchDataSet.FieldByName('CUSTOM_BRANCH_NUMBER').AsString) + ' | ' +
            FieldByName('DBDT_CUSTOM_NUMBERS').AsString;
      end;
    end;
  end;

  procedure PopulateDivInfo;
  begin
    with wwcsTempDBDT do
    begin
      FieldByName('DIVISION_NBR').Value := DivDataSet.FieldByName('CO_DIVISION_NBR').Value;
      if AlwaysDisplay or (DivDataSet.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        FieldByName('DIVISION_NAME').Value := DivDataSet.FieldByName('NAME').Value;
        if FieldByName('DBDT_CUSTOM_NUMBERS').IsNull then
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(DivDataSet.FieldByName('CUSTOM_DIVISION_NUMBER').AsString)
        else
          FieldByName('DBDT_CUSTOM_NUMBERS').Value := Trim(DivDataSet.FieldByName('CUSTOM_DIVISION_NUMBER').AsString) + ' | ' +
            FieldByName('DBDT_CUSTOM_NUMBERS').AsString;
      end;
    end;
  end;

  procedure MakeFieldNonVisible(FieldName: string);
  var
    aField: TField;
  begin
    aField := wwcsTempDBDT.FindField(FieldName);
    if assigned(aField) then
      aField.Visible := False;
  end;

begin
  if not assigned(wwcsTempDBDT) or
     (wwcsTempDBDT.FieldDefs.Count = 0) and (wwcsTempDBDT.FieldCount = 0) then
  begin
    if not assigned(wwcsTempDBDT) then
      wwcsTempDBDT := TevClientDataSet.Create(Application);

    with wwcsTempDBDT.FieldDefs do
    begin
      Clear;

      case DBDTLevel of
        CLIENT_LEVEL_DIVISION,
          CLIENT_LEVEL_BRANCH,
          CLIENT_LEVEL_DEPT,
          CLIENT_LEVEL_TEAM:
          begin
            with AddFieldDef do
            begin
              DataType := ftString;
              Name := 'DIVISION_NAME';
              Size := 40;
            end;

            with AddFieldDef do
            begin
              DataType := ftString;
              Name := 'DBDT_CUSTOM_NUMBERS';
              Size := 80;
            end;

            with AddFieldDef do
            begin
              DataType := ftInteger;
              Name := 'DIVISION_NBR';
            end;
          end;
      end;

      case DBDTLevel of
        CLIENT_LEVEL_BRANCH,
          CLIENT_LEVEL_DEPT,
          CLIENT_LEVEL_TEAM:
          begin
            with AddFieldDef do
            begin
              DataType := ftString;
              Name := 'BRANCH_NAME';
              Size := 40;
            end;

            with AddFieldDef do
            begin
              DataType := ftInteger;
              Name := 'BRANCH_NBR';
            end;
          end;
      end;

      case DBDTLevel of
        CLIENT_LEVEL_DEPT,
          CLIENT_LEVEL_TEAM:
          begin
            with AddFieldDef do
            begin
              DataType := ftString;
              Name := 'DEPARTMENT_NAME';
              Size := 40;
            end;

            with AddFieldDef do
            begin
              DataType := ftInteger;
              Name := 'DEPARTMENT_NBR';
            end;
          end;
      end;

      case DBDTLevel of
        CLIENT_LEVEL_TEAM:
          begin
            with AddFieldDef do
            begin
              DataType := ftString;
              Name := 'TEAM_NAME';
              Size := 40;
            end;

            with AddFieldDef do
            begin
              DataType := ftInteger;
              Name := 'TEAM_NBR';
            end;
          end;
      end;
    end;

  end;
  wwcsTempDBDT.CreateDataSet;

  MakeFieldNonVisible('TEAM_NBR');
  MakeFieldNonVisible('BRANCH_NBR');
  MakeFieldNonVisible('DEPT_NBR');
  MakeFieldNonVisible('DIVISION_NBR');

  with wwcsTempDBDT do
  begin
    DivDataSet := TevClientDataSet.Create(nil);
    BrchDataSet := TevClientDataSet.Create(nil);
    DeptDataSet := TevClientDataSet.Create(nil);
    TeamDataSet := TevClientDataSet.Create(nil);
    try
      DivDataSet.CloneCursor(aDivDataSet, True);
      BrchDataSet.CloneCursor(aBrchDataSet, True);
      DeptDataSet.CloneCursor(aDeptDataSet, True);
      TeamDataSet.CloneCursor(aTeamDataSet, True);

      case DBDTLevel of
        CLIENT_LEVEL_TEAM:
          begin
            TeamDataSet.First;
            while not TeamDataSet.EOF do
            begin
              if AlwaysDisplay or (TeamDataSet.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
              begin
                Insert;
                PopulateTeamInfo;
                if DeptDataSet.Locate('CO_DEPARTMENT_NBR', TeamDataSet.FieldByName('CO_DEPARTMENT_NBR').Value, []) then
                begin
                  PopulateDeptInfo;
                  if BrchDataSet.Locate('CO_BRANCH_NBR', DeptDataSet.FieldByName('CO_BRANCH_NBR').Value, []) then
                  begin
                    PopulateBrchInfo;
                    if DivDataSet.Locate('CO_DIVISION_NBR', BrchDataSet.FieldByName('CO_DIVISION_NBR').Value, []) then
                      PopulateDivInfo;
                  end;
                end;
                Post;
              end;
              TeamDataSet.Next;
            end;
          end;
        CLIENT_LEVEL_DEPT:
          begin
            DeptDataSet.First;
            while not DeptDataSet.EOF do
            begin
              if AlwaysDisplay or (DeptDataSet.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
              begin
                Insert;
                PopulateDeptInfo;
                if BrchDataSet.Locate('CO_BRANCH_NBR', DeptDataSet.FieldByName('CO_BRANCH_NBR').Value, []) then
                begin
                  PopulateBrchInfo;
                  if DivDataSet.Locate('CO_DIVISION_NBR', BrchDataSet.FieldByName('CO_DIVISION_NBR').Value, []) then
                    PopulateDivInfo;
                end;
                Post;
              end;
              DeptDataSet.Next;
            end;
          end;
        CLIENT_LEVEL_BRANCH:
          begin
            BrchDataSet.First;
            while not BrchDataSet.EOF do
            begin
              if AlwaysDisplay or (BrchDataSet.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
              begin
                Insert;
                PopulateBrchInfo;
                if DivDataSet.Locate('CO_DIVISION_NBR', BrchDataSet.FieldByName('CO_DIVISION_NBR').Value, []) then
                  PopulateDivInfo;
                Post;
              end;
              BrchDataSet.Next;
            end;
          end;
        CLIENT_LEVEL_DIVISION:
          begin
            if DivDataSet.RecordCount <> 0 then
            begin
              DivDataSet.First;
              while not DivDataSet.EOF do
              begin
                if AlwaysDisplay or (DivDataSet.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
                begin
                  Insert;
                  PopulateDivInfo;
                  Post;
                end;
                DivDataSet.Next;
              end;
            end;
          end;
      end;
    finally
      DivDataSet.Free;
      BrchDataSet.Free;
      DeptDataSet.Free;
      TeamDataSet.Free;
    end;

// Clear all Branches, Departments and Teams that don't belong to this company
    First;
    while not EOF do
    begin
      if FieldByName('DIVISION_NBR').IsNull then
        Delete
      else
        Next;
    end;

  end;
end;

procedure TDBDTSelectionListFiltr.Setup(const aDivDataSet, aBrchDataSet, aDeptDataSet, aTeamDataSet: TevBasicClientDataSet;
  const DivValue, BrchValue, DeptValue, TeamValue: Variant; const DBDTLevel: Char);
begin
  SetupDBDT_DS(wwcsTempDBDT, aDivDataSet, aBrchDataSet, aDeptDataSet, aTeamDataSet,
    DBDTLevel);

  SetCurrent(DivValue, BrchValue, DeptValue, TeamValue);
end;

procedure TDBDTSelectionListFiltr.SortGrid(aFieldName: string);
var
  SavePlace: TBookMark;
begin
  SavePlace := wwcsTempDBDT.GetBookmark;

  try
    wwcsTempDBDT.IndexFieldNames := aFieldName;

    if aFieldName = 'DIVISION_NAME' then
      wwcsTempDBDT.IndexFieldNames := wwcsTempDBDT.IndexFieldNames + ';BRANCH_NAME;DEPARTMENT_NAME;TEAM_NAME';

    if aFieldName = 'BRANCH_NAME' then
      wwcsTempDBDT.IndexFieldNames := wwcsTempDBDT.IndexFieldNames + ';DIVISION_NAME;DEPARTMENT_NAME;TEAM_NAME';

    if aFieldName = 'DEPARTMENT_NAME' then
      wwcsTempDBDT.IndexFieldNames := wwcsTempDBDT.IndexFieldNames + ';DIVISION_NAME;BRANCH_NAME;TEAM_NAME';

    if aFieldName = 'TEAM_NAME' then
      wwcsTempDBDT.IndexFieldNames := wwcsTempDBDT.IndexFieldNames + ';DIVISION_NAME;BRANCH_NAME;DEPARTMENT_NAME';

    FIndexFieldName := aFieldName;
    wwcsTempDBDT.GotoBookmark(SavePlace);
  finally
    wwcsTempDBDT.FreeBookmark(SavePlace);
  end;

  wwDBGrid1.Invalidate;
end;

procedure TDBDTSelectionListFiltr.wwDBGrid1TitleButtonClick(Sender: TObject;
  AFieldName: string);
begin
  SortGrid(AFieldName);
end;

procedure TDBDTSelectionListFiltr.wwDBGrid1CalcTitleAttributes(Sender: TObject;
  AFieldName: string; AFont: TFont; ABrush: TBrush;
  var ATitleAlignment: TAlignment);
begin
  if AFieldName = FIndexFieldName then
  begin
    AFont.Color :=  clBlue;
    ABrush.Color := clYellow;
  end
  else
  begin
    AFont.Color :=  clBlack;
    ABrush.Color := clBtnFace;
  end;
end;

procedure TDBDTSelectionListFiltr.FormShow(Sender: TObject);
begin
  BitBtn1.Enabled := wwDBGrid1.DataSource.DataSet.RecordCount > 0;
end;

procedure TDBDTSelectionListFiltr.wwDBGrid1DblClick(Sender: TObject);
begin
  if BitBtn1.Enabled then BitBtn1.Click;
end;

procedure TDBDTSelectionListFiltr.FormCreate(Sender: TObject);
begin
  SortGrid('DIVISION_NAME');
end;

function TDBDTSelectionListFiltr.SetCurrent(const DivValue, BrchValue, DeptValue, TeamValue: Variant): Boolean;
begin
  Result := False;

  if VarIsNull(DivValue) and VarIsNull(BrchValue) and VarIsNull(DeptValue) and VarIsNull(TeamValue) then
    Exit;

  wwcsTempDBDT.DisableControls;
  try
    with wwcsTempDBDT do
    begin
      Last;
      while not BOF do
      begin
        if (VarIsNull(DivValue) or (FieldByName('DIVISION_NBR').Value = DivValue)) and
           (VarIsNull(BrchValue) or (FieldByName('BRANCH_NBR').Value = BrchValue)) and
           (VarIsNull(DeptValue) or (FieldByName('DEPARTMENT_NBR').Value = DeptValue)) and
           (VarIsNull(TeamValue) or (FieldByName('TEAM_NBR').Value = TeamValue)) then
        begin
          Result := True;        
          Break;
        end;
        Prior;
      end;
    end;
  finally
    wwcsTempDBDT.EnableControls;
  end;
end;

function TDBDTSelectionListFiltr.ShowSelection(const DivValue, BrchValue, DeptValue, TeamValue: Variant): Boolean;
begin
  Result := False;
  SetCurrent(DivValue, BrchValue, DeptValue, TeamValue);
  if ShowModal = mrOk then
  begin
    Result := not VarSameValue(DivValue, wwcsTempDBDT.FieldByName('DIVISION_NBR').Value) or
      not VarSameValue(BrchValue, wwcsTempDBDT.FieldByName('BRANCH_NBR').Value) or
      not VarSameValue(DeptValue, wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value) or
      not VarSameValue(TeamValue, wwcsTempDBDT.FieldByName('TEAM_NBR').Value);
  end;
end;

initialization
  RegisterClass(TDBDTSelectionListFiltr);

end.
