unit evADRCommon;

interface

uses SysUtils, Variants, DB,
     isBaseClasses, evADRRequestQueue, EvCommonInterfaces, isTypes, isLogFile, isErrorUtils,
     isBasicUtils, EvStreamUtils, EvConsts, EvContext, EvMainboard, EvTypes, EvClasses,
     isSettings, evBasicUtils, evTransportShared, EvUtils;

const
  ADRRequestStatusNames: array [Low(TevADRRequestStatus)..High(TevADRRequestStatus)] of String =
    ('', 'Queued', 'Executing', 'Finished', 'Failed', 'Aborted');


  // Client Requests
  ADR_Clt_StartupCheck        = 'Client Startup Check';
  ADR_Clt_FullDBCopy          = 'Client Full DB Copy';
  ADR_Clt_SyncDBData          = 'Client Sync DB Data';
  ADR_Clt_CheckDBStatus       = 'Client Check DB Status';
  ADR_Clt_SyncDBList          = 'Client Sync DB List';
  ADR_Clt_CheckDBData         = 'Client Check DB Data';
  ADR_Clt_CheckDBTrans        = 'Client Check DB Trans';
  ADR_Clt_EnlistCheckDBStatus = 'Client Enlist "Check DB Status"';
  ADR_Clt_EnlistCompareDB     = 'Client Enlist "Compare DB"';
  ADR_Clt_EnlistCheckDBTrans  = 'Client Enlist "Check DB Trans"';
  ADR_Clt_EnlistFullDBCopy    = 'Client Enlist "Full DB Copy"';

  //Server Requests
  ADR_Srv_FullDBCopy    = 'Server Full DB Copy';
  ADR_Srv_RebuildTT     = 'Server Rebuild Temp Tables';
  ADR_Srv_RebuildAllTT  = 'Server Rebuild All Temp Tables';
  ADR_Srv_FinalizeRebuildAllTT = 'Server Finalize Rebuild All Temp Tables';
  ADR_Srv_SyncDBData    = 'Server Sync DB Data';
  ADR_Srv_SyncDBList    = 'Server Sync DB List';
  ADR_Srv_CheckDBData   = 'Server Check DB Data';
  ADR_Srv_CheckDBTrans  = 'Server Check DB Trans';
  ADR_Srv_SweepDB       = 'Server Sweep DB';
  ADR_Srv_EnlistSweepDB = 'Server Enlist "Sweep DB"';


type
  PevADRDBInfo = ^TevADRDBInfo;

  IevADRModule = interface;

  IevADRDBCachedStatus = interface
  ['{A16EFFC1-D654-4F7B-9044-947076074F03}']
    function  Database: String;
    function  LastUpdate: TDateTime;
    function  GetVersion: String;
    procedure SetVersion(const AValue: String);
    function  GetLastDBSync: TDateTime;
    procedure SetLastDBSync(const AValue: TDateTime);
    function  GetLastTransactionNbr: Integer;
    procedure SetLastTransactionNbr(const AValue: Integer);
    property  Version: String read GetVersion write SetVersion;
    property  LastDBSync: TDateTime read GetLastDBSync write SetLastDBSync;
    property  LastTransactionNbr: Integer read GetLastTransactionNbr write SetLastTransactionNbr;
  end;

  IevADRRequestQueueExt = interface(IevADRRequestQueue)
  ['{2A8E7653-E218-4578-8BA4-BB4D2E027810}']
    function  ADRModule: IevADRModule;
    function  NewDBRequest(const AOriginatedAt: TUTCDateTime; const ARequestTypeID: String; const AParams: array of Variant; const APositionOf: IevADRRequest = nil): IevADRRequest;
    function  GetRequests(const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []; const ADatabase: String = ''): IisList;
    function  RequestsExist(const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []; const ADatabase: String = ''): Boolean;
    procedure DeleteDBRequests(const ADatabase: String; const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []);
  end;

  IevADRDBRequest = interface(IevADRRequest)
  ['{CCEC0A6E-8D7A-4D34-8F29-E3822F07AC95}']
    function  QueueExt: IevADRRequestQueueExt;
    function  Database: String;
    function  DBVersion: String;
    function  LastTransactionNbr: Integer;
  end;


  IevADRTransRequest = interface(IevADRDBRequest)
  ['{3002006E-D437-434F-946B-867F30EA840E}']
    function  ADRRequestID: String;
  end;


  IevADRStatCounter = interface
  ['{71A5BD5A-A7D2-43D0-B451-7C590F89C1C4}']
    function  GetName: String;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    function  LastUpdate: TDateTime;
    property  Name: String read GetName;
    property  Value: Variant read GetValue write SetValue;
  end;

  IevADRDomainStat = interface
  ['{73A98381-300A-4109-9857-2FC096F40331}']
    procedure SetCounter(const ACounter: String; const AValue: Variant);
    procedure AddToCounter(const ACounter: String; const AValue: Extended);
    function  GetCounter(const ACounter: String): IevADRStatCounter;
    procedure DeleteCounter(const ACounter: String);
  end;


  IevADRStatistics = interface
  ['{0C8F0694-9F1A-4C3D-84DC-31E742C12B39}']
    function DomainStat(const ADomainName: String): IevADRDomainStat;
  end;


  IevADRModule = interface
  ['{076A1BE5-966B-4FD4-A181-39E8C54A58E6}']
    function  GetWorkingDir: String;
    function  RequestQueue: IevADRRequestQueueExt;
    procedure LogDebugEvent(const AText: String; const ADetail: String = '');
    procedure LogEvent(const AText: String; const ADetail: String = '');
    procedure LogError(const AText: String; const ADetail: String = '');
    procedure LogWarning(const AText: String; const ADetail: String = '');
    procedure AddDBRequestsIfMissing(const AOriginatedAt: TUTCDateTime; const ARequestTypeID: String; const AParams: array of Variant; const ATopPosition: Boolean = False);
    function  Statistics: IevADRDomainStat;
    function  GetLogDebugInfo: Boolean;
    procedure SetLogDebugInfo(const AValue: Boolean);
    procedure SendAlertEmail(const ASubject, AMessage: String);
    property  DebugInfo: Boolean read GetLogDebugInfo write SetLogDebugInfo;
  end;


  IevDBStatusStorage = interface
  ['{BE84DE1B-BD65-4282-B21B-E7137D25EB5A}']
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SetDBInfo(const ADBInfo: IevADRDBInfo);
    procedure ClearDBInfo(const ADatabase: String);
  end;



  TevADRDBRequest = class(TevADRRequest, IevADRDBRequest)
  private
    FDatabase: String;
    FDBVersion: String;
    FLastTransactionNbr: Integer;
  protected
    procedure Store; override;
    procedure Restore; override;

    procedure SetDatabase(const AValue: String);
    procedure SetDBVersion(const AValue: String);
    procedure SetLastTransactionNbr(const AValue: Integer);
    function  ReadyToRun: Boolean; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; virtual;
    procedure AfterRun(const AFailed: Boolean); override;
    procedure DoOnError; override;

    // IevADRDBRequest
    function  QueueExt: IevADRRequestQueueExt;
    function  Database: String;
    function  DBVersion: String;
    function  LastTransactionNbr: Integer;
    function  Description: String; override;
  public
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevCustomADRModule = class(TisInterfacedObject, IevADRModule)
  private
    FRequestQueue: IevADRRequestQueueExt;
    FStatistics: IevADRStatistics;
    FLogDebugInfo: Boolean;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  SetADRContext(const AValue: Boolean): Boolean;
    function  GetWorkingDir: String; virtual; abstract;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean); virtual;

    function  RequestQueue: IevADRRequestQueueExt;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisList; virtual;  // list of IevADRRequest
    procedure LogDebugEvent(const AText: String; const ADetail: String = '');
    procedure LogEvent(const AText: String; const ADetail: String = '');
    procedure LogError(const AText: String; const ADetail: String = '');
    procedure LogWarning(const AText: String; const ADetail: String = '');
    procedure AddDBRequestsIfMissing(const AOriginatedAt: TUTCDateTime; const ARequestTypeID: String; const AParams: array of Variant; const ATopPosition: Boolean = False);
    function  Statistics: IevADRDomainStat;
    function  GetLogDebugInfo: Boolean;
    procedure SetLogDebugInfo(const AValue: Boolean);
    procedure SendAlertEmail(const ASubject, AMessage: String);

    function  DoGetDBInfo(const ADatabase: String): IevADRDBInfo;
  end;


  TevDBStatusStorage = class(TisInterfacedObject, IevDBStatusStorage)
  private
    FStorage: IisSettings;
  protected
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SetDBInfo(const ADBInfo: IevADRDBInfo);
    procedure ClearDBInfo(const ADatabase: String);
  public
    constructor Create(const AStorageFolder: String); reintroduce;
  end;


  function GetFullDBCopyRequestName(const ADatabase: String): String;
  function GetGetSyncDataRequestName(const ADatabase: String): String;
  function GetCheckDBRequestName(const ADatabase: String): String;
  function GetApplySyncDataRequestName(const ADatabase: String): String;
  function GetSyncDBListName: String;
  function GetRebuildTmpTblsRequestName(const ADatabase: String): String;
  function GetRebuildTmpTblsForAllRequestName: String;
  function GetFinalizeRebuildTmpTblsForAllRequestName: String;
  function GetBackupRequestName(const ADatabase: String): String;
  function GetRestoreRequestName(const ADatabase: String): String;
  function GetGetDBHashRequestName(const ADatabase: String): String;
  function GetDBCompareRequestName(const ADatabase: String): String;
  function GetSweepDBRequestName(const ADatabase: String): String;
  function GetDBTransCheckRequestName(const ADatabase: String): String;

  function OpenDB(const ADatabase: String): TevDBType;

  function IsClientDB(const ADatabase: String): Boolean;
  function IsTempDB(const ADatabase: String): Boolean;

implementation

type
  TevCustomADRRequestQueue = class(TevADRRequestQueue, IevADRRequestQueueExt)
  private
    FADRModule: TevCustomADRModule;
  protected
    function  ADRModule: IevADRModule;
    function  NewDBRequest(const AOriginatedAt: TUTCDateTime; const ARequestTypeID: String; const AParams: array of Variant; const APositionOf: IevADRRequest = nil): IevADRRequest;
    function  GetRequests(const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []; const ADatabase: String = ''): IisList;
    function  RequestsExist(const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []; const ADatabase: String = ''): Boolean;
    procedure DeleteDBRequests(const ADatabase: String; const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []);
  public
    constructor Create(const AStorageDir: String; const AOwner: TevCustomADRModule); reintroduce;
  end;


  TevADRStatistics = class(TisCollection, IevADRStatistics)
  private
    FWorkFolder: String;
    procedure Initialize;
  protected
    function DomainStat(const ADomainName: String): IevADRDomainStat;
  public
    constructor Create(const AWorkFolder: String);
  end;

  TevADRDomainStat = class(TisCollection, IevADRDomainStat)
  private
    FStorage: IisSettings;
  protected
    procedure LoadFromStorage; override;

    procedure SetCounter(const ACounter: String; const AValue: Variant);
    procedure AddToCounter(const ACounter: String; const AValue: Extended);
    function  GetCounter(const ACounter: String): IevADRStatCounter;
    procedure DeleteCounter(const ACounter: String);
  public
    constructor Create(const ADomainName, AWorkFolder: String);
  end;

  TevADRStatCounter = class(TisNamedObject, IevADRStatCounter)
  private
    FStorage: IisSettings;
    FValue: Variant;
    FLastUpdate: TDateTime;
  protected
    procedure LoadFromStorage; override;
    procedure SaveToStorage; override;
    procedure RemoveFromStorage; override;

    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    function  LastUpdate: TDateTime;
  public
    constructor Create(const AName: String; const AStorage: IisSettings);
  end;



function OpenDB(const ADatabase: String): TevDBType;
var
  s: String;
begin
  if AnsiSameText(ADatabase, DB_System) then
    Result := dbtSystem
  else if AnsiSameText(ADatabase, DB_S_Bureau) then
    Result := dbtBureau
  else if AnsiSameText(ADatabase, DB_TempTables) then
    Result := dbtTemporary
  else if AnsiSameText(ADatabase, DB_Cl_Base) then
  begin
    ctx_DBAccess.CurrentClientNbr := 0;
    Result := dbtClient;
  end
  else if StartsWith(ADatabase, DB_Cl) then
  begin
    s := AnsiUpperCase(ADatabase);
    GetNextStrValue(s, AnsiUpperCase(DB_Cl));
    ctx_DBAccess.CurrentClientNbr := StrToInt(s);
    Result := dbtClient;
  end
  else
  begin
    Result := dbtUnknown;
    Assert(False);
  end;
end;


function IsClientDB(const ADatabase: String): Boolean;
begin
  Result := StartsWith(ADatabase, DB_Cl) and not AnsiSameText(ADatabase, DB_Cl_Base);
end;


function IsTempDB(const ADatabase: String): Boolean;
begin
  Result := AnsiSameText(ADatabase, DB_TempTables);
end;

function GetFullDBCopyRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' backup data';
end;

function GetGetSyncDataRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' get sync data';
end;


function GetCheckDBRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' check';
end;


function GetApplySyncDataRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' apply sync data';
end;

function GetSyncDBListName: String;
begin
  Result := 'Sync DB list';
end;

function GetRebuildTmpTblsRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' rebuild temp tables';
end;

function GetRebuildTmpTblsForAllRequestName: String;
begin
  Result := 'Rebuild temp tables for ALL clients';
end;

function GetFinalizeRebuildTmpTblsForAllRequestName: String;
begin
  Result := 'Finalize rebuild temp tables for ALL clients';
end;

function GetBackupRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' backup';
end;

function GetRestoreRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' restore';
end;

function GetGetDBHashRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' calc hash';
end;

function GetDBTransCheckRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' transactions check';
end;

function GetDBCompareRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' compare';
end;

function GetSweepDBRequestName(const ADatabase: String): String;
begin
  Result := ADatabase + ' sweep';
end;


{ TevADRDBRequest }

constructor TevADRDBRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FDatabase := AParams[0];
  FDBVersion := AParams[1];
  FLastTransactionNbr := AParams[2];
end;

function TevADRDBRequest.Database: String;
begin
  Result := FDatabase;
end;

function TevADRDBRequest.DBVersion: String;
begin
  Result := FDBVersion;
end;

function TevADRDBRequest.Description: String;
var
 fmt: String;
begin
  fmt := 'Database: %s   v%s   Last Transaction Nbr: %s'#13#13 +
         'Started at: %s'#13 +
         'Finished: %s'#13;

  Result := Format(fmt, [FDatabase,
                         FDBVersion,
                         IntToStr(FLastTransactionNbr),
                         DateTimeToText(StartedAt),
                         DateTimeToText(FinishedAt)]);
end;

function TevADRDBRequest.LastTransactionNbr: Integer;
begin
  Result := FLastTransactionNbr;
end;

function TevADRDBRequest.QueueExt: IevADRRequestQueueExt;
begin
  Result := QueueObj as TevCustomADRRequestQueue;
end;

function TevADRDBRequest.ReadyToRun: Boolean;
var
  i: Integer;
  ReqActDB: IevADRDBRequest;
  ExecRequests: IisList;
  Q: TevADRRequestQueue;
begin
  Result := inherited ReadyToRun;
  if Result then
  begin
    Q := QueueObj;
    if not Assigned(Q) then
      Result := False
    else
    begin
      ExecRequests := (Q as IevADRRequestQueue).QueryRequests(Domain, [arsExecuting]);

      for i := 0 to ExecRequests.Count - 1 do
        if Supports(ExecRequests[i], IevADRDBRequest) then
        begin
          // do not allow to run request for the same DB
          ReqActDB := ExecRequests[i] as IevADRDBRequest;
          if IsBlockingExecutionRequest(ReqActDB) then
          begin
            Result := False;
            break;
          end;
        end;
    end;
  end;
end;

procedure TevADRDBRequest.AfterRun(const AFailed: Boolean);
begin
  if not AFailed and (QueueExt <> nil) then
  begin
    QueueExt.ADRModule.Statistics.SetCounter('LastActivity', Caption);

    if Database <> '' then
      QueueExt.ADRModule.Statistics.SetCounter('DB\' + Database, GetTypeID);
  end;

  inherited;      
end;

procedure TevADRDBRequest.Store;
var
  ID: String;
begin
  inherited;
  ID := GetID;
  Storage.AsString[ID + '\Database'] := FDatabase;
  Storage.AsString[ID + '\DBVersion'] := FDBVersion;
  Storage.AsInteger[ID + '\LastTransactionNbr'] := FLastTransactionNbr;
end;


procedure TevADRDBRequest.SetDatabase(const AValue: String);
begin
  FDatabase := AValue;
end;

procedure TevADRDBRequest.SetDBVersion(const AValue: String);
begin
  FDBVersion := AValue;
end;

procedure TevADRDBRequest.SetLastTransactionNbr(const AValue: Integer);
begin
  FLastTransactionNbr := AValue;
end;


function TevADRDBRequest.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := (Database <> '') and AnsiSameText(AExistingRequest.Database, Database);
end;

procedure TevADRDBRequest.DoOnError;
begin
  QueueExt.ADRModule.LogError(Domain + ': ' + ' Unhandled error raised during processing of ' + Database, Error);
end;

procedure TevADRDBRequest.Restore;
begin
  inherited;
  FDatabase := Storage.AsString[GetID + '\Database'];
  FDBVersion := Storage.AsString[GetID + '\DBVersion'];
  FLastTransactionNbr := Storage.AsInteger[GetID + '\LastTransactionNbr'];
end;

{ TevCustomADRModule }

procedure TevCustomADRModule.AddDBRequestsIfMissing(const AOriginatedAt: TUTCDateTime; const ARequestTypeID: String;
  const AParams: array of Variant; const ATopPosition: Boolean);
var
  R: IevADRRequest;
begin
  if RequestQueue.RequestsExist(ARequestTypeID, [arsQueued, arsExecuting]) then
    Exit;

  R := RequestQueue.NewDBRequest(AOriginatedAt, ARequestTypeID, AParams);
  LogDebugEvent(ARequestTypeID + ' enlisted');

  if ATopPosition then
    R.MoveToTop;
end;

function TevCustomADRModule.DoGetDBInfo(const ADatabase: String): IevADRDBInfo;
var
  DBType: TevDBType;
begin
  Result := TevADRDBInfo.Create(ADatabase);

  DBType := OpenDB(ADatabase);
  Result.Version := ctx_DBAccess.GetDBVersion(DBType);
  Assert(Result.Version <> '');
  Result.Status := dsPresent;

  if not IsTempDB(ADatabase) then
    Result.LastTransactionNbr := ctx_DBAccess.GetLastTransactionNbr(DBType);
end;

procedure TevCustomADRModule.DoOnConstruction;
const
  VerInfoFile = 'ver2.info';  // Increment number whenever storage format changes
var
  S: IisStream;
begin
  inherited;

  FLogDebugInfo := True;

  if mb_TCPClient <> nil then
  begin
    if mb_TCPClient.Host = '' then
      mb_TCPClient.Host := mb_AppConfiguration.AsString['General\RequestBroker'];
    if mb_TCPClient.Port = '' then
      mb_TCPClient.Port := IntToStr(TCPClient_Port);
    mb_TCPClient.SetCreateConnectionTimeout(10000);
    mb_TCPClient.DatagramDispatcher.SessionTimeout := CLIENT_SESSION_NEVER_KEEP_ALIVE;
  end;

  //TODO: Remove it after Feb 2012
  if not FileExists(GetWorkingDir + VerInfoFile) then
  begin
    ClearDir(GetWorkingDir, True);
    ForceDirectories(GetWorkingDir);
    S := TisStream.CreateFromNewFile(GetWorkingDir + VerInfoFile);
    S.WriteLn('DO NOT DELETE THIS FILE!');
    S.WriteLn('This is ADR storage format version tag');
    S := nil;
  end;
  //

  ForceDirectories(GetWorkingDir);
  FStatistics := TevADRStatistics.Create(GetWorkingDir + 'Statistics');
  FRequestQueue := TevCustomADRRequestQueue.Create(GetWorkingDir, Self);
end;

procedure TevCustomADRModule.DoOnDestruction;
begin
  SetActive(False);
  inherited;
end;

function TevCustomADRModule.GetActive: Boolean;
begin
  Result := Assigned(FRequestQueue) and FRequestQueue.Active;
end;

function TevCustomADRModule.GetLogDebugInfo: Boolean;
begin
  Result := FLogDebugInfo;
end;

function TevCustomADRModule.GetRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisList;
begin
//  Result := RequestQueue.GetPage(APageNbr, AItemsPerPage, APageCount)
end;

procedure TevCustomADRModule.LogDebugEvent(const AText, ADetail: String);
begin
  if FLogDebugInfo then
    LogEvent(AText, ADetail);
end;

procedure TevCustomADRModule.LogError(const AText, ADetail: String);
begin
  mb_LogFile.AddContextEvent(etError, TypeID, AText, ADetail);
end;

procedure TevCustomADRModule.LogEvent(const AText, ADetail: String);
begin
  mb_LogFile.AddContextEvent(etInformation, TypeID, AText, ADetail);
end;

procedure TevCustomADRModule.LogWarning(const AText, ADetail: String);
begin
  mb_LogFile.AddContextEvent(etWarning, TypeID, AText, ADetail);
end;

function TevCustomADRModule.RequestQueue: IevADRRequestQueueExt;
begin
  Result := FRequestQueue;
end;

procedure TevCustomADRModule.SendAlertEmail(const ASubject, AMessage: String);
var
  sendTo: String;
begin
  if Context <> nil then
  begin
    sendTo := Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'];
    if (sendTo <> '') and (mb_GlobalSettings.EMailInfo.NotificationDomainName <> '') then
      try
        ctx_EMailer.SendQuickMail(sendTo, mb_GlobalSettings.EMailInfo.NotificationDomainName, ASubject, 'Domain: ' + ctx_DomainInfo.DomainName + #13 + AMessage);
      except
      end;
  end;
end;

procedure TevCustomADRModule.SetActive(const AValue: Boolean);
begin
  FRequestQueue.Active := AValue;
end;

function TevCustomADRModule.SetADRContext(const AValue: Boolean): Boolean;
begin
  Result := ctx_DBAccess.GetADRContext;
  ctx_DBAccess.SetADRContext(AValue);
end;

procedure TevCustomADRModule.SetLogDebugInfo(const AValue: Boolean);
begin
  FLogDebugInfo := AValue;
end;

function TevCustomADRModule.Statistics: IevADRDomainStat;
begin
  Result := FStatistics.DomainStat(ctx_DomainInfo.DomainName);
end;

{ TevCustomADRRequestQueue }

function TevCustomADRRequestQueue.ADRModule: IevADRModule;
begin
  Result := FADRModule;
end;

constructor TevCustomADRRequestQueue.Create(const AStorageDir: String; const AOwner: TevCustomADRModule);
begin
  FADRModule := AOwner;
  inherited Create(AStorageDir);
end;

procedure TevCustomADRRequestQueue.DeleteDBRequests(const ADatabase: String;
  const ARequestTypeID: String = ''; const AStatusList: TevADRRequestStatusList = []);
var
  RList: IisList;
  i: Integer;
begin
  RList := GetRequests(ARequestTypeID, AStatusList, ADataBase);
  for i := 0 to RList.Count - 1 do
    DeleteRequest(RList[i] as IevADRRequest);
end;

function TevCustomADRRequestQueue.GetRequests(const ARequestTypeID: String;
  const AStatusList: TevADRRequestStatusList; const ADatabase: String): IisList;
var
  i: Integer;
  R: IevADRRequest;
  Domain: String;
  bPattern: Boolean;
  sDb: String;
begin
  if ctx_DomainInfo <> nil then
    Domain := ctx_DomainInfo.DomainName
  else
    Domain := '';

  Result := QueryRequests(Domain, AStatusList);

  bPattern := FinishesWith(ADatabase, '*');
  if bPattern then
    sDb := Copy(ADatabase, 1, Length(ADatabase) - 1)
  else
    sDb := ADatabase;

  for i := Result.Count - 1 downto 0 do
  begin
    R := Result[i] as IevADRRequest;
    if not (((ARequestTypeID = '') or AnsiSameStr(R.TypeID, ARequestTypeID)) and
       ((sDb = '') or  Supports(R, IevADRDBRequest) and
             (not bPattern and AnsiSameText((R as IevADRDBRequest).Database, sDb) or
              bPattern and StartsWith((R as IevADRDBRequest).Database, sDb))))
     then
       Result.Delete(i);
  end;
end;

function TevCustomADRRequestQueue.NewDBRequest(const AOriginatedAt: TUTCDateTime;
  const ARequestTypeID: String; const AParams: array of Variant; const APositionOf: IevADRRequest): IevADRRequest;
var
  Cl: TevADRRequestClass;
begin
  Cl := TevADRRequestClass(ObjectFactory.GetImplementationOf(ARequestTypeID));
  Result := Cl.Create(AOriginatedAt, AParams);
  AddRequest(Result, APositionOf);
end;

function TevCustomADRRequestQueue.RequestsExist(
  const ARequestTypeID: String; const AStatusList: TevADRRequestStatusList; const ADatabase: String): Boolean;
var
  L: IisList;
begin
  L := GetRequests(ARequestTypeID, AStatusList, ADatabase);
  Result := L.Count > 0;
end;

{ TevADRStatistics }

constructor TevADRStatistics.Create(const AWorkFolder: String);
begin
  inherited Create(nil, True);
  FWorkFolder := AWorkFolder;
end;

function TevADRStatistics.DomainStat(const ADomainName: String): IevADRDomainStat;
begin
  Lock;
  try
    if ChildCount = 0 then
      Initialize;
    Result := FindChildByName(ADomainName) as IevADRDomainStat;
  finally
    Unlock;
  end;
end;

procedure TevADRStatistics.Initialize;
var
  i: Integer;
  O: IisNamedObject;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
  begin
    O := TevADRDomainStat.Create(mb_GlobalSettings.DomainInfoList[i].DomainName, FWorkFolder);
    AddChild(O);
  end;
end;


{ TevADRDomainStat }

procedure TevADRDomainStat.AddToCounter(const ACounter: String; const AValue: Extended);
var
  C: IevADRStatCounter;
begin
  Lock;
  try
    C := GetCounter(ACounter);
    if Assigned(C) then
      C.Value := C.Value + AValue
    else
      SetCounter(ACounter, AValue);
  finally
    Unlock;
  end;
end;

constructor TevADRDomainStat.Create(const ADomainName, AWorkFolder: String);
begin
  inherited Create(nil, True);
  SetName(ADomainName);
  ForceDirectories(AWorkFolder);
  FStorage := TisSettingsSQLite.Create(NormalizePath(AWorkFolder) + ADomainName + '_stat.db', 'Stat');
  LoadFromStorage;
end;

procedure TevADRDomainStat.DeleteCounter(const ACounter: String);
var
  C: IisNamedObject;
begin
  Lock;
  try
    C := FindChildByName(ACounter);
    RemoveChild(C);
    C.RemoveFromStorage;
  finally
    Unlock;
  end;
end;

function TevADRDomainStat.GetCounter(const ACounter: String): IevADRStatCounter;
begin
  Result := FindChildByName(ACounter) as IevADRStatCounter;
end;

procedure TevADRDomainStat.LoadFromStorage;

  procedure LoadNode(APath: String);
  var
    Names: IisStringListRO;
    C: IisInterfacedObject;
    i: Integer;
  begin
    APath := NormalizePath(APath);
    Names := FStorage.GetValueNames(APath);
    for i := 0 to Names.Count - 1 do
    begin
      try
        C := TevADRStatCounter.Create(APath + Names[i], FStorage);
        C.LoadFromStorage;
        AddChild(C);
      except
        // eat exceptions if storage data is corrupted
      end;
    end;

    Names := FStorage.GetChildNodes(APath);
    for i := 0 to Names.Count - 1 do
      LoadNode(APath + Names[i]);
  end;

begin
  Lock;
  try
    RemoveAllChildren;
    LoadNode('');
  finally
    Unlock;
  end;
end;

procedure TevADRDomainStat.SetCounter(const ACounter: String; const AValue: Variant);
var
  C: IevADRStatCounter;
begin
  Lock;
  try
    C := GetCounter(ACounter);
    if not Assigned(C) then
    begin
      C := TevADRStatCounter.Create(ACounter, FStorage);
      AddChild(C as IisInterfacedObject);
    end;
  finally
    Unlock;
  end;

  C.Value := AValue;
end;

{ TevADRStatCounter }

function TevADRStatCounter.LastUpdate: TDateTime;
begin
  FStorage.Lock;
  try
    Result := FLastUpdate;
  finally
    FStorage.Unlock;
  end;
end;

function TevADRStatCounter.GetValue: Variant;
begin
  FStorage.Lock;
  try
    Result := FValue;
  finally
    FStorage.Unlock;
  end;
end;

procedure TevADRStatCounter.SetValue(const AValue: Variant);
begin
  FStorage.Lock;
  try
    FValue := AValue;
    FLastUpdate := Now;
    SaveToStorage;
  finally
    FStorage.Unlock;
  end;
end;

constructor TevADRStatCounter.Create(const AName: String; const AStorage: IisSettings);
begin
  inherited Create;
  SetName(AName);
  FStorage := AStorage;
end;

procedure TevADRStatCounter.LoadFromStorage;
var
  S: IisStream;
begin
  FStorage.Lock;
  try
    S := TisStream.Create;
    S.AsString := BinToStr(FStorage.AsString[GetName]);
    S.Position := 0;
    FValue := S.ReadVariant;
    FLastUpdate := S.ReadDouble;
  finally
    FStorage.Unlock;
  end;
end;

procedure TevADRStatCounter.RemoveFromStorage;
begin
  FStorage.DeleteValue(GetName);
end;

procedure TevADRStatCounter.SaveToStorage;
var
  S: IisStream;
begin
  S := TisStream.Create;
  S.WriteVariant(FValue);
  S.WriteDouble(FLastUpdate);

  FStorage.AsString[GetName] := StrToBin(S.AsString);
end;

{ TevDBStatusStorage }

procedure TevDBStatusStorage.ClearDBInfo(const ADatabase: String);
begin
  FStorage.AsString[ctx_DomainInfo.DomainName + '\' + ADatabase] := '';
end;

constructor TevDBStatusStorage.Create(const AStorageFolder: String);
begin
  inherited Create;
  ForceDirectories(AStorageFolder);
  FStorage := TisSettingsSQLite.Create(NormalizePath(AStorageFolder) + 'status.db');
end;

function TevDBStatusStorage.GetDBInfo(const ADatabase: String): IevADRDBInfo;
var
  S: IisStream;
begin
  Result := TevADRDBInfo.Create(ADatabase);

  FStorage.Lock;
  try
    S := TisStream.Create;
    S.AsString := BinToStr(FStorage.AsString[ctx_DomainInfo.DomainName + '\' + ADatabase]);
    if S.Size > 0 then
      try
        (Result as IisInterfacedObject).AsStream := S;
      except
        Result := TevADRDBInfo.Create(ADatabase);
      end;
      S := nil;
   finally
     FStorage.Unlock;
   end;
end;

procedure TevDBStatusStorage.SetDBInfo(const ADBInfo: IevADRDBInfo);
var
  S: IisStream;
begin
  S := (ADBInfo as IisInterfacedObject).AsStream;
  FStorage.AsString[ctx_DomainInfo.DomainName + '\' + ADBInfo.Database] := StrToBin(S.AsString);
end;

end.

