unit evADRRequestQueue;

interface

uses SysUtils, Classes, TypInfo,
     isExceptions, isBaseClasses, EvStreamUtils, isThreadManager, isBasicUtils,
     isSettings, isVCLBugFix, isLogFile, isErrorUtils,
     EvCommonInterfaces, EvContext, EvMainboard, EvBasicUtils, EvConsts;

type
  TevADRRequestStatus = (arsUnknown, arsQueued, arsExecuting, arsFinished, arsFinishedWithErrors, arsAborted);
  TevADRRequestStatusList = set of TevADRRequestStatus;

  IevADRRequest = interface
  ['{89F912A4-19D3-4F17-8A41-6C90014AC691}']
    function  TypeID: String;
    procedure Lock;
    procedure Unlock;
    function  GetID: String;
    function  Domain: String;
    function  Caption: String;
    function  Description: String;
    function  Status: TevADRRequestStatus;
    function  Progress: String;
    function  Error: String;
    function  OriginatedAt: TUTCDateTime;
    function  QueuedAt: TDateTime;
    function  StartedAt: TDateTime;
    function  FinishedAt: TDateTime;
    procedure MoveToTop;
    function  ReadyToRun: Boolean;
    procedure RunAgain;

    property  ID: String read GetID;
  end;


  IevADRRequestQueue = interface
  ['{0F0F27F9-7686-425C-B6A6-9FA39536534E}']
    procedure Lock;
    procedure Unlock;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetExecSlots: Integer;
    procedure SetExecSlots(const AValue: Integer);
    procedure SetDomainBallancing(const AValue: Boolean);
    procedure WakeUpPulser;
    procedure AddRequest(const ARequest: IevADRRequest; const APositionOf: IevADRRequest = nil);
    procedure DeleteRequest(const ARequest: IevADRRequest);
    function  Count: Integer;
    function  GetByIndex(Index: Integer): IevADRRequest;
    function  FindRequestByID(const ARequestID: String): IevADRRequest;
    function  QueryRequests(const ADomain: String; const AStatusList: TevADRRequestStatusList): IisList;
    function  GetPage(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisList;  // list of IevADRRequest

    property  Items[Index: Integer]: IevADRRequest read GetByIndex; default;
    property  Active: Boolean read GetActive write SetActive;
    property  ExecSlots: Integer read GetExecSlots write SetExecSlots;
  end;


  TevADRRequestQueue = class;

  TevADRRequest = class(TisNamedObject, IevADRRequest, IevContextCallbacks)
  private
    FID: String;
    FStatus: TevADRRequestStatus;
    FTaskID: TTaskID;
    FDomain: String;
    FQueuedAt: TDateTime;
    FStartedAt: TDateTime;
    FFinishedAt: TDateTime;
    FError: String;
    FProgress: IisStringList;
    FStorage: IisSettings;
    FOriginatedAt: TUTCDateTime;
    function  Execute: Boolean;
    procedure StopExecution;
    procedure SetError(const AError: String);
    procedure SetStatus(const AValue: TevADRRequestStatus);
    procedure StoreState;
    procedure RestoreState;
    procedure SetID(const AValue: String);

    //  IevContextCallbacks
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure SaveToStorage; override;
    procedure LoadFromStorage; override;
    procedure RemoveFromStorage; override;

    procedure Store; virtual;
    procedure Restore; virtual;
    function  QueueObj: TevADRRequestQueue;
    procedure BeforeRun; virtual;     // Always calls before Run
    procedure Run; virtual; abstract; // Business logic of the request
    procedure AfterRun(const AFailed: Boolean); virtual;      // Always calls after Run
    procedure DoOnError; virtual;
    procedure SetProgress(const AValue: String);
    function  Storage: IisSettings;
    function  DeleteOnError: Boolean; virtual;

    // IevADRRequest
    function  GetID: String;
    function  Domain: String;
    function  Caption: String; virtual;
    function  Description: String; virtual;
    function  Status: TevADRRequestStatus;
    function  Progress: String;
    function  Error: String;
    function  OriginatedAt: TUTCDateTime;
    function  QueuedAt: TDateTime;
    function  StartedAt: TDateTime;
    function  FinishedAt: TDateTime;
    procedure MoveToTop;
    function  ReadyToRun: Boolean; virtual;
    procedure RunAgain;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); reintroduce; virtual;
  end;

  TevADRRequestClass = class of TevADRRequest;


  TevADRRequestQueue = class(TisCollection, IevADRRequestQueue)
  private
    FPropertyLock: IisLock;
    FStorage: IisSettings;
    FPulser: TReactiveThread;
    FExecSlots: Integer;
    FStorageDir: String;
    FKnownDomains: IisStringList;
    FDomainBallancing: Boolean;
    procedure InternalAddRequest(const ARequest: IevADRRequest; const APositionOf: IevADRRequest = nil);
    procedure InternalDeleteRequest(const ARequest: IevADRRequest);
    procedure DoDeleteRequest(const ARequest: IevADRRequest);
    function  GetIndexList(const ADomainName: String): IisInterfaceList;
    procedure AddRequestInIndex(const ARequest: IevADRRequest; const APositionOf: IevADRRequest = nil);
    procedure RemoveRequestFromIndex(const ARequest: IevADRRequest);
    procedure MoveToTop(const ARequest: IevADRRequest);

  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure LoadFromStorage; override;
    procedure RemoveFromStorage; override;
    function  GetNextRequestToRun: IevADRRequest;
    procedure Clear;

    // IevADRRequestQueue
    procedure WakeUpPulser;
    procedure AddRequest(const ARequest: IevADRRequest; const APositionOf: IevADRRequest = nil);
    procedure DeleteRequest(const ARequest: IevADRRequest);
    function  Count: Integer;
    function  GetByIndex(Index: Integer): IevADRRequest;
    function  FindRequestByID(const AID: String): IevADRRequest;
    function  QueryRequests(const ADomain: String; const AStatusList: TevADRRequestStatusList): IisList;
    function  GetExecSlots: Integer;
    procedure SetExecSlots(const AValue: Integer);
    procedure SetDomainBallancing(const AValue: Boolean);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetPage(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisList;
  public
    constructor Create(const AStorageDir: String); reintroduce;
  end;

implementation

type
  TevADRQueuePulser = class(TReactiveThread)
  private
    FLock: IisLock;
    FQueue: TevADRRequestQueue;
    FRunningRequests: IisList;
    function  GetRunningRequests: IisList;
  protected
    procedure DoOnStart; override;
    procedure DoWork; override;
    procedure DoOnStop; override;
  public
    procedure AfterConstruction; override;
  end;


function RequestObject(const ARequest: IevADRRequest): TevADRRequest;
begin
  Result := TevADRRequest((ARequest as IisInterfacedObject).GetImplementation);
end;


{ TevADRRequest }

function TevADRRequest.Error: String;
begin
  Lock;
  try
    Result := FError;
  finally
    Unlock;
  end;
end;

function TevADRRequest.FinishedAt: TDateTime;
begin
  Lock;
  try
    Result := FFinishedAt;
  finally
    Unlock;
  end;
end;

function TevADRRequest.Caption: String;
begin
  Result := 'Unspecified';
end;

function TevADRRequest.Progress: String;
begin
  Lock;
  try
    Result := FProgress[FProgress.Count - 1];
  finally
    Unlock;
  end;
end;

function TevADRRequest.GetID: String;
begin
  Result := FID;
end;

function TevADRRequest.StartedAt: TDateTime;
begin
  Lock;
  try
    Result := FStartedAt;
  finally
    Unlock;
  end;
end;

function TevADRRequest.Status: TevADRRequestStatus;
begin
  Lock;
  try
    Result := FStatus;
  finally
    Unlock;
  end;
end;

procedure TevADRRequest.SetError(const AError: String);
begin
  Lock;
  try
    if FError <> AError then
    begin
      FError := AError;
      StoreState;
    end;
  finally
    Unlock;
  end;
end;

procedure TevADRRequest.SetStatus(const AValue: TevADRRequestStatus);
var
  PrevStatus: TevADRRequestStatus;
begin
  Lock;
  try
    if AValue <> FStatus then
    begin
      PrevStatus := FStatus;
      FStatus := AValue;

      if FStatus <> arsExecuting then
        FTaskID := 0;

      if FStatus = arsExecuting then
      begin
        FStartedAt := Now;
        FFinishedAt := 0;
      end

      else if FStatus in [arsFinished, arsFinishedWithErrors, arsAborted] then
        FFinishedAt := Now
      else
      begin
        FStartedAt := 0;
        FFinishedAt := 0;
      end;

      if (FStatus = arsQueued) and ((FQueuedAt = 0) or (PrevStatus in [arsFinished, arsFinishedWithErrors, arsAborted])) then
        FQueuedAt := Now;

      StoreState;

      if (PrevStatus = arsExecuting) and (FStatus = arsFinishedWithErrors) then
        DoOnError;
    end;
  finally
    Unlock;
  end;
end;

function TevADRRequest.Description: String;
begin
  Result := '';
end;

procedure TevADRRequest.DoOnError;
begin
  GlobalLogFile.AddEvent(etError, 'Replication', Domain + ': ' + Caption, Error);
end;

procedure TevADRRequest.MoveToTop;
var
  Q: TevADRRequestQueue;
begin
  Q := QueueObj;
  if Assigned(Q) then
    Q.MoveToTop(Self);
end;

procedure TevADRRequest.RunAgain;
var
  Q: IevADRRequestQueue;
begin
  Q := QueueObj;
  if Assigned(Q) and (Status in [arsFinishedWithErrors, arsAborted]) then
    Q.AddRequest(Self);
end;

function TevADRRequest.QueuedAt: TDateTime;
begin
  Lock;
  try
    Result := FQueuedAt;
  finally
    Unlock;
  end;
end;

function TevADRRequest.ReadyToRun: Boolean;
begin
  Result := True;
end;

procedure TevADRRequest.SetProgress(const AValue: String);
begin
  Lock;
  try
    FProgress[FProgress.Count - 1] := AValue;
  finally
    Unlock;
  end;
end;

class function TevADRRequest.GetTypeID: String;
begin
  Result := 'Unknown';
end;

procedure TevADRRequest.DoOnConstruction;
begin
  inherited;
  InitLock;

  FProgress := TisStringList.Create;
  FProgress.Add('');

  SetID(GetUniqueID);

  if (Context <> nil) and (ctx_DomainInfo <> nil) then
    FDomain := ctx_DomainInfo.DomainName;
end;

procedure TevADRRequest.RestoreState;
var
  ID: String;
begin
  ID := GetID;

  FStatus := TevADRRequestStatus(Storage.AsInteger[ID + '\Status']);
  FQueuedAt := Storage.AsDateTime[ID + '\QueuedAt'];
  FStartedAt := Storage.AsDateTime[ID + '\StartedAt'];
  FFinishedAt := Storage.AsDateTime[ID + '\FinishedAt'];
  FError := Storage.AsString[ID + '\Error'];

  FOriginatedAt := Storage.AsString[ID + '\OriginatedAt'];
  if FOriginatedAt = '' then
    FOriginatedAt := LocalDateTimeToUTC(FQueuedAt);
end;

procedure TevADRRequest.StoreState;
var
  ID: String;
begin
  ID := GetID;

  Storage.AsInteger[ID + '\Status'] := Ord(FStatus);
  Storage.AsString[ID + '\OriginatedAt'] := FOriginatedAt;
  Storage.AsDateTime[ID + '\QueuedAt'] := FQueuedAt;
  Storage.AsDateTime[ID + '\StartedAt'] := FStartedAt;
  Storage.AsDateTime[ID + '\FinishedAt'] := FFinishedAt;
  Storage.AsString[ID + '\Error'] := FError;
end;

function TevADRRequest.Storage: IisSettings;
begin
  Result := FStorage;
end;

function TevADRRequest.QueueObj: TevADRRequestQueue;
var
  O: IisCollection;
begin
  O := GetOwner;
  if Assigned(O) then
    Result := TevADRRequestQueue(O.GetImplementation)
  else
    Result := nil;
end;

function TevADRRequest.Execute: Boolean;

  procedure RunInThread(const Params: TTaskParamList);
  var
    R: TevADRRequest;
    Q: TevADRRequestQueue;
    LogicError: Boolean;
    Deleted: Boolean;
  begin
    R := RequestObject(IInterface(Params[0]) as IevADRRequest);

    Q := R.QueueObj;
    Deleted := False;    
    try
      try
        R.SetStatus(arsExecuting);
        try
          LogicError := False;
          R.BeforeRun;
          try
            try
              R.Run;
            except
              LogicError := True;
              raise;
            end
          finally
            R.AfterRun(LogicError);
          end;
        finally
          if R.QueueObj = nil then
            Deleted := True;
        end;

        if Deleted or CurrentThreadTaskTerminated then
          Exit;

        R.SetError('');
        R.SetStatus(arsFinished);

        if Assigned(Q) then
          Q.DoDeleteRequest(R);
      except
        on E: Exception do
          if not Deleted and not CurrentThreadTaskTerminated then
          begin
            R.SetError(BuildStackedErrorStr(E));
            R.SetStatus(arsFinishedWithErrors);
            if R.DeleteOnError then
              Q.DoDeleteRequest(R);
          end;
      end;

    finally
      try
        if not Deleted and (R.Status = arsExecuting) then
        begin
          R.SetError('Execution aborted');
          R.SetStatus(arsAborted);
        end;
      finally
        Q.WakeUpPulser;
      end;
    end;
  end;

var
  StartingTask: TTaskID;
begin
  Lock;
  try
    if FTaskID = 0 then
    begin
      FTaskID := GlobalThreadManager.RunTask(@RunInThread, MakeTaskParams([Self as IevADRRequest]), 'Request "' + Caption + '"');
      StartingTask := FTaskID;
      Result := True;
    end
    else
    begin
      Result := False;
      StartingTask := 0;
    end;  
  finally
    Unlock;
  end;

  if Result then
    repeat
      Sleep(0);
    until (Status <> arsQueued) or CurrentThreadTaskTerminated or GlobalThreadManager.TaskIsTerminated(StartingTask);
end;

procedure TevADRRequest.StopExecution;
var
  Tsk: TTaskID;
begin
  Lock;
  try
    Tsk := FTaskID;
  finally
    Unlock;
  end;

  if Tsk <> 0 then
  begin
    GlobalThreadManager.TerminateTask(Tsk);
    GlobalThreadManager.WaitForTaskEnd(Tsk);
  end;
end;

procedure TevADRRequest.LoadFromStorage;
begin
  Restore;
end;

procedure TevADRRequest.RemoveFromStorage;
begin
  if Storage <> nil then
    Storage.DeleteNode(GetID);
end;

procedure TevADRRequest.SaveToStorage;
begin
  Storage.BeginUpdate;
  try
    Store;
  finally
    Storage.EndUpdate;
  end;
end;

procedure TevADRRequest.SetID(const AValue: String);
begin
  SetName(AValue);
  FID := GetName;
end;

procedure TevADRRequest.AfterRun(const AFailed: Boolean);
begin
  if FDomain <> '' then
  begin
    Context.SetCallbacks(nil);
    Mainboard.ContextManager.DestroyThreadContext;
  end;  
end;

procedure TevADRRequest.BeforeRun;
var
  EvoDomain: IevDomainInfo;
begin
  if FDomain <> '' then
  begin
    EvoDomain := mb_GlobalSettings.DomainInfoList.GetDomainInfo(FDomain);
    if not Assigned(EvoDomain) then
      raise Exception.CreateFmt('Evo domain "%s" is not valid', [FDomain]);

    Mainboard.ContextManager.CreateThreadContext(
      EncodeUserAtDomain(sADRUserName, FDomain),
      EvoDomain.SystemAccountInfo.Password);

    Context.SetCallbacks(Self);
  end;
end;

function TevADRRequest.Domain: String;
begin
  Result := FDomain;
end;

constructor TevADRRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  FOriginatedAt := AOriginatedAt;
  inherited Create;
end;

procedure TevADRRequest.DoOnDestruction;
begin
  inherited;

end;

procedure TevADRRequest.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
begin
// a plug
end;

procedure TevADRRequest.EndWait;
begin
  Lock;
  try
    if FProgress.Count > 1 then
      FProgress.Delete(FProgress.Count - 1)
    else
      SetProgress('');  
  finally
    Unlock;
  end;
end;

procedure TevADRRequest.StartWait(const AText: string; AMaxProgress: Integer);
begin
  Lock;
  try
    FProgress.Add(AText);
  finally
    Unlock;
  end;
end;

procedure TevADRRequest.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  SetProgress(AText);
end;

procedure TevADRRequest.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

function TevADRRequest.DeleteOnError: Boolean;
begin
  Result := False;
end;

function TevADRRequest.OriginatedAt: TUTCDateTime;
begin
  Result := FOriginatedAt;
end;

procedure TevADRRequest.Store;
begin
  Storage.AsString[GetID + '\Type'] := TypeID;
  Storage.AsString[GetID + '\Domain'] := FDomain;
  StoreState;
end;

procedure TevADRRequest.Restore;
begin
  Assert(Storage.AsString[GetID + '\Type'] = TypeID);
  FDomain := Storage.AsString[GetID + '\Domain'];
  RestoreState;
end;

{ TevADRRequestQueue }

procedure TevADRRequestQueue.Clear;
var
  PrevActive: Boolean;
begin
  PrevActive := GetActive;
  Lock;
  try
    SetActive(False);
    RemoveFromStorage;
    FKnownDomains.Clear;
    RemoveAllChildren;
  finally
    Unlock;
    SetActive(PrevActive);
  end;
end;

function TevADRRequestQueue.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevADRRequestQueue.DeleteRequest(const ARequest: IevADRRequest);
begin
  // Do not delete request if it tries to delete itself
  if RequestObject(ARequest).FTaskID = GetCurrentThreadTaskID then
    Exit;

  RequestObject(ARequest).StopExecution;
  DoDeleteRequest(ARequest);
  WakeUpPulser;
end;

function TevADRRequestQueue.GetActive: Boolean;
begin
  Result := Assigned(FPulser);
end;

function TevADRRequestQueue.GetByIndex(Index: Integer): IevADRRequest;
begin
  Result := GetChild(Index) as IevADRRequest;
end;

function TevADRRequestQueue.QueryRequests(const ADomain: String; const AStatusList: TevADRRequestStatusList): IisList;
var
  i: Integer;
begin
  if AStatusList = [arsExecuting] then
  begin
    // Use a shortcut for executing tasks
    FPropertyLock.Lock;
    try
      if GetActive then
        Result := TevADRQueuePulser(FPulser).GetRunningRequests;

      if ADomain <> '' then
        for i := Result.Count - 1 downto 0 do
          if not AnsiSameText((Result[i] as IevADRRequest).Domain, ADomain) then
            Result.Delete(i);
    finally
      FPropertyLock.Unlock;
    end;
  end

  else
  begin
    if ADomain = '' then
      Result := GetChildrenList
    else
      Result := GetIndexList(ADomain).GetList;

    if AStatusList <> [] then
      for i := Result.Count - 1 downto 0 do
       if not ((Result[i] as IevADRRequest).Status in AStatusList) then
         Result.Delete(i);
  end;

  if not Assigned(Result) then
    Result := TisList.Create;
end;

procedure TevADRRequestQueue.SetActive(const AValue: Boolean);
var
  PulserToDestroy: TObject;
begin
  PulserToDestroy := nil;

  FPropertyLock.Lock;
  try
    if not GetActive and AValue then
    begin
      FPulser := TevADRQueuePulser.Create(ClassName + ' pulser', tpHigher);
      TevADRQueuePulser(FPulser).FQueue := Self;
      WakeUpPulser;
    end

    else if GetActive and not AValue then
    begin
      PulserToDestroy := FPulser;
      FPulser := nil;
    end
   finally
     FPropertyLock.Unlock;
   end;

   FreeAndNil(PulserToDestroy);
end;

procedure TevADRRequestQueue.MoveToTop(const ARequest: IevADRRequest);
var
  i: Integer;
  QueueItems: IisInterfaceList;
begin
  Lock;
  try
    i := IndexOf(ARequest as IisInterfacedObject);
    if i <> -1 then
    begin
      MoveChild(i, 0);
      QueueItems := GetIndexList(ARequest.Domain);
      i := QueueItems.IndexOf(ARequest as IisInterfacedObject);
      QueueItems.Move(i, 0);
      
      WakeUpPulser;
    end;  
  finally
    Unlock;
  end;
end;

function TevADRRequestQueue.FindRequestByID(const AID: String): IevADRRequest;
begin
  Result := FindChildByName(AID) as IevADRRequest;
end;

procedure TevADRRequestQueue.DoOnDestruction;
begin
  SetActive(False);
  inherited;
end;

function TevADRRequestQueue.GetExecSlots: Integer;
begin
  FPropertyLock.Lock;
  try
    Result := FExecSlots;
  finally
    FPropertyLock.Unlock;
  end;
end;

procedure TevADRRequestQueue.SetExecSlots(const AValue: Integer);
begin
  FPropertyLock.Lock;
  try
    FExecSlots := AValue;
  finally
    FPropertyLock.Unlock;
  end;

  WakeUpPulser;  
end;

procedure TevADRRequestQueue.WakeUpPulser;
begin
  if GetActive then
    FPulser.Wakeup;
end;

procedure TevADRRequestQueue.AddRequest(const ARequest, APositionOf: IevADRRequest);
begin
  Lock;
  try
    if IndexOf(ARequest as IisInterfacedObject) = -1 then
    begin
      InternalAddRequest(ARequest, APositionOf);
      RequestObject(ARequest).FStorage := FStorage;      
      RequestObject(ARequest).SetStatus(arsQueued);
      RequestObject(ARequest).SaveToStorage;
      WakeUpPulser;
    end

    else if not (ARequest.Status in [arsQueued, arsExecuting]) then
    begin
      RequestObject(ARequest).SetStatus(arsQueued);
      WakeUpPulser
    end;
  finally
    Unlock;
  end;
end;

function TevADRRequestQueue.GetNextRequestToRun: IevADRRequest;
var
  i, j: Integer;
  Requests: IisList;
  Domains: IisStringList;
  V1, V2: IisVariant;

  function CountRequestForDomain(const ADomain: String): Integer;
  var
    i: Integer;
    R: IevADRRequest;
  begin
    Result := 0;
    for i := 0 to Requests.Count - 1 do
    begin
      R := Requests[i] as IevADRRequest;
      if AnsiSameText(R.Domain, ADomain) then
        Inc(Result);
    end;
  end;

  function GetDomainRequestToRun(const ADomain: String): IevADRRequest;
  var
    i: Integer;
    R: IevADRRequest;
    Requests: IisList;
  begin
    Result := nil;
    Requests := QueryRequests(ADomain, [arsQueued]);
    for i := 0 to Requests.Count - 1 do
    begin
      R := Requests[i] as IevADRRequest;
      if R.ReadyToRun then
      begin
        Result := R;
        break;
      end;
    end;
  end;

  function GetAnyRequestToRun: IevADRRequest;
  var
    i: Integer;
    R: IevADRRequest;
  begin
    Lock;
    try
      for i := 0 to ChildCount - 1 do
      begin
        R := GetByIndex(i) as IevADRRequest;
        if (R.Status = arsQueued) and R.ReadyToRun then
        begin
          Result := R;
          break;
        end;
      end;
    finally
      Unlock;
    end;
  end;


begin
  Result := nil;

  if not FDomainBallancing then
  begin
    Result := GetAnyRequestToRun;
    Exit;
  end;

// Prepare sorted domain list
  Domains := TisStringList.Create;
  Domains.Text := FKnownDomains.Text;

  // Collect info about executing requests
  Requests :=  QueryRequests('', [arsExecuting]);
  for i := 0 to Domains.Count - 1 do
  begin
    V1 := TisVariant.Create;
    V1.Value := CountRequestForDomain(Domains[i]);
    Domains.Objects[i] := V1;
  end;

  // Sort it by executing requests
  for i := 0 to Domains.Count - 2 do
  begin
    V1 := Domains.Objects[i] as IisVariant;
    for j := i + 1 to Domains.Count - 1 do
    begin
      V2 := Domains.Objects[j] as IisVariant;
      if V1.Value > V2.Value then
      begin
        V1 := V2;
        Domains.Exchange(i, j);
      end;
    end;
  end;

  // Find request for running
  for i := 0 to Domains.Count - 1 do
  begin
    Result := GetDomainRequestToRun(Domains[i]);
    if Assigned(Result) then
    begin
      // Move this domain to the bottom of list, so it will simulate domains rotation (if request count is equal for many of them)
      FKnownDomains.Lock;
      try
        j := FKnownDomains.IndexOf(Domains[i]);
        if (j <> -1) and (j < FKnownDomains.Count - 1) then
          FKnownDomains.Move(j, FKnownDomains.Count - 1);
      finally
        FKnownDomains.Unlock;
      end;
      break;
    end;
  end;
end;

procedure TevADRRequestQueue.InternalAddRequest(const ARequest, APositionOf: IevADRRequest);
var
  i: Integer;
begin
  if Assigned(APositionOf) then
  begin
    i := IndexOf(APositionOf as IisInterfacedObject);
    if i = -1 then
      i := 0;
    InsertChild(i, ARequest as IisInterfacedObject);
  end
  else
    AddChild(ARequest as IisInterfacedObject);

  AddRequestInIndex(ARequest, APositionOf);
end;

procedure TevADRRequestQueue.LoadFromStorage;
var
  PrevActive: Boolean;
  RequestID: String;
  RequestTypeID: String;
  Request: IevADRRequest;
  RequestIDs: IisStringListRO;
  i: Integer;
begin
  PrevActive := GetActive;
  SetActive(False);

  Lock;
  try
    RemoveAllChildren;

    RequestIDs := FStorage.GetChildNodes('');
    for i := 0 to RequestIDs.Count - 1 do
    begin
      RequestID := RequestIDs[i];
      RequestTypeID := FStorage.AsString[RequestID + '\Type'];
      try
        Request := nil;
        Request := ObjectFactory.CreateInstanceOf(RequestTypeID) as IevADRRequest;
        RequestObject(Request).SetID(RequestID);
        RequestObject(Request).FStorage := FStorage;
        RequestObject(Request).LoadFromStorage;
        InternalAddRequest(Request);

        if (RequestObject(Request).Status = arsFinished) or
           (RequestObject(Request).Status = arsFinishedWithErrors) and RequestObject(Request).DeleteOnError then
        begin
          InternalDeleteRequest(Request);
          RequestObject(Request).FStorage := nil;
          Request := nil;
        end;
      except
        // in case if request got screwed
        if Assigned(Request) then
        begin
          InternalDeleteRequest(Request);
          RequestObject(Request).FStorage := nil;
          Request := nil;
        end
        else
          FStorage.DeleteNode(RequestID);
      end;

      if Assigned(Request) and  not (Request.Status in [arsQueued, arsFinished, arsFinishedWithErrors]) then
        RequestObject(Request).SetStatus(arsQueued);
    end;

  finally
    Unlock;
    SetActive(PrevActive);
  end;
end;

procedure TevADRRequestQueue.RemoveFromStorage;
begin
  FStorage.DeleteNode('');
  inherited;
end;

procedure TevADRRequestQueue.DoOnConstruction;
begin
  inherited;
  InitLock;
  FPropertyLock := TisLock.Create;
  FStorage := TisSettingsSQLite.Create(FStorageDir + 'queue.db');
  FExecSlots := HowManyProcessors * 2;
  FKnownDomains := TisStringList.Create(nil, True);
  FDomainBallancing := True;
  LoadFromStorage;
end;

constructor TevADRRequestQueue.Create(const AStorageDir: String);
begin
  FStorageDir := NormalizePath(AStorageDir);
  if FStorageDir = '' then
    FStorageDir := AppDir;

  inherited Create;
end;

function TevADRRequestQueue.GetPage(const APageNbr, AItemsPerPage: Integer;
  out APageCount: Integer): IisList;
var
  i: Integer;
  BegNbr, EndNbr: Integer;
begin
  if APageNbr <= 0 then
    raise EisException.CreateFmt('Page number %d is out of bounds', [APageNbr]);
  Result := TisList.Create;
  Result.Capacity := AItemsPerPage;

  Lock;
  try
    BegNbr := (APageNbr - 1) * AItemsPerPage;
    EndNbr := BegNbr + AItemsPerPage;
    if EndNbr > Count - 1 then
      EndNbr := Count - 1;

    for i := BegNbr to EndNbr do
      Result.Add(GetByIndex(i));

    APageCount := Count div AItemsPerPage;
    if Count mod AItemsPerPage > 0 then
      Inc(APageCount);
  finally
    UnLock;
  end;
end;

procedure TevADRRequestQueue.InternalDeleteRequest(const ARequest: IevADRRequest);
begin
  RemoveRequestFromIndex(ARequest);
  RequestObject(ARequest).RemoveFromStorage;
  RemoveChild(ARequest as IisInterfacedObject);
end;

procedure TevADRRequestQueue.DoDeleteRequest(const ARequest: IevADRRequest);
begin
  Lock;
  try
    InternalDeleteRequest(ARequest);
    RequestObject(ARequest).FStorage := nil;
  finally
    Unlock;
  end;
end;

procedure TevADRRequestQueue.SetDomainBallancing(const AValue: Boolean);
begin
  FDomainBallancing := AValue;
end;

procedure TevADRRequestQueue.AddRequestInIndex(const ARequest: IevADRRequest; const APositionOf: IevADRRequest = nil);
var
  i: Integer;
  QueueItems: IisInterfaceList;
begin
  QueueItems := GetIndexList(ARequest.Domain);

  QueueItems.Lock;
  try
    if Assigned(APositionOf) then
    begin
      i := QueueItems.IndexOf(APositionOf);
      if i <> -1 then
        i := 0;
      QueueItems.Insert(i, ARequest);
    end
    else
      QueueItems.Add(ARequest);
  finally
    QueueItems.Unlock;
  end;
end;

procedure TevADRRequestQueue.RemoveRequestFromIndex(const ARequest: IevADRRequest);
var
  QueueItems: IisInterfaceList;
begin
  QueueItems := GetIndexList(ARequest.Domain);
  QueueItems.Delete(ARequest);
end;

function TevADRRequestQueue.GetIndexList(const ADomainName: String): IisInterfaceList;
var
  i: Integer;
begin
  FKnownDomains.Lock;
  try
    i := FKnownDomains.IndexOf(ADomainName);
    if i = -1 then
    begin
      Result := TisInterfaceList.Create(nil, True);
      FKnownDomains.AddObject(ADomainName, Result);
    end
    else
      Result := FKnownDomains.Objects[i] as IisInterfaceList;
  finally
    FKnownDomains.Unlock;
  end;
end;

{ TevADRQueuePulser }

procedure TevADRQueuePulser.AfterConstruction;
begin
  inherited;
  FLock := TisLock.Create;
end;

procedure TevADRQueuePulser.DoOnStart;
begin
  inherited;
  FRunningRequests := TisList.Create;
end;

procedure TevADRQueuePulser.DoOnStop;
var
  i: Integer;
  TasksToStop: array of TTaskID;
begin
  FLock.Lock;
  try
    SetLength(TasksToStop, FRunningRequests.Count);
    for i := 0 to FRunningRequests.Count - 1 do
      TasksToStop[i] := RequestObject(FRunningRequests[i] as IevADRRequest).FTaskID;

    // Send termination signal to all requests
    for i := Low(TasksToStop) to High(TasksToStop) do
      if TasksToStop[i] <> 0 then
        GlobalThreadManager.TerminateTask(TasksToStop[i]);

    // Wait until the last one has stopped
    for i := Low(TasksToStop) to High(TasksToStop) do
      if TasksToStop[i] <> 0 then
        GlobalThreadManager.WaitForTaskEnd(TasksToStop[i]);

    FRunningRequests := nil;        
  finally
    FLock.Unlock;
  end;

  inherited;
end;

procedure TevADRQueuePulser.DoWork;
var
  i: Integer;
  R: IevADRRequest;
begin
  FLock.Lock;
  try
    //Update running requests
    for i := FRunningRequests.Count - 1 downto 0 do
    begin
      R := FRunningRequests[i] as IevADRRequest;
      if (R.Status <> arsExecuting) or (RequestObject(R).QueueObj = nil) then
        FRunningRequests.Delete(i);
      R := nil;
    end;

    // Run new requests
    while FRunningRequests.Count < FQueue.FExecSlots do
    begin
      try
        R := FQueue.GetNextRequestToRun;
      except
        on E: Exception do
        begin
          GlobalLogFile.AddEvent(etFatalError, 'Replication', E.Message, GetStack(E));
          R := nil;
        end;
      end;

      if not Assigned(R)then
        break;

      if RequestObject(R).Execute then
        FRunningRequests.Add(R)
      else
        Sleep(0);
    end;
  finally
    FLock.Unlock;  
  end;
end;

function TevADRQueuePulser.GetRunningRequests: IisList;
begin
  FLock.Lock;
  try
    if Assigned(FRunningRequests) then
      Result := FRunningRequests.GetCopy
    else
      Result := TisList.Create;
  finally
    FLock.Unlock;
  end;
end;

end.