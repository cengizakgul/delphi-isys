unit evCommonDatagramDispatcher;

interface

uses SysUtils, Windows, isBaseClasses, EvCommonInterfaces, isSocket, isStreamTCP, isTransmitter,
     EvTransportShared, EvMainboard, EvTransportDatagrams, Dialogs, EvTypes, evRemoteMethods,
     isBasicUtils, evContext, SReportSettings, EvStreamUtils,  EvDataSet,
     Classes, EvConsts, EvTransportInterfaces, EvBasicUtils, EvClasses, isThreadManager,
     EvDataAccessComponents, DB, evExceptions, isAppIDs;

type
  TevCommonDatagramDispatcher = class(TevDatagramDispatcher)
  private
    FProgressUpdater: IisCollection;
  protected
    // Wait Object call back. It needs to be activated in most cases on server side only.
    procedure ActivateProgressUpdater;
    procedure DeactivateProgressUpdater;

    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;

    // RW Remote Engine
    procedure dmRWRemoteEngineRunReports(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRWRemoteEngineRunQBQuery(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRWRemoteEngineSetLowerRWEnginePriority(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRWRemoteEngineStopReportExecution(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // DB Access
    procedure dmDBAccessCleanDeletedClientsFromTemp(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDBVersion(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetGeneratorValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessSetGeneratorValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetClientsList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDataSets(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessOpenQuery(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessExecQuery(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessExecStoredProc(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessCreateClientDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessDeleteClientDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessBeginRebuildAllClients(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessRebuildTemporaryTables(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessEndRebuildAllClients(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessCommitTransactions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessDisableDBs(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessEnableDBs(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDisabledDBs(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessBackupDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessRestoreDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessSweepDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessDropDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessUndeleteClientDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetLastTransactionNbr(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetTransactionsStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDataSyncPacket(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessApplyDataSyncPacket(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDBHash(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessCheckDBHash(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetClientROFlag(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessSetClientROFlag(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetNewKeyValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetFieldValues(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetFieldVersions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessUpdateFieldVersion(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessUpdateFieldValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetVersionFieldAudit(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetVersionFieldAuditChange(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmDBAccessGetRecordAudit(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetBlobAuditData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetTableAudit(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetDatabaseAudit(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDBAccessGetInitialCreationNBRAudit(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Security
    procedure dmSecurityGetAuthorization(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetSecStructure(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetUserSecStructure(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetGroupSecStructure(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecuritySetGroupSecStructure(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecuritySetUserSecStructure(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityCreateEEAccount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityChangePassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityEnableSystemAccountRights(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetUserGroupLevelRights(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetUserRights(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetGroupRights(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetAvailableQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetAvailableExtQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecuritySetQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecuritySetExtQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityResetPassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityCheckAnswers(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGenerateNewPassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityValidatePassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetPasswordRules(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetExtQuestions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityCheckExtAnswers(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSecurityGetSecurityRules(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Global
    procedure dmGlobalFlagManagerSetFlag(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerTryLock(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerTryLock2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerTryUnlock(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerTryUnlock2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerReleaseFlag(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerForcedUnlock(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerForcedUnlock2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerGetLockedFlag(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerGetLockedFlag2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerGetLockedFlagList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalFlagManagerGetSetFlagList(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Global Settings
    procedure dmGlobalSettingsGet(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Version Update
    procedure dmVersionUpdateRemoteGetVersionInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVersionUpdateRemoteCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVersionUpdateRemoteGetUpdatePack(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Time Source
    procedure dmTimeSourceGetDateTime(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // EMailer
    procedure dmEMailerSendMail(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmEMailerSendQuickMail(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Remote Misc Routines
    procedure dmMiscCalcFinalizeTaxPayments(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalcPrintChecksAndCouponsForTaxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalcPayTaxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalcPreProcessForQuarter(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalculateTaxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetSystemAccountNbr(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalculateChecklines(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscRefreshEDS(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCreateCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCreateManualCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetManualTaxesListForCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscTCImport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetW2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscChangeCheckStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscHolidayDay(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscNextPayrollDates(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscReportToPDF(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscAllReportsToPDF(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetPDFReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetAllPDFReports(const ADatagram: IevDatagram; out AResult: IevDatagram);    
    procedure dmMiscRunReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetTaskList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetSysReportByNbr(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetReportFileByNbrAndType(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscSaveAPIBilling(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetAPIBilling(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetEEW2List(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetEEScheduled_E_DS(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalcEEScheduled_E_DS_Rate(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCalcEEScheduled_E_DS_Rate2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetSbEMail(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscAddTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscUpdateTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetTOARequestsForEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetTOARequestsForMgr(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscProcessTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetWCReportList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetPayrollTaxTotals(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetPayrollEDTotals(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscAddTOAForNewEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscAddEDsForNewEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetTOARequestsForCO(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscUpdateTOARequestsForCO(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscDeleteRowsInDatasets(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCreateEEChangeRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetEEChangeRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscForMgrGetEEChangeRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscProcessEEChangeRequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetCoCalendarDefaults(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscCreateCoCalendar(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscProcessSetEeEMail(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscStoreBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetBenefitRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscSendEmailToHR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscSendEmailToManagers(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetEeExistingBenefits(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscDeleteBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetEeBenefitsPackageAvaliableForEnrollment(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetSummaryBenefitEnrollmentStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscRevertBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetBenefitRequestConfirmationReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscSubmitBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscSendBenefitEMailToEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscEmptyBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscClosebenefitEnrollment(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetDataDictionaryXML(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscApproveBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetBenefitEmailNotificationsList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscDeleteEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscProcessStoreReportParametersFromXML(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscProcessGetCoReportsParametersAsXML (const Adatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscConvertXMLtoRWParameters(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmMiscGetACAHistory(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscPostACAHistory(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetACAOfferCodes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMiscGetACAReliefCodes(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Payroll Processing
    procedure dmPayrollProcessCleanupTaxableTips(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessCreatePR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessGetPayrollBatchDefaults(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessCreatePRBatchDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessGrossToNet(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessCreateTaxAdjCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessTaxCalculator(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDoERTaxAdjPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDoQtrEndTaxLiabAdj(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDoQuarterEndCleanup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDoMonthEndCleanup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessIsQuarterEndCleanupNecessary(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessLoadYTD(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessGetEEYTD(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessProcessBilling(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessProcessBillingForServices(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessProcessPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessProcessManualACH(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessProcessPrenoteACH(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessRecreateVmrHistory(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessReRunPayrolReportsForVmr(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessReprintPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessPrintPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessCopyPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessVoidPayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessVoidCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessUnvoidCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDeletePayroll(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessDeleteBatch(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollSetPayrollLock(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPayrollProcessRefreshScheduledEDsBatch(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmPayrollProcessRedistributeDBDT(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Payroll Check Printing

   procedure dmPayrollProcessRefreshScheduledEDsCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);    // Payroll Check Printing
    procedure dmPrCheckPrintingProxyPrintChecks(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPrCheckPrintingProxyCheckStubBlobToDatasets(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // VMR
    procedure dmVMRRemoteCheckScannedBarcode(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteDeleteFile(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteFileDoesExist(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteFindInBoxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteGetSBPrinterList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteGetPrinterList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteSetPrinterList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemotePrintRwResults(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemotePutInBoxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteRemoveFromBoxes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteRemoveScannedBarcode(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteRemoveScannedBarcodes(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteRetrieveFile(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteStoreFile(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRRemoteRetrievePickupSheetsXMLData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRProcessRelease(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRProcessRevert(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRGetVMRReportList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRGetVMRReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRDeleteDirectory(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRPurgeMailBox(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRPurgeClientMailBox(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRPurgeMailBoxContentList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRGetRemoteVMRReportList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmVMRGetRemoteVMRReport(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // License
    procedure dmLicenseGetLicenseInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);

    //Task Queue
    procedure dmTaskQueueCreateTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueAddTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueAddTasks(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueRemoveTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueGetTaskInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueGetTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueGetTaskRequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueGetUserTaskStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueGetUserTaskInfoList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTaskQueueModifyTaskProperty(const ADatagram: IevDatagram; out AResult: IevDatagram);

    //Async Calls
    procedure dmAsyncFunctionsMakeCall(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmAsyncFunctionsSetCallPriority(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmAsyncFunctionsTerminateCall(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmAsyncFunctionsSetCalcCapacity(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Global Callbacks
    procedure dmGlobalCallbacksSubscribe(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksUnsubscribe(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksSecurityChange(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksTaskSchedulerChange(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksTaskQueueEvent(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksGlobalFlagChange(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksGlobalSettingsChange(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksPopupMessage(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGlobalCallbacksSetDBMaintenance(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Context Callbacks
    procedure dmContextCallbacksUpdateWait(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmContextCallbacksAsyncCallReturn(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // EvoX Engine
    procedure dmEvoXEngineImport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmEvoXEngineCheckMapFile(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Single Sign On
    procedure dmSingleSignOnGetSwipeClockOneTimeURL(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSingleSignOnGetMRCOneTimeURL(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSingleSignOnGetSwipeClockInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);

    // Dashboard Data Provider
    procedure dmDashboardDataProviderGetCompanyReports(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDashboardDataProviderGetCompanyMessages(const ADatagram: IevDatagram; out AResult: IevDatagram);

  protected
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure DispatchDatagram(const ADatagram: IevDatagram; out AResult: IevDatagram); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
    procedure BeforeSendMethodResult(const ASession: IevSession; const AMethod, AResult: IevDatagram); override;
  end;


implementation

uses DateUtils, ISKbmMemDataSet, isStatistics, Variants;

type
  TevWaitInfo = record
    Text: String;
    Progress: Integer;
  end;

  IevContextCallbacksProxy = interface
  ['{EBB96EB8-34B7-4F5C-B8A7-56FD754D6ED2}']
    procedure SendProgressInf;
  end;

  TevContextCallbacksProxy = class(TisInterfacedObject, IevContextCallbacks, IevContextCallbacksProxy)
  private
    FSession: IevSession;
    FCallerDatagramHeader: IevDatagramHeader;
    FProgressInfoStack: IisList;
    FProgressLastSendTime: TDateTime;
    FLastSendProgressInfo: IevProgressInfo;
  protected
    procedure SendProgressInf;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
  public
    constructor Create(const ASession: IevSession; const ACallerDatagramHeader: IevDatagramHeader); reintroduce;
  end;


  TevProgressUpdater = class(TisCollection)
  private
    FTask: TTaskId;
    FWakeUpEvent: THandle;
    procedure BackgroundSender(const Params: TTaskParamList);
    procedure WakeUp;
  protected
    procedure DoOnConstruction; override;
  public
    destructor Destroy; override;
  end;


  IevTransactionKeyRemap = interface
  ['{679D0EE6-3AD6-42D8-A835-3EE13934F38D}']
    function Execute: IisValueMap;
  end;

  TEvRemapDataSet = class(TEvBasicClientDataSet)
  private
    FTableName: String;
    FParamIndex: Integer;
    FModified: Boolean;
  end;

  TevTransactionKeyRemap = class(TisInterfacedObject, IevTransactionKeyRemap)
  private
    FParams:  IisParamsCollection;
    FKeyMap:  IisValueMap;
    FNewRecByTables: IisListOfValues;
    procedure FirstPhase(const ADataSet: TEvRemapDataSet); overload;
    procedure FirstPhase(const APacket: IevDataChangePacket); overload;
    procedure SecondPhase;
    procedure ThirdPhase(const ADataSet: TEvRemapDataSet); overload;
    procedure ThirdPhase(const APacket: IevDataChangePacket); overload;
    function  Execute: IisValueMap;
  public
    constructor Create(const AMethodParams: IisParamsCollection); reintroduce;
  end;

// todo: REMOVE ASAP!!!
// FOR WELLS TROUBLESHOOTING ONLY!
threadvar
  tvDarkMatterStats: IisStatistics;


  { TevContextCallbacksProxy }

procedure TevContextCallbacksProxy.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.CreateResponseFor(FCallerDatagramHeader);
  D.Header.Method := METHOD_CONTEXT_CALLBACKS_ASYNCCALLRETURN;
  D.Header.SessionID := FSession.SessionID;
  D.Header.ContextID := FCallerDatagramHeader.ContextID;
  D.Params.AddValue('ACallID', ACallID);
  D.Params.AddValue('AResults', AResults);

  FSession.SendDatagram(D);
end;

constructor TevContextCallbacksProxy.Create(const ASession: IevSession; const ACallerDatagramHeader: IevDatagramHeader);
begin
  inherited Create;
  FSession := ASession;
  FCallerDatagramHeader := ACallerDatagramHeader;
  FProgressInfoStack := TisList.Create;
  FLastSendProgressInfo := TevProgressInfo.Create;
end;

procedure TevContextCallbacksProxy.EndWait;
begin
  FProgressInfoStack.Lock;
  try
    if FProgressInfoStack.Count > 0 then
      FProgressInfoStack.Delete(FProgressInfoStack.Count - 1);
  finally
    FProgressInfoStack.Unlock;  
  end;
  SendProgressInf;  
end;

procedure TevContextCallbacksProxy.SendProgressInf;
var
  D: IevDatagram;
  ProgressInfo: IevProgressInfo;
  s: String;
  i: Integer;
begin
  FProgressInfoStack.Lock;
  try
    if FProgressInfoStack.Count > 0 then
    begin
      if SecondsBetween(Now, FProgressLastSendTime) >= 1 then  // Send not often than 1 sec
      begin
        s := '';
        for i := 0 to FProgressInfoStack.Count - 1 do
        begin
          ProgressInfo := FProgressInfoStack[i] as IevProgressInfo;
          if ProgressInfo.ProgressText <> '' then
          begin
            s := s + ProgressInfo.ProgressText;
            if i > 0 then
              s := s + #13;
          end;
        end;
        ProgressInfo := FProgressInfoStack[FProgressInfoStack.Count - 1] as IevProgressInfo;

                                                // and only if progress has changed since last send
        if (FLastSendProgressInfo.ProgressText <> s) or
           (FLastSendProgressInfo.ProgressMax <> ProgressInfo.ProgressMax) or
           (FLastSendProgressInfo.ProgressCurr <> ProgressInfo.ProgressCurr) then
        begin
          D := TevDatagram.CreateResponseFor(FCallerDatagramHeader);
          D.Header.Method := METHOD_CONTEXT_CALLBACKS_UPDATEWAIT;
          D.Header.SessionID := FSession.SessionID;
          D.Header.ContextID := FCallerDatagramHeader.ContextID;
          D.Params.AddValue('AText', s);
          D.Params.AddValue('AMaxProgress', ProgressInfo.ProgressMax);
          D.Params.AddValue('ACurrentProgress', ProgressInfo.ProgressCurr);
          FSession.SendDatagram(D);

          FLastSendProgressInfo.ProgressMax := ProgressInfo.ProgressMax;
          FLastSendProgressInfo.ProgressCurr := ProgressInfo.ProgressCurr;
          FLastSendProgressInfo.ProgressText := ProgressInfo.ProgressText;
          FProgressLastSendTime := Now;
        end;
      end

      else if GetOwner <> nil then
        TevProgressUpdater(GetOwner.GetImplementation).WakeUp;
    end;
  finally
    FProgressInfoStack.Unlock;
  end;
end;

procedure TevContextCallbacksProxy.StartWait(const AText: string; AMaxProgress: Integer);
var
  ProgressInfo: IevProgressInfo;
begin
  ProgressInfo := TevProgressInfo.Create;
  ProgressInfo.ProgressMax := AMaxProgress;
  ProgressInfo.ProgressText := AText;
  FProgressInfoStack.Add(ProgressInfo);
  SendProgressInf;  
end;

procedure TevContextCallbacksProxy.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
var
  ProgressInfo: IevProgressInfo;
begin
  FProgressInfoStack.Lock;
  try
    if FProgressInfoStack.Count > 0 then
    begin
      ProgressInfo := FProgressInfoStack[FProgressInfoStack.Count - 1] as IevProgressInfo;
      ProgressInfo.ProgressText := AText;
      ProgressInfo.ProgressCurr := ACurrentProgress;
      if AMaxProgress <> - 1 then
        ProgressInfo.ProgressMax := AMaxProgress;
    end;
  finally
    FProgressInfoStack.UnLock;
  end;
  SendProgressInf;  
end;

procedure TevContextCallbacksProxy.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
var
  D: IevDatagram;
begin
  D := TevDatagram.CreateResponseFor(FCallerDatagramHeader);
  D.Header.Method := METHOD_CONTEXT_CALLBACKS_USERSCHEDULEREVENT;
  D.Header.SessionID := FSession.SessionID;
  D.Header.ContextID := FCallerDatagramHeader.ContextID;
  D.Params.AddValue('AEventID', AEventID);
  D.Params.AddValue('ASubject', ASubject);
  D.Params.AddValue('AScheduledTime', AScheduledTime);
  FSession.SendDatagram(D);
end;

{ TevCommonDatagramDispatcher }

procedure TevCommonDatagramDispatcher.dmRWRemoteEngineRunReports(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  RepList: TrwReportList;
  RepListStr: IevDualStream;
begin
  RepList := TrwReportList.Create;
  try
    RepListStr := IInterface(ADatagram.Params.Value['AReportList']) as IevDualStream;
    RepListStr.Position := 0;
    RepList.SetFromStream(RepListStr);
    RepListStr := ctx_RWRemoteEngine.RunReports(RepList);
  finally
    FreeAndNil(RepList);
  end;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RepListStr);
end;

procedure TevCommonDatagramDispatcher.dmRWRemoteEngineRunQBQuery(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  AQuery, Res: IevDualStream;
begin
  AQuery := IInterface(ADatagram.Params.Value['AQuery']) as IevDualStream;
  Res := ctx_RWRemoteEngine.RunQBQuery(AQuery,
    ADatagram.Params.Value['AClientNbr'],
    ADatagram.Params.Value['ACompanyNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmRWRemoteEngineSetLowerRWEnginePriority(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_RWRemoteEngine.SetLowerRWEnginePriority(ADatagram.Params.Value['AValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmRWRemoteEngineStopReportExecution(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_RWRemoteEngine.StopReportExecution;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessCreateClientDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CLds : IevDataSet;
  Res: Integer;
begin
  CLds := IInterface(ADatagram.params.Value['ClDataset']) as IevDataSet;
  Res := Context.DBAccess.CreateClientDB(CLds);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessCleanDeletedClientsFromTemp(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.CleanDeletedClientsFromTemp;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessDeleteClientDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.DeleteClientDB(ADatagram.Params.Value['ClientNumber']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetClientsList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin

  Res := Context.DBAccess.GetClientsList(ADatagram.Params.Value['allList'], ADatagram.Params.Value['fromTT']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDataSets(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Params: TGetDataParams;
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['DataSetParams']) as IevDualStream;
  Params := TGetDataParams.Create(S);
  try
    S := Context.DBAccess.GetDataSets(Params);
  finally
    FreeAndNil(Params);
  end;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDBVersion(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := Context.DBAccess.GetDBVersion(TevDBType(ADatagram.Params.Value['ADBType']));
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetGeneratorValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := Context.DBAccess.GetGeneratorValue(
           ADatagram.Params.Value['AGeneratorName'],
           TevDBType(ADatagram.Params.Value['ADBType']),
           ADatagram.Params.Value['AIncrement']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessOpenQuery(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.OpenQuery(ADatagram.Params.Value['AQueryName'],
     IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues,
     IInterface(ADatagram.Params.Value['AMacros']) as IisListOfValues,
     ADatagram.Params.Value['ALoadBlobs']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessRebuildTemporaryTables(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.RebuildTemporaryTables(
    ADatagram.Params.Value['aClientNbr'],
    ADatagram.Params.Value['aAllClientsMode'],
    ADatagram.Params.Value['aTempClientsMode']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessSetGeneratorValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.SetGeneratorValue(
    ADatagram.Params.Value['AGeneratorName'],
    TevDBType(ADatagram.Params.Value['ADBType']),
    ADatagram.Params.Value['AValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessCommitTransactions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Params: IisParamsCollection;
  DBInTran: String;
  ADBTypes: TevDBTypes;
  i: Integer;
  AData: IevDualStream;
  GetDSParams: TGetDataParams;
  KeyMap: IisValueMap;
  KeyRemap: IevTransactionKeyRemap;
begin
  Params := IInterface(ADatagram.Params.Value['Params']) as IisParamsCollection;

  // Generate a real keys first
  KeyRemap := TevTransactionKeyRemap.Create(Params);
  KeyMap := KeyRemap.Execute;
  KeyRemap := nil;

  DBInTran := ADatagram.Params.Value['DBInTran'];
  ADBTypes := [];
  for i := 1 to Length(DBInTran) do
    if DBInTran[i] = CH_DATABASE_SYSTEM then
      Include(ADBTypes, dbtSystem)
    else if DBInTran[i] = CH_DATABASE_CLIENT then
      Include(ADBTypes, dbtClient)
    else if DBInTran[i] = CH_DATABASE_SERVICE_BUREAU then
      Include(ADBTypes, dbtBureau)
    else if DBInTran[i] = CH_DATABASE_TEMP then
      Include(ADBTypes, dbtTemporary);

  // Playback actions recorded on client side
  Context.DBAccess.StartTransaction(ADBTypes, ADatagram.Params.Value['TransactionName']);
  try
    for i := 0 to Params.Count - 1 do
    begin
      if Params.Params[i].Value['Method'] = 'ApplyDataChangePacket' then
        Context.DBAccess.ApplyDataChangePacket(IInterface(Params.Params[i].Value['APacket']) as IevDataChangePacket)

      else if Params.Params[i].Value['Method'] = 'RunInTransaction' then
      begin
        Context.DBAccess.RunInTransaction(Params.Params[i].Value['AMethodName'],
                                          IInterface(Params.Params[i].Value['AParams']) as IisListOfValues);
      end
      else if Params.Params[i].Value['Method'] = 'ExecQuery' then
      begin
        Context.DBAccess.ExecQuery(Params.Params[i].Value['AQueryName'],
           IInterface(Params.Params[i].Value['AParams']) as IisListOfValues,
           IInterface(Params.Params[i].Value['AMacros']) as IisListOfValues);
      end
      else if Params.Params[i].Value['Method'] = 'GetDataSets' then
      begin
        AData := IInterface(Params.Params[i].Value['AData']) as IevDualStream;
        GetDSParams := TGetDataParams.Create(AData);
        try
          Context.DBAccess.GetDataSets(GetDSParams);
        finally
          FreeAndNil(GetDSParams);
        end;
      end
      else if Params.Params[i].Value['Method'] <> '' then
        raise EevException.Create('Unknown CommitTransaction option ' + Params.Params[i].Value['Method']);
    end;

    Context.DBAccess.CommitTransaction;
  except
    Context.DBAccess.RollbackTransaction;
    raise;
  end;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, KeyMap);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerForcedUnlock(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.GlobalFlagsManager.ForcedUnlock(
    ADatagram.Params.Value['AType'],
    ADatagram.Params.Value['ATag']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerGetLockedFlag(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevGlobalFlagInfo;
begin
  Res := Mainboard.GlobalFlagsManager.GetLockedFlag(
    ADatagram.Params.Value['AType'],
    ADatagram.Params.Value['ATag']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerGetLockedFlag2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevGlobalFlagInfo;
begin
  Res := Mainboard.GlobalFlagsManager.GetLockedFlag(
    ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerGetLockedFlagList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  List: IisList;
  Res: IevDualStream;
begin
  List := Mainboard.GlobalFlagsManager.GetLockedFlagList(
    ADatagram.Params.Value['AType']);
  AResult := CreateResponseFor(ADatagram.Header);
  Res := TevDualStreamHolder.Create;
  List.SaveItemsToStream(Res);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerGetSetFlagList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  List: IisList;
  Res: IevDualStream;
begin
  List := Mainboard.GlobalFlagsManager.GetSetFlagList(
    ADatagram.Params.Value['AType']);
  AResult := CreateResponseFor(ADatagram.Header);
  Res := TevDualStreamHolder.Create;
  List.SaveItemsToStream(Res);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerReleaseFlag(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Mainboard.GlobalFlagsManager.ReleaseFlag(
    ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerSetFlag(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Mainboard.GlobalFlagsManager.SetFlag(
    ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerTryLock2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Mainboard.GlobalFlagsManager.TryLock(
    ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerTryLock(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  AFlagInfo: IevGlobalFlagInfo;
begin
  AFlagInfo := IInterface(ADatagram.Params.Value['AFlagInfo']) as IevGlobalFlagInfo;
  Res := Mainboard.GlobalFlagsManager.TryLock(
    ADatagram.Params.Value['AType'],
    ADatagram.Params.Value['ATag'],
    AFlagInfo);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('AFlagInfo', AFlagInfo);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerTryUnlock2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Mainboard.GlobalFlagsManager.TryUnLock(
    ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerTryUnlock(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  AFlagInfo: IevGlobalFlagInfo;
begin
  AFlagInfo := IInterface(ADatagram.Params.Value['AFlagInfo']) as IevGlobalFlagInfo;
  Res := Mainboard.GlobalFlagsManager.TryUnLock(
    ADatagram.Params.Value['AType'],
    ADatagram.Params.Value['ATag'],
    AFlagInfo);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('AFlagInfo', AFlagInfo);
end;

procedure TevCommonDatagramDispatcher.dmTimeSourceGetDateTime(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: TDateTime;
begin
  Res := Now;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcFinalizeTaxPayments(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.FinalizeTaxPayments(S, IInterface(ADAtagram.Params.Value['ACHOptions.ini']) as IisStringList,
                                                      ADatagram.Params.Value['SbTaxPaymentNbr'], ADatagram.Params.Value['PostProcessReportName'],
                                                      ADatagram.Params.Value['DefaultMiscCheckForm']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcPayTaxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := Context.RemoteMiscRoutines.PayTaxes(
    ADatagram.Params.Value['FedTaxLiabilityList'],
    ADatagram.Params.Value['StateTaxLiabilityList'],
    ADatagram.Params.Value['SUITaxLiabilityList'],
    ADatagram.Params.Value['LocalTaxLiabilityList'],
    ADatagram.Params.Value['ForceChecks'],
    ADatagram.Params.Value['EftpReference'],
    ADatagram.Params.Value['SbTaxPaymentNbr'],
    ADatagram.Params.Value['PostProcessReportName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcPreProcessForQuarter(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  V: IisListOfValues;
begin
  V := Context.RemoteMiscRoutines.PreProcessForQuarter(
    ADatagram.Params.Value['CL_NBR'],
    ADatagram.Params.Value['CO_NBR'],
    ADatagram.Params.Value['ForceProcessing'],
    ADatagram.Params.Value['Consolidation'],
    ADatagram.Params.Value['BegDate'],
    ADatagram.Params.Value['EndDate'],
    ADatagram.Params.Value['QecCleanUpLimit'],
    ADatagram.Params.Value['TaxAdjLimit'],
    ADatagram.Params.Value['PrintReports']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, V);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcPrintChecksAndCouponsForTaxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: TrwReportResults;
  S: IevDualStream;
begin
  Res := Context.RemoteMiscRoutines.PrintChecksAndCouponsForTaxes(
    ADatagram.Params.Value['TaxPayrollFilter'],ADatagram.Params.Value['DefaultMiscCheckForm']);
  try
    AResult := CreateResponseFor(ADatagram.Header);
    S := Res.GetAsStream;
  finally
    Res.Free;
  end;
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalculateTaxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.CalculateTaxes(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalculateChecklines(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.CalculateChecklines(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscRefreshEDS(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.RefreshEDS(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCreateCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.CreateCheck(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscTCImport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.TCImport(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetW2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
  sTmp : String;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  sTmp := ADatagram.Params.Value['Param'];
  S := Context.RemoteMiscRoutines.GetW2(S, sTmp);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetEEW2List(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDataSet;
  sTmp : Integer;
begin
  sTmp := ADatagram.Params.Value['pEENBR'];
  S := Context.RemoteMiscRoutines.GetEEW2List(sTmp);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetEEScheduled_E_DS(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDataSet;
  eeList: string;
begin
  eeList := ADatagram.Params.Value['eeList'];
  S := Context.RemoteMiscRoutines.GetEEScheduled_E_DS(eeList);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcEEScheduled_E_DS_Rate(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  rate_type: String;
  rate_value: Variant;
begin
 Context.RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
                            ADatagram.Params.Value['aCO_BENEFIT_SUBTYPE_NBR'],
                            ADatagram.Params.Value['aCL_E_DS_NBR'],
                            ADatagram.Params.Value['aBeginDate'],
                            ADatagram.Params.Value['aEndDate'],
                            rate_type, rate_value);

  AResult := CreateResponseFor(ADatagram.Header);

  AResult.Params.AddValue('aRate_Type', rate_type);
  AResult.Params.AddValue('aValue', rate_value);
end;

procedure TevCommonDatagramDispatcher.dmMiscCalcEEScheduled_E_DS_Rate2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  rate_type: String;
  rate_value: Variant;
begin

 Context.RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
                            ADatagram.Params.Value['aEE_BENEFITS_NBR'],
                            ADatagram.Params.Value['aCL_E_DS_NBR'],
                            ADatagram.Params.Value['aBeginDate'],
                            ADatagram.Params.Value['aEndDate'],
                            ADatagram.Params.Value['aAMOUNT'],
                            ADatagram.Params.Value['aANNUAL_MAXIMUM_AMOUNT'],
                            ADatagram.Params.Value['aTARGET_AMOUNT'],
                            ADatagram.Params.Value['aFREQUENCY'],
                            rate_type, rate_value);

  AResult := CreateResponseFor(ADatagram.Header);

  AResult.Params.AddValue('aRate_Type', rate_type);
  AResult.Params.AddValue('aValue', rate_value);

end;

procedure TevCommonDatagramDispatcher.dmMiscChangeCheckStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.ChangeCheckStatus(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscHolidayDay(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.HolidayDay(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscNextPayrollDates(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.NextPayrollDates(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessCleanupTaxableTips(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.CleanupTaxableTips(
    ADatagram.Params.Value['PeriodBegin'],
    ADatagram.Params.Value['PeriodEnd']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessCreatePR(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := Context.PayrollProcessing.CreatePR(
    ADatagram.Params.Value['Co_Nbr'],
    ADatagram.Params.Value['CheckDate']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessCreatePRBatchDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  TcFileIn, TcFileOut: IevDualStream;
begin
  TcFileIn := IInterface(ADatagram.Params.Value['TcFileIn']) as IevDualStream;
  TcFileOut := IInterface(ADatagram.Params.Value['TcFileOut']) as IevDualStream;

  Context.PayrollProcessing.CreatePRBatchDetails(
    ADatagram.Params.Value['PrBatchNumber'],
    ADatagram.Params.Value['TwoChecksPerEE'],
    ADatagram.Params.Value['CalcCheckLines'],
    ADatagram.Params.Value['CheckType'],
    ADatagram.Params.Value['Check945Type'],
    ADatagram.Params.Value['NumberOfChecks'],
    ADatagram.Params.Value['SalaryOrHourly'],
    ADatagram.Params.Value['EENumbers'],
    TcFileIn,
    TcFileOut,
    TCImportLookupOption(Integer(ADatagram.Params.Value['TcLookupOption'])),
    TCImportDBDTOption(Integer(ADatagram.Params.Value['TcDBDTOption'])),
    ADatagram.Params.Value['TcFourDigitYear'],
    TCFileFormatOption(Integer(ADatagram.Params.Value['TcFileFormat'])),
    ADatagram.Params.Value['TcAutoImportJobCodes'],
    ADatagram.Params.Value['TcUseEmployeePayRates'],
    ADatagram.Params.Value['UpdateBalance'],
    ADatagram.Params.Value['IncludeTimeOffRequests']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessCreateTaxAdjCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.CreateTaxAdjCheck(
    ADatagram.Params.Value['aDate']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDoERTaxAdjPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.DoERTaxAdjPayroll(
    ADatagram.Params.Value['aDate']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDoQtrEndTaxLiabAdj(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.DoQtrEndTaxLiabAdj(
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['aDate'],
    ADatagram.Params.Value['MinLiability']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDoQuarterEndCleanup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  Warnings: String;
begin
  Res := Context.PayrollProcessing.DoQuarterEndCleanup(
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['QTREndDate'],
    ADatagram.Params.Value['MinTax'],
    ADatagram.Params.Value['CleanupAction'],
    ADatagram.Params.Value['ReportFilePath'],
    ADatagram.Params.Value['EECustomNbr'],
    ADatagram.Params.Value['ForceCleanup'],
    ADatagram.Params.Value['PrintReports'],
    ADatagram.Params.Value['CurrentQuarterOnly'],
    TResourceLockType(Integer(ADatagram.Params.Value['LockType'])),
    ADatagram.Params.Value['ExtraOptions'],
    Warnings
    );
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Integer(Res));
  AResult.Params.AddValue('Warnings', Warnings);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessIsQuarterEndCleanupNecessary(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Context.PayrollProcessing.IsQuarterEndCleanupNecessary(
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['QTREndDate']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Integer(Res));
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessLoadYTD(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Variant;
begin
  Res := Context.PayrollProcessing.LoadYTD(
    ADatagram.Params.Value['Cl_Nbr'],
    ADatagram.Params.Value['Co_Nbr'],
    ADatagram.Params.Value['Year'],
    ADatagram.Params.Value['EeNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessGetEEYTD(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Variant;
begin
  Res := Context.PayrollProcessing.GetEEYTD(
    ADatagram.Params.Value['AYear'],
    ADatagram.Params.Value['AEeNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;


procedure TevCommonDatagramDispatcher.dmPayrollProcessProcessBilling(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Variant;
begin
  Res := Context.PayrollProcessing.ProcessBilling(
    ADatagram.Params.Value['prNumber'],
    ADatagram.Params.Value['ForPayroll'],
    ADatagram.Params.Value['InvoiceNumber'],
    ADatagram.Params.Value['ReturnDS']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessProcessBillingForServices(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  bManualACH : Boolean;
  Res: Real;
begin
  bManualACH := ADatagram.Params.Value['bManualACH'];
  Res := Context.PayrollProcessing.ProcessBillingForServices(
    ADatagram.Params.Value['ClNbr'],
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['List'],
    ADatagram.Params.Value['EffDate'],
    ADatagram.Params.Value['Notes'],
    bManualACH);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('bManualACH', bManualACH);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessProcessPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Variant;
  Log: String;
begin
  // ticket 61143
  Log := '';//ADatagram.Params.Value['Log'];
  Res := Context.PayrollProcessing.ProcessPayroll(
    ADatagram.Params.Value['PayrollNumber'],
    ADatagram.Params.Value['JustPreProcess'],
    Log,
    ADatagram.Params.Value['CheckReturnQueue'],
    TResourceLockType(Integer(ADatagram.Params.Value['LockType'])),
    ADatagram.Params.Value['PrBatchNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('Log', Log);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessRecreateVmrHistory(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.ReCreateVmrHistory(
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['PeriodBeginDate'],
    ADatagram.Params.Value['PeriodEndDate']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessReprintPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDualStream;
  AWarnings: String;
  AErrors: String;
begin
  AWarnings := ADatagram.Params.Value['AWarnings'];
  AErrors := ADatagram.Params.Value['AErrors'];
  Res := Context.PayrollProcessing.ReprintPayroll(
    ADatagram.Params.Value['PrNbr'],
    ADatagram.Params.Value['PrintReports'],
    ADatagram.Params.Value['PrintInvoices'],
    ADatagram.Params.Value['CurrentTOA'],
    ADatagram.Params.Value['PayrollChecks'],
    ADatagram.Params.Value['AgencyChecks'],
    ADatagram.Params.Value['TaxChecks'],
    ADatagram.Params.Value['BillingChecks'],
    ADatagram.Params.Value['CheckForm'],
    ADatagram.Params.Value['MiscCheckForm'],
    ADatagram.Params.Value['CheckDate'],
    ADatagram.Params.Value['SkipCheckStubCheck'],
    ADatagram.Params.Value['DontPrintBankInfo'],
    ADatagram.Params.Value['HideBackground'],
    ADatagram.Params.Value['DontUseVMRSettings'],
    ADatagram.Params.Value['ForcePrintVoucher'],
    ADatagram.Params.Value['PrReprintHistoryNBR'],
    ADatagram.Params.Value['Reason'],
    AWarnings,
    AErrors);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AWarnings', AWarnings);
  AResult.Params.AddValue('AErrors', AErrors);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessReRunPayrolReportsForVmr(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.PayrollProcessing.ReRunPayrolReportsForVmr(
    ADatagram.Params.Value['PrNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPrCheckPrintingProxyPrintChecks(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CheckNumbers: TStringList;
begin
  CheckNumbers := TStringList.Create;
  try
    CheckNumbers.Text := ADatagram.Params.Value['CheckNumbers.Text'];

    Context.PayrollCheckPrint.PrintChecks(
      ADatagram.Params.Value['PayrollNbr'],
      CheckNumbers,
      TCheckType(Integer(ADatagram.Params.Value['CheckType'])),
      ADatagram.Params.Value['CurrentTOA'],
      ADatagram.Params.Value['PrintReports'],
      ADatagram.Params.Value['CheckForm'],
      ADatagram.Params.Value['Preview'],
      ADatagram.Params.Value['CheckDate'],
      ADatagram.Params.Value['SkipCheckStubCheck'],
      ADatagram.Params.Value['DontPrintBankInfo'],
      ADatagram.Params.Value['HideBackground']
      );
    AResult := CreateResponseFor(ADatagram.Header);
    AResult.Params.AddValue('CheckNumbers.Text', CheckNumbers.Text);

  finally
    CheckNumbers.Free;
  end;
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteCheckScannedBarcode(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  ExpectedBarCode, LastScannedBarCode: String;
begin
  ExpectedBarCode := ADatagram.Params.Value['ExpectedBarCode'];
  LastScannedBarCode := ADatagram.Params.Value['LastScannedBarCode'];
  Res := Context.VmrRemote.CheckScannedBarcode(
    ADatagram.Params.Value['BarCode'],
    ExpectedBarCode,
    LastScannedBarCode);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('ExpectedBarCode', ExpectedBarCode);
  AResult.Params.AddValue('LastScannedBarCode', LastScannedBarCode);
  AResult.Params.AddValue(METHOD_RESULT, Integer(Res));
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteDeleteFile(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.DeleteFile(
    ADatagram.Params.Value['FileName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteFileDoesExist(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Context.VmrRemote.FileDoesExist(
    ADatagram.Params.Value['FileName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Integer(Res));
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteFindInBoxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := Context.VmrRemote.FindInBoxes(
    ADatagram.Params.Value['BarCode']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteGetPrinterList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevVMRPrintersList;
begin
  Res := Context.VmrRemote.GetPrinterList(ADatagram.Params.Value['ALocation']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteGetSBPrinterList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevVMRPrintersList;
begin
  Res := Context.VmrRemote.GetSBPrinterList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemotePrintRwResults(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
  ARepResults: TrwReportResults;
begin
  ARepResults := TrwReportResults.Create;
  try
    S := IInterface(ADatagram.Params.Value['ARepResults']) as IevDualStream;
    ARepResults.SetFromStream(S);
    Context.VmrRemote.PrintRwResults(ARepResults);
  finally
    ARepResults.Free;
  end;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemotePutInBoxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := Context.VmrRemote.PutInBoxes(
    ADatagram.Params.Value['BarCode'],
    ADatagram.Params.Value['ToBarCode']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteRemoveFromBoxes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.RemoveFromBoxes(
    ADatagram.Params.Value['BarCode']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteRemoveScannedBarcode(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.RemoveScannedBarCode(
    ADatagram.Params.Value['BarCode']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteRemoveScannedBarcodes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.RemoveScannedBarCodes(
    ADatagram.Params.Value['MailBoxBarCode']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteRetrieveFile(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDualStream;
begin
  Res := Context.VmrRemote.RetrieveFile(
    ADatagram.Params.Value['FileName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteStoreFile(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
  Content: IevDualStream;
begin
  Content := IInterface(ADatagram.Params.Value['Content']) as IevDualStream;
  Res := Context.VmrRemote.StoreFile(
    ADatagram.Params.Value['FileName'],
    Content,
    ADatagram.Params.Value['AutoRename']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityChangePassword(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.ChangePassword(ADatagram.Params.Value['ANewPassword']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecurityCreateEEAccount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.CreateEEAccount(ADatagram.Params.Value['AUserName'],
                               ADatagram.Params.Value['APassword'],
                               ADatagram.Params.Value['ACompanyNumber'],
                               ADatagram.Params.Value['ASSN'],
                               ADatagram.Params.Value['ACheckNumber'],
                               ADatagram.Params.Value['ACheckTotalEarnings']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecurityEnableSystemAccountRights(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.EnableSystemAccountRights;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('SystemAccountRights', ctx_Security.AccountRights);

end;

procedure TevCommonDatagramDispatcher.dmSecurityGetAuthorization(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Context.UserAccount);
  AResult.Params.AddValue('AccountRights', ctx_Security.AccountRights);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetGroupSecStructure(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevSecurityStructure;
begin
  S := ctx_Security.GetGroupSecStructure(ADatagram.Params.Value['ASecGroupInternalNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetSecStructure(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ctx_Security.GetSecStructure);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetUserSecStructure(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevSecurityStructure;
begin
  S := ctx_Security.GetUserSecStructure(ADatagram.Params.Value['AUserInternalNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;


procedure TevCommonDatagramDispatcher.dmSecuritySetGroupSecStructure(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevSecurityStructure;
begin
  S := IInterface(ADatagram.Params.Value['ASecStruct']) as IevSecurityStructure;
  ctx_Security.SetGroupSecStructure(ADatagram.Params.Value['ASecGroupInternalNbr'], S);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecuritySetUserSecStructure(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevSecurityStructure;
begin
  S := IInterface(ADatagram.Params.Value['ASecStruct']) as IevSecurityStructure;
  ctx_Security.SetUserSecStructure(ADatagram.Params.Value['AUserInternalNbr'], S);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmLicenseGetLicenseInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.License.GetLicenseInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueAddTask(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  T: IevTask;
begin
  T := IInterface(ADatagram.Params.Value['ATask']) as IevTask;
  mb_TaskQueue.AddTask(T);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueCreateTask(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  T: IevTask;
begin
  T := Mainboard.TaskQueue.CreateTask(ADatagram.Params.Value['ATaskType'], ADatagram.Params.Value['ApplyDefaults']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, T);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueGetTask(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  T: IevTask;
begin
  T := Mainboard.TaskQueue.GetTask(ADatagram.Params.Value['AID']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, T);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueGetTaskInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  T: IevTaskInfo;
begin
  T := Mainboard.TaskQueue.GetTaskInfo(ADatagram.Params.Value['AID']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, T);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueGetTaskRequests(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := Mainboard.TaskQueue.GetTaskRequests(ADatagram.Params.Value['AID']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueGetUserTaskInfoList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := Mainboard.TaskQueue.GetUserTaskInfoList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueGetUserTaskStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := Mainboard.TaskQueue.GetUserTaskStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueModifyTaskProperty(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.TaskQueue.ModifyTaskProperty(ADatagram.Params.Value['AID'],
    ADatagram.Params.Value['APropName'], ADatagram.Params.Value['ANewValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueRemoveTask(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.TaskQueue.RemoveTask(ADatagram.Params.Value['AID']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

function TevCommonDatagramDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
begin
  Mainboard.ContextManager.CreateThreadContext(ADatagram.Header.User, ADatagram.Header.Password,
    '', ADatagram.Header.ContextID); // raises an exception if login is invalid
  Mainboard.ContextManager.DestroyThreadContext;  
  Result := True;
end;

procedure TevCommonDatagramDispatcher.dmAsyncFunctionsMakeCall(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  CallID: TisGUID;
begin
  CallID := ADatagram.Params.Value['ACallID'];

  Res := Mainboard.AsyncFunctions.MakeCall(
    ADatagram.Params.Value['AFunctionName'],
    IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues,
    ADatagram.Params.Value['APriority'],
    CallID);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('ACallID', CallID);
end;

procedure TevCommonDatagramDispatcher.dmAsyncFunctionsSetCallPriority(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := Mainboard.AsyncFunctions.SetCallPriority(
    ADatagram.Params.Value['ACallID'], ADatagram.Params.Value['ANewPriority']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmAsyncFunctionsTerminateCall(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.AsyncFunctions.TerminateCall(ADatagram.Params.Value['ACallID']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.DispatchDatagram(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CollectStat: Boolean;
  DMStat: IisStatistics;
begin
  DMStat := tvDarkMatterStats;
  tvDarkMatterStats := nil;

  CollectStat := ADatagram.Params.TryGetValue(DM_PARAM_STATISTICS, False);
  CollectStat := CollectStat and (Context <> nil) and not ctx_Statistics.Enabled;

  if (Context <> nil) and CollectStat then
  begin
    ctx_Statistics.Enabled := True;
    ctx_Statistics.BeginEvent(AppInfoByID(AppID).Abbr);
    ctx_Statistics.ActiveEvent.Properties.AddValue('Host', Mainboard.MachineInfo.Name);

    if Assigned(DMStat) then
      ctx_Statistics.ActiveEvent.AddEvent(DMStat.TopEvent);

    ctx_Statistics.BeginEvent('Dispatch');
    ctx_Statistics.ActiveEvent.Properties.AddValue('Method', ADatagram.Header.Method);
  end
  else
    CollectStat := False;

  try
    if Context <> nil then
    begin
      if ADatagram.Params.ValueExists('ADRContext') then
        ctx_DBAccess.SetADRContext(ADatagram.Params.Value['ADRContext']);

      if ADatagram.Params.ValueExists('ClientID') then
        ctx_DBAccess.CurrentClientNbr := ADatagram.Params.Value['ClientID'];
    end;

    inherited;
  finally
    if (Context <> nil) and CollectStat then
    begin
      ctx_Statistics.EndEvent;
      ctx_Statistics.EndEvent;
      if Assigned(AResult) then
        AResult.Params.AddValue(DM_PARAM_STATISTICS, ctx_Statistics.TopEvent);
    end;
  end;
end;

procedure TevCommonDatagramDispatcher.dmContextCallbacksUpdateWait(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Context <> nil then
    ctx_UpdateWait(ADatagram.Params.Value['AText'], ADatagram.Params.Value['ACurrentProgress'],
                   ADatagram.Params.Value['AMaxProgress']);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksTaskQueueEvent(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.TaskQueueEvent(
      ADatagram.Params.Value['AUserAtDomain'],
      TevTaskQueueEvent(Integer(ADatagram.Params.Value['AEvent'])),
      ADatagram.Params.Value['ATaskID']);
end;

procedure TevCommonDatagramDispatcher.dmContextCallbacksAsyncCallReturn(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  AResults: IisListOfValues;
begin
  if (Context <> nil) and (Context.Callbacks <> nil) then
  begin
    AResults := IInterface(ADatagram.Params.Value['AResults']) as IisListOfValues;
    Context.Callbacks.AsyncCallReturn(ADatagram.Params.Value['ACallID'], AResults);
  end;
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksGlobalFlagChange(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  AFlagInfo: IevGlobalFlagInfo;
  AOperation: Char;
begin
  AFlagInfo := IInterface(ADatagram.Params.Value['AFlagInfo']) as IevGlobalFlagInfo;
  AOperation := Chr(Byte(ADatagram.Params.Value['AOperation']));

  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.GlobalFlagChange(AFlagInfo, AOperation);
end;


procedure TevCommonDatagramDispatcher.dmGlobalSettingsGet(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Settings: IevSettings;
begin
  AResult := CreateResponseFor(ADatagram.Header);

  Settings := (mb_GlobalSettings as IisInterfacedObject).GetClone as IevSettings;

  AResult.Params.AddValue('EnvironmentID', Settings.EnvironmentID);
  AResult.Params.AddValue('DBConPoolSize', Settings.DBConPoolSize);
  AResult.Params.AddValue('DBAdminPassword', Settings.DBAdminPassword);
  AResult.Params.AddValue('DBUserPassword', Settings.DBUserPassword);    
  AResult.Params.AddValue('DomainInfoList', Settings.DomainInfoList);
  AResult.Params.AddValue('EMailInfo', Settings.EMailInfo);
end;

procedure TevCommonDatagramDispatcher.AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram);
begin
  inherited;
  if Assigned(FProgressUpdater) and (Context <> nil) and (Context.Callbacks <> nil) and
     Supports(Context.Callbacks, IevContextCallbacksProxy) then
    FProgressUpdater.RemoveChild(Context.Callbacks as IisInterfacedObject);
  Mainboard.ContextManager.RestoreThreadContext;
end;

procedure TevCommonDatagramDispatcher.BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader);
var
  CB: IevContextCallbacks;
  Session: IevSession;
  sUser: String;
  DMStat: IisStatistics;
begin
  inherited;
  DMStat := nil;
  Mainboard.ContextManager.StoreThreadContext;

  if StartsWith(ADatagramHeader.Method, METHOD_CONTEXT_CALLBACKS) then
    Mainboard.ContextManager.SetThreadContext(Mainboard.ContextManager.FindContextByID(ADatagramHeader.ContextID))

  else
  begin
    DMStat := TisStatistics.Create;

    DMStat.BeginEvent('Before Dispatch');

    DMStat.BeginEvent('Find Session');
    Session := FindSessionByID(ADatagramHeader.SessionID);
    DMStat.EndEvent;

    if Session.State = ssActive then
    begin
      sUser := ADatagramHeader.User;
      if sUser = '' then
        sUser := EncodeUserAtDomain(sGuestUserName, sDefaultDomain);

      DMStat.BeginEvent('Create Context');
      Mainboard.ContextManager.CreateThreadContext(sUser, ADatagramHeader.Password,
        '', ADatagramHeader.ContextID);
      DMStat.EndEvent;

      CB := TevContextCallbacksProxy.Create(Session, ADatagramHeader);
      Context.SetCallbacks(CB);

      if Assigned(FProgressUpdater) then
        FProgressUpdater.AddChild(Context.Callbacks as IisInterfacedObject);
    end;

    DMStat.EndEvent;
  end;

  tvDarkMatterStats := DMStat;
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessProcessPrenoteACH(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ACHFile: IevDualStream;
  ACHRegularReport: IevDualStream;
  ACHDetailedReport: IevDualStream;
  ACHSave: IisListOfValues;
  AExceptions: String;
  AchFileName: String;
begin

  Context.PayrollProcessing.ProcessPrenoteACH(
    IInterface(ADatagram.Params.Value['ACHOptions']) as IevACHOptions,
    ADatagram.Params.Value['APrenoteCLAccounts'],
    IInterface(ADatagram.Params.Value['ACHDataStream']) as IevDualStream,
    ACHFile,
    ACHRegularReport,
    ACHDetailedReport,
    ACHSave,
    AchFileName,
    AExceptions,
    ADatagram.Params.Value['ACHFolder']);

  AResult := CreateResponseFor(ADatagram.Header);

  AResult.Params.AddValue('ACHFile', ACHFile);
  AResult.Params.AddValue('ACHRegularReport', ACHRegularReport);
  AResult.Params.AddValue('ACHDetailedReport', ACHDetailedReport);
  AResult.Params.AddValue('ACHSave', ACHSave);
  AResult.Params.AddValue('AchFileName', AchFileName);
  AResult.Params.AddValue('AExceptions', AExceptions);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessProcessManualACH(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ACHFile: IevDualStream;
  ACHRegularReport: IevDualStream;
  ACHDetailedReport: IevDualStream;
  ACHSave: IisListOfValues;
  AExceptions: String;
  AchFileName: String;
begin

  Context.PayrollProcessing.ProcessManualACH(
    IInterface(ADatagram.Params.Value['ACHOptions']) as IevACHOptions,
    ADatagram.Params.Value['bPreprocess'],
    ADatagram.Params.Value['BatchBANK_REGISTER_TYPE'],
    IInterface(ADatagram.Params.Value['ACHDataStream']) as IevDualStream,
    ACHFile,
    ACHRegularReport,
    ACHDetailedReport,
    ACHSave,
    AchFileName,
    AExceptions,
    ADatagram.Params.Value['ACHFolder']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('ACHFile', ACHFile);
  AResult.Params.AddValue('ACHRegularReport', ACHRegularReport);
  AResult.Params.AddValue('ACHDetailedReport', ACHDetailedReport);
  AResult.Params.AddValue('ACHSave', ACHSave);
  AResult.Params.AddValue('AchFileName', AchFileName);
  AResult.Params.AddValue('AExceptions', AExceptions);
  
end;

procedure TevCommonDatagramDispatcher.dmVersionUpdateRemoteCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  UpdateInfo: IisParamsCollection;
  Res: Boolean;
begin
  Res := Mainboard.VersionUpdateRemote.Check(IInterface(ADatagram.Params.Value['AppFileInfo']) as IisParamsCollection,
                                       UpdateInfo);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('UpdateInfo', UpdateInfo);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVersionUpdateRemoteGetUpdatePack(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := Mainboard.VersionUpdateRemote.GetUpdatePack(IInterface(ADatagram.Params.Value['AppFileInfo']) as IisParamsCollection);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteRetrievePickupSheetsXMLData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IEvDualStream;
begin
  Res := Context.VmrRemote.RetrievePickupSheetsXMLData;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;


procedure TevCommonDatagramDispatcher.dmPayrollProcessCopyPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.CopyPayroll(ADatagram.Params.Value['APayrollNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDeletePayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.DeletePayroll(ADatagram.Params.Value['APayrollNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDeleteBatch(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  b: Boolean;
begin
  b:= ctx_PayrollProcessing.DeleteBatch(ADatagram.Params.Value['ABatchNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, b);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessVoidPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.VoidPayroll(ADatagram.Params.Value['APayrollNbr'],
      ADatagram.Params.Value['VoidTaxChecks'],
      ADatagram.Params.Value['VoidAgencyChecks'],
      ADatagram.Params.Value['VoidBillingChecks'],
      ADatagram.Params.Value['PrintReports'],
      ADatagram.Params.Value['DeleteInvoice'],
      ADatagram.Params.Value['BlockBilling']
    );
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksSecurityChange(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.SecurityChange(ADatagram.Params.Value['AChangedItem']);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksGlobalSettingsChange(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.GlobalSettingsChange(ADatagram.Params.Value['aChangedItem']);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksTaskSchedulerChange(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.TaskSchedulerChange(ADatagram.Params.Value['AChangedItem']);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessExecStoredProc(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.DBAccess.ExecStoredProc(ADatagram.Params.Value['AProcName'],
     IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmVersionUpdateRemoteGetVersionInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.VersionUpdateRemote.GetVersionInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalFlagManagerForcedUnlock2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.GlobalFlagsManager.ForcedUnlock(ADatagram.Params.Value['AFlagName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.ActivateProgressUpdater;
begin
  FProgressUpdater := TevProgressUpdater.Create;
end;

procedure TevCommonDatagramDispatcher.DeactivateProgressUpdater;
begin
  FProgressUpdater := nil;
end;

procedure TevCommonDatagramDispatcher.dmMiscReportToPDF(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.ReportToPdf(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscAllReportsToPDF(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
  R: IevDataset;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  R := Context.RemoteMiscRoutines.AllReportsToPdf(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, R);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetPDFReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := Context.RemoteMiscRoutines.GetPDFReport(ADatagram.Params.Value['aTaskId']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetAllPDFReports(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDataSet;
begin
  S := Context.RemoteMiscRoutines.GetAllPDFReports(ADatagram.Params.Value['aTaskId']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;


procedure TevCommonDatagramDispatcher.dmMiscRunReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  s: String;
  param: IisListOfValues;
begin
  param := IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues;
  s := Context.RemoteMiscRoutines.RunReport(ADatagram.Params.Value['aReportNbr'], param );
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, s);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetTaskList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IEvDataSet;
begin
  S := Context.RemoteMiscRoutines.GetTaskList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscSaveAPIBilling(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := IInterface(ADatagram.Params.Value['Params']) as IisListOfValues;
  Context.RemoteMiscRoutines.SaveAPIBilling(L);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessExecQuery(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.ExecQuery(ADatagram.Params.Value['AQueryName'],
     IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues,
     IInterface(ADatagram.Params.Value['AMacros']) as IisListOfValues);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksPopupMessage(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.PopupMessage(ADatagram.Params.Value['aText'], ADatagram.Params.Value['aFromUser'],
      ADatagram.Params.Value['aToUser']);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDisabledDBs(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisStringList;
begin
  L := ctx_DBAccess.GetDisabledDBs;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);  
end;

procedure TevCommonDatagramDispatcher.dmDBAccessBackupDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := ctx_DBAccess.BackupDB(ADatagram.Params.Value['ADBName'], ADatagram.Params.Value['AMetadataOnly'], ADatagram.Params.Value['AScramble'], ADatagram.Params.Value['ACompression']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessRestoreDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.RestoreDB(ADatagram.Params.Value['ADBName'], IInterface(ADatagram.Params.Value['AData']) as IevDualStream, ADatagram.Params.Value['ACompressed']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetLastTransactionNbr(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := Context.DBAccess.GetLastTransactionNbr(TevDBType(ADatagram.Params.Value['ADBType']));
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetClientROFlag(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  C: Char;
begin
  C := Context.DBAccess.GetClientROFlag;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, C);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessSetClientROFlag(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: String;
begin
  S := ADatagram.Params.Value['AValue'];
  Context.DBAccess.SetClientROFlag(S[1]);
  AResult := CreateResponseFor(ADatagram.Header);
end;


procedure TevCommonDatagramDispatcher.dmSecurityGetUserGroupLevelRights(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevSecAccountRights;
begin
  S := ctx_Security.GetUserGroupLevelRights(ADatagram.Params.Value['AUserInternalNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmVMRProcessRelease(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  function StringToReleaseTypeSet(s:string):TVmrReleaseTypeSet;
  var t:string;
  begin
    result := [];
    while s<>'' do
    begin
       t := Fetch(s,';');
       Result := Result + [TVmrReleaseType(StrToInt(t))];
    end;
  end;

var
  UnprintedReports: TrwReportResults;
  S: IisStream;
begin
  Context.VmrRemote.ProcessRelease(ADatagram.Params.Value['TopBoxNbr'],
                                   StringToReleaseTypeSet(ADatagram.Params.Value['ReleaseType']),
                                   UnprintedReports);
  AResult := CreateResponseFor(ADatagram.Header);

  if Assigned(UnprintedReports) then
  begin
    S := UnprintedReports.GetAsStream;
    UnprintedReports.Free;
  end
  else
    S := nil;

  AResult.Params.AddValue('UnprintedReports', S);
end;

procedure TevCommonDatagramDispatcher.dmVMRProcessRevert(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.ProcessRevert( ADatagram.Params.Value['TopBoxNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksSubscribe(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.Subscribe(
      ADatagram.Header.SessionID,
      TevCBEventType(ADatagram.Params.Value['AEventType']),
      ADatagram.Params.Value['AItemID']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksUnsubscribe(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.Unsubscribe(
      ADatagram.Header.SessionID,
      TevCBEventType(ADatagram.Params.Value['AEventType']),
      ADatagram.Params.Value['AItemID']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetAPIBilling(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  DataSet : IevDataSet;
begin
  DataSet := Context.RemoteMiscRoutines.GetAPIBilling;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, DataSet);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessPrintPayroll(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDualStream;
begin
  Res := Context.PayrollProcessing.PrintPayroll(
    ADatagram.Params.Value['PrNbr'],
    ADatagram.Params.Value['PrintChecks'],
    ADatagram.Params.Value['UseSbMiskCheckForm']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmAsyncFunctionsSetCalcCapacity(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.AsyncFunctions.SetCalcCapacity(ADatagram.Params.Value['AValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmTaskQueueAddTasks(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IEvDualStream;
  L: IisList;
begin
  S := IInterface(ADatagram.Params.Value['ATaskList']) as IEvDualStream;
  S.Position := 0;
  L := TisList.Create;
  L.LoadItemsFromStream(S);
  mb_TaskQueue.AddTasks(L);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmEMailerSendQuickMail(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if ADatagram.Params.ValueExists('Host') then
  begin
    ctx_EMailer.SmtpServer := ADatagram.Params.Value['Host'];
    ctx_EMailer.SmtpPort := ADatagram.Params.Value['Port'];
    ctx_EMailer.UserName := ADatagram.Params.Value['User'];
    ctx_EMailer.Password := ADatagram.Params.Value['Password'];
  end;

  ctx_EMailer.SendQuickMail(
    ADatagram.Params.Value['ATo'],
    ADatagram.Params.Value['AFrom'],
    ADatagram.Params.Value['ASubject'],
    ADatagram.Params.Value['ABody']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmEvoXEngineCheckMapFile(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  List: IisListOfValues;
  MapFile: IEvExchangeMapFile;
  Package: IEvExchangePackage;
  Options: IisStringListRO;
begin
  Package := IInterface(ADatagram.Params.Value['Package']) as IEvExchangePackage;
  MapFile := IInterface(ADatagram.Params.Value['MapFile']) as IEvExchangeMapFile;
  Options := IInterface(ADatagram.Params.Value['Options']) as IisStringListRO;

  List := Context.EvolutionExchangeEngine.CheckMapFile(MapFile, Package, Options);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, List);
end;

procedure TevCommonDatagramDispatcher.dmEvoXEngineImport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  List: IisListOfValues;
  InputData: IevDataSet;
  MapFile: IEvExchangeMapFile;
  Package: IEvExchangePackage;
  Options: IisStringListRO;
  CustomCoNbr: String;
  EffectiveDate: TDateTime;
begin
  InputData := IInterface(ADatagram.Params.Value['InputData']) as IevDataSet;
  MapFile := IInterface(ADatagram.Params.Value['MapFile']) as IEvExchangeMapFile;
  Package := IInterface(ADatagram.Params.Value['Package']) as IEvExchangePackage;
  Options := IInterface(ADatagram.Params.Value['Options']) as IisStringListRO;
  CustomCoNbr := ADatagram.Params.Value['CustomCoNbr'];
  EffectiveDate := ADatagram.Params.Value['EffectiveDate'];

  List := Context.EvolutionExchangeEngine.Import(InputData, MapFile, Package, Options, CustomCoNbr, EffectiveDate);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, List);
end;

procedure TevCommonDatagramDispatcher.dmEMailerSendMail(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Msg: IevMailMessage;
begin
  Msg := IInterface(ADatagram.Params.Value['AMessage']) as IevMailMessage;

  if ADatagram.Params.ValueExists('Host') then
  begin
    ctx_EMailer.SmtpServer := ADatagram.Params.Value['Host'];
    ctx_EMailer.SmtpPort := ADatagram.Params.Value['Port'];
    ctx_EMailer.UserName := ADatagram.Params.Value['User'];
    ctx_EMailer.Password := ADatagram.Params.Value['Password'];
  end;
  ctx_EMailer.SendMail(Msg);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.BeforeSendMethodResult(
  const ASession: IevSession; const AMethod, AResult: IevDatagram);
var
  V: IisNamedValue;
  Stat: IisStatEvent;
  Correction: Integer;
begin
  inherited;

  V := AResult.Params.FindValue(DM_PARAM_STATISTICS);
  if Assigned(V) then
  begin
    // Make time correction to reflect transmission time and datagram dispatcher overhead
    Stat := IInterface(V.Value) as IisStatEvent;
    Stat.Properties.AddValue('Receive time', AMethod.TransmissionTime);
    Correction  := CurrentTaskDuration - Stat.Duration + AMethod.TransmissionTime;
    Stat.AddCorrection(Correction);
    Stat.ChangeBeginTime(CurrentTaskStartTime);
  end;
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetGroupRights(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Rights: IevSecAccountRights;
begin
  Rights := ctx_Security.GetGroupRights(ADatagram.Params.Value['ASecGroupInternalNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Rights);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetUserRights(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Rights: IevSecAccountRights;
begin
  Rights := ctx_Security.GetUserRights(ADatagram.Params.Value['AUserInternalNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Rights);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetSbEMail(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iCoNbr : Integer;
  S: String;
begin
  iCoNbr := ADatagram.Params.Value['CoNbr'];
  S := Context.RemoteMiscRoutines.GetSBEmail(iCoNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetTOARequestsForMgr(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  MgrEENbr: Integer;
  ResultsList: IisInterfaceList;
begin
  MgrEENbr := ADatagram.Params.Value['MgrEENbr'];
  ResultsList := Context.RemoteMiscRoutines.GetTOARequestsForMgr(MgrEENbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ResultsList);
end;

procedure TevCommonDatagramDispatcher.dmMiscAddTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Requests : IisInterfaceList;
begin
  Requests := IInterface(ADatagram.Params.Value['Requests']) as IisInterfaceList;
  Context.RemoteMiscRoutines.AddTOARequests(Requests);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetTOARequestsForEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  EENbr: Integer;
  ResultsList: IisInterfaceList;
begin
  EENbr := ADatagram.Params.Value['EENbr'];
  ResultsList := Context.RemoteMiscRoutines.GetTOARequestsForEE(EENbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ResultsList);
end;

procedure TevCommonDatagramDispatcher.dmMiscProcessTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Operation: TevTOAOperation;
  Notes: String;
  EETimeOffAccrualOperNbrs: IisStringList;
  CheckMgr : boolean;
begin
  Operation := TevTOAOperation(Integer(ADatagram.Params.Value['Operation']));
  Notes := ADatagram.Params.Value['Notes'];
  EETimeOffAccrualOperNbrs := IInterface(ADatagram.Params.Value['EETimeOffAccrualOperNbrs']) as IisStringList;
  CheckMgr :=  ADatagram.Params.Value['CheckMgr'];
  Context.RemoteMiscRoutines.ProcessTOARequests(Operation, Notes, EETimeOffAccrualOperNbrs, CheckMgr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscUpdateTOARequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Requests : IisInterfaceList;
begin
  Requests := IInterface(ADatagram.Params.Value['Requests']) as IisInterfaceList;
  Context.RemoteMiscRoutines.UpdateTOARequests(Requests);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetWCReportList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CoNbr: integer;
  ReportNbrs: IisStringList;
  ResultDataSet: IevDataset;
begin
  CoNbr := ADatagram.Params.Value['CoNbr'];
  ReportNbrs := IInterface(ADatagram.Params.Value['ReportNbrs']) as IisStringList;
  ResultDataset := Context.RemoteMiscRoutines.GetWCReportList(CoNbr, ReportNbrs);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ResultDataset);
end;

procedure TevCommonDatagramDispatcher.dmVMRGetVMRReportList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ds: IevDataSet;
begin
  ds := Context.VmrRemote.GetVMRReportList(
        ADatagram.Params.Value['pCLNbr'],
        ADatagram.Params.Value['pCONbr'],
        ADatagram.Params.Value['pCheckFromDate'],
        ADatagram.Params.Value['pCheckToDate'],
        ADatagram.Params.Value['pVMRReportType']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ds);
end;

procedure TevCommonDatagramDispatcher.dmVMRGetVMRReport(
   const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  aSelectedReportNbrs : IisStringList;
  S : IevDualStream;
  ISPrint : boolean;
  sComment : string;
begin
  aSelectedReportNbrs := IInterface(ADatagram.Params.Value['SelectedReportNbrs']) as IisStringList;
  ISPrint := ADatagram.Params.Value['IsPrint'];
  sComment := ADatagram.Params.Value['sComment'];
  S := Context.VmrRemote.GetVMRReport(aSelectedReportNbrs, ISPrint, sComment);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmVMRDeleteDirectory(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.VmrRemote.DeleteDirectory(
    ADatagram.Params.Value['DirName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRPurgeMailBox(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iBoxNbr: integer;
  aForceDelete: Boolean;
begin
  iBoxNbr := ADatagram.Params.Value['BoxNbr'];
  aForceDelete := ADatagram.Params.Value['ForceDelete'];
  Context.VmrRemote.PurgeMailBox(iBoxNbr, aForceDelete);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRPurgeClientMailBox(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iClientNbr: integer;
  aForceDelete: Boolean;
begin
  iClientNbr := ADatagram.Params.Value['ClientNbr'];
  aForceDelete := ADatagram.Params.Value['ForceDelete'];
  Context.VmrRemote.PurgeClientMailBox(iClientNbr, aForceDelete);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmVMRPurgeMailBoxContentList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  SBMailBoxContentNbrs : IisStringList;
begin
  SBMailBoxContentNbrs := IInterface(ADatagram.Params.Value['SBMailBoxContentNbrs']) as IisStringList;
  Context.VmrRemote.PurgeMailBoxContentList(SBMailBoxContentNbrs);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetPayrollTaxTotals(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Totals: Variant;
begin
  Context.RemoteMiscRoutines.GetPayrollTaxTotals(
    ADatagram.Params.Value['PrNbr'],
    Totals);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('Totals', Totals);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetPayrollEDTotals(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Totals: Variant;
  Checks: Integer;
  Hours, GrossWages, NetWages: Currency;
begin
  Context.RemoteMiscRoutines.GetPayrollEDTotals(
    ADatagram.Params.Value['PrNbr'],
    Totals, Checks, Hours, GrossWages, NetWages);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('Totals', Totals);
  AResult.Params.AddValue('Checks', Checks);
  AResult.Params.AddValue('Hours', Hours);
  AResult.Params.AddValue('GrossWages', GrossWages);
  AResult.Params.AddValue('NetWages', NetWages);
end;

procedure TevCommonDatagramDispatcher.dmVMRRemoteSetPrinterList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ALocation : string;
  IsDelete: Boolean;
begin
  ALocation := ADatagram.Params.Value['ALocation'];
  IsDelete := ADatagram.Params.Value['IsDelete'];
  Context.VmrRemote.SetPrinterList(ALocation, IInterface(ADatagram.Params.Value['APrinterList']) as IevVMRPrintersList, IsDelete);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscAddTOAForNewEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CoNbr, EENbr: integer;
begin
  CoNbr := ADatagram.Params.Value['CoNbr'];
  EENbr := ADatagram.Params.Value['EENbr'];
  Context.RemoteMiscRoutines.AddTOAForNewEE(CoNbr, EENbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscAddEDsForNewEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  CoNbr, EENbr: integer;
begin
  CoNbr := ADatagram.Params.Value['CoNbr'];
  EENbr := ADatagram.Params.Value['EENbr'];
  Context.RemoteMiscRoutines.AddEDsForNewEE(CoNbr, EENbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetTOARequestsForCO(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ResultsList: IisInterfaceList;
begin
  ResultsList := Context.RemoteMiscRoutines.GetTOARequestsForCo(
    ADatagram.Params.Value['CONBR'],
    ADatagram.Params.Value['Codes'],
    ADatagram.Params.Value['FromDate'],
    ADatagram.Params.Value['ToDate'],
    ADatagram.Params.Value['Manager'],
    ADatagram.Params.Value['TOType'],
    ADatagram.Params.Value['EECode']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ResultsList);
end;

procedure TevCommonDatagramDispatcher.dmMiscUpdateTOARequestsForCO(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Requests : IisInterfaceList;
begin
  Requests := IInterface(ADatagram.Params.Value['Requests']) as IisInterfaceList;
  Context.RemoteMiscRoutines.UpdateTOARequestsForCO(Requests, ADatagram.Params.Value['CustomCoNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscDeleteRowsInDatasets(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.RemoteMiscRoutines.DeleteRowsInDataSets(
    ADatagram.Params.Value['AClientNbr'],
    IInterface(ADatagram.Params.Value['ATableNames']) as IisStringList,
    IInterface(ADatagram.Params.Value['AConditions']) as IisStringList,
    IInterface(ADatagram.Params.Value['ARowNbrs']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessDropDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.DropDB(ADatagram.Params.Value['ADBName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessApplyDataSyncPacket(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.ApplyDataSyncPacket(TevDBType(ADatagram.Params.Value['ADBType']),
    IInterface(ADatagram.Params.Value['AData']) as IisStream);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessCheckDBHash(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  sErrors: String;
begin
  ctx_DBAccess.CheckDBHash(TevDBType(ADatagram.Params.Value['ADBType']),
    IInterface(ADatagram.Params.Value['AData']) as IisStream,
    ADatagram.Params.Value['ARequiredLastTransactionNbr'],
    sErrors);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AErrors', sErrors);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDataSyncPacket(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStream;
  EndTrans: Integer;
begin
  Res := ctx_DBAccess.GetDataSyncPacket(TevDBType(ADatagram.Params.Value['ADBType']),
    ADatagram.Params.Value['AStartTransNbr'], EndTrans);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('AEndTransNbr', EndTrans);  
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDBHash(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStream;
begin
  Res := ctx_DBAccess.GetDBHash(TevDBType(ADatagram.Params.Value['ADBType']), ADatagram.Params.Value['ARequiredLastTransactionNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmGlobalCallbacksSetDBMaintenance(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.SetDBMaintenance(ADatagram.Params.Value['aDBName'],
      ADatagram.Params.Value['aMaintenance'],
      ADatagram.Params.TryGetValue('aADRContext', False)); // TODO: make a mandatory param after Feb 2012
end;

procedure TevCommonDatagramDispatcher.dmDBAccessDisableDBs(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.DisableDBs(IInterface(ADatagram.Params.Value['ADBList']) as IisStringList);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessEnableDBs(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.EnableDBs(IInterface(ADatagram.Params.Value['ADBList']) as IisStringList);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscCreateEEChangeRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  EEChangeRequest: IevEEChangeRequest;
begin
  EEChangeRequest := IInterface(ADatagram.Params.Value['EEChangeRequest']) as IevEEChangeRequest;
  Context.RemoteMiscRoutines.CreateEEChangeRequest(EEChangeRequest);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscForMgrGetEEChangeRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  MgrNbr: integer;
  RequestsList: IisInterfaceList;
  RequestType: TevEEChangeRequestType;
begin
  MgrNbr := ADatagram.Params.Value['MgrNbr'];
  RequestType := TevEEChangeRequestType(Integer(ADatagram.Params.Value['RequestType']));
  RequestsList := Context.RemoteMiscRoutines.GetEEChangeRequestListForMgr(MgrNbr, RequestType);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RequestsList);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetEEChangeRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  EENbr: integer;
  RequestsList: IisInterfaceList;
  RequestType: TevEEChangeRequestType;
begin
  EENbr := ADatagram.Params.Value['EENbr'];
  RequestType := TevEEChangeRequestType(Integer(ADatagram.Params.Value['RequestType']));
  RequestsList := Context.RemoteMiscRoutines.GetEEChangeRequestList(EENbr, RequestType);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RequestsList);
end;

procedure TevCommonDatagramDispatcher.dmMiscProcessEEChangeRequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  RequestsNbrs: IisStringList;
  Operation: boolean;
  ResultList: IisStringList;
begin
  RequestsNbrs := IInterface(ADatagram.Params.Value['RequestsNbrs']) as IisStringList;
  Operation := ADatagram.Params.Value['Operation'];
  ResultList := Context.RemoteMiscRoutines.ProcessEEChangeRequests(RequestsNbrs, Operation);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ResultList);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetCoCalendarDefaults(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisListOfValues;
  iCoNbr: integer;
begin
  iCoNbr := ADatagram.Params.Value['CoNbr'];
  S := Context.RemoteMiscRoutines.GetCoCalendarDefaults(iCoNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscCreateCoCalendar(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisStringList;
  iCoNbr: integer;
  CoSettings: IisListOfValues;
  sBasedOn, sMoveCheckDate, sMoveCallInDate, sMoveDeliveryDate: String;
begin
  iCoNbr := ADatagram.Params.Value['CoNbr'];
  CoSettings := IInterface(ADatagram.Params.Value['CoSettings']) as IisListofvalues;
  sBasedOn := ADatagram.Params.Value['BasedOn'];
  sMoveCheckDate := ADatagram.Params.Value['MoveCheckDate'];
  sMoveCallInDate := ADatagram.Params.Value['MoveCallInDate'];
  sMoveDeliveryDate := ADatagram.Params.Value['MoveDeliveryDate'];
  S := Context.RemoteMiscRoutines.CreateCoCalendar(iCoNbr, CoSettings, sBasedOn, sMoveCheckDate, sMoveCallInDate, sMoveDeliveryDate);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmSecurityCheckAnswers(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.CheckAnswers(ADatagram.Params.Value['AUserName'],
    IInterface(ADatagram.Params.Value['AQuestionAnswerList']) as IisListOfValues);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecurityCheckExtAnswers(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.CheckExtAnswers(ADatagram.Params.Value['AUserName'],
    ADatagram.Params.Value['APassword'],
    IInterface(ADatagram.Params.Value['AQuestionAnswerList']) as IisListOfValues);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGenerateNewPassword(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := ctx_Security.GenerateNewPassword;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetAvailableQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := ctx_Security.GetAvailableQuestions(ADatagram.Params.Value['AUserName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetExtQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := ctx_Security.GetExtQuestions(ADatagram.Params.Value['AUserName'],
    ADatagram.Params.Value['APassword']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetPasswordRules(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := ctx_Security.GetPasswordRules;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := ctx_Security.GetQuestions(ADatagram.Params.Value['AUserName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityResetPassword(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.ResetPassword(ADatagram.Params.Value['AUserName'],
    IInterface(ADatagram.Params.Value['AQuestionAnswerList']) as IisListOfValues,
    ADatagram.Params.Value['ANewPassword']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecuritySetQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.SetQuestions(IInterface(ADatagram.Params.Value['AQuestionAnswerList']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSecurityValidatePassword(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.ValidatePassword(ADatagram.Params.Value['APassword']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscProcessSetEeEMail(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  sNewEMail: String;
begin
  iEENbr := ADatagram.Params.Value['AEENbr'];
  sNewEmail := ADatagram.Params.Value['ANewEmail'];
  Context.RemoteMiscRoutines.SetEeEmail(iEENbr, sNewEMail);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscProcessGetCoReportsParametersAsXML (const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iCoNbr: integer;
  iCoReportsNbr: integer;
  sXML: String;
begin
  iCoNbr := ADatagram.Params.Value['aCoNbr'];
  iCoReportsNbr := ADatagram.Params.Value['aCoReportsNbr'];
  sXML := Context.RemoteMiscRoutines.GetCoReportsParametersAsXML(iCoNbr, iCoReportsNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, sXML);
end;

procedure TevCommonDatagramDispatcher.dmMiscProcessStoreReportParametersFromXML(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iCoNbr: integer;
  iCoReportsNbr: integer;
  sXML: String;
  iResult: integer;
begin
  iCoNbr := ADatagram.Params.Value['aCoNbr'];
  iCoReportsNbr := ADatagram.Params.Value['aCoReportsNbr'];
  sXML := ADatagram.Params.Value['aXMLParameters'];
  iResult := Context.RemoteMiscRoutines.StoreReportParametersFromXML(iCoNbr, iCoReportsNbr, sXML);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, iResult);
end;

procedure TevCommonDatagramDispatcher.dmMiscConvertXMLtoRWParameters( const ADatagram: IevDatagram; out AResult: IevDatagram);
var
 sXMLParameters: AnsiString;
 iiRes: IisStream;
begin
  sXMLparameters := ADatagram.Params.Value['sXMLParameters'];
  iiRes := Context.RemoteMiscRoutines.ConvertXMLtoRWParameters(sXMLParameters);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, iiRes);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetSecurityRules(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := ctx_Security.GetSecurityRules;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
  iRequestNbr: integer;
begin
  iRequestNbr := ADatagram.Params.Value['RequestNbr'];
  Res := Context.RemoteMiscRoutines.GetBenefitRequest(iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscStoreBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  RequestData: IisListOfValues;
  iEENbr: integer;
  iRequestNbr: integer;
  Res: integer;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  RequestData := IInterface(ADatagram.Params.Value['RequestData']) as IisListOfValues;
  iRequestNbr := ADatagram.Params.Value['RequestNbr'];
  Res := Context.RemoteMiscRoutines.StoreBenefitRequest(iEENbr, RequestData, iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetBenefitRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
  iEENbr: integer;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  Res := Context.RemoteMiscRoutines.GetBenefitRequestList(iEENbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscSendEmailToHR(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  sSubject, sMessage: String;
  Attachements: IisListOfValues;
  Res: IisListOfValues;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  sSubject := ADatagram.Params.Value['Subject'];
  sMessage := ADatagram.Params.Value['Message'];
  Attachements := IInterface(ADatagram.Params.Value['Attachements']) as IisListOfValues;
  Res := Context.RemoteMiscRoutines.SendEMailToHR(iEENbr, sSubject, sMessage, Attachements);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscSendEmailToManagers(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  ARequestType, AMessageDetails: String;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  ARequestType := ADatagram.Params.Value['ARequestType'];
  AMessageDetails := ADatagram.Params.Value['AMessageDetails'];
  Context.RemoteMiscRoutines.SendEMailToManagers(iEENbr, ARequestType, AMessageDetails);
  AResult := CreateResponseFor(ADatagram.Header);  
end;


procedure TevCommonDatagramDispatcher.dmMiscGetEeExistingBenefits(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  bCurrentOnly: boolean;
  Res: IevDataset;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  bCurrentOnly := ADatagram.Params.Value['ACurrentOnly'];
  Res := Context.RemoteMiscRoutines.GetEeExistingBenefits(iEENbr, bCurrentOnly);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscDeleteBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iRequestNbr: integer;
begin
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  Context.RemoteMiscRoutines.DeleteBenefitRequest(iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetEeBenefitsPackageAvaliableForEnrollment(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  bCoBenefitsOnly: boolean;
  dSearchForDate: TDateTime;
  sCategory: String;
  Res: IisListOfValues;
begin
  iEENbr := ADatagram.Params.Value['EENbr'];
  dSearchForDate := ADatagram.Params.Value['ASearchForDate'];
  sCategory := ADatagram.Params.Value['ACategory'];
  bCobenefitsOnly := ADatagram.Params.Value['AReturnCoBenefitsDataOnly'];
  Res := Context.RemoteMiscRoutines.GetEeBenefitPackageAvaliableForEnrollment(iEENbr, dSearchForDate, sCategory, bCobenefitsOnly);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetSummaryBenefitEnrollmentStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
Var
  EENbrList: IisStringList;
  sType: String;
  Res: IevDataset;
begin
  EENbrList := IInterface(ADatagram.Params.Value['EENbrList']) as IisStringList;
  sType := ADatagram.Params.Value['Type'];
  Res := Context.RemoteMiscRoutines.GetSummaryBenefitEnrollmentStatus(EENbrList, sType);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscRevertBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iRequestNbr: integer;
begin
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  Context.RemoteMiscRoutines.RevertBenefitRequest(iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetBenefitRequestConfirmationReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iRequestNbr: integer;
  isResultPDF: Boolean;
  Res: IisStream;
begin
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  isResultPDF := ADatagram.Params.Value['isResultPDF'];

  Res := Context.RemoteMiscRoutines.GetBenefitRequestConfirmationReport(iRequestNbr, isResultPDF);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscSubmitBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iRequestNbr: integer;
  SignatureParams: IisListOfValues;
begin
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  SignatureParams := IInterface(ADatagram.Params.Value['ASignatureParams']) as IisListOfValues;
  Context.RemoteMiscRoutines.SubmitBenefitRequest(iRequestNbr, SignatureParams);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscSendBenefitEMailToEE(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  sMessage: String;
  sSubject: String;
  sCategoryType: String;
  iRequestNbr: integer;
begin
  iEENbr := ADatagram.Params.Value['AEENbr'];
  sSubject := ADatagram.Params.Value['ASubject'];
  sMessage := ADatagram.Params.Value['AMessage'];
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  sCategoryType := ADatagram.Params.Value['ACategoryType'];
  Context.RemoteMiscRoutines.SendBenefitEmailToEE(iEENbr, sSubject, sMessage, iRequestNbr, sCategoryType);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscEmptyBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.RemoteMiscRoutines.GetEmptyBenefitRequest;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscClosebenefitEnrollment(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iEENbr: integer;
  sCategotyType: String;
  iRequestNbr: integer;
begin
  iEENbr := ADatagram.Params.Value['AEENbr'];
  sCategotyType := ADatagram.Params.Value['ACategoryType'];
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  Context.RemoteMiscRoutines.CloseBenefitEnrollment(iEENbr, sCategotyType, iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetDataDictionaryXML(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisStream;
begin
  S := Context.RemoteMiscRoutines.GetDataDictionaryXML;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscApproveBenefitRequest(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iRequestNbr: integer;
begin
  iRequestNbr := ADatagram.Params.Value['ARequestNbr'];
  Context.RemoteMiscRoutines.ApproveBenefitRequest(iRequestNbr);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetBenefitEmailNotificationsList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.RemoteMiscRoutines.GetBenefitEmailNotificationsList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmMiscCreateManualCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := IInterface(ADatagram.Params.Value['Params']) as IevDualStream;
  S := Context.RemoteMiscRoutines.CreateManualCheck(S);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetManualTaxesListForCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  iPrCheckNbr: integer;
  Res: IevDataset;
begin
  iPrCheckNbr := ADatagram.Params.Value['PrCheckNbr'];
  Res := Context.RemoteMiscRoutines.GetManualTaxesListForCheck(iPrCheckNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessSweepDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.SweepDB(ADatagram.Params.Value['ADBName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetTransactionsStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.DBAccess.GetTransactionsStatus(TevDBType(ADatagram.Params.Value['ADBType']));
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSecurityGetAvailableExtQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := ctx_Security.GetAvailableExtQuestions(ADatagram.Params.Value['AUserName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;


procedure TevCommonDatagramDispatcher.dmSecuritySetExtQuestions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_Security.SetExtQuestions(IInterface(ADatagram.Params.Value['AQuestionAnswerList']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetSystemAccountNbr(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
 Res: Integer;
begin
  Res := Context.RemoteMiscRoutines.GetSystemAccountNbr;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessBeginRebuildAllClients(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.BeginRebuildAllClients;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessEndRebuildAllClients(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.EndRebuildAllClients;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetFieldValues(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.DBAccess.GetFieldValues(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AField'],
    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['AsOfDate'],
    ADatagram.Params.Value['AEndDate'],
    ADatagram.Params.Value['AMultiColumn']
  );

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetFieldVersions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.GetFieldVersions(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AFields'],
    ADatagram.Params.Value['ANbr'],
    IInterface(ADatagram.Params.Value['ASettings']) as IisListOfValues
  );

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessUpdateFieldVersion(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.UpdateFieldVersion(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['AOldBeginDate'],
    ADatagram.Params.Value['AOldEndDate'],
    ADatagram.Params.Value['ANewBeginDate'],
    ADatagram.Params.Value['ANewEndDate'],
    IInterface(ADatagram.Params.Value['AFieldsAndValues']) as IisListOfValues
  );

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollSetPayrollLock(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.SetPayrollLock(
    ADatagram.Params.Value['APrNbr'],
    ADatagram.Params.Value['AUnlocked']
  );
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmSingleSignOnGetSwipeClockOneTimeURL(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := Context.SingleSignOn.GetSwipeclockOneTimeURL(ADatagram.Params.Value['ACoNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessDoMonthEndCleanup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
  Warnings: String;
begin
  Res := Context.PayrollProcessing.DoPAMonthEndCleanup(
    ADatagram.Params.Value['CoNbr'],
    ADatagram.Params.Value['MonthEndDate'],
    ADatagram.Params.Value['MinTax'],
    ADatagram.Params.Value['AutoProcess'],
    ADatagram.Params.Value['EECustomNbr'],
    ADatagram.Params.Value['PrintReports'],
    TResourceLockType(Integer(ADatagram.Params.Value['LockType'])),
    Warnings
    );
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Integer(Res));
  AResult.Params.AddValue('Warnings', Warnings);
end;

procedure TevCommonDatagramDispatcher.dmSingleSignOnGetMRCOneTimeURL(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := Context.SingleSignOn.GetMRCOneTimeURL(ADatagram.Params.Value['ACoNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmSingleSignOnGetSwipeClockInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var  
  Res: IisListOfValues;
begin
  Res := Context.SingleSignOn.GetSwipeClockInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetNewKeyValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  key: Integer;
begin
  key := Context.DBAccess.GetNewKeyValue(ADatagram.Params.Value['ATableName'], ADatagram.Params.Value['AIncrement']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, key);
end;

procedure TevCommonDatagramDispatcher.dmPrCheckPrintingProxyCheckStubBlobToDatasets(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  v: variant;
begin
  v := Context.PayrollCheckPrint.CheckStubBlobToDatasets(ADatagram.Params.Value['BlobData']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, v);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessUpdateFieldValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Context.DBAccess.UpdateFieldValue(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AField'],
    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['ABeginDate'],
    ADatagram.Params.Value['AEndDate'],
    ADatagram.Params.Value['AValue']
  );

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetVersionFieldAudit(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.DBAccess.GetVersionFieldAudit(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AField'],
    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['ABeginDate']
  );

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetVersionFieldAuditChange(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataset;
begin
  Res := Context.DBAccess.GetVersionFieldAuditChange(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AField'],
//    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['ABeginDate']
  );

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetBlobAuditData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStream;
begin
  Res := Context.DBAccess.GetBlobAuditData(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AField'],
    ADatagram.Params.Value['ARefValue']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetRecordAudit(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.GetRecordAudit(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['AFields'],
    ADatagram.Params.Value['ANbr'],
    ADatagram.Params.Value['ABeginDate']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetTableAudit(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.GetTableAudit(
    ADatagram.Params.Value['ATable'],
    ADatagram.Params.Value['ABeginDate']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetDatabaseAudit(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.GetDatabaseAudit(
    TevDBType(ADatagram.Params.Value['ADBType']),
    ADatagram.Params.Value['ABeginDate']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessGetInitialCreationNBRAudit(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IevDataSet;
begin
  Res := Context.DBAccess.GetInitialCreationNBRAudit(ADatagram.Params.Value['ATables']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDBAccessUndeleteClientDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_DBAccess.UndeleteClientDB(ADatagram.Params.Value['AClientID']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmDashboardDataProviderGetCompanyReports(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.DashboardDataProvider.GetCompanyReports(
    ADatagram.Params.Value['ACl_Nbr'], ADatagram.Params.Value['ACo_Nbr'], ADatagram.Params.Value['RemoteUser']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmDashboardDataProviderGetCompanyMessages(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.DashboardDataProvider.GetCompanyMessages;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessGetPayrollBatchDefaults(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Context.PayrollProcessing.GetPayrollBatchDefaults(ADatagram.Params.Value['PrNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessRefreshScheduledEDsCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.RefreshSchduledEDsCheck(ADatagram.Params.Value['CheckNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;
procedure TevCommonDatagramDispatcher.dmPayrollProcessRefreshScheduledEDsBatch(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.RefreshSchduledEDsBatch(ADatagram.Params.Value['BatchNbr'], ADatagram.Params.Value['RegularChecksOnly']);
  AResult := CreateResponseFor(ADatagram.Header);
end;
procedure TevCommonDatagramDispatcher.dmPayrollProcessGrossToNet(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Warnings: String;
  Res: Boolean;
begin
  Res := Context.PayrollProcessing.GrossToNet(Warnings, ADatagram.Params.Value['CheckNumber'],
    ADatagram.Params.Value['ApplyUpdatesInTransaction'],
    ADatagram.Params.Value['JustPreProcess'],
    ADatagram.Params.Value['FetchPayrollData'],
    ADatagram.Params.Value['SecondCheck']); // Result indicates if there were any shortfalls
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('Warnings', Warnings);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessVoidCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := ctx_PayrollProcessing.VoidCheck(ADatagram.Params.Value['CheckToVoid'],
      ADatagram.Params.Value['PrBatchNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;


procedure TevCommonDatagramDispatcher.dmPayrollProcessTaxCalculator(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
  PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: IisStream;
begin
  PR := IInterface(ADatagram.Params.Value['PR']) as IisStream;
  PR_BATCH := IInterface(ADatagram.Params.Value['PR_BATCH']) as IisStream;
  PR_CHECK := IInterface(ADatagram.Params.Value['PR_CHECK']) as IisStream;
  PR_CHECK_LINES := IInterface(ADatagram.Params.Value['PR_CHECK_LINES']) as IisStream;
  PR_CHECK_LINE_LOCALS := IInterface(ADatagram.Params.Value['PR_CHECK_LINE_LOCALS']) as IisStream;
  PR_CHECK_STATES := IInterface(ADatagram.Params.Value['PR_CHECK_STATES']) as IisStream;
  PR_CHECK_SUI := IInterface(ADatagram.Params.Value['PR_CHECK_SUI']) as IisStream;
  PR_CHECK_LOCALS := IInterface(ADatagram.Params.Value['PR_CHECK_LOCALS']) as IisStream;

  Context.PayrollProcessing.TaxCalculator(
    PR,
    PR_BATCH,
    PR_CHECK,
    PR_CHECK_LINES,
    PR_CHECK_LINE_LOCALS,
    PR_CHECK_STATES,
    PR_CHECK_SUI,
    PR_CHECK_LOCALS,
    ADatagram.Params.Value['DisableYTDs'],
    ADatagram.Params.Value['DisableShortfalls'],
    ADatagram.Params.Value['NetToGross']
    );

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('PR_CHECK', PR_CHECK);
  AResult.Params.AddValue('PR_CHECK_LINES', PR_CHECK_LINES);
  AResult.Params.AddValue('PR_CHECK_LINE_LOCALS', PR_CHECK_LINE_LOCALS);
  AResult.Params.AddValue('PR_CHECK_STATES', PR_CHECK_STATES);
  AResult.Params.AddValue('PR_CHECK_SUI', PR_CHECK_SUI);
  AResult.Params.AddValue('PR_CHECK_LOCALS', PR_CHECK_LOCALS);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessUnvoidCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.UnvoidCheck(ADatagram.Params.Value['CheckNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmPayrollProcessRedistributeDBDT(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  ctx_PayrollProcessing.RedistributeDBDT(ADatagram.Params.Value['APrCheckLineNbr'], IInterface(ADatagram.Params.Value['Redistribution']) as IisParamsCollection);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetSysReportByNbr(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := Context.RemoteMiscRoutines.GetSysReportByNbr(ADatagram.Params.Value['aReportNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetReportFileByNbrAndType(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IevDualStream;
begin
  S := Context.RemoteMiscRoutines.GetReportFileByNbrAndType(ADatagram.Params.Value['aReportNbr'], ADatagram.Params.Value['aReportType']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;

procedure TevCommonDatagramDispatcher.dmMiscDeleteEE(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  b: Boolean;
  sTmp : Integer;
begin
  sTmp := ADatagram.Params.Value['AEENbr'];
  b := Context.RemoteMiscRoutines.DeleteEE(sTmp);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, b);
end;

procedure TevCommonDatagramDispatcher.dmVMRGetRemoteVMRReportList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ds: IevDataSet;
  ALocation:string;
begin
  ALocation := ADatagram.Params.Value['ALocation'];
  ds := Context.VmrRemote.GetRemoteVMRReportList(ALocation);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ds);
end;

procedure TevCommonDatagramDispatcher.dmVMRGetRemoteVMRReport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  SBMailBoxContentNbr : integer;
  S : IevDualStream;
begin
  SBMailBoxContentNbr := ADatagram.Params.Value['SBMailBoxContentNbr'];
  S := Context.VmrRemote.GetRemoteVMRReport(SBMailBoxContentNbr);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, S);
end;



procedure TevCommonDatagramDispatcher.dmMiscGetACAHistory(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ds: IevDataSet;
begin
  ds := Context.RemoteMiscRoutines.GetACAHistory(ADatagram.Params.Value['aEENbr'],
                                                  ADatagram.Params.Value['aYear']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ds);
end;

procedure TevCommonDatagramDispatcher.dmMiscPostACAHistory(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  b: Boolean;
begin
  b := Context.RemoteMiscRoutines.PostACAHistory(ADatagram.Params.Value['aEENbr'],
                                                  ADatagram.Params.Value['aYear'],
                                                  IInterface(ADatagram.Params.Value['aCd']) as IevDataSet);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, b);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetACAOfferCodes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ds: IevDataSet;
begin
  ds := Context.RemoteMiscRoutines.GetACAOfferCodes(ADatagram.Params.Value['aCoNbr'], ADatagram.Params.Value['AsOfDate']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ds);
end;

procedure TevCommonDatagramDispatcher.dmMiscGetACAReliefCodes(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ds: IevDataSet;
begin
  ds := Context.RemoteMiscRoutines.GetACAReliefCodes(ADatagram.Params.Value['aCoNbr'], ADatagram.Params.Value['AsOfDate']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, ds);
end;

{ TevProgressUpdater }

procedure TevProgressUpdater.BackgroundSender(const Params: TTaskParamList);
var
  i: Integer;
begin
  while not CurrentThreadTaskTerminated do
  begin
    for i := 1 to 30 do
    begin
      if WaitForSingleObject(FWakeUpEvent, 1000) <> WAIT_TIMEOUT then
      begin
        AbortIfCurrentThreadTaskTerminated;
        break;
      end
      else
        AbortIfCurrentThreadTaskTerminated;
    end;

    Sleep(1000); // Start checking progress after 1 sec timeout

    Lock;
    try
      for i := 0 to ChildCount - 1 do
        (GetChild(i) as IevContextCallbacksProxy).SendProgressInf;
    finally
      Unlock;
    end;
  end;
end;

destructor TevProgressUpdater.Destroy;
begin
  if GlobalThreadManagerIsActive then
  begin
    GlobalThreadManager.TerminateTask(FTask);
    WakeUp;
    GlobalThreadManager.WaitForTaskEnd(FTask);
  end;
  CloseHandle(FWakeUpEvent);
  inherited;
end;

procedure TevProgressUpdater.DoOnConstruction;
begin
  inherited;
  InitLock;
  FWakeUpEvent := CreateEvent(nil, False, False, nil);
  FTask := GlobalThreadManager.RunTask(BackgroundSender, Self, MakeTaskParams([]), 'Progress Updater');
end;

procedure TevProgressUpdater.WakeUp;
begin
  SetEvent(FWakeUpEvent);
end;

{ TevTransactionKeyRemap }

constructor TevTransactionKeyRemap.Create(const AMethodParams: IisParamsCollection);
begin
  inherited Create;
  FParams := AMethodParams;
end;

function TevTransactionKeyRemap.Execute: IisValueMap;
var
  i: Integer;
  DS: TEvRemapDataSet;
  FDSList: TList;
begin
  FKeyMap := nil;
  FNewRecByTables := TisListOfValues.Create;
  FDSList := TList.Create;
  try
    FDSList.Capacity := FParams.Count;

    // First phase (collect new PKs)
    for i := 0 to FParams.Count - 1 do
      if FParams.Params[i].Value['Method'] = 'ApplyDataChangePacket' then
        FirstPhase(IInterface(FParams.Params[i].Value['APacket']) as IevDataChangePacket);

    // Generate new keys and create key map
    SecondPhase;

    // Third phase (update PK and FK fields)
    if FNewRecByTables.Count > 0 then
    begin
      for i := 0 to FDSList.Count - 1 do
      begin
        DS := TEvRemapDataSet(FDSList[i]);
        ThirdPhase(DS);
        if DS.FModified then  // DS was updated
          FParams[DS.FParamIndex].Value['AData'] := DS.Delta;  // Update delta stream in method params
      end;

      for i := 0 to FParams.Count - 1 do
        if FParams.Params[i].Value['Method'] = 'ApplyDataChangePacket' then
          ThirdPhase(IInterface(FParams.Params[i].Value['APacket']) as IevDataChangePacket)
    end;

  finally
    FNewRecByTables := nil;
    try
      for i := 0 to FDSList.Count - 1 do
        TEvRemapDataSet(FDSList[i]).Free;
    finally
      FDSList.Free;
    end;
  end;

  Result := FKeyMap;
end;

procedure TevTransactionKeyRemap.FirstPhase(const ADataSet: TEvRemapDataSet);
var
  Val: IisNamedValue;
  s: String;
  Key: Integer;
  PK: TField;
begin
  PK := ADataSet.Fields.FieldByName(ADataSet.FTableName + '_NBR');

  ADataSet.First;
  while not ADataSet.Eof do
  begin
    Key := PK.AsInteger;
    if (ADataSet.CompleteUpdateStatus = usInserted) and (Key < 0) then
    begin
      Val := FNewRecByTables.FindValue(ADataSet.FTableName);
      if not Assigned(Val) then
        Val := FNewRecByTables.Values[FNewRecByTables.AddValue(ADataSet.FTableName, '')];

      s := Val.Value;
      AddStrValue(s, IntToStr(- Key)); // to save some space not storing "-"
      Val.Value := s;
    end;

    ADataSet.Next;
  end;
end;

procedure TevTransactionKeyRemap.FirstPhase(const APacket: IevDataChangePacket);
var
  i: Integer;
  ChangeItem: IevDataChangeItem;
  Val: IisNamedValue;
  s: String;
  Key: Integer;
begin
  // Collect new records with negative nbrs grouped by tables
  for i := 0 to APacket.Count - 1 do
  begin
    ChangeItem := APacket[i];
    if ChangeItem.ChangeType = dctInsertRecord then
    begin
      Key := ChangeItem.Key;
      if Key < 0 then
      begin
        Val := FNewRecByTables.FindValue(ChangeItem.DataSource);
        if not Assigned(Val) then
          Val := FNewRecByTables.Values[FNewRecByTables.AddValue(ChangeItem.DataSource, '')];

        s := Val.Value;
        AddStrValue(s, IntToStr(- Key)); // to save some space not storing "-"
        Val.Value := s;
      end;
    end;
  end;
end;

procedure TevTransactionKeyRemap.ThirdPhase(const ADataSet: TEvRemapDataSet);
var
  i: Integer;
  Fld: TField;
begin
  // Update PK and FK fields with real numbers
  ADataSet.First;
  while not ADataSet.Eof do
  begin
    if ADataSet.CompleteUpdateStatus in [usInserted, usModified] then
    begin
      for i := 0 to ADataSet.FieldCount - 1 do
      begin
        Fld := ADataSet.Fields[i];
        if (Fld.Tag = 1) and (Fld.AsInteger < 0) then  // key field attribute
        begin
          if ADataSet.State <> dsEdit then
            ADataSet.Edit;
          try
            Fld.AsInteger := FKeyMap.DecodeValue(Fld.AsInteger);
          except
            on E: Exception do
            begin
              E.Message := 'Key mapping error for ' + ADataSet.FTableName + '.' + Fld.FieldName + #13 + E.Message;
              raise;
            end;
          end;
        end;
      end;

      if ADataSet.State = dsEdit then
      begin
        ADataSet.Post;
        ADataSet.FModified := True;
      end;
    end;
    ADataSet.Next;
  end;
end;
procedure TevTransactionKeyRemap.SecondPhase;
var
  i, j, NewKeyNbr: Integer;
  Keys: IisStringList;
begin
  FKeyMap := TisValueMap.Create;

  Keys := TisStringList.Create;
  for i :=0 to FNewRecByTables.Count - 1 do
  begin
    Keys.CommaText := FNewRecByTables[i].Value;

    // Cut off range of keys at once and update primary keys
    NewKeyNbr := Context.DBAccess.GetNewKeyValue(FNewRecByTables[i].Name, Keys.Count) - Keys.Count + 1;

    for j := 0 to Keys.Count - 1 do
    begin
      FKeyMap.EncodeValue(- StrToInt(Keys[j]), NewKeyNbr);
      Inc(NewKeyNbr);
    end;
  end;
end;

procedure TevTransactionKeyRemap.ThirdPhase(const APacket: IevDataChangePacket);
var
  i, j: Integer;
  ChangeItem: IevDataChangeItem;
  Val: IisNamedValue;
  v: Variant;
begin
  // Update PK and FK fields with real numbers
  for i := 0 to APacket.Count - 1 do
  begin
    ChangeItem := APacket[i];
    for j := 0 to ChangeItem.Fields.Count - 1 do
    begin
      Val := ChangeItem.Fields[j];
      v := Val.Value;
      if VarIsType(v, varInteger) and (v < 0) and
         not AnsiSameText(Val.Name, 'TAX_COUPON_SY_REPORTS_NBR') //todo REMOVE ASAP!!!
         and FinishesWith(Val.Name, '_NBR') then
      begin
        try
          Val.Value := FKeyMap.DecodeValue(Val.Value);
        except
          on E: Exception do
          begin
            E.Message := 'Key mapping error for ' + ChangeItem.DataSource + '.' +  Val.Name + #13 + E.Message;
            raise;
          end;
        end;
      end;
    end;

    if ChangeItem.Key < 0 then
      ChangeItem.Key := FKeyMap.DecodeValue(ChangeItem.Key);
  end;
end;

end.
