unit evTCPClient;

interface

uses Forms, Windows, isBaseClasses, EvCommonInterfaces, isSocket, isStreamTCP, isTransmitter,
     EvTransportShared, EvTransportDatagrams, evStreamUtils, isBasicUtils, EvConsts, isErrorUtils,
     SysUtils, EvContext, EvMainboard, Dialogs, isThreadManager, evUtils, ISZippingRoutines,
     evRemoteMethods, EvTransportInterfaces, EvBasicUtils, evCommonDatagramDispatcher,
     isExceptions, isLogFile;

implementation

uses DateUtils;

type
  TevTCPClient = class(TevCustomTCPClient, IevTCPClient, IevTrafficDumping)
  private
    FSessionID: TisGUID;
    FHost: String;
    FPort: String;
    FDumpFileName: String;
    FDumpTraffic: Boolean;
    FRestoreSessionTaskID: TTaskID;
    FConnectedHost: String;
    FCreateConnectionTimeout: Integer;
    FArtificialLatency: Integer;
    procedure Connect;
    function  GetValidSession: IevSession;
    procedure RestoreSession(const Params: TTaskParamList);
    procedure RestoreSessionInBackground;
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure OnConnectStatusChange(const AText: String; const AProgress: Integer); override;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  GetHost: String;
    function  GetPort: String;
    procedure SetHost(const AValue: String);
    function  GetArtificialLatency: Integer;
    procedure SetArtificialLatency(const AValue: Integer);
    function  ConnectedHost: String;
    procedure SetPort(const AValue: String);
    function  Session: IevSession;
    procedure Logoff;
    procedure SetCreateConnectionTimeout(const AValue: Integer);

    // Traffic dumping
    function  GetDumpTraffic: Boolean;
    function  GetDumpFileName: String;
    procedure StartDumping(const ADumpFileName: String);
    procedure StopDumping;
  public
    destructor Destroy; override;
  end;


  TevClientDatagramDispatcher = class(TevCommonDatagramDispatcher)
  protected
    procedure RegisterHandlers; override;
    procedure DoOnIdle; override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
  end;


function GetTCPClient: IevTCPClient;
begin
  Result := TevTCPClient.Create;
end;

{ TevTCPClient }

function TevTCPClient.GetValidSession: IevSession;
var
  Started: TDateTime;
begin
  Lock;
  try
    Result := Session;
    if not Assigned(Result) or (Result.State <> ssActive) then
    begin
      if Assigned(Result) and (Result.State <> ssInactive) then
        Disconnect(Result.SessionID);

      Started := Now;
      while True do
      begin
        try
          Connect;
          break;
        except
          on E: Exception do
          begin
            if SecondsBetween(Now, Started) >= FCreateConnectionTimeout then
              raise
            else
            begin
              mb_LogFile.AddEvent(etError, 'TCP Client', E.Message, GetErrorDetails(E));
              if Trim(FHost) = '' then break;

              AbortIfCurrentThreadTaskTerminated;
              Snooze(5000);
            end;
          end;
        end;
      end;

      Result := Session;
    end;
  finally
    UnLock;
  end;

  CheckCondition(Assigned(Result), 'Cannot create session');
end;

procedure TevTCPClient.Connect;
var
  Hosts: IisStringList;
  i: Integer;
  sErrors: String;
  bStop: Boolean;

  procedure TryConnect(AHost: String); // do not make it CONST! Delphi bug!
  begin
    ctx_StartWait('Connecting to server', 3);
    try
      try
        FSessionID := CreateConnection(AHost, FPort,
          EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain), Context.UserAccount.Password,
          FSessionID, Context.GetID);
        FConnectedHost := AHost;
        sErrors := '';
        bStop := True;
      except
        on E: EevConnection do
          AddStrValue(sErrors, E.Message, #13)
        else
          raise;
      end;
    finally
      ctx_EndWait;
    end;
  end;

begin
  ErrorIf(Trim(FHost) = '', 'Evolution server location is empty', EisException);

  Hosts := TisStringList.Create;
  Hosts.CommaText := Trim(FHost);
  sErrors := '';
  bStop := False;

  if FConnectedHost <> '' then
  begin
    i := Hosts.IndexOf(FConnectedHost);
    if i <> -1 then
      Hosts.Delete(i);
    TryConnect(FConnectedHost);
  end;

  if not bStop then
  begin
    for i := 0 to Hosts.Count - 1 do
    begin
      TryConnect(Hosts[i]);
      if bStop then
        break;
    end;
  end;

  if sErrors <> '' then
    raise EisException.CreateDetailed('Cannot connect to Evolution server', sErrors);
end;

function TevTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevClientDatagramDispatcher.Create;
  Result.SessionTimeout := CLIENT_SESSION_KEEP_ALIVE_FOREVER;
end;

destructor TevTCPClient.Destroy;
var
  Tsk: TTaskID;
begin
  Tsk := InterlockedExchange(Integer(FRestoreSessionTaskID), Integer(FRestoreSessionTaskID));
  if (Tsk <> 0) and GlobalThreadManagerIsActive then
  begin
    GlobalThreadManager.TerminateTask(Tsk);
    GlobalThreadManager.WaitForTaskEnd(Tsk);
  end;

  inherited;
end;

procedure TevTCPClient.DoOnConstruction;
begin
  inherited;
  FSSLCertFile := AppDir + 'cert.pem';
  FDumpFileName := '';
  FDumpTraffic := False;
  FCreateConnectionTimeout := 0; //sec

  // client doesn't need so many threads
  GetDatagramDispatcher.ThreadPoolCapacity := 2;
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPClient).ThreadPoolCapacity := 2;
end;

function TevTCPClient.GetDumpFileName: String;
begin
  Result := FDumpFileName;
end;

function TevTCPClient.GetDumpTraffic: Boolean;
begin
  Result := FDumpTraffic;
end;

function TevTCPClient.GetHost: String;
begin
  Result := FHost;
end;

function TevTCPClient.GetPort: String;
begin
  Result := FPort;
end;

procedure TevTCPClient.OnConnectStatusChange(const AText: String; const AProgress: Integer);
begin
  inherited;
  ctx_UpdateWait(AText, AProgress);
end;

procedure TevTCPClient.RestoreSession(const Params: TTaskParamList);
var
  S: IevSession;
  Started: TDateTime;
begin
  // make sure two things:
  // 1. All session connections are dropped
  // 2. server set session in inactive state
  Snooze(1000);

  Lock;
  try
    InterlockedExchange(Integer(FRestoreSessionTaskID), GetCurrentThreadTaskID);
    Started := Now;
    while not CurrentThreadTaskTerminated do
    begin
      try
        S := GetValidSession;
        break;
      except
        if SecondsBetween(Now, Started) >= 60 then
        begin
          try
            Disconnect(FSessionID);
          except
          end;
          raise;
        end;
        Snooze(5000);
      end;
    end;

  finally
    InterlockedExchange(Integer(FRestoreSessionTaskID), 0);
    Unlock;
  end;
end;

procedure TevTCPClient.RestoreSessionInBackground;
var
  Tsk: TTaskID;
begin
  if GetDatagramDispatcher.SessionTimeout <> CLIENT_SESSION_NEVER_KEEP_ALIVE then
  begin
    Tsk := InterlockedExchange(Integer(FRestoreSessionTaskID), Integer(FRestoreSessionTaskID));
    if Tsk <> 0 then
      GlobalThreadManager.TerminateTask(Tsk);

    Tsk := Context.RunParallelTask(RestoreSession, Self, MakeTaskParams([]), 'Restore client session');
    InterlockedExchange(Integer(FRestoreSessionTaskID), Integer(Tsk));
  end;  
end;

function TevTCPClient.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
  Stm, FileStream: IevDualStream;
begin
  Lock;
  try
    S := Session;
    if not Assigned(S) or (S.State in [ssInvalid, ssInactive]) then
      S := GetValidSession;
  finally
    Unlock;
  end;

  if IsQAMode and FDumpTraffic then
  begin
    Stm := TEvDualStreamHolder.Create;
   (ADatagram as IisInterfacedObject).WriteToStream(Stm);
    Stm.Position := 0;
    if not FileExists(FDumpFileName) then
      FileStream := TEvDualStreamHolder.CreateFromNewFile(FDumpFileName, False, False)
    else
      FileStream := TEvDualStreamHolder.CreateFromFile(FDumpFileName, False, False);
    FileStream.Position := FileStream.Size;
    FileStream.WriteStream(Stm);
    FileStream := nil;
    Stm := nil;
  end;

  ctx_StartWait;
  try
    ADatagram.Header.SessionID := S.SessionID;

    // This is an artificial latency to imitate network latency 
    if FArtificialLatency > 0 then
      Sleep(FArtificialLatency);

    Result := S.SendRequest(ADatagram);
  finally
    ctx_EndWait;
  end;
end;

function TevTCPClient.Session: IevSession;
begin
  Result := FindSession(FSessionID);
  if not Assigned(Result) then
    FSessionID := '';
end;

procedure TevTCPClient.SetHost(const AValue: String);
begin
  if FSessionID = '' then
  begin
    FHost := AValue;
    FConnectedHost := '';
  end;
end;

procedure TevTCPClient.SetPort(const AValue: String);
begin
  if FSessionID = '' then
  begin
    FPort := AValue;
    if FPort <> IntToStr(TCPClient_Port) then
      SetEncryptionType(etSSL);
  end;
end;


procedure TevTCPClient.StartDumping(const ADumpFileName: String);
begin
  FDumpFileName := ADumpFileName;
  FDumpTraffic := True;
end;

procedure TevTCPClient.StopDumping;
begin
  FDumpTraffic := False;
end;

procedure TevTCPClient.Logoff;
begin
  Lock;
  try
    if Session <> nil then
    begin
      try
        try
          if (Session.State = ssActive) and Assigned(Mainboard.GlobalCallbacks) then
            Mainboard.GlobalCallbacks.Unsubscribe('', cetAnyEvent, '');
        finally
          Disconnect(FSessionID);
        end  
      except
        // suppress all exceptions
        // todo: remove this logic in "M" release!
      end;
      FSessionID := '';
    end;
  finally
    Unlock;
  end;
end;

function TevTCPClient.ConnectedHost: String;
begin
  Result := FConnectedHost;
end;

procedure TevTCPClient.SetCreateConnectionTimeout(const AValue: Integer);
begin
  FCreateConnectionTimeout := AValue;
end;

function TevTCPClient.GetArtificialLatency: Integer;
begin
  Result := FArtificialLatency;
end;

procedure TevTCPClient.SetArtificialLatency(const AValue: Integer);
begin
  FArtificialLatency := AValue;
end;

{ TevClientDatagramDispatcher }

procedure TevClientDatagramDispatcher.DoOnIdle;
begin
  ctx_EvolutionGUI.OnWaitingServerResponse;
end;

procedure TevClientDatagramDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;

  if Application.Terminated then
    Exit;

  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
    try
      Context.EvolutionGUI.OnChangeSessionStatus(ASession.State, APreviousState);

      if ASession.State = ssActive then
        Context.EvolutionGUI.CheckTaskQueueStatus
      else if (APreviousState = ssActive) and (ASession.State <> ssActive) and (Mainboard.GlobalCallbacks <> nil) then
      begin
        // drop cached data
        Mainboard.GlobalCallbacks.SecurityChange('');
        Mainboard.GlobalCallbacks.GlobalSettingsChange('');
      end;

    finally
      try
        if ASession.KeepAlive and (APreviousState = ssActive) and (ASession.State = ssInactive) then
          TevTCPClient((Mainboard.TCPClient as IisInterfacedObject).GetImplementation).RestoreSessionInBackground;
      finally
        Mainboard.ContextManager.DestroyThreadContext;
      end;  
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevClientDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_GLOBALCALLBACKS_GLOBALSETTINGSCHANGE, dmGlobalCallbacksGlobalSettingsChange);
  AddHandler(METHOD_GLOBALCALLBACKS_SECURITYCHANGE, dmGlobalCallbacksSecurityChange);
  AddHandler(METHOD_GLOBALCALLBACKS_TASKQUEUEEVENT, dmGlobalCallbacksTaskQueueEvent);
  AddHandler(METHOD_GLOBALCALLBACKS_GLOBALFLAGCHANGE, dmGlobalCallbacksGlobalFlagChange);
  AddHandler(METHOD_GLOBALCALLBACKS_POPUPMESSAGE, dmGlobalCallbacksPopupMessage);

  AddHandler(METHOD_CONTEXT_CALLBACKS_UPDATEWAIT, dmContextCallbacksUpdateWait);
  AddHandler(METHOD_CONTEXT_CALLBACKS_ASYNCCALLRETURN, dmContextCallbacksAsyncCallReturn);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTCPClient, IevTCPClient, 'evTCPClient');

end.

