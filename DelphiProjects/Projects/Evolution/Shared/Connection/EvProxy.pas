unit evProxy;

interface

uses SysUtils, isBaseClasses, EvCommonInterfaces, EvContext, EvMainboard, EvTransportDatagrams,
     EvTransportInterfaces, EvBasicUtils, evExceptions, EvConsts, isStatistics;

type
  TevProxyModule = class(TisInterfacedObject, IevProxyModule)
  protected
    procedure NotRemotableMethod;
    function  Connected: Boolean;
    procedure OnDisconnect; virtual;  // Call it manually where it needs
    function  CreateDatagram(const AMethod: String): IevDatagram;
    function  SendRequest(const ADatagram: IevDatagram; const AProvideEnvironment: Boolean = True): IevDatagram;
  end;

implementation

{ TevProxyModule }

function TevProxyModule.SendRequest(const ADatagram: IevDatagram; const AProvideEnvironment: Boolean): IevDatagram;
var
  RemoteStat: IisStatEvent;
  V: IisNamedValue;
begin
  if AProvideEnvironment then
  begin
    ADatagram.Params.AddValue('ClientID', ctx_DBAccess.CurrentClientNbr);
    ADatagram.Params.AddValue('ADRContext', ctx_DBAccess.GetADRContext);
  end;

  if ctx_Statistics.Enabled then
  begin
    ADatagram.Params.AddValue(DM_PARAM_STATISTICS, True);
    ctx_Statistics.BeginEvent('Remote call');
    ctx_Statistics.ActiveEvent.Properties.AddValue('Method', ADatagram.Header.Method);
  end;
  
  try
    Result := Mainboard.TCPClient.SendRequest(ADatagram);
  finally
    if ctx_Statistics.Enabled then
    begin
      ctx_Statistics.ActiveEvent.Properties.AddValue('Sent bytes', ADatagram.TransmittedBytes);
      // Add remotely collected statistics
      if Assigned(Result) then
      begin
        ctx_Statistics.ActiveEvent.Properties.AddValue('Received bytes', Result.TransmittedBytes);
        if Result.Params.ValueExists(DM_PARAM_STATISTICS) then
        begin
          RemoteStat := IInterface(Result.Params.Value[DM_PARAM_STATISTICS]) as IisStatEvent;
          V := RemoteStat.Properties.FindValue('Receive time');
          if Assigned(V) then
          begin
            ctx_Statistics.ActiveEvent.Properties.AddValue('Send time', V.Value);
            RemoteStat.Properties.DeleteValue('Receive time');
          end;
          ctx_Statistics.ActiveEvent.AddEvent(RemoteStat);
        end;
        ctx_Statistics.ActiveEvent.Properties.AddValue('Receive time', Result.TransmissionTime);
       end;
      ctx_Statistics.EndEvent;
      ADatagram.Params.DeleteValue(DM_PARAM_STATISTICS);
    end;
  end;
end;

procedure TevProxyModule.NotRemotableMethod;
begin
  raise EevException.Create('Attempt to call local method as remote');
end;

function TevProxyModule.CreateDatagram(const AMethod: String): IevDatagram;
begin
  Result := TevDatagram.Create(AMethod);
  Result.Header.User := EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
  Result.Header.Password := Context.UserAccount.Password;
  Result.Header.ContextID := Context.GetID;
end;

procedure TevProxyModule.OnDisconnect;
begin
end;

function TevProxyModule.Connected: Boolean;
var
  S: IevSession;
begin
  S := Mainboard.TCPClient.Session;
  Result := Assigned(S) and (S.State = ssActive);
end;

end.

