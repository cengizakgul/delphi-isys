// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_BenefitChoice;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_SafeChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure,
  SPD_ChoiceFromListQueryEx, ActnList, 
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDataDictclient, ISDataAccessComponents, EvDataAccessComponents, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, ComCtrls;

type
  TBenefitChoice = class(TSafeChoiceFromListQuery)
    DM_COMPANY: TDM_COMPANY;
  private
    { Private declarations }

  public
    { Public declarations }
    procedure SetFilter( flt: string );
    destructor Destroy; override;

  end;

implementation

uses
  evutils, EvConsts;
{$R *.DFM}

{ TDBDTChoiceQuery }


destructor TBenefitChoice.Destroy;
begin
  inherited;

end;

procedure TBenefitChoice.SetFilter(flt: string);
begin
  dgChoiceList.DataSource := nil;

  cdsChoiceList.Data := DM_COMPANY.CO_BENEFITS.Data;
  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
  dgChoiceList.DataSource := dsChoiceList;

end;

end.
