// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ProcessException;

interface

uses
  Dialogs, Forms, SysUtils, classes,  SShowStringList, EvUtils, SDataStructure;
  

type    
  TableException = record
    T: string; // Type
    D: string; // Description
  end;

  TLogException = class
  private
    LogList: TStringList;
  public
    procedure StartList; //  Starts queuing of error message
                              // else Add will show messages when errors are detected
    procedure DeleteList;
    procedure ShowList(Title: string); // Show list of queued message
    function ListContainsExceptions: boolean;
    function Description(ExceptionType: string): string;
    constructor Create;
    destructor Destroy; override;
  end;

const
  PE_STATE_TRESHHOLD_SB_CHANGED = 'ST';
  PE_STATE_TRESHHOLD_SB_ROLLEDBACK = 'SR';
  PE_FED_100000_TRESHOLD_MET = 'FT';
  PE_FED_100000_ROLLEDBACK   = 'FR';
  PE_STATE_TRESHHOLD_EXCEEDED = 'SE';
  PE_NEW_HIRE_NOT_BILLED = 'NH';
  PE_GONNA_BE_LATE = 'BL';
  PE_CHECK_WAS_CUT_FOR_LATE = 'CL';
  PE_AS_OF_DATE_CHANGE = 'AS';
  PE_CREATION_DATE_CHANGE = 'CR';
  PE_PAYROLL_RESULTS = 'PR';
  PE_FED_CHECK_DUE_DATE = 'CF';
  PE_STATE_CHECK_DUE_DATE = 'CS';
  PE_LOCAL_TRESHHOLD_SB_CHANGED = 'LT';
  PE_LOCAL_TRESHHOLD_EXCEEDED = 'LE';

  NumExceptionDescriptions = 15;

var
  ExceptionDescriptions: array[1..NumExceptionDescriptions] of TableException =
    (
    (T: PE_STATE_TRESHHOLD_SB_CHANGED; D: 'State threshold should be changed'),
    (T: PE_FED_100000_TRESHOLD_MET; D: '$100,000 Federal threshold has been met'),
    (T: PE_STATE_TRESHHOLD_EXCEEDED; D: 'State threshold has been exceeded'),
    (T: PE_GONNA_BE_LATE; D: 'At least one of the calculated due dates is backdated'),
    (T: PE_CHECK_WAS_CUT_FOR_LATE; D: 'Late liabilities found. Special check(s) has been cut'),
    (T: PE_NEW_HIRE_NOT_BILLED; D: 'New hire was not billed'),
    (T: PE_AS_OF_DATE_CHANGE; D: 'As Of Date of a record was changed'),
    (T: PE_CREATION_DATE_CHANGE; D: 'Creation Date of a record was changed'),
    (T: PE_PAYROLL_RESULTS; D: 'Payroll was processed with warnings or exceptions'),
    (T: PE_FED_CHECK_DUE_DATE; D: 'Federal tax due date for this backdated payroll may be set late. Be sure to review your due dates and tax deposit frequencies if threshold limits have already been met.'),
    (T: PE_STATE_CHECK_DUE_DATE; D: 'Threshold driven state tax due date for this backdated payroll may be set late'),
    (T: PE_LOCAL_TRESHHOLD_SB_CHANGED; D: 'Local threshold should be changed'),
    (T: PE_LOCAL_TRESHHOLD_EXCEEDED; D: 'Local threshold has been exceeded'),
    (T: PE_STATE_TRESHHOLD_SB_ROLLEDBACK; D: 'State threshold should be rolled back'),
    (T: PE_FED_100000_ROLLEDBACK; D: '$100,000 Federal threshold should be rolled back')
    );

  LogException: TLogException;

implementation

procedure TLogException.StartList;
begin
  if not assigned(LogList) then
    LogList := TStringList.Create;

  LogList.Clear;
end;

procedure TLogException.ShowList(Title: string);
begin
  if ListContainsExceptions then
    DoShowStringList(LogList, Title);
end;

procedure TLogException.DeleteList;
begin
  if assigned(LogList) then
  begin
    LogList.Destroy;
    LogList := nil;
  end;
end;

function TLogException.ListContainsExceptions: boolean;
begin
  result := False;
  if assigned(LogList) then
    result := (LogList.Count > 0);
end;

constructor TLogException.Create;
begin
  inherited;
end;

destructor TLogException.Destroy;
begin
  DeleteList;
  inherited;
end;

function TLogException.Description(ExceptionType: string): string;
var
  I: integer;
begin
  for I := 1 to NumExceptionDescriptions do
    if ExceptionType = ExceptionDescriptions[I].T then
    begin
      result := ExceptionDescriptions[I].D;
      exit;
    end;

  result := 'Unknown exception';
end;

initialization
  LogException := TLogException.Create;

finalization
  LogException.Free;
end.
