object evStatisticsViewerFrm: TevStatisticsViewerFrm
  Left = 0
  Top = 0
  Width = 720
  Height = 560
  AutoScroll = False
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 0
    Top = 368
    Width = 720
    Height = 7
    Cursor = crVSplit
    Align = alBottom
  end
  object Splitter2: TSplitter
    Left = 389
    Top = 60
    Width = 9
    Height = 308
    Align = alRight
  end
  object Label3: TLabel
    Left = 0
    Top = 42
    Width = 720
    Height = 18
    Align = alTop
    AutoSize = False
    Caption = 'Events'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pnlFileList: TPanel
    Left = 0
    Top = 0
    Width = 720
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object Label1: TLabel
      Left = 0
      Top = 14
      Width = 71
      Height = 13
      Caption = 'Statistic File'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbDates: TevDBComboBox
      Left = 81
      Top = 12
      Width = 247
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
      OnChange = cbDatesChange
    end
  end
  object tvEvents: TTreeView
    Left = 0
    Top = 60
    Width = 389
    Height = 308
    Align = alClient
    HideSelection = False
    Indent = 19
    ReadOnly = True
    TabOrder = 1
    OnChange = tvEventsChange
  end
  object Panel2: TPanel
    Left = 0
    Top = 375
    Width = 720
    Height = 185
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TLabel
      Left = 0
      Top = 0
      Width = 720
      Height = 17
      Align = alTop
      AutoSize = False
      Caption = 'Event Info'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object mEventInfo: TMemo
      Left = 0
      Top = 17
      Width = 720
      Height = 168
      Align = alClient
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object pnlChart: TPanel
    Left = 398
    Top = 60
    Width = 322
    Height = 308
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter3: TSplitter
      Left = 0
      Top = 181
      Width = 322
      Height = 7
      Cursor = crVSplit
      Align = alBottom
    end
    object Chart: TChart
      Left = 0
      Top = 0
      Width = 322
      Height = 181
      AllowPanning = pmNone
      AllowZoom = False
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      BackWall.Pen.Visible = False
      Title.Text.Strings = (
        'Event summary')
      AxisVisible = False
      ClipPoints = False
      Frame.Visible = False
      Legend.Alignment = laBottom
      View3D = False
      View3DOptions.Elevation = 315
      View3DOptions.Orthogonal = False
      View3DOptions.Perspective = 0
      View3DOptions.Rotation = 360
      View3DWalls = False
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Series1: TPieSeries
        Marks.ArrowLength = 8
        Marks.Visible = False
        SeriesColor = clRed
        Title = 'Statistics'
        Circled = True
        OtherSlice.Text = 'Other'
        PieValues.DateTime = False
        PieValues.Name = 'Pie'
        PieValues.Multiplier = 1.000000000000000000
        PieValues.Order = loNone
      end
    end
    object lvSummary: TListView
      Left = 0
      Top = 188
      Width = 322
      Height = 120
      Align = alBottom
      Columns = <
        item
          Caption = 'Item'
          Width = 150
        end
        item
          AutoSize = True
          Caption = 'Value'
        end>
      ColumnClick = False
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ShowColumnHeaders = False
      TabOrder = 1
      ViewStyle = vsReport
    end
  end
end
