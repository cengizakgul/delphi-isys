inherited evVersionedCoAddress: TevVersionedCoAddress
  Left = 342
  Top = 321
  ClientHeight = 556
  ClientWidth = 989
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 989
    Height = 297
    inherited grFieldData: TevDBGrid
      Width = 941
      Height = 170
    end
    inherited pnlEdit: TevPanel
      Top = 214
      Width = 941
      inherited pnlButtons: TevPanel
        Left = 632
      end
    end
  end
  inherited pnlBottom: TevPanel
    Top = 297
    Width = 989
    Height = 259
    inherited isUIFashionPanel1: TisUIFashionPanel
      Height = 259
      Title = 'Company Address'
      inherited lAddress1: TevLabel
        Left = 21
        Top = 113
      end
      inherited lAddress2: TevLabel
        Left = 21
        Top = 152
      end
      inherited lCity: TevLabel
        Top = 191
      end
      inherited lState: TevLabel
        Left = 164
        Top = 191
      end
      inherited lZipCode: TevLabel
        Left = 215
        Top = 191
      end
      object lName: TevLabel [5]
        Left = 20
        Top = 35
        Width = 28
        Height = 13
        Caption = 'Name'
        FocusControl = edName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lDBA: TevLabel [6]
        Left = 20
        Top = 74
        Width = 22
        Height = 13
        Caption = 'DBA'
        FocusControl = edDBA
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      inherited edAddress1: TevDBEdit
        Left = 21
        Top = 128
        TabOrder = 2
      end
      inherited edAddress2: TevDBEdit
        Left = 21
        Top = 167
        TabOrder = 3
      end
      inherited edCity: TevDBEdit
        Top = 206
        TabOrder = 4
      end
      inherited edState: TevDBEdit
        Left = 164
        Top = 206
        CharCase = ecUpperCase
        TabOrder = 5
      end
      inherited edZipCode: TevDBEdit
        Left = 215
        Top = 206
        TabOrder = 6
      end
      object edName: TevDBEdit
        Left = 20
        Top = 50
        Width = 270
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edDBA: TevDBEdit
        Left = 20
        Top = 89
        Width = 270
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
