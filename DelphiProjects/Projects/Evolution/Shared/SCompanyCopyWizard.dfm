inherited CompanyCopyWizard: TCompanyCopyWizard
  Left = 598
  Top = 181
  Caption = 'Copy Wizard - '
  ClientHeight = 597
  ClientWidth = 908
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TevPanel
    Width = 908
    Height = 597
    Caption = ''
    inherited Panel2: TevPanel
      Top = 547
      Width = 908
      inherited btBack: TevBitBtn
        Left = 735
        ModalResult = 0
      end
      inherited btNext: TevBitBtn
        Left = 815
      end
      inherited btCancel: TevBitBtn
        Left = 654
      end
    end
    inherited Panel4: TevPanel
      Width = 908
      Height = 547
      Caption = ''
      inherited pctCopyWizard: TevPageControl
        Width = 906
        Height = 545
        ActivePage = tshCO
        inherited tshSelectBase: TTabSheet
          inherited evSplitter1: TevSplitter
            Left = 313
            Height = 514
          end
          inherited evPanel1: TevPanel
            Width = 313
            Height = 514
            inherited gdClient: TevDBGrid
              Width = 313
              Height = 514
              IniAttributes.SectionName = 'TCompanyCopyWizard\gdClient'
            end
          end
          inherited gdCompany: TevDBGrid
            Left = 316
            Width = 582
            Height = 514
            IniAttributes.SectionName = 'TCompanyCopyWizard\gdCompany'
          end
        end
        object tshCO: TTabSheet
          Caption = 'tshCO'
          ImageIndex = 1
          object lblCOxxCUSTOM_COMPANY_NUMBER: TevLabel
            Tag = 1
            Left = 8
            Top = 152
            Width = 81
            Height = 16
            Caption = '~Company Code'
            FocusControl = COxxCUSTOM_COMPANY_NUMBER
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxNAME: TevLabel
            Tag = 3
            Left = 8
            Top = 192
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = COxxNAME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxADDRESS1: TevLabel
            Tag = 4
            Left = 8
            Top = 232
            Width = 56
            Height = 16
            Caption = '~Address 1'
            FocusControl = COxxADDRESS1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxADDRESS2: TevLabel
            Tag = 5
            Left = 8
            Top = 272
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = COxxADDRESS2
          end
          object lblCOxxCITY: TevLabel
            Tag = 6
            Left = 8
            Top = 312
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = COxxCITY
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxSTATE: TevLabel
            Tag = 7
            Left = 156
            Top = 312
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = COxxSTATE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxZIP_CODE: TevLabel
            Tag = 8
            Left = 200
            Top = 312
            Width = 52
            Height = 16
            Caption = '~Zip Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxSTART_DATE: TevLabel
            Tag = 2
            Left = 160
            Top = 152
            Width = 57
            Height = 16
            Caption = '~Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxLEGAL_NAME: TevLabel
            Left = 456
            Top = 160
            Width = 57
            Height = 13
            Caption = 'Legal Name'
            Visible = False
          end
          object lblCOxxLEGAL_ADDRESS1: TevLabel
            Left = 456
            Top = 176
            Width = 76
            Height = 13
            Caption = 'Legal Address 1'
            Visible = False
          end
          object lblCOxxLEGAL_ADDRESS2: TevLabel
            Left = 456
            Top = 192
            Width = 76
            Height = 13
            Caption = 'Legal Address 2'
            Visible = False
          end
          object lblCOxxLEGAL_CITY: TevLabel
            Left = 456
            Top = 208
            Width = 46
            Height = 13
            Caption = 'Legal City'
            Visible = False
          end
          object lblCOxxLEGAL_STATE: TevLabel
            Left = 506
            Top = 208
            Width = 25
            Height = 13
            Caption = 'State'
            Visible = False
          end
          object lblCOxxLEGAL_ZIP_CODE: TevLabel
            Left = 536
            Top = 208
            Width = 72
            Height = 13
            Caption = 'Legal Zip Code'
            Visible = False
          end
          object lblCOxxFEIN: TevLabel
            Tag = 9
            Left = 296
            Top = 160
            Width = 67
            Height = 16
            Caption = '~EIN Number'
            FocusControl = COxxFEIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxBUSINESS_START_DATE: TevLabel
            Tag = 10
            Left = 413
            Top = 160
            Width = 104
            Height = 16
            Caption = '~Evolution Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCOxxTAX_SERVICE_START_DATE: TevLabel
            Tag = 11
            Left = 296
            Top = 200
            Width = 108
            Height = 13
            Caption = 'Tax Service Start Date'
          end
          object lblCOxxPIN_NUMBER: TevLabel
            Left = 456
            Top = 224
            Width = 58
            Height = 13
            Caption = 'PIN Number'
            Visible = False
          end
          object lblCOxxCL_COMMON_PAYMASTER_NBR: TevLabel
            Left = 328
            Top = 440
            Width = 204
            Height = 13
            Caption = 'lblCOxxCL_COMMON_PAYMASTER_NBR'
            Visible = False
          end
          object lblCOxxCL_CO_CONSOLIDATION_NBR: TevLabel
            Left = 328
            Top = 456
            Width = 192
            Height = 13
            Caption = 'lblCOxxCL_CO_CONSOLIDATION_NBR'
            Visible = False
          end
          object grCO: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
              'NAME'#9'25'#9'Name'#9'F'
              'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
              'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
              'FEIN'#9'9'#9'Fein'#9
              'DBA'#9'40'#9'Dba'#9'F'
              'UserFirstName'#9'20'#9'CSR First Name'#9'F'
              'UserLastName'#9'30'#9'CSR Last Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\grCO'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCO
            TabOrder = 13
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object COxxCUSTOM_COMPANY_NUMBER: TevDBEdit
            Left = 8
            Top = 168
            Width = 124
            Height = 21
            HelpContext = 10502
            DataField = 'CUSTOM_COMPANY_NUMBER'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxNAME: TevDBEdit
            Left = 8
            Top = 208
            Width = 265
            Height = 21
            HelpContext = 10503
            DataField = 'NAME'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxADDRESS1: TevDBEdit
            Left = 8
            Top = 248
            Width = 265
            Height = 21
            HelpContext = 10659
            DataField = 'ADDRESS1'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxADDRESS2: TevDBEdit
            Left = 8
            Top = 288
            Width = 265
            Height = 21
            HelpContext = 10659
            DataField = 'ADDRESS2'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxCITY: TevDBEdit
            Left = 8
            Top = 328
            Width = 137
            Height = 21
            HelpContext = 10659
            DataField = 'CITY'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxSTATE: TevDBEdit
            Left = 156
            Top = 328
            Width = 33
            Height = 21
            HelpContext = 10659
            DataField = 'STATE'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            TabOrder = 6
            UnboundDataType = wwDefault
            UsePictureMask = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxZIP_CODE: TevDBEdit
            Left = 200
            Top = 328
            Width = 72
            Height = 21
            HelpContext = 10659
            DataField = 'ZIP_CODE'
            DataSource = dsCO
            Picture.PictureMask = '*5{#,?}[-*4{#,?}]'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxSTART_DATE: TevDBDateTimePicker
            Left = 160
            Top = 168
            Width = 81
            Height = 21
            HelpContext = 10512
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'START_DATE'
            DataSource = dsCO
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object COxxFEIN: TevDBEdit
            Left = 296
            Top = 176
            Width = 81
            Height = 21
            HelpContext = 10527
            DataField = 'FEIN'
            DataSource = dsCO
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*9{#}'
            Picture.AutoFill = False
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object COxxBUSINESS_START_DATE: TevDBDateTimePicker
            Left = 413
            Top = 176
            Width = 97
            Height = 21
            HelpContext = 10561
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'BUSINESS_START_DATE'
            DataSource = dsCO
            Epoch = 1950
            ShowButton = True
            TabOrder = 9
          end
          object COxxTAX_SERVICE_START_DATE: TevDBDateTimePicker
            Left = 296
            Top = 216
            Width = 81
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TAX_SERVICE_START_DATE'
            DataSource = dsCO
            Epoch = 1950
            ShowButton = True
            TabOrder = 10
          end
          object evGroupBox1: TevGroupBox
            Left = 296
            Top = 248
            Width = 233
            Height = 185
            Caption = 'Cobra'
            TabOrder = 11
            object lblCOxxCOBRA_FEE_DAY_OF_MONTH_DUE: TevLabel
              Tag = 13
              Left = 16
              Top = 96
              Width = 72
              Height = 13
              Caption = 'Cobra Fee Due'
            end
            object lblCOxxCOBRA_NOTIFICATION_DAYS: TevLabel
              Tag = 14
              Left = 136
              Top = 96
              Width = 80
              Height = 13
              Caption = 'Notification Days'
            end
            object lblCOxxCOBRA_ELIGIBILITY_CONFIRM_DAYS: TevLabel
              Tag = 15
              Left = 16
              Top = 136
              Width = 98
              Height = 13
              Caption = 'Eligible Confirm Days'
            end
            object lblCOxxSS_DISABILITY_ADMIN_FEE: TevLabel
              Tag = 16
              Left = 136
              Top = 136
              Width = 79
              Height = 13
              Caption = 'SS Disability Fee'
            end
            object lblCOxxCHARGE_COBRA_ADMIN_FEE: TevLabel
              Tag = 12
              Left = 32
              Top = 12
              Width = 96
              Height = 16
              Caption = '~Charge Admin Fee'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Visible = False
            end
            object evdgChargeAdminFee: TevDBRadioGroup
              Left = 16
              Top = 24
              Width = 201
              Height = 57
              Caption = '~Charge Admin Fee'
              Columns = 2
              DataField = 'CHARGE_COBRA_ADMIN_FEE'
              DataSource = dsCO
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 0
              Values.Strings = (
                'Y'
                'N')
            end
            object COxxCOBRA_FEE_DAY_OF_MONTH_DUE: TevDBSpinEdit
              Left = 16
              Top = 112
              Width = 81
              Height = 21
              Increment = 1.000000000000000000
              DataField = 'COBRA_FEE_DAY_OF_MONTH_DUE'
              DataSource = dsCO
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object COxxCOBRA_NOTIFICATION_DAYS: TevDBSpinEdit
              Left = 136
              Top = 112
              Width = 81
              Height = 21
              Increment = 1.000000000000000000
              DataField = 'COBRA_NOTIFICATION_DAYS'
              DataSource = dsCO
              TabOrder = 2
              UnboundDataType = wwDefault
            end
            object COxxCOBRA_ELIGIBILITY_CONFIRM_DAYS: TevDBSpinEdit
              Left = 16
              Top = 152
              Width = 81
              Height = 21
              Increment = 1.000000000000000000
              DataField = 'COBRA_ELIGIBILITY_CONFIRM_DAYS'
              DataSource = dsCO
              TabOrder = 3
              UnboundDataType = wwDefault
            end
            object COxxSS_DISABILITY_ADMIN_FEE: TevDBEdit
              Left = 136
              Top = 152
              Width = 81
              Height = 21
              DataField = 'SS_DISABILITY_ADMIN_FEE'
              DataSource = dsCO
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object evButton1: TevBitBtn
            Left = 8
            Top = 360
            Width = 265
            Height = 33
            Caption = 'Get Address Info from Client'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 12
            OnClick = evButton1Click
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFFFFFFF0444444444400000888888888888888804444474447F
              FFF0888888D888DDDDD80F44477447FFFFF08D888DD88DDDDDD80FF474447888
              88F08DD8D888D88888D80FFF77787FFFFFF08DDD8DD8DDDDDDD80FF477478788
              88F08DD8DD8D8D8888D80F47777778FFFFF08D8DDDDDD8DDDDD80447777778FF
              88F0888DDDDDD8DD88D804447474747FFFF08888D8D8D8DDDDD804447777747F
              FFF08888DDDDD8DDDDD804444777447FFF0888888DDD88DDDD8DD44444444000
              008DD8888888888888DDD44444448DDDDDDDD8888888DDDDDDDDDD444448DDDD
              DDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
          object grbHR: TevGroupBox
            Left = 624
            Top = 160
            Width = 225
            Height = 211
            Caption = 'HR'
            TabOrder = 14
            object lblCOxxENABLE_HR: TevLabel
              Left = 12
              Top = 19
              Width = 25
              Height = 16
              Caption = '~HR'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object lblCOxxHCM_URL: TevLabel
              Left = 12
              Top = 163
              Width = 40
              Height = 13
              Caption = 'HCM Url'
            end
            object COxxADVANCED_HR_CORE: TevDBCheckBox
              Left = 12
              Top = 64
              Width = 85
              Height = 17
              Caption = 'Core HR'
              DataField = 'ADVANCED_HR_CORE'
              DataSource = dsCO
              Enabled = False
              TabOrder = 0
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object COxxADVANCED_HR_ATS: TevDBCheckBox
              Left = 12
              Top = 88
              Width = 117
              Height = 17
              Caption = 'Applicant Tracking'
              DataField = 'ADVANCED_HR_ATS'
              DataSource = dsCO
              Enabled = False
              TabOrder = 1
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object COxxADVANCED_HR_TALENT_MGMT: TevDBCheckBox
              Left = 12
              Top = 112
              Width = 133
              Height = 17
              Caption = 'Talent Management'
              DataField = 'ADVANCED_HR_TALENT_MGMT'
              DataSource = dsCO
              Enabled = False
              TabOrder = 2
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object COxxADVANCED_HR_ONLINE_ENROLLMENT: TevDBCheckBox
              Left = 12
              Top = 136
              Width = 144
              Height = 17
              Caption = 'Online Benefit Enrollment'
              DataField = 'ADVANCED_HR_ONLINE_ENROLLMENT'
              DataSource = dsCO
              Enabled = False
              TabOrder = 3
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object COxxENABLE_HR: TevDBComboBox
              Left = 12
              Top = 34
              Width = 142
              Height = 21
              HelpContext = 10568
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ENABLE_HR'
              DataSource = dsCO
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'No'#9'N'
                'Remote HR'#9'R'
                'Web HR'#9'Y'
                'Benefits'#9'B'
                'Advanced HR'#9'A'
                'AHR Employee Portal Only'#9'O')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 4
              UnboundDataType = wwDefault
              OnChange = COxxENABLE_HRChange
            end
            object COxxHCM_URL: TevDBEdit
              Left = 13
              Top = 180
              Width = 200
              Height = 21
              HelpContext = 10610
              DataField = 'HCM_URL'
              DataSource = dsCO
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              OnChange = COxxENABLE_HRChange
              Glowing = False
            end
          end
          object evGroupBox3: TevGroupBox
            Left = 624
            Top = 378
            Width = 225
            Height = 116
            Caption = 'Analytics'
            TabOrder = 15
            object evLabel69: TevLabel
              Left = 12
              Top = 59
              Width = 80
              Height = 16
              Caption = '~Analytics Level'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object COxxENABLE_ANALYTICS: TevDBRadioGroup
              Left = 12
              Top = 19
              Width = 142
              Height = 37
              HelpContext = 10604
              Caption = '~Include in Analytics'
              Columns = 2
              DataField = 'ENABLE_ANALYTICS'
              DataSource = dsCO
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 0
              Values.Strings = (
                'Y'
                'N')
              OnChange = COxxENABLE_ANALYTICSChange
            end
            object COxxANALYTICS_LICENSE: TevDBComboBox
              Left = 12
              Top = 76
              Width = 142
              Height = 21
              HelpContext = 18020
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ANALYTICS_LICENSE'
              DataSource = dsCO
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'N/A'#9'N'
                'Basic'#9'1')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
          end
        end
        object tshCO_ADDITIONAL_INFO_NAMES: TTabSheet
          Caption = 'tshCO_ADDITIONAL_INFO_NAMES'
          ImageIndex = 3
          object lblCO_ADDITIONAL_INFO_NAMESxxNAME: TevLabel
            Left = 9
            Top = 151
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = CO_ADDITIONAL_INFO_NAMESxxNAME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CO_ADDITIONAL_INFO_NAMESxxNAME: TevDBEdit
            Left = 8
            Top = 168
            Width = 244
            Height = 21
            HelpContext = 14001
            DataField = 'NAME'
            DataSource = dsCO_ADDITIONAL_INFO_NAMES
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object grInfoNames: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\grInfoNames'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCO_ADDITIONAL_INFO_NAMES
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object tshCO_LOCAL_TAX: TTabSheet
          Caption = 'tshCO_LOCAL_TAX'
          ImageIndex = 4
          object lblCO_LOCAL_TAXxxLOCAL_EIN_NUMBER: TevLabel
            Tag = 2
            Left = 8
            Top = 152
            Width = 67
            Height = 16
            Caption = '~EIN Number'
            FocusControl = CO_LOCAL_TAXxxLOCAL_EIN_NUMBER
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_LOCAL_TAXxxEFT_NAME: TevLabel
            Tag = 3
            Left = 8
            Top = 200
            Width = 141
            Height = 13
            Caption = 'Override Company EFT Name'
            FocusControl = CO_LOCAL_TAXxxEFT_NAME
          end
          object lblCO_LOCAL_TAXxxEFT_PIN_NUMBER: TevLabel
            Tag = 4
            Left = 8
            Top = 248
            Width = 81
            Height = 13
            Caption = 'EFT PIN Number'
            FocusControl = CO_LOCAL_TAXxxEFT_PIN_NUMBER
          end
          object lblCO_LOCAL_TAXxxEFT_STATUS: TevLabel
            Tag = 5
            Left = 8
            Top = 296
            Width = 105
            Height = 13
            Caption = 'EFT Enrollment Status'
            FocusControl = CO_LOCAL_TAXxxEFT_STATUS
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_LOCAL_TAXxxFINAL_TAX_RETURN: TevLabel
            Tag = 6
            Left = 248
            Top = 144
            Width = 211
            Height = 13
            Caption = 'lblCO_LOCAL_TAXxxFINAL_TAX_RETURN'
            Visible = False
          end
          object lblCO_LOCAL_TAXxxFILLER: TevLabel
            Left = 464
            Top = 208
            Width = 138
            Height = 13
            Caption = 'lblCO_LOCAL_TAXxxFILLER'
            Visible = False
          end
          object lblCO_LOCAL_TAXxxLocalName: TevLabel
            Tag = 1
            Left = 128
            Top = 152
            Width = 57
            Height = 13
            Caption = 'Local Name'
            Visible = False
            WordWrap = True
          end
          object grLocalTax: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'LocalName'#9'40'#9'Local'#9'F'
              'localState'#9'2'#9'State'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\grLocalTax'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCO_LOCAL_TAX
            TabOrder = 5
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object CO_LOCAL_TAXxxLOCAL_EIN_NUMBER: TevDBEdit
            Left = 8
            Top = 168
            Width = 94
            Height = 21
            HelpContext = 14504
            DataField = 'LOCAL_EIN_NUMBER'
            DataSource = dsCO_LOCAL_TAX
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_LOCAL_TAXxxEFT_NAME: TevDBEdit
            Left = 8
            Top = 216
            Width = 193
            Height = 21
            HelpContext = 14506
            DataField = 'EFT_NAME'
            DataSource = dsCO_LOCAL_TAX
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_LOCAL_TAXxxEFT_PIN_NUMBER: TevDBEdit
            Left = 8
            Top = 264
            Width = 89
            Height = 21
            HelpContext = 14507
            DataField = 'EFT_PIN_NUMBER'
            DataSource = dsCO_LOCAL_TAX
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_LOCAL_TAXxxEFT_STATUS: TevDBComboBox
            Left = 8
            Top = 312
            Width = 153
            Height = 21
            HelpContext = 14508
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EFT_STATUS'
            DataSource = dsCO_LOCAL_TAX
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object CO_LOCAL_TAXxxFINAL_TAX_RETURN: TevDBRadioGroup
            Left = 248
            Top = 160
            Width = 225
            Height = 49
            HelpContext = 14516
            Caption = '~Final Tax Return'
            Columns = 2
            DataField = 'FINAL_TAX_RETURN'
            DataSource = dsCO_LOCAL_TAX
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object tshCO_PHONE: TTabSheet
          Caption = 'tshCO_PHONE'
          ImageIndex = 7
          OnShow = tshCO_PHONEShow
          object lblCO_PHONExxCONTACT: TevLabel
            Left = 8
            Top = 152
            Width = 37
            Height = 13
            Caption = 'Contact'
            FocusControl = CO_PHONExxCONTACT
          end
          object lblCO_PHONExxPHONE_NUMBER: TevLabel
            Left = 8
            Top = 200
            Width = 80
            Height = 16
            Caption = '~Phone Number'
            FocusControl = CO_PHONExxPHONE_NUMBER
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_PHONExxPHONE_TYPE: TevLabel
            Left = 136
            Top = 200
            Width = 67
            Height = 16
            Caption = '~Phone Type'
            FocusControl = CO_PHONExxPHONE_TYPE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_PHONExxDESCRIPTION: TevLabel
            Left = 8
            Top = 248
            Width = 53
            Height = 13
            Caption = 'Description'
            FocusControl = CO_PHONExxDESCRIPTION
          end
          object grPhone: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'PHONE_NUMBER'#9'20'#9'Phone Number'#9'F'
              'CONTACT'#9'30'#9'Contact'#9'F'
              'PHONE_TYPE'#9'1'#9'Phone Type'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\grPhone'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCO_PHONE
            TabOrder = 4
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object CO_PHONExxCONTACT: TevDBEdit
            Left = 8
            Top = 168
            Width = 233
            Height = 21
            DataField = 'CONTACT'
            DataSource = dsCO_PHONE
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_PHONExxPHONE_NUMBER: TevDBEdit
            Left = 8
            Top = 216
            Width = 121
            Height = 21
            DataField = 'PHONE_NUMBER'
            DataSource = dsCO_PHONE
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_PHONExxPHONE_TYPE: TevDBComboBox
            Left = 136
            Top = 216
            Width = 105
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            AutoSize = False
            DataField = 'PHONE_TYPE'
            DataSource = dsCO_PHONE
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Primary Phone'#9'P'
              'Phone 2'#9'2'
              'Pager'#9'G'
              'Fax'#9'F'
              'Cell Phone'#9'C'
              'Emergency Phone'#9'E'
              'After Hours'#9'A'
              'None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object CO_PHONExxDESCRIPTION: TevDBEdit
            Left = 8
            Top = 264
            Width = 233
            Height = 21
            DataField = 'DESCRIPTION'
            DataSource = dsCO_PHONE
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object tshCO_STATES: TTabSheet
          Caption = 'tshCO_STATES'
          ImageIndex = 12
          object lblCO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR: TevLabel
            Tag = 4
            Left = 7
            Top = 232
            Width = 126
            Height = 16
            Caption = '~State Deposit Frequency'
            FocusControl = CO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSTATE_EIN: TevLabel
            Tag = 2
            Left = 8
            Top = 152
            Width = 55
            Height = 16
            Caption = '~State EIN'
            FocusControl = CO_STATESxxSTATE_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSTATE_SDI_EIN: TevLabel
            Tag = 3
            Left = 8
            Top = 192
            Width = 76
            Height = 16
            Caption = '~State SDI EIN'
            FocusControl = CO_STATESxxSTATE_SDI_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSTATE_EFT_ENROLLMENT_STATUS: TevLabel
            Tag = 5
            Left = 8
            Top = 272
            Width = 142
            Height = 16
            Caption = '~State EFT Enrollment Status'
            FocusControl = CO_STATESxxSTATE_EFT_ENROLLMENT_STATUS
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSTATE_EFT_PIN_NUMBER: TevLabel
            Tag = 6
            Left = 8
            Top = 312
            Width = 109
            Height = 13
            Caption = 'State EFT PIN Number'
            FocusControl = CO_STATESxxSTATE_EFT_PIN_NUMBER
          end
          object lblCO_STATESxxSTATE_EFT_EIN: TevLabel
            Tag = 7
            Left = 8
            Top = 352
            Width = 69
            Height = 13
            Caption = 'State EFT EIN'
          end
          object lblCO_STATESxxSTATE_SDI_EFT_EIN: TevLabel
            Tag = 8
            Left = 8
            Top = 392
            Width = 90
            Height = 13
            Caption = 'State SDI EFT EIN'
          end
          object lblCO_STATESxxSUI_EIN: TevLabel
            Tag = 9
            Left = 295
            Top = 152
            Width = 48
            Height = 16
            Caption = '~SUI EIN'
            FocusControl = CO_STATESxxSUI_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSUI_EFT_PIN_NUMBER: TevLabel
            Tag = 12
            Left = 295
            Top = 272
            Width = 102
            Height = 13
            Caption = 'SUI EFT PIN Number'
          end
          object lblCO_STATESxxSUI_EFT_NAME: TevLabel
            Tag = 10
            Left = 295
            Top = 192
            Width = 72
            Height = 13
            Caption = 'SUI EFT Name'
            FocusControl = CO_STATESxxSUI_EFT_NAME
          end
          object lblCO_STATESxxSUI_EFT_ENROLLMENT_STATUS: TevLabel
            Tag = 11
            Left = 295
            Top = 232
            Width = 135
            Height = 16
            Caption = '~SUI EFT Enrollment Status'
            FocusControl = CO_STATESxxSUI_EFT_ENROLLMENT_STATUS
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCO_STATESxxSUI_EFT_EIN: TevLabel
            Tag = 13
            Left = 295
            Top = 312
            Width = 62
            Height = 13
            Caption = 'SUI EFT EIN'
          end
          object lblCO_STATESxxSTATE: TevLabel
            Tag = 1
            Left = 184
            Top = 152
            Width = 25
            Height = 13
            Caption = 'State'
            Visible = False
            WordWrap = True
          end
          object lblCO_STATESxxTaxCollectionDistrict: TevLabel
            Left = 295
            Top = 352
            Width = 108
            Height = 13
            Caption = 'Tax Collection District  '
          end
          object lblCO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR: TevLabel
            Left = 296
            Top = 392
            Width = 123
            Height = 13
            Caption = 'TCD Deposit Frequency   '
          end
          object lblCO_STATESxxTCD_PAYMENT_METHOD: TevLabel
            Left = 496
            Top = 384
            Width = 114
            Height = 13
            Caption = 'TCD Payment Method   '
          end
          object grStates: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'STATE'#9'2'#9'State'#9'F'
              'Name'#9'20'#9'State Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\grStates'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnRowChanged = grStatesRowChanged
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCO_STATES
            TabOrder = 12
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object CO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR: TevDBLookupCombo
            Left = 7
            Top = 248
            Width = 162
            Height = 21
            HelpContext = 11006
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION')
            DataField = 'SY_STATE_DEPOSIT_FREQ_NBR'
            DataSource = dsCO_STATES
            LookupField = 'SY_STATE_DEPOSIT_FREQ_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object CO_STATESxxSTATE_EIN: TevDBEdit
            Left = 8
            Top = 168
            Width = 162
            Height = 21
            HelpContext = 11002
            DataField = 'STATE_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSTATE_SDI_EIN: TevDBEdit
            Left = 8
            Top = 208
            Width = 162
            Height = 21
            HelpContext = 11003
            DataField = 'STATE_SDI_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSTATE_EFT_ENROLLMENT_STATUS: TevDBComboBox
            Left = 8
            Top = 288
            Width = 244
            Height = 21
            HelpContext = 11025
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATE_EFT_ENROLLMENT_STATUS'
            DataSource = dsCO_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object CO_STATESxxSTATE_EFT_PIN_NUMBER: TevDBEdit
            Left = 8
            Top = 328
            Width = 244
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_EFT_PIN_NUMBER'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSTATE_EFT_EIN: TevDBEdit
            Left = 8
            Top = 368
            Width = 244
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_EFT_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSTATE_SDI_EFT_EIN: TevDBEdit
            Left = 8
            Top = 408
            Width = 244
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_SDI_EFT_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSUI_EIN: TevDBEdit
            Left = 295
            Top = 168
            Width = 162
            Height = 21
            HelpContext = 11004
            DataField = 'SUI_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSUI_EFT_NAME: TevDBEdit
            Left = 295
            Top = 208
            Width = 162
            Height = 21
            HelpContext = 11027
            DataField = 'SUI_EFT_NAME'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSUI_EFT_ENROLLMENT_STATUS: TevDBComboBox
            Left = 295
            Top = 248
            Width = 162
            Height = 21
            HelpContext = 11028
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SUI_EFT_ENROLLMENT_STATUS'
            DataSource = dsCO_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object CO_STATESxxSUI_EFT_PIN_NUMBER: TevDBEdit
            Left = 295
            Top = 288
            Width = 162
            Height = 21
            HelpContext = 11029
            DataField = 'SUI_EFT_PIN_NUMBER'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxSUI_EFT_EIN: TevDBEdit
            Left = 295
            Top = 328
            Width = 162
            Height = 21
            HelpContext = 11026
            DataField = 'SUI_EFT_EIN'
            DataSource = dsCO_STATES
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CO_STATESxxTaxCollectionDistrict: TevDBLookupCombo
            Left = 295
            Top = 368
            Width = 186
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'Agency Name'#9'F')
            DataField = 'TaxCollectionDistrict'
            DataSource = dsCO_STATES
            LookupField = 'SY_GLOBAL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 13
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR: TevDBLookupCombo
            Left = 296
            Top = 408
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F')
            DataField = 'TCD_DEPOSIT_FREQUENCY_NBR'
            DataSource = dsCO_STATES
            LookupField = 'SY_AGENCY_DEPOSIT_FREQ_NBR'
            Style = csDropDownList
            TabOrder = 14
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnEnter = CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBREnter
          end
          object CO_STATESxxTCD_PAYMENT_METHOD: TevDBComboBox
            Left = 496
            Top = 403
            Width = 119
            Height = 21
            HelpContext = 11025
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TCD_PAYMENT_METHOD'
            DataSource = dsCO_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EFT Credit'#9'C'
              'EFT Debit'#9'D'
              'Check'#9'H'
              'Mark as Paid'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 15
            UnboundDataType = wwDefault
          end
        end
        object tshEndPage: TTabSheet
          Caption = 'tshEndPage'
          ImageIndex = 6
        end
        object tsEmployees: TTabSheet
          Caption = 'Employees'
          ImageIndex = 7
          object gEmployees: TevDBGrid
            Left = 0
            Top = 0
            Width = 898
            Height = 514
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F'
              'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
              'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
              'Employee_MI_Calculate'#9'2'#9'MI'#9'F'
              'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
              'CURRENT_TERMINATION_CODE_DESC'#9'40'#9'Status'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCompanyCopyWizard\gEmployees'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsEmployees
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited dsClient: TevDataSource
    OnDataChange = dsClientDataChange
    Left = 72
    Top = 108
  end
  inherited TmpDS: TevClientDataSet
    Left = 541
    Top = 132
  end
  inherited dsCompany: TevDataSource
    Left = 149
    Top = 84
  end
  object dsCO: TevDataSource
    Left = 333
    Top = 76
  end
  object dsCO_ADDITIONAL_INFO_NAMES: TevDataSource
    Left = 53
    Top = 484
  end
  object dsCO_LOCAL_TAX: TevDataSource
    Left = 293
    Top = 44
  end
  object dsCO_STATES: TevDataSource
    OnDataChange = dsCO_STATESDataChange
    Left = 189
    Top = 460
  end
  object dsCO_PHONE: TevDataSource
    Left = 205
    Top = 484
  end
  object EE: TevClientDataSet
    ProviderName = 'EE_PROV'
    AggregatesActive = True
    PictureMasks.Strings = (
      'STATE'#9'*{&,@}'#9'T'#9'T')
    OnCalcFields = EECalcFields
    Left = 730
    Top = 33
    object EEEmployee_LastName_Calculate: TStringField
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'Employee_LastName_Calculate'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'LAST_NAME'
      KeyFields = 'CL_PERSON_NBR'
      Lookup = True
    end
    object EEEmployee_FirstName_Calculate: TStringField
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'Employee_FirstName_Calculate'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'FIRST_NAME'
      KeyFields = 'CL_PERSON_NBR'
      Lookup = True
    end
    object EEEmployee_MI_Calculate: TStringField
      FieldKind = fkLookup
      FieldName = 'Employee_MI_Calculate'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'MIDDLE_INITIAL'
      KeyFields = 'CL_PERSON_NBR'
      Size = 1
      Lookup = True
    end
    object EECURRENT_TERMINATION_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'CURRENT_TERMINATION_CODE'
      Origin = '"EE_HIST"."CURRENT_TERMINATION_CODE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EECL_PERSON_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_PERSON_NBR'
      Origin = '"EE_HIST"."CL_PERSON_NBR"'
      Required = True
      Visible = False
    end
    object EECURRENT_TERMINATION_CODE_DESC: TStringField
      DisplayWidth = 80
      FieldKind = fkInternalCalc
      FieldName = 'CURRENT_TERMINATION_CODE_DESC'
      Size = 80
    end
    object EEEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
      Origin = '"EE_HIST"."EE_NBR"'
      Required = True
      Visible = False
    end
    object EESOCIAL_SECURITY_NUMBER: TStringField
      DisplayWidth = 11
      FieldKind = fkLookup
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'SOCIAL_SECURITY_NUMBER'
      KeyFields = 'CL_PERSON_NBR'
      Visible = False
      Size = 11
      Lookup = True
    end
    object EECUSTOM_EMPLOYEE_NUMBER: TStringField
      Alignment = taRightJustify
      DisplayWidth = 6
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Origin = '"EE_HIST"."CUSTOM_EMPLOYEE_NUMBER"'
      Required = True
      Visible = False
      Size = 9
    end
    object EECO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 737
    Top = 77
  end
  object dsEmployees: TevDataSource
    DataSet = EE
    Left = 765
    Top = 36
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 773
    Top = 132
  end
end
