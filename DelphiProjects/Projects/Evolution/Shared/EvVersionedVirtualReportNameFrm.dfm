inherited evVersionedVirtualReportName: TevVersionedVirtualReportName
  Left = 468
  Top = 317
  ClientHeight = 366
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 366
    inherited grFieldData: TevDBGrid
      Height = 239
    end
    inherited pnlEdit: TevPanel
      Top = 283
      object lValue: TevLabel [2]
        Left = 243
        Top = 11
        Width = 27
        Height = 13
        Caption = 'Value'
      end
      inherited pnlButtons: TevPanel
        Top = 19
        Height = 28
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 243
        Top = 26
        Width = 184
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = dsFieldData
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 432
    Top = 48
  end
end
