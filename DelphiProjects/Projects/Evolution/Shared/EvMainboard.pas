unit EvMainboard;

interface

uses Classes, Windows, SysUtils, SyncObjs, isBaseClasses, isBasicUtils,  EvCommonInterfaces,
     isLogFile, isMessenger, Forms, isSettings, evConsts, EvTransportInterfaces, isThreadManager;


  function  Mainboard: IevMainboard;
  procedure ActivateMainboard;
  procedure DeactivateMainboard;

// Shortcuts to global functions
  function  mb_AppConfiguration: IisSettings;
  function  mb_AppSettings: IisSettings;
  function  mb_GlobalSettings: IevSettings;
  function  mb_TaskQueue: IevTaskQueue;
  function  mb_TaskScheduler: IevTaskScheduler;
  function  mb_AsyncFunctions: IevAsyncFunctions;
  function  mb_GlobalFlagsManager: IevGlobalFlagsManager;
  function  mb_TCPClient: IevTCPClient;
  function  mb_TCPServer: IevTCPServer;
  function  mb_LogFile: IevLogFile;
  function  mb_Messenger: IisMessenger;

implementation

uses evModuleRegister, EvContext, EvClasses, EvUtils;

type
  TevMainboard = class(TisInterfacedObject, IevMainboard)
  private
    FLogFile:             IevLogFile;
    FMessenger:           IisMessenger;
    FMachineInfo:         IevMachineInfo;
    FModuleRegister:      IevModuleRegister;
    FContextManager:      IevContextManager;
    FGlobalSettings:      IevSettings;
    FGlobalCallbacks:     IevGlobalCallbacks;
    FTimeSource:          IevTimeSource;
    FTaskQueue:           IevTaskQueue;
    FTaskScheduler:       IevTaskScheduler;
    FAsyncFunctions:      IevAsyncFunctions;
    FGlobalFlagsManager:  IevGlobalFlagsManager;
    FXmlRPCServer:        IevXmlRPCServer;
    FRASAccess:           IevRASAccess;
    FEvoXScheduler:       IEvExchangeScheduler;
    FADRClient:           IevADRClient;
    FADRServer:           IevADRServer;
    FVersionUpdateLocal:  IevVersionUpdateLocal;
    FVersionUpdateRemote: IevVersionUpdateRemote;
    FTCPClient:           IevTCPClient;
    FTCPServer:           IevTCPServer;
    FAppConfiguration:    IisSettings;
    FAppSettings:         IisSettings;
    FBBClient:            IevBBClient;
    FDBMaintenance:       IevDBMaintenance;
    FDashboardDataProvider: IevDashboardDataProvider;
  protected
    procedure DoOnConstruction; override;
    procedure InitializeModules;
    procedure DeinitializeModules;
    procedure ActivateModules;
    procedure DeactivateModules;
    function  AppConfiguration: IisSettings;
    function  AppSettings: IisSettings;
    function  LogFile: IevLogFile;
    function  Messenger: IisMessenger;
    function  MachineInfo: IevMachineInfo;
    function  ModuleRegister: IevModuleRegister;
    function  ContextManager: IevContextManager;
    function  TimeSource: IevTimeSource;
    function  GlobalSettings: IevSettings;
    function  GlobalCallbacks: IevGlobalCallbacks;
    function  TaskQueue: IevTaskQueue;
    function  TaskScheduler: IevTaskScheduler;
    function  AsyncFunctions: IevAsyncFunctions;
    function  GlobalFlagsManager: IevGlobalFlagsManager;
    function  XmlRPCServer: IevXmlRPCServer;
    function  RASAccess: IevRASAccess;
    function  VersionUpdateLocal: IevVersionUpdateLocal;
    function  VersionUpdateRemote: IevVersionUpdateRemote;
    function  TCPClient: IevTCPClient;
    function  TCPServer: IevTCPServer;
    function  BBClient: IevBBClient;
    function  EvoXScheduler: IEvExchangeScheduler;
    function  ADRClient: IevADRClient;
    function  ADRServer: IevADRServer;
    function  DBMaintenance: IevDBMaintenance;
    function  DashboardDataProvider: IevDashboardDataProvider;
  public
    destructor Destroy; override;
  end;


var
  vMainboard: IevMainboard;

function Mainboard: IevMainboard;
begin
  if not Assigned(vMainboard) then
    ActivateMainboard;
  Result := vMainboard;
end;

procedure ActivateMainboard;
begin
  if not Application.Terminated and not CurrentThreadTaskTerminated then
    if not Assigned(vMainboard) then
      vMainboard := TevMainboard.Create;
end;

procedure DeactivateMainboard;
begin
  vMainboard := nil;
end;

function  mb_Messenger: IisMessenger;
begin
  Result := Mainboard.Messenger;
end;

function  mb_GlobalSettings: IevSettings;
begin
  Result := Mainboard.GlobalSettings;
end;

function  mb_AppSettings: IisSettings;
begin
  Result := Mainboard.AppSettings;
end;

function  mb_AppConfiguration: IisSettings;
begin
  Result := Mainboard.AppConfiguration;
end;

function  mb_TaskQueue: IevTaskQueue;
begin
  Result := Mainboard.TaskQueue;
end;

function mb_TaskScheduler: IevTaskScheduler;
begin
  Result := Mainboard.TaskScheduler;
end;

function  mb_AsyncFunctions: IevAsyncFunctions;
begin
  Result := Mainboard.AsyncFunctions;
end;

function  mb_GlobalFlagsManager: IevGlobalFlagsManager;
begin
  Result := Mainboard.GlobalFlagsManager;
end;

function  mb_TCPClient: IevTCPClient;
begin
  Result := Mainboard.TCPClient;
end;

function  mb_TCPServer: IevTCPServer;
begin
  Result := Mainboard.TCPServer;
end;

function mb_LogFile: IevLogFile;
begin
  Result := Mainboard.LogFile;
end;

{ TevMainboard }

function TevMainboard.AsyncFunctions: IevAsyncFunctions;
begin
  Result := FAsyncFunctions;
end;

function TevMainboard.GlobalFlagsManager: IevGlobalFlagsManager;
begin
  Result := FGlobalFlagsManager;
end;

function TevMainboard.ContextManager: IevContextManager;
begin
  Result := FContextManager;
end;

destructor TevMainboard.Destroy;
begin
  FMessenger := nil;
  inherited;

  if not Application.Terminated then
    SetGlobalLogFile(nil);
end;

procedure TevMainboard.DoOnConstruction;
begin
  inherited;

  FLogFile := TevLogFile.Create(ChangeFileExt(ModuleFileName, '.log'));
  SetGlobalLogFile(FLogFile);

  FMessenger := CreateMessenger;

  FAppConfiguration := TisSettingsINI.Create(AppDir + sConfigFileName);

  GlobalLogFile.PurgeRecThreshold := FAppConfiguration.GetValue('General\LogFilePurgeRecThreshold', Integer(DefaultPurgeRecThreshold));
  GlobalLogFile.PurgeRecCount := FAppConfiguration.GetValue('General\LogFilePurgeRecCount', Integer(DefaultPurgeRecCount));

  FAppSettings := TisSettingsRegistry.Create(HKEY_CURRENT_USER,
    'SOFTWARE\Evolution\' +  ChangeFileExt(ExtractFileName(AppFileName), ''));

  FMachineInfo := TevMachineInfo.Create;

  FModuleRegister := TevModuleRegister.Create;
  FContextManager := TevContextManager.Create;
end;

function TevMainboard.GlobalSettings: IevSettings;
begin
  Result := FGlobalSettings;
end;

function TevMainboard.MachineInfo: IevMachineInfo;
begin
  Result := FMachineInfo;
end;

function TevMainboard.ModuleRegister: IevModuleRegister;
begin
  Result := FModuleRegister;
end;

function TevMainboard.RASAccess: IevRASAccess;
begin
  Result := FRASAccess;
end;

function TevMainboard.TaskQueue: IevTaskQueue;
begin
  Result := FTaskQueue;
end;


function TevMainboard.TaskScheduler: IevTaskScheduler;
begin
  Result := FTaskScheduler;
end;

function TevMainboard.XmlRPCServer: IevXmlRPCServer;
begin
  Result := FXmlRPCServer;
end;

function TevMainboard.LogFile: IevLogFile;
begin
  Result := FLogFile;
end;

function TevMainboard.TimeSource: IevTimeSource;
begin
  Result := FTimeSource;
end;

function TevMainboard.TCPClient: IevTCPClient;
begin
  Result := FTCPClient;
end;

function TevMainboard.TCPServer: IevTCPServer;
begin
  Result := FTCPServer;
end;

procedure TevMainboard.ActivateModules;
begin
  // All domains are setup at this point
  if not Mainboard.ModuleRegister.IsProxyModule(GlobalFlagsManager) then
    GlobalFlagsManager.Active := True;

  if (XmlRPCServer <> nil) and not ModuleRegister.IsProxyModule(XmlRPCServer) and
     Boolean(AppConfiguration.GetValue('General\API', True)) then
    XmlRPCServer.Active := True;

  if (TaskQueue <> nil) and not Mainboard.ModuleRegister.IsProxyModule(TaskQueue) and
     Boolean(AppConfiguration.GetValue('General\TaskQueue', True)) then
    TaskQueue.Active := True;

  if (TaskScheduler <> nil) and not ModuleRegister.IsProxyModule(TaskScheduler) and
     Boolean(AppConfiguration.GetValue('General\TaskScheduler', True)) then
    TaskScheduler.Active := True;

  if TCPServer <> nil then
    TCPServer.Active := True;

  if (BBClient <> nil) and not IsDebugMode then
    BBClient.Active := True;

  if (EvoXScheduler <> nil) and not ModuleRegister.IsProxyModule(EvoXScheduler) and
     Boolean(AppConfiguration.GetValue('General\EvoXScheduler', False)) then
    EvoXScheduler.Active := True;

  if (ADRClient <> nil) and Boolean(AppConfiguration.GetValue('General\ADRClient', True)) then
    ADRClient.Active := True;

  if (ADRServer <> nil) and Boolean(AppConfiguration.GetValue('General\ADRServer', True)) then
    ADRServer.Active := True;

  mb_LogFile.AddEvent(etInformation, 'Mainboard', 'Application started', '');
end;

procedure TevMainboard.DeactivateModules;
begin
  if BBClient <> nil then
    BBClient.Active := False;

  if TCPClient <> nil then
    TCPClient.Logoff;

  if TCPServer <> nil then
    TCPServer.Active := False;

  if (XmlRPCServer <> nil) and not ModuleRegister.IsProxyModule(XmlRPCServer) then
    XmlRPCServer.Active := False;

  if (TaskQueue <> nil) and not Mainboard.ModuleRegister.IsProxyModule(TaskQueue) then
    TaskQueue.Active := False;

  if (TaskScheduler <> nil) and not ModuleRegister.IsProxyModule(TaskScheduler) then
    TaskScheduler.Active := False;

  if (EvoXScheduler <> nil) and not ModuleRegister.IsProxyModule(EvoXScheduler) then
    EvoXScheduler.Active := False;

  if ADRClient <> nil then
    ADRClient.Active := False;

  if (ADRServer <> nil) and not ModuleRegister.IsProxyModule(EvoXScheduler) then
    ADRServer.Active := False;

  mb_LogFile.AddEvent(etInformation, 'Mainboard', 'Application stopped', '');
end;

procedure TevMainboard.InitializeModules;
begin
  FMachineInfo.Printers.Load;
  FGlobalSettings := ModuleRegister.CreateModuleInstance(IevSettings, True) as IevSettings;
  FGlobalFlagsManager := ModuleRegister.CreateModuleInstance(IevGlobalFlagsManager, True) as IevGlobalFlagsManager;
  FTimeSource := ModuleRegister.CreateModuleInstance(IevTimeSource, True) as IevTimeSource;
  FTaskQueue := ModuleRegister.CreateModuleInstance(IevTaskQueue, True) as IevTaskQueue;
  FTaskScheduler := ModuleRegister.CreateModuleInstance(IevTaskScheduler, True) as IevTaskScheduler;
  FAsyncFunctions := ModuleRegister.CreateModuleInstance(IevAsyncFunctions, True) as IevAsyncFunctions;
  FXmlRPCServer := ModuleRegister.CreateModuleInstance(IevXmlRPCServer, True) as IevXmlRPCServer;
  FRASAccess := ModuleRegister.CreateModuleInstance(IevRASAccess, True) as IevRASAccess;
  FVersionUpdateLocal := ModuleRegister.CreateModuleInstance(IevVersionUpdateLocal, True) as IevVersionUpdateLocal;
  FVersionUpdateRemote := ModuleRegister.CreateModuleInstance(IevVersionUpdateRemote, True) as IevVersionUpdateRemote;
  FTCPClient := ModuleRegister.CreateModuleInstance(IevTCPClient, True) as IevTCPClient;
  FTCPServer := ModuleRegister.CreateModuleInstance(IevTCPServer, True) as IevTCPServer;
  FGlobalCallbacks := ModuleRegister.CreateModuleInstance(IevGlobalCallbacks, True) as IevGlobalCallbacks;
  FBBClient := ModuleRegister.CreateModuleInstance(IevBBClient, True) as IevBBClient;
  FEvoXScheduler := ModuleRegister.CreateModuleInstance(IEvExchangeScheduler, True) as IevExchangeScheduler;
  FADRClient := ModuleRegister.CreateModuleInstance(IEvADRClient, True) as IevADRClient;
  FADRServer := ModuleRegister.CreateModuleInstance(IEvADRServer, True) as IevADRServer;
  FDBMaintenance := ModuleRegister.CreateModuleInstance(IevDBMaintenance, True) as IevDBMaintenance;
  FDashboardDataProvider := ModuleRegister.CreateModuleInstance(IevDashboardDataProvider, True) as IevDashboardDataProvider;
end;

function TevMainboard.GlobalCallbacks: IevGlobalCallbacks;
begin
  Result := FGlobalCallbacks;
end;

function TevMainboard.AppSettings: IisSettings;
begin
  Result := FAppSettings;
end;

function TevMainboard.AppConfiguration: IisSettings;
begin
  Result := FAppConfiguration;
end;

function TevMainboard.VersionUpdateLocal: IevVersionUpdateLocal;
begin
  Result := FVersionUpdateLocal;
end;

function TevMainboard.VersionUpdateRemote: IevVersionUpdateRemote;
begin
  Result := FVersionUpdateRemote;
end;

function TevMainboard.BBClient: IevBBClient;
begin
  Result := FBBClient;
end;

function TevMainboard.EvoXScheduler: IEvExchangeScheduler;
begin
  Result := FEvoXScheduler;
end;

function TevMainboard.ADRClient: IevADRClient;
begin
  Result := FADRClient;
end;

function TevMainboard.ADRServer: IevADRServer;
begin
  Result := FADRServer;
end;

procedure TevMainboard.DeinitializeModules;
begin
  FDashboardDataProvider := nil;
  FDBMaintenance := nil;
  FXmlRPCServer := nil;
  FRASAccess := nil;
  FVersionUpdateLocal := nil;
  FVersionUpdateRemote := nil;
  FBBClient := nil;
  FEvoXScheduler := nil;
  FADRClient := nil;
  FADRServer := nil;
  FTaskScheduler := nil;
  FTaskQueue := nil;
  FAsyncFunctions := nil;
  FTimeSource := nil;
  FGlobalCallbacks := nil;
  FTCPClient := nil;
  FTCPServer := nil;
  FGlobalSettings := nil;
end;

function TevMainboard.Messenger: IisMessenger;
begin
  Result := FMessenger;
end;

function TevMainboard.DBMaintenance: IevDBMaintenance;
begin
  Result := FDBMaintenance;
end;

function TevMainboard.DashboardDataProvider: IevDashboardDataProvider;
begin
  Result := FDashboardDataProvider;
end;

end.
