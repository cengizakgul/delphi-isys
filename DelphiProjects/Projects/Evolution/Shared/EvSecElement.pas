// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvSecElement;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, SSecurityInterface, Dialogs,
  EvConsts;

type

  TevElementType = (otMenu, otFunction, otScreen, otDataField, otDataset, otOther);

  TevSecAtomContext = class(TCollectionItem)
  private
    FPriority: Integer;
    FCaption: ShortString;
    FIconName: ShortString;
    FTag: ShortString;
    //FIcon: TIcon;
    //procedure SetIcon(const Value: TIcon);
    procedure SetIconName(const Value: ShortString);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Tag: ShortString read FTag write FTag;
    property Caption: ShortString read FCaption write FCaption;
    property IconName: ShortString read FIconName write SetIconName;
    property Priority: Integer read FPriority write FPriority;
    //property Icon: TIcon read FIcon write SetIcon;
  end;
  TevSecAtom = class(TComponent, ISecurityAtom)
  private
    FAtomType: ShortString;
    FAtomTag: ShortString;
    FAtomCaption: ShortString;
    FGetAtomContextRecordArrayEvent: TGetAtomContextRecordArrayEvent;
    FSecurityContexts: TOwnedCollection;
    procedure SetSecurityContexts(const Value: TOwnedCollection);
  protected
    function IsAtom: Boolean;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetCaption: ShortString;
    function SGetPossibleSecurityContexts: TAtomContextRecordArray;
    function Component: TComponent;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property AtomType: ShortString read FAtomType write FAtomType;
    property AtomTag: ShortString read FAtomTag write FAtomTag;
    property AtomCaption: ShortString read FAtomCaption write FAtomCaption;
    property Contexts: TOwnedCollection read FSecurityContexts write SetSecurityContexts;
    property OnGetContextRecordArray: TGetAtomContextRecordArrayEvent read FGetAtomContextRecordArrayEvent write FGetAtomContextRecordArrayEvent;
  end;

  TevSecElement = class(TComponent, ISecurityElement)
  private
    FSecurityState: ShortString;
    FOnGetRelations: TGetAtomElementRelationsArrayEvent;
    FOnSetSecurityState: TSetSecurityStateEvent;
    FevElementType: TevElementType;
    FevElementTypeString: ShortString;
    FAtomComponent: TComponent;
    FElementTag: ShortString;
    FOnGetStates: TGetElementStatesArrayEvent;
    procedure SetevElementType(const Value: TevElementType);
    procedure SetevElementTypeString(const Value: ShortString);
    procedure SetAtomComponent(const Value: TComponent);
    { Private declarations }
  protected
    { Protected declarations }
    function SGetType: ShortString; virtual;
    function SGetTag: ShortString; virtual;
    procedure SSetCurrentSecurityState(const State: ShortString); virtual;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray; virtual;
    function SGetStates: TElementStatesArray; virtual;
    function Component: TComponent;
    function Atom: ISecurityAtom;
    procedure Loaded; override;
  public
    { Public declarations }
    function SecurityState: ShortString;
  published
    { Published declarations }
    property AtomComponent: TComponent read FAtomComponent write SetAtomComponent;
    property ElementType: TevElementType read FevElementType write SetevElementType;
    property TypeString: ShortString read FevElementTypeString write SetevElementTypeString;
    property ElementTag: ShortString read FElementTag write FElementTag;
    property OnSetState: TSetSecurityStateEvent read FOnSetSecurityState write FOnSetSecurityState;
    property OnGetStates: TGetElementStatesArrayEvent read FOnGetStates write FOnGetStates;
    property OnGetRelations: TGetAtomElementRelationsArrayEvent read FOnGetRelations write FOnGetRelations;
  end;
  TevSimpleSecElement = class(TComponent, ISecurityElement)
  private
    FAtomComponent: TComponent;
    FOnSetSecurityState: TSetSecurityStateEvent;
    FSecurityState: ShortString;
    procedure SetAtomComponent(const Value: TComponent);
  protected
    function SGetType: ShortString; virtual;
    function SGetTag: ShortString; virtual;
    procedure SSetCurrentSecurityState(const State: ShortString); virtual;
    function SGetAtomElementRelationsArray: TAtomElementRelationsArray; virtual;
    function SGetStates: TElementStatesArray; virtual;
    function Component: TComponent;
    function Atom: ISecurityAtom;
    function SecurityState: ShortString;
    procedure Loaded; override;
  published
    property AtomComponent: TComponent read FAtomComponent write SetAtomComponent;
    property OnSetState: TSetSecurityStateEvent read FOnSetSecurityState write FOnSetSecurityState;
  end;


procedure Register;

function FindSecurityElement(const c: TComponent; const ElementTag: ShortString): ISecurityElement;

implementation

procedure Register;
begin
  RegisterComponents('Evolution', [TevSecElement, TevSecAtom, TevSimpleSecElement]);
end;

function FindSecurityElement(const c: TComponent; const ElementTag: ShortString): ISecurityElement;
var
  i: Integer;
  s: string;
begin
  s := StringReplace(ElementTag, '&', '', [rfReplaceAll]);
  c.GetInterface(ISecurityElement, Result);
  if Assigned(Result) then
    if StringReplace(Result.SGetTag, '&', '', [rfReplaceAll]) <> ElementTag then
      Result := nil;
  if not Assigned(Result) then
    for i := 0 to c.ComponentCount-1 do
    begin
      c.Components[i].GetInterface(ISecurityElement, Result);
      if Assigned(Result) then
        if StringReplace(Result.SGetTag, '&', '', [rfReplaceAll]) = ElementTag then
          Break
        else
          Result := nil;
    end;
end;

{ TOperation }

function TevSecElement.SGetTag: ShortString;
begin
  Result := ElementTag;
end;

function TevSecElement.SGetType: ShortString;
begin
  case FevElementType of
  otOther:
      Result := FevElementTypeString;
  otFunction:
      Result := etFunc;
  otScreen:
      Result := etScreen;
  otMenu:
      Result := etMenu;
  otDataField:
      Result := etDataField;
  otDataSet:
      Result := etDataset;
  else
    Assert(False);
  end;
end;

procedure TevSecElement.SSetCurrentSecurityState(const State: ShortString);
begin
  FSecurityState := State;
  if Assigned(FOnSetSecurityState) then
    FOnSetSecurityState(Self);
end;

function TevSecElement.SecurityState: ShortString;
begin
  Result := FSecurityState;
end;

procedure TevSecElement.SetevElementType(const Value: TevElementType);
begin
  if Value in [otFunction, otScreen, otDataField, otMenu, otDataset] then
    FevElementTypeString := '';
  FevElementType := Value;
end;

procedure TevSecElement.SetevElementTypeString(const Value: ShortString);
begin
  FevElementType := otOther;
  FevElementTypeString := Value;
end;

function TevSecElement.SGetAtomElementRelationsArray: TAtomElementRelationsArray;
var
  AvailContexts: TAtomContextRecordArray;
  procedure Add(const AtomContext, ElementState: ShortString; const A2E, E2A: TLinkType);
  var
    r: TAtomElementRelation;
    i: Integer;
    f: Boolean;
  begin
    f := False;
    for i := 0 to High(AvailContexts) do
      if AvailContexts[i].Tag = AtomContext then
      begin
        f := True;
        Break;
      end;
    if f then
    begin
      r.AtomContext := AtomContext;
      r.ElementState := ElementState;
      r.Atom2Element := A2E;
      r.Element2Atom := E2A;
      i := Length(Result);
      SetLength(Result, i+ 1);
      Result[i] := r;
    end;
  end;
var
  SecurityAtom: ISecurityAtom;
begin
  SetLength(Result, 0);
  AtomComponent.GetInterface(ISecurityAtom, SecurityAtom);
  if Assigned(SecurityAtom) then
  try
    if SecurityAtom.IsAtom then
    begin
      AvailContexts := SecurityAtom.SGetPossibleSecurityContexts;
      case FevElementType of
      otOther:
          ;
      otDataField:
        if SecurityAtom.SGetType = atFunc then
        begin
          Add(ctDisabled, stReadOnly, ltEqual, ltEqual);
          Add(ctEnabled, stEnabled, ltEqual, ltEqual);
        end;
      otFunction:
        if SecurityAtom.SGetType = atScreen then
        begin
          Add(ctDisabled, stDisabled, ltKeepLowest, ltNone);
          Add(ctEnabled, stEnabled, ltNone, ltNone);
        end
        else
        if SecurityAtom.SGetType = atFunc then
        begin
          Add(ctDisabled, stDisabled, ltEqual, ltEqual);
          Add(ctEnabled, stEnabled, ltEqual, ltEqual);
        end;
      otMenu:
        if SecurityAtom.SGetType = atScreen then
        begin
          Add(ctDisabled, stDisabled, ltKeepLowest, ltNone);
          Add(ctEnabled, stEnabled, ltNone, ltNone);
        end
        else
        if SecurityAtom.SGetType = atMenu then
        begin
          Add(ctDisabled, stDisabled, ltEqual, ltEqual);
          Add(ctEnabled, stEnabled, ltEqual, ltEqual);
        end;
      otScreen:
        if SecurityAtom.SGetType = atScreen then
        begin
          Add(ctDisabled, stDisabled, ltEqual, ltEqual);
          Add(ctReadOnly, stReadOnly, ltEqual, ltEqual);
          Add(ctEnabled, stEnabled, ltEqual, ltEqual);
        end
      else
        if SecurityAtom.SGetType = atMenu then
        begin
          Add(ctDisabled, stDisabled, ltEqual, ltKeepLowest);
          Add(ctEnabled, stReadOnly, ltNone, ltEqual);
          Add(ctEnabled, stEnabled, ltNone, ltEqual);
        end;
      otDataset:
        if SecurityAtom.SGetType = atScreen then
        begin
          Add(ctDisabled, stDisabled, ltKeepLowest, ltEqual);
          Add(ctReadOnly, stReadOnly, ltMinLimit, ltMaxLimit);
          Add(ctEnabled, stEnabled, ltEqual, ltNone);
        end
        else
        if SecurityAtom.SGetType = atDataset then
        begin
          Add(ctDisabled, stDisabled, ltEqual, ltEqual);
          Add(ctReadOnly, stReadOnly, ltEqual, ltEqual);
          Add(ctEnabled, stEnabled, ltEqual, ltEqual);
        end;
      else
        Assert(False);
      end;
      if Assigned(FOnGetRelations) then
        FOnGetRelations(Self, SecurityAtom, Result);
    end;
  finally
    SecurityAtom := nil;
  end;
end;

function TevSecElement.SGetStates: TElementStatesArray;
  function GetStatePriority(const State: ShortString): Word;
  begin
    Result := 0;
    if State = stDisabled then
      // Result := 0
    else
    if State = stEnabled then
      Result := 250
    else
    if State = stReadOnly then
      Result := 128
    else
      Assert(False);
  end;
  procedure AddState(const State: ShortString);
  begin
    SetLength(Result, Length(Result)+ 1);
    with Result[High(Result)] do
    begin
      Tag := State;
      Priority := GetStatePriority(State);
    end;
  end;
begin
  SetLength(Result, 0);
  case FevElementType of
  otOther:
      ;
  otFunction, otMenu:
    begin
      AddState(stDisabled);
      AddState(stEnabled);
    end;
  otDataField:
    begin
      AddState(stReadOnly);
      AddState(stEnabled);
    end;
  otScreen, otDataset:
    begin
      AddState(stDisabled);
      AddState(stReadOnly);
      AddState(stEnabled);
    end;
  else
    Assert(False);
  end;
  if Assigned(FOnGetStates) then
    FOnGetStates(Self, Result);
end;

procedure TevSecElement.SetAtomComponent(const Value: TComponent);
var
  SecurityAtom: ISecurityAtom;
begin
  if FAtomComponent <> Value then
  begin
    if Value = nil then
      FAtomComponent := Value
    else
    begin
      Value.GetInterface(ISecurityAtom, SecurityAtom);
      try
        if Assigned(SecurityAtom) then
          FAtomComponent := Value;
      finally
        SecurityAtom := nil;
      end;
    end;
  end;
end;

{ TevSecAtom }

procedure TevSecAtom.Assign(Source: TPersistent);
var
  c: TevSecAtom;
begin
  if Source is TevSecAtom then
  begin
    c := Source as TevSecAtom;
    AtomType := c.AtomType;
    AtomTag := c.AtomTag;
    AtomCaption := c.AtomCaption;
    Contexts := c.Contexts;
    OnGetContextRecordArray := c.OnGetContextRecordArray;
  end
  else
    inherited;
end;

function TevSecAtom.Component: TComponent;
begin
  Result := Self;
end;

constructor TevSecAtom.Create(Owner: TComponent);
begin
  inherited;
  FSecurityContexts := TOwnedCollection.Create(Self, TevSecAtomContext);
end;

destructor TevSecAtom.Destroy;
begin
  FSecurityContexts.Free;
  inherited;
end;

function TevSecAtom.IsAtom: Boolean;
begin
  Result := True;
end;

procedure TevSecAtom.SetSecurityContexts(const Value: TOwnedCollection);
begin
  FSecurityContexts.Assign(Value);
end;

function TevSecAtom.SGetCaption: ShortString;
begin
  Result := FAtomCaption;
end;

function TevSecAtom.SGetPossibleSecurityContexts: TAtomContextRecordArray;
var
  i: Integer;
begin
  SetLength(Result, FSecurityContexts.Count);
  for i := 0 to FSecurityContexts.Count-1 do
    with TevSecAtomContext(FSecurityContexts.Items[i]) do
    begin
      Result[i].Caption := Caption;
      Result[i].Tag := Tag;
      Result[i].IconName := IconName;
      //Result[i].Icon := Icon;
      Result[i].Priority := Priority;
    end;
  if Assigned(FGetAtomContextRecordArrayEvent) then
    FGetAtomContextRecordArrayEvent(ISecurityAtom(Self), Result);
end;

function TevSecAtom.SGetTag: ShortString;
begin
  Result := FAtomTag;
end;

function TevSecAtom.SGetType: ShortString;
begin
  Result := FAtomType;
end;

function TevSecElement.Component: TComponent;
begin
  Result := Self;
end;

function TevSecElement.Atom: ISecurityAtom;
begin
  if Assigned(AtomComponent) then
    AtomComponent.GetInterface(ISecurityAtom, Result)
  else
    Result := nil;
end;

procedure TevSecElement.Loaded;
begin
  inherited;
  if not Assigned(AtomComponent) then
    AtomComponent := Owner;
end;

{ TevSecAtomContext }

procedure TevSecAtomContext.Assign(Source: TPersistent);
var
  c: TevSecAtomContext;
begin
  if Source is TevSecAtomContext then
  begin
    c := Source as TevSecAtomContext;
    Caption := c.Caption;
    Tag := c.Tag;
    Priority := c.Priority;
    if c.IconName <> '' then
      IconName := c.IconName
    else
      //Icon.Assign(c.Icon);
  end
  else
    inherited;
end;

constructor TevSecAtomContext.Create(Collection: TCollection);
begin
  inherited;
  //FIcon := TIcon.Create;
end;

destructor TevSecAtomContext.Destroy;
begin
  //FIcon.Free;
  inherited;
end;

{procedure TevSecAtomContext.SetIcon(const Value: TIcon);
begin
  FIcon.Assign(Value);
  FIconName := '';
end;}

procedure TevSecAtomContext.SetIconName(const Value: ShortString);
begin
  FIconName := Value;
  //FIcon.Assign(nil);
end;

{ TevSimpleSecElement }

function TevSimpleSecElement.Atom: ISecurityAtom;
begin
  if Assigned(AtomComponent) then
    AtomComponent.GetInterface(ISecurityAtom, Result)
  else
    Result := nil;
end;

function TevSimpleSecElement.Component: TComponent;
begin
  Result := Self;
end;

procedure TevSimpleSecElement.Loaded;
begin
  inherited;
  if not Assigned(AtomComponent) then
    AtomComponent := Owner;
end;

function TevSimpleSecElement.SecurityState: ShortString;
begin
  Result := FSecurityState;
end;

procedure TevSimpleSecElement.SetAtomComponent(const Value: TComponent);
var
  SecurityAtom: ISecurityAtom;
begin
  if FAtomComponent <> Value then
  begin
    if Value = nil then
      FAtomComponent := Value
    else
    begin
      Value.GetInterface(ISecurityAtom, SecurityAtom);
      try
        if Assigned(SecurityAtom) then
          FAtomComponent := Value;
      finally
        SecurityAtom := nil;
      end;
    end;
  end;
end;

function TevSimpleSecElement.SGetAtomElementRelationsArray: TAtomElementRelationsArray;
var
  a: TAtomContextRecordArray;
  i: Integer;
begin
  Assert(Assigned(FAtomComponent));
  a := Atom.SGetPossibleSecurityContexts;
  SetLength(Result, Length(a));
  for i := 0 to High(Result) do
  begin
    Result[i].AtomContext := a[i].Tag;
    Result[i].ElementState := a[i].Tag;
    Result[i].Atom2Element := ltEqual;
    Result[i].Element2Atom := ltEqual;
  end;
end;

function TevSimpleSecElement.SGetStates: TElementStatesArray;
var
  a: TAtomContextRecordArray;
  i: Integer;
begin
  Assert(Assigned(FAtomComponent));
  a := Atom.SGetPossibleSecurityContexts;
  SetLength(Result, Length(a));
  for i := 0 to High(Result) do
  begin
    Result[i].Tag := a[i].Tag;
    Result[i].Priority := a[i].Priority;
  end;
end;

function TevSimpleSecElement.SGetTag: ShortString;
begin
  Assert(Assigned(FAtomComponent));
  Result := Atom.SGetTag;
end;

function TevSimpleSecElement.SGetType: ShortString;
begin
  Assert(Assigned(FAtomComponent));
  Result := Atom.SGetType;
end;

procedure TevSimpleSecElement.SSetCurrentSecurityState(
  const State: ShortString);
begin
  FSecurityState := State;
  if Assigned(FOnSetSecurityState) then
    FOnSetSecurityState(Self);
end;

end.
