// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvComponentsExt;

interface

uses  Classes, DB, Variants, EvStreamUtils, EvTypes, EvConsts, EvExceptions, EvDataAccessComponents, EvClientDataSet;

type
  IAchPayment = interface
  ['{B6035990-5BFE-4414-9221-56574643E97E}']
    procedure ProcessEFTPayment(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer; TaxType: TTaxTable);
    procedure ProcessEFTPSPayment(Amount1, amount2, amount3: Double; EffectiveDate, TaxPeriodEndDate: TDateTime; CONbr, TaxPaymentAchNbr: Integer;
      TaxName: string; CobraCredit: Boolean);
  end;

  TAchPaymentParams = class(TInterPackageData)
  private
    procedure CheckForProblems(const CoNbr, SY_GLOBAL_AGENCY_NBR: Variant);
  public
    procedure ProcessEFTPayment(const Amount: Double; const SecondAddendumAmount: Variant; const EffectiveDate, TaxPeriodEndDate: TDateTime; const PaymentSequenceNbr, COTaxEntityNbr,
      TaxPaymentAchNbr: Integer; const TaxType: TTaxTable);
    procedure ProcessEFTPSPayment(const Amount1, amount2, amount3: Double; const EffectiveDate, TaxPeriodEndDate: TDateTime; const CONbr, TaxPaymentAchNbr: Integer;
      const TaxName: string; CobraCredit: Boolean);
    procedure Read(const ir: IAchPayment);
  end;

  IEFTPS_Payment = interface
  ['{BBA0B0B5-C060-45CD-8AE8-803FB652421B}']
    function AddPayment(SBBankAccountNbr: Integer; CompanyFEIN, CompanyPIN, TaxName: string; DueDate, CheckDate: TDateTime; Amount: Real): string;
  end;

  TEFTPSPaymentParams = class(TInterPackageData)
  private
    FCounter: Integer;
  public
    constructor Create; overload;
    procedure AddPayment(const SBBankAccountNbr: Integer; const CompanyFEIN, CompanyPIN, TaxName: string;
      const DueDate, CheckDate: TDateTime; const Amount: Real; var Reference: string);
    procedure Read(const EFTPS_Payment: IEFTPS_Payment);
  end;

  INyDebitFile = interface
  ['{3C46B67F-DABB-432C-80B5-8125A45BB15C}']
    procedure AddPayment(const EIN: string; const LastPrInPeriod: TDateTime; const PayrollsInPeriod: Integer;
      const NyStateTax, NyCityTax, CityOfYorkersTax: Currency);
  end;

  TNyDebitPaymentParams = class(TInterPackageData)
  public
    procedure AddPayment(const EIN: string; const LastPrInPeriod: TDateTime; const PayrollsInPeriod: Integer;
      const NyStateTax, NyCityTax, CityOfYorkersTax: Currency);
    procedure Read(const NyDebitFile: INyDebitFile);
  end;

  IMaDebitFile = interface
  ['{C7752582-96BE-4FE8-A152-2ABDBA00A0C6}']
    procedure AddPayment(const RecordIdentifier: Char; const TaxPeriodEnd: TDateTime; const FEID: string;
      const BusinessName: string; const Amount: Currency; const SettlementDate: TDateTime;
      const SbBankNbr: Integer; const BankAccNumber: string; const AccountType: Char);
  end;

  TMaDebitPaymentParams = class(TInterPackageData)
  public
    procedure AddPayment(const RecordIdentifier: Char; const TaxPeriodEnd: TDateTime; const FEID: string;
      const BusinessName: string; const Amount: Currency; const SettlementDate: TDateTime; const SbBankAccountNbr, ClBankAccountNbr: Integer);
    procedure Read(const MaDebitFile: IMaDebitFile);
  end;

  ICaDebitFile = interface
  ['{8F7F1628-B266-4BC0-8BFA-3568F08BCA27}']
    procedure AddPayment(const CheckDate, DueDate: TDateTime; const AccountNumber, SecurityCode, TaxTypeCode: string;
      const StateWithholding, Sdi: Currency);
  end;

  TCaDebitPaymentParams = class(TInterPackageData)
  public
    procedure AddPayment(const CheckDate, DueDate: TDateTime; const AccountNumber, SecurityCode, TaxTypeCode: string;
      const StateWithholding, Sdi: Currency);
    procedure Read(const CaDebitFile: ICaDebitFile);
  end;

  IUniversalDebitPaymentParams = interface
  ['{A3AAB7FB-CB47-4535-AC44-0308AE541E2E}']
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId, GroupId, MainCoNbr: Integer;
                           const DueDate: TDateTime;const Amount : Currency;
                                const StateLiabNbrs, SuiLiabNbrs, LocalLiabNbrs: string);
  end;

  TUniversalDebitPaymentParams = class(TInterPackageData)
  private
    FCd: TEvClientDataSet;
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId, GroupId, MainCoNbr: Integer;
                           const DueDate: TDateTime; const Amount: Currency);
    procedure FinalizePayments;
    procedure Read(const UniversalDebitPaymentParams: IUniversalDebitPaymentParams);
  end;

  IOneMiscCheckPaymentParams = interface
  ['{67DDAEA7-041F-4289-9175-6CB7FCC240CD}']
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId,MainCoNbr,
                            TaxDepositId,BankAccRegNbr, FieldOfficeNbr: Integer;
                            const DueDate : TDateTime;
                            const TotalAmount : Currency; const GroupKey : String);
  end;

  TOneMiscCheckPaymentParams = class(TInterPackageData)
  private
    FCd: TEvClientDataSet;
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId,MainCoNbr,
                            TaxDepositId, BankAccRegNbr, FieldOfficeNbr: Integer;
                            const DueDate : TDateTime;
                            const TotalAmount : Currency; const GroupKey : String);
    procedure FinalizePayments;
    procedure Read(const OneMiscCheckPaymentParams: IOneMiscCheckPaymentParams);
  end;

  TTaxPaymentParams = class
  private
    FAchPayments: TAchPaymentParams;
    FEFTPS_Payment: TEFTPSPaymentParams;
    FNyDebitFile: TNyDebitPaymentParams;
    FMaDebitFile: TMaDebitPaymentParams;
    FCaDebitFile: TCaDebitPaymentParams;
    FChecks: IisStream;
    FWarnings: TStringList;
    FClNbr: Integer;
    FTaxPayrollFilter: string;
    FUnivDebitFile: TUniversalDebitPaymentParams;
    FOneMiscCheck: TOneMiscCheckPaymentParams;
    procedure SetWarnings(const Value: TStringList);
  public
    constructor Create; overload;
    constructor Create(const Stream: IisStream); overload;
    destructor Destroy; override;
    function AsMemStream: IisStream;
    property AchPayments: TAchPaymentParams read FAchPayments;
    property EFTPS: TEFTPSPaymentParams read FEFTPS_Payment;
    property NyDebitFile: TNyDebitPaymentParams read FNyDebitFile;
    property MaDebitFile: TMaDebitPaymentParams read FMaDebitFile;
    property CaDebitFile: TCaDebitPaymentParams read FCaDebitFile;
    property UnivDebitFile: TUniversalDebitPaymentParams read FUnivDebitFile;
    property OneMiscCheck: TOneMiscCheckPaymentParams read FOneMiscCheck;
    property TaxPayrollFilter: string read FTaxPayrollFilter write FTaxPayrollFilter;
    property Checks: IisStream read FChecks write FChecks;
    property Warnings: TStringList read FWarnings write SetWarnings;
    property ClNbr: Integer read FClNbr write FClNbr;
  end;



implementation

uses EvUtils, SDataStructure, SysUtils, EvContext, DateUtils;

{ TAchPaymentParams }

procedure TAchPaymentParams.CheckForProblems(const CoNbr, SY_GLOBAL_AGENCY_NBR: Variant);
begin
  ctx_DBAccess.EnableReadTransaction;
  try
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
  finally
    ctx_DBAccess.DisableReadTransaction;
  end;
  ctx_DataAccess.Activate([DM_SERVICE_BUREAU.SB_BANKS, DM_SYSTEM_MISC.SY_GLOBAL_AGENCY]);

  if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR',
    DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'ACH_SB_BANK_ACCOUNT_NBR'), []) then
    raise EInconsistentData.CreateHelp('SB Bank Account not found!', IDH_InconsistentData);

  if VarIsNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'TAX_SB_BANK_ACCOUNT_NBR')) then
    raise EInconsistentData.CreateHelp('Company #' + ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'CUSTOM_COMPANY_NUMBER'), 'N/A')+
      ' does not have SB Tax Bank Account setup!', IDH_InconsistentData);

  if ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR',
    DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'TAX_SB_BANK_ACCOUNT_NBR'), 'SB_BANKS_NBR'), 'ABA_NUMBER'), '') = '' then
    raise EInconsistentData.CreateHelp('Incorrect ABA number for bank account' + ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'TAX_SB_BANK_ACCOUNT_NBR'),
    'CUSTOM_BANK_ACCOUNT_NUMBER'), 'N/A'), IDH_InconsistentData);

  if ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
    SY_GLOBAL_AGENCY_NBR, 'RECEIVING_ABA_NUMBER'), '') = '' then
    raise EInconsistentData.CreateHelp('Incorrect ABA number for ' + ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
      SY_GLOBAL_AGENCY_NBR, 'AGENCY_NAME'), 'N/A') + '- Custom Company Number : '+ ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'CUSTOM_COMPANY_NUMBER'), 'N/A'), IDH_InconsistentData);

  if ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
    SY_GLOBAL_AGENCY_NBR, 'RECEIVING_ACCOUNT_NUMBER'), '') = '' then
    raise EInconsistentData.CreateHelp('Incorrect Bank Account number for ' + ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
      SY_GLOBAL_AGENCY_NBR, 'AGENCY_NAME'), 'N/A'), IDH_InconsistentData);
end;

procedure TAchPaymentParams.ProcessEFTPayment(const Amount: Double;
  const SecondAddendumAmount: Variant; const EffectiveDate,
  TaxPeriodEndDate: TDateTime; const PaymentSequenceNbr, COTaxEntityNbr,
  TaxPaymentAchNbr: Integer; const TaxType: TTaxTable);
begin
  DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.Activate;
  DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Activate;
  DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
  DM_SYSTEM_STATE.SY_STATES.Activate;
  DM_SYSTEM_STATE.SY_SUI.Activate;
  case TaxType of
  ttSui:
    CheckForProblems(DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_NBR'),
      DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'SY_SUI_NBR'),
      'SY_SUI_TAX_PMT_AGENCY_NBR'));
  ttState:
    CheckForProblems(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'CO_NBR'),
      DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE'),
      'SY_STATE_TAX_PMT_AGENCY_NBR'));
  ttLoc:
  begin
    if DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', COTaxEntityNbr, []) and
        DM_COMPANY.CO_STATES.Locate('CO_NBR;SY_STATES_NBR', VarArrayOf([DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_NBR').asInteger, DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').asInteger]), []) and
        (not DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull ) and
          DM_SYSTEM_LOCAL.SY_LOCALS.locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').asInteger,[]) and
           (DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('COMBINE_FOR_TAX_PAYMENTS').AsString = GROUP_BOX_YES) then
    begin
      Assert(DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsVariant, []),'Missing TCD Agency deposit frequency. Please contact with Isystems LLC.' );
      Assert(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ['SY_GLOBAL_AGENCY_NBR'], []), 'Missing TCD Agency set up. Please contact with Isystems LLC.');
      Assert((DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('AGENCY_TYPE').AsString = 'Y'), 'TCD Agency is inactive.  Select an active TCD Agency.');
      CheckForProblems(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'CO_NBR'),
                         DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger)
    end
    else
      CheckForProblems(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'CO_NBR'),
        DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCALS_NBR'),
        'SY_LOCAL_TAX_PMT_AGENCY_NBR'));
  end;
  else
    Assert(False);
  end;
  with MemStream do
  begin
    WriteByte(Ord('F'));
    WriteDouble(Amount);
    if VarIsNull(SecondAddendumAmount) then
      WriteByte(0)
    else
    if not VarIsArray(SecondAddendumAmount) then
    begin
      WriteByte(1);
      WriteDouble(SecondAddendumAmount);
    end
    else
    if VarArrayHighBound(SecondAddendumAmount, 1) = 1 then
    begin
      WriteByte(2);
      WriteDouble(SecondAddendumAmount[0]);
      WriteDouble(SecondAddendumAmount[1]);
    end
    else
    begin
      WriteByte(3);
      WriteDouble(SecondAddendumAmount[0]);
      WriteDouble(SecondAddendumAmount[1]);
      WriteDouble(SecondAddendumAmount[2]);
    end;
    WriteDouble(EffectiveDate);
    WriteDouble(TaxPeriodEndDate);
    WriteInteger(PaymentSequenceNbr);
    WriteInteger(COTaxEntityNbr);
    WriteInteger(TaxPaymentAchNbr);
    WriteByte(Ord(TaxType));
  end;
end;

procedure TAchPaymentParams.ProcessEFTPSPayment(const Amount1, amount2,
  amount3: Double; const EffectiveDate, TaxPeriodEndDate: TDateTime;
  const CONbr, TaxPaymentAchNbr: Integer; const TaxName: string; CobraCredit: Boolean);
begin
  ctx_DataAccess.Activate([DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY, DM_SYSTEM_STATE.SY_STATES]);
  CheckForProblems(CONbr,
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR',
    DM_COMPANY.CO.Lookup('CO_NBR', CONbr, 'SY_FED_TAX_PAYMENT_AGENCY_NBR'), 'SY_GLOBAL_AGENCY_NBR'));
  with MemStream do
  begin
    WriteByte(Ord('S'));
    WriteDouble(Amount1);
    WriteDouble(Amount2);
    WriteDouble(Amount3);
    WriteDouble(EffectiveDate);
    WriteDouble(TaxPeriodEndDate);
    WriteInteger(CONbr);
    WriteInteger(TaxPaymentAchNbr);
    WriteString(TaxName);
    WriteBoolean(CobraCredit);
  end;
end;

procedure TAchPaymentParams.Read(const ir: IAchPayment);
var
  Amount1, amount2, amount3: Double;
  EffectiveDate, TaxPeriodEndDate: TDateTime;
  CONbr, TaxPaymentAchNbr: Integer;
  TaxName: string;
  SecondAddendumAmount: Variant;
  PaymentSequenceNbr, COStateNbr: Integer;
  TaxType: TTaxTable;
  CobraCredit: Boolean;
begin
  with MemStream do
    case ReadByte of
    Ord('S'):
      begin
        Amount1 := ReadDouble;
        amount2 := ReadDouble;
        amount3 := ReadDouble;
        EffectiveDate := ReadDouble;
        TaxPeriodEndDate := ReadDouble;
        CONbr := ReadInteger;
        TaxPaymentAchNbr := ReadInteger;
        TaxName := ReadString;
        CobraCredit := ReadBoolean;
        ir.ProcessEFTPSPayment(Amount1, amount2, amount3, EffectiveDate,
          TaxPeriodEndDate, CONbr, TaxPaymentAchNbr, TaxName, CobraCredit);
      end;
    Ord('F'):
      begin
        Amount1 := ReadDouble;
        case ReadByte of
        0:
          SecondAddendumAmount := Null;
        1:
          SecondAddendumAmount := ReadDouble;
        2:
          begin
            SecondAddendumAmount := VarArrayOf([0, 0]);
            SecondAddendumAmount[0] := ReadDouble;
            SecondAddendumAmount[1] := ReadDouble;
          end;
        3:
          begin
            SecondAddendumAmount := VarArrayOf([0, 0, 0]);
            SecondAddendumAmount[0] := ReadDouble;
            SecondAddendumAmount[1] := ReadDouble;
            SecondAddendumAmount[2] := ReadDouble;
          end;
        else
          Assert(False);
        end;
        EffectiveDate := ReadDouble;
        TaxPeriodEndDate := ReadDouble;
        PaymentSequenceNbr := ReadInteger;
        COStateNbr := ReadInteger;
        TaxPaymentAchNbr := ReadInteger;
        TaxType := TTaxTable(ReadByte);
        ir.ProcessEFTPayment(Amount1, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate,
          PaymentSequenceNbr, COStateNbr, TaxPaymentAchNbr, TaxType);
      end;
    else
      Assert(False);
    end;
end;

{ TEFTPSPaymentParams }

procedure TEFTPSPaymentParams.AddPayment(const SBBankAccountNbr: Integer;
  const CompanyFEIN, CompanyPIN, TaxName: string; const DueDate,
  CheckDate: TDateTime; const Amount: Real; var Reference: string);
begin
  ctx_DBAccess.EnableReadTransaction;
  try
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
  finally
    ctx_DBAccess.DisableReadTransaction;
  end;

  if ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', SBBankAccountNbr, 'BATCH_FILER_ID'), '') = '' then
    raise EInconsistentData.CreateHelp('Batch Filer ID has to be setup on the Service Bureau Bank Account level.', IDH_InconsistentData);
  with MemStream do
  begin
    WriteInteger(SBBankAccountNbr);
    WriteString(CompanyFEIN);
    WriteString(CompanyPIN);
    WriteString(TaxName);
    WriteDouble(DueDate);
    WriteDouble(CheckDate);
    WriteDouble(Amount);
    Reference := Reference+ '*'+ IntToStr(FCounter);
    WriteString(Reference);
    Inc(FCounter);
  end;
end;

constructor TEFTPSPaymentParams.Create;
begin
  FCounter := 1;
  inherited Create;
end;

procedure TEFTPSPaymentParams.Read(const EFTPS_Payment: IEFTPS_Payment);
var
  SBBankAccountNbr: Integer;
  CompanyFEIN, CompanyPIN, TaxName: string;
  DueDate, CheckDate: TDateTime;
  Amount: Real;
  s: string;
begin
  with MemStream do
  begin
    SBBankAccountNbr := ReadInteger;
    CompanyFEIN := ReadString;
    CompanyPIN := ReadString;
    TaxName := ReadString;
    DueDate := ReadDouble;
    CheckDate := ReadDouble;
    Amount := ReadDouble;
    s := ReadString;
  end;
  DM_COMPANY.CO.DataRequired('ALL');
  DM_COMPANY.CO_TAX_DEPOSITS.DataRequired('TAX_PAYMENT_REFERENCE_NUMBER='+ QuotedStr(s));
  Assert(DM_COMPANY.CO_TAX_DEPOSITS.RecordCount=1);
  s := EFTPS_Payment.AddPayment(SBBankAccountNbr, CompanyFEIN, CompanyPIN, TaxName, DueDate, CheckDate, Amount);
  DM_COMPANY.CO_TAX_DEPOSITS.Edit;
  DM_COMPANY.CO_TAX_DEPOSITS['TAX_PAYMENT_REFERENCE_NUMBER'] := s;
  DM_COMPANY.CO_TAX_DEPOSITS.Post;
  ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_DEPOSITS])
end;

{ TNyDebitPaymentParams }

procedure TNyDebitPaymentParams.AddPayment(const EIN: string;
  const LastPrInPeriod: TDateTime; const PayrollsInPeriod: Integer;
  const NyStateTax, NyCityTax, CityOfYorkersTax: Currency);
var
  i: Integer;
begin
  for i := 1 to Length(EIN) do
    if not (EIN[i] in [' ', '0'..'9', 'A'..'Z']) then
      raise EInconsistentData.CreateHelp('NY state EIN contains unexpected character(s)', IDH_InconsistentData);
  with MemStream do
  begin
    WriteString(EIN);
    WriteDouble(LastPrInPeriod);
    WriteInteger(PayrollsInPeriod);
    WriteDouble(NyStateTax);
    WriteDouble(NyCityTax);
    WriteDouble(CityOfYorkersTax);
  end;
end;

procedure TNyDebitPaymentParams.Read(const NyDebitFile: INyDebitFile);
var
  EIN: string;
  LastPrInPeriod: TDateTime;
  PayrollsInPeriod: Integer;
  NyStateTax, NyCityTax, CityOfYorkersTax: Currency;
begin
  with MemStream do
  begin
    EIN := ReadString;
    LastPrInPeriod := ReadDouble;
    PayrollsInPeriod := ReadInteger;
    NyStateTax := ReadDouble;
    NyCityTax := ReadDouble;
    CityOfYorkersTax := ReadDouble;
  end;
  NyDebitFile.AddPayment(EIN, LastPrInPeriod, PayrollsInPeriod, NyStateTax, NyCityTax, CityOfYorkersTax);
end;

{ TTaxPaymentParams }

constructor TTaxPaymentParams.Create;
begin
  FAchPayments := TAchPaymentParams.Create;
  FEFTPS_Payment := TEFTPSPaymentParams.Create;
  FNyDebitFile := TNyDebitPaymentParams.Create;
  FMaDebitFile := TMaDebitPaymentParams.Create;
  FCaDebitFile := TCaDebitPaymentParams.Create;
  FUnivDebitFile := TUniversalDebitPaymentParams.Create;
  FOneMiscCheck := TOneMiscCheckPaymentParams.Create;
  FChecks := TisStream.Create;
  FWarnings := TStringList.Create;
  FTaxPayrollFilter := '';
end;

function TTaxPaymentParams.AsMemStream: IisStream;
begin
  Result := TisStream.Create;
  with Result do
  begin
    WriteInteger(ClNbr);
    WriteStream(FAchPayments.MemStream);
    WriteStream(FEFTPS_Payment.MemStream);
    WriteStream(FNyDebitFile.MemStream);
    WriteStream(FMaDebitFile.MemStream);
    WriteStream(FCaDebitFile.MemStream);
    WriteStream(FUnivDebitFile.MemStream);
    WriteStream(FOneMiscCheck.MemStream);
    WriteStream(FChecks);
    WriteString(FWarnings.CommaText);
    WriteString(FTaxPayrollFilter);
  end;
end;

constructor TTaxPaymentParams.Create(const Stream: IisStream);
begin
  Create;
  FClNbr := Stream.ReadInteger;
  Stream.ReadStream(FAchPayments.MemStream);
  Stream.ReadStream(FEFTPS_Payment.MemStream);
  Stream.ReadStream(FNyDebitFile.MemStream);
  Stream.ReadStream(FMaDebitFile.MemStream);
  Stream.ReadStream(FCaDebitFile.MemStream);
  Stream.ReadStream(FUnivDebitFile.MemStream);
  Stream.ReadStream(FOneMiscCheck.MemStream);
  Stream.ReadStream(FChecks);
  FWarnings.CommaText := Stream.ReadString;
  FTaxPayrollFilter := Stream.ReadString;
end;

destructor TTaxPaymentParams.Destroy;
begin
  FAchPayments.Free;
  FEFTPS_Payment.Free;
  FNyDebitFile.Free;
  FMaDebitFile.Free;
  FCaDebitFile.Free;
  FUnivDebitFile.Free;
  FOneMiscCheck.Free;
  FWarnings.Free;
  inherited;
end;

procedure TTaxPaymentParams.SetWarnings(const Value: TStringList);
begin
  FWarnings.Assign(Value);
end;

{ TMaDebitPaymentParams }

procedure TMaDebitPaymentParams.AddPayment(const RecordIdentifier: Char;
  const TaxPeriodEnd: TDateTime; const FEID, BusinessName: string;
  const Amount: Currency; const SettlementDate: TDateTime; const SbBankAccountNbr, ClBankAccountNbr: Integer);
var
  lSbBankNbr: Integer;
  lBankAccNumber, lAccountType: string;
begin
  if SbBankAccountNbr <> -1 then
  begin
    ctx_DBAccess.EnableReadTransaction;
    try
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
    finally
      ctx_DBAccess.DisableReadTransaction;
    end;
    Assert(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', SbBankAccountNbr, []));
    lSbBankNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.AsInteger;
    lBankAccNumber := StringReplace(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, '<', '', [rfReplaceAll]);
    lAccountType := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString;
  end
  else
  if ClBankAccountNbr <> -1 then
  begin
    DM_CLIENT.CL_BANK_ACCOUNT.Activate;
    Assert(DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', ClBankAccountNbr, []));
    lSbBankNbr := DM_CLIENT.CL_BANK_ACCOUNT.SB_BANKS_NBR.AsInteger;
    lBankAccNumber := StringReplace(DM_CLIENT.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, '<', '', [rfReplaceAll]);
    lAccountType := DM_CLIENT.CL_BANK_ACCOUNT.BANK_ACCOUNT_TYPE.AsString;
  end
  else
  begin
    Assert(False, 'Either client or Sb bank account has to passed');
    lSbBankNbr := 0; 
  end;
  MemStream.WriteBuffer(RecordIdentifier, SizeOf(RecordIdentifier));
  MemStream.WriteDouble(TaxPeriodEnd);
  MemStream.WriteString(FEID);
  MemStream.WriteString(BusinessName);
  MemStream.WriteCurrency(Amount);
  MemStream.WriteDouble(SettlementDate);
  MemStream.WriteInteger(lSbBankNbr);
  MemStream.WriteString(lBankAccNumber);
  Assert(lAccountType<>'');
  MemStream.WriteString(lAccountType[1]);
end;

procedure TMaDebitPaymentParams.Read(const MaDebitFile: IMaDebitFile);
var
  RecordIdentifier: Char;
  TaxPeriodEnd: TDateTime;
  FEID, BusinessName: string;
  Amount: Currency;
  SettlementDate: TDateTime;
  lSbBankNbr: Integer;
  lBankAccNumber, lAccountType: string;
begin
  MemStream.ReadBuffer(RecordIdentifier, SizeOf(RecordIdentifier));
  TaxPeriodEnd := MemStream.ReadDouble;
  FEID := MemStream.ReadString;
  BusinessName := MemStream.ReadString;
  Amount := MemStream.ReadCurrency;
  SettlementDate := MemStream.ReadDouble;
  lSbBankNbr := MemStream.ReadInteger;
  lBankAccNumber := MemStream.ReadString;
  lAccountType := MemStream.ReadString;
  MaDebitFile.AddPayment(RecordIdentifier, TaxPeriodEnd, FEID, BusinessName, Amount, SettlementDate, lSbBankNbr,
    lBankAccNumber, lAccountType[1]);
end;

{ TCaDebitPaymentParams }

procedure TCaDebitPaymentParams.AddPayment(const CheckDate,
  DueDate: TDateTime; const AccountNumber, SecurityCode,
  TaxTypeCode: string; const StateWithholding, Sdi: Currency);
begin
  MemStream.WriteDouble(CheckDate);
  MemStream.WriteDouble(DueDate);
  MemStream.WriteString(AccountNumber);
  MemStream.WriteString(SecurityCode);
  MemStream.WriteString(TaxTypeCode);
  MemStream.WriteCurrency(StateWithholding);
  MemStream.WriteCurrency(Sdi);
end;

procedure TCaDebitPaymentParams.Read(const CaDebitFile: ICaDebitFile);
var
  CheckDate, DueDate: TDateTime;
  AccountNumber, SecurityCode, TaxTypeCode: string;
  StateWithholding, Sdi: Currency;
begin
  CheckDate := MemStream.ReadDouble;
  DueDate := MemStream.ReadDouble;
  AccountNumber := MemStream.ReadString;
  SecurityCode := MemStream.ReadString;
  TaxTypeCode := MemStream.ReadString;
  StateWithholding := MemStream.ReadCurrency;
  Sdi := MemStream.ReadCurrency;
  CaDebitFile.AddPayment(CheckDate, DueDate, AccountNumber, SecurityCode, TaxTypeCode, StateWithholding, Sdi);
end;

{ TUniversalDebitPaymentParams }

procedure TUniversalDebitPaymentParams.AddPayment(const SyGlobalAgencyNbr, ClientId, GroupId,
  MainCoNbr: Integer; const DueDate : TDateTime; const Amount : Currency);
begin
  if FCd.Locate('GroupId', GroupId, []) then
  begin
    FCd.Edit;
    FCd.FieldByName('Amount').AsCurrency := FCd.FieldByName('Amount').AsCurrency + Amount;
  end
  else
  begin
   FCd.Append;
   FCd['SyGlobalAgencyNbr'] := SyGlobalAgencyNbr;
   FCd['ClientID'] := ctx_DataAccess.ClientID;
   FCd['GroupId'] := GroupId;
   FCd['MainCoNbr'] := MainCoNbr;
   FCd['DueDate'] := DueDate;
   FCd['Amount'] := Amount;
  end;
  FCd.Post;

end;

constructor TUniversalDebitPaymentParams.Create;
begin
  inherited;
  FCd := TEvClientDataSet.Create(nil);
  FCd.FieldDefs.Add('SyGlobalAgencyNbr', ftInteger);
  FCd.FieldDefs.Add('ClientID', ftInteger);
  FCd.FieldDefs.Add('GroupId', ftInteger);
  FCd.FieldDefs.Add('MainCoNbr', ftInteger);
  FCd.FieldDefs.Add('DueDate', ftDateTime);
  FCd.FieldDefs.Add('Amount', ftCurrency);
  FCd.CreateDataSet;
end;

destructor TUniversalDebitPaymentParams.Destroy;
begin
  FreeAndNil(FCd);
  inherited;
end;

procedure TUniversalDebitPaymentParams.FinalizePayments;
  function ListOfDepositLiabs(const ds: TevClientDataSet; const GroupId: Integer): string;
  begin
    Assert(not ds.Filtered);
    ds.Filter := 'CO_TAX_DEPOSITS_NBR='+ IntToStr(GroupId);
    ds.Filtered := True;
    try
      Result := GetFieldValueListString(ds, ds.TableName+ '_NBR');
    finally
      ds.Filtered := False;
      ds.Filter := '';
    end;
  end;
var
 SbBankAccNbr, BankAccRegNbr : integer;
begin

  FCd.First;
  while not FCd.Eof do
  begin
    if (DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(FCd.FieldByName('SyGlobalAgencyNbr').AsInteger, 'CUSTOM_DEBIT_POST_TO_REGISTER') = GROUP_BOX_YES) and
       (DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(FCd.FieldByName('SyGlobalAgencyNbr').AsInteger, 'STATE') <> 'NY') and
        (DM_COMPANY.CO.Locate('CO_NBR', FCd.FieldByName('MainCoNbr').AsInteger, [])) then
    begin
          if CompanyRequiresTaxesBeImpounded then
             SbBankAccNbr := DM_COMPANY.CO['TAX_SB_BANK_ACCOUNT_NBR']
          else
             SbBankAccNbr := DM_COMPANY.CO['TAX_CL_BANK_ACCOUNT_NBR'];

          ctx_PayrollCalculation.AddToBankAccountRegister(FCd.FieldByName('MainCoNbr').AsInteger,
                                     SbBankAccNbr,
                                     FCd.FieldByName('GroupId').AsInteger, -FCd.FieldByName('Amount').AsCurrency,
                                     BANK_REGISTER_STATUS_Outstanding,   BANK_REGISTER_TYPE_EFTPayment,
                                     DateOf(SysTime), FCd.FieldByName('DueDate').AsDateTime, DateOf(SysTime),
                                     BankAccRegNbr);

          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('GroupId').AsString);
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := True;
          DM_COMPANY.CO_SUI_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('GroupId').AsString);
          DM_COMPANY.CO_SUI_LIABILITIES.Filtered := True;
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('GroupId').AsString);
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := True;

          ctx_PayrollCalculation.AddConsolidatedPaymentsToBar(
                         [DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                           DM_COMPANY.CO_SUI_LIABILITIES,DM_COMPANY.CO_LOCAL_TAX_LIABILITIES],
                          FCd.FieldByName('GroupId').AsInteger,
                          BANK_REGISTER_STATUS_Outstanding,
                          BANK_REGISTER_TYPE_Consolidated, False);
    end;

    DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := '';
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := false;
    DM_COMPANY.CO_SUI_LIABILITIES.Filter := '';
    DM_COMPANY.CO_SUI_LIABILITIES.Filtered := false;
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := '';
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := false;

    MemStream.WriteInteger(FCd['SyGlobalAgencyNbr']);
    MemStream.WriteInteger(FCd['ClientID']);
    MemStream.WriteInteger(FCd['GroupId']);
    MemStream.WriteInteger(FCd['MainCoNbr']);
    MemStream.WriteDouble(FCd['DueDate']);
    MemStream.WriteCurrency(FCd['Amount']);
    MemStream.WriteString(ListOfDepositLiabs(DM_COMPANY.CO_STATE_TAX_LIABILITIES, FCd['GroupId']));
    MemStream.WriteString(ListOfDepositLiabs(DM_COMPANY.CO_SUI_LIABILITIES, FCd['GroupId']));
    MemStream.WriteString(ListOfDepositLiabs(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, FCd['GroupId']));
    FCd.Next;
  end;
end;

procedure TUniversalDebitPaymentParams.Read(
  const UniversalDebitPaymentParams: IUniversalDebitPaymentParams);
var
  ClNbr, SyGlobalAgencyNbr, GroupId, MainCoNbr: Integer;
  StateLiabNbrs, SuiLiabNbrs, LocalLiabNbrs: string;
  DueDate : TDateTime;
  Amount : Currency;
begin
  SyGlobalAgencyNbr := MemStream.ReadInteger;
  ClNbr := MemStream.ReadInteger;
  GroupId := MemStream.ReadInteger;
  MainCoNbr := MemStream.ReadInteger;
  DueDate := MemStream.ReadDouble;
  Amount := MemStream.ReadCurrency;
  StateLiabNbrs := MemStream.ReadString;
  SuiLiabNbrs := MemStream.ReadString;
  LocalLiabNbrs := MemStream.ReadString;
  UniversalDebitPaymentParams.AddPayment(SyGlobalAgencyNbr, ClNbr, GroupId, MainCoNbr,
                             DueDate, Amount, StateLiabNbrs, SuiLiabNbrs, LocalLiabNbrs);
end;



{ TOneMiscCheckPaymentParams }

procedure TOneMiscCheckPaymentParams.AddPayment(const SyGlobalAgencyNbr, ClientId,MainCoNbr,
  TaxDepositId, BankAccRegNbr, FieldOfficeNbr: Integer;
  const DueDate : TDateTime;
  const TotalAmount : Currency;
  const GroupKey : String);
begin
  if FCd.Locate('TaxDepositId', TaxDepositId, []) then
  begin
    FCd.Edit;
    FCd.FieldByName('TotalAmount').AsCurrency := FCd.FieldByName('TotalAmount').AsCurrency + TotalAmount;
  end
  else
  begin
    FCd.Append;
    FCd['SyGlobalAgencyNbr'] := SyGlobalAgencyNbr;
    FCd['ClientID'] := ctx_DataAccess.ClientID;
    FCd['MainCoNbr'] := MainCoNbr;
    FCd['TaxDepositId'] := TaxDepositId;
    FCd['BankAccRegNbr'] := BankAccRegNbr;
    FCd['FieldOfficeNbr'] := FieldOfficeNbr;
    FCd['DueDate'] := DueDate;
    FCd['TotalAmount'] := TotalAmount;
    FCd['GroupKey'] := GroupKey;
  end;
  FCd.Post;
end;

constructor TOneMiscCheckPaymentParams.Create;
begin
  inherited;
  FCd := TEvClientDataSet.Create(nil);
  FCd.FieldDefs.Add('SyGlobalAgencyNbr', ftInteger);
  FCd.FieldDefs.Add('ClientID', ftInteger);
  FCd.FieldDefs.Add('MainCoNbr', ftInteger);
  FCd.FieldDefs.Add('TaxDepositId', ftInteger);
  FCd.FieldDefs.Add('BankAccRegNbr', ftInteger);
  FCd.FieldDefs.Add('FieldOfficeNbr', ftInteger);
  FCd.FieldDefs.Add('DueDate', ftDateTime);
  FCd.FieldDefs.Add('TotalAmount', ftCurrency);
  FCd.FieldDefs.Add('GroupKey', ftString, 30, true);
  FCd.CreateDataSet;
end;

destructor TOneMiscCheckPaymentParams.Destroy;
begin
  FreeAndNil(FCd);
  inherited;
end;

procedure TOneMiscCheckPaymentParams.FinalizePayments;
var
 SbBankAccNbr, BankAccRegNbr : integer;
begin

  FCd.First;
  while not FCd.Eof do
  begin
    BankAccRegNbr := -1;
    if DM_COMPANY.CO.Locate('CO_NBR', FCd.FieldByName('MainCoNbr').AsInteger, []) then
    begin
          if CompanyRequiresTaxesBeImpounded then
             SbBankAccNbr := DM_COMPANY.CO['TAX_SB_BANK_ACCOUNT_NBR']
          else
             SbBankAccNbr := DM_COMPANY.CO['TAX_CL_BANK_ACCOUNT_NBR'];

          ctx_PayrollCalculation.AddToBankAccountRegister(FCd.FieldByName('MainCoNbr').AsInteger,
                                     SbBankAccNbr,
                                     FCd.FieldByName('TaxDepositId').AsInteger, -FCd.FieldByName('TotalAmount').AsCurrency,
                                     BANK_REGISTER_STATUS_Outstanding,   BANK_REGISTER_TYPE_Tax,
                                     DateOf(SysTime), FCd.FieldByName('DueDate').AsDateTime, DateOf(SysTime),
                                     BankAccRegNbr);

          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('TaxDepositId').AsString);
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := True;
          DM_COMPANY.CO_SUI_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('TaxDepositId').AsString);
          DM_COMPANY.CO_SUI_LIABILITIES.Filtered := True;
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(FCd.FieldByName('TaxDepositId').AsString);
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := True;

          ctx_PayrollCalculation.AddConsolidatedPaymentsToBar(
                         [DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                           DM_COMPANY.CO_SUI_LIABILITIES,DM_COMPANY.CO_LOCAL_TAX_LIABILITIES],
                          FCd.FieldByName('TaxDepositId').AsInteger,
                          BANK_REGISTER_STATUS_Outstanding,
                          BANK_REGISTER_TYPE_Consolidated, False);


    end;

    MemStream.WriteInteger(FCd['SyGlobalAgencyNbr']);
    MemStream.WriteInteger(FCd['ClientID']);
    MemStream.WriteInteger(FCd['MainCoNbr']);
    MemStream.WriteInteger(FCd['TaxDepositId']);
    MemStream.WriteInteger(BankAccRegNbr);

    MemStream.WriteInteger(FCd['FieldOfficeNbr']);
    MemStream.WriteDouble(FCd['DueDate']);
    MemStream.WriteCurrency(FCd['TotalAmount']);
    MemStream.WriteString(FCd['GroupKey']);
    FCd.Next;
  end;
end;


procedure TOneMiscCheckPaymentParams.Read(
  const OneMiscCheckPaymentParams: IOneMiscCheckPaymentParams);
var
  ClNbr, SyGlobalAgencyNbr, TaxDepositId, BankAccRegNbr, MainCoNbr,FieldOfficeNbr: Integer;
  DueDate : TDateTime;
  TotalAmount : Currency;
  GroupKey : String;
begin
  SyGlobalAgencyNbr := MemStream.ReadInteger;
  ClNbr := MemStream.ReadInteger;
  MainCoNbr := MemStream.ReadInteger;
  TaxDepositId := MemStream.ReadInteger;
  BankAccRegNbr := MemStream.ReadInteger;
  FieldOfficeNbr := MemStream.ReadInteger;
  DueDate := MemStream.ReadDouble;
  TotalAmount := MemStream.ReadCurrency;
  GroupKey := MemStream.ReadString;
  OneMiscCheckPaymentParams.AddPayment(SyGlobalAgencyNbr, ClNbr, MainCoNbr,
                         TaxDepositId, BankAccRegNbr,
                         FieldOfficeNbr,DueDate,TotalAmount,GroupKey);
end;

end.

