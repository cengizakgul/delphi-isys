unit EvDeferredCall;

interface

type
  TDeferredMethod0 = procedure of object;
  TDeferredMethod1 = procedure(param: Variant) of object;

procedure DeferredCall( m: TDeferredMethod0 ); overload;
procedure DeferredCall( m: TDeferredMethod1; param: Variant ); overload;

implementation
uses
  classes, windows, messages, forms, sysutils;

const
  WM_DEFERREDCALL = WM_USER;

type
  TCallInfo = class
    FNumArg: integer;
    FMethod: TMethod;
    FParam: Variant;
  public
    constructor Create( m: TDeferredMethod0); overload;
    constructor Create( m: TDeferredMethod1; param: Variant); overload;
  end;

  TMessageTarget = class
  private
    FWindow: HWND;
    procedure NeedWindow;
    procedure WndMethod(var Message: TMessage);
    function GetWindow: HWND;
  public
    property Window: HWND read GetWindow;
    destructor Destroy; override;
  end;

var
  GMessageTarget: TMessageTarget;


procedure DeferredCall( m: TDeferredMethod0 ); overload;
begin
  PostMessage(GMessageTarget.Window, WM_DEFERREDCALL, Integer(TCallInfo.Create(m)), 0 );
end;

procedure DeferredCall( m: TDeferredMethod1; param: Variant ); overload;
begin
  PostMessage(GMessageTarget.Window, WM_DEFERREDCALL, Integer(TCallInfo.Create(m, param)), 0 );
end;

{ TMessageTarget }

destructor TMessageTarget.Destroy;
begin
  if FWindow <> 0 then
    classes.DeallocateHWnd( FWindow );
  inherited;
end;

function TMessageTarget.GetWindow: HWND;
begin
  NeedWindow;
  Result := FWindow;
end;

procedure TMessageTarget.NeedWindow;
begin
  if FWindow = 0 then
    FWindow := classes.AllocateHWnd(WndMethod);
end;

procedure TMessageTarget.WndMethod(var Message: TMessage);
var
  ci: TCallInfo;
begin
  try
    if Message.Msg = WM_DEFERREDCALL then
    begin
      ci := nil;
      try
        ci := TCallInfo(Message.WParam);
        if ci.FNumArg = 0 then
          TDeferredMethod0(ci.FMethod)
        else if ci.FNumArg = 1 then
          TDeferredMethod1(ci.FMethod)( ci.FParam )
        else
          Assert(false);
      finally
        ci.Free;
      end;
    end
    else
      Message.Result := DefWindowProc(FWindow, Message.Msg, Message.wParam, Message.lParam);
  except
    Application.HandleException(Self);
  end
end;

{ TCallInfo }

constructor TCallInfo.Create(m: TDeferredMethod0);
begin
  FNumArg := 0;
  FMethod := TMethod(m);
end;

constructor TCallInfo.Create(m: TDeferredMethod1; param: Variant);
begin
  FNumArg := 1;
  FMethod := TMethod(m);
  FParam := param;
end;

initialization
  GMessageTarget := TMessageTarget.Create;

finalization
  FreeAndNil( GMessageTarget ); 

end.
