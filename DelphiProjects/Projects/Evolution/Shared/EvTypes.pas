// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvTypes;

interface

uses
  Classes, SysUtils, Variants, Messages, IsTypes, isBaseClasses,
  EvStreamUtils, EvConsts;

type
  TOnPrCheckLinesLogMessage = procedure(employeeNumber:string;employeeName:string;TypeOfEvent:string);

  IPrCheckLinesLoggerSetter = interface
  ['{6CD523C8-29A6-4803-A6DA-C2E6DEB4549A}']
    procedure SetLogger(ev:TOnPrCheckLinesLogMessage);
  end;

  TGraphicData = record
    FileExt: String;
    Data: IisStream;
  end;

  TAPIVendorKeyRec = record
    AppName: String;
    AppKey: String;
    VendorID: String;
    VendorName: String;
  end;

  TevSecElementState = Char;

  TLimitedTaxType = (ltEEOASDI, ltEROASDI, ltEEMedicare, ltERMedicare, ltFUI, ltEESDI, ltERSDI, ltSUI, ltDeduction);
  TCleanupPayrollAction = (cpaProcess, cpaQueue, cpaNone, cpaForceProcess);

  TControlType = (crComboBox, crDateEdit, crRadioGroup, crLookupCombo, crMemo, crNone, crEdit);
  TCheckType = (ctRegular, ctAgency, ctTax, ctBilling);

  TReportType = (rtUnknown, rtReport, rtCheck, rtASCIIFile, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR);
  TrwReportDestination = (rdtNone, rdtCancel, rdtPrinter, rdtPreview, rdtVMR, rdtPrepareVMR, rdtRemotePrinter,
                          rdtRemoteStorage);

  TrwPDFProtectionOption = (pdfPrint, pdfModifyStructure, pdfCopyInformation, pdfModifyAnnotation);
  TrwPDFProtectionOptions = set of TrwPDFProtectionOption;

  TrwPDFDocInfo = record
    Author:             String;
    Creator:            String;
    Subject:            String;
    Title:              String;
    Compression:        Boolean;
    ProtectionOptions:  TrwPDFProtectionOptions;
    OwnerPassword:      String;
    UserPassword:       String;
  end;

  TResourceLockType = (rlDoNotLock, rlTryOnce, rlTryUntilGet);

  TrwLayerNumber = 0..31;
  TrwPrintableLayers = set of TrwLayerNumber;

  //Time Clock import
  TCImportLookupOption = (loByName, loByNumber, loBySSN);
  TCImportDBDTOption = (loFull, loSmart);
  TCFileFormatOption = (loFixed, loCommaDelimited);

  TConnectionStatistic = record
    RawRead,
    RawWritten,
    CompressedRead,
    CompressedWritten,
    CPS: Cardinal;
    CurrentQueueMessage: string;
    AdminMessage: string;
  end;

  TRb28Param = packed record
    BegDate, EndDate: TDateTime;
    SortOrder, FirstSortField, FirstSortFieldColumn, TaxService, HeaderText: ShortString;
    DateField, DateFieldColumn: ShortString;
    pState, pFed, pFUI, pSUI, pLocal: Boolean
  end;
  TTaxTable = (ttFed, ttState, ttSUI, ttLoc);                   


  TevVendorKeyType = (
    tvkEvoSwipeClock,
    tvkEvoADI,
    tvkEvoQ,
    tvkEvoAloha,
    tvkEvoADIAdvanced,
    tvkEvoCheckDesigner,
    tvkEvoSwipeClockAdvanced,
    tvkEvoHire,
    tvkEvoHRISlink,
    tvkEvoDEMS,
    tvkEvoWorkersChoice,
    tvkEvoCRM,
    tvkEvoSlavic,
    tvkEvoInfinity,
    tvkEvoJBC,
    tvkEvoPayEarly,
    tvkEvoMyRecruitingCenter,
    tvkEvoAppraisal,
    tvkEvoAoD,
    tvkEvoM3,
    tvkEvoTacoBell
  );

const
  BoolToStr: array [Boolean] of ShortString = ('False', 'True');

  APIVendorKeys: array[Low(TevVendorKeyType).. High(TevVendorKeyType)] of TAPIVendorKeyRec = (
    (AppName: 'Evo Swipe Clock';           AppKey: '7BD84037E877497282AAE249214E5889'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo ADI';                   AppKey: 'F0B388DD829946C2B907C3A7E9E532C2'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Q';                     AppKey: '5B408E0E8C5A45C281DF32F292421F50'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Aloha';                 AppKey: 'A7EC03975A60478D845855A6CC71DBF3'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo ADI Advanced';          AppKey: 'EF34EB4164834C4A94CF10FCE89AE3D3'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Check Designer';        AppKey: '18C44361C57546B1BD06EA9F46B52D22'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Swipe Clock Advanced';  AppKey: '3BF7BA482B114156B387859D47357D37'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Hire';                  AppKey: 'FC8EC07026064E3A8CFD0209078EE361'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo HR IS Link';            AppKey: '2AE4A94AE3CF4CCD92F239D01114576C'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo DEMS';                  AppKey: '14742D293AF449349C0776F42D25F8C4'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Workers Choice';        AppKey: '2FD79336BB844142AB861AB1AAECF2DA'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo CRM';                   AppKey: '1FC4DEBF7A5A411A95F9643EDFED67C7'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Slavic';                AppKey: '2FF03CA07D864A4BA3BC5A1FC6FD6997'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Infinity';              AppKey: 'C7BF4595F6DC40D391BF67BAB7661E7B'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo JBC';                   AppKey: '633C8CFEB63F4CE8B2DAA4B7F4B9B869'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Elastic';               AppKey: 'B9C39C8BA053485C98499909772470EE'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo My Recruiting Center';  AppKey: '4FD12EE4AFF0433191159E7B43709FD5'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo Appraisal';             AppKey: '8428945913E14DDC83DF84382391F861'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo AoD';                   AppKey: 'BA5BBB00547A4A8B96FA14DEC7BB8379'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo M3';                    AppKey: 'AA63A5BE6C28492BA233E887A04F10C1'; VendorID: '1'; VendorName: 'ICP'),
    (AppName: 'Evo TacoBell';              AppKey: 'E8E883E1380E44F296DD94CA00E92306'; VendorID: '1'; VendorName: 'ICP')
  );

//SEG
type
  //TVmrReleaseType = (vrtNone,vrtPrinter,vrtSendVouchers,vrtPrinterVouchers,vrtStoreASCIIFiles,vrtAll);
  TVmrReleaseType = (vrtNone,vrtPrinter,vrtVouchers,vrtStoreASCIIFiles);
  TVmrReleaseTypeSet = set of TVmrReleaseType;

  TevDBType = (dbtUnknown, dbtUnspecified, dbtSystem, dbtBureau, dbtClient, dbtTemporary);
  TevDBTypes = set of TevDBType;

  TReportRequestType = (__DO_NOTHING, __DO_SEQUENTIAL, __DO_PARALLEL, __DO_COMBINE);


  TevDataChangeType = (dctUnknown, dctInsertRecord, dctUpdateFields, dctDeleteRecord);

  IevDataChangeItem = interface
  ['{9D5C43B9-DB49-4D25-B918-D856EEAEC1D9}']
    function  ChangeType: TevDataChangeType;
    function  DataSource: String;
    function  GetKey: Integer;
    procedure SetKey(const AValue: Integer);
    function  Fields: IisListOfValues;
    property  Key: Integer read GetKey write SetKey;
  end;


  IevDataChangePacket = interface
  ['{B36B32F7-6BD5-4544-88B6-0435802CFFF2}']
    procedure SetAsOfDate(const AValue: TDateTime);
    function  GetAsOfDate: TDateTime;
    function  NewInsertRecord(const ADataSource: String): IevDataChangeItem;
    function  NewUpdateFields(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
    function  NewDeleteRecord(const ADataSource: String; const AKey: Integer): IevDataChangeItem;
    procedure MergePacket(const APacket: IevDataChangePacket);
    procedure RemoveItem(const AItem: IevDataChangeItem);
    function  GetItem(Index: Integer): IevDataChangeItem;
    function  Count: Integer;
    property  Items[Index: Integer]: IevDataChangeItem read GetItem; default;
    property  AsOfDate: TDateTime read GetAsOfDate write SetAsOfDate;
  end;

implementation

end.

