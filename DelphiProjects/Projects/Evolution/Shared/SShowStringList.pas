// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SShowStringList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  EvUIUtils, ISBasicClasses, EvUIComponents;

type
  TShowStringList = class(TForm)
    BitBtn1: TevBitBtn;
    TitleLabel: TevLabel;
    Memo1: TevMemo;
    BitBtn2: TevBitBtn;
    procedure BitBtn2Click(Sender: TObject);
  private
  public
  end;

procedure DoShowStringList(aStringList: TStringList; Title: string = '');

implementation

{$R *.DFM}

procedure DoShowStringList(aStringList: TStringList; Title: string = '');
var
  Frm: TShowStringList;
begin
 Frm := TShowStringList.Create(Application);
 try
   with Frm do
   begin
     Memo1.Lines := aStringList;
     TitleLabel.Caption := Title;
     ShowModal;
   end;
  finally
    Frm.Free
  end;
end;

procedure TShowStringList.BitBtn2Click(Sender: TObject);
begin
  EvMessage('Printing has not been implemented yet');
end;

end.
