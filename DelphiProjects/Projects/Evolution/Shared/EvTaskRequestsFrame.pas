unit EvTaskRequestsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isBaseClasses, ComCtrls, ISBasicClasses,  evCommonInterfaces, EvUIComponents;

type
  TTaskRequestsFrame = class(TFrame)
    lvTaskRequests: TevListView;
  private
  public
    constructor Create(const AOwner: TComponent; const AParent: TWinControl; const ARequestList: IisListOfValues); reintroduce; virtual;
    procedure UpdateContent(const ARequestList: IisListOfValues);
  end;

  TTaskRequestsFrameClass = class of TTaskRequestsFrame;

implementation

{$R *.dfm}

{ TTaskRequestsFrame }

constructor TTaskRequestsFrame.Create(const AOwner: TComponent;
  const AParent: TWinControl; const ARequestList: IisListOfValues);
begin
  inherited Create(AOwner);
  UpdateContent(ARequestList);
  Parent := AParent;
end;

procedure TTaskRequestsFrame.UpdateContent(const ARequestList: IisListOfValues);
var
  R: IevTaskRequestInfo;
  i: Integer;
  s: String;
begin
  lvTaskRequests.Items.BeginUpdate;
  try
    lvTaskRequests.Items.Clear;
    for i := 0 to ARequestList.Count-1 do
      with lvTaskRequests.Items.Add do
      begin
        R := IInterface(ARequestList[i].Value) as IevTaskRequestInfo;
        Caption := R.Name;
        SubItems.Add(R.Status);

        if (R.FinishedAt = 0) and (R.StartedAt <> 0 )then
          SubItems.Add(FormatDateTime('mm/dd/yy hh:nn:ss am/pm', R.StartedAt))
        else
          SubItems.Add('');

        s := R.ProgressText;
        s := StringReplace(s, #13, ' ', [rfReplaceAll]);
        SubItems.Add(s);
      end;
  finally
    lvTaskRequests.Items.EndUpdate;
  end;
end;

end.
