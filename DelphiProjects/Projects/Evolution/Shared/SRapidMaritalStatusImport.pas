// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SRapidMaritalStatusImport;

interface

function FindMaritalStatus(State, Status, RapidCode, Local: String; Dep: Integer): String;
function GetStatusForStateAF(State, Status, RapidCode, Local: String; Dep: Integer): String;
function GetStatusForStateGL(State, Status, RapidCode, Local: String; Dep: Integer): String;
function GetStatusForStateMR(State, Status, RapidCode, Local: String; Dep: Integer): String;
function GetStatusForStateSW(State, Status, RapidCode, Local: String; Dep: Integer): String;

implementation

function FindMaritalStatus(State, Status, RapidCode, Local: String; Dep: Integer): String;
begin
  if State <> '' then

    if (Pos('*', RapidCode) = 1) and (Length(RapidCode) > 1) then
    begin
      Result := Copy(RapidCode, 2, Length(RapidCode));
      Exit;
    end;

    case State[1] of
      'A'..'F':
      begin
        result := GetStatusForStateAF(State, Status, RapidCode, Local, Dep);
      end;
      'G'..'L':
      begin
        result := GetStatusForStateGL(State, Status, RapidCode, Local, Dep);
      end;
      'M'..'R':
      begin
        result := GetStatusForStateMR(State, Status, RapidCode, Local, Dep);
      end;
      'S'..'W':
      begin
        result := GetStatusForStateSW(State, Status, RapidCode, Local, Dep);
      end;
    end;
end;

function GetStatusForStateAF(State, Status, RapidCode, Local: String; Dep: Integer): String;
begin
  if State = 'AL' then
  begin
    case Status[1] of
      'S':
      begin
        if (Dep = 0) and (RapidCode = '0') then
          result := '0'
        else if Dep > 0 then
          Result := 'S'
      end;
      'M':
      begin
        if (Dep = 0) and (RapidCode = '0') then
          Result := '0'
        else if Dep > 0 then
          Result := 'M'
      end;
      'H':
      begin
        Result := 'H';
      end;
    end;
  end
  else if State = 'AZ' then
  begin
    if RapidCode = '1' then
      Result := '10'
    else if RapidCode = '2' then
      Result := '23'
    else if RapidCode = '3' then
      Result := '25'
    else if RapidCode = '4' then
      Result := '31'
    else if RapidCode = '5' then
      Result := '37'
    else if RapidCode = '6' then
      Result := '19';
  end
  else if State = 'AR' then
  begin
    case Status[1] of
      'S':
      begin
        if Dep > 0 then
          Result := 'A'
        else
          Result := 'SO';
      end;
      'M':
      begin
        if (Dep > 0) and (RapidCode = 'J') then
          Result := 'C'
        else if (Dep > 0) then
          Result := 'B'
        else
          Result := 'SO';
      end;
    end;
  end
  else if State = 'AK' then
  begin
      Result := Status;
  end
  else if State = 'CA' then
  begin
    case Status[1] of
      'S': Result := 'SM';
      'M':
       begin
         if Dep < 2 then
           Result := 'M'
         else
           Result := 'M2'
       end;
      'H': Result := 'H';
    end;
  end
  else if State = 'CO' then
  begin
    Result := Status;
  end
  else if State = 'CT' then
  begin
    Result := RapidCode;
  end
  else if State = 'DE' then
  begin
    if Status = 'S' then
      Result := 'S'
    else if (Status = 'M') and (RapidCode = 'S') then
      Result := 'A'
    else
      Result := 'M';
  end
  else if State = 'DC' then
  begin
    if (Status = 'S') or (Status = 'H')then
      Result := 'S'
    else if RapidCode = 'J' then
      Result := 'M'
    else
      Result := 'A';
  end
  else if State = 'FL' then
  begin
    if (Status = 'S') or (Status = 'H')then
      Result := 'S'
    else
      Result := 'M'
  end;
end;

function GetStatusForStateGL(State, Status, RapidCode, Local: String; Dep: Integer): String;
begin
  if State = 'GA' then
  begin
    if Rapidcode = 'B' then
      Result := 'B1'
    else
      Result := RapidCode;
  end
  else if State = 'HI' then
  begin
    if Status = 'H' then
      Result  := 'S'
    else
      Result := Status;
  end
  else if State = 'IA' then
  begin
    if Dep < 2 then
      Result := 'S'
    else
      Result := 'S2';
  end
  else if State = 'ID' then
  begin
    if Status = 'H' then
      Result := 'S'
    else
      Result := Status;
  end
  else if State = 'IL' then
  begin
    if Status = 'M' then
      Result := Status
    else
      Result := 'S';
   { if Dep = 0 then
      Result := '0'
    else if Dep = 1 then
      Result := 'S'
    else
      Result := 'M';}
  end
  else if State = 'IN' then
  begin
    if Status = 'H' then
      Result := 'S'
    else
      Result := Status;
  end
  else if State = 'KS' then
  begin
    if Status = 'H' then
      Result := 'S'
    else
      Result := Status;
  end
  else if State = 'KY' then
  begin
    if Status = 'H' then
      Result := 'S'
    else
      Result := Status;
  end
  else if State = 'LA' then
  begin
    if Dep = 0 then
      Result := '0'
    else if (RapidCode = '0') or (RapidCode = '1') then
      Result := '1'
    else
      Result := '2';
  end;
end;

function GetStatusForStateMR(State, Status, RapidCode, Local: String; Dep: Integer): String;
begin
  if State = 'ME' then
  begin
    if (Status = 'S') or (Status = 'H') then
      Result := 'S'
    else if RapidCode = 'O' then
      Result := 'A'
    else
      Result := 'M';
  end
  else if State = 'MD' then
  begin
    if Local = 'WORCESTER' then
      Result := 'RA'
    else if Local = 'TALBOT' then
      Result := 'RB'
    else if Local = 'HOWARD' then
      Result := 'RC'
    else if (Local = 'BALTIMORE CITY') or (Local = 'ANNE ARUNDEL') or (Local = 'CALVERT') or
            (Local = 'CECIL') or (Local = 'DORCHESTER') or (Local = 'FREDERICK') or
            (Local = 'GARRET') or (Local = 'HARFORD') or (Local = 'KENT') or (Local = 'WASHINGTON') then
      Result := 'RD'
    else if (Local = 'BALTIMORE CNTY') or (Local = 'CAROLINE') or (Local = 'CARROLL') or
            (Local = 'QUEEN ANNE') or (Local = 'BALTIMORE CITY') then
      Result := 'RE'
    else if (Local = 'ALLEGANY') or (Local = 'CHARLES') then
      Result := 'RF'
    else if (Local = 'MONTGOMERY') or (Local = 'ST. MARYS') then
      Result := 'RG'
    else if (Local = 'PRINCE GEORGE') or (Local = 'SOMERSET') or (Local = 'WICOMICO') then
      Result := 'RH';
  end
  else if State = 'MA' then
  begin
    if (Status = 'H') then
      case Dep of
        0: Result := 'H0';
        1: Result := 'H1';
      else
        Result := 'H2'
      end
    else
      case Dep of
        0: Result := '0';
        1: Result := '1';
      else
        Result := '2'
      end;
  end
  else if State = 'MI' then
  begin
    Result := 'SM';
  end
  else if State = 'MN' then
  begin
    Result := Status;
  end
  else if State = 'MS' then
  begin
    if Status = 'S' then
      Result := 'S' //Need more clarification
    else
      Result := 'M';
  end
  else if State = 'MO' then
  begin
    if Status = 'M' then
      if RapidCode = '1' then
        Result := 'M1'
      else
        Result := 'M0'
    else if Status = 'H' then
      if Dep < 5 then
        Result := 'H'
      else
        Result := 'H2'
    else
      Result := 'S';
  end
  else if State = 'MT' then
  begin
    Result := Status;
  end
  else if State = 'NE' then
  begin
    Result := Status;
  end
  else if State = 'NH' then
  begin
    Result := Status;
  end
  else if State = 'NJ' then
  begin
    if RapidCode = '' then
      Result := 'A'
    else
      Result := RapidCode;
  end
  else if State = 'NM' then
  begin
    Result := Status;
  end
  else if State = 'NY' then
  begin
    if Status = 'H' then
      Result := 'S'
    else
      Result := Status;
  end
  else if State = 'NC' then
  begin
    if Status = 'M' then
      Result := 'MW'
    else if Status = 'H' then
      Result := 'HH'
    else
      Result := 'S';
  end
  else if State = 'ND' then
  begin
    if Status = 'M' then
      Result := 'M'
    else
      Result := 'S';
  end
  else if State = 'NV' then
  begin
    if (Status = 'S') or (Status = 'H') then
      Result := 'S'
    else
      Result := 'M';
  end
  else if State = 'OH' then
  begin
    Result := 'SM';
  end
  else if State = 'OK' then
  begin
    if (Status = 'M') and (RapidCode = 'D') then
      Result := 'D'
    else if Status = 'S' then
      Result := 'S'
    else
      Result := 'M';
  end
  else if State = 'OR' then
  begin
    if (Status = 'M') or (Status = 'H') then
      Result := 'M'
    else if Dep < 4 then
      Result  := 'S'
    else
      Result := 'M';
  end
  else if State = 'PA' then
  begin
    Result := 'SM';
  end
  else if State = 'PR' then
  begin
    if RapidCode = 'B' then
      Result := 'S'
    else if RapidCode = 'J' then
      Result := 'MH'
    else if RapidCode = 'O' then
      Result := 'M'
    else
      Result := 'MS';
  end
  else if State = 'RI' then
  begin
    Result := Status;
  end;

end;

function GetStatusForStateSW(State, Status, RapidCode, Local: String; Dep: Integer): String;
begin
  if State = 'SC' then
  begin
    if Dep = 0 then
      Result := '0'
    else
      Result := '1';
  end
  else if State = 'SD' then
  begin
    Result := Status;
  end
  else if State = 'TN' then
  begin
    Result := Status;
  end
  else if State = 'TX' then
  begin
    Result := Status;
  end
  else if State = 'UT' then
  begin
    Result := Status;
  end
  else if State = 'VA' then
  begin
    Result := 'SM';
  end
  else if State = 'VT' then
  begin
    Result := Status;
  end
  else if State = 'WA' then
  begin
    Result := Status;
  end
  else if State = 'WI' then
  begin
    if Status = 'S' then
      if Dep = 0 then
        Result := 'S0'
      else
        Result := 'S'
    else if Status = 'M' then
      if Dep = 0 then
        Result := 'M0'
      else
        Result := 'M';
  end
  else if State = 'WV' then
  begin
    Result := Status;
  end
  else if State = 'WY' then
  begin
    Result := Status;
  end;
end;

end.
