unit EvExchangeMapFileExt;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils, isBasicUtils,
     EvConsts, EvStreamUtils, EvTypes, rwCustomDataDictionary, EvExchangeMapFile,
     MSXML2_TLB, EvExchangeXMLUtils, EvExchangeConsts, rwEngineTypes, DIMime,
     EvImportMapRecord;

type
  IEvExchangeMapFileExt = interface(IEvExchangeMapFile)
  ['{1F7C4BC1-7141-4304-B302-215571FA4ABA}']
    procedure LoadFromXMLFile(const AFileName : String);
    procedure LoadFromXMLString(const AString: String);
  end;

  TEvExchangeMapFileExt = class(TEvExchangeMapFile, IEvExchangeMapFileExt)
    FXMLDoc: IXMLDOMDocument2;
    FFileName: String;
  protected
    procedure InitXMLDoc;
    procedure DoOnConstruction; override;
    procedure Parse;
    procedure CheckAndFixExamplePathFromFile(const AFileName: String);
    function  CheckAndFixExamplePathFromString(const AString: String): String;
    procedure CheckForErrors;

    // IEvExchangeMapFileExt implementation
    procedure LoadFromXMLFile(const AFileName: String);
    procedure LoadFromXMLString(const AString: String);
  end;

implementation

{ TEvExchangeMapFileExt }

procedure TEvExchangeMapFileExt.CheckAndFixExamplePathFromFile(const AFileName: String);
var
  StringList: IisStringList;
begin
  StringList := TisStringList.Create;
  StringList.LoadFromFile(AFileName);
  StringList.Text := CheckAndFixExamplePathFromString(StringList.Text);
  StringList.SaveToFile(AFileName);
end;

function TEvExchangeMapFileExt.CheckAndFixExamplePathFromString(const AString: String): String;
var
  iStartTagPos, iEndTagPos: integer;
  S: String;
begin
  Result := AString;
  iStartTagPos := Pos('<' + EvoXmapExampleFileNameTag + '>', Result);
  iEndTagPos := Pos('</' + EvoXmapExampleFileNameTag + '>', Result);
  if (iStartTagPos > 0) and (iEndTagPos > 0) then
  begin
    iStartTagPos := iStartTagPos + Length(EvoXmapExampleFileNameTag) + 2;
    S := Copy(Result, iStartTagPos , iEndTagPos - iStartTagPos);
    if S <> '' then
      Result := StringReplace(Result, '<' + EvoXmapExampleFileNameTag + '>' + S + '</' + EvoXmapExampleFileNameTag + '>',
        '<' + EvoXmapExampleFileNameTag + '>' + EvoXCDataStart + S + EvoXCDataEnd + '</' + EvoXmapExampleFileNameTag + '>', []);
  end;
end;

procedure TEvExchangeMapFileExt.CheckForErrors;
var
  XMLError: IXMLDOMParseError;
begin
  XMLError := FXMLDoc.parseError;
  try
    If XMLError.ErrorCode <> 0 Then
    begin
      raise Exception.Create('Cannot load map file. It has spoiled XML structure' + #13#10 +
        'Line: ' + IntToStr(XMLError.Line) +' Pos: ' + IntToStr(XMLError.linepos) + ' Error: ' +
        XMLError.reason + #13#10 + 'Text: ' + XMLError.srcText);
    end;
  finally
    XMLError := nil;
  end;
end;

procedure TEvExchangeMapFileExt.DoOnConstruction;
begin
  inherited;
end;

procedure TEvExchangeMapFileExt.InitXMLDoc;
begin
  try
    FXMLDoc := CoDOMDocument60.Create;
  except
    on E: Exception do
    begin
      E.Message := 'Library MSXML 6.0 or higher is required' + #13#13 + E.Message;
      raise;
    end;
  end;
  FXMLDoc.validateOnParse := true;
  FXMLDoc.setProperty('SelectionLanguage', 'XPath');
end;

procedure TEvExchangeMapFileExt.LoadFromXMLFile(const AFileName: String);
var
  tmpList: IisStringList;
begin
  tmpList := TisStringList.Create;
  tmpList.LoadFromFile(AFileName);
  LoadFromXMLString(tmpList.Text);
  FFileName := AFileName;
end;

procedure TEvExchangeMapFileExt.LoadFromXMLString(const AString: String);
var
  S: String;
begin
  InitXMLDoc;
  try
    Clear;

    FXMLDoc.loadXML(AString);
    FFileName := '';

    if not Assigned(FXMLDoc.documentElement) then
    begin
      S := CheckAndFixExamplePathFromString(AString);
      FXMLDoc.loadXML(S);
    end;

    CheckForErrors;
    CheckCondition(Assigned(FXMLDoc.documentElement), 'XML document has spoiled structure.');
    CheckCondition(FXMLDoc.documentElement.nodeName = EvoXevExchangeMapFileTag, 'XML document has spoiled structure.');

    Parse;
    DoReconcile;
  finally
    FXMLDoc := nil;
  end;
end;

procedure TEvExchangeMapFileExt.Parse;
var
  tmpNode, DataSourceNode, FieldNode, MapRecordNode: IXMLDOMNode;
  FieldsList, DataSourceOptionsList, MapFileOptionsList : IXMLDOMNodeList;
  DataSourceOptions, MapFileOptions : IisStringList;
  MapField: IevEchangeMapField;
  i: integer;
  sFieldType: String;
  S, sTmp: String;
  ExampleDataStream: IisStream;
  MapRecord: IevImportMapRecord;
begin
  // general information
  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapDescriptionTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Map file''s <' + EvoXmapDescriptionTag +'> tag is missed');
  SetDescription(tmpNode.Text);

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapPackageNameTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Map file''s <' + EvoXmapPackageNameTag +'> tag is missed');
  SetPackageName(tmpNode.Text);

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapPackageVersionTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Map file''s <' + EvoXmapPackageVersionTag +'> tag is missed');
  SetPackageVersion(StrToInt(tmpNode.Text));

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapExampleFileNameTag);
  if Assigned(tmpNode) then
    SetExampleFileName(tmpNode.Text);

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapExampleDataTag);
  if Assigned(tmpNode) then
  begin
    S := tmpNode.text;
    S := MimeDecodeString(S);
    ExampleDataStream := TisStream.Create;
    ExampleDataStream.WriteBuffer(S[1], Length(S));
    SetExampleData(ExampleDataStream);
  end;

  // map file options
  MapFileOptionsList := FXMLDoc.documentElement.SelectNodes(EvoXMapFileOptionsPath);
  if Assigned(MapFileOptionsList) then
  begin
    MapFileOptions := TisStringList.Create;
    for i := 0 to MapFileOptionsList.Length - 1 do
      MapFileOptions.Add(MapFileOptionsList[i].Text);
    SetMapFileOptions(MapFileOptions);
  end;

  // data source
  DataSourceNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXmapDataSourceTag);
  CheckCondition(Assigned(DataSourceNode), 'Error in XML document: Map file''s <' + EvoXmapDataSourceTag +'> tag is missed');

  tmpNode := FindChildNodebyName(DataSourceNode, EvoXmapdataSourceNameTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Map file''s <' + EvoXmapdataSourceNameTag +'> tag is missed');
  SetDataSourceName(tmpNode.Text);

  DataSourceOptionsList := DataSourceNode.SelectNodes(EvoXmapDataSourceOptionsPath);
  if Assigned(DataSourceOptionsList) then
  begin
    DataSourceOptions := TisStringList.Create;
    for i := 0 to DataSourceOptionsList.Length - 1 do
      DataSourceOptions.Add(DataSourceOptionsList[i].Text);
    SetDataSourceOptions(DataSourceOptions);
  end;

  // fields
  FieldsList := FXMLDoc.documentElement.SelectNodes(EvoXmapFieldsPath);
  CheckCondition(Assigned(FieldsList), 'Error in XML document: no map fields definitions found');
  for i := 0 to FieldsList.length - 1 do
    if FieldsList[i].NodeName = EvoXFieldTag then
    begin
      FieldNode := FieldsList[i];
      MapField := TevEchangeMapField.Create;

      tmpNode := FindChildNodebyName(FieldNode, EvoXmapInputFieldNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapInputFieldNameTag + '> tag is missed');
      MapField.SetInputFieldName(tmpNode.Text);

      tmpNode := FindChildNodebyName(FieldNode, EvoXmapFieldTypeTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapFieldTypeTag + '> tag is missed');
      sFieldType := trim(tmpNode.Text);

      if sFieldType = EvoXddtUnknown then
        MapField.SetFieldType(ddtUnknown)
      else if sFieldType = EvoXddtInteger then
        MapField.SetFieldType(ddtInteger)
      else if sFieldType = EvoXddtFloat then
        MapField.SetFieldType(ddtFloat)
      else if sFieldType = EvoXddtCurrency then
        MapField.SetFieldType(ddtCurrency)
      else if sFieldType = EvoXddtDateTime then
        MapField.SetFieldType(ddtDateTime)
      else if sFieldType = EvoXddtString then
        MapField.SetFieldType(ddtString)
      else if sFieldType = EvoXddtBoolean then
        MapField.SetFieldType(ddtBoolean)
      else if sFieldType = EvoXddtBLOB then
        MapField.SetFieldType(ddtBLOB)
      else
        raise Exception.Create('Unsupported data type: ' + sFieldType);


      if MapField.GetFieldType = ddtDateTime then
      begin
        tmpNode := FindChildNodebyName(FieldNode, EvoXmapFieldDateFormatTag);
        if Assigned(tmpNode) then
          MapField.SetDateFormat(tmpNode.Text);
        tmpNode := FindChildNodebyName(FieldNode, EvoXmapFieldTimeFormatTag);
        if Assigned(tmpNode) then
          MapField.SetTimeFormat(tmpNode.Text);
      end;

      if MapField.GetFieldType = ddtString then
      begin
        tmpNode := FindChildNodebyName(FieldNode, EvoXmapFieldSizeTag);
        if Assigned(tmpNode) then
          MapField.SetFieldSize(StrToInt(tmpNode.Text));
      end;

      tmpNode := FindChildNodebyName(FieldNode, EvoXExchangeFieldNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXExchangeFieldNameTag + '> tag is missed');
      MapField.SetExchangeFieldName(tmpNode.Text);

      tmpNode := FindChildNodebyName(FieldNode, EvoXgroupRecordNumberTag);
      if Assigned(tmpNode) then
        MapField.SetGroupRecordNumber(StrToInt(tmpNode.Text));

      tmpNode := FindChildNodebyName(FieldNode, EvoXasOfDateEnabledTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        MapField.SetAsOfDateEnabled(true)
      else
        MapField.SetAsOfDateEnabled(false);

      if MapField.GetAsOfDateEnabled then
      begin
        tmpNode := FindChildNodebyName(FieldNode, EvoXasOfDateSourceTag);
        CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXasOfDateSourceTag + '> tag is missed');
        MapField.SetAsOfDateSource(TEvoXAsOfDateSourceType(StrToInt(tmpNode.Text)));

        if MapField.GetAsOfDateSource = stDateEditor then
        begin
          tmpNode := FindChildNodebyName(FieldNode, EvoXasOfDateValueTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXasOfDateValueTag + '> tag is missed');
          MapField.SetAsOfDateValue(StrToFloat(tmpNode.Text))
        end
        else
        begin
          tmpNode := FindChildNodebyName(FieldNode, EvoXasOfDateFieldTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXasOfDateFieldTag + '> tag is missed');
          MapField.SetAsOfDateField(tmpNode.Text);

          tmpNode := FindChildNodebyName(FieldNode, EvoVasOfDateFieldDateFormat);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoVasOfDateFieldDateFormat + '> tag is missed');
          MapField.SetAsOfDateFieldDateFormat(tmpNode.Text);
        end;
      end;

      // map record
      MapRecordNode := FindChildNodebyName(FieldNode, EvoXmapRecordTag);
      if Assigned(MapRecordNode) then
      begin
        MapRecord := TevImportMapRecord.CreateEmpty;

        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapRecordNameTag);
        if Assigned(tmpNode) then
          MapRecord.SetMapRecordName(tmpNode.Text);


        // description
        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapRecordDescriptionTag);
        if Assigned(tmpNode) then
          MapRecord.Description := tmpNode.text
        else
          MapRecord.Description := MapRecord.GetMapRecordName;

        // type
        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapTypeTag);
        CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapTypeTag + '> tag is missed');
        if tmpNode.Text = 'mtpDirect' then
          MapRecord.MapType := mtpDirect
        else if tmpNode.Text = 'mtpMap' then
          MapRecord.MapType := mtpMap
        else if tmpNode.Text = 'mtpCustom' then
          MapRecord.MapType := mtpCustom
        else
          Assert(false, 'Error in XML document: Field''s <' + EvoXmapTypeTag + '> tag has wrong value: ' + tmpNode.Text);

        // other staff
        case Maprecord.MapType of
          mtpMap:       begin
                        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapValuesTag);
                        CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                          EvoXmapValuesTag + '> tag is missed');
                        sTmp := tmpNode.Text;
                        MapRecord.MapValues := sTmp;

                        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapDefaultTag);
                        CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                          EvoXmapDefaultTag + '> tag is missed');
                        MapRecord.MapDefault := tmpNode.Text;
                      end;
        mtpCustom:    begin
                        tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapFuncNameTag);
                        CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                          EvoXmapFuncNameTag + '> tag is missed');
                        MapRecord.FuncName := tmpNode.Text;
                      end;
        end;  // case
        MapField.SetMapRecord(MapRecord);
      end;

      MapField.DoReconcile;
      AddField(MapField);
    end;  // for
    
  DoReconcile;
end;

end.
