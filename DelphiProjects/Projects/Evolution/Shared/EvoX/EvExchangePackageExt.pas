unit EvExchangePackageExt;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils, isBasicUtils,
     EvConsts, EvStreamUtils, EvTypes, rwCustomDataDictionary, EvExchangePackage,
     MSXML2_TLB, EvExchangeXMLUtils, EvExchangeConsts, IEMap, EvImportMapRecord;

type
  IEvExchangePackageExt = interface(IEvExchangePackage)
  ['{9DB99636-8337-41C0-A4DE-D7FEA62AD6EE}']
    procedure LoadFromXMLFile(const AFileName : String);
    procedure LoadFromXMLString(const AXMLString: String);
  end;

  TEvExchangePackageExt = class(TEvExchangePackage, IEvExchangePackageExt)
  private
    FXMLDoc: IXMLDOMDocument2;
    FFileName: String;
  protected
    procedure InitXMLDoc;
    procedure DoOnConstruction; override;
    procedure Parse;
    procedure CheckForErrors;

    // IEvExchangePackageExt implementation
    procedure LoadFromXMLFile(const AFileName: String);
    procedure LoadFromXMLString(const AXMLString: String);
  end;

implementation

{ TEvExchangePackageExt }

procedure TEvExchangePackageExt.CheckForErrors;
var
  XMLError: IXMLDOMParseError;
begin
  XMLError := FXMLDoc.parseError;
  try
    If XMLError.ErrorCode <> 0 Then
    begin
      raise Exception.Create('Cannot load package file. It has spoiled XML structure' + #13#10 +
        'Line: ' + IntToStr(XMLError.Line) +' Pos: ' + IntToStr(XMLError.linepos) + ' Error: ' +
        XMLError.reason + #13#10 + 'Text: ' + XMLError.srcText);
    end
  finally
    XMLError := nil;
  end;
end;

procedure TEvExchangePackageExt.DoOnConstruction;
begin
  inherited;
end;

procedure TEvExchangePackageExt.InitXMLDoc;
begin
  try
    FXMLDoc := CoDOMDocument60.Create;
  except
    on E: Exception do
    begin
      E.Message := 'Library MSXML 6.0 or higher is required' + #13#13 +
        'Please restart Evolution client after installing MSXML' + #13#13 + E.Message;
      raise;
    end;
  end;
  FXMLDoc.validateOnParse := true;
  FXMLDoc.setProperty('SelectionLanguage', 'XPath');
end;

procedure TEvExchangePackageExt.LoadFromXMLFile(const AFileName: String);
var
  tmpList: IisStringList;
begin
  tmpList := TisStringList.Create;
  tmpList.LoadFromFile(AFileName);
  LoadFromXMLString(tmpList.Text);
  FFileName := AFileName;
end;

procedure TEvExchangePackageExt.LoadFromXMLString(const AXMLString: STring);
begin
  InitXMLDoc;
  try
    Clear;

    FXMLDoc.LoadXML(AXMLString);
    FFileName := '';

    CheckForErrors;
    CheckCondition(Assigned(FXMLDoc.documentElement), 'XML document has spoiled structure');
    CheckCondition(FXMLDoc.documentElement.nodeName = EvoXEvExchangePackageTag, 'XML document has spoiled structure');

    Parse;
    DoReconcile;
  finally
    FXMLDoc := nil;
  end;
end;

procedure TEvExchangePackageExt.Parse;
var
  tmpNode, FieldNode, MapRecordNode, KeyNode, GroupNode, SectionNode, LookupDataNode: IXMLDOMNode;
  FieldsList, MapRecordsList, KeysList, GroupsList, SectionsList : IXMLDOMNodeList;
  i, j : integer;
  sExchangeFieldName: String;
  sEvTableName: String;
  sEvFieldName: String;
  sRelatedParentField: String;
  sDescription: String;
  sMapRecordName: String;
  sDefaultMapRecord: String;
  sPackageMapRecord: String;
  sFieldDisplayName: String;
  sGroupName: String;
  sPackageGroupName: String;
  sPackageGroupDisplayName: String;
  sPackageSectionName: String;
  sPackageSectionDisplayName: String;
  sPreFormatFunctionName: String;
  bCalcRequired: boolean;
  bCalcdefault: boolean;
  bIsKeyField: boolean;
  bIgnoreRequiredByDD: boolean;
  bCheckRequiredByDD: boolean;
  bRequiredIfAnyMapped: boolean;
  bRequiredIfAnyOtherMapped: String;
  bUserEvoNamesForMap: boolean;
  bTrimInputValues: boolean;
  bAlwaysRequired: boolean;
  bAsOfDateEnabled: boolean;
  bNotPostThisField: boolean;
  iFieldWidth: integer;
  PackageField : IEvExchangePackageField;
  PackageGroup : IEvExchangePackageGroup;
  PackageSection: IEvExchangePackageSection;
  ErrorsList: IisStringList;
  MapRecord: IevImportMapRecord;
  MapRecords: IisListOfValues;
  KeyTables: IisStringList;
  KeyFields: IisStringList;
  KeyValues: IisStringList;
  Sections: IisStringList;
  iDelayedPostTableNumber: integer;
  sLookupTableName, sLookupFieldName: String;
  sLookupFieldSize: integer;
begin
  // loading package information
  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXPackageNameTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Package''s <' + EvoXPackageNameTag +'> tag is missed');
  Name := tmpNode.Text;

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXDisplayNameTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Package''s <'+ EvoXDisplayNameTag + '> tag is missed');
  DisplayName := tmpNode.Text;

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXVersionTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Package''s <' + EvoXVersionTag + '> tag is missed');
  Version := StrToInt(tmpNode.Text);

  tmpNode := FindChildNodebyName(FXMLDoc.documentElement, EvoXEngineNameTag);
  CheckCondition(Assigned(tmpNode), 'Error in XML document: Package''s <' + EvoXEngineNameTag + '> tag is missed');
  EngineName := tmpNode.Text;

  // loading groups information
  GroupsList := FXMLDoc.documentElement.SelectNodes(EvoXPackageGroupsPath);
  if GroupsList.length > 0 then
  begin
  for i := 0 to GroupsList.length - 1 do
    if GroupsList[i].NodeName = EvoXGroupTag then
    begin
      GroupNode := GroupsList[i];

      tmpNode := FindChildNodebyName(GroupNode, EvoXgroupNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXgroupNameTag + '> tag is missed');
      sPackageGroupName := tmpNode.Text;

      tmpNode := FindChildNodebyName(GroupNode, EvoXgroupDisplayNameTag);
      if Assigned(tmpNode) then
        sPackageGroupDisplayName := tmpNode.text
      else
        sPackageGroupDisplayName := '';

      PackageGroup := TEvExchangePackageGroup.Create(sPackageGroupName, sPackageGroupDisplayName);
      GetGroupsList.AddValue(sPackageGroupName, PackageGroup);
    end;
  end;

  // loading sections information
  SectionsList := FXMLDoc.documentElement.SelectNodes(EvoXPackageSectionsPath);
  if SectionsList.length > 0 then
  begin
  for i := 0 to SectionsList.length - 1 do
    if SectionsList[i].NodeName = EvoXSectionTag then
    begin
      SectionNode := SectionsList[i];

      tmpNode := FindChildNodebyName(SectionNode, EvoXsectionNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXsectionNameTag + '> tag is missed');
      sPackageSectionName := tmpNode.Text;

      tmpNode := FindChildNodebyName(SectionNode, EvoXsectionDisplayNameTag);
      if Assigned(tmpNode) then
        sPackageSectionDisplayName := tmpNode.text
      else
        sPackageSectionDisplayName := '';

      PackageSection := TEvExchangePackageSection.Create(sPackageSectionName, sPackageSectionDisplayName);
      GetSectionsList.AddValue(sPackageSectionName, PackageSection);
    end;
  end;

  // loading fields information
  FieldsList := FXMLDoc.documentElement.SelectNodes(EvoXPackageFieldsPath);
  CheckCondition(Assigned(FieldsList) and (FieldsList.length > 0), 'Error in XML document: no package fields definitions found');
  for i := 0 to FieldsList.length - 1 do
    if FieldsList[i].NodeName = EvoXFieldTag then
    begin
      FieldNode := FieldsList[i];

      tmpNode := FindChildNodebyName(FieldNode, EvoXExchangeFieldNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXExchangeFieldNameTag + '> tag is missed');
      sExchangeFieldName := tmpNode.Text;

      tmpNode := FindChildNodebyName(FieldNode, EvoXDisplayNameTag);
      if Assigned(tmpNode) then
        sFieldDisplayName := tmpNode.Text
      else
        sFieldDisplayName := sExchangeFieldName;

      tmpNode := FindChildNodebyName(FieldNode, EvoXEvTableNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXEvTableNameTag + '> tag is missed');
      sEvTableName := tmpNode.Text;

      tmpNode := FindChildNodebyName(FieldNode, EvoXDefaultMapRecordTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXDefaultMapRecordTag + '> tag is missed');
      sDefaultMapRecord := tmpNode.Text;

      tmpNode := FindChildNodebyName(FieldNode, EvoXPackageMapRecordTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXPackageMapRecordTag + '> tag is missed');
      sPackageMapRecord := tmpNode.Text;

      tmpNode := FindChildNodebyName(FieldNode, EvoXEvFieldNameTag);
      CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXEvFieldNameTag + '> tag is missed');
      sEvFieldName := tmpNode.Text;

      tmpNode := FindChildNodebyName(FieldNode, EvoXfieldIsKeyTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bIsKeyField := true
      else
        bIsKeyField := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXignoreRequiredByDDTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bIgnoreRequiredByDD := true
      else
        bIgnoreRequiredByDD := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXcheckRequiredByDDTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bCheckRequiredByDD := true
      else
        bCheckRequiredByDD := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXrequiredIfAnyMappedForTable);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bRequiredIfAnyMapped := true
      else
        bRequiredIfAnyMapped := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXrequiredIfAnyMappedForOtherTable);
      if Assigned(tmpNode) then
        bRequiredIfAnyOtherMapped := tmpNode.Text
      else
        bRequiredIfAnyOtherMapped := '';

      tmpNode := FindChildNodebyName(FieldNode, EvoXtrimInputSpacesTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bTrimInputValues := true
      else
        bTrimInputValues := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXfieldAlwaysRequiredTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bAlwaysRequired := true
      else
        bAlwaysRequired := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXFuseEvoNamesForMapTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'False') then
        bUserEvoNamesForMap := false
      else
        bUserEvoNamesForMap := true;

      iFieldWidth := 0;
      tmpNode := FindChildNodebyName(FieldNode, EvoXfieldWidthTag);
      if Assigned(tmpNode) then
        if Trim(tmpNode.Text) <> '' then
          iFieldWidth := StrToInt(tmpNode.Text);

      tmpNode := FindChildNodebyName(FieldNode, EvoXrelatedParentFieldTag);
      if Assigned(tmpNode) then
        sRelatedParentField := tmpNode.Text
      else
        sRelatedParentField := '';

      tmpNode := FindChildNodebyName(FieldNode, EvoXfieldDescriptionTag);
      if Assigned(tmpNode) then
        sDescription := tmpNode.Text
      else
        sDescription := '';

      tmpNode := FindChildNodebyName(FieldNode, EvoXpreFormatFunctionName);
      if Assigned(tmpNode) then
        sPreFormatFunctionName := tmpNode.Text
      else
        sPreFormatFunctionName := '';

      tmpNode := FindChildNodebyName(FieldNode, EvoXCalcRequiredTag);
      if Assigned(tmpNode) then
        bCalcRequired := UpperCase(tmpNode.Text) = 'TRUE'
      else
        bCalcRequired := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXCalcDefaultTag);
      if Assigned(tmpNode) then
        bCalcDefault := UpperCase(tmpNode.Text) = 'TRUE'
      else
        bCalcDefault := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXgroupNameTag);
      if Assigned(tmpNode) then
        sGroupName := Trim(tmpNode.Text)
      else
        sGroupName := '';

      tmpNode := FindChildNodebyName(FieldNode, EvoXasOfDateEnabledTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bAsOfDateEnabled := true
      else
        bAsOfDateEnabled := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXNotPostThisFieldFlagTag);
      if Assigned(tmpNode) and AnsiSameText(tmpNode.Text, 'True') then
        bNotPostThisField := true
      else
        bNotPostThisField := false;

      tmpNode := FindChildNodebyName(FieldNode, EvoXdelayedPostTableNumber);
      if Assigned(tmpNode) then
        iDelayedPostTableNumber := StrToint(Trim(tmpNode.Text))
      else
        iDelayedPostTableNumber := -1;

      // lookup data
      sLookupTableName := '';
      sLookupFieldName := '';
      sLookupFieldSize := 0;
      LookupDataNode := FindChildNodebyName(FieldNode, EvoXLookupDataTag);
      if Assigned(LookupDataNode) then
      begin
        tmpNode := FindChildNodebyName(LookupDataNode, EvoXLookupTableTag);
        if Assigned(tmpNode) then
          sLookupTableName := UpperCase(Trim(tmpNode.Text));
        tmpNode := FindChildNodebyName(LookupDataNode, EvoXLookupFieldTag);
        if Assigned(tmpNode) then
          sLookupFieldName := UpperCase(Trim(tmpNode.Text));
        tmpNode := FindChildNodebyName(LookupDataNode, EvoXfieldWidthTag);
        if Assigned(tmpNode) then
          sLookupFieldSize := StrToInt(Trim(tmpNode.Text));
      end;

      // loading sections the field included in
      Sections := TisStringList.Create;
      SectionsList := FieldNode.SelectNodes(EvoXSectionRecordsPath);
      for j := 0 to SectionsList.length - 1 do
      begin
        SectionNode := SectionsList[j];
        if Assigned(SectionNode) then
          Sections.Add(SectionNode.text);
      end;

      // loading key records information
      KeyTables := TisStringList.Create;
      KeyFields := TisStringList.Create;
      KeyValues := TisStringList.Create;
      KeysList := FieldNode.SelectNodes(EvoXKeyRecordsPath);
      for j := 0 to KeysList.length - 1 do
      begin
        KeyNode := KeysList[j];
        if Assigned(KeyNode) then
        begin
          tmpNode := FindChildNodebyName(KeyNode, EvoXKeyTableTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXKeyTableTag + '> tag is missed');
          KeyTables.Add(tmpNode.Text);

          tmpNode := FindChildNodebyName(KeyNode, EvoXKeyFieldTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXKeyFieldTag + '> tag is missed');
          KeyFields.Add(tmpNode.Text);

          tmpNode := FindChildNodebyName(KeyNode, EvoXKeyValueTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXKeyValueTag + '> tag is missed');
          KeyValues.Add(tmpNode.Text);
        end;
      end;

      // loading MapRecord information
      MapRecords := TisListOfValues.Create;
      MapRecordsList := FieldNode.SelectNodes(EvoXmapRecordsPath);
      for j := 0 to MapRecordsList.length - 1 do
      begin
        MapRecordNode := MapRecordsList[j];
        MapRecord := TevImportMapRecord.CreateEmpty;
        sMapRecordName := '';
        if Assigned(MapRecordNode) then
        begin
          //name
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapRecordNameTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapRecordNameTag + '> tag is missed');
          sMapRecordName := tmpNode.Text;
          CheckCondition(Trim(sMapRecordName) <> '', 'Error in XML document: Field''s <' + EvoXmapRecordNameTag + '> value is empty string');
          MapRecord.SetMapRecordName(sMapRecordName);

          // description
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapRecordDescriptionTag);
          if Assigned(tmpNode) then
            MapRecord.Description := tmpNode.text
          else
            MapRecord.Description := sMapRecordName;

          // KeyLookupTable and KeyLookupField
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapKeyLookupTableTag);
          if Assigned(tmpNode) then
          begin
            MapRecord.KeyLookupTable := tmpNode.text;
            tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapKeyLookupFieldTag);
            CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapKeyLookupFieldTag + '> tag is missed');
            MapRecord.KeyLookupField := tmpNode.Text;
          end;

          // Key Field
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapKeyFieldTag);
          if Assigned(tmpNode) then
            MapRecord.KeyField := tmpNode.text;

          // visible to user
          MapRecord.SetVisibleToUser(true);
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapRecordVisibleToUserTag);
          if Assigned(tmpNode) then
            if tmpNode.Text = '0' then
              MapRecord.SetVisibleToUser(false);

          // type
          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapTypeTag);
          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' + EvoXmapTypeTag + '> tag is missed');
          if tmpNode.Text = 'mtpDirect' then
            MapRecord.MapType := mtpDirect
          else if tmpNode.Text = 'mtpLookup' then
            MapRecord.MapType := mtpLookup
          else if tmpNode.Text = 'mtpCustom' then
            MapRecord.MapType := mtpCustom
          else
            Assert(false, 'Error in XML document: Field''s <' + EvoXmapTypeTag + '> tag has wrong value: ' + tmpNode.Text);

          // other staff
          case Maprecord.MapType of
          mtpLookup:    begin
                          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapLookupTableTag);
                          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                            EvoXmapLookupTableTag + '> tag is missed');
                          MapRecord.LookupTable := tmpNode.text;

                          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapLookupFieldTag);
                          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                            EvoXmapLookupFieldTag + '> tag is missed');
                          MapRecord.LookupField := tmpNode.text;

                          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapLookupTable1Tag);
                          if Assigned(tmpNode) then
                            MapRecord.LookupTable1 := tmpNode.text;

                          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapLookupField1Tag);
                          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                            EvoXmapLookupFieldTag + '> tag is missed');
                          MapRecord.LookupField1 := tmpNode.text;
                        end;
          mtpCustom:    begin
                          tmpNode := FindChildNodebyName(MapRecordNode, EvoXmapFuncNameTag);
                          CheckCondition(Assigned(tmpNode), 'Error in XML document: Field''s <' +
                            EvoXmapFuncNameTag + '> tag is missed');
                          MapRecord.FuncName := tmpNode.Text;
                        end;
          end;  // case
          MapRecords.AddValue(sMapRecordName, MapRecord);
        end;  // if
      end;  // j

      PackageField := TEvExchangePackageField.Create(sExchangeFieldName, sEvTableName, sEvFieldName);
      PackageField.SetDisplayName(sFieldDisplayName);
      PackageField.SetRelatedparentField(sRelatedParentField);
      PackageField.SetCheckRuleForRequiremet(bCalcRequired);
      PackageField.SetCalcDefault(bCalcDefault);
      PackageField.SetSectionRecords(Sections as IisStringListRO);
      PackageField.SetMapRecords(MapRecords);
      PackageField.SetKeys(KeyTables as IisStringListRO, KeyFields as IisStringListRO, KeyValues as IisStringListRO);
      PackageField.SetDefaultMapRecordName(sDefaultMapRecord);
      PackageField.SetDescription(sDescription);
      PackageField.SetFieldSize(iFieldWidth);
      PackageField.SetPackageMapRecordName(sPackageMapRecord);
      PackageField.SetAsKeyField(bIsKeyField);
      PackageField.SetIgnoreRequiredByDD(bIgnoreRequiredByDD);
      PackageField.SetCheckRequiredByDD(bCheckRequiredByDD);
      PackageField.SetUseEvoNamesForMap(bUserEvoNamesForMap);
      PackageField.SetTrimInputSpaces(bTrimInputValues);
      PackageField.SetAlwaysRequired(bAlwaysRequired);
      PackageField.SetGroupName(sGroupName);
      PackageField.SetPreFormatFunctionName(sPreFormatFunctionName);
      PackageField.SetAsOfDateEnabled(bAsOfDateEnabled);
      PackageField.SetRequiredIfAnyMappedForTable(bRequiredIfAnyMapped);
      PackageField.SetRequiredIfAnyMappedForOtherTable(bRequiredIfAnyOtherMapped);
      PackageField.SetDelayedPostTableNumber(iDelayedPostTableNumber);
      PackageField.SetNotPostThisFieldFlag(bNotPostThisField);
      if sLookupTableName <> '' then
        PackageField.SetLookupData(sLookupTableName, sLookupFieldName, sLookupFieldSize);

      PackageField.DoReconcile;

      CheckCondition(not GetFieldsList.ValueExists(PackageField.GetExchangeFieldName),
        '"' + PackageField.GetExchangeFieldName + '" package field already exists in the package');
      GetFieldsList.AddValue(PackageField.GetExchangeFieldName, PackageField);
    end;

  ErrorsList := DoLogicalCheck;
  CheckCondition(ErrorsList.Count = 0, 'Package definition has logical problems: ' + ErrorsList.Text);
end;

end.
