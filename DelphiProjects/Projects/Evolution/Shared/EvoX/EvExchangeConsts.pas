unit EvExchangeConsts;

interface

const
  EvoXXMLHeader = '<?xml version="1.0"?>';

  // Package Name
  EvoXEBasicEEPackage = 'EEPackage';
  EvoXYTDPrPackage ='YTDPRPackage';
  EvoXHrPackage = 'HRPackage';
  EvoGenericPrPackage ='GenericPrPackage';
  EvoMultiBatchPrPackage ='MultiBatchPrPackage';
  EvoXScheduledEDPackage = 'ScheduledEdPackage';

  //Client Package
   EvoXClient             = 'Client';
   EvoXBasicClientPackage = 'BasicClientPackage';
   NewCustomClientNbrPackageField = 'Client Code';
   NewClientNamePackageField = 'Client Name';
   EvoClientPackageEngine = 'ClientPackageEngine';
   ClientNamePackField = 'Client Name';
   CLFiller_411PackField = 'Use State Reciprocate Laws on W2';
   CLDeliveryStuffPackField = 'Delivery Group Stuff';
   CLBillingNamePackField = 'Billing Name';
   CLDeliveryMethodDestinationNamePackField = 'Delivery Method Destination Name';
   CLAutomaticSavePackField ='Automatic Save';
   CLReadOnlyPackField ='Read Only Settings';

  // filed names from package
  CustomCompanyNbrPackageField = 'Custom Company Nbr';
  CustomEENbrPackageField = 'EE Code';
  StateMaritalStatusPackageField = 'State Marital Status';
  RateNumberPackageField = 'Rate Number';
  CompanyHomeStatePackageField = 'Company Home State';
  EEHomeState = 'Home State';
  EvoXEEDivision = 'Division';
  EvoXEEBranch = 'Branch';
  EvoXEEDepartment = 'Department';
  EvoXEETeam = 'Team';
  EvoXEEDDAbaNumber = 'ABA Number';
  EvoXEEDDBankAccountName = 'Bank Account Name';
  EvoXEEDDAccountType = 'Account Type';
  EvoXGarnishmentIDPackageField = 'Garnishment ID';
  EvoXPriorityField = 'Priority';
  EvoXBenefitReferenceForEE = 'Employee Benefit Reference';
  EvoXBenefitReferenceForDep = 'Dependent Benefit Reference';
  EvoXBenefitDepFirstName = 'Benefitiary Dependent First Name';
  EvoXBenefitDepLastName = 'Benefitiary Dependent Last Name';
  EvoXSchedEDBenefitReference = 'Benefit Reference';
  EvoXSchedEDBenefitAmountType = 'Benefit Amount Type';
  EvoXYTDTaxSUIState = 'Tax SUI State';
  EvoXYTDDivision = 'Division';
  EvoXYTDBranch = 'Branch';
  EvoXYTDDepartment = 'Department';
  EvoXYTDTeam = 'Team';
  EvoXYTDCheckDate = 'Check Date';
  EvoPrRunNumber = 'Run Nbr';
  EvoXPayGradePositiion = 'Pay Grade Position';
  EvoXEEBenefitReference = 'EE Benefit Amount Type';
  EvoXDepBenefAvailableDate = 'Eligible for Benefits with a date';
  EvoXEELocal = 'Local';
  EvoXTOACurrentAccrued = 'EE TOA - Current Accrued';
  EvoXTOACurrentUsed = 'EE TOA - Current Used';
  EvoXEEAcaBenefit = 'ACA Benefit';
  EvoXEEAcaLowestCostBenefit = 'ACA Lowest Cost Benefit';
  EvoXEeBenefitRef = 'EE Benefit Reference';
  EvoXEeBenefitAmountType = 'EE Benefit Amount Type';
  EvoXEeBenefitStartDate = 'EE Benefit Effective Start Date';
  EvoXAssignDependentLastName = 'Assign Dependent Last Name';
  EvoXAssignDependentFirstName = 'Assign Dependent First Name';
  EvoXAssignDependentDOB = 'Assign Dependent Birth Date';

  // group names from package
  EvoXHRDependentsBenefits = 'Dependents Benefits';

  // common
  EvoXFieldsTag = 'fields';
  EvoXFieldTag = 'field';
  EvoXDataSourceTag = 'dataSource';
  EvoXDataSourceNameTag = 'dataSourceName';
  EvoXOptionsTag = 'options';
  EvoXOptionTag = 'option';
  EvoXMaxUnknownFieldSize = 100;
  UseSelfValue = '#SELF#';
  UseNullValue = '#NULL#';
  EvoXGroupsTag = 'groups';
  EvoXGroupTag = 'group';
  EvoXLookupDataTag = 'lookupData';
  EvoXSectionsTag = 'sections';
  EvoXSectionTag = 'section';
  EvoXCDataStart = '<![CDATA[';
  EvoXCDataEnd = ']]>';

  // consts for Package and PackageFields
  EvoXPackageNameTag = 'name';
  EvoXDisplayNameTag = 'displayName';
  EvoXVersionTag = 'version';
  EvoXExchangeFieldNameTag = 'exchangeFieldName';
  EvoXEvTableNameTag = 'evTableName';
  EvoXEvFieldNameTag = 'evFieldName';
  EvoXCalcRequiredTag = 'calcRequired';
  EvoXEvExchangePackageTag = 'evExchangePackage';
  EvoXEngineNameTag = 'engineName';
  EvoXRelatedParentFieldTag = 'relatedParentField';
  EvoXCalcDefaultTag = 'calcDefault';
  EvoXPackageFieldsPath = '/' + EvoXEvExchangePackageTag + '/' + EvoXFieldsTag + '/' + EvoXFieldTag;
  EvoXPackageGroupsPath = '/' + EvoXEvExchangePackageTag + '/' + EvoXGroupsTag + '/' + EvoXGroupTag;
  EvoXPackageSectionsPath = '/' + EvoXEvExchangePackageTag + '/' + EvoXSectionsTag + '/' + EvoXSectionTag;
  EvoXKeyRecordsTag = 'keyRecords';
  EvoXKeyRecordTag = 'keyRecord';
  EvoXKeyTableTag = 'keyTable';
  EvoXKeyFieldTag = 'keyField';
  EvoXKeyValueTag = 'keyValue';
  EvoXKeyRecordsPath = EvoXKeyRecordsTag + '/' + EvoXKeyRecordTag;
  EvoXSectionRecordsPath = EvoXSectionsTag + '/' + EvoXSectionTag;
  EvoXDefaultMapRecordTag = 'defaultMapRecord';
  EvoXfieldDescriptionTag = 'fieldDescription';
  EvoXfieldWidthTag = 'fieldWidth';
  EvoXPackageMapRecordTag = 'packageMapRecord';
  EvoXmapRecordVisibleToUserTag = 'mapRecordVisibleToUser';
  EvoXfieldIsKeyTag = 'fieldIsKey';
  EvoXmapKeyLookupTableTag = 'mapKeyLookupTable';
  EvoXmapKeyLookupFieldTag = 'mapKeyLookupField';
  EvoXmapKeyFieldTag = 'mapKeyField';
  EvoXignoreRequiredByDDTag = 'ignoreRequiredByDD';
  EvoXcheckRequiredByDDTag = 'checkRequiredByDD';
  EvoXFuseEvoNamesForMapTag = 'useEvoNamesForMap';
  EvoXtrimInputSpacesTag = 'trimInputSpaces';
  EvoXfieldAlwaysRequiredTag = 'fieldAlwaysRequired';
  EvoXgroupNameTag = 'groupName';
  EvoXgroupDisplayNameTag = 'groupDisplayName';
  EvoXgroupRecordNumberTag = 'groupRecordNumber';
  EvoXpreFormatFunctionName = 'preFormatFunctionName';
  EvoXasOfDateEnabledTag = 'asOfDateEnabled';
  EvoXasOfDateSourceTag = 'asOfDateSource';
  EvoXasOfDateValueTag = 'asOfDateValue';
  EvoXasOfDateFieldTag = 'asOfDateField';
  EvoXrequiredIfAnyMappedForTable = 'requiredIfAnyMappedForTable';
  EvoXrequiredIfAnyMappedForOtherTable = 'requiredIfAnyMappedForOtherTable';
  EvoXdelayedPostTableNumber = 'delayedPostTableNumber';
  EvoXNotPostThisFieldFlagTag = 'notPostThisFieldFlag';
  EvoXLookupTableTag = 'lookupTableName';
  EvoXLookupFieldTag = 'lookupFieldName';
  EvoXsectionNameTag = 'sectionName';
  EvoXsectionDisplayNameTag = 'sectionDisplayName';

  // consts for MapFile and MapField
  EvoXevExchangeMapFileTag = 'evExchangeMapFile';
  EvoXmapInputFieldNameTag = 'mapInputFieldName';
  EvoXmapFieldTypeTag = 'mapFieldType';
  EvoXmapFieldSizeTag = 'mapFieldSize';
  EvoXmapFieldDateFormatTag = 'mapFieldDateFormat';
  EvoXmapFieldTimeFormatTag = 'mapFieldTimeFormat';
  EvoXmapDescriptionTag = 'mapDescription';
  EvoXmapPackageNameTag = 'packageName';
  EvoXmapPackageVersionTag = 'packageVersion';
  EvoXmapExampleFileNameTag = 'exampleFileName';
  EvoXmapExampleDataTag = 'exampleData';
  EvoXmapDataSourceTag = 'dataSource';
  EvoXmapdataSourceNameTag = 'dataSourceName';
  EvoXmapDataSourceOptionsPath = EvoXOptionsTag + '/' + EvoXOptionTag;
  EvoXmapFieldsPath = '/' + EvoXevExchangeMapFileTag + '/' + EvoXFieldsTag + '/' + EvoXFieldTag;
  EvoXMapFileOptionsPath = '/' + EvoXevExchangeMapFileTag + '/' + EvoXOptionsTag + '/' + EvoXOptionTag;
  EvoXmapRecordTag = 'mapRecord';
  EvoXmapTypeTag = 'mapType';
  EvoXmapFuncNameTag = 'mapFuncName';
  EvoXmapValuesTag = 'mapValues';
  EvoXmapDefaultTag = 'mapDefault';
  EvoXmapLookupTableTag = 'mapLookupTable';
  EvoXmapLookupFieldTag = 'mapLookupField';
  EvoXmapLookupTable1Tag = 'mapLookupTable1';
  EvoXmapLookupField1Tag = 'mapLookupField1';
  EvoXmapRecordsTag = 'mapRecords';
  EvoXmapRecordNameTag = 'mapRecordName';
  EvoXmapRecordDescriptionTag = 'mapRecordDescription';
  EvoXmapRecordsPath = EvoXmapRecordsTag + '/' + EvoXmapRecordTag;
  EvoXuserMapTypeTag = 'userMapType';
  EvoVasOfDateFieldDateFormat = 'asOfDateFieldDateFormat';
  EvoXsectionRecordsTag = 'sectionRecords';
  EvoXsectionRecordTag = 'sectionRecord';

  // datatype names
  EvoXddtUnknown = 'ddtUnknown';
  EvoXddtInteger = 'ddtInteger';
  EvoXddtFloat = 'ddtFloat';
  EvoXddtCurrency = 'ddtCurrency';
  EvoXddtDateTime = 'ddtDateTime';
  EvoXddtString = 'ddtString';
  EvoXddtBoolean = 'ddtBoolean';
  EvoXddtBLOB = 'ddtBLOB';

  //Engine names
  EvoXEEPackageEngine = 'eePackageEngine';
  EvoXYTDPrPackageEngine = 'YTDPRPackageEngine';
  EvoGenericPrPackageEngine = 'GenericPrPackageEngine';
  EvoMultiBatchPrPackageEngine = 'MultiBatchPrPackageEngine';

  // Engine results
  EvoXInputRowsAmount = 'inputRowsAmount';
  EvoXImportedRowsAmount = 'importedRowsAmount';
  EvoXAllMessagesAmount = 'errorsAmount';
  EvoXRealErrorAmount = 'realErrorsAmount';
  EvoXStringErrorsList = 'errorsList';
  EvoXExtErrorsList = 'errorsListExtended';
  EovXRowWithErrorNbrs = 'rowWithErrorNbrs';

  //CSV reader
  sCSVReaderName = 'CSV Reader';
  fieldNamesInFirstRow = 'fieldNamesInFirstRow';
  fieldDelimiter = 'fieldDelimiter';
  DoubleQuoteChar = 'doubleQuoteChar';
  DoubleQuoteRequired = 'doubleQuoteRequired';

  // ASCII Fixed reader
  sASCIIFixedReaderName = 'ASCIIFixed Reader';
  fieldsList = 'fieldsList';

  // task parameters names
  EvoXTaskFolderPath = 'FolderPath';
  EvoXTaskMapFilePath = 'MapFilePath';
  EvoXTaskTimeInterval = 'TimeInterval'; // in minutes
  EvoXTaskFilePattern = 'FilePattern';
  EvoXTaskName = 'TaskName';
  EvoXTaskEmailAddress = 'EMailAddress';
  EvoXTaskEMailNotification = 'EMailNotification';
  EvoXTaskStopOnError = 'StopOnError';
  EvoXTaskStatusInactive = 'Inactive';
  EvoXTaskStatusActive = 'Active';
  EvoXTaskStatusSuspended = 'Suspended';
  EvoXTaskStatusIncorrect = 'Incorrect';
  EvoXTaskStatusCheckInterval = 'TaskStatusCheckInterval';
  EvoXDefaultTaskStatusCheckInterval = 5;

  // import function's options
  EvoXHowToHandleNullsOption = 'HandleNulls';
  EvoXSpecialNullStringOption = 'NullString';
  EvoXIgnoreNulls = 'I';  // default
  EvoXReplaceValueByNull = 'R';
  EvoXUseSpecialNullString = 'S';
  EvoXAutoCheckParam = 'AutoCheckParam';
  EvoXPaySalaryParam = 'PaySalaryParam';
  EvoXPayStandHoursParam = 'PayStandHoursParam';
  EvoXLoadPayrollDefaultsParam = 'LoadPayrollDefaultsParam';
  EvoXIncludeTOAParam = 'IncludeTOA';

  // generic strings
  EvoXLookupFailedMessage = ' If no other errors occur, the Import will occur with a null value. You should update the record accordingly.';
  EvoXDBDTLookupFailedMessage = ' It is not correct due DBDT Level setup for this company!';

  // other
  EvoXVirtualTablePrefix = 'EvoXVirtualTable_#';
  EvoXPrRunNbrDefaultValue = 1;


type
  TEvoxTaskEmailNotification = (tskNever, tskAlways, tskSuccess, tskException);
  TEvoXTaskEventType = tskSuccess..tskException;
  TEvExchangeHandleNulls = (tIgnore, tReplace, tSpecial);

implementation

end.

