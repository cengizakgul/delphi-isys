unit EvExchangeXMLUtils;

interface

uses Classes, Windows, SysUtils, Variants, MSXML2_TLB;

  function  FindChildNodebyName(AParentNode : IXMLDOMNode; AName : String) : IXMLDOMNode;
  function SaveChildToNode(ANode : IXMLDOMNode; AName, AVAlue : String; AAutoAdd : boolean = true) : IXMLDOMNode;

implementation

function SaveChildToNode(ANode : IXMLDOMNode; AName, AVAlue : String; AAutoAdd : boolean = true) : IXMLDOMNode;
var
  N : IXMLDOMNode;
begin
  N := FindChildNodebyName(ANode, AName);
  if Assigned(N) then
  begin
    result := N;
    N.Text := AValue;
    exit;
  end
  else
    if AAutoAdd then
    begin
      N := ANode.ownerDocument.createNode(NODE_ELEMENT, AName, '');
      N.text := AValue;
      result := N;
      ANode.appendChild(N);
    end
    else
      raise Exception.Create('Child node not found : ' + AName + '. Xml: ' + ANode.xml);
end;

function FindChildNodebyName(AParentNode: IXMLDOMNode; AName: String): IXMLDOMNode;
var
  i : integer;
  NList : IXMLDOMNodeList;
begin
  result := nil;
  NList := AParentNode.childNodes;
  if Assigned(NList) then
    for i := 0 to NList.length - 1 do
      if NList[i].nodeName = AName then
      begin
        result := NList[i];
        exit;
      end;
end;

end.
