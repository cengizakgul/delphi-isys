unit EvExchangeResultLog;

interface

uses isBaseClasses, isLogFile, EvStreamUtils, ISBasicUtils, EvCommonInterfaces;

type
  TEvExchangeResultLogEvent = class(TisInterfacedObject, IEvExchangeResultLogEvent)
  private
     FEventType: TLogEventType;
     FText: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function GetEventType: TLogEventType;
    function GetText: String;
  public
    class function GetTypeID: String; override;
    constructor Create(const AEventType: TLogEventType; const AText: String); reintroduce;
  end;

  TEvExchangeResultLog = class(TisInterfacedObject, IEvExchangeResultLog)
  private
    FEventsList: IisList;
    FErrorsCount: integer;
    FWarningsCount: integer;
    FInformationCount: integer;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetAmountOfErrors: integer;
    function  GetAmountOfWarnings: integer;
    function  GetAmountOfInformation: integer;
    procedure LoadFromStringList(const AErrorsList: IisStringList);
    function  GetAsStringList: IisStringList;
    function  Text: String;
    function  AddEvent(const AEventType: TLogEventType; const AText: String): IEvExchangeResultLogEvent;
    function  Count: Integer;
    function  GetItem(Index: Integer): IEvExchangeResultLogEvent;
    procedure Clear;
  public
    class function GetTypeID: String; override;
  end;

  function GetPrefixByType(const AEventType: TLogEventType): String;

implementation

  function GetPrefixByType(const AEventType: TLogEventType): String;
  begin
    if (AEventType = etError) or (AEventType = etFatalError) then
      Result := 'Error:   '
    else if (AEventType = etWarning) then
      Result := 'Warning: '
    else if (AEventType = etInformation) then
      Result := 'Info:    '
    else
      CheckCondition(false, 'Unknown event type');
  end;

{ TEvExchangeResultLogEvent }

constructor TEvExchangeResultLogEvent.Create(const AEventType: TLogEventType; const AText: String);
begin
  CheckCondition(AEventType <> etUnknown, 'Unknown event type is not allowed');
  FEventType := AEventType;
  FText := AText;
end;

function TEvExchangeResultLogEvent.GetEventType: TLogEventType;
begin
  Result := FEventType;
end;

function TEvExchangeResultLogEvent.GetText: String;
begin
  Result := FText;
end;

class function TEvExchangeResultLogEvent.GetTypeID: String;
begin
  Result := 'EvExchangeResultLogEvent';
end;

procedure TEvExchangeResultLogEvent.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEventType := TLogEventType(AStream.ReadInteger);
  FText := AStream.ReadString;
end;

procedure TEvExchangeResultLogEvent.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(Integer(FEventType));
  AStream.WriteString(FText);
end;

{ TEvExchangeResultLog }

function TEvExchangeResultLog.AddEvent(const AEventType: TLogEventType; const AText: String): IEvExchangeResultLogEvent;
begin
  Result := TEvExchangeResultLogEvent.Create(AEventType, AText);
  FEventsList.Add(Result);
  if (AEventType = etError) or (AEventType = etFatalError) then
    Inc(FErrorsCount);
  if AEventType = etWarning then
    Inc(FWarningsCount);
  if AEventType = etInformation then
    Inc(FInformationCount);
end;

procedure TEvExchangeResultLog.Clear;
begin
  FEventsList.Clear;
end;

function TEvExchangeResultLog.Count: Integer;
begin
  Result := FEventsList.Count;
end;

procedure TEvExchangeResultLog.DoOnConstruction;
begin
  inherited;
  FEventsList := TisList.Create;
end;

function TEvExchangeResultLog.GetAmountOfErrors: integer;
begin
  Result := FErrorsCount;
end;

function TEvExchangeResultLog.GetAmountOfinformation: integer;
begin
  Result := FInformationCount;
end;

function TEvExchangeResultLog.GetAmountOfWarnings: integer;
begin
  Result := FWarningsCount;
end;

function TEvExchangeResultLog.GetAsStringList: IisStringList;
var
  i: integer;
  Event: IEvExchangeResultLogEvent;
begin
  Result := TisStringList.Create;
  for i := 0 to FEventsList.Count - 1 do
  begin
    Event := FEventsList.Items[i] as IEvExchangeResultLogEvent;
    Result.Add(GetPrefixByType(Event.GetEventType) + Event.GetText)
  end;
end;

function TEvExchangeResultLog.GetItem(Index: Integer): IEvExchangeResultLogEvent;
begin
  Result := FEventsList.Items[Index] as IEvExchangeResultLogEvent;
end;

class function TEvExchangeResultLog.GetTypeID: String;
begin
  Result := 'EvExchangeResultLog';
end;

procedure TEvExchangeResultLog.LoadFromStringList(const AErrorsList: IisStringList);
var
  i: integer;
  Event: IEvExchangeResultLogEvent;
begin
  FEventsList.Clear;
  for i := 0 to AErrorsList.Count - 1 do
  begin
    Event := TEvExchangeResultLogEvent.Create(etError, AErrorsList[i]);
    FEventsList.Add(Event);
  end;
end;

procedure TEvExchangeResultLog.ReadSelfFromStream( const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEventsList.LoadItemsFromStream(AStream);
  FErrorsCount := AStream.ReadInteger;
  FWarningsCount := AStream.ReadInteger;
  FInformationCount := AStream.ReadInteger;
end;

function TEvExchangeResultLog.Text: String;
var
  i: integer;
  Event: IEvExchangeResultLogEvent;
begin
  Result := '';
  for i := 0 to FEventsList.Count - 1 do
  begin
    Event := FEventsList.Items[i] as IEvExchangeResultLogEvent;
    Result := Result + GetPrefixByType(Event.GetEventType) + Event.GetText + #13#10
  end;
end;

procedure TEvExchangeResultLog.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  FEventsList.SaveItemsToStream(AStream);
  AStream.WriteInteger(FErrorsCount);
  AStream.WriteInteger(FWarningsCount);
  AStream.WriteInteger(FInformationCount);
end;

initialization
  ObjectFactory.Register([TEvExchangeResultLogEvent, TEvExchangeResultLog]);

finalization
  SafeObjectFactoryUnRegister([TEvExchangeResultLogEvent, TEvExchangeResultLog]);

end.
