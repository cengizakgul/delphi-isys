unit EvExchangeMapFile;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils, isBasicUtils, DB,
     EvConsts, EvStreamUtils, EvTypes, rwCustomDataDictionary, SDDStreamClasses,
     rwEngineTypes, EvExchangeConsts, EvExchangeUtils, DIMime, DateUtils;

type
  TevEchangeMapField = class(TisInterfacedObject, IevEchangeMapField)
  private
    FInputFieldName: String;
    FFieldType: TDataDictDataType;
    FDateFormat: String;
    FTimeFormat: String;
    FFieldSize: integer;
    FExchangeFieldName: String;
    FMapRecord: IisListOfValues;
    FRecordNumber: integer;
    FAsOfDateEnabled: boolean;
    FAsOfDateSource: TEvoXAsOfDateSourceType;
    FAsOfDateValue: TDateTime;
    FAsOfDateField: String;
    FAsOfDateFieldDateFormat: String;
  protected
    procedure DoOnConstruction; override;
    procedure InitAsOfDate;

    // IevEchangeMapField implementation
    function  GetInputFieldName: String;
    function  GetFieldType: TDataDictDataType;
    function  GetDBFieldType: TFieldType;
    function  GetFieldSize: integer;
    function  GetDateFormat: String;
    function  GetTimeFormat: String;
    function  GetExchangeFieldName: String;
    function  GetMapRecord: IEvImportMapRecord;
    function  GetGroupRecordNumber: integer;
    function  GetAsOfDateEnabled: boolean;
    function  GetAsOfDateSource: TEvoXAsOfDateSourceType;
    function  GetAsOfDateValue: TDateTime;
    function  GetAsOfDateField: String;
    function  GetAsOfDateFieldDateFormat: String;
    function  GetXML: String;

    procedure SetInputFieldName(const AFieldName: String);
    procedure SetFieldType(const AFieldType: TDataDictDataType);
    procedure SetFieldSize(const AFieldSize: integer);
    procedure SetDateFormat(const ADateFormat: String);
    procedure SetTimeFormat(const ATimeFormat: String);
    procedure SetExchangeFieldName(const AFieldName: String);
    procedure SetMapRecord(const AMapRecord: IEvImportMapRecord);
    procedure SetGroupRecordNumber(const Value: integer);
    procedure SetAsOfDateEnabled(const Value: boolean);
    procedure SetAsOfDateSource(const Value: TEvoXAsOfDateSourceType);
    procedure SetAsOfDateValue(const Value: TDateTime);
    procedure SetAsOfDateField(const Value: String);
    procedure SetAsOfDateFieldDateFormat(const Value: String);
    procedure DoReconcile;

    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    destructor Destroy; override;
    class function GetTypeID: String; override;
  end;

  TEvExchangeMapFile = class(TisInterfacedObject, IEvExchangeMapFile)
  private
    FDescription: String;
    FPackageName: String;
    FPackageVersion: integer;
    FExampleFileName: String;
    FDataSourceName: String;
    FDataSourceOptions: IisStringList;
    FFieldsList: IisListOfValues;
    FExampleData: IEvDualStream;
    FMapFileOptions: IisStringList;
  protected
    procedure DoOnConstruction; override;
    procedure Clear;
    procedure SetPackageName(APackageName: String);
    procedure SetPackageVersion(APackageVersion: integer);
    procedure InitOptions;

    // IEvExchangeMapFile implementation
    function  GetDescription: String;
    function  GetPackageName: String;
    function  GetPackageVersion: integer;
    function  GetExampleFileName: String;
    function  GetExampleData: IEvDualStream;
    function  GetDataSourceName: String;
    function  GetDataSourceOptions: IisStringList;
    function  GetFieldsList: IisListOfValues;
    function  GetFieldByExchangeName(const AExchangeFieldName: String): IevEchangeMapField;
    function  GetMapFileOptions: IisStringList;
    function  GetXML: String;
    procedure SetDescription(ADescription: String);
    procedure SetPackageInformation(APackageInformation: IEvExchangePackage);
    procedure SetExampleFileName(AExampleFileName: String);
    procedure SetExampleData(AExampleData: IEvDualStream);
    procedure SetDataSourceName(ADataSourceName: String);
    procedure SetDataSourceOptions(ADataSourceOptions: IisStringList);
    procedure SetMapFileOptions(AMapFileOptions: IisStringList);

    procedure AddField(const AField: IevEchangeMapField);
    procedure DeleteFieldByExchangeName(const AExchangeFieldName: String);
    procedure DoReconcile;

    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    destructor Destroy; override;
    class function GetTypeID: String; override;
  end;

implementation

{ TevEchangeMapField }

destructor TevEchangeMapField.Destroy;
begin

  inherited;
end;

procedure TevEchangeMapField.DoOnConstruction;
begin
  inherited;
  FDateFormat := 'mm/dd/yyyy';
  FTimeFormat := 'hh:nn:ss';
  InitAsOfDate;
  FMapRecord := TisListOfValues.Create;
end;

procedure TevEchangeMapField.DoReconcile;
begin
  if FMapRecord.Count > 0 then
    CheckCondition((IInterface(FMaprecord.Values[0].Value) as IEvImportMapRecord).MapType in [mtpDirect, mtpMap, mtpCustom],
      'Map record has wrong type. Input field name: ' + FInputFieldName);
  CheckCondition(FInputFieldName <> '', 'Input field name cannot be empty');
  CheckCondition(FExchangeFieldName <> '', 'Exchange field cannot be empty');
  if FRecordNumber <> 0 then
    CheckCondition(Pos(RecordNumberToStr(FRecordNumber), FExchangeFieldName) > 0, 'Record number should be part of field name. EvoExchangeField: ' + FExchangeFieldName);
end;

function TevEchangeMapField.GetDateFormat: String;
begin
  Result := FDateFormat;
end;

function TevEchangeMapField.GetDBFieldType: TFieldType;
begin
  case FFieldType of
    ddtUnknown: Result := ftUnknown;
    ddtInteger: Result := ftInteger;
    ddtFloat: Result := ftFloat;
    ddtCurrency: Result := ftCurrency;
    ddtDateTime: Result := ftDateTime;
    ddtString: Result := ftString;
    ddtBoolean: Result := ftBoolean;
  else
    raise Exception.Create('Unsupported data type: ' + IntToStr(Integer(FFieldType)));
  end;
end;

function TevEchangeMapField.GetExchangeFieldName: String;
begin
  Result := FExchangeFieldName;
end;

function TevEchangeMapField.GetInputFieldName: String;
begin
  Result := FInputFieldName;
end;

function TevEchangeMapField.GetFieldSize: integer;
begin
  if FFieldType = ddtString then
    Result := FFieldSize
  else
  begin
    Result := 0;
    FFieldSize := 0;
  end;
end;

function TevEchangeMapField.GetFieldType: TDataDictDataType;
begin
  Result := FFieldType;
end;

function TevEchangeMapField.GetMapRecord: IEvImportMapRecord;
begin
  Result := nil;
  if FMapRecord.Count > 0 then
    Result := IInterface(FMapRecord.Values[0].Value) as IEvImportMapRecord;
end;

function TevEchangeMapField.GetTimeFormat: String;
begin
  Result := FTimeFormat;
end;

class function TevEchangeMapField.GetTypeID: String;
begin
  Result := 'evEchangeMapField';
end;

function TevEchangeMapField.GetXML: String;
var
  sType: String;
begin
  Result := StartTag(EvoXFieldTag);
  Result := Result + InsertTag(EvoXmapInputFieldNameTag, AddCDataTag(FInputFieldName));
  Result := Result + InsertTag(EvoXgroupRecordNumberTag, IntToStr(FRecordNumber));

  case FFieldType of
    ddtUnknown: sType := EvoXddtUnknown;
    ddtInteger: sType := EvoXddtInteger;
    ddtFloat: sType := EvoXddtFloat;
    ddtCurrency: sType := EvoXddtCurrency;
    ddtDateTime: sType := EvoXddtDateTime;
    ddtString: sType := EvoXddtString;
    ddtBoolean: sType := EvoXddtBoolean;
    ddtBLOB: sType := EvoXddtBLOB;
  else
    raise Exception.Create('Unsupported data type: ' + IntToStr(Integer(FFieldType)));
  end;

  Result := Result + InsertTag(EvoXmapFieldTypeTag, sType);
  if FFieldType = ddtDateTime then
  begin
    Result := Result + InsertTag(EvoXmapFieldDateFormatTag, AddCDataTag(FDateFormat));
    Result := result + InsertTag(EvoXmapFieldTimeFormatTag, AddCDataTag(FTimeFormat));
  end;
  if FFieldType = ddtString then
    Result := Result + InsertTag(EvoXmapFieldSizeTag, IntToStr(FFieldSize));

  Result := Result + InsertTag(EvoXExchangeFieldNameTag, AddCDataTag(FExchangeFieldName));

  if FAsOfDateEnabled then
    Result := Result + InsertTag(EvoXasOfDateEnabledTag, 'true')
  else
    Result := Result + InsertTag(EvoXasOfDateEnabledTag, 'false');
  Result := Result + InsertTag(EvoXasOfDateSourceTag, IntToStr(Integer(FAsOfDateSource)));
  Result := Result + InsertTag(EvoXasOfDateValueTag, FloatToStr(FAsOfDateValue));
  Result := Result + InsertTag(EvoXasOfDateFieldTag, AddCDataTag(FAsOfDateField));
  Result := Result + InsertTag(EvoVasOfDateFieldDateFormat, AddCDataTag(FAsOfDateFieldDateFormat));

  // map records info
  if FMapRecord.Count > 0 then
    Result := Result + (IInterface(FMapRecord.Values[0].Value) as IEvImportMapRecord).GetXML;

  Result := Result + EndTag(EvoXFieldTag);
end;

procedure TevEchangeMapField.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FInputFieldName := AStream.ReadString;
  FFieldType := TDataDictDataType(AStream.ReadInteger);
  FFieldSize := AStream.ReadInteger;
  FDateFormat := AStream.ReadString;
  FTimeFormat := AStream.ReadString;
  FExchangeFieldName := AStream.ReadString;
  FMapRecord.ReadFromStream(AStream);
  FRecordNumber := AStream.ReadInteger;
  FAsOfDateEnabled := AStream.ReadBoolean;
  FAsOfDateSource := TEvoXAsOfDateSourceType(AStream.ReadInteger);
  FAsOfDateValue := AStream.ReadDouble;
  FAsOfDateField := AStream.ReadString;
  FAsOfDateFieldDateFormat := AStream.ReadString;
end;

procedure TevEchangeMapField.SetDateFormat(const ADateFormat: String);
begin
  FDateFormat := ADateFormat;
end;

procedure TevEchangeMapField.SetExchangeFieldName(const AFieldName: String);
begin
  FExchangeFieldName := AFieldName;
end;

procedure TevEchangeMapField.SetInputFieldName(const AFieldName: String);
begin
  CheckCondition(Trim(AFieldName) <> '', 'Input field name is empty');
  FInputFieldName := AFieldName;
end;

procedure TevEchangeMapField.SetFieldSize(const AFieldSize: integer);
begin
  FFieldSize := AFieldSize;
end;

procedure TevEchangeMapField.SetFieldType(const AFieldType: TDataDictDataType);
begin
  FFieldType := AFieldType;
end;

procedure TevEchangeMapField.SetMapRecord(const AMapRecord: IEvImportMapRecord);
begin
  FMapRecord.Clear;
  FMaprecord.AddValue('0', AMapRecord);
end;

procedure TevEchangeMapField.SetTimeFormat(const ATimeFormat: String);
begin
  FTimeFormat := ATimeFormat;
end;

procedure TevEchangeMapField.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FInputFieldName);
  AStream.WriteInteger(Integer(FFieldType));
  AStream.WriteInteger(FFieldSize);
  AStream.WriteString(FDateFormat);
  AStream.WriteString(FTimeFormat);
  AStream.WriteString(FExchangeFieldName);
  FMapRecord.WriteToStream(AStream);
  AStream.WriteInteger(FRecordNumber);
  AStream.WriteBoolean(FAsOfDateEnabled);
  AStream.WriteInteger(Integer(FAsOfDateSource));
  AStream.WriteDouble(FAsOfDateValue);
  AStream.WriteString(FAsOfDateField);
  AStream.WriteString(FAsOfDateFieldDateFormat);
end;

function TevEchangeMapField.GetGroupRecordNumber: integer;
begin
  Result := FRecordNumber;
end;

procedure TevEchangeMapField.SetGroupRecordNumber(const Value: integer);
begin
  FRecordNumber := Value;
end;

function TevEchangeMapField.GetAsOfDateEnabled: boolean;
begin
  Result := FAsOfDateEnabled;
end;

procedure TevEchangeMapField.SetAsOfDateEnabled(const Value: boolean);
begin
  FAsOfDateEnabled := Value;
  if not FAsOfDateEnabled then
    InitAsOfDate;
end;

function TevEchangeMapField.GetAsOfDateSource: TEvoXAsOfDateSourceType;
begin
  Result := FAsOfDateSource;
end;

procedure TevEchangeMapField.SetAsOfDateSource(const Value: TEvoXAsOfDateSourceType);
begin
  FAsOfDateSource := Value;
  if FAsOfDateSource = stDateEditor then
    FAsOfDateValue := Today;
end;

function TevEchangeMapField.GetAsOfDateField: String;
begin
  CheckCondition(FAsOfDateSource = stMappedField, 'You cannot get AsOfDate field''s value if SourceType <> mapped field');
  Result := FAsOfDateField;
end;

function TevEchangeMapField.GetAsOfDateValue: TDateTime;
begin
  CheckCondition(FAsOfDateSource = stDateEditor, 'You cannot get AsOfDate field''s value if SourceType <> date editor');
  Result := FAsOfDateValue;
end;

procedure TevEchangeMapField.SetAsOfDateField(const Value: String);
begin
  CheckCondition(FAsOfDateSource = stMappedField, 'You cannot set up AsOfDate field''s name if SourceType <> mapped field');
  FAsOfDateField := Value;
end;

procedure TevEchangeMapField.SetAsOfDateValue(const Value: TDateTime);
begin
  CheckCondition(FAsOfDateSource = stDateEditor, 'You cannot set up AsOfDate''s value if SourceType <> date editor');
  FAsOfDateValue := Value;
end;

function TevEchangeMapField.GetAsOfDateFieldDateFormat: String;
begin
  CheckCondition(FAsOfDateSource = stMappedField, 'You cannot get AsOfDate field''s date format if SourceType <> mapped field');
  Result := FAsOfDateFieldDateFormat;
end;

procedure TevEchangeMapField.SetAsOfDateFieldDateFormat(
  const Value: String);
begin
  CheckCondition(FAsOfDateSource = stMappedField, 'You cannot set up AsOfDate field''s date format if SourceType <> mapped field');
  FAsOfDateFieldDateFormat := Value;
end;

procedure TevEchangeMapField.InitAsOfDate;
begin
  FAsOfDateFieldDateFormat := 'mm/dd/yyyy';
  FAsOfDateSource := stDateEditor;
  FAsOfDateValue := Today;
  FAsOfDateField := '';
end;

{ TEvExchangeMapFile }

procedure TEvExchangeMapFile.AddField(const AField: IevEchangeMapField);
begin
  AField.DoReconcile;
  FFieldsList.AddValue(AField.GetExchangeFieldName, AField);
end;

procedure TEvExchangeMapFile.Clear;
begin
  FDescription := '';
  FPackageName := '';
  FPackageVersion := 0;
  FExampleFileName := '';
  FDataSourceName := '';
  FDataSourceOptions.Clear;
  FFieldsList.Clear;
  FExampleData.Clear;
  FMapFileOptions.Clear;
  InitOptions;
end;

procedure TEvExchangeMapFile.DeleteFieldByExchangeName(
  const AExchangeFieldName: String);
begin
  FFieldsList.DeleteValue(AExchangeFieldName);
end;

destructor TEvExchangeMapFile.Destroy;
begin
  inherited;
end;

procedure TEvExchangeMapFile.DoOnConstruction;
begin
  inherited;
  FDataSourceOptions := TisStringList.Create;
  FFieldsList := TisListOfValues.Create;
  FExampleData := TEvDualStreamHolder.Create;
  FDataSourceName := sCSVReaderName;
  FMapFileOptions := TisStringList.Create;
  InitOptions;
end;

procedure TEvExchangeMapFile.DoReconcile;
begin
  InitOptions;
end;

function TEvExchangeMapFile.GetDataSourceName: String;
begin
  Result := FDataSourceName;
end;

function TEvExchangeMapFile.GetDataSourceOptions: IisStringList;
begin
  Result := FDataSourceOptions; 
end;

function TEvExchangeMapFile.GetDescription: String;
begin
  Result := FDescription;
end;

function TEvExchangeMapFile.GetExampleData: IEvDualStream;
begin
  Result := FExampleData;
end;

function TEvExchangeMapFile.GetExampleFileName: String;
begin
  Result := FExampleFileName;
end;

function TEvExchangeMapFile.GetFieldByExchangeName(
  const AExchangeFieldName: String): IevEchangeMapField;
begin
  Result := nil;
  if FFieldsList.ValueExists(AExchangeFieldName) then
    Result := IInterface(FFieldsList.Value[AExchangeFieldName]) as IevEchangeMapField;
end;

function TEvExchangeMapFile.GetFieldsList: IisListOfValues;
begin
  Result := FFieldsList;
end;

function TEvExchangeMapFile.GetMapFileOptions: IisStringList;
begin
  Result := FMapFileOptions;
end;

function TEvExchangeMapFile.GetPackageName: String;
begin
  Result := FPackageName;
end;

function TEvExchangeMapFile.GetPackageVersion: integer;
begin
  Result := FPackageVersion;
end;

class function TEvExchangeMapFile.GetTypeID: String;
begin
  Result := 'TEvExchangeMapFile'
end;

function TEvExchangeMapFile.GetXML: String;
var
  i: integer;
  MapFieldValue: IevEchangeMapField;
  sExampleData, sEncodedExampleData: String;
begin
  Result := EvoXXMLHeader + #13#10 + StartTag(EvoXevExchangeMapFileTag) + #13#10;
  Result := Result + InsertTag(EvoXmapDescriptionTag, AddCDataTag(FDescription));
  Result := Result + InsertTag(EvoXmapPackageNameTag, FPackageName);
  Result := Result + InsertTag(EvoXmapPackageVersionTag, IntToStr(FPackageVersion));

  if FExampleFileName <> '' then
    Result := Result + InsertTag(EvoXmapExampleFileNameTag, AddCDataTag(FExampleFileName));

  if FExampleData.Size > 0 then
  begin
    SetLength(sExampleData, FExampleData.Size);
    FExampleData.Position := 0;
    FExampleData.ReadBuffer(sExampleData[1], FExampleData.Size);
    sEncodedExampleData := MimeEncodeString(sExampleData);
    Result := Result + InsertTag(EvoXmapExampleDataTag, sEncodedExampleData);
  end;

  //map file options
  if FMapFileOptions.Count > 0 then
  begin
    Result := Result + StartTag(EvoXOptionsTag) + #13#10;
    for i := 0 to FMapFileOptions.Count - 1 do
      Result := Result + InsertTag(EvoXOptionTag, FMapFileOptions[i]);
    Result := Result + EndTag(EvoXOptionsTag) + #13#10;
  end;

  // DataSource
  Result := Result + StartTag(EvoXDataSourceTag) + #13#10;
  Result := Result + InsertTag(EvoXDataSourceNameTag, AddCDataTag(FDataSourceName));
  if FDataSourceOptions.Count > 0 then
  begin
    Result := Result + StartTag(EvoXOptionsTag) + #13#10;
    for i := 0 to FDataSourceOptions.Count - 1 do
      Result := Result + InsertTag(EvoXOptionTag, FDataSourceOptions[i]);
    Result := Result + EndTag(EvoXOptionsTag) + #13#10;
  end;
  Result := Result + EndTag(EvoXDataSourceTag) + #13#10;

  // fields
  Result := Result + StartTag(EvoXFieldsTag) + #13#10;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    MapFieldValue := IInterface(FFieldsList.Values[i].Value) as IevEchangeMapField;
    Result := Result + MapFieldValue.GetXML + #13#10;
  end;
  Result := Result + EndTag(EvoXFieldsTag) + #13#10;

  Result := Result + EndTag(EvoXevExchangeMapFileTag) + #13#10;
end;

procedure TEvExchangeMapFile.InitOptions;
begin
  if FMapFileOptions.IndexOfName(EvoXHowToHandleNullsOption) = -1 then
    FMapFileOptions.Add(EvoXHowToHandleNullsOption + '=' + EvoXIgnoreNulls);
end;

procedure TEvExchangeMapFile.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FDescription := AStream.ReadString;
  FPackageName := AStream.ReadString;
  FPackageVersion := AStream.ReadInteger;
  FExampleFileName := AStream.ReadString;
  FDataSourceName := AStream.ReadString;
  FDataSourceOptions.Text := AStream.ReadString;
  FFieldsList.ReadFromStream(AStream);
  AStream.ReadStream(FExampleData);
  FMapFileOptions.Text := AStream.ReadString;
end;

procedure TEvExchangeMapFile.SetDataSourceName(ADataSourceName: String);
begin
  FDataSourceName := ADataSourceName;
end;

procedure TEvExchangeMapFile.SetDataSourceOptions(
  ADataSourceOptions: IisStringList);
begin
  CheckCondition(Assigned(ADataSourceOptions), 'Datasource options object is not assigned');
  FDataSourceOptions := ADataSourceOptions;
end;

procedure TEvExchangeMapFile.SetDescription(ADescription: String);
begin
  FDescription := ADescription;
end;

procedure TEvExchangeMapFile.SetExampleData(AExampleData: IEvDualStream);
begin
  FExampleData := AExampleData.GetClone;
end;

procedure TEvExchangeMapFile.SetExampleFileName(AExampleFileName: String);
begin
  FExampleFileName := AExampleFileName;
end;

procedure TEvExchangeMapFile.SetMapFileOptions(AMapFileOptions: IisStringList);
begin
  CheckCondition(Assigned(AMapFileOptions), 'MapFile options object is not assigned');
  FMapFileOptions := AMapFileOptions;
end;

procedure TEvExchangeMapFile.SetPackageInformation(
  APackageInformation: IEvExchangePackage);
begin
  FPackageName := APackageInformation.GetName;
  FPackageVersion := APackageInformation.GetVersion;
end;

procedure TEvExchangeMapFile.SetPackageName(APackageName: String);
begin
  FPackageName := APackageName;
end;

procedure TEvExchangeMapFile.SetPackageVersion(APackageVersion: integer);
begin
  FPackageVersion := APackageVersion;
end;

procedure TEvExchangeMapFile.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FDescription);
  AStream.WriteString(FPackageName);
  AStream.WriteInteger(FPackageVersion);
  AStream.WriteString(FExampleFileName);
  AStream.WriteString(FDataSourceName);
  AStream.WriteString(FDataSourceOptions.Text);
  FFieldsList.WriteToStream(AStream);
  AStream.WriteStream(FExampleData);
  AStream.WriteString(FMapFileOptions.Text);
end;

initialization
  ObjectFactory.Register([TevEchangeMapField, TEvExchangeMapFile]);

finalization
  SafeObjectFactoryUnRegister([TevEchangeMapField, TEvExchangeMapFile]);

end.
