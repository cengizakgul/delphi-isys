unit EvExchangePackage;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils, isBasicUtils, DB,
     EvConsts, EvStreamUtils, EvTypes, rwCustomDataDictionary, SDDStreamClasses,
     rwEngineTypes, EvExchangeConsts, EvExchangeUtils, isTypes;

type
  TEvExchangePackageGroup = class(TisInterfacedObject, IEvExchangePackageGroup)
  private
    FName: String;
    FDisplayName: String;
  protected
    procedure DoOnConstruction; override;

    // IEvExchangePackageGroup implementation
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetXML: String;
    procedure DoReconcile;

    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    class function GetTypeID: String; override;

    constructor Create(const AGroupName : String; const ADisplayGroupName: String);
    destructor Destroy; override;
  end;

  TEvExchangePackageSection = class(TisInterfacedObject, IEvExchangePackageSection)
  private
    FName: String;
    FDisplayName: String;
  protected
    procedure DoOnConstruction; override;

    // IEvExchangePackageSection implementation
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetXML: String;
    procedure DoReconcile;

    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    class function GetTypeID: String; override;

    constructor Create(const ASectionName : String; const ADisplaySectionName: String);
    destructor Destroy; override;
  end;

  TEvExchangePackageField = class(TisInterfacedObject, IEvExchangePackageField)
  private
    FExchangeFieldName: String;
    FDisplayName: String;
    FEvTableName: String;
    FEvFieldName: String;
    FDDField: TddsField;
    FCheckRuleForRequiremet: boolean;
    FRelatedParentField: String;
    FCalcDefault: boolean;
    FMapRecords: IisListOfValues;
    FKeyTables: IisStringList;
    FKeyFields: IisStringList;
    FKeyValues: IisStringList;
    FSectionRecords: IisStringList;
    FDefaultMapRecord: String;
    FDescription: String;
    FFieldSize: integer;
    FPackageMapRecordName: String;
    FisKeyField: boolean;
    FignoreRequiredByDD: boolean;
    FcheckRequiredByDD: boolean;
    FuseEvoNamesForMap: boolean;
    FTrimInputSpaces: boolean;
    FAlwaysRequired: boolean;
    FGroupName: String;
    FRecordNumber: integer; // 0 - it's group definition field, record numbers starts from 1. not part of XML!!!
    FDefExchangeFieldName: String;  // not part of XML!!!
    FPreFormatFunctionName: String;
    FAsOfDateEnabled: boolean;
    FRequiredIfAnyMappedForTable: boolean;
    FRequiredIfAnyMappedForOtherTable: String; // it's other table name
    FDelayedPostTableNumber: integer;
    FNotPostThisFieldFlag: boolean;
    FFieldValues: IisListOfValues; // not streamed field

    //lookup data group of fields
    FLookupTableName: String;
    FLookupFieldName: String;
    FLookupFieldWidth: integer;
    FLookupDDField: TddsField;
  protected
    procedure DoOnConstruction; override;
    procedure LoadDDField;
    procedure LoadLookupDDField;
    procedure RefreshMapRecordsInfo;

    // IEvExchangePackageField implementation
    function  GetExchangeFieldName: String;
    function  GetDisplayName: String;
    function  GetEvTableName: String;
    function  GetEvFieldName: String;
    function  GetDDField: TddsField;
    function  GetEvDisplayName: String;
    function  GetType: TDataDictDataType;
    function  GetTypeName: String;
    function  GetFieldSize: integer;
    function  GetFieldValues: IisListOfValues;
    function  GetDefaultValue: String;
    function  GetRequiredByDD: boolean;
    function  GetRelatedParentField: String;
    function  GetMapRecord(AMapRecordName: String): IEvImportMapRecord;
    function  GetFirstMapRecordByType(const AMapRecordType: TEvImportMapType): IEvImportMapRecord;
    function  GetMapRecords: IisListOfValues;
    function  GetMapRecordsByType(const AMapType: TEvImportMapType): IisListOfValues;
    function  GetDefaultMapRecordName: String;
    function  GetPackageMapRecordName: String;
    function  GetKeyTables: IisStringListRO;
    function  GetKeyFields: IisStringListRO;
    function  GetKeyValues: IisStringListRO;
    function  GetSectionRecords: IisStringListRO;
    function  GetDescription: String;
    function  GetIgnoreRequiredByDD: boolean;
    function  GetCheckRequiredByDD: boolean;
    function  GetUseEvoNamesForMap: boolean;
    function  GetTrimInputSpaces: boolean;
    function  GetAlwaysRequired: boolean;
    function  GetGroupName: String;
    function  GetRecordNumber: integer;
    function  GetDefExchangeFieldName: String;
    function  GetPreFormatFunctionName: String;
    function  GetAsOfDateEnabled: boolean;
    function  GetRequiredIfAnyMappedForTable: boolean;
    function  GetRequiredIfAnyMappedForOtherTable: String;
    function  GetDelayedPostTableNumber: integer;
    function  GetNotPostThisFieldFlag: boolean;
    function  GetLookupTableName: String;
    function  GetLookupFieldName: String;
    function  GetXML: String;

    function  IsCheckRuleForRequirement: boolean;
    function  IsCalcDefault: boolean;
    function  IsKeyField: boolean;
    function  IsGroupDefinitionField: boolean;
    function  IsBelongsToGroup: boolean;
    function  IsBelongsToSection(const aSectionName: string): boolean;

    procedure SetDisplayName(const Value: String);
    procedure SetCheckRuleForRequiremet(const Value: boolean);
    procedure SetRelatedparentField(const Value: String);
    procedure SetCalcDefault(AValue: boolean);
    procedure SetMapRecords(AValue: IisListOfValues);
    procedure SetDefaultMapRecordName(const AName: String);
    procedure SetKeys(AKeyTables: IisStringListRO; AKeyFields: IisStringListRO; AKeyValues: IisStringListRO);
    procedure SetSectionRecords(ASectionRecords: IisStringListRO);
    procedure SetPackageMapRecordName(const AName: String);
    procedure SetDescription(const Value: String);
    procedure SetFieldSize(const Value: integer);
    procedure SetAsKeyField(const Value: boolean);
    procedure SetIgnoreRequiredByDD(const Value: boolean);
    procedure SetCheckRequiredByDD(const Value: boolean);
    procedure SetUseEvoNamesForMap(const Value: boolean);
    procedure SetTrimInputSpaces(const Value: boolean);
    procedure SetAlwaysRequired(const Value: boolean);
    procedure SetGroupName(const Value: String);
    procedure SetDefExchangeFieldName(const Value: String);
    procedure SetRecordNumber(const Value: integer);
    procedure SetPreFormatFunctionName(const Value: String);
    procedure SetAsOfDateEnabled(const Value: boolean);
    procedure SetRequiredIfAnyMappedForTable(const Value: boolean);
    procedure SetRequiredIfAnyMappedForOtherTable(const ATableName: String);
    procedure SetDelayedPostTableNumber(const AValue: integer);
    procedure SetNotPostThisFieldFlag(const AValue: boolean);
    procedure SetLookupData(const ATableName, AFieldName: String; AFieldSize: integer = 0);
    procedure DoReconcile;

    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  Revision: TisRevision; override;
  public
    property CheckRuleForRequiremet : boolean read FCheckRuleForRequiremet write SetCheckRuleForRequiremet;
    property RelatedparentField: String read GetRelatedParentField write SetRelatedparentField;

    class function GetTypeID: String; override;

    constructor Create(const AExchangeFieldName : String; const ATableName: String; const AFieldName : String); reintroduce;
    destructor Destroy; override;
  end;

  TEvExchangePackage = class(TisInterfacedObject, IEvExchangePackage)
  private
    FDisplayName: String;
    FName: String;
    FVersion: Integer;
    FFieldsList: IisListOfValues;
    FGroupsList:  IisListOfValues;
    FSectionList: IisListOfValues;
    FEngineName: String;

    procedure SetDisplayName(const Value: String);
    procedure SetName(const Value: String);
    procedure SetVersion(const Value: integer);
    procedure SetEngineName(const Value: String);
  protected
    procedure Clear;
    procedure DoOnConstruction; override;
    function  DoLogicalCheck: IisStringList;

    // IEvExchangePackage implementation  ////////////////////////
    function  GetName: String;
    function  GetDisplayName: String;
    function  GetVersion: integer;
    function  GetFieldsList: IisListOfValues;
    function  GetFieldByName(const AFieldName: String): IEvExchangePackageField;
    function  GetEngineName: String;

    function  GetXML: String;

    function  GetGroupsList: IisListOfValues;
    function  GetSectionsList: IisListOfValues;
    function  GetGroupByName(const AGroupName: String): IEvExchangePackageGroup;
    function  GetSectionByName(const ASectionName: String): IEvExchangePackageSection;
    function  GetFieldsDefsByGroupName(const AGroupName: String): IisListOfValues;
    function  GetFieldsDefsBySectionName(const ASectionName: String): IisListOfValues;
    function  GetMaxFieldsDefsIndexByGroupName(const AGroupName: String): integer;
    function  AddNewRecordToGroup(const AGroupName: String): integer;
    procedure AddRecordWithNumberToGroup(const AGroupName: String; const ARecordNumber: integer);
    procedure DeleteRecordFromGroup(const AGroupName: String; const ARecordNumber: integer);
    procedure DeleteAllGroupRecords;
    function  isGroupRecordExist(const AGroupName: String; const ARecordNumber: integer): boolean;
    function  GetGroupRecordNumbers(const AGroupName: String): TisDynIntegerArray;
    function  GetListOfEvAndVirtualTables: IisStringList;
    function  GetFieldsForEvOrVirtualTable(const AEvTable: String): IisListOfValues;

    procedure DoReconcile;
    ////////////////////////////////////////////////////////

    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);  override;
  public
    property  Name: String read FName write SetName;
    property  DisplayName: String read FDisplayName write SetDisplayName;
    property  Version: integer read FVersion write SetVersion;
    property  EngineName: String read FEngineName write SetEngineName;

    class function GetTypeID: String; override;
  end;


implementation

uses EvImportMapRecord;

{ EvExchangePackage }

function TEvExchangePackage.AddNewRecordToGroup(const AGroupName: String): integer;
var
  GroupFieldsList: IisListOfValues;
  Records: TisDynIntegerArray;
begin
  GroupFieldsList := GetFieldsDefsByGroupName(AGroupName);
  CheckCondition(GroupFieldsList.Count > 0, 'Group "' + AGroupName + '" is empty. Cannot add record');
  Records := GetGroupRecordNumbers(AGroupName);

  if Length(Records) = 0 then
    Result := 1
  else
    Result := Records[High(Records)] + 1;

  AddRecordWithNumberToGroup(AGroupName, Result);
end;

procedure TEvExchangePackage.Clear;
begin
  FDisplayName := '';
  FName := '';
  FVersion := 0;
  FFieldsList.Clear;
  FGroupsList.Clear;
  FSectionList.Clear;
end;

procedure TEvExchangePackage.DeleteRecordFromGroup(const AGroupName: String; const ARecordNumber: integer);
var
  i: integer;
  PackageField: IEvExchangePackageField;
begin
  for i := FFieldsList.Count - 1 downto 0 do
  begin
    PackageField := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if PackageField.IsBelongsToGroup and (not PackageField.IsGroupDefinitionField) and
      (PackageField.GetRecordNumber = ARecordNumber) and (PackageField.GetGroupName = AGroupName) then
      FFieldsList.DeleteValue(PackageField.GetExchangeFieldName);
  end;
end;

function TEvExchangePackage.DoLogicalCheck: IisStringList;
var
  i, j: integer;
  Field, RelatedField: IEvExchangePackageField;
  Group: IEvExchangePackageGroup;
  Section: IEvExchangePackageSection;
begin
  Result := TisStringList.Create;
  if Trim(FDisplayName) = '' then
    Result.Add('Display name is empty');
  if Trim(Name) = '' then
    Result.Add('Name is empty');
  if Trim(FEngineName) = '' then
    Result.Add('Engine name is empty');
  if FFieldsList.Count = 0 then
    Result.Add('No fields defined');

  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if Field.GetRelatedParentField <> '' then
      try
        RelatedField := IInterface(FFieldsList.Value[Field.GetRelatedParentField]) as IEvExchangePackageField;
      except
        on E:Exception do
          Result.Add('For field "' + Field.GetExchangeFieldName + '" cannot find related field with name: ' + Field.GetRelatedParentField);
      end;
    if Field.GetGroupName <> '' then
      try
        Group :=  GetGroupByName(Field.GetGroupName);
      except
        on E:Exception do
          Result.Add('For field "' + Field.GetExchangeFieldName + '" cannot find related group with name: ' + Field.GetGroupName);
      end;
    for j := 0 to Field.GetSectionRecords.Count - 1 do
      try
        Section := GetSectionByName(Field.GetSectionRecords[j]);
      except
        on E:Exception do
          Result.Add('For field "' + Field.GetExchangeFieldName + '" cannot find related section with name: ' + Field.GetSectionRecords[j]);
      end;
  end;
end;

procedure TEvExchangePackage.DoOnConstruction;
begin
  inherited;
  FFieldsList := TisListOfValues.Create;
  FGroupsList := TisListOfValues.Create;
  FSectionList := TisListOfValues.Create;
end;

procedure TEvExchangePackage.DoReconcile;
var
  i, j: integer;
  bFound: boolean;
  PackageField, RelatedPackageField: IEvExchangePackageField;
  KeyValues: IisStringListRO;
begin
  for i := 0 to FFieldsList.Count - 1 do
  begin
    PackageField := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;

    KeyValues := PackageField.GetKeyValues;
    for j := 0 to KeyValues.Count - 1 do
      if (KeyValues[j] <> UseSelfValue) and (KeyValues[j] <> UseNullValue) then
      begin
        RelatedPackageField := GetFieldByName(KeyValues[j]);
        CheckCondition(Assigned(RelatedPackageField), 'Package field "' + PackageField.GetExchangeFieldName +
          '" refers to "' + KeyValues[j] + '" package field as source of key values. But referenced field not found in package');
      end;

    // checking that group exist, if field belongs to group
    if PackageField.IsBelongsToGroup then
      if not FGroupsList.ValueExists(PackageField.GetGroupName) then
        raise Exception.Create('Package field "' + PackageField.GetExchangeFieldName + '" belongs to not existing group: "' +
          PackageField.GetGroupName + '"');

    // checking that group field definition exist if feld belongs to group
    if (PackageField.IsBelongsToGroup) and (not PackageField.IsGroupDefinitionField) then
    begin
      bFound := false;
      for j := 0 to FFieldsList.Count - 1 do
      begin
        if j = i then
          continue;

        RelatedPackageField := IInterface(FFieldsList.Values[j].Value) as IEvExchangePackageField;
        if (RelatedPackageField.IsBelongsToGroup) and (RelatedPackageField.IsGroupDefinitionField)
          and (RelatedPackageField.GetGroupName = PackageField.GetGroupName) then
        begin
          bFound := true;
          break;
        end;
      end;  // for j
      CheckCondition(bFound, 'Package field "' + PackageField.GetExchangeFieldName + '" belongs to group: "' +
        PackageField.GetGroupName + '" but there''s no group definition field found');
    end;

  end;  // for i
end;

function TEvExchangePackage.GetDisplayName: String;
begin
  Result := FDisplayName;
end;

function TEvExchangePackage.GetEngineName: String;
begin
  Result := FEngineName;
end;

function TEvExchangePackage.GetFieldByName(const AFieldName: String): IEvExchangePackageField;
begin
   Result := IInterface(FFieldsList.Value[AFieldName]) as IEvExchangePackageField;
end;

function TEvExchangePackage.GetFieldsDefsByGroupName(const AGroupName: String): IisListOfValues;
var
  i: integer;
  Field: IEvExchangePackageField;
begin
  Result := TisListOfValues.Create;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if (Field.GetGroupName = AGroupName) and Field.IsGroupDefinitionField then
      Result.AddValue(Field.GetExchangeFieldName, Field);
  end;
end;

function TEvExchangePackage.GetFieldsList: IisListOfValues;
begin
  Result := FFieldsList;
end;

function TEvExchangePackage.GetGroupByName(
  const AGroupName: String): IEvExchangePackageGroup;
begin
   Result := IInterface(FGroupsList.Value[AGroupName]) as IEvExchangePackageGroup;
end;

function TEvExchangePackage.GetGroupsList: IisListOfValues;
begin
  Result := FGroupsList;
end;

function TEvExchangePackage.GetSectionsList: IisListOfValues;
begin
  Result := FSectionList;
end;

function TEvExchangePackage.GetName: String;
begin
  Result := FName;
end;

function TEvExchangePackage.GetGroupRecordNumbers(const AGroupName: String): TisDynIntegerArray;
var
  i, j: integer;
  PackageField: IEvExchangePackageField;
  tmp: integer;
begin
  SetLength(Result, 0);
  for i := 0 to FFieldsList.Count - 1 do
  begin
    PackageField := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if (PackageField.GetGroupName = AGroupName) and (not PackageField.IsGroupDefinitionField)
      and (IndexInArray(Result, PackageField.GetRecordNumber) = -1)  then
    begin
      SetLength(Result, Succ(Length(Result)));
      Result[High(Result)] := PackageField.GetRecordNumber;
    end;
  end;  // for i

  if Length(Result) > 0 then
  begin
    // sorting the result
    for i := 0 to High(Result) do
      for j := 0 to High(Result) - 1 do
      begin
        if i = j then
          continue;
        if Result[j] > Result[j+1] then
        begin
          tmp := Result[j];
          Result[j] := Result[j+1];
          Result[j+1] := Tmp;
        end;
      end;
  end;

end;

class function TEvExchangePackage.GetTypeID: String;
begin
  Result := 'EvExchangePackage';
end;

function TEvExchangePackage.GetVersion: integer;
begin
  Result := FVersion;
end;

function TEvExchangePackage.GetXML: String;
var
  i: integer;
  FieldValue: IEvExchangePackageField;
  GroupValue: IEvExchangePackageGroup;
  SectionValue: IEvExchangePackageSection;
begin
  Result := EvoXXMLHeader + #13#10 + StartTag(EvoXEvExchangePackageTag) + #13#10;
  Result := Result + InsertTag(EvoXPackageNameTag, GetName);
  Result := Result + InsertTag(EvoXDisplayNameTag, GetDisplayName);
  Result := Result + InsertTag(EvoXVersionTag, IntToStr(GetVersion));
  Result := Result + InsertTag(EvoXEngineNameTag, GetEngineName);

  Result := Result + StartTag(EvoXFieldsTag) + #13#10;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    FieldValue := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    Result := Result + FieldValue.GetXML + #13#10;
  end;
  Result := Result + EndTag(EvoXFieldsTag) + #13#10;

  Result := Result + StartTag(EvoXGroupsTag) + #13#10;
  for i := 0 to FGroupsList.Count - 1 do
  begin
    GroupValue := IInterface(FGroupsList.Values[i].Value) as IEvExchangePackageGroup;
    Result := Result + GroupValue.GetXML + #13#10;
  end;
  Result := Result + EndTag(EvoXGroupsTag) + #13#10;

  Result := Result + StartTag(EvoXSectionsTag) + #13#10;
  for i := 0 to FSectionList.Count - 1 do
  begin
    SectionValue := IInterface(FSectionList.Values[i].Value) as IEvExchangePackageSection;
    Result := Result + SectionValue.GetXML + #13#10;
  end;
  Result := Result + EndTag(EvoXSectionsTag) + #13#10;

  Result := Result + EndTag(EvoXEvExchangePackageTag) + #13#10;
end;

function TEvExchangePackage.isGroupRecordExist(const AGroupName: String; const ARecordNumber: integer): boolean;
var
  i: integer;
  PackageField: IEvExchangePackageField;
begin
  Result := false;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    PackageField := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if PackageField.IsBelongsToGroup and (not PackageField.IsGroupDefinitionField) and
      (PackageField.GetRecordNumber = ARecordNumber) and AnsiSameText(PackageField.GetGroupName, AGroupName) then
    begin
      Result := true;
      exit;
    end;
  end;
end;

procedure TEvExchangePackage.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FName := ASTream.ReadString;
  FDisplayName := AStream.ReadString;
  FVersion := AStream.ReadInteger;
  FEngineName := AStream.ReadString;
  FFieldsList.ReadFromStream(AStream);
  FGroupsList.ReadFromStream(AStream);
  FSectionList.ReadFromStream(AStream);
end;

procedure TEvExchangePackage.SetDisplayName(const Value: String);
begin
  FDisplayName := Value;
end;

procedure TEvExchangePackage.SetEngineName(const Value: String);
begin
  FEngineName := Value;
end;

procedure TEvExchangePackage.SetName(const Value: String);
begin
  FName := AnsiUpperCase(Value);
end;

procedure TEvExchangePackage.SetVersion(const Value: integer);
begin
  FVersion := Value;
end;

procedure TEvExchangePackage.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  ASTream.WriteString(FName);
  AStream.WriteString(FDisplayName);
  AStream.WriteInteger(FVersion);
  AStream.WriteString(FEngineName);
  FFieldsList.WriteToStream(AStream);
  FGroupsList.WriteToStream(AStream);
  FSectionList.WriteToStream(AStream);
end;

{ TEvExchangePackageField }

constructor TEvExchangePackageField.Create(const AExchangeFieldName : String; const ATableName: String; const AFieldName : String);
begin
  inherited Create;

  FMapRecords := TisListOfValues.Create;
  FExchangeFieldName := AExchangeFieldName;
  FDisplayName := FExchangeFieldName;
  FEvTableName := AnsiUpperCase(ATableName);
  FEvFieldName := AnsiUpperCase(AFieldName);

  LoadDDField;
  LoadLookupDDField;
end;

destructor TEvExchangePackageField.Destroy;
begin
  FDDField := nil; // do not free global object
  inherited;
end;

procedure TEvExchangePackageField.LoadDDField;
var
  i: integer;
begin
  try
    FDDField := TddsField(EvoDataDictionary.Tables.TableByName(FevTableName).Fields.FieldByName(FEvFieldName));
  except
    on E: Exception do
      raise Exception.Create('Cannot find information in data dictionary. Table: ' + FevTableName +
        '. Field: ' + FEvFieldName + '. Exception: ' + E.Message);
  end;
  CheckCondition(Assigned(FDDField), 'Cannot find information in data dictionary. Table: ' + FevTableName +
    '. Field: ' + FEvFieldName);

  FFieldValues.Clear;
  for i := 0 to FDDField.FieldValues.Count - 1 do
    FFieldValues.AddValue(FDDField.FieldValues.Names[i], FDDField.FieldValues.ValueFromIndex[i]);
end;

function TEvExchangePackageField.GetDefaultValue: String;
begin
  Result := FDDField.DefaultValue;  
end;

function TEvExchangePackageField.GetEvDisplayName: String;
begin
  if FDDField.DisplayName <> '' then
    Result := FDDField.DisplayName
  else
    Result := FExchangeFieldName;  
end;

function TEvExchangePackageField.GetEvFieldName: String;
begin
  Result := FEvFieldName;
end;

function TEvExchangePackageField.GetEvTableName: String;
begin
  Result := FEvTableName
end;

function TEvExchangePackageField.GetExchangeFieldName: String;
begin
  Result := FExchangeFieldName;
end;

function TEvExchangePackageField.GetFieldSize: integer;
begin
  if FLookupTableName <> '' then
    Result := FLookupFieldWidth
  else
    Result := FFieldSize;
end;

function TEvExchangePackageField.GetFieldValues: IisListOfValues;
begin
  Result := FFieldValues;
end;

function TEvExchangePackageField.GetType: TDataDictDataType;
begin
  if FLookupTableName <> '' then
    Result := FLookupDDField.FieldType
  else
    Result := FDDField.FieldType;
end;

class function TEvExchangePackageField.GetTypeID: String;
begin
  Result := 'EvExchangePackageField';
end;

function TEvExchangePackageField.GetTypeName: String;
begin
  case GetType of
  ddtUnknown : Result := 'Unknown';
  ddtInteger :  Result := 'Integer';
  ddtFloat : Result := 'Float';
  ddtCurrency : Result := 'Currency';
  ddtDateTime : Result := 'DateTime';
  ddtString : Result := 'String';
  ddtBoolean : Result := 'Boolean';
  ddtBLOB: Result := 'BLOB';
  else
    raise Exception.Create('Unsupported datatype: ' + IntToStr(Integer(FDDField.FieldType)));
  end;
end;

procedure TEvExchangePackageField.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FExchangeFieldName := AStream.ReadString;
  FEvTableName := AStream.ReadString;
  FEvFieldName := AStream.ReadString;
  FCheckRuleForRequiremet := AStream.ReadBoolean;
  FRelatedParentField := AStream.ReadString;
  FCalcDefault := AStream.ReadBoolean;
  FMapRecords.ReadFromStream(AStream);
  FKeyTables.Text := AStream.ReadString;
  FKeyFields.Text := AStream.ReadString;
  FKeyValues.Text := AStream.ReadString;
  FDefaultMapRecord := AStream.ReadString;
  FDescription := AStream.ReadString;
  FFieldSize := AStream.ReadInteger;
  FPackageMapRecordName := AStream.ReadString;
  FisKeyField := AStream.ReadBoolean;
  FignoreRequiredByDD := AStream.ReadBoolean;
  FCheckRequiredByDD := AStream.ReadBoolean;
  FuseEvoNamesForMap := AStream.ReadBoolean;
  FTrimInputSpaces := AStream.ReadBoolean;
  FDisplayName := AStream.ReadString;
  FAlwaysRequired := AStream.ReadBoolean;
  FGroupName := AStream.ReadString;
  FRecordNumber := AStream.ReadInteger;
  FDefExchangeFieldName := AStream.ReadString;
  FPreFormatFunctionName := AStream.ReadString;
  FAsOfDateEnabled := AStream.ReadBoolean;
  FRequiredIfAnyMappedForTable := AStream.ReadBoolean;
  FRequiredIfAnyMappedForOtherTable := AStream.ReadString;
  FDelayedPostTableNumber := AStream.ReadInteger;
  FNotPostThisFieldFlag := AStream.ReadBoolean;

  if ARevision > 0 then
  begin
    FLookupTableName := AStream.ReadString;
    FLookupFieldName := AStream.ReadString;
    FLookupFieldWidth := AStream.ReadInteger;
  end;

  FSectionRecords.Text := AStream.ReadString;

  LoadDDField;
  LoadLookupDDField;
end;

procedure TEvExchangePackageField.SetCheckRuleForRequiremet(
  const Value: boolean);
begin
  FCheckRuleForRequiremet := Value;
end;

procedure TEvExchangePackageField.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FExchangeFieldName);
  AStream.WriteString(FEvTableName);
  AStream.WriteString(FEvFieldName);
  AStream.WriteBoolean(FCheckRuleForRequiremet);
  AStream.WriteString(FRelatedParentField);
  AStream.WriteBoolean(FCalcDefault);
  FMapRecords.WriteToStream(AStream);
  AStream.WriteString(FKeyTables.Text);
  AStream.WriteString(FKeyFields.Text);
  AStream.WriteString(FKeyValues.Text);
  AStream.WriteString(FDefaultMapRecord);
  AStream.WriteString(FDescription);
  AStream.WriteInteger(FFieldSize);
  AStream.WriteString(FPackageMapRecordName);
  AStream.WriteBoolean(FisKeyField);
  AStream.WriteBoolean(FignoreRequiredByDD);
  AStream.WriteBoolean(FCheckRequiredByDD);
  AStream.WriteBoolean(FuseEvoNamesForMap);
  AStream.WriteBoolean(FTrimInputSpaces);
  AStream.WriteString(FDisplayName);
  AStream.WriteBoolean(FAlwaysRequired);
  AStream.WriteString(FGroupName);
  AStream.WriteInteger(FRecordNumber);
  AStream.WriteString(FDefExchangeFieldName);
  AStream.WriteString(FPreFormatFunctionName);
  AStream.WriteBoolean(FAsOfDateEnabled);
  AStream.WriteBoolean(FRequiredIfAnyMappedForTable);
  AStream.WriteString(FRequiredIfAnyMappedForOtherTable);
  AStream.WriteInteger(FDelayedPostTableNumber);
  AStream.WriteBoolean(FNotPostThisFieldFlag);
  AStream.WriteString(FLookupTableName);
  AStream.WriteString(FLookupFieldName);
  AStream.WriteInteger(FLookupFieldWidth);
  AStream.WriteString(FSectionRecords.Text);
end;

function TEvExchangePackageField.GetXML: String;
var
  MapRecord: IEvImportMapRecord;
  i: integer;
begin
  Result := StartTag(EvoXFieldTag);
  Result := Result + InsertTag(EvoXExchangeFieldNameTag, GetExchangeFieldName);
  Result := Result + InsertTag(EvoXDisplayNameTag, GetDisplayName);
  Result := Result + InsertTag(EvoXEvTableNameTag, GetEvTableName);
  Result := Result + InsertTag(EvoXEvFieldNameTag, GetEvFieldName);
  Result := Result + InsertTag(EvoXDefaultMapRecordTag, FDefaultMapRecord);
  Result := Result + InsertTag(EvoXPackageMapRecordTag, FPackageMapRecordName);
  Result := Result + InsertTag(EvoXfieldDescriptionTag, FDescription);
  Result := Result + InsertTag(EvoXmapFieldSizeTag, IntToStr(FFieldSize));
  Result := Result + InsertTag(EvoXgroupNameTag, FGroupName);
  Result := Result + InsertTag(EvoXpreFormatFunctionName, FPreFormatFunctionName);

  if FDelayedPostTableNumber >= 0 then
    Result := Result + InsertTag(EvoXdelayedPostTableNumber, IntToStr(FDelayedPostTableNumber));

  if FignoreRequiredByDD then
    Result := Result + InsertTag(EvoXignoreRequiredByDDTag, 'true')
  else
    Result := Result + InsertTag(EvoXignoreRequiredByDDTag, 'false');

  if FCheckRequiredByDD then
    Result := Result + InsertTag(EvoXCheckRequiredByDDTag, 'true')
  else
    Result := Result + InsertTag(EvoXCheckRequiredByDDTag, 'false');

  if FNotPostThisFieldFlag then
    Result := Result + InsertTag(EvoXNotPostThisFieldFlagTag, 'true')
  else
    Result := Result + InsertTag(EvoXNotPostThisFieldFlagTag, 'false');

  if FuseEvoNamesForMap then
    Result := Result + InsertTag(EvoXFuseEvoNamesForMapTag, 'true')
  else
    Result := Result + InsertTag(EvoXFuseEvoNamesForMapTag, 'false');

  if FisKeyField then
    Result := Result + InsertTag(EvoXfieldIsKeyTag, 'true')
  else
    Result := Result + InsertTag(EvoXfieldIsKeyTag, 'false');

  if FRelatedParentField <> '' then
    Result := Result + InsertTag(EvoXrelatedParentFieldTag, FRelatedParentField);

  if FTrimInputSpaces then
    Result := Result + InsertTag(EvoXtrimInputSpacesTag, 'true')
  else
    Result := Result + InsertTag(EvoXtrimInputSpacesTag, 'false');

  if FCheckRuleForRequiremet then
    Result := Result + InsertTag(EvoXCalcRequiredTag, 'true')
  else
    Result := Result + InsertTag(EvoXCalcRequiredTag, 'false');

  if FCalcDefault then
    Result := Result + InsertTag(EvoXCalcDefaultTag, 'true')
  else
    Result := Result + InsertTag(EvoXCalcDefaultTag, 'false');

  if FAlwaysRequired then
    Result := Result + InsertTag(EvoXfieldAlwaysRequiredTag, 'true')
  else
    Result := Result + InsertTag(EvoXfieldAlwaysRequiredTag, 'false');

  if FAsOfDateEnabled then
    Result := Result + InsertTag(EvoXasOfDateEnabledTag, 'true')
  else
    Result := Result + InsertTag(EvoXasOfDateEnabledTag, 'false');

  if FRequiredIfAnyMappedForTable then
    Result := Result + InsertTag(EvoXrequiredIfAnyMappedForTable, 'true')
  else
    Result := Result + InsertTag(EvoXrequiredIfAnyMappedForTable, 'false');

  if FRequiredIfAnyMappedForOtherTable <> '' then
    Result := Result + InsertTag(EvoXrequiredIfAnyMappedForOtherTable, FRequiredIfAnyMappedForOtherTable);

  // sections info
  Result := Result + #13#10 + StartTag(EvoXsectionRecordsTag) + #13#10;
  for i := 0 to FSectionRecords.Count - 1 do
    Result := Result + InsertTag(EvoXsectionRecordTag, FSectionRecords[i]);
  Result := Result + #13#10 + EndTag(EvoXsectionRecordsTag) + #13#10;

  if FLookupTableName <> '' then
  begin
    Result := Result + #13#10 + StartTag(EvoXLookupDataTag) + #13#10;

    Result := Result + InsertTag(EvoXLookupTableTag, FLookupTableName);
    Result := Result + InsertTag(EvoXLookupFieldTag, FLookupFieldName);
    Result := Result + InsertTag(EvoXfieldWidthTag, IntToStr(FLookupFieldWidth));

    Result := Result + #13#10 + EndTag(EvoXLookupDataTag) + #13#10;
  end;

  // Key records info
  Result := Result + #13#10 + StartTag(EvoXKeyRecordsTag) + #13#10;
  for i := 0 to FKeyTables.Count - 1 do
  begin
    Result := Result + #13#10 + StartTag(EvoXKeyRecordTag) + #13#10;

    Result := Result + InsertTag(EvoXKeyTableTag, FKeyTables[i]);
    Result := Result + InsertTag(EvoXKeyFieldTag, FKeyFields[i]);
    Result := Result + InsertTag(EvoXKeyValueTag, FKeyValues[i]);

    Result := Result + #13#10 + EndTag(EvoXKeyRecordTag) + #13#10;
  end;
  Result := Result + #13#10 + EndTag(EvoXKeyRecordsTag) + #13#10;

  // map records info
  Result := Result + #13#10 + StartTag(EvoXmapRecordsTag) + #13#10;
  for i := 0 to FMapRecords.Count - 1 do
  begin
    MapRecord := IInterface(FmapRecords.Values[i].Value) as IEvImportMapRecord;
    Result := Result + MapRecord.GetXML;
  end;
  Result := Result + #13#10 + EndTag(EvoXmapRecordsTag) + #13#10;

  Result := Result + EndTag(EvoXFieldTag);
end;

procedure TEvExchangePackageField.SetRelatedparentField(
  const Value: String);
begin
  FRelatedparentField := Value;
end;

function TEvExchangePackageField.GetRelatedParentField: String;
begin
  Result := FRelatedParentField;
end;

function TEvExchangePackageField.IsCheckRuleForRequirement: boolean;
begin
  Result := FCheckRuleForRequiremet;
end;

function TEvExchangePackageField.GetDDField: TddsField;
begin
  Result := FDDField;
end;

function TEvExchangePackageField.IsCalcDefault: boolean;
begin
  Result := FCalcDefault;
end;

procedure TEvExchangePackageField.SetCalcDefault(AValue: boolean);
begin
  FCalcdefault := AValue;
end;

function TEvExchangePackageField.GetMapRecord(AMapRecordName: String): IEvImportMapRecord;
begin
  Result := IInterface(FMapRecords.Value[AMapRecordName]) as IEvImportMapRecord;
end;

procedure TEvExchangePackageField.RefreshMapRecordsInfo;
var
  i: integer;
  MapRecord: IEvImportMapRecord;
begin
  for i := 0 to FMapRecords.Count - 1 do
  begin
    Maprecord := IInterface(FMapRecords.Values[i].Value) as IEvImportMapRecord;
    MapRecord.ITable := '';
    MapRecord.IField := FExchangeFieldName;
    MapRecord.ETable := FEvTableName;
    MapRecord.EField := FEvFieldName;
  end;
  if FMapRecords.Count = 1 then
    FDefaultMapRecord := FMapRecords.Values[0].Name;
end;

procedure TEvExchangePackageField.DoOnConstruction;
begin
  inherited;
  FMaprecords := TisListOfValues.Create;
  FKeyTables := TisStringList.Create;
  FKeyFields := TisStringList.Create;
  FKeyValues := TisStringList.Create;
  FSectionRecords := TisStringList.Create;
  FuseEvoNamesForMap := true;
  FDelayedPostTableNumber := -1;
  FFieldValues := TisListOfValues.Create;
end;

procedure TEvExchangePackageField.SetMapRecords(AValue: IisListOfValues);
begin
  FMapRecords := AValue;
  RefreshMapRecordsInfo;
end;

function TEvExchangePackageField.GetKeyFields: IisStringListRO;
begin
  Result := FKeyFields as IisStringListRO;
end;

function TEvExchangePackageField.GetKeyTables: IisStringListRO;
begin
  Result := FKeyTables as IisStringListRO;
end;

function TEvExchangePackageField.GetKeyValues: IisStringListRO;
begin
  Result := FKeyValues as IisStringListRO;
end;

procedure TEvExchangePackageField.SetKeys(AKeyTables, AKeyFields,
  AKeyValues: IisStringListRO);
begin
  CheckCondition((AKeyTables.Count = AKeyFields.Count) and (AKeyFields.Count = AKeyValues.Count),
    'All list should have the same amount of members');
  FKeyTables.Text := AKeyTables.Text;
  FKeyFields.Text := AKeyFields.Text;
  FKeyValues.Text := AKeyValues.Text;
end;

function TEvExchangePackageField.GetRequiredByDD: boolean;
const
  EeBenStartDate = 'ee benefit effective start date';
  AssignDependentFirstName = 'assign dependent first name';
  AssignDependentLastName = 'assign dependent last name';
begin
  if LowerCase( Copy(FExchangeFieldName, 1, Length(EeBenStartDate))) = EeBenStartDate then
    Result := False
  else if (LowerCase( Copy(FExchangeFieldName, 1, Length(AssignDependentFirstName))) = AssignDependentFirstName) or
    (LowerCase( Copy(FExchangeFieldName, 1, Length(AssignDependentLastName))) = AssignDependentLastName) or
    ((FEvFieldName = 'STATE_OVERRIDE_VALUE') and (FEvTableName = 'PR_CHECK_STATES')) then
    Result := True
  else
    Result := FDDField.Required;
end;

function TEvExchangePackageField.GetDefaultMapRecordName: String;
begin
  Result := FDefaultMapRecord;
end;

procedure TEvExchangePackageField.SetDefaultMapRecordName(const AName: String);
begin
  FDefaultMapRecord := AName;
end;

procedure TEvExchangePackageField.DoReconcile;
var
  i: integer;
  MapRecord: IEvImportMapRecord;
begin
  CheckCondition(FDisplayName <> '', 'Display name cannot be empty');

  // default map record should exist
  if FDefaultMapRecord <> '' then
    CheckCondition(FMapRecords.ValueExists(FDefaultMapRecord), 'Default map record does not exist. ExchangeField: ' + FExchangeFieldName)
  else
    Assert(false, 'Default map record name cannot be empty. ExchangeField: ' + FExchangeFieldName);

  // package map record should exist
  if FPackageMapRecordName <> '' then
    CheckCondition(FMapRecords.ValueExists(FPackageMapRecordName), 'Package map record does not exist. ExchangeField: ' + FExchangeFieldName)
  else
    Assert(false, 'Package map record name cannot be empty. ExchangeField: ' + FExchangeFieldName);

  // map records should have proper types
  for i := 0 to FMapRecords.Count - 1 do
  begin
    MapRecord := IInterface(FmapRecords.Values[i].Value) as IEvImportMapRecord;
    CheckCondition(MapRecord.MapType in [mtpDirect, mtpLookup, mtpCustom],
      'Map record has wrong type: ' + IntToStr(Integer(MapRecord.MapType)) +
      '. ExchangeField: ' + FExchangeFieldName +
      '. Map record name: ' + FmapRecords.Values[i].Name);
  end;

  // size is required for strings
  if GetType = ddtString then
    CheckCondition(GetFieldSize > 0, 'String fields cannot have zero lengh in package definition. ExchangeField: ' + FExchangeFieldName);

  // Length of KetTables, KeyFields and KeyValues <> 0 and the same
  CheckCondition((FKeyFields.Count = FKeyTables.Count) and (FKeyValues.Count = FKeyTables.Count),
    'Something wrong with Key records. ExchangeField: ' + FExchangeFieldName); 

  if FRecordNumber > 0 then
    CheckCondition(FGroupName <> '', 'Group name is not assigned. ExchangeField: ' + FExchangeFieldName);

  if FGroupName = '' then
    CheckCondition(FDefExchangeFieldName = '', 'FDefExchangeFieldName is allowed for group fields only. ExchangeField: ' + FExchangeFieldName);

  if IsBelongsToGroup and (not IsGroupDefinitionField) then
  begin
    CheckCondition(FRecordNumber > 0, 'Field belongs to group but it has no record number. ExchangeField: ' + FExchangeFieldName);
    CheckCondition(Pos(RecordNumberToStr(FRecordNumber), FExchangeFieldName) > 0, 'Field has no record number in name. ExchangeField: ' + FExchangeFieldName);
    CheckCondition(Pos(RecordNumberToStr(FRecordNumber), FDisplayName) > 0, 'Field has no record number in display name. ExchangeField: ' + FExchangeFieldName);
  end;

  // lookup fields
  if FLookupTableName <> '' then
    CheckCondition(Assigned(FLookupDDField), 'Lookup data not loaded');
end;

function TEvExchangePackageField.GetDescription: String;
begin
  Result := FDescription;
end;

procedure TEvExchangePackageField.SetDescription(const Value: String);
begin
  FDescription := Value;
end;

function TEvExchangePackageField.GetMapRecords: IisListOfValues;
begin
  Result := FMapRecords;
end;

function TEvExchangePackageField.GetMapRecordsByType(const AMapType: TEvImportMapType): IisListOfValues;
var
  i: integer;
  MapRecord: IEvImportMapRecord;
begin
  Result := TisListOfValues.Create;
  for i := 0 to FMapRecords.Count - 1 do
  begin
    MapRecord := IInterface(FmapRecords.Values[i].Value) as IEvImportMapRecord;
    if MapRecord.MapType = AMapType then
      Result.AddValue(FmapRecords.Values[i].Name, MapRecord);
  end;
end;

procedure TEvExchangePackageField.SetFieldSize(const Value: integer);
begin
  FFieldSize := Value;
end;

function TEvExchangePackageField.GetPackageMapRecordName: String;
begin
  Result := FPackageMapRecordName;
end;

procedure TEvExchangePackageField.SetPackageMapRecordName(const AName: String);
begin
  FPackageMapRecordName := AName;
end;

function TEvExchangePackageField.GetFirstMapRecordByType(
  const AMapRecordType: TEvImportMapType): IEvImportMapRecord;
var
  MapRecord: IEvImportMapRecord;
  MapRecords: IisListOfValues;
  i: integer;
begin
  Result := nil;
  MapRecords := GetMapRecords;
  for i := 0 to MapRecords.Count - 1 do
  begin
    MapRecord := IInterface(MapRecords.Values[i].Value) as IEvImportMapRecord;
    if MapRecord.MapType = AMapRecordType then
    begin
      Result := MapRecord;
      exit;
    end;
  end;
end;

function TEvExchangePackageField.IsKeyField: boolean;
begin
  Result := FisKeyField;
end;

procedure TEvExchangePackageField.SetAsKeyField(const Value: boolean);
begin
  FisKeyField := Value;
end;

function TEvExchangePackageField.GetIgnoreRequiredByDD: boolean;
begin
  Result := FignoreRequiredByDD;
end;

procedure TEvExchangePackageField.SetIgnoreRequiredByDD(
  const Value: boolean);
begin
  FignoreRequiredByDD := Value;
end;

function TEvExchangePackageField.GetCheckRequiredByDD: boolean;
begin
  Result := FCheckRequiredByDD;
end;

procedure TEvExchangePackageField.SetCheckRequiredByDD(
  const Value: boolean);
begin
  FCheckRequiredByDD := Value;
end;

function TEvExchangePackageField.GetUseEvoNamesForMap: boolean;
begin
  Result := FuseEvoNamesForMap;
end;

procedure TEvExchangePackageField.SetUseEvoNamesForMap(
  const Value: boolean);
begin
  FuseEvoNamesForMap := Value;
end;

function TEvExchangePackageField.GetTrimInputSpaces: boolean;
begin
  Result := FTrimInputSpaces;
end;

procedure TEvExchangePackageField.SetTrimInputSpaces(const Value: boolean);
begin
  FTrimInputSpaces := Value;
end;

function TEvExchangePackageField.GetDisplayName: String;
begin
  Result := FDisplayName;
end;

procedure TEvExchangePackageField.SetDisplayName(const Value: String);
begin
  FDisplayName := Value;
end;

function TEvExchangePackageField.GetAlwaysRequired: boolean;
begin
  Result := FAlwaysRequired;
end;

procedure TEvExchangePackageField.SetAlwaysRequired(const Value: boolean);
begin
  FAlwaysRequired := Value;
end;

function TEvExchangePackageField.GetGroupName: String;
begin
  Result := FGroupName;
end;

procedure TEvExchangePackageField.SetGroupName(const Value: String);
begin
  FGroupName := Value;
end;

function TEvExchangePackageField.IsBelongsToGroup: boolean;
begin
  Result := (FGroupName <> '');
end;

function TEvExchangePackageField.IsGroupDefinitionField: boolean;
begin
  Result := (FGroupName <> '') and (FRecordNumber = 0);
end;

function TEvExchangePackageField.GetRecordNumber: integer;
begin
  Result := FRecordNumber;
end;

procedure TEvExchangePackageField.SetRecordNumber(const Value: integer);
begin
  CheckCondition(Value > 0, 'Record numbers should start from 1 but it is ' + IntToStr(Value) + '. ExchangeField ' + FExchangeFieldName);
  CheckCondition(FRecordNumber = 0, 'You cannot change record number. ExchangeField ' + FExchangeFieldName);
  FRecordNumber := Value;
  FExchangeFieldName := FExchangeFieldName + ' ' + RecordNumberToStr(FRecordNumber);
  FDisplayName := FDisplayName + ' ' + RecordNumberToStr(FRecordNumber);
end;

function TEvExchangePackageField.GetDefExchangeFieldName: String;
begin
  Result := FDefExchangeFieldName;
end;

procedure TEvExchangePackageField.SetDefExchangeFieldName(const Value: String);
begin
  CheckCondition(IsBelongsToGroup, 'Field should belons to any group');
  CheckCondition(not IsGroupDefinitionField, 'Operation not allowed for group definition fields');
  FDefExchangeFieldName := Value;
end;

function TEvExchangePackageField.GetPreFormatFunctionName: String;
begin
  Result := FPreFormatFunctionName;
end;

procedure TEvExchangePackageField.SetPreFormatFunctionName(const Value: String);
begin
  FPreFormatFunctionName := Value;
end;

function TEvExchangePackageField.GetAsOfDateEnabled: boolean;
begin
  Result := FAsOfDateEnabled;
end;

procedure TEvExchangePackageField.SetAsOfDateEnabled(const Value: boolean);
begin
  FAsOfDateEnabled := Value;
end;

function TEvExchangePackageField.GetRequiredIfAnyMappedForTable: boolean;
begin
  Result := FRequiredIfAnyMappedForTable;
end;

procedure TEvExchangePackageField.SetRequiredIfAnyMappedForTable(const Value: boolean);
begin
  FRequiredIfAnyMappedForTable := Value;
end;

function TEvExchangePackageField.GetRequiredIfAnyMappedForOtherTable: String;
begin
  Result := FRequiredIfAnyMappedForOtherTable;
end;

procedure TEvExchangePackageField.SetRequiredIfAnyMappedForOtherTable(const ATableName: String);
begin
  FRequiredIfAnyMappedForOtherTable := UpperCase(Trim(ATableName));
end;

function TEvExchangePackageField.GetDelayedPostTableNumber: integer;
begin
  Result := FDelayedPostTableNumber;
end;

procedure TEvExchangePackageField.SetDelayedPostTableNumber(const AValue: integer);
begin
  FDelayedPostTableNumber := AValue;
end;

function TEvExchangePackageField.GetNotPostThisFieldFlag: boolean;
begin
  Result := FNotPostThisFieldFlag;
end;

procedure TEvExchangePackageField.SetNotPostThisFieldFlag(const AValue: boolean);
begin
  FNotPostThisFieldFlag := AValue;
end;

function TEvExchangePackageField.GetLookupFieldName: String;
begin
  Result := FLookupFieldName;
end;

function TEvExchangePackageField.GetLookupTableName: String;
begin
  Result := FLookupTableName;
end;

procedure TEvExchangePackageField.SetLookupData(const ATableName, AFieldName: String; AFieldSize: integer = 0);
begin
  CheckCondition(Trim(ATableName) <> '', 'Lookup Table name cannot be empty');
  CheckCondition(Trim(AFieldName) <> '', 'Lookup Field name cannot be empty');
  FLookupTableName := ATableName;
  FLookupFieldName := AFieldName;
  FLookupFieldWidth := AFieldSize;
  LoadLookupDDField;

  if FLookupDDField.FieldType = ddtString then
    CheckCondition(FLookupFieldWidth > 0, 'You should provide width for string fields')
  else
    CheckCondition(FLookupFieldWidth = 0, 'You cannot provide width for not string fields');
end;

procedure TEvExchangePackageField.LoadLookupDDField;
begin
  if FLookupTableName <> '' then
  begin
    try
      FLookupDDField := TddsField(EvoDataDictionary.Tables.TableByName(FLookupTableName).Fields.FieldByName(FLookupFieldName));
    except
      on E: Exception do
        raise Exception.Create('Cannot find information in data dictionary. Table: ' + FLookupTableName +
          '. Field: ' + FLookupFieldName + '. Exception: ' + E.Message);
    end;
    CheckCondition(Assigned(FLookupDDField), 'Cannot find information in data dictionary. Table: ' + FLookupTableName +
      '. Field: ' + FLookupFieldName);
  end;
end;

function TEvExchangePackageField.Revision: TisRevision;
begin
  Result := 1;
end;

function TEvExchangePackageField.GetSectionRecords: IisStringListRO;
begin
  Result := FSectionRecords as IisStringListRO;
end;

procedure TEvExchangePackageField.SetSectionRecords(
  ASectionRecords: IisStringListRO);
begin
  FSectionRecords.Text := ASectionRecords.Text;
end;

function TEvExchangePackageField.IsBelongsToSection(
  const aSectionName: string): boolean;
begin
  Result := (GetSectionRecords.IndexOf(aSectionName) >= 0) or
    (aSectionName = '<ANY>') and (GetSectionRecords.Count > 0);
end;

{ TEvExchangePackageGroup }

constructor TEvExchangePackageGroup.Create(const AGroupName, ADisplayGroupName: String);
begin
  CheckCondition(Trim(AGroupName) <> '', 'Group name cannot be empty');
  FName := AGroupName;
  FDisplayName := ADisplayGroupName;
  if trim(FDisplayName) = '' then
    FDisplayName := FName + ' (Group)';
end;

destructor TEvExchangePackageGroup.Destroy;
begin
  inherited;
end;

procedure TEvExchangePackageGroup.DoOnConstruction;
begin
  inherited;

end;

procedure TEvExchangePackageGroup.DoReconcile;
begin

end;

function TEvExchangePackageGroup.GetDisplayName: String;
begin
  Result := FDisplayName;
end;

function TEvExchangePackageGroup.GetName: String;
begin
  Result := FName;
end;

class function TEvExchangePackageGroup.GetTypeID: String;
begin
  Result := 'EvExchangePackageGroup';
end;

function TEvExchangePackageGroup.GetXML: String;
begin
  Result := StartTag(EvoXGroupTag);
  Result := Result + InsertTag(EvoXgroupNameTag, FName);
  Result := Result + InsertTag(EvoXgroupDisplayNameTag, FDisplayName);
  Result := Result + EndTag(EvoXGroupTag);
end;

procedure TEvExchangePackageGroup.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FName := AStream.ReadString;
  FDisplayName := AStream.ReadString;
end;

procedure TEvExchangePackageGroup.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FName);
  AStream.WriteString(FDisplayName);
end;

procedure TEvExchangePackage.DeleteAllGroupRecords;
var
  i: integer;
  PackageField: IEvExchangePackageField;
begin
  for i := FFieldsList.Count - 1 downto 0 do
  begin
    PackageField := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if PackageField.IsBelongsToGroup and (not PackageField.IsGroupDefinitionField) then
      FFieldsList.DeleteValue(PackageField.GetExchangeFieldName);
  end;
end;

procedure TEvExchangePackage.AddRecordWithNumberToGroup(const AGroupName: String; const ARecordNumber: integer);
var
  i, j: integer;
  PackageField, ClonedField: IEvExchangePackageField;
  GroupFieldsList, ClonedFieldsList, NewFieldsList: IisListOfValues;
  KeyValues: IisStringList;
  GroupFieldsNames: IisStringList;
  MaxIndex: integer;
  tmpNamedValue: IisNamedValue;
begin
  GroupFieldsList := GetFieldsDefsByGroupName(AGroupName);
  CheckCondition(GroupFieldsList.Count > 0, 'Group "' + AGroupName + '" is empty. Cannot add record');

  CheckCondition(not isGroupRecordExist(AGroupName, ArecordNumber), 'Record #' + IntToStr(ARecordNumber) +
    ' for group "' + AgroupName + '" already exists');
  CheckCondition((ARecordNumber > 0) and (ARecordNumber < 100), 'Record number is out of range (1-99): ' + IntToStr(ARecordNumber));

  // filling list of fields names in group
  GroupFieldsNames := TisStringList.Create;
  for i := 0 to GroupFieldsList.Count - 1 do
  begin
    PackageField := IInterface(GroupFieldsList.Values[i].Value) as IEvExchangePackageField;
    GroupFieldsNames.Add(PackageField.GetExchangeFieldName);
  end;

  // adding record
  ClonedFieldsList := TisListOfValues.Create;
  for i := 0 to GroupFieldsList.Count - 1 do
  begin
    PackageField := IInterface(GroupFieldsList.Values[i].Value) as IEvExchangePackageField;
    ClonedField := (PackageField.GetClone) as IEvExchangePackageField;
    ClonedField.SetRecordNumber(ARecordNumber);
    ClonedField.SetDefExchangeFieldName(PackageField.GetExchangeFieldName);

    // updating KeyValues
    KeyValues := ClonedField.GetKeyValues as IisStringList;
    for j := 0 to KeyValues.Count - 1 do
      if GroupFieldsNames.IndexOf(KeyValues[j]) <> -1 then
      begin
        KeyValues[j] := KeyValues[j] + ' ' + RecordNumberToStr(ARecordNumber);
      end;

    // updating relatedParentField
    if GroupFieldsNames.IndexOf(ClonedField.GetRelatedParentField) <> -1 then
      ClonedField.SetRelatedparentField(ClonedField.GetRelatedParentField + ' ' + RecordNumberToStr(ARecordNumber));

    ClonedField.DoReconcile;
    ClonedFieldsList.AddValue(ClonedField.GetExchangeFieldName, ClonedField);
  end;

  // moving cloned fields to proper plase in list
  NewFieldsList := TisListOfValues.Create;
  MaxIndex := GetMaxFieldsDefsIndexByGroupName(AGroupName);
  for i := 0 to MaxIndex do
  begin
    tmpNamedValue := FFieldsList.Values[i];
    NewFieldsList.AddValue(tmpNamedValue.Name, IInterface(tmpNamedValue.Value) as IEvExchangePackageField);
  end;
  for i := 0 to ClonedFieldsList.Count - 1 do
  begin
    tmpNamedValue := ClonedFieldsList.Values[i];
    NewFieldsList.AddValue(tmpNamedValue.Name, IInterface(tmpNamedValue.Value) as IEvExchangePackageField);
  end;
  for i := MaxIndex + 1 to FFieldsList.Count - 1 do
  begin
    tmpNamedValue := FFieldsList.Values[i];
    NewFieldsList.AddValue(tmpNamedValue.Name, IInterface(tmpNamedValue.Value) as IEvExchangePackageField);
  end;
  FFieldsList := NewFieldsList;
end;

function TEvExchangePackage.GetMaxFieldsDefsIndexByGroupName(const AGroupName: String): integer;
var
  i: integer;
  Field: IEvExchangePackageField;
begin
  Result := -1;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if (Field.GetGroupName = AGroupName) and (Field.IsGroupDefinitionField) and (i > Result) then
      Result := i;
  end;
  CheckCondition(Result >= 0, 'Max index not found');
end;

function TEvExchangePackage.GetFieldsForEvOrVirtualTable(const AEvTable: String): IisListOfValues;
var
  i: integer;
  Field: IEvExchangePackageField;
  iDelayNumber: integer;
  bVirtualTable: boolean;
begin
  CheckCondition(Trim(AEvTable) <> '', 'Table name cannot be empty');
  Result := TisListOfValues.Create;

  iDelayNumber := -1;
  bVirtualTable := false;
  if Pos(EvoXVirtualTablePrefix, AEvtable) <> 0 then
  begin
    bVirtualTable := true;
    iDelayNumber := StrToint(Copy(AEvTable, Length(EvoXVirtualTablePrefix) + 1, MaxInt));
  end;

  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if Field.GetDelayedPostTableNumber < 0 then
    begin
      if Field.GetEvTableName = AEvTable then
        Result.AddValue(Field.GetExchangeFieldName, Field);
    end
    else
    begin
      if bVirtualTable and (Field.GetDelayedPostTableNumber = iDelayNumber) then
        Result.AddValue(Field.GetExchangeFieldName, Field);
    end;
  end;
end;

function TEvExchangePackage.GetListOfEvAndVirtualTables: IisStringList;
var
  i: integer;
  Field: IEvExchangePackageField;
  VirtualTables: IisStringList;
  S: String;
begin
  Result := TisStringList.Create;
  VirtualTables := TisStringList.Create;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    if Field.GetDelayedPostTableNumber < 0 then
    begin
      if Result.IndexOf(Field.GetEvTableName) = -1 then
        Result.Add(Field.GetEvTableName);
    end
    else
    begin
      // POST operation for this field should be postponed, so it belongs to virtual table, which should be at the end of list of tables
      S := EvoXVirtualTablePrefix + PadStringLeft(IntToStr(Field.GetDelayedPostTableNumber), '0', 3);
      if VirtualTables.IndexOf(S) = -1 then
        VirtualTables.Add(S);
    end;
  end;

  // sorting and adding virtual tables to the and of list;
  VirtualTables.Sort;
  for i := 0 to VirtualTables.Count - 1 do
    Result.Add(VirtualTables[i]);
end;

{ TEvExchangePackageSection }

constructor TEvExchangePackageSection.Create(const ASectionName, ADisplaySectionName: String);
begin
  CheckCondition(Trim(ASectionName) <> '', 'Section name cannot be empty');
  FName := ASectionName;
  FDisplayName := ADisplaySectionName;
  if trim(FDisplayName) = '' then
    FDisplayName := FName + ' (Section)';
end;

destructor TEvExchangePackageSection.Destroy;
begin
  inherited;
end;

procedure TEvExchangePackageSection.DoOnConstruction;
begin
  inherited;

end;

procedure TEvExchangePackageSection.DoReconcile;
begin

end;

function TEvExchangePackageSection.GetDisplayName: String;
begin
  Result := FDisplayName;
end;

function TEvExchangePackageSection.GetName: String;
begin
  Result := FName;
end;

class function TEvExchangePackageSection.GetTypeID: String;
begin
  Result := 'EvExchangePackageSection';
end;

function TEvExchangePackageSection.GetXML: String;
begin
  Result := StartTag(EvoXSectionTag);
  Result := Result + InsertTag(EvoXsectionNameTag, FName);
  Result := Result + InsertTag(EvoXsectionDisplayNameTag, FDisplayName);
  Result := Result + EndTag(EvoXSectionTag);
end;

procedure TEvExchangePackageSection.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FName := AStream.ReadString;
  FDisplayName := AStream.ReadString;
end;

procedure TEvExchangePackageSection.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FName);
  AStream.WriteString(FDisplayName);
end;

function TEvExchangePackage.GetSectionByName(
  const ASectionName: String): IEvExchangePackageSection;
begin
  Result := IInterface(FSectionList.Value[ASectionName]) as IEvExchangePackageSection;
end;

function TEvExchangePackage.GetFieldsDefsBySectionName(
  const ASectionName: String): IisListOfValues;
var
  i, j: integer;
  Field: IEvExchangePackageField;
begin
  Result := TisListOfValues.Create;
  for i := 0 to FFieldsList.Count - 1 do
  begin
    Field := IInterface(FFieldsList.Values[i].Value) as IEvExchangePackageField;
    for j := 0 to Field.GetSectionRecords.Count - 1 do
      if (Field.GetSectionRecords[j] = ASectionName) then
        Result.AddValue(Field.GetExchangeFieldName, Field);
  end;
end;

initialization
  ObjectFactory.Register([TEvExchangePackageField, TEvExchangePackage, TEvExchangePackageGroup, TEvExchangePackageSection]);

finalization
  SafeObjectFactoryUnRegister([TEvExchangePackageField, TEvExchangePackage, TEvExchangePackageGroup, TEvExchangePackageSection]);
end.
