unit EvExchangeUtils;

interface

uses Classes, Windows, SysUtils, rwEngineTypes, rwCustomDataDictionary, Variants,
  isBasicUtils, EvCommonInterfaces, IsTypes;

  function StartTag(const ATagName : String): String;
  function EndTag(const ATagName : String): String;
  function InsertTag(const ATagName : String; const AText : String) : String;

  function IfVariantIs(const AValue: Variant; const ADataType: TDataDictDataType;
    const ADateFormat: String = 'mm/dd/yyyy'; const ATimeFormat: String = 'hh:nn:ss'): String;

  function ConvertVariantToType(const AValue: Variant; const ADataType: TDataDictDataType;
    const ADateFormat: String = 'mm/dd/yyyy'; const ATimeFormat: String = 'hh:nn:ss'): Variant;

  function IndexInArray(const AArray: TisDynIntegerArray; const AIndex: integer): integer;

  function RecordNumberToStr(const ARecordNumber: integer): String;

  function AddCDataTag(const AData: String) : String;

implementation

uses isBaseClasses, EvExchangeConsts;

function AddCDataTag(const AData: String): String;
begin
  Result := EvoXCDataStart + AData + EvoXCDataEnd;
end;

function InsertTag(const ATagName: String; const AText: String): String;
begin
  result := StartTag(ATagName) + AText + EndTag(ATagName);
end;

function StartTag(const ATagName: String): String;
begin
  result := '<' + ATagName + '>';
end;

function EndTag(const ATagName: String): String;
begin
  result := '</' + ATagName + '>';
end;

function IfVariantIs(const AValue: Variant; const ADataType: TDataDictDataType;
  const ADateFormat: String = 'mm/dd/yyyy'; const ATimeFormat: String = 'hh:nn:ss'): String;
var
  formatSettings: TFormatSettings;
begin
  Result := '';
  if AValue = null then
    Result := 'Value is null'
  else
    case ADataType of
      ddtInteger,
      ddtFloat,
      ddtCurrency:  begin
                      try
                        StrToFloat(Trim(VarToStr(AValue)));
                        exit;
                      except
                        on E:Exception do
                          Result := E.Message;
                      end;
                    end;
      ddtDateTime:  begin
                      FillChar(formatSettings, SizeOf(formatSettings), 0);
                      GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
                      formatSettings.ShortDateFormat := ADateFormat;
                      formatSettings.ShortTimeFormat := ATimeFormat;
                      try
                        StrToDateTime(AValue, formatSettings);
                      except
                        on E:Exception do
                          Result := E.Message;
                      end;
                    end;
      ddtString, ddtBLOB:    begin
                      if VarisStr(AValue) then
                        exit;
                    end;
      ddtBoolean:   begin
                      if VarIsType(AValue, VarBoolean) then
                        exit;
                    end;
    else
      Result := 'Unsupported data type';
    end;
end;

function ConvertVariantToType(const AValue: Variant; const ADataType: TDataDictDataType;
  const ADateFormat: String = 'mm/dd/yyyy'; const ATimeFormat: String = 'hh:nn:ss'): Variant;

var
  formatSettings: TFormatSettings;
  bValue: boolean;
  i: integer;

  function ConvertSpecialDateTimeFormats(const AValue: Variant; const ADateFormat, ATimeFormat: String; AFormatSettings: TFormatSettings): TDateTime;
  const
    MaskYYYYMMDD = 'yyyymmdd';
    MaskYYYY_MM_DD = 'yyyy/mm/dd';
    MaskDD_MM_YYYY = 'dd/mm/yyyy';
    MaskMM_DD_YYYY = 'mm/dd/yyyy';
    MaskNoTimeDelimiter = 'hhnnss';
    MaskTimeDoubleColon = 'hh:nn:ss';
    MaskTimeBackSlash = '/hh/nn/ss';
  type
    TAllowedChars = set of char;
  var
    DatePartOfValue, TimePartOfValue: String;
    S: String;

    procedure CheckStringForAllowedChars(const AString: String; const AAllowedChars: TAllowedChars; const ErrorMessage: String);
    var
      i: integer;
    begin
      for i := 1 to Length(AString) do
        CheckCondition(AString[i] in AAllowedChars, ErrorMessage + ' Not allowed character: "' + AString[i] + '" in input');
    end;

    procedure DivideStringOntoDateAndTime(const AValue: String; const ADateFormat: String; const ATimeFormat: String; out ADate: String; out ATime: String);
    var
      S: String;
      DatePart1, DatePart2, DatePart3: String;
      iSpacePos: integer;
    begin
      S := Trim(AValue);
      S := StringReplace(S, '\', '/', [rfReplaceAll]);
      S := StringReplace(S, '.', '/', [rfReplaceAll]);
      S := StringReplace(S, '-', '/', [rfReplaceAll]);
      iSpacePos := Pos(S, ' ');
      if iSpacePos > 0 then
      begin
        // easy - space delimits date and time
        ADate := Trim(Copy(S, 1, iSpacePos - 1));
        ATime := Trim(Copy(S, iSpacePos, MaxInt));
      end
      else
      begin
        // not easy - depends on format of date
        DatePart1 := GetNextStrValue(S, '/');
        DatePart2 := GetNextStrValue(S, '/');
        DatePart3 := S;
        if ADateFormat = MaskYYYY_MM_DD then
        begin
          if ATimeFormat = MaskNoTimeDelimiter then
          begin
            if Length(S) = 7 then
            begin
              ADate := DatePart1 + '/' + DatePart2 + '/' + Copy(S, 1, 1);
              ATime := Trim(Copy(S, 2, Maxint));
            end
            else
            begin
              ADate := DatePart1 + '/' + DatePart2 + '/' + Copy(S, 1, 2);
              ATime := Trim(Copy(S, 3, Maxint));
            end;
          end
          else if ATimeFormat = MaskTimeBackSlash then
          begin
            DatePart3 := Trim(GetNextStrValue(S, '/'));
            ADate := DatePart1 + '/' + DatePart2 + '/' + DatePart3;
            ATime := '/' + S;
          end
          else
          begin
            ADate := DatePart1 + '/' + DatePart2 + '/' + Copy(S, 1, 2);
            ATime := Trim(Copy(S, 3, Maxint));
          end;
        end
        else if ADateFormat = MaskDD_MM_YYYY then
        begin
          ADate := DatePart1 + '/' + DatePart2 + '/' + Copy(S, 1, 4); // easy one, year is always 4 digits
          ATime := Trim(Copy(S, 5, MaxInt));
        end
        else if ADateFormat = MaskMM_DD_YYYY then
        begin
          ADate := DatePart1 + '/' + DatePart2 + '/' + Copy(S, 1, 4);
          ATime := Trim(Copy(S, 5, MaxInt));
        end
        else if ADateFormat = MaskYYYYMMDD then
        begin
          S := Trim(AValue);
          ADate := Copy(S, 1, 8);
          ATime := Trim(Copy(S, 9, MaxInt));
        end
      end;
    end;

    function ParseDelimitedDateString(const ADateString: String; const ADateFormat: String): TDateTime;
    var
      S: String;
      Part1, Part2, Part3: String;
    begin
      S := Trim(ADateString);
      S := StringReplace(ADateString, '\', '/', [rfReplaceAll]);
      S := StringReplace(ADateString, '.', '/', [rfReplaceAll]);
      S := StringReplace(ADateString, '-', '/', [rfReplaceAll]);
      Part1 := GetNextStrValue(S, '/');
      Part2 := GetNextStrValue(S, '/');
      Part3 := S;
      if ADateFormat = MaskYYYY_MM_DD then
        Result := EncodeDate(StrtoInt(Part1), StrToInt(Part2), StrToInt(Part3))
      else if ADateFormat = MaskDD_MM_YYYY then
        Result := EncodeDate(StrtoInt(Part3), StrToInt(Part2), StrToInt(Part1))
      else if ADateFormat = MaskMM_DD_YYYY then
        Result := EncodeDate(StrtoInt(Part3), StrToInt(Part1), StrToInt(Part2))
      else if ADateFormat = MaskYYYYMMDD then
        Result := EncodeDate(StrToInt(Copy(ADateString, 1, 4)), StrToInt(Copy(ADateString, 5, 2)), StrToInt(Copy(ADateString, 7, 2)))
      else
        raise Exception.Create('Not supported format');
    end;
  begin
    Result := 0;
    DivideStringOntoDateAndTime(AValue, ADateFormat, ATimeFormat, DatePartOfValue, TimePartOfValue);

    if ATimeFormat = '' then
    begin
      if ADateFormat = MaskYYYYMMDD then
      begin
        DatePartOfValue := Copy(DatePartOfValue, 1, 4) + '/' + Copy(DatePartOfValue, 5, 2) + '/' + Copy(DatePartOfValue, 7, 2);
        Result := ParseDelimitedDateString(DatePartOfValue, 'yyyy/mm/dd');
      end
      else if ADateFormat = MaskDD_MM_YYYY then
        Result := ParseDelimitedDateString(DatePartOfValue, ADateFormat)
      else if ADateFormat = MaskYYYY_MM_DD then
        Result := ParseDelimitedDateString(DatePartOfValue, ADateFormat)
      else if ADateFormat = MaskMM_DD_YYYY then
        Result := ParseDelimitedDateString(DatePartOfValue, ADateFormat);
      exit;
    end
    else
    begin
      AFormatSettings.ShortDateFormat := 'yyyy/mm/dd';
      if ADateFormat = MaskYYYYMMDD then
        DatePartOfValue := Copy(DatePartOfValue, 1, 4) + '/' + Copy(DatePartOfValue, 5, 2) + '/' + Copy(DatePartOfValue, 7, 2)
      else if ADateFormat = MaskDD_MM_YYYY then
        AFormatSettings.ShortDateFormat := MaskDD_MM_YYYY
      else if ADateFormat = MaskYYYY_MM_DD then
      else if ADateFormat = MaskMM_DD_YYYY then
        AFormatSettings.ShortDateFormat := MaskMM_DD_YYYY
      else
        Exit;

      CheckStringForAllowedChars(DatePartOfValue, ['0'..'9', '/', '\'], 'Wrong format of input for date.');

      AFormatSettings.ShortTimeFormat := 'hh:nn:ss';
      if Length(TimePartOfValue) > 0 then
      begin
        if ATimeFormat = MaskNoTimeDelimiter then
        begin
          CheckStringForAllowedChars(TimePartOfValue, ['0'..'9'], 'Wrong format of input for time.');
          TimePartOfValue := Copy(TimePartOfValue, 1, 2) + ':' + Copy(TimePartOfValue, 3, 2) + ':' + Copy(TimePartOfValue, 5, 2);
        end
        else if ATimeFormat = MaskTimeDoubleColon then
        begin
          CheckStringForAllowedChars(TimePartOfValue, ['0'..'9', ':'], 'Wrong format of input for time.');
        end
        else if ATimeFormat = MaskTimeBackSlash then
        begin
          CheckStringForAllowedChars(TimePartOfValue, ['0'..'9', '/', '\'], 'Wrong format of input for time.');
          TimePartOfValue := StringReplace(TimePartOfValue, '\', '/', [rfReplaceAll]);
          S := TimePartOfValue;
          GetNextStrValue(S, '/');
          TimePartOfValue := GetNextStrValue(S, '/') + ':' + GetNextStrValue(S, '/') + ':' + S;
        end
        else
          Exit;
      end;

      if Trim(TimePartOfValue) <> '' then
        Result := StrToDateTime(DatePartOfValue + ' ' + TimePartOfValue, AFormatSettings)
      else
        Result := StrToDate(DatePartOfValue, AFormatSettings);
    end;
  end;

begin
  Result := null;
  if AValue <> null then
    case ADataType of
      ddtInteger:   begin
                      i := StrToInt(Trim(VarToStr(AValue)));
                      Result := i;
                    end;
      ddtFloat,
      ddtCurrency:  begin
                     StrToFloat(Trim(VarToStr(AValue)));
                     Result := AValue;
                    end;
      ddtDateTime:  begin
                      try
                        FillChar(formatSettings, SizeOf(formatSettings), 0);
                        GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
                        Result := ConvertSpecialDateTimeFormats(AValue, ADateFormat, ATimeFormat, formatSettings);
                      except
                        on EE: Exception do
                          raise Exception.Create(EE.Message + '. Format mask "' + formatSettings.ShortDateFormat + ' ' + formatSettings.ShortTimeFormat + '"');
                      end;

                      if Result = 0 then
                      begin
                        try
                          FillChar(formatSettings, SizeOf(formatSettings), 0);
                          GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
                          if ADateFormat <> '' then
                            formatSettings.ShortDateFormat := ADateFormat;
                          formatSettings.ShortTimeFormat := ATimeFormat;
                          Result := StrToDateTime(AValue, formatSettings);
                        except
                          on EE: Exception do
                            raise Exception.Create(EE.Message + '. Format mask "' + formatSettings.ShortDateFormat + ' ' + formatSettings.ShortTimeFormat + '"');
                        end;
                      end;
                    end;
      ddtString, ddtBLOB:    begin
                      CheckCondition(VarisStr(AValue), 'Value is not string');
                      Result := AValue;
                    end;
      ddtBoolean:   begin
                      CheckCondition(VarIsType(AValue, VarBoolean), 'Value is not boolean');
                      bValue := AValue;
                      Result := bValue;
                    end;
    else
      Assert(false, 'Unsupported data type');
    end;
end;

function IndexInArray(const AArray: TisDynIntegerArray; const AIndex: integer): integer;
var
  i: integer;
begin
  Result := -1;
  for i := Low(AArray) to High(AArray) do
    if AArray[i] = AIndex then
    begin
      Result := i;
      exit;
    end;
end;

function RecordNumberToStr(const ARecordNumber: integer): String;
var
  S: String;
begin
  S := IntToStr(ARecordNumber);
  Result := '#' + StringOfChar('0', 2 - Length(S)) + S;
end;

end.
