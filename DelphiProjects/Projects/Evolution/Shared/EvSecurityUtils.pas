// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvSecurityUtils;

interface

uses
  Classes, SSecurityInterface, EvSecElement, EvConsts;

type
  TMenuSecAtom = class(TComponent, ISecurityAtom)
  private
    FName: string;
    function Component: TComponent;
    function IsAtom: Boolean;
    function SGetType: ShortString;
    function SGetTag: ShortString;
    function SGetCaption: ShortString;
    function SGetPossibleSecurityContexts: TAtomContextRecordArray;
    constructor CreateNamed(AOwner: TComponent; AName: string);
  end;

function FindMenuSecItem(sName: string; AOwner: TComponent): TMenuSecAtom;

implementation

{ TMenuSecAtom }

function FindMenuSecItem(sName: string; AOwner: TComponent): TMenuSecAtom;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Pred(AOwner.ComponentCount) do
    if (AOwner.Components[i] is TMenuSecAtom) and
       (TMenuSecAtom(AOwner.Components[i]).SGetTag = sName) then
    begin
      Result := TMenuSecAtom(AOwner.Components[i]);
      Break;
    end;
  if not Assigned(Result) then
  begin
    Result := TMenuSecAtom.CreateNamed(AOwner, sName);
    with TevSecElement.Create(Result) do
    begin
      AtomComponent := Result;
      ElementTag := sName;
      ElementType := otMenu;
    end;
  end;
end;

function TMenuSecAtom.Component: TComponent;
begin
  Result := Self;
end;

constructor TMenuSecAtom.CreateNamed(AOwner: TComponent; AName: string);
begin
  inherited Create(AOwner);
  FName := AName;
end;

function TMenuSecAtom.IsAtom: Boolean;
begin
  Result := True;
end;

function TMenuSecAtom.SGetCaption: ShortString;
begin
  Result := FName;
end;

function TMenuSecAtom.SGetPossibleSecurityContexts: TAtomContextRecordArray;
begin
  SetLength(Result, 0);
  AddContext(secMenuOff, Result);
  AddContext(secMenuOn, Result);
end;

function TMenuSecAtom.SGetTag: ShortString;
begin
  Result := FName;
end;

function TMenuSecAtom.SGetType: ShortString;
begin
  Result := atMenu;
end;

end.
