// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DBDTP_Filter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  DB,  Wwdatsrc, Grids, EvDataSet, EvCommonInterfaces,
  EvContext, Wwdbigrd, Wwdbgrid, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, ISBasicClasses, EvUIUtils, EvUIComponents,
  EvClientDataSet, LMDCustomButton, LMDButton, isUILMDButton, isUIEdit,
  ExtCtrls, isUIFashionPanel;

type
  TDBDTP_Filter = class(TForm)
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    evClientDataSet1: TevClientDataSet;
    bSelect: TevButton;
    isUIFashionPanel1: TisUIFashionPanel;
    evLabel1: TevLabel;
    edInput: TevEdit;
    bSearch: TevButton;
    fpSearchResults: TisUIFashionPanel;
    procedure bSearchClick(Sender: TObject);
    procedure bSelectClick(Sender: TObject);
    procedure edInputChange(Sender: TObject);
    procedure edInputKeyPress(Sender: TObject; var Key: Char);
  private
    FCaption: string;
    FCondition: string;
    { Private declarations }
  public
    { Public declarations }
    ClNbr, CoNbr: Integer;
    property Condition: string read FCondition;
    property Caption: string read FCaption;
  end;

function AskEeFilter(const ClNbr, CoNbr: Integer; out Condition, Caption: string): Boolean;

implementation

{$R *.dfm}

uses EvUtils;

procedure TDBDTP_Filter.bSearchClick(Sender: TObject);
var
  CurrClientNbr: Integer;
begin
  CurrClientNbr := ctx_DBAccess.CurrentClientNbr;
  try
    ctx_DBAccess.CurrentClientNbr := ClNbr;
    evClientDataSet1.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
    evClientDataSet1.Close;
    with TExecDSWrapper.Create('SelectDBDTG') do
    begin
      SetParam('StatusDate', ctx_DataAccess.AsOfDate);
      SetParam('CoNbr', CoNbr);
      SetParam('Input', '%'+ UpperCase(edInput.Text)+ '%');
      evClientDataSet1.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([evClientDataSet1]);
  finally
    ctx_DBAccess.CurrentClientNbr := CurrClientNbr;
  end;

  bSelect.Enabled := evDataSource1.DataSet.RecordCount > 0;
  bSearch.Enabled := False;
end;

procedure TDBDTP_Filter.bSelectClick(Sender: TObject);
begin
  FCondition := evDataSource1.DataSet['level_field']+ '='+ IntToStr(evDataSource1.DataSet['nbr']);
  FCaption := evDataSource1.DataSet['level_name']+ ' '+ evDataSource1.DataSet['name'];
  ModalResult := mrOk;
end;

procedure TDBDTP_Filter.edInputChange(Sender: TObject);
begin
  bSearch.Enabled := True;
end;

function AskEeFilter(const ClNbr, CoNbr: Integer; out Condition, Caption: string): Boolean;
var
  ds: TevClientDataSet;
  s: string;
begin
  Result := EvDialog('Pick filtering condition', 'Enter text to match with:', s);
  if Result then
  begin
    ds := TevClientDataSet.Create(nil);
    try
      ds.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
      ds.ClientID := ClNbr;
      ds.Close;
      with TExecDSWrapper.Create('SelectEe') do
      begin
        SetParam('StatusDate', ctx_DataAccess.AsOfDate);
        SetParam('CoNbr', CoNbr);
        SetParam('Input', '%'+ UpperCase(s)+ '%');
        ds.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([ds]);
      Result := False;
      if ds.RecordCount = 0 then
        EvMessage('No employees match this filter. Use different filter.')
      else if ds.RecordCount > 20 then
        EvMessage('More than 20 employees match this filter. Use different filter.')
      else
      begin
        Result := True;
        Condition := UplinkFieldCondition('EE_NBR', ds);
        Caption := IntToStr(ds.RecordCount)+ ' employees matching "'+ s+ '"';
      end;
    finally
      ds.Free;
    end;
  end;
end;

procedure TDBDTP_Filter.edInputKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    bSearch.Click;
end;

end.
