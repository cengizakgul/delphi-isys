// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STimeOffUtils;

interface

uses
   Db, SysUtils, SDataStructure, SDataDictClient, EvContext,
  isVCLBugFix, EvExceptions, EvClientDataSet;

type

  TAbstractToaSetupAccess = class
  private
    FCO_TIME_OFF_ACCRUAL: TEvClientDataSet;
    FCO_TIME_OFF_ACCRUAL_RATES: TEvClientDataSet;
    FCO_TIME_OFF_ACCRUAL_TIERS: TEvClientDataSet;
    FEE_TIME_OFF_ACCRUAL: TEvClientDataSet;
    FEE_TIME_OFF_ACCRUAL_OPER: TEvClientDataSet;
    FEE_HR_ATTENDANCE: TEvClientDataSet;
    FEE: TEvClientDataSet;
    FCurrentEE: TevClientDataSet;
    FStartingEeToaBalance: TevClientDataSet;
    FBetweenDateEeToaBalance: TevClientDataSet;
    FCoNbr: Integer;
    FCollapsedEeOpers: TevClientDataSet;
    FRoundingPrecision: Integer;
    FEeToaNbr: Integer;
    FPeriodEndDate: TDateTime;
    FChangedEeToa: TevClientDataSet;
    FCurrentEE_TIME_OFF_ACCRUAL: TevClientDataSet;
    FShadowCollapsedEeOpers: TevClientDataSet;
    FNewNbrCounter: Integer;
  public
    property CO_TIME_OFF_ACCRUAL: TEvClientDataSet read FCO_TIME_OFF_ACCRUAL;
    property CO_TIME_OFF_ACCRUAL_RATES: TEvClientDataSet read FCO_TIME_OFF_ACCRUAL_RATES;
    property CO_TIME_OFF_ACCRUAL_TIERS: TEvClientDataSet read FCO_TIME_OFF_ACCRUAL_TIERS;
    property EE_TIME_OFF_ACCRUAL: TEvClientDataSet read FEE_TIME_OFF_ACCRUAL;
    property EE_TIME_OFF_ACCRUAL_OPER: TEvClientDataSet read FEE_TIME_OFF_ACCRUAL_OPER;
    property EE_HR_ATTENDANCE: TEvClientDataSet read FEE_HR_ATTENDANCE;
    property EE: TEvClientDataSet read FEE;
    property CurrentEE: TevClientDataSet read FCurrentEE;
    property CurrentEE_TIME_OFF_ACCRUAL: TevClientDataSet read FCurrentEE_TIME_OFF_ACCRUAL;
    property StartingEeToaBalance: TevClientDataSet read FStartingEeToaBalance;
    property BetweenDateEeToaBalance: TevClientDataSet read FBetweenDateEeToaBalance;
    property CollapsedEeOpers: TevClientDataSet read FCollapsedEeOpers;
    property ShadowCollapsedEeOpers: TevClientDataSet read FShadowCollapsedEeOpers;
    property ChangedEeToa: TevClientDataSet read FChangedEeToa;
    property CoNbr: Integer read FCoNbr;
    property RoundingPrecision: Integer read FRoundingPrecision;
    property EeToaNbr: Integer read FEeToaNbr;
    property PeriodEndDate: TDateTime read FPeriodEndDate;
    procedure LocateEeToaSetup(const EeToaNbr: Integer; const PeriodEndDate: TDateTime); virtual;
    function HireDate: TDateTime;
    function PayrollCapAdjustment(const Accrued: Double): Double;
    function TotalCapAdjustment(const Accrued: Double): Variant;
    procedure CalcAnyDayEndBalance(const pd: TDateTime; out Accrued, Used: Double);
    function AddOper(const AdjToaOperNbr, ConToaOperNbr, PrNbr, PrBatchNbr, PrCheckNbr: Variant;
      const OperCode: Char; const Note: string; const AccrualDate: TDateTime;
      Accrued, Used: Double; AccruedCapped: Variant; const OverideEeToaNbr: Integer =-1): Integer; virtual;
    function AddEeToa(const EE_NBR, CO_TIME_OFF_ACCRUAL_NBR: Integer;
      const EFFECTIVE_ACCRUAL_DATE, NEXT_ACCRUE_DATE, ANNUAL_ACCRUAL_MAXIMUM, OVERRIDE_RATE: Variant): Integer; virtual;
    procedure ReadjustOperations(const PrNbr: Variant; const AccrualDate: TDateTime);
    procedure VoidPrTimeOff(const PrNbr: Integer; const AccrualDate: TDateTime; const VoidingPrNbr: Integer);
    procedure PostChanges;
    constructor Create(const CoNbr: Integer; const StartingDate: TDateTime); overload; virtual;
    destructor Destroy; override;
  end;

  THistoricToaSetupAccess = class(TAbstractToaSetupAccess)
  public
    constructor Create(const CoNbr: Integer; const StartingDate: TDateTime); overload; override;
    procedure LocateEeToaSetup(const EeToaNbr: Integer; const PeriodEndDate: TDateTime); override;
    function LocateEeProratorSetup(const EeToaNbr: Integer; const HoursWorked: Double; const PeriodEndDate: TDateTime): Boolean;
    function AddEeToa(const EE_NBR, CO_TIME_OFF_ACCRUAL_NBR: Integer;
      const EFFECTIVE_ACCRUAL_DATE, NEXT_ACCRUE_DATE, ANNUAL_ACCRUAL_MAXIMUM, OVERRIDE_RATE: Variant): Integer; override;
  end;
  
  TPayrollEntryCheckToaSetupAccess = class(THistoricToaSetupAccess)
  private
    FCheckDate: TDateTime;
    FShadowCurrentEeToa: TevClientDataSet;
  public
    function CheckBalance(const CheckRegardlessFlag: Boolean): Boolean;
    procedure Check;
    constructor Create(const CoNbr: Integer; const StartingDate, CheckDate: TDateTime); overload;
    destructor Destroy; override;
  end;

function DaySafeEncode(y, m, d: Word): TDateTime;
function SameDayAnotherYear(const OrigDate, YearDate: TDateTime): TDateTime;
function CountDownMonths(const DFrom, DTill: TDateTime): Integer;
function GenericIsDateInPeriod(const D: Variant; const DFrom, DTill: TDateTime): Boolean;
function AddMonths(const pd: TDateTime; const n: Integer): TDateTime;
procedure VoidPrTimeOff(const PrNbr: Integer; const AccrualDate: TDateTime; const VoidingPrNbr: Variant);

implementation

uses DateUtils, EvUtils, EvTypes, Math, Variants, EvConsts, EvBasicUtils, EvDataAccessComponents,
  Dialogs, Controls, EvUIUtils;

procedure VoidPrTimeOff(const PrNbr: Integer; const AccrualDate: TDateTime;
  const VoidingPrNbr: Variant);
var
  a: TAbstractToaSetupAccess;
  d, cdChangedToa: TEvClientDataSet;
begin
  cdChangedToa := TevClientDataSet.CreateWithChecksDisabled(nil);
  d := TevClientDataSet.CreateWithChecksDisabled(nil);
  try
    cdChangedToa.FieldDefs.Add('NBR', ftInteger);
    cdChangedToa.IndexFieldNames := 'NBR';
    cdChangedToa.CreateDataSet;
    cdChangedToa.LogChanges := False;
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('TABLENAME', 'PR');
      SetMacro('COLUMNS', 'CHECK_DATE, CO_NBR');
      SetMacro('NBRFIELD', 'PR');
      SetParam('RecordNbr', PrNbr);
      ctx_DataAccess.GetCustomData(d, AsVariant);
    end;
    Assert(d.RecordCount > 0, 'Incorrect payroll to void');
    if VarIsNull(VoidingPrNbr) then
    begin
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_NBR='+ IntToStr(PrNbr));
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filter :=
          ' OPERATION_CODE <> ''' + EE_TOA_OPER_CODE_REQUEST_PENDING +
          ''' and OPERATION_CODE <> ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
          ''' and OPERATION_CODE <> ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''' ';
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filtered := True;

      while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
      begin
        if not cdChangedToa.FindKey([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.FieldValues['EE_TIME_OFF_ACCRUAL_NBR']]) then
          cdChangedToa.AppendRecord([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.FieldValues['EE_TIME_OFF_ACCRUAL_NBR']]);
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Delete;
      end;
      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);
    end;

    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_NBR='+ IntToStr(PrNbr)+
        ' AND OPERATION_CODE = ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED + ''' ');
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
    while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
    begin
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);

    a := THistoricToaSetupAccess.Create(d['CO_NBR'], d['CHECK_DATE']);
    try
      if not VarIsNull(VoidingPrNbr) then
        a.VoidPrTimeOff(PrNbr, AccrualDate, VoidingPrNbr)
      else
        a.ChangedEeToa.Data := cdChangedToa.Data;
      a.ReadjustOperations(VoidingPrNbr, AccrualDate);
      a.PostChanges;
    finally
      a.Free;
    end;
  finally
    d.Free;
    cdChangedToa.Free;
  end;
end;

function DaySafeEncode(y, m, d: Word): TDateTime;
begin
  Result := 0;
  while True do
  try
    Result := EncodeDate(y, m, d);
    Break;
  except
    Dec(d);
    if d = 0 then
      raise;
  end;
end;

function SameDayAnotherYear(const OrigDate, YearDate: TDateTime): TDateTime;
var
  y, m, d: Word;
begin
  DecodeDate(OrigDate, y, m, d);
  Result := DaySafeEncode(YearOf(YearDate), m, d);
end;

function CountDownMonths(const DFrom, DTill: TDateTime): Integer;
var
  d1, m1, y1, d2, m2, y2: Word;
begin
  if DTill < DFrom then
    raise ESilentAccrualException.Create('Effective Date '+ FormatDateTime('MM/DD/YYYY', DFrom)+ ' is set after Period End Date '+ FormatDateTime('MM/DD/YYYY', DTill));
  DecodeDate(DFrom, y1, m1, d1);
  DecodeDate(DTill, y2, m2, d2);
  Result := (y2 - y1) * 12 + (m2 - m1);
  if d2 >= d1 then Inc(Result);
end;

function GenericIsDateInPeriod(const D: Variant; const DFrom, DTill: TDateTime): Boolean;
var
  t: TDateTime;
begin
  t := ConvertNull(D, 0);
  Result := (t >= DFrom) and (t <= DTill);
end;

function AddMonths(const pd: TDateTime; const n: Integer): TDateTime;
var
  y, m, d: Word;
begin
  DecodeDate(pd, y, m, d);
  Inc(m, n);
  while m > 12 do
  begin
    Inc(y);
    Dec(m, 12);
  end;
  Result := DaySafeEncode(y, m, d);
end;

{ TAbstractToaSetupAccess }

function TAbstractToaSetupAccess.AddEeToa(const EE_NBR,
  CO_TIME_OFF_ACCRUAL_NBR: Integer; const EFFECTIVE_ACCRUAL_DATE,
  NEXT_ACCRUE_DATE, ANNUAL_ACCRUAL_MAXIMUM, OVERRIDE_RATE: Variant): Integer;
var
  iKey, coTOANbr: Integer;
begin
  iKey := CurrentEE_TIME_OFF_ACCRUAL[CurrentEE_TIME_OFF_ACCRUAL.IndexFieldNames];
  CurrentEE_TIME_OFF_ACCRUAL.Append;
  CurrentEE_TIME_OFF_ACCRUAL['EE_NBR'] := EE_NBR;
  CurrentEE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'] := CO_TIME_OFF_ACCRUAL_NBR;
  CurrentEE_TIME_OFF_ACCRUAL['EFFECTIVE_ACCRUAL_DATE'] := EFFECTIVE_ACCRUAL_DATE;
  CurrentEE_TIME_OFF_ACCRUAL['NEXT_ACCRUE_DATE'] := NEXT_ACCRUE_DATE;
  CurrentEE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM'] := ANNUAL_ACCRUAL_MAXIMUM;
  CurrentEE_TIME_OFF_ACCRUAL['OVERRIDE_RATE'] := OVERRIDE_RATE;
  CurrentEE_TIME_OFF_ACCRUAL['JUST_RESET'] := GROUP_BOX_NO;
  CurrentEE_TIME_OFF_ACCRUAL['STATUS'] := GROUP_BOX_YES;
  CurrentEE_TIME_OFF_ACCRUAL['CURRENT_ACCRUED'] := 0;
  CurrentEE_TIME_OFF_ACCRUAL['CURRENT_USED'] := 0;
  CurrentEE_TIME_OFF_ACCRUAL['MARKED_ACCRUED'] := 0;
  CurrentEE_TIME_OFF_ACCRUAL['MARKED_USED'] := 0;
  CurrentEE_TIME_OFF_ACCRUAL['ROLLOVER_FREQ'] := TIME_OFF_ACCRUAL_FREQ_NONE;
  CurrentEE_TIME_OFF_ACCRUAL['ROLLOVER_PAYROLL_OF_MONTH'] := TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE;
  CurrentEE_TIME_OFF_ACCRUAL.Post;
  Result := CurrentEE_TIME_OFF_ACCRUAL['EE_TIME_OFF_ACCRUAL_NBR'];
  Assert(CurrentEE_TIME_OFF_ACCRUAL.FindKey([iKey]));
  iKey := StartingEeToaBalance[StartingEeToaBalance.IndexFieldNames];
  StartingEeToaBalance.Append;
  StartingEeToaBalance['EE_TIME_OFF_ACCRUAL_NBR'] := Result;
  StartingEeToaBalance['accrued'] := 0;
  StartingEeToaBalance['used'] := 0;
  StartingEeToaBalance.Post;
  Assert(StartingEeToaBalance.FindKey([iKey]));

  coTOANbr:= CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'];
  try
    if CO_TIME_OFF_ACCRUAL.Locate('CO_TIME_OFF_ACCRUAL_NBR', CO_TIME_OFF_ACCRUAL_NBR, []) then
    begin
      iKey := BetweenDateEeToaBalance[BetweenDateEeToaBalance.IndexFieldNames];
      BetweenDateEeToaBalance.Append;
      BetweenDateEeToaBalance['EE_TIME_OFF_ACCRUAL_NBR'] := Result;
      BetweenDateEeToaBalance['CO_TIME_OFF_ACCRUAL_NBR'] := CO_TIME_OFF_ACCRUAL_NBR;
      BetweenDateEeToaBalance['ANNUAL_ACCRUAL_MAXIMUM'] := CO_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM'];
      BetweenDateEeToaBalance['ANNUAL_USAGE_MAXIMUM'] := CO_TIME_OFF_ACCRUAL['ANNUAL_USAGE_MAXIMUM'];
      BetweenDateEeToaBalance['accrued'] := 0;
      BetweenDateEeToaBalance['used'] := 0;
      BetweenDateEeToaBalance.Post;
      Assert(BetweenDateEeToaBalance.FindKey([iKey]));
    end
    else
     raise EAccrualException.CreateFmtHelp('Can not find CO TOA record #%d', [CO_TIME_OFF_ACCRUAL_NBR], IDH_InconsistentData);
   finally
    CO_TIME_OFF_ACCRUAL.Locate('CO_TIME_OFF_ACCRUAL_NBR', coTOANbr, []);
   end;
end;

function TAbstractToaSetupAccess.AddOper(const AdjToaOperNbr, ConToaOperNbr,
  PrNbr, PrBatchNbr, PrCheckNbr: Variant; const OperCode: Char; const Note: string; const AccrualDate: TDateTime;
  Accrued, Used: Double; AccruedCapped: Variant; const OverideEeToaNbr: Integer =-1): Integer;

  function AnnualAccuralAdjustment(const eeTOANbr: integer; const Accrued: Double): Double;
  begin
    Result := Accrued;
    if BetweenDateEeToaBalance.Locate('ee_time_off_accrual_nbr',eeTOANbr,[]) and
      (not VarIsNull(BetweenDateEeToaBalance['ANNUAL_ACCRUAL_MAXIMUM']))  then
     if BetweenDateEeToaBalance['ANNUAL_ACCRUAL_MAXIMUM']<= (BetweenDateEeToaBalance.FieldByName('accrued').AsFloat + Accrued) then
        Result := BetweenDateEeToaBalance['ANNUAL_ACCRUAL_MAXIMUM']- BetweenDateEeToaBalance.FieldByName('accrued').AsFloat;
  end;

 var
  iEeToaNbr: Integer;
  sNote: string;
begin
  if (OperCode in [EE_TOA_OPER_CODE_RESET, EE_TOA_OPER_CODE_ROLLOVER_IN, EE_TOA_OPER_CODE_ROLLOVER])
  or (Accrued <> 0) or (Used <> 0) or not VarIsNull(AccruedCapped) then
  begin
    if (Accrued <> 0) then
      Accrued := AnnualAccuralAdjustment(EeToaNbr, Accrued);

    if OverideEeToaNbr <> -1 then
      iEeToaNbr := OverideEeToaNbr // no rounding can be made as we do not know override toa precision
    else
    begin
      iEeToaNbr := EeToaNbr;
      Accrued := RoundAll(Accrued, RoundingPrecision);
      Used := RoundAll(Used, RoundingPrecision);
      if not VarIsNull(AccruedCapped) then
        AccruedCapped := RoundAll(AccruedCapped, RoundingPrecision);
    end;
    if not VarIsNull(AdjToaOperNbr) then
    begin
      Assert(ShadowCollapsedEeOpers.FindKey([AdjToaOperNbr]));
      if (not VarIsNull(Accrued)) and  (Accrued<> 0) then
         Accrued := AnnualAccuralAdjustment(ShadowCollapsedEeOpers['EE_TIME_OFF_ACCRUAL_NBR'], Accrued );
      ShadowCollapsedEeOpers.Edit;
      ShadowCollapsedEeOpers['adj_accrued'] := ShadowCollapsedEeOpers['adj_accrued']+ Accrued;
      ShadowCollapsedEeOpers['adj_accrued_capped'] := ShadowCollapsedEeOpers['adj_accrued_capped']+
        ConvertNull(AccruedCapped, Accrued);
      ShadowCollapsedEeOpers['adj_used'] := ShadowCollapsedEeOpers['adj_used']+ Used;
      ShadowCollapsedEeOpers.Post;
      sNote := ' ('+ ShadowCollapsedEeOpers['NOTE']+ ')';
    end
    else
      sNote := '';
    if EE_TIME_OFF_ACCRUAL_OPER.FindKey([AdjToaOperNbr]) then
    begin
    if (not VarIsNull(AccruedCapped)) and  (AccruedCapped <> 0) then
      AccruedCapped := AnnualAccuralAdjustment(EE_TIME_OFF_ACCRUAL_OPER['EE_TIME_OFF_ACCRUAL_NBR'], AccruedCapped );

      EE_TIME_OFF_ACCRUAL_OPER.Edit;
      if not VarIsNull(AccruedCapped) then
        EE_TIME_OFF_ACCRUAL_OPER['ACCRUED_CAPPED'] :=
          ConvertNull(EE_TIME_OFF_ACCRUAL_OPER['ACCRUED_CAPPED'], EE_TIME_OFF_ACCRUAL_OPER['ACCRUED'])+
          AccruedCapped;
      EE_TIME_OFF_ACCRUAL_OPER['ACCRUED'] := EE_TIME_OFF_ACCRUAL_OPER['ACCRUED']+ Accrued;
      EE_TIME_OFF_ACCRUAL_OPER['USED'] := EE_TIME_OFF_ACCRUAL_OPER['USED']+ Used;
      EE_TIME_OFF_ACCRUAL_OPER.Post;
      iEeToaNbr := EE_TIME_OFF_ACCRUAL_OPER['EE_TIME_OFF_ACCRUAL_NBR'];
    end
    else
    begin
      EE_TIME_OFF_ACCRUAL_OPER.Append;
      EE_TIME_OFF_ACCRUAL_OPER['ACCRUAL_DATE'] := AccrualDate;
      EE_TIME_OFF_ACCRUAL_OPER['ACCRUED'] := Accrued;
      EE_TIME_OFF_ACCRUAL_OPER['ACCRUED_CAPPED'] := AccruedCapped;
      EE_TIME_OFF_ACCRUAL_OPER['USED'] := Used;
      EE_TIME_OFF_ACCRUAL_OPER['NOTE'] := ConvertCode(TEE_TIME_OFF_ACCRUAL_OPER, 'OPERATION_CODE', OperCode)+
        ' '+ Note+ sNote;
      EE_TIME_OFF_ACCRUAL_OPER['OPERATION_CODE'] := OperCode;
      EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := PrNbr;
      EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := PrBatchNbr;
      EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := PrCheckNbr;
      EE_TIME_OFF_ACCRUAL_OPER['EE_TIME_OFF_ACCRUAL_NBR'] := iEeToaNbr;
      EE_TIME_OFF_ACCRUAL_OPER['CONNECTED_EE_TOA_OPER_NBR'] := ConToaOperNbr;
      EE_TIME_OFF_ACCRUAL_OPER['ADJUSTED_EE_TOA_OPER_NBR'] := AdjToaOperNbr;
      EE_TIME_OFF_ACCRUAL_OPER.Post;
    end;
    if not ChangedEeToa.FindKey([iEeToaNbr]) then
      ChangedEeToa.AppendRecord([iEeToaNbr]);
    Result := EE_TIME_OFF_ACCRUAL_OPER['EE_TIME_OFF_ACCRUAL_OPER_NBR'];
    if VarIsNull(AdjToaOperNbr) then
    begin
      ShadowCollapsedEeOpers.Append;
      ShadowCollapsedEeOpers['accrued'] := Accrued;
      ShadowCollapsedEeOpers['adj_accrued'] := Accrued;
      ShadowCollapsedEeOpers['accrued_capped'] := AccruedCapped;
      ShadowCollapsedEeOpers['adj_accrued_capped'] := ConvertNull(AccruedCapped, Accrued);
      ShadowCollapsedEeOpers['adj_used'] := Used;
      ShadowCollapsedEeOpers['ee_time_off_accrual_nbr'] := iEeToaNbr;
      ShadowCollapsedEeOpers['ee_time_off_accrual_oper_nbr'] := EE_TIME_OFF_ACCRUAL_OPER['ee_time_off_accrual_oper_nbr'];
      ShadowCollapsedEeOpers['order_nbr'] := FNewNbrCounter;
      Inc(FNewNbrCounter);
      ShadowCollapsedEeOpers['PR_NBR'] := PrNbr;
      ShadowCollapsedEeOpers['PR_BATCH_NBR'] := PrBatchNbr;
      ShadowCollapsedEeOpers['PR_CHECK_NBR'] := PrCheckNbr;
      ShadowCollapsedEeOpers['CONNECTED_EE_TOA_OPER_NBR'] := ConToaOperNbr;
      ShadowCollapsedEeOpers['OPERATION_CODE'] := OperCode;
      ShadowCollapsedEeOpers['accrual_date'] := AccrualDate;
      ShadowCollapsedEeOpers['NOTE'] := EE_TIME_OFF_ACCRUAL_OPER['NOTE'];
      ShadowCollapsedEeOpers.Post;
    end;
  end
  else
    Result := -2;
end;

procedure TAbstractToaSetupAccess.CalcAnyDayEndBalance(const pd: TDateTime;
  out Accrued, Used: Double);
var
  fAccrued, fUsed: TField;
begin
  Accrued := StartingEeToaBalance['Accrued'];
  Used := StartingEeToaBalance['Used'];
  fAccrued := CollapsedEeOpers.FieldByName('adj_accrued_capped');
  fUsed := CollapsedEeOpers.FieldByName('adj_used');
  CollapsedEeOpers.IndexName := 'EeToaNbr';
  CollapsedEeOpers.SetRange([EeToaNbr], [EeToaNbr, pd]);
  CollapsedEeOpers.First;
  while not CollapsedEeOpers.Eof do
  begin
    Accrued := Accrued+ fAccrued.AsFloat;
    Used := Used+ fUsed.AsFloat;
    CollapsedEeOpers.Next;
  end;
end;

constructor TAbstractToaSetupAccess.Create(const CoNbr: Integer; const StartingDate: TDateTime);
begin
  FCoNbr := CoNbr;
  FNewNbrCounter := Low(FNewNbrCounter);
  FCO_TIME_OFF_ACCRUAL := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCO_TIME_OFF_ACCRUAL_RATES := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCO_TIME_OFF_ACCRUAL_TIERS := TevClientDataSet.CreateWithChecksDisabled(nil);
  FEE_TIME_OFF_ACCRUAL := TevClientDataSet.CreateWithChecksDisabled(nil);
  FEE_TIME_OFF_ACCRUAL_OPER := TevClientDataSet.CreateWithChecksDisabled(nil);
  FEE_HR_ATTENDANCE := TevClientDataSet.CreateWithChecksDisabled(nil);
  FEE := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCurrentEE := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCurrentEE_TIME_OFF_ACCRUAL := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCollapsedEeOpers := TevClientDataSet.CreateWithChecksDisabled(nil);
  FShadowCollapsedEeOpers := TevClientDataSet.CreateWithChecksDisabled(nil);
  FStartingEeToaBalance := TevClientDataSet.CreateWithChecksDisabled(nil);
  FBetweenDateEeToaBalance := TevClientDataSet.CreateWithChecksDisabled(nil);
  FChangedEeToa := TevClientDataSet.CreateWithChecksDisabled(nil);
  FChangedEeToa.FieldDefs.Add('NBR', ftInteger);
  FChangedEeToa.IndexFieldNames := 'NBR';
  FChangedEeToa.CreateDataSet;
  FChangedEeToa.LogChanges := False;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('TABLENAME', 'EE');
    SetMacro('COLUMNS', '*');
    SetMacro('CONDITION', 'CO_NBR = :CoNbr');
    SetParam('CoNbr', CoNbr);
    ctx_DataAccess.GetCustomData(FCurrentEE, AsVariant);
  end;
  FCurrentEE.IndexFieldNames := 'EE_NBR';
  FCurrentEE_TIME_OFF_ACCRUAL.ProviderName := DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.ProviderName;
  FCurrentEE_TIME_OFF_ACCRUAL.ClientID := ctx_DataAccess.ClientID;
  with TExecDSWrapper.Create('GenericSelectCurrent') do
  begin
    SetMacro('TABLENAME', 'EE_TIME_OFF_ACCRUAL');
    SetMacro('COLUMNS', '*');
    ctx_DataAccess.GetCustomData(FCurrentEE_TIME_OFF_ACCRUAL, AsVariant);
  end;
  FCurrentEE_TIME_OFF_ACCRUAL.IndexFieldNames := 'EE_TIME_OFF_ACCRUAL_NBR';
  FEE_TIME_OFF_ACCRUAL_OPER.ProviderName := DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.ProviderName;
  FEE_TIME_OFF_ACCRUAL_OPER.ClientID := ctx_DataAccess.ClientID;
  FEE_TIME_OFF_ACCRUAL_OPER.IndexFieldNames := 'EE_TIME_OFF_ACCRUAL_OPER_NBR';

  with TExecDSWrapper.Create('GenericSelectWithCondition') do
  begin
    SetMacro('TABLENAME', 'EE_TIME_OFF_ACCRUAL_OPER');
    SetMacro('COLUMNS', '*');
    SetMacro('CONDITION', AlwaysFalseCond);
    ctx_DataAccess.GetCustomData(FEE_TIME_OFF_ACCRUAL_OPER, AsVariant);
  end;

  FEE_TIME_OFF_ACCRUAL_OPER.Filter := ' not OPERATION_CODE in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ';
  FEE_TIME_OFF_ACCRUAL_OPER.Filtered := True;

  FEE_HR_ATTENDANCE.ProviderName := DM_EMPLOYEE.EE_HR_ATTENDANCE.ProviderName;
  FEE_HR_ATTENDANCE.ClientID := ctx_DataAccess.ClientID;
  FEE_HR_ATTENDANCE.IndexFieldNames := 'EE_HR_ATTENDANCE_NBR';
  with TExecDSWrapper.Create('GenericSelectWithCondition') do
  begin
    SetMacro('TABLENAME', 'EE_HR_ATTENDANCE');
    SetMacro('COLUMNS', '*');
    SetMacro('CONDITION', AlwaysFalseCond);
    ctx_DataAccess.GetCustomData(FEE_HR_ATTENDANCE, AsVariant);
  end;

  FCollapsedEeOpers.IndexDefs.Add('EeToaNbr', 'EE_TIME_OFF_ACCRUAL_NBR;ACCRUAL_DATE;order_nbr', []);
  FCollapsedEeOpers.IndexDefs.Add('PrNbr', 'OPERATION_CODE;EE_TIME_OFF_ACCRUAL_NBR;PR_NBR;order_nbr', []);
  with TExecDSWrapper.Create('CollapsedTimeOffOperations') do
  begin
    SetMacro('COND', 'o.accrual_date >= :BegDate');
    SetParam('BegDate', StartingDate);
    ctx_DataAccess.GetCustomData(FCollapsedEeOpers, AsVariant);
    FCollapsedEeOpers.LogChanges := False;
  end;
  FShadowCollapsedEeOpers.CloneCursor(FCollapsedEeOpers, True);
  FShadowCollapsedEeOpers.LogChanges := False;
  FShadowCollapsedEeOpers.IndexFieldNames := 'EE_TIME_OFF_ACCRUAL_OPER_NBR';
  with TExecDSWrapper.Create('DatedTimeOffBalances') do
  begin
    SetParam('EndDate', StartingDate-1);
    ctx_DataAccess.GetCustomData(FStartingEeToaBalance, AsVariant);
    FStartingEeToaBalance.LogChanges := False;
  end;
  FStartingEeToaBalance.IndexFieldNames := 'EE_TIME_OFF_ACCRUAL_NBR';

  with TExecDSWrapper.Create('BetweeenDateTimeOffBalances') do
  begin
    SetParam('BeginDate', GetBeginYear(StartingDate));
    SetParam('EndDate', StartingDate-1);
    ctx_DataAccess.GetCustomData(FBetweenDateEeToaBalance, AsVariant);
    FBetweenDateEeToaBalance.LogChanges := False;
  end;
  FBetweenDateEeToaBalance.IndexFieldNames := 'EE_TIME_OFF_ACCRUAL_NBR';
end;

destructor TAbstractToaSetupAccess.Destroy;
begin
  FreeAndNil(FCO_TIME_OFF_ACCRUAL);
  FreeAndNil(FCO_TIME_OFF_ACCRUAL_RATES);
  FreeAndNil(FCO_TIME_OFF_ACCRUAL_TIERS);
  FreeAndNil(FEE_TIME_OFF_ACCRUAL);
  FreeAndNil(FEE_HR_ATTENDANCE);
  FreeAndNil(FEE_TIME_OFF_ACCRUAL_OPER);
  FreeAndNil(FEE);
  FreeAndNil(FCurrentEE_TIME_OFF_ACCRUAL);
  FreeAndNil(FCurrentEE);
  FreeAndNil(FShadowCollapsedEeOpers);
  FreeAndNil(FCollapsedEeOpers);
  FreeAndNil(FStartingEeToaBalance);
  FreeAndNil(FChangedEeToa);
  inherited;
end;

function TAbstractToaSetupAccess.HireDate: TDateTime;
begin
  if not VarIsNull(EE_TIME_OFF_ACCRUAL['EFFECTIVE_ACCRUAL_DATE']) then
    Result := EE_TIME_OFF_ACCRUAL['EFFECTIVE_ACCRUAL_DATE']
  else
  if not VarIsNull(CurrentEE['CURRENT_HIRE_DATE']) then
    Result := CurrentEE['CURRENT_HIRE_DATE']
  else
    raise EAccrualException.CreateFmtHelp('Employee #%s. Missing current hire date',
      [CurrentEE['CUSTOM_EMPLOYEE_NUMBER']], IDH_InconsistentData);
end;

procedure TAbstractToaSetupAccess.LocateEeToaSetup(const EeToaNbr: Integer; const PeriodEndDate: TDateTime);
begin
  FEeToaNbr := EeToaNbr;
  FPeriodEndDate := PeriodEndDate;
end;

function TAbstractToaSetupAccess.PayrollCapAdjustment(const Accrued: Double): Double;
var
  dPayrollCap: Double;
begin
  Result := 0;
  dPayrollCap:= CO_TIME_OFF_ACCRUAL.FieldByName('PAYROLL_MAXIMUM_HOURS_TO_ACCRUE').AsFloat;
  if dPayrollCap > 0 then
  begin
    if Accrued > dPayrollCap then
      Result := dPayrollCap- Accrued;
  end;
end;

procedure TAbstractToaSetupAccess.PostChanges;
begin
  ctx_DataAccess.PostDataSets([CurrentEE_TIME_OFF_ACCRUAL, EE_TIME_OFF_ACCRUAL_OPER, EE_HR_ATTENDANCE]);
end;

procedure TAbstractToaSetupAccess.ReadjustOperations(const PrNbr: Variant;
  const AccrualDate: TDateTime);
var
  tmpAccrued : double;
  dBalance, dCapAdj, dAccrued, dCappedAccrued, dUsed: Double;
  fAccrued, fCappedAccrued, fPayrollCappedAccrued,
  fUsed, fPrNbr, fPrCheckNbr, fEeToaOperNbr,
  fPrBatchNbr: TField;
  cdBatchPeriodEndDate: TevClientDataSet;
  cdCheckPeriodEndDate: TevClientDataSet;
  cdChangedEeToa: TevClientDataSet;
  iPrNbr: Integer;
  sBookMark: string;
  procedure LocateSetup;
  var
    dPeriodEndDate: TDateTime;
  begin
    if not fPrCheckNbr.IsNull then
    begin
      Assert(cdCheckPeriodEndDate.FindKey([fPrCheckNbr.AsInteger]));
      dPeriodEndDate := cdCheckPeriodEndDate['PERIOD_END_DATE'];
    end
    else
    if not fPrBatchNbr.IsNull then
    begin
      Assert(cdBatchPeriodEndDate.FindKey([fPrBatchNbr.AsInteger]));
      dPeriodEndDate := cdBatchPeriodEndDate['PERIOD_END_DATE'];
    end
    else
      dPeriodEndDate := CollapsedEeOpers['accrual_date'];
    LocateEeToaSetup(CollapsedEeOpers['EE_TIME_OFF_ACCRUAL_NBR'], dPeriodEndDate);
  end;
begin
  cdBatchPeriodEndDate := TevClientDataSet.CreateWithChecksDisabled(nil);
  cdCheckPeriodEndDate := TevClientDataSet.CreateWithChecksDisabled(nil);
  cdChangedEeToa := TevClientDataSet.CreateWithChecksDisabled(nil);
  try
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('TABLE1', 'PR');
      SetMacro('TABLE2', 'PR_BATCH');
      SetMacro('COLUMNS', 't2.PR_BATCH_NBR, t2.PERIOD_END_DATE');
      SetMacro('JOINFIELD', 'PR');
      SetMacro('CONDITION', 't1.co_nbr=:CoNbr');
      SetParam('CoNbr', CoNbr);
      ctx_DataAccess.GetCustomData(cdBatchPeriodEndDate, AsVariant);
      cdBatchPeriodEndDate.IndexFieldNames := 'PR_BATCH_NBR';
    end;
    with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
    begin
      SetMacro('TABLE1', 'PR_CHECK');
      SetMacro('TABLE2', 'PR_BATCH');
      SetMacro('TABLE3', 'PR');
      SetMacro('JOINFIELD2', 'PR');
      SetMacro('JOINFIELD', 'PR_BATCH');
      SetMacro('COLUMNS', 't1.PR_CHECK_NBR, t2.PERIOD_END_DATE');
      SetMacro('CONDITION', 't3.co_nbr=:CoNbr');
      SetParam('CoNbr', CoNbr);
      ctx_DataAccess.GetCustomData(cdCheckPeriodEndDate, AsVariant);
      cdCheckPeriodEndDate.IndexFieldNames := 'PR_CHECK_NBR';
    end;
    fAccrued := CollapsedEeOpers.FieldByName('adj_accrued');
    fPayrollCappedAccrued := CollapsedEeOpers.FieldByName('payroll_cap_accrued');
    fCappedAccrued := CollapsedEeOpers.FieldByName('adj_accrued_capped');
    fUsed := CollapsedEeOpers.FieldByName('adj_used');
    fPrNbr := CollapsedEeOpers.FieldByName('PR_NBR');
    fPrBatchNbr := CollapsedEeOpers.FieldByName('PR_BATCH_NBR');
    fPrCheckNbr := CollapsedEeOpers.FieldByName('PR_CHECK_NBR');
    fEeToaOperNbr := CollapsedEeOpers.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR');
    CollapsedEeOpers.IndexName := 'PrNbr';
    ChangedEeToa.First;
    while not ChangedEeToa.Eof do
    begin
      CollapsedEeOpers.SetRange([EE_TOA_OPER_CODE_ACCRUAL, ChangedEeToa['NBR']], [EE_TOA_OPER_CODE_ACCRUAL, ChangedEeToa['NBR']]);
      CollapsedEeOpers.First;
      iPrNbr := fPrNbr.AsInteger;
      dAccrued := 0;
      while not CollapsedEeOpers.Eof do
      begin
        dAccrued := dAccrued+ fAccrued.AsFloat;
        CollapsedEeOpers.Next;
        if CollapsedEeOpers.Eof or (iPrNbr <> fPrNbr.AsInteger) then
        begin
          if not CollapsedEeOpers.Eof then
            CollapsedEeOpers.Prior;
          sBookMark := CollapsedEeOpers.Bookmark;
          LocateSetup;
          dCapAdj := PayrollCapAdjustment(dAccrued);
          if dCapAdj < 0 then
          begin
            while dCapAdj < 0 do
            begin
              CollapsedEeOpers.Edit;
              dCapAdj := dCapAdj+ fAccrued.AsFloat;
              if dCapAdj < 0 then
                fPayrollCappedAccrued.AsFloat := 0
              else
                fPayrollCappedAccrued.AsFloat := dCapAdj;
              CollapsedEeOpers.Post;
              CollapsedEeOpers.Prior;
            end;
            CollapsedEeOpers.Bookmark := sBookMark;
          end;
          CollapsedEeOpers.Next;
          iPrNbr := fPrNbr.AsInteger;
          dAccrued := 0;
        end;
      end;
      ChangedEeToa.Next;
    end;
    while True do
    begin
      cdChangedEeToa.Data := ChangedEeToa.Data;
      if cdChangedEeToa.RecordCount = 0 then
        Break;
      cdChangedEeToa.IndexFieldNames := 'NBR';
      CollapsedEeOpers.IndexName := 'EeToaNbr';
      cdChangedEeToa.First;
      while not cdChangedEeToa.Eof do
      begin
        Assert(StartingEeToaBalance.FindKey([cdChangedEeToa['NBR']]));
        dAccrued := StartingEeToaBalance['Accrued'];
        dCappedAccrued := StartingEeToaBalance['Accrued'];
        dUsed := StartingEeToaBalance['Used'];
        CollapsedEeOpers.SetRange([cdChangedEeToa['NBR']], [cdChangedEeToa['NBR']]);
        CollapsedEeOpers.First;
        while not CollapsedEeOpers.Eof do
        begin
          case string(CollapsedEeOpers['OPERATION_CODE'])[1] of
          EE_TOA_OPER_CODE_ROLLOVER_IN,
          EE_TOA_OPER_CODE_MANUAL_ADJ,
          EE_TOA_OPER_CODE_ACCRUAL,
          EE_TOA_OPER_CODE_USED:
            begin
              dUsed := dUsed+ fUsed.AsFloat;
              if not fPayrollCappedAccrued.IsNull then
                dAccrued := dAccrued+ fPayrollCappedAccrued.AsFloat
              else
                dAccrued := dAccrued+ fAccrued.AsFloat;
              dCappedAccrued := dCappedAccrued+ fCappedAccrued.AsFloat;
              LocateSetup;
              if CO_TIME_OFF_ACCRUAL.FieldByName('USE_BALANCE_CAP').AsString = GROUP_BOX_YES then // use balance
                dCapAdj := ConvertNull(TotalCapAdjustment(dAccrued- dUsed), 0)+ dAccrued- dCappedAccrued
              else
                dCapAdj := ConvertNull(TotalCapAdjustment(dAccrued), 0)+ dAccrued- dCappedAccrued;
              if dCapAdj <> 0 then
              begin
                AddOper(fEeToaOperNbr.AsInteger, Null, PrNbr, Null, Null, EE_TOA_OPER_CODE_ADJ,
                  'Cap', AccrualDate, 0, 0, dCapAdj);
                dCappedAccrued := dCappedAccrued+ dCapAdj;
              end;
            end;
          EE_TOA_OPER_CODE_RESET:
            begin
              LocateSetup;
              dBalance := dCappedAccrued- dUsed;
              if not VarIsNull(CO_TIME_OFF_ACCRUAL_RATES['MAXIMUM_CARRYOVER'])
              and (dBalance > CO_TIME_OFF_ACCRUAL_RATES['MAXIMUM_CARRYOVER']) then
                dBalance := CO_TIME_OFF_ACCRUAL_RATES['MAXIMUM_CARRYOVER'];
              if (fUsed.AsFloat <> -dUsed)
              or (fCappedAccrued.AsFloat <> dBalance- dCappedAccrued)
              or (fAccrued.AsFloat <> dBalance- dAccrued) then
                AddOper(fEeToaOperNbr.AsInteger, Null, PrNbr, Null, Null,
                  EE_TOA_OPER_CODE_ADJ, '', AccrualDate,
                  dBalance- dAccrued- fAccrued.AsFloat, -dUsed- fUsed.AsFloat,
                  dBalance- dCappedAccrued- fCappedAccrued.AsFloat);
              //Break;
              dUsed := 0;
              dCappedAccrued := dBalance;
              dAccrued := dBalance;
            end;
          EE_TOA_OPER_CODE_ROLLOVER:
            begin
              LocateSetup;
              //dBalance := dCappedAccrued- dUsed;
              if (fUsed.AsFloat <> -dUsed)
              or (fCappedAccrued.AsFloat <> -dCappedAccrued)
              or (fAccrued.AsFloat <> -dAccrued) then
              begin

                tmpAccrued := (dCappedAccrued- fCappedAccrued.AsFloat)- (dUsed- fUsed.AsFloat);
                if CollapsedEeOpers['CONNECTED_EE_TOA_OPER_NBR'] > 0 then
                begin
                 sBookMark := CollapsedEeOpers.Bookmark;
                 Assert(ShadowCollapsedEeOpers.FindKey([CollapsedEeOpers['CONNECTED_EE_TOA_OPER_NBR']]));
                 CollapsedEeOpers.SetRange([ShadowCollapsedEeOpers['ee_time_off_accrual_nbr']], [ShadowCollapsedEeOpers['ee_time_off_accrual_nbr']]);
                 LocateSetup;
                 Assert(StartingEeToaBalance.FindKey([ShadowCollapsedEeOpers['ee_time_off_accrual_nbr']]));

                 if CO_TIME_OFF_ACCRUAL.FieldByName('USE_BALANCE_CAP').AsString = GROUP_BOX_YES then // use balance
                    tmpAccrued := StartingEeToaBalance['Accrued'] - StartingEeToaBalance['Used']
                 else
                    tmpAccrued := StartingEeToaBalance['Accrued'];

                 tmpAccrued := ConvertNull(TotalCapAdjustment(tmpAccrued + ((dCappedAccrued- fCappedAccrued.AsFloat)- (dUsed- fUsed.AsFloat))),0);

                 CollapsedEeOpers.SetRange([cdChangedEeToa['NBR']], [cdChangedEeToa['NBR']]);

                 LocateSetup;
                 CollapsedEeOpers.Bookmark := sBookMark;

                 if tmpAccrued < 0 then
                   tmpAccrued := tmpAccrued + ((dCappedAccrued- fCappedAccrued.AsFloat)- (dUsed- fUsed.AsFloat))
                 else
                   tmpAccrued := (dCappedAccrued- fCappedAccrued.AsFloat)- (dUsed- fUsed.AsFloat);

                end;
                AddOper(CollapsedEeOpers['CONNECTED_EE_TOA_OPER_NBR'], Null, PrNbr, Null, Null,
                  EE_TOA_OPER_CODE_ADJ, '', AccrualDate,
                  tmpAccrued, 0, Null);

                AddOper(fEeToaOperNbr.AsInteger, Null, PrNbr, Null, Null,
                  EE_TOA_OPER_CODE_ADJ, '', AccrualDate,
                  -fAccrued.AsFloat- dAccrued, -fUsed.AsFloat- dUsed, -fCappedAccrued.AsFloat- dCappedAccrued);
              end;
              //Break;
              dUsed := 0;
              dCappedAccrued := 0;
              dAccrued := 0;
            end;
          EE_TOA_OPER_CODE_VOID,
          EE_TOA_OPER_CODE_ADJ:
            Assert(False); // should always be collapsed
          end;
          CollapsedEeOpers.Next;
        end;
        Assert(ChangedEeToa.FindKey([cdChangedEeToa['NBR']]));
        ChangedEeToa.Delete;
        cdChangedEeToa.Next;
      end;

      CollapsedEeOpers.CancelRange;
      CollapsedEeOpers.Filtered := False;
      CollapsedEeOpers.First;
      while not CollapsedEeOpers.Eof do
        if EE_TIME_OFF_ACCRUAL_OPER.FindKey([CollapsedEeOpers.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR')]) then
           CollapsedEeOpers.Delete
          else
           CollapsedEeOpers.Next;

    end;
  finally
    cdChangedEeToa.Free;
    cdCheckPeriodEndDate.Free;
    cdBatchPeriodEndDate.Free;
  end;
end;

function TAbstractToaSetupAccess.TotalCapAdjustment(const Accrued: Double): Variant;
begin
  Result := Null;
  if not VarIsNull(EE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM']) then
  begin
    if EE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM']< Accrued then
      Result := EE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM']- Accrued;
  end
  else
  if not VarIsNull(CO_TIME_OFF_ACCRUAL_RATES['ANNUAL_ACCRUAL_MAXIMUM']) then
  begin
    if CO_TIME_OFF_ACCRUAL_RATES['ANNUAL_ACCRUAL_MAXIMUM']< Accrued then
      Result := CO_TIME_OFF_ACCRUAL_RATES['ANNUAL_ACCRUAL_MAXIMUM']- Accrued;
  end
  else
  if not VarIsNull(CO_TIME_OFF_ACCRUAL['ANNUAL_MAXIMUM_HOURS_TO_ACCRUE']) then
  begin
    if CO_TIME_OFF_ACCRUAL['ANNUAL_MAXIMUM_HOURS_TO_ACCRUE']< Accrued then
      Result := CO_TIME_OFF_ACCRUAL['ANNUAL_MAXIMUM_HOURS_TO_ACCRUE']- Accrued;
  end;
end;

procedure TAbstractToaSetupAccess.VoidPrTimeOff(const PrNbr: Integer; const AccrualDate: TDateTime;
  const VoidingPrNbr: Integer);
var
  d: TevClientDataSet;
begin
  d := TevClientDataSet.CreateWithChecksDisabled(nil);
  try
    d.CloneCursor(CollapsedEeOpers, True);
    d.IndexFieldNames := 'PR_NBR';
    d.SetRange([PrNbr], [PrNbr]);
    d.First;
    while not d.Eof do
    begin
      AddOper(d['EE_TIME_OFF_ACCRUAL_OPER_NBR'], Null, VoidingPrNbr, Null, Null,
        EE_TOA_OPER_CODE_VOID, '', AccrualDate, -d['adj_accrued'], -d['adj_used'],
        -d['adj_accrued_capped'], d['ee_time_off_accrual_nbr']);
      d.Next;
    end;
  finally
    d.Free;
  end;
end;

{ THistoricToaSetupAccess }

function THistoricToaSetupAccess.AddEeToa(const EE_NBR,
  CO_TIME_OFF_ACCRUAL_NBR: Integer; const EFFECTIVE_ACCRUAL_DATE,
  NEXT_ACCRUE_DATE, ANNUAL_ACCRUAL_MAXIMUM, OVERRIDE_RATE: Variant): Integer;
begin
  Result := inherited AddEeToa(EE_NBR, CO_TIME_OFF_ACCRUAL_NBR, EFFECTIVE_ACCRUAL_DATE,
    NEXT_ACCRUE_DATE, ANNUAL_ACCRUAL_MAXIMUM, OVERRIDE_RATE);
  EE_TIME_OFF_ACCRUAL.Append;
  EE_TIME_OFF_ACCRUAL['EE_TIME_OFF_ACCRUAL_NBR'] := Result;
  EE_TIME_OFF_ACCRUAL['EE_NBR'] := EE_NBR;
  EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'] := CO_TIME_OFF_ACCRUAL_NBR;
  EE_TIME_OFF_ACCRUAL['EFFECTIVE_ACCRUAL_DATE'] := EFFECTIVE_ACCRUAL_DATE;
  EE_TIME_OFF_ACCRUAL['NEXT_ACCRUE_DATE'] := NEXT_ACCRUE_DATE;
  EE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM'] := ANNUAL_ACCRUAL_MAXIMUM;
  EE_TIME_OFF_ACCRUAL['OVERRIDE_RATE'] := OVERRIDE_RATE;
  EE_TIME_OFF_ACCRUAL['JUST_RESET'] := GROUP_BOX_NO;
  EE_TIME_OFF_ACCRUAL['STATUS'] := GROUP_BOX_YES;
  EE_TIME_OFF_ACCRUAL['CURRENT_ACCRUED'] := 0;
  EE_TIME_OFF_ACCRUAL['CURRENT_USED'] := 0;
  EE_TIME_OFF_ACCRUAL['MARKED_ACCRUED'] := 0;
  EE_TIME_OFF_ACCRUAL['MARKED_USED'] := 0;
  EE_TIME_OFF_ACCRUAL.Post;
end;

constructor THistoricToaSetupAccess.Create(const CoNbr: Integer; const StartingDate: TDateTime);

  procedure GetLatestRecords(const d: TevClientDataSet; const TableName, Columns,IndexFieldNames: string; const Condition: string = '');
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('TABLENAME', TableName);
      SetMacro('COLUMNS', Columns);
      SetMacro('CONDITION', Condition);
      ctx_DataAccess.GetCustomData(d, AsVariant);
    end;
    d.IndexFieldNames := IndexFieldNames;
    d.Name := TableName;
  end;

begin
  inherited;
  GetLatestRecords(FCO_TIME_OFF_ACCRUAL, 'CO_TIME_OFF_ACCRUAL', '*', 'CO_TIME_OFF_ACCRUAL_NBR', ' CO_NBR='+ IntToStr(CoNbr));
  GetLatestRecords(FCO_TIME_OFF_ACCRUAL_RATES, 'CO_TIME_OFF_ACCRUAL_RATES', '*', 'CO_TIME_OFF_ACCRUAL_NBR', ' 1 = 1' );
  GetLatestRecords(FCO_TIME_OFF_ACCRUAL_TIERS, 'CO_TIME_OFF_ACCRUAL_TIERS', '*', 'CO_TIME_OFF_ACCRUAL_NBR', ' 1 = 1' );
  GetLatestRecords(FEE_TIME_OFF_ACCRUAL, 'EE_TIME_OFF_ACCRUAL', '*', 'EE_TIME_OFF_ACCRUAL_NBR', ' 1 = 1' );
  GetLatestRecords(FEE, 'EE', '*', 'EE_NBR', ' CO_NBR='+ IntToStr(CoNbr));
end;

function THistoricToaSetupAccess.LocateEeProratorSetup(
  const EeToaNbr: Integer; const HoursWorked: Double; const PeriodEndDate: TDateTime): Boolean;
var
  sMonthCount: string;
begin
  sMonthCount := IntToStr(CountDownMonths(HireDate, PeriodEndDate));

  CO_TIME_OFF_ACCRUAL_TIERS.SetRange([EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']], [EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']]);
  CO_TIME_OFF_ACCRUAL_TIERS.Filter :='MINIMUM_MONTH_NUMBER<='+ sMonthCount+ ' and MAXIMUM_MONTH_NUMBER>='+ sMonthCount+
    ' and MINIMUM_HOURS<='+ FloatToStr(HoursWorked)+ ' and MAXIMUM_HOURS>'+ FloatToStr(HoursWorked);
  CO_TIME_OFF_ACCRUAL_TIERS.Filtered := true;
  Result := CO_TIME_OFF_ACCRUAL_TIERS.RecordCount = 1;
end;

procedure THistoricToaSetupAccess.LocateEeToaSetup(const EeToaNbr: Integer; const PeriodEndDate: TDateTime);
var
  sMonthCount: string;
begin
  inherited;
  if not EE_TIME_OFF_ACCRUAL.Locate('EE_TIME_OFF_ACCRUAL_NBR',EeToaNbr,[]) then
    raise EAccrualException.CreateFmtHelp('Can not find EE TOA record #%d ', [EeToaNbr], IDH_InconsistentData);
  if not CurrentEE_TIME_OFF_ACCRUAL.FindKey([EeToaNbr]) then
    raise EAccrualException.CreateFmtHelp('Can not find EE TOA record #%d', [EeToaNbr], IDH_InconsistentData);
  if not EE.Locate('EE_NBR',EE_TIME_OFF_ACCRUAL['EE_NBR'], []) then
    raise EAccrualException.CreateFmtHelp('Can not find EE record #%d ', [integer(EE_TIME_OFF_ACCRUAL['EE_NBR'])], IDH_InconsistentData);
  if not CurrentEE.FindKey([EE_TIME_OFF_ACCRUAL['EE_NBR']]) then
    raise EAccrualException.CreateFmtHelp('Can not find EE record #%d', [integer(EE_TIME_OFF_ACCRUAL['EE_NBR'])], IDH_InconsistentData);
  if not CO_TIME_OFF_ACCRUAL.Locate('CO_TIME_OFF_ACCRUAL_NBR', EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'], []) then
    raise EAccrualException.CreateFmtHelp('Can not find CO TOA record #%d', [integer(EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'])], IDH_InconsistentData);
  if string(CO_TIME_OFF_ACCRUAL['CALCULATION_METHOD'])[1] <> TIME_OFF_ACCRUAL_CALC_METHOD_BALANCES_ONLY then
  begin
    sMonthCount := IntToStr(CountDownMonths(HireDate, PeriodEndDate));

  CO_TIME_OFF_ACCRUAL_RATES.SetRange([EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']], [EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']]);
  CO_TIME_OFF_ACCRUAL_RATES.Filter := 'MINIMUM_MONTH_NUMBER<='+ sMonthCount+ ' and MAXIMUM_MONTH_NUMBER>='+ sMonthCount;
  CO_TIME_OFF_ACCRUAL_RATES.Filtered := true;
  if CO_TIME_OFF_ACCRUAL_RATES.RecordCount <> 1 then
      raise EAccrualException.CreateFmtHelp('Time off accrual "%s". Can''t find rate for month number %s', [CO_TIME_OFF_ACCRUAL['DESCRIPTION'], sMonthCount], IDH_InconsistentData);
  end
  else
    CO_TIME_OFF_ACCRUAL_RATES.SetRange([0], [0]); // to produce empty recordset

  FRoundingPrecision:= 6;
  if not StartingEeToaBalance.FindKey([EeToaNbr]) then
    raise EAccrualException.CreateFmtHelp('Can not find starting balance for EE TOA record #%d', [EeToaNbr], IDH_InconsistentData);

  if not BetweenDateEeToaBalance.FindKey([EeToaNbr]) then
    raise EAccrualException.CreateFmtHelp('Can not find between date balance for EE TOA record #%d', [EeToaNbr], IDH_InconsistentData);

end;

{ TPayrollEntryCheckToaSetupAccess }

procedure TPayrollEntryCheckToaSetupAccess.Check;
begin
  if not CheckBalance(False) then
    if EvMessage('Entered time exceeds available time off', mtWarning, mbOKCancel, mbCancel) = mrCancel then
      AbortEx;
end;

function TPayrollEntryCheckToaSetupAccess.CheckBalance(const CheckRegardlessFlag: Boolean): Boolean;
  function AddOtherCheckLinesHours: Double;
  var
    cd: TevClientDataSet;
  begin
    Result := 0;
    cd := TevClientDataSet.CreateWithChecksDisabled(nil);
    try
      cd.CloneCursor(DM_PAYROLL.PR_CHECK_LINES, False);
      if cd.Filter = '' then
      begin
        cd.Filter := 'PR_CHECK_NBR='+ IntToStr(DM_PAYROLL.PR_CHECK['PR_CHECK_NBR']);
        cd.Filtered := True;
      end;
      cd.First;
      while not cd.Eof do
      begin
        if cd['PR_CHECK_LINES_NBR'] <> DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'] then
          if not VarIsNull(cd['HOURS_OR_PIECES']) then
            if (cd['CL_E_DS_NBR'] = CO_TIME_OFF_ACCRUAL['CL_E_DS_NBR'])
            or not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_DS_NBR;CL_E_D_GROUPS_NBR',
              VarArrayOf([cd['CL_E_DS_NBR'], CO_TIME_OFF_ACCRUAL['USED_CL_E_D_GROUPS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
                Result := Result+ cd['HOURS_OR_PIECES'];
        cd.Next;
      end;
    finally
      cd.Free;
    end;
  end;
var
  dAccrued, dUsed: Double;
begin
  Result := True;
  try
    FShadowCurrentEeToa.SetRange([DM_PAYROLL.PR_CHECK['EE_NBR']], [DM_PAYROLL.PR_CHECK['EE_NBR']]);
    FShadowCurrentEeToa.First;
    while not FShadowCurrentEeToa.Eof do
    begin
      try
        LocateEeToaSetup(FShadowCurrentEeToa['EE_TIME_OFF_ACCRUAL_NBR'], FCheckDate);
        if (EE_TIME_OFF_ACCRUAL['STATUS'] = GROUP_BOX_YES)
        and (CheckRegardlessFlag
          or (CO_TIME_OFF_ACCRUAL['CHECK_TIME_OFF_AVAIL'] = GROUP_BOX_YES)
          or (DM_COMPANY.CO['CHECK_TIME_OFF_AVAIL'] = GROUP_BOX_YES)) then
            if (DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] = CO_TIME_OFF_ACCRUAL['CL_E_DS_NBR'])
            or not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_DS_NBR;CL_E_D_GROUPS_NBR',
              VarArrayOf([DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'], CO_TIME_OFF_ACCRUAL['USED_CL_E_D_GROUPS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
            begin
              CalcAnyDayEndBalance(FCheckDate, dAccrued, dUsed);
              if (DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES']+ AddOtherCheckLinesHours > (dAccrued- dUsed)) or
                 ( not VarIsNull( CO_TIME_OFF_ACCRUAL['ANNUAL_USAGE_MAXIMUM']) and
                    (CO_TIME_OFF_ACCRUAL.FieldByName('ANNUAL_USAGE_MAXIMUM').AsFloat <
                         (BetweenDateEeToaBalance.FieldByName('used').AsFloat + DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES']+ AddOtherCheckLinesHours))) then
              begin
                Result := False;
                Break;
              end;
            end;
      except
        on EAccrualException do ;
      end;
      FShadowCurrentEeToa.Next;
    end;
  except
    on EAbort do
      raise;
    on E: Exception do
      if EvMessage(E.Message, mtWarning, mbOKCancel, mbCancel) = mrCancel then
        AbortEx;
  end;
end;

constructor TPayrollEntryCheckToaSetupAccess.Create(const CoNbr: Integer;
  const StartingDate, CheckDate: TDateTime);
begin
  inherited Create(CoNbr, StartingDate);
  FCheckDate := CheckDate;
  FShadowCurrentEeToa := TevClientDataSet.CreateWithChecksDisabled(nil);
  FShadowCurrentEeToa.CloneCursor(CurrentEE_TIME_OFF_ACCRUAL, True);
  FShadowCurrentEeToa.IndexFieldNames := 'EE_NBR';
  DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
end;

destructor TPayrollEntryCheckToaSetupAccess.Destroy;
begin
  FreeAndNil(FShadowCurrentEeToa);
  inherited;
end;

end.
