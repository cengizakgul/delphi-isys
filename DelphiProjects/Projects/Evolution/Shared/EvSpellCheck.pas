// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//the same as in Resolution; ActiveDS function copied from gdydbcommon
unit EvSpellCheck;

interface

uses
  stdactns, classes, menus, stdctrls, Variants;

type

  TSpellChecker = class
  private
    oWordApplication: OleVariant;
    FCount: integer;
    FMaxSpellchecksByOneInstance: integer; //restart word after each FMaxSpellchecksByOneInstance spellchecks perfomed
  public
    constructor Create;
    destructor Destroy; override;

    procedure SpellCheck( var text: string );
    function IsAvailable: boolean;
  end;

  TCheckSpelling = class(TEditAction)
  protected
    function IsReadOnly( memo: TCustomMemo ): boolean;
  public
    function  HandlesTarget(Target: TObject): Boolean; override;
    procedure ExecuteTarget(Target: TObject); override;
    procedure UpdateTarget(Target: TObject); override;
  end;


function SpellChecker: TSpellChecker;
function CheckSpellingActionInstance: TCheckSpelling;
function EnsureHasSpellcheckItem( has: boolean; Self: TComponent; pm: TPopupMenu ): TPopupMenu;

implementation
uses
  comobj, sysutils, controls, forms,
  dbctrls, db, typinfo, evUIUtils;

var
  FSpellChecker: TSpellChecker;
  FCheckSpelling: TCheckSpelling;

function EnsureHasSpellcheckItem( has: boolean; Self: TComponent; pm: TPopupMenu ): TPopupMenu;
begin
  Result := EnsureHasMenuItem( has, Self, pm,  CheckSpellingActionInstance );
end;

function SpellChecker: TSpellChecker;
begin
  Result := FSpellChecker;
end;

function CheckSpellingActionInstance: TCheckSpelling;
begin
  Result := FCheckSpelling;
end;

{ TSpellChecker }

constructor TSpellChecker.Create;
begin
  oWordApplication := Null;
  FMaxSpellchecksByOneInstance := 1;
end;

destructor TSpellChecker.Destroy;
begin
  if not VarIsNull(oWordApplication) then
  begin
    oWordApplication.Quit(false);
    oWordApplication := Null;
  end;
  inherited;
end;

function TSpellChecker.IsAvailable: boolean;
begin
  Result := true;
  try
    ProgIDToClassID('Word.Application');
  except
    Result := false;
  end;
end;

procedure TSpellChecker.SpellCheck(var text: string);
const
  wdDialogToolsSpellingAndGrammar = 828;
  wdDoNotSaveChanges = 0;
//var
//  v: Variant;
begin
  if varIsNull( oWordApplication ) then
  begin
    oWordApplication := CreateOleObject('Word.Application');
    oWordApplication.Visible := false;
    FCount := 0;
  end;
  try
    oWordApplication.Documents.Add;
    oWordApplication.Selection.Text := text;
  //  v := oWordApplication.Dialogs.Item(wdDialogToolsSpellingAndGrammar).Show();
    oWordApplication.ActiveDocument.CheckSpelling;
    oWordApplication.Visible := false;
    text := oWordApplication.Selection.Text;
    text := StringReplace( text, #13, #13+#10, [rfReplaceAll] );
    oWordApplication.ActiveDocument.Close( wdDoNotSaveChanges );
    inc(FCount);
  except
    on E: Exception do begin
                          Application.HandleException( e );
                          if not VarIsNull(oWordApplication) then
                          begin
                            try
                              oWordApplication.Quit(false);
                            except
                            end;
                            oWordApplication := Null;
                          end;
                       end;
  end;
  if not VarIsNull(oWordApplication) then
    if ( FMaxSpellchecksByOneInstance <> 0 ) and ( FCount >= FMaxSpellchecksByOneInstance ) then
    begin
      try
        oWordApplication.Quit(false);
      except
        on e: Exception do Application.HandleException( e );
      end;
      oWordApplication := Null;
  end;
end;

{ TCheckSpelling }

resourcestring
{$ifdef RUSSIAN}
  sEditMenuCheckSpelling = '&��������� ����������';
{$else}
  sEditMenuCheckSpelling = 'Check &Spelling';
{$endif}

procedure TCheckSpelling.ExecuteTarget(Target: TObject);
var
  s: string;
  oldScreenCursor: TCursor;
  memo: TCustomMemo;
begin
  oldScreenCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    memo := Target as TCustomMemo;
    s := memo.Text;
    SpellChecker.SpellCheck( s );
    if (trim(s) <> '') and (memo.Text <> s) then
    begin
      if (memo is TDBMemo) and DSIsActive((memo as TDBMemo).DataSource) then
        (memo as TDBMemo).DataSource.DataSet.Edit;
      memo.Text := s;
    end;
  finally
    Screen.Cursor := oldScreenCursor;
  end;
end;

type
  TCheatCustomMemo = class(TCustomMemo)
  end;

function TCheckSpelling.HandlesTarget(Target: TObject): Boolean;
begin
  Result := inherited HandlesTarget(Target) and ( Target is TCustomMemo ) and not IsReadonly(TCustomMemo(Target));
end;

function TCheckSpelling.IsReadOnly(memo: TCustomMemo): boolean;
begin
  // properties are not virtual
  if memo is TDBMemo then
    Result := TDBMemo(memo).ReadOnly
  else
    Result := TCheatCustomMemo(memo).ReadOnly;
{  Result := TCheatCustomMemo(memo).Readonly;
  //!!xx
  if memo is TDBMemo then
    Result := false;
}
//paranoia; this is not needed currently but makes code less fragile
  if IsPublishedProp( memo, 'SecurityRO' ) then
    Result :=  Result or (GetOrdProp( memo, 'SecurityRO' ) = 1);
end;

procedure TCheckSpelling.UpdateTarget(Target: TObject);
begin
//  Visible := not TCheatCustomMemo(Target).Readonly;
  Enabled := SpellChecker.IsAvailable and ( (Target as TCustomMemo).Lines.Count > 0 );
end;

initialization
  FSpellChecker := TSpellChecker.Create;

  FCheckSpelling := TCheckSpelling.Create(nil);
  FCheckSpelling.Caption := sEditMenuCheckSpelling;// + '   place your shortcut here (F7 ?)';
  FCheckSpelling.ShortCut := TextToShortCut('F7');
  
finalization
  FreeAndNil( FCheckSpelling );

  try
    FreeAndNil( FSpellChecker );
  except
  end;
end.
