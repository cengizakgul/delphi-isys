// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy22
unit SPD_MemoEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  ExtCtrls, stdactns, menus, actnlist,
  ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TMemoEditor = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evMemo1: TevMemo;
    evPanel3: TevPanel;
    evBitBtn2: TevBitBtn;
    evBitBtn1: TevBitBtn;
  private
    function GetText: string;
    procedure SetText(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    procedure SetReadOnly( Value: boolean );
    property Text: string read GetText write SetText;

  end;

function EnsureHasEditMemoItem( has: boolean; Self: TComponent; pm: TPopupMenu ): TPopupMenu;

implementation

uses
  dbctrls, db, typinfo, evUIUtils;

type
  TEditMemo = class(TEditAction)
  protected
    function IsReadOnly( memo: TCustomMemo ): boolean;
  public
    function  HandlesTarget(Target: TObject): Boolean; override;
    procedure ExecuteTarget(Target: TObject); override;
    procedure UpdateTarget(Target: TObject); override;
  end;


var
  FEditMemo: TEditMemo;
{$R *.DFM}

{ TMemoEditor }

function TMemoEditor.GetText: string;
begin
  Result := evMemo1.Text;
end;

procedure TMemoEditor.SetReadOnly(Value: boolean);
begin
  evMemo1.ReadOnly := Value;
  if Value then
    Caption := 'View Text'
  else
    Caption := 'Edit Text'
end;

procedure TMemoEditor.SetText(const Value: string);
begin
  evMemo1.Text := Value;
end;

{ TEditText }
type
  TCheatCustomMemo = class(TCustomMemo)
  end;

procedure TEditMemo.ExecuteTarget(Target: TObject);
var
  memo: TCustomMemo;
begin
  memo := Target as TCustomMemo;
  with TMemoEditor.Create( nil ) do
  try
    Text := memo.Text;
    SetReadOnly( IsReadOnly(memo) );
    if ShowModal = mrOk then
      if not IsReadOnly(memo) and (Text <> memo.Text) then
      begin
        if (memo is TDBMemo) and DSIsActive((memo as TDBMemo).DataSource) then
          (memo as TDBMemo).DataSource.DataSet.Edit;
        memo.Text := Text;
      end;
  finally
    Free;
  end;
end;

function TEditMemo.HandlesTarget(Target: TObject): Boolean;
begin
  Result := inherited HandlesTarget(Target) and ( Target is TCustomMemo );
end;

function TEditMemo.IsReadOnly(memo: TCustomMemo): boolean;
begin
  // properties are not virtual
  if memo is TDBMemo then
    Result := TDBMemo(memo).ReadOnly
  else
    Result := TCheatCustomMemo(memo).ReadOnly;
{  Result := TCheatCustomMemo(memo).Readonly;
  //!!xx
  if memo is TDBMemo then
    Result := false;
}
//paranoia; this is not needed currently but makes code less fragile
  if IsPublishedProp( memo, 'SecurityRO' ) then
    Result :=  Result or (GetOrdProp( memo, 'SecurityRO' ) = 1);
end;

procedure TEditMemo.UpdateTarget(Target: TObject);
begin
  Enabled := true;
  if IsReadOnly(Target as TCustomMemo) then
    Caption := 'View in large window'
  else
    Caption := 'Edit in large window'
end;

function EnsureHasEditMemoItem( has: boolean; Self: TComponent; pm: TPopupMenu ): TPopupMenu;
begin
  Result := EnsureHasMenuItem( has, Self, pm,  FEditMemo );
end;

initialization
  FEditMemo := TEditMemo.Create( nil );
//  FEditMemo.Caption := 'Edit in large window';

finalization
  FreeAndNil( FEditMemo );
end.
