// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ADD_RECORDS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  Db, Wwdatsrc, ActnList, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, EvUtils, EvUIUtils, EvUIComponents, ISBasicClasses,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel;

type
  TADD_RECORDS = class(TFrame)
    evPanel1: TevPanel;
    evDBGrid1: TevDBGrid;
    evPanel2: TevPanel;
    evDBGrid2: TevDBGrid;
    evPanel3: TevPanel;
    evActionList1: TevActionList;
    Add: TAction;
    Delete: TAction;
    dsMasterSrc: TevDataSource;
    dsDetailSrc: TevDataSource;
    dsMasterDest: TevDataSource;
    dsDetailDest: TevDataSource;
    evBitBtn3: TevBitBtn;
    evBitBtn4: TevBitBtn;
    fpSource: TisUIFashionPanel;
    fpDetination: TisUIFashionPanel;
    procedure AddExecute(Sender: TObject);
    procedure AddUpdate(Sender: TObject);
    procedure DeleteExecute(Sender: TObject);
    procedure DeleteUpdate(Sender: TObject);
  private
    FSrc,
    FDest: TDataSet;
  protected
    procedure DoTransfer; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    property Dest : TDataSet read FDest write FDest;
    property Src : TDataSet read FSrc write FSrc;
  end;

implementation

{$R *.DFM}

constructor TADD_RECORDS.Create(AOwner: TComponent);
begin
  inherited;
  if Assigned(evDbgrid1.DataSource) and
     Assigned(evDbgrid1.DataSource.DataSet)
  then
    FSrc := evDbgrid1.DataSource.DataSet;

  if Assigned(evDbgrid2.DataSource) and
     Assigned(evDbgrid2.DataSource.DataSet)
  then
    FDest := evDbgrid2.DataSource.DataSet;
end;


procedure TADD_RECORDS.AddExecute(Sender: TObject);
var
  i: integer;
  SavePlace: TBookmark;
begin
  inherited;
  SavePlace := nil;
  if Assigned(FSrc) and Assigned(FDest) then
    with FDest do
    begin
      if State in [dsInsert, dsEdit] then
        Post;
      if State in [dsBrowse] then
        SavePlace := FSrc.GetBookMark;
      try
        FSrc.DisableControls;
        for i:= 0 to evDbgrid1.SelectedList.Count-1 do
        begin
          FSrc.GotoBookmark(evDbgrid1.SelectedList.items[i]);
          DoTransfer;
        end;
        FSrc.gotoBookMark(SavePlace);
        FSrc.EnableControls;
      finally
        FSrc.FreeBookMark(SavePlace);
      end;
    end;
end;

procedure TADD_RECORDS.AddUpdate(Sender: TObject);
begin
  if Assigned(FSrc) then
    (Sender as TAction).Enabled :=
      (FSrc.State in [dsBrowse]) and (evDbgrid1.SelectedList.Count > 0);

  if (FSrc.State in [dsBrowse]) and (FSrc.RecordCount = 1) then
    evDbgrid1.SelectRecord;
end;

procedure TADD_RECORDS.DeleteExecute(Sender: TObject);
begin
  with FDest do
  if (State in [dsBrowse]) and (RecordCount > 0) then
    if EvMessage('Delete record.  Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      Delete;
end;

procedure TADD_RECORDS.DeleteUpdate(Sender: TObject);
begin
  if Assigned(FDest) then
    (Sender as TAction).Enabled :=
     (FDest.State in [dsBrowse]) and
     (FDest.RecordCount > 0);
end;

procedure TADD_RECORDS.DoTransfer;
begin
end;


end.
