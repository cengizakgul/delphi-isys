unit EvAuditDatabaseFrm;

interface

uses
  Windows, Forms, Classes, Controls, StdCtrls, Variants, EvUIComponents, SysUtils,
  Grids, Wwdbigrd, Wwdbgrid, DB, Wwdatsrc, Buttons, ExtCtrls, DateUtils,
  evDataSet, isDataSet, EvCommonInterfaces, evContext, EvUtils, EvConsts,
  isBasicUtils, EvStreamUtils, ISBasicClasses, isTypes, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isBaseClasses, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, isUIwwDBComboBox, SDataStructure, SDDClasses, EvTypes;

type
  TevAuditDatabase = class(TFrame)
    pnlParams: TevPanel;
    grVersionAudit: TevDBGrid;
    dsrcVersionAudit: TevDataSource;
    procedure OnRecordInteract(Sender: TObject);
    procedure grVersionAuditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure grVersionAuditCreatingSortIndex(Sender: TObject;
      var AFields: String; var AOptions: TIndexOptions;
      var ADescFields: String);
    procedure grVersionAuditSortingChanged(Sender: TObject);
  private
    FDBType: TevDBType;
    FStartDate: TisDate;
    FChangesDS: IevDataSet;
    procedure BuildAuditData;
  public
    procedure Init(const ADBType: TevDBType; const AStartDate: TisDate); reintroduce;
  end;

implementation

uses EvAuditViewFrm;

{$R *.dfm}



{ TevAuditDatabase }

procedure TevAuditDatabase.Init(const ADBType: TevDBType; const AStartDate: TisDate);
begin
  if (FDBType <> ADBType) or (FStartDate <> AStartDate) then
  begin
    FDBType := ADBType;
    FStartDate := AStartDate;

    BuildAuditData;
  end;
end;

procedure TevAuditDatabase.BuildAuditData;
begin
  FChangesDS := ctx_DBAccess.GetDatabaseAudit(FDBType, FStartDate);
  FChangesDS.IndexFieldNames := 'last_change_date';
  dsrcVersionAudit.DataSet := FChangesDS.vclDataSet;
end;

procedure TevAuditDatabase.OnRecordInteract(Sender: TObject);
begin
  if FChangesDS.RecNo > 0 then
    TevAuditView.ShowTableAudit(FChangesDS['table_name'], Trunc(FChangesDS['last_change_date']));
end;

procedure TevAuditDatabase.grVersionAuditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    OnRecordInteract(Self);
end;

procedure TevAuditDatabase.grVersionAuditCreatingSortIndex(Sender: TObject;
  var AFields: String; var AOptions: TIndexOptions; var ADescFields: String);
begin
 if (AFields <> '') and not AnsiSameText(AFields, 'last_change_date') then
  begin
    AFields := '';
    ADescFields := '';
  end
  else
  begin
    AFields := 'last_change_date;table_name';
    if ixDescending in AOptions then
    begin
      AOptions :=  AOptions - [ixDescending];
      ADescFields := 'last_change_date';
    end;
  end;
end;

procedure TevAuditDatabase.grVersionAuditSortingChanged(Sender: TObject);
begin
  FChangesDS.First;
end;

end.

