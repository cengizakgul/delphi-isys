unit EvVersionedSYFedTaxTableBracketsFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, isUIwwDBComboBox, SFieldCodeValues;

type
  TEvVersionedSYFedTaxTableBrackets = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    pnlBottom: TevPanel;
    lblGreaterThanValue: TevLabel;
    lblLessThanValue: TevLabel;
    edLessThanValue: TevDBEdit;
    edGreaterThanValue: TevDBEdit;
    edPercentage: TevDBEdit;
    lblPercentage: TevLabel;
    procedure FormShow(Sender: TObject);
    procedure UpdateFieldOnChange(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TEvVersionedEELocal }

procedure TEvVersionedSYFedTaxTableBrackets.FormShow(Sender: TObject);
begin
  inherited;

  Fields[0].AddValue('Title', lblGreaterThanValue.Caption);
  Fields[0].AddValue('Width', 18);
  lblGreaterThanValue.Caption := Iff(Fields[0].Value['Required'], '~', '') + lblGreaterThanValue.Caption;
  edGreaterThanValue.DataField := Fields.ParamName(0);

  Fields[1].AddValue('Title', lblLessThanValue.Caption);
  Fields[1].AddValue('Width', 6);
  lblLessThanValue.Caption := Iff(Fields[1].Value['Required'], '~', '') + lblLessThanValue.Caption;
  edLessThanValue.DataField := Fields.ParamName(1);

  Fields[2].AddValue('Title', lblPercentage.Caption);
  Fields[2].AddValue('Width', 18);
  lblPercentage.Caption := Iff(Fields[2].Value['Required'], '~', '') + lblPercentage.Caption;
  edPercentage.DataField := Fields.ParamName(2);
end;

procedure TEvVersionedSYFedTaxTableBrackets.UpdateFieldOnChange(
  Sender: TObject);
begin
//  (Sender as TevDBEdit).UpdateRecord;
end;

end.

