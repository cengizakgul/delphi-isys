// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_Open_BASE;

interface

uses
  Windows,  SPackageEntry, SFrameEntry, SDataStructure, StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Controls, Classes, Db, Wwdatsrc, EvUtils, EvMainboard,
  SDDClasses, ISBasicClasses, Variants, SDataDicttemp, EvContext, EvTypes, EvUIUtils,
  EvUIComponents, EvClientDataSet, isBaseClasses, LMDCustomButton,
  LMDButton, isUILMDButton, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, ImgList, isUIFashionPanel, Forms;

type
  TEDIT_OPEN_BASE = class(TFrameEntry)
    DM_TEMPORARY: TDM_TEMPORARY;
    Panel1: TevPanel;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    Panel2: TevPanel;
    pnlSubbrowse: TevPanel;
    Panel3: TevPanel;
    BitBtn1: TevBitBtn;
    pnlFavoriteReport: TevPanel;
    spbFavoriteReports: TevSpeedButton;
    pnlBorder: TevPanel;
    pnlFashionBrowse: TisUIFashionPanel;
    pnlFashionBody: TevPanel;
    sbBrowseOpenBase: TScrollBox;
    PageControlImages: TevImageList;
    btnSwipeClock: TevSpeedButton;
    BtnMRCLogIn: TevSpeedButton;
    sbEDIT_OPEN_BASE_Interior: TScrollBox;
    fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel;
    wwdbgridSelectClient: TevDBGrid;
    Splitter1: TevSplitter;
    fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel;
    procedure wwdbgridSelectClientDblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure wwdbgridSelectClientKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure spbFavoriteReportsClick(Sender: TObject);
    procedure BtnMRCLogInClick(Sender: TObject);
    procedure btnSwipeClockClick(Sender: TObject);
  private
    HasCompany: Boolean;
    FClientReadOnlyFlag: Char;
    bClientReadOnlyFlag : boolean;
    FCachedClientReadOnly: Variant;
  protected
    function GetIfReadOnly: Boolean; override;
    function  ClientReadOnlyFlag: Char;
    procedure SetClientReadOnlyFlag(const bROFlag : boolean);
    procedure CheckStatus; virtual;
    procedure CommitData; virtual;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); virtual;
    procedure SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet); virtual;
    procedure AfterDataSetsReopen; virtual;
    function  IgnoreAutoJump: Boolean; virtual;
    procedure SetReadOnly(Value: Boolean); override;
    function BitBtn1Enabled: Boolean;
  public
    procedure AfterConstruction; override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

uses SysUtils, Dialogs, EvConsts, SSecurityInterface, SPD_CO_FAVORITE_REPORTS, IsBasicUtils;

function TEDIT_OPEN_BASE.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if not Result then
  begin
    if FCachedClientReadOnly = Null then
      if (Self is FindClass('TEDIT_CL_BASE')) then
        FCachedClientReadOnly := ctx_DataAccess.GetClReadOnlyRight
      else
        FCachedClientReadOnly := ctx_DataAccess.GetClCoReadOnlyRight;

    Result := FCachedClientReadOnly;
  end;
end;

procedure TEDIT_OPEN_BASE.wwdbgridSelectClientDblClick(Sender: TObject);
begin
  inherited;
  BitBtn1.Click;
end;

procedure TEDIT_OPEN_BASE.BitBtn1Click(Sender: TObject);
var
  aDS: TArrayDS;
  Close, bClReadOnly: Boolean;
  i: Integer;
  b: Boolean;
begin
  inherited;
  i := 0;
  HasCompany := False;
  if (PageControl1.ActivePage = TabSheet1) or bClientReadOnlyFlag then
  begin
    wwdsList.DataSet.DisableControls;
    try
      if ctx_Statistics.Enabled then
        ctx_Statistics.BeginEvent('OpenClient');

      with Owner as TFramePackageTmpl do
        if ButtonEnabled(NavOK) or ButtonEnabled(NavCommit) then
        begin
          i := wwdsList.DataSet.Recno;
          wwdsList.DataSet.Locate('CL_NBR;' + TevClientDataSet(wwdsMaster.DataSet).TableName + '_NBR', VarArrayOf([ctx_DataAccess.ClientID, wwdsMaster.KeyValue]), []);
        end;
      CheckStatus;
      with Owner as TFramePackageTmpl do
        if BitBtn1.Enabled and ButtonEnabled(NavCommit) then
          ClickButton(NavCommit);
      if i <> 0 then
        wwdsList.DataSet.Recno := i;
    finally
       wwdsList.DataSet.EnableControls;
    end;
    Close := False;
    GetDataSetsToReopen(aDS, Close);

    for i := Low(aDS) to High(aDS) do
    begin
      if Close or
         (aDS[i].ClientID >= 0) and
          (aDS[i].ClientID <> wwdsList.DataSet.FieldByName('CL_NBR').AsInteger) or
         (aDS[i].AsOfDate <> ctx_DataAccess.AsOfDate) or
         (aDS[i].OpenCondition <> GetDataSetConditions(aDS[i].TableName)) then
      begin
        aDS[i].Close;
      end;

      if aDS[i].Name = 'CO' then
        if wwdsList.DataSet.Name = 'TMP_CO' then
          HasCompany := True;
    end;

    SetDataSetsProps(aDS, wwdsList.DataSet as TevClientDataSet);
    ctx_DataAccess.OpenClient(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger);
    FCachedClientReadOnly := Null;
    ApplySecurity;
    SetClientReadOnlyFlag(false);
    ctx_DataAccess.OpenDataSets(aDS);

    if ctx_Statistics.Enabled then
    begin
      ctx_Statistics.ActiveEvent.Properties.AddValue('CL_NBR', wwdsList.DataSet.FieldByName('CL_NBR').AsString);
      ctx_Statistics.EndEvent;
    end;

    if DM_CLIENT.CL.Active then
      (Owner as TFramePackageTmpl).AutoSave(DM_CLIENT.CL.FieldByName('AUTO_SAVE_MINUTES').AsInteger);

    bClReadOnly := false;
    if ctx_DataAccess.GetClReadOnlyRight then
    begin
       if FClientReadOnlyFlag = READ_ONLY_MAINTENANCE then
          EvErrMessage('Your account is set to Maintenance Hold.')
       else
          EvErrMessage('Your account is set to Read Only.');
       SetReadOnly(True);
       bClReadOnly := True;
    end;

    if HasCompany and (DM_COMPANY.CO.RecordCount > 0) then
    begin
      case DM_COMPANY.CO.FieldByName('CREDIT_HOLD').AsString[1] of
      CREDIT_HOLD_LEVEL_LOW:
        EvMessage('Your account is past due! Please, contact Accounts Receivable.', mtInformation, [mbOK]);
      CREDIT_HOLD_LEVEL_MEDIUM:
        EvMessage('Your account is seriously past due! You will not be able to process payrolls until this issue is resolved. Please, contact Accounts Receivable.', mtWarning, [mbOK]);
      CREDIT_HOLD_LEVEL_HIGH:
      begin
        EvErrMessage('Due to outstanding A/R issues you no longer have access to your company. Please, contact Accounts Receivable immediately.');
        if ctx_AccountRights.Functions.GetState('UNLOCK_CREDIT_HOLD') <> stEnabled then
        begin
          for i := Low(aDS) to High(aDS) do
            aDS[i].Close;
          DM_COMPANY.CO.ClientID := 0;
          Exit;
        end;
      end;
      CREDIT_HOLD_LEVEL_MAINT:
      begin
        EvErrMessage('Client is under maintenance and can not be accessed at this time');
        if CheckReadOnlyCompanyFunction then
        begin
          for i := Low(aDS) to High(aDS) do
            aDS[i].Close;
          DM_COMPANY.CO.ClientID := 0;
          Exit;
        end;
      end;
      CREDIT_HOLD_LEVEL_READONLY:
      begin
         if (not bClReadOnly) and ctx_DataAccess.GetCoReadOnlyRight then
         begin
           EvErrMessage('Your account is set to Read Only.');
           SetReadOnly(True);
         end;
      end;
      end;
    end;

    if BitBtn1.Enabled then
      if HasCompany and (DM_COMPANY.CO.RecordCount > 0) and
         (DM_COMPANY.CO.FieldByName('PAYROLL_PASSWORD').AsString <> '') then
        EvMessage('Payroll Password: ' + DM_COMPANY.CO.FieldByName('PAYROLL_PASSWORD').AsString);

    wwdsList.DoDataChange(Self, nil);

    b := (not GetIfReadOnly) and  ((not BitBtn1.Enabled and (wwdsList.DataSet.RecordCount > 0)) or
        (TevClientDataSet(wwdsMaster.DataSet).State in [dsInsert,dsEdit]));
    (Owner as TFramePackageTmpl).SetButton(NavInsert, b);
    (Owner as TFramePackageTmpl).SetButton(NavDelete, b);

    AfterDataSetsReopen;

    if not IgnoreAutoJump and not BitBtn1.Enabled and
       (mb_AppSettings['Settings\AutoJump'] = 'Y') then
      PageControl1.SelectNextPage(True);
  end;
end;

function TEDIT_OPEN_BASE.BitBtn1Enabled: Boolean;
begin
  result :=(wwdsList.DataSet.RecordCount > 0) and
                     (((wwdsList.KeyValue <> wwdsMaster.KeyValue) and wwdsMaster.DataSet.Active) or
                      (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger <>  ctx_DataAccess.ClientID) or
                      (ctx_DataAccess.ClientID = 0));
end;

procedure TEDIT_OPEN_BASE.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  inherited;

  if not Assigned(wwdsMaster.DataSet) then
    Exit;

  BitBtn1.Enabled := BitBtn1Enabled;
end;


procedure TEDIT_OPEN_BASE.SetReadOnly(Value: Boolean);
begin
  inherited;
  BitBtn1.SecurityRO := False;
end;

procedure TEDIT_OPEN_BASE.Activate;
var
  SwipeClockAPIKey: IisListOfValues;
  MRCAPIKey: IisListOfValues;
  b: Boolean;
begin
  inherited;
  PageControl1.ActivePage := TabSheet1;

  b := (not GetIfReadOnly) and  ((not BitBtn1.Enabled and (wwdsList.DataSet.RecordCount > 0)) or
       (TevClientDataSet(wwdsMaster.DataSet).State in [dsInsert,dsEdit]));
  (Owner as TFramePackageTmpl).SetButton(NavInsert, b);
  (Owner as TFramePackageTmpl).SetButton(NavDelete, b);

  SwipeClockAPIKey := Context.License.FindAPIKey(tvkEvoSwipeClockAdvanced);
  btnSwipeClock.Visible := Assigned(SwipeClockAPIKey) and
   (ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_SWIPECLOCK') = stEnabled);

  MRCAPIKey := Context.License.FindAPIKey(tvkEvoMyRecruitingCenter);
  btnMRCLogIn.Visible := Assigned(MRCAPIKey) and
  (ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_MY_RECRUITING_CENTER') = stEnabled);
end;

procedure TEDIT_OPEN_BASE.wwdbgridSelectClientKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    BitBtn1.Click;
    Key := 0;
  end;
end;

procedure TEDIT_OPEN_BASE.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(GetDefaultDataSet, aDS);
end;

procedure TEDIT_OPEN_BASE.SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(aDS) to High(aDS) do
    if not aDS[i].Active then
    begin
      aDS[i].ClientID := ListDataSet.FieldByName('CL_NBR').AsInteger;
      aDS[i].AsOfDate := ctx_DataAccess.AsOfDate;
      aDS[i].RetrieveCondition := GetDataSetConditions(aDS[i].TableName);
    end;
end;

procedure TEDIT_OPEN_BASE.AfterDataSetsReopen;
begin
end;

procedure TEDIT_OPEN_BASE.CheckStatus;
begin

  with Owner as TFramePackageTmpl do
  begin
      if ButtonEnabled(NavOK) then
        ClickButton(NavOK);
  end;
end;

procedure TEDIT_OPEN_BASE.Deactivate;
begin
  CheckStatus;
  inherited;
end;

procedure TEDIT_OPEN_BASE.CommitData;
begin
  CheckStatus;
  with Owner as TFramePackageTmpl do
    if ButtonEnabled(NavCommit) then
      ClickButton(NavCommit);
end;

procedure TEDIT_OPEN_BASE.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if (wwdsList.DataSet.RecordCount = 0) then
    AllowChange := False
  else
  begin
    if BitBtn1Enabled then
       BitBtn1.Click;
    if HasCompany and not DM_COMPANY.CO.Active then
      AllowChange := False;
  end;
end;

function TEDIT_OPEN_BASE.IgnoreAutoJump: Boolean;
begin
  Result := False;
end;

procedure TEDIT_OPEN_BASE.spbFavoriteReportsClick(Sender: TObject);
begin
  inherited;
  if BitBtn1Enabled then
    Exit;

  if Assigned(DM_COMPANY.CO) and
     (DM_COMPANY.CO.Active = True) and
     ( DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) then
  begin
    TCO_FavoriteReports.execute (Trim(DM_COMPANY.CO.FieldByName('CO_NBR').Asstring),
                      Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString));
  end;
end;

function TEDIT_OPEN_BASE.ClientReadOnlyFlag: Char;
begin
  Result := FClientReadOnlyFlag;
end;

procedure TEDIT_OPEN_BASE.SetClientReadOnlyFlag(const bROFlag : boolean);
begin
  FClientReadOnlyFlag := ctx_DBAccess.GetClientROFlag;
  bClientReadOnlyFlag := bROFlag;
end;

procedure TEDIT_OPEN_BASE.BtnMRCLogInClick(Sender: TObject);
var
  sURL: String;
begin
  if (DM_COMPANY.CO.Active) and (DM_COMPANY.CO.CO_NBR.AsInteger > 0) then
  begin
    ctx_StartWait('Please wait ...');
    try
      sURL := Context.SingleSignOn.GetMRCOneTimeURL(DM_COMPANY.CO.CO_NBR.AsInteger);
      if Trim(sURL) <> '' then
         RunIsolatedProcess(sURL, '', [])
      else
         EvErrMessage('No URL from Server Side.');
    finally
      ctx_EndWait;
    end;
  end
  else
    ShowMessage('Please open company!');
end;

procedure TEDIT_OPEN_BASE.btnSwipeClockClick(Sender: TObject);
var
  sURL: String;
begin
  if (DM_COMPANY.CO.Active) and (DM_COMPANY.CO.CO_NBR.AsInteger > 0) then
  begin
    ctx_StartWait('Please wait ...');
    try
      sURL := Context.SingleSignOn.GetSwipeclockOneTimeURL(DM_COMPANY.CO.CO_NBR.AsInteger);
      if Trim(sURL) <> '' then
         RunIsolatedProcess(sURL, '', [])
      else
         EvErrMessage('No URL from Server Side.');
    finally
      ctx_EndWait;
    end;
  end
  else
    ShowMessage('Please open company!');
end;

procedure TEDIT_OPEN_BASE.AfterConstruction;
begin
  inherited;
  FCachedClientReadOnly := Null;
end;

end.
