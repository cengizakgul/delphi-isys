// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_DESCR_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_TINY_BASE, StdCtrls, Mask, wwdbedit,  Db, Wwdatsrc,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ISBasicClasses, EvUIComponents,
  isUIwwDBEdit;

type
  TEDIT_DESCR_BASE = class(TEDIT_TINY_BASE)
    evLabel1: TevLabel;
    dedtDescription: TevDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;

  end;

implementation

{$R *.DFM}
function TEDIT_DESCR_BASE.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

end.
