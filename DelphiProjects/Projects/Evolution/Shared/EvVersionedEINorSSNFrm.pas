unit EvVersionedEINorSSNFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, Windows, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem, isUIwwDBComboBox, DBCtrls, Messages;

type
  TevVersionedEINorSSN = class(TevVersionedFieldBase)
    pnlBottom: TevPanel;
    isUIFashionPanel1: TisUIFashionPanel;
    lablW2_Social_Security_Number: TevLabel;
    edSocial_Security_Number: TevDBEdit;
    rg_SSN_EIN: TevDBRadioGroup;
    procedure rg_SSN_EINChange(Sender: TObject);
    procedure btnNavInsertClick(Sender: TObject);
  protected
    procedure DoOnSave; override;
  end;

implementation

type
  SSNMaskType = (SSN, EIN);

const
  aSSNMask: array[SSNMaskType] of string = ('*3{#}-*2{#}-*4{#}', '*2{#}-*7{#}');

{$R *.dfm}

{ TevVersionedEINorSSN }

procedure TevVersionedEINorSSN.DoOnSave;
var
 LV: IisListOfValues;
begin
  inherited;
  LV := TisListOfValues.Create;
  LV.AddValue('EIN_OR_SOCIAL_SECURITY_NUMBER', rg_SSN_EIN.Value);
  LV.AddValue('SOCIAL_SECURITY_NUMBER', edSocial_Security_Number.text);
  ctx_DBAccess.UpdateFieldVersion('CL_PERSON', Nbr, EmptyDate, EmptyDate, Data['begin_date'], DayBeforeEndOfTime, LV);
end;

procedure TevVersionedEINorSSN.rg_SSN_EINChange(Sender: TObject);

  procedure ChangeMask(C: TCustomEdit; PictMask: string);
    function DelSubstr(const S, SubStr: string) : string;
    begin
      result := S;
      while pos(SubStr, result) > 0 do
        Delete(result, pos(SubStr, result), Length(SubStr));
    end;
  var
    S :string;
    i:integer;
  begin
    if (C is TwwDBEdit) then
    begin
      with TwwDBEdit(C) do
        if Assigned(DataSource) and Assigned(DataSource.DataSet) and
            Assigned(Field) and DataSource.DataSet.Active then
        begin
          S:= Field.AsString;
          S := DelSubstr(S, '-');
          Clear;
          Picture.PictureMask := PictMask;
          for i:= 1 to length(S) do
          begin
            SendMessage(Handle, WM_Char, word(S[i]), 0);
          end;
          if DataSource.DataSet.State in [dsEdit,dsInsert] then
            Field.Value := Text;
        end;
    end;
  end;
begin
   inherited;
   with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
       (Field.DataSet.State in [dsEdit, dsInsert]) then
    begin
      with (Sender as TevDBRadioGroup)  do
      begin
        if Value = GROUP_BOX_SSN then
          ChangeMask(edSocial_Security_Number, aSSNMask[SSN])
        else
          ChangeMask(edSocial_Security_Number, aSSNMask[EIN])
      end
    end
    else
    begin
        if Value = GROUP_BOX_SSN then
          edSocial_Security_Number.Picture.PictureMask := aSSNMask[SSN]
        else
          edSocial_Security_Number.Picture.PictureMask := aSSNMask[EIN];
    end;
end;

procedure TevVersionedEINorSSN.btnNavInsertClick(Sender: TObject);
var
  Q: IevQuery;
begin
  inherited;
  Q := TevQuery.Create('SELECT t.EIN_OR_SOCIAL_SECURITY_NUMBER, t.SOCIAL_SECURITY_NUMBER FROM CL_PERSON t WHERE {AsOfNow<t>} AND t.CL_PERSON_NBR = :nbr');
  Q.Params.AddValue('nbr', Nbr);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
  begin
     dsFieldData.DataSet['EIN_OR_SOCIAL_SECURITY_NUMBER']:= Q.Result['EIN_OR_SOCIAL_SECURITY_NUMBER'];
     dsFieldData.DataSet['SOCIAL_SECURITY_NUMBER']:= Q.Result['SOCIAL_SECURITY_NUMBER'];
  end;
end;

end.

