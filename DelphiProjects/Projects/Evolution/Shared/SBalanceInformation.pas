// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SBalanceInformation;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,  DB,
  EvUtils, SDataStructure, EvConsts, EvTypes, Variants, EvContext,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictsystem, EvBasicUtils,
  EvCommonInterfaces, EvDataSet, EvUIComponents, EvClientDataSet, Types, isBaseClasses;

const
  Margin = 2.0;

type
  TBalanceDM = class(TDataModule)
    cdsSummary: TevClientDataSet;
    cdsCoList: TevClientDataSet;
    Provider: TisProvider;
    cdsAllEarnings: TevClientDataSet;
    cdsAllMemos: TevClientDataSet;
    cdsTips: TevClientDataSet;
    cdsGTL: TevClientDataSet;
    cdsAggCheckLines: TevClientDataSet;
    cdsW2Box: TevClientDataSet;
    cdsCheckLines: TevClientDataSet;
    cdsFederalEExempt: TevClientDataSet;
    cdsFederalDExempt: TevClientDataSet;
    cdsFederalTax: TevClientDataSet;
    cdsEICEExempt: TevClientDataSet;
    cdsEICDExempt: TevClientDataSet;
    cdsEEOASDIEExempt: TevClientDataSet;
    cdsEEOASDIDExempt: TevClientDataSet;
    cdsEEMedicareEExempt: TevClientDataSet;
    cdsEEMedicareDExempt: TevClientDataSet;
    cdsEROASDIEExempt: TevClientDataSet;
    cdsEROASDIDExempt: TevClientDataSet;
    cdsERMedicareEExempt: TevClientDataSet;
    cdsERMedicareDExempt: TevClientDataSet;
    cdsFUIEExempt: TevClientDataSet;
    cdsFUIDExempt: TevClientDataSet;
    cdsChecks: TevClientDataSet;
    cdsAggChecks: TevClientDataSet;
    cdsLiab: TevClientDataSet;
    cdsAggLiab: TevClientDataSet;
    cdsFederalLiab: TevClientDataSet;
    cdsEICLiab: TevClientDataSet;
    cdsEEOASDILiab: TevClientDataSet;
    cdsEEMedicareLiab: TevClientDataSet;
    cdsERMedicareLiab: TevClientDataSet;
    cdsEROASDILiab: TevClientDataSet;
    cdsFUILiab: TevClientDataSet;
    cdsBackupWithholdingLiab: TevClientDataSet;
    cdsFederal1099: TevClientDataSet;
    cds3Party: TevClientDataSet;
    cds3PartyFederalLiab: TevClientDataSet;
    cds3PartyEEOASDILiab: TevClientDataSet;
    cds3PartyEROASDILiab: TevClientDataSet;
    cdsAggCheckStates: TevClientDataSet;
    cdsCheckStates: TevClientDataSet;
    cdsAggStateLiab: TevClientDataSet;
    cdsStateLiab: TevClientDataSet;
    cdsStateTaxLiab: TevClientDataSet;
    cdsStateTax: TevClientDataSet;
    cdsEESDILiab: TevClientDataSet;
    cdsERSDILiab: TevClientDataSet;
    cdsState1099: TevClientDataSet;
    cdsCheckLineStates: TevClientDataSet;
    cdsAggCheckLineStates: TevClientDataSet;
    cdsStateDExempt: TevClientDataSet;
    cdsStateEExempt: TevClientDataSet;
    cdsEESDIEExempt: TevClientDataSet;
    cdsEESDIDExempt: TevClientDataSet;
    cdsERSDIDExempt: TevClientDataSet;
    cdsERSDIEExempt: TevClientDataSet;
    cdsEESUIEExempt: TevClientDataSet;
    cdsEESUIDExempt: TevClientDataSet;
    cdsERSUIDExempt: TevClientDataSet;
    cdsERSUIEExempt: TevClientDataSet;
    cdsCheckLineSUI: TevClientDataSet;
    cdsAggCheckSUI: TevClientDataSet;
    cdsCheckSUI: TevClientDataSet;
    cdsAggSUILiab: TevClientDataSet;
    cdsSUILiab: TevClientDataSet;
    cdsEESUITax: TevClientDataSet;
    cdsEESUILiab: TevClientDataSet;
    cdsERSUILiab: TevClientDataSet;
    cdsSUILiabCL_NBR: TIntegerField;
    cdsSUILiabCUSTOM_CLIENT_NUMBER: TStringField;
    cdsSUILiabCLIENT_NAME: TStringField;
    cdsSUILiabCO_NBR: TIntegerField;
    cdsSUILiabCUSTOM_COMPANY_NUMBER: TStringField;
    cdsSUILiabCOMPANY_NAME: TStringField;
    cdsSUILiabPR_NBR: TIntegerField;
    cdsSUILiabRUN_NUMBER: TIntegerField;
    cdsSUILiabCHECK_DATE: TDateField;
    cdsSUILiabAMOUNT: TFloatField;
    cdsSUILiabCO_STATES_NBR: TIntegerField;
    cdsSUILiabSTATE: TStringField;
    cdsSUILiabSY_SUI_NBR: TIntegerField;
    cdsSUILiabEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    cdsEESUIMemo: TevClientDataSet;
    cdsERSUIMemo: TevClientDataSet;
    cdsEESDI1099: TevClientDataSet;
    cdsEESUI1099: TevClientDataSet;
    cdsERSUI1099: TevClientDataSet;
    cdsERSDI1099: TevClientDataSet;
    cdsCheckSUICL_NBR: TIntegerField;
    cdsCheckSUICUSTOM_CLIENT_NUMBER: TStringField;
    cdsCheckSUICLIENT_NAME: TStringField;
    cdsCheckSUICO_NBR: TIntegerField;
    cdsCheckSUICUSTOM_COMPANY_NUMBER: TStringField;
    cdsCheckSUICOMPANY_NAME: TStringField;
    cdsCheckSUIPR_NBR: TIntegerField;
    cdsCheckSUIRUN_NUMBER: TIntegerField;
    cdsCheckSUICHECK_DATE: TDateField;
    cdsCheckSUIPR_CHECK_NBR: TIntegerField;
    cdsCheckSUIPAYMENT_SERIAL_NUMBER: TIntegerField;
    cdsCheckSUICHECK_TYPE: TStringField;
    cdsCheckSUICL_PERSON_NBR: TIntegerField;
    cdsCheckSUIFIRST_NAME: TStringField;
    cdsCheckSUIMIDDLE_INITIAL: TStringField;
    cdsCheckSUILAST_NAME: TStringField;
    cdsCheckSUISOCIAL_SECURITY_NUMBER: TStringField;
    cdsCheckSUIEE_NBR: TIntegerField;
    cdsCheckSUICUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdsCheckSUICOMPANY_OR_INDIVIDUAL_NAME: TStringField;
    cdsCheckSUICO_STATES_NBR: TIntegerField;
    cdsCheckSUISTATE: TStringField;
    cdsCheckSUISUI_TAXABLE_WAGES: TFloatField;
    cdsCheckSUISUI_TAX: TFloatField;
    cdsCheckSUISUI_GROSS_WAGES: TFloatField;
    cdsCheckSUIDESCRIPTION: TStringField;
    cdsCheckSUISY_SUI_NBR: TIntegerField;
    cdsCheckSUIEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField;
    cdsERSUITax: TevClientDataSet;
    cdsSummaryClNbr: TIntegerField;
    cdsSummaryCoNbr: TIntegerField;
    cdsSummarySortKey: TStringField;
    cdsSummaryTextDescription: TStringField;
    cdsSummaryAmountWages: TCurrencyField;
    cdsSummaryAmountTaxHist: TCurrencyField;
    cdsSummaryAmountTaxLiab: TCurrencyField;
    cdsSummaryWagesDSName: TStringField;
    cdsSummaryLiabDSName: TStringField;
    cdsSummaryDifLiabHist: TCurrencyField;
    cdsSummaryDifWageHist: TCurrencyField;
    cdsCoSum: TevClientDataSet;
    cdsCoSumCoNbr: TIntegerField;
    cdsCoSumClNbr: TIntegerField;
    cdsCoSumOutBalance: TBooleanField;
    cdsCheckLocal: TevClientDataSet;
    cdsAggCheckLocal: TevClientDataSet;
    cdsAggLocalLiab: TevClientDataSet;
    cdsLocalLiab: TevClientDataSet;
    cdsCheckLineLocal: TevClientDataSet;
    cdsAggCheckLineLocal: TevClientDataSet;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    cdsLocalTaxLiab: TevClientDataSet;
    cdsLocalTax: TevClientDataSet;
    cdsLocalDExempt: TevClientDataSet;
    cdsLocalEExempt: TevClientDataSet;
    cdsLocal1099: TevClientDataSet;
    cdsCheckLineLocalCL_NBR: TIntegerField;
    cdsCheckLineLocalCUSTOM_CLIENT_NUMBER: TStringField;
    cdsCheckLineLocalCLIENT_NAME: TStringField;
    cdsCheckLineLocalCO_NBR: TIntegerField;
    cdsCheckLineLocalCUSTOM_COMPANY_NUMBER: TStringField;
    cdsCheckLineLocalCOMPANY_NAME: TStringField;
    cdsCheckLineLocalPR_CHECK_LINES_NBR: TIntegerField;
    cdsCheckLineLocalAMOUNT: TFloatField;
    cdsCheckLineLocalPR_NBR: TIntegerField;
    cdsCheckLineLocalRUN_NUMBER: TIntegerField;
    cdsCheckLineLocalCHECK_DATE: TDateField;
    cdsCheckLineLocalPR_CHECK_NBR: TIntegerField;
    cdsCheckLineLocalPAYMENT_SERIAL_NUMBER: TIntegerField;
    cdsCheckLineLocalCHECK_TYPE: TStringField;
    cdsCheckLineLocalCL_PERSON_NBR: TIntegerField;
    cdsCheckLineLocalFIRST_NAME: TStringField;
    cdsCheckLineLocalMIDDLE_INITIAL: TStringField;
    cdsCheckLineLocalLAST_NAME: TStringField;
    cdsCheckLineLocalSOCIAL_SECURITY_NUMBER: TStringField;
    cdsCheckLineLocalEE_NBR: TIntegerField;
    cdsCheckLineLocalCUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdsCheckLineLocalCL_E_DS_NBR: TIntegerField;
    cdsCheckLineLocalCUSTOM_E_D_CODE_NUMBER: TStringField;
    cdsCheckLineLocalE_D_CODE_TYPE: TStringField;
    cdsCheckLineLocalDESCRIPTION: TStringField;
    cdsCheckLineLocalSY_LOCALS_NBR: TIntegerField;
    cdsCheckLineLocalEXEMPT_EXCLUDE: TStringField;
    cdsCheckLineLocalPR_CHECK_LINE_LOCALS_NBR: TIntegerField;
    cdsCheckLineLocalLocal_Name: TStringField;
    cdsCheckLineLocalState: TStringField;
    cdsCheckLocalLocal_Name: TStringField;
    cdsCheckLocalState: TStringField;
    cdsCheckLocalCL_NBR: TIntegerField;
    cdsCheckLocalCUSTOM_CLIENT_NUMBER: TStringField;
    cdsCheckLocalCLIENT_NAME: TStringField;
    cdsCheckLocalCO_NBR: TIntegerField;
    cdsCheckLocalCUSTOM_COMPANY_NUMBER: TStringField;
    cdsCheckLocalCOMPANY_NAME: TStringField;
    cdsCheckLocalPR_NBR: TIntegerField;
    cdsCheckLocalRUN_NUMBER: TIntegerField;
    cdsCheckLocalCHECK_DATE: TDateField;
    cdsCheckLocalPR_CHECK_NBR: TIntegerField;
    cdsCheckLocalPAYMENT_SERIAL_NUMBER: TIntegerField;
    cdsCheckLocalCHECK_TYPE: TStringField;
    cdsCheckLocalCL_PERSON_NBR: TIntegerField;
    cdsCheckLocalFIRST_NAME: TStringField;
    cdsCheckLocalMIDDLE_INITIAL: TStringField;
    cdsCheckLocalLAST_NAME: TStringField;
    cdsCheckLocalSOCIAL_SECURITY_NUMBER: TStringField;
    cdsCheckLocalEE_NBR: TIntegerField;
    cdsCheckLocalCUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdsCheckLocalCOMPANY_OR_INDIVIDUAL_NAME: TStringField;
    cdsCheckLocalEE_LOCALS_NBR: TIntegerField;
    cdsCheckLocalCO_LOCAL_TAX_NBR: TIntegerField;
    cdsCheckLocalSY_LOCALS_NBR: TIntegerField;
    cdsCheckLocalLOCAL_TAXABLE_WAGE: TFloatField;
    cdsCheckLocalLOCAL_TAX: TFloatField;
    cdsLocalLiabLocal_Name: TStringField;
    cdsLocalLiabState: TStringField;
    cdsLocalLiabCL_NBR: TIntegerField;
    cdsLocalLiabCUSTOM_CLIENT_NUMBER: TStringField;
    cdsLocalLiabCLIENT_NAME: TStringField;
    cdsLocalLiabCO_NBR: TIntegerField;
    cdsLocalLiabCUSTOM_COMPANY_NUMBER: TStringField;
    cdsLocalLiabCOMPANY_NAME: TStringField;
    cdsLocalLiabPR_NBR: TIntegerField;
    cdsLocalLiabRUN_NUMBER: TIntegerField;
    cdsLocalLiabCHECK_DATE: TDateField;
    cdsLocalLiabAMOUNT: TFloatField;
    cdsLocalLiabSY_LOCALS_NBR: TIntegerField;
    cdsSummaryClName: TStringField;
    cdsSummaryCoName: TStringField;
    cdsSummaryClNumber: TStringField;
    cdsSummaryCoNumber: TStringField;
    cdsCoSumClName: TStringField;
    cdsCoSumCoName: TStringField;
    cdsCoSumClNumber: TStringField;
    cdsCoSumCoNumber: TStringField;
    cdsSummaryConsolidation: TBooleanField;
    cdsCoSumConsolidation: TBooleanField;
    cdsSummaryTaxPercent: TFloatField;
    cdsW2BoxCL_NBR: TIntegerField;
    cdsW2BoxCUSTOM_CLIENT_NUMBER: TStringField;
    cdsW2BoxCLIENT_NAME: TStringField;
    cdsW2BoxCO_NBR: TIntegerField;
    cdsW2BoxCUSTOM_COMPANY_NUMBER: TStringField;
    cdsW2BoxCOMPANY_NAME: TStringField;
    cdsW2BoxPR_CHECK_LINES_NBR: TIntegerField;
    cdsW2BoxAMOUNT: TFloatField;
    cdsW2BoxPR_NBR: TIntegerField;
    cdsW2BoxRUN_NUMBER: TIntegerField;
    cdsW2BoxCHECK_DATE: TDateField;
    cdsW2BoxPR_CHECK_NBR: TIntegerField;
    cdsW2BoxPAYMENT_SERIAL_NUMBER: TIntegerField;
    cdsW2BoxCHECK_TYPE: TStringField;
    cdsW2BoxCL_PERSON_NBR: TIntegerField;
    cdsW2BoxFIRST_NAME: TStringField;
    cdsW2BoxMIDDLE_INITIAL: TStringField;
    cdsW2BoxLAST_NAME: TStringField;
    cdsW2BoxSOCIAL_SECURITY_NUMBER: TStringField;
    cdsW2BoxEE_NBR: TIntegerField;
    cdsW2BoxCUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdsW2BoxCL_E_DS_NBR: TIntegerField;
    cdsW2BoxCUSTOM_E_D_CODE_NUMBER: TStringField;
    cdsW2BoxE_D_CODE_TYPE: TStringField;
    cdsW2BoxDESCRIPTION: TStringField;
    cdsW2BoxW2_BOX: TStringField;
    cdsW2BoxEE_EXEMPT_EXCLUDE_FEDERAL: TStringField;
    cdsW2BoxEE_EXEMPT_EXCLUDE_EIC: TStringField;
    cdsW2BoxEE_EXEMPT_EXCLUDE_OASDI: TStringField;
    cdsW2BoxEE_EXEMPT_EXCLUDE_MEDICARE: TStringField;
    cdsW2BoxER_EXEMPT_EXCLUDE_OASDI: TStringField;
    cdsW2BoxER_EXEMPT_EXCLUDE_MEDICARE: TStringField;
    cdsW2BoxER_EXEMPT_EXCLUDE_FUI: TStringField;
    cdsW2BoxW2_BOX_CALC: TStringField;
    cdsSummaryTaxAmount: TCurrencyField;
    cdsCheckLineSUICL_NBR: TIntegerField;
    cdsCheckLineSUICUSTOM_CLIENT_NUMBER: TStringField;
    cdsCheckLineSUICLIENT_NAME: TStringField;
    cdsCheckLineSUICO_NBR: TIntegerField;
    cdsCheckLineSUICUSTOM_COMPANY_NUMBER: TStringField;
    cdsCheckLineSUICOMPANY_NAME: TStringField;
    cdsCheckLineSUIPR_CHECK_LINES_NBR: TIntegerField;
    cdsCheckLineSUIAMOUNT: TFloatField;
    cdsCheckLineSUIPR_NBR: TIntegerField;
    cdsCheckLineSUIRUN_NUMBER: TIntegerField;
    cdsCheckLineSUICHECK_DATE: TDateField;
    cdsCheckLineSUIPR_CHECK_NBR: TIntegerField;
    cdsCheckLineSUIPAYMENT_SERIAL_NUMBER: TIntegerField;
    cdsCheckLineSUICHECK_TYPE: TStringField;
    cdsCheckLineSUICL_PERSON_NBR: TIntegerField;
    cdsCheckLineSUIFIRST_NAME: TStringField;
    cdsCheckLineSUIMIDDLE_INITIAL: TStringField;
    cdsCheckLineSUILAST_NAME: TStringField;
    cdsCheckLineSUISOCIAL_SECURITY_NUMBER: TStringField;
    cdsCheckLineSUIEE_NBR: TIntegerField;
    cdsCheckLineSUICUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdsCheckLineSUISTATE: TStringField;
    cdsCheckLineSUICL_E_DS_NBR: TIntegerField;
    cdsCheckLineSUICUSTOM_E_D_CODE_NUMBER: TStringField;
    cdsCheckLineSUIE_D_CODE_TYPE: TStringField;
    cdsCheckLineSUIDESCRIPTION: TStringField;
    cdsCheckLineSUISY_STATES_NBR: TIntegerField;
    cdsCheckLineSUIEMPLOYEE_EXEMPT_EXCLUDE_SUI: TStringField;
    cdsCheckLineSUIEMPLOYER_EXEMPT_EXCLUDE_SUI: TStringField;
    cdsCheckLineSUISY_SUI_NBR: TIntegerField;
    cdsCheckLineSUIEE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField;
    cdsSummaryMargin: TFloatField;
    cdsCoSumMargin: TFloatField;
    cdsLiabCL_NBR: TIntegerField;
    cdsLiabCUSTOM_CLIENT_NUMBER: TStringField;
    cdsLiabCLIENT_NAME: TStringField;
    cdsLiabCO_NBR: TIntegerField;
    cdsLiabCUSTOM_COMPANY_NUMBER: TStringField;
    cdsLiabCOMPANY_NAME: TStringField;
    cdsLiabPR_NBR: TIntegerField;
    cdsLiabRUN_NUMBER: TIntegerField;
    cdsLiabCHECK_DATE: TDateField;
    cdsLiabTAX_TYPE: TStringField;
    cdsLiabAMOUNT: TFloatField;
    cdsLiabTHIRD_PARTY: TStringField;
    cdsStateLiabCL_NBR: TIntegerField;
    cdsStateLiabCUSTOM_CLIENT_NUMBER: TStringField;
    cdsStateLiabCLIENT_NAME: TStringField;
    cdsStateLiabCO_NBR: TIntegerField;
    cdsStateLiabCUSTOM_COMPANY_NUMBER: TStringField;
    cdsStateLiabCOMPANY_NAME: TStringField;
    cdsStateLiabPR_NBR: TIntegerField;
    cdsStateLiabRUN_NUMBER: TIntegerField;
    cdsStateLiabCHECK_DATE: TDateField;
    cdsStateLiabTAX_TYPE: TStringField;
    cdsStateLiabAMOUNT: TFloatField;
    cdsStateLiabCO_STATES_NBR: TIntegerField;
    cdsStateLiabSTATE: TStringField;
    cdsCoSumOutBalWages: TBooleanField;
    cdsSummaryDontCheck: TBooleanField;
    cdsSummaryMNSui: TIntegerField;
    cdsSummaryAmountEE_Tax_: TCurrencyField;
    cdsSummaryAmountER_Tax_: TCurrencyField;
    cdsSummaryDifWageHist_EE_ER: TCurrencyField;
    cdsCoSumOutBalance_: TBooleanField;
    cdsCoSumOutBalance_EE_ER: TBooleanField;
    cdsSummarySkipBalanceControl: TBooleanField;
    cdsCoSumbTaxWageLimit: TBooleanField;
    cdsSummaryFICAEE_Wages_: TCurrencyField;
    cdsSummaryFICADifWageHist_: TCurrencyField;
    cdEEMedicare: TevClientDataSet;
    cdEEMedicareCO_NBR: TIntegerField;
    cdEEMedicareCL_PERSON_NBR: TIntegerField;
    cdEEMedicareMAKEUP_FICA_ON_CLEANUP_PR: TStringField;
    cdEEMedicareEeMedicateWagesTotal: TFloatField;
    cdEEMedicareEeMedicateWagesTotalQuarter: TFloatField;

    procedure cdsSummaryCalcFields(DataSet: TDataSet);
    procedure cdsW2BoxCalcFields(DataSet: TDataSet);
    procedure cdsLiabCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    QEEMedY, QEEMedYCO, QEEMedQ, QEEMedicarePerson, QEEY: IevQuery;
    nvSuiPeriodEndDate : TDateTime;
    WagesOOB: array of record
      CL_NBR: Integer;
      CO_NBR: Integer;
    end;
    function WagesOutOfBalance(const ClNbr, CoNbr: Integer): Boolean;
    function IsTaxableType(EDType: string): Boolean;
    procedure EFed(DataSet: TDataSet; var Accept: Boolean);
    procedure DFed(DataSet: TDataSet; var Accept: Boolean);
    procedure DEIC(DataSet: TDataSet; var Accept: Boolean);
    procedure EEIC(DataSet: TDataSet; var Accept: Boolean);
    procedure DEEMedicare(DataSet: TDataSet; var Accept: Boolean);
    procedure DEEOASDI(DataSet: TDataSet; var Accept: Boolean);
    procedure DERMedicare(DataSet: TDataSet; var Accept: Boolean);
    procedure DEROASDI(DataSet: TDataSet; var Accept: Boolean);
    procedure DFUI(DataSet: TDataSet; var Accept: Boolean);
    procedure EEEMedicare(DataSet: TDataSet; var Accept: Boolean);
    procedure EEEOASDI(DataSet: TDataSet; var Accept: Boolean);
    procedure EERMedicare(DataSet: TDataSet; var Accept: Boolean);
    procedure EEROASDI(DataSet: TDataSet; var Accept: Boolean);
    procedure EFUI(DataSet: TDataSet; var Accept: Boolean);
    procedure DEESDI(DataSet: TDataSet; var Accept: Boolean);
    procedure DEESUI(DataSet: TDataSet; var Accept: Boolean);
    procedure DERSDI(DataSet: TDataSet; var Accept: Boolean);
    procedure DERSUI(DataSet: TDataSet; var Accept: Boolean);
    procedure DState(DataSet: TDataSet; var Accept: Boolean);
    procedure EEESDI(DataSet: TDataSet; var Accept: Boolean);
    procedure EEESUI(DataSet: TDataSet; var Accept: Boolean);
    procedure EERSDI(DataSet: TDataSet; var Accept: Boolean);
    procedure EERSUI(DataSet: TDataSet; var Accept: Boolean);
    procedure EState(DataSet: TDataSet; var Accept: Boolean);
    procedure MEESUI(DataSet: TDataSet; var Accept: Boolean);
    procedure MERSUI(DataSet: TDataSet; var Accept: Boolean);
    procedure DLocal(DataSet: TDataSet; var Accept: Boolean);
    procedure ELocal(DataSet: TDataSet; var Accept: Boolean);
    procedure W2Filter(DataSet: TDataSet; var Accept: Boolean);
  end;

  TCompanyDescr = record
    ClNbr: Integer;
    CoNbr: Integer;
    Consolidation: Boolean;
  end;

function GetBalanceInformation(AOwner: TComponent; const Co: array of TCompanyDescr; BeginDate, EndDate: TDateTime): TBalanceDM;

var
  dmBalanceQ, dmBalanceA: TBalanceDM;

implementation

uses DateUtils;

{$R *.DFM}


procedure FixupLookupFields(const cd: TevClientDataSet); overload;
var
  i, iCurrentThreadID: Integer;
  lcd: TevClientDataSet;
begin
  iCurrentThreadID := Integer(GetCurrentThreadID);
  for i := 0 to cd.FieldCount-1 do
    if Assigned(cd.Fields[i].LookupDataSet)
    and (cd.Fields[i].LookupDataSet is TevClientDataSet) then
    begin
      lcd := TevClientDataSet(cd.Fields[i].LookupDataSet);
      if (lcd.Owner is TDataModule)
      and (lcd.Owner.Name = 'DM_'+ lcd.Name) then
        if lcd.Owner.Tag <> iCurrentThreadID then
          cd.Fields[i].LookupDataSet := ctx_DataAccess.GetDataSet(GetddTableClassByName(lcd.Name));
    end;
end;

procedure FixupLookupFields(const cd: TDataModule); overload;
var
  i: Integer;
begin
  for i := 0 to cd.ComponentCount-1 do
    if cd.Components[i] is TevClientDataSet then
      FixupLookupFields(TevClientDataSet(cd.Components[i]));
end;


function GetBalanceInformation(AOwner: TComponent; const Co: array of TCompanyDescr; BeginDate, EndDate: TDateTime): TBalanceDM;
var
  tCoList: TStringList;
  i: Integer;
  s: string;
  dsName: String;
  ds: TevClientDataSet;
  d: TDateTime;
  AddTax: real;
  Nbr: Integer;
  EeCount: Integer;

  procedure AddLine(sDesc, sTag: String; Co_Nbr: Integer;
                    AmountWages, AmountTaxHist, AmountTaxLiab, Percent, AmountEE_Tax_, AmountER_Tax_, FICAEE_Wages_ : Variant;
                    WagesDSName, LiabDSName: string; const dontCheck: Boolean = False; const MNSui: Integer = 0;
                    const SkipBalanceControl: Boolean = False);
  var
    t: TevClientDataSet;
  begin
    t := TevClientDataSet.Create(nil);
    with Result do
    try
      t.Data := cdsCoList.Data;
      t.Filter := 'ClNbr = ' + IntToStr(ctx_DataAccess.ClientID) + ' and CoNbr = ' + IntToStr(Co_Nbr);
      t.Filtered := True;
      t.First;
      while not t.Eof do
      begin
        if not cdsSummary.Locate('ClNbr;CoNbr;SortKey;Consolidation', VarArrayOf([ctx_DataAccess.ClientID, t['MainCoNbr'], sTag, t['Consolidation']]), []) then
        begin
          cdsSummary.Insert;
          cdsSummary['ClNbr'] := ctx_DataAccess.ClientID;
          cdsSummary['ClName'] := DM_CLIENT.CL['NAME'];
          cdsSummary['ClNumber'] := DM_CLIENT.CL['CUSTOM_CLIENT_NUMBER'];
          cdsSummary['CoNbr'] := t['MainCoNbr'];
          cdsSummary['Consolidation'] := (t['MainCoNbr'] <> Co_Nbr) or t['Consolidation'];
          if DM_COMPANY.CO.Locate('CO_NBR', Co_Nbr, []) then
          begin
            cdsSummary['CoName'] := DM_COMPANY.CO['NAME'];
            cdsSummary['CoNumber'] := DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'];
            cdsSummary['Margin'] := DM_COMPANY.CO['MINIMUM_TAX_THRESHOLD'];
          end;
          if VarIsNull(cdsSummary['Margin']) and (MNSui <> 0) then
            cdsSummary['Margin'] := Margin + 2.0;
          cdsSummary['TextDescription'] := sDesc;
          cdsSummary['SortKey'] := sTag;
          cdsSummary['WagesDSName'] := WagesDSName;
          cdsSummary['LiabDSName'] := LiabDSName;
          cdsSummary['TaxPercent'] := Percent * 100;
          cdsSummary['AmountWages'] := AmountWages;
          cdsSummary['AmountTaxHist'] := AmountTaxHist;

          cdsSummary['AmountEE_Tax_'] := AmountEE_Tax_;
          cdsSummary['AmountER_Tax_'] := AmountER_Tax_;

          cdsSummary['FICAEE_Wages_'] := FICAEE_Wages_;

          cdsSummary['AmountTaxLiab'] := AmountTaxLiab;
          cdsSummary['DontCheck'] := dontCheck;
          cdsSummary['MNSui'] := MNSui;
          cdsSummary['SkipBalanceControl'] := SkipBalanceControl;
        end
        else
        begin
          cdsSummary.Edit;
          cdsSummary['AmountWages'] := cdsSummary.FieldByName('AmountWages').AsCurrency + AmountWages;
          cdsSummary['AmountTaxHist'] := cdsSummary.FieldByName('AmountTaxHist').AsCurrency + AmountTaxHist;
          cdsSummary['AmountEE_Tax_'] := cdsSummary.FieldByName('AmountEE_Tax_').AsCurrency + AmountEE_Tax_;
          cdsSummary['AmountER_Tax_'] := cdsSummary.FieldByName('AmountER_Tax_').AsCurrency + AmountER_Tax_;
          cdsSummary['FICAEE_Wages_'] := cdsSummary.FieldByName('FICAEE_Wages_').AsCurrency + FICAEE_Wages_;

          cdsSummary['AmountTaxLiab'] := cdsSummary.FieldByName('AmountTaxLiab').AsCurrency + AmountTaxLiab;
        end;
        cdsSummary.Post;

        t.Next;
      end;
    finally
      t.Free;
    end;
  end;

  procedure AddToSummary(sDesc, sTag, WagesDSName: String);
  var
    i: Integer;
  begin
    for i := 0 to Pred(tCoList.Count) do
      if Result.cdsAggCheckLines.Locate('Co_Nbr', tCoList[i], []) then
      begin
        Result.cdsAggCheckLines.SetRange([tCoList[i]], [tCoList[i]]);
        try
          AddLine(sDesc, sTag, StrToInt(tCoList[i]), Result.cdsAggCheckLines.Aggregates.Find('Summary').Value, null, null, null, null, Null, null, WagesDSName, '');
        finally
          Result.cdsAggCheckLines.CancelRange;
        end;
      end
      else
        AddLine(sDesc, sTag, StrToInt(tCoList[i]), null, null, null, null, null, null, Null,  WagesDSName, '');
  end;

  procedure AddStatesToSummary(sDesc, sTag, WagesDSName: String);
  var
    i: Integer;
    Co_Nbr: Integer;
    State: string;
  begin
    with Result do
    begin
      cdsAggCheckLineStates.First;
      while not cdsAggCheckLineStates.Eof do
      begin
        i := cdsAggCheckLineStates.RecNo;
        cdsAggCheckLineStates.SetRange([cdsAggCheckLineStates['Co_Nbr'], cdsAggCheckLineStates['State']],
                                       [cdsAggCheckLineStates['Co_Nbr'], cdsAggCheckLineStates['State']]);
        try
          AddLine(cdsAggCheckLineStates.FieldByName('State').AsString + ' - ' + sDesc,
                  '2' + cdsAggCheckLineStates.FieldByName('State').AsString + sTag, cdsAggCheckLineStates['Co_Nbr'],
                  cdsAggCheckLineStates.Aggregates.Find('Summary').Value, null, null, null, null, null, Null,  WagesDSName, '');
        finally
          cdsAggCheckLineStates.CancelRange;
          cdsAggCheckLineStates.RecNo := i;
        end;

        Co_Nbr := cdsAggCheckLineStates['Co_Nbr'];
        State := cdsAggCheckLineStates['State'];
        while not cdsAggCheckLineStates.Eof and
              (Co_Nbr = cdsAggCheckLineStates['Co_Nbr']) and
              (State = cdsAggCheckLineStates['State']) do
          cdsAggCheckLineStates.Next;
      end;
    end;
  end;

  procedure AddTaxLine(sDesc, sTag, WagesField, TaxField, EE_Field, ER_Field, FICAField : String; Percent: Variant; WagesDSName, LiabDSName: string; bNoLiab: Boolean = False; SkipBalanceControl: Boolean = False);
  var
    i: Integer;
    AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent: Variant;
    AmountEE_Tax_, AmountER_Tax_, FICAEE_Wages_: Variant;

  begin
    for i := 0 to Pred(tCoList.Count) do
      with Result do
      begin
        AmountWages := null;
        AmountTaxHist := null;
        TaxPercent := null;
        AmountEE_Tax_:= null;
        AmountER_Tax_ := null;
        FICAEE_Wages_ := null;

        if cdsAggChecks.Locate('Co_Nbr', tCoList[i], []) then
        begin
          cdsAggChecks.SetRange([tCoList[i]], [tCoList[i]]);
          try
            if cdsAggChecks.Aggregates.IndexOf(WagesField) >= 0 then
              AmountWages := cdsAggChecks.Aggregates.Find(WagesField).Value;
            if cdsAggChecks.Aggregates.IndexOf(TaxField) >= 0 then
              AmountTaxHist := cdsAggChecks.Aggregates.Find(TaxField).Value;

            if Length(Trim(EE_Field)) >= 0 then
            if cdsAggChecks.Aggregates.IndexOf(EE_Field) >= 0 then
              AmountEE_Tax_ := cdsAggChecks.Aggregates.Find(EE_Field).Value;

            if Length(Trim(ER_Field)) >= 0 then
            if cdsAggChecks.Aggregates.IndexOf(ER_Field) >= 0 then
              AmountER_Tax_ := cdsAggChecks.Aggregates.Find(ER_Field).Value;

            if Length(Trim(FICAField)) >= 0 then
            if cdsAggChecks.Aggregates.IndexOf(FICAField) >= 0 then
              FICAEE_Wages_ := cdsAggChecks.Aggregates.Find(FICAField).Value;

            TaxPercent := Percent;
          finally
            cdsAggChecks.CancelRange;
          end;
        end;
        if not bNoLiab and cdsAggLiab.Locate('Co_Nbr', tCoList[i], []) then
        begin
          cdsAggLiab.SetRange([tCoList[i]], [tCoList[i]]);
          try
            AmountTaxLiab := cdsAggLiab.Aggregates.Find('Summary').Value;
          finally
            cdsAggLiab.CancelRange;
          end;
        end
        else if not bNoLiab then
          AmountTaxLiab := 0
        else
          AmountTaxLiab := null;
        AddLine(sDesc, sTag, StrToInt(tCoList[i]), AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent,
                        AmountEE_Tax_, AmountER_Tax_, FICAEE_Wages_,
                         WagesDSName, LiabDSName, False, 0, SkipBalanceControl);
      end;
  end;

  procedure AddStateTaxLines(sDesc, sTag, WagesField, TaxField, PercentField, WagesDSName, LiabDSName: string; bNoLiab: Boolean = False);
  var
    i: Integer;
    AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent: Variant;
    Co_Nbr: Integer;
    State: string;
  begin
    with Result do
    begin
      cdsAggCheckStates.First;
      cdsAggStateLiab.First;

      while not cdsAggCheckStates.Eof or not cdsAggStateLiab.Eof do
      begin
        AmountWages := null;
        AmountTaxHist := null;
        TaxPercent := null;
        AmountTaxLiab := null;
        Co_Nbr := 0;
        if not cdsAggCheckStates.Eof and
           (cdsAggStateLiab.Eof or
            (cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger < cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger = cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckStates.FieldByName('State').AsString <= cdsAggStateLiab.FieldByName('State').AsString)) then
        begin
          Co_Nbr := cdsAggCheckStates['Co_Nbr'];
          State := cdsAggCheckStates['State'];
          if PercentField <> '' then
            TaxPercent := DM_SYSTEM_STATE.SY_STATES.Lookup('State', State, PercentField);
          i := cdsAggCheckStates.RecNo;
          cdsAggCheckStates.SetRange([Co_Nbr, State], [Co_Nbr, State]);
          try
            if cdsAggCheckStates.Aggregates.IndexOf(WagesField) >= 0 then
              AmountWages := cdsAggCheckStates.Aggregates.Find(WagesField).Value;
            if cdsAggCheckStates.Aggregates.IndexOf(TaxField) >= 0 then
              AmountTaxHist := cdsAggCheckStates.Aggregates.Find(TaxField).Value;
          finally
            cdsAggCheckStates.CancelRange;
            cdsAggCheckStates.RecNo := i;
          end;
        end;
        if not cdsAggStateLiab.Eof and
           (cdsAggCheckStates.Eof or
            (cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger > cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger = cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckStates.FieldByName('State').AsString >= cdsAggStateLiab.FieldByName('State').AsString)) then
        begin
          Co_Nbr := cdsAggStateLiab['Co_Nbr'];
          State := cdsAggStateLiab['State'];
          if PercentField <> '' then
            TaxPercent := DM_SYSTEM_STATE.SY_STATES.Lookup('State', State, PercentField);
          i := cdsAggStateLiab.RecNo;
          cdsAggStateLiab.SetRange([Co_Nbr, State], [Co_Nbr, State]);
          try
            if bNoLiab then
              AmountTaxLiab := null
            else
              AmountTaxLiab := cdsAggStateLiab.Aggregates.Find('Summary').Value;
          finally
            cdsAggStateLiab.CancelRange;
            cdsAggStateLiab.RecNo := i;
          end;
        end;
        if (State <> 'NM') or (Pos(' SDI ', sDesc) = 0) then
          AddLine(State + ' - ' + sDesc, '2' + State + sTag, Co_nbr, AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent, Null, Null, Null, WagesDSName, LiabDSName);

        while not cdsAggStateLiab.Eof and
            ((cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggStateLiab.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggStateLiab.FieldByName('State').AsString <= State)) do
          cdsAggStateLiab.Next;

        while not cdsAggCheckStates.Eof and
            ((cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggCheckStates.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggCheckStates.FieldByName('State').AsString <= State)) do
          cdsAggCheckStates.Next;
      end;
    end;
  end;

  procedure AddSUITaxLines(sDesc, sTag, WagesField, TaxField, PercentField, WagesDSName, LiabDSName: string; bNoLiab: Boolean = False);
   Function GetNVSuiLiabAmount(bDate, eDate : TDateTime): Double;
   var
    Suisql : String;
    SuiQ : IevQuery;
   begin
        Result := 0;
        Suisql :='select sum(b.AMOUNT) amount from  CO_SUI a, CO_SUI_LIABILITIES b, PR c ' +
                 'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                  '{AsOfNow<c>} and c.PR_NBR= b.PR_NBR and ' +
                  'b.CO_SUI_NBR = a.CO_SUI_NBR and c.PAYROLL_TYPE = ''T'' and ' +
                   'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                   'a.SY_SUI_NBR = 118 and c.CO_NBR = :CO_NBR';
            SuiQ := TevQuery.Create(Suisql);
            SuiQ.Params.AddValue('CO_NBR', DM_COMPANY.CO_SUI['CO_NBR']);
            SuiQ.Params.AddValue('BeginDate', bDate);
            SuiQ.Params.AddValue('EndDate', eDate);
            SuiQ.Execute;
            if SuiQ.Result.RecordCount > 0 then
              Result:= SuiQ.Result.Fields[0].AsFloat;
   end;
  var
    i: Integer;
    Sort: string;
    Co_Nbr, Sy_Sui_Nbr, tmpSy_Sui_Nbr: Integer;
    AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent: Variant;
    AWages: Variant;
    List: string;
    consolidation_nbr  : integer;
  begin
    with Result do
    begin
      cdsAggCheckSUI.First;
      cdsAggSUILiab.First;

      while not cdsAggCheckSUI.Eof or not cdsAggSUILiab.Eof do
      begin
        AWages := null;
        AmountWages := null;
        AmountTaxHist := null;
        TaxPercent := null;
        AmountTaxLiab := null;
        Co_Nbr := 0;
        Sy_Sui_nbr := 0;
        if not cdsAggCheckSUI.Eof and
           (cdsAggSUILiab.Eof or
            (cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger < cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger = cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckSUI.FieldByName('Sy_Sui_Nbr').AsInteger <= cdsAggSUILiab.FieldByName('Sy_Sui_Nbr').AsInteger)) then
        begin
          Co_Nbr := cdsAggCheckSUI['Co_Nbr'];
          Sy_Sui_Nbr := cdsAggCheckSUI['Sy_Sui_Nbr'];
          i := cdsAggCheckSUI.RecNo;
          cdsAggCheckSUI.SetRange([Co_Nbr, Sy_Sui_Nbr], [Co_Nbr, Sy_Sui_Nbr]);
          try
            if cdsAggCheckSUI['State'] = 'IL' then
               AWages := cdsAggCheckSUI.Aggregates.Find('SUI_GROSS_WAGES_Sum').Value;
            if cdsAggCheckSUI.Aggregates.IndexOf(WagesField) >= 0 then
              AmountWages := cdsAggCheckSUI.Aggregates.Find(WagesField).Value;
            if cdsAggCheckSUI.Aggregates.IndexOf(TaxField) >= 0 then
              AmountTaxHist := cdsAggCheckSUI.Aggregates.Find(TaxField).Value;
          finally
            cdsAggCheckSUI.CancelRange;
            cdsAggCheckSUI.RecNo := i;
          end;
        end;
        if not cdsAggSUILiab.Eof and
           (cdsAggCheckSUI.Eof or
            (cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger > cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger = cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckSUI.FieldByName('Sy_Sui_Nbr').AsInteger >= cdsAggSUILiab.FieldByName('Sy_Sui_Nbr').AsInteger)) then
        begin
          Co_Nbr := cdsAggSUILiab['Co_Nbr'];
          Sy_Sui_Nbr := cdsAggSUILiab['Sy_Sui_Nbr'];
          i := cdsAggSUILiab.RecNo;
          cdsAggSUILiab.SetRange([Co_Nbr, Sy_Sui_Nbr], [Co_Nbr, Sy_Sui_Nbr]);
          try
            if bNoLiab then
              AmountTaxLiab := null
            else
              AmountTaxLiab := cdsAggSUILiab.Aggregates.Find('Summary').Value;
          finally
            cdsAggSUILiab.CancelRange;
            cdsAggSUILiab.RecNo := i;
          end;
        end;

        if (PercentField <> '') and
           DM_COMPANY.CO_SUI.Locate('Co_Nbr;Sy_Sui_Nbr', VarArrayOf([Co_Nbr, Sy_Sui_Nbr]), []) then
        begin
          if DM_COMPANY.CO_SUI['State'] = 'IL' then
          begin
            with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
            begin
              SetMacro('TableName', 'CO');
              SetMacro('Columns', 'CL_CO_CONSOLIDATION_NBR');
              SetMacro('NbrField', 'CO');
              SetParam('RecordNbr', Co_Nbr);
              ctx_DataAccess.CUSTOM_VIEW.Close;
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              ctx_DataAccess.CUSTOM_VIEW.Open;
            end;
            consolidation_nbr := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['CL_CO_CONSOLIDATION_NBR'], 0);

            if consolidation_nbr > 0 then
            begin
              with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
              begin
                SetMacro('Columns', 'CO_NBR');
                SetMacro('Table1', 'CO');
                SetMacro('Table2', 'CL_CO_CONSOLIDATION');
                SetMacro('JoinField', 'CL_CO_CONSOLIDATION');
                SetMacro('NbrField', 'PRIMARY_CO');
                SetParam('RecordNbr', Co_Nbr);
                ctx_DataAccess.CUSTOM_VIEW.Close;
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                ctx_DataAccess.CUSTOM_VIEW.Open;
              end;
              if not ctx_DataAccess.CUSTOM_VIEW.Fields[0].IsNull then
                Co_Nbr := ctx_DataAccess.CUSTOM_VIEW['CO_NBR'];
            end;

            Nbr := DM_COMPANY.CO_SUI.Recno;
            DM_COMPANY.CO_SUI.Filter := 'CO_NBR = '+IntToStr(Co_Nbr) + ' and State = ''IL'' and FINAL_TAX_RETURN = ''N''';
            DM_COMPANY.CO_SUI.Filtered := True;
            try
              while not DM_COMPANY.CO_SUI.Eof do
              begin
                if DM_SYSTEM_STATE.SY_SUI.Locate('Sy_Sui_Nbr', DM_COMPANY.CO_SUI['Sy_Sui_Nbr'], []) and
                   (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_ER_OTHER) then
                  if DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.IsNull then
                    AddTax := AddTax + DM_COMPANY.CO_SUI[PercentField]
                  else
                    AddTax := AddTax + DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.Value;
                DM_COMPANY.CO_SUI.Next;
              end;
            finally
              DM_COMPANY.CO_SUI.Filter := '';
              DM_COMPANY.CO_SUI.Filtered := False;
              DM_COMPANY.CO_SUI.Recno := Nbr;
            end;

            if consolidation_nbr > 0  then
            begin
              with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
              begin
                SetMacro('TableName', 'CO');
                SetMacro('Columns', 'CO_NBR');
                SetMacro('NbrField', 'CL_CO_CONSOLIDATION');
                SetParam('RecordNbr', consolidation_nbr);
                ctx_DataAccess.CUSTOM_VIEW.Close;
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                ctx_DataAccess.CUSTOM_VIEW.Open;
              end;

              if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 1 then
              begin
                List := '';
                while not ctx_DataAccess.CUSTOM_VIEW.Eof do
                begin
                  List := List + ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsString + ',';
                  ctx_DataAccess.CUSTOM_VIEW.Next;
                end;
                Delete(List, Length(List), 1);

                with TExecDSWrapper.Create('BR_CheckSUIInfo') do
                begin
                  SetMacro('CoList', List);
                  SetParam('BeginDate', BeginDate);
                  SetParam('EndDate', EndDate);
                  ctx_DataAccess.CUSTOM_VIEW.Close;
                  ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                  ctx_DataAccess.CUSTOM_VIEW.Open;
                end;

                AWages := 0;
                while not ctx_DataAccess.CUSTOM_VIEW.Eof do
                begin
                  AWages := AWages + ctx_DataAccess.CUSTOM_VIEW.FieldByName('SUI_GROSS_WAGES').AsFloat;
                  ctx_DataAccess.CUSTOM_VIEW.Next;
                end;
              end;
            end;
          end;
          Assert(DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', DM_COMPANY.CO_SUI['SY_SUI_NBR'], []));

          if DM_COMPANY.CO_SUI['SY_SUI_NBR'] = 58 then // MA Healthcare
          begin
            if EeCount = -1 then
              with ctx_DataAccess.CUSTOM_VIEW do
              begin
                EeCount := 0;
                if Active then
                  Close;
                with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                begin
                  SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate), 12));
                  SetParam('CoNbr', DM_COMPANY.CO_SUI['CO_NBR']);
                  SetMacro('SUINbrValues', '58');
                  DataRequest(AsVariant);
                end;
                Open;
                First;
                EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                Close;

                with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                begin
                  SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 1, 12));
                  SetParam('CoNbr', DM_COMPANY.CO_SUI['CO_NBR']);
                  SetMacro('SUINbrValues', '58');
                  DataRequest(AsVariant);
                end;
                Open;
                First;
                EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                Close;

                with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                begin
                  SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 2, 12));
                  SetParam('CoNbr', DM_COMPANY.CO_SUI['CO_NBR']);
                  SetMacro('SUINbrValues', '58');
                  DataRequest(AsVariant);
                end;
                Open;
                First;
                EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                Close;
              end;
              if EeCount / 3 < 6 then
                TaxPercent := 0
              else if not DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.IsNull then
                TaxPercent := DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.Value
              else
                TaxPercent := DM_COMPANY.CO_SUI[PercentField];
          end
          else if (DM_COMPANY.CO_SUI['State'] = 'IL') then
          begin
             if (AWages >= 50000) or (DM_COMPANY.CO_SUI[PercentField] < 0.054 - AddTax) then
             begin
                if not DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.IsNull then
                   TaxPercent := DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.Value
                else
                   TaxPercent := DM_COMPANY.CO_SUI[PercentField]
             end
             else
                   TaxPercent := 0.054 - AddTax;
          end
          else
          begin
            if not DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.IsNull then
                TaxPercent := DM_SYSTEM_STATE.SY_SUI.GLOBAL_RATE.Value
              else
                TaxPercent := DM_COMPANY.CO_SUI[PercentField]
          end;
        end
        else
          TaxPercent := null;

        tmpSy_Sui_Nbr := 0;
        Assert(DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', DM_COMPANY.CO_SUI['SY_SUI_NBR'], []));
        if DM_COMPANY.CO_SUI['SY_SUI_NBR'] = 118 then // NV SUI - Busines Tax
        begin
         if (GetYear(EndDate) = 2009) and
             ((GetQuarterNumber(EndDate) = 3) or (GetQuarterNumber(EndDate) = 4)) then
         begin
             tmpSy_Sui_Nbr := Sy_Sui_Nbr;
             AmountTaxHist := AmountTaxHist + GetNVSuiLiabAmount(GetBeginQuarter(EndDate),EndDate);
         end
         else
         if (CompareDate(EndDate, EncodeDate(2015,  6, 30)) = GreaterThanValue) and  (WagesField = 'SUI_TAXABLE_WAGES_Sum') then
         begin
           tmpSy_Sui_Nbr := Sy_Sui_Nbr;
           AmountTaxHist := 0;
           if (AmountWages > 50000) then
              AmountTaxHist := (AmountWages - 50000.00) * 0.01475;
         end
         else
         if (CompareDate(EndDate, EncodeDate(2013,  6, 30)) = GreaterThanValue) and  (WagesField = 'SUI_TAXABLE_WAGES_Sum') then
         begin
           tmpSy_Sui_Nbr := Sy_Sui_Nbr;
           AmountTaxHist := 0;
           if (AmountWages > 85000) then
              AmountTaxHist := (AmountWages - 85000.00) * 0.0117;
         end
         else
         if (EndDate >= 40725) and  (WagesField = 'SUI_TAXABLE_WAGES_Sum') then
         begin
             tmpSy_Sui_Nbr := Sy_Sui_Nbr;
             AmountTaxHist := 0;
             if (AmountWages > 62500) then
                AmountTaxHist := (AmountWages - 62500.00) * 0.0117;
         end;
        end;

        Sort := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', Sy_Sui_Nbr, 'SUI_TAX_NAME');
        if not (Sy_Sui_Nbr in [123, 124, 130]) then // NM SUI Workers Comp
          AddLine(Sort + ' - ' + sDesc, '2' + Sort + sTag, Co_Nbr, AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent, Null, Null, Null, WagesDSName, LiabDSName, DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', Sy_Sui_Nbr, 'E_D_CODE_TYPE') <> null, Iff(DM_COMPANY.CO_SUI['State'] = 'MN', Sy_Sui_Nbr, tmpSy_Sui_Nbr));

        while not cdsAggSUILiab.Eof and
            ((cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggSUILiab.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggSUILiab.FieldByName('Sy_Sui_Nbr').AsInteger <= Sy_Sui_Nbr)) do
          cdsAggSUILiab.Next;

        while not cdsAggCheckSUI.Eof and
            ((cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggCheckSUI.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggCheckSUI.FieldByName('Sy_Sui_Nbr').AsInteger <= Sy_Sui_Nbr)) do
          cdsAggCheckSUI.Next;
      end;
    end;
  end;

  procedure AddLocalsToSummary(sDesc, sTag, WagesDSName: String);
  var
    i: Integer;
    Co_Nbr, Sy_Locals_Nbr: Integer;
  begin
    with Result do
    begin
      cdsAggCheckLineLocal.First;
      while not cdsAggCheckLineLocal.Eof do
      begin
        i := cdsAggCheckLineLocal.RecNo;
        cdsAggCheckLineLocal.SetRange([cdsAggCheckLineLocal['Co_Nbr'], cdsAggCheckLineLocal['Sy_Locals_Nbr']],
                                      [cdsAggCheckLineLocal['Co_Nbr'], cdsAggCheckLineLocal['Sy_Locals_Nbr']]);
        try
          AddLine(cdsAggCheckLineLocal['State'] + ' - ' + cdsAggCheckLineLocal['Local_Name'] + ' - ' + sDesc,
                  '2' + cdsAggCheckLineLocal['State'] + #250 + Format('%15.15d', [cdsAggCheckLineLocal.FieldByName('Sy_Locals_Nbr').AsInteger]) + #250 + sTag,
                  cdsAggCheckLineLocal['Co_Nbr'], cdsAggCheckLineLocal.Aggregates.Find('Summary').Value, null, null, Null, null, Null, Null, WagesDSName, '')
        finally
          cdsAggCheckLineLocal.CancelRange;
          cdsAggCheckLineLocal.RecNo := i;
        end;

        Co_Nbr := cdsAggCheckLineLocal['Co_Nbr'];
        Sy_Locals_Nbr := cdsAggCheckLineLocal['Sy_Locals_Nbr'];
        while not cdsAggCheckLineLocal.Eof and
              (Co_Nbr = cdsAggCheckLineLocal['Co_Nbr']) and
              (Sy_Locals_Nbr = cdsAggCheckLineLocal['Sy_Locals_Nbr']) do
          cdsAggCheckLineLocal.Next;
      end;
    end;
  end;

  procedure AddLocalTaxLines(sDesc, sTag, WagesField, TaxField, WagesDSName, LiabDSName: string; bNoLiab: Boolean = False);
  var
    i: Integer;
    AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent: Variant;
    Co_Nbr, Sy_Locals_Nbr: Integer;
    ds: TevClientDataSet;
  begin
    with Result do
    begin
      cdsAggCheckLocal.First;
      cdsAggLocalLiab.First;

      while not cdsAggCheckLocal.Eof or not cdsAggLocalLiab.Eof do
      begin
        AmountWages := null;
        AmountTaxHist := null;
        TaxPercent := null;
        AmountTaxLiab := null;
        Co_Nbr := 0;
        Sy_Locals_Nbr := 0;
        if not cdsAggCheckLocal.Eof and
           (cdsAggLocalLiab.Eof or
            (cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger < cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger = cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckLocal.FieldByName('Sy_Locals_Nbr').AsInteger <= cdsAggLocalLiab.FieldByName('Sy_Locals_Nbr').AsInteger)) then
        begin
          Co_Nbr := cdsAggCheckLocal['Co_Nbr'];
          Sy_Locals_Nbr := cdsAggCheckLocal['Sy_Locals_Nbr'];
          i := cdsAggCheckLocal.RecNo;
          cdsAggCheckLocal.SetRange([Co_Nbr, Sy_Locals_Nbr], [Co_Nbr, Sy_Locals_Nbr]);
          try
            if cdsAggCheckLocal.Aggregates.IndexOf(WagesField) >= 0 then
              AmountWages := cdsAggCheckLocal.Aggregates.Find(WagesField).Value;
            if cdsAggCheckLocal.Aggregates.IndexOf(TaxField) >= 0 then
              AmountTaxHist := cdsAggCheckLocal.Aggregates.Find(TaxField).Value;
          finally
            cdsAggCheckLocal.CancelRange;
            cdsAggCheckLocal.RecNo := i;
          end;
        end;
        if not cdsAggLocalLiab.Eof and
           (cdsAggCheckLocal.Eof or
            (cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger > cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger) or
            (cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger = cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger) and
            (cdsAggCheckLocal.FieldByName('Sy_Locals_Nbr').AsInteger >= cdsAggLocalLiab.FieldByName('Sy_Locals_Nbr').AsInteger)) then
        begin
          Co_Nbr := cdsAggLocalLiab['Co_Nbr'];
          Sy_Locals_Nbr := cdsAggLocalLiab['Sy_Locals_Nbr'];
          i := cdsAggLocalLiab.RecNo;
          cdsAggLocalLiab.SetRange([Co_Nbr, Sy_Locals_Nbr], [Co_Nbr, Sy_Locals_Nbr]);
          try
            if bNoLiab then
              AmountTaxLiab := null
            else
              AmountTaxLiab := cdsAggLocalLiab.Aggregates.Find('Summary').Value;
          finally
            cdsAggLocalLiab.CancelRange;
            cdsAggLocalLiab.RecNo := i;
          end;
        end;
        if not cdsAggCheckLocal.Eof then
          ds := cdsAggCheckLocal
        else
          ds := cdsAggLocalLiab;
        AddLine(ds['State'] + ' - ' + ds['Local_Name'] + ' - ' + sDesc,
                '2' + ds['State'] + #250 + Format('%15.15d', [Sy_Locals_Nbr]) + #250 + sTag,
                Co_Nbr, AmountWages, AmountTaxHist, AmountTaxLiab, TaxPercent, Null, Null, Null, WagesDSName, LiabDSName, DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', Sy_Locals_Nbr, 'E_D_CODE_TYPE') <> null);

        while not cdsAggLocalLiab.Eof and
            ((cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggLocalLiab.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggLocalLiab.FieldByName('Sy_Locals_Nbr').AsInteger <= Sy_Locals_Nbr)) do
          cdsAggLocalLiab.Next;

        while not cdsAggCheckLocal.Eof and
            ((cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger < Co_Nbr) or
             (cdsAggCheckLocal.FieldByName('Co_Nbr').AsInteger = Co_Nbr) and
             (cdsAggCheckLocal.FieldByName('Sy_Locals_Nbr').AsInteger <= Sy_Locals_Nbr)) do
          cdsAggCheckLocal.Next;
      end;
    end;
  end;

var
  acc: array of Currency;
  bTaxWageLimit: Boolean;
  clNbr, coNbr, eeNbr, entNbr: Integer;
  fMargin: real;
  sql : String;
  Q: IevQuery;
  SkipBalanceControl : boolean;

  ListClPerson: IisStringList;

begin
  Result := TBalanceDM.Create(AOwner);
  Result.nvSuiPeriodEndDate := EndDate;
  FixupLookupFields(Result);
//  ConnectToSpdServer;
  tCoList := TStringList.Create;
  bTaxWageLimit := false;
  ctx_StartWait;
  with Result do
  try
    cdsSummary.CreateDataSet;
    cdsCoList.CreateDataSet;
    cdEEMedicare.CreateDataSet;

// Create the list of companies

    for i := Low(Co) to High(Co) do
    begin
      if not cdsCoList.Locate('ClNbr;CoNbr;MainCoNbr;Consolidation', VarArrayOf([Co[i].ClNbr, Co[i].CoNbr, Co[i].CoNbr, Co[i].Consolidation]), []) then
      begin
        cdsCoList.Insert;
        cdsCoList['ClNbr'] := Co[i].ClNbr;
        cdsCoList['CoNbr'] := Co[i].CoNbr;
        cdsCoList['MainCoNbr'] := Co[i].CoNbr;
        cdsCoList['Consolidation'] := Co[i].Consolidation;
        cdsCoList.Post;
      end;
      if Co[i].Consolidation then
      begin
        with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
        begin
          ctx_DataAccess.OpenClient(Co[i].ClNbr);
          SetMacro('Columns', 'CO_NBR');
          SetMacro('Table1', 'CO');
          SetMacro('Table2', 'CL_CO_CONSOLIDATION');
          SetMacro('JoinField', 'CL_CO_CONSOLIDATION');
          SetMacro('NbrField', 'PRIMARY_CO');
          SetParam('RecordNbr', Co[i].CoNbr);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          if not cdsCoList.Locate('ClNbr;CoNbr;MainCoNbr;Consolidation', VarArrayOf([Co[i].ClNbr, ctx_DataAccess.CUSTOM_VIEW['CO_NBR'], Co[i].CoNbr, False]), []) then
          begin
            cdsCoList.Insert;
            cdsCoList['ClNbr'] := Co[i].ClNbr;
            cdsCoList['CoNbr'] := ctx_DataAccess.CUSTOM_VIEW['CO_NBR'];
            cdsCoList['MainCoNbr'] := Co[i].CoNbr;
            cdsCoList['Consolidation'] := False;
            cdsCoList.Post;
          end;
          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;
      end;
    end;

    cdsCoList.First;
    while not cdsCoList.Eof do
    begin
      EeCount := -1;

// Open a client and create the condition for all selected companies for this client

      ctx_DataAccess.OpenClient(cdsCoList['ClNbr']);
      DM_CLIENT.CL.CheckDSConditionAndClient(cdsCoList['ClNbr'], '');
      DM_COMPANY.CO.CheckDSConditionAndClient(cdsCoList['ClNbr'], '');
      DM_COMPANY.CO_SUI.CheckDSConditionAndClient(cdsCoList['ClNbr'], '');
      DM_PAYROLL.PR.CheckDSConditionAndClient(cdsCoList['ClNbr'], '');
      tCoList.Clear;
      while not cdsCoList.Eof and (cdsCoList['ClNbr'] = ctx_DataAccess.ClientID) do
      begin
        tCoList.Add(cdsCoList.FieldByName('CoNbr').AsString);
        cdsCoList.Next;
      end;

// Let's go
      d := Trunc(EndDate) + 1 - 1/86400;

      DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.CheckDSCondition('');
      with TExecDSWrapper.Create('BR_CheckLinesByEDType') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckLines.Close;
        cdsCheckLines.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckWagesTaxes') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsChecks.Close;
        cdsChecks.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_FedLiab') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsLiab.Close;
        cdsLiab.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckStateInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckStates.Close;
        cdsCheckStates.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_StateLiab') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsStateLiab.Close;
        cdsStateLiab.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckStateLinesInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckLineStates.Close;
        cdsCheckLineStates.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckSUILinesInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckLineSUI.Close;
        cdsCheckLineSUI.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckSUIInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckSUI.Close;
        cdsCheckSUI.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_SUILiab') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsSUILiab.Close;
        cdsSUILiab.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckLocalLinesInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckLineLocal.Close;
        cdsCheckLineLocal.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_CheckLocalInfo') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsCheckLocal.Close;
        cdsCheckLocal.DataRequest(AsVariant);
      end;

      with TExecDSWrapper.Create('BR_LocalLiab') do
      begin
        SetMacro('CoList', tCoList.CommaText);
        SetParam('BeginDate', BeginDate);
        SetParam('EndDate', EndDate);
        cdsLocalLiab.Close;
        cdsLocalLiab.DataRequest(AsVariant);
      end;

      DM_SYSTEM_STATE.SY_STATES.AsOfDate := Trunc(d);
      DM_SYSTEM_STATE.SY_SUI.AsOfDate := Trunc(d);
      DM_COMPANY.CO_SUI.AsOfDate := Trunc(d);
      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.AsOfDate := Trunc(d);

      ctx_DataAccess.OpenDataSets([DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS, DM_SYSTEM_STATE.SY_STATES,
                    DM_SYSTEM_STATE.SY_SUI, DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS,
                    DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS, DM_SYSTEM_LOCAL.SY_LOCALS, DM_COMPANY.CO_SUI,
                    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE, DM_COMPANY.CO, DM_CLIENT.CL, DM_PAYROLL.PR]);
      ctx_DataAccess.OpenDataSets([cdsCheckLines, cdsChecks, cdsLiab, cdsCheckStates, cdsStateLiab,
                    cdsCheckLineStates, cdsCheckLineSUI, cdsCheckSUI, cdsSUILiab,
                    cdsCheckLineLocal, cdsCheckLocal, cdsLocalLiab]);

      with cdsChecks do
      begin
        IndexFieldNames := 'CL_NBR;CO_NBR;EE_NBR';
        First;
        clNbr := FieldByName('CL_NBR').AsInteger;
        coNbr := FieldByName('CO_NBR').AsInteger;
        eeNbr := FieldByName('EE_NBR').AsInteger;
        SetLength(acc, 6);
        for i := 0 to High(acc) do
          acc[i] := 0;

        while not Eof do
        begin
          acc[0] := acc[0] + FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat;
          acc[1] := acc[1] + FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat;
          acc[2] := acc[2] + FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat;
          acc[3] := acc[3] + FieldByName('ER_OASDI_TAXABLE_WAGES').AsFloat;
          acc[4] := acc[4] + FieldByName('ER_MEDICARE_TAXABLE_WAGES').AsFloat;
          acc[5] := acc[5] + FieldByName('ER_FUI_TAXABLE_WAGES').AsFloat;

          Next;
          if Eof or
             (clNbr <> FieldByName('CL_NBR').AsInteger) or
             (coNbr <> FieldByName('CO_NBR').AsInteger) or
             (eeNbr <> FieldByName('EE_NBR').AsInteger) then
          begin
            for i := 0 to High(acc) do
            begin
              if acc[i] < 0 then
              begin
                SetLength(WagesOOB, Succ(Length(WagesOOB)));
                WagesOOB[High(WagesOOB)].CL_NBR := clNbr;
                WagesOOB[High(WagesOOB)].CO_NBR := coNbr;
              end;
              acc[i] := 0;
            end;
            clNbr := FieldByName('CL_NBR').AsInteger;
            coNbr := FieldByName('CO_NBR').AsInteger;
            eeNbr := FieldByName('EE_NBR').AsInteger;
          end;
        end;
        IndexFieldNames := '';
        First;
      end;

//  This control only for one company...
//  it is important for PreProcess Quarter End.. for Balance screen, it doesnt matter..
      sql := 'select  a.ee_nbr from  PR_CHECK  a, PR b ' +
             ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
             ' b.PR_NBR = a.PR_NBR and ' +
             ' b.CHECK_DATE between :beginDateYear and :endDate ' +
             ' and b.STATUS in ( ''P'', ''V'' ) and  b.CO_NBR in (#CoList)' +
             ' group by a.EE_NBR ' +
             ' having sum(a.EE_OASDI_TAXABLE_WAGES) > :pWageLimit or ' +
                    ' sum(a.ER_OASDI_TAXABLE_WAGES) > :pWageLimit';
      Q := TevQuery.Create(sql);
      Q.Macros.AddValue('CoList', tCoList.CommaText);
      Q.Params.AddValue('pWageLimit', DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('OASDI_WAGE_LIMIT').Value );
      Q.Params.AddValue('beginDateYear', StartOfTheYear(BeginDate));
      Q.Params.AddValue('endDate', EndDate);
      Q.Execute;
      if Q.Result.RecordCount > 0 then
        bTaxWageLimit := True;

      sql := 'select Distinct ye.CL_PERSON_NBR, ye.MAKEUP_FICA_ON_CLEANUP_PR, ' +
                    'sum(ya.EE_MEDICARE_TAXABLE_WAGES) EeMedicateWagesTotal ' +
                    'from  PR_CHECK  ya, PR yb, EE ye ' +
                    'where {AsOfNow<ya>} and {AsOfNow<yb>} and {AsOfNow<ye>} and ' +
                    'yb.PR_NBR = ya.PR_NBR and ye.EE_NBR = ya.EE_NBR and ' +
                    'yb.CHECK_DATE between :beginDateYear and :endDate ' +
                    'and yb.STATUS in ( ''P'', ''V'' ) ' +
                    'group by ye.CL_PERSON_NBR, ye.MAKEUP_FICA_ON_CLEANUP_PR ' +
                    'having sum(ya.EE_MEDICARE_TAXABLE_WAGES) > :pWageLimit';

      QEEMedY := TevQuery.Create(sql);
      QEEMedY.Params.AddValue('pWageLimit', DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value );
      QEEMedY.Params.AddValue('beginDateYear', StartOfTheYear(BeginDate));
      QEEMedY.Params.AddValue('endDate', EndDate);
      QEEMedY.Execute;
      QEEMedY.Result.IndexFieldNames := 'CL_PERSON_NBR;MAKEUP_FICA_ON_CLEANUP_PR;';

      if QEEMedY.Result.RecordCount > 0 then
      begin
        ListClPerson := TisStringList.Create;
        ListClPerson.Clear;
        QEEMedY.Result.vclDataSet.First;
        while not QEEMedY.Result.vclDataSet.Eof do
        begin
         if QEEMedY.Result.vclDataSet.FieldByName('CL_PERSON_NBR').AsInteger > 0 then
           ListClPerson.Add(QEEMedY.Result.vclDataSet.FieldByName('CL_PERSON_NBR').AsString);
         QEEMedY.Result.vclDataSet.Next;
        end;

        if ListClPerson.Count > 0 then
        begin
          sql := 'select Distinct yb.CO_NBR, ye.CL_PERSON_NBR, ye.MAKEUP_FICA_ON_CLEANUP_PR, ' +
                    'sum(ya.EE_MEDICARE_TAXABLE_WAGES) EeMedicateWagesTotal ' +
                    'from  PR_CHECK  ya, PR yb, EE ye ' +
                    'where {AsOfNow<ya>} and {AsOfNow<yb>} and {AsOfNow<ye>} and ' +
                    'yb.PR_NBR = ya.PR_NBR and ye.EE_NBR = ya.EE_NBR and ' +
                    'yb.CHECK_DATE between :beginDateYear and :endDate ' +
                    'and yb.STATUS in ( ''P'', ''V'' ) and  yb.CO_NBR in (#CoList) ' +
                    'group by yb.CO_NBR, ye.CL_PERSON_NBR, ye.MAKEUP_FICA_ON_CLEANUP_PR ';

          QEEMedYCO := TevQuery.Create(sql);
          QEEMedYCO.Macros.AddValue('CoList', tCoList.CommaText);
          QEEMedYCO.Params.AddValue('pWageLimit', DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value );
          QEEMedYCO.Params.AddValue('beginDateYear', StartOfTheYear(BeginDate));
          QEEMedYCO.Params.AddValue('endDate', EndDate);
          QEEMedYCO.Execute;
          QEEMedYCO.Result.IndexFieldNames := 'CO_NBR;CL_PERSON_NBR;MAKEUP_FICA_ON_CLEANUP_PR;';

          sql := 'select Distinct qb.CO_NBR, qe.CL_PERSON_NBR, qe.MAKEUP_FICA_ON_CLEANUP_PR, ' +
                        'sum(qa.EE_MEDICARE_TAXABLE_WAGES) EeMedicateWagesTotalQuarter ' +
                        'from  PR_CHECK qa, PR qb, EE qe ' +
                        'where {AsOfNow<qa>} and  {AsOfNow<qb>} and {AsOfNow<qe>} and ' +
                        'qb.PR_NBR = qa.PR_NBR and qe.EE_NBR = qa.EE_NBR and ' +
                        'qb.CHECK_DATE between :beginDate and :endDate ' +
                        'and qb.STATUS in ( ''P'', ''V'' ) and  qb.CO_NBR in (#CoList) and ' +
                        BuildINsqlStatement('CL_PERSON_NBR', ListClPerson) +
                        ' group by qb.CO_NBR , qe.CL_PERSON_NBR, qe.MAKEUP_FICA_ON_CLEANUP_PR';

          QEEMedQ := TevQuery.Create(sql);
          QEEMedQ.Macros.AddValue('CoList', tCoList.CommaText);
          QEEMedQ.Params.AddValue('beginDate', BeginDate);
          QEEMedQ.Params.AddValue('endDate', EndDate);
          QEEMedQ.Execute;
          QEEMedQ.Result.IndexFieldNames := 'CO_NBR;CL_PERSON_NBR;MAKEUP_FICA_ON_CLEANUP_PR;';

          cdEEMedicare.IndexFieldNames := 'CO_NBR;CL_PERSON_NBR;MAKEUP_FICA_ON_CLEANUP_PR;';

          QEEMedQ.Result.vclDataSet.First;
          while not QEEMedQ.Result.vclDataSet.Eof do
          begin
           if QEEMedYCO.Result.vclDataSet.Locate('CL_PERSON_NBR;CO_NBR',
                 VarArrayOf([QEEMedQ.Result.vclDataSet.FieldByName('CL_PERSON_NBR').AsInteger,
                             QEEMedQ.Result.vclDataSet.FieldByName('CO_NBR').AsInteger]),[]) then
           begin
              cdEEMedicare.Append;
              cdEEMedicare['CO_NBR'] := QEEMedQ.Result['CO_NBR'];
              cdEEMedicare['CL_PERSON_NBR'] := QEEMedQ.Result['CL_PERSON_NBR'];
              cdEEMedicare['MAKEUP_FICA_ON_CLEANUP_PR'] := QEEMedQ.Result['MAKEUP_FICA_ON_CLEANUP_PR'];
              cdEEMedicare['EeMedicateWagesTotal'] := QEEMedYCO.Result['EeMedicateWagesTotal'];
              cdEEMedicare['EeMedicateWagesTotalQuarter'] := QEEMedQ.Result['EeMedicateWagesTotalQuarter'];
              cdEEMedicare.Post;
           end;
           QEEMedQ.Result.vclDataSet.Next;
          end;
        end;
      end;

      sql :='select CL_PERSON_NBR from EE where  {AsOfNow<EE>} ' +
                      'group by CO_NBR, CL_PERSON_NBR ' +
                      'having  count(CL_PERSON_NBR) >1 ';
      QEEY := TevQuery.Create(sql);
      QEEY.Execute;
      if QEEY.Result.RecordCount > 0 then
      begin
        ListClPerson := TisStringList.Create;
        ListClPerson.Clear;
        QEEY.Result.vclDataSet.First;
        while not QEEY.Result.vclDataSet.Eof do
        begin
         if QEEY.Result.vclDataSet.FieldByName('CL_PERSON_NBR').AsInteger > 0 then
           ListClPerson.Add(QEEY.Result.vclDataSet.FieldByName('CL_PERSON_NBR').AsString);
         QEEY.Result.vclDataSet.Next;
        end;
        if ListClPerson.Count > 0 then
        begin
            sql :='select t.cL_PERSON_NBR, t.CO_NBR from CL_PERSON p, ' +
                        '( select  distinct  e.cL_PERSON_NBR, e.CO_NBR from  EE e ' +
                            'where {AsOfNow<e>} and ' +
                                   BuildINsqlStatement('e.CL_PERSON_NBR', ListClPerson) + ' ) t ' +
                'where  p.CL_PERSON_NBR = t.CL_PERSON_NBR ' +
                'group by t.CO_NBR, t.CL_PERSON_NBR ' +
                'having  count(t.CL_PERSON_NBR) >1';

            QEEMedicarePerson := TevQuery.Create(sql);
            QEEMedicarePerson.Execute;
            QEEMedicarePerson.Result.IndexFieldNames := 'CL_PERSON_NBR;CO_NBR;';
        end;
      end;


      with cdsCheckStates do
      begin
        IndexFieldNames := 'CL_NBR;CO_NBR;EE_NBR;CO_STATES_NBR';
        First;
        clNbr := FieldByName('CL_NBR').AsInteger;
        coNbr := FieldByName('CO_NBR').AsInteger;
        eeNbr := FieldByName('EE_NBR').AsInteger;
        entNbr := FieldByName('CO_STATES_NBR').AsInteger;
        SetLength(acc, 3);
        for i := 0 to High(acc) do
          acc[i] := 0;
        while not Eof do
        begin
          acc[0] := acc[0] + FieldByName('STATE_TAXABLE_WAGES').AsFloat;
          acc[1] := acc[1] + FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat;
          acc[2] := acc[2] + FieldByName('ER_SDI_TAXABLE_WAGES').AsFloat;
          Next;
          if Eof or
             (clNbr <> FieldByName('CL_NBR').AsInteger) or
             (coNbr <> FieldByName('CO_NBR').AsInteger) or
             (eeNbr <> FieldByName('EE_NBR').AsInteger) or
             (entNbr <> FieldByName('CO_STATES_NBR').AsInteger) then
          begin
            for i := 0 to High(acc) do
            begin
              if acc[i] < 0 then
              begin
                SetLength(WagesOOB, Succ(Length(WagesOOB)));
                WagesOOB[High(WagesOOB)].CL_NBR := clNbr;
                WagesOOB[High(WagesOOB)].CO_NBR := coNbr;
              end;
              acc[i] := 0;
            end;
            clNbr := FieldByName('CL_NBR').AsInteger;
            coNbr := FieldByName('CO_NBR').AsInteger;
            eeNbr := FieldByName('EE_NBR').AsInteger;
            entNbr := FieldByName('CO_STATES_NBR').AsInteger;
          end;
        end;
        IndexFieldNames := '';
        First;
      end;

      cdsCheckSUI.IndexFieldNames := 'CL_NBR;CO_NBR;EE_NBR;SY_SUI_NBR';
      cdsCheckSUI.First;
      clNbr := cdsCheckSUI.FieldByName('CL_NBR').AsInteger;
      coNbr := cdsCheckSUI.FieldByName('CO_NBR').AsInteger;
      eeNbr := cdsCheckSUI.FieldByName('EE_NBR').AsInteger;
      entNbr := cdsCheckSUI.FieldByName('SY_SUI_NBR').AsInteger;
      SetLength(acc, 1);
      for i := 0 to High(acc) do
        acc[i] := 0;
      while not cdsCheckSUI.Eof do
      begin
        acc[0] := acc[0] + cdsCheckSUI.FieldByName('SUI_TAXABLE_WAGES').AsFloat;
        cdsCheckSUI.Next;
        if cdsCheckSUI.Eof or
           (clNbr <> cdsCheckSUI.FieldByName('CL_NBR').AsInteger) or
           (coNbr <> cdsCheckSUI.FieldByName('CO_NBR').AsInteger) or
           (eeNbr <> cdsCheckSUI.FieldByName('EE_NBR').AsInteger) or
           (entNbr <> cdsCheckSUI.FieldByName('SY_SUI_NBR').AsInteger) then
        begin
          for i := 0 to High(acc) do
          begin
            if acc[i] < 0 then
            begin
              SetLength(WagesOOB, Succ(Length(WagesOOB)));
              WagesOOB[High(WagesOOB)].CL_NBR := clNbr;
              WagesOOB[High(WagesOOB)].CO_NBR := coNbr;
            end;
            acc[i] := 0;
          end;
          clNbr := cdsCheckSUI.FieldByName('CL_NBR').AsInteger;
          coNbr := cdsCheckSUI.FieldByName('CO_NBR').AsInteger;
          eeNbr := cdsCheckSUI.FieldByName('EE_NBR').AsInteger;
          entNbr := cdsCheckSUI.FieldByName('SY_SUI_NBR').AsInteger;
        end;
      end;
      cdsCheckSUI.IndexFieldNames := '';
      cdsCheckSUI.First;

      with cdsCheckLocal do
      begin
        IndexFieldNames := 'CL_NBR;CO_NBR;EE_NBR;CO_LOCAL_TAX_NBR';
        First;
        clNbr := FieldByName('CL_NBR').AsInteger;
        coNbr := FieldByName('CO_NBR').AsInteger;
        eeNbr := FieldByName('EE_NBR').AsInteger;
        entNbr := FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
        SetLength(acc, 1);
        for i := 0 to High(acc) do
          acc[i] := 0;
        while not Eof do
        begin
          acc[0] := acc[0] + FieldByName('LOCAL_TAXABLE_WAGE').AsFloat;
          Next;
          if Eof or
             (clNbr <> FieldByName('CL_NBR').AsInteger) or
             (coNbr <> FieldByName('CO_NBR').AsInteger) or
             (eeNbr <> FieldByName('EE_NBR').AsInteger) or
             (entNbr <> FieldByName('CO_LOCAL_TAX_NBR').AsInteger) then
          begin
            for i := 0 to High(acc) do
            begin
              if acc[i] < 0 then
              begin
                SetLength(WagesOOB, Succ(Length(WagesOOB)));
                WagesOOB[High(WagesOOB)].CL_NBR := clNbr;
                WagesOOB[High(WagesOOB)].CO_NBR := coNbr;
              end;
              acc[i] := 0;
            end;
            clNbr := FieldByName('CL_NBR').AsInteger;
            coNbr := FieldByName('CO_NBR').AsInteger;
            eeNbr := FieldByName('EE_NBR').AsInteger;
            entNbr := FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
          end;
        end;
        IndexFieldNames := '';
        First;
      end;

// Work with check lines information

      cdsCheckLines.OnFilterRecord := Nil;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;

      Provider.DataSet := cdsCheckLines;
      cdsCheckLines.Filter := 'E_D_CODE_TYPE = ''E*''';
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('All Earnings', ' AE', 'cdsAllEarnings');
      if cdsAllEarnings.Active then
        cdsAllEarnings.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsAllEarnings.Data := cdsAggCheckLines.Data;

      cdsCheckLines.Filter := 'E_D_CODE_TYPE = ''M*''';
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('All Memos', ' AM', 'cdsAllMemos');
      if cdsAllMemos.Active then
        cdsAllMemos.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsAllMemos.Data := cdsAggCheckLines.Data;

      cdsCheckLines.Filter := 'E_D_CODE_TYPE = ''E1''';
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('Tips', ' AT', 'cdsTips');
      if cdsTips.Active then
        cdsTips.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsTips.Data := cdsAggCheckLines.Data;

      cdsCheckLines.Filter := 'E_D_CODE_TYPE = ''E3''';
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('GTL', ' GT', 'cdsGTL');
      if cdsGTL.Active then
        cdsGTL.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsGTL.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.W2Filter;
      cdsCheckLines.Filter := '';
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;

      if cdsAggCheckLines.RecordCount > 0 then
      begin
        cdsW2Box.Data := cdsAggCheckLines.Data;
        s := #255;
        while not cdsW2Box.Eof do
        begin
          if s <> cdsW2Box.FieldByName('W2_BOX_CALC').AsString then
          begin
            s := cdsW2Box.FieldByName('W2_BOX_CALC').AsString;
            if Pos(',', s) <> -1 then
              dsName := 'cdsW2Box' + StringReplace(s, ',', '_', [rfReplaceAll])
            else
              dsName :=  'cdsW2Box' + StringReplace(s, ' ', '_', [rfReplaceAll]);

            ds := FindComponent(dsName) as TevClientDataSet;
            if not Assigned(ds) then
            begin
              ds := TevClientDataSet.Create(Result);
              ds.Name := dsName;
            end;

            AddToSummary('E/D W2 Boxes (' + s + ')', ' W2 ' + s, ds.Name);
            if ds.Active then
              ds.AppendData(cdsAggCheckLines.Data, False)
            else
              ds.Data := cdsAggCheckLines.Data;
          end;
          cdsW2Box.Next;
        end;
      end;

      cdsCheckLines.OnFilterRecord := Result.EFed;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('Federal Tax Exempt Earnings', '0020', 'cdsFederalEExempt');
      if cdsFederalEExempt.Active then
        cdsFederalEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsFederalEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DFed;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('Federal Tax Exempt Deductions', '0030', 'cdsFederalDExempt');
      if cdsFederalDExempt.Active then
        cdsFederalDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsFederalDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EEIC;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EIC Tax Exempt Earnings', '0320', 'cdsEICEExempt');
      if cdsEICEExempt.Active then
        cdsEICEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEICEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DEIC;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EIC Tax Exempt Deductions', '0330', 'cdsEICDExempt');
      if cdsEICDExempt.Active then
        cdsEICDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEICDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EEEOASDI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EE OASDI Tax Exempt Earnings', '0620', 'cdsEEOASDIEExempt');
      if cdsEEOASDIEExempt.Active then
        cdsEEOASDIEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEEOASDIEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DEEOASDI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EE OASDI Tax Exempt Deductions', '0630', 'cdsEEOASDIDExempt');
      if cdsEEOASDIDExempt.Active then
        cdsEEOASDIDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEEOASDIDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EEEMedicare;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EE Medicare Tax Exempt Earnings', '0920', 'cdsEEMedicareEExempt');
      if cdsEEMedicareEExempt.Active then
        cdsEEMedicareEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEEMedicareEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DEEMedicare;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('EE Medicare Tax Exempt Deductions', '0930', 'cdsEEMedicareDExempt');
      if cdsEEMedicareDExempt.Active then
        cdsEEMedicareDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEEMedicareDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EEROASDI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('ER OASDI Tax Exempt Earnings', '0C20', 'cdsEROASDIEExempt');
      if cdsEROASDIEExempt.Active then
        cdsEROASDIEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEROASDIEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DEROASDI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('ER OASDI Tax Exempt Deductions', '0C30', 'cdsEROASDIDExempt');
      if cdsEROASDIDExempt.Active then
        cdsEROASDIDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsEROASDIDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EERMedicare;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('ER Medicare Tax Exempt Earnings', '0F20', 'cdsERMedicareEExempt');
      if cdsERMedicareEExempt.Active then
        cdsERMedicareEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsERMedicareEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DERMedicare;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('ER Medicare Tax Exempt Deductions', '0F30', 'cdsERMedicareDExempt');
      if cdsERMedicareDExempt.Active then
        cdsERMedicareDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsERMedicareDExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.EFUI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('FUI Tax Exempt Earnings', '0I20', 'cdsFUIEExempt');
      if cdsFUIEExempt.Active then
        cdsFUIEExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsFUIEExempt.Data := cdsAggCheckLines.Data;

      cdsCheckLines.OnFilterRecord := Result.DFUI;
      cdsCheckLines.Filtered := False;
      cdsCheckLines.Filtered := True;
      cdsCheckLines.First;
      cdsAggCheckLines.Data := Provider.Data;
      AddToSummary('FUI Tax Exempt Deductions', '0I30', 'cdsFUIDExempt');
      if cdsFUIDExempt.Active then
        cdsFUIDExempt.AppendData(cdsAggCheckLines.Data, False)
      else
        cdsFUIDExempt.Data := cdsAggCheckLines.Data;

// Work with checks information

      cdsLiab.Filtered := True;
      Provider.DataSet := cdsLiab;
      cdsAggChecks.Data := cdsChecks.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_FEDERAL + ''' or TAX_TYPE = ''' + TAX_LIABILITY_TYPE_FEDERAL_945 + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('Federal Taxes', '0000', 'FEDERAL_TAXABLE_WAGES_Sum', 'FEDERAL_TAX_Sum','','','', null, 'cdsFederalTax', 'cdsFederalLiab');
      if cdsFederalLiab.Active then
        cdsFederalLiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsFederalLiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_EIC + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('EIC Taxes', '0300', 'FEDERAL_TAXABLE_WAGES_Sum', 'EE_EIC_TAX_Sum','','', '', null, 'cdsFederalTax', 'cdsEICLiab');
      if cdsEICLiab.Active then
        cdsEICLiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsEICLiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_OASDI + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('EE OASDI Taxes', '0600', 'EE_OASDI_TAXABLE_WAGES_Sum', 'EE_OASDI_TAX_Sum',
                 'EE_OASDI_TAX_Sum_','ER_OASDI_TAX_Sum_','FICAEE_OASDIWages_SUM',
                 DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value,
                 'cdsFederalTax', 'cdsEEOASDILiab');
      if cdsEEOASDILiab.Active then
        cdsEEOASDILiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsEEOASDILiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_MEDICARE + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('EE Medicare Taxes', '0900', 'EE_MEDICARE_TAXABLE_WAGES_Sum', 'EE_MEDICARE_TAX_Sum',
                 'EE_MEDICARE_TAX_Sum_','ER_MEDICARE_TAX_Sum_','FICAEE_MEDICAREWages_SUM',
                 DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value,
                 'cdsFederalTax', 'cdsEEMedicareLiab');
      if cdsEEMedicareLiab.Active then
        cdsEEMedicareLiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsEEMedicareLiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_OASDI + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('ER OASDI Taxes', '0C00', 'ER_OASDI_TAXABLE_WAGES_Sum', 'ER_OASDI_TAX_Sum','','','',
                 DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value,
                 'cdsFederalTax', 'cdsEROASDILiab');
      if cdsEROASDILiab.Active then
        cdsEROASDILiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsEROASDILiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_MEDICARE + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('ER Medicare Taxes', '0F00', 'ER_MEDICARE_TAXABLE_WAGES_Sum', 'ER_MEDICARE_TAX_Sum','','','',
                 DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value,
                 'cdsFederalTax', 'cdsERMedicareLiab');
      if cdsERMedicareLiab.Active then
        cdsERMedicareLiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsERMedicareLiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_FUI + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;


      SkipBalanceControl := False;
      sql := 'select Count(*) amount from EE a where {AsOfNow<a>} and ' +
             'not FUI_RATE_CREDIT_OVERRIDE is null ';
      Q := TevQuery.Create(sql);
      Q.Execute;
      if Q.Result.Fields[0].AsInteger > 0 then
        SkipBalanceControl := True;

      if DM_COMPANY.CO.Locate('CO_NBR', cdsCoList['CoNbr'], []) and
         not DM_COMPANY.CO.FUI_RATE_OVERRIDE.IsNull then
        AddTaxLine('FUI Taxes', '0I00', 'ER_FUI_TAXABLE_WAGES_Sum', 'ER_FUI_TAX_Sum','','','',
                   DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FUI_RATE_REAL.Value -
                   DM_COMPANY.CO.FUI_RATE_OVERRIDE.Value,
                   'cdsFederalTax', 'cdsFUILiab', False, True)
      else
        AddTaxLine('FUI Taxes', '0I00', 'ER_FUI_TAXABLE_WAGES_Sum', 'ER_FUI_TAX_Sum','','','',
                   DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FUI_RATE_REAL.Value -
                   DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FUI_RATE_CREDIT.Value,
                   'cdsFederalTax', 'cdsFUILiab', False, SkipBalanceControl);
      if cdsFUILiab.Active then
        cdsFUILiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsFUILiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('Backup Withholding', '0L00', '', 'BACKUP_WITHHOLDING_Sum','','', '', null, 'cdsFederalTax', 'cdsBackupWithholdingLiab');
      if cdsBackupWithholdingLiab.Active then
        cdsBackupWithholdingLiab.AppendData(cdsAggLiab.Data, False)
      else
        cdsBackupWithholdingLiab.Data := cdsAggLiab.Data;

      if cdsFederalTax.Active then
        cdsFederalTax.AppendData(cdsAggChecks.Data, False)
      else
        cdsFederalTax.Data := cdsAggChecks.Data;

      cdsChecks.Filtered := True;
      Provider.DataSet := cdsChecks;

      cdsChecks.Filter := 'COMPANY_OR_INDIVIDUAL_NAME = ''' + GROUP_BOX_COMPANY + '''';
      cdsChecks.First;
      cdsAggChecks.Data := Provider.Data;
      AddTaxLine('Federal Taxes for 1099', '0P00', 'FEDERAL_TAXABLE_WAGES_Sum', 'FEDERAL_TAX_Sum','','', '',null, 'cdsFederal1099', '', True);
      if cdsFederal1099.Active then
        cdsFederal1099.AppendData(cdsAggChecks.Data, False)
      else
        cdsFederal1099.Data := cdsAggChecks.Data;

      cdsChecks.Filter := 'CHECK_TYPE = ''' + CHECK_TYPE2_3RD_PARTY + '''';
      cdsChecks.First;
      cdsAggChecks.Data := Provider.Data;
      Provider.DataSet := cdsLiab;

      cdsLiab.Filter := '(TAX_TYPE = ''' + TAX_LIABILITY_TYPE_FEDERAL + ''' or TAX_TYPE = ''' + TAX_LIABILITY_TYPE_FEDERAL_945 + ''') and THIRD_PARTY = ''' + GROUP_BOX_YES + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('3rd Party Federal Taxes', '0R00', 'FEDERAL_TAXABLE_WAGES_Sum', 'FEDERAL_TAX_Sum','','', '', null, 'cds3Party', 'cds3PartyFederalLiab');
      if cds3PartyFederalLiab.Active then
        cds3PartyFederalLiab.AppendData(cdsAggLiab.Data, False)
      else
        cds3PartyFederalLiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_OASDI + ''' and THIRD_PARTY = ''' + GROUP_BOX_YES + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('3rd Party EE OASDI Taxes', '0T00', 'EE_OASDI_TAXABLE_WAGES_Sum', 'EE_OASDI_TAX_Sum','','', '', null, 'cds3Party', 'cds3PartyEEOASDILiab');
      if cds3PartyEEOASDILiab.Active then
        cds3PartyEEOASDILiab.AppendData(cdsAggLiab.Data, False)
      else
        cds3PartyEEOASDILiab.Data := cdsAggLiab.Data;

      cdsLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_MEDICARE + ''' and THIRD_PARTY = ''' + GROUP_BOX_YES + '''';
      cdsLiab.First;
      cdsAggLiab.Data := Provider.Data;
      AddTaxLine('3rd Party EE Medicare Taxes', '0V00', 'EE_MEDICARE_TAXABLE_WAGES_Sum', 'EE_Medicare_TAX_Sum','','', '', null, 'cds3Party', 'cds3PartyEROASDILiab');
      if cds3PartyEROASDILiab.Active then
        cds3PartyEROASDILiab.AppendData(cdsAggLiab.Data, False)
      else
        cds3PartyEROASDILiab.Data := cdsAggLiab.Data;

      if cds3Party.Active then
        cds3Party.AppendData(cdsAggChecks.Data, False)
      else
        cds3Party.Data := cdsAggChecks.Data;

// Work with check states information

      cdsStateLiab.Filtered := True;
      Provider.DataSet := cdsStateLiab;
      cdsAggCheckStates.Data := cdsCheckStates.Data;

      cdsStateLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_STATE + '''';
      cdsStateLiab.First;
      cdsAggStateLiab.Data := Provider.Data;
      AddStateTaxLines('State Taxes', '000', 'STATE_TAXABLE_WAGES_Sum', 'STATE_TAX_Sum', '', 'cdsStateTax', 'cdsStateTaxLiab');
      if cdsStateTaxLiab.Active then
        cdsStateTaxLiab.AppendData(cdsAggStateLiab.Data, False)
      else
        cdsStateTaxLiab.Data := cdsAggStateLiab.Data;

      cdsStateLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_SDI + '''';
      cdsStateLiab.First;
      cdsAggStateLiab.Data := Provider.Data;
      AddStateTaxLines('EE SDI Taxes', '300', 'EE_SDI_TAXABLE_WAGES_Sum', 'EE_SDI_TAX_Sum', 'EE_SDI_RATE', 'cdsStateTax', 'cdsEESDILiab');
      if cdsEESDILiab.Active then
        cdsEESDILiab.AppendData(cdsAggStateLiab.Data, False)
      else
        cdsEESDILiab.Data := cdsAggStateLiab.Data;

      cdsStateLiab.Filter := 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_SDI + '''';
      cdsStateLiab.First;
      cdsAggStateLiab.Data := Provider.Data;
      AddStateTaxLines('ER SDI Taxes', '600', 'ER_SDI_TAXABLE_WAGES_Sum', 'ER_SDI_TAX_Sum', 'EE_SDI_RATE', 'cdsStateTax', 'cdsERSDILiab');
      if cdsERSDILiab.Active then
        cdsERSDILiab.AppendData(cdsAggStateLiab.Data, False)
      else
        cdsERSDILiab.Data := cdsAggStateLiab.Data;

      if cdsStateTax.Active then
        cdsStateTax.AppendData(cdsAggCheckStates.Data, False)
      else
        cdsStateTax.Data := cdsAggCheckStates.Data;

      cdsCheckStates.Filtered := True;
      Provider.DataSet := cdsCheckStates;

      cdsCheckStates.Filter := 'COMPANY_OR_INDIVIDUAL_NAME = ''' + GROUP_BOX_COMPANY + '''';
      cdsCheckStates.First;
      cdsAggCheckStates.Data := Provider.Data;
      AddStateTaxLines('State Tax for 1099', '900', 'STATE_TAXABLE_WAGES_Sum', 'STATE_TAX_Sum', '', 'cdsState1099', '', True);
      if cdsState1099.Active then
        cdsState1099.AppendData(cdsAggCheckStates.Data, False)
      else
        cdsState1099.Data := cdsAggCheckStates.Data;
      AddStateTaxLines('EE SDI for 1099', 'A00', 'EE_SDI_TAXABLE_WAGES_Sum', 'EE_SDI_TAX_Sum', 'EE_SDI_RATE', 'cdsEESDI1099', '', True);
      if cdsEESDI1099.Active then
        cdsEESDI1099.AppendData(cdsAggCheckStates.Data, False)
      else
        cdsEESDI1099.Data := cdsAggCheckStates.Data;
      AddStateTaxLines('ER SDI for 1099', 'B00', 'ER_SDI_TAXABLE_WAGES_Sum', 'ER_SDI_TAX_Sum', 'ER_SDI_RATE', 'cdsERSDI1099', '', True);
      if cdsERSDI1099.Active then
        cdsERSDI1099.AppendData(cdsAggCheckStates.Data, False)
      else
        cdsERSDI1099.Data := cdsAggCheckStates.Data;

// Work with check line states information

      cdsCheckLineStates.Filtered := True;

      Provider.DataSet := cdsCheckLineStates;
      cdsCheckLineStates.OnFilterRecord := Result.EState;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('State Tax Exempt Earnings', '020', 'cdsStateEExempt');
      if cdsStateEExempt.Active then
        cdsStateEExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsStateEExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineStates.OnFilterRecord := Result.DState;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('State Tax Exempt Deductions', '030', 'cdsStateDExempt');
      if cdsStateDExempt.Active then
        cdsStateDExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsStateDExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineStates.OnFilterRecord := Result.EEESDI;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('EE SDI Tax Exempt Earnings', '320', 'cdsEESDIEExempt');
      if cdsEESDIEExempt.Active then
        cdsEESDIEExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsEESDIEExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineStates.OnFilterRecord := Result.DEESDI;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('EE SDI Tax Exempt Deductions', '330', 'cdsEESDIDExempt');
      if cdsEESDIDExempt.Active then
        cdsEESDIDExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsEESDIDExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineStates.OnFilterRecord := Result.EERSDI;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('ER SDI Tax Exempt Earnings', '620', 'cdsERSDIEExempt');
      if cdsERSDIEExempt.Active then
        cdsERSDIEExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsERSDIEExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineStates.OnFilterRecord := Result.DERSDI;
      cdsCheckLineStates.Filtered := False;
      cdsCheckLineStates.Filtered := True;
      cdsCheckLineStates.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('ER SDI Tax Exempt Deductions', '630', 'cdsERSDIDExempt');
      if cdsERSDIDExempt.Active then
        cdsERSDIDExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsERSDIDExempt.Data := cdsAggCheckLineStates.Data;

// Work with check line SUI information

      cdsCheckLineSUI.Filtered := True;

      Provider.DataSet := cdsCheckLineSUI;
      cdsCheckLineSUI.OnFilterRecord := Result.EEESUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('EE SUI Tax Exempt Earnings', 'C20', 'cdsEESUIEExempt');
      if cdsEESUIEExempt.Active then
        cdsEESUIEExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsEESUIEExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineSUI.OnFilterRecord := Result.DEESUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('EE SUI Tax Exempt Deductions', 'C30', 'cdsEESUIDExempt');
      if cdsEESUIDExempt.Active then
        cdsEESUIDExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsEESUIDExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineSUI.OnFilterRecord := Result.EERSUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('ER SUI Tax Exempt Earnings', 'F20', 'cdsERSUIEExempt');
      if cdsERSUIEExempt.Active then
        cdsERSUIEExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsERSUIEExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineSUI.OnFilterRecord := Result.DERSUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('ER SUI Tax Exempt Deductions', 'F30', 'cdsERSUIDExempt');
      if cdsERSUIDExempt.Active then
        cdsERSUIDExempt.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsERSUIDExempt.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineSUI.OnFilterRecord := Result.MEESUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('EE SUI Taxable Memos', 'I00', 'cdsEESUIMemo');
      if cdsEESUIMemo.Active then
        cdsEESUIMemo.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsEESUIMemo.Data := cdsAggCheckLineStates.Data;

      cdsCheckLineSUI.OnFilterRecord := Result.MERSUI;
      cdsCheckLineSUI.Filtered := False;
      cdsCheckLineSUI.Filtered := True;
      cdsCheckLineSUI.First;
      cdsAggCheckLineStates.Data := Provider.Data;
      AddStatesToSummary('ER SUI Taxable Memos', 'L00', 'cdsERSUIMemo');
      if cdsERSUIMemo.Active then
        cdsERSUIMemo.AppendData(cdsAggCheckLineStates.Data, False)
      else
        cdsERSUIMemo.Data := cdsAggCheckLineStates.Data;

// Work with check SUI information

      cdsCheckSUI.Filtered := True;
      cdsSUILiab.Filtered := True;

      Provider.DataSet := cdsCheckSUI;
      cdsCheckSUI.Filter := 'EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE_OTHER + '''';
      cdsCheckSUI.First;
      cdsAggCheckSUI.Data := Provider.Data;

      Provider.DataSet := cdsSUILiab;
      cdsSUILiab.Filter := 'EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE_OTHER + '''';
      cdsSUILiab.First;
      cdsAggSUILiab.Data := Provider.Data;

      AddSUITaxLines('EE SUI Taxes', 'C00', 'SUI_TAXABLE_WAGES_Sum', 'SUI_TAX_Sum', 'Rate', 'cdsEESUITax', 'cdsEESUILiab');
      AddSUITaxLines('EE SUI Gross Wages', 'C05', 'SUI_GROSS_WAGES_Sum', '', '', 'cdsEESUITax', '', True);
      if cdsEESUILiab.Active then
        cdsEESUILiab.AppendData(cdsAggSUILiab.Data, False)
      else
        cdsEESUILiab.Data := cdsAggSUILiab.Data;
      if cdsEESUITax.Active then
        cdsEESUITax.AppendData(cdsAggCheckSUI.Data, False)
      else
        cdsEESUITax.Data := cdsAggCheckSUI.Data;

      Provider.DataSet := cdsCheckSUI;
      cdsCheckSUI.Filter := 'EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER_OTHER + '''';
      cdsCheckSUI.First;
      cdsAggCheckSUI.Data := Provider.Data;

      Provider.DataSet := cdsSUILiab;
      cdsSUILiab.Filter := 'EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER_OTHER + '''';
      cdsSUILiab.First;
      cdsAggSUILiab.Data := Provider.Data;

      AddSUITaxLines('ER SUI Taxes', 'F00', 'SUI_TAXABLE_WAGES_Sum', 'SUI_TAX_Sum', 'Rate', 'cdsERSUITax', 'cdsERSUILiab');
      AddSUITaxLines('ER SUI Gross Wages', 'F05', 'SUI_GROSS_WAGES_Sum', '', '', 'cdsERSUITax', '', True);
      if cdsERSUILiab.Active then
        cdsERSUILiab.AppendData(cdsAggSUILiab.Data, False)
      else
        cdsERSUILiab.Data := cdsAggSUILiab.Data;
      if cdsERSUITax.Active then
        cdsERSUITax.AppendData(cdsAggCheckSUI.Data, False)
      else
        cdsERSUITax.Data := cdsAggCheckSUI.Data;

      Provider.DataSet := cdsCheckSUI;
      cdsCheckSUI.Filter := '(EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_EE_OTHER +
                             ''') and COMPANY_OR_INDIVIDUAL_NAME = ''' + GROUP_BOX_COMPANY + '''';
      cdsCheckSUI.First;
      cdsAggCheckSUI.Data := Provider.Data;
      AddSUITaxLines('EE SUI for 1099', 'P00', 'SUI_TAXABLE_WAGES_Sum', 'SUI_TAX_Sum', '', 'cdsEESUI1099', '', True);
      if cdsEESUI1099.Active then
        cdsEESUI1099.AppendData(cdsAggCheckSUI.Data, False)
      else
        cdsEESUI1099.Data := cdsAggCheckSUI.Data;

      cdsCheckSUI.Filter := '(EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER + ''' or EE_ER_OR_EE_OTHER_OR_ER_OTHER = ''' + GROUP_BOX_ER_OTHER +
                             ''') and COMPANY_OR_INDIVIDUAL_NAME = ''' + GROUP_BOX_COMPANY + '''';
      cdsCheckSUI.First;
      cdsAggCheckSUI.Data := Provider.Data;
      AddSUITaxLines('ER SUI for 1099', 'S00', 'SUI_TAXABLE_WAGES_Sum', 'SUI_TAX_Sum', '', 'cdsERSUI1099', '', True);
      if cdsERSUI1099.Active then
        cdsERSUI1099.AppendData(cdsAggCheckSUI.Data, False)
      else
        cdsERSUI1099.Data := cdsAggCheckSUI.Data;

// Work with check locals information

      cdsLocalLiab.First;
      Provider.DataSet := cdsLocalLiab;
      cdsAggLocalLiab.Data := Provider.Data;
      cdsCheckLocal.First;
      Provider.DataSet := cdsCheckLocal;
      cdsAggCheckLocal.Data := Provider.Data;

      AddLocalTaxLines('Local Taxes', '000', 'LOCAL_TAXABLE_WAGE_Sum', 'LOCAL_TAX_Sum', 'cdsLocalTax', 'cdsLocalTaxLiab');
      if cdsLocalTaxLiab.Active then
        cdsLocalTaxLiab.AppendData(cdsAggLocalLiab.Data, False)
      else
        cdsLocalTaxLiab.Data := cdsAggLocalLiab.Data;
      if cdsLocalTax.Active then
        cdsLocalTax.AppendData(cdsAggCheckLocal.Data, False)
      else
        cdsLocalTax.Data := cdsAggCheckLocal.Data;

      cdsCheckLocal.Filtered := True;

      cdsCheckLocal.Filter := 'COMPANY_OR_INDIVIDUAL_NAME = ''' + GROUP_BOX_COMPANY + '''';
      cdsCheckLocal.First;
      cdsAggCheckLocal.Data := Provider.Data;
      AddLocalTaxLines('Local Tax for 1099', '010', 'LOCAL_TAXABLE_WAGE_Sum', '', 'cdsLocal1099', '', True);
      if cdsLocal1099.Active then
        cdsLocal1099.AppendData(cdsAggCheckLocal.Data, False)
      else
        cdsLocal1099.Data := cdsAggCheckLocal.Data;

// Work with check line locals information

      cdsCheckLineLocal.Filtered := True;

      Provider.DataSet := cdsCheckLineLocal;
      cdsCheckLineLocal.OnFilterRecord := Result.ELocal;
      cdsCheckLineLocal.Filtered := False;
      cdsCheckLineLocal.Filtered := True;
      cdsCheckLineLocal.First;
      cdsAggCheckLineLocal.FieldDefs.Clear;
      cdsAggCheckLineLocal.Data := Provider.Data;
      AddLocalsToSummary('Local Tax Exempt Earnings', '020', 'cdsLocalEExempt');
      if cdsLocalEExempt.Active then
        cdsLocalEExempt.AppendData(cdsAggCheckLineLocal.Data, False)
      else
        cdsLocalEExempt.Data := cdsAggCheckLineLocal.Data;

      cdsCheckLineLocal.OnFilterRecord := Result.DLocal;
      cdsCheckLineLocal.Filtered := False;
      cdsCheckLineLocal.Filtered := True;
      cdsCheckLineLocal.First;
      cdsAggCheckLineLocal.FieldDefs.Clear;
      cdsAggCheckLineLocal.Data := Provider.Data;
      AddLocalsToSummary('Local Tax Exempt Deductions', '030', 'cdsLocalDExempt');
      if cdsLocalDExempt.Active then
        cdsLocalDExempt.AppendData(cdsAggCheckLineLocal.Data, False)
      else
        cdsLocalDExempt.Data := cdsAggCheckLineLocal.Data;

    end;

    cdsCoSum.CreateDataSet;
    cdsSummary.First;
    while not cdsSummary.Eof do
    begin
      if VarIsNull(cdsSummary['Margin']) then
        fMargin := Margin
      else
        fMargin := cdsSummary['Margin'];
      if (cdsCoSum['ClNbr'] <> cdsSummary['ClNbr']) or
         (cdsCoSum['CoNbr'] <> cdsSummary['CoNbr']) then
      begin
        cdsCoSum.CheckBrowseMode;
        cdsCoSum.Insert;
        cdsCoSum['ClNbr'] := cdsSummary['ClNbr'];
        cdsCoSum['ClName'] := cdsSummary['ClName'];
        cdsCoSum['ClNumber'] := cdsSummary['ClNumber'];
        cdsCoSum['CoNbr'] := cdsSummary['CoNbr'];
        cdsCoSum['CoName'] := cdsSummary['CoName'];
        cdsCoSum['CoNumber'] := cdsSummary['CoNumber'];
        cdsCoSum['Consolidation'] := cdsSummary['Consolidation'];
        cdsCoSum['OutBalance'] := False;
        cdsCoSum['OutBalance_'] := False;
        cdsCoSum['OutBalance_EE_ER'] := False;
        cdsCoSum['OutBalWages'] := False;
        cdsCoSum['Margin'] := fMargin;
        cdsCoSum['bTaxWageLimit'] := bTaxWageLimit;
      end;

     if not cdsSummary.FieldByName('SkipBalanceControl').AsBoolean then
     begin
                  //  To control for EE and ER tax for Tipped employee
      if (not cdsSummary.FieldByName('DifWageHist_EE_ER').IsNull) and
          (cdsSummary.FieldByName('DifWageHist_EE_ER').AsFloat > 0.01) then
            cdsCoSum['OutBalance_EE_ER'] := True;

       if not cdsCoSum['OutBalance_'] then
         if not cdsSummary.FieldByName('DifWageHist_EE_ER').IsNull and
             (Abs(cdsSummary['DifWageHist_EE_ER']) > fMargin) then
           cdsCoSum['OutBalance_'] := True;

       if not cdsCoSum['OutBalance'] then
         if not cdsSummary.FieldByName('DifLiabHist').IsNull and
             (Abs(cdsSummary['DifLiabHist']) > fMargin) or
             not cdsSummary.FieldByName('FICADifWageHist_').IsNull and
             (Abs(cdsSummary['FICADifWageHist_']) > fMargin) then
           cdsCoSum['OutBalance'] := True
         else if WagesOutOfBalance(cdsSummary['ClNbr'], cdsSummary['CoNbr']) then
         begin
           cdsCoSum['OutBalance'] := True;
           cdsCoSum['OutBalWages'] := True;
         end;

      end;

      cdsSummary.Next;
    end;
    cdsCoSum.CheckBrowseMode;
  finally
    DM_SYSTEM_STATE.SY_STATES.ClearAsOfDateOverride;
    DM_SYSTEM_STATE.SY_SUI.ClearAsOfDateOverride;
    DM_COMPANY.CO_SUI.ClearAsOfDateOverride;
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.ClearAsOfDateOverride;

    tCoList.Free;
    ctx_EndWait;
  end;
end;

{ TBalanceDM }

procedure TBalanceDM.W2Filter(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (DataSet.FieldByName('W2_BOX').AsString <> '') or
            (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'W2_BOX')) <> '');
end;

procedure TBalanceDM.EFed(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_EXEMPT_EXCLUDE_FEDERAL').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_FEDERAL')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DFed(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EE_EXEMPT_EXCLUDE_FEDERAL').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_FEDERAL')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEIC(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_EXEMPT_EXCLUDE_EIC').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_EIC')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEIC(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EE_EXEMPT_EXCLUDE_EIC').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_EIC')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEEOASDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_EXEMPT_EXCLUDE_OASDI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_OASDI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEEOASDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EE_EXEMPT_EXCLUDE_OASDI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_OASDI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEROASDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('ER_EXEMPT_EXCLUDE_OASDI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_OASDI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEROASDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('ER_EXEMPT_EXCLUDE_OASDI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_OASDI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEEMedicare(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_EXEMPT_EXCLUDE_MEDICARE').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_MEDICARE')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEEMedicare(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EE_EXEMPT_EXCLUDE_MEDICARE').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_MEDICARE')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EERMedicare(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('ER_EXEMPT_EXCLUDE_MEDICARE').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_MEDICARE')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DERMedicare(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('ER_EXEMPT_EXCLUDE_MEDICARE').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_MEDICARE')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EFUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('ER_EXEMPT_EXCLUDE_FUI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_FUI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DFUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('ER_EXEMPT_EXCLUDE_FUI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'EXEMPT_FUI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EState(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_STATE').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_STATE')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DState(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_STATE').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_STATE')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEESDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SDI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_SDI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEESDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SDI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_SDI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EERSDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SDI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_SDI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DERSDI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SDI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_SDI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EEESUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE_OTHER)) and
            ((DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SUI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_SUI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DEESUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE_OTHER)) and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SUI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_SUI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.EERSUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER_OTHER)) and
            ((DataSet.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SUI').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_SUI')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DERSUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER_OTHER)) and
            (IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SUI').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_SUI')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.MEESUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'M') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_EE_OTHER)) and
            ((DataSet.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SUI').AsString = GROUP_BOX_INCLUDE) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYEE_SUI')) =  GROUP_BOX_INCLUDE));
end;

procedure TBalanceDM.MERSUI(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'M') and
            ((DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER) or
             (DataSet.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString = GROUP_BOX_ER_OTHER)) and
            ((DataSet.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SUI').AsString = GROUP_BOX_INCLUDE) or
             (VarToStr(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', DataSet['SY_STATES_NBR;E_D_CODE_TYPE'], 'EXEMPT_EMPLOYER_SUI')) =  GROUP_BOX_INCLUDE));
end;

procedure TBalanceDM.ELocal(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'E') and
            ((DataSet.FieldByName('PR_CHECK_LINE_LOCALS_NBR').AsInteger > 0) or
             (DataSet.FieldByName('EXEMPT_EXCLUDE').AsString = GROUP_BOX_EXEMPT) or
             (VarToStr(DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Lookup('SY_LOCALS_NBR;E_D_CODE_TYPE', DataSet['SY_LOCALS_NBR;E_D_CODE_TYPE'], 'EXEMPT')) =  GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.DLocal(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (Copy(DataSet.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) = 'D') and
            ((DataSet.FieldByName('PR_CHECK_LINE_LOCALS_NBR').AsInteger > 0) or
             IsTaxableType(DataSet.FieldByName('E_D_CODE_TYPE').AsString) and
             (DataSet.FieldByName('EXEMPT_EXCLUDE').AsString <> GROUP_BOX_EXEMPT) and
             (VarToStr(DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Lookup('SY_LOCALS_NBR;E_D_CODE_TYPE', DataSet['SY_LOCALS_NBR;E_D_CODE_TYPE'], 'EXEMPT')) <> GROUP_BOX_EXEMPT));
end;

procedure TBalanceDM.cdsSummaryCalcFields(DataSet: TDataSet);
var
  LReal: Currency;
  Rate: Real;

  aAmount, qAmount, cAmount, fAmount:  Double;
begin
  if not DataSet.FieldByName('DontCheck').AsBoolean then
  begin

    if not DataSet.FieldByName('AmountTaxHist').IsNull and
       not DataSet.FieldByName('AmountTaxLiab').IsNull then
      DataSet['DifLiabHist'] := Abs(DataSet['AmountTaxHist'] - DataSet['AmountTaxLiab']);
    if not DataSet.FieldByName('AmountWages').IsNull and
       not DataSet.FieldByName('TaxPercent').IsNull then
    begin
      DataSet['TaxAmount'] := DataSet['AmountWages'] * DataSet['TaxPercent'] / 100;
      if (DataSet['MNSui'] = 118) and
           (GetYear(nvSuiPeriodEndDate) = 2009) and
             ((GetQuarterNumber(nvSuiPeriodEndDate) = 3) or
              (GetQuarterNumber(nvSuiPeriodEndDate) = 4)) then
      begin
        Rate := 0.005;
        LReal := RoundTwo(DataSet['AmountWages'] * Rate);
        if (DataSet['AmountWages'] > 62500) then
           LReal := LReal + (DataSet['AmountWages'] - 62500) * 0.0067;
        DataSet['TaxAmount'] := LReal;
      end
      else
      if (DataSet['MNSui'] = 118) and (CompareDate(nvSuiPeriodEndDate, EncodeDate(2013,  6, 30)) = GreaterThanValue) then
      begin
        Rate := 0;
        LReal := RoundTwo(DataSet['AmountWages'] * Rate);
        if (DataSet['AmountWages'] > 85000) then
           LReal := LReal + (DataSet['AmountWages'] - 85000) * 0.0117;
        DataSet['TaxAmount'] := LReal;
      end
      else      
      if (DataSet['MNSui'] = 118) and (nvSuiPeriodEndDate >= 40725)  then
      begin
        Rate := 0;
        LReal := RoundTwo(DataSet['AmountWages'] * Rate);
        if (DataSet['AmountWages'] > 62500) then
           LReal := LReal + (DataSet['AmountWages'] - 62500) * 0.0117;
        DataSet['TaxAmount'] := LReal;
      end
      else
      if DataSet['MNSui'] <> 0 then
        DataSet['TaxAmount'] := DataSet['TaxAmount'] + DataSet['TaxAmount'] * ConvertNull(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DataSet['MNSui'], 'FUTURE_DEFAULT_RATE'{Additional Assessment %}), 0)
                         + ConvertNull(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DataSet['MNSui'], 'FUTURE_MAXIMUM_WAGE'{Additional Assessment Amount}), 0);

      if (DataSet['SortKey'] = '0900') then
      begin
         if Assigned(cdEEMedicare) and  (cdEEMedicare.RecordCount > 0) then
         begin
           cdEEMedicare.SetRange([DataSet['CoNbr']],[DataSet['CoNbr']]);
           try
             cdEEMedicare.First;

             if Assigned(QEEMedicarePerson) and (not DataSet['SkipBalanceControl']) then
              if QEEMedicarePerson.Result.Locate('CL_PERSON_NBR;CO_NBR',
                              VarArrayOf([cdEEMedicare.FieldByName('CL_PERSON_NBR').Value,
                                          cdEEMedicare.FieldByName('CO_NBR').Value]),[])  then
                 DataSet['SkipBalanceControl'] := true;

             while not cdEEMedicare.Eof do
             begin
                if ((ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0) -
                        ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)) >
                            DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value ) then
                 aAmount := (ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)
                                            * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_RATE').Value)-
                             (ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)
                                            * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value)
                else
                begin
                 aAmount := 0;
                 if ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0)  >
                     DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value then
                 begin
                   qAmount := ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0) -
                                ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0);
                   cAmount := ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0) -
                                 (DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value - qAmount);
                   aAmount := (cAmount * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_RATE').Value) -
                               (cAmount * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value);
                 end;
                end;

                DataSet['TaxAmount'] := ConvertNull(DataSet['TaxAmount'], 0)  + aAmount;

                cdEEMedicare.Next;
             end;
           finally
             cdEEMedicare.CancelRange;
           end;
         end;
      end;

    end;
    if not DataSet.FieldByName('TaxAmount').IsNull and
       not DataSet.FieldByName('AmountTaxHist').IsNull then
        begin
          DataSet['DifWageHist'] := Abs(DataSet['TaxAmount'] - DataSet['AmountTaxHist']);
          if not DataSet.FieldByName('FICAEE_Wages_').IsNull then
          begin
              fAmount := 0;
              if (DataSet['SortKey'] = '0900') then
              begin
                 if Assigned(cdEEMedicare) and  (cdEEMedicare.RecordCount > 0) then
                 begin
                   cdEEMedicare.SetRange([DataSet['CoNbr']],[DataSet['CoNbr']]);
                   try
                     cdEEMedicare.First;

                     if Assigned(QEEMedicarePerson) and (not DataSet['SkipBalanceControl']) then
                       if QEEMedicarePerson.Result.Locate('CL_PERSON_NBR;CO_NBR',
                                      VarArrayOf([cdEEMedicare.FieldByName('CL_PERSON_NBR').Value,
                                                  cdEEMedicare.FieldByName('CO_NBR').Value]),[])  then
                          DataSet['SkipBalanceControl'] := true;

                     while not cdEEMedicare.Eof do
                     begin
                        if ((ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0)  -
                                ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)) >
                                    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value ) then
                           aAmount := (ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)
                                                    * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_RATE').Value)-
                                     (ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0)
                                                    * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value)
                        else
                        begin
                          aAmount := 0;
                          if ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0) >
                               DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value then
                          begin
                             qAmount := ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotal').Value, 0) -
                                           ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0);
                             cAmount := ConvertNull(cdEEMedicare.FieldByName('EeMedicateWagesTotalQuarter').Value, 0) -
                                           (DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value - qAmount);
                             aAmount := (cAmount * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_RATE').Value) -
                                           (cAmount * DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value);
                          end;
                        end;
                        if (cdEEMedicare.FieldByName('MAKEUP_FICA_ON_CLEANUP_PR').AsString[1] <> 'Y') then
                           fAmount := fAmount + aAmount;

                       cdEEMedicare.Next;
                     end;
                   finally
                     cdEEMedicare.CancelRange;
                   end;
                 end;
              end;

              DataSet['FICADifWageHist_'] :=
                    Abs( fAmount + ((ConvertNull(DataSet['AmountWages'],0) - ConvertNull(DataSet['FICAEE_Wages_'],0)) *  ConvertNull(DataSet['TaxPercent'],0) / 100) -
                                              (ConvertNull(DataSet['AmountTaxHist'],0) - ConvertNull(DataSet['AmountEE_Tax_'],0)));

          end;
          if not DataSet.FieldByName('AmountER_Tax_').IsNull then
             DataSet['DifWageHist_EE_ER'] :=
                Abs(DataSet['TaxAmount'] - ((DataSet['AmountTaxHist']-DataSet['AmountEE_Tax_'])+DataSet['AmountER_Tax_']));
        end;

  end
  else
    DataSet['DifLiabHist'] := null;
end;

procedure TBalanceDM.cdsW2BoxCalcFields(DataSet: TDataSet);
begin
  if DataSet.FieldByName('W2_BOX').AsString <> '' then
    DataSet['W2_BOX_CALC'] := DataSet['W2_BOX']
  else
    DataSet['W2_BOX_CALC'] := DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', DataSet['E_D_CODE_TYPE'], 'W2_BOX');
end;

function TBalanceDM.IsTaxableType(EDType: string): Boolean;
begin
  Result := (EDType >= 'DA') and (EDType <= 'DI') or (EDType = 'DL') or (EDType > 'DM');
end;

procedure TBalanceDM.cdsLiabCalcFields(DataSet: TDataSet);
begin
  if DM_PAYROLL.PR.Active and DM_PAYROLL.PR.Locate('PR_NBR', DataSet['PR_NBR'], []) then
  begin
    DataSet['RUN_NUMBER'] := DM_PAYROLL.PR['RUN_NUMBER'];
    DataSet['CHECK_DATE'] := DM_PAYROLL.PR['CHECK_DATE'];
  end;
end;

function TBalanceDM.WagesOutOfBalance(const ClNbr,
  CoNbr: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(WagesOOB) to High(WagesOOB) do
    if (WagesOOB[i].CL_NBR = ClNbr) and
       (WagesOOB[i].CO_NBR = CoNbr) then
    begin
      Result := True;
      Exit;
    end;
end;

procedure TBalanceDM.DataModuleCreate(Sender: TObject);
begin
  SetLength(WagesOOB, 0);
  cdsSummary.SkipFieldCheck := True;

  with cdsAggCheckLines.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'FEDERAL_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(FEDERAL_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'FEDERAL_TAX_Sum';
    Active := True;
    Expression := 'Sum(FEDERAL_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_EIC_TAX_Sum';
    Active := True;
    Expression := 'Sum(EE_EIC_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_OASDI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(EE_OASDI_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_OASDI_TAX_Sum';
    Active := True;
    Expression := 'Sum(EE_OASDI_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_MEDICARE_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(EE_MEDICARE_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_MEDICARE_TAX_Sum';
    Active := True;
    Expression := 'Sum(EE_MEDICARE_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_OASDI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(ER_OASDI_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_OASDI_TAX_Sum';
    Active := True;
    Expression := 'Sum(ER_OASDI_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_MEDICARE_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(ER_MEDICARE_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_MEDICARE_TAX_Sum';
    Active := True;
    Expression := 'Sum(ER_MEDICARE_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_FUI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(ER_FUI_TAXABLE_WAGES)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_FUI_TAX_Sum';
    Active := True;
    Expression := 'Sum(ER_FUI_TAX)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'BACKUP_WITHHOLDING_Sum';
    Active := True;
    Expression := 'Sum(BACKUP_WITHHOLDING)';
  end;

  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_OASDI_TAX_Sum_';
    Active := True;
    Expression := 'Sum(EE_OASDI_TAX_)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'EE_MEDICARE_TAX_Sum_';
    Active := True;
    Expression := 'Sum(EE_MEDICARE_TAX_)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_OASDI_TAX_Sum_';
    Active := True;
    Expression := 'Sum(ER_OASDI_TAX_)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'ER_MEDICARE_TAX_Sum_';
    Active := True;
    Expression := 'Sum(ER_MEDICARE_TAX_)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'FICAEE_OASDIWages_SUM';
    Active := True;
    Expression := 'Sum(FICAEE_OASDIWages_)';
  end;
  with cdsAggChecks.Aggregates.Add do
  begin
    AggregateName := 'FICAEE_MEDICAREWages_SUM';
    Active := True;
    Expression := 'Sum(FICAEE_MEDICAREWages_)';
  end;


  with cdsAggLiab.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'STATE_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(STATE_TAXABLE_WAGES)';
  end;
  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'STATE_TAX_Sum';
    Active := True;
    Expression := 'Sum(STATE_TAX)';
  end;
  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'EE_SDI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(EE_SDI_TAXABLE_WAGES)';
  end;
  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'EE_SDI_TAX_Sum';
    Active := True;
    Expression := 'Sum(EE_SDI_TAX)';
  end;
  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'ER_SDI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(ER_SDI_TAXABLE_WAGES)';
  end;
  with cdsAggCheckStates.Aggregates.Add do
  begin
    AggregateName := 'ER_SDI_TAX_Sum';
    Active := True;
    Expression := 'Sum(ER_SDI_TAX)';
  end;

  with cdsAggStateLiab.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggCheckLineStates.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggCheckSUI.Aggregates.Add do
  begin
    AggregateName := 'SUI_TAXABLE_WAGES_Sum';
    Active := True;
    Expression := 'Sum(SUI_TAXABLE_WAGES)';
  end;
  with cdsAggCheckSUI.Aggregates.Add do
  begin
    AggregateName := 'SUI_TAX_Sum';
    Active := True;
    Expression := 'Sum(SUI_TAX)';
  end;
  with cdsAggCheckSUI.Aggregates.Add do
  begin
    AggregateName := 'SUI_GROSS_WAGES_Sum';
    Active := True;
    Expression := 'Sum(SUI_GROSS_WAGES)';
  end;

  with cdsAggSUILiab.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggCheckLocal.Aggregates.Add do
  begin
    AggregateName := 'LOCAL_TAXABLE_WAGE_Sum';
    Active := True;
    Expression := 'Sum(LOCAL_TAXABLE_WAGE)';
  end;
  with cdsAggCheckLocal.Aggregates.Add do
  begin
    AggregateName := 'LOCAL_TAX_Sum';
    Active := True;
    Expression := 'Sum(LOCAL_TAX)';
  end;

  with cdsAggLocalLiab.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

  with cdsAggCheckLineLocal.Aggregates.Add do
  begin
    AggregateName := 'Summary';
    Active := True;
    Expression := 'Sum(Amount)';
  end;

end;

initialization
finalization
  dmBalanceQ.Free;
  dmBalanceA.Free;

end.
