unit EvVersionedEERateFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, EvExceptions;

type
  TEvVersionedEERate = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    rgPrimary: TevDBRadioGroup;
    lNumber: TevLabel;
    edNumber: TevDBEdit;
    lAmount: TevLabel;
    edAmount: TevDBEdit;
    pnlBottom: TevPanel;
    procedure FormShow(Sender: TObject);
  protected
    procedure DoOnSave; override;
  end;

implementation

{$R *.dfm}


{ TEvVersionedEERate }

procedure TEvVersionedEERate.FormShow(Sender: TObject);
begin
  inherited;

  Fields[0].AddValue('Title', rgPrimary.Caption);
  Fields[0].AddValue('Width', 15);

  Fields[1].AddValue('Title', lNumber.Caption);
  Fields[1].AddValue('Width', 20);

  Fields[2].AddValue('Title', lAmount.Caption);
  Fields[2].AddValue('Width', 20);
  Fields[2].AddValue('Required', True);  // It is nullable in DB?!

  rgPrimary.Caption := Iff(Fields[0].Value['Required'], '~', '') + rgPrimary.Caption;
  rgPrimary.DataField := Fields.ParamName(0);

  lNumber.Caption := Iff(Fields[1].Value['Required'], '~', '') + lNumber.Caption;
  edNumber.DataField := Fields.ParamName(1);

  lAmount.Caption := Iff(Fields[2].Value['Required'], '~', '') + lAmount.Caption;
  edAmount.DataField := Fields.ParamName(2);
end;

procedure TEvVersionedEERate.DoOnSave;
begin
  if (Data.FieldByName('PRIMARY_RATE').AsString = GROUP_BOX_YES) then
      if CheckPrimaryRateFlag(true, deBeginDate.Date) then
        raise EUpdateError.CreateHelp('Employee has a primary rate set up!', IDH_ConsistencyViolation);

  inherited;
end;

end.

