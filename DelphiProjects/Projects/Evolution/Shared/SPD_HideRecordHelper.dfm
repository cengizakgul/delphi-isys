object HideRecordHelper: THideRecordHelper
  Left = 0
  Top = 0
  Width = 116
  Height = 27
  AutoScroll = False
  TabOrder = 0
  object butnNext: TevBitBtn
    Left = 0
    Top = 0
    Width = 75
    Height = 25
    Action = HideCurRecord
    TabOrder = 0
    NumGlyphs = 2
    Layout = blGlyphRight
    Margin = 0
  end
  object evBitBtn1: TevBitBtn
    Left = 0
    Top = 39
    Width = 75
    Height = 25
    Action = UnhideCurRecord
    TabOrder = 1
    NumGlyphs = 2
    Layout = blGlyphRight
    Margin = 0
  end
  object evActionList1: TevActionList
    Left = 24
    object HideCurRecord: TAction
      Caption = 'Hide'
      OnExecute = HideCurRecordExecute
      OnUpdate = HideCurRecordUpdate
    end
    object UnhideCurRecord: TAction
      Caption = 'Unhide'
      OnExecute = UnhideCurRecordExecute
      OnUpdate = UnhideCurRecordUpdate
    end
  end
  object dsHelper: TevDataSource
    OnDataChange = dsHelperDataChange
    Left = 56
  end
end
