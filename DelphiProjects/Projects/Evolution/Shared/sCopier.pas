// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy
unit sCopier;
//TODO:1 should be only one kind of TProcess*Event thus we won't need bloated ancestors of TCopyHelper
//2 add non virtual GetDataSetsToReopen with ONE parameter to frame base class

interface

uses
  SShowDataset,
  ContNrs,  classes, Variants, SDDClasses, isVCLBugFix, evExceptions,
  EvContext, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TProcessRelationEvent = function(Sender: TObject; ForeignKeyValue: string;
    SelectedDetails, Details: TEvClientDataSet): integer of object;
  // Details         = the target dataset that is to be copied to
  // SelectedDetails = the records that are to be copied, the one selected is
  //                   the one to be copied to Details on callback
  // ForeignKey      = The key of the master record of the Details in the new
  //                   dataset. Should be copied to the appropriate field in the
  //                   Details dataset.
  // Result          = the value of the primairy key of the record that is
  //                   copied to.

  TProcessDetailEvent = procedure(Sender: TObject; client, company: integer; selectedDetails, details: TEvClientDataSet) of object;
  TProcessClientDetailEvent = function(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer of object;

  TRelationsListItem = class(TObject)
  private
    FTable: TddTableClass; // Relation table to be copied
    FOnCopy: TProcessRelationEvent;
  public
    property Table: TddTableClass read FTable write FTable;
    property OnCopy: TProcessRelationEvent read FOnCopy write FOnCopy;
  end;

  TRelationsList = class(TObject)
  private
    FList: TObjectList;
  protected
    function GetOwnsObjects: boolean;
    procedure SetOwnsObjects(const Value: boolean);
    function GetItems(Index: Integer): TRelationsListItem;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Items[Index: Integer]: TRelationsListItem read GetItems; default;
    property Count: integer read GetCount;
    property OwnsObjects: boolean read GetOwnsObjects write SetOwnsObjects;
    function Add(Item: TRelationsListItem): integer; overload;
    function Add(Table: TddTableClass; OnCopy: TProcessRelationEvent): integer; overload;
    procedure Assign(RelationsList: TRelationsList);
  end;

  TCopier = class
  public
    procedure BeforeCopy; virtual; abstract;
    procedure Copy; virtual; abstract;
    procedure AfterCopy; virtual; abstract;

    procedure BeforeDelete; virtual; abstract;
    procedure Delete; virtual; abstract;
    procedure AfterDelete; virtual; abstract;
  end;

  TBasicCopier = class( TCopier )
  protected
    FCurClient: integer;
    FTablesToSave: TArrayDS;

    procedure SaveDataSets; virtual;
    procedure RestoreDataSets; virtual;
  public
    procedure AddTablesToSave( dss: TArrayDS );
    procedure BeforeCopy; override;
    procedure AfterCopy; override;

    procedure BeforeDelete; override;
    procedure AfterDelete; override;
    function IsFieldToCopy(fn: string; DataSet: TEvClientDataSet): boolean; virtual;

    property Client: integer read FCurClient;
    property TablesToSave: TArrayDS read FTablesToSave write FTablesToSave;
  end;

  TBeforeCopyEvent = procedure( Sender: TObject; selectedDetails: TEvClientDataSet; params: TStringList ) of object;

  TCLBaseCopier = class( TBasicCopier )
  private
    FOnBeforeCopy: TBeforeCopyEvent;
    FdbgChoiceList: TevDBGrid; // if assigned, any UserInteraction should be
    // skipped and the dgChoiceList selection should be substituted where nesc.
  protected
    FDetails: TevClientDataSet;
    FUserFriendlyName: string;
    FSelectedList: TList;

    FParams: TStringList;
    procedure ClearParams;
    function AddParam( nbr: integer ): TStringList;
    function FindParam( nbr: integer ): TStringList;
    procedure DumpParams;

    procedure UserInteraction( bCopyOrDelete: boolean ); virtual; abstract;
    procedure DoBeforeCopy( SelectedDetails: TevClientDataSet ); virtual;

    property dbgChoiceList: TevDBGrid read FdbgChoiceList write FdbgChoiceList; // logically should be named dgChoiceList but renamed to dbgChoiceList to avoid all the hassle with the 'With.. Do..' statements in the code
  public
    constructor Create( aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string );
    destructor Destroy; override;

    procedure Copy; override;
    procedure Delete; override;

    property OnBeforeCopy: TBeforeCopyEvent read FOnBeforeCopy write FOnBeforeCopy;
  end;

  TCompanyDetailCopier = class( TCLBaseCopier )
  private
    FTargetCompanyNumber: string;

    FOnDeleteDetail: TProcessDetailEvent;
    FOnCopyDetail: TProcessDetailEvent;

    procedure DoCopyDetailTo( client, company: integer; selectedDetails: TEvClientDataSet );
    procedure DoDeleteDetailFrom( client, company: integer; selectedDetails: TEvClientDataSet );
    procedure DoCopyDetailsTo( client, company: integer; selectedDetails: TEvClientDataSet );
    procedure DoDeleteDetailsFrom( client, company: integer; selectedDetails: TEvClientDataSet );
  protected
    procedure SaveDataSets; override;
    procedure RestoreDataSets; override;
    procedure UserInteraction( bCopyOrDelete: boolean ); override;
  public
    constructor Create( aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string  );
    function IsFieldToCopy(fn: string; DataSet: TEvClientDataSet): boolean; override;
    property OnCopyDetail: TProcessDetailEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessDetailEvent read FOnDeleteDetail write FOnDeleteDetail;
    property TargetCompanyNumber: string read FTargetCompanyNumber write FTargetCompanyNumber;
  end;

  TClientDetailCopier = class( TCLBaseCopier )
  private
    FTargetClientNumber: string;
    FMasterKeyValue: string;
    FOnDeleteDetail: TProcessClientDetailEvent;
    FOnCopyDetail: TProcessClientDetailEvent;
    FOnCopyRelation: TProcessRelationEvent;
    FRelationsList: TRelationsList;
    function DoCopyDetailTo(client: integer; selectedDetails: TEvClientDataSet): integer; // The result = the value of the primairy key of the record that is copied to.
    procedure DoDeleteDetailFrom(client: integer; selectedDetails: TEvClientDataSet);
    procedure DoCopyDetailsTo(client: integer; selectedDetails: TEvClientDataSet);
    procedure DoDeleteDetailsFrom(client: integer; selectedDetails: TEvClientDataSet);
    property MasterKeyValue: string read FMasterKeyValue write FMasterKeyValue;
  protected
    procedure SaveDataSets; override;
    procedure RestoreDataSets; override;
    procedure UserInteraction( bCopyOrDelete: boolean ); override;
    procedure CopyRelations(Client: integer; PrimaryKeyValue: integer; SelectedDetails: TevClientDataSet); // To be able to copy relation SelectedDetails will have to contain field with fieldname KeyField!
    function CopyRelationDetail(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer; // To help fire the OnCopyDetail event
  public
    constructor Create( aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string  );
    destructor Destroy; override;
    function IsFieldToCopy(fn: string; DataSet: TEvClientDataSet): boolean; override;
    property OnCopyDetail: TProcessClientDetailEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessClientDetailEvent read FOnDeleteDetail write FOnDeleteDetail;
    property OnCopyRelation: TProcessRelationEvent read FOnCopyRelation write FOnCopyRelation;
    property TargetClientNumber: string read FTargetClientNumber write FTargetClientNumber;
    property RelationsList: TRelationsList read FRelationsList;
  end;

  TProcessDBDTEvent = procedure( Sender: TObject; nbr: integer; selectedDetails, details: TEvClientDataSet; params: TStringList  ) of object;

  TDBDTCopier = class( TCLBaseCopier )
  private
    FTargetDBDTNumber: string;

    FDBDTLevel: char;
    FTargetUserFriendlyName: string;

    FParentKeyField: string;
    FParentDataSet: TevClientDataSet;

    FOnDeleteDetail: TProcessDBDTEvent;
    FOnCopyDetail: TProcessDBDTEvent;

    procedure DoCopyDetailTo( nbr: integer; selectedDetails: TEvClientDataSet );
    procedure DoDeleteDetailFrom( nbr: integer; selectedDetails: TEvClientDataSet );
    procedure DoCopyDetailsTo( nbr: integer; selectedDetails: TEvClientDataSet );
    procedure DoDeleteDetailsFrom( nbr: integer; selectedDetails: TEvClientDataSet );
    procedure SetDetailsCondition( nbr: integer );
  protected
    procedure SaveDataSets; override;
    procedure RestoreDataSets; override;
    procedure UserInteraction( bCopyOrDelete: boolean ); override;
  public
    constructor Create( aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string  );

    function IsFieldToCopy(fn: string; DataSet: TEvClientDataSet): boolean; override;
    property OnCopyDetail: TProcessDBDTEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessDBDTEvent read FOnDeleteDetail write FOnDeleteDetail;
    property TargetDBDTNumber: string read FTargetDBDTNumber;
  end;

function Plural( s: string ): string;

implementation

uses
  sDataStructure, evutils, SPD_CompanyChoiceQuery, sysutils, controls, SPD_ClientChoiceQuery, db, SPD_DBDTChoiceQuery, EvConsts;
{ TBasicCopier }

procedure TBasicCopier.AddTablesToSave(dss: TArrayDS);
var
  i: integer;
begin
  for i := low( dss ) to high( dss ) do
    AddDS( dss[i], FTablesToSave );
end;

procedure TBasicCopier.AfterCopy;
begin
  RestoreDataSets;
end;

procedure TBasicCopier.AfterDelete;
begin
  RestoreDataSets;
end;

procedure TBasicCopier.BeforeCopy;
begin
  SaveDataSets;
end;

procedure TBasicCopier.BeforeDelete;
begin
  SaveDataSets;
end;

procedure TBasicCopier.RestoreDataSets;
//var
//  i: integer;
begin
  ctx_DataAccess.OpenClient( FCurClient );
  DM_CLIENT.CL.LoadState;
  LoadDSStates( FTablesToSave );
  //for i:=low(FTablesToSave)  to high(FTablesToSave) do
    //ODS('FTablesToSave[%d]=%s',[i,FTablesToSave[i].TableName]);
end;

procedure TBasicCopier.SaveDataSets;
begin
  FCurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  DM_CLIENT.CL.SaveState;
  SaveDSStates( FTablesToSave );
end;

function TBasicCopier.IsFieldToCopy( fn: string; DataSet: TEvClientDataSet ):boolean;
begin
  Result := (fn <> 'EFFECTIVE_DATE') and (fn <> 'EFFECTIVE_UNTIL') and (fn <> 'REC_VERSION') and ( fn <> DataSet.TableName + '_NBR' )
         and (DataSet.FieldByName(fn).FieldKind = fkData );
end;

{ TCLBaseCopier }

constructor TCLBaseCopier.Create( aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string );
begin
  FParams := TStringList.Create;

  FDetails := aDetails;
  FSelectedList := aSelectedList;
  FUserFriendlyName := aUserFriendlyName;
  AddDS( FDetails, FTablesToSave );
end;

destructor TCLBaseCopier.Destroy;
begin
  ClearParams;
  FreeAndNil( FParams );
  inherited;
end;

procedure TCLBaseCopier.ClearParams;
var
  i: integer;
begin
  for i := 0 to FParams.count-1 do
  begin
    FParams.Objects[i].Free;
    FParams.Objects[i] := nil;
  end;
  FParams.Clear;
end;

procedure TCLBaseCopier.Copy;
begin
  UserInteraction( true );
end;

procedure TCLBaseCopier.Delete;
begin
  UserInteraction( false );
end;

procedure TCLBaseCopier.DoBeforeCopy(SelectedDetails: TevClientDataSet);
var
  nbr: integer;
begin
  ClearParams;
  if assigned( FOnBeforeCopy ) then
  begin
    selectedDetails.First;
    while not selectedDetails.Eof do
    begin
      nbr := selectedDetails.FieldByNAme( FDetails.TableNAme+'_NBR').AsInteger;
      FOnBeforeCopy( Self, selectedDetails, AddParam(nbr) );
      selectedDetails.Next;
    end;
  end;
//  DumpParams;
end;

function TCLBaseCopier.AddParam( nbr: integer ): TStringList;
begin
  Result := TStringList.Create;
  FParams.AddObject( IntToStr(nbr), Result );
end;

function TCLBaseCopier.FindParam(nbr: integer): TStringList;
var
  i: integer;
begin
  i := FParams.IndexOf( IntToStr( nbr ) );
  if i <> -1 then
    Result := FParams.Objects[i] as TStringList
  else
    Result := nil;
end;

procedure TCLBaseCopier.DumpParams;
var
  p: TStringList;
  i,j: integer;
begin
  for i := 0 to FParams.Count -1 do
  begin
    p := FParams.Objects[i] as TStringList;
    ODS('Params for %s', [FParams[i]]);
    if assigned(p) then
      for j := 0  to p.count -1 do
        ODS( p[j])
    else
      ODS('no stringlist!!!');
  end;
end;

{ TCompanyDetailCopier }

constructor TCompanyDetailCopier.Create(aDetails: TevClientDataSet; aSelectedList: TList; aUserFriendlyName: string );
begin
  inherited Create( aDetails, aSelectedList, aUserFriendlyName );
  AddDS( DM_COMPANY.CO, FTablesToSave );
end;

procedure TCompanyDetailCopier.SaveDataSets;
begin
  FDetails.Cancel;
  inherited;
end;

procedure TCompanyDetailCopier.RestoreDataSets;
begin
  inherited;
  DM_TEMPORARY.TMP_CO.Locate('CO_NBR;CL_NBR', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').AsString, FCurClient ]), []);
end;

procedure TCompanyDetailCopier.UserInteraction( bCopyOrDelete: boolean );
var
  SelectedDetails: TevClientDataSet;
  k: integer;
begin
  with TCompanyChoiceQuery.Create( nil ) do
  try
    if bCopyOrDelete then
    begin
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      Caption := Format('Select the companies that the %s(s) will be copied to.',[FUserFriendlyName]);
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &All';
      btnAll.Visible := False;
      ConfirmAllMessage := Format('This operation will copy the %ss to all clients and companies. Are you sure you want to do this?',[FUserFriendlyName]);
    end
    else
    begin
      SetFilter( '' );
      Caption := Format('Select the companies that the %s(s) will be deleted from.',[FUserFriendlyName]);
      btnNo.Caption := '&Cancel';
      btnYes.Caption := '&Delete';
      btnAll.Caption := 'Delete from &All';
      btnAll.Visible := False;
      ConfirmAllMessage := Format('This operation will delete the %ss from all clients and companies. Are you sure you want to do this?',[FUserFriendlyName]);
    end;
    if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
    begin
      FDetails.DisableControls;
      try
        SelectedDetails := CreateDataSetWithSelectedRows( FDetails, FSelectedList );
        try
          DM_TEMPORARY.TMP_CO.DisableControls;
          try
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                FTargetCompanyNumber := cdsChoiceList.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
                if bCopyOrDelete then
                  DoCopyDetailsTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger, SelectedDetails )
                else
                  DoDeleteDetailsFrom( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger, SelectedDetails )
              end;
            except
              ctx_DataAccess.CancelDataSets( FTablesToSave );
              raise;
            end;
          finally
            DM_TEMPORARY.TMP_CO.EnableControls;
          end;
        finally
          SelectedDetails.Free;
        end;
      finally
        FDetails.EnableControls;
      end;
    end;
  finally
    Free;
  end;

end;

procedure TCompanyDetailCopier.DoCopyDetailsTo( client, company: integer; selectedDetails: TEvClientDataSet );
begin
  ctx_DataAccess.OpenClient( client );
  if assigned(FDetails.FindField('CO_NBR')) then
    FDetails.EnsureDSCondition('CO_NBR=' + intToStr(company))
  else
    FDetails.EnsureDSCondition('');

  selectedDetails.First;
  while not selectedDetails.Eof do
  begin
    DoCopyDetailTo( client, company, selectedDetails );
    selectedDetails.Next;
  end;
  ctx_DataAccess.PostDataSets([FDetails]);
end;

procedure TCompanyDetailCopier.DoDeleteDetailsFrom( client, company: integer; selectedDetails: TEvClientDataSet );
begin
  ctx_DataAccess.OpenClient( client );
  if assigned(FDetails.FindField('CO_NBR') ) then
    FDetails.EnsureDSCondition('CO_NBR=' + intToStr(company))
  else
    FDetails.EnsureDSCondition('');
  selectedDetails.First;
  while not selectedDetails.Eof do
  begin
    DoDeleteDetailFrom( client, company, selectedDetails );
    selectedDetails.Next;
  end;
  ctx_DataAccess.PostDataSets([FDetails]);
end;

procedure TCompanyDetailCopier.DoCopyDetailTo(client, company: integer;
  selectedDetails: TEvClientDataSet);
begin
  Assert( assigned( FOnCopyDetail ) );
  if assigned( FOnCopyDetail ) then
    FOnCopyDetail( Self, client, company, selectedDetails, FDetails )
end;

procedure TCompanyDetailCopier.DoDeleteDetailFrom(client, company: integer;
  selectedDetails: TEvClientDataSet);
begin
  Assert( assigned( FOnDeleteDetail ) );
  if assigned( FOnDeleteDetail ) then
    FOnDeleteDetail( Self, client, company, selectedDetails, FDetails )
end;

function TCompanyDetailCopier.IsFieldToCopy(fn: string;
  DataSet: TEvClientDataSet): boolean;
begin
  Result := inherited IsFieldToCopy( fn, DataSet) and ( fn <> 'CO_NBR' );
end;

{ TClientDetailCopier }

constructor TClientDetailCopier.Create(aDetails: TevClientDataSet;
  aSelectedList: TList; aUserFriendlyName: string);
begin
  inherited Create( aDetails, aSelectedList, aUserFriendlyName );
  FRelationsList := TRelationsList.Create;
  AddDS( DM_CLIENT.CL, FTablesToSave );
end;

destructor TClientDetailCopier.Destroy;
begin
  FreeAndNil(FRelationsList);
  inherited;
end;

procedure TClientDetailCopier.CopyRelations(Client: integer; PrimaryKeyValue: integer; SelectedDetails: TevClientDataSet);
// Client          = Client to be copied to
// PrimaryKeyValue = Primary key value of record that subrecords are to be
//                   linked to.
// SelectedDetails = (From) the records that are to be copied

// FDetails        = (To) the target dataset that is copied to
// evClientDataSet = (Detail From) SelectedDetails for the relationscopier

  function CreateevClientDataSet(TableClass: TddTableClass): TevClientDataSet;
  begin
    Result := TevClientDataSet.Create(nil);
    try
      Result.ProviderName := TableClass.GetProviderName;
    except
      FreeAndNil(Result);
      raise;
    end;
  end;

var
  I, J: integer;
  MasterTableName: string;
  Bookmark: TBookmark;
  BookmarkList: TList;
  evClientDataSet: TevClientDataSet;
  RelationsCopier: TClientDetailCopier;
begin
  if RelationsList.Count > 0 then
  begin
    ctx_DataAccess.OpenClient(Client);
    FDetails.Open;
    MasterTableName := FDetails.TableName;
    FDetails.Locate(MasterTableName + '_NBR', VarArrayOf([PrimaryKeyValue]), []);
    for I := 0 to RelationsList.Count - 1 do begin
      // Step 1: Reset back to the current Client Database
      ctx_DataAccess.OpenClient(FCurClient);
      // Step 2: Create a ClientDataSet with provider
      evClientDataSet := CreateevClientDataSet(RelationsList[I].Table);
      try
        // Step 3: Set the RetrieveCondition to only load the relevant relation
        // records from the relation table
        evClientDataSet.RetrieveCondition := MasterTableName + '_NBR = ''' +
          SelectedDetails.FieldByName(MasterTableName + '_NBR').AsString + '''';
        // Step 4: Add all records to the Bookmarklist
        BookmarkList := TList.Create;
        try
          evClientDataSet.Open;
          evClientDataSet.First;
          while not evClientDataSet.Eof do
          begin
            Bookmark := evClientDataSet.GetBookmark;
            try
              BookmarkList.Add(Bookmark);
            except
              try
                BookmarkList.Remove(Bookmark);
              finally
                evClientDataSet.FreeBookmark(Bookmark);
              end;
              raise;
            end;
            evClientDataSet.Next;
          end;
          // Step 5: Create a new ClientDetailCopier
          RelationsCopier := TClientDetailCopier.Create(
            evClientDataSet, BookmarkList, FUserFriendlyName);
          try
            // Step 6: Make sure the same choice is applied and the user doesn't
            // get another choice window
            RelationsCopier.dbgChoiceList := dbgChoiceList;
            // Step 7: Make sure the OnCopyDetail event is properly fired
            RelationsCopier.MasterKeyValue := IntToStr(PrimaryKeyValue);
            RelationsCopier.OnCopyRelation := RelationsList[I].OnCopy;
            RelationsCopier.OnCopyDetail := RelationsCopier.CopyRelationDetail;
            // Step 8: Fire the copy action
            RelationsCopier.Copy;
          finally
            FreeAndNil(RelationsCopier);
          end;
        finally
          try
            for J := BookmarkList.Count -1 downto 0 do
            begin
              evClientDataSet.FreeBookmark(BookmarkList[J]);
            end;
          finally
            FreeAndNil(BookmarkList);
          end;
        end;
      finally
        FreeAndNil(evClientDataSet);
      end;
    end;
  end;
end;

procedure TClientDetailCopier.UserInteraction(bCopyOrDelete: boolean);
var
  ClientChoiceQuery: TClientChoiceQuery;
  SelectedDetails: TevClientDataSet; // represents the table to copy
  k: integer;
begin
  ClientChoiceQuery := TClientChoiceQuery.Create(nil);
  try
    if bCopyOrDelete then
    begin
      ClientChoiceQuery.SetFilter( 'CUSTOM_CLIENT_NUMBER<>'''+DM_CLIENT.CL.FieldByName('CUSTOM_CLIENT_NUMBER').AsString+'''' );
      ClientChoiceQuery.Caption := Format('Select the clients that the %s(s) will be copied to.',[FUserFriendlyName]);
      ClientChoiceQuery.btnNo.Caption := '&Cancel';
      ClientChoiceQuery.btnYes.Caption := 'C&opy';
      ClientChoiceQuery.btnAll.Caption := 'Copy to &All';
      ClientChoiceQuery.btnAll.Visible := False;
      ClientChoiceQuery.ConfirmAllMessage := Format('This operation will copy the %ss to all clients. Are you sure you want to do this?',[FUserFriendlyName]);
    end
    else
    begin
      ClientChoiceQuery.SetFilter( '' );
      ClientChoiceQuery.Caption := Format('Select the clients that the %s(s) will be deleted from.',[FUserFriendlyName]);
      ClientChoiceQuery.btnNo.Caption := '&Cancel';
      ClientChoiceQuery.btnYes.Caption := '&Delete';
      ClientChoiceQuery.btnAll.Caption := 'Delete from &All';
      ClientChoiceQuery.btnAll.Visible := False;
      ClientChoiceQuery.ConfirmAllMessage := Format('This operation will delete the %ss from all clients. Are you sure you want to do this?',[FUserFriendlyName]);
    end;
    if not Assigned(dbgChoiceList) then
    begin
      dbgChoiceList := ClientChoiceQuery.dgChoiceList;
      if not (ClientChoiceQuery.ShowModal = mrYes) then
        dbgChoiceList.SelectedList.Clear;
    end;
    if (dbgChoiceList.SelectedList.Count>0) then
    begin
      FDetails.DisableControls;
      try
        SelectedDetails := CreateDataSetWithSelectedRows( FDetails, FSelectedList );
        DoBeforeCopy( SelectedDetails );
        try
          DM_TEMPORARY.TMP_CL.DisableControls;
          try
            try
              for k := 0 to dbgChoiceList.SelectedList.Count - 1 do
              begin
                dbgChoiceList.DataSource.DataSet.GotoBookmark(dbgChoiceList.SelectedList[k]);
                //ODS('handling: client=%d',[dbgChoiceList.DataSource.DataSet.FieldByName('CL_NBR').AsInteger]);
                FTargetClientNumber := dbgChoiceList.DataSource.DataSet.FieldByName('CUSTOM_CLIENT_NUMBER').AsString;
                if bCopyOrDelete then
                  DoCopyDetailsTo(dbgChoiceList.DataSource.DataSet.FieldByName('CL_NBR').AsInteger, SelectedDetails)
                else
                  DoDeleteDetailsFrom(dbgChoiceList.DataSource.DataSet.FieldByName('CL_NBR').AsInteger,  SelectedDetails )
              end;
            except
              ctx_DataAccess.CancelDataSets( FTablesToSave );
              raise;
            end;
          finally
            DM_TEMPORARY.TMP_CL.EnableControls;
          end;
        finally
          SelectedDetails.Free;
        end;
      finally
        FDetails.EnableControls;
      end;
    end;
  finally
    FreeAndNil(ClientChoiceQuery);
  end;
end;

procedure TClientDetailCopier.DoCopyDetailsTo(client: integer;
  selectedDetails: TEvClientDataSet);
// SelectedDetails = the records that are to be copied
// FDetails = the target dataset that is copied to
var
  PrimaryKeyValue: integer;
begin
  ctx_DataAccess.OpenClient(Client);
  if Assigned(FDetails.FindField('CL_NBR')) then
    FDetails.EnsureDSCondition('CL_NBR=' + IntToStr(Client))
  else
    FDetails.EnsureDSCondition('');
  SelectedDetails.First;
  while not SelectedDetails.Eof do
  begin
    PrimaryKeyValue := DoCopyDetailTo(Client, SelectedDetails);
    ctx_DataAccess.PostDataSets([FDetails]);
    CopyRelations(Client, PrimaryKeyValue, SelectedDetails);
    SelectedDetails.Next;
  end;
end;

function TClientDetailCopier.DoCopyDetailTo(client: integer;
  selectedDetails: TEvClientDataSet): integer;
// SelectedDetails = the records that are to be copied
// FDetails = the target dataset that is copied to
// Result = the value of the primairy key of the record that is copied to.
begin
  Assert(Assigned(FOnCopyDetail), 'OnCopyDetail not assigned');
  if not Assigned(FOnCopyDetail) then raise EevException.Create('OnCopyDetail not assigned');
  FDetails.Open;
  Result := FOnCopyDetail(Self, client, selectedDetails, FDetails,
    FindParam(selectedDetails.FieldByNAme(FDetails.TableName+'_NBR').AsInteger));
end;

procedure TClientDetailCopier.DoDeleteDetailsFrom(client: integer;
  selectedDetails: TEvClientDataSet);
begin
  ctx_DataAccess.OpenClient( client );
  if assigned(FDetails.FindField('CL_NBR') ) then
    FDetails.EnsureDSCondition('CL_NBR=' + intToStr(client) )
  else
    FDetails.EnsureDSCondition('');

  selectedDetails.First;
  while not selectedDetails.Eof do
  begin
    DoDeleteDetailFrom( client, selectedDetails );
    selectedDetails.Next;
  end;
  ctx_DataAccess.PostDataSets([FDetails]);
end;

procedure TClientDetailCopier.DoDeleteDetailFrom(client: integer;
  selectedDetails: TEvClientDataSet);
begin
  Assert( assigned( FOnDeleteDetail ) );
  if assigned( FOnDeleteDetail ) then
    FOnDeleteDetail( Self, client, selectedDetails, FDetails, nil )
end;

procedure TClientDetailCopier.SaveDataSets;
begin
  FDetails.Cancel;
  inherited;
end;

procedure TClientDetailCopier.RestoreDataSets;
begin
  inherited;
  DM_TEMPORARY.TMP_CL.Locate('CL_NBR', VarArrayOf([FCurClient]), []);
end;

function TClientDetailCopier.IsFieldToCopy(fn: string;
  DataSet: TEvClientDataSet): boolean;
begin
  Result := inherited IsFieldToCopy( fn, DataSet) and ( fn <> 'CL_NBR' );
end;

function TClientDetailCopier.CopyRelationDetail(Sender: TObject;
  client: integer; selectedDetails, details: TEvClientDataSet;
  params: TStringList): integer;
begin
  Assert(Assigned(OnCopyRelation), 'OnCopyRelation not assigned');
  if not Assigned(OnCopyRelation) then raise EevException.Create('OnCopyDetail not assigned');
  Result := OnCopyRelation(Sender, MasterKeyValue, SelectedDetails, Details);
end;

{ TDBDTCopier }

procedure TDBDTCopier.RestoreDataSets;
//var
//  i: integer;
begin
{  ods('restore ds');
  for i:=low(FTablesToSave)  to high(FTablesToSave) do
    if FTablesToSave[i].Active then
      ODS('FTablesToSave[%d]=%s retrcond=<%s> recordcount=<%d>',[i,FTablesToSave[i].TableName, FTablesToSave[i].RetrieveCondition, FTablesToSave[i].Recordcount ])
    else
      ODS('FTablesToSave[%d]=%s retrcond=<%s> closed',[i,FTablesToSave[i].TableName, FTablesToSave[i].RetrieveCondition  ]);
}
  LoadDSStates( FTablesToSave );
{  ods('after restore ds');
  for i:=low(FTablesToSave)  to high(FTablesToSave) do
    if FTablesToSave[i].Active then
      ODS('FTablesToSave[%d]=%s retrcond=<%s> recordcount=<%d>',[i,FTablesToSave[i].TableName, FTablesToSave[i].RetrieveCondition, FTablesToSave[i].Recordcount ])
    else
      ODS('FTablesToSave[%d]=%s retrcond=<%s> closed',[i,FTablesToSave[i].TableName, FTablesToSave[i].RetrieveCondition  ]);
}
end;

procedure TDBDTCopier.SaveDataSets;
begin
  FDetails.Cancel;
  SaveDSStates( FTablesToSave );
end;

function TDBDTCopier.IsFieldToCopy(fn: string;
  DataSet: TEvClientDataSet): boolean;
var
  Parents: TddReferencesDesc;
  i: Integer;
begin
//!! actually this code should be in TCLBaseCopier.IsFieldToCopy thus eliminating
// nearly all needs to override this method but I won't make unnecessary changes before release ;-)
  Result := True;
  Parents := (DataSet as TddTable).GetParentsDesc;
  for i := 0 to High(Parents) do
    if fn = Parents[i].SrcFields[0] then
    begin
      Result := False;
      Break;
    end;

  Result := inherited IsFieldToCopy( fn, DataSet) and Result;
end;

procedure TDBDTCopier.UserInteraction(bCopyOrDelete: boolean);
var
  SelectedDetails: TevClientDataSet;
  k: integer;
  flt: string;
  Oldflt: String;
begin
  Oldflt := FDetails.RetrieveCondition;
  with TDBDTChoiceQuery.Create( nil ) do
  try
    if bCopyOrDelete then
    begin
      flt := FParentKeyField +'<>'+ FParentDataSet.FieldByName(FParentKeyField).AsString;
      flt := StringReplace( flt, 'CO_','', [rfReplaceAll, rfIgnoreCase]);
      SetFilter( FDBDTLevel, flt );
      Caption := Format('Select the %s you want these %s copied to',[Plural(FTargetUserFriendlyName),Plural(FUserFriendlyName)]);
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &All';
      btnAll.Visible := False;
      ConfirmAllMessage := Format('This operation will copy the %s to all %s. Are you sure you want to do this?',[Plural(FUserFriendlyName), Plural(FTargetUserFriendlyName)]);
    end
    else
    begin
      SetFilter( FDBDTLevel, '' );
      Caption := Format('Select the %s you want these %s deleted from',[Plural(FTargetUserFriendlyName),Plural(FUserFriendlyName)]);
      btnNo.Caption := '&Cancel';
      btnYes.Caption := '&Delete';
      btnAll.Caption := 'Delete from &All';
      btnAll.Visible := False;
      ConfirmAllMessage := Format('This operation will delete these %s from all %s. Are you sure you want to do this?',[Plural(FUserFriendlyName), Plural(FTargetUserFriendlyName)]);
    end;
    if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
    begin
      FDetails.RetrieveCondition := Oldflt;
      FDetails.DisableControls;
      try
        SelectedDetails := CreateDataSetWithSelectedRows( FDetails, FSelectedList );
        try
          try
            for k := 0 to dgChoiceList.SelectedList.Count - 1 do
            begin
              cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
              FTargetDBDTNumber := cdsChoiceList.FieldByName('DBDT_CUSTOM_NUMBERS').AsString;
//              FDetails.ParentDataset.Close; - very bad!!!
              if bCopyOrDelete then
                DoCopyDetailsTo( cdsChoiceList.FieldByName( StringReplace( FParentKeyField, 'CO_','', [rfReplaceAll, rfIgnoreCase]) ).AsInteger, SelectedDetails )
              else
                DoDeleteDetailsFrom( cdsChoiceList.FieldByName( StringReplace( FParentKeyField, 'CO_','', [rfReplaceAll, rfIgnoreCase]) ).AsInteger, SelectedDetails )
            end;
          except
            ctx_DataAccess.CancelDataSets( FTablesToSave );
            raise;
          end;
        finally
          SelectedDetails.Free;
        end;
      finally
        FDetails.EnableControls;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TDBDTCopier.DoCopyDetailsTo(nbr: integer;
  selectedDetails: TEvClientDataSet);
begin
  SetDetailsCondition( nbr );

  selectedDetails.First;
  while not selectedDetails.Eof do
  begin
    DoCopyDetailTo( nbr, selectedDetails );
    selectedDetails.Next;
  end;
//  ODS('before ctx_DataAccess.PostDataSets(FDetails): retr=<%s> count=<%d> nbr=<%s>',[FDetails.RetrieveCondition, FDetails.RecordCount, FDetails.FieldByName('CO_DEPARTMENT_NBR').AsString]);
  ctx_DataAccess.PostDataSets([FDetails]);
//  ODS('after ctx_DataAccess.PostDataSets(FDetails): retr=<%s> count=<%d> nbr=<%s>',[FDetails.RetrieveCondition, FDetails.RecordCount, FDetails.FieldByName('CO_DEPARTMENT_NBR').AsString]);
end;

procedure TDBDTCopier.DoCopyDetailTo(nbr: integer;
  selectedDetails: TEvClientDataSet);
begin
  Assert( assigned( FOnCopyDetail ) );
  if assigned( FOnCopyDetail ) then
    FOnCopyDetail( Self, nbr, selectedDetails, FDetails, FindParam(selectedDetails.FieldByNAme( FDetails.TableNAme+'_NBR').AsInteger ));
end;

procedure TDBDTCopier.SetDetailsCondition( nbr: integer );

  procedure SmartEnsureDSCondition( DataSet: TevClientDataSet; cond: string );
  begin
    if not DSConditionsEqual( DataSet.OpenCondition, ''  ) or not DataSet.Active then
      DataSet.DataRequired(cond )
    else
      DataSet.RetrieveCondition := cond;
  end;

begin
  if assigned(FDetails.FindField(FParentKeyField) ) then
    SmartEnsureDSCondition( FDetails, FParentKeyField + '=' + intToStr(nbr) )
  else
    SmartEnsureDSCondition( FDetails, '' );
end;

procedure TDBDTCopier.DoDeleteDetailsFrom(nbr: integer;
  selectedDetails: TEvClientDataSet);
begin
  SetDetailsCondition( nbr );
  selectedDetails.First;
  while not selectedDetails.Eof do
  begin
    DoDeleteDetailFrom( nbr, selectedDetails );
    selectedDetails.Next;
  end;
  ctx_DataAccess.PostDataSets([FDetails]);

end;

procedure TDBDTCopier.DoDeleteDetailFrom(nbr: integer;
  selectedDetails: TEvClientDataSet);
begin
  Assert( assigned( FOnDeleteDetail ) );
  if assigned( FOnDeleteDetail ) then
    FOnDeleteDetail( Self, nbr, SelectedDetails, FDetails, nil )
end;

constructor TDBDTCopier.Create(aDetails: TevClientDataSet;
  aSelectedList: TList; aUserFriendlyName: string);
begin
  inherited Create( aDetails, aSelectedList, aUserFriendlyName );

  if assigned( FDetails ) then
  begin
    if FDetails.TableName = 'CO_BRANCH' then
    begin
      FDBDTLevel := CLIENT_LEVEL_DIVISION;
      FUserFriendlyName := 'Branch';
      FTargetUserFriendlyName := 'Division';
      FParentKeyField := 'CO_DIVISION_NBR';
      FParentDataSet := DM_COMPANY.CO_DIVISION;
      AddDS( DM_COMPANY.CO_DIVISION, FTablesToSave );
    end
    else if FDetails.TableName = 'CO_DEPARTMENT' then
    begin
      FDBDTLevel := CLIENT_LEVEL_BRANCH;
      FUserFriendlyName := 'Department';
      FTargetUserFriendlyName := 'Branch';
      FParentKeyField := 'CO_BRANCH_NBR';
      FParentDataSet := DM_COMPANY.CO_BRANCH;
      AddDS( DM_COMPANY.CO_BRANCH, FTablesToSave );
    end
    else if FDetails.TableName = 'CO_TEAM' then
    begin
      FDBDTLevel := CLIENT_LEVEL_DEPT;
      FUserFriendlyName := 'Team';
      FTargetUserFriendlyName := 'Department';
      FParentKeyField := 'CO_DEPARTMENT_NBR';
      FParentDataSet := DM_COMPANY.CO_DEPARTMENT;
      AddDS( DM_COMPANY.CO_DEPARTMENT, FTablesToSave );
    end
    else
      AbortEx;
  end;
end;

function Plural( s: string ): string; //not complete --doesnt handle -y
const
  es_suffixes: array [0..4] of string = (
    'o','s','ch','sh','x'
  );
var
  i: integer;
begin
  for i := low(es_suffixes) to high(es_suffixes) do
    if copy( s, Length(s)-Length(es_suffixes[i])+1, Length(es_suffixes[i]) ) = es_suffixes[i] then
    begin
      Result := s + 'es';
      exit;
    end;
  Result := s + 's';
end;

constructor TRelationsList.Create;
begin
  inherited Create;
  FList := TObjectList.Create(True);
end;

destructor TRelationsList.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

function TRelationsList.Add(Table: TddTableClass;
  OnCopy: TProcessRelationEvent): integer;
var
  RelationsListItem: TRelationsListItem;
begin
  RelationsListItem := TRelationsListItem.Create;
  try
    Result := Add(RelationsListItem);
  except
    FreeAndNil(RelationsListItem);
    raise;
  end;
  RelationsListItem.Table := Table;
  RelationsListItem.OnCopy := OnCopy;
end;

function TRelationsList.Add(Item: TRelationsListItem): integer;
begin
  Result := FList.Add(Item);
end;

function TRelationsList.GetCount: integer;
begin
  Result := FList.Count;
end;

function TRelationsList.GetItems(Index: Integer): TRelationsListItem;
begin
  Result := FList[Index] as TRelationsListItem;
end;

function TRelationsList.GetOwnsObjects: boolean;
begin
  Result := FList.OwnsObjects;
end;

procedure TRelationsList.Assign(RelationsList: TRelationsList);
begin
  FList.Assign(RelationsList.FList);
end;

procedure TRelationsList.SetOwnsObjects(const Value: boolean);
begin
  FList.OwnsObjects := Value;
end;

end.
