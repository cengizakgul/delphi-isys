// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR_BASE;

interface

uses
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, SDataStructure, Db,
  Wwdatsrc, SysUtils, Windows, SPackageEntry, EvUtils, DBActns, ActnList,
  SPD_EDIT_Open_BASE, Variants, SDDClasses, ISBasicClasses,
  SDataDictclient, SDataDicttemp, EvDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  Forms, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_PR_BASE = class(TEDIT_OPEN_BASE)
    dsCL: TevDataSource;
    DM_CLIENT: TDM_CLIENT;
    DM_COMPANY: TDM_COMPANY;
    PRGrid: TevDBGrid;
    wwdsSubMaster: TevDataSource;
    DM_PAYROLL: TDM_PAYROLL;
    lablCompany: TevLabel;
    dbtxCompanyNumber: TevDBText;
    CompanyNameText: TevDBText;
    lablClient: TevLabel;
    dbtxClientNbr: TevDBText;
    dbtxClientName: TevDBText;
    ActionList: TevActionList;
    PRNext: TDataSetNext;
    PRPrior: TDataSetPrior;
    txCheckDate: TevDBText;
    txRunNumber: TevDBText;
    evLabel1: TevLabel;
    procedure PRGridDblClick(Sender: TObject);
    procedure PRGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PRNextUpdate(Sender: TObject);
  protected
    procedure ReopenPRChecks; virtual;
    procedure ReopenPRCheckWithLines; virtual;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation


{$R *.DFM}

procedure TEDIT_PR_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  AddDS(DM_PAYROLL.PR, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_COMPANY.CO;
  if not Assigned(dsCL.DataSet) then
    dsCL.DataSet := DM_CLIENT.CL;
  if not Assigned(wwdsSubMaster.DataSet) then
    wwdsSubMaster.DataSet := DM_PAYROLL.PR;
end;

procedure TEDIT_PR_BASE.Activate;
begin
  inherited;
  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(wwdsMaster.DataSet).ClientID, wwdsMaster.DataSet['CO_NBR']]), []);
  if not BitBtn1.Enabled and (wwdsList.DataSet.RecordCount > 0)then
    BitBtn1Click(Self);
  wwdsSubMaster.DoDataChange(Self, nil);
end;

procedure TEDIT_PR_BASE.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  AddDS(DM_PAYROLL.PR, aDS);
  inherited;
end;

procedure TEDIT_PR_BASE.PRGridDblClick(Sender: TObject);
begin
  inherited;
  if wwdsSubMaster.DataSet.RecordCount > 0 then
    PageControl1.SelectNextPage(True);
end;

procedure TEDIT_PR_BASE.PRGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (wwdsSubMaster.DataSet.RecordCount > 0) then
  begin
    PageControl1.SelectNextPage(True);
    Key := 0;
  end;
end;

function TEDIT_PR_BASE.GetDataSetConditions(sName: string): string;
begin
  if ((sName = 'CO') or
      (sName = 'PR') or
      (sName = 'EE') or
      (Copy(sName, 1, 3) = 'CO_')) and
     wwdsList.DataSet.Active then
    Result := 'CO_NBR = ' + IntTostr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger)
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_PR_BASE.ReopenPRChecks;
var
  q: TExecDSWrapper;
begin
  DM_EMPLOYEE.EE.Activate;
  DM_PAYROLL.PR_CHECK.CheckDSConditionAndClient(DM_PAYROLL.PR.ClientID, 'PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
  if not DM_PAYROLL.PR_CHECK.Active then
  begin
    Assert(DM_PAYROLL.PR.ClientID = ctx_DataAccess.ClientId);
    q := TExecDSWrapper.Create('GenericSelect2HistNbr');
    with q do
    begin
      SetMacro('Columns', 'T2.*');
      SetMacro('TABLE1', 'EE');
      SetMacro('TABLE2', 'PR_CHECK');
      SetMacro('NBRFIELD', 'PR');
      SetMacro('JOINFIELD', 'EE');
      SetParam('RecordNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
      SetParam('StatusDate', ctx_DataAccess.AsOfDate);
      DM_PAYROLL.PR_CHECK.PrepareLookups;
      try
        ctx_DataAccess.GetCustomData(DM_PAYROLL.PR_CHECK, AsVariant);
      finally
        DM_PAYROLL.PR_CHECK.UnPrepareLookups;
      end;
    end;
    DM_PAYROLL.PR_CHECK.SetOpenCondition('PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
  end;
end;

procedure TEDIT_PR_BASE.ReopenPRCheckWithLines;
begin
  DM_PAYROLL.PR_CHECK.DisableControls;
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  try
    ReopenPRChecks;
    DM_PAYROLL.PR_CHECK_LINES.CheckDSConditionAndClient(DM_PAYROLL.PR.ClientID, 'PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
    ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR_CHECK_LINES]);
    DM_PAYROLL.PR_CHECK_LINES.RetrieveCondition := '';
  finally
    DM_PAYROLL.PR_CHECK.EnableControls;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
  end;
end;

procedure TEDIT_PR_BASE.PRNextUpdate(Sender: TObject);
begin
  inherited;
  TAction(Sender).Enabled := True;
end;

end.
