object TaskRequestsFrame: TTaskRequestsFrame
  Left = 0
  Top = 0
  Width = 435
  Height = 266
  Align = alClient
  TabOrder = 0
  object lvTaskRequests: TevListView
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    Align = alClient
    Columns = <
      item
        Caption = 'Caption'
        Width = 150
      end
      item
        Caption = 'Status'
        Width = 150
      end
      item
        Caption = 'Started'
        Width = 130
      end
      item
        AutoSize = True
        Caption = 'Last Message'
      end>
    ReadOnly = True
    TabOrder = 0
    ViewStyle = vsReport
  end
end
