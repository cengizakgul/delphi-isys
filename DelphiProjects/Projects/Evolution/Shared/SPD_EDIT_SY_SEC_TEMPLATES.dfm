inherited EDIT_SY_SEC_TEMPLATES: TEDIT_SY_SEC_TEMPLATES
  object lablReport_Description: TevLabel [0]
    Left = 12
    Top = 384
    Width = 157
    Height = 13
    Caption = '~Template description'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 12
    Top = 24
    Width = 593
    Height = 353
    DisableThemesInTitle = False
    Selected.Strings = (
      'NAME'#9'40'#9'Description')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_SEC_TEMPLATES\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = wwdsMaster
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object dedtTemplate_Description: TevDBEdit [2]
    Left = 12
    Top = 400
    Width = 241
    Height = 21
    HelpContext = 54001
    DataField = 'NAME'
    DataSource = wwdsMaster
    Picture.PictureMaskFromDataSet = False
    Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
  end
  object bEdit: TevBitBtn [3]
    Left = 488
    Top = 392
    Width = 115
    Height = 25
    Caption = 'Edit rights'
    TabOrder = 2
    OnClick = bEditClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD8
      88DDDDDDDDDDDDDFFFDDD000000DDD80008DD888888DDDF888FD000000DDD80B
      BB08888888DDDF88888F000F0DDDD3B333B0888D8DDDD88DDD8800F0DDD443B0
      03B088D8DDD8D88FFF88D0F00D4443BBBBB0D8D88D88D888888D00FFF044443B
      BB0D88DDD8D88D8888DD00FF004F4DD3B0DD88DD88DD8DD88FDD000FF074DDD3
      B0DD888DD8D8DDD88FDD000FF0744DD3B0DD888DD8D88DD88FDD0000007FF4D3
      B088888888DDD8D88FDDD00004FF44D3B000D8888DDD88D88FFFDDDD444FF4D3
      BBB0DDDDD88DD8D8888FDDDD444FF4D3BBB0DDDD888DD8D8888FDDDD444444D3
      B033DDDD888888D88FDDDDDDD4444DDD38DDDDDDD8888DDDDDDD}
    NumGlyphs = 2
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_SEC_TEMPLATES.SY_SEC_TEMPLATES
    OnStateChange = wwdsMasterStateChange
    OnUpdateData = wwdsMasterUpdateData
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 252
    Top = 21
  end
end
