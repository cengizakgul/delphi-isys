// Copyright � 2000-2008 iSystems LLC. All rights reserved.
unit evAchPrenote;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  evAchBase, Db,   EvTypes, EvDataAccessComponents,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvConsts, EvMainboard, EvAchUtils, EvExceptions, EvContext, Variants;

type
  TevAchPrenote = class(TevAchBase)
  private
    FPrenoteCLAccounts: Boolean;
    procedure SetPrenoteCLAccounts(const Value: Boolean);
  protected
    procedure DoOnConstruction; override;
  public
    Companies: TEvBasicClientDataSet;
    procedure SaveAch; override;
    property PrenoteCLAccounts: Boolean read FPrenoteCLAccounts write SetPrenoteCLAccounts;
    function ACHDescription: string; override;
    procedure MainProcess; override;
    destructor Destroy; override;
    procedure WriteACHDetail(cTranType: Char);
  end;

implementation

uses
  EvUtils, SFieldCodeValues, SDataStructure, SACHTypes;

procedure TevAchPrenote.WriteACHDetail(cTranType: Char);
var
  s: string;
  Nbr: Integer;
  RecordTypeCode: String;
  IdentificationNumber, ReceiverName, DiscretionaryData,
  AddendaIndicator, TraceNumber: String;

  function RemovePunctuation(const Value: String): String;
  var
    i: Integer;
  begin
    Result := Value;
    i := 1;
    while i <= Length(Result) do
    begin
      if Result[i] in [' ',',','''','"','-','.'] then
        System.Delete(Result, i, 1)
      else
        Inc(i);  
    end;
  end;

begin

  // if no transactions was added when processing payroll ACH, it must be marked as "Deleted"
  FDetailAdded := True;

  // ONLY write ACH_LINE... because EACH segment handles detail records differently
  // by either using CUSTOM_VIEW (or not).
  
  RecordTypeCode := '6';
  if not CheckOffset(ACHFile.ACH_OFFSET.AsInteger) then
  begin
    if ACHFile.State in [dsInsert, dsEdit] then
      ACHFile.Cancel;
    Exit;
  end;
  Nbr := 0;

  // ALL OTHER fields were previously stamped before calling this procedure!
  // Finish Calculating Transaction Code based on AMOUNT

  ACHFile.TRAN_TYPE.AsString := cTranType;
  if Length(ACHFile.TRAN.AsString) < 2 then
    ACHFile.TRAN.AsString := ACHFile.TRAN.AsString +
      GetAchTransactionType(ACHFile.IN_PRENOTE.AsString,
      ACHFile.AMOUNT.AsCurrency, Nbr);

//  if FPrenote and (Length(ACHFile.TRAN.AsString) = 2) then
//  begin
//    ACHFile.TRAN.AsString := copy(ACHFile.TRAN.AsString,1,1) + '8';
//  end;

  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.BATCH_COMMENTS.AsString := ACHFile.BatchComments;

  s := RecordTypeCode + Copy(ACHFile.TRAN.Value
    + '  ', 1, 2) + Copy(ACHFile.ABA_NUMBER.Value + '         ', 1, 9)
    + Copy(ACHFile.BANK_ACCOUNT_NUMBER.Value + '                 ', 1, 17)
    + ZeroFillInt64ToStr(RoundInt64(Abs(ACHFile.AMOUNT.Value) * 100), 10);

  IdentificationNumber := '';

  if Options.CTS then
  if ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString <> '' then
  begin
    DM_CLIENT.CL_PERSON.Activate;
    DM_EMPLOYEE.EE.Activate;
    IdentificationNumber := StringReplace(DM_EMPLOYEE.EE.Lookup('CO_NBR;CUSTOM_EMPLOYEE_NUMBER',
      VarArrayOf([DM_COMPANY.CO['CO_NBR'], ACHFile.FieldValues['CUSTOM_EMPLOYEE_NUMBER']]), 'SOCIAL_SECURITY_NUMBER'), '-', '', [rfReplaceAll]) + StringOfChar(' ', 6);
    s := s + IdentificationNumber;
  end
  else if ACHFile.TRAN.Value <> '22' then
  begin
    IdentificationNumber := PadRight(Trim(ACHFile.CUSTOM_COMPANY_NUMBER.AsString), ' ', 13) + '01';
    s := s + IdentificationNumber;
  end
  else if Options.SunTrust then
  begin
    IdentificationNumber := PadRight(Trim(SBID), ' ', 13) + '01';
    s := s + IdentificationNumber;
  end
  else
  begin
    IdentificationNumber := '    ' + Copy(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString) + '           ', 1, 11);
    s := s + IdentificationNumber;
  end
  else
  begin
    IdentificationNumber := PadStringRight(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15);
    s := s + IdentificationNumber;
  end;

  if not Options.DisplayCoNbrAndName then
    ReceiverName := Copy(ACHFile.NAME.AsString + '                      ', 1, 22)
  else
    ReceiverName := Copy(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ' ' +
      RemovePunctuation(DM_COMPANY.CO.NAME.AsString) + '                      ', 1, 22);

  DiscretionaryData := '  ';

  AddendaIndicator := IntToStr(Nbr);
  TraceNumber := Copy(ORIGIN_BANK_ABA + '         ', 1, 8) + ZeroFillInt64ToStr(ACHFile.BATCH_NBR.AsInteger, 7);
  s := s + ReceiverName + DiscretionaryData + AddendaIndicator + TraceNumber;
  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  ACHFile.Post;

  IncHashTotal(ACHFile.ABA_NUMBER.AsString, FEntryHash);

  // for Sub-Totals
  if ACHFile.ACH_OFFSET.AsInteger = OFF_DONT_USE then
    Inc(FSubNbrOfEntries);
end;

function TevAchPrenote.ACHDescription: string;
begin
  Result := 'Prenote ACH';
end;

procedure TevAchPrenote.MainProcess;
var
  CLNbr, CONbr, OldRecCount: Integer;
  EffectiveDate: TDateTime;
begin
  FPrenote := True;
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');

  // Show ONLY ACH Bank Accounts...
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Filtered := True;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Filter := 'ACH_ACCOUNT = ''' + GROUP_BOX_YES + '''';

  Options.EffectiveDate := False;
  
  inherited;

  if not Companies.Eof then
  begin
    ctx_DataAccess.OpenClient(Companies.FieldByName('CL_NBR').AsInteger);
    DM_COMPANY.CO.DataRequired('ALL');
    DM_COMPANY.CO.Locate('CO_NBR', Companies.FieldByName('CO_NBR').Value, []);
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsString, []);
    DM_SERVICE_BUREAU.SB.DataRequired('ALL');
    DM_TEMPORARY.TMP_CO.DataRequired('CO_NBR>0');
    DM_EMPLOYEE.CL_BANK_ACCOUNT.DataRequired('ALL');
    DM_EMPLOYEE.CL_PERSON.DataRequired('ALL');
    DM_COMPANY.CO.DataRequired('ALL');
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('ALL');
  end;

  if not ACHFile.Active then
    ACHFile.Open;
    
  ACHFile.EmptyDataSet;

  FileHeaderProcessed := False;
  ClearGrandAndBatchTotals;
  SegmentNbr := 1;

  EffectiveDate := Date;

  FileSaved := False;

  Companies.IndexFieldNames := 'CUSTOM_COMPANY_NUMBER';

  CLNbr := -1;
  CONbr := -1;
  OldRecCount := 0;
  Companies.First;

  AssignHeaderInfo;
  StartBatchDefinition('PPD', EffectiveDate, 'PRENOTE', '', DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsInteger);

  while not Companies.EOF do
  begin
    if Companies.FieldByName('CL_NBR').AsInteger <> ClNbr then
    begin
      if ClNbr <> -1 then
      begin
        if SavingACH then
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_DIRECT_DEPOSIT]);
        WriteBatchHeader(True, SegmentNbr, OldRecCount);
        WriteBatchTotal(True, SegmentNbr, ACHFile.RecordCount, '');
      end;

      ClNbr := Companies.FieldByName('CL_NBR').AsInteger;
      CoNbr := -1;
      PrNbr := -1;
      ctx_DataAccess.OpenClient(ClNbr);
      DM_EMPLOYEE.CL_BANK_ACCOUNT.DataRequired('ALL');
      DM_EMPLOYEE.CL_PERSON.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('ALL');
      DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('ALL');
    end;

    if Companies.FieldByName('CO_NBR').Value <> CoNbr then
    begin
      if CoNbr <> -1 then
      begin
        WriteBatchHeader(True, SegmentNbr, OldRecCount);
        WriteBatchTotal(True, SegmentNbr, ACHFile.RecordCount, '');
      end;

      CoNbr := Companies.FieldByName('CO_NBR').Value;
      DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsString, []);

      DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      
      if Options.FedReserve then
      begin
        if not DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO['DD_CL_BANK_ACCOUNT_NBR'], []) then
          raise EInconsistentData.CreateHelp('DD_CL_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

        Assert(DM_SERVICE_BUREAU.SB_BANKS.LOCATE('SB_BANKS_NBR', DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ', Client Bank Account ' + DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
        ORIGIN_BANK_ABA := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.FieldByName('ABA_NUMBER').AsString);
      end;

      AddFileHeaderIfNotAddedYet;
      StartBatchDefinition('PPD', EffectiveDate, 'PRENOTE', '', DM_COMPANY.CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsInteger);
      OldRecCount := ACHFile.RecordCount;
    end;

    SegmentNbr := 1;

    if FPrenoteCLAccounts then
    begin
      DM_EMPLOYEE.CL_BANK_ACCOUNT.First;
      while not DM_EMPLOYEE.CL_BANK_ACCOUNT.EOF do
      begin
        AppendDetailRecord('', 0);
        ACHFile.IN_PRENOTE.AsString := 'Y';
        ACHFile.SetOrderNumber;

        ACHFile.NAME.Value := 'COMPANY ACCOUNT PRENOTE';

        Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_EMPLOYEE.CL_BANK_ACCOUNT.FieldByName('SB_BANKS_NBR').Value, []));
        ACHFile.ABA_NUMBER.Value := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''));
        if ACHFile.ABA_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect ABA number for Client Bank Account #' +
            DM_EMPLOYEE.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString, IDH_InconsistentData);
        ACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
        if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
          ACHFile.BANK_ACCOUNT_NUMBER.Value :=
            ACHTrim(DM_EMPLOYEE.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString)
        else
          ACHFile.BANK_ACCOUNT_NUMBER.Value := DM_EMPLOYEE.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
        if ACHFile.BANK_ACCOUNT_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect Bank Account number for Client Bank Account #' +
            DM_EMPLOYEE.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString, IDH_InconsistentData);
        ACHFile.TRAN.AsString :=
          GetAchAccountType(DM_EMPLOYEE.CL_BANK_ACCOUNT.BANK_ACCOUNT_TYPE.AsString);
        WriteACHDetail(TRAN_TYPE_COMPANY);

        DM_EMPLOYEE.CL_BANK_ACCOUNT.Next;
      end;
    end;

    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.First;
    while not DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EOF do
    begin
      if (DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('IN_PRENOTE').AsString = 'Y') and
         DM_EMPLOYEE.EE.Locate('EE_NBR', DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_NBR').Value, []) then
      begin
        AppendDetailRecord(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value, 0);
        ACHFile.IN_PRENOTE.AsString := 'Y';
        ACHFile.SetOrderNumber;

        DM_EMPLOYEE.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Value, []);
        if DM_EMPLOYEE.CL_PERSON.FieldByName('MIDDLE_INITIAL').IsNull then
          ACHFile.NAME.Value := DM_EMPLOYEE.CL_PERSON.FieldByName('FIRST_NAME').Value + ' ' +
            DM_EMPLOYEE.CL_PERSON.FieldByName('LAST_NAME').Value
        else
          ACHFile.NAME.Value := DM_EMPLOYEE.CL_PERSON.FieldByName('FIRST_NAME').Value + ' ' +
            DM_EMPLOYEE.CL_PERSON.FieldByName('MIDDLE_INITIAL').Value + '. ' + DM_EMPLOYEE.CL_PERSON.FieldByName('LAST_NAME').Value;

        ACHFile.ABA_NUMBER.Value := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_ABA_NUMBER').AsString);
        if ACHFile.ABA_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect ABA number for EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + ', CO #' +
            Companies.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
        ACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
        if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
          ACHFile.BANK_ACCOUNT_NUMBER.Value :=
            ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString)
        else
          ACHFile.BANK_ACCOUNT_NUMBER.Value := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString;
        if ACHFile.BANK_ACCOUNT_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect Bank Account number for EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) +
            ', CO #' + Companies.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
        ACHFile.TRAN.AsString :=
          GetAchAccountType(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_TYPE.AsString);
        WriteACHDetail(TRAN_TYPE_OUT);

        DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Edit;
        DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('IN_PRENOTE').AsString := 'N';
        DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Post;
      end;
      DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Next;
    end;

    Companies.Next;
  end;

  if CoNbr <> -1 then
  begin
    WriteBatchHeader(True, SegmentNbr, OldRecCount);
    WriteBatchTotal(True, SegmentNbr, ACHFile.RecordCount, '');
    if SavingACH then
      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_DIRECT_DEPOSIT]);
  end;

  WriteEof(True);
//    ACHFile.IndexFieldNames := 'custom_company_number;cl_nbr;pr_nbr;segment_nbr;order_nbr';
  ACHFile.IndexFieldNames := 'custom_company_number;cl_nbr;segment_nbr;order_nbr';
end;

procedure TevAchPrenote.SaveAch;
begin
  if Companies.RecordCount = 0 then
    Exit;
  inherited;
end;

procedure TevAchPrenote.SetPrenoteCLAccounts(const Value: Boolean);
begin
  FPrenoteCLAccounts := Value;
end;

destructor TevAchPrenote.Destroy;
begin
  Companies.Free;
  inherited;
end;

procedure TevAchPrenote.DoOnConstruction;
begin
  inherited;
  bPrenote := True;
  Companies := TEvBasicClientDataSet.Create(nil);
end;

end.
