// Copyright � 2000-2008 iSystems LLC. All rights reserved.
unit evAchEFT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db,  EvTypes, EvComponentsExt, EvBasicUtils, isBaseClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, DateUtils, EvContext, isVCLBugFix,
  EvExceptions, EvAchUtils, EvAchDataSet, evAchBase, evAchTaxList, EvClientDataSet,
  SACHTypes;
type
  TevAchEFT = class(TevAchBase, IAchPayment)
  private
    EFT_ACH: TevAchEFTDataSet;
    EFT_ACH_Backup: TevAchEFTDataSet;
    FNeedToStartPooling: Boolean;
    FACHPooled: Boolean;
    procedure StartPoolingAch_Internal;
    procedure BackupCurrentFile;
    procedure AddRecord(ACONbr, AGlobalAgencyNbr, ATaxPaymentAchNbr: Integer; AAmount: Double; AEffectiveDate: TDateTime;
                APaymentInfo, AState: String; ASecondPaymentInfo: string = ''; AEIN: string = ''; AOverrideName: string = ''; ASpecialFlag: string = ''; epn: String = '');
  protected
    procedure DoOnConstruction; override;
  public
    procedure LoadACHOptionsFromIniFile(const AACHOptionsIni: IisStringList);
    procedure SaveAch; override;
    //
    procedure SaveClientAch(PostDBChanges: Boolean = True);
    procedure StartPoolingAch;
    procedure StopPoolingAch;
    //

    procedure WriteBatchHeaderHuber(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchHeaderAkron(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchHeaderMASUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchHeaderCTSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchHeaderMNSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchHeaderNMSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);

    procedure Backup;
    procedure Restore;

    procedure MainProcess; override;
    function ACHDescription: string; override;

    procedure ProcessEFTPaymentLocal(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);

    procedure ProcessEFTPaymentSui(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);

    procedure ProcessEFTPaymentSuiAZ(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);

    procedure ProcessEFTPaymentSuiLA(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);

    procedure ProcessEFTPaymentSuiIA(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);

    procedure ProcessCA(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessNJ(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessNM(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessAL(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessME(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessHI(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessDC(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessMS(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessTN(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessOR(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessNY(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessMO(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessFL(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessNE(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessNC(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessOK(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessID(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessMA(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessIN(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessCO(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
    procedure ProcessXX(Amount: Double; SecondAmount: Variant; TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);

    procedure ProcessEFTPayment(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
      PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer; TaxType: TTaxTable);

    procedure ProcessEFTPSPayment(Amount1, Amount2, Amount3: Double; EffectiveDate, TaxPeriodEndDate: TDateTime;
      CONbr, TaxPaymentAchNbr: Integer; TaxName: string; CobraCredit: Boolean);
    procedure FillStringList(const ResultList: TStringList);
    function AchRecordCount: Integer;
    destructor Destroy; override;
  end;

  
implementation

uses
  EvUtils, EvConsts, SDataStructure, IniFiles, SDataDictclient;

function StripNonNumberChars(const Value: String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
    if Value[i] in ['0','1','2','3','4','5','6','7','8','9'] then
      Result := Result + Value[i];
end;

function CheckVariant(V: Variant; Element: Integer): Variant;
begin
  if VarIsNull(V) then
    Result := 0
  else
    if Element < 0 then
      Result := V
    else
      Result := V[Element];
end;

{ TevAchEFT }

procedure TevAchEFT.AddRecord(ACONbr, AGlobalAgencyNbr, ATaxPaymentAchNbr: Integer; AAmount: Double; AEffectiveDate: TDateTime;
  APaymentInfo, AState: String; ASecondPaymentInfo: string = ''; AEIN: string = ''; AOverrideName: string = ''; ASpecialFlag: string = ''; epn: String = '');
begin
  EFT_ACH.Append;
  EFT_ACH.CL_NBR.Value := ctx_DataAccess.ClientID;
  EFT_ACH.CO_NBR.Value := ACONbr;
  EFT_ACH.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR', VarArrayOf([ctx_DataAccess.ClientID, ACONbr]), 'CUSTOM_COMPANY_NUMBER');
  EFT_ACH.CO_TAX_PAYMENT_ACH_NBR.Value := ATaxPaymentAchNbr;
  EFT_ACH.AMOUNT.Value := AAmount;
  EFT_ACH.EFFECTIVE_DATE.Value := AEffectiveDate;
  EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value := AGlobalAgencyNbr;
  EFT_ACH.PAYMENT_INFO.Value := APaymentInfo;
  EFT_ACH.STATE.Value := AState;
  EFT_ACH.SECOND_PAYMENT_INFO.Value := ASecondPaymentInfo;
  EFT_ACH.EIN.Value := AEIN;
  EFT_ACH.OVERRIDE_NAME.AsString := AOverrideName;
  EFT_ACH.SPECIAL_FLAG.AsString := ASpecialFlag;
  if ASpecialFlag = 'CT SUI' then
    EFT_ACH.CUSTOM_COMPANY_NUMBER.AsString := epn;
  EFT_ACH.Post;
end;

procedure TevAchEFT.MainProcess;
var
  CLNbr, CONbr, OldRecCount, RecCount: Integer;
  EffectiveDate: TDateTime;
  FExceptions: TStringList;
  TotalWritten: Boolean;
  aName, aEIN, SaveEPN, SPECIAL_FLAG: String;
begin
  inherited;
  Options.CTS := False;
  FExceptions := TStringList.Create;
  try
    SegmentNbr := 1;
    ClNbr := -1;
    ACHFile.IndexName := 'DEFAULT_ORDER';
    CONbr := -1;
    PrNbr := -1;
    EffectiveDate := 0;
    RecCount := 0;
    OldRecCount := 0;
    EFT_ACH.First;
    TotalWritten := False;
    if not EFT_ACH.EOF then
      SPECIAL_FLAG := EFT_ACH.SPECIAL_FLAG.AsString;
    while not EFT_ACH.EOF do
    begin
      if (CoNbr <> -1) and ((EFT_ACH.CO_NBR.Value <> CoNbr) or (ClNbr <> EFT_ACH.CL_NBR.Value)
        or (EFT_ACH.EFFECTIVE_DATE.Value <> EffectiveDate))
        or ((EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI') and (SPECIAL_FLAG <> 'MA SUI'))
        or ((EFT_ACH.SPECIAL_FLAG.AsString <> 'MA SUI') and (SPECIAL_FLAG = 'MA SUI'))
        or ((EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI') and (SPECIAL_FLAG <> 'NM SUI'))
        or ((EFT_ACH.SPECIAL_FLAG.AsString <> 'NM SUI') and (SPECIAL_FLAG = 'NM SUI'))
        or ((EFT_ACH.SPECIAL_FLAG.AsString = 'HuberHeights') and (SPECIAL_FLAG <> 'HuberHeights'))
        or ((EFT_ACH.SPECIAL_FLAG.AsString <> 'HuberHeights') and (SPECIAL_FLAG = 'HuberHeights'))
      then
      begin
        if SPECIAL_FLAG = 'HuberHeights' then
          WriteBatchHeaderHuber(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aEIN, False, FPrenote)
        else
        if SPECIAL_FLAG = 'Akron' then
          WriteBatchHeaderAkron(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, SaveEPN, False, FPrenote)
        else
        if SPECIAL_FLAG = 'CT SUI' then
          WriteBatchHeaderCTSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, SaveEPN, False, FPrenote)
        else
        if SPECIAL_FLAG = 'MA SUI' then
          WriteBatchHeaderMASUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
        else
        if SPECIAL_FLAG = 'NM SUI' then
          WriteBatchHeaderNMSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
        else
//        if SPECIAL_FLAG = 'AZ SUI' then
//          WriteBatchHeaderCTSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
//        else
        if SPECIAL_FLAG = 'MN SUI' then
          WriteBatchHeaderMNSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
        else
          WriteBatchHeader(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote);
        Inc(RecCount);
        if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
          WriteBatchTotal(True, SegmentNbr, RecCount, '',OFF_DONT_USE,True,True,SaveEPN)
        else
          WriteBatchTotal(True, SegmentNbr, RecCount, '');
        TotalWritten := True;
      end;
      SPECIAL_FLAG := EFT_ACH.SPECIAL_FLAG.AsString;
      SaveEPN := EFT_ACH.CUSTOM_COMPANY_NUMBER.AsString;
      if ClNbr <> EFT_ACH.CL_NBR.Value then
      begin
        if CLNbr <> -1 then
          if SavingACH then
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
        ClNbr := EFT_ACH.CL_NBR.Value;
        ctx_DataAccess.OpenClient(ClNbr);
        CoNbr := -1;
        DM_COMPANY.CO.DataRequired('ALL');
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('ALL');
      end;
      if (EFT_ACH.CO_NBR.Value <> CoNbr) or (EFT_ACH.EFFECTIVE_DATE.Value <> EffectiveDate) or TotalWritten then
      begin
        TotalWritten := False;
        if EFT_ACH.CO_NBR.Value <> CoNbr then
        begin
          CoNbr := EFT_ACH.CO_NBR.Value;
          Assert(DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []));
          Assert(DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([ClNbr, CoNbr]), []));
          if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR',
            DM_COMPANY.CO.ACH_SB_BANK_ACCOUNT_NBR.Value, []) then
            raise EInconsistentData.CreateHelp('Company: '+IntToStr(CoNbr)+' SB Bank Account not found!', IDH_InconsistentData);
          AssignHeaderInfo;
          DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CoNbr));
        end;
        EffectiveDate := EFT_ACH.EFFECTIVE_DATE.Value;

        AddFileHeaderIfNotAddedYet;
        if FPrenote then
          StartBatchDefinition('CCD', EffectiveDate, 'PRENOTE', '', 0)
        else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'Akron' then
            StartBatchDefinition('CCD', EffectiveDate, 'TAXPAYMENT', '', 0)
          else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'KCMO' then
            StartBatchDefinition('CCD', EffectiveDate, 'KCMO-Tax', '', 0)
          else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'AZ SUI' then
            StartBatchDefinition('CCD', EffectiveDate, 'UI TAX PAY', '', 0)
          else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'MN SUI' then
            StartBatchDefinition('CCD', EffectiveDate, 'MN UI PAY', '', 0)
          else
            StartBatchDefinition('CCD', EffectiveDate, 'EFTPAYMENT', '', 0);
        Inc(RecCount);
        OldRecCount := RecCount;
      end;

      SegmentNbr := 1;
      CO_TAX_PAYMENT_ACH_NBR := EFT_ACH.CO_TAX_PAYMENT_ACH_NBR.Value;

      BatchBANK_REGISTER_TYPE := '';
      BatchSB_BANK_ACCOUNTS_NBR := 0;
      AppendDetailRecord('', EFT_ACH.AMOUNT.Value);
      if FPrenote then
        ACHFile.IN_PRENOTE.AsString := 'Y';
      Inc(RecCount);
      ACHFile.ORDER_NBR.Value := RecCount;
      if (EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI') then
      begin
        DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;      
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := ConvertNull(DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Lookup('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'TPA ID']), 'E_MAIL_ADDRESS'), '');
      end
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI' then
      begin
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := PadStringRight(Trim(EFT_ACH.EIN.AsString), ' ', 15);
      end
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
      begin
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := PadStringRight(Trim(EFT_ACH.EIN.AsString), ' ', 15);
      end
      else
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := EFT_ACH.EIN.AsString;
        
      if (EFT_ACH.SPECIAL_FLAG.AsString = 'IL SUI')
      or (EFT_ACH.SPECIAL_FLAG.AsString = 'IL STATE') then
      begin
        DM_COMPANY.CO_STATES.DataRequired('ALL');
        DM_COMPANY.CO_STATES.Locate('STATE','IL',[]);
        case DM_COMPANY.CO_STATES.USE_DBA_ON_TAX_RETURN.AsString[1] of
          USE_DBA_ON_TAX_RETURN_DBA:
            ACHFile.NAME.Value := copy(DM_COMPANY.CO.DBA.AsString+'                              ',1,30);
          USE_DBA_ON_TAX_RETURN_LEGAL:
            ACHFile.NAME.Value := copy(DM_COMPANY.CO.LEGAL_NAME.AsString+'                              ',1,30);
          USE_DBA_ON_TAX_RETURN_PRIMARY:
            ACHFile.NAME.Value := copy(DM_COMPANY.CO.NAME.AsString+'                              ',1,30);
        end;
      end
      else
      if not EFT_ACH.STATE.IsNull then
      begin
        if EFT_ACH.SPECIAL_FLAG.AsString = 'Akron' then
          ACHFile.NAME.Value := Copy(DM_TEMPORARY.TMP_CO.NAME.AsString + '                    ', 1, 20)
        else
          if EFT_ACH.STATE.Value = 'MA' then
            ACHFile.NAME.Value := DM_TEMPORARY.TMP_CO.NAME.AsString
          else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
            ACHFile.NAME.Value := Copy(DM_TEMPORARY.TMP_CO.NAME.AsString + '                    ', 1, 22)
          else
          if EFT_ACH.SPECIAL_FLAG.AsString = 'NV SUI' then
            ACHFile.NAME.Value := Copy(DM_TEMPORARY.TMP_CO.NAME.AsString + '                    ', 1, 22)
          else
            ACHFile.NAME.Value := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
              EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'AGENCY_NAME');
      end
      else
      begin
        ACHFile.NAME.Value := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
          EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'AGENCY_NAME');
//        bMA := False;
      end;
      ACHFile.ABA_NUMBER.Value := ACHTrim(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
        EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'RECEIVING_ABA_NUMBER'), ''));
      if ACHFile.ABA_NUMBER.AsString = '' then
        raise EInconsistentData.CreateHelp('Incorrect ABA number for ' + DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
          EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'AGENCY_NAME'), IDH_InconsistentData);
      ACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
      ACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value,
        'RECEIVING_ACCOUNT_NUMBER'), ''));
      if ACHFile.BANK_ACCOUNT_NUMBER.AsString = '' then
        raise EInconsistentData.CreateHelp('Incorrect Bank Account number for ' + DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
          EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'AGENCY_NAME'), IDH_InconsistentData);
      ACHFile.TRAN.AsString := GetAchAccountType(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR',
        EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'RECEIVING_ACCOUNT_TYPE'));
      if EFT_ACH.SECOND_PAYMENT_INFO.AsString = '' then
      begin
        WriteACHDetail(TRAN_TYPE_COMPANY, 1, (EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI') or (EFT_ACH.SPECIAL_FLAG.AsString = 'MO SUI'),
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'NV SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'AZ SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI');
      end
      else
      begin
        WriteACHDetail(TRAN_TYPE_COMPANY, 2, (EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI') or (EFT_ACH.SPECIAL_FLAG.AsString = 'MO SUI'),
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'NV SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'AZ SUI',
                                             EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI');
      end;

// Write addenda record
      AppendAddendaRecord('', 0);
      Inc(RecCount);
      ACHFile.ORDER_NBR.Value := RecCount;
      ACHFile.HIDE_FROM_REPORT.Value := 'Y';

      ACHFile.WriteAddenda(UpperCase(EFT_ACH.PAYMENT_INFO.AsString),
        AddendaNbr, EntryNbr);

// Write second addenda record
      if EFT_ACH.SECOND_PAYMENT_INFO.AsString <> '' then
      begin
        AppendAddendaRecord('', 0);
        ACHFile.HIDE_FROM_REPORT.Value := 'Y';
        Inc(RecCount);
        ACHFile.ORDER_NBR.Value := RecCount;
        ACHFile.WriteAddenda(UpperCase(EFT_ACH.SECOND_PAYMENT_INFO.AsString),
          AddendaNbr, EntryNbr);
      end;

// Debit SB tax bank account
      if DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.IsNull then
        raise EInconsistentData.CreateHelp('Company #' + VarToStr(DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR', VarArrayOf([CLNbr, CONbr]),
          'CUSTOM_COMPANY_NUMBER')) + ' does not have SB Tax Bank Account setup!', IDH_InconsistentData);
      if not FPrenote and (DM_COMPANY.CO.TAX_SERVICE.AsString <> TAX_SERVICE_DIRECT) then
      begin
        BatchBANK_REGISTER_TYPE := BANK_REGISTER_TYPE_EFTPayment;
        BatchSB_BANK_ACCOUNTS_NBR := DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.AsInteger;
      end;
      AppendDetailRecord('', EFT_ACH.AMOUNT.Value * (-1));
      if FPrenote then
        ACHFile.IN_PRENOTE.AsString := 'Y';
      Inc(RecCount);
      ACHFile.ORDER_NBR.Value := RecCount;
      if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := PadStringRight(Trim(EFT_ACH.EIN.AsString), ' ', 15)
      else if EFT_ACH.SPECIAL_FLAG.AsString = 'NV SUI' then
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := PadStringLeft(Trim(EFT_ACH.EIN.AsString), '0', 15)
      else
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := EFT_ACH.EIN.AsString;
      ACHFile.NAME.Value := 'TAX PAYMENT';
      if DM_COMPANY.CO.TAX_SERVICE.AsString = TAX_SERVICE_DIRECT then
      begin
        DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
        ACHFile.ABA_NUMBER.Value := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR',
          DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.TAX_CL_BANK_ACCOUNT_NBR.Value,
          'SB_BANKS_NBR'), 'ABA_NUMBER'), ''));
        if ACHFile.ABA_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect ABA number for bank account' + DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.TAX_CL_BANK_ACCOUNT_NBR.Value,
            'CUSTOM_BANK_ACCOUNT_NUMBER'), IDH_InconsistentData);
        ACHFile.BANK_ACCOUNT_NUMBER.Value := ACHTrim(ConvertNull(DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR',
          DM_COMPANY.CO.TAX_CL_BANK_ACCOUNT_NBR.Value, 'CUSTOM_BANK_ACCOUNT_NUMBER'), ''));
        ACHFile.TRAN.AsString := GetAchAccountType(ConvertNull(DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR',
          DM_COMPANY.CO.TAX_CL_BANK_ACCOUNT_NBR.Value, 'BANK_ACCOUNT_TYPE'), ''));
      end
      else
      begin
        ACHFile.ABA_NUMBER.Value := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR',
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.Value,
          'SB_BANKS_NBR'), 'ABA_NUMBER'), ''));
        if ACHFile.ABA_NUMBER.AsString = '' then
          raise EInconsistentData.CreateHelp('Incorrect ABA number for bank account' + DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.Value,
            'CUSTOM_BANK_ACCOUNT_NUMBER'), IDH_InconsistentData);
        ACHFile.BANK_ACCOUNT_NUMBER.Value := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR',
          DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.Value, 'CUSTOM_BANK_ACCOUNT_NUMBER'), ''));
        ACHFile.TRAN.AsString := GetAchAccountType(ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR',
          DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.Value, 'BANK_ACCOUNT_TYPE'), ''));
      end;
      ACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
      if EFT_ACH.AMOUNT.Value = 0 then
        WriteACHDetail(TRAN_TYPE_COMPANY, -1, (EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI') or (EFT_ACH.SPECIAL_FLAG.AsString = 'MO SUI'), False, False, False, False)
      else
        WriteACHDetail(TRAN_TYPE_COMPANY, 0, (EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI') or (EFT_ACH.SPECIAL_FLAG.AsString = 'MO SUI'), False, False, False, False);

      if EFT_ACH.SPECIAL_FLAG.AsString = 'Akron' then
      begin
        DM_SERVICE_BUREAU.SB.Activate;
        aName := Copy(DM_SERVICE_BUREAU.SB.SB_NAME.AsString + '                ', 1, 16) + 'AKRON';
      end
      else
      if (EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI') or (EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI') then
      begin
        DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;
        aName := ConvertNull(DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Lookup('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'TPA ID']), 'E_MAIL_ADDRESS'), '');
      end
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'MN SUI' then
      begin
        DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;
        aName := ConvertNull(DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Lookup('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([EFT_ACH.SY_GLOBAL_AGENCY_NBR.Value, 'AgentID']), 'E_MAIL_ADDRESS'), '');
      end
      else
        aName := EFT_ACH.OVERRIDE_NAME.AsString;
      if EFT_ACH.SPECIAL_FLAG.AsString = 'IL SUI' then
      begin
        DM_COMPANY.CO_STATES.DataRequired('ALL');
        DM_COMPANY.CO_STATES.Locate('STATE;CO_NBR',VarArrayOf(['IL', DM_COMPANY.CO.CO_NBR.Value]),[]);
        case DM_COMPANY.CO_STATES.USE_DBA_ON_TAX_RETURN.AsString[1] of
          USE_DBA_ON_TAX_RETURN_DBA:
            aName := copy(DM_COMPANY.CO.DBA.AsString+'                              ',1,30);
          USE_DBA_ON_TAX_RETURN_LEGAL:
            aName := copy(DM_COMPANY.CO.LEGAL_NAME.AsString+'                              ',1,30);
          USE_DBA_ON_TAX_RETURN_PRIMARY:
            aName := copy(DM_COMPANY.CO.NAME.AsString+'                              ',1,30);
        end;
      end;
      aEIN := EFT_ACH.EIN.AsString;
      EFT_ACH.Next;
    end;

    if CoNbr <> -1 then
    begin
      if SPECIAL_FLAG = 'HuberHeights' then
        WriteBatchHeaderHuber(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aEIN, False, FPrenote)
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
        WriteBatchHeaderCTSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, EFT_ACH.CUSTOM_COMPANY_NUMBER.AsString, False, FPrenote)
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'Akron' then
        WriteBatchHeaderAkron(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'MA SUI' then
        WriteBatchHeaderMASUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
      else
//      if EFT_ACH.SPECIAL_FLAG.AsString = 'AZ SUI' then
//        WriteBatchHeaderCTSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
//      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'MN SUI' then
        WriteBatchHeaderMNSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
      else
      if EFT_ACH.SPECIAL_FLAG.AsString = 'NM SUI' then
        WriteBatchHeaderNMSUI(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote)
      else
        WriteBatchHeader(True, SegmentNbr, OldRecCount, OFF_DONT_USE, False, False, aName, False, FPrenote);
      Inc(RecCount);
      if EFT_ACH.SPECIAL_FLAG.AsString = 'CT SUI' then
        WriteBatchTotal(True, SegmentNbr, RecCount, '',OFF_DONT_USE,True,True,SaveEPN)
      else
        WriteBatchTotal(True, SegmentNbr, RecCount, '');
    end;
    EFT_ACH.Filtered := False;
    if SavingACH then
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
    if not FACHPooled then
      WriteEof(True);
    ACHFile.IndexName := 'By_CoPrSegOrd';
    if FExceptions.Count > 0 then
      raise ETaxDepositException.CreateHelp(FExceptions.Text, IDH_CanNotPerformOperation);
  finally
    FExceptions.Free;
  end;
end;

//   TTaxTable = (ttFed, ttState, ttSUI, ttLoc);

procedure TevAchEFT.ProcessEFTPaymentLocal(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);
var
  PaymentInfo, SecondPaymentInfo, State, StateEIN, Temp, prevFilter, SpecialFlag: String;
  SYStateNbr, DepFreqNbr, RealCOStateNbr, CoNbr: Integer;
  Total: Double;
  prevFiltered: Boolean;
  Cincinnati :Boolean;
  Springfield: Boolean;
  NY_MCT_Mobility: Boolean;
  Lancaster: Boolean;
  Dayton: Boolean;
  Phili: Boolean;
  Kentucky: Boolean;
  Kettering: Boolean;
  Parma: Boolean;
  NYC_And_Yonkers: Boolean;
  Date1, Date2, Date3, Date4: TDateTime;
  epn: String;
  ganbr, syLocNbr: Integer;
  Amount0, Amount1, Amount2: String;
begin
  Specialflag := '';
  Cincinnati := False;
  Springfield := False;
  NY_MCT_Mobility := False;
  Lancaster := False;
  Dayton := False;
  Phili := False;
  Kentucky := False;
  Kettering := False;
  NYC_And_Yonkers := False;
  Parma := False;
  Temp := '';
  PaymentInfo := 'TXP*';

  DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.DataRequired('ALL');
  DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');

  DM_COMPANY.CO_STATES.DataRequired('ALL');
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
  SyStateNbr := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',COTaxEntityNbr,'SY_STATES_NBR');
  CoNbr := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',COTaxEntityNbr,'CO_NBR');

  DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(CoNbr));


  epn := ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',COTaxEntityNbr,'EFT_PIN_NUMBER'),'');
  prevFilter := DM_COMPANY.CO_STATES.Filter;
  prevFiltered := DM_COMPANY.CO_STATES.Filtered;
  DM_COMPANY.CO_STATES.Filter := 'CO_NBR = '+ IntToStr(CoNbr);
  DM_COMPANY.CO_STATES.Filtered := true;
  RealCOStateNbr := DM_COMPANY.CO_STATES.Lookup('SY_STATES_NBR', SyStateNbr, 'CO_STATES_NBR');

  DM_COMPANY.CO_STATES.Filter := prevFilter;
  DM_COMPANY.CO_STATES.Filtered := prevFiltered;

  syLocNbr := ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCALS_NBR'), 0);

  case syLocNbr of
    304,305,6055,
    6056,6057,6058:   Specialflag := 'Akron';
    269:              Springfield := True;
    178,1355,1722:    Lancaster := True;
    306,307:          Dayton := True;
    921,922:          Phili := True;
    2,3,828,829,1165,
    1166,967,968:     Kentucky := True;
    8, 2876:          Kettering := True;
    138:              SpecialFlag := 'KCMO';
    310, 311:         Cincinnati := True;
    1168:             SpecialFlag := 'Springboro';
    5724:             NY_MCT_Mobility := True;
    215, 216, 218:    NYC_And_Yonkers := True;
    1247, 5752:       Parma := True;
  end;

  if DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', COTaxEntityNbr, []) and
        DM_COMPANY.CO_STATES.Locate('CO_NBR;SY_STATES_NBR', VarArrayOf([DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_NBR').asInteger, DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').asInteger]), []) and
        (not DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull ) and
          DM_SYSTEM_LOCAL.SY_LOCALS.locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').asInteger,[]) and
           (DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('COMBINE_FOR_TAX_PAYMENTS').AsString = GROUP_BOX_YES) then
  begin
      Assert(DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsVariant, []),'Missing TCD Agency deposit frequency. Please contact with Isystems LLC.' );
      Assert(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ['SY_GLOBAL_AGENCY_NBR'], []), 'Missing TCD Agency set up. Please contact with Isystems LLC.');
      Assert((DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('AGENCY_TYPE').AsString = 'Y'), 'TCD Agency is inactive.  Select an active TCD Agency.');
      ganbr := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
  end
  else
    ganbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', syLocNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 0);

  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE');
  if State = 'NJ' then
    PaymentInfo := PaymentInfo + 'B';

  if (SpecialFlag = 'Akron') or Kettering or (SpecialFlag = 'KCMO') or (SpecialFlag = 'Springboro') then
  begin
    PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString + '*';
  end
  else
  if Lancaster then
  begin
    PaymentInfo := PaymentInfo + Copy(DM_COMPANY.CO.FEIN.AsString, 1, 2) + '-' + Copy(DM_COMPANY.CO.FEIN.AsString, 3, 7) + '*';
  end
  else
  if Springfield then
  begin
    PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString + '*';
  end
  else
  if Phili then
    PaymentInfo := PaymentInfo + Copy(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER'), 1, 7) + '*'
  else
    PaymentInfo := PaymentInfo + DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') + '*';

  DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');
  if Cincinnati then
    Temp := ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'TAX_RETURN_CODE'), '')
  else
    Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

  StateEIN := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER');

  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
  DepFreqNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SY_STATE_DEPOSIT_FREQ_NBR');

  if (Temp <> '') then
    PaymentInfo := PaymentInfo + Temp + '*'
  else
  begin
    if (SpecialFlag = 'Akron') or Springfield then
      PaymentInfo := PaymentInfo + '010'
    else
    if Lancaster then
      PaymentInfo := PaymentInfo + '01150'
    else
    if (State = 'NJ') and not VarIsNull(SecondAddendumAmount) then
      PaymentInfo := PaymentInfo + '01130'
    else
    if Kentucky then
    begin
      PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString;
    end
    else
    begin
      try
        PaymentInfo := PaymentInfo + DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'TAX_PAYMENT_TYPE_CODE');
      except
        raise EInconsistentData.CreateHelp('Tax Payment Type Code is not defined for ' + State, IDH_InconsistentData);
      end;
      if DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'INC_TAX_PAYMENT_CODE') = 'Y' then
        PaymentInfo := PaymentInfo + IntToStr(PaymentSequenceNbr);
    end;
    PaymentInfo := PaymentInfo + '*';
  end;

  if NYC_And_Yonkers then
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', TaxPeriodEndDate) + '*'
  else
  if Kentucky then
    PaymentInfo := PaymentInfo + 'W1*' + FormatDateTime('yyyymmdd', TaxPeriodEndDate) + '*'
  else
  if (SpecialFlag <> 'Akron') and (SpecialFlag <> 'Springboro')
  and (DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCALS_NBR'), 'SY_LOCAL_TAX_PMT_AGENCY_NBR') <> 57) then
  begin
    Date4 := EffectiveDate;
    if State = 'OH' then
    begin
      DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
      ganbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', syLocNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 0);
      if (ganbr = 123) or (ganbr = 2957) or Kettering then
        Date4 := TaxPeriodEndDate;
    end;
    if State = 'OR' then
      Date4 := TaxPeriodEndDate;
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', Date4) + '*';
  end
  else
  if State = 'IN' then
    PaymentInfo := PaymentInfo + FormatDateTime('yyyymmdd', TaxPeriodEndDate) + '*'
  else
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', TaxPeriodEndDate) + '*';

  SecondPaymentInfo := '';

  if State = 'OR' then
  begin
    PaymentInfo := PaymentInfo + 'L*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*L*' + PadStringLeft(IntToStr(RoundInt64(Abs(SecondAddendumAmount) * 100)), '0', 10) + '***\';
  end
  else
  if State = 'CA' then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100)), '0', 3)
      + '*T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*T*'
      + PadStringLeft(IntToStr(RoundInt64((Abs(VarToFloat(SecondAddendumAmount)) + Abs(Amount)) * 100)), '0', 3) + '*\';
  end
  else
  if (State = 'MO') and (SpecialFlag <> 'KCMO') then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
      + '*T*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100)), '0', 3) + '\';
  end
  else
  if State = 'NY' then
  begin

    if Abs(Amount) > 0.005 then
      Amount0 := 'S*'  + IntToStr(RoundInt64(Abs(Amount) * 100))
    else
      Amount0 := '*';

    if Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 0))) > 0.005 then
      Amount1 := '*C*' +IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 0))) * 100))
    else
      Amount1 := '**';

    if Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) > 0.005 then
      Amount2 := '*L*' +IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) * 100))
    else
      Amount2 := '**';

    PaymentInfo := PaymentInfo
      + Amount0
      + Amount1
      + Amount2
      + '*' + PadStringLeft(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_PIN_NUMBER'), ''), ' ', 6) + '\';
  end
  else
  if State = 'DC' then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
      + '*P*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 0))) * 100)), '0', 10)
      + '*I*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) * 100)), '0', 10)
      + '*' + StateEIN + '*\';
  end
  else
  if State = 'FL' then
  begin
    PaymentInfo := PaymentInfo + '1*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****\';
  end
  else
  if State = 'TN' then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*'
      + DM_COMPANY.CO.FEIN.AsString + '*' + Copy(DM_COMPANY.CO.NAME.AsString + '                         ', 1, 4) + '**\';
  end
  else
  if State = 'ME' then
  begin
    if VarIsNull(SecondAddendumAmount) then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '\'
    else
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount + SecondAddendumAmount[0]) * 100)), '0', 10) + '\';
  end
  else
  if State = 'HI' then
  begin
    ctx_PayrollCalculation.GetStateTaxDepositPeriod(State, DepFreqNbr, TaxPeriodEndDate, Date1, Date2, Date3);
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', Date1) + '*' + StringReplace(StateEIN, '-', '*', []) + '*N*\';
  end
  else
  begin
    if State = 'NJ' then
    begin
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount + ConvertNull(SecondAddendumAmount, 0)) * 100)), '0', 3) + '*'
        + Copy(DM_COMPANY.CO.NAME.AsString + '    ', 1, 4);
    end
    else
    if Lancaster then
    begin
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3);
      if ConvertNull(SecondAddendumAmount[1], 0) <> 0 then
        PaymentInfo := PaymentInfo + '*P*' + PadStringLeft(IntToStr(RoundInt64(Abs(SecondAddendumAmount[1]) * 100)), '0', 3) + '***'
      else
        PaymentInfo := PaymentInfo + '*****';
    end
    else
    if Dayton then
    begin
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3);
      PaymentInfo := PaymentInfo + '*P*' + IntToStr(RoundInt64(Abs(ConvertNull(SecondAddendumAmount[1], 0)) * 100))
          + '*I*' + IntToStr(RoundInt64(Abs(ConvertNull(SecondAddendumAmount[2], 0)) * 100)) + '*000050';
    end
    else
    if State = 'NE' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****'
    else
    if State = 'NC' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
    else
    if State = 'OK' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
    if Phili then
    begin
      case ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 0) of
        969, 972: DepFreqNbr := DayOfTheYear(TaxPeriodEndDate) div 7 + 1; // Check Date
        968, 971: DepFreqNbr := MonthOfTheYear(TaxPeriodEndDate) + 78; // Monthly
        967, 970: DepFreqNbr := GetQuarterNumber(TaxPeriodEndDate) + 91; // Quarterly
      else
        DepFreqNbr := 0;
      end;
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*P*0*' + PadStringLeft(IntToStr(DepFreqNbr), '0', 2)+ FormatDateTime('yy', TaxPeriodEndDate);
    end
    else
    if Kettering then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ****'
    else
    if SpecialFlag = 'KCMO' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*T*0'
    else
    if State = 'AL' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****'
    else
    if State = 'ID' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
    if SpecialFlag = 'Akron' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****'
    else
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****';

    if State = 'MA' then
      PaymentInfo := PaymentInfo + Copy(DM_COMPANY.CO.NAME.AsString + '      ', 1, 6) + '\'
    else
    if (SpecialFlag = 'Akron') or (SpecialFlag = 'Springboro') or Springfield or Kettering then
      PaymentInfo := PaymentInfo + ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER'), '') + '\'
    else
    if Lancaster then
      PaymentInfo := PaymentInfo + Copy(DM_COMPANY.CO.FEIN.AsString, 1, 2) + '-' + Copy(DM_COMPANY.CO.FEIN.AsString, 3, 7) + '\'
    else
    if SpecialFlag = 'KCMO' then
      PaymentInfo := PaymentInfo + '\           '  + FormatDateTime('yymmdd', GetEndQuarter(TaxPeriodEndDate))
    else
      PaymentInfo := PaymentInfo + ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '') + '\';

  end;

  if State = 'PA' then
  begin
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
    case ganbr of
      582:
      PaymentInfo := 'TXP*' +
        DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') + '*' +
        '001*' +
        FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
        ACHTrim(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', ganbr, 'RECEIVING_ACCOUNT_NUMBER'), '')) +
        '*' + IntToStr(RoundInt64(Abs(Amount) * 100)) + '\';
      297, 2041, 2093:
      PaymentInfo := 'TXP*' +
        DM_COMPANY.CO.FEIN.AsString + '*' +
        ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '')+
        '*' + FormatDateTime('yymmdd', TaxPeriodEndDate) +
        '*' + DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
        '*' + IntToStr(RoundInt64(Abs(Amount) * 100)) + '\';
      152, 1744:
      PaymentInfo := 'TXP*' +
        DM_COMPANY.CO.FEIN.AsString + '*' + '319' +
        '*' + FormatDateTime('yymmdd', TaxPeriodEndDate) +
        '*' + DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
        '*' + IntToStr(RoundInt64(Abs(Amount) * 100)) + '\';
      1937:
      PaymentInfo := 'TXP*' +
        DM_COMPANY.CO.FEIN.AsString + '*' +
        ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '')+
        '*' +
        FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
        'T*' + IntToStr(RoundInt64(Abs(Amount) * 100)) + '*'+
        IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '\';
      566:
      PaymentInfo := 'TXP*' +
        DM_COMPANY.CO.FEIN.AsString + '*' + 'EIT00' + 
        '*' +
        FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
        'T*' + IntToStr(RoundInt64(Abs(Amount) * 100)) + '*'+
        IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '\';
    end;
  end;

  if State = 'MI' then
  begin
    DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');

    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
    ganbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', syLocNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 0);

    if ganbr = 1659 then
    begin
      SpecialFlag := 'Grand Rapids';
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ*' +

         PadStringLeft('', '0', 10)

         + '*0*000000\';
    end
    else
    if ganbr = 2121 then
    begin
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

      PaymentInfo := 'TXP*' + copy(DM_COMPANY.CO.FEIN.AsString,1,2)+'-'+copy(DM_COMPANY.CO.FEIN.AsString,3,20) + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate)
       + '*T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount - VarToFloat(CheckVariant(SecondAddendumAmount, 1)) - VarToFloat(CheckVariant(SecondAddendumAmount, 2))) * 100)), '0', 10);
      if Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) > 0.005 then
        PaymentInfo := PaymentInfo + '*P*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) * 100)), '0', 10)
      else
        PaymentInfo := PaymentInfo + '**';
      if Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 2))) > 0.005 then
        PaymentInfo := PaymentInfo + '*I*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 2))) * 100)), '0', 10)
      else
        PaymentInfo := PaymentInfo + '**';

      PaymentInfo := PaymentInfo +  '\';
    end;
  end
  else
  if State = 'OH' then
  begin

    DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');
    if Cincinnati then
      Temp := ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'TAX_RETURN_CODE'), '')
    else
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
    ganbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', syLocNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 0);

    if ganbr = 195 then
    begin
      PaymentInfo := Format('TXP*%s*%s*%s*T*%s*****\',
        [DM_COMPANY.CO.FEIN.AsString, '01105', FormatDateTime('yymmdd', TaxPeriodEndDate),
         PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);
    end
    else
    if (ganbr = 123) or (ganbr = 2957) then
    begin
      PaymentInfo := copy(PaymentInfo,1,Length(PaymentInfo)-1)+PadStringLeft(epn, ' ', 6)+'\';
    end
    else
    if ganbr = 788 then
    begin
      SpecialFlag := 'Athens';
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ*' +

         PadStringLeft(IntToStr(RoundInt64(Abs(Amount/ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', 1863, 'TAX_RATE'), 0.0175)) * 100)), '0', 10)

         + '***' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end
    else
    if ganbr = 129 then
    begin
      SpecialFlag := 'Athens';
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');

      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ*' +

         PadStringLeft(IntToStr(RoundInt64(Abs(Amount/ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', 1863, 'TAX_RATE'), 0.0175)) * 100)), '0', 10)

         + '***' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end
    else
    if ganbr = 809 then
    begin
      SpecialFlag := 'HuberHeights';
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');
      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 11) + '*****' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end
    else
    if Parma then
    begin
      Temp := ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '');
      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end
    else
    if ((syLocNbr = 2872) or (syLocNbr = 1968) or (syLocNbr = 2850)) then
    begin
      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ*' +
         PadStringLeft(IntToStr(RoundInt64(Abs(Amount/0.015) * 100)), '0', 10) + '***' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end
    else
    if ganbr = 221 then
    begin
      PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' + Temp + '*' +
         FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
         'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*ZZ*' +
         PadStringLeft(IntToStr(RoundInt64(Abs(Amount/0.020) * 100)), '0', 10) + '***' +
         DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') +
         '\';
    end;
  end
  else
  if State = 'KY' then
  begin
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
    ganbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', syLocNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 0);
    if ganbr = 47 then
    begin
      PaymentInfo := copy(PaymentInfo, 1, Length(PaymentInfo)-6)+'\';
    end;
  end;

  if  NY_MCT_Mobility then
  begin
    DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');

    PaymentInfo := 'TXP*' +
       DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'LOCAL_EIN_NUMBER') + '*' +
       ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', COTaxEntityNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), 'TAX_PAYMENT_TYPE_CODE'), '') + '*' +
       FormatDateTime('yymmdd', TaxPeriodEndDate) + '*' +
       'M*' +
       PadStringLeft(IntToStr(RoundInt64(Abs(Amount/0.0034) * 100)), '0', 10) + '*' +
       'T*' +
       PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*' +
       epn +
       '\';
  end;

  if State = 'NY' then
    Total := Amount + ConvertNull(CheckVariant(SecondAddendumAmount, 0), 0) + ConvertNull(CheckVariant(SecondAddendumAmount, 1), 0)
  else
  if State = 'OR' then
    Total := Amount + SecondAddendumAmount
  else
    Total := Amount;

  AddRecord(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR'), ganbr,
   TaxPaymentAchNbr, Total, EffectiveDate, PaymentInfo, State, SecondPaymentInfo, StateEIN,
    ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_NAME'), ''), SpecialFlag);
end;

procedure TevAchEFT.ProcessEFTPaymentSuiLA(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);
var
  PaymentInfo, SecondPaymentInfo: String;
  SYStateNbr, RealCOStateNbr, CoNbr, SySuiNbr, AgencyNbr: Integer;
  LaSuiEIN, TaxPaymentCode, EndDate, AmountStr, PinNumber, StateEftName: String;
  SuiRate: Double;
  GrossWages: String;
begin
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');

  RealCOStateNbr := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_STATES_NBR');
  SuiRate        := ConvertNull(DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'RATE'),0);
  CoNbr          := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
  SySuiNbr       := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'SY_SUI_NBR');
  AgencyNbr      := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SySuiNbr, 'SY_SUI_TAX_PMT_AGENCY_NBR');
  StateEftName   := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_NAME'), '');
  SYStateNbr     := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', 'LA', 'SY_STATES_NBR');

  if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '') = '' then
    LaSuiEIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')
  else
    LaSuiEIN := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN');

  try
    TaxPaymentCode :=  DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE');
  except
    raise EInconsistentData.CreateHelp('SUI Tax Payment Type Code is not defined for the state of Louisiana', IDH_InconsistentData);
  end;
  if DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'INC_SUI_TAX_PAYMENT_CODE') = 'Y' then
    TaxPaymentCode := TaxPaymentCode + IntToStr(PaymentSequenceNbr);

  EndDate    := FormatDateTime('yy', TaxPeriodEndDate) + IntToStr(GetQuarterNumber(TaxPeriodEndDate));
  AmountStr  := PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10);
  PinNumber  := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '');
  if SuiRate <> 0 then
    GrossWages := PadStringLeft(IntToStr(RoundInt64(Abs(Amount/SuiRate) * 100)), '0', 10)
  else
    GrossWages := PadStringLeft(IntToStr(0), '0', 10);

  PaymentInfo := 'TXP*' + LaSuiEIN + '*' + TaxPaymentCode + '*' + EndDate +
                 '*T*' + GrossWages + '*T*' + GrossWages + '*T*' + AmountStr + '*00000\';

  if not VarIsNull(SecondAddendumAmount) then
  begin
    AmountStr  := PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100)), '0', 10);
    if SuiRate <> 0 then
      GrossWages := PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)/SuiRate) * 100)), '0', 10)
    else
      GrossWages := PadStringLeft(IntToStr(0), '0', 10);
    SecondPaymentInfo := 'TXP*' + LaSuiEIN + '*' + TaxPaymentCode + '*' + EndDate +
                 '*T*' + GrossWages + '*T*' + GrossWages + '*T*' + AmountStr + '*00000\';
  end;

  AddRecord(CoNbr, AgencyNbr, TaxPaymentAchNbr, Amount + ConvertNull(SecondAddendumAmount, 0),
    EffectiveDate, PaymentInfo, 'LA', SecondPaymentInfo, LaSuiEIN, StateEftName, 'LA SUI', '');

end;

procedure TevAchEFT.ProcessEFTPaymentSuiIA(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);
var
  PaymentInfo, SecondPaymentInfo: String;
  SYStateNbr, RealCOStateNbr, CoNbr, SySuiNbr, AgencyNbr: Integer;
  IaSuiEIN, TaxPaymentCode, EndDate, AmountStr, PinNumber, StateEftName, FEIN: String;
begin
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');

  RealCOStateNbr := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_STATES_NBR');
  CoNbr          := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');

  DM_COMPANY.CO.DataRequired('ALL');
  DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);

  FEIN           := DM_COMPANY.CO.FEIN.AsString;
  SySuiNbr       := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'SY_SUI_NBR');
  AgencyNbr      := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SySuiNbr, 'SY_SUI_TAX_PMT_AGENCY_NBR');
  StateEftName   := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_NAME'), '');
  SYStateNbr     := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', 'LA', 'SY_STATES_NBR');

  if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '') = '' then
    IaSuiEIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')
  else
    IaSuiEIN := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN');

  try
    TaxPaymentCode :=  DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE');
  except
    raise EInconsistentData.CreateHelp('SUI Tax Payment Type Code is not defined for the state of Louisiana', IDH_InconsistentData);
  end;
  if DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'INC_SUI_TAX_PAYMENT_CODE') = 'Y' then
    TaxPaymentCode := TaxPaymentCode + IntToStr(PaymentSequenceNbr);

  EndDate    := FormatDateTime('yymmdd', TaxPeriodEndDate);

  AmountStr  := IntToStr(RoundInt64(Abs(Amount) * 100));
  if Length(AmountStr) < 3 then
    AmountStr  := PadStringLeft(AmountStr, '0', 3);

  PinNumber  := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '');

  PaymentInfo := 'TXP*' + IaSuiEIN + '*' + TaxPaymentCode + '*' + EndDate +
                 '*T*' + AmountStr + '*****'+FEIN+'\';

  if not VarIsNull(SecondAddendumAmount) then
  begin
    AmountStr  := IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100));
    if Length(AmountStr) < 3 then
      AmountStr  := PadStringLeft(AmountStr, '0', 10);
    PaymentInfo := 'TXP*' + IaSuiEIN + '*' + TaxPaymentCode + '*' + EndDate +
                   '*T*' + AmountStr + '*****'+FEIN+'\';
  end;

  AddRecord(CoNbr, AgencyNbr, TaxPaymentAchNbr, Amount + ConvertNull(SecondAddendumAmount, 0),
    EffectiveDate, PaymentInfo, 'IA', SecondPaymentInfo, IaSuiEIN, StateEftName, 'IA SUI', '');

end;

procedure TevAchEFT.ProcessEFTPaymentSui(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer);
var
  PaymentInfo, SecondPaymentInfo, State, StateEIN, OverrideName: String;
  SYStateNbr, DepFreqNbr, RealCOStateNbr, CoNbr, GAgencyNbr: Integer;
  Total: Double;
  Date1, Date2, Date3: TDateTime;
  epn, POA, EFT: String;
begin
  PaymentInfo := 'TXP*';
  DM_COMPANY.CO_STATES.DataRequired('ALL');
  DM_COMPANY.CO_SUI.DataRequired('ALL');
  RealCOStateNbr := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_STATES_NBR');
  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE');
  if State = 'NJ' then
    PaymentInfo := PaymentInfo + 'B';
  if State = 'MN' then
  begin
    PaymentInfo := '000*';
  end;
  if State = 'CT' then
  begin
    DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_NBR'), []);
    epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '');
    if epn = '' then
      epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '');
    if epn = '' then
      epn := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN');
  end
  else
  if State = 'IL' then
  begin
    DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_NBR'), []);
    PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString + '*';
  end
  else
  if State = 'PA' then
  begin
    DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_NBR'), []);
    PaymentInfo := PaymentInfo + PadStringRight(DM_COMPANY.CO.FEIN.AsString, ' ', 15) + '*';
  end
  else
  begin
    if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '') = '' then
    begin
      if State = 'FL' then
        PaymentInfo := PaymentInfo + PadStringLeft(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), '0'), '0', 15) + '*'
      else
      if State = 'NV' then
        PaymentInfo := PaymentInfo + PadStringLeft(StripNonNumberChars(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')), '0', 9) + '*'
      else
        PaymentInfo := PaymentInfo + ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ') + '*';
    end
    else
    begin
      if State = 'FL' then
        PaymentInfo := PaymentInfo + PadStringLeft(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '0', 15) + '*'
      else
      if State = 'NV' then
        PaymentInfo := PaymentInfo + PadStringLeft(StripNonNumberChars(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')), '0', 9) + '*'
      else
        PaymentInfo := PaymentInfo + DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN') + '*';
    end;
  end;

  if State = 'NV' then
    StateEIN := StripNonNumberChars(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' '))
  else
  if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '') = '' then
    StateEIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')
  else
    StateEIN := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN');

  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
  SYStateNbr := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');
  DepFreqNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SY_STATE_DEPOSIT_FREQ_NBR');

  if ((State = 'AL') or (State = 'SC') or (State = 'MN') or (State = 'NM') or (State = 'NV') or (State = 'MA') or (State = 'AZ')) then
  else
  begin
    try
      PaymentInfo := PaymentInfo + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE');
    except
      raise EInconsistentData.CreateHelp('SUI Tax Payment Type Code is not defined for ' + State, IDH_InconsistentData);
    end;
    if DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'INC_SUI_TAX_PAYMENT_CODE') = 'Y' then
      PaymentInfo := PaymentInfo + IntToStr(PaymentSequenceNbr);
    PaymentInfo := PaymentInfo + '*';
  end;

  if State = 'SC' then
    PaymentInfo := PaymentInfo + FormatDateTime('yy', TaxPeriodEndDate) +
      IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '*'
  else
  if ((State = 'MN') or (State = 'NV') or (State = 'NM')) then
  begin
    CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
    DM_COMPANY.CO.DataRequired('ALL');
    DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
    PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString + '*';
  end
  else
  if State = 'MI' then
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', TaxPeriodEndDate)
  else
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', TaxPeriodEndDate) + '*';

  SecondPaymentInfo := '';
  if State = 'NE' then
  begin
    DM_SERVICE_BUREAU.SB.Activate;
    CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
    DM_COMPANY.CO.DataRequired('ALL');
    DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
    PaymentInfo := DM_COMPANY.CO.FEIN.AsString + '*' + StateEIN + '*'
      + FormatDateTime('yyyy"*"mm', GetEndQuarter(TaxPeriodEndDate)) + '*'
      + Copy(DM_COMPANY.CO.NAME.AsString + '                              ', 1, 30) + '*'
      + Copy(DM_SERVICE_BUREAU.SB.SB_NAME.AsString + '                  ', 1, 18) + '*/';
  end
  else
  if State = 'OR' then
  begin
    PaymentInfo := PaymentInfo + 'S*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*S*0000000000*S*' + PadStringLeft(IntToStr(RoundInt64(Abs(SecondAddendumAmount) * 100)), '0', 10) + '*\';
  end
  else
  if State = 'CA' then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100)), '0', 3)
      + '*T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*T*'
      + PadStringLeft(IntToStr(RoundInt64((Abs(VarToFloat(SecondAddendumAmount)) + Abs(Amount)) * 100)), '0', 3) + '*\';
  end
  else
  if State = 'MO' then
  begin
    DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_NBR'), []);  
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
      + '*' + DM_COMPANY.CO.FEIN.AsString + '*' + Trim(copy(DM_COMPANY.CO.NAME.AsString,1,24)) + '\';
  end
  else
  if State = 'NY' then
  begin
    PaymentInfo := PaymentInfo
      + 'S*'  + IntToStr(RoundInt64(Abs(Amount) * 100))
      + '*C*' + IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 0))) * 100))
      + '*L*' + IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) * 100))
      + '*' + PadStringLeft(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_PIN_NUMBER'), ''), ' ', 6) + '\';
  end
  else
  if State = 'DC' then
  begin
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
      + '*P*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 0))) * 100)), '0', 10)
      + '*I*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAddendumAmount, 1))) * 100)), '0', 10)
      + '*' + StateEIN + '*\';
  end
  else
  if State = 'FL' then
  begin
    PaymentInfo := PaymentInfo + '1*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****\';
  end
  else
  if State = 'TN' then
  begin
    CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
    DM_COMPANY.CO.DataRequired('ALL');
    DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
    PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*'
      + DM_COMPANY.CO.FEIN.AsString + '*' + Copy(DM_COMPANY.CO.NAME.AsString + '                         ', 1, 4) + '**\';
  end
  else
  if State = 'ME' then
  begin
    if VarIsNull(SecondAddendumAmount) then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '\'
    else
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount + SecondAddendumAmount[0]) * 100)), '0', 10) + '\';
  end
  else
  if State = 'HI' then
  begin
    ctx_PayrollCalculation.GetStateTaxDepositPeriod(State, DepFreqNbr, TaxPeriodEndDate, Date1, Date2, Date3);
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', Date1) + '*' + StringReplace(StateEIN, '-', '*', []) + '*N*\';
  end
  else
  begin
    if State = 'NJ' then
    begin
      CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
      DM_COMPANY.CO.DataRequired('ALL');
      DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount + ConvertNull(SecondAddendumAmount, 0)) * 100)), '0', 3) + '*'
        + Copy(DM_COMPANY.CO.NAME.AsString + '    ', 1, 4);
    end
    else
    if InPrenote and (State = 'NC') then
      PaymentInfo := PaymentInfo + 'Z*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****'
    else
    if State = 'NE' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****'
    else
    if State = 'NC' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
    else
    if State = 'OK' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
    if State = 'SC' then
      PaymentInfo := PaymentInfo + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*'
    else
    if State = 'IL' then
      PaymentInfo := PaymentInfo + 'U*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****'
    else
    if State = 'PA' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)
    else
    if State = 'NM' then
      PaymentInfo := PaymentInfo + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*' + StringOfChar('0', 43) + '*'
    else
    if State = 'MN' then
      PaymentInfo := PaymentInfo + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*' + StringOfChar('0', 42) + '*'
    else
    if State = 'NV' then
      PaymentInfo := PaymentInfo + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*'
        + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE') + '*000000*000000*                      *\'
    else
    if State = 'AL' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*****'
    else
    if State = 'ID' then
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
    if State = 'TX' then
      PaymentInfo := PaymentInfo + '1*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
    if State = 'MT' then
    begin
      CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
      DM_COMPANY.CO.DataRequired('ALL');
      DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
      PaymentInfo := PaymentInfo + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*' + Copy(DM_COMPANY.CO.NAME.AsString, 1, 20);
    end
    else
    if State = 'MI' then
      PaymentInfo := PaymentInfo + '*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
    else
      PaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3) + '*****';

    if not VarIsNull(SecondAddendumAmount) and (State <> 'NJ') then
      SecondPaymentInfo := PaymentInfo + 'T*' + PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAddendumAmount)) * 100)), '0', 3) + '*****';
    if State = 'MA' then
      PaymentInfo := PaymentInfo + Copy(DM_COMPANY.CO.NAME.AsString + '      ', 1, 6) + '\'
    else
    if State = 'SC' then
    begin
      CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
      DM_COMPANY.CO.DataRequired('ALL');
      DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);
      PaymentInfo := PaymentInfo + DM_COMPANY.CO.FEIN.AsString + '\';
    end
    else
    if State = 'ID' then
      PaymentInfo := PaymentInfo + '\'
    else
    if State = 'MI' then
      PaymentInfo := PaymentInfo + '\'
    else
      PaymentInfo := PaymentInfo + ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '') + '\';

    if not VarIsNull(SecondAddendumAmount) and (State <> 'NJ') then
      SecondPaymentInfo := SecondPaymentInfo + ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '') + '\';
  end;

  if State = 'VT' then
  begin
    DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;

    if DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Locate('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([107, 'POA']), [loCaseInsensitive]) then
      POA := DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.E_MAIL_ADDRESS.AsString
    else
      POA := '';

    if DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Locate('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([107, 'EFT Code']), [loCaseInsensitive]) then
      EFT := DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.E_MAIL_ADDRESS.AsString
    else
      EFT := '';

    PaymentInfo := 'DOL*' + POA + '*' +
                   ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'),'') + '*' +
                   '01111*' +
                   FormatDateTime('yymmdd', TaxPeriodEndDate) + '*T*' +
                   PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*' +
                   EFT + '\';

  end;

  if State = 'CT' then
  begin
    DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;
    PaymentInfo := 'TXP*' + DM_COMPANY.CO.FEIN.AsString + '*' +
                   DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE') + '*' +
                   FormatDateTime('yymmdd', GetEndQuarter(TaxPeriodEndDate)) + '*T*' +
                   PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) +'\';

  end;

  if State = 'MA' then
  begin
    epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '');
    if epn = '' then
      epn := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN');

    CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
    DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);

    PaymentInfo := 'TXP*' +
                   PadStringLeft(epn, ' ',9) + '*' +
                   PadStringLeft(DM_COMPANY.CO.FEIN.AsString, ' ',9) + '*' +
                   PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10) + '*' +
                   PadStringLeft('', '0', 43) + '*\';
  end;

  if State = 'AZ' then
  begin
    epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '');
    if epn = '' then
      epn := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN');

    DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;

    if DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Locate('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([62, 'AZURE ID']), [loCaseInsensitive]) then
      POA := DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.E_MAIL_ADDRESS.AsString
    else
      POA := '';

    PaymentInfo := 'TXP*' +
                   PadStringLeft(epn, ' ',7) + '*' +
                   PadStringLeft(DM_COMPANY.CO.FEIN.AsString, ' ',9) + '*1*' +
                   FormatDateTime('yyyy', TaxPeriodEndDate) + '*' +
                   IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '*' + POA + '*\';
  end;

  if State = 'AL' then
  begin
    epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '');
    if epn = '' then
      epn := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN');

    PaymentInfo := 'TXP*' +
                   epn + '*' +
                   IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '*' +
                   FormatDateTime('yyyy', TaxPeriodEndDate) + '\';
  end;
  
  if (State = 'NY') or (State = 'DC') then
    Total := Amount + ConvertNull(CheckVariant(SecondAddendumAmount, 0), 0) + ConvertNull(CheckVariant(SecondAddendumAmount, 1), 0)
  else
  if State = 'MO' then
    Total := Amount - ConvertNull(SecondAddendumAmount, 0)
  else
  if State = 'ME' then
  begin
    if not VarIsNull(SecondAddendumAmount) then
      Total := Amount + SecondAddendumAmount[0]
    else
      Total := Amount;
  end
  else
    Total := Amount + ConvertNull(SecondAddendumAmount, 0);

  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');

  CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');
  GAgencyNbr := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'SY_SUI_NBR'), 'SY_SUI_TAX_PMT_AGENCY_NBR');
  OverrideName := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_NAME'), '');

  AddRecord(CoNbr, GAgencyNbr, TaxPaymentAchNbr, Total, EffectiveDate, PaymentInfo, State, SecondPaymentInfo, StateEIN, OverrideName, State + ' SUI', epn);

end;

procedure TevAchEFT.ProcessEFTPayment(Amount: Double; SecondAddendumAmount: Variant; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr: Integer; TaxType: TTaxTable);
var
  State, EftName, TaxCode, StateEIN: String;
  SyStateNbr, DepFreqNbr, AgencyNbr, CoNbr: Integer;
  Total: Double;
  PaymentInfo, SecondPaymentInfo, SpecialFlag: String;
begin
  if TaxType = ttLoc then
    ProcessEFTPaymentLocal(Amount, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr)
  else
  if TaxType = ttSui then
  begin
    DM_COMPANY.CO_STATES.DataRequired('ALL');
    DM_COMPANY.CO_SUI.DataRequired('ALL');
    State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_STATES_NBR'), 'STATE');
    if State = 'LA' then
      ProcessEFTPaymentSuiLA(Amount, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr)
    else if State = 'AZ' then
      ProcessEFTPaymentSuiAZ(Amount, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr)
    else if State = 'IA' then
      ProcessEFTPaymentSuiIA(Amount, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr)
    else
      ProcessEFTPaymentSui(Amount, SecondAddendumAmount, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr);
  end
  else
  if TaxType = ttState then
  begin
    DM_COMPANY.CO_STATES.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
    DM_COMPANY.CO.DataRequired('ALL');

    State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE');
    SyStateNbr := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');
    DepFreqNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'SY_STATE_DEPOSIT_FREQ_NBR');
    CoNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'CO_NBR');
    AgencyNbr := DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SY_STATE_TAX_PMT_AGENCY_NBR');
    EftName := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE_EFT_NAME'), '');
    DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);

    if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE_EFT_EIN'), '') = '' then
      StateEIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE_EIN'), ' ')
    else
      StateEIN := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COTaxEntityNbr, 'STATE_EFT_EIN');

    try
      TaxCode := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'TAX_PAYMENT_TYPE_CODE');
    except
      raise EInconsistentData.CreateHelp('Tax Payment Type Code is not defined for ' + State, IDH_InconsistentData);
    end;

    if DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'INC_TAX_PAYMENT_CODE') = 'Y' then
      TaxCode := TaxCode + IntToStr(PaymentSequenceNbr);

    if State = 'CA' then
      ProcessCA(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'NJ' then
      ProcessNJ(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'AL' then
      ProcessAL(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'ME' then
      ProcessME(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'HI' then
      ProcessHI(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'DC' then
      ProcessDC(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'MS' then
      ProcessMS(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'TN' then
      ProcessTN(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'OR' then
      ProcessOR(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'NY' then
      ProcessNY(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'MO' then
      ProcessMO(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'FL' then
      ProcessFL(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'NE' then
      ProcessNE(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'NC' then
      ProcessNC(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'OK' then
      ProcessOK(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'ID' then
      ProcessID(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'MA' then
      ProcessMA(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else  
    if State = 'NM' then
      ProcessNM(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
    if State = 'IN' then
      ProcessIN(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else  
    if State = 'CO' then
      ProcessCO(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo)
    else
      ProcessXX(Amount, SecondAddendumAmount, TaxCode, StateEIN, EffectiveDate, TaxPeriodEndDate, PaymentSequenceNbr, COTaxEntityNbr, TaxPaymentAchNbr, AgencyNbr, Total, PaymentInfo, SecondPaymentInfo);

    if State = 'IL' then
      SpecialFlag := 'IL STATE';

    AddRecord(CoNbr, AgencyNbr, TaxPaymentAchNbr, Total, EffectiveDate, PaymentInfo, State, SecondPaymentInfo, StateEIN, EftName, SpecialFlag);

    ///

  end;
end;

procedure TevAchEFT.ProcessCA(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  TaxCode1: String;
  DepFreqNbr, TaxDepositsNbr: Integer;
  Second: Double;
begin

  DM_COMPANY.CO_TAX_PAYMENT_ACH.DataRequired('CO_TAX_PAYMENT_ACH_NBR='+IntToStr(TaxPaymentAchNbr));
  TaxDepositsNbr := DM_COMPANY.CO_TAX_PAYMENT_ACH.CO_TAX_DEPOSITS_NBR.AsInteger;
  DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR='+IntToStr(TaxDepositsNbr));

  DepFreqNbr := StrToIntDef(ExtractFromFiller(DM_COMPANY.CO_STATE_TAX_LIABILITIES.FILLER.AsString, 12, 8),
    DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SY_STATE_DEPOSIT_FREQ_NBR'));

  try
    TaxCode1 := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'TAX_PAYMENT_TYPE_CODE');
  except
    raise EInconsistentData.CreateHelp('Tax Payment Type Code is not defined for the state of California', IDH_InconsistentData);
  end;

  if DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'INC_TAX_PAYMENT_CODE') = 'Y' then
    TaxCode1 := TaxCode1 + IntToStr(PaymentSequenceNbr);

  Second := VarToFloat(SecondAmount);

  if Second < -0.005 then
    raise ETaxDepositException.Create('Can''t offset one tax bucket with another through EFT credit. This should be fixed through tax return by requesting a credit.');

  PaymentInfo :=
      Format('TXP*%s*%s*%s*T*%s*T*%s*T*%s*\',
       [StateEIN,
        TaxCode1,
        FormatDateTime('yymmdd', TaxPeriodEndDate),
        PadStringLeft(IntToStr(RoundInt64(Abs(Second) * 100)), '0', 3),
        PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3),
        PadStringLeft(IntToStr(RoundInt64((VarToFloat(SecondAmount) + Amount) * 100)), '0', 3)]);

  Total := Amount + ConvertNull(SecondAmount, 0);
end;

procedure TevAchEFT.ProcessNJ(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin
  if not VarIsNull(SecondAmount) then
    TaxCode := '01130';

  DM_COMPANY.CO.DataRequired('ALL');
  DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'CO_NBR'), []);

  PaymentInfo := Format('TXP*B%s*%s*%s*T*%s*%s\',
    [StateEIN,
     TaxCode,
     FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount + ConvertNull(SecondAmount, 0)) * 100)), '0', 3),
     Copy(DM_COMPANY.CO.NAME.AsString + '    ', 1, 4)]);

  Total := Amount + ConvertNull(SecondAmount, 0);
end;

procedure TevAchEFT.ProcessAL(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  EndDate, AmountStr, Pin: String;
begin
  EndDate := FormatDateTime('yymmdd', TaxPeriodEndDate);
  AmountStr := PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10);
  Pin := 'N' + PadStringLeft(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE_EFT_PIN_NUMBER'), ''), '0', 10);

  PaymentInfo := Format('TXP*IW%s*%s*%s*T*%s*                  *%s\',
    [StateEIN, TaxCode, EndDate, AmountStr, Pin]);

  if not VarIsNull(SecondAmount) then
  begin
    AmountStr := PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 10);
    SecondPaymentInfo := Format('TXP*IW%s*%s*%s*T*%s*                  *%s\',
      [StateEIN, TaxCode, EndDate, AmountStr, Pin]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);
end;

procedure TevAchEFT.ProcessME(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  SyStateNbr: Integer;
begin

  SyStateNbr := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', 'ME', 'SY_STATES_NBR');

  if not VarIsNull(SecondAmount) then
  begin
    try
      TaxCode := TaxCode + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE');
    except
      raise EInconsistentData.CreateHelp('SUI Tax Payment Type Code is not defined for the state of Maine', IDH_InconsistentData);
    end;
    if DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'INC_SUI_TAX_PAYMENT_CODE') = 'Y' then
      TaxCode := TaxCode + IntToStr(PaymentSequenceNbr);
  end;

  SecondPaymentInfo := '';

  if not VarIsNull(SecondAmount) then
    Total := Amount + SecondAmount[0]
  else
    Total := Amount;

  if (copy(TaxCode,1,5) = '01104') and (Length(TaxCode) > 5) then
    TaxCode := '13004';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate), PadStringLeft(IntToStr(RoundInt64(Abs(Total) * 100)), '0', 10)]);

  DM_COMPANY.CO_SUI.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');

  if not VarIsNull(SecondAmount) then
    AgencyNbr := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', SecondAmount[1], 'SY_SUI_NBR'),
      'SY_SUI_TAX_PMT_AGENCY_NBR');

end;

procedure TevAchEFT.ProcessHI(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  DepFreqNbr: Integer;
  Date1, Date2, Date3: TDateTime;
begin

  DepFreqNbr := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SY_STATE_DEPOSIT_FREQ_NBR');

  ctx_PayrollCalculation.GetStateTaxDepositPeriod('HI', DepFreqNbr, TaxPeriodEndDate, Date1, Date2, Date3);

  PaymentInfo := Format('TXP*00000000*%s*%s*%s*%s*N*\',
    [TaxCode,
     FormatDateTime('yymmdd', TaxPeriodEndDate),
     FormatDateTime('yymmdd', Date1),
     StringReplace(StateEIN, '-', '*', [])]);

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessDC(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'CO_NBR'), []);

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*P*%s*I*%s*%s*\',
    [DM_COMPANY.CO.FEIN.AsString,
    TaxCode,
    FormatDateTime('yymmdd', TaxPeriodEndDate),
    PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10),
    PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAmount, 0))) * 100)), '0', 10),
    PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAmount, 1))) * 100)), '0', 10),
    StateEIN]);

  Total := Amount + ConvertNull(CheckVariant(SecondAmount, 0), 0) + ConvertNull(CheckVariant(SecondAmount, 1), 0);

end;

procedure TevAchEFT.ProcessMS(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
    [StateEIN, TaxCode, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
      [StateEIN, TaxCode, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessTN(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*%s*%s**\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10),
     DM_COMPANY.CO.FEIN.AsString,
     Copy(DM_COMPANY.CO.NAME.AsString + '                         ', 1, 4)]);

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessOR(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  PaymentInfo := Format('TXP*%s*%s*%s*S*0000000000*S*%s*S*0000000000*\',
    [StateEIN, TaxCode,
     FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)]);

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessNY(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  Amount1, Amount2: String;  
begin

  if Abs(VarToFloat(CheckVariant(SecondAmount, 0))) > 0.005 then
    Amount1 := 'C*' + IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAmount, 0))) * 100))
  else
    Amount1 := '*';

  if Abs(VarToFloat(CheckVariant(SecondAmount, 1))) > 0.005 then
    Amount2 := 'L*' + IntToStr(RoundInt64(Abs(VarToFloat(CheckVariant(SecondAmount, 1))) * 100))
  else
    Amount2 := '*';

  PaymentInfo := Format('TXP*%s*%s*%s*S*%s*%s*%s*%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     IntToStr(RoundInt64(Abs(Amount) * 100)),
     Amount1, Amount2,
     PadStringLeft(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE_EFT_PIN_NUMBER'), ''), ' ', 6)]);

  Total := Amount + ConvertNull(CheckVariant(SecondAmount, 0), 0) + ConvertNull(CheckVariant(SecondAmount, 1), 0);

end;

procedure TevAchEFT.ProcessMO(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*T*%s\',
    [StateEIN, TaxCode,
    FormatDateTime('yymmdd', TaxPeriodEndDate),
    PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3),
    PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3)]);

  Total := Amount - ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessFL(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  PaymentInfo := Format('TXP*%s*%s*%s*1*%s*****\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessNE(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  Pin: String;
begin

  Pin := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SUI_EFT_PIN_NUMBER'), '');

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
    [StateEIN, TaxCode,
    FormatDateTime('yymmdd', TaxPeriodEndDate),
    PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10),
    Pin]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
      [StateEIN, TaxCode,
      FormatDateTime('yymmdd', TaxPeriodEndDate),
      PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 10),
      Pin]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessNC(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  SecondPaymentInfo := '';
  PaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 10)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
      [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(ConvertNull(SecondAmount, 0)) * 100)), '0', 10)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessOK(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*G*WTH*A*%s*%s*RTNPYM*%s*%s\',
    [StateEIN, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
     FormatDateTime('yyyymmdd', Date),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*G*WTH*A*%s*%s*RTNPYM*%s*%s\',
    [StateEIN, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
     FormatDateTime('yyyymmdd', Date),
     PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessID(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
      [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessMA(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
begin

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
    PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3),
    Copy(DM_COMPANY.CO.NAME.AsString + '      ', 1, 6)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
      [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3),
       Copy(DM_COMPANY.CO.NAME.AsString + '      ', 1, 6)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessNM(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer;
  var AgencyNbr: Integer; var Total: Double; var PaymentInfo,
  SecondPaymentInfo: String);
var
  State, Pin: String;
begin

  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE');
  Pin := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SUI_EFT_PIN_NUMBER'), '');

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s\',
      [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3)]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessIN(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  State, Pin: String;
begin

  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE');
  Pin := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SUI_EFT_PIN_NUMBER'), '');

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
    [StateEIN, TaxCode, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3), Pin]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
      [StateEIN, TaxCode, FormatDateTime('yyyymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3), Pin]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessCO(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  State, Pin: String;
  AmountP, AmountI: Double;
begin

  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE');
  Pin := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SUI_EFT_PIN_NUMBER'), '');

  SecondPaymentInfo := '';

  if not VarIsNull(SecondAmount) then
  begin
    AmountP := VarToFloat(SecondAmount[0]);
    AmountI := VarToFloat(SecondAmount[1]);
  end
  else
  begin
    AmountP := 0;
    AmountI := 0;
  end;

  PaymentInfo := Format('TXP*%s*%s*%s', [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate)]);

  if (Abs(Amount) > 0.005) then
    PaymentInfo := PaymentInfo + '*T*' + PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3)
  else
  if (Abs(AmountP) > 0.005) or (Abs(AmountI) > 0.005) or VarIsNull(SecondAmount) then
    PaymentInfo := PaymentInfo + '*T*0';

  if Abs(AmountP) > 0.005 then
    PaymentInfo := PaymentInfo + '*P*' + PadStringLeft(IntToStr(RoundInt64(Abs(AmountP) * 100)), '0', 3)
  else
  if (Abs(AmountI) > 0.005) or VarIsNull(SecondAmount) then
    PaymentInfo := PaymentInfo + '*P*0';

  if Abs(AmountI) > 0.005 then
    PaymentInfo := PaymentInfo + '*I*' + PadStringLeft(IntToStr(RoundInt64(Abs(AmountI) * 100)), '0', 3)
  else
  if (Abs(AmountP) > 0.005) or VarIsNull(SecondAmount) then
    PaymentInfo := PaymentInfo + '*I*0';

  PaymentInfo := PaymentInfo + '\';

  Total := Amount + AmountP + AmountI;

end;
procedure TevAchEFT.ProcessXX(Amount: Double; SecondAmount: Variant;
  TaxCode, StateEIN: String; EffectiveDate, TaxPeriodEndDate: TDateTime;
  PaymentSequenceNbr, CoStatesNbr, TaxPaymentAchNbr: Integer; var AgencyNbr: Integer; var Total: Double; var PaymentInfo, SecondPaymentInfo: String);
var
  State, Pin: String;
begin

  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'STATE');
  Pin := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'SUI_EFT_PIN_NUMBER'), '');

  SecondPaymentInfo := '';

  PaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
    [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
     PadStringLeft(IntToStr(RoundInt64(Abs(Amount) * 100)), '0', 3), Pin]);

  if not VarIsNull(SecondAmount) then
  begin
    SecondPaymentInfo := Format('TXP*%s*%s*%s*T*%s*****%s\',
      [StateEIN, TaxCode, FormatDateTime('yymmdd', TaxPeriodEndDate),
       PadStringLeft(IntToStr(RoundInt64(Abs(VarToFloat(SecondAmount)) * 100)), '0', 3), Pin]);
  end;

  Total := Amount + ConvertNull(SecondAmount, 0);

end;

procedure TevAchEFT.ProcessEFTPSPayment(Amount1, Amount2, Amount3: Double; EffectiveDate,
  TaxPeriodEndDate: TDateTime; CONbr, TaxPaymentAchNbr: Integer; TaxName: string; CobraCredit: Boolean);
var
  TaxCode, PaymentInfo: string;
  NewAmount1, NewAmount2, NewAmount3: Double;
begin
  TaxCode := GetEFTPSTaxCode(TaxName);
  if TaxCode = '' then
    Exit;
  NewAmount1 := Amount1;
  if Amount2 < 0 then
  begin
    NewAmount1 := NewAmount1 + Amount2;
    NewAmount2 := 0;
  end
  else
    NewAmount2 := Amount2;
  if Amount3 < 0 then
  begin
    NewAmount1 := NewAmount1 + Amount3;
    NewAmount3 := 0;
  end
  else
    NewAmount3 := Amount3;

  PaymentInfo := 'TXP*';
  DM_COMPANY.CO.DataRequired('ALL');
  PaymentInfo := PaymentInfo + ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CONbr, 'FEIN'), ' ') + '*';
  PaymentInfo := PaymentInfo + TaxCode + '*';
  if TaxCode = '09405' then
    PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', GetBeginMonth(GetEndYear(TaxPeriodEndDate)))
  else
  begin
    if TaxName = '945' then
      PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', GetBeginMonth(GetEndYear(TaxPeriodEndDate)))
    else
      PaymentInfo := PaymentInfo + FormatDateTime('yymmdd', TaxPeriodEndDate);
  end;
  if TaxCode = '09405' then
  begin
    PaymentInfo := PaymentInfo + '*09405*' + IntToStr(RoundInt64(Abs(NewAmount1) * 100)) + '\';
  end
  else
  if TaxCode = '09435' then
  begin
    PaymentInfo := PaymentInfo + '*09435*' + IntToStr(RoundInt64(Abs(Amount1 + Amount2 + Amount3) * 100)) + '****\';
  end
  else
  if TaxCode = '09455' then
  begin
    PaymentInfo := PaymentInfo + '*09455*' + IntToStr(RoundInt64(Abs(Amount1 + Amount2 + Amount3) * 100)) + '\';
  end
  else
  if CobraCredit then
  begin
    PaymentInfo := PaymentInfo + '*' + TaxCode + '*' + IntToStr(RoundInt64(Abs(Amount1 + Amount2 + Amount3) * 100)) + '********\';
  end
  else
  begin
    PaymentInfo := PaymentInfo + '*1*' + IntToStr(RoundInt64(Abs(NewAmount1) * 100));
    PaymentInfo := PaymentInfo + '*2*' + IntToStr(RoundInt64(NewAmount2 * 100));
    PaymentInfo := PaymentInfo + '*3*' + IntToStr(RoundInt64(NewAmount3 * 100)) + '\';
  end;

  DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');
  AddRecord(CONbr, DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', DM_COMPANY.CO.Lookup('CO_NBR', CONbr,
    'SY_FED_TAX_PAYMENT_AGENCY_NBR'), 'SY_GLOBAL_AGENCY_NBR'), TaxPaymentAchNbr, Amount1 + Amount2 + Amount3, EffectiveDate, PaymentInfo,
    '', '', ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CONbr, 'FEIN'), ''));

end;

function TevAchEFT.ACHDescription: string;
begin
  Result := 'Tax Payment ACH';
end;

procedure TevAchEFT.Backup;
begin
  EFT_ACH_Backup.Data := EFT_ACH.Data;
  EFT_ACH.Close;
  EFT_ACH.CreateDataSet;
end;

procedure TevAchEFT.Restore;
begin
  EFT_ACH.Data := EFT_ACH_Backup.Data;
end;

procedure TevAchEFT.SaveAch;
begin
  if EFT_ACH.RecordCount = 0 then
    Exit;
  AssignHeaderInfo;
  inherited SaveAch;
end;

procedure TevAchEFT.BackupCurrentFile;
var
  sBackupFileName: string;
begin
  sBackupFileName := ChangeFileExt(FileName, '.bak');
  DeleteFile(sBackupFileName);
  RenameFile(FileName, sBackupFileName);
end;

procedure TevAchEFT.StartPoolingAch;
begin
  FNeedToStartPooling := True;
  FACHPooled := True;
end;

procedure TevAchEFT.SaveClientAch(PostDBChanges: Boolean = True);
var
  DS: TevClientDataSet;
  S: string;
begin
  if EFT_ACH.RecordCount = 0 then
    Exit;
  if FNeedToStartPooling then
    StartPoolingAch_Internal;
  SavingACH := PostDBChanges;
  BackupCurrentFile;
  MainProcess;

  if ACHFile.RecordCount > 0 then
  begin
    DS := TevClientDataSet.Create(nil);
    try
      DS.Data := ACHFile.Data;
      WriteEof(True);
      SaveFileSaveACH;
      s := ACHFile.IndexName;
      ACHFile.IndexName := '';
      ACHFile.Data := DS.Data;
      ACHFile.AddIndex('By_CoPrSegOrd', 'cl_nbr;custom_company_number;pr_nbr;segment_nbr;order_nbr;',
        [ixCaseInsensitive], '', '', 0);
      ACHFile.IndexName := s;
    finally
      DS.Free;
    end;
  end;
end;

procedure TevAchEFT.StopPoolingAch;
var
  sBackupFileName: string;
begin
  if FNeedToStartPooling then
    Exit;
  sBackupFileName := ChangeFileExt(FileName, '.bak');
  DeleteFile(sBackupFileName);

  FileSaved := True;

  FACHPooled := False;
  WriteEof(True);
end;

procedure TevAchEFT.StartPoolingAch_Internal;
begin
  FNeedToStartPooling := False;
  FileHeaderProcessed := False;
  ClearGrandAndBatchTotals;
  AssignHeaderInfo;
end;

procedure TevAchEFT.FillStringList(const ResultList: TStringList);
var
  i, j: Integer;
  LV: IisListOfValues;
  SL, HD, EN: IisStringList;
begin
  ResultList.Clear;

  if EFT_ACH.RecordCount = 0 then
    Exit;

  AssignHeaderInfo;

  SavingACH := True;
  try
    MainProcess;
  finally
    SavingACH := False;
  end;

  if Options.BalanceBatches then
  begin
    RecreateForColumbiaEDPACH;
    ACHFile.IndexName := 'DEFAULT_ORDER';
  end
  else
  if (Combine = COMBINE_CO_NO_DD) or (Combine = COMBINE_CO_W_DD) or (Combine = COMBINE_CO_WP_DD) then
  begin
    CTSCombineTransactions;
    ACHFile.IndexName := 'DEFAULT_ORDER';
  end
  else
  if (Combine = COMBINE_CO_AND_SB) then
  begin
    CombineTransactions;
    ACHFile.IndexName := 'DEFAULT_ORDER';
  end;

  ACHFile.IndexName := 'DEFAULT_ORDER';

  ACHFile.First;
  while not ACHFile.Eof do
  begin
    if (Copy(ACHFile.ACH_TYPE.AsString + '  ', 1, 2) = '9:')
      and (ACHFile.ACH_OFFSET.AsInteger <> OFF_DONT_USE) then
      // skip this sub-total record
    else
      ResultList.Append(ACHFile.ACH_LINE.AsString +
        StringOfChar(' ', 94 - Length(ACHFile.ACH_LINE.AsString)));
    ACHFile.Next;
  end;
  LV := TisListOfValues.Create;
  HD := TisStringList.Create;
  EN := TisStringList.Create;
  SL := HD;
  for i := 0 to ResultList.Count - 1 do
  begin
    if copy(ResultList[i],1,1) = '5' then
    begin
      SL := TisStringList.Create;
      LV.AddValue(copy(ResultList[i],88,7), SL);
    end;
    if Assigned(SL) then
      SL.Add(ResultList[i])
    else
      EN.Add(ResultList[i]);
    if copy(ResultList[i],1,1) = '8' then
      SL := nil;
  end;
  ResultList.Clear;
  LV.Sort;
  for i := 0 to HD.Count - 1 do
    ResultList.Add(HD[i]);

  for j := 0 to LV.Count - 1 do
  begin
    SL := IInterface(LV.Values[j].Value) as IisStringList;
    for i := 0 to SL.Count - 1 do
      ResultList.Add(SL[i]);
  end;
  
  for i := 0 to EN.Count - 1 do
    ResultList.Add(EN[i]);

end;

function TevAchEFT.AchRecordCount: Integer;
begin
  if EFT_ACH.Active then
    Result := EFT_ACH.RecordCount
  else
    Result := 0;
end;

procedure TevAchEFT.LoadACHOptionsFromIniFile(const AACHOptionsIni: IisStringList);
var
  ACHOptions: TACHOptions;
  Ini: TMemIniFile;
  SL: TStringList;
begin
  if Assigned(AACHOptionsIni) then
  begin
    ACHOptions := TACHOptions.Create;
    Ini := TMemIniFile.Create({ExtractFilePath(Application.ExeName) + '..\ACHOptions.ini'}'');
    if Assigned(AACHOptionsIni) then
    begin
      SL := TStringList.Create;
      try
        SL.Text := AACHOptionsIni.Text;
        Ini.SetStrings(SL);
      finally
        SL.Free;
      end;
    end;
    try
      ACHOptions.BalanceBatches := Boolean(StrToInt(Ini.ReadString('ACH', 'BalanceACHBatches', '0')));
      ACHOptions.BlockDebits := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHBlockDebits', '0')));
      ACHOptions.CombineTrans := StrToInt(Ini.ReadString('ACH', 'CombineACHTrans', '0'));
      ACHOptions.USESBEIN := Boolean(StrToInt(Ini.ReadString('ACH', 'USESBEINForACH', '0')));
      ACHOptions.CTS := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHCTS', '0')));
      ACHOptions.SunTrust := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHSunTrust', '0')));
      ACHOptions.BlockSBCredit := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHBlockSBCredit', '0')));
      ACHOptions.FedReserve := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHFedReserve', '0')));
      ACHOptions.Intercept := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHIntercept', '0')));
      ACHOptions.PayrollHeader := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHPayrollHeader', '0')));
      ACHOptions.DescReversal := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHDescReversal', '0')));
      ACHOptions.BankOne := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHBankOne', '0')));
      ACHOptions.EffectiveDate := Boolean(StrToInt(Ini.ReadString('ACH', 'ACHEffectiveDate', '0')));
      ACHOptions.DisplayCoNbrAndName := Boolean(StrToInt(Ini.ReadString('ACH', 'DisplayCoNbrAndName', '0')));

      ACHOptions.CombineTreshold := StrToCurrDef(Ini.ReadString('ACH', 'CombineACHTreshold', '0.00'), 0);
      ACHOptions.CoId := Ini.ReadString('ACH', 'ACHCoId', '1');
      ACHOptions.SBEINOverride := Ini.ReadString('ACH', 'ACHSBEINOverride', '');
      ACHOptions.Header := Ini.ReadString('ACH', 'ACHHeader', '');
      ACHOptions.Footer := Ini.ReadString('ACH', 'ACHFooter', '');
      SetACHOptions(ACHOptions);
    finally
      Ini.Free;
      ACHOptions.Free;
    end;
  end;
end;

procedure TevAchEFT.DoOnConstruction;
begin
  inherited;

  EFT_ACH := TevAchEFTDataSet.Create(nil);
  EFT_ACH_Backup := TevAchEFTDataSet.Create(nil);

  DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');

  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');

  DM_TEMPORARY.TMP_CO.DataRequired('CO_NBR>0');

  InPrenote := False;
end;

destructor TevAchEFT.Destroy;
begin
  EFT_ACH.Free;
  EFT_ACH_Backup.Free;
  inherited;
end;

procedure TevAchEFT.ProcessEFTPaymentSuiAZ(Amount: Double;
  SecondAddendumAmount: Variant; EffectiveDate,
  TaxPeriodEndDate: TDateTime; PaymentSequenceNbr, COTaxEntityNbr,
  TaxPaymentAchNbr: Integer);
var
  PaymentInfo, SecondPaymentInfo, epn, POA: String;
  SYStateNbr, RealCOStateNbr, CoNbr, SySuiNbr, AgencyNbr: Integer;
  IaSuiEIN, TaxPaymentCode, EndDate, AmountStr, PinNumber, StateEftName, FEIN: String;
begin
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');

  RealCOStateNbr := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'CO_STATES_NBR');
  CoNbr          := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'CO_NBR');

  DM_COMPANY.CO.DataRequired('ALL');
  DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []);

  FEIN           := DM_COMPANY.CO.FEIN.AsString;
  SySuiNbr       := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', COTaxEntityNbr, 'SY_SUI_NBR');
  AgencyNbr      := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SySuiNbr, 'SY_SUI_TAX_PMT_AGENCY_NBR');
  StateEftName   := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'STATE_EFT_NAME'), '');
  SYStateNbr     := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', 'LA', 'SY_STATES_NBR');

  if ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '') = '' then
    IaSuiEIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN'), ' ')
  else
    IaSuiEIN := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN');

  try
    TaxPaymentCode :=  DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'SUI_TAX_PAYMENT_TYPE_CODE');
  except
    raise EInconsistentData.CreateHelp('SUI Tax Payment Type Code is not defined for the state of Louisiana', IDH_InconsistentData);
  end;
  if DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SYStateNbr, 'INC_SUI_TAX_PAYMENT_CODE') = 'Y' then
    TaxPaymentCode := TaxPaymentCode + IntToStr(PaymentSequenceNbr);

  EndDate    := FormatDateTime('yymmdd', TaxPeriodEndDate);

  AmountStr  := IntToStr(RoundInt64(Abs(Amount) * 100));
  if Length(AmountStr) < 3 then
    AmountStr  := PadStringLeft(AmountStr, '0', 3);

  PinNumber  := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_PIN_NUMBER'), '');

  epn := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EFT_EIN'), '');
  if epn = '' then
    epn := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', RealCOStateNbr, 'SUI_EIN');

  DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Activate;

  if DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.Locate('SY_GLOBAL_AGENCY_NBR;CONTACT_NAME', VarArrayOf([62, 'AZURE ID']), [loCaseInsensitive]) then
    POA := DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS.E_MAIL_ADDRESS.AsString
  else
    POA := '';

  PaymentInfo := 'TXP*' +
                 PadStringLeft(epn, ' ',7) + '*' +
                 PadStringLeft(DM_COMPANY.CO.FEIN.AsString, ' ',9) + '*0*' +
                 FormatDateTime('yyyy', TaxPeriodEndDate) + '*' +
                 IntToStr(GetQuarterNumber(TaxPeriodEndDate)) + '*' + POA + '*\';

  AddRecord(CoNbr, AgencyNbr, TaxPaymentAchNbr, Amount + ConvertNull(SecondAddendumAmount, 0),
    EffectiveDate, PaymentInfo, 'AZ', SecondPaymentInfo, IaSuiEIN, StateEftName, 'AZ SUI', '');

end;

procedure TevAchEFT.WriteBatchHeaderHuber(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?
  // ACH File Layout

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(DM_COMPANY.CO.NAME.AsString + '                ', 1, 16);
      s := '5200' + CompanyName;
    end;

    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := 'HUB';
      s := s + Copy(PadRight(CompanyData, ' ', 20), 1, 20);
    end;
  end;

  s := s + PadRight(aName, ' ', 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) + Copy(DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchEFT.WriteBatchHeaderAkron(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(DM_COMPANY.CO.NAME.AsString + '                ', 1, 16);
      s := '5200' + CompanyName;
    end;

    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := 'AKRON';
      s := s + Copy(PadRight(CompanyData, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) + Copy(DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchEFT.WriteBatchHeaderMASUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := ACHFile.NAME.AsString;
      s := s + Copy(PadRight(ACHFile.NAME.AsString, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) +Copy({aName}'MA DUA' + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchEFT.WriteBatchHeaderCTSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := ACHFile.NAME.AsString;
      s := copy(s, 1, 4) + PadRight(DM_COMPANY.CO.NAME.AsString, ' ', 16);
      s := s + Copy(PadRight(cn, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) +Copy(aName + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchEFT.WriteBatchHeaderMNSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?
  // ACH File Layout

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := ACHFile.NAME.AsString;
      s := s + Copy(PadRight(ACHFile.NAME.AsString, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) + Copy(DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchEFT.WriteBatchHeaderNMSUI(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?
  // ACH File Layout

  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := ACHFile.NAME.AsString;
      s := s + Copy(PadRight(ACHFile.NAME.AsString, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) + Copy('NMDWS          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

end.

