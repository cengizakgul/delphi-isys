unit EvAch;

interface

uses
  SysUtils, evConsts, EvUtils, IsBaseClasses, SACHTypes,
  EvCommonInterfaces, EvAchDataSet, EvAchUtils, DB, isBasicUtils,
   evStreamUtils, EvClientDataSet;

type

  TevTransactionType = (
    EMPLOYEE_DIRECT_DEPOSIT,
    CHILD_SUPPORT_PAYMENT,
    AGENCY_DIRECT_DEPOSIT,
    DIRECT_DEPOSIT_IMPOUND,
    CHILD_SUPPORT_IMPOUND,
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT,
    BILLING_IMPOUND_DEBIT_CLIENT,
    BILLING_IMPOUND_OFFSET_CREDIT,
    BILLING_IMPOUND_OFFSET_DEBIT,
    BILLING_IMPOUND_CREDIT_BUREAU,
    TRUST_IMPOUND_DEBIT_CLIENT,
    TRUST_IMPOUND_OFFSET_CREDIT,
    TRUST_IMPOUND_OFFSET_DEBIT,
    TRUST_IMPOUND_CREDIT_BUREAU,
    TAX_IMPOUND_DEBIT_CLIENT,
    TAX_IMPOUND_OFFSET_CREDIT,
    TAX_IMPOUND_OFFSET_DEBIT,
    TAX_IMPOUND_CREDIT_BUREAU,
    WC_IMPOUND_DEBIT_CLIENT,
    WC_IMPOUND_OFFSET_CREDIT,
    WC_IMPOUND_OFFSET_DEBIT,
    WC_IMPOUND_CREDIT_BUREAU,
    EFT_TAX_PAYMENT_DEBIT_ZERO,
    EFT_TAX_PAYMENT_CREDIT_ZERO
  );

type

  IevTransaction = interface
  ['{68F9CE8C-4165-4810-BCE1-1E2142950AA6}']
    function GetHSA: String;
    procedure SetHSA(const Value: String);
    function GetSTATE: String;
    procedure SetSTATE(const Value: String);
    function GetPayrollType: String;
    procedure SetPayrollType(const Value: String);
    function IsDirDepOffset: Boolean;
    function IsDirDepImpound: Boolean;
    function TypeDescr: String;
    function BlockedSb: Boolean;
    function BlockedCl: Boolean;
    function BankAccRegisterType: String;
    function OffsetType: Integer;
    function GetACH_ENTRY: IisListOfValues;
    procedure SetACH_ENTRY(const Value: IisListOfValues);
    function GetBANK_ACCOUNT_NBR: Integer;
    procedure SetBANK_ACCOUNT_NBR(const Value: Integer);
    function GetSign: String;
    function GetAmount: Currency;
    function GetBANK_ACCOUNT_NUMBER: String;
    function GetABA_NUMBER: String;
    function GetBANK_ACCOUNT_TYPE: String;
    function GetNAME: String;
    function GetCUSTOM_EMPLOYEE_NUMBER: String;
    function GetIN_PRENOTE: String;
    function GetAddenda: String;
    function GetEFFECTIVE_DATE: TDateTime;
    function GetCHECK_DATE: TDateTime;
    function GetTRANSACTION_TYPE: TevTransactionType;
    function GetRUN_NUMBER: Integer;
    function GetIMPOUND_TYPE: String;
    function GetPR_NBR: Integer;
    procedure SetEFFECTIVE_DATE(const Value:TDateTime);
    procedure SetCHECK_DATE(const Value: TDateTime);
    procedure SetTRANSACTION_TYPE(const Value: TevTransactionType);
    procedure SetAmount(const Value: Currency);
    function  GetCompanyDatas: String;
    procedure SetCompanyDatas(const Value: String);
    procedure SetBANK_ACCOUNT_NUMBER(const Value: String);
    procedure SetABA_NUMBER(const Value: String);
    procedure SetBANK_ACCOUNT_TYPE(const Value: String);
    procedure SetNAME(const Value: String);
    procedure SetCUSTOM_EMPLOYEE_NUMBER(const Value: String);
    function GetSSN: String;
    procedure SetSSN(const Value: String);
    procedure SetIN_PRENOTE(const Value: String);
    procedure SetAddenda(const Value: String);
    procedure SetRUN_NUMBER(const Value: Integer);
    procedure SetIMPOUND_TYPE(const Value: String);
    procedure SetPR_NBR(const Value: Integer);
    function GetCL_NBR: Integer;
    procedure SetCL_NBR(const Value: Integer);
    function GetUniqueID: String;
    procedure SetUniqueID(const Value: String);
    function GetBatch: IisListOfValues;
    procedure SetBatch(const Value: IisListOfValues);
    function GetIsReversal: Boolean;
    function GetAsXML: String;
    function TransactionCode: String;
    function IsImpound: Boolean;
    function IsOffset: Boolean;
    function IsDirectDeposit: Boolean;
    property AsXml: String read GetAsXML;
    property Amount: Currency read GetAmount write SetAmount;
    property BANK_ACCOUNT_NUMBER: String read GetBANK_ACCOUNT_NUMBER write SetBANK_ACCOUNT_NUMBER;
    property ABA_NUMBER: String read GetABA_NUMBER write SetABA_NUMBER;
    property BANK_ACCOUNT_TYPE: String read GetBANK_ACCOUNT_TYPE write SetBANK_ACCOUNT_TYPE;
    property NAME: String read GetNAME write SetNAME;
    property CUSTOM_EMPLOYEE_NUMBER: String read GetCUSTOM_EMPLOYEE_NUMBER write SetCUSTOM_EMPLOYEE_NUMBER;
    property SSN: String read GetSSN write SetSSN;
    property IN_PRENOTE: String read GetIN_PRENOTE write SetIN_PRENOTE;
    property Addenda: String read GetAddenda write SetAddenda;
    property CL_NBR: Integer read GetCL_NBR write SetCL_NBR;
    property PR_NBR: Integer read GetPR_NBR write SetPR_NBR;
    property CHECK_DATE: TDateTime read GetCHECK_DATE write SetCHECK_DATE;
    property RUN_NUMBER: Integer read GetRUN_NUMBER write SetRUN_NUMBER;
    property EFFECTIVE_DATE: TDateTime read GetEFFECTIVE_DATE write SetEFFECTIVE_DATE;
    property TRANSACTION_TYPE: TevTransactionType read GetTRANSACTION_TYPE write SetTRANSACTION_TYPE;
    property IMPOUND_TYPE: String read GetIMPOUND_TYPE write SetIMPOUND_TYPE;
    property Sign: String read GetSign;
    property Batch: IisListOfValues read GetBatch write SetBatch;
    property UniqueID: String read GetUniqueID write SetUniqueID;
    property BANK_ACCOUNT_NBR: Integer read GetBANK_ACCOUNT_NBR write SetBANK_ACCOUNT_NBR;
    property ACH_ENTRY: IisListOfValues read GetACH_ENTRY write SetACH_ENTRY;
    property IsReversal: Boolean read GetIsReversal;
    property CompanyDatas: String read GetCompanyDatas write SetCompanyDatas;
    property PayrollType: String read GetPayrollType write SetPayrollType;
    property STATE: String read GetSTATE write SetSTATE;
    property HSA: String read GetHSA write SetHSA;
  end;

  IevAchFile = interface
  ['{FAD77DED-798B-47F1-83A1-4B7C7BB01827}']
    function GetInTesting: Boolean;
    procedure SetInTesting(const Value: Boolean);
    function GetHeaderLine(const Index: Integer): String;
    function AddTransaction(const Tran: IevTransaction;
                            const ClientName: String;
                            const LegalName: String;
                            const CUSTOM_COMPANY_NUMBER: String;
                            const CompanyName: String): IisListOfValues;
    function GetTransactionType(const Value: IevTransaction): String;
    function GetSbOriginBankAba: String;
    function GetAddendaLine(const Batch, Item: IisListOfValues): String;
    function GetAchFiles(const TempString: String): IisListOfValues;
    function GetDetailLine(const Batch, Item: IisListOfValues): String;
    function TransactionCount(const Batch: IisListOfValues): Integer;
    function GetFileHeaderLine: String;
    function GetFileControlLine: String;
    procedure PopulateReportDataset(Dataset: TevClientDataset);
    function GetBatchDebits(const Batch: IisListOfValues): Currency;
    function GetBatchCredits(const Batch: IisListOfValues): Currency;
    function GetTransaction(const Batch: IisListOfValues; const Index: Integer): IisListOfValues;
    function GetBatchControlLine(const Index: Integer): String;
    function BatchCount: Integer;
    function GetBatchByIndex(const Index: Integer): IisListOfValues;
    function GetFileDebits: Currency;
    function GetFileCredits: Currency;
    function GetFileHash: Int64;
    function GetFileEntryCount: Integer;
    function GetPRINT_CLIENT_NAME: String;
    procedure SetPRINT_CLIENT_NAME(const Value: String);
    function GetBANK_ACCOUNT_REGISTER_NAME: String;
    procedure SetBANK_ACCOUNT_REGISTER_NAME(const Value: String);
    function GetCL_NBR: Integer;
    procedure SetCL_NBR(const Value: Integer);
    function GetCO_NBR: Integer;
    procedure SetCO_NBR(const Value: Integer);
    function GetCLIENT_NAME: String;
    procedure SetCLIENT_NAME(const Value: String);
    function GetLEGAL_NAME: String;
    procedure SetLEGAL_NAME(const Value: String);
    function GetCUSTOM_COMPANY_NUMBER: String;
    procedure SetCUSTOM_COMPANY_NUMBER(const Value: String);
    function GetCOMPANY_NAME: String;
    procedure SetCOMPANY_NAME(const Value: String);
    function GetWELLS_FARGO_ACH_FLAG: String;
    procedure SetWELLS_FARGO_ACH_FLAG(const Value: String);
    function GetCOMPANY_IDENTIFICATION: String;
    procedure SetCOMPANY_IDENTIFICATION(const Value: String);
    function GetCOID: String;
    procedure SetCOID(const Value: String);
    function GetFEIN: String;
    procedure SetFEIN(const Value: String);
    function GetSB_EIN: String;
    procedure SetSB_EIN(const Value: String);
    function BatchesBalanced: Boolean;
    function UnbalancedBatches: IisListOfValues;
    function AllBatches: IisListOfValues;
    property FEIN: String read GetFEIN write SetFEIN;
    property SB_EIN: String read GetSB_EIN write SetSB_EIN;
    property SbOriginBankAba: String read GetSbOriginBankAba;
    property PRINT_CLIENT_NAME: String read GetPRINT_CLIENT_NAME write SetPRINT_CLIENT_NAME;
    property BANK_ACCOUNT_REGISTER_NAME: String read GetBANK_ACCOUNT_REGISTER_NAME write SetBANK_ACCOUNT_REGISTER_NAME;
    property CL_NBR: Integer read GetCL_NBR write SetCL_NBR;
    property CO_NBR: Integer read GetCO_NBR write SetCO_NBR;
    property CLIENT_NAME: String read GetCLIENT_NAME write SetCLIENT_NAME;
    property LEGAL_NAME: String read GetLEGAL_NAME write SetLEGAL_NAME;
    property CUSTOM_COMPANY_NUMBER: String read GetCUSTOM_COMPANY_NUMBER write SetCUSTOM_COMPANY_NUMBER;
    property COMPANY_NAME: String read GetCOMPANY_NAME write SetCOMPANY_NAME;
    property WELLS_FARGO_ACH_FLAG: String read GetWELLS_FARGO_ACH_FLAG write SetWELLS_FARGO_ACH_FLAG;
    property COMPANY_IDENTIFICATION: String read GetCOMPANY_IDENTIFICATION write SetCOMPANY_IDENTIFICATION;
    property COID: String read GetCOID write SetCOID;
    property InTesting: Boolean read GetInTesting write SetInTesting;
  end;

  TevTransaction = class(TisInterfacedObject, IevTransaction)
  private
    FAmount: Currency;
    FBANK_ACCOUNT_NBR: Integer;
    FBANK_ACCOUNT_NUMBER: String;
    FABA_NUMBER: String;
    FBANK_ACCOUNT_TYPE: String;
    FNAME: String;
    FCUSTOM_EMPLOYEE_NUMBER: String;
    FSSN: String;
    FIN_PRENOTE: String;
    FAddenda: String;
    FCHECK_DATE: TDateTime;
    FRUN_NUMBER: Integer;
    FEFFECTIVE_DATE: TDateTime;
    FTRANSACTION_TYPE: TevTransactionType;
    FIMPOUND_TYPE: String;
    FCompanyDatas: String;
    FPR_NBR: Integer;
    FPayrollType: String;
    FCL_NBR: Integer;
    FBatch: IisListOfValues;
    FACH_ENTRY: IisListOfValues;
    FUniqueID: String;
    FSTATE: String;
    FHSA: String;
  protected
    function GetHSA: String;
    procedure SetHSA(const Value: String);
    function GetSTATE: String;
    procedure SetSTATE(const Value: String);
    function GetPayrollType: String;
    procedure SetPayrollType(const Value: String);
    function IsImpound: Boolean;
    function IsOffset: Boolean;
    function IsDirectDeposit: Boolean;
    function IsDirDepOffset: Boolean;
    function IsDirDepImpound: Boolean;
    function BankAccRegisterType: String;
    function BlockedSb: Boolean;
    function BlockedCl: Boolean;
    function TypeDescr: String;
    function OffsetType: Integer;
    procedure WriteToStream(const AStream: IisStream); override;
    procedure ReadFromStream(const AStream: IisStream); override;
    function GetIsReversal: Boolean;
    function GetACH_ENTRY: IisListOfValues;
    procedure SetACH_ENTRY(const Value: IisListOfValues);
    function GetBatch: IisListOfValues;
    procedure SetBatch(const Value: IisListOfValues);
    function GetSign: String;
    function GetAmount: Currency;
    function GetBANK_ACCOUNT_NUMBER: String;
    function GetABA_NUMBER: String;
    function GetBANK_ACCOUNT_TYPE: String;
    function GetNAME: String;
    function GetCUSTOM_EMPLOYEE_NUMBER: String;
    function GetSSN: String;
    procedure SetSSN(const Value: String);
    function GetIN_PRENOTE: String;
    function GetAddenda: String;
    function GetCHECK_DATE: TDateTime;
    function GetRUN_NUMBER: Integer;
    function GetEFFECTIVE_DATE: TDateTime;
    function GetTRANSACTION_TYPE: TevTransactionType;
    function GetIMPOUND_TYPE: String;
    function  GetCompanyDatas: String;
    function TransactionCode: String;
    procedure SetCompanyDatas(const Value: String);
    procedure SetAmount(const Value: Currency);
    procedure SetBANK_ACCOUNT_NUMBER(const Value: String);
    procedure SetABA_NUMBER(const Value: String);
    procedure SetBANK_ACCOUNT_TYPE(const Value: String);
    procedure SetNAME(const Value: String);
    procedure SetCUSTOM_EMPLOYEE_NUMBER(const Value: String);
    procedure SetIN_PRENOTE(const Value: String);
    procedure SetAddenda(const Value: String);
    procedure SetEFFECTIVE_DATE(const Value:TDateTime);
    procedure SetCHECK_DATE(const Value: TDateTime);
    procedure SetRUN_NUMBER(const Value: Integer);
    procedure SetTRANSACTION_TYPE(const Value: TevTransactionType);
    procedure SetIMPOUND_TYPE(const Value: String);
    function GetPR_NBR: Integer;
    procedure SetPR_NBR(const Value: Integer);
    function GetCL_NBR: Integer;
    procedure SetCL_NBR(const Value: Integer);
    function GetBANK_ACCOUNT_NBR: Integer;
    procedure SetBANK_ACCOUNT_NBR(const Value: Integer);
    function GetUniqueID: String;
    procedure SetUniqueID(const Value: String);
    function GetAsXML: String;
  public
    constructor Create(Params: IisListOfValues); overload;
    constructor Create; overload;
    class function GetTypeID: String; override;
  end;

  TevAchFile = class(TisInterfacedObject, IevAchFile)
  private
    FBatches: IisListOfValues;
    FSbOriginBankAba: String;
    FSbOriginPrintName: String;
    FFileHeaderAba: String;
    FSbEinNumber: String;
    FPRINT_CLIENT_NAME: String;
    FBANK_ACCOUNT_REGISTER_NAME: String;
    FCTSID: String;
    FSbName: String;
    FOptions: IevAchOptions;
    FReferenceCode: String;
    FCL_NBR, FCO_NBR: Integer;
    FSbBankAccountNbr: Integer;
    FForNonAchReport: Boolean;
    FCLIENT_NAME: String;
    FLEGAL_NAME: String;
    FCUSTOM_COMPANY_NUMBER: String;
    FCOMPANY_NAME: String;
    FWELLS_FARGO_ACH_FLAG: String;
    FCOMPANY_IDENTIFICATION: String;
    FCOID: String;
    FIncludeOffsets: Integer;
    FInTesting: Boolean;
    FFEIN: String;
    FSB_EIN: String;
    function GetBatch(const Tran: IevTransaction): IisListOfValues;
    function PayrollComment: String;
    function GetBatchComment(const Tran: IevTransaction): String;
    function ReversalDesc(const Value: TevTransactionType; const Amount: Currency): String;
    function GetTransactionClass(const Tran: IevTransaction): String;
    function TransactionGoesAlone(const Tran: IevTransaction): Boolean;
    function GetAchEntryID(const Tran: IevTransaction): String;
  protected
    function GetSB_EIN: String;
    procedure SetSB_EIN(const Value: String);
    function GetFEIN: String;
    procedure SetFEIN(const Value: String);
    function GetInTesting: Boolean;
    procedure SetInTesting(const Value: Boolean);
    function GetTransactionDescr(const Tran: IevTransaction): String;
    function GetTransactionName(const Value: IevTransaction): String;
    function GetTransactionType(const Value: IevTransaction): String;
    function BatchesBalanced: Boolean;
    function UnbalancedBatches: IisListOfValues;
    function AllBatches: IisListOfValues;
    function GetHeaderLine(const Index: Integer): String;
    function GetDetailLine(const Batch, Item: IisListOfValues): String;
    function GetAddendaLine(const Batch, Item: IisListOfValues): String;
    function GetBatchControlLine(const Index: Integer): String;
    function GetBatchDebits(const Batch: IisListOfValues): Currency;
    function GetBatchCredits(const Batch: IisListOfValues): Currency;
    function GetFileDebits: Currency;
    function GetFileCredits: Currency;
    function GetFileHeaderLine: String;
    function GetFileControlLine: String;
    function GetFileHash: Int64;
    function GetFileEntryCount: Integer;
    function GetFileLineCount: Integer;
    function GetBatchLineCount(const Batch: IisListOfValues): Integer;
    function AddTransaction(const Tran: IevTransaction;
                            const ClientName: String;
                            const LegalName: String;
                            const CUSTOM_COMPANY_NUMBER: String;
                            const CompanyName: String): IisListOfValues;
    function TransactionCount(const Batch: IisListOfValues): Integer;
    function BatchCount: Integer;
    function GetBatchByIndex(const Index: Integer): IisListOfValues;
    function GetTransaction(const Batch: IisListOfValues; const Index: Integer): IisListOfValues;
    function GetSbOriginBankAba: String;
    procedure PopulateReportDataset(Dataset: TevClientDataset);
    procedure SortBatches;
    function GetAchFiles(const TempString: String): IisListOfValues;
    function GetTextFile: IisStringList;
    procedure SetDatasetParams(const CL_NBR, CO_NBR: Integer);
    function GetPRINT_CLIENT_NAME: String;
    procedure SetPRINT_CLIENT_NAME(const Value: String);
    function GetBANK_ACCOUNT_REGISTER_NAME: String;
    procedure SetBANK_ACCOUNT_REGISTER_NAME(const Value: String);
    function GetCL_NBR: Integer;
    procedure SetCL_NBR(const Value: Integer);
    function GetCO_NBR: Integer;
    procedure SetCO_NBR(const Value: Integer);
    function GetCLIENT_NAME: String;
    procedure SetCLIENT_NAME(const Value: String);
    function GetLEGAL_NAME: String;
    procedure SetLEGAL_NAME(const Value: String);
    function GetCUSTOM_COMPANY_NUMBER: String;
    procedure SetCUSTOM_COMPANY_NUMBER(const Value: String);
    function GetCOMPANY_NAME: String;
    procedure SetCOMPANY_NAME(const Value: String);
    function GetWELLS_FARGO_ACH_FLAG: String;
    procedure SetWELLS_FARGO_ACH_FLAG(const Value: String);
    function GetCOMPANY_IDENTIFICATION: String;
    procedure SetCOMPANY_IDENTIFICATION(const Value: String);
    function GetCOID: String;
    procedure SetCOID(const Value: String);
  public
    constructor Create(const Options: IevAchOptions;
                       const SbOriginBankAba: String;
                       const CTSID: String;
                       const SbName: String;
                       const SbOriginPrintName: String;
                       const FileHeaderAba: String;
                       const SbEinNumber: String;
                       const SbBankAccountNbr: Integer;
                       const ForNonAchReport: Boolean;
                       const IncludeOffsets: Integer);
  end;

implementation

{ TevAchFile }

function TevAchFile.TransactionGoesAlone(const Tran: IevTransaction): Boolean;
begin
  Result :=
     (    (TAchCombineOptions(FOptions.CombineTrans) = COMBINE_DO_NOT)
       or ((TAchCombineOptions(FOptions.CombineTrans) = COMBINE_CO_WP_DD) and
          Tran.IsReversal and (Tran.TRANSACTION_TYPE in [
           DIRECT_DEPOSIT_IMPOUND,
           CHILD_SUPPORT_IMPOUND,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT]))
       or ((TAchCombineOptions(FOptions.CombineTrans) = COMBINE_CO_NO_DD) and
          (Tran.TRANSACTION_TYPE in [
           DIRECT_DEPOSIT_IMPOUND,
           CHILD_SUPPORT_IMPOUND,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT]))
       or (Tran.Addenda <> '')
       or (Tran.IN_PRENOTE = 'Y')
       or (Tran.TRANSACTION_TYPE in [EMPLOYEE_DIRECT_DEPOSIT, CHILD_SUPPORT_PAYMENT, EFT_TAX_PAYMENT_DEBIT_ZERO, EFT_TAX_PAYMENT_CREDIT_ZERO])
     );
end;

function TevAchFile.GetTransactionDescr(
  const Tran: IevTransaction): String;
begin
  if Tran.IsImpound or Tran.IsOffset then
    Result := GetTransactionName(Tran)
  else
    Result := Tran.NAME;
end;

function TevAchFile.GetAchEntryID(const Tran: IevTransaction): String;
begin
  Result := Format('%s;%s;%s;%s;',
                     [GetTransactionDescr(Tran),
                      Tran.ABA_NUMBER,
                      Tran.BANK_ACCOUNT_TYPE,
                      Tran.BANK_ACCOUNT_NUMBER]);

  if Tran.IsOffset then
    Result := Result + Format('%d;%s;', [Tran.OffsetType, Tran.Sign])
  else
    Result := Result + '0;+;';
end;

function TevAchFile.GetBatchComment(const Tran: IevTransaction): String;
begin
  if Tran.IsReversal
  //and not (Tran.IsDirDepImpound and FOptions.BalanceBatches and (FIncludeOffsets <> OFF_DONT_USE))
//  and not (FOptions.BalanceBatches and (Tran.TRANSACTION_TYPE = TAX_IMPOUND_DEBIT_CLIENT))
  then
  begin
    Result := 'REVERSAL';
    if FOptions.DescReversal then
      Result := Result + ReversalDesc(Tran.TRANSACTION_TYPE, Tran.Amount);
  end
  else
  if (Tran.TRANSACTION_TYPE = DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT)
  and (FOptions.BalanceBatches or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT)) then
    Result := PayrollComment
  else
  if Tran.IsOffset and (FOptions.BalanceBatches or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT)) then
    Result := 'IMPOUND'
  else
  if Tran.IsOffset then
    Result := 'ACHOFFSET'
  else
  case Tran.TRANSACTION_TYPE of
  EFT_TAX_PAYMENT_DEBIT_ZERO: Result := 'EFTPAYMENT';
  EFT_TAX_PAYMENT_CREDIT_ZERO: Result := 'EFTPAYMENT';
  EMPLOYEE_DIRECT_DEPOSIT: Result := PayrollComment;
  CHILD_SUPPORT_PAYMENT, CHILD_SUPPORT_IMPOUND:
  begin
    if FOptions.BalanceBatches then
      Result := 'IMPOUND'
    else
      Result := 'CHILD SUPP';
  end;
  AGENCY_DIRECT_DEPOSIT                : Result := PayrollComment;
  DIRECT_DEPOSIT_IMPOUND               :
  begin
    if FOptions.BalanceBatches and (FIncludeOffsets = OFF_DONT_USE) then
      Result := PayrollComment
    else
    if FOptions.BalanceBatches or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT)
    then
      Result := 'IMPOUND'
    else
      Result := PayrollComment;
  end;
  BILLING_IMPOUND_DEBIT_CLIENT,
  BILLING_IMPOUND_CREDIT_BUREAU:
  begin
    if FOptions.BalanceBatches
    or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
      Result := 'IMPOUND'
    else
      Result := 'BILLING';
  end;
  TRUST_IMPOUND_DEBIT_CLIENT,
  TRUST_IMPOUND_CREDIT_BUREAU:
  begin
    if FOptions.BalanceBatches
    or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
      Result := 'IMPOUND'
    else
      Result := 'TRUST';
  end;
  TAX_IMPOUND_DEBIT_CLIENT,
  TAX_IMPOUND_CREDIT_BUREAU:
  begin
    if FOptions.BalanceBatches
    or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
      Result := 'IMPOUND'
    else
      Result := 'TAX';
  end;
  WC_IMPOUND_DEBIT_CLIENT,
  WC_IMPOUND_CREDIT_BUREAU:
  begin
    if FOptions.BalanceBatches
    or (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
      Result := 'IMPOUND'
    else
      Result := 'WORKERS CO';
  end;
  else Result := 'INKNOWN';
  end;
end;

function TevAchFile.AddTransaction(
  const Tran: IevTransaction;
  const ClientName: String;
  const LegalName: String;
  const CUSTOM_COMPANY_NUMBER: String;
  const CompanyName: String): IisListOfValues;
var
  Transactions: IisListOfValues;
  CompanyNumber: String;
  UniqueID: String;
  TransactionType: String;
  Batch, Entry: IisListOfValues;
begin
  Batch := GetBatch(Tran);
  Result := Batch;

  if not Batch.ValueExists('PR_NBR') then
    Batch.AddValue('PR_NBR', Tran.PR_NBR); // for crazy report

  if TransactionGoesAlone(Tran) then
    UniqueID := GetUniqueID
  else
    UniqueID := GetAchEntryID(Tran);

  Tran.UniqueID := UniqueID;

  Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;

  if not TransactionGoesAlone(Tran) and Transactions.ValueExists(UniqueID) then
  begin
    Entry := IInterface(Transactions.Value[UniqueID]) as IisListOfValues;
    Entry.Value['AMOUNT'] := Entry.Value['AMOUNT'] + Tran.Amount;
  end
  else
  begin
    Entry := TisListOfValues.Create;
    Transactions.AddValue(UniqueID, Entry);
    CompanyNumber := Batch.Value['CompanyNumber'];
    Entry.AddValue('UniqueID', UniqueID);
    Entry.AddValue('PaymentType', Tran.IMPOUND_TYPE);
    TransactionType := GetTransactionType(Tran);
    Entry.AddValue('TransactionType', TransactionType);
    Entry.AddValue('IsImpound', Tran.IsImpound);
    Entry.AddValue('TRANSACTION_TYPE', Integer(Tran.TRANSACTION_TYPE));
    Entry.AddValue('IN_PRENOTE', Tran.IN_PRENOTE);
    Entry.AddValue('CUSTOM_COMPANY_NUMBER', CompanyNumber);
    Entry.AddValue('BATCH_COMMENTS', Batch.Value['COMMENTS']);
    Entry.AddValue('BANK_ACCOUNT_NUMBER', Tran.BANK_ACCOUNT_NUMBER);
    Entry.AddValue('HSA', Tran.HSA);
    Entry.AddValue('ABA_NUMBER', Tran.ABA_NUMBER);
    Entry.AddValue('BANK_ACCOUNT_TYPE', Tran.BANK_ACCOUNT_TYPE);
    Entry.AddValue('ACH_OFFSET', Tran.OffsetType);
    Entry.AddValue('NAME', GetTransactionDescr(Tran));
    Entry.AddValue('AMOUNT', Tran.Amount);
    Entry.AddValue('TRANSACTION_CODE', Tran.TransactionCode);

    if FOptions.CTS then
    begin
      if Tran.CUSTOM_EMPLOYEE_NUMBER <> '' then
        Entry.AddValue('IDENTIFICATION_NUMBER', PadRight(Trim(StringReplace(Tran.SSN, '-', '', [rfReplaceAll])),' ', 15))
      else if Tran.TransactionCode <> '22' then
        Entry.AddValue('IDENTIFICATION_NUMBER', PadRight(Trim(CompanyNumber), ' ', 13) + '01')
      else if FOptions.SunTrust then
        Entry.AddValue('IDENTIFICATION_NUMBER', PadRight(Trim(FCTSID), ' ', 13) + '01')
      else
        Entry.AddValue('IDENTIFICATION_NUMBER', Tran.CUSTOM_EMPLOYEE_NUMBER);
    end
    else
      Entry.AddValue('IDENTIFICATION_NUMBER', Tran.CUSTOM_EMPLOYEE_NUMBER);

    Entry.AddValue('BANK_ACCOUNT_NBR', Tran.BANK_ACCOUNT_NBR);

    Entry.AddValue('CL_NBR', FCL_NBR);
    Entry.AddValue('CO_NBR', FCO_NBR);
    Entry.AddValue('PR_NBR', Tran.PR_NBR);

    if Tran.Addenda <> '' then
      Entry.AddValue('Addenda', Tran.Addenda);

    if Tran.IN_PRENOTE = 'Y' then
      Entry.Value['AMOUNT'] := 0;

    if not FOptions.DisplayCoNbrAndName then
      Entry.AddValue('ReceiverName', PadRight(GetTransactionDescr(Tran), ' ', 22))
    else
      Entry.AddValue('ReceiverName', PadRight(CompanyNumber + ' ' +
        RemovePunctuation(CompanyName), ' ', 22));
  end;
  Tran.ACH_ENTRY := Entry;
end;

constructor TevAchFile.Create(const Options: IevAchOptions;
  const SbOriginBankAba, CTSID, SbName,
  SbOriginPrintName, FileHeaderAba, SbEinNumber: String;
  const SbBankAccountNbr: Integer;
  const ForNonAchReport: Boolean;
  const IncludeOffsets: Integer);
begin
  FInTesting := False;
  FBatches := TisListOfValues.Create;
  FOptions := Options;
  FIncludeOffsets := IncludeOffsets;
  FSbOriginBankAba := SbOriginBankAba;
  FCTSID := CTSID;
  FSbName := SbName;
  FSbOriginPrintName := SbOriginPrintName;
  FFileHeaderAba := FileHeaderAba;
  FSbEinNumber := SbEinNumber;
  FReferenceCode := ZeroFillInt64ToStr(0, 8);
  FSbBankAccountNbr := SbBankAccountNbr;
  FForNonAchReport := ForNonAchReport;
end;

function TevAchFile.GetTransactionClass(const Tran: IevTransaction): String;
begin
  case Tran.TRANSACTION_TYPE of
  EFT_TAX_PAYMENT_DEBIT_ZERO           : Result := 'CCD';
  EFT_TAX_PAYMENT_CREDIT_ZERO          : Result := 'CCD';
  EMPLOYEE_DIRECT_DEPOSIT              : Result := 'PPD';
  CHILD_SUPPORT_PAYMENT                : Result := 'CCD';
  AGENCY_DIRECT_DEPOSIT                : Result := 'PPD';
  DIRECT_DEPOSIT_IMPOUND               :
  begin
    if (FIncludeOffsets <> OFF_DONT_USE) and not (Tran.IsReversal and FOptions.BalanceBatches) then
      Result := 'CCD'
    else
      Result := 'PPD';
  end;
  CHILD_SUPPORT_IMPOUND                : Result := 'CCD';
  DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT :
  begin
    if not (Tran.IsReversal and FOptions.BalanceBatches) then
      Result := 'CCD'
    else
      Result := 'PPD';
  end;
  DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  :
  begin
    if not FOptions.BalanceBatches then
      Result := 'CCD'
    else
      Result := 'PPD';
  end;
  CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := 'CCD';
  CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := 'CCD';
  BILLING_IMPOUND_DEBIT_CLIENT         : Result := 'CCD';
  BILLING_IMPOUND_OFFSET_CREDIT        : Result := 'CCD';
  BILLING_IMPOUND_OFFSET_DEBIT         : Result := 'CCD';
  BILLING_IMPOUND_CREDIT_BUREAU        : Result := 'CCD';
  TRUST_IMPOUND_DEBIT_CLIENT           : Result := 'CCD';
  TRUST_IMPOUND_OFFSET_CREDIT          : Result := 'CCD';
  TRUST_IMPOUND_OFFSET_DEBIT           : Result := 'CCD';
  TRUST_IMPOUND_CREDIT_BUREAU          : Result := 'CCD';
  TAX_IMPOUND_DEBIT_CLIENT             : Result := 'CCD';
  TAX_IMPOUND_OFFSET_CREDIT            : Result := 'CCD';
  TAX_IMPOUND_OFFSET_DEBIT             : Result := 'CCD';
  TAX_IMPOUND_CREDIT_BUREAU            : Result := 'CCD';
  WC_IMPOUND_DEBIT_CLIENT              : Result := 'CCD';
  WC_IMPOUND_OFFSET_CREDIT             : Result := 'CCD';
  WC_IMPOUND_OFFSET_DEBIT              : Result := 'CCD';
  WC_IMPOUND_CREDIT_BUREAU             : Result := 'CCD';
  else raise Exception.Create('Unknown ACH transaction type.');
  end;
end;

function TevAchFile.ReversalDesc(const Value: TevTransactionType; const Amount: Currency): String;
begin
  case Value of
  AGENCY_DIRECT_DEPOSIT :
  if FOptions.PayrollHeader then
    Result := '-P'
  else
    Result := '-A';

  EMPLOYEE_DIRECT_DEPOSIT,
  CHILD_SUPPORT_PAYMENT,
  DIRECT_DEPOSIT_IMPOUND,
  CHILD_SUPPORT_IMPOUND:
    if FOptions.PayrollHeader then
      Result := '-P'
    else
      Result := '-D';

  DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
  DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
  CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
  CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT:
    if FOptions.PayrollHeader then
      Result := '+P'
    else
      Result := '+D';
      
  BILLING_IMPOUND_DEBIT_CLIENT,
  BILLING_IMPOUND_CREDIT_BUREAU        : Result := '-B';
  BILLING_IMPOUND_OFFSET_CREDIT,
  BILLING_IMPOUND_OFFSET_DEBIT         : Result := '+B';

  TRUST_IMPOUND_DEBIT_CLIENT,
  TRUST_IMPOUND_CREDIT_BUREAU          : Result := '-U';
  TRUST_IMPOUND_OFFSET_CREDIT,
  TRUST_IMPOUND_OFFSET_DEBIT           : Result := '+U';


  TAX_IMPOUND_DEBIT_CLIENT             : Result := '-T';
  TAX_IMPOUND_OFFSET_CREDIT            : Result := '+T';
  TAX_IMPOUND_OFFSET_DEBIT             : Result := '+T';
  TAX_IMPOUND_CREDIT_BUREAU            : Result := '-T';
  WC_IMPOUND_DEBIT_CLIENT,
  WC_IMPOUND_CREDIT_BUREAU             : Result := '-W';
  WC_IMPOUND_OFFSET_CREDIT,
  WC_IMPOUND_OFFSET_DEBIT              : Result := '+W';
  else Result := '';
  end;
end;

function TevAchFile.GetTransactionType(const Value: IevTransaction): String;
begin
  if Value.IsOffset  then
    Result := 'ACHOFFSET'
  else
  if Value.IsReversal and Value.IsImpound
  and (TAchCombineOptions(FOptions.CombineTrans) = COMBINE_DO_NOT) then
    Result := 'REVERSAL'
  else
  if Value.IsImpound and (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
    Result := 'IMPOUND'
  else
    case Value.TRANSACTION_TYPE of
    EMPLOYEE_DIRECT_DEPOSIT              : Result := PayrollComment;
    CHILD_SUPPORT_PAYMENT                : Result := 'CHILD SUPP';
    AGENCY_DIRECT_DEPOSIT                : Result := PayrollComment;
    DIRECT_DEPOSIT_IMPOUND               : Result := 'DD IMPOUND';
    CHILD_SUPPORT_IMPOUND                : Result := 'DD IMPOUND';
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT : Result := 'DD IMPOUND';
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  : Result := 'DD IMPOUND';
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := 'CHILD SUPP';
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := 'CHILD SUPP';
    BILLING_IMPOUND_DEBIT_CLIENT         : Result := 'BILLING';
    BILLING_IMPOUND_OFFSET_CREDIT        : Result := 'BILLING';
    BILLING_IMPOUND_OFFSET_DEBIT         : Result := 'BILLING';
    BILLING_IMPOUND_CREDIT_BUREAU        : Result := 'BILLING';
    TRUST_IMPOUND_DEBIT_CLIENT           : Result := 'TRUST';
    TRUST_IMPOUND_OFFSET_CREDIT          : Result := 'TRUST';
    TRUST_IMPOUND_OFFSET_DEBIT           : Result := 'TRUST';
    TRUST_IMPOUND_CREDIT_BUREAU          : Result := 'TRUST';
    TAX_IMPOUND_DEBIT_CLIENT             : Result := 'TAX';
    TAX_IMPOUND_OFFSET_CREDIT            : Result := 'TAX';
    TAX_IMPOUND_OFFSET_DEBIT             : Result := 'TAX';
    TAX_IMPOUND_CREDIT_BUREAU            : Result := 'TAX';
    WC_IMPOUND_DEBIT_CLIENT              : Result := 'WORKERS';
    WC_IMPOUND_OFFSET_CREDIT             : Result := 'WORKERS';
    WC_IMPOUND_OFFSET_DEBIT              : Result := 'WORKERS';
    WC_IMPOUND_CREDIT_BUREAU             : Result := 'WORKERS';
    else Result := '';
    end;
end;

function TevAchFile.GetTransactionName(const Value: IevTransaction): String;
begin
  if Value.IsOffset and (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
    Result := 'IMPOUND'
  else
  if Value.IsImpound and (TAchCombineOptions(FOptions.CombineTrans) <> COMBINE_DO_NOT) then
    Result := 'IMPOUND'
  else
    case Value.TRANSACTION_TYPE of
    DIRECT_DEPOSIT_IMPOUND               : Result := 'DD IMPOUND';
    CHILD_SUPPORT_IMPOUND                : Result := 'DD IMPOUND';
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT : Result := 'DD IMPOUND';
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  : Result := 'DD IMPOUND';
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := 'DD IMPOUND';
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := 'DD IMPOUND';
    BILLING_IMPOUND_DEBIT_CLIENT         : Result := 'BILLING IMPOUND';
    BILLING_IMPOUND_OFFSET_CREDIT        : Result := 'BILLING OFFSET';
    BILLING_IMPOUND_OFFSET_DEBIT         : Result := 'BILLING OFFSET';
    BILLING_IMPOUND_CREDIT_BUREAU        : Result := 'BILLING IMPOUND';
    TRUST_IMPOUND_DEBIT_CLIENT           : Result := 'TRUST IMPOUND';
    TRUST_IMPOUND_OFFSET_CREDIT          : Result := 'TRUST OFFSET';
    TRUST_IMPOUND_OFFSET_DEBIT           : Result := 'TRUST OFFSET';
    TRUST_IMPOUND_CREDIT_BUREAU          : Result := 'TRUST IMPOUND';
    TAX_IMPOUND_DEBIT_CLIENT             : Result := 'TAX IMPOUND';
    TAX_IMPOUND_OFFSET_CREDIT            : Result := 'TAX OFFSET';
    TAX_IMPOUND_OFFSET_DEBIT             : Result := 'TAX OFFSET';
    TAX_IMPOUND_CREDIT_BUREAU            : Result := 'TAX IMPOUND';
    WC_IMPOUND_DEBIT_CLIENT              : Result := 'WORKERS COMP IMPOUND';
    WC_IMPOUND_OFFSET_CREDIT             : Result := 'WORKERS COMP OFFSET';
    WC_IMPOUND_OFFSET_DEBIT              : Result := 'WORKERS COMP OFFSET';
    WC_IMPOUND_CREDIT_BUREAU             : Result := 'WORKERS COMP IMPOUND';
    else Result := '';
    end;
end;

function TevAchFile.GetAddendaLine(const Batch,
  Item: IisListOfValues): String;
var
  Number: Integer;
begin
  Number := (IInterface(Batch.Value['Transactions']) as IisListOfValues).IndexOf(Item.Value['UniqueID']) + 1;
  Result := Item.Value['Addenda'] + ZeroFillInt64ToStr(Number, 7);
end;

function TevAchFile.PayrollComment: String;
begin
  if not FOptions.PayrollHeader then
    Result := 'NET=PAY'
  else
    Result := 'PAYROLL';
end;

function TevAchFile.GetBatch(const Tran: IevTransaction): IisListOfValues;
var
  BatchName: String;
  Transactions: IisListOfValues;
  Name: String;
  ClassCode, Comments, Uniq: String;
  WYChildSupport, MEChildSupport, ALChildSupport, MIChildSupport: Boolean;
  CompanyIdentification, LineCompanyName, CompanyDatas, CompanyDatasUnique, BatchUniqID: String;
  ChildSupportTypes: set of TevTransactionType;
begin

  ClassCode := GetTransactionClass(Tran);
  Comments  := GetBatchComment(Tran);

  if FPRINT_CLIENT_NAME = GROUP_BOX_YES then
    Name := FCLIENT_NAME
  else
  if FBANK_ACCOUNT_REGISTER_NAME <> '' then
    Name := FBANK_ACCOUNT_REGISTER_NAME
  else
    Name := FCOMPANY_NAME;

  if FOptions.BalanceBatches then
    ChildSupportTypes := [CHILD_SUPPORT_PAYMENT, CHILD_SUPPORT_IMPOUND, CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT, CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT]
  else
    ChildSupportTypes := [CHILD_SUPPORT_PAYMENT];

  WYChildSupport := (Tran.STATE = 'WY') and (Tran.TRANSACTION_TYPE in ChildSupportTypes);
  MEChildSupport := (Tran.STATE = 'ME') and (Tran.TRANSACTION_TYPE in ChildSupportTypes);
  MIChildSupport := (Tran.STATE = 'MI') and (Tran.TRANSACTION_TYPE in ChildSupportTypes);
  ALChildSupport := (Tran.STATE = 'AL') and (Tran.TRANSACTION_TYPE in ChildSupportTypes);

  if WYChildSupport then
    Uniq := FFEIN +'/'+ FSB_EIN
  else if MEChildSupport then
    Uniq := FFEIN +'/'+ FSB_EIN
  else if MIChildSupport then
    Uniq := FFEIN +'/'+ FSB_EIN
  else if ALChildSupport then
    Uniq := FFEIN +'/'+ FSB_EIN
  else
    Uniq := '';

  if MIChildSupport then
    CompanyIdentification := PadRight('1' + FCOMPANY_IDENTIFICATION, ' ', 10)
  else if WYChildSupport then
    CompanyIdentification := PadRight('9' + FCOMPANY_IDENTIFICATION, ' ', 10)
  else
    CompanyIdentification := PadRight(FCOID + FCOMPANY_IDENTIFICATION, ' ', 10);

  if FOptions.SunTrust then
  begin
    LineCompanyName := PadRight(Name, ' ', 16);
    CompanyDatas := PadRight(PadRight(FCUSTOM_COMPANY_NUMBER, ' ', 9) + '*BATCHID', ' ', 20);
  end
  else
  begin
    if FOptions.BankOne then
      LineCompanyName := PadRight(PadRight(FCUSTOM_COMPANY_NUMBER, ' ', 8) + ' ' + Name, ' ', 16)
    else
    begin
      LineCompanyName := PadRight(PadRight(FCUSTOM_COMPANY_NUMBER, ' ', 5) + '*BATCHID', ' ', 16);
    end;

    if Tran.TRANSACTION_TYPE in [EFT_TAX_PAYMENT_DEBIT_ZERO, EFT_TAX_PAYMENT_CREDIT_ZERO] then
      CompanyDatas := Tran.CompanyDatas
    else
    if FOptions.CTS or FOptions.Intercept then
      CompanyDatas := PadRight(Name, ' ', 8) + PadRight(Trim(FCUSTOM_COMPANY_NUMBER), ' ', 6) + PadRight(FCTSID, ' ', 6)
    else
      CompanyDatas := PadRight(Trim(FCUSTOM_COMPANY_NUMBER) + ' ' + Name, ' ', 20);
  end;

  if ALChildSupport then
  begin
    LineCompanyName := PadRight(FCOMPANY_NAME, ' ', 16);
    CompanyDatas := PadRight(Trim(copy(Name,1,10)) + ' ' + FFEIN, ' ', 20);
  end
  else
  if WYChildSupport then
  begin
    LineCompanyName := PadRight(FCOMPANY_NAME, ' ', 16);
    CompanyDatas := PadRight(Trim(copy(Name,1,10)) + ' ' + FFEIN, ' ', 20);
  end
  else
  if MIChildSupport then
  begin
    LineCompanyName := PadRight(FCOMPANY_NAME, ' ', 16);
    CompanyDatas := PadRight(FFEIN, ' ', 20);
  end
  else
  if MEChildSupport then
  begin
    LineCompanyName := PadRight(FCOMPANY_NAME, ' ', 16);
    CompanyDatas := PadRight(FFEIN, ' ', 20);
  end;

  CompanyDatasUnique := CompanyDatas;

  BatchName := Format('%s;%s;%s;%s;%s;%s;%s;%s;%s;%s',
                                 [FCUSTOM_COMPANY_NUMBER,
                                  Name,
                                  Comments,
                                  DateToStr(Tran.CHECK_DATE),
                                  DateToStr(Tran.EFFECTIVE_DATE),
                                  ClassCode,
                                  CompanyIdentification, LineCompanyName, CompanyDatas,
                                  Uniq
                                  ]);

  if FBatches.ValueExists(BatchName) then
    Result := IInterface(FBatches.Value[BatchName]) as IisListOfValues
  else
  begin
    Result := TisListOfValues.Create;
    BatchUniqID := Format('%10.10d', [Random(9999999999)]);

    if pos('*BATCHID', CompanyDatas) > 0 then
      CompanyDatas := PadRight(PadRight(FCUSTOM_COMPANY_NUMBER, ' ', 9) + '*' + BatchUniqID, ' ', 20);

    if pos('*BATCHID', LineCompanyName) > 0 then
      LineCompanyName := PadRight(PadRight(FCUSTOM_COMPANY_NUMBER, ' ', 5) + '*' + BatchUniqID, ' ', 16);


    FBatches.AddValue(BatchName, Result);

    Result.AddValue('BatchUniqID', BatchUniqID);
    Result.AddValue('WELLS_FARGO_ACH_FLAG', FWELLS_FARGO_ACH_FLAG);
    Result.AddValue('ClientName', FCLIENT_NAME);
    Result.AddValue('LegalName', FLEGAL_NAME);
    Result.AddValue('CompanyNumber', FCUSTOM_COMPANY_NUMBER);
    Result.AddValue('CompanyName', FCOMPANY_NAME);
    Result.AddValue('CheckDate', Tran.CHECK_DATE);
    Result.AddValue('RunNumber', Tran.RUN_NUMBER);
    Result.AddValue('EffectiveDate', Tran.EFFECTIVE_DATE);
    Result.AddValue('Comments', Comments);
    Result.AddValue('ClassCode', ClassCode);
    Transactions := TisListOfValues.Create;
    Result.AddValue('Transactions', Transactions);

    Result.AddValue('CL_NBR', FCL_NBR);
    Result.AddValue('CO_NBR', FCO_NBR);

    Result.AddValue('CompanyIdentification', CompanyIdentification);
    Result.AddValue('LineCompanyName', LineCompanyName);
    Result.AddValue('CompanyDatas', CompanyDatas);

    Result.AddValue('NAME', Name);

  end;
end;

function TevAchFile.GetBatchCredits(
  const Batch: IisListOfValues): Currency;
var
  Transactions, Item: IisListOfValues;
  i: Integer;
begin
  Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
  Result := 0;
  for i := 0 to Transactions.Count - 1 do
  begin
    Item := IInterface(Transactions[i].Value) as IisListOfValues;
    if Item.Value['AMOUNT'] > 0 then
      Result := Result - Item.Value['AMOUNT'];
  end;
end;

function TevAchFile.GetBatchDebits(const Batch: IisListOfValues): Currency;
var
  Transactions, Item: IisListOfValues;
  i: Integer;
begin
  Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
  Result := 0;
  for i := 0 to Transactions.Count - 1 do
  begin
    Item := IInterface(Transactions[i].Value) as IisListOfValues;
    if Item.Value['AMOUNT'] < 0 then
      Result := Result - Item.Value['AMOUNT'];
  end;
end;

function TevAchFile.GetFileDebits: Currency;
var
  Batch, Transactions, Item: IisListOfValues;
  i, j: Integer;
begin
  Result := 0;
  for i := 0 to FBatches.Count - 1 do
  begin
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      if Item.Value['AMOUNT'] < 0 then
        Result := Result - Item.Value['AMOUNT'];
    end;
  end;
end;

function TevAchFile.GetFileCredits: Currency;
var
  Batch, Transactions, Item: IisListOfValues;
  i, j: Integer;
begin
  Result := 0;
  for i := 0 to FBatches.Count - 1 do
  begin
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      if Item.Value['AMOUNT'] > 0 then
        Result := Result + Item.Value['AMOUNT'];
    end;
  end;
end;

function TevAchFile.GetFileHash: Int64;
var
  Batch, Transactions, Item: IisListOfValues;
  i, j: Integer;
begin
  Result := 0;
  for i := 0 to FBatches.Count - 1 do
  begin
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      IncHashTotal(Item.Value['ABA_NUMBER'], Result);
    end;
  end;
end;

function TevAchFile.GetFileEntryCount: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to FBatches.Count - 1 do
    Result := Result + GetBatchLineCount(GetBatchByIndex(i));
end;

function TevAchFile.GetFileLineCount: Integer;
var
  i: Integer;
  Batch: IisListOfValues;
begin
  Result := 2;
  for i := 0 to FBatches.Count - 1 do
  begin
    Batch := IInterface(FBatches[i].Value) as IisListOfValues;
    Result := Result + GetBatchLineCount(Batch) + 2;
  end;
end;

function TevAchFile.GetBatchLineCount(const Batch: IisListOfValues): Integer;
var
  Transactions, Item: IisListOfValues;
  i: Integer;
begin
  Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
  Result := 0;
  for i := 0 to Transactions.Count - 1 do
  begin
    Item := IInterface(Transactions[i].Value) as IisListOfValues;
    Result := Result + 1;
    if Item.ValueExists('Addenda') then
      Result := Result + 1;
  end;
end;

function TevAchFile.GetBatchControlLine(const Index: Integer): String;
var
  Transactions, Item: IisListOfValues;
  i, Entries: Integer;
  EntryHash: Int64;
  Debits, Credits: Currency;
  Batch: IisListOfValues;
begin
  Batch := GetBatchByIndex(Index);
  Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
  EntryHash := 0;
  Debits := 0;
  Credits := 0;
  Entries := 0;
  for i := 0 to Transactions.Count - 1 do
  begin
    Item := IInterface(Transactions[i].Value) as IisListOfValues;
    IncHashTotal(Item.Value['ABA_NUMBER'], EntryHash);
    if Item.Value['AMOUNT'] > 0 then
      Credits := Credits + Item.Value['AMOUNT']
    else
      Debits := Debits - Item.Value['AMOUNT'];
    Inc(Entries);
    if Item.ValueExists('Addenda') then
      Inc(Entries);
  end;

  Result := UpperCase(
    '8200' + ZeroFillInt64ToStr(Entries, 6) +
    ZeroFillInt64ToStr(EntryHash, 10) +
    ZeroFillInt64ToStr(RoundInt64(Debits * 100), 12) +
    ZeroFillInt64ToStr(RoundInt64(Credits * 100), 12) +
    Batch.Value['CompanyIdentification'] + // 10
    '                   ' + '      ' +
    copy(FSbOriginBankAba + '        ', 1, 8) + PadLeft(IntToStr(Index+1), '0', 7));

end;

function TevAchFile.GetDetailLine(const Batch,
  Item: IisListOfValues): String;
var
  AddendaIndicator: String;
  Number: Integer;
begin
  if Item.ValueExists('Addenda') then
    AddendaIndicator := '1'
  else
    AddendaIndicator := '0';

  Number := (IInterface(Batch.Value['Transactions']) as IisListOfValues).IndexOf(Item.Value['UniqueID']) + 1;

  Result :=
    UpperCase(
      '6' +
      Item.Value['TRANSACTION_CODE'] +
      PadRight(Item.Value['ABA_NUMBER'], ' ', 9) +
      PadRight(Item.Value['BANK_ACCOUNT_NUMBER'], ' ', 17) +
      FormatAchAmount(Item.Value['AMOUNT']) +
      PadRight(Trim(Item.Value['IDENTIFICATION_NUMBER']), ' ', 15) +
      Item.Value['ReceiverName'] +
      PadRight(Item.Value['HSA'], ' ', 2) + // DiscretionaryData
      AddendaIndicator +
      PadRight(FSbOriginBankAba, ' ', 8) + ZeroFillInt64ToStr(Number, 7) // Trace Number
    );
end;

function TevAchFile.GetHeaderLine(const Index: Integer): String;
var
  OriginatingDFI: String;
  CompanyEntryDescription: String;
  Batch: IisListOfValues;
begin
  Batch := GetBatchByIndex(Index);
  OriginatingDFI := PadRight(FSbOriginBankAba, ' ', 8);
  CompanyEntryDescription := PadRight(Batch.Value['WELLS_FARGO_ACH_FLAG'] + Batch.Value['Comments'], ' ', 10);

  Result :=
   UpperCase('5200' +
              Batch.Value['LineCompanyName'] + // 16
              Batch.Value['CompanyDatas'] + // 20
              PadRight(Batch.Value['CompanyIdentification'], ' ', 10) +
              Batch.Value['ClassCode'] +
              CompanyEntryDescription +
              FormatDateTime('YYMMDD', Batch.Value['CheckDate']) +
              FormatDateTime('YYMMDD', Batch.Value['EffectiveDate']) +
              '   1' +
              OriginatingDFI +
              PadLeft(IntToStr(Index+1), '0', 7)
            );
end;

function TevAchFile.GetSbOriginBankAba: String;
begin
  Result := FSbOriginBankAba;
end;

function TevAchFile.GetTransaction(const Batch: IisListOfValues;
  const Index: Integer): IisListOfValues;
begin
  Result := IInterface((IInterface(Batch.Value['Transactions']) as IisListOfValues).Values[Index].Value) as IisListOfValues;
end;

function TevAchFile.TransactionCount(
  const Batch: IisListOfValues): Integer;
begin
  Result := (IInterface(Batch.Value['Transactions']) as IisListOfValues).Count;
end;

function TevAchFile.BatchCount: Integer;
begin
  Result := FBatches.Count;
end;

function TevAchFile.GetBatchByIndex(const Index: Integer): IisListOfValues;
begin
  Result := IInterface(FBatches[Index].Value) as IisListOfValues;
end;

function TevAchFile.GetFileHeaderLine: String;
begin
  Result := UpperCase('101 ' + FFileHeaderAba +
            FSbEinNumber + FormatDateTime('YYMMDDHHMM', Now) +
            'A094101' + PadRight(FSbOriginPrintName, ' ', 23) +
            PadRight(FSbName, ' ', 23) + FReferenceCode);
end;

function TevAchFile.GetFileControlLine: String;
begin
  Result := UpperCase(
    PadRight(
    '9' + ZeroFillInt64ToStr(BatchCount, 6) +
    ZeroFillInt64ToStr((GetFileLineCount + 9) div 10, 6) +
    ZeroFillInt64ToStr(GetFileEntryCount, 8) +
    ZeroFillInt64ToStr(GetFileHash, 10) +
    ZeroFillInt64ToStr(RoundInt64(GetFileDebits * 100), 12) +
    ZeroFillInt64ToStr(RoundInt64(GetFileCredits * 100), 12), ' ', 94));
end;

function TevAchFile.GetTextFile: IisStringList;
var
  ACHDataset: TevAchDataSet;
begin
  ACHDataset := TevAchDataSet.Create(nil);
  try
    PopulateReportDataset(ACHDataset);
    Result := ACHDataset.GetAchTextFileStrings;
  finally
    ACHDataset.Free;
  end;
end;

function TevAchFile.GetAchFiles(const TempString: String): IisListOfValues;
var
  Batch, Batches, SaveBatches: IisListOfValues;
  i: Integer;
  DS: TevClientDataset;
  EffectiveDate: TDateTime;
  MoreThanOneDate: Boolean;
begin
  Result := TisListOfValues.Create;
  if FBatches.Count > 0 then
  begin
    if not FOptions.EffectiveDate then
      Result.AddValue(TempString + '.ach', GetTextFile)
    else
    begin
      MoreThanOneDate := False;
      Batch := GetBatchByIndex(0);
      EffectiveDate := Batch.Value['EffectiveDate'];
      for i := 1 to BatchCount - 1 do
      begin
        Batch := GetBatchByIndex(i);
        if EffectiveDate <> Batch.Value['EffectiveDate'] then
        begin
          MoreThanOneDate := True;
          break;
        end;
      end;

      if not MoreThanOneDate then
        Result.AddValue(TempString + '.ach', GetTextFile)
      else
      begin
        DS := TevClientDataset.Create(nil);
        try
          DS.FieldDefs.Add('EFFECTIVE_DATE', ftDate);
          DS.CreateDataset;
          DS.Open;

          for i := 0 to BatchCount - 1 do
          begin
            Batch := GetBatchByIndex(i);
            EffectiveDate := Batch.Value['EffectiveDate'];
            if not DS.Locate('EFFECTIVE_DATE', EffectiveDate, []) then
            begin
              DS.Insert;
              DS.FieldByName('EFFECTIVE_DATE').AsDateTime := EffectiveDate;
              DS.Post;
            end;
          end;

          DS.IndexFieldNames := 'EFFECTIVE_DATE';
          DS.First;

          while not DS.Eof do
          begin
            Batches := TisListOfValues.Create;
            for i := 0 to BatchCount - 1 do
            begin
              Batch := GetBatchByIndex(i);
              EffectiveDate := Batch.Value['EffectiveDate'];
              if DS.FieldByName('EFFECTIVE_DATE').AsDateTime = EffectiveDate then
                Batches.AddValue(FBatches.Values[i].Name, Batch);
            end;
            SaveBatches := FBatches;
            FBatches := Batches;
            Result.AddValue(TempString + '.' + FormatDateTime('yyyymmdd', DS.FieldByName('EFFECTIVE_DATE').AsDateTime) + '.ach', GetTextFile);
            FBatches := SaveBatches;
            DS.Next;
          end;
          FBatches := Batches;
        finally
          DS.Free;
        end;
      end;  
    end;
  end;
end;

procedure TevAchFile.SortBatches;
var
  Batch, Batches: IisListOfValues;
  i, j: Integer;
  DS: TevClientDataset;
  Comments, Name: String;
  Transactions, STransactions, Item: IisListOfValues;
begin
  DS := TevClientDataset.Create(nil);
  try
    DS.FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 20);
    DS.FieldDefs.Add('GROUP', ftInteger);
    DS.FieldDefs.Add('ORDER', ftInteger);
    DS.FieldDefs.Add('BATCH_NAME', ftString, 500);
    DS.CreateDataset;
    DS.Open;

    for i := 0 to BatchCount - 1 do
    begin
      Batch := GetBatchByIndex(i);
      DS.Insert;
      DS.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := Batch.Value['CompanyNumber'];
      Comments := Batch.Value['Comments'];
      if Comments = PayrollComment then
        DS.FieldByName('GROUP').AsInteger := 1
      else if Comments = 'CHILD SUPP' then
        DS.FieldByName('GROUP').AsInteger := 2
      else if Comments = 'ACHOFFSET' then
        DS.FieldByName('GROUP').AsInteger := 1000 // should be at the end
      else
        DS.FieldByName('GROUP').AsInteger := 3; // impounds
      DS.FieldByName('ORDER').AsInteger := i;
      DS.FieldByName('BATCH_NAME').AsString := FBatches[i].Name;
      DS.Post;
    end;

    Batches := TisListOfValues.Create;
    DS.IndexFieldNames := 'CUSTOM_COMPANY_NUMBER;GROUP;ORDER';
    DS.First;

    while not DS.Eof do
    begin
      Batch := IInterface(FBatches.Value[DS.FieldByName('BATCH_NAME').AsString]) as IisListOfValues;
      Assert(Assigned(Batch));
      Batches.AddValue(DS.FieldByName('BATCH_NAME').AsString, Batch);
      DS.Next;
    end;
    FBatches := Batches;
  finally
    DS.Free;
  end;
  // put direct deposits first
  for i := 0 to FBatches.Count - 1 do
  begin
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    STransactions := TisListOfValues.Create;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      Name := Transactions[j].Name;
      if not Item.Value['IsImpound'] then
        STransactions.AddValue(Name, Item);
    end;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      Name := Transactions[j].Name;
      if Item.Value['IsImpound'] then
        STransactions.AddValue(Name, Item);
    end;
    Transactions.Clear;
    for j := 0 to STransactions.Count - 1 do
    begin
      Item := IInterface(STransactions[j].Value) as IisListOfValues;
      Name := STransactions[j].Name;
      Transactions.AddValue(Name, Item);
    end;
  end;

end;

procedure TevAchFile.PopulateReportDataset(Dataset: TevClientDataset);
var
  i, j, n: Integer;
  Batch, Trans: IisListOfValues;
begin

  SortBatches;

  if Trim(FOptions.Header) <> '' then
  begin
    Dataset.Append;

    Dataset.FieldByName('CL_NBR').AsInteger               := 0;
    Dataset.FieldByName('CO_NBR').AsInteger               := 0;
    Dataset.FieldByName('PR_NBR').AsInteger               := 0;
    Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := FSbBankAccountNbr;

    Dataset.FieldByName('ACH_TYPE').AsString := ACH_FILE_HEADER_STRING;
    Dataset.FieldByName('ACH_LINE').AsString := FOptions.Header;
    Dataset.FieldByName('SEGMENT_NBR').AsInteger := 0;
    Dataset.Post;
  end;

  Dataset.Append;

  Dataset.FieldByName('CL_NBR').AsInteger               := 0;
  Dataset.FieldByName('CO_NBR').AsInteger               := 0;
  Dataset.FieldByName('PR_NBR').AsInteger               := 0;
  Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := 0;
  Dataset.FieldByName('SEGMENT_NBR').AsInteger := 0;

  Dataset.FieldByName('ACH_TYPE').AsString := ACH_FILE_HEADER_STRING;
  Dataset.FieldByName('ACH_LINE').AsString := GetFileHeaderLine;
  Dataset.Post;

  for i := 0 to BatchCount - 1 do
  begin
    Batch := GetBatchByIndex(i);

    Dataset.Append;
    Dataset.FieldByName('CL_NBR').AsInteger               := Batch.Value['CL_NBR'];
    Dataset.FieldByName('CO_NBR').AsInteger               := Batch.Value['CO_NBR'];
    Dataset.FieldByName('PR_NBR').AsInteger               := Batch.Value['PR_NBR'];
    Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := 0;

    Dataset.FieldByName('ACH_TYPE').AsString              := ACH_BATCH_HEADER_STRING;
    Dataset.FieldByName('NAME').AsString                  := Batch.Value['NAME'];
    Dataset.FieldByName('COMMENTS').AsString              := Batch.Value['Comments'];
    Dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := Batch.Value['CompanyNumber'];
    Dataset.FieldByName('CHECK_DATE').AsDateTime          := Batch.Value['CheckDate'];
    Dataset.FieldByName('RUN_NUMBER').AsInteger           := Batch.Value['RunNumber'];
    Dataset.FieldByName('EFFECTIVE_DATE').AsDateTime      := Batch.Value['EffectiveDate'];
    Dataset.FieldByName('ENTRY_CLASS_CODE').AsString      := Batch.Value['ClassCode'];
    Dataset.FieldByName('SEGMENT_NBR').AsInteger := 1;
    Dataset.FieldByName('ACH_LINE').AsString              := GetHeaderLine(i);

    if Batch.Value['Comments'] = 'ACHOFFSET' then
      Dataset.FieldByName('HIDE_FROM_REPORT').Value        := GROUP_BOX_YES;

    Dataset.Post;

    for j := 0 to TransactionCount(Batch) - 1 do
    begin
      Trans := GetTransaction(Batch, j);
      Dataset.Append;
      Dataset.FieldByName('SEGMENT_NBR').AsInteger := 1;
      Dataset.FieldByName('CL_NBR').AsInteger               := Trans.Value['CL_NBR'];
      Dataset.FieldByName('CO_NBR').AsInteger               := Trans.Value['CO_NBR'];
      Dataset.FieldByName('PR_NBR').AsInteger               := Trans.Value['PR_NBR'];
      Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := Trans.Value['BANK_ACCOUNT_NBR'];

      Dataset.FieldByName('ACH_TYPE').AsString              := ACH_ENTRY_DETAIL_STRING;

      if FForNonAchReport then
        Dataset.FieldByName('COMMENTS').AsString := GetCommentForNonAchTransaction(Trans.Value['PaymentType'])
      else
        Dataset.FieldByName('COMMENTS').AsString := Trans.Value['TransactionType'];

      Dataset.FieldByName('IN_PRENOTE').AsString             := Trans.Value['IN_PRENOTE'];

      if FForNonAchReport then
      begin
        Dataset.FieldByName('AMOUNT').AsCurrency             := -Trans.Value['AMOUNT'];
        Dataset.FieldByName('CHECK_DATE').AsDateTime         := Batch.Value['CheckDate'];
        Dataset.FieldByName('RUN_NUMBER').AsInteger          := Batch.Value['RunNumber'];
        Dataset.FieldByName('EFFECTIVE_DATE').AsDateTime      := Batch.Value['EffectiveDate'];
      end
      else
        Dataset.FieldByName('AMOUNT').AsCurrency             := Trans.Value['AMOUNT'];

      Dataset.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString := Trans.Value['IDENTIFICATION_NUMBER'];
      Dataset.FieldByName('NAME').AsString                   := Trans.Value['NAME'];
      Dataset.FieldByName('BANK_ACCOUNT_NUMBER').AsString    := Trans.Value['BANK_ACCOUNT_NUMBER'];
      Dataset.FieldByName('ABA_NUMBER').AsString             := Trans.Value['ABA_NUMBER'];
      Dataset.FieldByName('CR_AFTER_LINE').AsString          := GROUP_BOX_NO;

      if Trans.Value['ACH_OFFSET'] <> 0 then
      begin
        Dataset.FieldByName('HIDE_FROM_REPORT').AsString     := GROUP_BOX_YES;
        Dataset.FieldByName('ACH_OFFSET').AsInteger          := Trans.Value['ACH_OFFSET'];
        Dataset.FieldByName('BATCH_COMMENTS').AsString       := 'ACHOFFSET';
      end
      else
      begin
        Dataset.FieldByName('HIDE_FROM_REPORT').AsString     := GROUP_BOX_NO;
        Dataset.FieldByName('BATCH_COMMENTS').AsString       := Trans.Value['TransactionType'];
      end;

      Dataset.FieldByName('TRAN').ASString                   := Trans.Value['TRANSACTION_CODE'];
                                                     
      Dataset.FieldByName('ACH_LINE').AsString               := GetDetailLine(Batch, Trans);
      Dataset.Post;

      if Trans.TryGetValue('Addenda', '') <> '' then
      begin
        Dataset.Append;
        Dataset.FieldByName('SEGMENT_NBR').AsInteger := 1;        
        Dataset.FieldByName('CL_NBR').AsInteger               := Trans.Value['CL_NBR'];
        Dataset.FieldByName('CO_NBR').AsInteger               := Trans.Value['CO_NBR'];
        Dataset.FieldByName('PR_NBR').AsInteger               := Trans.Value['PR_NBR'];
        Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := FSbBankAccountNbr;
        Dataset.FieldByName('ACH_TYPE').AsString              := ACH_ADDENDA_RECORD_STRING;
        Dataset.FieldByName('ACH_LINE').AsString              := GetAddendaLine(Batch, Trans);
        Dataset.FieldByName('HIDE_FROM_REPORT').AsString      := GROUP_BOX_YES;
        Dataset.Post;
      end;
    end;
    Dataset.Append;
    
    Dataset.FieldByName('SEGMENT_NBR').AsInteger := 1;
    Dataset.FieldByName('CL_NBR').AsInteger               := Trans.Value['CL_NBR'];
    Dataset.FieldByName('CO_NBR').AsInteger               := Trans.Value['CO_NBR'];
    Dataset.FieldByName('PR_NBR').AsInteger               := Trans.Value['PR_NBR'];
    Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := FSbBankAccountNbr;
    Dataset.FieldByName('ACH_TYPE').AsString              := ACH_BATCH_TOTAL_STRING;
    Dataset.FieldByName('DEBITS').AsCurrency              := GetBatchDebits(Batch);
    Dataset.FieldByName('CREDITS').AsCurrency             := GetBatchCredits(Batch);
    Dataset.FieldByName('ACH_LINE').AsString              := GetBatchControlLine(i);
    Dataset.FieldByName('HIDE_FROM_REPORT').AsString      := GROUP_BOX_YES;

    Dataset.Post;
  end;

  Dataset.Filter := 'ACH_TYPE = ''6:*'' or ACH_TYPE = ''7:*''';
  Dataset.Filtered := True;
  Dataset.First;

  Dataset.Filter := '';
  Dataset.Filtered := False;
  Dataset.First;

  Dataset.Append;
  Dataset.FieldByName('SEGMENT_NBR').AsInteger := 2;
  Dataset.FieldByName('CL_NBR').AsInteger               := 0;
  Dataset.FieldByName('CO_NBR').AsInteger               := 0;
  Dataset.FieldByName('PR_NBR').AsInteger               := 0;
  Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := 0;

  Dataset.FieldByName('ACH_TYPE').AsString            := ACH_FILE_TOTAL_STRING;
  Dataset.FieldByName('BATCH_NBR').Value              := BatchCount;
  Dataset.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value := GetFileEntryCount;
  Dataset.FieldByName('BANK_ACCOUNT_NUMBER').Value    := ZeroFillInt64ToStr(GetFileHash, 10);

  Dataset.FieldByName('DEBITS').Value := GetFileDebits;
  Dataset.FieldByName('CREDITS').Value := GetFileCredits;
  Dataset.FieldByName('ACH_LINE').Value := GetFileControlLine;
  Dataset.Post;

  n := (((GetFileLineCount + 9) div 10) * 10) - GetFileLineCount;

  for i := 1 to n do
  begin
    Dataset.Append;
    Dataset.FieldByName('SEGMENT_NBR').AsInteger := 3;
    Dataset.FieldByName('CL_NBR').AsInteger               := 0;
    Dataset.FieldByName('CO_NBR').AsInteger               := 0;
    Dataset.FieldByName('PR_NBR').AsInteger               := 0;
    Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := 0;

    Dataset.FieldByName('CL_NBR').Value := MaxInt;
    Dataset.FieldByName('ACH_TYPE').Value := '99:End-Of-File';
    Dataset.FieldByName('ORDER_NBR').Value := Dataset.RecordCount;
    Dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := 'zzzzzzzzzzzzzzzzzzzz';
    if not FInTesting or (i <> 1) then
      Dataset.FieldByName('ACH_LINE').Value := sDummy
    else
      Dataset.FieldByName('ACH_LINE').Value := '9 TEST '+ copy(sDummy,8,1000);

    Dataset.Post;
  end;

  if Trim(FOptions.Footer) <> '' then
  begin
    Dataset.Append;
    Dataset.FieldByName('SEGMENT_NBR').AsInteger := 4;
    Dataset.FieldByName('CL_NBR').AsInteger               := 0;
    Dataset.FieldByName('CO_NBR').AsInteger               := 0;
    Dataset.FieldByName('PR_NBR').AsInteger               := 0;
    Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := 0;

    Dataset.FieldByName('CL_NBR').Value := MaxInt;
    Dataset.FieldByName('ACH_TYPE').Value := '99:End-Of-File';
    Dataset.FieldByName('ORDER_NBR').Value := MaxInt;
    Dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := 'zzzzzzzzzzzzzzzzzzzz';
    Dataset.FieldByName('ACH_LINE').AsString := FOptions.Footer;
    Dataset.Post;
  end;

end;

procedure TevAchFile.SetDatasetParams(const CL_NBR, CO_NBR: Integer);
begin
  FCL_NBR := CL_NBR;
  FCO_NBR := CO_NBR;
end;

function TevAchFile.GetPRINT_CLIENT_NAME: String;
begin
  Result := FPRINT_CLIENT_NAME;
end;

procedure TevAchFile.SetPRINT_CLIENT_NAME(const Value: String);
begin
  FPRINT_CLIENT_NAME := Value;
end;

function TevAchFile.GetBANK_ACCOUNT_REGISTER_NAME: String;
begin
  Result := FBANK_ACCOUNT_REGISTER_NAME;
end;

procedure TevAchFile.SetBANK_ACCOUNT_REGISTER_NAME(const Value: String);
begin
  FBANK_ACCOUNT_REGISTER_NAME := Value;
end;

function TevAchFile.GetCL_NBR: Integer;
begin
  Result := FCL_NBR;
end;

function TevAchFile.GetCLIENT_NAME: String;
begin
  Result := FCLIENT_NAME;
end;

function TevAchFile.GetCO_NBR: Integer;
begin
  Result := FCO_NBR;
end;

function TevAchFile.GetCOMPANY_IDENTIFICATION: String;
begin
  Result := FCOMPANY_IDENTIFICATION;
end;

function TevAchFile.GetCOMPANY_NAME: String;
begin
  Result := FCOMPANY_NAME;
end;

function TevAchFile.GetCUSTOM_COMPANY_NUMBER: String;
begin
  Result := FCUSTOM_COMPANY_NUMBER;
end;

function TevAchFile.GetLEGAL_NAME: String;
begin
  Result := FLEGAL_NAME;
end;

function TevAchFile.GetWELLS_FARGO_ACH_FLAG: String;
begin
  Result := FWELLS_FARGO_ACH_FLAG;
end;

procedure TevAchFile.SetCL_NBR(const Value: Integer);
begin
  FCL_NBR := Value;
end;

procedure TevAchFile.SetCLIENT_NAME(const Value: String);
begin
  FCLIENT_NAME := Value;
end;

procedure TevAchFile.SetCO_NBR(const Value: Integer);
begin
  FCO_NBR := Value;
end;

procedure TevAchFile.SetCOMPANY_IDENTIFICATION(const Value: String);
begin
  FCOMPANY_IDENTIFICATION := Value;
end;

procedure TevAchFile.SetCOMPANY_NAME(const Value: String);
begin
  FCOMPANY_NAME := Value;
end;

procedure TevAchFile.SetCUSTOM_COMPANY_NUMBER(const Value: String);
begin
  FCUSTOM_COMPANY_NUMBER := Value;
end;

procedure TevAchFile.SetLEGAL_NAME(const Value: String);
begin
  FLEGAL_NAME := Value;
end;

procedure TevAchFile.SetWELLS_FARGO_ACH_FLAG(const Value: String);
begin
  FWELLS_FARGO_ACH_FLAG := Value;
end;

function TevAchFile.BatchesBalanced: Boolean;
var
  Batch, Transactions, Item: IisListOfValues;
  i, j: Integer;
  Amount: Currency;
begin
  Result := True;
  for i := 0 to FBatches.Count - 1 do
  begin
    Amount := 0;
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      Amount := Amount + Item.Value['AMOUNT'];
    end;
    if Abs(Amount) > 0.005 then
    begin
      Result := False;
      break;
    end;
  end;
end;

function TevAchFile.UnbalancedBatches: IisListOfValues;
var
  Batch, Transactions, Item: IisListOfValues;
  i, j: Integer;
  Amount: Currency;
begin
  Result := TisListOfValues.Create;
  for i := 0 to FBatches.Count - 1 do
  begin
    Amount := 0;
    Batch := IInterface(FBatches.Values[i].Value) as IisListOfValues;
    Transactions := IInterface(Batch.Value['Transactions']) as IisListOfValues;
    for j := 0 to Transactions.Count - 1 do
    begin
      Item := IInterface(Transactions[j].Value) as IisListOfValues;
      Amount := Amount + Item.Value['AMOUNT'];
    end;
    if Abs(Amount) > 0.005 then
      Result.AddValue(IntToStr(Result.Count-1), Batch);
  end;
end;

function TevAchFile.AllBatches: IisListOfValues;
begin
  Result := FBatches;
end;

function TevAchFile.GetInTesting: Boolean;
begin
  Result := FInTesting;
end;

procedure TevAchFile.SetInTesting(const Value: Boolean);
begin
  FInTesting := Value;
end;

function TevAchFile.GetFEIN: String;
begin
  Result := FFEIN;
end;

function TevAchFile.GetSB_EIN: String;
begin
  Result := FSB_EIN;
end;

procedure TevAchFile.SetFEIN(const Value: String);
begin
  FFEIN := Value;
end;

procedure TevAchFile.SetSB_EIN(const Value: String);
begin
  FSB_EIN := Value;
end;

function TevAchFile.GetCOID: String;
begin
  Result := FCOID;
end;

procedure TevAchFile.SetCOID(const Value: String);
begin
  FCOID := Value;
end;

{ TevTransaction }

constructor TevTransaction.Create(Params: IisListOfValues);
begin
  inherited Create;
  FPayrollType := 'R';
  FAmount := Params.Value['AMOUNT'];
  FBANK_ACCOUNT_NUMBER := Params.Value['BANK_ACCOUNT_NUMBER'];
  FABA_NUMBER := Params.Value['ABA_NUMBER'];
  FBANK_ACCOUNT_TYPE := Params.Value['BANK_ACCOUNT_TYPE'];
  FNAME := Params.Value['NAME'];
  if Params.ValueExists('CUSTOM_EMPLOYEE_NUMBER') then
    FCUSTOM_EMPLOYEE_NUMBER := Params.Value['CUSTOM_EMPLOYEE_NUMBER'];
  if Params.ValueExists('IN_PRENOTE') then
    FIN_PRENOTE := Params.Value['IN_PRENOTE'];
  if Params.ValueExists('Addenda') then
    FAddenda := Params.Value['Addenda'];
  if Params.ValueExists('BANK_ACCOUNT_NBR') then
    FBANK_ACCOUNT_NBR := Params.Value['BANK_ACCOUNT_NBR'];
end;

constructor TevTransaction.Create;
begin
  inherited Create;
  FAmount := 0;
  FBANK_ACCOUNT_NBR := 0;
  FPayrollType := 'R';
end;

function TevTransaction.GetABA_NUMBER: String;
begin
  Result := FABA_NUMBER;
end;

function TevTransaction.GetAddenda: String;
begin
  Result := FAddenda;
end;

function TevTransaction.GetAMOUNT: Currency;
begin
  Result := FAmount;
end;

function TevTransaction.GetBANK_ACCOUNT_NUMBER: String;
begin
  Result := FBANK_ACCOUNT_NUMBER;
end;

function TevTransaction.GetBANK_ACCOUNT_TYPE: String;
begin
  Result := FBANK_ACCOUNT_TYPE;
end;

function TevTransaction.GetCHECK_DATE: TDateTime;
begin
  Result := FCHECK_DATE;
end;

function TevTransaction.GetCUSTOM_EMPLOYEE_NUMBER: String;
begin
  Result := FCUSTOM_EMPLOYEE_NUMBER;
end;

function TevTransaction.GetSSN: String;
begin
  Result := FSSN;
end;

procedure TevTransaction.SetSSN(const Value: String);
begin
  FSSN := Value;
end;

function TevTransaction.GetEFFECTIVE_DATE: TDateTime;
begin
  Result := FEFFECTIVE_DATE;
end;

function TevTransaction.GetIN_PRENOTE: String;
begin
  Result := FIN_PRENOTE;
end;

function TevTransaction.GetNAME: String;
begin
  Result := FNAME;
end;

function TevTransaction.GetTRANSACTION_TYPE: TevTransactionType;
begin
  Result := FTRANSACTION_TYPE;
end;

procedure TevTransaction.ReadFromStream(const AStream: IisStream);
begin
  inherited;
  FAmount                 := AStream.ReadCurrency;
  FBANK_ACCOUNT_NUMBER    := AStream.ReadString;
  FABA_NUMBER             := AStream.ReadString;
  FBANK_ACCOUNT_TYPE      := AStream.ReadString;
  FNAME                   := AStream.ReadString;
  FCUSTOM_EMPLOYEE_NUMBER := AStream.ReadString;
  FIN_PRENOTE             := AStream.ReadString;
  FAddenda                := AStream.ReadString;
  FCHECK_DATE             := AStream.ReadDouble;
  FRUN_NUMBER             := AStream.ReadInteger;
  FEFFECTIVE_DATE         := AStream.ReadDouble;
  FTRANSACTION_TYPE       := TevTransactionType(AStream.ReadInteger);
  FIMPOUND_TYPE           := AStream.ReadString;
  FPR_NBR                 := AStream.ReadInteger;
  FBANK_ACCOUNT_NBR       := AStream.ReadInteger;
  FUniqueID               := AStream.ReadString;
  FSSN                    := AStream.ReadString;
  FCL_NBR                 := AStream.ReadInteger;
  FPayrollType            := AStream.ReadString;
  FSTATE                  := AStream.ReadString;
  FHSA                    := AStream.ReadString;
end;

procedure TevTransaction.SetABA_NUMBER(const Value: String);
begin
  FABA_NUMBER := Value;
end;

procedure TevTransaction.SetAddenda(const Value: String);
begin
  FAddenda := Value;
end;

procedure TevTransaction.SetAmount(const Value: Currency);
begin
  FAmount := Value;
end;

procedure TevTransaction.SetBANK_ACCOUNT_NUMBER(const Value: String);
begin
  FBANK_ACCOUNT_NUMBER := Value;
end;

procedure TevTransaction.SetBANK_ACCOUNT_TYPE(const Value: String);
begin
  FBANK_ACCOUNT_TYPE := Value;
end;

procedure TevTransaction.SetCHECK_DATE(const Value: TDateTime);
begin
  FCHECK_DATE := Value;
end;

procedure TevTransaction.SetCUSTOM_EMPLOYEE_NUMBER(const Value: String);
begin
  FCUSTOM_EMPLOYEE_NUMBER := Value;
end;

procedure TevTransaction.SetEFFECTIVE_DATE(const Value: TDateTime);
begin
  FEFFECTIVE_DATE := Value;
end;

procedure TevTransaction.SetTRANSACTION_TYPE(const Value: TevTransactionType);
begin
  FTRANSACTION_TYPE := Value;
end;

procedure TevTransaction.SetIN_PRENOTE(const Value: String);
begin
  FIN_PRENOTE := Value;
end;

procedure TevTransaction.SetNAME(const Value: String);
begin
  FNAME := Value;
end;

procedure TevTransaction.WriteToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteCurrency(FAmount);
  AStream.WriteString(FBANK_ACCOUNT_NUMBER);
  AStream.WriteString(FABA_NUMBER);
  AStream.WriteString(FBANK_ACCOUNT_TYPE);
  AStream.WriteString(FNAME);
  AStream.WriteString(FCUSTOM_EMPLOYEE_NUMBER);
  AStream.writeString(FIN_PRENOTE);
  AStream.WriteString(FAddenda);
  AStream.WriteDouble(FCHECK_DATE);
  AStream.WriteInteger(FRUN_NUMBER);
  AStream.WriteDouble(FEFFECTIVE_DATE);
  AStream.WriteInteger(Integer(FTRANSACTION_TYPE));
  AStream.WriteString(FIMPOUND_TYPE);
  AStream.WriteInteger(FPR_NBR);
  AStream.WriteInteger(FBANK_ACCOUNT_NBR);
  AStream.WriteString(FUniqueID);
  AStream.WriteString(FSSN);
  AStream.WriteInteger(FCL_NBR);
  AStream.WriteString(FPayrollType);
  AStream.WriteString(FSTATE);
  AStream.WriteString(FHSA);
end;

procedure TevTransaction.SetIMPOUND_TYPE(const Value: String);
begin
  FIMPOUND_TYPE := Value;
end;

procedure TevTransaction.SetRUN_NUMBER(const Value: Integer);
begin
  FRUN_NUMBER := Value;
end;

function TevTransaction.GetIMPOUND_TYPE: String;
begin
  Result := FIMPOUND_TYPE;
end;

function TevTransaction.GetRUN_NUMBER: Integer;
begin
  Result := FRUN_NUMBER;
end;

class function TevTransaction.GetTypeID: String;
begin
  Result := 'TevTransaction';
end;

procedure TevTransaction.SetPR_NBR(const Value: Integer);
begin
  FPR_NBR := Value;
end;

function TevTransaction.GetPR_NBR: Integer;
begin
  Result := FPR_NBR;
end;

procedure TevTransaction.SetCL_NBR(const Value: Integer);
begin
  FCL_NBR := Value;
end;

function TevTransaction.GetCL_NBR: Integer;
begin
  Result := FCL_NBR;
end;

function TevTransaction.GetSign: String;
begin
  if FAmount < -0.001 then
    Result := '-'
  else
    Result := '+';
end;

function TevTransaction.GetBatch: IisListOfValues;
begin
  Result := FBatch;
end;

procedure TevTransaction.SetBatch(const Value: IisListOfValues);
begin
  FBatch := Value;
end;

function TevTransaction.GetBANK_ACCOUNT_NBR: Integer;
begin
  Result := FBANK_ACCOUNT_NBR;
end;

procedure TevTransaction.SetBANK_ACCOUNT_NBR(const Value: Integer);
begin
  FBANK_ACCOUNT_NBR := Value;
end;

function TevTransaction.GetACH_ENTRY: IisListOfValues;
begin
  Result := FACH_ENTRY;
end;

procedure TevTransaction.SetACH_ENTRY(const Value: IisListOfValues);
begin
  FACH_ENTRY := Value;
end;

function TevTransaction.GetIsReversal: Boolean;
begin
  if (FAmount = 0)
  or (FPayrollType = PAYROLL_TYPE_TAX_ADJUSTMENT)
  or (FPayrollType = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT) then
    Result := False
  else
    case FTRANSACTION_TYPE of
    EMPLOYEE_DIRECT_DEPOSIT              : Result := FAmount < -0.005;
    CHILD_SUPPORT_PAYMENT                : Result := FAmount < -0.005;
    AGENCY_DIRECT_DEPOSIT                : Result := FAmount < -0.005;
    DIRECT_DEPOSIT_IMPOUND               : Result := FAmount > -0.005;
    CHILD_SUPPORT_IMPOUND                : Result := FAmount > -0.005;
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT : Result := FAmount < -0.005;
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  : Result := FAmount > -0.005;
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := FAmount < -0.005;
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := FAmount > -0.005;
    BILLING_IMPOUND_DEBIT_CLIENT         : Result := FAmount > -0.005;
    BILLING_IMPOUND_OFFSET_CREDIT        : Result := FAmount < -0.005;
    BILLING_IMPOUND_OFFSET_DEBIT         : Result := FAmount > -0.005;
    BILLING_IMPOUND_CREDIT_BUREAU        : Result := FAmount < -0.005;
    TRUST_IMPOUND_DEBIT_CLIENT           : Result := FAmount > -0.005;
    TRUST_IMPOUND_OFFSET_CREDIT          : Result := FAmount < -0.005;
    TRUST_IMPOUND_OFFSET_DEBIT           : Result := FAmount > -0.005;
    TRUST_IMPOUND_CREDIT_BUREAU          : Result := FAmount < -0.005;
    TAX_IMPOUND_DEBIT_CLIENT             : Result := FAmount > -0.005;
    TAX_IMPOUND_OFFSET_CREDIT            : Result := FAmount < -0.005;
    TAX_IMPOUND_OFFSET_DEBIT             : Result := FAmount > -0.005;
    TAX_IMPOUND_CREDIT_BUREAU            : Result := FAmount < -0.005;
    WC_IMPOUND_DEBIT_CLIENT              : Result := FAmount > -0.005;
    WC_IMPOUND_OFFSET_CREDIT             : Result := FAmount < -0.005;
    WC_IMPOUND_OFFSET_DEBIT              : Result := FAmount > -0.005;
    WC_IMPOUND_CREDIT_BUREAU             : Result := FAmount < -0.005;
    else Result := False;
    end;
end;

function TevTransaction.OffsetType: Integer;
begin
  if IsOffset then
  begin
    if FTRANSACTION_TYPE in [
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT] then
      Result := OFF_ONLY_EE_DD
    else
      Result := OFF_USE_OFFSETS
  end
  else
    Result := 0;
end;

function TevTransaction.TransactionCode: String;
begin
  if FBANK_ACCOUNT_TYPE = RECEIVING_ACCT_TYPE_SAVING then
    Result := '3'
  else
    Result := '2';

  if FTRANSACTION_TYPE = EFT_TAX_PAYMENT_DEBIT_ZERO then
    Result := Result + '9'
  else
  if FTRANSACTION_TYPE = EFT_TAX_PAYMENT_CREDIT_ZERO then
    Result := Result + '4'
  else
  if FIN_PRENOTE = 'Y' then
    Result := Result + '3'
  else
  if FAmount > 0.005 then
    Result := Result + '2'
  else
    Result := Result + '7';
end;


function TevTransaction.GetAsXML: String;
begin
  Result := '<TRAN>';
  Result := Result + '<TRANSACTION_TYPE>' + TypeDescr + '</TRANSACTION_TYPE>';
  Result := Result + '<IMPOUND_TYPE>' + FIMPOUND_TYPE + '</IMPOUND_TYPE>';
  Result := Result + '<AMOUNT>' + FloatToStr(FAmount) + '</AMOUNT>';
  Result := Result + '<BANK_ACCOUNT_NUMBER>' + FBANK_ACCOUNT_NUMBER + '</BANK_ACCOUNT_NUMBER>';
  Result := Result + '<ABA_NUMBER>' + FABA_NUMBER + '</ABA_NUMBER>';
  Result := Result + '<BANK_ACCOUNT_TYPE>' + FBANK_ACCOUNT_TYPE + '</AMOUNT>';
  Result := Result + '<NAME>' + FNAME + '</NAME>';
  Result := Result + '<CUSTOM_EMPLOYEE_NUMBER>' + FCUSTOM_EMPLOYEE_NUMBER + '</CUSTOM_EMPLOYEE_NUMBER>';
  Result := Result + '<IN_PRENOTE>' + FIN_PRENOTE + '</IN_PRENOTE>';
  Result := Result + '<ADDENDA>' + FAddenda + '</ADDENDA>';
  Result := Result + '<EFFECTIVE_DATE>' + DateToStr(FEFFECTIVE_DATE) + '</EFFECTIVE_DATE>';
  Result := Result + '<BANK_ACCOUNT_NBR>' + IntToStr(FBANK_ACCOUNT_NBR) + '</BANK_ACCOUNT_NBR>';
  Result := Result + '</TRAN>';
end;

function TevTransaction.GetCompanyDatas: String;
begin
  Result := FCompanyDatas;
end;

procedure TevTransaction.SetCompanyDatas(const Value: String);
begin
  FCompanyDatas := Value;
end;

function TevTransaction.GetUniqueID: String;
begin
  Result := FUniqueID;
end;

procedure TevTransaction.SetUniqueID(const Value: String);
begin
  FUniqueID := Value;
end;

function TevTransaction.IsImpound: Boolean;
begin
  Result := FTRANSACTION_TYPE in
 [DIRECT_DEPOSIT_IMPOUND,
  CHILD_SUPPORT_IMPOUND,
  BILLING_IMPOUND_DEBIT_CLIENT,
  BILLING_IMPOUND_CREDIT_BUREAU,
  TRUST_IMPOUND_DEBIT_CLIENT,
  TRUST_IMPOUND_CREDIT_BUREAU,
  TAX_IMPOUND_DEBIT_CLIENT,
  TAX_IMPOUND_CREDIT_BUREAU,
  WC_IMPOUND_DEBIT_CLIENT,
  WC_IMPOUND_CREDIT_BUREAU];
end;

function TevTransaction.IsOffset: Boolean;
begin
  Result := FTRANSACTION_TYPE in [
    DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
    DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
    CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT,
    BILLING_IMPOUND_OFFSET_CREDIT,
    BILLING_IMPOUND_OFFSET_DEBIT,
    TRUST_IMPOUND_OFFSET_CREDIT,
    TRUST_IMPOUND_OFFSET_DEBIT,
    TAX_IMPOUND_OFFSET_CREDIT,
    TAX_IMPOUND_OFFSET_DEBIT,
    WC_IMPOUND_OFFSET_CREDIT,
    WC_IMPOUND_OFFSET_DEBIT];
end;

function TevTransaction.IsDirectDeposit: Boolean;
begin
  Result := FTRANSACTION_TYPE in [EMPLOYEE_DIRECT_DEPOSIT, CHILD_SUPPORT_PAYMENT, AGENCY_DIRECT_DEPOSIT];
end;

function TevTransaction.IsDirDepOffset: Boolean;
begin
  Result := FTRANSACTION_TYPE in [DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT, DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT];
end;

function TevTransaction.IsDirDepImpound: Boolean;
begin
  Result := FTRANSACTION_TYPE in [DIRECT_DEPOSIT_IMPOUND, CHILD_SUPPORT_IMPOUND];
end;

function TevTransaction.BankAccRegisterType: String;
begin
  Result := '';
  case FTRANSACTION_TYPE of
  CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := BANK_REGISTER_TYPE_DirDepOffset;
  CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := BANK_REGISTER_TYPE_DirDepOffset;
  DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT : Result := BANK_REGISTER_TYPE_DirDepOffset;
  DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  : Result := BANK_REGISTER_TYPE_DirDepOffset;
  BILLING_IMPOUND_CREDIT_BUREAU        : Result := BANK_REGISTER_TYPE_BillingImpound;
  TRUST_IMPOUND_CREDIT_BUREAU          : Result := BANK_REGISTER_TYPE_TrustImpound;
  TAX_IMPOUND_CREDIT_BUREAU            : Result := BANK_REGISTER_TYPE_TaxImpound;
  WC_IMPOUND_CREDIT_BUREAU             : Result := BANK_REGISTER_TYPE_WCImpound;
  BILLING_IMPOUND_OFFSET_CREDIT        : Result := BANK_REGISTER_TYPE_BillingOffset;
  BILLING_IMPOUND_OFFSET_DEBIT         : Result := BANK_REGISTER_TYPE_BillingOffset;
  TRUST_IMPOUND_OFFSET_CREDIT          : Result := BANK_REGISTER_TYPE_TrustOffset;
  TRUST_IMPOUND_OFFSET_DEBIT           : Result := BANK_REGISTER_TYPE_TrustOffset;
  TAX_IMPOUND_OFFSET_CREDIT            : Result := BANK_REGISTER_TYPE_TaxOffset;
  TAX_IMPOUND_OFFSET_DEBIT             : Result := BANK_REGISTER_TYPE_TaxOffset;
  WC_IMPOUND_OFFSET_CREDIT             : Result := BANK_REGISTER_TYPE_WCOffset;
  WC_IMPOUND_OFFSET_DEBIT              : Result := BANK_REGISTER_TYPE_WCOffset;
  end;
end;

function TevTransaction.BlockedSb: Boolean;
begin
  Result := FTRANSACTION_TYPE in [
    BILLING_IMPOUND_OFFSET_CREDIT,
    BILLING_IMPOUND_OFFSET_DEBIT,
    BILLING_IMPOUND_CREDIT_BUREAU,
    TRUST_IMPOUND_OFFSET_CREDIT,
    TRUST_IMPOUND_OFFSET_DEBIT,
    TRUST_IMPOUND_CREDIT_BUREAU,
    TAX_IMPOUND_OFFSET_CREDIT,
    TAX_IMPOUND_OFFSET_DEBIT,
    TAX_IMPOUND_CREDIT_BUREAU,
    WC_IMPOUND_OFFSET_CREDIT,
    WC_IMPOUND_OFFSET_DEBIT,
    WC_IMPOUND_CREDIT_BUREAU
  ];
end;

function TevTransaction.BlockedCl: Boolean;
begin
  Result := (GetIsReversal or (GetAmount < 0))
            and
            (FTRANSACTION_TYPE in
               [DIRECT_DEPOSIT_IMPOUND,
                CHILD_SUPPORT_IMPOUND,
                BILLING_IMPOUND_DEBIT_CLIENT,
                TRUST_IMPOUND_DEBIT_CLIENT,
                TAX_IMPOUND_DEBIT_CLIENT,
                WC_IMPOUND_DEBIT_CLIENT,
                BILLING_IMPOUND_OFFSET_CREDIT,
                BILLING_IMPOUND_OFFSET_DEBIT,
                BILLING_IMPOUND_CREDIT_BUREAU,
                TRUST_IMPOUND_OFFSET_CREDIT,
                TRUST_IMPOUND_OFFSET_DEBIT,
                TRUST_IMPOUND_CREDIT_BUREAU,
                TAX_IMPOUND_OFFSET_CREDIT,
                TAX_IMPOUND_OFFSET_DEBIT,
                TAX_IMPOUND_CREDIT_BUREAU,
                WC_IMPOUND_OFFSET_CREDIT,
                WC_IMPOUND_OFFSET_DEBIT,
                WC_IMPOUND_CREDIT_BUREAU
               ])
end;

function TevTransaction.TypeDescr: String;
begin
  case FTRANSACTION_TYPE of
  EMPLOYEE_DIRECT_DEPOSIT              : Result := 'EE DIRECT DEPOSIT';
  CHILD_SUPPORT_PAYMENT                : Result := 'CHILD SUPPORT PAYMENT';
  EFT_TAX_PAYMENT_DEBIT_ZERO           : Result := 'EFTPAYMENT';
  EFT_TAX_PAYMENT_CREDIT_ZERO          : Result := 'EFTPAYMENT';
  AGENCY_DIRECT_DEPOSIT                : Result := 'AGENCY DIRECT DEPOSIT';
  DIRECT_DEPOSIT_IMPOUND               : Result := 'DIRECT DEPOSIT IMPOUND';
  CHILD_SUPPORT_IMPOUND                : Result := 'CHILD SUPPORT IMPOUND';
  DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT : Result := 'DD IMPOUND OFFSET CREDIT';
  DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT  : Result := 'DD IMPOUND OFFSET DEBIT';
  CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT  : Result := 'CHILD SUPP OFFSET DEBIT';
  CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT   : Result := 'CHILD SUPP OFFSET CREDIT';
  BILLING_IMPOUND_DEBIT_CLIENT         : Result := 'BILLING CLIENT DEBIT';
  BILLING_IMPOUND_OFFSET_CREDIT        : Result := 'BILLING OFFSET CREDIT';
  BILLING_IMPOUND_OFFSET_DEBIT         : Result := 'BILLING OFFSET DEBIT';
  BILLING_IMPOUND_CREDIT_BUREAU        : Result := 'BILLING BUREAU CREDIT';
  TRUST_IMPOUND_DEBIT_CLIENT           : Result := 'TRUST CLIENT DEBIT';
  TRUST_IMPOUND_OFFSET_CREDIT          : Result := 'TRUST OFFSET CREDIT';
  TRUST_IMPOUND_OFFSET_DEBIT           : Result := 'TRUST OFFSET DEBIT';
  TRUST_IMPOUND_CREDIT_BUREAU          : Result := 'TRUST BUREAU CREDIT';
  TAX_IMPOUND_DEBIT_CLIENT             : Result := 'TAX CLIENT DEBIT';
  TAX_IMPOUND_OFFSET_CREDIT            : Result := 'TAX OFFSET CREDIT';
  TAX_IMPOUND_OFFSET_DEBIT             : Result := 'TAX OFFSET DEBIT';
  TAX_IMPOUND_CREDIT_BUREAU            : Result := 'TAX BUREAU CREDIT';
  WC_IMPOUND_DEBIT_CLIENT              : Result := 'WC CLIENT DEBIT';
  WC_IMPOUND_OFFSET_CREDIT             : Result := 'WC OFFSET CREDIT';
  WC_IMPOUND_OFFSET_DEBIT              : Result := 'WC OFFSET DEBIT';
  WC_IMPOUND_CREDIT_BUREAU             : Result := 'WC BUREAU CREDIT';
  else Result := '';
  end;
end;

function TevTransaction.GetPayrollType: String;
begin
  Result := FPayrollType;
end;

procedure TevTransaction.SetPayrollType(const Value: String);
begin
  FPayrollType := Value;
end;

function TevTransaction.GetSTATE: String;
begin
  Result := FSTATE;
end;

procedure TevTransaction.SetSTATE(const Value: String);
begin
  FSTATE := Value;
end;

function TevTransaction.GetHSA: String;
begin
  Result := FHSA;
end;

procedure TevTransaction.SetHSA(const Value: String);
begin
  FHSA := Value;
end;

initialization

  ObjectFactory.Register([TevTransaction]);

end.
