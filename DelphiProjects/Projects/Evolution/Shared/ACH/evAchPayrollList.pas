unit evAchPayrollList;

interface

uses  DB, EvClientDataSet;

type
  TevAchPayrollList = class(TevClientDataSet)
  private
    function Get_CL_NBR: TField;
    function Get_CO_NBR: TField;
    function Get_CO_PR_ACH_NBR: TField;
    function Get_ACH_DATE: TField;
    function Get_WC_AMOUNT: TField;
    function Get_CHECK_DATE: TField;
    function Get_CUSTOM_CLIENT_NUMBER: TField;
    function Get_CUSTOM_COMPANY_NUMBER: TField;
    function Get_NAME: TField;
    function Get_NEW_STATUS: TField;
    function Get_PR_NBR: TField;
    function Get_PROCESS_DATE: TField;
    function Get_PR_TYPE: TField;
    function Get_PR_TYPE_DESCRIPTION: TField;
    function Get_RUN_NUMBER: TField;
    function Get_SELECTED: TField;
    function Get_SENT_DATE: TField;
    function Get_SENT_FILE: TField;
    function Get_SENT_USER: TField;
    function Get_STATUS: TField;
    function Get_BILLING_IMPOUND: TField;
    function Get_DD_IMPOUND: TField;
    function Get_TAX_IMPOUND: TField;
    function Get_TRUST_IMPOUND: TField;
    function Get_WC_IMPOUND: TField;
    function Get_COMPANY_FEIN: TField;
    function Get_CLIENT_NAME: TField;
    function Get_BANK_ACCOUNT_REGISTER_NAME: TField;
    function Get_LEGAL_NAME: TField;
  public
    procedure SortRecords;
    procedure CreateMyFields;
    property SELECTED: TField read Get_SELECTED;
    property CL_NBR: TField read Get_CL_NBR;
    property CO_NBR: TField read Get_CO_NBR;
    property CO_PR_ACH_NBR: TField read Get_CO_PR_ACH_NBR;
    property CHECK_DATE: TField read Get_CHECK_DATE;
    property CUSTOM_COMPANY_NUMBER: TField read Get_CUSTOM_COMPANY_NUMBER;
    property CUSTOM_CLIENT_NUMBER: TField read Get_CUSTOM_CLIENT_NUMBER;
    property NAME: TField read Get_NAME;
    property PR_NBR: TField read Get_PR_NBR;
    property RUN_NUMBER: TField read Get_RUN_NUMBER;
    property PROCESS_DATE: TField read Get_PROCESS_DATE;
    property ACH_DATE: TField read Get_ACH_DATE;
    property WC_AMOUNT: TField read Get_WC_AMOUNT;
    property STATUS: TField read Get_STATUS;
    property NEW_STATUS: TField read Get_NEW_STATUS;
    property SENT_DATE: TField read Get_SENT_DATE;
    property SENT_USER: TField read Get_SENT_USER;
    property SENT_FILE: TField read Get_SENT_FILE;
    property PR_TYPE: TField read Get_PR_TYPE;
    property PR_TYPE_DESCRIPTION: TField read Get_PR_TYPE_DESCRIPTION;
    property TRUST_IMPOUND: TField read Get_TRUST_IMPOUND;
    property TAX_IMPOUND: TField read Get_TAX_IMPOUND;
    property DD_IMPOUND: TField read Get_DD_IMPOUND;
    property BILLING_IMPOUND: TField read Get_BILLING_IMPOUND;
    property WC_IMPOUND: TField read Get_WC_IMPOUND;
    property COMPANY_FEIN: TField read Get_COMPANY_FEIN;
    property CLIENT_NAME: TField read Get_CLIENT_NAME;
    property LEGAL_NAME: TField read Get_LEGAL_NAME;
    property BANK_ACCOUNT_REGISTER_NAME: TField read Get_BANK_ACCOUNT_REGISTER_NAME;
  end;

implementation

{ TevPayrollList }

procedure TevAchPayrollList.CreateMyFields;
begin
  FieldDefs.Add('SELECTED', ftBoolean);
  FieldDefs.Add('CL_NBR', ftInteger);
  FieldDefs.Add('CO_NBR', ftInteger);
  FieldDefs.Add('CO_PR_ACH_NBR', ftInteger);
  FieldDefs.Add('CHECK_DATE', ftDateTime);
  FieldDefs.Add('PROCESS_DATE', ftDateTime);
  FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 20);
  FieldDefs.Add('CUSTOM_CLIENT_NUMBER', ftString, 20);
  FieldDefs.Add('NAME', ftString, 40);
  FieldDefs.Add('PR_NBR', ftInteger);
  FieldDefs.Add('RUN_NUMBER', ftInteger);
  FieldDefs.Add('ACH_DATE', ftDate);
  FieldDefs.Add('WC_AMOUNT', ftCurrency);
  FieldDefs.Add('STATUS', ftString, 1);
  FieldDefs.Add('NEW_STATUS', ftString, 1);
  FieldDefs.Add('SENT_DATE', ftDateTime);
  FieldDefs.Add('SENT_USER', ftString, 80);
  FieldDefs.Add('SENT_FILE', ftString, 80);
  FieldDefs.Add('PR_TYPE', ftString, 20);
  FieldDefs.Add('PR_TYPE_DESCRIPTION', ftString, 20);
  FieldDefs.Add('NEG_CHECKS', ftString, 1);
  FieldDefs.Add('TRUST', ftString, 1);
  FieldDefs.Add('TAX', ftString, 1);
  FieldDefs.Add('BILLING', ftString, 1);
  FieldDefs.Add('TRUST_IMPOUND', ftString, 1);
  FieldDefs.Add('TAX_IMPOUND', ftString, 1);
  FieldDefs.Add('DD_IMPOUND', ftString, 1);
  FieldDefs.Add('BILLING_IMPOUND', ftString, 1);
  FieldDefs.Add('WC_IMPOUND', ftString, 1);
  FieldDefs.Add('COMPANY_FEIN', ftString, 9);
  FieldDefs.Add('CLIENT_NAME', ftString, 40);
  FieldDefs.Add('BANK_ACCOUNT_REGISTER_NAME', ftString, 15);  
  FieldDefs.Add('LEGAL_NAME', ftString, 40);
  CreateDataSet;
  IndexFieldNames := 'CUSTOM_COMPANY_NUMBER;CL_NBR;CO_NBR;ACH_DATE;PR_NBR';
end;

function TevAchPayrollList.Get_ACH_DATE: TField;
begin
  Result := FieldByName('ACH_DATE');
end;

function TevAchPayrollList.Get_WC_AMOUNT: TField;
begin
  Result := FieldByName('WC_AMOUNT');
end;

function TevAchPayrollList.Get_BILLING_IMPOUND: TField;
begin
  Result := FieldByName('BILLING_IMPOUND');
end;

function TevAchPayrollList.Get_CHECK_DATE: TField;
begin
  Result := FieldByName('CHECK_DATE');
end;

function TevAchPayrollList.Get_CL_NBR: TField;
begin
  Result := FieldByName('CL_NBR');
end;

function TevAchPayrollList.Get_CO_NBR: TField;
begin
  Result := FieldByName('CO_NBR');
end;

function TevAchPayrollList.Get_CO_PR_ACH_NBR: TField;
begin
  Result := FieldByName('CO_PR_ACH_NBR');
end;

function TevAchPayrollList.Get_CUSTOM_CLIENT_NUMBER: TField;
begin
  Result := FieldByName('CUSTOM_CLIENT_NUMBER');
end;

function TevAchPayrollList.Get_CUSTOM_COMPANY_NUMBER: TField;
begin
  Result := FieldByName('CUSTOM_COMPANY_NUMBER');
end;

function TevAchPayrollList.Get_DD_IMPOUND: TField;
begin
  Result := FieldByName('DD_IMPOUND');
end;

function TevAchPayrollList.Get_NAME: TField;
begin
  Result := FieldByName('NAME');
end;

function TevAchPayrollList.Get_NEW_STATUS: TField;
begin
  Result := FieldByName('NEW_STATUS');
end;

function TevAchPayrollList.Get_PR_NBR: TField;
begin
  Result := FieldByName('PR_NBR');
end;

function TevAchPayrollList.Get_PR_TYPE: TField;
begin
  Result := FieldByName('PR_TYPE');
end;

function TevAchPayrollList.Get_PR_TYPE_DESCRIPTION: TField;
begin
  Result := FieldByName('PR_TYPE_DESCRIPTION');
end;

function TevAchPayrollList.Get_RUN_NUMBER: TField;
begin
  Result := FieldByName('RUN_NUMBER');
end;

function TevAchPayrollList.Get_SELECTED: TField;
begin
  Result := FieldByName('SELECTED');
end;

function TevAchPayrollList.Get_SENT_DATE: TField;
begin
  Result := FieldByName('SENT_DATE');
end;

function TevAchPayrollList.Get_SENT_FILE: TField;
begin
  Result := FieldByName('SENT_FILE');
end;

function TevAchPayrollList.Get_SENT_USER: TField;
begin
  Result := FieldByName('SENT_USER');
end;

function TevAchPayrollList.Get_STATUS: TField;
begin
  Result := FieldByName('STATUS');
end;

function TevAchPayrollList.Get_TAX_IMPOUND: TField;
begin
  Result := FieldByName('TAX_IMPOUND');
end;

function TevAchPayrollList.Get_TRUST_IMPOUND: TField;
begin
  Result := FieldByName('TRUST_IMPOUND');
end;

function TevAchPayrollList.Get_WC_IMPOUND: TField;
begin
  Result := FieldByName('WC_IMPOUND');
end;

procedure TevAchPayrollList.SortRecords;
begin
  if IndexDefs.IndexOf('SORT') >= 0 then
  begin
    IndexName := 'SORT';
    First;
  end;
end;

function TevAchPayrollList.Get_COMPANY_FEIN: TField;
begin
  Result := FieldByName('COMPANY_FEIN');
end;

function TevAchPayrollList.Get_CLIENT_NAME: TField;
begin
  Result := FieldByName('CLIENT_NAME');
end;

function TevAchPayrollList.Get_LEGAL_NAME: TField;
begin
  Result := FieldByName('LEGAL_NAME');
end;

function TevAchPayrollList.Get_BANK_ACCOUNT_REGISTER_NAME: TField;
begin
  Result := FieldByName('BANK_ACCOUNT_REGISTER_NAME');
end;

function TevAchPayrollList.Get_PROCESS_DATE: TField;
begin
  Result := FieldByName('PROCESS_DATE');
end;

end.
