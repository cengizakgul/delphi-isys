unit evCashProcessing;

interface

uses
  SysUtils, Classes, StrUtils, Db,   EvTypes, Variants, EvContext,
  SReportSettings, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvAchUtils, EvExceptions, IsBaseClasses, EvUtils,
  evAchPayrollList, SACHTypes, EvAchDataSet, EvCommonInterfaces, EvClientDataSet;

const
  bPositives = False;
  bNegatives = True;

type
  ELiabilityThresholdException = class(Exception);

type
  TevCashProcessing = class
  private
    v: Variant;
    FErrorMessages: String;
    FSkipList: IIsStringList;
    RunNumber: Integer;
    NumberOfDaysPrior: Integer;
    CheckDate: TDateTime;
    EffectiveDate: TDateTime;
    AchAmount: Currency;
    FOrderNbr: Integer;

    BillingImpoundSB_BANK_ACCOUNTS_NBR: Integer;
    WCImpoundSB_BANK_ACCOUNTS_NBR: Integer;
    TaxImpoundSB_BANK_ACCOUNTS_NBR: Integer;
    TrustImpoundSB_BANK_ACCOUNTS_NBR: Integer;

    ACHFile1: TevClientDataSet;
    DIR_DEP: TevClientDataSet;
    AGENCY_DIR_DEP: TevClientDataSet;
    cdsTaxDistr: TevClientDataSet;
    DBDTDirDep: TevClientDataSet;

    FClNbr: Integer;


    FTotalTaxLiability: Currency;
    FedTaxImpounded: Currency;
    PeriodFedTaxImpounded: Currency; 
    TaxCredits: Currency;

    FTaxCreditsDateFrom: TDateTime;
    FTaxCreditsDateInto: TDateTime;
    
    CheckDateDD, EffectiveDateDD: TDateTime;
    NumberOfDaysPriorDD: integer;

    SegmentNbr: Integer;
    PrNbr: Integer;

    FACHFiles: IisListOfValues;
    TempCredits: Currency;
    TempDebits: Currency;
    ORIGIN_BANK_ABA: String;
    AddendaNbr: Integer;
    EntryNbr: Integer;
    FFileName: String;
    FFileSaved: Boolean;
    strFillerStamp: String;
    FCombining: Boolean;
    FPreProcess: Boolean;
    FileHeaderProcessed: Boolean;
    FDetailAdded: Boolean;
    CO_PR_ACH_NBR: Integer;
    BatchUniqID: String;
    BatchBANK_REGISTER_TYPE: String;
    BatchSB_BANK_ACCOUNTS_NBR: Integer;
    FBatchCredits: Currency;
    FBatchDebits: Currency;
    FTotNbrOfBatches: Integer;
    ORIGIN_Name: String;
    SBID: String;
    FTotCredits: Currency;
    FTotDebits: Currency;
    FTotNbrOfEntries: Integer;
    FxTotNbrOfEntries: Integer;
    FSubNbrOfEntries: Integer;
    FSubEntryHash: Int64;
    FSubNbrOfBatches: Integer;
    FSubCredits: Currency;
    FSubDebits: Currency;
    DEST_Number: String;
    ORIGIN_Number: String;
    DEST_Name: String;
    FCoNbr: Integer;
    FBelowThresholdList: IisStringList;    
    function GetFEIN: String;
    function GetCheckDate: TDateTime;
    function CanProcessTrustImpound: Boolean;
    function CanProcessTaxImpound: Boolean;
    function GetAccountLevel: TevAccountLevel;
    procedure AppendDBDTAmount(iDiv, iBr, iDep, iTm: Variant;
      fAmount: Currency; Mode: String; const ChildSupport: Boolean = False);
    function IsResending: boolean;
    procedure GenerateAchReport(aACHFile, ShowAll: Boolean);
    procedure SetDivisionFilters;
    procedure ResetDivisionFilters;
    function ProcessWorkersComp: Boolean;
    function ProcessBilling: Currency;
    function ProcessTaxes(aNegative: Boolean): Currency;
    function ProcessTrustorObc: Currency;
    function ProcessDirectDeposits(aNegative: boolean): Currency;

    function DirectDeposits: Currency;

    function GetTotalTaxLiabilities: Currency;
    function TrustOrObcForAch: Currency;
    function BillingForAch: Currency;
    function IsBlockSBCredit: Boolean;
    function IsBlockDebits: Boolean;
    procedure StartBatchDefinition(aClassCode: string; aEffDate: TDateTime;
      aComments: string; BankRegisterType: string; SB_BANK_ACCOUNTS_NBR: Integer);
    procedure AddFileHeaderIfNotAddedYet;
    procedure WriteBatchHeader(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; bSBMode: Boolean = False; Prenote: Boolean = False;
      aFEIN: String = ''; bMNSUI: Boolean = False; bCTSUI: Boolean = False);
    procedure WriteBatchTotal(aHoldTotals: Boolean; aSegment: Integer; aOrder: Integer; CUSTOM_EMPLOYEE_NUMBER: string;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = True; aCRLF: Boolean = False);
    procedure AppendDetailRecord(CUSTOM_EMPLOYEE_NUMBER: string; AAmount: Currency; PAYMENT_TYPE, BANK_REGISTER_TYPE: String);
    procedure WriteDetail(cTranType: Char);
    procedure AppendAddendaRecord(CUSTOM_EMPLOYEE_NUMBER: String; AAmount: Currency);
    procedure ClearBatchTotals;
    procedure AddFileHeader;
    procedure IncrementSubTotals(bIsAch: Boolean);
    procedure AppendFileHeaderRecord;
  protected
    procedure DoOnConstruction;
  public
    Options: TACHOptions;
    PayrollList: TevAchPayrollList;
    dtFrom, dtTo: TDateTime;
    StatusFromIndex: Integer;
    StatusToIndex: Integer;
    BlockNegativeChecks: Boolean;
    BlockNegativeLiabilities: Boolean;
    BlockCompanyDD: Boolean;
    PayrollTypeIndex: Integer;
    SB_BANK_ACCOUNTS_NBR: Integer;
    ReversalACH: Boolean;
    OverrideBatchBANK_REGISTER_TYPE: string;
    ALL_PR_CHECKS: TevClientDataSet;
    cTaxCredits: Currency;
    FACHFile: TevAchDataSet;
    FSavingACH: Boolean;
    AgencyDirDep: IisListOfValues;
    ClBankAccounts: IevQuery;
    PrChecks: IevQuery;
    Threshold: Currency;
    CompanyTaxLiability: Currency;
    procedure ClearGrandAndBatchTotals;
    property PreProcess: Boolean write FPreProcess;
    property ACHFiles: IisListOfValues read FACHFiles;
    property FileName: String read FFileName;
    property SkipList: IisStringList read FSkipList;
    constructor Create;
    function ACHDescription: String;
    procedure ProcessOnePayroll(CurrentThresholdScope: String);
    procedure CheckThreshold(ccn, CurrentThresholdScope: String);
    procedure GenerateReport(aACHFile: Boolean; ShowAll: Boolean);
    procedure SetACHOptions(const NewACHOptions: IevACHOptions);
    function GetErrors: String;
    destructor Destroy; override;
  end;

implementation

uses
  evBasicUtils, DateUtils, EvConsts, SFieldCodeValues, SDataStructure,
  SDataDictclient, EvDataAccessComponents;

function TevCashProcessing.BillingForAch: Currency;
begin
  Result := 0;
  if not IsPayrollPeople or (RunNumber = 2) then
  begin
//        Billing Impound
//        Add up all amounts for all invoices for specific PR
//        Process if not zero.
    if (PayrollList.BILLING_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
    and (PayrollList.BILLING_IMPOUND.AsString <> PAYMENT_TYPE_NONE) then
    begin
      Result := ProcessBilling;
      if Abs(Result) > 0.005 then
      begin
        // Des says there should never be any negative billing amounts.
        if not IsResending then
        begin
          try
            DM_COMPANY.CO_BILLING_HISTORY.RetrieveCondition := 'PR_NBR = ' + IntToStr(PrNbr);
            DM_COMPANY.CO_BILLING_HISTORY.First;
            while not DM_COMPANY.CO_BILLING_HISTORY.Eof do
            begin
              DM_COMPANY.CO_BILLING_HISTORY.Edit;
              if PayrollList.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
                DM_COMPANY.CO_BILLING_HISTORY.STATUS.AsString := CO_BILLING_STATUS_INTERNAL_CHECK
              else
                DM_COMPANY.CO_BILLING_HISTORY.STATUS.AsString := CO_BILLING_STATUS_IGNORE;
              DM_COMPANY.CO_BILLING_HISTORY.Post;
              DM_COMPANY.CO_BILLING_HISTORY.Next;
            end;
          finally
            DM_COMPANY.CO_BILLING_HISTORY.RetrieveCondition := '';
          end;
        end;
      end;
      Inc(SegmentNbr);
    end;
  end;
end;

function TevCashProcessing.TrustOrObcForAch: Currency;
begin
  Result := 0;
  if not IsPayrollPeople or (RunNumber = 1) then
  begin
    if CanProcessTrustImpound then
    begin
      Result := ProcessTrustOrObc;
      Inc(SegmentNbr);
    end;
  end;
end;

function TevCashProcessing.DirectDeposits: Currency;
begin
  Result := 0;
  if not IsPayrollPeople or (RunNumber = 2) then
  begin
    if not BlockNegativeChecks then
    begin
      ProcessDirectDeposits(bNegatives);
      Inc(SegmentNbr);
    end;
    Result := ProcessDirectDeposits(bPositives);
    Inc(SegmentNbr);
  end;
end;

procedure TevCashProcessing.ResetDivisionFilters;
begin
  DM_COMPANY.CO_DIVISION.Filter := '';
  DM_COMPANY.CO_BRANCH.Filter := '';
  DM_COMPANY.CO_DEPARTMENT.Filter := '';
  DM_COMPANY.CO_TEAM.Filter := '';
  DM_COMPANY.CO_DIVISION.Filtered := False;
  DM_COMPANY.CO_BRANCH.Filtered := False;
  DM_COMPANY.CO_DEPARTMENT.Filtered := False;
  DM_COMPANY.CO_TEAM.Filtered := False;
end;

procedure TevCashProcessing.SetDivisionFilters;
begin
  DM_COMPANY.CO_DIVISION.Filter := 'TAX_CL_BANK_ACCOUNT_NBR is not null';
  DM_COMPANY.CO_BRANCH.Filter := 'TAX_CL_BANK_ACCOUNT_NBR is not null';
  DM_COMPANY.CO_DEPARTMENT.Filter := 'TAX_CL_BANK_ACCOUNT_NBR is not null';
  DM_COMPANY.CO_TEAM.Filter := 'TAX_CL_BANK_ACCOUNT_NBR is not null';
  DM_COMPANY.CO_DIVISION.Filtered := True;
  DM_COMPANY.CO_BRANCH.Filtered := True;
  DM_COMPANY.CO_DEPARTMENT.Filtered := True;
  DM_COMPANY.CO_TEAM.Filtered := True;
end;

//******************************************************************************
//                                             EMPLOYEE & AGENCY DIRECT DEPOSITS
//******************************************************************************

function TevCashProcessing.ProcessDirectDeposits(aNegative: boolean): Currency;
var
  bAnyProcessed: Boolean;
  sPPDComments, sPPDOffsetComments, sCustomEENumber: String;
  sCCDComments, sCCDOffsetComments: String;
  s, CustomEENumber: string;
  d: TDateTime;
  bCCD: Boolean;
  sMedIns, CustomCaseNumber: string;

  Totals, CSTotals, bAmount: Currency;

  function ConvertSSN(SSN: string): string;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Length(SSN) do
      if SSN[i] in ['0'..'9'] then
        Result := Result + SSN[i];
  end;
  
begin
  Totals := 0;
  CSTotals := 0;
  Result := 0;
  bAmount := 0;
  bAnyProcessed := False;

  if DBDTDirDep.Active then
    DBDTDirDep.Close;
  DBDTDirDep.CreateDataSet;

  if aNegative then
  begin
    DIR_DEP.Filter := 'amount < 0';
    DIR_DEP.Filtered := True;
    if ReversalACH then
    begin
      if Options.PayrollHeader then
      begin
        sPPDComments := 'PAYROLL';
        sCCDComments := 'PAYROLL';
      end
      else
      begin
        sPPDComments := 'NET=PAY';
        sCCDComments := 'NET=PAY';
      end;
      sPPDOffsetComments := 'ACHOFFSET';
      sCCDOffsetComments := 'ACHOFFSET';
    end
    else
    begin
      if Options.DescReversal then
      begin
        if Options.PayrollHeader then
        begin
          sPPDComments := 'REVERSAL-P';
          sCCDComments := 'REVERSAL-P';
          sPPDOffsetComments := 'REVERSAL+P';
          sCCDOffsetComments := 'REVERSAL+P';
        end
        else
        begin
          sPPDComments := 'REVERSAL-A';
          sCCDComments := 'REVERSAL-D';
          sPPDOffsetComments := 'REVERSAL+A';
          sCCDOffsetComments := 'REVERSAL+D';
        end;
      end
      else

      begin
        sPPDComments := 'REVERSAL';
        sCCDComments := 'REVERSAL';
        sPPDOffsetComments := 'REVERSAL';
        sCCDOffsetComments := 'REVERSAL';
      end;
    end;
  end
  else
  begin
    DIR_DEP.Filter := 'amount >= 0 or amount is null';
    DIR_DEP.Filtered := True;
    if ReversalACH then
    begin
      if Options.DescReversal then
      begin
        if Options.PayrollHeader then
        begin
          sPPDComments := 'REVERSAL-P';
          sCCDComments := 'REVERSAL-P';
          sPPDOffsetComments := 'REVERSAL+P';
          sCCDOffsetComments := 'REVERSAL+P';
        end
        else
        begin
          sPPDComments := 'REVERSAL-A';
          sCCDComments := 'REVERSAL-D';
          sPPDOffsetComments := 'REVERSAL+A';
          sCCDOffsetComments := 'REVERSAL+D';
        end;
      end
      else
      begin
        sPPDComments := 'REVERSAL';
        sCCDComments := 'REVERSAL';
        sPPDOffsetComments := 'REVERSAL';
        sCCDOffsetComments := 'REVERSAL';
      end;
    end
    else
    begin
      if Options.PayrollHeader then
      begin
        sPPDComments := 'PAYROLL';
        sCCDComments := 'PAYROLL';
      end
      else
      begin
        sPPDComments := 'NET=PAY';
        sCCDComments := 'NET=PAY';
      end;
      sPPDOffsetComments := 'ACHOFFSET';
      sCCDOffsetComments := 'ACHOFFSET';
    end;
  end;

  bCCD := False;
// Do Employees
  d := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
  if DIR_DEP.RecordCount > 0 then
  begin
    bAnyProcessed := True;
    FOrderNbr := FACHFile.RecordCount;
    DIR_DEP.First;

    if d < Date then
      d := Date;

    while ctx_PayrollCalculation.IsNonBusinessDay(d) do
      d := d + 1;

    if Options.Intercept and Options.BalanceBatches then
      d := EffectiveDateDD;

    if DIR_DEP.FieldByName('E_D_CODE_TYPE').Value = ED_DED_CHILD_SUPPORT then
    begin
      StartBatchDefinition('CCD', d, sCCDComments, '', 0);
      bCCD := True;
    end
    else
      StartBatchDefinition('PPD', d, sPPDComments, '', 0); // CHECKS have alredy been placed in Register

    NumberOfDaysPrior := NumberOfDaysPriorDD;
    CheckDate := CheckDateDD;
    EffectiveDate := EffectiveDateDD;
  end;

  while not DIR_DEP.EOF do //Custom View
  begin
    // ACH Client Data Set
    AddFileHeaderIfNotAddedYet;

    try
      if not bCCD and (DIR_DEP.FieldByName('E_D_CODE_TYPE').Value = ED_DED_CHILD_SUPPORT) then
      begin
        WriteBatchHeader(True, SegmentNbr, FOrderNbr);
        WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        Totals := Totals + TempCredits + TempDebits;
        FOrderNbr := FACHFile.RecordCount;
        StartBatchDefinition('CCD', d, sCCDComments, '', 0);
        bCCD := True;
      end
      else
      if bCCD and (DIR_DEP.FieldByName('E_D_CODE_TYPE').Value <> ED_DED_CHILD_SUPPORT) then
      begin
        WriteBatchHeader(True, SegmentNbr, FOrderNbr);
        WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        CSTotals := CSTotals + TempCredits + TempDebits;
        FOrderNbr := FACHFile.RecordCount;
        StartBatchDefinition('PPD', d, sPPDComments, '', 0);
        bCCD := False;
      end;

      if (DIR_DEP.FieldByName('ADDENDA').AsString = '') or Options.CTS then
        CustomEENumber := DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString
      else
        CustomEENumber := DIR_DEP.FieldByName('ADDENDA').AsString;

      if (DIR_DEP.FieldByName('IN_PRENOTE').AsString = 'Y') then
      begin
        DIR_DEP.Next;
        continue;
      end;

      if (DIR_DEP.FieldByName('IN_PRENOTE').AsString = 'Y') and
         (DM_SERVICE_BUREAU.SB.USE_PRENOTE.AsString = 'Y') then
      begin
        if aNegative or ReversalACH then
        begin
          DIR_DEP.Next;
          continue;
        end;
      end
      else
      begin
        if DIR_DEP.FieldByName('AMOUNT').AsCurrency = 0 then
        begin
          DIR_DEP.Next;
          continue;
        end;

        AppendDetailRecord(CustomEENumber, DIR_DEP.FieldByName('AMOUNT').Value, PayrollList.DD_IMPOUND.AsString, BANK_REGISTER_TYPE_DirDepOffset);

//        Result := Result + DIR_DEP.FieldByName('AMOUNT').Value;

        AchAmount := AchAmount + DIR_DEP.FieldByName('AMOUNT').Value;
        if (DIR_DEP['CHECK_TYPE'] = CHECK_TYPE2_MANUAL) and
           not DM_COMPANY.CO.MANUAL_CL_BANK_ACCOUNT_NBR.IsNull then
          AppendDBDTAmount(null, null, null, null,
                           DIR_DEP['AMOUNT'], 'MANUAL', DIR_DEP.FieldByName('E_D_CODE_TYPE').Value = ED_DED_CHILD_SUPPORT)
        else
          AppendDBDTAmount(DIR_DEP['CO_DIVISION_NBR'], DIR_DEP['CO_BRANCH_NBR'],
                           DIR_DEP['CO_DEPARTMENT_NBR'], DIR_DEP['CO_TEAM_NBR'],
                           DIR_DEP['AMOUNT'], 'DD', DIR_DEP.FieldByName('E_D_CODE_TYPE').Value = ED_DED_CHILD_SUPPORT);
      end;

      // Don't POST just yet,... non-common fields here...
      FACHFile.SetOrderNumber;

      // other fields here... for ACH FILE OUTPUT
      if DIR_DEP.FieldByName('MIDDLE_INITIAL').IsNull then
        FACHFile.NAME.Value := DIR_DEP.FieldByName('FIRST_NAME').Value + ' ' +
          DIR_DEP.FieldByName('LAST_NAME').Value
      else
        FACHFile.NAME.Value := DIR_DEP.FieldByName('FIRST_NAME').Value + ' ' +
          DIR_DEP.FieldByName('MIDDLE_INITIAL').Value + '. ' +
          DIR_DEP.FieldByName('LAST_NAME').Value;

      FACHFile.ABA_NUMBER.Value :=
        ACHTrim(DIR_DEP.FieldByName('EE_ABA_NUMBER').AsString);
      if FACHFile.ABA_NUMBER.AsString = '' then
        raise EInconsistentData.CreateHelp('Incorrect ABA number for EE ' + FACHFile.NAME.AsString + ', Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

      FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;

      if DIR_DEP.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
        FACHFile.BANK_ACCOUNT_NUMBER.Value := ACHTrim(DIR_DEP.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString)
      else
        FACHFile.BANK_ACCOUNT_NUMBER.Value := DIR_DEP.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString;
      if FACHFile.BANK_ACCOUNT_NUMBER.AsString = '' then
        raise EInconsistentData.CreateHelp('Incorrect Bank Account number for EE ' + FACHFile.NAME.AsString + ', Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

      if DIR_DEP.FieldByName('DEDUCT_WHOLE_CHECK').Value = 'Y' then
        FACHFile.COMMENTS.Value := FACHFile.COMMENTS.AsString + 'NET CHECK';

      FACHFile.TRAN.AsString := GetAchAccountType(DIR_DEP.FieldByName('EE_BANK_ACCOUNT_TYPE').AsString);

      if DIR_DEP.FieldByName('E_D_CODE_TYPE').Value <> ED_DED_CHILD_SUPPORT then
        WriteDetail(TRAN_TYPE_OUT)
      else
      begin
        Assert(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.Locate('EE_CHILD_SUPPORT_CASES_NBR', DIR_DEP['EE_CHILD_SUPPORT_CASES_NBR'], []));
        FACHFile.NAME.Value := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CL_AGENCY_NBR.Value, 'Agency_Name');
        WriteDetail(TRAN_TYPE_OUT);

        if (DIR_DEP.FieldByName('IN_PRENOTE').AsString = 'Y') and
           (DM_SERVICE_BUREAU.SB.USE_PRENOTE.AsString = 'Y') then
        begin
          AppendAddendaRecord(DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 0);
          FACHFile.IN_PRENOTE.AsString := 'Y';
        end
        else
          AppendAddendaRecord(DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, DIR_DEP.FieldByName('AMOUNT').Value);

        FACHFile.SetOrderNumber;
        FACHFile.HIDE_FROM_REPORT.Value := 'Y';

        FACHFile.ABA_NUMBER.Value :=
          ACHTrim(DIR_DEP.FieldByName('EE_ABA_NUMBER').AsString);
        if DIR_DEP.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
          FACHFile.BANK_ACCOUNT_NUMBER.Value :=
            ACHTrim(DIR_DEP.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString)
        else
          FACHFile.BANK_ACCOUNT_NUMBER.Value := DIR_DEP.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString;
        FACHFile.TRAN.AsString := GetAchAccountType(DIR_DEP.FieldByName('EE_BANK_ACCOUNT_TYPE').AsString);

        FACHFile.NAME.Value := AnsiUpperCase(DIR_DEP.FieldByName('LAST_NAME').Value) + ',' +
                                     AnsiUpperCase(DIR_DEP.FieldByName('FIRST_NAME').Value);

        CustomCaseNumber := ConvertNull(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CUSTOM_CASE_NUMBER.Value, '');
        if (CustomCaseNumber <> '') and (copy(CustomCaseNumber,Length(CustomCaseNumber),1) = 'X') then
          CustomCaseNumber := ConvertNull(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CUSTOM_FIELD.Value, '');

        if DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.IL_MED_INS_ELIGIBLE.Value = GROUP_BOX_NOT_APPLICATIVE then
          sMedIns := GROUP_BOX_YES
        else
          sMedIns := DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.IL_MED_INS_ELIGIBLE.Value;

        s := Format('DED*CS*%-1.20s*', [CustomCaseNumber]) +
             FormatDateTime('yymmdd"*"', FACHFile.CHECK_DATE.AsDateTime) +
             Format('%1.10s*%9.9s*%1.1s*%-0.10s*%-0.7s*', [IntToStr(RoundInt64(Abs(FACHFile.AMOUNT.Value) * 100)),
                                ConvertSSN(DIR_DEP.FieldByName('SOCIAL_SECURITY_NUMBER').AsString),
                                sMedIns,
                                FACHFile.NAME.AsString, ConvertNull(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FIPS_CODE.Value, '')]);
        if DIR_DEP.FieldByName('CURRENT_TERMINATION_CODE').AsString = EE_TERM_ACTIVE then
          s := s + '\'
        else
          s := s + 'Y\';

        FACHFile.WriteAddenda(s, AddendaNbr, EntryNbr);
      end;
    except
      on E: Exception do
      begin
        E.Message := 'Employee #' + FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString + #13 + E.Message;
        raise;
      end;
    end;

    DIR_DEP.Next;
  end;

  if bAnyProcessed then
    sCustomEENumber := DIR_DEP.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString
  else
    sCustomEENumber := '';

  if aNegative then
  begin
    AGENCY_DIR_DEP.Filter := 'MISCELLANEOUS_CHECK_AMOUNT < 0';
    AGENCY_DIR_DEP.Filtered := True;
  end
  else
  begin
    AGENCY_DIR_DEP.Filter := 'MISCELLANEOUS_CHECK_AMOUNT >= 0 or MISCELLANEOUS_CHECK_AMOUNT is null';
    AGENCY_DIR_DEP.Filtered := True;
  end;

// Do Agencies
  if AGENCY_DIR_DEP.RecordCount > 0 then
  begin
    AGENCY_DIR_DEP.First;
    if not bAnyProcessed then
    begin
      FOrderNbr := FACHFile.RecordCount;

      d := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
      if d < Date then
        d := Date;

      while ctx_PayrollCalculation.IsNonBusinessDay(d) do
        d := d + 1;

      if Options.Intercept and Options.BalanceBatches then
        d := EffectiveDateDD;

      StartBatchDefinition('PPD', d, sPPDComments, '', 0);
      bAnyProcessed := True;
    end;
    NumberOfDaysPrior := NumberOfDaysPriorDD;
    CheckDate := CheckDateDD;
    EffectiveDate := EffectiveDateDD;
  end;

  while not AGENCY_DIR_DEP.EOF do //Custom View
  begin

    // ACH Client Data Set

    AddFileHeaderIfNotAddedYet;

    if AGENCY_DIR_DEP.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency = 0 then
    begin
      AGENCY_DIR_DEP.Next;
      Continue;
    end;

    AppendDBDTAmount(null, null, null, null, AGENCY_DIR_DEP['MISCELLANEOUS_CHECK_AMOUNT'], 'DD');
    AppendDetailRecord('', AGENCY_DIR_DEP.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value, PayrollList.DD_IMPOUND.AsString, BANK_REGISTER_TYPE_DirDepOffset);
    AchAmount := AchAmount + AGENCY_DIR_DEP.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value;

    // Don't POST just yet,... non-common fields here...
    FACHFile.SetOrderNumber;

    Assert(DM_SERVICE_BUREAU.SB_AGENCY.Locate('SB_AGENCY_NBR', AGENCY_DIR_DEP.FieldByName('SB_AGENCY_NBR').Value, []), 'Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString);
    if Pos('C', AGENCY_DIR_DEP.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0 then
      FACHFile.NAME.Value := DM_COMPANY.CO.NAME.Value
    else
      FACHFile.NAME.Value := DM_SERVICE_BUREAU.SB_AGENCY.NAME.AsString;

    if not DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_AGENCY.SB_BANKS_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('DD SB_BANK is missing for agency ' + DM_SERVICE_BUREAU.SB_AGENCY.NAME.AsString, IDH_InconsistentData);
    FACHFile.ABA_NUMBER.Value := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);
    if FACHFile.ABA_NUMBER.AsString = '' then
      raise EInconsistentData.CreateHelp('Incorrect ABA number for bank ' + DM_SERVICE_BUREAU.SB_BANKS.NAME.AsString, IDH_InconsistentData);

    FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;

    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value := ACHTrim(DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_NUMBER.AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_NUMBER.AsString;
    if FACHFile.BANK_ACCOUNT_NUMBER.AsString = '' then
      raise EInconsistentData.CreateHelp('Incorrect Bank Account number for agency ' + DM_SERVICE_BUREAU.SB_AGENCY.NAME.AsString, IDH_InconsistentData);

    FACHFile.COMMENTS.Value := FACHFile.COMMENTS.AsString + 'NET CHECK';

    FACHFile.TRAN.AsString := GetAchAccountType(DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_TYPE.AsString);

    WriteDetail(TRAN_TYPE_OUT);

    AGENCY_DIR_DEP.Next;
  end;

  if not bAnyProcessed then
    Exit;

  WriteBatchHeader(True, SegmentNbr, FOrderNbr);
  WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, sCustomEENumber);

  if not IsBlockDebits and (PayrollList.DD_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
  and (PayrollList.DD_IMPOUND.AsString <> PAYMENT_TYPE_NONE) and not BlockCompanyDD then
  begin
    {
    *** Debit Client Account (Trust/Obc/DD) ***   [2 of 3] offset batches
    }

    DBDTDirDep.First;
    while not DBDTDirDep.Eof do
    begin
      if not (DBDTDirDep.FieldByName('Amount').AsCurrency = 0) then
      begin
        if Options.BalanceBatches and not DBDTDirDep['ChildSupport'] then
          StartBatchDefinition('PPD', EffectiveDate, sPPDComments, '', 0)
        else
          StartBatchDefinition('CCD', EffectiveDate, sCCDComments, '', 0);

        FOrderNbr := FACHFile.RecordCount;

        // reverse amount
        AppendDetailRecord('', DBDTDirDep['Amount'] * -1, PayrollList.DD_IMPOUND.AsString, BANK_REGISTER_TYPE_DirDepOffset);

        FACHFile.SetOrderNumber;

        FACHFile.NAME.Value := 'DD IMPOUND';

        // Using DD_CL_BANK_ACCOUNT_NBR,... don't care of Trust or Obc...
        FACHFile.BANK_ACCOUNT_NUMBER.Value := DBDTDirDep['BANK_ACCOUNT_NUMBER'];
        FACHFile.COMMENTS.AsString := FACHFile.COMMENTS.AsString + DBDTDirDep.FieldByName('CL_BANK_ACCOUNT_NBR').AsString;
        FACHFile.ABA_NUMBER.Value := DBDTDirDep['ABA_NUMBER'];
        FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
        FACHFile.TRAN.AsString := GetAchAccountType(DBDTDirDep['BANK_ACCOUNT_TYPE']);

        WriteDetail(TRAN_TYPE_OUT);
        WriteBatchHeader(False, SegmentNbr, FOrderNbr, OFF_DONT_USE, True, False);
        WriteBatchTotal(False, SegmentNbr, FACHFile.RecordCount, sCustomEENumber);
      end;
      DBDTDirDep.Next;
    end;
  end;
  Result := bAmount;
end;

function TevCashProcessing.IsBlockSBCredit: Boolean;
begin
  Result := Options.SunTrust and Options.BlockSBCredit;
end;

function TevCashProcessing.IsBlockDebits: Boolean;
begin
  Result := Options.BlockDebits or IsPayrollPeople;
end;

//******************************************************************************
//                                                                  TRUST or OBC
//******************************************************************************
{
Add   PR_CHECK.NET_WAGES
Where PR_CHECK.CHECK_TYPE = (CHECK_TYPE2_REGULAR or CHECK_TYPE2_VOID)
Add   Total for Misc. Checks (positive and negative)
Then  Debit-Client, Credit-AchOffset, Debit-AchOffset, Credit-Agency
}

function TevCashProcessing.ProcessTrustorObc: Currency;
var
  Negative: Boolean;
  PrAmount: Currency;
  TrustOrObc: string;
  Comments: string;
  OffsetComments: string;
  TrustOrObcDescription: string;
  BlockNegTrust: boolean;
begin
  Result := 0;
  PrAmount := 0;
  TrustOrObc := '';
  TrustOrObcDescription := '';

  if (DM_COMPANY.CO.TRUST_SERVICE.AsString <> TRUST_SERVICE_NO) then
  begin
    TrustOrObc := 'TRUST';
    TrustOrObcDescription := 'PAYROLL TRUST IMPOUND';
  end
  else if (DM_COMPANY.CO.OBC.AsString = 'Y') then
  begin
    TrustOrObc := 'OBC';
    TrustOrObcDescription := 'PROLL TRUST IMP FOR OBC';
  end
  else
    Exit;

  BlockNegTrust := DM_SERVICE_BUREAU.SB.IMPOUND_TRUST_MONIES_AS_RECEIV.Value = GROUP_BOX_NO;

  if DM_COMPANY.CO.TRUST_SERVICE.AsString <> TRUST_SERVICE_AGENCY then
  begin
    PrChecks.Result.First;
    while not PrChecks.Result.Eof do
    begin
      if PrChecks.Result.FieldByName('NET_WAGES').AsCurrency <> 0 then
      begin
        if PrChecks.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR then
          PrAmount := PrAmount + PrChecks.Result.FieldByName('NET_WAGES').AsCurrency;

        if not BlockNegTrust and (PrChecks.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_VOID) then
        begin
          // See if it was originally a manual check
          if ExtractFromFiller(PrChecks.Result.FieldByName('FILLER').AsString, 1, 8) <> '' then
          begin
            if ALL_PR_CHECKS.Lookup('PR_CHECK_NBR', StrToInt(ExtractFromFiller(PrChecks.Result.FieldByName('FILLER').AsString, 1, 8)), 'CHECK_TYPE') <> CHECK_TYPE2_MANUAL then
              PrAmount := PrAmount + PrChecks.Result.FieldByName('NET_WAGES').AsCurrency;
          end
          else
          begin
            if ExtractFromFiller(DM_PAYROLL.PR.FILLER.AsString, 1, 8) <> '' then
            begin
              if ALL_PR_CHECKS.Lookup('PR_NBR;EE_NBR;NET_WAGES', VarArrayOf([StrToInt(ExtractFromFiller(DM_PAYROLL.PR.FILLER.AsString, 1, 8)),
                PrChecks.Result.FieldByName('EE_NBR').Value,
                PrChecks.Result.FieldByName('NET_WAGES').Value * (-1)]),
                'CHECK_TYPE') <> CHECK_TYPE2_MANUAL then
                PrAmount := PrAmount + PrChecks.Result.FieldByName('NET_WAGES').AsCurrency;
            end
            else
              PrAmount := PrAmount + PrChecks.Result.FieldByName('NET_WAGES').AsCurrency;
          end;
        end;
      end;
      PrChecks.Result.Next;
    end;
  end;

  // Future!!! do a Locate for iPrNbr - for faster processing...
  DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.First;
  while not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Eof do
  begin
    if ((DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_TYPE.AsString = MISC_CHECK_TYPE_AGENCY) or
        (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_TYPE.AsString = MISC_CHECK_TYPE_AGENCY_VOID) and not BlockNegTrust) and
       (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.PR_NBR.AsInteger = PrNbr) and
       (DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldValues['CL_AGENCY_NBR'], 'PAYMENT_TYPE') <> Payment_Type_DirectDeposit) then
    begin
      if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_AMOUNT.AsCurrency <> 0 then
        PrAmount := PrAmount + DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_AMOUNT.AsCurrency;
    end;
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Next;
  end;

  if PrAmount = 0 then
    Exit;

  Result := PrAmount;

  FOrderNbr := FACHFile.RecordCount;

  // TRUST or OBC uses different "Debit No of Days Prior"
  if (DM_COMPANY.CO.TRUST_SERVICE.AsString <> TRUST_SERVICE_NO) or (DM_COMPANY.CO.OBC.AsString = 'Y') then
    NumberOfDaysPrior := DM_COMPANY.CO.DEBIT_NUMBER_DAYS_PRIOR_PR.AsInteger
  else
    NumberOfDaysPrior := DM_COMPANY.CO.DEBIT_NUMBER_OF_DAYS_PRIOR_DD.AsInteger;
  // Calculate CheckDate/Effective Date...
  CheckDate := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
  CalculateACHDates(CheckDate, EffectiveDate, NumberOfDaysPrior);

  Negative := (PrAmount < 0);

  if (Negative and (not ReversalACH)) or ((not Negative) and ReversalACH) then
  begin
    if IsBlockDebits then
      Exit;
    if Options.DescReversal then
    begin
      Comments := 'REVERSAL-U';
      OffsetComments := 'REVERSAL+U';
    end
    else
    begin
      Comments := 'REVERSAL';
      OffsetComments := 'REVERSAL';
    end;
  end
  else
  begin
    Comments := TrustOrObc;
    OffsetComments := 'ACHOFFSET';
  end;

   // ACH Client Data Set

  if not IsBlockDebits or IsPayrollPeople then
  begin
    AddFileHeaderIfNotAddedYet;

    StartBatchDefinition('CCD', EffectiveDate, Comments, '', 0);

      // debit client by taking positive billing amount and multiply by * -1

    AppendDetailRecord('', PrAmount * -1, PayrollList.TRUST_IMPOUND.AsString, BANK_REGISTER_TYPE_TrustImpound);
    FACHFile.SetOrderNumber;
    FACHFile.SEGMENT_NBR.Value := SegmentNbr;
    FACHFile.NAME.AsString     := TrustOrObcDescription;

      // debit client
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.PAYROLL_CL_BANK_ACCOUNT_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('TRUST/OBC: PAYROLL_CL_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', ClBankAccounts.Result.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ', Client Bank Account ' + ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;

    FACHFile.COMMENTS.Value := FACHFile.COMMENTS.Value + ClBankAccounts.Result.FieldByName('CL_BANK_ACCOUNT_NBR').AsString;

    FACHFile.ABA_NUMBER.AsString := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

    FACHFile.TRAN.AsString :=
      GetAchAccountType(ClBankAccounts.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString);

    WriteDetail(TRAN_TYPE_OUT);
    WriteBatchHeader(True, SegmentNbr, FOrderNbr);
    WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
  end;

  {
  *** Credit ACH Offset Account ***   [1 of 3] offset batches
  // similar to [3 of 3] in EE Direct Deposits...
  }
  if not IsBlockSBCredit then
  begin
    if not IsBlockDebits then
      StartBatchDefinition('CCD', EffectiveDate, Comments, BANK_REGISTER_TYPE_TrustImpound, TrustImpoundSB_BANK_ACCOUNTS_NBR)
    else
      StartBatchDefinition('PPD', EffectiveDate, Comments, BANK_REGISTER_TYPE_TrustImpound, TrustImpoundSB_BANK_ACCOUNTS_NBR);

    AddFileHeaderIfNotAddedYet;


    FOrderNbr := FACHFile.RecordCount;

    // Keep original amount
    AppendDetailRecord('', PrAmount, PayrollList.TRUST_IMPOUND.AsString, BANK_REGISTER_TYPE_TrustImpound);

    FACHFile.SetOrderNumber;
    AchAmount := AchAmount + PrAmount;
    FACHFile.CR_AFTER_LINE.Value := 'Y';
    FACHFile.NAME.AsString       := TrustOrObcDescription;

    // credit S/B  TRUST and OBC both goto Trust Impound
    if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.LOCATE('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.TRUST_SB_ACCOUNT_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('TRUST SB_BANK_ACCOUNTS_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.LOCATE('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []), 'Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString);
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;

    FACHFile.ABA_NUMBER.Value :=
      ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

    FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;

    FACHFile.TRAN.AsString :=
      GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString);

    WriteDetail(TRAN_TYPE_OUT);

    WriteBatchHeader(False, SegmentNbr, FOrderNbr, OFF_DONT_USE, not IsBlockDebits);

    WriteBatchTotal(False, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
  end;

end;

procedure TevCashProcessing.DoOnConstruction;
var
  TempString: String;
begin
  Randomize;
  FBelowThresholdList:= TisStringList.Create;
  FACHFile := TevAchDataSet.Create(nil);
  CompanyTaxLiability := 0;
  strFillerStamp := '';
  ReversalACH := False;
  v := VarArrayCreate([0, 12], varVariant);
  FACHFiles := TisListOfValues.Create;
  FFileSaved := False;
  FCombining := False;
  FSavingACH := False;
  TempString := FormatDateTime('YYYYMMDDHHNNSS', Now);
  FFileName := TempString + '.ach';
  strFillerStamp := TempString + ':' + Context.UserAccount.User + ':' + ExtractFileName(FFileName);
  Options := TACHOptions.Create;

  ALL_PR_CHECKS      := TevClientDataSet.Create(nil);
  DIR_DEP            := TevClientDataSet.Create(nil);
  DIR_DEP.ProviderName := 'CL_CUSTOM_PROV';
  AGENCY_DIR_DEP     := TevClientDataSet.Create(nil);
  AGENCY_DIR_DEP.ProviderName := 'CL_CUSTOM_PROV';
  ACHFile1       := TevClientDataSet.Create(nil);
  DBDTDirDep         := TevClientDataSet.Create(nil);
  cdsTaxDistr        := TevClientDataSet.Create(nil);

  DBDTDirDep.FieldDefs.Add('CO_DIVISION',         ftInteger);
  DBDTDirDep.FieldDefs.Add('CO_BRANCH',           ftInteger);
  DBDTDirDep.FieldDefs.Add('CO_DEPARTMENT',       ftInteger);
  DBDTDirDep.FieldDefs.Add('CO_TEAM',             ftInteger);
  DBDTDirDep.FieldDefs.Add('AMOUNT',              ftFloat);
  DBDTDirDep.FieldDefs.Add('BANK_ACCOUNT_NUMBER', ftString, 20);
  DBDTDirDep.FieldDefs.Add('ABA_NUMBER',          ftString, 20);
  DBDTDirDep.FieldDefs.Add('BANK_ACCOUNT_TYPE',   ftString);
  DBDTDirDep.FieldDefs.Add('ChildSupport',        ftBoolean);
  DBDTDirDep.FieldDefs.Add('CL_BANK_ACCOUNT_NBR', ftInteger);
  DBDTDirDep.CreateDataSet;

  cdsTaxDistr.AggregatesActive := True;
  cdsTaxDistr.ProviderName := 'CL_CUSTOM_PROV';

  cdsTaxDistr.FieldDefs.Add('TAX',               ftCurrency);
  cdsTaxDistr.FieldDefs.Add('CO_DIVISION_NBR',   ftInteger);
  cdsTaxDistr.FieldDefs.Add('CO_BRANCH_NBR',     ftInteger);
  cdsTaxDistr.FieldDefs.Add('CO_DEPARTMENT_NBR', ftInteger);
  cdsTaxDistr.FieldDefs.Add('CO_TEAM_NBR',       ftInteger);
  cdsTaxDistr.FieldDefs.Add('PR_CHECK_NBR',      ftInteger);

  cdsTaxDistr.CreateDataSet;

  cdsTaxDistr.AddIndex('DBDT','CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR', [], '', '', 0);

  StatusFromIndex := PR_ACH_STATUS_FROM_READY;
  StatusToIndex := PR_ACH_STATUS_TO_SENT;

  BlockNegativeChecks := False;

  ACHFile1.Data := FACHFile.Data;
  FSkipList := TisStringList.Create;
  FErrorMessages := '';

end;

function TevCashProcessing.ProcessTaxes(aNegative: Boolean): Currency;
var
  sComments: string;
  sOffsetComments: string;
  vDiv, vBr, vDep, vTm: Variant;
  f: Currency;
  TaxSum: real;
begin

  Result := 0;

  if not (DM_COMPANY.CO.TAX_SERVICE.AsString = 'Y') then
    exit;

  if (FTotalTaxLiability) = 0 then
    Exit;

  if ((FTotalTaxLiability < 0) and BlockNegativeLiabilities) then
    Exit;

  if DBDTDirDep.Active then
    DBDTDirDep.Close;
  DBDTDirDep.CreateDataSet;

  if cdsTaxDistr.Active then
  begin
    cdsTaxDistr.First;
    vDiv := cdsTaxDistr['CO_DIVISION_NBR'];
    vBr := cdsTaxDistr['CO_BRANCH_NBR'];
    vDep := cdsTaxDistr['CO_DEPARTMENT_NBR'];
    vTm := cdsTaxDistr['CO_TEAM_NBR'];
    f := 0;
    TaxSum := 0;
    while not cdsTaxDistr.Eof do
    begin
      if (vDiv <> cdsTaxDistr['CO_DIVISION_NBR']) or
         (vBr <> cdsTaxDistr['CO_BRANCH_NBR']) or
         (vDep <> cdsTaxDistr['CO_DEPARTMENT_NBR']) or
         (vTm <> cdsTaxDistr['CO_TEAM_NBR']) then
      begin
        AppendDBDTAmount(vDiv, vBr, vDep, vTm, RoundAll(TaxSum, 2), 'TAX');
        f := f + RoundAll(TaxSum, 2);
        TaxSum := 0;
        vDiv := cdsTaxDistr['CO_DIVISION_NBR'];
        vBr := cdsTaxDistr['CO_BRANCH_NBR'];
        vDep := cdsTaxDistr['CO_DEPARTMENT_NBR'];
        vTm := cdsTaxDistr['CO_TEAM_NBR'];
      end;
      TaxSum := TaxSum + cdsTaxDistr.FieldByName('TAX').AsFloat;
      cdsTaxDistr.Next;
    end;
    AppendDBDTAmount(vDiv, vBr, vDep, vTm, RoundAll(TaxSum, 2), 'TAX');
    f := f + RoundAll(TaxSum, 2);
    if FTotalTaxLiability - f <> 0 then
      AppendDBDTAmount(vDiv, vBr, vDep, vTm, FTotalTaxLiability - f, 'TAX');
  end
  else
    AppendDBDTAmount(null, null, null, null, FTotalTaxLiability, 'TAX');


  if (aNegative and (not ReversalACH)) or ((not aNegative) and ReversalACH) then
  begin
    if IsBlockDebits then
      Exit;
    if Options.DescReversal then
    begin
      sComments := 'REVERSAL-T';
      sOffsetComments := 'REVERSAL+T';
    end
    else
    begin
      sComments := 'REVERSAL';
      sOffsetComments := 'REVERSAL';
    end;
  end
  else
  begin
    sComments := 'TAX';
    sOffsetComments := 'ACHOFFSET';
  end;

  Result := FTotalTaxLiability;

  NumberOfDaysPrior := DM_COMPANY.CO.DEBIT_NUMBER_OF_DAYS_PRIOR_TAX.AsInteger;
  CheckDate := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
  CalculateACHDates(CheckDate, EffectiveDate, NumberOfDaysPrior);

  AddFileHeaderIfNotAddedYet;

  // debit client by taking positive billing amount and multiply by * -1

  if not IsBlockDebits then
  begin
    DBDTDirDep.First;
    while not DBDTDirDep.Eof do
    begin
      if DBDTDirDep.FieldByName('Amount').AsCurrency = 0 then
      begin
        DBDTDirDep.Next;
        Continue;
      end;

      StartBatchDefinition('CCD', EffectiveDate, sComments, '', 0);
      FOrderNbr := FACHFile.RecordCount;

      AppendDetailRecord('', DBDTDirDep['Amount'] * -1, PayrollList.TAX_IMPOUND.AsString, BANK_REGISTER_TYPE_TaxImpound);

      FACHFile.SEGMENT_NBR.Value := SegmentNbr;

      FACHFile.SetOrderNumber;

      FACHFile.NAME.Value := 'TAX IMPOUND';

      // debit client
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DBDTDirDep['BANK_ACCOUNT_NUMBER'];

      FACHFile.COMMENTS.Value := FACHFile.COMMENTS.Value + DBDTDirDep.FieldByName('CL_BANK_ACCOUNT_NBR').AsString;

      FACHFile.ABA_NUMBER.AsString := DBDTDirDep['ABA_NUMBER'];

      FACHFile.TRAN.AsString := GetAchAccountType(DBDTDirDep['BANK_ACCOUNT_TYPE']);

      WriteDetail(TRAN_TYPE_OUT);
      WriteBatchHeader(True, SegmentNbr, FOrderNbr);
      WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
      DBDTDirDep.Next;
    end;
  end;

  {
  *** Credit ACH Offset Account ***   [1 of 3] offset batches
  // similar to [3 of 3] in EE Direct Deposits...
  }
  if not IsBlockSBCredit then
  begin
    if not IsBlockDebits then
      StartBatchDefinition('CCD', EffectiveDate, sComments, BANK_REGISTER_TYPE_TaxImpound, TaxImpoundSB_BANK_ACCOUNTS_NBR)
    else
      StartBatchDefinition('PPD', EffectiveDate, sComments, BANK_REGISTER_TYPE_TaxImpound, TaxImpoundSB_BANK_ACCOUNTS_NBR);

    FOrderNbr := FACHFile.RecordCount;

    // Keep original amount
    AppendDetailRecord('', FTotalTaxLiability, PayrollList.TAX_IMPOUND.AsString, BANK_REGISTER_TYPE_TaxImpound);
    AchAmount := AchAmount + FTotalTaxLiability;

    FACHFile.SetOrderNumber;
    FACHFile.CR_AFTER_LINE.Value := 'Y';
    FACHFile.NAME.Value := 'TAX IMPOUND';

    // credit S/B
    if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('TAX_SB_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []), 'Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString);
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value := ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;


    FACHFile.ABA_NUMBER.AsString := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);
    FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;
    FACHFile.TRAN.AsString := GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString);

    WriteDetail(TRAN_TYPE_OUT);
    WriteBatchHeader(False, SegmentNbr, FOrderNbr, OFF_DONT_USE, not IsBlockDebits);
    WriteBatchTotal(False, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
  end;
end;

function TevCashProcessing.GetTotalTaxLiabilities: Currency;

  function GetLocalTaxLiabilities: Currency;
  begin
    Result := 0;
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.First;
    while not DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Eof do
    begin
      Result := Result + DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Next;
    end;
  end;

  function GetStateTaxLiabilities: Currency;
  begin
    Result := 0;
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.First;
    while not DM_COMPANY.CO_STATE_TAX_LIABILITIES.Eof do
    begin
      Result := Result + DM_COMPANY.CO_STATE_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      DM_COMPANY.CO_STATE_TAX_LIABILITIES.Next;
    end;
  end;

  function GetSUILiabilities: Currency;
  begin
    Result := 0;
    DM_COMPANY.CO_SUI_LIABILITIES.First;
    while not DM_COMPANY.CO_SUI_LIABILITIES.Eof do
    begin
      Result := Result + DM_COMPANY.CO_SUI_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      DM_COMPANY.CO_SUI_LIABILITIES.Next;
    end;
  end;

  function GetFedTax940Liability(aPR_NBR: integer): Currency;
  begin
    Result := 0;

    if StatusToIndex = PR_ACH_STATUS_TO_SENT then
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+IntToStr(aPR_NBR) +
      ' AND STATUS IN ('''+TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+''','''+
      TAX_DEPOSIT_STATUS_PENDING+''')'
    else
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+IntToStr(aPR_NBR);
    DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := True;


    DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
    while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
    begin
      if (DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('PR_NBR').AsInteger = aPR_NBR) and
         not TaxIs941(DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString) and
         (TAX_LIABILITY_TYPE_COBRA_CREDIT <> DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString)
      then
        Result := Result + DM_COMPANY.CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
    end;
  end;

  function GetFedTax941Liability(aPR_NBR: integer): Currency;
  begin
    Result := 0;
    if StatusToIndex = PR_ACH_STATUS_TO_SENT then
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+IntToStr(aPR_NBR) +
      ' AND STATUS IN ('''+TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+''','''+
      TAX_DEPOSIT_STATUS_PENDING+''')'
    else
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+IntToStr(aPR_NBR);
    DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := True;
    DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
    while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
    begin
      if (TaxIs941(DM_COMPANY.CO_FED_TAX_LIABILITIES.TAX_TYPE.AsString) and
         (TAX_LIABILITY_TYPE_COBRA_CREDIT <> DM_COMPANY.CO_FED_TAX_LIABILITIES.TAX_TYPE.AsString))
         or (
         (TAX_LIABILITY_TYPE_COBRA_CREDIT = DM_COMPANY.CO_FED_TAX_LIABILITIES.TAX_TYPE.AsString)
         and (DM_COMPANY.CO_FED_TAX_LIABILITIES.AMOUNT.AsCurrency < 0))
      then
      begin
        if TAX_LIABILITY_TYPE_COBRA_CREDIT <> DM_COMPANY.CO_FED_TAX_LIABILITIES.TAX_TYPE.AsString then
          Result := Result + DM_COMPANY.CO_FED_TAX_LIABILITIES.AMOUNT.AsCurrency
        else
          Result := Result - DM_COMPANY.CO_FED_TAX_LIABILITIES.AMOUNT.AsCurrency;
      end;
      DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
    end;
  end;

var
  FedTax940, FedTax941, AchFedTax941, Credit941: Currency;
begin

  DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+IntToStr(PrNbr);
  DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := True;
  DM_COMPANY.CO_FED_TAX_LIABILITIES.First;

  FedTax940 := GetFedTax940Liability(PrNbr);
  FedTax941 := GetFedTax941Liability(PrNbr);

  if FedTax941 < 0 then
  begin
    Credit941 := FedTax941;
    FedTax941 := 0;
  end
  else
    Credit941 := 0;

  Result := FedTax940;

  if FedTaxImpounded < 0 then
    FedTaxImpounded := 0;

  AchFedTax941 := FedTax941 + TaxCredits;
  if AchFedTax941 < 0 then
  begin
    if - AchFedTax941 > FedTaxImpounded then
      AchFedTax941 := -FedTaxImpounded;
  end;
  FedTaxImpounded := FedTaxImpounded + FedTax941;

  AchFedTax941 := AchFedTax941 + Credit941;

  Result := Result + AchFedTax941;

  Result := Result + GetStateTaxLiabilities;
  Result := Result + GetLocalTaxLiabilities;
  Result := Result + GetSUILiabilities;

end;

destructor TevCashProcessing.Destroy;
begin
  ALL_PR_CHECKS.Free;
  DIR_DEP.Free;
  AGENCY_DIR_DEP.Free;
  ACHFile1.Free;
  DBDTDirDep.Free;
  cdsTaxDistr.Free;
  FACHFile.Free;
  Options.Free;
  inherited;
end;

function TevCashProcessing.ProcessWorkersComp: boolean;
var
  sComments: string;
  sOffsetComments: string;
  fAmount: Currency;
begin

  Result := False;
  if ReversalACH then
  begin
    if Options.DescReversal then
    begin
      sComments := 'REVERSAL-W';
      sOffsetComments := 'REVERSAL+W';
    end
    else
    begin
      sComments := 'REVERSAL';
      sOffsetComments := 'REVERSAL';
    end;
  end
  else
  begin
    sComments := 'WORKERS COMP';
    sOffsetComments := 'ACHOFFSET';
  end;

  if PayrollList.WC_AMOUNT.IsNull then
  begin
    PayrollList.Edit;
    PayrollList.WC_AMOUNT.Value := ctx_PayrollCalculation.CalcWorkersComp(PrNbr);
    PayrollList.Post;
  end;

  fAmount := PayrollList.WC_AMOUNT.Value;
  
  if fAmount = 0 then
    Exit;

  Result := True;

  FOrderNbr := FACHFile.RecordCount;

  NumberOfDaysPrior := DM_COMPANY.CO.DEBIT_NUMBER_DAYS_PRIOR_WC.AsInteger;
  CheckDate := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
  CalculateACHDates(CheckDate, EffectiveDate, NumberOfDaysPrior);

  // ACH Client Data Set

  if not IsBlockDebits then
  begin
    StartBatchDefinition('CCD', EffectiveDate, sComments, '', 0);

    AddFileHeaderIfNotAddedYet;

    // debit client by taking positive billing amount and multiply by * -1

    AppendDetailRecord('', fAmount * -1, PayrollList.WC_IMPOUND.AsString, BANK_REGISTER_TYPE_WCImpound);

    FACHFile.SEGMENT_NBR.Value := SegmentNbr;

    FACHFile.SetOrderNumber;

    FACHFile.NAME.AsString := 'WORKERS COMP IMPOUND';

        // debit client
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.W_COMP_CL_BANK_ACCOUNT_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('W_COMP_CL_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', ClBankAccounts.Result.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ', Client Bank Account ' + ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;

    FACHFile.COMMENTS.Value := FACHFile.COMMENTS.Value + ClBankAccounts.Result.FieldByName('CL_BANK_ACCOUNT_NBR').AsString;

    FACHFile.ABA_NUMBER.AsString := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

    FACHFile.TRAN.AsString := GetAchAccountType(ClBankAccounts.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString);

    WriteDetail(TRAN_TYPE_OUT);
    WriteBatchHeader(True, SegmentNbr, FOrderNbr);
    WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
  end;

  {
  *** Credit ACH Offset Account ***   [1 of 3] offset batches
  // similar to [3 of 3] in EE Direct Deposits...
  }
  if not IsBlockSBCredit then
  begin
    if not IsBlockDebits then
      StartBatchDefinition('CCD', EffectiveDate, sComments, BANK_REGISTER_TYPE_WCImpound,
        WCImpoundSB_BANK_ACCOUNTS_NBR)
    else
      StartBatchDefinition('PPD', EffectiveDate, sComments, BANK_REGISTER_TYPE_WCImpound,
        WCImpoundSB_BANK_ACCOUNTS_NBR);


    FOrderNbr := FACHFile.RecordCount;

    // Keep original amount
    AppendDetailRecord('', fAmount, PayrollList.WC_IMPOUND.AsString, BANK_REGISTER_TYPE_WCImpound);
    AchAmount := AchAmount + fAmount;

    FACHFile.CR_AFTER_LINE.Value := 'Y';

    FACHFile.SetOrderNumber;

    FACHFile.NAME.Value := 'WORKERS COMP IMPOUND';

    // credit S/B
    if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.W_COMP_SB_BANK_ACCOUNT_NBR.Value, []) then
      raise EInconsistentData.CreateHelp('W_COMP_SB_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []), 'Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString);
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;


    FACHFile.ABA_NUMBER.AsString :=
      ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

    FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;

    FACHFile.TRAN.AsString :=
      GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString);

    WriteDetail(TRAN_TYPE_OUT);
    WriteBatchHeader(False, SegmentNbr, FOrderNbr, OFF_DONT_USE, not IsBlockDebits, False);
    WriteBatchTotal(False, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
  end;
end;

function TevCashProcessing.IsResending: boolean;
begin
  Result := StatusFromIndex = 3;
end;

function TevCashProcessing.AchDescription: String;
begin
  Result := 'PROCESS ' + 'DATES: ' + FormatDateTime('MM/DD/YY', dtFrom) +
            '  TO: ' + FormatDateTime('MM/DD/YY', dtTo);
end;

procedure TevCashProcessing.AppendDBDTAmount(iDiv, iBr, iDep, iTm: Variant; fAmount: Currency; Mode: string; const ChildSupport: boolean = false);
var
  vDiv, vBr, vDep, vTm: Variant;
  sBankAcc, sABA, sBankType: string;
begin
  vDiv := null;
  vBr := null;
  vDep := null;
  vTm := null;
  if not VarIsNull(iTm) and
     DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', iTm, []) and
     not VarIsNull(DM_COMPANY.CO_TEAM[Mode + '_CL_BANK_ACCOUNT_NBR']) then
  begin
    vTm := iTm;
    vDep := iDep;
    vBr := iBr;
    vDiv := iDiv;
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO_TEAM[Mode + '_CL_BANK_ACCOUNT_NBR'], []) then
      raise EInconsistentData.CreateHelp(Mode + '_CL_BANK_ACCOUNT_NBR is missing for team ' + DM_COMPANY.CO_TEAM.CUSTOM_TEAM_NUMBER.AsString, IDH_InconsistentData);
  end
  else if not VarIsNull(iDep) and
          DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', iDep, []) and
          not VarIsNull(DM_COMPANY.CO_DEPARTMENT[Mode + '_CL_BANK_ACCOUNT_NBR']) then
  begin
    vDep := iDep;
    vBr := iBr;
    vDiv := iDiv;
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO_DEPARTMENT[Mode + '_CL_BANK_ACCOUNT_NBR'], []) then
      raise EInconsistentData.CreateHelp(Mode + '_CL_BANK_ACCOUNT_NBR is missing for department ' + DM_COMPANY.CO_DEPARTMENT.CUSTOM_DEPARTMENT_NUMBER.AsString, IDH_InconsistentData);
  end
  else if not VarIsNull(iBr) and
          DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', iBr, []) and
          not VarIsNull(DM_COMPANY.CO_BRANCH[Mode + '_CL_BANK_ACCOUNT_NBR']) then
  begin
    vBr := iBr;
    vDiv := iDiv;
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO_BRANCH[Mode + '_CL_BANK_ACCOUNT_NBR'], []) then
      raise EInconsistentData.CreateHelp(Mode + '_CL_BANK_ACCOUNT_NBR is missing for branch ' + DM_COMPANY.CO_BRANCH.CUSTOM_BRANCH_NUMBER.AsString, IDH_InconsistentData);
  end
  else if not VarIsNull(iDiv) and
          DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', iDiv, []) and
          not VarIsNull(DM_COMPANY.CO_DIVISION[Mode + '_CL_BANK_ACCOUNT_NBR']) then
  begin
    vDiv := iDiv;
    if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO_DIVISION[Mode + '_CL_BANK_ACCOUNT_NBR'], []) then
      raise EInconsistentData.CreateHelp(Mode + '_CL_BANK_ACCOUNT_NBR is missing for division ' + DM_COMPANY.CO_DIVISION.CUSTOM_DIVISION_NUMBER.AsString, IDH_InconsistentData);
  end
  else if not ClBankAccounts.Result.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO[Mode + '_CL_BANK_ACCOUNT_NBR'], []) then
    raise EInconsistentData.CreateHelp(Mode + '_CL_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

  Assert(DM_SERVICE_BUREAU.SB_BANKS.LOCATE('SB_BANKS_NBR', ClBankAccounts.Result.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ', Client Bank Account ' + ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
  if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
    sBankAcc := ACHTrim(ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString)
  else
    sBankAcc := ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
  sBankType := ClBankAccounts.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
  sABA := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

  if DBDTDirDep.Locate('CO_DIVISION;CO_BRANCH;CO_DEPARTMENT;CO_TEAM;ChildSupport', VarArrayOf([vDiv, vBr, vDep, vTm, ChildSupport]), []) then
    DBDTDirDep.Edit
  else
  begin
    DBDTDirDep.Insert;
    DBDTDirDep['CO_TEAM'] := vTm;
    DBDTDirDep['CO_DEPARTMENT'] := vDep;
    DBDTDirDep['CO_BRANCH'] := vBr;
    DBDTDirDep['CO_DIVISION'] := vDiv;
    DBDTDirDep['BANK_ACCOUNT_NUMBER'] := sBankAcc;
    DBDTDirDep['ABA_NUMBER'] := sABA;
    DBDTDirDep['BANK_ACCOUNT_TYPE'] := sBankType;
    DBDTDirDep['ChildSupport'] := ChildSupport;
    DBDTDirDep['CL_BANK_ACCOUNT_NBR'] := ClBankAccounts.Result.FieldByName('CL_BANK_ACCOUNT_NBR').AsInteger;
  end;
  DBDTDirDep.FieldByName('Amount').AsFloat := DBDTDirDep.FieldByName('Amount').AsFloat + fAmount;
  DBDTDirDep.Post;
end;

function TevCashProcessing.GetAccountLevel: TevAccountLevel;
begin
  if (DM_COMPANY.CO['BANK_ACCOUNT_LEVEL'] = CLIENT_LEVEL_COMPANY) or (DM_COMPANY.CO['BILLING_LEVEL'] = CLIENT_LEVEL_COMPANY) then
    Result := lvCompany
  else
  if (DM_COMPANY.CO['BANK_ACCOUNT_LEVEL'] = CLIENT_LEVEL_DIVISION) or (DM_COMPANY.CO['BILLING_LEVEL'] = CLIENT_LEVEL_DIVISION) then
    Result := lvDivision
  else
  if (DM_COMPANY.CO['BANK_ACCOUNT_LEVEL'] = CLIENT_LEVEL_BRANCH) or (DM_COMPANY.CO['BILLING_LEVEL'] = CLIENT_LEVEL_BRANCH) then
    Result := lvBranch
  else
  if (DM_COMPANY.CO['BANK_ACCOUNT_LEVEL'] = CLIENT_LEVEL_DEPT) or (DM_COMPANY.CO['BILLING_LEVEL'] = CLIENT_LEVEL_DEPT) then
    Result := lvDepartment
  else
    Result := lvTeam;
end;

function TevCashProcessing.ProcessBilling: Currency;
var
  sComments: string;
  sOffsetComments: string;
  fAmount: Currency;
begin

  if ReversalACH then
  begin
    if Options.DescReversal then
    begin
      sComments := 'REVERSAL-B';
      sOffsetComments := 'REVERSAL+B';
    end
    else
    begin
      sComments := 'REVERSAL';
      sOffsetComments := 'REVERSAL';
    end;
  end
  else
  begin
    sComments := 'BILLING';
    sOffsetComments := 'ACHOFFSET';
  end;

  if DBDTDirDep.Active then
    DBDTDirDep.Close;
  DBDTDirDep.CreateDataSet;

  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;

  with TExecDSWrapper.Create('OldBillingForACH') do
  begin
    SetParam('CoNbr', FCoNbr);
    SetParam('PrNbr', PrNbr);
    if DM_COMPANY.CO['BILLING_LEVEL'] <> CLIENT_LEVEL_COMPANY then
      SetMacro('DBDT', 'b.CO_DIVISION_NBR, b.CO_BRANCH_NBR, b.CO_DEPARTMENT_NBR, b.CO_TEAM_NBR,')
    else
      SetMacro('DBDT', '');
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;

  fAmount := 0;
  if DM_COMPANY.CO['BILLING_LEVEL'] = CLIENT_LEVEL_COMPANY then
  begin
    if ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency <> 0 then
    begin
      AppendDBDTAmount(null, null, null, null, ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency, 'BILLING');
      fAmount := ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency
    end
  end
  else
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency <> 0 then
      begin
        AppendDBDTAmount(ctx_DataAccess.CUSTOM_VIEW['CO_DIVISION_NBR'],
                     ctx_DataAccess.CUSTOM_VIEW['CO_BRANCH_NBR'],
                     ctx_DataAccess.CUSTOM_VIEW['CO_DEPARTMENT_NBR'],
                     ctx_DataAccess.CUSTOM_VIEW['CO_TEAM_NBR'],
                     ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency,
                         'BILLING');
        fAmount := fAmount + ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency
      end;
      ctx_DataAccess.CUSTOM_VIEW.Next
    end;

  Result := fAmount;

  ctx_DataAccess.CUSTOM_VIEW.First;

  if ctx_DataAccess.CUSTOM_VIEW.RecordCount < 1 then
  begin
    ctx_DataAccess.CUSTOM_VIEW.Close;
    Exit;
  end;

  NumberOfDaysPrior := DM_COMPANY.CO.DEBIT_NUMBER_DAYS_PRIOR_BILL.AsInteger;
  CheckDate := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
  CalculateACHDates(CheckDate, EffectiveDate, NumberOfDaysPrior);

  if not IsBlockDebits then
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      if not (ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency = 0) then
      begin
        // ACH Client Data Set

        StartBatchDefinition('CCD', EffectiveDate, sComments, '', 0);

        AddFileHeaderIfNotAddedYet;

        // debit client by taking positive billing amount and multiply by * -1

        FOrderNbr := FACHFile.RecordCount;
        AppendDetailRecord('', ctx_DataAccess.CUSTOM_VIEW.FieldByName('Amount').AsCurrency * -1, PayrollList.BILLING_IMPOUND.AsString, BANK_REGISTER_TYPE_BillingImpound);

        FACHFile.SEGMENT_NBR.Value := SegmentNbr;

        FACHFile.SetOrderNumber;

        FACHFile.NAME.AsString := 'BILLING IMPOUND';

        case GetAccountLevel of
        lvDivision  : if not DBDTDirDep.Locate('CO_DIVISION', ctx_DataAccess.CUSTOM_VIEW['CO_DIVISION_NBR'], []) then
                        raise EInconsistentData.CreateHelp('Division account is missing' , IDH_InconsistentData);
        lvBranch    : if not DBDTDirDep.Locate('CO_DIVISION; CO_BRANCH', ctx_DataAccess.CUSTOM_VIEW['CO_DIVISION_NBR; CO_BRANCH_NBR'], []) then
                        raise EInconsistentData.CreateHelp('Branch account is missing' , IDH_InconsistentData);
        lvDepartment: if not DBDTDirDep.Locate('CO_DIVISION; CO_BRANCH; CO_DEPARTMENT',
                            ctx_DataAccess.CUSTOM_VIEW['CO_DIVISION_NBR; CO_BRANCH_NBR; CO_DEPARTMENT_NBR'], []) then
                        raise EInconsistentData.CreateHelp('Department account is missing' , IDH_InconsistentData);
        lvTeam      : if not DBDTDirDep.Locate('CO_DIVISION; CO_BRANCH; CO_DEPARTMENT; CO_TEAM',
                            ctx_DataAccess.CUSTOM_VIEW['CO_DIVISION_NBR; CO_BRANCH_NBR; CO_DEPARTMENT_NBR; CO_TEAM_NBR'], []) then
                        raise EInconsistentData.CreateHelp('Team account is missing' , IDH_InconsistentData);
        end;

        FACHFile.BANK_ACCOUNT_NUMBER.Value := DBDTDirDep['BANK_ACCOUNT_NUMBER'];

        FACHFile.COMMENTS.Value := FACHFile.COMMENTS.Value + DBDTDirDep.FieldByName('CL_BANK_ACCOUNT_NBR').AsString;

        FACHFile.ABA_NUMBER.AsString := DBDTDirDep['ABA_NUMBER'];

        FACHFile.TRAN.AsString := GetAchAccountType(DBDTDirDep['BANK_ACCOUNT_TYPE']);

        WriteDetail(TRAN_TYPE_OUT);
        WriteBatchHeader(True, SegmentNbr, FOrderNbr);
        WriteBatchTotal(True, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));
      end;
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;

    {
    *** Credit ACH Offset Account ***   [1 of 3] offset batches
    // similar to [3 of 3] in EE Direct Deposits...
    }

  if (fAmount <> 0) and not IsBlockSBCredit then
  begin
    if not IsBlockDebits then
      StartBatchDefinition('CCD', EffectiveDate, sComments, BANK_REGISTER_TYPE_BillingImpound,
        BillingImpoundSB_BANK_ACCOUNTS_NBR)
    else
      StartBatchDefinition('PPD', EffectiveDate, sComments, BANK_REGISTER_TYPE_BillingImpound,
        BillingImpoundSB_BANK_ACCOUNTS_NBR);

    AddFileHeaderIfNotAddedYet;

    FOrderNbr := FACHFile.RecordCount;

    // Keep original amount
    AppendDetailRecord('', fAmount, PayrollList.BILLING_IMPOUND.AsString, BANK_REGISTER_TYPE_BillingImpound);
    AchAmount := AchAmount + fAmount;

    FACHFile.CR_AFTER_LINE.Value := 'Y';

    FACHFile.SetOrderNumber;

    FACHFile.NAME.Value := 'BILLING IMPOUND';

    // credit S/B
    if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').Value, []) then
      raise EInconsistentData.CreateHelp('BILLING_SB_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);

    Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []), 'Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString);

    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.Value <> GROUP_BOX_YES then
      FACHFile.BANK_ACCOUNT_NUMBER.Value :=
        ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
    else
      FACHFile.BANK_ACCOUNT_NUMBER.Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;


    FACHFile.ABA_NUMBER.AsString :=
      ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);

    FACHFile.ABA_NUMBER2.AsString := Origin_Bank_Aba;

    FACHFile.TRAN.AsString :=
      GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString);

    WriteDetail(TRAN_TYPE_OUT);
    WriteBatchHeader(False, SegmentNbr, FOrderNbr, OFF_DONT_USE, not IsBlockDebits, False);
    WriteBatchTotal(False, SegmentNbr, FACHFile.RecordCount, IntToStr(FACHFile.RecordCount));

  end;

end;

procedure TevCashProcessing.GenerateAchReport(aACHFile, ShowAll: Boolean);
var
  ShowFileName: String;
  ReportParams: TrwReportParams;
  P: TisProvider;
  cds: TevClientDataSet;
begin
  ReportParams := TrwReportParams.Create;
  p := TisProvider.Create(nil);
  cds := TevClientDataSet.Create(nil);
  try
    ctx_RWLocalEngine.StartGroup;
    p.Name := 'TempProvider';
    ReportParams.Add('ShowEverything', IntToStr(Integer(ShowAll)));
    ReportParams.Add('PrintAchDetail', IntToStr(Integer(aACHFile)));
    ReportParams.Add('ProcDates', ACHDescription);
    try
      if IsPayrollPeople then
      begin
        p.DataSet := ACHFile1;
        ACHFile1.First;
        cds.Data := p.Data;

        if FFileSaved then
          ShowFileName := 'NON ACH TRANSACTIONS: posted to BAR'
        else
          ShowFileName := 'NON ACH TRANSACTIONS: not yet saved';

        ReportParams.Add('FileName', ShowFileName);
        ReportParams.Add('DataSets', DataSetsToVarArray([cds]));

        ctx_RWLocalEngine.CalcPrintReport(ACH_REPORT_SY_NBR, CH_DATABASE_SYSTEM, ReportParams);
      end;

      p.DataSet := FACHFile;
      FACHFile.First;
      cds.Data := p.Data;

      ReportParams.Add('DataSets', DataSetsToVarArray([cds]));

      if FFileSaved then
        ShowFileName := 'NON ACH TRANSACTIONS: posted to BAR'
      else
        ShowFileName := 'NON ACH TRANSACTIONS: not yet saved';

      ReportParams.Add('FileName', ShowFileName);

      ctx_RWLocalEngine.CalcPrintReport(ACH_REPORT_SY_NBR, CH_DATABASE_SYSTEM, ReportParams);

    finally
      ctx_RWLocalEngine.EndGroup(rdtPreview);
    end;

  finally
    p.Free;
    cds.Free;
    ReportParams.Free;
  end;
end;

function TevCashProcessing.GetErrors: String;
begin
  Result := FErrorMessages;
end;

procedure TevCashProcessing.GenerateReport(aACHFile, ShowAll: Boolean);
begin
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('SY_REPORT_WRITER_REPORTS_NBR='+IntToStr(ACH_REPORT_SY_NBR));
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.First;
  if DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Eof then
    Exit;
  GenerateAchReport(aACHFile, ShowAll);
end;

procedure TevCashProcessing.ProcessOnePayroll(CurrentThresholdScope: String);

  procedure OnPrNbrChanged;
  begin
    cdsTaxDistr.Close;
    PrNbr := PayrollList.PR_NBR.Value;

    DIR_DEP.Close;
    DIR_DEP.ClientID := FClNbr;

    with TExecDSWrapper.Create('DirDep') do
    begin
      if BlockNegativeChecks then
        SetMacro('CustomCondition', '(l.amount >= 0 or l.amount is null) and ');
      SetParam('StatusDate', DM_PAYROLL.PR.PROCESS_DATE.AsDateTime);
      SetParam('PrNbr', PrNbr);
      SetParam('CHECK_TYPE2_REGULAR', CHECK_TYPE2_REGULAR);
      SetParam('CHECK_TYPE2_VOID', CHECK_TYPE2_VOID);
      DIR_DEP.DataRequest(AsVariant);
    end;

    DIR_DEP.Open;

    if AgencyDirDep.ValueExists(PayrollList.CL_NBR.AsString+':'+
                                PayrollList.CO_NBR.AsString+':'+
                                PayrollList.PR_NBR.AsString) then
    begin
      AGENCY_DIR_DEP.Close;
      AGENCY_DIR_DEP.EmptyDataSet;
      AGENCY_DIR_DEP.Data := AgencyDirDep.Value[PayrollList.CL_NBR.AsString+':'+
                              PayrollList.CO_NBR.AsString+':'+
                              PayrollList.PR_NBR.AsString];
    end
    else
    begin
      AGENCY_DIR_DEP.Close;
      AGENCY_DIR_DEP.ClientID := FClNbr;
      with TExecDSWrapper.Create('AgencyDirDep') do
      begin
        if BlockNegativeChecks then
          SetMacro('CustomCondition', '(m.MISCELLANEOUS_CHECK_AMOUNT >= 0 or m.MISCELLANEOUS_CHECK_AMOUNT is null) and ');
        SetParam('ProcessDate', DM_PAYROLL.PR.PROCESS_DATE.AsDateTime);
        SetParam('CoNbr', FCoNbr);
        SetParam('PrNbr', PrNbr);
        SetParam('SB_BANK_ACCOUNTS_NBR', SB_BANK_ACCOUNTS_NBR);
        AGENCY_DIR_DEP.DataRequest(AsVariant);
      end;
      AGENCY_DIR_DEP.Open;
      AgencyDirDep.AddValue(PayrollList.CL_NBR.AsString+':'+
                            PayrollList.CO_NBR.AsString+':'+
                            PayrollList.PR_NBR.AsString, AGENCY_DIR_DEP.Data);
    end;

    FTaxCreditsDateFrom := GetBeginQuarter(IncDay(GetBeginQuarter(DM_COMPANY.PR.CHECK_DATE.AsDateTime), -1));
    FTaxCreditsDateInto := DM_COMPANY.PR.CHECK_DATE.AsDateTime;

    PeriodFedTaxImpounded := FedTaxImpounded;
    FTotalTaxLiability  := GetTotalTaxLiabilities;

    if (FTotalTaxLiability <> 0) and CanProcessTaxImpound
    and not ((FTotalTaxLiability < 0) and BlockNegativeLiabilities) then
    begin
      SetDivisionFilters;
      try
        if DM_COMPANY.CO_DIVISION.RecordCount +
           DM_COMPANY.CO_BRANCH.RecordCount +
           DM_COMPANY.CO_DEPARTMENT.RecordCount +
           DM_COMPANY.CO_TEAM.RecordCount > 0 then
          ctx_PayrollCalculation.GetDistrTaxes(Prnbr, cdsTaxDistr);
      finally
        ResetDivisionFilters;
      end;
    end;

    // TRUST or OBC uses different "Debit No of Days Prior"
    if (DM_COMPANY.CO.TRUST_SERVICE.AsString <> TRUST_SERVICE_NO) or (DM_COMPANY.CO.OBC.AsString = 'Y') then
      NumberOfDaysPriorDD := DM_COMPANY.CO.DEBIT_NUMBER_DAYS_PRIOR_PR.AsInteger
    else
      NumberOfDaysPriorDD := DM_COMPANY.CO.DEBIT_NUMBER_OF_DAYS_PRIOR_DD.AsInteger;

    CheckDateDD := DM_PAYROLL.PR.CHECK_DATE.AsDateTime;
    CalculateACHDates(CheckDateDD, EffectiveDateDD, NumberOfDaysPriorDD);

  end;

  procedure OnCoNbrChanged;
  begin
    FCoNbr := PayrollList.CO_NBR.Value;
    PrNbr := -1;
    if Options.FedReserve then
    begin
      Assert(DM_SERVICE_BUREAU.SB_BANKS.LOCATE('SB_BANKS_NBR', ClBankAccounts.Result.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ', Client Bank Account ' + ClBankAccounts.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
      ORIGIN_BANK_ABA := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);
    end;
    WCImpoundSB_BANK_ACCOUNTS_NBR      := DM_COMPANY.CO.W_COMP_SB_BANK_ACCOUNT_NBR.AsInteger;
    BillingImpoundSB_BANK_ACCOUNTS_NBR := DM_COMPANY.CO.BILLING_SB_BANK_ACCOUNT_NBR.AsInteger;
    TrustImpoundSB_BANK_ACCOUNTS_NBR   := DM_COMPANY.CO.TRUST_SB_ACCOUNT_NBR.AsInteger;
    TaxImpoundSB_BANK_ACCOUNTS_NBR     := DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.AsInteger;
  end;

  procedure OnClientNumberChanged;
  begin
    FClNbr := PayrollList.CL_NBR.AsInteger;
    FCoNbr := -1;
    PrNbr := -1;
    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  function AddAllTaxesForPr: Currency;
  var
    Idx: Integer;
  begin
    Result := 0;
    if ((FTotalTaxLiability < 0) and BlockNegativeLiabilities) then
    begin
      FTotalTaxLiability := 0;
      Exit;
    end;
    if not IsPayrollPeople or (RunNumber = 2) then
    begin
      if (FTotalTaxLiability <> 0) and CanProcessTaxImpound then
      begin
        if not FPreprocess and FBelowThresholdList.Find(CurrentThresholdScope, Idx) then
        begin
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;

          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.SB_BANK_ACCOUNTS_NBR.Value       := TaxImpoundSB_BANK_ACCOUNTS_NBR;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.Value              := PayrollList.CO_PR_ACH_NBR.AsInteger;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CHECK_DATE.Value                 := PayrollList.CHECK_DATE.AsDateTime;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.TRANSACTION_EFFECTIVE_DATE.Value := EffectiveDate;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.AMOUNT.Value                     := -FTotalTaxLiability;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.REGISTER_TYPE.Value              := BANK_REGISTER_TYPE_TaxImpound;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value                := BANK_REGISTER_MANUALTYPE_None;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.PROCESS_DATE.Value               := Date;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FILLER.Value                     := PutIntoFiller('', BatchUniqID, 18, 10);
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.Value                     := BANK_REGISTER_STATUS_WriteOff;

          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
        end
        else
        begin
          Result := ProcessTaxes(FTotalTaxLiability < 0);
          CompanyTaxLiability := CompanyTaxLiability + FTotalTaxLiability;

          if Result <> 0 then
            FTotalTaxLiability := 0; // EXTREMELY important!!!

          Inc(SegmentNbr);
        end;
      end;
    end;
  end;

begin

  if FClNbr <> PayrollList.CL_NBR.AsInteger then
    OnClientNumberChanged;
  if FCoNbr <> PayrollList.CO_NBR.AsInteger then
    OnCoNbrChanged;
    
  OnPrNbrChanged;

  FDetailAdded := False;

  CO_PR_ACH_NBR := PayrollList.CO_PR_ACH_NBR.AsInteger;

  if PayrollList.CL_NBR.AsInteger <> FClNbr then
    OnClientNumberChanged;

  Assert(not DM_COMPANY.CO_PR_ACH.IsEmpty);

  // New company...
  if PayrollList.CO_NBR.Value <> FCoNbr then
    OnCoNbrChanged;

  // check of payroll exists
  if not ((PrChecks.Result.RecordCount < 1) and
          (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.RecordCount < 1) and
          (FTotalTaxLiability = 0)) then
  begin
    AchAmount := 0;
    SegmentNbr := 1;
//    if (PayrollList.DD_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
//    and (PayrollList.DD_IMPOUND.AsString <> PAYMENT_TYPE_NONE)  then
      DirectDeposits;

    if (PayrollList.BILLING_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
    and (PayrollList.BILLING_IMPOUND.AsString <> PAYMENT_TYPE_NONE) then
      BillingForAch;

    if CanProcessTrustImpound then
      TrustOrObcForAch;
    //        Tax Impound (TotalTaxLiability already calculated above for each new Pr_Nbr)
    //        Add Up all taxes: Fed, State, Local, Sui for this PR
    if CanProcessTaxImpound then
      AddAllTaxesForPr;

    if (PayrollList.WC_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
    and (PayrollList.WC_IMPOUND.AsString <> PAYMENT_TYPE_NONE)then
    begin
      if not IsPayrollPeople or (RunNumber = 2) then
      begin
        ProcessWorkersComp;
        Inc(SegmentNbr);
      end;
    end;
  end;
end;

procedure TevCashProcessing.StartBatchDefinition(aClassCode: string;
  aEffDate: TDateTime; aComments, BankRegisterType: string;
  SB_BANK_ACCOUNTS_NBR: Integer);
begin
  FACHFile.BatchEffectiveDate := aEffDate;
  FACHFile.BatchClassCode := aClassCode;
  FACHFile.BatchComments := aComments;
  BatchBANK_REGISTER_TYPE := BankRegisterType;
  BatchSB_BANK_ACCOUNTS_NBR := SB_BANK_ACCOUNTS_NBR;
  BatchUniqID := Format('%10.10d', [Random(9999999999)]);

  ClearBatchTotals;
end;

procedure TevCashProcessing.CheckThreshold(ccn, CurrentThresholdScope: String);
begin
  if FPreprocess then
  if Abs(Threshold) > 0.009 then
  if Abs(CompanyTaxLiability) > 0.009 then
  if (DM_COMPANY.CO.TAX_SERVICE.AsString = 'Y') then
  if (Abs(CompanyTaxLiability) <= Threshold) then
  begin
    FErrorMessages := FErrorMessages +
      Format('Company #%s : The total tax liability $%0.2f is below threshold of $%0.2f',
      [ccn, Abs(CompanyTaxLiability), Threshold]) + #13 + #13;
    if FPreprocess then
    begin
      FBelowThresholdList.Add(CurrentThresholdScope);
      FBelowThresholdList.Sort;
    end;
  end;

  CompanyTaxLiability := 0;
end;


procedure TevCashProcessing.AddFileHeaderIfNotAddedYet;
begin
  if not Assigned(FACHFile) then
    Exit;

  if not FileHeaderProcessed then
    AddFileHeader;
end;

function TevCashProcessing.GetFEIN: String;
begin
  if Options.UseSBEIN then
    if Options.SBEINOverride = '' then
      Result := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString
    else
      Result := Options.CoId + Options.SBEINOverride
  else
    Result := Options.CoId + PayrollList.COMPANY_FEIN.AsString;
end;

function TevCashProcessing.GetCheckDate: TDateTime;
begin
  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    Result := FACHFile.BatchEffectiveDate
  else
    Result := PayrollList.CHECK_DATE.AsDateTime;
end;

procedure TevCashProcessing.WriteBatchHeader(aMoveLast: Boolean; aSegment,
  aOrder, aIsOffset: Integer; aHidden, aCRLF: Boolean; aName: String;
  bSBMode, Prenote: Boolean; aFEIN: String; bMNSUI, bCTSUI: Boolean);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  { NOTE:
     CheckDate / Eff-date should have already been calculated before calling
     this procedure.
  }

  if aMoveLast then
    FACHFile.Last; // position to eof

//  AppendBatchHeader; // add common info

  FACHFile.Append;

  FACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  FACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  FACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  FACHFile.PR_NBR.Value                    := PrNbr;
  FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  FACHFile.AMOUNT.Value                    := 0;
  FACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  FACHFile.CUSTOM_COMPANY_NUMBER.Value := PayrollList.CUSTOM_COMPANY_NUMBER.Value;
  FACHFile.CO_NBR.Value := PayrollList.CO_NBR.Value;
  FACHFile.CL_NBR.Value := PayrollList.CL_NBR.Value;

  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FACHFile.FEIN.Value := GetFEIN;
  FACHFile.CHECK_DATE.Value := GetCheckDate;
  FACHFile.RUN_NUMBER.Value := PayrollList.RUN_NUMBER.Value;

  FACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    FACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    FACHFile.DEBITS.Value := FBatchDebits;
  FACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  FACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  FACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;
  if aFEIN <> '' then
    FACHFile.FEIN.AsString := aFEIN;
  if aName <> '' then
    FACHFile.NAME.AsString := aName
  else if not FCombining then
    if IsPayrollPeople then
      FACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      FACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      FACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if bSBMode then
    FACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      FACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      FACHFile.NAME.AsString := PayrollList.CLIENT_NAME.AsString // for 5-record
    else
      FACHFile.NAME.AsString := PayrollList.NAME.AsString;
  FACHFile.ENTRY_CLASS_CODE.AsString := FACHFile.BatchClassCode; // CCD or PPD
  FACHFile.EFFECTIVE_DATE.AsDateTime := FACHFile.BatchEffectiveDate;
  FACHFile.COMMENTS.AsString := FACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  FACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    FACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    FACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?
  // ACH File Layout
  
  if bSBMode then
    cn := ''
  else
    cn := FACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + FACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + FACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(FACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(FACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end        
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := FACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(FACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end        
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        FACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        FACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
//      if Trim(SbID) = '' then
//        raise EInconsistentData.CreateHelp('Missing CTS ID', IDH_InconsistentData);
      CompanyData := PadRight(Copy(FACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(FACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    if bMNSUI then
    begin
      CompanyData := FACHFile.NAME.AsString;
      s := s + Copy(PadRight(FACHFile.NAME.AsString, ' ', 20), 1, 20);
    end
    else if bCTSUI then
    begin
      CompanyData := FACHFile.NAME.AsString;
      s := copy(s, 1, 4) + PadRight(DM_COMPANY.CO.NAME.AsString, ' ', 16);
      s := s + Copy(PadRight(cn, ' ', 20), 1, 20);
    end
    else
    begin
      CompanyData := Copy(PadRight(Trim(cn) + ' '+ FACHFile.NAME.AsString, ' ', 20), 1, 20);
      s := s + Copy(PadRight(Trim(cn) + ' '+ FACHFile.NAME.AsString, ' ', 20), 1, 20);
    end;
  end;

  if bCTSUI then
    s := s + Copy(FACHFile.FEIN.AsString + '          ', 1, 10) + Copy(FACHFile.ENTRY_CLASS_CODE.AsString
      + '   ', 1, 3) +Copy(aName + '          ', 1, 10)
      + FormatDateTime('YYMMDD', FACHFile.CHECK_DATE.AsDateTime) +
      FormatDateTime('YYMMDD', FACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
      + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + FACHFile.BATCH_NBR.AsString
  else
    s := s + Copy(FACHFile.FEIN.AsString + '          ', 1, 10) + Copy(FACHFile.ENTRY_CLASS_CODE.AsString
      + '   ', 1, 3) + Copy(DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + FACHFile.COMMENTS.AsString + '          ', 1, 10)
      + FormatDateTime('YYMMDD', FACHFile.CHECK_DATE.AsDateTime) +
      FormatDateTime('YYMMDD', FACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
      + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + FACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  FACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := FACHFile.FEIN.AsString;
  EntryClassCode := FACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + FACHFile.COMMENTS.AsString;
//  CheckDate := FACHFile.CHECK_DATE.AsDateTime;
//  EffectiveEntryDate := FACHFile.EFFECTIVE_DATE.AsDateTime;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;
//  BatchNumber := StrToInt(FACHFile.BATCH_NBR.AsString);

  FACHFile.Post;
end;

procedure TevCashProcessing.WriteBatchTotal(aHoldTotals: Boolean; aSegment,
  aOrder: Integer; CUSTOM_EMPLOYEE_NUMBER: string; aIsOffset: Integer;
  aHidden, aCRLF: Boolean);
begin
  {
    All Batch Totals are assumed as "hidden" from the ach report in order
    to make the report appear like the current version from Rapid Pay.
  }

  // Common Info for batch totals

  FACHFile.Append;

  FACHFile.ACH_TYPE.Value                  := ACH_BATCH_TOTAL_STRING;
  FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  FACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  FACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  FACHFile.PR_NBR.Value                    := PrNbr;
  FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  FACHFile.AMOUNT.Value                    := 0;
  FACHFile.IN_PRENOTE.AsString             := 'N';

  FACHFile.CUSTOM_COMPANY_NUMBER.Value := PayrollList.CUSTOM_COMPANY_NUMBER.Value;
  FACHFile.CO_NBR.Value := PayrollList.CO_NBR.Value;
  FACHFile.CL_NBR.Value := PayrollList.CL_NBR.Value;

  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FACHFile.FEIN.Value := GetFEIN;

  FACHFile.CHECK_DATE.Value := GetCheckDate;
  FACHFile.RUN_NUMBER.Value := PayrollList.RUN_NUMBER.Value;
  FACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    FACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    FACHFile.DEBITS.Value := FBatchDebits;
  FACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);
  // reset after batch control total
  Inc(FTotNbrOfBatches);
  // increment grand totals, too
  FTotCredits := FTotCredits + FBatchCredits;
  FTotDebits := FTotDebits - FBatchDebits;

  // Custom info for batch totals
  FACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    FACHFile.HIDE_FROM_REPORT.Value := 'Y';
  if aCRLF then
    FACHFile.CR_AFTER_LINE.Value := 'Y';
  FACHFile.SEGMENT_NBR.Value := aSegment; // assigned...?
  FACHFile.ORDER_NBR.Value := aOrder;
  FACHFile.NAME.Value := PayrollList.NAME.AsString;
  FACHFile.ENTRY_CLASS_CODE.Value := FACHFile.BatchClassCode;
  FACHFile.EFFECTIVE_DATE.Value := FACHFile.BatchEffectiveDate;
  FACHFile.COMMENTS.Value := FACHFile.BatchComments;

  FACHFile.Post;

  if aHoldTotals then
  begin
    // Hold credits/debits aside for other 3 offset records
    TempCredits := FBatchCredits;
    TempDebits := FBatchDebits;
  end;

  IncrementSubTotals(aIsOffset <> OFF_DONT_USE);

  ClearBatchTotals;
end;

procedure TevCashProcessing.AppendDetailRecord(CUSTOM_EMPLOYEE_NUMBER: string;
  AAmount: Currency; PAYMENT_TYPE, BANK_REGISTER_TYPE: String);

  procedure AddToBankAccountRegister(CO_PR_ACH_NBR: Integer; BANK_REGISTER_TYPE: string;
    SB_BANK_ACCOUNTS_NBR: Integer; CHECK_DATE: TDateTime; EFFECTIVE_DATE: TDateTime; Amount: Currency; ID: string);
  begin
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.SB_BANK_ACCOUNTS_NBR.Value := SB_BANK_ACCOUNTS_NBR;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.Value := CO_PR_ACH_NBR;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CHECK_DATE.Value := CHECK_DATE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.Value := BANK_REGISTER_STATUS_Pending;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.TRANSACTION_EFFECTIVE_DATE.Value := EFFECTIVE_DATE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.AMOUNT.Value := Amount;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.REGISTER_TYPE.Value := BANK_REGISTER_TYPE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.PROCESS_DATE.Value := Date;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Filler.Value := PutIntoFiller('', ID, 18, 10);

    if PAYMENT_TYPE = PAYMENT_TYPE_EXTERNAL_CHECK then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_NonSystemCheck
    else if PAYMENT_TYPE = PAYMENT_TYPE_INTERNAL_CHECK then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_SystemCheck
    else if PAYMENT_TYPE = PAYMENT_TYPE_WIRE_TRANSFER then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_WireTransfer
    else if PAYMENT_TYPE = PAYMENT_TYPE_CASH then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_Other
    else if PAYMENT_TYPE = PAYMENT_TYPE_OTHER then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_Other;

    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
  end;

var
  s: string;
begin
//   This procedure appends a detail record

  if ReversalACH and not FCombining then
    aAmount := -aAmount;

  FACHFile.Append;

  FACHFile.ACH_TYPE.Value                  := ACH_ENTRY_DETAIL_STRING;

  if BatchSB_BANK_ACCOUNTS_NBR = 0 then
    FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := SB_BANK_ACCOUNTS_NBR
  else
    FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;

  FACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  FACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  FACHFile.PR_NBR.Value                    := PrNbr;
  FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  FACHFile.AMOUNT.Value                    := aAmount;
  FACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  FACHFile.CUSTOM_COMPANY_NUMBER.Value := PayrollList.CUSTOM_COMPANY_NUMBER.Value;
  FACHFile.CO_NBR.Value := PayrollList.CO_NBR.Value;
  FACHFile.CL_NBR.Value := PayrollList.CL_NBR.Value;

  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FACHFile.FEIN.Value := GetFEIN;

  FACHFile.CHECK_DATE.Value := GetCheckDate;
  FACHFile.RUN_NUMBER.Value := PayrollList.RUN_NUMBER.Value;
  FACHFile.BATCH_NBR.Value := ZeroFillInt64ToStr(EntryNbr + 1, 7);
  FACHFile.BATCH_ID.Value := BatchUniqID;
  if aAmount < 0 then
    FBatchDebits := FBatchDebits + AAmount
  else
    if aAmount > 0 then
    FBatchCredits := FBatchCredits + AAmount;

  if FSavingACH then
  begin
    if OverrideBatchBANK_REGISTER_TYPE <> '' then
      s := OverrideBatchBANK_REGISTER_TYPE
    else
      s := BatchBANK_REGISTER_TYPE;
    if (Length(s) > 0) and (BatchSB_BANK_ACCOUNTS_NBR > 0) then
      AddToBankAccountRegister(
        CO_PR_ACH_NBR, s,
        BatchSB_BANK_ACCOUNTS_NBR, FACHFile.CHECK_DATE.AsDateTime,
        FACHFile.BatchEffectiveDate, aAmount, BatchUniqID);
  end;

  FACHFile.COMMENTS.Value := GetCommentForNonAchTransaction(PAYMENT_TYPE);

  Inc(EntryNbr);
  Inc(FxTotNbrOfEntries);
  Inc(FTotNbrOfEntries);
  AddendaNbr := 0;

  // Don't Post in this procedure (more fields will be assigned later)
end;

procedure TevCashProcessing.WriteDetail(cTranType: Char);
var
  s: string;
  RecordTypeCode: String;
  IdentificationNumber, ReceiverName, DiscretionaryData,
  TraceNumber: String;
begin

  // if no transactions was added when processing payroll ACH, it must be marked as "Deleted"
  FDetailAdded := True;

  // ONLY write ACH_LINE... because EACH segment handles detail records differently
  // by either using ctx_DataAccess.CUSTOM_VIEW (or not).

  RecordTypeCode := '6';

  // ALL OTHER fields were previously stamped before calling this procedure!
  // Finish Calculating Transaction Code based on AMOUNT

  FACHFile.TRAN_TYPE.AsString := cTranType;
  if Length(FACHFile.TRAN.AsString) < 2 then
    FACHFile.TRAN.AsString := FACHFile.TRAN.AsString +
      GetAchTransactionType(FACHFile.IN_PRENOTE.AsString,
      FACHFile.AMOUNT.AsCurrency, 0);
  FACHFile.ENTRY_CLASS_CODE.AsString := FACHFile.BatchClassCode; // CCD or PPD
  FACHFile.EFFECTIVE_DATE.AsDateTime := FACHFile.BatchEffectiveDate;
  FACHFile.BATCH_COMMENTS.AsString := FACHFile.BatchComments;

  s := RecordTypeCode + Copy(FACHFile.TRAN.Value
    + '  ', 1, 2) + Copy(FACHFile.ABA_NUMBER.Value + '         ', 1, 9)
    + Copy(FACHFile.BANK_ACCOUNT_NUMBER.Value + '                 ', 1, 17)
    + ZeroFillInt64ToStr(RoundInt64(Abs(FACHFile.AMOUNT.Value) * 100), 10);

  IdentificationNumber := '';

  if Options.CTS then
  begin
    if FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString <> '' then
    begin
      DM_CLIENT.CL_PERSON.Activate;
      DM_EMPLOYEE.EE.Activate;
      IdentificationNumber := StringReplace(DM_EMPLOYEE.EE.Lookup('CO_NBR;CUSTOM_EMPLOYEE_NUMBER',
        VarArrayOf([DM_COMPANY.CO['CO_NBR'], FACHFile.FieldValues['CUSTOM_EMPLOYEE_NUMBER']]), 'SOCIAL_SECURITY_NUMBER'), '-', '', [rfReplaceAll]) + StringOfChar(' ', 6);
    end
    else if FACHFile.TRAN.Value <> '22' then
      IdentificationNumber := PadRight(Trim(FACHFile.CUSTOM_COMPANY_NUMBER.AsString), ' ', 13) + '01'
    else if Options.SunTrust then
      IdentificationNumber := PadRight(Trim(SBID), ' ', 13) + '01'
    else
      IdentificationNumber := '    ' + Copy(Trim(FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString) + '           ', 1, 11);
  end
  else
    IdentificationNumber := PadStringLeft(Trim(FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15);

  s := s + IdentificationNumber;
    
  ReceiverName := Copy(FACHFile.NAME.AsString + '                      ', 1, 22);

  DiscretionaryData := '  ';
    
  TraceNumber := Copy(ORIGIN_BANK_ABA + '         ', 1, 8) + ZeroFillInt64ToStr(FACHFile.BATCH_NBR.AsInteger, 7);
  s := s + ReceiverName + DiscretionaryData + IntToStr(0) + TraceNumber;
  s := UpperCase(s);

  FACHFile.ACH_LINE.AsString := s;

  FACHFile.Post;

  // for Sub-Totals
  if FACHFile.ACH_OFFSET.AsInteger = OFF_DONT_USE then
    Inc(FSubNbrOfEntries);
end;

procedure TevCashProcessing.AppendAddendaRecord(CUSTOM_EMPLOYEE_NUMBER: String;
  AAmount: Currency);
begin
//   This procedure appends an addenda record

  if ReversalACH then
    AAmount := -AAmount;

  FACHFile.Append;

  FACHFile.ACH_TYPE.Value                  := ACH_ADDENDA_RECORD_STRING;
  FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  FACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  FACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  FACHFile.PR_NBR.Value                    := PrNbr;
  FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  FACHFile.AMOUNT.Value                    := AAmount;
  FACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  FACHFile.CUSTOM_COMPANY_NUMBER.Value := PayrollList.CUSTOM_COMPANY_NUMBER.Value;
  FACHFile.CO_NBR.Value := PayrollList.CO_NBR.Value;
  FACHFile.CL_NBR.Value := PayrollList.CL_NBR.Value;

  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FACHFile.FEIN.Value := GetFEIN;

  FACHFile.CHECK_DATE.Value := GetCheckDate;
  FACHFile.RUN_NUMBER.Value := PayrollList.RUN_NUMBER.Value;
  FACHFile.BATCH_NBR.Value := ZeroFillInt64ToStr(EntryNbr + 1, 7);
  Inc(AddendaNbr);
  Inc(FxTotNbrOfEntries);
  Inc(FTotNbrOfEntries);

  // Don't Post in this procedure (more fields will be assigned later)
end;

constructor TevCashProcessing.Create;
begin
  inherited;
  DoOnConstruction;
end;

procedure TevCashProcessing.ClearGrandAndBatchTotals;
begin
  // only clear items related to grand totals
  FxTotNbrOfEntries := 0;
  FTotNbrOfBatches := 0;
  FTotCredits := 0;
  FTotDebits := 0;
  // clear Sub-totals, too
  FSubEntryHash := 0;
  FSubNbrOfBatches := 0;
  FSubNbrOfEntries := 0;
  FSubCredits := 0;
  FSubDebits := 0;
  FFileSaved := False;
  // clear items related to batch totals
  EntryNbr := 0;
  FTotNbrOfEntries := 0;
  FBatchCredits := 0;
  FBatchDebits := 0;
end;

procedure TevCashProcessing.ClearBatchTotals;
begin
  // only clear items related to batch totals
  EntryNbr := 0;
  FTotNbrOfEntries := 0;
  FBatchCredits := 0;
  FBatchDebits := 0;
end;

procedure TevCashProcessing.AddFileHeader;
begin
  // "1" File Header Record
  if Trim(Options.Header) <> '' then
  begin
    AppendFileHeaderRecord;
    FACHFile.NAME.Value := ORIGIN_Name;
    FACHFile.ORDER_NBR.Value := FACHFile.RecordCount;
    FACHFile.ACH_LINE.AsString := Options.Header;
    FACHFile.Post;
  end;

  AppendFileHeaderRecord;

  FACHFile.NAME.Value := ORIGIN_Name;
  FACHFile.ORDER_NBR.Value := FACHFile.RecordCount;
  FACHFile.ACH_LINE.AsString := UpperCase('101' + ' ' + Dest_Number
    + Origin_Number + FormatDateTime('YYMMDDHHMM', Now) + 'A094101'
    + Copy(Dest_Name + '                       ', 1, 23)
    + Copy(Origin_Name + '                       ', 1, 23)
    + ZeroFillInt64ToStr(0, 8));

  FACHFile.Post;
end;

procedure TevCashProcessing.IncrementSubTotals(bIsAch: Boolean);
begin
  if (not bIsAch) then
  begin
    Inc(FSubNbrOfBatches);
    // increment grand totals, too
    FSubCredits := FSubCredits + FBatchCredits;
    FSubDebits := FSubDebits - FBatchDebits;
  end;
end;

procedure TevCashProcessing.AppendFileHeaderRecord;
begin
  FACHFile.Append;
  FACHFile.ACH_TYPE.Value                  := ACH_FILE_HEADER_STRING;
  FACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  FACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  FACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  FACHFile.PR_NBR.Value                    := PrNbr;
  FACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  FACHFile.AMOUNT.Value                    := 0;
  FACHFile.IN_PRENOTE.AsString             := 'N';

  FACHFile.CUSTOM_COMPANY_NUMBER.AsString := '                    ';

  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FACHFile.FEIN.Value := GetFEIN;
  FACHFile.CHECK_DATE.Value := GetCheckDate;
  FACHFile.RUN_NUMBER.Value := PayrollList.RUN_NUMBER.Value;
  FACHFile.ABA_NUMBER.AsString := DEST_Number;
  FACHFile.ABA_NUMBER2.AsString := DEST_Number;
  FACHFile.EIN_NUMBER.AsString := Copy(ORIGIN_Number, 2, 9);

  FileHeaderProcessed := True;
  ClearGrandAndBatchTotals;
end;

function TevCashProcessing.CanProcessTrustImpound: Boolean;
begin
  Result := (PayrollList.TRUST_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
        and (PayrollList.TRUST_IMPOUND.AsString <> PAYMENT_TYPE_NONE);
end;

function TevCashProcessing.CanProcessTaxImpound: Boolean;
begin
  Result := (PayrollList.TAX_IMPOUND.AsString <> PAYMENT_TYPE_ACH)
        and (PayrollList.TAX_IMPOUND.AsString <> PAYMENT_TYPE_NONE);
end;

procedure TevCashProcessing.SetACHOptions(const NewACHOptions: IevACHOptions);
begin
  Options.Assign2(NewACHOptions);
end;

end.
