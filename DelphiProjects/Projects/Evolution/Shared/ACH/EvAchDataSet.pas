unit EvAchDataSet;

interface

uses  DB, Classes, SACHTypes, SysUtils, EvAchUtils, IsBaseClasses, EvClientDataSet;

type

  TevAchDataSet = class(TevClientDataSet)
  private
    function Get_ACH_TYPE: TField;
    function Get_ACH_OFFSET: TField;
    function Get_ACH_LINE: TField;
    function Get_ABA_NUMBER: TField;
    function Get_ABA_NUMBER2: TField;
    function Get_AMOUNT: TField;
    function Get_BANK_ACCOUNT_NUMBER: TField;
    function Get_BANK_REGISTER_TYPE: TField;
    function Get_BATCH_COMMENTS: TField;
    function Get_BATCH_ID: TField;
    function Get_BATCH_NBR: TField;
    function Get_CHECK_DATE: TField;
    function Get_CL_NBR: TField;
    function Get_CO_NBR: TField;
    function Get_COMMENTS: TField;
    function Get_CR_AFTER_LINE: TField;
    function Get_CREDITS: TField;
    function Get_CUSTOM_COMPANY_NUMBER: TField;
    function Get_CUSTOM_EMPLOYEE_NUMBER: TField;
    function Get_IDENTIFICATION_NUMBER: TField;
    function Get_DEBITS: TField;
    function Get_EFFECTIVE_DATE: TField;
    function Get_EIN_NUMBER: TField;
    function Get_ENTRY_CLASS_CODE: TField;
    function Get_FEIN: TField;
    function Get_IN_PRENOTE: TField;
    function Get_NAME: TField;
    function Get_ORDER_NBR: TField;
    function Get_PR_NBR: TField;
    function Get_RUN_NUMBER: TField;
    function Get_SB_BANK_ACCOUNTS_NBR: TField;
    function Get_SEGMENT_NBR: TField;
    function Get_TRAN: TField;
    function Get_TRAN_TYPE: TField;
    function Get_HIDE_FROM_REPORT: TField;
    function Get_COMB_AUX: TField;
    function Get_CO_BANK_ACCOUNT_REGISTER_NBR: TField;
    function Get_ORIGINAL_COMMENTS: TField;
    procedure CreateAchFields;
  public
    BatchClassCode: String;
    BatchEffectiveDate: TDateTime;
    BatchComments: String;

    function GetAchTextFileStrings: IisStringList;

    procedure WriteAddenda(const sInfo: String; const AAddendaNbr: Integer;
                           const AEntryNbr: Integer);
    procedure AddAchFooter(const AFooter: String; const ASegmentNbr, APrNbr: Integer);
    procedure AddDummyPadRecords(const ASubTotalRecordsCount, ASegmentNbr, APrNbr: Integer);
    function CountDummySubTotalRecords(const AHeader: String): Integer;
    function ShouldSaveCurrentLine: Boolean;

    procedure SetOrderNumber;

    constructor Create(AOwner: TComponent); override;
    property ACH_TYPE: TField read Get_ACH_TYPE;
    property ACH_LINE: TField read Get_ACH_LINE;
    property TRAN: TField read Get_TRAN;
    property BATCH_NBR: TField read Get_BATCH_NBR;
    property CUSTOM_COMPANY_NUMBER: TField read Get_CUSTOM_COMPANY_NUMBER;
    property SEGMENT_NBR: TField read Get_SEGMENT_NBR;
    property ORDER_NBR: TField read Get_ORDER_NBR;
    property PR_NBR: TField read Get_PR_NBR;
    property CHECK_DATE: TField read Get_CHECK_DATE;
    property RUN_NUMBER: TField read Get_RUN_NUMBER;
    property NAME: TField read Get_NAME;
    property CL_NBR: TField read Get_CL_NBR;
    property CO_NBR: TField read Get_CO_NBR;
    property EFFECTIVE_DATE: TField read Get_EFFECTIVE_DATE;
    property CUSTOM_EMPLOYEE_NUMBER: TField read Get_CUSTOM_EMPLOYEE_NUMBER;
    property AMOUNT: TField read Get_AMOUNT;
    property ABA_NUMBER: TField read Get_ABA_NUMBER;
    property ABA_NUMBER2: TField read Get_ABA_NUMBER2;
    property EIN_NUMBER: TField read Get_EIN_NUMBER;
    property BANK_ACCOUNT_NUMBER: TField read Get_BANK_ACCOUNT_NUMBER;
    property FEIN: TField read Get_FEIN;
    property ENTRY_CLASS_CODE: TField read Get_ENTRY_CLASS_CODE;
    property COMMENTS: TField read Get_COMMENTS;
    property CREDITS: TField read Get_CREDITS;
    property DEBITS: TField read Get_DEBITS;
    property ACH_OFFSET: TField read Get_ACH_OFFSET;
    property HIDE_FROM_REPORT: TField read Get_HIDE_FROM_REPORT;
    property CR_AFTER_LINE: TField read Get_CR_AFTER_LINE;
    property IN_PRENOTE: TField read Get_IN_PRENOTE;
    property BATCH_COMMENTS: TField read Get_BATCH_COMMENTS;
    property BATCH_ID: TField read Get_BATCH_ID;
    property TRAN_TYPE: TField read Get_TRAN_TYPE;
    property SB_BANK_ACCOUNTS_NBR: TField read Get_SB_BANK_ACCOUNTS_NBR;
    property BANK_REGISTER_TYPE: TField read Get_BANK_REGISTER_TYPE;
    property COMB_AUX: TField read Get_COMB_AUX;
    property IDENTIFICATION_NUMBER: TField read Get_IDENTIFICATION_NUMBER;
    property CO_BANK_ACCOUNT_REGISTER_NBR: TField read Get_CO_BANK_ACCOUNT_REGISTER_NBR;
    property ORIGINAL_COMMENTS: TField read Get_ORIGINAL_COMMENTS;
  end;

implementation

{ TevAchDataSet }

procedure TevAchDataSet.SetOrderNumber;
begin
  ORDER_NBR.Value := RecordCount + 1;
end;

//Separates real records and dummy sub-total records
function TevAchDataSet.ShouldSaveCurrentLine: boolean;
begin
  Result := not ( (Copy(ACH_TYPE.AsString + '  ', 1, 2) = '9:')
                   and (ACH_OFFSET.AsInteger <> OFF_DONT_USE) );
end;

function TevAchDataSet.CountDummySubTotalRecords(const AHeader: String): Integer;
begin
  //RE 39610
  Result := 0;
  // count "dummy" sub-total records
  First;
  while not Self.Eof do
  begin
    if not ShouldSaveCurrentLine then
      Result := Result + 1;
    Next;
  end;
  if Trim(AHeader) <> '' then
    Result := Result + 1;
end;

procedure TevAchDataSet.AddDummyPadRecords(const ASubTotalRecordsCount, ASegmentNbr, APrNbr: Integer);
begin
  while (RecordCount - ASubTotalRecordsCount) mod 10 <> 0 do
  begin
    Append;
    CL_NBR.Value := MaxInt;
    ACH_TYPE.Value := '99:End-Of-File';
    SEGMENT_NBR.AsInteger := ASegmentNbr;
    ORDER_NBR.Value := RecordCount;
    CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
    PR_NBR.Value := APrNbr;
    CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(RecordCount + 1);
    ACH_LINE.Value := sDummy;
    Post;
  end;
end;

procedure TevAchDataSet.AddAchFooter(const AFooter: String; const ASegmentNbr, APrNbr: Integer);
begin
  if Trim(AFooter) <> '' then
  begin
    Append;
    CL_NBR.Value := MaxInt;
    ACH_TYPE.Value := '99:End-Of-File';
    SEGMENT_NBR.AsInteger := ASegmentNbr;
    ORDER_NBR.Value := RecordCount;
    CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
    PR_NBR.Value := APrNbr;
    CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(RecordCount + 1);
    ACH_LINE.AsString := AFooter;
    Post;
  end;
end;

procedure TevAchDataSet.WriteAddenda(const sInfo: String;
  const AAddendaNbr: Integer; const AEntryNbr: Integer);
begin
  TRAN_TYPE.AsString := TRAN_TYPE_OUT;
  TRAN.AsString := TRAN.AsString +
    GetAchTransactionType(IN_PRENOTE.AsString,
      AMOUNT.AsCurrency);
  ENTRY_CLASS_CODE.AsString := BatchClassCode; // CCD or PPD
  EFFECTIVE_DATE.AsDateTime := BatchEffectiveDate;
  BATCH_COMMENTS.AsString := BatchComments;

  ACH_LINE.AsString := Format('705%-80.80s', [sInfo]) +
          ZeroFillIntToStr(AAddendaNbr, 4) + ZeroFillIntToStr(AEntryNbr, 7);
  Post;
end;

function TevAchDataSet.Get_ABA_NUMBER: TField;
begin
  Result := FieldByName('ABA_NUMBER');
end;

function TevAchDataSet.Get_ABA_NUMBER2: TField;
begin
  Result := FieldByName('ABA_NUMBER2');
end;

function TevAchDataSet.Get_ACH_LINE: TField;
begin
  Result := FieldByName('ACH_LINE');
end;

function TevAchDataSet.Get_ACH_OFFSET: TField;
begin
  Result := FieldByName('ACH_OFFSET');
end;

function TevAchDataSet.Get_ACH_TYPE: TField;
begin
  Result := FieldByName('ACH_TYPE');
end;

function TevAchDataSet.Get_AMOUNT: TField;
begin
  Result := FieldByName('AMOUNT');
end;

function TevAchDataSet.Get_BANK_ACCOUNT_NUMBER: TField;
begin
  Result := FieldByName('BANK_ACCOUNT_NUMBER');
end;

function TevAchDataSet.Get_BANK_REGISTER_TYPE: TField;
begin
  Result := FieldByName('BANK_REGISTER_TYPE');
end;

function TevAchDataSet.Get_BATCH_COMMENTS: TField;
begin
  Result := FieldByName('BATCH_COMMENTS');
end;

function TevAchDataSet.Get_BATCH_ID: TField;
begin
  Result := FieldByName('BATCH_ID');
end;

function TevAchDataSet.Get_BATCH_NBR: TField;
begin
  Result := FieldByName('BATCH_NBR');
end;

function TevAchDataSet.Get_CHECK_DATE: TField;
begin
  Result := FieldByName('CHECK_DATE');
end;

function TevAchDataSet.Get_CL_NBR: TField;
begin
  Result := FieldByName('CL_NBR');
end;

function TevAchDataSet.Get_CO_NBR: TField;
begin
  Result := FieldByName('CO_NBR');
end;

function TevAchDataSet.Get_COMMENTS: TField;
begin
  Result := FieldByName('COMMENTS');
end;

function TevAchDataSet.Get_CR_AFTER_LINE: TField;
begin
  Result := FieldByName('CR_AFTER_LINE');
end;

function TevAchDataSet.Get_CREDITS: TField;
begin
  Result := FieldByName('CREDITS');
end;

function TevAchDataSet.Get_CUSTOM_COMPANY_NUMBER: TField;
begin
  Result := FieldByName('CUSTOM_COMPANY_NUMBER');
end;

function TevAchDataSet.Get_CUSTOM_EMPLOYEE_NUMBER: TField;
begin
  Result := FieldByName('CUSTOM_EMPLOYEE_NUMBER');
end;

function TevAchDataSet.Get_DEBITS: TField;
begin
  Result := FieldByName('DEBITS');
end;

function TevAchDataSet.Get_EFFECTIVE_DATE: TField;
begin
  Result := FieldByName('EFFECTIVE_DATE');
end;

function TevAchDataSet.Get_EIN_NUMBER: TField;
begin
  Result := FieldByName('EIN_NUMBER');
end;

function TevAchDataSet.Get_ENTRY_CLASS_CODE: TField;
begin
  Result := FieldByName('ENTRY_CLASS_CODE');
end;

function TevAchDataSet.Get_FEIN: TField;
begin
  Result := FieldByName('FEIN');
end;

function TevAchDataSet.Get_IN_PRENOTE: TField;
begin
  Result := FieldByName('IN_PRENOTE');
end;

function TevAchDataSet.Get_NAME: TField;
begin
  Result := FieldByName('NAME');
end;

function TevAchDataSet.Get_ORDER_NBR: TField;
begin
  Result := FieldByName('ORDER_NBR');
end;

function TevAchDataSet.Get_PR_NBR: TField;
begin
  Result := FieldByName('PR_NBR');
end;

function TevAchDataSet.Get_RUN_NUMBER: TField;
begin
  Result := FieldByName('RUN_NUMBER');
end;

function TevAchDataSet.Get_SB_BANK_ACCOUNTS_NBR: TField;
begin
  Result := FieldByName('SB_BANK_ACCOUNTS_NBR');
end;

function TevAchDataSet.Get_SEGMENT_NBR: TField;
begin
  Result := FieldByName('SEGMENT_NBR');
end;

function TevAchDataSet.Get_TRAN: TField;
begin
  Result := FieldByName('TRAN');
end;

function TevAchDataSet.Get_TRAN_TYPE: TField;
begin
  Result := FieldByName('TRAN_TYPE');
end;

function TevAchDataSet.Get_HIDE_FROM_REPORT: TField;
begin
  Result := FieldByName('HIDE_FROM_REPORT');
end;

function TevAchDataSet.Get_COMB_AUX: TField;
begin
  Result := FieldByName('COMB_AUX');
end;

function TevAchDataSet.Get_IDENTIFICATION_NUMBER: TField;
begin
  Result := FieldByName('IDENTIFICATION_NUMBER');
end;

function TevAchDataSet.Get_CO_BANK_ACCOUNT_REGISTER_NBR: TField;
begin
  Result := FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR');
end;

procedure TevAchDataSet.CreateAchFields;
begin
  FieldDefs.Add('CL_NBR',                 ftInteger    );
  FieldDefs.Add('CO_NBR',                 ftInteger    );
  FieldDefs.Add('ACH_TYPE',               ftString,  15);
  FieldDefs.Add('ACH_LINE',               ftString,  94);
  FieldDefs.Add('TRAN',                   ftString,   3);
  FieldDefs.Add('BATCH_NBR',              ftString,   7);
  FieldDefs.Add('CUSTOM_COMPANY_NUMBER',  ftString,  20);
  FieldDefs.Add('SEGMENT_NBR',            ftInteger    );
  FieldDefs.Add('ORDER_NBR',              ftInteger    );
  FieldDefs.Add('PR_NBR',                 ftInteger    );
  FieldDefs.Add('CHECK_DATE',             ftDateTime   );
  FieldDefs.Add('RUN_NUMBER',             ftInteger    );
  FieldDefs.Add('NAME',                   ftString,  22);
  FieldDefs.Add('EFFECTIVE_DATE',         ftDateTime   );
  FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString,  15);
  FieldDefs.Add('AMOUNT',                 ftCurrency   );
  FieldDefs.Add('ABA_NUMBER',             ftString,   9);
  FieldDefs.Add('ABA_NUMBER2',            ftString,   9);
  FieldDefs.Add('EIN_NUMBER',             ftString,   9);
  FieldDefs.Add('BANK_ACCOUNT_NUMBER',    ftString,  20);
  FieldDefs.Add('FEIN',                   ftString,  10);
  FieldDefs.Add('ENTRY_CLASS_CODE',       ftString,   3);
  FieldDefs.Add('COMMENTS',               ftString,  10);
  FieldDefs.Add('CREDITS',                ftCurrency   );
  FieldDefs.Add('DEBITS',                 ftCurrency   );
  FieldDefs.Add('ACH_OFFSET',             ftInteger    );
  FieldDefs.Add('HIDE_FROM_REPORT',       ftString,   1);
  FieldDefs.Add('CR_AFTER_LINE',          ftString,   1);
  FieldDefs.Add('IN_PRENOTE',             ftString,   1);
  FieldDefs.Add('BATCH_COMMENTS',         ftString,  10);
  FieldDefs.Add('BATCH_ID',               ftString,  10);
  FieldDefs.Add('TRAN_TYPE',              ftString,   1);
  FieldDefs.Add('SB_BANK_ACCOUNTS_NBR',   ftInteger    );
  FieldDefs.Add('BANK_REGISTER_TYPE',     ftString,   1);
  FieldDefs.Add('COMB_AUX',               ftString,   1);
  FieldDefs.Add('IDENTIFICATION_NUMBER',  ftString,   15);
  FieldDefs.Add('CO_BANK_ACCOUNT_REGISTER_NBR', ftInteger);
  FieldDefs.Add('ORIGINAL_COMMENTS',      ftString,   10);
  FieldDefs.Add('AMOUNT_AUX',             ftCurrency    );
  CreateDataSet;
  AddIndex('By_CoPrSegOrd', 'custom_company_number;cl_nbr;pr_nbr;segment_nbr;order_nbr;',
      [ixCaseInsensitive], '', '', 0);
end;

constructor TevAchDataSet.Create(AOwner: TComponent);
begin
  inherited;
  CreateAchFields;
end;

function TevAchDataSet.GetAchTextFileStrings: IisStringList;
begin
  Result := TisStringList.Create;
  First;
  while not Eof do
  begin
    if ShouldSaveCurrentLine then
      Result.Add(ACH_LINE.AsString + StringOfChar(' ', 94 - Length(ACH_LINE.AsString)));
    Next;
  end;
end;

function TevAchDataSet.Get_ORIGINAL_COMMENTS: TField;
begin
  Result := FieldByName('ORIGINAL_COMMENTS');
end;

end.

