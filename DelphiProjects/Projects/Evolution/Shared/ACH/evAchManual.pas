// Copyright � 2000-2008 iSystems LLC. All rights reserved.
unit evAchManual;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvTypes, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents, EvContext,
  ISDataAccessComponents, EvAchUtils, EvExceptions, isBaseClasses, evAchDataset,
  SACHTypes, StrUtils, Variants, EvCommonInterfaces, SReportSettings,
  EvStreamUtils, EvClasses, EvDataset;

type
  IevManualAchTransaction = Interface
  ['{A28C436A-6C26-49F2-AC70-0310FFE6480D}']
    function Get_Addenda: String;
    procedure Set_Addenda(Value: String);
    function Get_CL_NBR: Integer;
    procedure Set_CL_NBR(Value: Integer);
    function Get_CO_NBR: Integer;
    procedure Set_CO_NBR(Value: Integer);
    function Get_CHECK_DATE: TDateTime;
    procedure Set_CHECK_DATE(Value: TDateTime);
    function Get_SB_BANK_ACCOUNTS_NBR: Integer;
    procedure Set_SB_BANK_ACCOUNTS_NBR(Value: Integer);
    function Get_EffectiveDate: TDateTime;
    procedure Set_EffectiveDate(Value: TDateTime);
    function Get_Amount: Currency;
    procedure Set_Amount(Value: Currency);
    function Get_ClientName: String;
    procedure Set_ClientName(Value: String);
    function Get_PrintClientName: String;
    procedure Set_PrintClientName(Value: String);
    function Get_CompanyLegalName: String;
    procedure Set_CompanyLegalName(Value: String);
    function Get_CO_MANUAL_ACH_NBR: Integer;
    procedure Set_CO_MANUAL_ACH_NBR(Value: Integer);
    function Get_ClassCode: String;
    procedure Set_ClassCode(Value: String);
    function Get_BankAccountRegisterType: String;
    procedure Set_BankAccountRegisterType(Value: String);
    function Get_CustomEmployeeNumber: String;
    procedure Set_CustomEmployeeNumber(Value: String);
    function Get_IdentificationNumber: String;
    procedure Set_IdentificationNumber(Value: String);
    function Get_CustomCompanyNumber: String;
    procedure Set_CustomCompanyNumber(Value: String);
    function Get_OriginBankAba: String;
    procedure Set_OriginBankAba(Value: String);
    function Get_DestNumber: String;
    procedure Set_DestNumber(Value: String);
    function Get_DestName: String;
    procedure Set_DestName(Value: String);
    function Get_OriginName: String;
    procedure Set_OriginName(Value: String);
    function Get_OriginNumber: String;
    procedure Set_OriginNumber(Value: String);
    function Get_CompanyName: String;
    procedure Set_CompanyName(Value: String);
    function Get_FEIN: String;
    procedure Set_FEIN(Value: String);
    function Get_Name: String;
    procedure Set_Name(Value: String);
    function Get_ABA_NUMBER: String;
    procedure Set_ABA_NUMBER(Value: String);
    function Get_BANK_ACCOUNT_NUMBER: String;
    procedure Set_BANK_ACCOUNT_NUMBER(Value: String);
    function Get_AchAccountType: String;
    procedure Set_AchAccountType(Value: String);
    function Get_TranType: String;
    procedure Set_TranType(Value: String);
    function Get_Comments: String;
    procedure Set_Comments(Value: String);
    function Get_BatchUniqID: Integer;
    procedure Set_BatchUniqID(Value: Integer);
    function Get_Exception: String;
    procedure Set_Exception(Value: String);
    procedure CopyFrom(ATransaction: IevManualAchTransaction);
    property CL_NBR: Integer read Get_CL_NBR write Set_CL_NBR;
    property CO_NBR: Integer read Get_CO_NBR write Set_CO_NBR;
    property CHECK_DATE: TDateTime read Get_CHECK_DATE write  Set_CHECK_DATE;
    property SB_BANK_ACCOUNTS_NBR: Integer read Get_SB_BANK_ACCOUNTS_NBR write Set_SB_BANK_ACCOUNTS_NBR;
    property EffectiveDate: TDateTime read Get_EffectiveDate write Set_EffectiveDate;
    property Amount: Currency read Get_Amount write Set_Amount;
    property ClientName: String read Get_ClientName write Set_ClientName;
    property PrintClientName: String read Get_PrintClientName write Set_PrintClientName;
    property CompanyLegalName: String read Get_CompanyLegalName write Set_CompanyLegalName;
    property CO_MANUAL_ACH_NBR: Integer read Get_CO_MANUAL_ACH_NBR write Set_CO_MANUAL_ACH_NBR;
    property ClassCode: String read Get_ClassCode write Set_ClassCode;
    property BankAccountRegisterType: String read Get_BankAccountRegisterType write Set_BankAccountRegisterType;
    property CustomEmployeeNumber: String read Get_CustomEmployeeNumber write Set_CustomEmployeeNumber;
    property IdentificationNumber: String read Get_IdentificationNumber write Set_IdentificationNumber;
    property CustomCompanyNumber: String read Get_CustomCompanyNumber write Set_CustomCompanyNumber;
    property OriginBankAba: String read Get_OriginBankAba write Set_OriginBankAba;
    property DestNumber: String read Get_DestNumber write Set_DestNumber;
    property DestName: String read Get_DestName write Set_DestName;
    property OriginName: String read Get_OriginName write Set_OriginName;
    property OriginNumber: String read Get_OriginNumber write Set_OriginNumber;
    property CompanyName: String read Get_CompanyName write Set_CompanyName;
    property FEIN: String read Get_FEIN write Set_FEIN;
    property ABA_NUMBER: String read Get_ABA_NUMBER write Set_ABA_NUMBER;
    property BANK_ACCOUNT_NUMBER: String read Get_BANK_ACCOUNT_NUMBER write Set_BANK_ACCOUNT_NUMBER;
    property AchAccountType: String read Get_AchAccountType write Set_AchAccountType;
    property TranType: String read Get_TranType write Set_TranType;
    property BatchUniqID: Integer read Get_BatchUniqID write Set_BatchUniqID;
    property Comments: String read Get_Comments write Set_Comments;
    property Name: String read Get_Name write Set_Name;
    property Exception: String read Get_Exception write Set_Exception;
    property Addenda: String read Get_Addenda write Set_Addenda;
  end;

type
  TevManualAchTransaction = class(TisInterfacedObject, IevManualAchTransaction)
    FValues: IisListOfValues;
    function Get_Addenda: String;
    procedure Set_Addenda(Value: String);
    function Get_CL_NBR: Integer;
    procedure Set_CL_NBR(Value: Integer);
    function Get_CO_NBR: Integer;
    procedure Set_CO_NBR(Value: Integer);
    function Get_CHECK_DATE: TDateTime;
    procedure Set_CHECK_DATE(Value: TDateTime);
    function Get_SB_BANK_ACCOUNTS_NBR: Integer;
    procedure Set_SB_BANK_ACCOUNTS_NBR(Value: Integer);
    function Get_EffectiveDate: TDateTime;
    procedure Set_EffectiveDate(Value: TDateTime);
    function Get_Amount: Currency;
    procedure Set_Amount(Value: Currency);
    function Get_ClientName: String;
    procedure Set_ClientName(Value: String);
    function Get_PrintClientName: String;
    procedure Set_PrintClientName(Value: String);
    function Get_CompanyLegalName: String;
    procedure Set_CompanyLegalName(Value: String);
    function Get_CO_MANUAL_ACH_NBR: Integer;
    procedure Set_CO_MANUAL_ACH_NBR(Value: Integer);
    function Get_ClassCode: String;
    procedure Set_ClassCode(Value: String);
    function Get_BankAccountRegisterType: String;
    procedure Set_BankAccountRegisterType(Value: String);
    function Get_CustomEmployeeNumber: String;
    procedure Set_CustomEmployeeNumber(Value: String);
    function Get_IdentificationNumber: String;
    procedure Set_IdentificationNumber(Value: String);
    function Get_CustomCompanyNumber: String;
    procedure Set_CustomCompanyNumber(Value: String);
    function Get_OriginBankAba: String;
    procedure Set_OriginBankAba(Value: String);
    function Get_DestNumber: String;
    procedure Set_DestNumber(Value: String);
    function Get_DestName: String;
    procedure Set_DestName(Value: String);
    function Get_OriginName: String;
    procedure Set_OriginName(Value: String);
    function Get_OriginNumber: String;
    procedure Set_OriginNumber(Value: String);
    function Get_CompanyName: String;
    procedure Set_CompanyName(Value: String);
    function Get_FEIN: String;
    procedure Set_FEIN(Value: String);
    function Get_Name: String;
    procedure Set_Name(Value: String);
    function Get_ABA_NUMBER: String;
    procedure Set_ABA_NUMBER(Value: String);
    function Get_BANK_ACCOUNT_NUMBER: String;
    procedure Set_BANK_ACCOUNT_NUMBER(Value: String);
    function Get_AchAccountType: String;
    procedure Set_AchAccountType(Value: String);
    function Get_TranType: String;
    procedure Set_TranType(Value: String);
    function Get_Comments: String;
    procedure Set_Comments(Value: String);
    function Get_BatchUniqID: Integer;
    procedure Set_BatchUniqID(Value: Integer);
    function Get_Exception: String;
    procedure Set_Exception(Value: String);
    procedure CopyFrom(ATransaction: IevManualAchTransaction);
    procedure DoOnConstruction; override;
    property CL_NBR: Integer read Get_CL_NBR write Set_CL_NBR;
    property CO_NBR: Integer read Get_CO_NBR write Set_CO_NBR;
    property CHECK_DATE: TDateTime read Get_CHECK_DATE write  Set_CHECK_DATE;
    property SB_BANK_ACCOUNTS_NBR: Integer read Get_SB_BANK_ACCOUNTS_NBR write Set_SB_BANK_ACCOUNTS_NBR;
    property EffectiveDate: TDateTime read Get_EffectiveDate write Set_EffectiveDate;
    property Amount: Currency read Get_Amount write Set_Amount;
    property ClientName: String read Get_ClientName write Set_ClientName;
    property PrintClientName: String read Get_PrintClientName write Set_PrintClientName;
    property CompanyLegalName: String read Get_CompanyLegalName write Set_CompanyLegalName;
    property CO_MANUAL_ACH_NBR: Integer read Get_CO_MANUAL_ACH_NBR write Set_CO_MANUAL_ACH_NBR;
    property ClassCode: String read Get_ClassCode write Set_ClassCode;
    property BankAccountRegisterType: String read Get_BankAccountRegisterType write Set_BankAccountRegisterType;
    property CustomEmployeeNumber: String read Get_CustomEmployeeNumber write Set_CustomEmployeeNumber;
    property IdentificationNumber: String read Get_IdentificationNumber write Set_IdentificationNumber;
    property CustomCompanyNumber: String read Get_CustomCompanyNumber write Set_CustomCompanyNumber;
    property OriginBankAba: String read Get_OriginBankAba write Set_OriginBankAba;
    property DestNumber: String read Get_DestNumber write Set_DestNumber;
    property DestName: String read Get_DestName write Set_DestName;
    property OriginName: String read Get_OriginName write Set_OriginName;
    property OriginNumber: String read Get_OriginNumber write Set_OriginNumber;
    property CompanyName: String read Get_CompanyName write Set_CompanyName;
    property FEIN: String read Get_FEIN write Set_FEIN;
    property ABA_NUMBER: String read Get_ABA_NUMBER write Set_ABA_NUMBER;
    property BANK_ACCOUNT_NUMBER: String read Get_BANK_ACCOUNT_NUMBER write Set_BANK_ACCOUNT_NUMBER;
    property AchAccountType: String read Get_AchAccountType write Set_AchAccountType;
    property TranType: String read Get_TranType write Set_TranType;
    property BatchUniqID: Integer read Get_BatchUniqID write Set_BatchUniqID;
    property Comments: String read Get_Comments write Set_Comments;
    property Name: String read Get_Name write Set_Name;
    property Exception: String read Get_Exception write Set_Exception;
  end;


type
  TevAchManual = class
  private
    FExceptions: String;
    FOptions: TACHOptions;
    FACHFile: TevAchDataSet;
    FACHFiles: IisListOfValues;
    FFileName: String;
    FSBID: String;
    function GetTransactions: IisList;
    procedure PatchAchFiles(const AchSave: IisListOfValues);
    procedure ExportSingleAchFile(const ATransactions: IisList);
    procedure CreateReportsAndAchFiles(const ATransactions: IisList);
    procedure UpdateStatus(const ATransactions: IisList);
    function GetBatchName(ATransaction: IevManualAchTransaction): String;
    function GetBatchCompanyName(ATransaction: IevManualAchTransaction; ABatchUniqID: Int64): String;
    function GetCompanyData(ATransaction: IevManualAchTransaction; ABatchUniqID: Int64): String;
  protected
    procedure SetFileName(const AValue: String);
  public
    ACHFolder: String;
    OverrideBatchBANK_REGISTER_TYPE: String;
    Trans: TEvBasicClientDataSet;
    constructor Create;
    destructor Destroy; override;
    procedure Process;
    procedure Preprocess;
    procedure SetACHOptions(const NewACHOptions: IevACHOptions);
    property ACHFile: TevAchDataSet read FACHFile;
    property ACHFiles: IisListOfValues read FACHFiles;
    property FileName: String read FFileName write SetFileName;
    property Exceptions: String read FExceptions;
  end;

implementation

uses
  EvUtils, EvConsts, SDataStructure;

{ TevAchManual }

function PadRight(const S: String; Len: Integer): String;
begin
  Result := copy(S + StringOfChar(' ', Len), 1, Len);
end;

function PadLeft(const S: String; Len: Integer): String;
begin
  Result := StringOfChar(' ', Len) + S;
  Result := copy(Result, Length(Result) - Len + 1, Len);
end;

function RemovePunctuation(const Value: String): String;
var
  i: Integer;
begin
  Result := Value;
  i := 1;
  while i <= Length(Result) do
  begin
    if Result[i] in [' ',',','''','"','-','.'] then
      System.Delete(Result, i, 1)
    else
      Inc(i);
  end;
end;

procedure TevAchManual.Preprocess;
var
  Transactions: IisList;
begin
  Transactions := GetTransactions;
  if Transactions.Count > 0 then
    CreateReportsAndAchFiles(Transactions);
end;

function TevAchManual.GetBatchName(ATransaction: IevManualAchTransaction): String;
begin
  if IsPayrollPeople then
    Result := ATransaction.CompanyLegalName
  else if ATransaction.PrintClientName = GROUP_BOX_YES then
    Result := ATransaction.ClientName
  else
    Result := ATransaction.CompanyName;
end;

function TevAchManual.GetBatchCompanyName(ATransaction: IevManualAchTransaction; ABatchUniqID: Int64): String;
begin
  if IsPayrollPeople then
    Result := PadRight(GetBatchName(ATransaction), 16)
  else if FOptions.SunTrust then
    Result := GetBatchName(ATransaction)
  else
  begin
    if FOptions.BankOne then
      Result := copy(Copy(ATransaction.CustomCompanyNumber+ '        ', 1, 8) + ' ' +
        GetBatchName(ATransaction) + '                ', 1, 16)
    else
      Result := copy(Copy(ATransaction.CustomCompanyNumber+ '     ', 1, 5) + '*' +
        IntToStr(ABatchUniqID) + '                ', 1, 16);
  end;
  Result := AnsiUpperCase(Result);
end;

function TevAchManual.GetCompanyData(ATransaction: IevManualAchTransaction; ABatchUniqID: Int64): String;
begin
  if IsPayrollPeople then
  begin
    Result := copy(Copy(ATransaction.CustomCompanyNumber+ '     ', 1, 6) + '*' +
          IntToStr(ABatchUniqID) + '                ', 1, 20);
  end
  else if FOptions.SunTrust then
  begin
    Result := copy(Copy(ATransaction.CustomCompanyNumber+ '        ', 1, 9) + '*' +
          IntToStr(ABatchUniqID) + '                ', 1, 20);
  end
  else
  begin
    if FOptions.CTS or FOptions.Intercept then
      Result := PadRight(Copy(GetBatchName(ATransaction), 1, 8), 8) +
        PadRight(Copy(ATransaction.CustomCompanyNumber, 1, 6), 6) +
        PadRight(FSBID, 6)
    else
      Result := copy(PadRight(Trim(ATransaction.CustomCompanyNumber) + ' '+
        ATransaction.CompanyName, 20), 1, 20);
  end;
  Result := AnsiUpperCase(Result);
end;

function TevAchManual.GetTransactions: IisList;
var
  Transactions: IisList;
  Transaction: IevManualAchTransaction;
  DestNumber, DestName, OriginBankAba, OriginName, Origin_Number: String;
  CustomHeader, tmp, BankAccountNumber, FEIN: String;
  n: Integer;
  OriginBankNbr: Integer;

  function GetIdentificationNumber: String;
  begin
    Result := '';
    if FOptions.CTS then
    begin
      if ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString <> '' then
      begin
        DM_CLIENT.CL_PERSON.Activate;
        DM_EMPLOYEE.EE.Activate;
        Result := StringReplace(DM_EMPLOYEE.EE.Lookup('CO_NBR;CUSTOM_EMPLOYEE_NUMBER',
          VarArrayOf([DM_COMPANY.CO['CO_NBR'], ACHFile.FieldValues['CUSTOM_EMPLOYEE_NUMBER']]), 'SOCIAL_SECURITY_NUMBER'), '-', '', [rfReplaceAll]) + StringOfChar(' ', 6);
      end
      else if ACHFile.TRAN.AsString <> '22' then
        Result := PadRight(Trim(ACHFile.CUSTOM_COMPANY_NUMBER.AsString), 13) + '01'
      else if FOptions.SunTrust then
        Result := PadRight(Trim(FSBID), 13) + '01'
      else
        Result := '    ' + copy(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString) + '           ', 1, 11);
    end
    else
      begin
        Result := PadStringLeft(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15);
      end;
  end;

  function GetRegisterType: String;
  begin
    if Length(OverrideBatchBANK_REGISTER_TYPE) > 0 then
      Result := OverrideBatchBANK_REGISTER_TYPE
    else
      Result := BANK_REGISTER_TYPE_Manual;
  end;

  procedure GetCompanyInfo;
  var
    i: Integer;
  begin
    OriginBankNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.ACH_ORIGIN_SB_BANKS_NBR.AsInteger;
    DestName := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', OriginBankNbr, 'PRINT_NAME');
    DestNumber := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', OriginBankNbr, 'ABA_NUMBER');
    if FOptions.FedReserve then
    begin
      if not DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO['DD_CL_BANK_ACCOUNT_NBR'], []) then
        raise EInconsistentData.CreateHelp('DD_CL_BANK_ACCOUNT_NBR is missing for company ' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

      Assert(DM_SERVICE_BUREAU.SB_BANKS.LOCATE('SB_BANKS_NBR', DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('SB_BANKS_NBR').Value, []), 'Problem with client bank account, Company ' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ', Client Bank Account ' + DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
      OriginBankAba := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.FieldByName('ABA_NUMBER').AsString);
    end
    else
      OriginBankAba := copy(ACHTrim(DestNumber), 1, 8);
    Origin_Number := PadLeft(DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString, 10);
    CustomHeader := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_HEADER_RECORD.AsString;
    OriginName := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;

    i := 0;
    n := 1;
    tmp := '';
    while i < Length(CustomHeader) do
    begin
      Inc(i);
      if CustomHeader[i] <> '*' then
        tmp := tmp + CustomHeader[i];
      if (CustomHeader[i] = '*') or (i = Length(CustomHeader)) then
      begin
        if tmp <> '' then
        begin
          if n = 1 then
            DestNumber := copy(PadLeft(tmp, 9),1,9)
          else
          if n = 2 then
            Origin_Number := copy(PadLeft(tmp, 10),1,10)
          else
          if n = 3 then
            DestName := tmp
          else
          if n = 4 then
            OriginName := tmp;
          tmp := '';
        end;
        Inc(n);
      end;
    end;
  end;


  function ConvertSSN(SSN: string): string;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Length(SSN) do
      if SSN[i] in ['0'..'9'] then
        Result := Result + SSN[i];
  end;

var
  AGENCY_NAME, Addenda, MedIns, TmpStr: String;
  A: IevQuery;  
begin
  if Length(FOptions.CoId) = 0 then
    FOptions.CoId := '1'
  else if FOptions.CoId = '@' then
    FOptions.CoId := ' ';

  Transactions := TisList.Create;
  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');
  DM_EMPLOYEE.CL_BANK_ACCOUNT.DataRequired('ALL');
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  FSBID := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.ACH_ORIGIN_SB_BANKS_NBR.AsInteger, 'CtsId');
  Trans.IndexFieldNames := 'CL_NBR;CO_NBR';
  Trans.First;
  while not Trans.Eof do
  begin
    try
      if not DM_COMPANY.CO.Active
      or (Trans.FieldByName('CO_NBR').AsInteger <> DM_COMPANY.CO.CO_NBR.AsInteger)
      or (Trans.FieldByName('CL_NBR').AsInteger <> ctx_DBAccess.CurrentClientNbr) then
      begin
        if Trans.FieldByName('CL_NBR').AsInteger <> ctx_DBAccess.CurrentClientNbr then
        begin
          ctx_DataAccess.OpenClient(Trans.FieldByName('CL_NBR').AsInteger);
          DM_EMPLOYEE.CL_BANK_ACCOUNT.DataRequired('ALL');
          DM_EMPLOYEE.CL_PERSON.DataRequired('ALL');
          DM_COMPANY.CO.DataRequired('ALL');
          DM_CLIENT.CL_PERSON.Activate;
        end;
        DM_COMPANY.CO.Locate('CO_NBR', Trans.FieldByName('CO_NBR').Value, []);
        if not DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.ACH_SB_BANK_ACCOUNT_NBR.Value, []) then
          raise EInconsistentData.CreateHelp('SB Bank Account not found for company #' + DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString, IDH_InconsistentData);
        DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + Trans.FieldByName('CO_NBR').AsString);
        DM_PAYROLL.PR.DataRequired('CO_NBR=' + Trans.FieldByName('CO_NBR').AsString);
        if FOptions.UseSBEIN then
        begin
          if FOptions.SBEINOverride = '' then
            FEIN := FOptions.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString
          else
            FEIN := FOptions.CoId + FOptions.SBEINOverride;
        end
        else
          FEIN := FOptions.CoId + DM_COMPANY.CO.FEIN.AsString;
        GetCompanyInfo;
      end;
      if not (FOptions.BlockDebits or IsPayrollPeople) then
      begin
        if not Trans.FieldByName('FROM_EE_DIRECT_DEPOSIT_NBR').IsNull then
        begin
          DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_DIRECT_DEPOSIT_NBR=' +
            Trans.FieldByName('FROM_EE_DIRECT_DEPOSIT_NBR').AsString);
          if DM_EMPLOYEE.EE.Locate('EE_NBR', DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_NBR.Value, []) then
          begin
            Transaction := TevManualAchTransaction.Create;
            Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
            Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
            Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
            Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
            Transaction.ClassCode               := 'CCD';
            Transaction.EffectiveDate           := Trans.FieldByName('DEBIT_DATE').AsDateTime;
            Transaction.Comments                := 'MAN DEBIT';
            Transaction.BankAccountRegisterType := GetRegisterType;
            Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').Value, 0);
            Transaction.Amount                  := -Trans.FieldByName('AMOUNT').AsCurrency;
            Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
            Transaction.IdentificationNumber    := GetIdentificationNumber;
            Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
            Transaction.OriginBankAba           := OriginBankAba;
            Transaction.DestNumber              := DestNumber;
            Transaction.DestName                := DestName;
            Transaction.OriginName              := OriginName;
            Transaction.OriginNumber            := Origin_Number;
            Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
            Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
            Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
            Transaction.FEIN                    := FEIN;
            Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;

            DM_EMPLOYEE.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, []);

            if DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.IsNull then
              Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
                DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString
            else
              Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
                DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.AsString + '. ' + DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString;
            if Length(ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)) > 0 then
              Transaction.ABA_NUMBER := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)
            else
              raise EInconsistentData.CreateHelp('Incorrect ABA number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) + ', CO #' +
                Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
            if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
              BankAccountNumber := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString)
            else
              BankAccountNumber := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString;
            if BankAccountNumber = '' then
              raise EInconsistentData.CreateHelp('Incorrect Bank Account number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) +
                ', CO #' + Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
            Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
            Transaction.AchAccountType := GetAchAccountType(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_TYPE.AsString)+
                                 GetAchTransactionType('N', -Trans.FieldByName('AMOUNT').AsCurrency, 0);
            Transaction.TranType := TRAN_TYPE_OUT;
            Transactions.Add(Transaction);
          end;
        end
        else
        if not Trans.FieldByName('FROM_CL_BANK_ACCOUNT_NBR').IsNull then
        begin
          if DM_EMPLOYEE.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', Trans.FieldByName('FROM_CL_BANK_ACCOUNT_NBR').Value, []) then
          begin
            Transaction := TevManualAchTransaction.Create;
            Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
            Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
            Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
            Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
            Transaction.ClassCode               := 'CCD';
            Transaction.EffectiveDate           := Trans.FieldByName('DEBIT_DATE').AsDateTime;
            Transaction.Comments                := 'MAN DEBIT';
            Transaction.BankAccountRegisterType := GetRegisterType;
            Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').Value, 0);
            Transaction.Amount                  := -Trans.FieldByName('AMOUNT').AsCurrency;
            Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
            Transaction.IdentificationNumber    := GetIdentificationNumber;
            Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
            Transaction.OriginBankAba           := OriginBankAba;
            Transaction.DestNumber              := DestNumber;
            Transaction.DestName                := DestName;
            Transaction.OriginName              := OriginName;
            Transaction.OriginNumber            := Origin_Number;
            Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
            Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
            Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
            Transaction.FEIN                    := FEIN;
            Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
            Transaction.Name                    := 'MANUAL TRANSFER';
            if not DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_EMPLOYEE.CL_BANK_ACCOUNT.SB_BANKS_NBR.Value, []) then
              raise EInconsistentData.CreateHelp(Format('Missing bank for client bank account number %s',
                 [DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString]),
                 IDH_InconsistentData);
            if Length(ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''))) = 0 then
              raise EInconsistentData.CreateHelp('Incorrect ABA number for Client Bank Account #' +
                DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData)
            else
              Transaction.ABA_NUMBER := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''));
            if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
              BankAccountNumber := ACHTrim(DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
            else
              BankAccountNumber := DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;
            if BankAccountNumber = '' then
              raise EInconsistentData.CreateHelp('Incorrect Bank Account number for Client Bank Account #' +
                DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData);
            Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
            Transaction.AchAccountType := GetAchAccountType(DM_EMPLOYEE.CL_BANK_ACCOUNT.BANK_ACCOUNT_TYPE.AsString)+
                                 GetAchTransactionType('N', -Trans.FieldByName('AMOUNT').AsCurrency, 0);
            Transaction.TranType := TRAN_TYPE_COMPANY;
            Transactions.Add(Transaction);
          end;
        end
        else
        begin
          if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR',
            Trans.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').Value, []) then
          begin
            Transaction := TevManualAchTransaction.Create;
            Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
            Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
            Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
            Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
            Transaction.ClassCode               := 'CCD';
            Transaction.EffectiveDate           := Trans.FieldByName('DEBIT_DATE').AsDateTime;
            Transaction.Comments                := 'MAN DEBIT';
            Transaction.BankAccountRegisterType := GetRegisterType;
            Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').Value, 0);
            Transaction.Amount                  := -Trans.FieldByName('AMOUNT').AsCurrency;
            Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
            Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
            Transaction.OriginBankAba           := OriginBankAba;
            Transaction.DestNumber              := DestNumber;
            Transaction.DestName                := DestName;
            Transaction.OriginName              := OriginName;
            Transaction.OriginNumber            := Origin_Number;
            Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
            Transaction.IdentificationNumber    := GetIdentificationNumber;
            Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
            Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
            Transaction.FEIN                    := FEIN;
            Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
            Transaction.Name                    := 'MANUAL TRANSFER';
            Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []));
            if Length(ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''))) = 0 then
              raise EInconsistentData.CreateHelp('Incorrect ABA number for SB Bank Account #' +
                DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData)
            else
              Transaction.ABA_NUMBER := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''));
            if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
              BankAccountNumber := ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
            else
              BankAccountNumber := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;
            if BankAccountNumber = '' then
              raise EInconsistentData.CreateHelp('Incorrect Bank Account number for SB Bank Account #' +
                DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData);
            Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
            Transaction.AchAccountType := GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString)+
                                 GetAchTransactionType('N', -Trans.FieldByName('AMOUNT').AsCurrency, 0);
            Transaction.TranType := TRAN_TYPE_OUT;
            Transactions.Add(Transaction);
          end;
        end;
      end;

      TmpStr := Trim(ExtractFromFiller(Trans.FieldByName('FILLER').AsString,11,10));
      if (TmpStr <> '') and not Trans.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').IsNull then
      begin
        DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('EE_CHILD_SUPPORT_CASES_NBR='+TmpStr);
        DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_DIRECT_DEPOSIT_NBR=' +
          Trans.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').AsString);
        if DM_EMPLOYEE.EE.Locate('EE_NBR', DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.EE_NBR.Value, []) then
        begin
          DM_EMPLOYEE.CL_PERSON.DataRequired('CL_PERSON_NBR=' + DM_EMPLOYEE.EE.CL_PERSON_NBR.AsString);
          
          Transaction := TevManualAchTransaction.Create;

          AGENCY_NAME := '';

          if not DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CL_AGENCY_NBR.IsNull then
          begin
            DM_CLIENT.CL_AGENCY.DataRequired('CL_AGENCY_NBR='+DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CL_AGENCY_NBR.AsString);
            if DM_CLIENT.CL_AGENCY.FieldByName('SB_AGENCY_NBR').AsInteger <> 0 then
            begin
              A := TevQuery.Create('select NAME from SB_AGENCY where {AsOfNow<SB_AGENCY>} and SB_AGENCY_NBR='+DM_CLIENT.CL_AGENCY.FieldByName('SB_AGENCY_NBR').AsString);
              AGENCY_NAME := A.Result.FieldByName('NAME').AsString;
            end
          end;

          if DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('IL_MED_INS_ELIGIBLE').AsString = GROUP_BOX_NOT_APPLICATIVE then
            MedIns := GROUP_BOX_YES
          else
            MedIns := DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('IL_MED_INS_ELIGIBLE').AsString;

          Addenda :=
            Format('705DED*CS*%-1.20s*', [DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('CUSTOM_CASE_NUMBER').AsString]) +
            FormatDateTime('yymmdd"*"', Trans.FieldByName('CREDIT_DATE').AsDateTime) +
            Format('%1.10s*%9.9s*%1.1s*%-0.10s*%-0.7s*',
              [IntToStr(RoundInt64(Abs(Trans.FieldByName('AMOUNT').AsCurrency) * 100)),
               ConvertSSN(DM_EMPLOYEE.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString),
               MedIns,
               DM_EMPLOYEE.CL_PERSON.FieldByName('LAST_NAME').AsString + ',' + DM_EMPLOYEE.CL_PERSON.FieldByName('FIRST_NAME').AsString,
               DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('FIPS_CODE').AsString]);

          if DM_EMPLOYEE.EE.FieldByName('CURRENT_TERMINATION_CODE').AsString = EE_TERM_ACTIVE then
            Addenda := Addenda + '\'
          else
            Addenda := Addenda + 'Y\';

          Addenda := UpperCase(Addenda);

          Transaction.Addenda := Addenda;
          Transaction.TranType                := TRAN_TYPE_OUT;
          Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
          Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
          Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
          Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
          Transaction.ClassCode               := 'CCD';
          Transaction.Comments                := 'CHILD SUPP';
          Transaction.EffectiveDate           := Trans.FieldByName('CREDIT_DATE').AsDateTime;
          Transaction.BankAccountRegisterType := GetRegisterType;
          Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').Value, 0);
          Transaction.Amount                  := Trans.FieldByName('AMOUNT').AsCurrency;
          Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
          Transaction.IdentificationNumber    := GetIdentificationNumber;
          Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
          Transaction.OriginBankAba           := OriginBankAba;
          Transaction.DestNumber              := DestNumber;
          Transaction.DestName                := DestName;
          Transaction.OriginName              := OriginName;
          Transaction.OriginNumber            := Origin_Number;
          Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
          Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
          Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
          Transaction.FEIN                    := FEIN;
          Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
          DM_EMPLOYEE.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, []);
          if AGENCY_NAME <> '' then
            Transaction.Name := AGENCY_NAME
          else
          if DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.IsNull then
            Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
              DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString
          else
            Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
              DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.AsString + '. ' + DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString;
          if Length(ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)) > 0 then
            Transaction.ABA_NUMBER := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)
          else
            raise EInconsistentData.CreateHelp('Incorrect ABA number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) + ', CO #' +
              Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
          if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
            BankAccountNumber := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString)
          else
            BankAccountNumber := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString;
          if BankAccountNumber = '' then
            raise EInconsistentData.CreateHelp('Incorrect Bank Account number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) +
              ', CO #' + Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
          Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
          Transaction.AchAccountType := GetAchAccountType(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_TYPE.AsString)+
                                GetAchTransactionType('N', Trans.FieldByName('AMOUNT').AsCurrency, 0);
          Transactions.Add(Transaction);                      
        end;
      end
      else
      if not Trans.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').IsNull then
      begin
        DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_DIRECT_DEPOSIT_NBR=' +
          Trans.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').AsString);
        if DM_EMPLOYEE.EE.Locate('EE_NBR', DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_NBR.Value, []) then
        begin
          DM_EMPLOYEE.CL_PERSON.DataRequired('CL_PERSON_NBR='+DM_EMPLOYEE.EE.CL_PERSON_NBR.AsString);
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('EE_DIRECT_DEPOSIT_NBR=' +
            Trans.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').AsString);

          DM_EMPLOYEE.EE_SCHEDULED_E_DS.First;

          while not DM_EMPLOYEE.EE_SCHEDULED_E_DS.Eof do
          begin
            if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.EFFECTIVE_START_DATE.AsDateTime < Trans.FieldByName('CREDIT_DATE').AsDateTime)
            and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.EFFECTIVE_END_DATE.IsNull or
              ((DM_EMPLOYEE.EE_SCHEDULED_E_DS.EFFECTIVE_END_DATE.AsDateTime > Trans.FieldByName('CREDIT_DATE').AsDateTime))) then
              break;
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Next;
          end;

          Transaction := TevManualAchTransaction.Create;
          Transaction.TranType                := TRAN_TYPE_OUT;
          Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
          Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
          Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
          Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
          Transaction.ClassCode               := 'PPD';
          Transaction.Comments                := 'NET=PAY';
          Transaction.EffectiveDate           := Trans.FieldByName('CREDIT_DATE').AsDateTime;
          Transaction.BankAccountRegisterType := GetRegisterType;
          Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').Value, 0);
          Transaction.Amount                  := Trans.FieldByName('AMOUNT').AsCurrency;
          Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
          Transaction.IdentificationNumber    := GetIdentificationNumber;
          Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
          Transaction.OriginBankAba           := OriginBankAba;
          Transaction.DestNumber              := DestNumber;
          Transaction.DestName                := DestName;
          Transaction.OriginName              := OriginName;
          Transaction.OriginNumber            := Origin_Number;
          Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
          Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
          Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
          Transaction.FEIN                    := FEIN;
          Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
          DM_EMPLOYEE.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, []);
          if DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.IsNull then
            Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
              DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString
          else
            Transaction.Name := DM_EMPLOYEE.CL_PERSON.FIRST_NAME.AsString + ' ' +
              DM_EMPLOYEE.CL_PERSON.MIDDLE_INITIAL.AsString + '. ' + DM_EMPLOYEE.CL_PERSON.LAST_NAME.AsString;
          if Length(ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)) > 0 then
            Transaction.ABA_NUMBER := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_ABA_NUMBER.AsString)
          else
            raise EInconsistentData.CreateHelp('Incorrect ABA number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) + ', CO #' +
              Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
          if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
            BankAccountNumber := ACHTrim(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString)
          else
            BankAccountNumber := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_NUMBER.AsString;
          if BankAccountNumber = '' then
            raise EInconsistentData.CreateHelp('Incorrect Bank Account number for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString) +
              ', CO #' + Trans.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);
          Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
          Transaction.AchAccountType := GetAchAccountType(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.EE_BANK_ACCOUNT_TYPE.AsString)+
                                GetAchTransactionType('N', Trans.FieldByName('AMOUNT').AsCurrency, 0);

          Transactions.Add(Transaction);
        end;
      end
      else
      if not Trans.FieldByName('TO_CL_BANK_ACCOUNT_NBR').IsNull then
      begin
        if DM_EMPLOYEE.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', Trans.FieldByName('TO_CL_BANK_ACCOUNT_NBR').Value, []) then
        begin
          Transaction := TevManualAchTransaction.Create;
          Transaction.TranType                := TRAN_TYPE_COMPANY;
          Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
          Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
          Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
          Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
          Transaction.ClassCode               := 'CCD';
          Transaction.Comments                := 'MAN CREDIT';
          Transaction.EffectiveDate           := Trans.FieldByName('CREDIT_DATE').AsDateTime;
          Transaction.BankAccountRegisterType := GetRegisterType;
          Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').Value, 0);
          Transaction.Amount                  := Trans.FieldByName('AMOUNT').AsCurrency;
          Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
          Transaction.IdentificationNumber    := GetIdentificationNumber;
          Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
          Transaction.OriginBankAba           := OriginBankAba;
          Transaction.DestNumber              := DestNumber;
          Transaction.DestName                := DestName;
          Transaction.OriginName              := OriginName;
          Transaction.OriginNumber            := Origin_Number;
          Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
          Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
          Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
          Transaction.FEIN                    := FEIN;
          Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
          Transaction.Name                    := 'MANUAL TRANSFER';
          if not DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_EMPLOYEE.CL_BANK_ACCOUNT.SB_BANKS_NBR.Value, []) then
            raise EInconsistentData.CreateHelp(Format('Missing bank for client bank account number %s',
               [DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString]),
               IDH_InconsistentData);
          if Length(ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''))) = 0 then
            raise EInconsistentData.CreateHelp('Incorrect ABA number for Client Bank Account #' +
              DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData)
          else
            Transaction.ABA_NUMBER := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''));
          if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
            BankAccountNumber := ACHTrim(DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
          else
            BankAccountNumber := DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;
          if BankAccountNumber = '' then
            raise EInconsistentData.CreateHelp('Incorrect Bank Account number for Client Bank Account #' +
              DM_EMPLOYEE.CL_BANK_ACCOUNT.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData);
          Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
          Transaction.AchAccountType := GetAchAccountType(DM_EMPLOYEE.CL_BANK_ACCOUNT.BANK_ACCOUNT_TYPE.AsString)+
                               GetAchTransactionType('N', Trans.FieldByName('AMOUNT').AsCurrency, 0);
          Transactions.Add(Transaction);
        end;
      end
      else
      begin
        if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR',
          Trans.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').Value, []) then
        begin
          Transaction := TevManualAchTransaction.Create;
          Transaction.TranType                := TRAN_TYPE_OUT;
          Transaction.Name                    := 'MANUAL TRANSFER';
          Transaction.ClientName              := DM_COMPANY.CL.NAME.AsString;
          Transaction.PrintClientName         := DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString;
          Transaction.CompanyLegalName        := DM_COMPANY.CO.LEGAL_NAME.AsString;
          Transaction.CO_MANUAL_ACH_NBR       := Trans.FieldByName('CO_MANUAL_ACH_NBR').AsInteger;
          Transaction.ClassCode               := 'CCD';
          Transaction.Comments                := 'MAN CREDIT';
          Transaction.EffectiveDate           := Trans.FieldByName('CREDIT_DATE').AsDateTime;
          Transaction.BankAccountRegisterType := GetRegisterType;
          Transaction.SB_BANK_ACCOUNTS_NBR    := ConvertNull(Trans.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').Value, 0);
          Transaction.Amount                  := Trans.FieldByName('AMOUNT').AsCurrency;
          Transaction.CustomEmployeeNumber    := DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString;
          Transaction.CustomCompanyNumber     := DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString;
          Transaction.OriginBankAba           := OriginBankAba;
          Transaction.DestNumber              := DestNumber;
          Transaction.DestName                := DestName;
          Transaction.OriginName              := OriginName;
          Transaction.OriginNumber            := Origin_Number;
          Transaction.CompanyName             := DM_COMPANY.CO.NAME.AsString;
          Transaction.IdentificationNumber    := GetIdentificationNumber;
          Transaction.CO_NBR                  := Trans.FieldByName('CO_NBR').AsInteger;
          Transaction.CL_NBR                  := Trans.FieldByName('CL_NBR').AsInteger;
          Transaction.FEIN                    := FEIN;
          Transaction.CHECK_DATE              := Trans.FieldByName('DEBIT_DATE').AsDateTime;
          Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.Value, []));
          if Length(ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''))) = 0 then
            raise EInconsistentData.CreateHelp('Incorrect ABA number for SB Bank Account #' +
              DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData)
          else
            Transaction.ABA_NUMBER := ACHTrim(ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.Value, ''));
          if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
            BankAccountNumber := ACHTrim(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString)
          else
            BankAccountNumber := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;
          if BankAccountNumber = '' then
            raise EInconsistentData.CreateHelp('Incorrect Bank Account number for SB Bank Account #' +
              DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString, IDH_InconsistentData);
          Transaction.BANK_ACCOUNT_NUMBER := BankAccountNumber;
          Transaction.AchAccountType := GetAchAccountType(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString)+
                               GetAchTransactionType('N', Trans.FieldByName('AMOUNT').AsCurrency, 0);
          Transactions.Add(Transaction);
        end;
      end;
    except
      on E: Exception do
        FExceptions := FExceptions + E.Message;
    end;
    Trans.Next;
  end;
  Result := Transactions;
end;

procedure TevAchManual.PatchAchFiles(const AchSave: IisListOfValues);
var
  ReportId: Integer;
  ReportName, ReportDB, Num: ShortString;
  ASCIIFile: IisStringList;
  ReportParams: TrwReportParams;
  ResData: IisStream;
  Res: TrwReportResults;
  ReportResult: TrwReportResult;
  i: Integer;
begin
  ReportId := -1;
  if (Length(FOptions.PostProcessReportName) > 0)
  and (FOptions.PostProcessReportName <> 'N/A') then
  begin
    ReportName := FOptions.PostProcessReportName;
    ReportDB := '';
    Num := '';
    for i := Length(ReportName)-1 downto 1 do
    begin
      if ReportName[i] = '(' then
        break
      else
        Num := ReportName[i] + Num;
    end;
    if (Length(Num) > 0) and (Num[1] in ['S','B']) then
    begin
      ReportDB := Num[1];
      ReportId := StrToIntDef(copy(Num,2,100), -1);
    end;
  end;

  if (ReportId > 0) and Assigned(ACHSave) then
  begin
    for i := 0 to AchSave.Count - 1 do
    begin
      ASCIIFile := IInterface(AchSave.Values[i].Value) as IisStringList;
      ReportParams := TrwReportParams.Create;
      try
        ReportParams.Add('Data', ASCIIFile.Text);
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(ReportID, ReportDB, ReportParams);
        finally
          ResData := ctx_RWLocalEngine.EndGroup(rdtNone);
          Res := TrwReportResults.Create(ResData);
          try
            ReportResult := Res[0];
            ASCIIFile.Text := ReportResult.Data.AsString;
            AchSave.Values[i].Value := ASCIIFile;
          finally
            Res.Free;
          end;
        end;
      finally
        ReportParams.Free;
      end;
    end;
  end;
end;

procedure TevAchManual.CreateReportsAndAchFiles(const ATransactions: IisList);
var
  i, j: Integer;
  datelist: TStringList;
  Transactions1: IisList;
  Transaction: IevManualAchTransaction;
  AchText: IisStringList;
begin
  if not FOptions.EffectiveDate then
  begin
    ExportSingleAchFile(ATransactions);
    AchText := ACHFile.GetAchTextFileStrings;
    FACHFiles.AddValue(FileName, AchText);
  end
  else
  begin
    datelist := TStringList.Create;
    try
      datelist.Sorted := True;
      datelist.Duplicates := dupIgnore;
      for j := 0 to ATransactions.Count - 1 do
        datelist.Add(DateToStr((ATransactions[j] as IevManualAchTransaction).EffectiveDate));

      for i := 0 to datelist.Count - 1 do
      begin
        Transactions1 := TisList.Create;
        ACHFile.EmptyDataSet;
        for j := 0 to ATransactions.Count - 1 do
          if datelist[i] = DateToStr((ATransactions[j] as IevManualAchTransaction).EffectiveDate) then
          begin
            Transaction := TevManualAchTransaction.Create;
            Transaction.CopyFrom(ATransactions[j] as IevManualAchTransaction);
            Transactions1.Add(Transaction);
          end;
        ExportSingleAchFile(Transactions1);
        AchText := ACHFile.GetAchTextFileStrings;
        FACHFiles.AddValue(ChangeFileExt(FileName, '.' + FormatDateTime('yyyymmdd', StrToDate(datelist[i])) + ExtractFileExt(FileName)), AchText);
      end;
      ExportSingleAchFile(ATransactions);
    finally
      datelist.Free;
    end;
  end;

  PatchAchFiles(FACHFiles);

end;

procedure TevAchManual.ExportSingleAchFile(const ATransactions: IisList);
var
  EntryHash, TotalHash: Int64;
  TotalDebits, TotalCredits, Debits, Credits: Double;
  i: Integer;
  EntryCount: Integer;
  Transaction: IevManualAchTransaction;
  ReceiverName, TraceNumber: String;
  BatchUniqID: Int64;
  NbrOfEntries: Integer;
  NbrOfBatches: Integer;
  NbrOfDummyRecords: Integer;
  HeaderNeeded: Boolean;

  function FooterNeeded(AIndex: Integer): Boolean;
  var
    After: IevManualAchTransaction;
  begin
    Result := (AIndex = (ATransactions.Count - 1)) or not FOptions.BalanceBatches;
    if not Result and (AIndex < (ATransactions.Count-1)) then
    begin
      After := ATransactions[AIndex+1] as IevManualAchTransaction;
      Result :=
        (Transaction.CL_NBR <> After.CL_NBR) or
        (Transaction.CO_NBR <> After.CO_NBR) or
        (Transaction.CHECK_DATE <> After.CHECK_DATE) or
        (Transaction.EffectiveDate <> After.EffectiveDate) or
        (Transaction.OriginBankAba <> After.OriginBankAba);
    end;
  end;

var
 AddInd: String;  
begin
  ACHFile.EmptyDataSet;


  if Trim(FOptions.Header) <> '' then
  begin
    ACHFile.Append;
    ACHFile.AMOUNT.AsCurrency := 0;
    ACHFile.ACH_TYPE.AsInteger := ACH_FILE_HEADER;
    ACHFile.PR_NBR.AsInteger := 0;    
    ACHFile.NAME.Value := '';
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
    ACHFile.ORDER_NBR.Value := 1;
    ACHFile.ACH_LINE.AsString := Trim(FOptions.Header);
    ACHFile.Post;
  end;

  ACHFile.Append;
  ACHFile.ACH_TYPE.AsString := '1:File Header';
  ACHFile.TRAN.AsString := '';
  ACHFile.CL_NBR.AsInteger := 0;
  ACHFile.CO_NBR.AsInteger := 0;
  ACHFile.DEBITS.AsCurrency := 0;
  ACHFile.CREDITS.AsCurrency := 0;
  ACHFile.BATCH_NBR.AsString := '';
  ACHFile.SEGMENT_NBR.AsInteger := 1;
  ACHFile.PR_NBR.AsInteger := 0;
  ACHFile.ENTRY_CLASS_CODE.AsString := '';
  ACHFile.COMMENTS.AsString := '';
  ACHFile.EFFECTIVE_DATE.AsDateTime := Date;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.BANK_ACCOUNT_NUMBER.AsString := '';
  ACHFile.AMOUNT.AsCurrency := 0;
  ACHFile.IN_PRENOTE.AsString := 'N'; // Initialize to 'N' other code will overide this value
  ACHFile.CUSTOM_COMPANY_NUMBER.AsString := '                    ';
  ACHFile.FEIN.AsString := (ATransactions[0] as IevManualAchTransaction).FEIN;
  ACHFile.CHECK_DATE.AsDateTime := Date;
  ACHFile.RUN_NUMBER.AsInteger := 1;
  ACHFile.ABA_NUMBER.AsString := (ATransactions[0] as IevManualAchTransaction).DestNumber;
  ACHFile.ABA_NUMBER2.AsString := (ATransactions[0] as IevManualAchTransaction).DestNumber;
  ACHFile.EIN_NUMBER.AsString := copy((ATransactions[0] as IevManualAchTransaction).OriginNumber, 2, 9);
  ACHFile.NAME.AsString := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;
  ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
  ACHFile.ACH_LINE.AsString := UpperCase('101' + ' ' + (ATransactions[0] as IevManualAchTransaction).DestNumber
    + (ATransactions[0] as IevManualAchTransaction).OriginNumber + FormatDateTime('YYMMDDHHMM', Now) + 'A094101'
    + PadRight(AnsiUpperCase((ATransactions[0] as IevManualAchTransaction).DestName), 23)
    + PadRight((ATransactions[0] as IevManualAchTransaction).OriginName, 23)
    + ZeroFillInt64ToStr(0, 8));

  ACHFile.Post;
  TotalHash := 0;
  TotalDebits := 0;
  TotalCredits := 0;
  Debits := 0;
  Credits := 0;
  BatchUniqID := 0;
  NbrOfBatches := 0;
  NbrOfEntries := 0;

  HeaderNeeded := True;

  for i := 0 to ATransactions.Count - 1 do
  begin
    Transaction := ATransactions[i] as IevManualAchTransaction;
    // if BalanceBatches option selected we need to join transactions into one batch
    if HeaderNeeded then
    begin
      NbrOfBatches := NbrOfBatches + 1;
      HeaderNeeded := False;
      BatchUniqID := Random(9999999999);
      ACHFile.Append;
      ACHFile.ACH_TYPE.AsString := '5:Batch Header';
      ACHFile.BATCH_NBR.AsString := Format('%10d',[BatchUniqID]);
      ACHFile.NAME.AsString := GetBatchName(Transaction);
      ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
      ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger := Transaction.SB_BANK_ACCOUNTS_NBR;
      ACHFile.BANK_REGISTER_TYPE.AsString := '';
      ACHFile.RUN_NUMBER.AsInteger := 1;
      if FOptions.BalanceBatches then
        Transaction.Comments := 'MANUAL';
      ACHFile.COMMENTS.AsString := Transaction.Comments;
      ACHFile.BATCH_COMMENTS.AsString := Transaction.Comments;
      ACHFile.SEGMENT_NBR.AsInteger := 1;
      ACHFile.PR_NBR.AsInteger := 0;
      ACHFile.CHECK_DATE.AsDateTime := Transaction.CHECK_DATE;
      ACHFile.EFFECTIVE_DATE.AsDateTime := Transaction.EffectiveDate;
      ACHFile.CUSTOM_COMPANY_NUMBER.AsString := Transaction.CustomCompanyNumber;
      ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
      ACHFile.AMOUNT.AsCurrency := 0;
      ACHFile.IN_PRENOTE.AsString := 'N';
      ACHFile.CL_NBR.AsInteger := Transaction.CL_NBR;
      ACHFile.CO_NBR.AsInteger := Transaction.CO_NBR;
      ACHFile.FEIN.AsString := Transaction.FEIN;
      ACHFile.ACH_OFFSET.AsInteger := OFF_DONT_USE;

      ACHFile.ACH_LINE.AsString :=
        '5' +
        '200' +
        PadRight(AnsiUpperCase(GetBatchCompanyName(Transaction, BatchUniqID)), 16) +
        PadRight(GetCompanyData(Transaction, BatchUniqID), 20) +
        PadRight(Transaction.FEIN, 10) +
        PadRight(Transaction.ClassCode, 3) +
        PadRight(Transaction.Comments, 10) +
        FormatDateTime('YYMMDD', Transaction.CHECK_DATE) +
        FormatDateTime('YYMMDD', Transaction.EffectiveDate) +
        PadRight('', 3) + // SettlementDate
        PadRight('1',1) + // OriginatorStatusCode
        PadRight(Transaction.OriginBankAba, 8) +
        ZeroFillInt64ToStr(NbrOfBatches, 7);

      ACHFile.Post;
      NbrOfEntries := 0;
    end;
//    Transaction.BatchUniqID := BatchUniqID;
    ACHFile.Append;
    ACHFile.CR_AFTER_LINE.AsString := GROUP_BOX_NO;
    ACHFile.ACH_TYPE.AsString := '6:Entry Detail';
    ACHFile.BATCH_COMMENTS.AsString := Transaction.Comments;
    ACHFile.BATCH_NBR.AsString := Format('%10d',[BatchUniqID]);
    ACHFile.AMOUNT.AsCurrency := Abs(Transaction.Amount);
    ACHFile.ABA_NUMBER.AsString := Transaction.ABA_NUMBER;
    ACHFile.ABA_NUMBER2.AsString := Transaction.OriginBankAba;
    ACHFile.BANK_ACCOUNT_NUMBER.AsString := Transaction.BANK_ACCOUNT_NUMBER;
    ACHFile.CHECK_DATE.AsDateTime := Transaction.CHECK_DATE;
    ACHFile.FEIN.AsString := Transaction.FEIN;
    ACHFile.NAME.AsString := Transaction.Name;
    ACHFile.TRAN_TYPE.AsString := Transaction.TranType;
    ACHFile.EFFECTIVE_DATE.AsDateTime := Transaction.EffectiveDate;
    ACHFile.CL_NBR.AsInteger := Transaction.CL_NBR;
    ACHFile.CO_NBR.AsInteger := Transaction.CO_NBR;
    ACHFile.CUSTOM_COMPANY_NUMBER.AsString := Transaction.CustomCompanyNumber;
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';// Transaction.Value['CustomEmployeeNumber'];
    ACHFile.EIN_NUMBER.AsString := Transaction.OriginNumber;
    ACHFile.TRAN.AsString := Transaction.AchAccountType;
    ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
    ACHFile.ENTRY_CLASS_CODE.AsString := Transaction.ClassCode;

    if not FOptions.DisplayCoNbrAndName then
      ReceiverName := Transaction.Name
    else
      ReceiverName := Transaction.CustomCompanyNumber + ' ' +
        RemovePunctuation(Transaction.CompanyName);

    NbrOfEntries := NbrOfEntries + 1;
    TraceNumber := copy(Transaction.OriginBankAba + '         ', 1, 8) + ZeroFillInt64ToStr(NbrOfEntries, 7);

    if Transaction.Amount > 0 then
    begin
      Credits := Credits + Transaction.Amount;
      TotalCredits := TotalCredits + Transaction.Amount;
    end
    else
    begin
      Debits := Debits - Transaction.Amount;
      TotalDebits := TotalDebits - Transaction.Amount;
    end;

    IncHashTotal(Transaction.ABA_NUMBER, EntryHash);

    if Transaction.Addenda <> '' then
      AddInd := '1'
    else
      AddInd := '0';

    ACHFile.ACH_LINE.AsString :=
      '6' +
      Transaction.AchAccountType +
      PadRight(Transaction.ABA_NUMBER, 9) +
      PadRight(Transaction.BANK_ACCOUNT_NUMBER, 17) +
      ZeroFillIntToStr(RoundInt64(Abs(Transaction.Amount) * 100), 10) +
      PadRight(Transaction.IdentificationNumber, 15) +
      PadRight(ReceiverName, 22) +
      PadRight('  ', 2) +
      AddInd +
      PadLeft(TraceNumber, 15);
    ACHFile.Post;

    if Transaction.Addenda <> '' then
    begin
      ACHFile.Append;
      ACHFile.CR_AFTER_LINE.AsString := GROUP_BOX_NO;
      ACHFile.ACH_TYPE.AsString := '7:Addenda Record';
      ACHFile.BATCH_COMMENTS.AsString := Transaction.Comments;
      ACHFile.BATCH_NBR.AsString := Format('%10d',[BatchUniqID]);
      ACHFile.AMOUNT.AsCurrency := 0;
      ACHFile.ABA_NUMBER.AsString := Transaction.ABA_NUMBER;
      ACHFile.ABA_NUMBER2.AsString := Transaction.OriginBankAba;
      ACHFile.BANK_ACCOUNT_NUMBER.AsString := Transaction.BANK_ACCOUNT_NUMBER;
      ACHFile.CHECK_DATE.AsDateTime := Transaction.CHECK_DATE;
      ACHFile.FEIN.AsString := Transaction.FEIN;
      ACHFile.NAME.AsString := Transaction.Name;
      ACHFile.TRAN_TYPE.AsString := Transaction.TranType;
      ACHFile.EFFECTIVE_DATE.AsDateTime := Transaction.EffectiveDate;
      ACHFile.CL_NBR.AsInteger := Transaction.CL_NBR;
      ACHFile.CO_NBR.AsInteger := Transaction.CO_NBR;
      ACHFile.CUSTOM_COMPANY_NUMBER.AsString := Transaction.CustomCompanyNumber;
      ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';// Transaction.Value['CustomEmployeeNumber'];
      ACHFile.EIN_NUMBER.AsString := Transaction.OriginNumber;
      ACHFile.TRAN.AsString := Transaction.AchAccountType;
      ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
      ACHFile.ENTRY_CLASS_CODE.AsString := Transaction.ClassCode;

      NbrOfEntries := NbrOfEntries + 1;
      TraceNumber := ZeroFillInt64ToStr(NbrOfEntries, 7);

      ACHFile.ACH_LINE.AsString := PadRight(Transaction.Addenda, 83) + '0001' + PadLeft(TraceNumber, 7);
    end;

    if FooterNeeded(i) then
    begin
      HeaderNeeded := True;
      ACHFile.Append;
      ACHFile.ACH_TYPE.AsString := '8:Batch Total';
      ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger := Transaction.SB_BANK_ACCOUNTS_NBR;
      ACHFile.BANK_REGISTER_TYPE.AsString := '';
      ACHFile.SEGMENT_NBR.AsInteger := 1;
      ACHFile.PR_NBR.AsInteger := 0;
      ACHFile.CREDITS.AsCurrency := Credits;
      ACHFile.DEBITS.AsCurrency := Debits;
      ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := Transaction.CustomEmployeeNumber;
      ACHFile.AMOUNT.AsCurrency := ACHFile.CREDITS.AsCurrency - ACHFile.CREDITS.AsCurrency;
      ACHFile.CUSTOM_COMPANY_NUMBER.AsString := Transaction.CustomCompanyNumber;
      ACHFile.IN_PRENOTE.AsString := 'N';
      ACHFile.RUN_NUMBER.AsInteger := 1;
      ACHFile.CL_NBR.AsInteger := Transaction.CL_NBR;
      ACHFile.CO_NBR.AsInteger := Transaction.CO_NBR;
      ACHFile.FEIN.AsString := Transaction.FEIN;
      ACHFile.EFFECTIVE_DATE.AsDateTime := Transaction.EffectiveDate;
      ACHFile.CHECK_DATE.AsDateTime := Transaction.CHECK_DATE;
      ACHFile.BATCH_NBR.AsString := Format('%10d',[BatchUniqID]);
      TotalHash := TotalHash + EntryHash;
      ACHFile.ACH_LINE.AsString :=
        '8200' +
        ZeroFillInt64ToStr(NbrOfEntries, 6) +
        ZeroFillInt64ToStr(EntryHash, 10) +
        ZeroFillInt64ToStr(RoundInt64(Abs(ACHFile.DEBITS.AsCurrency) * 100), 12) +
        ZeroFillInt64ToStr(RoundInt64(Abs(ACHFile.CREDITS.AsCurrency) * 100), 12) +
        copy(Transaction.FEIN + '          ', 1, 10) +
        '                   ' +
        '      ' +
        copy(Transaction.OriginBankAba + '        ', 1, 8) +
        ZeroFillInt64ToStr(NbrOfBatches, 7);
      ACHFile.Post;
      EntryHash := 0;
      NbrOfEntries := 0;
      Debits := 0;
      Credits := 0;
    end;
  end;

  EntryCount := 0;
  for i := 0 to ATransactions.Count - 1 do
  begin
    EntryCount := EntryCount + 1;
    Transaction := ATransactions[i] as IevManualAchTransaction;
    if Transaction.Addenda <> '' then
      EntryCount := EntryCount + 1;
  end;

  // addeding the record exclusively for the ACH reports
  NbrOfDummyRecords := 1; // dummy records count
  begin
    ACHFile.Append;
    ACHFile.ACH_TYPE.AsString := '9:File Total';
    ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger := Transaction.SB_BANK_ACCOUNTS_NBR;
    ACHFile.BANK_REGISTER_TYPE.AsString := '';
    ACHFile.SEGMENT_NBR.AsInteger := 1;
    ACHFile.PR_NBR.AsInteger := 0;
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := ATransactions.Count; // Entries:
    ACHFile.AMOUNT.AsCurrency := 0;
    ACHFile.IN_PRENOTE.AsString := 'N';
    ACHFile.CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
    ACHFile.CHECK_DATE.AsDateTime := Date;
    ACHFile.RUN_NUMBER.AsInteger := 1;
    ACHFile.ACH_OFFSET.AsInteger := OFF_USE_OFFSETS;
    ACHFile.CL_NBR.AsInteger := MaxInt;
    ACHFile.BATCH_NBR.AsInteger := NbrOfBatches;
    ACHFile.BANK_ACCOUNT_NUMBER.AsString := ZeroFillInt64ToStr(TotalHash, 10); // ABA Hash:
    ACHFile.DEBITS.AsCurrency := TotalDebits;
    ACHFile.CREDITS.AsCurrency := TotalCredits;
    ACHFile.ACH_LINE.AsString :=
      '9' +
      ZeroFillInt64ToStr(NbrOfBatches, 6) +
      ZeroFillInt64ToStr((ACHFile.RecordCount + 2 + 9) div 10, 6) +
      ZeroFillInt64ToStr(EntryCount, 8) +
      ZeroFillInt64ToStr(TotalHash, 10) +
      ZeroFillInt64ToStr(RoundInt64(TotalDebits * 100), 12) +
      ZeroFillInt64ToStr(RoundInt64(TotalCredits * 100), 12) +
      '          ' +
      '          ' +
      '          ' +
      '         ';
    ACHFile.Post;
  end;

  if Length(Trim(FOptions.Header)) > 0 then
    NbrOfDummyRecords := NbrOfDummyRecords + 1;

  ACHFile.Append;
  ACHFile.ACH_TYPE.AsString := '9:File Total';
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger := Transaction.SB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString := '';
  ACHFile.SEGMENT_NBR.AsInteger := 1;
  ACHFile.PR_NBR.AsInteger := 0;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := ATransactions.Count; // Entries:
  ACHFile.AMOUNT.AsCurrency := 0;
  ACHFile.IN_PRENOTE.AsString := 'N';
  ACHFile.CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
  ACHFile.CHECK_DATE.AsDateTime := Date;
  ACHFile.RUN_NUMBER.AsInteger := 1;
  ACHFile.ACH_OFFSET.AsInteger := OFF_DONT_USE;
  ACHFile.CL_NBR.AsInteger := MaxInt;
  ACHFile.BATCH_NBR.AsInteger := ATransactions.Count; // Batches:
  ACHFile.BANK_ACCOUNT_NUMBER.AsString := ZeroFillInt64ToStr(TotalHash, 10); // ABA Hash:
  ACHFile.DEBITS.AsCurrency := TotalDebits;
  ACHFile.CREDITS.AsCurrency := TotalCredits;
  ACHFile.ACH_LINE.AsString :=
    '9' +
    ZeroFillInt64ToStr(NbrOfBatches, 6) +
    ZeroFillInt64ToStr((ACHFile.RecordCount + 9 - NbrOfDummyRecords) div 10, 6) +
    ZeroFillInt64ToStr(EntryCount, 8) +
    ZeroFillInt64ToStr(TotalHash, 10) +
    ZeroFillInt64ToStr(RoundInt64(TotalDebits * 100), 12) +
    ZeroFillInt64ToStr(RoundInt64(TotalCredits * 100), 12) +
    '          ' +
    '          ' +
    '          ' +
    '         ';
  ACHFile.Post;

  while (ACHFile.RecordCount - NbrOfDummyRecords) mod 10 <> 0 do
  begin
    ACHFile.Append;
    ACHFile.CL_NBR.AsInteger := MaxInt;
    ACHFile.ACH_TYPE.AsString := '99:End-Of-File';
    ACHFile.SEGMENT_NBR.AsInteger := 1;
    ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
    ACHFile.CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
    ACHFile.PR_NBR.AsInteger := 0;
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(ACHFile.RecordCount + 1);
    ACHFile.ACH_LINE.AsString := sDummy;
    ACHFile.Post;
  end;

  if Trim(FOptions.Footer) <> '' then
  begin
    ACHFile.Append;
    ACHFile.CL_NBR.AsInteger := MaxInt;
    ACHFile.ACH_TYPE.AsString := '99:End-Of-File';
    ACHFile.SEGMENT_NBR.AsInteger := 1;
    ACHFile.ORDER_NBR.AsInteger := ACHFile.RecordCount;
    ACHFile.CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
    ACHFile.PR_NBR.AsInteger := 0;
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(ACHFile.RecordCount + 1);
    ACHFile.ACH_LINE.AsString := FOptions.Footer;
    ACHFile.Post;
  end;

end;

constructor TevAchManual.Create{(AOwner: TComponent)};
begin
  inherited;
  FExceptions := '';
  FACHFile := TevAchDataSet.Create(nil);
  FileName := FormatDateTime('YYYYMMDDHHNNSS', Now) + '.ach';
  Randomize;
  ACHFolder := '';
  FACHFiles := TisListOfValues.Create;
  FOptions := TACHOptions.Create;
  Trans := TEvBasicClientDataSet.Create(nil);
end;

destructor TevAchManual.Destroy;
begin
  ACHFile.Free;
  FOptions.Free;
  Trans.Free;
  inherited;
end;

procedure TevAchManual.SetACHOptions(const NewACHOptions: IevACHOptions);
begin
  FOptions.Assign2(NewACHOptions);
end;

procedure TevAchManual.Process;
var
  Transactions: IisList;
begin
  Transactions := GetTransactions;
  if Transactions.Count > 0 then
    CreateReportsAndAchFiles(Transactions);
  if Transactions.Count > 0 then
    UpdateStatus(Transactions);
end;

procedure TevAchManual.SetFileName(const AValue: String);
begin
  FFileName := AValue;
end;

procedure TevAchManual.UpdateStatus(
  const ATransactions: IisList);
var
  Transaction: IevManualAchTransaction;
  i: Integer;
begin
  for i := 0 to ATransactions.Count - 1 do
  begin
    try
      Transaction := ATransactions[i] as IevManualAchTransaction;
      if not DM_COMPANY.CO.Active
      or not DM_COMPANY.CO_MANUAL_ACH.Active
      or not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active
      or (Transaction.CO_NBR <> DM_COMPANY.CO.CO_NBR.AsInteger)
      or (Transaction.CL_NBR <> ctx_DBAccess.CurrentClientNbr) then
      begin
        if Transaction.CL_NBR <> ctx_DBAccess.CurrentClientNbr then
        begin
          ctx_DataAccess.OpenClient(Transaction.CL_NBR);
          DM_COMPANY.CO.DataRequired('ALL');
        end;
        DM_COMPANY.CO.Locate('CO_NBR', Transaction.CO_NBR, []);
        DM_COMPANY.CO_MANUAL_ACH.DataRequired('CO_NBR=' + IntToStr(Transaction.CO_NBR));
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('1=2');
      end;
      // adding the record to the BAR
      if (Transaction.BankAccountRegisterType <> '')
      and (Transaction.SB_BANK_ACCOUNTS_NBR <> 0) then
      begin
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := Transaction.SB_BANK_ACCOUNTS_NBR;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_MANUAL_ACH_NBR').AsInteger := Transaction.CO_MANUAL_ACH_NBR;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').AsDateTime := Transaction.CHECK_DATE;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').AsDateTime := Transaction.EffectiveDate;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').AsCurrency := Transaction.Amount;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').AsString := Transaction.BankAccountRegisterType;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').AsString := BANK_REGISTER_MANUALTYPE_None;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').AsDateTime := Date;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').AsString := PutIntoFiller('', IntToStr(Transaction.BatchUniqID), 18, 10);
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      end;
      // updating the manual ACH and billing history status
      if DM_COMPANY.CO_MANUAL_ACH.Locate('CO_MANUAL_ACH_NBR', Transaction.CO_MANUAL_ACH_NBR, []) then
      if DM_COMPANY.CO_MANUAL_ACH.STATUS.AsString <> COMMON_REPORT_STATUS__COMPLETED then
      begin
        DM_COMPANY.CO_MANUAL_ACH.Edit;
        DM_COMPANY.CO_MANUAL_ACH.STATUS.AsString := COMMON_REPORT_STATUS__COMPLETED;
        DM_COMPANY.CO_MANUAL_ACH.Post;

        if Trim(ExtractFromFiller(DM_COMPANY.CO_MANUAL_ACH.FILLER.AsString,1,10)) <> '' then
        begin
          if StrToIntDef(ExtractFromFiller(DM_COMPANY.CO_MANUAL_ACH.FILLER.AsString,1,10), -1) > 0 then
          begin
            DM_COMPANY.CO_BILLING_HISTORY.DataRequired('CO_NBR=' + IntToStr(Transaction.CO_NBR) + ' AND STATUS = ''R'' AND INVOICE_NUMBER = ' +
              ExtractFromFiller(DM_COMPANY.CO_MANUAL_ACH.FILLER.AsString,1,10));
            DM_COMPANY.CO_BILLING_HISTORY.First;
            while not DM_COMPANY.CO_BILLING_HISTORY.Eof do
            begin
              DM_COMPANY.CO_BILLING_HISTORY.Edit;
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := 'S';
              DM_COMPANY.CO_BILLING_HISTORY.Post;
              DM_COMPANY.CO_BILLING_HISTORY.Next;
            end;
          end;
        end;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH, DM_COMPANY.CO_BILLING_HISTORY, DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
    except
      on E: Exception do
      begin
        ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_MANUAL_ACH, DM_COMPANY.CO_BILLING_HISTORY, DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
        Transaction.Exception := E.Message;
      end;
    end;
  end;
end;

{ TevManualAchTransaction }

function TevManualAchTransaction.Get_ABA_NUMBER: String;
begin
  Result := FValues.Value['ABA_NUMBER'];
end;

function TevManualAchTransaction.Get_AchAccountType: String;
begin
  Result := FValues.Value['AchAccountType'];
end;

function TevManualAchTransaction.Get_Amount: Currency;
begin
  Result := FValues.Value['Amount'];
end;

function TevManualAchTransaction.Get_BANK_ACCOUNT_NUMBER: String;
begin
  Result := FValues.Value['BANK_ACCOUNT_NUMBER'];
end;

function TevManualAchTransaction.Get_BankAccountRegisterType: String;
begin
  Result := FValues.Value['BankAccountRegisterType'];
end;

function TevManualAchTransaction.Get_CHECK_DATE: TDateTime;
begin
  Result := FValues.Value['CHECK_DATE'];
end;

function TevManualAchTransaction.Get_CL_NBR: Integer;
begin
  Result := FValues.Value['CL_NBR'];
end;

function TevManualAchTransaction.Get_ClassCode: String;
begin
  Result := FValues.Value['ClassCode'];
end;

function TevManualAchTransaction.Get_ClientName: String;
begin
  Result := FValues.Value['ClientName'];
end;

function TevManualAchTransaction.Get_CO_MANUAL_ACH_NBR: Integer;
begin
  Result := FValues.Value['CO_MANUAL_ACH_NBR'];
end;

function TevManualAchTransaction.Get_CO_NBR: Integer;
begin
  Result := FValues.Value['CO_NBR'];
end;

function TevManualAchTransaction.Get_Comments: String;
begin
  Result := FValues.Value['Comments'];
end;

function TevManualAchTransaction.Get_CompanyLegalName: String;
begin
  Result := FValues.Value['CompanyLegalName'];
end;

function TevManualAchTransaction.Get_CompanyName: String;
begin
  Result := FValues.Value['CompanyName'];
end;

function TevManualAchTransaction.Get_CustomCompanyNumber: String;
begin
  Result := FValues.Value['CustomCompanyNumber'];
end;

function TevManualAchTransaction.Get_CustomEmployeeNumber: String;
begin
  Result := FValues.Value['CustomEmployeeNumber'];
end;

function TevManualAchTransaction.Get_DestName: String;
begin
  Result := FValues.Value['DestName'];
end;

function TevManualAchTransaction.Get_DestNumber: String;
begin
  Result := FValues.Value['DestNumber'];
end;

function TevManualAchTransaction.Get_EffectiveDate: TDateTime;
begin
  Result := FValues.Value['EffectiveDate'];
end;

function TevManualAchTransaction.Get_FEIN: String;
begin
  Result := FValues.Value['FEIN'];
end;

function TevManualAchTransaction.Get_IdentificationNumber: String;
begin
  Result := FValues.Value['IdentificationNumber'];
end;

function TevManualAchTransaction.Get_Name: String;
begin
  Result := FValues.Value['Name'];
end;

function TevManualAchTransaction.Get_OriginBankAba: String;
begin
  Result := FValues.Value['OriginBankAba'];
end;

function TevManualAchTransaction.Get_OriginName: String;
begin
  Result := FValues.Value['OriginName'];
end;

function TevManualAchTransaction.Get_OriginNumber: String;
begin
  Result := FValues.Value['OriginNumber'];
end;

function TevManualAchTransaction.Get_PrintClientName: String;
begin
  Result := FValues.Value['PrintClientName'];
end;

function TevManualAchTransaction.Get_SB_BANK_ACCOUNTS_NBR: Integer;
begin
  Result := FValues.Value['SB_BANK_ACCOUNTS_NBR'];
end;

function TevManualAchTransaction.Get_TranType: String;
begin
  Result := FValues.Value['TranType'];
end;

procedure TevManualAchTransaction.Set_ABA_NUMBER(Value: String);
begin
  FValues.AddValue('ABA_NUMBER', Value);
end;

procedure TevManualAchTransaction.Set_AchAccountType(Value: String);
begin
  FValues.AddValue('AchAccountType', Value);
end;

procedure TevManualAchTransaction.Set_Amount(Value: Currency);
begin
  FValues.AddValue('Amount', Value);
end;

procedure TevManualAchTransaction.Set_BANK_ACCOUNT_NUMBER(Value: String);
begin
  FValues.AddValue('BANK_ACCOUNT_NUMBER', Value);
end;

procedure TevManualAchTransaction.Set_BankAccountRegisterType(
  Value: String);
begin
  FValues.AddValue('BankAccountRegisterType', Value);
end;

procedure TevManualAchTransaction.Set_CHECK_DATE(Value: TDateTime);
begin
  FValues.AddValue('CHECK_DATE', Value);
end;

procedure TevManualAchTransaction.Set_CL_NBR(Value: Integer);
begin
  FValues.AddValue('CL_NBR', Value);
end;

procedure TevManualAchTransaction.Set_ClassCode(Value: String);
begin
  FValues.AddValue('ClassCode', Value);
end;

procedure TevManualAchTransaction.Set_ClientName(Value: String);
begin
  FValues.AddValue('ClientName', Value);
end;

procedure TevManualAchTransaction.Set_CO_MANUAL_ACH_NBR(Value: Integer);
begin
  FValues.AddValue('CO_MANUAL_ACH_NBR', Value);
end;

procedure TevManualAchTransaction.Set_CO_NBR(Value: Integer);
begin
  FValues.AddValue('CO_NBR', Value);
end;

procedure TevManualAchTransaction.Set_Comments(Value: String);
begin
  FValues.AddValue('Comments', Value);
end;

procedure TevManualAchTransaction.Set_CompanyLegalName(Value: String);
begin
  FValues.AddValue('CompanyLegalName', Value);
end;

procedure TevManualAchTransaction.Set_CompanyName(Value: String);
begin
  FValues.AddValue('CompanyName', Value);
end;

procedure TevManualAchTransaction.Set_CustomCompanyNumber(Value: String);
begin
  FValues.AddValue('CustomCompanyNumber', Value);
end;

procedure TevManualAchTransaction.Set_CustomEmployeeNumber(Value: String);
begin
  FValues.AddValue('CustomEmployeeNumber', Value);
end;

procedure TevManualAchTransaction.Set_DestName(Value: String);
begin
  FValues.AddValue('DestName', Value);
end;

procedure TevManualAchTransaction.Set_DestNumber(Value: String);
begin
  FValues.AddValue('DestNumber', Value);
end;

procedure TevManualAchTransaction.Set_EffectiveDate(Value: TDateTime);
begin
  FValues.AddValue('EffectiveDate', Value);
end;

procedure TevManualAchTransaction.Set_FEIN(Value: String);
begin
  FValues.AddValue('FEIN', Value);
end;

procedure TevManualAchTransaction.Set_IdentificationNumber(Value: String);
begin
  FValues.AddValue('IdentificationNumber', Value);
end;

procedure TevManualAchTransaction.Set_Name(Value: String);
begin
  FValues.AddValue('Name', Value);
end;

procedure TevManualAchTransaction.Set_OriginBankAba(Value: String);
begin
  FValues.AddValue('OriginBankAba', Value);
end;

procedure TevManualAchTransaction.Set_OriginName(Value: String);
begin
  FValues.AddValue('OriginName', Value);
end;

procedure TevManualAchTransaction.Set_OriginNumber(Value: String);
begin
  FValues.AddValue('OriginNumber', Value);
end;

procedure TevManualAchTransaction.Set_PrintClientName(Value: String);
begin
  FValues.AddValue('PrintClientName', Value);
end;

procedure TevManualAchTransaction.Set_SB_BANK_ACCOUNTS_NBR(Value: Integer);
begin
  FValues.AddValue('SB_BANK_ACCOUNTS_NBR', Value);
end;

procedure TevManualAchTransaction.Set_TranType(Value: String);
begin
  FValues.AddValue('TranType', Value);
end;

function TevManualAchTransaction.Get_BatchUniqID: Integer;
begin
  Result := FValues.Value['BatchUniqID'];
end;

procedure TevManualAchTransaction.Set_BatchUniqID(Value: Integer);
begin
  FValues.AddValue('BatchUniqID', Value);
end;

procedure TevManualAchTransaction.DoOnConstruction;
begin
  inherited;
  FValues := TisListOfValues.Create;
  FValues.AddValue('BatchUniqID', 0);
  FValues.AddValue('Exception', '');
end;

function TevManualAchTransaction.Get_Exception: String;
begin
  Result := FValues.Value['Exception'];
end;

procedure TevManualAchTransaction.Set_Exception(Value: String);
begin
  FValues.AddValue('Exception', Value);
end;

procedure TevManualAchTransaction.CopyFrom(
  ATransaction: IevManualAchTransaction);
begin
  CL_NBR                  := ATransaction.CL_NBR;
  CO_NBR                  := ATransaction.CO_NBR;
  CHECK_DATE              := ATransaction.CHECK_DATE;
  SB_BANK_ACCOUNTS_NBR    := ATransaction.SB_BANK_ACCOUNTS_NBR;
  EffectiveDate           := ATransaction.EffectiveDate;
  Amount                  := ATransaction.Amount;
  ClientName              := ATransaction.ClientName;
  PrintClientName         := ATransaction.PrintClientName;
  CompanyLegalName        := ATransaction.CompanyLegalName;
  CO_MANUAL_ACH_NBR       := ATransaction.CO_MANUAL_ACH_NBR;
  ClassCode               := ATransaction.ClassCode;
  BankAccountRegisterType := ATransaction.BankAccountRegisterType;
  CustomEmployeeNumber    := ATransaction.CustomEmployeeNumber;
  IdentificationNumber    := ATransaction.IdentificationNumber;
  CustomCompanyNumber     := ATransaction.CustomCompanyNumber;
  OriginBankAba           := ATransaction.OriginBankAba;
  DestNumber              := ATransaction.DestNumber;
  DestName                := ATransaction.DestName;
  OriginName              := ATransaction.OriginName;
  OriginNumber            := ATransaction.OriginNumber;
  CompanyName             := ATransaction.CompanyName;
  FEIN                    := ATransaction.FEIN;
  ABA_NUMBER              := ATransaction.ABA_NUMBER;
  BANK_ACCOUNT_NUMBER     := ATransaction.BANK_ACCOUNT_NUMBER;
  AchAccountType          := ATransaction.AchAccountType;
  TranType                := ATransaction.TranType;
  Comments                := ATransaction.Comments;
  BatchUniqID             := ATransaction.BatchUniqID;
  Name                    := ATransaction.Name;
  Exception               := ATransaction.Exception;
end;

function TevManualAchTransaction.Get_Addenda: String;
begin
  if FValues.ValueExists('Addenda') then
    Result := FValues.Value['Addenda']
  else
    Result := '';
end;

procedure TevManualAchTransaction.Set_Addenda(Value: String);
begin
  FValues.AddValue('Addenda', Value);
end;

end.
