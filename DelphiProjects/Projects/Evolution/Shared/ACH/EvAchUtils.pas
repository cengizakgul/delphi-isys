unit EvAchUtils;

interface

uses SysUtils, DB, Classes, EvConsts, EvContext, evBasicUtils,
  DateUtils, Variants,  evStreamUtils, SReportSettings, StrUtils,
  evUtils, SDataStructure, evTypes, evCommonInterfaces, evDataset,
  SDDClasses, isBaseClasses, isBasicUtils, SACHTypes, EvClientDataSet;

const
  ACH_FILE_HEADER    = 1;
  ACH_BATCH_HEADER   = 5;
  ACH_ENTRY_DETAIL   = 6;
  ACH_ADDENDA_RECORD = 7;
  ACH_BATCH_TOTAL    = 8;
  ACH_FILE_TOTAL     = 9;
  ACH_END_OF_FILE    = 99;

  ACH_FILE_HEADER_STRING = '1:File Header';
  ACH_BATCH_HEADER_STRING = '5:Batch Header';
  ACH_ENTRY_DETAIL_STRING = '6:Entry Detail';
  ACH_ADDENDA_RECORD_STRING = '7:Addenda Record';
  ACH_BATCH_TOTAL_STRING = '8:Batch Total';
  ACH_FILE_TOTAL_STRING = '9:File Total';
  ACH_END_OF_FILE_STRING = '99:End-Of-File';

  TRAN_TYPE_COMPANY = '0';
  TRAN_TYPE_OFFSET = '1';
  TRAN_TYPE_OUT = '2';

const
  sDummy     = '9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999';

function ZeroFillInt64ToStr(n: Int64; len: Integer): string;
function ZeroFillIntToStr(n: Integer; len: Integer): string;
procedure IncHashTotal(aABA: string; var aTotal: Int64);
function ACHTrim(S: string): string;
function GetFromFiller(aField: TField; FieldName: string): string;
procedure SaveToFiller(aField: TField; FieldName, NewValue: string);
function AchReportStatusStr(Index: Integer): String;
function AchPayrollNewStatus(aStatus: string; Index: Integer): string;
function IsPayrollPeople: Boolean;
function GetAchAccountType(aBankType: String): String;
function GetAchTransactionType(In_Prenote: String; aAmount: Currency; NbrOfAddendaRecords: Integer = 1): String;
procedure CalculateACHDates(var CheckDate: TDateTime; var EffectiveDate: TDateTime;
  NumberOfDaysPrior: Integer);
function TaxIs941(const AType: String): Boolean;
function GetCommentForNonAchTransaction(PAYMENT_TYPE: String): String;

type
  TevAccountLevel = (lvCompany, lvDivision, lvBranch, lvDepartment, lvTeam);

function GenerateReport(Dataset: TevClientDataSet; Preprocess, Details, ShowAll: Boolean;
  DateFrom, DateTo: TDateTime; ACHFolder, FileName: String): IevDualStream;
function GetDirectDeposits(const PrNbr: Integer; const ProcessDate: TDateTime; const BlockNegativeChecks, SbUsePrenote, ReversalACH: Boolean): IisParamsCollection;
function GetChildSupport(const PrNbr: Integer; const ProcessDate: TDateTime; const BlockNegativeChecks, SbUsePrenote, ReversalACH: Boolean): IisParamsCollection;
function GetAgencyDirectDeposits(const PrNbr: Integer; const BlockNegativeChecks, ReversalACH: Boolean): IisParamsCollection;
function GetGarnishmentDirectDeposits(const PrNbr: Integer; const BlockNegativeChecks, ReversalACH: Boolean): IisParamsCollection;
function ReadLiabilityStatusCode(StatusFromIndex: Integer): String;
function CalculateACHEffectiveDate(ACheckDate: TDateTime; NumberOfDaysPrior: Integer): TDateTime;
function FormatAchAmount(const Amount: Currency): String;
function RemovePunctuation(const Value: String): String;

implementation

function GetDirectDeposits(const PrNbr: Integer; const ProcessDate: TDateTime; const BlockNegativeChecks, SbUsePrenote, ReversalACH: Boolean): IisParamsCollection;
var
  Q: IevQuery;
  L: IisListOfValues;
  CType: String;
begin
  Result := TisParamsCollection.Create;
  Q := TevQuery.Create('EEDirDep');
  Q.Params.AddValue('StatusDate', ProcessDate);

  if BlockNegativeChecks then
    Q.Macros.AddValue('CustomCondition', '(l.amount >= 0 or l.amount is null) and ');

  Q.Params.AddValue('PrNbr', PrNbr);

  if Q.Result.RecordCount > 0 then
  begin
    Q.Result.First;

    while not Q.Result.Eof do
    begin
      if not (ReversalACH and (Q.Result.FieldByName('IN_PRENOTE').AsString = GROUP_BOX_YES)) then
      begin
        L := Result.AddParams;
        L.AddValue('CUSTOM_EMPLOYEE_NUMBER', Q.Result.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        L.AddValue('SSN', Q.Result.FieldByName('SOCIAL_SECURITY_NUMBER').AsString);

        if Q.Result.FieldByName('MIDDLE_INITIAL').IsNull then
          L.AddValue('NAME', Q.Result.FieldByName('FIRST_NAME').AsString + ' ' + Q.Result.FieldByName('LAST_NAME').AsString)
        else
          L.AddValue('NAME', Q.Result.FieldByName('FIRST_NAME').AsString + ' ' + Q.Result.FieldByName('MIDDLE_INITIAL').AsString + '. ' +
            Q.Result.FieldByName('LAST_NAME').AsString);

        L.AddValue('DEDUCT_WHOLE_CHECK', Q.Result.FieldByName('DEDUCT_WHOLE_CHECK').AsString);
        L.AddValue('CHECK_TYPE', Q.Result.FieldByName('CHECK_TYPE').AsString); // for DBDT distribution

        if not SbUsePrenote then
          L.AddValue('IN_PRENOTE', 'N')
        else
          L.AddValue('IN_PRENOTE', Q.Result.FieldByName('IN_PRENOTE').AsString);

        if Q.Result.FieldByName('SKIP_HOURS').AsString = 'N' then
        begin
          CType := '';
          if (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'MA')
          or (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'MB') then
            CType := 'R'
          else
          if (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'DL')
          or (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'DM')
          or (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'DV') then
            CType := 'X'
          else
          if (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'D-')
          or (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'D5')
          or (Q.Result.FieldByName('E_D_CODE_TYPE').AsString = 'D6') then
            CType := 'E';

          if CType <> '' then
            L.AddValue('HSA', 'E' + CType);
        end;

        if (Q.Result.FieldByName('IN_PRENOTE').AsString = GROUP_BOX_YES) and SbUsePrenote then
        begin
          L.AddValue('AMOUNT', 0);
        end
        else
        if not ReversalACH then
          L.AddValue('AMOUNT', Q.Result.FieldByName('AMOUNT').AsCurrency)
        else
          L.AddValue('AMOUNT', -Q.Result.FieldByName('AMOUNT').AsCurrency);

        L.AddValue('BANK_ACCOUNT_TYPE', Q.Result.FieldByName('EE_BANK_ACCOUNT_TYPE').AsString);

        L.AddValue('BANK_ACCOUNT_NUMBER', Q.Result.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString);

        if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
          L.Value['BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['BANK_ACCOUNT_NUMBER']);

        L.AddValue('ABA_NUMBER', Q.Result.FieldByName('EE_ABA_NUMBER').AsString);

        if (Q.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL)
        and not DM_COMPANY.CO.MANUAL_CL_BANK_ACCOUNT_NBR.IsNull then
        begin
          L.AddValue('MODE', 'MANUAL');
          L.AddValue('DIVISION', '');
          L.AddValue('BRANCH', '');
          L.AddValue('DEPARTMENT', '');
          L.AddValue('TEAM', '');
        end
        else
        begin
          L.AddValue('MODE', 'DD');
          L.AddValue('DIVISION', Q.Result.FieldByName('DIVISION').AsString);
          L.AddValue('BRANCH', Q.Result.FieldByName('BRANCH').AsString);
          L.AddValue('DEPARTMENT', Q.Result.FieldByName('DEPARTMENT').AsString);
          L.AddValue('TEAM', Q.Result.FieldByName('TEAM').AsString);
        end;

      end;
      Q.Result.Next;
    end;
  end;
end;

function FormatAchAmount(const Amount: Currency): String;
begin
  Result := ZeroFillInt64ToStr(RoundInt64(Abs(Amount) * 100), 10);
end;

function RemovePunctuation(const Value: String): String;
var
  i: Integer;
begin
  Result := Value;
  i := 1;
  while i <= Length(Result) do
  begin
    if Result[i] in [' ',',','''','"','-','.'] then
      System.Delete(Result, i, 1)
    else
      Inc(i);
  end;
end;

function ConvertSSN(SSN: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(SSN) do
    if SSN[i] in ['0'..'9'] then
      Result := Result + SSN[i];
end;

function GetBatchUniqID: String;
begin
  Result := Format('%10.10d', [Random(9999999999)]);
end;

function GetChildSupport(const PrNbr: Integer; const ProcessDate: TDateTime; const BlockNegativeChecks, SbUsePrenote, ReversalACH: Boolean): IisParamsCollection;
var
  Q, A: IevQuery;
  L: IisListOfValues;
  AGENCY_NAME: String;
  MedIns: String;
begin
  Result := TisParamsCollection.Create;
  Q := TevQuery.Create('CSDirDep');
  Q.Params.AddValue('StatusDate', ProcessDate);

  if BlockNegativeChecks then
    Q.Macros.AddValue('CustomCondition', '(l.amount >= 0 or l.amount is null) and ');
  Q.Params.AddValue('PrNbr', PrNbr);
  if SbUsePrenote then
    Q.Macros.AddValue('SbUsePrenote', '1=1 or')
  else
    Q.Macros.AddValue('SbUsePrenote', '1=2 or ');
  if Q.Result.RecordCount > 0 then
  begin


    Q.Result.First;
    while not Q.Result.Eof do
    begin
      if not (ReversalACH and (Q.Result.FieldByName('IN_PRENOTE').AsString = GROUP_BOX_YES)) then
      begin
        L := Result.AddParams;
        L.AddValue('CUSTOM_EMPLOYEE_NUMBER', Q.Result.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        L.AddValue('SSN', Q.Result.FieldByName('SOCIAL_SECURITY_NUMBER').AsString);

        if Q.Result.FieldByName('IN_PRENOTE').AsString = GROUP_BOX_YES then
          L.AddValue('AMOUNT', 0)
        else
        if not ReversalACH then
          L.AddValue('AMOUNT', Q.Result.FieldByName('AMOUNT').AsCurrency)
        else
          L.AddValue('AMOUNT', -Q.Result.FieldByName('AMOUNT').AsCurrency);

        L.AddValue('BANK_ACCOUNT_TYPE', 'C');
        L.AddValue('ORIGINATION_STATE', Q.Result.FieldByName('ORIGINATION_STATE').AsString);
        
        L.AddValue('IN_PRENOTE', Q.Result.FieldByName('IN_PRENOTE').AsString);
        L.AddValue('BANK_ACCOUNT_NUMBER', Q.Result.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString);
        if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
          L.Value['BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['BANK_ACCOUNT_NUMBER']);

        L.AddValue('ABA_NUMBER', Q.Result.FieldByName('EE_ABA_NUMBER').AsString);

        if (Q.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL)
        and not DM_COMPANY.CO.MANUAL_CL_BANK_ACCOUNT_NBR.IsNull then
        begin
          L.AddValue('MODE', 'MANUAL');
          L.AddValue('DIVISION', '');
          L.AddValue('BRANCH', '');
          L.AddValue('DEPARTMENT', '');
          L.AddValue('TEAM', '');
        end
        else
        begin
          L.AddValue('MODE', 'DD');
          L.AddValue('DIVISION', Q.Result.FieldByName('DIVISION').AsString);
          L.AddValue('BRANCH', Q.Result.FieldByName('BRANCH').AsString);
          L.AddValue('DEPARTMENT', Q.Result.FieldByName('DEPARTMENT').AsString);
          L.AddValue('TEAM', Q.Result.FieldByName('TEAM').AsString);
        end;

        L.AddValue('ADDENDA', Q.Result.FieldByName('ADDENDA').AsString);

        if Q.Result.FieldByName('SB_AGENCY_NBR').AsInteger <> 0 then
        begin
          A := TevQuery.Create('select NAME from SB_AGENCY where {AsOfNow<SB_AGENCY>} and SB_AGENCY_NBR='+Q.Result.FieldByName('SB_AGENCY_NBR').AsString);
          AGENCY_NAME := A.Result.FieldByName('NAME').AsString;
        end
        else
        begin
          AGENCY_NAME := '';
        end;

        L.AddValue('NAME', AGENCY_NAME);
        if Q.Result.FieldByName('IL_MED_INS_ELIGIBLE').AsString = GROUP_BOX_NOT_APPLICATIVE then
          MedIns := GROUP_BOX_YES
        else
          MedIns := Q.Result.FieldByName('IL_MED_INS_ELIGIBLE').AsString;
        L.AddValue('Addenda',
          Format('705DED*CS*%-1.20s*', [Q.Result.FieldByName('CUSTOM_CASE_NUMBER').AsString]) +
          FormatDateTime('yymmdd"*"', Q.Result.FieldByName('CHECK_DATE').AsDateTime) +
          Format('%1.10s*%9.9s*%1.1s*%-0.10s*%-0.7s*',
            [IntToStr(RoundInt64(Abs(Q.Result.FieldByName('AMOUNT').AsCurrency) * 100)),
             ConvertSSN(Q.Result.FieldByName('SOCIAL_SECURITY_NUMBER').AsString),
             MedIns,
             Q.Result.FieldByName('LAST_NAME').AsString + ',' + Q.Result.FieldByName('FIRST_NAME').AsString,
             Q.Result.FieldByName('FIPS_CODE').AsString]));
        if Q.Result.FieldByName('CURRENT_TERMINATION_CODE').AsString = EE_TERM_ACTIVE then
          L.Value['Addenda'] := L.Value['Addenda'] + '\'
        else
          L.Value['Addenda'] := L.Value['Addenda'] + 'Y\';
        L.Value['Addenda'] := PadRight(UpperCase(L.Value['Addenda']), ' ', 83)+'0001';
      end;
      Q.Result.Next;
    end;
  end;
end;

function GetGarnishmentDirectDeposits(const PrNbr: Integer; const BlockNegativeChecks, ReversalACH: Boolean): IisParamsCollection;
const
  AgencyName = 'MN Department Of Revenue';
  GarnDD = 'SELECT '+
      'e.CUSTOM_EMPLOYEE_NUMBER,'+
      'f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME,'+
      'f.SOCIAL_SECURITY_NUMBER, ' +
      'l.AMOUNT,'+
      'b.CUSTOM_BANK_ACCOUNT_NUMBER,'+
      'b.BANK_ACCOUNT_TYPE,'+
      'b.SB_BANKS_NBR,'+
      'k.CHECK_TYPE,'+
      'p.CHECK_DATE, '+
      'p.CHECK_DATE,' +
      'l.CO_DIVISION_NBR DIVISION,'+
      'l.CO_BRANCH_NBR BRANCH,'+
      'l.CO_DEPARTMENT_NBR DEPARTMENT,'+
      'l.CO_TEAM_NBR TEAM,'+
      'n.GARNISNMENT_ID, ' +
      'a.SB_AGENCY_NBR '+
      'FROM PR                     p ' +
      'JOIN PR_CHECK               k on k.pr_nbr=p.pr_nbr ' +
      'JOIN EE                     e on e.ee_nbr=k.ee_nbr ' +
      'JOIN PR_CHECK_LINES         l on l.pr_check_nbr=k.pr_check_nbr ' +      
      'JOIN CL_PERSON              f on f.cl_person_nbr=e.cl_person_nbr ' +
      'JOIN EE_SCHEDULED_E_DS      n on n.ee_scheduled_e_ds_nbr=l.ee_scheduled_e_ds_nbr ' +
      'JOIN CL_AGENCY              a on a.CL_AGENCY_NBR=n.CL_AGENCY_NBR ' +
      'JOIN CL_E_DS                c on c.cl_e_ds_nbr=n.cl_e_ds_nbr ' +
      'JOIN CL_BANK_ACCOUNT        b on b.CL_BANK_ACCOUNT_NBR=a.CL_BANK_ACCOUNT_NBR '+
    'WHERE #CustomCondition '+
      'p.pr_nbr=:PrNbr and ' +
      '{AsOfNow<p>} and ' +      
      '{AsOfNow<k>} and '+
      '{AsOfNow<e>} and ' +
      '{AsOfNow<l>} and ' +      
      '{AsOfNow<f>} and ' +
      '{AsOfNow<n>} and ' +
      '{AsOfNow<a>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<b>} and ' +
      'a.SB_AGENCY_NBR = :AgencyNbr and ' +
      'k.check_type in ('''+CHECK_TYPE2_REGULAR+''','''+CHECK_TYPE2_VOID+''') and ' +
      'c.E_D_CODE_TYPE = '''+ED_DED_GARNISH+''' ' +
      'ORDER BY k.ee_nbr';

var
  Q: IevQuery;
  L: IisListOfValues;
  AgencyNbr: Integer;
  StateEIN: String;
begin
  Result := TisParamsCollection.Create;

  Q := TevQuery.Create('select SB_AGENCY_NBR from SB_AGENCY where NAME='''+AgencyName+''' and {AsOfNow<SB_AGENCY>}');

  if Q.Result.RecordCount > 0 then
    AgencyNbr := Q.Result['SB_AGENCY_NBR']
  else
    AgencyNbr := 0;

  Q := TevQuery.Create('select s.STATE_EIN from co_states s join pr p on p.co_nbr=s.co_nbr where state=''MN'' and p.pr_nbr=:PrNbr and {AsOfNow<s>} and {AsOfNow<p>}');
  Q.Params.AddValue('PrNbr', PrNbr);
  StateEIN := ConvertNull(Q.Result['STATE_EIN'], 0);

  Q := TevQuery.Create(GarnDD);
  if BlockNegativeChecks then
    Q.Macros.AddValue('CustomCondition', '(l.amount >= 0 or l.amount is null) and ');
  Q.Params.AddValue('PrNbr', PrNbr);
  Q.Params.AddValue('AgencyNbr', AgencyNbr);

  if Q.Result.RecordCount > 0 then
  begin
    Q.Result.First;

    while not Q.Result.Eof do
    begin
      L := Result.AddParams;

      DM_SERVICE_BUREAU.SB_AGENCY.DataRequired('SB_AGENCY_NBR='+Q.Result.FieldByName('SB_AGENCY_NBR').AsString);
      DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+DM_SERVICE_BUREAU.SB_AGENCY.SB_BANKS_NBR.AsString);

//      if Pos('C', Q.Result.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0 then
//        L.AddValue('NAME', Q.Result.FieldByName('NAME').AsString)
//      else
        L.AddValue('NAME', DM_SERVICE_BUREAU.SB_AGENCY.NAME.AsString);

      L.AddValue('BANK_ACCOUNT_NUMBER', DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_NUMBER.AsString);
      L.AddValue('BANK_ACCOUNT_TYPE', DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_TYPE.AsString);
      L.AddValue('IN_PRENOTE', 'N');

      if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
        L.Value['BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['BANK_ACCOUNT_NUMBER']);

      L.AddValue('ABA_NUMBER', ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString));

      DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+Q.Result.FieldByName('SB_BANKS_NBR').AsString);
      L.AddValue('CLIENT_ABA_NUMBER', ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString));

      L.AddValue('CLIENT_BANK_ACCOUNT_NUMBER', Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
      if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
        L.Value['CLIENT_BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['CLIENT_BANK_ACCOUNT_NUMBER']);

      if not ReversalACH then
        L.AddValue('AMOUNT', Q.Result.FieldByName('AMOUNT').AsCurrency)
      else
        L.AddValue('AMOUNT', -Q.Result.FieldByName('AMOUNT').AsCurrency);

      L.AddValue('CLIENT_BANK_ACCOUNT_TYPE', Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString);

      L.AddValue('Addenda',
        Format('705TPP*209*%s*', [StateEIN]) +
        FormatDateTime('yyyymmdd"*"', Q.Result.FieldByName('CHECK_DATE').AsDateTime) +
        Format('%1.10s*%9.9s*%-0.13s*L%-0.10s*',
          [IntToStr(RoundInt64(Abs(Q.Result.FieldByName('AMOUNT').AsCurrency) * 100)),
           ConvertSSN(Q.Result.FieldByName('SOCIAL_SECURITY_NUMBER').AsString),
           Q.Result.FieldByName('LAST_NAME').AsString + ',' + Q.Result.FieldByName('FIRST_NAME').AsString,
           Trim(Q.Result.FieldByName('GARNISNMENT_ID').AsString)]));

      L.Value['Addenda'] := L.Value['Addenda'] + '\';
      L.Value['Addenda'] := PadRight(UpperCase(L.Value['Addenda']), ' ', 83)+'0001';

      Q.Result.Next;
    end;
  end;
end;

function GetAgencyDirectDeposits(const PrNbr: Integer; const BlockNegativeChecks, ReversalACH: Boolean): IisParamsCollection;
var
  Q, QG: IevQuery;
  L: IisListOfValues;
  AgencyNbr: Integer;
const
  AgencyName = 'MN Department Of Revenue';
  AgencyDirDep =
      'select a.SB_BANKS_NBR,'+
      'a.CUSTOM_BANK_ACCOUNT_NUMBER,'+
      'a.BANK_ACCOUNT_TYPE,' +
      'g.SB_AGENCY_NBR,'+
      'g.PAYMENT_TYPE,'+
      'c.NAME,' +
      'g.CLIENT_AGENCY_CUSTOM_FIELD,'+
      'm.MISCELLANEOUS_CHECK_TYPE,'+
      'm.MISCELLANEOUS_CHECK_AMOUNT, ' +
      'm.PR_MISCELLANEOUS_CHECKS_NBR ' +
      'FROM PR                      s ' +
      'JOIN CO                      c on c.CO_NBR = s.CO_NBR ' +
      'JOIN PR_MISCELLANEOUS_CHECKS m on m.PR_NBR = :PrNbr ' +
      'JOIN CL_AGENCY               g on g.CL_AGENCY_NBR = m.CL_AGENCY_NBR ' +
      'JOIN CL_BANK_ACCOUNT         a on a.CL_BANK_ACCOUNT_NBR = g.CL_BANK_ACCOUNT_NBR ' +
    'WHERE '+
      's.PR_NBR=:PrNbr and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<g>} and ' +
      '{AsOfNow<a>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<m>} and ' +
      'm.MISCELLANEOUS_CHECK_AMOUNT <> 0 and ' +
      '#CustomCondition ' +
      'm.MISCELLANEOUS_CHECK_TYPE in (''L'', ''G'') and ' +
      'g.PAYMENT_TYPE=''D'' ' +
      'ORDER BY a.SB_BANKS_NBR';

  GarnDD = 'SELECT '+
      'e.CUSTOM_EMPLOYEE_NUMBER,'+
      'f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME,'+
      'f.SOCIAL_SECURITY_NUMBER, ' +
      'l.AMOUNT,'+
      'b.CUSTOM_BANK_ACCOUNT_NUMBER,'+
      'b.BANK_ACCOUNT_TYPE,'+
      'b.SB_BANKS_NBR,'+
      'k.CHECK_TYPE,'+
      'p.CHECK_DATE, '+
      'p.CHECK_DATE,' +
      'l.CO_DIVISION_NBR DIVISION,'+
      'l.CO_BRANCH_NBR BRANCH,'+
      'l.CO_DEPARTMENT_NBR DEPARTMENT,'+
      'l.CO_TEAM_NBR TEAM,'+
      'n.GARNISNMENT_ID, ' +
      'a.SB_AGENCY_NBR, '+
      'l.PR_MISCELLANEOUS_CHECKS_NBR ' +
      'FROM PR                     p ' +
      'JOIN PR_CHECK               k on k.pr_nbr=p.pr_nbr ' +
      'JOIN EE                     e on e.ee_nbr=k.ee_nbr ' +
      'JOIN PR_CHECK_LINES         l on l.pr_check_nbr=k.pr_check_nbr ' +
      'JOIN CL_PERSON              f on f.cl_person_nbr=e.cl_person_nbr ' +
      'JOIN EE_SCHEDULED_E_DS      n on n.ee_scheduled_e_ds_nbr=l.ee_scheduled_e_ds_nbr ' +
      'JOIN CL_AGENCY              a on a.CL_AGENCY_NBR=n.CL_AGENCY_NBR ' +
      'JOIN CL_E_DS                c on c.cl_e_ds_nbr=n.cl_e_ds_nbr ' +
      'JOIN CL_BANK_ACCOUNT        b on b.CL_BANK_ACCOUNT_NBR=a.CL_BANK_ACCOUNT_NBR '+
    'WHERE #CustomCondition '+
      'p.pr_nbr=:PrNbr and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<k>} and '+
      '{AsOfNow<e>} and ' +
      '{AsOfNow<l>} and ' +
      '{AsOfNow<f>} and ' +
      '{AsOfNow<n>} and ' +
      '{AsOfNow<a>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<b>} and ' +
      'a.SB_AGENCY_NBR = :AgencyNbr and ' +
      'k.check_type in ('''+CHECK_TYPE2_REGULAR+''','''+CHECK_TYPE2_VOID+''') and ' +
      'c.E_D_CODE_TYPE = '''+ED_DED_GARNISH+''' ' +
      'ORDER BY k.ee_nbr';


begin
  Result := TisParamsCollection.Create;

  Q := TevQuery.Create('select SB_AGENCY_NBR from SB_AGENCY where NAME='''+AgencyName+''' and {AsOfNow<SB_AGENCY>}');

  if Q.Result.RecordCount > 0 then
    AgencyNbr := Q.Result['SB_AGENCY_NBR']
  else
    AgencyNbr := 0;

  QG := TevQuery.Create(GarnDD);
  if BlockNegativeChecks then
    QG.Macros.AddValue('CustomCondition', '(l.amount >= 0 or l.amount is null) and ');
  QG.Params.AddValue('PrNbr', PrNbr);
  QG.Params.AddValue('AgencyNbr', AgencyNbr);

  Q := TevQuery.Create(AgencyDirDep);
  if BlockNegativeChecks then
    Q.Macros.AddValue('CustomCondition', '(m.MISCELLANEOUS_CHECK_AMOUNT >= 0 or m.MISCELLANEOUS_CHECK_AMOUNT is null) and ');
  Q.Params.AddValue('PrNbr', PrNbr);

  if Q.Result.RecordCount > 0 then
  begin

    while QG.Result.RecordCount > 0 do
    begin
      if Q.Result.Locate('PR_MISCELLANEOUS_CHECKS_NBR', QG.Result['PR_MISCELLANEOUS_CHECKS_NBR'], []) then
      begin
        Q.Result.Edit;
        Q.Result['MISCELLANEOUS_CHECK_AMOUNT'] := Q.Result['MISCELLANEOUS_CHECK_AMOUNT'] - QG.Result['AMOUNT'];
        Q.Result.Post;
        if Q.Result['MISCELLANEOUS_CHECK_AMOUNT'] = 0 then
          Q.Result.Delete;
      end;
      QG.Result.Delete;
    end;

    Q.Result.First;

    while not Q.Result.Eof do
    begin
      L := Result.AddParams;

      DM_SERVICE_BUREAU.SB_AGENCY.DataRequired('SB_AGENCY_NBR='+Q.Result.FieldByName('SB_AGENCY_NBR').AsString);
      DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+DM_SERVICE_BUREAU.SB_AGENCY.SB_BANKS_NBR.AsString);

      if Pos('C', Q.Result.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0 then
        L.AddValue('NAME', Q.Result.FieldByName('NAME').AsString)
      else
        L.AddValue('NAME', DM_SERVICE_BUREAU.SB_AGENCY.NAME.AsString);

      L.AddValue('BANK_ACCOUNT_NUMBER', DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_NUMBER.AsString);
      L.AddValue('BANK_ACCOUNT_TYPE', DM_SERVICE_BUREAU.SB_AGENCY.ACCOUNT_TYPE.AsString);
      L.AddValue('IN_PRENOTE', 'N');

      if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
        L.Value['BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['BANK_ACCOUNT_NUMBER']);

      L.AddValue('ABA_NUMBER', ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString));

      DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+Q.Result.FieldByName('SB_BANKS_NBR').AsString);
      L.AddValue('CLIENT_ABA_NUMBER', ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString));

      L.AddValue('CLIENT_BANK_ACCOUNT_NUMBER', Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
      if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
        L.Value['CLIENT_BANK_ACCOUNT_NUMBER'] := ACHTrim(L.Value['CLIENT_BANK_ACCOUNT_NUMBER']);

      if not ReversalACH then
        L.AddValue('AMOUNT', Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency)
      else
        L.AddValue('AMOUNT', -Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency);

      L.AddValue('CLIENT_BANK_ACCOUNT_TYPE', Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString);

      L.AddValue('CHECK_TYPE', Q.Result.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString);

      Q.Result.Next;
    end;
  end;
end;

function ReadLiabilityStatusCode(StatusFromIndex: Integer): String;
begin
  case StatusFromIndex of
    PR_ACH_STATUS_FROM_READY,
    PR_ACH_STATUS_FROM_HOLD,
    PR_ACH_STATUS_FROM_READY_OR_HOLD,
    PR_ACH_STATUS_FROM_DELETED: Result := 'status = ''' + TAX_DEPOSIT_STATUS_PENDING + ''' or status = ''' + TAX_DEPOSIT_STATUS_PAID_WO_FUNDS + '''';
    PR_ACH_STATUS_FROM_SENT: Result := 'status = ''' + TAX_DEPOSIT_STATUS_IMPOUNDED + ''' or status = ''' + TAX_DEPOSIT_STATUS_PAID + ''' or status = ''' + TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE + '''';
  end;
end;

function GenerateReport(Dataset: TevClientDataSet; Preprocess, Details, ShowAll: Boolean;
  DateFrom, DateTo: TDateTime; ACHFolder, FileName: String): IevDualStream;
var
  ReportParams: TrwReportParams;
  cds: TevClientDataSet;
  ShowFileName, ProcessDates: String;
begin

  ProcessDates := 'PROCESS ' + 'DATES: ' + FormatDateTime('MM/DD/YY', DateFrom) + '  TO: ' + FormatDateTime('MM/DD/YY', DateTo);

  if not Preprocess then
    ShowFileName := 'FILE NAME: '+ ACHFolder + '\' + FileName
  else
  if copy(FileName, 1, 7) = 'NON ACH' then
    ShowFileName := FileName
  else
    ShowFileName := 'FILE NAME: (not yet saved)';

  ctx_RWLocalEngine.StartGroup;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''ACH Report''');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'ACH Report', [loCaseInsensitive]) then
      Exit;
    ReportParams := TrwReportParams.Create;
    cds := TevClientDataSet.Create(nil);
    try
      ctx_RWLocalEngine.StartGroup;
      ReportParams.Add('ShowEverything', IntToStr(Integer(ShowAll)));
      ReportParams.Add('PrintAchDetail', IntToStr(Integer(Details)));
      ReportParams.Add('ProcDates', ProcessDates);
      try
        Dataset.First;
        cds.Data := Dataset.Data;
        ReportParams.Add('DataSets', DataSetsToVarArray([cds]));
        ReportParams.Add('FileName', ShowFileName);

        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
          CH_DATABASE_SYSTEM, ReportParams);

      finally
        ctx_RWLocalEngine.EndGroup(rdtPreview);
      end;

    finally
      cds.Free;
      ReportParams.Free;
    end;
  finally
    Result := ctx_RWLocalEngine.EndGroup(rdtNone);
  end;
end;

function GetCommentForNonAchTransaction(PAYMENT_TYPE: String): String;
begin
  if PAYMENT_TYPE = PAYMENT_TYPE_EXTERNAL_CHECK then
    Result := 'E'
  else if PAYMENT_TYPE = PAYMENT_TYPE_INTERNAL_CHECK then
    Result := 'I'
  else if PAYMENT_TYPE = PAYMENT_TYPE_WIRE_TRANSFER then
    Result := 'W'
  else if PAYMENT_TYPE = PAYMENT_TYPE_CASH then
    Result := 'C'
  else if PAYMENT_TYPE = PAYMENT_TYPE_OTHER then
    Result := 'O'
  else if PAYMENT_TYPE = PAYMENT_TYPE_ACH then
    Result := 'A'
  else if PAYMENT_TYPE = PAYMENT_TYPE_NONE then
    Result := 'N';
end;

function TaxIs941(const AType: String): Boolean;
begin
  Result := (AType = TAX_LIABILITY_TYPE_FEDERAL)
         or (AType = TAX_LIABILITY_TYPE_EE_OASDI)
         or (AType = TAX_LIABILITY_TYPE_ER_OASDI)
         or (AType = TAX_LIABILITY_TYPE_EE_MEDICARE)
         or (AType = TAX_LIABILITY_TYPE_ER_MEDICARE)
         or (AType = TAX_LIABILITY_TYPE_EE_EIC)
         or (AType = TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING)
         or (AType = TAX_LIABILITY_TYPE_COBRA_CREDIT);
end;

function CalculateACHEffectiveDate(ACheckDate: TDateTime; NumberOfDaysPrior: Integer): TDateTime;
var
  CheckDate, EffectiveDate: TDateTime;
begin
  CheckDate := ACheckDate;
  
  if CheckDate < Date then
    CheckDate := Date;

  EffectiveDate := CheckDate;
  if NumberOfDaysPrior > 0 then
    while (NumberOfDaysPrior > 0) and (Date <= EffectiveDate) do
    begin
      EffectiveDate := EffectiveDate - 1;
      if not ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) then
        Dec(NumberOfDaysPrior);
    end
  else
    while NumberOfDaysPrior < 0 do
    begin
      EffectiveDate := EffectiveDate + 1;
      if not ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) then
        Inc(NumberOfDaysPrior);
    end;

  if EffectiveDate < Date then
    EffectiveDate := Date;

  while ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) do
      EffectiveDate := EffectiveDate + 1;

  Result := EffectiveDate;
end;

procedure CalculateACHDates(var CheckDate: TDateTime; var EffectiveDate: TDateTime;
  NumberOfDaysPrior: Integer);
begin
  if CheckDate < Date then
    CheckDate := Date;

  EffectiveDate := CheckDate;
  if NumberOfDaysPrior > 0 then
    while NumberOfDaysPrior > 0 do
    begin
      EffectiveDate := EffectiveDate - 1;
      if not ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) then
        Dec(NumberOfDaysPrior);
    end
  else
    while NumberOfDaysPrior < 0 do
    begin
      EffectiveDate := EffectiveDate + 1;
      if not ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) then
        Inc(NumberOfDaysPrior);
    end;

//  EffectiveDate := CheckDate - NumberOfDaysPrior;

  if EffectiveDate < Date then
    EffectiveDate := Date;

  while ctx_PayrollCalculation.IsNonBusinessDay(EffectiveDate) do
      EffectiveDate := EffectiveDate + 1;
  while ctx_PayrollCalculation.IsNonBusinessDay(CheckDate) do
      CheckDate := CheckDate + 1
end;

function GetAchAccountType(aBankType: string): string;
begin
  {  Build 1st digit of transaction
      2_ = Checking or MoneyMarket
      3_ = Savings
   }
  if aBankType = RECEIVING_ACCT_TYPE_SAVING then
    Result := '3'
  else
    Result := '2'; // Default = Checking
end;

function GetAchTransactionType(In_Prenote: string; aAmount: Currency; NbrOfAddendaRecords: Integer = 1): string;
begin
  {  Build 2nd digit of transaction
         _2 = Credit        _3 = prenote credit
         _7 = Debit         _8 = prenote debit
      }
  if (In_Prenote = 'Y') or (aAmount = 0) then
  begin
    Result := '3'; // Default = Credit
    if (aAmount = 0) and (In_Prenote <> 'Y') then
      Result := '4';
    if (aAmount < 0) or (NbrOfAddendaRecords = -1) then
    begin
      if aAmount = 0 then
        Result := '9'
      else
        Result := '8';
    end;
  end
  else
  begin
    Result := '2'; // Default = Credit
    if aAmount < 0 then
      Result := '7';
  end;
end;

function IsPayrollPeople: Boolean;
begin
  Result := Abs(StrToInt(ctx_DomainInfo.LicenseInfo.SerialNumber)) mod 1000 in [31{, 36}];
end;

function AchPayrollNewStatus(aStatus: string; Index: Integer): string;
begin
  Result := aStatus;
  case Index of
    PR_ACH_STATUS_TO_SENT    : Result := COMMON_REPORT_STATUS__COMPLETED;
    PR_ACH_STATUS_TO_HOLD    : Result := COMMON_REPORT_STATUS__HOLD;
    PR_ACH_STATUS_TO_DELETED : Result := COMMON_REPORT_STATUS__DELETED;
    PR_ACH_STATUS_TO_READY   : Result := COMMON_REPORT_STATUS__PENDING;
  end;
end;

function AchReportStatusStr(Index: Integer): String;
begin
  case Index of
    0: Result := '''' + COMMON_REPORT_STATUS__PENDING + '''';
    1: Result := '''' + COMMON_REPORT_STATUS__HOLD + '''';
    2: Result := '''' + COMMON_REPORT_STATUS__PENDING + ''', ''' + COMMON_REPORT_STATUS__HOLD + '''';
    3: Result := '''' + COMMON_REPORT_STATUS__COMPLETED + '''';
    4: Result := '''' + COMMON_REPORT_STATUS__DELETED + '''';
    5: Result := '''' + COMMON_REPORT_STATUS__IN_PROCESS + '''';
  end;
end;

function GetFromFiller(aField: TField; FieldName: string): string;
var
  Temp: TStringList;
begin
  Temp := TStringList.Create;
  Temp.Text := aField.AsString;

  Result := Temp.Values[FieldName];
  Temp.Destroy;
end;

procedure SaveToFiller(aField: TField; FieldName, NewValue: string);
var
  Temp: TStringList;
  TableName: string;
begin
  Temp := TStringList.Create;

  Temp.Text := aField.AsString;

  if Temp.IndexOfName(FieldName) = -1 then
    Temp.Add(FieldName + '=' + NewValue)
  else
    Temp.Values[FieldName] := NewValue;

  if Length(Temp.Text) > aField.Size then
  begin
    TableName := '<unknown>';

    if Assigned(aField.DataSet) then
      TableName := aField.DataSet.Name;

    Assert(False, TableName + ':' + aField.FieldName + ' filler length has been exceeded');
  end;

  if not (aField.DataSet.State in [dsEdit, dsInsert]) then
    aField.DataSet.Edit;
  aField.AsString := Temp.Text;
  aField.DataSet.Post;

  Temp.Destroy;
end;

procedure IncHashTotal(aABA: string; var aTotal: Int64);
var
  iAba: Int64;
begin
  // make sure only first 8 digits are passed into total (9th is a check-digit)
  if Length(Trim(aABA)) = 9 then
    iAba := StrToInt(Copy(aABA, 1, 8))
  else
    iAba := StrToInt(aABA);

  aTotal := aTotal + iAba;
end;

function ZeroFillInt64ToStr(n: Int64; len: Integer): string;
begin
  // 123,7 = "0000123"
  Result := IntToStr(n);
  if Length(Result) > len then
    Delete(Result, 1, Length(Result) - len);
  while Length(Result) < len do
    Result := '0' + Result;
end;

function ZeroFillIntToStr(n: Integer; len: Integer): string;
begin
  // 123,7 = "0000123"
  Result := IntToStr(n);
  while Length(Result) < len do
    Result := '0' + Result;
end;

function ACHTrim(S: string): string;

  procedure GetRidOfChar(var S: string; X: string);
  var
    I: Integer;
  begin
    while True do
    begin
      I := Pos(X, S);
      if I = 0 then
        Break;
      Delete(S, I, 1);
    end;
  end;

begin
  Result := Trim(S);
  GetRidOfChar(Result, ' ');
  GetRidOfChar(Result, '-');
  GetRidOfChar(Result, '_');
  GetRidOfChar(Result, '.');
  GetRidOfChar(Result, ':');
  GetRidOfChar(Result, '<');
  GetRidOfChar(Result, '>');
  GetRidOfChar(Result, '^');
  GetRidOfChar(Result, '|');
  GetRidOfChar(Result, '~');
end;

end.
