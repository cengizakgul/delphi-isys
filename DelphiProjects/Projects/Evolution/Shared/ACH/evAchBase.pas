 // Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit evAchBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
   SReportSettings, EvTypes, Variants, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, IsBaseClasses, EvDataAccessComponents, ISDataAccessComponents, SACHTypes,
  EvCommonInterfaces, EvContext, isBasicUtils, EvAchDataSet, EvAchUtils, EvExceptions, EvClientDataSet;

type

  TevAchBase = class(TComponent)
  private
    procedure IncrementSubTotals(bIsAch: Boolean);
    procedure ClearBatchTotals;
    procedure SaveFileByName(const AFileName: string);
  protected
    FFileSaved: Boolean;
    DEST_Number: string;
    v: Variant;
    vi: array [0..2] of Int64;
    FTotNbrOfBatches: Integer;
    FTotEntryHash: Int64;
    FEntryHash: Int64;
    FSubCredits: Currency;
    FSubDebits: Currency;
    FTotDebits: Currency;
    FTotCredits: Currency;
    FxTotNbrOfEntries: Integer;
    SBID: string;
    FORIGIN_BANK_NBR: Integer;
    FTotNbrOfEntries: Integer;
    FDEST_BANK_NBR: Integer;
    FSubNbrOfBatches: Integer;
    FSubNbrOfEntries: Integer;
    FSubEntryHash: Int64;
    FACHFiles: IisListOfValues;
    FACHFile: TevAchDataSet;
    FFileName: String;
    FBtFileName: String;
    FPreProcess: Boolean;
    Combining: Boolean;
    BatchUniqID: String;
    PrNbr: Integer;
    AddendaNbr: Integer;
    EntryNbr: Integer;
    SegmentNbr: Integer;
    SavingACH: Boolean;
    CO_TAX_PAYMENT_ACH_NBR: Integer;
    CO_BANK_ACCOUNT_REGISTER: TevClientDataSet;
    CO_PR_ACH_NBR: Integer;
    CO_MANUAL_ACH_NBR: Integer;
    BatchBANK_REGISTER_TYPE: String;
    strFillerStamp: String;
    BatchSB_BANK_ACCOUNTS_NBR: Integer;
    FileHeaderProcessed: Boolean;
    TempCredits: Currency;
    TempDebits: Currency;
    DEST_BANK_ACCT: String;
    BANK_ACCOUNT_TYPE: String;
    FDetailAdded: Boolean;
    TotalTaxCredits: Currency;
    FPrenote: Boolean;
    FBatchCredits: Currency;
    FBatchDebits: Currency;
    function  CheckOffset(IsOffset: Integer): Boolean;
    procedure SetPrenote(const Value: Boolean);
    procedure DoOnConstruction; virtual;
    procedure MainProcess; virtual; abstract;
    procedure CTSCombineTransactions;
    procedure CombineTransactions;
    procedure AssignHeaderInfo;
    procedure BackupContext;
    procedure RestoreContext;
    procedure ClearGrandAndBatchTotals;
    procedure AddFileHeader;
    procedure AddFileHeaderIfNotAddedYet;
    procedure SaveFileSaveACH;
    procedure RecreateForColumbiaEDPACH;
    procedure WriteEOF(const addSubTotal: Boolean);
    procedure WriteACHDetail(cTranType: Char; NbrOfAddendaRecords: Integer;
      aCTSUI: Boolean; aNVSUI: Boolean; aMASUI: Boolean; aAZSUI, aNMSUI: Boolean);
    procedure AppendAddendaRecord(CUSTOM_EMPLOYEE_NUMBER: String; AAmount: Currency);
    procedure AppendDetailRecord(CUSTOM_EMPLOYEE_NUMBER: string; AAmount: Currency);

    procedure AppendFileHeaderRecord;
    procedure AppendFileTotalRecord;
    procedure WriteBatchHeader(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
    procedure WriteBatchTotal(
              aHoldTotals: Boolean;
              aSegment: Integer;
              aOrder: Integer;
              CUSTOM_EMPLOYEE_NUMBER: string;
              aIsOffset: Integer = OFF_DONT_USE;
              aHidden: Boolean = True;
              aCRLF: Boolean = False;
              aFEIN: String = '');
    procedure StartBatchDefinition(aClassCode: string; aEffDate: TDateTime;
      aComments: string; BankRegisterType: string; SB_BANK_ACCOUNTS_NBR: Integer);
  public
    ORIGIN_Number: string;  
    DEST_BANK_ABA: String;  
    DEST_Name: string;
    Options: TACHOptions;
    ReversalACH: Boolean;
    IncludeOffsets: Integer;
    Combine: TAchCombineOptions;
    OverrideBatchBANK_REGISTER_TYPE: string;
    fThreshold: Currency;
    bPrenote: Boolean;
    ORIGIN_BANK_ABA: String;
    ORIGIN_Name: string;        
    ACHFolder: String;
    constructor Create(AOwner: TComponent); override;
    procedure SetACHOptions(NewACHOptions: TACHOptions); overload;
    procedure SetACHOptions(const NewACHOptions: IevACHOptions); overload;
    procedure SaveAch; virtual;
    property FileSaved: Boolean read FFileSaved write FFileSaved;
    function GetSavedAchFileStrings(const AFileName: String): IisStringList;
    function ACHDescription: string; virtual; abstract;
    property ACHFile: TevAchDataSet read FACHFile;
    property ACHFiles: IisListOfValues read FACHFiles;
    property FileName: String read FFileName write FFileName;
    property Preprocess: Boolean read FPreprocess write FPreprocess;
    property InPrenote: Boolean read FPrenote write SetPrenote;
    destructor Destroy; override;
  end;

procedure GenerateAchReport(aACHFile, ShowAll, FileSaved: Boolean;
  const ACHFileName, ACHFolder: String; const AchDataSet: TevAchDataSet;
  const ACHDescription: String);

implementation

uses
  EvBasicUtils, EvUtils, EvConsts, SFieldCodeValues, SDataStructure, StrUtils,
  SDataDictbureau;

{ TevAchBase }

function TevAchBase.CheckOffset(IsOffset: Integer): Boolean;
begin
  Result := (IncludeOffsets = OFF_USE_OFFSETS) or
            (IncludeOffsets = OFF_DONT_USE) and (IsOffset = OFF_DONT_USE) or
            (IncludeOffsets = OFF_ONLY_EE_DD) and (IsOffset <> OFF_USE_OFFSETS);
end;

procedure TevAchBase.SaveFileByName(const AFileName: string);
var
  MyFile: IisStringList;
begin
  MyFile := ACHFile.GetAchTextFileStrings;
  FACHFiles.AddValue(AFileName, MyFile);
end;

procedure TevAchBase.AppendAddendaRecord(CUSTOM_EMPLOYEE_NUMBER: string; AAmount: Currency);
begin
//   This procedure appends an addenda record

  if ReversalACH then
    AAmount := -AAmount;

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_ADDENDA_RECORD_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  ACHFile.AMOUNT.Value                    := AAmount;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
    if Options.SBEINOverride = '' then
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString
    else
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride
  else if not Combining then
    ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FieldByName('FEIN').AsString
  else
    ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.FieldByName('RUN_NUMBER').Value;

  ACHFile.BATCH_NBR.Value := ZeroFillInt64ToStr(EntryNbr + 1, 7);
  Inc(AddendaNbr);
  Inc(FxTotNbrOfEntries);
  Inc(FTotNbrOfEntries);

  // Don't Post in this procedure (more fields will be assigned later)
end;

procedure TevAchBase.AppendDetailRecord(CUSTOM_EMPLOYEE_NUMBER: string; AAmount: Currency);

  procedure AddToBankAccountRegister(CO_PR_ACH_NBR, CO_TAX_PAYMENT_ACH_NBR, CO_MANUAL_ACH_NBR: Integer; BANK_REGISTER_TYPE: string;
    SB_BANK_ACCOUNTS_NBR: Integer; CHECK_DATE: TDateTime; EFFECTIVE_DATE: TDateTime; Amount: Currency; ID: string);
  begin
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;

    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').Value := SB_BANK_ACCOUNTS_NBR;
    if CO_TAX_PAYMENT_ACH_NBR <> 0 then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_PAYMENT_ACH_NBR').Value := CO_TAX_PAYMENT_ACH_NBR;
    if CO_MANUAL_ACH_NBR <> 0 then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_MANUAL_ACH_NBR').Value := CO_MANUAL_ACH_NBR;
    if CO_PR_ACH_NBR <> 0 then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_PR_ACH_NBR').Value := CO_PR_ACH_NBR;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value := CHECK_DATE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').Value := BANK_REGISTER_STATUS_Outstanding;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := EFFECTIVE_DATE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').Value := Amount;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').Value := BANK_REGISTER_TYPE;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').Value := BANK_REGISTER_MANUALTYPE_None;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').Value := Date;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').Value := PutIntoFiller('', ID, 18, 10);

    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
  end;

var
  s: string;
begin
//   This procedure appends a detail record

  if ReversalACH and not Combining then
    aAmount := -aAmount;

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_ENTRY_DETAIL_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  ACHFile.AMOUNT.Value                    := aAmount;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
    if Options.SBEINOverride = '' then
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString
    else
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride
  else if not Combining then
    ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FieldByName('FEIN').AsString
  else
    ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.FieldByName('RUN_NUMBER').Value;

  ACHFile.BATCH_NBR.Value := ZeroFillInt64ToStr(EntryNbr + 1, 7);
  ACHFile.BATCH_ID.Value := BatchUniqID;
  if aAmount < 0 then
    FBatchDebits := FBatchDebits + AAmount
  else
    if aAmount > 0 then
    FBatchCredits := FBatchCredits + AAmount;

  if SavingACH then
  begin
    if OverrideBatchBANK_REGISTER_TYPE <> '' then
      s := OverrideBatchBANK_REGISTER_TYPE
    else
      s := BatchBANK_REGISTER_TYPE;
    if (Length(s) > 0) and (BatchSB_BANK_ACCOUNTS_NBR > 0) then
      AddToBankAccountRegister(
        CO_PR_ACH_NBR, CO_TAX_PAYMENT_ACH_NBR, CO_MANUAL_ACH_NBR, s,
        BatchSB_BANK_ACCOUNTS_NBR, ACHFile.CHECK_DATE.AsDateTime,
        ACHFile.BatchEffectiveDate, aAmount, BatchUniqID);
  end;

  Inc(EntryNbr);
  Inc(FxTotNbrOfEntries);
  Inc(FTotNbrOfEntries);
  AddendaNbr := 0;

  // Don't Post in this procedure (more fields will be assigned later)
end;

procedure TevAchBase.AppendFileHeaderRecord;
begin
  ACHFile.Append;
  ACHFile.ACH_TYPE.Value                  := ACH_FILE_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N';

  ACHFile.CUSTOM_COMPANY_NUMBER.AsString := '                    ';
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
    if Options.SBEINOverride = '' then
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString
    else
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride
  else if not Combining then
    ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FieldByName('FEIN').AsString
  else
    ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.FieldByName('RUN_NUMBER').Value;

  ACHFile.ABA_NUMBER.AsString := DEST_Number;
  ACHFile.ABA_NUMBER2.AsString := DEST_Number;
  ACHFile.EIN_NUMBER.AsString := Copy(ORIGIN_Number, 2, 9);

  FileHeaderProcessed := True;
  ClearGrandAndBatchTotals;
end;

procedure TevAchBase.AppendFileTotalRecord;
begin
  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_FILE_TOTAL_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N';

  ACHFile.CUSTOM_COMPANY_NUMBER.AsString := 'zzzzzzzzzzzzzzzzzzzz';
  ACHFile.CHECK_DATE.Value := Date;
  ACHFile.RUN_NUMBER.Value := 0;
  ACHFile.CL_NBR.Value := MaxInt;

end;

procedure TevAchBase.AssignHeaderInfo;
var
  CustomHeader: string;

  procedure GetFromHeader(var MyString: string);
  begin
    if Pos('*', CustomHeader) <> 0 then
    begin
      if Pos('*', CustomHeader) <> 1 then
        MyString := Copy(CustomHeader, 1, Pos('*', CustomHeader) - 1);
      Delete(CustomHeader, 1, Pos('*', CustomHeader));
    end;
  end;

begin
  FDEST_BANK_NBR := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SB_BANKS_NBR').AsInteger;
  Dest_Bank_Acct := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
  if DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', FDEST_BANK_NBR, 'ALLOW_HYPHENS') <> GROUP_BOX_YES then
    Dest_Bank_Acct := ACHTrim(Dest_Bank_Acct);
  Bank_Account_Type := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('BANK_ACCOUNT_TYPE').AsString;
  FORIGIN_BANK_NBR := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('ACH_ORIGIN_SB_BANKS_NBR').AsInteger;
  CustomHeader := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('CUSTOM_HEADER_RECORD').AsString;
  Dest_Bank_Aba := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', FDEST_BANK_NBR, 'ABA_NUMBER');
  Dest_Bank_Aba := ACHTrim(Dest_Bank_Aba);
  Dest_Number := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', FORIGIN_BANK_NBR, 'ABA_NUMBER');
  Origin_Bank_Aba := Dest_Number;
  Origin_Bank_Aba := ACHTrim(Origin_Bank_Aba);
  Origin_Bank_Aba := Copy(Origin_Bank_Aba, 1, 8);
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);
  Origin_Name := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;
  Dest_Name := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', FORIGIN_BANK_NBR, 'PRINT_NAME');
  Origin_Number := PadLeft(DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString, ' ', 10);
  SBID := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', FORIGIN_BANK_NBR, 'CtsId');

  if CustomHeader <> '' then
  begin
    CustomHeader := CustomHeader + '*';
    GetFromHeader(Dest_Number);
    Dest_Number := PadLeft(Dest_Number, ' ', 9);
    Dest_Number := Copy(Dest_Number, 1, 9);
    GetFromHeader(Origin_Number);
    Origin_Number := PadLeft(Origin_Number, ' ', 10);
    Origin_Number := Copy(Origin_Number, 1, 10);
    GetFromHeader(Dest_Name);
    GetFromHeader(Origin_Name);
  end;
end;

procedure TevAchBase.BackupContext;
begin
  v[0] := EntryNbr;
  v[1] := FTotNbrOfEntries;
  v[2] := FBatchCredits;
  v[3] := FBatchDebits;
  // only clear items related to grand totals
  v[4] := FxTotNbrOfEntries;
  v[5] := FTotNbrOfBatches;
  v[6] := FTotCredits;
  v[7] := FTotDebits;
  // clear Sub-totals, too
  v[8] := FSubNbrOfBatches;
  v[9] := FSubNbrOfEntries;
  v[10] := FSubCredits;
  v[11] := FSubDebits;
  v[12] := ACHFile.Data;

  vi[0] := FEntryHash;
  vi[1] := FTotEntryHash;
  vi[2] := FSubEntryHash
end;

procedure TevAchBase.AddFileHeader;
begin
  // "1" File Header Record
  if Trim(Options.Header) <> '' then
  begin
    AppendFileHeaderRecord;
    ACHFile.NAME.Value := ORIGIN_Name;
    ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;
    ACHFile.ACH_LINE.AsString := Options.Header;
    ACHFile.Post;
  end;

  AppendFileHeaderRecord;

  ACHFile.NAME.Value := ORIGIN_Name;
  ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;
  ACHFile.ACH_LINE.AsString := UpperCase('101' + ' ' + Dest_Number
    + Origin_Number + FormatDateTime('YYMMDDHHMM', Now) + 'A094101'
    + Copy(Dest_Name + '                       ', 1, 23)
    + Copy(Origin_Name + '                       ', 1, 23)
    + ZeroFillInt64ToStr(0, 8));

  ACHFile.Post;
end;

procedure TevAchBase.AddFileHeaderIfNotAddedYet;
begin
  if not Assigned(ACHFile) then
    Exit;

  if not FileHeaderProcessed then
    AddFileHeader;
end;

procedure TevAchBase.ClearBatchTotals;
begin
  // only clear items related to batch totals
  EntryNbr := 0;
  FEntryHash := 0;
  FTotNbrOfEntries := 0;
  FBatchCredits := 0;
  FBatchDebits := 0;
end;

procedure TevAchBase.ClearGrandAndBatchTotals;
begin
  // only clear items related to grand totals
  FTotEntryHash := 0;
  FxTotNbrOfEntries := 0;
  FTotNbrOfBatches := 0;
  FTotCredits := 0;
  FTotDebits := 0;
  // clear Sub-totals, too
  FSubEntryHash := 0;
  FSubNbrOfBatches := 0;
  FSubNbrOfEntries := 0;
  FSubCredits := 0;
  FSubDebits := 0;
  FFileSaved := False;
  // clear items related to batch totals
  EntryNbr := 0;
  FEntryHash := 0;
  FTotNbrOfEntries := 0;
  FBatchCredits := 0;
  FBatchDebits := 0;
end;

procedure TevAchBase.CombineTransactions;
var
  DS: TevAchDataSet;
  fSum: Currency;
  dEffDate: TDateTime;
  TranType, EntryClassCode, Tran, AbaNumber, BankAccountNumber: string;
  Comments: string;
  CoNbr: Integer;
  s: string;

  function GetBatchComments(const ATranType, AName, ACoName, ACustEmpNbr, ABatchComments: String): String;
  begin
    if ATranType = TRAN_TYPE_OFFSET then
      Result := 'ACHOFFSET'
    else if AName = 'MANUAL TRANSFER' then
      Result := 'MANUAL'
    else if (ACustEmpNbr = '') and
            (ABatchComments <> 'NET=PAY') and
            (ABatchComments <> 'PAYROLL')
    then
      Result := 'IMPOUND'
    else
      Result := ABatchComments;
  end;

  function GetRecordName(const ATranType, AName, ACoName, ACustEmpNbr, ABatchComments: String): String;
  begin
    if ATranType = TRAN_TYPE_OFFSET then
      Result := 'ACH OFFSET-'+AName
    else if AName = 'MANUAL TRANSFER' then
      Result := 'MANUAL'
    else if TranType = TRAN_TYPE_COMPANY then
      Result := ACoName
    else if (ACustEmpNbr = '') and
            (ABatchComments <> 'NET=PAY') and
            (ABatchComments <> 'PAYROLL')
    then
      Result := 'IMPOUND'
    else
      Result := AName;
  end;

  procedure SaveRecordParamsAndInitSum;
  begin
    CoNbr              := DS.CO_NBR.AsInteger;
    dEffDate           := DS.EFFECTIVE_DATE.AsDateTime;
    EntryClassCode     := DS.ENTRY_CLASS_CODE.AsString;
    Tran               := DS.TRAN.AsString;
    TranType           := DS.TRAN_TYPE.AsString;
    AbaNumber          := DS.ABA_NUMBER.AsString;
    BankAccountNumber  := DS.BANK_ACCOUNT_NUMBER.AsString;
    fSum               := DS.AMOUNT.AsCurrency;
  end;

  procedure SetBatchUniqueIDToBAR(const ABatchID: String);
  begin
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
    if not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Eof then
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').AsString := PutIntoFiller(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').AsString, ABatchID, 18, 10);
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
    end;
  end;

  procedure RemoveZeroBatches;
  var
    sFields: String;
    sType: String;
  begin
    sFields := ACHFile.IndexFieldNames;
    ACHFile.IndexFieldNames := 'custom_company_number;cl_nbr;order_nbr';
    ACHFile. First;
    while not ACHFile.Eof do
    begin
      sType := LeftStr(ACHFile.FieldByName('ACH_TYPE').AsString, 1);
      if ((sType = '5'){ or (sType = '8')})
      and (ACHFile.FieldByName('AMOUNT').AsCurrency = 0)
      and (ACHFile.FieldByName('DEBITS').AsCurrency = 0)
      and (ACHFile.FieldByName('CREDITS').AsCurrency = 0) then
      begin
        ACHFile.Next;
        if not ACHFile.Eof then
        begin
          sType := LeftStr(ACHFile.FieldByName('ACH_TYPE').AsString, 1);
          if sType = '8' then
          begin
            ACHFile.Prior;
            ACHFile.Delete;
            ACHFile.Delete;
          end;
        end;
      end
      else
        ACHFile.Next;
    end;
    ACHFile.IndexFieldNames := sFields;
    ACHFile.First;
  end;

begin
  ctx_StartWait;
  try
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Close;
    DM_TEMPORARY.TMP_CL.DataRequired();
    DM_TEMPORARY.TMP_CO.DataRequired();
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    try
      DM_TEMPORARY.TMP_PR.DataRequired();
    finally
      DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    end;
    FileHeaderProcessed := False;
    ClearGrandAndBatchTotals;
    DS := TevAchDataSet.Create(nil);
    Combining := True;
    try

      DS.Data := ACHFile.Data;

      DS.First;
      while not DS.Eof do
      begin
        DS.Edit;
        if ((copy(DS.FieldByName('TRAN').AsString,2,1) <> '2')
        and (copy(DS.FieldByName('TRAN').AsString,2,1) <> '7')) then
          DS.FieldByName('COMB_AUX').AsString := '1'
        else
          DS.FieldByName('COMB_AUX').AsString := '0';
        DS.Post;
        DS.Next;
      end;

      DS.First;

      DS.IndexFieldNames := 'EFFECTIVE_DATE;TRAN_TYPE;ENTRY_CLASS_CODE;ABA_NUMBER;BANK_ACCOUNT_NUMBER;COMB_AUX;ORDER_NBR';

      
      // ACH entry detail or addenda record
      DS.Filter := 'ACH_TYPE = ''6:*'' or ACH_TYPE = ''7:*''';
      DS.Filtered := True;

      DS.First;      

      ACHFile.IndexFieldNames := '';
      ACHFile.EmptyDataSet;
      if DS.RecordCount = 0 then
        Exit;

      AddFileHeaderIfNotAddedYet;
      SaveRecordParamsAndInitSum;

      Comments := GetBatchComments(DS.TRAN_TYPE.AsString,
                                   DS.NAME.AsString,
                                   DM_TEMPORARY.TMP_CO.NAME.AsString,
                                   DS.CUSTOM_EMPLOYEE_NUMBER.AsString,
                                   DS.BATCH_COMMENTS.AsString);


      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
      DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
      PrNbr := DS['PR_NBR'];
      DM_COMPANY.CL.Open;
      DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.CO_NBR.AsString);
      DM_COMPANY.CO.Open;
      StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], Comments, '', 0);
      WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount, 0, TranType = TRAN_TYPE_OFFSET, False, '', EntryClassCode <> 'PPD');
      repeat
        if ctx_DataAccess.ClientID <> DS.CL_NBR.AsInteger then
        begin
          if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active and
            (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.ChangeCount > 0) then
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
          ctx_DataAccess.OpenClient(DS.CL_NBR.AsInteger);
        end;

        DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.CO_NBR.AsString);
        if DS.BATCH_ID.IsNull then
          s := 'None'
        else
          s := DS.BATCH_ID.AsString;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CheckDSConditionAndClient(DS['CL_NBR'], 'STATUS =  ''' + BANK_REGISTER_STATUS_Outstanding + ''' and StrCopy(FILLER, 17, 10) = ''' + s + '''');
        ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, DM_COMPANY.CO, DM_COMPANY.CL]);

        SetBatchUniqueIDToBAR(BatchUniqID);

        DS.Next;

        if (TranType = TRAN_TYPE_COMPANY) and
          ((ctx_DataAccess.ClientID <> DS.CL_NBR.AsInteger) or (CoNbr <> DS.CO_NBR.AsInteger)) or
           (dEffDate <> DS.EFFECTIVE_DATE.AsDateTime) or (DS.TRAN_TYPE.AsString = TRAN_TYPE_OFFSET) or
           (EntryClassCode <> DS.ENTRY_CLASS_CODE.AsString) or
           (DS.CUSTOM_EMPLOYEE_NUMBER.AsString <> '') or
           (TranType <> DS.TRAN_TYPE.AsString) or
//           (sTran <> DS.TRAN.AsString) or
          ((copy(Tran,2,1) <> '2') and (copy(Tran,2,1) <> '7')) or
           (AbaNumber <> DS.ABA_NUMBER.AsString) or
           (BankAccountNumber <> DS.BANK_ACCOUNT_NUMBER.AsString) or
           DS.Eof then
        begin

          if not DS.Eof then
            DS.Prior
          else
            DS.Last;

          if (Abs(fSum) > 0.001) or ((copy(Tran,2,1) <> '2') and (copy(Tran,2,1) <> '7')) then
          begin
            if DS.ACH_TYPE.AsString[1] = '6' then
              AppendDetailRecord('', fSum)
            else
              AppendAddendaRecord('', 0);

            ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;

            ACHFile.NAME.Value := GetRecordName(DS.TRAN_TYPE.AsString,
                                                DS.NAME.AsString,
                                                DM_TEMPORARY.TMP_CO.NAME.AsString,
                                                DS.CUSTOM_EMPLOYEE_NUMBER.AsString,
                                                DS.BATCH_COMMENTS.AsString);

            if TranType = TRAN_TYPE_COMPANY then
              ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := DM_TEMPORARY.TMP_CO.CUSTOM_COMPANY_NUMBER.Value
            else
              ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := DS.CUSTOM_EMPLOYEE_NUMBER.Value;

            ACHFile.ABA_NUMBER.Value          := AbaNumber;
            ACHFile.ABA_NUMBER2.Value         := Origin_Bank_Aba;
            ACHFile.BANK_ACCOUNT_NUMBER.Value := BankAccountNumber;
            ACHFile.COMMENTS.Value            := Comments;

            if (copy(Tran,2,1) = '2') or (copy(Tran,2,1) = '7') then
            begin
              if fSum < 0 then
                ACHFile.TRAN.Value := copy(Tran,1,1) + '7'
              else
                ACHFile.TRAN.Value := copy(Tran,1,1) + '2';
            end
            else
              ACHFile.TRAN.Value := Tran;

            if TranType = TRAN_TYPE_OFFSET then
            begin
              ACHFile.HIDE_FROM_REPORT.Value := 'Y';
              ACHFile.ACH_OFFSET.Value := DS.ACH_OFFSET.Value;
            end;

            if DS.ACH_TYPE.AsString[1] = '6' then
              WriteACHDetail(' ', StrToInt(Copy(DS.ACH_LINE.Value, 79, 1)), False, False, False, False, False)
            else
              ACHFile.WriteAddenda(Copy(DS.ACH_LINE.Value, 4, 80),
                AddendaNbr, EntryNbr);
          end;

          DS.Next;

          if (DS.ACH_TYPE.AsString[1] = '6') and
             ((TranType <> DS.TRAN_TYPE.AsString) or
              (DS.ENTRY_CLASS_CODE.AsString = 'CCD') and not DS.Eof or
              (DS.ENTRY_CLASS_CODE.AsString = 'PPD') and ((ctx_DataAccess.ClientID <> DS.CL_NBR.AsInteger) or (CoNbr <> DS.CO_NBR.AsInteger)) or
              (EntryClassCode <> DS.ENTRY_CLASS_CODE.AsString)) then
          begin
            WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
            // write batch header and footer

            if (DM_TEMPORARY.TMP_CL['CL_NBR'] <> DS['CL_NBR']) then
              DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
            if (DM_TEMPORARY.TMP_CO['CL_NBR'] <> DS['CL_NBR']) or
               (DM_TEMPORARY.TMP_CO['CO_NBR'] <> DS['CO_NBR']) then
              DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
            if (DM_TEMPORARY.TMP_PR['CL_NBR'] <> DS['CL_NBR']) or
               (DM_TEMPORARY.TMP_PR['PR_NBR'] <> DS['PR_NBR']) then
            begin
              DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
              PrNbr := DS.PR_NBR.AsInteger;
            end;

            Comments := GetBatchComments(DS.TRAN_TYPE.AsString,
                                         DS.NAME.AsString,
                                         DM_TEMPORARY.TMP_CO.NAME.AsString,
                                         DS.CUSTOM_EMPLOYEE_NUMBER.AsString,
                                         DS.BATCH_COMMENTS.AsString);

            StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], Comments, '', 0);
            WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount, 0, DS['TRAN_TYPE'] = TRAN_TYPE_OFFSET, False, '', DS['ENTRY_CLASS_CODE'] <> 'PPD');
          end;
          SaveRecordParamsAndInitSum;
        end
        else
          fSum := fSum + DS.AMOUNT.AsCurrency;

        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
      until DS.Eof;
      WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
      RemoveZeroBatches;      
      WriteEof(False);
    finally
      Combining := False;
      DS.Free;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevAchBase.CTSCombineTransactions;
var
  DS: TevClientDataSet;
  fSum: Currency;
  dEffDate: TDateTime;
  sEntryClassCode, sTran, sAbaNumber, sBankAccountNumber: string;
  sComments: string;
  CoNbr: Integer;
  sTranType: string;
  s: string;

  procedure RemoveZeroBatches;
  var
    sFields: String;
    sType: String;
  begin
    sFields := ACHFile.IndexFieldNames;
    ACHFile.IndexFieldNames := 'custom_company_number;cl_nbr;order_nbr';
    ACHFile. First;
    while not ACHFile.Eof do
    begin
      sType := LeftStr(ACHFile.FieldByName('ACH_TYPE').AsString, 1);
      if ((sType = '5'){ or (sType = '8')})
      and (ACHFile.FieldByName('AMOUNT').AsCurrency = 0)
      and (ACHFile.FieldByName('DEBITS').AsCurrency = 0)
      and (ACHFile.FieldByName('CREDITS').AsCurrency = 0) then
      begin
        ACHFile.Next;
        if not ACHFile.Eof then
        begin
          sType := LeftStr(ACHFile.FieldByName('ACH_TYPE').AsString, 1);
          if sType = '8' then
          begin
            ACHFile.Prior;
            ACHFile.Delete;
            ACHFile.Delete;
          end;
        end;
      end
      else
        ACHFile.Next;
    end;
    ACHFile.IndexFieldNames := sFields;
    ACHFile.First;
  end;

begin
  ctx_StartWait;
  try
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Close;
    DM_TEMPORARY.TMP_CL.DataRequired();
    DM_TEMPORARY.TMP_CO.DataRequired();
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    try
      DM_TEMPORARY.TMP_PR.DataRequired();
    finally
      DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    end;
    FileHeaderProcessed := False;
    ClearGrandAndBatchTotals;
    DS := TevClientDataSet.Create(nil);
    Combining := True;
    try
      DS.Data := ACHFile.Data;

      DS.First;
      while not DS.Eof do
      begin
        DS.Edit;
        if ((copy(DS.FieldByName('TRAN').AsString,2,1) <> '2')
        and (copy(DS.FieldByName('TRAN').AsString,2,1) <> '7')) then
          DS.FieldByName('COMB_AUX').AsString := '1'
        else
          DS.FieldByName('COMB_AUX').AsString := '0';
        DS.Post;  
        DS.Next;
      end;

      DS.First;

      DS.IndexFieldNames := 'custom_company_number;CL_NBR;CO_NBR;' + {CHECK_DATE;}
      'EFFECTIVE_DATE;ENTRY_CLASS_CODE;'+{TRAN;}
      'ABA_NUMBER;BANK_ACCOUNT_NUMBER;COMB_AUX;ORDER_NBR';

      DS.Filter := 'ACH_TYPE = ''6:*'' or ACH_TYPE = ''7:*''';
      DS.Filtered := True;

      DS.First;

      ACHFile.IndexFieldNames := '';
      ACHFile.EmptyDataSet;
      if DS.RecordCount = 0 then
        Exit;

      AddFileHeaderIfNotAddedYet;
      CoNbr := DS['CO_NBR'];
      dEffDate := DS['EFFECTIVE_DATE'];
      sEntryClassCode := DS['ENTRY_CLASS_CODE'];
      sTran := DS['TRAN'];
      sAbaNumber := DS['ABA_NUMBER'];
      sBankAccountNumber := DS['BANK_ACCOUNT_NUMBER'];
      fSum := DS['AMOUNT'];
      if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
        sComments := 'MANUAL'
      else if (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString = '') and (DS['BATCH_COMMENTS'] <> 'NET=PAY') and (DS['BATCH_COMMENTS'] <> 'PAYROLL') then
        sComments := 'IMPOUND'
      else
        sComments := DS['BATCH_COMMENTS'];

      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
      DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
      PrNbr := DS['PR_NBR'];
      DM_COMPANY.CL.Open;
      DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO.Open;
      StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, '', 0);
      WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount);
      sTranType := DS['TRAN_TYPE'];
      repeat
        if (ctx_DataAccess.ClientID <> DS['CL_NBR']) and
           DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active and
           (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.ChangeCount > 0) then
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
        ctx_DataAccess.OpenClient(DS['CL_NBR']);
        if DS.FieldByName('BATCH_ID').IsNull then
          s := 'None'
        else
          s := DS.FieldByName('BATCH_ID').AsString;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CheckDSConditionAndClient(DS['CL_NBR'], 'STATUS =  ''' + BANK_REGISTER_STATUS_Outstanding + ''' and StrCopy(FILLER, 17, 10) = ''' + s + '''');
        DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.FieldByName('CO_NBR').AsString);
        ctx_DataAccess.OpenDataSets([DM_COMPANY.CL, DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, DM_COMPANY.CO]);
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
        while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Eof do
        begin
          begin
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').AsString := PutIntoFiller(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('Filler').AsString, BatchUniqID, 18, 10);
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
            Break;
          end;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
        end;
        DS.Next;
        if (ctx_DataAccess.ClientID <> DS['CL_NBR']) or
           (CoNbr <> DS['CO_NBR']) or
           (dEffDate <> DS['EFFECTIVE_DATE']) or
           (DS.FieldByName('TRAN_TYPE').AsString = TRAN_TYPE_OFFSET) or
           (sEntryClassCode <> DS['ENTRY_CLASS_CODE']) or
           (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString <> '') or
//           (sTran <> DS['TRAN']) or
           ((copy(sTran,2,1) <> '2') and (copy(sTran,2,1) <> '7')) or
           (sAbaNumber <> DS['ABA_NUMBER']) or
           (sBankAccountNumber <> DS['BANK_ACCOUNT_NUMBER']) or
           DS.Eof then
        begin

          if not DS.Eof then
            DS.Prior
          else
            DS.Last;

          if (DM_TEMPORARY.TMP_CL['CL_NBR'] <> DS['CL_NBR']) then
            DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
          if (DM_TEMPORARY.TMP_CO['CL_NBR'] <> DS['CL_NBR']) or
             (DM_TEMPORARY.TMP_CO['CO_NBR'] <> DS['CO_NBR']) then
            DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
          if (DM_TEMPORARY.TMP_PR['CL_NBR'] <> DS['CL_NBR']) or
             (DM_TEMPORARY.TMP_PR['PR_NBR'] <> DS['PR_NBR']) then
          begin
            DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
            PrNbr := DS['PR_NBR'];
          end;

          if (Abs(fSum) > 0.001) or ((copy(sTran,2,1) <> '2') and (copy(sTran,2,1) <> '7')) then
          begin
            if DS.FieldByName('ACH_TYPE').AsString[1] = '6' then
              AppendDetailRecord('', fSum)
            else
              AppendAddendaRecord('', 0);
            ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;
            if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
              ACHFile.NAME.Value := 'MANUAL'
            else if (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString = '') and (DS['BATCH_COMMENTS'] <> 'NET=PAY') and (DS['BATCH_COMMENTS'] <> 'PAYROLL') then
              ACHFile.NAME.Value := 'IMPOUND'
            else
              ACHFile.NAME.Value := DS.FieldByName('NAME').Value;
            ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value;
            ACHFile.ABA_NUMBER.Value := sAbaNumber;
            ACHFile.ABA_NUMBER2.Value := Origin_Bank_Aba;
            ACHFile.BANK_ACCOUNT_NUMBER.Value := sBankAccountNumber;
            ACHFile.COMMENTS.Value := sComments;

            if (copy(sTran,2,1) = '2') or (copy(sTran,2,1) = '7') then
            begin
              if fSum < 0 then
                ACHFile.TRAN.Value := copy(sTran,1,1) + '7'
              else
                ACHFile.TRAN.Value := copy(sTran,1,1) + '2';
            end
            else
              ACHFile.TRAN.Value := sTran;

            if DS['TRAN_TYPE'] = TRAN_TYPE_OFFSET then
            begin
              ACHFile.HIDE_FROM_REPORT.Value := 'Y';
              ACHFile.ACH_OFFSET.Value := DS['ACH_OFFSET'];
            end;
            if DS.FieldByName('ACH_TYPE').AsString[1] = '6' then
              WriteACHDetail(' ', StrToInt(Copy(DS.FieldByName('ACH_LINE').Value, 79, 1)), False, False, False, False, False)
            else
              ACHFile.WriteAddenda(Copy(DS.FieldByName('ACH_LINE').Value, 4, 80),
                AddendaNbr, EntryNbr);

            sTranType := DS['TRAN_TYPE'];
          end;

          DS.Next;

          if (DS.FieldByName('ACH_TYPE').AsString[1] = '6') and
             ((ctx_DataAccess.ClientID <> DS['CL_NBR']) or
              (CoNbr <> DS['CO_NBR']) or
              (DS['ENTRY_CLASS_CODE'] = 'CCD') and not DS.Eof or
              (sEntryClassCode <> DS['ENTRY_CLASS_CODE'])) then
          begin

            WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
            // write batch header and footer

            if (DM_TEMPORARY.TMP_CL['CL_NBR'] <> DS['CL_NBR']) then
              DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
            if (DM_TEMPORARY.TMP_CO['CL_NBR'] <> DS['CL_NBR']) or
               (DM_TEMPORARY.TMP_CO['CO_NBR'] <> DS['CO_NBR']) then
              DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
            if (DM_TEMPORARY.TMP_PR['CL_NBR'] <> DS['CL_NBR']) or
               (DM_TEMPORARY.TMP_PR['PR_NBR'] <> DS['PR_NBR']) then
            begin
              DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
              PrNbr := DS['PR_NBR'];
            end;
            if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
              sComments := 'MANUAL'
            else if (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString = '') and (DS['BATCH_COMMENTS'] <> 'NET=PAY') and (DS['BATCH_COMMENTS'] <> 'PAYROLL') then
              sComments := 'IMPOUND'
            else
              sComments := DS['BATCH_COMMENTS'];
            StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, '', 0);
            WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount);
          end;

          CoNbr := DS['CO_NBR'];
          dEffDate := DS['EFFECTIVE_DATE'];
          sEntryClassCode := DS['ENTRY_CLASS_CODE'];
          sTran := DS['TRAN'];
          sAbaNumber := DS['ABA_NUMBER'];
          sBankAccountNumber := DS['BANK_ACCOUNT_NUMBER'];
          fSum := DS['AMOUNT'];
        end
        else
          fSum := fSum + DS['AMOUNT'];

        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
      until DS.Eof;
      WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
      RemoveZeroBatches;
      WriteEof(False);
    finally
      Combining := False;
      DS.Free;
    end;
  finally
    ctx_EndWait;
  end;
end;

destructor TevAchBase.Destroy;
begin
  ACHFile.Free;
  Options.Free;
  inherited;
end;

procedure TevAchBase.DoOnConstruction;
var
  TempString: String;
begin
  inherited;
  Randomize;
  FBtFileName := '';
  ACHFolder := '';
  FACHFile := TevAchDataSet.Create(nil);
  IncludeOffsets := OFF_DONT_USE;
  bPrenote := False;
  strFillerStamp := '';
  Combine := COMBINE_DO_NOT;
  ReversalACH := False;
  v := VarArrayCreate([0, 12], varVariant);
  FACHFiles := TisListOfValues.Create;
  FFileSaved := False;
  Combining := False;
  SavingACH := False;
  TempString := FormatDateTime('YYYYMMDDHHNNSS', Now);
  FFileName := TempString + '.ach';
  strFillerStamp := TempString + ':' + Context.UserAccount.User + ':' + ExtractFileName(FileName);
  Options := TACHOptions.Create;
end;

procedure GenerateAchReport(aACHFile, ShowAll, FileSaved: Boolean;
  const ACHFileName, ACHFolder: String; const AchDataSet: TevAchDataSet;
  const ACHDescription: String);
var
  FileName: String;
  ReportParams: TrwReportParams;
  P: TisProvider;
  cds: TevClientDataSet;
begin
  if FileSaved then
    FileName := 'FILE NAME: ' + ACHFolder + '\' + ACHFileName
  else
    FileName := 'FILE NAME: (not yet saved)';
  ReportParams := TrwReportParams.Create;
  p := TisProvider.Create(nil);
  cds := TevClientDataSet.Create(nil);
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('SY_REPORT_WRITER_REPORTS_NBR='+IntToStr(ACH_REPORT_SY_NBR));
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.First;
    if DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Eof then
      raise EMissingReport.CreateHelp('"ACH Report" report does not exist in the database!', IDH_MissingReport);
    p.Name := 'TempProvider';
    p.DataSet := AchDataSet;
    p.DataSet.First;
    cds.Data := p.Data;

    ReportParams.Add('DataSets', DataSetsToVarArray([cds]));
    ReportParams.Add('ShowEverything', IntToStr(Integer(ShowAll)));
    ReportParams.Add('PrintAchDetail', IntToStr(Integer(aACHFile)));
    ReportParams.Add('ProcDates', ACHDescription);
    ReportParams.Add('FileName', FileName);

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(ACH_REPORT_SY_NBR, CH_DATABASE_SYSTEM, ReportParams);
    finally
      ctx_RWLocalEngine.EndGroup(rdtPreview);
    end;

  finally
    p.Free;
    cds.Free;
    ReportParams.Free;
  end;
end;

function TevAchBase.GetSavedAchFileStrings(
  const AFileName: String): IisStringList;
begin
  Result := nil;
  Result := IInterface(FACHFiles.TryGetValue(AFileName, IInterface(Result))) as IisStringList;
end;

procedure TevAchBase.IncrementSubTotals(bIsAch: Boolean);
begin
  if (not bIsAch) then
  begin
    Inc(FSubNbrOfBatches);
    // increment grand totals, too
    FSubEntryHash := FSubEntryHash + FEntryHash;
    FSubCredits := FSubCredits + FBatchCredits;
    FSubDebits := FSubDebits - FBatchDebits;
  end;
end;

procedure TevAchBase.RecreateForColumbiaEDPACH;
var
  DS: TevClientDataSet;
  dEffDate: TDateTime;
  dCheckDate: Variant;
  sEntryClassCode, sAbaNumber, sBankAccountNumber: string;
  sComments: string;
  CoNbr: Integer;
  sTranType: string;
begin
  ctx_StartWait;
  try
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Close;
    DM_TEMPORARY.TMP_CL.DataRequired();
    DM_TEMPORARY.TMP_CO.DataRequired();
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    try
      DM_TEMPORARY.TMP_PR.DataRequired();
    finally
      DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    end;
    FileHeaderProcessed := False;
    ClearGrandAndBatchTotals;
    DS := TevClientDataSet.Create(nil);
    Combining := True;
    try
      DS.IndexFieldNames := 'custom_company_number;CL_NBR;CO_NBR;CHECK_DATE;EFFECTIVE_DATE;ENTRY_CLASS_CODE;ABA_NUMBER;BANK_ACCOUNT_NUMBER;ORDER_NBR';
      DS.Data := ACHFile.Data;
      DS.Filter := 'ACH_TYPE = ''6:*'' or ACH_TYPE = ''7:*''';
      DS.Filtered := True;

      ACHFile.IndexFieldNames := '';
      ACHFile.EmptyDataSet;
      AddFileHeaderIfNotAddedYet;

      CoNbr := DS['CO_NBR'];
      dCheckDate := DS['CHECK_DATE'];
      dEffDate := DS['EFFECTIVE_DATE'];
      sEntryClassCode := DS['ENTRY_CLASS_CODE'];
      sAbaNumber := DS['ABA_NUMBER'];
      sBankAccountNumber := DS['BANK_ACCOUNT_NUMBER'];
      if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
        sComments := 'MANUAL'
      else if (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString = '') and (DS['BATCH_COMMENTS'] <> 'NET=PAY') and (DS['BATCH_COMMENTS'] <> 'PAYROLL') then
        sComments := 'IMPOUND'
      else
        sComments := DS['BATCH_COMMENTS'];

      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
      DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
      PrNbr := DS['PR_NBR'];
      DM_COMPANY.CL.Open;
      DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO.Open;
      if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
        StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, BANK_REGISTER_TYPE_Manual, 0)
      else
        StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, '', 0);
      WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount);
      sTranType := DS['TRAN_TYPE'];
      repeat
        ctx_DataAccess.OpenClient(DS['CL_NBR']);
        DM_COMPANY.CO.CheckDSConditionAndClient(DS['CL_NBR'], 'CO_NBR = ' + DS.FieldByName('CO_NBR').AsString);
        ctx_DataAccess.OpenDataSets([DM_COMPANY.CL, DM_COMPANY.CO]);

        if (DM_TEMPORARY.TMP_CL['CL_NBR'] <> DS['CL_NBR']) then
          DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
        if (DM_TEMPORARY.TMP_CO['CL_NBR'] <> DS['CL_NBR']) or
           (DM_TEMPORARY.TMP_CO['CO_NBR'] <> DS['CO_NBR']) then
          DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
        if (DM_TEMPORARY.TMP_PR['CL_NBR'] <> DS['CL_NBR']) or
           (DM_TEMPORARY.TMP_PR['PR_NBR'] <> DS['PR_NBR']) then
        begin
          DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
          PrNbr := DS['PR_NBR'];
        end;

        if DS.FieldByName('ACH_TYPE').AsString[1] = '6' then
          AppendDetailRecord('', DS['AMOUNT'])
        else
          AppendAddendaRecord('', 0);
        ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;
        ACHFile.NAME.Value := DS.FieldByName('NAME').Value;
        ACHFile.CUSTOM_EMPLOYEE_NUMBER.Value := DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value;
        ACHFile.ABA_NUMBER.Value := sAbaNumber;
        ACHFile.ABA_NUMBER2.Value := Origin_Bank_Aba;
        ACHFile.BANK_ACCOUNT_NUMBER.Value := sBankAccountNumber;
        ACHFile.COMMENTS.Value := DS['COMMENTS'];
        ACHFile.TRAN.Value := DS['TRAN'];
        if DS.FieldByName('ACH_TYPE').AsString[1] = '6' then
          WriteACHDetail(' ', StrToInt(Copy(DS.FieldByName('ACH_LINE').Value, 79, 1)), False, False, False, False, False)
        else
          ACHFile.WriteAddenda(Copy(DS.FieldByName('ACH_LINE').Value, 4, 80),
                               AddendaNbr, EntryNbr);
        sTranType := DS['TRAN_TYPE'];
        DS.Next;

        if (DS.FieldByName('ACH_TYPE').AsString[1] = '6') and
           ((ctx_DataAccess.ClientID <> DS['CL_NBR']) or
            (CoNbr <> DS['CO_NBR']) or
            (dCheckDate <> DS['CHECK_DATE']) or
            (dEffDate <> DS['EFFECTIVE_DATE']) or
            ((sEntryClassCode <> DS['ENTRY_CLASS_CODE']) and (sComments <> 'MANUAL'))) then
        begin
          WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
          // write batch header and footer

          if (DM_TEMPORARY.TMP_CL['CL_NBR'] <> DS['CL_NBR']) then
            DM_TEMPORARY.TMP_CL.Locate('CL_NBR', DS['CL_NBR'], []);
          if (DM_TEMPORARY.TMP_CO['CL_NBR'] <> DS['CL_NBR']) or
             (DM_TEMPORARY.TMP_CO['CO_NBR'] <> DS['CO_NBR']) then
            DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DS['CL_NBR;CO_NBR'], []);
          if (DM_TEMPORARY.TMP_PR['CL_NBR'] <> DS['CL_NBR']) or
             (DM_TEMPORARY.TMP_PR['PR_NBR'] <> DS['PR_NBR']) then
          begin
            DM_TEMPORARY.TMP_PR.Locate('CL_NBR;PR_NBR', DS['CL_NBR;PR_NBR'], []);
            PrNbr := DS['PR_NBR'];
          end;
          if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
            sComments := 'MANUAL'
          else if (DS.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString = '') and (DS['BATCH_COMMENTS'] <> 'NET=PAY') and (DS['BATCH_COMMENTS'] <> 'PAYROLL') then
            sComments := 'IMPOUND'
          else
            sComments := DS['BATCH_COMMENTS'];
          if DS.FieldByName('NAME').AsString = 'MANUAL TRANSFER' then
            StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, BANK_REGISTER_TYPE_Manual, 0)
          else
            StartBatchDefinition(DS['ENTRY_CLASS_CODE'], DS['EFFECTIVE_DATE'], sComments, '', 0);
          WriteBatchHeader(False, SegmentNbr, ACHFile.RecordCount);
        end;

        CoNbr := DS['CO_NBR'];
        dCheckDate := DS['CHECK_DATE'];
        dEffDate := DS['EFFECTIVE_DATE'];
        sEntryClassCode := DS['ENTRY_CLASS_CODE'];
        sAbaNumber := DS['ABA_NUMBER'];
        sBankAccountNumber := DS['BANK_ACCOUNT_NUMBER'];
      until DS.Eof;
      WriteBatchTotal(False, SegmentNbr, ACHFile.RecordCount, ''{, Integer(sTranType = TRAN_TYPE_OFFSET)});
      WriteEof(False);
    finally
      Combining := False;
      DS.Free;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevAchBase.RestoreContext;
begin
  FEntryHash := vi[0];
  FTotEntryHash := vi[1];
  FSubEntryHash := vi[2];

  EntryNbr := v[0];
  FTotNbrOfEntries := v[1];
  FBatchCredits := v[2];
  FBatchDebits := v[3];
  // only clear items related to grand totals
  FxTotNbrOfEntries := v[4];
  FTotNbrOfBatches := v[5];
  FTotCredits := v[6];
  FTotDebits := v[7];
  // clear Sub-totals, too
  FSubNbrOfBatches := v[8];
  FSubNbrOfEntries := v[9];
  FSubCredits := v[10];
  FSubDebits := v[11];
  ACHFile.Data := v[12];
  ACHFile.IndexFieldNames := 'custom_company_number;cl_nbr;order_nbr';
end;

procedure TevAchBase.SaveAch;
begin

  DM_SERVICE_BUREAU.SB.Activate;
  ACHFile.EmptyDataSet;

  SavingACH := True;
  try
    MainProcess;
  finally
    SavingACH := False;
  end;

  if Combine <> COMBINE_DO_NOT then
  begin
    FBtFileName := ExtractFilePath(FileName) + 'BelowThreshold ' + FormatDateTime('d-m-yyyy hh-nn-ss', Now) + '.txt';
    if FileExists(FBtFileName) then
      DeleteFile(FBtFileName);

    if Options.CTS or (Combine = COMBINE_CO_NO_DD) or (Combine = COMBINE_CO_W_DD) or (Combine = COMBINE_CO_WP_DD) then
    begin
      CTSCombineTransactions;
      if FileExists(FBtFileName) then
        CTSCombineTransactions;
    end
    else
    begin
      CombineTransactions;
      if FileExists(FBtFileName) then
        CombineTransactions;
    end;
  end;

  if Options.BalanceBatches and not bPrenote then
    RecreateForColumbiaEDPACH;

  SaveFileSaveACH;

  FFileSaved := True;

end;

procedure TevAchBase.SaveFileSaveACH;
var
  datelist: TStringList;
  i: Integer;
  cds: TevClientDataSet;
  sType: string;
begin
  if ACHFile.RecordCount = 0 then
    Exit;
  if not Options.EffectiveDate then
    SaveFileByName(FileName)
  else
  begin
    cds := TevClientDataSet.Create(nil);
    datelist := TStringList.Create;
    try
      datelist.Sorted := True;
      datelist.Duplicates := dupIgnore;
      ACHFile.First;
      while not ACHFile.Eof do
      begin
        if LeftStr(ACHFile.ACH_TYPE.AsString, 1) = '5' then
          datelist.Add(ACHFile.FieldByName('EFFECTIVE_DATE').AsString);
        ACHFile.Next;
      end;

      cds.Data := ACHFile.Data;
      try
        for i := 0 to datelist.Count - 1 do
        begin
          ACHFile.Filtered := True;
          ACHFile.Filter := 'ACH_TYPE = ''9:*'' or ACH_TYPE = ''99:*''';
          while ACHFile.RecordCount > 0 do
            ACHFile.Delete;

          ACHFile.Filter := 'ACH_TYPE = ''9:*'' or ACH_TYPE = ''99:*'' or ACH_TYPE = ''1:*'' or EFFECTIVE_DATE=''' + datelist[i] + '''';

          ClearGrandAndBatchTotals;
          while not ACHFile.Eof do
          begin
            sType := LeftStr(ACHFile.ACH_TYPE.AsString, 1);
            if sType = '5' then
              Inc(FTotNbrOfBatches);
            if (sType = '5') or
               (sType = '8') then
            begin
              ACHFile.Edit;
              ACHFile.ACH_LINE.AsString := StuffString(ACHFile.ACH_LINE.AsString, 88, 7, ZeroFillInt64ToStr(FTotNbrOfBatches, 7));
              ACHFile.Post;
            end;
            if (sType = '6') or
               (sType = '7') then
              Inc(FxTotNbrOfEntries);
            if sType = '6' then
              IncHashTotal(ACHFile.ABA_NUMBER.AsString, FTotEntryHash);
            if sType = '8' then
            begin
              FTotCredits := FTotCredits + ACHFile.CREDITS.AsCurrency;
              FTotDebits := FTotDebits - ACHFile.DEBITS.AsCurrency;
            end;
            ACHFile.Next;
          end;
          WriteEOF(False);
          SaveFileByName(ChangeFileExt(FileName, '.' + FormatDateTime('yyyymmdd', StrToDate(datelist[i])) + ExtractFileExt(FileName)));
        end;
        if datelist.Count = 1 then
          FFileName := FACHFiles[0].Name;
      finally
        ACHFile.Data := cds.Data;
      end;
    finally
      datelist.Free;
      cds.Free;
    end;
  end;
end;

procedure TevAchBase.SetACHOptions(NewACHOptions: TACHOptions);
begin
  Options.Assign(NewACHOptions);
end;

procedure TevAchBase.SetACHOptions(const NewACHOptions: IevACHOptions);
begin
  Options.Assign2(NewACHOptions);
end;

procedure TevAchBase.StartBatchDefinition(aClassCode: string;
  aEffDate: TDateTime; aComments, BankRegisterType: string;
  SB_BANK_ACCOUNTS_NBR: Integer);
begin
  ACHFile.BatchEffectiveDate := aEffDate;
  ACHFile.BatchClassCode := aClassCode;
  ACHFile.BatchComments := aComments;
  BatchBANK_REGISTER_TYPE := BankRegisterType;
  BatchSB_BANK_ACCOUNTS_NBR := SB_BANK_ACCOUNTS_NBR;
  BatchUniqID := Format('%10.10d', [Random(9999999999)]);

  ClearBatchTotals;
end;

procedure TevAchBase.WriteACHDetail(cTranType: Char;
  NbrOfAddendaRecords: Integer; aCTSUI: Boolean; aNVSUI: Boolean;
  aMASUI: Boolean; aAZSUI, aNMSUI: Boolean);
var
  s: string;
  Nbr: Integer;
  RecordTypeCode: String;
  IdentificationNumber, ReceiverName, DiscretionaryData,
  AddendaIndicator, TraceNumber: String;

  function RemovePunctuation(const Value: String): String;
  var
    i: Integer;
  begin
    Result := Value;
    i := 1;
    while i <= Length(Result) do
    begin
      if Result[i] in [' ',',','''','"','-','.'] then
        System.Delete(Result, i, 1)
      else
        Inc(i);  
    end;
  end;

begin

  // if no transactions was added when processing payroll ACH, it must be marked as "Deleted"
  FDetailAdded := True;

  // ONLY write ACH_LINE... because EACH segment handles detail records differently
  // by either using ctx_DataAccess.CUSTOM_VIEW (or not).
  
  RecordTypeCode := '6';
  if not CheckOffset(ACHFile.ACH_OFFSET.AsInteger) then
  begin
    if ACHFile.State in [dsInsert, dsEdit] then
      ACHFile.Cancel;
    Exit;
  end;
  if NbrOfAddendaRecords > 0 then
    Nbr := NbrOfAddendaRecords
  else
    Nbr := 0;

  // ALL OTHER fields were previously stamped before calling this procedure!
  // Finish Calculating Transaction Code based on AMOUNT

  ACHFile.TRAN_TYPE.AsString := cTranType;
  if Length(ACHFile.TRAN.AsString) < 2 then
    ACHFile.TRAN.AsString := ACHFile.TRAN.AsString +
      GetAchTransactionType(ACHFile.IN_PRENOTE.AsString,
      ACHFile.AMOUNT.AsCurrency, NbrOfAddendaRecords);

  if FPrenote and (Length(ACHFile.TRAN.AsString) = 2) then
  begin
    if NbrOfAddendaRecords <> -1 then
      ACHFile.TRAN.AsString := copy(ACHFile.TRAN.AsString,1,1) + '3'
    else
      ACHFile.TRAN.AsString := copy(ACHFile.TRAN.AsString,1,1) + '8';
  end;

  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.BATCH_COMMENTS.AsString := ACHFile.BatchComments;

  s := RecordTypeCode + Copy(ACHFile.TRAN.Value
    + '  ', 1, 2) + Copy(ACHFile.ABA_NUMBER.Value + '         ', 1, 9)
    + Copy(ACHFile.BANK_ACCOUNT_NUMBER.Value + '                 ', 1, 17)
    + ZeroFillInt64ToStr(RoundInt64(Abs(ACHFile.AMOUNT.Value) * 100), 10);

  IdentificationNumber := '';

  if Options.CTS then
  if ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString <> '' then
  begin
    DM_CLIENT.CL_PERSON.Activate;
    DM_EMPLOYEE.EE.Activate;
    IdentificationNumber := StringReplace(DM_EMPLOYEE.EE.Lookup('CO_NBR;CUSTOM_EMPLOYEE_NUMBER',
      VarArrayOf([DM_COMPANY.CO['CO_NBR'], ACHFile.FieldValues['CUSTOM_EMPLOYEE_NUMBER']]), 'SOCIAL_SECURITY_NUMBER'), '-', '', [rfReplaceAll]) + StringOfChar(' ', 6);
    s := s + IdentificationNumber;
  end
  else if ACHFile.TRAN.Value <> '22' then
  begin
    IdentificationNumber := PadRight(Trim(ACHFile.CUSTOM_COMPANY_NUMBER.AsString), ' ', 13) + '01';
    s := s + IdentificationNumber;
  end
  else if Options.SunTrust then
  begin
    IdentificationNumber := PadRight(Trim(SBID), ' ', 13) + '01';
    s := s + IdentificationNumber;
  end
  else
  begin
    IdentificationNumber := '    ' + Copy(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString) + '           ', 1, 11);
    s := s + IdentificationNumber;
  end
  else
  begin
    if aCTSUI then
      IdentificationNumber := PadStringRight(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15)
    else
    if aMASUI or aNMSUI then
      IdentificationNumber := PadStringRight(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15)
    else
    if aNVSUI then
      IdentificationNumber := PadStringLeft(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), '0', 15)
    else  
    if aAZSUI then
      IdentificationNumber := PadStringRight(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), '0', 15)
    else
      IdentificationNumber := PadStringLeft(Trim(ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString), ' ', 15);
    s := s + IdentificationNumber;
  end;


  if aAZSUI then
    ReceiverName := Copy(RemovePunctuation(DM_COMPANY.CO.NAME.AsString) + '                      ', 1, 22)
  else
  if aMASUI then
    ReceiverName := Copy(DM_SERVICE_BUREAU.SB.SB_NAME.AsString+ '                      ', 1, 22)
  else
  if not Options.DisplayCoNbrAndName then
    ReceiverName := Copy(ACHFile.NAME.AsString + '                      ', 1, 22)
  else
    ReceiverName := Copy(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString + ' ' +
      RemovePunctuation(DM_COMPANY.CO.NAME.AsString) + '                      ', 1, 22);

  if aNVSUI then
    DiscretionaryData := 'NV'
  else
    DiscretionaryData := '  ';

  AddendaIndicator := IntToStr(Nbr);
  TraceNumber := Copy(ORIGIN_BANK_ABA + '         ', 1, 8) + ZeroFillInt64ToStr(ACHFile.BATCH_NBR.AsInteger, 7);
  s := s + ReceiverName + DiscretionaryData + AddendaIndicator + TraceNumber;
  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  ACHFile.Post;

  IncHashTotal(ACHFile.ABA_NUMBER.AsString, FEntryHash);

  // for Sub-Totals
  if ACHFile.ACH_OFFSET.AsInteger = OFF_DONT_USE then
    Inc(FSubNbrOfEntries);
end;

procedure TevAchBase.WriteBatchHeader(aMoveLast: Boolean; aSegment: Integer; aOrder: Integer;
      aIsOffset: Integer = OFF_DONT_USE; aHidden: Boolean = False; aCRLF: Boolean = False;
      aName: String = ''; SBMode: Boolean = False; Prenote: Boolean = False);
var
  s: string;
  cn: string;
  CompanyName, CompanyData,
  CompanyIdentification,
  EntryClassCode,
  CompanyEntryDescription,
  OriginatorStatusCode,
  OriginatingDFI: String;
begin
  if not CheckOffset(aIsOffset) then
    Exit;

  if aMoveLast then
    ACHFile.Last; // position to eof

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_HEADER_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := '';
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N'; // Initialize to 'N' other code will overide this value

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
    begin
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString;
    end
    else
    begin
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
    end;
  end
  else
  begin
    if not Combining then
      ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FEIN.AsString
    else
      ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
  end;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.CHECK_DATE.Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.RUN_NUMBER.Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);

  ACHFile.SEGMENT_NBR.AsInteger := aSegment; // specific info
  ACHFile.ORDER_NBR.AsInteger := aOrder; // record count BEFORE 6's were added
  DM_CLIENT.CL.Activate;

  if aName <> '' then
    ACHFile.NAME.AsString := aName
  else if not Combining then
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_COMPANY.CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_COMPANY.CO.NAME.AsString // for 5-record
  else if SBMode then
    ACHFile.NAME.AsString := Origin_Name
  else
    if IsPayrollPeople then
      ACHFile.NAME.AsString := DM_COMPANY.CO.LEGAL_NAME.AsString
    else if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CL.NAME.AsString // for 5-record
    else
      ACHFile.NAME.AsString := DM_TEMPORARY.TMP_CO.NAME.AsString;
  ACHFile.ENTRY_CLASS_CODE.AsString := ACHFile.BatchClassCode; // CCD or PPD
  ACHFile.EFFECTIVE_DATE.AsDateTime := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.AsString := ACHFile.BatchComments; // NET=PAY etc...
  // ACH Report Custom Printing (partial vs. full)
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.AsString := 'Y'; // is Suppressed ?
  if aCRLF then
    ACHFile.CR_AFTER_LINE.AsString := 'Y'; // CR/LF after printing of detail line?
  
  if SBMode then
    cn := ''
  else
    cn := ACHFile.CUSTOM_COMPANY_NUMBER.AsString;

  CompanyName := '';
  CompanyData := '';

  if Prenote and not Options.CTS then
  begin
    CompanyName := PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
    CompanyData := copy(CompanyName,17,20);
    CompanyName := copy(CompanyName,1,16);

    s := '5200' + PadRight(Copy(cn+ '     ', 1, 6) + ACHFile.NAME.AsString, ' ', 36);
  end
  else if IsPayrollPeople and not Prenote then
  begin
    CompanyName := PadRight(ACHFile.NAME.AsString, ' ', 16);
    CompanyData := Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '     ', 1, 6) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end        
  else if Options.SunTrust and not Prenote then
  begin
    CompanyName := ACHFile.NAME.AsString;
    CompanyData := Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID + '                ', 1, 20);

    s := '5200' + PadRight(ACHFile.NAME.AsString, ' ', 16) +
          Copy(Copy(cn+ '        ', 1, 9) + '*' +
          BatchUniqID +
          '                ', 1, 20);
  end        
  else
  begin
    if Options.BankOne then
    begin
      CompanyName := Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '        ', 1, 8) + ' ' +
        ACHFile.NAME.AsString + '                ', 1, 16);
    end
    else
    begin
      CompanyName := Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
      s := '5200' +
        Copy(Copy(cn+ '     ', 1, 5) + '*' +
        BatchUniqID + '                ', 1, 16);
    end;
    if Options.CTS or Options.Intercept then
    begin
      CompanyData := PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
      s := s + PadRight(Copy(ACHFile.NAME.AsString, 1, 8), ' ', 8) + PadRight(Copy(Trim(cn), 1, 6), ' ', 6) + PadRight(SBID, ' ', 6);
    end
    else
    begin
      CompanyData := Copy(PadRight(Trim(cn) + ' '+ ACHFile.NAME.AsString, ' ', 20), 1, 20);
      s := s + Copy(PadRight(Trim(cn) + ' '+ ACHFile.NAME.AsString, ' ', 20), 1, 20);
    end;
  end;

  s := s + Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + Copy(ACHFile.ENTRY_CLASS_CODE.AsString
    + '   ', 1, 3) + Copy(DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString + '          ', 1, 10)
    + FormatDateTime('YYMMDD', ACHFile.CHECK_DATE.AsDateTime) +
    FormatDateTime('YYMMDD', ACHFile.EFFECTIVE_DATE.AsDateTime) + '   1'
    + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString;

  s := UpperCase(s);

  ACHFile.ACH_LINE.AsString := s;

  CompanyIdentification := ACHFile.FEIN.AsString;
  EntryClassCode := ACHFile.ENTRY_CLASS_CODE.AsString;
  CompanyEntryDescription := DM_COMPANY.CO.WELLS_FARGO_ACH_FLAG.Value + ACHFile.COMMENTS.AsString;
  OriginatorStatusCode := '1';
  OriginatingDFI := ORIGIN_BANK_ABA;

  ACHFile.Post;
end;

procedure TevAchBase.WriteBatchTotal(
  aHoldTotals: Boolean;
  aSegment,
  aOrder: Integer;
  CUSTOM_EMPLOYEE_NUMBER: string;
  aIsOffset: Integer;
  aHidden,
  aCRLF: Boolean;
  aFEIN: String);
begin
  {
    All Batch Totals are assumed as "hidden" from the ach report in order
    to make the report appear like the current version from Rapid Pay.
  }
  if not CheckOffset(aIsOffset) then
    Exit;

  // Common Info for batch totals

  ACHFile.Append;

  ACHFile.ACH_TYPE.Value                  := ACH_BATCH_TOTAL_STRING;
  ACHFile.SB_BANK_ACCOUNTS_NBR.AsInteger  := BatchSB_BANK_ACCOUNTS_NBR;
  ACHFile.BANK_REGISTER_TYPE.AsString     := BatchBANK_REGISTER_TYPE;
  ACHFile.SEGMENT_NBR.AsInteger           := SegmentNbr;
  ACHFile.PR_NBR.Value                    := PrNbr;
  ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := CUSTOM_EMPLOYEE_NUMBER;
  ACHFile.AMOUNT.Value                    := 0;
  ACHFile.IN_PRENOTE.AsString             := 'N';

  if not Combining then
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := ctx_DataAccess.ClientID;
  end
  else
  begin
    ACHFile.CUSTOM_COMPANY_NUMBER.Value := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    ACHFile.CO_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').Value;
    ACHFile.CL_NBR.Value := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').Value;
  end;
  // Don't know why, but FEIN is prefixed with a "1" according to ACH Rules!!!
  Assert(DM_SERVICE_BUREAU.SB.RecordCount > 0);

  if Length(Options.CoId) = 0 then
    Options.CoId := '1'
  else if Options.CoId = '@' then
    Options.CoId := ' ';

  if Options.UseSBEIN then
  begin
    if Options.SBEINOverride = '' then
      ACHFile.FEIN.Value := Options.CoId + DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString
    else
      ACHFile.FEIN.Value := Options.CoId + Options.SBEINOverride;
  end
  else if not Combining then
    ACHFile.FEIN.Value := Options.CoId + DM_COMPANY.CO.FieldByName('FEIN').AsString
  else
    ACHFile.FEIN.Value := Options.CoId + DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;

  if (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_EFTPayment) or
     (BatchBANK_REGISTER_TYPE = BANK_REGISTER_TYPE_Manual) then
    ACHFile.CHECK_DATE.Value := ACHFile.BatchEffectiveDate
  else if not Combining then
    ACHFile.CHECK_DATE.Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value
  else
    ACHFile.CHECK_DATE.Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
  if not Combining then
    ACHFile.RUN_NUMBER.Value := DM_PAYROLL.PR.FieldByName('RUN_NUMBER').Value;

  ACHFile.AMOUNT.Value := (FBatchCredits - FBatchDebits);
  if FBatchCredits <> 0 then
    ACHFile.CREDITS.Value := FBatchCredits;
  if FBatchDebits <> 0 then
    ACHFile.DEBITS.Value := FBatchDebits;
  ACHFile.BATCH_NBR.AsString := ZeroFillInt64ToStr(FTotNbrOfBatches + 1, 7);
  // reset after batch control total
  Inc(FTotNbrOfBatches);
  // increment grand totals, too
  FTotEntryHash := FTotEntryHash + FEntryHash;
  FTotCredits := FTotCredits + FBatchCredits;
  FTotDebits := FTotDebits - FBatchDebits;

  // Custom info for batch totals
  ACHFile.ACH_OFFSET.AsInteger := aIsOffset;
  if aHidden then
    ACHFile.HIDE_FROM_REPORT.Value := 'Y';
  if aCRLF then
    ACHFile.CR_AFTER_LINE.Value := 'Y';
  ACHFile.SEGMENT_NBR.Value := aSegment; // assigned...?
  ACHFile.ORDER_NBR.Value := aOrder;
  if not Combining then
    ACHFile.NAME.Value := DM_COMPANY.CO.FieldByName('NAME').AsString
  else
    ACHFile.NAME.Value := DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString;
  ACHFile.ENTRY_CLASS_CODE.Value := ACHFile.BatchClassCode;
  ACHFile.EFFECTIVE_DATE.Value := ACHFile.BatchEffectiveDate;
  ACHFile.COMMENTS.Value := ACHFile.BatchComments;
//    FieldByName('ABA_NUMBER').AsInteger := iEntryHash;

  // ACH File Layout
  ACHFile.ACH_LINE.AsString := UpperCase('8200' + ZeroFillInt64ToStr(FTotNbrOfEntries, 6)
    + ZeroFillInt64ToStr(FEntryHash, 10) + ZeroFillInt64ToStr(RoundInt64(Abs(FBatchDebits)
    * 100), 12) + ZeroFillInt64ToStr(RoundInt64(Abs(FBatchCredits) * 100), 12) +
    Copy(ACHFile.FEIN.AsString + '          ', 1, 10) + '                   '
    + '      ' + Copy(ORIGIN_BANK_ABA + '        ', 1, 8) + ACHFile.BATCH_NBR.AsString);


  ACHFile.Post;

  if aHoldTotals then
  begin
    // Hold credits/debits aside for other 3 offset records
    TempCredits := FBatchCredits;
    TempDebits := FBatchDebits;
  end;

  IncrementSubTotals(aIsOffset <> OFF_DONT_USE);

  ClearBatchTotals;

end;

procedure TevAchBase.WriteEOF(const addSubTotal: Boolean);
var
  i: Integer;
begin

  Inc(SegmentNbr);

  if ACHFile.RecordCount > 0 then
  begin
    i := ACHFile.RecordCount + 1;
    if Trim(Options.Header) <> '' then
      Dec(i);

    if addSubTotal then
    begin

      // Sub-Total amounts (does not get written into the file)
      AppendFileTotalRecord;

      // Trick Report into knowing this is a sub-total record
      ACHFile.ACH_OFFSET.AsInteger := OFF_USE_OFFSETS;

      ACHFile.BATCH_NBR.Value := FSubNbrOfBatches;
      ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(FSubNbrOfEntries);
      // total could be too big to fit into ABA_NUMBER field,... so put into this one instead
      ACHFile.BANK_ACCOUNT_NUMBER.Value := ZeroFillInt64ToStr(FSubEntryHash, 10);
      ACHFile.DEBITS.Value := FSubDebits;
      ACHFile.CREDITS.Value := FSubCredits;
      ACHFile.SEGMENT_NBR.AsInteger := SegmentNbr;
      ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;

      ACHFile.ACH_LINE.Value := UpperCase('9' + ZeroFillInt64ToStr(FSubNbrOfBatches, 6)
        + ZeroFillInt64ToStr((i + 9) div 10, 6) + ZeroFillInt64ToStr(FSubNbrOfEntries, 8)
        + ZeroFillInt64ToStr(FSubEntryHash, 10) + ZeroFillInt64ToStr(RoundInt64(FSubDebits * 100), 12)
        + ZeroFillInt64ToStr(RoundInt64(FSubCredits * 100), 12)
        + '          ' + '          ' + '          ' + '         ');

      ACHFile.Post;
    end;

    // Grand-Total amounts (true)
    AppendFileTotalRecord;

    ACHFile.BATCH_NBR.Value := FTotNbrOfBatches;
    ACHFile.CUSTOM_EMPLOYEE_NUMBER.AsString := IntToStr(FxTotNbrOfEntries);
    // total could be too big to fit into ABA_NUMBER field,... so put into this one instead
    ACHFile.BANK_ACCOUNT_NUMBER.Value := ZeroFillInt64ToStr(FTotEntryHash, 10);
    ACHFile.DEBITS.Value := FTotDebits;
    ACHFile.CREDITS.Value := FTotCredits;
    ACHFile.SEGMENT_NBR.AsInteger := SegmentNbr;
    ACHFile.ORDER_NBR.Value := ACHFile.RecordCount;

    ACHFile.ACH_LINE.Value := UpperCase('9' + ZeroFillInt64ToStr(FTotNbrOfBatches, 6)
      + ZeroFillInt64ToStr((i + 9) div 10, 6) + ZeroFillInt64ToStr(FxTotNbrOfEntries, 8)
      + ZeroFillInt64ToStr(FTotEntryHash, 10) + ZeroFillInt64ToStr(RoundInt64(FTotDebits * 100), 12)
      + ZeroFillInt64ToStr(RoundInt64(FTotCredits * 100), 12)
      + '          ' + '          ' + '          ' + '         ');

    ACHFile.Post;

    Inc(SegmentNbr);

    ACHFile.AddDummyPadRecords(ACHFile.CountDummySubTotalRecords(Options.Header), SegmentNbr, PrNbr);
    ACHFile.AddAchFooter(Options.Footer, SegmentNbr, PrNbr);
    ACHFile.First;
  end
  else
    raise EACHNoMatchingData.Create('Sorry, but no matching ACH data was found!');

end;

constructor TevAchBase.Create(AOwner: TComponent);
begin
  inherited;
  DoOnConstruction;
end;

procedure TevAchBase.SetPrenote(const Value: Boolean);
begin
  FPrenote := Value;
end;

end.
