unit evCashManagement;

interface

uses IsBaseClasses, EvStreamUtils, evAchBase, evAchUtils, DateUtils, evDataset,
  evCommonInterfaces, evAchDataset, SysUtils, evConsts, SACHTypes, evUtils,
   evBasicUtils, SDataStructure, evExceptions, DB, evContext,
  evMainBoard, evAchPayrollList, SReportSettings, evTypes,
  evAch, evClasses, IsBasicUtils, EvClientDataSet;

type
  TevCashManagement = class(TisInterfacedObject, IevCashManagement)
  private
    function GetNearestWorkingDate(const ADate: TDateTime): TDateTime;
    procedure PatchAchFiles(const AchSave: IisListOfValues; Options: IevAchOptions);
  protected
    function GetDebugInfo(const AllBatches, AllTrans: IisListOfValues): String;
    procedure PrepareCashManagement(const Payrolls: IevDataset;
      const Options: IevAchOptions; const SbData: IisListOfValues;
      const Companies: IisParamsCollection);
    procedure ProcessCashManagementForCompany(const IncludeOffsets: Integer;
      const CompanyData: IisListOfValues; const Options: IevAchOptions; const SbData: IisListOfValues);
    procedure  FinalizeCashManagement(const Companies: IisListOfValues;
      const Options: IevACHOptions; const SbData: IisListOfValues;
      const IncludeOffsets: Integer; const Preprocess: Boolean;
      var AchFileReport: IisStream; var AchRegularReport: IisStream;
      var AchDetailedReport: IisStream; var NonAchReport: IisStream;
      var WTFile: IisStream; var FileName: String; var AchSave: IisListOfValues;
      var Exceptions: String; var Warnings: String; const FromDate, ToDate: TDateTime;
      const ACHFolder: String);
    procedure UpdateAchStatus(Payrolls: TevClientDataSet; const NewStatusIndex: Integer);
  end;

function GetCashManagement: IevCashManagement;

implementation

const
  TAX_TYPES_941 = '''' + TAX_LIABILITY_TYPE_FEDERAL + ''',' +
                   '''' + TAX_LIABILITY_TYPE_EE_OASDI + ''',' +
                   '''' + TAX_LIABILITY_TYPE_ER_OASDI + ''',' +
                   '''' + TAX_LIABILITY_TYPE_EE_MEDICARE + ''',' +
                   '''' + TAX_LIABILITY_TYPE_ER_MEDICARE + ''',' +
                   '''' + TAX_LIABILITY_TYPE_EE_EIC + ''',' +
                   '''' + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING + ''',' +
                   '''' + TAX_LIABILITY_TYPE_COBRA_CREDIT + '''';


procedure TevCashManagement.ProcessCashManagementForCompany(
  const IncludeOffsets: Integer; const CompanyData: IisListOfValues;
  const Options: IevAchOptions; const SbData: IisListOfValues);
var
  CO: IevTable;
  DataStream: IisStream;
  Dataset: IevDataset;
  CO_LOCAL_TAX_LIABILITIES: IevTable;
  CO_STATE_TAX_LIABILITIES: IevTable;
  CO_SUI_LIABILITIES: IevTable;
  CO_FED_TAX_LIABILITIES: IevTable;
  CO_PR_ACH: IevTable;
  CL_BANK_ACCOUNT: IevDataset;
  CL_NBR, CO_NBR, PR_NBR, RUN_NUMBER: Integer;
  CHECK_DATE: TDateTime;
  Item, A: IisListOfValues;
  PrList: String;
  FedTaxImpounded, TaxCredits: Currency;
  FedLiabDateFrom: TDateTime;
  i, j: Integer;
  Q, Q1: IevQuery;
  TaxDistr: TevClientDataset;
  TaxSum: Double;
  f: Double;
  DirectDepostTotal: Double;
  DBDTCount: Integer;
  BankNbr: Integer;
  StateTaxAmount, LocalTaxAmount, SuiAmount, TotalTaxes: Currency;
  Warnings, Exceptions, CompanyExceptions: String;

  BillingAmount: Currency;
  BillingBureauAccountNbr: Integer;
  BillingBureauAccountNumber: String;
  BillingBureauAccountType: String;
  BillingBureauAccountAba: String;

  TrustAmount: Currency;

  TrustBureauAccountNbr: Integer;
  TrustBureauAccountNumber: String;
  TrustBureauAccountType: String;
  TrustBureauAccountAba: String;
  TrustClientAccountNumber: String;
  TrustClientAccountType: String;
  TrustClientAccountAba: String;

  TaxBureauAccountNbr: Integer;
  TaxBureauAccountNumber: String;
  TaxBureauAccountType: String;
  TaxBureauAccountAba: String;

  WorkersCompAmount: Currency;
  WcBureauAccountNbr: Integer;
  WcBureauAccountNumber: String;
  WcBureauAccountType: String;
  WcBureauAccountAba: String;
  WcClientAccountNumber: String;
  WcClientAccountType: String;
  WcClientAccountAba: String;

  procedure UpdateStateTaxLiability;
  begin
    CO_STATE_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
    CO_STATE_TAX_LIABILITIES.Filtered := True;
    try
      CO_STATE_TAX_LIABILITIES.First;
      while not CO_STATE_TAX_LIABILITIES.Eof do
      begin
        CO_STATE_TAX_LIABILITIES.Edit;
        if (CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
        or (CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
        begin
          if CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS then
            CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID
          else
            CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
          if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
            CO_STATE_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_YES
          else
            CO_STATE_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
        end;
        CO_STATE_TAX_LIABILITIES.Post;
        CO_STATE_TAX_LIABILITIES.Next;
      end;
    finally
      CO_STATE_TAX_LIABILITIES.Filter := '';
      CO_STATE_TAX_LIABILITIES.Filtered := False;
    end;
  end;

  procedure UpdateLocalTaxLiability;
  begin
    CO_LOCAL_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
    CO_LOCAL_TAX_LIABILITIES.Filtered := True;
    try
      CO_LOCAL_TAX_LIABILITIES.First;
      while not CO_LOCAL_TAX_LIABILITIES.Eof do
      begin
        CO_LOCAL_TAX_LIABILITIES.Edit;
        if (CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
        or (CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
        begin
          if CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS then
            CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID
          else
            CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
          if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
            CO_LOCAL_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_YES
          else
            CO_LOCAL_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
        end;
        CO_LOCAL_TAX_LIABILITIES.Post;
        CO_LOCAL_TAX_LIABILITIES.Next;
      end;
    finally
      CO_LOCAL_TAX_LIABILITIES.Filter := '';
      CO_LOCAL_TAX_LIABILITIES.Filtered := False;
    end;
  end;

  procedure UpdateSuiLiability;
  begin
    CO_SUI_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
    CO_SUI_LIABILITIES.Filtered := True;
    try
      CO_SUI_LIABILITIES.First;
      while not CO_SUI_LIABILITIES.Eof do
      begin
        CO_SUI_LIABILITIES.Edit;
        if (CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
        or (CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
        begin
          if CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS then
            CO_SUI_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID
          else
            CO_SUI_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
          if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
            CO_SUI_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_YES
          else
            CO_SUI_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
        end;
        CO_SUI_LIABILITIES.Post;
        CO_SUI_LIABILITIES.Next;
      end;
    finally
      CO_SUI_LIABILITIES.Filter := '';
      CO_SUI_LIABILITIES.Filtered := False;
    end;
  end;

  procedure UpdateFederalTaxLiability;
  var
    AmountPending: Currency;
    Rec: array of Variant;
    i: Integer;
    cond: String;
  begin
    CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR) +
      ' AND (TAX_TYPE <> '''+TAX_LIABILITY_TYPE_COBRA_CREDIT+''' OR '+
      '(TAX_TYPE = '''+TAX_LIABILITY_TYPE_COBRA_CREDIT+''' AND AMOUNT > 0))';
    CO_FED_TAX_LIABILITIES.Filtered := True;
    CO_FED_TAX_LIABILITIES.First;
    while not CO_FED_TAX_LIABILITIES.Eof do
    begin
      CO_FED_TAX_LIABILITIES.Edit;

      if (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING)
      or (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS) then
      begin
        if CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS then
          CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID
        else
          CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
        if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
          CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_YES
        else
          CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
      end;

      CO_FED_TAX_LIABILITIES.Post;

      CO_FED_TAX_LIABILITIES.Next;
    end;

    cond :=
    '(PR_NBR = ' + IntToStr(PR_NBR)+' OR (CHECK_DATE >= '''+DateToStr(FedLiabDateFrom)+''' AND '+
      'CHECK_DATE <= '''+DateToStr(CHECK_DATE)+''' AND CO_NBR='+CO.FieldByName('CO_NBR').AsString+')) AND '+
      'TAX_TYPE = '''+ TAX_LIABILITY_TYPE_COBRA_CREDIT +'''';
    CO_FED_TAX_LIABILITIES.Filter := cond;
    CO_FED_TAX_LIABILITIES.Filtered := True;
    CO_FED_TAX_LIABILITIES.Last;
    while (FedTaxImpounded > 0) and not CO_FED_TAX_LIABILITIES.Bof do
    begin
      CO_FED_TAX_LIABILITIES.Edit;
      if (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) and
         CO_FED_TAX_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
      begin
        if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
          CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
        if Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH then
          CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_YES
        else
          CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
        if (- CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency) > FedTaxImpounded then
        begin
          AmountPending := CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency + FedTaxImpounded;
          CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency := -FedTaxImpounded;
          CO_FED_TAX_LIABILITIES.Post;
          SetLength(Rec, CO_FED_TAX_LIABILITIES.Fields.Count);
          for i := 0 to CO_FED_TAX_LIABILITIES.Fields.Count - 1 do
            Rec[i] := CO_FED_TAX_LIABILITIES.Fields[i].Value;
          CO_FED_TAX_LIABILITIES.Append;
          for i := 0 to CO_FED_TAX_LIABILITIES.Fields.Count - 1 do
            if not AnsiSameText(CO_FED_TAX_LIABILITIES.Fields[i].FieldName, 'CO_FED_TAX_LIABILITIES_NBR') then
              CO_FED_TAX_LIABILITIES.Fields[i].Value := Rec[i];
          CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency := AmountPending;
          CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PENDING;
          CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
        end
        else
          FedTaxImpounded := FedTaxImpounded + CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      end;
      CO_FED_TAX_LIABILITIES.Post;
      CO_FED_TAX_LIABILITIES.Prior;
    end;
  end;


  function GetTaxCredits(const CO_NBR: Integer; const CheckDate: TDateTime): Currency;
  var
    TaxCreditsDateFrom: TDateTime;
  begin
    TaxCreditsDateFrom := GetBeginQuarter(IncDay(GetBeginQuarter(CheckDate), -1));
    Result := 0;
    CO_FED_TAX_LIABILITIES.Filter := 'CO_NBR='+IntToStr(CO_NBR) +
      ' AND STATUS IN ('''+TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+''','''+
      TAX_DEPOSIT_STATUS_PENDING+''') AND TAX_TYPE = '''+ TAX_LIABILITY_TYPE_COBRA_CREDIT+'''' +
      ' AND CHECK_DATE >= '''+DateToStr(TaxCreditsDateFrom)+''' AND CHECK_DATE <='''+DateToStr(CheckDate)+'''';
    CO_FED_TAX_LIABILITIES.Filtered := True;
    CO_FED_TAX_LIABILITIES.First;
    while not CO_FED_TAX_LIABILITIES.Eof do
    begin
      if CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency < 0 then
        Result := Result + CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      CO_FED_TAX_LIABILITIES.Next;
    end;
    CO_FED_TAX_LIABILITIES.Filtered := False;
  end;

  function GetFedTaxImpounded(const CheckDate: TDateTime): Currency;
  var
    TaxCreditsDateFrom: TDateTime;
  begin
    TaxCreditsDateFrom := GetBeginQuarter(IncDay(GetBeginQuarter(CheckDate), -1));
    Result := 0;
    CO_FED_TAX_LIABILITIES.First;
    while not CO_FED_TAX_LIABILITIES.Eof do
    begin
      if (CO_FED_TAX_LIABILITIES.FieldByName('CHECK_DATE').AsDateTime >= TaxCreditsDateFrom)
        and (CO_FED_TAX_LIABILITIES.FieldByName('CHECK_DATE').AsDateTime <= CheckDate)
      and
      ((CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)
       or (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
      )
      and
      ((CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_FEDERAL)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_OASDI)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_ER_OASDI)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_MEDICARE)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_ER_MEDICARE)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_EIC)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING)
      or (CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_COBRA_CREDIT)
      ) then
      begin
        Result := Result + CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
      end;
      CO_FED_TAX_LIABILITIES.Next;
    end;
  end;

  function GetTotalTaxLiabilities: Currency;

    function GetFedTax940Liability: Currency;
    begin
      Result := 0;

      if CompanyData.Value['StatusToIndex'] = PR_ACH_STATUS_TO_SENT then
        CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+Dataset.FieldByName('PR_NBR').AsString +
        ' AND STATUS IN ('''+TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+''','''+
        TAX_DEPOSIT_STATUS_PENDING+''')'
      else
        CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+Dataset.FieldByName('PR_NBR').AsString;
      CO_FED_TAX_LIABILITIES.Filtered := True;


      CO_FED_TAX_LIABILITIES.First;
      while not CO_FED_TAX_LIABILITIES.Eof do
      begin
        if (CO_FED_TAX_LIABILITIES.FieldByName('PR_NBR').AsInteger = Dataset.FieldByName('PR_NBR').AsInteger) and
           not TaxIs941(CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString) and
           (TAX_LIABILITY_TYPE_COBRA_CREDIT <> CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString)
        then
          Result := Result + CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
        CO_FED_TAX_LIABILITIES.Next;
      end;
    end;

    function GetFedTax941Liability: Currency;
    begin
      Result := 0;
      if CompanyData.Value['StatusToIndex'] = PR_ACH_STATUS_TO_SENT then
        CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+Dataset.FieldByName('PR_NBR').AsString +
        ' AND STATUS IN ('''+TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+''','''+
        TAX_DEPOSIT_STATUS_PENDING+''')'
      else
        CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+Dataset.FieldByName('PR_NBR').AsString;
      CO_FED_TAX_LIABILITIES.Filtered := True;
      CO_FED_TAX_LIABILITIES.First;
      while not CO_FED_TAX_LIABILITIES.Eof do
      begin
        if (TaxIs941(CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString) and
           (TAX_LIABILITY_TYPE_COBRA_CREDIT <> CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString))
           or (
           (TAX_LIABILITY_TYPE_COBRA_CREDIT = CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString)
           and (CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency > 0))
        then
        begin
          Result := Result + CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency
        end;
        CO_FED_TAX_LIABILITIES.Next;
      end;
    end;

  var
    FedTax940, FedTax941, AchFedTax941, Credit941: Currency;
  begin

    if CO_PR_ACH.FieldByName('STATUS').AsString = COMMON_REPORT_STATUS__COMPLETED then
    begin
      FedTaxImpounded := StrToFloatDef(GetFromFiller(CO_PR_ACH.FieldByName('FILLER'), 'FEDTAXIMP'),0);
      TaxCredits := StrToFloatDef(GetFromFiller(CO_PR_ACH.FieldByName('FILLER'), 'TAXCREDIT'),0);
    end
    else
    begin
      FedTaxImpounded := GetFedTaxImpounded(Dataset.FieldByName('CHECK_DATE').AsDateTime);
      TaxCredits := GetTaxCredits(Dataset.FieldByName('CO_NBR').AsInteger, Dataset.FieldByName('CHECK_DATE').AsDateTime);
    end;

    CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+Dataset.FieldByName('PR_NBR').AsString;
    CO_FED_TAX_LIABILITIES.Filtered := True;
    CO_FED_TAX_LIABILITIES.First;

    FedTax940 := GetFedTax940Liability;
    FedTax941 := GetFedTax941Liability;

    if FedTax941 < 0 then
    begin
      Credit941 := FedTax941;
      FedTax941 := 0;
    end
    else
      Credit941 := 0;

    Result := FedTax940;

    if FedTaxImpounded < 0 then
      FedTaxImpounded := 0;

    AchFedTax941 := FedTax941 + TaxCredits;
    if AchFedTax941 < 0 then
    begin
      if - AchFedTax941 > FedTaxImpounded then
        AchFedTax941 := -FedTaxImpounded;
    end;
    FedTaxImpounded := FedTaxImpounded + FedTax941;

    AchFedTax941 := AchFedTax941 + Credit941;

    Result := Result + AchFedTax941;

    Result := Result + StateTaxAmount;
    Result := Result + LocalTaxAmount;
    Result := Result + SuiAmount;

  end;

  procedure RaiseLimitationException(const Mes: String);
  begin
    if CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_WARN then
    begin
      if Length(Warnings) > 0 then
        Warnings := Warnings + #13#10;
      Warnings := Warnings + Mes;
    end
    else if (CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_WOC) or (CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_STOP) then
    begin
      if Length(CompanyExceptions) > 0 then
        CompanyExceptions := CompanyExceptions + #13#10;
      CompanyExceptions := CompanyExceptions + Mes;
    end;
  end;

var
  ParamName, Account, Aba, AccountType, PayrollType, s: String;

  XML: IisStringList;
  Transactions, TmpTransactions, XMLs: IisListOfValues;

  TaxesByDates: IisListOfValues;

  Stopped: Boolean;
  DDTransactions: IisListOfValues;
  BillingTransactions: IisListOfValues;
  TrustTransactions: IisListOfValues;
  TaxTransactions: IisListOfValues;
  WcTransactions: IisListOfValues;
  Tran, TmpTran: IevTransaction;
  EffectiveDate: TDateTime;
  Impounds: IisParamsCollection;
  Employees: IisParamsCollection;
  ChildSupport: IisParamsCollection;
  Agencies: IisParamsCollection;
  Garnishment: IisParamsCollection;
  UsingOffsets: Boolean;

  data_CO_STATE_TAX_LIABILITIES: IisStream;
  data_CO_LOCAL_TAX_LIABILITIES: IisStream;
  data_CO_SUI_LIABILITIES: IisStream;
  data_CO_FED_TAX_LIABILITIES: IisStream;

  idata_CO_STATE_TAX_LIABILITIES: IisStream;
  idata_CO_LOCAL_TAX_LIABILITIES: IisStream;
  idata_CO_SUI_LIABILITIES: IisStream;
  idata_CO_FED_TAX_LIABILITIES: IisStream;

  deltas_CO_STATE_TAX_LIABILITIES: IisListOfValues;
  deltas_CO_LOCAL_TAX_LIABILITIES: IisListOfValues;
  deltas_CO_SUI_LIABILITIES: IisListOfValues;
  deltas_CO_FED_TAX_LIABILITIES: IisListOfValues;

  WriteOff: IisParamsCollection;
  WriteOffItem: IisListOfValues;

  procedure GetDBDTAccount(const Team, Department, Branch, Division, Mode: String; const Amount: Currency; var AccountNumber, AbaNumber, AccountType, ParamName: String);
  var
    Dbdt, Accounts, SbBanks: IevDataset;
  const
    DbdtSelect = 'SELECT %s_CL_BANK_ACCOUNT_NBR AS CL_BANK_ACCOUNT_NBR, CUSTOM_%s_NUMBER AS NAME FROM CO_%s d WHERE {AsOfNow<d>} AND CO_%s_NBR=%s';
    AccSelect = 'SELECT CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE, SB_BANKS_NBR FROM CL_BANK_ACCOUNT WHERE {AsOfNow<CL_BANK_ACCOUNT>} AND CL_BANK_ACCOUNT_NBR=%s';
  begin
    if Team <> '' then
    begin
      Dbdt := (TevQuery.Create(Format(DbdtSelect, [Mode,'TEAM','TEAM','TEAM',Team])) as IevQuery).Result;
      if not Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').IsNull then
      begin
        Accounts := (TevQuery.Create(Format(AccSelect, [Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').AsString])) as IevQuery).Result;
        if Accounts.RecordCount = 0 then
          raise EInconsistentData.CreateHelp(Mode+'_CL_BANK_ACCOUNT_NBR is missing for team ' + Dbdt.FieldByName('NAME').AsString, IDH_InconsistentData);
        ParamName := Team+';'+Department+';'+Branch+';'+Division;
      end;
    end;
    if (ParamName = '') and (Department <> '') then
    begin
      Dbdt := (TevQuery.Create(Format(DbdtSelect, [Mode,'DEPARTMENT','DEPARTMENT','DEPARTMENT',Department])) as IevQuery).Result;
      if not Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').IsNull then
      begin
        Accounts := (TevQuery.Create(Format(AccSelect, [Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').AsString])) as IevQuery).Result;
        if Accounts.RecordCount = 0 then
          raise EInconsistentData.CreateHelp(Mode+'_CL_BANK_ACCOUNT_NBR is missing for department ' + Dbdt.FieldByName('NAME').AsString, IDH_InconsistentData);
        ParamName := 'NULL;'+Department+';'+Branch+';'+Division;
      end;
    end;
    if (ParamName = '') and (Branch <> '')then
    begin
      Dbdt := (TevQuery.Create(Format(DbdtSelect, [Mode,'BRANCH','BRANCH','BRANCH',Branch])) as IevQuery).Result;
      if not Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').IsNull then
      begin
        Accounts := (TevQuery.Create(Format(AccSelect, [Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').AsString])) as IevQuery).Result;
        if CL_BANK_ACCOUNT.RecordCount = 0 then
          raise EInconsistentData.CreateHelp(Mode+'_CL_BANK_ACCOUNT_NBR is missing for branch ' + Dbdt.FieldByName('NAME').AsString, IDH_InconsistentData);
        ParamName := 'NULL;NULL;'+Branch+';'+Division;
      end;
    end;
    if (ParamName = '') and (Division <> '') then
    begin
      Dbdt := (TevQuery.Create(Format(DbdtSelect, [Mode,'DIVISION','DIVISION','DIVISION',Division])) as IevQuery).Result;
      if not Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').IsNull then
      begin
        Accounts := (TevQuery.Create(Format(AccSelect, [Dbdt.FieldByName('CL_BANK_ACCOUNT_NBR').AsString])) as IevQuery).Result;
        if CL_BANK_ACCOUNT.RecordCount = 0 then
          raise EInconsistentData.CreateHelp(Mode+'_CL_BANK_ACCOUNT_NBR is missing for division ' + Dbdt.FieldByName('NAME').AsString, IDH_InconsistentData);
        ParamName := 'NULL;NULL;NULL;'+Division;
      end;
    end;
    if (ParamName = '') then
    begin
      ParamName := 'NULL;NULL;NULL;NULL';
      Accounts := (TevQuery.Create(Format(AccSelect, [CO.FieldByName(Mode+'_CL_BANK_ACCOUNT_NBR').AsString])) as IevQuery).Result;
    end;
    SbBanks := (TevQuery.Create('SELECT ALLOW_HYPHENS, ABA_NUMBER FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+Accounts.FieldByName('SB_BANKS_NBR').AsString) as IevQuery).Result;
    if SbBanks.RecordCount = 0 then
      raise EevException.Create('Problem with client bank account, Company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ', Client Bank Account ' + Accounts.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString);
    if AccountNumber <> '' then
    begin

    end
    else
    begin
      if SbBanks.FieldByName('ALLOW_HYPHENS').Value <> GROUP_BOX_YES then
        AccountNumber := ACHTrim(Accounts.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString)
      else
        AccountNumber := Accounts.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
      AccountType := Accounts.FieldByName('BANK_ACCOUNT_TYPE').AsString;
      AbaNumber := ACHTrim(SbBanks.FieldByName('ABA_NUMBER').AsString);
    end;
  end;


begin

  deltas_CO_STATE_TAX_LIABILITIES := TisListOfValues.Create;
  deltas_CO_LOCAL_TAX_LIABILITIES := TisListOfValues.Create;
  deltas_CO_SUI_LIABILITIES       := TisListOfValues.Create;
  deltas_CO_FED_TAX_LIABILITIES   := TisListOfValues.Create;

  Transactions        := TisListOfValues.Create;

  WriteOff            := TisParamsCollection.Create;
  TaxesByDates        := TisListOfValues.Create;

  Stopped := False;
  Warnings := '';
  Exceptions := '';

  CL_NBR := CompanyData.Value['CL_NBR'];
  CO_NBR := CompanyData.Value['CO_NBR'];

  ctx_DataAccess.OpenClient(CL_NBR);

  Dataset := TevDataSet.Create;

  DataStream := IInterface(CompanyData.Value['Payrolls']) as IevDualStream;
  DataStream.Position := 0;
  Dataset.Data := DataStream;

  Dataset.First;
  PrList := Dataset.FieldByName('PR_NBR').AsString;
  FedLiabDateFrom := GetBeginQuarter(IncDay(GetBeginQuarter(Dataset.FieldByName('CHECK_DATE').AsDateTime), -1));
  Dataset.Next;

  while not Dataset.Eof do
  begin
    PrList := PrList + ',' + Dataset.FieldByName('PR_NBR').AsString;
    if FedLiabDateFrom > GetBeginQuarter(IncDay(GetBeginQuarter(Dataset.FieldByName('CHECK_DATE').AsDateTime), -1)) then
       FedLiabDateFrom := GetBeginQuarter(IncDay(GetBeginQuarter(Dataset.FieldByName('CHECK_DATE').AsDateTime), -1));
    Dataset.Next;
  end;

  CO := TevTable.Create('CO', 'CO_NBR,CO_ACH_PROCESS_LIMITATIONS,DD_CL_BANK_ACCOUNT_NBR,NAME,'+
                              'LEGAL_NAME,WELLS_FARGO_ACH_FLAG,FEIN,CUSTOM_COMPANY_NUMBER,'+
                              'BANK_ACCOUNT_REGISTER_NAME,DEBIT_NUMBER_OF_DAYS_PRIOR_DD,'+
                              'CO_MAX_AMOUNT_FOR_DD,DEBIT_NUMBER_DAYS_PRIOR_BILL,BILLING_LEVEL,'+
                              'BILLING_SB_BANK_ACCOUNT_NBR,TRUST_SERVICE,OBC,TRUST_SB_ACCOUNT_NBR,'+
                              'PAYROLL_CL_BANK_ACCOUNT_NBR,CO_MAX_AMOUNT_FOR_PAYROLL,DEBIT_NUMBER_DAYS_PRIOR_PR,'+
                              'TAX_SERVICE,CO_MAX_AMOUNT_FOR_TAX_IMPOUND,TAX_SB_BANK_ACCOUNT_NBR,'+
                              'DEBIT_NUMBER_OF_DAYS_PRIOR_TAX,W_COMP_SB_BANK_ACCOUNT_NBR,W_COMP_CL_BANK_ACCOUNT_NBR,'+
                              'DEBIT_NUMBER_DAYS_PRIOR_WC,ACH_SB_BANK_ACCOUNT_NBR,BILLING_CL_BANK_ACCOUNT_NBR,'+
                              'TAX_CL_BANK_ACCOUNT_NBR,PAYROLL_CL_BANK_ACCOUNT_NBR,DD_CL_BANK_ACCOUNT_NBR,'+
                              'W_COMP_CL_BANK_ACCOUNT_NBR,MANUAL_CL_BANK_ACCOUNT_NBR',
                              'CO_NBR='+IntToStr(CO_NBR), SysDate);

  CO_LOCAL_TAX_LIABILITIES := TevTable.Create('CO_LOCAL_TAX_LIABILITIES', '', 'PR_NBR IN ('+PrList+') AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''', SysDate);
  CO_STATE_TAX_LIABILITIES := TevTable.Create('CO_STATE_TAX_LIABILITIES', '', 'PR_NBR IN ('+PrList+') AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''', SysDate);
  CO_SUI_LIABILITIES       := TevTable.Create('CO_SUI_LIABILITIES', '', 'PR_NBR IN ('+PrList+') AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''', SysDate);
  CO_FED_TAX_LIABILITIES   := TevTable.Create('CO_FED_TAX_LIABILITIES', '', 'CO_NBR='+IntToStr(CO_NBR)+' AND CHECK_DATE >= '''+ DateToStr(FedLiabDateFrom)+ ''' AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''', SysDate);

  CO_PR_ACH := TevTable.Create('CO_PR_ACH', '', 'PR_NBR IN ('+PrList+')', SysDate);

  CL_BANK_ACCOUNT := (TevQuery.Create('select * from CL_BANK_ACCOUNT where {AsOfNow<CL_BANK_ACCOUNT>} and CL_BANK_ACCOUNT_NBR='+CO.FieldByName('DD_CL_BANK_ACCOUNT_NBR').AsString) as IevQuery).Result;

  Q := TevQuery.Create('select NAME, PRINT_CLIENT_NAME from CL where {AsOfNow<CL>}');

  CompanyData.AddValue('CompanyName', CO.FieldByName('NAME').AsString);
  CompanyData.AddValue('LegalName', CO.FieldByName('LEGAL_NAME').AsString);
  CompanyData.AddValue('ClientName', Q.Result['NAME']);
  CompanyData.AddValue('WELLS_FARGO_ACH_FLAG', CO.FieldByName('WELLS_FARGO_ACH_FLAG').AsString);

  if Options.UseSBEIN then
    if Options.SBEINOverride = '' then
      CompanyData.AddValue('CompanyIdentification', Trim(SbData.Value['SB_EIN_NUMBER']))
    else
      CompanyData.AddValue('CompanyIdentification', Options.SBEINOverride)
  else
    CompanyData.AddValue('CompanyIdentification', CO.FieldByName('FEIN').AsString);

  CompanyData.AddValue('CoId', Options.CoId);

  CompanyData.AddValue('FEIN', CO.FieldByName('FEIN').AsString);
  CompanyData.AddValue('SB_EIN', SbData.Value['SB_EIN_NUMBER']);

  if Options.FedReserve then
  begin
    Q1 := TevQuery.Create('select ABA_NUMBER from SB_BANKS where {AsOfNow<SB_BANKS>} and SB_BANKS_NBR='+
      CL_BANK_ACCOUNT.FieldByName('SB_BANKS_NBR').AsString);

    if Q1.Result.RecordCount = 0 then
       raise EInconsistentData.CreateHelp('DD_CL_BANK_ACCOUNT_NBR is missing for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

    CompanyData.AddValue('ORIGIN_BANK_ABA', ACHTrim(Q1.Result.FieldByName('ABA_NUMBER').AsString));
  end
  else
    CompanyData.AddValue('ORIGIN_BANK_ABA', SbData.Value['SbOriginBankAba']);

  CompanyData.AddValue('PRINT_CLIENT_NAME', Q.Result['PRINT_CLIENT_NAME']);
  CompanyData.AddValue('BANK_ACCOUNT_REGISTER_NAME', CO.FieldByName('BANK_ACCOUNT_REGISTER_NAME').AsString);

  idata_CO_STATE_TAX_LIABILITIES := CO_STATE_TAX_LIABILITIES.Data;
  idata_CO_LOCAL_TAX_LIABILITIES := CO_LOCAL_TAX_LIABILITIES.Data;
  idata_CO_SUI_LIABILITIES       := CO_SUI_LIABILITIES.Data;
  idata_CO_FED_TAX_LIABILITIES   := CO_FED_TAX_LIABILITIES.Data;

  Dataset.First;
  while not Dataset.Eof do
  begin
    CompanyExceptions := '';
    data_CO_STATE_TAX_LIABILITIES := CO_STATE_TAX_LIABILITIES.Data;
    data_CO_LOCAL_TAX_LIABILITIES := CO_LOCAL_TAX_LIABILITIES.Data;
    data_CO_SUI_LIABILITIES       := CO_SUI_LIABILITIES.Data;
    data_CO_FED_TAX_LIABILITIES   := CO_FED_TAX_LIABILITIES.Data;

    try
      PR_NBR     := Dataset.FieldByName('PR_NBR').AsInteger;
      RUN_NUMBER := Dataset.FieldByName('RUN_NUMBER').AsInteger;
      CHECK_DATE := Dataset.FieldByName('CHECK_DATE').AsDateTime;

      Q := TevQuery.Create('select PAYROLL_TYPE from PR where {AsOfNow<PR>} and PR_NBR=' + Dataset.FieldByName('PR_NBR').AsString);
      PayrollType := Q.Result['PAYROLL_TYPE'];

      CO_PR_ACH.Locate('PR_NBR', PR_NBR, []);

      StateTaxAmount  := 0;
      LocalTaxAmount  := 0;
      SuiAmount       := 0;

      DDTransactions      := TIsListOfValues.Create;
      BillingTransactions := TIsListOfValues.Create;
      TrustTransactions   := TIsListOfValues.Create;
      TaxTransactions     := TIsListOfValues.Create;
      WcTransactions      := TIsListOfValues.Create;

      DirectDepostTotal := 0;

      EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_OF_DAYS_PRIOR_DD').AsInteger);

      Employees := GetDirectDeposits(PR_NBR, Dataset.FieldByName('CHECK_DATE').AsDateTime, CompanyData.Value['BlockNegativeChecks'], CompanyData.Value['SbUsePrenote'], False);
      Impounds := TisParamsCollection.Create;

      if Assigned(Employees) then
      begin
        for i := 0 to Employees.Count - 1 do
        begin
          Item := IInterface(Employees[i]) as IisListOfValues;
          if (Abs(Item.Value['AMOUNT']) > 0.005) or (Item.Value['IN_PRENOTE']='Y') then
          begin
            // adding the employee direct deposit transaction
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := Item.Value['AMOUNT'];
            Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
            Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
            Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
            Tran.NAME                   := Item.Value['NAME'];
            Tran.CUSTOM_EMPLOYEE_NUMBER := Item.Value['CUSTOM_EMPLOYEE_NUMBER'];
            Tran.IN_PRENOTE             := Item.Value['IN_PRENOTE'];
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
            Tran.IMPOUND_TYPE           := PAYMENT_TYPE_ACH;
            Tran.TRANSACTION_TYPE       := EMPLOYEE_DIRECT_DEPOSIT;
            Tran.SSN                    := Item.Value['SSN'];
            if (CompanyData.Value['HSA']) and Item.ValueExists('HSA') then
              Tran.HSA := Item.Value['HSA']
            else
              Tran.HSA := '';  

            DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);

            ParamName := '';
            Account := '';
            Aba := '';
            AccountType := '';

            if Item.Value['IN_PRENOTE'] <> 'Y' then
              DirectDepostTotal := DirectDepostTotal + Item.Value['AMOUNT'];

            GetDBDTAccount(Item.Value['TEAM'], Item.Value['DEPARTMENT'], Item.Value['BRANCH'], Item.Value['DIVISION'], Item.Value['MODE'], Item.Value['AMOUNT'], Account, Aba, AccountType, ParamName);

            ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

            if Item.Value['AMOUNT'] < 0.005 then
              ParamName := ParamName + ';-'
            else
              ParamName := ParamName + ';+';

            if not CompanyData.Value['BlockCompanyDD'] then
            begin
              A := Impounds.ParamsByName(ParamName);
              if not Assigned(A) then
              begin
                A := Impounds.AddParams(ParamName);
                A.AddValue('AMOUNT', - Item.Value['AMOUNT']);
                A.AddValue('NAME', 'DD IMPOUND');
                A.AddValue('BANK_ACCOUNT_NUMBER', Account);
                A.AddValue('ABA_NUMBER', Aba);
                A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
              end
              else
                A.Value['AMOUNT'] := A.Value['AMOUNT'] - Item.Value['AMOUNT'];
            end;
          end;
        end;
      end;

      ChildSupport := GetChildSupport(PR_NBR, Dataset.FieldByName('CHECK_DATE').AsDateTime, CompanyData.Value['BlockNegativeChecks'], CompanyData.Value['SbUsePrenote'], False);

      if Assigned(ChildSupport) then
      begin
        for i := 0 to ChildSupport.Count - 1 do
        begin
          Item := IInterface(ChildSupport[i]) as IisListOfValues;

          // adding the child support transaction
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := Item.Value['AMOUNT'];
          Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
          Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
          Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
          Tran.NAME                   := Item.Value['NAME'];
          Tran.CUSTOM_EMPLOYEE_NUMBER := Item.Value['CUSTOM_EMPLOYEE_NUMBER'];
          Tran.IN_PRENOTE             := Item.Value['IN_PRENOTE'];
          Tran.Addenda                := Item.Value['Addenda'];
          Tran.SSN                    := Item.Value['SSN'];
          Tran.STATE                  := Item.Value['ORIGINATION_STATE'];
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
          Tran.IMPOUND_TYPE           := PAYMENT_TYPE_ACH;
          Tran.TRANSACTION_TYPE       := CHILD_SUPPORT_PAYMENT;
          DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);


          DirectDepostTotal := DirectDepostTotal + Item.Value['AMOUNT'];

          if Abs(Item.Value['AMOUNT']) > 0.005 then
          begin
            ParamName := '';
            Account := '';
            Aba := '';
            AccountType := '';

            GetDBDTAccount(Item.Value['TEAM'], Item.Value['DEPARTMENT'], Item.Value['BRANCH'], Item.Value['DIVISION'], Item.Value['MODE'], Item.Value['AMOUNT'], Account, Aba, AccountType, ParamName);

            ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';Y';

            if Item.Value['AMOUNT'] < 0.005 then
              ParamName := ParamName + ';-'
            else
              ParamName := ParamName + ';+';

            if (Tran.STATE = 'AL') then
              ParamName := ParamName + ';ALCS'
            else if (Tran.STATE = 'WY') then
              ParamName := ParamName + ';WYCS'
            else if (Tran.STATE = 'ME') then
              ParamName := ParamName + ';MECS'
            else if (Tran.STATE = 'MI') then
              ParamName := ParamName + ';MICS';

            A := Impounds.ParamsByName(ParamName);

            if not Assigned(A) then
            begin
              A := Impounds.AddParams(ParamName);
              A.AddValue('AMOUNT', - Item.Value['AMOUNT']);
              A.AddValue('NAME', 'DD IMPOUND');
              A.AddValue('BANK_ACCOUNT_NUMBER', Account);
              A.AddValue('ABA_NUMBER', Aba);
              A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
              A.AddValue('CHILD', True);
              if (Tran.STATE = 'AL') then
                A.AddValue('STATE', 'AL')
              else if (Tran.STATE = 'WY') then
                A.AddValue('STATE', 'WY')
              else if (Tran.STATE = 'ME') then
                A.AddValue('STATE', 'ME')
              else if (Tran.STATE = 'MI') then
                A.AddValue('STATE', 'MI');
            end
            else
              A.Value['AMOUNT'] := A.Value['AMOUNT'] - Item.Value['AMOUNT'];
          end;
        end;
      end;

      Agencies := GetAgencyDirectDeposits(PR_NBR, CompanyData.Value['BlockNegativeChecks'], False);

      if Assigned(Agencies) then
      begin
        for i := 0 to Agencies.Count - 1 do
        begin
          Item := IInterface(Agencies[i]) as IisListOfValues;

          // adding the agency direct deposit transaction
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := Item.Value['AMOUNT'];
          Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
          Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
          Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
          Tran.NAME                   := Item.Value['NAME'];
          Tran.IN_PRENOTE             := Item.Value['IN_PRENOTE'];
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
          Tran.IMPOUND_TYPE           := PAYMENT_TYPE_ACH;
          Tran.TRANSACTION_TYPE       := AGENCY_DIRECT_DEPOSIT;

          DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);

          DirectDepostTotal := DirectDepostTotal + Item.Value['AMOUNT'];

          ParamName := 'NULL;NULL;NULL;NULL;' +';DD;'+Item.Value['CLIENT_BANK_ACCOUNT_NUMBER']+';'+Item.Value['CLIENT_ABA_NUMBER']+';'+Item.Value['CLIENT_BANK_ACCOUNT_TYPE']+';N';

          if Item.Value['AMOUNT'] < 0.005 then
            ParamName := ParamName + ';-'
          else
            ParamName := ParamName + ';+';

          A := Impounds.ParamsByName(ParamName);
          if not Assigned(A) then
          begin
            A := Impounds.AddParams(ParamName);
            A.AddValue('AMOUNT', - Item.Value['AMOUNT']);
            A.AddValue('NAME', 'DD IMPOUND');
            A.AddValue('BANK_ACCOUNT_NUMBER', Item.Value['CLIENT_BANK_ACCOUNT_NUMBER']);
            A.AddValue('ABA_NUMBER', Item.Value['CLIENT_ABA_NUMBER']);
            A.AddValue('BANK_ACCOUNT_TYPE', Item.Value['CLIENT_BANK_ACCOUNT_TYPE']);
          end
          else
            A.Value['AMOUNT'] := A.Value['AMOUNT'] - Item.Value['AMOUNT'];

        end;
      end;

      Garnishment := GetGarnishmentDirectDeposits(PR_NBR, CompanyData.Value['BlockNegativeChecks'], False);

      if Assigned(Garnishment) then
      begin
        for i := 0 to Garnishment.Count - 1 do
        begin
          Item := IInterface(Garnishment[i]) as IisListOfValues;

          // adding the agency direct deposit transaction
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := Item.Value['AMOUNT'];
          Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
          Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
          Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
          Tran.NAME                   := Item.Value['NAME'];
          Tran.IN_PRENOTE             := Item.Value['IN_PRENOTE'];
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
          Tran.IMPOUND_TYPE           := PAYMENT_TYPE_ACH;
          Tran.TRANSACTION_TYPE       := AGENCY_DIRECT_DEPOSIT;
          Tran.Addenda                := Item.Value['Addenda'];
          DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);

          DirectDepostTotal := DirectDepostTotal + Item.Value['AMOUNT'];

          ParamName := 'NULL;NULL;NULL;NULL;' +';DD;'+Item.Value['CLIENT_BANK_ACCOUNT_NUMBER']+';'+Item.Value['CLIENT_ABA_NUMBER']+';'+Item.Value['CLIENT_BANK_ACCOUNT_TYPE']+';N';

          if Item.Value['AMOUNT'] < 0.005 then
            ParamName := ParamName + ';-'
          else
            ParamName := ParamName + ';+';

          A := Impounds.ParamsByName(ParamName);
          if not Assigned(A) then
          begin
            A := Impounds.AddParams(ParamName);
            A.AddValue('AMOUNT', - Item.Value['AMOUNT']);
            A.AddValue('NAME', 'DD IMPOUND');
            A.AddValue('BANK_ACCOUNT_NUMBER', Item.Value['CLIENT_BANK_ACCOUNT_NUMBER']);
            A.AddValue('ABA_NUMBER', Item.Value['CLIENT_ABA_NUMBER']);
            A.AddValue('BANK_ACCOUNT_TYPE', Item.Value['CLIENT_BANK_ACCOUNT_TYPE']);
          end
          else
            A.Value['AMOUNT'] := A.Value['AMOUNT'] - Item.Value['AMOUNT'];

        end;
      end;

      if (Impounds.Count > 0) and (Dataset.FieldByName('DD_IMPOUND').AsString <> PAYMENT_TYPE_NONE) then
      begin

        for i := 0 to Impounds.Count - 1 do
        begin
          Item := IInterface(Impounds[i]) as IisListOfValues;
          if Abs(Item.Value['AMOUNT']) > 0.005 then
          begin
            ParamName := Impounds.ParamName(i);

            UsingOffsets := (IncludeOffsets <> OFF_DONT_USE)
              and not Options.BlockDebits
              and (Dataset.FieldByName('DD_IMPOUND').AsString = PAYMENT_TYPE_ACH);

            // adding the direct deposit impound transaction
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
            Tran.Amount                 := Item.Value['AMOUNT'];
            Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
            Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
            Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
            Tran.NAME                   := Item.Value['NAME'];
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE       := EffectiveDate;

            Tran.IMPOUND_TYPE           := Dataset.FieldByName('DD_IMPOUND').AsString;
            if not Item.ValueExists('CHILD') then
              Tran.TRANSACTION_TYPE       := DIRECT_DEPOSIT_IMPOUND
            else
            begin
              Tran.TRANSACTION_TYPE       := CHILD_SUPPORT_IMPOUND;
              if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'AL') then
                Tran.STATE := 'AL'
              else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'MI') then
                Tran.STATE := 'MI'
              else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'WY') then
                Tran.STATE := 'WY'
              else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'ME') then
                Tran.STATE := 'ME';
            end;

            DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);

            if UsingOffsets then
            begin
              // credit offset transactions effective as dd effective date
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.Amount                 := -Item.Value['AMOUNT'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := 'DD IMPOUND';
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := EffectiveDate;
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('DD_IMPOUND').AsString;
              if not Item.ValueExists('CHILD') then
                Tran.TRANSACTION_TYPE       := DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT
              else
              begin
                Tran.TRANSACTION_TYPE       := CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT;
                if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'AL') then
                  Tran.STATE := 'AL'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'WY') then
                  Tran.STATE := 'WY'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'MI') then
                  Tran.STATE := 'MI'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'ME') then
                  Tran.STATE := 'ME';
              end;
              DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);
              // debit offset transactions effective as check date
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.Amount                 := Item.Value['AMOUNT'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := 'DD IMPOUND';
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('DD_IMPOUND').AsString;
              if not Item.ValueExists('CHILD') then
                Tran.TRANSACTION_TYPE       := DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT
              else
              begin
                Tran.TRANSACTION_TYPE       := CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT;
                if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'AL') then
                  Tran.STATE := 'AL'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'WY') then
                  Tran.STATE := 'WY'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'MI') then
                  Tran.STATE := 'MI'
                else if Item.ValueExists('STATE') and (Item.Value['STATE'] = 'ME') then
                  Tran.STATE := 'ME';
              end;
              DDTransactions.AddValue(IntToStr(DDTransactions.Count), Tran);

            end;
          end;
        end;
      end;

      if (DirectDepostTotal > 0.005) and not CO.FieldByName('CO_MAX_AMOUNT_FOR_DD').IsNull
      and (CO.FieldByName('CO_MAX_AMOUNT_FOR_DD').AsCurrency > 0.005) then
      begin
        if DirectDepostTotal > CO.FieldByName('CO_MAX_AMOUNT_FOR_DD').AsCurrency then
        begin
          if not CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').IsNull then
          begin
            s := Format ('%s %s-%d Direct deposit impound %0.2f exceeded the set limitation of %0.2f',
              [CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString,
               DateToStr(CHECK_DATE), RUN_NUMBER,
               DirectDepostTotal, CO.FieldByName('CO_MAX_AMOUNT_FOR_DD').AsCurrency]);

            if (CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_WOC) then
              s := 'SKIPPED: '+s;

            RaiseLimitationException(s);
          end;
        end;
      end;

      if Dataset.FieldByName('BILLING_IMPOUND').AsString <> PAYMENT_TYPE_NONE then
      begin

        Impounds := TisParamsCollection.Create;

        BillingAmount := 0;

        EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_DAYS_PRIOR_BILL').AsInteger);

        Q := TevQuery.Create('BillingForACH');
        Q.Params.AddValue('PrNbr', PR_NBR);

        if CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_COMPANY then
        begin
          Q.Macros.AddValue('DBDT', '');
          ParamName := '';
          Account := '';
          Aba := '';
          AccountType := '';

          GetDBDTAccount('', '', '', '', 'BILLING', 0, Account, Aba, AccountType, ParamName);

          ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

          A := Impounds.AddParams(ParamName);
          A.AddValue('AMOUNT', - Q.Result.FieldByName('AMOUNT').AsCurrency);
          A.AddValue('NAME', 'BILLING IMPOUND');
          A.AddValue('BANK_ACCOUNT_NUMBER', Account);
          A.AddValue('ABA_NUMBER', Aba);
          A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
          BillingAmount := Q.Result.FieldByName('AMOUNT').AsCurrency;

        end
        else
        begin
          Q.Macros.AddValue('DBDT', 'b.CO_DIVISION_NBR, b.CO_BRANCH_NBR, b.CO_DEPARTMENT_NBR, b.CO_TEAM_NBR,');
          while not Q.Result.Eof do
          begin
            ParamName := '';
            Account := '';
            Aba := '';
            AccountType := '';
            GetDBDTAccount(Q.Result.FieldByName('CO_TEAM_NBR').AsString, Q.Result.FieldByName('CO_DEPARTMENT_NBR').AsString,
                           Q.Result.FieldByName('CO_BRANCH_NBR').AsString, Q.Result.FieldByName('CO_DIVISION_NBR').AsString,
                           'BILLING', 0, Account, Aba, AccountType, ParamName);
            ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

            if Options.BalanceBatches then
            begin
              if Q.Result.FieldByName('AMOUNT').AsCurrency < 0.005 then
                ParamName := ParamName + ';-'
              else
                ParamName := ParamName + ';+';
            end;

            A := Impounds.ParamsByName(ParamName);
            if not Assigned(A) then
            begin
              A := Impounds.AddParams(ParamName);
              A.AddValue('AMOUNT', - Q.Result.FieldByName('AMOUNT').AsCurrency);
              A.AddValue('NAME', 'BILLING IMPOUND');
              A.AddValue('BANK_ACCOUNT_NUMBER', Account);
              A.AddValue('ABA_NUMBER', Aba);
              A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
            end
            else
              A.Value['AMOUNT'] := A.Value['AMOUNT'] - Q.Result.FieldByName('AMOUNT').AsCurrency;
            BillingAmount := BillingAmount + Q.Result.FieldByName('AMOUNT').AsCurrency;
            Q.Result.Next;
          end;
        end;

        if BillingAmount <> 0 then
        begin
          if CO.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').IsNull then
            raise EevException.Create('Billing SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          Q := TevQuery.Create('SELECT SB_BANKS_NBR, CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE FROM SB_BANK_ACCOUNTS WHERE {AsOfNow<SB_BANK_ACCOUNTS>} AND SB_BANK_ACCOUNTS_NBR='+
            CO.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').AsString);

          Q.Execute;

          if Q.Result.RecordCount = 0 then
            raise EevException.Create('Billing SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          BillingBureauAccountNbr := CO.FieldByName('BILLING_SB_BANK_ACCOUNT_NBR').AsInteger;
          BillingBureauAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
          BillingBureauAccountType   := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
          BankNbr                    := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

          Q := TevQuery.Create('SELECT ABA_NUMBER, ALLOW_HYPHENS FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
          Q.Execute;
          BillingBureauAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

          if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
            BillingBureauAccountNumber := ACHTrim(BillingBureauAccountNumber);

          for i := 0 to Impounds.Count - 1 do
          begin
            Item := IInterface(Impounds[i]) as IisListOfValues;
            if Abs(Item.Value['AMOUNT']) > 0.005 then
            begin
              // debit client account for billing
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.Amount                 := Item.Value['AMOUNT'];
              Tran.BANK_ACCOUNT_NBR       := BillingBureauAccountNbr;
              Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
              Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
              Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
              Tran.NAME                   := 'BILLING IMPOUND';
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := EffectiveDate;
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('BILLING_IMPOUND').AsString;
              Tran.TRANSACTION_TYPE       := BILLING_IMPOUND_DEBIT_CLIENT;
              BillingTransactions.AddValue(IntToStr(BillingTransactions.Count), Tran);
            end;
          end;  

          UsingOffsets := (IncludeOffsets = OFF_USE_OFFSETS) and not Options.BlockDebits
            and (Dataset.FieldByName('BILLING_IMPOUND').AsString = PAYMENT_TYPE_ACH);

          if UsingOffsets then
          begin
            // credit offset account for billing
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := BillingAmount;
            Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
            Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
            Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
            Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
            Tran.NAME                   := 'BILLING IMPOUND';
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := EffectiveDate;
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('BILLING_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := BILLING_IMPOUND_OFFSET_CREDIT;
            BillingTransactions.AddValue(IntToStr(BillingTransactions.Count), Tran);

            // debit offset account for billing
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := -BillingAmount;
            Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
            Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
            Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
            Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
            Tran.NAME                   := 'BILLING IMPOUND';
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('BILLING_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := BILLING_IMPOUND_OFFSET_DEBIT;
            BillingTransactions.AddValue(IntToStr(BillingTransactions.Count), Tran);

          end;

          // credit SB account for billing
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := BillingAmount;
          Tran.BANK_ACCOUNT_NBR       := BillingBureauAccountNbr;
          Tran.BANK_ACCOUNT_NUMBER    := BillingBureauAccountNumber;
          Tran.ABA_NUMBER             := BillingBureauAccountAba;
          Tran.BANK_ACCOUNT_TYPE      := BillingBureauAccountType;
          Tran.NAME                   := 'BILLING IMPOUND';
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          if UsingOffsets then
            Tran.EFFECTIVE_DATE       := GetNearestWorkingDate(CHECK_DATE)
          else
            Tran.EFFECTIVE_DATE       := EffectiveDate;  
          Tran.IMPOUND_TYPE           := Dataset.FieldByName('BILLING_IMPOUND').AsString;
          Tran.TRANSACTION_TYPE       := BILLING_IMPOUND_CREDIT_BUREAU;
          BillingTransactions.AddValue(IntToStr(BillingTransactions.Count), Tran);

        end;
      end;

      if ((CO.FieldByName('TRUST_SERVICE').AsString <> TRUST_SERVICE_NO) or (CO.FieldByName('OBC').AsString = TRUST_SERVICE_ALL))
      and (Dataset.FieldByName('TRUST_IMPOUND').AsString <> PAYMENT_TYPE_NONE) then
      begin

        if CO.FieldByName('TRUST_SERVICE').AsString = TRUST_SERVICE_ALL then
        begin
          Q := TevQuery.Create('SELECT SUM(NET_WAGES) NET_WAGES '+
                               'FROM PR_CHECK WHERE {AsOfNow<PR_CHECK>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
                               ' AND CHECK_TYPE='''+CHECK_TYPE2_REGULAR+'''');

          TrustAmount := Q.Result.FieldByName('NET_WAGES').AsCurrency;
        end
        else
          TrustAmount := 0;

        if not SbData.Value['BlockNegativeTrust'] and (CO.FieldByName('TRUST_SERVICE').AsString <> TRUST_SERVICE_AGENCY) then
        begin

          Q := TevQuery.Create(
            ' SELECT SUM(p.NET_WAGES) NET_WAGES '+
            ' FROM PR_CHECK p, '+
            '      PR_CHECK c '+
            ' WHERE p.PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
            ' AND {AsOfNow<c>}'+
            ' AND {AsOfNow<p>}'+
            ' AND c.PR_CHECK_NBR=TRIM(STRCOPY(p.FILLER,0,8))'+
            ' AND p.CHECK_TYPE='''+CHECK_TYPE2_VOID+''''+
            ' AND c.CHECK_TYPE<>'''+CHECK_TYPE2_MANUAL+''''+
            ' AND STRCOPY(p.FILLER,0,8) <> ''''');

          if Q.Result.FieldByName('NET_WAGES').AsCurrency <> 0 then
            TrustAmount := TrustAmount + Q.Result.FieldByName('NET_WAGES').AsCurrency;

          Q := TevQuery.Create(
            ' SELECT SUM(p.NET_WAGES) NET_WAGES ' +
            ' FROM PR_CHECK p, PR r, PR_CHECK m ' +
            ' WHERE ' +
            ' p.PR_NBR='+Dataset.FieldByName('PR_NBR').AsString +
            ' AND {AsOfNow<p>}' +
            ' AND {AsOfNow<r>}' +
            ' AND r.PR_NBR=p.PR_NBR' +
            ' AND {AsOfNow<m>}' +
            ' AND m.PR_NBR=STRCOPY(r.FILLER,0,8) '+
            ' AND m.EE_NBR=p.EE_NBR ' +
            ' AND m.NET_WAGES=-p.NET_WAGES ' +
            ' AND p.CHECK_TYPE='''+CHECK_TYPE2_VOID+''''+
            ' AND STRCOPY(p.FILLER,0,8) = ''''' +
            ' AND STRCOPY(r.FILLER,0,8) <> ''''' +
            ' AND m.CHECK_TYPE<>'''+CHECK_TYPE2_MANUAL+'''');

          if Q.Result.FieldByName('NET_WAGES').AsCurrency <> 0 then
            TrustAmount := TrustAmount + Q.Result.FieldByName('NET_WAGES').AsCurrency;

          Q := TevQuery.Create(
            ' SELECT SUM(p.NET_WAGES) NET_WAGES ' +
            ' FROM PR_CHECK p, PR r ' +
            ' WHERE ' +
            ' p.PR_NBR='+Dataset.FieldByName('PR_NBR').AsString +
            ' AND {AsOfNow<p>}' +
            ' AND {AsOfNow<r>}' +
            ' AND r.PR_NBR=p.PR_NBR ' +
            ' AND p.CHECK_TYPE='''+CHECK_TYPE2_VOID+''''+
            ' AND STRCOPY(p.FILLER,0,8) = ''''' +
            ' AND STRCOPY(r.FILLER,0,8) = ''''');
          if Q.Result.FieldByName('NET_WAGES').AsCurrency <> 0 then
            TrustAmount := TrustAmount + Q.Result.FieldByName('NET_WAGES').AsCurrency;
        end;

        Q := TevQuery.Create(
          ' SELECT' +
          '   SUM(MISCELLANEOUS_CHECK_AMOUNT) MISCELLANEOUS_CHECK_AMOUNT ' +
          ' FROM ' +
          '   PR_MISCELLANEOUS_CHECKS c, ' +
          '   CL_AGENCY a ' +
          ' WHERE ' +
          ' {AsOfNow<c>}' +
          ' AND c.PR_NBR = '+Dataset.FieldByName('PR_NBR').AsString +
          ' AND c.MISCELLANEOUS_CHECK_TYPE = '''+MISC_CHECK_TYPE_AGENCY+''''+
          ' AND {AsOfNow<a>}' +
          ' AND a.CL_AGENCY_NBR=c.CL_AGENCY_NBR ' +
          ' AND a.PAYMENT_TYPE <> '''+PAYMENT_TYPE_DIRECTDEPOSIT+'''');
        if Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency <> 0 then
          TrustAmount := TrustAmount + Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency;

        if not SbData.Value['BlockNegativeTrust'] then
        begin
          Q := TevQuery.Create(
            ' SELECT' +
            '   SUM(MISCELLANEOUS_CHECK_AMOUNT) MISCELLANEOUS_CHECK_AMOUNT ' +
            ' FROM ' +
            '   PR_MISCELLANEOUS_CHECKS c, ' +
            '   CL_AGENCY a ' +
            ' WHERE ' +
            ' {AsOfNow<c>}' +
            ' AND c.PR_NBR = '+Dataset.FieldByName('PR_NBR').AsString +
            ' AND c.MISCELLANEOUS_CHECK_TYPE = '''+MISC_CHECK_TYPE_AGENCY_VOID+''''+
            ' AND {AsOfNow<a>}' +
            ' AND a.CL_AGENCY_NBR=c.CL_AGENCY_NBR ' +
            ' AND a.PAYMENT_TYPE <> '''+PAYMENT_TYPE_DIRECTDEPOSIT+'''');
          if Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency <> 0 then
            TrustAmount := TrustAmount + Q.Result.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency;
        end;

        if TrustAmount <> 0 then
        begin

          if CO.FieldByName('TRUST_SB_ACCOUNT_NBR').IsNull then
            raise EevException.Create('Trust SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          Q := TevQuery.Create('SELECT SB_BANKS_NBR, CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE FROM SB_BANK_ACCOUNTS WHERE {AsOfNow<SB_BANK_ACCOUNTS>} AND SB_BANK_ACCOUNTS_NBR='+
            CO.FieldByName('TRUST_SB_ACCOUNT_NBR').AsString);

          Q.Execute;

          if Q.Result.RecordCount = 0 then
            raise EevException.Create('Trust SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          TrustBureauAccountNbr := CO.FieldByName('TRUST_SB_ACCOUNT_NBR').AsInteger;
          TrustBureauAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
          TrustBureauAccountType   := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
          BankNbr                  := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

          Q := TevQuery.Create('SELECT ABA_NUMBER, ALLOW_HYPHENS FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
          Q.Execute;
          TrustBureauAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

          if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
            TrustBureauAccountNumber := ACHTrim(TrustBureauAccountNumber);

          Q := TevQuery.Create('SELECT CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE, SB_BANKS_NBR FROM CL_BANK_ACCOUNT WHERE {AsOfNow<CL_BANK_ACCOUNT>} AND CL_BANK_ACCOUNT_NBR='+ CO.FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').AsString);
          if Q.Result.RecordCount = 0 then
            raise EInconsistentData.CreateHelp('TRUST/OBC: PAYROLL_CL_BANK_ACCOUNT_NBR is missing for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

          TrustClientAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
          TrustClientAccountType := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
          BankNbr := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

          Q := TevQuery.Create('SELECT ALLOW_HYPHENS, ABA_NUMBER FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
          if Q.Result.RecordCount = 0 then
            raise EInconsistentData.CreateHelp('Problem with client bank account, Company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ', Client Bank Account ' + TrustClientAccountNumber, IDH_InconsistentData);

          TrustClientAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

          if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
            TrustClientAccountNumber := ACHTrim(TrustClientAccountNumber);

          if Dataset.FieldByName('TRUST_IMPOUND').AsString = PAYMENT_TYPE_ACH then
          begin
            if not CO.FieldByName('CO_MAX_AMOUNT_FOR_PAYROLL').IsNull
            and (CO.FieldByName('CO_MAX_AMOUNT_FOR_PAYROLL').AsCurrency > 0.005) then
            begin
              if TrustAmount > CO.FieldByName('CO_MAX_AMOUNT_FOR_PAYROLL').AsCurrency then
              begin
                if not CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').IsNull then
                begin
                  s := Format ('%s %s-%d Trust impound %0.2f exceeded the set limitation of %0.2f',
                    [CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString,
                     DateToStr(CHECK_DATE), RUN_NUMBER,
                     TrustAmount, CO.FieldByName('CO_MAX_AMOUNT_FOR_PAYROLL').AsCurrency]);

                  if (CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_WOC) then
                    s := 'SKIPPED: '+s;

                  RaiseLimitationException(s);
                end;
              end;
            end;
          end;

          if TrustAmount <> 0 then
          begin
            if (CO.FieldByName('TRUST_SERVICE').AsString <> TRUST_SERVICE_NO) or (CO.FieldByName('OBC').AsString = 'Y') then
              EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_DAYS_PRIOR_PR').AsInteger)
            else
              EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_OF_DAYS_PRIOR_DD').AsInteger);

            if CO.FieldByName('TRUST_SERVICE').AsString <> TRUST_SERVICE_NO then
              s := 'PAYROLL TRUST IMPOUND'
            else if CO.FieldByName('OBC').AsString = TRUST_SERVICE_ALL then
              s := 'PROLL TRUST IMP FOR OBC';

            // debit client account for trust
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := - TrustAmount;
            Tran.BANK_ACCOUNT_NBR       := TrustBureauAccountNbr;
            Tran.BANK_ACCOUNT_NUMBER    := TrustClientAccountNumber;
            Tran.ABA_NUMBER             := TrustClientAccountAba;
            Tran.BANK_ACCOUNT_TYPE      := TrustClientAccountType;
            Tran.NAME                   := s;
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := EffectiveDate;
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('TRUST_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := TRUST_IMPOUND_DEBIT_CLIENT;
            TrustTransactions.AddValue(IntToStr(TrustTransactions.Count), Tran);

            UsingOffsets := (IncludeOffsets = OFF_USE_OFFSETS) and not Options.BlockDebits
              and (Dataset.FieldByName('TRUST_IMPOUND').AsString = PAYMENT_TYPE_ACH);

            if UsingOffsets then
            begin
              // credit offset account for trust
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.Amount                 := TrustAmount;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := s;
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := EffectiveDate;
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('TRUST_IMPOUND').AsString;
              Tran.TRANSACTION_TYPE       := TRUST_IMPOUND_OFFSET_CREDIT;
              TrustTransactions.AddValue(IntToStr(TrustTransactions.Count), Tran);

              // debit offset account for trust
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.Amount                 := - TrustAmount;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := s;
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('TRUST_IMPOUND').AsString;
              Tran.TRANSACTION_TYPE       := TRUST_IMPOUND_OFFSET_DEBIT;
              TrustTransactions.AddValue(IntToStr(TrustTransactions.Count), Tran);
            end;

            // credit SB account for Trust
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := TrustAmount;
            Tran.BANK_ACCOUNT_NBR       := TrustBureauAccountNbr;
            Tran.BANK_ACCOUNT_NUMBER    := TrustBureauAccountNumber;
            Tran.ABA_NUMBER             := TrustBureauAccountAba;
            Tran.BANK_ACCOUNT_TYPE      := TrustBureauAccountType;
            Tran.NAME                   := s;
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;

            if UsingOffsets then
              Tran.EFFECTIVE_DATE       := GetNearestWorkingDate(CHECK_DATE)
            else
              Tran.EFFECTIVE_DATE       := EffectiveDate;

            Tran.IMPOUND_TYPE           := Dataset.FieldByName('TRUST_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := TRUST_IMPOUND_CREDIT_BUREAU;
            TrustTransactions.AddValue(IntToStr(TrustTransactions.Count), Tran);

          end;

        end;
      end;

      if (Dataset.FieldByName('TAX_IMPOUND').AsString <> PAYMENT_TYPE_NONE)
      and (CO.FieldByName('TAX_SERVICE').AsString = 'Y') then
      begin

        Q := TevQuery.Create('SELECT AMOUNT, CO_SUI_LIABILITIES_NBR FROM CO_SUI_LIABILITIES WHERE {AsOfNow<CO_SUI_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
          ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          SuiAmount := SuiAmount + Q.Result.FieldByName('AMOUNT').AsCurrency;
          Q.Result.Next;
        end;

        Q := TevQuery.Create('SELECT AMOUNT, CO_LOCAL_TAX_LIABILITIES_NBR FROM CO_LOCAL_TAX_LIABILITIES WHERE {AsOfNow<CO_LOCAL_TAX_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
          ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          LocalTaxAmount := LocalTaxAmount + Q.Result.FieldByName('AMOUNT').AsCurrency;
          Q.Result.Next;
        end;

        Q := TevQuery.Create('SELECT AMOUNT, CO_STATE_TAX_LIABILITIES_NBR FROM CO_STATE_TAX_LIABILITIES WHERE {AsOfNow<CO_STATE_TAX_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
          ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          StateTaxAmount := StateTaxAmount + Q.Result.FieldByName('AMOUNT').AsCurrency;
          Q.Result.Next;
        end;

        TotalTaxes := GetTotalTaxLiabilities;

        if TaxesByDates.ValueExists(DateToStr(CHECK_DATE)) then
          TaxesByDates.Value[DateToStr(CHECK_DATE)] := TaxesByDates.Value[DateToStr(CHECK_DATE)] + TotalTaxes
        else
          TaxesByDates.AddValue(DateToStr(CHECK_DATE), TotalTaxes);

        if TotalTaxes <> 0 then
        begin

          if (Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH)
          and not CO.FieldByName('CO_MAX_AMOUNT_FOR_TAX_IMPOUND').IsNull
          and (CO.FieldByName('CO_MAX_AMOUNT_FOR_TAX_IMPOUND').AsCurrency > 0.005) then
          begin
            if Abs(TotalTaxes) > CO.FieldByName('CO_MAX_AMOUNT_FOR_TAX_IMPOUND').AsCurrency then
            begin
              if not CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').IsNull then
              begin
                s := Format ('%s %s-%d Tax impound %0.2f exceeded the set limitation of %0.2f',
                  [CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString,
                   DateToStr(CHECK_DATE), RUN_NUMBER,
                   TotalTaxes, CO.FieldByName('CO_MAX_AMOUNT_FOR_TAX_IMPOUND').AsCurrency]);

                if (CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_WOC) then
                  s := 'SKIPPED: '+s;

                RaiseLimitationException(s);

              end;
            end;
          end;

          Impounds := TisParamsCollection.Create;

          if not (CompanyData.Value['BlockNegativeLiabilities'] and (TotalTaxes < 0)) then
          begin
            DBDTCount := 0;
            Q := TevQuery.Create('SELECT COUNT(CO_DIVISION_NBR) CO_DIVISION_NBR FROM CO_DIVISION WHERE {AsOfNow<CO_DIVISION>} and TAX_CL_BANK_ACCOUNT_NBR is not NULL');
            DBDTCount := DBDTCount + Q.Result.FieldByName('CO_DIVISION_NBR').AsInteger;
            Q := TevQuery.Create('SELECT COUNT(CO_BRANCH_NBR) CO_BRANCH_NBR FROM CO_BRANCH WHERE {AsOfNow<CO_BRANCH>} and TAX_CL_BANK_ACCOUNT_NBR is not NULL');
            DBDTCount := DBDTCount + Q.Result.FieldByName('CO_BRANCH_NBR').AsInteger;
            Q := TevQuery.Create('SELECT COUNT(CO_DEPARTMENT_NBR) CO_DEPARTMENT_NBR FROM CO_DEPARTMENT WHERE {AsOfNow<CO_DEPARTMENT>} and TAX_CL_BANK_ACCOUNT_NBR is not NULL');
            DBDTCount := DBDTCount + Q.Result.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
            Q := TevQuery.Create('SELECT COUNT(CO_TEAM_NBR) CO_TEAM_NBR FROM CO_TEAM WHERE {AsOfNow<CO_TEAM>} and TAX_CL_BANK_ACCOUNT_NBR is not NULL');
            DBDTCount := DBDTCount + Q.Result.FieldByName('CO_TEAM_NBR').AsInteger;

            if CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').IsNull then
              raise EevException.Create('Tax SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

            Q := TevQuery.Create('SELECT SB_BANKS_NBR, CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE FROM SB_BANK_ACCOUNTS WHERE {AsOfNow<SB_BANK_ACCOUNTS>} AND SB_BANK_ACCOUNTS_NBR='+
              CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').AsString);

            Q.Execute;

            if Q.Result.RecordCount = 0 then
              raise EevException.Create('Tax SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

            TaxBureauAccountNbr := CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').AsInteger;
            TaxBureauAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
            TaxBureauAccountType   := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
            BankNbr                := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

            Q := TevQuery.Create('SELECT ABA_NUMBER, ALLOW_HYPHENS FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
            Q.Execute;
            TaxBureauAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

            if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
              TaxBureauAccountNumber := ACHTrim(TaxBureauAccountNumber);

            if DBDTCount > 0 then
            begin
              TaxDistr := TevClientDataSet.Create(nil);
              try
                TaxDistr.AggregatesActive := True;
                TaxDistr.ProviderName := 'CL_CUSTOM_PROV';
                TaxDistr.FieldDefs.Add('TAX',               ftCurrency);
                TaxDistr.FieldDefs.Add('CO_DIVISION_NBR',   ftInteger);
                TaxDistr.FieldDefs.Add('CO_BRANCH_NBR',     ftInteger);
                TaxDistr.FieldDefs.Add('CO_DEPARTMENT_NBR', ftInteger);
                TaxDistr.FieldDefs.Add('CO_TEAM_NBR',       ftInteger);
                TaxDistr.FieldDefs.Add('PR_CHECK_NBR',      ftInteger);
                TaxDistr.CreateDataSet;
                ctx_PayrollCalculation.GetDistrTaxes(Dataset.FieldByName('PR_NBR').AsInteger, TaxDistr);
                TaxDistr.First;
                if TaxDistr.RecordCount = 0 then
                begin
                  ParamName := '';
                  Account := '';
                  Aba := '';
                  AccountType := '';

                  GetDBDTAccount('', '', '', '', 'TAX', 0, Account, Aba, AccountType, ParamName);

                  ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

                  if Options.BalanceBatches then
                  begin
                    if TotalTaxes < 0.005 then
                      ParamName := ParamName + ';-'
                    else
                      ParamName := ParamName + ';+';
                  end;

                  A := Impounds.ParamsByName(ParamName);
                  if not Assigned(A) then
                  begin
                    A := Impounds.AddParams(ParamName);
                    A.AddValue('AMOUNT', TotalTaxes);
                    A.AddValue('NAME', 'TAX IMPOUND');
                    A.AddValue('BANK_ACCOUNT_NUMBER', Account);
                    A.AddValue('ABA_NUMBER', Aba);
                    A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
                  end
                  else
                    A.Value['AMOUNT'] := A.Value['AMOUNT'] + TotalTaxes;

                end
                else
                while not TaxDistr.Eof do
                begin

                  ParamName := '';
                  Account := '';
                  Aba := '';
                  AccountType := '';

                  GetDBDTAccount(TaxDistr.FieldByName('CO_TEAM_NBR').AsString, TaxDistr.FieldByName('CO_DEPARTMENT_NBR').AsString,
                                 TaxDistr.FieldByName('CO_BRANCH_NBR').AsString, TaxDistr.FieldByName('CO_DIVISION_NBR').AsString,
                                 'TAX', 0, Account, Aba, AccountType, ParamName);

                  ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

                  A := Impounds.ParamsByName(ParamName);
                  if not Assigned(A) then
                  begin
                    A := Impounds.AddParams(ParamName);
                    A.AddValue('AMOUNT', TaxDistr.FieldByName('TAX').AsFloat);
                    A.AddValue('NAME', 'TAX IMPOUND');
                    A.AddValue('BANK_ACCOUNT_NUMBER', Account);
                    A.AddValue('ABA_NUMBER', Aba);
                    A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
                  end
                  else
                    A.Value['AMOUNT'] := A.Value['AMOUNT'] + TaxDistr.FieldByName('TAX').AsFloat;

                  TaxDistr.Next;
                end;
                f := 0;
                TaxSum := 0;
                for i := 0 to Impounds.Count - 1 do
                begin
                  A := Impounds.Params[i];
                  TaxSum := TaxSum + A.Value['AMOUNT'];
                  f := f + RoundAll(A.Value['AMOUNT'], 2);
                  A.Value['AMOUNT'] := RoundAll(A.Value['AMOUNT'], 2);
                  if i = (Impounds.Count - 1) then
                  begin
                    if TotalTaxes <> f then
                      A.Value['AMOUNT'] := A.Value['AMOUNT'] + (TotalTaxes - f);
                  end;
                end;
              finally
                TaxDistr.Free;
              end;
            end
            else
            begin

              ParamName := '';
              Account := '';
              Aba := '';
              AccountType := '';

              GetDBDTAccount('NULL', 'NULL', 'NULL', 'NULL', 'TAX', 0, Account, Aba, AccountType, ParamName);

              ParamName := ParamName +';'+Account+';'+Aba+';'+AccountType+';N';

              A := Impounds.AddParams(ParamName);
              A.AddValue('AMOUNT', TotalTaxes);
              A.AddValue('NAME', 'TAX IMPOUND');
              A.AddValue('BANK_ACCOUNT_NUMBER', Account);
              A.AddValue('ABA_NUMBER', Aba);
              A.AddValue('BANK_ACCOUNT_TYPE', AccountType);
            end;

            EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_OF_DAYS_PRIOR_TAX').AsInteger);

            for i := 0 to Impounds.Count - 1 do
            begin
              Item := IInterface(Impounds[i]) as IisListOfValues;
              if Abs(Item.Value['AMOUNT']) > 0.005 then
              begin
                // debit client account for tax
                Tran := TevTransaction.Create;
                Tran.PayrollType            := PayrollType;
                Tran.Amount                 := -Item.Value['AMOUNT'];
                Tran.BANK_ACCOUNT_NBR       := TaxBureauAccountNbr;
                Tran.BANK_ACCOUNT_NUMBER    := Item.Value['BANK_ACCOUNT_NUMBER'];
                Tran.ABA_NUMBER             := Item.Value['ABA_NUMBER'];
                Tran.BANK_ACCOUNT_TYPE      := Item.Value['BANK_ACCOUNT_TYPE'];
                Tran.NAME                   := 'TAX IMPOUND';
                Tran.PR_NBR                 := PR_NBR;
                Tran.CL_NBR                 := CL_NBR;
                Tran.CHECK_DATE             := CHECK_DATE;
                Tran.RUN_NUMBER             := RUN_NUMBER;
                Tran.EFFECTIVE_DATE         := EffectiveDate;
                Tran.IMPOUND_TYPE           := Dataset.FieldByName('TAX_IMPOUND').AsString;
                Tran.TRANSACTION_TYPE       := TAX_IMPOUND_DEBIT_CLIENT;
                TaxTransactions.AddValue(IntToStr(TaxTransactions.Count), Tran);
              end;
            end;

            UsingOffsets := (IncludeOffsets = OFF_USE_OFFSETS) and not Options.BlockDebits
              and (Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH);

            if UsingOffsets then
            begin
              // credit offset account for tax
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.Amount                 := TotalTaxes;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := 'TAX IMPOUND';
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := EffectiveDate;
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('TAX_IMPOUND').AsString;
              Tran.TRANSACTION_TYPE       := TAX_IMPOUND_OFFSET_CREDIT;
              TaxTransactions.AddValue(IntToStr(TaxTransactions.Count), Tran);
              // debit offset account for tax
              Tran := TevTransaction.Create;
              Tran.PayrollType            := PayrollType;
              Tran.Amount                 := -TotalTaxes;
              Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
              Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
              Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
              Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
              Tran.NAME                   := 'TAX IMPOUND';
              Tran.PR_NBR                 := PR_NBR;
              Tran.CL_NBR                 := CL_NBR;
              Tran.CHECK_DATE             := CHECK_DATE;
              Tran.RUN_NUMBER             := RUN_NUMBER;
              Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
              Tran.IMPOUND_TYPE           := Dataset.FieldByName('TAX_IMPOUND').AsString;
              Tran.TRANSACTION_TYPE       := TAX_IMPOUND_OFFSET_DEBIT;
              TaxTransactions.AddValue(IntToStr(TaxTransactions.Count), Tran);
            end;

            // credit SB account for tax
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := TotalTaxes;
            Tran.BANK_ACCOUNT_NBR       := TaxBureauAccountNbr;
            Tran.BANK_ACCOUNT_NUMBER    := TaxBureauAccountNumber;
            Tran.ABA_NUMBER             := TaxBureauAccountAba;
            Tran.BANK_ACCOUNT_TYPE      := TaxBureauAccountType;
            Tran.NAME                   := 'TAX IMPOUND';
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            if UsingOffsets then
              Tran.EFFECTIVE_DATE       := GetNearestWorkingDate(CHECK_DATE)
            else
              Tran.EFFECTIVE_DATE       := EffectiveDate;
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('TAX_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := TAX_IMPOUND_CREDIT_BUREAU;
            TaxTransactions.AddValue(IntToStr(TaxTransactions.Count), Tran);

          end;
        end;

        if not CompanyData.Value['ReversalACH']
        and not (CompanyData.Value['BlockNegativeLiabilities'] and (TotalTaxes < 0))
        and (Dataset.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH) 
        and (CO.FieldByName('TAX_SERVICE').AsString = 'Y')  then
        begin
          UpdateFederalTaxLiability;
          UpdateStateTaxLiability;
          UpdateLocalTaxLiability;
          UpdateSuiLiability;
        end;

        if (Dataset.FieldByName('TAX_IMPOUND').AsString <> PAYMENT_TYPE_ACH)
        and (Dataset.FieldByName('TAX_IMPOUND').AsString <> PAYMENT_TYPE_NONE)
        and (CO.FieldByName('TAX_SERVICE').AsString = 'Y')
        and not CompanyData.Value['ReversalACH'] then
        begin
          Q := TevQuery.Create('SELECT count(*), sum(AMOUNT) FROM CO_SUI_LIABILITIES WHERE {AsOfNow<CO_SUI_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
            ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
          if (Q.Result.Fields[0].AsInteger > 0) and (TotalTaxes = 0) then
          begin
            CO_SUI_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
            CO_SUI_LIABILITIES.Filtered := True;
            try
              CO_SUI_LIABILITIES.First;
              while not CO_SUI_LIABILITIES.Eof do
              begin
                CO_SUI_LIABILITIES.Edit;
                if (CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
                or (CO_SUI_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
                begin
                  CO_SUI_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
                  CO_SUI_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                end;
                CO_SUI_LIABILITIES.Post;
                CO_SUI_LIABILITIES.Next;
              end;
            finally
              CO_SUI_LIABILITIES.Filter := '';
              CO_SUI_LIABILITIES.Filtered := False;
            end;
          end;

          Q := TevQuery.Create('SELECT count(*), sum(AMOUNT) FROM CO_LOCAL_TAX_LIABILITIES WHERE {AsOfNow<CO_LOCAL_TAX_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
            ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
          if (Q.Result.Fields[0].AsInteger > 0) and (TotalTaxes = 0) then
          begin
            CO_LOCAL_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
            CO_LOCAL_TAX_LIABILITIES.Filtered := True;
            try
              CO_LOCAL_TAX_LIABILITIES.First;
              while not CO_LOCAL_TAX_LIABILITIES.Eof do
              begin
                CO_LOCAL_TAX_LIABILITIES.Edit;
                if (CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
                or (CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
                begin
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                end;
                CO_LOCAL_TAX_LIABILITIES.Post;
                CO_LOCAL_TAX_LIABILITIES.Next;
              end;
            finally
              CO_LOCAL_TAX_LIABILITIES.Filter := '';
              CO_LOCAL_TAX_LIABILITIES.Filtered := False;
            end;
          end;

          Q := TevQuery.Create('SELECT count(*), sum(AMOUNT) FROM CO_STATE_TAX_LIABILITIES WHERE {AsOfNow<CO_STATE_TAX_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
            ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
          if (Q.Result.Fields[0].AsInteger > 0) and (TotalTaxes = 0) then
          begin
            CO_STATE_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
            CO_STATE_TAX_LIABILITIES.Filtered := True;
            try
              CO_STATE_TAX_LIABILITIES.First;
              while not CO_STATE_TAX_LIABILITIES.Eof do
              begin
                CO_STATE_TAX_LIABILITIES.Edit;
                if (CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
                or (CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
                begin
                  CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
                  CO_STATE_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                end;
                CO_STATE_TAX_LIABILITIES.Post;
                CO_STATE_TAX_LIABILITIES.Next;
              end;
            finally
              CO_STATE_TAX_LIABILITIES.Filter := '';
              CO_STATE_TAX_LIABILITIES.Filtered := False;
            end;
          end;

          Q := TevQuery.Create('SELECT count(*), sum(AMOUNT) FROM CO_FED_TAX_LIABILITIES WHERE {AsOfNow<CO_FED_TAX_LIABILITIES>} AND PR_NBR='+Dataset.FieldByName('PR_NBR').AsString+
            ' AND (' + ReadLiabilityStatusCode(CompanyData.Value['StatusFromIndex']) + ') AND THIRD_PARTY <> ''' + GROUP_BOX_YES + '''');
          if (Q.Result.Fields[0].AsInteger > 0) and (TotalTaxes = 0) then
          begin
            CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR = ' + IntToStr(PR_NBR);
            CO_FED_TAX_LIABILITIES.Filtered := True;
            try
              CO_FED_TAX_LIABILITIES.First;
              while not CO_FED_TAX_LIABILITIES.Eof do
              begin
                CO_FED_TAX_LIABILITIES.Edit;
                if (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS)
                or (CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) then
                begin
                  CO_FED_TAX_LIABILITIES.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
                  CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                end;
                CO_FED_TAX_LIABILITIES.Post;
                CO_FED_TAX_LIABILITIES.Next;
              end;
            finally
              CO_FED_TAX_LIABILITIES.Filter := '';
              CO_FED_TAX_LIABILITIES.Filtered := False;
            end;
          end;

        end;

      end;


      if (Dataset.FieldByName('WC_IMPOUND').AsString <> PAYMENT_TYPE_NONE)
      then
      begin

        WorkersCompAmount := ctx_PayrollCalculation.CalcWorkersComp(Dataset.FieldByName('PR_NBR').AsInteger);
        if WorkersCompAmount <> 0 then
        begin

          if CO.FieldByName('W_COMP_SB_BANK_ACCOUNT_NBR').IsNull then
            raise EevException.Create('WC SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          Q := TevQuery.Create('SELECT SB_BANKS_NBR, CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE FROM SB_BANK_ACCOUNTS WHERE {AsOfNow<SB_BANK_ACCOUNTS>} AND SB_BANK_ACCOUNTS_NBR='+
            CO.FieldByName('W_COMP_SB_BANK_ACCOUNT_NBR').AsString);

          Q.Execute;

          if Q.Result.RecordCount = 0 then
            raise EevException.Create('WC SB account setup is incorrect for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);

          WcBureauAccountNbr := CO.FieldByName('W_COMP_SB_BANK_ACCOUNT_NBR').AsInteger;
          WcBureauAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
          WcBureauAccountType   := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
          BankNbr               := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

          Q := TevQuery.Create('SELECT ABA_NUMBER, ALLOW_HYPHENS FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
          Q.Execute;
          WcBureauAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

          if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
            WcBureauAccountNumber := ACHTrim(WcBureauAccountNumber);

          // client WC account

          if CO.FieldByName('W_COMP_CL_BANK_ACCOUNT_NBR').IsNull then
            raise EInconsistentData.CreateHelp('W_COMP_CL_BANK_ACCOUNT_NBR is missing for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

          Q := TevQuery.Create('SELECT CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE, SB_BANKS_NBR FROM CL_BANK_ACCOUNT WHERE {AsOfNow<CL_BANK_ACCOUNT>} AND CL_BANK_ACCOUNT_NBR='+ CO.FieldByName('W_COMP_CL_BANK_ACCOUNT_NBR').AsString);
          if Q.Result.RecordCount = 0 then
            raise EInconsistentData.CreateHelp('W_COMP_CL_BANK_ACCOUNT_NBR is missing for company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, IDH_InconsistentData);

          WcClientAccountNumber := Q.Result.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString;
          WcClientAccountType   := Q.Result.FieldByName('BANK_ACCOUNT_TYPE').AsString;
          BankNbr               := Q.Result.FieldByName('SB_BANKS_NBR').AsInteger;

          Q := TevQuery.Create('SELECT ALLOW_HYPHENS, ABA_NUMBER FROM SB_BANKS WHERE {AsOfNow<SB_BANKS>} AND SB_BANKS_NBR='+IntToStr(BankNbr));
          if Q.Result.RecordCount = 0 then
            raise EInconsistentData.CreateHelp('Problem with client bank account, Company ' + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ', Client Bank Account ' + WcClientAccountNumber, IDH_InconsistentData);

          WcClientAccountAba := Q.Result.FieldByName('ABA_NUMBER').AsString;

          if Q.Result.FieldByName('ALLOW_HYPHENS').AsString <> GROUP_BOX_YES then
            WcClientAccountNumber := ACHTrim(WcClientAccountNumber);

          EffectiveDate := CalculateACHEffectiveDate(CHECK_DATE, CO.FieldByName('DEBIT_NUMBER_DAYS_PRIOR_WC').AsInteger);

          // debit client account for WC
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := - WorkersCompAmount;
          Tran.BANK_ACCOUNT_NBR       := WcBureauAccountNbr;
          Tran.BANK_ACCOUNT_NUMBER    := WcClientAccountNumber;
          Tran.ABA_NUMBER             := WcClientAccountAba;
          Tran.BANK_ACCOUNT_TYPE      := WcClientAccountType;
          Tran.NAME                   := 'WORKERS COMP IMPOUND';
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          Tran.EFFECTIVE_DATE         := EffectiveDate;
          Tran.IMPOUND_TYPE           := Dataset.FieldByName('WC_IMPOUND').AsString;
          Tran.TRANSACTION_TYPE       := WC_IMPOUND_DEBIT_CLIENT;
          WcTransactions.AddValue(IntToStr(WcTransactions.Count), Tran);

          UsingOffsets := (IncludeOffsets = OFF_USE_OFFSETS) and not Options.BlockDebits
            and (Dataset.FieldByName('WC_IMPOUND').AsString = PAYMENT_TYPE_ACH);

          if UsingOffsets then
          begin
            // credit offset account for WC
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := WorkersCompAmount;
            Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
            Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
            Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
            Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
            Tran.NAME                   := 'WORKERS COMP IMPOUND';
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := EffectiveDate;
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('WC_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := WC_IMPOUND_OFFSET_CREDIT;
            WcTransactions.AddValue(IntToStr(WcTransactions.Count), Tran);

            // debit offset account for WC
            Tran := TevTransaction.Create;
            Tran.PayrollType            := PayrollType;
            Tran.Amount                 := - WorkersCompAmount;
            Tran.BANK_ACCOUNT_NBR       := SbData.Value['SbBankAccountNbr'];
            Tran.BANK_ACCOUNT_NUMBER    := SbData.Value['SbOffsetBankAccount'];
            Tran.ABA_NUMBER             := SbData.Value['SbOffsetBankAba'];
            Tran.BANK_ACCOUNT_TYPE      := SbData.Value['SbOffsetAccountType'];
            Tran.NAME                   := 'WORKERS COMP IMPOUND';
            Tran.PR_NBR                 := PR_NBR;
            Tran.CL_NBR                 := CL_NBR;
            Tran.CHECK_DATE             := CHECK_DATE;
            Tran.RUN_NUMBER             := RUN_NUMBER;
            Tran.EFFECTIVE_DATE         := GetNearestWorkingDate(CHECK_DATE);
            Tran.IMPOUND_TYPE           := Dataset.FieldByName('WC_IMPOUND').AsString;
            Tran.TRANSACTION_TYPE       := WC_IMPOUND_OFFSET_DEBIT;
            WcTransactions.AddValue(IntToStr(WcTransactions.Count), Tran);
          end;

          // credit SB account for WC
          Tran := TevTransaction.Create;
          Tran.PayrollType            := PayrollType;
          Tran.Amount                 := WorkersCompAmount;
          Tran.BANK_ACCOUNT_NBR       := WcBureauAccountNbr;
          Tran.BANK_ACCOUNT_NUMBER    := WcBureauAccountNumber;
          Tran.ABA_NUMBER             := WcBureauAccountAba;
          Tran.BANK_ACCOUNT_TYPE      := WcBureauAccountType;
          Tran.NAME                   := 'WORKERS COMP IMPOUND';
          Tran.PR_NBR                 := PR_NBR;
          Tran.CL_NBR                 := CL_NBR;
          Tran.CHECK_DATE             := CHECK_DATE;
          Tran.RUN_NUMBER             := RUN_NUMBER;
          if UsingOffsets then
            Tran.EFFECTIVE_DATE       := GetNearestWorkingDate(CHECK_DATE)
          else
            Tran.EFFECTIVE_DATE       := EffectiveDate;
          Tran.IMPOUND_TYPE           := Dataset.FieldByName('WC_IMPOUND').AsString;
          Tran.TRANSACTION_TYPE       := WC_IMPOUND_CREDIT_BUREAU;
          WcTransactions.AddValue(IntToStr(WcTransactions.Count), Tran);

        end;
      end;

      for i := 0 to DDTransactions.Count - 1 do
        Transactions.AddValue(IntToStr(Transactions.Count), DDTransactions[i].Value);
      for i := 0 to BillingTransactions.Count - 1 do
        Transactions.AddValue(IntToStr(Transactions.Count), BillingTransactions[i].Value);
      for i := 0 to TrustTransactions.Count - 1 do
        Transactions.AddValue(IntToStr(Transactions.Count), TrustTransactions[i].Value);
      for i := 0 to TaxTransactions.Count - 1 do
        Transactions.AddValue(IntToStr(Transactions.Count), TaxTransactions[i].Value);
      for i := 0 to WcTransactions.Count - 1 do
        Transactions.AddValue(IntToStr(Transactions.Count), wcTransactions[i].Value);

      deltas_CO_STATE_TAX_LIABILITIES.AddValue(IntToStr(PR_NBR), CO_STATE_TAX_LIABILITIES.GetChangePacket);
      deltas_CO_LOCAL_TAX_LIABILITIES.AddValue(IntToStr(PR_NBR), CO_LOCAL_TAX_LIABILITIES.GetChangePacket);
      deltas_CO_SUI_LIABILITIES.AddValue(IntToStr(PR_NBR), CO_SUI_LIABILITIES.GetChangePacket);
      deltas_CO_FED_TAX_LIABILITIES.AddValue(IntToStr(PR_NBR), CO_FED_TAX_LIABILITIES.GetChangePacket);

      if CompanyExceptions <> '' then
        raise EevException.Create(CompanyExceptions);

    except
      on E: Exception do
      begin
        if Length(Exceptions) > 0 then
          Exceptions := Exceptions + #13#10;
        Exceptions := Exceptions + E.Message;

        if CO.FieldByName('CO_ACH_PROCESS_LIMITATIONS').AsString = CO_ACH_PROCESS_LIMITATIONS_STOP then
        begin
          Stopped := True;
          Transactions.Clear;
          deltas_CO_STATE_TAX_LIABILITIES.Clear;
          deltas_CO_LOCAL_TAX_LIABILITIES.Clear;
          deltas_CO_SUI_LIABILITIES.Clear;
          deltas_CO_FED_TAX_LIABILITIES.Clear;
          CO_STATE_TAX_LIABILITIES.Data := idata_CO_STATE_TAX_LIABILITIES;
          CO_LOCAL_TAX_LIABILITIES.Data := idata_CO_LOCAL_TAX_LIABILITIES;
          CO_SUI_LIABILITIES.Data       := idata_CO_SUI_LIABILITIES;
          CO_FED_TAX_LIABILITIES.Data   := idata_CO_FED_TAX_LIABILITIES;
          break;
        end
        else
        begin
          CO_STATE_TAX_LIABILITIES.Data := data_CO_STATE_TAX_LIABILITIES;
          CO_LOCAL_TAX_LIABILITIES.Data := data_CO_LOCAL_TAX_LIABILITIES;
          CO_SUI_LIABILITIES.Data       := data_CO_SUI_LIABILITIES;
          CO_FED_TAX_LIABILITIES.Data   := data_CO_FED_TAX_LIABILITIES;
        end;
      end;
    end;

    Dataset.Next;
  end;

  if not Stopped then
  begin
    for j := 0 to TaxesByDates.Count - 1 do
    begin
      if (Abs(TaxesByDates.Values[j].Value) > 0.009) and
      (CO.FieldByName('TAX_SERVICE').AsString = 'Y') and
      (Abs(Options.CombineThreshold) > 0.009) and
      (Abs(TaxesByDates.Values[j].Value) < Options.CombineThreshold) then
      begin
        CompanyData.AddValue('Warnings',
          Format('#%s %s : The total tax liability $%0.2f is below threshold of $%0.2f',
            [CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString,
             TaxesByDates.Values[j].Name,
             Abs(TaxesByDates.Values[j].Value),
             Options.CombineThreshold]));

        i := 0;
        while i < Transactions.Count do
        begin
          Tran := IInterface(Transactions.Values[i].Value) as IevTransaction;
          if DateToStr(Tran.CHECK_DATE) = TaxesByDates.Values[j].Name then
          begin
            if Tran.TRANSACTION_TYPE = TAX_IMPOUND_DEBIT_CLIENT then
            begin
              Dataset.Locate('PR_NBR', Tran.PR_NBR, []);
              if Assigned(WriteOff.ParamsByName(IntToStr(Tran.PR_NBR))) then
              begin
                WriteOffItem := WriteOff.ParamsByName(IntToStr(Tran.PR_NBR));
                WriteOffItem.Value['Amount'] := WriteOffItem.Value['Amount'] + Tran.Amount;
              end
              else
              begin
                WriteOffItem := WriteOff.AddParams(IntToStr(Tran.PR_NBR));
                WriteOffItem.AddValue('Amount', Tran.Amount);
                WriteOffItem.AddValue('SB_BANK_ACCOUNTS_NBR', Tran.BANK_ACCOUNT_NBR);
                WriteOffItem.AddValue('EffectiveDate', Tran.EFFECTIVE_DATE);
                WriteOffItem.AddValue('CHECK_DATE', Tran.CHECK_DATE);
                WriteOffItem.AddValue('CO_PR_ACH_NBR', Dataset.FieldByName('CO_PR_ACH_NBR').AsInteger);
              end;
            end;
          end;
          if Tran.TRANSACTION_TYPE in [TAX_IMPOUND_DEBIT_CLIENT, TAX_IMPOUND_OFFSET_CREDIT, TAX_IMPOUND_OFFSET_DEBIT, TAX_IMPOUND_CREDIT_BUREAU] then
            Transactions.Delete(i)
          else
            i := i + 1;
        end;

        if not CompanyData.Value['ReversalACH'] then
        begin
          for i := 0 to deltas_CO_STATE_TAX_LIABILITIES.Count - 1 do
          begin
            if Dataset.Locate('PR_NBR', deltas_CO_STATE_TAX_LIABILITIES.Values[i].Name, []) then
            begin
              if Dataset.FieldByName('CHECK_DATE').AsString = TaxesByDates.Values[j].Name then
              begin
                CO_STATE_TAX_LIABILITIES.Filtered := False;
                CO_STATE_TAX_LIABILITIES.Filter := 'PR_NBR='+deltas_CO_STATE_TAX_LIABILITIES.Values[i].Name;
                CO_STATE_TAX_LIABILITIES.Filtered := True;
                CO_STATE_TAX_LIABILITIES.First;
                while not CO_STATE_TAX_LIABILITIES.Eof do
                begin
                  if CO_STATE_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString = GROUP_BOX_YES then
                  begin
                    CO_STATE_TAX_LIABILITIES.Edit;
                    CO_STATE_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                    CO_STATE_TAX_LIABILITIES.Post;
                  end;
                  CO_STATE_TAX_LIABILITIES.Next;
                end;
                deltas_CO_STATE_TAX_LIABILITIES.Values[i].Value := CO_STATE_TAX_LIABILITIES.GetChangePacket;
                CO_STATE_TAX_LIABILITIES.Filtered := False;
              end;
            end;
          end;

          for i := 0 to deltas_CO_LOCAL_TAX_LIABILITIES.Count - 1 do
          begin
            if Dataset.Locate('PR_NBR', deltas_CO_LOCAL_TAX_LIABILITIES.Values[i].Name, []) then
            begin
              if Dataset.FieldByName('CHECK_DATE').AsString = TaxesByDates.Values[j].Name then
              begin
                CO_LOCAL_TAX_LIABILITIES.Filtered := False;
                CO_LOCAL_TAX_LIABILITIES.Filter := 'PR_NBR='+deltas_CO_LOCAL_TAX_LIABILITIES.Values[i].Name;
                CO_LOCAL_TAX_LIABILITIES.Filtered := True;
                CO_LOCAL_TAX_LIABILITIES.First;
                while not CO_LOCAL_TAX_LIABILITIES.Eof do
                begin
                  if CO_LOCAL_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString = GROUP_BOX_YES then
                  begin
                    CO_LOCAL_TAX_LIABILITIES.Edit;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                    CO_LOCAL_TAX_LIABILITIES.Post;
                  end;
                  CO_LOCAL_TAX_LIABILITIES.Next;
                end;
                deltas_CO_LOCAL_TAX_LIABILITIES.Values[i].Value := CO_LOCAL_TAX_LIABILITIES.GetChangePacket;
                CO_LOCAL_TAX_LIABILITIES.Filtered := False;
              end;
            end;
          end;

          for i := 0 to deltas_CO_FED_TAX_LIABILITIES.Count - 1 do
          begin
            if Dataset.Locate('PR_NBR', deltas_CO_FED_TAX_LIABILITIES.Values[i].Name, []) then
            begin
              if Dataset.FieldByName('CHECK_DATE').AsString = TaxesByDates.Values[j].Name then
              begin
                CO_FED_TAX_LIABILITIES.Filtered := False;
                CO_FED_TAX_LIABILITIES.Filter := 'PR_NBR='+deltas_CO_FED_TAX_LIABILITIES.Values[i].Name;
                CO_FED_TAX_LIABILITIES.Filtered := True;
                CO_FED_TAX_LIABILITIES.First;
                while not CO_FED_TAX_LIABILITIES.Eof do
                begin
                  if CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString = GROUP_BOX_YES then
                  begin
                    CO_FED_TAX_LIABILITIES.Edit;
                    CO_FED_TAX_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                    CO_FED_TAX_LIABILITIES.Post;
                  end;
                  CO_FED_TAX_LIABILITIES.Next;
                end;
                deltas_CO_FED_TAX_LIABILITIES.Values[i].Value := CO_FED_TAX_LIABILITIES.GetChangePacket;
                CO_FED_TAX_LIABILITIES.Filtered := False;
              end;
            end;
          end;

          for i := 0 to deltas_CO_SUI_LIABILITIES.Count - 1 do
          begin
            if Dataset.Locate('PR_NBR', deltas_CO_SUI_LIABILITIES.Values[i].Name, []) then
            begin
              if Dataset.FieldByName('CHECK_DATE').AsString = TaxesByDates.Values[j].Name then
              begin
                CO_SUI_LIABILITIES.Filtered := False;
                CO_SUI_LIABILITIES.Filter := 'PR_NBR='+deltas_CO_SUI_LIABILITIES.Values[i].Name;
                CO_SUI_LIABILITIES.Filtered := True;
                CO_SUI_LIABILITIES.First;
                while not CO_SUI_LIABILITIES.Eof do
                begin
                  if CO_SUI_LIABILITIES.FieldByName('IMPOUNDED').AsString = GROUP_BOX_YES then
                  begin
                    CO_SUI_LIABILITIES.Edit;
                    CO_SUI_LIABILITIES.FieldByName('IMPOUNDED').AsString := GROUP_BOX_NO;
                    CO_SUI_LIABILITIES.Post;
                  end;
                  CO_SUI_LIABILITIES.Next;
                end;
                deltas_CO_SUI_LIABILITIES.Values[i].Value := CO_SUI_LIABILITIES.GetChangePacket;
                CO_SUI_LIABILITIES.Filtered := False;
              end;
            end;
          end;

        end;
      end;
    end;
  end;

  XMLs := TisListOfValues.Create;
  for i := 0 to Transactions.Count - 1 do
  begin
    Tran := IInterface(Transactions.Values[i].Value) as IevTransaction;
    if XMLs.ValueExists(IntToStr(Tran.PR_NBR)) then
      XML := IInterface(XMLs.Value[IntToStr(Tran.PR_NBR)]) as IisStringList
    else
    begin
      XML := TisStringList.Create;
      XMLs.AddValue(IntToStr(Tran.PR_NBR), XML);
    end;
    XML.Text := XML.Text + Tran.AsXML;
  end;

  if Options.CombineTrans <> Integer(COMBINE_DO_NOT) then
  begin
    TmpTransactions := TisListOfValues.Create;
    for i := 0 to Transactions.Count - 1 do
    begin
      Tran := IInterface(Transactions.Values[i].Value) as IevTransaction;
      if Tran.IsImpound or Tran.IsOffset then
      begin
        s := Format('%d;%s;%s;%s;%s', [Integer(Tran.TRANSACTION_TYPE),
                           Tran.BANK_ACCOUNT_NUMBER,
                           Tran.BANK_ACCOUNT_TYPE,
                           DateToStr(Tran.CHECK_DATE),
                           DateToStr(Tran.EFFECTIVE_DATE)]);

        if (Options.CombineTrans = Integer(COMBINE_CO_WP_DD)) and
          Tran.IsReversal and
          (Tran.TRANSACTION_TYPE in [
           DIRECT_DEPOSIT_IMPOUND,
           CHILD_SUPPORT_IMPOUND,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT]) then
          s := s + ';'+Format('%10.10d', [Random(9999999999)])
        else
        if (Options.CombineTrans = Integer(COMBINE_CO_NO_DD)) and
          (Tran.TRANSACTION_TYPE in [
           DIRECT_DEPOSIT_IMPOUND,
           CHILD_SUPPORT_IMPOUND,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_CREDIT,
           DIRECT_DEPOSIT_IMPOUND_OFFSET_DEBIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_CREDIT,
           CHILD_SUPPORT_IMPOUND_OFFSET_DEBIT]) then
          s := s + ';'+Format('%10.10d', [Random(9999999999)]);

        if TmpTransactions.ValueExists(s) then
        begin
          TmpTran := IInterface(TmpTransactions.Value[s]) as IevTransaction;
          TmpTran.Amount := TmpTran.Amount + Tran.Amount;
        end
        else
          TmpTransactions.AddValue(s, Tran);
      end
      else
      begin
        s := GetUniqueID;
        TmpTransactions.AddValue(s, Tran);
      end;
    end;
    Transactions.Clear;
    for i := 0 to TmpTransactions.Count - 1 do
    begin
      Tran := IInterface(TmpTransactions.Values[i].Value) as IevTransaction;
      s := TmpTransactions.Values[i].Name;
      Transactions.AddValue(s, Tran);
    end;
  end;

  CompanyData.AddValue('Transactions', Transactions.AsStream);
  CompanyData.AddValue('XMLs', XMLs);

  CompanyData.AddValue('CO_STATE_TAX_LIABILITIES', deltas_CO_STATE_TAX_LIABILITIES.AsStream);
  CompanyData.AddValue('CO_LOCAL_TAX_LIABILITIES', deltas_CO_LOCAL_TAX_LIABILITIES.AsStream);
  CompanyData.AddValue('CO_SUI_LIABILITIES',  deltas_CO_SUI_LIABILITIES.AsStream);
  CompanyData.AddValue('CO_FED_TAX_LIABILITIES', deltas_CO_FED_TAX_LIABILITIES.AsStream);

  CompanyData.AddValue('XMLs', XMLs);

  if Warnings <> '' then
    CompanyData.AddValue('Warnings', Warnings);
  if Exceptions <> '' then
    CompanyData.AddValue('Exceptions', Exceptions);

  CompanyData.AddValue('WriteOff', WriteOff);

end;

procedure TevCashManagement.FinalizeCashManagement(
  const Companies: IisListOfValues; const Options: IevACHOptions;
  const SbData: IisListOfValues; const IncludeOffsets: Integer;
  const Preprocess: Boolean; var AchFileReport: IisStream;
  var AchRegularReport: IisStream; var AchDetailedReport: IisStream;
  var NonAchReport: IisStream; var WTFile: IisStream; var FileName: String;
  var AchSave: IisListOfValues; var Exceptions: String; var Warnings: String;
  const FromDate, ToDate: TDateTime; const ACHFolder: String);
var
  TempString, Comment, Descr, Exceeded: String;
  CompanyData: IisListOfValues;
  Transaction: IevTransaction;
  ACHDataset: TevAchDataSet;
  i, j, k: Integer;
  AchFile, AchTransactions, NonAchTransactions: IevAchFile;
  Transactions, Entries, Entry: IisListOfValues;
  WTReportNeeded: Boolean;
  NonAchOptions: IevACHOptions;
  AllTrans: IisListOfValues;
  UnbalancedBatches, AllBatches, Batch: IisListOfValues;
  Amount, TotalAchFileDebit, TotalAchFileCredit: Currency;
  n: Integer;
  BlockSbCredit: Boolean;
  CO_PR_ACH: IevTable;

  deltas_CO_STATE_TAX_LIABILITIES: IisListOfValues;
  deltas_CO_LOCAL_TAX_LIABILITIES: IisListOfValues;
  deltas_CO_SUI_LIABILITIES: IisListOfValues;
  deltas_CO_FED_TAX_LIABILITIES: IisListOfValues;

  WriteOff: IisParamsCollection;
  WriteOffItem: IisListOfValues;

  Totals: IisListOfValues;
  TotalName: String;

  CalcFields: TDataSetNotifyEvent;
  Q: IevQuery;
  Dataset: IevDataset;
  DataStream: IevDualStream;
  SB_MAX_ACH_FILE_TOTAL: Double;
begin
  Companies.Sort;

  Totals := TisListOfValues.Create;
  BlockSbCredit := Options.SunTrust and Options.BlockSBCredit;
  AllTrans := TisListOfValues.Create;
  TempString := FormatDateTime('YYYYMMDDHHNNSS', Now);
  FileName := TempString + '.ach';

  AchTransactions := TevAchFile.Create(Options, SbData.Value['SbOriginBankAba'],
    SbData.Value['CTSID'], SbData.Value['SbName'], SbData.Value['SbOriginPrintName'],
    SbData.Value['FileHeaderAba'], SbData.Value['SbEinNumber'],
    SbData.Value['SbBankAccountNbr'], False, IncludeOffsets);

  NonAchOptions := TevACHOptions.Create;
  (NonAchOptions as IisInterfacedObject).AsStream := (Options as IisInterfacedObject).AsStream;
  NonAchOptions.CombineTrans := Integer(COMBINE_DO_NOT);
  NonAchOptions.BalanceBatches := False;

  NonAchTransactions:= TevAchFile.Create(NonAchOptions, SbData.Value['SbOriginBankAba'],
    SbData.Value['CTSID'], SbData.Value['SbName'], SbData.Value['SbOriginPrintName'],
    SbData.Value['FileHeaderAba'], SbData.Value['SbEinNumber'],
    SbData.Value['SbBankAccountNbr'], True, OFF_DONT_USE);

  WTReportNeeded := False;

  if SbData.ValueExists('SB_ACH_FILE_LIMITATIONS') then
  begin
    SB_MAX_ACH_FILE_TOTAL := SbData.Value['SB_MAX_ACH_FILE_TOTAL'];
    TotalAchFileDebit := 0;
    TotalAchFileCredit := 0;
    for i := 0 to Companies.Count - 1 do
    begin
      CompanyData := IInterface(Companies.Values[i].Value) as IisListOfValues;
      if not CompanyData.ValueExists('Exceptions') then
      begin
        Transactions := TisListOfValues.Create;
        Transactions.AsStream := IInterface(CompanyData.Value['Transactions']) as IisStream;
        for j := 0 to Transactions.Count - 1 do
        begin
          Transaction := IInterface(Transactions.Values[j].Value) as IevTransaction;
          if (Transaction.IMPOUND_TYPE = PAYMENT_TYPE_ACH)
          and not ((BlockSbCredit and Transaction.BlockedSb) or (Options.BlockDebits and Transaction.BlockedCl))
          then
          begin
            if Transaction.Amount > 0 then
              TotalAchFileCredit := TotalAchFileCredit + Transaction.Amount
            else
              TotalAchFileDebit := TotalAchFileDebit + Abs(Transaction.Amount);
          end;
        end;
      end;
    end;

    if (TotalAchFileCredit > SB_MAX_ACH_FILE_TOTAL) or (Abs(TotalAchFileDebit) > SB_MAX_ACH_FILE_TOTAL) then
    begin

      if SB_MAX_ACH_FILE_TOTAL < TotalAchFileCredit then
        Exceeded := Format('ACH total credit %0.2f has exceeded the set limitation of %0.2f', [TotalAchFileCredit, SB_MAX_ACH_FILE_TOTAL]);
      if SB_MAX_ACH_FILE_TOTAL < TotalAchFileDebit then
        Exceeded := Format('ACH total debit %0.2f has exceeded the set limitation of %0.2f', [TotalAchFileDebit, SB_MAX_ACH_FILE_TOTAL]);

      if SbData.Value['SB_ACH_FILE_LIMITATIONS'] = SB_ACH_FILE_LIMITATIONS_WARN then
      begin
        Warnings := Warnings + Exceeded;
      end
      else
      if SbData.Value['SB_ACH_FILE_LIMITATIONS'] = SB_ACH_FILE_LIMITATIONS_STOP then
      begin
        raise EevException.Create(Exceeded);
      end;
    end;

  end;
  for i := 0 to Companies.Count - 1 do
  begin
    CompanyData := IInterface(Companies.Values[i].Value) as IisListOfValues;

    if CompanyData.ValueExists('Warnings') then
    begin
      if Warnings <> '' then
        Warnings := Warnings + #13#10;
      Warnings := Warnings + CompanyData.Value['Warnings'];
    end;

    if CompanyData.ValueExists('Exceptions') then
    begin
      if Exceptions <> '' then
        Exceptions := Exceptions + #13#10;
      Exceptions := Exceptions + CompanyData.Value['Exceptions'];
    end
    else
    begin

      AchTransactions.CL_NBR                        := CompanyData.Value['CL_NBR'];
      AchTransactions.CO_NBR                        := CompanyData.Value['CO_NBR'];
      AchTransactions.PRINT_CLIENT_NAME             := CompanyData.Value['PRINT_CLIENT_NAME'];
      AchTransactions.BANK_ACCOUNT_REGISTER_NAME    := CompanyData.Value['BANK_ACCOUNT_REGISTER_NAME'];
      AchTransactions.CLIENT_NAME                   := CompanyData.Value['ClientName'];
      AchTransactions.LEGAL_NAME                    := CompanyData.Value['LegalName'];
      AchTransactions.FEIN                          := CompanyData.Value['FEIN'];
      AchTransactions.SB_EIN                        := CompanyData.Value['SB_EIN'];
      AchTransactions.CUSTOM_COMPANY_NUMBER         := CompanyData.Value['CUSTOM_COMPANY_NUMBER'];
      AchTransactions.COMPANY_NAME                  := CompanyData.Value['CompanyName'];
      AchTransactions.WELLS_FARGO_ACH_FLAG          := CompanyData.Value['WELLS_FARGO_ACH_FLAG'];
      AchTransactions.COMPANY_IDENTIFICATION        := CompanyData.Value['CompanyIdentification'];
      AchTransactions.COID                          := CompanyData.Value['CoId'];

      NonAchTransactions.CL_NBR                     := CompanyData.Value['CL_NBR'];
      NonAchTransactions.CO_NBR                     := CompanyData.Value['CO_NBR'];
      NonAchTransactions.PRINT_CLIENT_NAME          := CompanyData.Value['PRINT_CLIENT_NAME'];
      NonAchTransactions.BANK_ACCOUNT_REGISTER_NAME := CompanyData.Value['BANK_ACCOUNT_REGISTER_NAME'];
      NonAchTransactions.CLIENT_NAME                := CompanyData.Value['ClientName'];
      NonAchTransactions.LEGAL_NAME                 := CompanyData.Value['LegalName'];
      NonAchTransactions.FEIN                       := CompanyData.Value['FEIN'];
      NonAchTransactions.SB_EIN                     := CompanyData.Value['SB_EIN'];
      NonAchTransactions.CUSTOM_COMPANY_NUMBER      := CompanyData.Value['CUSTOM_COMPANY_NUMBER'];
      NonAchTransactions.COMPANY_NAME               := CompanyData.Value['CompanyName'];
      NonAchTransactions.WELLS_FARGO_ACH_FLAG       := CompanyData.Value['WELLS_FARGO_ACH_FLAG'];
      NonAchTransactions.COMPANY_IDENTIFICATION     := CompanyData.Value['CompanyIdentification'];
      NonAchTransactions.COID                       := CompanyData.Value['CoId'];

      Transactions := TisListOfValues.Create;
      Transactions.AsStream := IInterface(CompanyData.Value['Transactions']) as IisStream;

      if not Preprocess and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
      begin
        ctx_DataAccess.OpenClient(CompanyData.Value['CL_NBR']);

        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.LookupsEnabled := False;

        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('1=2');

        deltas_CO_STATE_TAX_LIABILITIES := TisListOfValues.Create;
        deltas_CO_LOCAL_TAX_LIABILITIES := TisListOfValues.Create;
        deltas_CO_SUI_LIABILITIES       := TisListOfValues.Create;
        deltas_CO_FED_TAX_LIABILITIES   := TisListOfValues.Create;
      end;

      if not Preprocess and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
        ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try

        if not Preprocess and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
        begin
          deltas_CO_STATE_TAX_LIABILITIES.AsStream := IInterface(CompanyData.Value['CO_STATE_TAX_LIABILITIES']) as IisStream;
          deltas_CO_LOCAL_TAX_LIABILITIES.AsStream := IInterface(CompanyData.Value['CO_LOCAL_TAX_LIABILITIES']) as IisStream;
          deltas_CO_SUI_LIABILITIES.AsStream       := IInterface(CompanyData.Value['CO_SUI_LIABILITIES']) as IisStream;
          deltas_CO_FED_TAX_LIABILITIES.AsStream   := IInterface(CompanyData.Value['CO_FED_TAX_LIABILITIES']) as IisStream;

          Dataset := TevDataSet.Create;
          DataStream := IInterface(CompanyData.Value['Payrolls']) as IevDualStream;
          DataStream.Position := 0;
          Dataset.Data := DataStream;
          Dataset.First;
          while not Dataset.Eof do
          begin
            if Dataset.FieldByName('BILLING_IMPOUND').AsString = PAYMENT_TYPE_ACH then
            begin
              DM_COMPANY.CO_BILLING_HISTORY.DataRequired('PR_NBR = ' + Dataset.FieldByName('PR_NBR').AsString);
              DM_COMPANY.CO_BILLING_HISTORY.First;
              while not DM_COMPANY.CO_BILLING_HISTORY.Eof do
              begin
                DM_COMPANY.CO_BILLING_HISTORY.Edit;
                DM_COMPANY.CO_BILLING_HISTORY.STATUS.AsString := CO_BILLING_STATUS_ACH_SENT;
                DM_COMPANY.CO_BILLING_HISTORY.Post;
                DM_COMPANY.CO_BILLING_HISTORY.Next;
              end;
              ctx_DataAccess.PostDatasets([DM_COMPANY.CO_BILLING_HISTORY]);
            end;
            Dataset.Next;
          end;

          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try

            for j := 0 to deltas_CO_STATE_TAX_LIABILITIES.Count - 1 do
              ctx_DBAccess.ApplyDataChangePacket(IInterface(deltas_CO_STATE_TAX_LIABILITIES.Values[j].Value) as IevDataChangePacket);

            for j := 0 to deltas_CO_LOCAL_TAX_LIABILITIES.Count - 1 do
              ctx_DBAccess.ApplyDataChangePacket(IInterface(deltas_CO_LOCAL_TAX_LIABILITIES.Values[j].Value) as IevDataChangePacket);

            for j := 0 to deltas_CO_SUI_LIABILITIES.Count - 1 do
              ctx_DBAccess.ApplyDataChangePacket(IInterface(deltas_CO_SUI_LIABILITIES.Values[j].Value) as IevDataChangePacket);

            for j := 0 to deltas_CO_FED_TAX_LIABILITIES.Count - 1 do
              ctx_DBAccess.ApplyDataChangePacket(IInterface(deltas_CO_FED_TAX_LIABILITIES.Values[j].Value) as IevDataChangePacket);

          except
            ctx_DataAccess.RollbackNestedTransaction;
          end;
          ctx_DataAccess.CommitNestedTransaction;

        end;

        Totals.Clear;

        for j := 0 to Transactions.Count - 1 do
        begin
          Transaction := IInterface(Transactions.Values[j].Value) as IevTransaction;

          if CompanyData.Value['ReversalACH'] then
            Transaction.Amount := - Transaction.Amount;

          if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_ACH then
          begin
            AchFile := AchTransactions;
            if Transaction.TRANSACTION_TYPE in
           [EMPLOYEE_DIRECT_DEPOSIT,
            CHILD_SUPPORT_PAYMENT,
            AGENCY_DIRECT_DEPOSIT,
            BILLING_IMPOUND_CREDIT_BUREAU,
            TRUST_IMPOUND_CREDIT_BUREAU,
            TAX_IMPOUND_CREDIT_BUREAU,
            WC_IMPOUND_CREDIT_BUREAU] then
            begin
              TotalName := IntToStr(Transaction.CL_NBR)+'_'+IntToStr(Transaction.PR_NBR);
              if Totals.ValueExists(TotalName) then
                Totals.Value[TotalName] := Totals.Value[TotalName] + Transaction.Amount
              else
                Totals.AddValue(TotalName, Transaction.Amount);
            end;
          end
          else
            AchFile := NonAchTransactions;

          if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_WIRE_TRANSFER then
            WTReportNeeded := True;

          if not (
             (BlockSbCredit and Transaction.BlockedSb)
             or
             (Options.BlockDebits and Transaction.BlockedCl)
              )
          then
          begin

            Transaction.Batch := AchFile.AddTransaction(
                Transaction,
                CompanyData.Value['ClientName'],
                CompanyData.Value['LegalName'],
                CompanyData.Value['CUSTOM_COMPANY_NUMBER'],
                CompanyData.Value['CompanyName']);

            if Abs(Transaction.Amount) > 0.005 then
              AllTrans.AddValue(IntToStr(AllTrans.Count), Transaction);

          end;

          if not Preprocess and not CompanyData.Value['ReversalACH'] and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
          begin

            if not (Transaction.IsOffset and (Transaction.IMPOUND_TYPE <> PAYMENT_TYPE_ACH))
            and (Transaction.BankAccRegisterType <> '') then
            begin
              Q := TevQuery.Create('select CO_PR_ACH_NBR from CO_PR_ACH where {AsOfNow<CO_PR_ACH>} and PR_NBR='+IntToStr(Transaction.PR_NBR));

              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Insert;
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.SB_BANK_ACCOUNTS_NBR.Value := Transaction.BANK_ACCOUNT_NBR;
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.Value := Q.Result.FieldByName('CO_PR_ACH_NBR').Value;
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_NBR.Value := CompanyData.Value['CO_NBR'];
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CHECK_DATE.AsDateTime := Transaction.CHECK_DATE;

              if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_ACH then
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.AsString := BANK_REGISTER_STATUS_Outstanding
              else
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.AsString := BANK_REGISTER_STATUS_Pending;

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.TRANSACTION_EFFECTIVE_DATE.AsDateTime := Transaction.EFFECTIVE_DATE;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.AMOUNT.Value := Transaction.Amount;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.REGISTER_TYPE.AsString := Transaction.BankAccRegisterType;

            if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_ACH then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.AsString := BANK_REGISTER_MANUALTYPE_None
            else if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_EXTERNAL_CHECK then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_NonSystemCheck
            else if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_INTERNAL_CHECK then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_SystemCheck
            else if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_WIRE_TRANSFER then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_WireTransfer
            else if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_CASH then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_Other
            else if Transaction.IMPOUND_TYPE = PAYMENT_TYPE_OTHER then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.Value := BANK_REGISTER_MANUALTYPE_Other;

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.PROCESS_DATE.AsDateTime := Date;
            if Assigned(Transaction.Batch) then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Filler.AsString := PutIntoFiller('', Transaction.Batch.Value['BatchUniqID'], 18, 10);
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
          end;
        end;

        end;

        if not Preprocess and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
        begin
          Dataset.First;
          while not Dataset.Eof do
          begin
            CO_PR_ACH := TevTable.Create('CO_PR_ACH', '', 'PR_NBR = ' + Dataset.FieldByName('PR_NBR').AsString, SysDate);
            CO_PR_ACH.First;
            while not CO_PR_ACH.Eof do
            begin
              SaveToFiller(CO_PR_ACH.FieldByName('FILLER'), 'SENT', ExtractFileName(FileName) + ':' + Context.UserAccount.User + ':' + ExtractFileName(FileName));
              CO_PR_ACH.Edit;
              CO_PR_ACH.FieldByName('STATUS').AsString := COMMON_REPORT_STATUS__COMPLETED;
              if Totals.ValueExists(IntToStr(ctx_DataAccess.ClientId)+'_'+CO_PR_ACH.FieldByName('PR_NBR').AsString) then
                CO_PR_ACH.FieldByName('AMOUNT').AsCurrency := Totals.Value[IntToStr(ctx_DataAccess.ClientId)+'_'+CO_PR_ACH.FieldByName('PR_NBR').AsString]
              else
                CO_PR_ACH.FieldByName('AMOUNT').AsCurrency := 0;
              CO_PR_ACH.Post;
              CO_PR_ACH.Next;
            end;
            CO_PR_ACH.SaveChanges;
            Dataset.Next;
          end;
        end;

        if not Preprocess and not CompanyData.Value['ReversalACH'] and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
        begin
          WriteOff := IInterface(CompanyData.Value['WriteOff']) as IisParamsCollection;
          for j := 0 to WriteOff.Count - 1 do
          begin
            WriteOffItem := WriteOff.Params[j];

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Insert;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.SB_BANK_ACCOUNTS_NBR.Value := WriteOffItem.Value['SB_BANK_ACCOUNTS_NBR'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.Value := WriteOffItem.Value['CO_PR_ACH_NBR'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_NBR.Value := CompanyData.Value['CO_NBR'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CHECK_DATE.AsDateTime := WriteOffItem.Value['CHECK_DATE'];

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.AsString := BANK_REGISTER_STATUS_WriteOff;

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.TRANSACTION_EFFECTIVE_DATE.AsDateTime := WriteOffItem.Value['EffectiveDate'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.AMOUNT.Value := - WriteOffItem.Value['Amount'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.REGISTER_TYPE.AsString := BANK_REGISTER_TYPE_TaxImpound;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.MANUAL_TYPE.AsString := BANK_REGISTER_MANUALTYPE_None;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.PROCESS_DATE.AsDateTime := Date;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
          end;
          ctx_DataAccess.PostDatasets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
          ctx_DataAccess.CommitNestedTransaction;
        end;
      except
        on E: Exception do
        begin
          if not Preprocess and (CompanyData.Value['StatusFromIndex'] <> PR_ACH_STATUS_FROM_SENT) then
            ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      end;
    end;
  end;


  if AchTransactions.BatchCount > 0 then
  begin
    ACHDataset := TevAchDataSet.Create(nil);
    try
      AchTransactions.PopulateReportDataset(ACHDataset);
      AchFileReport := GenerateReport(ACHDataset, Preprocess, True, True, FromDate, ToDate, ACHFolder, FileName);
      AchRegularReport := GenerateReport(ACHDataset, Preprocess, False, False, FromDate, ToDate, ACHFolder, FileName);
      AchDetailedReport := GenerateReport(ACHDataset, Preprocess, False, True, FromDate, ToDate, ACHFolder, FileName);
    finally
      ACHDataset.Free;
    end;

    if not Preprocess then
      AchSave := AchTransactions.GetAchFiles(TempString);
  end;

  if NonAchTransactions.BatchCount > 0 then
  begin
    ACHDataset := TevAchDataSet.Create(nil);
    try
      NonAchTransactions.PopulateReportDataset(ACHDataset);

      if not Preprocess then
        NonAchReport := GenerateReport(ACHDataset, True, False, False, FromDate, ToDate, '', 'NON ACH TRANSACTIONS: posted to BAR')
      else
        NonAchReport := GenerateReport(ACHDataset, True, False, False, FromDate, ToDate, '', 'NON ACH TRANSACTIONS: not yet saved');

      if WTReportNeeded then
      begin
        if not Preprocess then
          WTFile := GenerateReport(ACHDataset, True, True, True, FromDate, ToDate, '', 'NON ACH TRANSACTIONS: posted to BAR')
        else
          WTFile := GenerateReport(ACHDataset, True, True, True, FromDate, ToDate, '', 'NON ACH TRANSACTIONS: not yet saved');
      end;

    finally
      ACHDataset.Free;
    end;
  end;

  if Options.BalanceBatches and not AchTransactions.BatchesBalanced then
  begin
    if Warnings <> '' then
      Warnings := Warnings + #13#10#13#10;
    Warnings := Warnings + 'BalanceBatches option selected, but not all batches are balanced.';
    if IsDebugMode then
    begin
      UnbalancedBatches := AchTransactions.UnbalancedBatches;

      for j := 0 to UnbalancedBatches.Count - 1 do
      begin
        Batch := IInterface(UnbalancedBatches.Values[j].Value) as IisListOfValues;

        TempString := Batch.Value['ClassCode']+' ' +Batch.Value['Comments']+ ' ' + DateToStr(Batch.Value['EffectiveDate']);

        Warnings := Warnings + #13#10#13#10 + TempString;
        for i := 0 to AllTrans.Count - 1 do
        begin
          Transaction := IInterface(AllTrans.Values[i].Value) as IevTransaction;
          if Transaction.Batch = Batch then
          begin
            Comment := Transaction.TypeDescr;
            if Transaction.IsReversal then
              Comment := Comment + ' (REVERSAL)';
            TempString := '- ' + PadRight(Comment, ' ', 42) +
              PadRight(Transaction.BANK_ACCOUNT_NUMBER, ' ', 21) +
              PadLeft(Format('%0.2f', [Transaction.Amount]), ' ', 15);
            Warnings := Warnings + #13#10 + TempString;
          end;
        end;
      end;
    end;
  end;

  if IsDebugMode then
  begin
(*    TempString := GetDebugInfo(AchTransactions.AllBatches, AllTrans);
    if TempString <> '' then
    begin
      if Warnings <> '' then
        Warnings := Warnings + #13#10#13#10;
      Warnings := Warnings + TempString;
    end;
*)
  end;

  PatchAchFiles(AchSave, Options);

end;

function TevCashManagement.GetDebugInfo(const AllBatches, AllTrans: IisListOfValues): String;
var
  i, j, k, n: Integer;
  Batch, Entries, Entry: IisListOfValues;
  TempString, Comment: String;
  Transaction: IevTransaction;
  Amount: Currency;
begin
  Result := '';

  if AllBatches.Count > 0 then
  begin
    Result := ' List of transactions in the file:';

    for j := 0 to AllBatches.Count - 1 do
    begin
      Batch := IInterface(AllBatches.Values[j].Value) as IisListOfValues;

      TempString := Batch.Value['ClassCode']+' ' +Batch.Value['Comments']+ ' ' + DateToStr(Batch.Value['EffectiveDate']);

      Result := Result + #13#10#13#10 + TempString;

      Entries := IInterface(Batch.Value['Transactions']) as IisListOfValues;
      for k := 0 to Entries.Count - 1 do
      begin
        Entry := IInterface(Entries.Values[k].Value) as IisListOfvalues;
        n := 0;
        for i := 0 to AllTrans.Count - 1 do
        begin
          Transaction := IInterface(AllTrans.Values[i].Value) as IevTransaction;
          if Transaction.ACH_ENTRY = Entry then
          begin
            n := n + 1;
            Comment := Transaction.TypeDescr;
            if Transaction.IsReversal then
              Comment := Comment + ' (REVERSAL)';
            TempString := ' ' + PadRight(Comment, ' ', 42) +
              PadRight(Transaction.BANK_ACCOUNT_NUMBER, ' ', 21) +
              PadLeft(Format('%0.2f', [Transaction.Amount]), ' ', 15);
            Result := Result + #13#10 + TempString;
          end;
        end;
        if n > 1 then
        begin
          Amount := Entry.Value['AMOUNT'];
          TempString := PadLeft(Format('%0.2f', [Amount]), ' ', 15+64);
          Result := Result + #13#10 + TempString;
        end;
      end;
    end;
  end;
end;

procedure TevCashManagement.PrepareCashManagement(
  const Payrolls: IevDataset;
  const Options: IevAchOptions;
  const SbData: IisListOfValues;
  const Companies: IisParamsCollection);
var
  i: Integer;
  ParamName: String;
  CL_NBR, CO_NBR: Integer;
  CompanyData: IisListOfValues;
  SbUsePrenote: Boolean;
  SbOffsetBankAccount,
  SbOffsetAccountType,
  SbOffsetBankAba,
  SbOriginBankAba,
  FileHeaderAba,
  SbOriginName,
  SbOriginPrintName,
  CustomHeader,
  CTSID,
  SbEinNumber,
  SbName: String;
  SbBankAccountNbr: Integer;
  CompanyPayrolls: TevClientDataset;
  CO: IevTable;  
begin
  Payrolls.First;
  if Payrolls.RecordCount > 0 then
  begin
    DM_SERVICE_BUREAU.SB.LookupsEnabled := False;
    DM_SERVICE_BUREAU.SB.DataRequired('ALL');
    SbUsePrenote := DM_SERVICE_BUREAU.SB.USE_PRENOTE.AsString = GROUP_BOX_YES;
    ctx_DataAccess.OpenClient(Payrolls.FieldByName('CL_NBR').AsInteger);

    CO := TevTable.Create('CO', 'CO_NBR,CO_ACH_PROCESS_LIMITATIONS,DD_CL_BANK_ACCOUNT_NBR,NAME,'+
                                'LEGAL_NAME,WELLS_FARGO_ACH_FLAG,FEIN,CUSTOM_COMPANY_NUMBER,'+
                                'BANK_ACCOUNT_REGISTER_NAME,DEBIT_NUMBER_OF_DAYS_PRIOR_DD,'+
                                'CO_MAX_AMOUNT_FOR_DD,DEBIT_NUMBER_DAYS_PRIOR_BILL,BILLING_LEVEL,'+
                                'BILLING_SB_BANK_ACCOUNT_NBR,TRUST_SERVICE,OBC,TRUST_SB_ACCOUNT_NBR,'+
                                'PAYROLL_CL_BANK_ACCOUNT_NBR,CO_MAX_AMOUNT_FOR_PAYROLL,DEBIT_NUMBER_DAYS_PRIOR_PR,'+
                                'TAX_SERVICE,CO_MAX_AMOUNT_FOR_TAX_IMPOUND,TAX_SB_BANK_ACCOUNT_NBR,'+
                                'DEBIT_NUMBER_OF_DAYS_PRIOR_TAX,W_COMP_SB_BANK_ACCOUNT_NBR,W_COMP_CL_BANK_ACCOUNT_NBR,'+
                                'DEBIT_NUMBER_DAYS_PRIOR_WC,ACH_SB_BANK_ACCOUNT_NBR,BILLING_CL_BANK_ACCOUNT_NBR,'+
                                'TAX_CL_BANK_ACCOUNT_NBR,PAYROLL_CL_BANK_ACCOUNT_NBR,DD_CL_BANK_ACCOUNT_NBR,'+
                                'W_COMP_CL_BANK_ACCOUNT_NBR,MANUAL_CL_BANK_ACCOUNT_NBR',
                                'CO_NBR='+Payrolls.FieldByName('CO_NBR').AsString, SysDate);

    SbBankAccountNbr := CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsInteger;
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.LookupsEnabled := False;
    DM_SERVICE_BUREAU.SB_BANKS.LookupsEnabled := False;
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('SB_BANK_ACCOUNTS_NBR='+CO.FieldByName('ACH_SB_BANK_ACCOUNT_NBR').AsString);
    DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.SB_BANKS_NBR.AsString);
    SbOffsetAccountType := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.BANK_ACCOUNT_TYPE.AsString;

    CustomHeader := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_HEADER_RECORD.AsString;
      
    SbOffsetBankAba := ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString);
    SbOffsetBankAccount := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CUSTOM_BANK_ACCOUNT_NUMBER.AsString;
    if DM_SERVICE_BUREAU.SB_BANKS.ALLOW_HYPHENS.AsString <> GROUP_BOX_YES then
       SbOffsetBankAccount := ACHTrim(SbOffsetBankAccount);
    DM_SERVICE_BUREAU.SB_BANKS.LookupsEnabled := False;   
    DM_SERVICE_BUREAU.SB_BANKS.DataRequired('SB_BANKS_NBR='+DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.ACH_ORIGIN_SB_BANKS_NBR.AsString);
    CTSID := DM_SERVICE_BUREAU.SB_BANKS.FieldByName('CTSID').AsString;
    FileHeaderAba := DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString;
    SbOriginBankAba := copy(ACHTrim(DM_SERVICE_BUREAU.SB_BANKS.ABA_NUMBER.AsString),1,8);
    SbOriginName := DM_SERVICE_BUREAU.SB.SB_NAME.AsString;
    SbOriginPrintName := DM_SERVICE_BUREAU.SB_BANKS.PRINT_NAME.AsString;
    SbEinNumber := PadLeft(DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString, ' ', 10);
    SbName := DM_SERVICE_BUREAU.SB.SB_NAME.AsString;

    // CUSTOM_HEADER_RECORD may be used to override some parameters
    if CustomHeader <> '' then
    begin
      CustomHeader := CustomHeader + '*';
      if pos('*', CustomHeader) > 0 then
      begin
        if pos('*', CustomHeader) <> 1 then
        begin
          FileHeaderAba := copy(CustomHeader, 1, pos('*', CustomHeader)-1);
          FileHeaderAba := copy(PadLeft(FileHeaderAba, ' ', 9), 1, 9);
        end;  
        CustomHeader := copy(CustomHeader, pos('*', CustomHeader)+1, 10000);
      end;
      if pos('*', CustomHeader) > 0 then
      begin
        if pos('*', CustomHeader) <> 1 then
        begin
          SbEinNumber := copy(CustomHeader, 1, pos('*', CustomHeader)-1);
          SbEinNumber := copy(PadLeft(SbEinNumber, ' ', 10), 1, 10);
        end;  
        CustomHeader := copy(CustomHeader, pos('*', CustomHeader)+1, 10000);
      end;
      if pos('*', CustomHeader) > 0 then
      begin
        if pos('*', CustomHeader) <> 1 then
          SbOriginPrintName := copy(CustomHeader, 1, pos('*', CustomHeader)-1);
        CustomHeader := copy(CustomHeader, pos('*', CustomHeader)+1, 10000);
      end;
      if pos('*', CustomHeader) > 0 then
      begin
        if pos('*', CustomHeader) <> 1 then
          SbName := copy(CustomHeader, 1, pos('*', CustomHeader)-1);
      end;
    end;

    if not DM_SERVICE_BUREAU.SB.SB_ACH_FILE_LIMITATIONS.IsNull
         and not (DM_SERVICE_BUREAU.SB.SB_ACH_FILE_LIMITATIONS.AsString = SB_ACH_FILE_LIMITATIONS_NONE)
         and not DM_SERVICE_BUREAU.SB.SB_MAX_ACH_FILE_TOTAL.IsNull
         and (DM_SERVICE_BUREAU.SB.SB_MAX_ACH_FILE_TOTAL.AsCurrency > 0.005) then
    begin
      SbData.AddValue('SB_ACH_FILE_LIMITATIONS', DM_SERVICE_BUREAU.SB.SB_ACH_FILE_LIMITATIONS.AsString);
      SbData.AddValue('SB_MAX_ACH_FILE_TOTAL', DM_SERVICE_BUREAU.SB.SB_MAX_ACH_FILE_TOTAL.AsCurrency);
    end;

    SbData.AddValue('CTSID', CTSID); // used in 5-record when Options.CTS or Options.Intercept is True in 6-record when Options.CTS and Options.SunTrust are true
    SbData.AddValue('FileHeaderAba', FileHeaderAba); // used in 1-record and in report dataset as ABA_NUMBER and ABA_NUMBER2
    SbData.AddValue('SbOffsetBankAba', SbOffsetBankAba); //
    SbData.AddValue('SbOffsetBankAccount', SbOffsetBankAccount); //
    SbData.AddValue('SbOffsetAccountType', SbOffsetAccountType); //
    SbData.AddValue('SbOriginBankAba', SbOriginBankAba);
    SbData.AddValue('SbOriginPrintName', SbOriginPrintName);
    SbData.AddValue('SbEinNumber', SbEinNumber);
    SbData.AddValue('SbName', SbName);
    SbData.AddValue('SbBankAccountNbr', SbBankAccountNbr);
    SbData.AddValue('SB_EIN_NUMBER', PadLeft(DM_SERVICE_BUREAU.SB.EIN_NUMBER.AsString, ' ', 10));

    if DM_SERVICE_BUREAU.SB.IMPOUND_TRUST_MONIES_AS_RECEIV.Value = GROUP_BOX_NO then
      SbData.AddValue('BlockNegativeTrust', True)
    else
      SbData.AddValue('BlockNegativeTrust', False);

    while not Payrolls.Eof do
    begin
      ParamName := Payrolls.FieldByName('CL_NBR').AsString+':'+Payrolls.FieldByName('CO_NBR').AsString;
      CompanyData := Companies.ParamsByName(ParamName);
      if not Assigned(CompanyData) then
      begin
        CompanyData := Companies.AddParams(ParamName);
        CompanyData.AddValue('CL_NBR', Payrolls.FieldByName('CL_NBR').AsString);
        CompanyData.AddValue('CO_NBR', Payrolls.FieldByName('CO_NBR').AsString);
        CompanyData.AddValue('CUSTOM_COMPANY_NUMBER', Payrolls.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
        CompanyData.AddValue('NAME', Payrolls.FieldByName('NAME').AsString);
        CompanyData.AddValue('SbUsePrenote', SbUsePrenote);
      end;
      Payrolls.Next;
    end;
  end;

  CompanyPayrolls := TevClientDataset.Create(nil);
  try
    CompanyPayrolls.Data := Payrolls.Data;
    for i := 0 to Companies.Count - 1 do
    begin
      CL_NBR := Companies[i].Value['CL_NBR'];
      CO_NBR := Companies[i].Value['CO_NBR'];
      CompanyPayrolls.Filter := 'CL_NBR='+IntToStr(CL_NBR)+' AND CO_NBR='+IntToStr(CO_NBR);
      CompanyPayrolls.Filtered := True;
      Companies[i].AddValue('Payrolls', CompanyPayrolls.GetDataStream(True, False).GetClone);
    end;
  finally
    CompanyPayrolls.Free;
  end;
end;

procedure TevCashManagement.UpdateAchStatus(Payrolls: TevClientDataSet; const NewStatusIndex: Integer);
var
  Clients: IisListOfValues;
  i: Integer;
  NewPrAchStatus: String;
  list: TevAchPayrollList;
  CO_PR_ACH: IevTable;
begin
  Clients := TisListOfValues.Create;
  list := TevAchPayrollList(Payrolls);
  list.First;
  while not list.Eof do
  begin
    if not Clients.ValueExists(list.CL_NBR.AsString) then
    begin
      Clients.AddValue(list.CL_NBR.AsString, list.CO_PR_ACH_NBR.AsString);
    end
    else
      Clients.Value[list.CL_NBR.AsString] :=
        Clients.Value[list.CL_NBR.AsString] + ','+list.CO_PR_ACH_NBR.AsString;
    list.Next;
  end;
  for i := 0 to Clients.Count - 1 do
  begin
    ctx_DataAccess.OpenClient(StrToInt(Clients.Values[i].Name));

    CO_PR_ACH := TevTable.Create('CO_PR_ACH', '', 'CO_PR_ACH_NBR IN ('+ Clients.Values[i].Value+')', SysDate);

    CO_PR_ACH.First;
    while not CO_PR_ACH.Eof do
    begin
      NewPrAchStatus := AchPayrollNewStatus(CO_PR_ACH.FieldByName('STATUS').AsString, NewStatusIndex);
      if (  (CO_PR_ACH.FieldByName('DD_IMPOUND').AsString = PAYMENT_TYPE_ACH)
         or (CO_PR_ACH.FieldByName('WC_IMPOUND').AsString = PAYMENT_TYPE_ACH)
         or (CO_PR_ACH.FieldByName('BILLING_IMPOUND').AsString = PAYMENT_TYPE_ACH)
         or (CO_PR_ACH.FieldByName('TAX_IMPOUND').AsString = PAYMENT_TYPE_ACH)
         or (CO_PR_ACH.FieldByName('TRUST_IMPOUND').AsString = PAYMENT_TYPE_ACH)
        )
          or
        ((NewPrAchStatus <> COMMON_REPORT_STATUS__HOLD)
        and (NewPrAchStatus <> COMMON_REPORT_STATUS__DELETED)
        and (NewPrAchStatus <> COMMON_REPORT_STATUS__PENDING)
        ) then
      begin
        CO_PR_ACH.Edit;
        CO_PR_ACH.FieldByName('STATUS').AsString := NewPrAchStatus;
        CO_PR_ACH.Post;
      end;
      CO_PR_ACH.Next;
    end;
    CO_PR_ACH.SaveChanges;
  end;
end;

function GetCashManagement: IevCashManagement;
begin
  Result := TevCashManagement.Create;
end;

function TevCashManagement.GetNearestWorkingDate(
  const ADate: TDateTime): TDateTime;
begin
  if ADate < Date then
    Result := Date
  else
    Result := ADate;

  while ctx_PayrollCalculation.IsNonBusinessDay(Result) do
    Result := Result + 1;
end;

procedure TevCashManagement.PatchAchFiles(const AchSave: IisListOfValues;
  Options: IevAchOptions);
var  
  ReportId: Integer;
  ReportName, ReportDB, Num: ShortString;
  ASCIIFile: IisStringList;
  ReportParams: TrwReportParams;
  ResData: IisStream;
  Res: TrwReportResults;
  ReportResult: TrwReportResult;
  i: Integer;
begin
  ReportId := -1;
  if (Length(Options.PostProcessReportName) > 0)
  and (Options.PostProcessReportName <> 'N/A') then
  begin
    ReportName := Options.PostProcessReportName;
    ReportDB := '';
    Num := '';
    for i := Length(ReportName)-1 downto 1 do
    begin
      if ReportName[i] = '(' then
        break
      else
        Num := ReportName[i] + Num;
    end;
    if (Length(Num) > 0) and (Num[1] in ['S','B']) then
    begin
      ReportDB := Num[1];
      ReportId := StrToIntDef(copy(Num,2,100), -1);
    end;
  end;

  if (ReportId > 0) and Assigned(ACHSave) then
  begin
    for i := 0 to AchSave.Count - 1 do
    begin
      ASCIIFile := IInterface(AchSave.Values[i].Value) as IisStringList;
      ReportParams := TrwReportParams.Create;
      try
        ReportParams.Add('Data', ASCIIFile.Text);
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(ReportID, ReportDB, ReportParams);
        finally
          ResData := ctx_RWLocalEngine.EndGroup(rdtNone);
          Res := TrwReportResults.Create(ResData);
          try
            ReportResult := Res[0];
            ASCIIFile.Text := ReportResult.Data.AsString;
            AchSave.Values[i].Value := ASCIIFile;
          finally
            Res.Free;
          end;
        end;
      finally
        ReportParams.Free;
      end;
    end;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetCashManagement, IevCashManagement, 'ACH Module');

end.

