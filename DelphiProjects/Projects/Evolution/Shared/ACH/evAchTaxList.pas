unit evAchTaxList;

interface

uses  DB, EvDataAccessComponents, Classes, EvUtils;

type

  TevAchEFTDataSet = class(TEvBasicClientDataSet)
  private
    function Get_AMOUNT: TField;
    function Get_CL_NBR: TField;
    function Get_CO_NBR: TField;
    function Get_CO_TAX_PAYMENT_ACH_NBR: TField;
    function Get_CUSTOM_COMPANY_NUMBER: TField;
    function Get_EFFECTIVE_DATE: TField;
    function Get_EIN: TField;
    function Get_OVERRIDE_NAME: TField;
    function Get_PAYMENT_INFO: TField;
    function Get_SECOND_PAYMENT_INFO: TField;
    function Get_SPECIAL_FLAG: TField;
    function Get_STATE: TField;
    function Get_SY_GLOBAL_AGENCY_NBR: TField;
  public
    constructor Create(AOwner: TComponent); override;
    property CL_NBR: TField read Get_CL_NBR;
    property CO_NBR: TField read Get_CO_NBR;
    property CUSTOM_COMPANY_NUMBER: TField read Get_CUSTOM_COMPANY_NUMBER;
    property CO_TAX_PAYMENT_ACH_NBR: TField read Get_CO_TAX_PAYMENT_ACH_NBR;
    property AMOUNT: TField read Get_AMOUNT;
    property EFFECTIVE_DATE: TField read Get_EFFECTIVE_DATE;
    property SY_GLOBAL_AGENCY_NBR: TField read Get_SY_GLOBAL_AGENCY_NBR;
    property PAYMENT_INFO: TField read Get_PAYMENT_INFO;
    property SECOND_PAYMENT_INFO: TField read Get_SECOND_PAYMENT_INFO;
    property EIN: TField read Get_EIN;
    property OVERRIDE_NAME: TField read Get_OVERRIDE_NAME;
    property STATE: TField read Get_STATE;
    property SPECIAL_FLAG: TField read Get_SPECIAL_FLAG;
  end;


implementation

{ TevAchEFTDataSet }

constructor TevAchEFTDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FieldDefs.Add('CL_NBR', ftInteger);
  FieldDefs.Add('CO_NBR', ftInteger);
  FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 80);
  FieldDefs.Add('CO_TAX_PAYMENT_ACH_NBR', ftInteger);
  FieldDefs.Add('AMOUNT', ftCurrency);
  FieldDefs.Add('EFFECTIVE_DATE', ftDateTime);
  FieldDefs.Add('SY_GLOBAL_AGENCY_NBR', ftInteger);
  FieldDefs.Add('PAYMENT_INFO', ftString, 80);
  FieldDefs.Add('SECOND_PAYMENT_INFO', ftString, 80);
  FieldDefs.Add('EIN', ftString, 15);
  FieldDefs.Add('OVERRIDE_NAME', ftString, 40);
  FieldDefs.Add('STATE', ftString, 2);
  FieldDefs.Add('SPECIAL_FLAG', ftString, 20);
  CreateDataSet;
  IndexFieldNames := 'CUSTOM_COMPANY_NUMBER;EFFECTIVE_DATE';
end;

function TevAchEFTDataSet.Get_AMOUNT: TField;
begin
  Result := FieldByName('AMOUNT');
end;

function TevAchEFTDataSet.Get_CL_NBR: TField;
begin
  Result := FieldByName('CL_NBR');
end;

function TevAchEFTDataSet.Get_CO_NBR: TField;
begin
  Result := FieldByName('CO_NBR');
end;

function TevAchEFTDataSet.Get_CO_TAX_PAYMENT_ACH_NBR: TField;
begin
  Result := FieldByName('CO_TAX_PAYMENT_ACH_NBR');
end;

function TevAchEFTDataSet.Get_CUSTOM_COMPANY_NUMBER: TField;
begin
  Result := FieldByName('CUSTOM_COMPANY_NUMBER');
end;

function TevAchEFTDataSet.Get_EFFECTIVE_DATE: TField;
begin
  Result := FieldByName('EFFECTIVE_DATE');
end;

function TevAchEFTDataSet.Get_EIN: TField;
begin
  Result := FieldByName('EIN');
end;

function TevAchEFTDataSet.Get_OVERRIDE_NAME: TField;
begin
  Result := FieldByName('OVERRIDE_NAME');
end;

function TevAchEFTDataSet.Get_PAYMENT_INFO: TField;
begin
  Result := FieldByName('PAYMENT_INFO');
end;

function TevAchEFTDataSet.Get_SECOND_PAYMENT_INFO: TField;
begin
  Result := FieldByName('SECOND_PAYMENT_INFO');
end;

function TevAchEFTDataSet.Get_SPECIAL_FLAG: TField;
begin
  Result := FieldByName('SPECIAL_FLAG');
end;

function TevAchEFTDataSet.Get_STATE: TField;
begin
  Result := FieldByName('STATE');
end;

function TevAchEFTDataSet.Get_SY_GLOBAL_AGENCY_NBR: TField;
begin
  Result := FieldByName('SY_GLOBAL_AGENCY_NBR');
end;

end.