unit EvThreadMsgQueue;

interface

uses Classes, SysUtils, Windows;

type

  TevThreadMsg = record
    SenderThreadID:   Integer;
    Message: Integer;
    wParam: Integer;
    lParam: Integer;
  end;

  PevThreadMsg = ^TevThreadMsg;


  // Thread queue

  IevThreadQueue = interface
  ['{A93A360E-0E31-41F8-AC3C-13D6F203099C}']
    function  GetThreadID: Integer;
    procedure PushMessage(const AMsg: TevThreadMsg);
    function  PopMessage(var AMsg: TevThreadMsg): Boolean;
    function  PeekMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
    property  ThreadID: Integer read GetThreadID;
  end;

  TevThreadQueue = class(TInterfacedObject, IevThreadQueue)
  private
    FThreadID: Integer;
    FQueue: TThreadList;
    procedure Clear;
  protected
    function    GetThreadID: Integer;
    procedure   PushMessage(const AMsg: TevThreadMsg);
    function    PopMessage(var AMsg: TevThreadMsg): Boolean;
    function    PeekMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;

  public
    constructor Create(const AThreadID: Integer);
    destructor  Destroy; override;
  end;


  // Global queue manager
  TevThreadMsgQueueManager = class(TObject)
  private
    FQueueList: IInterfaceList;
    function  GetQueueByThread(const AThreadID: Integer): IevThreadQueue;
    function  CreateQueue(const AThreadID: Integer): IevThreadQueue;
  protected
    procedure PostThreadMessage(const AThreadID: Integer; const AMessage: Integer; const wParam, lParam: Integer);
    function  GetCurrentThreadMessage(var Msg: TevThreadMsg): Boolean;
    function  PeekCurrentThreadMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
    procedure DestroyCurrentThreadQueue;
  public
    constructor Create;
  end;

  procedure evPostThreadMessage(const AThreadID: Integer; const AMessage: Integer; const wParam: Integer = 0; const lParam: Integer = 0);
  function  evGetCurrentThreadMessage(var Msg: TevThreadMsg): Boolean;
  function  evPeekCurrentThreadMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
  procedure evDestroyCurrentThreadQueue;

implementation

var EvThreadQueue: TevThreadMsgQueueManager;



procedure evPostThreadMessage(const AThreadID: Integer; const AMessage: Integer; const wParam: Integer = 0; const lParam: Integer = 0);
begin
  EvThreadQueue.PostThreadMessage(AThreadID, AMessage, wParam, lParam);
end;

function evGetCurrentThreadMessage(var Msg: TevThreadMsg): Boolean;
begin
  Result := EvThreadQueue.GetCurrentThreadMessage(Msg);
end;

procedure evDestroyCurrentThreadQueue;
begin
  EvThreadQueue.DestroyCurrentThreadQueue;
end;

function evPeekCurrentThreadMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
begin
  Result := EvThreadQueue.PeekCurrentThreadMessage(Msg, wMsgFilterMin, wMsgFilterMax, Remove);
end;


{ TevThreadMsgQueueManager }

constructor TevThreadMsgQueueManager.Create;
begin
  FQueueList := TInterfaceList.Create;
end;

function TevThreadMsgQueueManager.CreateQueue(const AThreadID: Integer): IevThreadQueue;
begin
  FQueueList.Lock;
  try
    Result := GetQueueByThread(AThreadID);
    if not Assigned(Result) then
    begin
      Result := TevThreadQueue.Create(AThreadID);
      FQueueList.Add(Result);
    end;
  finally
    FQueueList.Unlock;
  end;
end;

procedure TevThreadMsgQueueManager.DestroyCurrentThreadQueue;
var
 TQ: IevThreadQueue;
begin
  TQ := GetQueueByThread(GetCurrentThreadID);
  if Assigned(TQ) then
    FQueueList.Delete(FQueueList.IndexOf(TQ));
end;

function TevThreadMsgQueueManager.GetCurrentThreadMessage(var Msg: TevThreadMsg): Boolean;
var
 TQ: IevThreadQueue;
begin
  Result := True;
  TQ := CreateQueue(GetCurrentThreadID);

  if Assigned(TQ) then
    while not TQ.PopMessage(Msg) do
    begin
      // wait for message
      Sleep(1);
    end;
end;

function TevThreadMsgQueueManager.GetQueueByThread(const AThreadID: Integer): IevThreadQueue;
var
  i: Integer;
begin
  Result := nil;

  FQueueList.Lock;
  try
    for i := 0 to FQueueList.Count - 1 do
      if IevThreadQueue(FQueueList.Get(i)).ThreadID = AThreadID  then
      begin
        Result := IevThreadQueue(FQueueList[i]);
        break;
      end;
  finally
    FQueueList.Unlock;
  end;
end;

function TevThreadMsgQueueManager.PeekCurrentThreadMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
var
  TQ: IevThreadQueue;
begin
  TQ := CreateQueue(GetCurrentThreadID);

  if Assigned(TQ) then
    Result :=  TQ.PeekMessage(Msg, wMsgFilterMin, wMsgFilterMax, Remove)
  else
    Result := False;  
end;

procedure TevThreadMsgQueueManager.PostThreadMessage(const AThreadID, AMessage, wParam, lParam: Integer);
var
 TQ: IevThreadQueue;
 Msg: TevThreadMsg;
begin
  TQ := CreateQueue(AThreadID);

  if Assigned(TQ) then
  begin
    Msg.SenderThreadID := GetCurrentThreadId;
    Msg.Message := AMessage;
    Msg.wParam := wParam;
    Msg.lParam := lParam;
    TQ.PushMessage(Msg);
  end;  
end;

{ TevThreadQueue }

procedure TevThreadQueue.Clear;
var
  L: TList;
  i: Integer;
begin
  L := FQueue.LockList;
  try
    for i := 0 to L.Count - 1 do
      Dispose(PevThreadMsg(L[i]));
    L.Clear;
  finally
    FQueue.UnlockList;
  end;
end;

constructor TevThreadQueue.Create(const AThreadID: Integer);
var
  L: TList;
begin
  FThreadID := AThreadID;

  FQueue := TThreadList.Create;
  L := FQueue.LockList;
  try
    L.Capacity := 16384;
  finally
    FQueue.UnlockList;
  end;
end;

destructor TevThreadQueue.Destroy;
begin
  Clear;
  inherited;
  FreeAndNil(FQueue);
end;

function TevThreadQueue.GetThreadID: Integer;
begin
  Result := FThreadID;
end;

function TevThreadQueue.PeekMessage(var Msg: TevThreadMsg; wMsgFilterMin, wMsgFilterMax: Integer; Remove: Boolean): Boolean;
var
  i: Integer;
  L: TList;
  M: TevThreadMsg;
begin
  Result := False;
  L := FQueue.LockList;
  try
    for i := 0 to L.Count - 1 do
    begin
      M := PevThreadMsg(L[i])^;
      Result := (M.Message >= wMsgFilterMin) and (M.Message <= wMsgFilterMax);
      if Result then
      begin
        Msg := M;
        if Remove then
        begin
          Dispose(PevThreadMsg(L[i]));
          L.Delete(i);
        end;
        break;
      end;
    end
  finally
    FQueue.UnlockList;
  end;
end;

function TevThreadQueue.PopMessage(var AMsg: TevThreadMsg): Boolean;
var
  L: TList;
begin
  L := FQueue.LockList;
  try
    Result := L.Count > 0;
    if Result then
    begin
      AMsg := PevThreadMsg(L[0])^;
      Dispose(PevThreadMsg(L[0]));
      L.Delete(0);
    end;
  finally
    FQueue.UnlockList;
  end;
end;

procedure TevThreadQueue.PushMessage(const AMsg: TevThreadMsg);
var
  MsgItem: PevThreadMsg;
begin
  New(MsgItem);
  MsgItem^ := AMsg;
  FQueue.Add(MsgItem);
end;

initialization
  EvThreadQueue := TevThreadMsgQueueManager.Create;

finalization
  FreeAndNil(EvThreadQueue);

end.
 