// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDDAddProps;

interface

uses
  SDDClasses, typinfo, SDataDictAdd, SDataStructure, evTypes, StrUtils,
  SDataDictSystem, SDataDictBureau, SDataDictClient, SDataDictTemp;

type
  TddAddReferenceDesc = record
    ParentDatabase: TddDatabaseClass;
    ParentTable: TddTableClass;
    ParentFields: string;
    ChildDatabase: TddDatabaseClass;
    ChildTable: TddTableClass;
    ChildFields: string;
  end;

  TTableProp = record
    Table: TddTableClass;
    case PType: TVarType of
    varInteger: (IntValue: Integer);
    varBoolean: (BoolValue: Boolean);
    varString: (StringValue: ShortString);
  end;

const
  DEFAULT_UNSECURE = False;
  DEFAULT_CURRENT = False;
  DEFAULT_FILLER_LENGTH = -1;

  ddAutoFillFields: array [0..154] of TTableProp =
  (
  (Table: TCO_TIME_OFF_ACCRUAL_TIERS; StringValue: 'CO_TIME_OFF_ACCRUAL_NBR'),
  (Table: TSB_AGENCY_REPORTS; StringValue: 'SB_AGENCY_NBR'),
  (Table: TSB_BANK_ACCOUNTS; StringValue: 'SB_BANKS_NBR'),
  (Table: TSB_SERVICES_CALCULATIONS; StringValue: 'SB_SERVICES_NBR'),
  (Table: TSB_TEAM_MEMBERS; StringValue: 'SB_TEAM_NBR'),
  (Table: TSB_DELIVERY_COMPANY_SVCS; StringValue: 'SB_DELIVERY_COMPANY_NBR'),
  (Table: TSB_DELIVERY_SERVICE_OPT; StringValue: 'SB_DELIVERY_SERVICE_NBR'),
  (Table: TSB_MEDIA_TYPE_OPTION; StringValue: 'SB_MEDIA_TYPE_NBR'),
  (Table: TSB_MAIL_BOX_OPTION; StringValue: 'SB_MAIL_BOX_NBR'),
  (Table: TSB_MAIL_BOX_CONTENT; StringValue: 'SB_MAIL_BOX_NBR'),
  (Table: TSY_STATE_MARITAL_STATUS; StringValue: 'SY_STATES_NBR'),
  (Table: TSY_STATE_TAX_CHART; StringValue: 'SY_STATES_NBR'),
  (Table: TSY_STATE_EXEMPTIONS; StringValue: 'SY_STATES_NBR'),
  (Table: TSY_SUI; StringValue: 'SY_STATES_NBR'),
  (Table: TSY_STATE_DEPOSIT_FREQ; StringValue: 'SY_STATES_NBR'),
  (Table: TSY_LOCAL_EXEMPTIONS; StringValue: 'SY_LOCALS_NBR'),
  (Table: TSY_LOCAL_DEPOSIT_FREQ; StringValue: 'SY_LOCALS_NBR'),
  (Table: TSY_LOCAL_TAX_CHART; StringValue: 'SY_LOCALS_NBR'),
  (Table: TSY_LOCAL_MARITAL_STATUS; StringValue: 'SY_LOCALS_NBR'),
  (Table: TSY_GL_AGENCY_FIELD_OFFICE; StringValue: 'SY_GLOBAL_AGENCY_NBR'),
  (Table: TSY_AGENCY_DEPOSIT_FREQ; StringValue: 'SY_GLOBAL_AGENCY_NBR'),
  (Table: TSY_GL_AGENCY_HOLIDAYS; StringValue: 'SY_GLOBAL_AGENCY_NBR'),
  (Table: TSY_GL_AGENCY_REPORT; StringValue: 'SY_GL_AGENCY_FIELD_OFFICE_NBR'),
  (Table: TSY_DELIVERY_METHOD; StringValue: 'SY_DELIVERY_SERVICE_NBR'),
  (Table: TCO_ACA_CERT_ELIGIBILITY; StringValue: 'CO_NBR'),
  (Table: TCO_ADDITIONAL_INFO_NAMES; StringValue: 'CO_NBR'),
  (Table: TCO_PHONE; StringValue: 'CO_NBR'),
  (Table: TCO_PAY_GROUP; StringValue: 'CO_NBR'),
  (Table: TCO_PENSIONS; StringValue: 'CO_NBR'),
  (Table: TCO_E_D_CODES; StringValue: 'CO_NBR'),
  (Table: TCO_TIME_OFF_ACCRUAL; StringValue: 'CO_NBR'),
  (Table: TCO_REPORTS; StringValue: 'CO_NBR'),
  (Table: TCO_SHIFTS; StringValue: 'CO_NBR'),
  (Table: TCO_UNIONS; StringValue: 'CO_NBR'),
  (Table: TCO_STATES; StringValue: 'CO_NBR'),
  (Table: TCO_WORKERS_COMP; StringValue: 'CO_NBR'),
  (Table: TCO_DIVISION; StringValue: 'CO_NBR'),
  (Table: TCO_DIVISION_LOCALS; StringValue: 'CO_DIVISION_NBR'),
  (Table: TCO_DIV_PR_BATCH_DEFLT_ED; StringValue: 'CO_DIVISION_NBR'),
  (Table: TCO_BRANCH; StringValue: 'CO_DIVISION_NBR'),
  (Table: TCO_BRANCH_LOCALS; StringValue: 'CO_BRANCH_NBR'),
  (Table: TCO_BRCH_PR_BATCH_DEFLT_ED; StringValue: 'CO_BRANCH_NBR'),
  (Table: TCO_DEPARTMENT; StringValue: 'CO_BRANCH_NBR'),
  (Table: TCO_DEPARTMENT_LOCALS; StringValue: 'CO_DEPARTMENT_NBR'),
  (Table: TCO_DEPT_PR_BATCH_DEFLT_ED; StringValue: 'CO_DEPARTMENT_NBR'),
  (Table: TCO_TEAM; StringValue: 'CO_DEPARTMENT_NBR'),
  (Table: TCO_TEAM_LOCALS; StringValue: 'CO_TEAM_NBR'),
  (Table: TCO_TEAM_PR_BATCH_DEFLT_ED; StringValue: 'CO_TEAM_NBR'),
  (Table: TCO_GENERAL_LEDGER; StringValue: 'CO_NBR'),
  (Table: TCO_SALESPERSON; StringValue: 'CO_NBR'),
  (Table: TCO_SALESPERSON_FLAT_AMT; StringValue: 'CO_SALESPERSON_NBR'),
  (Table: TCO_BANK_ACCOUNT_REGISTER; StringValue: 'CO_NBR'),
  (Table: TCO_LOCATIONS; StringValue: 'CO_NBR'),
  (Table: TCO_JOBS; StringValue: 'CO_NBR'),
  (Table: TCO_JOBS_LOCALS; StringValue: 'CO_JOBS_NBR'),
  (Table: TCO_HR_SALARY_GRADES; StringValue: 'CO_NBR'),
  (Table: TCO_LOCAL_TAX; StringValue: 'CO_NBR'),
  (Table: TCO_BILLING_HISTORY; StringValue: 'CO_NBR'),
  (Table: TCO_PR_CHECK_TEMPLATES; StringValue: 'CO_NBR'),
  (Table: TCO_BATCH_LOCAL_OR_TEMPS; StringValue: 'CO_PR_CHECK_TEMPLATES_NBR'),
  (Table: TCO_BATCH_STATES_OR_TEMPS; StringValue: 'CO_PR_CHECK_TEMPLATES_NBR'),
  (Table: TCO_PR_CHECK_TEMPLATE_E_DS; StringValue: 'CO_PR_CHECK_TEMPLATES_NBR'),
  (Table: TCO_PR_BATCH_DEFLT_ED; StringValue: 'CO_NBR'),
  (Table: TCO_HR_POSITIONS; StringValue: 'CO_NBR'),
  (Table: TCO_HR_RECRUITERS; StringValue: 'CO_NBR'),
  (Table: TCO_HR_SUPERVISORS; StringValue: 'CO_NBR'),
  (Table: TCO_HR_REFERRALS; StringValue: 'CO_NBR'),
  (Table: TCO_HR_PERFORMANCE_RATINGS; StringValue: 'CO_NBR'),
  (Table: TCO_HR_CAR; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFITS; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_CATEGORY; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_DISCOUNT; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_PACKAGE; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_PROVIDERS; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_SETUP; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_SUBTYPE; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_RATES; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_RATES; StringValue: 'CO_BENEFITS_NBR'),
  (Table: TCO_BENEFIT_STATES; StringValue: 'CO_NBR'),
  (Table: TCO_BENEFIT_ENROLLMENT; StringValue: 'CO_NBR'),
  (Table: TCO_GROUP; StringValue: 'CO_NBR'),
  (Table: TCO_BILLING_HISTORY_DETAIL; StringValue: 'CO_BILLING_HISTORY_NBR'),
  (Table: TCO_SERVICES; StringValue: 'CO_NBR'),
  (Table: TCO_FED_TAX_LIABILITIES; StringValue: 'CO_NBR'),
  (Table: TCO_TAX_RETURN_QUEUE; StringValue: 'CO_NBR'),
  (Table: TCO_HR_APPLICANT_INTERVIEW; StringValue: 'CO_HR_APPLICANT_NBR'),
  (Table: TCO_HR_APPLICANT; StringValue: 'CO_NBR'),
  (Table: TCO_HR_ATTENDANCE_TYPES; StringValue: 'CO_NBR'),
  (Table: TCO_HR_PROPERTY; StringValue: 'CO_NBR'),
  (Table: TCO_MANUAL_ACH; StringValue: 'CO_NBR'),
  (Table: TCO_PR_ACH; StringValue: 'CO_NBR'),
  (Table: TCO_PR_FILTERS; StringValue: 'CO_NBR'),
  (Table: TCO_STATE_TAX_LIABILITIES; StringValue: 'CO_NBR'),
  (Table: TCO_SUI; StringValue: 'CO_NBR'),
  (Table: TCO_SUI_LIABILITIES; StringValue: 'CO_NBR'),
  (Table: TCO_TAX_DEPOSITS; StringValue: 'CO_NBR'),
  (Table: TCO_TAX_PAYMENT_ACH; StringValue: 'CO_NBR'),
  (Table: TCO_TIME_OFF_ACCRUAL_RATES; StringValue: 'CO_TIME_OFF_ACCRUAL_NBR'),
  (Table: TCO_LOCAL_TAX_LIABILITIES; StringValue: 'CO_NBR'),
  (Table: TCO_AUTO_ENLIST_RETURNS; StringValue: 'CO_NBR'),
  (Table: TCO_ENLIST_GROUPS; StringValue: 'CO_NBR'),
  (Table: TCO_VENDOR; StringValue: 'CO_NBR'),
  (Table: TCO_VENDOR_DETAIL_VALUES; StringValue: 'CO_NBR'),
  (Table: TCO_DASHBOARDS; StringValue: 'CO_NBR'),
  (Table: TCO_DASHBOARDS_USER; StringValue: 'CO_NBR'),
  (Table: TCO_ACA_PERIODS; StringValue: 'CO_NBR'),
  (Table: TCO_MESSAGES; StringValue: 'CO_NBR'),
  (Table: TCL_E_D_LOCAL_EXMPT_EXCLD; StringValue: 'CL_E_DS_NBR'),
  (Table: TCL_E_D_STATE_EXMPT_EXCLD; StringValue: 'CL_E_DS_NBR'),
  (Table: TCL_FUNDS; StringValue: 'CL_PENSION_NBR'),
  (Table: TCL_PERSON_DEPENDENTS; StringValue: 'CL_PERSON_NBR'),
  (Table: TCL_UNION_DUES; StringValue: 'CL_UNION_NBR'),
  (Table: TCL_E_D_GROUP_CODES; StringValue: 'CL_E_D_GROUPS_NBR'),
  (Table: TCL_HR_PERSON_EDUCATION; StringValue: 'CL_PERSON_NBR'),
  (Table: TCL_HR_PERSON_HANDICAPS; StringValue: 'CL_PERSON_NBR'),
  (Table: TCL_HR_PERSON_SKILLS; StringValue: 'CL_PERSON_NBR'),
  (Table: TCL_MAIL_BOX_GROUP_OPTION; StringValue: 'CL_MAIL_BOX_GROUP_NBR'),
  (Table: TCL_PERSON_DOCUMENTS; StringValue: 'CL_PERSON_NBR'),
  (Table: TEE_TIME_OFF_ACCRUAL; StringValue: 'EE_NBR'),
  (Table: TEE_SCHEDULED_E_DS; StringValue: 'EE_NBR'),
  (Table: TEE_RATES; StringValue: 'EE_NBR'),
  (Table: TEE_AUTOLABOR_DISTRIBUTION; StringValue: 'EE_NBR'),
  (Table: TEE_CHILD_SUPPORT_CASES; StringValue: 'EE_NBR'),
  (Table: TEE_DIRECT_DEPOSIT; StringValue: 'EE_NBR'),
  (Table: TEE_HR_ATTENDANCE; StringValue: 'EE_NBR'),
  (Table: TEE_HR_CO_PROVIDED_EDUCATN; StringValue: 'EE_NBR'),
  (Table: TEE_HR_INJURY_OCCURRENCE; StringValue: 'EE_NBR'),
  (Table: TEE_HR_PROPERTY_TRACKING; StringValue: 'EE_NBR'),
  (Table: TEE_LOCALS; StringValue: 'EE_NBR'),
  (Table: TEE_PENSION_FUND_SPLITS; StringValue: 'EE_NBR'),
  (Table: TEE_PIECE_WORK; StringValue: 'EE_NBR'),
  (Table: TEE_STATES; StringValue: 'EE_NBR'),
  (Table: TEE_WORK_SHIFTS; StringValue: 'EE_NBR'),
  (Table: TEE_ADDITIONAL_INFO; StringValue: 'EE_NBR'),
  (Table: TEE_BENEFIT_PAYMENT; StringValue: 'EE_NBR'),
  (Table: TEE_EMERGENCY_CONTACTS; StringValue: 'EE_NBR'),
  (Table: TEE_BENEFITS; StringValue: 'EE_NBR'),
  (Table: TEE_HR_CAR; StringValue: 'EE_NBR'),
  (Table: TEE_TIME_OFF_ACCRUAL_OPER; StringValue: 'EE_TIME_OFF_ACCRUAL_NBR'),
  (Table: TEE_HR_PERFORMANCE_RATINGS; StringValue: 'EE_NBR'),
  (Table: TPR_BATCH; StringValue: 'PR_NBR'),
  (Table: TPR_CHECK; StringValue: 'PR_BATCH_NBR'),
  (Table: TPR_CHECK_LINES; StringValue: 'PR_CHECK_NBR'),
  (Table: TPR_CHECK_STATES; StringValue: 'PR_CHECK_NBR'),
  (Table: TPR_CHECK_SUI; StringValue: 'PR_CHECK_NBR'),
  (Table: TPR_CHECK_LOCALS; StringValue: 'PR_CHECK_NBR'),
  (Table: TPR_CHECK_LINE_LOCALS; StringValue: 'PR_CHECK_LINES_NBR'),
  (Table: TPR_MISCELLANEOUS_CHECKS; StringValue: 'PR_NBR'),
  (Table: TPR_REPORTS; StringValue: 'PR_NBR'),
  (Table: TPR_REPRINT_HISTORY; StringValue: 'PR_NBR'),
  (Table: TPR_REPRINT_HISTORY_DETAIL; StringValue: 'PR_REPRINT_HISTORY_NBR'),
  (Table: TPR_SCHEDULED_E_DS; StringValue: 'PR_NBR'),
  (Table: TPR_SERVICES; StringValue: 'PR_NBR'),
  (Table: TPR_SCHEDULED_EVENT; StringValue: 'CO_NBR'),
  (Table: TPR_SCHEDULED_EVENT_BATCH; StringValue: 'PR_SCHEDULED_EVENT_NBR')
  );

  ddUnsecureTables: array [0..0] of TTableProp =
  (
  (Table: TSY_STORAGE; BoolValue: True)
  );

  ddHistTables: array [0..0] of TTableProp =
  (
  (Table: TPR_CHECK_LINES_DISTRIBUTED; BoolValue: False)
  );

  ddCurrentTables: array [0..24] of TTableProp =
  (
  (Table: TSB_MAIL_BOX; BoolValue: True),
  (Table: TSB_MAIL_BOX_OPTION; BoolValue: True),
  (Table: TSB_MAIL_BOX_CONTENT; BoolValue: True),
  (Table: TCO_BANK_ACCOUNT_REGISTER; BoolValue: True),
  (Table: TCO_FED_TAX_LIABILITIES; BoolValue: True),
  (Table: TCO_LOCAL_TAX_LIABILITIES; BoolValue: True),
  (Table: TCO_STATE_TAX_LIABILITIES; BoolValue: True),
  (Table: TCO_SUI_LIABILITIES; BoolValue: True),
  (Table: TEE_TIME_OFF_ACCRUAL; BoolValue: True),
  (Table: TEE_TIME_OFF_ACCRUAL_OPER; BoolValue: True),
  (Table: TPR; BoolValue: True),
  (Table: TPR_BATCH; BoolValue: True),
  (Table: TPR_CHECK; BoolValue: True),
  (Table: TPR_CHECK_LINES; BoolValue: True),
  (Table: TPR_CHECK_LINE_LOCALS; BoolValue: True),
  (Table: TPR_CHECK_LOCALS; BoolValue: True),
  (Table: TPR_CHECK_STATES; BoolValue: True),
  (Table: TPR_CHECK_SUI; BoolValue: True),
  (Table: TPR_MISCELLANEOUS_CHECKS; BoolValue: True),
  (Table: TSB_USER; BoolValue: True),  
  (Table: TSB_SEC_CLIENTS; BoolValue: True),
  (Table: TSB_SEC_ROW_FILTERS; BoolValue: True),
  (Table: TSB_SEC_GROUP_MEMBERS; BoolValue: True),
  (Table: TSB_SEC_GROUPS; BoolValue: True),
  (Table: TSB_SEC_RIGHTS; BoolValue: True)
  );

  ddFillerLength: array [0..30] of TTableProp =
  (
  (Table: TSY_STATE_DEPOSIT_FREQ; IntValue: 15),
  (Table: TSB_BANK_ACCOUNTS; IntValue: 60),
  (Table: TSB_BANKS; IntValue: 7),
  (Table: TSB_SERVICES; IntValue: 1),
  (Table: TCO; IntValue: 60),
  (Table: TCO_BANK_ACCOUNT_REGISTER; IntValue: 28),
  (Table: TCO_BILLING_HISTORY; IntValue: 8),
  (Table: TCO_BRCH_PR_BATCH_DEFLT_ED; IntValue: 3),
  (Table: TCO_DEPT_PR_BATCH_DEFLT_ED; IntValue: 3),
  (Table: TCO_DIV_PR_BATCH_DEFLT_ED; IntValue: 3),
  (Table: TCO_FED_TAX_LIABILITIES; IntValue: 20),
  (Table: TCO_JOBS; IntValue: 60),
  (Table: TCO_LOCAL_TAX; IntValue: 10),
  (Table: TCO_LOCAL_TAX_LIABILITIES; IntValue: 20),
  (Table: TCO_PR_ACH; IntValue: 512),
  (Table: TCO_SERVICES; IntValue: 5),
  (Table: TCO_STATE_TAX_LIABILITIES; IntValue: 20),
  (Table: TCO_STATES; IntValue: 21),
  (Table: TCO_SUI_LIABILITIES; IntValue: 20),
  (Table: TCO_TAX_PAYMENT_ACH; IntValue: 106),
  (Table: TCO_TEAM_PR_BATCH_DEFLT_ED; IntValue: 3),
  (Table: TCO_TIME_OFF_ACCRUAL; IntValue: 14),
  (Table: TPR; IntValue: 10),
  (Table: TPR_CHECK; IntValue: 8),
  (Table: TPR_BATCH; IntValue: 8),
  (Table: TPR_CHECK_LINES; IntValue: 12),
  (Table: TPR_MISCELLANEOUS_CHECKS; IntValue: 8),
  (Table: TSY_GLOBAL_AGENCY; IntValue: 2),
  (Table: TCO_PR_BATCH_DEFLT_ED; IntValue: 3),
  (Table: TSY_GL_AGENCY_REPORT; IntValue: 3),
  (Table: TSY_GL_AGENCY_REPORT; IntValue: 3)
  );


function GetIsAutoFillField(const Table: TddTableClass; const FieldName: string): Boolean;
function GetFillerLength(const Table: TddTableClass): Integer;
function GetIsHistorical(const Table: TddTableClass): Boolean;
function GetIsUnSecure(const Table: TddTableClass): Boolean;
function GetIsCurrent(const Table: TddTableClass): Boolean;

implementation

function GetFillerLength(const Table: TddTableClass): Integer;
var
  i: Integer;
  s: string;
begin
  s := Table.ClassName;
  if LeftStr(s, 3) = 'TMP' then
    s := RightStr(s, Length(s) - 4);
  Result := DEFAULT_FILLER_LENGTH;
  for i := 0 to High(ddFillerLength) do
    if ddFillerLength[i].Table.ClassName = s then
    begin
      Result := ddFillerLength[i].IntValue;
      Break;
    end;
end;

function GetIsHistorical(const Table: TddTableClass): Boolean;
var
  i: Integer;
begin
  Result := GetddDatabaseByTableClass(Table).GetDBType <> dbtTemporary;
  if Result then
    for i := 0 to High(ddHistTables) do
      if ddHistTables[i].Table = Table then
      begin
        Result := ddHistTables[i].BoolValue;
        Break;
      end;
end;


function GetIsAutoFillField(const Table: TddTableClass; const FieldName: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to High(ddAutoFillFields) do
    if ddAutoFillFields[i].Table = Table then
    begin
      Result := ddAutoFillFields[i].StringValue = FieldName;
      if Result then
        Break;
    end;
end;

function GetIsUnSecure(const Table: TddTableClass): Boolean;
var
  i: Integer;
begin
  Result := DEFAULT_UNSECURE;
  for i := 0 to High(ddUnsecureTables) do
    if ddUnsecureTables[i].Table = Table then
    begin
      Result := ddUnsecureTables[i].BoolValue;
      Break;
    end;
end;

function GetIsCurrent(const Table: TddTableClass): Boolean;
var
  i: Integer;
begin
  Result := DEFAULT_CURRENT;
  for i := 0 to High(ddCurrentTables) do
    if ddCurrentTables[i].Table = Table then
    begin
      Result := ddCurrentTables[i].BoolValue;
      Break;
    end;
end;

var
  i: Integer;

initialization 
  for i := 0 to High(ddAutoFillFields) do
    Assert(IsPublishedProp(ddAutoFillFields[i].Table, ddAutoFillFields[i].StringValue), ddAutoFillFields[i].Table.ClassName + '.' + ddAutoFillFields[i].StringValue + ' does not exist');

end.
