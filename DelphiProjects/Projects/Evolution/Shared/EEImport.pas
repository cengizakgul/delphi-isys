// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EEImport;

interface

uses
  IEMap, EvUtils, SysUtils, EvConsts, SFieldCodeValues,  db, classes, EvTypes, Variants,
  SRapidMaritalStatusImport, isBasicUtils, EvClientDataSet;

type
  THRTraxDefinition = class(TIEDefinitionBase)
  private
    FImpEngine: IIEMap;
  published
    procedure AI_EE_WORK_SHIFTS(const ds: TevClientDataSet; const KeyTables,
      KeyFields: TStringArray; const KeyValues: TVariantArray);
    procedure AI_EE_RATES(const ds: TevClientDataSet; const KeyTables,
      KeyFields: TStringArray; const KeyValues: TVariantArray);
    procedure AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet;
      const KeyTables, KeyFields: TStringArray;
      const KeyValues: TVariantArray);
    procedure AI_EE_STATES(const ds: TevClientDataSet; const KeyTables,
      KeyFields: TStringArray; const KeyValues: TVariantArray);
    procedure AI_EE_HR_ATTENDANCE(const ds: TevClientDataSet; const KeyTables,
      KeyFields: TStringArray; const KeyValues: TVariantArray);
    function IECL_PERSON_STATE(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEActiveWeeks(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEBranch(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEDept(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEEDTarget(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEFirstNameMI(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IETaxValue(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function EEShift(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IETermCode(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function IEVetCode(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeTax(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeSDI(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeSUI(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function RapidMaritalStatus(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function ValidateMaritalStatus(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
  public
    constructor Create(const AIEMap: IIEMap); reintroduce;
  end;

implementation

uses
  EEIFuncs;

// custom functions
procedure THRTraxDefinition.AI_EE_STATES(const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray);
begin
  ds['IMPORTED_MARITAL_STATUS'] := GROUP_BOX_SINGLE;
end;

function THRTraxDefinition.IEDept(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  s: string;
  sDiv, sBranch: string;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DEPARTMENT'));
  if IsImport then
  begin
    Result := True;
    s := KeyValues[2];
    sDiv := GetNextStrValue(s, ',');
    sBranch := GetNextStrValue(s, ',');
    SetLength(KeyFields, 2);
    SetLength(KeyTables, 2);
    SetLength(KeyValues, 2);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'));
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'));
    SetLength(OTables, 3);
    SetLength(OFields, 3);
    SetLength(OValues, 3);
    OTables[2] := 'EE';
    OFields[2] := 'CO_DIVISION_NBR';
    if sDiv <> '' then
      OValues[2] := FImpEngine.IDS('CO_DIVISION').Lookup('CO_NBR;CUSTOM_DIVISION_NUMBER', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], PadStringLeft(sDiv, ' ', 20)]), 'CO_DIVISION_NBR')
    else
      OValues[2] := FImpEngine.IDS('CO_DIVISION')['CO_DIVISION_NBR'];
    OTables[1] := 'EE';
    OFields[1] := 'CO_BRANCH_NBR';
    if sBranch <> '' then
      OValues[1] := FImpEngine.IDS('CO_BRANCH').Lookup('CO_DIVISION_NBR;CUSTOM_BRANCH_NUMBER', VarArrayOf([OValues[2], PadStringLeft(sBranch, ' ', 20)]), 'CO_BRANCH_NBR')
    else
      OValues[1] := FImpEngine.IDS('CO_BRANCH')['CO_BRANCH_NBR'];
    OTables[0] := 'EE';
    OFields[0] := 'CO_DEPARTMENT_NBR';
    OValues[0] := FImpEngine.IDS('CO_DEPARTMENT').Lookup('CO_BRANCH_NBR;CUSTOM_DEPARTMENT_NUMBER', VarArrayOf([OValues[1], Value]), 'CO_DEPARTMENT_NBR');
  end
  else
  begin
    Result := FImpEngine.IDS('CO_DEPARTMENT').Locate('CO_DEPARTMENT_NBR', Value, []);
    if Result then
    begin
      SetLength(OTables, 1);
      SetLength(OFields, 1);
      SetLength(OValues, 1);
      OTables[0] := '';
      OFields[0] := 'DEPT.';
      OValues[0] := FImpEngine.IDS('CO_DEPARTMENT')['CUSTOM_DEPARTMENT_NUMBER'];
    end
  end
end;

function THRTraxDefinition.IEBranch(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  s: string;
  sDiv: string;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'));
  if IsImport then
  begin
    Result := True;
    s := KeyValues[2];
    sDiv := GetNextStrValue(s, ',');
    SetLength(KeyFields, 2);
    SetLength(KeyTables, 2);
    SetLength(KeyValues, 2);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'));
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[1] := 'EE';
    OFields[1] := 'CO_DIVISION_NBR';
    if sDiv <> '' then
      OValues[1] := FImpEngine.IDS('CO_DIVISION').Lookup('CO_NBR;CUSTOM_DIVISION_NUMBER', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], PadStringLeft(sDiv, ' ', 20)]), 'CO_DIVISION_NBR')
    else
      OValues[1] := FImpEngine.IDS('CO_DIVISION')['CO_DIVISION_NBR'];
    OTables[0] := 'EE';
    OFields[0] := 'CO_BRANCH_NBR';
    OValues[0] := FImpEngine.IDS('CO_BRANCH').Lookup('CO_DIVISION_NBR;CUSTOM_BRANCH_NUMBER', VarArrayOf([OValues[1], Value]), 'CO_BRANCH_NBR');
  end
  else
  begin
    Result := FImpEngine.IDS('CO_BRANCH').Locate('CO_BRANCH_NBR', Value, []);
    if Result then
    begin
      SetLength(OTables, 1);
      SetLength(OFields, 1);
      SetLength(OValues, 1);
      OTables[0] := '';
      OFields[0] := 'BRANCH';
      OValues[0] := FImpEngine.IDS('CO_BRANCH')['CUSTOM_BRANCH_NUMBER'];
    end
  end
end;

function THRTraxDefinition.IEFirstNameMI(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);

    OTables[0] := 'CL_PERSON';
    OFields[0] := 'FIRST_NAME';
    if Pos(' ', Value) > 0 then
      OValues[0] := Copy(Value, 1, Pred(Pos(' ', Value)))
    else
      OValues[0] := Value;

    OTables[1] := 'CL_PERSON';
    OFields[1] := 'MIDDLE_INITIAL';
    if Pos(' ', Value) > 0 then
      OValues[1] := Copy(Value, Succ(Pos(' ', Value)), Length(Value))
    else
      OValues[1] := null;
  end
  else
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_PERSON'), 'CL_PERSON_NBR = ''' + VarToStr(KeyValues[0]) + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'FIRST NAME';
    if FImpEngine.IDS('CL_PERSON').FieldByName('MIDDLE_INITIAL').AsString <> '' then
      OValues[0] := FImpEngine.IDS('CL_PERSON')['FIRST_NAME'] + ' ' + FImpEngine.IDS('CL_PERSON')['MIDDLE_INITIAL']
    else
      OValues[0] := FImpEngine.IDS('CL_PERSON')['FIRST_NAME'];
  end;
end;

function THRTraxDefinition.IEVetCode(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 4);
    SetLength(OFields, 4);
    SetLength(OValues, 4);

    OTables[0] := 'CL_PERSON';
    OTables[1] := 'CL_PERSON';
    OTables[2] := 'CL_PERSON';
    OTables[3] := 'CL_PERSON';

    OFields[0] := 'VIETNAM_VETERAN';
    OFields[1] := 'DISABLED_VETERAN';
    OFields[2] := 'MILITARY_RESERVE';
    OFields[3] := 'VETERAN';

    OValues[0] := GROUP_BOX_NO;
    OValues[1] := GROUP_BOX_NO;
    OValues[2] := GROUP_BOX_NO;
    OValues[3] := GROUP_BOX_NO;
    if Value = 'V' then
      OValues[0] := GROUP_BOX_YES;
    if Value = 'S' then
      OValues[1] := GROUP_BOX_YES;
    if Value = 'A' then
      OValues[2] := GROUP_BOX_YES;
    if Value = 'O' then
      OValues[3] := GROUP_BOX_YES;
  end
  else
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_PERSON'), 'CL_PERSON_NBR = ''' + VarToStr(KeyValues[0]) + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'MILITARY/VET CODE';
    if FImpEngine.IDS('CL_PERSON')['VIETNAM_VETERAN'] = GROUP_BOX_YES then
      OValues[0] := 'V'
    else if FImpEngine.IDS('CL_PERSON')['DISABLED_VETERAN'] = GROUP_BOX_YES then
      OValues[0] := 'S'
    else if FImpEngine.IDS('CL_PERSON')['MILITARY_RESERVE'] = GROUP_BOX_YES then
      OValues[0] := 'A'
    else if FImpEngine.IDS('CL_PERSON')['VETERAN'] = GROUP_BOX_YES then
      OValues[0] := 'O'
    else
      OValues[0] := ' ';
  end;
end;

function THRTraxDefinition.IETermCode(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);

    OTables[0] := 'EE';
    OFields[0] := 'CURRENT_TERMINATION_CODE';
    OValues[0] := unAssigned;
    if Field = 'TERMINATION CODE' then
    begin
      if Pos(#9 + Value, EE_TerminationCode_ComboChoices) > 0 then
        OValues[0] := Value
      else
        OValues[0] := EE_TERM_TERMINATED;
    end
    else if Field = 'PAY STATUS' then
    begin
      if (Value = 'FT') or
         (Value = 'PT') or
         (Value = 'OC') or
         (Value = 'RE') then
        OValues[0] := EE_TERM_ACTIVE
      else if Value = 'TE' then
        OValues[0] := EE_TERM_TERMINATED
      else if Value = 'LE' then
        OValues[0] := EE_TERM_LEAVE;
    end
    else if Field = 'SEASONAL' then
      if Value = 'Y' then
        OValues[0] := EE_TERM_SEASONAL;
    Result := not VarIsEmpty(OValues[0]);
  end
  else
  begin
    Result := True;
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE'), 'EE_NBR = ''' + VarToStr(KeyValues[0]) + '''');
    SetLength(OTables, 3);
    SetLength(OFields, 3);
    SetLength(OValues, 3);
    OTables[0] := '';
    OTables[1] := '';
    OTables[2] := '';
    OFields[0] := 'TERMINATION CODE';
    OFields[1] := 'PAY STATUS';
    OFields[2] := 'SEASONAL';
    OValues[0] := FImpEngine.IDS('EE')['CURRENT_TERMINATION_CODE'];
    if FImpEngine.IDS('EE')['CURRENT_TERMINATION_CODE'] = EE_TERM_LEAVE then
      OValues[1] := 'LE'
    else if FImpEngine.IDS('EE')['CURRENT_TERMINATION_CODE'] = EE_TERM_TERMINATED then
      OValues[1] := 'TE'
    else
      OValues[1] := 'FT';
    if FImpEngine.IDS('EE')['CURRENT_TERMINATION_CODE'] = EE_TERM_SEASONAL then
      OValues[2] := 'Y'
    else
      OValues[2] := 'N';
  end;
end;

function THRTraxDefinition.IETaxValue(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  ds: TevClientDataSet;
  sField: string;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);

    if Pos('FED', Field) > 0 then
    begin
      OTables[0] := 'EE';
      OFields[0] := 'OVERRIDE_FED_TAX_TYPE';
      OTables[1] := 'EE';
      OFields[1] := 'OVERRIDE_FED_TAX_VALUE';
    end
    else
    begin
      OTables[0] := 'EE_STATES';
      OFields[0] := 'OVERRIDE_STATE_TAX_TYPE';
      OTables[1] := 'EE_STATES';
      OFields[1] := 'OVERRIDE_STATE_TAX_VALUE';
    end;

    if Pos('ADDTL %', Field) > 0 then
      OValues[0] := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT
    else if Pos('ADDTL', Field) > 0 then
      OValues[0] := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT
    else if Pos('VOL %', Field) > 0 then
      OValues[0] := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT
    else if Pos('VOL', Field) > 0 then
      OValues[0] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT
    else
      OValues[0] := OVERRIDE_VALUE_TYPE_NONE;
    OValues[1] := Value;
  end
  else
  begin
    SetLength(OTables, 4);
    SetLength(OFields, 4);
    SetLength(OValues, 4);
    OTables[0] := '';
    OTables[1] := '';
    OTables[2] := '';
    OTables[3] := '';
    if Pos('STATE', Field) > 0 then
    begin
      ds := FImpEngine.IDS('EE_STATES');
      sField := 'OVERRIDE_STATE_TAX_';
      OFields[0] := 'ADDTL STATE';
      OFields[1] := 'ADDTL % STATE';
      OFields[2] := 'VOL STATE';
      OFields[3] := 'VOL % STATE';
    end
    else
    begin
      ds := FImpEngine.IDS('EE');
      sField := 'OVERRIDE_FED_TAX_';
      OFields[0] := 'ADDTL FED';
      OFields[1] := 'ADDTL % FED';
      OFields[2] := 'VOL FED';
      OFields[3] := 'VOL % FED';
    end;
    OValues[0] := 0;
    OValues[1] := 0;
    OValues[2] := 0;
    OValues[3] := 0;
    FImpEngine.CurrentDataRequired(ds, KeyFields[0] + ' = ''' + VarToStr(KeyValues[0]) + '''');
    if ds.FieldByName(sField + 'TYPE').AsString = OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT then
      OValues[0] := ds.FieldByName(sField + 'VALUE').Value
    else if ds.FieldByName(sField + 'TYPE').AsString = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT then
      OValues[1] := ds.FieldByName(sField + 'VALUE').Value
    else if ds.FieldByName(sField + 'TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT then
      OValues[2] := ds.FieldByName(sField + 'VALUE').Value
    else if ds.FieldByName(sField + 'TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT then
      OValues[3] := ds.FieldByName(sField + 'VALUE').Value;
  end;
end;

function THRTraxDefinition.IEActiveWeeks(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  s: string;

  procedure InvertValue(var Value: string);
  var
    i: Integer;
  begin
    for i := 1 to Length(Value) do
      if Value[i] = GROUP_BOX_YES then
        Value[i] := GROUP_BOX_NO
      else
        Value[i] := GROUP_BOX_YES
  end;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 5);
    SetLength(OFields, 5);
    SetLength(OValues, 5);
    s := VarToStr(Value);
    InvertValue(s);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'EXCLUDE_WEEK_1';
    OValues[0] := Copy(s, 1, 1);
    OTables[1] := 'EE_SCHEDULED_E_DS';
    OFields[1] := 'EXCLUDE_WEEK_2';
    OValues[1] := Copy(s, 2, 1);
    OTables[2] := 'EE_SCHEDULED_E_DS';
    OFields[2] := 'EXCLUDE_WEEK_3';
    OValues[2] := Copy(s, 3, 1);
    OTables[3] := 'EE_SCHEDULED_E_DS';
    OFields[3] := 'EXCLUDE_WEEK_4';
    OValues[3] := Copy(s, 4, 1);
    OTables[4] := 'EE_SCHEDULED_E_DS';
    OFields[4] := 'EXCLUDE_WEEK_5';
    OValues[4] := Copy(s, 5, 1);
  end
  else
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_SCHEDULED_E_DS'), 'EE_SCHEDULED_E_DS_NBR = ''' + VarToStr(KeyValues[0]) + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'ED ACTIVE WEEK';
    s := FImpEngine.IDS('EE_SCHEDULED_E_DS')['EXCLUDE_WEEK_1'] +
         FImpEngine.IDS('EE_SCHEDULED_E_DS')['EXCLUDE_WEEK_2'] +
         FImpEngine.IDS('EE_SCHEDULED_E_DS')['EXCLUDE_WEEK_3'] +
         FImpEngine.IDS('EE_SCHEDULED_E_DS')['EXCLUDE_WEEK_4'] +
         FImpEngine.IDS('EE_SCHEDULED_E_DS')['EXCLUDE_WEEK_5'];
    InvertValue(s);
    OValues[0] := s
  end;
end;

procedure THRTraxDefinition.AI_EE_RATES(const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray);
var
  i: Integer;
begin
  for i := 0 to High(KeyFields) do
    if KeyFields[i] = 'PRIMARY_RATE' then
    begin
      ds['RATE_NUMBER'] := 1;
      Break;
    end
    else if KeyFields[i] = 'RATE_NUMBER' then
    begin
      if KeyValues[i] = '1' then
        ds['PRIMARY_RATE'] := GROUP_BOX_YES;
      Break;
    end;
end;

function THRTraxDefinition.IEEDTarget(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'TARGET_AMOUNT';
    OValues[0] := Value;
    OTables[1] := 'EE_SCHEDULED_E_DS';
    OFields[1] := 'TARGET_ACTION';
    OValues[1] := SCHED_ED_TARGET_REMOVE;
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'ED TARGET';
    OValues[0] := Value;
  end;
end;

procedure THRTraxDefinition.AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray);
var
  i: Integer;
begin
  for i := 0 to High(KeyFields) do
    if KeyFields[i] = 'CUSTOM_E_D_CODE_NUMBER' then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS(KeyTables[i]), KeyFields[i] + '=''' + KeyValues[i] + '''');
      ds['CL_E_D_GROUPS_NBR'] := FImpEngine.IDS(KeyTables[i])['CL_E_D_GROUPS_NBR'];
      ds['CL_AGENCY_NBR'] := FImpEngine.IDS(KeyTables[i])['DEFAULT_CL_AGENCY_NBR'];
      Break;
    end
end;

function THRTraxDefinition.HomeTax(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'CO_STATES_NBR';
    if FImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value]), [loCaseInsensitive]) then
      OValues[0] := FImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
      OValues[0] := 0;
    OTables[1] := 'EE';
    OFields[1] := 'HOME_TAX_EE_STATES_NBR';
    OValues[1] := VarArrayOf(['EE_STATES', 'CO_STATES_NBR', OValues[0], 'EE_STATES_NBR']);
    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'CO_STATES';
    KeyFields[High(KeyFields)] := 'CO_STATES_NBR';
    KeyValues[High(KeyValues)] := OValues[0];
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'SIT TAX CODE';
    OValues[0] := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', Value, 'STATE');
  end;
end;

function THRTraxDefinition.HomeSUI(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'SUI_APPLY_CO_STATES_NBR';
    if FImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value]), [loCaseInsensitive]) then
      OValues[0] := FImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
      OValues[0] := 0;

    if Length(KeyTables) = 2 then
    begin
      SetLength(KeyTables, Succ(Length(KeyTables)));
      SetLength(KeyFields, Succ(Length(KeyFields)));
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyTables[High(KeyTables)] := 'CO_STATES';
      KeyFields[High(KeyFields)] := 'CO_STATES_NBR';
      KeyValues[High(KeyValues)] := OValues[0];
    end;
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'SUI TAX CODE';
    OValues[0] := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', Value, 'STATE');
  end;
end;

function THRTraxDefinition.RapidMaritalStatus(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  State, Status, RapidCode, Local, Dep: String;
  s: string;
begin
  s := Value;
  State := GetNextStrValue(s, ';');
  Status := GetNextStrValue(s, ';');
  RapidCode := GetNextStrValue(s, ';');
  Local := GetNextStrValue(s, ';');
  Dep := GetNextStrValue(s, ';');
  if Dep = '' then
    Dep := '0';
  SetLength(KeyTables, Succ(Length(KeyTables)));
  SetLength(KeyFields, Succ(Length(KeyFields)));
  SetLength(KeyValues, Succ(Length(KeyValues)));
  KeyTables[High(KeyTables)] := 'CO_STATES';
  KeyFields[High(KeyFields)] := 'STATE';
  KeyValues[High(KeyValues)] := State;
  SetLength(OTables, 2);
  SetLength(OFields, 2);
  SetLength(OValues, 2);
  OTables[0] := 'EE_STATES';
  OFields[0] := 'STATE_MARITAL_STATUS';
  OValues[0] := FindMaritalStatus(State, Status, RapidCode, Local, StrToInt(Dep));

  //TS: reso #40631 Next 5 lines were added to get around the SY_STATE_Marital_status_nbr NULL Error
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = '''+ VarToStr(state)+'''');
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
  OTables[1] := 'EE_STATES';
  OFields[1] := 'SY_STATE_MARITAL_STATUS_NBR';
  OValues[1] := FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Lookup('STATUS_TYPE', OValues[0], 'SY_STATE_MARITAL_STATUS_NBR');

  Result := not (VarIsNull(OValues[0])) and (VarIsNull(OValues[1]));
end;

function THRTraxDefinition.HomeSDI(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'SDI_APPLY_CO_STATES_NBR';
    if FImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value]), [loCaseInsensitive]) then
      OValues[0] := FImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
      OValues[0] := 0;
    if Length(KeyTables) = 2 then
    begin
      SetLength(KeyTables, Succ(Length(KeyTables)));
      SetLength(KeyFields, Succ(Length(KeyFields)));
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyTables[High(KeyTables)] := 'CO_STATES';
      KeyFields[High(KeyFields)] := 'CO_STATES_NBR';
      KeyValues[High(KeyValues)] := OValues[0];
    end;
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'SDI TAX CODE';
    OValues[0] := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', Value, 'STATE');
  end;
end;

function THRTraxDefinition.IECL_PERSON_STATE(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'));
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'CL_PERSON';
    OFields[0] := 'STATE';
    OValues[0] := Value;
    OTables[1] := 'CL_PERSON';
    OFields[1] := 'RESIDENTIAL_STATE_NBR';
    OValues[1] := FImpEngine.IDS('SY_STATES').Lookup('STATE', Value, 'SY_STATES_NBR');
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'STATE';
    OValues[0] := Value;
  end;
end;

procedure THRTraxDefinition.AI_EE_WORK_SHIFTS(const ds: TevClientDataSet;
  const KeyTables, KeyFields: TStringArray;
  const KeyValues: TVariantArray);
begin
  ds['SHIFT_RATE'] := FImpEngine.IDS('CO_SHIFTS')['DEFAULT_AMOUNT'];
  ds['SHIFT_PERCENTAGE'] := FImpEngine.IDS('CO_SHIFTS')['DEFAULT_PERCENTAGE'];
end;

function THRTraxDefinition.EEShift(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_SHIFTS'));
  if IsImport then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'EE_WORK_SHIFTS';
    OFields[0] := 'CO_SHIFTS_NBR';
    OTables[1] := 'EE';
    OFields[1] := 'AUTOPAY_CO_SHIFTS_NBR';
    if FImpEngine.IDS('CO_SHIFTS').Locate('CO_NBR;NAME', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value]), [{loCaseInsensitive}]) then
    begin
      OValues[0] := FImpEngine.IDS('CO_SHIFTS')['CO_SHIFTS_NBR'];
      OValues[1] := FImpEngine.IDS('CO_SHIFTS')['CO_SHIFTS_NBR'];
    end
    else
    begin
      OValues[0] := null;
      OValues[1] := null;
    end;
//    SetLength(KeyTables, Succ(Length(KeyTables)));
//    SetLength(KeyFields, Succ(Length(KeyFields)));
//    SetLength(KeyValues, Succ(Length(KeyValues)));
//    KeyTables[High(KeyTables)] := 'CO_SHIFTS';
//    KeyFields[High(KeyFields)] := 'CO_SHIFTS_NBR';
//    KeyValues[High(KeyValues)] := OValues[0];
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'SHIFT DIFF';
    OValues[0] := FImpEngine.IDS('CO_SHIFTS').Lookup('CO_SHIFTS_NBR', Value, 'NAME');
  end;
end;

procedure THRTraxDefinition.AI_EE_HR_ATTENDANCE(const ds: TevClientDataSet;
  const KeyTables, KeyFields: TStringArray;
  const KeyValues: TVariantArray);
begin
  ds['ATTENDANCE_TYPE_NUMBER'] := 1;
end;

function THRTraxDefinition.ValidateMaritalStatus(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  i: Integer;
  State: Variant;
  Bool1, Bool2: Boolean;
begin
  if IsImport then
  begin
    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'CO_STATES';
    KeyFields[High(KeyFields)] := 'STATE';

    State := Null;
    for i:=0 to High(KeyValues) do
      if (KeyTables[i] = 'CO_STATES') and (KeyFields[i] = 'STATE') then
      begin
        State := KeyValues[i];
        break;
      end;

    Assert( not VarIsNull(State) );

    KeyValues[High(KeyValues)] := State;
    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'STATE_MARITAL_STATUS';
    OValues[0] := Value;

    //TS: reso #40631 Next 5 lines were added to get around the SY_STATE_Marital_status_nbr NULL Error
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = '''+ VarToStr(state)+'''');
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
    OTables[1] := 'EE_STATES';
    OFields[1] := 'SY_STATE_MARITAL_STATUS_NBR';
    OValues[1] := FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Lookup('STATUS_TYPE', OValues[0], 'SY_STATE_MARITAL_STATUS_NBR');

    Bool1 := VarToStr(OValues[0]) <> '';
    Bool2 := VarToStr(OValues[1]) <> '';
    Result := Bool1 and Bool2;
  end
  else
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := '';
    OFields[0] := 'STATE MARITAL';
    OValues[0] := Value;
    Result := True;
  end;
end;

constructor THRTraxDefinition.Create(const AIEMap: IIEMap);
begin
  inherited Create;

  FImpEngine := AIEMap;
end;

end.

