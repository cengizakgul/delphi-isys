// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DBDTChoiceQuery;
// Usage:
//   Create
//   SetFilter
//   ShowModal
//   Free

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_SafeChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure, 
  SPD_ChoiceFromListQueryEx, ActnList, SPD_DBDTSelectionListFiltr,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDataDictclient, ISDataAccessComponents, EvDataAccessComponents, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TDBDTChoiceQuery = class(TSafeChoiceFromListQuery)
    DM_COMPANY: TDM_COMPANY;
    cdsChoiceListDIVISION_NAME: TStringField;
    cdsChoiceListBRANCH_NAME: TStringField;
    cdsChoiceListDEPARTMENT_NAME: TStringField;
    cdsChoiceListTEAM_NAME: TStringField;
    cdsChoiceListDBDT_CUSTOM_NUMBERS: TStringField;
    cdsChoiceListBRANCH_NBR: TIntegerField;
    cdsChoiceListDEPARTMENT_NBR: TIntegerField;
    cdsChoiceListTEAM_NBR: TIntegerField;
    cdsChoiceListDIVISION_NBR: TIntegerField;
  private
    { Private declarations }
//    temp: TevClientDataSet;
  public
    { Public declarations }
    procedure SetFilter( level: char; flt: string; reset:boolean= true);
    destructor Destroy; override;

  end;


implementation

uses
  evutils, EvConsts;
{$R *.DFM}

{ TDBDTChoiceQuery }


destructor TDBDTChoiceQuery.Destroy;
begin
//  FreeAndNil( temp );
  inherited;

end;

procedure TDBDTChoiceQuery.SetFilter( level: char; flt: string; reset:boolean= true);
var
  linestodel: integer;
  i: integer;
begin
  dgChoiceList.DataSource := nil;

  if reset then
  begin
    DM_COMPANY.CO_DEPARTMENT.EnsureDSCondition('');
    DM_COMPANY.CO_TEAM.EnsureDSCondition('');
    SetupDBDT_DS( cdsChoiceList, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM, level);
  end
  else // Copy Address To - it's allowed display all DBDT regardless display option
    SetupDBDT_DS( cdsChoiceList, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM, level, True);

  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
  dsChoiceList.DataSet := cdsChoiceList;


  linestodel := 0;
  if level = CLIENT_LEVEL_DIVISION then
    linestodel := 3
  else if level = CLIENT_LEVEL_BRANCH then
    linestodel := 2
  else if level = CLIENT_LEVEL_DEPT then
    linestodel := 1;
  for i := 1 to linestodel do
    dgChoiceList.Selected.Delete(4-i);

  dgChoiceList.DataSource := dsChoiceList;

end;

end.
