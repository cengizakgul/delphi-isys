inherited EDIT_PR_BASE: TEDIT_PR_BASE
  inherited PageControl1: TevPageControl [0]
    TabOrder = 0
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited Panel3: TevPanel
            inherited BitBtn1: TevBitBtn
              Left = 5
              Width = 121
              Caption = 'Open company'
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'FEIN'#9'9'#9'Fein'#9)
                IniAttributes.SectionName = 'TEDIT_PR_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Payroll'
              object PRGrid: TevDBGrid
                Left = 18
                Top = 48
                Width = 39
                Height = 9
                DisableThemesInTitle = False
                Selected.Strings = (
                  'RUN_NUMBER'#9'5'#9'Run Number'#9
                  'CHECK_DATE'#9'10'#9'Check Date'#9
                  'SCHEDULED'#9'1'#9'Scheduled'#9
                  'PAYROLL_TYPE'#9'1'#9'Type'#9
                  'STATUS'#9'1'#9'Status'#9
                  'EXCLUDE_ACH'#9'1'#9'Exclude Ach'#9
                  'EXCLUDE_TAX_DEPOSITS'#9'1'#9'Exclude Tax Deposits'#9
                  'EXCLUDE_AGENCY'#9'1'#9'Exclude Agency'#9
                  'MARK_LIABS_PAID_DEFAULT'#9'1'#9'Mark Liabilities as Paid'#9
                  'EXCLUDE_BILLING'#9'1'#9'Exclude Billing'#9
                  'EXCLUDE_R_C_B_0R_N'#9'1'#9'Exclude R C B 0r N'#9
                  'SCHEDULED_CALL_IN_DATE'#9'10'#9'Scheduled Call In Date'#9
                  'ACTUAL_CALL_IN_DATE'#9'10'#9'Actual Call In Date'#9
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9
                  'SCHEDULED_DELIVERY_DATE'#9'10'#9'Scheduled Delivery Date'#9
                  'SCHEDULED_CHECK_DATE'#9'10'#9'Scheduled Check Date'#9)
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_PR_BASE\PRGrid'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                MultiSelectOptions = [msoAutoUnselect]
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnDblClick = PRGridDblClick
                OnKeyDown = PRGridKeyDown
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
  end
  inherited Panel1: TevPanel [1]
    TabOrder = 1
    object lablCompany: TevLabel [0]
      Left = 8
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Company'
    end
    object dbtxCompanyNumber: TevDBText [1]
      Left = 64
      Top = 16
      Width = 65
      Height = 17
      DataField = 'CUSTOM_COMPANY_NUMBER'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CompanyNameText: TevDBText [2]
      Left = 136
      Top = 16
      Width = 241
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lablClient: TevLabel [3]
      Left = 8
      Top = 0
      Width = 26
      Height = 13
      Caption = 'Client'
    end
    object dbtxClientNbr: TevDBText [4]
      Left = 64
      Top = 0
      Width = 65
      Height = 15
      DataField = 'CUSTOM_CLIENT_NUMBER'
      DataSource = dsCL
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dbtxClientName: TevDBText [5]
      Left = 136
      Top = 0
      Width = 241
      Height = 15
      DataField = 'NAME'
      DataSource = dsCL
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txCheckDate: TevDBText [6]
      Left = 510
      Top = 16
      Width = 113
      Height = 17
      DataField = 'CHECK_DATE'
      DataSource = wwdsSubMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object txRunNumber: TevDBText [7]
      Left = 430
      Top = 16
      Width = 67
      Height = 17
      Alignment = taCenter
      DataField = 'RUN_NUMBER'
      DataSource = wwdsSubMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel1: TevLabel [8]
      Left = 389
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Payroll'
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_CO.CO
    Left = 136
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster
    Left = 190
    Top = 34
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Top = 34
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 297
    Top = 39
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 255
    Top = 39
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 336
    Top = 39
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 378
    Top = 42
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_PR.PR
    Left = 174
    Top = 48
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 423
    Top = 42
  end
  object ActionList: TevActionList
    Left = 379
    Top = 75
    object PRNext: TDataSetNext
      Category = 'Dataset'
      Caption = '&Next'
      Hint = 'Next'
      ImageIndex = 2
      ShortCut = 107
      OnUpdate = PRNextUpdate
      DataSource = wwdsSubMaster
    end
    object PRPrior: TDataSetPrior
      Category = 'Dataset'
      Caption = '&Prior'
      Hint = 'Prior'
      ImageIndex = 1
      ShortCut = 106
      OnUpdate = PRNextUpdate
      DataSource = wwdsSubMaster
    end
  end
end
