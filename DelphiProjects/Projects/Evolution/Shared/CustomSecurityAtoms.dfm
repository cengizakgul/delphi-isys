object dmCustomSecAtoms: TdmCustomSecAtoms
  OldCreateOrder = False
  Left = 406
  Top = 168
  Height = 517
  Width = 1131
  object eTaxExemptions: TevSecElement
    AtomComponent = aTaxExemptions
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 141
    Top = 10
  end
  object aTaxExemptions: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    AtomCaption = 'Ability to change tax exemptions'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 10
  end
  object eUnlockCreditHold: TevSecElement
    AtomComponent = aUnlockCreditHold
    ElementType = otFunction
    ElementTag = 'UNLOCK_CREDIT_HOLD'
    Left = 144
    Top = 67
  end
  object aUnlockCreditHold: TevSecAtom
    AtomType = 'F'
    AtomTag = 'UNLOCK_CREDIT_HOLD'
    AtomCaption = 'Ability to unlock a company with a "high" credit hold'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 48
    Top = 67
  end
  object aDeletePr: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_DELETE_PAYROLL'
    AtomCaption = 'Ability to Delete Processed Payrolls'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 48
    Top = 128
  end
  object eDeletePr: TevSecElement
    AtomComponent = aDeletePr
    ElementType = otFunction
    ElementTag = 'USER_CAN_DELETE_PAYROLL'
    Left = 144
    Top = 128
  end
  object eAdjustShortFalls: TevSecElement
    AtomComponent = aAdjustShortFalls
    ElementType = otFunction
    ElementTag = 'SHORTFALL_ADJUSTMENTS'
    Left = 141
    Top = 186
  end
  object aAdjustShortFalls: TevSecAtom
    AtomType = 'F'
    AtomTag = 'SHORTFALL_ADJUSTMENTS'
    AtomCaption = 'Ability to adjust shortfalls'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 186
  end
  object eAdjustTaxes: TevSecElement
    AtomComponent = aAdjustTaxes
    ElementType = otFunction
    ElementTag = 'USER_CAN_ADJUST_TAXES'
    Left = 141
    Top = 242
  end
  object aAdjustTaxes: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_ADJUST_TAXES'
    AtomCaption = 'Ability to adjust taxes'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 242
  end
  object eVoidPayroll: TevSecElement
    AtomComponent = aVoidPayroll
    ElementType = otFunction
    ElementTag = 'USER_CAN_VOID_PAYROLL'
    Left = 141
    Top = 362
  end
  object aVoidPayroll: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_VOID_PAYROLL'
    AtomCaption = 'Ability to void payroll'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 362
  end
  object aCopyPayroll: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_COPY_PAYROLL'
    AtomCaption = 'Ability to copy payroll'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 306
  end
  object eCopyPayroll: TevSecElement
    AtomComponent = aCopyPayroll
    ElementType = otFunction
    ElementTag = 'USER_CAN_COPY_PAYROLL'
    Left = 141
    Top = 306
  end
  object aPayrollRegister: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_REGISTER'
    AtomCaption = 'Ability to edit payroll bank account register'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 418
  end
  object ePayrollRegister: TevSecElement
    AtomComponent = aPayrollRegister
    ElementType = otFunction
    ElementTag = 'USER_CAN_REGISTER'
    Left = 141
    Top = 418
  end
  object ePrCreationWizard: TevSecElement
    AtomComponent = aPrCreationWizard
    ElementType = otFunction
    ElementTag = 'PR_CREATION_WIZARD2'
    Left = 384
    Top = 16
  end
  object aPrCreationWizard: TevSecAtom
    AtomType = 'F'
    AtomTag = 'PR_CREATION_WIZARD2'
    AtomCaption = 
      'Payroll wizard including deposit/liability, billing, and ACH blo' +
      'ck options'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 272
    Top = 16
  end
  object aEditRwDict: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ABILITY_EDIT_DD'
    AtomCaption = 'Ability to edit data dictionary'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 68
  end
  object eEditRwDict: TevSecElement
    AtomComponent = aEditRwDict
    ElementType = otFunction
    ElementTag = 'ABILITY_EDIT_DD'
    Left = 381
    Top = 68
  end
  object eSeeSystemRwDict: TevSecElement
    AtomComponent = aSeeSystemRwDict
    ElementType = otFunction
    ElementTag = 'ABILITY_SEE_SY'
    Left = 381
    Top = 180
  end
  object aSeeSystemRwDict: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ABILITY_SEE_SY'
    AtomCaption = 'Ability to see hidden tables'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 180
  end
  object aUnvoidVoidedCheck: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ABILITY_UNVOID_CHECK'
    AtomCaption = 'Ability to unvoid voided check'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 233
  end
  object eUnvoidVoidedCheck: TevSecElement
    AtomComponent = aUnvoidVoidedCheck
    ElementType = otFunction
    ElementTag = 'ABILITY_UNVOID_CHECK'
    Left = 381
    Top = 233
  end
  object aEditRwLib: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ABILITY_EDIT_RWLIB'
    AtomCaption = 'Ability to edit RW library'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 124
  end
  object eEditRwLib: TevSecElement
    AtomComponent = aEditRwLib
    ElementType = otFunction
    ElementTag = 'ABILITY_EDIT_RWLIB'
    Left = 381
    Top = 124
  end
  object aPayLiabWoFunds: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ABILITY_PAY_WO_FUNDS'
    AtomCaption = 'Ability to pay liabilities w/o funds'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 289
  end
  object ePayLiabWoFunds: TevSecElement
    AtomComponent = aPayLiabWoFunds
    ElementType = otFunction
    ElementTag = 'ABILITY_PAY_WO_FUNDS'
    Left = 381
    Top = 289
  end
  object aOfflineClient: TevSecAtom
    AtomType = 'F'
    AtomTag = 'OFFLINE_CLIENT'
    AtomCaption = 'Ability to use offline client'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 345
  end
  object eOfflineClient: TevSecElement
    AtomComponent = aOfflineClient
    ElementType = otFunction
    ElementTag = 'OFFLINE_CLIENT'
    Left = 381
    Top = 345
  end
  object aManualOasdiMedicare: TevSecAtom
    AtomType = 'F'
    AtomTag = 'MANUAL_OASDI_MEDICARE'
    AtomCaption = 'Ability to manually correct OASDI & Medicare'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 401
  end
  object eManualOasdiMedicare: TevSecElement
    AtomComponent = aManualOasdiMedicare
    ElementType = otFunction
    ElementTag = 'MANUAL_OASDI_MEDICARE'
    Left = 381
    Top = 401
  end
  object aReturnDelete: TevSecAtom
    AtomType = 'F'
    AtomTag = 'RETURN_DELETE'
    AtomCaption = 'Ability to delete/alternate processed tax returns'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 457
  end
  object eReturnDelete: TevSecElement
    AtomComponent = aReturnDelete
    ElementType = otFunction
    ElementTag = 'RETURN_DELETE'
    Left = 381
    Top = 457
  end
  object aUpdateAsOf: TevSecAtom
    AtomType = 'F'
    AtomTag = 'UPDATE_AS_OF'
    AtomCaption = 'Ability to update as of another date'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 269
    Top = 511
  end
  object eUpdateAsOf: TevSecElement
    AtomComponent = aUpdateAsOf
    ElementType = otFunction
    ElementTag = 'UPDATE_AS_OF'
    Left = 381
    Top = 511
  end
  object aAdminQueue: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ALLOW_ADMIN_QUEUE'
    AtomCaption = 'Ability to administrate queue'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 537
  end
  object eAdminQueue: TevSecElement
    AtomComponent = aAdminQueue
    ElementType = otFunction
    ElementTag = 'ALLOW_ADMIN_QUEUE'
    Left = 141
    Top = 533
  end
  object eaPrintUnprocessedChecks: TevSecElement
    AtomComponent = aPrintUnprocessedChecks
    ElementType = otFunction
    ElementTag = 'USER_CAN_PRINT_UNPROCESSED_CHECKS'
    Left = 656
    Top = 8
  end
  object aPrintUnprocessedChecks: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_PRINT_UNPROCESSED_CHECKS'
    AtomCaption = 'Ability to print unprocessed manual checks'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 8
  end
  object aReprintProcessedChecks: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_REPRINT_PROCESSED_CHECKS'
    AtomCaption = 'Ability to reprint processed payroll checks'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 56
  end
  object eaReprintProcessedChecks: TevSecElement
    AtomComponent = aReprintProcessedChecks
    ElementType = otFunction
    ElementTag = 'USER_CAN_REPRINT_PROCESSED_CHECKS'
    Left = 632
    Top = 56
  end
  object aReprintPayrollReports: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_REPRINT_PAYROLL_REPORTS'
    AtomCaption = 'Ability to reprint payroll reports'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 104
  end
  object eaReprintPayrollReports: TevSecElement
    AtomComponent = aReprintPayrollReports
    ElementType = otFunction
    ElementTag = 'USER_CAN_REPRINT_PAYROLL_REPORTS'
    Left = 632
    Top = 104
  end
  object aReprintProcessedMiscChecks: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_REPRINT_MISC_CHECKS'
    AtomCaption = 'Ability to reprint processed misc checks'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 152
  end
  object eaReprintProcessedMiscChecks: TevSecElement
    AtomComponent = aReprintProcessedMiscChecks
    ElementType = otFunction
    ElementTag = 'USER_CAN_REPRINT_MISC_CHECKS'
    Left = 632
    Top = 152
  end
  object aCreateBackdatedPayrolls: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_CREATE_BACKDATED_PAYROLLS'
    AtomCaption = 'Ability to create backdated payrolls'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 200
  end
  object eaCreateBackdatedPayrolls: TevSecElement
    AtomComponent = aCreateBackdatedPayrolls
    ElementType = otFunction
    ElementTag = 'USER_CAN_CREATE_BACKDATED_PAYROLLS'
    Left = 632
    Top = 200
  end
  object aRollbackTaxPayments: TevSecAtom
    AtomType = 'F'
    AtomTag = 'ROLLBACK_TAX_PAYMENTS'
    AtomCaption = 'Ability to rollback tax payments'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 248
  end
  object eRollbackTaxPayments: TevSecElement
    AtomComponent = aRollbackTaxPayments
    ElementType = otFunction
    ElementTag = 'ROLLBACK_TAX_PAYMENTS'
    Left = 632
    Top = 248
  end
  object aChangeQuarterLockDate: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_CHANGE_QUARTER_LOCK_DATE'
    AtomCaption = 'Ability to change quarter lock date'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 304
  end
  object eaChangeQuarterLockDate: TevSecElement
    AtomComponent = aChangeQuarterLockDate
    ElementType = otFunction
    ElementTag = 'USER_CAN_CHANGE_QUARTER_LOCK_DATE'
    Left = 632
    Top = 304
  end
  object aUndeleteRecord: TevSecAtom
    AtomType = 'F'
    AtomTag = 'UNDELETE_RECORD'
    AtomCaption = 'Ability to undelete records'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 523
    Top = 359
  end
  object eUndeleteRecord: TevSecElement
    AtomComponent = aUndeleteRecord
    ElementType = otFunction
    ElementTag = 'UNDELETE_RECORD'
    Left = 635
    Top = 359
  end
  object aVMR: TevSecAtom
    AtomType = 'F'
    AtomTag = 'VMR'
    AtomCaption = 'Ability to access VMR'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 523
    Top = 407
  end
  object eVMR: TevSecElement
    AtomComponent = aVMR
    ElementType = otFunction
    ElementTag = 'VMR'
    Left = 635
    Top = 407
  end
  object aAbilitySendPayrollDirectly: TevSecAtom
    AtomType = 'F'
    AtomTag = 'SEND_PAYROLL_DIRECTLY'
    AtomCaption = 'Ability to directly process payrolls'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 523
    Top = 471
  end
  object eAbilitySendPayrollDirectly: TevSecElement
    AtomComponent = aAbilitySendPayrollDirectly
    ElementType = otFunction
    ElementTag = 'SEND_PAYROLL_DIRECTLY'
    Left = 635
    Top = 463
  end
  object aMarkPayrollCompleted: TevSecAtom
    AtomType = 'F'
    AtomTag = 'MARK_PAYROLL_COMPLETED'
    AtomCaption = 'Ability to mark payrolls as completed'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 523
    Top = 511
  end
  object eMarkPayrollCompleted: TevSecElement
    AtomComponent = aMarkPayrollCompleted
    ElementType = otFunction
    ElementTag = 'MARK_PAYROLL_COMPLETED'
    Left = 635
    Top = 511
  end
  object eEeHeDocuments: TevSecElement
    AtomComponent = aEeHeDocuments
    ElementType = otFunction
    ElementTag = 'USER_CAN_ACCESS_HR_DOCUMENTS'
    Left = 141
    Top = 594
  end
  object aEeHeDocuments: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_ACCESS_HR_DOCUMENTS'
    AtomCaption = 'Ability to see and edit EE HR documents'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 594
  end
  object aDeleteClient: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_DELETE_CLIENT'
    AtomCaption = 'Ability to delete client'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 255
      end>
    Left = 271
    Top = 562
  end
  object eDeleteClient: TevSecElement
    AtomComponent = aDeleteClient
    ElementType = otFunction
    ElementTag = 'USER_CAN_DELETE_CLIENT'
    Left = 381
    Top = 562
  end
  object aCoBankAccounts: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_EDIT_CO_BANK_ACCOUNTS'
    AtomCaption = 'Access to Company Cash Management'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 255
      end>
    Left = 520
    Top = 560
  end
  object eCoBankAccounts: TevSecElement
    AtomComponent = aCoBankAccounts
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_CO_BANK_ACCOUNTS'
    Left = 632
    Top = 560
  end
  object aSubmitPayroll: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_SUBMIT_PAYROLL'
    AtomCaption = 'Ability to submit payroll'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 45
    Top = 480
  end
  object eSubmitPayroll: TevSecElement
    AtomComponent = aSubmitPayroll
    ElementType = otFunction
    ElementTag = 'USER_CAN_SUBMIT_PAYROLL'
    Left = 140
    Top = 480
  end
  object aVoidPrevQtrCheck: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_VOID_PREV_QTR_CHECK'
    AtomCaption = 'Ability to void check from previous quarter'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 270
    Top = 610
  end
  object eVoidPrevQtrCheck: TevSecElement
    AtomComponent = aVoidPrevQtrCheck
    ElementType = otFunction
    ElementTag = 'USER_CAN_VOID_PREV_QTR_CHECK'
    Left = 385
    Top = 610
  end
  object aVoidCheck: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_VOID_CHECK'
    AtomCaption = 'Ability to void checks'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 612
  end
  object eVoidCheck: TevSecElement
    AtomComponent = aVoidCheck
    ElementType = otFunction
    ElementTag = 'USER_CAN_VOID_CHECK'
    Left = 631
    Top = 612
  end
  object aTimeClockImport: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_AUTO_CREATE_CHECKS_TIME_CLOCK'
    AtomCaption = 'Auto Create Checks including Termed EEs'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 16
  end
  object eTimeClockImport: TevSecElement
    AtomComponent = aTimeClockImport
    ElementType = otFunction
    ElementTag = 'USER_AUTO_CREATE_CHECKS_TIME_CLOCK'
    Left = 888
    Top = 16
  end
  object aCreateManualACH: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_CREATE_MANUAL_ACH'
    AtomCaption = 'Ability to create Manual ACH'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 80
  end
  object aAllowEvoXImport: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_IMPORT_USING_EVOX'
    AtomCaption = 'Ability to import using EvoX'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 144
  end
  object aCreateEvoXMap: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_CREATE_EDIT_EVOX_MAP'
    AtomCaption = 'Ability to create and edit EvoX map files'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 208
  end
  object aAllowEvoXExport: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_EXPORT_USING_EVOX'
    AtomCaption = 'Ability to export using EvoX'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 784
    Top = 272
  end
  object aChangeSecuredFlagInReports: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_UNMASK_REPORT_DATA'
    AtomCaption = 'Ability to unmask sensitive data on reports'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 784
    Top = 328
  end
  object aAccessProcessedreportsInWebClient: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_ACCESS_PROCESSEDREPORTS_WEB'
    AtomCaption = 'Ability to access processed reports in webclient'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end
      item
        Tag = 'R'
        Caption = 'Readonly'
        IconName = 'TREESCREENSREADONLY'
        Priority = 124
      end>
    Left = 784
    Top = 392
  end
  object eBlockChangingOfSSN: TevSecElement
    AtomComponent = aBlockChangingOfSSN
    ElementType = otFunction
    ElementTag = 'BLOCK_CHANGING_OF_SSN'
    Left = 917
    Top = 490
  end
  object aBlockChangingOfSSN: TevSecAtom
    AtomType = 'F'
    AtomTag = 'BLOCK_CHANGING_OF_SSN'
    AtomCaption = 'Block changing of SSN'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 789
    Top = 490
  end
  object aCLReadOnlyMode: TevSecAtom
    AtomType = 'F'
    AtomTag = 'UNLOCK_CL_READ_ONLY_MODE'
    AtomCaption = 'Ability to put a Client On or Off Read Only Mode'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 880
    Top = 83
  end
  object eCLReadOnlyMode: TevSecElement
    AtomComponent = aCLReadOnlyMode
    ElementType = otFunction
    ElementTag = 'UNLOCK_CL_READ_ONLY_MODE'
    Left = 984
    Top = 83
  end
  object aApproveBenefitsEnrollment: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_APPROVE_BENEFITS_ENROLLMENT'
    AtomCaption = 'Ability to approve benefits enrollment'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 608
  end
  object aDefineBenefitsEnrollment: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_DEFINE_BENEFITS_ENROLLMENT'
    AtomCaption = 'Ability to define benefits enrollment'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 552
  end
  object aGloballyUpdatePayrates: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_GLOBALLY_UPDATE_PAY_RATES'
    AtomCaption = 'Ability to globally update pay rates'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 776
    Top = 664
  end
  object aAccessDeleteVMRMailBoxes: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_ACCESS_DELETEVMRMAILBOX'
    AtomCaption = 'Ability to delete VMR Mail Boxes'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 928
    Top = 352
  end
  object aSwipeClockLogin: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_LOGIN_SWIPECLOCK'
    AtomCaption = 'Ability to login into EvoClock from Evolution'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 32
    Top = 672
  end
  object aMRCSignOn: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_LOGIN_MY_RECRUITING_CENTER'
    AtomCaption = 'Access to evoMRC'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 136
    Top = 672
  end
  object aDisplayUsername: TevSecAtom
    AtomType = 'F'
    AtomTag = 'DISPLAY_USERNAME_IN_AUDIT'
    AtomCaption = 'Display User Name in Audit'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 520
    Top = 660
  end
  object eDisplayUsername: TevSecElement
    AtomComponent = aDisplayUsername
    ElementType = otFunction
    ElementTag = 'DISPLAY_USERNAME_IN_AUDIT'
    Left = 631
    Top = 660
  end
  object eApplyEEQuickEntry: TevSecElement
    AtomComponent = aApplyEEQuickEntry
    ElementType = otFunction
    ElementTag = 'APPLY_EE_QUICK_ENTRY_IN_WEBCLIENT'
    Left = 399
    Top = 716
  end
  object aApplyEEQuickEntry: TevSecAtom
    AtomType = 'F'
    AtomTag = 'APPLY_EE_QUICK_ENTRY_IN_WEBCLIENT'
    AtomCaption = 'Apply EE Quick Entry in WebClient'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 288
    Top = 716
  end
  object aBlockVoidingChecks: TevSecAtom
    AtomType = 'F'
    AtomTag = 'BLOCK_VOIDING_CHECKS'
    AtomCaption = 'Block voiding checks with direct deposit'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 272
    Top = 668
  end
  object eBlockVoidingChecks: TevSecElement
    AtomComponent = aBlockVoidingChecks
    ElementType = otFunction
    ElementTag = 'BLOCK_VOIDING_CHECKS'
    Left = 383
    Top = 668
  end
  object aSetEffectivePeriodForLocals: TevSecAtom
    AtomType = 'F'
    AtomTag = 'SET_EFFECTIVE_PERIOD_FOR_LOCALS'
    AtomCaption = 'Ability to set effective period for locals'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 16
    Top = 732
  end
  object eSetEffectivePeriodForLocals: TevSecElement
    AtomComponent = aSetEffectivePeriodForLocals
    ElementType = otFunction
    ElementTag = 'SET_EFFECTIVE_PERIOD_FOR_LOCALS'
    Left = 127
    Top = 732
  end
  object ePreprocessBatch: TevSecElement
    AtomComponent = aPreprocessBatch
    ElementType = otFunction
    ElementTag = 'USER_CAN_PREPROCESS_BATCH'
    Left = 940
    Top = 696
  end
  object aPreprocessBatch: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_PREPROCESS_BATCH'
    AtomCaption = 'Ability to pre-process by batch'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 941
    Top = 640
  end
  object aDashBoard: TevSecAtom
    AtomType = 'F'
    AtomTag = 'USER_CAN_ACCESS_TO_DASHBOARD'
    AtomCaption = 'Access to Dashboard'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 255
      end>
    Left = 520
    Top = 728
  end
  object eDashBoard: TevSecElement
    AtomComponent = aDashBoard
    ElementType = otFunction
    ElementTag = 'USER_CAN_ACCESS_TO_DASHBOARD'
    Left = 632
    Top = 728
  end
  object aUpdateACAAsOf: TevSecAtom
    AtomType = 'F'
    AtomTag = 'UPDATE_ACA_AS_OF'
    AtomCaption = 'Ability to update ACA fields as of another date'
    Contexts = <
      item
        Tag = 'N'
        Caption = 'Disabled'
        IconName = 'TREEFUNCTIONSDISABLED'
        Priority = 0
      end
      item
        Tag = 'Y'
        Caption = 'Enabled'
        IconName = 'TREEFUNCTIONSENABLED'
        Priority = 256
      end>
    Left = 909
    Top = 267
  end
  object eUpdateACAAsOf: TevSecElement
    AtomComponent = aUpdateACAAsOf
    ElementType = otFunction
    ElementTag = 'UPDATE_ACA_AS_OF'
    Left = 997
    Top = 272
  end
end
