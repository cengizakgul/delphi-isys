// Copyright � 2000-2004 iSystems LLC. All rights reserved.   
unit SPD_EDIT_EE_BASE;

interface

uses
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, SDataStructure, Db,
  Wwdatsrc, SysUtils, Windows, SPackageEntry, EvUtils, DBActns, ActnList,
  SPD_EDIT_Open_BASE, wwdblook, Messages, EvStreamUtils, EvContext,
  Wwdotdot, Wwdbcomb, EvSecElement, EvConsts, Variants, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictclient,
  SDataDicttemp, SPD_EmployeeChoiceQuery, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_BASE = class(TEDIT_OPEN_BASE)
    dsCL: TevDataSource;
    DM_CLIENT: TDM_CLIENT;
    DM_COMPANY: TDM_COMPANY;
    DBText2: TevDBText;
    DBText3: TevDBText;
    DBText4: TevDBText;
    EmployeeNbrText: TevDBText;
    lblEENbr: TevLabel;
    wwdsEmployee: TevDataSource;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    wwdsSubMaster: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    ActionList: TevActionList;
    EENext: TDataSetNext;
    EEPrior: TDataSetPrior;
    pnlEEChoice: TevPanel;
    dblkupEE_NBR: TevDBLookupCombo;
    dbtxtname: TevDBLookupCombo;
    EEClone: TevClientDataSet;
    FocusNbr: TAction;
    FocusName: TAction;
    Action1: TAction;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    bOpenFilteredByDBDTG: TevBitBtn;
    bOpenFilteredByEe: TevBitBtn;
    lEeFilter: TLabel;
    cbShowRemovedEmployees: TevCheckBox;
    lblClient: TevLabel;
    lblCompany: TevLabel;
    dbtxClientNbr: TevDBText;
    dbtCoNumber: TevDBText;
    dbtxClientName: TevDBText;
    CompanyNameText: TevDBText;
    pnlTopLeft: TevPanel;
    evLabel47: TevLabel;
    lblComma: TevLabel;
    lblSSN: TevLabel;
    sbEESunkenBrowse2: TScrollBox;
    evSplitterSunken2: TevSplitter;
    pnlEEBrowseLeft2: TevPanel;
    fpEEBrowseLeft2: TisUIFashionPanel;
    pnlFPEEBrowseLeftBody2: TevPanel;
    pnlEEBrowseRight2: TevPanel;
    fpEEBrowseRight2: TisUIFashionPanel;
    pnlFPEEBrowseRightBody2: TevPanel;
    pnlBrowseBorder: TevPanel;
    wwDBGrid4: TevDBGrid;
    procedure wwdsEmployeeDataChange(Sender: TObject; Field: TField);
    procedure wwDBGrid4DblClick(Sender: TObject);
    procedure wwDBGrid4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EENextExecute(Sender: TObject);
    procedure EEPriorExecute(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure dblkupEE_NBRChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FocusNameExecute(Sender: TObject);
    procedure FocusNbrExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure dbtxtnameCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure EECloneFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure wwDBGrid4FilterChange(Sender: TObject; DataSet: TDataSet;
      NewFilter: string);
    procedure bOpenFilteredByDBDTGClick(Sender: TObject);
    procedure bOpenFilteredByEeClick(Sender: TObject);
    procedure dblkupEE_NBRDropDown(Sender: TObject);
    procedure cbShowRemovedEmployeesClick(Sender: TObject);
    procedure EECloneBeforeClose(DataSet: TDataSet);
    procedure EECloneAfterOpen(DataSet: TDataSet);
    procedure wwdsSubMaster2DataChange(Sender: TObject; Field: TField);
    procedure fpEEBrowseLeft2Resize(Sender: TObject);
    procedure fpEEBrowseRight2Resize(Sender: TObject);
  private
    //EEFilter: string;
    MultiCompanyClient: Boolean;
    FCachedEEReadOnly: Variant;
    procedure CreateEEFilter;
    procedure FilterOutNotEnabledEmployees;
    procedure OnEEFilter(DataSet: TDataSet; var Accept: Boolean);
    procedure PositionNameAndComma;
  protected
    bPayrollsInQueue: Boolean;
    FActivated: Boolean;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetIfReadOnly: Boolean; override;
    procedure SetReadOnly(Value: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;

    procedure SetCurrentUserFilterPlusEE(aEENbr: integer);
    procedure RefreshEeClone;    
  public
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet;
      var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure CopyPositionAndPayGradeTo(const primary_rate :Boolean);    
  end;

implementation

uses SPD_DBDTP_Filter, dialogs;

{$R *.DFM}

var
  CurrentFilter: string = 'CURRENT_TERMINATION_CODE_DESC like ''%ACTIVE%''';

procedure TEDIT_EE_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  AddDS(DM_CLIENT.CL_PERSON, SupportDataSets);
  AddDS(DM_EMPLOYEE.EE, SupportDataSets);
  DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, GetDataSetConditions('EE'));
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_COMPANY.CO;
  if not Assigned(dsCL.DataSet) then
    dsCL.DataSet := DM_CLIENT.CL;
  if not Assigned(wwdsEmployee.DataSet) then
    wwdsEmployee.DataSet := DM_EMPLOYEE.EE;
  if not Assigned(wwdsSubMaster.DataSet) then
    wwdsSubMaster.DataSet := DM_EMPLOYEE.EE;
  if not Assigned(wwdsSubMaster2.DataSet) then
    wwdsSubMaster2.DataSet := DM_CLIENT.CL_PERSON;
end;

procedure TEDIT_EE_BASE.Activate;
var
  Msg: TMsg;
  r: integer;
begin
  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
  while PeekMessage(Msg, 0, WM_KEYUP, WM_KEYUP, PM_REMOVE) do
    ;
  bPayrollsInQueue := False;
  DM_EMPLOYEE.EE.DisableControls;
  try
    r := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
    FilterOutNotEnabledEmployees;
    DM_EMPLOYEE.EE.FilterOptions := DM_EMPLOYEE.EE.FilterOptions + [foCaseInsensitive];
    DM_EMPLOYEE.EE.UserFilter := CurrentFilter;
    DM_EMPLOYEE.EE.UserFiltered := True;
    DM_EMPLOYEE.EE.Locate('EE_NBR', r, []);
  finally
    DM_EMPLOYEE.EE.EnableControls;
  end;
  inherited;
  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(wwdsMaster.DataSet).ClientID, wwdsMaster.DataSet['CO_NBR']]), []);
  if (wwdsList.DataSet.RecordCount > 0) and
      ((wwdsList.KeyValue = wwdsMaster.KeyValue) and wwdsMaster.DataSet.Active or
       (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger =  ctx_DataAccess.ClientID)) then
      BitBtn1Click(Self);

  dblkupEE_NBR.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  dbtxtname.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  FActivated := True;
  wwdsEmployeeDataChange(nil, nil);

end;

procedure TEDIT_EE_BASE.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  AddDS(DM_CLIENT.CL_PERSON, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  AddDS(DM_EMPLOYEE.EE, aDS);
  inherited;
end;

procedure TEDIT_EE_BASE.wwdsEmployeeDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
  begin
    if dblkupEE_NBR.LookupValue <> DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString then
      dblkupEE_NBR.LookupValue := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
    if dbtxtname.LookupValue <> DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString then
      dbtxtname.LookupValue := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
//    butnNext.Enabled := DM_EMPLOYEE.EE.RecNo < DM_EMPLOYEE.EE.RecordCount;
//    butnPrior.Enabled := DM_EMPLOYEE.EE.RecNo > 1;
  end;

  if FActivated and
    (TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse) and
    (TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsBrowse) then
    if (DM_EMPLOYEE.EE.RecordCount = 0) and
      ((DM_CLIENT.CL_PERSON.RecordCount <> 0) or
      DM_CLIENT.CL_PERSON.Filtered) then
    begin
      DM_CLIENT.CL_PERSON.Filter := 'CL_PERSON_NBR = 0';
      DM_CLIENT.CL_PERSON.Filtered := True;
    end
    else
    begin
      if DM_CLIENT.CL_PERSON.Filtered = true then
      begin
        DM_CLIENT.CL_PERSON.Filtered := False;
        DM_EMPLOYEE.EE.Close;
        DM_EMPLOYEE.EE.Open;
      end;

      DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE['CL_PERSON_NBR'], []);
    end;
end;

procedure TEDIT_EE_BASE.wwDBGrid4DblClick(Sender: TObject);
begin
  inherited;
  if wwdsEmployee.DataSet.RecordCount > 0 then
    PageControl1.SelectNextPage(True);
end;

procedure TEDIT_EE_BASE.wwDBGrid4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (wwdsEmployee.DataSet.RecordCount > 0) then
  begin
    PageControl1.SelectNextPage(True);
    Key := 0;
  end;
end;

function TEDIT_EE_BASE.GetDataSetConditions(sName: string): string;
begin
  if wwdsList.DataSet.Active and (MultiCompanyClient or (ctx_PayrollCalculation.GetEeFilter <> '')) then
  begin
    if sName = 'EE' then
    begin
      Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
      if ctx_PayrollCalculation.GetEeFilter <> '' then
        Result := Result + ' and ' + ctx_PayrollCalculation.GetEeFilter;
    end
    else
      if sName = 'EE_TIME_OFF_ACCRUAL_OPER' then
        Result := UplinkRetrieveCondition('EE_TIME_OFF_ACCRUAL_NBR', DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL)
      else
        if Copy(sName, 1, 3) = 'EE_' then
          Result := UplinkRetrieveCondition('EE_NBR', DM_EMPLOYEE.EE)
        else
          if sName = 'CO_BRANCH' then
            Result := UplinkRetrieveCondition('CO_DIVISION_NBR', DM_COMPANY.CO_DIVISION)
          else
            if sName = 'CO_DEPARTMENT' then
              Result := UplinkRetrieveCondition('CO_BRANCH_NBR', DM_COMPANY.CO_BRANCH)
            else
              if sName = 'CO_TEAM' then
                Result := UplinkRetrieveCondition('CO_DEPARTMENT_NBR', DM_COMPANY.CO_DEPARTMENT)
              else
                if ((sName = 'CO') or
                  (Copy(sName, 1, 3) = 'CO_')) and
                  wwdsList.DataSet.Active then
                  Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger)
                else
                  Result := inherited GetDataSetConditions(sName);
  end
  else
    if (sName = 'CO_BRANCH') or
      (sName = 'CO_DEPARTMENT') or
      (sName = 'CO_TEAM') or
      (Copy(sName, 1, 3) = 'EE_') then
      Result := ''
    else
      if ((sName = 'CO') or
        (sName = 'EE') or
        (Copy(sName, 1, 3) = 'CO_')) and
        wwdsList.DataSet.Active then
        Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger)
      else
        Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_EE_BASE.EENextExecute(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  DM_EMPLOYEE.EE.Next;
end;

procedure TEDIT_EE_BASE.EEPriorExecute(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  DM_EMPLOYEE.EE.Prior;
end;

procedure TEDIT_EE_BASE.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  BitBtn1.Enabled := BitBtn1.Enabled or (ctx_PayrollCalculation.GetEeFilter <> '');
  dblkupEE_NBR.Enabled := not BitBtn1.Enabled;
  dbtxtname.Enabled := not BitBtn1.Enabled;
end;

procedure TEDIT_EE_BASE.Deactivate;
var
  r: Integer;
begin
  inherited;
  EEClone.Close;
  r := 0;
  DM_EMPLOYEE.EE.DisableControls;
  try
    if DM_EMPLOYEE.EE.Active then
      r := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;

    ///gdy - #7313
    DM_EMPLOYEE.EE.Filter := '';
    DM_EMPLOYEE.EE.Filtered := false;

    DM_EMPLOYEE.EE.UserFiltered := False;
    DM_EMPLOYEE.EE.UserFilter := '';
    if DM_EMPLOYEE.EE.Active then
      DM_EMPLOYEE.EE.Locate('EE_NBR', r, []);
  finally
    DM_EMPLOYEE.EE.EnableControls;
  end;
  DM_CLIENT.CL_PERSON.Filtered := False;
  FActivated := False;
end;

procedure TEDIT_EE_BASE.dblkupEE_NBRChange(Sender: TObject);
begin
  inherited;
  if TevDBLookupCombo(Sender).LookupTable.Active and
    TevDBLookupCombo(Sender).Focused and
    (Trim(TevDBLookupCombo(Sender).Text) <> '') then
    TevDBLookupCombo(Sender).LookupTable.Locate(TevDBLookupCombo(Sender).LookupField, TevDBLookupCombo(Sender).LookupValue, []);
end;

procedure TEDIT_EE_BASE.BitBtn1Click(Sender: TObject);
var
  s1: string;
  b: Boolean;
begin
  if Sender = BitBtn1 then
    ctx_PayrollCalculation.SetEeFilter('', '');

  s1 := DM_EMPLOYEE.EE.UserFilter;
  b := DM_EMPLOYEE.EE.UserFiltered;
  EEClone.Close;
  inherited;
  
  FCachedEEReadOnly := Null;

  if not DM_EMPLOYEE.EE.Active then
    Exit;

  with DM_TEMPORARY.TMP_CO.ShadowDataSet do
  try
    SetRange([ctx_DataAccess.ClientID], [ctx_DataAccess.ClientID]);
    MultiCompanyClient := RecordCount > 1;
  finally
    CancelRange;
  end;

  ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', 'count(*)');
    SetMacro('TableName', 'pr');
    SetMacro('Condition', 'co_nbr = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) +
      ' and STATUS in (''' + PAYROLL_STATUS_COMPLETED + ''', ''' + PAYROLL_STATUS_PROCESSING + ''', ''' + PAYROLL_STATUS_PREPROCESSING + ''') and ' +
      'PAYROLL_TYPE <> ''' + PAYROLL_TYPE_TAX_DEPOSIT + ''' and PAYROLL_TYPE <> ''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + '''');
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  bPayrollsInQueue := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  if GetIfReadOnly then
       SetReadOnly(True)
  else
      ClearReadOnly;
  DM_EMPLOYEE.EE.UserFilter := s1;
  DM_EMPLOYEE.EE.UserFiltered := b;
  FilterOutNotEnabledEmployees;
  CreateEEFilter;
  RefreshEeClone;
  {s := DM_EMPLOYEE.EE.IndexName;
  try
    DM_EMPLOYEE.EE.IndexName := '';
    EEClone.Filtered := True;
    EEClone.AssignDataFrom(DM_EMPLOYEE.EE);
  finally
    if s <> '' then
      DM_EMPLOYEE.EE.IndexName := s;
  end;}
  dblkupEE_NBR.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  dbtxtname.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  (Owner as TFramePackageTmpl).btnNavInsert.Enabled := not GetIfReadOnly;
  (Owner as TFramePackageTmpl).btnNavDelete.Enabled := not GetIfReadOnly and ((Owner as TFramePackageTmpl).DeleteDataSet.RecordCount > 0);
  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
end;

procedure TEDIT_EE_BASE.FocusNameExecute(Sender: TObject);
begin
  inherited;
  if dbtxtname.CanFocus then
    dbtxtname.SetFocus;
end;

procedure TEDIT_EE_BASE.FocusNbrExecute(Sender: TObject);
begin
  inherited;
  if dblkupEE_NBR.CanFocus then
    dblkupEE_NBR.SetFocus;
end;

procedure TEDIT_EE_BASE.Action1Execute(Sender: TObject);
begin
  inherited;
  PutFocus(Self);
end;

function TEDIT_EE_BASE.GetIfReadOnly: Boolean;
begin
  Result := bPayrollsInQueue or inherited GetIfReadOnly;

  if not Result then
  begin
    if (FCachedEEReadOnly = Null) then
        FCachedEEReadOnly := ctx_DataAccess.GetEEReadOnlyRight;
    Result := FCachedEEReadOnly;
  end;

end;

procedure TEDIT_EE_BASE.SetReadOnly(Value: Boolean);
begin
  inherited;
  bOpenFilteredByDBDTG.SecurityRO := False;
  bOpenFilteredByEe.SecurityRO := False;
  cbShowRemovedEmployees.SecurityRO := False;
  dblkupEE_NBR.SecurityRO := False;
  dbtxtname.SecurityRO := False;
  butnNext.SecurityRO := False;
  butnPrior.SecurityRO := False;
end;

procedure TEDIT_EE_BASE.dbtxtnameCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  if not Modified then
    TevDBLookupCombo(Sender).LookupValue := TevDBLookupCombo(Sender).OldLookup
  else
    if TevDBLookupCombo(Sender).Focused and (Trim(TevDBLookupCombo(Sender).Text) <> '') and
    (TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField] <> DM_EMPLOYEE.EE['EE_NBR']) then
      DM_EMPLOYEE.EE.Locate('EE_NBR', TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField], []);
  inherited;
end;

procedure TEDIT_EE_BASE.EECloneFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  //Accept := Pos(';' + DataSet.FieldByName('EE_NBR').AsString + ';', EEFilter) > 0;
end;

procedure TEDIT_EE_BASE.CreateEEFilter;
//var
  {r: Integer;
  i: Integer;}
  //l: TStringList;
begin
  {l := DM_EMPLOYEE.EE.GetFieldValueList('EE_NBR', True);
  try
    EEClone.FilterList := l;
    EEClone.FilterListFieldName := 'EE_NBR';
  finally
    l.Free;
  end;}
  {r := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  DM_EMPLOYEE.EE.DisableControls;
  DM_EMPLOYEE.EE.LookupsEnabled := False;
  try
    DM_EMPLOYEE.EE.First;
    EEFilter := ';';
    i := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Index;
    while not DM_EMPLOYEE.EE.Eof do
    begin
      EEFilter := EEFilter + DM_EMPLOYEE.EE.Fields[i].AsString + ';';
      DM_EMPLOYEE.EE.Next;
    end;
  finally
    DM_EMPLOYEE.EE.Locate('EE_NBR', r, []);
    DM_EMPLOYEE.EE.LookupsEnabled := True;
    DM_EMPLOYEE.EE.EnableControls;
  end;}
end;

procedure TEDIT_EE_BASE.wwDBGrid4FilterChange(Sender: TObject;
  DataSet: TDataSet; NewFilter: string);
begin
  inherited;
  CurrentFilter := NewFilter;
  EEClone.Close;
  CreateEEFilter;
  RefreshEeClone;
  {s := DM_EMPLOYEE.EE.IndexName;
  try
    DM_EMPLOYEE.EE.IndexName := '';
    EEClone.Filtered := True;
    EEClone.AssignDataFrom(DM_EMPLOYEE.EE);
  finally
    if s <> '' then
      DM_EMPLOYEE.EE.IndexName := s;
  end;}
  dblkupEE_NBR.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  dbtxtname.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
end;

//gdy - 7313

procedure TEDIT_EE_BASE.FilterOutNotEnabledEmployees;
var
  nbr: Integer;
begin
  nbr := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  if not cbShowRemovedEmployees.Checked then
  begin
    //DM_EMPLOYEE.EE.Filter := 'EE_ENABLED <> '''+GROUP_BOX_NO+'''';
    DM_EMPLOYEE.EE.OnFilterRecord := OnEEFilter;
    DM_EMPLOYEE.EE.Filtered := true;
  end
  else
  begin
    //DM_EMPLOYEE.EE.Filter := '';
    DM_EMPLOYEE.EE.OnFilterRecord := nil;
    DM_EMPLOYEE.EE.Filtered := false;
  end;
  DM_EMPLOYEE.EE.Locate('EE_NBR', nbr, []);
end;

procedure TEDIT_EE_BASE.bOpenFilteredByDBDTGClick(Sender: TObject);
begin
  inherited;
  with TDBDTP_Filter.Create(nil) do
  try
    ClNbr := wwdsList.DataSet.FieldByName('CL_NBR').AsInteger;
    CoNbr := wwdsList.DataSet.FieldByName('CO_NBR').AsInteger;
    if ShowModal = mrOk then
    begin
      ctx_PayrollCalculation.SetEeFilter(Condition, Caption);
      BitBtn1Click(Sender);
    end;
  finally
    Free;
  end;
end;

procedure TEDIT_EE_BASE.bOpenFilteredByEeClick(Sender: TObject);
var
  sCond, sCapt: string;
begin
  inherited;
  if AskEeFilter(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger,
    wwdsList.DataSet.FieldByName('CO_NBR').AsInteger, sCond, sCapt) then
  begin
    ctx_PayrollCalculation.SetEeFilter(sCond, sCapt);
    BitBtn1Click(Sender);
  end;
end;

procedure TEDIT_EE_BASE.dblkupEE_NBRDropDown(Sender: TObject);
begin
  inherited;
  EEClone.IndexFieldNames := 'CUSTOM_EMPLOYEE_NUMBER';
end;

procedure TEDIT_EE_BASE.cbShowRemovedEmployeesClick(Sender: TObject);
begin
  if not (csLoading in ComponentState) then
  begin
    BitBtn1Click(Sender);
    if cbShowRemovedEmployees.Checked and (Pos('ACTIVE', DM_EMPLOYEE.EE.UserFilter) <> 0)
      and (Pos('CURRENT_TERMINATION_CODE_DESC', DM_EMPLOYEE.EE.UserFilter) <> 0) then
      if EvMessage('Clear user filter (to show inactive employees)?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then
      begin
        DM_EMPLOYEE.EE.UserFilter := '';
        DM_EMPLOYEE.EE.UserFiltered := true;
        wwDBGrid4FilterChange(wwDBGrid4, wwDBGrid4.DataSource.DataSet, DM_EMPLOYEE.EE.UserFilter);
      end
  end
end;

procedure TEDIT_EE_BASE.EECloneBeforeClose(DataSet: TDataSet);
begin
  inherited;
  dbtxtname.LookupTable := nil;
  dblkupEE_NBR.LookupTable := nil;
end;

procedure TEDIT_EE_BASE.EECloneAfterOpen(DataSet: TDataSet);
begin
  inherited;
  dbtxtname.LookupTable := EEClone;
  dblkupEE_NBR.LookupTable := EEClone;
end;

procedure TEDIT_EE_BASE.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if Kind in [NavOk] then
  begin
    wwDBGrid4.Enabled := not Value;
    butnPrior.Enabled := not Value;
    butnNext.Enabled := not Value;
    dblkupEE_NBR.Enabled := not Value;
    dbtxtname.Enabled := not Value;
  end;

  if (Kind in [NavInsert]) and BitBtn1.Enabled  and (ctx_PayrollCalculation.GetEeFilter = '') then
     Value := false;
end;

procedure TEDIT_EE_BASE.OnEEFilter(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := DataSet.FieldByName('EE_ENABLED').AsString <> GROUP_BOX_NO;
end;

procedure TEDIT_EE_BASE.RefreshEeClone;
var
  ms: IEvDualStream;
begin
  ms := DM_EMPLOYEE.EE.GetDataStream(True, False);
  EEClone.LoadFromUniversalStream(ms);
  {  s := DM_EMPLOYEE.EE.IndexName;
    try
      DM_EMPLOYEE.EE.IndexName := '';
      EEClone.Filtered := True;
      EEClone.AssignDataFrom(DM_EMPLOYEE.EE);
    finally
      if s <> '' then
        DM_EMPLOYEE.EE.IndexName := s;
    end;}
end;

procedure TEDIT_EE_BASE.SetCurrentUserFilterPlusEE(aEENbr: integer);
begin
  DM_EMPLOYEE.EE.DisableControls;
  try
    DM_EMPLOYEE.EE.UserFiltered := False;
    if Trim(CurrentFilter) <> '' then
      DM_EMPLOYEE.EE.UserFilter := CurrentFilter + ' OR EE_NBR='+IntToStr(aEENbr)
    else
      DM_EMPLOYEE.EE.UserFilter := 'EE_NBR='+IntToStr(aEENbr);
    DM_EMPLOYEE.EE.UserFiltered := True;
    DM_EMPLOYEE.EE.Locate('EE_NBR', aEEnbr, []);
  finally
    DM_EMPLOYEE.EE.EnableControls;
  end;
end;

procedure TEDIT_EE_BASE.CopyPositionAndPayGradeTo(const primary_rate :Boolean);
var
  k, tmpRateNbr: integer;
  fPositionValue, fPositionGrades, fRateNumber, tmpEENbr : variant;
  s : string;
  cd: TevClientDataSet;
begin
  tmpEENbr := DM_EMPLOYEE.EE_RATES.FieldByName('EE_NBR').AsInteger;
  tmpRateNbr := DM_EMPLOYEE.EE_RATES.FieldByName('EE_RATES_NBR').AsInteger;
  fPositionValue := DM_EMPLOYEE.EE_RATES.FieldByName('CO_HR_POSITIONS_NBR').Value;
  fPositionGrades := DM_EMPLOYEE.EE_RATES.FieldByName('CO_HR_POSITION_GRADES_NBR').Value;
  fRateNumber := DM_EMPLOYEE.EE_RATES.FieldByName('RATE_NUMBER').Value;
  if (DM_CLIENT.EE.FieldByName('EE_NBR').AsString <> '') and (tmpRateNbr > 0) then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the Position and Pay Grade for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');

        cd := TevClientDataset.Create(nil);
        cd.ProviderName := DM_EMPLOYEE.EE_RATES.ProviderName;
        cd.DataRequired('ALL');
        if primary_rate then
        begin
          cd.Filter :='PRIMARY_RATE = ''Y''';
          cd.Filtered := true;
        end;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            if (not primary_rate and  cd.Locate('EE_NBR;RATE_NUMBER',VarArrayOf([cdsChoiceList.FieldByName('EE_NBR').Value,fRateNumber]),[]) ) or
                   (primary_rate and  cd.Locate('EE_NBR',cdsChoiceList.FieldByName('EE_NBR').Value,[]) ) then
            begin
                cd.Edit;
                cd['CO_HR_POSITIONS_NBR'] := fPositionValue;
                cd['CO_HR_POSITION_GRADES_NBR'] := fPositionGrades;
                cd.Post;
            end;
          end;
        finally
          ctx_DataAccess.PostDataSets([cd]);
          s := DM_CLIENT.EE_RATES.RetrieveCondition;
          DM_EMPLOYEE.EE_RATES.Close;
          DM_EMPLOYEE.EE_RATES.DataRequired('ALL');
          DM_EMPLOYEE.EE_RATES.RetrieveCondition := s;
          DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;EE_RATES_NBR',VarArrayOf([tmpEENbr,tmpRateNbr]), []);
          cd.Free;
          ctx_EndWait;
        end;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TEDIT_EE_BASE.PositionNameAndComma;
begin
  //move around the comma and first name as the employee name changes.
  //RecNo < 0 means the table isn't open
  lblComma.Visible := DM_CLIENT.CL_PERSON.RecNo >0;
  lblComma.Left  := DBText2.Left + DBText2.Width + 2;
  DBText3.Left   := lblComma.Left + 14;
end;

procedure TEDIT_EE_BASE.wwdsSubMaster2DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  PositionNameAndComma;
end;

procedure TEDIT_EE_BASE.fpEEBrowseLeft2Resize(Sender: TObject);
begin
  inherited;
   wwdbgridSelectClient.Width  := fpEEBrowseLeft2.Width - 40;
   wwdbgridSelectClient.Height := fpEEBrowseLeft2.Height - 65;
end;

procedure TEDIT_EE_BASE.fpEEBrowseRight2Resize(Sender: TObject);
begin
  inherited;
   wwDBGrid4.Width  := fpEEBrowseRight2.Width - 40;
   wwDBGrid4.Height := fpEEBrowseRight2.Height - 65;
end;

end.

