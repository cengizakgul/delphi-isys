// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvUtils;

{$WARN UNIT_PLATFORM OFF}

interface

uses
  SysUtils, Windows, Messages, db, DateUtils,
  classes, Math,  Variants, Graphics,
  SReportSettings, EvTypes, isTypes,
  TypInfo, SSecurityInterface, EvStreamUtils, ISBasicUtils, rwEngineTypes,
  SyncObjs,
  EvBasicUtils, ISUtils, StrUtils, EvDataAccessComponents, EvConsts, isBaseClasses,
  EvCommonInterfaces, isLogFile, SDM_ddTable, EvExceptions,
  isExceptions, isThreadManager, SDDClasses, EvClasses, EvClientDataSet;


function  VarToFloat(Value: Variant): extended;
function  VarToInt(Value: Variant): integer;
function  RoundAll(Number: Double; Precision: Integer): Double;
function  RoundTwo(Number: Double): Double;
function  RoundInt64(Number: Double): Int64;

procedure SaveDSStates(DS: array of TevClientDataSet; const SaveData: Boolean = False);
procedure LoadDSStates(DS: array of TevClientDataSet);
function  MakeArrayDS(const arr: array of TevClientDataSet): TArrayDS;
procedure AddDS(const DataSet: TevClientDataSet; var aDS: TArrayDS); overload;
procedure AddDS(const DataSets: TArrayDS; var aDS: TArrayDS); overload;
procedure AddDS(const DataSets: array of TevClientDataSet; var aDS: TArrayDS); overload;
procedure RemoveDS(const DataSet: TevClientDataSet; var aDS: TArrayDS); overload;
procedure AddDSWithCheck(const DataSet: TevClientDataSet; var aDS: TArrayDS; CheckCondition: string); overload;
procedure AddDSWithCheck(const DataSets: array of TevClientDataSet; var aDS: TArrayDS; CheckCondition: string); overload;
function  DSConditionsEqual( c1, c2: string ): boolean;
function  NormalizeFileName(const AFileName: string): string;
procedure CheckFileSize(FileName: string; MaxSize: DWord);
function  CapitalizeWords(const AText: string): string;
procedure CreateNewClientDataSet(aComponent: TComponent; var aClientDS: TevClientDataSet; aDS_From: TevClientDataSet);
function  ClonedLookup(const d: TEvClientDataSet; const KeyFieldNames: string; const KeyFieldValues: Variant;
                       const ResultFieldNames: string): Variant;
function  TaxTableName(const TaxTable: TTaxTable): string;

function IntToHexChar(const D: Integer): Char;
function HexCharToInt(const s: Char): Integer;
function PadStringLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
function PadStringRight(MyString: string; MyPad: Char; MyLength: Integer): string;
function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
function PadRight(MyString: string; MyPad: Char; MyLength: Integer): string;
function ConvertNull(MyValue, Replacement: Variant): Variant;
function DayIsWeekend(Day: TDateTime): Boolean;
function ExtractFromFiller(Filler: string; Position, Count: Integer): string;
function PutIntoFiller(Filler, PutString: string; Position, Count: Integer): string;

function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
function CheckForReversedNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
function DateIsNotFedHoliday(const SY_GLOBAL_AGENCY_NBR: Integer; const d: TDateTime): Boolean;

function GetMasterInquiryPIN(const SBBankAccountNbr: Integer): string;
function BreakCompanyFreqIntoPayFreqList(const Freq: string): TStringList;
function ReturnDescription(Input, Code: string): string;
procedure CustomQuery(const cd: TEvBasicClientDataSet; const ASQL: string; const ADatabase: String; const AParams: IisListOfValues = nil);
procedure HandleCustomNumber(aField: TField; aNumberOfSpaces: Integer);

function ScheduleToText(Schedule: string): string;

// misc date
function GetFedHoliday(const Year: Integer; const Holiday: Char; const SY_GLOBAL_AGENCY_NBR: integer;const d: TDateTime): TDateTime;
function DateIsFederalHoliday(const d: TDateTime; out HolidayRule: Char; const SY_GLOBAL_AGENCY_NBR: integer = -1): Boolean;
function GetSeqWeekDay(const dFrom, dTill: TDateTime; dDayOfWeek: Integer; const SeqNumber: Integer): TDateTime;

function BinaryToText(const Source: string): string;
function TextToBinary(Source: string): string;
procedure DecodeToStream(Source: string; Stream: TStream);

// Global variables
procedure LockResource(const TypeName, Tag, FailureMessage: string; const LockType: TResourceLockType = rlTryOnce);
procedure UnLockResource(const TypeName, Tag: string; const LockType: TResourceLockType = rlTryOnce);

// some Payroll Calculations Routines
function ConvertDeduction(EDCodeType: string; Amount: Variant): Real;
function GetTaxFreq(TaxFreq: string): Integer;
function GroupTermLifeRate(Age: Integer): Real;
function CalculateAge(CurrentDate, DateOfBirth: TDateTime; EndOfYear: Boolean): Integer;
function GetCheckFreq(CheckFreq: string): Integer;
function WeeksInFreq(Freq: string): Double;
function ThisIsTaxOrWages(MyString: string): Boolean;
function Is_ER_SUI(SUI_Type: string): Boolean;
function GetEFTPSTaxCode(TaxName: string): string;
function TypeIsTaxable(EDCodeType: string): Boolean;
function ListSpecTaxedDedTypes: String;
function TypeIsSpecTaxedDed(EDCodeType: string): Boolean;
function ListPensionTypes: String;
function TypeIsPension(EDCodeType: string): Boolean;
function TypeIsHSA(ACode: String): Boolean;
function ListCatchUpTypes: String;
function TypeIsCatchUp(EDCodeType: string): Boolean;
function List3rdPartyTypes: String;
function TypeIs3rdParty(EDCodeType: string): Boolean;
function TypeIsTaxableMemo(EDCodeType: string): Boolean;
function TypeIsTipsBefore2014(EDCodeType: string): Boolean;
function TypeIsTipsAfter2013(EDCodeType: string): Boolean;
function ListDistributedMemoTypes: String;
function TypeIsDistributedMemo(EDCodeType: string): Boolean;
function TypeIsCorrectionRun(PRType: string): Boolean;
function TypeIsCobraCredit(EDCodeType, EDCodeDescription: string): Boolean;
function TypeIsManagerApproval(PRType: string): Boolean;

procedure OpenDataSetForPayroll(aDataSet: TevClientDataSet; PrNbr: Integer);

procedure SetEESchedEDChanged(const Value: string);
function EESchedEDChanged: string;

procedure PopulateDataSets(const DS: array of TevClientDataSet; const Condition: string = ''; const OpenWithLookups: Boolean = True);
procedure DecodeFieldLevelSecurityStructure(const m: IisStream; out cdMaster, cdDetail: TEvClientDataset);
function CreateLookupDataset(const ds: TEvBasicClientDataSet; const Owner: TComponent): TevClientDataset;

function Get_EE_YTD(EmpNumber: Integer; StartDate, EndDate: TDateTime; const YTDType: String; aRecNumber: Integer): Double;
function GetNextInvoiceNumber: Integer;

procedure ODS(const Fmt: string; const Args: array of const); overload;
procedure ODS(const msg: string); overload;

function IsDebugMode: Boolean;
function IsQAMode: Boolean;
function UnAttended: Boolean;
function GetToken(const s: string; var p: Integer; out token: string): Boolean;

//VMR
function CheckVmrActive: Boolean;
function CheckVmrLicenseActive: Boolean;
function CheckVmrSetupActive: Boolean;
function CheckRemoteVMRActive: Boolean;
function GetRemotePrinterList:IisStringList;
function GetPrinterCount (aLoc:string):integer;
function GetTrayCount (aLoc:string):integer;
function GetSBPaperInfo:IevDataSet;
function GetSBOption(aLoc:string):IevDataSet;

function SbSendsTaxPaymentsForCompany(TaxServiceLevel: string = ''): Boolean;
function CompanyRequiresTaxesBeImpounded(TaxServiceLevel: string = ''): Boolean;

function GetFieldValueList(const LiabilityDataSet: TevClientDataSet; const FieldName: string; const ExcludeNulls: Boolean = False): TStringList;
function GetFieldValueListString(const LiabilityDataSet: TevClientDataSet; const FieldName: string): string;
function GetVarArrayFromStrings(const s: TStringList; const VarType: TVarType = varVariant): Variant;
function GetVarArrayFieldValueList(const LiabilityDataSet: TevClientDataSet; const FieldName: string; const VarType: TVarType = varVariant; const ExcludeNulls: Boolean = False): Variant;
function UplinkFieldCondition(const FieldName: string; const Ds: TevClientDataSet): string;
function DataSetsToVarArray(const Value: array of TevClientDataSet): Variant;

procedure CreateFullTable(aTableName: string; var aClientDataSet: TevClientDataSet; Columns: String = '*');
function LocateAsOfDateRecord(DataSet: TevClientDataSet; KeyFields: string; KeyFieldValues: Variant; AsOfDate: TDateTime; ErrorWhenNoLocate: Boolean = False): Boolean;
function GetAsOfDateValue(DataSet: TevClientDataSet; KeyFields: string; KeyFieldValues: Variant; ResultField: string; AsOfDate: TDateTime;
  ReturnLastValue: Boolean = False; ErrorOnNull: Boolean = False; ReturnToBookmark: Boolean = False): Variant;

function SumDataSet(const ADataSet: TevClientDataSet; const AFieldName: String; const ACondition: String = ''): Variant;

function CreateFakeSSN: string;
procedure VerifyManualCheckChange(PrNbr, CheckNbr: String);

function IsCoAsOfDateTable(const TableName: string): Boolean;
function IsSbAsOfDateTable(const TableName: string): Boolean;

function GetReportsByType(const AType : string; const ALevels: string; AOwner: TComponent): TevClientDataSet;

function ValidateABANumber(ABANumber: Variant): integer;
function GetValidateABAResult(ABANumber: Variant): integer;

function IsMaintenanceMode: Boolean;

function CheckLicenses: Boolean;

function BuildINsqlStatement(const ALeftSide: String; const AItems: IisStringList): String; overload;
function BuildINsqlStatement(const ALeftSide: String; const AItems: String): String; overload;
function BuildINsqlStatement(const ALeftSide: String; const AItems: IisIntegerList): String; overload;

procedure CheckSSN(const ASSN: String; const AShortCheck: boolean = false); // do not pass EIN!!
procedure CheckEIN(const AEIN: String); // do not pass SSN

function GetReadOnlyClientCompanyList: IisStringList;
function GetReadOnlyClintCompanyDataset: IevDataSet;
function GetReadOnlyClintCompanyListFilter(const withand: Boolean=true; const prefix : string = '';const WithNot : boolean=True): String;
function GetReadOnlyClintCompanyFilter(const withand: Boolean=true; const prefixCL : string = ''; const prefixCO : string = '';
                                       const WithList : Boolean=false; const WithSomeResult : boolean = false): String;

function CheckReadOnlyClientCompanyFunction: Boolean;
function CheckReadOnlyClientFunction: Boolean;
function CheckReadOnlyCompanyFunction: Boolean;

function GetOBCBankAccountInfo : String;
function GetCoBenefitRateNumber(const SubTypeNbr: integer; const pNow : TDateTime; const  pEndDate : TDateTime): boolean;
function GetCoBenefitRateNumberMaxDependents(const SubTypeNbr: integer; const pNow : TDateTime; const  pEndDate : TDateTime): integer;
function GetEnableHrBenefit(const coNbr: integer): Boolean;
procedure AttachSUIToPayrolls(CONbr, COSUINbr, COStateNbr: Integer; BeginCheckDate, EndCheckDate: TDateTime);
procedure SetUpCO_E_D_CODESDefault(const Code,Desc : string);
function CreatecdTCDLocalsDataset: IevDataSet;
function GetCoLocalTaxDeduct(const deduct, localType: string; const newRecord : boolean = false):string;

function GetSBAnalyticsLicense: Boolean;

function HashTotalABAOk(S: string): Integer;
function HashTotalABA(S: string): Integer;

function FrequencyIn(OneFrequency, MultipleFrequencies: string): Boolean;

procedure SortDSBy( ds: TEvClientDataSet; aFieldNames, aDescFields: string );
function Iff(const Condition: Boolean; const TruePart, FalsePart: Variant): Variant;

function BlockHr: Boolean;

function  SysTime: TDateTime;
function  SysDate: TisDate;

function GetDBTypeByTable(const ATableName: String): TevDBType;
function GetDBNameByType(const ADBType: TevDBType): String;
function GetDBNameByTable(const ATableName: String): String;
function GetDBTypeByDBName(const ADBName: String): TevDBType;

function GetEDCheckLineCount(EDNbr: Integer): Integer;
function DBRadioGroup_ED_GetAvailFromDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string; const AState: Integer = 0): string;
function DBRadioGroup_ED_GetRightDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string): string;

function TaskNbrToId(const ATaskNbr: String): TisGUID;

function IsRWAFormat(AData: IEvDualStream): Boolean;

procedure updateSbENABLE_ANALYTICSforClient(const clNbr: integer);
procedure updateSbENABLE_ANALYTICS(const coNbr: integer; const enableANALYTICS: string);
function DashboardUserCount:integer;

function CheckPrimaryRateFlag(const withRateNbr: Boolean;const aAsOfDate: TDateTime):boolean;

implementation

uses
  SFieldCodeValues, SDataStructure, EvDataSet,
  PCLPrinter, ShellApi, ShlObj, SPD_ProcessException, FileCtrl, VarCmplx,
  EvContext, EvMainboard, EvInitApp, isSettings, ExtCtrls,
  ISDataAccessComponents, SDDStreamClasses, rwCustomDataDictionary;

var
  FDebugMode: Boolean;
  FQAMode: Boolean;

const
  aWFactor: array[1..8] of integer=(3, 7, 1, 3, 7, 1, 3, 7);

procedure HandleCustomNumber(aField: TField; aNumberOfSpaces: Integer);
var
  aValue: string;
begin
  aValue := aField.AsString;
  if aValue = '' then
    Exit;
  aField.Value := PadStringLeft(aValue, ' ', aNumberOfSpaces);
end;

procedure PopulateDataSets(const DS: array of TevClientDataSet; const Condition: string = ''; const OpenWithLookups: Boolean = True);
var
  i: Integer;
begin
  for i := 0 to High(DS) do
  begin
    DS[i].Close;
    DS[i].ClientID := ctx_DataAccess.ClientID;
    DS[i].RetrieveCondition := Condition;
    DS[i].OpenWithLookups := OpenWithLookups;
  end;
end;

function CreateLookupDataset(const ds: TEvBasicClientDataSet; const Owner: TComponent): TevClientDataset;
begin
  Result := TevClientDataSet.Create(Owner);
  if ds is TevClientDataset then
    Result.ClientId := TevClientDataset(ds).ClientId;
  Result.Data := CreateLookupData(ds);
end;

procedure DecodeFieldLevelSecurityStructure(const m: IisStream; out cdMaster, cdDetail: TEvClientDataset);
var
  MainTotal, DetailTotal, i, j: Integer;
  Nbr: Integer;
begin
  Nbr := 1;
  cdMaster := TevClientDataset.Create(nil);
  cdMaster.FieldDefs.Add('MAIN_NBR', ftInteger, 0, True);
  cdMaster.FieldDefs.Add('TAG', ftString, 15, True);
  cdMaster.FieldDefs.Add('NAME', ftString, 40, True);
  cdMaster.CreateDataSet;
  cdDetail := TevClientDataset.Create(nil);
  cdDetail.FieldDefs.Add('MAIN_NBR', ftInteger, 0, True);
  cdDetail.FieldDefs.Add('TABLE_NAME', ftString, 40, True);
  cdDetail.FieldDefs.Add('FIELD_NAME', ftString, 40, True);
  cdDetail.FieldDefs.Add('FIELD_TYPE', ftString, 1, True);
  cdDetail.FieldDefs.Add('FIELD_LENGTH', ftInteger, 0, True);
  cdDetail.CreateDataSet;
  m.Position := 0;
  MainTotal := m.ReadInteger;
  for i := 0 to MainTotal-1 do
  begin
    cdMaster.Append;
    Inc(Nbr);
    cdMaster['MAIN_NBR'] := Nbr;
    cdMaster['TAG'] := m.ReadString;
    cdMaster['NAME'] := m.ReadString;
    cdMaster.Post;
    DetailTotal := m.ReadInteger;
    for j := 0 to DetailTotal-1 do
    begin
      cdDetail.Append;
      cdDetail['MAIN_NBR'] := Nbr;
      cdDetail['TABLE_NAME'] := m.ReadString;
      cdDetail['FIELD_NAME'] := m.ReadString;
      cdDetail['FIELD_TYPE'] := m.ReadString;
      cdDetail['FIELD_LENGTH'] := m.ReadInteger;
      cdDetail.Post;
    end;
  end;
  cdDetail.MergeChangeLog;
  cdMaster.MergeChangeLog;
end;

procedure SetEESchedEDChanged(const Value: string);
begin
  ctx_DataAccess.EESchedEDChanged := Value;
end;

function EESchedEDChanged: string;
begin
  Result := ctx_DataAccess.EESchedEDChanged;
end;

function GetSeqWeekDay(const dFrom, dTill: TDateTime; dDayOfWeek: Integer; const SeqNumber: Integer): TDateTime;
var
  d: Integer;
  ld: TDateTime;
  i: Integer;
begin
  i := 0;
  ld := 0;
  Result := 0;
  Inc(dDayOfWeek);
  if dDayOfWeek = 8 then
    dDayOfWeek := 1;
  for d := Trunc(dFrom) to Trunc(dTill) do
    if DayOfWeek(d) = dDayOfWeek then
    begin
      Inc(i);
      if i = SeqNumber then
      begin
        Result := d;
        Break;
      end
      else
        ld := d;
    end;
  if SeqNumber = -1 then
    Result := ld;
end;


function GetFedHoliday(const Year: Integer; const Holiday: Char; const SY_GLOBAL_AGENCY_NBR: integer;const d: TDateTime): TDateTime;
begin
  Result := 0;
  case Holiday of
  FED_HOLIDAY_NEW_YEAR:
    begin
     if GetMonth(d) = 12 then
       Result := EncodeDate(Year + 1, 1, 1)
     else
       Result := EncodeDate(Year, 1, 1);
    end;
  FED_HOLIDAY_WASHINGTON_BIRTHDAY:
    Result := GetSeqWeekDay(EncodeDate(Year, 2, 1), EncodeDate(Year, 3, 1)-1, 1, 3);
  FED_HOLIDAY_ML_KING_DAY:
    Result := GetSeqWeekDay(EncodeDate(Year, 1, 1), EncodeDate(Year, 1, 31), 1, 3);
  FED_HOLIDAY_INDEPENDANCE_DAY:
    Result := EncodeDate(Year, 7, 4);
  FED_HOLIDAY_MEMORIAL_DAY:
    Result := GetSeqWeekDay(EncodeDate(Year, 5, 1), EncodeDate(Year, 5, 31), 1, -1);
  FED_HOLIDAY_VETERAN_DAY:
    Result := EncodeDate(Year, 11, 11);
  FED_HOLIDAY_COLUMBUS_DAY:
    Result := GetSeqWeekDay(EncodeDate(Year, 10, 1), EncodeDate(Year, 10, 31), 1, 2);
  FED_HOLIDAY_THANKSGIVING:
    Result := GetSeqWeekDay(EncodeDate(Year, 11, 1), EncodeDate(Year, 11, 30), 4, 4);
  FED_HOLIDAY_CHRISMAS:
    Result := EncodeDate(Year, 12, 25);
  FED_HOLIDAY_LABOR_DAY:
    Result := GetSeqWeekDay(EncodeDate(Year, 9, 1), EncodeDate(Year, 9, 30), 1, 1);
  end;
  if DayOfWeek(Result) = 7 then
  begin                          //  NY - SY GLOBAL AGENCY's
    if (SY_GLOBAL_AGENCY_NBR <> 116) and (SY_GLOBAL_AGENCY_NBR <> 2180) then
       Result := Result- 1;
  end
  else
  if DayOfWeek(Result) = 1 then
    Result := Result+ 1;
end;

function DateIsFederalHoliday(const d: TDateTime; out HolidayRule: Char; const SY_GLOBAL_AGENCY_NBR: integer = -1): Boolean;
var
  Year: Integer;
  bmonth, smonth : integer;
  dDate : TDateTime;
begin
  Result := False;
  Year := GetYear(d);
  DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Activate;
  DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Filter := 'SY_GLOBAL_AGENCY_NBR = ' + IntToStr(SY_GLOBAL_AGENCY_NBR);
  DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Filtered := true;
  if DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.RecordCount > 0 then
  begin
    HolidayRule := HOLIDAY_RULE_DISREGARD;
    dDate := 0;

    DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.First;
    while not DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Eof do
    begin
      if DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('WEEK_NUMBER').AsInteger <> 0 then
      begin
         bmonth := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger;
         smonth := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger + 1;
         if smonth > 12 then smonth := 1;

         dDate := GetSeqWeekDay(EncodeDate(Year,bmonth, 1), EncodeDate(Year, smonth, 1)-1,
                     DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('DAY_OF_WEEK').AsInteger,
                     DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('WEEK_NUMBER').AsInteger);
      end
      else
      if DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger > 0 then
      begin
                   //   special control for New Year holiday
         if (DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger = 1) and
            (DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('DAY_NUMBER').AsInteger = 1) then
         begin
           if GetMonth(d) = 12 then
               dDate := EncodeDate(Year + 1, 1, 1)
           else
               dDate := EncodeDate(Year, 1, 1);
         end
         else
           dDate := EncodeDate(Year,
                   DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger,
                    DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('DAY_NUMBER').AsInteger);
      end
      else
      if DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('MONTH_NUMBER').AsInteger = 0 then
         dDate := EncodeDateDay(Year, DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('DAY_NUMBER').AsInteger);

      if DayOfWeek(dDate) = 7 then
         dDate := dDate + DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('SATURDAY_OFFSET').AsInteger
      else
      if DayOfWeek(dDate) = 1 then
         dDate := dDate + DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('SUNDAY_OFFSET').AsInteger;

      if dDate = d then
      begin
         HolidayRule := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.FieldByName('CALC_TYPE').AsString[1];
         Result := True;
         Exit;
      end;

      DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Next;
    end;
  end
  else
  begin
    HolidayRule := 'X';
    if GetFedHoliday(Year, FED_HOLIDAY_NEW_YEAR, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_WASHINGTON_BIRTHDAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_ML_KING_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_INDEPENDANCE_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_MEMORIAL_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_VETERAN_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_COLUMBUS_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_THANKSGIVING, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_CHRISMAS, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end
    else
    if GetFedHoliday(Year, FED_HOLIDAY_LABOR_DAY, SY_GLOBAL_AGENCY_NBR, d) = d then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure CreateNewClientDataSet(aComponent: TComponent; var aClientDS: TevClientDataSet; aDS_From: TevClientDataSet);
var
  I: integer;
  aField: TField;
  DoCreate: boolean;
  vData: variant;
begin
  DoCreate := True;
  if assigned(aClientDs) then
  begin
    with aClientDS do
    begin
      if Active then
      begin
        vData := Data;
        Close;
        DoCreate := False;
      end;
    end;
  end
  else
    aClientDS := TevClientDataSet.Create(aComponent);
  with aClientDS do
  begin
    Filter := '';
    Filtered := False;
    IndexName := '';
    for I := 0 to aDS_From.FieldCount - 1 do
    begin
      if not (FindField(aDS_From.Fields[I].FieldName) = nil) then
        continue;
      aField := DefaultFieldClasses[aDS_From.Fields[I].DataType].Create(aComponent);
      with aField do
      begin
        Visible := aDS_From.Fields[I].Visible;
        Size := aDS_From.Fields[I].Size;
        FieldName := aDS_From.Fields[I].FieldName;
        Required := False;
        DataSet := aClientDS;
        DisplayLabel := aDS_From.Fields[I].DisplayLabel;
        FieldKind := aDS_From.Fields[I].FieldKind;
        KeyFields := aDS_From.Fields[I].KeyFields;
        LookupDataSet := aDS_From.Fields[I].LookupDataSet;
        LookupKeyFields := aDS_From.Fields[I].LookupKeyFields;
        LookupResultField := aDS_From.Fields[I].LookupResultField;
      end;
      if aField is TBCDField then
        TBCDField(aField).Precision := TBCDField(aDS_From.Fields[I]).Precision;
      if aDS_From.Fields[I].DataType = ftFixedChar then
        TStringField(aField).FixedChar := True;
    end;
    if DoCreate then
      CreateDataSet
    else
      Data := vData;
  end;
end;


procedure CustomQuery(const cd: TEvBasicClientDataSet; const ASQL: string; const ADatabase: String; const AParams: IisListOfValues = nil);
var
  DS: string;
  p: TGetDataParams;
  r: TGetDataResults;
  ms: IisStream;
  w: IExecDSWrapper;
  i: Integer;
begin
  if ADatabase = CH_DATABASE_CLIENT then
    DS := 'CL_CUSTOM_PROV'
  else if ADatabase = CH_DATABASE_SYSTEM then
    DS := 'SY_CUSTOM_PROV'
  else if ADatabase = CH_DATABASE_SERVICE_BUREAU then
    DS := 'SB_CUSTOM_PROV'
  else
    DS := 'TEMP_CUSTOM_PROV';

  w := TExecDSWrapper.Create(ASQL);
  w.Params := AParams;

  p := TGetDataParams.Create;
  try
    p.Write(DS, 0, w);
    for i := 1 to 10 do
    begin
      ms := ctx_DBAccess.GetDataSets(p);
      if Assigned(ms) then Break;
    end;
    r := TGetDataResults.Create(ms);
    try
      r.Read(cd, True);
    finally
      r.Free;
    end;
  finally
    p.Free;
  end;
end;

function ReturnDescription(Input, Code: string): string;
var
  MyStrings: TStrings;
  I, TabPos: Integer;
begin
  MyStrings := TStringList.Create;
  try
    MyStrings.Text := Input;
    Result := '';
    for I := 0 to MyStrings.Count - 1 do
    begin
      TabPos := Pos(#9, MyStrings[I]);
      if TabPos <> -1 then
        if Copy(MyStrings[I], TabPos + 1, Length(MyStrings[I])) = Code then
        begin
          Result := Copy(MyStrings[I], 1, TabPos - 1);
          Break;
        end;
    end;
  finally
    MyStrings.Free;
  end;
end;

function CapitalizeWords(const AText: string): string;
var
  s, w: String;
begin
  Result := '';

  s := AnsiLowerCase(Trim(AText));

  while s <> ''  do
  begin
    w := GetNextStrValue(s, ' ');
    s := TrimLeft(s);
    AddStrValue(Result, AnsiUpperCase(w[1]) + Copy(w, 2, Length(w)), ' ');
  end;

  Result := Trim(StringReplace(Result, '_', ' ', [rfReplaceAll]));
end;


function GetMasterInquiryPIN(const SBBankAccountNbr: Integer): string;
begin
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
  Result := ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', SBBankAccountNbr, 'MASTER_INQUIRY_PIN'), '');
  if Result = '' then
    raise EInconsistentData.CreateHelp('Master Inquiry PIN has to be setup on the Service Bureau Bank Account level.', IDH_InconsistentData);
end;

function BreakCompanyFreqIntoPayFreqList(const Freq: string): TStringList;
begin

  Result := TStringList.Create;

  case Freq[1] of
    CO_FREQ_DAILY:
      Result.Add(FREQUENCY_TYPE_DAILY);
    CO_FREQ_WEEKLY:
      Result.Add(FREQUENCY_TYPE_WEEKLY);
    CO_FREQ_BWEEKLY:
      Result.Add(FREQUENCY_TYPE_BIWEEKLY);
    CO_FREQ_MONTHLY:
      Result.Add(FREQUENCY_TYPE_MONTHLY);
    CO_FREQ_SMONTHLY:
      Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
    CO_FREQ_QUARTERLY:
      Result.Add(FREQUENCY_TYPE_QUARTERLY);
    CO_FREQ_WEEKLY_BWEEKLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
      end;
    CO_FREQ_WEEKLY_SMONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
      end;
    CO_FREQ_WEEKLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_BWEEKLY_SMONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
      end;
    CO_FREQ_BWEEKLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_SMONTHLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
      end;
    CO_FREQ_WEEKLY_BWEEKLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_WEEKLY_SMONTHLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY:
      begin
        Result.Add(FREQUENCY_TYPE_WEEKLY);
        Result.Add(FREQUENCY_TYPE_BIWEEKLY);
        Result.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
        Result.Add(FREQUENCY_TYPE_MONTHLY);
      end;
  end;
end;

function VarToInt(Value: Variant): integer;
var
  eResult: integer;
begin
  if VarIsEmpty(Value) or VarIsNull(Value) then
    eResult := 0
  else
    eResult := Value;
  result := eResult;
end;

function VarToFloat(Value: Variant): extended;
var
  eResult: extended;
begin
  if VarIsEmpty(Value) or VarIsNull(Value) then
    eResult := 0.0
  else
    eResult := Value;
  result := eResult;
end;

function TaxTableName(const TaxTable: TTaxTable): string;
begin
  case TaxTable of
    ttFed:
      Result := 'Federal';
    ttState:
      Result := 'State';
    ttSUI:
      Result := 'SUI';
    ttLoc:
      Result := 'Local';
  else
    Assert(False);
  end;
end;

function RoundAll(Number: Double; Precision: Integer): Double;
{ From Delphi help:
Round returns an Int64 value that is the value of X rounded to the nearest whole number.
If X is exactly halfway between two whole numbers, the result is always the even number.
}
const
  ArPower: array [0..8] of integer = (1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000); //works faster than Power function
var
  P: Extended;
begin
  Result := Number;
  if Precision < 0 then
    Exit;

  if Precision > High(ArPower) then
    P := Power(10, Precision)
  else
    P := ArPower[Precision];
  Result := Number * P;
  Result := Round(Result + 1 / MaxLongInt) / P;
end;

function RoundTwo(Number: Double): Double;
begin
  Result := RoundAll(Number, 2);
end;

function RoundInt64(Number: Double): Int64;
begin
  Result := Trunc(RoundAll(Number, 0));
end;

function IntToHexChar(const D: Integer): Char;
begin
  case d of
  0..9: Result := Chr(Ord('0')+ d);
  10..15: Result := Chr(Ord('A')+ d- 10);
  else
    Assert(False);
    Result := ' ';
  end
end;

function HexCharToInt(const s: Char): Integer;
begin
  case s of
  '0'..'9': Result := Ord(s)- Ord('0');
  'A'..'F': Result := Ord(s)- Ord('A')+ 10;
  else
    Assert(False);
    Result := 0;
  end
end;

function ClonedLookup(const d: TEvClientDataSet; const KeyFieldNames: string; const KeyFieldValues: Variant;
  const ResultFieldNames: string): Variant;
var
  cd: TevClientDataSet;
begin
  cd := TevClientDataSet.Create(nil);
  try
    cd.CloneCursor(d, True);
    Assert(cd.Locate(KeyFieldNames, KeyFieldValues, []));
    Result := cd[ResultFieldNames];
  finally
    cd.Free;
  end;
end;

function PadStringLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := MyString;
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

function PadStringRight(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := MyString;
  while Length(Result) < MyLength do
    Result := Result + MyPad;
end;

function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

function PadRight(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := Result + MyPad;
end;

function ConvertNull(MyValue, Replacement: Variant): Variant;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function DayIsWeekend(Day: TDateTime): Boolean;
begin
  Result := False;
  if (DayOfWeek(Day) = 1) or (DayOfWeek(Day) = 7) then Result := True;
end;

function ExtractFromFiller(Filler: string; Position, Count: Integer): string;
begin
  if Length(Filler) < Position then
    Result := ''
  else
    if Length(Filler) < Position + Count then
    Result := Trim(Copy(Filler, Position, Length(Filler)))
  else
    Result := Trim(Copy(Filler, Position, Count));
end;

function PutIntoFiller(Filler, PutString: string; Position, Count: Integer): string;
begin
  if Length(Filler) < Position then
    Result := PadRight(Filler, ' ', Position-1)+ PutString
  else
  if Length(Filler) < Position+ Count then
    Result := Copy(Filler, 1, Position-1)+ PutString
  else
    Result := Copy(Filler, 1, Position-1)+ PadRight(PutString, ' ', Count)+
      Copy(Filler, Position+ Count, High(Integer));
end;

function ConvertDeduction(EDCodeType: string; Amount: Variant): Real;
begin
  Result := ConvertNull(Amount, 0);
  if EDCodeType[1] = 'D' then Result := Result * (-1);
end;

function GetTaxFreq(TaxFreq: string): Integer;
begin
  Result := 0;
  if Length(TaxFreq) > 0 then
    case TaxFreq[1] of
      FREQUENCY_TYPE_ANNUAL: Result := 1;
      FREQUENCY_TYPE_SEMI_ANNUAL: Result := 2;
      FREQUENCY_TYPE_QUARTERLY: Result := 4;
      FREQUENCY_TYPE_MONTHLY: Result := 12;
      FREQUENCY_TYPE_SEMI_MONTHLY: Result := 24;
      FREQUENCY_TYPE_BIWEEKLY: Result := 26;
      FREQUENCY_TYPE_WEEKLY: Result := 52;
      FREQUENCY_TYPE_DAILY: Result := 260;
    end;
end;

function GroupTermLifeRate(Age: Integer): Real;
begin
  case Age of
    0..24: Result := 0.05;
    25..29: Result := 0.06;
    30..34: Result := 0.08;
    35..39: Result := 0.09;
    40..44: Result := 0.10;
    45..49: Result := 0.15;
    50..54: Result := 0.23;
    55..59: Result := 0.43;
    60..64: Result := 0.66;
    65..69: Result := 1.27;
  else
    Result := 2.06;
  end;
end;

function CalculateAge(CurrentDate, DateOfBirth: TDateTime; EndOfYear: Boolean): Integer;
var
  Year, Month, Day, Year2, Month2, Day2: Word;
begin
  if EndOfYear then
  begin
    DecodeDate(CurrentDate, Year2, Month2, Day2);
    DecodeDate(DateOfBirth, Year, Month, Day);
    Result := Year2 - Year;
  end
  else
  begin
    DecodeDate(CurrentDate, Year2, Month2, Day2);
    DecodeDate(DateOfBirth, Year, Month, Day);

    Result := Year2 - Year;
    if (Month2 < Month) or ((Month2 = Month) and (Day2 <= Day)) then
      Result := Result - 1;
  end;
end;

function GetCheckFreq(CheckFreq: string): Integer;
begin
  Result := 0;
  case CheckFreq[1] of
    FREQUENCY_TYPE_QUARTERLY: Result := 4;
    FREQUENCY_TYPE_MONTHLY: Result := 12;
    FREQUENCY_TYPE_SEMI_MONTHLY: Result := 24;
    FREQUENCY_TYPE_BIWEEKLY: Result := 26;
    FREQUENCY_TYPE_WEEKLY: Result := 52;
    FREQUENCY_TYPE_DAILY: Result := 365;
  end;
end;

function WeeksInFreq(Freq: string): Double;
begin
  Result := 0;
  case Freq[1] of
    FREQUENCY_TYPE_QUARTERLY: Result := 13;
    FREQUENCY_TYPE_MONTHLY: Result := 52/12;
    FREQUENCY_TYPE_SEMI_MONTHLY: Result := 52/24;
    FREQUENCY_TYPE_BIWEEKLY: Result := 2;
    FREQUENCY_TYPE_WEEKLY: Result := 1;
    FREQUENCY_TYPE_DAILY: Result := 1/5;
  end;
end;

function TypeIsTaxable(EDCodeType: string): Boolean;
begin
  Result := (EDCodeType[1] = 'E') or TypeIsSpecTaxedDed(EDCodeType);
end;

function ListSpecTaxedDedTypes: String;
begin
  Result := '''' + ED_ST_401K + ''',''' + ED_ST_403B + ''',''' + ED_ST_457 + ''','''
    + ED_ST_501C + ''',''' + ED_ST_SIMPLE + ''',''' + ED_ST_SEP + ''',''' + ED_ST_PENSION + ''','''
    + ED_ST_MASS_RETIREMENT + ''',''' + ED_ST_DEFERRED_COMP + ''',''' + ED_ST_STATE_PENSION + ''','''
    + ED_ST_PRETAX_INS + ''',''' + ED_ST_SB_PREAX_INS + ''',''' + ED_ST_SP_TAXED_DEDUCTION + ''','''
    + ED_MEMO_PENSION + ''',''' + ED_MEMO_PENSION_SUI + ''','''
    + ''',''' + ED_ST_DISCRIMINARY_S125 + ''','''
    + ED_ST_DEPENDENT_CARE + ''',''' + ED_ST_TIAA_CREFF + ''',''' + ED_ST_CATCH_UP + ''','''
    + ED_ST_401K_CATCH_UP + ''',''' + ED_ST_403B_CATCH_UP + ''',''' + ED_ST_457_CATCH_UP + ''','''
    + ED_ST_501C_CATCH_UP + ''',''' + ED_ST_SIMPLE_CATCH_UP + ''','''
    + ED_ST_HSA_SINGLE + ''',''' + ED_ST_HSA_FAMILY + ''',''' + ED_ST_HSA_CATCH_UP + '''';
end;

function TypeIsSpecTaxedDed(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListSpecTaxedDedTypes) <> 0;
end;

function ListPensionTypes: String;
begin
  Result := '''' + ED_ST_401K + ''',''' + ED_ST_403B + ''',''' + ED_ST_457 + ''','''
    + ED_ST_501C + ''',''' + ED_ST_SIMPLE + ''',''' + ED_ST_SEP + ''',''' + ED_ST_PENSION + ''','''
    + ED_ST_MASS_RETIREMENT + ''',''' + ED_ST_TIAA_CREFF + ''','''+ED_ST_ROTH_IRA + ''','''
    + ED_ST_ROTH_401K + ''',''' + ED_ST_ROTH_403B + ''',''' + ED_ST_ROTH_CATCH_UP + ''','''
    + ED_ST_401K_CATCH_UP + ''',''' + ED_ST_403B_CATCH_UP + ''',''' + ED_ST_457_CATCH_UP + ''','''
    + ED_ST_501C_CATCH_UP + ''',''' + ED_ST_SIMPLE_CATCH_UP + ''',''' + ED_ST_CATCH_UP + ''','''
    + ED_ST_STATE_PENSION + ''','''
    + ED_DED_AFTER_TAX_PENSION + ''','''
    + ED_ST_HSA_CATCH_UP + '''';
end;

function TypeIsHSA(ACode: String): Boolean;
begin
  Result := (ACode = ED_ST_HSA_SINGLE)
    or (ACode = ED_ST_HSA_FAMILY)
    or (ACode = ED_ST_HSA_CATCH_UP)
    or (ACode = ED_MEMO_ER_HSA_SINGLE)
    or (ACode = ED_MEMO_ER_HSA_FAMILY)
    or (ACode = ED_EWOD_TAXABLE_ER_HSA_SINGLE)
    or (ACode = ED_EWOD_TAXABLE_ER_HSA_FAMILY);
end;

function TypeIsPension(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListPensionTypes) <> 0;
end;

function ListCatchUpTypes: String;
begin
  Result := '''' + ED_ST_ROTH_CATCH_UP + ''','''
    + ED_ST_401K_CATCH_UP + ''',''' + ED_ST_403B_CATCH_UP + ''',''' + ED_ST_457_CATCH_UP + ''','''
    + ED_ST_501C_CATCH_UP + ''',''' + ED_ST_SIMPLE_CATCH_UP + ''',''' + ED_ST_CATCH_UP + ''','''
    + ED_ST_HSA_CATCH_UP + '''';
end;

function TypeIsCatchUp(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListCatchUpTypes) <> 0;
end;

function List3rdPartyTypes: String;
begin
  Result := '''' + ED_3RD_SHORT + ''',''' + ED_3RD_LONG + ''',''' + ED_3RD_NON_TAX + '''';
end;

function TypeIs3rdParty(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', List3rdPartyTypes) <> 0;
end;

function TypeIsCobraCredit(EDCodeType, EDCodeDescription: string): Boolean;
begin
  Result := (EDCodeType = ED_MEMO_COBRA_CREDIT) or
         ((EDCodeType = ED_MEMO_SIMPLE) and (Pos('COBRA', UpperCase(EDCodeDescription)) > 0));
end;

function TypeIsManagerApproval(PRType: string): Boolean;
begin
  Result := Pos('''' + PRType + '''', ListManagerApprovalTypes) <> 0;
end;

function TypeIsTaxableMemo(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListTaxableMemoTypes) <> 0;
end;

function TypeIsTipsBefore2014(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListTipsTypesBefore2014) <> 0;
end;

function TypeIsTipsAfter2013(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListTipsTypesAfter2013) <> 0;
end;

function ListDistributedMemoTypes: String;
begin
  Result := '''' + ED_MEMO_SIMPLE + ''',''' + ED_MEMO_TIPPED_SALES + ''',''' + ED_MEMO_NM_WORKERS_COMP + ''','''
    + ED_MEMO_WY_WORKERS_COMP + ''',''' + ED_MEMO_ER_INSURANCE_PREMIUM + ''',''' + ED_MEMO_WASHINGTON_L_I + '''';
end;

function TypeIsDistributedMemo(EDCodeType: string): Boolean;
begin
  Result := Pos('''' + EDCodeType + '''', ListDistributedMemoTypes) <> 0;
end;

function TypeIsCorrectionRun(PRType: string): Boolean;
begin
  Result := Pos('''' + PRType + '''', ListCorrectionRunTypes) <> 0;
end;

function ThisIsTaxOrWages(MyString: string): Boolean;
var
  L: Integer;
begin
  Result := False;
  L := Length(MyString);
  if (copy(MyString, L - 2, 3) = 'TAX') or (copy(MyString, L - 4, 5) = 'WAGES')  or (copy(MyString, L - 3, 4) = 'WAGE') then
    Result := True;
end;

function Is_ER_SUI(SUI_Type: string): Boolean;
begin
  if SUI_Type[1] in [GROUP_BOX_ER, GROUP_BOX_ER_OTHER] then
    Result := True
  else
    Result := False;
end;

function GetEFTPSTaxCode(TaxName: string): string;
begin
  Result := '';
  if Pos('1042', TaxName) <> 0 then
    Result := '10425'
  else
    if Pos('1120', TaxName) <> 0 then
    Result := '11206'
  else
    if Pos('720', TaxName) <> 0 then
    Result := '72005'
  else
    if Pos('940', TaxName) <> 0 then
    Result := '09405'
  else
    if Pos('941', TaxName) <> 0 then
    Result := '94105'
  else
    if Pos('943', TaxName) <> 0 then
    Result := '09435'
  else
    if Pos('944', TaxName) <> 0 then
    Result := '94405'
  else
    if Pos('945', TaxName) <> 0 then
    Result := '09455'
  else
    if Pos('990C', TaxName) <> 0 then
    Result := '99026'
  else
    if Pos('990PF', TaxName) <> 0 then
    Result := '99036'
  else
    if Pos('990T', TaxName) <> 0 then
    Result := '99046'
  else
    if Pos('CT-1', TaxName) <> 0 then
    Result := '10005';
end;

procedure LockResource(const TypeName, Tag, FailureMessage: string; const LockType: TResourceLockType = rlTryOnce);
var
  ResInfo: IevGlobalFlagInfo;
begin
  while LockType in [rlTryOnce, rlTryUntilGet] do
  begin
    if mb_GlobalFlagsManager.TryLock(TypeName, Tag, ResInfo) then
      Break;

    if (LockType = rlTryOnce) and Assigned(ResInfo) then
      raise ELockedResource.CreateHelp(FailureMessage + ResInfo.UserName, IDH_LockedResource)
    else
      Snooze(5000);  // Very important to do! Otherwise it'll eat all CPU.
  end;
end;

procedure UnLockResource(const TypeName, Tag: string; const LockType: TResourceLockType = rlTryOnce);
var
  ResInfo: IevGlobalFlagInfo;
begin
  while LockType in [rlTryOnce, rlTryUntilGet] do
  begin
    if mb_GlobalFlagsManager.TryUnlock(TypeName, Tag, ResInfo) then
      Break;

    if LockType = rlTryOnce then
      raise EevException.Create('Cannot unlock resource')
    else
      Snooze(5000);  // Very important to do! Otherwise it'll eat all CPU.
  end;
end;

function HashTotalABA(S: string): Integer;  //gdy
  var J, Sum, H, Ks:Integer;
begin
  Assert( Length(s) in [8,9] );
  Sum := 0;
  for J := low(aWFactor) to high(aWFactor) do
      Sum := Sum + StrToInt(S[J]) * aWFactor[J];

  H := ((Sum - 1) div 10) * 10 + 10;
  Ks := H - Sum;
  result := Ks;
end;

//SEG BEGIN
function HashTotalABAOk(S: string): Integer;
  var J, Sum, H, Ks:Integer;
begin
  try
    if StrToInt(S) = 0 then
    begin
      result := 0;
      exit;
    end;

    Sum := 0;
    for J := low(aWFactor) to high(aWFactor) do
       Sum := Sum + StrToInt(S[J]) * aWFactor[J];

    H := ((Sum - 1) div 10) * 10 + 10;
    Ks := H - Sum;
    result := Ks - StrToInt(S[9]);
  except
    result := 1;
  end;
end;

procedure OpenDataSetForPayroll(aDataSet: TevClientDataSet; PrNbr: Integer);
begin
  aDataSet.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
  aDataSet.Open;
end;

procedure ODS(const Fmt: string; const Args: array of const); overload;
begin
  ODS( Format( Fmt, Args ) );
end;

procedure ODS(const msg: string); overload;
begin
  OutputDebugString(pchar(msg));
end;



//end gdy

function Get_EE_YTD(EmpNumber: Integer; StartDate, EndDate: TDateTime; const YTDType: String; aRecNumber: Integer): Double;
var
  Proc: IevStoredProc;
begin
  if ctx_AccountRights.Functions.GetState('FIELDS_WAGES') = stEnabled then
  begin
    Proc := TevStoredProc.Create('EE_YTD');
    Proc.Params.AddValue('EE_NUMBER', EmpNumber);
    Proc.Params.AddValue('START_DATE', StartDate);
    Proc.Params.AddValue('END_DATE', EndDate);
    Proc.Params.AddValue('YTD_TYPE', YTDType);
    Proc.Params.AddValue('RECORD_NUMBER', aRecNumber);
    Proc.Execute;
    Result := ConvertNull(Proc.Result.Value['YTD'], 0);
  end
  else
    Result := 0;
end;

function GetNextInvoiceNumber: Integer;
begin
  Result := ctx_DBAccess.GetGeneratorValue('G_NEXT_INVOICE_NUMBER', dbtBureau, 1);
end;

function Parse(const FormattedString, Index: string): string;
var
  l: TStringList;
begin
  l := TStringList.Create;
  with l do
  try
    CommaText := FormattedString;
    result := Values[Index];
  finally
    Free;
  end;
end;

function UnAttended: Boolean;
begin
  Result := GetCurrentThreadID <> MainThreadID;
end;

function IsDebugMode: Boolean;
begin
  Result := FDebugMode;
end;

function IsQAMode: Boolean;
begin
  Result := FQAMode;
end;

const
  Base64Table = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

function Base64Encode(Value: String): String;
var
  AIn  : array[1..3] of Byte;
  AOut : array[1..4] of Byte;
  AWork: array[1..3] of Byte;
  I: Integer;
  O: LongInt;
begin
  Result := '';
  I := 1;
  O := Length(Value);
  case Length(Value) mod 3 of
    1 : Value := Value + #0 + #0;
    2 : Value := Value + #0;
  end;
  while I < Length(Value) do
  begin
    AIn[1] := Byte(Value[I]);
    AIn[2] := Byte(Value[I+1]);
    AIn[3] := Byte(Value[I+2]);

    AOut[1] := Byte(AIn[1] shr 2);
    AWork[1] := Byte(AIn[1] shl 4);
    AWork[2] := Byte(AWork[1] and $30);
    AWork[3] := Byte(AIn[2] shr 4);
    AOut[2] := Byte(AWork[2] or AWork[3]);
    AWork[1] := Byte(AIn[2] shl 2);
    AWork[2] := Byte(AWork[1] and $3C);
    AWork[3] := Byte(AIn[3] shr 6);
    AOut[3] := Byte(AWork[2] or AWork[3]);
    AOut[4] := Byte(AIn[3] and $3F);

    Inc(I, 3);
    Result := Result + Base64Table[AOut[1]+1] + Base64Table[AOut[2]+1] + Base64Table[AOut[3]+1] + Base64Table[AOut[4]+1];
  end;
  if O mod 3 > 0 then
    Result[Length(Result)] := '=';
  if O mod 3 = 1 then
    Result[Length(Result)-1] := '=';
end;

function Base64Decode(const Value: String): String;
var
  AIn  : array[1..4] of Byte;
  AOut : array[1..3] of Byte;
  AWork: array[1..3] of Byte;
  I: Integer;
  C: Integer;
begin
  Result := '';
  I := 1;
  while I < Length(Value) do
  begin
    C := 3;
    FillChar(AWork, SizeOf(AWork), #0);
    FillChar(AOut, SizeOf(AWork), #0);
    AIn[1] := Byte(Pos(Value[I],Base64Table)-1);
    AIn[2] := Byte(Pos(Value[I+1],Base64Table)-1);
    AIn[3] := Byte(Pos(Value[I+2],Base64Table)-1);
    AIn[4] := Byte(Pos(Value[I+3],Base64Table)-1);
    if Value[I+3]='=' then
    begin
      C := 2;
      AIn[4] := 0;
      if Value[I+2]='=' then
      begin
        C := 1;
        AIn[3] := 0;
      end;
    end;
    AWork[2] := Byte(AIn[1] shl 2);
    AWork[3] := Byte(AIn[2] shr 4);
    AOut[1] := Byte(AWork[2] or AWork[3]);
    AWork[2] := Byte(AIn[2] shl 4);
    AWork[3] := Byte(AIn[3] shr 2);
    AOut[2] := Byte(AWork[2] or AWork[3]);
    AWork[2] := Byte(AIn[3] shl 6);
    AOut[3] := Byte(AWork[2] or AIn[4]);
    Result := Result + Char(AOut[1]);
    if C > 1 then
      Result := Result + Char(AOut[2]);
    if C > 2 then
      Result := Result + Char(AOut[3]);
    Inc(I, 4);
  end;
end;

function TextToBinary(Source: string): string;
begin
  if Copy(Source, 1, 6) <> '$MIME$' then
    Result := Source
  else
  begin
    Result := Base64Decode(Copy(Source, 7, Length(Source)));
    Result := Copy(Result, 1, StrToInt(Fetch(Result, ';')))
  end
end;

function BinaryToText(const Source: string): string;
label
  Code;
var
  i: Integer;
begin
  for i := 1 to Length(Source) do
    if not (Source[i] in [' '..'z']) then
      goto Code;
  Result := Source;
  Exit;

Code:
  Result := Base64Encode(IntToStr(Length(Source)) + ';' + Source);
  Result := '$MIME$' + Result
end;


procedure DecodeToStream(Source: string; Stream: TStream);
begin
  Source := Base64Decode(Copy(Source, 7, Length(Source)));
  Stream.Write(Source[1], Length(Source));
end;

function ScheduleToText(Schedule: string): string;
var
  t, st, d: string;
  i: Integer;
  b: Word;

  function ReadCommonInfo(var s: string): string;
  var
    st, sd: TDateTime;
    rt: Boolean;
    i, j: Integer;
  begin
    st := StrToFloat(Fetch(s));
    sd := StrToInt(Fetch(s));
    rt := Fetch(s) = 'Y';
    if rt then
    begin
      Result := 'Every ' + Fetch(s);
      i := StrToInt(Fetch(s));
      if i = 0 then
        Result := Result + ' minute(s)'
      else
        Result := Result + ' hour(s)';
      Result := Result + ' from ' + TimeToStr(st);
      rt := Fetch(s) = 'D';
      if not rt then
        Result := Result + ' to ' + TimeToStr(StrToFloat(Fetch(s)))
      else
      begin
        i := StrToInt(Fetch(s));
        j := StrToInt(Fetch(s));
        Result := Result + ' for ';
        if i > 0 then
          Result := Result + IntToStr(i) + ' hour(s)';
        if j > 0 then
        begin
          if i > 0 then
            Result := Result + ' and ';
          Result := Result + IntToStr(j) + ' minute(s)';
        end;
      end;
      Result := Result + ' %s, starting ' + DateToStr(sd);
    end
    else
      Result := 'At ' + TimeToStr(st) + ' %s, starting ' + DateToStr(sd);
    Fetch(s);
  end;

begin
  t := Fetch(Schedule);
  if t = 'D' then
  begin
    Result := ReadCommonInfo(Schedule);
    if Fetch(Schedule) = 'Y' then
      Result := Result + ' and ending ' + DateToStr(StrToInt(Fetch(Schedule)));
    i := StrToInt(Fetch(Schedule));
    if i = 1 then
      Result := Format(Result, ['every day'])
    else
      Result := Format(Result, ['every ' + IntToStr(i) +  ' days']);
  end
  else if t = 'W' then
  begin
    Result := ReadCommonInfo(Schedule);
    if Fetch(Schedule) = 'Y' then
      Result := Result + ' and ending ' + DateToStr(StrToInt(Fetch(Schedule)));
    i := StrToInt(Fetch(Schedule));
    b := StrToInt(Fetch(Schedule));
    st := '';
    if Boolean(b and $01) then
      st := st + ShortDayNames[2] + ',';
    if Boolean(b and $02) then
      st := st + ShortDayNames[3] + ',';
    if Boolean(b and $04) then
      st := st + ShortDayNames[4] + ',';
    if Boolean(b and $08) then
      st := st + ShortDayNames[5] + ',';
    if Boolean(b and $10) then
      st := st + ShortDayNames[6] + ',';
    if Boolean(b and $20) then
      st := st + ShortDayNames[7] + ',';
    if Boolean(b and $40) then
      st := st + ShortDayNames[1] + ',';
    if i = 1 then
      Result := Format(Result, ['every ' + Copy(st, 1, Pred(Length(st))) + ' of every week'])
    else
      Result := Format(Result, ['every ' + Copy(st, 1, Pred(Length(st))) + ' of every ' + IntToStr(i) +  ' weeks']);
  end
  else if t = 'M' then
  begin
    Result := ReadCommonInfo(Schedule);
    if Fetch(Schedule) = 'Y' then
      Result := Result + ' and ending ' + DateToStr(StrToInt(Fetch(Schedule)));
    if Fetch(Schedule) = 'D' then
    begin
      d := Fetch(Schedule);
      if d = 'L' then
        st := 'on last day'
      else
        st := 'on day ' + d
    end
    else
    begin
      st := 'on the ';
      case StrToInt(Fetch(Schedule)) of
      0: st := st + 'first ';
      1: st := st + 'second ';
      2: st := st + 'third ';
      3: st := st + 'forth ';
      4: st := st + 'last ';
      end;
      case StrToInt(Fetch(Schedule)) of
      0: st := st + ShortDayNames[2];
      1: st := st + ShortDayNames[3];
      2: st := st + ShortDayNames[4];
      3: st := st + ShortDayNames[5];
      4: st := st + ShortDayNames[6];
      5: st := st + ShortDayNames[7];
      6: st := st + ShortDayNames[1];
      end;
    end;
    b := StrToInt(Fetch(Schedule));
    if b = $0FFF then
      Result := Format(Result, [st + ' of every month'])
    else
    begin
      st := st + ' of ';
      if (b and $0001) <> 0 then
        st := st + ShortMonthNames[1] + ',';
      if (b and $0002) <> 0 then
        st := st + ShortMonthNames[2] + ',';
      if (b and $0004) <> 0 then
        st := st + ShortMonthNames[3] + ',';
      if (b and $0008) <> 0 then
        st := st + ShortMonthNames[4] + ',';
      if (b and $0010) <> 0 then
        st := st + ShortMonthNames[5] + ',';
      if (b and $0020) <> 0 then
        st := st + ShortMonthNames[6] + ',';
      if (b and $0040) <> 0 then
        st := st + ShortMonthNames[7] + ',';
      if (b and $0080) <> 0 then
        st := st + ShortMonthNames[8] + ',';
      if (b and $0100) <> 0 then
        st := st + ShortMonthNames[9] + ',';
      if (b and $0200) <> 0 then
        st := st + ShortMonthNames[10] + ',';
      if (b and $0400) <> 0 then
        st := st + ShortMonthNames[11] + ',';
      if (b and $0800) <> 0 then
        st := st + ShortMonthNames[12] + ',';
      Result := Format(Result, [Copy(st, 1, Pred(Length(st)))]);
    end
  end
  else if t = 'O' then
  begin
    Result := ReadCommonInfo(Schedule);
    Result := StringReplace(Result, ' %s, starting ', ' on ', []);
  end
  else if t = 'S' then
    Result := 'Run at system startup';
end;

function FrequencyIn(OneFrequency, MultipleFrequencies: string): Boolean;
begin
  Result := False;
  if (OneFrequency[1] = FREQUENCY_TYPE_WEEKLY) and (MultipleFrequencies[1] in [CO_FREQ_WEEKLY, CO_FREQ_WEEKLY_BWEEKLY,
    CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_WEEKLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY,
      CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]) then
    Result := True;
  if (OneFrequency[1] = FREQUENCY_TYPE_BIWEEKLY) and (MultipleFrequencies[1] in [CO_FREQ_BWEEKLY, CO_FREQ_WEEKLY_BWEEKLY,
    CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY,
      CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]) then
    Result := True;
  if (OneFrequency[1] = FREQUENCY_TYPE_SEMI_MONTHLY) and (MultipleFrequencies[1] in [CO_FREQ_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY,
    CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY,
      CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]) then
    Result := True;
  if (OneFrequency[1] = FREQUENCY_TYPE_MONTHLY) and (MultipleFrequencies[1] in [CO_FREQ_MONTHLY, CO_FREQ_WEEKLY_MONTHLY,
    CO_FREQ_BWEEKLY_MONTHLY, CO_FREQ_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY,
      CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]) then
    Result := True;
  if (OneFrequency[1] = FREQUENCY_TYPE_DAILY) and (MultipleFrequencies[1] = CO_FREQ_DAILY) then
    Result := True;
  if (OneFrequency[1] = FREQUENCY_TYPE_QUARTERLY) and (MultipleFrequencies[1] = CO_FREQ_QUARTERLY) then
    Result := True;
end;

procedure SortDSBy( ds: TEvClientDataSet; aFieldNames, aDescFields: string );
var
  i: integer;
const
  sIndexName = 'SORT'; //'CASE_SENS_SORT'
begin
  with ds do
  begin
    DisableControls;
    try
      IndexDefs.Update;
      i := IndexDefs.IndexOf( sIndexName );
      if i <> -1 then
        DeleteIndex( sIndexName );
      if aFieldNames <> '' then
        AddIndex( sIndexName, aFieldNames, [ixCaseInsensitive], aDescFields);
      IndexDefs.Update;
      if aFieldNames <> '' then
        IndexName := sIndexName
      else
      begin
        IndexName := '';
        IndexFieldNames := '';
      end;
    finally
      EnableControls;
    end;
  end;
end;

procedure CheckFileSize(FileName: string; MaxSize: DWord);
var
  h: THandle;
  fs: DWord;
begin
  h := CreateFile(PChar(FileName + #0), GENERIC_READ, FILE_SHARE_WRITE, nil, OPEN_EXISTING, 0, 0);
  if h = INVALID_HANDLE_VALUE then
    RaiseLastOSError;
  try
    fs := GetFileSize(h, nil);
    if fs = INVALID_FILE_SIZE then
      RaiseLastOSError;
    if fs > MaxSize * 1024 then
      raise ECanNotPerformOperation.CreateHelp('File size must be less than ' + IntToStr(MaxSize) + 'k', IDH_CanNotPerformOperation);
  finally
    if not CloseHandle(h) then
      RaiseLastOSError;
  end;
end;

function GetToken(const s: string; var p: Integer; out token: string): Boolean;
var
  i, j: Integer;
begin
  i := Length(s);
  j := p;
  while (j <= i) and not (s[j] in [#9, #13])  do
    Inc(j);
  token := Copy(s, p, j - p);
  p := j + 1;
  Result := p > i;
end;

function CheckVmrLicenseActive: Boolean;
begin
  Result := CheckVmrActive and (ctx_AccountRights.Functions.GetState('VMR') = stEnabled);
end;

function CheckVmrActive: Boolean;
begin
  DM_SERVICE_BUREAU.SB_OPTION.Active := true;
  Result := Context.License.VMR and
       not (ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', BlockVmrTag, 'OPTION_STRING_VALUE'), 'N')='Y');
end;

function CheckVmrSetupActive: Boolean;
begin
  with DM_CLIENT.CL do
    Result := Active
    and not FieldByName('AGENCY_CHECK_MB_GROUP_NBR').IsNull
    and not FieldByName('PR_CHECK_MB_GROUP_NBR').IsNull
    and not FieldByName('PR_REPORT_MB_GROUP_NBR').IsNull
    //and not FieldByName('PR_REPORT_SECOND_MB_GROUP_NBR').IsNull
    and not FieldByName('TAX_CHECK_MB_GROUP_NBR').IsNull
    and not FieldByName('TAX_EE_RETURN_MB_GROUP_NBR').IsNull
    and not FieldByName('TAX_RETURN_MB_GROUP_NBR').IsNull
    //and not FieldByName('TAX_RETURN_SECOND_MB_GROUP_NBR').IsNull
    ;
end;

function CheckRemoteVMRActive: Boolean;
begin
  Result := CheckVmrActive and
    (ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', RemoteVmrTag, 'OPTION_STRING_VALUE'), 'N')='Y');
end;

function SbSendsTaxPaymentsForCompany(TaxServiceLevel: string = ''): Boolean;
begin
  if TaxServiceLevel = '' then
    TaxServiceLevel := DM_COMPANY.CO.TAX_SERVICE.AsString;
  Result := (TaxServiceLevel = TAX_SERVICE_FULL) or (TaxServiceLevel = TAX_SERVICE_DIRECT);
end;

function CompanyRequiresTaxesBeImpounded(TaxServiceLevel: string = ''): Boolean;
begin
  if TaxServiceLevel = '' then
    TaxServiceLevel := DM_COMPANY.CO.TAX_SERVICE.AsString;
  Result := (TaxServiceLevel = TAX_SERVICE_FULL);
end;

function GetFieldValueListString(const LiabilityDataSet: TevClientDataSet; const FieldName: string): string;
var
  l: TStringList;
begin
  l := GetFieldValueList(LiabilityDataSet, FieldName);
  try
    Result := l.CommaText;
  finally
    l.Free;
  end;
end;

function GetFieldValueList(const LiabilityDataSet: TevClientDataSet; const FieldName: string; const ExcludeNulls: Boolean = False): TStringList;
begin
  Result := LiabilityDataSet.GetFieldValueList(FieldName, ExcludeNulls);
end;

function GetVarArrayFromStrings(const s: TStringList; const VarType: TVarType = varVariant): Variant;
var
  i: Integer;
begin
  Result := VarArrayCreate([0, s.Count-1], varVariant);
  for i := 0 to s.Count-1 do
    Result[i] := VarAsType(s[i], VarType);
end;

function GetVarArrayFieldValueList(const LiabilityDataSet: TevClientDataSet; const FieldName: string;
  const VarType: TVarType = varVariant; const ExcludeNulls: Boolean = False): Variant;
var
  s: TStringList;
begin
  s := GetFieldValueList(LiabilityDataSet, FieldName, ExcludeNulls);
  try
    Result := GetVarArrayFromStrings(s, VarType);
  finally
    s.Free;
  end;
end;

function UplinkFieldCondition(const FieldName: string; const Ds: TevClientDataSet): string;
var
  f: TField;
  i: Integer;
  iKey, iMaxKey, iMinKey: Integer;
  s: string;
  d: TDataSet;
begin
  Assert(Assigned(Ds));
  i := 1;
  iMaxKey := Low(iMaxKey);
  iMinKey := High(iMinKey);
  Result := '';
  s := '';
  d := Ds.ShadowDataSet;
  f := d.FieldByName(FieldName);
  d.First;
  while not d.Eof do
  begin
    iKey := f.AsInteger;
    s := s+ ','+ IntToStr(iKey);
    iMaxKey := Max(iKey, iMaxKey);
    iMinKey := Min(iKey, iMinKey);
    Inc(i);
    d.Next;
    if d.Eof or ((i mod 1400) = 0) then
      if s <> '' then
      begin
        Delete(s, 1, 1);
        if Result = '' then
          Result := FieldName+ ' in ('+ s+ ') '
        else
          Result := Result+ ' or '+ FieldName+ ' in ('+ s+ ') ';
        s := '';
      end;
  end;
  if Result = '' then
    Result := AlwaysFalseCond
  else
  if Length(Result) > 20*1024 then // to stay below 32K limit
    Result := '('+ FieldName+ ' between '+ IntToStr(iMinKey)+ ' and '+ IntToStr(iMaxKey)+ ')'
  else
    Result := '('+ Result+ ')';
end;

procedure CreateFullTable(aTableName: string; var aClientDataSet: TevClientDataSet; Columns: String = '*');
var
  Q: IevQuery;
begin
  aClientDataSet := TevClientDataSet.Create(nil);
  aClientDataSet.Name := aTableName;
  Q := TevQuery.Create('GenericSelectOrdered');
  Q.Macros.AddValue('COLUMNS', Columns);
  Q.Macros.AddValue('TableName', aTableName);
  Q.Macros.AddValue('Order', aTableName + '_NBR, EFFECTIVE_DATE ');
  Q.Execute;
  aClientDataSet.Data := Q.Result.Data;
end;

function LocateAsOfDateRecord(DataSet: TevClientDataSet; KeyFields: string; KeyFieldValues: Variant; AsOfDate: TDateTime; ErrorWhenNoLocate: Boolean = False): Boolean;

var
  EffectiveDateField, ActiveRecordField, NbrField: TField;

  function RecordMatches: Boolean;
  var
    aFieldName, FieldsLeft: string;
    Position, Count: Integer;
    aValue: Variant;
  begin
    Result := True;
    FieldsLeft := KeyFields;
    Count := 0;
    repeat
      Position := Pos(';', FieldsLeft);
      if Position = 0 then
      begin
        aFieldName := FieldsLeft;
        FieldsLeft := '';
      end
      else
      begin
        aFieldName := Copy(FieldsLeft, 1, Position - 1);
        Delete(FieldsLeft, 1, Position);
      end;

      if VarIsArray(KeyFieldValues) then
        aValue := KeyFieldValues[Count]
      else
        aValue := KeyFieldValues;

      if DataSet.FieldByName(aFieldName).Value <> aValue then
      begin
        Result := False;
        Break;
      end;
      Inc(Count);
    until FieldsLeft = '';
  end;

  function PinpointRecord: Boolean;
  var
    RecordNbr: Integer;
  begin
    Result := False;
    if (AsOfDate >= EffectiveDateField.AsDateTime) and RecordMatches then
    begin
      RecordNbr := NbrField.AsInteger;
      DataSet.Next;
      while (AsOfDate >= EffectiveDateField.AsDateTime)
      and (RecordNbr = NbrField.AsInteger)
      and RecordMatches and not DataSet.EOF do
        DataSet.Next;
      if (AsOfDate >= EffectiveDateField.AsDateTime) and not DataSet.EOF and (RecordNbr = NbrField.AsInteger) then
      else
      begin
        if (AsOfDate < EffectiveDateField.AsDateTime) or not RecordMatches
        or (RecordNbr <> NbrField.AsInteger) then
          DataSet.Prior;
        if ActiveRecordField.AsString <> 'N' then
          Result := True;
      end;
    end;
  end;

var
  I: Integer;
  V: String;
begin
  EffectiveDateField := DataSet.FieldByName('EFFECTIVE_DATE');
  ActiveRecordField := DataSet.FieldByName('ACTIVE_RECORD');
  NbrField := DataSet.FieldByName(DataSet.Name + '_NBR');
  if PinpointRecord then
  begin
    Result := True;
    Exit;
  end;
  Result := False;
  DataSet.First;
  while not DataSet.EOF do
  begin
    if PinpointRecord then
    begin
      Result := True;
      Break;
    end
    else
      DataSet.Next;
  end;
  if not Result and ErrorWhenNoLocate then
  begin
    if VarIsArray(KeyFieldValues) then
    begin
      V := '';
      for I := VarArrayLowBound(KeyFieldValues, 1) to VarArrayHighBound(KeyFieldValues, 1) do
        V := V + VarToStr(KeyFieldValues[I]) + ';';
      Delete(V, 1, Length(V));
    end
    else
      V := VarToStr(KeyFieldValues);

    raise EInconsistentData.CreateHelp('A record could not be located. Table: ' + DataSet.Name
      + ', Key Fields: ' + KeyFields + ', Key Values: ' + V + ', As of Date: ' + DateToStr(AsOfDate), IDH_InconsistentData);
  end;
end;

function GetAsOfDateValue(DataSet: TevClientDataSet; KeyFields: string; KeyFieldValues: Variant; ResultField: string; AsOfDate: TDateTime;
  ReturnLastValue: Boolean = False; ErrorOnNull: Boolean = False; ReturnToBookmark: Boolean = False): Variant;
var
  MyBookmark: TBookmark;
begin
  Result := Null;
  if ReturnToBookmark then
    MyBookmark := DataSet.GetBookmark;
  try
    if LocateAsOfDateRecord(DataSet, KeyFields, KeyFieldValues, AsOfDate, ErrorOnNull) or (ReturnLastValue) then
      Result := DataSet.FieldByName(ResultField).Value;
    if ReturnToBookmark then
      DataSet.GotoBookmark(MyBookmark);
  finally
    if ReturnToBookmark then
      DataSet.FreeBookmark(MyBookmark);
  end;
end;

function Iff(const Condition: Boolean; const TruePart, FalsePart: Variant): Variant;
begin
  if Condition then
    Result := TruePart
  else
    Result := FalsePart;
end;

function CreateFakeSSN: string;
var
  t: TDateTime;
  y, m, d, h, mm, ss, ms: Word;
  Nbr1, Nbr2: String;

  function Convert10to36(ANbr: Integer): String;
  const
    aLtrs = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var
    r: Integer;
  begin
    Result := '';
    while ANbr > 0 do
    begin
      r := ANbr mod 36;
      Result := aLtrs[r + 1] + Result;
      ANbr := ANbr div 36;
    end;
  end;

begin
  t := SysTime;
  DecodeDateTime(t, y, m, d, h, mm, ss, ms);
  Nbr1 := Convert10to36((m - 1) * 31 + d);
  Nbr1 := StringOfChar('0', 2 - Length(Nbr1)) + Nbr1;
  Nbr2 := Convert10to36((h * 60 + mm) * 60 + ss);
  Nbr2 := StringOfChar('0', 4 - Length(Nbr2)) + Nbr2;
  Result := 'xxx-' + Nbr1 + '-' + Nbr2;
end;

function BlockHr: Boolean;
begin
  Result:=  False;

   //  Changed  Client level logic...
   //  Created Company level Enable hr Field with Manchester.. Maybe, This field will implement..
{
  Result := Context.UserAccount.AccountType = uatRemote;
  if Result then
  begin
    DM_CLIENT.CL.Activate;
    Result := (DM_CLIENT.CL.FieldByName('ENABLE_HR').AsString = 'N') or
              (DM_CLIENT.CL.FieldByName('ENABLE_HR').AsString = 'S');
  end;
}
end;

function DataSetsToVarArray(const Value: array of TevClientDataSet): Variant;
var
  i: Integer;
begin
  Result := VarArrayCreate([0, Length(Value)-1], varVariant);
  for i := Low(Value) to High(Value) do
    Result[i] := Value[i].Data;
end;

procedure CheckPassword(Text: string);
  function CheckMixed(Text: string): Boolean;
  var
    i: Integer;
    bL, bD, bS, bU: Boolean;
  begin
    bL := False;
    bU := False;
    bD := False;
    bS := False;
    for i := 1 to Length(Text) do
    begin
      if Text[i] in ['a'..'z'] then
        bL := True
      else if Text[i] in ['A'..'Z'] then
        bU := True
      else if Text[i] in ['0'..'9'] then
        bD := True
      else if Text[i] in ['!','@','#','%','^','&','*','(',')','_','-','=','+','{','}','[',']','|','\',':',';','"','''','<','>'] then
        bS := True;
    end;
    i := 0;
    if bL then
      Inc(i);
    if bU then
      Inc(i);
    if bD then
      Inc(i);
    if bS then
      Inc(i);
    Result := i >= 3;
  end;
begin
  DM_SERVICE_BUREAU.SB.Activate;
  if (DM_SERVICE_BUREAU.SB.PSWD_MIN_LENGTH.Value <> 0) and
     (Length(Text) < DM_SERVICE_BUREAU.SB.PSWD_MIN_LENGTH.Value) then
    raise EInvalidLogin.Create('Password length can not be less than ' + DM_SERVICE_BUREAU.SB.PSWD_MIN_LENGTH.AsString + ' characters');
  if (DM_SERVICE_BUREAU.SB.PSWD_FORCE_MIXED.Value = GROUP_BOX_YES) and
     not CheckMixed(Text) then
    raise EInvalidLogin.Create('Password does not meet complexity requirements');
end;

procedure VerifyManualCheckChange(PrNbr, CheckNbr: String);
begin
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then
      Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'count(*)');
      SetMacro('TABLE1', 'PR_REPRINT_HISTORY');
      SetMacro('TABLE2', 'PR_REPRINT_HISTORY_DETAIL');
      SetMacro('JOINFIELD', 'PR_REPRINT_HISTORY');
      SetMacro('CONDITION', 't2.MISCELLANEOUS_CHECK=''N'' and t1.PR_NBR=' + PrNbr
        + ' and t2.BEGIN_CHECK_NUMBER<=' + CheckNbr + ' and t2.END_CHECK_NUMBER>=' + CheckNbr);
      DataRequest(AsVariant);
    end;
    Open;
    if Fields[0].Value > 0 then
      raise EUpdateError.CreateHelp('You can not change manual check that has been printed!', IDH_ConsistencyViolation);
  end;
end;

function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
var
  d: TDateTime;
  i: Integer;
begin
  if Days > 0 then
    d := DefaultDate+ 1
  else
    d := DefaultDate;
  i := 1;
  repeat
    if (DayOfWeek(d) in [1, 7])
    or not DateIsNotFedHoliday(SY_GLOBAL_AGENCY_NBR, d) then
      d := d+ 1
    else
    if i < Days then
    begin
      d := d+ 1;
      Inc(i);
    end
    else
      Break;
  until False;
  Result := d;
end;

function CheckForReversedNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
var
  d: TDateTime;
  i: Integer;
begin
  if Days > 0 then
    d := DefaultDate- 1
  else
    d := DefaultDate;
  i := 1;
  repeat
    if (DayOfWeek(d) in [1, 7])
    or not DateIsNotFedHoliday(SY_GLOBAL_AGENCY_NBR, d) then
      d := d- 1
    else
    if i < Days then
    begin
      d := d- 1;
      Inc(i);
    end
    else
      Break;
  until False;
  Result := d
end;

function DateIsNotFedHoliday(const SY_GLOBAL_AGENCY_NBR: Integer; const d: TDateTime): Boolean;
var
  HolidayRule: Char;
begin
  Result := not DateIsFederalHoliday(d, HolidayRule, SY_GLOBAL_AGENCY_NBR);
end;

function  NormalizeFileName(const AFileName: String): String;
var
  i: Integer;
  c: Char;
  fPath: String;
begin
  Result := ExtractFileName(AFileName);
  fPath := ExtractFilePath(AFileName);

  for i := 1 to Length(Result) do
  begin
    c := AnsiUpperCase(Result[i])[1];
    if not((c >= '0') and (c <= '9') or (c >= 'A') and (c <= 'Z')) or (Pos(c, ' .,;~_[]{}!@#$%^()-+=`/\') > 0) then
      Result[i] := '_';
  end;

  Result := NormalizePath(fPath) + Result;
end;


function IsCoAsOfDateTable(const TableName: string): Boolean;
begin
  Result := (LeftStr(TableName, 2) = 'CO') and
             (TableName <> 'CO_FED_TAX_LIABILITIES') and
             (TableName <> 'CO_STATE_TAX_LIABILITIES') and
             (TableName <> 'CO_LOCAL_TAX_LIABILITIES') and
             (TableName <> 'CO_SUI_LIABILITIES') and
             (TableName <> 'CO_PR_ACH') and
             (TableName <> 'CO_BILLING_HISTORY') and
             (TableName <> 'CO_BILLING_HISTORY_DETAIL') and
             (TableName <> 'CO_BANK_ACCOUNT_REGISTER') or
            (LeftStr(TableName, 2) = 'EE');
end;

function IsSbAsOfDateTable(const TableName: string): Boolean;
begin
  Result := (TableName = 'SB') or
            (TableName = 'SB_ACCOUNTANT') or
            (TableName = 'SB_AGENCY') or
            (TableName = 'SB_BANKS') or
            (TableName = 'SB_DELIVERY_COMPANY') or
            (TableName = 'SB_DELIVERY_COMPANY_SVCS') or
            (TableName = 'SB_BANK_ACCOUNTS') or
            (TableName = 'SB_USER') or
            (TableName = 'SB_SERVICES') or
            (TableName = 'SB_SERVICES_CALCULATIONS') or
            (TableName = 'SB_REFERRALS') or
            (TableName = 'SB_TEAM') or
            (TableName = 'SB_TEAM_MEMBERS') or
            (TableName = 'SB_REPORTS');
end;


function SumDataSet(const ADataSet: TevClientDataSet; const AFieldName: String; const ACondition: String = ''): Variant;
var
  DS: TevClientDataSet;
  F: TField;
begin
  DS := TevClientDataSet.Create(nil);
  try
    DS.CloneCursor(ADataSet, False);
    if ACondition <> '' then
    begin
      if DS.Filter <> '' then
         DS.Filter := DS.Filter + ' and ' + ACondition
      else
         DS.Filter := ACondition;
      DS.Filtered := True;
    end;

    F := DS.FieldByName(AFieldName);
    DS.First;
    Result := 0;

    while not DS.Eof do
    begin
      Result := Result + F.AsFloat;
      DS.Next;
    end;
  finally
    DS.Free;
  end;
end;

function GetReportsByType(const AType : string; const ALevels: string; AOwner: TComponent): TevClientDataSet;

  procedure AddLevel(const ALev: String; const ALevPrif: String);
  var
    DS: TevClientDataSet;
    s : String;
  begin
    DS := TevClientDataSet.Create(nil);
    try
      s := 'select r.' +
           ALevPrif +'_report_writer_reports_nbr, r.report_description from ' +
           ALevPrif +'_report_writer_reports r where {AsofNow<r>} and r.report_type in ('+ AType +') ';

      CustomQuery(DS, S, ALev, nil);

      DS.First;
      while not DS.Eof do
      begin
        Result.Append;
        Result.Fields[0].AsString := ALev;
        Result.Fields[1].AsInteger := DS.Fields[0].AsInteger;
        Result.Fields[2].AsString := DS.Fields[1].AsString;
        Result.Post;
        DS.Next;
      end;
    finally
      DS.Free;
    end;
  end;

begin
  Result := TevClientDataSet.Create(AOwner);
  try
    Result.FieldDefs.Add('Level', ftString, 1);
    Result.FieldDefs.Add('Nbr', ftInteger);
    Result.FieldDefs.Add('Description', ftString, 64);
    Result.CreateDataSet;

    if Pos(CH_DATABASE_SYSTEM, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SYSTEM, 'SY');

    if Pos(CH_DATABASE_SERVICE_BUREAU, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SERVICE_BUREAU, 'SB');

    if Pos(CH_DATABASE_CLIENT, ALevels) <> 0 then
      AddLevel(CH_DATABASE_CLIENT, 'CL');

    Result.First;

  except
    Result.Free;
    raise;
  end;
end;

function GetQAMode: Boolean;
begin
  Result := isISystemsDomain;
end;


function GetDebugMode: Boolean;
begin
  Result := mb_AppConfiguration.AsBoolean['General\DebugMode'];
  if Result then
    Result := GetQAMode;
end;

function ValidateABANumber(ABANumber: Variant): integer;
begin
    Result := GetValidateABAResult(ABANumber);
    case Result of
      1:raise EUpdateError.CreateHelp(ValidateABA_MSGS[1], IDH_ConsistencyViolation);
      2:raise EInvalidHashTotal.CreateHelp(ValidateABA_MSGS[2], IDH_ConsistencyViolation);
      3:raise EInvalidHashTotal.CreateHelp(ValidateABA_MSGS[3], IDH_ConsistencyViolation);
    end
end;

function GetValidateABAResult(ABANumber: Variant): integer;
var
  L : integer;
begin
   Result := 0;
   L := Length(VarToStr(ABANumber));
   if (L <> 9) and (L <> 0) then
   begin
     Result := 1;
     exit;
   end;
   if (VarToStr(ABANumber) <> '') then
   begin
     if HashTotalABAOk(VarToStr(ABANumber)) <> 0 then
     begin
       Result := 2;
       exit;
     end;
     if VarToStr(ABANumber)[1] = '5' then
     begin
       Result := 3;
     end;
   end;
end;

function IsMaintenanceMode: Boolean;
var
  DBList: IisStringList;
begin
  DBList := ctx_DBAccess.GetDisabledDBs;
  DBList.CaseSensitive := False;
  Result := (DBList.IndexOf('*') <> -1) or
            (DBList.IndexOf('SYSTEM') <> -1) or
            (DBList.IndexOf('S_BUREAU') <> -1) or
            (DBList.IndexOf('TMP_TBLS') <> -1);
end;


function CheckLicenses: Boolean;
var
  i: Integer;
  D: IevDomainInfo;
  sError: String;
begin
  sError := '';
  Mainboard.ContextManager.StoreThreadContext;
  try
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      begin
        D := mb_GlobalSettings.DomainInfoList[i];
        try
          Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, D.DomainName), '');
          Context.License.Check;
        except
          on E: Exception do
            AddStrValue(sError, 'SB: ' + D.DomainName + '   ' + E.Message, #13);
        end;
      end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;

  if sError <> '' then
  begin
    Result := False;
    Mainboard.LogFile.AddEvent(etError, LOG_EVENT_CLASS_DIAGNOSTIC, sError, '');
  end
  else
    Result := True;
end;


function BuildINsqlStatement(const ALeftSide: String; const AItems: String): String;
var
  Items: IisStringList;
begin
  Items := TisStringList.Create;
  Items.CommaText := AItems;
  Result := BuildINsqlStatement(ALeftSide, Items);
end;


function BuildINsqlStatement(const ALeftSide: String; const AItems: IisStringList): String;
var
  Items: IisIntegerList;
  i: Integer;
begin
  Items := TisIntegerList.Create;
  Items.Capacity := AItems.Count;
  for i := 0 to AItems.Count - 1 do
    Items.Add(StrToInt(AItems[i]));

  result := BuildINsqlStatement(ALeftSide, Items);
end;

function BuildINsqlStatement(const ALeftSide: String; const AItems: IisIntegerList): String;
var
  s: String;
  i, iCounter: Integer;
begin
  Result := '';
  s := '';
  iCounter := 0;
  for i := 0 to AItems.Count - 1 do
  begin
    if s <> '' then
      s := s + ',';
    s := s + IntToStr(AItems[i]);
    Inc(iCounter);
    if iCounter = 1490 then  // Firebird IN statement 1500 limitation
    begin
      AddStrValue(Result, ALeftSide + ' IN (' + s + ')', ' OR ');
      s := '';
      iCounter := 0;
    end;
  end;

  if s <> '' then
    AddStrValue(Result, ALeftSide + ' IN (' + s + ')', ' OR ');

  if Result <> '' then
    Result := '(' + Result + ')';
end;



procedure CheckSSN(const ASSN: String; const AShortCheck: boolean = false); // do not pass EIN!!
const
  sInvalidSSN = 'Invalid SSN (';
var
  tmp, tmp1 : String;
  i : integer;

  function CheckIfNumberEquals(const SSN, G1, G2, G3: string): boolean;
  begin
    Result := False;
    if (Copy(SSN,1,3) = G1) and (Copy(SSN, 5,2) = G2) and (Copy(SSN, 8, 4) = G3) then
      Result := True;
  end;
begin
  tmp := Trim(ASSN);

  // checking length and delimiters
  if (Length(ASSN) <> 11) then
    raise Exception.Create(sInvalidSSN + tmp + '): wrong length');

  tmp1 := Copy(tmp, 4, 1);
  if (tmp1 <> '.') and (tmp1 <> '-') then
    raise Exception.Create(sInvalidSSN + tmp + '): unknown delimiter');

  tmp1 := Copy(tmp, 7, 1);
  if (tmp1 <> '.') and (tmp1 <> '-') then
    raise Exception.Create(sInvalidSSN + tmp + '): unknown delimiter');

  // generic characters check
  for i := 1 to Length(tmp) do
  begin
    if not (tmp[i] in ['0'..'9']) then
    begin
      if (i = 4) or (i = 7) then continue
      else
        raise Exception.Create(sInvalidSSN + tmp + '): not digital character');
    end;
  end;

  if AShortCheck then
    exit;

  // any field all zeroes
  if (Copy(tmp,1,3) = '000') or (Copy(tmp, 5,2) = '00') or (Copy(tmp, 8, 4) = '0000') then
    raise Exception.Create(sInvalidSSN + tmp + '): any field all zeroes');

  // 666 in positions 1-3
  if (Copy(tmp,1,3) = '666') then
    raise Exception.Create(sInvalidSSN + tmp + '): 666 in position 1-3');

  // leading number 9
  tmp1 := Copy(tmp, 1, 1);
  if (tmp1 = '9') then
    raise Exception.Create(sInvalidSSN + tmp + '): leading number 9');

  // all single digits are the same
//  if CheckIfNumberEquals(tmp, StringOfChar(tmp[1], 3), StringOfChar(tmp[1], 2), StringOfChar(tmp[1], 4)) then
//    raise Exception.Create(sInvalidSSN + tmp + '): all single digits are the same');

  // check numbers
  if CheckIfNumberEquals(tmp, '123', '45', '6789') or CheckIfNumberEquals(tmp, '078', '05', '1120') then
    raise Exception.Create(sInvalidSSN + tmp + '): prohibited number');
end;

procedure CheckEIN(const AEIN: String); // do not pass SSN
const
  sInvalidEIN = 'Invalid EIN (';
var
  tmp, tmp1 : String;
  i : integer;
begin
  tmp := Trim(AEIN);

  // checking length and delimiters
  if (Length(AEIN) <> 10) then
    raise Exception.Create(sInvalidEIN + tmp + '): wrong length');

  tmp1 := Copy(tmp, 3, 1);
  if (tmp1 <> '.') and (tmp1 <> '-') then
    raise Exception.Create(sInvalidEIN + tmp + '): unknown delimiter');

  // generic characters check
  for i := 1 to Length(tmp) do
  begin
    if not (tmp[i] in ['0'..'9']) then
    begin
      if (i = 3) then continue
      else
        raise Exception.Create(sInvalidEIN + tmp + '): not digital character');
    end;
  end;
end;

function CheckReadOnlyClientFunction: Boolean;
begin
 Result := ctx_AccountRights.Functions.GetState('UNLOCK_CL_READ_ONLY_MODE') <> stEnabled;
end;

function CheckReadOnlyCompanyFunction: Boolean;
begin
 Result := ctx_AccountRights.Functions.GetState('UNLOCK_CREDIT_HOLD') <> stEnabled;
end;

function CheckReadOnlyClientCompanyFunction: Boolean;
begin
 Result := CheckReadOnlyClientFunction;
 if not Result then
    Result := CheckReadOnlyCompanyFunction;
end;

function GetReadOnlyClintCompanyDataset: IevDataSet;
var
  s, sFilter : String;
  Q: IevQuery;
begin
  s:= 'select a.CL_NBR,a.CO_NBR,a.CREDIT_HOLD,b.READ_ONLY from TMP_CO a, TMP_CL b where a.CL_NBR = b.CL_NBR and ' +
         '( b.READ_ONLY = ''Y'' or b.READ_ONLY = ''R'' or a.CREDIT_HOLD = ''R'') order by a.CL_NBR, a.CO_NBR ';
  Q := TevQuery.Create(s);
  Q.Execute;
  Result := Q.Result;

  sFilter := 'READ_ONLY = ''Y'' ';
  if CheckReadOnlyClientFunction then
     sFilter := sFilter + ' or READ_ONLY = ''R''';
  if CheckReadOnlyCompanyFunction then
     sFilter := sFilter + ' or CREDIT_HOLD = ''R''';

  Result.Filter := sFilter;
  Result.Filtered := true;
end;

function GetReadOnlyClientCompanyList: IisStringList;
var
  Q : IevDataSet;
begin
  Result := TisStringList.CreateAsIndex;
  Q := GetReadOnlyClintCompanyDataset;
  if Q.Fields[0].AsInteger > 0 then
  begin
    Result.Clear;
    Result.Capacity := Q.RecordCount;
    Q.First;
    while not Q.Eof do
    begin
     if Q.FieldByName('CL_NBR').AsInteger > 0 then
        Result.Add(Q.FieldByName('CL_NBR').AsString+';'+Q.FieldByName('CO_NBR').AsString);
     Q.Next;
    end;
  end;
end;

function GetReadOnlyClintCompanyFilter(const withand: Boolean=true; const prefixCL : string = ''; const prefixCO : string = '';
                                       const WithList : Boolean=false; const WithSomeResult : boolean = false): String;
var
 Q : IevDataSet;
begin
  Result := '';
  Q := GetReadOnlyClintCompanyDataset;
  if Q.Fields[0].AsInteger > 0 then
  begin
     if WithList then
     begin
       Result := prefixCL + 'READ_ONLY in ( ''Y'' ';
       if CheckReadOnlyClientFunction then
          Result := Result + ',''R''';
       Result := Result + ')';
       if CheckReadOnlyCompanyFunction then
          Result := Result + ' or ' + prefixCO + 'CREDIT_HOLD = ''R''';
     end
     else
     begin
       Result := 'not ' + prefixCL + 'READ_ONLY in ( ''Y'' ';
       if CheckReadOnlyClientFunction then
          Result := Result + ',''R''';
       Result := Result + ')';
       if CheckReadOnlyCompanyFunction then
          Result := Result + ' and ' + prefixCO + 'CREDIT_HOLD <> ''R''';
     end;
     if Result <> '' then
        Result:= ' (' + result + ')';
  end;
  if WithSomeResult and (Result = '') then
     Result:= ' 1 = 1 ';
  if (result <> '') and withand then
     Result := ' and ' + Result;
end;


function GetReadOnlyClintCompanyListFilter(const withand: Boolean=true; const prefix : string = '';const WithNot : boolean=True): String;
var
 bNot : Boolean;
 Q : IevDataSet;
begin
  Result := '';
  Q := GetReadOnlyClintCompanyDataset;
  if Q.Fields[0].AsInteger > 0 then
  begin
    bNot := false;
    Q.First;
    while not Q.Eof do
    begin
     if (Q.FieldByName('CL_NBR').AsInteger > 0) and
        (Q.FieldByName('CO_NBR').AsInteger > 0) then
     begin
        if WithNot then
        begin
          if bNot then
             result := Result + ' and ';
          Result:= result + ' ( not ( ' + prefix + 'CL_NBR = ' + Q.FieldByName('CL_NBR').AsString +
                               ' and ' + prefix + 'CO_NBR = ' + Q.FieldByName('CO_NBR').AsString + '))';
        end
        else
        begin
          if bNot then
             result := Result + ' or ';
          Result:= result + ' ( ' + prefix + 'CL_NBR = ' + Q.FieldByName('CL_NBR').AsString +
                               ' and ' + prefix + 'CO_NBR = ' + Q.FieldByName('CO_NBR').AsString + ')';
        end;
        bNot := True;
     end;
     Q.Next;
    end;
    if (Result <> '') and  not WithNot then
       Result:= ' (' + result + ')';
    if (result <> '') and withand then
       result := ' and ' + Result;
  end;
end;

function GetOBCBankAccountInfo : String;
begin
  result :='';
  if (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
  begin
      result :=DM_SERVICE_BUREAU.SB.AR_EXPORT_FORMAT.AsString[1];
      if DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').IsNull then
        raise EInconsistentData.CreateHelp('The company uses OBC, but OBC service bureau bank account is not setup.', IDH_InconsistentData);
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
      if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').Value, []) then
      begin
        if (DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('OBC_ACCOUNT').AsString = 'Y') and
           (DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('BANK_CHECK').AsString <> 'Z')  then
          result := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('BANK_CHECK').AsString[1];
      end
      else
        raise EInconsistentData.CreateHelp('The company uses OBC, but OBC service bureau bank account is not setup.', IDH_InconsistentData);
  end;
end;

 function GetCoBenefitRateNumber(const SubTypeNbr: integer; const pNow : TDateTime; const  pEndDate : TDateTime): boolean;
var
  tmpEndDate : variant;
begin
   result := false;

   if not DM_COMPANY.CO_BENEFIT_RATES.Active then
     DM_COMPANY.CO_BENEFIT_RATES.DataRequired();

   tmpEndDate := pEndDate;
   if pEndDate < pNow then
      tmpEndDate := pNow;

   if DM_COMPANY.CO_BENEFIT_RATES.IndexDefs.IndexOf('CO_BENEFIT_SUBTYPE_NBR') = -1 then
     DM_COMPANY.CO_BENEFIT_RATES.IndexDefs.Add('CO_BENEFIT_SUBTYPE_NBR', 'CO_BENEFIT_SUBTYPE_NBR;PERIOD_BEGIN;PERIOD_END', []);

   if DM_COMPANY.CO_BENEFIT_RATES.IndexName <> 'CO_BENEFIT_SUBTYPE_NBR' then
      DM_COMPANY.CO_BENEFIT_RATES.IndexName := 'CO_BENEFIT_SUBTYPE_NBR';

   DM_COMPANY.CO_BENEFIT_RATES.FindNearest([SubTypeNbr]);
   while (DM_COMPANY.CO_BENEFIT_RATES['CO_BENEFIT_SUBTYPE_NBR'] = SubTypeNbr) and
         (not DM_COMPANY.CO_BENEFIT_RATES.Eof) do
   begin
     if (DM_COMPANY.CO_BENEFIT_RATES['PERIOD_BEGIN'] <= pNow) then
     begin
        if ((DM_COMPANY.CO_BENEFIT_RATES['PERIOD_BEGIN'] <= tmpEndDate) and
            (DM_COMPANY.CO_BENEFIT_RATES['PERIOD_END'] >= tmpEndDate)) then
        begin
           result := true;
           break;
        end;
     end;
     DM_COMPANY.CO_BENEFIT_RATES.Next;
   end;
end;

function GetCoBenefitRateNumberMaxDependents(const SubTypeNbr: integer; const pNow : TDateTime; const  pEndDate : TDateTime): integer;
begin
  if not GetCoBenefitRateNumber(SubTypeNbr, pNow, pEndDate) then
    Result := -1
  else begin
    Result := DM_COMPANY.CO_BENEFIT_RATES.FieldByName('MAX_DEPENDENTS').AsInteger;
  end;
end;

function GetEnableHrBenefit(const coNbr: integer): Boolean;
var
 Q :IEvQuery;
begin
  Result := False;
  Q:=TEvQuery.Create('SELECT ENABLE_HR FROM CO WHERE {AsOfNow<CO>} and CO_NBR = :CONBR ');
  Q.Params.AddValue('CONBR', coNbr);
  if (ctx_DataAccess.ClientID > 0) then
   if (Q.Result.FieldByName('ENABLE_HR').AsString <> GROUP_BOX_BENEFITS) then
      Result := true;
end;

function GetSBAnalyticsLicense: Boolean;
var
 Q :IEvQuery;
begin
  Result := False;
  Q:=TEvQuery.Create('SELECT SB_NBR FROM SB WHERE {AsOfNow<SB>} and ANALYTICS_LICENSE = ''N''');
  if (ctx_DataAccess.ClientID > 0) then
    Result := Q.Result.FieldByName('SB_NBR').AsInteger > 0;
end;

procedure AttachSUIToPayrolls(CONbr, COSUINbr, COStateNbr: Integer; BeginCheckDate, EndCheckDate: TDateTime);
var
  AllOK: Boolean;
begin
  ctx_StartWait;
  with ctx_DataAccess.CUSTOM_VIEW, DM_PAYROLL do
  try
    PR.DataRequired('CO_NBR=' + IntToStr(CONbr));
    PR_CHECK_SUI.DataRequired('ALL');
    PR.First;
    AllOK := False;
    while not PR.EOF do
    begin
      if (PR.FieldByName('CHECK_DATE').AsDateTime >= BeginCheckDate) and (PR.FieldByName('CHECK_DATE').AsDateTime <= EndCheckDate)
        and (PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
      try
        AllOK := False;
        ctx_DataAccess.UnlockPR;
        if Active then
          Close;
        with TExecDSWrapper.Create('GetAllChecksForSUIState') do
        begin
          SetParam('COStateNbr', COStateNbr);
          SetParam('PrNbr', PR.FieldByName('PR_NBR').AsInteger);
          SetParam('StatusDate', PR.FieldByName('PROCESS_DATE').AsDateTime);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        while not EOF do
        begin
          if not PR_CHECK_SUI.Locate('PR_CHECK_NBR;CO_SUI_NBR', VarArrayOf([FieldByName('PR_CHECK_NBR').Value, COSUINbr]), []) then
          begin
            PR_CHECK_SUI.Insert;
            PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').Value := FieldByName('PR_CHECK_NBR').Value;
            PR_CHECK_SUI.FieldByName('PR_NBR').Value := FieldByName('PR_NBR').Value;
            PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value := COSUINbr;
            PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value := 0;
            PR_CHECK_SUI.FieldByName('SUI_TAX').Value := 0;
            PR_CHECK_SUI.Post;
          end;
          Next;
        end;
        ctx_DataAccess.PostDataSets([PR_CHECK_SUI]);
        AllOK := True;
      finally
        ctx_DataAccess.LockPR(AllOK);
      end;
      PR.Next;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure SetUpCO_E_D_CODESDefault(const Code,Desc : string);
begin
  if length(Code) > 0 then
  begin
    if (Code = ED_MEMO_SIMPLE) or (Code = ED_MEMO_ER_INSURANCE_PREMIUM) then
    begin
      if pos('cobra', Desc) = 0 then
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
      else
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_EE;
    end
    else
    if Code = ED_MEMO_COBRA_CREDIT then
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
    else
    if Code = ED_MEMO_PERCENT_OF_TAX_WAGES then
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
    else
    if Code[1] = 'D' then
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_EE
    else
       DM_COMPANY.CO_E_D_CODES['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_NA;
  end;
end;

function CreatecdTCDLocalsDataset: IevDataSet;
var
  Q: IevQuery;
  s : String;
begin
   s :='select a.CO_LOCAL_TAX_NBR, b.CO_STATES_NBR, a.SY_STATES_NBR, a.SY_LOCALS_NBR, b.TCD_DEPOSIT_FREQUENCY_NBR, b.TCD_PAYMENT_METHOD ' +
         ' from CO_LOCAL_TAX a, CO_STATES b where {AsOfNow<a>} and {AsOfNow<b>} and ' +
            ' a.SY_STATES_NBR = b.SY_STATES_NBR and a.CO_NBR = b.CO_NBR and b.TCD_DEPOSIT_FREQUENCY_NBR is not null ';
   Q := TevQuery.Create(s);
   Q.Execute;
   Result := Q.Result;
end;

function GetCoLocalTaxDeduct(const deduct, localType: string; const newRecord : boolean = false):string;
begin
{ TODO: Added inPlymouth. 'A' and ' ' values should be converted to the actual values and this function should be deleted }
  result := deduct;
  if (result='')
  or (result='A') or (result=' ')
  or newRecord then
  begin
    if (localType = LOCAL_TYPE_INC_NON_RESIDENTIAL) then
       result := DEDUCT_NEVER
    else
       result := DEDUCT_ALWAYS;
  end;
end;

function SysTime: TDateTime;
begin
  Result := Mainboard.TimeSource.GetDateTime;
end;

function SysDate: TisDate;
begin
  Result := Trunc(DateOf(SysTime));
end;

function MakeArrayDS(const arr: array of TevClientDataSet): TArrayDS;
var
  i: Integer;
begin
  SetLength(Result, Length(arr));
  for i := Low(arr) to High(arr) do
    Result[i] := arr[i];
end;

procedure SaveDSStates(DS: array of TevClientDataSet; const SaveData: Boolean = False);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].SaveState(SaveData);
end;

procedure LoadDSStates(DS: array of TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].LoadState;
end;

function DSConditionsEqual( c1, c2: string ): boolean;
begin
  if Trim(UpperCase(c1)) = 'ALL' then
    c1 := '';
  if Trim(UpperCase(c2)) = 'ALL' then
    c2 := '';
  Result := UpperCase(StringReplace( c1, ' ', '', [rfReplaceAll])) =
            UpperCase(StringReplace( c2, ' ', '', [rfReplaceAll]));
end;

procedure AddDSWithCheck(const DataSets: array of TevClientDataSet; var aDS: TArrayDS; CheckCondition: string);
var
  i: Integer;
begin
  for i := Low(DataSets) to High(DataSets) do
    AddDSWithCheck(DataSets[i], aDS, CheckCondition);
end;

procedure AddDSWithCheck(const DataSet: TevClientDataSet; var aDS: TArrayDS; CheckCondition: string);
begin
  DataSet.CheckDSCondition(CheckCondition);
  AddDS(DataSet, aDS);
end;

procedure RemoveDS(const DataSet: TevClientDataSet; var aDS: TArrayDS);
var
  i, j: Integer;
begin
  for i := High(aDS) downto Low(aDS) do
    if aDS[i] = DataSet then
    begin
      for j := i+ 1 to High(aDS) do
        aDS[j-1] := aDS[j];
      SetLength(aDS, Length(aDS)-1);
    end;
end;

procedure AddDS(const DataSets: TArrayDS; var aDS: TArrayDS);
var
  i: Integer;
begin
  for i := Low(DataSets) to High(DataSets) do
    AddDS(DataSets[i], aDS);
end;

procedure AddDS(const DataSets: array of TevClientDataSet; var aDS: TArrayDS);
var
  i: Integer;
begin
  for i := Low(DataSets) to High(DataSets) do
    AddDS(DataSets[i], aDS);
end;


procedure AddDS(const DataSet: TevClientDataSet; var aDS: TArrayDS);

  function IsDSClassInParentsOfDSClass(const DSClass, DSChildClass: TClass): Boolean;
  var
    i: Integer;
    ClassSet: TddReferencesDesc;
  begin
    Result := False;
    ClassSet := TddTableClass(DSChildClass).GetParentsDesc;
    for i := 0 to High(ClassSet) do
      if DSClass = ClassSet[i].Table then
      begin
        Result := True;
        Break;
      end;
  end;

var
  i, j: Integer;
begin
  if not Assigned(DataSet) then
    Exit;

  for i := Low(aDS) to High(aDS) do
    if aDS[i] = DataSet then
      Exit;

  SetLength(aDS, Length(aDS) + 1);
  if DataSet is TddTable then
    for i := 0 to High(aDS) - 1 do
      if (aDS[i] is TddTable) and
         IsDSClassInParentsOfDSClass(DataSet.ClassType, aDS[i].ClassType) and
         not IsDSClassInParentsOfDSClass(aDS[i].ClassType, DataSet.ClassType) then
      begin
        for j := High(aDS) downto i + 1 do
          aDS[j] := aDS[j - 1];
        aDS[i] := DataSet;
        Exit;
      end;

  aDS[High(aDS)] := DataSet;
end;

function GetDBTypeByTable(const ATableName: String): TevDBType;
begin
  if StartsWith(ATableName, 'SY_') then
    Result := dbtSystem
  else if StartsWith(ATableName, 'SB') then
    Result := dbtBureau
  else if StartsWith(ATableName, 'TMP_') then
    Result := dbtTemporary
  else if StartsWith(ATableName, 'CL') or  StartsWith(ATableName, 'CO') or  StartsWith(ATableName, 'EE') or  StartsWith(ATableName, 'PR') then
    Result := dbtClient
  else
    Result := dbtUnknown;
end;

function GetDBNameByType(const ADBType: TevDBType): String;
const
  DBNames: array [Low(TevDBType)..High(TevDBType)] of String = ('', '', DB_System, DB_S_Bureau, DB_Cl, DB_TempTables);
begin
  Result := DBNames[ADBType];
end;

function GetDBNameByTable(const ATableName: String): String;
begin
  Result := GetDBNameByType(GetDBTypeByTable(ATableName));
end;

function GetDBTypeByDBName(const ADBName: String): TevDBType;
begin
  if AnsiSameText(ADBName, DB_System) then
    Result := dbtSystem
  else if AnsiSameText(ADBName, DB_S_Bureau) then
    Result := dbtBureau
  else if AnsiSameText(ADBName, DB_TempTables) then
    Result := dbtTemporary
  else if StartsWith(ADBName, DB_Cl) then
    Result := dbtClient
  else
    Result := dbtUnknown;
end;

function GetEDCheckLineCount(EDNbr: Integer): Integer;
begin
  result := 0;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'count(CL_E_DS_NBR) EdCount');
      SetMacro('TableName', 'PR_CHECK_LINES ');
      SetMacro('NbrField', 'CL_E_DS');
      SetParam('RecordNbr', EdNbr);
      DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
      result := ctx_DataAccess.CUSTOM_VIEW.FieldByName('EDCount').AsInteger;
  end;
end;

function DBRadioGroup_ED_GetAvailFromDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string; const AState: Integer = 0): string;
begin
  Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');
  if (EdType = ED_EWOD_SPC_TAXED_EARNINGS) or
     (EdType = ED_ST_SP_TAXED_EARNING) or (EdType = ED_ST_SP_TAXED_DEDUCTION) or
     (EdType = ED_ST_SP_TAXED_1099_EARNINGS) or
     (EdType = ED_ST_1099_R_SP_TAXED) then

    Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE + GROUP_BOX_EXEMPT

  else
  begin
    if (EdType = ED_ST_EXEMPT_EARNINGS) or (EdType = ED_ST_TIAA_CREFF) then
      Result := GROUP_BOX_EXEMPT

    else if EdType = '' then
      Result := ''

    else if EdType[1] = 'E' then
    begin
      if IsInSystem then
      begin
        if (DefaultValue = GROUP_BOX_EXEMPT) or (DefaultValue = GROUP_BOX_YES) then
          Result := GROUP_BOX_EXEMPT
        else
          Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE;
      end
      else
        Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE;
    end

    else if (EdType[1] = 'D') or (EdType[1] = 'M') then
    begin
      if IsInSystem then
        Result := DefaultValue
      else
        Result := GROUP_BOX_EXEMPT;
    end;
  end;
end;

function DBRadioGroup_ED_GetRightDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string): string;
begin
  if (EdType = ED_EWOD_SPC_TAXED_EARNINGS) or (EdType = ED_ST_SP_TAXED_EARNING) or
     (EdType = ED_ST_SP_TAXED_DEDUCTION) or (EdType = '') or (EdType = ED_ST_SP_TAXED_1099_EARNINGS) then

    Result := '-'

  else if IsInSystem then
  begin
    if DefaultValue = 'Y' then
      Result := GROUP_BOX_EXEMPT
    else if DefaultValue = 'N' then
      Result := GROUP_BOX_INCLUDE
    else
      Result := DefaultValue;
  end

  else if EdType[1] = 'E' then
    Result := GROUP_BOX_INCLUDE

  else
    Result := GROUP_BOX_EXEMPT;
end;

function GetSBPaperInfo:IevDataSet;
var
  s: string;
  q : IevQuery;
begin
  Result := nil;
  s:= ' SELECT B.SB_PAPER_INFO_NBR, B.WEIGHT, B.MEDIA_TYPE FROM SB_PAPER_INFO B  '+
      ' WHERE {AsOfNow<B>} ';
  q := TevQuery.Create(s);
  q.Execute;
  q.Result.First;
  Result := q.Result;
end;

function GetSBOption(aLoc:string):IevDataSet;
var
  s: string;
  q : IevQuery;
begin
  Result := nil;
  s:= ' SELECT B.OPTION_String_Value, B.OPTION_INTEGER_VALUE, B.OPTION_TAG FROM SB_OPTION B  '+
      ' WHERE {AsOfNow<B>} and B.OPTION_tag Like ''' + aLoc + '%''';
  q := TevQuery.Create(s);
  q.Execute;
  q.Result.First;
  Result := q.Result;
end;

function GetRemotePrinterList:IisStringList;
var
  Q: IevQuery;
  s: string;
begin
  Result := TisStringList.Create;

  if CheckRemoteVMRActive then
  begin
    //@NEWYORK,@LAX,@BURLINGTON
    s:= ' SELECT B.OPTION_String_Value FROM SB_OPTION B  '+
        ' WHERE {AsOfNow<B>} and B.OPTION_tag = ''' + VmrRemoteLocation + '''';
    Q := TevQuery.Create(s);
    Q.Execute;
    Q.Result.First;
    Result.SetCommaText(Uppercase(Q.Result.Fields[0].asString));
  end;
end;

function GetPrinterCount (aLoc:string):integer;
var
  Q: IevQuery;
  s: string;
begin
  //aLoc: @NEWYORK|_
  s:= ' select count(*) from sb_Option B '+
      ' WHERE {AsOfNow<B>} and ( (B.OPTION_tag like '''+aLOC+ '_'') or (B.OPTION_tag like ''' + aLoc + '__''))';
  Q := TevQuery.Create(s);
  Q.Execute;
  Result := Q.Result.Fields[0].asInteger;
end;

function GetTrayCount (aLoc:string):integer;
var
  Q: IevQuery;
  s: string;
begin
  //@NEWYORK0TRAY%
  s:= ' select count(*) from sb_Option B '+
      ' WHERE {AsOfNow<B>} and B.OPTION_tag like '''+aLoc+'''';
  Q := TevQuery.Create(s);
  Q.Execute;
  Result := Q.Result.Fields[0].asInteger;
end;

function TaskNbrToId(const ATaskNbr: String): TisGUID;
var
  TaskInf: IevTaskInfo;
  TaskList: IisListOfValues;
  i: Integer;
  sDomain, sUser : String;
begin
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  sDomain := Context.UserAccount.Domain;
  sUser := Context.UserAccount.User;

  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    if  not (AnsiSameText(sDomain, TaskInf.GetDomain) and AnsiSameText(sUser, TaskInf.GetUser)) then
      continue;
    if TaskInf.Nbr = StrToInt(ATaskNbr) then
    begin
      result := TaskInf.ID;
      exit;
    end;
  end;
end;

function IsRWAFormat(AData: IEvDualStream): Boolean;
var
  s: String;
  p: Integer;
  n: Byte;
const
  cRWADNA    = 'TPF0';
begin
  Result := False;
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cRWADNA));
    AData.Read(s[1], Length(cRWADNA));
    if AnsiSameStr(s, cRWADNA) then
    begin
      AData.Read(n, 1);
      s := StringOfChar(' ', n);
      AData.Read(s[1], n);
      Result := SameText(s, 'TrwRenderingPaper') or SameText(s, 'TrwEnginePaper') or SameText(s, 'TrwVirtualPage') or
              SameText(s, 'TrwPrintRepository');
    end;

  finally
    AData.Position := p;
  end;
end;


procedure updateSbENABLE_ANALYTICSforClient(const clNbr: integer);
begin
 if (clNbr > 0) then
 begin
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Close;
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.DataRequired('CL_NBR = ' + intToStr(clNbr));
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.First;
   while not DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Eof do
   begin
     DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Edit;
     DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.FieldValues['ENABLE_ANALYTICS']:= 'N';
     DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Post;
     DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Next;
   end;
   ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED]);
 end;
end;

procedure updateSbENABLE_ANALYTICS(const coNbr: integer; const enableANALYTICS: string);
begin
 if (coNbr > 0) then
 begin
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Close;
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.DataRequired('CL_NBR = ' + intToStr(DM_COMPANY.CO.ClientID ) + ' and CO_NBR = ' +  IntToStr(coNbr));

   if DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Locate('CL_NBR; CO_NBR', VarArrayOf([DM_COMPANY.CO.ClientID, coNbr]), []) then
      DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Edit
   else
   begin
      DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Insert;
      DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.FieldValues['CL_NBR'] := DM_COMPANY.CO.ClientID;
      DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.FieldValues['CO_NBR'] := coNbr;
   end;
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.FieldValues['ENABLE_ANALYTICS']:= enableANALYTICS;
   DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED.Post;
   ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_ANALYTICS_ENABLED]);
 end;
end;

function DashboardUserCount:integer;
var
  Q: IevQuery;
begin
  Q := TevQuery.Create('  select count(DISTINCT SB_USER_NBR)  from CO_DASHBOARDS_USER where {AsOfNow<CO_DASHBOARDS_USER>} and CO_NBR =:coNbr');
  Q.Params.AddValue('coNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  Q.Execute;
  result := Q.Result.Fields[0].AsInteger;
end;

function CheckPrimaryRateFlag(const withRateNbr: Boolean;const aAsOfDate: TDateTime):boolean;
var
  sFilter: string;
  Q: IevQuery;
begin
  result:= false;
  sFilter:= '';
  if withRateNbr then
     sFilter:= 'ee_rates_nbr <> :eeRatesNbr and ';

    Q := TevQuery.CreateFmt(
        '/*Cl_*/' +
        'EXECUTE BLOCK'#13 +
        'RETURNS (returnEERatesNumber INTEGER)'#13 +
        'AS'#13 +
        'DECLARE VARIABLE eeNbr INTEGER;'#13 +
        'DECLARE VARIABLE eeRatesNbr INTEGER;'#13 +
        'DECLARE VARIABLE asofdate DATE;'#13 +
        'BEGIN'#13 +
        '  eeNbr = %d;'#13 +
        '  eeRatesNbr = %d;'#13 +
        '  asofdate = ''%s'';'#13 +
        ''#13 +
        'SELECT Count(ee_nbr) FROM ee_rates a WHERE ee_nbr = :eeNbr AND ' + sFilter +
        '         PRIMARY_RATE = ''Y'' and '#13 +
        '         {GenericAsOfDate<:asofdate, a>} '#13 +
        '         group by ee_nbr '#13 +
        '         into :returnEERatesNumber;'#13 +
        ''#13 +
        '  SUSPEND;'#13 +
        'END',
          [DM_EMPLOYEE.EE_RATES.FieldByName('EE_NBR').AsInteger,
            DM_EMPLOYEE.EE_RATES.FieldByName('EE_RATES_NBR').AsInteger,
           DateToUTC(aAsOfDate)]);
  Q.Execute;
  if (Q.Result.RecordCount >0)  and ( Q.Result.Fields[0].Value <> null ) then
     result:= True;
end;

initialization
  FDebugMode := GetDebugMode;
  FQAMode := GetQAMode;

end.
