unit rwModalInputFormFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, rwInputFormContainerFrm, SReportSettings, EvContext;

type
  TrwModalInputForm = class(TForm)
    pnlButtons: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    InputFormContainer: TrwInputFormContainer;
    procedure InputFormContainerResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FParams: TrwReportParams;
  public
    procedure SetInitialSize(ARect: TRect);
    property  Params: TrwReportParams read FParams write FParams;
  end;

implementation

uses Types, EvUtils;

{$R *.dfm}

{ TrwModalInputForm }

procedure TrwModalInputForm.SetInitialSize(ARect: TRect);
begin
  ClientWidth := ARect.Right - ARect.Left;
  ClientHeight := ARect.Bottom - ARect.Top + pnlButtons.Height;
end;

procedure TrwModalInputForm.InputFormContainerResize(Sender: TObject);
begin
  InputFormContainer.FrameResize(Sender);
end;

procedure TrwModalInputForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  ctx_RWLocalEngine.CloseInputForm(ModalResult, Params);
end;

end.
