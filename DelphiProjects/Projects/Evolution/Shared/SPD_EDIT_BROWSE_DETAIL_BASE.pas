// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_BROWSE_DETAIL_BASE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, DB, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, ISBasicClasses, EvUIComponents;

type
  TEDIT_BROWSE_DETAIL_BASE = class(TFrameEntry)
    PC: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtDetails: TTabSheet;
    gBrowse: TevDBGrid;
    procedure gBrowseDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TEDIT_BROWSE_DETAIL_BASE.gBrowseDblClick(Sender: TObject);
begin
  inherited;
  if Assigned(gBrowse) and Assigned(gBrowse.DataSource) and Assigned(gBrowse.DataSource.DataSet)
  and gBrowse.DataSource.DataSet.Active then
    PC.ActivatePage(tshtDetails);
end;

end.
