unit EvAuditVerFieldViewFrm;

interface

uses Forms, DB, Variants, Types, SysUtils, DateUtils, Graphics,
     kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
     EvDataAccessComponents, Wwdatsrc, ISBasicClasses, EvUIComponents,
     Controls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, ComCtrls, Classes,
     ExtCtrls, evDataSet, isDataSet, EvCommonInterfaces, evContext, EvUtils,
     EvConsts, Buttons, isBasicUtils, EvStreamUtils, wwdblook,
     LMDCustomButton, LMDButton, isUILMDButton, EvVerFieldAsOfFrm, Mask,
     wwdbedit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox, isBaseClasses,
     dbcgrids, DBCtrls, rwCustomDataDictionary, SDDStreamClasses,
  isUIFashionPanel;

type
  TevAuditVerFieldView = class(TFrame)
    frmRightViewer: TevVerFieldAsOf;
    frmLeftViewer: TevVerFieldAsOf;
    dsTimeLine: TevDataSource;
    grTimeLine: TDBCtrlGrid;
    dbtDateTime: TevDBText;
    dbtUserInfo: TevDBText;
    pnlBottom: TevPanel;
    fpAuditTop: TisUIFashionPanel;
    sbAuditForm: TScrollBox;
    pnlLowered: TevPanel;
    evSplitter1: TevSplitter;
    procedure dsTimeLineDataChange(Sender: TObject; Field: TField);
    procedure FrameResize(Sender: TObject);
    procedure frmRightViewergrFieldDataRowChanged(Sender: TObject);
    procedure frmLeftViewergrFieldDataRowChanged(Sender: TObject);
    procedure pnlBottomResize(Sender: TObject);
    procedure grTimeLinePaintPanel(DBCtrlGrid: TDBCtrlGrid;
      Index: Integer);
  private
    FTable: String;
    FField: String;
    FNbr:   Integer;
    FShowTextValue: Boolean;
    FChanges: IisParamsCollection;
    FTimeLine: IevDataSet;
    procedure BuildChangeDateList;
    procedure CalcDifs(const ALeftDS, ARightDS: TDataSet;
                       out ALeftDif: IisListOfValues; out ARightDif: IisListOfValues);
  public
    procedure Init(const ATable, AField: String; const ANbr: Integer); reintroduce;
  end;

implementation

{$R *.DFM}

{ TevAuditVerFieldView }

procedure TevAuditVerFieldView.BuildChangeDateList;
const
    DSStruct: array [0..2] of TDSFieldDef = (
      (FieldName: 'change_time';  DataType: ftDateTime;  Size: 0;  Required: False),
      (FieldName: 'user_name';  DataType: ftString;  Size: 60;  Required: False),
      (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False));

var
  i: Integer;
begin
  FTimeLine := TevDataSet.Create(DSStruct);
  (FTimeLine.Fields[0] as TDateTimeField).DisplayFormat := 'mm/dd/yyyy hh:nn:ss AM/PM';

  if Assigned(FChanges) then
  begin
    for i := FChanges.Count - 2 downto 0 do
      FTimeLine.AppendRecord([FChanges[i].Value['ChangeTime'], FChanges[i].Value['UserName'],
                              FChanges[i].Value['UserId']]);
  end;

  FTimeLine.Last;
  dsTimeLine.DataSet := FTimeLine.vclDataSet;
end;

procedure TevAuditVerFieldView.Init(const ATable, AField: String; const ANbr: Integer);
var
  Res: IisListOfValues;
  tbl: TDataDictTable;
  fld: TDataDictField;
  fk: TDataDictForeignKey;
begin
  if (FTable <> ATable) or (FField <> AField) or (FNbr <> ANbr) then
  begin
    FTable := ATable;
    FField := AField;
    FNbr := ANbr;

    tbl := EvoDataDictionary.Tables.TableByName(ATable);
    fld := tbl.Fields.FieldByName(AField);
    fk := tbl.ForeignKeys.ForeignKeyByField(fld);
    FShowTextValue := Assigned(fk) or (fld.FieldValues.Count > 0);

    Res := ctx_DBAccess.GetVersionFieldAudit(FTable, FField, FNbr, EmptyDate);
    if Assigned(Res) and (Res.Count = 1) then
      FChanges := IInterface(Res[0].Value) as IisParamsCollection
    else
      FChanges := nil;

    BuildChangeDateList;
  end;
end;

procedure TevAuditVerFieldView.dsTimeLineDataChange(Sender: TObject; Field: TField);
var
  n: Integer;
  LeftDS, RightDS: IevDataSet;
  LeftDif, ARightDif: IisListOfValues;
begin
  n := FTimeLine.RecordCount - FTimeLine.RecNo + 1;
  
  if Assigned(FChanges) then
  begin
    if n - 1 >= 0 then
      RightDS := IInterface(FChanges[n - 1].Value['Data']) as IevDataSet;
    if n >= 0 then
      LeftDS := IInterface(FChanges[n].Value['Data']) as IevDataSet;

    CalcDifs(LeftDS.vclDataSet, RightDS.vclDataSet, LeftDif, ARightDif);
    LeftDS.First;
    RightDS.First;
  end
  else
  begin
    LeftDS := nil;
    RightDS := nil;
  end;

  frmLeftViewer.Init(LeftDS, LeftDif, FShowTextValue);
  frmRightViewer.Init(RightDS, ARightDif, FShowTextValue);

  if n = 1 then
    frmRightViewer.fpChange.Title := 'As of Now'
  else
    frmRightViewer.fpChange.Title := 'After Change';

end;

procedure TevAuditVerFieldView.FrameResize(Sender: TObject);
begin
  grTimeLine.ColCount := grTimeLine.ClientWidth div 160;

 { frmLeftViewer.Left := 0;
  frmLeftViewer.Top := lCurrentInfo.BoundsRect.Bottom;
  frmLeftViewer.Width := ClientWidth div 2 - 5;
  frmLeftViewer.Height := ClientHeight - lCurrentInfo.BoundsRect.Bottom;

  frmRightViewer.Width := ClientWidth div 2 - 5;
  frmRightViewer.Left := ClientWidth - frmRightViewer.Width;
  frmRightViewer.Top := lCurrentInfo.BoundsRect.Bottom;
  frmRightViewer.Height := ClientHeight - lCurrentInfo.BoundsRect.Bottom;   }
end;

procedure TevAuditVerFieldView.CalcDifs(const ALeftDS, ARightDS: TDataSet;
  out ALeftDif: IisListOfValues; out ARightDif: IisListOfValues);
var
  s: String;
begin
  ALeftDif := TisListOfValues.Create;
  ARightDif := TisListOfValues.Create;

  ARightDS.DisableControls;
  ALeftDS.DisableControls;
  try
    ARightDS.First;
    while not ARightDS.Eof do
    begin
      if not ALeftDS.Locate('begin_date', ARightDS['begin_date'], []) then
        ARightDif.AddValue(IntToStr(ARightDS.RecNo), 'N')
      else
      begin
        s := '';
        if ALeftDS['end_date'] <> ARightDS['end_date'] then
          s := 'end_date';
        if not VarSameValue(ALeftDS['value'], ARightDS['value']) then
          s := s + ' value text_value';

        if s <> '' then
          ARightDif.AddValue(IntToStr(ARightDS.RecNo), Trim(s));
      end;
      ARightDS.Next;
    end;

    ALeftDS.First;
    while not ALeftDS.Eof do
    begin
      if not ARightDS.Locate('begin_date', ALeftDS['begin_date'], []) then
        ALeftDif.AddValue(IntToStr(ALeftDS.RecNo), 'D')
      else
      begin
        s := '';
        if ALeftDS['end_date'] <> ARightDS['end_date'] then
          s := 'end_date';
        if not VarSameValue(ALeftDS['value'], ARightDS['value']) then
          s := s + ' value text_value';

        if s <> '' then
          ALeftDif.AddValue(IntToStr(ALeftDS.RecNo), Trim(s));
      end;
      ALeftDS.Next;
    end;

  finally
    ALeftDS.EnableControls;
    ARightDS.EnableControls;
  end;
end;

procedure TevAuditVerFieldView.frmRightViewergrFieldDataRowChanged(Sender: TObject);
begin
  if frmRightViewer.Tag = 0 then
  begin
    frmRightViewer.Tag := 1;
    try
      if Assigned(frmLeftViewer.dsFieldData.DataSet) and Assigned(frmRightViewer.dsFieldData.DataSet) then
        frmLeftViewer.dsFieldData.DataSet.Locate('begin_date', frmRightViewer.dsFieldData.DataSet['begin_date'], []);
    finally
      frmRightViewer.Tag := 0;
    end;
  end;
end;

procedure TevAuditVerFieldView.frmLeftViewergrFieldDataRowChanged(Sender: TObject);
begin
  if frmLeftViewer.Tag = 0 then
  begin
    frmLeftViewer.Tag := 1;
    try
      if Assigned(frmLeftViewer.dsFieldData.DataSet) and Assigned(frmRightViewer.dsFieldData.DataSet) then
        frmRightViewer.dsFieldData.DataSet.Locate('begin_date', frmLeftViewer.dsFieldData.DataSet['begin_date'], []);
    finally
      frmLeftViewer.Tag := 0;
    end;
  end;  
end;

procedure TevAuditVerFieldView.pnlBottomResize(Sender: TObject);
begin
  frmLeftViewer.Width := pnlBottom.Width div 2;
end;

procedure TevAuditVerFieldView.grTimeLinePaintPanel(
  DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  if DBCtrlGrid.PanelIndex = Index then
    begin
      dbtDateTime.Font.Color := clBlack;
      dbtUserInfo.Font.Color := clBlack;
    end
  else
    begin
      dbtDateTime.Font.Color := clGrayText;
      dbtUserInfo.Font.Color := clGrayText;
    end;
end;

end.
