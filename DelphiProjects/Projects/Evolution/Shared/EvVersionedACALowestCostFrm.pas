unit EvVersionedACALowestCostFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, isUIwwDBComboBox, SFieldCodeValues,
  wwdblook, isUIwwDBLookupCombo, SDataDictsystem, SDataDictclient;

type
  ACABenefitObject = class(TObject)
    CO_BENEFITS_NBR : String;
    BENEFIT_NAME    : String;
  end;

type
  TEvVersionedACALowestCost = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    pnlBottom: TevPanel;
    cbACALowestCost: TevDBLookupCombo;
    lblACALowestCost: TevLabel;
    evLabel3: TevLabel;
    cbAcaBenefit: TevComboBox;
    DM_CLIENT: TDM_CLIENT;
    cdSubType: TevClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure dsFieldDataDataChange(Sender: TObject; Field: TField);
    procedure dsFieldDataStateChange(Sender: TObject);
    procedure cbAcaBenefitChange(Sender: TObject);
  protected
    pACABenefitObject: ACABenefitObject;
    procedure SyncControlsState; override;
    procedure ClearACABEnefits;
    procedure ResetBenefitSubType(CO_BENEFITS_NBR: String);
    procedure LocateSubType;
    procedure LoadACABEnefits;
  end;

implementation

{$R *.dfm}


{ TEvVersionedEELocal }


procedure TEvVersionedACALowestCost.FormShow(Sender: TObject);

begin
  inherited;

  Fields[0].AddValue('Title', lblACALowestCost.Caption);
  Fields[0].AddValue('Width', 18);
  lblACALowestCost.Caption := Iff(Fields[0].Value['Required'], '~', '') + lblACALowestCost.Caption;
  cbACALowestCost.DataField := Fields.ParamName(0);

end;

procedure TEvVersionedACALowestCost.SyncControlsState;
begin
  inherited;

end;

procedure TEvVersionedACALowestCost.ClearACABenefits;
begin
  While cbAcaBenefit.Items.Count > 0 do
  begin
    ACABenefitObject(cbAcaBenefit.Items.Objects[0]).Free;
    cbAcaBenefit.Items.Delete(0);
  end;
end;

procedure TEvVersionedACALowestCost.ResetBenefitSubType(
  CO_BENEFITS_NBR: String);
var
  Q : IevQuery;
  s : String;

begin
  s :=  'Select CO_BENEFITS_NBR From CO_BENEFITS ' +
           ' Where {AsOfDate<CO_BENEFITS>} and CO_BENEFITS_NBR = ' + CO_BENEFITS_NBR;
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
    CO_BENEFITS_NBR := Q.Result.Fields[0].asString;

   if CO_BENEFITS_NBR <> '' then
    cdSubType.Filter := 'CO_BENEFITS_NBR = ' + CO_BENEFITS_NBR
  else
    cdSubType.Filter := 'CO_BENEFITS_NBR = -1';

  cdSubType.Filtered := true;

  dsFieldData.DataSet.Edit;
  dsFieldData.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').asString := '-1';

end;

procedure TEvVersionedACALowestCost.dsFieldDataDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if dsFieldData.DataSet.State in [dsEdit, dsInsert] then exit;

  LoadACABEnefits;  
  LocateSubType;
end;

procedure TEvVersionedACALowestCost.LocateSubType;
var
  Q : IevQuery;
  s : String;
  CO_BENEFITS_NBR: String;
  i : Integer;
Begin

  cdSubType.Filter := 'CO_BENEFITS_NBR = -1';
  cdSubType.Filtered := true;

  if dsFieldData.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').asString = '' then
    exit;
  s :=  'Select CO_BENEFITS_NBR From CO_BENEFIT_SUBTYPE Where ' +
        '{AsOfDate<CO_BENEFIT_SUBTYPE>}' +
       ' and CO_BENEFIT_SUBTYPE_NBR = ' + dsFieldData.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').asString;
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
    CO_BENEFITS_NBR := Q.Result.Fields[0].asString;

  for i := 0 to cbAcaBenefit.Items.Count - 1 do
  begin
    if ACABenefitObject(cbAcaBenefit.Items.Objects[i]).CO_BENEFITS_NBR = CO_BENEFITS_NBR then
    begin
      cbAcaBenefit.ItemIndex := i;
      break;
    end;
  end;
 if CO_BENEFITS_NBR <> '' then
    cdSubType.Filter := 'CO_BENEFITS_NBR = ' + CO_BENEFITS_NBR
  else
    cdSubType.Filter := 'CO_BENEFITS_NBR = -1';
 cdSubType.Filtered := True;

end;

procedure TEvVersionedACALowestCost.dsFieldDataStateChange(Sender: TObject);
begin
  inherited;
  grFieldData.Columns[2].DisplayWidth := 50;
end;

procedure TEvVersionedACALowestCost.LoadACABEnefits;
var
  Q : IevQuery;
  s : String;
  CoNbr: Integer;

begin
  ClearACABenefits;

  if AnsiSameText(Table, 'EE') then
    Q := TevQuery.CreateFmt('SELECT t.co_nbr FROM %s t WHERE %s_nbr = :nbr AND {AsOfNow<t>}', [Table, Table])
  else
    Assert(False);

  Q.Params.AddValue('nbr', Nbr);
  CoNbr := Q.Result.Fields[0].AsInteger;

  s :='select DISTINCT  a.CO_BENEFITS_NBR, a.BENEFIT_NAME ' +
                              ' from CO_BENEFITS a,   CO_BENEFIT_SUBTYPE b ' +
                              ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and ' +
                              ' a.CO_NBR = :coNbr and b.ACA_LOWEST_COST = ''Y'' ' +
                              ' order by a.BENEFIT_NAME';
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Params.AddValue('coNbr', CoNbr);
  Q.Execute;

  if Q.Result.RecordCount > 0 then
  begin
    while not Q.Result.Eof do
    begin
     pACABenefitObject := ACABenefitObject.Create;
     pACABenefitObject.CO_BENEFITS_NBR := Q.Result.Fields[0].asString;
     pACABenefitObject.BENEFIT_NAME    := Q.Result.Fields[1].asString;
     cbAcaBenefit.Items.AddObject(Q.Result.Fields[1].asString,pACABenefitObject);
     Q.Result.Next;
    end;
  end;

  cdSubType.Close;
  with TExecDSWrapper.Create('select a.CO_BENEFITS_NBR, b.CO_BENEFIT_SUBTYPE_NBR, b.DESCRIPTION ' +
                          ' from CO_BENEFITS a,   CO_BENEFIT_SUBTYPE b ' +
                          ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and  a.CO_NBR = :coNbr and ' +
                          ' b.ACA_LOWEST_COST = ''Y'' ' +
                          ' order by b.DESCRIPTION') do
  begin
    SetParam('coNbr', CoNbr);
    ctx_DataAccess.GetCustomData(cdSubType, AsVariant);
  end;
  cdSubType.Open;
  cdSubType.IndexFieldNames := 'CO_BENEFITS_NBR';

  cbACALowestCost.LookupTable:=cdSubType;

  try
    grFieldData.Columns[2].DisplayWidth := 50;
  except
  end;
end;

procedure TEvVersionedACALowestCost.cbAcaBenefitChange(
  Sender: TObject);
begin
  inherited;
  ResetBenefitSubType(ACABenefitObject(cbAcaBenefit.Items.Objects[cbAcaBenefit.ItemIndex]).CO_BENEFITS_NBR);
end;

end.
