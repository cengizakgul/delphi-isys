inherited CompanyChoiceQuery: TCompanyChoiceQuery
  Left = 536
  Top = 247
  Width = 653
  Height = 402
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 323
    Width = 637
    inherited btnYes: TevBitBtn
      Left = 399
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 525
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 269
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 637
    Height = 288
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'20'#9'Number'#9'F'
      'NAME'#9'40'#9'Name'#9'F'
      'CO_NBR'#9'10'#9'Co Nbr'#9'F')
    IniAttributes.SectionName = 'TCompanyChoiceQuery\dgChoiceList'
  end
  inherited pnlEffectiveDate: TevPanel
    Top = 288
    Width = 637
  end
  object DM_TEMPORARY: TDM_TEMPORARY [3]
    Left = 64
    Top = 40
  end
  inherited dsChoiceList: TevDataSource
    Left = 192
    Top = 40
  end
end
