// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPrintingRoutines;

interface

uses
  SysUtils, Classes,  Graphics, DB, Math, Windows, EvUtils, SDataStructure, EvContext,
  EvConsts, EvTypes,  Variants, EvBasicUtils, EvClientDataSet;

function GetNumberText(Number: Double): string;
function ConvertAmountForSecurityFont(Amount: Double): string;
function CreateAgencyCheckStubDetails(Limit: Integer; HideSSN: boolean = False): string;
function CreateTaxCheckStubDetails(var Header, EIN: string): string;
procedure ListAllShortfalls(PRNbr: Integer; ShortfallList: TevClientDataSet);
procedure SplitDataSetForCollation(aDataSet: TevClientDataSet);

implementation

uses kbmMemTable, StrUtils, EvDataAccessComponents;

function ConvertAmountForSecurityFont(Amount: Double): string;
var
  Cents: string;
begin
  Result := '.-' + IntToStr(Trunc(Amount));
  Cents := IntToStr(Round(Frac(Amount) * 100));
  if Length(Cents) = 1 then
    Cents := '0' + Cents;
  Result := Result + Chr(Ord(Cents[1]) + 17);
  Result := Result + Chr(Ord(Cents[2]) + 17);
end;

function GetNumberText(Number: Double): string;
{Procedure to convert check amount to text}
var
  ChkAmtNum, DecPos: Integer;
  ChkAmtDec, CheckTxt, ChkAmtTxt: string;
  A1: array[0..19] of string;
  A2: array[0..9] of string;
begin
  A1[0] := '';
  A1[1] := 'One';
  A1[2] := 'Two';
  A1[3] := 'Three';
  A1[4] := 'Four';
  A1[5] := 'Five';
  A1[6] := 'Six';
  A1[7] := 'Seven';
  A1[8] := 'Eight';
  A1[9] := 'Nine';
  A1[10] := 'Ten';
  A1[11] := 'Eleven';
  A1[12] := 'Twelve';
  A1[13] := 'Thirteen';
  A1[14] := 'Fourteen';
  A1[15] := 'Fifteen';
  A1[16] := 'Sixteen';
  A1[17] := 'Seventeen';
  A1[18] := 'Eighteen';
  A1[19] := 'Nineteen';

  A2[0] := '';
  A2[1] := '';
  A2[2] := 'Twenty';
  A2[3] := 'Thirty';
  A2[4] := 'Forty';
  A2[5] := 'Fifty';
  A2[6] := 'Sixty';
  A2[7] := 'Seventy';
  A2[8] := 'Eighty';
  A2[9] := 'Ninety';

  if Number < 0 then
  begin
    CheckTxt := 'Minus ';
    Number := Abs(Number);
  end;

  ChkAmtTxt := FloatToStrF(Number, ffFixed, 15, 2);
  ChkAmtNum := 0;
   {Get the position of the decimal point}
  DecPos := Pos('.', ChkAmtTxt);
  if DecPos <> 0 then
  begin
         {Break amount into number and decimal parts}
    ChkAmtDec := Copy(ChkAmtTxt, DecPos + 1, Length(ChkAmtTxt) - DecPos);
    if Length(ChkAmtDec) = 1 then ChkAmtDec := ChkAmtDec + '0';
    ChkAmtTxt := Copy(ChkAmtTxt, 1, DecPos - 1);
  end;
  if ChkAmtTxt = '0' then
    ChkAmtTxt := ''
  else
    ChkAmtNum := StrToInt(ChkAmtTxt);
   {Million Conversion}
  if (Length(ChkAmtTxt) = 7) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Million ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 6);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Hundred Thousand Conversion}
  if (Length(ChkAmtTxt) = 6) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Hundred ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 5);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
    if Length(ChkAmtTxt) < 4 then CheckTxt := CheckTxt + 'Thousand ';
  end;
   {Ten Thousand Conversion}
  if (Length(ChkAmtTxt) = 5) and (ChkAmtNum <> 0) then
  begin
    ChkAmtNum := StrToInt(Copy(ChkAmtTxt, 1, 2));
    if ChkAmtNum < 20 then
      CheckTxt := CheckTxt + A1[ChkAmtNum] + ' Thousand '
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtTxt[1])];
      if ChkAmtTxt[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtTxt[2])] + 'Thousand '
      else
        CheckTxt := CheckTxt + ' Thousand ';
    end;
    ChkAmtTxt := Copy(ChkAmtTxt, 3, 3);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Thousand Conversion}
  if (Length(ChkAmtTxt) = 4) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Thousand ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 3);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Hundred Conversion}
  if (Length(ChkAmtTxt) = 3) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Hundred ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 2);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Ten conversion}
  if (Length(ChkAmtTxt) = 2) and (ChkAmtNum <> 0) then
  begin
    if ChkAmtNum < 20 then
      CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt)] + ' Dollars and '
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtTxt[1])];
      if ChkAmtTxt[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtTxt[2])] + ' Dollars and '
      else
        CheckTxt := CheckTxt + ' Dollars and ';
    end;
  end;
   {One conversion}
  if Length(ChkAmtTxt) = 1 then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])];
    if ChkAmtNum = 1 then
      CheckTxt := CheckTxt + ' Dollar and '
    else
      if ChkAmtNum = 0 then
      CheckTxt := CheckTxt + ' Dollars and '
    else
      CheckTxt := CheckTxt + ' Dollars and ';
  end;
  if Length(ChkAmtTxt) = 0 then CheckTxt := CheckTxt + 'No Dollars and ';
   { Add Decimal place text string to Amount Line}
  if StrToInt(ChkAmtDec) <> 0 then
  begin
    if StrToInt(ChkAmtDec) < 20 then
      CheckTxt := CheckTxt + A1[StrToInt(ChkAmtDec)]
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtDec[1])];
      if ChkAmtDec[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtDec[2])]
    end;
    if StrToInt(ChkAmtDec) = 1 then
      CheckTxt := CheckTxt + ' Cent'
    else
      CheckTxt := CheckTxt + ' Cents';
  end
  else
    CheckTxt := CheckTxt + 'No Cents';
  Result := CheckTxt;
end;

function CreateAgencyCheckStubDetails(Limit: Integer; HideSSN: boolean = False): string;
var
  LastEmployee: string;
  LineCount, EmployeeCount: Integer;
  ChildSupportCases, GarnishmentIDs: TevClientDataSet;
  TempStr: String;
begin
  LineCount := 0;
  EmployeeCount := 0;
  LastEmployee := '';
  Result := '';
  ChildSupportCases := TevClientDataSet.Create(Nil);
  GarnishmentIDs := TevClientDataSet.Create(Nil);
  with ctx_DataAccess.CUSTOM_VIEW do
  try
    if Active then
      Close;
    with TExecDSWrapper.Create('ChildSupportCases') do
    begin
      SetParam('CheckNbr', DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value);
      DataRequest(AsVariant);
    end;
    Open;
    ChildSupportCases.Data := Data;

    Close;
    with TExecDSWrapper.Create('GarnishmentIDs') do
    begin
      SetParam('CheckNbr', DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value);
      DataRequest(AsVariant);
    end;
    Open;
    GarnishmentIDs.Data := Data;

    Close;
    with TExecDSWrapper.Create('AgencyCheckDetails') do
    begin
      SetParam('CheckNbr', DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not EOF do
    begin
      Result := Result + PadStringLeft(FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ' ', 9) + ' '
        + PadStringRight(FieldByName('LAST_NAME').AsString + ', ' + FieldByName('FIRST_NAME').AsString + ' '
        + FieldByName('MIDDLE_INITIAL').AsString, ' ', 30) + ' ';
      if HideSSN then
        Result := Result + '           '
      else
        Result := Result + PadStringRight(FieldByName('SOCIAL_SECURITY_NUMBER').AsString, ' ', 11);
      if ChildSupportCases.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([FieldByName('EE_NBR').Value, FieldByName('CL_E_DS_NBR').Value]), [])
      and not ChildSupportCases.FieldByName('CUSTOM_CASE_NUMBER').IsNull then
        TempStr := 'C #' + ChildSupportCases.FieldByName('CUSTOM_CASE_NUMBER').AsString
        + ' ' + ChildSupportCases.FieldByName('FIPS_CODE').AsString + ' '
      else
        TempStr := '';
      if GarnishmentIDs.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([FieldByName('EE_NBR').Value, FieldByName('CL_E_DS_NBR').Value]), []) then
        TempStr := TempStr + 'ID:' + GarnishmentIDs.FieldByName('GARNISNMENT_ID').AsString + ' ';
      TempStr := TempStr + FieldByName('DESCRIPTION').AsString;
      Result := Result + ' ' + Copy(PadStringRight(TempStr, ' ', 38), 1, 38);
      Result := Result + ' ' + PadStringLeft(FloatToStrF(FieldByName('AMOUNT').Value, ffFixed, 11, 2), ' ', 11) + #13 + #10;

      if LastEmployee <> FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString then
      begin
        Inc(EmployeeCount);
        LastEmployee := FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString;
      end;
      Inc(LineCount);
      Next;
    end;
    Close;
    if LineCount > Limit then
      Result := '----- This check reflects payments on behalf of ' + IntToStr(EmployeeCount) + ' employees. -----';
  finally
    ChildSupportCases.Free;
    GarnishmentIDs.Free;
  end;
end;

function CreateTaxCheckStubDetails(var Header, EIN: string): string;
var
  TaxDepositNbr, EECount: Integer;
  TotalTaxWages, TotalTax, TotalTaxCredit: Double;
  TempDataSet: TevClientDataSet;
  Check940, Check941, Check945, CheckState, CheckSUI, CheckLocal: Boolean;
  CheckDate, DueDate: TDateTime;
  isWaLI: Boolean;
  iPos: Integer;

  procedure GetTotalsForFederalTaxes(TaxDepositNbr: Integer; TaxType: Char;
    var EECount: Integer; var TotalTaxWages, TotalTax: Double);
  var
    TaxName, SumExpression: string;
    Date1, Date2, Date3: TDateTime;
  begin
    case TaxType of
      TAX_LIABILITY_TYPE_FEDERAL, TAX_LIABILITY_TYPE_FEDERAL_945, TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING:
        TaxName := 'FEDERAL';
      TAX_LIABILITY_TYPE_EE_OASDI:
        TaxName := 'EE_OASDI';
      TAX_LIABILITY_TYPE_EE_MEDICARE:
        TaxName := 'EE_MEDICARE';
      TAX_LIABILITY_TYPE_EE_EIC:
        TaxName := 'EE_EIC';
      TAX_LIABILITY_TYPE_ER_OASDI:
        TaxName := 'ER_OASDI';
      TAX_LIABILITY_TYPE_ER_MEDICARE:
        TaxName := 'ER_MEDICARE';
      TAX_LIABILITY_TYPE_ER_FUI:
        TaxName := 'ER_FUI';
    end;

    EECount := 0;
    TotalTaxWages := 0;
    TotalTax := 0;
    with DM_COMPANY.CO_FED_TAX_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        if FieldByName('TAX_TYPE').AsString = TaxType then
          TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        Next;
      end;
    end;

    if TotalTax <> 0 then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
      SumExpression := TaxName + '_TAXABLE_WAGES';
      if TaxType <> TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING then
        TaxName := TaxName + '_TAX'
      else
        TaxName := 'OR_CHECK_BACK_UP_WITHHOLDING';

      if TaxType = TAX_LIABILITY_TYPE_EE_EIC then
        SumExpression := 'FEDERAL_TAXABLE_WAGES';
      if TaxType = TAX_LIABILITY_TYPE_EE_OASDI then
        SumExpression := SumExpression + '+EE_OASDI_TAXABLE_TIPS';
      if TaxType = TAX_LIABILITY_TYPE_ER_OASDI then
        SumExpression := SumExpression + '+ER_OASDI_TAXABLE_TIPS';

      with TExecDSWrapper.Create('FedTaxesTotals') do
      begin
        SetMacro('SumExpression', SumExpression);
        SetMacro('TaxName', TaxName);
        if TaxType = TAX_LIABILITY_TYPE_FEDERAL then
          SetMacro('FilterCondition', 'C.CHECK_TYPE_945<>''Y'' and');
        if TaxType = TAX_LIABILITY_TYPE_FEDERAL_945 then
          SetMacro('FilterCondition', 'C.CHECK_TYPE_945=''Y'' and');
        SetParam('CONbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        if TaxType = TAX_LIABILITY_TYPE_ER_FUI then
        begin
          SetParam('BeginDate', GetBeginQuarter(CheckDate));
          SetParam('EndDate', GetEndQuarter(CheckDate));
        end
        else
        begin
          ctx_PayrollCalculation.GetFedTaxDepositPeriod(DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1],
            CheckDate, Date1, Date2, Date3);
          SetParam('BeginDate', Date1);
          SetParam('EndDate', Date2);
        end;
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
      TotalTaxWages := ConvertNull(FieldByName('TAXABLE_WAGES').Value, 0);
    end;
  end;

  procedure GetTotalsForCobraTax(TaxDepositNbr: Integer;
    var EECount: Integer; var TotalTax: Double);
  var
    sQuery: string;
    Date1, Date2, Date3: TDateTime;
  begin
    EECount := 0;
    TotalTax := 0;
    with DM_COMPANY.CO_FED_TAX_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        if FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_COBRA_CREDIT then
          TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        Next;
      end;
    end;

    if TotalTax <> 0 then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;

      sQuery := 'select count(distinct C.EE_NBR) EE_COUNT ' +
                'from PR_CHECK_LINES PCL '+
                '  join PR_CHECK C on PCL.PR_CHECK_NBR = C.PR_CHECK_NBR '+
                '  join PR P on C.PR_NBR=P.PR_NBR '+
                '  join CL_E_DS ED on PCL.CL_E_DS_NBR=ED.CL_E_DS_NBR '+
                '  join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and '+

                '(CO.CL_CO_CONSOLIDATION_NBR=(select CO.CL_CO_CONSOLIDATION_NBR from CO CO '+
                'join CL_CO_CONSOLIDATION N on N.CL_CO_CONSOLIDATION_NBR=CO.CL_CO_CONSOLIDATION_NBR '+
                'where CO.CO_NBR=:CONbr and {AsOfNow<CO>} and {AsOfNow<N>} '+
                ') or CO.CO_NBR=:CONbr) ' +

                'where P.CHECK_DATE between :BeginDate and :EndDate '+
                'and {AsOfNow<ED>} and {AsOfNow<C>} and  {AsOfNow<P>} and {AsOfNow<PCL>} and '+
                '(ED.E_D_Code_Type = '''+ED_MEMO_COBRA_CREDIT+''' or (ED.E_D_Code_Type = '''+ED_MEMO_SIMPLE+''' and Upper(ED.Description) like ''%COBRA%'')) ';

      with TExecDSWrapper.Create(sQuery) do
      begin
        SetParam('CONbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        ctx_PayrollCalculation.GetFedTaxDepositPeriod(DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1],
            CheckDate, Date1, Date2, Date3);
        SetParam('BeginDate', Date1);
        SetParam('EndDate', Date2);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
    end;
  end;

  procedure GetTotalsForStateTaxes(TaxDepositNbr, COStateNbr: Integer; TaxType: Char;
    var EECount: Integer; var TotalTaxWages, TotalTax, TotalTaxCredit: Double);
  var
    TaxName: string;
    Date1, Date2, Date3: TDateTime;
  begin
    case TaxType of
      TAX_LIABILITY_TYPE_STATE:
        TaxName := 'STATE';
      TAX_LIABILITY_TYPE_EE_SDI:
        TaxName := 'EE_SDI';
      TAX_LIABILITY_TYPE_ER_SDI:
        TaxName := 'ER_SDI';
    end;

    EECount := 0;
    TotalTaxWages := 0;
    TotalTax := 0;
    TotalTaxCredit := 0;
    with DM_COMPANY.CO_STATE_TAX_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        if (FieldByName('TAX_TYPE').AsString = TaxType) and (FieldByName('CO_STATES_NBR').AsInteger = COStateNbr) then
        begin
          if FieldByName('ADJUSTMENT_TYPE').AsString = TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT then
            TotalTaxCredit := TotalTaxCredit + FieldByName('AMOUNT').Value
          else
            TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        end;
        Next;
      end;
    end;

    if TotalTax <> 0 then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
      with TExecDSWrapper.Create('StateTaxesTotals') do
      begin
        SetMacro('TaxName', TaxName);
        SetParam('TaxType', TaxType);
        SetParam('CoStateNbr', COStateNbr);
        SetParam('CONbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        ctx_PayrollCalculation.GetStateTaxDepositPeriod(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStateNbr, 'STATE'),
          DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CoStateNbr, 'SY_STATE_DEPOSIT_FREQ_NBR'), CheckDate, Date1, Date2, Date3);
        SetParam('BeginDate', Date1);
        SetParam('EndDate', Date2);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
      TotalTaxWages := ConvertNull(FieldByName('TAXABLE_WAGES').Value, 0);
    end;
  end;

  procedure GetTotalsForSUITaxes(TaxDepositNbr, COSUINbr: Integer;
    var EECount: Integer; var TotalTaxWages, TotalTax: Double);
  begin
    EECount := 0;
    TotalTaxWages := 0;
    TotalTax := 0;
    with DM_COMPANY.CO_SUI_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        if FieldByName('CO_SUI_NBR').AsInteger = COSUINbr then
          TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        Next;
      end;
    end;

    if TotalTax <> 0 then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
      with TExecDSWrapper.Create('SUIDistinctTaxesTotals') do
      begin
        SetParam('CoLevelNbr', COSUINbr);
        SetParam('CONbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        SetParam('BeginDate', GetBeginQuarter(CheckDate));
        SetParam('EndDate', GetEndQuarter(CheckDate));
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
      TotalTaxWages := ConvertNull(FieldByName('TAXABLE_WAGES').Value, 0);
    end;
  end;

{  procedure GetTotalsForLocalTaxes(TaxDepositNbr, COLocalNbr: Integer;
    var EECount: Integer; var TotalTaxWages, TotalTax: Double);
  var
    Date1, Date2, Date3: TDateTime;
  begin
    EECount := 0;
    TotalTaxWages := 0;
    TotalTax := 0;
    with DM_COMPANY.CO_LOCAL_TAX_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        Next;
      end;
    end;

    if TotalTax <> 0 then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
      with TExecDSWrapper.Create('LocalTaxesTotals') do
      begin
        SetParam('CoLocalNbr', COLocalNbr);
        SetParam('CONbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        ctx_PayrollCalculation.GetLocalTaxDepositPeriod(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalNbr, 'SY_LOCALS_NBR'),
          DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalNbr, 'SY_LOCAL_DEPOSIT_FREQ_NBR'), CheckDate, Date1, Date2, Date3);
        SetParam('BeginDate', Date1);
        SetParam('EndDate', Date2);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
      TotalTaxWages := ConvertNull(FieldByName('TAXABLE_WAGES').Value, 0);
      TotalTax := ConvertNull(FieldByName('TAX').Value, 0);
    end;
  end;
}

  procedure GetTotalsForLocalTaxes(TaxDepositNbr, COLocalNbr: Integer;
    var EECount: Integer; var TotalTaxWages, TotalTax: Double);
  begin
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      EECount := 0;
      TotalTaxWages := 0;
      TotalTax := 0;
      if Active then Close;

      with TExecDSWrapper.Create('LocalTaxesTotals') do
      begin
        SetParam('CoLocalNbr', COLocalNbr);
        SetParam('TaxDepositNbr', TaxDepositNbr);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      EECount := ConvertNull(FieldByName('EE_COUNT').Value, 0);
      TotalTaxWages := ConvertNull(FieldByName('TAXABLE_WAGES').Value, 0);
      TotalTax := ConvertNull(FieldByName('TAX').Value, 0);
    end;
  {
    with DM_COMPANY.CO_LOCAL_TAX_LIABILITIES do
    begin
      First;
      while not EOF do
      begin
        if (FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger = TaxDepositNbr) and FieldByName('PR_NBR').IsNull then
          TotalTax := TotalTax + FieldByName('AMOUNT').Value;
        Next;
      end;
    end;}
  end;

begin
  DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.DataRequired('ALL');
  Header := '';
  Result := '';
  Check940 := False;
  Check941 := False;
  Check945 := False;
  CheckState := False;
  CheckSUI := False;
  CheckLocal := False;
  TaxDepositNbr := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
  EIN := DM_COMPANY.CO.FEIN.AsString;

  DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr));

  DueDate := 1;
  CheckDate := 1;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', ' Distinct DUE_DATE, CHECK_DATE');
      SetMacro('TableName', 'CO_FED_TAX_LIABILITIES');
      SetMacro('NbrField', 'CO_TAX_DEPOSITS');
      SetParam('RecordNbr', TaxDepositNbr);
      SetMacro('Condition', 'AMOUNT>0');
      DataRequest(AsVariant);
    end;
    Open;
    First;
    if not (BOF and EOF) then
    begin
      DueDate := FieldByName('DUE_DATE').Value;
      CheckDate := FieldByName('CHECK_DATE').Value;
    end
    else
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
      begin
        SetMacro('Columns', ' Distinct DUE_DATE, CHECK_DATE');
        SetMacro('TableName', 'CO_STATE_TAX_LIABILITIES');
        SetMacro('NbrField', 'CO_TAX_DEPOSITS');
        SetParam('RecordNbr', TaxDepositNbr);
        SetMacro('Condition', 'AMOUNT>0');
        DataRequest(AsVariant);
      end;
      Open;
      First;
      if not (BOF and EOF) then
      begin
        DueDate := FieldByName('DUE_DATE').Value;
        CheckDate := FieldByName('CHECK_DATE').Value;
      end
      else
      begin
        if Active then Close;
        with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
        begin
          SetMacro('Columns', ' Distinct DUE_DATE, CHECK_DATE');
          SetMacro('TableName', 'CO_SUI_LIABILITIES');
          SetMacro('NbrField', 'CO_TAX_DEPOSITS');
          SetParam('RecordNbr', TaxDepositNbr);
          SetMacro('Condition', 'AMOUNT>0');
          DataRequest(AsVariant);
        end;
        Open;
        First;
        if not (BOF and EOF) then
        begin
          DueDate := FieldByName('DUE_DATE').Value;
          CheckDate := FieldByName('CHECK_DATE').Value;
        end
        else
        begin
          if Active then Close;
          with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
          begin
            SetMacro('Columns', ' Distinct DUE_DATE, CHECK_DATE');
            SetMacro('TableName', 'CO_LOCAL_TAX_LIABILITIES');
            SetMacro('NbrField', 'CO_TAX_DEPOSITS');
            SetParam('RecordNbr', TaxDepositNbr);
            SetMacro('Condition', 'AMOUNT>0');
            DataRequest(AsVariant);
          end;
          Open;
          First;
          if not (BOF and EOF) then
          begin
            DueDate := FieldByName('DUE_DATE').Value;
            CheckDate := FieldByName('CHECK_DATE').Value;
          end;
        end;
      end;
    end;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_FEDERAL, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('Federal', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_EE_OASDI, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('EE OASDI', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_EE_MEDICARE, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('EE Medicare', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_EE_EIC, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('EE EIC', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_ER_OASDI, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('ER OASDI', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_ER_MEDICARE, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('ER Medicare', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check941 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_ER_FUI, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('ER FUI', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check940 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_FEDERAL_945, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('Federal', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check945 := True;
  end;

  GetTotalsForFederalTaxes(TaxDepositNbr, TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING, EECount, TotalTaxWages, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('Backup Withholding', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check945 := True;
  end;

  GetTotalsForCobraTax(TaxDepositNbr, EECount, TotalTax);
  if TotalTax <> 0 then
  begin
    Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight('Cobra Credit', ' ', 40)
      + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
      + ' ' + PadStringLeft(' ', ' ', 10) + ' '
      + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
    Check945 := True;
  end;

  TempDataSet := TevClientDataSet.Create(nil);
  try
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Columns', ' Distinct t1.CO_STATES_NBR, STATE');
        SetMacro('Table1', 'CO_STATES');
        SetMacro('Table2', 'CO_STATE_TAX_LIABILITIES');
        SetMacro('JoinField', 'CO_STATES');
        SetMacro('NbrField', 'CO_TAX_DEPOSITS');
        SetParam('RecordNbr', TaxDepositNbr);
        DataRequest(AsVariant);
      end;
      Open;
      TempDataSet.Data := Data;
    end;
    TempDataSet.First;
    while not TempDataSet.EOF do
    begin
      DM_COMPANY.CO_STATES.DataRequired('ALL');
      DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr));

      GetTotalsForStateTaxes(TaxDepositNbr, TempDataSet.FieldByName('CO_STATES_NBR').AsInteger,
        TAX_LIABILITY_TYPE_STATE, EECount, TotalTaxWages, TotalTax, TotalTaxCredit);
      if TotalTax <> 0 then
      begin
        EIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', TempDataSet.FieldByName('CO_STATES_NBR').Value, 'STATE_EIN'), '');
        if TotalTaxCredit = 0 then
          Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight(TempDataSet.FieldByName('STATE').AsString
            + ' State', ' ', 40) + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
            + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
            + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10
        else
          Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight(TempDataSet.FieldByName('STATE').AsString
            + ' State (' + FloatToStrF(TotalTax, ffFixed, 15, 2) + ' tax; ' + FloatToStrF(TotalTaxCredit, ffFixed, 15, 2) + ' cr)', ' ', 40)
            + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
            + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
            + PadStringLeft(FloatToStrF(TotalTax + TotalTaxCredit, ffFixed, 15, 2), ' ', 9) + #13 + #10;
        CheckState := True;
      end;

      GetTotalsForStateTaxes(TaxDepositNbr, TempDataSet.FieldByName('CO_STATES_NBR').AsInteger,
        TAX_LIABILITY_TYPE_EE_SDI, EECount, TotalTaxWages, TotalTax, TotalTaxCredit);
      if TotalTax <> 0 then
      begin
        EIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', TempDataSet.FieldByName('CO_STATES_NBR').Value, 'STATE_SDI_EIN'), '');
        Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight(TempDataSet.FieldByName('STATE').AsString
          + ' EE SDI', ' ', 40) + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
          + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
          + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
        CheckState := True;
      end;

      GetTotalsForStateTaxes(TaxDepositNbr, TempDataSet.FieldByName('CO_STATES_NBR').AsInteger,
        TAX_LIABILITY_TYPE_ER_SDI, EECount, TotalTaxWages, TotalTax, TotalTaxCredit);
      if TotalTax <> 0 then
      begin
        EIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', TempDataSet.FieldByName('CO_STATES_NBR').Value, 'STATE_SDI_EIN'), '');
        Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + PadStringRight(TempDataSet.FieldByName('STATE').AsString
          + ' ER SDI', ' ', 40) + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
          + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
          + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
        CheckState := True;
      end;

      TempDataSet.Next;
    end;

    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Columns', ' Distinct t1.CO_SUI_NBR, SY_SUI_NBR');
        SetMacro('Table1', 'CO_SUI');
        SetMacro('Table2', 'CO_SUI_LIABILITIES');
        SetMacro('JoinField', 'CO_SUI');
        SetMacro('NbrField', 'CO_TAX_DEPOSITS');
        SetParam('RecordNbr', TaxDepositNbr);
        DataRequest(AsVariant);
      end;
      Open;
      TempDataSet.Data := Data;
    end;
    isWaLI := TempDataSet.RecordCount > 0;
    TempDataSet.First;
    while not TempDataSet.EOF do
    begin
      DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
      DM_COMPANY.CO_SUI_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr));

      GetTotalsForSUITaxes(TaxDepositNbr, TempDataSet.FieldByName('CO_SUI_NBR').AsInteger, EECount, TotalTaxWages, TotalTax);
      if TotalTax <> 0 then
      begin
        DM_COMPANY.CO_STATES.DataRequired('ALL');

        // Payments made for "NV-Business Tax" and "NV-Business Tax-Financial" should pull the EIN from CO_SUI.TAX_RETURN_CODE
        if (TempDataSet.FieldByName('SY_SUI_NBR').AsInteger = 118) or (TempDataSet.FieldByName('SY_SUI_NBR').AsInteger = 223) then
          EIN := ConvertNull(DM_COMPANY.CO_SUI.Lookup('SY_SUI_NBR;CO_NBR', VarArrayOf([
            TempDataSet.FieldByName('SY_SUI_NBR').Value,
            DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'TAX_RETURN_CODE'), '')
        else
          EIN := ConvertNull(DM_COMPANY.CO_STATES.Lookup('SY_STATES_NBR;CO_NBR', VarArrayOf([
            DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', TempDataSet.FieldByName('SY_SUI_NBR').Value, 'SY_STATES_NBR'),
            DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'SUI_EIN'), '');

        Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + Copy(PadStringRight(VarToStr(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR'
          , TempDataSet.FieldByName('SY_SUI_NBR').Value, 'SUI_TAX_NAME')), ' ', 40), 1, 40)
          + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
          + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
          + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
        isWaLI := isWaLI and (TempDataSet.FieldByName('SY_SUI_NBR').AsInteger in [121,122]);
        CheckSUI := True;
      end;
      TempDataSet.Next;
    end;

    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Columns', ' Distinct t1.CO_LOCAL_TAX_NBR, SY_LOCALS_NBR, LOCAL_EIN_NUMBER');
        SetMacro('Table1', 'CO_LOCAL_TAX');
        SetMacro('Table2', 'CO_LOCAL_TAX_LIABILITIES');
        SetMacro('JoinField', 'CO_LOCAL_TAX');
        SetMacro('NbrField', 'CO_TAX_DEPOSITS');
        SetParam('RecordNbr', TaxDepositNbr);
        DataRequest(AsVariant);
      end;
      Open;
      TempDataSet.Data := Data;
    end;
    TempDataSet.First;
    while not TempDataSet.EOF do
    begin
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr));

      GetTotalsForLocalTaxes(TaxDepositNbr, TempDataSet.FieldByName('CO_LOCAL_TAX_NBR').AsInteger, EECount, TotalTaxWages, TotalTax);
      if TotalTax <> 0 then
      begin
        EIN := TempDataSet.FieldByName('LOCAL_EIN_NUMBER').AsString;
        Result := Result + PadStringRight(EIN, ' ', 15) + ' ' + Copy(PadStringRight(VarToStr(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR'
          , TempDataSet.FieldByName('SY_LOCALS_NBR').Value, 'NAME')), ' ', 40), 1, 40)
          + ' ' + PadStringLeft(IntToStr(EECount) + ' Employees', ' ', 15)
          + ' ' + PadStringLeft(FloatToStrF(TotalTaxWages, ffFixed, 15, 2), ' ', 10) + ' '
          + PadStringLeft(FloatToStrF(TotalTax, ffFixed, 15, 2), ' ', 9) + #13 + #10;
        CheckLocal := True;
      end;
      TempDataSet.Next;
    end;
  finally
    TempDataSet.Free;
  end;

  if Check940 then
    Header := Header + '940, ';
  if Check941 then
  begin
    if DM_COMPANY.CO.FieldByName('BUSINESS_TYPE').AsString = BUSINESS_TYPE_AGRICULTURE then
      Header := Header + '943, '
    else
      Header := Header + '941, ';
  end;
  if Check945 then
    Header := Header + '945, ';
  if CheckState then
    Header := Header + 'State, ';
  if CheckSUI then
    Header := Header + 'SUI, ';
  if CheckLocal then
    Header := Header + 'Local, ';
  if Header <> '' then
    Delete(Header, Length(Header) - 1, 2)
  else
    Exit;

  if (Header = 'SUI') and isWaLI then
  begin
    EIN := DM_COMPANY.CO.WORKERS_COMP_POLICY_ID.AsString;
    iPos := 1;
    repeat
      Result := StuffString(Result, iPos, 15, PadStringRight(EIN, ' ', 15));
      iPos := PosEx(#13#10, Result, iPos);
      if iPos <> 0 then
      begin
        iPos := iPos + 2;
        if iPos > Length(Result) then
          iPos := 0;
      end;
    until iPos = 0;
  end;

  Header := Header + '          ';
  Header := Header + 'Due Date: ' + FormatDateTime('mm/dd/yyyy', DueDate) + '          ';
  case GetQuarterNumber(CheckDate) of
    1: Header := Header + '1st Qtr. ';
    2: Header := Header + '2nd Qtr. ';
    3: Header := Header + '3rd Qtr. ';
    4: Header := Header + '4th Qtr. ';
  end;
  Header := Header + IntToStr(GetYear(CheckDate));
end;

procedure ListAllShortfalls(PRNbr: Integer; ShortfallList: TevClientDataSet);
var
  TempDS: TevClientDataSet;
  EENbr, ShrtfNbr: Integer;
  TaxRate, Shortfall: Double;
  CheckDate: TDateTime;
  CO_Nbr: string;

  procedure PopulateIt(EENbr: Integer; Description: string; Amount: Double);
  var
    PersonNbr: Integer;
  begin
    with DM_EMPLOYEE, DM_CLIENT do
    begin
      ShortfallList.Insert;
      ShortfallList.FieldByName('EE_NUMBER').Value := EE.Lookup('EE_NBR', EENbr, 'CUSTOM_EMPLOYEE_NUMBER');
      PersonNbr := EE.Lookup('EE_NBR', EENbr, 'CL_PERSON_NBR');
      ShortfallList.FieldByName('EE_NAME').Value := CL_PERSON.Lookup('CL_PERSON_NBR', PersonNbr, 'LAST_NAME') + ', '
        + CL_PERSON.Lookup('CL_PERSON_NBR', PersonNbr, 'FIRST_NAME') + ' ' + ConvertNull(CL_PERSON.Lookup('CL_PERSON_NBR', PersonNbr,
        'MIDDLE_INITIAL'), '');
      Inc(ShrtfNbr);
      ShortfallList.FieldByName('SHORTFALL_NUMBER').Value := ShrtfNbr;
      ShortfallList.FieldByName('SHORTFALL_DESCRIPTION').Value := Description;
      ShortfallList.FieldByName('SHORTFALL_TO_DATE').Value := Amount;
      ShortfallList.Post;
    end;
  end;

  procedure PopulateItForED(EENbr, EDNbr: Integer; Amount: Double);
  begin
    with DM_CLIENT do
    begin
      PopulateIt(EENbr, CL_E_DS.Lookup('CL_E_DS_NBR', EDNbr, 'CUSTOM_E_D_CODE_NUMBER') + ' '
        + CL_E_DS.Lookup('CL_E_DS_NBR', EDNbr, 'DESCRIPTION'), Amount);
    end;
  end;

begin
  ShrtfNbr := 0;

  if ShortfallList.Active then
    ShortfallList.Close;

  with ShortfallList.FieldDefs do
  begin
    Add('EE_NUMBER', ftString, 9, True);
    Add('EE_NAME', ftString, 50, True);
    Add('SHORTFALL_NUMBER', ftInteger, 0, True);
    Add('SHORTFALL_DESCRIPTION', ftString, 40, True);
    Add('SHORTFALL_TO_DATE', ftCurrency, 0, True);
  end;
  ShortfallList.CreateDataSet;

  with DM_EMPLOYEE, DM_PAYROLL, DM_CLIENT, DM_SYSTEM_LOCAL, DM_SYSTEM_FEDERAL do
  begin
// Open all necessary tables
    if PR.Active then
    begin
      if not PR.Locate('PR_NBR', PRNbr, []) then
        PR.DataRequired('PR_NBR=' + IntToStr(PRNbr));
    end
    else
      PR.DataRequired('PR_NBR=' + IntToStr(PRNbr));
    CO_Nbr := PR.FieldByName('CO_NBR').AsString;

    EE.DataRequired('CO_NBR=' + CO_Nbr);
    EE_SCHEDULED_E_DS.DataRequired('ALL');
    CL_E_DS.DataRequired('ALL');
    CL_PERSON.DataRequired('ALL');
    SY_LOCALS.DataRequired('ALL');
    SY_FED_TAX_TABLE.DataRequired('ALL');
// Necessary tables opened

    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('MaxCheckDatesForEE') do
    begin
      SetParam('PrNbr', PrNbr);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;

    TempDS := TevClientDataSet.Create(nil);
    try
      TempDS.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

// List all missing ALWAYS_PAY secheduled EDs
      EE_SCHEDULED_E_DS.First;
      while not EE_SCHEDULED_E_DS.EOF do
      begin
        if (EE_SCHEDULED_E_DS.FieldByName('ALWAYS_PAY').AsString = 'Y') and (EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString =
          CALC_METHOD_FIXED) then
        begin
          EENbr := EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger;
          if (EE.Lookup('EE_NBR', EENbr, 'CO_NBR') = CO_Nbr) and (not VarIsNull(TempDS.Lookup('EE_NBR', EENbr, 'MAX_CHECK_DATE'))) then
          begin
            CheckDate := TempDS.Lookup('EE_NBR', EENbr, 'MAX_CHECK_DATE');
            ctx_DataAccess.CUSTOM_VIEW.Close;
            with TExecDSWrapper.Create('DistinctCheckDatePr') do
            begin
              SetParam('Freq', EE.Lookup('EE_NBR', EENbr, 'PAY_FREQUENCY'));
              SetParam('StartDate', CheckDate);
              SetParam('EndDate', PR.FieldByName('CHECK_DATE').Value);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            ctx_DataAccess.CUSTOM_VIEW.First;
            if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_COUNT').Value, 0) > 0 then
            begin
              PopulateItForED(EENbr, EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger,
                ConvertNull(EE_SCHEDULED_E_DS.FieldByName('AMOUNT').Value, 0) * ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_COUNT').Value);
            end;
          end;
        end;
        EE_SCHEDULED_E_DS.Next;
      end;

// List all EDs that have shortfalls
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListEDWithShortfalls') do
      begin
        SetParam('PrNbr', PrNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SHORTFALL').Value, 0) <> 0 then
        begin
          PopulateItForED(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, ctx_DataAccess.CUSTOM_VIEW.FieldByName('CL_E_DS_NBR').Value,
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('SHORTFALL').Value);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

// List all federal taxes that have shortfalls
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListFedTaxesWithShortfalls') do
      begin
        SetParam('PrNbr', PrNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SHORTFALL').Value, 0) <> 0 then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, 'Federal Tax',
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('SHORTFALL').Value);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

// List all OASDI taxes that have shortfalls
      CheckDate := PR.FieldByName('CHECK_DATE').Value;
      TaxRate := SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value;
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListOtherWithShortfalls') do
      begin
        SetMacro('Columns', 'C.EE_NBR, sum(C.EE_OASDI_TAXABLE_WAGES+C.EE_OASDI_TAXABLE_TIPS) TAX_WAGE, ' +
          'sum(C.EE_OASDI_TAX) TAX');
        SetParam('StartDate', GetBeginYear(CheckDate));
        SetParam('EndDate', CheckDate);
        SetParam('CoNbr', CO_Nbr);
        SetMacro('Group', 'C.EE_NBR');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        Shortfall := RoundTwo(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('TAX_WAGE').Value, 0) * TaxRate
          - ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('TAX').Value, 0));
        if Abs(Shortfall) > TaxShortfallPickupLimit then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, 'OASDI Tax', Shortfall);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

// List all Medicare taxes that have shortfalls
      TaxRate := SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value;
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListOtherWithShortfalls') do
      begin
        SetMacro('Columns', 'C.EE_NBR, sum(C.EE_MEDICARE_TAXABLE_WAGES) TAX_WAGE, '
          + 'sum(C.EE_MEDICARE_TAX) TAX');
        SetParam('StartDate', GetBeginYear(CheckDate));
        SetParam('EndDate', CheckDate);
        SetParam('CoNbr', CO_Nbr);
        SetMacro('Group', 'C.EE_NBR');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        Shortfall := RoundTwo(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('TAX_WAGE').Value, 0) * TaxRate
          - ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('TAX').Value, 0));
        if Abs(Shortfall) > TaxShortfallPickupLimit then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, 'Medicare Tax', Shortfall);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

// List all state taxes that have shortfalls
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListAllStateWithShortfalls') do
      begin
        SetParam('PrNbr', PrNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('STATE_SHORTFALL').Value, 0) <> 0 then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, ctx_DataAccess.CUSTOM_VIEW.FieldByName('STATE').AsString + ' State Tax',
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('STATE_SHORTFALL').Value);
        end;
        if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('SDI_SHORTFALL').Value, 0) <> 0 then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, ctx_DataAccess.CUSTOM_VIEW.FieldByName('STATE').AsString + ' SDI Tax',
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('SDI_SHORTFALL').Value);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

// List all local taxes that have shortfalls
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListAllLocalWithShortfalls') do
      begin
        SetParam('PrNbr', PrNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('LOCAL_SHORTFALL').Value, 0) <> 0 then
        begin
          PopulateIt(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').Value, SY_LOCALS.Lookup('SY_LOCALS_NBR',
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('SY_LOCALS_NBR').AsString, 'NAME') + ' Tax',
            ctx_DataAccess.CUSTOM_VIEW.FieldByName('LOCAL_SHORTFALL').Value);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    finally
      TempDS.Free;
    end;
  end;

  ShortfallList.IndexFieldNames := 'EE_NUMBER;SHORTFALL_NUMBER';
end;

procedure SplitDataSetForCollation(aDataSet: TevClientDataSet);
var
  TempDataSet: TevClientDataSet;
  HalfCount, I: Integer;

  procedure CopyRecord;
  var
    I: Integer;
  begin
    aDataSet.Append;
    for I := 0 to TempDataSet.FieldCount - 1 do
      aDataSet.Fields[I].Value := TempDataSet.Fields[I].Value;
    aDataSet.Post;
  end;

begin
  TempDataSet := TevClientDataSet.Create(nil);
  try
    TempDataSet.Data := aDataSet.Data;
    TempDataSet.IndexFieldNames := aDataSet.IndexFieldNames;
    aDataSet.IndexFieldNames := '';

    HalfCount := TempDataSet.RecordCount div 2;
    if TempDataSet.RecordCount mod 2 = 1 then
      Inc(HalfCount);

    aDataSet.First;
    while not aDataSet.EOF do
      aDataSet.Delete;

    for I := 1 to HalfCount do
    begin
      TempDataSet.RecNo := I;
      CopyRecord;

      if HalfCount + I <= TempDataSet.RecordCount then
      begin
        TempDataSet.RecNo := HalfCount + I;
        CopyRecord;
      end;
    end;
  finally
    TempDataSet.Free;
  end;
end;

end.
