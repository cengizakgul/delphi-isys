inherited EvVersionedEnableHR: TEvVersionedEnableHR
  Left = 634
  Top = 310
  ClientHeight = 367
  ClientWidth = 784
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 784
    Height = 367
    inherited grFieldData: TevDBGrid
      Width = 736
      Height = 240
    end
    inherited pnlEdit: TevPanel
      Top = 284
      Width = 736
      object lblHR: TevLabel [2]
        Left = 246
        Top = 11
        Width = 16
        Height = 13
        Caption = 'HR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      inherited pnlButtons: TevPanel
        Left = 427
      end
      object evcbHR: TevDBComboBox
        Left = 246
        Top = 26
        Width = 172
        Height = 21
        HelpContext = 10568
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'ENABLE_HR'
        DataSource = dsFieldData
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'No'#9'N'
          'Remote HR'#9'R'
          'Web HR'#9'Y'
          'Benefits'#9'B'
          'Advanced HR'#9'A'
          'AHR Employee Portal Only'#9'O')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
        OnChange = UpdateFieldOnChange
      end
    end
  end
end
