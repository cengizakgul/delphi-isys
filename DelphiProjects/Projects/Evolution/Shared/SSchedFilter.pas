// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSchedFilter;

interface

uses
   Db, Wwdatsrc, StdCtrls, Controls, ComCtrls, Classes, Forms, EvUIComponents;

type
  TSchedFilterScreen = class(TFrame)
    Label2: TevLabel;
    DateTimePicker1: TevDateTimePicker;
    DateTimePicker2: TevDateTimePicker;
    Label3: TevLabel;
    Label4: TevLabel;
    CheckBox1: TevCheckBox;
    wwdsServiceBureau: TevDataSource;
    ComboBox2: TevComboBox;
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TSchedFilterScreen.CheckBox1Click(Sender: TObject);
begin
  DateTimePicker1.Enabled := (CheckBox1.Checked);
  DateTimePicker2.Enabled := (CheckBox1.Checked);
end;

end.
