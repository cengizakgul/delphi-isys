object rwModalPreviewForm: TrwModalPreviewForm
  Left = 423
  Top = 199
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'RW Preview'
  ClientHeight = 305
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inline rwPreviewContainer: TrwPreviewContainer
    Left = 0
    Top = 0
    Width = 522
    Height = 305
    Align = alClient
    AutoScroll = False
    TabOrder = 0
    OnResize = rwPreviewContainerResize
  end
end
