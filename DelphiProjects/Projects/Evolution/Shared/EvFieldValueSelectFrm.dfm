object EvFieldValueSelect: TEvFieldValueSelect
  Left = 682
  Top = 262
  Width = 623
  Height = 453
  Caption = 'Field Value'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlValues: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 607
    Height = 415
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Caption = 'pnlValues'
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Please select a Field Value'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 12
    Margins.Bottom = 45
    DesignSize = (
      607
      415)
    object grValues: TevDBGrid
      Left = 24
      Top = 48
      Width = 551
      Height = 302
      DisableThemesInTitle = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEvFieldValueSelect\grValues'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsrcValues
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = 14544093
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
    object btnOk: TevBitBtn
      Left = 421
      Top = 378
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
      Color = clBlack
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
        CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
        E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
        7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        E1E1E144A27700905001A16901AB7601AC7901AC7901AB7601A16900905055A8
        82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939D9D9D9E
        9E9E9E9E9E9D9D9D9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
        55A88200915202AC7700C38C00D79B00DA9C00DA9C00D79C01C38C01AB760092
        5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C6C6C6C9
        C8C8C9C8C8C7C7C7B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
        0090510FB48300D29800D59800D19200CF9000D09100D39600D69B00D19801AB
        76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C4C4C4C0C0C0BE
        BEBEBFBFBFC3C2C2C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
        16AB7810C99600D39700CD8CFFFFFFFFFFFFFFFFFF00CC8C00D19500D59B01C1
        8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC3C2C2BCBCBBFFFFFFFF
        FFFFFFFFFFBBBBBBC0C0C0C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
        39C49D00D19800CB8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CA8C00CF9600D2
        9B01AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BABABAFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFB9B9B9BFBFBFC3C2C29D9D9D7D7C7CFFFFFFFFFFFF008946
        52D2B000CC92FFFFFFFFFFFFFFFFFF00C484FFFFFFFFFFFFFFFFFF00C88D00D0
        9A00AD79008B4AFFFFFFFFFFFF7A7979C7C7C7BCBCBBFFFFFFFFFFFFFFFFFFB3
        B3B3FFFFFFFFFFFFFFFFFFB8B7B7C0C0C09F9F9E7C7B7BFFFFFFFFFFFF008845
        68DDBE00C991FFFFFFFFFFFF00C68C00C89100C58BFFFFFFFFFFFFFFFFFF00CC
        9600AD78008B4AFFFFFFFFFFFF797979D3D2D2B9B9B9FFFFFFFFFFFFB6B6B5B8
        B8B8B5B5B5FFFFFFFFFFFFFFFFFFBDBCBC9F9F9E7C7B7BFFFFFFFFFFFF008846
        76E0C600CB9800C59000C69100C89500C99700C89400C38CFFFFFFFFFFFF00C7
        9200AB75008C4BFFFFFFFFFFFF797979D7D6D6BCBCBBB6B6B5B7B6B6B9B9B9BA
        BABAB9B9B9B4B4B4FFFFFFFFFFFFB8B7B79D9D9D7D7C7CFFFFFFFFFFFF41A675
        59C9A449DEBC00C79400C89700C99800C99900C99800C79400C38EFFFFFF00BD
        8A00A06740A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B9B9B9BABABABB
        BBBBBABABAB8B8B8B4B4B4FFFFFFAFAFAF9292929B9B9BFFFFFFFFFFFFCCE8DB
        0A9458ADF8E918D0A700C49500C69700C69800C79800C79800C69700C59612B5
        85008F50CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B8B7B7B8
        B8B8B9B9B9B9B9B9B8B7B7B7B6B6A7A7A7818080E6E6E6FFFFFFFFFFFFFFFFFF
        55B185199C63BCFFF75EE4C900C59A00C39600C49700C59A22CAA22FC1960293
        556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB8B7B7B6
        B6B5B7B6B6B8B7B7BEBDBDB4B4B4858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF6ABB940E965974D5B6A0F4E194EFDC7CE6CC5ED6B52EB58703915255B3
        88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBEDEDEDE8
        E7E7DDDDDDCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
        78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      Margin = 0
      Kind = bkOK
    end
    object btnCancel: TevBitBtn
      Left = 511
      Top = 378
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
      Color = clBlack
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        EDEDED9499C82C3CC02B3BBE2B3ABE2B3ABE2B3ABE2B3BBE2C3CC09499C8EDED
        EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDA5A5A56C6C6C6B6B6B6B6B6B6B
        6B6B6B6B6B6B6B6B6C6C6CA5A5A5EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFEDEDED
        969BC92F3EC35F71F9697DFF697CFF697CFF697CFF697DFF5F71F92F3EC3969B
        C9EDEDEDFFFFFFFFFFFFFFFFFFEDEDEDA7A7A76E6E6E9E9E9EA6A6A6A6A6A6A6
        A6A6A6A6A6A6A6A69E9E9E6E6E6EA7A7A7EDEDEDFFFFFFFFFFFFEDEDED969BC9
        2F3EC2586BF65F74FF5D72FE5E72FD5E73FD5E72FD5D72FE5F74FF586BF62F3E
        C2969BC9EDEDEDFFFFFFEDEDEDA7A7A76E6E6E999999A1A1A1A09F9FA09F9FA0
        9F9FA09F9FA09F9FA1A1A19999996E6E6EA7A7A7EDEDEDFFFFFF9499C8303FC2
        5568F3586CFC4E64F94D63F85468F9576BF95468F94D63F84E64F9586CFC5568
        F3303FC29499C8FFFFFFA5A5A56E6E6E9796969C9C9C9797979796969999999A
        9A9A9999999796969797979C9C9C9796966E6E6EA5A5A5FFFFFF2D3DC05367F2
        556BFA4960F7FFFFFFFFFFFF3E56F6475EF63E56F6FFFFFFFFFFFF4960F7556B
        FA5166F22D3DC0FFFFFF6D6D6D9695959B9B9B959594FFFFFFFFFFFF90909093
        9393909090FFFFFFFFFFFF9595949B9B9B9595946D6D6DFFFFFF2B3BBF6276FC
        4D64F64259F4FFFFFFFFFFFFFFFFFF2C46F3FFFFFFFFFFFFFFFFFF4259F44E64
        F65F75FC2C3BBFFFFFFF6B6B6BA1A1A1969595909090FFFFFFFFFFFFFFFFFF88
        8888FFFFFFFFFFFFFFFFFF909090969595A1A0A06B6B6BFFFFFF2A3ABF7386FA
        495FF3435AF36E80F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E80F6435AF3495F
        F36E81FA2B3ABFFFFFFF6B6B6BAAA9A9929292909090A4A4A4FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFA4A4A4909090929292A7A7A76B6B6BFFFFFF2939BF8696FB
        425AF14259F1354EF05B70F2FFFFFFFFFFFFFFFFFF5B70F2354EF04259F1435B
        F17D90F92A39BFFFFFFF6B6B6BB3B3B38F8F8F8F8F8F8989899A9A9AFFFFFFFF
        FFFFFFFFFF9A9A9A8989898F8F8F909090AFAFAF6B6B6BFFFFFF2737BF9AA8FB
        3A55EF3953EE2844EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2844ED3953EE3B55
        EF8E9DFA2838BFFFFFFF6A6A6ABFBFBF8C8C8C8A8A8A858484FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF8584848A8A8A8C8C8CB8B7B76A6A6AFFFFFF2637BF9FABF1
        314CED2B47EBFFFFFFFFFFFFFFFFFF5369EFFFFFFFFFFFFFFFFFFF2C47EB314C
        ED9FABF12737BFFFFFFF6A6A6ABEBDBD878787858484FFFFFFFFFFFFFFFFFF96
        9595FFFFFFFFFFFFFFFFFF858484878787BEBDBD6A6A6AFFFFFF2838C19FABF1
        8091F4213EE8FFFFFFFFFFFF5D72EE2340E85D72EEFFFFFFFFFFFF213EE88091
        F49FABF12838C1FFFFFF6B6B6BBEBDBDAEAEAE818080FFFFFFFFFFFF99999981
        8080999999FFFFFFFFFFFF818080AEAEAEBEBDBD6B6B6BFFFFFFB4BAEA2E3EC3
        97A5EF778AF25B71EE6074EE2643E62C48E72643E66074EE5B71EE778AF297A5
        EF2E3EC3B4BAEAFFFFFFC6C6C66E6E6EB9B9B9A9A8A89999999A9A9A81808083
        83838180809A9A9A999999A9A8A8B9B9B96E6E6EC6C6C6FFFFFFFFFFFFB6BCEA
        2E3EC295A2EE7688F01E3BE42340E52541E52340E51E3BE47688F095A2EE2E3E
        C2B6BCEAFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB7B6B6A7A7A77E7E7D80807F80
        807F80807F7E7E7DA7A7A7B7B6B66E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFF
        B6BCEA2F3DC394A0EFADB9F8ADB8F7ADB9F7ADB8F7ADB9F894A0EF2F3DC3B6BC
        EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB6B6B5CAC9C9C9C8C8CA
        C9C9C9C8C8CAC9C9B6B6B56E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFB4BAEA303FC44555CE4454CD4354CD4454CD4555CE303FC4B4BAEAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C66F6F6F7E7E7D7D7C7C7D
        7C7C7D7C7C7E7E7D6F6F6FC6C6C6FFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      Margin = 0
      Kind = bkCancel
    end
  end
  object dsrcValues: TevDataSource
    Left = 176
    Top = 112
  end
end
