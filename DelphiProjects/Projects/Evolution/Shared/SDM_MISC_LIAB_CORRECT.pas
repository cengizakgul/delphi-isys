// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_MISC_LIAB_CORRECT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Variants, EvBasicUtils, kbmMemTable, ISKbmMemDataSet, EvDataset,
  ISBasicClasses, ISDataAccessComponents, EvDataAccessComponents, EvContext,
  ISBasicUtils, DateUtils, Types, EvCommonInterfaces,
  EvUIComponents, EvClientDataSet;

type
  TTaxType = (ttFUI, ttState, ttSUI, ttLocal);
  TDM_MISC_LIAB_CORRECT = class(TDataModule)
    cdTaxes: TevClientDataSet;
    cdTaxesTaxType: TStringField;
    cdTaxesNbr: TIntegerField;
    cdTaxesTaxDesc: TStringField;
    cdTaxesTotalCalc: TCurrencyField;
    cdTaxesTotalReal: TCurrencyField;
    cdTaxesTotalDiff: TCurrencyField;
    cdTaxesDo: TBooleanField;
    cdTaxesNbr2: TIntegerField;
    cdTaxesDueDate: TDateTimeField;
    cdTaxesAdjType: TStringField;
    cdTaxesState: TStringField;
    procedure cdTaxesAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
    CO, CO_STATES, CO_LOCALS, CO_SUI, SY_SUI, SY_FED_TAX_TABLE: TEvClientDataSet;
    cdWages, cdLiabs: TEvClientDataset;
    FCoNbr: Integer;
    FClNbr: Integer;
    FEndDate: TDateTime;
    FHasTaxService: Boolean;
    FLoadFUI: Boolean;
    FLoadState: Boolean;
    FLoadLocal: Boolean;
    FLoadSUI: Boolean;
    procedure Load;
    procedure AddLine(const TaxType: TTaxType; const TaxNbr: Variant; const TaxDesc: string; const TotalCalc, TotalReal: Currency; const DueDate: TDateTime; const AdjType: String; const State: string);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; const ClNbr, CoNbr: Integer; const TaxService: Boolean; const EndDate: TDateTime; const LoadFUI, LoadState, LoadSUI, LoadLocal: Boolean); reintroduce;
    function CalcVTHealthcare: Currency;
    procedure ApplyChanges;
    property ClNbr: Integer read FClNbr;
    property CONbr: Integer read FCoNbr;
    property HasTaxService: Boolean read FHasTaxService;
    property EndDate: TDateTime read FEndDate;
    property LoadFUI: Boolean read FLoadFUI;
    property LoadState: Boolean read FLoadState;
    property LoadSUI: Boolean read FLoadSUI;
    property LoadLocal: Boolean read FLoadLocal;
  end;

implementation

uses SDataStructure, EvUtils, EvConsts;

{$R *.DFM}

function RoundDown(Arg: Double): Double;
begin
  Result := Int(Arg);
end;

{ TDM_MISC_LIAB_CORRECT }

function TDM_MISC_LIAB_CORRECT.CalcVTHealthcare: Currency;
const
  VT_HEALTHCARE_SY_SUI_NBR = 130;
var
  CONSOLIDATED_COMPANIES: IevQuery;
  TotalHoursWorked: Double;
  FTE_WEEKLY_HOURS, FTE_MULTIPLIER, FTE_EXEMPTION, GLOBAL_RATE: Real;
  E: IevQuery;
//  sl: TStringList;

  function CalcHours(CoNbrs: String): Real;
  begin
    Result := 0;

    E := TevQuery.Create(
      '/*Cl_*/ '+
      '  select CUSTOM_EMPLOYEE_NUMBER, CL_PERSON_NBR, case when HOURS > '+FloatToStr(FTE_WEEKLY_HOURS*FTE_MULTIPLIER)+' then '+FloatToStr(FTE_WEEKLY_HOURS*FTE_MULTIPLIER)+' else HOURS end HOURS from'+
      ' (select CL_PERSON_NBR, MIN(CUSTOM_EMPLOYEE_NUMBER) CUSTOM_EMPLOYEE_NUMBER, sum(EEHOURS) HOURS from '+
      ' (select CL_PERSON_NBR, CUSTOM_EMPLOYEE_NUMBER, PERIOD_BEGIN_DATE, RUN_NUMBER, '+
      ' case when GROSSWAGES >= 0 then ' +
      ' (case when GROSSHOURS <> 0 then GROSSHOURS else (case when STANDARD_HOURS > 0 then STANDARD_HOURS else (case when FTE_WEEKLY_HOURS*WEEKS-OTHERHOURS>0 then FTE_WEEKLY_HOURS*WEEKS-OTHERHOURS else 0 end) end) end) '+
      ' else (case when GROSSHOURS <> 0 then GROSSHOURS else (case when STANDARD_HOURS > 0 then STANDARD_HOURS else (case when FTE_WEEKLY_HOURS*WEEKS-OTHERHOURS>0 then FTE_WEEKLY_HOURS*WEEKS-OTHERHOURS else 0 end) end) end)*-1.0 '+
      ' end ' +
      ' EEHOURS, '+
      '   GROSSWAGES, OTHERHOURS '+
      ' from '+
      '( select cast(:FTE_WEEKLY_HOURS as FLOAT) FTE_WEEKLY_HOURS, e.CL_PERSON_NBR, e.CUSTOM_EMPLOYEE_NUMBER, '+
      ' case b.FREQUENCY '+
      '   when ''Q'' then 13.00 '+
      '   when ''M'' then 52.000/12.000 '+
      '   when ''S'' then 52.000/24.000 '+
      '   when ''B'' then 2.00 '+
      '   when ''W'' then 1.00 '+
      '   when ''D'' then 1.000/5.000 '+
      ' end WEEKS, '+
      ' CONVERTNULLDOUBLE(e.STANDARD_HOURS, 0) STANDARD_HOURS, '+
      ' CONVERTNULLDOUBLE(e.SALARY_AMOUNT, 0) SALARY_AMOUNT,'+
      ' b.PERIOD_BEGIN_DATE, p.RUN_NUMBER, '+
      ' sum(CONVERTNULLDOUBLE(l.AMOUNT, 0)) GROSSWAGES, '+
      ' sum(case when a.EE_STATES_NBR = l.EE_SUI_STATES_NBR and ('+

      '     exists (select k.CL_E_DS_NBR from CL_E_D_GROUPS j join CL_E_D_GROUP_CODES k on k.CL_E_D_GROUPS_NBR=j.CL_E_D_GROUPS_NBR where upper(j.NAME) like ''VT HEALTHCARE'' and k.CL_E_DS_NBR=z.CL_E_DS_NBR)'+
      '     or (z.E_D_CODE_TYPE in ('''+ED_OEARN_SALARY+''','''+ED_OEARN_REGULAR+''','''+ED_OEARN_STD_EARNINGS+''','''+ED_OEARN_OVERTIME+''','''+ED_OEARN_WAITSTAFF_OVERTIME+''''+
      '        ,'''+ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME+''','''+ED_OEARN_WEIGHTED_3_HALF_TIME_OT+'''))'+
      '     or ((z.E_D_CODE_TYPE in ('''+ED_OEARN_AVG_OVERTIME+''','''+ED_OEARN_AVG_MULT_OVERTIME+''') and (z.OVERSTATE_HOURS_FOR_OVERTIME = ''N'')))'+

      ' ) then CONVERTNULLDOUBLE(l.HOURS_OR_PIECES, 0) else 0.00 end) GROSSHOURS,'+

      ' sum(case when (CONVERTNULLDOUBLE(l.HOURS_OR_PIECES, 0.00) <> 0 or CONVERTNULLDOUBLE(e.SALARY_AMOUNT, 0.00) <> 0) and a.EE_STATES_NBR = l.EE_SUI_STATES_NBR and ('+

      '     exists (select k.CL_E_DS_NBR from CL_E_D_GROUPS j join CL_E_D_GROUP_CODES k on k.CL_E_D_GROUPS_NBR=j.CL_E_D_GROUPS_NBR where upper(j.NAME) like ''VT HEALTHCARE'' and k.CL_E_DS_NBR=z.CL_E_DS_NBR)'+
      '     or (z.E_D_CODE_TYPE in ('''+ED_OEARN_SALARY+''','''+ED_OEARN_REGULAR+''','''+ED_OEARN_STD_EARNINGS+''','''+ED_OEARN_OVERTIME+''','''+ED_OEARN_WAITSTAFF_OVERTIME+''''+
      '        ,'''+ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME+''','''+ED_OEARN_WEIGHTED_3_HALF_TIME_OT+'''))'+
      '     or ((z.E_D_CODE_TYPE in ('''+ED_OEARN_AVG_OVERTIME+''','''+ED_OEARN_AVG_MULT_OVERTIME+''') and (z.OVERSTATE_HOURS_FOR_OVERTIME = ''N'')))'+

      ') then 1 else 0 end) GROSSCOUNT,'+

      ' sum(CONVERTNULLDOUBLE(l.HOURS_OR_PIECES, 0)) OTHERHOURS '+

      ' from PR_CHECK_LINES l'+
      ' join CL_E_DS z on z.CL_E_DS_NBR=l.CL_E_DS_NBR'+
      ' join PR_CHECK c on c.PR_CHECK_NBR=l.PR_CHECK_NBR '+
      ' join EE e on e.EE_NBR=c.EE_NBR '+
      ' join PR p on p.PR_NBR=c.PR_NBR '+
      ' join PR_BATCH b on b.PR_BATCH_NBR=c.PR_BATCH_NBR '+
      ' join CO_SUI k on k.SY_SUI_NBR=' + IntToStr(VT_HEALTHCARE_SY_SUI_NBR) +
      ' join CO_STATES x on x.CO_STATES_NBR=k.CO_STATES_NBR'+
      ' join EE_STATES s on s.EE_NBR=e.EE_NBR and s.CO_STATES_NBR=x.CO_STATES_NBR'+
      ' join CL_PERSON n on n.CL_PERSON_NBR=e.CL_PERSON_NBR'+
      ' left join EE_STATES a on a.EE_NBR=e.EE_NBR and a.SUI_APPLY_CO_STATES_NBR=x.CO_STATES_NBR'+
      ' left join EE_STATES f on f.EE_STATES_NBR=e.HOME_TAX_EE_STATES_NBR '+
      ' where p.CHECK_DATE between :BeginDate and :EndDate '+
      ' and e.CO_NBR in ('+CoNbrs+')'+
      ' and CONVERTNULLSTRING(a.ER_SUI_EXEMPT_EXCLUDE, ''Z'') <> ''E'' '+
      ' and c.CHECK_STATUS<>''V'' and c.CHECK_TYPE <> ''V'' '+
      ' and not exists (select 1 from PR_CHECK v where c.PR_CHECK_NBR = cast(trim(v.FILLER) as integer) and v.EE_NBR=c.EE_NBR and v.FILLER is not null and trim(v.FILLER) <> '''') '+
      ' and ageindays(p.CHECK_DATE, ''1/1/1900'') >= ageindays(addyear(CONVERTNULLDATE(n.BIRTH_DATE, addyear(p.CHECK_DATE, -18)), 18), ''1/1/1900'') ' +
      ' and ((x.COMPANY_PAID_HEALTH_INSURANCE=''Y'' and e.HEALTHCARE_COVERAGE<> '''+HEALTHCARE_ELIGIBLE_INSURED+''') '+
            'or (x.COMPANY_PAID_HEALTH_INSURANCE<>''Y'' and f.SUI_APPLY_CO_STATES_NBR=x.CO_STATES_NBR))'+

      ' and (z.E_D_CODE_TYPE like ''E%'' or z.E_D_CODE_TYPE like ''M%'' '+
      '     or exists (select k.CL_E_DS_NBR from CL_E_D_GROUPS j join CL_E_D_GROUP_CODES k on k.CL_E_D_GROUPS_NBR=j.CL_E_D_GROUPS_NBR where upper(j.NAME) like ''VT HEALTHCARE'' and k.CL_E_DS_NBR=z.CL_E_DS_NBR)'+
      '     or (z.E_D_CODE_TYPE in ('''+ED_OEARN_SALARY+''','''+ED_OEARN_REGULAR+''','''+ED_OEARN_STD_EARNINGS+''','''+ED_OEARN_OVERTIME+''','''+ED_OEARN_WAITSTAFF_OVERTIME+''''+
      '        ,'''+ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME+''','''+ED_OEARN_WEIGHTED_3_HALF_TIME_OT+'''))'+
      '     or ((z.E_D_CODE_TYPE in ('''+ED_OEARN_AVG_OVERTIME+''','''+ED_OEARN_AVG_MULT_OVERTIME+''') and (z.OVERSTATE_HOURS_FOR_OVERTIME = ''N''))))'+
      ' and {AsOfDate<x>} '+
      ' and {AsOfDate<p>} '+
      ' and {AsOfDate<l>} '+
      ' and {AsOfDate<c>} '+
      ' and {AsOfDate<f>} '+
      ' and {AsOfDate<s>} '+
      ' and {AsOfDate<a>} '+
      ' and {AsOfDate<n>} '+
      ' and {AsOfDate<b>} '+
      ' and {AsOfDate<k>} '+
      ' and {AsOfDate<z>} '+
      ' and {AsOfDate<e>} ' +
      ' group by 1,2,3,4,5,6,7,8'+
      ') where GROSSCOUNT > 0 ) group by 1) order by 1,2');



//    sl := TStringList.Create;

    E.Param['BeginDate'] := GetBeginQuarter(FEndDate);
    E.Param['EndDate'] := FEndDate;
    E.Param['StatusDate'] := FEndDate;
    E.Param['FTE_WEEKLY_HOURS'] := FTE_WEEKLY_HOURS;
    E.Execute;
    E.Result.First;
    while not E.Result.Eof do
    begin
//      sl.Add(E.Result.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString + ' ' + E.Result.FieldByName('HOURS').AsString);
      Result := Result + E.Result['HOURS'];
      E.Result.Next;
    end;
//    sl.SaveToFile('c:\1297.txt');
  end;

var
  CONSOLIDATION_TYPE: String;
  CONSOLIDATION_SCOPE: String;
  CoNumbers: String;
begin
  E := TevQuery.Create('select FTE_WEEKLY_HOURS, FTE_MULTIPLIER, FTE_EXEMPTION, GLOBAL_RATE from SY_SUI where SY_SUI_NBR = '+IntToStr(VT_HEALTHCARE_SY_SUI_NBR)+' and {AsOfDate<SY_SUI>}');
  E.Param['StatusDate'] := FEndDate;
  FTE_WEEKLY_HOURS := E.Result['FTE_WEEKLY_HOURS'];
  FTE_MULTIPLIER := E.Result['FTE_MULTIPLIER'];
  FTE_EXEMPTION := ConvertNull(E.Result['FTE_EXEMPTION'],0);
  GLOBAL_RATE := ConvertNull(E.Result['GLOBAL_RATE'],0);

  TotalHoursWorked := 0;

  if DM_COMPANY.CO.FieldByName('CL_CO_CONSOLIDATION_NBR').IsNull then
    TotalHoursWorked := CalcHours(IntToStr(CONbr))
  else
  begin
    DM_CLIENT.CL_CO_CONSOLIDATION.DataRequired('ALL');

    CONSOLIDATION_TYPE := DM_CLIENT.CL_CO_CONSOLIDATION.Lookup('CL_CO_CONSOLIDATION_NBR', DM_COMPANY.CO.FieldByName('CL_CO_CONSOLIDATION_NBR').Value, 'CONSOLIDATION_TYPE');
    CONSOLIDATION_SCOPE := DM_CLIENT.CL_CO_CONSOLIDATION.Lookup('CL_CO_CONSOLIDATION_NBR', DM_COMPANY.CO.FieldByName('CL_CO_CONSOLIDATION_NBR').Value, 'SCOPE');
    if (CONSOLIDATION_TYPE = CONSOLIDATION_TYPE_FED) or (CONSOLIDATION_SCOPE = CONSOLIDATION_SCOPE_PAYROLL_ONLY) then
      TotalHoursWorked := CalcHours(IntToStr(CONbr))
    else
    if DM_CLIENT.CL_CO_CONSOLIDATION.Lookup('CL_CO_CONSOLIDATION_NBR', DM_COMPANY.CO.FieldByName('CL_CO_CONSOLIDATION_NBR').Value, 'PRIMARY_CO_NBR') = CoNbr then
    begin
      CONSOLIDATED_COMPANIES := TevQuery.Create('GenericSelect2CurrentWithCondition');
      CONSOLIDATED_COMPANIES.Macro['COLUMNS'] :='T1.CO_NBR, T2.CO_STATES_NBR';
      CONSOLIDATED_COMPANIES.Macro['TABLE1'] := 'CO';
      CONSOLIDATED_COMPANIES.Macro['TABLE2'] := 'CO_SUI';
      CONSOLIDATED_COMPANIES.Macro['JOINFIELD'] := 'CO';
      CONSOLIDATED_COMPANIES.Macro['CONDITION'] := 'T1.CL_CO_CONSOLIDATION_NBR=' + DM_COMPANY.CO.FieldByName('CL_CO_CONSOLIDATION_NBR').AsString
              + ' and T2.SY_SUI_NBR=' + CO_SUI.FieldByName('SY_SUI_NBR').AsString;
      CONSOLIDATED_COMPANIES.Result.First;
      if not CONSOLIDATED_COMPANIES.Result.Eof then
      begin
        CoNumbers := CONSOLIDATED_COMPANIES.Result.FieldByName('CO_NBR').AsString;
        CONSOLIDATED_COMPANIES.Result.Next;
        while not CONSOLIDATED_COMPANIES.Result.Eof do
        begin
          CoNumbers := CoNumbers + ',' + CONSOLIDATED_COMPANIES.Result.FieldByName('CO_NBR').AsString;
          CONSOLIDATED_COMPANIES.Result.Next;
        end;
        TotalHoursWorked := CalcHours(CoNumbers);
      end;
    end;
  end;

  Result := (RoundDown(TotalHoursWorked /(FTE_WEEKLY_HOURS*FTE_MULTIPLIER)) - FTE_EXEMPTION) * GLOBAL_RATE;
  if Result < 0 then
    Result := 0;
end;

procedure TDM_MISC_LIAB_CORRECT.AddLine(const TaxType: TTaxType;
  const TaxNbr: Variant; const TaxDesc: string; const TotalCalc,
  TotalReal: Currency; const DueDate: TDateTime; const AdjType: String; const State: string);
begin
  if (TotalReal- TotalCalc) <> 0 then
  begin
    cdTaxes.Append;
    cdTaxes['TaxType'] := IntToHexChar(Ord(TaxType));
    cdTaxes['TaxNbr'] := TaxNbr;
    cdTaxes['TaxDesc'] := TaxDesc;
    cdTaxes['TotalCalc'] := TotalCalc;
    cdTaxes['TotalReal'] := TotalReal;
    if (DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['Sy_Sui_Nbr'], 'E_D_CODE_TYPE') = null) then
      cdTaxes['TotalDiff'] := TotalReal- TotalCalc
    else
      cdTaxes['TotalDiff'] := 0;
    cdTaxes['ToDo'] := (TaxType <> ttFui) or (EndDate = GetEndYear(EndDate));
    cdTaxes['DueDate'] := DueDate;
    cdTaxes['AdjType'] := AdjType;
    cdTaxes['State'] := State;
    cdTaxes.Post;
  end;
end;

constructor TDM_MISC_LIAB_CORRECT.Create(AOwner: TComponent; const ClNbr, CoNbr: Integer; const TaxService: Boolean; const EndDate: TDateTime;
  const LoadFUI, LoadState, LoadSUI, LoadLocal: Boolean);
begin
  inherited Create(AOwner);
  FClNbr := ClNbr;
  FCoNbr := CoNbr;
  FEndDate := EndDate;
  FHasTaxService := TaxService;
  FLoadFUI := LoadFUI;
  FLoadState := LoadState;
  FLoadLocal := LoadLocal;
  FLoadSUI := LoadSUI;
  cdTaxes.CreateDataSet;
  cdTaxes.LogChanges := False;
  CO := TevClientDataSet.Create(Self);
  CO.ProviderName := DM_COMPANY.CO.ProviderName;
  CO_STATES := TevClientDataSet.Create(Self);
  CO_STATES.ProviderName := DM_COMPANY.CO_STATES.ProviderName;
  CO_LOCALS := TevClientDataSet.Create(Self);
  CO_LOCALS.ProviderName := DM_COMPANY.CO_LOCAL_TAX.ProviderName;
  CO_SUI := TevClientDataSet.Create(Self);
  CO_SUI.ProviderName := DM_COMPANY.CO_SUI.ProviderName;
  SY_FED_TAX_TABLE := TevClientDataSet.Create(Self);
  SY_FED_TAX_TABLE.ProviderName := DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.ProviderName;
  SY_SUI := TevClientDataSet.Create(Self);
  SY_SUI.ProviderName := DM_SYSTEM_FEDERAL.SY_SUI.ProviderName;
  cdWages := TevClientDataSet.Create(Self);
  cdLiabs := TevClientDataSet.Create(Self);
  Load;
end;

procedure TDM_MISC_LIAB_CORRECT.Load;
var
  Wages, LCalc, LReal: Currency;
  State, AdjType: String;
  IlRate, Rate: Real;
  Nbr: Integer;
  DueDate, PeriodBegDate, PeriodEndDate: TDateTime;
  EeCount : integer;
  FutureDefaultRate: Real;
  FutureMaximumWage: Real;
  Q: IevQuery;
begin
  DM_SYSTEM_LOCAL.SY_LOCALS.AsOfDate := Trunc(FEndDate);
  DM_SYSTEM_LOCAL.SY_LOCALS.Activate;

  DM_SYSTEM_STATE.SY_STATES.AsOfDate := Trunc(FEndDate);
  DM_SYSTEM_STATE.SY_STATES.Activate;

  DM_SYSTEM_STATE.SY_SUI.AsOfDate := Trunc(FEndDate);
  DM_SYSTEM_STATE.SY_SUI.Activate;
  ctx_DataAccess.OpenClient(ClNbr);
  DM_COMPANY.CO.CheckDSConditionAndClient(ClNbr, '');

  CO_SUI.AsOfDate := Trunc(FEndDate);
  CO_LOCALS.AsOfDate := Trunc(FEndDate);
  CO_STATES.AsOfDate := Trunc(FEndDate);
  CO.AsOfDate := Trunc(FEndDate);
  SY_SUI.AsOfDate := Trunc(FEndDate);

  PopulateDataSets([SY_FED_TAX_TABLE, SY_SUI], '', False);
  PopulateDataSets([CO, CO_STATES, CO_LOCALS, CO_SUI], 'CO_NBR='+ IntToStr(CONbr), False);
  ctx_DataAccess.OpenDataSets([DM_COMPANY.CO, CO, CO_STATES, CO_LOCALS, CO_SUI, SY_FED_TAX_TABLE, SY_SUI]);
  if CO.Locate('CO_NBR', CoNbr, [])
  and DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []) then
  begin
    if LoadSui then
    begin

      Q := TevQuery.Create('ConsolidationQuarterSuiWages');
      Q.Param['BegDate'] := GetBeginQuarter(EndDate);
      Q.Param['EndDate'] := EndDate;
      Q.Param['CoNbr'] := CoNbr;
      cdWages.Data := Q.Result.vclDataset.Data;

      Q := TevQuery.Create('ConsolidationQuarterSuiLiabs');
      Q.Param['BegDate'] := GetBeginQuarter(EndDate);
      Q.Param['EndDate'] := EndDate;
      Q.Param['CoNbr'] := CoNbr;
      cdLiabs.Data := Q.Result.vclDataset.Data;

      CO_SUI.First;
      while not CO_SUI.Eof do
      begin
        Assert(CO_STATES.Locate('CO_STATES_NBR', CO_SUI['CO_STATES_NBR'], []));
          if (CO_SUI['CO_NBR'] = CoNbr) and (CO_STATES['SUI_EXEMPT'] = GROUP_BOX_NO)
// Ignoring NM EE & ER Workers Comp as they won't balance by design ////////////
              and not (CO_SUI.FieldByName('SY_SUI_NBR').AsInteger in [123, 124])
////////////////////////////////////////////////////////////////////////////////
           then
          begin
            Assert(SY_SUI.Locate('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], []));
            Rate := ConvertNull(SY_SUI['GLOBAL_RATE'], ConvertNull(CO_SUI['RATE'], 0));
            if CO_SUI['SY_SUI_NBR'] = 130 then {VT Healthcare}
              LReal := CalcVTHealthcare
            else
            if cdWages.Locate('CO_SUI_NBR', CO_SUI['CO_SUI_NBR'], []) then
            begin
              Wages := ConvertNull(cdWages['wages'], 0);

              if CO_SUI['SY_SUI_NBR'] = 55 then // LA SUI
              begin
                Q := TevQuery.Create(
                  'select k.ee_nbr, sum(s.SUI_TAXABLE_WAGES) wages, sum(s.SUI_GROSS_WAGES) gross_wages '+
                  'from CO_SUI cs '+
                  'join CO_STATES cst on cst.co_states_nbr = cs.co_states_nbr and {AsOfNow<cst>} ' +
                  'join CO_STATES cst2 on '+
                    'cst2.state = cst.state and {AsOfNow<cst2>} and '+
                    '(cst2.co_nbr = cst.co_nbr or exists ('+
                      'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
                      'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
                        'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
                        '((cn.PRIMARY_CO_NBR = cst.co_nbr)) and '+
                        'o.co_nbr=cst2.co_nbr and '+
                        'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') and '+
                        'cn.SCOPE <> '''+ CONSOLIDATION_SCOPE_PAYROLL_ONLY + ''' '+
                    ')) '+
                  'join CO_SUI cs2 on cs2.sy_sui_nbr = cs.sy_sui_nbr and cs2.co_states_nbr = cst2.co_states_nbr and {AsOfNow<cs2>} '+
                  'join PR p on p.check_date between :BegDate and :EndDate and {AsOfNow<p>} and p.co_nbr = cs2.co_nbr and p.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''','''+ PAYROLL_STATUS_VOIDED+ ''') '+
                  'join PR_CHECK_SUI s on s.co_sui_nbr = cs2.co_sui_nbr and s.pr_nbr=p.pr_nbr and {AsOfNow<s>} ' +
                  'join PR_CHECK k on k.pr_check_nbr=s.pr_check_nbr and {AsOfNow<k>} ' +
                  'where cs.co_nbr = :CoNbr and cs.SY_SUI_NBR=55 and {AsOfNow<cs>} ' +
                  'group by 1');

                Q.Param['BegDate'] := GetBeginQuarter(EndDate);
                Q.Param['EndDate'] := EndDate;
                Q.Param['CoNbr'] := CoNbr;
                Wages := 0;
                Q.Result.First;
                while not Q.Result.Eof do
                begin
                  Wages := Wages + RoundAll(ConvertNull(Q.Result['wages'], 0), 0);
                  Q.Result.Next;
                end;
              end;

              if CO_SUI['SY_SUI_NBR'] = 118 then {NV Business Tax}
                Rate := 0.005;

              if (CO_SUI['SY_SUI_NBR'] = 15) and (cdWages['gross_wages'] < 50000) then {IL ER SUI}
              begin
               IlRate := 0;
               if ConvertNull(CO_SUI.Lookup('CO_NBR;SY_SUI_NBR', VarArrayOf([CO_SUI['CO_NBR'], 83]), 'FINAL_TAX_RETURN') , 'Y') <> 'Y' then
               begin
                if not VarIsNull(SY_SUI.Lookup('SY_SUI_NBR', 83, 'GLOBAL_RATE')) then
                  IlRate := SY_SUI.Lookup('SY_SUI_NBR', 83, 'GLOBAL_RATE')
                else
                  IlRate := ConvertNull(CO_SUI.Lookup('CO_NBR;SY_SUI_NBR', VarArrayOf([CO_SUI['CO_NBR'], 83]), 'RATE'), 0);
               end;
               if (Rate+ IlRate) > 0.054 then
                 Rate := 0.054- IlRate;
              end;

              if (CO_SUI['SY_SUI_NBR'] = 58) then // Massachusetts Healthcare ER SUI
              begin
                with ctx_DataAccess.CUSTOM_VIEW do
                begin
                  EeCount := 0;
                  if Active then
                    Close;
                  with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                  begin
                    SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate), 12));
                    SetParam('CoNbr', CoNbr);
                    SetMacro('SUINbrValues', '58');
                    DataRequest(AsVariant);
                  end;
                  Open;
                  First;
                  EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                  Close;

                  with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                  begin
                    SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 1, 12));
                    SetParam('CoNbr', CoNbr);
                    SetMacro('SUINbrValues', '58');
                    DataRequest(AsVariant);
                  end;
                  Open;
                  First;
                  EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                  Close;

                  with TExecDSWrapper.Create('TotalSUIEmployeesByPeriod') do
                  begin
                    SetParam('MidDate', EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 2, 12));
                    SetParam('CoNbr', CoNbr);
                    SetMacro('SUINbrValues', '58');
                    DataRequest(AsVariant);
                  end;
                  Open;
                  First;
                  EeCount := EeCount + ConvertNull(FieldByName('EE_COUNT').Value, 0);
                  Close;
                end;
                if EeCount / 3 < 6 then
                  Rate := 0
              end;

              State := CO_STATES['STATE'];
              if (State = 'NY') or (State = 'MA') then
                Wages := RoundAll(Wages, 0)
              else
              if (State = 'MN') then
                Wages := RoundDown(Wages);

              if CO_SUI['SY_SUI_NBR'] = 52 then // MI-SUI
                LReal := RoundAll(Wages * Rate, 0)
              else
                LReal := RoundTwo(Wages * Rate);

              if (CO_SUI['SY_SUI_NBR'] = 118 {NV Business Tax}) then
              begin
                if CompareDate(EndDate, EncodeDate(2015,  6, 30)) = GreaterThanValue then
                begin
                  if DoubleCompare(Wages, coGreater, 50000) then
                    LReal := (Wages - 50000) * 0.01475
                  else
                    LReal := 0;
                end
                else
                if CompareDate(EndDate, EncodeDate(2013,  6, 30)) = GreaterThanValue then
                begin
                  if DoubleCompare(Wages, coGreater, 85000) then
                    LReal := (Wages - 85000) * 0.0117
                  else
                    LReal := 0;
                end
                else              
                if CompareDate(EndDate, EncodeDate(2011,  6, 30)) = GreaterThanValue then
                begin
                  if DoubleCompare(Wages, coGreater, 62500) then
                    LReal := (Wages - 62500) * 0.0117
                  else
                    LReal := 0;
                end
                else
                if (CompareDate(EndDate, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
                // new logic
                begin
                  if DoubleCompare(Wages, coLessOrEqual ,62500) then
                    LReal := Wages * 0.005
                  else if DoubleCompare(Wages, coGreater, 62500) then
                    LReal := 312.50 + (Wages - 62500) * 0.0117;
                end
                else
                // old logic
                if Wages > 62500 then
                  LReal := LReal + (Wages - 62500) * 0.0067;
              end;

              if (State = 'MN') then
              begin
                FutureDefaultRate := ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], 'FUTURE_DEFAULT_RATE'), 0);
                FutureMaximumWage := ConvertNull(SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], 'FUTURE_MAXIMUM_WAGE'), 0);

                LReal := LReal + LReal * (FutureDefaultRate - FutureMaximumWage);
                LReal := LReal + LReal * FutureMaximumWage;
                LReal := RoundTwo(LReal);
              end;
            end
            else
              LReal := 0;
            if DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], 'ROUND_TO_NEAREST_DOLLAR') = GROUP_BOX_YES then // round to whole dollars
              LReal := RoundAll(LReal, 0);
            if cdLiabs.Locate('CO_SUI_NBR', CO_SUI['CO_SUI_NBR'], []) then
              LCalc := ConvertNull(cdLiabs['liabs'], 0)
            else
              LCalc := 0;
            if (LCalc <> LReal) then
            begin
              ctx_PayrollCalculation.GetSuiTaxDepositPeriod(CO_SUI['SY_SUI_NBR'], EndDate, PeriodBegDate, PeriodEndDate, DueDate);
              if (CO_SUI['SY_SUI_NBR'] <> 118) or ((CO_SUI['SY_SUI_NBR'] = 118) and (PeriodEndDate > StrToDate('07/01/2009'))) then
              begin
                if (CO_SUI['SY_SUI_NBR'] = 130 {VT Healthcare}) or (CO_SUI['SY_SUI_NBR'] = 118 {NV Business Tax}) then
                  AdjType := TAX_LIABILITY_ADJUSTMENT_TYPE_NONE
                else
                  AdjType := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
                AddLine(ttSui, CO_SUI['CO_SUI_NBR'], 'SUI - '+ CO_SUI['DESCRIPTION'], LCalc, LReal, DueDate, AdjType, CO_STATES['STATE']);
              end;
            end;
            if (Integer(CO_SUI['SY_SUI_NBR']) in [6, 65]) {CO}
            and (CO_STATES['MO_TAX_CREDIT_ACTIVE'] = GROUP_BOX_YES) then
              with ctx_DataAccess.CUSTOM_VIEW do
              begin
                Close;
                with TExecDSWrapper.Create('ConsolidationQuarterSuiCreditLiabs') do
                begin
                  SetMacro('m1', ' = ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NONE  +''' or ADJUSTMENT_TYPE <>');
                  SetParam('BegDate', GetBeginYear(EndDate));
                  SetParam('EndDate', EndDate);
                  SetParam('CoNbr', CoNbr);
                  SetParam('SySuiNbrs', CO_SUI['SY_SUI_NBR']);
                  DataRequest(AsVariant);
                end;
                Open;
                LReal := RoundTwo(ConvertNull(FieldValues['liabs'], 0)); // liabs w/o credits
                Close;
                with TExecDSWrapper.Create('ConsolidationQuarterSuiCreditLiabs') do
                begin
                  SetMacro('m1', '=');
                  SetParam('BegDate', GetBeginYear(EndDate));
                  SetParam('EndDate', EndDate);
                  SetParam('CoNbr', CoNbr);
                  SetParam('SySuiNbrs', CO_SUI['SY_SUI_NBR']);
                  DataRequest(AsVariant);
                end;
                Open;
                LCalc := RoundTwo(ConvertNull(FieldValues['liabs'], 0)); // existing credits
                LReal := RoundTwo(LReal* 0.2); // expected credit
                if LCalc- LReal <> 0 then
                begin
                  ctx_PayrollCalculation.GetSuiTaxDepositPeriod(CO_SUI['SY_SUI_NBR'], EndDate, PeriodBegDate, PeriodEndDate, DueDate);
                  AddLine(ttSui, CO_SUI['CO_SUI_NBR'], 'CO SUI credit', LCalc, -LReal,
                    DueDate, TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT, CO_STATES['STATE']);
                end;
              end;
          end;
        CO_SUI.Next;
      end;
    end;
    if LoadState then
    begin
      CO_STATES.First;
      while not CO_STATES.Eof do
      begin
        if CO_STATES['MO_TAX_CREDIT_ACTIVE'] = GROUP_BOX_YES then
          if CO_STATES['STATE'] = 'MO' then
            with ctx_DataAccess.CUSTOM_VIEW do
            begin
              Close;
              with TExecDSWrapper.Create('ConsolidationQuarterMoLiabs') do
              begin
                SetMacro('m1', ' = ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NONE  +''' or ADJUSTMENT_TYPE <>');
                SetParam('BegDate', GetBeginYear(EndDate));
                SetParam('EndDate', EndDate);
                SetParam('CoNbr', CoNbr);
                DataRequest(AsVariant);
              end;
              Open;
              LReal := RoundAll(ConvertNull(FieldValues['liabs'], 0), 0); // MO state liabs w/o credits
              Nbr := ConvertNull(FieldValues['co_states_nbr'], 0);
              Close;
              with TExecDSWrapper.Create('ConsolidationQuarterMoLiabs') do
              begin
                SetMacro('m1', '=');
                SetParam('BegDate', GetBeginYear(EndDate));
                SetParam('EndDate', EndDate);
                SetParam('CoNbr', CoNbr);
                DataRequest(AsVariant);
              end;
              Open;
              LCalc := RoundAll(ConvertNull(FieldValues['liabs'], 0), 0); // MO state liab existing credits
              if LReal <= 5000 then
                LReal := LReal * 0.02 // 2%
              else
              if LReal <= 10000 then
                LReal := 5000*0.02+ (LReal-5000) * 0.01 // 1%
              else
                LReal := 5000*0.02+ 5000*0.01+ (LReal-10000) * 0.005; // 1/2%
              LReal := RoundAll(LReal, 0);
              if LCalc- LReal <> 0 then
              begin
                Assert(CO_STATES.Locate('CO_NBR;STATE', VarArrayOf([CoNbr, 'MO']), []));
                ctx_PayrollCalculation.GetStateTaxDepositPeriod('MO', CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'],
                  EndDate, PeriodBegDate, PeriodEndDate, DueDate);
                AddLine(ttState, Nbr, 'MO State credit', LCalc, -LReal, DueDate, TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT, CO_STATES['STATE']);
              end;
            end;
        CO_STATES.Next;
      end;
    end;
    if LoadLocal then
    begin
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        Close;
        with TExecDSWrapper.Create('ConsolidationQuarterLocalWages') do
        begin
          SetParam('BegDate', GetBeginQuarter(EndDate));
          SetParam('EndDate', EndDate);
          SetParam('CoNbr', CoNbr);
          DataRequest(AsVariant);
        end;
        Open;
        cdWages.Data := Data;
        Close;
        with TExecDSWrapper.Create('ConsolidationQuarterLocalLiabs') do
        begin
          SetParam('BegDate', GetBeginQuarter(EndDate));
          SetParam('EndDate', EndDate);
          SetParam('CoNbr', CoNbr);
          DataRequest(AsVariant);
        end;
        Open;
        cdLiabs.Data := Data;
        Close;
      end;
      CO_LOCALS.First;
      while not CO_LOCALS.Eof do
      begin
        if (CO_LOCALS['LOCAL_ACTIVE'] = GROUP_BOX_YES)
        and DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCALS['SY_LOCALS_NBR'], [])
        and (DM_SYSTEM_LOCAL.SY_LOCALS['LOCAL_ACTIVE'] = GROUP_BOX_YES)
        and (DM_SYSTEM_LOCAL.SY_LOCALS['CALCULATION_METHOD'] = CALC_METHOD_GROSS) then
        begin
          Assert(DM_SYSTEM_LOCAL.SY_STATES.Locate('SY_STATES_NBR', CO_LOCALS['SY_STATES_NBR'], []));

          //    80102   --   it works only NY-MCT Mobility Tax (sy_locals_nbr = 5724)
          if DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('SY_LOCALS_NBR').AsInteger = 5724 then
          begin

            Q := TevQuery.Create('select TAX_RATE from CO_LOCAL_TAX where CO_NBR = :CoNbr and SY_LOCALS_NBR=:SyLocalsNbr and {AsOfDate<CO_LOCAL_TAX>}');
            Q.Params.AddValue('StatusDate', EndDate);
            Q.Params.AddValue('SyLocalsNbr', CO_LOCALS['SY_LOCALS_NBR']);
            Q.Params.AddValue('CoNbr', CO_LOCALS['CO_NBR']);
            if not Q.Result.FieldByName('TAX_RATE').IsNull then
              Rate := Q.Result.FieldByName('TAX_RATE').Value
            else
            begin
              Q := TevQuery.Create('select TAX_RATE from SY_LOCALS where SY_LOCALS_NBR=:SyLocalsNbr and {AsOfDate<SY_LOCALS>}');
              Q.Params.AddValue('StatusDate', EndDate);
              Q.Params.AddValue('SyLocalsNbr', CO_LOCALS['SY_LOCALS_NBR']);
              Rate := ConvertNull(Q.Result.FieldByName('TAX_RATE').Value, 0);
            end;
            
            if cdWages.Locate('CO_LOCAL_TAX_nbr', CO_LOCALS['CO_LOCAL_TAX_nbr'], []) then
            begin
              Wages := ConvertNull(cdWages['wages'], 0);
              LReal := RoundTwo(Wages* Rate);
            end
            else
              LReal := 0;
            if DM_SYSTEM_LOCAL.SY_LOCALS['ROUND_TO_NEAREST_DOLLAR'] = GROUP_BOX_YES then // round to whole dollars
              LReal := RoundAll(LReal, 0);
            if cdLiabs.Locate('CO_LOCAL_TAX_nbr', CO_LOCALS['CO_LOCAL_TAX_nbr'], []) then
              LCalc := ConvertNull(cdLiabs['liabs'], 0)
            else
              LCalc := 0;
            if LCalc <> LReal then
              if (CO_LOCALS['SELF_ADJUST_TAX'] <> GROUP_BOX_NO)
              or (Abs(LCalc- LReal) <= 1) then
              begin
                ctx_PayrollCalculation.GetLocalTaxDepositPeriod(CO_LOCALS['SY_LOCALS_NBR'],
                 CO_LOCALS['SY_LOCAL_DEPOSIT_FREQ_NBR'], EndDate, PeriodBegDate, PeriodEndDate, DueDate);
                AddLine(ttLocal, CO_LOCALS['CO_LOCAL_TAX_nbr'], 'Local - '+ DM_SYSTEM_LOCAL.SY_LOCALS['NAME'], LCalc, LReal, DueDate, TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END, DM_SYSTEM_LOCAL.SY_STATES['STATE']);
              end;
          end;
        end;
        CO_LOCALS.Next;
      end;
    end;
  end;
end;

procedure TDM_MISC_LIAB_CORRECT.cdTaxesAfterInsert(DataSet: TDataSet);
begin
  DataSet.Tag := DataSet.Tag+ 1;
  DataSet['NBR'] := DataSet.Tag;
end;

procedure TDM_MISC_LIAB_CORRECT.ApplyChanges;
var
  lPrNbr: Integer;
  function FakePrNbr: Integer;
    function GetNextRunNumber(const TheDate: TDateTime): Integer;
    begin
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then Close;
        with TExecDSWrapper.Create('NextRunNumberForCheckDate') do
        begin
          SetParam('CheckDate', TheDate);
          SetParam('Co_Nbr', CoNbr);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        Result := ConvertNull(Fields[0].Value, 0);
        Close;
        Result := Result + 1;
      end;
    end;
  begin
    if lPrNbr = 0 then
      with DM_PAYROLL, DM_COMPANY do
      try
        PR.Insert;
        PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
        PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
        PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
        PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
        PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
        PR.FieldByName('CO_NBR').AsInteger := CoNbr;
        PR.FieldByName('CHECK_DATE').AsDateTime := FEndDate;
        PR.FieldByName('PROCESS_DATE').AsDateTime := Date;
        PR.FieldByName('RUN_NUMBER').AsInteger := GetNextRunNumber(PR.FieldByName('CHECK_DATE').AsDateTime);
        PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
        PR.FieldByName('SCHEDULED').Value := 'N';
        PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_TAX_ADJUSTMENT;
        PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
        PR.FieldByName('EXCLUDE_ACH').Value := 'N';
        PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
        PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y';
        PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
        PR.FieldByName('EXCLUDE_BILLING').Value := 'Y';
        PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_REPORTS;
        PR.TAX_IMPOUND.Value := CO.TAX_IMPOUND.Value;
        PR.TRUST_IMPOUND.Value := CO.TRUST_IMPOUND.Value;
        PR.DD_IMPOUND.Value := CO.DD_IMPOUND.Value;
        PR.BILLING_IMPOUND.Value := CO.BILLING_IMPOUND.Value;
        PR.WC_IMPOUND.Value := CO.WC_IMPOUND.Value;

        PR.Post;
        if HasTaxService then
        begin
          CO_PR_ACH.Insert;
          CO_PR_ACH.FieldByName('PR_NBR').Value := PR['PR_NBR'];
          CO_PR_ACH.FieldByName('CO_NBR').Value := CoNbr;
          CO_PR_ACH.FieldByName('STATUS').Value := COMMON_REPORT_STATUS__PENDING;
          CO_PR_ACH.FieldByName('ACH_DATE').Value := Date;
          CO_PR_ACH.FieldByName('AMOUNT').Value := 0;
          CO_PR_ACH.TAX_IMPOUND.Value := PR.TAX_IMPOUND.Value;
          CO_PR_ACH.TRUST_IMPOUND.Value := PR.TRUST_IMPOUND.Value;
          CO_PR_ACH.DD_IMPOUND.Value := PR.DD_IMPOUND.Value;
          CO_PR_ACH.BILLING_IMPOUND.Value := PR.BILLING_IMPOUND.Value;
          CO_PR_ACH.WC_IMPOUND.Value := PR.WC_IMPOUND.Value;
          CO_PR_ACH.STATUS_DATE.Value := Date;

          CO_PR_ACH.Post;
        end;
        lPrNbr := PR['PR_NBR'];
      except
        PR.Cancel;
        raise;
      end;
    Result := lPrNbr;
  end;
var
  cd: TevClientDataSet;
begin
  lPrNbr := 0;
  ctx_DataAccess.OpenClient(ClNbr);
  with DM_COMPANY, DM_PAYROLL do
  begin
    CO.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
    PopulateDataSets([CO_PR_ACH, CO_FED_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, PR], 'CO_NBR=-1', False);
    ctx_DataAccess.OpenDataSets([CO, CO_PR_ACH, CO_FED_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, PR]);
    cdTaxes.First;
    while not cdTaxes.Eof do
    begin
      if (cdTaxes['ToDo']) and (cdTaxes['TotalDiff'] <> 0) then
      begin
        cd := nil;
        case TTaxType(Integer(cdTaxes['TaxType'])) of
        ttFUI:
          begin
            cd := DM_COMPANY.CO_FED_TAX_LIABILITIES;
            cd.Append;
            cd['TAX_TYPE'] := TAX_LIABILITY_TYPE_ER_FUI;
          end;
        ttSUI:
          begin
            cd := DM_COMPANY.CO_SUI_LIABILITIES;
            cd.Append;
            cd['CO_SUI_NBR'] := cdTaxes['TaxNbr'];
          end;
        ttState:
          begin
            cd := DM_COMPANY.CO_STATE_TAX_LIABILITIES;
            cd.Append;
            cd['CO_STATES_NBR'] := cdTaxes['TaxNbr'];
            cd['TAX_TYPE'] := TAX_LIABILITY_TYPE_STATE;
          end;
        ttLocal:
          begin
            cd := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;
            cd.Append;
            cd['CO_LOCAL_TAX_nbr'] := cdTaxes['TaxNbr'];
          end;
        else
          Assert(False);
        end;
        cd['CO_NBR'] := CoNbr;
        {if HasTaxService then
          cd['STATUS'] := TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED
        else}
        cd['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
        cd['ADJUSTMENT_TYPE'] := cdTaxes['AdjType'];
        cd['DUE_DATE'] := cdTaxes['DueDate'];
        cd['AMOUNT'] := cdTaxes['TotalDiff'];
        cd['THIRD_PARTY'] := 'N';
        cd['CHECK_DATE'] := FEndDate;
        cd['PR_NBR'] := FakePrNbr;
        cd.Post;
      end;
      cdTaxes.Next;
    end;
    ctx_DataAccess.PostDataSets([PR, CO_PR_ACH, CO_FED_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES]);
  end;
end;

end.

