unit EvVersionedCOStatesFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, DBCtrls, isUIwwDBComboBox, SFieldCodeValues,
  wwdblook, isUIwwDBLookupCombo, SDataDictsystem;

type
  TCDObject = class(TObject)
    SY_GLOBAL_AGENCY_NBR : String;
    AGENCY_NAME          : String;
  end;

type
  TEvVersionedCOStates = class(TevVersionedFieldBase)
    isUIFashionPanel1: TisUIFashionPanel;
    pnlBottom: TevPanel;
    cbTCDDEpositFrequency: TevDBLookupCombo;
    lblTCDDeposit: TevLabel;
    evLabel3: TevLabel;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    cbTaxCollectionDistrict: TevComboBox;
    procedure FormShow(Sender: TObject);
    procedure UpdateFieldOnChange(Sender: TObject);
    procedure dsFieldDataDataChange(Sender: TObject; Field: TField);
    procedure dsFieldDataStateChange(Sender: TObject);
    procedure cbTaxCollectionDistrictChange(Sender: TObject);
  protected
    pTCDObject : TCDObject;
    //SY_GLOBAL_AGENCY_NBR : String;
    procedure SyncControlsState; override;
    procedure ClearCollectionDistricts;
    procedure ResetDepositFrequency(SY_GLOBAL_AGENCY_NBR: String);
    procedure LocateDistrict;
    procedure LoadCollectionDistricts;
  end;

implementation

{$R *.dfm}


{ TEvVersionedEELocal }


procedure TEvVersionedCOStates.FormShow(Sender: TObject);

begin
  inherited;

  Fields[0].AddValue('Title', lblTCDDeposit.Caption);
  Fields[0].AddValue('Width', 18);
  lblTCDDeposit.Caption := Iff(Fields[0].Value['Required'], '~', '') + lblTCDDeposit.Caption;
  cbTCDDEpositFrequency.DataField := Fields.ParamName(0);

end;

procedure TEvVersionedCOStates.SyncControlsState;


begin
  inherited;

end;

procedure TEvVersionedCOStates.UpdateFieldOnChange(
  Sender: TObject);
begin
//  (Sender as TevDBEdit).UpdateRecord;
end;


procedure TEvVersionedCOStates.ClearCollectionDistricts;
begin
  While cbTaxCollectionDistrict.Items.Count > 0 do
  begin
    TCDObject(cbTaxCollectionDistrict.Items.Objects[0]).Free;
    cbTaxCollectionDistrict.Items.Delete(0);
  end;
end;

procedure TEvVersionedCOStates.ResetDepositFrequency(
  SY_GLOBAL_AGENCY_NBR: String);
var
  Q : IevQuery;
  s : String;

begin
  s :=  'Select SY_GLOBAL_AGENCY_NBR From SY_AGENCY_DEPOSIT_FREQ Where ' +
        '{AsOfDate<SY_AGENCY_DEPOSIT_FREQ>}' +
  {     '((Effective_Date <= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+
       ''') and (Effective_Until >= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+'''))' +   }
       ' and SY_GLOBAL_AGENCY_NBR = ' + SY_GLOBAL_AGENCY_NBR;
  //Q := TevQuery.Create(s);
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
    SY_GLOBAL_AGENCY_NBR := Q.Result.Fields[0].asString;
  //take the TCD_DEPOSIT_FREQUENCY_NBR and run a query
  //get the SY_GLOBAL_AGENCY_NBR FROM THAT QUERY ON SY_AGENCY_DEPOSIT_FREQ
  //APPLY FILTER BASED ON THAT VALUE
   if SY_GLOBAL_AGENCY_NBR <> '' then
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = ' + SY_GLOBAL_AGENCY_NBR
  else
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = -1';
  DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filtered := true;
  dsFieldData.DataSet.Edit;
  dsFieldData.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').asString := '-1';

end;

procedure TEvVersionedCOStates.dsFieldDataDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if dsFieldData.DataSet.State in [dsEdit, dsInsert] then exit;
  LoadCollectionDistricts;
  LocateDistrict;


end;

procedure TEvVersionedCOStates.LocateDistrict;
var
  Q : IevQuery;
  s : String;
  SY_GLOBAL_AGENCY_NBR: String;
  i : Integer;

begin
  //if dsFieldData.DataSet.State <> dsBrowse then exit;
  DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = -1';
  DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filtered := true;

  if dsFieldData.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').asString = '' then    
    exit;
  s :=  'Select SY_GLOBAL_AGENCY_NBR From SY_AGENCY_DEPOSIT_FREQ Where ' +
        '{AsOfDate<SY_AGENCY_DEPOSIT_FREQ>}' +
{       '((Effective_Date <= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+
       ''') and (Effective_Until >= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+'''))' +   }
  ' and SY_AGENCY_DEPOSIT_FREQ_NBR = ' + dsFieldData.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').asString;
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
    SY_GLOBAL_AGENCY_NBR := Q.Result.Fields[0].asString;


  for i := 0 to cbTaxCollectionDistrict.Items.Count - 1 do
  begin
    if TCDObject(cbTaxCollectionDistrict.Items.Objects[i]).SY_GLOBAL_AGENCY_NBR = SY_GLOBAL_AGENCY_NBR then
    begin
      cbTaxCollectionDistrict.ItemIndex := i;
      break;
    end;
  end;
 if SY_GLOBAL_AGENCY_NBR <> '' then
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = ' + SY_GLOBAL_AGENCY_NBR
  else
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = -1';
 DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filtered := True;
end;

procedure TEvVersionedCOStates.dsFieldDataStateChange(Sender: TObject);
begin
  inherited;
  grFieldData.Columns[2].DisplayWidth := 50;
end;

procedure TEvVersionedCOStates.LoadCollectionDistricts;
var
  Q : IevQuery;
  s : String;


begin
  ClearCollectionDistricts;
  s := 'select SY_GLOBAL_AGENCY_NBR, AGENCY_NAME from SY_GLOBAL_AGENCY WHERE ' +
       '{AsOfDate<SY_GLOBAL_AGENCY>} ' +
       {'((Effective_Date <= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+
       ''') and (Effective_Until >= '''+FormatDateTime('M/D/YYYY',deEndDate.Date)+'''))' +  }
       ' AND STATE = ''PA'' AND AGENCY_TYPE = ''Y'' order by AGENCY_NAME';
  Q := TevQuery.CreateAsOf(s,deBeginDate.Date);
  Q.Execute;

  if Q.Result.RecordCount > 0 then
  begin
    while not Q.Result.Eof do
    begin
     pTCDObject := TCDObject.Create;
     pTCDObject.SY_GLOBAL_AGENCY_NBR := Q.Result.Fields[0].asString;
     pTCDObject.AGENCY_NAME          := Q.Result.Fields[1].asString;
     cbTaxCollectionDistrict.Items.AddObject(Q.Result.Fields[1].asString,pTCDObject);
     Q.Result.Next;
    end;
  end;


  try
    grFieldData.Columns[2].DisplayWidth := 50;
  except
  end;
end;

procedure TEvVersionedCOStates.cbTaxCollectionDistrictChange(
  Sender: TObject);
begin
  inherited;
  ResetDepositFrequency(TCDObject(cbTaxCollectionDistrict.Items.Objects[cbTaxCollectionDistrict.ItemIndex]).SY_GLOBAL_AGENCY_NBR);
end;

end.
