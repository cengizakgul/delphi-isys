object ChoiceFromListQuery: TChoiceFromListQuery
  Left = 581
  Top = 216
  Width = 371
  Height = 377
  BorderIcons = []
  Caption = 'ChoiceFromListQuery'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 298
    Width = 355
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      355
      41)
    object btnYes: TevBitBtn
      Left = 194
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Yes'
      Default = True
      ModalResult = 6
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD88DDDDD
        DDDDDDDD88FDDDDDDDDDDDDD2288DDDDDDDDDDD8888FDDDDDDDDDDD2AA28DDDD
        DDDDDDD8888FDDDDDDDDDDD2AA28DDDDDDDDDDDD88DDDDDDDDDDDDDD228DDDDD
        DDDDDDDDDFFDDDDDDDDDDDDDD888DDDDDDDDDDDD888FDDDDDDDDDDDD22288DDD
        DDDDDDDD888FDDDDDDDDDDDD2A228DDDDDDDDDDD8888FDDDDDDDDDDD2AA288DD
        DDDDDDDD8888FDDDDDDDDDDD2AA228DDDDDDDDDD88888FDDDDDDDDDD2AAA288D
        DDDDDDDD88888FDDDDDDDDDD2AAA228DDDDDDDDD888888FDDDDDDDDD2AAAA288
        DDDDDDDD888888FDDDDDDDDD2AAAA228DDDDDDDD8888888FDDDDDDDD2AAAAA28
        DDDDDDDD8888888DDDDDDDDD2222222DDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object btnNo: TevBitBtn
      Left = 282
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&No'
      ModalResult = 7
      TabOrder = 1
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD888888D
        DDDDDDDDD88888FDDDDDDDD8899999188DDDDDD888888888FDDDDD8999999999
        18DDDD888FDDDD888FDDD89991DDDD99918DD8888FDDDDD888FDD999918DDDD9
        991DD88888FDDDDD88FD89999918DDDD991888FD888FDDDDD88F991D99918DDD
        D99188FDD888FDDDD88F991DD99918DDD99188FDDD888FDDD88F991DDD99918D
        D99188FDDDD888FDD88F991DDDD99918D99188FDDDDD888FD88F9918DDDD9991
        8991D88FDDDDD88888FDD9918DDDD999991DD888FDDDDD8888FDD99918DDDD99
        991DDD888FDDDD888FDDDD999188889991DDDDD888888888FDDDDDD999999999
        1DDDDDDDD88888FDDDDDDDDDD999991DDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  object dgChoiceList: TevDBGrid
    Left = 0
    Top = 0
    Width = 355
    Height = 298
    DisableThemesInTitle = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TChoiceFromListQuery\dgChoiceList'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsChoiceList
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object dsChoiceList: TevDataSource
    Left = 168
    Top = 48
  end
end
