// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_QuarterlyDateDialog;

interface


uses
  Windows, ComCtrls, Controls, Grids, StdCtrls, Buttons, Classes, Forms, Sysutils,
  Graphics, LMDCustomButton, LMDButton, isUILMDButton, ISBasicClasses,
  EvUIComponents, ExtCtrls, isUIFashionPanel, Dialogs;

const
  DaysPerMonth: array[1..12] of Integer =
    (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);	    { usual numbers of days }


type
  TQuarterlyDateDialog = class(TForm)
    fpDateSelection: TisUIFashionPanel;
    btnOK: TevButton;
    btnCancel: TevButton;
    qdsCalendar: TMonthCalendar;
    fpPresets: TisUIFashionPanel;
    pnlLeft: TevPanel;
    isUIFashionPanel1: TisUIFashionPanel;
    rbPreset: TevRadioButton;
    pnlRadioGroups: TevPanel;
    rgDay: TevRadioGroup;
    rgQuarter: TevRadioGroup;
    rgYear: TevRadioGroup;
    rbCustom: TevRadioButton;
    procedure FormShow(Sender: TObject);
    procedure rgQuarterClick(Sender: TObject);
    procedure rgDayClick(Sender: TObject);
    procedure rgYearClick(Sender: TObject);
    procedure rbPresetClick(Sender: TObject);
    procedure UpdateScreen;
    procedure qdsCalendarClick(Sender: TObject);
    procedure rbCustomClick(Sender: TObject);
  private
    { Private declarations }
    CurrYear,
    CurrMonth,
    CurrDay   : Word;
    Loading: Boolean;
    procedure LoadYears;
    procedure GetQuarter;
    procedure GetDay;
    procedure SetQuarter;
    procedure SetDay;
    procedure SetYear;
    function IsPresetDate(Value: TDate): Boolean;
  protected

  public
    { Public declarations }
    PreventCustomDates : Boolean;
  end;

implementation

{$R *.DFM}


procedure TQuarterlyDateDialog.LoadYears;
var
  i     : Integer;
begin
  DecodeDate(qdsCalendar.Date,CurrYear,CurrMonth,CurrDay);
  rgYear.Items.Clear;
  //load last, current and next years;
  for i := 0 to 2 do
    rgYear.Items.Add(IntToStr(CurrYear + i - 1));
  rgYear.ItemIndex := 1;
  GetQuarter;
  GetDay;
end;

procedure TQuarterlyDateDialog.FormShow(Sender: TObject);
begin
  DecodeDate(qdsCalendar.Date,CurrYear,CurrMonth,CurrDay);

  Loading := True;
  rbCustom.Enabled := not PreventCustomDates;
  qdsCalendar.Enabled := not PreventCustomDates;
  rgDay.Enabled := not PreventCustomDates; //force quarters intended to be quarter begin only.
  if PreventCustomDates then
    fpPresets.Title := 'Quarter - Required'
  else
    fpPresets.Title := 'Quarter';
  LoadYears;
  UpdateScreen;
  Loading := False;
end;

procedure TQuarterlyDateDialog.GetDay;
var
  DaysInMonth : Integer;
begin
  DaysInMonth := DaysPerMonth[CurrMonth];                        { normally, just return number }
  if (CurrMonth = 2) and IsLeapYear(CurrYear) then Inc(DaysInMonth); { plus 1 in leap February }

  //now set the item index
  if CurrDay = 1 then
    rgDay.ItemIndex :=0
  else if CurrDay = DaysInMonth then
    rgDay.ItemIndex := 1;

end;

procedure TQuarterlyDateDialog.GetQuarter;
begin
  Case CurrMonth of
  1..3 : rgQuarter.ItemIndex := 0;
  4..6 : rgQuarter.ItemIndex := 1;
  7..9 : rgQuarter.ItemIndex := 2;
  else rgQuarter.ItemIndex := 3;
  end;
end;

procedure TQuarterlyDateDialog.SetDay;
var
  EndDayThisMonth : Integer;
  CurrMonthBegin : Integer;
begin
   EndDayThisMonth := DaysPerMonth[CurrMonth];
   CurrMonthBegin := CurrMonth;
   if CurrDay = EndDayThisMonth then
   begin
     Case rgQuarter.ItemIndex of
     0 : CurrMonth := 3;
     1 : CurrMonth := 6;
     2 : CurrMonth := 9;
     3 : CurrMonth := 12;
     end;
     rgDay.ItemIndex := 1;
   end
   else if CurrDay = 1 then
   begin
     Case rgQuarter.ItemIndex of
     0 : CurrMonth := 1;
     1 : CurrMonth := 4;
     2 : CurrMonth := 7;
     3 : CurrMonth := 10;
     end;
     rgDay.ItemIndex := 0;
   end;
  //needs to be reset, cuz it may have changed
  if CurrMonth <> CurrMonthBegin then
    CurrDay := DaysPerMonth[CurrMonth];
  qdsCalendar.Date := EncodeDate(CurrYear,CurrMonth,CurrDay);
end;

procedure TQuarterlyDateDialog.SetQuarter;

begin
  SetDay;  
end;

procedure TQuarterlyDateDialog.rgQuarterClick(Sender: TObject);
begin
  if Loading then exit;
   case rgDay.ItemIndex of
     0: begin
       Case rgQuarter.ItemIndex of
       0 : CurrMonth := 1;
       1 : CurrMonth := 4;
       2 : CurrMonth := 7;
       3 : CurrMonth := 10;
       end;
       CurrDay := 1;
     end;
     1: begin
       Case rgQuarter.ItemIndex of
       0 : CurrMonth := 3;
       1 : CurrMonth := 6;
       2 : CurrMonth := 9;
       3 : CurrMonth := 12;
       end;
       CurrDay := DaysPerMonth[CurrMonth];
     end;
   end;
  SetQuarter;
end;

procedure TQuarterlyDateDialog.rgDayClick(Sender: TObject);
begin
  if Loading then exit;

  Case rgDay.ItemIndex of
    0: begin
       Case rgQuarter.ItemIndex of
       0 : CurrMonth := 1;
       1 : CurrMonth := 4;
       2 : CurrMonth := 7;
       3 : CurrMonth := 10;
       end;
       CurrDay := 1;
    end;
    1: begin
       Case rgQuarter.ItemIndex of
       0 : CurrMonth := 3;
       1 : CurrMonth := 6;
       2 : CurrMonth := 9;
       3 : CurrMonth := 12;
       end;
       CurrDay := DaysPerMonth[CurrMonth];
    end;
  end;
  SetDay;
end;

procedure TQuarterlyDateDialog.SetYear;
begin
  CurrYear := StrToInt(rgYear.Items.Strings[rgYear.ItemIndex]);
  qdsCalendar.Date := EncodeDate(CurrYear,CurrMonth,CurrDay);
end;

procedure TQuarterlyDateDialog.rgYearClick(Sender: TObject);
begin
  SetYear;
end;

procedure TQuarterlyDateDialog.rbPresetClick(Sender: TObject);
begin
 // DecodeDate(qdsCalendar.Date,CurrYear,CurrMonth,CurrDay);
  CurrYear := StrToInt(rgYear.Items.Strings[rgYear.ItemIndex]);
  fpPresets.Enabled := True;
  pnlRadioGroups.Font.Color := clWindowText;
  rgDay.OnClick(rgDay);
end;

function TQuarterlyDateDialog.IsPresetDate(Value: TDate): Boolean;
var
  isPreset : Boolean;
  DaysInMonth : Integer;
  i  : Integer;
begin
  //check for years outside the range of 3 presented in the dialog.
  try
    if ((CurrYear < StrToInt(rgYear.Items[0])) or (CurrYear > StrToInt(rgYear.Items[2]))) then
    begin
      Result := False;
      Exit;
    end
    else
    begin
      //pick the appropriate radio button based on the current year
      for i := 0 to 2 do
      begin
        if StrToInt(rgYear.Items[i]) = CurrYear then
        begin
          rgYear.ItemIndex := i;
          Break;
        end;
      end;
    end;
  except
  end;

  Case CurrMonth of
  2,5,8,11: isPreset := False;
  else
    isPreset := True;
  end;
  Result := isPreset;
  if not isPreset then exit;
  DaysInMonth := DaysPerMonth[CurrMonth];                        { normally, just return number }

  if CurrDay = 1 then
  begin
    Case CurrMonth of
      1,4,7,10: begin
        isPreset := True;
        rgDay.OnClick := nil;
        rgDay.ItemIndex := 0;
        rgDay.OnClick := rgDayClick;
      end;
    else
      isPreset := False;
    end;
  end
  else if CurrDay = DaysInMonth then
  begin
    Case CurrMonth of
      3,6,9,12: begin
        isPreset := True;
        rgDay.OnClick := nil;
        rgDay.ItemIndex := 1;
        rgDay.OnClick := rgDayClick;
      end;
    else
      isPreset := False;      
    end;

  end
  else
    isPreset := False;

  //now set proper quarter
  rgQuarter.OnClick := nil;
  Case CurrMonth of
    1..3 : rgQuarter.ItemIndex := 0;
    4..6 : rgQuarter.ItemIndex := 1;
    7..9 : rgQuarter.ItemIndex := 2;
    10..12 : rgQuarter.ItemIndex := 3;
  end;
  rgQuarter.OnClick := rgQuarterClick;
  Result := isPreset;
end;

procedure TQuarterlyDateDialog.UpdateScreen;
begin
 { if ((CurrYear < StrToInt(rgYear.Items[0])) or (CurrYear > StrToInt(rgYear.Items[2]))) then
  begin
    fpPresets.Enabled := False;
    pnlRadioGroups.Font.Color := clGray;
    rbCustom.OnClick := nil;
    rbCustom.Checked := True;
    rbCustom.OnClick := rbCustomClick;
    Exit;
  end;     }

  if PreventCustomDates then
  begin
    rbPreset.Checked := True;
    rbPreset.OnClick(nil);
  end
  else
  begin
    if IsPresetDate(qdsCalendar.Date) then
    begin
      fpPresets.Enabled := True;
      pnlRadioGroups.Font.Color := clWindowText;
      rbPreset.OnClick := nil;
      rbPreset.Checked := True;
      rbPreset.OnClick := rbPresetClick;
    end
    else
    begin
      fpPresets.Enabled := False;
      pnlRadioGroups.Font.Color := clGray;
      rbCustom.OnClick := nil;
      rbCustom.Checked := True;
      rbCustom.OnClick := rbCustomClick;
    end;
  end;
end;

procedure TQuarterlyDateDialog.qdsCalendarClick(Sender: TObject);
begin
  DecodeDate(qdsCalendar.Date,CurrYear,CurrMonth, CurrDay);
  //GetQuarter;
  UpdateScreen;
end;

procedure TQuarterlyDateDialog.rbCustomClick(Sender: TObject);
begin
  qdsCalendar.Date := EncodeDate(CurrYear,CurrMonth,CurrDay);
  fpPresets.Enabled := False;
  pnlRadioGroups.Font.Color := clGray;
  SetDay;
end;

end.
