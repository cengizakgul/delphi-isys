// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BASE;

interface

uses
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, SDataStructure, Db,
  Wwdatsrc, SysUtils, Windows, SPackageEntry, EvUtils, Variants,
  SPD_EDIT_Open_BASE, Mask, wwdbedit, Wwdotdot, Wwdbcomb, EvSecElement,
  SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  Forms, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_BASE = class(TEDIT_OPEN_BASE)
    Label5: TEvLabel;
    DBText1: TEvDBText;
    CompanyNameText: TEvDBText;
    lablClient: TEvLabel;
    dbtxClientNbr: TEvDBText;
    dbtxClientName: TEvDBText;
    dsCL: TevDataSource;
    DM_CLIENT: TDM_CLIENT;
    DM_COMPANY: TDM_COMPANY;
    pnlUser: TevPanel;
    lablUserFirst: TevLabel;
    dbtxUserFirst: TevDBText;
    dbtxUserLast: TevDBText;
    lablUserLast: TevLabel;
    lblAdditionalTools: TevLabel;
    pnlTopLeft: TevPanel;
    sbEDIT_CO_BASE_Inner: TScrollBox;
    pnlsbEDIT_CO_BASE_Inner_Border: TevPanel;
    pnlEDIT_CO_BASE_LEFT: TevPanel;
    splEDIT_CO_BASE: TevSplitter;
    pnlEDIT_CO_BASE_RIGHT: TevPanel;
    fpEDIT_CO_BASE_Company: TisUIFashionPanel;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_COMPANY.CO;
  if not Assigned(dsCL.DataSet) then
    dsCL.DataSet := DM_CLIENT.CL;
end;

procedure TEDIT_CO_BASE.Activate;
begin
  inherited;
  TevClientDataSet(wwdsMaster.DataSet).IndexFieldNames := 'CO_NBR';
  wwdsMaster.DataSet.First;
  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(wwdsMaster.DataSet).ClientID, wwdsMaster.DataSet['CO_NBR']]), []);
  if not BitBtn1.Enabled then
    BitBtn1Click(Self);

end;

procedure TEDIT_CO_BASE.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  inherited;
end;

function TEDIT_CO_BASE.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
  if (sName <> 'CL') and
     (Copy(sName, 1, 3) <> 'CL_') and
     wwdsList.DataSet.Active then
    Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
end;

end.
