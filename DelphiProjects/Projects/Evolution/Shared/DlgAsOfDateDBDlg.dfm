inherited dlAsOfDateDBDlg: TdlAsOfDateDBDlg
  Left = 353
  Top = 266
  ClientHeight = 200
  ClientWidth = 267
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel [0]
    Left = 146
    Top = 47
    Width = 27
    Height = 13
    Caption = 'Value'
  end
  inherited OKBtn: TButton
    TabOrder = 3
  end
  inherited CancelBtn: TButton
    TabOrder = 4
  end
  object evDBComboDlg1: TevDBComboDlg
    Left = 146
    Top = 64
    Width = 111
    Height = 21
    OnCustomDlg = OnCustomDlg
    ShowButton = True
    Style = csDropDownList
    Picture.PictureMaskFromDataSet = False
    TabOrder = 2
    WordWrap = False
    UnboundDataType = wwDefault
  end
end
