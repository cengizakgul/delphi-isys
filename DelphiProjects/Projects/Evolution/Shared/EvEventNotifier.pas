unit EvEventNotifier;

interface

uses Classes, isBaseClasses, isLogFile, SysUtils, isThreadManager, EvContext,
     EvMainboard, ISBasicUtils, DateUtils, EvConsts, evExceptions;

type
  IEvEventNotifier = interface
  ['{20955953-5694-4936-ACC7-820A78ED504B}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
  end;

  TEvEventNotifier = class(TisInterfacedObject, IEvEventNotifier, ILogFileCallback)
  private
    FActive: boolean;
    FRulesList: IInterfaceList;
    FEventNotifierWatchDogId: TTaskId;
  protected
    procedure DoOnConstruction; override;
    procedure EventNotifierWatchDog(const Params: TTaskParamList);
    function  AnalizeAndFindRule(ALogEvent: ILogEvent): IisListOfValues;
    procedure ProcessRule(ARule: IisListofValues; ALogEvent: ILogEvent);
    procedure InitializeRules;
    procedure LoadRulesSettings;
    procedure SendNotification(ARule: IisListofValues);
    procedure AddEvent(ALogEvent: ILogEvent);
    procedure ResetCounters;

    // IEvEventNotifier
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);

    // ILogFileCallback
    procedure NewEventLogged(const ALogEvent: ILogEvent);
  public
    destructor Destroy; override;
  end;

implementation

const
  DiagnosticExpiredThreshold = 1;
  DiagnosticExpiredDelay = 15;
  CannotConnectToRPThreshold = 10;
  CannotConnectToRPDelay = 30;

  ConfigPrefix = 'eMail\AlertRules\';

  // consts for rule's variables
  RuleName = 'RuleName';
  RuleType = 'RuleType';
  RuleEventSeverity = 'EventSeverity';
  RuleEventClass = 'EventClass';
  RuleEventMessagePattern = 'EventMessagePattern';
  RuleTypeThreshold = 'RuleTypeThreshold';
  InitialThreshold = 'InitialThreshold';
  TimeDelay = 'TimeDelay';
  EventsCounted = 'EventsCounted';
  MessageSentFlag = 'MessageSentFlag';
  LastMessageSentTime = 'LastMessageSentTime';
  MessageToBeSent = 'MessageToBeSent';
  DiagnosticExpired = 'DiagnosticExpired';
  CannotConnectToRP = 'CannotConnectToRP';
  LastErrorText = 'LastErrorText';

{ TEvEventNotifier }

procedure TEvEventNotifier.AddEvent(ALogEvent: ILogEvent);
var
  Rule : IisListOfValues;
begin
  if FActive then
  begin
    Rule := AnalizeAndFindRule(ALogEvent);
    if Assigned(Rule) then
      ProcessRule(Rule, ALogEvent);
  end;
end;

function TEvEventNotifier.AnalizeAndFindRule(ALogEvent: ILogEvent): IisListOfValues;
var
  i : integer;
  Rule : IisListOfValues;
begin
  Lock;
  try
    Result := nil;
    for i := 0 to FRulesList.Count - 1 do
    begin
      Rule := FRulesList[i] as IisListOfValues;
      if (Rule.Value[RuleEventSeverity] <= ALogEvent.EventType) and
         (Rule.Value[RuleEventClass] = ALogEvent.EventClass) and
         ((Rule.Value[RuleEventMessagePattern] = '') or
          StartsWith(ALogEvent.Text, Rule.Value[RuleEventMessagePattern])) then
      begin
        Result := Rule;
        exit;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TEvEventNotifier.ResetCounters;
var
  Rule : IisListOfValues;
  i : integer;
begin
  Lock;
  try
    for i := 0 to FRulesList.Count - 1 do
    begin
      Rule := FRulesList[i] as IisListOfValues;
      Rule.Value[EventsCounted] := 0;
      Rule.Value[LastMessageSentTime] := now;
      Rule.Value[MessageSentFlag] := false;
    end;
  finally
    Unlock;
  end;
end;

destructor TEvEventNotifier.Destroy;
begin
  if FEventNotifierWatchDogId <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FEventNotifierWatchDogId);
    GlobalThreadManager.WaitForTaskEnd(FEventNotifierWatchDogId);
  end;
  
  inherited;
end;

function TEvEventNotifier.GetActive: Boolean;
begin
  result := FActive;
end;

procedure TEvEventNotifier.SetActive(const AValue: Boolean);
begin
  FActive := AValue;
end;

procedure TEvEventNotifier.InitializeRules;
var
  Rule : IisListOfValues;
begin
  Lock;
  try
    FRulesList.Clear;

    { todo : change hardcoded logic for something more flexible }

    // Rule #0 - Diagnostic Event
    Rule := TisListOfValues.Create;
    Rule.AddValue(RuleName, DiagnosticExpired);
    Rule.AddValue(RuleType, RuleTypeThreshold);
    Rule.AddValue(RuleEventSeverity, etError);
    Rule.AddValue(RuleEventClass, LOG_EVENT_CLASS_DIAGNOSTIC);
    Rule.AddValue(RuleEventMessagePattern, '');
    Rule.AddValue(InitialThreshold, DiagnosticExpiredThreshold);
    Rule.AddValue(TimeDelay, DiagnosticExpiredDelay); // minutes
    Rule.AddValue(EventsCounted, 0);
    Rule.AddValue(MessageSentFlag, false);
    Rule.AddValue(LastMessageSentTime, now);
    Rule.AddValue(MessageToBeSent, 'License problem');
    FRulesList.Add(Rule);

    // Rule #1 - CannotConnectToRP
    Rule := TisListOfValues.Create;
    Rule.AddValue(RuleName, CannotConnectToRP);
    Rule.AddValue(RuleType, RuleTypeThreshold);
    Rule.AddValue(RuleEventSeverity, etError);
    Rule.AddValue(RuleEventClass, LOG_EVENT_CLASS_COMMUNICATION);
    Rule.AddValue(RuleEventMessagePattern, 'Cannot connect to RP');
    Rule.AddValue(InitialThreshold, CannotConnectToRPThreshold);
    Rule.AddValue(TimeDelay, CannotConnectToRPDelay); // minutes
    Rule.AddValue(EventsCounted, 0);
    Rule.AddValue(MessageSentFlag, false);
    Rule.AddValue(LastMessageSentTime, now);
    Rule.AddValue(MessageToBeSent, 'RB cannot connect to RP');
    FRulesList.Add(Rule);
  finally
    Unlock;
  end;
end;

procedure TEvEventNotifier.LoadRulesSettings;
var
  i: integer;
  Rule: IisListOfValues;
  v: Variant;
begin
  Lock;
  try
    for i := 0 to FRulesList.Count - 1 do
    begin
      Rule := FRulesList[i] as IisListOfValues;
      v := mb_AppConfiguration.GetValue(ConfigPrefix + Rule.Value[RuleName] + '\' + InitialThreshold, 0);
      if v <> 0 then
        Rule.Value[InitialThreshold] := v;

      v := mb_AppConfiguration.GetValue(ConfigPrefix + Rule.Value[RuleName] + '\' + TimeDelay, 0);
      if v <> 0 then
        Rule.Value[TimeDelay] := v;

      v := mb_AppConfiguration.GetValue(ConfigPrefix + Rule.Value[RuleName] + '\' + MessageToBeSent, '');
      if v <> '' then
        Rule.Value[MessageToBeSent] := v;
    end;
  finally
    Unlock;
  end;
end;

procedure TEvEventNotifier.ProcessRule(ARule: IisListofValues; ALogEvent : ILogEvent);
  procedure DoIt;
  begin
    ARule.Value[LastMessageSentTime] := now;
    SendNotification(ARule);
    ARule.Value[EventsCounted] := 0;
    ARule.Value[MessageSentFlag] := true;
    if ARule.ValueExists(LastErrorText) then
      ARule.DeleteValue(LastErrorText);
  end;
begin
  if Assigned(ALogEvent) then
  begin
    ARule.Value[EventsCounted] := ARule.Value[EventsCounted] + 1;
    ARule.AddValue(LastErrorText, ALogEvent.Text);
  end;
  // RuleType is RuleTypeThreshold
  if ARule.Value[RuleType] = RuleTypeThreshold then
  begin
    if ARule.Value[EventsCounted] >= ARule.Value[InitialThreshold] then
      if not ARule.Value[MessageSentFlag] then
        DoIt
      else
        if SecondsBetween(ARule.Value[LastMessageSentTime], now) >= (ARule.Value[TimeDelay] * 60) then
          DoIt;
  end
  else
    raise EevException.Create('Unsupported RuleType');
end;

procedure TEvEventNotifier.EventNotifierWatchDog(const Params: TTaskParamList);
const
  MaxErrorCnt = 10;
var
  i : integer;
  iErrorCnt : integer;
  bFlag : boolean;

  procedure DoCheck;
  var
    Rule : IisListOfValues;
  begin
    Rule := FRulesList[i] as IisListOfValues;
    ProcessRule(Rule, nil);
  end;
begin
  iErrorCnt := 0;
  bFlag := false;
  repeat
    if FActive then
      try
        Lock;
        try
          for i := 0 to FRulesList.Count - 1 do
            DoCheck;
        finally
          Unlock;
        end;
      except
        on E:Exception do
        begin
          Inc(iErrorCnt);
          if iErrorCnt <= MaxErrorCnt then
            mb_LogFile.AddEvent(etError, 'EvEventNotifier', 'Error in EvEventNotifierWatchDog.', 'Error in EvEventNotifierWatchDog. ' + E.Message)
          else
            if not bFlag then
            begin
              bFlag := true;
              mb_LogFile.AddEvent(etInformation, 'EvEventNotifier', 'Too many errors in EvEventNotifierWatchDog.', 'Logging of errors in EvEventNotifierWatchDog is stopped. Reason: too many errors');
            end;
        end;
      end;  // try except
    Snooze(60000); // one time per minute
  until CurrentThreadTaskTerminated;
end;

procedure TEvEventNotifier.SendNotification(ARule: IisListofValues);
var
  sSubject, sFrom, sTo, sMessage : String;
begin
  sFrom := 'Evolution@' + mb_GlobalSettings.EmailInfo.NotificationDomainName;
  sTo := Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'];
  sSubject := 'Alert from ' + ExtractFileName(AppFileName) + ' on ' + Mainboard.MachineInfo.Name;
  sMessage := ARule.Value[MessageToBeSent];
  if ARule.ValueExists(LastErrorText) then
    sMessage := sMessage + #13#10 + 'Last error:' + #13#10 + ARule.Value[LastErrorText];

  if (sTo <> '') and (sFrom <> '') then
    try
      Mainboard.ContextManager.StoreThreadContext;
      try
        Mainboard.ContextManager.CreateThreadContext(sGuestUserName, '');
        ctx_EMailer.SendQuickMail(sTo, sFrom, sSubject, sMessage);
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;
    except
      on E:Exception do
        mb_LogFile.AddEvent(etError, 'Email Notifier', 'Email sending problem', E.Message);
    end;
end;

procedure TEvEventNotifier.NewEventLogged(const ALogEvent: ILogEvent);
begin
  AddEvent(ALogEvent);
end;

procedure TEvEventNotifier.DoOnConstruction;
begin
  inherited;
  InitLock;
  FRulesList := TInterfaceList.Create;
  InitializeRules;
  LoadRulesSettings;

// watchdog is commented because we decided not to send async notifications
//  FEventNotifierWatchDogId := GlobalThreadManager.RunTask(EventNotifierWatchDog, Self, MakeTaskParams([]), 'EvEventNotifier watchdog');
end;

end.
