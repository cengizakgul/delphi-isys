// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RegistryFunctions;

interface

uses
  Registry, Windows, SysUtils, Forms, Classes, IniFiles;

function ReadKeyFieldNames(const Key: string; KeyPath: String = ''): TStringList;
function ReadFromRegistry(const Key, KeyField: string; DefaultValue: string; KeyPath: String = ''): string;
function ReadSectionFromRegistry(const Key: string; var List: TStringList; KeyPath: String = ''): string;
procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; KeyPath: String = '');
function CheckKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = ''): boolean;
procedure CleanKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = '');
procedure CleanKeyInRegistry(const Key: string; KeyPath: String = '');

var
  EVOLUTION_REGISTRY: string;
const
  sRootKey = HKEY_CURRENT_USER;

implementation

function ReadFromRegistry(const Key, KeyField: string; DefaultValue: string; KeyPath: String = ''): string;
var
  TempRegistry: TRegistry;
begin
  Result := DefaultValue;

  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) and
         ValueExists(KeyField) then
        Result := ReadString(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function ReadKeyFieldNames(const Key: string; KeyPath: String = ''): TStringList;
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  Result := TStringList.Create;
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;
      if OpenKey(KeyPath + Key, False) then
        GetValueNames(Result);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function ReadSectionFromRegistry(const Key: string; var List: TStringList; KeyPath: String = ''): string;
var
  TempRegistry: TRegIniFile;
begin
  Result := '';

  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegIniFile.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        ReadSection('', List);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; KeyPath: String = '');
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;
      if OpenKey(KeyPath + Key, NewValue <> '') then
        if NewValue = '' then
          DeleteValue(KeyField)
        else
          WriteString(KeyField, NewValue);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure CleanKeyInRegistry(const Key: string; KeyPath: String = ''); 
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath, False) then
        DeleteKey(Key);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure CleanKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = '');
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        if ValueExists(KeyField) then
          DeleteValue(KeyField);
    end
  finally
    TempRegistry.Free;
  end;
end;

function CheckKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = ''): boolean;
var
  TempRegistry: TRegistry;
begin
  Result := False;

  if KeyPath = '' then
    KeyPath := EVOLUTION_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        Result := ValueExists(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function GetFileName(FileName: string): string;
begin
  Result := ExtractFileName(FileName);
  if Pos('.', Result) > 0 then
    Result := Copy(Result, 1, Pred(Pos('.', Result)));
end;

initialization
  EVOLUTION_REGISTRY := 'SOFTWARE\Evolution\' + GetFileName(Application.ExeName) + '\';

end.
