// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_SEC_TEMPLATES;

interface

uses
   SFrameEntry, wwdblook, StdCtrls, wwdbedit, Wwdotdot,
  Wwdbcomb, Mask, DBCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, Classes,
  Db, Wwdatsrc, SDataStructure, SFieldCodeValues, Buttons, EvTypes, EvStreamUtils,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvContext, EvCommonInterfaces,
  EvExceptions, EvUIComponents, EvClientDataSet, LMDCustomButton,
  LMDButton, isUILMDButton, isUIwwDBEdit;

type
  TEDIT_SY_SEC_TEMPLATES = class(TFrameEntry)
    lablReport_Description: TevLabel;
    wwDBGrid1: TevDBGrid;
    dedtTemplate_Description: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    bEdit: TevBitBtn;
    procedure wwdsMasterStateChange(Sender: TObject);
    procedure bEditClick(Sender: TObject);
    procedure wwdsMasterUpdateData(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetBlobField: TBlobField; virtual;
  public
    function GetInsertControl: TWinControl; override;
  private
  end;

implementation

uses SPD_EDIT_SECURITY, EvUtils,
  sSecurityClassDefs, SysUtils, SSecurityInterface;

{$R *.DFM}

function TEDIT_SY_SEC_TEMPLATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_SEC_TEMPLATES;
end;

function TEDIT_SY_SEC_TEMPLATES.GetInsertControl: TWinControl;
begin
  Result := dedtTemplate_Description;
end;

procedure TEDIT_SY_SEC_TEMPLATES .wwdsMasterStateChange(Sender: TObject);
begin
  inherited;
  bEdit.Enabled := (wwdsMaster.State = dsInsert)
  or ((wwdsMaster.State = dsBrowse) and (wwdsMaster.DataSet.RecordCount > 0)); // and not GetIfReadOnly;
end;

procedure TEDIT_SY_SEC_TEMPLATES.bEditClick(Sender: TObject);
var
  o: TEDIT_SECURITY;
  m: IEvDualStream;
  Sec: IevSecurityStructure;
  aBlobField: TBlobField;
begin
  inherited;
  o := TEDIT_SECURITY.Create(nil);
  try
    o.Caption := 'Security rights Template (' + wwdsDetail.DataSet.FieldByName('NAME').AsString + ')';
    m := TevDualStreamHolder.CreateInMemory;
    Sec := ctx_Security.GetUserSecStructure(Context.UserAccount.InternalNbr);
    Sec.Obj.StreamStructureTo(m);
    Sec := nil;

    m.Position := 0;
    o.Collection := TSecurityAtomCollection.CreateFromStream(m);
    try
      m.Clear;
      aBlobField := GetBlobField;
      if not aBlobField.IsNull then
      begin
        m := TevClientDataSet(aBlobField.DataSet).GetBlobData(aBlobField.FieldName);
        o.Collection.LoadContextsFromStream(m);
      end;
      o.btnSave.Enabled := not GetIfReadOnly;
      if o.ShowModal = mrOk then
      begin
        if wwdsMaster.State = dsBrowse then
          wwdsMaster.Edit;
        m.Clear;
        o.Collection.SaveContextsToStream(m);
        TevClientDataSet(wwdsMaster.DataSet).UpdateBlobData(aBlobField.FieldName, m);
      end;
    finally
      o.Collection.Free;
    end;
  finally
    o.Free;
  end;
end;

function TEDIT_SY_SEC_TEMPLATES.GetBlobField: TBlobField;
begin
  Result := wwdsDetail.DataSet.FindField('DATA') as TBlobField;
end;

procedure TEDIT_SY_SEC_TEMPLATES.wwdsMasterUpdateData(Sender: TObject);
begin
  inherited;
  if GetBlobField.IsNull then
    raise EUpdateError.CreateHelp('You have to set rights before you save changes', IDH_ConsistencyViolation);
end;

initialization
  RegisterClass(TEDIT_SY_SEC_TEMPLATES);

end.
