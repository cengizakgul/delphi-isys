unit EvLegacy;

interface

uses SysUtils, EvConsts, EvStreamUtils, EvTypes, SReportSettings,
     EvCommonInterfaces, DB, EvMainboard, isSchedule, isTypes, isBasicUtils, EvClientDataSet;

type
  IevTaskExt = interface
  ['{35FB8C6A-01C3-4DE4-B26A-CFC59E5D8998}']
    procedure SetAccountInfo(const ADomain, AUser: String);
  end;



function TaskTypeToMethodName(const ATaskType: String): String;
function MethodNameToTaskType(const AMethodName: String): String;
function TaskFromBlobField(const ATaskType: String; const AField: TBlobField): IevTask;
function TaskFromStream(const ATaskType: String; const AStream: IisStream): IevTask;
function OldFormatToScheduleEvent(const AEventData: String): IisScheduleEvent;

implementation

function TaskTypeToMethodName(const ATaskType: String): String;
begin
  if AnsiSameText(ATaskType, QUEUE_TASK_RUN_REPORT) then
    Result := 'TRunReportQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_PAYROLL) then
    Result := 'TProcessPayrollsQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_CREATE_PAYROLL) then
    Result := 'TCreatePayrollsQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_TAX_PAYMENTS) then
    Result := 'TPayTaxesQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_QUARTER_END) then
    Result := 'TPreProcessForQuarterTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_PAYROLL) then
    Result := 'TPreprocessPayrollsQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_TAX_RETURNS) then
    Result := 'TProcessQarterReturnsTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_PAYROLL_ACH) then
    Result := 'TProcessSBACHQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_QEC) then
    Result := 'TQuarterEndCleanupQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_REBUILD_TMP_TABLES) then
    Result := 'TRebuildTempTablesTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_REBUILD_VMR_HIST) then
    Result := 'TReprintPayrollToVmrQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_REPRINT_PAYROLL) then
    Result := 'TReprintPayrollQueueTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS) then
    Result := 'TEvBenefitEnrollmentNotificationsTask'
  else if AnsiSameText(ATaskType, QUEUE_TASK_ACA_STATUS_UPDATE) then
    Result := 'TEvACAUpdateTask'
  else
    Result := ATaskType;
end;


function MethodNameToTaskType(const AMethodName: String): String;
begin
  if AnsiSameText(AMethodName, 'TRunReportQueueTask') then
    Result := QUEUE_TASK_RUN_REPORT
  else if AnsiSameText(AMethodName, 'TProcessPayrollsQueueTask') then
    Result := QUEUE_TASK_PROCESS_PAYROLL
  else if AnsiSameText(AMethodName, 'TCreatePayrollsQueueTask') then
    Result := QUEUE_TASK_CREATE_PAYROLL
  else if AnsiSameText(AMethodName, 'TPayTaxesQueueTask') then
    Result := QUEUE_TASK_PROCESS_TAX_PAYMENTS
  else if AnsiSameText(AMethodName, 'TPreProcessForQuarterTask') then
    Result := QUEUE_TASK_PREPROCESS_QUARTER_END
  else if AnsiSameText(AMethodName, 'TPreprocessPayrollsQueueTask') then
    Result := QUEUE_TASK_PREPROCESS_PAYROLL
  else if AnsiSameText(AMethodName, 'TProcessQarterReturnsTask') then
    Result := QUEUE_TASK_PROCESS_TAX_RETURNS
  else if AnsiSameText(AMethodName, 'TProcessSBACHQueueTask') then
    Result := QUEUE_TASK_PAYROLL_ACH
  else if AnsiSameText(AMethodName, 'TQuarterEndCleanupQueueTask') then
    Result := QUEUE_TASK_QEC
  else if AnsiSameText(AMethodName, 'TRebuildTempTablesTask') then
    Result := QUEUE_TASK_REBUILD_TMP_TABLES
  else if AnsiSameText(AMethodName, 'TReprintPayrollToVmrQueueTask') then
    Result := QUEUE_TASK_REBUILD_VMR_HIST
  else if AnsiSameText(AMethodName, 'TReprintPayrollQueueTask') then
    Result := QUEUE_TASK_REPRINT_PAYROLL
  else if AnsiSameText(AMethodName, 'TEvBenefitEnrollmentNotificationsTask') then
    Result := QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS
  else if AnsiSameText(AMethodName, 'TEvACAUpdateTask') then
    Result := QUEUE_TASK_ACA_STATUS_UPDATE
  else
    Result := AMethodName;
end;


function TaskFromStream(const ATaskType: String; const AStream: IisStream): IevTask;
var
  sID: String;
begin
  Result := mb_TaskQueue.CreateTask(ATaskType, False);
  sID := Result.ID;
  Result.AsStream := AStream;
  Result.ID := sID;
end;

function TaskFromBlobField(const ATaskType: String; const AField: TBlobField): IevTask;
var
  S: IisStream;
begin
  if not AField.IsNull then
  begin
    if AField.DataSet is TevClientDataSet then
      S := TevClientDataSet(AField.DataSet).GetBlobData(AField.FieldName)
    else
    begin
      S := TisStream.Create;
      AField.SaveToStream(S.RealStream);
    end;
    Result := TaskFromStream(ATaskType, S);
  end
  else
    Result := nil;
end;

function OldFormatToScheduleEvent(const AEventData: String): IisScheduleEvent;
var
  s, t, d: String;
  b, hr, min: Word;
  Event: IisScheduleEvent;
  m: TMonth;
  dw: TDayOfWeek;

  procedure ReadEventInfo(const AReadEndDate: Boolean);
  begin
    Event.StartTime := StrToFloat(GetNextStrValue(s, ' '));
    Event.StartDate := StrToInt(GetNextStrValue(s, ' '));

    if GetNextStrValue(s, ' ') = 'Y' then // repeat
    begin
      Event.RepeatIntervalNbr := StrToInt(GetNextStrValue(s, ' '));
      Event.RepeatInterval := TRepeatInterval(StrToInt(GetNextStrValue(s, ' ')) + 1);

      if GetNextStrValue(s, ' ') = 'D' then
      begin
        Event.RepeatUntil := ruDuration;
        hr := StrToInt(GetNextStrValue(s, ' '));
        min := StrToInt(GetNextStrValue(s, ' '));
        Event.RepeatUntilTime := EncodeTime(hr, min, 0, 0);
      end
      else
      begin
        Event.RepeatUntil := ruTime;
        Event.RepeatUntilTime := StrToFloat(GetNextStrValue(s, ' '));
      end;
    end;

    Event.RunIfMissed := GetNextStrValue(s, ' ') = 'Y';

    if AReadEndDate then
      if GetNextStrValue(s, ' ') = 'Y' then
        Event.EndDate := StrToInt(GetNextStrValue(s, ' '));
  end;

begin
  s:= AEventData;
  t := GetNextStrValue(s, ' ');

  // daily
  if t = 'D' then
  begin
    Event := TisScheduleEventDaily.Create;
    ReadEventInfo(True);
    (Event as IisScheduleEventDaily).EveryNbrDay := StrToInt(GetNextStrValue(s, ' '));
  end

  // weekly
  else if t = 'W' then
  begin
    Event := TisScheduleEventWeekly.Create;
    ReadEventInfo(True);
    (Event as IisScheduleEventWeekly).EveryNbrWeek := StrToInt(GetNextStrValue(s, ' '));

    b := StrToInt(GetNextStrValue(s, ' '));
    for dw := dwMonday to dwSunday do
      if (b and (1 shl (Ord(dw) - 1))) <> 0 then
        (Event as IisScheduleEventWeekly).RunDays := (Event as IisScheduleEventWeekly).RunDays + [dw];
  end

  // monthly
  else if t = 'M' then
  begin
    Event := TisScheduleEventMonthly.Create;
    ReadEventInfo(True);

    if GetNextStrValue(s, ' ') = 'D' then
    begin
      (Event as IisScheduleEventMonthly).WeekOfMonth := wmUnknown;
      d := GetNextStrValue(s, ' ');
      if d = 'L' then
        (Event as IisScheduleEventMonthly).DayOfMonth := dnLastDayOfMonth
      else
        (Event as IisScheduleEventMonthly).DayOfMonth := StrToInt(d);
    end
    else
    begin
      (Event as IisScheduleEventMonthly).WeekOfMonth := TWeekOfMonth(StrToInt(GetNextStrValue(s, ' ')) + 1);
      (Event as IisScheduleEventMonthly).DayOfWeek := TDayOfWeek(StrToInt(GetNextStrValue(s, ' ')) + 1);
    end;

    b := StrToInt(GetNextStrValue(s, ' '));
    for m := mJanuary to mDecember do
      if b and (1 shl (Ord(m) - 1)) <> 0 then
        (Event as IisScheduleEventMonthly).Months := (Event as IisScheduleEventMonthly).Months + [m];
  end

  // Once
  else if t = 'O' then
  begin
    Event := TisScheduleEventOnce.Create;
    ReadEventInfo(False);
  end

  // Startup
  else if t = 'S' then
    Event := TisScheduleEventStartup.Create;

  Result := Event;
end;


end.


