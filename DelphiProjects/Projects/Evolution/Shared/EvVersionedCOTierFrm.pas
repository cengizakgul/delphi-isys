unit EvVersionedCOTierFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem, SDataDictclient, EvExceptions;

type
  TevVersionedCOTier = class(TevVersionedFieldBase)
    lValue: TevLabel;
    cbClCoCons: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
   protected
    procedure DoOnSave; override;    
  end;

implementation

{$R *.dfm}


{ TevVersionedCOTier }

procedure TevVersionedCOTier.DoOnSave;
var
  Q, Q1: IEvQuery;
  s: string;
  tmpDASHBOARD_COUNT, tmpUSERS_COUNT: integer;
  procedure GetCompanyValues;
  var
     Q2: IEvQuery;
     s: string;
  begin
    tmpDASHBOARD_COUNT:= DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger;
    tmpUSERS_COUNT:= DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger;

    s:= 'select * from SY_ANALYTICS_TIER where {AsOfNow<SY_ANALYTICS_TIER>} and  SY_ANALYTICS_TIER_NBR = ' +
                  IntToStr(Data['SY_ANALYTICS_TIER_NBR']);
    Q2 := TevQuery.Create(s);
    Q2.Execute;
    if Q2.Result.RecordCount > 0 then
    Begin
      if (tmpDASHBOARD_COUNT = 0) then
          tmpDASHBOARD_COUNT:= Q2.Result.FieldByName('DASHBOARD_MAX_COUNT').AsInteger;
      if (tmpUSERS_COUNT = 0) then
          tmpUSERS_COUNT:= Q2.Result.FieldByName('USERS_MAX_COUNT').AsInteger;
    end;
  end;

begin
  inherited;
   GetCompanyValues;

    s:= 'select * from CO_DASHBOARDS where {AsOfNow<CO_DASHBOARDS>} and  CO_NBR = ' +
                  IntToStr(DM_CLIENT.CO.FieldByName('CO_NBR').AsInteger);
    Q1 := TevQuery.Create(s);
    Q1.Execute;

    if Q1.Result.RecordCount > 0 then
      if (Q1.Result.RecordCount > tmpDASHBOARD_COUNT) or
          (DashboardUserCount > tmpUSERS_COUNT) then
            raise EUpdateError.CreateHelp('You have exceeded the Maximum Number allowed for this Tier.  Please adjust the number.', IDH_ConsistencyViolation);

    s:= 'select * from SY_ANALYTICS_TIER where {AsOfDate<SY_ANALYTICS_TIER>} and  SY_ANALYTICS_TIER_NBR = ' +
                  IntToStr(DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger);
    Q := TevQuery.Create(s);
    Q.Params.AddValue('StatusDate', deBeginDate.DateTime);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
      if (DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger > Q.Result.FieldByName('DASHBOARD_MAX_COUNT').AsInteger) or
          (DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger > Q.Result.FieldByName('USERS_MAX_COUNT').AsInteger) or
          (DM_CLIENT.CO.FieldByName('LOOKBACK_YEARS').AsInteger > Q.Result.FieldByName('LOOKBACK_YEARS_MAX').AsInteger) then
            raise EUpdateError.CreateHelp('You have exceeded the Maximum Number allowed for this Tier.  Please adjust the number.', IDH_ConsistencyViolation);

end;

end.

