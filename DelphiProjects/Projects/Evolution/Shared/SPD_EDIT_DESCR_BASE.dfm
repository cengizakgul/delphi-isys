inherited EDIT_DESCR_BASE: TEDIT_DESCR_BASE
  inherited pnlBrowse: TevPanel
    Height = 193
    inherited dgBrowse: TevDBGrid
      Height = 193
      Selected.Strings = (
        'DESCRIPTION'#9'40'#9'Description'#9'F')
      IniAttributes.SectionName = 'TEDIT_DESCR_BASE\dgBrowse'
    end
  end
  inherited pnlDBControls: TevPanel
    Top = 193
    Height = 73
    object evLabel1: TevLabel
      Left = 8
      Top = 13
      Width = 73
      Height = 13
      Caption = '~Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dedtDescription: TevDBEdit
      Left = 80
      Top = 8
      Width = 321
      Height = 21
      DataField = 'DESCRIPTION'
      DataSource = wwdsDetail
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
end
