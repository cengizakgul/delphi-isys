inherited EvVersionedEERate: TEvVersionedEERate
  Left = 875
  Top = 403
  ClientHeight = 468
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 367
    inherited grFieldData: TevDBGrid
      Height = 240
      Constraints.MinHeight = 100
    end
    inherited pnlEdit: TevPanel
      Top = 284
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 367
    Width = 791
    Height = 101
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 101
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Pay Rate'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lNumber: TevLabel
        Left = 118
        Top = 35
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Rate Number'
        FocusControl = edNumber
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lAmount: TevLabel
        Left = 209
        Top = 35
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Rate Amount'
        FocusControl = edAmount
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object rgPrimary: TevDBRadioGroup
        Left = 20
        Top = 35
        Width = 91
        Height = 37
        HelpContext = 19002
        Caption = 'Primary Rate'
        Columns = 2
        DataSource = dsFieldData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
        OnChange = UpdateFieldOnChange
      end
      object edNumber: TevDBEdit
        Left = 118
        Top = 50
        Width = 83
        Height = 21
        HelpContext = 19002
        DataSource = dsFieldData
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edAmount: TevDBEdit
        Left = 209
        Top = 50
        Width = 83
        Height = 21
        HelpContext = 19002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '[-]*12[#][.*4[#]]'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
