unit EvInitApp;

interface

uses SysUtils, Forms, EvMainboard, EvContext, isBasicUtils, EvUtils, EvConsts, isBaseClasses,
     SEncryptionRoutines, isThreadManager, isAppIDs, EvBasicUtils;

  procedure InitializeEvoApp;
  procedure UninitializeEvoApp;
  function  IsStandalone: Boolean;

implementation

var Standalone: Boolean;
var SingleInstance: Boolean;

function  IsStandalone: Boolean;
begin
  Result := Standalone;
end;

procedure InitializeEvoApp;
var
  sDomain: String;
  F: TextFile;
begin
  Standalone := (AppID = EvoSAAppInfo.AppID) or
                (AppID = EvoSARWAdapterAppInfo.AppID) or
                (AppID = EvoRequestProcSAAppInfo.AppID) or
                (AppID = EvoUtilISystemsAppInfo.AppID) or
                (AppID = EvoUtilDBMaintenanceAppInfo.AppID);


  SingleInstance := (AppID = EvoRequestBrokerAppInfo.AppID) or
                    (AppID = EvoRequestProcAppInfo.AppID) or
                    (AppID = EvoRequestProcSAAppInfo.AppID) or
                    (AppID = EvoAPIAdapterAppInfo.AppID) or
                    (AppID = EvoADRClientAppInfo.AppID) or
                    (AppID = EvoADRServerAppInfo.AppID) or
                    (AppID = EvoUtilDBMaintenanceAppInfo.AppID);


  // Identify EvoDomain
  if FileExists(AppDir + 'SB.dat') then
  begin
    AssignFile(F, AppDir + 'SB.dat');
    try
      Reset(F);
      try
        Read(F, sDomain);
        sDomain := DecryptHexString(sDomain);
      except
        on E: Exception do
          E.Message := 'Cannot read SB.dat file.'#13 + E.Message;
      end;
    finally
      CloseFile(F);
    end;
  end

  else
    sDomain := sDefaultDomain;

  ActivateMainboard;
  Mainboard.InitializeModules;

  // Init Application Temp Folder
  SetAppTempFolder(mb_AppConfiguration.GetValue('General\TempFolder', AppTempFolder));
  if SingleInstance then
    ClearDir(AppTempFolder);


  // Initial state is a guest context.
  // It gets substituted with the real one ater authorization has been approved
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');
end;

procedure UninitializeEvoApp;
begin
  SetAppTerminatingFlag;
  Mainboard.ContextManager.SetThreadContext(nil);
  Mainboard.DeinitializeModules;
  DeactivateGlobalThreadManager;
  DeactivateMainboard;

  if SingleInstance then
    ClearDir(AppTempFolder, True);
end;

end.

