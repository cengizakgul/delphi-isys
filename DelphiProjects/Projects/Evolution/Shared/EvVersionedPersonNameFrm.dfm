inherited evVersionedPersonName: TevVersionedPersonName
  Left = 603
  Top = 358
  ClientHeight = 504
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 366
    inherited grFieldData: TevDBGrid
      Height = 239
    end
    inherited pnlEdit: TevPanel
      Top = 283
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 366
    Width = 791
    Height = 138
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 138
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Person Names'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lFirstName: TevLabel
        Left = 20
        Top = 35
        Width = 50
        Height = 13
        Caption = 'First Name'
        FocusControl = edFirstName
      end
      object lMiddleName: TevLabel
        Left = 261
        Top = 35
        Width = 12
        Height = 13
        Caption = 'MI'
        FocusControl = edMiddleName
      end
      object lLastName: TevLabel
        Left = 20
        Top = 74
        Width = 51
        Height = 13
        Caption = 'Last Name'
        FocusControl = edLastName
      end
      object edFirstName: TevDBEdit
        Left = 20
        Top = 50
        Width = 232
        Height = 21
        DataSource = dsFieldData
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edMiddleName: TevDBEdit
        Left = 261
        Top = 50
        Width = 29
        Height = 21
        DataSource = dsFieldData
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edLastName: TevDBEdit
        Left = 20
        Top = 89
        Width = 270
        Height = 21
        DataSource = dsFieldData
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
