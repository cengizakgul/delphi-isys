inherited EvVersionedACALowestCost: TEvVersionedACALowestCost
  Left = 403
  Top = 244
  Caption = '/'
  ClientHeight = 564
  ClientWidth = 820
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 820
    Height = 424
    inherited grFieldData: TevDBGrid
      Width = 772
      Height = 297
    end
    inherited pnlEdit: TevPanel
      Top = 341
      Width = 772
      inherited pnlButtons: TevPanel
        Left = 463
      end
      inherited deBeginDate: TevDBDateTimePicker
        OnChange = nil
      end
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 424
    Width = 820
    Height = 140
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 279
      Height = 140
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Lowest Cost Benefit  '
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lblACALowestCost: TevLabel
        Left = 20
        Top = 74
        Width = 100
        Height = 13
        Caption = 'Lowest Cost Benefit  '
      end
      object evLabel3: TevLabel
        Left = 20
        Top = 35
        Width = 63
        Height = 13
        Caption = 'ACA Benefit  '
      end
      object cbACALowestCost: TevDBLookupCombo
        Left = 20
        Top = 89
        Width = 230
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description'#9'F')
        DataField = 'ACA_CO_BENEFIT_SUBTYPE_NBR'
        DataSource = dsFieldData
        LookupTable = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
        LookupField = 'CO_BENEFIT_SUBTYPE_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object cbAcaBenefit: TevComboBox
        Left = 20
        Top = 50
        Width = 230
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbAcaBenefitChange
      end
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 368
    Top = 104
  end
  object cdSubType: TevClientDataSet
    Left = 560
    Top = 144
  end
end
