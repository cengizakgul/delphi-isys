// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDDClasses;

interface

uses
  db, typinfo, SysUtils, Classes, StrUtils,  EvConsts, EvExceptions, Windows,
  EvTypes, isBaseClasses, EvClientDataSet, IsTypes, Math, Controls, Forms, DBConsts,
  isStrUtils;

type
  TddTable = class;
  TddDatabase = class;

  TddFieldList = array of string;
  TddTableList = array of string;

  TddTableClass = class of TddTable;
  TddDatabaseClass = class of TddDatabase;

  TableField = record
    T: TddTableClass; // Table Name
    F: string; // Field Name
    C: string; // Choices
    G: string; // Groupings
    D: string; //  Default Initialization Values
    S: boolean;// Field is secured, starred
  end;

  TevFieldValuesRec = record
    Values:   string;
    DefValue: string;
  end;

  PTevFieldValuesRec = ^TevFieldValuesRec;

  TevFieldValues = class(TStringList)
  private
    function    GetItem(Index: Integer): TevFieldValuesRec;
    procedure   AddField(const AField: TableField);
  public
    constructor Create;
    destructor  Destroy; override;
    procedure   Clear; override;
    function    FieldIndex(const AField: String): Integer;
    function    GetDefaultValue(const AField: String): String;
    property    Items[Index: Integer]: TevFieldValuesRec read GetItem; default;
  end;

  TevFieldCodeValues = class(TStringList)
  private
    function GetItem(Index: Integer): TevFieldValues;
  public
    destructor  Destroy; override;
    procedure   Clear; override;
    procedure   Initialize;
    function    FindFields(const ATable: TddTableClass): TevFieldValues; overload;
    function    FindFields(const ATableName: String): TevFieldValues; overload;
    property    Items[Index: Integer]: TevFieldValues read GetItem; default;
  end;

  TddParam = record
    Name: string;
    case ParamType: TFieldType of
    ftString: (ParamSize: Integer);
  end;

  TddParamList = array of TddParam;

  TddReferenceDesc = record
    Database: TddDatabaseClass;
    Table: TddTableClass;
    SrcFields: array of string;
    DestFields: array of string;
    MasterDetail: Boolean;
    ParentEntityPropName: String;
  end;

  TddReferencesDesc = array of TddReferenceDesc;

  TStrEntry = class(TComponent)
  private
    class function GetData(const ddTableClass: TddTableClass): TddTable;
  protected
    function GetDataSet(const ddTableClass: TddTableClass): TddTable;
    procedure SetName(const NewName: TComponentName); override;
    procedure Loaded; override;
  public
    CustomAccess: Boolean;
    class function GetClientID: Integer; virtual;
    constructor Create(AOwner: TComponent); override;
  end;

  TevGetFieldNameMode = (fmAll, fmNonVersioned, fmVersioned);

  TddTable = class(TevClientDataSet)
  private
    FCheckValues: TevFieldValues;
    FMapFields: array of Integer;
    function GetField(Index: Integer): TField;
  protected
    function GetDateTimeField(Index: Integer): TDateTimeField;
    function GetStringField(Index: Integer): TStringField;
    function GetIntegerField(Index: Integer): TIntegerField;
    function GetFloatField(Index: Integer): TFloatField;
    function GetBlobField(Index: Integer): TBlobField;
    function GetDateField(Index: Integer): TDateField;
    procedure DoOnNewRecord; override;
    procedure DoBeforeDelete; override;
    procedure DoAfterClose; override;
    procedure DoAfterOpen; override;
    procedure DoBeforePost; override;
    procedure OnBeforeFieldDefAdded(const FieldName: string; var DataType: TFieldType; var DataLength: Integer; var Required: Boolean); override;
    procedure DataEvent(Event: TDataEvent; Info: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    class function ReadOnlyTable: Boolean; virtual;
    class function GetClassIndex: Integer; virtual;
    class function GetParameters: TddParamList; virtual;
    class function GetFillerLength: Integer; virtual;
    class function GetIsHistorical: Boolean; virtual;
    class function GetFieldSize(FieldName: string): Integer; virtual;
    class function GetFieldPrecision(FieldName: string): Integer; virtual;
    class function GetFieldDisplayLabel(const FieldName: string): string; virtual;
    class function GetIsRequiredField(FieldName: string): Boolean; virtual;
    class function GetIsAutoFillField(FieldName: string): Boolean; virtual;
    class function GetFieldType(FieldName: string): TFieldType; virtual;
    class function GetIsCurrent: Boolean; virtual;
    class function GetIsUnsecure: Boolean; virtual;
    class function GetRequiredFields: TddFieldList; virtual;
    class function GetPrimaryKey: TddFieldList; virtual;
    class function GetLogicalKeys: TddFieldList; virtual;
    class function GetDisplayKey: string; virtual;
    class function GetFieldList: TddFieldList;
    class function GetFieldNameList(const AMode: TevGetFieldNameMode = fmAll): IisStringList;
    class function GetParentsDesc: TddReferencesDesc; virtual;
    class function GetChildrenDesc: TddReferencesDesc; virtual;
    class function GetTableName: string;
    class function GetProviderName: string;
    class function GetDBType: TevDBType; virtual;
    class function IsVersionedTable: Boolean; virtual;
    class function IsVersionedField(FieldName: string): Boolean; virtual;
    class function GetEntityName: String; virtual;
    class function GetFieldEntityProperty(const FieldName: string): string; virtual;
    class function GetRefBlobFieldList: TddReferencesDesc;
    class function GetRefBlobTableClass: TddTableClass; virtual;
    class function GetParentTable(const AFieldName: String): TddTableClass;
    class function GetEvTableNbr: Integer; virtual;
    class function GetEvFieldNbr(const AFieldName: string): Integer; virtual;
    class function GetDisplayFormat(const AFieldName: string): String; virtual;
  published
    property BlobsLoadMode default blmManualLoad;
  end;

  TddSYTable = class(TddTable)
  protected
    procedure SetClientID(const Value: Integer); override;
  public
    class function GetDBType: TevDBType; override;
  end;

  TddSBTable = class(TddTable)
  protected
    procedure SetClientID(const Value: Integer); override;
  public
    class function GetDBType: TevDBType; override;
    class function GetRefBlobTableClass: TddTableClass; override;
  end;

  TddTMPTable = class(TddTable)
  protected
    procedure SetClientID(const Value: Integer); override;
    function  GetAsOfDate: TisDate; override;
    procedure SetAsOfDate(const AValue: TisDate); override;
  public
    class function GetDBType: TevDBType; override;
  end;

  TddClienDBTable = class(TddTable)
  public
    class function GetDBType: TevDBType; override;
    class function GetRefBlobTableClass: TddTableClass; override;
  end;

  TddCLTable = class(TddClienDBTable);
  TddCOTable = class(TddClienDBTable);
  TddEETable = class(TddClienDBTable);
  TddPRTable = class(TddClienDBTable);

  TddDatabase = class(TStrEntry)
  public
    class function GetDBType: TevDBType; virtual; abstract;
    class function GetTableList: TddTableList;
  end;

  TddCLDatabase = class(TddDatabase)
  public
    class function GetClientID: Integer; override;
  end;


function GetddDatabase(DBClass: TddDatabaseClass): TddDatabase;
function UplinkRetrieveCondition(const UpLinkFieldName: string; const UpLinkDataSet: TddTable; const ExtraCheckCondition: string = ''): string;
function GetSecuredFields: IisStringList;

implementation

uses
  SDDAddProps, EvUtils, EvDataAccessComponents,
  SFieldCodeValues, SDataStructure, EvBasicUtils, DateUtils, EvContext, EvMainboard,
  EvCommonInterfaces;

var gvSecuredFields: IisStringList;
var gvDisplayLabels: IisListOfValues;
var gvDisplayKeys: IisListOfValues;
var gvDisplayFormats: IisListOfValues;

function GetSecuredFields: IisStringList;
begin
  Result := TisStringList.CreateAsIndex;
  Result.AddStrings(gvSecuredFields);
end;

procedure BuildSecuredFieldsList;
begin
  gvSecuredFields := TisStringList.CreateAsIndex;

  gvSecuredFields.Add('Co.Payroll_Password');
  gvSecuredFields.Add('Sb.Development_Ftp_Password');
  gvSecuredFields.Add('Sb_User.User_Password');
  gvSecuredFields.Add('Sb_User.Login_Question1');
  gvSecuredFields.Add('Sb_User.Login_Question2');
  gvSecuredFields.Add('Sb_User.Login_Question3');
  gvSecuredFields.Add('Sb_User.Login_Answer1');
  gvSecuredFields.Add('Sb_User.Login_Answer2');
  gvSecuredFields.Add('Sb_User.Login_Answer3');
  gvSecuredFields.Add('Sb_User.Sec_Question1');
  gvSecuredFields.Add('Sb_User.Sec_Question2');
  gvSecuredFields.Add('Sb_User.Sec_Answer1');
  gvSecuredFields.Add('Sb_User.Sec_Answer2');
  gvSecuredFields.Add('EE.SelfServe_Password');
  gvSecuredFields.Add('EE.Login_Question1');
  gvSecuredFields.Add('EE.Login_Question2');
  gvSecuredFields.Add('EE.Login_Question3');
  gvSecuredFields.Add('EE.Login_Answer1');
  gvSecuredFields.Add('EE.Login_Answer2');
  gvSecuredFields.Add('EE.Login_Answer3');
  gvSecuredFields.Add('EE.Sec_Question1');
  gvSecuredFields.Add('EE.Sec_Question2');
  gvSecuredFields.Add('EE.Sec_Answer1');
  gvSecuredFields.Add('EE.Sec_Answer2');
  gvSecuredFields.Add('TMP_EE.SelfServe_Password');
end;


// This is a temporary solution. This data should be stored in DB schema.
procedure BuildDisplayLabelList;
begin
  gvDisplayLabels := TisListOfValues.Create;

  gvDisplayLabels.AddValue('CL.RECIPROCATE_SUI', 'Reciprocate SUI');
  gvDisplayLabels.AddValue('CL.TERMINATION_CODE', 'Status Code');
  gvDisplayLabels.AddValue('CL_3_PARTY_SICK_PAY_ADMIN.W2', 'W2');
  gvDisplayLabels.AddValue('CL_E_DS.CL_3_PARTY_SICK_PAY_ADMIN_NBR', 'Third Party Sick Administrator');
  gvDisplayLabels.AddValue('CL_E_DS.CL_PENSION_NBR', 'Pension Link');
  gvDisplayLabels.AddValue('CL_E_DS.CUSTOM_E_D_CODE_NUMBER', 'E/D Code');
  gvDisplayLabels.AddValue('CL_E_DS.DESCRIPTION', 'Description');
  gvDisplayLabels.AddValue('CL_E_DS.EE_EXEMPT_EXCLUDE_EIC', 'EE EIC');
  gvDisplayLabels.AddValue('CL_E_DS.EE_EXEMPT_EXCLUDE_FEDERAL', 'EE Federal');
  gvDisplayLabels.AddValue('CL_E_DS.EE_EXEMPT_EXCLUDE_MEDICARE', 'EE Medicare');
  gvDisplayLabels.AddValue('CL_E_DS.EE_EXEMPT_EXCLUDE_OASDI', 'EE OASDI');
  gvDisplayLabels.AddValue('CL_E_DS.ER_EXEMPT_EXCLUDE_FUI', 'ER FUI');
  gvDisplayLabels.AddValue('CL_E_DS.ER_EXEMPT_EXCLUDE_MEDICARE', 'ER Medicare');
  gvDisplayLabels.AddValue('CL_E_DS.ER_EXEMPT_EXCLUDE_OASDI', 'ER OASDI');
  gvDisplayLabels.AddValue('CL_E_DS.E_D_CODE_TYPE', 'Code Type');
  gvDisplayLabels.AddValue('CL_E_DS.W2_BOX', 'Override W2 Box');
  gvDisplayLabels.AddValue('CL_E_D_LOCAL_EXMPT_EXCLD.EXEMPT_EXCLUDE', 'Local Tax');
  gvDisplayLabels.AddValue('CL_E_D_LOCAL_EXMPT_EXCLD.SY_LOCALS_NBR', 'Local');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYEE_EXEMPT_EXCLUDE_SDI', 'EE SDI');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYEE_EXEMPT_EXCLUDE_STATE', 'EE State');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYEE_EXEMPT_EXCLUDE_SUI', 'EE SUI');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYEE_STATE_OR_TYPE', 'Type');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYEE_STATE_OR_VALUE', 'Value');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYER_EXEMPT_EXCLUDE_SDI', 'ER SDI');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.EMPLOYER_EXEMPT_EXCLUDE_SUI', 'ER SUI');
  gvDisplayLabels.AddValue('CL_E_D_STATE_EXMPT_EXCLD.SY_STATES_NBR', 'State');
  gvDisplayLabels.AddValue('CL_PENSION.MARK_PENSION_ON_W2', 'Mark Pension Box on W2');
  gvDisplayLabels.AddValue('CL_PERSON.EIN_OR_SOCIAL_SECURITY_NUMBER', 'EIN or SSN');
  gvDisplayLabels.AddValue('CL_PERSON.FIRST_NAME', 'First Name');
  gvDisplayLabels.AddValue('CL_PERSON.LAST_NAME', 'Last Name');
  gvDisplayLabels.AddValue('CL_PERSON.MIDDLE_INITIAL', 'MI');
  gvDisplayLabels.AddValue('CL_PERSON.SOCIAL_SECURITY_NUMBER', 'SSN');
  gvDisplayLabels.AddValue('CL_PERSON.W2_FIRST_NAME', 'First Name');
  gvDisplayLabels.AddValue('CL_PERSON.W2_LAST_NAME', 'Last Name');
  gvDisplayLabels.AddValue('CL_PERSON.W2_MIDDLE_NAME', 'Middle Name');
  gvDisplayLabels.AddValue('CL_PERSON.W2_NAME_SUFFIX', 'Name Suffix');
  gvDisplayLabels.AddValue('CO.ADDRESS1', 'Address 1');
  gvDisplayLabels.AddValue('CO.ADDRESS2', 'Address 2');
  gvDisplayLabels.AddValue('CO.APPLY_MISC_LIMIT_TO_1099', 'Apply Misc Limit to 1099');
  gvDisplayLabels.AddValue('CO.BUSINESS_TYPE', 'Business Type');
  gvDisplayLabels.AddValue('CO.CITY', 'City');
  gvDisplayLabels.AddValue('CO.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO.CO_WORKERS_COMP_NBR', 'W/C Code');
  gvDisplayLabels.AddValue('CO.DBA', 'DBA');
  gvDisplayLabels.AddValue('CO.EMPLOYER_TYPE', 'Employer Type');
  gvDisplayLabels.AddValue('CO.FEDERAL_TAX_DEPOSIT_FREQUENCY', 'Federal Tax Deposit Frequency');
  gvDisplayLabels.AddValue('CO.FEDERAL_TAX_EXEMPT_STATUS', 'Exempt Federal');
  gvDisplayLabels.AddValue('CO.FED_945_TAX_DEPOSIT_FREQUENCY', '945 Tax Deposit Frequency');
  gvDisplayLabels.AddValue('CO.FED_TAX_EXEMPT_EE_MEDICARE', 'Exempt EE Medicare');
  gvDisplayLabels.AddValue('CO.FED_TAX_EXEMPT_EE_OASDI', 'Exempt EE OASDI');
  gvDisplayLabels.AddValue('CO.FED_TAX_EXEMPT_ER_MEDICARE', 'Exempt ER Medicare');
  gvDisplayLabels.AddValue('CO.FED_TAX_EXEMPT_ER_OASDI', 'Exempt ER OASDI');
  gvDisplayLabels.AddValue('CO.FEIN', 'EIN Number');
  gvDisplayLabels.AddValue('CO.FUI_RATE_OVERRIDE', 'FUI Rate Credit Override');
  gvDisplayLabels.AddValue('CO.FUI_TAX_DEPOSIT_FREQUENCY', 'FUI Tax Deposit Frequency');
  gvDisplayLabels.AddValue('CO.FUI_TAX_EXEMPT', 'Exempt FUI');
  gvDisplayLabels.AddValue('CO.LEGAL_ADDRESS1', 'Address 1');
  gvDisplayLabels.AddValue('CO.LEGAL_ADDRESS2', 'Address 2');
  gvDisplayLabels.AddValue('CO.LEGAL_CITY', 'City');
  gvDisplayLabels.AddValue('CO.LEGAL_NAME', 'Name');
  gvDisplayLabels.AddValue('CO.LEGAL_STATE', 'State');
  gvDisplayLabels.AddValue('CO.LEGAL_ZIP_CODE', 'Zip');
  gvDisplayLabels.AddValue('CO.MAKE_UP_TAX_DEDUCT_SHORTFALLS', 'FIT/SIT Shortfall Makeup');
  gvDisplayLabels.AddValue('CO.NAME', 'Name');
  gvDisplayLabels.AddValue('CO.NAME_ON_INVOICE', 'Print on Invoices');
  gvDisplayLabels.AddValue('CO.PRINT_CPA', 'Print on Reports');
  gvDisplayLabels.AddValue('CO.STATE', 'State');
  gvDisplayLabels.AddValue('CO.TERMINATION_CODE', 'Status Code');
  gvDisplayLabels.AddValue('CO.TERMINATION_DATE', 'Terminated');
  gvDisplayLabels.AddValue('CO.USE_DBA_ON_TAX_RETURN', 'Name to Use on Tax Returns');
  gvDisplayLabels.AddValue('CO.ZIP_CODE', 'Zip');
  gvDisplayLabels.AddValue('CO.ENABLE_HR', 'HR');
  gvDisplayLabels.AddValue('CO.SY_ANALYTICS_TIER_NBR', 'Tier ID');
  gvDisplayLabels.AddValue('CO.SY_FED_ACA_OFFER_CODES_NBR', 'ACA Coverage');
  gvDisplayLabels.AddValue('CO.SY_FED_ACA_RELIEF_CODES_NBR', 'ACA Relief Code');
  gvDisplayLabels.AddValue('CO.ACA_HEALTH_CARE_START', 'ACA Health Care Start Date');
  gvDisplayLabels.AddValue('CO.ACA_SELF_INSURED', 'ACA Self Insured');
  gvDisplayLabels.AddValue('CO.ALE_TRANSITION_RELIEF', 'ALE Transition Relief');
  gvDisplayLabels.AddValue('CO_BRANCH.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO_DEPARTMENT.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO_DIVISION.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO_JOBS.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.CL_E_DS_NBR', 'OPT Client E/D');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.EXEMPT', 'Exempt');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.FINAL_TAX_RETURN', 'Final Tax Return');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.LOCAL_ACTIVE', 'Active');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.LOCAL_EIN_NUMBER', 'EIN Number');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.MISCELLANEOUS_AMOUNT', 'Miscellaneous Amount');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.SELF_ADJUST_TAX', 'Self Adjust Tax');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.SY_LOCAL_DEPOSIT_FREQ_NBR', 'Deposit Frequency');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.TAX_RATE', 'Override Tax Rate');
  gvDisplayLabels.AddValue('CO_LOCAL_TAX.TAX_RETURN_CODE', 'Tax Return Code');
  gvDisplayLabels.AddValue('CO_STATES.COMPANY_PAID_HEALTH_INSURANCE', 'Offers Healthcare');
  gvDisplayLabels.AddValue('CO_STATES.EE_SDI_EXEMPT', 'EE SDI Exempt');
  gvDisplayLabels.AddValue('CO_STATES.ER_SDI_EXEMPT', 'ER SDI Exempt');
  gvDisplayLabels.AddValue('CO_STATES.LAST_TAX_RETURN', 'Last Tax Return');
  gvDisplayLabels.AddValue('CO_STATES.STATE_EFT_EIN', 'State EFT EIN');
  gvDisplayLabels.AddValue('CO_STATES.STATE_EFT_PIN_NUMBER', 'State EFT PIN Number');
  gvDisplayLabels.AddValue('CO_STATES.STATE_EIN', 'State EIN');
  gvDisplayLabels.AddValue('CO_STATES.STATE_EXEMPT', 'State Exempt');
  gvDisplayLabels.AddValue('CO_STATES.STATE_NON_PROFIT', 'Inactive State');
  gvDisplayLabels.AddValue('CO_STATES.STATE_SDI_EFT_EIN', 'State SDI EFT EIN ');
  gvDisplayLabels.AddValue('CO_STATES.STATE_SDI_EIN', 'State SDI EIN');
  gvDisplayLabels.AddValue('CO_STATES.SUI_EFT_EIN', 'EFT EIN');
  gvDisplayLabels.AddValue('CO_STATES.SUI_EFT_PIN_NUMBER', 'EFT PIN Number');
  gvDisplayLabels.AddValue('CO_STATES.SUI_EIN', 'EIN');
  gvDisplayLabels.AddValue('CO_STATES.SUI_EXEMPT', 'SUI Exempt');
  gvDisplayLabels.AddValue('CO_STATES.SUI_TAX_DEPOSIT_FREQUENCY', 'Tax Deposit Frequency');
  gvDisplayLabels.AddValue('CO_STATES.TAX_RETURN_CODE', 'Tax Return Code');
  gvDisplayLabels.AddValue('CO_STATES.TCD_DEPOSIT_FREQUENCY_NBR', 'TCD Deposit Frequency');
  gvDisplayLabels.AddValue('CO_STATES.USE_DBA_ON_TAX_RETURN', 'Name for Tax Returns');
  gvDisplayLabels.AddValue('CO_SUI.FINAL_TAX_RETURN', 'SUI Inactive');
  gvDisplayLabels.AddValue('CO_SUI.LAST_TAX_RETURN', 'Last Tax Return');
  gvDisplayLabels.AddValue('CO_SUI.RATE', 'Rate');
  gvDisplayLabels.AddValue('CO_SUI.SUI_REIMBURSER', 'SUI Reimburser');
  gvDisplayLabels.AddValue('CO_SUI.TAX_RETURN_CODE', 'Tax Return Code');
  gvDisplayLabels.AddValue('CO_TEAM.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('CO_WORKERS_COMP.CO_STATES_NBR', 'State');
  gvDisplayLabels.AddValue('CO_WORKERS_COMP.EXPERIENCE_RATING', 'Experience Rating');
  gvDisplayLabels.AddValue('CO_WORKERS_COMP.RATE', 'Rate');
  gvDisplayLabels.AddValue('EE.BASE_RETURNS_ON_THIS_EE', 'Combine Returns on this EE');
  gvDisplayLabels.AddValue('EE.COMPANY_OR_INDIVIDUAL_NAME', '1099 or Employee');
  gvDisplayLabels.AddValue('EE.CORPORATE_OFFICER', 'Corporate Officer');
  gvDisplayLabels.AddValue('EE.CO_BRANCH_NBR', 'Branch Name');
  gvDisplayLabels.AddValue('EE.CO_DEPARTMENT_NBR', 'Deaprtment Name');
  gvDisplayLabels.AddValue('EE.CO_DIVISION_NBR', 'Division Name');
  gvDisplayLabels.AddValue('EE.CO_HR_POSITIONS_NBR', 'Position for Pay Grade');
  gvDisplayLabels.AddValue('EE.CO_JOBS_NBR', 'Default Job');
  gvDisplayLabels.AddValue('EE.CO_TEAM_NBR', 'Team Name');
  gvDisplayLabels.AddValue('EE.CO_WORKERS_COMP_NBR', 'Default WC');
  gvDisplayLabels.AddValue('EE.CURRENT_TERMINATION_CODE', 'Current Status Code');
  gvDisplayLabels.AddValue('EE.DISTRIBUTION_CODE_1099R', 'Distribution Code');
  gvDisplayLabels.AddValue('EE.EXEMPT_EMPLOYEE_MEDICARE', 'EE Medicare Exempt');
  gvDisplayLabels.AddValue('EE.EXEMPT_EMPLOYEE_OASDI', 'EE OASDI Exempt');
  gvDisplayLabels.AddValue('EE.EXEMPT_EMPLOYER_FUI', 'ER FUI Exempt');
  gvDisplayLabels.AddValue('EE.EXEMPT_EMPLOYER_MEDICARE', 'ER Medicare Exempt');
  gvDisplayLabels.AddValue('EE.EXEMPT_EMPLOYER_OASDI', 'ER OASDI Exempt');
  gvDisplayLabels.AddValue('EE.EXEMPT_EXCLUDE_EE_FED', 'EE Federal');
  gvDisplayLabels.AddValue('EE.FUI_RATE_CREDIT_OVERRIDE', 'FUI Rate Credit Override');
  gvDisplayLabels.AddValue('EE.HEALTHCARE_COVERAGE', 'Healthcare Coverage');
  gvDisplayLabels.AddValue('EE.HIGHLY_COMPENSATED', 'Highly Compensated');
  gvDisplayLabels.AddValue('EE.HOME_TAX_EE_STATES_NBR', 'State');
  gvDisplayLabels.AddValue('EE.MAKEUP_FICA_ON_CLEANUP_PR', 'Ignore FICA on Cleanup Payroll');
  gvDisplayLabels.AddValue('EE.NEW_HIRE_REPORT_SENT', 'New Hire Report Sent');
  gvDisplayLabels.AddValue('EE.PENSION_PLAN_1099R', 'Pension Plan');
  gvDisplayLabels.AddValue('EE.SALARY_AMOUNT', 'Salary Amount');
  gvDisplayLabels.AddValue('EE.TAX_AMT_DETERMINED_1099R', 'Tax Amount Determined');
  gvDisplayLabels.AddValue('EE.TIPPED_DIRECTLY', 'Tipped Directly');
  gvDisplayLabels.AddValue('EE.TOTAL_DISTRIBUTION_1099R', 'Total Distribution');
  gvDisplayLabels.AddValue('EE.W2_DECEASED', 'Deceased');
  gvDisplayLabels.AddValue('EE.W2_DEFERRED_COMP', 'Deferred Compensation');
  gvDisplayLabels.AddValue('EE.W2_LEGAL_REP', 'Legal Rep');
  gvDisplayLabels.AddValue('EE.W2_PENSION', 'Pension');
  gvDisplayLabels.AddValue('EE.W2_STATUTORY_EMPLOYEE', 'Statutory Employee');
  gvDisplayLabels.AddValue('EE.W2_TYPE', 'Annual Form Type');
  gvDisplayLabels.AddValue('EE.BENEFITS_ELIGIBLE', 'Benefits Eligible');
  gvDisplayLabels.AddValue('EE.ACA_STATUS', 'ACA Status');
  gvDisplayLabels.AddValue('EE.ACA_COVERAGE_OFFER', 'ACA Coverage Offer');
  gvDisplayLabels.AddValue('EE.ACA_POLICY_ORIGIN', 'ACA Policy Origin');
  gvDisplayLabels.AddValue('EE.EE_ACA_SAFE_HARBOR', 'ACA Safe Harbor');
  gvDisplayLabels.AddValue('EE.ACA_FORMAT', 'ACA Format');
  gvDisplayLabels.AddValue('EE.ACA_TYPE', 'ACA Type');
  gvDisplayLabels.AddValue('EE.ACA_SAFE_HARBOR_TYPE', 'Safe Harbor Type');
  gvDisplayLabels.AddValue('EE.SY_FED_ACA_OFFER_CODES_NBR', 'ACA Coverage Offer');
  gvDisplayLabels.AddValue('EE.SY_FED_ACA_RELIEF_CODES_NBR', 'ACA Relief Code');
  gvDisplayLabels.AddValue('EE_BENEFITS.COBRA_STATUS', 'Elected');
  gvDisplayLabels.AddValue('EE_LOCALS.CO_LOCAL_TAX_NBR', 'Local');
  gvDisplayLabels.AddValue('EE_LOCALS.CO_LOCATIONS_NBR', 'Location');
  gvDisplayLabels.AddValue('EE_LOCALS.DEDUCT', 'Deduct');
  gvDisplayLabels.AddValue('EE_LOCALS.EXEMPT_EXCLUDE', 'Exempt or Block');
  gvDisplayLabels.AddValue('EE_LOCALS.INCLUDE_IN_PRETAX', 'Include pretax deductions in tax wages');
  gvDisplayLabels.AddValue('EE_LOCALS.LOCAL_ENABLED', 'Local Enabled');
  gvDisplayLabels.AddValue('EE_LOCALS.MISCELLANEOUS_AMOUNT', 'Misc Amount');
  gvDisplayLabels.AddValue('EE_LOCALS.OVERRIDE_LOCAL_TAX_TYPE', 'Override Tax Type');
  gvDisplayLabels.AddValue('EE_LOCALS.OVERRIDE_LOCAL_TAX_VALUE', 'Override Tax Value');
  gvDisplayLabels.AddValue('EE_LOCALS.PERCENTAGE_OF_TAX_WAGES', '% of Taxable Wages');
  gvDisplayLabels.AddValue('EE_LOCALS.WORK_ADDRESS_OVR', 'Work Address Override');
  gvDisplayLabels.AddValue('EE_RATES.PRIMARY_RATE', 'Primary Rate');
  gvDisplayLabels.AddValue('EE_RATES.RATE_AMOUNT', 'Rate Amount');
  gvDisplayLabels.AddValue('EE_RATES.RATE_NUMBER', 'Rate Number');
  gvDisplayLabels.AddValue('EE_STATES.EE_SDI_EXEMPT_EXCLUDE', 'EE SDI');
  gvDisplayLabels.AddValue('EE_STATES.EE_SUI_EXEMPT_EXCLUDE', 'EE SUI');
  gvDisplayLabels.AddValue('EE_STATES.ER_SDI_EXEMPT_EXCLUDE', 'ER SDI');
  gvDisplayLabels.AddValue('EE_STATES.ER_SUI_EXEMPT_EXCLUDE', 'ER SUI');
  gvDisplayLabels.AddValue('EE_STATES.RECIPROCAL_CO_STATES_NBR', 'Reciprocal State');
  gvDisplayLabels.AddValue('EE_STATES.SDI_APPLY_CO_STATES_NBR', 'SDI');
  gvDisplayLabels.AddValue('EE_STATES.STATE_EXEMPT_EXCLUDE', 'State Exempt or Block');
  gvDisplayLabels.AddValue('EE_STATES.SUI_APPLY_CO_STATES_NBR', 'SUI');
  gvDisplayLabels.AddValue('SY_STATE_DEPOSIT_FREQ.SY_GL_AGENCY_FIELD_OFFICE_NBR', 'Agency');
  gvDisplayLabels.AddValue('SY_FED_TAX_TABLE.FED_POVERTY_LEVEL', 'Federal Poverty Level');
  gvDisplayLabels.AddValue('SB_ACA_GROUP_ADD_MEMBERS.ALE_EIN_NUMBER', 'EIN Number');      
  gvDisplayLabels.AddValue('SB_ACA_GROUP_ADD_MEMBERS.ALE_NUMBER_OF_FTES', 'Number of FTEs');      
end;

// This is a temporary solution. This data should be stored in DB schema.
procedure BuildDisplayKeys;
begin
  gvDisplayKeys := TisListOfValues.Create;

  // System DB
  gvDisplayKeys.AddValue('SY_SUI', '[SUI_TAX_NAME]');
  gvDisplayKeys.AddValue('SY_STORAGE', '[TAG]');
  gvDisplayKeys.AddValue('SY_STATES', '[STATE]');
  gvDisplayKeys.AddValue('SY_STATE_MARITAL_STATUS', '[STATUS_DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_STATE_EXEMPTIONS', '[E_D_CODE_TYPE]');
  gvDisplayKeys.AddValue('SY_STATE_DEPOSIT_FREQ', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_SEC_TEMPLATES', '[NAME]');
  gvDisplayKeys.AddValue('SY_REPORTS_GROUP', '[NAME]');
  gvDisplayKeys.AddValue('SY_REPORT_WRITER_REPORTS', '[REPORT_DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_REPORT_GROUPS', '[NAME]');
  gvDisplayKeys.AddValue('SY_REPORT_GROUP_MEMBERS', '[SY_REPORT_WRITER_REPORTS_NBR]');
  gvDisplayKeys.AddValue('SY_RECIPROCATED_STATES', '[MAIN_STATE_NBR]   [PARTICIPANT_STATE_NBR]');
  gvDisplayKeys.AddValue('SY_QUEUE_PRIORITY', '[METHOD_NAME]');
  gvDisplayKeys.AddValue('SY_MEDIA_TYPE', '[NAME]');
  gvDisplayKeys.AddValue('SY_LOCALS', '[NAME]   [SY_STATES_NBR]   [LOCAL_TYPE]');
  gvDisplayKeys.AddValue('SY_LOCAL_MARITAL_STATUS', '[SY_STATE_MARITAL_STATUS_NBR]');
  gvDisplayKeys.AddValue('SY_LOCAL_EXEMPTIONS', '[E_D_CODE_TYPE]');
  gvDisplayKeys.AddValue('SY_HR_REFUSAL_REASON', '[SY_STATES_NBR]   [DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_HR_OSHA_ANATOMIC_CODES', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_HR_INJURY_CODES', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_HR_HANDICAPS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_HR_ETHNICITY', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_HR_EEO', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_GLOBAL_AGENCY', '[AGENCY_NAME]');
  gvDisplayKeys.AddValue('SY_GL_AGENCY_HOLIDAYS', '[HOLIDAY_NAME]');
  gvDisplayKeys.AddValue('SY_GL_AGENCY_FIELD_OFFICE', '[ADDITIONAL_NAME]');
  gvDisplayKeys.AddValue('SY_FED_TAX_PAYMENT_AGENCY', '[SY_GLOBAL_AGENCY_NBR]   [SY_REPORTS_NBR]');
  gvDisplayKeys.AddValue('SY_FED_REPORTING_AGENCY', '[SY_GLOBAL_AGENCY_NBR]');
  gvDisplayKeys.AddValue('SY_FED_EXEMPTIONS', '[E_D_CODE_TYPE]');
  gvDisplayKeys.AddValue('SY_DELIVERY_SERVICE', '[NAME]');
  gvDisplayKeys.AddValue('SY_DELIVERY_METHOD', '[NAME]');
  gvDisplayKeys.AddValue('SY_COUNTY', '[COUNTY_NAME]');
  gvDisplayKeys.AddValue('SY_AGENCY_DEPOSIT_FREQ', '[SY_GLOBAL_AGENCY_NBR]   [DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_LOCAL_DEPOSIT_FREQ', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_VENDOR_CATEGORIES', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_ANALYTICS_TIER', '[TIER_ID]');
  gvDisplayKeys.AddValue('SY_DASHBOARDS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_FED_ACA_OFFER_CODES', '[ACA_OFFER_CODE]  [ACA_OFFER_CODE_DESCRIPTION]');
  gvDisplayKeys.AddValue('SY_FED_ACA_RELIEF_CODES', '[ACA_RELIEF_CODE]  [ACA_RELIEF_CODE_DESCRIPTION]');

  // Service Bureau DB
  gvDisplayKeys.AddValue('SB', '[SB_NAME]');
  gvDisplayKeys.AddValue('SB_ACCOUNTANT', '[NAME]');
  gvDisplayKeys.AddValue('SB_AGENCY', '[NAME]');
  gvDisplayKeys.AddValue('SB_AGENCY_REPORTS', '[SB_AGENCY_NBR]   [SB_REPORTS_NBR]');
  gvDisplayKeys.AddValue('SB_BANKS', '[NAME]');
  gvDisplayKeys.AddValue('SB_BANK_ACCOUNTS', '[SB_BANKS_NBR]   [BANK_ACCOUNT_TYPE]   [CUSTOM_BANK_ACCOUNT_NUMBER]');
  gvDisplayKeys.AddValue('SB_DELIVERY_COMPANY', '[NAME]');
  gvDisplayKeys.AddValue('SB_DELIVERY_COMPANY_SVCS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_ENLIST_GROUPS', '[SY_REPORT_GROUPS_NBR]');
  gvDisplayKeys.AddValue('SB_GLOBAL_AGENCY_CONTACTS', '[SY_GLOBAL_AGENCY_NBR]   [CONTACT_NAME]');
  gvDisplayKeys.AddValue('SB_HOLIDAYS', '[HOLIDAY_DATE]');
  gvDisplayKeys.AddValue('SB_MAIL_BOX', '[BARCODE]');
  gvDisplayKeys.AddValue('SB_MEDIA_TYPE', '[SY_MEDIA_TYPE_NBR]');
  gvDisplayKeys.AddValue('SB_MEDIA_TYPE_OPTION', '[OPTION_TAG]');
  gvDisplayKeys.AddValue('SB_MULTICLIENT_REPORTS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_OPTION', '[OPTION_TAG]');
  gvDisplayKeys.AddValue('SB_OTHER_SERVICE', '[NAME]');
  gvDisplayKeys.AddValue('SB_PAPER_INFO', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_REFERRALS', '[NAME]');
  gvDisplayKeys.AddValue('SB_REPORTS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_REPORT_WRITER_REPORTS', '[REPORT_DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_SALES_TAX_STATES', '[STATE]');
  gvDisplayKeys.AddValue('SB_SEC_GROUPS', '[NAME]');
  gvDisplayKeys.AddValue('SB_SEC_GROUP_MEMBERS', '[SB_USER_NBR]');
  gvDisplayKeys.AddValue('SB_SEC_TEMPLATES', '[NAME]');
  gvDisplayKeys.AddValue('SB_SERVICES', '[SERVICE_NAME]');
  gvDisplayKeys.AddValue('SB_TEAM', '[TEAM_DESCRIPTION]');
  gvDisplayKeys.AddValue('SB_TEAM_MEMBERS', '[SB_USER_NBR]');
  gvDisplayKeys.AddValue('SB_USER', '[USER_ID]');
  gvDisplayKeys.AddValue('SB_USER_NOTICE', '[NAME]');
  gvDisplayKeys.AddValue('SB_STORAGE', '[TAG]');
  gvDisplayKeys.AddValue('SB_ACA_GROUP', '[ACA_GROUP_DESCRIPTION]');


  // Client DB
  gvDisplayKeys.AddValue('CL', '[CUSTOM_CLIENT_NUMBER]');
  gvDisplayKeys.AddValue('CL_3_PARTY_SICK_PAY_ADMIN', '[NAME]');
  gvDisplayKeys.AddValue('CL_AGENCY', '[SB_AGENCY_NBR]');
  gvDisplayKeys.AddValue('CL_BANK_ACCOUNT', '[SB_BANKS_NBR]   [BANK_ACCOUNT_TYPE]   [CUSTOM_BANK_ACCOUNT_NUMBER]');
  gvDisplayKeys.AddValue('CL_BILLING', '[NAME]');
  gvDisplayKeys.AddValue('CL_COMMON_PAYMASTER', '[CPM_NAME]');
  gvDisplayKeys.AddValue('CL_CO_CONSOLIDATION', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_DELIVERY_GROUP', '[DELIVERY_GROUP_DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_DELIVERY_METHOD', '[NAME]');
  gvDisplayKeys.AddValue('CL_E_DS', '[CUSTOM_E_D_CODE_NUMBER]');
  gvDisplayKeys.AddValue('CL_E_D_GROUPS', '[NAME]');
  gvDisplayKeys.AddValue('CL_E_D_GROUP_CODES', '[CL_E_D_GROUPS_NBR]   [CL_E_DS_NBR]');
  gvDisplayKeys.AddValue('CL_E_D_LOCAL_EXMPT_EXCLD', '[SY_LOCALS_NBR]   [CL_E_DS_NBR]');
  gvDisplayKeys.AddValue('CL_E_D_STATE_EXMPT_EXCLD', '[SY_STATES_NBR]   [CL_E_DS_NBR]');
  gvDisplayKeys.AddValue('CL_FUNDS', '[NAME]');
  gvDisplayKeys.AddValue('CL_HR_COURSE', '[NAME]');
  gvDisplayKeys.AddValue('CL_HR_PERSON_EDUCATION', '[CL_HR_SCHOOL_NBR]   [START_DATE]');
  gvDisplayKeys.AddValue('CL_HR_PERSON_HANDICAPS', '[SY_HR_HANDICAPS_NBR]');
  gvDisplayKeys.AddValue('CL_HR_PERSON_SKILLS', '[CL_HR_SKILLS_NBR]');
  gvDisplayKeys.AddValue('CL_HR_REASON_CODES', '[NAME]');
  gvDisplayKeys.AddValue('CL_HR_SCHOOL', '[NAME]');
  gvDisplayKeys.AddValue('CL_HR_SKILLS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_MAIL_BOX_GROUP', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_MAIL_BOX_GROUP_OPTION', '[OPTION_TAG]');
  gvDisplayKeys.AddValue('CL_PENSION', '[NAME]');
  gvDisplayKeys.AddValue('CL_PERSON', '[FIRST_NAME] [LAST_NAME]');
  gvDisplayKeys.AddValue('CL_PERSON_DEPENDENTS', '[FIRST_NAME] [LAST_NAME]');
  gvDisplayKeys.AddValue('CL_PERSON_DOCUMENTS', '[NAME]');
  gvDisplayKeys.AddValue('CL_PIECES', '[NAME]');
  gvDisplayKeys.AddValue('CL_REPORT_WRITER_REPORTS', '[REPORT_DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_TIMECLOCK_IMPORTS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CL_UNION', '[NAME]');
  gvDisplayKeys.AddValue('CO', '[CUSTOM_COMPANY_NUMBER]');
  gvDisplayKeys.AddValue('CO_ADDITIONAL_INFO_NAMES', '[NAME]');
  gvDisplayKeys.AddValue('CO_ADDITIONAL_INFO_VALUES', '[INFO_VALUE]');
  gvDisplayKeys.AddValue('CO_AUTO_ENLIST_RETURNS', '[SY_GL_AGENCY_REPORT_NBR]');
  gvDisplayKeys.AddValue('CO_BATCH_LOCAL_OR_TEMPS', '[CO_LOCAL_TAX_NBR]   [CO_PR_CHECK_TEMPLATES_NBR]');
  gvDisplayKeys.AddValue('CO_BATCH_STATES_OR_TEMPS', '[CO_STATES_NBR]   [CO_PR_CHECK_TEMPLATES_NBR]');
  gvDisplayKeys.AddValue('CO_BILLING_HISTORY', '[INVOICE_NUMBER]');
  gvDisplayKeys.AddValue('CO_BRANCH', '[CO_DIVISION_NBR]   #[CUSTOM_BRANCH_NUMBER]([NAME])');
  gvDisplayKeys.AddValue('CO_BRANCH_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('CO_CALENDAR_DEFAULTS', '[FREQUENCY][PERIOD_BEGIN_DATE]');
  gvDisplayKeys.AddValue('CO_DEPARTMENT', '[CO_BRANCH_NBR]   #[CUSTOM_DEPARTMENT_NUMBER]([NAME])');
  gvDisplayKeys.AddValue('CO_DEPARTMENT_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('CO_DIVISION', '#[CUSTOM_DIVISION_NUMBER]([NAME])');
  gvDisplayKeys.AddValue('CO_DIVISION_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('CO_ENLIST_GROUPS', '[SY_REPORT_GROUPS_NBR]');
  gvDisplayKeys.AddValue('CO_E_D_CODES', '[CL_E_DS_NBR]');
  gvDisplayKeys.AddValue('CO_GENERAL_LEDGER', '[LEVEL_TYPE]   [LEVEL_NBR]   [DATA_NBR]   [DATA_TYPE]');
  gvDisplayKeys.AddValue('CO_GROUP', '[GROUP_NAME]');
  gvDisplayKeys.AddValue('CO_GROUP_MANAGER', '[EE_NBR]');
  gvDisplayKeys.AddValue('CO_GROUP_MEMBER', '[EE_NBR]');
  gvDisplayKeys.AddValue('CO_HR_APPLICANT_INTERVIEW', '[INTERVIEW_DATE]   [SCHEDULED_BY]');
  gvDisplayKeys.AddValue('CO_HR_ATTENDANCE_TYPES', '[ATTENDANCE_DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_PERFORMANCE_RATINGS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_POSITIONS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_PROPERTY', '[PROPERTY_DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_RECRUITERS', '[NAME]');
  gvDisplayKeys.AddValue('CO_HR_REFERRALS', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_SALARY_GRADES', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_HR_SUPERVISORS', '[SUPERVISOR_NAME]');
  gvDisplayKeys.AddValue('CO_JOBS', '[DESCRIPTION]   [TRUE_DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_JOBS_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('CO_JOB_GROUPS', '[NAME]');
  gvDisplayKeys.AddValue('CO_LOCAL_TAX', '[SY_LOCALS_NBR]');
  gvDisplayKeys.AddValue('CO_PAY_GROUP', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_PENSIONS', '[CL_PENSION_NBR]');
  gvDisplayKeys.AddValue('CO_PHONE', '[PHONE_NUMBER] x[PHONE_EXT]   [PHONE_TYPE]');
  gvDisplayKeys.AddValue('CO_PR_ACH', '[PR_NBR]');
  gvDisplayKeys.AddValue('CO_PR_BATCH_DEFLT_ED', '[CO_E_D_CODES_NBR]');
  gvDisplayKeys.AddValue('CO_PR_CHECK_TEMPLATES', '[NAME]');
  gvDisplayKeys.AddValue('CO_PR_CHECK_TEMPLATE_E_DS', '[CL_E_DS_NBR]');
  gvDisplayKeys.AddValue('CO_PR_FILTERS', '[NAME]');
  gvDisplayKeys.AddValue('CO_REPORTS', '[CUSTOM_NAME]');
  gvDisplayKeys.AddValue('CO_SALESPERSON', '[SB_USER_NBR]');
  gvDisplayKeys.AddValue('CO_SERVICES', '[NAME]');
  gvDisplayKeys.AddValue('CO_SHIFTS', '[NAME]');
  gvDisplayKeys.AddValue('CO_STATES', '[STATE]');
  gvDisplayKeys.AddValue('CO_STORAGE', '[TAG]');
  gvDisplayKeys.AddValue('CO_SUI', '[SY_SUI_NBR]');
  gvDisplayKeys.AddValue('CO_TEAM', '[CO_DEPARTMENT_NBR]   #[CUSTOM_TEAM_NUMBER]([NAME])');
  gvDisplayKeys.AddValue('CO_TEAM_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('CO_TIME_OFF_ACCRUAL', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_TIME_OFF_ACCRUAL_RATES', '[HOURS]   [MAXIMUM_MONTH_NUMBER]   [MINIMUM_MONTH_NUMBER]');
  gvDisplayKeys.AddValue('CO_TIME_OFF_ACCRUAL_TIERS', '[RATE]   [MAXIMUM_MONTH_NUMBER]   [MINIMUM_MONTH_NUMBER]');
  gvDisplayKeys.AddValue('CO_UNIONS', '[CL_UNION_NBR]');
  gvDisplayKeys.AddValue('CO_WORKERS_COMP', '[WORKERS_COMP_CODE]   [DESCRIPTION]   [CO_STATES_NBR]');
  gvDisplayKeys.AddValue('EE', '#[CUSTOM_EMPLOYEE_NUMBER]   [CL_PERSON_NBR]');
  gvDisplayKeys.AddValue('EE_ADDITIONAL_INFO', '[CO_ADDITIONAL_INFO_NAMES_NBR]');
  gvDisplayKeys.AddValue('EE_BENEFITS', '[BENEFIT_TYPE]   [CO_BENEFIT_SUBTYPE_NBR]   [BENEFIT_EFFECTIVE_DATE] - [EXPIRATION_DATE]');
  gvDisplayKeys.AddValue('EE_BENEFIT_PAYMENT', '[PAYMENT_NUMBER][PAYMENT_DATE]');
  gvDisplayKeys.AddValue('EE_CHILD_SUPPORT_CASES', '[CUSTOM_CASE_NUMBER]');
  gvDisplayKeys.AddValue('EE_DIRECT_DEPOSIT', '[EE_ABA_NUMBER]   [EE_BANK_ACCOUNT_NUMBER]');
  gvDisplayKeys.AddValue('EE_EMERGENCY_CONTACTS', '#[CONTACT_PRIORITY]   [CONTACT_NAME]');
  gvDisplayKeys.AddValue('EE_HR_ATTENDANCE', '[CO_HR_ATTENDANCE_TYPES_NBR]   [PERIOD_FROM] - [PERIOD_TO]');
  gvDisplayKeys.AddValue('EE_HR_CAR', '[CO_HR_CAR_NBR]');
  gvDisplayKeys.AddValue('EE_HR_CO_PROVIDED_EDUCATN', '[COURSE]   [DATE_START] - [DATE_FINISH]');
  gvDisplayKeys.AddValue('EE_HR_INJURY_OCCURRENCE', '[CASE_NUMBER]');
  gvDisplayKeys.AddValue('EE_HR_PERFORMANCE_RATINGS', '[CO_HR_PERFORMANCE_RATINGS_NBR]   [REVIEW_DATE]');
  gvDisplayKeys.AddValue('EE_HR_PROPERTY_TRACKING', '[CO_HR_PROPERTY_NBR]   [SERIAL_NUMBER]   [ISSUED_DATE]');
  gvDisplayKeys.AddValue('EE_LOCALS', '[CO_LOCAL_TAX_NBR]');
  gvDisplayKeys.AddValue('EE_PIECE_WORK', '[CL_PIECES_NBR]');
  gvDisplayKeys.AddValue('EE_RATES', '[RATE_NUMBER]');
  gvDisplayKeys.AddValue('EE_STATES', '[CO_STATES_NBR]');
  gvDisplayKeys.AddValue('EE_TIME_OFF_ACCRUAL', '[CO_TIME_OFF_ACCRUAL_NBR]');
  gvDisplayKeys.AddValue('EE_WORK_SHIFTS', '[CO_SHIFTS_NBR]');
  gvDisplayKeys.AddValue('PR', '[CHECK_DATE] - [RUN_NUMBER]');
  gvDisplayKeys.AddValue('PR_CHECK', '[CUSTOM_PR_BANK_ACCT_NUMBER]   [PAYMENT_SERIAL_NUMBER]');
  gvDisplayKeys.AddValue('PR_CHECK_LINE_LOCALS', '[EE_LOCALS_NBR]');
  gvDisplayKeys.AddValue('PR_CHECK_STATES', '[EE_STATES_NBR]');
  gvDisplayKeys.AddValue('PR_CHECK_SUI', '[CO_SUI_NBR]');
  gvDisplayKeys.AddValue('PR_MISCELLANEOUS_CHECKS', '[CUSTOM_PR_BANK_ACCT_NUMBER]   [PAYMENT_SERIAL_NUMBER]');
  gvDisplayKeys.AddValue('PR_SERVICES', '[CO_SERVICES_NBR]');
  gvDisplayKeys.AddValue('CO_HR_POSITION_GRADES', '[CO_HR_SALARY_GRADES_NBR]');
  gvDisplayKeys.AddValue('CO_ADDITIONAL_INFO', '[CO_ADDITIONAL_INFO_NAMES_NBR]');
  gvDisplayKeys.AddValue('CO_BENEFITS', '[BENEFIT_NAME]');
  gvDisplayKeys.AddValue('CO_BENEFIT_CATEGORY', '[CATEGORY_TYPE]');
  gvDisplayKeys.AddValue('CO_BENEFIT_DISCOUNT', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_BENEFIT_PACKAGE', '[NAME]');
  gvDisplayKeys.AddValue('CO_BENEFIT_PKG_ASMNT', '[CO_BENEFIT_PACKAGE_NBR]');
  gvDisplayKeys.AddValue('CO_BENEFIT_PKG_DETAIL', '[CO_BENEFIT_PACKAGE_NBR]');
  gvDisplayKeys.AddValue('CO_BENEFIT_PROVIDERS', '[NAME]');
  gvDisplayKeys.AddValue('CO_BENEFIT_STATES', '[SY_STATES_NBR]');
  gvDisplayKeys.AddValue('CO_BENEFIT_SUBTYPE', '[DESCRIPTION]');
  gvDisplayKeys.AddValue('CO_USER_REPORTS', '[SB_USER_NBR]   [CO_REPORTS_NBR]');
  gvDisplayKeys.AddValue('EE_BENEFICIARY', '[EE_BENEFITS_NBR]   [CL_PERSON_DEPENDENTS_NBR]   [PRIMARY_BENEFICIARY]');
  gvDisplayKeys.AddValue('EE_BENEFIT_REFUSAL', '[CO_BENEFIT_CATEGORY_NBR]   [SY_HR_REFUSAL_REASON_NBR]');
  gvDisplayKeys.AddValue('CO_LOCATIONS', '[ACCOUNT_NUMBER]   [ADDRESS1] [ADDRESS2], [CITY], [STATE]');
  gvDisplayKeys.AddValue('CO_QEC_RUN', '[QUARTER_BEGIN_DATE]');
  gvDisplayKeys.AddValue('CO_BENEFIT_RATES', '[CO_BENEFIT_SUBTYPE_NBR]   [PERIOD_BEGIN] - [PERIOD_END]');
end;


procedure BuildDisplayFormats;
begin
  gvDisplayFormats := TisListOfValues.Create;

  // Client DB
  gvDisplayFormats.AddValue('CL_E_DS.E_D_CODE_TYPE', '[VALUE]   [DESCRIPTION]');
end;


function UplinkRetrieveCondition(const UpLinkFieldName: string; const UpLinkDataSet: TddTable; const ExtraCheckCondition: string = ''): string;
begin
  Result := MakeString(['', UpLinkDataSet.GetTableName, UpLinkDataSet.GetClassIndex, StringReplace(UpLinkDataSet.RetrieveCondition, '@', '', [rfReplaceAll]), UpLinkDataSet.GenerationNumber, UpLinkFieldName, ExtraCheckCondition], '@');
end;

function GetddDatabase(DBClass: TddDatabaseClass): TddDatabase;
begin
  Result := nil;
end;

{ TddTable }

function TddTable.GetBlobField(Index: Integer): TBlobField;
begin
  Result := GetField(Index) as TBlobField;
end;

function TddTable.GetDateField(Index: Integer): TDateField;
begin
  Result := GetField(Index) as TDateField;
end;

function TddTable.GetDateTimeField(Index: Integer): TDateTimeField;
begin
  Result := GetField(Index) as TDateTimeField;
end;

function TddTable.GetField(Index: Integer): TField;
var
  TypeData: PTypeData;
  PropList: PPropList;
  sName: string;
  i: Integer;
  Count: Integer;
begin
  Result := nil;
  if csDesigning in ComponentState then
    Exit;

  if (Index <= High(FMapFields)) and (FMapFields[Index] <> 0) then
    Result := Fields[Pred(FMapFields[Index])]
  else
  begin
    sName := '';
    TypeData := GetTypeData(ClassInfo);
    if TypeData.PropCount <> 0 then
    begin
      GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
      try
        Count := GetPropList(ClassInfo, [tkClass], PropList);
        for i := 0 to Pred(Count) do
          if PropList[i]^.Index = Index then
          begin
            sName := PropList[i]^.Name;
            Break;
          end;
      finally
        FreeMem(PropList);
      end;
    end;

    Result := FindField(sName);
    if Assigned(Result) then
    begin
      if Index > High(FMapFields) then
        SetLength(FMapFields, Succ(Index));
      FMapFields[Index] := Succ(Result.Index);
    end;
  end;
end;

function TddTable.GetIntegerField(Index: Integer): TIntegerField;
begin
  Result := GetField(Index) as TIntegerField;
end;

function TddTable.GetStringField(Index: Integer): TStringField;
begin
  Result := GetField(Index) as TStringField;
end;

class function TddTable.GetChildrenDesc: TddReferencesDesc;
begin
  SetLength(Result, 0);
end;

class function TddTable.GetParentsDesc: TddReferencesDesc;
begin
  SetLength(Result, 0);
end;

class function TddTable.GetFieldList: TddFieldList;
var
  TypeData: PTypeData;
  PropList: PPropList;
  i: Integer;
  Count: Integer;
begin
  SetLength(Result, 0);
  TypeData := GetTypeData(ClassInfo);
  if TypeData.PropCount <> 0 then
  begin
    GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
    try
      Count := GetPropList(ClassInfo, [tkClass], PropList);
      for i := 0 to Pred(Count) do
        if (PropList[i]^.PropType^.Kind = tkClass) and
           (PropList[i]^.Index > 0) then
        begin
          SetLength(Result, Succ(Length(Result)));
          Result[High(Result)] := PropList[i]^.Name;
        end;
    finally
      FreeMem(PropList);
    end;
  end;
end;

class function TddTable.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

class function TddTable.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 0);
end;

function TddTable.GetFloatField(Index: Integer): TFloatField;
begin
  Result := GetField(Index) as TFloatField;
end;

class function TddTable.GetFillerLength: Integer;
begin
  Result := SDDAddProps.GetFillerLength(Self);
end;

class function TddTable.GetIsCurrent: Boolean;
begin
  Result := SDDAddProps.GetIsCurrent(Self);
end;

class function TddTable.GetIsHistorical: Boolean;
begin
  Result := SDDAddProps.GetIsHistorical(Self);
end;

procedure TddTable.DoOnNewRecord;
var
  Parents: TddReferencesDesc;
  i, j: Integer;
  F: TField;
  RV: TevFieldValuesRec;
begin
  if Assigned(FCheckValues) then
    for i := 0 to Fields.Count - 1 do
    begin
      F := Fields[i];
      j := FCheckValues.FieldIndex(F.FieldName);
      if j <> -1 then
      begin
        RV := FCheckValues[j];
        if Length(RV.DefValue) > 0 then
          F.Value := RV.DefValue
      end;
    end;

  inherited;
  Parents := GetParentsDesc;
  for i := 0 to High(Parents) do
    for j := 0 to High(Parents[i].SrcFields) do
      if GetIsAutoFillField(Parents[i].SrcFields[j]) then
        FindField(Parents[i].SrcFields[j]).Assign(Parents[i].Database.GetData(Parents[i].Table).FindField(Parents[i].DestFields[j]));
end;

procedure TddTable.DoBeforeDelete;
var
  i, j: Integer;
  b: Boolean;
  Children: TddReferencesDesc;
  s: string;
begin
  Children := nil;
  inherited;
  if DeleteChildren then
  begin
    ctx_StartWait;
    try
      Children := GetChildrenDesc;
      for i := 0 to High(GetChildrenDesc) do
        with Children[i].Database.GetData(Children[i].Table) do
          if Active then
          begin
            s := '';
            for j := 0 to High(Children[i].SrcFields) do
              s := s + Children[i].SrcFields[j] + ';';
            system.Delete(s, Length(s), 1);
            DisableControls;
            b := ctx_DataAccess.TempCommitDisable;
            ctx_DataAccess.TempCommitDisable := True;
            try
              while Locate(s, Self[s], []) do
                Delete;
            finally
              ctx_DataAccess.TempCommitDisable := b;
              EnableControls;
            end;
          end;
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TddTable.DoAfterClose;
begin
  inherited;
  FCheckValues := nil;
end;

procedure TddTable.DoAfterOpen;
begin
  FCheckValues := FieldCodeValues.FindFields(TddTableClass(ClassType));
  inherited;
end;

class function TddTable.GetIsUnsecure: Boolean;
begin
  Result := SDDAddProps.GetIsUnsecure(Self);
end;

constructor TddTable.Create(AOwner: TComponent);
begin
  BlobsLoadMode := blmManualLoad;
  inherited;
  Name := RightStr(ClassName, Pred(Length(ClassName)));
  ProviderName := Name + '_PROV';
end;

class function TddTable.GetIsRequiredField(FieldName: string): Boolean;
var
  a: TddFieldList;
  i: Integer;
begin
  Result := False;
  a := GetRequiredFields;
  for i := 0 to High(a) do
    if FieldName = a[i] then
    begin
      Result := True;
      Break;
    end;
end;

class function TddTable.GetFieldSize(FieldName: string): Integer;
begin
  Result := 0;
end;

class function TddTable.GetFieldDisplayLabel(const FieldName: string): string;
begin
  Result := gvDisplayLabels.TryGetValue(GetTableName + '.' + FieldName, '');
  if Result = '' then
    Result := CapitalizeWords(FieldName);
end;

class function TddTable.GetIsAutoFillField(FieldName: string): Boolean;
begin
  Result := SDDAddProps.GetIsAutoFillField(Self, FieldName);
end;

class function TddTable.GetFieldType(FieldName: string): TFieldType;
var
  i: TFieldType;
  c: TClass;
begin
  Result := ftUnknown;
  if IsPublishedProp(Self, FieldName) then
  begin
    c := GetTypeData(GetPropInfo(Self, FieldName).PropType^).ClassType;
    if IsPublishedProp(Self, FieldName) then
      for i := Low(DefaultFieldClasses) to High(DefaultFieldClasses) do
        if DefaultFieldClasses[i] = c then
        begin
          Result := i;
          Break;
        end;
  end;
end;

class function TddTable.GetTableName: string;
begin
  Result := Copy(ClassName, 2, Length(ClassName));
end;

class function TddTable.GetProviderName: string;
begin
  Result := GetTableName + '_PROV';
end;

procedure TddTable.OnBeforeFieldDefAdded(const FieldName: string;
  var DataType: TFieldType; var DataLength: Integer;
  var Required: Boolean);
var
  i: Integer;
begin
  if (DataType = ftString) and (AnsiCompareText('FILLER', FieldName) = 0) then
  begin
    i := GetFillerLength;
    if i <> DEFAULT_FILLER_LENGTH then
      DataLength := i
  end;
  Required := GetIsRequiredField(FieldName);
end;

class function TddTable.GetParameters: TddParamList;
begin
  if GetIsHistorical then
  begin
    SetLength(Result, 1);
    Result[0].Name := 'EFFECTIVE_DATE';
    Result[0].ParamType := ftDateTime;
  end
  else
    SetLength(Result, 0);
end;

class function TddTable.GetClassIndex: Integer;
begin
  Result := 0;
end;

procedure TddTable.DataEvent(Event: TDataEvent; Info: Integer);
begin
  inherited;
  if Event = deFieldListChange then
    SetLength(FMapFields, 0);
end;

class function TddTable.GetDBType: TevDBType;
begin
  Result := dbtUnknown;
end;

class function TddTable.GetFieldPrecision(FieldName: string): Integer;
begin
  Result := 0;
end;

class function TddTable.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 0);
end;

class function TddTable.ReadOnlyTable: Boolean;
begin
  Result := False;
end;

class function TddTable.GetFieldNameList(const AMode: TevGetFieldNameMode): IisStringList;
var
  L: TddFieldList;
  i: Integer;
  ver: Boolean;
  s: String;
begin
   Result := TisStringList.Create;
   Result.CaseSensitive := False;

   L := GetFieldList;
   Result.Capacity := Length(L);
   for i := Low(L) to High(L) do
   begin
     s := AnsiUpperCase(L[i]);
     if AMode = fmAll then
       Result.Add(s)
     else if (s <> GetTableName + '_NBR') and (s <> 'REC_VERSION') and (s <> 'EFFECTIVE_DATE') and (s <> 'EFFECTIVE_UNTIL') then
     begin
       ver := IsVersionedField(L[i]);
       if (AMode = fmNonVersioned) and not ver or
          (AMode = fmVersioned) and ver then
         Result.Add(s);
     end
   end;

   Result.Capacity := Result.Count;
end;

class function TddTable.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TddTable.IsVersionedField(FieldName: string): Boolean;
begin
  Result := False;
end;


class function TddTable.GetEntityName: String;
begin
  Result := '';
end;

class function TddTable.GetFieldEntityProperty(const FieldName: string): string;
begin
  Result := '';
end;

class function TddTable.GetRefBlobFieldList: TddReferencesDesc;
var
  ParentInfo: TddReferencesDesc;
  i: Integer;
  RefBlobTbl: TddTableClass;
begin
  SetLength(Result, 0);

  RefBlobTbl := GetRefBlobTableClass;
  if RefBlobTbl = nil then
    Exit;

  ParentInfo := GetParentsDesc;

  for i := 0 to High(ParentInfo) do
  begin
    if ParentInfo[i].Table = RefBlobTbl then
    begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := ParentInfo[i];
    end;
  end;
end;

class function TddTable.GetRefBlobTableClass: TddTableClass;
begin
  Result := nil;
end;

class function TddTable.GetParentTable(const AFieldName: String): TddTableClass;
var
  ParentInfo: TddReferencesDesc;
  i, j: Integer;
begin
  Result := nil;
  ParentInfo := GetParentsDesc;

  for i := 0 to High(ParentInfo) do
  begin
    for j := 0 to High(ParentInfo[i].SrcFields) do
      if AnsiSameText(ParentInfo[i].SrcFields[j], AFieldName) then
      begin
        Result :=  ParentInfo[i].Table;
        Exit;
      end;
  end;
end;

procedure TddTable.DoBeforePost;
var
  i, j: Integer;
  RV: TevFieldValuesRec;  
begin
  inherited;
  if Assigned(FCheckValues) then
  begin
    for i := 0 to Fields.Count - 1 do
    begin
      if (Fields[i] is TStringField) and not Fields[i].IsNull then
      begin
        j := FCheckValues.FieldIndex(Fields[i].FieldName);
        if j <> -1 then
        begin
          RV := FCheckValues[j];
          if (Length(RV.Values) > 0) and (Pos(Fields[i].AsString + #13, RV.Values) = 0) then
            if Length(RV.DefValue) > 0 then
              Fields[i].Value := RV.DefValue
            else if Trim(Fields[i].AsString) = '' then
              Fields[i].Clear
          else
            Assert(False, 'Table ' + TableName +  ' (Nbr=' + FieldByName(TableName + '_NBR').AsString + ') Field ' +
              Fields[i].FieldName + ' has unacceptable value "' + Fields[i].AsString + '"');
        end;
      end;
    end;
  end;
end;

class function TddTable.GetEvTableNbr: Integer;
begin
  Result := 0;
end;

class function TddTable.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  Result := 0;
end;


class function TddTable.GetDisplayKey: string;
var
  LK: TddFieldList;
  i: Integer;
begin
  Result := gvDisplayKeys.TryGetValue(GetTableName, '');
  if Result = '' then
  begin
    LK := GetLogicalKeys;
    for i := 0 to High(LK) do
      AddStrValue(Result, '[' + LK[i] + ']', ',');
  end;
end;

class function TddTable.GetDisplayFormat(const AFieldName: string): String;
begin
  Result := gvDisplayFormats.TryGetValue(GetTableName + '.' + AFieldName, '');
end;

{ TStrEntry }

constructor TStrEntry.Create(AOwner: TComponent);
begin
  inherited;
  CustomAccess := False;
end;

class function TStrEntry.GetClientID: Integer;
begin
  Result := -1;
end;

class function TStrEntry.GetData(const ddTableClass: TddTableClass): TddTable;
begin
  Result := ctx_DataAccess.GetDataSet(ddTableClass);

  if GetClientID < 0 then
    Result.ClientID := GetClientID;
end;

function TStrEntry.GetDataSet(const ddTableClass: TddTableClass): TddTable;
begin
  if (csDesigning in ComponentState) and not CustomAccess then
  begin
    Result := nil;
    Exit;
  end;
  Result := GetData(ddTableClass);
end;

procedure TStrEntry.Loaded;
var
  Names: TStrings;
  i: Integer;
begin
  inherited;
  Names := TStringList.Create;
  try
    GetFixupReferenceNames(nil, Names);
    for i := 0 to Pred(Names.Count) do
      if IsPublishedProp(Self, Copy(Names[i], 4, Length(Names[i]))) then
        GetData(TddTableClass(GetTypeData(GetPropInfo(Self.ClassType,  Copy(Names[i], 4, Length(Names[i]))).PropType^).ClassType));
    GlobalFixupReferences;
  finally
    Names.Free;
  end;
end;

procedure TStrEntry.SetName(const NewName: TComponentName);
var
  s: string;
begin
  s := Copy(ClassName, 2, Length(ClassName));
  inherited SetName(s);
end;


procedure TevFieldCodeValues.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    TevFieldValues(Objects[i]).Free;

  inherited;
end;

destructor TevFieldCodeValues.Destroy;
begin
  Clear;
  inherited;
end;

function TevFieldCodeValues.FindFields(const ATable: TddTableClass): TevFieldValues;
begin
  Result := FindFields(Copy(ATable.ClassName, 2, Length(ATable.ClassName)));
end;

function TevFieldCodeValues.FindFields(const ATableName: String): TevFieldValues;
var
  i: Integer;
begin
  i := IndexOf('T' + UpperCase(ATableName));

  if i = - 1 then
    Result := nil
  else
    Result := Items[i];
end;

function TevFieldCodeValues.GetItem(Index: Integer): TevFieldValues;
begin
  Result := TevFieldValues(Objects[Index]);
end;

procedure TevFieldCodeValues.Initialize;
var
  i, j: Integer;
  TF:   TableField;
  Flds: TevFieldValues;
begin
  Sorted := False;
  Clear;

  for i := Low(FieldsToPopulate) to High(FieldsToPopulate) do
  begin
    TF := FieldsToPopulate[i];

    j := IndexOf(UpperCase(TF.T.ClassName));
    if j = -1 then
      j := AddObject(UpperCase(TF.T.ClassName), TevFieldValues.Create);
    Flds := TevFieldValues(Objects[j]);
    Flds.AddField(FieldsToPopulate[i]);
  end;

  Sorted := True;

(*  for i := 0 to Count - 1 do
    TevFieldValues(Objects[i]).Sorted := True;*)
end;



{ TevFieldValues }

procedure TevFieldValues.AddField(const AField: TableField);
var
  F: PTevFieldValuesRec;
  j: Integer;
  h: String;
begin
  New(F);

  h := AField.C;
  if (Length(h) > 0) and (h[Length(h)] <> #13) then
    h := h + #13;

  while Length(h) > 0 do
  begin
    j := Pos(#9, h);
    System.Delete(h, 1, j);
    j := Pos(#13, h);
    F^.Values := F^.Values + Copy(h, 1, j);
    System.Delete(h, 1, j);
  end;

  F^.DefValue := AField.D;
  try
    AddObject(UpperCase(AField.F), Pointer(F));
  except
    on E: Exception do
    begin
      MessageBox(0, PChar(Format('Duplicate field name [%s] table [%s] in array SFieldCodeValues.FieldsToPopulate'#13'%s', [AField.F, AField.T.GetTableName, E.Message])), 'Error', MB_OK + MB_ICONSTOP);
    end;
  end;
end;

procedure TevFieldValues.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Dispose(PTevFieldValuesRec((Objects[i])));

  inherited;
end;

constructor TevFieldValues.Create;
begin
  inherited;
  Duplicates := dupError;
  Sorted := True;
end;

destructor TevFieldValues.Destroy;
begin
  Clear;
  inherited;
end;

function TevFieldValues.FieldIndex(const AField: String): Integer;
begin
  Result := IndexOf(UpperCase(AField));
end;

function TevFieldValues.GetDefaultValue(const AField: String): String;
var
  i: Integer;
begin
  i := FieldIndex(AField);
  if i <> -1 then
    Result := Items[i].DefValue
  else
    Result := '';
end;

function TevFieldValues.GetItem(Index: Integer): TevFieldValuesRec;
begin
  Result := PTevFieldValuesRec((Objects[Index]))^;
end;

{ TddCLDatabase }

class function TddCLDatabase.GetClientID: Integer;
begin
  Result := 0;
end;

{ TddDatabase }

class function TddDatabase.GetTableList: TddTableList;
var
  TypeData: PTypeData;
  PropList: PPropList;
  i: Integer;
  Count: Integer;
begin
  SetLength(Result, 0);
  TypeData := GetTypeData(ClassInfo);
  if TypeData.PropCount <> 0 then
  begin
    GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
    try
      Count := GetPropList(ClassInfo, [tkClass], PropList);
      SetLength(Result, Count);
      for i := 0 to Count - 1 do
        Result[i] := PropList[i]^.Name;
    finally
      FreeMem(PropList);
    end;
  end;
end;

{ TddSYTable }

class function TddSYTable.GetDBType: TevDBType;
begin
  Result := dbtSystem;
end;

procedure TddSYTable.SetClientID(const Value: Integer);
begin
  inherited SetClientID(0);
end;

{ TddSBTable }

class function TddSBTable.GetDBType: TevDBType;
begin
  Result := dbtBureau;
end;

class function TddSBTable.GetRefBlobTableClass: TddTableClass;
begin
  Result := TddTableClass(FindClass('TSB_BLOB'));
end;

procedure TddSBTable.SetClientID(const Value: Integer);
begin
  inherited SetClientID(0);
end;

{ TddTMPTable }

function TddTMPTable.GetAsOfDate: TisDate;
begin
  Result := EmptyDate;
end;

class function TddTMPTable.GetDBType: TevDBType;
begin
  Result := dbtTemporary;
end;

procedure TddTMPTable.SetAsOfDate(const AValue: TisDate);
begin
end;

procedure TddTMPTable.SetClientID(const Value: Integer);
begin
  inherited SetClientID(0);
end;

{ TddClienDBTable }

class function TddClienDBTable.GetDBType: TevDBType;
begin
  Result := dbtClient;
end;

class function TddClienDBTable.GetRefBlobTableClass: TddTableClass;
begin
  Result := TddTableClass(FindClass('TCL_BLOB'));
end;

initialization
  BuildSecuredFieldsList;
  BuildDisplayLabelList;
  BuildDisplayKeys;
  BuildDisplayFormats;

end.


