inherited evVersionedAddress: TevVersionedAddress
  Left = 336
  Top = 384
  ClientHeight = 519
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Height = 340
    inherited grFieldData: TevDBGrid
      Height = 213
    end
    inherited pnlEdit: TevPanel
      Top = 257
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 340
    Width = 791
    Height = 179
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 179
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Address'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lAddress1: TevLabel
        Left = 20
        Top = 35
        Width = 47
        Height = 13
        Caption = 'Address 1'
        FocusControl = edAddress1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lAddress2: TevLabel
        Left = 20
        Top = 74
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = edAddress2
      end
      object lCity: TevLabel
        Left = 21
        Top = 113
        Width = 17
        Height = 13
        Caption = 'City'
        FocusControl = edCity
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lState: TevLabel
        Left = 163
        Top = 113
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = edState
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lZipCode: TevLabel
        Left = 214
        Top = 113
        Width = 15
        Height = 13
        Caption = 'Zip'
        FocusControl = edZipCode
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edAddress1: TevDBEdit
        Left = 20
        Top = 50
        Width = 270
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edAddress2: TevDBEdit
        Left = 20
        Top = 89
        Width = 270
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edCity: TevDBEdit
        Left = 21
        Top = 128
        Width = 134
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edState: TevDBEdit
        Left = 163
        Top = 128
        Width = 43
        Height = 21
        HelpContext = 18002
        CharCase = ecUpperCase
        DataSource = dsFieldData
        Picture.PictureMask = '*{&,@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edZipCode: TevDBEdit
        Left = 214
        Top = 128
        Width = 76
        Height = 21
        HelpContext = 18002
        DataSource = dsFieldData
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
