// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SCEDFields;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, CheckLst,  EvUtils, ISBasicClasses, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, ComCtrls;

type
  TSCEDFields = class(TForm)
    clbFields: TevCheckListBox;
    evBitBtn2: TevBitBtn;
    evBitBtn1: TevBitBtn;
    evcbStartDate: TevCheckBox;
    evFilterOptions: TevGroupBox;
    evrgStartDate: TevRadioButton;
    evrgDateRange: TevRadioButton;
    evStartDate: TevDateTimePicker;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    evdrStartDate: TevDateTimePicker;
    evdrEndDate: TevDateTimePicker;
    procedure clbFieldsClickCheck(Sender: TObject);
    procedure evcbStartDateClick(Sender: TObject);
    procedure evrgStartDateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TSCEDFields.clbFieldsClickCheck(Sender: TObject);
var
  b: Boolean;
  i: Integer;
begin
  b := False;
  for i := 0 to Pred(clbFields.Items.Count) do
    if clbFields.Checked[i] then
    begin
      b := True;
      Break;
    end;
  evBitBtn2.Enabled := b;
end;

procedure TSCEDFields.evcbStartDateClick(Sender: TObject);
begin
 evFilterOptions.Enabled := not evcbStartDate.Checked;
 evrgStartDate.Enabled := evFilterOptions.Enabled;
 evrgDateRange.Enabled := evFilterOptions.Enabled;
 if not evFilterOptions.Enabled then
 begin
   evStartDate.Enabled := evFilterOptions.Enabled;
   evdrStartDate.Enabled := evFilterOptions.Enabled;
   evdrEndDate.Enabled := evFilterOptions.Enabled;
 end
 else
   evrgStartDateClick(Sender); 
end;

procedure TSCEDFields.evrgStartDateClick(Sender: TObject);
begin
   evStartDate.Enabled := evrgStartDate.Checked;
   evdrStartDate.Enabled := evrgDateRange.Checked;
   evdrEndDate.Enabled := evrgDateRange.Checked;
end;

end.
