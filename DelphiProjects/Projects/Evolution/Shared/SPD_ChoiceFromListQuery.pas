// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChoiceFromListQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TChoiceFromListQuery = class(TForm)
    evPanel1: TevPanel;
    dgChoiceList: TevDBGrid;
    btnYes: TevBitBtn;
    btnNo: TevBitBtn;
    dsChoiceList: TevDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

{ TChoiceFromListQuery }


end.
