// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit IEMap;

interface
                                       
uses
  Windows, SysUtils, EvUtils,  Classes, db,
  SFieldCodeValues, EvConsts, SDataStructure, Variants, DateUtils, isBasicUtils,
  SDDClasses, EvDataAccessComponents, StrUtils, EvContext, EvCommonInterfaces,
  EvDataSet, EvTypes, IsBaseClasses, EvStreamUtils, EvQueries, EvClientDataSet,
  EvExchangeConsts;

const
  sPaySuiteEmployeeImport = 'PaySuite Employee Import';
  sPaySuitePayrollImport = 'PaySuite Payroll Import';
  sEvoXImport = 'EvoX';

type
  TStringArray = array of string;
  TVariantArray = array of variant;

  //gdy33
  IIEDefinition = interface
    ['{92F662C9-B221-458A-B83E-C07A7A6802A8}']
    procedure ExecuteAfterInsert( aMethodName: string; const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );
    function ExecuteCustomFunc( aMethodName: string; IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): boolean;
  end;

  IEvoXDefinition = interface(IIEDefinition)
  ['{503AD516-79EB-4B6E-B9CE-EB472EAE6E43}']
    function  GetRowValues: IisListOfValues;
    procedure SetRowValues(const AValue: IisListOfValues);
    function  GetCurrentPackageField: IEvExchangePackageField;
    procedure SetCurrentPackageField(const APackageField: IEvExchangePackageField);
    function  GetCustomWarnings: IisStringList;
    procedure SetCustomWarnings(const ACustomWarnings: IisStringList);
  end;

  IevExchangeChangeRecord = interface
  ['{D9F05559-67AF-4470-9A83-A895B56CF786}']
    function  GetOperation: Char;
    function  GetClientNbr: integer;
    function  GetTableName: String;
    function  GetRecordNbr: integer;
    function  GetAdditionalParams: IisListOfValues;
  end;

  ENotEnoughKeys = class(Exception); //gdy35

  //gdy33
  //inherit your class from TIEDefinitionBase and
  //declare custom functions in 'published' section
  TIEDefinitionBase = class( TInterfacedObject, IIEDefinition )
  private
    function GetMethod(aMethodName: string; out method: TMethod): boolean;
  protected
    {IIEDefinition}
    procedure ExecuteAfterInsert(aMethodName: string; const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );
    function ExecuteCustomFunc(aMethodName: string; IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): boolean;
  published
  end;

  TMapType = (mtDirect, mtLookup, mtMap, mtCustom, mtMapLookup);
  TMapRecord = record
    ITable: string;
    IField: string;
    ETable: string;
    EField: string;
    KeyField: string;
    KeyLookupTable: string;
    KeyLookupField: string;
    KeyValue: string;
    GroupName: string;
    RecordNumber: integer;
    case MapType: TMapType of
      mtCustom: (FuncName: ShortString);
      mtMap: (MapValues, MapDefault: PChar);
      mtLookup: (LookupTable, LookupField, LookupTable1, LookupField1: ShortString);
      mtMapLookup: (MLookupTable, MLookupField: ShortString; MMapValues, MMapDefault: PChar);
  end;

  TUpdateRecord = record
    KeyTables: TStringArray;
    KeyFields: TStringArray;
    KeyValues: TVariantArray;
    Table: string;
    Field: string;
    OldValue: Variant;
    Value: Variant;
    EffectiveDate: TDateTime;
    GroupRecordNumber: integer;
    UpdateAsOfDate: TDateTime;
    NotPostThisRecord: boolean;
  end;
  TUpdateRecordArray = array of TUpdateRecord;

  TEvImportMapRecords = array of IEvImportMapRecord;
  TEvEchangeDefaultsStages = (EvoxStBefore, EvoxStAfter);
  TEvExchangeDefaultValues = procedure(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages) of object;
  TEvExchangeCalcFields = function(const ADataSet: TevClientDataSet): boolean of object;
  TEvExchangeChangeLog = procedure(const AOperation: Char; const AClientNbr: integer; const ATableName: String;
    const ARecordNbr: integer; const APostedParams: IisListOfValues) of object;
  TEvExchangeCheckValuesBeforeChange = procedure(const ATableName: String; const AOperation: char; const AOldValues: TevClientDataSet; const ANewValues: IisListOfValues) of object;

  IIEMap = interface
  ['{0BA9B94E-C09F-4D09-8D78-48F7EDCB51FF}']
    procedure StartMapping(MapArray: array of TMapRecord; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
    procedure StartMappingI(MapArray: TEvImportMaprecords; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
    procedure EndMapping;
    function  MapValue(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                         var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                         out OTables, OFields: TStringArray; out OValues: TVariantArray;
                                         out UsedMapRecord: IEvImportMapRecord; const GroupName: string = ''; NumberInGroup: integer = 0): Boolean;
    function IDS(dsName: string): TevClientDataSet;
    procedure PostUpdates(const UpdateArray: TUpdateRecordArray; const RowMappedValues: IisListOfValues = nil);
    procedure CurrentDataRequired(DS: TevClientDataSet; Condition: string = '1=1');
    procedure GetRequiredKeys(const Table, Field: string; const Value: Variant;
                              const KeyTable, KeyField: string; var KeyValues: TVariantArray);
    procedure RestrictKeyTables( const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );//gdy32
    procedure ClearDataSetsAndVariables;

    function GetListOfVariables: IisListOfValues;
  end;

  TIntegerArray = array of integer;

  TIEMap = class(TisInterfacedObject, IIEMap)
  private
    Map: TEvImportMaprecords;
    IEDef: IIEDefinition;
    dsHolder: TComponent;
    EList: string;
    bDel: Boolean;
    FEvoX: boolean;
    FImportName: String;

    // EvoX staff
    FSharedVariables: IisListOfValues;
    dsHolderForUpdates: TComponent;
    FDefaultsCallback: TEvExchangeDefaultValues;
    FCalcFieldsCallback: TEvExchangeCalcFields;
    FChangeLog: TEvExchangeChangeLog;
    FCheckValuesBeforeChange: TEvExchangeCheckValuesBeforeChange;
  protected
    procedure DoOnConstruction; override;
    procedure RestrictKeyTablesInternal(var kds: TevClientDataSet; var CondFieldArray: TStringArray; var CondValueArray: TIntegerArray; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );
    function  ComputeCalcFields(const ADataSet: TevClientDataSet): boolean;

    // IEMap interface
    procedure StartMapping(MapArray: array of TMapRecord; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
    procedure StartMappingI(MapArray: TEvImportMaprecords; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
    procedure EndMapping;
    function  MapValue(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                         var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                         out OTables, OFields: TStringArray; out OValues: TVariantArray;
                                         out UsedMapRecord: IEvImportMapRecord; const GroupName: string = ''; NumberInGroup: integer = 0): Boolean;
    function  IDS(dsName: string): TevClientDataSet;
    function  IDSForUpdates(dsName: string): TevClientDataSet;
    procedure PostUpdates(const UpdateArray: TUpdateRecordArray; const RowMappedValues: IisListOfValues = nil);
    procedure GetRequiredKeys(const Table, Field: string; const Value: Variant;
                              const KeyTable, KeyField: string; var KeyValues: TVariantArray);
    procedure CurrentDataRequired(DS: TevClientDataSet; Condition: string = AlwaysTrueCond);
    procedure RestrictKeyTables( const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );//gdy32
    procedure ClearDataSetsAndVariables;
    function  GetListOfVariables: IisListOfValues;
    procedure PostFieldValueAsOfDate(AsOfDate: TDatetime; const aTable: string; aKeyValue: integer; aField: TField; FieldValue: variant);
  public
    constructor Create(const AImportName: String = ''; const ADefaultsCallback: TEvExchangeDefaultValues = nil; const AChangeLog: TEvExchangeChangeLog = nil;
      ACalcFields: TEvExchangeCalcFields = nil; ACheckValuesBeforeChange: TEvExchangeCheckValuesBeforeChange = nil); reintroduce;
    destructor Destroy; override;
  end;

  IEMapValue = interface
  ['{0116B8D8-1514-46E3-A0FF-3F175875F7A2}']
    procedure SetUpdateAsOfDate(const Value: TDateTime);
    function  GetUpdateAsOfDate: TDateTime;
    procedure SetValue(const Value: Variant);
    function  GetValue: Variant;
    procedure SetIsMappedValue(const Value: boolean);
    function  GetIsMappedValue: boolean;
    property  Value: Variant read GetValue write SetValue;
    property  UpdateAsOfDate: TDateTime read GetUpdateAsOfDate write SetUpdateAsOfDate;
    property  IsMappedValue: boolean read GetIsMappedValue write SetIsMappedValue;
  end;

  TIEMapValue = class(TisInterfacedObject, IEMapValue)
  private
    FUpdateAsOfDate: TDateTime;
    FValue: Variant;
    FIsMappedValue: boolean;
  protected
    procedure DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    // IEMapValue;
    procedure SetUpdateAsOfDate(const Value: TDateTime);
    function  GetUpdateAsOfDate: TDateTime;
    procedure SetValue(const Value: Variant);
    function  GetValue: Variant;
    procedure SetIsMappedValue(const Value: boolean);
    function  GetIsMappedValue: boolean;
  public
    property Value: Variant read GetValue write SetValue;
    property UpdateAsOfDate: TDateTime read GetUpdateAsOfDate write SetUpdateAsOfDate;
    destructor Destroy; override;
    class function GetTypeID: String; override;
  end;

  function GetIEMapFieldValue(const AVariant: Variant): Variant;

implementation

uses EvImportMapRecord, kbmMemTable, ISKbmMemDataSet, EvClasses;

type
  TBenefitRefRecord = record
    Keys: IisListOfValues;
    Value: Variant;
  end;
  TBenefitRefRecordArray = array of TBenefitRefRecord;

function GetIEMapFieldValue(const AVariant: Variant): Variant;
var
  tmp: IEMapValue;
begin
  tmp := IInterface(AVariant) as IEMapValue;
  Result := tmp.Value;
end;

{ TIEMap }

procedure TIEMap.StartMapping(MapArray: array of TMapRecord; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
var
  tmpMapArray: TevImportMaprecords;
  i: integer;
begin
  SetLength(tmpMapArray, Length(MapArray));
  for i := 0 to High(MapArray) do
    tmpMapArray[i] := TEvImportMapRecord.Create(MapArray[i]);
  StartMappingI(tmpMapArray, aIEDef, bDeleted);
end;

procedure TIEMap.StartMappingI(MapArray: TevImportMaprecords; aIEDef: IIEDefinition = nil; bDeleted: Boolean = True);
var
  i: Integer;
  s: string;
begin
  SetLength(Map, Length(MapArray));
  s := '';
  for i := 0 to High(MapArray) do
  begin
    Map[i] := MapArray[i];
    if Pos('''' + UpperCase(Map[i].ETable) + '''', s) = 0 then
      s := s + '''' + UpperCase(Map[i].ETable) + ''',';
  end;
  EList := Copy(s, 1, Pred(Length(s)));
  IEDef := aIEDef;
  dsHolder := TComponent.Create(nil);
  dsHolderForUpdates := TComponent.Create(nil);
  bDel := bDeleted;
end;

procedure TIEMap.EndMapping;
begin
  IEDef := nil;
  Map := nil;

  FreeAndNil(dsHolder);
  FreeAndNil(dsHolderForUpdates);
  
  EList := '';
end;

//gdy30
function ComposeCond( aKeyField: string; aKeyValue: Variant ): string;
begin
  if VarIsEmpty(aKeyValue) then
    Result := ' (1=1) '
// I'm not sure that this will comletely fix problem with null values as part of COMPOSITE key; Dim?
  else if VarIsNull(aKeyValue) then
    Result := Format( ' (%s is null) ', [aKeyField] )
  else
    Result := Format( ' (%s=''%s'') ', [aKeyField, VarToStr(aKeyValue)] );
end;

procedure AddCond( var aCondFieldArray: TStringArray; var aCondValueArray: TIntegerArray; aField: string; aValue: integer );
begin
  SetLength( aCondFieldArray, Succ(Length(aCondFieldArray)));
  SetLength( aCondValueArray, Succ(Length(aCondValueArray)));
  aCondFieldArray[High(aCondFieldArray)] := aField;
  aCondValueArray[High(aCondValueArray)] := aValue;
end;

//gdy32
procedure TIEMap.RestrictKeyTablesInternal( var kds: TevClientDataSet; var CondFieldArray: TStringArray; var CondValueArray: TIntegerArray; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );
var
  j, k: integer;
  sCond: string;
  UsedTables: array of Boolean;
begin
  SetLength(CondFieldArray, 0);
  SetLength(CondValueArray, 0);
  kds := nil;
  SetLength(UsedTables, Length(KeyTables));

  for j := 0 to High(UsedTables) do
    UsedTables[j] := False;
  for j := 0 to High(KeyTables) do
    if (KeyTables[j] <> '') and (KeyFields[j] <> '') and not UsedTables[j] then
    begin
      kds := IDS(KeyTables[j]);
      sCond := '';
      for k := 0 to High(CondFieldArray) do //gdy
      begin
        if kds.FieldCount = 0 then
          CurrentDataRequired(kds, AlwaysFalseCond);
        if Assigned(kds.FindField(CondFieldArray[k])) then
          sCond := sCond + CondFieldArray[k] + ' = ' + IntToStr(CondValueArray[k]) + ' and ';
      end;
      UsedTables[j] := True;
      sCond := sCond + ComposeCond( KeyFields[j], KeyValues[j] );
      for k := Succ(j) to High(KeyTables) do
        if (KeyTables[k] <> '') and (KeyFields[k] <> '') and not UsedTables[k] and
           (AnsiCompareText(KeyTables[j], KeyTables[k]) = 0) then
        begin
          sCond := sCond + ' and ' + ComposeCond( KeyFields[k], KeyValues[k] );
          UsedTables[k] := True;
        end;
      CurrentDataRequired(kds, sCond);
      AddCond( CondFieldArray, CondValueArray, KeyTables[j] + '_NBR', kds.FieldByName(KeyTables[j] + '_NBR').AsInteger ); //gdy31
    end;
end;

//gdy32
procedure TIEMap.RestrictKeyTables( const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray );
var
  dummyCondFieldArray: TStringArray;
  dummyCondValueArray: TIntegerArray; //gdy31
  dummykds: TevClientDataSet;
begin
  RestrictKeyTablesInternal( dummykds, dummyCondFieldArray, dummyCondValueArray, KeyTables, KeyFields, KeyValues );
end;

function TIEMap.MapValue(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray;
                                     out UsedMapRecord: IEvImportMapRecord; const GroupName: string; NumberInGroup: integer): Boolean;
var
  i, iPos: Integer;
  t: TStringList;
  ds: TevClientDataSet;
  s: string;
  CondFieldArray: TStringArray;
  CondValueArray: TIntegerArray; //gdy31
  kds: TevClientDataSet;
  v, v1: Variant;

  function LookupValue(LookupTable, LookupField: string; LookupValue: Variant; ResultField: string): Variant;
  var
    i, j: Integer;
    s, s1: string;

    function FindValue(FieldName: string): Variant;
    var
      i: Integer;
    begin
      Result := null;
      for i := 0 to High(CondFieldArray) do
        if AnsiCompareText(CondFieldArray[i], FieldName) = 0 then
        begin
          Result := CondValueArray[i];
          Break;
        end;
    end;
  begin
    ds := IDS(LookupTable);
    CurrentDataRequired(ds);
    Result := null;
    if VarIsArray(LookupValue) then
    begin
      s := LookupField;
      LookupField := '';
      j := 0;
      for i := VarArrayLowBound(LookupValue, 1) to VarArrayHighBound(LookupValue, 1) do
      begin
        s1 := GetNextStrValue(s, ';');
        if Assigned(ds.FindField(s1)) and
           (not IsImport or (AnsiCompareText(LookupTable + '_NBR', s1) <> 0)) then
        begin
          LookupField := LookupField + s1 + ';';
          LookupValue[j] := LookupValue[i];
          Inc(j);
        end;
      end;
      LookupField := Copy(LookupField, 1, Pred(Length(LookupField)));
    end;

    if ds.Locate(LookupField, LookupValue, [loCaseInsensitive]) then
    begin
      if Pos(';', ResultField) = 0 then
        Result := ds[ResultField]
      else
      begin
        v := VarArrayCreate([0, -1], varVariant);
        s := ResultField;
        i := 0;
        repeat
          s1 := GetNextStrValue(s, ';');
          VarArrayRedim(v, Succ(i));
          if Assigned(ds.FindField(s1)) then
            Result[i] := ds.FindField(s1).Value
          else
            Result[i] := FindValue(s1);
        until s = '';
      end;
      AddCond( CondFieldArray, CondValueArray, LookupTable + '_NBR', ds[LookupTable + '_NBR'] ); //gdy31
    end;
  end;

  function IsKeyFieldMatch(rec: IEvImportMapRecord): Boolean;
  begin
    Result := True;
    if rec.KeyValue <> '' then
    begin
      ds := IDS(KeyTables[0]);
      CurrentDataRequired(ds, KeyFields[0] + ' = ''' + VarToStr(KeyValues[0]) + '''');
      Result := ds.FieldByName(rec.KeyField).AsString = rec.KeyValue;
    end;
  end;

  function IsSpaceLeadField(const aTable, aField: string): boolean;
  begin
    Result := (aTable = 'EE') and (aField = 'CUSTOM_EMPLOYEE_NUMBER') or
      (
        ((aTable = 'EE') or (aTable = 'EE_RATES') or (aTable = 'PR_CHECK_LINES') or (aTable = 'EE_AUTOLABOR_DISTRIBUTION')) and
        ((aField = 'CO_DIVISION_NBR') or (aField = 'CO_BRANCH_NBR') or
         (aField = 'CO_DEPARTMENT_NBR') or (aField = 'CO_TEAM_NBR'))
      );
  end;
begin
  Result := False;
  UsedMapRecord := nil;

  iPos := -1;
  if NumberInGroup = 0 then // if not mapping Group's field
  begin
    for i := Low(Map) to High(Map) do
      if IsImport and (AnsiCompareText(Map[i].ITable, Table) = 0) and (AnsiCompareText(Map[i].IField, Field) = 0) or
         not IsImport and (AnsiCompareText(Map[i].ETable, Table) = 0) and (AnsiCompareText(Map[i].EField, Field) = 0) and IsKeyFieldMatch(Map[i]) then
      begin
        iPos := i;
        Break;
      end
  end
  else
    for i := High(Map) downto Low(Map) do
      if (IsImport and (AnsiCompareText(Map[i].ITable, Table) = 0) and (AnsiCompareText(Map[i].IField, Field) = 0) or
         not IsImport and (AnsiCompareText(Map[i].ETable, Table) = 0) and (AnsiCompareText(Map[i].EField, Field) = 0) and IsKeyFieldMatch(Map[i]))
         and
         (Map[i].RecordNumber = NumberInGroup) and (Map[i].GroupName = GroupName) then
      begin
        iPos := i;
        Break;
      end;

  if iPos = -1 then
    Exit;

  Result := True;
  UsedMapRecord := Map[iPos];

  for i := 0 to High(KeyValues) do
    if VarToStr(KeyValues[i]) <> '' then
    begin
      if Map[iPos].KeyLookupTable <> '' then
      begin
        if KeyFields[i] = '' then
          KeyFields[i] := Map[iPos].KeyLookupField;
        if KeyTables[i] = '' then
          KeyTables[i] := Map[iPos].KeyLookupTable;
      end
      else
      begin
        if KeyFields[i] = '' then
          KeyFields[i] := Map[iPos].KeyField;
        if KeyTables[i] = '' then
          KeyTables[i] := Map[iPos].ETable;
      end;
    end;

  RestrictKeyTablesInternal( kds, CondFieldArray, CondValueArray, KeyTables, KeyFields, KeyValues ); //gdy32

  if IsImport and (Map[iPos].KeyValue <> '') then
  begin
    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    if Map[iPos].KeyLookupTable <> '' then
    begin
      KeyTables[High(KeyTables)] := Map[iPos].KeyLookupTable;
      KeyFields[High(KeyFields)] := Map[iPos].KeyLookupField;
    end
    else
    begin
      KeyTables[High(KeyTables)] := Map[iPos].ETable;
      KeyFields[High(KeyFields)] := Map[iPos].KeyField;
    end;
    KeyValues[High(KeyValues)] := Map[iPos].KeyValue;
  end
  else if not IsImport and
          (Map[iPos].KeyField <> '') and
          (Map[iPos].KeyValue = '') then
  begin
    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := Map[iPos].ETable;
    KeyFields[High(KeyFields)] := Map[iPos].KeyField;
    if Map[iPos].KeyLookupTable <> '' then
    begin
      CurrentDataRequired(IDS(Map[iPos].KeyLookupTable), Map[iPos].KeyField + ' = ''' + kds.FieldByName(Map[iPos].KeyField).AsString + '''');
      KeyValues[High(KeyValues)] := IDS(Map[iPos].KeyLookupTable).FieldByName(Map[iPos].KeyLookupField).AsString;
    end
    else
      KeyValues[High(KeyValues)] := kds.FieldByName(Map[iPos].KeyField).AsString;
  end;

  SetLength(OTables, 1);
  SetLength(OFields, 1);
  SetLength(OValues, 1);

  // to support NULLs as input values for EvoX
  if FEvoX and (Value = null) then
  begin
    OTables[0] := Map[iPos].ETable;
    OFields[0] := Map[iPos].EField;
    OValues[0] := Value;
    exit;
  end;

  // mapping
  if Map[iPos].MapType = mtpDirect then
  begin
    if IsImport then
    begin
      OTables[0] := Map[iPos].ETable;
      OFields[0] := Map[iPos].EField;
    end
    else
    begin
      OTables[0] := Map[iPos].ITable;
      OFields[0] := Map[iPos].IField;
    end;
    if (OTables[0] = 'EE_STATES') and (OFields[0] = 'SOC_CODE') and not VarIsNull(Value) then
      OValues[0] := StringReplace(Value, '-', '', [])
    else
      OValues[0] := Value;
  end
  else if Map[iPos].MapType = mtpCustom then
  begin
    Result := assigned(IEDef) and IEDef.ExecuteCustomFunc(Map[iPos].FuncName, IsImport, Table, Field, Value, KeyTables, KeyFields, KeyValues, OTables, OFields, OValues);//gdy33
  end
  else if (Map[iPos].MapType = mtpMap) or (Map[iPos].MapType = mtpMapCustom) then
  begin
    if IsImport then
    begin
      OTables[0] := Map[iPos].ETable;
      OFields[0] := Map[iPos].EField;
    end
    else
    begin
      OTables[0] := Map[iPos].ITable;
      OFields[0] := Map[iPos].IField;
    end;

    if Pos('=', Map[iPos].MapValues) > 0 then
    begin
      t := TStringList.Create;
      try
        t. CommaText := Map[iPos].MapValues;
        Result := False;
        OValues[0] := null;
        for i := 0 to Pred(t.Count) do
          if IsImport and ((Copy(t[i], 1, Pred(Pos('=', t[i]))) = Value) or
            IsSpaceLeadField(OTables[0], OFields[0]) and ( Trim(Copy(t[i], 1, Pred(Pos('=', t[i])))) = Trim(Value)) )
          or
            not IsImport and (Copy(t[i], Succ(Pos('=', t[i])), Length(t[i])) = Value) then
          begin
            if IsImport then
            begin
              OValues[0] := Copy(t[i], Succ(Pos('=', t[i])), Length(t[i]));
              if IsSpaceLeadField(OTables[0], OFields[0]) then
              begin
                if OFields[0] = 'CUSTOM_EMPLOYEE_NUMBER' then
                  OValues[0] := PadStringLeft(Trim(OValues[0]), ' ', 9)
                else
                  OValues[0] := PadStringLeft(Trim(OValues[0]), ' ', 20);
              end;
            end
            else
              OValues[0] := Copy(t[i], 1, Pred(Pos('=', t[i])));
            Result := True;
            Break;
          end;
        if (Map[iPos].MapDefault <> '') and VarIsNull(OValues[0]) then
        begin
          OValues[0] := string(Map[iPos].MapDefault);
          Result := True;
        end;
      finally
        t.Free;
      end;
    end
    else if IsImport then
      if Pos(#9 + Value, Map[iPos].MapValues) > 0 then
        OValues[0] := Value
      else if Map[iPos].MapDefault <> '' then
        OValues[0] := string(Map[iPos].MapDefault)
      else
        Result := False
    else
      OValues[0] := Value;

    if Map[iPos].MapType = mtpMapCustom then
    begin
      v1 := OValues[0];
      Result := assigned(IEDef) and IEDef.ExecuteCustomFunc(Map[iPos].FuncName, IsImport, Table, Field, v1, KeyTables, KeyFields, KeyValues, OTables, OFields, OValues);
    end;
  end
  else if Map[iPos].MapType = mtpLookup then
  begin
    if IsImport then
    begin
      OTables[0] := Map[iPos].ETable;
      OFields[0] := Map[iPos].EField;
      s := '';
      v := VarArrayCreate([Low(CondFieldArray), Succ(High(CondFieldArray))], varVariant);
      for i := Low(CondFieldArray) to High(CondFieldArray) do
      begin
        s := s + CondFieldArray[i] + ';';
        v[i] := CondValueArray[i];
      end;
      if Map[iPos].LookupTable1 <> '' then
      begin
        v[VarArrayHighBound(v,1)] := Value; //gdy
        Value := LookupValue(Map[iPos].LookupTable1, s + Map[iPos].LookupField1, v, Map[iPos].LookupField);
      end;
      v[VarArrayHighBound(v,1)] := Value; //gdy
      OValues[0] := LookupValue(Map[iPos].LookupTable, s + Map[iPos].LookupField, v, Map[iPos].LookupTable + '_NBR');
    end
    else
    begin
      OTables[0] := Map[iPos].ITable;
      OFields[0] := Map[iPos].IField;
      if Map[iPos].LookupTable1 <> '' then
      begin
        Value := LookupValue(Map[iPos].LookupTable, Map[iPos].LookupTable + '_NBR', Value, Map[iPos].LookupField);
        if not VarIsNull(Value) then
          OValues[0] := LookupValue(Map[iPos].LookupTable1, Map[iPos].LookupField, Value, Map[iPos].LookupField1)
        else
          OValues[0] := null
      end
      else
        OValues[0] := LookupValue(Map[iPos].LookupTable, Map[iPos].LookupTable + '_NBR', Value, Map[iPos].LookupField);
    end;
  end
  else if Map[iPos].MapType = mtpMapLookup then
  begin
    ds := IDS(Map[iPos].MLookupTable);
    CurrentDataRequired(ds);
    if IsImport then
    begin
      OTables[0] := Map[iPos].ETable;
      OFields[0] := Map[iPos].EField;
      t := TStringList.Create;
      try
        t.CommaText := Map[iPos].MMapValues;
        Result := False;
        OValues[0] := null;
        for i := 0 to Pred(t.Count) do
          if (Copy(t[i], 1, Pred(Pos('=', t[i]))) = Value) or
            ( IsSpaceLeadField(OTables[0], OFields[0]) and ( Trim(Copy(t[i], 1, Pred(Pos('=', t[i])))) = Trim(Value) ) ) then
          with Map[iPos] do
          begin
            if ((ETable = 'EE_SCHEDULED_E_DS') and (EField = 'CO_BENEFIT_SUBTYPE_NBR') or (ETable = 'EE') and (EField = 'ACA_CO_BENEFIT_SUBTYPE_NBR') or
              (ETable = 'EE_BENEFICIARY') and (EField = 'EE_BENEFITS_NBR')) and
              (MLookupTable = 'CO_BENEFITS') and (MLookupField = 'BENEFIT_NAME') then
              OValues[0] := ds.Lookup('CO_NBR;BENEFIT_NAME', VarArrayOf([IDS('CO').FieldValues['CO_NBR'] ,Copy(t[i], Succ(Pos('=', t[i])), Length(t[i]))]), 'CO_BENEFITS_NBR')
            else if IsSpaceLeadField(OTables[0], OFields[0]) then
            begin
              ds.First;
              while not (ds.Eof or ( Trim(ds.FieldByName(MLookupField).AsString) = Trim( Copy(t[i], Succ(Pos('=', t[i])), Length(t[i])) ) )) do
                ds.Next;

              if Trim(ds.FieldByName(MLookupField).AsString) = Trim(Copy(t[i], Succ(Pos('=', t[i])), Length(t[i]))) then
                OValues[0] := ds.FieldByName(EField).Value
              else
                OValues[0] := null;  

              ds.First;
            end
            else
              OValues[0] := ds.Lookup(MLookupField, Copy(t[i], Succ(Pos('=', t[i])), Length(t[i])), EField);

            Result := True;
            Break;
          end;
        if (Map[iPos].MMapDefault <> '') and VarIsNull(OValues[0]) then
        begin
          OValues[0] := ds.Lookup(Map[iPos].MLookupField, string(Map[iPos].MMapDefault), Map[iPos].EField);
          Result := True;
        end;
      finally
        t.Free;
      end;
    end
    else
    begin
      OTables[0] := Map[iPos].ITable;
      OFields[0] := Map[iPos].IField;
      s := VarToStr(ds.Lookup(Map[iPos].EField, Value, Map[iPos].MLookupField));
      t := TStringList.Create;
      try
        t.CommaText := Map[iPos].MMapValues;
        Result := False;
        for i := 0 to Pred(t.Count) do
          if Copy(t[i], Succ(Pos('=', t[i])), Length(t[i])) = s then
          begin
            OValues[0] := Copy(t[i], 1, Pred(Pos('=', t[i])));
            Result := True;
            Break;
          end;
      finally
        t.Free;
      end;
    end;
  end;
end;

procedure TIEMap.PostUpdates(const UpdateArray: TUpdateRecordArray; const RowMappedValues: IisListOfValues = nil);
const
  sElementDeleted = '#DEL#';
var
  i, j, k, l: Integer;
  ds, kds, tds: TevClientDataSet;
  sCond: string;
  CondFieldArray: TStringArray;
  CondValueArray: TIntegerArray; //gdy31

  aExec: array of IExecDSWrapper;
  aTables: TStringArray;
  aKeys: array of Integer;
  aEffDates: array of TDateTime;
  aAsOfDate: array of Boolean;
  aDatasets: array of TevClientDataSet;
  sF, sP: string;
  p: TGetDataParams;
  cdsNbr: TevClientDataSet;
  NewNbr: Integer;
  Cur: Integer;
  b: Boolean;
  d: TDateTime;
  InsPos: Integer;
  UsedTables: array of Boolean;
  tFieldArray: TStringArray;
  tValueArray: TVariantArray;
  tew: IExecDSWrapper;
  tmpFieldValue: IEMapValue;
  HrTraxBenefitsRef: TBenefitRefRecordArray;
  bAllKeysFound: boolean;

  // EvoX variables
  ListOfTablesAndFields: IisStringList;

  function GetIEMapFieldUpdateDate(const AVariant: Variant): TDateTime;
  var
    tmp: IEMapValue;
  begin
    tmp := IInterface(AVariant) as IEMapValue;
    Result := tmp.UpdateAsOfDate;
  end;

  procedure AddtField( fieldname: string; fieldvalue: Variant ); //gdy34
  begin
    if not VarIsEmpty(fieldvalue) then
    begin
      SetLength( tFieldArray, Length(tFieldArray)+1 );
      SetLength( tValueArray, Length(tValueArray)+1 );
      tFieldArray[high(tFieldArray)] := fieldname;
      tValueArray[high(tValueArray)] := fieldvalue;
    end;
  end;

  function GetNewNbr(TableName: string): Integer;
  begin
    Result := ctx_DBAccess.GetGeneratorValue('G_'+TableName, GetddDatabaseByTableName(TableName).GetDBType, 1);
  end;

 //gdy31
  function FindExecForKeyTable( aDs: TevClientDataSet; aTablename: string; const aThisTableKeyFields: array of string; const aThisTableKeyValues: array of Variant; out idx: integer ): boolean;
  var
    k: integer;
    n: Integer;
  begin
//    Assert( parDS.Name = parTablename );
    Result := false;
    for k := Low(aExec) to High(aExec) do
      if AnsiCompareText(aTables[k], aTablename) = 0 then
      begin
        Result := true;
        for n := low(aThisTableKeyFields) to High(aThisTableKeyFields) do
          Result := Result and (aThisTableKeyValues[n] = GetIEMapFieldValue(aExec[k].Params.GetValueByName(aThisTableKeyFields[n])));
        if Result then
          for n := low(CondFieldArray) to High(CondFieldArray) do
            Result := Result and ( not assigned(aDs.FindField(CondFieldArray[n])) or (GetIEMapFieldValue(aExec[k].Params.GetValueByName(CondFieldArray[n])) = CondValueArray[n]));
        if Result then
        begin
          idx := k;
          break;
        end;
      end
  end;

  procedure UpdateRecord;
  var
    l: Integer;
    dsCheck, dsCheckTOAOper: TevClientDataSet;
    FieldValue: Variant;
    Operation: Char;
//    oldUpdateDate: TDateTime;
    newUpdateAsOfDate: TDateTime;
    bDataUpdated: boolean;
    SortIndex: String;
    bTOATable, bEELocalsTable: boolean;

    function CheckFieldInList(const ATable, AField: String): boolean;
    begin
      Result := false;
      if ListOfTablesAndFields.IndexOf(ATable + '.' + AField) <> -1 then
        Result := true;
    end;

    function AddTOAOperInfo(const AItIsInsert: boolean): boolean;
    var
      CurrentValueAccrued, CurrentValueUsed, NewValue: Variant;
      Q: IevQuery;
      MappedFieldPos: integer;

      function FindMappedFieldPos(const AFieldName: String): integer;
      var
        i: integer;
      begin
        Result := -1;
        for i := 0 to RowMappedValues.Count - 1 do
        begin
          if Pos(AFieldName, RowMappedValues.Values[i].Name) > 0 then
          begin
            Result := i;
            exit;
          end;  
        end;
      end;
    begin
      Result := false;
      CurrentValueAccrued := null;
      CurrentValueUsed := null;
      // calculating current values for update
      if not AItIsInsert then
      begin
        Q := TevQuery.Create('TimeOffBalances');
        Q.Macros.AddValue('COND', 'and e.ee_time_off_accrual_nbr= :Nbr');
        Q.Params.AddValue('Nbr', dsCheck['EE_TIME_OFF_ACCRUAL_NBR']);
        Q.Execute;
        CheckCondition(Q.Result.RecordCount > 0, '"TimeOffBalances" returned no records');
        CurrentValueAccrued := Q.Result.FieldValues['accrued'];
        CurrentValueUsed := Q.Result.FieldValues['used'];
      end;
      // Accrued
      NewValue := null;
      if AItIsInsert then
        NewValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CURRENT_ACCRUED'))
      else
      begin
        MappedFieldPos := FindMappedFieldPos(EvoXTOACurrentAccrued);
        if (MappedFieldPos <> -1) and (RowMappedValues.Values[MappedFieldPos].Value <> null) then
          NewValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CURRENT_ACCRUED'));
      end;
      if (AItIsInsert and (NewValue <> 0)) or ((not AItIsInsert) and (NewValue <> null) and (CurrentValueAccrued <> NewValue)) then
      begin
        Result := true;
        dsCheckTOAOper.Append;
        dsCheckTOAOper['EE_TIME_OFF_ACCRUAL_NBR'] :=  dsCheck['EE_TIME_OFF_ACCRUAL_NBR'];
        dsCheckTOAOper['ACCRUAL_DATE'] := Date;
        if AItIsInsert then
          dsCheckTOAOper['ACCRUED'] := NewValue
        else
          dsCheckTOAOper['ACCRUED'] := NewValue - CurrentValueAccrued;
        dsCheckTOAOper['NOTE'] := 'Manual Adj: Evo Exchange Import';
        dsCheckTOAOper['OPERATION_CODE'] := EE_TOA_OPER_CODE_MANUAL_ADJ;
        if dsCheckTOAOper['USED'] = null then
          dsCheckTOAOper['USED'] := 0;
        try
          dsCheckTOAOper.Post;
        except
          dsCheckTOAOper.Cancel;
          raise;
        end;
      end;
      // Used
      NewValue := null;
      if AItIsInsert then
        NewValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CURRENT_USED'))
      else
      begin
        MappedFieldPos := FindMappedFieldPos(EvoXTOACurrentUsed);
        if (MappedFieldPos <> -1) and (RowMappedValues.Values[MappedFieldPos].Value <> null) then
          NewValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CURRENT_USED'));
      end;
      if (AItIsInsert and (NewValue <> 0)) or ((not AItIsInsert) and (NewValue <> null) and (CurrentValueUsed <> NewValue)) then
      begin
        Result := true;
        dsCheckTOAOper.Append;
        dsCheckTOAOper['EE_TIME_OFF_ACCRUAL_NBR'] :=  dsCheck['EE_TIME_OFF_ACCRUAL_NBR'];
        dsCheckTOAOper['ACCRUAL_DATE'] := Date;
        if AItIsInsert then
          dsCheckTOAOper['USED'] := NewValue
        else
          dsCheckTOAOper['USED'] := NewValue - CurrentValueUsed;
        dsCheckTOAOper['NOTE'] := 'Manual Adj: Evo Exchange Import';
        dsCheckTOAOper['OPERATION_CODE'] := EE_TOA_OPER_CODE_MANUAL_ADJ;
        if dsCheckTOAOper['ACCRUED'] = null then
          dsCheckTOAOper['ACCRUED'] := 0;
        try
          dsCheckTOAOper.Post;
        except
          dsCheckTOAOper.Cancel;
          raise;
        end;
      end;
    end;

    procedure CheckSSNIfNeeded;
    var
      dsCl: TevClientDataSet;
    begin
      if (FImportName = sPaySuiteEmployeeImport) or (FImportName = sPaySuitePayrollImport) then
        exit;
        
      if aTables[i] = 'CL_PERSON' then
      begin
        dsCl := IDSForUpdates('CL');
        dsCl.ProviderName := 'CL_PROV';
        dsCl.SkipFieldCheck := True;
        if (not dsCl.Active) or (dsCl.ClientID <> ctx_DataAccess.ClientID) then
        begin
          dsCl.DataRequired('CL_NBR=' + IntToStr(ctx_DataAccess.ClientId));
          CheckCondition(dsCl.RecordCount > 0, 'Client not found. CL_NBR=' + IntToStr(ctx_DataAccess.ClientId));
        end;

        if dsCheck['EIN_OR_SOCIAL_SECURITY_NUMBER'] = GROUP_BOX_SSN  then
        begin
          // SSN
          if dsCl['BLOCK_INVALID_SSN'] = 'Y' then
            CheckSSN(dsCheck['SOCIAL_SECURITY_NUMBER'])
          else
            CheckSSN(dsCheck['SOCIAL_SECURITY_NUMBER'], True);
        end
        else
          CheckEIN(dsCheck['SOCIAL_SECURITY_NUMBER']);
      end;
    end;

    procedure CheckBenefits;
    var
      BenefitsSubtypeNbr, EEBenefitsNbr: Variant;
    begin
      BenefitsSubtypeNbr := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CO_BENEFIT_SUBTYPE_NBR'));
      EEBenefitsNbr := GetIEMapFieldValue(aExec[i].Params.GetValueByName('EE_BENEFITS_NBR'));
      if (BenefitsSubtypeNbr <> null) and (EEBenefitsNbr <> null) then
        raise Exception.Create('You cannot assign values to both "CO_BENEFIT_SUBTYPE_NBR" and "EE_BENEFITS_NBR" fields');

      if Operation = 'U' then
      begin
        if (dsCheck['CO_BENEFIT_SUBTYPE_NBR'] <> null) and (EEBenefitsNbr <> null) then
          raise Exception.Create('You cannot replace existing link to "EE_BENEFITS" by link to "CO_BENEFIT_SUBTYPE"');
        if (dsCheck['EE_BENEFITS_NBR'] <> null) and (BenefitsSubtypeNbr <> null) then
          raise Exception.Create('You cannot replace existing link to "CO_BENEFIT_SUBTYPE" by link to "EE_BENEFITS"');
      end;
    end;

    procedure RecalculateCoHrPositionGradesNbr;
    var
      OldCoHrPositionGradesNbr: Variant;
      CoHrPositionsNbr: Variant;
      sGradeDescription: String;
      Q: IevQuery;
    begin
      if aExec[i].Params.ValueExists('CO_HR_POSITION_GRADES_NBR') then
      begin
        OldCoHrPositionGradesNbr := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CO_HR_POSITION_GRADES_NBR'));
        if OldCoHrPositionGradesNbr <> null then
        begin
          CheckCondition(aExec[i].Params.ValueExists('CO_HR_POSITIONS_NBR'), 'HR position cannot be null, if grade provided');
          CoHrPositionsNbr := GetIEMapFieldValue(aExec[i].Params.GetValueByName('CO_HR_POSITIONS_NBR'));
          CheckCondition(CoHrPositionsNbr <> null, 'HR position cannot be null, if grade provided');
          Q := TevQuery.Create('select DESCRIPTION from CO_HR_SALARY_GRADES where CO_HR_SALARY_GRADES_NBR=:p1');
          Q.Params.AddValue('p1', OldCoHrPositionGradesNbr);
          Q.Execute;
          sGradeDescription := Q.Result['DESCRIPTION'];

          Q := TevQuery.Create('select CO_HR_POSITION_GRADES_NBR from CO_HR_POSITION_GRADES a ' +
            ' where {AsOfNow<a>} and a.CO_HR_SALARY_GRADES_NBR in (select CO_HR_SALARY_GRADES_NBR from CO_HR_SALARY_GRADES b ' +
            ' where {AsOfNow<b>} and b.DESCRIPTION=:grade_description) ' +
            ' and a.CO_HR_POSITIONS_NBR = :position_nbr');
          Q.Params.AddValue('grade_description', sGradeDescription);
          Q.Params.AddValue('position_nbr', CoHrPositionsNbr);
          Q.Execute;

          CheckCondition(Q.Result.RecordCount > 0, 'Pair of Grade and Position were not found in "CO_HR_POSITION_GRADES" table');
          CheckCOndition(Q.Result.RecordCount = 1, 'Not unique pair of Grade and Position found in "CO_HR_POSITION_GRADES" table');
          aExec[i].Params.GetValueByName('CO_HR_POSITION_GRADES_NBR').Value := Q.Result['CO_HR_POSITION_GRADES_NBR'];
        end;
      end;
    end;

    procedure SetDeductAccordingCoLocals;
    var
      Q: IevQuery;
    begin
      Q := TevQuery.Create('select DEDUCT from CO_LOCAL_TAX a ' +
        ' where {AsOfNow<a>} and a.CO_LOCAL_TAX_NBR = :p1');
      Q.Params.AddValue('p1', dsCheck['CO_LOCAL_TAX_NBR']);
      Q.Execute;
      CheckCondition(Q.Result.RecordCount = 1, 'Cannot find record in CO_LOCAL_TAX table. CO_LOCAL_TAX_NBR=' + VarToStr(dsCheck['CO_LOCAL_TAX_NBR']));
      if (Q.Result['DEDUCT'] <> null) and (Q.Result['DEDUCT'] <> dsCheck['DEDUCT']) then
        dsCheck['DEDUCT'] := Q.Result['DEDUCT'];
    end;

    procedure StoreFieldInDataset(const AFIeld: TField; const ANewValue: Variant);
    var
      s: String;
      BlobData: IisStream;
    begin
      if AFIeld.DataType in [ftBlob, ftMemo] then
      begin
        if (ANewValue = null) or (ANewValue = '') then
          AFIeld.Clear
        else
        begin
          BlobData := TEvDualStreamHolder.Create;
          s := ANewValue;
          BlobData.WriteBuffer(s[1], Length(s));
          BlobData.Position := 0;
          TBlobField(AFIeld).LoadFromStream(BlobData.RealStream);
        end;
      end
      else
        AField.Value := ANewValue;
    end;

    procedure AddDS(aDS: TevClientDataSet);
    var
      i: integer;
      bExists: boolean;
    begin
      i := Low(aDatasets);
      bExists := False;
      while not bExists and (i <= High(aDatasets)) do
      begin
        if aDatasets[i].Name = aDS.Name then
          bExists := True
        else
          Inc(i);
      end;

      if not bExists then
      begin
        SetLength(aDatasets, Succ(Length(aDatasets)));
        aDatasets[High(aDatasets)] := aDS;
      end;  
    end;

  var ssf: string;

  begin
    bTOATable := false;

    dsCheck := IDSForUpdates(aTables[i]);
    dsCheck.ProviderName := aTables[i] + '_PROV';
    dsCheck.SkipFieldCheck := True;
    if (not dsCheck.Active) or (dsCheck.ClientID <> ctx_DataAccess.ClientID) then
    begin
// ticket 100099 
// fix for inability to perform EvoX import mixed import for companies belonging to different clients
      if (dsCheck.ClientID <> ctx_DataAccess.ClientID) then
        dsCheck.ClientID := ctx_DataAccess.ClientID;
// end of ticket 100099

      dsCheck.DataRequired();
      SortIndex := aTables[i] + '_NBR_INDEX';
      (dsCheck as TkbmCustomMemTable).AddIndex(SortIndex, aTables[i] + '_NBR', []);
      (dsCheck as TkbmCustomMemTable).IndexDefs.Update;
      (dsCheck as TkbmCustomMemTable).IndexName := SortIndex;
    end;

    if aTables[i] = 'EE_TIME_OFF_ACCRUAL' then
    begin
      bTOATable := true;
      dsCheckTOAOper := IDSForUpdates('EE_TIME_OFF_ACCRUAL_OPER');
      dsCheckTOAOper.ProviderName := 'EE_TIME_OFF_ACCRUAL_OPER_PROV';
      dsCheckTOAOper.SkipFieldCheck := True;
      if (not dsCheckTOAOper.Active) or (dsCheckTOAOper.ClientID <> ctx_DataAccess.ClientID) then
        dsCheckTOAOper.DataRequired('1=2');
    end;

    if (aTables[i] = 'EE_RATES') and (not FEvoX) then
      RecalculateCoHrPositionGradesNbr;

    bEELocalsTable := aTables[i] = 'EE_LOCALS';

    if dsCheck.FindKey([GetIEMapFieldValue(aExec[i].Params.GetValueByName(aTables[i] + '_NBR'))]) then
      Operation := 'U'
    else
      Operation := 'I';

    if aTables[i] = 'EE_SCHEDULED_E_DS' then
      CheckBenefits;

    if FEvoX then
      FCheckValuesBeforeChange(aTables[i], Operation, dsCheck, aExec[i].Params);

    if Operation = 'U' then
    begin
      // update
      if FEvoX then
      begin
        // EvoX
        if bTOATable and AddTOAOperInfo(false) then // during updates do not change TIME_OFF_ACCRUAL table at all
          AddDS(dsCheckTOAOper);

        // fields with newUpdateAsOfDate = 0
        bDataUpdated := false;
        for l := 0 to Pred(dsCheck.FieldCount) do
        begin
          FieldValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
          newUpdateAsOfDate := GetIEMapFieldUpdateDate(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
          if newUpdateAsOfDate = 0 then
          begin
            if (dsCheck.Fields[l].Value <> FieldValue) and CheckFieldInList(ATables[i], dsCheck.Fields[l].FieldName) then
            begin
              if not bTOATable then
              begin
                dsCheck.Edit;
                try
                  StoreFieldInDataset(dsCheck.Fields[l], FieldValue);
                  dsCheck.Post;
                except
                  dsCheck.Cancel;
                  raise;
                end;
                bDataUpdated := true;
              end
              else
              begin
                if (dsCheck.Fields[l].FieldName <> 'CURRENT_USED') and
                (dsCheck.Fields[l].FieldName <> 'CURRENT_ACCRUED') then
                begin
                  dsCheck.Edit;
                  try
                    StoreFieldInDataset(dsCheck.Fields[l], FieldValue);
                    dsCheck.Post;
                  except
                    dsCheck.Cancel;
                    raise;
                  end;
                  bDataUpdated := true;
                end;
              end;
            end;
          end; // if newUpdateAsOfDate = 0
        end;
        bDataUpdated := ComputeCalcFields(dsCheck) or bDataUpdated; // order is very important!
        if bDataUpdated then
        begin
          CheckSSNIfNeeded;
          AddDS(dsCheck);
        end;

        // fields with newUpdateAsOfDate <> 0
        for l := 0 to Pred(dsCheck.FieldCount) do
        begin
          FieldValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
          newUpdateAsOfDate := GetIEMapFieldUpdateDate(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
          if (newUpdateAsOfDate <> 0) and CheckFieldInList(ATables[i], dsCheck.Fields[l].FieldName) then
          begin
            CheckCondition(dsCheck.Locate(aTables[i] + '_NBR', GetIEMapFieldValue(aExec[i].Params.GetValueByName(aTables[i] + '_NBR')), []),
              'Cannot find record for update. ' + aTables[i] + '_NBR=' + IntToStr(GetIEMapFieldValue(aExec[i].Params.GetValueByName(aTables[i] + '_NBR'))));

            PostFieldValueAsOfDate(newUpdateAsOfDate, aTables[i], GetIEMapFieldValue(aExec[i].Params.GetValueByName(aTables[i] + '_NBR')), dsCheck.Fields[l], FieldValue);
          end;
        end;  // for l
      end  // if EvoX
      else
      begin
        // not EvoX
        dsCheck.Edit;
        for l := 0 to Pred(dsCheck.FieldCount) do
          dsCheck.Fields[l].Value := GetIEMapFieldValue(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
        try
          dsCheck.Post;
        except
          dsCheck.Cancel;
          raise;
        end;
        CheckSSNIfNeeded;
        AddDS(dsCheck);
      end;
    end
    else
    begin
      // insert
      dsCheck.Append;
      if FEvoX then
      begin
        // EvoX
        CheckCondition(Assigned(FDefaultsCallback), 'Iternal error. Callback for calculation defaults are not assigned');
        FDefaultsCallback(dsCheck, EvoxStBefore);
        for l := 0 to Pred(dsCheck.FieldCount) do
        begin
          ssf := dsCheck.Fields[l].FieldName;

          if (bTOATable and ((ssf = 'CURRENT_ACCRUED') or (ssf = 'CURRENT_USED'))) then
            // skip these fields for TIME_OFF_ACCRUAL_TABLE, let they stay defaulted to zero for inserting
            continue;

          if ((aTables[i] = 'EE') and ((ssf = 'SELFSERVE_ENABLED') or (ssf = 'TIME_OFF_ENABLED') or (ssf = 'BENEFITS_ENABLED') or (ssf = 'ACA_STATUS') or (ssf = 'BENEFITS_ELIGIBLE')) and
            ((not aExec[i].Params.ValueExists(ssf)) or (not (IInterface(aExec[i].Params.GetValueByName(ssf)) as IEMapValue).IsMappedValue))) then
            // ticket #103480, when these fields are not mapped the default values are taken from Company Settings, see TEvExchangeDefCalculator.InitEEForCompany
            continue;

          FieldValue := GetIEMapFieldValue(aExec[i].Params.GetValueByName(ssf));
          newUpdateAsOfDate := GetIEMapFieldUpdateDate(aExec[i].Params.GetValueByName(ssf));
          CheckCondition(newUpdateAsOfDate = 0, 'Field: ' + ssf + '. AsOfDate is set. It is not allowed for new records');

          if FieldValue <> null then
            StoreFieldInDataset(dsCheck.Fields[l], FieldValue);
        end;
        FDefaultsCallback(dsCheck, EvoxStAfter);

        if bEELocalsTable then
          if (not aExec[i].Params.ValueExists('DEDUCT')) or (not (IInterface(aExec[i].Params.GetValueByName('DEDUCT')) as IEMapValue).IsMappedValue) then
            SetDeductAccordingCoLocals;

        try
          dsCheck.Post;
        except
          dsCheck.Cancel;
          raise;
        end;
        ComputeCalcFields(dsCheck);

        CheckSSNIfNeeded;
        AddDS(dsCheck);
        if bTOATable and AddTOAOperInfo(true) then
          AddDS(dsCheckTOAOper);
      end
      else
      begin
        // not EvoX
        for l := 0 to Pred(dsCheck.FieldCount) do
          dsCheck.Fields[l].Value := GetIEMapFieldValue(aExec[i].Params.GetValueByName(dsCheck.Fields[l].FieldName));
        try
          dsCheck.Post;
        except
          dsCheck.Cancel;
          raise;
        end;
        CheckSSNIfNeeded;
        AddDS(dsCheck);
      end;
    end;
    if Assigned(FChangeLog) then
      FChangeLog(Operation, ctx_DataAccess.ClientId, aTables[i], GetIEMapFieldValue(aExec[i].Params.GetValueByName(aTables[i] + '_NBR')), aExec[i].Params);
  end;

var
  execIdx: integer;
  dNow: TDateTime;

  //for TMP_* updating
  lookup_source_table: TevClientDataSet;
  lookup_source: string;
  lookup_key: string;
  tmpValue : Variant;

  benefit_rate_name: string;
  s : String;
  Q : IevQuery;
  EDSNbrs: IisListOfValues;
label
  n1;
begin
  if FEvoX then
    CheckCondition(Assigned(RowMappedValues), 'Internal error. List of mapped values is not assigned');

  EDSNbrs := TisListOfValues.Create;
  dNow := SysTime;

  ListOfTablesAndFields := TisStringList.Create;

  SetLength(HrTraxBenefitsRef, 0);

  // trick in order to support CO_BENEFITS in EE_SCHEDULED_ED for Generic HR import
  // we are passing value for not existing field EE_SCHEDULED_E_DS.CO_BENEFITS_NBR
  // so we have to store it somewhere and don't post
  if not FEvoX then
    for i := High(UpdateArray) downto Low(UpdateArray) do
    begin
      if (UpdateArray[i].Table = 'EE_SCHEDULED_E_DS') and (UpdateArray[i].Field = 'CO_BENEFITS_NBR') then
      begin
        SetLength(HrTraxBenefitsRef, Succ(Length(HrTraxBenefitsRef)));
        HrTraxBenefitsRef[High(HrTraxBenefitsRef)].Keys := TisListOfValues.Create;
        for j := Low(UpdateArray[i].KeyTables) to High(UpdateArray[i].KeyTables) do
          HrTraxBenefitsRef[High(HrTraxBenefitsRef)].Keys.AddValue(UpdateArray[i].KeyTables[j] + '.' +
            UpdateArray[i].KeyFields[j], UpdateArray[i].KeyValues[j]);
        HrTraxBenefitsRef[High(HrTraxBenefitsRef)].Value := UpdateArray[i].Value;
        UpdateArray[i].Table := sElementDeleted;
      end;
    end;

  for i := Low(UpdateArray) to High(UpdateArray) do
  begin
    if UpdateArray[i].Table <> sElementDeleted then
    begin
      s := UpdateArray[i].Table + '.' + UpdateArray[i].Field;
      if ListOfTablesAndFields.IndexOf(s) = -1 then
        ListOfTablesAndFields.Add(s);
    end;
  end;

  // workaround in order to support CO_BENEFITS in EE_SCHEDULED_ED for Generic HR import
  // implementing lookup for EE_SCHEDULED_E_DS.CO_BENEFIT_SUBTYPE_NBR using stored value of EE_SCHEDULED_E_DS.CO_BENEFITS_NBR
  if not FEvoX then
    for i := Low(UpdateArray) to High(UpdateArray) do
    begin
      if (UpdateArray[i].Table <> sElementDeleted) and (UpdateArray[i].Table = 'EE_SCHEDULED_E_DS')
        and (UpdateArray[i].Field = 'CO_BENEFIT_SUBTYPE_NBR') and (UpdateArray[i].Value <> null) then
      begin
        bAllKeysFound := False;
        for j := Low(HrTraxBenefitsRef) to High(HrTraxBenefitsRef) do
        begin
          bAllKeysFound := True;
          for k := Low(UpdateArray[i].KeyTables) to High(UpdateArray[i].KeyTables) do
          begin
            if (not HrTraxBenefitsRef[j].Keys.ValueExists(UpdateArray[i].KeyTables[k] + '.' + UpdateArray[i].KeyFields[k])) or
              (HrTraxBenefitsRef[j].Keys.Value[UpdateArray[i].KeyTables[k] + '.' + UpdateArray[i].KeyFields[k]] <> UpdateArray[i].KeyValues[k]) then
            begin
              bAllKeysFound := false;
              break;
            end;
          end; // for k
          if bAllKeysFound then
          begin
            CurrentDataRequired(IDS('CO_BENEFIT_SUBTYPE'), 'DESCRIPTION=''' + VarToStr(UpdateArray[i].Value) +
              ''' and CO_BENEFITS_NBR=' + VarToStr(HrTraxBenefitsRef[j].Value));
            UpdateArray[i].Value := IDS('CO_BENEFIT_SUBTYPE').FieldValues['CO_BENEFIT_SUBTYPE_NBR'];
            CheckCondition(UpdateArray[i].Value <> null, 'Cannot find CO_BENEFIT_SUBTYPE_NBR for DESCRIPTION="' + VarToStr(UpdateArray[i].Value) +
              '" and CO_BENEFITS_NBR=' + VarToStr(HrTraxBenefitsRef[j].Value));
          end;
        end;  // for j
        if not bAllKeysFound then
          raise Exception.Create('Not found CO_BENEFITS_NBR for "ED BENEFIT AMOUNT TYPE":' + UpdateArray[i].Value);
      end;
    end;  // for i

  for i := Low(UpdateArray) to High(UpdateArray) do
  begin
    if UpdateArray[i].Table <> sElementDeleted then
    begin
      SetLength(CondFieldArray, 0);
      SetLength(CondValueArray, 0);
      SetLength(UsedTables, Length(UpdateArray[i].KeyTables));

//PLYMOUTH:TODO
{
      if (CompanyAsOfDate <> 0) and
         ((LeftStr(UpdateArray[i].Table, 2) = 'CO') or
          (LeftStr(UpdateArray[i].Table, 2) = 'EE') or
          (LeftStr(UpdateArray[i].Table, 2) = 'CL')) then
        UpdateArray[i].EffectiveDate := CompanyAsOfDate;
}
      for j := 0 to High(UsedTables) do
        UsedTables[j] := False;

      for j := 0 to High(UpdateArray[i].KeyTables) do
        if not UsedTables[j] then
        begin
          kds := IDS(UpdateArray[i].KeyTables[j]);
          sCond := '';
          for k := 0 to High(CondFieldArray) do //gdy
          begin
            if kds.FieldCount = 0 then
              CurrentDataRequired(kds, AlwaysFalseCond);
            if Assigned(kds.FindField(CondFieldArray[k])) then //gdy
              sCond := sCond + CondFieldArray[k] + ' = ' + IntToStr(CondValueArray[k]) + ' and ';
          end;
          UsedTables[j] := True;

          sCond := sCond + ComposeCond( UpdateArray[i].KeyFields[j], UpdateArray[i].KeyValues[j] );
          SetLength(tFieldArray, 0); //gdy34
          SetLength(tValueArray, 0); //gdy34
          AddtField( UpdateArray[i].KeyFields[j], UpdateArray[i].KeyValues[j] ); //gdy34
          for k := Succ(j) to High(UpdateArray[i].KeyTables) do
            if not UsedTables[k] and
               (AnsiCompareText(UpdateArray[i].KeyTables[j], UpdateArray[i].KeyTables[k]) = 0) then
            begin
              AddtField( UpdateArray[i].KeyFields[k], UpdateArray[i].KeyValues[k] ); //gdy34
              sCond := sCond + ' and ' + ComposeCond( UpdateArray[i].KeyFields[k], UpdateArray[i].KeyValues[k] );
              UsedTables[k] := True;
            end;
          CurrentDataRequired(kds, sCond);

          if (kds.RecordCount > 1) and (kds.Name = 'EE_SCHEDULED_E_DS') then
          begin
            sCond := sCond + ' and (EFFECTIVE_END_DATE is null)';
            CurrentDataRequired(kds, sCond);
          end;

          if not bDel and (kds.RecordCount > 1) then
            raise ENotEnoughKeys.CreateFmt( 'Cannot uniquely identify record in %s using condition <%s>', [kds.Name, sCond] );
          if kds.RecordCount > 0 then
            AddCond( CondFieldArray, CondValueArray, UpdateArray[i].KeyTables[j] + '_NBR', kds.FieldByName(UpdateArray[i].KeyTables[j] + '_NBR').AsInteger ) //gdy31
          else
          begin
            if FindExecForKeyTable( kds, UpdateArray[i].KeyTables[j], tFieldArray, tValueArray, execIdx ) then //gdy31
            begin
              AddCond( CondFieldArray, CondValueArray, aTables[execIdx] + '_NBR', GetIEMapFieldValue(aExec[execIdx].Params.GetValueByName(aTables[execIdx] + '_NBR')) ); //gdy31
              goto n1;
            end;
            kds.Insert;
            for k := 0 to Pred(kds.FieldCount) do
              for l := Low(FieldsToPopulate) to High(FieldsToPopulate) do
                if FieldsToPopulate[l].T.ClassName = 'T' + UpdateArray[i].KeyTables[j] then
                  if FieldsToPopulate[l].F = kds.Fields[k].FieldName then
                    if Length(FieldsToPopulate[l].D) > 0 then
                      kds.Fields[k].Value := FieldsToPopulate[l].D;

            //gdy33
            if assigned(IEDef) then
              IEDef.ExecuteAfterInsert( 'AI_' + UpperCase(UpdateArray[i].KeyTables[j]), kds, UpdateArray[i].KeyTables, UpdateArray[i].KeyFields, UpdateArray[i].KeyValues );

            SetLength(aExec, Succ(Length(aExec)));
            SetLength(aTables, Succ(Length(aTables)));
            SetLength(aKeys, Succ(Length(aKeys)));
            SetLength(aEffDates, Succ(Length(aEffDates)));
            SetLength(aAsOfDate, Succ(Length(aAsOfDate)));
            aExec[High(aExec)] := TExecDSWrapper.Create(';GenericInsert');
            aTables[High(aTables)] := UpdateArray[i].KeyTables[j];
            aEffDates[High(aEffDates)] := UpdateArray[i].EffectiveDate;
            aAsOfDate[High(aAsOfDate)] := False;
            sF := '';
            sP := '';
            with aExec[High(aExec)] do
            begin
              for k := 0 to Pred(kds.FieldCount) do
              begin
                sF := sF + kds.Fields[k].FieldName + ',';
                sP := sP + ':' + kds.Fields[k].FieldName + ',';
                tmpFieldValue := TIEMapValue.Create;
                tmpFieldValue.Value := kds.Fields[k].Value;
                Params.AddValue(kds.Fields[k].FieldName, tmpFieldValue);
              end;
              sF := Copy(sF, 1, Pred(Length(sF)));
              sP := Copy(sP, 1, Pred(Length(sP)));
              Macros.AddValue('TableName', UpdateArray[i].KeyTables[j]);
              Macros.AddValue('FieldList', sF);
              Macros.AddValue('ParamList', sP);
              for k := 0 to High(tFieldArray) do
              begin
                (IInterface(Params.Value[tFieldArray[k]]) as IEMapValue).Value := tValueArray[k];
                kds[tFieldArray[k]] := tValueArray[k];
              end;
              NewNbr := GetNewNbr(UpdateArray[i].KeyTables[j]);
              tmpFieldValue := TIEMapValue.Create;
              tmpFieldValue.Value := NewNbr;
              Params.AddValue(UpdateArray[i].KeyTables[j] + '_NBR', tmpFieldValue);
              kds[UpdateArray[i].KeyTables[j] + '_NBR'] := NewNbr;

              tmpFieldValue := TIEMapValue.Create;
              tmpFieldValue.Value := UpdateArray[i].EffectiveDate;
              Params.AddValue('EFFECTIVE_DATE', tmpFieldValue);

              tmpFieldValue := TIEMapValue.Create;
              tmpFieldValue.Value := dNow;
              Params.AddValue('CREATION_DATE', tmpFieldValue);

              tmpFieldValue := TIEMapValue.Create;
              tmpFieldValue.Value := Context.UserAccount.InternalNBR;
              Params.AddValue('CHANGED_BY', tmpFieldValue);

              tmpFieldValue := TIEMapValue.Create;
              tmpFieldValue.Value := Null;
              Params.AddValue('ACTIVE_RECORD', tmpFieldValue);

              AddCond( CondFieldArray, CondValueArray, UpdateArray[i].KeyTables[j] + '_NBR', GetIEMapFieldValue(Params.GetValueByName(UpdateArray[i].KeyTables[j] + '_NBR')) ); //gdy31
              aKeys[High(aKeys)] := CondValueArray[High(CondValueArray)];
              for k := 0 to High(CondValueArray) do
                if Assigned(kds.FindField(CondFieldArray[k])) then
                begin
                  kds[CondFieldArray[k]] := CondValueArray[k];
                  (IInterface(Params.Value[CondFieldArray[k]]) as IEMapValue).Value :=CondValueArray[k];
                end;
            end;
            kds.Post;
          n1:
          end;
        end;

      sCond := '';
      ds := IDS(UpdateArray[i].Table);
      for k := 0 to High(CondFieldArray) do//gdy
      begin
        if ds.FieldCount = 0 then
          CurrentDataRequired(ds, AlwaysFalseCond);
        if Assigned(ds.FindField(CondFieldArray[k])) then //gdy
        begin
          if sCond <> '' then
            sCond := sCond + ' and ';
          sCond := sCond + CondFieldArray[k] + ' = ' + IntToStr(CondValueArray[k]);
        end;
      end;

      if (UpperCase(UpdateArray[i].Table) = 'EE_SCHEDULED_E_DS') and (sCond <> '') then
      begin
        if not EDSNbrs.ValueExists(sCond) then
        begin
         s := 'select EE_SCHEDULED_E_DS_NBR from EE_SCHEDULED_E_DS a where ' + sCond +
               ' and {AsOfNow<a>} ' +
               ' order by a.EFFECTIVE_START_DATE desc ';
         Q := TevQuery.Create(s);
         Q.Execute;
         if (Q.Result.RecordCount > 1) and (Q.Result.Fields[0].AsInteger > 0) then
           begin
             EDSNbrs.AddValue(sCond, IntToStr(Q.Result.Fields[0].AsInteger));
             sCond := sCond + ' and EE_SCHEDULED_E_DS_NBR = ' + IntToStr(Q.Result.Fields[0].AsInteger);
           end
           else
           EDSNbrs.AddValue(sCond, '0');
        end
        else
          if EDSNbrs.Value[sCond] <> '0' then
           sCond := sCond + ' and EE_SCHEDULED_E_DS_NBR = ' + EDSNbrs.Value[sCond];
      end;

      Cur := -1;
      NewNbr := 0;
      InsPos := -1;
      if sCond = '' then
        for j := 0 to High(UpdateArray[i].KeyTables) do
        begin
          kds := IDS(UpdateArray[i].KeyTables[j]);
          if Assigned(kds.FindField(UpdateArray[i].Table + '_NBR')) then
          begin
            if kds.FindField(UpdateArray[i].Table + '_NBR').IsNull then
            begin
              NewNbr := GetNewNbr(UpdateArray[i].Table);
              kds.Edit;
              kds[UpdateArray[i].Table + '_NBR'] := NewNbr;
              kds.Post;
              for l := 0 to High(aExec) do
                if (AnsiCompareText(aTables[l], kds.Name) = 0) and
                   (aKeys[l] = kds[kds.Name + '_NBR']) and
                   (aEffDates[l] = UpdateArray[i].EffectiveDate) then
                begin
                  tmpFieldValue := TIEMapValue.Create;
                  tmpFieldValue.Value := NewNbr;
                  aExec[l].Params.AddValue(UpdateArray[i].Table + '_NBR', tmpFieldValue);
                  InsPos := l;
                  Break;
                end;
            end
            else
              NewNbr := kds.FindField(UpdateArray[i].Table + '_NBR').AsInteger;
            CurrentDataRequired(ds, UpdateArray[i].Table + '_NBR = ''' + IntToStr(NewNbr) + '''');
            for l := 0 to High(aExec) do
              if (AnsiCompareText(aTables[l], UpdateArray[i].Table) = 0) and
                 (aKeys[l] = NewNbr) and
                 (aEffDates[l] = UpdateArray[i].EffectiveDate) then
              begin
                Cur := l;
                Break;
              end;
            Break;
          end;
        end
      else
      begin
        CurrentDataRequired(ds, sCond);
        if ds.RecordCount = 0 then
        begin
          for l := 0 to High(aExec) do
          begin
            b := True;
            for k := 0 to High(CondFieldArray) do
            begin
              if  aExec[l].Params.ValueExists(CondFieldArray[k]) then
                tmpValue := GetIEMapFieldValue(aExec[l].Params.GetValueByName(CondFieldArray[k]))
              else
                tmpValue := null;
              b := b and (not Assigned(ds.FindField(CondFieldArray[k])) or
                          tmpValue = CondValueArray[k]);
            end;
            if (AnsiCompareText(aTables[l], UpdateArray[i].Table) = 0) and
               b and
               (aEffDates[l] = UpdateArray[i].EffectiveDate) then
            begin
              Cur := l;
              Break;
            end;
          end;
          if Cur = -1 then
            NewNbr := GetNewNbr(UpdateArray[i].Table)
          else
            NewNbr := aKeys[Cur];
        end
        else
        begin
          for l := 0 to High(aExec) do
            if (AnsiCompareText(aTables[l], UpdateArray[i].Table) = 0) and
               (aKeys[l] = ds.FieldByName(UpdateArray[i].Table + '_NBR').AsInteger) and
               (aEffDates[l] = UpdateArray[i].EffectiveDate) then
            begin
              Cur := l;
              Break;
            end;
          NewNbr := ds.FieldByName(UpdateArray[i].Table + '_NBR').AsInteger;
        end;
      end;

      if Cur = -1 then
      begin
        SetLength(aExec, Succ(Length(aExec)));
        SetLength(aTables, Succ(Length(aTables)));
        SetLength(aKeys, Succ(Length(aKeys)));
        SetLength(aEffDates, Succ(Length(aEffDates)));
        SetLength(aAsOfDate, Succ(Length(aAsOfDate)));
        if InsPos = -1 then
          InsPos := High(aExec)
        else
          for k := High(aExec) downto Succ(InsPos) do
          begin
            aExec[k] := aExec[Pred(k)];
            aTables[k] := aTables[Pred(k)];
            aEffDates[k] := aEffDates[Pred(k)];
            aKeys[k] := aKeys[Pred(k)];
            aAsOfDate[k] := aAsOfDate[Pred(k)];
          end;
        aExec[InsPos] := TExecDSWrapper.Create(';GenericInsert');
        aTables[InsPos] := UpdateArray[i].Table;
        aKeys[InsPos] := NewNbr;
        aEffDates[InsPos] := UpdateArray[i].EffectiveDate;
        aAsOfDate[InsPos] := False;
        sF := '';
        sP := '';
        with aExec[InsPos] do
        begin
          if Round(ds.FieldByName('EFFECTIVE_DATE').AsDateTime * 86400) > Round(UpdateArray[i].EffectiveDate * 86400) then
          begin
            aAsOfDate[InsPos] := True;
            tds := nil;
            for k := 0 to Pred(dsHolder.ComponentCount) do
              if AnsiCompareText(dsHolder.Components[k].Name, 'ASOFDATE') = 0 then
              begin
                tds := dsHolder.Components[k] as TevClientDataSet;
                Break;
              end;

            if not Assigned(tds) then
            begin
              tds := TevClientDataSet.Create(dsHolder);
              tds.Name := 'ASOFDATE';
              tds.ProviderName := ds.ProviderName;
            end;

            p := TGetDataParams.Create;
            try
              with TExecDSWrapper.Create('SelectLastChangeNBR') do
              begin
                SetMacro('Columns', '*');
                SetMacro('TableName', ds.Name);
                d := UpdateArray[i].EffectiveDate + 1/86400;
                SetParam('EffectiveDate', d, True, ftDateTime);
                SetParam('RecordNbr', ds.FieldByName(ds.Name + '_NBR').AsInteger);
                ds := tds;
                p.Write(ds.ProviderName, ctx_DataAccess.ClientID, AsVariant);
                with TGetDataResults.Create(ctx_DBAccess.GetDataSets(p)) do
                try
                  Read(ds, True);
                finally
                  Free;
                end;
              end;
            finally
              p.Free;
            end;
          end;

          if ds.RecordCount = 0 then
          begin
            ds.Insert;
            Assert(NewNbr <> 0);
            for k := 0 to Pred(ds.FieldCount) do
              for l := Low(FieldsToPopulate) to High(FieldsToPopulate) do
                if FieldsToPopulate[l].T.ClassName = 'T' + UpdateArray[i].Table then
                  if FieldsToPopulate[l].F = ds.Fields[k].FieldName then
                    if Length(FieldsToPopulate[l].D) > 0 then
                      ds.Fields[k].Value := FieldsToPopulate[l].D;

            //gdy33
            if assigned(IEDef) then
              IEDef.ExecuteAfterInsert( 'AI_' + UpperCase(UpdateArray[i].Table), ds, UpdateArray[i].KeyTables, UpdateArray[i].KeyFields, UpdateArray[i].KeyValues );

            ds.FieldByName(UpdateArray[i].Table + '_NBR').AsInteger := NewNbr;
            for k := 0 to High(CondValueArray) do
              if Assigned(ds.FindField(CondFieldArray[k])) then
                ds[CondFieldArray[k]] := CondValueArray[k];
            for k := 0 to High(UpdateArray[i].KeyTables) do
              if AnsiCompareText(UpdateArray[i].KeyTables[k], UpdateArray[i].Table) = 0 then
                if not VarIsEmpty( UpdateArray[i].KeyValues[k] ) then //gdy34
                  ds[UpdateArray[i].KeyFields[k]] := UpdateArray[i].KeyValues[k];
            ds.Post;
          end;

          for k := 0 to Pred(ds.FieldCount) do
          begin
            sF := sF + ds.Fields[k].FieldName + ',';
            sP := sP + ':' + ds.Fields[k].FieldName + ',';
            tmpFieldValue := TIEMapValue.Create;
            tmpFieldValue.Value := ds.Fields[k].Value;
            Params.AddValue(ds.Fields[k].FieldName, tmpFieldValue);
          end;

          sF := Copy(sF, 1, Pred(Length(sF)));
          sP := Copy(sP, 1, Pred(Length(sP)));
          if ds.Name = 'ASOFDATE' then
          begin
            tmpFieldValue := TIEMapValue.Create;
            tmpFieldValue.Value := 'X';
            Params.AddValue('ACTIVE_RECORD', tmpFieldValue);
          end
          else
          begin
            tmpFieldValue := TIEMapValue.Create;
            tmpFieldValue.Value := Null;
            Params.AddValue('ACTIVE_RECORD', tmpFieldValue);
          end;
          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := UpdateArray[i].EffectiveDate;
          Params.AddValue('EFFECTIVE_DATE', tmpFieldValue);

          Macros.AddValue('TableName', UpdateArray[i].Table);
          Macros.AddValue('FieldList', sF);
          Macros.AddValue('ParamList', sP);

          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := dNow;
          Params.AddValue('CREATION_DATE', tmpFieldValue);

          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := Context.UserAccount.InternalNBR;
          Params.AddValue('CHANGED_BY', tmpFieldValue);
        end;
        Cur := InsPos;
      end;

      if not bDel then //gdy30
        Assert(ds.RecordCount = 1, 'Cannot uniquely identify record in table. TableName: ' + ds.Name) //gdy30
      else
        Assert(ds.RecordCount > 0, 'Cannot uniquely identify record in table. TableName: ' + ds.Name );

      if not VarIsEmpty( UpdateArray[i].Value ) then //gdy30
      begin
        if VarIsArray( UpdateArray[i].Value ) then
        begin
          sCond := '';
          for k := 0 to High(CondFieldArray) do//gdy
          begin
            if IDS(UpdateArray[i].Value[0]).FieldCount = 0 then
              CurrentDataRequired(IDS(UpdateArray[i].Value[0]), AlwaysFalseCond);
            if Assigned(IDS(UpdateArray[i].Value[0]).FindField(CondFieldArray[k])) then //gdy
            begin
              if sCond <> '' then
                sCond := sCond + ' and ';
              sCond := sCond + CondFieldArray[k] + ' = ' + IntToStr(CondValueArray[k]);
            end;
          end;
          CurrentDataRequired(IDS(UpdateArray[i].Value[0]), sCond);
          UpdateArray[i].Value := IDS(UpdateArray[i].Value[0]).Lookup(UpdateArray[i].Value[1], UpdateArray[i].Value[2], UpdateArray[i].Value[3]);
        end;

        ds.Edit;
        ds.FieldByName(UpdateArray[i].Field).Value := UpdateArray[i].Value;

        // update SHIFT_RATE and SHIFT_PERCENTAGE from CO_SHIFTS defaults when make reimport
        // EVoX dosn't need this logic, it implements it in different way and in different place
        if not FEvoX then
        begin
          if (UpperCase(UpdateArray[i].Table) = 'EE_WORK_SHIFTS')
            and Assigned(ds.FindField('SHIFT_RATE')) and Assigned(ds.FindField('SHIFT_PERCENTAGE')) then
            if IDS('CO_SHIFTS').Locate('CO_SHIFTS_NBR', UpdateArray[i].Value, []) then
            begin
              ds['SHIFT_RATE'] := IDS('CO_SHIFTS')['DEFAULT_AMOUNT'];
              ds['SHIFT_PERCENTAGE'] := IDS('CO_SHIFTS')['DEFAULT_PERCENTAGE'];
            end;

          if (UpperCase(UpdateArray[i].Table) = 'EE_SCHEDULED_E_DS') and
             ((UpperCase(UpdateArray[i].Field) = 'EFFECTIVE_START_DATE') or (UpperCase(UpdateArray[i].Field) = 'EFFECTIVE_END_DATE')) and
             Assigned(ds.FindField('SCHEDULED_E_D_ENABLED'))  then
          begin
            ds['SCHEDULED_E_D_ENABLED'] := 'Y';
          end;
        end;

        ds.Post;

        tmpFieldValue := TIEMapValue.Create;
        tmpFieldValue.Value := ds.FieldByName(UpdateArray[i].Field).Value;
        tmpFieldValue.UpdateAsOfDate := UpdateArray[i].UpdateAsOfDate;
        tmpFieldValue.IsMappedValue := True;
        aExec[Cur].Params.SetValueByName(UpdateArray[i].Field, tmpFieldValue);
        if (UpperCase(UpdateArray[i].Table) = 'EE_WORK_SHIFTS')
          and Assigned(ds.FindField('SHIFT_RATE')) and Assigned(ds.FindField('SHIFT_PERCENTAGE')) then
        begin
          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := ds.FieldByName('SHIFT_RATE').Value;
          aExec[Cur].Params.SetValueByName('SHIFT_RATE', tmpFieldValue);

          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := ds.FieldByName('SHIFT_PERCENTAGE').Value;
          aExec[Cur].Params.SetValueByName('SHIFT_PERCENTAGE', tmpFieldValue);
        end;
        if (UpperCase(UpdateArray[i].Table) = 'EE_SCHEDULED_E_DS') and
           ((UpperCase(UpdateArray[i].Field) = 'EFFECTIVE_START_DATE') or (UpperCase(UpdateArray[i].Field) = 'EFFECTIVE_END_DATE')) and
           Assigned(ds.FindField('SCHEDULED_E_D_ENABLED'))  then
        begin
          tmpFieldValue := TIEMapValue.Create;
          tmpFieldValue.Value := ds.FieldByName('SCHEDULED_E_D_ENABLED').Value;
          aExec[Cur].Params.SetValueByName('SCHEDULED_E_D_ENABLED', tmpFieldValue);
        end;
      end;
    end;
  end;

  SetLength(aDatasets, 0);
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try

    for i := Low(aExec) to High(aExec) do
    begin
      aAsOfDate[i] := True;
      UpdateRecord;
    end;

    ctx_DataAccess.PostDataSets(aDatasets, True);

    for i := Low(aDatasets) to High(aDatasets) do
      aDatasets[i].DoAfterChangesApply;

    ctx_DataAccess.CommitNestedTransaction;
  except
    on E : Exception do
    begin
      for i := Low(aDatasets) to High(aDatasets) do
        aDatasets[i].CancelUpdates;

      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;
end;

procedure TIEMap.GetRequiredKeys(const Table, Field: string; const Value: Variant;
                          const KeyTable, KeyField: string; var KeyValues: TVariantArray);
var
  ds, rds: TevClientDataSet;
  aValue: Variant;
  aField: string;
begin
  ds := IDS(Table);
  aValue := Value;
  aField := Field;
  repeat
    CurrentDataRequired(ds, aField + ' = ''' + VarToStr(aValue) + '''');
    if Assigned(ds.FindField(KeyField)) and (ds.FindField(KeyField).FieldKind = fkData) then
    begin
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyValues[High(KeyValues)] := ds.FindField(KeyField).Value;
    end
    else if Assigned(ds.FindField(KeyTable + '_NBR')) then
    begin
      rds := IDS(KeyTable);
      CurrentDataRequired(rds, KeyTable + '_NBR = ''' + ds.FindField(KeyTable + '_NBR').AsString + '''');
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyValues[High(KeyValues)] := rds.FieldByName(KeyField).Value;
    end;
{    if Assigned(ds.ParentDataSet) then
    begin
      aField := ds.ParentDataSet.Name + '_NBR';
      aValue := ds.FieldByName(aField).Value;
    end;
    ds := ds.ParentDataSet;}
    ds := nil;
  until (Length(KeyValues) > 0) or not Assigned(ds);

  if Length(KeyValues) = 0 then
  begin
    rds := IDS(KeyTable);
    if rds.FieldCount = 0 then
      CurrentDataRequired(rds, AlwaysFalseCond);
    if Assigned(rds.FindField(Field)) then
    begin
      CurrentDataRequired(rds, Field + ' = ''' + VarToStr(Value) + '''');
      rds.First;
      while not rds.Eof do
      begin
        if (rds.Name = KeyTable) and
           Assigned(rds.FindField(KeyField)) and
           (rds.FindField(KeyField).FieldKind = fkData) then
        begin
          SetLength(KeyValues, Succ(Length(KeyValues)));
          KeyValues[High(KeyValues)] := rds.FindField(KeyField).Value;
        end
        else
          GetRequiredKeys(rds.Name, rds.Name + '_NBR', rds.FieldByName(rds.Name + '_NBR').Value,
                          KeyTable, KeyField, KeyValues);
        rds.Next;
      end;
    end;
  end;
end;

procedure TIEMap.CurrentDataRequired(DS: TevClientDataSet; Condition: string = AlwaysTrueCond);
var
  p: TGetDataParams;
  i: integer;
  IndexDef: TIndexDef;
  bFound: boolean;
  sFixedCondition: String;
begin
//  ODS( 'CurrentDataRequired: ' + DS.Name + ': <' + Condition + '>');
  if ds.ProviderName = 'TEMP_CUSTOM_PROV' then
    ds.CheckDSCondition(Condition)
  else
    ds.CheckDSConditionAndClient(ctx_DataAccess.ClientID, AlwaysTrueCond);
  if not ds.Active then
  begin
    ds.IndexFieldNames := '';
    p := TGetDataParams.Create;
    try
      with TExecDSWrapper.Create('GenericSelectWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', ds.Name);
        if ds.ProviderName = 'TEMP_CUSTOM_PROV' then
          SetMacro('Condition', Condition)
        else if bDel then
          SetMacro('Condition', 'ACTIVE_RECORD in (''C'', ''N'')')
        else
          SetMacro('Condition', '{AsOfNow<' + ds.Name + '>}');
        p.Write(ds.ProviderName, ctx_DataAccess.ClientID, AsVariant);
        with TGetDataResults.Create(ctx_DBAccess.GetDataSets(p)) do
        try
          Read(ds, True);
        finally
          Free;
        end;
      end;
      DS.LogChanges := False;
      DS.SkipFieldCheck := True;
      if ds.ProviderName <> 'TEMP_CUSTOM_PROV' then
        DS.SetOpenCondition('1=1');
      if Assigned(ds.FindField('ACTIVE_RECORD')) then
        ds.IndexFieldNames := 'ACTIVE_RECORD';
      if FEvoX then
      begin
        DS.IndexName := '';
        DS.IndexDefs.Clear;
      end;
    finally
      p.Free;
    end;
  end;
  if (ds.ProviderName = 'TEMP_CUSTOM_PROV') or (Trim(Condition) = '') or (not FEvoX) then
    DS.RetrieveCondition := Condition
  else
  begin
    // EvoX
    bFound := false;
    sFixedCondition := Trim(Condition);
    if (Length(sFixedCondition) >= 2) and (sFixedCondition[1] = '(') {and (sFixedCondition[Length(sFixedCondition)] = ')')} and (pos(')', sFixedCondition) = Length(sFixedCondition)) then
      sFixedCondition := Copy(sFixedCondition, 2, Length(sFixedCondition) - 2);
    if sFixedCondition <> '' then
    begin
      for i := 0 to DS.IndexDefs.Count - 1 do
      begin
        IndexDef := DS.IndexDefs[i];
        if IndexDef.Name = sFixedCondition then
        begin
          bFound := true;
          break;
        end;
      end;
      if not bFound then
        DS.AddFilteredIndex(sFixedCondition, DS.Name + '_NBR', [], sFixedCondition, []);
      DS.IndexName := sFixedCondition;
      if DS.IndexDefs.Count > 5 then
      begin
        for i := DS.IndexDefs.Count - 1 downto 0 do
        begin
          IndexDef := DS.IndexDefs[i];
          if (IndexDef.Name <> sFixedCondition) and (IndexDef.Name <> AlwaysFalseCond) then
            DS.IndexDefs.Delete(i);
        end
      end;
    end
    else
    begin
      DS.IndexName := '';
      DS.RetrieveCondition := '';
    end
  end;
end;

function TIEMap.IDS(dsName: string): TevClientDataSet;
var
  i: Integer;
  DBCl: TddDatabaseClass;
begin
  Result := nil;
  for i := 0 to Pred(dsHolder.ComponentCount) do
    if AnsiCompareText(dsHolder.Components[i].Name, dsName) = 0 then
    begin
      Result := dsHolder.Components[i] as TevClientDataSet;
      Break;
    end;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(dsHolder);
    Result.Name := UpperCase(dsName);
    Result.SkipFieldCheck := True;
    DBCl := GetddDatabaseByTableName(Result.Name);
    if Assigned(DBCl) then
      Result.ProviderName := ctx_DataAccess.GetCustomViewByddDatabase(DBCl).ProviderName;
  end;
end;

//gdy33
destructor TIEMap.Destroy;
begin
  EndMapping;
  inherited;
end;

procedure TIEMap.DoOnConstruction;
begin
  inherited;

  FSharedVariables := TisListOfValues.Create;
end;

function TIEMap.GetListOfVariables: IisListOfValues;
begin
  Result := FSharedVariables;
end;

function TIEMap.IDSForUpdates(dsName: string): TevClientDataSet;
var
  i: Integer;
  Tbl: TClass;
begin
  Result := nil;
  for i := 0 to Pred(dsHolderForUpdates.ComponentCount) do
    if AnsiCompareText(dsHolderForUpdates.Components[i].Name, dsName) = 0 then
    begin
      Result := dsHolderForUpdates.Components[i] as TevClientDataSet;
      Break;
    end;

  if not Assigned(Result) then
  begin
    Tbl := GetClass('T' + dsName);
    CheckCondition(Assigned(Tbl), 'Cannot get class: ' + 'T' + dsName);
    Result := TddTableClass(Tbl).Create(dsHolderForUpdates);
  end;
end;

constructor TIEMap.Create(const AImportName: String = ''; const ADefaultsCallback: TEvExchangeDefaultValues = nil;
  const AChangeLog: TEvExchangeChangeLog = nil; ACalcFields: TEvExchangeCalcFields = nil;  ACheckValuesBeforeChange: TEvExchangeCheckValuesBeforeChange = nil);
begin
  inherited Create;

  FImportName := AImportName;
  if FImportName = sEvoXImport then
    FEvoX := true;

  if FEvoX then
  begin
    FDefaultsCallback := ADefaultsCallback;
    FChangeLog := AChangeLog;
    FCalcFieldsCallback := ACalcFields;
    FCheckValuesBeforeChange := ACheckValuesBeforeChange;
    CheckCondition(Assigned(FDefaultsCallback), 'Internal error. EvoX requires defaults callback');
    CheckCondition(Assigned(FChangeLog), 'Internal error. EvoX requires change log callback');
    CheckCondition(Assigned(FCalcFieldsCallback), 'Internal error. EvoX requires calc fields callback');
    CheckCondition(Assigned(FCheckValuesBeforeChange), 'Internal error. EvoX requires check values before change callback');
  end;
end;

function TIEMap.ComputeCalcFields(const ADataSet: TevClientDataSet): boolean;
begin
  Result := FCalcFieldsCallback(ADataSet);
end;

procedure TIEMap.PostFieldValueAsOfDate(AsOfDate: TDatetime;
  const aTable: string; aKeyValue: integer; aField: TField;
  FieldValue: variant);
var
  ChangePacket: IevDataChangePacket;
begin
  ChangePacket := TevDataChangePacket.Create;
  with ChangePacket.NewUpdateFields(aTable, aKeyValue) do
  begin
    Fields.AddValue(aField.FieldName, FieldValue);
    Fields.AddValue('effective_date', AsOfDate);
  end;

  ctx_DBAccess.ApplyDataChangePacket(ChangePacket);
end;

{ TIEDefinitionBase }
type
  AI_Procedure = procedure(const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray) of object;
  IECustomFunc = function(IsImport: Boolean; const Table, Field: string; Value: Variant;
                                     var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
                                     out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean of object;

function TIEDefinitionBase.GetMethod( aMethodName: string; out method: TMethod ): boolean;
begin
  //getmethodprop
  method.Data := Self;
  method.Code := MethodAddress( aMethodName );
  Result := assigned( method.Code );
end;

procedure TIEDefinitionBase.ExecuteAfterInsert(aMethodName: string;
  const ds: TevClientDataSet; const KeyTables, KeyFields: TStringArray;
  const KeyValues: TVariantArray);
var
  ai: AI_Procedure;
begin
  if GetMethod( aMethodName, TMethod(ai) ) then
    ai( ds, KeyTables, KeyFields, KeyValues );
end;

function TIEDefinitionBase.ExecuteCustomFunc(aMethodName: string;
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): boolean;
var
  cp: IECustomFunc;
begin
  Result := GetMethod(aMethodName, TMethod(cp)) and
            cp(IsImport, Table, Field, Value, KeyTables, KeyFields, KeyValues, OTables, OFields, OValues );
end;

procedure TIEMap.ClearDataSetsAndVariables;
begin
  FSharedVariables.Clear;
  try
    dsHolder.Free;
  finally
    dsHolder := TComponent.Create(nil);
  end;
end;

{ TIEMapValue }

destructor TIEMapValue.Destroy;
begin
  inherited;
end;

procedure TIEMapValue.DoOnConstruction;
begin
  inherited;
  FUpdateAsOfDate := 0;
  FValue := null;
end;

function TIEMapValue.GetIsMappedValue: boolean;
begin
  Result := FIsMappedValue;
end;

class function TIEMapValue.GetTypeID: String;
begin
  Result := 'IEMapValue';
end;

function TIEMapValue.GetUpdateAsOfDate: TDateTime;
begin
  Result := FUpdateAsOfDate;
end;

function TIEMapValue.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TIEMapValue.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FUpdateAsOfDate := AStream.ReadDouble;
  FValue := AStream.ReadVariant;
  FIsMappedValue := AStream.ReadBoolean;
end;

procedure TIEMapValue.SetIsMappedValue(const Value: boolean);
begin
  FIsMappedValue := Value;
end;

procedure TIEMapValue.SetUpdateAsOfDate(const Value: TDateTime);
begin
  FUpdateAsOfDate := Value;
end;

procedure TIEMapValue.SetValue(const Value: Variant);
begin
  FValue := Value;
end;

procedure TIEMapValue.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteDouble(FUpdateAsOfDate);
  AStream.WriteVariant(FValue);
  AStream.WriteBoolean(FIsMappedValue);
end;

initialization
  ObjectFactory.Register([TIEMapValue]);

finalization
  SafeObjectFactoryUnRegister([TIEMapValue]);

end.
