object dlAsOfDate2RB: TdlAsOfDate2RB
  Left = 521
  Top = 243
  BorderStyle = bsDialog
  Caption = 'Dialog'
  ClientHeight = 192
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evRadioGroup1: TevRadioGroup
    Left = 8
    Top = 8
    Width = 129
    Height = 177
    Caption = 'Date'
    Items.Strings = (
      ' 1/1/2009'
      ' 10/1/2008'
      ' 7/1/2008'
      ' 4/1/2008'
      ' 1/1/2008'
      ' 10/1/2007'
      '')
    TabOrder = 0
    OnClick = evRadioGroup1Click
  end
  object OKBtn: TButton
    Left = 275
    Top = 160
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 163
    Top = 159
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object evDateTimePicker1: TevDateTimePicker
    Left = 33
    Top = 155
    Width = 95
    Height = 21
    Date = 37928.000000000000000000
    Time = 37928.000000000000000000
    Enabled = False
    TabOrder = 3
  end
  object evGroupBox1: TevRadioGroup
    Left = 147
    Top = 8
    Width = 105
    Height = 113
    Caption = 'Local Enabled'
    TabOrder = 4
    OnClick = evGroupBox1Click
  end
  object evGroupBox2: TevRadioGroup
    Left = 259
    Top = 8
    Width = 105
    Height = 113
    Caption = 'Deduct'
    TabOrder = 5
  end
end
