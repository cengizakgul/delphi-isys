unit SPD_PersonDocuments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ISBasicClasses,  Mask, wwdbedit,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, DB, Wwdatsrc, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  wwdbdatetimepicker, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBEdit, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel;

type
  TPersonDocuments = class(TFrame)
    odDocs: TOpenDialog;
    sdDocs: TSaveDialog;
    pnlDocs1: TPanel;
    grDocs: TevDBGrid;
    dsDocs: TevDataSource;
    cdBrowse: TevClientDataSet;
    cdBrowseNAME: TStringField;
    cdBrowseFILE_NAME: TStringField;
    cdBrowseCREATION_DATE: TDateTimeField;
    cdBrowseBEGIN_DATE: TDateTimeField;
    cdBrowseEND_DATE: TDateTimeField;
    dsEditDoc: TevDataSource;
    pnlDocs2: TPanel;
    btnAddDoc: TevBitBtn;
    btnDeleteDoc: TevBitBtn;
    btnSaveDoc: TevBitBtn;
    btnShowDoc: TevBitBtn;
    btnEditDoc: TevBitBtn;
    evPanel1: TevPanel;
    cdBrowseCL_PERSON_DOCUMENTS_NBR: TIntegerField;
    cdBrowseCL_PERSON_NBR: TIntegerField;
    evLabel2: TevLabel;
    pnlGridBorder: TevPanel;
    fpGridDocuments: TisUIFashionPanel;
    pnlFPBody: TevPanel;
    fpDocDetails: TisUIFashionPanel;
    evLabel15: TevLabel;
    evLabel28: TevLabel;
    evLabel1: TevLabel;
    dbeDocDescr: TevDBEdit;
    wwdpInitial_Effective_date: TevDBDateTimePicker;
    evDBDateTimePicker1: TevDBDateTimePicker;
    fpControls: TisUIFashionPanel;
    pnlDocsBackground: TevPanel;
    procedure btnSaveDocClick(Sender: TObject);
    procedure btnShowDocClick(Sender: TObject);
    procedure btnShowDocSetSecurityRO(const Control: TControl;
      var ReadOnly, Accept: Boolean);
    procedure btnDeleteDocClick(Sender: TObject);
    procedure btnEditDocClick(Sender: TObject);
    procedure btnAddDocClick(Sender: TObject);
    procedure dsEditDocDataChange(Sender: TObject; Field: TField);
    procedure cdBrowseAfterScroll(DataSet: TDataSet);
    procedure FrameEnter(Sender: TObject);
  private
    { Private declarations }
    procedure LoadDoc;

  public
  end;

implementation

uses
  SDataStructure, evStreamUtils, isZippingRoutines, evUtils;
{$R *.dfm}

procedure TPersonDocuments.LoadDoc;
var
  MS1, MS2: IisStream;
  fext: String;
begin
  // Edit, Insert
  if odDocs.Execute then
  begin
    CheckFileSize(odDocs.FileName, 2000);

    fext := ExtractFileExt(odDocs.FileName);
    Delete(fext, 1, 1);

    DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('File_Name').AsString := ExtractFileName(odDocs.FileName);

    MS1 := TisStream.Create;
    MS1.LoadFromFile(odDocs.FileName);
    MS1.Position := 0;

    MS2 := TisStream.Create;
    MS2.WriteString(fext);
    DeflateStream(MS1.RealStream, MS2.RealStream);
    MS2.Position := 0;
    DM_CLIENT.CL_PERSON_DOCUMENTS.UpdateBlobData('Document', MS2);
  end;
end;

procedure TPersonDocuments.btnSaveDocClick(Sender: TObject);
var
  MS1, MS2: IisStream;
  fext: String;
begin
  // Save to Hard disk
  if DM_CLIENT.CL_PERSON_DOCUMENTS.RecordCount = 0 then
    Exit;

  if DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('DOCUMENT').IsNull then
    Exit;

  MS1 := TisStream.Create;
  MS1 := DM_CLIENT.CL_PERSON_DOCUMENTS.GetBlobData('DOCUMENT');
  MS1.Position := 0;

  fext := MS1.ReadString;
  sdDocs.DefaultExt := '.' + fext;

  if Trim(DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('File_Name').AsString) = '' then
    sdDocs.FileName := 'eeDocument.' + fext
  else
    sdDocs.FileName := Trim(DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('File_Name').AsString);

  if not sdDocs.Execute then
    Exit;

  MS2 := TisStream.Create;
  InflateStream(MS1.RealStream, MS2.RealStream);
  MS2.Position := 0;
  MS2.SaveToFile(sdDocs.FileName);
end;

procedure TPersonDocuments.btnShowDocClick(Sender: TObject);
var
  MS1, MS2: IisStream;
  fext: String;
  Buf1: array[0..1023] of AnsiChar;
  Buf2: array[0..1023] of AnsiChar;
begin
  if DM_CLIENT.CL_PERSON_DOCUMENTS.RecordCount = 0 then
    Exit;
  if DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('DOCUMENT').IsNull then
    Exit;

  MS1 := TisStream.Create;
  MS1 := DM_CLIENT.CL_PERSON_DOCUMENTS.GetBlobData('DOCUMENT');
  MS1.Position := 0;

  GetTempPath(1023, Buf1);
  GetTempFileName(Buf1, 'evo', 0, Buf2);
  fext := ChangeFileExt(String(PChar(@Buf2[0])), fext) + '.' + MS1.ReadString;

  MS2 := TisStream.Create;
  InflateStream(MS1.RealStream, MS2.RealStream);
  MS2.Position := 0;
  MS2.SaveToFile(fext);

  RunShellViewer(fext);
end;

procedure TPersonDocuments.btnShowDocSetSecurityRO(const Control: TControl;
  var ReadOnly, Accept: Boolean);
begin
  ReadOnly := false;
end;

procedure TPersonDocuments.btnDeleteDocClick(Sender: TObject);
begin
 if EvMessage('Do you really want to delete the attachment?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    DM_CLIENT.CL_PERSON_DOCUMENTS.Delete;
end;

procedure TPersonDocuments.btnEditDocClick(Sender: TObject);
begin
  DM_CLIENT.CL_PERSON_DOCUMENTS.Edit;
  Loaddoc;
end;

procedure TPersonDocuments.btnAddDocClick(Sender: TObject);
begin
  DM_CLIENT.CL_PERSON_DOCUMENTS.Append;
  DM_CLIENT.CL_PERSON_DOCUMENTS.FieldByName('BEGIN_DATE').AsDateTime := Date;
  Loaddoc;
end;

procedure TPersonDocuments.dsEditDocDataChange(Sender: TObject;
  Field: TField);
var
  previousDocNBR : integer;
begin
  previousDocNBR := dsEditDoc.DataSet.FieldByName('CL_PERSON_DOCUMENTS_NBR').asInteger;

  cdBrowse.Close;
  cdBrowse.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', '*');
    SetMacro('TableName', 'CL_PERSON_DOCUMENTS');
    SetMacro('Condition', ' CL_PERSON_NBR =  '+ IntToStr(dsEditDoc.DataSet.FieldByName('CL_PERSON_NBR').asInteger) );
    cdBrowse.DataRequest(AsVariant);
  end;
  cdBrowse.open;
  dsDocs.DataSet := cdBrowse;
  if ( dsDocs.DataSet.Active) and
     ( previousDocNBR > 0 ) then
    dsDocs.DataSet.locate('CL_PERSON_DOCUMENTS_NBR', previousDocNBR, []);
  btnDeleteDoc.Enabled := not DM_CLIENT.CL_PERSON_DOCUMENTS.IsEmpty;
  btnEditDoc.Enabled   := btnDeleteDoc.Enabled;
  btnSaveDoc.Enabled   := btnDeleteDoc.Enabled;
  btnShowDoc.Enabled   := DM_CLIENT.CL_PERSON_DOCUMENTS.RecordCount > 0;
end;
procedure TPersonDocuments.cdBrowseAfterScroll(DataSet: TDataSet);
begin
  if Assigned(dsDocs.DataSet) and
     ( dsEditDoc.DataSet.State = dsBrowse ) then
  begin
    dsEditDoc.OnDataChange := nil;
    try
       dsEditDoc.DataSet.Locate('CL_PERSON_DOCUMENTS_NBR', dsDocs.DataSet.FieldByName('CL_PERSON_DOCUMENTS_NBR').asInteger, []);
    finally
      dsEditDoc.OnDataChange := dsEditDocDataChange;
    end;
  end;
end;

procedure TPersonDocuments.FrameEnter(Sender: TObject);
begin
  //GUI 2.0 COLUMN FIX
  grDocs.Columns[0].DisplayWidth := 36;
  grDocs.Columns[1].DisplayWidth := 37;
  grDocs.Columns[2].DisplayWidth := 18;
  grDocs.Columns[3].DisplayWidth := 13;
  grDocs.Columns[4].DisplayWidth := 13;
end;

end.
