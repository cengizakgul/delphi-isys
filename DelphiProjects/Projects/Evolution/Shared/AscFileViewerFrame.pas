// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit AscFileViewerFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  ExtCtrls, EvStreamUtils, ComCtrls,
  ISBasicClasses, EvMainboard, EvUIComponents;

type
  TOnSaveToFile = procedure (const Strings: TStringList; const FileName: string) of object;
  TAscFileViewer = class(TFrame)
    evPanel1: TevPanel;
    bSave: TevBitBtn;
    evMemo: TevRichEdit;
    SaveDialog: TSaveDialog;
    procedure bSaveClick(Sender: TObject);
  private
    FRegistryKeyName: string;
    FList: TStringList;
    FOnSaveToFile: TOnSaveToFile;
    { Private declarations }
  protected
    procedure Loaded; override;
  public
    { Public declarations }
    constructor CreateExt(const AOwner: TComponent; const AParent: TWinControl; const m: IisStream); overload;
    constructor CreateExt(const AOwner: TComponent; const AParent: TWinControl; const m: TStringList); overload;
    destructor Destroy; override;
    procedure LoadFromStream(const m: IisStream);
    procedure LoadFromStrings(const m: TStringList);
    procedure SaveToFile; overload;
    procedure SaveToFile(const FileName: string); overload;
    property OnSaveToFile: TOnSaveToFile read FOnSaveToFile write FOnSaveToFile; 
  published
    property RegistryKeyName: string read FRegistryKeyName write FRegistryKeyName;
  end;

implementation

{$R *.DFM}
                             
{ TAscFileViewer }

const
  FileNameStr = 'FileName';

procedure TAscFileViewer.Loaded;
begin
  inherited;
  SaveDialog.FileName := mb_AppSettings[RegistryKeyName + '\' + FileNameStr];
end;

procedure TAscFileViewer.SaveToFile;
begin
  if SaveDialog.Execute then
    SaveToFile(SaveDialog.FileName);
end;

procedure TAscFileViewer.LoadFromStream(const m: IisStream);
begin
  if Assigned(m) then
  begin
    m.Position := 0;
    FList.LoadFromStream(m.RealStream);
    evMemo.Lines.Assign(FList);
  end;
end;

procedure TAscFileViewer.SaveToFile(const FileName: string);
begin
  if Assigned(OnSaveToFile) then
    OnSaveToFile(FList, FileName)
  else
    FList.SaveToFile(FileName);
  mb_AppSettings[RegistryKeyName + '\' + FileNameStr] := FileName;
end;

constructor TAscFileViewer.CreateExt(const AOwner: TComponent;
  const AParent: TWinControl; const m: IisStream);
begin
  Create(AOwner);
  Align := alClient;
  Parent := AParent;
  FList := TStringList.Create;
  LoadFromStream(m);
end;

procedure TAscFileViewer.bSaveClick(Sender: TObject);
begin
  SaveToFile;
end;

destructor TAscFileViewer.Destroy;
begin
  FreeAndNil(FList);
  inherited;
end;

constructor TAscFileViewer.CreateExt(const AOwner: TComponent;
  const AParent: TWinControl; const m: TStringList);
begin
  Create(AOwner);
  Align := alClient;
  Parent := AParent;
  FList := TStringList.Create;
  LoadFromStrings(m);
end;

procedure TAscFileViewer.LoadFromStrings(const m: TStringList);
begin
  if Assigned(m) then
  begin
    FList.Assign(m);
    evMemo.Lines.Assign(FList);
  end;
end;

end.
