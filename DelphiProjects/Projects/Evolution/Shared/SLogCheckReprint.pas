// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SLogCheckReprint;

interface

uses
  Classes, EvUtils, SDataStructure, SysUtils, SPD_Payroll_Expert, EvTypes, Variants, EvClientDataSet,ISBasicUtils;

function LogCheckReprint(CheckNumbers: TStringList; MiscCheck: Boolean; PrNbr: Integer; Reason: String = ''): String;
function AskForCheckReprintReason: String;

implementation

function GetCheckReprintReason(const AReason: String; cs : tevClientDataset ): String;
begin
  if AReason = '' then
  begin
    Payroll_Expert := TPayroll_Expert.Create(nil);
    try
      Payroll_Expert.wwdsTemp.DataSet := cs;
      while cs.FieldByName('REASON').IsNull do
        Payroll_Expert.DisplayExpert(crMemo, 'Please, enter a reason for check reprint.',
          nil, VarArrayOf(['REASON']));
    finally
      Payroll_Expert.Free;
    end;
    Result := cs.FieldByName('REASON').AsString;
  end
  else
    Result := AReason;
end;

function AskForCheckReprintReason: String;
var
  cs: TevClientDataSet;
begin
  cs := TevClientDataSet.Create(nil);
  try
    cs.ProviderName := DM_PAYROLL.PR_REPRINT_HISTORY.ProviderName;
    cs.DataRequired('PR_NBR=0');
    cs.Insert;
    cs.FieldByName('PR_NBR').Value := 0;
    cs.FieldByName('REPRINT_DATE').Value := Now;
    Result := '';
    Result := GetCheckReprintReason(Result, cs);
    AddStrValue(Result, '0', '#PR_REPRINT_HISTORY_NBR#');    
    cs.Cancel;
  finally
    cs.Free;
  end;
end;

function LogCheckReprint(CheckNumbers: TStringList; MiscCheck: Boolean; PrNbr: Integer; Reason: String = ''): String;
var
  MySortedList: TStringList;
  MyTransactionManager: TTransactionManager;
  BeginNumber, EndNumber: string;
  I: Integer;
  cs, csdetails : TevClientDataSet;
begin
  Result := '';
  if CheckNumbers = nil then
    Exit;

  MyTransactionManager := TTransactionManager.Create(nil);
  cs := TevClientDataSet.Create(nil);
  csDetails := TevClientDataSet.Create(nil);
  try
    cs.ProviderName := DM_PAYROLL.PR_REPRINT_HISTORY.ProviderName;
    cs.DataRequired('PR_NBR=' + IntToStr(PrNbr));

    csDetails.ProviderName := DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.ProviderName;
    csDetails.DataRequired('PR_REPRINT_HISTORY_NBR=1');

    MyTransactionManager.Initialize([cs, csDetails]);
// Take care of a parent record
    cs.Insert;
    cs.FieldByName('PR_NBR').Value := PrNbr;
    cs.FieldByName('REPRINT_DATE').Value := Now;
    cs.FieldByName('REASON').AsString := GetCheckReprintReason(Reason, cs);
    Result := cs.FieldByName('REASON').AsString;
    AddStrValue(Result, cs.FieldByName('PR_REPRINT_HISTORY_NBR').AsString, '#PR_REPRINT_HISTORY_NBR#');
    cs.Post;
// Now deal with children
    MySortedList := TStringList.Create;
    try
      MySortedList.Sorted := True;
      for I := 0 to CheckNumbers.Count - 1 do
        MySortedList.Add(PadStringLeft(CheckNumbers.Strings[I], '0', 9));
      BeginNumber := MySortedList.Strings[0];
      EndNumber := MySortedList.Strings[0];
      for I := 1 to MySortedList.Count - 1 do
      begin
        if StrToInt(MySortedList.Strings[I]) <> StrToInt(EndNumber) + 1 then
        begin
          csDetails.Insert;
          if MiscCheck then
            csDetails.FieldByName('MISCELLANEOUS_CHECK').AsString := 'Y'
          else
            csDetails.FieldByName('MISCELLANEOUS_CHECK').AsString := 'N';
          csDetails.FieldByName('BEGIN_CHECK_NUMBER').Value := StrToInt(BeginNumber);
          csDetails.FieldByName('END_CHECK_NUMBER').Value := StrToInt(EndNumber);
          csDetails.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger := cs.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger;
          csDetails.Post;
          BeginNumber := MySortedList.Strings[I];
        end;
        EndNumber := MySortedList.Strings[I];
      end;
      csDetails.Insert;
      if MiscCheck then
        csDetails.FieldByName('MISCELLANEOUS_CHECK').AsString := 'Y'
      else
        csDetails.FieldByName('MISCELLANEOUS_CHECK').AsString := 'N';
      csDetails.FieldByName('BEGIN_CHECK_NUMBER').Value := StrToInt(BeginNumber);
      csDetails.FieldByName('END_CHECK_NUMBER').Value := StrToInt(EndNumber);
      csDetails.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger := cs.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger;
      csDetails.Post;
    finally
      MySortedList.Free;
    end;
    MyTransactionManager.ApplyUpdates;
  finally
    csDetails.free;
    cs.free;
    MyTransactionManager.free;
  end;
end;

end.
