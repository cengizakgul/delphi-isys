unit EvUIUtils;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
     Windows,
     Classes, DB, Dialogs, SysUtils, Controls, Menus, ActnList, ComCtrls, StdCtrls,
     Variants, ShellAPI, FileCtrl, Jpeg, Graphics, Forms, TypInfo,
     isUtils, EvMainboard, EvBasicUtils, isBasicUtils, isTypes, EvUtils,
     EvSendMail, SDataStructure, EvDataAccessComponents, EvConsts, EvUIComponents,
     StdActns, EvClientDataSet, EvContext, wwdbedit, isBaseClasses, EvStreamUtils;

type
  TDBDTFilterContext = class;

  IDBDTCache = interface
  ['{1878D0B6-F635-4E55-8146-F3A8BB9847A2}']
    procedure EnableDBDTCache;
    procedure DisableDBDTCache;
    procedure FilterDivision( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterBranch( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterDepartment( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterTeam( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
  end;

  TDBDTFilterContext = class
  private
    FDBDTCache: IDBDTCache;
    FDiv, FBrh, FDep: integer;
    FDivNull, FBrhNull, FDepNull: boolean;
  public
    constructor Create( t: IDBDTCache );
    function SetParams( aDiv, aBrh, aDep, aTeam: TIntegerField  ):boolean;
    procedure CO_DIVISIONFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CO_BRANCHFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CO_DEPARTMENTFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CO_TEAMFilterRecord(DataSet: TDataSet; var Accept: Boolean);

    property Division: integer read FDiv;
    property Branch: integer read FBrh;
    property Department: integer read FDep;

    property DivisionIsNull: boolean read FDivNull;
    property BranchIsNull: boolean read FBrhNull;
    property DepartmentIsNull: boolean read FDepNull;
  end;

  //To continue enumeration, the callback function must return true;
  //to stop enumeration, it must return false
  TOnSelected = function( ds: TEvClientDataSet ): boolean of object;
  TOnSelectedProc = function( ds: TEvClientDataSet; par: TObject ): boolean;
  TfsOption = (fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty);
  TfsOptions = set of TfsOption;
  // return false if enumeration was aborted by callback
  // if (grid.selectionlist.count = 0) and (fsForCurrentIfListIsEmpty in anOptions) then calls callback for current record

  TevTableChangeType = (tcInsert, tcUpdate, tcDelete);


function  EvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                    DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word; overload;
function  EvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; Captions: TStrings;
                    DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word; overload;
procedure EvMessage(const Msg: string; HelpCtx: Integer = 0); overload;
procedure EvErrMessage(const Msg: string; HelpCtx: Integer = 0);
procedure EvExceptMessage(const AException: Exception);
function  EvDialog(const ACaption, APrompt: string; var Value: string; ASecure: Boolean = False): Boolean;
function  EvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                     AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
function  EvChboxDialog(const ACaption, APrompt, ALabel: string;
                     var Checked: Boolean): Boolean;
function  EvDateDialog(const ACaption, APrompt: string; var Value: TDate ): Boolean;
function  EvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
procedure SaveComponentState(Control: TComponent; const AdditKey: string = '');
procedure RestoreComponentState(Control: TComponent; const AdditKey: string = '');
procedure DataSetToExcel(ADataSet:TDataSet; AList: TStringList); overload;
procedure DataSetToExcel(ADataSet:TDataSet; AFileName: string = ''; AList: TStringList = nil); overload;
procedure RunShellViewer(const AFile: String; const DeleteFileAfterClose: Boolean = True);
function  RunFolderDialog(DialogTitle, DefaultFolder: String): String;
function  PutFocus(c: TWinControl): Boolean;
procedure AddScreenShotHotKey(AOwner: TComponent);
procedure GrayOut(const control: TControl; const grayed: Boolean);
procedure GrayOutControl( control: TControl; grayed: boolean );
procedure SetFieldRequired(aReq:boolean;aField:TField;evLabel:TevLabel=nil);
function  ForSelected(grid: TevDbGrid; selproc: TOnSelectedProc; par: TObject; anOptions: TfsOptions = []): boolean; overload;
function  ForSelected(grid: TevDbGrid; selevent: TOnSelected; anOptions: TfsOptions = []): boolean; overload;
function  CreateDataSetWithSelectedRows( Source: TEvClientDataSet; bookmarks: TList ): TEvClientDataSet;
function  EnsureHasMenu( Self: TComponent; pm: TPopupMenu ): TPopupMenu;
function  EnsureHasMenuItem( has: boolean; Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu; overload;
function  EnsureHasMenuItem( has: boolean; Self: TComponent; pm: TPopupMenu; aHandler: TNotifyEvent; aCaption: string; aShortCut: string; achecked: boolean ; anEnabled: boolean ): TPopupMenu; overload;
procedure AssignVMRClientLookups(const ts: TTabSheet);
function  CheckGlSetupExist(DS: TevClientDataSet;aFields: array of string;ShowMes:boolean = true): boolean;
function  CheckGlSetuped(DS: TevClientDataSet;aFields: array of string): boolean;
function  DSIsActive( ds: TDataSet ): boolean; overload;
function  DSIsActive( ds: TDataSource ): boolean; overload;
procedure SetSelectedFromFields(const Grid: TevDBGrid; const DataSet: TevClientDataSet; const HideFields: array of ShortString);
procedure ActivateFrameAs(const AFrameMenuPath: String; const AParams: array of Variant; const AContext: Integer = 0);
function  GetFrameClassForTable( tablename: string ): TClass;
procedure DoHint(AControl: TControl);
procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String;const ACustomOptions: IisListOfValues = nil); overload;
procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String;
                                   const  DefaultToQuarters, ForceQuarters: Boolean; const ACustomOptions: IisListOfValues = nil); overload;
procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String; out AAffectedFields: TisCommaDilimitedString;
                                   const DefaultToQuarters, ForceQuarters: Boolean; const ACustomOptions: IisListOfValues = nil); overload;
procedure ShowVersionedFieldEditor(const AControl: TControl); overload;

function  GetExternalLinks: IisParamsCollection;
procedure SaveExternalLinks(const AExtLinks: IisParamsCollection);


//For use with auto creation of calendar
var
  AutoBackToPreviosScreen: Boolean = False;
  CameFrom: String;

implementation

uses EvVersionedFieldBaseFrm, VarUtils;

var
  ShellTmpFiles: TStringList = nil;
  FFrameActivateParamsRec: TFrameActivateParamsRec;  

function EvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word;
begin
  Result := ISMessage(Msg, DlgType, Buttons, DefaultButton, HelpCtx);
end;

function EvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; Captions: TStrings; DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word;
begin
  Result := ISMessage(Msg, DlgType, Buttons, Captions, DefaultButton, HelpCtx);
end;

procedure EvMessage(const Msg: string; HelpCtx: Integer = 0); overload;
begin
  ISMessage(Msg, HelpCtx);
end;

procedure EvErrMessage(const Msg: string; HelpCtx: Integer = 0);
begin
  ISErrMessage(Msg, HelpCtx, mb_AppSettings.AsBoolean['Settings\SendErrorScreen']);
end;

procedure EvExceptMessage(const AException: Exception);
var
  b: Boolean;
begin
  if Mainboard <> nil then
    b := mb_AppSettings.AsBoolean['Settings\SendErrorScreen']
  else
    b := True;
    
  isExceptMessage(AException, b);
end;

function EvDialog(const ACaption, APrompt: string; var Value: string; ASecure: Boolean = False): Boolean;
begin
  Result := ISDialog(ACaption, APrompt, Value, ASecure);
end;

function EvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                     AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
begin
  Result := ISNbrDialog(ACaption, APrompt, Value, AIncrement, AMinVal, AMaxVal);
end;

function EvChboxDialog(const ACaption, APrompt, ALabel: string;
                     var Checked: Boolean): Boolean;
begin
  Result := ISChboxDialog(ACaption, APrompt, ALabel, Checked);
end;

function EvDateDialog(const ACaption, APrompt: string; var Value: TDate ): Boolean;
begin
  Result := ISDateDialog(ACaption, APrompt, Value);
end;

function EvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
begin
  Result := ISDefaultDialog(ACaption, APrompt, ADefault);
end;

procedure SaveComponentState(Control: TComponent; const AdditKey: string = '');
begin
  if Control is TCustomEdit then
    mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] := TCustomEdit(Control).Text
  else if Control is TCheckBox then
    mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] := BoolToYN(TCheckBox(Control).Checked)
  else if Control is TRadioButton then
    mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] := BoolToYN(TRadioButton(Control).Checked)
  else if Control is TevRadioGroup then
    mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] := IntToStr(TevRadioGroup(Control).ItemIndex)
  else
    Assert(False, 'Unknown control classname "' + Control.ClassName + '" during state saving');
end;

procedure RestoreComponentState(Control: TComponent; const AdditKey: string = '');
begin
  if Control is TevSpinEdit then
    TevSpinEdit(Control).Value := StrToIntDef(mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name], TevSpinEdit(Control).Value)
  else
  if Control is TCustomEdit then
    TCustomEdit(Control).Text := mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name]
  else
    if Control is TCheckBox then
    TCheckBox(Control).Checked := mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] = 'Y'
  else
    if Control is TRadioButton then
    TRadioButton(Control).Checked := mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name] = 'Y'
  else
    if Control is TevRadioGroup then
    TevRadioGroup(Control).ItemIndex := StrToIntDef(mb_AppSettings['Misc\' + Control.Owner.ClassName + '\' + AdditKey + Control.Name], TevRadioGroup(Control).ItemIndex)
  else
    Assert(False, 'Unknown control classname "' + Control.ClassName + '" during state restoring');
end;

procedure DataSetToExcel(ADataSet:TDataSet; AList: TStringList);
begin
  DataSetToExcel(ADataSet, '', AList);
end;

procedure DataSetToExcel(ADataSet:TDataSet; AFileName: string = ''; AList: TStringList = nil);
var
  F: TextFile;
  i,j,k: Integer;
  BM: TBookMark;
  SD: TSaveDialog;

  procedure WriteLine(AStr: string);
  begin
    if Assigned(AList) then
      AList.Add(AStr)
    else
      writeln(F, AStr);
  end;

  procedure WriteExcel(y, x:Integer; Str:Variant; Fnt:Smallint; Align:TAlignment);
  var
    strh:  String;
    strh1: String[3];
    strh2: String[2];
    stro: String;
  begin
    strh:='';
    strh1:='';
    strh2:='';
    case Fnt of
     0: strh2:='D';
     1: strh2:='';
    end;
    case Align of
      taLeftJustify:  strh1:='L';
      taRightJustify: strh1:='R';
      taCenter:       strh1:='C';
    end;

    if VarType(Str) in [varInteger, varSingle, varDouble, varCurrency] then
    begin
      strh1:='R';
      strh:='';
      stro:=FloatToStr(str);
    end
    else
    begin
      strh:='"';
      stro := VarToStr(str);
    end;
    WriteLine('F;P0;FG0'+strh1+';S'+strh2+'M'+IntToStr(Fnt+6)+';Y'+IntToStr(y)+';X'+IntToStr(x));
    WriteLine('C;Y'+IntToStr(y)+';X'+IntToStr(x)+';K'+strh+Stro+strh);
  end;

begin
  if (AFileName = '') and not Assigned(AList) then
  begin
    SD:=TSaveDialog.Create(nil);
    try
      SD.DefaultExt:='slk';
      SD.Filter:='Files SYLK (*.slk)|*.slk|All Files (*.*)|*.*';
      SD.Title:='Save data to Excel file';
      SD.Options:=[ofHideReadOnly];

      if SD.Execute then
        AFileName := SD.FileName
      else
        Exit;
    finally
      SD.Free;
    end;
  end;

  if not Assigned(AList) then
  begin
    assign(F, AFileName);
    rewrite(F);
  end;

  WriteLine('ID;PWXL;N;E');
  WriteLine('P;PGeneral');
  WriteLine('P;FArial;M200');
  WriteLine('P;FArial;M200;SB');
  WriteLine('P;FArial;M200;SI');
  WriteLine('P;FArial;M200;SBI');
  WriteLine('P;EArial;M200');
  WriteLine('F;P0;DG0G8;SM0;M255');
  WriteLine('O;L;D;V0;K47;G100 0.001');
  WriteLine('P;ETimes New Roman;M240;SB');
  WriteLine('P;ETimes New Roman;M240');

  j:=0;
  for k:=0 to ADataSet.FieldCount-1 do
    if ADataSet.Fields[k].Visible then
    begin
      j:=j+1;
      WriteExcel(1, j, ADataSet.Fields[k].DisplayLabel, 0, taCenter);
    end;

  if j>0 then
  begin
    ADataSet.DisableControls;
    BM:=ADataSet.GetBookMark;
    ADataSet.First;
    i:=2;
    while not ADataSet.Eof do
    begin
      j:=1;
      for k:=0 to ADataSet.FieldCount-1 do
        if ADataSet.Fields[k].Visible then
        begin
          WriteExcel(i, j, ADataSet.Fields[k].AsVariant, 1, ADataSet.Fields[k].Alignment);
          j:=j+1;
        end;
      i:=i+1;
      ADataSet.Next;
    end;
    ADataSet.GoToBookMark(BM);
    ADataSet.FreeBookMark(BM);
    ADataSet.EnableControls;
  end;
  WriteLine('E'#10#13);
  if not Assigned(AList) then
    close(F);
end;

procedure RunShellViewer(const AFile: String; const DeleteFileAfterClose: Boolean = True);
begin
 if DeleteFileAfterClose then
 begin
   if not Assigned(ShellTmpFiles) then
     ShellTmpFiles := TStringList.Create;
   ShellTmpFiles.Add(AFile);
 end;

 ShellExecute(0, 'open', PAnsiChar(AFile), nil, nil, SW_SHOWNORMAL);
end;

procedure DeleteShellTmpFiles;
var
  i: Integer;
begin
  if Assigned(ShellTmpFiles) then
  begin
    for i := 0 to ShellTmpFiles.Count - 1 do
      SysUtils.DeleteFile(ShellTmpFiles[i]);
    FreeAndNil(ShellTmpFiles);
  end;
end;

function RunFolderDialog(DialogTitle, DefaultFolder: String): String;
begin
  Result := DefaultFolder;
  SelectDirectory(DialogTitle, '', Result);
end;

function PutFocus(c: TWinControl): Boolean;
var
  i: Integer;
  TL: TList;
begin
  TL := TList.Create;
  try
    c.GetTabOrderList(TL);
    Result := False;
    for i := 0 to Pred(TL.Count) do
      if TWinControl(TL[i]).CanFocus then
      begin
        if (TWinControl(TL[i]).ControlCount > 0) and
           not (TWinControl(TL[i]) is TevDBGrid) and
           not (TWinControl(TL[i]) is TevDBLookupCombo) and
           not (TWinControl(TL[i]) is TevDBRadioGroup) then
          Result := PutFocus(TWinControl(TL[i]))
        else if not (TWinControl(TL[i]) is TRadioButton) or
                TRadioButton(TL[i]).Checked then
        begin
          TWinControl(TL[i]).SetFocus;
          Result := True;
        end;
        if Result then
          Exit;
      end;
  finally
    TL.Free;
  end;
end;


type
  TScreenSnapShotAction = class(TAction)
  private
    procedure SendScreenShot(Sender: TObject);
  public
    procedure AfterConstruction; override;
  end;

{ TScreenSnapShotAction }

procedure TScreenSnapShotAction.AfterConstruction;
begin
  inherited;
  ShortCut := TextToShortCut('Ctrl+Alt+S');
  OnExecute := SendScreenShot;
end;

procedure TScreenSnapShotAction.SendScreenShot(Sender: TObject);
var
  FileName: string;
  t: TStringList;
  b: TBitMap;
  j: TJPEGImage;
begin
  inherited;
  FileName := AppTempFolder + '_ScreenShot.jpg';
  try
    t := TStringList.Create;
    b := TBitMap.Create;
    j := TJPEGImage.Create;
    try
      ScreenShot(0, 0, Screen.Width, Screen.Height, b);
      j.CompressionQuality := 50;
      j.Assign(b);
      j.SaveToFile(FileName);
      t.Add(FileName);
      MailTo(nil, nil, nil, t, 'ScreenShot - ' + DateTimeToStr(Now), '');
    finally
      t.Free;
      b.Free;
      j.Free;
    end;
  finally
    SysUtils.DeleteFile(FileName);
  end;
end;

procedure AddScreenShotHotKey(AOwner: TComponent);
var
  al: TActionList;
begin
  if Assigned(AOwner) then
  begin
    al := TActionList.Create(AOwner);
    TScreenSnapShotAction.Create(al).ActionList := al;
  end
end;

procedure GrayOut(const control: TControl; const grayed: Boolean);
begin
  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) then
    if not grayed then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      //SetEnumProp( control, 'ParentColor', 'true' );  original code,
      //changed to make disabled controls a consistent color per Jake
      SetOrdProp( control, 'Color', clBtnFace );
    end;
end;


procedure GrayOutControl( control: TControl; grayed: boolean );
begin
  if IsPublishedProp( control, 'Enabled' )  then
    SetEnumProp( control, 'Enabled', GetEnumNAme( TypeInfo(boolean), ord(not grayed)) );
  if grayed then
    if IsPublishedProp( control, 'DataSource' ) and IsPublishedProp( control, 'DataField' ) then
      if Assigned(GetObjectProp( control, 'DataSource' ) as TDataSource) then
        with GetObjectProp( control, 'DataSource' ) as TDataSource do
          if State in [dsEdit, dsInsert] then
            if not DataSet.FieldByName( GetStrProp(control, 'DataField') ).IsNull then
              DataSet.FieldByName( GetStrProp(control, 'DataField') ).Clear;
  GrayOut( control, grayed );
end;

procedure SetFieldRequired(aReq:boolean;aField:TField;evLabel:TevLabel=nil);
var
Red:boolean;
aLabel:string;
begin
  if(assigned(evLabel)) then begin
     aLabel := trim(evLabel.Caption);
     Red := (aLabel[1]='~');
     if aReq then
     begin
        if not Red then  aLabel := '~'+aLabel;
     end else
     begin
        if Red  then aLabel := Copy(aLabel, 2, Length(aLabel)-1);
     end;
     evLabel.Caption := aLabel;
  end;
  aField.Required := aReq;
end;


{ TDBDTFilterContext }

procedure TDBDTFilterContext.CO_DIVISIONFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  FDBDTCache.FilterDivision( Self, DataSet, Accept );
end;

procedure TDBDTFilterContext.CO_BRANCHFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  FDBDTCache.FilterBranch( Self, DataSet, Accept );
end;

procedure TDBDTFilterContext.CO_DEPARTMENTFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  FDBDTCache.FilterDepartment( Self, DataSet, Accept );
end;

procedure TDBDTFilterContext.CO_TEAMFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  FDBDTCache.FilterTeam( Self, DataSet, Accept );
end;

constructor TDBDTFilterContext.Create(t: IDBDTCache);
begin
  FDBDTCache := t;
  FDivNull := true;
  FBrhNull := true;
  FDepNull := true;
end;

function TDBDTFilterContext.SetParams(aDiv, aBrh, aDep, aTeam: TIntegerField): boolean;
begin
  Result := false;

  if FDivNull <> aDiv.IsNull then
  begin
    Result := true;
    FDivNull := aDiv.IsNull;
  end;

  if not FDivNull then
    if FDiv <> aDiv.AsInteger then
    begin
      Result := true;
      FDiv := aDiv.AsInteger;
    end;

  if FBrhNull <> aBrh.IsNull then
  begin
    Result := true;
    FBrhNull := aBrh.IsNull;
  end;

  if not FBrhNull then
    if FBrh <> aBrh.AsInteger then
    begin
      Result := true;
      FBrh := aBrh.AsInteger;
    end;

  if FDepNull <> aDep.IsNull then
  begin
    Result := true;
    FDepNull := aDep.IsNull;
  end;

  if not FDepNull then
    if FDep <> aDep.AsInteger then
    begin
      Result := true;
      FDep := aDep.AsInteger;
    end;
end;


function ForSelectedInternal( ds: TEvClientDataSet; list: TList; selproc: TOnSelectedProc; selEvent: TOnSelected; par: TObject; anOptions: TfsOptions ): boolean;

  function CallProcBack: boolean;
  begin
    if assigned( selproc ) then
      Result := selproc( ds, par )
    else if assigned( selevent ) then
      Result := selevent( ds )
    else
      Result := true;
  end;

  function DoForSelected: boolean;
  var
    curPos: TBookmark;
    i: integer;
  begin
    Result := true;
    ds.First;
    while not ds.Eof do
    begin
      curPos := ds.GetBookmark;
      try
        for i := 0 to list.Count-1 do
          if ds.CompareBookmarks( curPos, list[i] ) = 0 then
          begin
            Result := Result and CallProcBack;
            if not Result then
              Exit;
          end;
      finally
        ds.FreeBookmark( curPos );
      end;
      ds.Next;
    end;
  end;

  function HandleEmptyListAndDoForSelected: boolean;
  begin
    if assigned( list ) and (list.Count > 0) then
      Result := DoForSelected
    else if fsForCurrentIfListIsEmpty in anOptions then
      Result := CallProcBack
    else
      Result := true;
  end;

  function DisableControlsAndDoForSelected: boolean;
  begin
    if fsDisableControls in anOptions then
    begin
      ds.DisableControls;
      try
        Result := HandleEmptyListAndDoForSelected;
      finally
        ds.EnableControls;
      end;
    end
    else
      Result := HandleEmptyListAndDoForSelected;
  end;
var
  savePos: TBookmark;
begin
  if  assigned( ds ) and
     ( assigned( selproc ) or assigned( selevent ) ) then
  begin
    if fsSavePosition in anOptions then
    begin
      savePos := ds.GetBookmark;
      try
        try
          Result := DisableControlsAndDoForSelected;
        finally
          if ds.BookmarkValid( savePos ) then
            ds.GotoBookmark( savePos );
        end;
      finally
        ds.FreeBookmark( savePos );
      end;
    end
    else
      Result := DisableControlsAndDoForSelected;
  end
  else
    Result := true; //it wasn't aborted by callback
end;

function ForSelectedInGrid( grid: TevDbGrid; selproc: TOnSelectedProc; selEvent: TOnSelected; par: TObject; anOptions: TfsOptions  ): boolean;
begin
  if assigned( grid ) and assigned( grid.DataSource ) then
    Result := ForSelectedInternal( grid.DataSource.DataSet as TEvClientDataSet, grid.SelectedList, selproc, selevent, par, anOptions )
  else
    Result := true; //it wasn't aborted by callback
end;

function ForSelected( grid: TevDbGrid; selproc: TOnSelectedProc; par: TObject; anOptions: TfsOptions ): boolean;
begin
  Result := ForSelectedInGrid(grid, selproc, nil, par, anOptions);
end;

function ForSelected(grid: TevDbGrid; selevent: TOnSelected; anOptions: TfsOptions): boolean;
begin
  Result := ForSelectedInGrid( grid, nil, selevent, nil, anOptions );
end;

function doOnSelected( ds: TevClientDataset; par: TObject ): boolean;
var
  j: integer;
  res: TevClientDataSet;
begin
  res := (par as TevClientDataSet);
  res.Insert;
  for j := 0 to ds.Fields.Count - 1 do
    res[ds.Fields[j].FullName] := ds.Fields[j].Value;
  res.Post;
  Result := true;
end;

function CreateDataSetWithSelectedRows( Source: TEvClientDataSet; bookmarks: TList ): TEvClientDataSet;
begin
  Result := TEvClientDataSet.Create( nil );
  Result.SkipFieldCheck := True;
  Result.FieldDefs := Source.FieldDefs;
  Result.CreateDataSet;
  ForSelectedInternal( Source, bookmarks, doOnSelected, nil, Result, [] );
end;

const
  sDynamicPopupMenuName = 'DynamicPopupMenu';

function EnsureHasMenu( Self: TComponent; pm: TPopupMenu ): TPopupMenu;
begin
  Result := pm;
  if not Assigned( Result ) then begin
    Result := Self.FindComponent(sDynamicPopupMenuName) as TPopupMenu;
    if not Assigned( Result ) then begin
      Result := TPopupMenu.Create(Self);
      Result.Name := sDynamicPopupMenuName;
    end;
  end;
end;


function EnsureHasMenuItem( has: boolean; Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu;
var
  mi: TMenuItem;
  i: integer;
begin
  Result := pm;
  if has then
  begin
    Result := EnsureHasMenu( Self, Result );
    mi := nil;
    for i := 0 to Result.Items.Count-1 do
      if Result.Items[i].Action = actionInstance then
      begin
        mi := Result.Items[i];
        break;
      end;
    if not assigned( mi ) then begin
      mi := TMenuItem.Create(Self);
      mi.Action := actionInstance;
      Result.Items.Add(mi);
    end;
  end
  else
  begin
    if not assigned( Result ) then
      Result := Self.FindComponent( sDynamicPopupMenuName ) as TPopupMenu;
    if assigned(Result) then
      for i := 0 to Result.Items.Count-1 do
        if Result.Items[i].Action = actionInstance then
        begin
          Result.Items[i].Free; //calls Delete
          break;
        end;
  end;
end;

function EnsureHasMenuItem( has: boolean; Self: TComponent; pm: TPopupMenu; aHandler: TNotifyEvent; aCaption: string; aShortCut: string; achecked: boolean; anEnabled: boolean  ): TPopupMenu;//gdy24

  function MethodsAreEqual( m1, m2: TMethod ): boolean;
  begin
    Result := (m1.Code = m2.Code) and (m1.Data = m2.Data);
  end;

var
  mi: TMenuItem;
  i: integer;
begin
  Result := pm;
  if has then
  begin
    Result := EnsureHasMenu( Self, Result );
    mi := nil;
    for i := 0 to Result.Items.Count-1 do
      if MethodsAreEqual( TMethod(Result.Items[i].OnClick), TMethod(aHandler)) then
      begin
        mi := Result.Items[i];
        break;
      end;
    if not assigned( mi ) then
    begin
      mi := TMenuItem.Create(Self);
      Result.Items.Add(mi);
    end;

    with mi do
    begin
      OnClick := aHandler;
      Caption := aCaption;
      Shortcut := TextToShortcut( aShortCut );
      Enabled := anEnabled;
      Checked := aChecked;
    end;
  end
  else
  begin
    if not assigned( Result ) then
      Result := Self.FindComponent( sDynamicPopupMenuName ) as TPopupMenu;
    if assigned(Result) then
      for i := 0 to Result.Items.Count-1 do
        if MethodsAreEqual( TMethod(Result.Items[i].OnClick), TMethod(aHandler)) then
        begin
          Result.Items[i].Free; //calls Delete
          break;
        end;
  end;
end;

procedure AssignVMRClientLookups;

  function CreateDatasetFromVariant(const Owner: TComponent; const v: Variant): TEvBasicClientDataSet;
  begin
    Result := TEvBasicClientDataSet.Create(Owner);
    Result.Data := v;
  end;

var
  i: Integer;
  v: Variant;
begin
  v := CreateLookupData(DM_CLIENT.CL_MAIL_BOX_GROUP);
  for i := 0 to ts.ControlCount-1 do
    if ts.Controls[i] is TevDBLookupCombo then
      if TevDBLookupCombo(ts.Controls[i]).LookupTable.Name = DM_CLIENT.CL_MAIL_BOX_GROUP.Name then
      begin
        TevDBLookupCombo(ts.Controls[i]).LookupTable := nil;
        if ts.Controls[i].FindComponent(DM_CLIENT.CL_MAIL_BOX_GROUP.Name) <> nil then
          ts.Controls[i].FindComponent(DM_CLIENT.CL_MAIL_BOX_GROUP.Name).Free;
        TevDBLookupCombo(ts.Controls[i]).LookupTable := CreateDatasetFromVariant(ts.Controls[i], v);
        TevDBLookupCombo(ts.Controls[i]).LookupTable.Name := DM_CLIENT.CL_MAIL_BOX_GROUP.Name;
      end;
end;

function CheckGlSetupExist(DS: TevClientDataSet;aFields: array of string;ShowMes:boolean = true): boolean;
var i:integer;
   BM: TBookMark;
begin
   result := false;
   DS.DisableControls;
   try
     BM := DS.GetBookmark();
     with DS do
     begin
       first;
       while not EOF do
       begin
         for i := Low(aFields) to High(aFields) do
         begin
          if(FieldByName(aFields[i]).AsString <>'') then
          begin
            Result := true;
            break;
          end;
         end;
         if Result then break;
         next;
       end;
     end;
     DS.GotoBookmark(BM);
     DS.FreeBookmark(BM);
   finally
     DS.EnableControls;
   end;
   if  ShowMes and Result then EvMessage(sDontForgetSetupGL);
end;

function CheckGlSetuped(DS: TevClientDataSet;aFields: array of string): boolean;
var i:integer;
begin
   result := False;
   DS.UpdateRecord;
   with DS do
   begin
      for i := Low(aFields) to High(aFields) do
      begin
         if(FieldByName(aFields[i]).AsString <> '') then
          begin
            Result := true;
            break;
          end;
      end;
   end;
end;

function DSIsActive( ds: TDataSet ): boolean; overload;
begin
  Result := assigned(ds) and ds.Active;
end;

function DSIsActive( ds: TDataSource ): boolean; overload;
begin
  Result :=  assigned(ds) and assigned(ds.DataSet) and ds.DataSet.Active;
end;


procedure SetSelectedFromFields(const Grid: TevDBGrid; const DataSet: TevClientDataSet; const HideFields: array of ShortString);
  function IsNotHidden(const FieldName: string): Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := 0 to High(HideFields) do
      if UpperCase(HideFields[i]) = UpperCase(FieldName) then
      begin
        Result := False;
        Break;
      end;
  end;
  function Parse_(const s: string): string;
  var
    i: Integer;
  begin
    Result := s;
    for i := 1 to Length(Result) do
      if Result[i] = '_' then Result[i] := ' ';
  end;
var
  i: Integer;
  s: string;
begin
  Grid.Selected.Clear;
  for i := 0 to DataSet.Fields.Count-1 do
    if DataSet.Fields[i].Visible
    and (DataSet.Fields[i].DataType in [ftString, ftCurrency, ftFloat, ftInteger, ftDateTime, ftDate, ftTime])
    and (UpperCase(Copy(DataSet.Fields[i].FieldName, Length(DataSet.Fields[i].FieldName)-3, 4)) <> '_NBR')
    and IsNotHidden(DataSet.Fields[i].FieldName) then
    begin
      if DataSet.Fields[i].DisplayName <> DataSet.Fields[i].FieldName then
        s := DataSet.Fields[i].DisplayName
      else
        s := Parse_(UpperCase(DataSet.Fields[i].FieldName[1])+ LowerCase(Copy(DataSet.Fields[i].FieldName, 2, 255)));
      Grid.Selected.Append(DataSet.Fields[i].FieldName+ #9+ IntToStr(DataSet.Fields[i].DisplayWidth)+ #9+ s+ #9'F');
    end;
  Grid.ApplySelected;
end;


//AFrameMenuPath = <Button> - <MenuItem>[ - <MenuItem>]
procedure ActivateFrameAs(const AFrameMenuPath: String; const AParams: array of Variant; const AContext: Integer = 0);
var
  i: Integer;
begin
  SetLength(FFrameActivateParamsRec.Params, Length(AParams));
  for i := Low(AParams) to High(AParams) do
    FFrameActivateParamsRec.Params[i] := AParams[i];
  FFrameActivateParamsRec.Context := AContext;
  FFrameActivateParamsRec.FramePath := AFrameMenuPath;
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_FRAME_PARAMS, Integer(@FFrameActivateParamsRec), 0);
end;

function GetFrameClassForTable( tablename: string ): TClass;
const
  NonStandardTableEditors: array [0..10] of
    record
      TableName: string;
      FrameClass: string;
    end = (
      ( TableName: 'CL_3_PARTY_SICK_PAY_ADMIN';   FrameClass: 'TEDIT_CL_3RDPARTY_SICK_ADMIN'),
      ( TableName: 'CL_DELIVERY_GROUP';           FrameClass: 'TEDIT_CL_DELIVERY_GROUPS'),
      ( TableName: 'CL_E_D_GROUPS';               FrameClass: 'TEDIT_CL_ED_GROUPS'),
      ( TableName: 'CO_E_D_CODES';                FrameClass: 'TEDIT_CO_ED_CODES'),
      ( TableName: 'CO_SUI';                      FrameClass: 'TEDIT_CO_STATES'),
      ( TableName: 'CO_TAX_DEPOSITS';             FrameClass: 'TEDIT_CO_TAX_DEPOSIT'),
      ( TableName: 'EE';                          FrameClass: 'TEDIT_EE_AND_CL_PERSON'),
      ( TableName: 'EE_CHILD_SUPPORT_CASES';      FrameClass: 'TEDIT_EE_CHILD_SUPPORT'),
      ( TableName: 'SB_REPORT_WRITER_REPORTS';    FrameClass: 'TEDIT_SB_RWREPORTS'),
      ( TableName: 'SY_REPORT_WRITER_REPORTS';    FrameClass: 'TEDIT_SY_RWREPORTS'),
      ( TableName: 'EEStatesDS';                  FrameClass: 'TEDIT_CO_STATES')
  );
var
  i: integer;
  frameclass: string;
begin
  frameclass := 'TEDIT_'+tablename;
  for i := 0 to high(NonStandardTableEditors) do
    if NonStandardTableEditors[i].TableName = tablename then
    begin
      frameclass := NonStandardTableEditors[i].FrameClass;
      break;
    end;
  Result := GetClass(frameclass);
end;

procedure DoHint(AControl: TControl);
var
  pt: TPoint;
begin
  pt := AControl.ClientToScreen(Point(1,1)) ;
  Application.ActivateHint(pt) ;
end;

procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String; const ACustomOptions: IisListOfValues);
var
  AffectedFields: TisCommaDilimitedString;
begin
  ShowVersionedFieldEditor(ADataSet, AField, AffectedFields, False, False, ACustomOptions);
end;

procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String;
  const DefaultToQuarters, ForceQuarters: Boolean; const ACustomOptions: IisListOfValues);
var
  AffectedFields: TisCommaDilimitedString;
begin
  ShowVersionedFieldEditor(ADataSet, AField, AffectedFields, DefaultToQuarters, ForceQuarters, ACustomOptions);
end;

procedure ShowVersionedFieldEditor(const ADataSet: TevClientDataSet; const AField: String; out AAffectedFields: TisCommaDilimitedString;
  const DefaultToQuarters, ForceQuarters: Boolean; const ACustomOptions: IisListOfValues);
var
  tbl: String;
  Fld: TField;
  bReadOnly: Boolean;
  EditorClass: TevVersionedFieldBaseClass;
  Options: IisParamsCollection;
begin
  Assert(Assigned(ADataSet));

  AAffectedFields := '';
  tbl := ADataSet.TableName;
  Fld := ADataSet.FindField(tbl + '_nbr');

  if Assigned(Fld) and (ADataSet.RecordCount > 0) then
  begin
    bReadOnly :=false;
    if (ctx_AccountRights.Functions.GetState('UPDATE_AS_OF') <> stEnabled) and
       (not Contains(SET_EFFECTIVE_PERIOD_FOR_LOCALS, tbl +'.'+ AField + ',')) and
       (not Contains(UPDATE_ACA_AS_OF, tbl +'.'+ AField + ',')) then
      bReadOnly := true;

    if (ctx_AccountRights.Functions.GetState('USER_CAN_EDIT_TAX_EXEMPTIONS') <> stEnabled) and
       Contains(USER_CAN_EDIT_TAX_EXEMPTIONS, tbl +'.'+ AField + ',') then
       bReadOnly := true;

    if Contains(BLOCK_CHANGING_OF_SSN, tbl +'.'+ AField + ',') and
       (ctx_AccountRights.Functions.GetState('BLOCK_CHANGING_OF_SSN') = stEnabled) then
       bReadOnly := true;

    if Contains(SET_EFFECTIVE_PERIOD_FOR_LOCALS, tbl +'.'+ AField + ',') and
       (ctx_AccountRights.Functions.GetState('SET_EFFECTIVE_PERIOD_FOR_LOCALS') <> stEnabled) and
       (ctx_AccountRights.Functions.GetState('UPDATE_AS_OF') <> stEnabled) then
       bReadOnly := true;

    if Contains(UPDATE_ACA_AS_OF, tbl +'.'+ AField + ',') and
       (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled) then
       bReadOnly := true;

    bReadOnly := (ADataSet.State <> dsBrowse) or (ADataSet.ChangeCount > 0) or (ADataSet.AsOfDate <> SysDate) or ADataSet.ReadOnly or bReadOnly;

    EditorClass := TevVersionedFieldBase.GetEditorClass(tbl, AField, AAffectedFields);

    if Assigned(ACustomOptions) then
    begin
      Options := TisParamsCollection.Create;
      Options.AddParams(AField).Assign(ACustomOptions);
    end;

    if EditorClass.ShowField(tbl, AAffectedFields, Fld.AsInteger, bReadOnly, DefaultToQuarters, ForceQuarters, Options) then
      ADataSet.RefreshFieldValue(AAffectedFields);
  end;
end;

procedure ShowVersionedFieldEditor(const AControl: TControl);
var
  PM: TPopupMenu;
  i: Integer;
begin
  if not Assigned(AControl) or not IsPublishedProp(AControl, 'PopupMenu') then
    Exit;

  PM  := TPopupMenu(Pointer(GetOrdProp(AControl, 'PopupMenu')));

  if not Assigned(PM) then
    Exit;

  for i := 0 to PM.Items.Count - 1 do
  begin
    if Contains(PM.Items[i].Caption, 'Effective Period') then
    begin
      PM.PopupComponent := AControl;
      PM.Items[i].Click;
      break;
    end;
  end;
end;

function GetExternalLinks: IisParamsCollection;
var
  i: Integer;
  t: IisStringListRO;
  s: String;
  LV: IisListOfValues;
  Pict: IisStream;
begin
  Result := TisParamsCollection.Create;

  // TODO: Remove after Plymouth
  // Legacy format of External Links
  t := mb_AppSettings.GetValueNames('External');
  for i := 0 to t.Count - 1 do
    if t[i] <> 'Default' then
    begin
      s := 'External\' + t[i];

      LV := Result.AddParams(IntToStr(i + 1));
      LV.AddValue('Name', t[i]);
      LV.AddValue('Path', mb_AppSettings.AsString['External\' + t[i]]);
      LV.AddValue('Dashboard', False);
      LV.AddValue('Default', False);
      LV.AddValue('Icon', Pict);
    end;

  // New Format of External Links
  t := mb_AppSettings.GetChildNodes('External');
  for i := 0 to t.Count - 1 do
  begin
    s := 'External\' + t[i] +'\';

    LV := Result.AddParams(IntToStr(i + 1));
    LV.AddValue('Name', mb_AppSettings.AsString[s + 'Name']);
    LV.AddValue('Path', mb_AppSettings.AsString[s + 'Path']);
    LV.AddValue('Dashboard', mb_AppSettings.AsBoolean[s + 'Dashboard']);
    LV.AddValue('Default', mb_AppSettings.AsInteger['External\Default'] = StrToIntDef(t[i], i + 1));
    Pict := mb_AppSettings.AsBlob[s + 'Icon'];
    LV.AddValue('Icon', Pict);
  end;
end;

procedure SaveExternalLinks(const AExtLinks: IisParamsCollection);
var
  i: Integer;
  s: String;
  Pict: IisStream;
begin
  mb_AppSettings.DeleteNode('External');

  for i := 0 to AExtLinks.Count - 1 do
  begin
    s := 'External\' + IntToStr(i + 1) + '\';
    mb_AppSettings.AsString[s + 'Name'] := AExtLinks[i].Value['Name'];
    mb_AppSettings.AsString[s + 'Path'] := AExtLinks[i].Value['Path'];
    mb_AppSettings.AsBoolean[s + 'Dashboard'] := AExtLinks[i].Value['Dashboard'];

    if AExtLinks[i].Value['Default'] then
      mb_AppSettings.AsInteger['External\Default'] := i + 1;

    Pict := IInterface(AExtLinks[i].Value['Icon']) as IisStream;
    mb_AppSettings.AsBlob[s + 'Icon'] := Pict;
  end;
end;

initialization

finalization
  DeleteShellTmpFiles;

end.
