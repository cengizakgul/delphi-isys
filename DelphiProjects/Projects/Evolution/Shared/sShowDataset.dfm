object ShowDatasetForm: TShowDatasetForm
  Left = 268
  Top = 159
  Width = 696
  Height = 480
  Caption = 'ShowDataSetForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BottomPanel: TPanel
    Left = 0
    Top = 386
    Width = 688
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      688
      41)
    object OKButton: TButton
      Left = 600
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
  end
  object DBGrid: TDBGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 386
    Align = alClient
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 427
    Width = 688
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 150
      end>
  end
  object DataSource: TDataSource
    AutoEdit = False
    Left = 104
    Top = 16
  end
end
