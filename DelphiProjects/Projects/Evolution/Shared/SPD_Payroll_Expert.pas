// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Payroll_Expert;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, Db, Wwdatsrc,
  DBCtrls, wwDBlook, EvTypes, wwDBDateTimePicker,  
  Buttons, Variants, ISBasicClasses, isVCLBugFix, EvUIComponents, EvClientDataSet,
  jpeg, LMDCustomButton, LMDButton, isUILMDButton;

type
  TPayroll_Expert = class(TForm)
    Image1: TevImage;
    lablMessage: TevLabel;
    wwdsTemp: TevDataSource;
    bevlOne: TEvBevel;
    evBitBtn1: TevBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure SortChange(Sender: TObject);
  public
    { Public declarations }
    function DisplayExpert(ControlType: TControlType; MyCaption: string; MyDataSet: TevClientDataSet; Parameters: Variant): Variant;
    function DisplaySortEmployee(MyDataSet: TDataSet; EeNbr: Integer): Variant;
  end;

var
  Payroll_Expert: TPayroll_Expert;

implementation

uses
  SFieldCodeValues;

{$R *.DFM}

function TPayroll_Expert.DisplayExpert(ControlType: TControlType; MyCaption: string; MyDataSet: TevClientDataSet; Parameters: Variant): Variant;
var
  ComboBox: TevDBComboBox;
  DateEdit: TevDBDateTimePicker;
  RadioGroup: TevDBRadioGroup;
  LookupBox: TevDBLookupCombo;
  Memo: TevMemo;
  TempVar: Variant;
  Edit: TevDBEdit;
  I: Integer;
begin
  Result := Null;
  lablMessage.Caption := MyCaption;
  case ControlType of
    crComboBox:
    {Parameters:
     0. Field Name
     1. Combo Choices
    }
      begin
        ComboBox := TevDBComboBox.Create(Payroll_Expert);
        ComboBox.Style := stdctrls.csDropDownList;
        ComboBox.Parent := Payroll_Expert;
        ComboBox.Left := 16;
        ComboBox.Top := 200;
        ComboBox.Width := 193;
        ComboBox.MapList := True;
        ComboBox.DataSource := wwdsTemp;
        ComboBox.DataField := Parameters[0];
        ComboBox.Items.Text := Parameters[1];
        ActiveControl := ComboBox;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end
        else
          ComboBox.DataSource.DataSet[ComboBox.DataField] := ComboBox.Value;
        ComboBox.Free;
      end;
    crDateEdit:
    {Parameters:
     0. Field Name
    }
      begin
        DateEdit := TevDBDateTimePicker.Create(Payroll_Expert);
        DateEdit.Parent := Payroll_Expert;
        DateEdit.Left := 16;
        DateEdit.Top := 200;
        DateEdit.Width := 193;
        DateEdit.Epoch := 0;
        DateEdit.DataSource := wwdsTemp;
        DateEdit.DataField := Parameters[0];
        ActiveControl := DateEdit;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end
        else
          DateEdit.DataSource.DataSet[DateEdit.DataField] := DateEdit.Date;
        DateEdit.Free;
      end;
    crRadioGroup:
    {Parameters:
     0. Field Name
     1. Number of Columns
     2. Items
     3. Values
    }
      begin
        RadioGroup := TevDBRadioGroup.Create(Payroll_Expert);
        RadioGroup.Parent := Payroll_Expert;
        RadioGroup.Left := 16;
        RadioGroup.Top := 184;
        RadioGroup.Width := 193;
        TempVar := Parameters[2];
        if VarArrayHighBound(TempVar, 1) > 1 then
          RadioGroup.Height := 49
        else
          RadioGroup.Height := 41;
        for I := 0 to VarArrayHighBound(TempVar, 1) do
          RadioGroup.Items.Add('&' + TempVar[I]);
        TempVar := Parameters[3];
        for I := 0 to VarArrayHighBound(TempVar, 1) do
          RadioGroup.Values.Add(TempVar[I]);
        RadioGroup.DataSource := wwdsTemp;
        RadioGroup.DataField := Parameters[0];
        RadioGroup.Columns := Parameters[1];
        RadioGroup.Caption := '';
//      ActiveControl:=RadioGroup;
        RadioGroup.TabOrder := 0;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end
        else
          if RadioGroup.Values[RadioGroup.ItemIndex] <> RadioGroup.DataSource.Dataset[RadioGroup.DataField] then
          RadioGroup.DataSource.DataSet[RadioGroup.DataField] := RadioGroup.Values[RadioGroup.ItemIndex];
        RadioGroup.Free;
      end;
    crLookupCombo:
    {Parameters:
     0. Lookup Field Name
     1. Selected Text
     2. Nothing
     - or -
     2. Data Field Name
     - or -
     2. LookupValue -and- 3. Text
     Returns LookupValue
    }
      begin
        LookupBox := TevDBLookupCombo.Create(Payroll_Expert);
        LookupBox.Parent := Payroll_Expert;
        LookupBox.Left := 16;
        LookupBox.Top := 200;
        LookupBox.Width := 193;
        LookupBox.AutoDropDown := True;
        if VarArrayHighBound(Parameters, 1) = 2 then
        begin
          LookupBox.DataSource := wwdsTemp;
          LookupBox.DataField := Parameters[2];
        end;
        if VarArrayHighBound(Parameters, 1) = 3 then
        begin
          LookupBox.LookupValue := Parameters[2];
          LookupBox.Text := Parameters[3];
        end;
        LookupBox.Selected.Text := Parameters[1];
        LookupBox.LookupField := Parameters[0];
        LookupBox.LookupTable := MyDataSet;
        LookupBox.Style := csDropDownList;
        ActiveControl := LookupBox;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end;
        Result := LookupBox.LookupValue;
        LookupBox.Free;
        MyDataSet.IndexName := '';
      end;
    crMemo:
    {Parameters:
     0. Field Name
    }
      begin
        Memo := TevMemo.Create(Payroll_Expert);
        Memo.Parent := Payroll_Expert;
        Memo.Left := 0;
        Memo.Top := 184;
        Memo.Width := 225;
        Memo.Height := 49;
//      Memo.DataSource:=wwdsTemp;
        Memo.WantReturns := False;
//      Memo.DataField:=Parameters[0];
        ActiveControl := Memo;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end
        else
          wwdsTemp.DataSet.FieldByName(Parameters[0]).Assign(Memo.Lines);
        Memo.Free;
      end;
    crEdit:
    {Parameters:
     0. Field Name
    }
      begin
        Edit := TevDBEdit.Create(Payroll_Expert);
        Edit.Parent := Payroll_Expert;
        Edit.Left := 16;
        Edit.Top := 200;
        Edit.Width := 193;
        Edit.DataSource:=wwdsTemp;
        Edit.DataField:=Parameters[0];
        ActiveControl := Edit;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end
        else
        begin
          if Edit.Text= '' then
            wwdsTemp.DataSet.FieldByName(Parameters[0]).Value := Null
          else
            wwdsTemp.DataSet.FieldByName(Parameters[0]).Value := Edit.Text;
        end;
        Edit.Free;
      end;
    crNone:
    {No Parameters}
      begin
        bevlOne.Top := 224;
        lablMessage.Height := 113;
        if ShowModal <> mrOK then
        begin
          wwdsTemp.DataSet.Cancel;
          AbortEx;
        end;
        bevlOne.Top := 176;
        lablMessage.Height := 65;
      end;
  end;
end;

function TPayroll_Expert.DisplaySortEmployee(MyDataSet: TDataSet; EeNbr: Integer): Variant;
var
  LookupBox: TevDBLookupCombo;
  SortBox: TevComboBox;
begin
  Result := Null;
  lablMessage.Caption := 'Please, select an employee for the new check';

  LookupBox := TevDBLookupCombo.Create(Payroll_Expert);
  SortBox := TevComboBox.Create(Payroll_Expert);
  try
    LookupBox.Name := 'LookupBox';
    LookupBox.Parent := Payroll_Expert;

    SortBox.Left := 16;
    SortBox.Top := 185;
    SortBox.Width := 193;
    SortBox.Parent := Payroll_Expert;
    SortBox.Items.Text := 'EE Code'#13#10'EE Name'#13#10'SSN';
    SortBox.ItemIndex := 0;
    SortBox.Style := stdctrls.csDropDownList;
    SortBox.OnChange := SortChange;
    TevClientDataSet(MyDataSet).IndexFieldNames := 'CUSTOM_EMPLOYEE_NUMBER';

    LookupBox.Left := 16;
    LookupBox.Top := 210;
    LookupBox.Width := 193;
    LookupBox.AutoDropDown := True;
    LookupBox.Selected.Text := 'Trim_Number' + #9 + '10' + #9 + 'CUSTOM_EMPLOYEE_NUMBER' + #13#10
                             + 'Employee_Name_Calculate' + #9 + '30' + #9 + 'Employee_Name_Calculate' + #13#10
                             + 'SOCIAL_SECURITY_NUMBER'#9'20'#9'SOCIAL_SECURITY_NUMBER'#13#10;
    LookupBox.LookupField := 'EE_NBR';
    LookupBox.LookupTable := MyDataSet;
    LookupBox.Style := csDropDownList;
    MyDataSet.Locate('EE_NBR', EeNbr, []);
    LookupBox.LookupValue := MyDataSet.FieldByName('EE_NBR').AsString;
    LookupBox.Text := MyDataSet.FieldByName('Trim_Number').AsString;
    ActiveControl := LookupBox;
    if ShowModal <> mrOK then
    begin
      wwdsTemp.DataSet.Cancel;
      AbortEx;
    end
    else
      wwdsTemp.DataSet.FieldByName('EE_NBR').AsInteger := MyDataSet.FieldByName('EE_NBR').AsInteger;
    Result := MyDataSet.FieldByName('EE_NBR').AsInteger;
  finally
    LookupBox.Free;
    SortBox.Free;
  end;
end;

procedure TPayroll_Expert.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) and
    not ((ActiveControl is TDBComboBox) and TCustomComboBox(ActiveControl).DroppedDown) then
    ModalResult := mrCancel;
end;

procedure TPayroll_Expert.SortChange(Sender: TObject);
var
  LookupBox: TevDBLookupCombo;
  sValue: string;
begin
  LookupBox := FindComponent('LookupBox') as TevDBLookupCombo;
  sValue := LookupBox.LookupValue;
  case TevComboBox(Sender).ItemIndex of
  0:
  begin
    TevClientDataSet(LookupBox.LookupTable).IndexFieldNames := 'CUSTOM_EMPLOYEE_NUMBER';
    LookupBox.LookupTable.Locate('EE_NBR', sValue, []);
    LookupBox.Selected.Text := 'Trim_Number' + #9 + '10' + #9 + 'CUSTOM_EMPLOYEE_NUMBER' + #13#10
                             + 'Employee_Name_Calculate' + #9 + '30' + #9 + 'Employee_Name_Calculate' + #13#10
                             + 'SOCIAL_SECURITY_NUMBER'#9'20'#9'SOCIAL_SECURITY_NUMBER'#13#10;
    LookupBox.Text := LookupBox.LookupTable.FieldByName('Trim_Number').AsString;
  end;
  1:
  begin
    TevClientDataSet(LookupBox.LookupTable).IndexFieldNames := 'Employee_Name_Calculate';
    LookupBox.LookupTable.Locate('EE_NBR', sValue, []);
    LookupBox.Selected.Text := 'Employee_Name_Calculate' + #9 + '30' + #9 + 'Employee_Name_Calculate' + #13#10
                             + 'Trim_Number' + #9 + '10' + #9 + 'CUSTOM_EMPLOYEE_NUMBER' + #13#10
                             + 'SOCIAL_SECURITY_NUMBER'#9'20'#9'SOCIAL_SECURITY_NUMBER'#13#10;
    LookupBox.Text := LookupBox.LookupTable.FieldByName('Employee_Name_Calculate').AsString;
  end;
  2:
  begin
    TevClientDataSet(LookupBox.LookupTable).IndexFieldNames := 'SOCIAL_SECURITY_NUMBER';
    LookupBox.LookupTable.Locate('EE_NBR', sValue, []);
    LookupBox.Selected.Text := 'SOCIAL_SECURITY_NUMBER'#9'20'#9'SOCIAL_SECURITY_NUMBER'#13#10
                             + 'Trim_Number' + #9 + '10' + #9 + 'CUSTOM_EMPLOYEE_NUMBER' + #13#10
                             + 'Employee_Name_Calculate' + #9 + '30' + #9 + 'Employee_Name_Calculate' + #13#10;
    LookupBox.Text := LookupBox.LookupTable.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
  end;
  end;
end;

initialization
  RegisterClass(TPAYROLL_EXPERT);

finalization
  UnregisterClass(TPAYROLL_EXPERT);

end.
