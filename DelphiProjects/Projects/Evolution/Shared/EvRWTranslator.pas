unit EvRWTranslator;

interface

uses
  SysUtils, Classes, Variants, DB,
  evUtils, EvTypes, EvStreamUtils, ISZippingRoutines, SReportSettings,
  evCommonInterfaces, isBaseClasses, evExceptions, EvContext;

resourcestring
  ECantFindTableFieldCombo = 'Cannot identify Table/Field combination to translate';
  EDataNotString = 'Variant type of parameter Data is not a string';

type
  TRWTranslator = class
  // Do not instantiate.
  // The Translate class function is called from the report writer.
  public
    class function Translate(const Table, Field: string; const Data: Variant): Variant;
  end;

  TSpecificRWTranslator = class
  // Abstract ancestor class to specific Table Field Translator classes.
  public
    function Translate(const Data: Variant): Variant; virtual; abstract;
  end;

  TSpecificRWTranslatorClass = class of TSpecificRWTranslator;

  SpecificRWTranslator = record
    Table: string;
    Field: string;
    TranslatorClass: TSpecificRWTranslatorClass;
  end;

  TRWTaskTranslator = class(TSpecificRWTranslator)
  // Class to translate SB_TASK.TASK
  public
    function Translate(const Data: Variant): Variant; override;
  end;

  TRWScheduleTranslator = class(TSpecificRWTranslator)
  // Class to translate SB_TASK.Schedule
  public
    function Translate(const Data: Variant): Variant; override;
  end;

  TRWTaxReturnDataTranslator = class(TSpecificRWTranslator)
  // Class to translate CO_TAX_RETURN_QUEUE.CL_BLOB_NBR -> DATA
  // This is an exception! Data must be data field from CL_BLOB table!
  public
    function Translate(const Data: Variant): Variant; override;
  end;

  TRWBenefitEnrollmentRequestTranslator = class(TSpecificRWTranslator)
  // Class to translate EE_CHANGE_REQUEST.REQUEST_DATA -> DATA
  public
    function Translate(const Data: Variant): Variant; override;
  end;

  TRWXmlCheckStubTranslator = class(TSpecificRWTranslator)
  // Class to translate PR_CHECK xml checkstub
  public
    function Translate(const Data: Variant): Variant; override;
  end;

const
  // Add new Table/Field combo's to this array
  SpecificRWTranslatorArray: array[0..4] of SpecificRWTranslator = (
    (Table: 'SB_TASK'; Field: 'Task'; TranslatorClass: TRWTaskTranslator),
    (Table: 'SB_TASK'; Field: 'Schedule'; TranslatorClass: TRWScheduleTranslator),
    (Table: 'CO_TAX_RETURN_QUEUE'; Field: 'Data'; TranslatorClass: TRWTaxReturnDataTranslator),
    (Table: 'EE_CHANGE_REQUEST'; Field: 'REQUEST_DATA'; TranslatorClass: TRWBenefitEnrollmentRequestTranslator),
    (Table: 'PR_CHECK'; Field: 'Data'; TranslatorClass: TRWXmlCheckStubTranslator)
    );

implementation

function TRWTaskTranslator.Translate(const Data: Variant): Variant;
var
  Task: IevTask;
  Stream: IisStream;
begin
  if not VarIsType(Data, [varStrArg, varString]) then
    raise EevException.Create(EDataNotString);

  Stream := TisStream.Create;
  Stream.AsString := Data;
  Stream.Position := 0;
  Task := ObjectFactory.CreateInstanceFromStream(Stream) as IevTask;

  Result := VarArrayCreate([0, 8], varVariant);
  Result[0] := VarArrayOf(['Caption', Task.Caption]);
  Result[1] := VarArrayOf(['TaskType', Task.TaskType]);
  Result[2] := VarArrayOf(['User', Task.User]);
  Result[3] := VarArrayOf(['Priority', Task.Priority]);
  Result[4] := VarArrayOf(['MaxThreads', Task.MaxThreads]);
  Result[5] := VarArrayOf(['DaysToStayInQueue', Task.DaysToStayInQueue]);
  Result[6] := VarArrayOf(['SendEmailNotification', Task.SendEmailNotification]);
  Result[7] := VarArrayOf(['EmailSendRule', Task.EmailSendRule]);
  Result[8] := VarArrayOf(['NotificationEmail', Task.NotificationEmail]);
end;

function TRWScheduleTranslator.Translate(const Data: Variant): Variant;
begin
  if not VarIsType(Data, [varStrArg, varString]) then
    raise EevException.Create(EDataNotString);
  Result := VarArrayCreate([0, 0], varVariant);
  Result[0] := VarArrayOf(['Schedule', ScheduleToText(Data)]);
end;

class function TRWTranslator.Translate(const Table, Field: string; const Data: Variant): Variant;
var
  I: integer;
  SpecificRWTranslator: TSpecificRWTranslator;
begin
  SpecificRWTranslator := nil;
  for I := Low(SpecificRWTranslatorArray) to
    High(SpecificRWTranslatorArray) do
  begin
    if (CompareText(Table, SpecificRWTranslatorArray[I].Table) = 0) and
      (CompareText(Field, SpecificRWTranslatorArray[I].Field) = 0) then
    begin
      SpecificRWTranslator :=
        SpecificRWTranslatorArray[I].TranslatorClass.Create;
      Break;
    end;
  end;
  if not Assigned(SpecificRWTranslator) then
    raise EevException.Create(ECantFindTableFieldCombo);
  try
    Result := SpecificRWTranslator.Translate(Data);
  finally
    FreeAndNil(SpecificRWTranslator);
  end;
end;

{ TRWTaxReturnDataTranslator }

function TRWTaxReturnDataTranslator.Translate(const Data: Variant): Variant;
var
  i: Integer;
  SrcStream, DestStream: IEvDualStream;
  sData: String;
  RepRes: TrwReportResults;

  function LayersIntoString(ALayers: TrwPrintableLayers): String;
  var
    i: TrwLayerNumber;
  begin
    Result := '';
    for i := Low(TrwLayerNumber) to High(TrwLayerNumber) do
      if i in ALayers then
      begin
        if Result <> '' then
          Result := Result + ',';
        Result := Result + IntToStr(Ord(i));
      end;
  end;


  function PagesInfoIntoArray(APagesInfo: TrwResPagesInfo): Variant;
  var
    i: Integer;
  begin
    Result := VarArrayCreate([0, APagesInfo.Count - 1], varVariant);
    for i := 0 to APagesInfo.Count - 1 do
    begin
      Result[i] := IntToStr(APagesInfo[i].WidthMM) + ',' +
                   IntToStr(APagesInfo[i].HeightMM) + ',' +
                   IntToStr(APagesInfo[i].SimplexPageCount) + ',' +
                   IntToStr(APagesInfo[i].DuplexPageCount);
    end;
  end;

begin
  if not VarIsType(Data, [varString, varOleStr]) then
    raise EevException.Create(EDataNotString);

  sData := Data;
  SrcStream := TEvDualStreamHolder.Create(Length(sData));
  SrcStream.WriteBuffer(sData[1], Length(sData));
  SrcStream.Position := 0;

  i := SrcStream.ReadInteger;
  if i = $FF then // new format
  begin
    DestStream := UnCompressStreamFromStream(SrcStream);
    RepRes := TrwReportResults.Create;
    try
      DestStream.Position := 0;
      RepRes.SetFromStream(DestStream);
      if RepRes.Count > 0 then
      begin
        Result := VarArrayCreate([0, 18], varVariant);

        DestStream := RepRes[0].Data;
        SetLength(sData, DestStream.Size);
        DestStream.Position := 0;
        DestStream.ReadBuffer(sData[1], DestStream.Size);

        Result[0] := RepRes[0].ReportName;
        Result[1] := RepRes[0].ReportType;
        Result[2] := sData;
        Result[3] := RepRes[0].Nbr;
        Result[4] := RepRes[0].Level;
        Result[5] := RepRes[0].Tag;
        Result[6] := RepRes[0].VmrTag;
        Result[7] := RepRes[0].VmrCoNbr;
        Result[8] := RepRes[0].VmrPrNbr;
        Result[9] := RepRes[0].VmrEeNbr;
        Result[10] := RepRes[0].VmrEventDate;
        Result[11] := RepRes[0].VmrJobDescr;
        Result[12] := RepRes[0].MediaType;
        Result[13] := LayersIntoString(RepRes[0].Layers);
        Result[14] := PagesInfoIntoArray(RepRes[0].PagesInfo);
        Result[15] := RepRes[0].Copies;
        Result[16] := RepRes[0].Tray;
        Result[17] := RepRes[0].OutBinNbr;
        Result[18] := RepRes[0].FileName;
      end
      else
        Result := VarArrayCreate([0, -1], varVariant);
    finally
      RepRes.Free;
    end;
  end
  else
    Assert(False);
end;

{ TRWBenefitEnrollmentRequestTranslator }

function TRWBenefitEnrollmentRequestTranslator.Translate(const Data: Variant): Variant;
var
  RequestData: IisListOfValues;
  Stream: IisStream;
  ds: IevDataset;
begin
  if not VarIsType(Data, [varStrArg, varString]) then
    raise EevException.Create(EDataNotString);

  Stream := TisStream.Create;
  Stream.AsString := Data;
  Stream.Position := 0;
  RequestData := TisListOfValues.Create;
  RequestData.ReadFromStream(Stream);

  Result := VarArrayCreate([0, 3], varVariant);
  ds := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
  Result[0] := ds.Data;
  ds := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
  Result[1] := ds.Data;
  ds := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DEPENDENT_DATASET']) as IevDataset;
  Result[2] := ds.Data;
  ds := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_BENEFICIARY_DATASET']) as IevDataset;
  Result[3] := ds.Data;
end;

{ TRWXmlCheckStubTranslator }

function TRWXmlCheckStubTranslator.Translate(const Data: Variant): Variant;
begin
  if not VarIsType(Data, [varString, varOleStr]) then
    raise EevException.Create(EDataNotString);

  Result := ctx_PayrollCheckPrint.CheckStubBlobToDatasets(Data);
end;

end.

