unit EvAuditRecordFrm;

interface

uses
  Windows, Forms, Classes, Controls, StdCtrls, Variants, EvUIComponents, SysUtils,
  Grids, Wwdbigrd, Wwdbgrid, DB, Wwdatsrc, Buttons, ExtCtrls, DateUtils,
  evDataSet, isDataSet, EvCommonInterfaces, evContext, EvUtils, EvConsts,
  isBasicUtils, EvStreamUtils, ISBasicClasses, isTypes, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isBaseClasses, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, isUIwwDBComboBox, SDataStructure, SDDClasses, Dialogs, isVCLBugFix;

type
  TevAuditRecord = class(TFrame)
    pnlParams: TevPanel;
    btnShowBlob: TevSpeedButton;
    grVersionAudit: TevDBGrid;
    dsrcVersionAudit: TevDataSource;
    cbOperationType: TevDBComboBox;
    procedure btnShowBlobClick(Sender: TObject);
    procedure grVersionAuditCellChanged(Sender: TObject);
    procedure OnRecordInteract(Sender: TObject);
    procedure grVersionAuditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grVersionAuditCreatingSortIndex(Sender: TObject;
      var AFields: String; var AOptions: TIndexOptions;
      var ADescFields: String);
    procedure grVersionAuditSortingChanged(Sender: TObject);
  private
    FTable: String;
    FFields: TisCommaDilimitedString;
    FNbr:   Integer;
    FStartDate: TisDate;
    FChangesDS: IevDataSet;
    FBlobFields: IisStringList;
    procedure ShowSelectedBlob;
    procedure BuildAuditData;
  public
    procedure AfterConstruction; override;
    procedure Init(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const AStartDate: TisDate); reintroduce;
  end;

implementation

uses EvAuditBlobFrm, EvAuditViewFrm;

{$R *.dfm}



{ TevAuditRecord }

procedure TevAuditRecord.Init(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const AStartDate: TisDate);
var
  TblClass: TddTableClass;
  RefBlobs: TddReferencesDesc;
  i: Integer;
begin
  if (FTable <> ATable) or (FNbr <> ANbr) or (FStartDate <> AStartDate) then
  begin
    FTable := ATable;
    FFields := AFields;
    FNbr := ANbr;
    FStartDate := AStartDate;

    TblClass := GetddTableClassByName(ATable);

    FBlobFields := TblClass.GetFieldNameList;
    for i := FBlobFields.Count - 1 downto 0 do
      if TblClass.GetFieldType(FBlobFields[i]) <> ftBlob then
        FBlobFields.Delete(i);

    RefBlobs := TblClass.GetRefBlobFieldList;
    for i := 0 to High(RefBlobs) do
      FBlobFields.Add(RefBlobs[i].SrcFields[0]);

    BuildAuditData;
  end;
end;

procedure TevAuditRecord.btnShowBlobClick(Sender: TObject);
begin
  ShowSelectedBlob;
end;

procedure TevAuditRecord.AfterConstruction;
begin
  inherited;
  btnShowBlob.Parent := grVersionAudit;
end;

procedure TevAuditRecord.ShowSelectedBlob;

  function GetBlobData(blob_ref: Variant): IisStream;
  begin
    if VarToStr(blob_ref) <> ''  then
      Result := ctx_DBAccess.GetBlobAuditData(FTable, FChangesDS['field_name'], blob_ref)
    else
      Result := nil;
  end;

begin
  if StartsWith(grVersionAudit.SelectedField.FieldName, 'old_') or
     StartsWith(grVersionAudit.SelectedField.FieldName, 'new_') then
    TevAuditBlob.ShowBlob(
      GetBlobData(FChangesDS['old_value']),
      GetBlobData(FChangesDS['new_value']));
end;

procedure TevAuditRecord.BuildAuditData;
begin
  FChangesDS := ctx_DBAccess.GetRecordAudit(FTable, FFields, FNbr, FStartDate);
  if Assigned(FChangesDS) then
  begin
    FChangesDS.IndexFieldNames := 'change_date;field_name';
    dsrcVersionAudit.DataSet := FChangesDS.vclDataSet;
  end
  else
    dsrcVersionAudit.DataSet := nil; 
end;

procedure TevAuditRecord.grVersionAuditCellChanged(Sender: TObject);
var
  R: TRect;
begin
  if Assigned(FBlobFields) and Assigned(FChangesDS) and (FChangesDS.RecordCount > 0) then
  begin
    if not grVersionAudit.SelectedField.IsNull and (FBlobFields.IndexOf(FChangesDS['field_name']) <> -1) and
       FinishesWith(grVersionAudit.SelectedField.FieldName, '_text') then
    begin
      R := grVersionAudit.CellRect(grVersionAudit.GetActiveCol, grVersionAudit.GetActiveRow + 1);
      R.Left := R.Right - (R.Bottom - R.Top);
      btnShowBlob.BoundsRect := R;
      btnShowBlob.Visible := True;
    end
    else
      btnShowBlob.Visible := False;
  end
  else
    btnShowBlob.Visible := False;
end;

procedure TevAuditRecord.OnRecordInteract(Sender: TObject);
begin
  if FChangesDS.RecNo > 0 then
    if not AnsiSameText(FChangesDS['field_name'], FFields) then
      TevAuditView.ShowFieldAudit(FTable, FChangesDS['field_name'], FNbr, FStartDate);
end;

procedure TevAuditRecord.grVersionAuditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    OnRecordInteract(Self);
end;

procedure TevAuditRecord.grVersionAuditCreatingSortIndex(Sender: TObject; var AFields: String; var AOptions: TIndexOptions; var ADescFields: String);
begin
  if (AFields <> '') and not AnsiSameText(AFields, 'change_date') or
     (ADescFields <> '') and not AnsiSameText(ADescFields, 'change_date')
  then
  begin
    AFields := '';
    ADescFields := '';
  end
  else
  begin
    AFields := 'change_date;field_name';
    if ixDescending in AOptions then
    begin
      AOptions :=  AOptions - [ixDescending];
      ADescFields := 'change_date';
    end;
  end;
end;

procedure TevAuditRecord.grVersionAuditSortingChanged(Sender: TObject);
begin
  FChangesDS.First;
end;

end.

