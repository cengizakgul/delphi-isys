// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCopyWizard;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, Wwdbigrd, Wwdbgrid,  ComCtrls, StdCtrls, Buttons,
  ExtCtrls, Db, Wwdatsrc, EvUtils, SDataStructure, evTypes,
  Variants, EvConsts, StrUtils, SDDClasses, EvContext,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvMainboard,
  ISDataAccessComponents, EvDataAccessComponents, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isBaseClasses;

type
  TvsField = record
    FName: String;
    FMandatory: Boolean;
  end;

  TexFieldsArray = array of TvsField;

  TKey = record
    NewKey: Integer;
    RecNo: Integer;
    OldKey: Integer;
  end;

  TSecPassField =  record
    PrTableName:string; // Primary table
    SlTableName:string; // Slave table
    FieldName:string;// field name in Primary table
  end;

  TSecPassFieldValue =  record
    SecPassField:TSecPassField;
    Key:TKey;
    PrKey:integer; // Key _NBR in Primary table
    NewValue:integer;
    OldValue:integer;
    KeyIndex:integer;
  end;

  FDynamicFields = record
    Table: string;
    Field: string;
    Nbr: integer;
    value: variant;
  end;

  TDynamicFieldUpdate = array of FDynamicFields;


  TKeyList = array of TKey;

  TvsTableInf = Class(TComponent)
  private
    blobNbrList: IisIntegerList;
    KeyList: TKeyList;
    SecPassFields:array of TSecPassField;
    SecPassValues: array of TSecPassFieldValue ;
    procedure GetBaseData;
    function CheckRecord(DS: TevClientDataSet): Boolean;
  public
    Name: String;
    Filter: String;
    IgnoreFields: array of String;
    FirstPass: Boolean;
    ExcludeList: TexFieldsArray;
    DSBase: TevClientDataSet;
    procedure AssignField(aField:TField;aValue:variant);
    constructor CreateT(AOwner: TComponent; T: String; bNbr : IisIntegerList);
  end;
  TTableList = array of TvsTableInf;

  TtList = record
    CLBase, COBase: Integer;
    CLNew, CONew: Integer;
    TableList: TTableList;
  end;
  TatList = TtList;



  TDBLevel = (evClient, evCompany, evEmployee);

  TCopyWizard = class(TForm)
    Panel1: TevPanel;
    Panel2: TevPanel;
    btBack: TevBitBtn;
    btNext: TevBitBtn;
    btCancel: TevBitBtn;
    Panel4: TevPanel;
    pctCopyWizard: TevPageControl;
    tshSelectBase: TTabSheet;
    TmpDS: TevClientDataSet;
    evPanel1: TevPanel;
    gdClient: TevDBGrid;
    evSplitter1: TevSplitter;
    gdCompany: TevDBGrid;
    dsClient: TevDataSource;
    dsCompany: TevDataSource;
    evPanel2: TevPanel;
    cbClearOutFields: TCheckBox;
    lablImportFolder: TevLabel;
    cbUseImport: TCheckBox;
    editImportFolder: TevEdit;
    spbImportSourceFile: TevSpeedButton;
    DM_TEMPORARY: TDM_TEMPORARY;
    procedure FormShow(Sender: TObject);
    procedure cbUseImportClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btBackClick(Sender: TObject);
    procedure btNextClick(Sender: TObject);
    procedure spbImportSourceFileClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
  private
    NewCLId: Integer;
    FCLNumber: String;
    FCONumber: String;
    BlobNbr: IisIntegerList;
    CachedCoStates,CachedEEStates:TevClientDataSet;
  protected
    function ProcessValue(TableName: String; aField: TField): Variant; virtual;
  public
    procedure CacheCo_And_EE_States;
    property CLNumber: String read FCLNumber write FCLNumber;
    property CONumber: String read FCONumber write FCONumber;
    function CreateTableList(L: TDBLevel; TL: String): TTableList;
    function GetTableIndexByName(S: String; T: TTableList): Integer;
    function HandleClientPages(TL: TTableList; GoForward: Boolean): String;
    function CheckForRequiredValues(TableInfo: TvsTableInf): boolean;
    function GetKeyListByName(TL: TTableList; T: String): TKeyList;
    function GetNewKeyValue(K: TKeyList; RecNbr, OKey: Integer): Integer;
    procedure CleanOutFields(TableInfo: TvsTableInf; CoNbr: Integer = 0);
    procedure LoadExcludeList(TableInfo: TvsTableInf);
    procedure LoadValuesFromFile(TableInfo: TvsTableInf);
    function CreateNewDBStructure(CLList: TtList; CoList: TtList; Level: TDBLevel): Integer;
    function IsValidChildTable(FName: String; TL: TTableList; Level: TDBLevel; var ActualTable: String): boolean;
    procedure SetFocusOnActivePage;
  end;

function NbrInList(Nbr: Integer): Boolean;

const
  SecPassFieldsDefinition : array[0..7] of TSecPassField = (
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'AGENCY_CHECK_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'PR_CHECK_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'PR_REPORT_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'PR_REPORT_SECOND_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'TAX_CHECK_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'TAX_EE_RETURN_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'TAX_RETURN_MB_GROUP_NBR'),
   (PrTableName:'CL';SlTableName:'CL_MAIL_BOX_GROUP';FieldName:'TAX_RETURN_SECOND_MB_GROUP_NBR')
  );

  BlobFieldList  = 'CL_BANK_ACCOUNT.LOGO_CL_BLOB_NBR,CL_BANK_ACCOUNT.SIGNATURE_CL_BLOB_NBR,EE_ADDITIONAL_INFO.INFO_BLOB,';

  TablesToCopyCLList = 'CL' + #13 +
                       'CL_MAIL_BOX_GROUP' + #13 +
                       'CL_BANK_ACCOUNT' + #13 +
                       'CL_BILLING' + #13 +
                       'CL_DELIVERY_METHOD' + #13 +
                       'CL_DELIVERY_GROUP' + #13 +
                       'CL_AGENCY' + #13 +
                       'CL_E_D_GROUPS' + #13 +
                       'CL_PENSION' + #13 +
                       'CL_PIECES' + #13 +
                       'CL_TIMECLOCK_IMPORTS' + #13 +
                       'CL_FUNDS' + #13 +
                       'CL_E_DS' + #13 +
                       'CL_E_D_GROUP_CODES' + #13 +
                       'CL_E_D_LOCAL_EXMPT_EXCLD' + #13 +
                       'CL_E_D_STATE_EXMPT_EXCLD' + #13 +
                       'CL_3_PARTY_SICK_PAY_ADMIN' + #13 +
                       'CL_REPORT_WRITER_REPORTS'+ #13 +
                       'CL_MAIL_BOX_GROUP_OPTION' + #13 +
                       'CL_BLOB';

  TablesToCopyCOList = 'CL_MAIL_BOX_GROUP'  + #13 +
                       'CL_AGENCY' + #13 +
                       'CO'  + #13 +
                       'CO_LOCATIONS'  + #13 +
                       'CO_ADDITIONAL_INFO_NAMES'  + #13 +
                       'CO_LOCAL_TAX'  + #13 +
                       'CO_PAY_GROUP'  + #13 +
                       'CO_PENSIONS'  + #13 +
                       'CO_PHONE'  + #13 +
                       'CO_PR_CHECK_TEMPLATES'  + #13 +
                       'CO_SALESPERSON'  + #13 +
                       'CO_SALESPERSON_FLAT_AMT'  + #13 +
                       'CO_SERVICES'  + #13 +
                       'CO_ENLIST_GROUPS'  + #13 +
                       'CO_AUTO_ENLIST_RETURNS'  + #13 +
                       'CO_STATES'  + #13 +
                       'CO_SUI'  + #13 +
                       'CO_WORKERS_COMP'  + #13 +
                       'CO_TIME_OFF_ACCRUAL'  + #13 +
                       'CO_TIME_OFF_ACCRUAL_RATES'  + #13 +
                       'CO_SHIFTS'  + #13 +
                       'CO_JOBS'  + #13 +
                       'CO_E_D_CODES'  + #13 +
                       'CO_BATCH_LOCAL_OR_TEMPS'  + #13 +
                       'CO_BATCH_STATES_OR_TEMPS'  + #13 +
                       'CO_JOBS_LOCALS'  + #13 +
                       'CO_DIVISION'  + #13 +
                       'CO_DIVISION_LOCALS'  + #13 +
                       'CO_DIV_PR_BATCH_DEFLT_ED'  + #13 +
                       'CO_BRANCH'  + #13 +
                       'CO_BRANCH_LOCALS'  + #13 +
                       'CO_BRCH_PR_BATCH_DEFLT_ED'  + #13 +
                       'CO_DEPARTMENT'  + #13 +
                       'CO_DEPARTMENT_LOCALS'  + #13 +
                       'CO_DEPT_PR_BATCH_DEFLT_ED'  + #13 +
                       'CO_TEAM'  + #13 +
                       'CO_TEAM_LOCALS'  + #13 +
                       'CO_TEAM_PR_BATCH_DEFLT_ED' + #13 +
                       'CO_GENERAL_LEDGER'+ #13 +
                       'CO_REPORTS'  + #13 +
                       'CO_PR_BATCH_DEFLT_ED'  + #13 +
                       'CO_PR_FILTERS'  + #13 +
                       'CO_BENEFIT_CATEGORY' + #13 +
                       'CO_BENEFITS' + #13 +
                       'CO_BENEFIT_SUBTYPE' + #13 +
                       'CO_BENEFIT_RATES';

  TablesToCopyEEList = 'CL_PERSON'  + #13 +
                       'CL_PERSON_DEPENDENTS'  + #13 +
                       'CL_E_D_GROUPS'  + #13 +
                       'EE'  + #13 +
                       'EE_ADDITIONAL_INFO'  + #13 +
                       'EE_AUTOLABOR_DISTRIBUTION'  + #13 +
                       'EE_BENEFITS'  + #13 +
                       'EE_CHILD_SUPPORT_CASES'  + #13 +
                       'EE_BENEFIT_PAYMENT'  + #13 +
                       'EE_BENEFICIARY'  + #13 +
                       'EE_DIRECT_DEPOSIT'  + #13 +
                       'EE_EMERGENCY_CONTACTS'  + #13 +
                       'EE_LOCALS'  + #13 +
                       'EE_PENSION_FUND_SPLITS'  + #13 +
                       'EE_PIECE_WORK'  + #13 +
                       'EE_RATES'  + #13 +
                       'EE_STATES'  + #13 +
                       'EE_TIME_OFF_ACCRUAL'  + #13 +
                       'EE_WORK_SHIFTS' + #13 +
                       'CL_AGENCY' + #13 +
                       'EE_SCHEDULED_E_DS'  + #13 +                       
                       'CL_BLOB';


var
  Nbrs: array of Integer;

implementation

uses
  DateUtils, SDataDictbureau, ISBasicUtils, evDataSet, EvCommonInterfaces,
  SDataDictclient, EvStreamUtils;

{$R *.DFM}

procedure TCopyWizard.FormShow(Sender: TObject);
var
  I: Integer;
begin
  NewCLId := 0;
  ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CO]);
  for I := 0 to pctCopyWizard.PageCount - 1 do
  begin
    pctCopyWizard.Pages[I].TabVisible := False;
  end;
  pctCopyWizard.ActivePageIndex := 0;
  btNext.ModalResult := mrNone;
end;

{ TvsTableInf }
// To get CO_NBR or EE_NBR
function TvsTableInf.CheckRecord(DS: TevClientDataSet): Boolean;
begin
  Result := True;
  if Length(Nbrs) = 0 then
    Exit;

  if LeftStr(DS.Name, 2) = 'CO' then
  begin
    if DS.Name = 'CO_SALESPERSON_FLAT_AMT' then
    begin
      DM_COMPANY.CO_SALESPERSON.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_SALESPERSON.Lookup('CO_SALESPERSON_NBR', DS['CO_SALESPERSON_NBR'], 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_TIME_OFF_ACCRUAL_RATES' then
    begin
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_TIME_OFF_ACCRUAL.Lookup('CO_TIME_OFF_ACCRUAL_NBR', DS['CO_TIME_OFF_ACCRUAL_NBR'], 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_BATCH_LOCAL_OR_TEMPS' then
    begin
      DM_COMPANY.CO_LOCAL_TAX.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', DS['CO_LOCAL_TAX_NBR'], 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_BATCH_STATES_OR_TEMPS' then
    begin
      DM_COMPANY.CO_STATES.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DS['CO_STATES_NBR'], 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_BENEFIT_SUBTYPE' then
    begin
      DM_COMPANY.CO_BENEFITS.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_BENEFITS.Lookup('CO_BENEFITS_NBR', DS['CO_BENEFITS_NBR'], 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_BENEFIT_RATES' then
    begin
      DM_COMPANY.CO_BENEFITS.DataRequired();
      DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_BENEFITS.Lookup('CO_BENEFITS_NBR',
                                      DM_COMPANY.CO_BENEFIT_SUBTYPE.Lookup('CO_BENEFIT_SUBTYPE_NBR', DS['CO_BENEFIT_SUBTYPE_NBR'], 'CO_BENEFITS_NBR'), 'CO_NBR'), 0));
    end
    else if DS.Name = 'CO_JOBS_LOCALS' then
    begin
      DM_COMPANY.CO_JOBS.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_JOBS.Lookup('CO_JOBS_NBR', DS['CO_JOBS_NBR'], 'CO_NBR'), 0));
    end
    else if (DS.Name = 'CO_DIVISION_LOCALS') or
            (DS.Name = 'CO_DIV_PR_BATCH_DEFLT_ED') or
            (DS.Name = 'CO_BRANCH') then
    begin
      DM_COMPANY.CO_DIVISION.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', DS['CO_DIVISION_NBR'], 'CO_NBR'), 0));
    end
    else if (DS.Name = 'CO_BRANCH_LOCALS') or
            (DS.Name = 'CO_BRCH_PR_BATCH_DEFLT_ED') or
            (DS.Name = 'CO_DEPARTMENT') then
    begin
      DM_COMPANY.CO_DIVISION.DataRequired();
      DM_COMPANY.CO_BRANCH.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR',
                                      DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', DS['CO_BRANCH_NBR'], 'CO_DIVISION_NBR'), 'CO_NBR'), 0));
    end
    else if (DS.Name = 'CO_DEPARTMENT_LOCALS') or
            (DS.Name = 'CO_DEPT_PR_BATCH_DEFLT_ED') or
            (DS.Name = 'CO_TEAM') then
    begin
      DM_COMPANY.CO_DIVISION.DataRequired();
      DM_COMPANY.CO_BRANCH.DataRequired();
      DM_COMPANY.CO_DEPARTMENT.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR',
                                      DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR',
                                      DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', DS['CO_DEPARTMENT_NBR'], 'CO_BRANCH_NBR'), 'CO_DIVISION_NBR'), 'CO_NBR'), 0));
    end
    else if (DS.Name = 'CO_TEAM_LOCALS') or
            (DS.Name = 'CO_TEAM_PR_BATCH_DEFLT_ED') then
    begin
      DM_COMPANY.CO_DIVISION.DataRequired();
      DM_COMPANY.CO_BRANCH.DataRequired();
      DM_COMPANY.CO_DEPARTMENT.DataRequired();
      DM_COMPANY.CO_TEAM.DataRequired();
      Result := NbrInList(ConvertNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR',
                                      DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR',
                                      DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR',
                                      DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', DS['CO_TEAM_NBR'], 'CO_DEPARTMENT_NBR'), 'CO_BRANCH_NBR'), 'CO_DIVISION_NBR'), 'CO_NBR'), 0));
    end
    else
      Result := NbrInList(DS.FieldByName('CO_NBR').AsInteger)
  end
  else if LeftStr(DS.Name, 2) = 'EE' then
  begin
    if DS.Name = 'EE_BENEFICIARY' then
    begin
      DM_EMPLOYEE.EE_BENEFITS.DataRequired();
      Result := NbrInList(ConvertNull(DM_EMPLOYEE.EE_BENEFITS.Lookup('EE_BENEFITS_NBR', DS['EE_BENEFITS_NBR'], 'EE_NBR'), 0));
    end
    else
      Result := NbrInList(DS.FieldByName('EE_NBR').AsInteger);
  end;

end;

constructor TvsTableInf.CreateT(AOwner: TComponent; T: String; bNbr : IisIntegerList);
var
  i:integer;
begin
  blobNbrList := bNbr;
  Name := T;
  FirstPass := True;
  SetLength(SecPassValues,0);
  SetLength(SecPassFields,0);

  DSBase := TevClientDataSet.Create(Self);
  GetBaseData;

  for i := 0 to High(SecPassFieldsDefinition) do
  begin
     if SecPassFieldsDefinition[i].PrTableName = T then
     begin
        SetLength(SecPassFields,Length(SecPassFields)+1);
        SecPassFields[High(SecPassFields)] := SecPassFieldsDefinition[i];
     end;
  end;
end;

procedure TvsTableInf.AssignField(aField:TField;aValue:variant);
  var 
    SecPasFieldIndex:integer;
  function IsSecPassField : integer;
    var i:integer;
  begin
     result := -1;
     for i:= 0 to High(SecPassFields) do
     begin
         if aField.FieldName = SecPassFields[i].FieldName then
         begin
           result := i;
           break;
         end;
     end;
  end;

begin
  SecPasFieldIndex := IsSecPassField;
  if  SecPasFieldIndex = -1 then
     aField.Value := aValue
  else
  begin
     if Name = 'CL' then
     begin
       SetLength(SecPassValues,Length(SecPassValues)+1);
       with SecPassValues[High(SecPassValues)] do
       begin
          SecPassField := SecPassFields[SecPasFieldIndex];
          PrKey := DSBase.FieldByName(Name+'_NBR').AsInteger;
          OldValue := aValue;
          KeyIndex := DSBase.RecNo - 1;
       end;
     end;
  end;
end;

procedure TvsTableInf.GetBaseData;
var
  DS: TevClientDataSet;
  SaveFilter: String;
  SaveFiltered: Boolean;
  I: Integer;

  procedure CopyBlobValue(const aFieldName: string);
  var
    BlobValue: IisStream;
  begin
    BlobValue := TisStream.Create;
    try
      BlobValue := DS.GetBlobData(aFieldName);
      BlobValue.Position := 0;
      DSBase.UpdateBlobData(aFieldName, BlobValue);
    finally
      BlobValue := nil;
    end;
  end;
begin
  DS := ctx_DataAccess.GetDataSet(GetddTableClassByName(Name));
  SaveFilter := DS.Filter;
  SaveFiltered := DS.Filtered;
  try
    DS.Filter := Filter;
    DS.Filtered := True;
    DS.OpenWithLookups := True;
    DS.DisableControls;

    if (name = 'CL_BLOB') then
      DS.DataRequired(BuildINsqlStatement('CL_BLOB_NBR', blobNbrList))
    else
      DS.DataRequired('');

    CreateNewClientDataSet(Self, DSBase, DS);
    DSBase.PictureMasks.Assign(DS.PictureMasks);
    DS.First;
    while not DS.EOF do
    begin
      if CheckRecord(DS) then
      begin
        DSBase.Insert;
        for I := 0 to DSBase.FieldCount - 1 do
        begin
          if DSBase.Fields[I].IsBlob then
            CopyBlobValue( DSBase.Fields[I].FieldName )
          else
          begin
            DSBase.Fields[I].Value := DS.FieldByName(DSBase.Fields[I].FieldName).Value;

            if Contains(BlobFieldList, Name + '.' + DSBase.Fields[I].FieldName + ',') then
              blobNbrList.Add(DS.FieldByName(DSBase.Fields[I].FieldName).AsInteger);
          end;
        end;
        DSBase.Post;
      end;
      DS.Next;
    end;
  finally
    DS.Filter := SaveFilter;
    DS.Filtered := SaveFiltered;
    DS.EnableControls;
  end;
end;

function TCopyWizard.CreateTableList(L: TDBLevel; TL: String): TTableList;
var
  S: TStringList;
  I: Integer;
  TableList: TTableList;
begin
  S := TStringList.Create;
  BlobNbr := TisIntegerList.Create;
  BlobNbr.Add(-1);
  try
    S.Text := TL;
    SetLength(TableList, S.Count);
    for I := 0  to S.Count - 1 do
      TableList[I] := TvsTableInf.CreateT(Self, S[I], BlobNbr);
  finally
    Result := TableList;
    S.Free;
  end;
end;

function TCopyWizard.GetKeyListByName(TL: TTableList; T: String): TKeyList;
var
  I: Integer;
  KL: TKeyList;
begin
  KL := Nil;
  for I := Low(TL) to High(TL) do
  begin
    if TL[I].Name = T then
    begin
      KL := TL[I].KeyList;
      Break;
    end;
  end;
  Result := KL;
end;

function TCopyWizard.GetTableIndexByName(S: String; T: TTableList): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := Low(T) to High(T) do
  begin
    if T[I].Name = S then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TCopyWizard.HandleClientPages(TL: TTableList; GoForward: Boolean): String;
var
  TableName, S: String;
  aTableInfo: TvsTableInf;
  C: TComponent;
  i, j, k: Integer;
begin
  pctCopyWizard.Visible := False;
  try
    repeat
      TableName := Copy(pctCopyWizard.ActivePage.Name, Pos('tsh', pctCopyWizard.ActivePage.Name)+3, Length(pctCopyWizard.ActivePage.Name));
      i := GetTableIndexByName(TableName, TL);
      if i = -1 then
        aTableInfo := Nil
      else
        aTableInfo := TL[i];

      if not GoForward or CheckForRequiredValues(aTableInfo) then
      begin
        if GoForward then
          pctCopyWizard.ActivePageIndex := pctCopyWizard.ActivePageIndex + 1
        else
          pctCopyWizard.ActivePageIndex := pctCopyWizard.ActivePageIndex - 1;
        if pctCopyWizard.ActivePageIndex = 0 then
        begin
          Result := '';
          Exit;
        end;
        S := Copy(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name, 4, Length(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name));
        if S = 'EndPage' then
          j := Succ(High(TL))
        else
          j := GetTableIndexByName(S, TL);
        for k := Succ(i) to Pred(j) do
        begin
          aTableInfo := TL[k];
          CleanOutFields(aTableInfo);
        end;
        if S = 'EndPage' then
        begin
          Result := S;
          Exit;
        end;
        aTableInfo := TL[j];
        Self.Caption := 'Client Copy Wizard - ' + aTableInfo.Name;
        LoadExcludeList(aTableInfo);
        CleanOutFields(aTableInfo);
        C := FindComponent('ds' + aTableInfo.Name);
        if Assigned(C) then
        begin
          TevDataSource(C).DataSet := aTableInfo.DSBase;
        end;
      end;
    until (aTableInfo.DSBase.RecordCount > 0) and Assigned(aTableInfo.ExcludeList);
  finally
    pctCopyWizard.Visible := True;
  end;
  SetFocusOnActivePage;
  Result := aTableInfo.Name;
end;


function TCopyWizard.CheckForRequiredValues(TableInfo: TvsTableInf): boolean;
var
  I: Integer;
begin
  Result := True;
  if not Assigned(TableInfo) then
    Exit;
  TableInfo.DSBase.First;
  while not TableInfo.DSBase.Eof do
  begin
    for I := Low(TableInfo.ExcludeList) to High(TableInfo.ExcludeList) do
    begin
      if (TableInfo.ExcludeList[I].FMandatory) and ((TableInfo.DSBase.FieldByName(TableInfo.ExcludeList[I].FName).IsNull) or (TableInfo.DSBase.FieldByName(TableInfo.ExcludeList[I].FName).AsString = '')) then
      begin
        Result := False;
        if pctCopyWizard.ActivePageIndex <= 1 then
          btBack.Enabled := False;
        ShowMessage('One or more required fields is missing data.');
        Exit;
      end;
    end;
    if Result = False then
      Exit;
    TableInfo.DSBase.Next;
  end;
end;

procedure TCopyWizard.CleanOutFields(TableInfo: TvsTableInf; CoNbr: Integer = 0);
var
  I: Integer;
  F: TField;
  FieldName: String;
begin
  if not TableInfo.FirstPass then
    Exit;
  TableInfo.DSBase.First;
  while not TableInfo.DSBase.Eof do
  begin
    TableInfo.DSBase.Edit;
    if cbClearOutFields.Checked then
      for I := Low(TableInfo.ExcludeList) to High(TableInfo.ExcludeList) do
      begin
        F := TableInfo.DSBase.FindField(TableInfo.ExcludeList[I].FName);
        if Assigned(F) then
          F.Clear;
      end;

    if Copy(TableInfo.Name, 1, 2) = 'CO' then
      for I := 0 to TableInfo.DSBase.FieldCount - 1 do
      begin
        FieldName := TableInfo.DSBase.Fields[I].FieldName;
        if (TableInfo.Name + '_NBR' <> FieldName)
           and (Copy(FieldName, Length(FieldName) - 3, 4) = '_NBR')
           and (Pos('CL_', FieldName) <> 0)
        then begin
          if ((TableInfo.Name = 'CO_BENEFITS') and (FieldName = 'CL_AGENCY_NBR'))
          then continue;
          TableInfo.DSBase.Fields[I].Clear;
        end;
      end;

    if TableInfo.Name = 'CO' then
    begin
      TableInfo.DSBase.FieldByName('INITIAL_EFFECTIVE_DATE').Value := BeginOfTime;
      DM_CLIENT.CL_BILLING.DataRequired('');
      TableInfo.DSBase.FieldByName('CL_BILLING_NBR').Value := DM_CLIENT.CL_BILLING.FieldByName('CL_BILLING_NBR').Value;
      DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('');
      TableInfo.DSBase.FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CL_BANK_ACCOUNT_NBR').Value;
      TableInfo.DSBase.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CL_BANK_ACCOUNT_NBR').Value;
      TableInfo.DSBase.FieldByName('BILLING_CL_BANK_ACCOUNT_NBR').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CL_BANK_ACCOUNT_NBR').Value;
      TableInfo.DSBase.FieldByName('DD_CL_BANK_ACCOUNT_NBR').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CL_BANK_ACCOUNT_NBR').Value;
      TableInfo.DSBase.FieldByName('W_COMP_CL_BANK_ACCOUNT_NBR').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CL_BANK_ACCOUNT_NBR').Value;
      DM_CLIENT.CL_AGENCY.DataRequired('');
      TableInfo.DSBase.FieldByName('WORKERS_COMP_CL_AGENCY_NBR').Value := DM_CLIENT.CL_AGENCY.FieldByName('CL_AGENCY_NBR').Value;

      TableInfo.DSBase.FieldByName('SY_ANALYTICS_TIER_NBR').Clear;
      TableInfo.DSBase.FieldByName('DASHBOARD_COUNT').Clear;
      TableInfo.DSBase.FieldByName('USERS_COUNT').Clear;
      TableInfo.DSBase.FieldByName('LOOKBACK_YEARS').Clear;
      TableInfo.DSBase.FieldByName('MOBILE_APP').Clear;
      TableInfo.DSBase.FieldByName('INTERNAL_BENCHMARKING').Clear;
      TableInfo.DSBase.FieldByName('SHOW_EIN_NUMBER_ON_CHECK').AsString := GROUP_BOX_NO

    end
    else if TableInfo.Name = 'CO_E_D_CODES' then
    begin
      DM_CLIENT.CL_E_DS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('ED_Lookup').IsNull then
        Assert(DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('ED_Lookup').Value, []), 'Cannot find client E/D ' + TableInfo.DSBase.FieldByName('ED_Lookup').Value);
      TableInfo.DSBase.FieldByName('CL_E_DS_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('ED_Lookup').Value, 'CL_E_DS_NBR');
    end
    else if TableInfo.Name = 'CO_GENERAL_LEDGER' then
    begin
      if (TableInfo.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ED) or
         (TableInfo.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ED_OFFSET) then
      begin
        DM_CLIENT.CL_E_DS.DataRequired('');
        if (not TableInfo.DSBase.FieldByName('Data_Name').IsNull) and
          (TableInfo.DSBase.FieldByName('Data_Name').AsString <> '') then
        begin
          Assert(DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('Data_Name').Value, []), 'Cannot find client E/D ' + TableInfo.DSBase.FieldByName('Data_Name').Value + ' in destination client');
          TableInfo.DSBase.FieldByName('DATA_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('Data_Name').Value, 'CL_E_DS_NBR');
        end
        else
        begin
          TableInfo.DSBase.FieldByName('DATA_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', Null, 'CL_E_DS_NBR');
        end;
      end
      else if (TableInfo.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_AGENCY_CHECK) or
              (TableInfo.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_AGENCY_CHECK_OFFSET) then
      begin
        DM_CLIENT.CL_AGENCY.DataRequired('');
        if (not TableInfo.DSBase.FieldByName('Data_Name').IsNull) and
          (TableInfo.DSBase.FieldByName('Data_Name').AsString <> '') then
        begin
          Assert(DM_CLIENT.CL_AGENCY.Locate('Agency_Name', TableInfo.DSBase.FieldByName('Data_Name').Value, []), 'Cannot find client agency ' + TableInfo.DSBase.FieldByName('Data_Name').Value + ' in destination client');
          TableInfo.DSBase.FieldByName('DATA_NBR').Value := DM_CLIENT.CL_AGENCY.Lookup('Agency_Name', TableInfo.DSBase.FieldByName('Data_Name').Value, 'CL_AGENCY_NBR');
        end
        else
        begin
          TableInfo.DSBase.FieldByName('DATA_NBR').Value := DM_CLIENT.CL_AGENCY.Lookup('Agency_Name', Null, 'CL_AGENCY_NBR');
        end;
      end;
    end
    else if TableInfo.Name = 'CO_TIME_OFF_ACCRUAL' then
    begin
      DM_CLIENT.CL_E_DS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('CustomEDCodeNumber').IsNull then
        Assert(DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('CustomEDCodeNumber').Value, []), 'Cannot find client E/D ' + TableInfo.DSBase.FieldByName('CustomEDCodeNumber').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_DS_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('CustomEDCodeNumber').Value, 'CL_E_DS_NBR');
    end
    else if TableInfo.Name = 'CO_SHIFTS' then
    begin
      DM_CLIENT.CL_E_D_GROUPS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('EDGroupName').IsNull then
        Assert(DM_CLIENT.CL_E_D_GROUPS.Locate('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, []), 'Cannot find E/D group ' + TableInfo.DSBase.FieldByName('EDGroupName').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_D_GROUPS.Lookup('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, 'CL_E_D_GROUPS_NBR');
    end
    else if TableInfo.Name = 'CO_PENSIONS' then
    begin
      DM_CLIENT.CL_PENSION.DataRequired('');
      if not TableInfo.DSBase.FieldByName('PensionName').IsNull then
        Assert(DM_CLIENT.CL_PENSION.Locate('NAME', TableInfo.DSBase.FieldByName('PensionName').Value, []), 'Cannot find pension ' + TableInfo.DSBase.FieldByName('PensionName').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_PENSION_NBR').Value := DM_CLIENT.CL_PENSION.Lookup('NAME', TableInfo.DSBase.FieldByName('PensionName').Value, 'CL_PENSION_NBR');
    end
    else if TableInfo.Name = 'CO_TIME_OFF_ACCRUAL' then
    begin
      DM_CLIENT.CL_E_D_GROUPS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('EDGroupName').IsNull then
        Assert(DM_CLIENT.CL_E_D_GROUPS.Locate('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, []), 'Cannot find E/D group ' + TableInfo.DSBase.FieldByName('EDGroupName').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_D_GROUPS.Lookup('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, 'CL_E_D_GROUPS_NBR');
    end
    else if TableInfo.Name = 'CO_WORKERS_COMP' then
    begin
      DM_CLIENT.CL_E_D_GROUPS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('EDGroupName').IsNull then
        Assert(DM_CLIENT.CL_E_D_GROUPS.Locate('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, []), 'Cannot find E/D group ' + TableInfo.DSBase.FieldByName('EDGroupName').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_D_GROUPS.Lookup('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, 'CL_E_D_GROUPS_NBR');
    end
    else if (TableInfo.Name = 'CO_BENEFIT_RATES') then
    begin
      DM_CLIENT.CL_E_D_GROUPS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('EDGroupName').IsNull then
        Assert(DM_CLIENT.CL_E_D_GROUPS.Locate('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, []), 'Cannot find E/D group ' + TableInfo.DSBase.FieldByName('EDGroupName').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_D_GROUPS.Lookup('NAME', TableInfo.DSBase.FieldByName('EDGroupName').Value, 'CL_E_D_GROUPS_NBR');
    end;


    if Copy(TableInfo.Name, 1, 2) = 'EE' then
    for I := 0 to TableInfo.DSBase.FieldCount - 1 do
    begin
      FieldName := TableInfo.DSBase.Fields[I].FieldName;
      if (TableInfo.Name + '_NBR' <> FieldName) and (Copy(FieldName, Length(FieldName) - 3, 4) = '_NBR')
         and ((Pos('CL_', FieldName) <> 0) or (Pos('CO_', FieldName) <> 0)) and (FieldName <> 'CL_PERSON_NBR') and
         (FieldName <> 'CL_PERSON_DEPENDENTS_NBR')and (FieldName <> 'CL_AGENCY_NBR')
         and ( FieldName <> 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR')
         and ( FieldName <> 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR')
         and ( FieldName <> 'MIN_PPP_CL_E_D_GROUPS_NBR')
         and ( FieldName <> 'MAX_PPP_CL_E_D_GROUPS_NBR')
         and ( FieldName <> 'CO_LOCATIONS_NBR') then
        TableInfo.DSBase.Fields[I].Clear;
    end;

    if TableInfo.Name = 'EE' then
    begin
      DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      TableInfo.DSBase.FieldByName('CO_NBR').Value := CoNbr;
      DM_COMPANY.CO_DIVISION.DataRequired('');
      if not TableInfo.DSBase.FieldByName('DivisionNumber').IsNull then
        Assert(DM_COMPANY.CO_DIVISION.Locate('CUSTOM_DIVISION_NUMBER;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('DivisionNumber').Value, CoNbr]), []), 'Cannot find division ' + TableInfo.DSBase.FieldByName('DivisionNumber').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_DIVISION_NBR').Value := DM_COMPANY.CO_DIVISION.Lookup('CUSTOM_DIVISION_NUMBER;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('DivisionNumber').Value, CoNbr]), 'CO_DIVISION_NBR');
      DM_COMPANY.CO_BRANCH.DataRequired('');
      if not TableInfo.DSBase.FieldByName('BranchNumber').IsNull then
        Assert(DM_COMPANY.CO_BRANCH.Locate('CUSTOM_BRANCH_NUMBER;CO_DIVISION_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('BranchNumber').Value, TableInfo.DSBase.FieldByName('CO_DIVISION_NBR').Value]), []), 'Cannot find branch ' + TableInfo.DSBase.FieldByName('BranchNumber').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_BRANCH_NBR').Value := DM_COMPANY.CO_BRANCH.Lookup('CUSTOM_BRANCH_NUMBER;CO_DIVISION_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('BranchNumber').Value, TableInfo.DSBase.FieldByName('CO_DIVISION_NBR').Value]), 'CO_BRANCH_NBR');
      DM_COMPANY.CO_DEPARTMENT.DataRequired('');
      if not TableInfo.DSBase.FieldByName('DepartmentNumber').IsNull then
        Assert(DM_COMPANY.CO_DEPARTMENT.Locate('CUSTOM_DEPARTMENT_NUMBER;CO_BRANCH_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('DepartmentNumber').Value, TableInfo.DSBase.FieldByName('CO_BRANCH_NBR').Value]), []), 'Cannot find department ' + TableInfo.DSBase.FieldByName('DepartmentNumber').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_DEPARTMENT_NBR').Value := DM_COMPANY.CO_DEPARTMENT.Lookup('CUSTOM_DEPARTMENT_NUMBER;CO_BRANCH_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('DepartmentNumber').Value, TableInfo.DSBase.FieldByName('CO_BRANCH_NBR').Value]), 'CO_DEPARTMENT_NBR');
      DM_COMPANY.CO_TEAM.DataRequired('');
      if not TableInfo.DSBase.FieldByName('TeamNumber').IsNull then
        Assert(DM_COMPANY.CO_TEAM.Locate('CUSTOM_TEAM_NUMBER;CO_DEPARTMENT_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('TeamNumber').Value, TableInfo.DSBase.FieldByName('CO_DEPARTMENT_NBR').Value]), []), 'Cannot find team ' + TableInfo.DSBase.FieldByName('TeamNumber').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_TEAM_NBR').Value := DM_COMPANY.CO_TEAM.Lookup('CUSTOM_TEAM_NUMBER;CO_DEPARTMENT_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('TeamNumber').Value, TableInfo.DSBase.FieldByName('CO_DEPARTMENT_NBR').Value]), 'CO_TEAM_NBR');
      TableInfo.DSBase.FieldByName('PR_CHECK_MB_GROUP_NBR').Clear;
      TableInfo.DSBase.FieldByName('PR_EE_REPORT_MB_GROUP_NBR').Clear;
      TableInfo.DSBase.FieldByName('PR_EE_REPORT_SEC_MB_GROUP_NBR').Clear;
      TableInfo.DSBase.FieldByName('TAX_EE_RETURN_MB_GROUP_NBR').Clear;
      //to clean up ESS credential
      TableInfo.DSBase.FieldByName('SELFSERVE_PASSWORD').Clear;
      TableInfo.DSBase.FieldByName('SELFSERVE_USERNAME').Clear;
      TableInfo.DSBase.FieldByName('E_MAIL_ADDRESS').Clear;
      TableInfo.DSBase.FieldByName('BENEFIT_E_MAIL_ADDRESS').Clear;
    end
    else if TableInfo.Name = 'EE_SCHEDULED_E_DS' then
    begin
      DM_CLIENT.CL_E_DS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('CUSTOM_E_D_CODE_NUMBER').IsNull then
        Assert(DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('CUSTOM_E_D_CODE_NUMBER').Value, []), 'Cannot find client E/D ' + TableInfo.DSBase.FieldByName('CUSTOM_E_D_CODE_NUMBER').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_DS_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('CUSTOM_E_D_CODE_NUMBER').Value, 'CL_E_DS_NBR');
    end
    else if TableInfo.Name = 'EE_STATES' then
    begin
      DM_COMPANY.CO_STATES.DataRequired('');
      if not TableInfo.DSBase.FieldByName('State_Lookup').IsNull then
        Assert(DM_COMPANY.CO_STATES.Locate('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('State_Lookup').Value, CoNbr]), []), 'Cannot find state ' + TableInfo.DSBase.FieldByName('State_Lookup').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_STATES_NBR').Value := DM_COMPANY.CO_STATES.Lookup('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('State_Lookup').Value, CoNbr]), 'CO_STATES_NBR');
      if not TableInfo.DSBase.FieldByName('SDI_State_Lookup').IsNull then
        Assert(DM_COMPANY.CO_STATES.Locate('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('SDI_State_Lookup').Value, CoNbr]), []), 'Cannot find SDI state ' + TableInfo.DSBase.FieldByName('SDI_State_Lookup').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('SDI_APPLY_CO_STATES_NBR').Value := DM_COMPANY.CO_STATES.Lookup('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('SDI_State_Lookup').Value, CoNbr]), 'CO_STATES_NBR');
      if not TableInfo.DSBase.FieldByName('SUI_State_Lookup').IsNull then
        Assert(DM_COMPANY.CO_STATES.Locate('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('SUI_State_Lookup').Value, CoNbr]), []), 'Cannot find SUI state ' + TableInfo.DSBase.FieldByName('SUI_State_Lookup').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('SUI_APPLY_CO_STATES_NBR').Value := DM_COMPANY.CO_STATES.Lookup('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('SUI_State_Lookup').Value, CoNbr]), 'CO_STATES_NBR');
      if not TableInfo.DSBase.FieldByName('RECIPROCAL_State_Lookup').IsNull then
        Assert(DM_COMPANY.CO_STATES.Locate('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('RECIPROCAL_State_Lookup').Value, CoNbr]), []), 'Cannot find reciprocal state ' + TableInfo.DSBase.FieldByName('RECIPROCAL_State_Lookup').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('RECIPROCAL_CO_STATES_NBR').Value := DM_COMPANY.CO_STATES.Lookup('STATE;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('RECIPROCAL_State_Lookup').Value, CoNbr]), 'CO_STATES_NBR');
    end
    else if TableInfo.Name = 'EE_LOCALS' then
    begin
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('');
      if not TableInfo.DSBase.FieldByName('Local_Name_Lookup').IsNull then
        Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('LocalName;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('Local_Name_Lookup').Value, CoNbr]), []), 'Cannot find local ' + TableInfo.DSBase.FieldByName('Local_Name_Lookup').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_LOCAL_TAX_NBR').Value := DM_COMPANY.CO_LOCAL_TAX.Lookup('LocalName;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('Local_Name_Lookup').Value, CoNbr]), 'CO_LOCAL_TAX_NBR');

      DM_COMPANY.CO_LOCATIONS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('CO_LOCATIONS_ACCOUNTNUMBER').IsNull then
        Assert(DM_COMPANY.CO_LOCATIONS.Locate('CO_NBR;ACCOUNT_NUMBER', VarArrayof([CoNbr,TableInfo.DSBase.FieldByName('CO_LOCATIONS_ACCOUNTNUMBER').value]), []), 'Cannot find local ' + TableInfo.DSBase.FieldByName('CO_LOCATIONS_ACCOUNTNUMBER').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_LOCATIONS_NBR').Value := DM_COMPANY.CO_LOCATIONS.Lookup('ACCOUNT_NUMBER;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('CO_LOCATIONS_ACCOUNTNUMBER').Value, CoNbr]), 'CO_LOCATIONS_NBR');;
    end
    else if TableInfo.Name = 'EE_TIME_OFF_ACCRUAL' then
    begin
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.DataRequired('');
      if not TableInfo.DSBase.FieldByName('DESCRIPTION').IsNull then
        Assert(DM_COMPANY.CO_TIME_OFF_ACCRUAL.Locate('DESCRIPTION;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('DESCRIPTION').Value, CoNbr]), []), 'Cannot find time off accrual ' + TableInfo.DSBase.FieldByName('DESCRIPTION').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').Value := DM_COMPANY.CO_TIME_OFF_ACCRUAL.Lookup('DESCRIPTION;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('Description').Value, CoNbr]), 'CO_TIME_OFF_ACCRUAL_NBR');
    end
    else if TableInfo.Name = 'EE_WORK_SHIFTS' then
    begin
      DM_COMPANY.CO_SHIFTS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('NameLKUP').IsNull then
        Assert(DM_COMPANY.CO_SHIFTS.Locate('NAME;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('NameLKUP').Value, CoNbr]), []), 'Cannot find shift ' + TableInfo.DSBase.FieldByName('NameLKUP').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_SHIFTS_NBR').Value := DM_COMPANY.CO_SHIFTS.Lookup('NAME;CO_NBR', VarArrayOf([TableInfo.DSBase.FieldByName('NameLKUP').Value, CoNbr]), 'CO_SHIFTS_NBR');
    end
    else if TableInfo.Name = 'EE_ADDITIONAL_INFO' then
    begin
      DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.DataRequired('');
      if not TableInfo.DSBase.FieldByName('Name').IsNull then
        Assert(DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.Locate('NAME;CO_NBR', VarArrayOf([Trim(TableInfo.DSBase.FieldByName('Name').Value), CoNbr]), []), 'Cannot find additional info ' + TableInfo.DSBase.FieldByName('Name').Value + ' in destination company');
      TableInfo.DSBase.FieldByName('CO_ADDITIONAL_INFO_NAMES_NBR').Value := DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.Lookup('NAME;CO_NBR', VarArrayOf([Trim(TableInfo.DSBase.FieldByName('Name').Value), CoNbr]), 'CO_ADDITIONAL_INFO_NAMES_NBR');
    end
    else if TableInfo.Name = 'EE_PENSION_FUND_SPLITS' then
    begin
      DM_CLIENT.CL_FUNDS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('Funds_lookup').IsNull then
        Assert(DM_CLIENT.CL_FUNDS.Locate('NAME', TableInfo.DSBase.FieldByName('Funds_lookup').Value, []), 'Cannot find fund ' + TableInfo.DSBase.FieldByName('Funds_lookup').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_FUNDS_NBR').Value := DM_CLIENT.CL_FUNDS.Lookup('NAME', TableInfo.DSBase.FieldByName('Funds_lookup').Value, 'CL_FUNDS_NBR');
      DM_CLIENT.CL_E_DS.DataRequired('');
      if not TableInfo.DSBase.FieldByName('E_D_Code_lookup').IsNull then
        Assert(DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('E_D_Code_lookup').Value, []), 'Cannot find client E/D ' + TableInfo.DSBase.FieldByName('E_D_Code_lookup').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_E_DS_NBR').Value := DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', TableInfo.DSBase.FieldByName('E_D_Code_lookup').Value, 'CL_E_DS_NBR');
    end
    else if TableInfo.Name = 'EE_PIECE_WORK' then
    begin
      DM_CLIENT.CL_PIECES.DataRequired('');
      if not TableInfo.DSBase.FieldByName('Name').IsNull then
        Assert(DM_CLIENT.CL_PIECES.Locate('Name', TableInfo.DSBase.FieldByName('Name').Value, []), 'Cannot find piece ' + TableInfo.DSBase.FieldByName('Name').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CL_PIECES_NBR').Value := DM_CLIENT.CL_PIECES.Lookup('NAME', TableInfo.DSBase.FieldByName('Name').Value, 'CL_PIECES_NBR');
    end
    else if (TableInfo.Name = 'EE_BENEFITS') then
    begin
      DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      if not TableInfo.DSBase.FieldByName('Benefit').IsNull then
        Assert(DM_CLIENT.CO_BENEFITS.Locate('Benefit_Name', TableInfo.DSBase.FieldByName('Benefit').Value, []), 'Cannot find benefit ' + TableInfo.DSBase.FieldByName('Benefit').Value + ' in destination client');
      TableInfo.DSBase.FieldByName('CO_BENEFITS_NBR').Value := DM_COMPANY.CO_BENEFITS.Lookup('Benefit_Name', TableInfo.DSBase.FieldByName('Benefit').Value, 'CO_BENEFITS_NBR');

      TableInfo.DSBase.FieldByName('CO_BENEFIT_ENROLLMENT_NBR').Clear;

    end
    else if (TableInfo.Name = 'EE_BENEFIT_PAYMENT') or
            (TableInfo.Name = 'EE_CHILD_SUPPORT_CASES')
    then
    begin
      TableInfo.DSBase.FieldByName('CL_AGENCY_NBR').Clear;
    end;

    TableInfo.DSBase.Post;
    TableInfo.DSBase.Next;
  end;

  if cbUseImport.Checked and DirectoryExists(editImportFolder.Text) then
    LoadValuesFromFile(TableInfo);
  TableInfo.FirstPass := False;
end;

procedure TCopyWizard.LoadExcludeList(TableInfo: TvsTableInf);
var
  I, X: Integer;
  FieldName, FieldNamePrefix: String;
begin
  if not Assigned(TableInfo.ExcludeList) then
  begin
    X := 0;
    SetLength(TableInfo.ExcludeList, X);
    for I := 0 to ComponentCount - 1 do
    begin
      FieldNamePrefix := Copy(Components[I].Name, 1, Pos('xx', Components[I].Name) - 1);
      Delete(FieldNamePrefix, 1, 3);
      if (FieldNamePrefix = TableInfo.Name) and (Components[I].ClassName = 'TevLabel') and not TevLabel(Components[I]).WordWrap then
      begin
        FieldName := Copy(Components[I].Name, Pos('xx', Components[I].Name) + 2, Length(Components[I].Name));

        if (TableInfo.Name = 'CL_PENSION') and (FieldName = 'NAME') then continue;

        SetLength(TableInfo.ExcludeList, Length(TableInfo.ExcludeList) + 1);

        TableInfo.ExcludeList[X].FName := FieldName;

        if TevLabel(Components[I]).Caption[1] = '~' then
          TableInfo.ExcludeList[X].FMandatory := True
        else
          TableInfo.ExcludeList[X].FMandatory := False;
        Inc(X);
      end;
    end;
  end;
end;

procedure TCopyWizard.CacheCo_And_EE_States;
begin
  ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE_STATES,DM_EMPLOYEE.CO_STATES]);
  CachedEEStates :=  CreateLookupDataset(DM_EMPLOYEE.EE_STATES, Self);
  CachedCoStates := CreateLookupDataset(DM_EMPLOYEE.CO_STATES, Self);
  ctx_DataAccess.OpenDataSets([CachedEEStates,CachedCOStates]);
end;


function TCopyWizard.CreateNewDBStructure(CLList: TtList; COList: TtList; Level: TDBLevel): Integer;
var
  I,j, N, NewKey, NewCoLocationsNBR  : Integer;
  T: TvsTableInf;
  DS : TevClientDataSet;
  F1, F2 : TField;
  ActualTable: String;
  TL: TtList;
  KeyLst : TKeyList;
  CurrClient: Integer;
  ChangedDS: TArrayDS;
  DynamicFieldUpdate: TDynamicFieldUpdate;
function CalcHomeTaxEeStatesNbr:integer;
var
  Co_States_Nbr:integer;
  StateName:string;
begin

  Co_States_Nbr := ConvertNull(CachedEEStates.Lookup('EE_STATES_NBR',DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value,'CO_STATES_NBR'),0);
  StateName     := ConvertNull(CachedCoStates.Lookup('CO_STATES_NBR',Co_States_Nbr,'State'),'');

  Co_States_Nbr := DM_EMPLOYEE.CO_STATES.Lookup('CO_NBR;STATE',
                                                        VarArrayOf([DM_COMPANY.CO['CO_NBR'], StateName]),
                                                        'CO_STATES_NBR');
  Result := DM_EMPLOYEE.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR',
                                       VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, Co_States_Nbr]),
                                       'EE_STATES_NBR');
end;

label
  l1;
begin
  Result := 0;
  ctx_DataAccess.TempCommitDisable := True;
  try
    SetLength(ChangedDS,0);
    SetLength(DynamicFieldUpdate,0);
        
    if Level = evClient then
      TL := CLList
    else if (Level = evCompany) and (Assigned(CLList.TableList)) then
    begin
      ctx_DataAccess.OpenClient(CLList.CLNew);
      TL := CoList;
    end
    else
      TL := CoList;

    for I := Low(TL.TableList) to High(TL.TableList) do
    begin
      SetLength(TL.TableList[I].KeyList, 0);
      SetLength(TL.TableList[I].SecPassValues, 0);
    end;

    for I := Low(TL.TableList) to High(TL.TableList) do
    begin
      T := TL.TableList[I];
      DS := ctx_DataAccess.GetDataSet(GetddTableClassByName(T.Name));
      SetLength(ChangedDS,Length(ChangedDS)+1);
      ChangedDS[High(ChangedDS)] := DS;
      if (DS.Name = 'CL') and (NewCLId = 0) then
      begin
        ctx_StartWait;
        try
          NewCLId := ctx_DBAccess.CreateClientDB(nil);
          DS.Close;
        finally
          ctx_EndWait;
        end;
      end;

      if NewCLId <> 0 then
        DS.ClientID :=  NewCLId
      else
        DS.ClientID := CLList.CLNew;

      if DS.Active then
        DS.Close;
      ctx_DataAccess.OpenDataSets([DS]);

      SetLength(T.KeyList, T.DSBase.RecordCount);

      T.DSBase.First;
      while not T.DSBase.Eof do
      try
        if (Level = evCompany) and (T.Name = 'CL_MAIL_BOX_GROUP')
           and DS.Locate('DESCRIPTION', T.DSBase.FieldByName('DESCRIPTION').Value, []) then
        begin
          T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
          T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName('CL_MAIL_BOX_GROUP_NBR').AsInteger;
          T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName('CL_MAIL_BOX_GROUP_NBR').AsInteger;
          T.DSBase.Next;
          Continue;
        end;
        if ((Level = evCompany) or (Level = evEmployee)) and (T.Name = 'CL_AGENCY')
           and DS.Locate('AGENCY_NAME', T.DSBase.FieldByName('AGENCY_NAME').Value, []) then
        begin
          T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
          T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName('CL_AGENCY_NBR').AsInteger;
          T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName('CL_AGENCY_NBR').AsInteger;
          T.DSBase.Next;
          Continue;
        end;

        if (T.Name = 'CL_PERSON') and DS.Locate('SOCIAL_SECURITY_NUMBER', T.DSBase.FieldByName('SOCIAL_SECURITY_NUMBER').Value, []) then
        begin
          T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
          T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName('CL_PERSON_NBR').AsInteger;
          T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName('CL_PERSON_NBR').AsInteger;
          T.DSBase.Next;
          Continue;
        end;

        if (T.Name = 'CL_PERSON_DEPENDENTS') and DS.Locate('LAST_NAME;FIRST_NAME;MIDDLE_INITIAL;SOCIAL_SECURITY_NUMBER', T.DSBase['LAST_NAME;FIRST_NAME;MIDDLE_INITIAL;SOCIAL_SECURITY_NUMBER'], []) then
        begin
          T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
          T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName('CL_PERSON_DEPENDENTS_NBR').AsInteger;
          T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName('CL_PERSON_DEPENDENTS_NBR').AsInteger;
          T.DSBase.Next;
          Continue;
        end;
        if (Level = evEmployee) and (T.Name = 'CL_E_D_GROUPS')
           and DS.Locate('NAME', T.DSBase.FieldByName('NAME').Value, []) then
        begin
          T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
          T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
          T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
          T.DSBase.Next;
          Continue;
        end;

        if DS.Name = 'CL' then
        begin
          // CL table already has initial record and it contains the values of the parent client if the client is based on the other
          DS.Edit;
          DS.FieldByName('CL_NBR').AsInteger := NewCLId;
          DS.FieldByName('ADDRESS1').AsString := 'Default Address1';
          DS.FieldByName('CITY').AsString := 'Default City';
          DS.FieldByName('STATE').AsString := '  ';
          DS.FieldByName('ZIP_CODE').AsString := '00000';

          DS.FieldByName('ADDRESS2').Value := null;
          DS.FieldByName('CONTACT1').Value := null;
          DS.FieldByName('PHONE1').Value := null;
          DS.FieldByName('DESCRIPTION1').Value := null;
          DS.FieldByName('CONTACT2').Value := null;
          DS.FieldByName('PHONE2').Value := null;
          DS.FieldByName('DESCRIPTION2').Value := null;
          DS.FieldByName('FAX1').Value := null;
          DS.FieldByName('FAX1_DESCRIPTION').Value := null;
          DS.FieldByName('FAX2').Value := null;
          DS.FieldByName('FAX2_DESCRIPTION').Value := null;
          DS.FieldByName('E_MAIL_ADDRESS1').Value := null;
          DS.FieldByName('E_MAIL_ADDRESS2').Value := null;
          DS.FieldByName('AUTO_SAVE_MINUTES').Value := 0;
          DS.FieldByName('SB_ACCOUNTANT_NUMBER').Value := null;
          DS.FieldByName('ACCOUNTANT_CONTACT').Value := null;
          DS.FieldByName('PRINT_CPA').Value := GROUP_BOX_NO;
          DS.FieldByName('LEASING_COMPANY').Value := GROUP_BOX_NO;
          DS.FieldByName('RECIPROCATE_SUI').Value := GROUP_BOX_YES;
          DS.FieldByName('SECURITY_FONT').Value := GROUP_BOX_YES;
          DS.FieldByName('CUSTOMER_SERVICE_SB_USER_NBR').Value := null;
          DS.FieldByName('CSR_SB_TEAM_NBR').Value := null;
          DS.FieldByName('START_DATE').Value := Today;
          DS.FieldByName('TERMINATION_DATE').Value := null;
          DS.FieldByName('TERMINATION_CODE').Value := TERMINATION_ACTIVE;
          DS.FieldByName('FILLER').Value := null;
          DS.FieldByName('ENABLE_HR').Value := CLIENT_FEATURE_NO;
          DS.FieldByName('RECIPROCATE_STATE').Value := GROUP_BOX_NO;
          DS.FieldByName('PHONE1_EXT').Value := null;
          DS.FieldByName('PHONE2_EXT').Value := null;
          DS.FieldByName('PRINT_CLIENT_NAME').Value := GROUP_BOX_NO;
          DS.FieldByName('MAINTENANCE_HOLD').Value := GROUP_BOX_NO;
          DS.FieldByName('READ_ONLY').Value := GROUP_BOX_NO;
          DS.FieldByName('BLOCK_INVALID_SSN').Value := GROUP_BOX_NO;

          DS.Post;
          DS.MergeChangeLog;
          DS.Edit;
        end
        else
          DS.Insert;

        if ((Level = evClient) and (DS.Name = 'CL_MAIL_BOX_GROUP') ) then
          DS.FieldByName('CL_MAIL_BOX_GROUP_NBR').AsInteger := ctx_DBAccess.GetGeneratorValue('G_CL_MAIL_BOX_GROUP', dbtClient, 1);
        T.KeyList[T.DSBase.RecNo - 1].RecNo := T.DSBase.RecNo;
        T.KeyList[T.DSBase.RecNo - 1].OldKey := T.DSBase.FieldByName(T.Name + '_NBR').AsInteger;

        for N := 0 to T.DSBase.Fields.Count - 1 do
        begin
          ActualTable := '';
          F1 := T.DSBase.Fields[N];

          if (F1.FieldName <> T.Name + '_NBR') and (F1.FieldName <> 'EFFECTIVE_DATE') and (F1.FieldName <> 'EFFECTIVE_UNTIL')
              and (F1.FieldName <> 'REC_VERSION') then
          begin
            F2 := DS.Fields.FindField(F1.FieldName);
            if Assigned(F2) and (not F1.IsNull) then
            begin
              if (T.Name = 'CO_GENERAL_LEDGER') and (F1.FieldName = 'LEVEL_NBR') then
              begin
                if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_DIVISION then
                  ActualTable := 'CO_DIVISION'
                else if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_BRANCH then
                  ActualTable := 'CO_BRANCH'
                else if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_DEPT then
                  ActualTable := 'CO_DEPARTMENT'
                else if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_TEAM then
                  ActualTable := 'CO_TEAM'
                else if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_EMPLOYEE then
                begin
                  ActualTable := 'EE';
                  if T.DSBase.FieldByName('LEVEL_NBR').AsInteger <> 0 then
                  begin
                    DS.Cancel;
                    goto l1;
                  end;
                end
                else if T.DSBase.FieldByName('LEVEL_TYPE').AsString = GL_LEVEL_JOB then
                  ActualTable := 'CO_JOBS';
                if ActualTable <> '' then
                begin
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, ActualTable), T.DSBase.RecNo, F1.Value);
                  if NewKey <> 0 then
                    T.AssignField(F2,NewKey);
                end
                else
                  T.AssignField(F2,F1.Value);
              end
              else if (T.Name = 'CO') and (
                    (F1.FieldName = 'AGENCY_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_REPORT_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_RETURN_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_RETURN_SECOND_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_REPORT_SECOND_MB_GROUP_NBR')
                ) then
              begin
                NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_MAIL_BOX_GROUP'),T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'CO') and (F1.FieldName = 'CO_LOCATIONS_NBR') then
                T.AssignField(F2, F1.Value) // to get Source CO_LOCATIONS_NBR
              else if ( (T.Name = 'CO_DIVISION') or
                        (T.Name = 'CO_BRANCH') or
                        (T.Name = 'CO_DEPARTMENT') or
                        (T.Name = 'CO_TEAM')
                      )
                      and ( (F1.FieldName = 'PR_CHECK_MB_GROUP_NBR') or
                            (F1.FieldName = 'PR_EE_REPORT_MB_GROUP_NBR') or
                            (F1.FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR') or
                            (F1.FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR') or
                            (F1.FieldName = 'PR_DBDT_REPORT_MB_GROUP_NBR') or
                            (F1.FieldName = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR')
                          )
              then begin
                NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_MAIL_BOX_GROUP'),T.DSBase.RecNo, F1.Value);

                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'CO_REPORTS') and (F1.FieldName = 'OVERRIDE_MB_GROUP_NBR') then
              begin
                NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_MAIL_BOX_GROUP'),T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'CO_TIME_OFF_ACCRUAL') and (F1.FieldName = 'CO_HR_ATTENDANCE_TYPES_NBR') then
                T.AssignField(F2, null)
              else if (T.Name = 'CO_BENEFITS')
                   and ((F1.FieldName = 'CL_AGENCY_NBR') or
                        (F1.FieldName = 'EE_DEDUCTION_NBR') or
                        (F1.FieldName = 'ER_DEDUCTION_NBR') or
                        (F1.FieldName = 'EE_COBRA_NBR') or
                        (F1.FieldName = 'ER_COBRA_NBR')) then
              begin
                if (F1.FieldName = 'CL_AGENCY_NBR') then
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_AGENCY'),T.DSBase.RecNo, F1.Value)
                else
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CO_E_D_CODES'),T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'CO_GENERAL_LEDGER') and (F1.FieldName = 'DATA_NBR') then
              begin
                if (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_STATE) or
                   (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_EE_SDI) or
                   (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ER_SDI) or
                   (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_STATE_OFFSET) or
                   (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_EE_SDI_OFFSET) or
                   (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ER_SDI_OFFSET) then
                  ActualTable := 'CO_STATES'
                else if (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_EE_SUI) or
                        (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ER_SUI) or
                        (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_EE_SUI_OFFSET) or
                        (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_ER_SUI_OFFSET) then
                  ActualTable := 'CO_SUI'
                else if (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_WC_IMPOUND) or
                        (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_WC_IMPOUND_OFFSET) then
                  ActualTable := 'CO_WORKERS_COMP'
                else if (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_LOCAL) or
                        (T.DSBase.FieldByName('DATA_TYPE').AsString = GL_DATA_LOCAL_OFFSET) then
                  ActualTable := 'CO_LOCAL_TAX';
                if ActualTable <> '' then
                begin
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, ActualTable), T.DSBase.RecNo, F1.Value);
                  if NewKey <> 0 then
                    T.AssignField(F2,NewKey);
                end
                else
                  T.AssignField(F2,F1.Value);
              end
              else if (T.Name = 'EE') and (F1.FieldName = 'HOME_TAX_EE_STATES_NBR') then
                  T.AssignField(F2, - F1.Value)
              else if (T.Name = 'EE_SCHEDULED_E_DS') and
                   ( (F1.FieldName = 'SCHEDULED_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'THRESHOLD_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'MAX_PPP_CL_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'MIN_PPP_CL_E_D_GROUPS_NBR') or
                     (F1.FieldName = 'CL_AGENCY_NBR')  ) then
              begin
                  if (F1.FieldName = 'CL_AGENCY_NBR') then
                    NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_AGENCY'),T.DSBase.RecNo, F1.Value)
                  else
                    NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_E_D_GROUPS'),T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'EE_BENEFICIARY') and
                      (F1.FieldName = 'CL_PERSON_DEPENDENTS_NBR') then
              begin
                NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, 'CL_PERSON_DEPENDENTS'),T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if IsValidChildTable(F1.FieldName, TL.TableList, Level, ActualTable) then
              begin
                if ActualTable = '' then
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, F1.FieldName), T.DSBase.RecNo, F1.Value)
                else
                  NewKey := GetNewKeyValue(GetKeyListByName(TL.TableList, ActualTable), T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if IsValidChildTable(F1.FieldName, CLList.TableList, Level, ActualTable) then
              begin
                if ActualTable = '' then
                  NewKey := GetNewKeyValue(GetKeyListByName(CLList.TableList, F1.FieldName), T.DSBase.RecNo, F1.Value)
                else
                  NewKey := GetNewKeyValue(GetKeyListByName(CLList.TableList, ActualTable), T.DSBase.RecNo, F1.Value);
                if NewKey <> 0 then
                  T.AssignField(F2,NewKey);
              end
              else if (T.Name = 'CL') and
                 (
                    (F1.FieldName = 'AGENCY_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_CHECK_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_REPORT_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_RETURN_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR') or
                    (F1.FieldName = 'TAX_RETURN_SECOND_MB_GROUP_NBR') or
                    (F1.FieldName = 'PR_REPORT_SECOND_MB_GROUP_NBR')
                 ) then
              begin
                   T.AssignField(F2,F1.Value);
              end
              else if (T.Name = 'CL') and
                      ((F1.FieldName = 'RECIPROCATE_SUI') or (F1.FieldName = 'TERMINATION_CODE') ) then
              begin
                   T.AssignField(F2,F1.Value);

                   SetLength(DynamicFieldUpdate,Length(DynamicFieldUpdate)+1);
                   DynamicFieldUpdate[High(DynamicFieldUpdate)].Table := 'CL';
                   DynamicFieldUpdate[High(DynamicFieldUpdate)].Field := F1.FieldName;
                   DynamicFieldUpdate[High(DynamicFieldUpdate)].Nbr := DS.FieldByName(T.Name + '_NBR').AsInteger;
                   DynamicFieldUpdate[High(DynamicFieldUpdate)].value := F1.Value;
              end
              else if (T.Name = 'CL_MAIL_BOX_GROUP') and
                 (
                   (F1.FieldName = 'PRIM_SB_DELIVERY_METHOD_NBR') or
                   (F1.FieldName = 'PRIM_SB_MEDIA_TYPE_NBR') or
                   (F1.FieldName = 'SEC_SB_DELIVERY_METHOD_NBR') or
                   (F1.FieldName = 'SEC_SB_MEDIA_TYPE_NBR') or
                   (F1.FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR')
                 ) then
              begin
                if F1.FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR' then
                  T.AssignField(F2,null)
                else
                  T.AssignField(F2,F1.Value);
              end
              else if (Pos('_NBR', F1.FieldName) > 0) and
                      (Level = evClient) and
                      (LeftStr(F1.FieldName, 3) <> 'SY_') and
                      (LeftStr(F1.FieldName, 3) <> 'SB_') and
                      (F1.FieldName <> 'CUSTOMER_SERVICE_SB_USER_NBR') and
                      (F1.FieldName <> 'CSR_SB_TEAM_NBR') then
                F2.Clear
              else
                T.AssignField(F2,F1.Value);
            end;
          end;
        end;
        DS.Post;
        T.KeyList[T.DSBase.RecNo - 1].NewKey := DS.FieldByName(T.Name + '_NBR').AsInteger;
l1:
        T.DSBase.Next;
      except
        DS.Cancel;
        raise;
      end;
    end;

    if Level = evEmployee then
    begin
      DM_EMPLOYEE.EE.First;
      while not DM_EMPLOYEE.EE.EOF do
      begin
        if DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').asInteger < 0 then
        begin
          DM_EMPLOYEE.EE.Edit;
          DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value := -DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value;
          DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value :=CalcHomeTaxEeStatesNbr;
          DM_EMPLOYEE.EE.Post;
        end;
        DM_EMPLOYEE.EE.Next;
      end;
    end;

    CurrClient := ctx_DataAccess.ClientID;
    try
      if NewCLId <> 0 then
        ctx_DBAccess.CurrentClientNbr := NewCLId;  // deceive OpenClient function

      if Level = evClient then
      begin
         //CL table should always be first, it's ok
         T := TL.TableList[0];//CL
         DS := ctx_DataAccess.GetDataSet(GetddTableClassByName(T.Name));

         // to update AGENCY_CHECK_MB_GROUP_NBR....
         for j:=Low(T.SecPassValues) to High(T.SecPassValues) do
         begin
            if DS.FieldByName(T.Name + '_NBR').Value  <> T.KeyList[T.SecPassValues[j].KeyIndex].NewKey then
               DS.Locate(T.Name + '_NBR',T.KeyList[T.SecPassValues[j].KeyIndex].NewKey,[]);
            DS.Edit;
            KeyLst := GetKeyListByName( TL.TableList, T.SecPassValues[j].SecPassField.SlTableName );
            DS.FieldByName(T.SecPassValues[j].SecPassField.FieldName).Value :=
            GetNewKeyValue(KeyLst,
                           T.KeyList[T.SecPassValues[j].KeyIndex].RecNo,
                           T.SecPassValues[j].OldValue);
            DS.Post;
         end;

         // to swipe CL and CL_MAIL_BOX_GROUP for ctx_DataAccess.PostDataSets,  not to have trigger error "TI_CL"
         for j := low( ChangedDS ) to high( ChangedDS) do
         begin
           if ChangedDS[j].name = 'CL_MAIL_BOX_GROUP' then
           begin
             DS := ChangedDS[0]; //CL
             ChangedDS[0] := ChangedDS[j];
             ChangedDS[j] := DS;
             break;
           end;
         end;
      end;

      NewCoLocationsNBR := 0;
      if Level = evCompany then
      begin
        for I := Low(TL.TableList) to High(TL.TableList) do
        begin
          if TL.TableList[I].Name = 'CO_LOCATIONS' then  //Old CO_LOCATIONS
          begin
            T := TL.TableList[I];
            for j := low( ChangedDS ) to high( ChangedDS) do
            begin
              if ChangedDS[j].name = 'CO_LOCATIONS' then // New CO_LOCATIONS
              begin
                if T.DSBase.Locate('CO_LOCATIONS_NBR', DM_COMPANY.CO.FieldByName('CO_LOCATIONS_NBR').asInteger, []) then
                begin
                  if (ChangedDS[j].Locate('CO_NBR;ACCOUNT_NUMBER;ADDRESS1;ADDRESS2;CITY;STATE;ZIP_CODE',
                               VarArrayof([DM_COMPANY.CO['CO_NBR'],T.DSBase['ACCOUNT_NUMBER'], T.DSBase['ADDRESS1'], T.DSBase['ADDRESS2'],
                                           T.DSBase['CITY'],T.DSBase['STATE'],T.DSBase['ZIP_CODE']]),[])) then
                    NewCoLocationsNBR := ChangedDS[j].FieldByName('CO_LOCATIONS_NBR').asInteger;

                  //CO
                  DM_COMPANY.CO.Edit;
                  DM_COMPANY.CO.FieldByName('CO_LOCATIONS_NBR').clear;
                  DM_COMPANY.CO.post;
                end;
                break;
              end;
            end;
            break;
          end;
        end;
      end;

      try
        ctx_DataAccess.PostDataSets(ChangedDS, True);

        ctx_DBAccess.StartTransaction([dbtClient]);
        try
         for j:=Low(DynamicFieldUpdate) to High(DynamicFieldUpdate) do
         begin
           ctx_DBAccess.UpdateFieldValue(DynamicFieldUpdate[j].Table, DynamicFieldUpdate[j].Field, DynamicFieldUpdate[j].Nbr,
                                         Trunc(StrToDateTime('1/1/1900')), Trunc(StrToDateTime('12/30/9999')), DynamicFieldUpdate[j].value);
         end;
         ctx_DBAccess.CommitTransaction;
        except
         ctx_DBAccess.RollbackTransaction;
         raise;
        end;
      except
        ctx_DataAccess.CancelDataSets(ChangedDS);
        raise;
      end;
      case Level of
         evClient:  Result := NewCLId;
         evCompany:
         begin
           if NewCoLocationsNBR > 0 then
           begin
             DM_COMPANY.CO.Edit;
             DM_COMPANY.CO.FieldByName('CO_LOCATIONS_NBR').asInteger := NewCoLocationsNBR;
             DM_COMPANY.CO.post;
             ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
           end;

           updateSbENABLE_ANALYTICS(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString);
           Result := DM_COMPANY.CO['CO_NBR'];
         end;
      end;

    finally
      if NewCLId <> 0 then
        ctx_DBAccess.CurrentClientNbr := CurrClient;
    end;
  finally
    ctx_DataAccess.TempCommitDisable := False;
  end;
end;


function TCopyWizard.GetNewKeyValue(K: TKeyList; RecNbr,
  OKey: Integer): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := Low(K) to High(K) do
  begin
    if (K[I].OldKey = OKey) then
    begin
      Result := K[I].NewKey;
      Break;
    end;
  end;
end;

function TCopyWizard.IsValidChildTable(FName: String; TL: TTableList; Level: TDBLevel; var ActualTable: String): boolean;
var
  I: Integer;
  S, Lvl: String;
begin
  result := False;
  if Assigned(TL) then
  begin
    Lvl := TL[0].Name+'_';
    if ( ((Lvl = 'CL_MAIL_BOX_GROUP_') or (Lvl = 'CL_AGENCY_')) and (Copy(FName, 1, 3) = 'CO_'))
       or (Level = evCompany) then
      Lvl := 'CO_';
    if (Lvl = 'CL_PERSON_') and (Copy(FName, 1, 3) = 'EE_') then
      Lvl := 'EE_';
    if (Pos(Lvl, FName) > 0) and (Pos('_NBR', FName) > 0) then
    begin
      S := Copy(FName, Pos(Lvl, FName), Length(FName));
      if Pos('_NBR', S) > 0 then
      begin
        S := Copy(S, 1, Pos('_NBR', S)-1);
        for I := Low(TL) to High(TL) do
        begin
          if S = TL[I].Name then
          begin
            Result := True;
            ActualTable := S;
            Break;
          end;
        end;
      end;
    end;
  end;
end;

procedure TCopyWizard.cbUseImportClick(Sender: TObject);
begin
  if cbUseImport.Checked then
  begin
    lablImportFolder.Visible := True;
    editImportFolder.Visible := True;
    spbImportSourceFile.Visible := True;
    editImportFolder.Text := mb_AppSettings['COImportFilesPath'];
  end
  else
  begin
    lablImportFolder.Visible := False;
    editImportFolder.Visible := False;
    spbImportSourceFile.Visible := False;
  end;
end;

procedure TCopyWizard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DM_TEMPORARY.TMP_CL.Filtered := False;
  DM_TEMPORARY.TMP_CL.Filter := '';
end;

procedure TCopyWizard.btBackClick(Sender: TObject);
begin
  if pctCopyWizard.ActivePageIndex <= 1 then
    btBack.Enabled := False;
  btNext.Enabled := True;  
end;

procedure TCopyWizard.btNextClick(Sender: TObject);
begin
  if pctCopyWizard.ActivePageIndex > 0 then
    btBack.Enabled := True;
  evPanel2.Visible := False;
end;

procedure TCopyWizard.SetFocusOnActivePage;
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  if (Components[I] is TWinControl) and (TWinControl(Components[I]).Parent = pctCopyWizard.ActivePage)
  and (TWinControl(Components[I]).TabOrder = 0) then
  begin
    TWinControl(Components[I]).SetFocus;
    Break;
  end;
end;

procedure TCopyWizard.spbImportSourceFileClick(Sender: TObject);
begin
  editImportFolder.Text := RunFolderDialog('Please, select Import Files Folder.', editImportFolder.Text);
  mb_AppSettings['COImportFilesPath'] := editImportFolder.Text;
  BringToFront;
end;

procedure TCopyWizard.LoadValuesFromFile(TableInfo: TvsTableInf);
var
  aTable: TevClientDataSet;
  F: TextFile;
  S, FieldNamePrefix, FieldName, Temp: String;
  V: Variant;
  I, LastTag: Integer;
  FileName: TFileName;
  CompFound: Boolean;

  function QuickLocate(DataSet: TevClientDataSet; KeyFields: string; KeyFieldValues: Variant): Boolean;
  var
    aFieldName, FieldsLeft: string;
    Position, Count: Integer;
    aValue: Variant;
  begin
    Result := False;
    DataSet.First;
    while not DataSet.EOF do
    begin
      Result := True;
      FieldsLeft := KeyFields;
      Count := 0;
      repeat
        Position := Pos(';', FieldsLeft);
        if Position = 0 then
        begin
          aFieldName := FieldsLeft;
          FieldsLeft := '';
        end
        else
        begin
          aFieldName := Copy(FieldsLeft, 1, Position - 1);
          Delete(FieldsLeft, 1, Position);
        end;

        if VarIsArray(KeyFieldValues) then
          aValue := KeyFieldValues[Count]
        else
          aValue := KeyFieldValues;

        if DataSet.FieldByName(aFieldName).DataType in [ftString, ftFixedChar, ftWideString] then
        begin
          if UpperCase(DataSet.FieldByName(aFieldName).AsString) <> UpperCase(VarToStr(aValue)) then
          begin
            Result := False;
            Break;
          end;
        end
        else
        begin
          if DataSet.FieldByName(aFieldName).Value <> aValue then
          begin
            Result := False;
            Break;
          end;
        end;
        Inc(Count);
      until FieldsLeft = '';
      if Result then
        Break;
      DataSet.Next;
    end;
  end;

begin
  if editImportFolder.Text[Length(editImportFolder.Text)] <> '\' then
    FileName := editImportFolder.Text + '\' + TableInfo.Name + '.imp'
  else
    FileName := editImportFolder.Text + TableInfo.Name + '.imp';
  if not FileExists(FileName) then
    Exit;

  aTable := TevClientDataSet.Create(Self);
  try
    with aTable.FieldDefs.AddFieldDef do
    begin
      DataType := ftString;
      Name := 'CL_NUMBER';
      Size := 20;
      Index := 0;
    end;

    if Copy(TableInfo.Name, 1, 2) = 'CO' then
    with aTable.FieldDefs.AddFieldDef do
    begin
      DataType := ftString;
      Name := 'CO_NUMBER';
      Size := 20;
      Index := 1;
    end;

    LastTag := 0;
    repeat
      Inc(LastTag);
      CompFound := False;
      for I := 0 to ComponentCount - 1 do
      begin
        FieldNamePrefix := Copy(Components[I].Name, 1, Pos('xx', Components[I].Name) - 1);
        Delete(FieldNamePrefix, 1, 3);
        if (FieldNamePrefix = TableInfo.Name) and (Components[I].ClassName = 'TevLabel') and (Components[I].Tag = LastTag) then
        begin
          FieldName := Copy(Components[I].Name, Pos('xx', Components[I].Name) + 2, Length(Components[I].Name));
          with aTable.FieldDefs.AddFieldDef do
          begin
            DataType := ftString;
            Name := FieldName;
            Size := TableInfo.DSBase.FieldByName(FieldName).DisplayWidth;
            Required := TevLabel(Components[I]).WordWrap;
          end;
          CompFound := True;
          Break;
        end;
      end;
    until not CompFound;
    aTable.CreateDataSet;

    if TableInfo.Name = 'CO_STATES' then
    begin
      DM_SYSTEM_STATE.SY_STATES.Activate;
      DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
    end;
    AssignFile(F, FileName);
    Reset(F);
    try
      while not EOF(F) do
      begin
        Readln(F, S);
        S := S + ',';
        aTable.Append;
        I := 0;
        while Pos(',', S) <> 0 do
        begin
          if S[1] = '"' then
          begin
            Delete(S, 1, 1);
            Temp := Copy(S, 1, Pos('"', S) - 1);
            Delete(S, 1, Pos('"', S) + 1);
          end
          else
          begin
            Temp := Copy(S, 1, Pos(',', S) - 1);
            Delete(S, 1, Pos(',', S));
          end;
          aTable.Fields[I].AsString := Temp;
          Inc(I);
        end;
        aTable.Post;
      end;
    finally
      CloseFile(F);
    end;

    S := 'CL_NUMBER;';
    if Copy(TableInfo.Name, 1, 2) = 'CO' then
      S := S + 'CO_NUMBER;';
    for I := 0 to aTable.FieldCount - 1 do
    if aTable.Fields[I].Required and (aTable.Fields[I].FieldName <> 'CL_NUMBER') and (aTable.Fields[I].FieldName <> 'CO_NUMBER') then
      S := S + aTable.Fields[I].FieldName + ';';
    Delete(S, Length(S), 1);

    // At this point aTable contains the dataset to be imported, DSBase contains
    // the original values from the record that is being copied. For some
    // unknown reason (I checked, noone knows anymore) the required values
    // (required values are gotten from the screen, wordwrap property of the
    // corresponding component) in aTable are checked against the values in
    // DSBase. If they are not the same then the values in DSBase are used to
    // populate the screen instead of the values from aTable.

    TableInfo.DSBase.First;
    while not TableInfo.DSBase.Eof do
    begin
      V := VarArrayCreate([0, 0], varVariant);
      V[0] := CLNumber;
      if Copy(TableInfo.Name, 1, 2) = 'CO' then
      begin
        VarArrayRedim(V, VarArrayHighBound(V, 1) + 1);
        V[1] := CONumber;
      end;
      for I := 0 to aTable.FieldCount - 1 do
      if aTable.Fields[I].Required and
        (aTable.Fields[I].FieldName <> 'CL_NUMBER') and
        (aTable.Fields[I].FieldName <> 'CO_NUMBER') then
      begin
        if (TableInfo.Name = 'CL_BANK_ACCOUNT') and
          (aTable.Fields[I].FieldName = 'Bank_Name') then
        begin
          VarArrayRedim(V, VarArrayHighBound(V, 1) + 1);
          V[VarArrayHighBound(V, 1)] := aTable.Lookup('CL_NUMBER', CLNumber, 'Bank_Name');
        end
        else
        begin
          VarArrayRedim(V, VarArrayHighBound(V, 1) + 1);
          V[VarArrayHighBound(V, 1)] := TableInfo.DSBase.FieldByName(aTable.Fields[I].FieldName).Value;
        end;
      end;
      if QuickLocate(aTable, S, V) then
      begin
        TableInfo.DSBase.Edit;
        for I := 0 to aTable.FieldCount - 1 do
        if aTable.Fields[I].Required then
        begin
          if aTable.Fields[I].FieldName = 'Bank_Name' then
          begin
            S := ProcessValue(TableInfo.Name, aTable.Fields[I]);
            V := DM_SERVICE_BUREAU.SB_BANKS.Lookup('NAME', VarArrayOf([S]), 'ABA_NUMBER;SB_BANKS_NBR');
            if not (VarType(V) in [varNull]) then
            begin
              TableInfo.DSBase.FieldByName('Print_Name_Lookup').Value := S;
              TableInfo.DSBase.FieldByName('Bank_Name').Value:= S;
              TableInfo.DSBase.FieldByName('ABA_NBR_Lookup').Value := V[0];
              TableInfo.DSBase.FieldByName('SB_BANKS_NBR').AsInteger := V[1];
            end;
          end;
        end
        else
        begin
          if (aTable.Fields[I].FieldName <> 'CL_NUMBER') and
            (aTable.Fields[I].FieldName <> 'CO_NUMBER') then
          begin
            S := aTable.Fields[I].FieldName;
            TableInfo.DSBase.FieldByName(S).Value := ProcessValue(TableInfo.Name, aTable.Fields[I]);


            if aTable.Fields[I].FieldName = 'Agency_Name' then
             begin
               S := ProcessValue(TableInfo.Name, aTable.Fields[I]);
               V := DM_SERVICE_BUREAU.SB_AGENCY.Lookup('NAME', S, 'SB_AGENCY_NBR');
               if not (VarType(V) in [varNull]) then
                begin
                 TableInfo.DSBase.FieldByName('SB_AGENCY_NBR').AsInteger := V;
               end;
             end;


          end;
        end;
        TableInfo.DSBase.Post;
      end;
      TableInfo.DSBase.Next;
    end;
  finally
    aTable.Free;
  end;
end;

function TCopyWizard.ProcessValue(TableName: String; aField: TField): Variant;
begin
  Result := aField.Value;
end;

function NbrInList(Nbr: Integer): Boolean;
var
  i: Integer;
begin
  Result := Length(Nbrs) = 0;
  if not Result then
    for i := 0 to High(Nbrs) do
      if Nbrs[i] = Nbr then
      begin
        Result := True;
        Break;
      end;
end;

procedure TCopyWizard.btCancelClick(Sender: TObject);
begin
  ctx_DataAccess.OpenClient(0);
end;


end.
