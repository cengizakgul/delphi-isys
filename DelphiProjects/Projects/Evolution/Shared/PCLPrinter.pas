// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit PCLPrinter;

interface

uses
  Windows, SysUtils, WinSpool, Dialogs, Classes, Printers,
  ISUtils, EvMainboard, evExceptions;

const
  BufSize = 16384;

type
  TPrnBuffRec = record
    BuffLength: word;
    Buffer: array[0..BufSize] of char;
  end;

  pPrnBuffRec = ^TPrnBuffRec;

  TPrnBuffRec2 = record
    BuffLength: word;
    Buffer: PChar;
  end;

  pPrnBuffRec2 = ^TPrnBuffRec2;

function GetNumberText(Number: Real): string;
procedure StartPrinter(var hPrinter: THandle; const Index: Integer);
procedure StopPrinter(hPrinter: THandle);
procedure PrintFile(hPrinter: THandle; const sFileName: string);
procedure NewPrintFile(hPrinter: THandle; const sFileName: string);
procedure PrintStream(hPrinter: THandle; aStream: TMemoryStream);
procedure PrintString(hPrinter: THandle; s: string);
procedure NewPrintString(hPrinter: THandle; StrToPrint: string);
procedure PrintStringXY(hPrinter: THandle; s: string; X, Y: real);
procedure NewPrintStringXY(hPrinter: THandle; StrToPrint: string; X, Y: real);
procedure NewPrintStringXYDots(hPrinter: THandle; StrToPrint: string; X, Y: integer);
procedure PrintFileXY(hPrinter: THandle; s: string; X, Y: real);
procedure PrintStringXYWrap(hPrinter: THandle; s: string; X, Y, Width: real);
procedure NewPrintStringXYWrap(hPrinter: THandle; StrToPrint: string; X, Y, Width: real);
procedure UploadFonts(hPrinter: THandle; sPath: string);
procedure UploadMacro(hPrinter: THandle; sFileName: string; iMacroNumber: integer);
procedure StartMacro(hPrinter: THandle; iMacroNumber: integer);
procedure StopMacro(hPrinter: THandle; iMacroNumber: integer);
procedure ClearMacro(hPrinter: THandle; iMacroNumber: integer);
procedure NewUploadMacro(hPrinter: THandle; sFileName: string; iMacroNumber: integer);
procedure NewStartMacro(hPrinter: THandle; iMacroNumber: integer);
procedure NewStopMacro(hPrinter: THandle; iMacroNumber: integer);
procedure NewClearMacro(hPrinter: THandle; iMacroNumber: integer);
procedure FormFeed(hPrinter: THandle);
procedure PreparePrinter(hPrinter: THandle);
procedure NewPreparePrinter(hPrinter: THandle);
function RightJustify(S: string; N: integer): string;
function FileToString(const sFileName: TFileName): String;
function GetPrinterDriverName(PrinterName: String): String;

procedure StartCheckPrinter(var hPrinter: THandle);
procedure StartReportPrinter(var hPrinter: THandle);
function CleanPCL(PCLFile: String): String;
function GetTopAdjustment(PageNbr: Integer = 0): Integer;
function SelectPrinter(ReportPrinter: Boolean): Integer;

implementation

function SelectPrinter(ReportPrinter: Boolean): Integer;
var
  aPrinterIndex: Integer;
  aPrinterName: string;
begin
  if ReportPrinter then
    aPrinterName := mb_AppSettings['Settings\ReportsPrinter']
  else
    aPrinterName := mb_AppSettings['Settings\ChecksPrinter'];
  if aPrinterName = '' then
    aPrinterIndex := -1
  else
    aPrinterIndex := Printer.Printers.IndexOf(aPrinterName);
  if Printer.PrinterIndex <> aPrinterIndex then
    Printer.PrinterIndex := aPrinterIndex;
  Result := aPrinterIndex;
end;

procedure StartReportPrinter(var hPrinter: THandle);
begin
  StartPrinter(hPrinter, SelectPrinter(True));
end;

procedure StartCheckPrinter(var hPrinter: THandle);
begin
  StartPrinter(hPrinter, SelectPrinter(False));
end;

function FileToString(const sFileName: TFileName): String;
var
  Count: integer;
  f: file;
  Buffer: array[0..BufSize] of char;
  S: String;
begin
  Result := '';
  AssignFile(f, sFileName);
  FileMode := 0;
  Reset(f, 1);
  while not eof(f) do
  begin
    BlockRead(f, Buffer, BufSize + 1, Count);
    if Count > 0 then
    begin
      SetLength(S, Count);
      CopyMemory(@S[1], @Buffer, Count);
      Result := Result + S;
    end;
  end;
  Close(f);
  FileMode := 2;
end;

function GetNumberText(Number: Real): string;
{Procedure to convert check amount to text}
type
  A1H = array[0..19] of string;
  A2H = array[0..9] of string;
const
  A1: A1H = ('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven',
    'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen',
    'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen',
    'Eighteen', 'Nineteen');
  A2: A2H = ('', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy',
    'Eighty', 'Ninety');
var
  ChkAmtNum, DecPos: Integer;
  ChkAmtDec, CheckTxt, ChkAmtTxt: string;
begin
  ChkAmtTxt := FloatToStrF(Number, ffFixed, 15, 2);
  ChkAmtNum := 0;
   {Get the position of the decimal point}
  DecPos := Pos('.', ChkAmtTxt);
  if DecPos <> 0 then
  begin
         {Break amount into number and decimal parts}
    ChkAmtDec := Copy(ChkAmtTxt, DecPos + 1, Length(ChkAmtTxt) - DecPos);
    if Length(ChkAmtDec) = 1 then ChkAmtDec := ChkAmtDec + '0';
    ChkAmtTxt := Copy(ChkAmtTxt, 1, DecPos - 1);
  end;
  if ChkAmtTxt = '0' then
    ChkAmtTxt := ''
  else
    ChkAmtNum := StrToInt(ChkAmtTxt);
   {Million Conversion}
  if (Length(ChkAmtTxt) = 7) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Million ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 6);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Hundred Thousand Conversion}
  if (Length(ChkAmtTxt) = 6) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Hundred ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 5);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
    if Length(ChkAmtTxt) < 4 then CheckTxt := CheckTxt + 'Thousand ';
  end;
   {Ten Thousand Conversion}
  if (Length(ChkAmtTxt) = 5) and (ChkAmtNum <> 0) then
  begin
    ChkAmtNum := StrToInt(Copy(ChkAmtTxt, 1, 2));
    if ChkAmtNum < 20 then
      CheckTxt := CheckTxt + A1[ChkAmtNum] + ' Thousand '
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtTxt[1])];
      if ChkAmtTxt[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtTxt[2])] + 'Thousand '
      else
        CheckTxt := CheckTxt + ' Thousand ';
    end;
    ChkAmtTxt := Copy(ChkAmtTxt, 3, 3);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Thousand Conversion}
  if (Length(ChkAmtTxt) = 4) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Thousand ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 3);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Hundred Conversion}
  if (Length(ChkAmtTxt) = 3) and (ChkAmtNum <> 0) then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])] + ' Hundred ';
    ChkAmtTxt := Copy(ChkAmtTxt, 2, 2);
    ChkAmtNum := StrToInt(ChkAmtTxt);
    ChkAmtTxt := IntToStr(ChkAmtNum);
  end;
   {Ten conversion}
  if (Length(ChkAmtTxt) = 2) and (ChkAmtNum <> 0) then
  begin
    if ChkAmtNum < 20 then
      CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt)] + ' Dollars and '
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtTxt[1])];
      if ChkAmtTxt[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtTxt[2])] + 'Dollars and '
      else
        CheckTxt := CheckTxt + ' Dollars ';
    end;
  end;
   {One conversion}
  if Length(ChkAmtTxt) = 1 then
  begin
    CheckTxt := CheckTxt + A1[StrToInt(ChkAmtTxt[1])];
    if ChkAmtNum = 1 then
      CheckTxt := CheckTxt + ' Dollar and '
    else
      if ChkAmtNum = 0 then
      CheckTxt := CheckTxt + 'Dollars and '
    else
      CheckTxt := CheckTxt + ' Dollars and ';
  end;
  if Length(ChkAmtTxt) = 0 then CheckTxt := CheckTxt + 'No Dollars and ';
   { Add Decimal place text string to Amount Line}
  if DecPos <> 0 then
  begin
    if StrToInt(ChkAmtDec) < 20 then
      CheckTxt := CheckTxt + A1[StrToInt(ChkAmtDec)]
    else
    begin
      CheckTxt := CheckTxt + A2[StrToInt(ChkAmtDec[1])];
      if ChkAmtDec[2] <> '0' then
        CheckTxt := CheckTxt + '-' + A1[StrToInt(ChkAmtDec[2])]
    end;
    if StrToInt(ChkAmtDec) = 1 then
      CheckTxt := CheckTxt + ' Cent'
    else
      CheckTxt := CheckTxt + ' Cents';
  end
  else
    CheckTxt := CheckTxt + 'No Cents';
  Result := CheckTxt;
end;

procedure StartPrinter(var hPrinter: THandle; const Index: Integer);
type
  TDoc_Info_1 = record
    pDocName: pChar;
    pOutputFile: pChar;
    pDataType: pChar;
  end;
var
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port: array[0..255] of char;
  hDeviceMode: THandle;
  Doc_Info_1: TDoc_Info_1;
begin
  if Printer.PrinterIndex <> Index then
    Printer.PrinterIndex := Index;
  Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
  if not WinSpool.OpenPrinter(@Device, hPrinter, nil) then exit;
  Doc_Info_1.pDocName := 'MyDocument';
  Doc_Info_1.pOutputFile := nil;
  Doc_Info_1.pDatatype := 'RAW';
  if StartDocPrinter(hPrinter, 1, @Doc_Info_1) = 0 then
  begin
    WinSpool.ClosePrinter(hPrinter);
    exit;
  end;
  if not StartPagePrinter(hPrinter) then
  begin
    EndDocPrinter(hPrinter);
    WinSpool.ClosePrinter(hPrinter);
    exit;
  end;
end;

procedure StopPrinter(hPrinter: THandle);
begin
  EndPagePrinter(hPrinter);
  EndDocPrinter(hPrinter);
  WinSpool.ClosePrinter(hPrinter);
end;

procedure PrintFile(hPrinter: THandle; const sFileName: string);
var
  Count, BytesWritten: DWord;
  f: file;
  Buffer: Pointer;
begin
  AssignFile(f, sFileName);
  FileMode := 0;
  Reset(f, 1);
  GetMem(Buffer, BufSize);
  while not eof(f) do
  begin
    Blockread(f, Buffer^, BufSize, Count);
    if Count > 0 then
      if not WritePrinter(hPrinter, Buffer, Count, BytesWritten) then
      begin
        EndPagePrinter(hPrinter);
        EndDocPrinter(hPrinter);
        WinSpool.ClosePrinter(hPrinter);
        FreeMem(Buffer, BufSize);
        Close(f);
        FileMode := 2;
        exit;
      end;
  end;
  FreeMem(Buffer, BufSize);
  Close(f);
  FileMode := 2;
end;

procedure NewPrintFile(hPrinter: THandle; const sFileName: string);
var
  Count: integer;
  f: file;
  Buff: TPrnBuffRec;
  pBuff: pPrnBuffRec;
begin
  GetMem(pBuff, SizeOf(TPrnBuffRec));
  Buff := pBuff^;
  AssignFile(f, sFileName);
  FileMode := 0;
  Reset(f, 1);
  while not eof(f) do
  begin
    BlockRead(f, Buff.Buffer, SizeOf(Buff.Buffer), Count);
    if Count > 0 then
    begin
      Buff.BuffLength := Count;
      Escape(hPrinter, PASSTHROUGH, SizeOf(TPrnBuffRec), @Buff, nil);
    end;
  end;
  FreeMem(pBuff);
  Close(f);
  FileMode := 2;
end;

procedure PrintStream(hPrinter: THandle; aStream: TMemoryStream);
var
  Count, BytesWritten: DWord;
  Buffer: Pointer;
begin
  GetMem(Buffer, BufSize);
  repeat
    Count := aStream.Read(Buffer^, BufSize);
    if Count > 0 then
      if not WritePrinter(hPrinter, Buffer, Count, BytesWritten) then
      begin
        EndPagePrinter(hPrinter);
        EndDocPrinter(hPrinter);
        WinSpool.ClosePrinter(hPrinter);
        Break;
      end;
  until Count <> BufSize;
  FreeMem(Buffer, BufSize);
end;

procedure PrintString(hPrinter: THandle; s: string);
var
  BytesWritten: DWord;
  Buf: Pchar;
begin
  GetMem(Buf, BufSize);
  try
    StrPCopySafe(Buf, s, BufSize);
    WritePrinter(hPrinter, Buf, Length(s), BytesWritten);
  finally
    FreeMem(Buf);
  end;
end;

procedure NewPrintString(hPrinter: THandle; StrToPrint: string);
var
  I: Integer;
  S: String;
  Buff: TPrnBuffRec;
  pBuff: pPrnBuffRec;
begin
  GetMem(pBuff, SizeOf(TPrnBuffRec));
  Buff := pBuff^;
  for I := 0 to Length(StrToPrint) div BufSize do
  begin
    if (I + 1) * BufSize > Length(StrToPrint) then
      S := Copy(StrToPrint, I * BufSize + 1, Length(StrToPrint) - (I * BufSize + 1) + 1)
    else
      S := Copy(StrToPrint, I * BufSize + 1, BufSize);
    if S <> '' then
    begin
      CopyMemory(@Buff.Buffer, @S[1], Length(S));
      Buff.BuffLength := Length(S);
      Escape(hPrinter, PASSTHROUGH, SizeOf(TPrnBuffRec), @Buff, nil);
    end;
  end;
  FreeMem(pBuff);
end;


procedure PrintStringXY(hPrinter: THandle; s: string; X, Y: real);
begin
  PrintString(hPrinter, chr(27) + '&a' + IntToStr(Round((X - 3.0) * 72)) + 'H');
  PrintString(hPrinter, chr(27) + '&a' + IntToStr(Round((Y + 1.2) * 72)) + 'V');
  PrintString(hPrinter, s);
end;

procedure NewPrintStringXY(hPrinter: THandle; StrToPrint: string; X, Y: real);
begin
  NewPrintString(hPrinter, chr(27) + '&a' + IntToStr(Round(X * 72)) + 'H');
  NewPrintString(hPrinter, chr(27) + '&a' + IntToStr(Round(Y * 72)) + 'V');
  NewPrintString(hPrinter, StrToPrint);
end;

procedure NewPrintStringXYDots(hPrinter: THandle; StrToPrint: string; X, Y: integer);
begin
  NewPrintString(hPrinter, chr(27) + '*p' + IntToStr(X) + 'X');
  NewPrintString(hPrinter, chr(27) + '*p' + IntToStr(Y) + 'Y');
  NewPrintString(hPrinter, StrToPrint);
end;

procedure PrintFileXY(hPrinter: THandle; s: string; X, Y: real);
begin
  PrintString(hPrinter, chr(27) + '&a' + IntToStr(Round((X - 3.0) * 72)) + 'H');
  PrintString(hPrinter, chr(27) + '&a' + IntToStr(Round((Y + 1.2) * 72)) + 'V');
  PrintFile(hPrinter, s);
  PreparePrinter(hPrinter);
end;

procedure PrintStringXYWrap(hPrinter: THandle; s: string; X, Y, Width: real);
var
  TempWidth, I: Integer;
begin
  TempWidth := Round(Width * 17 / 14);
  I := 0;
  while I * TempWidth < Length(s) do
  begin
    PrintStringXY(hPrinter, copy(s, I * TempWidth + 1, TempWidth), X, Y + I * 1.8);
    inc(I);
  end;
end;

procedure NewPrintStringXYWrap(hPrinter: THandle; StrToPrint: string; X, Y, Width: real);
var
  TempWidth, I: Integer;
begin
  TempWidth := Round(Width * 17 / 14);
  I := 0;
  while I * TempWidth < Length(StrToPrint) do
  begin
    NewPrintStringXY(hPrinter, Copy(StrToPrint, I * TempWidth + 1, TempWidth), X, Y + I * 1.8);
    Inc(I);
  end;
end;

procedure UploadFonts(hPrinter: THandle; sPath: string);
var
  MySearch: TSearchRec;
  Found: Integer;
begin
  if DirectoryExists(sPath) then
  try
    Found := FindFirst(sPath + '\*.hpf', faAnyFile, MySearch);
    while Found = 0 do
    begin
      PrintFile(hPrinter, sPath + '\' + MySearch.Name);
      Found := FindNext(MySearch);
    end;
  finally
    SysUtils.FindClose(MySearch);
  end
  else
    raise EevException.Create('Fonts directory was not found!');
end;

procedure UploadMacro(hPrinter: THandle; sFileName: string; iMacroNumber: integer);
begin
  PrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'Y');
  PrintString(hPrinter, chr(27) + '&f0X');
  PrintString(hPrinter, chr(27) + '&f0S');
  PrintFile(hPrinter, sFileName);
  PrintString(hPrinter, chr(27) + '&f1S');
  PrintString(hPrinter, chr(27) + '&f1X');
  PrintString(hPrinter, chr(27) + '&f10X');
end;

procedure NewUploadMacro(hPrinter: THandle; sFileName: string; iMacroNumber: integer);
begin
  NewPrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'y0x0S');
  NewPrintFile(hPrinter, sFileName);
  NewPrintString(hPrinter, chr(27) + '&f1s1x10X');
end;

procedure StartMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  PrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'Y');
  PrintString(hPrinter, chr(27) + '&f4X');
  PreparePrinter(hPrinter);
end;

procedure NewStartMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  NewPrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'y4X');
end;

procedure StopMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  PrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'Y');
  PrintString(hPrinter, chr(27) + '&f5X');
end;

procedure NewStopMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  NewPrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'y5X');
end;

procedure ClearMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  PrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'Y');
  PrintString(hPrinter, chr(27) + '&f5X');
  PrintString(hPrinter, chr(27) + '&f6X');
  PrintString(hPrinter, chr(27) + '&f7X');
  PrintString(hPrinter, chr(27) + '&f8X');
end;

procedure NewClearMacro(hPrinter: THandle; iMacroNumber: integer);
begin
  NewPrintString(hPrinter, chr(27) + '&f' + IntToStr(iMacroNumber) + 'y5x6x7x8X');
end;

procedure FormFeed(hPrinter: THandle);
begin
  PrintString(hPrinter, chr(27) + '&l0H');
end;

procedure PreparePrinter(hPrinter: THandle);
begin
  PrintString(hPrinter, chr(27) + '&l0E');
  PrintString(hPrinter, chr(27) + '&a0L');
  PrintString(hPrinter, chr(27) + '(s3T');
  PrintString(hPrinter, chr(27) + '(s0P');
  PrintString(hPrinter, chr(27) + '(s12H');
  PrintString(hPrinter, chr(27) + '(s10V');
end;

procedure NewPreparePrinter(hPrinter: THandle);
begin
  NewPrintString(hPrinter, chr(27) + '&l0ol0e&a0L');
end;

function RightJustify(S: string; N: integer): string;
var
  Count: Integer;
  TempS: string;
begin
  Count := 0;
  TempS := S;
  while TempS[Length(TempS)] = ' ' do
  begin
    Delete(TempS, Length(TempS), 1);
    Insert(' ', TempS, 1);
    Inc(Count);
    if Count > Length(S) then Break;
  end;
  for Count := 1 to N - Length(S) do
    Insert(' ', TempS, 1);
  Result := TempS;
end;

function GetPrinterDriverName(PrinterName: String): String;
var
  hPrinter: THandle;
  PrinterInfo: PPrinterInfo2;
  Needed: PDWORD;
begin
  Result := '';
  if Pos(' on ', PrinterName) <> 0 then
    PrinterName := Copy(PrinterName, 1, Pos(' on ', PrinterName) - 1);
  WinSpool.OpenPrinter(PChar(PrinterName), hPrinter, nil);
  GetMem(PrinterInfo, SizeOf(TPrinterInfo2));
  GetMem(Needed, SizeOf(PDWORD));
  try
    Needed^ := SizeOf(TPrinterInfo2);
    if not GetPrinter(hPrinter, 2, PrinterInfo, SizeOf(TPrinterInfo2), Needed) then
    begin
      FreeMem(PrinterInfo);
      GetMem(PrinterInfo, Needed^);
      if not GetPrinter(hPrinter, 2, PrinterInfo, Needed^, Needed) then
        Exit;
    end;
    Result := PrinterInfo^.pDriverName;
  finally
    FreeMem(Needed);
    FreeMem(PrinterInfo);
    WinSpool.ClosePrinter(hPrinter);
  end;
end;

function CleanPCL(PCLFile: String): String;
var
  x: integer;
  sPCLFile: String;
begin
  sPCLFile := PCLFile;

// Remove [ESC E] (Escape) at the beginning of the file
  if (Length(sPCLFile) > 1) and (sPCLFile[1] = chr(27)) and (sPCLFile[2] = 'E') then
    Delete(sPCLFile, 1, 2);

// Remove [ESC] (form feed) at the end of the file
  if (sPCLFile[Length(sPCLFile)] = chr(12)) then
    Delete(sPCLFile, Length(sPCLFile), 1);

// Remove Executive Paper size
  x := Pos(chr(27) + '&l1A', sPCLFile);
  if (x > 0) then
    Delete(sPCLFile, x, 5);

 // Remove Letter Paper size
  x := Pos(chr(27) + '&l2A', sPCLFile);
  if (x > 0) then
    Delete(sPCLFile, x, 5);

 // Remove legal paper size
  x := Pos(chr(27) + '&l3A', sPCLFile);
  if (x > 0) then
    Delete(sPCLFile, x, 5);

  // Remove A4 paper size
  x := Pos(chr(27) + '&l26A', sPCLFile);
  if (x > 0) then
    Delete(sPCLFile, x, 6);

  // Remove Number of copies
  x := Pos(chr(27) + '&l1X', sPCLFile);
  if (x > 0) then
    Delete(sPCLFile, x, 5);

  Result := sPCLFile;
end;

function GetTopAdjustment(PageNbr: Integer = 0): Integer;
var
  PrinterName: String;
  DPIAdjust: Integer;
begin
  PrinterName := UpperCase(GetPrinterDriverName(Printer.Printers[Printer.PrinterIndex]));
  DPIAdjust := Round(GetDeviceCaps(Printer.Handle, LOGPIXELSX) / 100);
  if (Pos('HP LASERJET 4050', PrinterName) <> 0) or (Pos('HP LASERJET 8', PrinterName) <> 0) then
  begin
    if (Pos('HP LASERJET 8', PrinterName) <> 0) and (Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and (PageNbr > 0) then
      Result := - Round(DPIAdjust * 10)
    else
      Result := - Round(DPIAdjust * 25);
  end
  else
  if (Win32Platform = VER_PLATFORM_WIN32_NT) and ((Pos('HP LASERJET 4', PrinterName) <> 0) or (Pos('HP LASERJET 5', PrinterName) <> 0)) then
  begin
    if Win32MajorVersion = 5 then // Windows 2000
      Result := - Round(DPIAdjust * 20)
    else
      Result := - Round(DPIAdjust * 25);
  end
  else
    Result := - Round(DPIAdjust * 10);
end;

end.
