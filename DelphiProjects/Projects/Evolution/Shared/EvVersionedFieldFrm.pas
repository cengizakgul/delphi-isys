unit EvVersionedFieldFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, TypInfo;

type
  TevVersionedField = class(TevVersionedFieldBase)
    lValue: TevLabel;
    procedure deBeginDateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FFieldEditor: TControl;
    FFieldValues: IevDataSet;
    FFieldValuesAsOfPeriod: array [0..1] of TisDate;
    procedure BuildDropDownListIfNeeds(Sender: TObject);
    procedure LookupTextValue(Sender: TObject);
    function  CreateValueEditor: TControl;

  protected
    procedure DoAfterDataBuild; override;
    procedure CustomKeyUp(Sender: TObject; var Key: Word;
            Shift: TShiftState);
    procedure DisplayOrOverrideNavInsertMessage; override;
  end;

implementation

{$R *.dfm}


{ TevVersionedField }

procedure TevVersionedField.BuildDropDownListIfNeeds(Sender: TObject);
var
  dbCB: TevDBComboBox;
  d: TDateTime;
  Vals: IisListOfValues;
begin
  if not (Data.State in [dsEdit, dsInsert]) then
    Data.Edit;

  if (FFieldValuesAsOfPeriod[0] <> Data['begin_date']) or (FFieldValuesAsOfPeriod[1] <> Data['end_date']) then
  begin
    if Data['end_date'] > SysDate then
      d := SysDate
    else
      d := Data['end_date'];

    Vals := ctx_DBAccess.GetFieldValues(Table, Fields.ParamName(0), Nbr, d, Data['end_date'], False);
    FFieldValues := IInterface(Vals.Value['Data']) as IevDataSet;
    FFieldValues.IndexFieldNames := 'description';

    dbCB := Sender as TevDBComboBox;
    dbCB.Items.BeginUpdate;
    try
      dbCB.Items.Clear;
      FFieldValues.First;
      dbCB.Items.Capacity := FFieldValues.RecordCount;
      while not FFieldValues.Eof do
      begin
        dbCB.Items.Add(FFieldValues.Fields[1].AsString);
        FFieldValues.Next;
      end;
    finally
      dbCB.Items.EndUpdate;
    end;

    FFieldValuesAsOfPeriod[0] := Data['begin_date'];
    FFieldValuesAsOfPeriod[1] := Data['end_date'];
  end;
end;


function TevVersionedField.CreateValueEditor: TControl;
var
  dbEdit: TevDBEdit;
  dbDateEdit: TevDBDateTimePicker;
  dbCB: TevDBComboBox;
  FldType: TFieldType;
begin
  FldType := Data.FieldByName(Fields.ParamName(0)).DataType;

  if Fields[0].Value['ShowAsText'] then
  begin
    dbCB := TevDBComboBox.Create(Self);
    dbCB.DataSource := dsFieldData;
    dbCB.DataField := '$' + Fields.ParamName(0);
    dbCB.OnDropDown := BuildDropDownListIfNeeds;
    dbCB.OnChange := LookupTextValue;
    dbCB.DropDownCount := 16;
    dbCB.MapList := False;
    dbCB.AllowClearKey := not Fields[0].Value['Required'];
    Result := dbCB;
  end

  else if FldType in [ftDate, ftTime, ftDateTime] then
  begin
    dbDateEdit := TevDBDateTimePicker.Create(Self);

    case FldType of
      ftDate:     dbDateEdit.UnboundDataType := wwDTEdtDate;
      ftTime:     dbDateEdit.UnboundDataType := wwDTEdtTime;
      ftDateTime: dbDateEdit.UnboundDataType := wwDTEdtDateTime;
    end;

    dbDateEdit.DataSource := dsFieldData;
    dbDateEdit.DataField := Fields.ParamName(0);
    dbDateEdit.OnChange := UpdateFieldOnChange;
    Result := dbDateEdit;
  end

  else
  begin
    dbEdit := TevDBEdit.Create(Self);
    dbEdit.DataSource := dsFieldData;
    dbEdit.DataField := Fields.ParamName(0);
    dbEdit.OnChange := UpdateFieldOnChange;
    dbEdit.Picture.PictureMaskFromDataSet := True;
    dbEdit.Picture.AllowInvalidExit := False;
    dbEdit.OnKeyUp :=  CustomKeyUp;

    Result := dbEdit;
  end;

  Result.Top := deBeginDate.Top;
  Result.Left := lValue.Left;
  Result.Width := pnlButtons.Left - Result.Left - 20;
  Result.Anchors := [akLeft, akTop, akRight];
  Result.Parent := pnlEdit;
end;

procedure TevVersionedField.DoAfterDataBuild;
var
  i: Integer;
begin
  inherited;

  if not Assigned(FFieldEditor) then
  begin
    for i := 0 to Fields.Count - 1 do
      Fields[i].AddValue('ShowAsText', Data.Fields.FindField('$' + Fields.ParamName(i)) <> nil);

    FFieldEditor := CreateValueEditor;
  end;

  FFieldValues := nil;
  FFieldValuesAsOfPeriod[0] := EmptyDate;
  FFieldValuesAsOfPeriod[1] := EmptyDate;
end;

procedure TevVersionedField.LookupTextValue(Sender: TObject);
begin
  if Assigned(FFieldValues) and Assigned(Data) then
    if Data.State in [dsEdit, dsInsert] then
    begin
      Data['$' + Fields.ParamName(0)] := (Sender as TevDBComboBox).Text;
      if FFieldValues.FindKey([(Sender as TevDBComboBox).Text]) then
        Data[Fields.ParamName(0)] := FFieldValues.Fields[0].AsString
      else
        Data[Fields.ParamName(0)] := Null;
    end;
end;

procedure TevVersionedField.deBeginDateChange(Sender: TObject);
begin
  inherited;
  if Fields[0].Value['Reference'] and Fields[0].Value['ShowAsText'] and (Data.State in [dsEdit, dsInsert]) then
  begin
    Data['$' + Fields.ParamName(0)] := '';
    Data[Fields.ParamName(0)] := Null;
    FFieldValues := nil;
  end;
end;

procedure TevVersionedField.FormShow(Sender: TObject);
begin
  inherited;
  lValue.Caption := Iff(Fields[0].Value['Required'], '~', '') + Fields[0].Value['Title'];
end;

procedure TevVersionedField.CustomKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;
begin
  if (Sender is TEvDBEdit) then
  begin
    btnNavOk.Enabled := False;
    s := (Sender as TEvDBEdit).Text;
     if Length(s) >= 1 then btnNavOk.Enabled := True;
  end;
end;

procedure TevVersionedField.DisplayOrOverrideNavInsertMessage;
begin
  if (Fields.ParamName(0) = 'EXEMPT_EXCLUDE') then
    ShowMessage('Please Note: Changes should be made as of the next check date.')
  else
    inherited;
end;

end.

