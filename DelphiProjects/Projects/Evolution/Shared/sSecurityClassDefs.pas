// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sSecurityClassDefs;

interface

uses Classes, Graphics, Contnrs, SysUtils, SSecurityInterface, EvConsts, isBasicUtils,
    evExceptions, EvStreamUtils;

type

  TSecurityAtom = class;
  TSecurityElement = class;
  TSecurityElementCollection = class;
  TSecurityAtomCollection = class;

  ISecurityCollectionRing = interface
  ['{D63E61A1-6976-4545-AE7F-D5E61816A456}']
    function Collections(Index: Integer): TSecurityAtomCollection;
    function CollectionCount: Integer;
  end;

  TOnAtomContextChange = procedure(const Atom: TSecurityAtom) of object;

  TSecurityContext = class
  private
    function GetCaption: ShortString;
  protected
    FCaption: ShortString;
    FTag: ShortString;
    FPriority: Integer;
    FIcon: TIcon;
  public
    constructor CreateFromStream(const t: IisStream);
    property Tag: ShortString read FTag;
    property Caption: ShortString read GetCaption;
    property Priority: Integer read FPriority;
    property Icon: TIcon read FIcon;
    procedure StreamStructureTo(const t: IisStream);
    procedure MarkFromGroup(const AValue: Boolean);
    function  IsFromGroup: Boolean;
  end;

  TSecurityAtom = class
  protected
    FTag: ShortString;
    FContexts: TList;
    FElements: TList;
    FCaption: ShortString;
    FAtomType: ShortString;
    FCurrentContextIndex: Integer;
    FCollection: TSecurityAtomCollection;
    FOnContextChange: TOnAtomContextChange;
    FChangingContext: Boolean;
    function GetContext(Intex: Integer): TSecurityContext;
    function GetContextCount: Integer;
    function GetElement(Intex: Integer): TSecurityElement;
    function GetElementCount: Integer;
    function GetCurrentContext: TSecurityContext;
    procedure SetCurrentContext(const Value: TSecurityContext);
    procedure SetOnContextChange(const Value: TOnAtomContextChange);
  public
    constructor CreateFromStream(const t: IisStream; const Collection: TSecurityAtomCollection);
    destructor Destroy; override;
    procedure StreamStructureTo(const t: IisStream; const MaxContext: TSecurityContext = nil);
    function GetLinkedAtoms(const DoNotLinkThroughDataset: Boolean = False): TSecurityAtomCollection;
    function GetContextByTag(const Tag: string): TSecurityContext;
    procedure LinkElement(const Element: TSecurityElement);
    property Tag: ShortString read FTag;
    property AtomType: ShortString read FAtomType;
    property Caption: ShortString read FCaption;
    property ContextCount: Integer read GetContextCount;
    property Contexts[Intex: Integer]: TSecurityContext read GetContext;
    procedure DeleteContext(Index: Integer);
    property ElementCount: Integer read GetElementCount;
    property Elements[Intex: Integer]: TSecurityElement read GetElement;
    property CurrentContext: TSecurityContext read GetCurrentContext write SetCurrentContext;
    property Collection: TSecurityAtomCollection read FCollection;
    property OnContextChange: TOnAtomContextChange read FOnContextChange write SetOnContextChange;
  end;

  TElementStates = class
  protected
    FStates: TElementStatesArray;
    function GetItems(Index: Integer): TElementState;
    function GetCount: Integer;
  public
    constructor CreateFromStream(const t: IisStream);
    procedure StreamStructureTo(const t: IisStream);
    property Items[Index: Integer]: TElementState read GetItems; default;
    property Count: Integer read GetCount;
    function IndexOfState(const State: ShortString): Integer;
  end;

  TElementToAtomLink = class
  protected
    FAtom: TSecurityAtom;
    FRelations: TAtomElementRelationsArray;
  public
    property Atom: TSecurityAtom read FAtom;
    property Relations: TAtomElementRelationsArray read FRelations;
    constructor CreateFromStream(const t: IisStream; const Element: TSecurityElement);
    procedure StreamStructureTo(const t: IisStream);
  end;

  TElementToAtomLinks = class(TObjectList)
  private
    function GetItems(Index: Integer): TElementToAtomLink;
  public
    constructor Create;
    constructor CreateFromStream(const t: IisStream; const Element: TSecurityElement);
    procedure StreamStructureTo(const t: IisStream);
    property Items[Index: Integer]: TElementToAtomLink read GetItems; default;
  end;

  TSecurityElement = class
  protected
    FAtomLinks: TElementToAtomLinks;
    FTag: ShortString;
    FElementType: ShortString;
    FStates: TElementStates;
    FStateIndex: Integer;
    FInStateChangeProcess: Boolean;
    FCollection: TSecurityElementCollection;
    function GetLinkedAtom(Index: Integer): TElementToAtomLink;
    function GetLinkedAtomCount: Integer;
    procedure SetStateIndex(const Value: Integer);
    function GetStates(Index: Integer): TElementState;
    procedure ChangeAtomContext(const Atom: TSecurityAtom; const Relation: TAtomElementRelation); virtual;
    procedure ChangeElementState(const Atom: TSecurityAtom; const Relation: TAtomElementRelation); virtual;
    function ExpectedAtomContext(const Atom: TSecurityAtom): TSecurityContext;
    function ExpectedState(const Atom: TSecurityAtom): TElementState;
  public
    constructor CreateFromStream(const t: IisStream; const Collection: TSecurityElementCollection);
    destructor Destroy; override;
    procedure StreamStructureTo(const t: IisStream);
    procedure NotifyContextChange(SecurityAtom: TSecurityAtom);
    property Tag: ShortString read FTag;
    property ElementType: ShortString read FElementType;
    property States[Index: Integer]: TElementState read GetStates;
    property CurrentStateIndex: Integer read FStateIndex write SetStateIndex;
    function IndexOfState(const State: ShortString): Integer;
    property LinkedAtomCount: Integer read GetLinkedAtomCount;
    property LinkedAtoms[Index: Integer]: TElementToAtomLink read GetLinkedAtom;
    property Collection: TSecurityElementCollection read FCollection;
  end;

  TSecuritySortedCollection = class
  private
    FItems: TStringList;
    FReleaseCollectedInstances: Boolean;
  public
    constructor Create(const ReleaseCollectedInstances: Boolean = True);
    destructor Destroy; override;
    procedure Clear;
  end;

  TSecurityElementCollection = class(TSecuritySortedCollection)
  private
    FSecurityAtomCollection: TSecurityAtomCollection;
    function GetElements(Index: Integer): TSecurityElement;
    function GetElementCount: Integer;
  public
    constructor CreateFromStream(const t: IisStream; const SecurityAtomCollection: TSecurityAtomCollection);
    procedure StreamStructureTo(const t: IisStream);
    property SecurityAtomCollection: TSecurityAtomCollection read FSecurityAtomCollection;
    property Elements[Index: Integer]: TSecurityElement read GetElements;
    property ElementCount: Integer read GetElementCount;
    procedure AddUniqueElement(Element: TSecurityElement);
    function FindElement(const Tag: ShortString; const ElementType: ShortString): TSecurityElement;
  end;

  TSecurityAtomCollection = class(TSecuritySortedCollection)
  private
    FSecurityElementCollection: TSecurityElementCollection;
    function GetAtoms(Index: Integer): TSecurityAtom;
    function GetAtomCount: Integer;
  protected
  public
    constructor Create(const ReleaseCollectedInstances: Boolean = True);
    constructor CreateFromStream(const t: IisStream; const ReleaseCollectedInstances: Boolean = True);
    destructor  Destroy; override;
    procedure   StreamStructureTo(const t: IisStream; const MaxRights: ISecurityCollectionRing = nil);
    procedure   SaveContextsToStream(const t: IisStream; const MaxRights: ISecurityCollectionRing = nil);
    procedure   LoadContextsFromStream(const t: IisStream; const MaxRights: ISecurityCollectionRing = nil);
    property    Atoms[Index: Integer]: TSecurityAtom read GetAtoms;
    property    AtomCount: Integer read GetAtomCount;
    property    SecurityElementCollection: TSecurityElementCollection read FSecurityElementCollection;
    procedure   AddUniqueAtom(Atom: TSecurityAtom);
    function    FindAtom(const Tag: ShortString; const AtomType: ShortString): TSecurityAtom;
    procedure   DeleteAtom(Atom: TSecurityAtom);
  end;

const
  msgWrongStreamFmt = 'Wrong stream format';

implementation

const
  PreviousStreamVersion = 1001;
  CurrentStreamVersion = 1002;


function StripString(const s: string): string;
begin
  Result := StringReplace(s, '&', '', [rfReplaceAll]);
end;

{ TSecurityAtomContext }

constructor TSecurityContext.CreateFromStream(const t: IisStream);
begin
  FTag := t.ReadString;
  FCaption := t.ReadString;
  FPriority := t.ReadInteger;
  if t.ReadBoolean then
    t.ReadString;
end;

function TSecurityContext.GetCaption: ShortString;
var
  s: String;
begin
  s := FCaption;

  if IsFromGroup then
    GetNextStrValue(s, 'G ');

  Result := s;  
end;

function TSecurityContext.IsFromGroup: Boolean;
begin
  Result := StartsWith(FCaption, 'G ');
end;

procedure TSecurityContext.MarkFromGroup(const AValue: Boolean);
var
  s: String;
begin
  if AValue <> IsFromGroup then
    if AValue then
      FCaption := 'G ' + FCaption
    else
    begin
      s := FCaption;
      GetNextStrValue(s, 'G ');
      FCaption := s;
    end;
end;

procedure TSecurityContext.StreamStructureTo(const t: IisStream);
begin
  t.WriteString(FTag);
  t.WriteString(FCaption);
  t.WriteInteger(FPriority);
  t.WriteBoolean(False);
end;

{ TSecurityAtom }

constructor TSecurityAtom.CreateFromStream(const t: IisStream;
  const Collection: TSecurityAtomCollection);
var
  i, j: Integer;
begin
  FContexts := TList.Create;
  FElements := TList.Create;
  FAtomType := t.ReadString;
  FTag := StripString(t.ReadString);
  FCaption := t.ReadString;
  FChangingContext := False;
  FCollection := Collection;
  j := t.ReadInteger;
  for i := 0 to j do
    FContexts.Add(TSecurityContext.CreateFromStream(t));
  Assert(ContextCount > 0);
  CurrentContext := GetContextByTag(ctDisabled);
end;

destructor TSecurityAtom.Destroy;
var
  i: Integer;
begin
  for i := 0 to FContexts.Count - 1 do
    TSecurityContext(FContexts.Items[i]).Free;
  FContexts.Free;
  FElements.Free;
  inherited;
end;

procedure TSecurityAtom.StreamStructureTo(const t: IisStream; const MaxContext: TSecurityContext = nil);

  procedure WriteContexts;
  var
    i, j: Integer;
  begin
    j := 0;
    for i := 0 to ContextCount - 1 do
      if not Assigned(MaxContext) or (Contexts[i].Priority <= MaxContext.Priority) then
        Inc(j);
    t.WriteInteger(j - 1);
    for i := 0 to ContextCount - 1 do
      if not Assigned(MaxContext) or (Contexts[i].Priority <= MaxContext.Priority) then
        Contexts[i].StreamStructureTo(t);
  end;

begin
  t.WriteString(AtomType);
  t.WriteString(Tag);
  t.WriteString(Caption);
  WriteContexts;
end;

function TSecurityAtom.GetContext(Intex: Integer): TSecurityContext;
begin
  Result := TSecurityContext(FContexts[Intex]);
end;

function TSecurityAtom.GetContextByTag(const Tag: string): TSecurityContext;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to ContextCount - 1 do
    if Contexts[i].Tag = Tag then
    begin
      Result := Contexts[i];
      Break;
    end;
end;

function TSecurityAtom.GetContextCount: Integer;
begin
  Result := FContexts.Count;
end;

function TSecurityAtom.GetCurrentContext: TSecurityContext;
begin
  if ContextCount > 0 then
    Result := Contexts[FCurrentContextIndex]
  else
    Result := nil;
end;

function TSecurityAtom.GetElement(Intex: Integer): TSecurityElement;
begin
  Result := TSecurityElement(FElements[Intex]);
end;

function TSecurityAtom.GetElementCount: Integer;
begin
  Result := FElements.Count;
end;

function TSecurityAtom.GetLinkedAtoms(const DoNotLinkThroughDataset: Boolean = False): TSecurityAtomCollection;
var
  i, j: Integer;
begin
  Result := TSecurityAtomCollection.Create(False);
  for i := 0 to ElementCount - 1 do
    if not DoNotLinkThroughDataset
    or (Elements[i].ElementType <> etDataset) then
      for j := 0 to Elements[i].LinkedAtomCount - 1 do
        if (Result.FindAtom(Elements[i].LinkedAtoms[j].Atom.Tag, Elements[i].LinkedAtoms[j].Atom.AtomType) = nil)
          and (Elements[i].LinkedAtoms[j].Atom <> Self) then
          Result.AddUniqueAtom(Elements[i].LinkedAtoms[j].Atom);
end;

procedure TSecurityAtom.SetCurrentContext(const Value: TSecurityContext);
var
  i, j: Integer;
begin
  i := FContexts.IndexOf(Value);
  Assert(i > -1, 'Attempt to set unavailable context.');
  if not FChangingContext and (i <> FCurrentContextIndex) then
  begin
    FChangingContext := True;
    try
      FCurrentContextIndex := i;
      for j := 0 to ElementCount - 1 do
        Elements[j].NotifyContextChange(Self);
      if Assigned(FOnContextChange) then FOnContextChange(Self);
    finally
      FChangingContext := False;
    end;
  end;
end;

procedure TSecurityAtom.SetOnContextChange(
  const Value: TOnAtomContextChange);
begin
  FOnContextChange := Value;
end;

procedure TSecurityAtom.DeleteContext(Index: Integer);
begin
  TSecurityContext(FContexts[Index]).Free;
  FContexts.Delete(Index);
end;

{ TSecurityElement }

procedure TSecurityElement.ChangeAtomContext(const Atom: TSecurityAtom;
  const Relation: TAtomElementRelation);
var
  i: Integer;
  c, c2: TSecurityContext;
begin
  case Relation.Element2Atom of
  ltEqual:
  begin
    c := Atom.GetContextByTag(Relation.AtomContext);
    if Assigned(c) then
      Atom.CurrentContext := c;
  end;
  ltMinLimit, ltKeepLowest:
    begin
      c := Atom.GetContextByTag(Relation.AtomContext);
      Assert(Assigned(c));
      for i := 0 to Atom.ElementCount-1 do
        if Atom.Elements[i] <> Self then
        begin
          c2 := Atom.Elements[i].ExpectedAtomContext(Atom);
          if Assigned(c2)
          and (c.Priority <= c2.Priority) then
            c := c2;
        end;
      Atom.CurrentContext := c;
    end;
  ltMaxLimit:
    begin
      c2 := Atom.GetContextByTag(Relation.AtomContext);
      Assert(Assigned(c2));
      if Atom.CurrentContext.Priority > c2.Priority then
        Atom.CurrentContext := c2;
    end;
  ltNone: ;
  else
    Assert(False);
  end;
end;

procedure TSecurityElement.ChangeElementState(const Atom: TSecurityAtom;
  const Relation: TAtomElementRelation);
var
  i: Integer;
  r, r2: TElementState;
begin
  case Relation.Atom2Element of
  ltEqual:
    CurrentStateIndex := IndexOfState(Relation.ElementState);
  ltMinLimit, ltKeepLowest:
    begin
      r := States[IndexOfState(Relation.ElementState)];
      for i := 0 to LinkedAtomCount-1 do
        if LinkedAtoms[i].Atom <> Atom then
        begin
          r2 := ExpectedState(LinkedAtoms[i].Atom);
          if (r2.Priority > r.Priority) then
            r := r2;
        end;
      CurrentStateIndex := IndexOfState(r.Tag);
    end;
  ltMaxLimit:
      if States[CurrentStateIndex].Priority > States[IndexOfState(Relation.ElementState)].Priority then
        CurrentStateIndex := IndexOfState(Relation.ElementState);
  ltNone: ;
  else
    Assert(False);
  end;
end;

constructor TSecurityElement.CreateFromStream(const t: IisStream;
  const Collection: TSecurityElementCollection);
  function GetLowestPriorityStateIndex: Integer;
  var
    i, pLowest: Integer;
  begin
    pLowest := High(pLowest);
    Result := -1;
    for i := 0 to FStates.Count-1 do
      if FStates[i].Priority < pLowest then
      begin
        Result := i;
        pLowest := FStates[i].Priority;
      end;
    Assert(Result <> -1);
  end;
begin
  FCollection := Collection;
  FTag := StripString(t.ReadString);
  FElementType := t.ReadString;
  FStates := TElementStates.CreateFromStream(t);
  FStateIndex := GetLowestPriorityStateIndex;
  FAtomLinks := TElementToAtomLinks.CreateFromStream(t, Self);
  FInStateChangeProcess := False;
end;

destructor TSecurityElement.Destroy;
begin
  FStates.Free;
  FAtomLinks.Free;
  inherited;
end;

function TSecurityElement.ExpectedAtomContext(
  const Atom: TSecurityAtom): TSecurityContext;
var
  i, j: Integer;
begin
  Result := nil;
  for i := 0 to LinkedAtomCount-1 do
    if LinkedAtoms[i].Atom = Atom then
      for j := Low(LinkedAtoms[i].Relations) to High(LinkedAtoms[i].Relations) do
        if LinkedAtoms[i].Relations[j].ElementState = States[CurrentStateIndex].Tag then
          if LinkedAtoms[i].Relations[j].Element2Atom <> ltNone then
          begin
            Result := Atom.GetContextByTag(LinkedAtoms[i].Relations[j].AtomContext);
            Break;
          end;
end;

function TSecurityElement.ExpectedState(const Atom: TSecurityAtom): TElementState;
var
  i, j: Integer;
  b: Boolean;
begin
  b := False;
  for i := 0 to LinkedAtomCount-1 do
    if LinkedAtoms[i].Atom = Atom then
      for j := Low(LinkedAtoms[i].Relations) to High(LinkedAtoms[i].Relations) do
        if LinkedAtoms[i].Relations[j].AtomContext = Atom.CurrentContext.Tag then
          if LinkedAtoms[i].Relations[j].Atom2Element <> ltNone then
          begin
            Result := States[IndexOfState(LinkedAtoms[i].Relations[j].ElementState)];
            b := True;
            Break;
          end;
  Assert(b);
end;

function TSecurityElement.GetLinkedAtom(Index: Integer): TElementToAtomLink;
begin
  Result :=  FAtomLinks[Index];
end;

function TSecurityElement.GetLinkedAtomCount: Integer;
begin
  Result := FAtomLinks.Count;
end;

function TSecurityElement.GetStates(Index: Integer): TElementState;
begin
  Result := FStates[Index];
end;

function TSecurityElement.IndexOfState(const State: ShortString): Integer;
begin
  Result := FStates.IndexOfState(State);
end;

procedure TSecurityElement.NotifyContextChange(
  SecurityAtom: TSecurityAtom);
var
  i, j: Integer;
begin
  for i := 0 to LinkedAtomCount-1 do
    if LinkedAtoms[i].Atom = SecurityAtom then
      for j := Low(LinkedAtoms[i].Relations) to High(LinkedAtoms[i].Relations) do
        if LinkedAtoms[i].Relations[j].AtomContext = SecurityAtom.CurrentContext.Tag then
        begin
          ChangeElementState(SecurityAtom, LinkedAtoms[i].Relations[j]);
          Break;
        end;
end;

procedure TSecurityElement.SetStateIndex(const Value: Integer);
var
  i, j: Integer;
begin
  Assert((Value >= 0) and (Value < FStates.Count), 'Bad state index: '+ IntToStr(Value));
  if Value <> FStateIndex then
  begin
    Assert(not FInStateChangeProcess, 'Recurrent try to set element''s state');
    FInStateChangeProcess := True;
    try
      FStateIndex := Value;
      for i := 0 to LinkedAtomCount - 1 do
      begin
        for j := Low(LinkedAtoms[i].Relations) to High(LinkedAtoms[i].Relations) do
          if LinkedAtoms[i].Relations[j].ElementState = FStates[FStateIndex].Tag then
          begin
            ChangeAtomContext(LinkedAtoms[i].Atom, LinkedAtoms[i].Relations[j]);
            Break;
          end;
      end;
    finally
      FInStateChangeProcess := False;
    end;
  end;
end;

procedure TSecurityElement.StreamStructureTo(const t: IisStream);
begin
  t.WriteString(Tag);
  t.WriteString(ElementType);
  FStates.StreamStructureTo(t);
  FAtomLinks.StreamStructureTo(t);
end;

{ TSecuritySortedCollection }

procedure TSecuritySortedCollection.Clear;
var
  i: Integer;
begin
  if Assigned(FItems) then
  begin
    if FReleaseCollectedInstances then
      for i := 0 to FItems.Count - 1 do
        FItems.Objects[i].Free;
    FItems.Clear;
  end;
end;

constructor TSecuritySortedCollection.Create(const ReleaseCollectedInstances: Boolean = True);
begin
  FItems := TStringList.Create;
  FItems.Sorted := True;
  FReleaseCollectedInstances := ReleaseCollectedInstances;
end;

destructor TSecuritySortedCollection.Destroy;
begin
  Clear;
  FItems.Free;
  inherited;
end;

{ TSecurityElementCollection }

procedure TSecurityElementCollection.AddUniqueElement(
  Element: TSecurityElement);
begin
  Assert(FindElement(Element.Tag, Element.ElementType) = nil, 'Attempt to add duplicated element entry');
  FItems.AddObject(Element.ElementType+ ';'+ Element.Tag, Element)
end;

constructor TSecurityElementCollection.CreateFromStream(const t: IisStream;
  const SecurityAtomCollection: TSecurityAtomCollection);
var
  i, j: Integer;
begin
  inherited Create;
  FSecurityAtomCollection := SecurityAtomCollection;
  i := t.ReadInteger;
  for j := 0 to i-1 do
    AddUniqueElement(TSecurityElement.CreateFromStream(t, Self));
end;

procedure TSecurityElementCollection.StreamStructureTo(const t: IisStream);
var
  i: Integer;
begin
  t.WriteInteger(ElementCount);
  for i := 0 to ElementCount-1 do
    Elements[i].StreamStructureTo(t);
end;

function TSecurityElementCollection.FindElement(const Tag: ShortString;
  const ElementType: ShortString): TSecurityElement;
var
  i: Integer;
begin
  i := FItems.IndexOf(ElementType+ ';'+ StripString(Tag));
  if i = -1 then
    Result := nil
  else
    Result := TSecurityElement(FItems.Objects[i]);
end;

function TSecurityElementCollection.GetElementCount: Integer;
begin
  Result := FItems.Count;
end;

function TSecurityElementCollection.GetElements(
  Index: Integer): TSecurityElement;
begin
  Result := TSecurityElement(FItems.Objects[Index]);
end;

{ TSecurityAtomCollection }

procedure TSecurityAtomCollection.AddUniqueAtom(Atom: TSecurityAtom);
begin
  Assert(FindAtom(Atom.Tag, Atom.AtomType) = nil, 'Attempt to add duplicated element entry');
  FItems.AddObject(Atom.AtomType+ ';'+ Atom.Tag, Atom)
end;

constructor TSecurityAtomCollection.CreateFromStream(const t: IisStream;
  const ReleaseCollectedInstances: Boolean = True);
var
  i, j, ver: Integer;
begin
  inherited Create(ReleaseCollectedInstances);
  ver := t.ReadInteger;
  Assert(ver >= PreviousStreamVersion, msgWrongStreamFmt);
  i := t.ReadInteger;
  for j := 0 to i- 1 do
    AddUniqueAtom(TSecurityAtom.CreateFromStream(t, Self));
  FSecurityElementCollection := TSecurityElementCollection.CreateFromStream(t, Self);
end;

destructor TSecurityAtomCollection.Destroy;
begin
  FSecurityElementCollection.Free;
  inherited;
end;

function TSecurityAtomCollection.FindAtom(const Tag: ShortString;
  const AtomType: ShortString): TSecurityAtom;
var
  i: Integer;
begin
  i := FItems.IndexOf(AtomType+ ';'+ StripString(Tag));
  if i = -1 then
    Result := nil
  else
    Result := TSecurityAtom(FItems.Objects[i]);
end;

procedure TSecurityAtomCollection.StreamStructureTo(const t: IisStream;
  const MaxRights: ISecurityCollectionRing);
var
  i, j: Integer;
  oc: TSecurityContext;
  oa: TSecurityAtom;
begin
  t.WriteInteger(CurrentStreamVersion);
  t.WriteInteger(AtomCount);
  for i := 0 to AtomCount- 1 do
  begin
    oc := nil;
    if Assigned(MaxRights) then
      for j := 0 to MaxRights.CollectionCount - 1 do
      begin
        oa := MaxRights.Collections(j).FindAtom(Atoms[i].Tag, Atoms[i].AtomType);
        if Assigned(oa) and (not Assigned(oc) or (oc.Priority < oa.CurrentContext.Priority)) then
          oc := oa.CurrentContext;
      end;
    Atoms[i].StreamStructureTo(t, oc);
  end;
  SecurityElementCollection.StreamStructureTo(t);
end;

function TSecurityAtomCollection.GetAtomCount: Integer;
begin
  Result := FItems.Count;
end;

function TSecurityAtomCollection.GetAtoms(Index: Integer): TSecurityAtom;
begin
  Result := TSecurityAtom(FItems.Objects[Index]);
end;

procedure TSecurityAtomCollection.LoadContextsFromStream(
  const t: IisStream; const MaxRights: ISecurityCollectionRing = nil);
var
  i, j, k: Integer;
  o, o2: TSecurityAtom;
  oc: TSecurityContext;
  AtomType, Tag, CurrentContextTag: string;
  bFromGroup: Boolean;
  StreamVersion: Integer;
begin
  StreamVersion := t.ReadInteger;
  Assert(StreamVersion >= PreviousStreamVersion, 'Old stream version');
  j := t.ReadInteger;
  for i := 0 to j do
  begin
    AtomType := t.ReadString;
    Tag := t.ReadString;
    if StreamVersion > PreviousStreamVersion then
      bFromGroup := t.ReadBoolean
    else
      bFromGroup := False;
    CurrentContextTag := t.ReadString;
    o := FindAtom(Tag, AtomType);
    if Assigned(o) then
    begin
      oc := nil;
      if Assigned(MaxRights) then
        for k := 0 to MaxRights.CollectionCount - 1 do
        begin
          o2 := MaxRights.Collections(k).FindAtom(Tag, AtomType);
          if Assigned(o2) and (not Assigned(oc) or (oc.Priority < o2.CurrentContext.Priority)) then
            oc := o2.CurrentContext;
        end;
      if not Assigned(oc) or (o.GetContextByTag(CurrentContextTag).Priority <= oc.Priority) then
        oc := o.GetContextByTag(CurrentContextTag);
      if not Assigned(oc) then
        oc := o.GetContextByTag(ctDisabled);
      o.CurrentContext := oc;
      oc.MarkFromGroup(bFromGroup);
    end;
  end;
end;

constructor TSecurityAtomCollection.Create(
  const ReleaseCollectedInstances: Boolean);
begin
  inherited Create(ReleaseCollectedInstances);
  FSecurityElementCollection := TSecurityElementCollection.Create(ReleaseCollectedInstances);
end;

procedure TSecurityAtomCollection.SaveContextsToStream(const t: IisStream;
  const MaxRights: ISecurityCollectionRing = nil);
var
  i, k: Integer;
  a: TSecurityAtom;
  oc, oc2: TSecurityContext;
begin
  t.WriteInteger(CurrentStreamVersion);
  t.WriteInteger(AtomCount - 1);
  for i := 0 to AtomCount - 1 do
  begin
    t.WriteString(Atoms[i].AtomType);
    t.WriteString(Atoms[i].Tag);
    oc2 := Atoms[i].CurrentContext;
    oc := nil;
    if Assigned(MaxRights) then
      for k := 0 to MaxRights.CollectionCount - 1 do
      begin
        a := MaxRights.Collections(k).FindAtom(Atoms[i].Tag, Atoms[i].AtomType);
        if Assigned(a) and (not Assigned(oc) or (oc.Priority < a.CurrentContext.Priority)) then
        begin
          oc := Atoms[i].GetContextByTag(a.CurrentContext.Tag);
        end;
      end;
    if Assigned(oc)
    and (oc.Priority < oc2.Priority) then
      oc2 := oc;
    t.WriteBoolean(oc2.IsFromGroup);
    t.WriteString(oc2.Tag);
  end;
end;

procedure TSecurityAtomCollection.DeleteAtom(Atom: TSecurityAtom);
var
  i, j: Integer;
  E: TSecurityElement;
begin
  i := FItems.IndexOfObject(Atom);
  if i <> -1 then
  begin
    FItems.Delete(i);

    for i:= 0 to FSecurityElementCollection.GetElementCount - 1 do
    begin
      E := FSecurityElementCollection.GetElements(i);
      for j := E.FAtomLinks.Count - 1 downto 0 do
        if E.FAtomLinks.GetItems(j).FAtom = Atom then
          E.FAtomLinks.Delete(j);
    end;
  end;
end;

{ TElementStates }

constructor TElementStates.CreateFromStream(const t: IisStream);
var
  i: Integer;
begin
  i := t.ReadInteger;
  Assert(i >= 0);
  try
    SetLength(FStates, i);
  except
    On E: Exception do
      raise EevException.Create('***'+ E.Message+ ' ('+ IntToStr(i)+ ')');
  end;
  Assert(Low(FStates) = 0);
  for i := 0 to High(FStates) do
    t.ReadBuffer(FStates[i], SizeOf(TElementState));
end;

function TElementStates.GetCount: Integer;
begin
  Result := Length(FStates);
end;

function TElementStates.GetItems(Index: Integer): TElementState;
begin
  Assert((Index >= 0) and (Index < Count), 'Bad state index');
  Result := FStates[Index];
end;

function TElementStates.IndexOfState(const State: ShortString): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count-1 do
    if Items[i].Tag = State then
    begin
      Result := i;
      Break;
    end;
end;

procedure TElementStates.StreamStructureTo(const t: IisStream);
var
  i: Integer;
begin
  t.WriteInteger(Length(FStates));
  Assert(Low(FStates) = 0);
  for i := 0 to High(FStates) do
    t.WriteBuffer(FStates[i], SizeOf(TElementState));
end;

{ TElementToAtomLinks }

constructor TElementToAtomLinks.Create;
begin
  inherited;
  OwnsObjects := True;
end;

constructor TElementToAtomLinks.CreateFromStream(const t: IisStream; const Element: TSecurityElement);
var
  i, j: Integer;
begin
  inherited;
  OwnsObjects := True;
  i := t.ReadInteger;
  Capacity := i;
  for j := 0 to i-1 do
    Add(TElementToAtomLink.CreateFromStream(t, Element));
end;

function TElementToAtomLinks.GetItems(Index: Integer): TElementToAtomLink;
var
  o: TObject;
begin
  o := inherited Items[Index];
  Assert(o is TElementToAtomLink);
  Result := TElementToAtomLink(o);
end;

procedure TElementToAtomLinks.StreamStructureTo(const t: IisStream);
var
  i: Integer;
begin
  t.WriteInteger(Count);
  for i := 0 to Count-1 do
    Items[i].StreamStructureTo(t);
end;

{ TElementToAtomLink }

constructor TElementToAtomLink.CreateFromStream(const t: IisStream; const Element: TSecurityElement);
var
  i: Integer;
  sTag, sType: ShortString;
begin
  sTag := t.ReadString;
  sType := t.ReadString;
  FAtom := Element.Collection.SecurityAtomCollection.FindAtom(sTag, sType);

  Assert(Assigned(FAtom), 'Can''t find atom to link with');
  FAtom.LinkElement(Element);

  i := t.ReadInteger;
  Assert(i >= 0);
  try
    SetLength(FRelations, i);
  except
    On E: Exception do
      raise EevException.Create('***'+ E.Message+ ' ('+ IntToStr(i)+ ')');
  end;
  Assert(Low(Relations) = 0);
  for i := 0 to High(Relations) do
    t.ReadBuffer(Relations[i], SizeOf(TAtomElementRelation));
end;

procedure TElementToAtomLink.StreamStructureTo(const t: IisStream);
var
  i: Integer;
begin
  t.WriteString(Atom.Tag);
  t.WriteString(Atom.AtomType);
  Assert(Low(Relations) = 0);
  t.WriteInteger(High(Relations)+1);
  for i := 0 to High(Relations) do
    t.WriteBuffer(Relations[i], SizeOf(TAtomElementRelation));
end;

procedure TSecurityAtom.LinkElement(const Element: TSecurityElement);
begin
  if FElements.IndexOf(Element) = -1 then
    FElements.Add(Element);
end;

end.
