unit EvFieldValueSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Forms, isUIFashionPanel,
  ISBasicClasses, EvUIComponents, DB, Wwdatsrc, StdCtrls, LMDCustomButton,
  LMDButton, isUILMDButton, EvDataSet, EvConsts, EvContext, Grids, isBaseClasses,
  Wwdbigrd, Wwdbgrid, Classes, Controls, ExtCtrls, EvUtils, EvCommonInterfaces;

type
  TEvFieldValueSelect = class(TForm)
    pnlValues: TisUIFashionPanel;
    grValues: TevDBGrid;
    dsrcValues: TevDataSource;
    btnOk: TevBitBtn;
    btnCancel: TevBitBtn;
  private
    FData: IevDataSet;
    procedure Initialize(const ATable, AField: String; const ARecordNbr: Integer; const AsOfDate: TDateTime; const AAllowedValues: IisStringList);
  public
    class function SelectLookupValue(const AValueName: String; const ATable, AField: String; const ARecordNbr: Integer; var ASelectedNbr: Integer;
                                     const AsOfDate: TDateTime = 0; const AAllowedValues: IisStringList = nil): Boolean;
  end;

implementation

uses isDataSet;

{$R *.DFM}

class function TEvFieldValueSelect.SelectLookupValue(const AValueName: String; const ATable, AField: String; const ARecordNbr: Integer;
  var ASelectedNbr: Integer; const AsOfDate: TDateTime = 0; const AAllowedValues: IisStringList = nil): Boolean;
var
  Frm: TEvFieldValueSelect;
begin
  Frm := TEvFieldValueSelect.Create(Application);
  try
    Frm.Caption := AValueName + ' selection';
    Frm.pnlValues.Title := 'Please select a ' + AValueName;
    Frm.Initialize(ATable, AField, ARecordNbr, AsOfDate, AAllowedValues);

    if not Frm.FData.Locate('value', ASelectedNbr, []) then
      Frm.FData.First;

    Result := Frm.ShowModal = mrOk;
    if Result then
      ASelectedNbr := Frm.FData.FieldByName('value').AsInteger;
  finally
    Frm.Free;
  end;
end;

procedure TEvFieldValueSelect.Initialize(const ATable, AField: String; const ARecordNbr: Integer; const AsOfDate: TDateTime;
  const AAllowedValues: IisStringList);
var
  DS: IevDataSet;
  Fld: TField;
  i: Integer;
  Vals: IisListOfValues;
  Titles: IisStringList;
begin
  Vals := ctx_DBAccess.GetFieldValues(ATable, AField, ARecordNbr, AsOfDate, DayBeforeEndOfTime, True);
  DS := IInterface(Vals.Value['Data']) as IevDataSet;
  Titles := IInterface(Vals.Value['Titles']) as IisStringList;

  if Assigned(AAllowedValues) then
  begin
    Fld := DS.FieldByName('Value');
    DS.LogChanges := False;
    DS.First;
    AAllowedValues.Sorted := True;
    while not DS.Eof do
    begin
      if AAllowedValues.IndexOf(Fld.AsString) = -1 then
        DS.Delete
      else
        DS.Next;
    end;
  end;

  FData := DS;

  grValues.Selected.Clear;
  for i := 1 to FData.FieldCount - 1 do
    grValues.Selected.Add(FData.Fields[i].FieldName + #9 + IntToStr(FData.Fields[i].Size) + #9 + Titles[i] + #9'F');
  grValues.DefaultSort := FData.Fields[1].FieldName;

  dsrcValues.DataSet := FData.VCLDataset;
end;

end.
