object SchedFilterScreen: TSchedFilterScreen
  Left = 0
  Top = 0
  Width = 345
  Height = 114
  TabOrder = 0
  object Label2: TevLabel
    Left = 24
    Top = 24
    Width = 92
    Height = 13
    Caption = 'Filter Company Rep'
  end
  object Label3: TevLabel
    Left = 24
    Top = 64
    Width = 23
    Height = 13
    Caption = 'From'
  end
  object Label4: TevLabel
    Left = 120
    Top = 64
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object DateTimePicker1: TevDateTimePicker
    Left = 24
    Top = 80
    Width = 81
    Height = 21
    CalAlignment = dtaLeft
    Date = 36507.5696898148
    Time = 36507.5696898148
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 0
  end
  object DateTimePicker2: TevDateTimePicker
    Left = 120
    Top = 80
    Width = 81
    Height = 21
    CalAlignment = dtaLeft
    Date = 36507.5697940972
    Time = 36507.5697940972
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 1
  end
  object CheckBox1: TevCheckBox
    Left = 168
    Top = 40
    Width = 89
    Height = 17
    Caption = 'Filter by Date'
    TabOrder = 2
    OnClick = CheckBox1Click
  end
  object ComboBox2: TevComboBox
    Left = 24
    Top = 40
    Width = 129
    Height = 21
    ItemHeight = 13
    TabOrder = 3
  end
  object wwdsServiceBureau: TevDataSource
    Left = 304
    Top = 8
  end
end
