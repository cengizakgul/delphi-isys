object SCEDFields: TSCEDFields
  Left = 449
  Top = 195
  BorderStyle = bsDialog
  Caption = 'Choose fields to update'
  ClientHeight = 415
  ClientWidth = 316
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object clbFields: TevCheckListBox
    Left = 8
    Top = 120
    Width = 297
    Height = 249
    OnClickCheck = clbFieldsClickCheck
    ItemHeight = 13
    TabOrder = 0
  end
  object evBitBtn2: TevBitBtn
    Left = 142
    Top = 377
    Width = 75
    Height = 25
    Caption = 'Update'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    Color = clBlack
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
    ParentColor = False
    Margin = 0
  end
  object evBitBtn1: TevBitBtn
    Left = 229
    Top = 377
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Color = clBlack
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
    ParentColor = False
    Margin = 0
  end
  object evcbStartDate: TevCheckBox
    Left = 89
    Top = 2
    Width = 118
    Height = 17
    Caption = 'Ignore Filter Options'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = evcbStartDateClick
  end
  object evFilterOptions: TevGroupBox
    Left = 8
    Top = 23
    Width = 297
    Height = 90
    Caption = 'Filter Options '
    Enabled = False
    TabOrder = 4
    object evLabel1: TevLabel
      Left = 105
      Top = 45
      Width = 51
      Height = 13
      Caption = 'Start Date '
    end
    object evLabel3: TevLabel
      Left = 201
      Top = 45
      Width = 48
      Height = 13
      Caption = 'End Date '
    end
    object evrgStartDate: TevRadioButton
      Left = 8
      Top = 20
      Width = 89
      Height = 17
      Caption = 'Start Date'
      Checked = True
      Enabled = False
      TabOrder = 0
      TabStop = True
      OnClick = evrgStartDateClick
    end
    object evrgDateRange: TevRadioButton
      Left = 8
      Top = 65
      Width = 89
      Height = 17
      Caption = 'Date Range'
      Enabled = False
      TabOrder = 1
      OnClick = evrgStartDateClick
    end
    object evStartDate: TevDateTimePicker
      Left = 105
      Top = 16
      Width = 89
      Height = 21
      Date = 41331.000000000000000000
      Time = 41331.000000000000000000
      Enabled = False
      TabOrder = 2
    end
    object evdrStartDate: TevDateTimePicker
      Left = 105
      Top = 61
      Width = 89
      Height = 21
      Date = 41331.000000000000000000
      Time = 41331.000000000000000000
      Enabled = False
      TabOrder = 3
    end
    object evdrEndDate: TevDateTimePicker
      Left = 201
      Top = 61
      Width = 89
      Height = 21
      Date = 41331.000000000000000000
      Time = 41331.000000000000000000
      Enabled = False
      TabOrder = 4
    end
  end
end
