object evBlobViewer: TevBlobViewer
  Left = 0
  Top = 0
  Width = 434
  Height = 362
  TabOrder = 0
  object imgBlob: TevImage
    Left = 0
    Top = 0
    Width = 434
    Height = 362
    Align = alClient
    Center = True
    Proportional = True
    Stretch = True
    Visible = False
  end
  object memBlob: TevMemo
    Left = 0
    Top = 0
    Width = 434
    Height = 362
    Align = alClient
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    Visible = False
    WordWrap = False
    SpellcheckEnabled = False
  end
end
