// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SReturnQueueEnlistProcedures;

interface

uses
  Variants, EvBasicUtils, Classes, EvExceptions, EvDataAccessComponents, EvClientDataSet, isBaseClasses;

procedure EnlistCompany(const CoNbrList: array of Integer; const PeriodEndDate: TDateTime;
                           const ClCoConsolidationNbr: Variant; ConsolidationType: string = ''; const Scope:string='');
function CalculateReturnDueDate(const GlAgencyNbr: Integer; const ReturnFrequency, WhenDueType: string;
                                const PeriodEndDate: TDateTime;const NumberOfDaysForWhenDue: Variant): TDateTime;

Const
  FED_REPORT_ALL = 'A';
  FED_REPORT_EXCEPT_940 = 'D';
  FED_REPORT_940 = 'F';

implementation

uses Dialogs, EvUtils, SDataStructure, EvConsts, SFieldCodeValues,  SysUtils, Db, Controls, Forms, Windows,
     EvTypes, EvContext;

function CalculateReturnDueDate(const GlAgencyNbr: Integer; const ReturnFrequency, WhenDueType: string;
                                const PeriodEndDate: TDateTime;const NumberOfDaysForWhenDue: Variant): TDateTime;
var
  RealPeriodEndDate: TDateTime;
begin
  case ReturnFrequency[1] of
  FREQUENCY_TYPE_SEMI_MONTHLY:
    if GetDay(PeriodEndDate) < 16 then
      RealPeriodEndDate := GetBeginMonth(PeriodEndDate)+ 14 // the 15th
    else
      RealPeriodEndDate := GetEndMonth(PeriodEndDate);
  FREQUENCY_TYPE_MONTHLY:
      RealPeriodEndDate := GetEndMonth(PeriodEndDate);
  FREQUENCY_TYPE_QUARTERLY:
      RealPeriodEndDate := GetEndQuarter(PeriodEndDate);
  FREQUENCY_TYPE_ANNUAL:
      RealPeriodEndDate := GetEndYear(PeriodEndDate);
  else
    RealPeriodEndDate := 0; // to correct hint message
    Assert(False);
  end;
  case WhenDueType[1] of
  WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT:
    begin
      if VarIsNull(NumberOfDaysForWhenDue) then
        raise EInconsistentData.CreateHelp('Global agency report needs the numbers of days to be set up', IDH_InconsistentData);

      Result := RealPeriodEndDate + NumberOfDaysForWhenDue;
    end;
  WHEN_DUE_LAST_DAY_OF_NEXT_MONTH:
      Result := GetEndMonth(RealPeriodEndDate+ 1);

  WHEN_DUE_SEMI_MONTHLY_15TH_EOM:
      if GetDay(RealPeriodEndDate)>= 16 then
         Result := GetBeginMonth(GetEndMonth(RealPeriodEndDate)+ 1)+ 14   // the 15th of next month
       else
         Result := GetEndMonth(RealPeriodEndDate);

  WHEN_DUE_LAST_DAY_OF_SECOND_MONTH:
      Result := GetEndMonth(GetEndMonth(GetEndMonth(RealPeriodEndDate) + 1) + 1);

  else
    Result := 0; // to correct hint message
    Assert(False);
  end;

  if Result <> 0 then
    Result := ctx_PayrollCalculation.CheckDueDateForHoliday(Result, GlAgencyNbr);

end;

procedure EnlistCompany(const CoNbrList: array of Integer; const PeriodEndDate: TDateTime;
  const ClCoConsolidationNbr: Variant;ConsolidationType: string = ''; const Scope: string = '');

var
  cdSY_GL_AGENCY_REPORT, cdCOEnlistGroup, cdCOAutoEnlistReturns, cdCODontAutoEnlistReturns, cdSYReportMember, SyAgencyNbr: TevClientDataset;
  cdTCDAgency : TevClientDataset;
  PeriodType: set of Char;
  FMinBegDate, FMaxEndDate: TDateTime;
  procedure AddSyAgency(const Nbr, ActiveControl : Variant; const HasLiabs: Boolean; const Freq: string; DepMethod: string; FedReport: string);
  begin
    if not VarIsNull(Nbr) then
    begin
      if DepMethod = '' then
        DepMethod := '-'
      else
        case DepMethod[1] of
        '-': ;
        TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_DEBIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
          DepMethod := AG_PAYMENT_METHOD_EFT;
        TAX_PAYMENT_METHOD_CHECK, TAX_PAYMENT_METHOD_NOTICES,
        TAX_PAYMENT_METHOD_NOTICES_CHECKS:
          DepMethod := AG_PAYMENT_METHOD_CHECK;
        else
          raise EInconsistentData.CreateHelp('Unknown Deposit Method : "'+ DepMethod[1]+ '"', IDH_InconsistentData);
        end;
      if not SyAgencyNbr.Locate('Nbr;ActiveControl', VarArrayOf([Nbr,ActiveControl]), []) then
        SyAgencyNbr.AppendRecord([Nbr, ActiveControl, HasLiabs, Freq, DepMethod, FedReport])
      else
        if HasLiabs and not SyAgencyNbr['HasLiabs'] then
        begin
          SyAgencyNbr.Edit;
          SyAgencyNbr['HasLiabs'] := HasLiabs;
          SyAgencyNbr.Post;
        end;
    end;
  end;
  function CheckIfHasLiabs(const ds: TevClientDataset; const f: string): Boolean;
  begin
    ds.Filter := f;
    ds.Filtered := True;
    Result := ds.RecordCount > 0;
    ds.Filtered := False;
    ds.Filter := '';
  end;
  procedure ShowErrorMessage(const s: string);
  var m: string;
  begin
    with DM_COMPANY do
      m := 'Company '+ CO['NAME']+ ' has shown a problem with '#13+ s+ #13'Fix it and try to enlist this company again';
    raise EInconsistentData.CreateHelp(m, IDH_InconsistentData);
  end;
  function Check945Presented: Boolean;
  var
    CoList: string;
    i: Integer;
  begin
    CoList := IntToStr(CoNbrList[0]);
    for i := 1 to High(CoNbrList) do
      CoList := CoList+ ','+ IntToStr(CoNbrList[i]);
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
      begin
        SetMacro('COLUMNS', 'count(*) c');
        SetMacro('TABLE1', 'PR_CHECK');
        SetMacro('TABLE2', 'PR');
        SetMacro('JOINFIELD', 'PR');
        SetMacro('CONDITION', 'T2.CHECK_DATE BETWEEN :BEGDATE AND :ENDDATE and T2.CO_NBR IN ('+ CoList+ ') '+
          'AND T1.CHECK_TYPE_945='''+ CHECK_TYPE_945_945+ '''');
        SetParam('BEGDATE', FMinBegDate);
        SetParam('ENDDATE', FMaxEndDate);
        DataRequest(AsVariant);
      end;
      Open;
      Assert(RecordCount = 1);
      Result := FieldValues['c'] > 0;
    end;
  end;

  function CheckCoDontEnlistReturns:Boolean;
  begin
     result := True;
     if cdCODontAutoEnlistReturns.Active and (cdCODontAutoEnlistReturns.RecordCount > 0) and
        cdCODontAutoEnlistReturns.Locate('SY_GL_AGENCY_REPORT_NBR', cdSY_GL_AGENCY_REPORT['SY_GL_AGENCY_REPORT_NBR'], []) then
        result := false;
  end;

  function CheckCoEnlistReturns(var ActiveControl : Integer) :Boolean;
  begin
     result := False;
     if cdCOAutoEnlistReturns.FieldByName('PROCESS_TYPE').AsString[1] in  [PROCESS_TYPE_ENLIST, PROCESS_TYPE_PA_ENLIST] then
     begin
       Result := True;
       if SyAgencyNbr.Locate('NBR;ActiveControl', VarArrayOf([cdSY_GL_AGENCY_REPORT['SY_GLOBAL_AGENCY_NBR'],0]), []) then
         ActiveControl := 0;
     end;
  end;

  function CheckGroupMember:Boolean;
  var
    bGroupMember : Boolean;

    Function CheckMediaType:Boolean;
    begin
     Result :=False;
     if (cdCOEnlistGroup['PROCESS_TYPE'] = PROCESS_TYPE_ENLIST) and
         (
          ((cdCOEnlistGroup['SY_REPORT_GROUPS_NBR'] = TAX_RETURN_GROUP_W2) and
           (cdSY_GL_AGENCY_REPORT['MEDIA_TYPE'] = MEDIA_TYPE_PLAIN_LETTER))
          or
          ((cdCOEnlistGroup['SY_REPORT_GROUPS_NBR'] = TAX_RETURN_GROUP_1099M) and
           (cdSY_GL_AGENCY_REPORT['MEDIA_TYPE'] = MEDIA_TYPE_PLAIN_LETTER))
          or
          ((cdCOEnlistGroup['SY_REPORT_GROUPS_NBR'] = TAX_RETURN_GROUP_1095B) and
           (cdSY_GL_AGENCY_REPORT['MEDIA_TYPE'] = MEDIA_TYPE_PLAIN_LETTER))
          or
          ((cdCOEnlistGroup['SY_REPORT_GROUPS_NBR'] = TAX_RETURN_GROUP_1095C) and
           (cdSY_GL_AGENCY_REPORT['MEDIA_TYPE'] = MEDIA_TYPE_PLAIN_LETTER))
          or
          (cdCOEnlistGroup['MEDIA_TYPE'] = cdSY_GL_AGENCY_REPORT['MEDIA_TYPE'])
          or
          (cdCOEnlistGroup.FieldByName('MEDIA_TYPE').isnull)
        ) then
           result := True;
    end;
  begin
     result := False;
     bGroupMember := False;

     cdCOEnlistGroup.First;
     while not cdCOEnlistGroup.Eof do
     begin
       if cdSYReportMember.FindKey([cdSY_GL_AGENCY_REPORT['SY_REPORT_WRITER_REPORTS_NBR'],
                                     cdCOEnlistGroup['SY_REPORT_GROUPS_NBR']]) then
       begin
         bGroupMember := True;
         result := CheckMediaType;
         if Result then break;
       end;
       cdCOEnlistGroup.Next;
     end;

     if not bGroupMember then
     begin
       if cdCOEnlistGroup.FindKey([cdSY_GL_AGENCY_REPORT['Tax_Return_Group']]) then
         result := CheckMediaType
       else
         result := True;
     end;
  end;
var
  CoNbr: Integer;
  FBegDate, FEndDate: TDateTime;
  d: TDateTime;
  i, j, ActiveControl : integer;
  s, sFedReport, fFedReport: string;
  FilterString, PaFilter: string;

  SyLocalNbrList: IisStringList;

  bInsert, PALocalControl: Boolean;

  lActiveSuis: TStringList;
begin
  FMinBegDate := 99999;
  FMaxEndDate := 0;
  try
    PeriodType := [];
    if GetEndYear(PeriodEndDate)=PeriodEndDate then
      PeriodType := PeriodType+ [FREQUENCY_TYPE_ANNUAL];
    if GetEndQuarter(PeriodEndDate)=PeriodEndDate then
      PeriodType := PeriodType+ [FREQUENCY_TYPE_QUARTERLY];
    if GetEndMonth(PeriodEndDate)=PeriodEndDate then
      PeriodType := PeriodType+ [FREQUENCY_TYPE_MONTHLY, FREQUENCY_TYPE_SEMI_MONTHLY];
    if GetDay(PeriodEndDate) = 15 then
      PeriodType := PeriodType+ [FREQUENCY_TYPE_SEMI_MONTHLY];
    DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_REPORT_GROUPS.DataRequired('NAME like ''Tax Return-%''');

    SyAgencyNbr := TevClientDataSet.Create(nil);
    lActiveSuis := TStringList.Create;
    cdSY_GL_AGENCY_REPORT := TevClientDataSet.Create(nil);
    cdCOEnlistGroup := TevClientDataSet.Create(nil);
    cdCOAutoEnlistReturns := TevClientDataSet.Create(nil);
    cdSYReportMember := TevClientDataSet.Create(nil);
    cdTCDAgency := TevClientDataSet.Create(nil);
    cdCODontAutoEnlistReturns := TevClientDataSet.Create(nil);

    SyLocalNbrList := TisStringList.Create;
    SyLocalNbrList.Clear;
    try
      lActiveSuis.Sorted := True;
      SyAgencyNbr.FieldDefs.Add('Nbr', ftInteger, 0, True);
      SyAgencyNbr.FieldDefs.Add('ActiveControl', ftInteger, 0, True);
      SyAgencyNbr.FieldDefs.Add('HasLiabs', ftBoolean, 0, True);
      SyAgencyNbr.FieldDefs.Add('Freq', ftString, 1, True);
      SyAgencyNbr.FieldDefs.Add('DepMethod', ftString, 1, True);
      SyAgencyNbr.FieldDefs.Add('FedReport', ftString, 1, True);
      SyAgencyNbr.CreateDataSet;
      SyAgencyNbr.LogChanges := False;

      cdTCDAgency.FieldDefs.Add('AgencyNbr', ftInteger, 0, True);
      cdTCDAgency.FieldDefs.Add('Freq', ftString, 1, True);
      cdTCDAgency.FieldDefs.Add('DepMethod', ftString, 1, True);
      cdTCDAgency.FieldDefs.Add('State', ftString, 2, True);
      cdTCDAgency.CreateDataSet;

      s := '-1';
      for j := Low(CoNbrList) to High(CoNbrList) do
      begin
        CoNbr := CoNbrList[j];
        s := s+ ','+ IntToStr(CoNbr);
        if DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Locate('AGENCY_NAME', 'SYSTEM USE ONLY', [loCaseInsensitive]) then
          AddSyAgency(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY['SY_GLOBAL_AGENCY_NBR'], 1, False, FREQUENCY_TYPE_DONT_CARE, '-',FED_REPORT_ALL);
        DM_COMPANY.CO.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
        Assert(DM_COMPANY.CO.RecordCount = 1);
        DM_COMPANY.CO_SUI.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
        DM_COMPANY.CO_SUI.First;
        while not DM_COMPANY.CO_SUI.Eof do
        begin
          if (ConvertNull(DM_COMPANY.CO_SUI['FINAL_TAX_RETURN'], GROUP_BOX_NO) = GROUP_BOX_NO)
          and (DM_SYSTEM_STATE.SY_SUI.ShadowDataSet.CachedLookup(DM_COMPANY.CO_SUI['SY_SUI_NBR'], 'EE_ER_OR_EE_OTHER_OR_ER_OTHER') = GROUP_BOX_ER) then
            if lActiveSuis.IndexOf(IntToStr(DM_COMPANY.CO_SUI['SY_SUI_NBR'])) = -1 then
              lActiveSuis.Append(IntToStr(DM_COMPANY.CO_SUI['SY_SUI_NBR']));
          DM_COMPANY.CO_SUI.Next;
        end;
        FBegDate := 0;
        FEndDate := 0;
        for i := 1 to 4 do
        begin
          case i of
          1:
            begin
              if not (FREQUENCY_TYPE_ANNUAL in PeriodType) then
                Continue;
              FBegDate := GetBeginYear(PeriodEndDate);
              FEndDate := GetEndYear(PeriodEndDate);
            end;
          2:
            begin
              if not (FREQUENCY_TYPE_QUARTERLY in PeriodType) then
                Continue;
              FBegDate := GetBeginQuarter(PeriodEndDate);
              FEndDate := GetEndQuarter(PeriodEndDate);
            end;
          3:
            begin
              if not (FREQUENCY_TYPE_MONTHLY in PeriodType) then
                Continue;
              FBegDate := GetBeginMonth(PeriodEndDate);
              FEndDate := GetEndMonth(PeriodEndDate);
            end;
          4:
            begin
              if not (FREQUENCY_TYPE_SEMI_MONTHLY in PeriodType) then
                Continue;
              if GetDay(PeriodEndDate) <= 15 then
              begin
                FBegDate := GetBeginMonth(PeriodEndDate);
                FEndDate := FBegDate+ 14; // the 15th
              end
              else
              begin
                FBegDate := GetBeginMonth(PeriodEndDate)+ 15; // the 16th
                FEndDate := GetEndMonth(PeriodEndDate);
              end;
            end;
          else
            Assert(False);
          end;
          if FBegDate < FMinBegDate then
            FMinBegDate := FBegDate;
          if FEndDate > FMaxEndDate then
            FMaxEndDate := FEndDate;
          FilterString := 'CO_NBR='+ IntToStr(CoNbr)+ ' and CHECK_DATE between '''+ DateToStr(FBegDate)+ ''' and '''+ DateToStr(FEndDate)+ ''' and THIRD_PARTY = ''N''';
          DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired(FilterString);
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired(FilterString);
          DM_COMPANY.CO_SUI_LIABILITIES.DataRequired(FilterString);
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired(FilterString);

          sFedReport := FED_REPORT_EXCEPT_940;
          fFedReport := FED_REPORT_940;
          if DM_COMPANY.CO['SY_FED_REPORTING_AGENCY_NBR'] = DM_COMPANY.CO['FUI_SY_TAX_REPORT_AGENCY'] then
          begin
           sFedReport := FED_REPORT_ALL;
           fFedReport := FED_REPORT_ALL;
          end;
          AddSyAgency(DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY.Lookup('SY_FED_REPORTING_AGENCY_NBR', DM_COMPANY.CO['SY_FED_REPORTING_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'), 1,
            CheckIfHasLiabs(DM_COMPANY.CO_FED_TAX_LIABILITIES, 'TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_ER_FUI+ ''''),
            DM_COMPANY.CO['FEDERAL_TAX_DEPOSIT_FREQUENCY'], DM_COMPANY.CO['FEDERAL_TAX_PAYMENT_METHOD'], sFedReport);
          AddSyAgency(DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY.Lookup('SY_FED_REPORTING_AGENCY_NBR', DM_COMPANY.CO['FUI_SY_TAX_REPORT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR'), 1,
            CheckIfHasLiabs(DM_COMPANY.CO_FED_TAX_LIABILITIES, 'TAX_TYPE = '''+ TAX_LIABILITY_TYPE_ER_FUI+ ''''),
            DM_COMPANY.CO['FUI_TAX_DEPOSIT_FREQUENCY'], DM_COMPANY.CO['FEDERAL_TAX_PAYMENT_METHOD'], fFedReport);

          DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+ IntToStr(CoNbr));
          DM_COMPANY.CO_STATES.First;
          while not DM_COMPANY.CO_STATES.Eof do
          begin
            if not (DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES['STATE'], [])) then
                ShowErrorMessage(ConvertNull(DM_COMPANY.CO_STATES['STATE'], 'N/A')+ ' state setup');
            if not DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Locate('SY_STATE_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'], []) then
                ShowErrorMessage(DM_COMPANY.CO_STATES['STATE']+ ' state dep. frequency setup');


            if (not DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull) and
               (DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger, [])) and
                DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR',DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.FieldByName('SY_GLOBAL_AGENCY_NBR').Value, []) then
              if DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('AGENCY_TYPE').AsString = GROUP_BOX_YES then
                  cdTCDAgency.AppendRecord([
                       DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.FieldByName('SY_GLOBAL_AGENCY_NBR').Value,
                        DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').Value,
                         DM_COMPANY.CO_STATES.FieldByName('TCD_PAYMENT_METHOD').Value,
                          DM_COMPANY.CO_STATES.FieldByName('state').Value]);

            AddSyAgency(DM_SYSTEM_STATE.SY_STATES['SY_STATE_REPORTING_AGENCY_NBR'], 0,
                CheckIfHasLiabs(DM_COMPANY.CO_STATE_TAX_LIABILITIES, 'CO_STATES_NBR='+ IntToStr(DM_COMPANY.CO_STATES['CO_STATES_NBR'])),
                DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ['FREQUENCY_TYPE'], DM_COMPANY.CO_STATES['STATE_TAX_DEPOSIT_METHOD'],FED_REPORT_ALL);

            if ConvertNull(DM_COMPANY.CO_STATES['STATE_NON_PROFIT'], GROUP_BOX_NO) = GROUP_BOX_NO then
            begin
              AddSyAgency(DM_SYSTEM_STATE.SY_STATES['SY_STATE_REPORTING_AGENCY_NBR'], 1,
                CheckIfHasLiabs(DM_COMPANY.CO_STATE_TAX_LIABILITIES, 'CO_STATES_NBR='+ IntToStr(DM_COMPANY.CO_STATES['CO_STATES_NBR'])),
                DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ['FREQUENCY_TYPE'], DM_COMPANY.CO_STATES['STATE_TAX_DEPOSIT_METHOD'],FED_REPORT_ALL);
            end;
            DM_COMPANY.CO_STATES.Next;
          end;
          DM_COMPANY.CO_SUI.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
          DM_COMPANY.CO_SUI.First;
          while not DM_COMPANY.CO_SUI.Eof do
          begin
            if not (DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', DM_COMPANY.CO_SUI['SY_SUI_NBR'], [])) then
                ShowErrorMessage(ConvertNull(DM_COMPANY.CO_SUI['DESCRIPTION'], 'N/A')+ ' SUI setup');

            AddSyAgency(DM_SYSTEM_STATE.SY_SUI['SY_SUI_REPORTING_AGENCY_NBR'], 0,
                CheckIfHasLiabs(DM_COMPANY.CO_SUI_LIABILITIES, 'CO_SUI_NBR='+ IntToStr(DM_COMPANY.CO_SUI['CO_SUI_NBR'])),
                FREQUENCY_TYPE_QUARTERLY, DM_COMPANY.CO_STATES['SUI_TAX_DEPOSIT_METHOD'],FED_REPORT_ALL);
            if ConvertNull(DM_COMPANY.CO_SUI['FINAL_TAX_RETURN'], GROUP_BOX_NO) = GROUP_BOX_NO then
            begin
              AddSyAgency(DM_SYSTEM_STATE.SY_SUI['SY_SUI_REPORTING_AGENCY_NBR'], 1,
                CheckIfHasLiabs(DM_COMPANY.CO_SUI_LIABILITIES, 'CO_SUI_NBR='+ IntToStr(DM_COMPANY.CO_SUI['CO_SUI_NBR'])),
                FREQUENCY_TYPE_QUARTERLY, DM_COMPANY.CO_STATES['SUI_TAX_DEPOSIT_METHOD'],FED_REPORT_ALL);
            end;
            DM_COMPANY.CO_SUI.Next;
          end;

          DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
          DM_COMPANY.CO_LOCAL_TAX.First;
          while not DM_COMPANY.CO_LOCAL_TAX.Eof do
          begin
            if  not (DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], []))
             or not (DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.Locate('SY_LOCAL_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR'], [])) then
                if DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_STATES_NBR'], []) then
                  ShowErrorMessage(DM_SYSTEM_STATE.SY_STATES['STATE']+ ' state local(s) setup (nbr#'+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])+ ')')
                else
                  ShowErrorMessage('local setup (nbr#'+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])+ ')');

            SyLocalNbrList.Add(DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsString);

            if (DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_STATES_NBR'], [])) and
                (cdTCDAgency.Locate('State',DM_SYSTEM_STATE.SY_STATES.fieldByName('State').Value,[])) and
                (DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('LOCAL_TYPE').AsString[1] in [LOCAL_TYPE_INC_RESIDENTIAL, LOCAL_TYPE_INC_NON_RESIDENTIAL, LOCAL_TYPE_SCHOOL]) then
            begin
               AddSyAgency(cdTCDAgency['AgencyNbr'], 0,
                  CheckIfHasLiabs(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 'paymentCO_LOCAL_TAX_NBR='+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])),
                  cdTCDAgency['Freq'], cdTCDAgency['DepMethod'],FED_REPORT_ALL);

               if ConvertNull(DM_COMPANY.CO_LOCAL_TAX['LOCAL_ACTIVE'], GROUP_BOX_YES) = GROUP_BOX_YES then
               begin
                 AddSyAgency(cdTCDAgency['AgencyNbr'], 1,
                    CheckIfHasLiabs(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 'paymentCO_LOCAL_TAX_NBR='+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])),
                    cdTCDAgency['Freq'], cdTCDAgency['DepMethod'],FED_REPORT_ALL);
               end;
            end
            else
            begin

              PALocalControl := true;                      //  Special logic to exclude  PA local residential tax returns
              if (DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('SY_STATES_NBR').AsInteger = 41) and
                 (DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('LOCAL_TYPE').AsString = LOCAL_TYPE_INC_RESIDENTIAL) then
                  PALocalControl := false;

               AddSyAgency(DM_SYSTEM_LOCAL.SY_LOCALS['SY_LOCAL_REPORTING_AGENCY_NBR'], 0,
                    CheckIfHasLiabs(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 'paymentCO_LOCAL_TAX_NBR='+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])),
                     DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ['FREQUENCY_TYPE'], DM_COMPANY.CO_LOCAL_TAX['PAYMENT_METHOD'],FED_REPORT_ALL);

               if ConvertNull(DM_COMPANY.CO_LOCAL_TAX['LOCAL_ACTIVE'], GROUP_BOX_YES) = GROUP_BOX_YES then
               begin
                if PALocalControl then
                  AddSyAgency(DM_SYSTEM_LOCAL.SY_LOCALS['SY_LOCAL_REPORTING_AGENCY_NBR'], 1,
                     CheckIfHasLiabs(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 'paymentCO_LOCAL_TAX_NBR='+ IntToStr(DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'])),
                     DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ['FREQUENCY_TYPE'], DM_COMPANY.CO_LOCAL_TAX['PAYMENT_METHOD'],FED_REPORT_ALL);
               end;
            end;
            DM_COMPANY.CO_LOCAL_TAX.Next;
          end;

        end;
      end;

      CoNbr := CoNbrList[0];
      DM_COMPANY.CO.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR = '+ IntToStr(CoNbr));
      DM_COMPANY.CO_WORKERS_COMP.DataRequired('CO_NBR = '+ IntToStr(CoNbr));

      DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('ALL');

      if VarIsNull(ClCoConsolidationNbr) then
        DM_COMPANY.CO_TAX_RETURN_QUEUE.Filter :=
         'CO_NBR = '+ IntToStr(CoNbr) + ' and CL_CO_CONSOLIDATION_NBR IS NULL and SY_GL_AGENCY_REPORT_NBR IS NULL and PERIOD_END_DATE = ''' +
             DateToStr(PeriodEndDate) + ''' and STATUS = ' + QuotedStr(TAX_RETURN_STATUS_UNPROCESSED) + ''
      else
        DM_COMPANY.CO_TAX_RETURN_QUEUE.Filter :=
         'CO_NBR = '+ IntToStr(CoNbr) + 'and CL_CO_CONSOLIDATION_NBR = ' + intToStr(ClCoConsolidationNbr) +
          'and SY_GL_AGENCY_REPORT_NBR IS NULL and PERIOD_END_DATE = ''' +
            DateToStr(PeriodEndDate) + ''' and STATUS = ' + QuotedStr(TAX_RETURN_STATUS_UNPROCESSED) + '';
      DM_COMPANY.CO_TAX_RETURN_QUEUE.Filtered := true;

      if DM_COMPANY.CO_TAX_RETURN_QUEUE.RecordCount > 0 then
      begin
        ctx_DataAccess.StartNestedTransaction([dbtClient]);
        try
          while DM_COMPANY.CO_TAX_RETURN_QUEUE.RecordCount > 0 do
            DM_COMPANY.CO_TAX_RETURN_QUEUE.Delete;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
          ctx_DataAccess.CommitNestedTransaction;
        except
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      end;
      DM_COMPANY.CO_TAX_RETURN_QUEUE.Filter :='';
      DM_COMPANY.CO_TAX_RETURN_QUEUE.Filtered := False;


      s :=
       'select t1.SY_GL_AGENCY_REPORT_NBR, t1.DEPOSIT_FREQUENCY, t1.ENLIST_AUTOMATICALLY, ' +
       't1.PAYMENT_METHOD, t1.TAX945_CHECKS , t1.TAX943_COMPANY , t1.TAX944_COMPANY , t1.TAXRETURN940, ' +
       't5.SY_GLOBAL_AGENCY_NBR, ' +
       't1.WHEN_DUE_TYPE, ' +
       't1.SY_GL_AGENCY_FIELD_OFFICE_NBR, t1.RETURN_FREQUENCY, ' +
       't1.PRINT_WHEN, t1.NUMBER_OF_DAYS_FOR_WHEN_DUE, t1.CONSOLIDATED_FILTER, ' +
       't1.SYSTEM_TAX_TYPE, t1.TAX_SERVICE_FILTER, t1.TAX_RETURN_ACTIVE, ' +
       't4.NAME, t4.MEDIA_TYPE VMEDIA_TYPE, t3.MEDIA_TYPE MEDIA_TYPE, t2.ACTIVE_YEAR_FROM, ' +
       't2.ACTIVE_YEAR_TO, t2.ACTIVE_REPORT, t2.SY_REPORTS_NBR, ' +
      't3.SY_REPORT_WRITER_REPORTS_NBR, t3.REPORT_TYPE, t2.SY_REPORTS_GROUP_NBR, ' +

      'CASE t1.RETURN_FREQUENCY ' +
        'WHEN ''A'' THEN 19 ' +
        'WHEN ''Q'' THEN 20 ' +
        'WHEN ''M'' THEN 21 ' +
        'WHEN ''S'' THEN 22 ' +
        'ELSE 20 ' +
      'END Tax_Return_Group ' +

      'from SY_GL_AGENCY_REPORT t1, SY_REPORT_WRITER_REPORTS t3, ' +
            'SY_REPORTS t2, SY_REPORTS_GROUP t4, ' +
            'SY_GL_AGENCY_FIELD_OFFICE t5 ' +
       'where ' +
         '(t1.TAX_RETURN_ACTIVE = ''Y'' or ' +
           '(t1.TAX_RETURN_ACTIVE = ''N'' and ' +
           't1.TAX_RETURN_INACTIVE_START_DATE is not null and ' +
            ':pENDDATE <= t1.TAX_RETURN_INACTIVE_START_DATE)) and ' +

          '(t1.BEGIN_EFFECTIVE_MONTH is null or t1.END_EFFECTIVE_MONTH is null or ' +
           't1.BEGIN_EFFECTIVE_MONTH = 0 or t1.END_EFFECTIVE_MONTH = 0 or ' +
            '(t1.BEGIN_EFFECTIVE_MONTH <= EXTRACTMONTH(:pENDDATE) and ' +
            'EXTRACTMONTH(:pENDDATE) <= t1.END_EFFECTIVE_MONTH)) and ' +

         't1.SY_GL_AGENCY_FIELD_OFFICE_NBR = t5.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
         't1.SY_REPORTS_GROUP_NBR = t2.SY_REPORTS_GROUP_NBR and ' +
         't1.SY_REPORTS_GROUP_NBR = t4.SY_REPORTS_GROUP_NBR and ' +
         't3.SY_REPORT_WRITER_REPORTS_NBR = t2.SY_REPORT_WRITER_REPORTS_NBR and ' +

         '{AsOfNow<t2>} and {AsOfNow<t3>} and ' +
         '{AsOfNow<t1>} and {AsOfNow<t4>} and ' +
         '{AsOfNow<t5>} and ' +

         '( ' +
          '(t2.ACTIVE_YEAR_FROM is null and t2.ACTIVE_YEAR_TO is null) ' +
           'or ' +
            '(t2.ACTIVE_YEAR_FROM<= :pENDDATE and :pENDDATE <= t2.ACTIVE_YEAR_TO ) ' +
           'or ' +
            '(t2.ACTIVE_YEAR_FROM <= :pENDDATE and t2.ACTIVE_YEAR_TO is null) ' +
          ') ';

      ctx_DataAccess.SY_CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create(s) do
      begin
        d := Trunc(PeriodEndDate);
        SetParam('pENDDATE', d);
        ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      cdSY_GL_AGENCY_REPORT.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      Assert(cdSY_GL_AGENCY_REPORT.FindField('SY_GL_AGENCY_REPORT_NBR') <> nil);
      cdSY_GL_AGENCY_REPORT.AddIndex('NBR', 'SY_GL_AGENCY_REPORT_NBR', []);
      cdSY_GL_AGENCY_REPORT.IndexDefs.Update;
      cdSY_GL_AGENCY_REPORT.IndexName := 'NBR';

      s :='select a.SY_REPORT_GROUPS_NBR, a.PROCESS_TYPE, a.MEDIA_TYPE from ' +
          'CO_ENLIST_GROUPS a ' +
          'where {AsOfNow<a>} and a.CO_NBR =:pCoNbr ';

      if not cdCOEnlistGroup.Active then
      begin
          with TExecDSWrapper.Create(s) do
          begin
            SetParam('pCoNbr', CoNbr);
            ctx_DataAccess.GetCustomData(cdCOEnlistGroup, AsVariant);
          end;
          Assert(cdCOEnlistGroup.FindField('SY_REPORT_GROUPS_NBR') <> nil);
          cdCOEnlistGroup.AddIndex('NBR', 'SY_REPORT_GROUPS_NBR', []);
          cdCOEnlistGroup.IndexDefs.Update;
          cdCOEnlistGroup.IndexName := 'NBR';
      end;

      s :='select a.SY_GL_AGENCY_REPORT_NBR, a.PROCESS_TYPE from ' +
           'CO_AUTO_ENLIST_RETURNS a ' +
           'where {AsOfNow<a>} and a.CO_NBR =:pCoNbr ';

      if not cdCOAutoEnlistReturns.Active then
      begin
          with TExecDSWrapper.Create(s) do
          begin
            SetParam('pCoNbr', CoNbr);
            ctx_DataAccess.GetCustomData(cdCOAutoEnlistReturns, AsVariant);
          end;
          Assert(cdCOAutoEnlistReturns.FindField('SY_GL_AGENCY_REPORT_NBR') <> nil);
          cdCOAutoEnlistReturns.AddIndex('NBR', 'SY_GL_AGENCY_REPORT_NBR', []);
          cdCOAutoEnlistReturns.IndexDefs.Update;
          cdCOAutoEnlistReturns.IndexName := 'NBR';
      end;

      if (cdCOAutoEnlistReturns.RecordCount > 0) and  (SyLocalNbrList.Count > 0) then
      begin
        PaFilter := '';

        if cdTCDAgency.Locate('State','PA',[]) then
          PaFilter :='((a.COMBINE_FOR_TAX_PAYMENTS = ''Y'' and b.SY_GLOBAL_AGENCY_NBR = ' + cdTCDAgency.fieldByname('AgencyNbr').AsString + ') ';

        if PaFilter <> '' then
           PaFilter := PaFilter +' or '
        else
           PaFilter := ' ( ';

        PaFilter := PaFilter + BuildINsqlStatement('a.SY_LOCALS_NBR', SyLocalNbrList) + ' ) ';

        s :='select DISTINCT d.SY_GL_AGENCY_REPORT_NBR ' +
            'from SY_LOCALS a, SY_GLOBAL_AGENCY b, SY_GL_AGENCY_FIELD_OFFICE c, ' +
             'SY_GL_AGENCY_REPORT d, SY_REPORTS_GROUP e ' +
              'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
               '{AsOfNow<c>} and {AsOfNow<d>} and ' +
               '{AsOfNow<e>} and b.STATE = ''PA'' and ' +
               PaFilter + ' and ' +
              ' a.LOCAL_TYPE IN ('''+LOCAL_TYPE_SCHOOL+''',''' + LOCAL_TYPE_INC_RESIDENTIAL + ''','''+LOCAL_TYPE_INC_NON_RESIDENTIAL+''') and ' +
               'a.SY_LOCAL_REPORTING_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
               'c.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
               'c.SY_GL_AGENCY_FIELD_OFFICE_NBR = d.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
               'd.SY_REPORTS_GROUP_NBR = e.SY_REPORTS_GROUP_NBR and ' +
               'a.COMBINE_FOR_TAX_PAYMENTS = ''Y'' and ' +
               'd.SYSTEM_TAX_TYPE = ''G'' and d.ENLIST_AUTOMATICALLY = ''Y''  and ' +
               'b.SY_GLOBAL_AGENCY_NBR <> 441 ';

        if not cdCODontAutoEnlistReturns.Active then
        begin
            with TExecDSWrapper.Create(s) do
              ctx_DataAccess.GetSyCustomData(cdCODontAutoEnlistReturns, AsVariant);

            Assert(cdCODontAutoEnlistReturns.FindField('SY_GL_AGENCY_REPORT_NBR') <> nil);
            cdCODontAutoEnlistReturns.AddIndex('NBR', 'SY_GL_AGENCY_REPORT_NBR', []);
            cdCODontAutoEnlistReturns.IndexDefs.Update;
            cdCODontAutoEnlistReturns.IndexName := 'NBR';
            cdCOAutoEnlistReturns.Filter :='PROCESS_TYPE =''B'' ';
            cdCOAutoEnlistReturns.Filtered := True;
            try
              cdCOAutoEnlistReturns.First;
              if cdCOAutoEnlistReturns.RecordCount > 0 then
              begin
                while not cdCOAutoEnlistReturns.Eof do
                begin
                    if cdCODontAutoEnlistReturns.locate('SY_GL_AGENCY_REPORT_NBR', cdCOAutoEnlistReturns['SY_GL_AGENCY_REPORT_NBR'], []) then
                       cdCODontAutoEnlistReturns.Delete;
                 cdCOAutoEnlistReturns.next;
                end;
              end
              else
              begin
                cdCODontAutoEnlistReturns.first;
                while not cdCODontAutoEnlistReturns.Eof do
                  cdCODontAutoEnlistReturns.Delete;
              end;
            finally
              cdCOAutoEnlistReturns.Filter :='';
              cdCOAutoEnlistReturns.Filtered := false;
            end;
        end;
      end;

      s :='select a.SY_REPORT_WRITER_REPORTS_NBR, a.SY_REPORT_GROUPS_NBR ' +
          'from SY_REPORT_GROUP_MEMBERS a, SY_REPORT_GROUPS b ' +
          'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
            'a.SY_REPORT_GROUPS_NBR = b.SY_REPORT_GROUPS_NBR and ' +
            'a.SY_REPORT_GROUPS_NBR not in ( 19, 20, 21, 22 )   and ' +
            'b.NAME like ''Tax Return-%''';

      if not cdSYReportMember.Active then
      begin
          with TExecDSWrapper.Create(s) do
          begin
            ctx_DataAccess.GetSyCustomData(cdSYReportMember, AsVariant);
          end;
          Assert(cdSYReportMember.FindField('SY_REPORT_WRITER_REPORTS_NBR') <> nil);
          Assert(cdSYReportMember.FindField('SY_REPORT_GROUPS_NBR') <> nil);
          cdSYReportMember.AddIndex('NBR', 'SY_REPORT_WRITER_REPORTS_NBR;SY_REPORT_GROUPS_NBR', []);
          cdSYReportMember.IndexDefs.Update;
          cdSYReportMember.IndexName := 'NBR';
      end;


      DM_SYSTEM_MISC.SY_REPORT_GROUPS.First;
      while not DM_SYSTEM_MISC.SY_REPORT_GROUPS.Eof do
      begin
        if not cdCOEnlistGroup.FindKey([DM_SYSTEM_MISC.SY_REPORT_GROUPS['SY_REPORT_GROUPS_NBR']]) then
          cdCOEnlistGroup.AppendRecord([DM_SYSTEM_MISC.SY_REPORT_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').Value , 'A', null]);
        DM_SYSTEM_MISC.SY_REPORT_GROUPS.Next;
      end;

      cdSY_GL_AGENCY_REPORT.First;
      while not cdSY_GL_AGENCY_REPORT.Eof do
      begin
         bInsert := False;
         ActiveControl := 1;

         if cdCOAutoEnlistReturns.FindKey([cdSY_GL_AGENCY_REPORT['SY_GL_AGENCY_REPORT_NBR']]) then
         begin
             bInsert := CheckCoEnlistReturns(ActiveControl);
         end
         else
         begin
           if cdSY_GL_AGENCY_REPORT['ENLIST_AUTOMATICALLY'] = GROUP_BOX_YES then
               bInsert := CheckGroupMember;
         end;

         if bInsert then
            bInsert := CheckCoDontEnlistReturns;

         if bInsert
            and SyAgencyNbr.Locate('NBR;ActiveControl', VarArrayOf([cdSY_GL_AGENCY_REPORT['SY_GLOBAL_AGENCY_NBR'],ActiveControl]), []) then
         begin
            if ((SyAgencyNbr['FedReport'] = FED_REPORT_EXCEPT_940 ) and
                  (cdSY_GL_AGENCY_REPORT['TAXRETURN940'] = GROUP_BOX_YES))
                or
               ((SyAgencyNbr['FedReport'] = FED_REPORT_940 ) and
                 (cdSY_GL_AGENCY_REPORT['TAXRETURN940'] = GROUP_BOX_NO))  then
                 bInsert := False;
            if  bInsert and
                (string(cdSY_GL_AGENCY_REPORT['RETURN_FREQUENCY'])[1] in PeriodType)
            and ((cdSY_GL_AGENCY_REPORT['DEPOSIT_FREQUENCY'] = FREQUENCY_TYPE_DONT_CARE)
              or (ConvertNull(cdSY_GL_AGENCY_REPORT['DEPOSIT_FREQUENCY'], ' ') = SyAgencyNbr['Freq']))
            and ((ConvertNull(cdSY_GL_AGENCY_REPORT['TAX_SERVICE_FILTER'], CO_TAX_SERVICE_FILTER_ALL) = CO_TAX_SERVICE_FILTER_ALL)
              or ((ConvertNull(cdSY_GL_AGENCY_REPORT['TAX_SERVICE_FILTER'], CO_TAX_SERVICE_FILTER_ALL) = CO_TAX_SERVICE_FILTER_WITH)
                and SbSendsTaxPaymentsForCompany)
              or ((ConvertNull(cdSY_GL_AGENCY_REPORT['TAX_SERVICE_FILTER'], CO_TAX_SERVICE_FILTER_ALL) = CO_TAX_SERVICE_FILTER_WITHOUT)
                and not SbSendsTaxPaymentsForCompany))
            and ((cdSY_GL_AGENCY_REPORT['TAXRETURN940'] = GROUP_BOX_NO)
              or (not ((cdSY_GL_AGENCY_REPORT['TAXRETURN940'] = GROUP_BOX_YES) and (DM_COMPANY.CO['FUI_TAX_EXEMPT'] = GROUP_BOX_YES))))
            and ((cdSY_GL_AGENCY_REPORT['PAYMENT_METHOD'] = AG_PAYMENT_METHOD_DONT_CARE)
              or (SyAgencyNbr['DepMethod'] = '-')
              or (cdSY_GL_AGENCY_REPORT['PAYMENT_METHOD'] = SyAgencyNbr['DepMethod']))
            and ((cdSY_GL_AGENCY_REPORT['TAX945_CHECKS'] = TAX945_CHECKS_DONT_CARE)
              or ((cdSY_GL_AGENCY_REPORT['TAX945_CHECKS'] = TAX945_CHECKS_EXIST) and Check945Presented)
              or (cdSY_GL_AGENCY_REPORT['TAX945_CHECKS'] = TAX945_CHECKS_DONOTEXIST) and not Check945Presented)
            and ((cdSY_GL_AGENCY_REPORT['TAX943_COMPANY'] = GROUP_BOX_NOT_APPLICATIVE)
              or ((cdSY_GL_AGENCY_REPORT['TAX943_COMPANY'] = GROUP_BOX_YES) and (ConvertNull(DM_COMPANY.CO['BUSINESS_TYPE'], '')=BUSINESS_TYPE_AGRICULTURE))
              or (cdSY_GL_AGENCY_REPORT['TAX943_COMPANY'] = GROUP_BOX_NO) and (ConvertNull(DM_COMPANY.CO['BUSINESS_TYPE'], '')<>BUSINESS_TYPE_AGRICULTURE))
            and ((cdSY_GL_AGENCY_REPORT['TAX944_COMPANY'] = GROUP_BOX_NOT_APPLICATIVE)
              or ((cdSY_GL_AGENCY_REPORT['TAX944_COMPANY'] = GROUP_BOX_YES) and (ConvertNull(DM_COMPANY.CO['BUSINESS_TYPE'], '')=BUSINESS_TYPE_944))
              or (cdSY_GL_AGENCY_REPORT['TAX944_COMPANY'] = GROUP_BOX_NO) and (ConvertNull(DM_COMPANY.CO['BUSINESS_TYPE'], '')<>BUSINESS_TYPE_944))
            and ((cdSY_GL_AGENCY_REPORT['PRINT_WHEN'] = PRINT_WHEN_ALWAYS)
              or ((cdSY_GL_AGENCY_REPORT['PRINT_WHEN'] = PRINT_WHEN_NO_DUES)
                and not (SyAgencyNbr['HasLiabs']))
              or ((cdSY_GL_AGENCY_REPORT['PRINT_WHEN'] = PRINT_WHEN_DUES)
                and (SyAgencyNbr['HasLiabs']))) then
            begin
                   if DM_COMPANY.CO_STATES.Locate('STATE', 'WY', []) then
                      if cdSY_GL_AGENCY_REPORT['SY_REPORT_WRITER_REPORTS_NBR'] = 1129 then
                        bInsert := DM_COMPANY.CO_WORKERS_COMP.Locate('CO_STATES_NBR', DM_COMPANY.CO_STATES.CO_STATES_NBR.AsInteger, []);

                    // child or parent
                   if (Scope <>'') then
                   begin  //  parent
                     if (not VarIsNull(ClCoConsolidationNbr)) then
                     begin
                       if (Scope[1]=CONSOLIDATION_SCOPE_ALL) then
                       begin
                         if (cdSY_GL_AGENCY_REPORT['CONSOLIDATED_FILTER'] = CONSOLIDATED_FILTER_NONCONSOLIDATED) then
                             bInsert := false;
                       end
                       else
                       if (Scope[1]=CONSOLIDATION_SCOPE_PAYROLL_ONLY) then
                           bInsert := false;
                     end
                     else  //  child
                     begin
                       if (Scope[1]=CONSOLIDATION_SCOPE_ALL) then
                          if (cdSY_GL_AGENCY_REPORT['CONSOLIDATED_FILTER'] = CONSOLIDATED_FILTER_CONSOLIDATED) then
                           if  not ((ConsolidationType[1] = CONSOLIDATION_TYPE_STATE) and
                                    (cdSY_GL_AGENCY_REPORT['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED ) and
                                    (cdSY_GL_AGENCY_REPORT['CONSOLIDATED_FILTER'] = CONSOLIDATED_FILTER_CONSOLIDATED)) then
                             bInsert := false;
                     end;
                   end;

                   if (not VarIsNull(ClCoConsolidationNbr)) then
                   begin
                     if ConsolidationType[1] = CONSOLIDATION_TYPE_STATE  then
                         if  (cdSY_GL_AGENCY_REPORT['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_STATE ) and
                             (cdSY_GL_AGENCY_REPORT['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_SUI) then
                               bInsert := false;
                     if ConsolidationType[1] = CONSOLIDATION_TYPE_FED  then
                         if  (cdSY_GL_AGENCY_REPORT['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED ) and
                             (cdSY_GL_AGENCY_REPORT['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE) then
                             bInsert := false;
                   end;


                   if bInsert then
                      if not DM_COMPANY.CO_TAX_RETURN_QUEUE.Locate('CO_NBR;SY_GL_AGENCY_REPORT_NBR;CL_CO_CONSOLIDATION_NBR;PERIOD_END_DATE',
                        VarArrayOf([CoNbr, cdSY_GL_AGENCY_REPORT['SY_GL_AGENCY_REPORT_NBR'], ClCoConsolidationNbr, PeriodEndDate]), []) then
                      begin
                          DM_COMPANY.CO_TAX_RETURN_QUEUE.Insert;
                          try
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['SY_REPORTS_GROUP_NBR'] := 0;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['SY_GL_AGENCY_REPORT_NBR'] := cdSY_GL_AGENCY_REPORT['SY_GL_AGENCY_REPORT_NBR'];

                            DM_COMPANY.CO_TAX_RETURN_QUEUE['CL_CO_CONSOLIDATION_NBR'] := ClCoConsolidationNbr;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR'] := CoNbr;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_UNPROCESSED;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['SB_COPY_PRINTED'] := GROUP_BOX_NO;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['CLIENT_COPY_PRINTED'] := GROUP_BOX_NO;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED'] := GROUP_BOX_NO;
                            DM_COMPANY.CO_TAX_RETURN_QUEUE['PERIOD_END_DATE'] := PeriodEndDate;

                            DM_COMPANY.CO_TAX_RETURN_QUEUE['DUE_DATE'] :=
                              CalculateReturnDueDate(
                                SyAgencyNbr['Nbr'],
                                cdSY_GL_AGENCY_REPORT['RETURN_FREQUENCY'],
                                cdSY_GL_AGENCY_REPORT['WHEN_DUE_TYPE'],
                                PeriodEndDate,
                                cdSY_GL_AGENCY_REPORT['NUMBER_OF_DAYS_FOR_WHEN_DUE']);

                            DM_COMPANY.CO_TAX_RETURN_QUEUE['PRODUCE_ASCII_FILE'] :=
                                  BoolToYN((cdSY_GL_AGENCY_REPORT['VMEDIA_TYPE'] = REPORT_RESULT_TYPE_FILE)
                                  or ((cdSY_GL_AGENCY_REPORT['VMEDIA_TYPE'] = REPORT_RESULT_TYPE_BOTH) and
                                     (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', SyAgencyNbr['Nbr'], 'MAG_MEDIA') = GROUP_BOX_YES)));
                            DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;
                          except
                            DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;
                            raise;
                          end;
                      end;
            end;
         end;

         cdSY_GL_AGENCY_REPORT.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
    finally
      SyAgencyNbr.Free;
      lActiveSuis.Free;
      cdSY_GL_AGENCY_REPORT.Free;
      cdCOEnlistGroup.Free;
      cdCOAutoEnlistReturns.Free;
      cdCODontAutoEnlistReturns.Free;
      cdSYReportMember.Free;
      cdTCDAgency.Free;
    end;
  except
    on E: Exception do
    begin
      ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
      raise;
    end;
  end;
end;

end.
