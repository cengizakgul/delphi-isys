object CopyWizard: TCopyWizard
  Left = 392
  Top = 112
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'CopyWizard'
  ClientHeight = 509
  ClientWidth = 738
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 738
    Height = 509
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel1'
    TabOrder = 0
    object Panel2: TevPanel
      Left = 0
      Top = 459
      Width = 738
      Height = 50
      Align = alBottom
      BevelOuter = bvLowered
      TabOrder = 0
      DesignSize = (
        738
        50)
      object btBack: TevBitBtn
        Left = 569
        Top = 13
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Back'
        Enabled = False
        ModalResult = 4
        TabOrder = 1
        OnClick = btBackClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDD
          848DDDDDDDDDDDDDF8FDDDDDDDDDDD84448DDDDDDDDDDDF888FDDDDDDDDD844F
          F48DDDDDDDDDF88DD8FDDDDDDD844FFFF48DDDDDDDF88DDDD8FDDDDD844FFFFF
          F48DDDDDF88DDDDDD8FDDDD44FFFFFFFF48DDDD88DDDDDDDD8FDDD477FFFFFFF
          F48DDD8FFDDDDDDDD8FDDDD4477FFFFFF48DDDD88FFDDDDDD8FDDDDDD4477FFF
          F48DDDDDD88FFDDDD8FDDDDDDDD4477FF48DDDDDDDD88FFDD8FDDDDDDDDDD447
          748DDDDDDDDDD88FF8FDDDDDDDDDDDD4448DDDDDDDDDDDD888FDDDDDDDDDDDDD
          D4DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btNext: TevBitBtn
        Left = 649
        Top = 13
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Next'
        Default = True
        ModalResult = 1
        TabOrder = 2
        OnClick = btNextClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDDD4888DDDDDDD
          DDDDD8FFDDDDDDDDDDDDD444888DDDDDDDDDD888FFDDDDDDDDDDD47F44888DDD
          DDDDD8FD88FFDDDDDDDDD47FFF44888DDDDDD8FDDD88FFDDDDDDD47FFFFF4488
          8DDDD8FDDDDD88FFDDDDD47FFFFFFF4488DDD8FDDDDDDD88FDDDD47FFFFFFF77
          4DDDD8FDDDDDDDFF8DDDD47FFFFF7744DDDDD8FDDDDDFF88DDDDD47FFF7744DD
          DDDDD8FDDDFF88DDDDDDD47F7744DDDDDDDDD8FDFF88DDDDDDDDD47744DDDDDD
          DDDDD8FF88DDDDDDDDDDD444DDDDDDDDDDDDD888DDDDDDDDDDDDD4DDDDDDDDDD
          DDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Layout = blGlyphRight
        Margin = 0
      end
      object btCancel: TevBitBtn
        Left = 488
        Top = 13
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Cancel = True
        Caption = '&Cancel'
        ModalResult = 2
        TabOrder = 0
        OnClick = btCancelClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
          DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
          9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
          DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
          DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
          DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
          91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
          999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
        NumGlyphs = 2
        Margin = 0
      end
      object evPanel2: TevPanel
        Left = 1
        Top = 1
        Width = 336
        Height = 48
        Align = alLeft
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 3
        object lablImportFolder: TevLabel
          Left = 128
          Top = 5
          Width = 69
          Height = 13
          Caption = 'Imports Folder:'
          Visible = False
        end
        object spbImportSourceFile: TevSpeedButton
          Left = 303
          Top = 19
          Width = 23
          Height = 22
          HideHint = True
          AutoSize = False
          Visible = False
          OnClick = spbImportSourceFileClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
            7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
            7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          ParentColor = False
          ShortCut = 0
        end
        object cbClearOutFields: TCheckBox
          Left = 16
          Top = 8
          Width = 97
          Height = 17
          Caption = 'Clear Out Fields'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object cbUseImport: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Use Import Files'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = cbUseImportClick
        end
        object editImportFolder: TevEdit
          Left = 128
          Top = 19
          Width = 169
          Height = 21
          TabOrder = 2
          Visible = False
        end
      end
    end
    object Panel4: TevPanel
      Left = 0
      Top = 0
      Width = 738
      Height = 459
      Align = alClient
      BevelOuter = bvLowered
      Caption = 'Panel4'
      TabOrder = 1
      object pctCopyWizard: TevPageControl
        Left = 1
        Top = 1
        Width = 736
        Height = 457
        ActivePage = tshSelectBase
        Align = alClient
        Style = tsFlatButtons
        TabOrder = 0
        object tshSelectBase: TTabSheet
          Caption = 'tshSelectBase'
          object evSplitter1: TevSplitter
            Left = 257
            Top = 0
            Height = 426
          end
          object evPanel1: TevPanel
            Left = 0
            Top = 0
            Width = 257
            Height = 426
            Align = alLeft
            BevelOuter = bvNone
            Caption = 'evPanel1'
            TabOrder = 0
            object gdClient: TevDBGrid
              Left = 0
              Top = 0
              Width = 257
              Height = 426
              DisableThemesInTitle = False
              ControlInfoInDataSet = True
              Selected.Strings = (
                'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'
                'NAME'#9'40'#9'Name')
              IniAttributes.Enabled = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TCopyWizard\gdClient'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = dsClient
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              UseTFields = True
              PaintOptions.AlternatingRowColor = clCream
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object gdCompany: TevDBGrid
            Left = 260
            Top = 0
            Width = 468
            Height = 426
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
              'NAME'#9'25'#9'Name'#9'F'
              'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
              'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
              'FEIN'#9'9'#9'Fein'#9
              'DBA'#9'40'#9'Dba'#9'F'
              'UserFirstName'#9'20'#9'CSR First Name'#9'F'
              'UserLastName'#9'30'#9'CSR Last Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCopyWizard\gdCompany'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsCompany
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  object dsClient: TevDataSource
    DataSet = DM_TMP_CL.TMP_CL
    Left = 128
    Top = 84
  end
  object TmpDS: TevClientDataSet
    Left = 589
    Top = 220
  end
  object dsCompany: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    MasterDataSource = dsClient
    Left = 93
    Top = 164
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 405
    Top = 76
  end
end
