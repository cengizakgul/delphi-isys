object evAuditBlob: TevAuditBlob
  Left = 888
  Top = 435
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Blob Data'
  ClientHeight = 505
  ClientWidth = 798
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object pnlOldValue: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 401
    Height = 505
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Before change'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 6
    inline frmOldValue: TevBlobViewer
      Left = 24
      Top = 48
      Width = 351
      Height = 431
      Align = alClient
      TabOrder = 0
      inherited imgBlob: TevImage
        Width = 351
        Height = 431
      end
      inherited memBlob: TevMemo
        Width = 351
        Height = 431
        WordWrap = True
      end
    end
  end
  object pnlNewValue: TisUIFashionPanel
    Left = 401
    Top = 0
    Width = 401
    Height = 505
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 1
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'After change'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 6
    inline frmNewValue: TevBlobViewer
      Left = 24
      Top = 48
      Width = 351
      Height = 431
      Align = alClient
      TabOrder = 0
      inherited imgBlob: TevImage
        Width = 351
        Height = 431
      end
      inherited memBlob: TevMemo
        Width = 351
        Height = 431
        WordWrap = True
      end
    end
  end
end
