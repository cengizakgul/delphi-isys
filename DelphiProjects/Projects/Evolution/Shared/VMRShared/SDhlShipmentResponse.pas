
{***********************************************************************************************************}
{                                                                                                           }
{                                             XML Data Binding                                              }
{                                                                                                           }
{         Generated on: 11/3/2004 12:33:26 PM                                                               }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\ShipmentResponse.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\ShipmentResponse.xdb   }
{                                                                                                           }
{***********************************************************************************************************}

unit SDhlShipmentResponse;

interface

uses SysUtils, xmldom, XMLDoc, XMLIntf, EvStreamUtils, EvUtils;

type

{ Forward Decls }

  IXMLShipmentType = interface;
  IXMLFaultsType = interface;
  IXMLFaultType = interface;
  IXMLResultType = interface;
  IXMLShippingCredentialsType = interface;
  IXMLShipmentDetailType = interface;
  IXMLOriginType = interface;
  IXMLServiceType = interface;
  IXMLServiceLevelCommitmentType = interface;
  IXMLRateEstimateType = interface;
  IXMLChargesType = interface;
  IXMLChargeType = interface;
  IXMLTypeType = interface;
  IXMLLabelType = interface;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{53BA9A49-CBE2-4E5E-BDC7-9F68AFF2864E}']
    { Property Accessors }
    function Get_Action: string;
    function Get_Version: string;
    function Get_Test: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Result: IXMLResultType;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_Label_: IXMLLabelType;
    function Get_TransactionTrace: string;
    { Methods & Properties }
    property Action: string read Get_Action;
    property Version: string read Get_Version;
    property Test: string read Get_Test;
    property Faults: IXMLFaultsType read Get_Faults;
    property Result: IXMLResultType read Get_Result;
    property ShippingCredentials: IXMLShippingCredentialsType read Get_ShippingCredentials;
    property ShipmentDetail: IXMLShipmentDetailType read Get_ShipmentDetail;
    property Label_: IXMLLabelType read Get_Label_;
    property TransactionTrace: string read Get_TransactionTrace;
  end;

{ IXMLFaultsType }

  IXMLFaultsType = interface(IXMLNodeCollection)
    ['{D94FC517-3212-49DA-B5C5-DD810F45BF6C}']
    { Property Accessors }
    function Get_Fault(Index: Integer): IXMLFaultType;
    { Methods & Properties }
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
    property Fault[Index: Integer]: IXMLFaultType read Get_Fault; default;
  end;

{ IXMLFaultType }

  IXMLFaultType = interface(IXMLNode)
    ['{A5C35E6C-1B3C-4C8D-A427-8CFF651FD924}']
    { Property Accessors }
    function Get_Source: string;
    function Get_Code: string;
    function Get_Desc: string;
    function Get_Context: string;
    { Methods & Properties }
    property Source: string read Get_Source;
    property Code: string read Get_Code;
    property Desc: string read Get_Desc;
    property Context: string read Get_Context;
  end;

{ IXMLResultType }

  IXMLResultType = interface(IXMLNode)
    ['{6B09BB6D-D1D2-47D0-93A5-42517D9CBD1A}']
    { Property Accessors }
    function Get_Code: string;
    function Get_Desc: string;
    { Methods & Properties }
    property Code: string read Get_Code;
    property Desc: string read Get_Desc;
  end;

{ IXMLShippingCredentialsType }

  IXMLShippingCredentialsType = interface(IXMLNode)
    ['{7F884A11-05CD-4692-9AF7-6B5465FE9D7F}']
    { Property Accessors }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
    { Methods & Properties }
    property ShippingKey: string read Get_ShippingKey;
    property AccountNbr: Integer read Get_AccountNbr;
  end;

{ IXMLShipmentDetailType }

  IXMLShipmentDetailType = interface(IXMLNode)
    ['{1751B0D1-0CFB-4FDF-A64F-CC6C130CBDD4}']
    { Property Accessors }
    function Get_DateGenerated: WideString;
    function Get_AirbillNbr: string;
    function Get_ShipDate: WideString;
    function Get_Origin: IXMLOriginType;
    function Get_RouteCode: string;
    function Get_Service: IXMLServiceType;
    function Get_ServiceLevelCommitment: IXMLServiceLevelCommitmentType;
    function Get_RateEstimate: IXMLRateEstimateType;
    { Methods & Properties }
    property DateGenerated: WideString read Get_DateGenerated;
    property AirbillNbr: string read Get_AirbillNbr;
    property ShipDate: WideString read Get_ShipDate;
    property Origin: IXMLOriginType read Get_Origin;
    property RouteCode: string read Get_RouteCode;
    property Service: IXMLServiceType read Get_Service;
    property ServiceLevelCommitment: IXMLServiceLevelCommitmentType read Get_ServiceLevelCommitment;
    property RateEstimate: IXMLRateEstimateType read Get_RateEstimate;
  end;

{ IXMLOriginType }

  IXMLOriginType = interface(IXMLNode)
    ['{B25B9876-AFAF-41F2-A879-0DE2AF8AE684}']
    { Property Accessors }
    function Get_Station: string;
    { Methods & Properties }
    property Station: string read Get_Station;
  end;

{ IXMLServiceType }

  IXMLServiceType = interface(IXMLNode)
    ['{929B8AC6-72BF-47EF-A56E-2DCC903455A8}']
    { Property Accessors }
    function Get_Code: string;
    function Get_Desc: string;
    function Get_Day: string;
    { Methods & Properties }
    property Code: string read Get_Code;
    property Desc: string read Get_Desc;
    property Day: string read Get_Day;
  end;

{ IXMLServiceLevelCommitmentType }

  IXMLServiceLevelCommitmentType = interface(IXMLNode)
    ['{211F0450-BBC8-438C-9640-6A7AB0E15016}']
    { Property Accessors }
    function Get_Desc: string;
    { Methods & Properties }
    property Desc: string read Get_Desc;
  end;

{ IXMLRateEstimateType }

  IXMLRateEstimateType = interface(IXMLNode)
    ['{D650E549-3FC6-4C4C-A525-F444F6C43CD5}']
    { Property Accessors }
    function Get_TotalChargeEstimate: Currency;
    function Get_Charges: IXMLChargesType;
    { Methods & Properties }
    property TotalChargeEstimate: Currency read Get_TotalChargeEstimate;
    property Charges: IXMLChargesType read Get_Charges;
  end;

{ IXMLChargesType }

  IXMLChargesType = interface(IXMLNodeCollection)
    ['{3B72055C-01CB-43B5-BE50-7BE75FBD9345}']
    { Property Accessors }
    function Get_Charge(Index: Integer): IXMLChargeType;
    { Methods & Properties }
    function Add: IXMLChargeType;
    function Insert(const Index: Integer): IXMLChargeType;
    property Charge[Index: Integer]: IXMLChargeType read Get_Charge; default;
  end;

{ IXMLChargeType }

  IXMLChargeType = interface(IXMLNode)
    ['{1B360A95-F5BD-4C15-803A-6976A717E3EE}']
    { Property Accessors }
    function Get_Type_: IXMLTypeType;
    function Get_Value: string;
    { Methods & Properties }
    property Type_: IXMLTypeType read Get_Type_;
    property Value: string read Get_Value;
  end;

{ IXMLTypeType }

  IXMLTypeType = interface(IXMLNode)
    ['{79B6942C-F33F-4BD2-A3D4-5442CB4812F7}']
    { Property Accessors }
    function Get_Code: string;
    function Get_Desc: string;
    { Methods & Properties }
    property Code: string read Get_Code;
    property Desc: string read Get_Desc;
  end;

{ IXMLLabelType }

  IXMLLabelType = interface(IXMLNode)
    ['{EAAAE333-9C0C-45E2-8E32-EF85749F4BD1}']
    { Property Accessors }
    function Get_ImageType: string;
    function Get_Image: IEvDualStream;
    { Methods & Properties }
    property ImageType: string read Get_ImageType;
    property Image: IEvDualStream read Get_Image;
  end;

{ Forward Decls }

  TXMLShipmentType = class;
  TXMLFaultsType = class;
  TXMLFaultType = class;
  TXMLResultType = class;
  TXMLShippingCredentialsType = class;
  TXMLShipmentDetailType = class;
  TXMLOriginType = class;
  TXMLServiceType = class;
  TXMLServiceLevelCommitmentType = class;
  TXMLRateEstimateType = class;
  TXMLChargesType = class;
  TXMLChargeType = class;
  TXMLTypeType = class;
  TXMLLabelType = class;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  protected
    { IXMLShipmentType }
    function Get_Action: string;
    function Get_Version: string;
    function Get_Test: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Result: IXMLResultType;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_Label_: IXMLLabelType;
    function Get_TransactionTrace: string;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultsType }

  TXMLFaultsType = class(TXMLNodeCollection, IXMLFaultsType)
  protected
    { IXMLFaultsType }
    function Get_Fault(Index: Integer): IXMLFaultType;
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultType }

  TXMLFaultType = class(TXMLNode, IXMLFaultType)
  protected
    { IXMLFaultType }
    function Get_Source: string;
    function Get_Code: string;
    function Get_Desc: string;
    function Get_Context: string;
  end;

{ TXMLResultType }

  TXMLResultType = class(TXMLNode, IXMLResultType)
  protected
    { IXMLResultType }
    function Get_Code: string;
    function Get_Desc: string;
  end;

{ TXMLShippingCredentialsType }

  TXMLShippingCredentialsType = class(TXMLNode, IXMLShippingCredentialsType)
  protected
    { IXMLShippingCredentialsType }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
  end;

{ TXMLShipmentDetailType }

  TXMLShipmentDetailType = class(TXMLNode, IXMLShipmentDetailType)
  protected
    { IXMLShipmentDetailType }
    function Get_DateGenerated: WideString;
    function Get_AirbillNbr: string;
    function Get_ShipDate: WideString;
    function Get_Origin: IXMLOriginType;
    function Get_RouteCode: string;
    function Get_Service: IXMLServiceType;
    function Get_ServiceLevelCommitment: IXMLServiceLevelCommitmentType;
    function Get_RateEstimate: IXMLRateEstimateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOriginType }

  TXMLOriginType = class(TXMLNode, IXMLOriginType)
  protected
    { IXMLOriginType }
    function Get_Station: string;
  end;

{ TXMLServiceType }

  TXMLServiceType = class(TXMLNode, IXMLServiceType)
  protected
    { IXMLServiceType }
    function Get_Code: string;
    function Get_Desc: string;
    function Get_Day: string;
  end;

{ TXMLServiceLevelCommitmentType }

  TXMLServiceLevelCommitmentType = class(TXMLNode, IXMLServiceLevelCommitmentType)
  protected
    { IXMLServiceLevelCommitmentType }
    function Get_Desc: string;
  end;

{ TXMLRateEstimateType }

  TXMLRateEstimateType = class(TXMLNode, IXMLRateEstimateType)
  protected
    { IXMLRateEstimateType }
    function Get_TotalChargeEstimate: Currency;
    function Get_Charges: IXMLChargesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLChargesType }

  TXMLChargesType = class(TXMLNodeCollection, IXMLChargesType)
  protected
    { IXMLChargesType }
    function Get_Charge(Index: Integer): IXMLChargeType;
    function Add: IXMLChargeType;
    function Insert(const Index: Integer): IXMLChargeType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLChargeType }

  TXMLChargeType = class(TXMLNode, IXMLChargeType)
  protected
    { IXMLChargeType }
    function Get_Type_: IXMLTypeType;
    function Get_Value: string;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTypeType }

  TXMLTypeType = class(TXMLNode, IXMLTypeType)
  protected
    { IXMLTypeType }
    function Get_Code: string;
    function Get_Desc: string;
  end;

{ TXMLLabelType }

  TXMLLabelType = class(TXMLNode, IXMLLabelType)
  protected
    { IXMLLabelType }
    function Get_ImageType: string;
    function Get_Image: IEvDualStream;
  end;

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
function LoadShipment(const FileName: WideString): IXMLShipmentType;
function NewShipment: IXMLShipmentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
begin
  Result := Doc.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function LoadShipment(const FileName: WideString): IXMLShipmentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function NewShipment: IXMLShipmentType;
begin
  Result := NewXMLDocument.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('Faults', TXMLFaultsType);
  RegisterChildNode('Result', TXMLResultType);
  RegisterChildNode('ShippingCredentials', TXMLShippingCredentialsType);
  RegisterChildNode('ShipmentDetail', TXMLShipmentDetailType);
  RegisterChildNode('Label', TXMLLabelType);
  inherited;
end;

function TXMLShipmentType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

function TXMLShipmentType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

function TXMLShipmentType.Get_Test: string;
begin
  Result := AttributeNodes['test'].Text;
end;

function TXMLShipmentType.Get_Faults: IXMLFaultsType;
begin
  Result := ChildNodes['Faults'] as IXMLFaultsType;
end;

function TXMLShipmentType.Get_Result: IXMLResultType;
begin
  Result := ChildNodes['Result'] as IXMLResultType;
end;

function TXMLShipmentType.Get_ShippingCredentials: IXMLShippingCredentialsType;
begin
  Result := ChildNodes['ShippingCredentials'] as IXMLShippingCredentialsType;
end;

function TXMLShipmentType.Get_ShipmentDetail: IXMLShipmentDetailType;
begin
  Result := ChildNodes['ShipmentDetail'] as IXMLShipmentDetailType;
end;

function TXMLShipmentType.Get_Label_: IXMLLabelType;
begin
  Result := ChildNodes['Label'] as IXMLLabelType;
end;

function TXMLShipmentType.Get_TransactionTrace: string;
begin
  Result := ChildNodes['TransactionTrace'].Text;
end;

{ TXMLFaultsType }

procedure TXMLFaultsType.AfterConstruction;
begin
  RegisterChildNode('Fault', TXMLFaultType);
  ItemTag := 'Fault';
  ItemInterface := IXMLFaultType;
  inherited;
end;

function TXMLFaultsType.Get_Fault(Index: Integer): IXMLFaultType;
begin
  Result := List[Index] as IXMLFaultType;
end;

function TXMLFaultsType.Add: IXMLFaultType;
begin
  Result := AddItem(-1) as IXMLFaultType;
end;

function TXMLFaultsType.Insert(const Index: Integer): IXMLFaultType;
begin
  Result := AddItem(Index) as IXMLFaultType;
end;

{ TXMLFaultType }

function TXMLFaultType.Get_Source: string;
begin
  Result := ChildNodes['Source'].Text;
end;

function TXMLFaultType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

function TXMLFaultType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

function TXMLFaultType.Get_Context: string;
begin
  Result := ChildNodes['Context'].Text;
end;

{ TXMLResultType }

function TXMLResultType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

function TXMLResultType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

{ TXMLShippingCredentialsType }

function TXMLShippingCredentialsType.Get_ShippingKey: string;
begin
  Result := ChildNodes['ShippingKey'].Text;
end;

function TXMLShippingCredentialsType.Get_AccountNbr: Integer;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

{ TXMLShipmentDetailType }

procedure TXMLShipmentDetailType.AfterConstruction;
begin
  RegisterChildNode('Origin', TXMLOriginType);
  RegisterChildNode('Service', TXMLServiceType);
  RegisterChildNode('ServiceLevelCommitment', TXMLServiceLevelCommitmentType);
  RegisterChildNode('RateEstimate', TXMLRateEstimateType);
  inherited;
end;

function TXMLShipmentDetailType.Get_DateGenerated: WideString;
begin
  Result := ChildNodes['DateGenerated'].Text;
end;

function TXMLShipmentDetailType.Get_AirbillNbr: string;
begin
  Result := ChildNodes['AirbillNbr'].Text;
end;

function TXMLShipmentDetailType.Get_ShipDate: WideString;
begin
  Result := ChildNodes['ShipDate'].Text;
end;

function TXMLShipmentDetailType.Get_Origin: IXMLOriginType;
begin
  Result := ChildNodes['Origin'] as IXMLOriginType;
end;

function TXMLShipmentDetailType.Get_RouteCode: string;
begin
  Result := ChildNodes['RouteCode'].Text;
end;

function TXMLShipmentDetailType.Get_Service: IXMLServiceType;
begin
  Result := ChildNodes['Service'] as IXMLServiceType;
end;

function TXMLShipmentDetailType.Get_ServiceLevelCommitment: IXMLServiceLevelCommitmentType;
begin
  Result := ChildNodes['ServiceLevelCommitment'] as IXMLServiceLevelCommitmentType;
end;

function TXMLShipmentDetailType.Get_RateEstimate: IXMLRateEstimateType;
begin
  Result := ChildNodes['RateEstimate'] as IXMLRateEstimateType;
end;

{ TXMLOriginType }

function TXMLOriginType.Get_Station: string;
begin
  Result := ChildNodes['Station'].Text;
end;

{ TXMLServiceType }

function TXMLServiceType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

function TXMLServiceType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

function TXMLServiceType.Get_Day: string;
begin
  Result := ChildNodes['Day'].Text;
end;

{ TXMLServiceLevelCommitmentType }

function TXMLServiceLevelCommitmentType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

{ TXMLRateEstimateType }

procedure TXMLRateEstimateType.AfterConstruction;
begin
  RegisterChildNode('Charges', TXMLChargesType);
  inherited;
end;

function TXMLRateEstimateType.Get_TotalChargeEstimate: Currency;
begin
  Result := ChildNodes['TotalChargeEstimate'].NodeValue;
end;

function TXMLRateEstimateType.Get_Charges: IXMLChargesType;
begin
  Result := ChildNodes['Charges'] as IXMLChargesType;
end;

{ TXMLChargesType }

procedure TXMLChargesType.AfterConstruction;
begin
  RegisterChildNode('Charge', TXMLChargeType);
  ItemTag := 'Charge';
  ItemInterface := IXMLChargeType;
  inherited;
end;

function TXMLChargesType.Get_Charge(Index: Integer): IXMLChargeType;
begin
  Result := List[Index] as IXMLChargeType;
end;

function TXMLChargesType.Add: IXMLChargeType;
begin
  Result := AddItem(-1) as IXMLChargeType;
end;

function TXMLChargesType.Insert(const Index: Integer): IXMLChargeType;
begin
  Result := AddItem(Index) as IXMLChargeType;
end;

{ TXMLChargeType }

procedure TXMLChargeType.AfterConstruction;
begin
  RegisterChildNode('Type', TXMLTypeType);
  inherited;
end;

function TXMLChargeType.Get_Type_: IXMLTypeType;
begin
  Result := ChildNodes['Type'] as IXMLTypeType;
end;

function TXMLChargeType.Get_Value: string;
begin
  Result := ChildNodes['Value'].Text;
end;

{ TXMLTypeType }

function TXMLTypeType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

function TXMLTypeType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

{ TXMLLabelType }

function TXMLLabelType.Get_ImageType: string;
begin
  Result := ChildNodes['ImageType'].Text;
end;

function TXMLLabelType.Get_Image: IEvDualStream;
begin
  Result := TEvDualStreamHolder.CreateInMemory;
  if ChildNodes['Image'].Text <> '' then
  try
    DecodeToStream('$MIME$'+StringReplace(ChildNodes['Image'].Text, #$A, '', [rfReplaceAll]), Result.RealStream);
  finally
  end;
end;

end.