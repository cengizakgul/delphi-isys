unit SServiceDhl;

interface

uses Classes, XMLDoc, XMLIntf, SVmrClasses, SReportSettings,  Variants, SDhleCommerceFault,
  SDhleCommerceRequest, SDhlShippingKeyRequest, SDhlShipmentRequest, SDhlShippingKeyResponse,EvContext,
  SDhlVoidRequest, SDhlVoidResponse, SDhlShipmentResponse, DB, SVmrOptionEditFrame,
  SVmrDHLAddressedEditFrame, EvExceptions,ActiveX, EvClientDataSet;

type
  IBaseXMLECommerceType = interface({SDhleCommerceRequest.}IXMLECommerceType)
    ['{FD04EA78-ED2D-4098-91E0-0648268FD382}']
    //procedure InitDefaultRequestParams(const ID, Password: string);
    function GetResponseXML: string;
    procedure SetResponseXML(Value: string);
    property ResponseXML: string read GetResponseXML write SetResponseXML;
  end;
  IInitializedXMLECommerceType = interface(IBaseXMLECommerceType)
    ['{FD04EA78-ED2D-4098-91E0-0648268FD382}']
    procedure InitDefaultRequestParams(const ID, Password: string);
  end;
  TInitializedXMLECommerceType = class(SDhleCommerceRequest.TXMLECommerceType, IInitializedXMLECommerceType, SDhleCommerceRequest.IXMLECommerceType)
  private
    FResponseXML: string;
    function GetResponseXML: string;
    procedure SetResponseXML(Value: string);
  public
    procedure InitDefaultRequestParams(const ID, Password: string);
    property ResponseXML: string read GetResponseXML write SetResponseXML;
  end;
  IXMLECommerceRegister = interface(IBaseXMLECommerceType)
    ['{2FC5E101-E094-48FE-9614-4B5839F1FA99}']
    function GetShippingKey: SDhlShippingKeyRequest.IXMLRegisterType;
    property ShippingKey: SDhlShippingKeyRequest.IXMLRegisterType read GetShippingKey;
    procedure InitDefaultRequestParams(const ID, Password, AccountNbr, PostalCode: string);
  end;
  TXMLECommerceRegister = class(TInitializedXMLECommerceType, IXMLECommerceRegister)
  private
    function GetShippingKey: SDhlShippingKeyRequest.IXMLRegisterType;
  public
    procedure AfterConstruction; override;
    property ShippingKey: SDhlShippingKeyRequest.IXMLRegisterType read GetShippingKey;
    procedure InitDefaultRequestParams(const ID, Password, AccountNbr, PostalCode: string);
  end;
  IXMLECommerceShipment = interface(IBaseXMLECommerceType)
    ['{7B6C76EF-FF1A-4244-B545-09DADB5F9CAF}']
    function GetShipment: SDhlShipmentRequest.IXMLShipmentType;
    property Shipment: SDhlShipmentRequest.IXMLShipmentType read GetShipment;
    procedure InitDefaultRequestParams(const ID, Password, ShippingKey, AccountNbr: string;
      const ShippingDate: TDateTime; const ServiceCode: string; const Weight: Double;
      const ContentDescription, ShipperReference, SpecialServiceCode, SentByName,
      SentByPhone, AddressName, AddressAddr1, AddressAddr2, AddressCity, AddressState,
      AddressZip, AddressAttnTo, AddressPhone, TransactionTrace: string);
  end;
  TXMLECommerceShipment = class(TInitializedXMLECommerceType, IXMLECommerceShipment)
  private
    function GetShipment: SDhlShipmentRequest.IXMLShipmentType;
  public
    procedure AfterConstruction; override;
    property Shipment: SDhlShipmentRequest.IXMLShipmentType read GetShipment;
    procedure InitDefaultRequestParams(const ID, Password, ShippingKey, AccountNbr: string;
      const ShippingDate: TDateTime; const ServiceCode: string; const Weight: Double;
      const ContentDescription, ShipperReference, SpecialServiceCode, SentByName,
      SentByPhone, AddressName, AddressAddr1, AddressAddr2, AddressCity, AddressState,
      AddressZip, AddressAttnTo, AddressPhone, TransactionTrace: string);
  end;
  IXMLECommerceShipmentResponse = interface({SDhleCommerceRequest.}IXMLECommerceType)
    ['{B225A717-7A9C-4197-A817-BE0A85F58190}']
    function GetShipment: SDhlShipmentResponse.IXMLShipmentType;
    property Shipment: SDhlShipmentResponse.IXMLShipmentType read GetShipment;
  end;
  TXMLECommerceShipmentResponse = class(TInitializedXMLECommerceType, IXMLECommerceShipmentResponse)
  private
    function GetShipment: SDhlShipmentResponse.IXMLShipmentType;
  public
    procedure AfterConstruction; override;
    property Shipment: SDhlShipmentResponse.IXMLShipmentType read GetShipment;
  end;
  IXMLECommerceVoid = interface(IBaseXMLECommerceType)
    ['{574AE382-F97F-47C4-915B-7644B46D124A}']
    function GetShipment: SDhlVoidRequest.IXMLShipmentType;
    property Shipment: SDhlVoidRequest.IXMLShipmentType read GetShipment;
    procedure InitDefaultRequestParams(const ID, Password, ShippingKey, AccountNbr,
      AirBillNbr, TransactionTrace: string);
  end;
  TXMLECommerceVoid = class(TInitializedXMLECommerceType, IXMLECommerceVoid)
  private
    function GetShipment: SDhlVoidRequest.IXMLShipmentType;
  public
    procedure AfterConstruction; override;
    property Shipment: SDhlVoidRequest.IXMLShipmentType read GetShipment;
    procedure InitDefaultRequestParams(const ID, Password, ShippingKey, AccountNbr,
      AirBillNbr, TransactionTrace: string);
  end;
  IXMLECommerceVoidResponse = interface({SDhleCommerceRequest.}IXMLECommerceType)
    ['{0371926D-D535-4D20-85D5-92B77D60A606}']
    function GetShipment: SDhlVoidResponse.IXMLShipmentType;
    property Shipment: SDhlVoidResponse.IXMLShipmentType read GetShipment;
  end;
  TXMLECommerceVoidResponse = class(TInitializedXMLECommerceType, IXMLECommerceVoidResponse)
  private
    function GetShipment: SDhlVoidResponse.IXMLShipmentType;
  public
    procedure AfterConstruction; override;
    property Shipment: SDhlVoidResponse.IXMLShipmentType read GetShipment;
  end;

  TVmrDhlService = class(TVmrAccountedService)
  protected
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
    function ExecuteSOAPRequest(const Request: IBaseXMLECommerceType; const ResponseClass: TClass): IXMLNode;
    procedure SaveApiFiles(Request: IBaseXMLECommerceType;FileNameTemplate:string;ForceSave:boolean = False);
  public
    function ApiIsActive: Boolean;
    procedure ApiTestCredentials;
    function ApiSendDummyPackage: IXMLECommerceShipmentResponse;
    function ApiGenerateCertificationXML: string;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
    function VoidPackage(const AirBill: string): IXMLECommerceVoidResponse;
    function ShipPackage(const ServiceCode: string; const Weight: Double;
      const ContentDescription, ShipperReference, SpecialServiceCode,
      AddressName, AddressAddr1, AddressAddr2, AddressCity, AddressState,
      AddressZip, AddressAttnTo, AddressPhone, TransactionTrace: string): IXMLECommerceShipmentResponse;
    function NewECommerceRequest: IInitializedXMLECommerceType;
    function NewXMLECommerceRegister: IXMLECommerceRegister;
    function NewXMLECommerceShipment: IXMLECommerceShipment;
    function NewXMLECommerceVoid: IXMLECommerceVoid;
  end;

  TVmrDhlDeliveryMethod = class(TVmrAddressedDeliveryMethod)
  private
    dWeight: Double;
    sAirBill: string;
    oDhlService: TVmrDhlService;
  protected
    procedure AddDHLLabel;
    class function GetServiceCode: string; virtual; abstract;
    class function GetSpecialServiceCode: string; virtual; abstract;
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public


    class function GetMediaClasses: TVmrMediaTypeClassArray; override;
    function OnPickupSheetScan(cd: TEvClientDataset): string; override;
    class function GetShippedMessage(cd: TEvClientDataSet): string; override;
    procedure Release(const cd: TEvClientDataSet); override;
    procedure RevertToUnreleased(const Media: TVmrMediaType; const cd:
            TEvClientDataSet); override;

     function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;

  end;

  TVmrDhlGround = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrDhlExpress1030am = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;


  TVmrDhlExpressSaturday = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrDhlNextAfternoon = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrDhlExpress1200pm = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrDhlSecondDay5pm = class(TVmrDhlDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;


implementation

uses EvUtils, Math, SysUtils, EvTypes, SDhlServiceOptionFrame, SOAPHTTPTrans, EvStreamUtils,
  DateUtils, SDataStructure, EvConsts, StrUtils, EvCommonInterfaces;

function TVmrDhlService.NewECommerceRequest: IInitializedXMLECommerceType;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TInitializedXMLECommerceType, TargetNamespace) as IInitializedXMLECommerceType;
end;

function TVmrDhlService.NewXMLECommerceRegister: IXMLECommerceRegister;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TXMLECommerceRegister, TargetNamespace) as IXMLECommerceRegister;
end;

function TVmrDhlService.NewXMLECommerceShipment: IXMLECommerceShipment;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TXMLECommerceShipment, TargetNamespace) as IXMLECommerceShipment;
end;

function TVmrDhlService.NewXMLECommerceVoid: IXMLECommerceVoid;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TXMLECommerceVoid, TargetNamespace) as IXMLECommerceVoid;
end;

{
******************************** TVmrDhlService ********************************
}

function TVmrDhlService.ApiGenerateCertificationXML: string;
var
  l: TStringList;
  ShippingRequest: IXMLECommerceShipment;
  VoidingRequest: IXMLECommerceVoid;
  Thursday: TDateTime;
  aAirBill: string;
  UserName,UserPhone:string;

  function ProcessShippingTest(TestName:string): IXMLECommerceShipmentResponse;
  begin
    Result := ExecuteSOAPRequest(ShippingRequest, TXMLECommerceShipmentResponse) as IXMLECommerceShipmentResponse;
    {
    if Result.Shipment.Faults.Count = 0 then
      Result.Shipment.Label_.Image.SaveToFile('c:\'+ Result.Shipment.ShipmentDetail.AirbillNbr+ '.png');
    l.Append(ShippingRequest.XML);
    l.Append('');
    l.Append(ShippingRequest.ResponseXML);
    l.Append('');
    l.Append('***********************************************');
    }
    SaveApiFiles(ShippingRequest,'"Cert Ship Test # "'+'"'+TestName+'"',true);
  end;
  function ProcessVoidingTest(TestName:string): IXMLECommerceVoidResponse;
  begin
    Result := ExecuteSOAPRequest(VoidingRequest, TXMLECommerceVoidResponse) as IXMLECommerceVoidResponse;
    {
    l.Append(VoidingRequest.XML);
    l.Append('');
    l.Append(VoidingRequest.ResponseXML);
    l.Append('');
    l.Append('***********************************************');
    }
    SaveApiFiles(VoidingRequest,'"Cert Void Test # "'+'"'+TestName+'"',true);
  end;
begin
  Thursday := Today;
  while DayOfWeek(Thursday) <> 5 do
    Thursday := Thursday+ 1;
  l := TStringList.Create;

  UserName := Context.UserAccount.FirstName+ ' ' + Context.UserAccount.LastName;
  UserPhone :=  '802-5555555';

  try
    CoInitialize(nil);
    // Express Service with Declared Value and shipment type of Letter
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'E', 0.4,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Code := 'AP';
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Value := 333;
    aAirBill := ProcessShippingTest('01').Shipment.ShipmentDetail.AirbillNbr; // for void test

    // Express Saturday Service (shipped on Friday)
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday+1, 'E', 0.4,
      'Payroll checks and reports', 'DummyTest', 'SAT',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ProcessShippingTest('02');

    // Express Saturday (shipped not on Friday)
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'E', 0.4,
      'Payroll checks and reports', 'DummyTest', 'SAT',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
//    ShippingRequest.Shipment.ShipmentProcessingInstructions.Overrides.Override.Code := 'ES';
    ProcessShippingTest('03');

    // Express 10:30am
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'E', 0.4,
      'Payroll checks and reports', 'DummyTest', '1030',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ProcessShippingTest('04');

    // Second Day Service, weight over 20lb, Asset Protection, Bill To Receiver
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'S', 21,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Code := 'AP';
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Value := 333;
    ShippingRequest.Shipment.Billing.Party.Code := 'R';
    ShippingRequest.Shipment.Billing.AccountNbr := 32423442;
    ProcessShippingTest('05');

    // offshore destination (Alaska or Hawaii)
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'S', 1,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1st St & Airport Rd', '', 'Holy Cross',
      'AK', '99602', 'Joe', '907-5555555', 'DummyTrace');
    ProcessShippingTest('06');

    // Bill to 3rd party, Zip override, Notification Message
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'S', 21,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '45401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.Billing.Party.Code := '3';
    ShippingRequest.Shipment.Billing.AccountNbr := 32423442;
    ShippingRequest.Shipment.ShipmentProcessingInstructions.Overrides.Override.Code := 'Z1';
    ShippingRequest.Shipment.ShipmentProcessingInstructions.Notification.Message := 'Your payroll is coming!';
    ShippingRequest.Shipment.ShipmentProcessingInstructions.Notification.Notify.EmailAddress := 'dummysb@att.com';
    ProcessShippingTest('07');

        // Shipment to non-existing zip 00001
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'S', 21,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '00001', 'Joe', '802-5555555', 'DummyTrace');
//    ShippingRequest.Shipment.ShipmentProcessingInstructions.Overrides.Override.Code := 'Z2';
    ProcessShippingTest('08');

    // Letter with declared value of $1000
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'E', 0.4,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Code := 'AP';
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Value := 1000.00;
    ProcessShippingTest('09');

    // COD with HAZ
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'G', 1,
      'Payroll checks and reports', 'DummyTest', 'HAZ',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.Billing.CODPayment.Code := 'P';
    ShippingRequest.Shipment.Billing.CODPayment.Value := 45;
    ProcessShippingTest('10');

    // ShippingRequest with invalid account number
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      '123', Thursday, 'G', 1,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ProcessShippingTest('11');

    // Package with Asset Protection declared value of 100,000 and dimentions pf 45x56x100
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'E', 1,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Code := 'AP';
    ShippingRequest.Shipment.ShipmentDetail.AdditionalProtection.Value := 100000.00;
    ShippingRequest.Shipment.ShipmentDetail.Dimensions.Length := 100;
    ShippingRequest.Shipment.ShipmentDetail.Dimensions.Width := 45;
    ShippingRequest.Shipment.ShipmentDetail.Dimensions.Height := 56;
    ProcessShippingTest('12');

    // Ground Service
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'G', 1,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ProcessShippingTest('13');
    // HAZ
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'G', 1,
      'Payroll checks and reports', 'DummyTest', 'HAZ',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ProcessShippingTest('14');
    // COD
    ShippingRequest := NewXMLECommerceShipment;
    ShippingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Thursday, 'G', 1,
      'Payroll checks and reports', 'DummyTest', '',
      UserName, UserPhone, 'Dummy Bureau', '1 Main St', '', 'Burlington',
      'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
    ShippingRequest.Shipment.Billing.CODPayment.Code := 'P';
    ShippingRequest.Shipment.Billing.CODPayment.Value := 44.55;
    ProcessShippingTest('15');


    // void API generated bill
    VoidingRequest := NewXMLECommerceVoid;
    VoidingRequest.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], aAirBill, 'DummyTrace');
    ProcessVoidingTest('01');
    Result := 'Certification files have been saved to C:\DHL\'+FormatDateTime('YYYY_MMM_DD',Now);
    //l.Text;
  finally
    CoUnInitialize;
    l.Free;
  end;
end;

function TVmrDhlService.ApiIsActive: Boolean;
begin
  Result := not VarIsNull(Options['API_LOGIN']) and not VarIsNull(Options['API_PASSWORD'])
    and not VarIsNull(Options['API_KEY']) and not VarIsNull(Options['ACC_NUMBER'])
    and (Options.FieldByName('API_LIVE').AsString[1] in ['Y','T']);
end;

function TVmrDhlService.ApiSendDummyPackage: IXMLECommerceShipmentResponse;
begin
  Result := ShipPackage(TVmrDhlGround.GetServiceCode, 0.4,
    'Payroll checks and reports', 'DummyTest', TVmrDhlGround.GetSpecialServiceCode,
    'Dummy Bureau', '1 Main St', '', 'Burlington',
    'VT', '05401', 'Joe', '802-5555555', 'DummyTrace');
end;

procedure TVmrDhlService.ApiTestCredentials;
var
  Request: IInitializedXMLECommerceType;
begin
  try
    CoInitialize(nil);

    Request := NewECommerceRequest;
    Request.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD']);
    ExecuteSOAPRequest(Request, nil);
    SaveApiFiles(Request,'"ApiTest" HH_MM_SS');
  finally
    CoUnInitialize;
  end;
end;

procedure TVmrDhlService.SaveApiFiles(Request: IBaseXMLECommerceType;FileNameTemplate:string;ForceSave:boolean = False);
var
  FileName:string;
  FileDir:string;
  FS:TFileStream;
  SS:TStringStream;
  S: IXMLECommerceShipmentResponse;
begin
  if (Options['DHL_SAVE_API_FILES'] <> 'Y') and (not ForceSave) then exit;
  //'mm_dd_yyyy__hh_mm_ss_z'
  FileName :=Trim(FormatDateTime(FileNameTemplate,now));
  FileName := StringReplace(FileName,'\','_',[rfReplaceAll]);
  FileName := StringReplace(FileName,'/','_',[rfReplaceAll]);
  FileName := StringReplace(FileName,':','_',[rfReplaceAll]);
  FileDir := Trim(FormatDateTime('"C:\DHL\"YYYY_MMM_DD',now));
  ForceDirectories(FileDir);


  FS := TFileStream.Create(FileDir+'\'+FileName+' Req.XML',fmCreate);
  SS := TStringStream.Create(Request.XML);
  try
    FS.CopyFrom(SS,SS.Size);
  finally
    FreeAndNil(SS);
    FreeAndNil(FS);
  end;
  SS := TStringStream.Create(Request.ResponseXML);
  FS := TFileStream.Create(FileDir+'\'+FileName+' Res.XML',fmCreate);
  try
    FS.CopyFrom(SS,SS.Size);
  finally
    FreeAndNil(SS);
    FreeAndNil(FS);
  end;
  S := LoadXMLData(Request.ResponseXML).GetDocBinding('eCommerce',TXMLECommerceShipmentResponse, TargetNamespace) as IXMLECommerceShipmentResponse;
  if S.Shipment.Label_.Image.Size >0 then
    S.Shipment.Label_.Image.SaveToFile(FileDir+'\'+FileName+' Label.png');
end;


function TVmrDhlService.ExecuteSOAPRequest(
  const Request: IBaseXMLECommerceType;
  const ResponseClass: TClass): IXMLNode;
var
  rr: THTTPReqResp;
  s: TStringStream;
  fr: SDhleCommerceFault.IXMLECommerceType;
begin
  rr := THTTPReqResp.Create(nil);
  try
    rr.InvokeOptions := rr.InvokeOptions+ [soIgnoreInvalidCerts];
    rr.UseUTF8InHeader := False;
    if Options['API_LIVE'] = 'Y' then
      rr.URL := 'https://eCommerce.airborne.com/ApiLanding.asp'
    else
      rr.URL := 'https://ecommerce.airborne.com/ApiLandingTest.asp';
    s := TStringStream.Create('');
    rr.Execute(Request.XML, s);
    Request.ResponseXML := s.DataString;
    fr := LoadXMLData(s.DataString).GetDocBinding('eCommerce', SDhleCommerceFault.TXMLECommerceType, TargetNamespace) as SDhleCommerceFault.IXMLECommerceType;
    if fr.Faults.Count > 0 then
      raise EInconsistentData.CreateHelp(fr.Faults[0].Description, IDH_InconsistentData);
    if ResponseClass = nil then
      Result := nil
    else
      Result := LoadXMLData(s.DataString).GetDocBinding('eCommerce', ResponseClass, TargetNamespace);
  finally
    rr.Free;
  end;
end;

class function TVmrDhlService.GetMethodClasses: TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 5);
  Result[0] := TVmrDhlGround;
  Result[1] := TVmrDhlExpress1030am;
  Result[2] := TVmrDhlExpress1200pm;
  Result[3] := TVmrDhlNextAfternoon;
  Result[4] := TVmrDhlSecondDay5pm;
 // Result[5] := TVmrDhlExpressSaturday
end;

{ TExtendedXMLECommerceType }


function TInitializedXMLECommerceType.GetResponseXML: string;
begin
  Result := FResponseXML;
end;

procedure TInitializedXMLECommerceType.InitDefaultRequestParams(const ID, Password: string);
begin
  Set_Version('1.1');
  Set_Action('Request');
  Get_Requestor.ID := ID;
  Get_Requestor.Password := Password;
end;

{ TXMLECommerceRegister }

procedure TXMLECommerceRegister.AfterConstruction;
begin
  RegisterChildNode('Register', SDhlShippingKeyRequest.TXMLRegisterType);
  inherited;
end;

function TXMLECommerceRegister.GetShippingKey: SDhlShippingKeyRequest.IXMLRegisterType;
begin
  Result := ChildNodes['Register'] as SDhlShippingKeyRequest.IXMLRegisterType;
end;

procedure TXMLECommerceRegister.InitDefaultRequestParams(const ID, Password, AccountNbr, PostalCode: string);
begin
  inherited InitDefaultRequestParams(ID, Password);
  ShippingKey.Version := '1.0';
  ShippingKey.Action := 'ShippingKey';
  ShippingKey.AccountNbr.Text := AccountNbr;
  ShippingKey.PostalCode.Text := PostalCode;
end;

{ TXMLECommerceShipment }

procedure TXMLECommerceShipment.AfterConstruction;
begin
  RegisterChildNode('Shipment', SDhlShipmentRequest.TXMLShipmentType);
  inherited;
end;

function TXMLECommerceShipment.GetShipment: SDhlShipmentRequest.IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as SDhlShipmentRequest.IXMLShipmentType;
end;

procedure TXMLECommerceShipment.InitDefaultRequestParams(const ID,
  Password, ShippingKey, AccountNbr: string; const ShippingDate: TDateTime;
  const ServiceCode: string; const Weight: Double;
  const ContentDescription, ShipperReference, SpecialServiceCode,
  SentByName, SentByPhone, AddressName, AddressAddr1, AddressAddr2,
  AddressCity, AddressState, AddressZip, AddressAttnTo,
  AddressPhone, TransactionTrace: string);
begin
  inherited InitDefaultRequestParams(ID, Password);
  Shipment.Set_Version('1.0');
  Shipment.Set_Action('GenerateLabel');
  Shipment.ShippingCredentials.ShippingKey := ShippingKey;
  Shipment.ShippingCredentials.AccountNbr := StrToIntDef(AccountNbr, 0);
  Shipment.ShipmentDetail.ShipDate := FormatDateTime('YYYY-MM-DD', ShippingDate);
  Shipment.ShipmentDetail.Service.Code := ServiceCode;
  if (Weight <= 0.5) and (ServiceCode <> 'G') then
    Shipment.ShipmentDetail.ShipmentType.Code := 'L'
  else
  begin
    Shipment.ShipmentDetail.ShipmentType.Code := 'P';
    Shipment.ShipmentDetail.Weight := Ceil(Weight);
  end;
  Shipment.ShipmentDetail.ContentDesc := ContentDescription;
  Shipment.ShipmentDetail.ShipperReference := ShipperReference;
  //Shipment.ShipmentDetail.AdditionalProtection.Code := 'NR';
  if SpecialServiceCode <> '' then
    Shipment.ShipmentDetail.SpecialServices.SpecialService.Code := SpecialServiceCode;
  Shipment.Billing.Party.Code := 'S';
  Shipment.Sender.SentBy := SentByName;
  Shipment.Sender.PhoneNbr := SentByPhone;
  Shipment.Receiver.Address.CompanyName := AddressName;
  Shipment.Receiver.Address.Street := AddressAddr1;
  Shipment.Receiver.Address.StreetLine2 := AddressAddr2;
  Shipment.Receiver.Address.City := AddressCity;
  Shipment.Receiver.Address.State := AddressState;
  Shipment.Receiver.Address.PostalCode := AddressZip;
  Shipment.Receiver.Address.Country := 'US';
  Shipment.Receiver.AttnTo := AddressAttnTo;
  Shipment.Receiver.PhoneNbr := AddressPhone;
  Shipment.ShipmentProcessingInstructions.Label_.ImageType := 'PNG';
  //Shipment.ShipmentProcessingInstructions.Label_.ImageType := 'GIF';
  Shipment.TransactionTrace.Text := TransactionTrace;
end;

{ TXMLECommerceVoid }

procedure TXMLECommerceVoid.AfterConstruction;
begin
  RegisterChildNode('Shipment', SDhlVoidRequest.TXMLShipmentType);
  inherited;
end;

function TXMLECommerceVoid.GetShipment: SDhlVoidRequest.IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as SDhlVoidRequest.IXMLShipmentType;
end;

procedure TXMLECommerceVoid.InitDefaultRequestParams(const ID, Password,
  ShippingKey, AccountNbr, AirBillNbr, TransactionTrace: string);
begin
  inherited InitDefaultRequestParams(ID, Password);
  Shipment.Set_Version('1.0');
  Shipment.Set_Action('Void');
  Shipment.ShippingCredentials.ShippingKey := ShippingKey;
  Shipment.ShippingCredentials.AccountNbr := StrToIntDef(AccountNbr, 0);
  Shipment.ShipmentDetail.AirbillNbr := AirBillNbr;
  Shipment.TransactionTrace := TransactionTrace;
end;

{ TXMLECommerceShipmentResponse }

procedure TXMLECommerceShipmentResponse.AfterConstruction;
begin
  RegisterChildNode('Shipment', SDhlShipmentResponse.TXMLShipmentType);
  inherited;
end;

function TXMLECommerceShipmentResponse.GetShipment: SDhlShipmentResponse.IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as SDhlShipmentResponse.IXMLShipmentType;
end;

procedure TInitializedXMLECommerceType.SetResponseXML(Value: string);
begin
  FResponseXML := Value;
end;

{ TXMLECommerceVoidResponse }

procedure TXMLECommerceVoidResponse.AfterConstruction;
begin
  RegisterChildNode('Shipment', SDhlVoidResponse.TXMLShipmentType);
  inherited;
end;

function TXMLECommerceVoidResponse.GetShipment: SDhlVoidResponse.IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as SDhlVoidResponse.IXMLShipmentType;
end;


{
**************************** TVmrDhlDeliveryMethod *****************************
}

procedure TVmrDhlDeliveryMethod.PrepareExtraOptionFields(
  var Prefix: string; var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited;
  f.Add('PHONE', ftString, 30);
  f.Find('NAME').Size := 35;
  f.Find('ADDRESS1').Size := 35;
  f.Find('ADDRESS2').Size := 35;
  f.Find('CITY').Size := 35;
end;

class function TVmrDhlDeliveryMethod.GetMediaClasses: TVmrMediaTypeClassArray;
begin
  SetLength(Result, 2);
  Result[0] := TVmrSeparatedPaperMediaType;
  Result[1] := TVmrGroupedPaperMediaType;
end;

class function TVmrDhlDeliveryMethod.GetShippedMessage(cd: TEvClientDataSet):string;
begin
  Result := inherited GetShippedMessage(cd);
  if not VarIsNull(cd['TRACKING_INFO']) then
    Result := Result+ #13'The tracking number for this shipment is '+ cd['TRACKING_INFO']+'. '+#13+
      'You can track your shipment at www.dhl.com or use this link: '+
      #13'http://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber='+ cd['TRACKING_INFO']+
      #13;
end;

function TVmrDhlDeliveryMethod.OnPickupSheetScan(
  cd: TEvClientDataset): string;
begin
  Result := 'Put all pages in DHL letter envelope and affix printed DHL label';
end;

procedure TVmrDhlDeliveryMethod.AddDHLLabel;
var
  Response: IXMLECommerceShipmentResponse;

  s: string;
  i,k: Integer;
  P: TrwRepParams;
  ms: IisStream;
  RR: TrwReportResults;
  R : TrwReportResult;
  RL2:TrwReportResults;
  Job: TVmrPrintJob;

  function FindPrinterIndexForDHLLabel: Integer;
  var
    i: Integer;
  begin
    Result := -1;
    for i := 0 to PrInfo.Count - 1 do
      if PrInfo[i].Active  and ((PrInfo[i].Location = '') or AnsiSameText(PrInfo[i].Location,Location)) then
        if PrInfo[i].FindBinByMediaType(MEDIA_TYPE_DHL_LABEL) <> nil then
        begin
           Result := i;
           break;
        end;

    if Result = -1 then
      raise EPrinterNotDefined.Create('Could not find a printer for media type: DHL Label #1239');
  end;

begin
  k := -1;
  if (Length(RLA) = 0) or not oDhlService.ApiIsActive then
    exit;

  for i:=0 to High(RLA) do
  begin
     Job := RLA[i];
     if VarIsNull(RLA[i].SbUpLevelMailBoxNbr) and (PrInfo[RLA[i].PrinterIndex].FindBinByMediaType(MEDIA_TYPE_DHL_LABEL) <> nil) then
     begin
       k := i;
       break;
     end;
  end;

  if k = -1 then // we need to Add New Part for DHL label
  begin
     k:=FindPrinterIndexForDHLLabel;

     RL2 := TrwReportResults.Create;
     RL2.PrinterName := PrInfo[k].Name;
     SetLength(RLA, Length(RLA)+1);
     i := High(RLA);

     RLA[i].PrinterIndex := k;
     RLA[i].Res := RL2;
     RLA[i].SbMailBoxNbr := RLA[0].SbMailBoxNbr;
     RLA[i].SbUpLevelMailBoxNbr := RLA[0].SbUpLevelMailBoxNbr;
     RLA[i].ClNbr := RLA[0].ClNbr;
     RLA[i].CoNbr := Null;
     RLA[i].PrNbr := Null;
     RLA[i].PartNumber := RLA[0].PartCount+1;
     RLA[i].PartCount := RLA[0].PartCount;
     RLA[i].BarCode := RLA[0].BarCode;
     RLA[i].SbContentNbrs := VarArrayCreate([0, RLA[i].Res.Count-1], varVariant);
     RLA[i].Description := RLA[0].Description;
     RLA[i].cd := RLA[0].cd;
     k := i;
     for i := 0 to High(RLA) do
     begin
       if RLA[i].SbMailBoxNbr = RLA[0].SbMailBoxNbr then
          RLA[i].PartCount := RLA[i].PartCount +1;
     end;
  end;
  if k <>-1 then
  begin
    Job := RLA[k];
    dWeight := CalcWeight;
    if dWeight = 0 then
      raise EInconsistentData.CreateHelp('DHL API is active but paper weight is zero', IDH_InconsistentData);
    Response := oDhlService.ShipPackage(GetServiceCode, dWeight* 1.05, 'Payroll',
      IntToStr(BoxNbr), GetSpecialServiceCode,
      VarToStr(ExtraOptions['NAME']), VarToStr(ExtraOptions['ADDRESS1']), VarToStr(ExtraOptions['ADDRESS2']),
      VarToStr(ExtraOptions['CITY']), VarToStr(ExtraOptions['STATE']), VarToStr(ExtraOptions['ZIP']),
      VarToStr(ExtraOptions['NOTE']), VarToStr(ExtraOptions['PHONE']), IntToStr(BoxNbr));
    if Response.Shipment.Faults.Count > 0 then
    begin
      s := '';
      for i := 0 to Response.Shipment.Faults.Count-1 do
        s := s+ #13+ Response.Shipment.Faults[i].Desc;
      raise EInconsistentData.CreateHelp('DHL API returned error: '+ s, IDH_InconsistentData);
    end;
    sAirBill := Response.Shipment.ShipmentDetail.AirbillNbr;
    Job.cd.Edit;
    Job.cd['TRACKING_INFO'] := Response.Shipment.ShipmentDetail.AirbillNbr;
    Job.cd['COST'] := Response.Shipment.ShipmentDetail.RateEstimate.TotalChargeEstimate;
    Job.cd.Post;

    P :=inherited GetPickupSheetReportParams(Job);
    P.NBR := VarToInt(oDhlService.Options['DHL_LABEL_REPORT_NBR']);
    P.Params.Add('PNG_PICTURE', Response.Shipment.Label_.Image.AsVarArrayOfBytes);
    P.Params.Add('Weight', dWeight);

    if Assigned(P) then
    begin
      with ctx_RWLocalEngine do
      begin
        StartGroup;
        CalcPrintReport(P);
        ms := EndGroup(rdtNone);
      end;
      RR := TrwReportResults.Create(ms);
      try
        if RR.Count > 0 then
        begin
          if Assigned(RR[0]) then
          begin
            if RR[0].ErrorMessage <> '' then
               raise EMissingReport.CreateHelp('DHL Label report failed with error: '+ RR[0].ErrorMessage, IDH_MissingReport);
            R := RR[0];
            R.Collection := Job.Res;
            R.Index := 0;
            R.Tray := PrInfo[Job.PrinterIndex].FindBinNameByMediaType(MEDIA_TYPE_DHL_LABEL);
            if R.Tray = '' then
               raise EPrinterNotDefined.CreateFmt('Printer %s does not have DHL Label tray assigned', [PrInfo[Job.PrinterIndex].Name]);
          end;
        end;
      finally
        RR.Free;
      end;
    end;
  end;
end;

procedure TVmrDhlDeliveryMethod.Release(const cd: TEvClientDataSet);
var
i:integer;
begin
  if Length(RLA) = 0 then Exit;
  oDhlService := CreateSbDeliveryService(cd) as TVmrDhlService;
  try
    sAirBill := '';
    try
      AddDHLLabel;
      inherited;
    except
      try
        for i:=0 to High(RLA) do
        begin
           try
             if not VarIsNull(RLA[i].cd['TRACKING_INFO']) then
             begin
               oDhlService.VoidPackage(RLA[i].cd['TRACKING_INFO']);
               RLA[i].cd.Edit;
               RLA[i].cd['TRACKING_INFO'] := Null;
               RLA[i].cd['COST'] := Null;
               RLA[i].cd.Post;
             end;
           finally
           end;
        end;
      except
      end;
      raise;
    end;
  finally
    FreeAndNil(oDhlService);
  end;
end;

procedure TVmrDhlDeliveryMethod.RevertToUnreleased(
  const Media: TVmrMediaType; const cd: TEvClientDataSet);
begin
  if not VarIsNull(cd['TRACKING_INFO']) then
  begin
    oDhlService := CreateSbDeliveryService(cd) as TVmrDhlService;
    try
      if oDhlService.ApiIsActive then
      begin
        oDhlService.VoidPackage(cd['TRACKING_INFO']);
        cd.Edit;
        cd['TRACKING_INFO'] := Null;
        cd['COST'] := Null;
        cd.Post;
      end;
    finally
      FreeAndNil(oDhlService);
    end;
  end;
  inherited;
end;


{ TVmrDhlGround }

class function TVmrDhlGround.GetServiceCode: string;
begin
  Result := 'G';
end;

class function TVmrDhlGround.GetSpecialServiceCode: string;
begin
  Result := '';
end;

{ TVmrDhlNextAfternoon}

class function TVmrDhlNextAfternoon.GetServiceCode: string;
begin
  Result := 'N';
end;

class function TVmrDhlNextAfternoon.GetSpecialServiceCode: string;
begin
  Result := '';
end;

{ TVmrDhlExpress1030am }

class function TVmrDhlExpress1030am.GetServiceCode: string;
begin
  Result := 'E';
end;

class function TVmrDhlExpress1030am.GetSpecialServiceCode: string;
begin
  Result := '1030';
end;

{ TVmrDhlExpress1200pm }

class function TVmrDhlExpress1200pm.GetServiceCode: string;
begin
  Result := 'E';
end;

class function TVmrDhlExpress1200pm.GetSpecialServiceCode: string;
begin
  Result := '';
end;

{ TVmrDhlExpressSaturday }

class function TVmrDhlExpressSaturday.GetServiceCode: string;
begin
  Result := 'E';
end;

class function TVmrDhlExpressSaturday.GetSpecialServiceCode: string;
begin
  Result := 'SAT';
end;

{ TVmrDhlSecondDay5pm }

class function TVmrDhlSecondDay5pm.GetServiceCode: string;
begin
  Result := 'S';
end;

class function TVmrDhlSecondDay5pm.GetSpecialServiceCode: string;
begin
  Result := '';
end;


function TVmrDhlService.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TDhlServiceOptionFrame;
end;

procedure TVmrDhlService.PrepareOptionFields(var LoadRecordCount: Integer;
  const f: TFieldDefs);
begin
  inherited;
  f.Add('API_LOGIN', ftString, 40);
  f.Add('API_PASSWORD', ftString, 40);
  f.Add('API_KEY', ftString, 128);
  f.Add('API_LIVE', ftString, 1);
  f.Add('DHL_LABEL_REPORT_NBR', ftString,20);
  f.Add('DHL_SAVE_API_FILES', ftString,1);
end;


function TVmrDhlService.ShipPackage(const ServiceCode: string;
  const Weight: Double; const ContentDescription, ShipperReference,
  SpecialServiceCode, AddressName, AddressAddr1, AddressAddr2, AddressCity,
  AddressState, AddressZip, AddressAttnTo, AddressPhone,
  TransactionTrace: string): IXMLECommerceShipmentResponse;
var
  Request: IXMLECommerceShipment;
begin
  try
    CoInitialize(nil);
    DM_SERVICE_BUREAU.SB.Activate;
    Request := NewXMLECommerceShipment;
    Request.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], Today, ServiceCode, Weight, ContentDescription, ShipperReference,

    SpecialServiceCode,

    Context.UserAccount.FirstName + ' '+ Context.UserAccount.LastName,
    DM_SERVICE_BUREAU.SB['PHONE'],
    AddressName, AddressAddr1, AddressAddr2, AddressCity, AddressState,LeftStr(AddressZip,5), AddressAttnTo,
    AddressPhone, TransactionTrace);
    Result := ExecuteSOAPRequest(Request, TXMLECommerceShipmentResponse) as IXMLECommerceShipmentResponse;
    if Result.Shipment.Faults.Count > 0 then
      SaveApiFiles(Request,Format('"ShipPkg # %s" HH_MM_SS ',['']))
    else
      SaveApiFiles(Request,Format('"ShipPkg # %s" HH_MM_SS ',[ConvertNull(Result.Shipment.ShipmentDetail.AirbillNbr,'')]));

    if Result.Shipment.Faults.Count > 0 then
      raise EInconsistentData.CreateHelp(Result.Shipment.Faults[0].Desc, IDH_InconsistentData);
  finally
     CoUnInitialize;
  end;
end;

function TVmrDhlService.VoidPackage(
  const AirBill: string): IXMLECommerceVoidResponse;
var
  Request: IXMLECommerceVoid;
begin
  try
    CoInitialize(nil);
    Request := NewXMLECommerceVoid;
    Request.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD'], Options['API_KEY'],
      Options['ACC_NUMBER'], AirBill, '');
    Result := ExecuteSOAPRequest(Request, TXMLECommerceVoidResponse) as IXMLECommerceVoidResponse;
      SaveApiFiles(Request,Format('"VoidPkg # %s" HH_MM_SS ',[Airbill]));
    if Result.Shipment.Faults.Count > 0 then
      raise EInconsistentData.CreateHelp(Result.Shipment.Faults[0].Desc, IDH_InconsistentData);
  finally
       CoUnInitialize;
  end;
end;



function TVmrDhlDeliveryMethod.GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
   Result := TVmrDhlAddressedEditFrame;
end;

end.

