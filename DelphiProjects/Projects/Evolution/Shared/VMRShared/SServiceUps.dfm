inherited ServiceUps: TServiceUps
  object evLabel1: TevLabel [0]
    Left = 16
    Top = 16
    Width = 85
    Height = 13
    Caption = 'UPS Account No.'
  end
  object evDBEdit1: TevDBEdit [1]
    Left = 128
    Top = 16
    Width = 137
    Height = 21
    DataField = 'ACC_NUMBER'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
  end
end
