// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SServicePickup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, DBCtrls, SReportSettings, ISBasicClasses,
  SVmrRealDeliveryEditFrame, Mask, wwdbedit, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TServicePickupFrame = class (TVmrRealDeliveryEditFrame)
    EvDBMemo1: TEvDBMemo;
    evLabel122: TevLabel;
  end;
  
  TVmrCourierPickupMethod = class (TVmrPackagedDeliveryMethod)
  protected
    function GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams; override;
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount: 
            Integer; const f: TFieldDefs); override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    class function OnPickup(cd: TEvClientDataSet): Boolean; override;
    function OnPickupSheetScan(cd: TEvClientDataset): string; override;
    procedure PopulateNewMailBox; override;
  end;
  
  TVmrCourierPickupService = class (TVmrDeliveryService)
  public
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
  end;
  
implementation

{$R *.dfm}

uses SDataStructure, EvUtils;

{ TVmrCourierPickupService }

{
*************************** TVmrCourierPickupMethod ****************************
}
function TVmrCourierPickupMethod.GetExtraOptionEditFrameClass: 
        TVmrOptionEditFrameClass;
begin
  Result := TServicePickupFrame;
end;

function TVmrCourierPickupMethod.GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams;
begin
  Result := inherited GetPickupSheetReportParams(Job);
  Result.NBR := 761;
  Result.Params.Add('Addr0', '');
  Result.Params.Add('Addr1', 'FOR PICKUP');
  Result.Params.Add('Addr2', Description);
  Result.Params.Add('Addr3', '');
end;

class function TVmrCourierPickupMethod.OnPickup(cd: TEvClientDataSet): Boolean;
var
  dm: TVmrDeliveryMethod;
begin
  dm := InstanceSbDeliveryMethod(cd);
  try
    Result := EvMessage('Check this information:'#13#13+
      dm.ExtraOptions.FieldByName('COURIER_INFO').AsString+
      #13#13'Is everything as should be?', mtConfirmation, [mbYes, mbNo], mbNo) = mrYes;
  finally
    dm.Free;
  end;
end;

function TVmrCourierPickupMethod.OnPickupSheetScan(cd: TEvClientDataset): 
        string;
begin
  Result := 'Put all pages in a pickup envelope and put adhesive barcode label on it';
end;

procedure TVmrCourierPickupMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := ExtraOptions.FieldByName('COURIER_INFO').AsString;
end;

procedure TVmrCourierPickupMethod.PrepareExtraOptionFields(var Prefix: string; 
        var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('COURIER_INFO', ftString, 512);
end;

{
*************************** TVmrCourierPickupService ***************************
}
class function TVmrCourierPickupService.GetMethodClasses: 
        TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 1);
  Result[0] := TVmrCourierPickupMethod;
end;

initialization
  RegisterVmrClass(TVmrCourierPickupService);
  RegisterVmrClass(TVmrCourierPickupMethod);

end.


