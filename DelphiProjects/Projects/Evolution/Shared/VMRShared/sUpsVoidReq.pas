
{********************************************************************************************************}
{                                                                                                        }
{                                            XML Data Binding                                            }
{                                                                                                        }
{         Generated on: 6/9/2009 2:30:38 PM                                                              }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\voidreq2.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\voidreq2.xdb   }
{                                                                                                        }
{********************************************************************************************************}

unit sUpsVoidReq;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUpsVoidShipmentRequestType = interface;
  IXMLRequestType = interface;
  IXMLTransactionReferenceType = interface;
  IXMLExpandedVoidShipmentType = interface;
  IXMLString_List = interface;

{ IXMLUpsVoidShipmentRequestType }

  IXMLUpsVoidShipmentRequestType = interface(IXMLNode)
    ['{3C385842-C05C-4C51-A0DF-04AA7BB59A14}']
    { Property Accessors }
    function Get_Request: IXMLRequestType;
    function Get_ExpandedVoidShipment: IXMLExpandedVoidShipmentType;
    { Methods & Properties }
    property Request: IXMLRequestType read Get_Request;
    property ExpandedVoidShipment: IXMLExpandedVoidShipmentType read Get_ExpandedVoidShipment;
  end;

{ IXMLRequestType }

  IXMLRequestType = interface(IXMLNode)
    ['{280EB640-62DA-4071-868B-F7C9A195AD70}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: WideString;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: WideString);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property RequestAction: WideString read Get_RequestAction write Set_RequestAction;
    property RequestOption: WideString read Get_RequestOption write Set_RequestOption;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{C7DCEB13-0107-4D01-BB1B-6B2BE269E4AD}']
    { Property Accessors }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property CustomerContext: WideString read Get_CustomerContext write Set_CustomerContext;
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ IXMLExpandedVoidShipmentType }

  IXMLExpandedVoidShipmentType = interface(IXMLNode)
    ['{E81947B2-729C-41F2-A179-7859725D4325}']
    { Property Accessors }
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_TrackingNumber: IXMLString_List;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
    { Methods & Properties }
    property ShipmentIdentificationNumber: WideString read Get_ShipmentIdentificationNumber write Set_ShipmentIdentificationNumber;
    property TrackingNumber: IXMLString_List read Get_TrackingNumber;
  end;

{ IXMLString_List }

  IXMLString_List = interface(IXMLNodeCollection)
    ['{8D73EE5D-E0EF-430D-AE41-4CD28964D6B1}']
    { Methods & Properties }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
    property Items[Index: Integer]: WideString read Get_Item; default;
  end;

{ Forward Decls }

  TXMLVoidShipmentRequestType = class;
  TXMLRequestType = class;
  TXMLTransactionReferenceType = class;
  TXMLExpandedVoidShipmentType = class;
  TXMLString_List = class;

{ TXMLVoidShipmentRequestType }

  TXMLVoidShipmentRequestType = class(TXMLNode, IXMLUpsVoidShipmentRequestType)
  protected
    { IXMLUpsVoidShipmentRequestType }
    function Get_Request: IXMLRequestType;
    function Get_ExpandedVoidShipment: IXMLExpandedVoidShipmentType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRequestType }

  TXMLRequestType = class(TXMLNode, IXMLRequestType)
  protected
    { IXMLRequestType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: WideString;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
  end;

{ TXMLExpandedVoidShipmentType }

  TXMLExpandedVoidShipmentType = class(TXMLNode, IXMLExpandedVoidShipmentType)
  private
    FTrackingNumber: IXMLString_List;
  protected
    { IXMLExpandedVoidShipmentType }
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_TrackingNumber: IXMLString_List;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLString_List }

  TXMLString_List = class(TXMLNodeCollection, IXMLString_List)
  protected
    { IXMLString_List }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
  end;

{ Global Functions }

function GetVoidShipmentRequest(Doc: IXMLDocument): IXMLUpsVoidShipmentRequestType;
function LoadVoidShipmentRequest(const FileName: WideString): IXMLUpsVoidShipmentRequestType;
function NewVoidShipmentRequest: IXMLUpsVoidShipmentRequestType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetVoidShipmentRequest(Doc: IXMLDocument): IXMLUpsVoidShipmentRequestType;
begin
  Result := Doc.GetDocBinding('VoidShipmentRequest', TXMLVoidShipmentRequestType, TargetNamespace) as IXMLUpsVoidShipmentRequestType;
end;

function LoadVoidShipmentRequest(const FileName: WideString): IXMLUpsVoidShipmentRequestType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('VoidShipmentRequest', TXMLVoidShipmentRequestType, TargetNamespace) as IXMLUpsVoidShipmentRequestType;
end;

function NewVoidShipmentRequest: IXMLUpsVoidShipmentRequestType;
begin
  Result := NewXMLDocument.GetDocBinding('VoidShipmentRequest', TXMLVoidShipmentRequestType, TargetNamespace) as IXMLUpsVoidShipmentRequestType;
end;

{ TXMLVoidShipmentRequestType }

procedure TXMLVoidShipmentRequestType.AfterConstruction;
begin
  RegisterChildNode('Request', TXMLRequestType);
  RegisterChildNode('ExpandedVoidShipment', TXMLExpandedVoidShipmentType);
  inherited;
end;

function TXMLVoidShipmentRequestType.Get_Request: IXMLRequestType;
begin
  Result := ChildNodes['Request'] as IXMLRequestType;
end;

function TXMLVoidShipmentRequestType.Get_ExpandedVoidShipment: IXMLExpandedVoidShipmentType;
begin
  Result := ChildNodes['ExpandedVoidShipment'] as IXMLExpandedVoidShipmentType;
end;

{ TXMLRequestType }

procedure TXMLRequestType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  inherited;
end;

function TXMLRequestType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLRequestType.Get_RequestAction: WideString;
begin
  Result := ChildNodes['RequestAction'].Text;
end;

procedure TXMLRequestType.Set_RequestAction(Value: WideString);
begin
  ChildNodes['RequestAction'].NodeValue := Value;
end;

function TXMLRequestType.Get_RequestOption: WideString;
begin
  Result := ChildNodes['RequestOption'].Text;
end;

procedure TXMLRequestType.Set_RequestOption(Value: WideString);
begin
  ChildNodes['RequestOption'].NodeValue := Value;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_CustomerContext: WideString;
begin
  Result := ChildNodes['CustomerContext'].Text;
end;

procedure TXMLTransactionReferenceType.Set_CustomerContext(Value: WideString);
begin
  ChildNodes['CustomerContext'].NodeValue := Value;
end;

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

{ TXMLExpandedVoidShipmentType }

procedure TXMLExpandedVoidShipmentType.AfterConstruction;
begin
  FTrackingNumber := CreateCollection(TXMLString_List, IXMLNode, 'TrackingNumber') as IXMLString_List;
  inherited;
end;

function TXMLExpandedVoidShipmentType.Get_ShipmentIdentificationNumber: WideString;
begin
  Result := ChildNodes['ShipmentIdentificationNumber'].Text;
end;

procedure TXMLExpandedVoidShipmentType.Set_ShipmentIdentificationNumber(Value: WideString);
begin
  ChildNodes['ShipmentIdentificationNumber'].NodeValue := Value;
end;

function TXMLExpandedVoidShipmentType.Get_TrackingNumber: IXMLString_List;
begin
  Result := FTrackingNumber;
end;

{ TXMLString_List }

function TXMLString_List.Add(const Value: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLString_List.Insert(const Index: Integer; const Value: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;
function TXMLString_List.Get_Item(Index: Integer): WideString;
begin
  Result := List[Index].NodeValue;
end;

end.