// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrFtp2EditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, EvUIComponents, isUIwwDBEdit,
  DBCtrls;

type
  TVmrFtp2EditFrame = class(TVmrOptionEditFrame)
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBCheckBox1: TevDBCheckBox;
    evDBCheckBox2: TevDBCheckBox;
    evDBCheckBox3: TevDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterVmrClass(TVmrFtp2EditFrame);

end.
