
{**********************************************************************************************************}
{                                                                                                          }
{                                             XML Data Binding                                             }
{                                                                                                          }
{         Generated on: 11/3/2004 12:30:31 PM                                                              }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\ShipmentRequest.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\ShipmentRequest.xdb   }
{                                                                                                          }
{**********************************************************************************************************}

unit SDhlShipmentRequest;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLShipmentType = interface;
  IXMLShippingCredentialsType = interface;
  IXMLShipmentDetailType = interface;
  IXMLServiceType = interface;
  IXMLShipmentTypeType = interface;
  IXMLDimensionsType = interface;
  IXMLAdditionalProtectionType = interface;
  IXMLSpecialServicesType = interface;
  IXMLSpecialServiceType = interface;
  IXMLBillingType = interface;
  IXMLPartyType = interface;
  IXMLCODPaymentType = interface;
  IXMLSenderType = interface;
  IXMLReceiverType = interface;
  IXMLAddressType = interface;
  IXMLShipmentProcessingInstructionsType = interface;
  IXMLLabelType = interface;
  IXMLOverridesType = interface;
  IXMLOverrideType = interface;
  IXMLNotificationType = interface;
  IXMLNotifyType = interface;
  IXMLTransactionTraceType = interface;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{1FC671C3-5AC6-477B-B911-6D9129378ADA}']
    { Property Accessors }
    function Get_Action: string;
    function Get_Version: string;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_Billing: IXMLBillingType;
    function Get_Sender: IXMLSenderType;
    function Get_Receiver: IXMLReceiverType;
    function Get_ShipmentProcessingInstructions: IXMLShipmentProcessingInstructionsType;
    function Get_TransactionTrace: IXMLTransactionTraceType;
    procedure Set_Action(Value: string);
    procedure Set_Version(Value: string);
    { Methods & Properties }
    property Action: string read Get_Action write Set_Action;
    property Version: string read Get_Version write Set_Version;
    property ShippingCredentials: IXMLShippingCredentialsType read Get_ShippingCredentials;
    property ShipmentDetail: IXMLShipmentDetailType read Get_ShipmentDetail;
    property Billing: IXMLBillingType read Get_Billing;
    property Sender: IXMLSenderType read Get_Sender;
    property Receiver: IXMLReceiverType read Get_Receiver;
    property ShipmentProcessingInstructions: IXMLShipmentProcessingInstructionsType read Get_ShipmentProcessingInstructions;
    property TransactionTrace: IXMLTransactionTraceType read Get_TransactionTrace;
  end;

{ IXMLShippingCredentialsType }

  IXMLShippingCredentialsType = interface(IXMLNode)
    ['{F3BE97A3-3FC6-4346-B7D1-A7D171BA338C}']
    { Property Accessors }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
    procedure Set_ShippingKey(Value: string);
    procedure Set_AccountNbr(Value: Integer);
    { Methods & Properties }
    property ShippingKey: string read Get_ShippingKey write Set_ShippingKey;
    property AccountNbr: Integer read Get_AccountNbr write Set_AccountNbr;
  end;

{ IXMLShipmentDetailType }

  IXMLShipmentDetailType = interface(IXMLNode)
    ['{042FCAD8-EB3A-481E-8145-1E31313580DE}']
    { Property Accessors }
    function Get_ShipDate: WideString;
    function Get_Service: IXMLServiceType;
    function Get_ShipmentType: IXMLShipmentTypeType;
    function Get_Weight: Double;
    function Get_ContentDesc: string;
    function Get_ShipperReference: string;
    function Get_Dimensions: IXMLDimensionsType;
    function Get_AdditionalProtection: IXMLAdditionalProtectionType;
    function Get_SpecialServices: IXMLSpecialServicesType;
    procedure Set_ShipDate(Value: WideString);
    procedure Set_Weight(Value: Double);
    procedure Set_ContentDesc(Value: string);
    procedure Set_ShipperReference(Value: string);
    { Methods & Properties }
    property ShipDate: WideString read Get_ShipDate write Set_ShipDate;
    property Service: IXMLServiceType read Get_Service;
    property ShipmentType: IXMLShipmentTypeType read Get_ShipmentType;
    property Weight: Double read Get_Weight write Set_Weight;
    property ContentDesc: string read Get_ContentDesc write Set_ContentDesc;
    property ShipperReference: string read Get_ShipperReference write Set_ShipperReference;
    property Dimensions: IXMLDimensionsType read Get_Dimensions;
    property AdditionalProtection: IXMLAdditionalProtectionType read Get_AdditionalProtection;
    property SpecialServices: IXMLSpecialServicesType read Get_SpecialServices;
  end;

{ IXMLServiceType }

  IXMLServiceType = interface(IXMLNode)
    ['{2ABCE3B2-F63C-4812-A22D-F6FA57D3854E}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLShipmentTypeType }

  IXMLShipmentTypeType = interface(IXMLNode)
    ['{124033F2-2245-4310-A973-12376A73261C}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLDimensionsType }

  IXMLDimensionsType = interface(IXMLNode)
    ['{A2A822B2-63AB-464C-96AE-388EAF2D0521}']
    { Property Accessors }
    function Get_Length: Double;
    function Get_Width: Double;
    function Get_Height: Double;
    procedure Set_Length(Value: Double);
    procedure Set_Width(Value: Double);
    procedure Set_Height(Value: Double);
    { Methods & Properties }
    property Length: Double read Get_Length write Set_Length;
    property Width: Double read Get_Width write Set_Width;
    property Height: Double read Get_Height write Set_Height;
  end;

{ IXMLAdditionalProtectionType }

  IXMLAdditionalProtectionType = interface(IXMLNode)
    ['{82AF4B7C-7C9E-43EE-B406-08887F9BBF91}']
    { Property Accessors }
    function Get_Code: string;
    function Get_Value: Currency;
    procedure Set_Code(Value: string);
    procedure Set_Value(Value: Currency);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
    property Value: Currency read Get_Value write Set_Value;
  end;

{ IXMLSpecialServicesType }

  IXMLSpecialServicesType = interface(IXMLNode)
    ['{DA1CA3D5-E1C0-4663-909E-97EF632CD75D}']
    { Property Accessors }
    function Get_SpecialService: IXMLSpecialServiceType;
    { Methods & Properties }
    property SpecialService: IXMLSpecialServiceType read Get_SpecialService;
  end;

{ IXMLSpecialServiceType }

  IXMLSpecialServiceType = interface(IXMLNode)
    ['{A1799406-3FA7-41A2-9E26-E80F24BADAEB}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLBillingType }

  IXMLBillingType = interface(IXMLNode)
    ['{3E26869F-02C5-408B-9E17-C2BEEEE972CF}']
    { Property Accessors }
    function Get_Party: IXMLPartyType;
    function Get_AccountNbr: Integer;
    function Get_CODPayment: IXMLCODPaymentType;
    procedure Set_AccountNbr(Value: Integer);
    { Methods & Properties }
    property Party: IXMLPartyType read Get_Party;
    property AccountNbr: Integer read Get_AccountNbr write Set_AccountNbr;
    property CODPayment: IXMLCODPaymentType read Get_CODPayment;
  end;

{ IXMLPartyType }

  IXMLPartyType = interface(IXMLNode)
    ['{0AC225D4-63B7-4472-8C3A-7616A7FE1F5A}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLCODPaymentType }

  IXMLCODPaymentType = interface(IXMLNode)
    ['{716EBDD7-FE40-4348-AF14-3185B639C51D}']
    { Property Accessors }
    function Get_Code: string;
    function Get_Value: Currency;
    procedure Set_Code(Value: string);
    procedure Set_Value(Value: Currency);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
    property Value: Currency read Get_Value write Set_Value;
  end;

{ IXMLSenderType }

  IXMLSenderType = interface(IXMLNode)
    ['{D994EAF9-39EC-4C6A-8B92-A2A1092E7AB4}']
    { Property Accessors }
    function Get_SentBy: string;
    function Get_PhoneNbr: string;
    procedure Set_SentBy(Value: string);
    procedure Set_PhoneNbr(Value: string);
    { Methods & Properties }
    property SentBy: string read Get_SentBy write Set_SentBy;
    property PhoneNbr: string read Get_PhoneNbr write Set_PhoneNbr;
  end;

{ IXMLReceiverType }

  IXMLReceiverType = interface(IXMLNode)
    ['{186257D5-6064-40D2-AD5F-4BD6EF7E175D}']
    { Property Accessors }
    function Get_Address: IXMLAddressType;
    function Get_AttnTo: string;
    function Get_PhoneNbr: string;
    procedure Set_AttnTo(Value: string);
    procedure Set_PhoneNbr(Value: string);
    { Methods & Properties }
    property Address: IXMLAddressType read Get_Address;
    property AttnTo: string read Get_AttnTo write Set_AttnTo;
    property PhoneNbr: string read Get_PhoneNbr write Set_PhoneNbr;
  end;

{ IXMLAddressType }

  IXMLAddressType = interface(IXMLNode)
    ['{3AEC29C0-6F14-4DEC-917C-F79073203417}']
    { Property Accessors }
    function Get_CompanyName: string;
    function Get_SuiteDepartmentName: string;
    function Get_Street: string;
    function Get_StreetLine2: string;
    function Get_City: string;
    function Get_State: string;
    function Get_PostalCode: string;
    function Get_Country: string;
    procedure Set_CompanyName(Value: string);
    procedure Set_SuiteDepartmentName(Value: string);
    procedure Set_Street(Value: string);
    procedure Set_StreetLine2(Value: string);
    procedure Set_City(Value: string);
    procedure Set_State(Value: string);
    procedure Set_PostalCode(Value: string);
    procedure Set_Country(Value: string);
    { Methods & Properties }
    property CompanyName: string read Get_CompanyName write Set_CompanyName;
    property SuiteDepartmentName: string read Get_SuiteDepartmentName write Set_SuiteDepartmentName;
    property Street: string read Get_Street write Set_Street;
    property StreetLine2: string read Get_StreetLine2 write Set_StreetLine2;
    property City: string read Get_City write Set_City;
    property State: string read Get_State write Set_State;
    property PostalCode: string read Get_PostalCode write Set_PostalCode;
    property Country: string read Get_Country write Set_Country;
  end;

{ IXMLShipmentProcessingInstructionsType }

  IXMLShipmentProcessingInstructionsType = interface(IXMLNode)
    ['{E13529DA-82AF-41DE-85A2-19BA5DF148F7}']
    { Property Accessors }
    function Get_Label_: IXMLLabelType;
    function Get_Overrides: IXMLOverridesType;
    function Get_Notification: IXMLNotificationType;
    { Methods & Properties }
    property Label_: IXMLLabelType read Get_Label_;
    property Overrides: IXMLOverridesType read Get_Overrides;
    property Notification: IXMLNotificationType read Get_Notification;
  end;

{ IXMLLabelType }

  IXMLLabelType = interface(IXMLNode)
    ['{C1432220-980A-4049-8E17-3E734F9B191E}']
    { Property Accessors }
    function Get_ImageType: string;
    procedure Set_ImageType(Value: string);
    { Methods & Properties }
    property ImageType: string read Get_ImageType write Set_ImageType;
  end;

{ IXMLOverridesType }

  IXMLOverridesType = interface(IXMLNode)
    ['{5EF33CAC-2EC6-4727-9CE5-F94F78F97C6D}']
    { Property Accessors }
    function Get_Override: IXMLOverrideType;
    { Methods & Properties }
    property Override: IXMLOverrideType read Get_Override;
  end;

{ IXMLOverrideType }

  IXMLOverrideType = interface(IXMLNode)
    ['{DD6A0EB7-92D7-47BE-87FC-CFD86116536B}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLNotificationType }

  IXMLNotificationType = interface(IXMLNode)
    ['{6BA30731-27C1-478B-B92A-1D9E2F224AF5}']
    { Property Accessors }
    function Get_Notify: IXMLNotifyType;
    function Get_Message: string;
    procedure Set_Message(Value: string);
    { Methods & Properties }
    property Notify: IXMLNotifyType read Get_Notify;
    property Message: string read Get_Message write Set_Message;
  end;

{ IXMLNotifyType }

  IXMLNotifyType = interface(IXMLNode)
    ['{171D2A5C-6F5A-4DF9-BA6E-07EABA15A944}']
    { Property Accessors }
    function Get_EmailAddress: string;
    procedure Set_EmailAddress(Value: string);
    { Methods & Properties }
    property EmailAddress: string read Get_EmailAddress write Set_EmailAddress;
  end;

{ IXMLTransactionTraceType }

  IXMLTransactionTraceType = interface(IXMLNode)
    ['{5C120F81-6BED-4CB5-82A7-38BA2993C261}']
  end;

{ Forward Decls }

  TXMLShipmentType = class;
  TXMLShippingCredentialsType = class;
  TXMLShipmentDetailType = class;
  TXMLServiceType = class;
  TXMLShipmentTypeType = class;
  TXMLDimensionsType = class;
  TXMLAdditionalProtectionType = class;
  TXMLSpecialServicesType = class;
  TXMLSpecialServiceType = class;
  TXMLBillingType = class;
  TXMLPartyType = class;
  TXMLCODPaymentType = class;
  TXMLSenderType = class;
  TXMLReceiverType = class;
  TXMLAddressType = class;
  TXMLShipmentProcessingInstructionsType = class;
  TXMLLabelType = class;
  TXMLOverridesType = class;
  TXMLOverrideType = class;
  TXMLNotificationType = class;
  TXMLNotifyType = class;
  TXMLTransactionTraceType = class;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  protected
    { IXMLShipmentType }
    function Get_Action: string;
    function Get_Version: string;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_Billing: IXMLBillingType;
    function Get_Sender: IXMLSenderType;
    function Get_Receiver: IXMLReceiverType;
    function Get_ShipmentProcessingInstructions: IXMLShipmentProcessingInstructionsType;
    function Get_TransactionTrace: IXMLTransactionTraceType;
    procedure Set_Action(Value: string);
    procedure Set_Version(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShippingCredentialsType }

  TXMLShippingCredentialsType = class(TXMLNode, IXMLShippingCredentialsType)
  protected
    { IXMLShippingCredentialsType }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
    procedure Set_ShippingKey(Value: string);
    procedure Set_AccountNbr(Value: Integer);
  end;

{ TXMLShipmentDetailType }

  TXMLShipmentDetailType = class(TXMLNode, IXMLShipmentDetailType)
  protected
    { IXMLShipmentDetailType }
    function Get_ShipDate: WideString;
    function Get_Service: IXMLServiceType;
    function Get_ShipmentType: IXMLShipmentTypeType;
    function Get_Weight: Double;
    function Get_ContentDesc: string;
    function Get_ShipperReference: string;
    function Get_Dimensions: IXMLDimensionsType;
    function Get_AdditionalProtection: IXMLAdditionalProtectionType;
    function Get_SpecialServices: IXMLSpecialServicesType;
    procedure Set_ShipDate(Value: WideString);
    procedure Set_Weight(Value: Double);
    procedure Set_ContentDesc(Value: string);
    procedure Set_ShipperReference(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServiceType }

  TXMLServiceType = class(TXMLNode, IXMLServiceType)
  protected
    { IXMLServiceType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLShipmentTypeType }

  TXMLShipmentTypeType = class(TXMLNode, IXMLShipmentTypeType)
  protected
    { IXMLShipmentTypeType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLDimensionsType }

  TXMLDimensionsType = class(TXMLNode, IXMLDimensionsType)
  protected
    { IXMLDimensionsType }
    function Get_Length: Double;
    function Get_Width: Double;
    function Get_Height: Double;
    procedure Set_Length(Value: Double);
    procedure Set_Width(Value: Double);
    procedure Set_Height(Value: Double);
  end;

{ TXMLAdditionalProtectionType }

  TXMLAdditionalProtectionType = class(TXMLNode, IXMLAdditionalProtectionType)
  protected
    { IXMLAdditionalProtectionType }
    function Get_Code: string;
    function Get_Value: Currency;
    procedure Set_Code(Value: string);
    procedure Set_Value(Value: Currency);
  end;

{ TXMLSpecialServicesType }

  TXMLSpecialServicesType = class(TXMLNode, IXMLSpecialServicesType)
  protected
    { IXMLSpecialServicesType }
    function Get_SpecialService: IXMLSpecialServiceType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSpecialServiceType }

  TXMLSpecialServiceType = class(TXMLNode, IXMLSpecialServiceType)
  protected
    { IXMLSpecialServiceType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLBillingType }

  TXMLBillingType = class(TXMLNode, IXMLBillingType)
  protected
    { IXMLBillingType }
    function Get_Party: IXMLPartyType;
    function Get_AccountNbr: Integer;
    function Get_CODPayment: IXMLCODPaymentType;
    procedure Set_AccountNbr(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPartyType }

  TXMLPartyType = class(TXMLNode, IXMLPartyType)
  protected
    { IXMLPartyType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLCODPaymentType }

  TXMLCODPaymentType = class(TXMLNode, IXMLCODPaymentType)
  protected
    { IXMLCODPaymentType }
    function Get_Code: string;
    function Get_Value: Currency;
    procedure Set_Code(Value: string);
    procedure Set_Value(Value: Currency);
  end;

{ TXMLSenderType }

  TXMLSenderType = class(TXMLNode, IXMLSenderType)
  protected
    { IXMLSenderType }
    function Get_SentBy: string;
    function Get_PhoneNbr: string;
    procedure Set_SentBy(Value: string);
    procedure Set_PhoneNbr(Value: string);
  end;

{ TXMLReceiverType }

  TXMLReceiverType = class(TXMLNode, IXMLReceiverType)
  protected
    { IXMLReceiverType }
    function Get_Address: IXMLAddressType;
    function Get_AttnTo: string;
    function Get_PhoneNbr: string;
    procedure Set_AttnTo(Value: string);
    procedure Set_PhoneNbr(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAddressType }

  TXMLAddressType = class(TXMLNode, IXMLAddressType)
  protected
    { IXMLAddressType }
    function Get_CompanyName: string;
    function Get_SuiteDepartmentName: string;
    function Get_Street: string;
    function Get_StreetLine2: string;
    function Get_City: string;
    function Get_State: string;
    function Get_PostalCode: string;
    function Get_Country: string;
    procedure Set_CompanyName(Value: string);
    procedure Set_SuiteDepartmentName(Value: string);
    procedure Set_Street(Value: string);
    procedure Set_StreetLine2(Value: string);
    procedure Set_City(Value: string);
    procedure Set_State(Value: string);
    procedure Set_PostalCode(Value: string);
    procedure Set_Country(Value: string);
  end;

{ TXMLShipmentProcessingInstructionsType }

  TXMLShipmentProcessingInstructionsType = class(TXMLNode, IXMLShipmentProcessingInstructionsType)
  protected
    { IXMLShipmentProcessingInstructionsType }
    function Get_Label_: IXMLLabelType;
    function Get_Overrides: IXMLOverridesType;
    function Get_Notification: IXMLNotificationType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLabelType }

  TXMLLabelType = class(TXMLNode, IXMLLabelType)
  protected
    { IXMLLabelType }
    function Get_ImageType: string;
    procedure Set_ImageType(Value: string);
  end;

{ TXMLOverridesType }

  TXMLOverridesType = class(TXMLNode, IXMLOverridesType)
  protected
    { IXMLOverridesType }
    function Get_Override: IXMLOverrideType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOverrideType }

  TXMLOverrideType = class(TXMLNode, IXMLOverrideType)
  protected
    { IXMLOverrideType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLNotificationType }

  TXMLNotificationType = class(TXMLNode, IXMLNotificationType)
  protected
    { IXMLNotificationType }
    function Get_Notify: IXMLNotifyType;
    function Get_Message: string;
    procedure Set_Message(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLNotifyType }

  TXMLNotifyType = class(TXMLNode, IXMLNotifyType)
  protected
    { IXMLNotifyType }
    function Get_EmailAddress: string;
    procedure Set_EmailAddress(Value: string);
  end;

{ TXMLTransactionTraceType }

  TXMLTransactionTraceType = class(TXMLNode, IXMLTransactionTraceType)
  protected
    { IXMLTransactionTraceType }
  end;

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
function LoadShipment(const FileName: WideString): IXMLShipmentType;
function NewShipment: IXMLShipmentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
begin
  Result := Doc.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function LoadShipment(const FileName: WideString): IXMLShipmentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function NewShipment: IXMLShipmentType;
begin
  Result := NewXMLDocument.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('ShippingCredentials', TXMLShippingCredentialsType);
  RegisterChildNode('ShipmentDetail', TXMLShipmentDetailType);
  RegisterChildNode('Billing', TXMLBillingType);
  RegisterChildNode('Sender', TXMLSenderType);
  RegisterChildNode('Receiver', TXMLReceiverType);
  RegisterChildNode('ShipmentProcessingInstructions', TXMLShipmentProcessingInstructionsType);
  RegisterChildNode('TransactionTrace', TXMLTransactionTraceType);
  inherited;
end;

function TXMLShipmentType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLShipmentType.Set_Action(Value: string);
begin
  SetAttribute('action', Value);
end;

function TXMLShipmentType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

procedure TXMLShipmentType.Set_Version(Value: string);
begin
  SetAttribute('version', Value);
end;

function TXMLShipmentType.Get_ShippingCredentials: IXMLShippingCredentialsType;
begin
  Result := ChildNodes['ShippingCredentials'] as IXMLShippingCredentialsType;
end;

function TXMLShipmentType.Get_ShipmentDetail: IXMLShipmentDetailType;
begin
  Result := ChildNodes['ShipmentDetail'] as IXMLShipmentDetailType;
end;

function TXMLShipmentType.Get_Billing: IXMLBillingType;
begin
  Result := ChildNodes['Billing'] as IXMLBillingType;
end;

function TXMLShipmentType.Get_Sender: IXMLSenderType;
begin
  Result := ChildNodes['Sender'] as IXMLSenderType;
end;

function TXMLShipmentType.Get_Receiver: IXMLReceiverType;
begin
  Result := ChildNodes['Receiver'] as IXMLReceiverType;
end;

function TXMLShipmentType.Get_ShipmentProcessingInstructions: IXMLShipmentProcessingInstructionsType;
begin
  Result := ChildNodes['ShipmentProcessingInstructions'] as IXMLShipmentProcessingInstructionsType;
end;

function TXMLShipmentType.Get_TransactionTrace: IXMLTransactionTraceType;
begin
  Result := ChildNodes['TransactionTrace'] as IXMLTransactionTraceType;
end;

{ TXMLShippingCredentialsType }

function TXMLShippingCredentialsType.Get_ShippingKey: string;
begin
  Result := ChildNodes['ShippingKey'].Text;
end;

procedure TXMLShippingCredentialsType.Set_ShippingKey(Value: string);
begin
  ChildNodes['ShippingKey'].NodeValue := Value;
end;

function TXMLShippingCredentialsType.Get_AccountNbr: Integer;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

procedure TXMLShippingCredentialsType.Set_AccountNbr(Value: Integer);
begin
  ChildNodes['AccountNbr'].NodeValue := Value;
end;

{ TXMLShipmentDetailType }

procedure TXMLShipmentDetailType.AfterConstruction;
begin
  RegisterChildNode('Service', TXMLServiceType);
  RegisterChildNode('ShipmentType', TXMLShipmentTypeType);
  RegisterChildNode('Dimensions', TXMLDimensionsType);
  RegisterChildNode('AdditionalProtection', TXMLAdditionalProtectionType);
  RegisterChildNode('SpecialServices', TXMLSpecialServicesType);
  inherited;
end;

function TXMLShipmentDetailType.Get_ShipDate: WideString;
begin
  Result := ChildNodes['ShipDate'].Text;
end;

procedure TXMLShipmentDetailType.Set_ShipDate(Value: WideString);
begin
  ChildNodes['ShipDate'].NodeValue := Value;
end;

function TXMLShipmentDetailType.Get_Service: IXMLServiceType;
begin
  Result := ChildNodes['Service'] as IXMLServiceType;
end;

function TXMLShipmentDetailType.Get_ShipmentType: IXMLShipmentTypeType;
begin
  Result := ChildNodes['ShipmentType'] as IXMLShipmentTypeType;
end;

function TXMLShipmentDetailType.Get_Weight: Double;
begin
  Result := ChildNodes['Weight'].NodeValue;
end;

procedure TXMLShipmentDetailType.Set_Weight(Value: Double);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

function TXMLShipmentDetailType.Get_ContentDesc: string;
begin
  Result := ChildNodes['ContentDesc'].Text;
end;

procedure TXMLShipmentDetailType.Set_ContentDesc(Value: string);
begin
  ChildNodes['ContentDesc'].NodeValue := Value;
end;

function TXMLShipmentDetailType.Get_ShipperReference: string;
begin
  Result := ChildNodes['ShipperReference'].Text;
end;

procedure TXMLShipmentDetailType.Set_ShipperReference(Value: string);
begin
  ChildNodes['ShipperReference'].NodeValue := Value;
end;

function TXMLShipmentDetailType.Get_Dimensions: IXMLDimensionsType;
begin
  Result := ChildNodes['Dimensions'] as IXMLDimensionsType;
end;

function TXMLShipmentDetailType.Get_AdditionalProtection: IXMLAdditionalProtectionType;
begin
  Result := ChildNodes['AdditionalProtection'] as IXMLAdditionalProtectionType;
end;

function TXMLShipmentDetailType.Get_SpecialServices: IXMLSpecialServicesType;
begin
  Result := ChildNodes['SpecialServices'] as IXMLSpecialServicesType;
end;

{ TXMLServiceType }

function TXMLServiceType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLServiceType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLShipmentTypeType }

function TXMLShipmentTypeType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLShipmentTypeType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLDimensionsType }

function TXMLDimensionsType.Get_Length: Double;
begin
  Result := ChildNodes['Length'].NodeValue;
end;

procedure TXMLDimensionsType.Set_Length(Value: Double);
begin
  ChildNodes['Length'].NodeValue := Value;
end;

function TXMLDimensionsType.Get_Width: Double;
begin
  Result := ChildNodes['Width'].NodeValue;
end;

procedure TXMLDimensionsType.Set_Width(Value: Double);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

function TXMLDimensionsType.Get_Height: Double;
begin
  Result := ChildNodes['Height'].NodeValue;
end;

procedure TXMLDimensionsType.Set_Height(Value: Double);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

{ TXMLAdditionalProtectionType }

function TXMLAdditionalProtectionType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLAdditionalProtectionType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLAdditionalProtectionType.Get_Value: Currency;
begin
  Result := ChildNodes['Value'].NodeValue;
end;

procedure TXMLAdditionalProtectionType.Set_Value(Value: Currency);
begin
  ChildNodes['Value'].NodeValue := Value;
end;

{ TXMLSpecialServicesType }

procedure TXMLSpecialServicesType.AfterConstruction;
begin
  RegisterChildNode('SpecialService', TXMLSpecialServiceType);
  inherited;
end;

function TXMLSpecialServicesType.Get_SpecialService: IXMLSpecialServiceType;
begin
  Result := ChildNodes['SpecialService'] as IXMLSpecialServiceType;
end;

{ TXMLSpecialServiceType }

function TXMLSpecialServiceType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLSpecialServiceType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLBillingType }

procedure TXMLBillingType.AfterConstruction;
begin
  RegisterChildNode('Party', TXMLPartyType);
  RegisterChildNode('CODPayment', TXMLCODPaymentType);
  inherited;
end;

function TXMLBillingType.Get_Party: IXMLPartyType;
begin
  Result := ChildNodes['Party'] as IXMLPartyType;
end;

function TXMLBillingType.Get_AccountNbr: Integer;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

procedure TXMLBillingType.Set_AccountNbr(Value: Integer);
begin
  ChildNodes['AccountNbr'].NodeValue := Value;
end;

function TXMLBillingType.Get_CODPayment: IXMLCODPaymentType;
begin
  Result := ChildNodes['CODPayment'] as IXMLCODPaymentType;
end;

{ TXMLPartyType }

function TXMLPartyType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLPartyType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLCODPaymentType }

function TXMLCODPaymentType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLCODPaymentType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLCODPaymentType.Get_Value: Currency;
begin
  Result := ChildNodes['Value'].NodeValue;
end;

procedure TXMLCODPaymentType.Set_Value(Value: Currency);
begin
  ChildNodes['Value'].NodeValue := Value;
end;

{ TXMLSenderType }

function TXMLSenderType.Get_SentBy: string;
begin
  Result := ChildNodes['SentBy'].Text;
end;

procedure TXMLSenderType.Set_SentBy(Value: string);
begin
  ChildNodes['SentBy'].NodeValue := Value;
end;

function TXMLSenderType.Get_PhoneNbr: string;
begin
  Result := ChildNodes['PhoneNbr'].Text;
end;

procedure TXMLSenderType.Set_PhoneNbr(Value: string);
begin
  ChildNodes['PhoneNbr'].NodeValue := Value;
end;

{ TXMLReceiverType }

procedure TXMLReceiverType.AfterConstruction;
begin
  RegisterChildNode('Address', TXMLAddressType);
  inherited;
end;

function TXMLReceiverType.Get_Address: IXMLAddressType;
begin
  Result := ChildNodes['Address'] as IXMLAddressType;
end;

function TXMLReceiverType.Get_AttnTo: string;
begin
  Result := ChildNodes['AttnTo'].Text;
end;

procedure TXMLReceiverType.Set_AttnTo(Value: string);
begin
  ChildNodes['AttnTo'].NodeValue := Value;
end;

function TXMLReceiverType.Get_PhoneNbr: string;
begin
  Result := ChildNodes['PhoneNbr'].Text;
end;

procedure TXMLReceiverType.Set_PhoneNbr(Value: string);
begin
  ChildNodes['PhoneNbr'].NodeValue := Value;
end;

{ TXMLAddressType }

function TXMLAddressType.Get_CompanyName: string;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLAddressType.Set_CompanyName(Value: string);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLAddressType.Get_SuiteDepartmentName: string;
begin
  Result := ChildNodes['SuiteDepartmentName'].Text;
end;

procedure TXMLAddressType.Set_SuiteDepartmentName(Value: string);
begin
  ChildNodes['SuiteDepartmentName'].NodeValue := Value;
end;

function TXMLAddressType.Get_Street: string;
begin
  Result := ChildNodes['Street'].Text;
end;

procedure TXMLAddressType.Set_Street(Value: string);
begin
  ChildNodes['Street'].NodeValue := Value;
end;

function TXMLAddressType.Get_StreetLine2: string;
begin
  Result := ChildNodes['StreetLine2'].Text;
end;

procedure TXMLAddressType.Set_StreetLine2(Value: string);
begin
  ChildNodes['StreetLine2'].NodeValue := Value;
end;

function TXMLAddressType.Get_City: string;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLAddressType.Set_City(Value: string);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLAddressType.Get_State: string;
begin
  Result := ChildNodes['State'].Text;
end;

procedure TXMLAddressType.Set_State(Value: string);
begin
  ChildNodes['State'].NodeValue := Value;
end;

function TXMLAddressType.Get_PostalCode: string;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLAddressType.Set_PostalCode(Value: string);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLAddressType.Get_Country: string;
begin
  Result := ChildNodes['Country'].Text;
end;

procedure TXMLAddressType.Set_Country(Value: string);
begin
  ChildNodes['Country'].NodeValue := Value;
end;

{ TXMLShipmentProcessingInstructionsType }

procedure TXMLShipmentProcessingInstructionsType.AfterConstruction;
begin
  RegisterChildNode('Label', TXMLLabelType);
  RegisterChildNode('Overrides', TXMLOverridesType);
  RegisterChildNode('Notification', TXMLNotificationType);
  inherited;
end;

function TXMLShipmentProcessingInstructionsType.Get_Label_: IXMLLabelType;
begin
  Result := ChildNodes['Label'] as IXMLLabelType;
end;

function TXMLShipmentProcessingInstructionsType.Get_Overrides: IXMLOverridesType;
begin
  Result := ChildNodes['Overrides'] as IXMLOverridesType;
end;

function TXMLShipmentProcessingInstructionsType.Get_Notification: IXMLNotificationType;
begin
  Result := ChildNodes['Notification'] as IXMLNotificationType;
end;

{ TXMLLabelType }

function TXMLLabelType.Get_ImageType: string;
begin
  Result := ChildNodes['ImageType'].Text;
end;

procedure TXMLLabelType.Set_ImageType(Value: string);
begin
  ChildNodes['ImageType'].NodeValue := Value;
end;

{ TXMLOverridesType }

procedure TXMLOverridesType.AfterConstruction;
begin
  RegisterChildNode('Override', TXMLOverrideType);
  inherited;
end;

function TXMLOverridesType.Get_Override: IXMLOverrideType;
begin
  Result := ChildNodes['Override'] as IXMLOverrideType;
end;

{ TXMLOverrideType }

function TXMLOverrideType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLOverrideType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLNotificationType }

procedure TXMLNotificationType.AfterConstruction;
begin
  RegisterChildNode('Notify', TXMLNotifyType);
  inherited;
end;

function TXMLNotificationType.Get_Notify: IXMLNotifyType;
begin
  Result := ChildNodes['Notify'] as IXMLNotifyType;
end;

function TXMLNotificationType.Get_Message: string;
begin
  Result := ChildNodes['Message'].Text;
end;

procedure TXMLNotificationType.Set_Message(Value: string);
begin
  ChildNodes['Message'].NodeValue := Value;
end;

{ TXMLNotifyType }

function TXMLNotifyType.Get_EmailAddress: string;
begin
  Result := ChildNodes['EmailAddress'].Text;
end;

procedure TXMLNotifyType.Set_EmailAddress(Value: string);
begin
  ChildNodes['EmailAddress'].NodeValue := Value;
end;

{ TXMLTransactionTraceType }

end.