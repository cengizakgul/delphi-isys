// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SServiceE;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, isFTP,
  Forms, Dialogs, SVmrClasses, DB, SVmrOptionEditFrame, EvContext,
  SReportSettings, EvExceptions, EvClientDataSet, SBSimpleSftp, SBSftpCommon;

type
  TVmrEMedia = class(TVmrMediaType)
  protected
    procedure Encrypt(const RL: TrwReportResults);
    procedure ConvertToPDF(const RL: TrwReportResults);
    procedure ConvertToZIP(const RL: TrwReportResults);
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function OnPickupSheetScan(cd: TEvClientDataSet): string; override;
  end;

  TVmrEDelivery = class(TVmrDeliveryMethod)
  public
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); override;
    class function GetMediaClasses: TVmrMediaTypeClassArray; override;
    procedure RevertToUnreleased(const Media: TVmrMediaType; const cd:
            TEvClientDataSet); override;
  end;

  TVmrEDeliveryService = class(TVmrDeliveryService)
  public
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
  end;

  TVmrFtpDeliveryMethod = class(TVmrEDelivery)
  private
    FFTPParamArray: array of TrwReportResults;
  protected
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
  public
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); override;
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    procedure PopulateNewMailBox; override;
    procedure PrepareToRelease; override;
    procedure Release(const cd: TEvClientDataSet); override;
  end;

  TVmrEmailDeliveryMethod = class(TVmrEDelivery)
  private
    FEmailParamArray: array of TrwReportResults;
  protected
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
  public
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); override;
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    procedure PopulateNewMailBox; override;
    procedure PrepareToRelease; override;
    procedure Release(const cd: TEvClientDataSet); override;
  end;

  TVmrDirectEmailDeliveryMethod = class(TVmrEDelivery)
  protected
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
  public
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); override;
    procedure PopulateNewMailBox; override;
    procedure PrepareToRelease; override;
    procedure Release(const cd: TEvClientDataSet); override;
  end;

  TVmrWebDeliveryMethod = class(TVmrEDelivery)
  private
  protected
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function  GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:TEvClientDataSet; const RL: TrwReportResults); override;
    procedure PopulateNewMailBox; override;
    procedure PrepareToRelease; override;
    procedure Release(const cd: TEvClientDataSet); override;
  end;


procedure Register;

implementation

uses SVmrEmail2EditFrame, SVmrEmailEditFrame, SVmrFtpEditFrame, SVmrFtp2EditFrame, EvUtils,
  EvTypes, EvStreamUtils, SDataStructure,
  Variants, EvBasicUtils, EvConsts, SCdMediaWriterForm, SVmrEMediaClParamFrame,isBaseClasses,
  ISZippingRoutines,IsBasicUtils, SVmrWebExtraOptionFrame, EvCommonInterfaces, EvDataset,sVMRUtils;

procedure Register;
begin
end;

{
********************************** TVmrEMedia **********************************
}
procedure TVmrEMedia.ConvertToPDF(const RL: TrwReportResults);
var
  rwPDFInfoRec: TrwPDFDocInfo;
  idsPDF: IEvDualStream;
  i:integer;
begin
     rwPDFInfoRec.Author := '';
     rwPDFInfoRec.Creator := '';
     rwPDFInfoRec.Subject := '';
     rwPDFInfoRec.Compression := True;
     rwPDFInfoRec.ProtectionOptions := [];
     rwPDFInfoRec.OwnerPassword := '';
     rwPDFInfoRec.UserPassword :=  '';

     rwPDFInfoRec.OwnerPassword := Trim(ConvertNull(ExtraOptions.FieldByName('PRINT_PASSWORD').Value,''));
     if rwPDFInfoRec.OwnerPassword <> '' then
      rwPDFInfoRec.ProtectionOptions := [pdfPrint, pdfCopyInformation];

     for i:=0 to RL.Count-1 do
     begin
       if RL[i].ReportType = rtASCIIFile then  // Can not convert ASCII files to PDF format
          continue;
       rwPDFInfoRec.Title := RL[i].ReportName;
       idsPDF := ctx_RWLocalEngine.ConvertRWAtoPDF(RL[i].Data, rwPDFInfoRec);
       RL[i].Data := idsPDF;
     end;
end;

procedure TVmrEMedia.ConvertToZIP(const RL: TrwReportResults);
var path,sResultFile,p:string;
 R:TrwReportResult;
 i :integer;
begin
  if RL.Count = 0 then
    Exit;

    p :=  ConvertNull(ExtraOptions.FieldByName('PRINT_PASSWORD').Value,'');
    path := IncludeTrailingPathDelimiter(path);
    sResultFile := AppTempFolder + GetUniqueID + '.zip';
    for i := 0 to RL.Count - 1 do
    begin
      if ISXLSFormat(RL[i].data) then
        AddToFileFromStream(sResultFile,RL[i].VmrFileName+'.xls',RL[i].Data.RealStream,p)
      else
        AddToFileFromStream(sResultFile,RL[i].VmrFileName+'.txt',RL[i].Data.RealStream,p);
    end;

    R := TrwReportResult.Create(nil);
    R.Data := TEvDualStreamHolder.CreateInMemory;
    R.Data.LoadFromFile(sResultFile);
    R.VmrFileName := 'ASCII reports';
    SysUtils.DeleteFile(sResultFile);

    RL.Clear;
    R.Collection := RL;
    RL[0].ReportName := 'ASCII reports';
end;


procedure TVmrEMedia.Encrypt(const RL: TrwReportResults);
begin
  EncryptReportResults(RL, ExtraOptions.FieldByName('PRINT_PASSWORD').AsString);
end;

function TVmrEMedia.GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TVmrEMediaClParamFrame;
end;

function TVmrEMedia.OnPickupSheetScan(cd: TEvClientDataSet): string;
var
  RL: TrwReportResults;
  s: string;
begin
  RL := CreateResultCollection(cd);
  try
    Encrypt(RL);
    CreateTempFiles(s, RL);
    try
      BurnDirectoryToCd(s);
    finally
      DeleteTempFiles(s, RL);
    end;
  finally
    RL.Free;
  end;
end;

procedure TVmrEMedia.PrepareExtraOptionFields(var Prefix: string; var
        LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('PRINT_PASSWORD', ftString, 20);
  f.Add('CONVERT_TO_PDF', ftString, 1);
end;

{
******************************** TVmrEDelivery *********************************
}
class function TVmrEDelivery.GetMediaClasses: TVmrMediaTypeClassArray;
begin
  SetLength(Result, 1);
  Result[0] := TVmrEMedia;
end;

{
***************************** TVmrEDeliveryService *****************************
}
class function TVmrEDeliveryService.GetMethodClasses:
        TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 4);
  Result[0] := TVmrEmailDeliveryMethod;
  Result[1] := TVmrFtpDeliveryMethod;
  Result[2] := TVmrDirectEmailDeliveryMethod;
  Result[3] := TVmrWebDeliveryMethod;
end;

{
**************************** TVmrFtpDeliveryMethod *****************************
}
procedure TVmrFtpDeliveryMethod.InternalAddToRelease(const Media: TVmrMediaType; const
        cd: TEvClientDataSet; const RL: TrwReportResults);
var
  xmlRL, rwaRL: TrwReportResults;
begin
  Assert(Media is TVmrEMedia);

  xmlRL := TrwReportResults.Create;
  rwaRL := TrwReportResults.Create;
  try
    if (ConvertNull(ExtraOptions.FieldByName('SEND_PURE_XML').Value,0) = 1) then
      while RL.Count>0 do
        if IsXMLFormat(RL[0].Data) then
          RL[0].Collection := xmlRL
        else
          RL[0].Collection := rwaRL
    else
      RL.MoveTo(rwaRL);

    if ConvertNull(TVmrEMedia(Media).ExtraOptions.FieldByName('CONVERT_TO_PDF').Value,'N')='Y' then
       TVmrEMedia(Media).ConvertToPDF(rwaRL)
    else
       TVmrEMedia(Media).Encrypt(rwaRL);

    rwaRL.MoveTo(RL);
    xmlRL.MoveTo(RL);
  finally
    FreeAndNil(xmlRL);
    FreeAndNil(rwaRL);
  end;

  SetLength(FFTPParamArray, Length(FFTPParamArray)+1);
  FFTPParamArray[High(FFTPParamArray)] := TrwReportResults.Create;
  RL.MoveTo(FFTPParamArray[High(FFTPParamArray)]);
//  Publish(RL);
  cd['SCANNED_TIME'] := cd['PRINTED_TIME'];
  cd['SHIPPED_TIME'] := cd['PRINTED_TIME'];
end;

function TVmrFtpDeliveryMethod.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrFtp2EditFrame;
end;

function TVmrFtpDeliveryMethod.GetOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrFtpEditFrame;
end;

procedure TVmrFtpDeliveryMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := 'Ftp to '+ ExtraOptions.FieldByName('PATH').AsString;
end;

procedure TVmrFtpDeliveryMethod.PrepareExtraOptionFields(var Prefix: string;
        var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('PATH', ftString, 60);
  f.Add('CREATE_FOLDERS', ftInteger);
  f.Add('SEND_PURE_XML', ftInteger);
  f.Add('SAVE_ASCII', ftInteger);
end;

procedure TVmrFtpDeliveryMethod.PrepareOptionFields(var LoadRecordCount:
        Integer; const f: TFieldDefs);
begin
  inherited PrepareOptionFields(LoadRecordCount, f);
  f.Add('FTP_SERVER', ftString, 60);
  f.Add('USE_SFTP', ftInteger);
//  f.Add('USER_NAME', ftString, 40);
//  f.Add('PASSWORD', ftString, 40);
end;

procedure TVmrFtpDeliveryMethod.PrepareToRelease;
begin
  inherited;
  SetLength(FFTPParamArray,0);
end;

procedure TVmrFtpDeliveryMethod.Release(const cd: TEvClientDataSet);
    function GetNextSuffix(const s: string): string;
    var
      res: string;
      i: integer;
    begin
      if s = '' then
        Result := 'a'
      else
      begin
        i := Length(s);
        res := '';
        while res = '' do
        begin
          if i <= 0 then
            res := StringOfChar('a',Length(s)+1)
          else
          if Copy(s,i,1) <> 'z' then
            res := Copy(s, 1, i-1) + chr(ord(s[i]) + 1) + StringOfChar('a',Length(s)-i)
          else
            i := i - 1;
        end;
        Result := res;
      end;
    end;
    function GetFTPFileName(const s1, s2, ext: string): string;
    begin
      if s2 <> '' then
        Result := Format('%s(%s)%s',[s1,s2,ext])
      else
        Result := s1 + ext;
    end;

    function GetExtension(aData: IevDualStream; AsXML: Integer): string;
    begin
      if Assigned(aData) then
        if IsPDFFormat(aData) then
          Result := '.pdf'
        else if (AsXML=1) and IsXMLFormat(aData) then
          Result := '.xml'
        else
          Result := '.rwa'
      else
        Result := '';
    end;

    function GetASCIIFileName(const RepResult: TrwReportResult; var AFileName: string; var AExt: string): boolean;
    var
      FileName: String;
      Ext: String;
    begin
      Result := Length(Trim(RepResult.Tag))>0;
      if not Result then
        exit;
      FileName := AFileName;
      Ext := AExt;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired('OPTION_TAG='''+VmrContentASCII_FileName+''' and SB_MAIL_BOX_CONTENT_NBR='+RepResult.Tag);
      Result := (DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.RecordCount>0);
      if Result then
      begin
        FileName := ExtractFileName(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName('OPTION_STRING_VALUE').AsString);
        if Pos('.',FileName)>0 then
        begin
          Ext := ExtractFileExt(FileName);
          SetLength(FileName,Length(FileName)-Length(Ext));
        end;
        // Check generated filename
        DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired('OPTION_TAG='''+VmrContentASCII_FileName_Generated+''' and SB_MAIL_BOX_CONTENT_NBR='+RepResult.Tag);
        Result := (DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.RecordCount = 0);
        if not Result then
          if DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName('OPTION_INTEGER_VALUE').IsNull then
            Result := true
          else
            Result := DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName('OPTION_INTEGER_VALUE').AsInteger <> 1;
        if Result then
        begin
          AFileName := FileName;
          AExt := Ext;
        end;
      end;
    end;

    procedure PublishViaFTP(const R: TrwReportResults; const useASCIIFileName :Boolean);
    var
      F: TisFTP;
      s: string;
      i,j: Integer;
      l:IisStringList;
      fn:string;
      ext:string;
      crtFolders: Integer;
      asXML: Integer;
      suffix: string;
    begin

      F := TisFTP.Create;
      try
        F.User := Options.FieldByName('USER_NAME').AsString;
        F.Password := Options.FieldByName('PASSWORD').AsString;
        F.Host := Options.FieldByName('FTP_SERVER').AsString;
        F.Connect;
        if Trim(ExtraOptions.FieldByName('PATH').AsString) <>'' then
          F.ChangeDir(ExtraOptions.FieldByName('PATH').AsString);
        crtFolders := ConvertNull(ExtraOptions.FieldByName('CREATE_FOLDERS').Value,1);
        asXML := ConvertNull(ExtraOptions.FieldByName('SEND_PURE_XML').Value,0);
        if crtFolders = 1 then
        begin
          s := FormatDateTime('MM_DD_YYYY__HH_NN_SS__ZZZ', SysTime);
          F.MakeDir(s);
          F.ChangeDir(s);
        end;
        F.ReadDirectory();
        F.DirectoryListing.CaseSensitive := false;
        l := TisStringList.Create;
        for i := 0 to R.Count-1 do
          if Assigned(R[i].Data) then
          begin
            ext := GetExtension(R[i].Data, asXML);
            fn := '';
            if useASCIIFileName then
              GetASCIIFileName(R[i], fn, ext);
            if fn = '' then
              fn := StringReplace(R[i].VmrJobDescr+ '-'+ R[i].ReportName,'.','_',[rfReplaceAll]);
            fn := ValidateFileName(fn);
            if l.IndexOf(fn) <> -1  then
            begin
               j := 1;
               while l.IndexOf(fn+'_'+IntToStr(j)) <> -1 do
                  j := j + 1;
               fn := fn+'_'+IntToStr(j);
            end;
            l.Add(fn);
            suffix := '';
            while F.DirectoryListing.IndexOf(GetFTPFileName(fn, suffix, ext)) <> -1 do
                suffix := GetNextSuffix(suffix);
            fn := GetFTPFileName(fn, suffix, ext);
            F.Put(R[i].Data,fn  );
          end;
        if crtFolders = 1 then
          F.ChangeDir('..');
      finally
        F.Free;
      end;
    end;

    function AbsPath(const CurrentDir :String; const FileName: string): string;
    begin
      if (Length(CurrentDir) = 0) or ((CurrentDir[Length(CurrentDir)] <> '\') and (CurrentDir[Length(CurrentDir)] <> '/')) then
        result := CurrentDir + '/' + FileName
      else
        result := CurrentDir + FileName;
    end;

    procedure PublishViaSFTP(const R: TrwReportResults; const useASCIIFileName :Boolean);
    const SFTPBufferSize = 65536;
    var
      FSFTP: TElSimpleSFTPClient;
      i,j: Integer;
      l:IisStringList;
      fn:string;
      ext:string;
      crtFolders: Integer;
      asXML: Integer;
      suffix: string;
      FtpFolder: string;
      h: TSBSftpFileHandle;
      dt: TDateTime;
      Attrs: TElSftpFileAttributes;
      dirList: TList;
      sdirList: iIsStringList;
      buf: array [0..SFTPBufferSize-1] of Byte;
      len, offset: Integer;
    begin

      FSFTP := TElSimpleSFTPClient.Create(nil);
      Attrs := TElSftpFileAttributes.Create;
      dirList := TList.Create();
      try
        FSFTP.Username := Options.FieldByName('USER_NAME').AsString;
        FSFTP.Password := Options.FieldByName('PASSWORD').AsString;
        FSFTP.Address := Options.FieldByName('FTP_SERVER').AsString;
        try
          FSFTP.Open;
        except on E:Exception do
          raise Exception.CreateFmt('Error: ''%s'' while connecting to the SFTP-server %s.',[E.Message, FSFTP.Address]);
        end;
        if Trim(ExtraOptions.FieldByName('PATH').AsString) <> '' then
        begin
          FtpFolder := ExtraOptions.FieldByName('PATH').AsString;
          if (FtpFolder[1]<>'/') and (FTPFolder[1]<>'\') then
            FtpFolder := '/'+FtpFolder;
          try
            h := FSFTP.OpenDirectory(FtpFolder);
            FSFTP.CloseHandle(h);
          except on E:Exception do
            raise Exception.CreateFmt('Error: ''%s'' while checking ''%s'' on the SFTP-server %s.',[E.Message,FtpFolder,FSFTP.Address]);
          end;
        end
        else
          FtpFolder := '/';

        crtFolders := ConvertNull(ExtraOptions.FieldByName('CREATE_FOLDERS').Value,1);
        asXML := ConvertNull(ExtraOptions.FieldByName('SEND_PURE_XML').Value,0);

        if crtFolders = 1 then
        begin
          dt := SysTime;
          Attrs.MTime := dt;
          Attrs.ATime := Attrs.MTime;
          Attrs.CTime := Attrs.MTime;
          Attrs.CATime := Attrs.MTime;
          Attrs.IncludedAttributes := [saATime, saMTime, saCTime, saCATime];
          FtpFolder := AbsPath(FtpFolder, FormatDateTime('MM_DD_YYYY__HH_NN_SS__ZZZ', dt));
          try
            FSFTP.MakeDirectory(FtpFolder, Attrs);
          except on E:Exception do
            raise Exception.CreateFmt('Error: ''%s'' while creating directory ''%s'' on the SFTP-server %s.',[E.Message,FtpFolder,FSFTP.Address]);
          end;
        end;

        try
          h := FSFTP.OpenDirectory(FtpFolder);
          FSFTP.ReadDirectory(h, dirList);
          FSFTP.CloseHandle(h);
        except on E:Exception do
          raise Exception.CreateFmt('Error: ''%s'' while reading directory ''%s'' on the SFTP-server %s.',[E.Message,FtpFolder,FSFTP.Address]);
        end;
        sdirList := TisStringList.Create();
        for i:=0 to dirlist.Count-1 do
        begin
          if (TElSftpFileInfo(dirList.Items[i]).Name<>'.') and (TElSftpFileInfo(dirList.Items[i]).Name<>'..') then
            sdirList.Add(TElSftpFileInfo(dirList.Items[i]).Name);
          TElSftpFileInfo(dirList.Items[i]).Free();
        end;
        sdirList.CaseSensitive := false;
        l := TisStringList.Create;
        for i := 0 to R.Count-1 do
          if Assigned(R[i].Data) then
          begin
            ext := GetExtension(R[i].Data, asXML);
            fn := '';
            if useASCIIFileName then
              GetASCIIFileName(R[i], fn, ext);
            if fn = '' then
              fn := StringReplace(R[i].VmrJobDescr+ '-'+ R[i].ReportName,'.','_',[rfReplaceAll]);
            fn := ValidateFileName(fn);
            if l.IndexOf(fn) <> -1  then
            begin
               j := 1;
               while l.IndexOf(fn+'_'+IntToStr(j)) <> -1 do
                  j := j + 1;
               fn := fn+'_'+IntToStr(j);
            end;
            l.Add(fn);
            suffix := '';
            while sdirList.IndexOf(GetFTPFileName(fn, suffix, ext)) <> -1 do
                suffix := GetNextSuffix(suffix);
            fn := AbsPath(FtpFolder, GetFTPFileName(fn, suffix, ext));

            try
              h := FSFTP.CreateFile(fn);
            except on E:Exception do
              raise Exception.CreateFmt('Error: ''%s'' while saving ''%s'' to the SFTP-server %s.',[E.Message, fn, FSFTP.Address]);
            end;
            offset := 0;
            R[i].Data.Seek(0,soBeginning);
            repeat
              len := R[i].Data.Read(buf, SFTPBufferSize);
              try
               FSFTP.Write(h, offset, @buf[0], len);
              except on E:Exception do
                raise Exception.CreateFmt('Error: ''%s'' while writing ''%s'' to the SFTP-server %s.',[E.Message,fn,FSFTP.Address]);
              end;
              offset := offset + len;
            until len<SFTPBufferSize;
            FSFTP.CloseHandle(h);
          end;
      finally
        dirlist.Free();
        Attrs.Free();
        FSFTP.Free;
      end;
    end;

var
  i:integer;
  store_fl: Boolean;
begin
  inherited;
  try
    store_fl := ConvertNull(ExtraOptions.FieldByName('SAVE_ASCII').AsInteger,0) = 1;
    if vrtPrinter in ReleaseType then
    begin
      if ConvertNull(Options.FieldByName('USE_SFTP').Value,0) = 0 then
        for i := 0 to High(FFTPParamArray) do
          PublishViaFTP(FFTPParamArray[i], store_fl)
      else
        for i := 0 to High(FFTPParamArray) do
          PublishViaSFTP(FFTPParamArray[i], store_fl);
    end;

  finally
    for i := 0 to High(FFTPParamArray) do
      FFTPParamArray[i].Free;
    SetLength(FFTPParamArray, 0);
  end;

end;


{
*************************** TVmrEmailDeliveryMethod ****************************
}
procedure TVmrEmailDeliveryMethod.InternalAddToRelease(const Media: TVmrMediaType;
        const cd: TEvClientDataSet; const RL: TrwReportResults);
var
  aRL,ASCIIRL: TrwReportResults;
  UsePdf:boolean;
  i:integer;
begin
  Assert(Media is TVmrEMedia);

  aRL := ctx_RWLocalEngine.MergeGraphicReports(RL,'Payroll reports');

  ASCIIRL := TrwReportResults.Create;
  UsePdf := ConvertNull(TVmrEMedia(Media).ExtraOptions.FieldByName('CONVERT_TO_PDF').Value,'N')='Y';
  if UsePdf {and not (vrtStoreASCIIFiles in ReleaseType)} then
  begin
      i := 0;
      while i < aRL.Count do
      begin
        if aRL[i].ReportType = rtASCIIFile then
          aRL[i].Collection := ASCIIRL
        else
          Inc(i);
      end;
     TVmrEMedia(Media).ConvertToPDF(aRL);

     AssignVmrFileNames(AsciiRL, IsIncludeDescInEmail(BoxNbr));

     TVmrEMedia(Media).ConvertToZip(AsciiRL);
     if AsciiRL.Count >0 then
     begin
       while AsciiRL.Count >0 do
         AsciiRL[0].Collection := aRL;
     end else
       AsciiRL.Free;

  end else
     TVmrEMedia(Media).Encrypt(aRL);

  SetLength(FEmailParamArray, Length(FEmailParamArray)+1);
  FEmailParamArray[High(FEmailParamArray)] := TrwReportResults.Create;
  aRL.MoveTo(FEmailParamArray[High(FEmailParamArray)]);
  cd['SCANNED_TIME'] := cd['PRINTED_TIME'];
  cd['SHIPPED_TIME'] := cd['PRINTED_TIME'];
end;

function TVmrEmailDeliveryMethod.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrEmail2EditFrame;
end;

function TVmrEmailDeliveryMethod.GetOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrEmailEditFrame;
end;

procedure TVmrEmailDeliveryMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := 'Email to '+ ExtraOptions.FieldByName('EMAIL_ADDRESS').AsString;
end;

procedure TVmrEmailDeliveryMethod.PrepareExtraOptionFields(var Prefix: string;
        var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('EMAIL_ADDRESS', ftString, 80);
  f.Add('SAVE_ASCII', ftInteger);
end;

procedure TVmrEmailDeliveryMethod.PrepareOptionFields(var LoadRecordCount:
        Integer; const f: TFieldDefs);
begin
  inherited PrepareOptionFields(LoadRecordCount, f);
end;

procedure TVmrEmailDeliveryMethod.PrepareToRelease;
begin
  inherited;
  SetLength(FEmailParamArray, 0);
end;

procedure TVmrEmailDeliveryMethod.Release(const cd: TEvClientDataSet);
var
  store_fl,send_em:boolean;

  procedure Send(const R: TrwReportResults);
  begin
    if send_em then
    begin
        EmailReportResults(R, Options.FieldByName('FROM_ADDRESS').AsString,
              ExtraOptions.FieldByName('EMAIL_ADDRESS').AsString, false);
    end;
    if store_fl then
    begin
         StoreReportResults(R,not send_em);
    end;
  end;
var
  i: Integer;
begin
  inherited;
  send_em :=  ConvertNull(Trim(ExtraOptions.FieldByName('EMAIL_ADDRESS').AsString),'') <> '';
  store_fl := ConvertNull(ExtraOptions.FieldByName('SAVE_ASCII').AsInteger,0) = 1;
  if (not Send_em) and (vrtPrinter in ReleaseType) then
    Raise Exception.Create('Email address is empty!');
  try
    if vrtPrinter in ReleaseType then
      for i := 0 to High(FEmailParamArray) do
      begin
        Send(FEmailParamArray[i]);
        MarkSentEmailContent(FEmailParamArray[i]);
      end;
  finally
    for i := 0 to High(FEmailParamArray) do
      FEmailParamArray[i].Free;
    SetLength(FEmailParamArray, 0);
  end;
end;

{ TVmrDirectEmailDeliveryMethod }

procedure TVmrDirectEmailDeliveryMethod.InternalAddToRelease(
  const Media: TVmrMediaType; const cd: TEvClientDataSet; const RL: TrwReportResults);
var
  s: string;
begin
  Assert(Media is TVmrEMedia);
  Assert(RL.Count > 0);
  if RL[0].VmrTag = '' then
     raise EInvalidParameters.CreateHelp('Some of content can not be delivered by this delivery method,'+#13+'because for this content can not be found required settings.'+#13+'Please, use other delivery method.', IDH_InvalidParameters)
    //raise EInvalidParameters.CreateHelp('Could not find settings for Content line:'+ RL[0].VmrJobDescr, IDH_InvalidParameters);
    //raise EInvalidParameters.CreateHelp('Could not find settings for EE_NBR='+ IntToStr(RL[0].VmrEeNbr), IDH_InvalidParameters)
  else
  begin
    s := RL[0].VmrTag;
    Fetch(s, ';');
    raise EInvalidParameters.CreateHelp('Web password and email is not set for employee '+ Fetch(s, ';'), IDH_InvalidParameters);
  end;
end;

function TVmrDirectEmailDeliveryMethod.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TVmrEmailEditFrame;
end;

procedure TVmrDirectEmailDeliveryMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := 'Direct email';
end;

procedure TVmrDirectEmailDeliveryMethod.PrepareOptionFields(
  var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareOptionFields(LoadRecordCount, f);
end;

procedure TVmrDirectEmailDeliveryMethod.PrepareToRelease;
begin
  inherited;
end;

procedure TVmrDirectEmailDeliveryMethod.Release(
  const cd: TEvClientDataSet);
begin
  inherited;
end;

procedure TVmrEDelivery.InternalAddToRelease(const Media: TVmrMediaType;
  const cd: TEvClientDataSet; const RL: TrwReportResults);
begin
  inherited;
  Inc(FPackagedEmailCounter, RL.Count);
end;

procedure TVmrEDelivery.RevertToUnreleased(const Media: TVmrMediaType;
  const cd: TEvClientDataSet);
begin
  cd.Edit;
  cd['PRINTED_TIME'] := Null;
  cd.Post;
end;

{ TVmrWebDeliveryMethod }

function TVmrWebDeliveryMethod.GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TVMRWebExtraOptionFrame;
end;

function TVmrWebDeliveryMethod.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TVmrEmailEditFrame;
end;

procedure TVmrWebDeliveryMethod.InternalAddToRelease(
  const Media: TVmrMediaType; const cd: TEvClientDataSet;
  const RL: TrwReportResults);
begin
  Assert(Media is TVmrEMedia);
  cd['SCANNED_TIME'] := cd['PRINTED_TIME'];
  cd['SHIPPED_TIME'] := cd['PRINTED_TIME'];
end;

procedure TVmrWebDeliveryMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := 'Send to Web';
end;

procedure TVmrWebDeliveryMethod.PrepareExtraOptionFields(
  var Prefix: string; var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('WEB_NOTES', ftString, 255);
end;

procedure TVmrWebDeliveryMethod.PrepareOptionFields(
  var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareOptionFields(LoadRecordCount, f);
end;

procedure TVmrWebDeliveryMethod.PrepareToRelease;
begin
  inherited;
end;

procedure TVmrWebDeliveryMethod.Release(const cd: TEvClientDataSet);
const
  PrSQL = ' Select max(CAST(Trim(StrCopy(T1.DESCRIPTION, 200, 8)) as VARCHAR(8))) CheckDate '+
          ' from SB_MAIL_BOX_CONTENT T1 where {AsOfNow<T1>} '+
          ' and CAST(Trim(StrCopy(T1.DESCRIPTION, 218, 10)) as INTEGER) > 0 '+
          ' and T1.SB_MAIL_BOX_NBR = :NBR';

  TaxReturnSQL = ' Select max(CAST(Trim(StrCopy(T1.DESCRIPTION, 200, 8)) as VARCHAR(8))) TaxEndingDate '+
          ' from SB_MAIL_BOX_CONTENT T1 where {AsOfNow<T1>} '+
          ' and CAST(Trim(StrCopy(T1.DESCRIPTION, 218, 10)) as INTEGER) <= 0 '+
          ' and T1.SB_MAIL_BOX_NBR = :NBR';
var
  qry: IevQuery;
  CheckDate, TaxEndingDate : TDateTime;
begin
  inherited;

  if not (vrtPrinter in ReleaseType) then
     exit;

  qry := TevQuery.Create(PrSQL);
  qry.Params.AddValue('NBR',cd['SB_MAIL_BOX_NBR']);
  qry.Execute;
  CheckDate := GetEventDate(qry.Result.Fields[0].asString);

  qry := TevQuery.Create(TaxReturnSQL);
  qry.Params.AddValue('NBR',cd['SB_MAIL_BOX_NBR']);
  qry.Execute;
  TaxEndingDate := GetEventDate(qry.Result.Fields[0].asString);

  if (CheckDate > 0) or (TaxEndingDate > 0) then
    SendNotificationEmail(Options.FieldByName('FROM_ADDRESS').asString,
                          cd.FieldByName('NOTIFICATION_EMAIL').asString,
                          ExtraOptions.FieldByName('Web_NOTES').asString,
                          CheckDate, TaxEndingDate);
end;

initialization
  RegisterVmrClass(TVmrEDeliveryService);
  RegisterVmrClass(TVmrEmailDeliveryMethod);
  RegisterVmrClass(TVmrFtpDeliveryMethod);
  RegisterVmrClass(TVmrEMedia);
  RegisterVmrClass(TVmrDirectEmailDeliveryMethod);
  RegisterVmrClass(TVmrWebDeliveryMethod);
end.

