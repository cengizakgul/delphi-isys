inherited VmrFtp2EditFrame: TVmrFtp2EditFrame
  Width = 354
  Height = 180
  object evLabel1: TevLabel [0]
    Left = 8
    Top = 8
    Width = 98
    Height = 13
    Caption = 'FTP server directory:'
  end
  object evDBEdit1: TevDBEdit [1]
    Left = 8
    Top = 32
    Width = 313
    Height = 21
    DataField = 'PATH'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBCheckBox1: TevDBCheckBox [2]
    Left = 11
    Top = 63
    Width = 373
    Height = 17
    Caption = 'Create folder for each Release'
    DataField = 'CREATE_FOLDERS'
    DataSource = dsMain
    TabOrder = 1
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object evDBCheckBox2: TevDBCheckBox [3]
    Left = 11
    Top = 87
    Width = 373
    Height = 17
    Caption = 'Do not convert XML files to RWA'
    DataField = 'SEND_PURE_XML'
    DataSource = dsMain
    TabOrder = 2
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object evDBCheckBox3: TevDBCheckBox [4]
    Left = 11
    Top = 111
    Width = 373
    Height = 17
    Caption = 'Use Output ASCII File Name for ASCII results'
    DataField = 'SAVE_ASCII'
    DataSource = dsMain
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
end
