unit SVmrDHLAddressedEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrAddressedEditFrame, SDDClasses, SDataDictsystem,
  SDataStructure, DB, Wwdatsrc, ISBasicClasses,  StdCtrls,
  DBCtrls, Buttons, wwdblook, Mask, wwdbedit, EvUIComponents;

type
  TVmrDHLAddressedEditFrame = class(TVmrAddressedEditFrame)
    evDBEdit6: TevDBEdit; 
    lbPhone: TevLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses SVmrOptionEditFrame,SVmrClasses, EvUtils;

{$R *.dfm}


initialization
  RegisterVmrClass(TVmrDHLAddressedEditFrame);

end.
