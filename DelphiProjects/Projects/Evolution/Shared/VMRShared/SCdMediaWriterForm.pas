// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SCdMediaWriterForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  mbDrvLib, mbCDBC, ExtCtrls, EvContext,
  ISBasicClasses, EvMainboard, EvExceptions, ComCtrls, EvUIComponents;

const
  WM_WRITE_DONE = WM_USER+ $55;
type
  TCdMediaWriterForm = class(TForm)
    evLabel1: TevLabel;
    cbChooseDrive: TevComboBox;
    lMessage: TevLabel;
    bGo: TevButton;
    bCancel: TevButton;
    MCDB: TMCDBurner;
    Timer1: TTimer;
    cbAutoBurn: TevCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure bGoClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MCDBWriteDone(Sender: TObject; Error: String);
  private
    FPath: string;
    FErrorMessage: string;
    procedure DoWmWriteDone(var Msg: TMsg); message WM_WRITE_DONE;
    //FFiles: TStringList;
    //procedure SetFiles(const Value: TStringList);
    { Private declarations }
  public
    { Public declarations }
    property Path: string read FPath write FPath;
    //property Files: TStringList read FFiles write SetFiles;
  end;

function BurnDirectoryToCd(const Path: string): Boolean;
function CheckIfCdRwIsPresented: Boolean;

implementation

{$R *.dfm}

uses EvTypes, EvUtils, EvBasicUtils;

function CheckIfCdRwIsPresented: Boolean;
var
  i: Integer;
  mcdb: TMCDBurner;
begin
  Result := False;
  mcdb := TMCDBurner.Create(nil);
  try
    if mcdb.InitializeASPI(True) then
    begin
      for i := Pred(mcdb.Devices.Count) downto 0 do
      begin
        mcdb.Device := mcdb.Devices[i];
        if mcdb.DeviceCapabilities * [dcWriteCDR, dcWriteCDRW, dcWriteDVDR] <> [] then
        begin
          Result := True;
          Break;
        end;
      end;
    end;
  finally
    mcdb.Free;
  end;
end;

function BurnDirectoryToCd(const Path: string): Boolean;
var
  F: TCdMediaWriterForm;
begin
  F := TCdMediaWriterForm.Create(nil);
  try
    F.Path := Path;
    Result := F.ShowModal = mrOk;
  finally
    F.Free;
  end;
end;

procedure TCdMediaWriterForm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  if mcdb.InitializeASPI(True) then
  begin
    cbChooseDrive.Items.Assign(mcdb.Devices);
    for i := Pred(cbChooseDrive.Items.Count) downto 0 do
    begin
      mcdb.Device := cbChooseDrive.Items[i];
      if mcdb.DeviceCapabilities * [dcWriteCDR, dcWriteCDRW, dcWriteDVDR] = [] then
        cbChooseDrive.Items.Delete(i);
    end;
    cbChooseDrive.Enabled := cbChooseDrive.Items.Count > 0;
  end
  else
    cbChooseDrive.Enabled := False;
  if not cbChooseDrive.Enabled then
    raise EDeviceNotAvailable.Create('You do not have any CD/DVD writers available');
  cbChooseDrive.ItemIndex := mb_AppSettings.AsInteger['Settings\LastUsedCdWriter'];
  cbAutoBurn.Checked := mb_AppSettings['Settings\AutoBurnStart'] = 'Y';
  //FFiles := TStringList.Create;
end;

procedure TCdMediaWriterForm.bGoClick(Sender: TObject);
{var
  i: Integer;}
begin
  Timer1.Enabled := False;
  mb_AppSettings.AsInteger['Settings\LastUsedCdWriter'] := cbChooseDrive.ItemIndex;
  mb_AppSettings['Settings\AutoBurnStart'] := BoolToYN(cbAutoBurn.Checked);
  lMessage.Caption := '';
  lMessage.Update;
  ctx_StartWait('Preparing to write ...');
  {bGo.Enabled := False;
  bGo.Update;
  bCancel.Enabled := False;
  bCancel.Update;}
  try
    MCDB.ClearAll;
    {for i := 0 to Files.Count-1 do
    begin
      Assert(Assigned(MCDB.CreateDir()}
    MCDB.InsertDir('', Path);
    Assert(MCDB.Prepare(False));
    Assert(MCDB.BurnCD);
    lMessage.Caption := 'Writing';
    ctx_UpdateWait('Writing...');
  except
    //bCancel.Enabled := True;
    ctx_EndWait;
    mcdb.LoadMedium(True);
    raise;
  end;
end;

procedure TCdMediaWriterForm.Timer1Timer(Sender: TObject);
begin
  mcdb.Device := cbChooseDrive.Text;
  if not mcdb.TestUnitReady then
    lMessage.Caption := 'Load compatible blank media'
  else
    if not (mcdb.DiscType in [2,3,5,7,8]) then
    begin
      lMessage.Caption := 'Load compatible blank media';
      mcdb.LoadMedium(True);
    end
    else
    if mcdb.SessionsOnDisc > 0 then
    begin
      lMessage.Caption := 'Media is not blank.';
      lMessage.Update;
      mcdb.LoadMedium(True);
      Sleep(4000);
    end
    else
    begin
      lMessage.Caption := 'Ready. Press "Write" button';
      bGo.Enabled := True;
      if cbAutoBurn.Checked then
        bGo.Click;
    end;
end;

procedure TCdMediaWriterForm.FormShow(Sender: TObject);
begin
  //Assert(Files.Count > 0);
  mcdb.LoadMedium(True);
  bGo.Enabled := False;
  Timer1.Enabled := True;
end;

{procedure TCdMediaWriterForm.SetFiles(const Value: TStringList);
begin
  FFiles.Assign(Value);
end;}

procedure TCdMediaWriterForm.FormDestroy(Sender: TObject);
begin
  //FreeAndNil(FFiles);
end;

procedure TCdMediaWriterForm.DoWmWriteDone(var Msg: TMsg);
begin
  ctx_EndWait;
  mcdb.LoadMedium(True);
  if FErrorMessage <> '' then
    raise EDeviceIOException.Create(FErrorMessage);
  ModalResult := mrOk;
end;

procedure TCdMediaWriterForm.MCDBWriteDone(Sender: TObject; Error: String);
begin
  FErrorMessage := Error;
  PostMessage(Handle, WM_WRITE_DONE, 0, 0);
end;

end.

