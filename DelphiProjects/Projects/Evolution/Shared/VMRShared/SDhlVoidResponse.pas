
{*******************************************************************************************************}
{                                                                                                       }
{                                           XML Data Binding                                            }
{                                                                                                       }
{         Generated on: 11/18/2004 2:58:45 PM                                                           }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\VoidResponse.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\VoidResponse.xdb   }
{                                                                                                       }
{*******************************************************************************************************}

unit SDhlVoidResponse;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLShipmentType = interface;
  IXMLFaultsType = interface;
  IXMLFaultType = interface;
  IXMLFaultTypeList = interface;
  IXMLResultType = interface;
  IXMLShippingCredentialsType = interface;
  IXMLShipmentDetailType = interface;
  IXMLShipmentDetailTypeList = interface;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{DD77D858-8DB5-47DF-AE19-A1FBE13A3F94}']
    { Property Accessors }
    function Get_Action: string;
    function Get_Version: string;
    function Get_Test: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Result: IXMLResultType;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailTypeList;
    function Get_TransactionTrace: string;
    { Methods & Properties }
    property Action: string read Get_Action;
    property Version: string read Get_Version;
    property Test: string read Get_Test;
    property Faults: IXMLFaultsType read Get_Faults;
    property Result: IXMLResultType read Get_Result;
    property ShippingCredentials: IXMLShippingCredentialsType read Get_ShippingCredentials;
    property ShipmentDetail: IXMLShipmentDetailTypeList read Get_ShipmentDetail;
    property TransactionTrace: string read Get_TransactionTrace;
  end;

{ IXMLFaultsType }

  IXMLFaultsType = interface(IXMLNodeCollection)
    ['{6ADAEC7C-9DFB-49AD-94E8-B39EED5347DB}']
    { Property Accessors }
    function Get_Fault(Index: Integer): IXMLFaultType;
    { Methods & Properties }
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
    property Fault[Index: Integer]: IXMLFaultType read Get_Fault; default;
  end;

{ IXMLFaultType }

  IXMLFaultType = interface(IXMLNode)
    ['{5C2DA887-FA6D-4C1D-8BEA-28A3D0080881}']
    { Property Accessors }
    function Get_Source: string;
    function Get_Code: Integer;
    function Get_Desc: string;
    function Get_Context: string;
    { Methods & Properties }
    property Source: string read Get_Source;
    property Code: Integer read Get_Code;
    property Desc: string read Get_Desc;
    property Context: string read Get_Context;
  end;

{ IXMLFaultTypeList }

  IXMLFaultTypeList = interface(IXMLNodeCollection)
    ['{3EF941FC-4CD4-4F05-A914-C798FB875063}']
    { Methods & Properties }
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
    function Get_Item(Index: Integer): IXMLFaultType;
    property Items[Index: Integer]: IXMLFaultType read Get_Item; default;
  end;

{ IXMLResultType }

  IXMLResultType = interface(IXMLNode)
    ['{0E6C6493-C125-415B-B1D8-E8302AA95458}']
    { Property Accessors }
    function Get_Code: Integer;
    function Get_Desc: string;
    { Methods & Properties }
    property Code: Integer read Get_Code;
    property Desc: string read Get_Desc;
  end;

{ IXMLShippingCredentialsType }

  IXMLShippingCredentialsType = interface(IXMLNode)
    ['{6BB32597-FCAC-4378-A937-6A7A8885805D}']
    { Property Accessors }
    function Get_ShippingKey: string;
    function Get_AccountNbr: string;
    { Methods & Properties }
    property ShippingKey: string read Get_ShippingKey;
    property AccountNbr: string read Get_AccountNbr;
  end;

{ IXMLShipmentDetailType }

  IXMLShipmentDetailType = interface(IXMLNode)
    ['{9014923F-CB0F-4F43-A020-A4504C5B8FF3}']
    { Property Accessors }
    function Get_AirbillNbr: string;
    { Methods & Properties }
    property AirbillNbr: string read Get_AirbillNbr;
  end;

{ IXMLShipmentDetailTypeList }

  IXMLShipmentDetailTypeList = interface(IXMLNodeCollection)
    ['{8808F2A8-431E-421B-BDED-6E32A84FD1DE}']
    { Methods & Properties }
    function Add: IXMLShipmentDetailType;
    function Insert(const Index: Integer): IXMLShipmentDetailType;
    function Get_Item(Index: Integer): IXMLShipmentDetailType;
    property Items[Index: Integer]: IXMLShipmentDetailType read Get_Item; default;
  end;

{ Forward Decls }

  TXMLShipmentType = class;
  TXMLFaultsType = class;
  TXMLFaultType = class;
  TXMLFaultTypeList = class;
  TXMLResultType = class;
  TXMLShippingCredentialsType = class;
  TXMLShipmentDetailType = class;
  TXMLShipmentDetailTypeList = class;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  private
    FShipmentDetail: IXMLShipmentDetailTypeList;
  protected
    { IXMLShipmentType }
    function Get_Action: string;
    function Get_Version: string;
    function Get_Test: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Result: IXMLResultType;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailTypeList;
    function Get_TransactionTrace: string;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultsType }

  TXMLFaultsType = class(TXMLNodeCollection, IXMLFaultsType)
  protected
    { IXMLFaultsType }
    function Get_Fault(Index: Integer): IXMLFaultType;
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultType }

  TXMLFaultType = class(TXMLNode, IXMLFaultType)
  protected
    { IXMLFaultType }
    function Get_Source: string;
    function Get_Code: Integer;
    function Get_Desc: string;
    function Get_Context: string;
  end;

{ TXMLFaultTypeList }

  TXMLFaultTypeList = class(TXMLNodeCollection, IXMLFaultTypeList)
  protected
    { IXMLFaultTypeList }
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
    function Get_Item(Index: Integer): IXMLFaultType;
  end;

{ TXMLResultType }

  TXMLResultType = class(TXMLNode, IXMLResultType)
  protected
    { IXMLResultType }
    function Get_Code: Integer;
    function Get_Desc: string;
  end;

{ TXMLShippingCredentialsType }

  TXMLShippingCredentialsType = class(TXMLNode, IXMLShippingCredentialsType)
  protected
    { IXMLShippingCredentialsType }
    function Get_ShippingKey: string;
    function Get_AccountNbr: string;
  end;

{ TXMLShipmentDetailType }

  TXMLShipmentDetailType = class(TXMLNode, IXMLShipmentDetailType)
  protected
    { IXMLShipmentDetailType }
    function Get_AirbillNbr: string;
  end;

{ TXMLShipmentDetailTypeList }

  TXMLShipmentDetailTypeList = class(TXMLNodeCollection, IXMLShipmentDetailTypeList)
  protected
    { IXMLShipmentDetailTypeList }
    function Add: IXMLShipmentDetailType;
    function Insert(const Index: Integer): IXMLShipmentDetailType;
    function Get_Item(Index: Integer): IXMLShipmentDetailType;
  end;

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
function LoadShipment(const FileName: WideString): IXMLShipmentType;
function NewShipment: IXMLShipmentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
begin
  Result := Doc.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function LoadShipment(const FileName: WideString): IXMLShipmentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function NewShipment: IXMLShipmentType;
begin
  Result := NewXMLDocument.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('Faults', TXMLFaultsType);
  RegisterChildNode('Result', TXMLResultType);
  RegisterChildNode('ShippingCredentials', TXMLShippingCredentialsType);
  RegisterChildNode('ShipmentDetail', TXMLShipmentDetailType);
  FShipmentDetail := CreateCollection(TXMLShipmentDetailTypeList, IXMLShipmentDetailType, 'ShipmentDetail') as IXMLShipmentDetailTypeList;
  inherited;
end;

function TXMLShipmentType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

function TXMLShipmentType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

function TXMLShipmentType.Get_Test: string;
begin
  Result := AttributeNodes['test'].Text;
end;

function TXMLShipmentType.Get_Faults: IXMLFaultsType;
begin
  Result := ChildNodes['Faults'] as IXMLFaultsType;
end;

function TXMLShipmentType.Get_Result: IXMLResultType;
begin
  Result := ChildNodes['Result'] as IXMLResultType;
end;

function TXMLShipmentType.Get_ShippingCredentials: IXMLShippingCredentialsType;
begin
  Result := ChildNodes['ShippingCredentials'] as IXMLShippingCredentialsType;
end;

function TXMLShipmentType.Get_ShipmentDetail: IXMLShipmentDetailTypeList;
begin
  Result := FShipmentDetail;
end;

function TXMLShipmentType.Get_TransactionTrace: string;
begin
  Result := ChildNodes['TransactionTrace'].Text;
end;

{ TXMLFaultsType }

procedure TXMLFaultsType.AfterConstruction;
begin
  RegisterChildNode('Fault', TXMLFaultType);
  ItemTag := 'Fault';
  ItemInterface := IXMLFaultType;
  inherited;
end;

function TXMLFaultsType.Get_Fault(Index: Integer): IXMLFaultType;
begin
  Result := List[Index] as IXMLFaultType;
end;

function TXMLFaultsType.Add: IXMLFaultType;
begin
  Result := AddItem(-1) as IXMLFaultType;
end;

function TXMLFaultsType.Insert(const Index: Integer): IXMLFaultType;
begin
  Result := AddItem(Index) as IXMLFaultType;
end;

{ TXMLFaultType }

function TXMLFaultType.Get_Source: string;
begin
  Result := ChildNodes['Source'].Text;
end;

function TXMLFaultType.Get_Code: Integer;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

function TXMLFaultType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

function TXMLFaultType.Get_Context: string;
begin
  Result := ChildNodes['Context'].Text;
end;

{ TXMLFaultTypeList }

function TXMLFaultTypeList.Add: IXMLFaultType;
begin
  Result := AddItem(-1) as IXMLFaultType;
end;

function TXMLFaultTypeList.Insert(const Index: Integer): IXMLFaultType;
begin
  Result := AddItem(Index) as IXMLFaultType;
end;
function TXMLFaultTypeList.Get_Item(Index: Integer): IXMLFaultType;
begin
  Result := List[Index] as IXMLFaultType;
end;

{ TXMLResultType }

function TXMLResultType.Get_Code: Integer;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

function TXMLResultType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

{ TXMLShippingCredentialsType }

function TXMLShippingCredentialsType.Get_ShippingKey: string;
begin
  Result := ChildNodes['ShippingKey'].Text;
end;

function TXMLShippingCredentialsType.Get_AccountNbr: string;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

{ TXMLShipmentDetailType }

function TXMLShipmentDetailType.Get_AirbillNbr: string;
begin
  Result := ChildNodes['AirbillNbr'].NodeValue;
end;

{ TXMLShipmentDetailTypeList }

function TXMLShipmentDetailTypeList.Add: IXMLShipmentDetailType;
begin
  Result := AddItem(-1) as IXMLShipmentDetailType;
end;

function TXMLShipmentDetailTypeList.Insert(const Index: Integer): IXMLShipmentDetailType;
begin
  Result := AddItem(Index) as IXMLShipmentDetailType;
end;
function TXMLShipmentDetailTypeList.Get_Item(Index: Integer): IXMLShipmentDetailType;
begin
  Result := List[Index] as IXMLShipmentDetailType;
end;

end.