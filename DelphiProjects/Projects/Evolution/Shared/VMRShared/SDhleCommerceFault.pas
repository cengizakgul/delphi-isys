
{*********************************************************************************************************}
{                                                                                                         }
{                                            XML Data Binding                                             }
{                                                                                                         }
{         Generated on: 11/18/2004 11:51:56 AM                                                            }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\eCommerceFault.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\eCommerceFault.xdb   }
{                                                                                                         }
{*********************************************************************************************************}

unit SDhleCommerceFault;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLECommerceType = interface;
  IXMLFaultsType = interface;
  IXMLFaultType = interface;
  IXMLRequestorType = interface;
  IXMLAirborneBusinessFunctionType = interface;

{ IXMLECommerceType }

  IXMLECommerceType = interface(IXMLNode)
    ['{F7E2AD0D-DB54-4A51-BBD1-71EAF5A546C4}']
    { Property Accessors }
    function Get_Version: string;
    function Get_Transmission_reference: string;
    function Get_Timestamp: string;
    function Get_Action: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Requestor: IXMLRequestorType;
    { Methods & Properties }
    property Version: string read Get_Version;
    property Transmission_reference: string read Get_Transmission_reference;
    property Timestamp: string read Get_Timestamp;
    property Action: string read Get_Action;
    property Faults: IXMLFaultsType read Get_Faults;
    property Requestor: IXMLRequestorType read Get_Requestor;
  end;

{ IXMLFaultsType }

  IXMLFaultsType = interface(IXMLNodeCollection)
    ['{55329479-52AC-4C6A-BCFE-7336008DFC1B}']
    { Property Accessors }
    function Get_Fault(Index: Integer): IXMLFaultType;
    { Methods & Properties }
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
    property Fault[Index: Integer]: IXMLFaultType read Get_Fault; default;
  end;

{ IXMLFaultType }

  IXMLFaultType = interface(IXMLNode)
    ['{CF5DA03C-4DC2-48CE-871E-AE6678283F69}']
    { Property Accessors }
    function Get_Source: string;
    function Get_Code: string;
    function Get_Description: string;
    function Get_Context: string;
    { Methods & Properties }
    property Source: string read Get_Source;
    property Code: string read Get_Code;
    property Description: string read Get_Description;
    property Context: string read Get_Context;
  end;

{ IXMLRequestorType }

  IXMLRequestorType = interface(IXMLNode)
    ['{14D3B96A-204D-4EC5-B205-561B822C3A92}']
    { Property Accessors }
    function Get_ID: string;
    function Get_Password: string;
    { Methods & Properties }
    property ID: string read Get_ID;
    property Password: string read Get_Password;
  end;

{ IXMLAirborneBusinessFunctionType }

  IXMLAirborneBusinessFunctionType = interface(IXMLNode)
    ['{B4748442-3BDA-415D-B28E-31DAE59F7AD3}']
  end;

{ Forward Decls }

  TXMLECommerceType = class;
  TXMLFaultsType = class;
  TXMLFaultType = class;
  TXMLRequestorType = class;
  TXMLAirborneBusinessFunctionType = class;

{ TXMLECommerceType }

  TXMLECommerceType = class(TXMLNode, IXMLECommerceType)
  protected
    { IXMLECommerceType }
    function Get_Version: string;
    function Get_Transmission_reference: string;
    function Get_Timestamp: string;
    function Get_Action: string;
    function Get_Faults: IXMLFaultsType;
    function Get_Requestor: IXMLRequestorType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultsType }

  TXMLFaultsType = class(TXMLNodeCollection, IXMLFaultsType)
  protected
    { IXMLFaultsType }
    function Get_Fault(Index: Integer): IXMLFaultType;
    function Add: IXMLFaultType;
    function Insert(const Index: Integer): IXMLFaultType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFaultType }

  TXMLFaultType = class(TXMLNode, IXMLFaultType)
  protected
    { IXMLFaultType }
    function Get_Source: string;
    function Get_Code: string;
    function Get_Description: string;
    function Get_Context: string;
  end;

{ TXMLRequestorType }

  TXMLRequestorType = class(TXMLNode, IXMLRequestorType)
  protected
    { IXMLRequestorType }
    function Get_ID: string;
    function Get_Password: string;
  end;

{ TXMLAirborneBusinessFunctionType }

  TXMLAirborneBusinessFunctionType = class(TXMLNode, IXMLAirborneBusinessFunctionType)
  protected
    { IXMLAirborneBusinessFunctionType }
  end;

{ Global Functions }

function GeteCommerce(Doc: IXMLDocument): IXMLECommerceType;
function LoadeCommerce(const FileName: WideString): IXMLECommerceType;
function NeweCommerce: IXMLECommerceType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GeteCommerce(Doc: IXMLDocument): IXMLECommerceType;
begin
  Result := Doc.GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

function LoadeCommerce(const FileName: WideString): IXMLECommerceType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

function NeweCommerce: IXMLECommerceType;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

{ TXMLECommerceType }

procedure TXMLECommerceType.AfterConstruction;
begin
  RegisterChildNode('Faults', TXMLFaultsType);
  RegisterChildNode('Requestor', TXMLRequestorType);
  inherited;
end;

function TXMLECommerceType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

function TXMLECommerceType.Get_Transmission_reference: string;
begin
  Result := AttributeNodes['transmission_reference'].Text;
end;

function TXMLECommerceType.Get_Timestamp: string;
begin
  Result := AttributeNodes['timestamp'].Text;
end;

function TXMLECommerceType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

function TXMLECommerceType.Get_Faults: IXMLFaultsType;
begin
  Result := ChildNodes['Faults'] as IXMLFaultsType;
end;

function TXMLECommerceType.Get_Requestor: IXMLRequestorType;
begin
  Result := ChildNodes['Requestor'] as IXMLRequestorType;
end;

{ TXMLFaultsType }

procedure TXMLFaultsType.AfterConstruction;
begin
  RegisterChildNode('Fault', TXMLFaultType);
  ItemTag := 'Fault';
  ItemInterface := IXMLFaultType;
  inherited;
end;

function TXMLFaultsType.Get_Fault(Index: Integer): IXMLFaultType;
begin
  Result := List[Index] as IXMLFaultType;
end;

function TXMLFaultsType.Add: IXMLFaultType;
begin
  Result := AddItem(-1) as IXMLFaultType;
end;

function TXMLFaultsType.Insert(const Index: Integer): IXMLFaultType;
begin
  Result := AddItem(Index) as IXMLFaultType;
end;

{ TXMLFaultType }

function TXMLFaultType.Get_Source: string;
begin
  Result := ChildNodes['Source'].Text;
end;

function TXMLFaultType.Get_Code: string;
begin
  Result := ChildNodes['Code'].Text;
end;

function TXMLFaultType.Get_Description: string;
begin
  Result := ChildNodes['Description'].Text;
end;

function TXMLFaultType.Get_Context: string;
begin
  Result := ChildNodes['Context'].Text;
end;

{ TXMLRequestorType }

function TXMLRequestorType.Get_ID: string;
begin
  Result := ChildNodes['ID'].Text;
end;

function TXMLRequestorType.Get_Password: string;
begin
  Result := ChildNodes['Password'].Text;
end;

{ TXMLAirborneBusinessFunctionType }

end.