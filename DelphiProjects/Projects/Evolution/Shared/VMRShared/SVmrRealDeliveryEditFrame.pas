// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrRealDeliveryEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SDataStructure,
  wwdblook, StdCtrls, Mask, wwdbedit, DBCtrls, SDDClasses, 
  ISBasicClasses, EvUIComponents, isUIwwDBEdit;

type
  TVmrRealDeliveryEditFrame = class(TVmrOptionEditFrame)
    edEmailAddr: TevDBEdit;
    lEmailAddr: TevLabel;
    evDBCheckBox1: TevDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses SVmrClasses, EvUtils;

{ TVmrRealDeliveryEditFrame }

initialization
  RegisterVmrClass(TVmrRealDeliveryEditFrame);

end.
