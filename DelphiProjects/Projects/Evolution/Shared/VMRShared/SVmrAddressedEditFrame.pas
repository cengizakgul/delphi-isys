// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrAddressedEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SDataStructure,
  wwdblook, StdCtrls, Mask, wwdbedit, DBCtrls, SDDClasses,
  ISBasicClasses, SVmrRealDeliveryEditFrame, Buttons, SDataDictsystem, EvUIComponents, EvContext,
  LMDCustomButton, LMDButton, isUILMDButton, isUIDBMemo,
  isUIwwDBLookupCombo, isUIwwDBEdit;

type
  TVmrAddressedEditFrame = class(TVmrRealDeliveryEditFrame)
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    evLabel6: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evDBEdit5: TevDBEdit;
    evLabel7: TevLabel;
    bGetClAddress: TevBitBtn;
    procedure bGetClAddressClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor CreateBound(const AOwner: TComponent; const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource); override;
  end;

implementation

{$R *.dfm}

uses SVmrClasses, EvUtils;

{ TVmrAddressedEditFrame }

constructor TVmrAddressedEditFrame.CreateBound(const AOwner: TComponent;
  const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource);
begin
  inherited;
  DM_SYSTEM_STATE.SY_STATES.DataRequired('');
end;

procedure TVmrAddressedEditFrame.bGetClAddressClick(Sender: TObject);
begin
  inherited;
  if Owner.Name = 'EDIT_SB_MAIL_BOX' then
  begin
    ctx_DataAccess.OpenClient(MasterDS.DataSet.FieldByName('CL_NBR').Value);
  end;

  DM_CLIENT.CL.Activate;

  Assert(DM_CLIENT.CL.RecordCount=1);

  dsMain.Edit;
  dsMain.DataSet['NAME'] := DM_CLIENT.CL['NAME'];
  dsMain.DataSet['ADDRESS1'] := DM_CLIENT.CL['ADDRESS1'];
  dsMain.DataSet['ADDRESS2'] := DM_CLIENT.CL['ADDRESS2'];
  dsMain.DataSet['CITY'] := DM_CLIENT.CL['CITY'];
  dsMain.DataSet['STATE'] := DM_CLIENT.CL['STATE'];
  dsMain.DataSet['ZIP'] := DM_CLIENT.CL['ZIP_CODE'];
  dsMain.DataSet['EMAIL'] := DM_CLIENT.CL['E_MAIL_ADDRESS1'];
end;

initialization
  RegisterVmrClass(TVmrAddressedEditFrame);

end.
