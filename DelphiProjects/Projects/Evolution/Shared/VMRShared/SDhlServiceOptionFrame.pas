unit SDhlServiceOptionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFedexSbParamFrame, DB, Wwdatsrc, ISBasicClasses, 
  StdCtrls, Mask, wwdbedit, ExtCtrls, DBCtrls, wwdblook, EvDataAccessComponents,
  EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIwwDBEdit;

type
  TDhlServiceOptionFrame = class(TAccountedSbParamFrame)
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    evDBEdit4: TevDBEdit;
    evButton1: TevButton;
    evButton2: TevButton;
    evButton3: TevButton;
    evDBRadioGroup1: TevDBRadioGroup;
    evLabel5: TevLabel;
    edDHLReport: TevDBLookupCombo;
    evDBRadioGroup2: TevDBRadioGroup;
    procedure evButton1Click(Sender: TObject);
    procedure evButton2Click(Sender: TObject);
    procedure evButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetReportsByMediaType(MediaType, ALev:string; AOwner: TComponent): TevClientDataSet;
    constructor CreateBound(const AOwner: TComponent; const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource); override;
  end;

implementation

{$R *.dfm}



uses SServiceDhl, SVmrOptionEditFrame, EvUtils, SDhlShipmentResponse,
  EvConsts;

function TDhlServiceOptionFrame.GetReportsByMediaType(MediaType, ALev:string; AOwner: TComponent): TevClientDataSet;
begin
  Result := TevClientDataSet.Create(AOwner);
  Result.FieldDefs.Add('Level', ftString, 1);
  Result.FieldDefs.Add('Nbr', ftInteger);
  Result.FieldDefs.Add('Description', ftString, 64);

  Result.CreateDataSet;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
     SetMacro('TABLENAME', 'Sy_Report_Writer_Reports');
     SetMacro('COLUMNS', 'Sy_Report_Writer_Reports_Nbr,Report_Description,Media_Type');
     SetMacro('CONDITION', 'Media_Type =:MT');
     SetParam('MT', MediaType);
     ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
     ctx_DataAccess.OpenDataSets([ctx_DataAccess.SY_CUSTOM_VIEW]);
  end;

  ctx_DataAccess.SY_CUSTOM_VIEW.First;
  while not ctx_DataAccess.SY_CUSTOM_VIEW.EOF do
  begin
    Result.Append;
    Result.Fields[0].AsString := ALev;
    Result.Fields[1].AsInteger :=ctx_DataAccess.SY_CUSTOM_VIEW.FieldByName('Sy_Report_Writer_Reports_Nbr').AsInteger;
    Result.Fields[2].AsString := ctx_DataAccess.SY_CUSTOM_VIEW.FieldByName('Report_Description').AsString;
    Result.Post;
    ctx_DataAccess.SY_CUSTOM_VIEW.Next;
  end;

  ctx_DataAccess.SY_CUSTOM_VIEW.Close;
end;

constructor TDhlServiceOptionFrame.CreateBound(const AOwner: TComponent;
  const cd: TDataSet; const OptionedTypeObject: TObject;
  const MasterDataSource: TDataSource);
begin
  inherited;
  //evButton2.Visible := DebugMode;
  //evButton3.Visible := DebugMode;

  edDHLReport.LookupTable := GetReportsByMediaType(MEDIA_TYPE_DHL_LABEL, 'S', Self);
end;

procedure TDhlServiceOptionFrame.evButton1Click(Sender: TObject);
begin
  if dsMain.DataSet.State in [dsEdit, dsInsert] then
    dsMain.DataSet.UpdateRecord;
  TVmrDhlService(FOptionedTypeObject).ApiTestCredentials;
  EvMessage('Credentials are verified');
end;

procedure TDhlServiceOptionFrame.evButton2Click(Sender: TObject);
var
  r: IXMLECommerceShipmentResponse;
begin
  if dsMain.DataSet.State in [dsEdit, dsInsert] then
    dsMain.DataSet.UpdateRecord;
  r := TVmrDhlService(FOptionedTypeObject).ApiSendDummyPackage;
  EvMessage('Package is sent. Tacking # is '+ r.Shipment.ShipmentDetail.AirbillNbr);
end;

procedure TDhlServiceOptionFrame.evButton3Click(Sender: TObject);
begin
  dsMain.DataSet.CheckBrowseMode;
  EvMessage(TVmrDhlService(FOptionedTypeObject).ApiGenerateCertificationXML);
end;

end.
