inherited VmrEmail2EditFrame: TVmrEmail2EditFrame
  object evLabel1: TevLabel [0]
    Left = 8
    Top = 16
    Width = 69
    Height = 13
    Caption = 'Email Address:'
  end
  object evDBEdit1: TevDBEdit [1]
    Left = 8
    Top = 40
    Width = 297
    Height = 21
    DataField = 'EMAIL_ADDRESS'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
  end
  object evDBCheckBox1: TevDBCheckBox [2]
    Left = 11
    Top = 71
    Width = 373
    Height = 17
    Caption = 'Use Output ASCII File Name for ASCII results'
    DataField = 'SAVE_ASCII'
    DataSource = dsMain
    TabOrder = 1
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
end
