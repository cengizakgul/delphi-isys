inherited VmrFtpEditFrame: TVmrFtpEditFrame
  Height = 344
  object evLabel1: TevLabel [0]
    Left = 8
    Top = 8
    Width = 55
    Height = 13
    Caption = 'FTP server:'
  end
  object evLabel2: TevLabel [1]
    Left = 8
    Top = 64
    Width = 54
    Height = 13
    Caption = 'User name:'
  end
  object evLabel3: TevLabel [2]
    Left = 8
    Top = 112
    Width = 49
    Height = 13
    Caption = 'Password:'
  end
  object evLabel4: TevLabel [3]
    Left = 8
    Top = 200
    Width = 74
    Height = 16
    Caption = '~SMTP server:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object evLabel5: TevLabel [4]
    Left = 8
    Top = 248
    Width = 83
    Height = 16
    Caption = '~FROM address:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object evDBEdit1: TevDBEdit [5]
    Left = 8
    Top = 32
    Width = 297
    Height = 21
    DataField = 'FTP_SERVER'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit2: TevDBEdit [6]
    Left = 8
    Top = 80
    Width = 297
    Height = 21
    DataField = 'USER_NAME'
    DataSource = dsMain
    TabOrder = 1
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit3: TevDBEdit [7]
    Left = 8
    Top = 128
    Width = 297
    Height = 21
    DataField = 'PASSWORD'
    DataSource = dsMain
    PasswordChar = '*'
    TabOrder = 2
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object SMTPServerDBEdit: TevDBEdit [8]
    Left = 8
    Top = 216
    Width = 297
    Height = 21
    DataField = 'SMTP_SERVER'
    DataSource = dsMain
    TabOrder = 3
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object FromAddressDBEdit: TevDBEdit [9]
    Left = 8
    Top = 264
    Width = 297
    Height = 21
    DataField = 'FROM_ADDRESS'
    DataSource = dsMain
    TabOrder = 4
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBCheckBox2: TevDBCheckBox [10]
    Left = 11
    Top = 167
    Width = 206
    Height = 17
    Caption = 'Use SFTP protocol'
    DataField = 'USE_SFTP'
    DataSource = dsMain
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
end
