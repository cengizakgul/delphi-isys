inherited VmrGroupedPaperMediaEditFrame: TVmrGroupedPaperMediaEditFrame
  Width = 514
  Height = 318
  object lPrintPassword: TevLabel [0]
    Left = 8
    Top = 91
    Width = 79
    Height = 13
    Caption = 'ASCII Password:'
  end
  object evDBRadioGroup1: TevDBRadioGroup [1]
    Left = 8
    Top = 8
    Width = 137
    Height = 65
    Caption = 'Stuff Checks'
    Columns = 2
    DataField = 'STUFF_CHECKS'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 0
    Values.Strings = (
      'Y'
      'N')
  end
  object evDBRadioGroup2: TevDBRadioGroup [2]
    Left = 168
    Top = 8
    Width = 137
    Height = 65
    Caption = 'Stuff W2s'
    Columns = 2
    DataField = 'STUFF_W2'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 1
    Values.Strings = (
      'Y'
      'N')
  end
  object evDBRadioGroup3: TevDBRadioGroup [3]
    Left = 328
    Top = 8
    Width = 137
    Height = 65
    Caption = 'Stuff 1099s'
    Columns = 2
    DataField = 'STUFF_1099'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 2
    Values.Strings = (
      'Y'
      'N')
  end
  object edPrintPassword: TevDBEdit [4]
    Left = 112
    Top = 91
    Width = 145
    Height = 21
    DataField = 'PRINT_PASSWORD'
    DataSource = dsMain
    TabOrder = 3
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
end
