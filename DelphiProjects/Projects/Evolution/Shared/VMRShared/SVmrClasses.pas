// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrClasses;

interface

uses
  Classes,  DB, SVmrOptionEditFrame, EvTypes, SReportSettings, SDDClasses,
  SDataDictClient, SDataDictBureau, Controls, Dialogs, EvContext, isBasicUtils,isBaseClasses,
  EvExceptions,EvStreamUtils, EvCommonInterfaces, EvDataAccessComponents, EvUIUtils, EvClientDataSet;

type
  TVmrPrintJob = record
    SbMailBoxNbr: Integer;
    SbUpLevelMailBoxNbr: Variant;
    ClNbr, CoNbr, PrNbr: Variant;
    BarCode: string;
    PartNumber, PartCount: Integer;
    SbContentNbrs: Variant;
    Description: string;
    PrinterIndex: Integer;
    Res: TrwReportResults;
    cd: TevClientDataSet;
  end;
  TVmrSelfServeType = record
      SelfServeEEs:IisStringList;
      SelfServeText:string;
      FromAddress:string;

  end;
  TVmrPrintJobs = array of TVmrPrintJob;
  TVmtKeyFieldType = array of string;
  TVmtKeyValueType = array of Variant;
  TClassArray = array of TClass;
  TVmrDeliveryMethodClass = class of TVmrDeliveryMethod;
  TVmrDeliveryMethodClassArray = array of TVmrDeliveryMethodClass;
  TVmrMediaTypeClass = class of TVmrMediaType;
  TVmrMediaTypeClassArray = array of TVmrMediaTypeClass;

  TVmrDeliveryService = class;
  TVmrMediaType = class;
  TVmrPackagedDeliveryMethod = class;

  TVmrOptionDataSet = class(TddTable)
  private
    FOptionDataSet: TevClientDataSet;
    FOptionKeyFields: TVmtKeyFieldType;
    FOptionKeyValues: TVmtKeyValueType;
    FOptionPrefix: string;
  protected
    procedure DoAfterInsert; override;
    procedure DoApply(const FOptionDataSet: TevClientDataSet; const
            FOptionPrefix: string; const FOptionKeyFields: TVmtKeyFieldType;
            const FOptionKeyValues: TVmtKeyValueType; const AlwaysAdd: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Apply; overload;
    procedure Apply(const cd: TevClientDataSet; const KeyFields:
            TVmtKeyFieldType; const KeyValues: TVmtKeyValueType; const Prefix:
            string); overload;
    procedure Load(const KeyFields: TVmtKeyFieldType; const KeyValues:
            TVmtKeyValueType; const Table: TddTableClass; const Prefix: string;
            const LoadRecordCount: Integer);
    property OptionDataSet: TevClientDataSet read FOptionDataSet;
  end;

  TVmrOptionedType = class(TObject)
  private
    FBoxNbr: Integer;
    FExtraOptions: TVmrOptionDataSet;
    FKeyValue: Integer;
    FOptions: TVmrOptionDataSet;
  protected
    constructor Create(const SbNbr: Integer; const ExtraTable: TddTableClass;
            const ExtraPrefix: string; const ExtraNbr: Integer);
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); virtual;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); virtual;
    procedure PrepareOptions(const Nbr: Integer; out Table: TddTableClass; out
            KeyFields: TVmtKeyFieldType; out KeyValues: TVmtKeyValueType);
            virtual; abstract;
    procedure AfterCreated; virtual;
  public
    constructor CreateClMailBoxSetup(const SbNbr: Integer; const Prefix: string;
            const Nbr: Integer);
    constructor CreateSbMailBoxSetup(const SbNbr: Integer; const Prefix: string;
            const Nbr: Integer; const Dummy: Byte = 0);
    constructor CreateSbSetup(const SbNbr: Integer);
    destructor Destroy; override;
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; virtual;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; virtual;
    property BoxNbr: Integer read FBoxNbr write FBoxNbr;
    property ExtraOptions: TVmrOptionDataSet read FExtraOptions;
    property KeyValue: Integer read FKeyValue write FKeyValue;
    property Options: TVmrOptionDataSet read FOptions;
  end;

  TVmrOptionedTypeClass = class of TVmrOptionedType;

  TVmrDeliveryMethod = class(TVmrOptionedType)
  private
    FDescription: string;
    FLocation: string;
    FSentEmailContentNbrs: string;
    FReleaseType:TVmrReleaseTypeSet;
    FDirectEmailParamArray: array of TrwReportResults;
    ConfNotice:TStringList;
    FSelfServeArray:array of TVmrSelfServeType;
    FEmailErrorList: TStringList;
    FSentEmailCounter: Integer;
    function  OverrideFromAddr(FromAddr:string):string;
    procedure CollectSelfServeEEs(const cd: TEvClientDataSet;const RL: TrwReportResults);
    procedure DeleteFromSelfServeEEs(EeNbr:integer);
    function  GetSelfServeText(CL_NBR,CL_MAIL_BOX_GROUP_NBR:integer):string;
  protected
    FDirectEmailCounter: Integer;
    FPackagedEmailCounter: Integer;
    function IsIncludeDescInEmail(SBBoxNbr:Integer):boolean;
    function GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams; virtual;
    procedure PrepareOptions(const Nbr: Integer; out Table: TddTableClass; out
            KeyFields: TVmtKeyFieldType; out KeyValues: TVmtKeyValueType);
            override;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;

  public
    destructor Destroy; override;
    procedure  AfterCreated;  override;
    procedure RevertToUnreleased(const Media: TVmrMediaType; const cd:
            TEvClientDataSet); virtual;
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); virtual; abstract;
    procedure AddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet);
    class function GetMediaClasses: TVmrMediaTypeClassArray; virtual; abstract;
    function  GetPickupSheetReport(const Job: TVmrPrintJob): TrwReportResult;
    procedure PopulateNewMailBox; virtual; abstract;
    procedure PrepareToRelease; virtual;
    procedure Release(const cd: TEvClientDataSet); virtual;
    procedure FinishRelease(const cd: TEvClientDataSet); virtual;
    property Description: string read FDescription write FDescription;
    property Location: string read FLocation write FLocation;
    property ReleaseType:TVmrReleaseTypeSet read FReleaseType write FReleaseType;
    function CreateSbDeliveryService(const cd: TEvClientDataSet): TVmrDeliveryService;

    class procedure SetupEmailerByFromAddress(const AAddressFrom: String);

    procedure AssignVmrFileNames(const R: TrwReportResults; IsIncludeDesc : boolean);
    procedure EmailReportResults(const R: TrwReportResults; const FromAddr, ToAddr : string; const IsTaxReturns: boolean);
    procedure StoreReportResults(const R: TrwReportResults; StoringRequired:boolean);

    function IsPDFFormat(aData:IevDualStream):boolean;
    function IsZipFormat(aData:IevDualStream):boolean;
    function IsXMLFormat(aData:IevDualStream):boolean;

    procedure EmailDirectly(const RL: TrwReportResults; const FromAddr{, SmtpHost}: string; MailBoxNbr :integer; const NotificationEmail: string);
    procedure SendNotificationEmail(FromAddr, ToAddr, Webnotes : string; CheckDate,TaxEndingDate :TDateTime);

    procedure MarkSentEmailContent(const sCntNbrs: string); overload;
    procedure MarkSentEmailContent(const R: TrwReportResults); overload;
    procedure PostSentEmailContent(const cd: TEvClientDataSet; const MarkSuccess: boolean);
  end;

  TVmrDeliveryService = class(TVmrOptionedType)
  protected
    procedure PrepareOptions(const Nbr: Integer; out Table: TddTableClass; out
            KeyFields: TVmtKeyFieldType; out KeyValues: TVmtKeyValueType);
            override;
  public
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; virtual;
            abstract;
  end;

  TVmrRealDelivery = class(TVmrDeliveryMethod)
  protected
    RL: TrwReportResults;
    RLA: TVmrPrintJobs;
    RLA_ASCII: array of TrwReportResults;
    PrInfo: IevVMRPrintersList;
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
    procedure AddCdMedia(const Media: TVmrMediaType; const cd: TEvClientDataSet); virtual;
    procedure AddGroupedPaper(const Media: TVmrMediaType; const cd: TEvClientDataSet); virtual;
    procedure AddGroupedAscii(const RLAscii: TrwReportResults; const Media: TVmrMediaType; const cd: TEvClientDataSet); virtual;
    function  CalcWeight: double;
    procedure PrintVmrReports; virtual;
    procedure StoreCoverSheetsAndLabelsForRemotePrint; virtual;
    procedure DeleteCoverSheetsAndLabelsForRemotePrint(MailBoxNbr: integer); virtual;
  public
    destructor Destroy; override;
    procedure RevertToUnreleased(const Media: TVmrMediaType; const cd:
            TEvClientDataSet); override;
    procedure InternalAddToRelease(const Media: TVmrMediaType; const cd:
            TEvClientDataSet; const RL: TrwReportResults); override;
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    class function GetShippedMessage(cd: TEvClientDataSet): string; virtual;
    class function OnPickup(cd: TEvClientDataSet): Boolean; virtual;
    function OnPickupSheetScan(cd: TEvClientDataset): string; virtual; abstract;
    class function OnShipmentPacked(cd: TEvClientDataset): string; virtual;
    procedure PrepareToRelease; override;
    procedure Release(const cd: TEvClientDataSet); override;

  end;

  TVmrPackagedDeliveryMethod = class(TVmrRealDelivery)
  public
    class function GetMediaClasses: TVmrMediaTypeClassArray; override;
  end;

  TVmrMediaType = class(TVmrOptionedType)
  protected
    function GetCoverLetter(const cd: TevClientDataSet; const Params:
            TReportParamArrayType): TrwReportResults; virtual;
    procedure PrepareOptions(const Nbr: Integer; out Table: TddTableClass; out
            KeyFields: TVmtKeyFieldType; out KeyValues: TVmtKeyValueType);
            override;
  public
    function CreateResultCollection(const cd: TevClientDataSet):
            TrwReportResults;
    function OnPickupSheetScan(cd: TEvClientDataSet): string; virtual;
  end;

  TVmrPaperMedia = class(TVmrMediaType)
  protected
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public
    function GetPickupSheetMediaNotes: string; virtual; abstract;
  end;

  TVmrSeparatedPaperMediaType = class(TVmrPaperMedia)
  protected
    function GetCoverLetter(const cd: TevClientDataSet; const Params:
            TReportParamArrayType): TrwReportResults; override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function GetPickupSheetMediaNotes: string; override;
  end;

  TVmrGroupedPaperMediaType = class(TVmrPaperMedia)
  protected
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    function GetPickupSheetMediaNotes: string; override;
  end;


  TVmrAddressedDeliveryMethod = class(TVmrPackagedDeliveryMethod)
  protected
    function GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams; override;
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public
    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    procedure PopulateNewMailBox; override;
  end;

  TVmrAccountedService = class(TVmrDeliveryService)
  protected
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;
  public
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
  end;

  TVmrCourier2Service = class(TVmrAccountedService)
  public
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
  end;

  TVmrCourier2Service1 = class(TVmrCourier2Service)
  end;

  TVmrCourier2Service2 = class(TVmrCourier2Service)
  end;

  TVmrCourier2Service3 = class(TVmrCourier2Service)
  end;
  
  TVmrCourier2DeliveryMethod = class(TVmrAddressedDeliveryMethod)
  public
    class function GetShippedMessage(cd: TEvClientDataSet): string; override;
    function OnPickupSheetScan(cd: TEvClientDataset): string; override;
  end;
  
  TVmrFedexService = class(TVmrAccountedService)
  public
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
  end;

  TVmrFedexDeliveryMethod = class(TVmrAddressedDeliveryMethod)
  public
    class function GetShippedMessage(cd: TEvClientDataSet): string; override;
  end;



  TVmrFedexPrepaid = class(TVmrFedexDeliveryMethod)
  public
    function OnPickupSheetScan(cd: TEvClientDataset): string; override;
    class function OnShipmentPacked(cd: TEvClientDataset): string; override;
  end;

  TVmrCourierDeliveryMethod = class(TVmrAddressedDeliveryMethod)
  end;

  TVmrDeliveryServiceClass = class of TVmrDeliveryService;

function FindVmrClass(const sClassName: string): TClass;
function ListVmrInheritedClasses(const rBaseClass: TClass): string;
function ListVmrClasses(const a: TClassArray): string;
function CheckIfListed(const a: TClassArray; const c: TClass): Boolean;
procedure RegisterVmrClass(const c: TClass);

function InstanceClPrimaryDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
function InstanceClSecondaryDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
function InstanceClPrimaryMedia(const ds1: TevClientDataSet): TVmrMediaType;
function InstanceClSecondaryMedia(const ds1: TevClientDataSet): TVmrMediaType;
function InstanceSbDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
function InstanceSbMedia(const ds1: TevClientDataSet): TVmrMediaType;

function LayersToStr(const l: TrwPrintableLayers): string;
function StrToLayers(s: string): TrwPrintableLayers;
function FormatJobInfo(const CoNbr, PrNbr: Integer; const EventDate: TDateTime; const Descr, Descr2: string): string;
function FormatBarCode(const iSbNbr, iPartNumber, iPartCount: Integer): string;
procedure CreateResultFiles(Path: string; const R: TrwReportResults;Ext:string='rwa');
procedure CreateTempFiles(out Path: string; const R: TrwReportResults;Ext:string='rwa');
procedure DeleteTempFiles(const Path: string; const R: TrwReportResults;Ext:string='rwa');
procedure EncryptReportResults(const RL: TrwReportResults; const PrintPassword: string);
function  GetReportsByAncestor(const AAncestor : string; const ALevels: string; AOwner: TComponent): TevClientDataSet;


implementation

uses EvUtils, SysUtils, Variants, SServiceUps, SServicePickup, SDataStructure,
  SVmrGroupedPaperMediaEditFrame, SVmrAddressedEditFrame, EvBasicUtils, SFedexSbParamFrame,
  VarUtils, DateUtils, EvConsts, SFieldCodeValues, SServiceE,
  SVmrRealDeliveryEditFrame, SVmrEmailEditFrame, Windows, SServiceDhl,SVmrEMediaClParamFrame,
  EvDataset, SVMRUtils;

var
  FRegisteredClasses: array of TClass;

function GetReportsByAncestor(const AAncestor : string; const ALevels: string; AOwner: TComponent): TevClientDataSet;

  procedure AddLevel(const ALev: String; const ALevPrif: String);
  var
    DS: TevClientDataSet;
    s : String;
  begin
    DS := TevClientDataSet.Create(nil);
    try
      s := 'select t.' +
           ALevPrif +'_report_writer_reports_nbr, t.report_description, t.class_name, t.ancestor_class_name from ' +
           ALevPrif +'_report_writer_reports t where {AsOfNow<t>} and t.ancestor_class_name = ''' +
           AAncestor+''' order by t.ancestor_class_name';

      CustomQuery(DS, S, ALev, nil);

      DS.First;
      while not DS.Eof do
      begin
        Result.Append;
        Result.Fields[0].AsString := ALev;
        Result.Fields[1].AsInteger := DS.Fields[0].AsInteger;
        Result.Fields[2].AsString := DS.Fields[1].AsString;
        Result.Fields[3].AsString := DS.Fields[2].AsString;
        Result.Fields[4].AsString := DS.Fields[3].AsString;
        Result.Post;
        DS.Next;
      end;
    finally
      DS.Free;
    end;
  end;

begin
  Result := TevClientDataSet.Create(AOwner);
  try
    Result.FieldDefs.Add('Level', ftString, 1);
    Result.FieldDefs.Add('Nbr', ftInteger);
    Result.FieldDefs.Add('Description', ftString, 64);
    Result.FieldDefs.Add('Class_Name', ftString, 40);
    Result.FieldDefs.Add('Ancestor_Class_Name', ftString, 40);
    Result.CreateDataSet;

    if Pos(CH_DATABASE_SYSTEM, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SYSTEM, 'SY');

    if Pos(CH_DATABASE_SERVICE_BUREAU, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SERVICE_BUREAU, 'SB');

    if Pos(CH_DATABASE_CLIENT, ALevels) <> 0 then
      AddLevel(CH_DATABASE_CLIENT, 'CL');

    Result.First;

  except
    Result.Free;
    raise;
  end;
end;


function FindVmrClass(const sClassName: string): TClass;
var
  i: Integer;
begin
  Result := nil;
  for i := Low(FRegisteredClasses) to High(FRegisteredClasses) do
    if FRegisteredClasses[i].ClassNameIs(sClassName) then
    begin
      Result := FRegisteredClasses[i];
      Break;
    end;
end;

function CheckIfListed(const a: TClassArray; const c: TClass): Boolean;
var
  i: Integer;
begin
  Result := Assigned(c);
  if Result then
  begin
    Result := False;
    for i := Low(a) to High(a) do
      if a[i] = c then
      begin
        Result := True;
        Break;
      end;
  end;
end;

function ListVmrClasses(const a: TClassArray): string;
var
  i: Integer;
  l: TStringList;
begin
  l := TStringList.Create;
  try
    for i := Low(a) to High(a) do
      l.Append(a[i].ClassName);
    l.Sort;
    Result := l.CommaText;
  finally
    l.Free;
  end;
end;

function ListVmrInheritedClasses(const rBaseClass: TClass): string;
var
  i: Integer;
  a: TClassArray;
begin
  SetLength(a, 0);
  for i := Low(FRegisteredClasses) to High(FRegisteredClasses) do
    if (FRegisteredClasses[i] = rBaseClass) or FRegisteredClasses[i].InheritsFrom(rBaseClass) then
    begin
      SetLength(a, Length(a)+ 1);
      a[High(a)] := FRegisteredClasses[i];
    end;
  Result := ListVmrClasses(a);
end;

procedure RegisterVmrClass(const c: TClass);
var
  i: Integer;
begin
  i := Length(FRegisteredClasses);
  SetLength(FRegisteredClasses, i+ 1);
  FRegisteredClasses[i] := c;
end;

{ TVmrOptionDataSet }

{ TVmrOptionedType }

{
****************************** TVmrOptionDataSet *******************************
}
constructor TVmrOptionDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FOptionDataSet := nil;
end;

procedure TVmrOptionDataSet.Apply;
begin
  DoApply(FOptionDataSet, FOptionPrefix, FOptionKeyFields, FOptionKeyValues, False);
end;

procedure TVmrOptionDataSet.Apply(const cd: TevClientDataSet; const KeyFields:
        TVmtKeyFieldType; const KeyValues: TVmtKeyValueType; const Prefix:
        string);
begin
  DoApply(cd, Prefix, KeyFields, KeyValues, True);
end;

procedure TVmrOptionDataSet.DoAfterInsert;
begin
  inherited;
  FieldValues['RECORD_NUMBER'] := RecordCount;
end;

procedure TVmrOptionDataSet.DoApply(const FOptionDataSet: TevClientDataSet;
        const FOptionPrefix: string; const FOptionKeyFields: TVmtKeyFieldType;
        const FOptionKeyValues: TVmtKeyValueType; const AlwaysAdd: Boolean);
var
  i, j, k: Integer;
  sTag: string;
  b: Boolean;
//  DatasetWasPosted: boolean;
begin
//  DatasetWasPosted := False;
  if FieldDefs.Count > 0 then
  begin
    Assert(Active);
    Assert(FOptionDataSet.Active);
    b := ctx_DataAccess.TempCommitDisable;
    ctx_DataAccess.TempCommitDisable := True;
    CheckBrowseMode;
    try
      DisableControls;
      FOptionDataSet.DisableControls;
      try
        IndexFieldNames := 'RECORD_NUMBER';
        k := 1;
        {s := '';
        SetLength(vKey, Length(FOptionKeyFields)+1);
        for i := Low(FOptionKeyFields) to High(FOptionKeyFields) do
        begin
          if s = '' then
            s := FOptionKeyFields[i]
          else
            s := s+ ';'+ FOptionKeyFields[i];
          case VarType(FOptionKeyValues[i]) of
          varNull: vKey[i].VType := varNull;
          varInteger:
            begin
              vKey[i].VType := varInteger;
              vKey[i].VInteger := VarToInt(FOptionKeyValues[i]);
            end;
          else
            Assert(False);
          end;
        end;
        vKey[High(vKey)].VType := vars;
        FOptionDataSet.IndexFieldNames := s+ ';OPTION_TAG';}
        FOptionDataSet.IndexFieldNames := 'OPTION_TAG';
        First;
        while not Eof do
        begin
          for i := 0 to Fields.Count-1 do
          if Fields[i].FieldName <> 'RECORD_NUMBER' then
            begin
              sTag := FOptionPrefix+IntToStr(k)+'#'+Fields[i].FieldName;
              if AlwaysAdd then
              begin
                FOptionDataSet.Append;
                FOptionDataSet['OPTION_TAG'] := sTag;
                for j := Low(FOptionKeyFields) to High(FOptionKeyFields) do
                  FOptionDataSet[FOptionKeyFields[j]] := FOptionKeyValues[j];
                if Fields[i] is TIntegerField then
                  FOptionDataSet['OPTION_INTEGER_VALUE'] := Fields[i].AsInteger
                else
                if Fields[i] is TStringField then
                  FOptionDataSet['OPTION_STRING_VALUE'] := Fields[i].AsString;
                FOptionDataSet.Post;
//                DatasetWasPosted := True;
              end
              else
              if Fields[i].IsNull then
              begin
                if FOptionDataSet.FindKey([sTag]) then
                  FOptionDataSet.Delete;
              end
              else
              if not FOptionDataSet.FindKey([sTag]) then
              begin
                FOptionDataSet.Append;
                FOptionDataSet['OPTION_TAG'] := sTag;
                for j := Low(FOptionKeyFields) to High(FOptionKeyFields) do
                  FOptionDataSet[FOptionKeyFields[j]] := FOptionKeyValues[j];
                if Fields[i] is TIntegerField then
                  FOptionDataSet['OPTION_INTEGER_VALUE'] := Fields[i].AsInteger
                else
                if Fields[i] is TStringField then
                  FOptionDataSet['OPTION_STRING_VALUE'] := Fields[i].AsString;
                FOptionDataSet.Post;
//                DatasetWasPosted := True;
              end
              else
              begin
                FOptionDataSet.Edit;
                if Fields[i] is TIntegerField then
                begin
                  if Fields[i].Value <> FOptionDataSet['OPTION_INTEGER_VALUE'] then
                    FOptionDataSet['OPTION_INTEGER_VALUE'] := Fields[i].Value;
                end
                else
                if Fields[i] is TStringField then
                begin
                  if Fields[i].Value <> FOptionDataSet['OPTION_STRING_VALUE'] then
                    FOptionDataSet['OPTION_STRING_VALUE'] := Fields[i].Value;
                end;
                if FOptionDataSet.Modified then begin
                  FOptionDataSet.Post;
//                  DatasetWasPosted := True;
                end else begin
                  FOptionDataSet.Cancel;
//                  DatasetWasPosted := True;
                end;
              end;
            end;
          Inc(k);
          Next;
        end;
//        Assert(DatasetWasPosted, 'At least one parameter needs to be assigned a value');
      finally
        FOptionDataSet.EnableControls;
        EnableControls;
      end;
    finally
      ctx_DataAccess.TempCommitDisable := b;
    end;
    MergeChangeLog;
  end;
end;

procedure TVmrOptionDataSet.Load(const KeyFields: TVmtKeyFieldType; const
        KeyValues: TVmtKeyValueType; const Table: TddTableClass; const Prefix:
        string; const LoadRecordCount: Integer);
var
  i, j: Integer;
  s: string;
begin
  Assert(Length(KeyFields)=Length(KeyValues));
  Assert(not Active);
  Assert(FieldDefs.Count > 0);
  SetLength(FOptionKeyFields, Length(KeyFields));
  Assert(Low(KeyFields)=Low(FOptionKeyFields));
  for i := Low(KeyFields) to High(KeyFields) do
    FOptionKeyFields[i] := KeyFields[i];
  SetLength(FOptionKeyValues, Length(KeyValues));
  for i := Low(KeyValues) to High(KeyValues) do
    FOptionKeyValues[i] := KeyValues[i];
  FOptionPrefix := Prefix;
  FOptionDataSet := Table.Create(Self);
  s := '';
  for i := Low(KeyFields) to High(KeyFields) do
  begin
    if s = '' then
      s := KeyFields[i]
    else
      s := s+ ' and '+ KeyFields[i];
    if VarIsNull(KeyValues[i]) then
      s := s+ ' is null'
    else
      s := s+ '='+ IntToStr(KeyValues[i]);
  end;
  FOptionDataSet.DataRequired(s);
  {s := '';
  for i := Low(KeyFields) to High(KeyFields) do
  begin
    if s = '' then
      s := KeyFields[i]
    else
      s := s+ ';'+ KeyFields[i];
  end;
  FOptionDataSet.IndexFieldNames := s+ ';OPTION_TAG';}
  FOptionDataSet.IndexFieldNames := 'OPTION_TAG';
  FieldDefs.Add('RECORD_NUMBER', ftInteger);
  CreateDataSet;
  IndexFieldNames := 'RECORD_NUMBER';
  for j := 1 to LoadRecordCount do
  begin
    Append;
    FieldValues['RECORD_NUMBER'] := j;
    for i := 0 to Fields.Count-1 do
      if FOptionDataSet.FindKey([FOptionPrefix+IntToStr(j)+'#'+Fields[i].FieldName]) then
      begin
        if Fields[i] is TIntegerField then
          Fields[i].Value := FOptionDataSet['OPTION_INTEGER_VALUE']
        else
        if Fields[i] is TStringField then
          Fields[i].Value := FOptionDataSet['OPTION_STRING_VALUE']
        else
          Assert(False);
      end
      else begin // default value initialization
        if ( Fields[i].FieldName = 'CONVERT_TO_PDF' ) then
          Fields[i].Value := GROUP_BOX_NO
        else if ( Fields[i].FieldName = 'SAVE_ASCII' ) then
          Fields[i].Value := 0
        else if ( Fields[i].FieldName = 'CREATE_FOLDERS' ) then
          Fields[i].Value := 1
        else if ( Fields[i].FieldName = 'USE_SFTP' ) then
          Fields[i].Value := 0
        else if ( Fields[i].FieldName = 'SEND_PURE_XML' ) then
          Fields[i].Value := 0
      end;
    Post;
  end;
  MergeChangeLog;
end;

{
******************************* TVmrOptionedType *******************************
}
procedure TVmrOptionedType.AfterCreated;
begin
end;

constructor TVmrOptionedType.Create(const SbNbr: Integer; const ExtraTable:
        TddTableClass; const ExtraPrefix: string; const ExtraNbr: Integer);
var
  cTable: TddTableClass;
  sPrefix: string;                     
  iRecordCount: Integer;
  aKeyFields: TVmtKeyFieldType;
  aKeyValues: TVmtKeyValueType;
begin
  FOptions := TVmrOptionDataSet.Create(nil);
  FExtraOptions := TVmrOptionDataSet.Create(nil);
  SetLength(aKeyFields, 0);
  SetLength(aKeyValues, 0);
  iRecordCount := 1;
  PrepareOptionFields(iRecordCount, FOptions.FieldDefs);
  if FOptions.FieldDefs.Count > 0 then
  begin
    PrepareOptions(SbNbr, cTable, aKeyFields, aKeyValues);
    FOptions.Load(aKeyFields, aKeyValues, cTable, '', iRecordCount);
  end;
  if ExtraTable <> nil then
  begin
    sPrefix := '';
    iRecordCount := 1;
    PrepareExtraOptionFields(sPrefix, iRecordCount, FExtraOptions.FieldDefs);
    if FExtraOptions.FieldDefs.Count > 0 then
    begin
      SetLength(aKeyFields, 1);
      aKeyFields[0] := Copy(ExtraTable.GetTableName, 1, Length(ExtraTable.GetTableName)-
        Length('_OPTION'))+ '_NBR';
      SetLength(aKeyValues, 1);
      aKeyValues[0] := ExtraNbr;
      FExtraOptions.Load(aKeyFields, aKeyValues, ExtraTable, ExtraPrefix+ sPrefix, iRecordCount);
    end;
  end;
  AfterCreated;
end;

constructor TVmrOptionedType.CreateClMailBoxSetup(const SbNbr: Integer; const
        Prefix: string; const Nbr: Integer);
begin
  Create(SbNbr, TCL_MAIL_BOX_GROUP_OPTION, Prefix, Nbr);
end;

constructor TVmrOptionedType.CreateSbMailBoxSetup(const SbNbr: Integer; const
        Prefix: string; const Nbr: Integer; const Dummy: Byte = 0);
begin
  Create(SbNbr, TSB_MAIL_BOX_OPTION, Prefix, Nbr);
end;

constructor TVmrOptionedType.CreateSbSetup(const SbNbr: Integer);
begin
  Create(SbNbr, nil, '', 0);
end;

destructor TVmrOptionedType.Destroy;
begin
  inherited;
  FreeAndNil(FOptions);
  FreeAndNil(FExtraOptions);
end;

function TVmrOptionedType.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := nil;
end;

function TVmrOptionedType.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := nil;
end;

procedure TVmrOptionedType.PrepareExtraOptionFields(var Prefix: string; var
        LoadRecordCount: Integer; const f: TFieldDefs);
begin
  f.Count; // to avoid designer critic
end;

procedure TVmrOptionedType.PrepareOptionFields(var LoadRecordCount: Integer;
        const f: TFieldDefs);
begin
  f.Count; // to avoid designer critic
end;

{ TVmrMediaType }

{
****************************** TVmrDeliveryMethod ******************************
}

function TVmrDeliveryMethod.OverrideFromAddr(FromAddr:string):string;
  var
    qsbmailbox : IevQuery;
  begin
    qsbmailbox := TevQuery.Create('SELECT CL_NBR, CL_MAIL_BOX_GROUP_NBR FROM SB_MAIL_BOX a WHERE a.SB_MAIL_BOX_NBR = :SBNBR AND {AsOfNow<a>} ');
    qsbmailbox.Params.AddValue('SBNBR', BoxNbr);
    qsbmailbox.Execute;
    if  (DM_CLIENT.CL_MAIL_BOX_GROUP.ClientID <> qsbmailbox.Result.Fields[0].asInteger)
     or (not DM_CLIENT.CL_MAIL_BOX_GROUP.Active) then
    begin
     DM_CLIENT.CL_MAIL_BOX_GROUP.Close;
     DM_CLIENT.CL_MAIL_BOX_GROUP.CheckDSConditionAndClient(qsbmailbox.Result.Fields[0].asInteger,'');
    end;
    ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_MAIL_BOX_GROUP,DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION]);
    Result :=Trim(
                   ConvertNull(
                        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Lookup(
                              'CL_MAIL_BOX_GROUP_NBR;OPTION_TAG',
                              VarArrayOf(
                                [convertNull(qsbmailbox.Result['CL_MAIL_BOX_GROUP_NBR'],0),
                                 VmrReplyEmailAddress
                                ]
                              ),
                              'OPTION_STRING_VALUE'
                        ),
                        ''
                   )
                 );
    if Trim(Result)='' then
      Result := FromAddr;
end;

class procedure TVmrDeliveryMethod.SetupEmailerByFromAddress(const AAddressFrom: String);
var
  from,from2:string;
  i: integer;
  l,es: TStringList;
begin
  from := UpperCase(AAddressFrom);
  from2 := Copy(from,Pos('@',from),Length(From));
  i := 1;
  l := TStringList.Create;
  es := TStringList.Create;
  DM_SERVICE_BUREAU.SB_OPTION.Active := true;
  try
    while DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSmtpSetupTag+'_'+IntToStr(i), []) do
    begin
      l.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString;
      while l.Count < 5 do l.Add('');
      l[1] := UpperCase(l[1]);
      if (l[1] = from) or (l[1] = from2) then
        es.Add(l.CommaText);
      inc(i);
    end;
    for i:= 0 to es.Count-1 do
    begin
       l.CommaText := es[i];
       if l[1] = From then
         break;
    end;
    if i = es.Count then
    begin
      for i:= 0 to es.Count-1 do
      begin
         l.CommaText := es[i];
         if l[1] = From2 then
           break;
      end;
    end;
    if i< es.Count then
    begin
       ctx_EMailer.SmtpServer := l[0];
       if l[4] <> '' then
         ctx_EMailer.SmtpPort := StrToInt(l[4]);
       ctx_EMailer.UserName := l[2];
       ctx_EMailer.Password := l[3];

    end;
  finally
    l.Free;
    es.Free;
  end;

end;

function TVmrDeliveryMethod.IsZipFormat(aData: IevDualStream): boolean;
var
  s: String;
  p: Integer;

const
  cZipDNA = 'PK';
begin
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cZipDNA));
    AData.Read(s[1], Length(cZipDNA));
    Result :=  AnsiSameStr(s, cZipDNA);
    if Result then
    begin
       AData.Read(s[1], 2);
       if not(((s[1]=#1) and (s[2]=#2))
          or ((s[1]=#3) and (s[2]=#4))
          or ((s[1]=#5) and (s[2]=#6))
          or ((s[1]=#7) and (s[2]=#8))) then
         Result := false;

    end;
  finally
    AData.Position := p;
  end;

end;

function TVmrDeliveryMethod.IsPDFFormat(aData: IevDualStream): boolean;
var
  s: String;
  p: Integer;
const
  cPDFDNA = '%PDF-';
begin
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cPDFDNA));
    AData.Read(s[1], Length(cPDFDNA));
    Result :=  AnsiSameStr(s, cPDFDNA);
  finally
    AData.Position := p;
  end;
end;

function TVmrDeliveryMethod.IsXMLFormat(aData:IevDualStream):boolean;
var
  s: String;
  p: Integer;
const
  cXMLPrefix = '<?xml';
begin
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cXMLPrefix));
    AData.Read(s[1], Length(cXMLPrefix));
    Result :=  AnsiSameStr(s, cXMLPrefix);
  finally
    AData.Position := p;
  end;
end;

procedure TVmrDeliveryMethod.AssignVmrFileNames(const R: TrwReportResults; IsIncludeDesc : boolean);
    var i,j:integer;
        sl:IisStringList;
  begin
    sl := TisStringList.Create;
    for i:= 0 to R.Count - 1 do
    begin
       R[i].VmrFileName := ValidateFileName(R[i].ReportName);

       if IsIncludeDesc then
         R[i].VmrFileName := Self.Description + '_' +R[i].VmrFileName;

       if sl.IndexOf(R[i].VmrFileName) <> -1 then
       begin
          j := 2;
          while sl.IndexOf(R[i].VmrFileName+ '('+ IntToStr(j)+ ')') <> -1 do
            Inc(j);
          R[i].VmrFileName := R[i].VmrFileName+ '('+ IntToStr(j)+ ')';

       end;
       sl.Add(R[i].VmrFileName);
    end;
end;

procedure TVmrDeliveryMethod.EmailReportResults(const R: TrwReportResults; const FromAddr, ToAddr :string; const IsTaxReturns: boolean);
var
  i: Integer;
  s, sPath : string;
  Has7ZFiles, IncludeDesc :boolean;
  Msg: IevMailMessage;
  sReportTypes: String;
begin
  s := '';
  Has7ZFiles := false;
  for i := 0 to R.Count-1 do
    s := s+ #13#10+ R[i].VmrJobDescr+ ' - '+ R[i].ReportName;

  Msg := ctx_EMailer.CreateMessage;

  IncludeDesc := IsIncludeDescInEmail(BoxNbr);
  AssignVmrFileNames(R,IncludeDesc);

  sPath := IncludeTrailingPathDelimiter(sPath);
  if IsTaxReturns then
    sReportTypes := 'Tax return files'
  else
    sReportTypes := 'Payroll files';
  if IncludeDesc then
     Msg.Subject := Self.Description + ' ' + sReportTypes
  else
     Msg.Subject := sReportTypes;
  Msg.AddressTo := ToAddr;
  Msg.AddressFrom := OverrideFromAddr(FromAddr);
  SetupEmailerByFromAddress(Msg.AddressFrom);

  for i := 0 to R.Count-1 do
  begin
    if  Assigned(R[i].Data) then
    begin

       if IsPDFFormat(R[i].Data) then
          Msg.AttachStream(R[i].VmrFileName + '.pdf', R[i].Data)
       else if IsZipFormat(R[i].Data) then
       begin
          Msg.AttachStream(R[i].VmrFileName + '.zip', R[i].Data);
          Has7ZFiles := true;
       end else
          Msg.AttachStream(R[i].VmrFileName + '.rwa', R[i].Data)
    end;
  end;
  Msg.Body := 'Your reports are in the attached files'#13#10#13#10 + s + #13#10;
  if Has7ZFiles then
  begin
      Msg.Body := Msg.Body + #13#10 +
              'To view .zip files you need to use winZip (www.winzip.com)'+ #13#10+
              'or free 7-Zip (www.7-zip.org) archiver.' + #13#10 ;
  end;

  if ConfNotice.Text = '' then
    Msg.Body := Msg.Body + #13#10 +
     'Confidentiality Notice:'#13#10 +
     'This information may contain confidential and/or privileged material and is'#13#10 +
     'only transmitted for the intended recipient.  Any review, re- transmission,'#13#10 +
     'on version or hard copy, copying, reproduction, circulation, publication,'#13#10 +
     'dissemination, or other use of, or taking of any action, or omission to take'#13#10 +
     'action, and reliance upon this information by persons or entities other than'#13#10 +
     'the intended recipient will be enforced by all legal means, including action'#13#10 +
     'for damages. If you receive this message in error, please contact the sender'#13#10 +
     'and delete the material from any computer, Disk drive, diskette, or other'#13#10 +
     'storage device or media.'
  else
     for i:= 0 to ConfNotice.Count-1 do
        Msg.Body := Msg.Body +  ConfNotice.Strings[i]+#13#10;

  try
    ctx_EMailer.SendMail(Msg);
  except
    on E: Exception do
    begin
      E.Message := 'Email server: '+ctx_EMailer.SmtpServer+' returned this error on attempt to deliver'#13+
                     E.Message+#13+#13+
                     'With next parameters:'+#13 +
                     'From Address:'+FromAddr +#13 +
                     'To Address:' +ToAddr;
      raise;
    end;
  end;
end;

procedure TVmrDeliveryMethod.EmailDirectly(const RL: TrwReportResults; const FromAddr: string;MailBoxNbr :integer; const NotificationEmail: string);
  var
     MailBoxCntNbr :integer;
     SelfSerfText:string;
  procedure CreateTempFile(const FileName, FileExtendtion: string; const Data: IEvDualStream; out OutFileName: string);
  var
    j: Integer;
    Path: string;
  begin
    repeat
      Path := AppTempFolder + IntToStr(RandomInteger(0, High(Integer)));
    until CreateDir(Path);
    Path := IncludeTrailingPathDelimiter(Path);
    OutFileName := Path+ ValidateFileName(FileName);
    if FileExists(OutFileName+ FileExtendtion) then
    begin
      j := 2;
      while FileExists(OutFileName+ '('+ IntToStr(j)+ ')'+ FileExtendtion) do
        Inc(j);
      OutFileName := OutFileName+ '('+ IntToStr(j)+ ')';
    end;
    OutFileName := OutFileName+ FileExtendtion;
    ForceDirectories(ExtractFilePath(OutFileName));
    Data.SaveToFile(OutFileName);
  end;
  procedure DeleteTempFile(const FileName: string);
  begin
    SysUtils.DeleteFile(FileName);
    SysUtils.RemoveDir(ExtractFilePath(FileName));
  end;

  function  EmailPDF(const idsPDF: IEvDualStream; const FromAddr, ToAddr, SmtpHost: string; IsTaxReturn : boolean): Boolean;
  var
    i:integer;
    Msg: IevMailMessage;
    s : string;
  begin
    Result := false;
    Msg := ctx_EMailer.CreateMessage;

    if IsTaxReturn then
      s := 'tax'
    else
      s := 'payroll';

    Msg.Subject := 'Your ' + s + ' information has arrived';

    Msg.AddressTo := ToAddr;
    Msg.AddressFrom := FromAddr;

    SetupEmailerByFromAddress(FromAddr);

    Msg.Body :=  SelfSerfText+#13#10+'You need your VMR password to view it.'#13#10;
    if ConfNotice.Text = '' then
    begin
       Msg.Body := Msg.Body + 'Confidentiality Notice:'#13#10 +
         'This information may contain confidential and/or privileged material and is'#13#10 +
         'only transmitted for the intended recipient.  Any review, re- transmission,'#13#10 +
         'on version or hard copy, copying, reproduction, circulation, publication,'#13#10 +
         'dissemination, or other use of, or taking of any action, or omission to take'#13#10 +
         'action, and reliance upon this information by persons or entities other than'#13#10 +
         'the intended recipient will be enforced by all legal means, including action'#13#10 +
         'for damages. If you receive this message in error, please contact the sender'#13#10 +
         'and delete the material from any computer, Disk drive, diskette, or other'#13#10 +
         'storage device or media.'
    end
    else
    begin
      for i:= 0 to ConfNotice.Count-1 do
        Msg.Body := Msg.Body +  ConfNotice.Strings[i]+#13#10;
    end;

    if IsTaxReturn then
       Msg.AttachStream('TaxInfo.pdf',idsPDF)
    else
       Msg.AttachStream('PayrollInfo.pdf',idsPDF);

    try
      ctx_EMailer.SendMail(Msg);
      FSentEmailCounter := FSentEmailCounter + 1;
      Result := true;
    except
      on E: Exception do
      begin
        FEmailErrorList.Add('(' + IntToStr(FEmailErrorList.Count+1) +') Email server ' + ctx_EMailer.SmtpServer +' reports: ' + E.Message+#13+
                           'With next parameters:'+#13 +
                           'From Address:'+FromAddr +#13 +
                           'To address:' +ToAddr+#13#13);
      end;
    end;
  end;

  function Process(const RL: TrwReportResults): string;
  var
    aFromAddr,s, sPassword, sEmail, sSentCntNbrs: string;
    i: Integer;
    rwPDFInfoRec: TrwPDFDocInfo;
    idsPDF: IEvDualStream;
    qsbmailbox : IevQuery;
    qsbOption : IevQuery;
    qsbMailBoxOption : IevQuery;
    IsTaxReturn : boolean;
    RL_Tax: TrwReportResults;
    DirectEmailVouchers: boolean;
  begin
      Result := '';
      qsbmailbox := TevQuery.Create('SELECT CL_NBR, CL_MAIL_BOX_GROUP_NBR FROM SB_MAIL_BOX a WHERE a.SB_MAIL_BOX_NBR = :SBNBR AND {AsOfNow<a>} ');
      qsbmailbox.Params.AddValue('SBNBR', BoxNbr);
      qsbmailbox.Execute;
      SelfSerfText := GetSelfServeText(qsbmailbox.Result.Fields[0].asInteger,
                                       qsbmailbox.Result.Fields[1].asInteger);

      qsbOption := TevQuery.Create('SELECT OPTION_STRING_VALUE FROM SB_OPTION a WHERE a.OPTION_TAG = ''SMTP_HOST'' AND {AsOfNow<a>} ');
      qsbOption.Execute;
      aFromAddr := OverrideFromAddr(FromAddr);
      sSentCntNbrs := '';

      qsbMailBoxOption := TevQuery.Create('SELECT OPTION_STRING_VALUE FROM SB_MAIL_BOX_OPTION a WHERE a.SB_MAIL_BOX_NBR = :SBNBR ' +
                                'AND a.OPTION_TAG='''+ VmrDirectEmailVouchers +''' AND {AsOfNow<a>} ');
      qsbMailBoxOption.Params.AddValue('SBNBR', BoxNbr);
      qsbMailBoxOption.Execute;
      DirectEmailVouchers := (qsbMailBoxOption.Result.RecordCount > 0) and
                              (Trim(qsbMailBoxOption.Result.FieldByName('OPTION_STRING_VALUE').asString) = GROUP_BOX_YES);

      RL_Tax := TrwReportResults.Create;
      i:=0;
      try
        while i < RL.Count do
        begin
           s := RL[i].VmrTag;


           MailBoxCntNbr := StrToInt(RL[i].Tag);
           Fetch(s, ';');
           Fetch(s, ';');
           sPassword := Fetch(s, ';');
           sEmail := Fetch(s, ';');

           // to get Tax Report which is TAX_RETURN_TYPE_FED_EE_EeCopy or TAX_RETURN_TYPE_FED_EE_EeCopy_W2
           IsTaxReturn := (RL[i].ReportType = rtTaxReturn) and (sPassword <> '') and (sEmail <> '');

           rwPDFInfoRec.Author := '';
           rwPDFInfoRec.Creator := '';
           rwPDFInfoRec.Subject := '';
           rwPDFInfoRec.Title := RL[i].ReportName;
           rwPDFInfoRec.Compression := True;
           rwPDFInfoRec.OwnerPassword := '';
           rwPDFInfoRec.UserPassword := '';
           rwPDFInfoRec.ProtectionOptions := [];

           if sPassword <> '' then
           begin
             rwPDFInfoRec.OwnerPassword := sPassword;
             rwPDFInfoRec.ProtectionOptions := [pdfPrint, pdfCopyInformation];
           end;

           idsPDF := ctx_RWLocalEngine.ConvertRWAtoPDF(RL[i].Data, rwPDFInfoRec);
           if (RL[i].ReportType = rtTaxReturn) and (not DirectEmailVouchers) then
           begin
             if (Trim(NotificationEmail)<>'') then
             begin
               RL[i].Data := idsPDF;
               RL[i].Collection := RL_Tax;
             end
           end
           else
           begin
             if idsPDF.Size <> 0 then
               if EmailPDF(idsPDF, aFromAddr, sEmail, Trim(qsbOption.Result.Fields[0].asString), IsTaxReturn) then
                 sSentCntNbrs := sSentCntNbrs + RL[i].Tag + ';';
             DeleteFromSelfServeEEs(RL[i].VmrEeNbr);
             inc(i);
           end;
        end;
        if RL_Tax.Count>0 then
        begin
          EmailReportResults(RL_Tax, aFromAddr, NotificationEmail, true);
          RL_Tax.Clear();
        end;
      finally
        RL_Tax.Free();
      end;
      Result := sSentCntNbrs;
  end;
var
  i: Integer;
  RlDirectEmail: TrwReportResults;
  qSbMailOption : IevQuery;
  sSentCntNbrs: string;
  bWasSuccess: boolean;
begin
  qSbMailOption := TevQuery.Create(Format(' SELECT SB_MAIL_BOX_CONTENT_NBR FROM SB_MAIL_BOX_OPTION a WHERE {AsOfNow<a>} '+
                                          ' AND a.SB_MAIL_BOX_NBR = :SBNBR AND (a.OPTION_TAG = ''%s'') and (SB_MAIL_BOX_CONTENT_NBR is NULL)',
                                          [VMR_CONTENT_EMAILED_FLAG]));
  qSbMailOption.Params.AddValue('SBNBR', MailBoxNbr);
  qSbMailOption.Execute;
  bWasSuccess := qSbMailOption.Result.RecordCount > 0;

  qSbMailOption := TevQuery.Create(Format(' SELECT SB_MAIL_BOX_CONTENT_NBR FROM SB_MAIL_BOX_OPTION a WHERE {AsOfNow<a>} '+
                                          ' AND a.SB_MAIL_BOX_NBR = :SBNBR AND (a.OPTION_TAG = ''%s'' OR a.OPTION_TAG = ''%s'')',
                                          [VMR_DIRECT_EMAILED_FLAG, VMR_CONTENT_EMAILED_FLAG])); //I don't know where VMR_DIRECT_EMAILED_FLAG is set
  qSbMailOption.Params.AddValue('SBNBR', MailBoxNbr);
  qSbMailOption.Execute;

  RlDirectEmail := TrwReportResults.Create;
  try
    i := 0;
    while i < RL.Count do
      if (RL[i].VmrTag <> '') and ((not qSbMailOption.Result.Locate('SB_MAIL_BOX_CONTENT_NBR',StrToInt(RL[i].Tag),[])) or bWasSuccess) then
        RL[i].Collection := RlDirectEmail
      else
        Inc(i);
    if RlDirectEmail.Count > 0 then
    begin
      sSentCntNbrs := Process(RlDirectEmail);
      MarkSentEmailContent(sSentCntNbrs);
    end;
  finally
    RlDirectEmail.Free;
  end;
end;

procedure TVmrDeliveryMethod.MarkSentEmailContent(const sCntNbrs: string);
begin
  FSentEmailContentNbrs := FSentEmailContentNbrs + sCntNbrs;
end;

procedure TVmrDeliveryMethod.MarkSentEmailContent(const R: TrwReportResults);
var
  i: integer;
  sCntNbrs: string;
begin
  sCntNbrs := '';
  for i := 0 to R.Count - 1 do
    if R[i].Tag <> '' then
      sCntNbrs := sCntNbrs + R[i].Tag + ';';
  MarkSentEmailContent(sCntNbrs);
end;

function TVmrDeliveryMethod.GetSelfServeText(CL_NBR,CL_MAIL_BOX_GROUP_NBR:integer):string;
begin
    if  (DM_CLIENT.CL_MAIL_BOX_GROUP.ClientID <> CL_NBR)
     or (not DM_CLIENT.CL_MAIL_BOX_GROUP.Active) then
    begin
     DM_CLIENT.CL_MAIL_BOX_GROUP.Close;
     DM_CLIENT.CL_MAIL_BOX_GROUP.CheckDSConditionAndClient(CL_NBR,'');
    end;
    ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_MAIL_BOX_GROUP,DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION]);
    Result :=Trim(ConvertNull(DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Lookup(
                              'CL_MAIL_BOX_GROUP_NBR;OPTION_TAG',
                              VarArrayOf([CL_MAIL_BOX_GROUP_NBR,VmrSelfServeEmailText]
                              ),'OPTION_STRING_VALUE'),''));
end;

procedure TVmrDeliveryMethod.CollectSelfServeEEs(
  const cd: TEvClientDataSet;
  const RL: TrwReportResults);
  var i:integer;
  l:IisStringList;
  DataSetIntf: IevDataSetHolder;
  s:string;
begin
 l := TisStringList.CreateUnique;
 for i:=0 to RL.Count-1 do
 begin
   if Trim(RL[i].Tag)<>'' then
     l.Add(RL[i].Tag);
 end;

 if l.Count =0 then
   exit;
 DataSetIntf := TevDataSetHolder.CreateCloneAndRetrieveData(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
      Format('SB_MAIL_BOX_NBR = %u and %s and OPTION_TAG=''%s''',
      [Integer(cd['SB_MAIL_BOX_NBR']),
       BuildInSqlStatement('SB_MAIL_BOX_CONTENT_NBR', l.CommaText),
       VMR_SELF_SERVE_EE_NBRS]));

  with DataSetIntf do
  begin
    DataSet.First;
    s := '';
    l.Clear;
    while not DataSet.Eof do
    begin
       s := DataSet.FieldByName('OPTION_STRING_VALUE').AsString;
       while s <> '' do
       begin
         l.Add(Fetch(s,','));
       end;
       DataSet.Next;
    end;

    if l.Count >0 then
    begin
      SetLength(FSelfServeArray, Length(FSelfServeArray)+1);
      FSelfServeArray[High(FSelfServeArray)].SelfServeEEs := l;
      FSelfServeArray[High(FSelfServeArray)].SelfServeText :=
           GetSelfServeText(cd.FieldByName('CL_NBR').AsInteger,
                            cd.FieldByName('CL_MAIL_BOX_GROUP_NBR').AsInteger);
      FSelfServeArray[High(FSelfServeArray)].FromAddress := Options.FieldByName('FROM_ADDRESS').AsString;
    end;

  end;
end;

procedure TVmrDeliveryMethod.DeleteFromSelfServeEEs(EeNbr: integer);
  var i:integer;
  ee:string;
begin
//  exit;
  ee := IntToStr(EeNbr);
  for i := 0 to High(FSelfServeArray) do
  begin
     while FSelfServeArray[i].SelfServeEEs.IndexOf(ee)<>-1 do
       FSelfServeArray[i].SelfServeEEs.Delete(FSelfServeArray[i].SelfServeEEs.IndexOf(ee));
  end;
end;

procedure TVmrDeliveryMethod.AddToRelease(const Media: TVmrMediaType;
  const cd: TEvClientDataSet);
  procedure AddToDirectEmail(const RL: TrwReportResults);
  var
    RD: TrwReportResults;
    i: Integer;
  begin
    RD := TrwReportResults.Create;
    try
      i := 0;
      while i < RL.Count do
      begin
        if RL[i].VmrTag <> '' then
          RL[i].Collection := RD
        else
          Inc(i);
      end;
//      if (RD.Count > 0) and (ReleaseType in [vrtSendVouchers,vrtPrinterVouchers]) then
      if (RD.Count > 0){ and (vrtVouchers in ReleaseType) }then
      begin
        SetLength(FDirectEmailParamArray, Length(FDirectEmailParamArray)+1);
        FDirectEmailParamArray[High(FDirectEmailParamArray)] := RD;
      end
      else
        FreeAndNil(RD);
    except
      RD.Free;
      raise;
    end;
  end;
var
  RL: TrwReportResults;
begin
  cd.Edit;
  try
    if (vrtPrinter in ReleaseType) and not(CheckRemoteVMRActive and InheritsFrom(TVmrRealDelivery) and (Length(Location)> 0) and (Location[1] = '@')) then
      cd['PRINTED_TIME'] := SysTime;
    RL := Media.CreateResultCollection(cd);
    if Assigned(RL) then
    try
      CollectSelfServeEEs(cd,RL);
      AddToDirectEmail(RL);
      if (RL.Count > 0) and ((vrtPrinter in ReleaseType) or (vrtStoreASCIIFiles in ReleaseType)) then
      begin
          InternalAddToRelease(Media, cd, RL);
      end
      else
      begin
         if  vrtPrinter in ReleaseType then
         begin
          cd['SCANNED_TIME'] := cd['PRINTED_TIME'];
          cd['SHIPPED_TIME'] := cd['PRINTED_TIME'];
         end;
      end;
    finally
      RL.Free;
    end
    else
    begin
      if vrtPrinter in ReleaseType then
      begin
        cd['SCANNED_TIME'] := cd['PRINTED_TIME'];
        cd['SHIPPED_TIME'] := cd['PRINTED_TIME'];
      end;
    end;
    cd.Post;
  except
    cd.Cancel;
    raise;
  end;
end;

function TVmrDeliveryMethod.CreateSbDeliveryService(const cd: TEvClientDataSet): TVmrDeliveryService;
var
  c: TClass;
begin
  ctx_DataAccess.Activate([DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD, DM_SYSTEM_MISC.SY_DELIVERY_SERVICE, DM_SYSTEM_MISC.SY_DELIVERY_METHOD]);
  Assert(DM_SYSTEM_MISC.SY_DELIVERY_SERVICE.Locate('SY_DELIVERY_SERVICE_NBR',
    DM_SYSTEM_MISC.SY_DELIVERY_METHOD.Lookup('SY_DELIVERY_METHOD_NBR',
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Lookup('SB_DELIVERY_METHOD_NBR',
        cd['SB_DELIVERY_METHOD_NBR'], 'SY_DELIVERY_METHOD_NBR'),
      'SY_DELIVERY_SERVICE_NBR'),
    []));
  Assert(DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE.Locate('SY_DELIVERY_SERVICE_NBR', DM_SYSTEM_MISC.SY_DELIVERY_SERVICE['SY_DELIVERY_SERVICE_NBR'], []));
  c := FindVmrClass(DM_SYSTEM_MISC.SY_DELIVERY_SERVICE['CLASS_NAME']);
  Assert(Assigned(c));
  Assert(c.InheritsFrom(TVmrOptionedType));
  Result := TVmrOptionedTypeClass(c).CreateSbSetup(DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE['SB_DELIVERY_SERVICE_NBR']) as TVmrDeliveryService;
end;

function TVmrDeliveryMethod.GetPickupSheetReport(const Job: TVmrPrintJob): TrwReportResult;
var
  P: TrwRepParams;
  ms: IisStream;
  RR: TrwReportResults;
begin
  Result := nil;
  P := GetPickupSheetReportParams(Job);
  if Assigned(P) then
  begin
    with ctx_RWLocalEngine do
    begin
      StartGroup;
      CalcPrintReport(P);
      ms := EndGroup(rdtNone);
    end;
    RR := TrwReportResults.Create(ms);
    try
      if RR.Count > 0 then
      begin
        if RR[0].ErrorMessage <> '' then
          raise EMissingReport.CreateHelp('Pickup sheet report failed with error: '+ RR[0].ErrorMessage, IDH_MissingReport);
        Result := RR[0];
        Result.Collection := nil;
      end;
    finally
      RR.Free;
    end;
  end;
end;

function TVmrDeliveryMethod.GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams;
begin
  Result := TrwRepParams.Create(nil);
  Result.NBR := 0;
  Result.Level := 'S';
  //Result.Params.Add('SbMailBoxNbr', BoxNbr);
  Result.Params.Add('PartCount', Job.PartCount);
  Result.Params.Add('PartNumber', Job.PartNumber);
  Job.Res.Descr := Job.BarCode+ Format('%2.2u%2.2u', [Job.PartNumber, Job.PartCount]); // barcode
  Result.Params.Add('BarCode', Job.Res.Descr);
  Result.Params.Add('SbMailBoxNbr', Job.SbMailBoxNbr);
  Result.Params.Add('UplinkSbMailBoxNbr', Job.SbUpLevelMailBoxNbr);
  Result.Params.Add('SbMailBoxContentNbrs', Job.SbContentNbrs);
  Result.Params.Add('ClNbr', Job.ClNbr);
  Result.Params.Add('FirstCoNbr', Job.CoNbr);
  Result.Params.Add('FirstPrNbr', Job.PrNbr);
  if not VarIsNull(Job.SbUpLevelMailBoxNbr) then
    Result.Params.Add('Description', Job.Description); 
end;

procedure TVmrDeliveryMethod.PrepareOptionFields(
  var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited;
  f.Add('SMTP_SERVER', ftString, 40);
  //EMail Address 
  f.Add('FROM_ADDRESS', ftString, 80);
  f.Add('USER_NAME', ftString, 40);
  f.Add('PASSWORD', ftString, 40);   
end;

procedure TVmrDeliveryMethod.PrepareOptions(const Nbr: Integer; out Table:
        TddTableClass; out KeyFields: TVmtKeyFieldType; out KeyValues:
        TVmtKeyValueType);
var
  v: Variant;
begin
  Table := TSB_DELIVERY_SERVICE_OPT;
  SetLength(KeyFields, 2);
  KeyFields[0] := 'SB_DELIVERY_SERVICE_NBR';
  KeyFields[1] := 'SB_DELIVERY_METHOD_NBR';
  SetLength(KeyValues, 2);
  v := DM_SYSTEM_MISC.SY_DELIVERY_METHOD.Lookup('SY_DELIVERY_METHOD_NBR', DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SY_DELIVERY_METHOD_NBR'], 'SY_DELIVERY_SERVICE_NBR');
  DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE.Activate;
  KeyValues[0] := DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE.Lookup('SY_DELIVERY_SERVICE_NBR', v, 'SB_DELIVERY_SERVICE_NBR');
  KeyValues[1] := Nbr;
end;

{
***************************** TVmrDeliveryService ******************************
}
procedure TVmrDeliveryService.PrepareOptions(const Nbr: Integer; out Table:
        TddTableClass; out KeyFields: TVmtKeyFieldType; out KeyValues:
        TVmtKeyValueType);
begin
  Table := TSB_DELIVERY_SERVICE_OPT;
  SetLength(KeyFields, 1);
  KeyFields[0] := 'SB_DELIVERY_SERVICE_NBR';
  SetLength(KeyValues, 1);
  KeyValues[0] := Nbr;
end;

{
******************************* TVmrRealDelivery *******************************
}
destructor TVmrRealDelivery.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(RLA) do
    FreeAndNil(RLA[i].Res);
  SetLength(RLA, 0);
  inherited;
end;

procedure TVmrRealDelivery.InternalAddToRelease(const Media: TVmrMediaType; const cd:
        TEvClientDataSet; const RL: TrwReportResults);
var
  RLAscii: TrwReportResults;
  i: Integer;
  procedure CopyASCIIResults;
  var
    i:integer;
    R :TrwReportResult;
  begin
      RLAscii := TrwReportResults.Create;
      for i:=0 to RL.Count - 1 do
      begin
          if RL[i].ReportType = rtASCIIFile then
          begin
             R := RLAscii.Add;
             R.Assign(RL[i]);
          end;
      end;
      if (RLAscii.Count > 0) and (vrtPrinter in ReleaseType)  then
        AddGroupedAscii(RLAscii, Media, cd)
      else
        FreeAndNil(RLAscii);

  end;
begin
  Self.RL := RL;
  if Media is TVmrEMedia then
  begin
    AddCdMedia(Media, cd);
    CopyASCIIResults();
  end
  else
  begin
    RLAscii := TrwReportResults.Create;
    try
      i := 0;
      while i < RL.Count do
        if RL[i].ReportType = rtASCIIFile then
          RL[i].Collection := RLAscii
        else
          Inc(i);
      if (RLAscii.Count > 0) and ((vrtPrinter in ReleaseType) or (vrtStoreASCIIFiles in ReleaseType))  then
        AddGroupedAscii(RLAscii, Media, cd)
      else
        FreeAndNil(RLAscii);
    finally
      //RLAscii.Free;
    end;
    if (RL.Count > 0) and (vrtPrinter in ReleaseType)then
        AddGroupedPaper(Media, cd);
  end;
end;

function TVmrRealDelivery.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrRealDeliveryEditFrame;
end;

function TVmrRealDelivery.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TVmrEmailEditFrame;
end;

class function TVmrRealDelivery.GetShippedMessage(cd: TEvClientDataSet): string;
begin
  Result := 'Your package has left our building and is on its way to you!';
end;

class function TVmrRealDelivery.OnPickup(cd: TEvClientDataSet): Boolean;
begin
  Result := True;
end;

class function TVmrRealDelivery.OnShipmentPacked(cd: TEvClientDataset): string;
begin
  Result := '';
end;

procedure TVmrRealDelivery.PrepareExtraOptionFields(var Prefix: string; var
        LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('EMAIL', ftString, 80);
  f.Add('SAVE_ASCII', ftInteger);
end;

procedure TVmrRealDelivery.PrepareOptionFields(var LoadRecordCount: Integer;
        const f: TFieldDefs);
begin
  inherited PrepareOptionFields(LoadRecordCount, f);
end;

procedure TVmrRealDelivery.PrepareToRelease;
var
  i: Integer;
begin
  for i := 0 to High(RLA) do
    FreeAndNil(RLA[i].Res);
  SetLength(RLA, 0);
  SetLength(RLA_ASCII, 0);

 PrInfo := (ctx_VmrRemote.GetPrinterList(Self.Location) as IisInterfacedObject).GetClone as IevVMRPrintersList;
end;

procedure TVmrRealDelivery.Release(const cd: TEvClientDataSet);
  procedure AddTopLevelPickupSheet;
  var
    i: Integer;
  begin
    for i := 0 to High(RLA) do
      if VarIsNull(RLA[i].SbUpLevelMailBoxNbr) and (RLA[i].PartNumber = 1) then
        Exit;
    SetLength(RLA, Length(RLA)+1);
    i := High(RLA);
    RLA[i].SbMailBoxNbr := cd['SB_MAIL_BOX_NBR'];
    RLA[i].SbUpLevelMailBoxNbr := cd['UP_LEVEL_MAIL_BOX_NBR'];
    Assert(VarIsNull(RLA[i].SbUpLevelMailBoxNbr));
    RLA[i].Res := TrwReportResults.Create;
    RLA[i].ClNbr := cd['CL_NBR'];
    RLA[i].CoNbr := RLA[i-1].CoNbr;
    RLA[i].PrNbr := RLA[i-1].PrNbr;
    RLA[i].PrinterIndex := -1;
    RLA[i].PartNumber := 1;
    RLA[i].PartCount := 1;
    RLA[i].BarCode := VarToStr(cd['BARCODE']);
    RLA[i].SbContentNbrs := Null;
    RLA[i].Description := cd['DESCRIPTION'];
    RLA[i].cd := cd;
  end;

var
  i, j: Integer;
  r: TrwReportResult;
  send_em,store_fl:boolean;
begin
  if Length(RLA) > 0 then
    AddTopLevelPickupSheet;

  for i := 0 to High(RLA) do
  begin
    if RLA[i].PrinterIndex = -1 then
      for j := 0 to PrInfo.Count - 1 do
        if PrInfo[j].Active
        and ((PrInfo[j].Location = '') or AnsiSameText(PrInfo[j].Location, Location)) then
        begin
          RLA[i].PrinterIndex := j;
          RLA[i].Res.PrinterName := PrInfo[j].Name;
          Break;
        end;

    if RLA[i].PrinterIndex = -1 then
      raise EPrinterNotDefined.CreateFmt('Can not find printer for %s mailbox content', [RLA[i].Description]);

    if PrInfo[RLA[i].PrinterIndex].FindBinByMediaType(MEDIA_TYPE_PICKUP_SHEET) = nil then
      raise EPrinterNotDefined.CreateFmt('Printer %s does not have pickup sheets tray assigned', [PrInfo[RLA[i].PrinterIndex].Name]);
  end;

  for i := 0 to High(RLA) do
  begin
    Assert(cd.Locate('SB_MAIL_BOX_NBR', RLA[i].SbMailBoxNbr, []));

    R := GetPickupSheetReport(RLA[i]);
    if Assigned(R) then
    begin
      R.Collection := RLA[i].Res;
      R.Index := 0;
      R.Tray := PrInfo[RLA[i].PrinterIndex].FindBinNameByMediaType(MEDIA_TYPE_PICKUP_SHEET);
    end;
  end;

  if Length(RLA) > 0 then
  begin
    if CheckRemoteVMRActive and ((Length(Location)> 0) and (Location[1] = '@') ) then
    begin
      for i := 0 to High(RLA) do
        //If we have returned to unreleased, we must delete cover sheets and labels before saving them again
        DeleteCoverSheetsAndLabelsForRemotePrint(RLA[i].SbMailBoxNbr);

      StoreCoverSheetsAndLabelsForRemotePrint;
    end
    else
      PrintVmrReports;
  end;

  if (Length(RLA_ASCII) > 0) then
  begin
    send_em :=  ConvertNull(Trim(ExtraOptions.FieldByName('EMAIL').AsString),'') <> '';
    store_fl := ConvertNull(ExtraOptions.FieldByName('SAVE_ASCII').AsInteger,0) = 1;
    if (store_fl or send_em) then
    begin
      for i := 0 to High(RLA_ASCII) do
      begin
        if  vrtStoreASCIIFiles in ReleaseType then
          if store_fl then
            StoreReportResults(RLA_ASCII[i],not send_em);

        if (send_em) and (vrtPrinter in ReleaseType) then
        begin
          EmailReportResults(RLA_ASCII[i], Options.FieldByName('FROM_ADDRESS').AsString,
                             ExtraOptions.FieldByName('EMAIL').AsString, false);
          MarkSentEmailContent(RLA_ASCII[i]);
        end;
      end;
    end

    else
      raise EevException.Create('Please, specify an email address for the ASCII files included in this mailbox.');
  end;

  inherited;
end;

procedure TVmrRealDelivery.PrintVmrReports;
var
  i:integer;
begin
  for i := 0 to High(RLA) do
    ctx_VMRRemote.PrintRwResults(RLA[i].Res);
end;

procedure TVmrRealDelivery.StoreCoverSheetsAndLabelsForRemotePrint;
var
  i, j:integer;
  sFileName: string;
begin
  for i := 0 to High(RLA) do
  begin

    for j := 0 to RLA[i].Res.Count - 1 do
    begin
      //Only store cover letters and labels (Tag is empty)
      if RLA[i].Res[j].Tag = '' then
      begin
        ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
        
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Append;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['SB_MAIL_BOX_NBR'] := RLA[i].SbMailBoxNbr;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['DESCRIPTION'] := FormatJobInfo(RLA[i].Res[j].VmrCoNbr, RLA[i].Res[j].VmrPrNbr, RLA[i].Res[j].VmrEventDate, VmrCoverSheetsAndLabelsFolder, RLA[i].Res[j].ReportName);
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['MEDIA_TYPE'] := RLA[i].Res[j].MediaType;

        sFileName := IncludeTrailingPathDelimiter(RLA[i].ClNbr) +
          IncludeTrailingPathDelimiter(FormatDateTime('MMDDYY', SysTime))+
          IncludeTrailingPathDelimiter(Copy(VmrCoverSheetsAndLabelsFolder, 1, 10))+
          IntToHex(RandomInteger(0, High(Integer)), 8)+ '.rwa';
        Assert(Length(sFileName) <= 80, 'Error saving FILE_NAME to SB_MAILBOX_CONTENT');
        sFileName := ctx_VmrRemote.StoreFile(sFileName, RLA[i].Res[j].Data, True);
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['FILE_NAME'] := sFileName;
        if RLA[i].Res[j].PagesInfo.Count = 1 then
        begin
           DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['PAGE_COUNT'] := RLA[i].Res[j].PagesInfo[0].DuplexPageCount+ RLA[i].Res[j].PagesInfo[0].SimplexPageCount;
        end else
           DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['PAGE_COUNT'] := 0;

        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Post;
      end;
    end;
  end;
  ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
end;

procedure TVmrRealDelivery.DeleteCoverSheetsAndLabelsForRemotePrint(MailBoxNbr: integer);
var
  sNbr: string;
begin
  if MailBoxNbr > 0 then
  begin
    sNbr := BuildINsqlStatement('SB_MAIL_BOX_NBR', IntToStr(MailBoxNbr));
    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired(sNbr);
    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Last;

    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Bof do
    begin
      if StartsWith(DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['Description'], VmrCoverSheetsAndLabelsFolder) then
      begin
        if (DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['FILE_NAME'] <> '') then
          ctx_VmrRemote.DeleteFile(DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['FILE_NAME']);
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Delete;
      end
      else
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Prior;
    end;
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
  end;
end;

{
************************** TVmrPackagedDeliveryMethod **************************
}
class function TVmrPackagedDeliveryMethod.GetMediaClasses:
        TVmrMediaTypeClassArray;
begin
  SetLength(Result, 2);
  Result[0] := TVmrGroupedPaperMediaType;
  Result[1] := TVmrEMedia;
end;

{ TVmrDeliveryService }

{
************************* TVmrSeparatedPaperMediaType **************************
}
function TVmrSeparatedPaperMediaType.GetCoverLetter(const cd: TevClientDataSet;
        const Params: TReportParamArrayType): TrwReportResults;
begin
  Result := nil;
end;

function TVmrSeparatedPaperMediaType.GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result :=  TVmrEMediaClParamFrame;
end;

function TVmrSeparatedPaperMediaType.GetPickupSheetMediaNotes: string;
begin
  Result := 'Stuff before mailing';
end;

{
************************** TVmrGroupedPaperMediaType ***************************
}
function TVmrGroupedPaperMediaType.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrGroupedPaperMediaEditFrame;
end;

function TVmrGroupedPaperMediaType.GetPickupSheetMediaNotes: string;
begin
  Result := '';
  if ExtraOptions.FieldByName('STUFF_CHECKS').AsString = 'Y' then
    Result := 'Checks - Staff'
  else
    Result := 'Checks - DO NOT Staff';
  if ExtraOptions.FieldByName('STUFF_W2').AsString = 'Y' then
    Result := 'W2 - Staff'
  else
    Result := 'W2 - DO NOT Staff';
  if ExtraOptions.FieldByName('STUFF_1099').AsString = 'Y' then
    Result := '1099 - Staff'
  else
    Result := '1099 - DO NOT Staff';
end;

procedure TVmrGroupedPaperMediaType.PrepareExtraOptionFields(var Prefix: string;
        var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('STUFF_CHECKS', ftString, 1);
  f.Add('STUFF_W2', ftString, 1);
  f.Add('STUFF_1099', ftString, 1);
end;

{
************************* TVmrAddressedDeliveryMethod **************************
}
function TVmrAddressedDeliveryMethod.GetExtraOptionEditFrameClass:
        TVmrOptionEditFrameClass;
begin
  Result := TVmrAddressedEditFrame;
end;

function TVmrAddressedDeliveryMethod.GetPickupSheetReportParams(const Job: TVmrPrintJob): TrwRepParams;
begin
  Result := inherited GetPickupSheetReportParams(Job);
  Result.NBR := 761;
  Result.Params.Add('Addr0', VarToStr(ExtraOptions['NAME']));
  Result.Params.Add('Addr1', VarToStr(ExtraOptions['ADDRESS1']));
  Result.Params.Add('Addr2', VarToStr(ExtraOptions['ADDRESS2']));
  Result.Params.Add('Addr3', VarToStr(ExtraOptions['CITY'])+ ' '+
    VarToStr(ExtraOptions['STATE'])+ '  '+ VarToStr(ExtraOptions['ZIP']));
  Result.Params.Add('Note', VarToStr(ExtraOptions['NOTE']));
end;

procedure TVmrAddressedDeliveryMethod.PopulateNewMailBox;
begin
  DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := ExtraOptions.FieldByName('NAME').AsString+ '; '+
    ExtraOptions.FieldByName('ADDRESS1').AsString+ '; '+
    ExtraOptions.FieldByName('ADDRESS2').AsString+ '; '+
    ExtraOptions.FieldByName('CITY').AsString+ ','+ ExtraOptions.FieldByName('STATE').AsString+ ' '+
    ExtraOptions.FieldByName('ZIP').AsString;
end;

procedure TVmrAddressedDeliveryMethod.PrepareExtraOptionFields(var Prefix:
        string; var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited PrepareExtraOptionFields(Prefix, LoadRecordCount, f);
  f.Add('NAME', ftString, 80);
  f.Add('ADDRESS1', ftString, 80);
  f.Add('ADDRESS2', ftString, 80);
  f.Add('CITY', ftString, 80);
  f.Add('STATE', ftString, 2);
  f.Add('ZIP', ftString, 10);
  f.Add('NOTE', ftString, 512);

end;

{ TVmrDeliveryMethod }

{ TVmrAddressedDeliveryMethod }

{ TVmrGroupedPaperMediaType }

{
***************************** TVmrAccountedService *****************************
}
function TVmrAccountedService.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TAccountedSbParamFrame;
end;

procedure TVmrAccountedService.PrepareOptionFields(var LoadRecordCount: Integer;
        const f: TFieldDefs);
begin
  f.Add('ACC_NUMBER', ftString, 20);
end;

{
******************************** TVmrMediaType *********************************
}
function TVmrMediaType.CreateResultCollection(const cd: TevClientDataSet):
        TrwReportResults;
var
  i: Integer;
  cdContent: TevClientDataSet;
  cdEeSetup: TevClientDataSet;
  sEeSetupCondition: string;
  R: TrwReportResult;
  Rs: TrwReportResults;
  P: TReportParamArrayType;
begin
  Result := nil;
  cdContent := TevClientDataSet.Create(nil);
  cdEeSetup := TevClientDataSet.Create(nil);
  try
    cdContent.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    with TExecDSWrapper.Create('VmrSbMailBoxContents') do
    begin
      SetParam('BoxNbr', BoxNbr);
      cdContent.DataRequest(AsVariant);
    end;
    DM_SERVICE_BUREAU.SB_MAIL_BOX.CheckDSCondition('SB_MAIL_BOX_NBR = '+IntToStr(BoxNbr));
    ctx_DataAccess.OpenDataSets([cdContent]);
    if cdContent.RecordCount > 0 then
    begin
      with cdContent.GetFieldValueList('EE_NBR', True) do
      try
        if Count = 0 then
          sEeSetupCondition := ''
        else if Count < 14000 then
          sEeSetupCondition := BuildInSqlStatement('t1.EE_NBR', CommaText)
        else
          sEeSetupCondition := 't1.EE_NBR > 0';
      finally
        Free;
      end;
      if sEeSetupCondition <> '' then
        with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
        begin
          SetMacro('CONDITION', 't2.WEB_PASSWORD <> '''' and t2.E_MAIL_ADDRESS <> '''' and '+ sEeSetupCondition);
          SetMacro('COLUMNS', 't1.EE_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, t2.WEB_PASSWORD, t2.E_MAIL_ADDRESS EMAIL');
          SetMacro('TABLE1', 'EE');
          SetMacro('TABLE2', 'CL_PERSON');
          SetMacro('JOINFIELD', 'CL_PERSON');
          cdEeSetup.IndexFieldNames := 'EE_NBR';
          ctx_DataAccess.GetCustomData(cdEeSetup, AsVariant);
        end;
      Result := TrwReportResults.Create;
      cdContent.First;
      while not cdContent.Eof do
      begin

        if cdContent.FieldByName('EE_NBR').IsNull
        or not cdEeSetup.Active
        or not cdEeSetup.FindKey([cdContent.FieldByName('EE_NBR').AsInteger]) then
        begin
          R := Result.AddReportResult(cdContent.FieldByName('REP_DESCR').AsString,
            TReportType(cdContent.FieldByName('REPORT_TYPE').AsInteger),
            ctx_VMRRemote.RetrieveFile(cdContent.FieldByName('FILE_NAME').AsString));
          //R.VmrFileName := cdContent.FieldByName('FILE_NAME').AsString;
          R.MediaType := cdContent.FieldByName('MEDIA_TYPE').AsString;
          R.Layers := StrToLayers(cdContent.FieldByName('LAYERS').AsString);
          R.VmrJobDescr := cdContent.FieldByName('JOB_DESCR').AsString;
          R.VmrCoNbr := cdContent.FieldByName('CO_NBR').AsInteger;
          R.VmrPrNbr := cdContent.FieldByName('PR_NBR').AsInteger;
          R.VmrEeNbr := cdContent.FieldByName('EE_NBR').AsInteger;
          R.VmrEventDate := EncodeDate(StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 1, 4), 1990),
            StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 5, 2), 1),
            StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 7, 2), 1));
          if R.PagesInfo.Count = 0 then
            R.PagesInfo.Add;
          R.PagesInfo[0].SimplexPageCount := cdContent.FieldByName('SIMPLEX_PAGES').AsInteger;
          R.PagesInfo[0].DuplexPageCount := cdContent.FieldByName('DUPLEX_PAGES').AsInteger;
          R.Tag := cdContent.FieldByName('SB_MAIL_BOX_CONTENT_NBR').AsString;
          R.VmrTag := '';
        end;
        cdContent.Next;
      end;

      SetLength(P, 3);
      P[0].ParamName := 'SbMailBoxNbr';
      P[0].ParamValue := BoxNbr;
      P[1].ParamName := 'Clients';
      P[1].ParamValue := VarArrayOf([]);
      P[2].ParamName := 'Companies';
      P[2].ParamValue := VarArrayOf([]);

      Rs := GetCoverLetter(cd, P);
      if Assigned(Rs) then
      begin
        for i := Rs.Count-1 downto 0 do
        begin
          R := Rs[i];
          R.Collection := Result;
          R.Index := 0;
        end;
      end;
      cdContent.First;
      while not cdContent.Eof do
      begin

        if not cdContent.FieldByName('EE_NBR').IsNull
        and cdEeSetup.Active
        and cdEeSetup.FindKey([cdContent.FieldByName('EE_NBR').AsInteger]) then
        begin
          R := Result.AddReportResult(cdContent.FieldByName('REP_DESCR').AsString,
            TReportType(cdContent.FieldByName('REPORT_TYPE').AsInteger),
            ctx_VMRRemote.RetrieveFile(cdContent.FieldByName('FILE_NAME').AsString));
          //R.VmrFileName := cdContent.FieldByName('FILE_NAME').AsString;
          R.MediaType := cdContent.FieldByName('MEDIA_TYPE').AsString;
          R.Layers := StrToLayers(cdContent.FieldByName('LAYERS').AsString);
          R.VmrJobDescr := cdContent.FieldByName('JOB_DESCR').AsString;
          R.VmrCoNbr := cdContent.FieldByName('CO_NBR').AsInteger;
          R.VmrPrNbr := cdContent.FieldByName('PR_NBR').AsInteger;
          R.VmrEeNbr := cdContent.FieldByName('EE_NBR').AsInteger;
          R.VmrEventDate := EncodeDate(StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 1, 4), 1990),
            StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 5, 2), 1),
            StrToIntDef(Copy(cdContent.FieldByName('EVENT_DATE').AsString, 7, 2), 1));
          if R.PagesInfo.Count = 0 then
            R.PagesInfo.Add;
          R.PagesInfo[0].SimplexPageCount := cdContent.FieldByName('SIMPLEX_PAGES').AsInteger;
          R.PagesInfo[0].DuplexPageCount := cdContent.FieldByName('DUPLEX_PAGES').AsInteger;
          R.Tag := cdContent.FieldByName('SB_MAIL_BOX_CONTENT_NBR').AsString;
          R.VmrTag := MakeString([cdContent.FieldByName('EE_NBR').AsString, Trim(cdEeSetup.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), Trim(cdEeSetup.FieldByName('WEB_PASSWORD').AsString), Trim(cdEeSetup.FieldByName('EMAIL').AsString)], ';');
        end;
        cdContent.Next;
      end;
    end;
  finally
    cdContent.Free;
  end;
end;

function TVmrMediaType.GetCoverLetter(const cd: TevClientDataSet; const Params:
        TReportParamArrayType): TrwReportResults;
var
  P: TrwReportParams;
begin
  ctx_RWLocalEngine.StartGroup;
  P := TrwReportParams.Create;
  try
    if not VarIsNull(cd['SB_COVER_LETTER_REPORT_NBR']) then
    begin
      P.LoadFromParamArray(Params);
      ctx_RWLocalEngine.CalcPrintReport(cd['SB_COVER_LETTER_REPORT_NBR'], 'B', P);
    end
    else
    if not VarIsNull(cd['SY_COVER_LETTER_REPORT_NBR']) then
    begin
      P.Clear;
      P.LoadFromParamArray(Params);
      ctx_RWLocalEngine.CalcPrintReport(cd['SY_COVER_LETTER_REPORT_NBR'], 'S', P);
    end;
  finally
    Result := TrwReportResults.Create(ctx_RWLocalEngine.EndGroup(rdtNone));
  end;
end;

function TVmrMediaType.OnPickupSheetScan(cd: TEvClientDataSet): string;
begin
  Result := '';
end;

procedure TVmrMediaType.PrepareOptions(const Nbr: Integer; out Table:
        TddTableClass; out KeyFields: TVmtKeyFieldType; out KeyValues:
        TVmtKeyValueType);
begin
  Table := TSB_MEDIA_TYPE_OPTION;
  SetLength(KeyFields, 1);
  KeyFields[0] := 'SB_MEDIA_TYPE_NBR';
  SetLength(KeyValues, 1);
  KeyValues[0] := Nbr;
end;

function InstanceClOptionedType(const ds1, ds2: TevClientDataSet; const KeyField, Prefix: string): TVmrOptionedType;
var
  c: TClass;
  PkKeyFieldName: string;
begin
  Assert(ds1.Active);
  Result := nil;
  ds2.CheckDSCondition('');
  ctx_DataAccess.OpenDataSets([ds2]);
  PkKeyFieldName := ds2.Name+ '_NBR';
  ds2.IndexFieldNames := PkKeyFieldName;
  if ds2.FindKey([ds1[KeyField]]) then
  begin
    c := FindVmrClass(ds2['CLASS_NAME']);
    if Assigned(c) then
      if c.InheritsFrom(TVmrOptionedType) then
        Result := TVmrOptionedTypeClass(c).CreateClMailBoxSetup(ds2[PkKeyFieldName], Prefix, ds1['CL_MAIL_BOX_GROUP_NBR']);
  end;
end;

function InstanceClPrimaryDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
begin
  Result := InstanceClOptionedType(ds1, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD, 'PRIM_SB_DELIVERY_METHOD_NBR', 'PD') as TVmrDeliveryMethod;
end;

function InstanceClSecondaryDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
begin
  Result := InstanceClOptionedType(ds1, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD, 'SEC_SB_DELIVERY_METHOD_NBR', 'SD') as TVmrDeliveryMethod;
end;

function InstanceClPrimaryMedia(const ds1: TevClientDataSet): TVmrMediaType;
begin
  Result := InstanceClOptionedType(ds1, DM_SERVICE_BUREAU.SB_MEDIA_TYPE, 'PRIM_SB_MEDIA_TYPE_NBR', 'PM') as TVmrMediaType;
end;

function InstanceClSecondaryMedia(const ds1: TevClientDataSet): TVmrMediaType;
begin
  Result := InstanceClOptionedType(ds1, DM_SERVICE_BUREAU.SB_MEDIA_TYPE, 'SEC_SB_MEDIA_TYPE_NBR', 'SM') as TVmrMediaType;
end;

function InstanceSbOptionedType(const ds1, ds2: TevClientDataSet; const KeyField, Prefix: string): TVmrOptionedType;
var
  c: TClass;
  PkKeyFieldName: string;
begin
  Assert(ds1.Active);
  Result := nil;
  ds2.CheckDSCondition('');
  ctx_DataAccess.OpenDataSets([ds2]);
  PkKeyFieldName := ds2.Name+ '_NBR';
  ds2.IndexFieldNames := PkKeyFieldName;
  if ds2.FindKey([ds1[KeyField]]) then
  begin
    c := FindVmrClass(ds2['CLASS_NAME']);
    if Assigned(c) then
      if c.InheritsFrom(TVmrOptionedType) then
      begin
        Result := TVmrOptionedTypeClass(c).CreateSbMailBoxSetup(ds2[PkKeyFieldName], Prefix, ds1['sb_mail_box_nbr']);
        Result.BoxNbr := ds1['sb_mail_box_nbr'];
      end;
  end;
end;

function InstanceSbDeliveryMethod(const ds1: TevClientDataSet): TVmrDeliveryMethod;
begin
  Result := InstanceSbOptionedType(ds1, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD, 'SB_DELIVERY_METHOD_NBR', 'PD') as TVmrDeliveryMethod;
  if Assigned(Result) then
    Result.Description := VarToStr(ds1['DESCRIPTION']);
end;

function InstanceSbMedia(const ds1: TevClientDataSet): TVmrMediaType;
begin
  Result := InstanceSbOptionedType(ds1, DM_SERVICE_BUREAU.SB_MEDIA_TYPE, 'SB_MEDIA_TYPE_NBR', 'PM') as TVmrMediaType;
end;

function LayersToStr(const l: TrwPrintableLayers): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 255 do
    if TrwLayerNumber(i) in l then
      if Result = '' then
        Result := IntToStr(i)
      else
        Result := Result+ ','+ IntToStr(i);
end;

function StrToLayers(s: string): TrwPrintableLayers;
var
  i: Integer;
begin
  Result := [];
  while s <> '' do
  begin
    i := StrToInt(Fetch(s, ','));
    Result := Result+ [TrwLayerNumber(i)];
  end;
end;

function FormatJobInfo(const CoNbr, PrNbr: Integer; const EventDate: TDateTime; const Descr, Descr2: string): string;
begin
  Result := Format('%-99.99s %-99.99s %-8.8s%-10u%-10u', [Descr, Descr2, FormatDateTime('YYYYMMDD', EventDate),  CoNbr, PrNbr]);
end;

function FormatBarCode(const iSbNbr, iPartNumber, iPartCount: Integer): string;
begin
  Result := Format('%u%2.2u%2.2u', [iSbNbr, iPartNumber, iPartCount]);
end;

procedure CreateResultFiles(Path: string; const R: TrwReportResults;Ext:string = 'rwa');
var
  i, j: Integer;
begin
  Path := IncludeTrailingPathDelimiter(Path);
  for i := 0 to R.Count-1 do
  begin
    if not Assigned(R[i].Data) then
      Continue;

    R[i].VmrFileName := ValidateFileName(IncludeTrailingPathDelimiter(R[i].VmrJobDescr)+ R[i].ReportName);
    if FileExists(Path+ R[i].VmrFileName+ '.'+ext) then
    begin
      j := 2;
      while FileExists(Path+ R[i].VmrFileName+ '('+ IntToStr(j)+ ')'+ '.'+ext) do
        Inc(j);
      R[i].VmrFileName := R[i].VmrFileName+ '('+ IntToStr(j)+ ')';
    end;
    ForceDirectories(ExtractFilePath(Path+ R[i].VmrFileName));
    R[i].Data.SaveToFile(Path+ R[i].VmrFileName+ '.'+ext);
  end;

end;

procedure CreateTempFiles(out Path: string; const R:
        TrwReportResults;Ext:string ='rwa');
begin
  repeat
    Path := AppTempFolder + IntToStr(RandomInteger(0, High(Integer)));
  until CreateDir(Path);
  CreateResultFiles(Path, R,Ext);
end;

procedure DeleteTempFiles(const Path: string; const R:
        TrwReportResults;Ext:string = 'rwa');
var
  i: Integer;
  s: string;
begin
  for i := 0 to R.Count-1 do
    SysUtils.DeleteFile(Path+ R[i].VmrFileName + '.'+ext);
  for i := 0 to R.Count-1 do
  begin
    s := ExtractFilePath(R[i].VmrFileName+'.'+ext);
    if s <> '' then
      SysUtils.RemoveDir(Path+ s);
  end;
  RemoveDir(Path);
end;

procedure EncryptReportResults(const RL: TrwReportResults; const PrintPassword: string);
var
  i: Integer;
  t:IEvDualStream;
begin
  for i := 0 to RL.Count-1 do
    if Assigned(RL[i].Data) then
    begin
      t := RL[i].Data;
      ctx_RWLocalEngine.MakeSecureRWA(t, PrintPassword, PrintPassword);
      RL[i].Data := t;
    end;
end;




{ TVmrFedexService }

{
******************************* TVmrFedexService *******************************
}
class function TVmrFedexService.GetMethodClasses: TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 1);
  Result[0] := TVmrFedexPrepaid;
end;

{
******************************* TVmrFedexPrepaid *******************************
}
function TVmrFedexPrepaid.OnPickupSheetScan(cd: TEvClientDataset): string;
begin
  Result := 'Put all pages in FEDEX letter envelope and affix prepaid/preprinted FEDEX label';
end;

class function TVmrFedexPrepaid.OnShipmentPacked(cd: TEvClientDataset): string;
var
  s: string;
begin
  if EvDialog('Scan tracking information from the prepaid/preprinted FEDEX label', 'Tracking#', s) then
    if s <> '' then
    begin
      cd.Edit;
      cd['TRACKING_INFO'] := s;
      cd.Post;
    end;
end;

{ TVmrFedexDeliveryMethod }

{
*************************** TVmrFedexDeliveryMethod ****************************
}
class function TVmrFedexDeliveryMethod.GetShippedMessage(cd: TEvClientDataSet):
        string;
begin
  Result := inherited GetShippedMessage(cd);
  if not VarIsNull(cd['TRACKING_INFO']) then
    Result := Result+ #13'The tracking number for this shipment '+ cd['TRACKING_INFO']+
      #13'You can track your shipment at www.fedex.com';
end;

{ TVmrUpsService }

{
******************************** TVmrUpsPrepaid ********************************
}
{function TVmrUpsPrepaid.OnPickupSheetScan(cd: TEvClientDataset): string;
begin
  Result := 'Put all pages in UPS letter envelope and use prepaid/preprinted UPS letter label';
end;
}
{class function TVmrUpsPrepaid.OnShipmentPacked(cd: TEvClientDataset): string;
var
  s: string;
begin
  if EvDialog('Scan tracking information from new UPS prepaid/preprinted label', 'Tracking#', s) then
    if s <> '' then
    begin
      cd.Edit;
      cd['TRACKING_INFO'] := s;
      cd.Post;
    end;
end;
}
{
******************************** TVmrUpsService ********************************
}
{
class function TVmrUpsService.GetMethodClasses: TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 1);
  Result[0] := TVmrUpsPrepaid;
end;
}

{ TVmrUpsDeliveryMethod }

{
**************************** TVmrUpsDeliveryMethod *****************************
}
{class function TVmrUpsDeliveryMethod.GetShippedMessage(cd: TEvClientDataSet):
        string;
begin
  Result := inherited GetShippedMessage(cd);
  if not VarIsNull(cd['TRACKING_INFO']) then
    Result := Result+ #13'The tracking number for this shipment '+ cd['TRACKING_INFO']+
      #13'You can track your shipment at www.ups.com';
end;
}
{ TVmrCourierService }

{
***************************** TVmrCourier2Service ******************************
}
class function TVmrCourier2Service.GetMethodClasses:
        TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 1);
  Result[0] := TVmrCourier2DeliveryMethod;
end;

{ TVmrCourier2DeliveryMethod }

{
************************** TVmrCourier2DeliveryMethod **************************
}
class function TVmrCourier2DeliveryMethod.GetShippedMessage(cd:
        TEvClientDataSet): string;
begin
  Result := inherited GetShippedMessage(cd);
end;

function TVmrCourier2DeliveryMethod.OnPickupSheetScan(cd: TEvClientDataset):
        string;
begin
  Result := 'Put all pages in courier envelope';
end;

procedure TVmrRealDelivery.AddCdMedia(const Media: TVmrMediaType; const cd: TEvClientDataSet);
var
  j: Integer;
begin
  SetLength(RLA, Length(RLA)+1);
  RLA[High(RLA)].PrinterIndex := -1;
  RLA[High(RLA)].Res := TrwReportResults.Create;
  RLA[High(RLA)].SbMailBoxNbr := cd['SB_MAIL_BOX_NBR'];
  RLA[High(RLA)].SbUpLevelMailBoxNbr := cd['UP_LEVEL_MAIL_BOX_NBR'];
  RLA[High(RLA)].ClNbr := cd['CL_NBR'];
  RLA[High(RLA)].CoNbr := Null;
  RLA[High(RLA)].PrNbr := Null;
  for j := 0 to RL.Count-1 do
    if RL[j].VmrCoNbr <> 0 then
    begin
      RLA[High(RLA)].CoNbr := RL[j].VmrCoNbr;
      RLA[High(RLA)].PrNbr := RL[j].VmrPrNbr;
      Break;
    end;
  RLA[High(RLA)].PartNumber := 1;
  RLA[High(RLA)].PartCount := 1;
  RLA[High(RLA)].BarCode := VarToStr(cd['BARCODE']);
  RLA[High(RLA)].Description := cd['DESCRIPTION'];
  RLA[High(RLA)].cd := cd;
end;

procedure TVmrRealDelivery.AddGroupedPaper(const Media: TVmrMediaType;
  const cd: TEvClientDataSet);

  function CheckPresentedMediaTypes(const p: IevVMRPrinterInfo; const s: ShortString): ShortString;
  var
    i: Integer;
  begin
    Result := '';
    for i := 0 to P.BinCount - 1 do
      if P.Bins[i].MediaType <> ' ' then
        if Pos(P.Bins[i].MediaType, s) <> 0 then
          Result := Result+ P.Bins[i].MediaType;
  end;

  procedure RaiseMediaTypeException(const Types: string);
  var
    s: string;
    i: Integer;
  begin
    s := '';
    for i := 1 to Length(Types) do
      s := s+ ', '+ ReturnDescription(MediaType_ComboChoices, Types[i]);
    if Location = '' then
      raise EPrinterNotDefined.Create('Could not find a printer for media types: ' + Copy(s, 3, 1024))
    else
      raise EPrinterNotDefined.Create('Location '+ Location+ ' does not have printer for media types: ' + Copy(s, 3, 1024));
  end;

var
  RL2: TrwReportResults;
  i, j, k: Integer;
  iPrinterIndexes: array of Integer;
  sMediaTypes: ShortString;
  OrigRlaLength: Integer;
begin
  OrigRlaLength := Length(RLA);
  //break apart by printer;
  repeat
    sMediaTypes := '';
    for i := 0 to RL.Count-1 do
      if Pos(RL[i].MediaType, sMediaTypes) = 0 then
        sMediaTypes := sMediaTypes+ RL[i].MediaType;

    for i := 0 to PrInfo.Count - 1 do
      if PrInfo[i].Active and ((PrInfo[i].Location = '') or AnsiSameText(PrInfo[i].Location,Location)) then
        PrInfo[i].Tag := CheckPresentedMediaTypes(PrInfo[i], sMediaTypes)
      else
        PrInfo[i].Tag := '';

    j := 0;
    SetLength(iPrinterIndexes, 0);
    for i := 0 to PrInfo.Count - 1 do
      if Length(PrInfo[i].Tag) > j then
      begin
        SetLength(iPrinterIndexes, Length(iPrinterIndexes)+1);
        iPrinterIndexes[High(iPrinterIndexes)] := i;
        j := Length(PrInfo[i].Tag);
      end;

    if (j = 0) then
      RaiseMediaTypeException(sMediaTypes);

    i := RandomInteger(0, Length(iPrinterIndexes)); // i >= 0 and i < length
    Assert(i <= High(iPrinterIndexes)); // just to make sure the prev line works as expected
    k := iPrinterIndexes[i];
    RL2 := TrwReportResults.Create;
    RL2.PrinterName := PrInfo[k].Name;
    SetLength(RLA, Length(RLA)+1);
    RLA[High(RLA)].PrinterIndex := k;
    RLA[High(RLA)].Res := RL2;
    i := 0;
    while i < RL.Count do
      if Pos(RL[i].MediaType, PrInfo[k].Tag) <> 0 then
      begin
        RL[i].Tray := PrInfo[k].FindBinNameByMediaType(RL[i].MediaType);
        RL[i].Collection := RL2;
      end
      else
        Inc(i);
  until RL.Count = 0;
  
  for i := OrigRlaLength to High(RLA) do
  begin
    RLA[i].SbMailBoxNbr := cd['SB_MAIL_BOX_NBR'];
    RLA[i].SbUpLevelMailBoxNbr := cd['UP_LEVEL_MAIL_BOX_NBR'];
    RLA[i].ClNbr := cd['CL_NBR'];
    RLA[i].CoNbr := Null;
    RLA[i].PrNbr := Null;
    for j := 0 to RLA[i].Res.Count-1 do
      if RLA[i].Res[j].VmrCoNbr <> 0 then
      begin
        RLA[i].CoNbr := RLA[i].Res[j].VmrCoNbr;
        RLA[i].PrNbr := RLA[i].Res[j].VmrPrNbr;
        Break;
      end;
    RLA[i].PartNumber := i- OrigRlaLength+ 1;
    RLA[i].PartCount := Length(RLA)- OrigRlaLength;
    RLA[i].BarCode := VarToStr(cd['BARCODE']);
    RLA[i].SbContentNbrs := VarArrayCreate([0, RLA[i].Res.Count-1], varVariant);
    for j := 0 to RLA[i].Res.Count-1 do
      RLA[i].SbContentNbrs[j] := RLA[i].Res[j].Tag;
    RLA[i].Description := cd['DESCRIPTION'];
    RLA[i].cd := cd;
  end;
end;

procedure TVmrRealDelivery.AddGroupedAscii(const RLAscii: TrwReportResults; const Media: TVmrMediaType;
  const cd: TEvClientDataSet);
begin
    EncryptReportResults(RLAscii, Media.ExtraOptions.FieldByName('PRINT_PASSWORD').AsString);
    SetLength(RLA_ASCII, Length(RLA_ASCII)+1);
    RLA_ASCII[High(RLA_ASCII)] := RLAscii;
    Inc(FPackagedEmailCounter, RLAscii.Count);
end;

procedure TVmrRealDelivery.RevertToUnreleased(const Media: TVmrMediaType;
  const cd: TEvClientDataSet);
begin
  cd.Edit;
  cd['PRINTED_TIME'] := Null;
  cd.Post;
end;

procedure TVmrDeliveryMethod.PrepareToRelease;
begin
  SetLength(FDirectEmailParamArray, 0);
  SetLength(FSelfServeArray, 0);
  FEmailErrorList.Clear();
  FSentEmailCounter := 0;
  FSentEmailContentNbrs := '';
end;

procedure TVmrDeliveryMethod.Release(const cd: TEvClientDataSet);
  var c: string;
      vc:integer;

  // This procedure is used for vrtVouchers release type not only TVmrDirectEmailDeliveryMethod type
  // FDirectEmailParamArray is used for vrtVouchers release type not only TVmrDirectEmailDeliveryMethod type
  procedure ProcessDirectEmails;
  var
    i: Integer;
    sEmail: String;
    emailField: TField;
  begin
    if not (vrtVouchers in ReleaseType) then
      exit;
    try
      emailField := ExtraOptions.FindField('EMAIL_ADDRESS');
      if emailField <> nil then
        sEmail := emailField.AsString
      else
        sEmail := '';
      for i := 0 to High(FDirectEmailParamArray) do
      begin
        Inc(FDirectEmailCounter, FDirectEmailParamArray[i].Count);
        EmailDirectly(
             FDirectEmailParamArray[i],
          Options.FieldByName('FROM_ADDRESS').AsString,
             {Options.FieldByName('SMTP_SERVER').AsString,}
             cd.FieldByName('SB_MAIL_BOX_NBR').AsInteger,
             sEmail
             );
      end;
    finally
      for i := 0 to High(FDirectEmailParamArray) do
        FreeAndNil(FDirectEmailParamArray[i]);
      SetLength(FDirectEmailParamArray, 0);
    end;
  end;

  procedure  ProcessSelfServeEmails;
     var i:integer;
         SsText, sFrom: string;
         cdEeSetup :TevClientDataSet;

    procedure EmailSelfServe(const FromAddr, ToAddr: string);
    var
      i:integer;
      Msg: IevMailMessage;
    begin
      SetupEmailerByFromAddress(FromAddr);
      Msg := ctx_EMailer.CreateMessage;

      Msg.Subject := 'Self Serve notification';
      Msg.AddressFrom := FromAddr;
      Msg.AddressTo := ToAddr;

      Msg.Body := ssText + #13#10;

      if ConfNotice.Text = '' then
      begin
         Msg.Body := Msg.Body + 'Confidentiality Notice:'#13#10 +
         'This information may contain confidential and/or privileged material and is'#13#10 +
         'only transmitted for the intended recipient.  Any review, re- transmission,'#13#10 +
         'on version or hard copy, copying, reproduction, circulation, publication,'#13#10 +
         'dissemination, or other use of, or taking of any action, or omission to take'#13#10 +
         'action, and reliance upon this information by persons or entities other than'#13#10 +
         'the intended recipient will be enforced by all legal means, including action'#13#10 +
         'for damages. If you receive this message in error, please contact the sender'#13#10 +
         'and delete the material from any computer, Disk drive, diskette, or other'#13#10 +
         'storage device or media.'
      end
      else
      begin
        for i:= 0 to ConfNotice.Count-1 do
          Msg.Body := Msg.Body +  ConfNotice.Strings[i]+#13#10;
      end;

      try
        ctx_EMailer.SendMail(Msg);
        FSentEmailCounter := FSentEmailCounter + 1;
      except
        on E: Exception do
        begin
        FEmailErrorList.Add('(' + IntToStr(FEmailErrorList.Count+1) +') Email server ' + ctx_EMailer.SmtpServer +' reports: ' + E.Message+#13+
                              'With next parameters:'+#13 +
                              'From Address:'+Msg.AddressFrom +#13 +
                              'To address:' +ToAddr+#13#13);
        end;
      end;
  end;

  begin
    cdEeSetup := TevClientDataSet.Create(nil);
    for i := 0 to High(FSelfServeArray) do
      begin
          if FSelfServeArray[i].SelfServeEEs.Count = 0 then
            continue;
          with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
          begin
            c := 't1.E_MAIL_ADDRESS <> '''' and t1.E_MAIL_ADDRESS is not null and '+
                  BuildInSqlStatement( 't1.EE_NBR', FSelfServeArray[i].SelfServeEEs.CommaText) + ' and ' +
                 't1.SELFSERVE_ENABLED <> ''N'' and ' +
                 't1.SELFSERVE_USERNAME <>'''' and t1.SELFSERVE_USERNAME is not null and '+
                 't1.SELFSERVE_PASSWORD <>'''' and t1.SELFSERVE_PASSWORD is not null ';
            SetMacro('CONDITION', c);
            SetMacro('COLUMNS', 't1.EE_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, t1.E_MAIL_ADDRESS EMAIL');
            SetMacro('TABLE1', 'EE');
            SetMacro('TABLE2', 'CL_PERSON');
            SetMacro('JOINFIELD', 'CL_PERSON');
            cdEeSetup.IndexFieldNames := 'EE_NBR';
            ctx_DataAccess.GetCustomData(cdEeSetup, AsVariant);
          end;
          cdEeSetup.First;
          sFrom := OverrideFromAddr(FSelfServeArray[i].FromAddress);
          while not cdEeSetup.Eof do
          begin
            SsText:=FSelfServeArray[i].SelfServeText;
            EmailSelfServe(sFrom, cdEeSetup['EMAIL']);
            cdEeSetup.Next;
          end;
      end;
  end;


begin
  vc := Length(FDirectEmailParamArray);
  ProcessDirectEmails;
  if ((vc = 0) and (vrtPrinter in ReleaseType)) or
     ((vc > 0) and (vrtVouchers  in ReleaseType))then  // Force Self Serve emailing
    ProcessSelfServeEmails;
  if FEmailErrorList.Count>0 then
    raise ESendEmailError.CreateFmt('Email sent: %d, errors: %d.'#13'Following error(s) occurred while attempt to deliver: '#13'%s',
                              [FSentEmailCounter, FEmailErrorList.Count, FEmailErrorList.Text]);
end;

procedure TVmrDeliveryMethod.PostSentEmailContent(const cd: TEvClientDataSet; const MarkSuccess: boolean);
var
  cNbrs: TStringList;
  cNbr: string;
  DataSetFlags: IevDataSetHolder;
  i: Integer;
begin
  cNbrs := TStringList.Create();
  try
    DataSetFlags := TevDataSetHolder.CreateCloneAndRetrieveData(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
      Format('(SB_MAIL_BOX_NBR=%u) and (OPTION_TAG=''%s'')', [Integer(cd['SB_MAIL_BOX_NBR']), VMR_CONTENT_EMAILED_FLAG]));
    while Length(FSentEmailContentNbrs)>0 do
    begin
      cNbr := Trim(Fetch(FSentEmailContentNbrs,';'));
      if cNbr<>'' then
        if cNbrs.IndexOf(cNbr) = -1 then
          cNbrs.Add(cNbr);
    end;
    if cNbrs.Count>0 then
    begin
      for i:=0 to cNbrs.Count-1 do
      begin
        if DataSetFlags.DataSet.Locate('SB_MAIL_BOX_CONTENT_NBR',cNbrs.Strings[i],[]) then
          DataSetFlags.DataSet.Edit
        else
        begin
          DataSetFlags.DataSet.Append();
          DataSetFlags.DataSet['SB_MAIL_BOX_NBR'] := cd['SB_MAIL_BOX_NBR'];
          DataSetFlags.DataSet['SB_MAIL_BOX_CONTENT_NBR'] := cNbrs.Strings[i];
          DataSetFlags.DataSet['OPTION_TAG'] := VMR_CONTENT_EMAILED_FLAG;
        end;
        DataSetFlags.DataSet['OPTION_STRING_VALUE'] := FormatDateTime('mm/dd/yyyy hh:nn:ss', SysTime);
        DataSetFlags.DataSet.Post();
      end;
      ctx_DataAccess.PostDataSets([DataSetFlags.DataSet]);
    end;
    DataSetFlags := TevDataSetHolder.CreateCloneAndRetrieveData(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
      Format('(SB_MAIL_BOX_NBR=%u) and (OPTION_TAG=''%s'') and (SB_MAIL_BOX_CONTENT_NBR is NULL)', [Integer(cd['SB_MAIL_BOX_NBR']), VMR_CONTENT_EMAILED_FLAG]));
    if DataSetFlags.DataSet.RecordCount>0 then
    begin
      if not MarkSuccess then
        DataSetFlags.DataSet.Delete()
      else
      begin
        DataSetFlags.DataSet.Edit();
        DataSetFlags.DataSet['OPTION_STRING_VALUE'] := FormatDateTime('mm/dd/yyyy hh:nn:ss', SysTime);
      end;
      // ticket 104221 - what to do if error on delivery
      if DataSetFlags.DataSet.State in [dsEdit, dsInsert] then
         DataSetFlags.DataSet.Post();
    end
    else
      if MarkSuccess then
      begin
        DataSetFlags.DataSet.Append();
        DataSetFlags.DataSet['SB_MAIL_BOX_NBR'] := cd['SB_MAIL_BOX_NBR'];
        DataSetFlags.DataSet['OPTION_TAG'] := VMR_CONTENT_EMAILED_FLAG;
        DataSetFlags.DataSet['OPTION_STRING_VALUE'] := FormatDateTime('mm/dd/yyyy hh:nn:ss', SysTime);
        DataSetFlags.DataSet.Post();
      end;
    ctx_DataAccess.PostDataSets([DataSetFlags.DataSet]);
  finally
    cNbrs.Free();
  end;
end;

procedure TVmrDeliveryMethod.FinishRelease(const cd: TEvClientDataSet);
  procedure PostEmailedCounters;
  var
    DataSetIntf: IevDataSetHolder;
  begin
    DataSetIntf := TevDataSetHolder.CreateCloneAndRetrieveData(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
      Format('SB_MAIL_BOX_NBR=%u and (OPTION_TAG=''%s'' or OPTION_TAG=''%s'')', [Integer(cd['SB_MAIL_BOX_NBR']),
      VMR_PACKAGED_EMAILED_COUNTER, VMR_DIRECT_EMAILED_COUNTER]));

    if DataSetIntf.DataSet.Locate('OPTION_TAG', VMR_PACKAGED_EMAILED_COUNTER, []) then
      DataSetIntf.DataSet.Edit
    else
    begin
      DataSetIntf.DataSet.Append;
      DataSetIntf.DataSet['SB_MAIL_BOX_NBR'] := cd['SB_MAIL_BOX_NBR'];
      DataSetIntf.DataSet['OPTION_TAG'] := VMR_PACKAGED_EMAILED_COUNTER;
    end;
    DataSetIntf.DataSet['OPTION_INTEGER_VALUE'] := FPackagedEmailCounter;
    DataSetIntf.DataSet.Post;

    if vrtVouchers in ReleaseType then
    begin
      if DataSetIntf.DataSet.Locate('OPTION_TAG', VMR_DIRECT_EMAILED_COUNTER, []) then
        DataSetIntf.DataSet.Edit
      else
      begin
        DataSetIntf.DataSet.Append;
        DataSetIntf.DataSet['SB_MAIL_BOX_NBR'] := cd['SB_MAIL_BOX_NBR'];
        DataSetIntf.DataSet['OPTION_TAG'] := VMR_DIRECT_EMAILED_COUNTER;
      end;
      DataSetIntf.DataSet['OPTION_INTEGER_VALUE'] := FDirectEmailCounter;
      DataSetIntf.DataSet.Post;
    end;

    ctx_DataAccess.PostDataSets([DataSetIntf.DataSet]);
  end;

begin
  PostEmailedCounters;
  PostSentEmailContent(cd, true);
end;

procedure TVmrDeliveryMethod.RevertToUnreleased(const Media: TVmrMediaType;
  const cd: TEvClientDataSet);
var
  DataSetIntf: IevDataSetHolder;
begin
  inherited;
  DataSetIntf := TevDataSetHolder.CreateCloneAndRetrieveData(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
    Format('SB_MAIL_BOX_NBR=%u and (OPTION_TAG=''%s'' or OPTION_TAG=''%s'')', [Integer(cd['SB_MAIL_BOX_NBR']),
    VMR_PACKAGED_EMAILED_COUNTER, VMR_DIRECT_EMAILED_COUNTER]));
  while DataSetIntf.DataSet.RecordCount > 0 do
    DataSetIntf.DataSet.Delete;
  ctx_DataAccess.PostDataSets([DataSetIntf.DataSet]);
  cd.Edit;
  cd['PRINTED_TIME'] := Null;
  cd.Post;
end;

procedure TVmrDeliveryMethod.AfterCreated;
var
 i:integer;
 HasText:boolean;
 S: IisStream;
begin
   inherited;
   ConfNotice :=TStringList.Create;
   FEmailErrorList := TStringList.Create;
   DM_SERVICE_BUREAU.SB.DataRequired('');
   DM_SERVICE_BUREAU.SB.Open;

   ConfNotice.Clear();
   if not DM_SERVICE_BUREAU.SB.FieldByName('VMR_CONFIDENCIAL_NOTES').IsNull then
   begin
    S := TevClientDataSet(DM_SERVICE_BUREAU.SB).GetBlobData('VMR_CONFIDENCIAL_NOTES');
    if Assigned(S) then
      ConfNotice.Text := S.AsString;
   end;

   HasText := false;
   for i := 0 to ConfNotice.Count - 1 do
   begin
       if Trim(ConfNotice.Strings[i]) <>'' then
       begin
          HasText := true;
          break;
       end;
   end;

   if not HasText then ConfNotice.Clear;
end;

procedure TVmrDeliveryMethod.StoreReportResults(const R: TrwReportResults;StoringRequired:boolean);
var
  i,j: Integer;
  sPath,h: string;
  AFile: IEvDualStream;
  VMRFileName:string;
  ext:string;
begin
  try

    for i := 0 to R.Count-1 do
    begin
      if R[i].Tag ='' then // Cover letter
        continue;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired('OPTION_TAG='''+VmrContentASCII_FileName+''' and SB_MAIL_BOX_CONTENT_NBR='+R[i].Tag);
      DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired('SB_MAIL_BOX_CONTENT_NBR='+R[i].Tag);
      VMRFileName:= Trim(DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.FieldByName('FILE_NAME').AsString);
      sPath :=  Trim(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.OPTION_STRING_VALUE.Value);
      if (length(sPath)=0) then
      begin
        if StoringRequired then
        begin
          h := NormalizeFileName(R[i].ReportName);
          if ISXLSFormat(R[i].Data) then
            sPath := 'C:\ASCIIResults\VMR\' + h + ' ' + FormatDateTime('ddmmyy_hhmmss', Now) + '.xls'
          else
            sPath := 'C:\ASCIIResults\VMR\' + h + ' ' + FormatDateTime('ddmmyy_hhmmss', Now) + '.txt'; // default file name
        end else
           continue;
      end;

      if FileExists(sPath) then
      begin
        ext := Copy(sPath,Pos('.',sPath),Length(sPath));
        SetLength(sPath,Pos('.',sPath)-1);
        j := 2;
        while FileExists(sPath + '('+ IntToStr(j)+ ')'+ ext) do
          Inc(j);
        sPath := sPath + '('+ IntToStr(j)+ ')' + ext;
      end;
      try
        AFile := ctx_VMRRemote.RetrieveFile(VMRFileName);
        try
          ForceDirectories(ExtractFilePath(sPath));
          AFile.SaveToFile(sPath);
        except
          sPath := 'C:\ASCIIResults\VMR\'+ExtractFileName(sPath);
          ForceDirectories('C:\ASCIIResults\VMR\');
          AFile.SaveToFile(sPath);
        end;
      except
       on E: Exception do
       begin
          Raise EevException.Create('Can not save data to file: '+ sPath);
       end;
      end;
    end;
  finally

  end;

end;



{ TVmrPaperMedia }

procedure TVmrPaperMedia.PrepareExtraOptionFields(var Prefix: string;
  var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited;
  f.Add('PRINT_PASSWORD', ftString, 20);
  f.Add('CONVERT_TO_PDF', ftString, 1);
end;


destructor TVmrDeliveryMethod.Destroy;
begin
  FreeAndNil(ConfNotice);
  FreeAndNil(FEmailErrorList);
  inherited;
end;

function TVmrRealDelivery.CalcWeight: double;
var
  i,j,k: Integer;
  Job: TVmrPrintJob;
begin
  Result := 0;
  for i:= 0 to High(RLA) do
  begin
    Job := RLA[i];
    for j := 0 to Job.Res.Count-1 do
    begin
      if Job.Res[j].PagesInfo.Count > 0 then
        for k := 0 to PrInfo[Job.PrinterIndex].BinCount - 1 do
        begin
          if PrInfo[Job.PrinterIndex].Bins[k].Name = Job.Res[j].Tray then
          begin
            Result := Result+
                      PrInfo[Job.PrinterIndex].Bins[k].PaperWeight *
                          (
                              Job.Res[j].PagesInfo[0].DuplexPageCount/2+
                              Job.Res[j].PagesInfo[0].SimplexPageCount
                          )/ 500;
            Break;
          end;
        end;
    end;
  end;
end;

function TVmrDeliveryMethod.IsIncludeDescInEmail(SBBoxNbr:Integer):boolean;
var
  qsbmailbox : IevQuery;
begin
  qsbmailbox := TevQuery.Create('SELECT OPTION_STRING_VALUE FROM SB_MAIL_BOX_OPTION a WHERE a.SB_MAIL_BOX_NBR = :SBNBR ' +
                                'AND a.OPTION_TAG='''+ VmrIncludeDescriptioninEmails +''' AND {AsOfNow<a>} ');
  qsbmailbox.Params.AddValue('SBNBR', SBBoxNbr);
  qsbmailbox.Execute;

  Result := ( qsbmailbox.Result.RecordCount > 0 ) and
            ( Trim(qsbmailbox.Result.FieldByName('OPTION_STRING_VALUE').asString)= GROUP_BOX_YES );
end;

procedure TVmrDeliveryMethod.SendNotificationEmail(FromAddr,ToAddr, WebNotes: string; CheckDate,TaxEndingDate :TDateTime);
var
  i :  integer;
  Msg: IevMailMessage;
begin
  if not (vrtPrinter in ReleaseType) then
     exit;

  FromAddr := OverrideFromAddr(FromAddr);
  if (Trim(FromAddr) = '') or (Trim(ToAddr) = '') then
     exit;

  Msg := ctx_EMailer.CreateMessage;
  Msg.AddressTo := ToAddr;
  Msg.AddressFrom := FromAddr;
  SetupEmailerByFromAddress(Msg.AddressFrom);

  if IsIncludeDescInEmail(BoxNbr) then
     Msg.Subject := Self.Description + ' Payroll Reports/Returns notification'
  else
     Msg.Subject := 'Payroll Reports/Returns notification';

  if CheckDate > 0 then
     Msg.Body := ' Your payroll reports for the check date ' + FormatDateTime('mm/dd/yyyy',CheckDate);

  if TaxEndingDate > 0 then
     Msg.Body := Msg.Body + #13#10 + ' Your tax returns for period ending date ' + FormatDateTime('mm/dd/yyyy',TaxEndingDate);

  Msg.Body := Msg.Body + ' are available to be viewed online';

  if (Trim(WebNotes) <> '') and
     ((Pos('/', WebNotes) > 0) or (Pos('www', lowercase(WebNotes)) > 0))
  then Msg.Body := Msg.Body +' at ' + WebNotes + #13#10
  else Msg.Body := Msg.Body +'. ' + #13#10 + WebNotes + #13#10;


  Msg.Body := Msg.Body + #13#10;
  if ConfNotice.Text = '' then
     Msg.Body := Msg.Body +
     'Confidentiality Notice:'#13#10 +
     'This information may contain confidential and/or privileged material and is'#13#10 +
     'only transmitted for the intended recipient.  Any review, re- transmission,'#13#10 +
     'on version or hard copy, copying, reproduction, circulation, publication,'#13#10 +
     'dissemination, or other use of, or taking of any action, or omission to take'#13#10 +
     'action, and reliance upon this information by persons or entities other than'#13#10 +
     'the intended recipient will be enforced by all legal means, including action'#13#10 +
     'for damages. If you receive this message in error, please contact the sender'#13#10 +
     'and delete the material from any computer, Disk drive, diskette, or other'#13#10 +
     'storage device or media.'
  else
     for i:= 0 to ConfNotice.Count-1 do
        Msg.Body := Msg.Body +  ConfNotice.Strings[i]+#13#10;

  try
    ctx_EMailer.SendMail(Msg);
  except
    on E: Exception do
    begin
      E.Message := 'Email server: '+ctx_EMailer.SmtpServer+' returned this error on attempt to deliver'#13+
                    E.Message+#13+#13+
                    'With next parameters:'+#13 +
                    'From Address:'+FromAddr +#13 +
                    'To Address:' +ToAddr;
      raise;
    end;
  end;
end;

initialization
  //SetLength(FRegisteredClasses, 0);
  RegisterVmrClass(TVmrSeparatedPaperMediaType);
  RegisterVmrClass(TVmrGroupedPaperMediaType);
  RegisterVmrClass(TVmrFedexService);
  RegisterVmrClass(TVmrFedexPrepaid);
  RegisterVmrClass(TVmrUpsService);
//  RegisterVmrClass(TVmrUpsPrepaid);
  RegisterVmrClass(TVmrUpsGround);
  RegisterVmrClass(TVmrUpsThreeDaySelect);
  RegisterVmrClass(TVmrUpsSecondDayAir);
  RegisterVmrClass(TVmrUpsSecondDayAirAM);
  RegisterVmrClass(TVmrUpsNextDayAirSaver);
  RegisterVmrClass(TVmrUpsNextDayAir);

  RegisterVmrClass(TVmrCourier2Service1);
  RegisterVmrClass(TVmrCourier2Service2);
  RegisterVmrClass(TVmrCourier2Service3);
  RegisterVmrClass(TVmrCourier2DeliveryMethod);
  RegisterVmrClass(TVmrDhlService);
  RegisterVmrClass(TVmrDhlGround);
  RegisterVmrClass(TVmrDhlExpress1030am);
  RegisterVmrClass(TVmrDhlExpress1200pm);
  //RegisterVmrClass(TVmrDhlExpressSaturday);
  RegisterVmrClass(TVmrDhlNextAfternoon);
  RegisterVmrClass(TVmrDhlSecondDay5pm);


end.


