
{******************************************************************************************************}
{                                                                                                      }
{                                           XML Data Binding                                           }
{                                                                                                      }
{         Generated on: 11/3/2004 12:37:13 PM                                                          }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\VoidRequest.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\VoidRequest.xdb   }
{                                                                                                      }
{******************************************************************************************************}

unit SDhlVoidRequest;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLShipmentType = interface;
  IXMLShippingCredentialsType = interface;
  IXMLShipmentDetailType = interface;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{FE7FD946-7DA8-42A5-A0BB-F746224342B4}']
    { Property Accessors }
    function Get_Action: string;
    function Get_Version: string;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_TransactionTrace: string;
    procedure Set_Action(Value: string);
    procedure Set_Version(Value: string);
    procedure Set_TransactionTrace(Value: string);
    { Methods & Properties }
    property Action: string read Get_Action write Set_Action;
    property Version: string read Get_Version write Set_Version;
    property ShippingCredentials: IXMLShippingCredentialsType read Get_ShippingCredentials;
    property ShipmentDetail: IXMLShipmentDetailType read Get_ShipmentDetail;
    property TransactionTrace: string read Get_TransactionTrace write Set_TransactionTrace;
  end;

{ IXMLShippingCredentialsType }

  IXMLShippingCredentialsType = interface(IXMLNode)
    ['{31152540-54B0-4119-AE60-78175A83F0AE}']
    { Property Accessors }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
    procedure Set_ShippingKey(Value: string);
    procedure Set_AccountNbr(Value: Integer);
    { Methods & Properties }
    property ShippingKey: string read Get_ShippingKey write Set_ShippingKey;
    property AccountNbr: Integer read Get_AccountNbr write Set_AccountNbr;
  end;

{ IXMLShipmentDetailType }

  IXMLShipmentDetailType = interface(IXMLNode)
    ['{2183C30B-8515-40AB-9B8D-37125C15D893}']
    { Property Accessors }
    function Get_AirbillNbr: string;
    procedure Set_AirbillNbr(Value: string);
    { Methods & Properties }
    property AirbillNbr: string read Get_AirbillNbr write Set_AirbillNbr;
  end;

{ Forward Decls }

  TXMLShipmentType = class;
  TXMLShippingCredentialsType = class;
  TXMLShipmentDetailType = class;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  protected
    { IXMLShipmentType }
    function Get_Action: string;
    function Get_Version: string;
    function Get_ShippingCredentials: IXMLShippingCredentialsType;
    function Get_ShipmentDetail: IXMLShipmentDetailType;
    function Get_TransactionTrace: string;
    procedure Set_Action(Value: string);
    procedure Set_Version(Value: string);
    procedure Set_TransactionTrace(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShippingCredentialsType }

  TXMLShippingCredentialsType = class(TXMLNode, IXMLShippingCredentialsType)
  protected
    { IXMLShippingCredentialsType }
    function Get_ShippingKey: string;
    function Get_AccountNbr: Integer;
    procedure Set_ShippingKey(Value: string);
    procedure Set_AccountNbr(Value: Integer);
  end;

{ TXMLShipmentDetailType }

  TXMLShipmentDetailType = class(TXMLNode, IXMLShipmentDetailType)
  protected
    { IXMLShipmentDetailType }
    function Get_AirbillNbr: string;
    procedure Set_AirbillNbr(Value: string);
  end;

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
function LoadShipment(const FileName: WideString): IXMLShipmentType;
function NewShipment: IXMLShipmentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetShipment(Doc: IXMLDocument): IXMLShipmentType;
begin
  Result := Doc.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function LoadShipment(const FileName: WideString): IXMLShipmentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

function NewShipment: IXMLShipmentType;
begin
  Result := NewXMLDocument.GetDocBinding('Shipment', TXMLShipmentType, TargetNamespace) as IXMLShipmentType;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('ShippingCredentials', TXMLShippingCredentialsType);
  RegisterChildNode('ShipmentDetail', TXMLShipmentDetailType);
  inherited;
end;

function TXMLShipmentType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLShipmentType.Set_Action(Value: string);
begin
  SetAttribute('action', Value);
end;

function TXMLShipmentType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

procedure TXMLShipmentType.Set_Version(Value: string);
begin
  SetAttribute('version', Value);
end;

function TXMLShipmentType.Get_ShippingCredentials: IXMLShippingCredentialsType;
begin
  Result := ChildNodes['ShippingCredentials'] as IXMLShippingCredentialsType;
end;

function TXMLShipmentType.Get_ShipmentDetail: IXMLShipmentDetailType;
begin
  Result := ChildNodes['ShipmentDetail'] as IXMLShipmentDetailType;
end;

function TXMLShipmentType.Get_TransactionTrace: string;
begin
  Result := ChildNodes['TransactionTrace'].Text;
end;

procedure TXMLShipmentType.Set_TransactionTrace(Value: string);
begin
  ChildNodes['TransactionTrace'].NodeValue := Value;
end;

{ TXMLShippingCredentialsType }

function TXMLShippingCredentialsType.Get_ShippingKey: string;
begin
  Result := ChildNodes['ShippingKey'].Text;
end;

procedure TXMLShippingCredentialsType.Set_ShippingKey(Value: string);
begin
  ChildNodes['ShippingKey'].NodeValue := Value;
end;

function TXMLShippingCredentialsType.Get_AccountNbr: Integer;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

procedure TXMLShippingCredentialsType.Set_AccountNbr(Value: Integer);
begin
  ChildNodes['AccountNbr'].NodeValue := Value;
end;

{ TXMLShipmentDetailType }

function TXMLShipmentDetailType.Get_AirbillNbr: string;
begin
  Result := ChildNodes['AirbillNbr'].NodeValue;
end;

procedure TXMLShipmentDetailType.Set_AirbillNbr(Value: string);
begin
  ChildNodes['AirbillNbr'].NodeValue := Value;
end;

end.