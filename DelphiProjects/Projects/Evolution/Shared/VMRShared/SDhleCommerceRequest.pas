
{***********************************************************************************************************}
{                                                                                                           }
{                                             XML Data Binding                                              }
{                                                                                                           }
{         Generated on: 11/2/2004 4:15:35 PM                                                                }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\eCommerceRequest.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\eCommerceRequest.xdb   }
{                                                                                                           }
{***********************************************************************************************************}

unit SDhleCommerceRequest;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLECommerceType = interface;
  IXMLRequestorType = interface;

{ IXMLECommerceType }

  IXMLECommerceType = interface(IXMLNode)
    ['{FDB6EAA7-AC4E-4E8A-B1D1-D23FCDE206AD}']
    { Property Accessors }
    function Get_Version: string;
    function Get_Action: string;
    function Get_Requestor: IXMLRequestorType;
    procedure Set_Version(Value: string);
    procedure Set_Action(Value: string);
    { Methods & Properties }
    property Version: string read Get_Version write Set_Version;
    property Action: string read Get_Action write Set_Action;
    property Requestor: IXMLRequestorType read Get_Requestor;
  end;

{ IXMLRequestorType }

  IXMLRequestorType = interface(IXMLNode)
    ['{82F0E5ED-FF72-416D-9624-8A0388D4AC23}']
    { Property Accessors }
    function Get_ID: string;
    function Get_Password: string;
    procedure Set_ID(Value: string);
    procedure Set_Password(Value: string);
    { Methods & Properties }
    property ID: string read Get_ID write Set_ID;
    property Password: string read Get_Password write Set_Password;
  end;

{ Forward Decls }

  TXMLECommerceType = class;
  TXMLRequestorType = class;

{ TXMLECommerceType }

  TXMLECommerceType = class(TXMLNode, IXMLECommerceType)
  protected
    { IXMLECommerceType }
    function Get_Version: string;
    function Get_Action: string;
    function Get_Requestor: IXMLRequestorType;
    procedure Set_Version(Value: string);
    procedure Set_Action(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRequestorType }

  TXMLRequestorType = class(TXMLNode, IXMLRequestorType)
  protected
    { IXMLRequestorType }
    function Get_ID: string;
    function Get_Password: string;
    procedure Set_ID(Value: string);
    procedure Set_Password(Value: string);
  end;

{ Global Functions }

function GeteCommerce(Doc: IXMLDocument): IXMLECommerceType;
function LoadeCommerce(const FileName: WideString): IXMLECommerceType;
function NeweCommerce: IXMLECommerceType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GeteCommerce(Doc: IXMLDocument): IXMLECommerceType;
begin
  Result := Doc.GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

function LoadeCommerce(const FileName: WideString): IXMLECommerceType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

function NeweCommerce: IXMLECommerceType;
begin
  Result := NewXMLDocument.GetDocBinding('eCommerce', TXMLECommerceType, TargetNamespace) as IXMLECommerceType;
end;

{ TXMLECommerceType }

procedure TXMLECommerceType.AfterConstruction;
begin
  RegisterChildNode('Requestor', TXMLRequestorType);
  inherited;
end;

function TXMLECommerceType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

procedure TXMLECommerceType.Set_Version(Value: string);
begin
  SetAttribute('version', Value);
end;

function TXMLECommerceType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLECommerceType.Set_Action(Value: string);
begin
  SetAttribute('action', Value);
end;

function TXMLECommerceType.Get_Requestor: IXMLRequestorType;
begin
  Result := ChildNodes['Requestor'] as IXMLRequestorType;
end;

{ TXMLRequestorType }

function TXMLRequestorType.Get_ID: string;
begin
  Result := ChildNodes['ID'].Text;
end;

procedure TXMLRequestorType.Set_ID(Value: string);
begin
  ChildNodes['ID'].NodeValue := Value;
end;

function TXMLRequestorType.Get_Password: string;
begin
  Result := ChildNodes['Password'].Text;
end;

procedure TXMLRequestorType.Set_Password(Value: string);
begin
  ChildNodes['Password'].NodeValue := Value;
end;

end.