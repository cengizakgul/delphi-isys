
{****************************************************************************************************************}
{                                                                                                                }
{                                                XML Data Binding                                                }
{                                                                                                                }
{         Generated on: 6/8/2009 7:49:12 PM                                                                      }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipconfirmresp1.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipconfirmresp1.xdb   }
{                                                                                                                }
{****************************************************************************************************************}

unit sUpsShipConfirmResp;


interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUpsShipmentConfirmResponseType = interface;
  IXMLResponseType = interface;
  IXMLTransactionReferenceType = interface;
  IXMLErrorType = interface;
  IXMLShipmentChargesType = interface;
  IXMLTransportationChargesType = interface;
  IXMLServiceOptionsChargesType = interface;
  IXMLTotalChargesType = interface;
  IXMLBillingWeightType = interface;
  IXMLUnitOfMeasurementType = interface;

{ IXMLUpsShipmentConfirmResponseType }

  IXMLUpsShipmentConfirmResponseType = interface(IXMLNode)
    ['{32440C45-D2A4-413A-8867-37A09C933711}']
    { Property Accessors }
    function Get_Response: IXMLResponseType;
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_BillingWeight: IXMLBillingWeightType;
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_ShipmentDigest: WideString;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
    procedure Set_ShipmentDigest(Value: WideString);
    { Methods & Properties }
    property Response: IXMLResponseType read Get_Response;
    property ShipmentCharges: IXMLShipmentChargesType read Get_ShipmentCharges;
    property BillingWeight: IXMLBillingWeightType read Get_BillingWeight;
    property ShipmentIdentificationNumber: WideString read Get_ShipmentIdentificationNumber write Set_ShipmentIdentificationNumber;
    property ShipmentDigest: WideString read Get_ShipmentDigest write Set_ShipmentDigest;
  end;

{ IXMLResponseType }

  IXMLResponseType = interface(IXMLNode)
    ['{F42CA319-EE5D-4AEE-A676-9F2FCF6C73D4}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    function Get_Error: IXMLErrorType;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property ResponseStatusCode: Integer read Get_ResponseStatusCode write Set_ResponseStatusCode;
    property ResponseStatusDescription: WideString read Get_ResponseStatusDescription write Set_ResponseStatusDescription;
    property Error: IXMLErrorType read Get_Error;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{A98CFB2F-28B0-495B-9AEA-573BC9BCD959}']
    { Property Accessors }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property CustomerContext: WideString read Get_CustomerContext write Set_CustomerContext;
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ IXMLErrorType }

  IXMLErrorType = interface(IXMLNode)
    ['{7E6E278F-7BED-4C60-B7A1-3985895328C1}']
    { Property Accessors }
    function Get_ErrorSeverity: WideString;
    function Get_ErrorCode: Integer;
    function Get_ErrorDescription: WideString;
    procedure Set_ErrorSeverity(Value: WideString);
    procedure Set_ErrorCode(Value: Integer);
    procedure Set_ErrorDescription(Value: WideString);
    { Methods & Properties }
    property ErrorSeverity: WideString read Get_ErrorSeverity write Set_ErrorSeverity;
    property ErrorCode: Integer read Get_ErrorCode write Set_ErrorCode;
    property ErrorDescription: WideString read Get_ErrorDescription write Set_ErrorDescription;
  end;

{ IXMLShipmentChargesType }

  IXMLShipmentChargesType = interface(IXMLNode)
    ['{AF671AD8-B12A-45E4-9287-FF6F1273C59A}']
    { Property Accessors }
    function Get_TransportationCharges: IXMLTransportationChargesType;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_TotalCharges: IXMLTotalChargesType;
    { Methods & Properties }
    property TransportationCharges: IXMLTransportationChargesType read Get_TransportationCharges;
    property ServiceOptionsCharges: IXMLServiceOptionsChargesType read Get_ServiceOptionsCharges;
    property TotalCharges: IXMLTotalChargesType read Get_TotalCharges;
  end;

{ IXMLTransportationChargesType }

  IXMLTransportationChargesType = interface(IXMLNode)
    ['{60AB37CC-6493-46B1-A0D6-A888B1397E81}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLServiceOptionsChargesType }

  IXMLServiceOptionsChargesType = interface(IXMLNode)
    ['{8EFF4D02-D22B-416C-8A0B-E8EDD3B005F9}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLTotalChargesType }

  IXMLTotalChargesType = interface(IXMLNode)
    ['{58879833-9E9D-41BA-9DCC-9E6F2C4B0C4D}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLBillingWeightType }

  IXMLBillingWeightType = interface(IXMLNode)
    ['{21DE3CBF-CA27-4013-8905-3996C99EDFEB}']
    { Property Accessors }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: WideString;
    procedure Set_Weight(Value: WideString);
    { Methods & Properties }
    property UnitOfMeasurement: IXMLUnitOfMeasurementType read Get_UnitOfMeasurement;
    property Weight: WideString read Get_Weight write Set_Weight;
  end;

{ IXMLUnitOfMeasurementType }

  IXMLUnitOfMeasurementType = interface(IXMLNode)
    ['{7F978BCB-D0C7-4AAD-9831-FACE99167A50}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ Forward Decls }

  TXMLShipmentConfirmResponseType = class;
  TXMLResponseType = class;
  TXMLTransactionReferenceType = class;
  TXMLErrorType = class;
  TXMLShipmentChargesType = class;
  TXMLTransportationChargesType = class;
  TXMLServiceOptionsChargesType = class;
  TXMLTotalChargesType = class;
  TXMLBillingWeightType = class;
  TXMLUnitOfMeasurementType = class;

{ TXMLShipmentConfirmResponseType }

  TXMLShipmentConfirmResponseType = class(TXMLNode, IXMLUpsShipmentConfirmResponseType)
  protected
    { IXMLUpsShipmentConfirmResponseType }
    function Get_Response: IXMLResponseType;
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_BillingWeight: IXMLBillingWeightType;
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_ShipmentDigest: WideString;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
    procedure Set_ShipmentDigest(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLResponseType }

  TXMLResponseType = class(TXMLNode, IXMLResponseType)
  protected
    { IXMLResponseType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    function Get_Error: IXMLErrorType;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
  end;

{ TXMLErrorType }

  TXMLErrorType = class(TXMLNode, IXMLErrorType)
  protected
    { IXMLErrorType }
    function Get_ErrorSeverity: WideString;
    function Get_ErrorCode: Integer;
    function Get_ErrorDescription: WideString;
    procedure Set_ErrorSeverity(Value: WideString);
    procedure Set_ErrorCode(Value: Integer);
    procedure Set_ErrorDescription(Value: WideString);
  end;

{ TXMLShipmentChargesType }

  TXMLShipmentChargesType = class(TXMLNode, IXMLShipmentChargesType)
  protected
    { IXMLShipmentChargesType }
    function Get_TransportationCharges: IXMLTransportationChargesType;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_TotalCharges: IXMLTotalChargesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransportationChargesType }

  TXMLTransportationChargesType = class(TXMLNode, IXMLTransportationChargesType)
  protected
    { IXMLTransportationChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLServiceOptionsChargesType }

  TXMLServiceOptionsChargesType = class(TXMLNode, IXMLServiceOptionsChargesType)
  protected
    { IXMLServiceOptionsChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLTotalChargesType }

  TXMLTotalChargesType = class(TXMLNode, IXMLTotalChargesType)
  protected
    { IXMLTotalChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLBillingWeightType }

  TXMLBillingWeightType = class(TXMLNode, IXMLBillingWeightType)
  protected
    { IXMLBillingWeightType }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: WideString;
    procedure Set_Weight(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUnitOfMeasurementType }

  TXMLUnitOfMeasurementType = class(TXMLNode, IXMLUnitOfMeasurementType)
  protected
    { IXMLUnitOfMeasurementType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;

{ Global Functions }

//function GetShipmentConfirmResponse(Doc: IXMLDocument): IXMLShipmentConfirmResponseType;
//function LoadShipmentConfirmResponse(const FileName: WideString): IXMLShipmentConfirmResponseType;
//function NewShipmentConfirmResponse: IXMLShipmentConfirmResponseType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

{function GetShipmentConfirmResponse(Doc: IXMLDocument): IXMLUpsShipmentConfirmResponseType;
begin
  Result := Doc.GetDocBinding('ShipmentConfirmResponse', TXMLUpsShipmentConfirmResponseType, TargetNamespace) as IXMLUpsShipmentConfirmResponseType;
end;

function LoadShipmentConfirmResponse(const FileName: WideString): IXMLUpsShipmentConfirmResponseType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('ShipmentConfirmResponse', TXMLUpsShipmentConfirmResponseType, TargetNamespace) as IXMLShipmentConfirmResponseType;
end;

function NewShipmentConfirmResponse: IXMLShipmentConfirmResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentConfirmResponse', TXMLShipmentConfirmResponseType, TargetNamespace) as IXMLShipmentConfirmResponseType;
end;
}
{ TXMLUpsShipmentConfirmResponseType }

procedure TXMLShipmentConfirmResponseType.AfterConstruction;
begin
  RegisterChildNode('Response', TXMLResponseType);
  RegisterChildNode('ShipmentCharges', TXMLShipmentChargesType);
  RegisterChildNode('BillingWeight', TXMLBillingWeightType);
  inherited;
end;

function TXMLShipmentConfirmResponseType.Get_Response: IXMLResponseType;
begin
  Result := ChildNodes['Response'] as IXMLResponseType;
end;

function TXMLShipmentConfirmResponseType.Get_ShipmentCharges: IXMLShipmentChargesType;
begin
  Result := ChildNodes['ShipmentCharges'] as IXMLShipmentChargesType;
end;

function TXMLShipmentConfirmResponseType.Get_BillingWeight: IXMLBillingWeightType;
begin
  Result := ChildNodes['BillingWeight'] as IXMLBillingWeightType;
end;

function TXMLShipmentConfirmResponseType.Get_ShipmentIdentificationNumber: WideString;
begin
  Result := ChildNodes['ShipmentIdentificationNumber'].Text;
end;

procedure TXMLShipmentConfirmResponseType.Set_ShipmentIdentificationNumber(Value: WideString);
begin
  ChildNodes['ShipmentIdentificationNumber'].NodeValue := Value;
end;

function TXMLShipmentConfirmResponseType.Get_ShipmentDigest: WideString;
begin
  Result := ChildNodes['ShipmentDigest'].Text;
end;

procedure TXMLShipmentConfirmResponseType.Set_ShipmentDigest(Value: WideString);
begin
  ChildNodes['ShipmentDigest'].NodeValue := Value;
end;

{ TXMLResponseType }

procedure TXMLResponseType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  RegisterChildNode('Error', TXMLErrorType);
  inherited;
end;

function TXMLResponseType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLResponseType.Get_ResponseStatusCode: Integer;
begin
  Result := ChildNodes['ResponseStatusCode'].NodeValue;
end;

procedure TXMLResponseType.Set_ResponseStatusCode(Value: Integer);
begin
  ChildNodes['ResponseStatusCode'].NodeValue := Value;
end;

function TXMLResponseType.Get_ResponseStatusDescription: WideString;
begin
  Result := ChildNodes['ResponseStatusDescription'].Text;
end;

procedure TXMLResponseType.Set_ResponseStatusDescription(Value: WideString);
begin
  ChildNodes['ResponseStatusDescription'].NodeValue := Value;
end;

function TXMLResponseType.Get_Error: IXMLErrorType;
begin
  Result := ChildNodes['Error'] as IXMLErrorType;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_CustomerContext: WideString;
begin
  Result := ChildNodes['CustomerContext'].Text;
end;

procedure TXMLTransactionReferenceType.Set_CustomerContext(Value: WideString);
begin
  ChildNodes['CustomerContext'].NodeValue := Value;
end;

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

{ TXMLErrorType }

function TXMLErrorType.Get_ErrorSeverity: WideString;
begin
  Result := ChildNodes['ErrorSeverity'].Text;
end;

procedure TXMLErrorType.Set_ErrorSeverity(Value: WideString);
begin
  ChildNodes['ErrorSeverity'].NodeValue := Value;
end;

function TXMLErrorType.Get_ErrorCode: Integer;
begin
  Result := ChildNodes['ErrorCode'].NodeValue;
end;

procedure TXMLErrorType.Set_ErrorCode(Value: Integer);
begin
  ChildNodes['ErrorCode'].NodeValue := Value;
end;

function TXMLErrorType.Get_ErrorDescription: WideString;
begin
  Result := ChildNodes['ErrorDescription'].Text;
end;

procedure TXMLErrorType.Set_ErrorDescription(Value: WideString);
begin
  ChildNodes['ErrorDescription'].NodeValue := Value;
end;

{ TXMLShipmentChargesType }

procedure TXMLShipmentChargesType.AfterConstruction;
begin
  RegisterChildNode('TransportationCharges', TXMLTransportationChargesType);
  RegisterChildNode('ServiceOptionsCharges', TXMLServiceOptionsChargesType);
  RegisterChildNode('TotalCharges', TXMLTotalChargesType);
  inherited;
end;

function TXMLShipmentChargesType.Get_TransportationCharges: IXMLTransportationChargesType;
begin
  Result := ChildNodes['TransportationCharges'] as IXMLTransportationChargesType;
end;

function TXMLShipmentChargesType.Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
begin
  Result := ChildNodes['ServiceOptionsCharges'] as IXMLServiceOptionsChargesType;
end;

function TXMLShipmentChargesType.Get_TotalCharges: IXMLTotalChargesType;
begin
  Result := ChildNodes['TotalCharges'] as IXMLTotalChargesType;
end;

{ TXMLTransportationChargesType }

function TXMLTransportationChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLTransportationChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLTransportationChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLTransportationChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLServiceOptionsChargesType }

function TXMLServiceOptionsChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLServiceOptionsChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLServiceOptionsChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLServiceOptionsChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLTotalChargesType }

function TXMLTotalChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLTotalChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLTotalChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLTotalChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLBillingWeightType }

procedure TXMLBillingWeightType.AfterConstruction;
begin
  RegisterChildNode('UnitOfMeasurement', TXMLUnitOfMeasurementType);
  inherited;
end;

function TXMLBillingWeightType.Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
begin
  Result := ChildNodes['UnitOfMeasurement'] as IXMLUnitOfMeasurementType;
end;

function TXMLBillingWeightType.Get_Weight: WideString;
begin
  Result := ChildNodes['Weight'].Text;
end;

procedure TXMLBillingWeightType.Set_Weight(Value: WideString);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

{ TXMLUnitOfMeasurementType }

function TXMLUnitOfMeasurementType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLUnitOfMeasurementType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

end.
