
{**************************************************************************************************************}
{                                                                                                              }
{                                               XML Data Binding                                               }
{                                                                                                              }
{         Generated on: 6/8/2009 7:51:27 PM                                                                    }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipacceptreq1.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipacceptreq1.xdb   }
{                                                                                                              }
{**************************************************************************************************************}

unit sUpsShipAcceptReq;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUpsShipmentAcceptRequestType = interface;
  IXMLRequestType = interface;
  IXMLTransactionReferenceType = interface;

{ IXMLUpsShipmentAcceptRequestType }

  IXMLUpsShipmentAcceptRequestType = interface(IXMLNode)
    ['{BC84644A-05CD-4391-9FF3-3E9BBCF08874}']
    { Property Accessors }
    function Get_Request: IXMLRequestType;
    function Get_ShipmentDigest: WideString;
    procedure Set_ShipmentDigest(Value: WideString);
    { Methods & Properties }
    property Request: IXMLRequestType read Get_Request;
    property ShipmentDigest: WideString read Get_ShipmentDigest write Set_ShipmentDigest;
  end;

{ IXMLRequestType }

  IXMLRequestType = interface(IXMLNode)
    ['{FC02FCB8-4E55-465A-A1EC-5400E2CD2705}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: string;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: string);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property RequestAction: WideString read Get_RequestAction write Set_RequestAction;
    property RequestOption: string read Get_RequestOption write Set_RequestOption;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{1621A811-9FD4-4B1C-80E7-B9F213569A62}']
    { Property Accessors }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property CustomerContext: WideString read Get_CustomerContext write Set_CustomerContext;
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ Forward Decls }

  TXMLShipmentAcceptRequestType = class;
  TXMLRequestType = class;
  TXMLTransactionReferenceType = class;

{ TXMLShipmentAcceptRequestType }

  TXMLShipmentAcceptRequestType = class(TXMLNode, IXMLUpsShipmentAcceptRequestType)
  protected
    { IXMLUpsShipmentAcceptRequestType }
    function Get_Request: IXMLRequestType;
    function Get_ShipmentDigest: WideString;
    procedure Set_ShipmentDigest(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRequestType }

  TXMLRequestType = class(TXMLNode, IXMLRequestType)
  protected
    { IXMLRequestType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: string;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
  end;

{ Global Functions }

function GetUpsShipmentAcceptRequest(Doc: IXMLDocument): IXMLUpsShipmentAcceptRequestType;
function LoadUpsShipmentAcceptRequest(const FileName: WideString): IXMLUpsShipmentAcceptRequestType;
function NewUpsShipmentAcceptRequest: IXMLUpsShipmentAcceptRequestType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetUpsShipmentAcceptRequest(Doc: IXMLDocument): IXMLUpsShipmentAcceptRequestType;
begin
  Result := Doc.GetDocBinding('ShipmentAcceptRequest', TXMLShipmentAcceptRequestType, TargetNamespace) as IXMLUpsShipmentAcceptRequestType;
end;

function LoadUpsShipmentAcceptRequest(const FileName: WideString): IXMLUpsShipmentAcceptRequestType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('ShipmentAcceptRequest', TXMLShipmentAcceptRequestType, TargetNamespace) as IXMLUpsShipmentAcceptRequestType;
end;

function NewUpsShipmentAcceptRequest: IXMLUpsShipmentAcceptRequestType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentAcceptRequest', TXMLShipmentAcceptRequestType, TargetNamespace) as IXMLUpsShipmentAcceptRequestType;
end;

{ TXMLShipmentAcceptRequestType }

procedure TXMLShipmentAcceptRequestType.AfterConstruction;
begin
  RegisterChildNode('Request', TXMLRequestType);
  inherited;
end;

function TXMLShipmentAcceptRequestType.Get_Request: IXMLRequestType;
begin
  Result := ChildNodes['Request'] as IXMLRequestType;
end;

function TXMLShipmentAcceptRequestType.Get_ShipmentDigest: WideString;
begin
  Result := ChildNodes['ShipmentDigest'].Text;
end;

procedure TXMLShipmentAcceptRequestType.Set_ShipmentDigest(Value: WideString);
begin
  ChildNodes['ShipmentDigest'].NodeValue := Value;
end;

{ TXMLRequestType }

procedure TXMLRequestType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  inherited;
end;

function TXMLRequestType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLRequestType.Get_RequestAction: WideString;
begin
  Result := ChildNodes['RequestAction'].Text;
end;

procedure TXMLRequestType.Set_RequestAction(Value: WideString);
begin
  ChildNodes['RequestAction'].NodeValue := Value;
end;

function TXMLRequestType.Get_RequestOption: string;
begin
  Result := ChildNodes['RequestOption'].NodeValue;
end;

procedure TXMLRequestType.Set_RequestOption(Value: string);
begin
  ChildNodes['RequestOption'].NodeValue := Value;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_CustomerContext: WideString;
begin
  Result := ChildNodes['CustomerContext'].Text;
end;

procedure TXMLTransactionReferenceType.Set_CustomerContext(Value: WideString);
begin
  ChildNodes['CustomerContext'].NodeValue := Value;
end;

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

end.