// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrEmail2EditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, DBCtrls, EvUIComponents;

type
  TVmrEmail2EditFrame = class(TVmrOptionEditFrame)
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBCheckBox1: TevDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterVmrClass(TVmrEmail2EditFrame);

end.
