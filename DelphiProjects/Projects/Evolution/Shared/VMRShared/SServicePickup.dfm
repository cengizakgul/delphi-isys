inherited ServicePickupFrame: TServicePickupFrame
  Width = 568
  Height = 416
  inherited lEmailAddr: TevLabel
    Top = 256
  end
  object evLabel122: TevLabel [1]
    Left = 8
    Top = 8
    Width = 231
    Height = 13
    Caption = 'Courier Info (i.e. name, ID, car license plate #, ...)'
  end
  inherited edEmailAddr: TevDBEdit
    Top = 256
    TabOrder = 2
  end
  object EvDBMemo1: TEvDBMemo [3]
    Left = 8
    Top = 24
    Width = 489
    Height = 217
    DataField = 'COURIER_INFO'
    DataSource = dsMain
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  inherited evDBCheckBox1: TevDBCheckBox
    Top = 286
  end
end
