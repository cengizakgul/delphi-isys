
{******************************************************************************************************************}
{                                                                                                                  }
{                                                 XML Data Binding                                                 }
{                                                                                                                  }
{         Generated on: 11/3/2004 12:16:50 PM                                                                      }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\RegisterAccountResponse.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\RegisterAccountResponse.xdb   }
{                                                                                                                  }
{******************************************************************************************************************}

unit SDhlShippingKeyResponse;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRegisterType = interface;
  IXMLResultType = interface;

{ IXMLRegisterType }

  IXMLRegisterType = interface(IXMLNode)
    ['{F91A5C12-81E2-426A-9B0C-02C759D7FCC0}']
    { Property Accessors }
    function Get_Version: string;
    function Get_Action: string;
    function Get_Result: IXMLResultType;
    function Get_AccountNbr: Integer;
    function Get_ShippingKey: string;
    { Methods & Properties }
    property Version: string read Get_Version;
    property Action: string read Get_Action;
    property Result: IXMLResultType read Get_Result;
    property AccountNbr: Integer read Get_AccountNbr;
    property ShippingKey: string read Get_ShippingKey;
  end;

{ IXMLResultType }

  IXMLResultType = interface(IXMLNode)
    ['{95F52F65-AABF-40D3-82D2-8FDC48551AA7}']
    { Property Accessors }
    function Get_Code: Integer;
    function Get_Desc: string;
    { Methods & Properties }
    property Code: Integer read Get_Code;
    property Desc: string read Get_Desc;
  end;

{ Forward Decls }

  TXMLRegisterType = class;
  TXMLResultType = class;

{ TXMLRegisterType }

  TXMLRegisterType = class(TXMLNode, IXMLRegisterType)
  protected
    { IXMLRegisterType }
    function Get_Version: string;
    function Get_Action: string;
    function Get_Result: IXMLResultType;
    function Get_AccountNbr: Integer;
    function Get_ShippingKey: string;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLResultType }

  TXMLResultType = class(TXMLNode, IXMLResultType)
  protected
    { IXMLResultType }
    function Get_Code: Integer;
    function Get_Desc: string;
  end;

{ Global Functions }

function GetRegister(Doc: IXMLDocument): IXMLRegisterType;
function LoadRegister(const FileName: WideString): IXMLRegisterType;
function NewRegister: IXMLRegisterType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRegister(Doc: IXMLDocument): IXMLRegisterType;
begin
  Result := Doc.GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

function LoadRegister(const FileName: WideString): IXMLRegisterType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

function NewRegister: IXMLRegisterType;
begin
  Result := NewXMLDocument.GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

{ TXMLRegisterType }

procedure TXMLRegisterType.AfterConstruction;
begin
  RegisterChildNode('Result', TXMLResultType);
  inherited;
end;

function TXMLRegisterType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

function TXMLRegisterType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

function TXMLRegisterType.Get_Result: IXMLResultType;
begin
  Result := ChildNodes['Result'] as IXMLResultType;
end;

function TXMLRegisterType.Get_AccountNbr: Integer;
begin
  Result := ChildNodes['AccountNbr'].NodeValue;
end;

function TXMLRegisterType.Get_ShippingKey: string;
begin
  Result := ChildNodes['ShippingKey'].Text;
end;

{ TXMLResultType }

function TXMLResultType.Get_Code: Integer;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

function TXMLResultType.Get_Desc: string;
begin
  Result := ChildNodes['Desc'].Text;
end;

end.