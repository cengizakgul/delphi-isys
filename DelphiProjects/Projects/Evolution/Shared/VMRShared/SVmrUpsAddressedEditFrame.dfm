inherited VmrUpsAddressedEditFrame: TVmrUpsAddressedEditFrame
  Width = 559
  inherited evLabel3: TevLabel
    Top = 111
  end
  inherited evLabel4: TevLabel
    Top = 143
  end
  inherited evLabel5: TevLabel
    Top = 143
  end
  inherited evLabel6: TevLabel
    Left = 31
    Top = 202
    Width = 60
    Caption = 'Note/AttnTo'
  end
  object lbPhone: TevLabel [8]
    Left = 62
    Top = 179
    Width = 31
    Height = 13
    Caption = 'Phone'
  end
  object evLabel8: TevLabel [9]
    Left = 322
    Top = 177
    Width = 43
    Height = 13
    Caption = 'Package'
  end
  inherited edEmailAddr: TevDBEdit
    Top = 279
  end
  inherited evDBEdit3: TevDBEdit
    Top = 111
  end
  inherited evDBEdit4: TevDBEdit
    Top = 143
  end
  inherited evDBLookupCombo1: TevDBLookupCombo
    Top = 143
  end
  inherited EvDBMemo1: TEvDBMemo
    Top = 199
  end
  object evDBEdit6: TevDBEdit [20]
    Left = 96
    Top = 174
    Width = 201
    Height = 21
    DataField = 'PHONE'
    DataSource = dsMain
    TabOrder = 10
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBComboBox1: TevDBComboBox [21]
    Left = 368
    Top = 172
    Width = 145
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = False
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'PACKAGE'
    DataSource = dsMain
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Package'
      'UPS Letter'
      'UPS Pak'
      'UPS Tube'
      'UPS Box')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 11
    UnboundDataType = wwDefault
  end
end
