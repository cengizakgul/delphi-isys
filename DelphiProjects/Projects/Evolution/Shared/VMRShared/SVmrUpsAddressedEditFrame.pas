unit SVmrUpsAddressedEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrAddressedEditFrame, SDDClasses, SDataDictsystem,
  SDataStructure, DB, Wwdatsrc, ISBasicClasses,  StdCtrls,
  DBCtrls, Buttons, wwdblook, Mask, wwdbedit, Wwdotdot, Wwdbcomb, EvUIComponents,
  isUIwwDBComboBox, LMDCustomButton, LMDButton, isUILMDButton, isUIDBMemo,
  isUIwwDBLookupCombo, isUIwwDBEdit;

type
  TVmrUpsAddressedEditFrame = class(TVmrAddressedEditFrame)
    evDBEdit6: TevDBEdit; 
    lbPhone: TevLabel;
    evLabel8: TevLabel;
    evDBComboBox1: TevDBComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
    constructor CreateBound(const AOwner: TComponent; const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource); override;
  end;


implementation

uses SVmrOptionEditFrame,SVmrClasses,SserviceUps, EvUtils;

{$R *.dfm}


{ TVmrUpsAddressedEditFrame }

constructor TVmrUpsAddressedEditFrame.CreateBound(const AOwner: TComponent;
  const cd: TDataSet; const OptionedTypeObject: TObject;
  const MasterDataSource: TDataSource);
  var s:string;
begin
  inherited;
  s := (OptionedTypeObject as TVmrUpsDeliveryMethod).PossiblePackages;
  evDBComboBox1.Items.CommaText := s;
end;

initialization
  RegisterVmrClass(TVmrUpsAddressedEditFrame);

end.
