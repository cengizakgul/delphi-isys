inherited VmrEMediaClParamFrame: TVmrEMediaClParamFrame
  object lPrintPassword: TevLabel [0]
    Left = 15
    Top = 15
    Width = 49
    Height = 13
    Caption = 'Password:'
  end
  object edPrintPassword: TevDBEdit [1]
    Left = 96
    Top = 15
    Width = 145
    Height = 21
    DataField = 'PRINT_PASSWORD'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBCheckBox1: TevDBCheckBox [2]
    Left = 12
    Top = 36
    Width = 97
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Convert to PDF:'
    DataField = 'CONVERT_TO_PDF'
    DataSource = dsMain
    TabOrder = 1
    ValueChecked = 'Y'
    ValueUnchecked = 'N'
  end
  inherited dsMain: TevDataSource
    Left = 24
    Top = 88
  end
end
