// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrDeliveryMediaTypes;

interface

uses SVmrClasses;

type
  TVmrPaper = class(TVmrMediaType)
  end;

implementation

initialization
  RegisterVmrClass(TVmrPaper);

end.
