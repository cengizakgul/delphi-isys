// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrFtpEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, evExceptions, EvUIComponents,
  isUIwwDBEdit, DBCtrls;

type
  TVmrFtpEditFrame = class(TVmrOptionEditFrame)
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    SMTPServerDBEdit: TevDBEdit;
    FromAddressDBEdit: TevDBEdit;
    evDBCheckBox2: TevDBCheckBox;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.dfm}

uses
  SPackageEntry;

procedure TVmrFtpEditFrame.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
end;

initialization
  RegisterVmrClass(TVmrFtpEditFrame);
end.
