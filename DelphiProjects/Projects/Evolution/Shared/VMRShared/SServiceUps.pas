unit SServiceUps;

interface

uses Classes, XMLDoc, XMLIntf, SVmrClasses, SReportSettings,  Variants,  EvContext,
  DB, SVmrOptionEditFrame, EvExceptions,ActiveX,SVmrUpsAddressedEditFrame, EvUIUtils,
  sUpsShipAcceptReq,
  sUpsShipAcceptResp,
  sUpsShipConfirmReq,
  sUpsShipConfirmResp,
  sUpsVoidReq,
  sUpsVoidResp,
  sUPSOpenshipments
  , EvClientDataSet;

type

  TShipmentParams = Record
    ServiceCode: string;
    Weight: Double;
    ContentDescription: string;
    ShipperReference: string;
    SpecialServiceCode: string;
    Package:string;
    AddressName: string;
    AddressAddr1: string;
    AddressAddr2: string;
    AddressCity: string;
    AddressState: string;
    AddressZip: string;
    AddressAttnTo: string;
    AddressPhone: string;
    TransactionTrace: string;
    AccountNbr:string;

//    UpsShipmentConfirmRequest: IXMLUpsShipmentConfirmRequestType;
    UpsShipmentConfirmResponse: IXMLUpsShipmentConfirmResponseType;
//    UpsShipmentAcceptRequest: IXMLUpsShipmentAcceptRequestType;
    UpsShipmentAcceptResponse: IXMLUpsShipmentAcceptResponseType;
  end;

  VoidShipmentParams = Record

  end;

  TVmrUpsService = class(TVmrAccountedService)
  protected
    procedure PrepareOptionFields(var LoadRecordCount: Integer; const f:
            TFieldDefs); override;

    function BuildAuthXML:string;
    function Base64Str(s:string; var Data:TStringStream):boolean;
    function ExecuteSOAPRequest(const Request: IXMLNode; const ResponseClass: TClass): IXMLNode;
    procedure SaveApiFiles(SP:TShipmentParams;FileNameTemplate:string;ForceSave:boolean = False);
  public

    ShipParams:TShipmentParams;

    function ApiIsActive: Boolean;
    procedure ApiTestCredentials;
    function ApiSendDummyPackage: TShipmentParams;
    function ApiGenerateCertificationXML: string;
    function GetOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    class function GetMethodClasses: TVmrDeliveryMethodClassArray; override;
    function VoidPackage(const MbNbr: string): boolean;
    function ShipPackage(var SP:TShipmentParams):boolean;
    function ExportPackageToWorldShip(var SP:TShipmentParams):boolean;
    function DelPackageFromWorldShip(const MbNbr:string):boolean;

    function NewUpsShipmentConfirmRequest: IXMLUpsShipmentConfirmRequestType;
    function NewUpsShipmentConfirmResponse: IXMLUpsShipmentConfirmResponseType;
    function NewUpsShipmentAcceptRequest: IXMLUpsShipmentAcceptRequestType;
    function NewUpsShipmentAcceptResponse: IXMLUpsShipmentAcceptResponseType;

    function NewVoidShipmentRequest: IXMLUpsVoidShipmentRequestType;
    function NewVoidShipmentResponse: IXMLUpsVoidShipmentResponseType;

  end;

  TVmrUpsDeliveryMethod = class(TVmrAddressedDeliveryMethod)
  private
    dWeight: Double;
    sAirBill: string;
    oUpsService: TVmrUpsService;
  protected

    procedure AddUpsLabel;
    class function GetServiceCode: string; virtual; abstract;
    class function GetSpecialServiceCode: string; virtual; abstract;
    procedure PrepareExtraOptionFields(var Prefix: string; var LoadRecordCount:
            Integer; const f: TFieldDefs); override;
  public


    class function GetMediaClasses: TVmrMediaTypeClassArray; override;
    function OnPickupSheetScan(cd: TEvClientDataset): string; override;
    class function GetShippedMessage(cd: TEvClientDataSet): string; override;
    procedure Release(const cd: TEvClientDataSet); override;
    procedure RevertToUnreleased(const Media: TVmrMediaType; const cd:
            TEvClientDataSet); override;

    function GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass; override;
    class function PossiblePackages:string;virtual;
  end;

  TVmrUpsGround = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  public
    class function PossiblePackages:string;override;
  end;

  TVmrUpsThreeDaySelect = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  public  
    class function PossiblePackages:string;override;
  end;


  TVmrUpsSecondDayAir = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrUpsSecondDayAirAM = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrUpsNextDayAirSaver = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;

  TVmrUpsNextDayAir = class(TVmrUpsDeliveryMethod)
  protected
    class function GetServiceCode: string; override;
    class function GetSpecialServiceCode: string; override;
  end;


implementation

uses EvUtils, Math, SysUtils, EvTypes, SUpsServiceOptionFrame, SOAPHTTPTrans, EvStreamUtils,
  DateUtils, SDataStructure,EvConsts, StrUtils;



{
******************************** TVmrUpsService ********************************
}

(*function TVmrUpsService.ApiGenerateCertificationXML: string;
begin
 /// Dummy
end;
*)

function TVmrUpsService.ApiGenerateCertificationXML: string;
var
  l: TStringList;
  ShipConfReq: IXMLUpsShipmentConfirmRequestType;

  Thursday: TDateTime;
  UserName,UserPhone:string;

  SP : TShipmentParams;

  procedure ProcessShippingTest(TestName:string);
  begin
    ShipPackage(SP);
    SaveApiFiles(SP,'"Cert Ship Test # "'+'"'+TestName+'"',true);
  end;
  procedure ProcessVoidingTest(TestName:string);
  begin
    //Result := ExecuteSOAPRequest(VoidingRequest, TXMLECommerceVoidResponse) as IXMLECommerceVoidResponse;
    {
    l.Append(VoidingRequest.XML);
    l.Append('');
    l.Append(VoidingRequest.ResponseXML);
    l.Append('');
    l.Append('***********************************************');
    }
    SaveApiFiles(SP,'"Cert Void Test # "'+'"'+TestName+'"',true);
  end;
begin
  Thursday := Today;
  while DayOfWeek(Thursday) <> 5 do
    Thursday := Thursday+ 1;
  l := TStringList.Create;

  UserName := Context.UserAccount.FirstName+ ' ' + Context.UserAccount.LastName;
  UserPhone :=  '802-5555555';

  try
    CoInitialize(nil);
    // Express Service with Declared Value and shipment type of Letter
    ShipConfReq := NewUpsShipmentConfirmRequest;

    SP.Weight := 0.4;
    SP.ContentDescription := 'Payroll checks and reports';
    SP.ShipperReference := 'DummyTest';
    SP.SpecialServiceCode := '';
    SP.AddressName := 'Dummy Bureau';
    SP.AddressAddr1 := '1 Main St';
    SP.AddressAddr2 := '';
    SP.AddressCity := 'Burlington';
    SP.AddressState := 'VT';
    SP.AddressZip := '05401';
    SP.AddressAttnTo := 'Joe';
    SP.AddressPhone := '802-5555555';
    SP.TransactionTrace := 'DummyTrace';
    SP.ServiceCode := TVmrUpsGround.GetServiceCode;
    ProcessShippingTest('01');
    SP.UpsShipmentConfirmResponse := nil;
    SP.UpsShipmentAcceptResponse := nil;

    //aAirBill := SP.UpsShipmentConfirmResponse.ShipmentIdentificationNumber; // for void test

    SP.ServiceCode := TVmrUpsThreeDaySelect.GetServiceCode;
    ProcessShippingTest('02');

    SP.ServiceCode := TVmrUpsSecondDayAir.GetServiceCode;
    ProcessShippingTest('03');
    SP.ServiceCode := TVmrUpsSecondDayAirAM.GetServiceCode;
    ProcessShippingTest('04');
    SP.ServiceCode := TVmrUpsNextDayAirSaver.GetServiceCode;
    ProcessShippingTest('05');
    SP.ServiceCode := TVmrUpsNextDayAir.GetServiceCode;
    ProcessShippingTest('06');


    ProcessVoidingTest('01');
    Result := 'Certification files have been saved to C:\DHL\'+FormatDateTime('YYYY_MMM_DD',Now);
    //l.Text;
  finally
    CoUnInitialize;
    l.Free;
  end;
end;


function TVmrUpsService.ApiIsActive: Boolean;
begin
 { Result := not VarIsNull(Options['API_LOGIN']) and not VarIsNull(Options['API_PASSWORD'])
    and not VarIsNull(Options['API_KEY']) and not VarIsNull(Options['ACC_NUMBER'])
    and (Options.FieldByName('API_LIVE').AsString[1] in ['Y','T']);
  }
  Result := not VarIsNull(Options['UPS_WS_PATH'])
           and (Options.FieldByName('UPS_WS_LIVE').AsString[1] = 'Y')
           and not VarIsNull(Options['ACC_NUMBER'])
           //and not VarIsNull(Options['WHEN_EXPORT'])
           ;
end;

function TVmrUpsService.ApiSendDummyPackage: TShipmentParams;
var
  SP:TShipmentParams;
  P:string;
  sl:TstringList;

  procedure Send(ServiceCode:string;PP:string);
  var i:integer;
  begin
     SP.SpecialServiceCode := ServiceCode;
     sl.CommaText := PP;
     for i:=0 to sl.Count -1 do
     begin
         p := sl.Strings[i];
         SP.TransactionTrace :=SP.SpecialServiceCode + '' + p;
         SP.Package := p;
         ExportPackageToWorldShip(SP);
     end;
  end;
begin
  SP.ServiceCode := '03';
  SP.Weight := 0.4;
  SP.ContentDescription := 'Payroll checks and reports';
  SP.ShipperReference := 'DummyTest';
  SP.AddressName := 'Dummy Bureau';
  SP.AddressAddr1 := '1 Main St';
  SP.AddressAddr2 := '';
  SP.AddressCity := 'Burlington';
  SP.AddressState := 'VT';
  SP.AddressZip := '05401';
  SP.AddressAttnTo := 'Joe';
  SP.AddressPhone := '802-5555555';
  SP.TransactionTrace := 'DummyTrace';
  SP.Package := 'Package';

// 'Package,"UPS Letter","UPS Pak","UPS Tube","UPS Express Box Large","UPS Express Box Medium","UPS Express Box Small"';
  sl := TStringList.Create;
  Send(TVmrUpsGround.GetSpecialServiceCode,TVmrUpsGround.PossiblePackages);
  Send(TVmrUpsThreeDaySelect.GetSpecialServiceCode,TVmrUpsThreeDaySelect.PossiblePackages);
  Send(TVmrUpsSecondDayAir.GetSpecialServiceCode,TVmrUpsSecondDayAir.PossiblePackages);
  Send(TVmrUpsSecondDayAirAM.GetSpecialServiceCode,TVmrUpsSecondDayAirAM.PossiblePackages);
  Send(TVmrUpsNextDayAirSaver.GetSpecialServiceCode,TVmrUpsNextDayAirSaver.PossiblePackages);
  Send(TVmrUpsNextDayAir.GetSpecialServiceCode,TVmrUpsNextDayAir.PossiblePackages);
  sl.Free;
  Result := SP;
end;

procedure TVmrUpsService.ApiTestCredentials;
begin
 { try
    CoInitialize(nil);

    Request := NewUpsShipmentConfirmRequest;

    //Request.InitDefaultRequestParams(Options['API_LOGIN'], Options['API_PASSWORD']);
    ExecuteSOAPRequest(Request, nil);
    SaveApiFiles(Request,'"ApiTest" HH_MM_SS');
  finally
    CoUnInitialize;
  end;
 }
end;

procedure TVmrUpsService.SaveApiFiles(SP:TShipmentParams;FileNameTemplate:string;ForceSave:boolean = False);
begin
  /// dummy
end;

(*
procedure TVmrUpsService.SaveApiFiles(Request: IXMLNode;FileNameTemplate:string;ForceSave:boolean = False);
var
  FileName:string;
  FileDir:string;
  FS:TFileStream;
  SS:TStringStream;
  S: IXMLUpsShipmentAcceptResponseType;
begin
  if (Options['DHL_SAVE_API_FILES'] <> 'Y') and (not ForceSave) then exit;

  FileName :=Trim(FormatDateTime(FileNameTemplate,now));
  FileName := StringReplace(FileName,'\','_',[rfReplaceAll]);
  FileName := StringReplace(FileName,'/','_',[rfReplaceAll]);
  FileName := StringReplace(FileName,':','_',[rfReplaceAll]);
  FileDir := Trim(FormatDateTime('"C:\DHL\"YYYY_MMM_DD',now));
  ForceDirectories(FileDir);


  FS := TFileStream.Create(FileDir+'\'+FileName+' Req.XML',fmCreate);
  SS := TStringStream.Create(Request.XML);
  try
    FS.CopyFrom(SS,SS.Size);
  finally
    FreeAndNil(SS);
    FreeAndNil(FS);
  end;
  SS := TStringStream.Create(Request.ResponseXML);
  FS := TFileStream.Create(FileDir+'\'+FileName+' Res.XML',fmCreate);
  try
    FS.CopyFrom(SS,SS.Size);
  finally
    FreeAndNil(SS);
    FreeAndNil(FS);
  end;
  S := LoadXMLData(Request.ResponseXML).GetDocBinding('eCommerce',TXMLECommerceShipmentResponse, TargetNamespace) as IXMLECommerceShipmentResponse;
  if S.Shipment.Label_.Image.Size >0 then
    S.Shipment.Label_.Image.SaveToFile(FileDir+'\'+FileName+' Label.png');
end;

*)

function TVmrUpsService.ExecuteSOAPRequest(
  const Request: IXMLNode;
  const ResponseClass: TClass): IXMLNode;
begin
  /// dummy
end;
(*
function TVmrUpsService.ExecuteSOAPRequest(
  const Request: IBaseXMLECommerceType;
  const ResponseClass: TClass): IXMLNode;
var
  rr: THTTPReqResp;
  s: TStringStream;
  fr: SDhleCommerceFault.IXMLECommerceType;
begin
  rr := THTTPReqResp.Create(nil);
  try
    rr.InvokeOptions := rr.InvokeOptions+ [soIgnoreInvalidCerts];
    rr.UseUTF8InHeader := False;
    if Options['API_LIVE'] = 'Y' then
      rr.URL := 'https://eCommerce.airborne.com/ApiLanding.asp'
    else
      rr.URL := 'https://ecommerce.airborne.com/ApiLandingTest.asp';
    s := TStringStream.Create('');
    rr.Execute(Request.XML, s);
    Request.ResponseXML := s.DataString;
    fr := LoadXMLData(s.DataString).GetDocBinding('eCommerce', SDhleCommerceFault.TXMLECommerceType, TargetNamespace) as SDhleCommerceFault.IXMLECommerceType;
    if fr.Faults.Count > 0 then
      raise EInconsistentData.CreateHelp(fr.Faults[0].Description, IDH_InconsistentData);
    if ResponseClass = nil then
      Result := nil
    else
      Result := LoadXMLData(s.DataString).GetDocBinding('eCommerce', ResponseClass, TargetNamespace);
  finally
    rr.Free;
  end;
end;

*)

class function TVmrUpsService.GetMethodClasses: TVmrDeliveryMethodClassArray;
begin
  SetLength(Result, 6);
  Result[0] := TVmrUpsGround;
  Result[1] := TVmrUpsThreeDaySelect;
  Result[2] := TVmrUpsSecondDayAir;
  Result[3] := TVmrUpsSecondDayAirAM;
  Result[4] := TVmrUpsNextDayAir;
  Result[5] := TVmrUpsNextDayAirSaver;

 // Result[5] := TVmrUpsExpressSaturday
end;

{
**************************** TVmrUpsDeliveryMethod *****************************
}

procedure TVmrUpsDeliveryMethod.PrepareExtraOptionFields(
  var Prefix: string; var LoadRecordCount: Integer; const f: TFieldDefs);
begin
  inherited;
  f.Add('PHONE', ftString, 30);
  f.Find('NAME').Size := 35;
  f.Find('ADDRESS1').Size := 35;
  f.Find('ADDRESS2').Size := 35;
  f.Find('CITY').Size := 35;
  f.Add('PACKAGE', ftString, 15);
end;

class function TVmrUpsDeliveryMethod.GetMediaClasses: TVmrMediaTypeClassArray;
begin
  SetLength(Result, 2);
  Result[0] := TVmrSeparatedPaperMediaType;
  Result[1] := TVmrGroupedPaperMediaType;
end;

class function TVmrUpsDeliveryMethod.GetShippedMessage(cd: TEvClientDataSet):string;
var tn:string;
begin
  Result := inherited GetShippedMessage(cd);
  if not VarIsNull(cd['TRACKING_INFO']) then
    tn := cd['TRACKING_INFO']
  else
    EvDialog('Input required','Please, input tracking number for '+cd['DESCRIPTION'],tn);
  Result := Result+ #13'The tracking number for this shipment is '+ tn+'. '+#13+
      'You can track your shipment at www.ups.com'+
      //' or use this link: '+
      //#13'http://track.dhl-usa.com/TrackByNbr.asp?ShipmentNumber='+ cd['TRACKING_INFO']+
      #13;
end;

function TVmrUpsDeliveryMethod.OnPickupSheetScan(
  cd: TEvClientDataset): string;
var
 s:IXMLUPSOpenShipmentsType;
 f :string;
 p:string;
begin
  oUpsService := CreateSbDeliveryService(cd) as TVmrUpsService;
  Result := 'Put all pages in UPS letter envelope and affix printed UPS label';
  if not oUpsService.ApiIsActive then
    exit;
  p := IncludeTrailingPathDelimiter(oUpsService.Options['UPS_WS_PATH']);
  f := p+cd.fieldByName('SB_MAIL_BOX_NBR').AsString+ '.out';
  if FileExists(f) then
  begin
   s := LoadUPSOpenShipments(f);
   if UpperCase(s.OpenShipment[0].ProcessStatus) <> 'PROCESSED' then
   begin
      Result := 'Export failed with next error:'+#13#10 +
                              s.OpenShipment[0].ProcessMessage.Items[0].Error.ErrorMessage;
      exit;
   end;
  end else
  begin
    Result := 'Warning:Can''t find result of exporting to WorldShip program!!!';
    exit;
  end;

  cd.Edit;
  cd['TRACKING_INFO'] := s.OpenShipment[0].ProcessMessage[0].TrackingNumbers.TrackingNumber[0];
  //cd['COST'] := Response.Shipment.ShipmentDetail.RateEstimate.TotalChargeEstimate;
  cd.Post;

{  if  oUpsService.Options['WHEN_EXPORT'] = 'P' then
  begin
    oUpsService.ExportPackageToWorldShip(oUpsService.ShipParams);
  end;
}
end;



procedure TVmrUpsDeliveryMethod.AddUpsLabel;
var
  ShipRes:boolean;
begin
  //if oUpsService.Options['WHEN_EXPORT'] = 'P' then /// Export on Pickup
  //   exit;

  if (Length(RLA) =0) or (not oUPSService.ApiIsActive) then
    exit;

  //if k <>-1 then
  begin
    //Job := RLA[k];
    //if oDhlService.ApiIsActive then
    //begin
      dWeight := CalcWeight;
      if dWeight = 0 then
        raise EInconsistentData.CreateHelp('UPS API is active but paper weight is zero', IDH_InconsistentData);

      oUpsService.ShipParams.Weight:= max(dWeight* 1.05,0.1);
      oUpsService.ShipParams.ServiceCode:= GetServiceCode;
      oUpsService.ShipParams.ContentDescription := 'Payroll';
      oUpsService.ShipParams.ShipperReference := IntToStr(BoxNbr);
      oUpsService.ShipParams.SpecialServiceCode := GetSpecialServiceCode;
      oUpsService.ShipParams.AddressName :=VarToStr(ExtraOptions['NAME']);
      oUpsService.ShipParams.AddressAddr1 := VarToStr(ExtraOptions['ADDRESS1']);
      oUpsService.ShipParams.AddressAddr2 := VarToStr(ExtraOptions['ADDRESS2']);
      oUpsService.ShipParams.AddressCity := VarToStr(ExtraOptions['CITY']);
      oUpsService.ShipParams.AddressState := VarToStr(ExtraOptions['STATE']);
      oUpsService.ShipParams.AddressZip := VarToStr(ExtraOptions['ZIP']);
      oUpsService.ShipParams.AddressAttnTo := VarToStr(ExtraOptions['NOTE']);
      oUpsService.ShipParams.AddressPhone := VarToStr(ExtraOptions['PHONE']);
      oUpsService.ShipParams.TransactionTrace := IntToStr(BoxNbr);
      oUpsService.ShipParams.Package := VarToStr(ExtraOptions['PACKAGE']);

      //ShipRes := oUpsService.ShipPackage(oUpsService.ShipParams);
      ShipRes := oUpsService.ExportPackageToWorldShip(oUpsService.ShipParams);
      if not ShipRes then
      begin
        raise EInconsistentData.CreateHelp('UPS returned error', IDH_InconsistentData);
      end;
      //sAirBill := oUpsService.ShipParams.UpsShipmentConfirmResponse.ShipmentIdentificationNumber;
      //Job.cd.Edit;
      //Job.cd['TRACKING_INFO'] := sAirBill;
      //Job.cd['COST'] := oUpsService.ShipParams.UpsShipmentConfirmResponse.ShipmentCharges.TotalCharges.MonetaryValue;
      //Job.cd.Post;

      {P :=inherited GetPickupSheetReportParams(Job);
      P.NBR := VarToInt(oUpsService.Options['UPS_LABEL_REPORT_NBR']);
      P.Params.Add('PNG_PICTURE', oUpsService.ShipParams.UpsShipmentAcceptResponse.ShipmentResults.PackageResults.Items[0].LabelImage.GraphicImage);
      P.Params.Add('Weight', dWeight);

      if Assigned(P) then
      begin
        with ctx_RWLocalEngine do
        begin
          StartGroup;
          CalcPrintReport(P);
          ms := EndGroup(rdtNone);
        end;
        RR := TrwReportResults.Create(ms);
        try
          if RR.Count > 0 then
          begin
            if Assigned(RR[0]) then
            begin
              if RR[0].ErrorMessage <> '' then
                 raise EMissingReport.CreateHelp('UPS Label report failed with error: '+ RR[0].ErrorMessage, IDH_MissingReport);
              R := RR[0];
              R.Collection := Job.Res;
              R.Index := 0;
              R.Tray := GetPrinterBinName(PrInfo[Job.PrinterIndex], MEDIA_TYPE_DHL_LABEL);
              if R.Tray = '' then
                 raise EPrinterNotDefined.CreateFmt('Printer %s does not have UPS Label tray assigned', [PrInfo[Job.PrinterIndex].PrinterName]);
            end;
          end;
        finally
          RR.Free;
        end;

      end;

    //end;
    }
  end;
end;

{procedure TVmrUpsDeliveryMethod.AddUpsLabel;
var
  ConfirmResponse: IXMLUpsShipmentConfirmResponseType;
  AcceptResponse:  IXMLUpsShipmentAcceptResponseType;

  s: string;
  i,k: Integer;
  P: TrwRepParams;
  ms: IisStream;
  RR: TrwReportResults;
  R : TrwReportResult;
  RL2:TrwReportResults;
  Job: TVmrPrintJob;
  ShipRes:boolean;
  function FindPrinterIndexForDHLLabel:integer;
  var i,j:integer;
  begin
    Result := -1;
    for i := 0 to High(PrInfo) do
    begin
      if PrInfo[i].Active  and ((PrInfo[i].Location = '') or (PrInfo[i].Location = Location)) then
      begin
        for j := Low(PrInfo[i].Bins) to High(PrInfo[i].Bins) do
           if PrInfo[i].Bins[j].MediaType = MEDIA_TYPE_DHL_LABEL then
           begin
                Result := i;
                exit;
           end;
      end;
    end;
    if Result = -1 then
      raise EPrinterNotDefined.Create('Could not find a printer for media type: UPS Label');
  end;

begin
  if oUpsService.Options['WHEN_EXPORT'] = 'P' then /// Export on Pickup
     exit;
  k := -1;
  if (Length(RLA) =0) or (not oUPSService.ApiIsActive) then
    exit;
  for i:=0 to High(RLA) do
  begin
     Job := RLA[i];
     if VarIsNull(RLA[i].SbUpLevelMailBoxNbr) and (GetPrinterBinName(PrInfo[RLA[i].PrinterIndex], MEDIA_TYPE_DHL_LABEL)<>'') then
     begin
       k := i;
       break;
     end;
  end;
  if k = -1 then // we need to Add New Part for Ups label
  begin
     k:=FindPrinterIndexForDHLLabel;

     RL2 := TrwReportResults.Create;
     RL2.PrinterName := PrInfo[k].PrinterName;
     SetLength(RLA, Length(RLA)+1);
     i := High(RLA);

     RLA[i].PrinterIndex := k;
     RLA[i].Res := RL2;
     RLA[i].SbMailBoxNbr := RLA[0].SbMailBoxNbr;
     RLA[i].SbUpLevelMailBoxNbr := RLA[0].SbUpLevelMailBoxNbr;
     RLA[i].ClNbr := RLA[0].ClNbr;
     RLA[i].CoNbr := Null;
     RLA[i].PrNbr := Null;
     RLA[i].PartNumber := RLA[0].PartCount+1;
     RLA[i].PartCount := RLA[0].PartCount;
     RLA[i].BarCode := RLA[0].BarCode;
     RLA[i].SbContentNbrs := VarArrayCreate([0, RLA[i].Res.Count-1], varVariant);
     RLA[i].Description := RLA[0].Description;
     RLA[i].cd := RLA[0].cd;
     k := i;
     for i := 0 to High(RLA) do
     begin
       if RLA[i].SbMailBoxNbr = RLA[0].SbMailBoxNbr then
          RLA[i].PartCount := RLA[i].PartCount +1;
     end;
  end;
  if k <>-1 then
  begin
    Job := RLA[k];
    //if oDhlService.ApiIsActive then
    //begin
      dWeight := CalcWeight;
      if dWeight = 0 then
        raise EInconsistentData.CreateHelp('UPS API is active but paper weight is zero', IDH_InconsistentData);

      oUpsService.ShipParams.Weight:= dWeight* 1.05;
      oUpsService.ShipParams.ServiceCode:= GetServiceCode;
      oUpsService.ShipParams.ContentDescription := 'Payroll';
      oUpsService.ShipParams.ShipperReference := IntToStr(BoxNbr);
      oUpsService.ShipParams.SpecialServiceCode := GetSpecialServiceCode;
      oUpsService.ShipParams.AddressName :=VarToStr(ExtraOptions['NAME']);
      oUpsService.ShipParams.AddressAddr1 := VarToStr(ExtraOptions['ADDRESS1']);
      oUpsService.ShipParams.AddressAddr2 := VarToStr(ExtraOptions['ADDRESS2']);
      oUpsService.ShipParams.AddressCity := VarToStr(ExtraOptions['CITY']);
      oUpsService.ShipParams.AddressState := VarToStr(ExtraOptions['STATE']);
      oUpsService.ShipParams.AddressZip := VarToStr(ExtraOptions['ZIP']);
      oUpsService.ShipParams.AddressAttnTo := VarToStr(ExtraOptions['NOTE']);
      oUpsService.ShipParams.AddressPhone := VarToStr(ExtraOptions['PHONE']);
      oUpsService.ShipParams.TransactionTrace := IntToStr(BoxNbr);
      oUpsService.ShipParams.Package := 'Package';

      ShipRes := oUpsService.ShipPackage(oUpsService.ShipParams);
      if not ShipRes then
      begin
        s := oUpsService.ShipParams.UpsShipmentConfirmResponse.Response.ResponseStatusDescription;
        //s := '';
        //for i := 0 to Response.Shipment.Faults.Count-1 do
        //  s := s+ #13+ Response.Shipment.Faults[i].Desc;
        raise EInconsistentData.CreateHelp('UPS API returned error: '+ s, IDH_InconsistentData);
      end;
      sAirBill := oUpsService.ShipParams.UpsShipmentConfirmResponse.ShipmentIdentificationNumber;
      Job.cd.Edit;
      Job.cd['TRACKING_INFO'] := sAirBill;
      Job.cd['COST'] := oUpsService.ShipParams.UpsShipmentConfirmResponse.ShipmentCharges.TotalCharges.MonetaryValue;
      Job.cd.Post;

      P :=inherited GetPickupSheetReportParams(Job);
      P.NBR := VarToInt(oUpsService.Options['UPS_LABEL_REPORT_NBR']);
      P.Params.Add('PNG_PICTURE', oUpsService.ShipParams.UpsShipmentAcceptResponse.ShipmentResults.PackageResults.Items[0].LabelImage.GraphicImage);
      P.Params.Add('Weight', dWeight);

      if Assigned(P) then
      begin
        with ctx_RWLocalEngine do
        begin
          StartGroup;
          CalcPrintReport(P);
          ms := EndGroup(rdtNone);
        end;
        RR := TrwReportResults.Create(ms);
        try
          if RR.Count > 0 then
          begin
            if Assigned(RR[0]) then
            begin
              if RR[0].ErrorMessage <> '' then
                 raise EMissingReport.CreateHelp('UPS Label report failed with error: '+ RR[0].ErrorMessage, IDH_MissingReport);
              R := RR[0];
              R.Collection := Job.Res;
              R.Index := 0;
              R.Tray := GetPrinterBinName(PrInfo[Job.PrinterIndex], MEDIA_TYPE_DHL_LABEL);
              if R.Tray = '' then
                 raise EPrinterNotDefined.CreateFmt('Printer %s does not have UPS Label tray assigned', [PrInfo[Job.PrinterIndex].PrinterName]);
            end;
          end;
        finally
          RR.Free;
        end;
      end;
    //end;
  end;
end;
}
procedure TVmrUpsDeliveryMethod.Release(const cd: TEvClientDataSet);
var
i:integer;
begin
  inherited;
  if Length(RLA) = 0 then Exit;
  oUpsService := CreateSbDeliveryService(cd) as TVmrUpsService;
  try
    sAirBill := '';
    try
      AddUpsLabel;
    except
      try
        for i:=0 to High(RLA) do
        begin
           try
             if not VarIsNull(RLA[i].cd['TRACKING_INFO']) then
             begin
               oUpsService.VoidPackage(RLA[i].cd['TRACKING_INFO']);
               RLA[i].cd.Edit;
               RLA[i].cd['TRACKING_INFO'] := Null;
               RLA[i].cd['COST'] := Null;
               RLA[i].cd.Post;
             end;
           finally
           end;
        end;
      except
      end;
      raise;
    end;
  finally
    FreeAndNil(oUpsService);
  end;
end;

procedure TVmrUpsDeliveryMethod.RevertToUnreleased(
  const Media: TVmrMediaType; const cd: TEvClientDataSet);
begin
  //if not VarIsNull(cd['TRACKING_INFO']) then
  //begin
    oUpsService := CreateSbDeliveryService(cd) as TVmrUpsService;
    try
      if oUpsService.ApiIsActive then
      begin
        oUpsService.VoidPackage(cd['SB_MAIL_BOX_NBR']);
        cd.Edit;
        cd['TRACKING_INFO'] := Null;
        cd['COST'] := Null;
        cd.Post;
      end;
    finally
      FreeAndNil(oUpsService);
    end;
  //end;
  inherited;
end;


{ TVmrUpsGround }

class function TVmrUpsGround.GetServiceCode: string;
begin
  Result := '03';
end;

class function TVmrUpsGround.GetSpecialServiceCode: string;
begin
  Result := 'Ground';
end;

{ TVmrUpsNextAfternoon}

class function TVmrUpsSecondDayAirAM.GetServiceCode: string;
begin
  Result := '59';
end;

class function TVmrUpsSecondDayAirAM.GetSpecialServiceCode: string;
begin
  Result := '2nd Day Air AM';
end;

{ TVmrUpsExpress1030am }

class function TVmrUpsThreeDaySelect.GetServiceCode: string;
begin
  Result := '12';
end;

class function TVmrUpsThreeDaySelect.GetSpecialServiceCode: string;
begin
  Result := '3 Day Select';
end;

{ TVmrUpsExpress1200pm }

class function TVmrUpsNextDayAirSaver.GetServiceCode: string;
begin
  Result := '13';
end;

class function TVmrUpsNextDayAirSaver.GetSpecialServiceCode: string;
begin
  Result := 'Next Day Air Saver';
end;

{ TVmrUpsExpressSaturday }

class function TVmrUpsSecondDayAir.GetServiceCode: string;
begin
  Result := '02';
end;

class function TVmrUpsSecondDayAir.GetSpecialServiceCode: string;
begin
  Result := '2nd Day Air';
end;

{ TVmrUpsSecondDay5pm }

class function TVmrUpsNextDayAir.GetServiceCode: string;
begin
  Result := '01';
end;

class function TVmrUpsNextDayAir.GetSpecialServiceCode: string;
begin
  Result := 'Next Day Air';
end;


function TVmrUpsService.GetOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
  Result := TUpsServiceOptionFrame;
end;

procedure TVmrUpsService.PrepareOptionFields(var LoadRecordCount: Integer;
  const f: TFieldDefs);
begin
  inherited;
  f.Add('API_LOGIN', ftString, 40);
  f.Add('API_PASSWORD', ftString, 40);
  f.Add('API_KEY', ftString, 128);
  f.Add('API_LIVE', ftString, 1);
  f.Add('UPS_LABEL_REPORT_NBR', ftString,20);
  f.Add('UPS_SAVE_API_FILES', ftString,1);
  f.Add('UPS_WS_PATH', ftString, 128);
  f.Add('UPS_WS_LIVE', ftString, 128);
  f.Add('WHEN_EXPORT', ftString, 1);
end;


function TVmrUpsService.ExportPackageToWorldShip(
  var SP: TShipmentParams): boolean;

  var s  :IXMLUPSOpenShipmentsType;
      f  :TFileStream;
      ss :TStringStream;
begin
  result := true;
  try
    CoInitialize(nil) ;
    s := NewUPSOpenShipments;

    with s.Add do
    begin
      ProcessStatus := '';
      ShipTo.CompanyOrName := SP.AddressName;
      ShipTo.Attention := SP.AddressAttnTo;
      ShipTo.Address1 := SP.AddressAddr1;
      ShipTo.Address2 := SP.AddressAddr2;
      ShipTo.CountryTerritory := 'United States';
      ShipTo.PostalCode := SP.AddressZip;
      ShipTo.CityOrTown := SP.AddressCity;
      ShipTo.StateProvinceCounty := SP.AddressState;
      //ShipTo.Telephone := '1112546878777';
      //ShipTo.FaxNumber := '1112546878777';
      ShipTo.LocationID := '';
      //ShipTo.ResidentialIndicator := '1';


      ShipmentInformation.ServiceType := SP.SpecialServiceCode;
      ShipmentInformation.NumberOfPackages := '1';
      ShipmentInformation.DescriptionOfGoods := 'Confidential Reports';
      ShipmentInformation.ShipperNumber := Options['ACC_NUMBER'];
      ShipmentInformation.BillingOption := 'PP';
      ShipmentInformation.BillDutyTaxTo := '';
      ShipmentInformation.SplitDutyAndTax := '';



      with Package.Add do
      begin
        PackageType := SP.Package;
        Weight := FloatToStr(SP.Weight);
        TrackingNumber := '';
        LargePackageIndicator := '';
        Reference1 := SP.TransactionTrace;
      end;

      //////////////

    end;
    f := TFileStream.Create(Options['UPS_WS_PATH']+SP.TransactionTrace+'.xml',fmCreate);
    ss := TStringStream.Create('<?xml version="1.0" encoding="UTF-8"?>' + #13#10+ s.XML);
    try
      ss.Position := 0;
      f.CopyFrom(ss,ss.Size);
    finally
       ss.Free;
       f.Free;
    end;
  finally
    CoUnInitialize;

  end;
end;

function TVmrUpsService.DelPackageFromWorldShip(
  const MbNbr: string): boolean;


   // var s,sx  :IXMLUPSOpenShipmentsType;
begin
   result := true;
   if FileExists(Options['UPS_WS_PATH']+MbNbr+'.xml') then
   begin
     // s := LoadUPSOpenShipments(Options['UPS_WS_PATH']+MbNbr+'.xml');
     // if s.OpenShipment[0].Package.Items[0].Reference1 = mbNbr then
      begin
        DeleteFile(Options['UPS_WS_PATH']+MbNbr+'.xml');
        exit;
      end;
   end else
   if FileExists(Options['UPS_WS_PATH']+MbNbr+'.xxx') and FileExists(Options['UPS_WS_PATH']+MbNbr+'.out') then
   begin
      //s := LoadUPSOpenShipments(Options['UPS_WS_PATH']+MbNbr+'.out');
      //sx := LoadUPSOpenShipments(Options['UPS_WS_PATH']+MbNbr+'.xxx');
      //if (s.OpenShipment[0].Shipment.Reference1 = mbNbr) and
      //   (sx.OpenShipment[0].Shipment.Reference1 = mbNbr)then
      //begin

      //  if s.OpenShipment[0].ProcessStatus = 'Processed' then
          result := false;
      //end;

   end;
end;


function TVmrUpsService.ShipPackage(var SP:TShipmentParams): boolean;
{const ServiceCode: string;
  const Weight: Double; const ContentDescription, ShipperReference,
  SpecialServiceCode, AddressName, AddressAddr1, AddressAddr2, AddressCity,
  AddressState, AddressZip, AddressAttnTo, AddressPhone,
  TransactionTrace: string}
var
  ShipConfReq: IXMLUpsShipmentConfirmRequestType;
  ShipConfResp: IXMLUpsShipmentConfirmResponseType;
  ShipAccReq:IXMLUpsShipmentAcceptRequestType;
  ShipAccResp:IXMLUpsShipmentAcceptResponseType;
  rr: THTTPReqResp;
  s : TstringStream;
begin
  Result := true;
  s := TstringStream.Create('');
  try
    CoInitialize(nil);
    DM_SERVICE_BUREAU.SB.Activate;
    ShipConfReq := NewUpsShipmentConfirmRequest;
    ShipAccReq  := NewUpsShipmentAcceptRequest;

    rr := THTTPReqResp.Create(nil);
    rr.InvokeOptions := rr.InvokeOptions+ [soIgnoreInvalidCerts];
    rr.UseUTF8InHeader := False;
    if Options['API_LIVE'] = 'Y' then
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/ShipConfirm'
    else
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/ShipConfirm';

    ShipConfReq.Request.TransactionReference.CustomerContext := SP.ShipperReference;
    ShipConfReq.Request.TransactionReference.XpciVersion := '1.0001';
    ShipConfReq.Request.RequestAction :='ShipConfirm';
    ShipConfReq.Request.RequestOption :='nonvalidate';
    ShipConfReq.LabelSpecification.LabelPrintMethod.Code := 'GIF';
    ShipConfReq.LabelSpecification.LabelImageFormat.Code := 'GIF';
    ShipConfReq.LabelSpecification.LabelStockSize.Height := 4;
    ShipConfReq.LabelSpecification.LabelStockSize.Width  := 6;

    ShipConfReq.Shipment.PaymentInformation.Prepaid.BillShipper.AccountNumber := Options['ACC_NUMBER'];
    ShipConfReq.Shipment.Service.Code := '01';

    with ShipConfReq.Shipment.Shipper do
    begin
     Name := SP.AddressName;
     ShipperNumber := Options['ACC_NUMBER'];
     AttentionName := Context.UserAccount.FirstName + ' '+ Context.UserAccount.LastName;
     PhoneNumber.UnStructuredPhoneNumber := DM_SERVICE_BUREAU.SB['PHONE'];

     Address.AddressLine1 := DM_SERVICE_BUREAU.SB['ADDRESS1'];
     Address.AddressLine2 := DM_SERVICE_BUREAU.SB['ADDRESS2'];
     //Address.AddressLine3 := 'USShipperAddr3';
     Address.City := DM_SERVICE_BUREAU.SB['CITY'];

     Address.StateProvinceCode := DM_SERVICE_BUREAU.SB['STATE'];
     Address.PostalCode := LeftStr(DM_SERVICE_BUREAU.SB['ZIP_CODE'],5);
     Address.CountryCode := 'US';
    end;

    with ShipConfReq.Shipment.ShipFrom do
    begin
      CompanyName := DM_SERVICE_BUREAU.SB['SB_NAME'];
//      AttentionName := 'ShipFromAttnName';
      PhoneNumber.UnStructuredPhoneNumber := DM_SERVICE_BUREAU.SB['PHONE'];

      Address.AddressLine1 := DM_SERVICE_BUREAU.SB['ADDRESS1'];
      Address.AddressLine2 := DM_SERVICE_BUREAU.SB['ADDRESS2'];
      //Address.AddressLine3 := 'USShipFromAddr3';
      Address.City := DM_SERVICE_BUREAU.SB['CITY'];

      Address.StateProvinceCode := DM_SERVICE_BUREAU.SB['STATE'];
      Address.PostalCode := LeftStr(DM_SERVICE_BUREAU.SB['ZIP_CODE'],5);
      Address.CountryCode := 'US';
    end;


    with ShipConfReq.Shipment.ShipTo do
    begin
      CompanyName := SP.AddressName;
      AttentionName := SP.AddressAttnTo;
      PhoneNumber.UnStructuredPhoneNumber := SP.AddressPhone;

      Address.AddressLine1 := SP.AddressAddr1;
      Address.AddressLine2 := SP.AddressAddr2;

      Address.City := SP.AddressCity;
      Address.StateProvinceCode := SP.AddressState;
      Address.PostalCode := LeftStr(SP.AddressZip,5);
      Address.CountryCode := 'US';
//      Address.ResidentialAddress := '';
    end;


    with ShipConfReq.Shipment.Package.Add do
    begin
      Description :=  '1st';
      PackagingType.Code := '03';
      PackageWeight.UnitOfMeasurement.Code := 'LBS';
      PackageWeight.Weight := SP.Weight;
      //ReferenceNumber.Code := '';
      //ReferenceNumber.Value := '';
//      PackageServiceOptions.COD.CODFundsCode := 0;
//      PackageServiceOptions.COD.CODCode := 0;
//      PackageServiceOptions.COD.CODAmount := 0;
//      PackageServiceOptions.InsuredValue.CurrencyCode := 'USD';
//      PackageServiceOptions.InsuredValue.MonetaryValue := '0';
    end;

    rr.Execute('<?xml version="1.0" encoding="UTF-8"?>'+#13#10+BuildAuthXML+#13#10+
               '<?xml version="1.0" encoding="UTF-8"?>'+#13#10+ShipConfReq.XML, s);
    ShipConfResp := LoadXMLData(s.DataString).GetDocBinding('ShipmentConfirmResponse', TXMLShipmentConfirmResponseType, TargetNamespace) as IXMLUpsShipmentConfirmResponseType;


    if ShipConfResp.Response.ResponseStatusCode = 0 then
    begin
       raise EInconsistentData.CreateHelp(ShipConfResp.Response.Error.ErrorDescription, IDH_InconsistentData);
    end;
//    TrackId := ShipConfResp.ShipmentIdentificationNumber;


    ShipAccReq.Request.TransactionReference.CustomerContext :='ShipConfirmUS';
    ShipAccReq.Request.TransactionReference.XpciVersion := '1.0001';
    ShipAccReq.Request.RequestAction := 'ShipAccept';
    ShipAccReq.Request.RequestOption := '01';
    ShipAccReq.ShipmentDigest := ShipConfResp.ShipmentDigest;
    s.Size := 0;
    if Options['API_LIVE'] = 'Y' then
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/ShipAccept'
    else
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/ShipAccept';

    rr.Execute('<?xml version="1.0" encoding="UTF-8"?>'+#13#10+BuildAuthXML+#10#13+
               '<?xml version="1.0" encoding="UTF-8"?>'+#13#10+ShipAccReq.XML, s);

    ShipAccResp := LoadXMLData(s.DataString).GetDocBinding('ShipmentAcceptResponse', TXMLShipmentAcceptResponseType, TargetNamespace) as IXMLUpsShipmentAcceptResponseType;
    if ShipAccResp.Response.ResponseStatusCode = 0 then
    begin
      raise EInconsistentData.CreateHelp(ShipAccResp.Response.ResponseStatusDescription, IDH_InconsistentData);
    end;

    ShipAccResp.ShipmentResults.PackageResults.Items[0].LabelImage.GraphicImage;

    SP.UpsShipmentAcceptResponse := ShipAccResp;
    SP.UpsShipmentConfirmResponse := ShipConfResp;

  finally
     CoUnInitialize;
     s.Free;
  end;
end;

function TVmrUpsService.VoidPackage(
  const MbNbr: string): boolean;
begin
  result := DelPackageFromWorldShip(MbNbr);
(*  try
    {CoInitialize(nil);
    s := TstringStream.Create('');
    rr := THTTPReqResp.Create(nil);

    rr.InvokeOptions := rr.InvokeOptions+ [soIgnoreInvalidCerts];
    rr.UseUTF8InHeader := False;
    if Options['API_LIVE'] = 'Y' then
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/Void'
    else
      rr.URL := 'https://wwwcie.ups.com/ups.app/xml/Void';
    VoidReq := NewVoidShipmentRequest ;
    VoidReq.Request.TransactionReference.CustomerContext := '';
    VoidReq.Request.TransactionReference.XpciVersion := '1.0001';
    VoidReq.Request.RequestAction := 'Void';
    VoidReq.Request.RequestOption := '1';
    VoidReq.ExpandedVoidShipment.ShipmentIdentificationNumber := AirBill;

    rr.Execute('<?xml version="1.0" encoding="UTF-8"?>'+#13#10+BuildAuthXML+#10#13+
               '<?xml version="1.0" encoding="UTF-8"?>'+#13#10+VoidReq.XML, s);

    VoidResp := LoadXMLData(s.DataString).GetDocBinding('VoidShipmentResponse', TXMLVoidShipmentResponseType, TargetNamespace) as IXMLUpsVoidShipmentResponseType;
    if VoidResp.Response.ResponseStatusCode = 0 then
    begin
      raise EInconsistentData.CreateHelp(VoidResp.Response.ResponseStatusDescription, IDH_InconsistentData);
    end;

    //SaveApiFiles(VoidReq,Format('"VoidPkg # %s" HH_MM_SS ',[Airbill]));
    }
  finally
    //   CoUnInitialize;
  end;
  *)
end;



function TVmrUpsDeliveryMethod.GetExtraOptionEditFrameClass: TVmrOptionEditFrameClass;
begin
   Result := TVmrUpsAddressedEditFrame;
end;

function TVmrUpsService.BuildAuthXML: string;
begin
  Result := Format('<AccessRequest xml:lang="en-US">'#13#10 +
   '<AccessLicenseNumber>%s</AccessLicenseNumber>'#13#10+
   '<UserId>%s</UserId>'#13#10+
   '<Password>%s</Password>'#13#10+
   '</AccessRequest>',[ Options['API_KEY'],Options['API_LOGIN'], Options['API_PASSWORD']]);
end;

function TVmrUpsService.NewUpsShipmentAcceptRequest: IXMLUpsShipmentAcceptRequestType;
begin
   Result := NewXMLDocument.GetDocBinding('ShipmentAcceptRequest', TXMLShipmentAcceptRequestType, TargetNamespace) as IXMLUpsShipmentAcceptRequestType;
end;

function TVmrUpsService.NewUpsShipmentAcceptResponse: IXMLUpsShipmentAcceptResponseType;
begin
   Result := NewXMLDocument.GetDocBinding('ShipmentAcceptResponse', TXMLShipmentAcceptResponseType, TargetNamespace) as IXMLUpsShipmentAcceptResponseType;
end;

function TVmrUpsService.NewUpsShipmentConfirmRequest: IXMLUpsShipmentConfirmRequestType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentConfirmRequest', TXMLShipmentConfirmRequestType, TargetNamespace) as IXMLUpsShipmentConfirmRequestType;
end;

function TVmrUpsService.NewUpsShipmentConfirmResponse: IXMLUpsShipmentConfirmResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentConfirmResponse', TXMLShipmentConfirmResponseType, TargetNamespace) as IXMLUpsShipmentConfirmResponseType;
end;

function TVmrUpsService.NewVoidShipmentRequest: IXMLUpsVoidShipmentRequestType;
begin
  Result := NewXMLDocument.GetDocBinding('VoidShipmentRequest', TXMLVoidShipmentRequestType, TargetNamespace) as IXMLUpsVoidShipmentRequestType;
end;

function TVmrUpsService.NewVoidShipmentResponse: IXMLUpsVoidShipmentResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('VoidShipmentResponse', TXMLVoidShipmentResponseType, TargetNamespace) as IXMLUpsVoidShipmentResponseType;
end;

function TVmrUpsService.Base64Str(s: string;
  var Data: TStringStream): boolean;
var
  a : array[1..4] of char;
  b : array[1..4] of byte;
  d : array[1..4] of byte;
  n4 : integer;
  n8,n6 : byte;
  m,n,k : word;

  function Base64Value(c:char; var v:byte) : boolean;
  begin
    Result := true;
    case c of
      'A'..'Z' : v := byte(c)-byte('A');
      'a'..'z' : v := byte(c)-byte('a')+26;
      '0'..'9' : v := byte(c)-byte('0')+52;
      '+'      : v := 62;
      '/'      : v := 63;
      else Result := false;
    end;
  end;

begin
  Result := false;
  Data.Size := 0;
  n := length(s);
  if n=0 then Exit;
  if s[n]=#10 then dec(n);
  if s[n]=#13 then dec(n);
  //if n>76 then Exit;
  //if n=0 then  Exit;
  if (n mod 4)<>0 then Exit;
  n4 := (n-1) div 4;

  for m := 0 to n4  do
  begin
    for k := 1 to 4 do a[k] := s[m*4+k];

    n8 := 3; n6 := 4;

    if m = n4 then
    begin
      if (a[3]='=') and (a[4]='=') then
      begin
        n8:=1; n6:=2;
        a[3]:=#0; a[4]:=#0;
      end else
      if (a[4]='=') then
      begin
        n8 := 2; n6 := 3;
        a[4]:=#0;
      end;
    end;

    FillChar(b[1],4,0);

    for k := 1 to n6 do
    begin
      if not Base64Value(a[k],b[k]) then
      begin
        Data.Size := 0;
        Exit;
      end;
    end;

    d[1] := byte((b[1] shl 2)) + (b[2] shr 4);
    d[2] := byte((b[2] shl 4)) + (b[3] shr 2) ;
    d[3] := byte((b[3] shl 6)) + b[4];
    for k := 1 to n8 do s := s + char(d[k]);
    Data.Write(d[1],n8);
  end;
  Result := true;
end;

class function TVmrUpsGround.PossiblePackages: string;
begin
   Result := 'Package';
end;

class function TVmrUpsDeliveryMethod.PossiblePackages: string;
begin
   Result := 'Package,"UPS Letter","UPS Pak","UPS Tube","UPS Express Box Large","UPS Express Box Medium","UPS Express Box Small"';
end;

class function TVmrUpsThreeDaySelect.PossiblePackages: string;
begin
   Result := 'Package';
end;

end.

