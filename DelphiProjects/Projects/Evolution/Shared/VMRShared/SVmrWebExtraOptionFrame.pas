// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrWebExtraOptionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, DBCtrls, EvUIComponents;

type
  TVMRWebExtraOptionFrame = class(TVmrOptionEditFrame)
    evlabNotes: TevLabel;
    EvDBMemo1: TEvDBMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterVmrClass(TVMRWebExtraOptionFrame);

end.
