inherited VmrDHLAddressedEditFrame: TVmrDHLAddressedEditFrame
  Width = 559
  Height = 371
  inherited evLabel3: TevLabel
    Top = 111
  end
  inherited evLabel4: TevLabel
    Top = 143
  end
  inherited evLabel5: TevLabel
    Top = 143
  end
  inherited evLabel6: TevLabel
    Left = 31
    Top = 202
    Width = 60
    Caption = 'Note/AttnTo'
  end
  object lbPhone: TevLabel [8]
    Left = 62
    Top = 179
    Width = 31
    Height = 13
    Caption = 'Phone'
  end
  inherited edEmailAddr: TevDBEdit
    Top = 279
  end
  inherited evDBEdit3: TevDBEdit
    Top = 111
  end
  inherited evDBEdit4: TevDBEdit
    Top = 143
  end
  inherited evDBLookupCombo1: TevDBLookupCombo
    Top = 143
  end
  inherited EvDBMemo1: TEvDBMemo
    Top = 199
  end
  object evDBEdit6: TevDBEdit [19]
    Left = 96
    Top = 174
    Width = 289
    Height = 21
    DataField = 'PHONE'
    DataSource = dsMain
    TabOrder = 10
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
  end
end
