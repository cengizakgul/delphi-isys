// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrGroupedPaperMediaEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  StdCtrls,
  ExtCtrls, DBCtrls, ISBasicClasses, Mask, wwdbedit, EvUIComponents,
  isUIwwDBEdit;

type
  TVmrGroupedPaperMediaEditFrame = class(TVmrOptionEditFrame)
    evDBRadioGroup1: TevDBRadioGroup;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    lPrintPassword: TevLabel;
    edPrintPassword: TevDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses SVmrClasses;

initialization
  RegisterVmrClass(TVmrGroupedPaperMediaEditFrame);

end.
