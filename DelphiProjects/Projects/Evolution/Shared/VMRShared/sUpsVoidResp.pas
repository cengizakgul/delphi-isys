
{*********************************************************************************************************}
{                                                                                                         }
{                                            XML Data Binding                                             }
{                                                                                                         }
{         Generated on: 6/9/2009 1:51:49 PM                                                               }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\voidresp1.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\voidresp1.xdb   }
{                                                                                                         }
{*********************************************************************************************************}

unit sUpsVoidResp;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUpsVoidShipmentResponseType = interface;
  IXMLResponseType = interface;
  IXMLTransactionReferenceType = interface;
  IXMLStatusType = interface;
  IXMLStatusCodeType = interface;

{ IXMLVoidShipmentResponseType }

  IXMLUpsVoidShipmentResponseType = interface(IXMLNode)
    ['{EC27B756-D104-4F47-9941-39196C23E0C5}']
    { Property Accessors }
    function Get_Response: IXMLResponseType;
    function Get_Status: IXMLStatusType;
    { Methods & Properties }
    property Response: IXMLResponseType read Get_Response;
    property Status: IXMLStatusType read Get_Status;
  end;

{ IXMLResponseType }

  IXMLResponseType = interface(IXMLNode)
    ['{715E60E0-5914-4F0F-B5A2-D53CCB85CC48}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property ResponseStatusCode: Integer read Get_ResponseStatusCode write Set_ResponseStatusCode;
    property ResponseStatusDescription: WideString read Get_ResponseStatusDescription write Set_ResponseStatusDescription;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{D97324B0-AC74-43F7-B8F3-78DB96B43F3D}']
    { Property Accessors }
    function Get_XpciVersion: WideString;
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ IXMLStatusType }

  IXMLStatusType = interface(IXMLNode)
    ['{1D592AAE-3E10-473A-866A-7BE96384542E}']
    { Property Accessors }
    function Get_Code: Integer;
    function Get_Description: WideString;
    function Get_StatusType: IXMLStatusType;
    function Get_StatusCode: IXMLStatusCodeType;
    procedure Set_Code(Value: Integer);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Code: Integer read Get_Code write Set_Code;
    property Description: WideString read Get_Description write Set_Description;
    property StatusType: IXMLStatusType read Get_StatusType;
    property StatusCode: IXMLStatusCodeType read Get_StatusCode;
  end;

{ IXMLStatusCodeType }

  IXMLStatusCodeType = interface(IXMLNode)
    ['{1294D931-404E-4236-B896-6F8AB26B8FA8}']
    { Property Accessors }
    function Get_Code: Integer;
    function Get_Description: WideString;
    procedure Set_Code(Value: Integer);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Code: Integer read Get_Code write Set_Code;
    property Description: WideString read Get_Description write Set_Description;
  end;

{ Forward Decls }

  TXMLVoidShipmentResponseType = class;
  TXMLResponseType = class;
  TXMLTransactionReferenceType = class;
  TXMLStatusType = class;
  TXMLStatusCodeType = class;

{ TXMLVoidShipmentResponseType }

  TXMLVoidShipmentResponseType = class(TXMLNode, IXMLUpsVoidShipmentResponseType)
  protected
    { IXMLVoidShipmentResponseType }
    function Get_Response: IXMLResponseType;
    function Get_Status: IXMLStatusType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLResponseType }

  TXMLResponseType = class(TXMLNode, IXMLResponseType)
  protected
    { IXMLResponseType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_XpciVersion: WideString;
    procedure Set_XpciVersion(Value: WideString);
  end;

{ TXMLStatusType }

  TXMLStatusType = class(TXMLNode, IXMLStatusType)
  protected
    { IXMLStatusType }
    function Get_Code: Integer;
    function Get_Description: WideString;
    function Get_StatusType: IXMLStatusType;
    function Get_StatusCode: IXMLStatusCodeType;
    procedure Set_Code(Value: Integer);
    procedure Set_Description(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLStatusCodeType }

  TXMLStatusCodeType = class(TXMLNode, IXMLStatusCodeType)
  protected
    { IXMLStatusCodeType }
    function Get_Code: Integer;
    function Get_Description: WideString;
    procedure Set_Code(Value: Integer);
    procedure Set_Description(Value: WideString);
  end;

{ Global Functions }

function GetVoidShipmentResponse(Doc: IXMLDocument): IXMLUpsVoidShipmentResponseType;
function LoadVoidShipmentResponse(const FileName: WideString): IXMLUpsVoidShipmentResponseType;
function NewVoidShipmentResponse: IXMLUpsVoidShipmentResponseType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetVoidShipmentResponse(Doc: IXMLDocument): IXMLUpsVoidShipmentResponseType;
begin
  Result := Doc.GetDocBinding('VoidShipmentResponse', TXMLVoidShipmentResponseType, TargetNamespace) as IXMLUpsVoidShipmentResponseType;
end;

function LoadVoidShipmentResponse(const FileName: WideString): IXMLUpsVoidShipmentResponseType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('VoidShipmentResponse', TXMLVoidShipmentResponseType, TargetNamespace) as IXMLUpsVoidShipmentResponseType;
end;

function NewVoidShipmentResponse: IXMLUpsVoidShipmentResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('VoidShipmentResponse', TXMLVoidShipmentResponseType, TargetNamespace) as IXMLUpsVoidShipmentResponseType;
end;

{ TXMLVoidShipmentResponseType }

procedure TXMLVoidShipmentResponseType.AfterConstruction;
begin
  RegisterChildNode('Response', TXMLResponseType);
  RegisterChildNode('Status', TXMLStatusType);
  inherited;
end;

function TXMLVoidShipmentResponseType.Get_Response: IXMLResponseType;
begin
  Result := ChildNodes['Response'] as IXMLResponseType;
end;

function TXMLVoidShipmentResponseType.Get_Status: IXMLStatusType;
begin
  Result := ChildNodes['Status'] as IXMLStatusType;
end;

{ TXMLResponseType }

procedure TXMLResponseType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  inherited;
end;

function TXMLResponseType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLResponseType.Get_ResponseStatusCode: Integer;
begin
  Result := ChildNodes['ResponseStatusCode'].NodeValue;
end;

procedure TXMLResponseType.Set_ResponseStatusCode(Value: Integer);
begin
  ChildNodes['ResponseStatusCode'].NodeValue := Value;
end;

function TXMLResponseType.Get_ResponseStatusDescription: WideString;
begin
  Result := ChildNodes['ResponseStatusDescription'].Text;
end;

procedure TXMLResponseType.Set_ResponseStatusDescription(Value: WideString);
begin
  ChildNodes['ResponseStatusDescription'].NodeValue := Value;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

{ TXMLStatusType }

procedure TXMLStatusType.AfterConstruction;
begin
  RegisterChildNode('StatusType', TXMLStatusType);
  RegisterChildNode('StatusCode', TXMLStatusCodeType);
  inherited;
end;

function TXMLStatusType.Get_Code: Integer;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

procedure TXMLStatusType.Set_Code(Value: Integer);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLStatusType.Get_Description: WideString;
begin
  Result := ChildNodes['Description'].Text;
end;

procedure TXMLStatusType.Set_Description(Value: WideString);
begin
  ChildNodes['Description'].NodeValue := Value;
end;

function TXMLStatusType.Get_StatusType: IXMLStatusType;
begin
  Result := ChildNodes['StatusType'] as IXMLStatusType;
end;

function TXMLStatusType.Get_StatusCode: IXMLStatusCodeType;
begin
  Result := ChildNodes['StatusCode'] as IXMLStatusCodeType;
end;

{ TXMLStatusCodeType }

function TXMLStatusCodeType.Get_Code: Integer;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

procedure TXMLStatusCodeType.Set_Code(Value: Integer);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLStatusCodeType.Get_Description: WideString;
begin
  Result := ChildNodes['Description'].Text;
end;

procedure TXMLStatusCodeType.Set_Description(Value: WideString);
begin
  ChildNodes['Description'].NodeValue := Value;
end;

end.