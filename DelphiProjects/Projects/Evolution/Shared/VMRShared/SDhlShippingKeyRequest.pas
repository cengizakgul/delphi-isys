
{*****************************************************************************************************************}
{                                                                                                                 }
{                                                XML Data Binding                                                 }
{                                                                                                                 }
{         Generated on: 11/1/2004 4:39:25 PM                                                                      }
{       Generated from: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\RegisterAccountRequest.xdr   }
{   Settings stored in: C:\ISystems\Evolution Support\Specs\Delivery Module\DHL\XDRs\RegisterAccountRequest.xdb   }
{                                                                                                                 }
{*****************************************************************************************************************}

unit SDhlShippingKeyRequest;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRegisterType = interface;
  IXMLAccountNbrType = interface;
  IXMLPostalCodeType = interface;

{ IXMLRegisterType }

  IXMLRegisterType = interface(IXMLNode)
    ['{131090A8-1C68-4AA5-87AF-F934BCE82137}']
    { Property Accessors }
    function Get_Version: string;
    function Get_Action: string;
    function Get_AccountNbr: IXMLAccountNbrType;
    function Get_PostalCode: IXMLPostalCodeType;
    procedure Set_Version(Value: string);
    procedure Set_Action(Value: string);
    { Methods & Properties }
    property Version: string read Get_Version write Set_Version;
    property Action: string read Get_Action write Set_Action;
    property AccountNbr: IXMLAccountNbrType read Get_AccountNbr;
    property PostalCode: IXMLPostalCodeType read Get_PostalCode;
  end;

{ IXMLAccountNbrType }

  IXMLAccountNbrType = interface(IXMLNode)
    ['{7E6CC0D3-8846-454F-ACA3-2166C3A5EE7D}']
  end;

{ IXMLPostalCodeType }

  IXMLPostalCodeType = interface(IXMLNode)
    ['{9647C290-1868-421E-8219-6C2E08519F07}']
  end;

{ Forward Decls }

  TXMLRegisterType = class;
  TXMLAccountNbrType = class;
  TXMLPostalCodeType = class;

{ TXMLRegisterType }

  TXMLRegisterType = class(TXMLNode, IXMLRegisterType)
  protected
    { IXMLRegisterType }
    function Get_Version: string;
    function Get_Action: string;
    function Get_AccountNbr: IXMLAccountNbrType;
    function Get_PostalCode: IXMLPostalCodeType;
    procedure Set_Version(Value: string);
    procedure Set_Action(Value: string);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAccountNbrType }

  TXMLAccountNbrType = class(TXMLNode, IXMLAccountNbrType)
  protected
    { IXMLAccountNbrType }
  end;

{ TXMLPostalCodeType }

  TXMLPostalCodeType = class(TXMLNode, IXMLPostalCodeType)
  protected
    { IXMLPostalCodeType }
  end;

{ Global Functions }

function GetRegister(Doc: IXMLDocument): IXMLRegisterType;
function LoadRegister(const FileName: WideString): IXMLRegisterType;
function NewRegister: IXMLRegisterType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRegister(Doc: IXMLDocument): IXMLRegisterType;
begin
  Result := Doc.GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

function LoadRegister(const FileName: WideString): IXMLRegisterType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

function NewRegister: IXMLRegisterType;
begin
  Result := NewXMLDocument.GetDocBinding('Register', TXMLRegisterType, TargetNamespace) as IXMLRegisterType;
end;

{ TXMLRegisterType }

procedure TXMLRegisterType.AfterConstruction;
begin
  RegisterChildNode('AccountNbr', TXMLAccountNbrType);
  RegisterChildNode('PostalCode', TXMLPostalCodeType);
  inherited;
end;

function TXMLRegisterType.Get_Version: string;
begin
  Result := AttributeNodes['version'].Text;
end;

procedure TXMLRegisterType.Set_Version(Value: string);
begin
  SetAttribute('version', Value);
end;

function TXMLRegisterType.Get_Action: string;
begin
  Result := AttributeNodes['action'].Text;
end;

procedure TXMLRegisterType.Set_Action(Value: string);
begin
  SetAttribute('action', Value);
end;

function TXMLRegisterType.Get_AccountNbr: IXMLAccountNbrType;
begin
  Result := ChildNodes['AccountNbr'] as IXMLAccountNbrType;
end;

function TXMLRegisterType.Get_PostalCode: IXMLPostalCodeType;
begin
  Result := ChildNodes['PostalCode'] as IXMLPostalCodeType;
end;

{ TXMLAccountNbrType }

{ TXMLPostalCodeType }

end.