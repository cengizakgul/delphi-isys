
{***************************************************************************************************************}
{                                                                                                               }
{                                               XML Data Binding                                                }
{                                                                                                               }
{         Generated on: 6/8/2009 7:12:17 PM                                                                     }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipconfirmreq1.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipconfirmreq1.xdb   }
{                                                                                                               }
{***************************************************************************************************************}

unit sUpsShipConfirmReq;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUpsShipmentConfirmRequestType = interface;
  IXMLRequestType = interface;
  IXMLTransactionReferenceType = interface;
  IXMLLabelSpecificationType = interface;
  IXMLLabelPrintMethodType = interface;
  IXMLLabelImageFormatType = interface;
  IXMLLabelStockSizeType = interface;
  IXMLShipmentType = interface;
  IXMLShipperType = interface;
  IXMLPhoneNumberType = interface;
  IXMLStructuredPhoneNumberType = interface;
  IXMLAddressType = interface;
  IXMLShipToType = interface;
  IXMLShipFromType = interface;
  IXMLPaymentInformationType = interface;
  IXMLPrepaidType = interface;
  IXMLBillShipperType = interface;
  IXMLServiceType = interface;
  IXMLShipmentServiceOptionsType = interface;
  IXMLShipmentNotificationType = interface;
  IXMLEMailMessageType = interface;
  IXMLPackageType = interface;
  IXMLPackageTypeList = interface;
  IXMLPackagingType = interface;
  IXMLPackageWeightType = interface;
  IXMLUnitOfMeasurementType = interface;
  IXMLReferenceNumberType = interface;
  IXMLPackageServiceOptionsType = interface;
  IXMLCODType = interface;
  IXMLCODAmountType = interface;
  IXMLInsuredValueType = interface;

{ IXMLUpsShipmentConfirmRequestType }

  IXMLUpsShipmentConfirmRequestType = interface(IXMLNode)
    ['{4C8AAA24-8D5F-4A82-9A42-D1B92F0E74FD}']
    { Property Accessors }
    function Get_Request: IXMLRequestType;
    function Get_LabelSpecification: IXMLLabelSpecificationType;
    function Get_Shipment: IXMLShipmentType;
    { Methods & Properties }
    property Request: IXMLRequestType read Get_Request;
    property LabelSpecification: IXMLLabelSpecificationType read Get_LabelSpecification;
    property Shipment: IXMLShipmentType read Get_Shipment;
  end;

{ IXMLRequestType }

  IXMLRequestType = interface(IXMLNode)
    ['{B0405D7D-79F1-436F-AC34-21010ED7A9F8}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: WideString;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: WideString);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property RequestAction: WideString read Get_RequestAction write Set_RequestAction;
    property RequestOption: WideString read Get_RequestOption write Set_RequestOption;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{267196B9-26F2-4BC0-9462-605298B0C44D}']
    { Property Accessors }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property CustomerContext: WideString read Get_CustomerContext write Set_CustomerContext;
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ IXMLLabelSpecificationType }

  IXMLLabelSpecificationType = interface(IXMLNode)
    ['{6D9C7842-3ADA-4D6B-824A-DCEEB7534499}']
    { Property Accessors }
    function Get_LabelPrintMethod: IXMLLabelPrintMethodType;
    function Get_LabelImageFormat: IXMLLabelImageFormatType;
    function Get_LabelStockSize: IXMLLabelStockSizeType;
    { Methods & Properties }
    property LabelPrintMethod: IXMLLabelPrintMethodType read Get_LabelPrintMethod;
    property LabelImageFormat: IXMLLabelImageFormatType read Get_LabelImageFormat;
    property LabelStockSize: IXMLLabelStockSizeType read Get_LabelStockSize;
  end;

{ IXMLLabelPrintMethodType }

  IXMLLabelPrintMethodType = interface(IXMLNode)
    ['{41BBE963-A8C6-4E13-8895-C3925492BB8E}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ IXMLLabelImageFormatType }

  IXMLLabelImageFormatType = interface(IXMLNode)
    ['{41BBE963-A8C6-4E13-8895-C3925492BB8E}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ IXMLLabelStockSizeType }

  IXMLLabelStockSizeType = interface(IXMLNode)
    ['{5E03C4BB-69C3-41CB-8DD9-E581F57BB604}']
    { Property Accessors }
    function Get_Height: Integer;
    function Get_Width: Integer;
    procedure Set_Height(Value: Integer);
    procedure Set_Width(Value: Integer);
    { Methods & Properties }
    property Height: Integer read Get_Height write Set_Height;
    property Width: Integer read Get_Width write Set_Width;
  end;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{E2F27877-852E-428B-BA4B-127D9E9A0590}']
    { Property Accessors }
    function Get_Shipper: IXMLShipperType;
    function Get_ShipTo: IXMLShipToType;
    function Get_ShipFrom: IXMLShipFromType;
    function Get_PaymentInformation: IXMLPaymentInformationType;
    function Get_Service: IXMLServiceType;
    function Get_ShipmentServiceOptions: IXMLShipmentServiceOptionsType;
    function Get_Package: IXMLPackageTypeList;
    { Methods & Properties }
    property Shipper: IXMLShipperType read Get_Shipper;
    property ShipTo: IXMLShipToType read Get_ShipTo;
    property ShipFrom: IXMLShipFromType read Get_ShipFrom;
    property PaymentInformation: IXMLPaymentInformationType read Get_PaymentInformation;
    property Service: IXMLServiceType read Get_Service;
    property ShipmentServiceOptions: IXMLShipmentServiceOptionsType read Get_ShipmentServiceOptions;
    property Package: IXMLPackageTypeList read Get_Package;
  end;

{ IXMLShipperType }

  IXMLShipperType = interface(IXMLNode)
    ['{AA74E2D3-ADAE-4197-9591-3DC0B75E00A6}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;

    function Get_ShipperNumber: WideString;
    function Get_Address: IXMLAddressType;
    procedure Set_Name(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
    procedure Set_ShipperNumber(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
    property AttentionName: WideString read Get_AttentionName write Set_AttentionName;
    property PhoneNumber: IXMLPhoneNumberType read Get_PhoneNumber;
    property ShipperNumber: WideString read Get_ShipperNumber write Set_ShipperNumber;
    property Address: IXMLAddressType read Get_Address;
  end;

{ IXMLPhoneNumberType }

  IXMLPhoneNumberType = interface(IXMLNode)
    ['{AF5D2D88-3F3D-4E77-A3B4-504728E4B5E2}']
    { Property Accessors }
    function Get_StructuredPhoneNumber: IXMLStructuredPhoneNumberType;
    function Get_UnStructuredPhoneNumber: string;
    procedure Set_UnStructuredPhoneNumber(Value: string);

    { Methods & Properties }
    property StructuredPhoneNumber: IXMLStructuredPhoneNumberType read Get_StructuredPhoneNumber;
    property UnStructuredPhoneNumber: string read Get_UnStructuredPhoneNumber write Set_UnStructuredPhoneNumber;

  end;

{ IXMLStructuredPhoneNumberType }

  IXMLStructuredPhoneNumberType = interface(IXMLNode)
    ['{34495139-AEF0-4282-8CC6-D0B5D61D83C3}']
    { Property Accessors }
    function Get_PhoneDialPlanNumber: Integer;
    function Get_PhoneLineNumber: Integer;
    function Get_PhoneExtension: Integer;
    procedure Set_PhoneDialPlanNumber(Value: Integer);
    procedure Set_PhoneLineNumber(Value: Integer);
    procedure Set_PhoneExtension(Value: Integer);
    { Methods & Properties }
    property PhoneDialPlanNumber: Integer read Get_PhoneDialPlanNumber write Set_PhoneDialPlanNumber;
    property PhoneLineNumber: Integer read Get_PhoneLineNumber write Set_PhoneLineNumber;
    property PhoneExtension: Integer read Get_PhoneExtension write Set_PhoneExtension;
  end;

{ IXMLAddressType }

  IXMLAddressType = interface(IXMLNode)
    ['{B5C529B7-B0CA-4196-8EE1-48834CACF0E0}']
    { Property Accessors }
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_StateProvinceCode: WideString;
    function Get_PostalCode: String;
    function Get_CountryCode: WideString;
    function Get_ResidentialAddress: WideString;
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_StateProvinceCode(Value: WideString);
    procedure Set_PostalCode(Value: String);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_ResidentialAddress(Value: WideString);
    { Methods & Properties }
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property City: WideString read Get_City write Set_City;
    property StateProvinceCode: WideString read Get_StateProvinceCode write Set_StateProvinceCode;
    property PostalCode: string read Get_PostalCode write Set_PostalCode;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property ResidentialAddress: WideString read Get_ResidentialAddress write Set_ResidentialAddress;
  end;

{ IXMLShipToType }

  IXMLShipToType = interface(IXMLNode)
    ['{6AA77471-2E82-4D09-90FA-7EEBC605A577}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;
    function Get_Address: IXMLAddressType;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property AttentionName: WideString read Get_AttentionName write Set_AttentionName;
    property PhoneNumber: IXMLPhoneNumberType read Get_PhoneNumber;
    property Address: IXMLAddressType read Get_Address;
  end;

{ IXMLShipFromType }

  IXMLShipFromType = interface(IXMLNode)
    ['{53D8FC09-09E1-46E2-AE6A-F8FB3DE27281}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;
    function Get_Address: IXMLAddressType;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property AttentionName: WideString read Get_AttentionName write Set_AttentionName;
    property PhoneNumber: IXMLPhoneNumberType read Get_PhoneNumber;
    property Address: IXMLAddressType read Get_Address;
  end;

{ IXMLPaymentInformationType }

  IXMLPaymentInformationType = interface(IXMLNode)
    ['{839F10C0-3909-4860-836E-24A46FD25D12}']
    { Property Accessors }
    function Get_Prepaid: IXMLPrepaidType;
    { Methods & Properties }
    property Prepaid: IXMLPrepaidType read Get_Prepaid;
  end;

{ IXMLPrepaidType }

  IXMLPrepaidType = interface(IXMLNode)
    ['{8C4444E0-B2F8-41DC-8AAC-4E77EB7E7889}']
    { Property Accessors }
    function Get_BillShipper: IXMLBillShipperType;
    { Methods & Properties }
    property BillShipper: IXMLBillShipperType read Get_BillShipper;
  end;

{ IXMLBillShipperType }

  IXMLBillShipperType = interface(IXMLNode)
    ['{6882C253-6A9C-44A0-860F-0B6D074E42BF}']
    { Property Accessors }
    function Get_AccountNumber: WideString;
    procedure Set_AccountNumber(Value: WideString);
    { Methods & Properties }
    property AccountNumber: WideString read Get_AccountNumber write Set_AccountNumber;
  end;

{ IXMLServiceType }

  IXMLServiceType = interface(IXMLNode)
    ['{7AFDBF4A-3590-4B7E-9FEA-4A49857D0C9B}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLShipmentServiceOptionsType }

  IXMLShipmentServiceOptionsType = interface(IXMLNodeCollection)
    ['{E51429B1-7F5D-4C03-8F2A-6D43596F4B4B}']
    { Property Accessors }
    function Get_ShipmentNotification(Index: Integer): IXMLShipmentNotificationType;
    { Methods & Properties }
    function Add: IXMLShipmentNotificationType;
    function Insert(const Index: Integer): IXMLShipmentNotificationType;
    property ShipmentNotification[Index: Integer]: IXMLShipmentNotificationType read Get_ShipmentNotification; default;
  end;

{ IXMLShipmentNotificationType }

  IXMLShipmentNotificationType = interface(IXMLNode)
    ['{7D14FF71-7C85-495E-8435-75A7F4EB95DB}']
    { Property Accessors }
    function Get_EMailMessage: IXMLEMailMessageType;
    { Methods & Properties }
    property EMailMessage: IXMLEMailMessageType read Get_EMailMessage;
  end;

{ IXMLEMailMessageType }

  IXMLEMailMessageType = interface(IXMLNode)
    ['{ED44A66C-0166-43FE-BF51-70A791A5F617}']
    { Property Accessors }
    function Get_EMailAddress: WideString;
    function Get_Memo: WideString;
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Memo(Value: WideString);
    { Methods & Properties }
    property EMailAddress: WideString read Get_EMailAddress write Set_EMailAddress;
    property Memo: WideString read Get_Memo write Set_Memo;
  end;

{ IXMLPackageType }

  IXMLPackageType = interface(IXMLNode)
    ['{4E9EE46C-3ACE-48FD-B951-87107123E2D9}']
    { Property Accessors }
    function Get_Description: WideString;
    function Get_PackagingType: IXMLPackagingType;
    function Get_PackageWeight: IXMLPackageWeightType;
    function Get_ReferenceNumber: IXMLReferenceNumberType;
    function Get_PackageServiceOptions: IXMLPackageServiceOptionsType;
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Description: WideString read Get_Description write Set_Description;
    property PackagingType: IXMLPackagingType read Get_PackagingType;
    property PackageWeight: IXMLPackageWeightType read Get_PackageWeight;
    property ReferenceNumber: IXMLReferenceNumberType read Get_ReferenceNumber;
    property PackageServiceOptions: IXMLPackageServiceOptionsType read Get_PackageServiceOptions;
  end;

{ IXMLPackageTypeList }

  IXMLPackageTypeList = interface(IXMLNodeCollection)
    ['{BEC299BE-1EF0-42F0-BE15-6B22EFE652CB}']
    { Methods & Properties }
    function Add: IXMLPackageType;
    function Insert(const Index: Integer): IXMLPackageType;
    function Get_Item(Index: Integer): IXMLPackageType;
    property Items[Index: Integer]: IXMLPackageType read Get_Item; default;
  end;

{ IXMLPackagingType }

  IXMLPackagingType = interface(IXMLNode)
    ['{A66E873F-DDC9-4583-8009-4B322FBA187E}']
    { Property Accessors }
    function Get_Code: string;
    procedure Set_Code(Value: string);
    { Methods & Properties }
    property Code: string read Get_Code write Set_Code;
  end;

{ IXMLPackageWeightType }

  IXMLPackageWeightType = interface(IXMLNode)
    ['{577D2606-907C-49D3-A90B-3801050D2C90}']
    { Property Accessors }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: real;
    procedure Set_Weight(Value: real);
    { Methods & Properties }
    property UnitOfMeasurement: IXMLUnitOfMeasurementType read Get_UnitOfMeasurement;
    property Weight: Real read Get_Weight write Set_Weight;
  end;

{ IXMLUnitOfMeasurementType }

  IXMLUnitOfMeasurementType = interface(IXMLNode)
    ['{02FBB3FD-513D-44BD-8DBC-F0F59F7546D5}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ IXMLReferenceNumberType }

  IXMLReferenceNumberType = interface(IXMLNode)
    ['{52094282-0A89-46FC-AFD0-4E1101432BD1}']
    { Property Accessors }
    function Get_Code: WideString;
    function Get_Value: WideString;
    procedure Set_Code(Value: WideString);
    procedure Set_Value(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
    property Value: WideString read Get_Value write Set_Value;
  end;

{ IXMLPackageServiceOptionsType }

  IXMLPackageServiceOptionsType = interface(IXMLNode)
    ['{A2C15585-FB92-4510-BA60-A187B6489313}']
    { Property Accessors }
    function Get_COD: IXMLCODType;
    function Get_InsuredValue: IXMLInsuredValueType;
    { Methods & Properties }
    property COD: IXMLCODType read Get_COD;
    property InsuredValue: IXMLInsuredValueType read Get_InsuredValue;
  end;

{ IXMLCODType }

  IXMLCODType = interface(IXMLNode)
    ['{D9433930-48FC-4C72-AE36-95AE3ADB8B99}']
    { Property Accessors }
    function Get_CODFundsCode: Integer;
    function Get_CODCode: Integer;
    function Get_CODAmount: IXMLCODAmountType;
    procedure Set_CODFundsCode(Value: Integer);
    procedure Set_CODCode(Value: Integer);
    { Methods & Properties }
    property CODFundsCode: Integer read Get_CODFundsCode write Set_CODFundsCode;
    property CODCode: Integer read Get_CODCode write Set_CODCode;
    property CODAmount: IXMLCODAmountType read Get_CODAmount;
  end;

{ IXMLCODAmountType }

  IXMLCODAmountType = interface(IXMLNode)
    ['{E09D0A47-9B48-40CC-BC8E-10D4F733635A}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: Integer;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: Integer);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: Integer read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLInsuredValueType }

  IXMLInsuredValueType = interface(IXMLNode)
    ['{448814FB-EACF-4BAB-AB5D-86A03479CE68}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ Forward Decls }

  TXMLShipmentConfirmRequestType = class;
  TXMLRequestType = class;
  TXMLTransactionReferenceType = class;
  TXMLLabelSpecificationType = class;
  TXMLLabelPrintMethodType = class;
  TXMLLabelImageFormatType = class;
  TXMLLabelStockSizeType = class;
  TXMLShipmentType = class;
  TXMLShipperType = class;
  TXMLPhoneNumberType = class;
  TXMLStructuredPhoneNumberType = class;
  TXMLAddressType = class;
  TXMLShipToType = class;
  TXMLShipFromType = class;
  TXMLPaymentInformationType = class;
  TXMLPrepaidType = class;
  TXMLBillShipperType = class;
  TXMLServiceType = class;
  TXMLShipmentServiceOptionsType = class;
  TXMLShipmentNotificationType = class;
  TXMLEMailMessageType = class;
  TXMLPackageType = class;
  TXMLPackageTypeList = class;
  TXMLPackagingType = class;
  TXMLPackageWeightType = class;
  TXMLUnitOfMeasurementType = class;
  TXMLReferenceNumberType = class;
  TXMLPackageServiceOptionsType = class;
  TXMLCODType = class;
  TXMLCODAmountType = class;
  TXMLInsuredValueType = class;

{ TXMLShipmentConfirmRequestType }

  TXMLShipmentConfirmRequestType = class(TXMLNode, IXMLUpsShipmentConfirmRequestType)
  protected
    { IXMLUpsShipmentConfirmRequestType }
    function Get_Request: IXMLRequestType;
    function Get_LabelSpecification: IXMLLabelSpecificationType;
    function Get_Shipment: IXMLShipmentType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRequestType }

  TXMLRequestType = class(TXMLNode, IXMLRequestType)
  protected
    { IXMLRequestType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_RequestAction: WideString;
    function Get_RequestOption: WideString;
    procedure Set_RequestAction(Value: WideString);
    procedure Set_RequestOption(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
  end;

{ TXMLLabelSpecificationType }

  TXMLLabelSpecificationType = class(TXMLNode, IXMLLabelSpecificationType)
  protected
    { IXMLLabelSpecificationType }
    function Get_LabelPrintMethod: IXMLLabelPrintMethodType;
    function Get_LabelStockSize: IXMLLabelStockSizeType;
    function Get_LabelImageFormat: IXMLLabelImageFormatType;

  public
    procedure AfterConstruction; override;
  end;

{ TXMLLabelPrintMethodType }

  TXMLLabelPrintMethodType = class(TXMLNode, IXMLLabelPrintMethodType)
  protected
    { IXMLLabelPrintMethodType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;

{ TXMLLabelImageFormatType }

  TXMLLabelImageFormatType = class(TXMLNode, IXMLLabelImageFormatType)
  protected
    { IXMLLabelPrintMethodType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;


{ TXMLLabelStockSizeType }

  TXMLLabelStockSizeType = class(TXMLNode, IXMLLabelStockSizeType)
  protected
    { IXMLLabelStockSizeType }
    function Get_Height: Integer;
    function Get_Width: Integer;
    procedure Set_Height(Value: Integer);
    procedure Set_Width(Value: Integer);
  end;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  private
    FPackage: IXMLPackageTypeList;
  protected
    { IXMLShipmentType }
    function Get_Shipper: IXMLShipperType;
    function Get_ShipTo: IXMLShipToType;
    function Get_ShipFrom: IXMLShipFromType;
    function Get_PaymentInformation: IXMLPaymentInformationType;
    function Get_Service: IXMLServiceType;
    function Get_ShipmentServiceOptions: IXMLShipmentServiceOptionsType;
    function Get_Package: IXMLPackageTypeList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipperType }

  TXMLShipperType = class(TXMLNode, IXMLShipperType)
  protected
    { IXMLShipperType }
    function Get_Name: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;
    function Get_ShipperNumber: WideString;
    function Get_Address: IXMLAddressType;
    procedure Set_Name(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
    procedure Set_ShipperNumber(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPhoneNumberType }

  TXMLPhoneNumberType = class(TXMLNode, IXMLPhoneNumberType)
  protected
    { IXMLPhoneNumberType }
    function Get_StructuredPhoneNumber: IXMLStructuredPhoneNumberType;
    procedure Set_UnStructuredPhoneNumber(Value: string);
    function Get_UnStructuredPhoneNumber: string;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLStructuredPhoneNumberType }

  TXMLStructuredPhoneNumberType = class(TXMLNode, IXMLStructuredPhoneNumberType)
  protected
    { IXMLStructuredPhoneNumberType }
    function Get_PhoneDialPlanNumber: Integer;
    function Get_PhoneLineNumber: Integer;
    function Get_PhoneExtension: Integer;
    procedure Set_PhoneDialPlanNumber(Value: Integer);
    procedure Set_PhoneLineNumber(Value: Integer);
    procedure Set_PhoneExtension(Value: Integer);
  end;

{ TXMLAddressType }

  TXMLAddressType = class(TXMLNode, IXMLAddressType)
  protected
    { IXMLAddressType }
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_StateProvinceCode: WideString;
    function Get_PostalCode: string;
    function Get_CountryCode: WideString;
    function Get_ResidentialAddress: WideString;
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_StateProvinceCode(Value: WideString);
    procedure Set_PostalCode(Value: string);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_ResidentialAddress(Value: WideString);
  end;

{ TXMLShipToType }

  TXMLShipToType = class(TXMLNode, IXMLShipToType)
  protected
    { IXMLShipToType }
    function Get_CompanyName: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;
    function Get_Address: IXMLAddressType;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipFromType }

  TXMLShipFromType = class(TXMLNode, IXMLShipFromType)
  protected
    { IXMLShipFromType }
    function Get_CompanyName: WideString;
    function Get_AttentionName: WideString;
    function Get_PhoneNumber: IXMLPhoneNumberType;
    function Get_Address: IXMLAddressType;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_AttentionName(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaymentInformationType }

  TXMLPaymentInformationType = class(TXMLNode, IXMLPaymentInformationType)
  protected
    { IXMLPaymentInformationType }
    function Get_Prepaid: IXMLPrepaidType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPrepaidType }

  TXMLPrepaidType = class(TXMLNode, IXMLPrepaidType)
  protected
    { IXMLPrepaidType }
    function Get_BillShipper: IXMLBillShipperType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBillShipperType }

  TXMLBillShipperType = class(TXMLNode, IXMLBillShipperType)
  protected
    { IXMLBillShipperType }
    function Get_AccountNumber: WideString;
    procedure Set_AccountNumber(Value: WideString);
  end;

{ TXMLServiceType }

  TXMLServiceType = class(TXMLNode, IXMLServiceType)
  protected
    { IXMLServiceType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLShipmentServiceOptionsType }

  TXMLShipmentServiceOptionsType = class(TXMLNodeCollection, IXMLShipmentServiceOptionsType)
  protected
    { IXMLShipmentServiceOptionsType }
    function Get_ShipmentNotification(Index: Integer): IXMLShipmentNotificationType;
    function Add: IXMLShipmentNotificationType;
    function Insert(const Index: Integer): IXMLShipmentNotificationType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipmentNotificationType }

  TXMLShipmentNotificationType = class(TXMLNode, IXMLShipmentNotificationType)
  protected
    { IXMLShipmentNotificationType }
    function Get_EMailMessage: IXMLEMailMessageType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEMailMessageType }

  TXMLEMailMessageType = class(TXMLNode, IXMLEMailMessageType)
  protected
    { IXMLEMailMessageType }
    function Get_EMailAddress: WideString;
    function Get_Memo: WideString;
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Memo(Value: WideString);
  end;

{ TXMLPackageType }

  TXMLPackageType = class(TXMLNode, IXMLPackageType)
  protected
    { IXMLPackageType }
    function Get_Description: WideString;
    function Get_PackagingType: IXMLPackagingType;
    function Get_PackageWeight: IXMLPackageWeightType;
    function Get_ReferenceNumber: IXMLReferenceNumberType;
    function Get_PackageServiceOptions: IXMLPackageServiceOptionsType;
    procedure Set_Description(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageTypeList }

  TXMLPackageTypeList = class(TXMLNodeCollection, IXMLPackageTypeList)
  protected
    { IXMLPackageTypeList }
    function Add: IXMLPackageType;
    function Insert(const Index: Integer): IXMLPackageType;
    function Get_Item(Index: Integer): IXMLPackageType;
  end;

{ TXMLPackagingType }

  TXMLPackagingType = class(TXMLNode, IXMLPackagingType)
  protected
    { IXMLPackagingType }
    function Get_Code: string;
    procedure Set_Code(Value: string);
  end;

{ TXMLPackageWeightType }

  TXMLPackageWeightType = class(TXMLNode, IXMLPackageWeightType)
  protected
    { IXMLPackageWeightType }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: real;
    procedure Set_Weight(Value: real);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUnitOfMeasurementType }

  TXMLUnitOfMeasurementType = class(TXMLNode, IXMLUnitOfMeasurementType)
  protected
    { IXMLUnitOfMeasurementType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;

{ TXMLReferenceNumberType }

  TXMLReferenceNumberType = class(TXMLNode, IXMLReferenceNumberType)
  protected
    { IXMLReferenceNumberType }
    function Get_Code: WideString;
    function Get_Value: WideString;
    procedure Set_Code(Value: WideString);
    procedure Set_Value(Value: WideString);
  end;

{ TXMLPackageServiceOptionsType }

  TXMLPackageServiceOptionsType = class(TXMLNode, IXMLPackageServiceOptionsType)
  protected
    { IXMLPackageServiceOptionsType }
    function Get_COD: IXMLCODType;
    function Get_InsuredValue: IXMLInsuredValueType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCODType }

  TXMLCODType = class(TXMLNode, IXMLCODType)
  protected
    { IXMLCODType }
    function Get_CODFundsCode: Integer;
    function Get_CODCode: Integer;
    function Get_CODAmount: IXMLCODAmountType;
    procedure Set_CODFundsCode(Value: Integer);
    procedure Set_CODCode(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCODAmountType }

  TXMLCODAmountType = class(TXMLNode, IXMLCODAmountType)
  protected
    { IXMLCODAmountType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: Integer;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: Integer);
  end;

{ TXMLInsuredValueType }

  TXMLInsuredValueType = class(TXMLNode, IXMLInsuredValueType)
  protected
    { IXMLInsuredValueType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ Global Functions }

function GetUpsShipmentConfirmRequest(Doc: IXMLDocument): IXMLUpsShipmentConfirmRequestType;
function LoadUpsShipmentConfirmRequest(const FileName: WideString): IXMLUpsShipmentConfirmRequestType;
function NewUpsShipmentConfirmRequest: IXMLUpsShipmentConfirmRequestType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetUpsShipmentConfirmRequest(Doc: IXMLDocument): IXMLUpsShipmentConfirmRequestType;
begin
  Result := Doc.GetDocBinding('ShipmentConfirmRequest', TXMLShipmentConfirmRequestType, TargetNamespace) as IXMLUpsShipmentConfirmRequestType;
end;

function LoadUpsShipmentConfirmRequest(const FileName: WideString): IXMLUpsShipmentConfirmRequestType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('ShipmentConfirmRequest', TXMLShipmentConfirmRequestType, TargetNamespace) as IXMLUpsShipmentConfirmRequestType;
end;

function NewUpsShipmentConfirmRequest: IXMLUpsShipmentConfirmRequestType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentConfirmRequest', TXMLShipmentConfirmRequestType, TargetNamespace) as IXMLUpsShipmentConfirmRequestType;
end;

{ TXMLShipmentConfirmRequestType }

procedure TXMLShipmentConfirmRequestType.AfterConstruction;
begin
  RegisterChildNode('Request', TXMLRequestType);
  RegisterChildNode('LabelSpecification', TXMLLabelSpecificationType);
  RegisterChildNode('Shipment', TXMLShipmentType);
  inherited;
end;

function TXMLShipmentConfirmRequestType.Get_Request: IXMLRequestType;
begin
  Result := ChildNodes['Request'] as IXMLRequestType;
end;

function TXMLShipmentConfirmRequestType.Get_LabelSpecification: IXMLLabelSpecificationType;
begin
  Result := ChildNodes['LabelSpecification'] as IXMLLabelSpecificationType;
end;

function TXMLShipmentConfirmRequestType.Get_Shipment: IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as IXMLShipmentType;
end;

{ TXMLRequestType }

procedure TXMLRequestType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  inherited;
end;

function TXMLRequestType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLRequestType.Get_RequestAction: WideString;
begin
  Result := ChildNodes['RequestAction'].Text;
end;

procedure TXMLRequestType.Set_RequestAction(Value: WideString);
begin
  ChildNodes['RequestAction'].NodeValue := Value;
end;

function TXMLRequestType.Get_RequestOption: WideString;
begin
  Result := ChildNodes['RequestOption'].Text;
end;

procedure TXMLRequestType.Set_RequestOption(Value: WideString);
begin
  ChildNodes['RequestOption'].NodeValue := Value;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_CustomerContext: WideString;
begin
  Result := ChildNodes['CustomerContext'].Text;
end;

procedure TXMLTransactionReferenceType.Set_CustomerContext(Value: WideString);
begin
  ChildNodes['CustomerContext'].NodeValue := Value;
end;

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

{ TXMLLabelSpecificationType }

procedure TXMLLabelSpecificationType.AfterConstruction;
begin
  RegisterChildNode('LabelPrintMethod', TXMLLabelPrintMethodType);
  RegisterChildNode('LabelImageFormat', TXMLLabelImageFormatType);
  RegisterChildNode('LabelStockSize', TXMLLabelStockSizeType);

  inherited;
end;

function TXMLLabelSpecificationType.Get_LabelImageFormat: IXMLLabelImageFormatType;
begin
  Result := ChildNodes['LabelImageFormat'] as IXMLLabelImageFormatType;
end;

function TXMLLabelSpecificationType.Get_LabelPrintMethod: IXMLLabelPrintMethodType;
begin
  Result := ChildNodes['LabelPrintMethod'] as IXMLLabelPrintMethodType;
end;

function TXMLLabelSpecificationType.Get_LabelStockSize: IXMLLabelStockSizeType;
begin
  Result := ChildNodes['LabelStockSize'] as IXMLLabelStockSizeType;
end;

{ TXMLLabelPrintMethodType }

function TXMLLabelPrintMethodType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLLabelPrintMethodType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLLabelStockSizeType }

function TXMLLabelStockSizeType.Get_Height: Integer;
begin
  Result := ChildNodes['Height'].NodeValue;
end;

procedure TXMLLabelStockSizeType.Set_Height(Value: Integer);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLLabelStockSizeType.Get_Width: Integer;
begin
  Result := ChildNodes['Width'].NodeValue;
end;

procedure TXMLLabelStockSizeType.Set_Width(Value: Integer);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('Shipper', TXMLShipperType);
  RegisterChildNode('ShipTo', TXMLShipToType);
  RegisterChildNode('ShipFrom', TXMLShipFromType);
  RegisterChildNode('PaymentInformation', TXMLPaymentInformationType);
  RegisterChildNode('Service', TXMLServiceType);
  RegisterChildNode('ShipmentServiceOptions', TXMLShipmentServiceOptionsType);
  RegisterChildNode('Package', TXMLPackageType);
  FPackage := CreateCollection(TXMLPackageTypeList, IXMLPackageType, 'Package') as IXMLPackageTypeList;
  inherited;
end;

function TXMLShipmentType.Get_Shipper: IXMLShipperType;
begin
  Result := ChildNodes['Shipper'] as IXMLShipperType;
end;

function TXMLShipmentType.Get_ShipTo: IXMLShipToType;
begin
  Result := ChildNodes['ShipTo'] as IXMLShipToType;
end;

function TXMLShipmentType.Get_ShipFrom: IXMLShipFromType;
begin
  Result := ChildNodes['ShipFrom'] as IXMLShipFromType;
end;

function TXMLShipmentType.Get_PaymentInformation: IXMLPaymentInformationType;
begin
  Result := ChildNodes['PaymentInformation'] as IXMLPaymentInformationType;
end;

function TXMLShipmentType.Get_Service: IXMLServiceType;
begin
  Result := ChildNodes['Service'] as IXMLServiceType;
end;

function TXMLShipmentType.Get_ShipmentServiceOptions: IXMLShipmentServiceOptionsType;
begin
  Result := ChildNodes['ShipmentServiceOptions'] as IXMLShipmentServiceOptionsType;
end;

function TXMLShipmentType.Get_Package: IXMLPackageTypeList;
begin
  Result := FPackage;
end;

{ TXMLShipperType }

procedure TXMLShipperType.AfterConstruction;
begin
  RegisterChildNode('PhoneNumber', TXMLPhoneNumberType);
  RegisterChildNode('Address', TXMLAddressType);
  inherited;
end;

function TXMLShipperType.Get_Name: WideString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLShipperType.Set_Name(Value: WideString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLShipperType.Get_AttentionName: WideString;
begin
  Result := ChildNodes['AttentionName'].Text;
end;

procedure TXMLShipperType.Set_AttentionName(Value: WideString);
begin
  ChildNodes['AttentionName'].NodeValue := Value;
end;

function TXMLShipperType.Get_PhoneNumber: IXMLPhoneNumberType;
begin
  Result := ChildNodes['PhoneNumber'] as IXMLPhoneNumberType;
end;

function TXMLShipperType.Get_ShipperNumber: WideString;
begin
  Result := ChildNodes['ShipperNumber'].Text;
end;

procedure TXMLShipperType.Set_ShipperNumber(Value: WideString);
begin
  ChildNodes['ShipperNumber'].NodeValue := Value;
end;

function TXMLShipperType.Get_Address: IXMLAddressType;
begin
  Result := ChildNodes['Address'] as IXMLAddressType;
end;

{ TXMLPhoneNumberType }

procedure TXMLPhoneNumberType.AfterConstruction;
begin
  RegisterChildNode('StructuredPhoneNumber', TXMLStructuredPhoneNumberType);
  inherited;
end;

function TXMLPhoneNumberType.Get_UnStructuredPhoneNumber: string;
begin
   Result := GetNodeValue;
end;

function TXMLPhoneNumberType.Get_StructuredPhoneNumber: IXMLStructuredPhoneNumberType;
begin
  Result := ChildNodes['StructuredPhoneNumber'] as IXMLStructuredPhoneNumberType;
end;

procedure TXMLPhoneNumberType.Set_UnStructuredPhoneNumber(Value: string);
begin
  SetNodeValue(Value);
end;

{ TXMLStructuredPhoneNumberType }

function TXMLStructuredPhoneNumberType.Get_PhoneDialPlanNumber: Integer;
begin
  Result := ChildNodes['PhoneDialPlanNumber'].NodeValue;
end;

procedure TXMLStructuredPhoneNumberType.Set_PhoneDialPlanNumber(Value: Integer);
begin
  ChildNodes['PhoneDialPlanNumber'].NodeValue := Value;
end;

function TXMLStructuredPhoneNumberType.Get_PhoneLineNumber: Integer;
begin
  Result := ChildNodes['PhoneLineNumber'].NodeValue;
end;

procedure TXMLStructuredPhoneNumberType.Set_PhoneLineNumber(Value: Integer);
begin
  ChildNodes['PhoneLineNumber'].NodeValue := Value;
end;

function TXMLStructuredPhoneNumberType.Get_PhoneExtension: Integer;
begin
  Result := ChildNodes['PhoneExtension'].NodeValue;
end;

procedure TXMLStructuredPhoneNumberType.Set_PhoneExtension(Value: Integer);
begin
  ChildNodes['PhoneExtension'].NodeValue := Value;
end;

{ TXMLAddressType }

function TXMLAddressType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLAddressType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLAddressType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLAddressType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLAddressType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLAddressType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLAddressType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLAddressType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLAddressType.Get_StateProvinceCode: WideString;
begin
  Result := ChildNodes['StateProvinceCode'].Text;
end;

procedure TXMLAddressType.Set_StateProvinceCode(Value: WideString);
begin
  ChildNodes['StateProvinceCode'].NodeValue := Value;
end;

function TXMLAddressType.Get_PostalCode: string;
begin
  Result := ChildNodes['PostalCode'].NodeValue;
end;

procedure TXMLAddressType.Set_PostalCode(Value: string);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLAddressType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLAddressType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLAddressType.Get_ResidentialAddress: WideString;
begin
  Result := ChildNodes['ResidentialAddress'].Text;
end;

procedure TXMLAddressType.Set_ResidentialAddress(Value: WideString);
begin
  ChildNodes['ResidentialAddress'].NodeValue := Value;
end;

{ TXMLShipToType }

procedure TXMLShipToType.AfterConstruction;
begin
  RegisterChildNode('PhoneNumber', TXMLPhoneNumberType);
  RegisterChildNode('Address', TXMLAddressType);
  inherited;
end;

function TXMLShipToType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLShipToType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLShipToType.Get_AttentionName: WideString;
begin
  Result := ChildNodes['AttentionName'].Text;
end;

procedure TXMLShipToType.Set_AttentionName(Value: WideString);
begin
  ChildNodes['AttentionName'].NodeValue := Value;
end;

function TXMLShipToType.Get_PhoneNumber: IXMLPhoneNumberType;
begin
  Result := ChildNodes['PhoneNumber'] as IXMLPhoneNumberType;
end;

function TXMLShipToType.Get_Address: IXMLAddressType;
begin
  Result := ChildNodes['Address'] as IXMLAddressType;
end;

{ TXMLShipFromType }

procedure TXMLShipFromType.AfterConstruction;
begin
  RegisterChildNode('PhoneNumber', TXMLPhoneNumberType);
  RegisterChildNode('Address', TXMLAddressType);
  inherited;
end;

function TXMLShipFromType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLShipFromType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLShipFromType.Get_AttentionName: WideString;
begin
  Result := ChildNodes['AttentionName'].Text;
end;

procedure TXMLShipFromType.Set_AttentionName(Value: WideString);
begin
  ChildNodes['AttentionName'].NodeValue := Value;
end;

function TXMLShipFromType.Get_PhoneNumber: IXMLPhoneNumberType;
begin
  Result := ChildNodes['PhoneNumber'] as IXMLPhoneNumberType;
end;

function TXMLShipFromType.Get_Address: IXMLAddressType;
begin
  Result := ChildNodes['Address'] as IXMLAddressType;
end;

{ TXMLPaymentInformationType }

procedure TXMLPaymentInformationType.AfterConstruction;
begin
  RegisterChildNode('Prepaid', TXMLPrepaidType);
  inherited;
end;

function TXMLPaymentInformationType.Get_Prepaid: IXMLPrepaidType;
begin
  Result := ChildNodes['Prepaid'] as IXMLPrepaidType;
end;

{ TXMLPrepaidType }

procedure TXMLPrepaidType.AfterConstruction;
begin
  RegisterChildNode('BillShipper', TXMLBillShipperType);
  inherited;
end;

function TXMLPrepaidType.Get_BillShipper: IXMLBillShipperType;
begin
  Result := ChildNodes['BillShipper'] as IXMLBillShipperType;
end;

{ TXMLBillShipperType }

function TXMLBillShipperType.Get_AccountNumber: WideString;
begin
  Result := ChildNodes['AccountNumber'].Text;
end;

procedure TXMLBillShipperType.Set_AccountNumber(Value: WideString);
begin
  ChildNodes['AccountNumber'].NodeValue := Value;
end;

{ TXMLServiceType }

function TXMLServiceType.Get_Code: string;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

procedure TXMLServiceType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLShipmentServiceOptionsType }

procedure TXMLShipmentServiceOptionsType.AfterConstruction;
begin
  RegisterChildNode('ShipmentNotification', TXMLShipmentNotificationType);
  ItemTag := 'ShipmentNotification';
  ItemInterface := IXMLShipmentNotificationType;
  inherited;
end;

function TXMLShipmentServiceOptionsType.Get_ShipmentNotification(Index: Integer): IXMLShipmentNotificationType;
begin
  Result := List[Index] as IXMLShipmentNotificationType;
end;

function TXMLShipmentServiceOptionsType.Add: IXMLShipmentNotificationType;
begin
  Result := AddItem(-1) as IXMLShipmentNotificationType;
end;

function TXMLShipmentServiceOptionsType.Insert(const Index: Integer): IXMLShipmentNotificationType;
begin
  Result := AddItem(Index) as IXMLShipmentNotificationType;
end;

{ TXMLShipmentNotificationType }

procedure TXMLShipmentNotificationType.AfterConstruction;
begin
  RegisterChildNode('EMailMessage', TXMLEMailMessageType);
  inherited;
end;

function TXMLShipmentNotificationType.Get_EMailMessage: IXMLEMailMessageType;
begin
  Result := ChildNodes['EMailMessage'] as IXMLEMailMessageType;
end;

{ TXMLEMailMessageType }

function TXMLEMailMessageType.Get_EMailAddress: WideString;
begin
  Result := ChildNodes['EMailAddress'].Text;
end;

procedure TXMLEMailMessageType.Set_EMailAddress(Value: WideString);
begin
  ChildNodes['EMailAddress'].NodeValue := Value;
end;

function TXMLEMailMessageType.Get_Memo: WideString;
begin
  Result := ChildNodes['Memo'].Text;
end;

procedure TXMLEMailMessageType.Set_Memo(Value: WideString);
begin
  ChildNodes['Memo'].NodeValue := Value;
end;

{ TXMLPackageType }

procedure TXMLPackageType.AfterConstruction;
begin
  RegisterChildNode('PackagingType', TXMLPackagingType);
  RegisterChildNode('PackageWeight', TXMLPackageWeightType);
  RegisterChildNode('ReferenceNumber', TXMLReferenceNumberType);
  RegisterChildNode('PackageServiceOptions', TXMLPackageServiceOptionsType);
  inherited;
end;

function TXMLPackageType.Get_Description: WideString;
begin
  Result := ChildNodes['Description'].Text;
end;

procedure TXMLPackageType.Set_Description(Value: WideString);
begin
  ChildNodes['Description'].NodeValue := Value;
end;

function TXMLPackageType.Get_PackagingType: IXMLPackagingType;
begin
  Result := ChildNodes['PackagingType'] as IXMLPackagingType;
end;

function TXMLPackageType.Get_PackageWeight: IXMLPackageWeightType;
begin
  Result := ChildNodes['PackageWeight'] as IXMLPackageWeightType;
end;

function TXMLPackageType.Get_ReferenceNumber: IXMLReferenceNumberType;
begin
  Result := ChildNodes['ReferenceNumber'] as IXMLReferenceNumberType;
end;

function TXMLPackageType.Get_PackageServiceOptions: IXMLPackageServiceOptionsType;
begin
  Result := ChildNodes['PackageServiceOptions'] as IXMLPackageServiceOptionsType;
end;

{ TXMLPackageTypeList }

function TXMLPackageTypeList.Add: IXMLPackageType;
begin
  Result := AddItem(-1) as IXMLPackageType;
end;

function TXMLPackageTypeList.Insert(const Index: Integer): IXMLPackageType;
begin
  Result := AddItem(Index) as IXMLPackageType;
end;
function TXMLPackageTypeList.Get_Item(Index: Integer): IXMLPackageType;
begin
  Result := List[Index] as IXMLPackageType;
end;

{ TXMLPackagingType }

function TXMLPackagingType.Get_Code: string;
begin
  Result := ChildNodes['Code'].NodeValue;
end;

procedure TXMLPackagingType.Set_Code(Value: string);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLPackageWeightType }

procedure TXMLPackageWeightType.AfterConstruction;
begin
  RegisterChildNode('UnitOfMeasurement', TXMLUnitOfMeasurementType);
  inherited;
end;

function TXMLPackageWeightType.Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
begin
  Result := ChildNodes['UnitOfMeasurement'] as IXMLUnitOfMeasurementType;
end;

function TXMLPackageWeightType.Get_Weight: real;
begin
  Result := ChildNodes['Weight'].NodeValue;
end;

procedure TXMLPackageWeightType.Set_Weight(Value: real);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

{ TXMLUnitOfMeasurementType }

function TXMLUnitOfMeasurementType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLUnitOfMeasurementType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLReferenceNumberType }

function TXMLReferenceNumberType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLReferenceNumberType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

function TXMLReferenceNumberType.Get_Value: WideString;
begin
  Result := ChildNodes['Value'].Text;
end;

procedure TXMLReferenceNumberType.Set_Value(Value: WideString);
begin
  ChildNodes['Value'].NodeValue := Value;
end;

{ TXMLPackageServiceOptionsType }

procedure TXMLPackageServiceOptionsType.AfterConstruction;
begin
  RegisterChildNode('COD', TXMLCODType);
  RegisterChildNode('InsuredValue', TXMLInsuredValueType);
  inherited;
end;

function TXMLPackageServiceOptionsType.Get_COD: IXMLCODType;
begin
  Result := ChildNodes['COD'] as IXMLCODType;
end;

function TXMLPackageServiceOptionsType.Get_InsuredValue: IXMLInsuredValueType;
begin
  Result := ChildNodes['InsuredValue'] as IXMLInsuredValueType;
end;

{ TXMLCODType }

procedure TXMLCODType.AfterConstruction;
begin
  RegisterChildNode('CODAmount', TXMLCODAmountType);
  inherited;
end;

function TXMLCODType.Get_CODFundsCode: Integer;
begin
  Result := ChildNodes['CODFundsCode'].NodeValue;
end;

procedure TXMLCODType.Set_CODFundsCode(Value: Integer);
begin
  ChildNodes['CODFundsCode'].NodeValue := Value;
end;

function TXMLCODType.Get_CODCode: Integer;
begin
  Result := ChildNodes['CODCode'].NodeValue;
end;

procedure TXMLCODType.Set_CODCode(Value: Integer);
begin
  ChildNodes['CODCode'].NodeValue := Value;
end;

function TXMLCODType.Get_CODAmount: IXMLCODAmountType;
begin
  Result := ChildNodes['CODAmount'] as IXMLCODAmountType;
end;

{ TXMLCODAmountType }

function TXMLCODAmountType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLCODAmountType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLCODAmountType.Get_MonetaryValue: Integer;
begin
  Result := ChildNodes['MonetaryValue'].NodeValue;
end;

procedure TXMLCODAmountType.Set_MonetaryValue(Value: Integer);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLInsuredValueType }

function TXMLInsuredValueType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLInsuredValueType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLInsuredValueType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLInsuredValueType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLLabelImageFormatType }

function TXMLLabelImageFormatType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLLabelImageFormatType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

end.