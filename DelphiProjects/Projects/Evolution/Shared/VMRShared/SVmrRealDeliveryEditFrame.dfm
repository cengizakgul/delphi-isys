inherited VmrRealDeliveryEditFrame: TVmrRealDeliveryEditFrame
  Width = 618
  Height = 336
  object lEmailAddr: TevLabel [0]
    Left = 8
    Top = 16
    Width = 67
    Height = 13
    Caption = 'Email ASCII to'
  end
  object edEmailAddr: TevDBEdit [1]
    Left = 96
    Top = 16
    Width = 289
    Height = 21
    DataField = 'EMAIL'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBCheckBox1: TevDBCheckBox [2]
    Left = 95
    Top = 46
    Width = 373
    Height = 17
    Caption = 'Use Output ASCII File Name for ASCII results'
    DataField = 'SAVE_ASCII'
    DataSource = dsMain
    TabOrder = 1
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited dsMain: TevDataSource
    Left = 440
    Top = 304
  end
end
