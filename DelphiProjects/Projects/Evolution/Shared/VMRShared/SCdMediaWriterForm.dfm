object CdMediaWriterForm: TCdMediaWriterForm
  Left = 474
  Top = 289
  BorderStyle = bsDialog
  Caption = 'CD Writer Dialog'
  ClientHeight = 127
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 8
    Top = 8
    Width = 76
    Height = 13
    Caption = 'Choose Device:'
  end
  object lMessage: TevLabel
    Left = 8
    Top = 56
    Width = 148
    Height = 13
    Caption = 'Load blank CD media and click'
  end
  object cbChooseDrive: TevComboBox
    Left = 104
    Top = 8
    Width = 161
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object bGo: TevButton
    Left = 192
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Write'
    Default = True
    TabOrder = 1
    OnClick = bGoClick
  end
  object bCancel: TevButton
    Left = 192
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object cbAutoBurn: TevCheckBox
    Left = 8
    Top = 88
    Width = 129
    Height = 17
    Caption = 'Start automatically'
    TabOrder = 3
  end
  object PrBar: TProgressBar
    Left = 0
    Top = 110
    Width = 274
    Height = 17
    Align = alBottom
    TabOrder = 4
  end
  object MCDB: TMCDBurner
    DeviceBufferSize = 0
    DeviceFreeBufferSize = 0
    UnderrunProtection = True
    ReadSpeed = 0
    WriteSpeed = 0
    FinalizeDisc = False
    TestWrite = False
    ReplaceFile = False
    PerformOPC = False
    CacheSize = 4194304
    OnWriteDone = MCDBWriteDone
    Version = '1.1.7'
    WritePostGap = True
    Left = 88
    Top = 80
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 128
    Top = 80
  end
end
