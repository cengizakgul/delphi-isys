
{*************************************************************************************************************}
{                                                                                                             }
{                                              XML Data Binding                                               }
{                                                                                                             }
{         Generated on: 7/9/2009 1:32:31 PM                                                                   }
{       Generated from: E:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\Openshipments.xdr   }
{   Settings stored in: E:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\Openshipments.xdb   }
{                                                                                                             }
{*************************************************************************************************************}

unit sUpsOpenshipments;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUPSOpenShipmentsType = interface;
  IXMLOpenShipmentType = interface;
  IXMLReceiverType = interface;
  IXMLReturnToType = interface;
  IXMLShipForType = interface;
  IXMLShipperType = interface;
  IXMLShipmentType = interface;
  IXMLThirdPartyType = interface;
  IXMLDeclareValueType = interface;
  IXMLAdditionalHandlingType = interface;
  IXMLCODType = interface;
  IXMLAODTagType = interface;
  IXMLShipNotificationType = interface;
  IXMLQuantumViewNotifyDetailsType = interface;
  IXMLQuantumViewNotifyType = interface;
  IXMLQuantumViewNotifyTypeList = interface;
  IXMLSpecialCommoditiesType = interface;
  IXMLReturnNotificationDetailsType = interface;
  IXMLReturnNotificationType = interface;
  IXMLReturnNotificationTypeList = interface;
  IXMLUPSExchangeCollectType = interface;
  IXMLReturnServiceType = interface;
  IXMLInvoiceType = interface;
  IXMLLineItemType = interface;
  IXMLLineItemTypeList = interface;
  IXMLProductType = interface;
  IXMLSoldToType = interface;
  IXMLShipToType = interface;
  IXMLShipFromType = interface;
  IXMLProducerType = interface;
  IXMLUltimateConsigneeType = interface;
  IXMLImporterType = interface;
  IXMLThirdPartyReceiverType = interface;
  IXMLShipmentInformationType = interface;
  IXMLDeclaredValueType = interface;
  IXMLQVNOptionType = interface;
  IXMLQVNRecipientAndNotificationTypesType = interface;
  IXMLQVNRecipientAndNotificationTypesTypeList = interface;
  IXMLDeliveryConfirmationType = interface;
  IXMLNotifyBeforeDelOptionType = interface;
  IXMLHandlingChargeOptionType = interface;
  IXMLLTLLineItemType = interface;
  IXMLLTLLineItemTypeList = interface;
  IXMLPackageType = interface;
  IXMLPackageTypeList = interface;
  IXMLQVNOrReturnNotificationOptionType = interface;
  IXMLQVNOrReturnRecipientAndNotificationTypesType = interface;
  IXMLQVNOrReturnRecipientAndNotificationTypesTypeList = interface;
  IXMLVerbalConfirmationOptionType = interface;
  IXMLFlexibleParcelInsuranceOptionType = interface;
  IXMLDryIceOptionType = interface;
  IXMLInternationalDocumentationType = interface;
  IXMLGoodsType = interface;
  IXMLGoodsTypeList = interface;
  IXMLProcessMessageType = interface;
  IXMLProcessMessageTypeList = interface;
  IXMLErrorType = interface;
  IXMLShipmentRatesType = interface;
  IXMLShipmentChargesType = interface;
  IXMLRateType = interface;
  IXMLShipperChargesType = interface;
  IXMLReceiverChargesType = interface;
  IXMLShipmentandHandlingChargesType = interface;
  IXMLDeclared_ValueType = interface;
  IXMLC_O_DType = interface;
  IXMLReturn_ServicesType = interface;
  IXMLSaturday_DeliveryType = interface;
  IXMLSaturday_PickupType = interface;
  IXMLProactive_ResponseType = interface;
  IXMLDelivery_ConfirmationType = interface;
  IXMLDelivery_AreaSurchargeType = interface;
  IXMLPackageRatesType = interface;
  IXMLPackageRateType = interface;
  IXMLPackageChargesType = interface;
  IXMLAdditional_HandlingType = interface;
  IXMLVerbal_ConfirmationType = interface;
  IXMLQVNType = interface;
  IXMLDry_IceType = interface;
  IXMLTrackingNumbersType = interface;

{ IXMLUPSOpenShipmentsType }

  IXMLUPSOpenShipmentsType = interface(IXMLNodeCollection)
    ['{596F7363-4442-4AB2-843C-F7F2970E0E72}']
    { Property Accessors }
    function Get_OpenShipment(Index: Integer): IXMLOpenShipmentType;
    { Methods & Properties }
    function Add: IXMLOpenShipmentType;
    function Insert(const Index: Integer): IXMLOpenShipmentType;
    property OpenShipment[Index: Integer]: IXMLOpenShipmentType read Get_OpenShipment; default;
  end;

{ IXMLOpenShipmentType }

  IXMLOpenShipmentType = interface(IXMLNode)
    ['{2CB137E3-2C3A-4B61-B7C9-EF48F0979401}']
    { Property Accessors }
    function Get_ProcessStatus: WideString;
    function Get_ShipmentOption: WideString;
    //function Get_ProcessStatus: WideString;
    //function Get_ShipmentOption: WideString;
    function Get_Receiver: IXMLReceiverType;
    function Get_ReturnTo: IXMLReturnToType;
    function Get_ShipFor: IXMLShipForType;
    function Get_Shipper: IXMLShipperType;
    function Get_Shipment: IXMLShipmentType;
    function Get_Invoice: IXMLInvoiceType;
    function Get_ShipTo: IXMLShipToType;
    function Get_ShipFrom: IXMLShipFromType;
    function Get_Producer: IXMLProducerType;
    function Get_UltimateConsignee: IXMLUltimateConsigneeType;
    function Get_Importer: IXMLImporterType;
    function Get_ThirdParty: IXMLThirdPartyType;
    function Get_ThirdPartyReceiver: IXMLThirdPartyReceiverType;
    function Get_ShipmentInformation: IXMLShipmentInformationType;
    function Get_LTLLineItem: IXMLLTLLineItemTypeList;
    function Get_Package: IXMLPackageTypeList;
    function Get_InternationalDocumentation: IXMLInternationalDocumentationType;
    function Get_Goods: IXMLGoodsTypeList;
    function Get_ProcessMessage: IXMLProcessMessageTypeList;
    procedure Set_ProcessStatus(Value: WideString);
    procedure Set_ShipmentOption(Value: WideString);
    //procedure Set_ProcessStatus(Value: WideString);
    //procedure Set_ShipmentOption(Value: WideString);
    { Methods & Properties }
    property ShipmentOption: WideString read Get_ShipmentOption write Set_ShipmentOption;
    property ProcessStatus: WideString read Get_ProcessStatus write Set_ProcessStatus;
    property Receiver: IXMLReceiverType read Get_Receiver;
    property ReturnTo: IXMLReturnToType read Get_ReturnTo;
    property ShipFor: IXMLShipForType read Get_ShipFor;
    property Shipper: IXMLShipperType read Get_Shipper;
    property Shipment: IXMLShipmentType read Get_Shipment;
    property Invoice: IXMLInvoiceType read Get_Invoice;
    property ShipTo: IXMLShipToType read Get_ShipTo;
    property ShipFrom: IXMLShipFromType read Get_ShipFrom;
    property Producer: IXMLProducerType read Get_Producer;
    property UltimateConsignee: IXMLUltimateConsigneeType read Get_UltimateConsignee;
    property Importer: IXMLImporterType read Get_Importer;
    property ThirdParty: IXMLThirdPartyType read Get_ThirdParty;
    property ThirdPartyReceiver: IXMLThirdPartyReceiverType read Get_ThirdPartyReceiver;
    property ShipmentInformation: IXMLShipmentInformationType read Get_ShipmentInformation;
    property LTLLineItem: IXMLLTLLineItemTypeList read Get_LTLLineItem;
    property Package: IXMLPackageTypeList read Get_Package;
    property InternationalDocumentation: IXMLInternationalDocumentationType read Get_InternationalDocumentation;
    property Goods: IXMLGoodsTypeList read Get_Goods;
    property ProcessMessage: IXMLProcessMessageTypeList read Get_ProcessMessage;
  end;

{ IXMLReceiverType }

  IXMLReceiverType = interface(IXMLNode)
    ['{450295C4-05C2-447B-ADFE-7B55CBA06C21}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_LocationID: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    function Get_EmailAddress1: WideString;
    function Get_EmailAddress2: WideString;
    function Get_EmailContact1: WideString;
    function Get_EmailContact2: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_LocationID(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
    procedure Set_EmailAddress2(Value: WideString);
    procedure Set_EmailContact1(Value: WideString);
    procedure Set_EmailContact2(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property City: WideString read Get_City write Set_City;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property StateOrProvince: WideString read Get_StateOrProvince write Set_StateOrProvince;
    property Residential: WideString read Get_Residential write Set_Residential;
    property CustomerIDNumber: WideString read Get_CustomerIDNumber write Set_CustomerIDNumber;
    property Phone: WideString read Get_Phone write Set_Phone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property LocationID: WideString read Get_LocationID write Set_LocationID;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
    property RecordOwner: WideString read Get_RecordOwner write Set_RecordOwner;
    property EmailAddress1: WideString read Get_EmailAddress1 write Set_EmailAddress1;
    property EmailAddress2: WideString read Get_EmailAddress2 write Set_EmailAddress2;
    property EmailContact1: WideString read Get_EmailContact1 write Set_EmailContact1;
    property EmailContact2: WideString read Get_EmailContact2 write Set_EmailContact2;
  end;

{ IXMLReturnToType }

  IXMLReturnToType = interface(IXMLNode)
    ['{E36DAB38-3D2C-4FE8-92B8-BC3A6FEA5A14}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_EmailAddress1: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property City: WideString read Get_City write Set_City;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property StateOrProvince: WideString read Get_StateOrProvince write Set_StateOrProvince;
    property Residential: WideString read Get_Residential write Set_Residential;
    property Phone: WideString read Get_Phone write Set_Phone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property EmailAddress1: WideString read Get_EmailAddress1 write Set_EmailAddress1;
  end;

{ IXMLShipForType }

  IXMLShipForType = interface(IXMLNode)
    ['{CFAFA3C2-41B4-4056-9F19-70A98919EF18}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_EmailAddress1: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property City: WideString read Get_City write Set_City;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property StateOrProvince: WideString read Get_StateOrProvince write Set_StateOrProvince;
    property Residential: WideString read Get_Residential write Set_Residential;
    property Phone: WideString read Get_Phone write Set_Phone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property EmailAddress1: WideString read Get_EmailAddress1 write Set_EmailAddress1;
  end;

{ IXMLShipperType }

  IXMLShipperType = interface(IXMLNode)
    ['{B8970A6D-62C9-42C0-8417-5E522E132C61}']
    { Property Accessors }
    function Get_UpsAccountNumber: WideString;
    procedure Set_UpsAccountNumber(Value: WideString);
    { Methods & Properties }
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
  end;

{ IXMLShipmentType }

  IXMLShipmentType = interface(IXMLNode)
    ['{31B36E92-CD12-4CC7-AFCB-3311A4A76275}']
    { Property Accessors }
    function Get_ServiceLevel: WideString;
    function Get_PackageType: WideString;
    function Get_NumberOfPackages: WideString;
    function Get_ShipmentActualWeight: WideString;
    function Get_ShipmentDimensionalWeight: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_CostAllocationCode: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_DocumentOnly: WideString;
    function Get_GoodsNotInFreeCirculation: WideString;
    function Get_BillingOption: WideString;
    function Get_ThirdParty: IXMLThirdPartyType;
    function Get_SaturdayDelivery: WideString;
    function Get_DeclareValue: IXMLDeclareValueType;
    function Get_AdditionalHandling: IXMLAdditionalHandlingType;
    function Get_COD: IXMLCODType;
    function Get_AODTag: IXMLAODTagType;
    function Get_ShipNotification: IXMLShipNotificationType;
    function Get_QuantumViewNotifyDetails: IXMLQuantumViewNotifyDetailsType;
    function Get_SpecialCommodities: IXMLSpecialCommoditiesType;
    function Get_SignatureAdultSignatureRequired: WideString;
    function Get_ReturnNotificationDetails: IXMLReturnNotificationDetailsType;
    function Get_CollectionDate: WideString;
    function Get_ImportID: WideString;
    function Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
    function Get_ReturnService: IXMLReturnServiceType;
    procedure Set_ServiceLevel(Value: WideString);
    procedure Set_PackageType(Value: WideString);
    procedure Set_NumberOfPackages(Value: WideString);
    procedure Set_ShipmentActualWeight(Value: WideString);
    procedure Set_ShipmentDimensionalWeight(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_CostAllocationCode(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_DocumentOnly(Value: WideString);
    procedure Set_GoodsNotInFreeCirculation(Value: WideString);
    procedure Set_BillingOption(Value: WideString);
    procedure Set_SaturdayDelivery(Value: WideString);
    procedure Set_SignatureAdultSignatureRequired(Value: WideString);
    procedure Set_CollectionDate(Value: WideString);
    procedure Set_ImportID(Value: WideString);
    { Methods & Properties }
    property ServiceLevel: WideString read Get_ServiceLevel write Set_ServiceLevel;
    property PackageType: WideString read Get_PackageType write Set_PackageType;
    property NumberOfPackages: WideString read Get_NumberOfPackages write Set_NumberOfPackages;
    property ShipmentActualWeight: WideString read Get_ShipmentActualWeight write Set_ShipmentActualWeight;
    property ShipmentDimensionalWeight: WideString read Get_ShipmentDimensionalWeight write Set_ShipmentDimensionalWeight;
    property DescriptionOfGoods: WideString read Get_DescriptionOfGoods write Set_DescriptionOfGoods;
    property CostAllocationCode: WideString read Get_CostAllocationCode write Set_CostAllocationCode;
    property Reference1: WideString read Get_Reference1 write Set_Reference1;
    property Reference2: WideString read Get_Reference2 write Set_Reference2;
    property DocumentOnly: WideString read Get_DocumentOnly write Set_DocumentOnly;
    property GoodsNotInFreeCirculation: WideString read Get_GoodsNotInFreeCirculation write Set_GoodsNotInFreeCirculation;
    property BillingOption: WideString read Get_BillingOption write Set_BillingOption;
    property ThirdParty: IXMLThirdPartyType read Get_ThirdParty;
    property SaturdayDelivery: WideString read Get_SaturdayDelivery write Set_SaturdayDelivery;
    property DeclareValue: IXMLDeclareValueType read Get_DeclareValue;
    property AdditionalHandling: IXMLAdditionalHandlingType read Get_AdditionalHandling;
    property COD: IXMLCODType read Get_COD;
    property AODTag: IXMLAODTagType read Get_AODTag;
    property ShipNotification: IXMLShipNotificationType read Get_ShipNotification;
    property QuantumViewNotifyDetails: IXMLQuantumViewNotifyDetailsType read Get_QuantumViewNotifyDetails;
    property SpecialCommodities: IXMLSpecialCommoditiesType read Get_SpecialCommodities;
    property SignatureAdultSignatureRequired: WideString read Get_SignatureAdultSignatureRequired write Set_SignatureAdultSignatureRequired;
    property ReturnNotificationDetails: IXMLReturnNotificationDetailsType read Get_ReturnNotificationDetails;
    property CollectionDate: WideString read Get_CollectionDate write Set_CollectionDate;
    property ImportID: WideString read Get_ImportID write Set_ImportID;
    property UPSExchangeCollect: IXMLUPSExchangeCollectType read Get_UPSExchangeCollect;
    property ReturnService: IXMLReturnServiceType read Get_ReturnService;
  end;

{ IXMLThirdPartyType }

  IXMLThirdPartyType = interface(IXMLNode)
    ['{FCD1593F-8D98-44E2-BA3A-C2BD7298B20C}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyName: WideString;
    function Get_CompanyOrName: WideString;
    function Get_ContactPerson: WideString;
    function Get_Attention: WideString;
    function Get_AddressLine1: WideString;
    function Get_Address1: WideString;
    function Get_AddressLine2: WideString;
    function Get_Address2: WideString;
    function Get_AddressLine3: WideString;
    function Get_Address3: WideString;
    function Get_City: WideString;
    function Get_CityOrTown: WideString;
    function Get_CountryCode: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Residential: WideString;
    function Get_ResidentialIndicator: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyName(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property Attention: WideString read Get_Attention write Set_Attention;
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property City: WideString read Get_City write Set_City;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property StateOrProvince: WideString read Get_StateOrProvince write Set_StateOrProvince;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Residential: WideString read Get_Residential write Set_Residential;
    property ResidentialIndicator: WideString read Get_ResidentialIndicator write Set_ResidentialIndicator;
    property CustomerIDNumber: WideString read Get_CustomerIDNumber write Set_CustomerIDNumber;
    property Phone: WideString read Get_Phone write Set_Phone;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property FaxNumber: WideString read Get_FaxNumber write Set_FaxNumber;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
    property RecordOwner: WideString read Get_RecordOwner write Set_RecordOwner;
  end;

{ IXMLDeclareValueType }

  IXMLDeclareValueType = interface(IXMLNode)
    ['{EB728F26-ECB5-4505-B738-3A0AAB141E29}']
    { Property Accessors }
    function Get_Amount: WideString;
    procedure Set_Amount(Value: WideString);
    { Methods & Properties }
    property Amount: WideString read Get_Amount write Set_Amount;
  end;

{ IXMLAdditionalHandlingType }

  IXMLAdditionalHandlingType = interface(IXMLNode)
    ['{7C6B0A75-97B6-41B7-9DAD-1486057715A7}']
    { Property Accessors }
    function Get_NumberOfAdditionalHandling: WideString;
    procedure Set_NumberOfAdditionalHandling(Value: WideString);
    { Methods & Properties }
    property NumberOfAdditionalHandling: WideString read Get_NumberOfAdditionalHandling write Set_NumberOfAdditionalHandling;
  end;

{ IXMLCODType }

  IXMLCODType = interface(IXMLNode)
    ['{8DDD4ABD-819D-437F-9ABE-76687C49C894}']
    { Property Accessors }
    function Get_CashOnly: WideString;
    function Get_CashierCheckorMoneyOrderOnlyIndicator: WideString;
    function Get_AddShippingChargesToCODIndicator: WideString;
    function Get_Amount: WideString;
    function Get_Currency: WideString;
    procedure Set_CashOnly(Value: WideString);
    procedure Set_CashierCheckorMoneyOrderOnlyIndicator(Value: WideString);
    procedure Set_AddShippingChargesToCODIndicator(Value: WideString);
    procedure Set_Amount(Value: WideString);
    procedure Set_Currency(Value: WideString);
    { Methods & Properties }
    property CashOnly: WideString read Get_CashOnly write Set_CashOnly;
    property CashierCheckorMoneyOrderOnlyIndicator: WideString read Get_CashierCheckorMoneyOrderOnlyIndicator write Set_CashierCheckorMoneyOrderOnlyIndicator;
    property AddShippingChargesToCODIndicator: WideString read Get_AddShippingChargesToCODIndicator write Set_AddShippingChargesToCODIndicator;
    property Amount: WideString read Get_Amount write Set_Amount;
    property Currency: WideString read Get_Currency write Set_Currency;
  end;

{ IXMLAODTagType }

  IXMLAODTagType = interface(IXMLNode)
    ['{B15ED6A8-BB19-4616-A22E-F8F2458F20BF}']
    { Property Accessors }
    function Get_NumberOfAOD: WideString;
    procedure Set_NumberOfAOD(Value: WideString);
    { Methods & Properties }
    property NumberOfAOD: WideString read Get_NumberOfAOD write Set_NumberOfAOD;
  end;

{ IXMLShipNotificationType }

  IXMLShipNotificationType = interface(IXMLNode)
    ['{F288C42A-AD96-40F3-A231-2A4DFFA33D5D}']
    { Property Accessors }
    function Get_EmailText: WideString;
    procedure Set_EmailText(Value: WideString);
    { Methods & Properties }
    property EmailText: WideString read Get_EmailText write Set_EmailText;
  end;

{ IXMLQuantumViewNotifyDetailsType }

  IXMLQuantumViewNotifyDetailsType = interface(IXMLNode)
    ['{4507C846-3E2E-4141-8444-19EF2E7640A1}']
    { Property Accessors }
    function Get_QuantumViewNotify: IXMLQuantumViewNotifyTypeList;
    function Get_FailureNotificationEMailAddress: WideString;
    function Get_ShipperCompanyName: WideString;
    function Get_SubjectLineOptions: WideString;
    function Get_NotificationMessage: WideString;
    procedure Set_FailureNotificationEMailAddress(Value: WideString);
    procedure Set_ShipperCompanyName(Value: WideString);
    procedure Set_SubjectLineOptions(Value: WideString);
    procedure Set_NotificationMessage(Value: WideString);
    { Methods & Properties }
    property QuantumViewNotify: IXMLQuantumViewNotifyTypeList read Get_QuantumViewNotify;
    property FailureNotificationEMailAddress: WideString read Get_FailureNotificationEMailAddress write Set_FailureNotificationEMailAddress;
    property ShipperCompanyName: WideString read Get_ShipperCompanyName write Set_ShipperCompanyName;
    property SubjectLineOptions: WideString read Get_SubjectLineOptions write Set_SubjectLineOptions;
    property NotificationMessage: WideString read Get_NotificationMessage write Set_NotificationMessage;
  end;

{ IXMLQuantumViewNotifyType }

  IXMLQuantumViewNotifyType = interface(IXMLNode)
    ['{A7E33FA1-ACA8-4A6E-8261-4C80EE46AFE7}']
    { Property Accessors }
    function Get_NotificationEMailAddress: WideString;
    function Get_NotificationRequest: WideString;
    procedure Set_NotificationEMailAddress(Value: WideString);
    procedure Set_NotificationRequest(Value: WideString);
    { Methods & Properties }
    property NotificationEMailAddress: WideString read Get_NotificationEMailAddress write Set_NotificationEMailAddress;
    property NotificationRequest: WideString read Get_NotificationRequest write Set_NotificationRequest;
  end;

{ IXMLQuantumViewNotifyTypeList }

  IXMLQuantumViewNotifyTypeList = interface(IXMLNodeCollection)
    ['{F6E55641-B928-46EC-AC35-024F263C72E0}']
    { Methods & Properties }
    function Add: IXMLQuantumViewNotifyType;
    function Insert(const Index: Integer): IXMLQuantumViewNotifyType;
    function Get_Item(Index: Integer): IXMLQuantumViewNotifyType;
    property Items[Index: Integer]: IXMLQuantumViewNotifyType read Get_Item; default;
  end;

{ IXMLSpecialCommoditiesType }

  IXMLSpecialCommoditiesType = interface(IXMLNode)
    ['{F7077BF2-790E-4C40-BEA5-E405524FC918}']
    { Property Accessors }
    function Get_CommoditiesDetails: WideString;
    function Get_ISCAlcoholicBeverages: WideString;
    function Get_ISCDiagnosticSpecimens: WideString;
    function Get_ISCPerishables: WideString;
    function Get_ISCPlants: WideString;
    function Get_ISCSeeds: WideString;
    function Get_ISCSpecialException: WideString;
    function Get_ISCTobacco: WideString;
    procedure Set_CommoditiesDetails(Value: WideString);
    procedure Set_ISCAlcoholicBeverages(Value: WideString);
    procedure Set_ISCDiagnosticSpecimens(Value: WideString);
    procedure Set_ISCPerishables(Value: WideString);
    procedure Set_ISCPlants(Value: WideString);
    procedure Set_ISCSeeds(Value: WideString);
    procedure Set_ISCSpecialException(Value: WideString);
    procedure Set_ISCTobacco(Value: WideString);
    { Methods & Properties }
    property CommoditiesDetails: WideString read Get_CommoditiesDetails write Set_CommoditiesDetails;
    property ISCAlcoholicBeverages: WideString read Get_ISCAlcoholicBeverages write Set_ISCAlcoholicBeverages;
    property ISCDiagnosticSpecimens: WideString read Get_ISCDiagnosticSpecimens write Set_ISCDiagnosticSpecimens;
    property ISCPerishables: WideString read Get_ISCPerishables write Set_ISCPerishables;
    property ISCPlants: WideString read Get_ISCPlants write Set_ISCPlants;
    property ISCSeeds: WideString read Get_ISCSeeds write Set_ISCSeeds;
    property ISCSpecialException: WideString read Get_ISCSpecialException write Set_ISCSpecialException;
    property ISCTobacco: WideString read Get_ISCTobacco write Set_ISCTobacco;
  end;

{ IXMLReturnNotificationDetailsType }

  IXMLReturnNotificationDetailsType = interface(IXMLNode)
    ['{BC71B440-D055-40EB-B8D7-9BF9A48CFE4E}']
    { Property Accessors }
    function Get_ReturnNotification: IXMLReturnNotificationTypeList;
    function Get_FailureNotificationEMailAddress: WideString;
    function Get_ShipperCompanyName: WideString;
    function Get_SubjectLineOptions: WideString;
    function Get_NotificationMessage: WideString;
    procedure Set_FailureNotificationEMailAddress(Value: WideString);
    procedure Set_ShipperCompanyName(Value: WideString);
    procedure Set_SubjectLineOptions(Value: WideString);
    procedure Set_NotificationMessage(Value: WideString);
    { Methods & Properties }
    property ReturnNotification: IXMLReturnNotificationTypeList read Get_ReturnNotification;
    property FailureNotificationEMailAddress: WideString read Get_FailureNotificationEMailAddress write Set_FailureNotificationEMailAddress;
    property ShipperCompanyName: WideString read Get_ShipperCompanyName write Set_ShipperCompanyName;
    property SubjectLineOptions: WideString read Get_SubjectLineOptions write Set_SubjectLineOptions;
    property NotificationMessage: WideString read Get_NotificationMessage write Set_NotificationMessage;
  end;

{ IXMLReturnNotificationType }

  IXMLReturnNotificationType = interface(IXMLNodeCollection)
    ['{148C3C6C-9244-406F-A66B-6BD600648E8B}']
    { Property Accessors }
    function Get_NotificationEMailAddress(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const NotificationEMailAddress: WideString): IXMLNode;
    function Insert(const Index: Integer; const NotificationEMailAddress: WideString): IXMLNode;
    property NotificationEMailAddress[Index: Integer]: WideString read Get_NotificationEMailAddress; default;
  end;

{ IXMLReturnNotificationTypeList }

  IXMLReturnNotificationTypeList = interface(IXMLNodeCollection)
    ['{7C53BEBB-81DA-434E-B158-A764D110F694}']
    { Methods & Properties }
    function Add: IXMLReturnNotificationType;
    function Insert(const Index: Integer): IXMLReturnNotificationType;
    function Get_Item(Index: Integer): IXMLReturnNotificationType;
    property Items[Index: Integer]: IXMLReturnNotificationType read Get_Item; default;
  end;

{ IXMLUPSExchangeCollectType }

  IXMLUPSExchangeCollectType = interface(IXMLNode)
    ['{6594F02C-0619-4911-A9ED-A79E2828C414}']
    { Property Accessors }
    function Get_BuyerEMailAddress: WideString;
    function Get_SellerEMailAddress: WideString;
    function Get_ExchangeCollectAmount: WideString;
    function Get_Currency: WideString;
    function Get_BuyerPercentageOfServiceFee: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_AdditionalComments: WideString;
    procedure Set_BuyerEMailAddress(Value: WideString);
    procedure Set_SellerEMailAddress(Value: WideString);
    procedure Set_ExchangeCollectAmount(Value: WideString);
    procedure Set_Currency(Value: WideString);
    procedure Set_BuyerPercentageOfServiceFee(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_AdditionalComments(Value: WideString);
    { Methods & Properties }
    property BuyerEMailAddress: WideString read Get_BuyerEMailAddress write Set_BuyerEMailAddress;
    property SellerEMailAddress: WideString read Get_SellerEMailAddress write Set_SellerEMailAddress;
    property ExchangeCollectAmount: WideString read Get_ExchangeCollectAmount write Set_ExchangeCollectAmount;
    property Currency: WideString read Get_Currency write Set_Currency;
    property BuyerPercentageOfServiceFee: WideString read Get_BuyerPercentageOfServiceFee write Set_BuyerPercentageOfServiceFee;
    property DescriptionOfGoods: WideString read Get_DescriptionOfGoods write Set_DescriptionOfGoods;
    property AdditionalComments: WideString read Get_AdditionalComments write Set_AdditionalComments;
  end;

{ IXMLReturnServiceType }

  IXMLReturnServiceType = interface(IXMLNode)
    ['{1B99453D-ADB3-4C15-A7BC-175463445D0B}']
    { Property Accessors }
    function Get_Options: WideString;
    function Get_PrintAddressOnlyInvoice: WideString;
    function Get_SenderCompanyName: WideString;
    function Get_RecipientEmailAddress: WideString;
    function Get_FailureOfDeliveryEmailAddress: WideString;
    function Get_InstructionAndReceiptLanguage: WideString;
    function Get_MerchandiseDescOfPackage: WideString;
    procedure Set_Options(Value: WideString);
    procedure Set_PrintAddressOnlyInvoice(Value: WideString);
    procedure Set_SenderCompanyName(Value: WideString);
    procedure Set_RecipientEmailAddress(Value: WideString);
    procedure Set_FailureOfDeliveryEmailAddress(Value: WideString);
    procedure Set_InstructionAndReceiptLanguage(Value: WideString);
    procedure Set_MerchandiseDescOfPackage(Value: WideString);
    { Methods & Properties }
    property Options: WideString read Get_Options write Set_Options;
    property PrintAddressOnlyInvoice: WideString read Get_PrintAddressOnlyInvoice write Set_PrintAddressOnlyInvoice;
    property SenderCompanyName: WideString read Get_SenderCompanyName write Set_SenderCompanyName;
    property RecipientEmailAddress: WideString read Get_RecipientEmailAddress write Set_RecipientEmailAddress;
    property FailureOfDeliveryEmailAddress: WideString read Get_FailureOfDeliveryEmailAddress write Set_FailureOfDeliveryEmailAddress;
    property InstructionAndReceiptLanguage: WideString read Get_InstructionAndReceiptLanguage write Set_InstructionAndReceiptLanguage;
    property MerchandiseDescOfPackage: WideString read Get_MerchandiseDescOfPackage write Set_MerchandiseDescOfPackage;
  end;

{ IXMLInvoiceType }

  IXMLInvoiceType = interface(IXMLNode)
    ['{E34812E8-D50B-4C6C-9AE3-A2255232BB50}']
    { Property Accessors }
    function Get_ReasonForExport: WideString;
    function Get_TermOfSale: WideString;
    function Get_DeclarationStatement: WideString;
    function Get_AdditionalComments: WideString;
    function Get_InvoiceCurrency: WideString;
    function Get_InvoiceLineTotal: WideString;
    function Get_DiscountRebate: WideString;
    function Get_Freight: WideString;
    function Get_Insurance: WideString;
    function Get_OtherCharges: WideString;
    function Get_TotalInvoiceAmount: WideString;
    function Get_LineItem: IXMLLineItemTypeList;
    function Get_SoldTo: IXMLSoldToType;
    procedure Set_ReasonForExport(Value: WideString);
    procedure Set_TermOfSale(Value: WideString);
    procedure Set_DeclarationStatement(Value: WideString);
    procedure Set_AdditionalComments(Value: WideString);
    procedure Set_InvoiceCurrency(Value: WideString);
    procedure Set_InvoiceLineTotal(Value: WideString);
    procedure Set_DiscountRebate(Value: WideString);
    procedure Set_Freight(Value: WideString);
    procedure Set_Insurance(Value: WideString);
    procedure Set_OtherCharges(Value: WideString);
    procedure Set_TotalInvoiceAmount(Value: WideString);
    { Methods & Properties }
    property ReasonForExport: WideString read Get_ReasonForExport write Set_ReasonForExport;
    property TermOfSale: WideString read Get_TermOfSale write Set_TermOfSale;
    property DeclarationStatement: WideString read Get_DeclarationStatement write Set_DeclarationStatement;
    property AdditionalComments: WideString read Get_AdditionalComments write Set_AdditionalComments;
    property InvoiceCurrency: WideString read Get_InvoiceCurrency write Set_InvoiceCurrency;
    property InvoiceLineTotal: WideString read Get_InvoiceLineTotal write Set_InvoiceLineTotal;
    property DiscountRebate: WideString read Get_DiscountRebate write Set_DiscountRebate;
    property Freight: WideString read Get_Freight write Set_Freight;
    property Insurance: WideString read Get_Insurance write Set_Insurance;
    property OtherCharges: WideString read Get_OtherCharges write Set_OtherCharges;
    property TotalInvoiceAmount: WideString read Get_TotalInvoiceAmount write Set_TotalInvoiceAmount;
    property LineItem: IXMLLineItemTypeList read Get_LineItem;
    property SoldTo: IXMLSoldToType read Get_SoldTo;
  end;

{ IXMLLineItemType }

  IXMLLineItemType = interface(IXMLNode)
    ['{716BF6A8-437B-4408-8E5B-C07B32FB05D1}']
    { Property Accessors }
    function Get_Product: IXMLProductType;
    function Get_NumberOfUnits: WideString;
    procedure Set_NumberOfUnits(Value: WideString);
    { Methods & Properties }
    property Product: IXMLProductType read Get_Product;
    property NumberOfUnits: WideString read Get_NumberOfUnits write Set_NumberOfUnits;
  end;

{ IXMLLineItemTypeList }

  IXMLLineItemTypeList = interface(IXMLNodeCollection)
    ['{BC7C2F2D-4712-4E11-94BB-FF4B72B13418}']
    { Methods & Properties }
    function Add: IXMLLineItemType;
    function Insert(const Index: Integer): IXMLLineItemType;
    function Get_Item(Index: Integer): IXMLLineItemType;
    property Items[Index: Integer]: IXMLLineItemType read Get_Item; default;
  end;

{ IXMLProductType }

  IXMLProductType = interface(IXMLNode)
    ['{824E167B-AD9C-462B-B61D-83403653640C}']
    { Property Accessors }
    function Get_ProductNumber: WideString;
    function Get_PartNumber: WideString;
    function Get_ProductName: WideString;
    function Get_HarmonizedCode: WideString;
    function Get_ProductDescription: WideString;
    function Get_UnitOfMeasure: WideString;
    function Get_CountryOfOriginCode: WideString;
    function Get_CurrencyCode: WideString;
    function Get_UnitPrice: WideString;
    procedure Set_ProductNumber(Value: WideString);
    procedure Set_PartNumber(Value: WideString);
    procedure Set_ProductName(Value: WideString);
    procedure Set_HarmonizedCode(Value: WideString);
    procedure Set_ProductDescription(Value: WideString);
    procedure Set_UnitOfMeasure(Value: WideString);
    procedure Set_CountryOfOriginCode(Value: WideString);
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_UnitPrice(Value: WideString);
    { Methods & Properties }
    property ProductNumber: WideString read Get_ProductNumber write Set_ProductNumber;
    property PartNumber: WideString read Get_PartNumber write Set_PartNumber;
    property ProductName: WideString read Get_ProductName write Set_ProductName;
    property HarmonizedCode: WideString read Get_HarmonizedCode write Set_HarmonizedCode;
    property ProductDescription: WideString read Get_ProductDescription write Set_ProductDescription;
    property UnitOfMeasure: WideString read Get_UnitOfMeasure write Set_UnitOfMeasure;
    property CountryOfOriginCode: WideString read Get_CountryOfOriginCode write Set_CountryOfOriginCode;
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property UnitPrice: WideString read Get_UnitPrice write Set_UnitPrice;
  end;

{ IXMLSoldToType }

  IXMLSoldToType = interface(IXMLNode)
    ['{8DB6C01D-2B3D-4770-BA49-724686E82092}']
    { Property Accessors }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
    { Methods & Properties }
    property CompanyName: WideString read Get_CompanyName write Set_CompanyName;
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property AddressLine1: WideString read Get_AddressLine1 write Set_AddressLine1;
    property AddressLine2: WideString read Get_AddressLine2 write Set_AddressLine2;
    property AddressLine3: WideString read Get_AddressLine3 write Set_AddressLine3;
    property City: WideString read Get_City write Set_City;
    property CountryCode: WideString read Get_CountryCode write Set_CountryCode;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property StateOrProvince: WideString read Get_StateOrProvince write Set_StateOrProvince;
    property Residential: WideString read Get_Residential write Set_Residential;
    property CustomerIDNumber: WideString read Get_CustomerIDNumber write Set_CustomerIDNumber;
    property Phone: WideString read Get_Phone write Set_Phone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
    property RecordOwner: WideString read Get_RecordOwner write Set_RecordOwner;
  end;

{ IXMLShipToType }

  IXMLShipToType = interface(IXMLNode)
    ['{D7B8F541-0A7E-4053-BD65-100B57DBEEED}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_EmailAddress: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_ReceiverUpsAccountNumber: WideString;
    function Get_LocationID: WideString;
    function Get_ResidentialIndicator: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_EmailAddress(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_ReceiverUpsAccountNumber(Value: WideString);
    procedure Set_LocationID(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property FaxNumber: WideString read Get_FaxNumber write Set_FaxNumber;
    property EmailAddress: WideString read Get_EmailAddress write Set_EmailAddress;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property ReceiverUpsAccountNumber: WideString read Get_ReceiverUpsAccountNumber write Set_ReceiverUpsAccountNumber;
    property LocationID: WideString read Get_LocationID write Set_LocationID;
    property ResidentialIndicator: WideString read Get_ResidentialIndicator write Set_ResidentialIndicator;
  end;

{ IXMLShipFromType }

  IXMLShipFromType = interface(IXMLNode)
    ['{887CD24E-AFEC-402B-B4E0-8B9813AC2C13}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_TaxIDType: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_ResidentialIndicator: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_TaxIDType(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property FaxNumber: WideString read Get_FaxNumber write Set_FaxNumber;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property TaxIDType: WideString read Get_TaxIDType write Set_TaxIDType;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
    property ResidentialIndicator: WideString read Get_ResidentialIndicator write Set_ResidentialIndicator;
  end;

{ IXMLProducerType }

  IXMLProducerType = interface(IXMLNode)
    ['{8CD507DF-4F60-4C72-8900-75C9177BCA25}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
  end;

{ IXMLUltimateConsigneeType }

  IXMLUltimateConsigneeType = interface(IXMLNode)
    ['{7FA42893-2CA9-4604-BC02-2829D010C5A0}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
  end;

{ IXMLImporterType }

  IXMLImporterType = interface(IXMLNode)
    ['{64911996-54E9-435A-921F-C91298B6DBEF}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property TaxIDNumber: WideString read Get_TaxIDNumber write Set_TaxIDNumber;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
  end;

{ IXMLThirdPartyReceiverType }

  IXMLThirdPartyReceiverType = interface(IXMLNode)
    ['{76120DDC-B313-430F-959F-7878DE38F2A1}']
    { Property Accessors }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    { Methods & Properties }
    property CustomerID: WideString read Get_CustomerID write Set_CustomerID;
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property Attention: WideString read Get_Attention write Set_Attention;
    property Address1: WideString read Get_Address1 write Set_Address1;
    property Address2: WideString read Get_Address2 write Set_Address2;
    property Address3: WideString read Get_Address3 write Set_Address3;
    property CountryTerritory: WideString read Get_CountryTerritory write Set_CountryTerritory;
    property PostalCode: WideString read Get_PostalCode write Set_PostalCode;
    property CityOrTown: WideString read Get_CityOrTown write Set_CityOrTown;
    property StateProvinceCounty: WideString read Get_StateProvinceCounty write Set_StateProvinceCounty;
    property Telephone: WideString read Get_Telephone write Set_Telephone;
    property FaxNumber: WideString read Get_FaxNumber write Set_FaxNumber;
    property UpsAccountNumber: WideString read Get_UpsAccountNumber write Set_UpsAccountNumber;
  end;

{ IXMLShipmentInformationType }

  IXMLShipmentInformationType = interface(IXMLNode)
    ['{231B3EDA-6FE4-4D7B-8E32-FC83FAA4062C}']
    { Property Accessors }
    function Get_VoidIndicator: WideString;
    function Get_ServiceType: WideString;
    function Get_PackageType: WideString;
    function Get_NumberOfPackages: WideString;
    function Get_ShipmentActualWeight: WideString;
    function Get_ShipmentDimensionalWeight: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_Reference3: WideString;
    function Get_Reference4: WideString;
    function Get_Reference5: WideString;
    function Get_DocumentOnly: WideString;
    function Get_GoodsNotInFreeCirculation: WideString;
    function Get_SpecialInstructionForShipment: WideString;
    function Get_ShipperNumber: WideString;
    function Get_BillingOption: WideString;
    function Get_BillTransportationTo: WideString;
    function Get_BillDutyTaxTo: WideString;
    function Get_SplitDutyAndTax: WideString;
    function Get_SaturdayDelivery: WideString;
    function Get_SaturdayPickUp: WideString;
    function Get_DeclaredValue: IXMLDeclaredValueType;
    function Get_MerchandiseDescriptionForPackage: WideString;
    function Get_AdditionalHandling: IXMLAdditionalHandlingType;
    function Get_COD: IXMLCODType;
    function Get_ReturnOfDocument: WideString;
    function Get_QVNOption: IXMLQVNOptionType;
    function Get_SpecialCommodities: IXMLSpecialCommoditiesType;
    function Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
    function Get_UserDefinedID: WideString;
    function Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
    function Get_ReturnService: IXMLReturnServiceType;
    function Get_NotifyBeforeDelOption: IXMLNotifyBeforeDelOptionType;
    function Get_HandlingChargeOption: IXMLHandlingChargeOptionType;
    function Get_FoodItems: WideString;
    function Get_HundredWeightPricingApplied: WideString;
    function Get_USI: WideString;
    function Get_SubProNumber: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_AdditionalDocuments: WideString;
    function Get_ProactiveResponseOption: WideString;
    procedure Set_VoidIndicator(Value: WideString);
    procedure Set_ServiceType(Value: WideString);
    procedure Set_PackageType(Value: WideString);
    procedure Set_NumberOfPackages(Value: WideString);
    procedure Set_ShipmentActualWeight(Value: WideString);
    procedure Set_ShipmentDimensionalWeight(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_Reference3(Value: WideString);
    procedure Set_Reference4(Value: WideString);
    procedure Set_Reference5(Value: WideString);
    procedure Set_DocumentOnly(Value: WideString);
    procedure Set_GoodsNotInFreeCirculation(Value: WideString);
    procedure Set_SpecialInstructionForShipment(Value: WideString);
    procedure Set_ShipperNumber(Value: WideString);
    procedure Set_BillingOption(Value: WideString);
    procedure Set_BillTransportationTo(Value: WideString);
    procedure Set_BillDutyTaxTo(Value: WideString);
    procedure Set_SplitDutyAndTax(Value: WideString);
    procedure Set_SaturdayDelivery(Value: WideString);
    procedure Set_SaturdayPickUp(Value: WideString);
    procedure Set_MerchandiseDescriptionForPackage(Value: WideString);
    procedure Set_ReturnOfDocument(Value: WideString);
    procedure Set_UserDefinedID(Value: WideString);
    procedure Set_FoodItems(Value: WideString);
    procedure Set_HundredWeightPricingApplied(Value: WideString);
    procedure Set_USI(Value: WideString);
    procedure Set_SubProNumber(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_AdditionalDocuments(Value: WideString);
    procedure Set_ProactiveResponseOption(Value: WideString);
    { Methods & Properties }
    property VoidIndicator: WideString read Get_VoidIndicator write Set_VoidIndicator;
    property ServiceType: WideString read Get_ServiceType write Set_ServiceType;
    property PackageType: WideString read Get_PackageType write Set_PackageType;
    property NumberOfPackages: WideString read Get_NumberOfPackages write Set_NumberOfPackages;
    property ShipmentActualWeight: WideString read Get_ShipmentActualWeight write Set_ShipmentActualWeight;
    property ShipmentDimensionalWeight: WideString read Get_ShipmentDimensionalWeight write Set_ShipmentDimensionalWeight;
    property DescriptionOfGoods: WideString read Get_DescriptionOfGoods write Set_DescriptionOfGoods;
    property Reference1: WideString read Get_Reference1 write Set_Reference1;
    property Reference2: WideString read Get_Reference2 write Set_Reference2;
    property Reference3: WideString read Get_Reference3 write Set_Reference3;
    property Reference4: WideString read Get_Reference4 write Set_Reference4;
    property Reference5: WideString read Get_Reference5 write Set_Reference5;
    property DocumentOnly: WideString read Get_DocumentOnly write Set_DocumentOnly;
    property GoodsNotInFreeCirculation: WideString read Get_GoodsNotInFreeCirculation write Set_GoodsNotInFreeCirculation;
    property SpecialInstructionForShipment: WideString read Get_SpecialInstructionForShipment write Set_SpecialInstructionForShipment;
    property ShipperNumber: WideString read Get_ShipperNumber write Set_ShipperNumber;
    property BillingOption: WideString read Get_BillingOption write Set_BillingOption;
    property BillTransportationTo: WideString read Get_BillTransportationTo write Set_BillTransportationTo;
    property BillDutyTaxTo: WideString read Get_BillDutyTaxTo write Set_BillDutyTaxTo;
    property SplitDutyAndTax: WideString read Get_SplitDutyAndTax write Set_SplitDutyAndTax;
    property SaturdayDelivery: WideString read Get_SaturdayDelivery write Set_SaturdayDelivery;
    property SaturdayPickUp: WideString read Get_SaturdayPickUp write Set_SaturdayPickUp;
    property DeclaredValue: IXMLDeclaredValueType read Get_DeclaredValue;
    property MerchandiseDescriptionForPackage: WideString read Get_MerchandiseDescriptionForPackage write Set_MerchandiseDescriptionForPackage;
    property AdditionalHandling: IXMLAdditionalHandlingType read Get_AdditionalHandling;
    property COD: IXMLCODType read Get_COD;
    property ReturnOfDocument: WideString read Get_ReturnOfDocument write Set_ReturnOfDocument;
    property QVNOption: IXMLQVNOptionType read Get_QVNOption;
    property SpecialCommodities: IXMLSpecialCommoditiesType read Get_SpecialCommodities;
    property DeliveryConfirmation: IXMLDeliveryConfirmationType read Get_DeliveryConfirmation;
    property UserDefinedID: WideString read Get_UserDefinedID write Set_UserDefinedID;
    property UPSExchangeCollect: IXMLUPSExchangeCollectType read Get_UPSExchangeCollect;
    property ReturnService: IXMLReturnServiceType read Get_ReturnService;
    property NotifyBeforeDelOption: IXMLNotifyBeforeDelOptionType read Get_NotifyBeforeDelOption;
    property HandlingChargeOption: IXMLHandlingChargeOptionType read Get_HandlingChargeOption;
    property FoodItems: WideString read Get_FoodItems write Set_FoodItems;
    property HundredWeightPricingApplied: WideString read Get_HundredWeightPricingApplied write Set_HundredWeightPricingApplied;
    property USI: WideString read Get_USI write Set_USI;
    property SubProNumber: WideString read Get_SubProNumber write Set_SubProNumber;
    property Length: WideString read Get_Length write Set_Length;
    property Width: WideString read Get_Width write Set_Width;
    property Height: WideString read Get_Height write Set_Height;
    property AdditionalDocuments: WideString read Get_AdditionalDocuments write Set_AdditionalDocuments;
    property ProactiveResponseOption: WideString read Get_ProactiveResponseOption write Set_ProactiveResponseOption;
  end;

{ IXMLDeclaredValueType }

  IXMLDeclaredValueType = interface(IXMLNode)
    ['{A20BCA2F-77CE-4972-BA1A-575695292767}']
    { Property Accessors }
    function Get_Amount: WideString;
    function Get_ShipperPaid: WideString;
    procedure Set_Amount(Value: WideString);
    procedure Set_ShipperPaid(Value: WideString);
    { Methods & Properties }
    property Amount: WideString read Get_Amount write Set_Amount;
    property ShipperPaid: WideString read Get_ShipperPaid write Set_ShipperPaid;
  end;

{ IXMLQVNOptionType }

  IXMLQVNOptionType = interface(IXMLNode)
    ['{2E712907-6038-4A48-82A0-27EE6B690E3E}']
    { Property Accessors }
    function Get_QVNRecipientAndNotificationTypes: IXMLQVNRecipientAndNotificationTypesTypeList;
    function Get_ShipFromCompanyOrName: WideString;
    function Get_FailedEMailAddress: WideString;
    function Get_SubjectLine: WideString;
    function Get_Memo: WideString;
    procedure Set_ShipFromCompanyOrName(Value: WideString);
    procedure Set_FailedEMailAddress(Value: WideString);
    procedure Set_SubjectLine(Value: WideString);
    procedure Set_Memo(Value: WideString);
    { Methods & Properties }
    property QVNRecipientAndNotificationTypes: IXMLQVNRecipientAndNotificationTypesTypeList read Get_QVNRecipientAndNotificationTypes;
    property ShipFromCompanyOrName: WideString read Get_ShipFromCompanyOrName write Set_ShipFromCompanyOrName;
    property FailedEMailAddress: WideString read Get_FailedEMailAddress write Set_FailedEMailAddress;
    property SubjectLine: WideString read Get_SubjectLine write Set_SubjectLine;
    property Memo: WideString read Get_Memo write Set_Memo;
  end;

{ IXMLQVNRecipientAndNotificationTypesType }

  IXMLQVNRecipientAndNotificationTypesType = interface(IXMLNode)
    ['{5A474E81-E44F-4D6C-983F-C2ED2F7E445E}']
    { Property Accessors }
    function Get_CompanyOrName: WideString;
    function Get_ContactName: WideString;
    function Get_EMailAddress: WideString;
    function Get_Ship: WideString;
    function Get_Exception: WideString;
    function Get_Delivery: WideString;
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactName(Value: WideString);
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Ship(Value: WideString);
    procedure Set_Exception(Value: WideString);
    procedure Set_Delivery(Value: WideString);
    { Methods & Properties }
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property ContactName: WideString read Get_ContactName write Set_ContactName;
    property EMailAddress: WideString read Get_EMailAddress write Set_EMailAddress;
    property Ship: WideString read Get_Ship write Set_Ship;
    property Exception: WideString read Get_Exception write Set_Exception;
    property Delivery: WideString read Get_Delivery write Set_Delivery;
  end;

{ IXMLQVNRecipientAndNotificationTypesTypeList }

  IXMLQVNRecipientAndNotificationTypesTypeList = interface(IXMLNodeCollection)
    ['{1F8EC26B-563A-49E2-8BFD-447FD1A79B4B}']
    { Methods & Properties }
    function Add: IXMLQVNRecipientAndNotificationTypesType;
    function Insert(const Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
    function Get_Item(Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
    property Items[Index: Integer]: IXMLQVNRecipientAndNotificationTypesType read Get_Item; default;
  end;

{ IXMLDeliveryConfirmationType }

  IXMLDeliveryConfirmationType = interface(IXMLNode)
    ['{AFB077E8-51AE-480B-BCB6-C570F1E41A45}']
    { Property Accessors }
    function Get_SignatureRequired: WideString;
    function Get_AdultSignatureRequired: WideString;
    procedure Set_SignatureRequired(Value: WideString);
    procedure Set_AdultSignatureRequired(Value: WideString);
    { Methods & Properties }
    property SignatureRequired: WideString read Get_SignatureRequired write Set_SignatureRequired;
    property AdultSignatureRequired: WideString read Get_AdultSignatureRequired write Set_AdultSignatureRequired;
  end;

{ IXMLNotifyBeforeDelOptionType }

  IXMLNotifyBeforeDelOptionType = interface(IXMLNode)
    ['{0B565266-088E-483D-A016-9F3A784F06AE}']
    { Property Accessors }
    function Get_ContactPerson: WideString;
    function Get_Phone: WideString;
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_Phone(Value: WideString);
    { Methods & Properties }
    property ContactPerson: WideString read Get_ContactPerson write Set_ContactPerson;
    property Phone: WideString read Get_Phone write Set_Phone;
  end;

{ IXMLHandlingChargeOptionType }

  IXMLHandlingChargeOptionType = interface(IXMLNode)
    ['{4A5A7B99-B234-4253-8EE0-79CE23EFDAD0}']
    { Property Accessors }
    function Get_HandlingChargeType: WideString;
    function Get_HCAmountIncreaseDecreaseType: WideString;
    function Get_HandlingChargeFlatRate: WideString;
    function Get_HandlingChargePercentage: WideString;
    procedure Set_HandlingChargeType(Value: WideString);
    procedure Set_HCAmountIncreaseDecreaseType(Value: WideString);
    procedure Set_HandlingChargeFlatRate(Value: WideString);
    procedure Set_HandlingChargePercentage(Value: WideString);
    { Methods & Properties }
    property HandlingChargeType: WideString read Get_HandlingChargeType write Set_HandlingChargeType;
    property HCAmountIncreaseDecreaseType: WideString read Get_HCAmountIncreaseDecreaseType write Set_HCAmountIncreaseDecreaseType;
    property HandlingChargeFlatRate: WideString read Get_HandlingChargeFlatRate write Set_HandlingChargeFlatRate;
    property HandlingChargePercentage: WideString read Get_HandlingChargePercentage write Set_HandlingChargePercentage;
  end;

{ IXMLLTLLineItemType }

  IXMLLTLLineItemType = interface(IXMLNode)
    ['{405F58AA-A525-4DA4-9BF1-F1855672656F}']
    { Property Accessors }
    function Get_FreightClass: WideString;
    function Get_LineDescription: WideString;
    function Get_NumberOfIdenticalHandlingUnit: WideString;
    function Get_HandlingUnitWeight: WideString;
    function Get_PieceOnPallet: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_PackagingType: WideString;
    procedure Set_FreightClass(Value: WideString);
    procedure Set_LineDescription(Value: WideString);
    procedure Set_NumberOfIdenticalHandlingUnit(Value: WideString);
    procedure Set_HandlingUnitWeight(Value: WideString);
    procedure Set_PieceOnPallet(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_PackagingType(Value: WideString);
    { Methods & Properties }
    property FreightClass: WideString read Get_FreightClass write Set_FreightClass;
    property LineDescription: WideString read Get_LineDescription write Set_LineDescription;
    property NumberOfIdenticalHandlingUnit: WideString read Get_NumberOfIdenticalHandlingUnit write Set_NumberOfIdenticalHandlingUnit;
    property HandlingUnitWeight: WideString read Get_HandlingUnitWeight write Set_HandlingUnitWeight;
    property PieceOnPallet: WideString read Get_PieceOnPallet write Set_PieceOnPallet;
    property Length: WideString read Get_Length write Set_Length;
    property Width: WideString read Get_Width write Set_Width;
    property Height: WideString read Get_Height write Set_Height;
    property PackagingType: WideString read Get_PackagingType write Set_PackagingType;
  end;

{ IXMLLTLLineItemTypeList }

  IXMLLTLLineItemTypeList = interface(IXMLNodeCollection)
    ['{673C4267-27BD-4097-8A98-770F078D2386}']
    { Methods & Properties }
    function Add: IXMLLTLLineItemType;
    function Insert(const Index: Integer): IXMLLTLLineItemType;
    function Get_Item(Index: Integer): IXMLLTLLineItemType;
    property Items[Index: Integer]: IXMLLTLLineItemType read Get_Item; default;
  end;

{ IXMLPackageType }

  IXMLPackageType = interface(IXMLNode)
    ['{90DE57E3-60CE-4B8F-9990-A4388D60C69C}']
    { Property Accessors }
    function Get_PackageType: WideString;
    function Get_Weight: WideString;
    function Get_TrackingNumber: WideString;
    function Get_USPSNumber: WideString;
    function Get_LargePackageIndicator: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_Reference3: WideString;
    function Get_Reference4: WideString;
    function Get_Reference5: WideString;
    function Get_AdditionalHandlingOption: WideString;
    function Get_ShipperReleaseOption: WideString;
    function Get_COD: IXMLCODType;
    function Get_DeclaredValue: IXMLDeclaredValueType;
    function Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
    function Get_QVNOrReturnNotificationOption: IXMLQVNOrReturnNotificationOptionType;
    function Get_VerbalConfirmationOption: IXMLVerbalConfirmationOptionType;
    function Get_DangerousGoodsOption: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_MerchandiseDescription: WideString;
    function Get_FlexibleParcelInsuranceOption: IXMLFlexibleParcelInsuranceOptionType;
    function Get_ProactiveResponseOption: WideString;
    function Get_DryIceOption: IXMLDryIceOptionType;
    procedure Set_PackageType(Value: WideString);
    procedure Set_Weight(Value: WideString);
    procedure Set_TrackingNumber(Value: WideString);
    procedure Set_USPSNumber(Value: WideString);
    procedure Set_LargePackageIndicator(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_Reference3(Value: WideString);
    procedure Set_Reference4(Value: WideString);
    procedure Set_Reference5(Value: WideString);
    procedure Set_AdditionalHandlingOption(Value: WideString);
    procedure Set_ShipperReleaseOption(Value: WideString);
    procedure Set_DangerousGoodsOption(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_MerchandiseDescription(Value: WideString);
    procedure Set_ProactiveResponseOption(Value: WideString);
    { Methods & Properties }
    property PackageType: WideString read Get_PackageType write Set_PackageType;
    property Weight: WideString read Get_Weight write Set_Weight;
    property TrackingNumber: WideString read Get_TrackingNumber write Set_TrackingNumber;
    property USPSNumber: WideString read Get_USPSNumber write Set_USPSNumber;
    property LargePackageIndicator: WideString read Get_LargePackageIndicator write Set_LargePackageIndicator;
    property Reference1: WideString read Get_Reference1 write Set_Reference1;
    property Reference2: WideString read Get_Reference2 write Set_Reference2;
    property Reference3: WideString read Get_Reference3 write Set_Reference3;
    property Reference4: WideString read Get_Reference4 write Set_Reference4;
    property Reference5: WideString read Get_Reference5 write Set_Reference5;
    property AdditionalHandlingOption: WideString read Get_AdditionalHandlingOption write Set_AdditionalHandlingOption;
    property ShipperReleaseOption: WideString read Get_ShipperReleaseOption write Set_ShipperReleaseOption;
    property COD: IXMLCODType read Get_COD;
    property DeclaredValue: IXMLDeclaredValueType read Get_DeclaredValue;
    property DeliveryConfirmation: IXMLDeliveryConfirmationType read Get_DeliveryConfirmation;
    property QVNOrReturnNotificationOption: IXMLQVNOrReturnNotificationOptionType read Get_QVNOrReturnNotificationOption;
    property VerbalConfirmationOption: IXMLVerbalConfirmationOptionType read Get_VerbalConfirmationOption;
    property DangerousGoodsOption: WideString read Get_DangerousGoodsOption write Set_DangerousGoodsOption;
    property Length: WideString read Get_Length write Set_Length;
    property Width: WideString read Get_Width write Set_Width;
    property Height: WideString read Get_Height write Set_Height;
    property MerchandiseDescription: WideString read Get_MerchandiseDescription write Set_MerchandiseDescription;
    property FlexibleParcelInsuranceOption: IXMLFlexibleParcelInsuranceOptionType read Get_FlexibleParcelInsuranceOption;
    property ProactiveResponseOption: WideString read Get_ProactiveResponseOption write Set_ProactiveResponseOption;
    property DryIceOption: IXMLDryIceOptionType read Get_DryIceOption;
  end;

{ IXMLPackageTypeList }

  IXMLPackageTypeList = interface(IXMLNodeCollection)
    ['{21FD2ED4-B864-4B07-A2DF-4F6A32AB9735}']
    { Methods & Properties }
    function Add: IXMLPackageType;
    function Insert(const Index: Integer): IXMLPackageType;
    function Get_Item(Index: Integer): IXMLPackageType;
    property Items[Index: Integer]: IXMLPackageType read Get_Item; default;
  end;

{ IXMLQVNOrReturnNotificationOptionType }

  IXMLQVNOrReturnNotificationOptionType = interface(IXMLNode)
    ['{27A14F7A-D2C1-4F5A-B98B-EE54ED1898C8}']
    { Property Accessors }
    function Get_QVNOrReturnRecipientAndNotificationTypes: IXMLQVNOrReturnRecipientAndNotificationTypesTypeList;
    function Get_ShipFromCompanyOrName: WideString;
    function Get_FailedEMailAddress: WideString;
    function Get_SubjectLine: WideString;
    function Get_Memo: WideString;
    procedure Set_ShipFromCompanyOrName(Value: WideString);
    procedure Set_FailedEMailAddress(Value: WideString);
    procedure Set_SubjectLine(Value: WideString);
    procedure Set_Memo(Value: WideString);
    { Methods & Properties }
    property QVNOrReturnRecipientAndNotificationTypes: IXMLQVNOrReturnRecipientAndNotificationTypesTypeList read Get_QVNOrReturnRecipientAndNotificationTypes;
    property ShipFromCompanyOrName: WideString read Get_ShipFromCompanyOrName write Set_ShipFromCompanyOrName;
    property FailedEMailAddress: WideString read Get_FailedEMailAddress write Set_FailedEMailAddress;
    property SubjectLine: WideString read Get_SubjectLine write Set_SubjectLine;
    property Memo: WideString read Get_Memo write Set_Memo;
  end;

{ IXMLQVNOrReturnRecipientAndNotificationTypesType }

  IXMLQVNOrReturnRecipientAndNotificationTypesType = interface(IXMLNode)
    ['{F9FB12B0-F4C9-426C-9898-E77A411B2BD5}']
    { Property Accessors }
    function Get_CompanyOrName: WideString;
    function Get_ContactName: WideString;
    function Get_EMailAddress: WideString;
    function Get_Ship: WideString;
    function Get_Exception: WideString;
    function Get_Delivery: WideString;
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactName(Value: WideString);
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Ship(Value: WideString);
    procedure Set_Exception(Value: WideString);
    procedure Set_Delivery(Value: WideString);
    { Methods & Properties }
    property CompanyOrName: WideString read Get_CompanyOrName write Set_CompanyOrName;
    property ContactName: WideString read Get_ContactName write Set_ContactName;
    property EMailAddress: WideString read Get_EMailAddress write Set_EMailAddress;
    property Ship: WideString read Get_Ship write Set_Ship;
    property Exception: WideString read Get_Exception write Set_Exception;
    property Delivery: WideString read Get_Delivery write Set_Delivery;
  end;

{ IXMLQVNOrReturnRecipientAndNotificationTypesTypeList }

  IXMLQVNOrReturnRecipientAndNotificationTypesTypeList = interface(IXMLNodeCollection)
    ['{245B73A6-845E-4B47-9C85-9DDE3962C0C7}']
    { Methods & Properties }
    function Add: IXMLQVNOrReturnRecipientAndNotificationTypesType;
    function Insert(const Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
    function Get_Item(Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
    property Items[Index: Integer]: IXMLQVNOrReturnRecipientAndNotificationTypesType read Get_Item; default;
  end;

{ IXMLVerbalConfirmationOptionType }

  IXMLVerbalConfirmationOptionType = interface(IXMLNode)
    ['{EF9B111B-51F0-44E8-9C2C-580F29E2CB54}']
    { Property Accessors }
    function Get_VerbalConfirmationContactName: WideString;
    function Get_VerbalConfirmationTelephone: WideString;
    procedure Set_VerbalConfirmationContactName(Value: WideString);
    procedure Set_VerbalConfirmationTelephone(Value: WideString);
    { Methods & Properties }
    property VerbalConfirmationContactName: WideString read Get_VerbalConfirmationContactName write Set_VerbalConfirmationContactName;
    property VerbalConfirmationTelephone: WideString read Get_VerbalConfirmationTelephone write Set_VerbalConfirmationTelephone;
  end;

{ IXMLFlexibleParcelInsuranceOptionType }

  IXMLFlexibleParcelInsuranceOptionType = interface(IXMLNode)
    ['{34870DD0-4938-4707-8C67-7F20A23A8550}']
    { Property Accessors }
    function Get_FlexibleParcelInsuranceType: WideString;
    function Get_FlexibleParcelInsuranceValue: WideString;
    procedure Set_FlexibleParcelInsuranceType(Value: WideString);
    procedure Set_FlexibleParcelInsuranceValue(Value: WideString);
    { Methods & Properties }
    property FlexibleParcelInsuranceType: WideString read Get_FlexibleParcelInsuranceType write Set_FlexibleParcelInsuranceType;
    property FlexibleParcelInsuranceValue: WideString read Get_FlexibleParcelInsuranceValue write Set_FlexibleParcelInsuranceValue;
  end;

{ IXMLDryIceOptionType }

  IXMLDryIceOptionType = interface(IXMLNode)
    ['{E8F257D4-A2CC-4840-8B30-2F83AC7EE6A0}']
    { Property Accessors }
    function Get_Weight: WideString;
    function Get_UnitOfMeasure: WideString;
    function Get_RegulationSet: WideString;
    function Get_IsMedical: WideString;
    function Get_DryIcePackagingType: WideString;
    procedure Set_Weight(Value: WideString);
    procedure Set_UnitOfMeasure(Value: WideString);
    procedure Set_RegulationSet(Value: WideString);
    procedure Set_IsMedical(Value: WideString);
    procedure Set_DryIcePackagingType(Value: WideString);
    { Methods & Properties }
    property Weight: WideString read Get_Weight write Set_Weight;
    property UnitOfMeasure: WideString read Get_UnitOfMeasure write Set_UnitOfMeasure;
    property RegulationSet: WideString read Get_RegulationSet write Set_RegulationSet;
    property IsMedical: WideString read Get_IsMedical write Set_IsMedical;
    property DryIcePackagingType: WideString read Get_DryIcePackagingType write Set_DryIcePackagingType;
  end;

{ IXMLInternationalDocumentationType }

  IXMLInternationalDocumentationType = interface(IXMLNode)
    ['{0C6D48A7-7067-45B0-ACB1-26AA154EDFD2}']
    { Property Accessors }
    function Get_InvoiceImporterSameAsShipTo: WideString;
    function Get_InvoiceTermOfSale: WideString;
    function Get_InvoiceReasonForExport: WideString;
    function Get_InvoiceDeclarationStatement: WideString;
    function Get_InvoiceAdditionalComments: WideString;
    function Get_InvoiceCurrencyCode: WideString;
    function Get_InvoiceDiscount: WideString;
    function Get_InvoiceChargesFreight: WideString;
    function Get_InvoiceChargesInsurance: WideString;
    function Get_InvoiceChargesOther: WideString;
    function Get_InvoiceLineTotal: WideString;
    function Get_CustomsValueTotal: WideString;
    function Get_CustomsValueCurrencyCode: WideString;
    function Get_CustomsValueOrigin: WideString;
    function Get_SEDCode: WideString;
    function Get_SEDPartiesToTransaction: WideString;
    function Get_SEDUltimateConsigneeSameAsShipTo: WideString;
    function Get_SEDCountryTerritoryOfUltimateDestination: WideString;
    function Get_SEDAESTransactionNumber: WideString;
    function Get_SEDAESTransactionNumberType: WideString;
    function Get_SEDRoutedTransaction: WideString;
    function Get_SEDCurrencyConversion: WideString;
    function Get_SEDExportCode: WideString;
    function Get_COUPSPrepareCO: WideString;
    function Get_COOwnerOrAgent: WideString;
    function Get_NAFTAProducer: WideString;
    function Get_NAFTABlanketPeriodStart: WideString;
    function Get_NAFTABlanketPeriodEnd: WideString;
    procedure Set_InvoiceImporterSameAsShipTo(Value: WideString);
    procedure Set_InvoiceTermOfSale(Value: WideString);
    procedure Set_InvoiceReasonForExport(Value: WideString);
    procedure Set_InvoiceDeclarationStatement(Value: WideString);
    procedure Set_InvoiceAdditionalComments(Value: WideString);
    procedure Set_InvoiceCurrencyCode(Value: WideString);
    procedure Set_InvoiceDiscount(Value: WideString);
    procedure Set_InvoiceChargesFreight(Value: WideString);
    procedure Set_InvoiceChargesInsurance(Value: WideString);
    procedure Set_InvoiceChargesOther(Value: WideString);
    procedure Set_InvoiceLineTotal(Value: WideString);
    procedure Set_CustomsValueTotal(Value: WideString);
    procedure Set_CustomsValueCurrencyCode(Value: WideString);
    procedure Set_CustomsValueOrigin(Value: WideString);
    procedure Set_SEDCode(Value: WideString);
    procedure Set_SEDPartiesToTransaction(Value: WideString);
    procedure Set_SEDUltimateConsigneeSameAsShipTo(Value: WideString);
    procedure Set_SEDCountryTerritoryOfUltimateDestination(Value: WideString);
    procedure Set_SEDAESTransactionNumber(Value: WideString);
    procedure Set_SEDAESTransactionNumberType(Value: WideString);
    procedure Set_SEDRoutedTransaction(Value: WideString);
    procedure Set_SEDCurrencyConversion(Value: WideString);
    procedure Set_SEDExportCode(Value: WideString);
    procedure Set_COUPSPrepareCO(Value: WideString);
    procedure Set_COOwnerOrAgent(Value: WideString);
    procedure Set_NAFTAProducer(Value: WideString);
    procedure Set_NAFTABlanketPeriodStart(Value: WideString);
    procedure Set_NAFTABlanketPeriodEnd(Value: WideString);
    { Methods & Properties }
    property InvoiceImporterSameAsShipTo: WideString read Get_InvoiceImporterSameAsShipTo write Set_InvoiceImporterSameAsShipTo;
    property InvoiceTermOfSale: WideString read Get_InvoiceTermOfSale write Set_InvoiceTermOfSale;
    property InvoiceReasonForExport: WideString read Get_InvoiceReasonForExport write Set_InvoiceReasonForExport;
    property InvoiceDeclarationStatement: WideString read Get_InvoiceDeclarationStatement write Set_InvoiceDeclarationStatement;
    property InvoiceAdditionalComments: WideString read Get_InvoiceAdditionalComments write Set_InvoiceAdditionalComments;
    property InvoiceCurrencyCode: WideString read Get_InvoiceCurrencyCode write Set_InvoiceCurrencyCode;
    property InvoiceDiscount: WideString read Get_InvoiceDiscount write Set_InvoiceDiscount;
    property InvoiceChargesFreight: WideString read Get_InvoiceChargesFreight write Set_InvoiceChargesFreight;
    property InvoiceChargesInsurance: WideString read Get_InvoiceChargesInsurance write Set_InvoiceChargesInsurance;
    property InvoiceChargesOther: WideString read Get_InvoiceChargesOther write Set_InvoiceChargesOther;
    property InvoiceLineTotal: WideString read Get_InvoiceLineTotal write Set_InvoiceLineTotal;
    property CustomsValueTotal: WideString read Get_CustomsValueTotal write Set_CustomsValueTotal;
    property CustomsValueCurrencyCode: WideString read Get_CustomsValueCurrencyCode write Set_CustomsValueCurrencyCode;
    property CustomsValueOrigin: WideString read Get_CustomsValueOrigin write Set_CustomsValueOrigin;
    property SEDCode: WideString read Get_SEDCode write Set_SEDCode;
    property SEDPartiesToTransaction: WideString read Get_SEDPartiesToTransaction write Set_SEDPartiesToTransaction;
    property SEDUltimateConsigneeSameAsShipTo: WideString read Get_SEDUltimateConsigneeSameAsShipTo write Set_SEDUltimateConsigneeSameAsShipTo;
    property SEDCountryTerritoryOfUltimateDestination: WideString read Get_SEDCountryTerritoryOfUltimateDestination write Set_SEDCountryTerritoryOfUltimateDestination;
    property SEDAESTransactionNumber: WideString read Get_SEDAESTransactionNumber write Set_SEDAESTransactionNumber;
    property SEDAESTransactionNumberType: WideString read Get_SEDAESTransactionNumberType write Set_SEDAESTransactionNumberType;
    property SEDRoutedTransaction: WideString read Get_SEDRoutedTransaction write Set_SEDRoutedTransaction;
    property SEDCurrencyConversion: WideString read Get_SEDCurrencyConversion write Set_SEDCurrencyConversion;
    property SEDExportCode: WideString read Get_SEDExportCode write Set_SEDExportCode;
    property COUPSPrepareCO: WideString read Get_COUPSPrepareCO write Set_COUPSPrepareCO;
    property COOwnerOrAgent: WideString read Get_COOwnerOrAgent write Set_COOwnerOrAgent;
    property NAFTAProducer: WideString read Get_NAFTAProducer write Set_NAFTAProducer;
    property NAFTABlanketPeriodStart: WideString read Get_NAFTABlanketPeriodStart write Set_NAFTABlanketPeriodStart;
    property NAFTABlanketPeriodEnd: WideString read Get_NAFTABlanketPeriodEnd write Set_NAFTABlanketPeriodEnd;
  end;

{ IXMLGoodsType }

  IXMLGoodsType = interface(IXMLNode)
    ['{BA35CB33-130E-4CFA-83D0-79C73FCC2ED2}']
    { Property Accessors }
    function Get_PartNumber: WideString;
    function Get_DescriptionOfGood: WideString;
    function Get_InvNAFTATariffCode: WideString;
    function Get_InvNAFTACOCountryTerritoryOfOrigin: WideString;
    function Get_InvoiceUnits: WideString;
    function Get_InvoiceUnitOfMeasure: WideString;
    function Get_InvoiceSEDUnitPrice: WideString;
    function Get_InvoiceCurrencyCode: WideString;
    function Get_InvoiceNetWeight: WideString;
    function Get_SEDCOGrossWeight: WideString;
    function Get_SEDLbsOrKgs: WideString;
    function Get_SEDPrintGoodOnSED: WideString;
    function Get_InvoiceSEDCOMarksAndNumbers: WideString;
    function Get_SEDScheduleBNumber: WideString;
    function Get_SEDScheduleBUnits1: WideString;
    function Get_SEDScheduleBUnits2: WideString;
    function Get_SEDUnitsOfMeasure1: WideString;
    function Get_SEDUnitsOfMeasure2: WideString;
    function Get_SEDECCN: WideString;
    function Get_SEDException: WideString;
    function Get_SEDLicenceNumber: WideString;
    function Get_SEDLicenceNumberExpirationDate: WideString;
    function Get_NAFTAPrintGoodOnNAFTA: WideString;
    function Get_NAFTAPreferenceCriterion: WideString;
    function Get_NAFTAProducer: WideString;
    function Get_NAFTAMultipleCountriesTerritoriesOfOrigin: WideString;
    function Get_NAFTANetCostMethod: WideString;
    function Get_NAFTANetCostPeriodStartDate: WideString;
    function Get_NAFTANetCostPeriodEndDate: WideString;
    function Get_COPrintGoodOnCO: WideString;
    function Get_CONumberOfPackageContainingGood: WideString;
    procedure Set_PartNumber(Value: WideString);
    procedure Set_DescriptionOfGood(Value: WideString);
    procedure Set_InvNAFTATariffCode(Value: WideString);
    procedure Set_InvNAFTACOCountryTerritoryOfOrigin(Value: WideString);
    procedure Set_InvoiceUnits(Value: WideString);
    procedure Set_InvoiceUnitOfMeasure(Value: WideString);
    procedure Set_InvoiceSEDUnitPrice(Value: WideString);
    procedure Set_InvoiceCurrencyCode(Value: WideString);
    procedure Set_InvoiceNetWeight(Value: WideString);
    procedure Set_SEDCOGrossWeight(Value: WideString);
    procedure Set_SEDLbsOrKgs(Value: WideString);
    procedure Set_SEDPrintGoodOnSED(Value: WideString);
    procedure Set_InvoiceSEDCOMarksAndNumbers(Value: WideString);
    procedure Set_SEDScheduleBNumber(Value: WideString);
    procedure Set_SEDScheduleBUnits1(Value: WideString);
    procedure Set_SEDScheduleBUnits2(Value: WideString);
    procedure Set_SEDUnitsOfMeasure1(Value: WideString);
    procedure Set_SEDUnitsOfMeasure2(Value: WideString);
    procedure Set_SEDECCN(Value: WideString);
    procedure Set_SEDException(Value: WideString);
    procedure Set_SEDLicenceNumber(Value: WideString);
    procedure Set_SEDLicenceNumberExpirationDate(Value: WideString);
    procedure Set_NAFTAPrintGoodOnNAFTA(Value: WideString);
    procedure Set_NAFTAPreferenceCriterion(Value: WideString);
    procedure Set_NAFTAProducer(Value: WideString);
    procedure Set_NAFTAMultipleCountriesTerritoriesOfOrigin(Value: WideString);
    procedure Set_NAFTANetCostMethod(Value: WideString);
    procedure Set_NAFTANetCostPeriodStartDate(Value: WideString);
    procedure Set_NAFTANetCostPeriodEndDate(Value: WideString);
    procedure Set_COPrintGoodOnCO(Value: WideString);
    procedure Set_CONumberOfPackageContainingGood(Value: WideString);
    { Methods & Properties }
    property PartNumber: WideString read Get_PartNumber write Set_PartNumber;
    property DescriptionOfGood: WideString read Get_DescriptionOfGood write Set_DescriptionOfGood;
    property InvNAFTATariffCode: WideString read Get_InvNAFTATariffCode write Set_InvNAFTATariffCode;
    property InvNAFTACOCountryTerritoryOfOrigin: WideString read Get_InvNAFTACOCountryTerritoryOfOrigin write Set_InvNAFTACOCountryTerritoryOfOrigin;
    property InvoiceUnits: WideString read Get_InvoiceUnits write Set_InvoiceUnits;
    property InvoiceUnitOfMeasure: WideString read Get_InvoiceUnitOfMeasure write Set_InvoiceUnitOfMeasure;
    property InvoiceSEDUnitPrice: WideString read Get_InvoiceSEDUnitPrice write Set_InvoiceSEDUnitPrice;
    property InvoiceCurrencyCode: WideString read Get_InvoiceCurrencyCode write Set_InvoiceCurrencyCode;
    property InvoiceNetWeight: WideString read Get_InvoiceNetWeight write Set_InvoiceNetWeight;
    property SEDCOGrossWeight: WideString read Get_SEDCOGrossWeight write Set_SEDCOGrossWeight;
    property SEDLbsOrKgs: WideString read Get_SEDLbsOrKgs write Set_SEDLbsOrKgs;
    property SEDPrintGoodOnSED: WideString read Get_SEDPrintGoodOnSED write Set_SEDPrintGoodOnSED;
    property InvoiceSEDCOMarksAndNumbers: WideString read Get_InvoiceSEDCOMarksAndNumbers write Set_InvoiceSEDCOMarksAndNumbers;
    property SEDScheduleBNumber: WideString read Get_SEDScheduleBNumber write Set_SEDScheduleBNumber;
    property SEDScheduleBUnits1: WideString read Get_SEDScheduleBUnits1 write Set_SEDScheduleBUnits1;
    property SEDScheduleBUnits2: WideString read Get_SEDScheduleBUnits2 write Set_SEDScheduleBUnits2;
    property SEDUnitsOfMeasure1: WideString read Get_SEDUnitsOfMeasure1 write Set_SEDUnitsOfMeasure1;
    property SEDUnitsOfMeasure2: WideString read Get_SEDUnitsOfMeasure2 write Set_SEDUnitsOfMeasure2;
    property SEDECCN: WideString read Get_SEDECCN write Set_SEDECCN;
    property SEDException: WideString read Get_SEDException write Set_SEDException;
    property SEDLicenceNumber: WideString read Get_SEDLicenceNumber write Set_SEDLicenceNumber;
    property SEDLicenceNumberExpirationDate: WideString read Get_SEDLicenceNumberExpirationDate write Set_SEDLicenceNumberExpirationDate;
    property NAFTAPrintGoodOnNAFTA: WideString read Get_NAFTAPrintGoodOnNAFTA write Set_NAFTAPrintGoodOnNAFTA;
    property NAFTAPreferenceCriterion: WideString read Get_NAFTAPreferenceCriterion write Set_NAFTAPreferenceCriterion;
    property NAFTAProducer: WideString read Get_NAFTAProducer write Set_NAFTAProducer;
    property NAFTAMultipleCountriesTerritoriesOfOrigin: WideString read Get_NAFTAMultipleCountriesTerritoriesOfOrigin write Set_NAFTAMultipleCountriesTerritoriesOfOrigin;
    property NAFTANetCostMethod: WideString read Get_NAFTANetCostMethod write Set_NAFTANetCostMethod;
    property NAFTANetCostPeriodStartDate: WideString read Get_NAFTANetCostPeriodStartDate write Set_NAFTANetCostPeriodStartDate;
    property NAFTANetCostPeriodEndDate: WideString read Get_NAFTANetCostPeriodEndDate write Set_NAFTANetCostPeriodEndDate;
    property COPrintGoodOnCO: WideString read Get_COPrintGoodOnCO write Set_COPrintGoodOnCO;
    property CONumberOfPackageContainingGood: WideString read Get_CONumberOfPackageContainingGood write Set_CONumberOfPackageContainingGood;
  end;

{ IXMLGoodsTypeList }

  IXMLGoodsTypeList = interface(IXMLNodeCollection)
    ['{EFB817BB-9F2F-4C4A-8978-800ECD9D9807}']
    { Methods & Properties }
    function Add: IXMLGoodsType;
    function Insert(const Index: Integer): IXMLGoodsType;
    function Get_Item(Index: Integer): IXMLGoodsType;
    property Items[Index: Integer]: IXMLGoodsType read Get_Item; default;
  end;

{ IXMLProcessMessageType }

  IXMLProcessMessageType = interface(IXMLNode)
    ['{03420127-ECFB-4958-995D-47C9063544E6}']
    { Property Accessors }
    function Get_Error: IXMLErrorType;
    function Get_ShipmentRates: IXMLShipmentRatesType;
    function Get_TrackingNumbers: IXMLTrackingNumbersType;
    function Get_ImportID: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    procedure Set_ImportID(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    { Methods & Properties }
    property Error: IXMLErrorType read Get_Error;
    property ShipmentRates: IXMLShipmentRatesType read Get_ShipmentRates;
    property TrackingNumbers: IXMLTrackingNumbersType read Get_TrackingNumbers;
    property ImportID: WideString read Get_ImportID write Set_ImportID;
    property Reference1: WideString read Get_Reference1 write Set_Reference1;
    property Reference2: WideString read Get_Reference2 write Set_Reference2;
  end;

{ IXMLProcessMessageTypeList }

  IXMLProcessMessageTypeList = interface(IXMLNodeCollection)
    ['{08443799-DC27-4314-817B-D6F673BB7B68}']
    { Methods & Properties }
    function Add: IXMLProcessMessageType;
    function Insert(const Index: Integer): IXMLProcessMessageType;
    function Get_Item(Index: Integer): IXMLProcessMessageType;
    property Items[Index: Integer]: IXMLProcessMessageType read Get_Item; default;
  end;

{ IXMLErrorType }

  IXMLErrorType = interface(IXMLNode)
    ['{5E199EA6-76B9-43C6-A543-78FFD7C38C7F}']
    { Property Accessors }
    function Get_ErrorMessage: WideString;
    procedure Set_ErrorMessage(Value: WideString);
    { Methods & Properties }
    property ErrorMessage: WideString read Get_ErrorMessage write Set_ErrorMessage;
  end;

{ IXMLShipmentRatesType }

  IXMLShipmentRatesType = interface(IXMLNode)
    ['{8E8E0C5A-3600-4F1B-99A5-9C28D00E03D2}']
    { Property Accessors }
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_ShipperCharges: IXMLShipperChargesType;
    function Get_ReceiverCharges: IXMLReceiverChargesType;
    function Get_ShipmentandHandlingCharges: IXMLShipmentandHandlingChargesType;
    function Get_Declared_Value: IXMLDeclared_ValueType;
    function Get_C_O_D: IXMLC_O_DType;
    function Get_Return_Services: IXMLReturn_ServicesType;
    function Get_Saturday_Delivery: IXMLSaturday_DeliveryType;
    function Get_Saturday_Pickup: IXMLSaturday_PickupType;
    function Get_Proactive_Response: IXMLProactive_ResponseType;
    function Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
    function Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
    function Get_PackageRates: IXMLPackageRatesType;
    { Methods & Properties }
    property ShipmentCharges: IXMLShipmentChargesType read Get_ShipmentCharges;
    property ShipperCharges: IXMLShipperChargesType read Get_ShipperCharges;
    property ReceiverCharges: IXMLReceiverChargesType read Get_ReceiverCharges;
    property ShipmentandHandlingCharges: IXMLShipmentandHandlingChargesType read Get_ShipmentandHandlingCharges;
    property Declared_Value: IXMLDeclared_ValueType read Get_Declared_Value;
    property C_O_D: IXMLC_O_DType read Get_C_O_D;
    property Return_Services: IXMLReturn_ServicesType read Get_Return_Services;
    property Saturday_Delivery: IXMLSaturday_DeliveryType read Get_Saturday_Delivery;
    property Saturday_Pickup: IXMLSaturday_PickupType read Get_Saturday_Pickup;
    property Proactive_Response: IXMLProactive_ResponseType read Get_Proactive_Response;
    property Delivery_Confirmation: IXMLDelivery_ConfirmationType read Get_Delivery_Confirmation;
    property Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType read Get_Delivery_AreaSurcharge;
    property PackageRates: IXMLPackageRatesType read Get_PackageRates;
  end;

{ IXMLShipmentChargesType }

  IXMLShipmentChargesType = interface(IXMLNode)
    ['{56BAD0DE-CEF4-45D5-98BA-BA979D8FABB8}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLRateType }

  IXMLRateType = interface(IXMLNode)
    ['{38A5BB20-524A-4FBE-A7C5-21CB87F97967}']
    { Property Accessors }
    function Get_Reference: WideString;
    function Get_Published_: WideString;
    function Get_CCC: WideString;
    function Get_Negotiated: WideString;
    procedure Set_Reference(Value: WideString);
    procedure Set_Published_(Value: WideString);
    procedure Set_CCC(Value: WideString);
    procedure Set_Negotiated(Value: WideString);
    { Methods & Properties }
    property Reference: WideString read Get_Reference write Set_Reference;
    property Published_: WideString read Get_Published_ write Set_Published_;
    property CCC: WideString read Get_CCC write Set_CCC;
    property Negotiated: WideString read Get_Negotiated write Set_Negotiated;
  end;

{ IXMLShipperChargesType }

  IXMLShipperChargesType = interface(IXMLNode)
    ['{794FD434-6620-4271-A545-9C683087F5BE}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLReceiverChargesType }

  IXMLReceiverChargesType = interface(IXMLNode)
    ['{AA0747F1-2483-4812-8572-4B047E395FF3}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLShipmentandHandlingChargesType }

  IXMLShipmentandHandlingChargesType = interface(IXMLNode)
    ['{27C55036-F711-4027-A3F6-E5D739592D48}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLDeclared_ValueType }

  IXMLDeclared_ValueType = interface(IXMLNode)
    ['{8A60FEE7-0139-474C-B69F-1C2817942036}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLC_O_DType }

  IXMLC_O_DType = interface(IXMLNode)
    ['{03D7055E-2373-4B92-9C27-454A83E87178}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLReturn_ServicesType }

  IXMLReturn_ServicesType = interface(IXMLNode)
    ['{626F0C1B-8F08-4915-8765-1D98A9F63E8C}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLSaturday_DeliveryType }

  IXMLSaturday_DeliveryType = interface(IXMLNode)
    ['{3023AF0B-EE49-4987-894D-130BDC94FDFA}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLSaturday_PickupType }

  IXMLSaturday_PickupType = interface(IXMLNode)
    ['{AFF651D9-BBAF-409F-905F-10413C4BB2A1}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLProactive_ResponseType }

  IXMLProactive_ResponseType = interface(IXMLNode)
    ['{548C739A-7790-4D7C-B53B-C4BB5E166D15}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLDelivery_ConfirmationType }

  IXMLDelivery_ConfirmationType = interface(IXMLNode)
    ['{8AA292CB-A45C-4B40-87DA-CAA8E055982A}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLDelivery_AreaSurchargeType }

  IXMLDelivery_AreaSurchargeType = interface(IXMLNode)
    ['{2EE7E907-73A1-4109-B8BC-C22F90F3E0FB}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLPackageRatesType }

  IXMLPackageRatesType = interface(IXMLNodeCollection)
    ['{41F51536-D4AC-4EA8-A1CE-8CDADB275A9D}']
    { Property Accessors }
    function Get_PackageRate(Index: Integer): IXMLPackageRateType;
    { Methods & Properties }
    function Add: IXMLPackageRateType;
    function Insert(const Index: Integer): IXMLPackageRateType;
    property PackageRate[Index: Integer]: IXMLPackageRateType read Get_PackageRate; default;
  end;

{ IXMLPackageRateType }

  IXMLPackageRateType = interface(IXMLNode)
    ['{EC8D85E4-DF33-4AA4-9FF4-7737D76498DB}']
    { Property Accessors }
    function Get_TrackingNumber: WideString;
    function Get_USPSNumber: WideString;
    function Get_PackageCharges: IXMLPackageChargesType;
    function Get_Additional_Handling: IXMLAdditional_HandlingType;
    function Get_C_O_D: IXMLC_O_DType;
    function Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
    function Get_Verbal_Confirmation: IXMLVerbal_ConfirmationType;
    function Get_Declared_Value: IXMLDeclared_ValueType;
    function Get_QVN: IXMLQVNType;
    function Get_Proactive_Response: IXMLProactive_ResponseType;
    function Get_Dry_Ice: IXMLDry_IceType;
    function Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
    procedure Set_TrackingNumber(Value: WideString);
    procedure Set_USPSNumber(Value: WideString);
    { Methods & Properties }
    property TrackingNumber: WideString read Get_TrackingNumber write Set_TrackingNumber;
    property USPSNumber: WideString read Get_USPSNumber write Set_USPSNumber;
    property PackageCharges: IXMLPackageChargesType read Get_PackageCharges;
    property Additional_Handling: IXMLAdditional_HandlingType read Get_Additional_Handling;
    property C_O_D: IXMLC_O_DType read Get_C_O_D;
    property Delivery_Confirmation: IXMLDelivery_ConfirmationType read Get_Delivery_Confirmation;
    property Verbal_Confirmation: IXMLVerbal_ConfirmationType read Get_Verbal_Confirmation;
    property Declared_Value: IXMLDeclared_ValueType read Get_Declared_Value;
    property QVN: IXMLQVNType read Get_QVN;
    property Proactive_Response: IXMLProactive_ResponseType read Get_Proactive_Response;
    property Dry_Ice: IXMLDry_IceType read Get_Dry_Ice;
    property Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType read Get_Delivery_AreaSurcharge;
  end;

{ IXMLPackageChargesType }

  IXMLPackageChargesType = interface(IXMLNode)
    ['{79D961F0-C185-4C5C-96A9-8A7AC63A28C5}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLAdditional_HandlingType }

  IXMLAdditional_HandlingType = interface(IXMLNode)
    ['{1D719A75-2DE0-470D-81A3-59B07B242496}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLVerbal_ConfirmationType }

  IXMLVerbal_ConfirmationType = interface(IXMLNode)
    ['{3B824BE2-C05D-419E-A257-68C3123E7C30}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLQVNType }

  IXMLQVNType = interface(IXMLNode)
    ['{1CF307D2-BA69-4F63-B4C4-053311D124BB}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLDry_IceType }

  IXMLDry_IceType = interface(IXMLNode)
    ['{6F4AAB41-F545-4EA9-B264-2F01EA6F4414}']
    { Property Accessors }
    function Get_Rate: IXMLRateType;
    { Methods & Properties }
    property Rate: IXMLRateType read Get_Rate;
  end;

{ IXMLTrackingNumbersType }

  IXMLTrackingNumbersType = interface(IXMLNodeCollection)
    ['{B46D0A9D-B5A5-4603-A2D4-23D66EE78C7A}']
    { Property Accessors }
    function Get_TrackingNumber(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const TrackingNumber: WideString): IXMLNode;
    function Insert(const Index: Integer; const TrackingNumber: WideString): IXMLNode;
    property TrackingNumber[Index: Integer]: WideString read Get_TrackingNumber; default;
  end;

{ Forward Decls }

  TXMLOpenShipmentsType = class;
  TXMLOpenShipmentType = class;
  TXMLReceiverType = class;
  TXMLReturnToType = class;
  TXMLShipForType = class;
  TXMLShipperType = class;
  TXMLShipmentType = class;
  TXMLThirdPartyType = class;
  TXMLDeclareValueType = class;
  TXMLAdditionalHandlingType = class;
  TXMLCODType = class;
  TXMLAODTagType = class;
  TXMLShipNotificationType = class;
  TXMLQuantumViewNotifyDetailsType = class;
  TXMLQuantumViewNotifyType = class;
  TXMLQuantumViewNotifyTypeList = class;
  TXMLSpecialCommoditiesType = class;
  TXMLReturnNotificationDetailsType = class;
  TXMLReturnNotificationType = class;
  TXMLReturnNotificationTypeList = class;
  TXMLUPSExchangeCollectType = class;
  TXMLReturnServiceType = class;
  TXMLInvoiceType = class;
  TXMLLineItemType = class;
  TXMLLineItemTypeList = class;
  TXMLProductType = class;
  TXMLSoldToType = class;
  TXMLShipToType = class;
  TXMLShipFromType = class;
  TXMLProducerType = class;
  TXMLUltimateConsigneeType = class;
  TXMLImporterType = class;
  TXMLThirdPartyReceiverType = class;
  TXMLShipmentInformationType = class;
  TXMLDeclaredValueType = class;
  TXMLQVNOptionType = class;
  TXMLQVNRecipientAndNotificationTypesType = class;
  TXMLQVNRecipientAndNotificationTypesTypeList = class;
  TXMLDeliveryConfirmationType = class;
  TXMLNotifyBeforeDelOptionType = class;
  TXMLHandlingChargeOptionType = class;
  TXMLLTLLineItemType = class;
  TXMLLTLLineItemTypeList = class;
  TXMLPackageType = class;
  TXMLPackageTypeList = class;
  TXMLQVNOrReturnNotificationOptionType = class;
  TXMLQVNOrReturnRecipientAndNotificationTypesType = class;
  TXMLQVNOrReturnRecipientAndNotificationTypesTypeList = class;
  TXMLVerbalConfirmationOptionType = class;
  TXMLFlexibleParcelInsuranceOptionType = class;
  TXMLDryIceOptionType = class;
  TXMLInternationalDocumentationType = class;
  TXMLGoodsType = class;
  TXMLGoodsTypeList = class;
  TXMLProcessMessageType = class;
  TXMLProcessMessageTypeList = class;
  TXMLErrorType = class;
  TXMLShipmentRatesType = class;
  TXMLShipmentChargesType = class;
  TXMLRateType = class;
  TXMLShipperChargesType = class;
  TXMLReceiverChargesType = class;
  TXMLShipmentandHandlingChargesType = class;
  TXMLDeclared_ValueType = class;
  TXMLC_O_DType = class;
  TXMLReturn_ServicesType = class;
  TXMLSaturday_DeliveryType = class;
  TXMLSaturday_PickupType = class;
  TXMLProactive_ResponseType = class;
  TXMLDelivery_ConfirmationType = class;
  TXMLDelivery_AreaSurchargeType = class;
  TXMLPackageRatesType = class;
  TXMLPackageRateType = class;
  TXMLPackageChargesType = class;
  TXMLAdditional_HandlingType = class;
  TXMLVerbal_ConfirmationType = class;
  TXMLQVNType = class;
  TXMLDry_IceType = class;
  TXMLTrackingNumbersType = class;

{ TXMLOpenShipmentsType }

  TXMLOpenShipmentsType = class(TXMLNodeCollection, IXMLUPSOpenShipmentsType)
  protected
    { IXMLUPSOpenShipmentsType }
    function Get_OpenShipment(Index: Integer): IXMLOpenShipmentType;
    function Add: IXMLOpenShipmentType;
    function Insert(const Index: Integer): IXMLOpenShipmentType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLOpenShipmentType }

  TXMLOpenShipmentType = class(TXMLNode, IXMLOpenShipmentType)
  private
    FLTLLineItem: IXMLLTLLineItemTypeList;
    FPackage: IXMLPackageTypeList;
    FGoods: IXMLGoodsTypeList;
    FProcessMessage: IXMLProcessMessageTypeList;
  protected
    { IXMLOpenShipmentType }
    function Get_ProcessStatus: WideString;
    function Get_ShipmentOption: WideString;
    //function Get_ProcessStatus: WideString;
    //function Get_ShipmentOption: WideString;
    function Get_Receiver: IXMLReceiverType;
    function Get_ReturnTo: IXMLReturnToType;
    function Get_ShipFor: IXMLShipForType;
    function Get_Shipper: IXMLShipperType;
    function Get_Shipment: IXMLShipmentType;
    function Get_Invoice: IXMLInvoiceType;
    function Get_ShipTo: IXMLShipToType;
    function Get_ShipFrom: IXMLShipFromType;
    function Get_Producer: IXMLProducerType;
    function Get_UltimateConsignee: IXMLUltimateConsigneeType;
    function Get_Importer: IXMLImporterType;
    function Get_ThirdParty: IXMLThirdPartyType;
    function Get_ThirdPartyReceiver: IXMLThirdPartyReceiverType;
    function Get_ShipmentInformation: IXMLShipmentInformationType;
    function Get_LTLLineItem: IXMLLTLLineItemTypeList;
    function Get_Package: IXMLPackageTypeList;
    function Get_InternationalDocumentation: IXMLInternationalDocumentationType;
    function Get_Goods: IXMLGoodsTypeList;
    function Get_ProcessMessage: IXMLProcessMessageTypeList;
    procedure Set_ProcessStatus(Value: WideString);
    procedure Set_ShipmentOption(Value: WideString);
   //procedure Set_ProcessStatus(Value: WideString);
   // procedure Set_ShipmentOption(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReceiverType }

  TXMLReceiverType = class(TXMLNode, IXMLReceiverType)
  protected
    { IXMLReceiverType }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_LocationID: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    function Get_EmailAddress1: WideString;
    function Get_EmailAddress2: WideString;
    function Get_EmailContact1: WideString;
    function Get_EmailContact2: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_LocationID(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
    procedure Set_EmailAddress2(Value: WideString);
    procedure Set_EmailContact1(Value: WideString);
    procedure Set_EmailContact2(Value: WideString);
  end;

{ TXMLReturnToType }

  TXMLReturnToType = class(TXMLNode, IXMLReturnToType)
  protected
    { IXMLReturnToType }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_EmailAddress1: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
  end;

{ TXMLShipForType }

  TXMLShipForType = class(TXMLNode, IXMLShipForType)
  protected
    { IXMLShipForType }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_EmailAddress1: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_EmailAddress1(Value: WideString);
  end;

{ TXMLShipperType }

  TXMLShipperType = class(TXMLNode, IXMLShipperType)
  protected
    { IXMLShipperType }
    function Get_UpsAccountNumber: WideString;
    procedure Set_UpsAccountNumber(Value: WideString);
  end;

{ TXMLShipmentType }

  TXMLShipmentType = class(TXMLNode, IXMLShipmentType)
  protected
    { IXMLShipmentType }
    function Get_ServiceLevel: WideString;
    function Get_PackageType: WideString;
    function Get_NumberOfPackages: WideString;
    function Get_ShipmentActualWeight: WideString;
    function Get_ShipmentDimensionalWeight: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_CostAllocationCode: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_DocumentOnly: WideString;
    function Get_GoodsNotInFreeCirculation: WideString;
    function Get_BillingOption: WideString;
    function Get_ThirdParty: IXMLThirdPartyType;
    function Get_SaturdayDelivery: WideString;
    function Get_DeclareValue: IXMLDeclareValueType;
    function Get_AdditionalHandling: IXMLAdditionalHandlingType;
    function Get_COD: IXMLCODType;
    function Get_AODTag: IXMLAODTagType;
    function Get_ShipNotification: IXMLShipNotificationType;
    function Get_QuantumViewNotifyDetails: IXMLQuantumViewNotifyDetailsType;
    function Get_SpecialCommodities: IXMLSpecialCommoditiesType;
    function Get_SignatureAdultSignatureRequired: WideString;
    function Get_ReturnNotificationDetails: IXMLReturnNotificationDetailsType;
    function Get_CollectionDate: WideString;
    function Get_ImportID: WideString;
    function Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
    function Get_ReturnService: IXMLReturnServiceType;
    procedure Set_ServiceLevel(Value: WideString);
    procedure Set_PackageType(Value: WideString);
    procedure Set_NumberOfPackages(Value: WideString);
    procedure Set_ShipmentActualWeight(Value: WideString);
    procedure Set_ShipmentDimensionalWeight(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_CostAllocationCode(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_DocumentOnly(Value: WideString);
    procedure Set_GoodsNotInFreeCirculation(Value: WideString);
    procedure Set_BillingOption(Value: WideString);
    procedure Set_SaturdayDelivery(Value: WideString);
    procedure Set_SignatureAdultSignatureRequired(Value: WideString);
    procedure Set_CollectionDate(Value: WideString);
    procedure Set_ImportID(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLThirdPartyType }

  TXMLThirdPartyType = class(TXMLNode, IXMLThirdPartyType)
  protected
    { IXMLThirdPartyType }
    function Get_CustomerID: WideString;
    function Get_CompanyName: WideString;
    function Get_CompanyOrName: WideString;
    function Get_ContactPerson: WideString;
    function Get_Attention: WideString;
    function Get_AddressLine1: WideString;
    function Get_Address1: WideString;
    function Get_AddressLine2: WideString;
    function Get_Address2: WideString;
    function Get_AddressLine3: WideString;
    function Get_Address3: WideString;
    function Get_City: WideString;
    function Get_CityOrTown: WideString;
    function Get_CountryCode: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Residential: WideString;
    function Get_ResidentialIndicator: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyName(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
  end;

{ TXMLDeclareValueType }

  TXMLDeclareValueType = class(TXMLNode, IXMLDeclareValueType)
  protected
    { IXMLDeclareValueType }
    function Get_Amount: WideString;
    procedure Set_Amount(Value: WideString);
  end;

{ TXMLAdditionalHandlingType }

  TXMLAdditionalHandlingType = class(TXMLNode, IXMLAdditionalHandlingType)
  protected
    { IXMLAdditionalHandlingType }
    function Get_NumberOfAdditionalHandling: WideString;
    procedure Set_NumberOfAdditionalHandling(Value: WideString);
  end;

{ TXMLCODType }

  TXMLCODType = class(TXMLNode, IXMLCODType)
  protected
    { IXMLCODType }
    function Get_CashOnly: WideString;
    function Get_CashierCheckorMoneyOrderOnlyIndicator: WideString;
    function Get_AddShippingChargesToCODIndicator: WideString;
    function Get_Amount: WideString;
    function Get_Currency: WideString;
    procedure Set_CashOnly(Value: WideString);
    procedure Set_CashierCheckorMoneyOrderOnlyIndicator(Value: WideString);
    procedure Set_AddShippingChargesToCODIndicator(Value: WideString);
    procedure Set_Amount(Value: WideString);
    procedure Set_Currency(Value: WideString);
  end;

{ TXMLAODTagType }

  TXMLAODTagType = class(TXMLNode, IXMLAODTagType)
  protected
    { IXMLAODTagType }
    function Get_NumberOfAOD: WideString;
    procedure Set_NumberOfAOD(Value: WideString);
  end;

{ TXMLShipNotificationType }

  TXMLShipNotificationType = class(TXMLNode, IXMLShipNotificationType)
  protected
    { IXMLShipNotificationType }
    function Get_EmailText: WideString;
    procedure Set_EmailText(Value: WideString);
  end;

{ TXMLQuantumViewNotifyDetailsType }

  TXMLQuantumViewNotifyDetailsType = class(TXMLNode, IXMLQuantumViewNotifyDetailsType)
  private
    FQuantumViewNotify: IXMLQuantumViewNotifyTypeList;
  protected
    { IXMLQuantumViewNotifyDetailsType }
    function Get_QuantumViewNotify: IXMLQuantumViewNotifyTypeList;
    function Get_FailureNotificationEMailAddress: WideString;
    function Get_ShipperCompanyName: WideString;
    function Get_SubjectLineOptions: WideString;
    function Get_NotificationMessage: WideString;
    procedure Set_FailureNotificationEMailAddress(Value: WideString);
    procedure Set_ShipperCompanyName(Value: WideString);
    procedure Set_SubjectLineOptions(Value: WideString);
    procedure Set_NotificationMessage(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLQuantumViewNotifyType }

  TXMLQuantumViewNotifyType = class(TXMLNode, IXMLQuantumViewNotifyType)
  protected
    { IXMLQuantumViewNotifyType }
    function Get_NotificationEMailAddress: WideString;
    function Get_NotificationRequest: WideString;
    procedure Set_NotificationEMailAddress(Value: WideString);
    procedure Set_NotificationRequest(Value: WideString);
  end;

{ TXMLQuantumViewNotifyTypeList }

  TXMLQuantumViewNotifyTypeList = class(TXMLNodeCollection, IXMLQuantumViewNotifyTypeList)
  protected
    { IXMLQuantumViewNotifyTypeList }
    function Add: IXMLQuantumViewNotifyType;
    function Insert(const Index: Integer): IXMLQuantumViewNotifyType;
    function Get_Item(Index: Integer): IXMLQuantumViewNotifyType;
  end;

{ TXMLSpecialCommoditiesType }

  TXMLSpecialCommoditiesType = class(TXMLNode, IXMLSpecialCommoditiesType)
  protected
    { IXMLSpecialCommoditiesType }
    function Get_CommoditiesDetails: WideString;
    function Get_ISCAlcoholicBeverages: WideString;
    function Get_ISCDiagnosticSpecimens: WideString;
    function Get_ISCPerishables: WideString;
    function Get_ISCPlants: WideString;
    function Get_ISCSeeds: WideString;
    function Get_ISCSpecialException: WideString;
    function Get_ISCTobacco: WideString;
    procedure Set_CommoditiesDetails(Value: WideString);
    procedure Set_ISCAlcoholicBeverages(Value: WideString);
    procedure Set_ISCDiagnosticSpecimens(Value: WideString);
    procedure Set_ISCPerishables(Value: WideString);
    procedure Set_ISCPlants(Value: WideString);
    procedure Set_ISCSeeds(Value: WideString);
    procedure Set_ISCSpecialException(Value: WideString);
    procedure Set_ISCTobacco(Value: WideString);
  end;

{ TXMLReturnNotificationDetailsType }

  TXMLReturnNotificationDetailsType = class(TXMLNode, IXMLReturnNotificationDetailsType)
  private
    FReturnNotification: IXMLReturnNotificationTypeList;
  protected
    { IXMLReturnNotificationDetailsType }
    function Get_ReturnNotification: IXMLReturnNotificationTypeList;
    function Get_FailureNotificationEMailAddress: WideString;
    function Get_ShipperCompanyName: WideString;
    function Get_SubjectLineOptions: WideString;
    function Get_NotificationMessage: WideString;
    procedure Set_FailureNotificationEMailAddress(Value: WideString);
    procedure Set_ShipperCompanyName(Value: WideString);
    procedure Set_SubjectLineOptions(Value: WideString);
    procedure Set_NotificationMessage(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReturnNotificationType }

  TXMLReturnNotificationType = class(TXMLNodeCollection, IXMLReturnNotificationType)
  protected
    { IXMLReturnNotificationType }
    function Get_NotificationEMailAddress(Index: Integer): WideString;
    function Add(const NotificationEMailAddress: WideString): IXMLNode;
    function Insert(const Index: Integer; const NotificationEMailAddress: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReturnNotificationTypeList }

  TXMLReturnNotificationTypeList = class(TXMLNodeCollection, IXMLReturnNotificationTypeList)
  protected
    { IXMLReturnNotificationTypeList }
    function Add: IXMLReturnNotificationType;
    function Insert(const Index: Integer): IXMLReturnNotificationType;
    function Get_Item(Index: Integer): IXMLReturnNotificationType;
  end;

{ TXMLUPSExchangeCollectType }

  TXMLUPSExchangeCollectType = class(TXMLNode, IXMLUPSExchangeCollectType)
  protected
    { IXMLUPSExchangeCollectType }
    function Get_BuyerEMailAddress: WideString;
    function Get_SellerEMailAddress: WideString;
    function Get_ExchangeCollectAmount: WideString;
    function Get_Currency: WideString;
    function Get_BuyerPercentageOfServiceFee: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_AdditionalComments: WideString;
    procedure Set_BuyerEMailAddress(Value: WideString);
    procedure Set_SellerEMailAddress(Value: WideString);
    procedure Set_ExchangeCollectAmount(Value: WideString);
    procedure Set_Currency(Value: WideString);
    procedure Set_BuyerPercentageOfServiceFee(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_AdditionalComments(Value: WideString);
  end;

{ TXMLReturnServiceType }

  TXMLReturnServiceType = class(TXMLNode, IXMLReturnServiceType)
  protected
    { IXMLReturnServiceType }
    function Get_Options: WideString;
    function Get_PrintAddressOnlyInvoice: WideString;
    function Get_SenderCompanyName: WideString;
    function Get_RecipientEmailAddress: WideString;
    function Get_FailureOfDeliveryEmailAddress: WideString;
    function Get_InstructionAndReceiptLanguage: WideString;
    function Get_MerchandiseDescOfPackage: WideString;
    procedure Set_Options(Value: WideString);
    procedure Set_PrintAddressOnlyInvoice(Value: WideString);
    procedure Set_SenderCompanyName(Value: WideString);
    procedure Set_RecipientEmailAddress(Value: WideString);
    procedure Set_FailureOfDeliveryEmailAddress(Value: WideString);
    procedure Set_InstructionAndReceiptLanguage(Value: WideString);
    procedure Set_MerchandiseDescOfPackage(Value: WideString);
  end;

{ TXMLInvoiceType }

  TXMLInvoiceType = class(TXMLNode, IXMLInvoiceType)
  private
    FLineItem: IXMLLineItemTypeList;
  protected
    { IXMLInvoiceType }
    function Get_ReasonForExport: WideString;
    function Get_TermOfSale: WideString;
    function Get_DeclarationStatement: WideString;
    function Get_AdditionalComments: WideString;
    function Get_InvoiceCurrency: WideString;
    function Get_InvoiceLineTotal: WideString;
    function Get_DiscountRebate: WideString;
    function Get_Freight: WideString;
    function Get_Insurance: WideString;
    function Get_OtherCharges: WideString;
    function Get_TotalInvoiceAmount: WideString;
    function Get_LineItem: IXMLLineItemTypeList;
    function Get_SoldTo: IXMLSoldToType;
    procedure Set_ReasonForExport(Value: WideString);
    procedure Set_TermOfSale(Value: WideString);
    procedure Set_DeclarationStatement(Value: WideString);
    procedure Set_AdditionalComments(Value: WideString);
    procedure Set_InvoiceCurrency(Value: WideString);
    procedure Set_InvoiceLineTotal(Value: WideString);
    procedure Set_DiscountRebate(Value: WideString);
    procedure Set_Freight(Value: WideString);
    procedure Set_Insurance(Value: WideString);
    procedure Set_OtherCharges(Value: WideString);
    procedure Set_TotalInvoiceAmount(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLineItemType }

  TXMLLineItemType = class(TXMLNode, IXMLLineItemType)
  protected
    { IXMLLineItemType }
    function Get_Product: IXMLProductType;
    function Get_NumberOfUnits: WideString;
    procedure Set_NumberOfUnits(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLineItemTypeList }

  TXMLLineItemTypeList = class(TXMLNodeCollection, IXMLLineItemTypeList)
  protected
    { IXMLLineItemTypeList }
    function Add: IXMLLineItemType;
    function Insert(const Index: Integer): IXMLLineItemType;
    function Get_Item(Index: Integer): IXMLLineItemType;
  end;

{ TXMLProductType }

  TXMLProductType = class(TXMLNode, IXMLProductType)
  protected
    { IXMLProductType }
    function Get_ProductNumber: WideString;
    function Get_PartNumber: WideString;
    function Get_ProductName: WideString;
    function Get_HarmonizedCode: WideString;
    function Get_ProductDescription: WideString;
    function Get_UnitOfMeasure: WideString;
    function Get_CountryOfOriginCode: WideString;
    function Get_CurrencyCode: WideString;
    function Get_UnitPrice: WideString;
    procedure Set_ProductNumber(Value: WideString);
    procedure Set_PartNumber(Value: WideString);
    procedure Set_ProductName(Value: WideString);
    procedure Set_HarmonizedCode(Value: WideString);
    procedure Set_ProductDescription(Value: WideString);
    procedure Set_UnitOfMeasure(Value: WideString);
    procedure Set_CountryOfOriginCode(Value: WideString);
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_UnitPrice(Value: WideString);
  end;

{ TXMLSoldToType }

  TXMLSoldToType = class(TXMLNode, IXMLSoldToType)
  protected
    { IXMLSoldToType }
    function Get_CompanyName: WideString;
    function Get_ContactPerson: WideString;
    function Get_AddressLine1: WideString;
    function Get_AddressLine2: WideString;
    function Get_AddressLine3: WideString;
    function Get_City: WideString;
    function Get_CountryCode: WideString;
    function Get_PostalCode: WideString;
    function Get_StateOrProvince: WideString;
    function Get_Residential: WideString;
    function Get_CustomerIDNumber: WideString;
    function Get_Phone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_RecordOwner: WideString;
    procedure Set_CompanyName(Value: WideString);
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_AddressLine1(Value: WideString);
    procedure Set_AddressLine2(Value: WideString);
    procedure Set_AddressLine3(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_CountryCode(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_StateOrProvince(Value: WideString);
    procedure Set_Residential(Value: WideString);
    procedure Set_CustomerIDNumber(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_RecordOwner(Value: WideString);
  end;

{ TXMLShipToType }

  TXMLShipToType = class(TXMLNode, IXMLShipToType)
  protected
    { IXMLShipToType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_EmailAddress: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_ReceiverUpsAccountNumber: WideString;
    function Get_LocationID: WideString;
    function Get_ResidentialIndicator: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_EmailAddress(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_ReceiverUpsAccountNumber(Value: WideString);
    procedure Set_LocationID(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
  end;

{ TXMLShipFromType }

  TXMLShipFromType = class(TXMLNode, IXMLShipFromType)
  protected
    { IXMLShipFromType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_TaxIDType: WideString;
    function Get_UpsAccountNumber: WideString;
    function Get_ResidentialIndicator: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_TaxIDType(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
    procedure Set_ResidentialIndicator(Value: WideString);
  end;

{ TXMLProducerType }

  TXMLProducerType = class(TXMLNode, IXMLProducerType)
  protected
    { IXMLProducerType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
  end;

{ TXMLUltimateConsigneeType }

  TXMLUltimateConsigneeType = class(TXMLNode, IXMLUltimateConsigneeType)
  protected
    { IXMLUltimateConsigneeType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
  end;

{ TXMLImporterType }

  TXMLImporterType = class(TXMLNode, IXMLImporterType)
  protected
    { IXMLImporterType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_TaxIDNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_TaxIDNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
  end;

{ TXMLThirdPartyReceiverType }

  TXMLThirdPartyReceiverType = class(TXMLNode, IXMLThirdPartyReceiverType)
  protected
    { IXMLThirdPartyReceiverType }
    function Get_CustomerID: WideString;
    function Get_CompanyOrName: WideString;
    function Get_Attention: WideString;
    function Get_Address1: WideString;
    function Get_Address2: WideString;
    function Get_Address3: WideString;
    function Get_CountryTerritory: WideString;
    function Get_PostalCode: WideString;
    function Get_CityOrTown: WideString;
    function Get_StateProvinceCounty: WideString;
    function Get_Telephone: WideString;
    function Get_FaxNumber: WideString;
    function Get_UpsAccountNumber: WideString;
    procedure Set_CustomerID(Value: WideString);
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_Attention(Value: WideString);
    procedure Set_Address1(Value: WideString);
    procedure Set_Address2(Value: WideString);
    procedure Set_Address3(Value: WideString);
    procedure Set_CountryTerritory(Value: WideString);
    procedure Set_PostalCode(Value: WideString);
    procedure Set_CityOrTown(Value: WideString);
    procedure Set_StateProvinceCounty(Value: WideString);
    procedure Set_Telephone(Value: WideString);
    procedure Set_FaxNumber(Value: WideString);
    procedure Set_UpsAccountNumber(Value: WideString);
  end;

{ TXMLShipmentInformationType }

  TXMLShipmentInformationType = class(TXMLNode, IXMLShipmentInformationType)
  protected
    { IXMLShipmentInformationType }
    function Get_VoidIndicator: WideString;
    function Get_ServiceType: WideString;
    function Get_PackageType: WideString;
    function Get_NumberOfPackages: WideString;
    function Get_ShipmentActualWeight: WideString;
    function Get_ShipmentDimensionalWeight: WideString;
    function Get_DescriptionOfGoods: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_Reference3: WideString;
    function Get_Reference4: WideString;
    function Get_Reference5: WideString;
    function Get_DocumentOnly: WideString;
    function Get_GoodsNotInFreeCirculation: WideString;
    function Get_SpecialInstructionForShipment: WideString;
    function Get_ShipperNumber: WideString;
    function Get_BillingOption: WideString;
    function Get_BillTransportationTo: WideString;
    function Get_BillDutyTaxTo: WideString;
    function Get_SplitDutyAndTax: WideString;
    function Get_SaturdayDelivery: WideString;
    function Get_SaturdayPickUp: WideString;
    function Get_DeclaredValue: IXMLDeclaredValueType;
    function Get_MerchandiseDescriptionForPackage: WideString;
    function Get_AdditionalHandling: IXMLAdditionalHandlingType;
    function Get_COD: IXMLCODType;
    function Get_ReturnOfDocument: WideString;
    function Get_QVNOption: IXMLQVNOptionType;
    function Get_SpecialCommodities: IXMLSpecialCommoditiesType;
    function Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
    function Get_UserDefinedID: WideString;
    function Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
    function Get_ReturnService: IXMLReturnServiceType;
    function Get_NotifyBeforeDelOption: IXMLNotifyBeforeDelOptionType;
    function Get_HandlingChargeOption: IXMLHandlingChargeOptionType;
    function Get_FoodItems: WideString;
    function Get_HundredWeightPricingApplied: WideString;
    function Get_USI: WideString;
    function Get_SubProNumber: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_AdditionalDocuments: WideString;
    function Get_ProactiveResponseOption: WideString;
    procedure Set_VoidIndicator(Value: WideString);
    procedure Set_ServiceType(Value: WideString);
    procedure Set_PackageType(Value: WideString);
    procedure Set_NumberOfPackages(Value: WideString);
    procedure Set_ShipmentActualWeight(Value: WideString);
    procedure Set_ShipmentDimensionalWeight(Value: WideString);
    procedure Set_DescriptionOfGoods(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_Reference3(Value: WideString);
    procedure Set_Reference4(Value: WideString);
    procedure Set_Reference5(Value: WideString);
    procedure Set_DocumentOnly(Value: WideString);
    procedure Set_GoodsNotInFreeCirculation(Value: WideString);
    procedure Set_SpecialInstructionForShipment(Value: WideString);
    procedure Set_ShipperNumber(Value: WideString);
    procedure Set_BillingOption(Value: WideString);
    procedure Set_BillTransportationTo(Value: WideString);
    procedure Set_BillDutyTaxTo(Value: WideString);
    procedure Set_SplitDutyAndTax(Value: WideString);
    procedure Set_SaturdayDelivery(Value: WideString);
    procedure Set_SaturdayPickUp(Value: WideString);
    procedure Set_MerchandiseDescriptionForPackage(Value: WideString);
    procedure Set_ReturnOfDocument(Value: WideString);
    procedure Set_UserDefinedID(Value: WideString);
    procedure Set_FoodItems(Value: WideString);
    procedure Set_HundredWeightPricingApplied(Value: WideString);
    procedure Set_USI(Value: WideString);
    procedure Set_SubProNumber(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_AdditionalDocuments(Value: WideString);
    procedure Set_ProactiveResponseOption(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDeclaredValueType }

  TXMLDeclaredValueType = class(TXMLNode, IXMLDeclaredValueType)
  protected
    { IXMLDeclaredValueType }
    function Get_Amount: WideString;
    function Get_ShipperPaid: WideString;
    procedure Set_Amount(Value: WideString);
    procedure Set_ShipperPaid(Value: WideString);
  end;

{ TXMLQVNOptionType }

  TXMLQVNOptionType = class(TXMLNode, IXMLQVNOptionType)
  private
    FQVNRecipientAndNotificationTypes: IXMLQVNRecipientAndNotificationTypesTypeList;
  protected
    { IXMLQVNOptionType }
    function Get_QVNRecipientAndNotificationTypes: IXMLQVNRecipientAndNotificationTypesTypeList;
    function Get_ShipFromCompanyOrName: WideString;
    function Get_FailedEMailAddress: WideString;
    function Get_SubjectLine: WideString;
    function Get_Memo: WideString;
    procedure Set_ShipFromCompanyOrName(Value: WideString);
    procedure Set_FailedEMailAddress(Value: WideString);
    procedure Set_SubjectLine(Value: WideString);
    procedure Set_Memo(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLQVNRecipientAndNotificationTypesType }

  TXMLQVNRecipientAndNotificationTypesType = class(TXMLNode, IXMLQVNRecipientAndNotificationTypesType)
  protected
    { IXMLQVNRecipientAndNotificationTypesType }
    function Get_CompanyOrName: WideString;
    function Get_ContactName: WideString;
    function Get_EMailAddress: WideString;
    function Get_Ship: WideString;
    function Get_Exception: WideString;
    function Get_Delivery: WideString;
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactName(Value: WideString);
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Ship(Value: WideString);
    procedure Set_Exception(Value: WideString);
    procedure Set_Delivery(Value: WideString);
  end;

{ TXMLQVNRecipientAndNotificationTypesTypeList }

  TXMLQVNRecipientAndNotificationTypesTypeList = class(TXMLNodeCollection, IXMLQVNRecipientAndNotificationTypesTypeList)
  protected
    { IXMLQVNRecipientAndNotificationTypesTypeList }
    function Add: IXMLQVNRecipientAndNotificationTypesType;
    function Insert(const Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
    function Get_Item(Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
  end;

{ TXMLDeliveryConfirmationType }

  TXMLDeliveryConfirmationType = class(TXMLNode, IXMLDeliveryConfirmationType)
  protected
    { IXMLDeliveryConfirmationType }
    function Get_SignatureRequired: WideString;
    function Get_AdultSignatureRequired: WideString;
    procedure Set_SignatureRequired(Value: WideString);
    procedure Set_AdultSignatureRequired(Value: WideString);
  end;

{ TXMLNotifyBeforeDelOptionType }

  TXMLNotifyBeforeDelOptionType = class(TXMLNode, IXMLNotifyBeforeDelOptionType)
  protected
    { IXMLNotifyBeforeDelOptionType }
    function Get_ContactPerson: WideString;
    function Get_Phone: WideString;
    procedure Set_ContactPerson(Value: WideString);
    procedure Set_Phone(Value: WideString);
  end;

{ TXMLHandlingChargeOptionType }

  TXMLHandlingChargeOptionType = class(TXMLNode, IXMLHandlingChargeOptionType)
  protected
    { IXMLHandlingChargeOptionType }
    function Get_HandlingChargeType: WideString;
    function Get_HCAmountIncreaseDecreaseType: WideString;
    function Get_HandlingChargeFlatRate: WideString;
    function Get_HandlingChargePercentage: WideString;
    procedure Set_HandlingChargeType(Value: WideString);
    procedure Set_HCAmountIncreaseDecreaseType(Value: WideString);
    procedure Set_HandlingChargeFlatRate(Value: WideString);
    procedure Set_HandlingChargePercentage(Value: WideString);
  end;

{ TXMLLTLLineItemType }

  TXMLLTLLineItemType = class(TXMLNode, IXMLLTLLineItemType)
  protected
    { IXMLLTLLineItemType }
    function Get_FreightClass: WideString;
    function Get_LineDescription: WideString;
    function Get_NumberOfIdenticalHandlingUnit: WideString;
    function Get_HandlingUnitWeight: WideString;
    function Get_PieceOnPallet: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_PackagingType: WideString;
    procedure Set_FreightClass(Value: WideString);
    procedure Set_LineDescription(Value: WideString);
    procedure Set_NumberOfIdenticalHandlingUnit(Value: WideString);
    procedure Set_HandlingUnitWeight(Value: WideString);
    procedure Set_PieceOnPallet(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_PackagingType(Value: WideString);
  end;

{ TXMLLTLLineItemTypeList }

  TXMLLTLLineItemTypeList = class(TXMLNodeCollection, IXMLLTLLineItemTypeList)
  protected
    { IXMLLTLLineItemTypeList }
    function Add: IXMLLTLLineItemType;
    function Insert(const Index: Integer): IXMLLTLLineItemType;
    function Get_Item(Index: Integer): IXMLLTLLineItemType;
  end;

{ TXMLPackageType }

  TXMLPackageType = class(TXMLNode, IXMLPackageType)
  protected
    { IXMLPackageType }
    function Get_PackageType: WideString;
    function Get_Weight: WideString;
    function Get_TrackingNumber: WideString;
    function Get_USPSNumber: WideString;
    function Get_LargePackageIndicator: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    function Get_Reference3: WideString;
    function Get_Reference4: WideString;
    function Get_Reference5: WideString;
    function Get_AdditionalHandlingOption: WideString;
    function Get_ShipperReleaseOption: WideString;
    function Get_COD: IXMLCODType;
    function Get_DeclaredValue: IXMLDeclaredValueType;
    function Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
    function Get_QVNOrReturnNotificationOption: IXMLQVNOrReturnNotificationOptionType;
    function Get_VerbalConfirmationOption: IXMLVerbalConfirmationOptionType;
    function Get_DangerousGoodsOption: WideString;
    function Get_Length: WideString;
    function Get_Width: WideString;
    function Get_Height: WideString;
    function Get_MerchandiseDescription: WideString;
    function Get_FlexibleParcelInsuranceOption: IXMLFlexibleParcelInsuranceOptionType;
    function Get_ProactiveResponseOption: WideString;
    function Get_DryIceOption: IXMLDryIceOptionType;
    procedure Set_PackageType(Value: WideString);
    procedure Set_Weight(Value: WideString);
    procedure Set_TrackingNumber(Value: WideString);
    procedure Set_USPSNumber(Value: WideString);
    procedure Set_LargePackageIndicator(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
    procedure Set_Reference3(Value: WideString);
    procedure Set_Reference4(Value: WideString);
    procedure Set_Reference5(Value: WideString);
    procedure Set_AdditionalHandlingOption(Value: WideString);
    procedure Set_ShipperReleaseOption(Value: WideString);
    procedure Set_DangerousGoodsOption(Value: WideString);
    procedure Set_Length(Value: WideString);
    procedure Set_Width(Value: WideString);
    procedure Set_Height(Value: WideString);
    procedure Set_MerchandiseDescription(Value: WideString);
    procedure Set_ProactiveResponseOption(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageTypeList }

  TXMLPackageTypeList = class(TXMLNodeCollection, IXMLPackageTypeList)
  protected
    { IXMLPackageTypeList }
    function Add: IXMLPackageType;
    function Insert(const Index: Integer): IXMLPackageType;
    function Get_Item(Index: Integer): IXMLPackageType;
  end;

{ TXMLQVNOrReturnNotificationOptionType }

  TXMLQVNOrReturnNotificationOptionType = class(TXMLNode, IXMLQVNOrReturnNotificationOptionType)
  private
    FQVNOrReturnRecipientAndNotificationTypes: IXMLQVNOrReturnRecipientAndNotificationTypesTypeList;
  protected
    { IXMLQVNOrReturnNotificationOptionType }
    function Get_QVNOrReturnRecipientAndNotificationTypes: IXMLQVNOrReturnRecipientAndNotificationTypesTypeList;
    function Get_ShipFromCompanyOrName: WideString;
    function Get_FailedEMailAddress: WideString;
    function Get_SubjectLine: WideString;
    function Get_Memo: WideString;
    procedure Set_ShipFromCompanyOrName(Value: WideString);
    procedure Set_FailedEMailAddress(Value: WideString);
    procedure Set_SubjectLine(Value: WideString);
    procedure Set_Memo(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLQVNOrReturnRecipientAndNotificationTypesType }

  TXMLQVNOrReturnRecipientAndNotificationTypesType = class(TXMLNode, IXMLQVNOrReturnRecipientAndNotificationTypesType)
  protected
    { IXMLQVNOrReturnRecipientAndNotificationTypesType }
    function Get_CompanyOrName: WideString;
    function Get_ContactName: WideString;
    function Get_EMailAddress: WideString;
    function Get_Ship: WideString;
    function Get_Exception: WideString;
    function Get_Delivery: WideString;
    procedure Set_CompanyOrName(Value: WideString);
    procedure Set_ContactName(Value: WideString);
    procedure Set_EMailAddress(Value: WideString);
    procedure Set_Ship(Value: WideString);
    procedure Set_Exception(Value: WideString);
    procedure Set_Delivery(Value: WideString);
  end;

{ TXMLQVNOrReturnRecipientAndNotificationTypesTypeList }

  TXMLQVNOrReturnRecipientAndNotificationTypesTypeList = class(TXMLNodeCollection, IXMLQVNOrReturnRecipientAndNotificationTypesTypeList)
  protected
    { IXMLQVNOrReturnRecipientAndNotificationTypesTypeList }
    function Add: IXMLQVNOrReturnRecipientAndNotificationTypesType;
    function Insert(const Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
    function Get_Item(Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
  end;

{ TXMLVerbalConfirmationOptionType }

  TXMLVerbalConfirmationOptionType = class(TXMLNode, IXMLVerbalConfirmationOptionType)
  protected
    { IXMLVerbalConfirmationOptionType }
    function Get_VerbalConfirmationContactName: WideString;
    function Get_VerbalConfirmationTelephone: WideString;
    procedure Set_VerbalConfirmationContactName(Value: WideString);
    procedure Set_VerbalConfirmationTelephone(Value: WideString);
  end;

{ TXMLFlexibleParcelInsuranceOptionType }

  TXMLFlexibleParcelInsuranceOptionType = class(TXMLNode, IXMLFlexibleParcelInsuranceOptionType)
  protected
    { IXMLFlexibleParcelInsuranceOptionType }
    function Get_FlexibleParcelInsuranceType: WideString;
    function Get_FlexibleParcelInsuranceValue: WideString;
    procedure Set_FlexibleParcelInsuranceType(Value: WideString);
    procedure Set_FlexibleParcelInsuranceValue(Value: WideString);
  end;

{ TXMLDryIceOptionType }

  TXMLDryIceOptionType = class(TXMLNode, IXMLDryIceOptionType)
  protected
    { IXMLDryIceOptionType }
    function Get_Weight: WideString;
    function Get_UnitOfMeasure: WideString;
    function Get_RegulationSet: WideString;
    function Get_IsMedical: WideString;
    function Get_DryIcePackagingType: WideString;
    procedure Set_Weight(Value: WideString);
    procedure Set_UnitOfMeasure(Value: WideString);
    procedure Set_RegulationSet(Value: WideString);
    procedure Set_IsMedical(Value: WideString);
    procedure Set_DryIcePackagingType(Value: WideString);
  end;

{ TXMLInternationalDocumentationType }

  TXMLInternationalDocumentationType = class(TXMLNode, IXMLInternationalDocumentationType)
  protected
    { IXMLInternationalDocumentationType }
    function Get_InvoiceImporterSameAsShipTo: WideString;
    function Get_InvoiceTermOfSale: WideString;
    function Get_InvoiceReasonForExport: WideString;
    function Get_InvoiceDeclarationStatement: WideString;
    function Get_InvoiceAdditionalComments: WideString;
    function Get_InvoiceCurrencyCode: WideString;
    function Get_InvoiceDiscount: WideString;
    function Get_InvoiceChargesFreight: WideString;
    function Get_InvoiceChargesInsurance: WideString;
    function Get_InvoiceChargesOther: WideString;
    function Get_InvoiceLineTotal: WideString;
    function Get_CustomsValueTotal: WideString;
    function Get_CustomsValueCurrencyCode: WideString;
    function Get_CustomsValueOrigin: WideString;
    function Get_SEDCode: WideString;
    function Get_SEDPartiesToTransaction: WideString;
    function Get_SEDUltimateConsigneeSameAsShipTo: WideString;
    function Get_SEDCountryTerritoryOfUltimateDestination: WideString;
    function Get_SEDAESTransactionNumber: WideString;
    function Get_SEDAESTransactionNumberType: WideString;
    function Get_SEDRoutedTransaction: WideString;
    function Get_SEDCurrencyConversion: WideString;
    function Get_SEDExportCode: WideString;
    function Get_COUPSPrepareCO: WideString;
    function Get_COOwnerOrAgent: WideString;
    function Get_NAFTAProducer: WideString;
    function Get_NAFTABlanketPeriodStart: WideString;
    function Get_NAFTABlanketPeriodEnd: WideString;
    procedure Set_InvoiceImporterSameAsShipTo(Value: WideString);
    procedure Set_InvoiceTermOfSale(Value: WideString);
    procedure Set_InvoiceReasonForExport(Value: WideString);
    procedure Set_InvoiceDeclarationStatement(Value: WideString);
    procedure Set_InvoiceAdditionalComments(Value: WideString);
    procedure Set_InvoiceCurrencyCode(Value: WideString);
    procedure Set_InvoiceDiscount(Value: WideString);
    procedure Set_InvoiceChargesFreight(Value: WideString);
    procedure Set_InvoiceChargesInsurance(Value: WideString);
    procedure Set_InvoiceChargesOther(Value: WideString);
    procedure Set_InvoiceLineTotal(Value: WideString);
    procedure Set_CustomsValueTotal(Value: WideString);
    procedure Set_CustomsValueCurrencyCode(Value: WideString);
    procedure Set_CustomsValueOrigin(Value: WideString);
    procedure Set_SEDCode(Value: WideString);
    procedure Set_SEDPartiesToTransaction(Value: WideString);
    procedure Set_SEDUltimateConsigneeSameAsShipTo(Value: WideString);
    procedure Set_SEDCountryTerritoryOfUltimateDestination(Value: WideString);
    procedure Set_SEDAESTransactionNumber(Value: WideString);
    procedure Set_SEDAESTransactionNumberType(Value: WideString);
    procedure Set_SEDRoutedTransaction(Value: WideString);
    procedure Set_SEDCurrencyConversion(Value: WideString);
    procedure Set_SEDExportCode(Value: WideString);
    procedure Set_COUPSPrepareCO(Value: WideString);
    procedure Set_COOwnerOrAgent(Value: WideString);
    procedure Set_NAFTAProducer(Value: WideString);
    procedure Set_NAFTABlanketPeriodStart(Value: WideString);
    procedure Set_NAFTABlanketPeriodEnd(Value: WideString);
  end;

{ TXMLGoodsType }

  TXMLGoodsType = class(TXMLNode, IXMLGoodsType)
  protected
    { IXMLGoodsType }
    function Get_PartNumber: WideString;
    function Get_DescriptionOfGood: WideString;
    function Get_InvNAFTATariffCode: WideString;
    function Get_InvNAFTACOCountryTerritoryOfOrigin: WideString;
    function Get_InvoiceUnits: WideString;
    function Get_InvoiceUnitOfMeasure: WideString;
    function Get_InvoiceSEDUnitPrice: WideString;
    function Get_InvoiceCurrencyCode: WideString;
    function Get_InvoiceNetWeight: WideString;
    function Get_SEDCOGrossWeight: WideString;
    function Get_SEDLbsOrKgs: WideString;
    function Get_SEDPrintGoodOnSED: WideString;
    function Get_InvoiceSEDCOMarksAndNumbers: WideString;
    function Get_SEDScheduleBNumber: WideString;
    function Get_SEDScheduleBUnits1: WideString;
    function Get_SEDScheduleBUnits2: WideString;
    function Get_SEDUnitsOfMeasure1: WideString;
    function Get_SEDUnitsOfMeasure2: WideString;
    function Get_SEDECCN: WideString;
    function Get_SEDException: WideString;
    function Get_SEDLicenceNumber: WideString;
    function Get_SEDLicenceNumberExpirationDate: WideString;
    function Get_NAFTAPrintGoodOnNAFTA: WideString;
    function Get_NAFTAPreferenceCriterion: WideString;
    function Get_NAFTAProducer: WideString;
    function Get_NAFTAMultipleCountriesTerritoriesOfOrigin: WideString;
    function Get_NAFTANetCostMethod: WideString;
    function Get_NAFTANetCostPeriodStartDate: WideString;
    function Get_NAFTANetCostPeriodEndDate: WideString;
    function Get_COPrintGoodOnCO: WideString;
    function Get_CONumberOfPackageContainingGood: WideString;
    procedure Set_PartNumber(Value: WideString);
    procedure Set_DescriptionOfGood(Value: WideString);
    procedure Set_InvNAFTATariffCode(Value: WideString);
    procedure Set_InvNAFTACOCountryTerritoryOfOrigin(Value: WideString);
    procedure Set_InvoiceUnits(Value: WideString);
    procedure Set_InvoiceUnitOfMeasure(Value: WideString);
    procedure Set_InvoiceSEDUnitPrice(Value: WideString);
    procedure Set_InvoiceCurrencyCode(Value: WideString);
    procedure Set_InvoiceNetWeight(Value: WideString);
    procedure Set_SEDCOGrossWeight(Value: WideString);
    procedure Set_SEDLbsOrKgs(Value: WideString);
    procedure Set_SEDPrintGoodOnSED(Value: WideString);
    procedure Set_InvoiceSEDCOMarksAndNumbers(Value: WideString);
    procedure Set_SEDScheduleBNumber(Value: WideString);
    procedure Set_SEDScheduleBUnits1(Value: WideString);
    procedure Set_SEDScheduleBUnits2(Value: WideString);
    procedure Set_SEDUnitsOfMeasure1(Value: WideString);
    procedure Set_SEDUnitsOfMeasure2(Value: WideString);
    procedure Set_SEDECCN(Value: WideString);
    procedure Set_SEDException(Value: WideString);
    procedure Set_SEDLicenceNumber(Value: WideString);
    procedure Set_SEDLicenceNumberExpirationDate(Value: WideString);
    procedure Set_NAFTAPrintGoodOnNAFTA(Value: WideString);
    procedure Set_NAFTAPreferenceCriterion(Value: WideString);
    procedure Set_NAFTAProducer(Value: WideString);
    procedure Set_NAFTAMultipleCountriesTerritoriesOfOrigin(Value: WideString);
    procedure Set_NAFTANetCostMethod(Value: WideString);
    procedure Set_NAFTANetCostPeriodStartDate(Value: WideString);
    procedure Set_NAFTANetCostPeriodEndDate(Value: WideString);
    procedure Set_COPrintGoodOnCO(Value: WideString);
    procedure Set_CONumberOfPackageContainingGood(Value: WideString);
  end;

{ TXMLGoodsTypeList }

  TXMLGoodsTypeList = class(TXMLNodeCollection, IXMLGoodsTypeList)
  protected
    { IXMLGoodsTypeList }
    function Add: IXMLGoodsType;
    function Insert(const Index: Integer): IXMLGoodsType;
    function Get_Item(Index: Integer): IXMLGoodsType;
  end;

{ TXMLProcessMessageType }

  TXMLProcessMessageType = class(TXMLNode, IXMLProcessMessageType)
  protected
    { IXMLProcessMessageType }
    function Get_Error: IXMLErrorType;
    function Get_ShipmentRates: IXMLShipmentRatesType;
    function Get_TrackingNumbers: IXMLTrackingNumbersType;
    function Get_ImportID: WideString;
    function Get_Reference1: WideString;
    function Get_Reference2: WideString;
    procedure Set_ImportID(Value: WideString);
    procedure Set_Reference1(Value: WideString);
    procedure Set_Reference2(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProcessMessageTypeList }

  TXMLProcessMessageTypeList = class(TXMLNodeCollection, IXMLProcessMessageTypeList)
  protected
    { IXMLProcessMessageTypeList }
    function Add: IXMLProcessMessageType;
    function Insert(const Index: Integer): IXMLProcessMessageType;
    function Get_Item(Index: Integer): IXMLProcessMessageType;
  end;

{ TXMLErrorType }

  TXMLErrorType = class(TXMLNode, IXMLErrorType)
  protected
    { IXMLErrorType }
    function Get_ErrorMessage: WideString;
    procedure Set_ErrorMessage(Value: WideString);
  end;

{ TXMLShipmentRatesType }

  TXMLShipmentRatesType = class(TXMLNode, IXMLShipmentRatesType)
  protected
    { IXMLShipmentRatesType }
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_ShipperCharges: IXMLShipperChargesType;
    function Get_ReceiverCharges: IXMLReceiverChargesType;
    function Get_ShipmentandHandlingCharges: IXMLShipmentandHandlingChargesType;
    function Get_Declared_Value: IXMLDeclared_ValueType;
    function Get_C_O_D: IXMLC_O_DType;
    function Get_Return_Services: IXMLReturn_ServicesType;
    function Get_Saturday_Delivery: IXMLSaturday_DeliveryType;
    function Get_Saturday_Pickup: IXMLSaturday_PickupType;
    function Get_Proactive_Response: IXMLProactive_ResponseType;
    function Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
    function Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
    function Get_PackageRates: IXMLPackageRatesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipmentChargesType }

  TXMLShipmentChargesType = class(TXMLNode, IXMLShipmentChargesType)
  protected
    { IXMLShipmentChargesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRateType }

  TXMLRateType = class(TXMLNode, IXMLRateType)
  protected
    { IXMLRateType }
    function Get_Reference: WideString;
    function Get_Published_: WideString;
    function Get_CCC: WideString;
    function Get_Negotiated: WideString;
    procedure Set_Reference(Value: WideString);
    procedure Set_Published_(Value: WideString);
    procedure Set_CCC(Value: WideString);
    procedure Set_Negotiated(Value: WideString);
  end;

{ TXMLShipperChargesType }

  TXMLShipperChargesType = class(TXMLNode, IXMLShipperChargesType)
  protected
    { IXMLShipperChargesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReceiverChargesType }

  TXMLReceiverChargesType = class(TXMLNode, IXMLReceiverChargesType)
  protected
    { IXMLReceiverChargesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipmentandHandlingChargesType }

  TXMLShipmentandHandlingChargesType = class(TXMLNode, IXMLShipmentandHandlingChargesType)
  protected
    { IXMLShipmentandHandlingChargesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDeclared_ValueType }

  TXMLDeclared_ValueType = class(TXMLNode, IXMLDeclared_ValueType)
  protected
    { IXMLDeclared_ValueType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLC_O_DType }

  TXMLC_O_DType = class(TXMLNode, IXMLC_O_DType)
  protected
    { IXMLC_O_DType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReturn_ServicesType }

  TXMLReturn_ServicesType = class(TXMLNode, IXMLReturn_ServicesType)
  protected
    { IXMLReturn_ServicesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSaturday_DeliveryType }

  TXMLSaturday_DeliveryType = class(TXMLNode, IXMLSaturday_DeliveryType)
  protected
    { IXMLSaturday_DeliveryType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSaturday_PickupType }

  TXMLSaturday_PickupType = class(TXMLNode, IXMLSaturday_PickupType)
  protected
    { IXMLSaturday_PickupType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProactive_ResponseType }

  TXMLProactive_ResponseType = class(TXMLNode, IXMLProactive_ResponseType)
  protected
    { IXMLProactive_ResponseType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDelivery_ConfirmationType }

  TXMLDelivery_ConfirmationType = class(TXMLNode, IXMLDelivery_ConfirmationType)
  protected
    { IXMLDelivery_ConfirmationType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDelivery_AreaSurchargeType }

  TXMLDelivery_AreaSurchargeType = class(TXMLNode, IXMLDelivery_AreaSurchargeType)
  protected
    { IXMLDelivery_AreaSurchargeType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageRatesType }

  TXMLPackageRatesType = class(TXMLNodeCollection, IXMLPackageRatesType)
  protected
    { IXMLPackageRatesType }
    function Get_PackageRate(Index: Integer): IXMLPackageRateType;
    function Add: IXMLPackageRateType;
    function Insert(const Index: Integer): IXMLPackageRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageRateType }

  TXMLPackageRateType = class(TXMLNode, IXMLPackageRateType)
  protected
    { IXMLPackageRateType }
    function Get_TrackingNumber: WideString;
    function Get_USPSNumber: WideString;
    function Get_PackageCharges: IXMLPackageChargesType;
    function Get_Additional_Handling: IXMLAdditional_HandlingType;
    function Get_C_O_D: IXMLC_O_DType;
    function Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
    function Get_Verbal_Confirmation: IXMLVerbal_ConfirmationType;
    function Get_Declared_Value: IXMLDeclared_ValueType;
    function Get_QVN: IXMLQVNType;
    function Get_Proactive_Response: IXMLProactive_ResponseType;
    function Get_Dry_Ice: IXMLDry_IceType;
    function Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
    procedure Set_TrackingNumber(Value: WideString);
    procedure Set_USPSNumber(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageChargesType }

  TXMLPackageChargesType = class(TXMLNode, IXMLPackageChargesType)
  protected
    { IXMLPackageChargesType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAdditional_HandlingType }

  TXMLAdditional_HandlingType = class(TXMLNode, IXMLAdditional_HandlingType)
  protected
    { IXMLAdditional_HandlingType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLVerbal_ConfirmationType }

  TXMLVerbal_ConfirmationType = class(TXMLNode, IXMLVerbal_ConfirmationType)
  protected
    { IXMLVerbal_ConfirmationType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLQVNType }

  TXMLQVNType = class(TXMLNode, IXMLQVNType)
  protected
    { IXMLQVNType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDry_IceType }

  TXMLDry_IceType = class(TXMLNode, IXMLDry_IceType)
  protected
    { IXMLDry_IceType }
    function Get_Rate: IXMLRateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTrackingNumbersType }

  TXMLTrackingNumbersType = class(TXMLNodeCollection, IXMLTrackingNumbersType)
  protected
    { IXMLTrackingNumbersType }
    function Get_TrackingNumber(Index: Integer): WideString;
    function Add(const TrackingNumber: WideString): IXMLNode;
    function Insert(const Index: Integer; const TrackingNumber: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetUPSOpenShipments(Doc: IXMLDocument): IXMLUPSOpenShipmentsType;
function LoadUPSOpenShipments(const FileName: WideString): IXMLUPSOpenShipmentsType;
function NewUPSOpenShipments: IXMLUPSOpenShipmentsType;

const
  TargetNamespace = 'x-schema:OpenShipments.xdr';

implementation

{ Global Functions }

function GetUPSOpenShipments(Doc: IXMLDocument): IXMLUPSOpenShipmentsType;
begin
  Result := Doc.GetDocBinding('OpenShipments', TXMLOpenShipmentsType, TargetNamespace) as IXMLUPSOpenShipmentsType;
end;

function LoadUPSOpenShipments(const FileName: WideString): IXMLUPSOpenShipmentsType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('OpenShipments', TXMLOpenShipmentsType, TargetNamespace) as IXMLUPSOpenShipmentsType;
end;

function NewUPSOpenShipments: IXMLUPSOpenShipmentsType;
begin
  Result := NewXMLDocument.GetDocBinding('OpenShipments', TXMLOpenShipmentsType, TargetNamespace) as IXMLUPSOpenShipmentsType;
end;

{ TXMLOpenShipmentsType }

procedure TXMLOpenShipmentsType.AfterConstruction;
begin
  RegisterChildNode('OpenShipment', TXMLOpenShipmentType);
  ItemTag := 'OpenShipment';
  ItemInterface := IXMLOpenShipmentType;
  inherited;
end;

function TXMLOpenShipmentsType.Get_OpenShipment(Index: Integer): IXMLOpenShipmentType;
begin
  Result := List[Index] as IXMLOpenShipmentType;
end;

function TXMLOpenShipmentsType.Add: IXMLOpenShipmentType;
begin
  Result := AddItem(-1) as IXMLOpenShipmentType;
end;

function TXMLOpenShipmentsType.Insert(const Index: Integer): IXMLOpenShipmentType;
begin
  Result := AddItem(Index) as IXMLOpenShipmentType;
end;

{ TXMLOpenShipmentType }

procedure TXMLOpenShipmentType.AfterConstruction;
begin
  RegisterChildNode('Receiver', TXMLReceiverType);
  RegisterChildNode('ReturnTo', TXMLReturnToType);
  RegisterChildNode('ShipFor', TXMLShipForType);
  RegisterChildNode('Shipper', TXMLShipperType);
  RegisterChildNode('Shipment', TXMLShipmentType);
  RegisterChildNode('Invoice', TXMLInvoiceType);
  RegisterChildNode('ShipTo', TXMLShipToType);
  RegisterChildNode('ShipFrom', TXMLShipFromType);
  RegisterChildNode('Producer', TXMLProducerType);
  RegisterChildNode('UltimateConsignee', TXMLUltimateConsigneeType);
  RegisterChildNode('Importer', TXMLImporterType);
  RegisterChildNode('ThirdParty', TXMLThirdPartyType);
  RegisterChildNode('ThirdPartyReceiver', TXMLThirdPartyReceiverType);
  RegisterChildNode('ShipmentInformation', TXMLShipmentInformationType);
  RegisterChildNode('LTLLineItem', TXMLLTLLineItemType);
  RegisterChildNode('Package', TXMLPackageType);
  RegisterChildNode('InternationalDocumentation', TXMLInternationalDocumentationType);
  RegisterChildNode('Goods', TXMLGoodsType);
  RegisterChildNode('ProcessMessage', TXMLProcessMessageType);
  FLTLLineItem := CreateCollection(TXMLLTLLineItemTypeList, IXMLLTLLineItemType, 'LTLLineItem') as IXMLLTLLineItemTypeList;
  FPackage := CreateCollection(TXMLPackageTypeList, IXMLPackageType, 'Package') as IXMLPackageTypeList;
  FGoods := CreateCollection(TXMLGoodsTypeList, IXMLGoodsType, 'Goods') as IXMLGoodsTypeList;
  FProcessMessage := CreateCollection(TXMLProcessMessageTypeList, IXMLProcessMessageType, 'ProcessMessage') as IXMLProcessMessageTypeList;
  inherited;
end;

function TXMLOpenShipmentType.Get_ProcessStatus: WideString;
begin
  Result := AttributeNodes['ProcessStatus'].Text;
end;

procedure TXMLOpenShipmentType.Set_ProcessStatus(Value: WideString);
begin
  SetAttribute('ProcessStatus', Value);
end;

function TXMLOpenShipmentType.Get_ShipmentOption: WideString;
begin
  Result := AttributeNodes['ShipmentOption'].Text;
end;

procedure TXMLOpenShipmentType.Set_ShipmentOption(Value: WideString);
begin
  SetAttribute('ShipmentOption', Value);
end;

{function TXMLOpenShipmentType.Get_ProcessStatus: WideString;
begin
  Result := AttributeNodes['ProcessStatus'].Text;
end;
}
{procedure TXMLOpenShipmentType.Set_ProcessStatus(Value: WideString);
begin
  SetAttribute('ProcessStatus', Value);
end;
}
{function TXMLOpenShipmentType.Get_ShipmentOption: WideString;
begin
  Result := AttributeNodes['ShipmentOption'].Text;
end;
}
{procedure TXMLOpenShipmentType.Set_ShipmentOption(Value: WideString);
begin
  SetAttribute('ShipmentOption', Value);
end;
}
function TXMLOpenShipmentType.Get_Receiver: IXMLReceiverType;
begin
  Result := ChildNodes['Receiver'] as IXMLReceiverType;
end;

function TXMLOpenShipmentType.Get_ReturnTo: IXMLReturnToType;
begin
  Result := ChildNodes['ReturnTo'] as IXMLReturnToType;
end;

function TXMLOpenShipmentType.Get_ShipFor: IXMLShipForType;
begin
  Result := ChildNodes['ShipFor'] as IXMLShipForType;
end;

function TXMLOpenShipmentType.Get_Shipper: IXMLShipperType;
begin
  Result := ChildNodes['Shipper'] as IXMLShipperType;
end;

function TXMLOpenShipmentType.Get_Shipment: IXMLShipmentType;
begin
  Result := ChildNodes['Shipment'] as IXMLShipmentType;
end;

function TXMLOpenShipmentType.Get_Invoice: IXMLInvoiceType;
begin
  Result := ChildNodes['Invoice'] as IXMLInvoiceType;
end;

function TXMLOpenShipmentType.Get_ShipTo: IXMLShipToType;
begin
  Result := ChildNodes['ShipTo'] as IXMLShipToType;
end;

function TXMLOpenShipmentType.Get_ShipFrom: IXMLShipFromType;
begin
  Result := ChildNodes['ShipFrom'] as IXMLShipFromType;
end;

function TXMLOpenShipmentType.Get_Producer: IXMLProducerType;
begin
  Result := ChildNodes['Producer'] as IXMLProducerType;
end;

function TXMLOpenShipmentType.Get_UltimateConsignee: IXMLUltimateConsigneeType;
begin
  Result := ChildNodes['UltimateConsignee'] as IXMLUltimateConsigneeType;
end;

function TXMLOpenShipmentType.Get_Importer: IXMLImporterType;
begin
  Result := ChildNodes['Importer'] as IXMLImporterType;
end;

function TXMLOpenShipmentType.Get_ThirdParty: IXMLThirdPartyType;
begin
  Result := ChildNodes['ThirdParty'] as IXMLThirdPartyType;
end;

function TXMLOpenShipmentType.Get_ThirdPartyReceiver: IXMLThirdPartyReceiverType;
begin
  Result := ChildNodes['ThirdPartyReceiver'] as IXMLThirdPartyReceiverType;
end;

function TXMLOpenShipmentType.Get_ShipmentInformation: IXMLShipmentInformationType;
begin
  Result := ChildNodes['ShipmentInformation'] as IXMLShipmentInformationType;
end;

function TXMLOpenShipmentType.Get_LTLLineItem: IXMLLTLLineItemTypeList;
begin
  Result := FLTLLineItem;
end;

function TXMLOpenShipmentType.Get_Package: IXMLPackageTypeList;
begin
  Result := FPackage;
end;

function TXMLOpenShipmentType.Get_InternationalDocumentation: IXMLInternationalDocumentationType;
begin
  Result := ChildNodes['InternationalDocumentation'] as IXMLInternationalDocumentationType;
end;

function TXMLOpenShipmentType.Get_Goods: IXMLGoodsTypeList;
begin
  Result := FGoods;
end;

function TXMLOpenShipmentType.Get_ProcessMessage: IXMLProcessMessageTypeList;
begin
  Result := FProcessMessage;
end;

{ TXMLReceiverType }

function TXMLReceiverType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLReceiverType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLReceiverType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLReceiverType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLReceiverType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLReceiverType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLReceiverType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLReceiverType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLReceiverType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLReceiverType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLReceiverType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLReceiverType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLReceiverType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLReceiverType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLReceiverType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLReceiverType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLReceiverType.Get_StateOrProvince: WideString;
begin
  Result := ChildNodes['StateOrProvince'].Text;
end;

procedure TXMLReceiverType.Set_StateOrProvince(Value: WideString);
begin
  ChildNodes['StateOrProvince'].NodeValue := Value;
end;

function TXMLReceiverType.Get_Residential: WideString;
begin
  Result := ChildNodes['Residential'].Text;
end;

procedure TXMLReceiverType.Set_Residential(Value: WideString);
begin
  ChildNodes['Residential'].NodeValue := Value;
end;

function TXMLReceiverType.Get_CustomerIDNumber: WideString;
begin
  Result := ChildNodes['CustomerIDNumber'].Text;
end;

procedure TXMLReceiverType.Set_CustomerIDNumber(Value: WideString);
begin
  ChildNodes['CustomerIDNumber'].NodeValue := Value;
end;

function TXMLReceiverType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLReceiverType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

function TXMLReceiverType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLReceiverType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLReceiverType.Get_LocationID: WideString;
begin
  Result := ChildNodes['LocationID'].Text;
end;

procedure TXMLReceiverType.Set_LocationID(Value: WideString);
begin
  ChildNodes['LocationID'].NodeValue := Value;
end;

function TXMLReceiverType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLReceiverType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

function TXMLReceiverType.Get_RecordOwner: WideString;
begin
  Result := ChildNodes['RecordOwner'].Text;
end;

procedure TXMLReceiverType.Set_RecordOwner(Value: WideString);
begin
  ChildNodes['RecordOwner'].NodeValue := Value;
end;

function TXMLReceiverType.Get_EmailAddress1: WideString;
begin
  Result := ChildNodes['EmailAddress1'].Text;
end;

procedure TXMLReceiverType.Set_EmailAddress1(Value: WideString);
begin
  ChildNodes['EmailAddress1'].NodeValue := Value;
end;

function TXMLReceiverType.Get_EmailAddress2: WideString;
begin
  Result := ChildNodes['EmailAddress2'].Text;
end;

procedure TXMLReceiverType.Set_EmailAddress2(Value: WideString);
begin
  ChildNodes['EmailAddress2'].NodeValue := Value;
end;

function TXMLReceiverType.Get_EmailContact1: WideString;
begin
  Result := ChildNodes['EmailContact1'].Text;
end;

procedure TXMLReceiverType.Set_EmailContact1(Value: WideString);
begin
  ChildNodes['EmailContact1'].NodeValue := Value;
end;

function TXMLReceiverType.Get_EmailContact2: WideString;
begin
  Result := ChildNodes['EmailContact2'].Text;
end;

procedure TXMLReceiverType.Set_EmailContact2(Value: WideString);
begin
  ChildNodes['EmailContact2'].NodeValue := Value;
end;

{ TXMLReturnToType }

function TXMLReturnToType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLReturnToType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLReturnToType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLReturnToType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLReturnToType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLReturnToType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLReturnToType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLReturnToType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLReturnToType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLReturnToType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLReturnToType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLReturnToType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLReturnToType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLReturnToType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLReturnToType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLReturnToType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLReturnToType.Get_StateOrProvince: WideString;
begin
  Result := ChildNodes['StateOrProvince'].Text;
end;

procedure TXMLReturnToType.Set_StateOrProvince(Value: WideString);
begin
  ChildNodes['StateOrProvince'].NodeValue := Value;
end;

function TXMLReturnToType.Get_Residential: WideString;
begin
  Result := ChildNodes['Residential'].Text;
end;

procedure TXMLReturnToType.Set_Residential(Value: WideString);
begin
  ChildNodes['Residential'].NodeValue := Value;
end;

function TXMLReturnToType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLReturnToType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

function TXMLReturnToType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLReturnToType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLReturnToType.Get_EmailAddress1: WideString;
begin
  Result := ChildNodes['EmailAddress1'].Text;
end;

procedure TXMLReturnToType.Set_EmailAddress1(Value: WideString);
begin
  ChildNodes['EmailAddress1'].NodeValue := Value;
end;

{ TXMLShipForType }

function TXMLShipForType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLShipForType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLShipForType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLShipForType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLShipForType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLShipForType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLShipForType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLShipForType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLShipForType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLShipForType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLShipForType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLShipForType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLShipForType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLShipForType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLShipForType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLShipForType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLShipForType.Get_StateOrProvince: WideString;
begin
  Result := ChildNodes['StateOrProvince'].Text;
end;

procedure TXMLShipForType.Set_StateOrProvince(Value: WideString);
begin
  ChildNodes['StateOrProvince'].NodeValue := Value;
end;

function TXMLShipForType.Get_Residential: WideString;
begin
  Result := ChildNodes['Residential'].Text;
end;

procedure TXMLShipForType.Set_Residential(Value: WideString);
begin
  ChildNodes['Residential'].NodeValue := Value;
end;

function TXMLShipForType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLShipForType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

function TXMLShipForType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLShipForType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLShipForType.Get_EmailAddress1: WideString;
begin
  Result := ChildNodes['EmailAddress1'].Text;
end;

procedure TXMLShipForType.Set_EmailAddress1(Value: WideString);
begin
  ChildNodes['EmailAddress1'].NodeValue := Value;
end;

{ TXMLShipperType }

function TXMLShipperType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLShipperType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

{ TXMLShipmentType }

procedure TXMLShipmentType.AfterConstruction;
begin
  RegisterChildNode('ThirdParty', TXMLThirdPartyType);
  RegisterChildNode('DeclareValue', TXMLDeclareValueType);
  RegisterChildNode('AdditionalHandling', TXMLAdditionalHandlingType);
  RegisterChildNode('COD', TXMLCODType);
  RegisterChildNode('AODTag', TXMLAODTagType);
  RegisterChildNode('ShipNotification', TXMLShipNotificationType);
  RegisterChildNode('QuantumViewNotifyDetails', TXMLQuantumViewNotifyDetailsType);
  RegisterChildNode('SpecialCommodities', TXMLSpecialCommoditiesType);
  RegisterChildNode('ReturnNotificationDetails', TXMLReturnNotificationDetailsType);
  RegisterChildNode('UPSExchangeCollect', TXMLUPSExchangeCollectType);
  RegisterChildNode('ReturnService', TXMLReturnServiceType);
  inherited;
end;

function TXMLShipmentType.Get_ServiceLevel: WideString;
begin
  Result := ChildNodes['ServiceLevel'].Text;
end;

procedure TXMLShipmentType.Set_ServiceLevel(Value: WideString);
begin
  ChildNodes['ServiceLevel'].NodeValue := Value;
end;

function TXMLShipmentType.Get_PackageType: WideString;
begin
  Result := ChildNodes['PackageType'].Text;
end;

procedure TXMLShipmentType.Set_PackageType(Value: WideString);
begin
  ChildNodes['PackageType'].NodeValue := Value;
end;

function TXMLShipmentType.Get_NumberOfPackages: WideString;
begin
  Result := ChildNodes['NumberOfPackages'].Text;
end;

procedure TXMLShipmentType.Set_NumberOfPackages(Value: WideString);
begin
  ChildNodes['NumberOfPackages'].NodeValue := Value;
end;

function TXMLShipmentType.Get_ShipmentActualWeight: WideString;
begin
  Result := ChildNodes['ShipmentActualWeight'].Text;
end;

procedure TXMLShipmentType.Set_ShipmentActualWeight(Value: WideString);
begin
  ChildNodes['ShipmentActualWeight'].NodeValue := Value;
end;

function TXMLShipmentType.Get_ShipmentDimensionalWeight: WideString;
begin
  Result := ChildNodes['ShipmentDimensionalWeight'].Text;
end;

procedure TXMLShipmentType.Set_ShipmentDimensionalWeight(Value: WideString);
begin
  ChildNodes['ShipmentDimensionalWeight'].NodeValue := Value;
end;

function TXMLShipmentType.Get_DescriptionOfGoods: WideString;
begin
  Result := ChildNodes['DescriptionOfGoods'].Text;
end;

procedure TXMLShipmentType.Set_DescriptionOfGoods(Value: WideString);
begin
  ChildNodes['DescriptionOfGoods'].NodeValue := Value;
end;

function TXMLShipmentType.Get_CostAllocationCode: WideString;
begin
  Result := ChildNodes['CostAllocationCode'].Text;
end;

procedure TXMLShipmentType.Set_CostAllocationCode(Value: WideString);
begin
  ChildNodes['CostAllocationCode'].NodeValue := Value;
end;

function TXMLShipmentType.Get_Reference1: WideString;
begin
  Result := ChildNodes['Reference1'].Text;
end;

procedure TXMLShipmentType.Set_Reference1(Value: WideString);
begin
  ChildNodes['Reference1'].NodeValue := Value;
end;

function TXMLShipmentType.Get_Reference2: WideString;
begin
  Result := ChildNodes['Reference2'].Text;
end;

procedure TXMLShipmentType.Set_Reference2(Value: WideString);
begin
  ChildNodes['Reference2'].NodeValue := Value;
end;

function TXMLShipmentType.Get_DocumentOnly: WideString;
begin
  Result := ChildNodes['DocumentOnly'].Text;
end;

procedure TXMLShipmentType.Set_DocumentOnly(Value: WideString);
begin
  ChildNodes['DocumentOnly'].NodeValue := Value;
end;

function TXMLShipmentType.Get_GoodsNotInFreeCirculation: WideString;
begin
  Result := ChildNodes['GoodsNotInFreeCirculation'].Text;
end;

procedure TXMLShipmentType.Set_GoodsNotInFreeCirculation(Value: WideString);
begin
  ChildNodes['GoodsNotInFreeCirculation'].NodeValue := Value;
end;

function TXMLShipmentType.Get_BillingOption: WideString;
begin
  Result := ChildNodes['BillingOption'].Text;
end;

procedure TXMLShipmentType.Set_BillingOption(Value: WideString);
begin
  ChildNodes['BillingOption'].NodeValue := Value;
end;

function TXMLShipmentType.Get_ThirdParty: IXMLThirdPartyType;
begin
  Result := ChildNodes['ThirdParty'] as IXMLThirdPartyType;
end;

function TXMLShipmentType.Get_SaturdayDelivery: WideString;
begin
  Result := ChildNodes['SaturdayDelivery'].Text;
end;

procedure TXMLShipmentType.Set_SaturdayDelivery(Value: WideString);
begin
  ChildNodes['SaturdayDelivery'].NodeValue := Value;
end;

function TXMLShipmentType.Get_DeclareValue: IXMLDeclareValueType;
begin
  Result := ChildNodes['DeclareValue'] as IXMLDeclareValueType;
end;

function TXMLShipmentType.Get_AdditionalHandling: IXMLAdditionalHandlingType;
begin
  Result := ChildNodes['AdditionalHandling'] as IXMLAdditionalHandlingType;
end;

function TXMLShipmentType.Get_COD: IXMLCODType;
begin
  Result := ChildNodes['COD'] as IXMLCODType;
end;

function TXMLShipmentType.Get_AODTag: IXMLAODTagType;
begin
  Result := ChildNodes['AODTag'] as IXMLAODTagType;
end;

function TXMLShipmentType.Get_ShipNotification: IXMLShipNotificationType;
begin
  Result := ChildNodes['ShipNotification'] as IXMLShipNotificationType;
end;

function TXMLShipmentType.Get_QuantumViewNotifyDetails: IXMLQuantumViewNotifyDetailsType;
begin
  Result := ChildNodes['QuantumViewNotifyDetails'] as IXMLQuantumViewNotifyDetailsType;
end;

function TXMLShipmentType.Get_SpecialCommodities: IXMLSpecialCommoditiesType;
begin
  Result := ChildNodes['SpecialCommodities'] as IXMLSpecialCommoditiesType;
end;

function TXMLShipmentType.Get_SignatureAdultSignatureRequired: WideString;
begin
  Result := ChildNodes['Signature-AdultSignatureRequired'].Text;
end;

procedure TXMLShipmentType.Set_SignatureAdultSignatureRequired(Value: WideString);
begin
  ChildNodes['Signature-AdultSignatureRequired'].NodeValue := Value;
end;

function TXMLShipmentType.Get_ReturnNotificationDetails: IXMLReturnNotificationDetailsType;
begin
  Result := ChildNodes['ReturnNotificationDetails'] as IXMLReturnNotificationDetailsType;
end;

function TXMLShipmentType.Get_CollectionDate: WideString;
begin
  Result := ChildNodes['CollectionDate'].Text;
end;

procedure TXMLShipmentType.Set_CollectionDate(Value: WideString);
begin
  ChildNodes['CollectionDate'].NodeValue := Value;
end;

function TXMLShipmentType.Get_ImportID: WideString;
begin
  Result := ChildNodes['ImportID'].Text;
end;

procedure TXMLShipmentType.Set_ImportID(Value: WideString);
begin
  ChildNodes['ImportID'].NodeValue := Value;
end;

function TXMLShipmentType.Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
begin
  Result := ChildNodes['UPSExchangeCollect'] as IXMLUPSExchangeCollectType;
end;

function TXMLShipmentType.Get_ReturnService: IXMLReturnServiceType;
begin
  Result := ChildNodes['ReturnService'] as IXMLReturnServiceType;
end;

{ TXMLThirdPartyType }

function TXMLThirdPartyType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLThirdPartyType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLThirdPartyType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLThirdPartyType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLThirdPartyType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLThirdPartyType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLThirdPartyType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLThirdPartyType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLThirdPartyType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLThirdPartyType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLThirdPartyType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLThirdPartyType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLThirdPartyType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLThirdPartyType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLThirdPartyType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLThirdPartyType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLThirdPartyType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_StateOrProvince: WideString;
begin
  Result := ChildNodes['StateOrProvince'].Text;
end;

procedure TXMLThirdPartyType.Set_StateOrProvince(Value: WideString);
begin
  ChildNodes['StateOrProvince'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLThirdPartyType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Residential: WideString;
begin
  Result := ChildNodes['Residential'].Text;
end;

procedure TXMLThirdPartyType.Set_Residential(Value: WideString);
begin
  ChildNodes['Residential'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_ResidentialIndicator: WideString;
begin
  Result := ChildNodes['ResidentialIndicator'].Text;
end;

procedure TXMLThirdPartyType.Set_ResidentialIndicator(Value: WideString);
begin
  ChildNodes['ResidentialIndicator'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_CustomerIDNumber: WideString;
begin
  Result := ChildNodes['CustomerIDNumber'].Text;
end;

procedure TXMLThirdPartyType.Set_CustomerIDNumber(Value: WideString);
begin
  ChildNodes['CustomerIDNumber'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLThirdPartyType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLThirdPartyType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_FaxNumber: WideString;
begin
  Result := ChildNodes['FaxNumber'].Text;
end;

procedure TXMLThirdPartyType.Set_FaxNumber(Value: WideString);
begin
  ChildNodes['FaxNumber'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLThirdPartyType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLThirdPartyType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

function TXMLThirdPartyType.Get_RecordOwner: WideString;
begin
  Result := ChildNodes['RecordOwner'].Text;
end;

procedure TXMLThirdPartyType.Set_RecordOwner(Value: WideString);
begin
  ChildNodes['RecordOwner'].NodeValue := Value;
end;

{ TXMLDeclareValueType }

function TXMLDeclareValueType.Get_Amount: WideString;
begin
  Result := ChildNodes['Amount'].Text;
end;

procedure TXMLDeclareValueType.Set_Amount(Value: WideString);
begin
  ChildNodes['Amount'].NodeValue := Value;
end;

{ TXMLAdditionalHandlingType }

function TXMLAdditionalHandlingType.Get_NumberOfAdditionalHandling: WideString;
begin
  Result := ChildNodes['NumberOfAdditionalHandling'].Text;
end;

procedure TXMLAdditionalHandlingType.Set_NumberOfAdditionalHandling(Value: WideString);
begin
  ChildNodes['NumberOfAdditionalHandling'].NodeValue := Value;
end;

{ TXMLCODType }

function TXMLCODType.Get_CashOnly: WideString;
begin
  Result := ChildNodes['CashOnly'].Text;
end;

procedure TXMLCODType.Set_CashOnly(Value: WideString);
begin
  ChildNodes['CashOnly'].NodeValue := Value;
end;

function TXMLCODType.Get_CashierCheckorMoneyOrderOnlyIndicator: WideString;
begin
  Result := ChildNodes['CashierCheckorMoneyOrderOnlyIndicator'].Text;
end;

procedure TXMLCODType.Set_CashierCheckorMoneyOrderOnlyIndicator(Value: WideString);
begin
  ChildNodes['CashierCheckorMoneyOrderOnlyIndicator'].NodeValue := Value;
end;

function TXMLCODType.Get_AddShippingChargesToCODIndicator: WideString;
begin
  Result := ChildNodes['AddShippingChargesToCODIndicator'].Text;
end;

procedure TXMLCODType.Set_AddShippingChargesToCODIndicator(Value: WideString);
begin
  ChildNodes['AddShippingChargesToCODIndicator'].NodeValue := Value;
end;

function TXMLCODType.Get_Amount: WideString;
begin
  Result := ChildNodes['Amount'].Text;
end;

procedure TXMLCODType.Set_Amount(Value: WideString);
begin
  ChildNodes['Amount'].NodeValue := Value;
end;

function TXMLCODType.Get_Currency: WideString;
begin
  Result := ChildNodes['Currency'].Text;
end;

procedure TXMLCODType.Set_Currency(Value: WideString);
begin
  ChildNodes['Currency'].NodeValue := Value;
end;

{ TXMLAODTagType }

function TXMLAODTagType.Get_NumberOfAOD: WideString;
begin
  Result := ChildNodes['NumberOfAOD'].Text;
end;

procedure TXMLAODTagType.Set_NumberOfAOD(Value: WideString);
begin
  ChildNodes['NumberOfAOD'].NodeValue := Value;
end;

{ TXMLShipNotificationType }

function TXMLShipNotificationType.Get_EmailText: WideString;
begin
  Result := ChildNodes['EmailText'].Text;
end;

procedure TXMLShipNotificationType.Set_EmailText(Value: WideString);
begin
  ChildNodes['EmailText'].NodeValue := Value;
end;

{ TXMLQuantumViewNotifyDetailsType }

procedure TXMLQuantumViewNotifyDetailsType.AfterConstruction;
begin
  RegisterChildNode('QuantumViewNotify', TXMLQuantumViewNotifyType);
  FQuantumViewNotify := CreateCollection(TXMLQuantumViewNotifyTypeList, IXMLQuantumViewNotifyType, 'QuantumViewNotify') as IXMLQuantumViewNotifyTypeList;
  inherited;
end;

function TXMLQuantumViewNotifyDetailsType.Get_QuantumViewNotify: IXMLQuantumViewNotifyTypeList;
begin
  Result := FQuantumViewNotify;
end;

function TXMLQuantumViewNotifyDetailsType.Get_FailureNotificationEMailAddress: WideString;
begin
  Result := ChildNodes['FailureNotificationEMailAddress'].Text;
end;

procedure TXMLQuantumViewNotifyDetailsType.Set_FailureNotificationEMailAddress(Value: WideString);
begin
  ChildNodes['FailureNotificationEMailAddress'].NodeValue := Value;
end;

function TXMLQuantumViewNotifyDetailsType.Get_ShipperCompanyName: WideString;
begin
  Result := ChildNodes['ShipperCompanyName'].Text;
end;

procedure TXMLQuantumViewNotifyDetailsType.Set_ShipperCompanyName(Value: WideString);
begin
  ChildNodes['ShipperCompanyName'].NodeValue := Value;
end;

function TXMLQuantumViewNotifyDetailsType.Get_SubjectLineOptions: WideString;
begin
  Result := ChildNodes['SubjectLineOptions'].Text;
end;

procedure TXMLQuantumViewNotifyDetailsType.Set_SubjectLineOptions(Value: WideString);
begin
  ChildNodes['SubjectLineOptions'].NodeValue := Value;
end;

function TXMLQuantumViewNotifyDetailsType.Get_NotificationMessage: WideString;
begin
  Result := ChildNodes['NotificationMessage'].Text;
end;

procedure TXMLQuantumViewNotifyDetailsType.Set_NotificationMessage(Value: WideString);
begin
  ChildNodes['NotificationMessage'].NodeValue := Value;
end;

{ TXMLQuantumViewNotifyType }

function TXMLQuantumViewNotifyType.Get_NotificationEMailAddress: WideString;
begin
  Result := ChildNodes['NotificationEMailAddress'].Text;
end;

procedure TXMLQuantumViewNotifyType.Set_NotificationEMailAddress(Value: WideString);
begin
  ChildNodes['NotificationEMailAddress'].NodeValue := Value;
end;

function TXMLQuantumViewNotifyType.Get_NotificationRequest: WideString;
begin
  Result := ChildNodes['NotificationRequest'].Text;
end;

procedure TXMLQuantumViewNotifyType.Set_NotificationRequest(Value: WideString);
begin
  ChildNodes['NotificationRequest'].NodeValue := Value;
end;

{ TXMLQuantumViewNotifyTypeList }

function TXMLQuantumViewNotifyTypeList.Add: IXMLQuantumViewNotifyType;
begin
  Result := AddItem(-1) as IXMLQuantumViewNotifyType;
end;

function TXMLQuantumViewNotifyTypeList.Insert(const Index: Integer): IXMLQuantumViewNotifyType;
begin
  Result := AddItem(Index) as IXMLQuantumViewNotifyType;
end;
function TXMLQuantumViewNotifyTypeList.Get_Item(Index: Integer): IXMLQuantumViewNotifyType;
begin
  Result := List[Index] as IXMLQuantumViewNotifyType;
end;

{ TXMLSpecialCommoditiesType }

function TXMLSpecialCommoditiesType.Get_CommoditiesDetails: WideString;
begin
  Result := ChildNodes['CommoditiesDetails'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_CommoditiesDetails(Value: WideString);
begin
  ChildNodes['CommoditiesDetails'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCAlcoholicBeverages: WideString;
begin
  Result := ChildNodes['ISC-AlcoholicBeverages'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCAlcoholicBeverages(Value: WideString);
begin
  ChildNodes['ISC-AlcoholicBeverages'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCDiagnosticSpecimens: WideString;
begin
  Result := ChildNodes['ISC-DiagnosticSpecimens'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCDiagnosticSpecimens(Value: WideString);
begin
  ChildNodes['ISC-DiagnosticSpecimens'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCPerishables: WideString;
begin
  Result := ChildNodes['ISC-Perishables'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCPerishables(Value: WideString);
begin
  ChildNodes['ISC-Perishables'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCPlants: WideString;
begin
  Result := ChildNodes['ISC-Plants'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCPlants(Value: WideString);
begin
  ChildNodes['ISC-Plants'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCSeeds: WideString;
begin
  Result := ChildNodes['ISC-Seeds'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCSeeds(Value: WideString);
begin
  ChildNodes['ISC-Seeds'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCSpecialException: WideString;
begin
  Result := ChildNodes['ISC-SpecialException'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCSpecialException(Value: WideString);
begin
  ChildNodes['ISC-SpecialException'].NodeValue := Value;
end;

function TXMLSpecialCommoditiesType.Get_ISCTobacco: WideString;
begin
  Result := ChildNodes['ISC-Tobacco'].Text;
end;

procedure TXMLSpecialCommoditiesType.Set_ISCTobacco(Value: WideString);
begin
  ChildNodes['ISC-Tobacco'].NodeValue := Value;
end;

{ TXMLReturnNotificationDetailsType }

procedure TXMLReturnNotificationDetailsType.AfterConstruction;
begin
  RegisterChildNode('ReturnNotification', TXMLReturnNotificationType);
  FReturnNotification := CreateCollection(TXMLReturnNotificationTypeList, IXMLReturnNotificationType, 'ReturnNotification') as IXMLReturnNotificationTypeList;
  inherited;
end;

function TXMLReturnNotificationDetailsType.Get_ReturnNotification: IXMLReturnNotificationTypeList;
begin
  Result := FReturnNotification;
end;

function TXMLReturnNotificationDetailsType.Get_FailureNotificationEMailAddress: WideString;
begin
  Result := ChildNodes['FailureNotificationEMailAddress'].Text;
end;

procedure TXMLReturnNotificationDetailsType.Set_FailureNotificationEMailAddress(Value: WideString);
begin
  ChildNodes['FailureNotificationEMailAddress'].NodeValue := Value;
end;

function TXMLReturnNotificationDetailsType.Get_ShipperCompanyName: WideString;
begin
  Result := ChildNodes['ShipperCompanyName'].Text;
end;

procedure TXMLReturnNotificationDetailsType.Set_ShipperCompanyName(Value: WideString);
begin
  ChildNodes['ShipperCompanyName'].NodeValue := Value;
end;

function TXMLReturnNotificationDetailsType.Get_SubjectLineOptions: WideString;
begin
  Result := ChildNodes['SubjectLineOptions'].Text;
end;

procedure TXMLReturnNotificationDetailsType.Set_SubjectLineOptions(Value: WideString);
begin
  ChildNodes['SubjectLineOptions'].NodeValue := Value;
end;

function TXMLReturnNotificationDetailsType.Get_NotificationMessage: WideString;
begin
  Result := ChildNodes['NotificationMessage'].Text;
end;

procedure TXMLReturnNotificationDetailsType.Set_NotificationMessage(Value: WideString);
begin
  ChildNodes['NotificationMessage'].NodeValue := Value;
end;

{ TXMLReturnNotificationType }

procedure TXMLReturnNotificationType.AfterConstruction;
begin
  ItemTag := 'NotificationEMailAddress';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLReturnNotificationType.Get_NotificationEMailAddress(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLReturnNotificationType.Add(const NotificationEMailAddress: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := NotificationEMailAddress;
end;

function TXMLReturnNotificationType.Insert(const Index: Integer; const NotificationEMailAddress: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := NotificationEMailAddress;
end;

{ TXMLReturnNotificationTypeList }

function TXMLReturnNotificationTypeList.Add: IXMLReturnNotificationType;
begin
  Result := AddItem(-1) as IXMLReturnNotificationType;
end;

function TXMLReturnNotificationTypeList.Insert(const Index: Integer): IXMLReturnNotificationType;
begin
  Result := AddItem(Index) as IXMLReturnNotificationType;
end;
function TXMLReturnNotificationTypeList.Get_Item(Index: Integer): IXMLReturnNotificationType;
begin
  Result := List[Index] as IXMLReturnNotificationType;
end;

{ TXMLUPSExchangeCollectType }

function TXMLUPSExchangeCollectType.Get_BuyerEMailAddress: WideString;
begin
  Result := ChildNodes['BuyerEMailAddress'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_BuyerEMailAddress(Value: WideString);
begin
  ChildNodes['BuyerEMailAddress'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_SellerEMailAddress: WideString;
begin
  Result := ChildNodes['SellerEMailAddress'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_SellerEMailAddress(Value: WideString);
begin
  ChildNodes['SellerEMailAddress'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_ExchangeCollectAmount: WideString;
begin
  Result := ChildNodes['ExchangeCollectAmount'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_ExchangeCollectAmount(Value: WideString);
begin
  ChildNodes['ExchangeCollectAmount'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_Currency: WideString;
begin
  Result := ChildNodes['Currency'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_Currency(Value: WideString);
begin
  ChildNodes['Currency'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_BuyerPercentageOfServiceFee: WideString;
begin
  Result := ChildNodes['BuyerPercentageOfServiceFee'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_BuyerPercentageOfServiceFee(Value: WideString);
begin
  ChildNodes['BuyerPercentageOfServiceFee'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_DescriptionOfGoods: WideString;
begin
  Result := ChildNodes['DescriptionOfGoods'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_DescriptionOfGoods(Value: WideString);
begin
  ChildNodes['DescriptionOfGoods'].NodeValue := Value;
end;

function TXMLUPSExchangeCollectType.Get_AdditionalComments: WideString;
begin
  Result := ChildNodes['AdditionalComments'].Text;
end;

procedure TXMLUPSExchangeCollectType.Set_AdditionalComments(Value: WideString);
begin
  ChildNodes['AdditionalComments'].NodeValue := Value;
end;

{ TXMLReturnServiceType }

function TXMLReturnServiceType.Get_Options: WideString;
begin
  Result := ChildNodes['Options'].Text;
end;

procedure TXMLReturnServiceType.Set_Options(Value: WideString);
begin
  ChildNodes['Options'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_PrintAddressOnlyInvoice: WideString;
begin
  Result := ChildNodes['PrintAddressOnlyInvoice'].Text;
end;

procedure TXMLReturnServiceType.Set_PrintAddressOnlyInvoice(Value: WideString);
begin
  ChildNodes['PrintAddressOnlyInvoice'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_SenderCompanyName: WideString;
begin
  Result := ChildNodes['SenderCompanyName'].Text;
end;

procedure TXMLReturnServiceType.Set_SenderCompanyName(Value: WideString);
begin
  ChildNodes['SenderCompanyName'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_RecipientEmailAddress: WideString;
begin
  Result := ChildNodes['RecipientEmailAddress'].Text;
end;

procedure TXMLReturnServiceType.Set_RecipientEmailAddress(Value: WideString);
begin
  ChildNodes['RecipientEmailAddress'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_FailureOfDeliveryEmailAddress: WideString;
begin
  Result := ChildNodes['FailureOfDeliveryEmailAddress'].Text;
end;

procedure TXMLReturnServiceType.Set_FailureOfDeliveryEmailAddress(Value: WideString);
begin
  ChildNodes['FailureOfDeliveryEmailAddress'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_InstructionAndReceiptLanguage: WideString;
begin
  Result := ChildNodes['InstructionAndReceiptLanguage'].Text;
end;

procedure TXMLReturnServiceType.Set_InstructionAndReceiptLanguage(Value: WideString);
begin
  ChildNodes['InstructionAndReceiptLanguage'].NodeValue := Value;
end;

function TXMLReturnServiceType.Get_MerchandiseDescOfPackage: WideString;
begin
  Result := ChildNodes['MerchandiseDescOfPackage'].Text;
end;

procedure TXMLReturnServiceType.Set_MerchandiseDescOfPackage(Value: WideString);
begin
  ChildNodes['MerchandiseDescOfPackage'].NodeValue := Value;
end;

{ TXMLInvoiceType }

procedure TXMLInvoiceType.AfterConstruction;
begin
  RegisterChildNode('LineItem', TXMLLineItemType);
  RegisterChildNode('SoldTo', TXMLSoldToType);
  FLineItem := CreateCollection(TXMLLineItemTypeList, IXMLLineItemType, 'LineItem') as IXMLLineItemTypeList;
  inherited;
end;

function TXMLInvoiceType.Get_ReasonForExport: WideString;
begin
  Result := ChildNodes['ReasonForExport'].Text;
end;

procedure TXMLInvoiceType.Set_ReasonForExport(Value: WideString);
begin
  ChildNodes['ReasonForExport'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_TermOfSale: WideString;
begin
  Result := ChildNodes['TermOfSale'].Text;
end;

procedure TXMLInvoiceType.Set_TermOfSale(Value: WideString);
begin
  ChildNodes['TermOfSale'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_DeclarationStatement: WideString;
begin
  Result := ChildNodes['DeclarationStatement'].Text;
end;

procedure TXMLInvoiceType.Set_DeclarationStatement(Value: WideString);
begin
  ChildNodes['DeclarationStatement'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_AdditionalComments: WideString;
begin
  Result := ChildNodes['AdditionalComments'].Text;
end;

procedure TXMLInvoiceType.Set_AdditionalComments(Value: WideString);
begin
  ChildNodes['AdditionalComments'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_InvoiceCurrency: WideString;
begin
  Result := ChildNodes['InvoiceCurrency'].Text;
end;

procedure TXMLInvoiceType.Set_InvoiceCurrency(Value: WideString);
begin
  ChildNodes['InvoiceCurrency'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_InvoiceLineTotal: WideString;
begin
  Result := ChildNodes['InvoiceLineTotal'].Text;
end;

procedure TXMLInvoiceType.Set_InvoiceLineTotal(Value: WideString);
begin
  ChildNodes['InvoiceLineTotal'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_DiscountRebate: WideString;
begin
  Result := ChildNodes['DiscountRebate'].Text;
end;

procedure TXMLInvoiceType.Set_DiscountRebate(Value: WideString);
begin
  ChildNodes['DiscountRebate'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_Freight: WideString;
begin
  Result := ChildNodes['Freight'].Text;
end;

procedure TXMLInvoiceType.Set_Freight(Value: WideString);
begin
  ChildNodes['Freight'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_Insurance: WideString;
begin
  Result := ChildNodes['Insurance'].Text;
end;

procedure TXMLInvoiceType.Set_Insurance(Value: WideString);
begin
  ChildNodes['Insurance'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_OtherCharges: WideString;
begin
  Result := ChildNodes['OtherCharges'].Text;
end;

procedure TXMLInvoiceType.Set_OtherCharges(Value: WideString);
begin
  ChildNodes['OtherCharges'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_TotalInvoiceAmount: WideString;
begin
  Result := ChildNodes['TotalInvoiceAmount'].Text;
end;

procedure TXMLInvoiceType.Set_TotalInvoiceAmount(Value: WideString);
begin
  ChildNodes['TotalInvoiceAmount'].NodeValue := Value;
end;

function TXMLInvoiceType.Get_LineItem: IXMLLineItemTypeList;
begin
  Result := FLineItem;
end;

function TXMLInvoiceType.Get_SoldTo: IXMLSoldToType;
begin
  Result := ChildNodes['SoldTo'] as IXMLSoldToType;
end;

{ TXMLLineItemType }

procedure TXMLLineItemType.AfterConstruction;
begin
  RegisterChildNode('Product', TXMLProductType);
  inherited;
end;

function TXMLLineItemType.Get_Product: IXMLProductType;
begin
  Result := ChildNodes['Product'] as IXMLProductType;
end;

function TXMLLineItemType.Get_NumberOfUnits: WideString;
begin
  Result := ChildNodes['NumberOfUnits'].Text;
end;

procedure TXMLLineItemType.Set_NumberOfUnits(Value: WideString);
begin
  ChildNodes['NumberOfUnits'].NodeValue := Value;
end;

{ TXMLLineItemTypeList }

function TXMLLineItemTypeList.Add: IXMLLineItemType;
begin
  Result := AddItem(-1) as IXMLLineItemType;
end;

function TXMLLineItemTypeList.Insert(const Index: Integer): IXMLLineItemType;
begin
  Result := AddItem(Index) as IXMLLineItemType;
end;
function TXMLLineItemTypeList.Get_Item(Index: Integer): IXMLLineItemType;
begin
  Result := List[Index] as IXMLLineItemType;
end;

{ TXMLProductType }

function TXMLProductType.Get_ProductNumber: WideString;
begin
  Result := ChildNodes['ProductNumber'].Text;
end;

procedure TXMLProductType.Set_ProductNumber(Value: WideString);
begin
  ChildNodes['ProductNumber'].NodeValue := Value;
end;

function TXMLProductType.Get_PartNumber: WideString;
begin
  Result := ChildNodes['PartNumber'].Text;
end;

procedure TXMLProductType.Set_PartNumber(Value: WideString);
begin
  ChildNodes['PartNumber'].NodeValue := Value;
end;

function TXMLProductType.Get_ProductName: WideString;
begin
  Result := ChildNodes['ProductName'].Text;
end;

procedure TXMLProductType.Set_ProductName(Value: WideString);
begin
  ChildNodes['ProductName'].NodeValue := Value;
end;

function TXMLProductType.Get_HarmonizedCode: WideString;
begin
  Result := ChildNodes['HarmonizedCode'].Text;
end;

procedure TXMLProductType.Set_HarmonizedCode(Value: WideString);
begin
  ChildNodes['HarmonizedCode'].NodeValue := Value;
end;

function TXMLProductType.Get_ProductDescription: WideString;
begin
  Result := ChildNodes['ProductDescription'].Text;
end;

procedure TXMLProductType.Set_ProductDescription(Value: WideString);
begin
  ChildNodes['ProductDescription'].NodeValue := Value;
end;

function TXMLProductType.Get_UnitOfMeasure: WideString;
begin
  Result := ChildNodes['UnitOfMeasure'].Text;
end;

procedure TXMLProductType.Set_UnitOfMeasure(Value: WideString);
begin
  ChildNodes['UnitOfMeasure'].NodeValue := Value;
end;

function TXMLProductType.Get_CountryOfOriginCode: WideString;
begin
  Result := ChildNodes['CountryOfOriginCode'].Text;
end;

procedure TXMLProductType.Set_CountryOfOriginCode(Value: WideString);
begin
  ChildNodes['CountryOfOriginCode'].NodeValue := Value;
end;

function TXMLProductType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLProductType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLProductType.Get_UnitPrice: WideString;
begin
  Result := ChildNodes['UnitPrice'].Text;
end;

procedure TXMLProductType.Set_UnitPrice(Value: WideString);
begin
  ChildNodes['UnitPrice'].NodeValue := Value;
end;

{ TXMLSoldToType }

function TXMLSoldToType.Get_CompanyName: WideString;
begin
  Result := ChildNodes['CompanyName'].Text;
end;

procedure TXMLSoldToType.Set_CompanyName(Value: WideString);
begin
  ChildNodes['CompanyName'].NodeValue := Value;
end;

function TXMLSoldToType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLSoldToType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLSoldToType.Get_AddressLine1: WideString;
begin
  Result := ChildNodes['AddressLine1'].Text;
end;

procedure TXMLSoldToType.Set_AddressLine1(Value: WideString);
begin
  ChildNodes['AddressLine1'].NodeValue := Value;
end;

function TXMLSoldToType.Get_AddressLine2: WideString;
begin
  Result := ChildNodes['AddressLine2'].Text;
end;

procedure TXMLSoldToType.Set_AddressLine2(Value: WideString);
begin
  ChildNodes['AddressLine2'].NodeValue := Value;
end;

function TXMLSoldToType.Get_AddressLine3: WideString;
begin
  Result := ChildNodes['AddressLine3'].Text;
end;

procedure TXMLSoldToType.Set_AddressLine3(Value: WideString);
begin
  ChildNodes['AddressLine3'].NodeValue := Value;
end;

function TXMLSoldToType.Get_City: WideString;
begin
  Result := ChildNodes['City'].Text;
end;

procedure TXMLSoldToType.Set_City(Value: WideString);
begin
  ChildNodes['City'].NodeValue := Value;
end;

function TXMLSoldToType.Get_CountryCode: WideString;
begin
  Result := ChildNodes['CountryCode'].Text;
end;

procedure TXMLSoldToType.Set_CountryCode(Value: WideString);
begin
  ChildNodes['CountryCode'].NodeValue := Value;
end;

function TXMLSoldToType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLSoldToType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLSoldToType.Get_StateOrProvince: WideString;
begin
  Result := ChildNodes['StateOrProvince'].Text;
end;

procedure TXMLSoldToType.Set_StateOrProvince(Value: WideString);
begin
  ChildNodes['StateOrProvince'].NodeValue := Value;
end;

function TXMLSoldToType.Get_Residential: WideString;
begin
  Result := ChildNodes['Residential'].Text;
end;

procedure TXMLSoldToType.Set_Residential(Value: WideString);
begin
  ChildNodes['Residential'].NodeValue := Value;
end;

function TXMLSoldToType.Get_CustomerIDNumber: WideString;
begin
  Result := ChildNodes['CustomerIDNumber'].Text;
end;

procedure TXMLSoldToType.Set_CustomerIDNumber(Value: WideString);
begin
  ChildNodes['CustomerIDNumber'].NodeValue := Value;
end;

function TXMLSoldToType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLSoldToType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

function TXMLSoldToType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLSoldToType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLSoldToType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLSoldToType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

function TXMLSoldToType.Get_RecordOwner: WideString;
begin
  Result := ChildNodes['RecordOwner'].Text;
end;

procedure TXMLSoldToType.Set_RecordOwner(Value: WideString);
begin
  ChildNodes['RecordOwner'].NodeValue := Value;
end;

{ TXMLShipToType }

function TXMLShipToType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLShipToType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLShipToType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLShipToType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLShipToType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLShipToType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLShipToType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLShipToType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLShipToType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLShipToType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLShipToType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLShipToType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLShipToType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLShipToType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLShipToType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLShipToType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLShipToType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLShipToType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLShipToType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLShipToType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLShipToType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLShipToType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLShipToType.Get_FaxNumber: WideString;
begin
  Result := ChildNodes['FaxNumber'].Text;
end;

procedure TXMLShipToType.Set_FaxNumber(Value: WideString);
begin
  ChildNodes['FaxNumber'].NodeValue := Value;
end;

function TXMLShipToType.Get_EmailAddress: WideString;
begin
  Result := ChildNodes['EmailAddress'].Text;
end;

procedure TXMLShipToType.Set_EmailAddress(Value: WideString);
begin
  ChildNodes['EmailAddress'].NodeValue := Value;
end;

function TXMLShipToType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLShipToType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLShipToType.Get_ReceiverUpsAccountNumber: WideString;
begin
  Result := ChildNodes['ReceiverUpsAccountNumber'].Text;
end;

procedure TXMLShipToType.Set_ReceiverUpsAccountNumber(Value: WideString);
begin
  ChildNodes['ReceiverUpsAccountNumber'].NodeValue := Value;
end;

function TXMLShipToType.Get_LocationID: WideString;
begin
  Result := ChildNodes['LocationID'].Text;
end;

procedure TXMLShipToType.Set_LocationID(Value: WideString);
begin
  ChildNodes['LocationID'].NodeValue := Value;
end;

function TXMLShipToType.Get_ResidentialIndicator: WideString;
begin
  Result := ChildNodes['ResidentialIndicator'].Text;
end;

procedure TXMLShipToType.Set_ResidentialIndicator(Value: WideString);
begin
  ChildNodes['ResidentialIndicator'].NodeValue := Value;
end;

{ TXMLShipFromType }

function TXMLShipFromType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLShipFromType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLShipFromType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLShipFromType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLShipFromType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLShipFromType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLShipFromType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLShipFromType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLShipFromType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLShipFromType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLShipFromType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLShipFromType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLShipFromType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLShipFromType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLShipFromType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLShipFromType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLShipFromType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLShipFromType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLShipFromType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLShipFromType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLShipFromType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLShipFromType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLShipFromType.Get_FaxNumber: WideString;
begin
  Result := ChildNodes['FaxNumber'].Text;
end;

procedure TXMLShipFromType.Set_FaxNumber(Value: WideString);
begin
  ChildNodes['FaxNumber'].NodeValue := Value;
end;

function TXMLShipFromType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLShipFromType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLShipFromType.Get_TaxIDType: WideString;
begin
  Result := ChildNodes['TaxIDType'].Text;
end;

procedure TXMLShipFromType.Set_TaxIDType(Value: WideString);
begin
  ChildNodes['TaxIDType'].NodeValue := Value;
end;

function TXMLShipFromType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLShipFromType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

function TXMLShipFromType.Get_ResidentialIndicator: WideString;
begin
  Result := ChildNodes['ResidentialIndicator'].Text;
end;

procedure TXMLShipFromType.Set_ResidentialIndicator(Value: WideString);
begin
  ChildNodes['ResidentialIndicator'].NodeValue := Value;
end;

{ TXMLProducerType }

function TXMLProducerType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLProducerType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLProducerType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLProducerType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLProducerType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLProducerType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLProducerType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLProducerType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLProducerType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLProducerType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLProducerType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLProducerType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLProducerType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLProducerType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLProducerType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLProducerType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLProducerType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLProducerType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLProducerType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLProducerType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLProducerType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLProducerType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLProducerType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLProducerType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

{ TXMLUltimateConsigneeType }

function TXMLUltimateConsigneeType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLUltimateConsigneeType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLUltimateConsigneeType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

{ TXMLImporterType }

function TXMLImporterType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLImporterType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLImporterType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLImporterType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLImporterType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLImporterType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLImporterType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLImporterType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLImporterType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLImporterType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLImporterType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLImporterType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLImporterType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLImporterType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLImporterType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLImporterType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLImporterType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLImporterType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLImporterType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLImporterType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLImporterType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLImporterType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLImporterType.Get_TaxIDNumber: WideString;
begin
  Result := ChildNodes['TaxIDNumber'].Text;
end;

procedure TXMLImporterType.Set_TaxIDNumber(Value: WideString);
begin
  ChildNodes['TaxIDNumber'].NodeValue := Value;
end;

function TXMLImporterType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLImporterType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

{ TXMLThirdPartyReceiverType }

function TXMLThirdPartyReceiverType.Get_CustomerID: WideString;
begin
  Result := ChildNodes['CustomerID'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_CustomerID(Value: WideString);
begin
  ChildNodes['CustomerID'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_Attention: WideString;
begin
  Result := ChildNodes['Attention'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_Attention(Value: WideString);
begin
  ChildNodes['Attention'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_Address1: WideString;
begin
  Result := ChildNodes['Address1'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_Address1(Value: WideString);
begin
  ChildNodes['Address1'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_Address2: WideString;
begin
  Result := ChildNodes['Address2'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_Address2(Value: WideString);
begin
  ChildNodes['Address2'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_Address3: WideString;
begin
  Result := ChildNodes['Address3'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_Address3(Value: WideString);
begin
  ChildNodes['Address3'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_CountryTerritory: WideString;
begin
  Result := ChildNodes['CountryTerritory'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_CountryTerritory(Value: WideString);
begin
  ChildNodes['CountryTerritory'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_PostalCode: WideString;
begin
  Result := ChildNodes['PostalCode'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_PostalCode(Value: WideString);
begin
  ChildNodes['PostalCode'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_CityOrTown: WideString;
begin
  Result := ChildNodes['CityOrTown'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_CityOrTown(Value: WideString);
begin
  ChildNodes['CityOrTown'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_StateProvinceCounty: WideString;
begin
  Result := ChildNodes['StateProvinceCounty'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_StateProvinceCounty(Value: WideString);
begin
  ChildNodes['StateProvinceCounty'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_Telephone: WideString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_Telephone(Value: WideString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_FaxNumber: WideString;
begin
  Result := ChildNodes['FaxNumber'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_FaxNumber(Value: WideString);
begin
  ChildNodes['FaxNumber'].NodeValue := Value;
end;

function TXMLThirdPartyReceiverType.Get_UpsAccountNumber: WideString;
begin
  Result := ChildNodes['UpsAccountNumber'].Text;
end;

procedure TXMLThirdPartyReceiverType.Set_UpsAccountNumber(Value: WideString);
begin
  ChildNodes['UpsAccountNumber'].NodeValue := Value;
end;

{ TXMLShipmentInformationType }

procedure TXMLShipmentInformationType.AfterConstruction;
begin
  RegisterChildNode('DeclaredValue', TXMLDeclaredValueType);
  RegisterChildNode('AdditionalHandling', TXMLAdditionalHandlingType);
  RegisterChildNode('COD', TXMLCODType);
  RegisterChildNode('QVNOption', TXMLQVNOptionType);
  RegisterChildNode('SpecialCommodities', TXMLSpecialCommoditiesType);
  RegisterChildNode('DeliveryConfirmation', TXMLDeliveryConfirmationType);
  RegisterChildNode('UPSExchangeCollect', TXMLUPSExchangeCollectType);
  RegisterChildNode('ReturnService', TXMLReturnServiceType);
  RegisterChildNode('NotifyBeforeDelOption', TXMLNotifyBeforeDelOptionType);
  RegisterChildNode('HandlingChargeOption', TXMLHandlingChargeOptionType);
  inherited;
end;

function TXMLShipmentInformationType.Get_VoidIndicator: WideString;
begin
  Result := ChildNodes['VoidIndicator'].Text;
end;

procedure TXMLShipmentInformationType.Set_VoidIndicator(Value: WideString);
begin
  ChildNodes['VoidIndicator'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_ServiceType: WideString;
begin
  Result := ChildNodes['ServiceType'].Text;
end;

procedure TXMLShipmentInformationType.Set_ServiceType(Value: WideString);
begin
  ChildNodes['ServiceType'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_PackageType: WideString;
begin
  Result := ChildNodes['PackageType'].Text;
end;

procedure TXMLShipmentInformationType.Set_PackageType(Value: WideString);
begin
  ChildNodes['PackageType'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_NumberOfPackages: WideString;
begin
  Result := ChildNodes['NumberOfPackages'].Text;
end;

procedure TXMLShipmentInformationType.Set_NumberOfPackages(Value: WideString);
begin
  ChildNodes['NumberOfPackages'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_ShipmentActualWeight: WideString;
begin
  Result := ChildNodes['ShipmentActualWeight'].Text;
end;

procedure TXMLShipmentInformationType.Set_ShipmentActualWeight(Value: WideString);
begin
  ChildNodes['ShipmentActualWeight'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_ShipmentDimensionalWeight: WideString;
begin
  Result := ChildNodes['ShipmentDimensionalWeight'].Text;
end;

procedure TXMLShipmentInformationType.Set_ShipmentDimensionalWeight(Value: WideString);
begin
  ChildNodes['ShipmentDimensionalWeight'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_DescriptionOfGoods: WideString;
begin
  Result := ChildNodes['DescriptionOfGoods'].Text;
end;

procedure TXMLShipmentInformationType.Set_DescriptionOfGoods(Value: WideString);
begin
  ChildNodes['DescriptionOfGoods'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Reference1: WideString;
begin
  Result := ChildNodes['Reference1'].Text;
end;

procedure TXMLShipmentInformationType.Set_Reference1(Value: WideString);
begin
  ChildNodes['Reference1'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Reference2: WideString;
begin
  Result := ChildNodes['Reference2'].Text;
end;

procedure TXMLShipmentInformationType.Set_Reference2(Value: WideString);
begin
  ChildNodes['Reference2'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Reference3: WideString;
begin
  Result := ChildNodes['Reference3'].Text;
end;

procedure TXMLShipmentInformationType.Set_Reference3(Value: WideString);
begin
  ChildNodes['Reference3'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Reference4: WideString;
begin
  Result := ChildNodes['Reference4'].Text;
end;

procedure TXMLShipmentInformationType.Set_Reference4(Value: WideString);
begin
  ChildNodes['Reference4'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Reference5: WideString;
begin
  Result := ChildNodes['Reference5'].Text;
end;

procedure TXMLShipmentInformationType.Set_Reference5(Value: WideString);
begin
  ChildNodes['Reference5'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_DocumentOnly: WideString;
begin
  Result := ChildNodes['DocumentOnly'].Text;
end;

procedure TXMLShipmentInformationType.Set_DocumentOnly(Value: WideString);
begin
  ChildNodes['DocumentOnly'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_GoodsNotInFreeCirculation: WideString;
begin
  Result := ChildNodes['GoodsNotInFreeCirculation'].Text;
end;

procedure TXMLShipmentInformationType.Set_GoodsNotInFreeCirculation(Value: WideString);
begin
  ChildNodes['GoodsNotInFreeCirculation'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_SpecialInstructionForShipment: WideString;
begin
  Result := ChildNodes['SpecialInstructionForShipment'].Text;
end;

procedure TXMLShipmentInformationType.Set_SpecialInstructionForShipment(Value: WideString);
begin
  ChildNodes['SpecialInstructionForShipment'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_ShipperNumber: WideString;
begin
  Result := ChildNodes['ShipperNumber'].Text;
end;

procedure TXMLShipmentInformationType.Set_ShipperNumber(Value: WideString);
begin
  ChildNodes['ShipperNumber'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_BillingOption: WideString;
begin
  Result := ChildNodes['BillingOption'].Text;
end;

procedure TXMLShipmentInformationType.Set_BillingOption(Value: WideString);
begin
  ChildNodes['BillingOption'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_BillTransportationTo: WideString;
begin
  Result := ChildNodes['BillTransportationTo'].Text;
end;

procedure TXMLShipmentInformationType.Set_BillTransportationTo(Value: WideString);
begin
  ChildNodes['BillTransportationTo'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_BillDutyTaxTo: WideString;
begin
  Result := ChildNodes['BillDutyTaxTo'].Text;
end;

procedure TXMLShipmentInformationType.Set_BillDutyTaxTo(Value: WideString);
begin
  ChildNodes['BillDutyTaxTo'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_SplitDutyAndTax: WideString;
begin
  Result := ChildNodes['SplitDutyAndTax'].Text;
end;

procedure TXMLShipmentInformationType.Set_SplitDutyAndTax(Value: WideString);
begin
  ChildNodes['SplitDutyAndTax'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_SaturdayDelivery: WideString;
begin
  Result := ChildNodes['SaturdayDelivery'].Text;
end;

procedure TXMLShipmentInformationType.Set_SaturdayDelivery(Value: WideString);
begin
  ChildNodes['SaturdayDelivery'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_SaturdayPickUp: WideString;
begin
  Result := ChildNodes['SaturdayPickUp'].Text;
end;

procedure TXMLShipmentInformationType.Set_SaturdayPickUp(Value: WideString);
begin
  ChildNodes['SaturdayPickUp'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_DeclaredValue: IXMLDeclaredValueType;
begin
  Result := ChildNodes['DeclaredValue'] as IXMLDeclaredValueType;
end;

function TXMLShipmentInformationType.Get_MerchandiseDescriptionForPackage: WideString;
begin
  Result := ChildNodes['MerchandiseDescriptionForPackage'].Text;
end;

procedure TXMLShipmentInformationType.Set_MerchandiseDescriptionForPackage(Value: WideString);
begin
  ChildNodes['MerchandiseDescriptionForPackage'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_AdditionalHandling: IXMLAdditionalHandlingType;
begin
  Result := ChildNodes['AdditionalHandling'] as IXMLAdditionalHandlingType;
end;

function TXMLShipmentInformationType.Get_COD: IXMLCODType;
begin
  Result := ChildNodes['COD'] as IXMLCODType;
end;

function TXMLShipmentInformationType.Get_ReturnOfDocument: WideString;
begin
  Result := ChildNodes['ReturnOfDocument'].Text;
end;

procedure TXMLShipmentInformationType.Set_ReturnOfDocument(Value: WideString);
begin
  ChildNodes['ReturnOfDocument'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_QVNOption: IXMLQVNOptionType;
begin
  Result := ChildNodes['QVNOption'] as IXMLQVNOptionType;
end;

function TXMLShipmentInformationType.Get_SpecialCommodities: IXMLSpecialCommoditiesType;
begin
  Result := ChildNodes['SpecialCommodities'] as IXMLSpecialCommoditiesType;
end;

function TXMLShipmentInformationType.Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
begin
  Result := ChildNodes['DeliveryConfirmation'] as IXMLDeliveryConfirmationType;
end;

function TXMLShipmentInformationType.Get_UserDefinedID: WideString;
begin
  Result := ChildNodes['UserDefinedID'].Text;
end;

procedure TXMLShipmentInformationType.Set_UserDefinedID(Value: WideString);
begin
  ChildNodes['UserDefinedID'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_UPSExchangeCollect: IXMLUPSExchangeCollectType;
begin
  Result := ChildNodes['UPSExchangeCollect'] as IXMLUPSExchangeCollectType;
end;

function TXMLShipmentInformationType.Get_ReturnService: IXMLReturnServiceType;
begin
  Result := ChildNodes['ReturnService'] as IXMLReturnServiceType;
end;

function TXMLShipmentInformationType.Get_NotifyBeforeDelOption: IXMLNotifyBeforeDelOptionType;
begin
  Result := ChildNodes['NotifyBeforeDelOption'] as IXMLNotifyBeforeDelOptionType;
end;

function TXMLShipmentInformationType.Get_HandlingChargeOption: IXMLHandlingChargeOptionType;
begin
  Result := ChildNodes['HandlingChargeOption'] as IXMLHandlingChargeOptionType;
end;

function TXMLShipmentInformationType.Get_FoodItems: WideString;
begin
  Result := ChildNodes['FoodItems'].Text;
end;

procedure TXMLShipmentInformationType.Set_FoodItems(Value: WideString);
begin
  ChildNodes['FoodItems'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_HundredWeightPricingApplied: WideString;
begin
  Result := ChildNodes['HundredWeightPricingApplied'].Text;
end;

procedure TXMLShipmentInformationType.Set_HundredWeightPricingApplied(Value: WideString);
begin
  ChildNodes['HundredWeightPricingApplied'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_USI: WideString;
begin
  Result := ChildNodes['USI'].Text;
end;

procedure TXMLShipmentInformationType.Set_USI(Value: WideString);
begin
  ChildNodes['USI'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_SubProNumber: WideString;
begin
  Result := ChildNodes['SubProNumber'].Text;
end;

procedure TXMLShipmentInformationType.Set_SubProNumber(Value: WideString);
begin
  ChildNodes['SubProNumber'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Length: WideString;
begin
  Result := ChildNodes['Length'].Text;
end;

procedure TXMLShipmentInformationType.Set_Length(Value: WideString);
begin
  ChildNodes['Length'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Width: WideString;
begin
  Result := ChildNodes['Width'].Text;
end;

procedure TXMLShipmentInformationType.Set_Width(Value: WideString);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_Height: WideString;
begin
  Result := ChildNodes['Height'].Text;
end;

procedure TXMLShipmentInformationType.Set_Height(Value: WideString);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_AdditionalDocuments: WideString;
begin
  Result := ChildNodes['AdditionalDocuments'].Text;
end;

procedure TXMLShipmentInformationType.Set_AdditionalDocuments(Value: WideString);
begin
  ChildNodes['AdditionalDocuments'].NodeValue := Value;
end;

function TXMLShipmentInformationType.Get_ProactiveResponseOption: WideString;
begin
  Result := ChildNodes['ProactiveResponseOption'].Text;
end;

procedure TXMLShipmentInformationType.Set_ProactiveResponseOption(Value: WideString);
begin
  ChildNodes['ProactiveResponseOption'].NodeValue := Value;
end;

{ TXMLDeclaredValueType }

function TXMLDeclaredValueType.Get_Amount: WideString;
begin
  Result := ChildNodes['Amount'].Text;
end;

procedure TXMLDeclaredValueType.Set_Amount(Value: WideString);
begin
  ChildNodes['Amount'].NodeValue := Value;
end;

function TXMLDeclaredValueType.Get_ShipperPaid: WideString;
begin
  Result := ChildNodes['ShipperPaid'].Text;
end;

procedure TXMLDeclaredValueType.Set_ShipperPaid(Value: WideString);
begin
  ChildNodes['ShipperPaid'].NodeValue := Value;
end;

{ TXMLQVNOptionType }

procedure TXMLQVNOptionType.AfterConstruction;
begin
  RegisterChildNode('QVNRecipientAndNotificationTypes', TXMLQVNRecipientAndNotificationTypesType);
  FQVNRecipientAndNotificationTypes := CreateCollection(TXMLQVNRecipientAndNotificationTypesTypeList, IXMLQVNRecipientAndNotificationTypesType, 'QVNRecipientAndNotificationTypes') as IXMLQVNRecipientAndNotificationTypesTypeList;
  inherited;
end;

function TXMLQVNOptionType.Get_QVNRecipientAndNotificationTypes: IXMLQVNRecipientAndNotificationTypesTypeList;
begin
  Result := FQVNRecipientAndNotificationTypes;
end;

function TXMLQVNOptionType.Get_ShipFromCompanyOrName: WideString;
begin
  Result := ChildNodes['ShipFromCompanyOrName'].Text;
end;

procedure TXMLQVNOptionType.Set_ShipFromCompanyOrName(Value: WideString);
begin
  ChildNodes['ShipFromCompanyOrName'].NodeValue := Value;
end;

function TXMLQVNOptionType.Get_FailedEMailAddress: WideString;
begin
  Result := ChildNodes['FailedEMailAddress'].Text;
end;

procedure TXMLQVNOptionType.Set_FailedEMailAddress(Value: WideString);
begin
  ChildNodes['FailedEMailAddress'].NodeValue := Value;
end;

function TXMLQVNOptionType.Get_SubjectLine: WideString;
begin
  Result := ChildNodes['SubjectLine'].Text;
end;

procedure TXMLQVNOptionType.Set_SubjectLine(Value: WideString);
begin
  ChildNodes['SubjectLine'].NodeValue := Value;
end;

function TXMLQVNOptionType.Get_Memo: WideString;
begin
  Result := ChildNodes['Memo'].Text;
end;

procedure TXMLQVNOptionType.Set_Memo(Value: WideString);
begin
  ChildNodes['Memo'].NodeValue := Value;
end;

{ TXMLQVNRecipientAndNotificationTypesType }

function TXMLQVNRecipientAndNotificationTypesType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLQVNRecipientAndNotificationTypesType.Get_ContactName: WideString;
begin
  Result := ChildNodes['ContactName'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_ContactName(Value: WideString);
begin
  ChildNodes['ContactName'].NodeValue := Value;
end;

function TXMLQVNRecipientAndNotificationTypesType.Get_EMailAddress: WideString;
begin
  Result := ChildNodes['EMailAddress'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_EMailAddress(Value: WideString);
begin
  ChildNodes['EMailAddress'].NodeValue := Value;
end;

function TXMLQVNRecipientAndNotificationTypesType.Get_Ship: WideString;
begin
  Result := ChildNodes['Ship'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_Ship(Value: WideString);
begin
  ChildNodes['Ship'].NodeValue := Value;
end;

function TXMLQVNRecipientAndNotificationTypesType.Get_Exception: WideString;
begin
  Result := ChildNodes['Exception'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_Exception(Value: WideString);
begin
  ChildNodes['Exception'].NodeValue := Value;
end;

function TXMLQVNRecipientAndNotificationTypesType.Get_Delivery: WideString;
begin
  Result := ChildNodes['Delivery'].Text;
end;

procedure TXMLQVNRecipientAndNotificationTypesType.Set_Delivery(Value: WideString);
begin
  ChildNodes['Delivery'].NodeValue := Value;
end;

{ TXMLQVNRecipientAndNotificationTypesTypeList }

function TXMLQVNRecipientAndNotificationTypesTypeList.Add: IXMLQVNRecipientAndNotificationTypesType;
begin
  Result := AddItem(-1) as IXMLQVNRecipientAndNotificationTypesType;
end;

function TXMLQVNRecipientAndNotificationTypesTypeList.Insert(const Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
begin
  Result := AddItem(Index) as IXMLQVNRecipientAndNotificationTypesType;
end;
function TXMLQVNRecipientAndNotificationTypesTypeList.Get_Item(Index: Integer): IXMLQVNRecipientAndNotificationTypesType;
begin
  Result := List[Index] as IXMLQVNRecipientAndNotificationTypesType;
end;

{ TXMLDeliveryConfirmationType }

function TXMLDeliveryConfirmationType.Get_SignatureRequired: WideString;
begin
  Result := ChildNodes['SignatureRequired'].Text;
end;

procedure TXMLDeliveryConfirmationType.Set_SignatureRequired(Value: WideString);
begin
  ChildNodes['SignatureRequired'].NodeValue := Value;
end;

function TXMLDeliveryConfirmationType.Get_AdultSignatureRequired: WideString;
begin
  Result := ChildNodes['AdultSignatureRequired'].Text;
end;

procedure TXMLDeliveryConfirmationType.Set_AdultSignatureRequired(Value: WideString);
begin
  ChildNodes['AdultSignatureRequired'].NodeValue := Value;
end;

{ TXMLNotifyBeforeDelOptionType }

function TXMLNotifyBeforeDelOptionType.Get_ContactPerson: WideString;
begin
  Result := ChildNodes['ContactPerson'].Text;
end;

procedure TXMLNotifyBeforeDelOptionType.Set_ContactPerson(Value: WideString);
begin
  ChildNodes['ContactPerson'].NodeValue := Value;
end;

function TXMLNotifyBeforeDelOptionType.Get_Phone: WideString;
begin
  Result := ChildNodes['Phone'].Text;
end;

procedure TXMLNotifyBeforeDelOptionType.Set_Phone(Value: WideString);
begin
  ChildNodes['Phone'].NodeValue := Value;
end;

{ TXMLHandlingChargeOptionType }

function TXMLHandlingChargeOptionType.Get_HandlingChargeType: WideString;
begin
  Result := ChildNodes['HandlingChargeType'].Text;
end;

procedure TXMLHandlingChargeOptionType.Set_HandlingChargeType(Value: WideString);
begin
  ChildNodes['HandlingChargeType'].NodeValue := Value;
end;

function TXMLHandlingChargeOptionType.Get_HCAmountIncreaseDecreaseType: WideString;
begin
  Result := ChildNodes['HCAmountIncreaseDecreaseType'].Text;
end;

procedure TXMLHandlingChargeOptionType.Set_HCAmountIncreaseDecreaseType(Value: WideString);
begin
  ChildNodes['HCAmountIncreaseDecreaseType'].NodeValue := Value;
end;

function TXMLHandlingChargeOptionType.Get_HandlingChargeFlatRate: WideString;
begin
  Result := ChildNodes['HandlingChargeFlatRate'].Text;
end;

procedure TXMLHandlingChargeOptionType.Set_HandlingChargeFlatRate(Value: WideString);
begin
  ChildNodes['HandlingChargeFlatRate'].NodeValue := Value;
end;

function TXMLHandlingChargeOptionType.Get_HandlingChargePercentage: WideString;
begin
  Result := ChildNodes['HandlingChargePercentage'].Text;
end;

procedure TXMLHandlingChargeOptionType.Set_HandlingChargePercentage(Value: WideString);
begin
  ChildNodes['HandlingChargePercentage'].NodeValue := Value;
end;

{ TXMLLTLLineItemType }

function TXMLLTLLineItemType.Get_FreightClass: WideString;
begin
  Result := ChildNodes['FreightClass'].Text;
end;

procedure TXMLLTLLineItemType.Set_FreightClass(Value: WideString);
begin
  ChildNodes['FreightClass'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_LineDescription: WideString;
begin
  Result := ChildNodes['LineDescription'].Text;
end;

procedure TXMLLTLLineItemType.Set_LineDescription(Value: WideString);
begin
  ChildNodes['LineDescription'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_NumberOfIdenticalHandlingUnit: WideString;
begin
  Result := ChildNodes['NumberOfIdenticalHandlingUnit'].Text;
end;

procedure TXMLLTLLineItemType.Set_NumberOfIdenticalHandlingUnit(Value: WideString);
begin
  ChildNodes['NumberOfIdenticalHandlingUnit'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_HandlingUnitWeight: WideString;
begin
  Result := ChildNodes['HandlingUnitWeight'].Text;
end;

procedure TXMLLTLLineItemType.Set_HandlingUnitWeight(Value: WideString);
begin
  ChildNodes['HandlingUnitWeight'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_PieceOnPallet: WideString;
begin
  Result := ChildNodes['PieceOnPallet'].Text;
end;

procedure TXMLLTLLineItemType.Set_PieceOnPallet(Value: WideString);
begin
  ChildNodes['PieceOnPallet'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_Length: WideString;
begin
  Result := ChildNodes['Length'].Text;
end;

procedure TXMLLTLLineItemType.Set_Length(Value: WideString);
begin
  ChildNodes['Length'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_Width: WideString;
begin
  Result := ChildNodes['Width'].Text;
end;

procedure TXMLLTLLineItemType.Set_Width(Value: WideString);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_Height: WideString;
begin
  Result := ChildNodes['Height'].Text;
end;

procedure TXMLLTLLineItemType.Set_Height(Value: WideString);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLLTLLineItemType.Get_PackagingType: WideString;
begin
  Result := ChildNodes['PackagingType'].Text;
end;

procedure TXMLLTLLineItemType.Set_PackagingType(Value: WideString);
begin
  ChildNodes['PackagingType'].NodeValue := Value;
end;

{ TXMLLTLLineItemTypeList }

function TXMLLTLLineItemTypeList.Add: IXMLLTLLineItemType;
begin
  Result := AddItem(-1) as IXMLLTLLineItemType;
end;

function TXMLLTLLineItemTypeList.Insert(const Index: Integer): IXMLLTLLineItemType;
begin
  Result := AddItem(Index) as IXMLLTLLineItemType;
end;
function TXMLLTLLineItemTypeList.Get_Item(Index: Integer): IXMLLTLLineItemType;
begin
  Result := List[Index] as IXMLLTLLineItemType;
end;

{ TXMLPackageType }

procedure TXMLPackageType.AfterConstruction;
begin
  RegisterChildNode('COD', TXMLCODType);
  RegisterChildNode('DeclaredValue', TXMLDeclaredValueType);
  RegisterChildNode('DeliveryConfirmation', TXMLDeliveryConfirmationType);
  RegisterChildNode('QVNOrReturnNotificationOption', TXMLQVNOrReturnNotificationOptionType);
  RegisterChildNode('VerbalConfirmationOption', TXMLVerbalConfirmationOptionType);
  RegisterChildNode('FlexibleParcelInsuranceOption', TXMLFlexibleParcelInsuranceOptionType);
  RegisterChildNode('DryIceOption', TXMLDryIceOptionType);
  inherited;
end;

function TXMLPackageType.Get_PackageType: WideString;
begin
  Result := ChildNodes['PackageType'].Text;
end;

procedure TXMLPackageType.Set_PackageType(Value: WideString);
begin
  ChildNodes['PackageType'].NodeValue := Value;
end;

function TXMLPackageType.Get_Weight: WideString;
begin
  Result := ChildNodes['Weight'].Text;
end;

procedure TXMLPackageType.Set_Weight(Value: WideString);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

function TXMLPackageType.Get_TrackingNumber: WideString;
begin
  Result := ChildNodes['TrackingNumber'].Text;
end;

procedure TXMLPackageType.Set_TrackingNumber(Value: WideString);
begin
  ChildNodes['TrackingNumber'].NodeValue := Value;
end;

function TXMLPackageType.Get_USPSNumber: WideString;
begin
  Result := ChildNodes['USPSNumber'].Text;
end;

procedure TXMLPackageType.Set_USPSNumber(Value: WideString);
begin
  ChildNodes['USPSNumber'].NodeValue := Value;
end;

function TXMLPackageType.Get_LargePackageIndicator: WideString;
begin
  Result := ChildNodes['LargePackageIndicator'].Text;
end;

procedure TXMLPackageType.Set_LargePackageIndicator(Value: WideString);
begin
  ChildNodes['LargePackageIndicator'].NodeValue := Value;
end;

function TXMLPackageType.Get_Reference1: WideString;
begin
  Result := ChildNodes['Reference1'].Text;
end;

procedure TXMLPackageType.Set_Reference1(Value: WideString);
begin
  ChildNodes['Reference1'].NodeValue := Value;
end;

function TXMLPackageType.Get_Reference2: WideString;
begin
  Result := ChildNodes['Reference2'].Text;
end;

procedure TXMLPackageType.Set_Reference2(Value: WideString);
begin
  ChildNodes['Reference2'].NodeValue := Value;
end;

function TXMLPackageType.Get_Reference3: WideString;
begin
  Result := ChildNodes['Reference3'].Text;
end;

procedure TXMLPackageType.Set_Reference3(Value: WideString);
begin
  ChildNodes['Reference3'].NodeValue := Value;
end;

function TXMLPackageType.Get_Reference4: WideString;
begin
  Result := ChildNodes['Reference4'].Text;
end;

procedure TXMLPackageType.Set_Reference4(Value: WideString);
begin
  ChildNodes['Reference4'].NodeValue := Value;
end;

function TXMLPackageType.Get_Reference5: WideString;
begin
  Result := ChildNodes['Reference5'].Text;
end;

procedure TXMLPackageType.Set_Reference5(Value: WideString);
begin
  ChildNodes['Reference5'].NodeValue := Value;
end;

function TXMLPackageType.Get_AdditionalHandlingOption: WideString;
begin
  Result := ChildNodes['AdditionalHandlingOption'].Text;
end;

procedure TXMLPackageType.Set_AdditionalHandlingOption(Value: WideString);
begin
  ChildNodes['AdditionalHandlingOption'].NodeValue := Value;
end;

function TXMLPackageType.Get_ShipperReleaseOption: WideString;
begin
  Result := ChildNodes['ShipperReleaseOption'].Text;
end;

procedure TXMLPackageType.Set_ShipperReleaseOption(Value: WideString);
begin
  ChildNodes['ShipperReleaseOption'].NodeValue := Value;
end;

function TXMLPackageType.Get_COD: IXMLCODType;
begin
  Result := ChildNodes['COD'] as IXMLCODType;
end;

function TXMLPackageType.Get_DeclaredValue: IXMLDeclaredValueType;
begin
  Result := ChildNodes['DeclaredValue'] as IXMLDeclaredValueType;
end;

function TXMLPackageType.Get_DeliveryConfirmation: IXMLDeliveryConfirmationType;
begin
  Result := ChildNodes['DeliveryConfirmation'] as IXMLDeliveryConfirmationType;
end;

function TXMLPackageType.Get_QVNOrReturnNotificationOption: IXMLQVNOrReturnNotificationOptionType;
begin
  Result := ChildNodes['QVNOrReturnNotificationOption'] as IXMLQVNOrReturnNotificationOptionType;
end;

function TXMLPackageType.Get_VerbalConfirmationOption: IXMLVerbalConfirmationOptionType;
begin
  Result := ChildNodes['VerbalConfirmationOption'] as IXMLVerbalConfirmationOptionType;
end;

function TXMLPackageType.Get_DangerousGoodsOption: WideString;
begin
  Result := ChildNodes['DangerousGoodsOption'].Text;
end;

procedure TXMLPackageType.Set_DangerousGoodsOption(Value: WideString);
begin
  ChildNodes['DangerousGoodsOption'].NodeValue := Value;
end;

function TXMLPackageType.Get_Length: WideString;
begin
  Result := ChildNodes['Length'].Text;
end;

procedure TXMLPackageType.Set_Length(Value: WideString);
begin
  ChildNodes['Length'].NodeValue := Value;
end;

function TXMLPackageType.Get_Width: WideString;
begin
  Result := ChildNodes['Width'].Text;
end;

procedure TXMLPackageType.Set_Width(Value: WideString);
begin
  ChildNodes['Width'].NodeValue := Value;
end;

function TXMLPackageType.Get_Height: WideString;
begin
  Result := ChildNodes['Height'].Text;
end;

procedure TXMLPackageType.Set_Height(Value: WideString);
begin
  ChildNodes['Height'].NodeValue := Value;
end;

function TXMLPackageType.Get_MerchandiseDescription: WideString;
begin
  Result := ChildNodes['MerchandiseDescription'].Text;
end;

procedure TXMLPackageType.Set_MerchandiseDescription(Value: WideString);
begin
  ChildNodes['MerchandiseDescription'].NodeValue := Value;
end;

function TXMLPackageType.Get_FlexibleParcelInsuranceOption: IXMLFlexibleParcelInsuranceOptionType;
begin
  Result := ChildNodes['FlexibleParcelInsuranceOption'] as IXMLFlexibleParcelInsuranceOptionType;
end;

function TXMLPackageType.Get_ProactiveResponseOption: WideString;
begin
  Result := ChildNodes['ProactiveResponseOption'].Text;
end;

procedure TXMLPackageType.Set_ProactiveResponseOption(Value: WideString);
begin
  ChildNodes['ProactiveResponseOption'].NodeValue := Value;
end;

function TXMLPackageType.Get_DryIceOption: IXMLDryIceOptionType;
begin
  Result := ChildNodes['DryIceOption'] as IXMLDryIceOptionType;
end;

{ TXMLPackageTypeList }

function TXMLPackageTypeList.Add: IXMLPackageType;
begin
  Result := AddItem(-1) as IXMLPackageType;
end;

function TXMLPackageTypeList.Insert(const Index: Integer): IXMLPackageType;
begin
  Result := AddItem(Index) as IXMLPackageType;
end;
function TXMLPackageTypeList.Get_Item(Index: Integer): IXMLPackageType;
begin
  Result := List[Index] as IXMLPackageType;
end;

{ TXMLQVNOrReturnNotificationOptionType }

procedure TXMLQVNOrReturnNotificationOptionType.AfterConstruction;
begin
  RegisterChildNode('QVNOrReturnRecipientAndNotificationTypes', TXMLQVNOrReturnRecipientAndNotificationTypesType);
  FQVNOrReturnRecipientAndNotificationTypes := CreateCollection(TXMLQVNOrReturnRecipientAndNotificationTypesTypeList, IXMLQVNOrReturnRecipientAndNotificationTypesType, 'QVNOrReturnRecipientAndNotificationTypes') as IXMLQVNOrReturnRecipientAndNotificationTypesTypeList;
  inherited;
end;

function TXMLQVNOrReturnNotificationOptionType.Get_QVNOrReturnRecipientAndNotificationTypes: IXMLQVNOrReturnRecipientAndNotificationTypesTypeList;
begin
  Result := FQVNOrReturnRecipientAndNotificationTypes;
end;

function TXMLQVNOrReturnNotificationOptionType.Get_ShipFromCompanyOrName: WideString;
begin
  Result := ChildNodes['ShipFromCompanyOrName'].Text;
end;

procedure TXMLQVNOrReturnNotificationOptionType.Set_ShipFromCompanyOrName(Value: WideString);
begin
  ChildNodes['ShipFromCompanyOrName'].NodeValue := Value;
end;

function TXMLQVNOrReturnNotificationOptionType.Get_FailedEMailAddress: WideString;
begin
  Result := ChildNodes['FailedEMailAddress'].Text;
end;

procedure TXMLQVNOrReturnNotificationOptionType.Set_FailedEMailAddress(Value: WideString);
begin
  ChildNodes['FailedEMailAddress'].NodeValue := Value;
end;

function TXMLQVNOrReturnNotificationOptionType.Get_SubjectLine: WideString;
begin
  Result := ChildNodes['SubjectLine'].Text;
end;

procedure TXMLQVNOrReturnNotificationOptionType.Set_SubjectLine(Value: WideString);
begin
  ChildNodes['SubjectLine'].NodeValue := Value;
end;

function TXMLQVNOrReturnNotificationOptionType.Get_Memo: WideString;
begin
  Result := ChildNodes['Memo'].Text;
end;

procedure TXMLQVNOrReturnNotificationOptionType.Set_Memo(Value: WideString);
begin
  ChildNodes['Memo'].NodeValue := Value;
end;

{ TXMLQVNOrReturnRecipientAndNotificationTypesType }

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_CompanyOrName: WideString;
begin
  Result := ChildNodes['CompanyOrName'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_CompanyOrName(Value: WideString);
begin
  ChildNodes['CompanyOrName'].NodeValue := Value;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_ContactName: WideString;
begin
  Result := ChildNodes['ContactName'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_ContactName(Value: WideString);
begin
  ChildNodes['ContactName'].NodeValue := Value;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_EMailAddress: WideString;
begin
  Result := ChildNodes['EMailAddress'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_EMailAddress(Value: WideString);
begin
  ChildNodes['EMailAddress'].NodeValue := Value;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_Ship: WideString;
begin
  Result := ChildNodes['Ship'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_Ship(Value: WideString);
begin
  ChildNodes['Ship'].NodeValue := Value;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_Exception: WideString;
begin
  Result := ChildNodes['Exception'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_Exception(Value: WideString);
begin
  ChildNodes['Exception'].NodeValue := Value;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesType.Get_Delivery: WideString;
begin
  Result := ChildNodes['Delivery'].Text;
end;

procedure TXMLQVNOrReturnRecipientAndNotificationTypesType.Set_Delivery(Value: WideString);
begin
  ChildNodes['Delivery'].NodeValue := Value;
end;

{ TXMLQVNOrReturnRecipientAndNotificationTypesTypeList }

function TXMLQVNOrReturnRecipientAndNotificationTypesTypeList.Add: IXMLQVNOrReturnRecipientAndNotificationTypesType;
begin
  Result := AddItem(-1) as IXMLQVNOrReturnRecipientAndNotificationTypesType;
end;

function TXMLQVNOrReturnRecipientAndNotificationTypesTypeList.Insert(const Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
begin
  Result := AddItem(Index) as IXMLQVNOrReturnRecipientAndNotificationTypesType;
end;
function TXMLQVNOrReturnRecipientAndNotificationTypesTypeList.Get_Item(Index: Integer): IXMLQVNOrReturnRecipientAndNotificationTypesType;
begin
  Result := List[Index] as IXMLQVNOrReturnRecipientAndNotificationTypesType;
end;

{ TXMLVerbalConfirmationOptionType }

function TXMLVerbalConfirmationOptionType.Get_VerbalConfirmationContactName: WideString;
begin
  Result := ChildNodes['VerbalConfirmationContactName'].Text;
end;

procedure TXMLVerbalConfirmationOptionType.Set_VerbalConfirmationContactName(Value: WideString);
begin
  ChildNodes['VerbalConfirmationContactName'].NodeValue := Value;
end;

function TXMLVerbalConfirmationOptionType.Get_VerbalConfirmationTelephone: WideString;
begin
  Result := ChildNodes['VerbalConfirmationTelephone'].Text;
end;

procedure TXMLVerbalConfirmationOptionType.Set_VerbalConfirmationTelephone(Value: WideString);
begin
  ChildNodes['VerbalConfirmationTelephone'].NodeValue := Value;
end;

{ TXMLFlexibleParcelInsuranceOptionType }

function TXMLFlexibleParcelInsuranceOptionType.Get_FlexibleParcelInsuranceType: WideString;
begin
  Result := ChildNodes['FlexibleParcelInsuranceType'].Text;
end;

procedure TXMLFlexibleParcelInsuranceOptionType.Set_FlexibleParcelInsuranceType(Value: WideString);
begin
  ChildNodes['FlexibleParcelInsuranceType'].NodeValue := Value;
end;

function TXMLFlexibleParcelInsuranceOptionType.Get_FlexibleParcelInsuranceValue: WideString;
begin
  Result := ChildNodes['FlexibleParcelInsuranceValue'].Text;
end;

procedure TXMLFlexibleParcelInsuranceOptionType.Set_FlexibleParcelInsuranceValue(Value: WideString);
begin
  ChildNodes['FlexibleParcelInsuranceValue'].NodeValue := Value;
end;

{ TXMLDryIceOptionType }

function TXMLDryIceOptionType.Get_Weight: WideString;
begin
  Result := ChildNodes['Weight'].Text;
end;

procedure TXMLDryIceOptionType.Set_Weight(Value: WideString);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

function TXMLDryIceOptionType.Get_UnitOfMeasure: WideString;
begin
  Result := ChildNodes['UnitOfMeasure'].Text;
end;

procedure TXMLDryIceOptionType.Set_UnitOfMeasure(Value: WideString);
begin
  ChildNodes['UnitOfMeasure'].NodeValue := Value;
end;

function TXMLDryIceOptionType.Get_RegulationSet: WideString;
begin
  Result := ChildNodes['RegulationSet'].Text;
end;

procedure TXMLDryIceOptionType.Set_RegulationSet(Value: WideString);
begin
  ChildNodes['RegulationSet'].NodeValue := Value;
end;

function TXMLDryIceOptionType.Get_IsMedical: WideString;
begin
  Result := ChildNodes['IsMedical'].Text;
end;

procedure TXMLDryIceOptionType.Set_IsMedical(Value: WideString);
begin
  ChildNodes['IsMedical'].NodeValue := Value;
end;

function TXMLDryIceOptionType.Get_DryIcePackagingType: WideString;
begin
  Result := ChildNodes['DryIcePackagingType'].Text;
end;

procedure TXMLDryIceOptionType.Set_DryIcePackagingType(Value: WideString);
begin
  ChildNodes['DryIcePackagingType'].NodeValue := Value;
end;

{ TXMLInternationalDocumentationType }

function TXMLInternationalDocumentationType.Get_InvoiceImporterSameAsShipTo: WideString;
begin
  Result := ChildNodes['InvoiceImporterSameAsShipTo'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceImporterSameAsShipTo(Value: WideString);
begin
  ChildNodes['InvoiceImporterSameAsShipTo'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceTermOfSale: WideString;
begin
  Result := ChildNodes['InvoiceTermOfSale'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceTermOfSale(Value: WideString);
begin
  ChildNodes['InvoiceTermOfSale'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceReasonForExport: WideString;
begin
  Result := ChildNodes['InvoiceReasonForExport'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceReasonForExport(Value: WideString);
begin
  ChildNodes['InvoiceReasonForExport'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceDeclarationStatement: WideString;
begin
  Result := ChildNodes['InvoiceDeclarationStatement'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceDeclarationStatement(Value: WideString);
begin
  ChildNodes['InvoiceDeclarationStatement'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceAdditionalComments: WideString;
begin
  Result := ChildNodes['InvoiceAdditionalComments'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceAdditionalComments(Value: WideString);
begin
  ChildNodes['InvoiceAdditionalComments'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceCurrencyCode: WideString;
begin
  Result := ChildNodes['InvoiceCurrencyCode'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceCurrencyCode(Value: WideString);
begin
  ChildNodes['InvoiceCurrencyCode'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceDiscount: WideString;
begin
  Result := ChildNodes['InvoiceDiscount'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceDiscount(Value: WideString);
begin
  ChildNodes['InvoiceDiscount'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceChargesFreight: WideString;
begin
  Result := ChildNodes['InvoiceChargesFreight'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceChargesFreight(Value: WideString);
begin
  ChildNodes['InvoiceChargesFreight'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceChargesInsurance: WideString;
begin
  Result := ChildNodes['InvoiceChargesInsurance'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceChargesInsurance(Value: WideString);
begin
  ChildNodes['InvoiceChargesInsurance'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceChargesOther: WideString;
begin
  Result := ChildNodes['InvoiceChargesOther'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceChargesOther(Value: WideString);
begin
  ChildNodes['InvoiceChargesOther'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_InvoiceLineTotal: WideString;
begin
  Result := ChildNodes['InvoiceLineTotal'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_InvoiceLineTotal(Value: WideString);
begin
  ChildNodes['InvoiceLineTotal'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_CustomsValueTotal: WideString;
begin
  Result := ChildNodes['CustomsValueTotal'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_CustomsValueTotal(Value: WideString);
begin
  ChildNodes['CustomsValueTotal'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_CustomsValueCurrencyCode: WideString;
begin
  Result := ChildNodes['CustomsValueCurrencyCode'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_CustomsValueCurrencyCode(Value: WideString);
begin
  ChildNodes['CustomsValueCurrencyCode'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_CustomsValueOrigin: WideString;
begin
  Result := ChildNodes['CustomsValueOrigin'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_CustomsValueOrigin(Value: WideString);
begin
  ChildNodes['CustomsValueOrigin'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDCode: WideString;
begin
  Result := ChildNodes['SED-Code'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDCode(Value: WideString);
begin
  ChildNodes['SED-Code'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDPartiesToTransaction: WideString;
begin
  Result := ChildNodes['SED-PartiesToTransaction'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDPartiesToTransaction(Value: WideString);
begin
  ChildNodes['SED-PartiesToTransaction'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDUltimateConsigneeSameAsShipTo: WideString;
begin
  Result := ChildNodes['SED-UltimateConsigneeSameAsShipTo'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDUltimateConsigneeSameAsShipTo(Value: WideString);
begin
  ChildNodes['SED-UltimateConsigneeSameAsShipTo'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDCountryTerritoryOfUltimateDestination: WideString;
begin
  Result := ChildNodes['SED-CountryTerritoryOfUltimateDestination'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDCountryTerritoryOfUltimateDestination(Value: WideString);
begin
  ChildNodes['SED-CountryTerritoryOfUltimateDestination'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDAESTransactionNumber: WideString;
begin
  Result := ChildNodes['SED-AESTransactionNumber'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDAESTransactionNumber(Value: WideString);
begin
  ChildNodes['SED-AESTransactionNumber'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDAESTransactionNumberType: WideString;
begin
  Result := ChildNodes['SED-AESTransactionNumberType'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDAESTransactionNumberType(Value: WideString);
begin
  ChildNodes['SED-AESTransactionNumberType'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDRoutedTransaction: WideString;
begin
  Result := ChildNodes['SED-RoutedTransaction'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDRoutedTransaction(Value: WideString);
begin
  ChildNodes['SED-RoutedTransaction'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDCurrencyConversion: WideString;
begin
  Result := ChildNodes['SED-CurrencyConversion'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDCurrencyConversion(Value: WideString);
begin
  ChildNodes['SED-CurrencyConversion'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_SEDExportCode: WideString;
begin
  Result := ChildNodes['SED-ExportCode'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_SEDExportCode(Value: WideString);
begin
  ChildNodes['SED-ExportCode'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_COUPSPrepareCO: WideString;
begin
  Result := ChildNodes['CO-UPSPrepare-CO'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_COUPSPrepareCO(Value: WideString);
begin
  ChildNodes['CO-UPSPrepare-CO'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_COOwnerOrAgent: WideString;
begin
  Result := ChildNodes['CO-OwnerOrAgent'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_COOwnerOrAgent(Value: WideString);
begin
  ChildNodes['CO-OwnerOrAgent'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_NAFTAProducer: WideString;
begin
  Result := ChildNodes['NAFTA-Producer'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_NAFTAProducer(Value: WideString);
begin
  ChildNodes['NAFTA-Producer'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_NAFTABlanketPeriodStart: WideString;
begin
  Result := ChildNodes['NAFTA-BlanketPeriodStart'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_NAFTABlanketPeriodStart(Value: WideString);
begin
  ChildNodes['NAFTA-BlanketPeriodStart'].NodeValue := Value;
end;

function TXMLInternationalDocumentationType.Get_NAFTABlanketPeriodEnd: WideString;
begin
  Result := ChildNodes['NAFTA-BlanketPeriodEnd'].Text;
end;

procedure TXMLInternationalDocumentationType.Set_NAFTABlanketPeriodEnd(Value: WideString);
begin
  ChildNodes['NAFTA-BlanketPeriodEnd'].NodeValue := Value;
end;

{ TXMLGoodsType }

function TXMLGoodsType.Get_PartNumber: WideString;
begin
  Result := ChildNodes['PartNumber'].Text;
end;

procedure TXMLGoodsType.Set_PartNumber(Value: WideString);
begin
  ChildNodes['PartNumber'].NodeValue := Value;
end;

function TXMLGoodsType.Get_DescriptionOfGood: WideString;
begin
  Result := ChildNodes['DescriptionOfGood'].Text;
end;

procedure TXMLGoodsType.Set_DescriptionOfGood(Value: WideString);
begin
  ChildNodes['DescriptionOfGood'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvNAFTATariffCode: WideString;
begin
  Result := ChildNodes['Inv-NAFTA-TariffCode'].Text;
end;

procedure TXMLGoodsType.Set_InvNAFTATariffCode(Value: WideString);
begin
  ChildNodes['Inv-NAFTA-TariffCode'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvNAFTACOCountryTerritoryOfOrigin: WideString;
begin
  Result := ChildNodes['Inv-NAFTA-CO-CountryTerritoryOfOrigin'].Text;
end;

procedure TXMLGoodsType.Set_InvNAFTACOCountryTerritoryOfOrigin(Value: WideString);
begin
  ChildNodes['Inv-NAFTA-CO-CountryTerritoryOfOrigin'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceUnits: WideString;
begin
  Result := ChildNodes['InvoiceUnits'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceUnits(Value: WideString);
begin
  ChildNodes['InvoiceUnits'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceUnitOfMeasure: WideString;
begin
  Result := ChildNodes['InvoiceUnitOfMeasure'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceUnitOfMeasure(Value: WideString);
begin
  ChildNodes['InvoiceUnitOfMeasure'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceSEDUnitPrice: WideString;
begin
  Result := ChildNodes['Invoice-SED-UnitPrice'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceSEDUnitPrice(Value: WideString);
begin
  ChildNodes['Invoice-SED-UnitPrice'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceCurrencyCode: WideString;
begin
  Result := ChildNodes['InvoiceCurrencyCode'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceCurrencyCode(Value: WideString);
begin
  ChildNodes['InvoiceCurrencyCode'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceNetWeight: WideString;
begin
  Result := ChildNodes['InvoiceNetWeight'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceNetWeight(Value: WideString);
begin
  ChildNodes['InvoiceNetWeight'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDCOGrossWeight: WideString;
begin
  Result := ChildNodes['SED-CO-GrossWeight'].Text;
end;

procedure TXMLGoodsType.Set_SEDCOGrossWeight(Value: WideString);
begin
  ChildNodes['SED-CO-GrossWeight'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDLbsOrKgs: WideString;
begin
  Result := ChildNodes['SED-LbsOrKgs'].Text;
end;

procedure TXMLGoodsType.Set_SEDLbsOrKgs(Value: WideString);
begin
  ChildNodes['SED-LbsOrKgs'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDPrintGoodOnSED: WideString;
begin
  Result := ChildNodes['SED-PrintGoodOn-SED'].Text;
end;

procedure TXMLGoodsType.Set_SEDPrintGoodOnSED(Value: WideString);
begin
  ChildNodes['SED-PrintGoodOn-SED'].NodeValue := Value;
end;

function TXMLGoodsType.Get_InvoiceSEDCOMarksAndNumbers: WideString;
begin
  Result := ChildNodes['Invoice-SED-CO-MarksAndNumbers'].Text;
end;

procedure TXMLGoodsType.Set_InvoiceSEDCOMarksAndNumbers(Value: WideString);
begin
  ChildNodes['Invoice-SED-CO-MarksAndNumbers'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDScheduleBNumber: WideString;
begin
  Result := ChildNodes['SED-ScheduleBNumber'].Text;
end;

procedure TXMLGoodsType.Set_SEDScheduleBNumber(Value: WideString);
begin
  ChildNodes['SED-ScheduleBNumber'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDScheduleBUnits1: WideString;
begin
  Result := ChildNodes['SED-ScheduleBUnits1'].Text;
end;

procedure TXMLGoodsType.Set_SEDScheduleBUnits1(Value: WideString);
begin
  ChildNodes['SED-ScheduleBUnits1'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDScheduleBUnits2: WideString;
begin
  Result := ChildNodes['SED-ScheduleBUnits2'].Text;
end;

procedure TXMLGoodsType.Set_SEDScheduleBUnits2(Value: WideString);
begin
  ChildNodes['SED-ScheduleBUnits2'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDUnitsOfMeasure1: WideString;
begin
  Result := ChildNodes['SED-UnitsOfMeasure1'].Text;
end;

procedure TXMLGoodsType.Set_SEDUnitsOfMeasure1(Value: WideString);
begin
  ChildNodes['SED-UnitsOfMeasure1'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDUnitsOfMeasure2: WideString;
begin
  Result := ChildNodes['SED-UnitsOfMeasure2'].Text;
end;

procedure TXMLGoodsType.Set_SEDUnitsOfMeasure2(Value: WideString);
begin
  ChildNodes['SED-UnitsOfMeasure2'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDECCN: WideString;
begin
  Result := ChildNodes['SED-ECCN'].Text;
end;

procedure TXMLGoodsType.Set_SEDECCN(Value: WideString);
begin
  ChildNodes['SED-ECCN'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDException: WideString;
begin
  Result := ChildNodes['SED-Exception'].Text;
end;

procedure TXMLGoodsType.Set_SEDException(Value: WideString);
begin
  ChildNodes['SED-Exception'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDLicenceNumber: WideString;
begin
  Result := ChildNodes['SED-LicenceNumber'].Text;
end;

procedure TXMLGoodsType.Set_SEDLicenceNumber(Value: WideString);
begin
  ChildNodes['SED-LicenceNumber'].NodeValue := Value;
end;

function TXMLGoodsType.Get_SEDLicenceNumberExpirationDate: WideString;
begin
  Result := ChildNodes['SED-LicenceNumberExpirationDate'].Text;
end;

procedure TXMLGoodsType.Set_SEDLicenceNumberExpirationDate(Value: WideString);
begin
  ChildNodes['SED-LicenceNumberExpirationDate'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTAPrintGoodOnNAFTA: WideString;
begin
  Result := ChildNodes['NAFTA-PrintGoodOn-NAFTA'].Text;
end;

procedure TXMLGoodsType.Set_NAFTAPrintGoodOnNAFTA(Value: WideString);
begin
  ChildNodes['NAFTA-PrintGoodOn-NAFTA'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTAPreferenceCriterion: WideString;
begin
  Result := ChildNodes['NAFTA-PreferenceCriterion'].Text;
end;

procedure TXMLGoodsType.Set_NAFTAPreferenceCriterion(Value: WideString);
begin
  ChildNodes['NAFTA-PreferenceCriterion'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTAProducer: WideString;
begin
  Result := ChildNodes['NAFTA-Producer'].Text;
end;

procedure TXMLGoodsType.Set_NAFTAProducer(Value: WideString);
begin
  ChildNodes['NAFTA-Producer'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTAMultipleCountriesTerritoriesOfOrigin: WideString;
begin
  Result := ChildNodes['NAFTA-MultipleCountriesTerritoriesOfOrigin'].Text;
end;

procedure TXMLGoodsType.Set_NAFTAMultipleCountriesTerritoriesOfOrigin(Value: WideString);
begin
  ChildNodes['NAFTA-MultipleCountriesTerritoriesOfOrigin'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTANetCostMethod: WideString;
begin
  Result := ChildNodes['NAFTA-NetCostMethod'].Text;
end;

procedure TXMLGoodsType.Set_NAFTANetCostMethod(Value: WideString);
begin
  ChildNodes['NAFTA-NetCostMethod'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTANetCostPeriodStartDate: WideString;
begin
  Result := ChildNodes['NAFTA-NetCostPeriodStartDate'].Text;
end;

procedure TXMLGoodsType.Set_NAFTANetCostPeriodStartDate(Value: WideString);
begin
  ChildNodes['NAFTA-NetCostPeriodStartDate'].NodeValue := Value;
end;

function TXMLGoodsType.Get_NAFTANetCostPeriodEndDate: WideString;
begin
  Result := ChildNodes['NAFTA-NetCostPeriodEndDate'].Text;
end;

procedure TXMLGoodsType.Set_NAFTANetCostPeriodEndDate(Value: WideString);
begin
  ChildNodes['NAFTA-NetCostPeriodEndDate'].NodeValue := Value;
end;

function TXMLGoodsType.Get_COPrintGoodOnCO: WideString;
begin
  Result := ChildNodes['CO-PrintGoodOn-CO'].Text;
end;

procedure TXMLGoodsType.Set_COPrintGoodOnCO(Value: WideString);
begin
  ChildNodes['CO-PrintGoodOn-CO'].NodeValue := Value;
end;

function TXMLGoodsType.Get_CONumberOfPackageContainingGood: WideString;
begin
  Result := ChildNodes['CO-NumberOfPackageContainingGood'].Text;
end;

procedure TXMLGoodsType.Set_CONumberOfPackageContainingGood(Value: WideString);
begin
  ChildNodes['CO-NumberOfPackageContainingGood'].NodeValue := Value;
end;

{ TXMLGoodsTypeList }

function TXMLGoodsTypeList.Add: IXMLGoodsType;
begin
  Result := AddItem(-1) as IXMLGoodsType;
end;

function TXMLGoodsTypeList.Insert(const Index: Integer): IXMLGoodsType;
begin
  Result := AddItem(Index) as IXMLGoodsType;
end;
function TXMLGoodsTypeList.Get_Item(Index: Integer): IXMLGoodsType;
begin
  Result := List[Index] as IXMLGoodsType;
end;

{ TXMLProcessMessageType }

procedure TXMLProcessMessageType.AfterConstruction;
begin
  RegisterChildNode('Error', TXMLErrorType);
  RegisterChildNode('ShipmentRates', TXMLShipmentRatesType);
  RegisterChildNode('TrackingNumbers', TXMLTrackingNumbersType);
  inherited;
end;

function TXMLProcessMessageType.Get_Error: IXMLErrorType;
begin
  Result := ChildNodes['Error'] as IXMLErrorType;
end;

function TXMLProcessMessageType.Get_ShipmentRates: IXMLShipmentRatesType;
begin
  Result := ChildNodes['ShipmentRates'] as IXMLShipmentRatesType;
end;

function TXMLProcessMessageType.Get_TrackingNumbers: IXMLTrackingNumbersType;
begin
  Result := ChildNodes['TrackingNumbers'] as IXMLTrackingNumbersType;
end;

function TXMLProcessMessageType.Get_ImportID: WideString;
begin
  Result := ChildNodes['ImportID'].Text;
end;

procedure TXMLProcessMessageType.Set_ImportID(Value: WideString);
begin
  ChildNodes['ImportID'].NodeValue := Value;
end;

function TXMLProcessMessageType.Get_Reference1: WideString;
begin
  Result := ChildNodes['Reference1'].Text;
end;

procedure TXMLProcessMessageType.Set_Reference1(Value: WideString);
begin
  ChildNodes['Reference1'].NodeValue := Value;
end;

function TXMLProcessMessageType.Get_Reference2: WideString;
begin
  Result := ChildNodes['Reference2'].Text;
end;

procedure TXMLProcessMessageType.Set_Reference2(Value: WideString);
begin
  ChildNodes['Reference2'].NodeValue := Value;
end;

{ TXMLProcessMessageTypeList }

function TXMLProcessMessageTypeList.Add: IXMLProcessMessageType;
begin
  Result := AddItem(-1) as IXMLProcessMessageType;
end;

function TXMLProcessMessageTypeList.Insert(const Index: Integer): IXMLProcessMessageType;
begin
  Result := AddItem(Index) as IXMLProcessMessageType;
end;
function TXMLProcessMessageTypeList.Get_Item(Index: Integer): IXMLProcessMessageType;
begin
  Result := List[Index] as IXMLProcessMessageType;
end;

{ TXMLErrorType }

function TXMLErrorType.Get_ErrorMessage: WideString;
begin
  Result := ChildNodes['ErrorMessage'].Text;
end;

procedure TXMLErrorType.Set_ErrorMessage(Value: WideString);
begin
  ChildNodes['ErrorMessage'].NodeValue := Value;
end;

{ TXMLShipmentRatesType }

procedure TXMLShipmentRatesType.AfterConstruction;
begin
  RegisterChildNode('ShipmentCharges', TXMLShipmentChargesType);
  RegisterChildNode('ShipperCharges', TXMLShipperChargesType);
  RegisterChildNode('ReceiverCharges', TXMLReceiverChargesType);
  RegisterChildNode('ShipmentandHandlingCharges', TXMLShipmentandHandlingChargesType);
  RegisterChildNode('Declared_Value', TXMLDeclared_ValueType);
  RegisterChildNode('C_O_D', TXMLC_O_DType);
  RegisterChildNode('Return_Services', TXMLReturn_ServicesType);
  RegisterChildNode('Saturday_Delivery', TXMLSaturday_DeliveryType);
  RegisterChildNode('Saturday_Pickup', TXMLSaturday_PickupType);
  RegisterChildNode('Proactive_Response', TXMLProactive_ResponseType);
  RegisterChildNode('Delivery_Confirmation', TXMLDelivery_ConfirmationType);
  RegisterChildNode('Delivery_AreaSurcharge', TXMLDelivery_AreaSurchargeType);
  RegisterChildNode('PackageRates', TXMLPackageRatesType);
  inherited;
end;

function TXMLShipmentRatesType.Get_ShipmentCharges: IXMLShipmentChargesType;
begin
  Result := ChildNodes['ShipmentCharges'] as IXMLShipmentChargesType;
end;

function TXMLShipmentRatesType.Get_ShipperCharges: IXMLShipperChargesType;
begin
  Result := ChildNodes['ShipperCharges'] as IXMLShipperChargesType;
end;

function TXMLShipmentRatesType.Get_ReceiverCharges: IXMLReceiverChargesType;
begin
  Result := ChildNodes['ReceiverCharges'] as IXMLReceiverChargesType;
end;

function TXMLShipmentRatesType.Get_ShipmentandHandlingCharges: IXMLShipmentandHandlingChargesType;
begin
  Result := ChildNodes['ShipmentandHandlingCharges'] as IXMLShipmentandHandlingChargesType;
end;

function TXMLShipmentRatesType.Get_Declared_Value: IXMLDeclared_ValueType;
begin
  Result := ChildNodes['Declared_Value'] as IXMLDeclared_ValueType;
end;

function TXMLShipmentRatesType.Get_C_O_D: IXMLC_O_DType;
begin
  Result := ChildNodes['C_O_D'] as IXMLC_O_DType;
end;

function TXMLShipmentRatesType.Get_Return_Services: IXMLReturn_ServicesType;
begin
  Result := ChildNodes['Return_Services'] as IXMLReturn_ServicesType;
end;

function TXMLShipmentRatesType.Get_Saturday_Delivery: IXMLSaturday_DeliveryType;
begin
  Result := ChildNodes['Saturday_Delivery'] as IXMLSaturday_DeliveryType;
end;

function TXMLShipmentRatesType.Get_Saturday_Pickup: IXMLSaturday_PickupType;
begin
  Result := ChildNodes['Saturday_Pickup'] as IXMLSaturday_PickupType;
end;

function TXMLShipmentRatesType.Get_Proactive_Response: IXMLProactive_ResponseType;
begin
  Result := ChildNodes['Proactive_Response'] as IXMLProactive_ResponseType;
end;

function TXMLShipmentRatesType.Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
begin
  Result := ChildNodes['Delivery_Confirmation'] as IXMLDelivery_ConfirmationType;
end;

function TXMLShipmentRatesType.Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
begin
  Result := ChildNodes['Delivery_AreaSurcharge'] as IXMLDelivery_AreaSurchargeType;
end;

function TXMLShipmentRatesType.Get_PackageRates: IXMLPackageRatesType;
begin
  Result := ChildNodes['PackageRates'] as IXMLPackageRatesType;
end;

{ TXMLShipmentChargesType }

procedure TXMLShipmentChargesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLShipmentChargesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLRateType }

function TXMLRateType.Get_Reference: WideString;
begin
  Result := ChildNodes['Reference'].Text;
end;

procedure TXMLRateType.Set_Reference(Value: WideString);
begin
  ChildNodes['Reference'].NodeValue := Value;
end;

function TXMLRateType.Get_Published_: WideString;
begin
  Result := ChildNodes['Published'].Text;
end;

procedure TXMLRateType.Set_Published_(Value: WideString);
begin
  ChildNodes['Published'].NodeValue := Value;
end;

function TXMLRateType.Get_CCC: WideString;
begin
  Result := ChildNodes['CCC'].Text;
end;

procedure TXMLRateType.Set_CCC(Value: WideString);
begin
  ChildNodes['CCC'].NodeValue := Value;
end;

function TXMLRateType.Get_Negotiated: WideString;
begin
  Result := ChildNodes['Negotiated'].Text;
end;

procedure TXMLRateType.Set_Negotiated(Value: WideString);
begin
  ChildNodes['Negotiated'].NodeValue := Value;
end;

{ TXMLShipperChargesType }

procedure TXMLShipperChargesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLShipperChargesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLReceiverChargesType }

procedure TXMLReceiverChargesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLReceiverChargesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLShipmentandHandlingChargesType }

procedure TXMLShipmentandHandlingChargesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLShipmentandHandlingChargesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLDeclared_ValueType }

procedure TXMLDeclared_ValueType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLDeclared_ValueType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLC_O_DType }

procedure TXMLC_O_DType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLC_O_DType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLReturn_ServicesType }

procedure TXMLReturn_ServicesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLReturn_ServicesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLSaturday_DeliveryType }

procedure TXMLSaturday_DeliveryType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLSaturday_DeliveryType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLSaturday_PickupType }

procedure TXMLSaturday_PickupType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLSaturday_PickupType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLProactive_ResponseType }

procedure TXMLProactive_ResponseType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLProactive_ResponseType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLDelivery_ConfirmationType }

procedure TXMLDelivery_ConfirmationType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLDelivery_ConfirmationType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLDelivery_AreaSurchargeType }

procedure TXMLDelivery_AreaSurchargeType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLDelivery_AreaSurchargeType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLPackageRatesType }

procedure TXMLPackageRatesType.AfterConstruction;
begin
  RegisterChildNode('PackageRate', TXMLPackageRateType);
  ItemTag := 'PackageRate';
  ItemInterface := IXMLPackageRateType;
  inherited;
end;

function TXMLPackageRatesType.Get_PackageRate(Index: Integer): IXMLPackageRateType;
begin
  Result := List[Index] as IXMLPackageRateType;
end;

function TXMLPackageRatesType.Add: IXMLPackageRateType;
begin
  Result := AddItem(-1) as IXMLPackageRateType;
end;

function TXMLPackageRatesType.Insert(const Index: Integer): IXMLPackageRateType;
begin
  Result := AddItem(Index) as IXMLPackageRateType;
end;

{ TXMLPackageRateType }

procedure TXMLPackageRateType.AfterConstruction;
begin
  RegisterChildNode('PackageCharges', TXMLPackageChargesType);
  RegisterChildNode('Additional_Handling', TXMLAdditional_HandlingType);
  RegisterChildNode('C_O_D', TXMLC_O_DType);
  RegisterChildNode('Delivery_Confirmation', TXMLDelivery_ConfirmationType);
  RegisterChildNode('Verbal_Confirmation', TXMLVerbal_ConfirmationType);
  RegisterChildNode('Declared_Value', TXMLDeclared_ValueType);
  RegisterChildNode('QVN', TXMLQVNType);
  RegisterChildNode('Proactive_Response', TXMLProactive_ResponseType);
  RegisterChildNode('Dry_Ice', TXMLDry_IceType);
  RegisterChildNode('Delivery_AreaSurcharge', TXMLDelivery_AreaSurchargeType);
  inherited;
end;

function TXMLPackageRateType.Get_TrackingNumber: WideString;
begin
  Result := ChildNodes['TrackingNumber'].Text;
end;

procedure TXMLPackageRateType.Set_TrackingNumber(Value: WideString);
begin
  ChildNodes['TrackingNumber'].NodeValue := Value;
end;

function TXMLPackageRateType.Get_USPSNumber: WideString;
begin
  Result := ChildNodes['USPSNumber'].Text;
end;

procedure TXMLPackageRateType.Set_USPSNumber(Value: WideString);
begin
  ChildNodes['USPSNumber'].NodeValue := Value;
end;

function TXMLPackageRateType.Get_PackageCharges: IXMLPackageChargesType;
begin
  Result := ChildNodes['PackageCharges'] as IXMLPackageChargesType;
end;

function TXMLPackageRateType.Get_Additional_Handling: IXMLAdditional_HandlingType;
begin
  Result := ChildNodes['Additional_Handling'] as IXMLAdditional_HandlingType;
end;

function TXMLPackageRateType.Get_C_O_D: IXMLC_O_DType;
begin
  Result := ChildNodes['C_O_D'] as IXMLC_O_DType;
end;

function TXMLPackageRateType.Get_Delivery_Confirmation: IXMLDelivery_ConfirmationType;
begin
  Result := ChildNodes['Delivery_Confirmation'] as IXMLDelivery_ConfirmationType;
end;

function TXMLPackageRateType.Get_Verbal_Confirmation: IXMLVerbal_ConfirmationType;
begin
  Result := ChildNodes['Verbal_Confirmation'] as IXMLVerbal_ConfirmationType;
end;

function TXMLPackageRateType.Get_Declared_Value: IXMLDeclared_ValueType;
begin
  Result := ChildNodes['Declared_Value'] as IXMLDeclared_ValueType;
end;

function TXMLPackageRateType.Get_QVN: IXMLQVNType;
begin
  Result := ChildNodes['QVN'] as IXMLQVNType;
end;

function TXMLPackageRateType.Get_Proactive_Response: IXMLProactive_ResponseType;
begin
  Result := ChildNodes['Proactive_Response'] as IXMLProactive_ResponseType;
end;

function TXMLPackageRateType.Get_Dry_Ice: IXMLDry_IceType;
begin
  Result := ChildNodes['Dry_Ice'] as IXMLDry_IceType;
end;

function TXMLPackageRateType.Get_Delivery_AreaSurcharge: IXMLDelivery_AreaSurchargeType;
begin
  Result := ChildNodes['Delivery_AreaSurcharge'] as IXMLDelivery_AreaSurchargeType;
end;

{ TXMLPackageChargesType }

procedure TXMLPackageChargesType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLPackageChargesType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLAdditional_HandlingType }

procedure TXMLAdditional_HandlingType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLAdditional_HandlingType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLVerbal_ConfirmationType }

procedure TXMLVerbal_ConfirmationType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLVerbal_ConfirmationType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLQVNType }

procedure TXMLQVNType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLQVNType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLDry_IceType }

procedure TXMLDry_IceType.AfterConstruction;
begin
  RegisterChildNode('Rate', TXMLRateType);
  inherited;
end;

function TXMLDry_IceType.Get_Rate: IXMLRateType;
begin
  Result := ChildNodes['Rate'] as IXMLRateType;
end;

{ TXMLTrackingNumbersType }

procedure TXMLTrackingNumbersType.AfterConstruction;
begin
  ItemTag := 'TrackingNumber';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTrackingNumbersType.Get_TrackingNumber(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLTrackingNumbersType.Add(const TrackingNumber: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := TrackingNumber;
end;

function TXMLTrackingNumbersType.Insert(const Index: Integer; const TrackingNumber: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := TrackingNumber;
end;

end.