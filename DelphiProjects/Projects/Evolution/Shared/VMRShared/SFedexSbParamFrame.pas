// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SFedexSbParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, EvUIComponents, isUIwwDBEdit;

type
  TAccountedSbParamFrame = class (TVmrOptionEditFrame)
    evDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
  end;

implementation

uses EvUtils;

{$R *.dfm}

initialization

end.
