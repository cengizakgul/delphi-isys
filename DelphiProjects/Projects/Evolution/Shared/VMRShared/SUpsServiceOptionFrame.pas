// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SUpsServiceOptionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, wwdblook, ExtCtrls, DBCtrls,SFedexSbParamFrame,
  Wwdotdot, Wwdbcomb, EvUIUtils, EvUIComponents;

type
  TUpsServiceOptionFrame = class (TAccountedSbParamFrame)
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evButton1: TevButton;
    evButton2: TevButton;
    evButton3: TevButton;
    evDBRadioGroup1: TevDBRadioGroup;
    edDHLReport: TevDBLookupCombo;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBEdit5: TevDBEdit;
    evLabel6: TevLabel;
    evDBRadioGroup3: TevDBRadioGroup;
    cbExportType: TevDBComboBox;
    Label1: TLabel;
    procedure evButton1Click(Sender: TObject);
    procedure evButton2Click(Sender: TObject);
    procedure evButton3Click(Sender: TObject);
  end;

implementation

uses EvUtils,sServiceUps;

{$R *.dfm}

procedure TUpsServiceOptionFrame.evButton1Click(Sender: TObject);
begin
  inherited;
   if dsMain.DataSet.State in [dsEdit, dsInsert] then
    dsMain.DataSet.UpdateRecord;
  TVmrUpsService(FOptionedTypeObject).ApiTestCredentials;
  EvMessage('Credentials are verified');
end;

procedure TUpsServiceOptionFrame.evButton2Click(Sender: TObject);
var
  SP:TShipmentParams;
begin
  inherited;
  if dsMain.DataSet.State in [dsEdit, dsInsert] then
      dsMain.DataSet.UpdateRecord;
  SP := TVmrUpsService(FOptionedTypeObject).ApiSendDummyPackage;
//  EvMessage('Package is sent. Tacking # is '+
//      SP.UpsShipmentConfirmResponse.ShipmentIdentificationNumber);
  

end;

procedure TUpsServiceOptionFrame.evButton3Click(Sender: TObject);
begin
  inherited;
  dsMain.DataSet.CheckBrowseMode;
  EvMessage(TVmrUpsService(FOptionedTypeObject).ApiGenerateCertificationXML);
end;

end.
