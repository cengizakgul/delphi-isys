// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrEmailEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  StdCtrls, Mask,
  wwdbedit, SVmrClasses, ISBasicClasses, evExceptions, EvUIComponents;

type
  TVmrEmailEditFrame = class(TVmrOptionEditFrame)
    evDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    evDBEdit4: TevDBEdit;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.dfm}

uses
  SPackageEntry;

procedure TVmrEmailEditFrame.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  if (not Handled) and (Kind = NavCommit) and (
    (Trim(dsMain.DataSet.FieldByName('FROM_ADDRESS').AsString) = '')
    //or    (Trim(dsMain.DataSet.FieldByName('SMTP_SERVER').AsString) = '')
    ) then
  begin
    raise EevException.Create(RequiredErrorString);
    Handled := True;
  end;
end;

initialization
  RegisterVmrClass(TVmrEmailEditFrame);
end.
