inherited VmrEmailEditFrame: TVmrEmailEditFrame
  Width = 424
  object evLabel1: TevLabel [0]
    Left = 16
    Top = 152
    Width = 89
    Height = 13
    Caption = '~SMTP server:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object evLabel2: TevLabel [1]
    Left = 16
    Top = 8
    Width = 89
    Height = 13
    Caption = '~FROM address:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel3: TevLabel [2]
    Left = 16
    Top = 56
    Width = 54
    Height = 13
    Caption = 'User name:'
    Visible = False
  end
  object evLabel4: TevLabel [3]
    Left = 16
    Top = 104
    Width = 49
    Height = 13
    Caption = 'Password:'
    Visible = False
  end
  object evDBEdit1: TevDBEdit [4]
    Left = 16
    Top = 168
    Width = 297
    Height = 21
    DataField = 'SMTP_SERVER'
    DataSource = dsMain
    TabOrder = 0
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  object evDBEdit2: TevDBEdit [5]
    Left = 16
    Top = 24
    Width = 297
    Height = 21
    DataField = 'FROM_ADDRESS'
    DataSource = dsMain
    TabOrder = 1
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
  end
  object evDBEdit3: TevDBEdit [6]
    Left = 16
    Top = 72
    Width = 297
    Height = 21
    DataField = 'USER_NAME'
    DataSource = dsMain
    TabOrder = 2
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  object evDBEdit4: TevDBEdit [7]
    Left = 16
    Top = 120
    Width = 297
    Height = 21
    DataField = 'PASSWORD'
    DataSource = dsMain
    PasswordChar = '*'
    TabOrder = 3
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  inherited dsMain: TevDataSource
    Left = 56
    Top = 80
  end
end
