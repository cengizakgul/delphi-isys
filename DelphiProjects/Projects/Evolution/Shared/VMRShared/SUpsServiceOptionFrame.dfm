inherited UpsServiceOptionFrame: TUpsServiceOptionFrame
  Width = 538
  Height = 467
  object evLabel2: TevLabel [1]
    Left = 16
    Top = 232
    Width = 46
    Height = 13
    Caption = 'API Login'
    Visible = False
  end
  object evLabel3: TevLabel [2]
    Left = 16
    Top = 256
    Width = 66
    Height = 13
    Caption = 'API Password'
    Visible = False
  end
  object evLabel4: TevLabel [3]
    Left = 16
    Top = 280
    Width = 82
    Height = 13
    Caption = 'API Shipping Key'
    Visible = False
  end
  object evLabel5: TevLabel [4]
    Left = 16
    Top = 308
    Width = 100
    Height = 13
    Caption = 'Report for UPS label:'
    Visible = False
  end
  object evLabel6: TevLabel [5]
    Left = 8
    Top = 39
    Width = 102
    Height = 26
    Caption = 'WorldShip XML Auto Import Folder'
    WordWrap = True
  end
  object Label1: TLabel [6]
    Left = 17
    Top = 69
    Width = 95
    Height = 13
    Caption = 'Export to WS when:'
    Visible = False
  end
  object evDBEdit2: TevDBEdit [8]
    Left = 128
    Top = 232
    Width = 160
    Height = 21
    DataField = 'API_LOGIN'
    DataSource = dsMain
    TabOrder = 1
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  object evDBEdit3: TevDBEdit [9]
    Left = 128
    Top = 256
    Width = 160
    Height = 21
    DataField = 'API_PASSWORD'
    DataSource = dsMain
    TabOrder = 2
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  object evDBEdit4: TevDBEdit [10]
    Left = 128
    Top = 280
    Width = 353
    Height = 21
    DataField = 'API_KEY'
    DataSource = dsMain
    TabOrder = 3
    UnboundDataType = wwDefault
    UsePictureMask = False
    Visible = False
    WantReturns = False
    WordWrap = False
  end
  object evButton1: TevButton [11]
    Left = 24
    Top = 200
    Width = 177
    Height = 25
    Caption = 'Test API access and credentials'
    TabOrder = 4
    Visible = False
    OnClick = evButton1Click
  end
  object evButton2: TevButton [12]
    Left = 16
    Top = 136
    Width = 201
    Height = 25
    Caption = 'Send Test Packages'
    TabOrder = 5
    OnClick = evButton2Click
  end
  object evButton3: TevButton [13]
    Left = 16
    Top = 168
    Width = 201
    Height = 25
    Caption = 'Generate certification XML packets'
    TabOrder = 6
    Visible = False
    OnClick = evButton3Click
  end
  object evDBRadioGroup1: TevDBRadioGroup [14]
    Left = 280
    Top = 96
    Width = 201
    Height = 49
    Caption = 'API is live and billable'
    Columns = 3
    DataField = 'API_LIVE'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'Testing'
      'No')
    TabOrder = 7
    Values.Strings = (
      'Y'
      'T'
      'N')
    Visible = False
  end
  object edDHLReport: TevDBLookupCombo [15]
    Left = 128
    Top = 304
    Width = 353
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION'#9'F')
    DataField = 'UPS_LABEL_REPORT_NBR'
    DataSource = dsMain
    LookupField = 'NBR'
    Style = csDropDownList
    TabOrder = 8
    Visible = False
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object evDBRadioGroup2: TevDBRadioGroup [16]
    Left = 280
    Top = 152
    Width = 201
    Height = 41
    Caption = 'Save API Files'
    Columns = 2
    DataField = 'UPS_SAVE_API_FILES'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 9
    Values.Strings = (
      'Y'
      'N')
    Visible = False
  end
  object evDBEdit5: TevDBEdit [17]
    Left = 128
    Top = 40
    Width = 353
    Height = 21
    DataField = 'UPS_WS_PATH'
    DataSource = dsMain
    TabOrder = 10
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
  end
  object evDBRadioGroup3: TevDBRadioGroup [18]
    Left = 16
    Top = 96
    Width = 185
    Height = 33
    Caption = 'Use WorldShip XML Auto Import'
    Columns = 2
    DataField = 'UPS_WS_LIVE'
    DataSource = dsMain
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 11
    Values.Strings = (
      'Y'
      'N')
  end
  object cbExportType: TevDBComboBox [19]
    Left = 128
    Top = 64
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'WHEN_EXPORT'
    DataSource = dsMain
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Release'#9'R'
      'Pickup'#9'P')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 12
    UnboundDataType = wwDefault
    Visible = False
  end
  inherited dsMain: TevDataSource
    Left = 392
  end
end
