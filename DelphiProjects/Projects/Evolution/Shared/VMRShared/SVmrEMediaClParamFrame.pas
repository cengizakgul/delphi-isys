// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrEMediaClParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVmrOptionEditFrame, DB, Wwdatsrc,  SVmrClasses,
  StdCtrls, Mask, wwdbedit, ISBasicClasses, DBCtrls, EvUIComponents,
  isUIwwDBEdit;

type
  TVmrEMediaClParamFrame= class(TVmrOptionEditFrame)
    edPrintPassword: TevDBEdit;
    lPrintPassword: TevLabel;
    evDBCheckBox1: TevDBCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
