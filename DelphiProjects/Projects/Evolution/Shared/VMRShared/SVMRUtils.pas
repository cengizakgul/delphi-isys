unit SVMRUtils;

interface
uses sysUtils, isBaseClasses, SFieldCodeValues, EvStreamUtils, Classes;

function GetRegularCheckList :IisStringList;
function GetMiscCheckList    :IisStringList;
function GetEventDate (sDate : string) : TDateTime;
function IsXLSFormat(aData: IevDualStream): boolean;

implementation

function GetRegularCheckList : IisStringList;
var i : char;
begin
  Result := TisStringList.Create;
  for i := Low(RegularCheckReportNbrs) to High(RegularCheckReportNbrs) do
   Result.Add(IntTostr(RegularCheckReportNbrs[i]));
end;

function GetMiscCheckList : IisStringList;
var i : char;
begin
  Result := TisStringList.Create;
  for i := Low(MiscCheckReportNbrs) to High(MiscCheckReportNbrs) do
   Result.Add(IntTostr(MiscCheckReportNbrs[i]));
end;

function GetEventDate (sDate : string) : TDateTime;
  var iMonth, iDay, iYear : word;
begin
  Result := 0;
  if Length(sDate) < 8 then exit;
  iYear  := StrToIntDef(Copy(sDate,1,4),0);
  iMonth := StrToIntDef(Copy(sDate,5,2),0);
  iDay   := StrToIntDef(Copy(sDate,7,2),0);
  Result := EncodeDate(iYear,iMonth,iDay);
end;

function IsXLSFormat(aData: IevDualStream): boolean;
var
  s, s1: String;
  p: Integer;
  xlsSignature: String;
begin
  xlsSignature := 'D0CF11E0A1B11AE1';
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', 8);
    s1 := StringOfChar(' ', 8);
    HexToBin(PChar(xlsSignature), PChar(s1), 8);
    AData.Read(s[1], 8);
    Result := AnsiSameStr(s, s1);
  finally
    AData.Position := p;
  end;
end;

end.
