// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrOptionEditFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Wwdatsrc,  ISBasicClasses, EvDataAccessComponents, EvUIComponents;

resourcestring
  RequiredErrorString =
    'Not all required fields have been assigned a value.'+#13#10+
    'Please fill in all required fields before proceeding.';

type
  TVmrOptionEditFrameEvent = procedure of object;
  TVmrOptionEditFrame = class(TFrame)
    dsMain: TevDataSource;
    procedure dsMainStateChange(Sender: TObject);
  private
    FMD: TDataSource;
    { Private declarations }
  protected
    FOptionedTypeObject: TObject;
  public
    { Public declarations }
    property MasterDS :TDataSource read FMD;
    constructor CreateBound(const AOwner: TComponent; const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource); virtual;
    destructor Destroy; override;
    procedure CancelChanges;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); virtual;
  end;
  TVmrOptionEditFrameClass = class of TVmrOptionEditFrame;

implementation

{$R *.dfm}

{ TVmrOptionEditFrame }

procedure TVmrOptionEditFrame.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  // Placeholder.
  // RDD: I don't like the Handled := False code here at all so I have chosen to
  // ommit it.
end;

procedure TVmrOptionEditFrame.CancelChanges;
begin
  Assert(Assigned(dsMain.DataSet));
  if dsMain.DataSet.State in [dsEdit, dsInsert] then
    dsMain.DataSet.Cancel;
  TEvBasicClientDataSet(dsMain.DataSet).CancelUpdates;
end;

constructor TVmrOptionEditFrame.CreateBound(const AOwner: TComponent;
  const cd: TDataSet; const OptionedTypeObject: TObject; const MasterDataSource: TDataSource);
begin
  inherited Create(AOwner);
  dsMain.DataSet := cd;
  FMD := MasterDataSource;
  FOptionedTypeObject := OptionedTypeObject;
end;

destructor TVmrOptionEditFrame.Destroy;
begin
  inherited;
end;

procedure TVmrOptionEditFrame.dsMainStateChange(Sender: TObject);
begin
  if dsMain.State in [dsEdit, dsInsert] then
    if Assigned(FMD) then
      FMD.Edit;
end;

end.
