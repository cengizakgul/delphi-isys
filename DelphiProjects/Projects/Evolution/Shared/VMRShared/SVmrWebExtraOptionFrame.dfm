inherited VMRWebExtraOptionFrame: TVMRWebExtraOptionFrame
  Width = 312
  Height = 154
  object evlabNotes: TevLabel [0]
    Left = 8
    Top = 8
    Width = 227
    Height = 13
    Caption = 'Text to be included with Emails (256 chars max.)'
  end
  object EvDBMemo1: TEvDBMemo [1]
    Left = 8
    Top = 24
    Width = 289
    Height = 89
    DataField = 'WEB_NOTES'
    DataSource = dsMain
    MaxLength = 256
    TabOrder = 0
  end
  inherited dsMain: TevDataSource
    Left = 56
    Top = 0
  end
end
