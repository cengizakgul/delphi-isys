
{***************************************************************************************************************}
{                                                                                                               }
{                                               XML Data Binding                                                }
{                                                                                                               }
{         Generated on: 6/8/2009 7:15:49 PM                                                                     }
{       Generated from: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipacceptresp1.xml   }
{   Settings stored in: C:\Release\tickets\44589\TestXML\My Documents\Current Doc Project\shipacceptresp1.xdb   }
{                                                                                                               }
{***************************************************************************************************************}

unit sUpsShipAcceptResp;

interface

uses xmldom, XMLDoc, XMLIntf,EvStreamUtils,EvUtils;

type

{ Forward Decls }

  IXMLUpsShipmentAcceptResponseType = interface;
  IXMLResponseType = interface;
  IXMLTransactionReferenceType = interface;
  IXMLShipmentResultsType = interface;
  IXMLShipmentChargesType = interface;
  IXMLTransportationChargesType = interface;
  IXMLServiceOptionsChargesType = interface;
  IXMLTotalChargesType = interface;
  IXMLBillingWeightType = interface;
  IXMLUnitOfMeasurementType = interface;
  IXMLPackageResultsType = interface;
  IXMLPackageResultsTypeList = interface;
  IXMLLabelImageType = interface;
  IXMLLabelImageFormatType = interface;

{ IXMLUpsShipmentAcceptResponseType }

  IXMLUpsShipmentAcceptResponseType = interface(IXMLNode)
    ['{3901CCEA-86C3-42C4-9A21-B11FEB3B8AFF}']
    { Property Accessors }
    function Get_Response: IXMLResponseType;
    function Get_ShipmentResults: IXMLShipmentResultsType;
    { Methods & Properties }
    property Response: IXMLResponseType read Get_Response;
    property ShipmentResults: IXMLShipmentResultsType read Get_ShipmentResults;
  end;

{ IXMLResponseType }

  IXMLResponseType = interface(IXMLNode)
    ['{1B7E468D-73F2-477D-A177-D31B267A5E1E}']
    { Property Accessors }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
    { Methods & Properties }
    property TransactionReference: IXMLTransactionReferenceType read Get_TransactionReference;
    property ResponseStatusCode: Integer read Get_ResponseStatusCode write Set_ResponseStatusCode;
    property ResponseStatusDescription: WideString read Get_ResponseStatusDescription write Set_ResponseStatusDescription;
  end;

{ IXMLTransactionReferenceType }

  IXMLTransactionReferenceType = interface(IXMLNode)
    ['{5B1A41BC-CDE5-4E3C-82C0-F51BF5FB2901}']
    { Property Accessors }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
    { Methods & Properties }
    property CustomerContext: WideString read Get_CustomerContext write Set_CustomerContext;
    property XpciVersion: WideString read Get_XpciVersion write Set_XpciVersion;
  end;

{ IXMLShipmentResultsType }

  IXMLShipmentResultsType = interface(IXMLNode)
    ['{7D5C5179-AFA8-4A54-AC85-92D698BB0F6F}']
    { Property Accessors }
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_BillingWeight: IXMLBillingWeightType;
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_PackageResults: IXMLPackageResultsTypeList;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
    { Methods & Properties }
    property ShipmentCharges: IXMLShipmentChargesType read Get_ShipmentCharges;
    property BillingWeight: IXMLBillingWeightType read Get_BillingWeight;
    property ShipmentIdentificationNumber: WideString read Get_ShipmentIdentificationNumber write Set_ShipmentIdentificationNumber;
    property PackageResults: IXMLPackageResultsTypeList read Get_PackageResults;
  end;

{ IXMLShipmentChargesType }

  IXMLShipmentChargesType = interface(IXMLNode)
    ['{C0F9132B-9966-4711-9675-3AEFC453F2A3}']
    { Property Accessors }
    function Get_TransportationCharges: IXMLTransportationChargesType;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_TotalCharges: IXMLTotalChargesType;
    { Methods & Properties }
    property TransportationCharges: IXMLTransportationChargesType read Get_TransportationCharges;
    property ServiceOptionsCharges: IXMLServiceOptionsChargesType read Get_ServiceOptionsCharges;
    property TotalCharges: IXMLTotalChargesType read Get_TotalCharges;
  end;

{ IXMLTransportationChargesType }

  IXMLTransportationChargesType = interface(IXMLNode)
    ['{FB59FA88-7D4E-4B24-9DDA-A5D7104BB9EC}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLServiceOptionsChargesType }

  IXMLServiceOptionsChargesType = interface(IXMLNode)
    ['{4AA63AFF-C20C-4300-B525-4BFB5734380B}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLTotalChargesType }

  IXMLTotalChargesType = interface(IXMLNode)
    ['{28F32118-1460-44FC-A6D0-E9B41A8B0DF4}']
    { Property Accessors }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
    { Methods & Properties }
    property CurrencyCode: WideString read Get_CurrencyCode write Set_CurrencyCode;
    property MonetaryValue: WideString read Get_MonetaryValue write Set_MonetaryValue;
  end;

{ IXMLBillingWeightType }

  IXMLBillingWeightType = interface(IXMLNode)
    ['{94470E50-0E8A-4EE1-AC50-0518D27EBAFE}']
    { Property Accessors }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: WideString;
    procedure Set_Weight(Value: WideString);
    { Methods & Properties }
    property UnitOfMeasurement: IXMLUnitOfMeasurementType read Get_UnitOfMeasurement;
    property Weight: WideString read Get_Weight write Set_Weight;
  end;

{ IXMLUnitOfMeasurementType }

  IXMLUnitOfMeasurementType = interface(IXMLNode)
    ['{427DC6E2-53D1-45FE-B79D-284CA8EFE343}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ IXMLPackageResultsType }

  IXMLPackageResultsType = interface(IXMLNode)
    ['{7DFAAD9E-73DA-4607-B079-5AA3FE318867}']
    { Property Accessors }
    function Get_TrackingNumber: WideString;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_LabelImage: IXMLLabelImageType;
    procedure Set_TrackingNumber(Value: WideString);
    { Methods & Properties }
    property TrackingNumber: WideString read Get_TrackingNumber write Set_TrackingNumber;
    property ServiceOptionsCharges: IXMLServiceOptionsChargesType read Get_ServiceOptionsCharges;
    property LabelImage: IXMLLabelImageType read Get_LabelImage;
  end;

{ IXMLPackageResultsTypeList }

  IXMLPackageResultsTypeList = interface(IXMLNodeCollection)
    ['{34B7404B-AEEB-4E47-A4DC-CFCF389F4102}']
    { Methods & Properties }
    function Add: IXMLPackageResultsType;
    function Insert(const Index: Integer): IXMLPackageResultsType;
    function Get_Item(Index: Integer): IXMLPackageResultsType;
    property Items[Index: Integer]: IXMLPackageResultsType read Get_Item; default;
  end;

{ IXMLLabelImageType }

  IXMLLabelImageType = interface(IXMLNode)
    ['{83BA7EA8-3BBE-4E77-A1A9-DAE1CCB0A481}']
    { Property Accessors }
    function Get_LabelImageFormat: IXMLLabelImageFormatType;
    function Get_GraphicImage: IEvDualStream;
    { Methods & Properties }
    property LabelImageFormat: IXMLLabelImageFormatType read Get_LabelImageFormat;
    property GraphicImage: IEvDualStream read Get_GraphicImage;
  end;

{ IXMLLabelImageFormatType }

  IXMLLabelImageFormatType = interface(IXMLNode)
    ['{22BB4716-E063-416E-B378-C01A9B729D5A}']
    { Property Accessors }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
    { Methods & Properties }
    property Code: WideString read Get_Code write Set_Code;
  end;

{ Forward Decls }

  TXMLShipmentAcceptResponseType = class;
  TXMLResponseType = class;
  TXMLTransactionReferenceType = class;
  TXMLShipmentResultsType = class;
  TXMLShipmentChargesType = class;
  TXMLTransportationChargesType = class;
  TXMLServiceOptionsChargesType = class;
  TXMLTotalChargesType = class;
  TXMLBillingWeightType = class;
  TXMLUnitOfMeasurementType = class;
  TXMLPackageResultsType = class;
  TXMLPackageResultsTypeList = class;
  TXMLLabelImageType = class;
  TXMLLabelImageFormatType = class;

{ TXMLShipmentAcceptResponseType }

  TXMLShipmentAcceptResponseType = class(TXMLNode, IXMLUpsShipmentAcceptResponseType)
  protected
    { IXMLUpsShipmentAcceptResponseType }
    function Get_Response: IXMLResponseType;
    function Get_ShipmentResults: IXMLShipmentResultsType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLResponseType }

  TXMLResponseType = class(TXMLNode, IXMLResponseType)
  protected
    { IXMLResponseType }
    function Get_TransactionReference: IXMLTransactionReferenceType;
    function Get_ResponseStatusCode: Integer;
    function Get_ResponseStatusDescription: WideString;
    procedure Set_ResponseStatusCode(Value: Integer);
    procedure Set_ResponseStatusDescription(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransactionReferenceType }

  TXMLTransactionReferenceType = class(TXMLNode, IXMLTransactionReferenceType)
  protected
    { IXMLTransactionReferenceType }
    function Get_CustomerContext: WideString;
    function Get_XpciVersion: WideString;
    procedure Set_CustomerContext(Value: WideString);
    procedure Set_XpciVersion(Value: WideString);
  end;

{ TXMLShipmentResultsType }

  TXMLShipmentResultsType = class(TXMLNode, IXMLShipmentResultsType)
  private
    FPackageResults: IXMLPackageResultsTypeList;
  protected
    { IXMLShipmentResultsType }
    function Get_ShipmentCharges: IXMLShipmentChargesType;
    function Get_BillingWeight: IXMLBillingWeightType;
    function Get_ShipmentIdentificationNumber: WideString;
    function Get_PackageResults: IXMLPackageResultsTypeList;
    procedure Set_ShipmentIdentificationNumber(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLShipmentChargesType }

  TXMLShipmentChargesType = class(TXMLNode, IXMLShipmentChargesType)
  protected
    { IXMLShipmentChargesType }
    function Get_TransportationCharges: IXMLTransportationChargesType;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_TotalCharges: IXMLTotalChargesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransportationChargesType }

  TXMLTransportationChargesType = class(TXMLNode, IXMLTransportationChargesType)
  protected
    { IXMLTransportationChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLServiceOptionsChargesType }

  TXMLServiceOptionsChargesType = class(TXMLNode, IXMLServiceOptionsChargesType)
  protected
    { IXMLServiceOptionsChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLTotalChargesType }

  TXMLTotalChargesType = class(TXMLNode, IXMLTotalChargesType)
  protected
    { IXMLTotalChargesType }
    function Get_CurrencyCode: WideString;
    function Get_MonetaryValue: WideString;
    procedure Set_CurrencyCode(Value: WideString);
    procedure Set_MonetaryValue(Value: WideString);
  end;

{ TXMLBillingWeightType }

  TXMLBillingWeightType = class(TXMLNode, IXMLBillingWeightType)
  protected
    { IXMLBillingWeightType }
    function Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
    function Get_Weight: WideString;
    procedure Set_Weight(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUnitOfMeasurementType }

  TXMLUnitOfMeasurementType = class(TXMLNode, IXMLUnitOfMeasurementType)
  protected
    { IXMLUnitOfMeasurementType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;

{ TXMLPackageResultsType }

  TXMLPackageResultsType = class(TXMLNode, IXMLPackageResultsType)
  protected
    { IXMLPackageResultsType }
    function Get_TrackingNumber: WideString;
    function Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
    function Get_LabelImage: IXMLLabelImageType;
    procedure Set_TrackingNumber(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPackageResultsTypeList }

  TXMLPackageResultsTypeList = class(TXMLNodeCollection, IXMLPackageResultsTypeList)
  protected
    { IXMLPackageResultsTypeList }
    function Add: IXMLPackageResultsType;
    function Insert(const Index: Integer): IXMLPackageResultsType;
    function Get_Item(Index: Integer): IXMLPackageResultsType;
  end;

{ TXMLLabelImageType }

  TXMLLabelImageType = class(TXMLNode, IXMLLabelImageType)
  protected
    { IXMLLabelImageType }
    function Get_LabelImageFormat: IXMLLabelImageFormatType;
    function Get_GraphicImage: IEvDualStream;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLLabelImageFormatType }

  TXMLLabelImageFormatType = class(TXMLNode, IXMLLabelImageFormatType)
  protected
    { IXMLLabelImageFormatType }
    function Get_Code: WideString;
    procedure Set_Code(Value: WideString);
  end;

{ Global Functions }

function GetUpsShipmentAcceptResponse(Doc: IXMLDocument): IXMLUpsShipmentAcceptResponseType;
function LoadUpsShipmentAcceptResponse(const FileName: WideString): IXMLUpsShipmentAcceptResponseType;
function NewUpsShipmentAcceptResponse: IXMLUpsShipmentAcceptResponseType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetUpsShipmentAcceptResponse(Doc: IXMLDocument): IXMLUpsShipmentAcceptResponseType;
begin
  Result := Doc.GetDocBinding('ShipmentAcceptResponse', TXMLShipmentAcceptResponseType, TargetNamespace) as IXMLUpsShipmentAcceptResponseType;
end;

function LoadUpsShipmentAcceptResponse(const FileName: WideString): IXMLUpsShipmentAcceptResponseType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('ShipmentAcceptResponse', TXMLShipmentAcceptResponseType, TargetNamespace) as IXMLUpsShipmentAcceptResponseType;
end;

function NewUpsShipmentAcceptResponse: IXMLUpsShipmentAcceptResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('ShipmentAcceptResponse', TXMLShipmentAcceptResponseType, TargetNamespace) as IXMLUpsShipmentAcceptResponseType;
end;

{ TXMLShipmentAcceptResponseType }

procedure TXMLShipmentAcceptResponseType.AfterConstruction;
begin
  RegisterChildNode('Response', TXMLResponseType);
  RegisterChildNode('ShipmentResults', TXMLShipmentResultsType);
  inherited;
end;

function TXMLShipmentAcceptResponseType.Get_Response: IXMLResponseType;
begin
  Result := ChildNodes['Response'] as IXMLResponseType;
end;

function TXMLShipmentAcceptResponseType.Get_ShipmentResults: IXMLShipmentResultsType;
begin
  Result := ChildNodes['ShipmentResults'] as IXMLShipmentResultsType;
end;

{ TXMLResponseType }

procedure TXMLResponseType.AfterConstruction;
begin
  RegisterChildNode('TransactionReference', TXMLTransactionReferenceType);
  inherited;
end;

function TXMLResponseType.Get_TransactionReference: IXMLTransactionReferenceType;
begin
  Result := ChildNodes['TransactionReference'] as IXMLTransactionReferenceType;
end;

function TXMLResponseType.Get_ResponseStatusCode: Integer;
begin
  Result := ChildNodes['ResponseStatusCode'].NodeValue;
end;

procedure TXMLResponseType.Set_ResponseStatusCode(Value: Integer);
begin
  ChildNodes['ResponseStatusCode'].NodeValue := Value;
end;

function TXMLResponseType.Get_ResponseStatusDescription: WideString;
begin
  Result := ChildNodes['ResponseStatusDescription'].Text;
end;

procedure TXMLResponseType.Set_ResponseStatusDescription(Value: WideString);
begin
  ChildNodes['ResponseStatusDescription'].NodeValue := Value;
end;

{ TXMLTransactionReferenceType }

function TXMLTransactionReferenceType.Get_CustomerContext: WideString;
begin
  Result := ChildNodes['CustomerContext'].Text;
end;

procedure TXMLTransactionReferenceType.Set_CustomerContext(Value: WideString);
begin
  ChildNodes['CustomerContext'].NodeValue := Value;
end;

function TXMLTransactionReferenceType.Get_XpciVersion: WideString;
begin
  Result := ChildNodes['XpciVersion'].Text;
end;

procedure TXMLTransactionReferenceType.Set_XpciVersion(Value: WideString);
begin
  ChildNodes['XpciVersion'].NodeValue := Value;
end;

{ TXMLShipmentResultsType }

procedure TXMLShipmentResultsType.AfterConstruction;
begin
  RegisterChildNode('ShipmentCharges', TXMLShipmentChargesType);
  RegisterChildNode('BillingWeight', TXMLBillingWeightType);
  RegisterChildNode('PackageResults', TXMLPackageResultsType);
  FPackageResults := CreateCollection(TXMLPackageResultsTypeList, IXMLPackageResultsType, 'PackageResults') as IXMLPackageResultsTypeList;
  inherited;
end;

function TXMLShipmentResultsType.Get_ShipmentCharges: IXMLShipmentChargesType;
begin
  Result := ChildNodes['ShipmentCharges'] as IXMLShipmentChargesType;
end;

function TXMLShipmentResultsType.Get_BillingWeight: IXMLBillingWeightType;
begin
  Result := ChildNodes['BillingWeight'] as IXMLBillingWeightType;
end;

function TXMLShipmentResultsType.Get_ShipmentIdentificationNumber: WideString;
begin
  Result := ChildNodes['ShipmentIdentificationNumber'].Text;
end;

procedure TXMLShipmentResultsType.Set_ShipmentIdentificationNumber(Value: WideString);
begin
  ChildNodes['ShipmentIdentificationNumber'].NodeValue := Value;
end;

function TXMLShipmentResultsType.Get_PackageResults: IXMLPackageResultsTypeList;
begin
  Result := FPackageResults;
end;

{ TXMLShipmentChargesType }

procedure TXMLShipmentChargesType.AfterConstruction;
begin
  RegisterChildNode('TransportationCharges', TXMLTransportationChargesType);
  RegisterChildNode('ServiceOptionsCharges', TXMLServiceOptionsChargesType);
  RegisterChildNode('TotalCharges', TXMLTotalChargesType);
  inherited;
end;

function TXMLShipmentChargesType.Get_TransportationCharges: IXMLTransportationChargesType;
begin
  Result := ChildNodes['TransportationCharges'] as IXMLTransportationChargesType;
end;

function TXMLShipmentChargesType.Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
begin
  Result := ChildNodes['ServiceOptionsCharges'] as IXMLServiceOptionsChargesType;
end;

function TXMLShipmentChargesType.Get_TotalCharges: IXMLTotalChargesType;
begin
  Result := ChildNodes['TotalCharges'] as IXMLTotalChargesType;
end;

{ TXMLTransportationChargesType }

function TXMLTransportationChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLTransportationChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLTransportationChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLTransportationChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLServiceOptionsChargesType }

function TXMLServiceOptionsChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLServiceOptionsChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLServiceOptionsChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLServiceOptionsChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLTotalChargesType }

function TXMLTotalChargesType.Get_CurrencyCode: WideString;
begin
  Result := ChildNodes['CurrencyCode'].Text;
end;

procedure TXMLTotalChargesType.Set_CurrencyCode(Value: WideString);
begin
  ChildNodes['CurrencyCode'].NodeValue := Value;
end;

function TXMLTotalChargesType.Get_MonetaryValue: WideString;
begin
  Result := ChildNodes['MonetaryValue'].Text;
end;

procedure TXMLTotalChargesType.Set_MonetaryValue(Value: WideString);
begin
  ChildNodes['MonetaryValue'].NodeValue := Value;
end;

{ TXMLBillingWeightType }

procedure TXMLBillingWeightType.AfterConstruction;
begin
  RegisterChildNode('UnitOfMeasurement', TXMLUnitOfMeasurementType);
  inherited;
end;

function TXMLBillingWeightType.Get_UnitOfMeasurement: IXMLUnitOfMeasurementType;
begin
  Result := ChildNodes['UnitOfMeasurement'] as IXMLUnitOfMeasurementType;
end;

function TXMLBillingWeightType.Get_Weight: WideString;
begin
  Result := ChildNodes['Weight'].Text;
end;

procedure TXMLBillingWeightType.Set_Weight(Value: WideString);
begin
  ChildNodes['Weight'].NodeValue := Value;
end;

{ TXMLUnitOfMeasurementType }

function TXMLUnitOfMeasurementType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLUnitOfMeasurementType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

{ TXMLPackageResultsType }

procedure TXMLPackageResultsType.AfterConstruction;
begin
  RegisterChildNode('ServiceOptionsCharges', TXMLServiceOptionsChargesType);
  RegisterChildNode('LabelImage', TXMLLabelImageType);
  inherited;
end;

function TXMLPackageResultsType.Get_TrackingNumber: WideString;
begin
  Result := ChildNodes['TrackingNumber'].Text;
end;

procedure TXMLPackageResultsType.Set_TrackingNumber(Value: WideString);
begin
  ChildNodes['TrackingNumber'].NodeValue := Value;
end;

function TXMLPackageResultsType.Get_ServiceOptionsCharges: IXMLServiceOptionsChargesType;
begin
  Result := ChildNodes['ServiceOptionsCharges'] as IXMLServiceOptionsChargesType;
end;

function TXMLPackageResultsType.Get_LabelImage: IXMLLabelImageType;
begin
  Result := ChildNodes['LabelImage'] as IXMLLabelImageType;
end;

{ TXMLPackageResultsTypeList }

function TXMLPackageResultsTypeList.Add: IXMLPackageResultsType;
begin
  Result := AddItem(-1) as IXMLPackageResultsType;
end;

function TXMLPackageResultsTypeList.Insert(const Index: Integer): IXMLPackageResultsType;
begin
  Result := AddItem(Index) as IXMLPackageResultsType;
end;
function TXMLPackageResultsTypeList.Get_Item(Index: Integer): IXMLPackageResultsType;
begin
  Result := List[Index] as IXMLPackageResultsType;
end;

{ TXMLLabelImageType }

procedure TXMLLabelImageType.AfterConstruction;
begin
  RegisterChildNode('LabelImageFormat', TXMLLabelImageFormatType);
  inherited;
end;

function TXMLLabelImageType.Get_LabelImageFormat: IXMLLabelImageFormatType;
begin
  Result := ChildNodes['LabelImageFormat'] as IXMLLabelImageFormatType;
end;

function TXMLLabelImageType.Get_GraphicImage: IEvDualStream;
var s :string;
begin
  s := ChildNodes['GraphicImage'].Text;
  Result := TEvDualStreamHolder.CreateInMemory;
  if s <> '' then
  try
    DecodeToStream('$MIME$'+s, Result.RealStream);
  finally
  end;

end;

{ TXMLLabelImageFormatType }

function TXMLLabelImageFormatType.Get_Code: WideString;
begin
  Result := ChildNodes['Code'].Text;
end;

procedure TXMLLabelImageFormatType.Set_Code(Value: WideString);
begin
  ChildNodes['Code'].NodeValue := Value;
end;

end.