unit EvStatisticsViewerFrm;

interface

uses Forms, SysUtils, StdCtrls, Controls, ExtCtrls, Classes, ComCtrls, isBasicUtils, isBaseClasses,
     isStatistics, Mask, wwdbedit, Wwdotdot, Wwdbcomb, ISBasicClasses,
      TeEngine, Series, TeeProcs, Chart, Graphics, EvUIComponents,
  isUIwwDBComboBox;

type
  TevStatisticsViewerFrm = class(TFrame)
    pnlFileList: TPanel;
    Label1: TLabel;
    cbDates: TevDBComboBox;
    tvEvents: TTreeView;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Label2: TLabel;
    pnlChart: TPanel;
    mEventInfo: TMemo;
    Chart: TChart;
    Series1: TPieSeries;
    lvSummary: TListView;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Label3: TLabel;
    procedure cbDatesChange(Sender: TObject);
    procedure tvEventsChange(Sender: TObject; Node: TTreeNode);
  private
    procedure ShowEventInfo(const AEvent: IisStatEvent);
    procedure BuildChart(const AEvent: IisStatEvent);
    procedure BuildSummary(const AEvent: IisStatEvent);
  public
    procedure InitForFiles;
    procedure InitForEvent(const ATopEvent: IisStatEvent);
  end;

implementation

uses Variants;

{$R *.DFM}

type
  TEventNode = class(TTreeNode)
  private
    FEvent: IisStatEvent;
  end;


{ TevStatisticsViewerFrm }

procedure TevStatisticsViewerFrm.InitForFiles;
var
  Files: IisStringList;
  i: Integer;
  s, sApp: String;
begin
  if cbDates.Items.Count = 0 then
  begin
    Files := GetFilesByMask(AppDir + 'Statistics\*_*.ips');

    cbDates.Clear;
    for i := 0 to Files.Count - 1 do
    begin
      s := ChangeFileExt(ExtractFileName(Files[i]), '');
      sApp := GetNextStrValue(s, '_');
      s := sApp + ' ' + Copy(s, 1, 2) + '/' + Copy(s, 3, 2) + '/' + Copy(s, 5, 2);
      cbDates.Items.Add(s + #9 + Files[i]);
    end;
  end;

  pnlFileList.Visible := True;
end;

procedure TevStatisticsViewerFrm.cbDatesChange(Sender: TObject);
var
  i: Integer;
  TopItem: TTreeNode;
  Item: TEventNode;
  Evt: IisStatEvent;
  FFile: IisStatisticsFile;  

  procedure AddEvents(const AParent: TEventNode);
  var
    i: Integer;
    N: TEventNode;
  begin
    for i := 0 to AParent.FEvent.EventsCount - 1 do
    begin
      N := TEventNode.Create(tvEvents.Items);
      N.FEvent := AParent.FEvent.Events[i];
      tvEvents.Items.AddNode(N, AParent, N.FEvent.Name + ' ' + IntToStr(N.FEvent.Duration), nil, naAddChild);
      AddEvents(N);
    end;
  end;

begin
  FFile := TisStatisticsFile.Create(cbDates.Value, True);

  tvEvents.Items.BeginUpdate;
  try
    tvEvents.Items.Clear;
    TopItem := tvEvents.Items.AddChild(nil, cbDates.Text);
    TopItem.Data := Pointer(-1);

    for i := 0 to FFile.Count - 1 do
    begin
      Evt := FFile.Events[i];
      Item := TEventNode.Create(tvEvents.Items);
      Item.FEvent := FFile.Events[i];
      tvEvents.Items.AddNode(Item, TopItem, Evt.Name, nil, naAddChild);

      AddEvents(Item);
    end;

    TopItem.Expand(False);
  finally
    tvEvents.Items.EndUpdate;
  end;
end;

procedure TevStatisticsViewerFrm.tvEventsChange(Sender: TObject; Node: TTreeNode);
begin
  if Node is TEventNode then
    ShowEventInfo(TEventNode(Node).FEvent)
  else
    ShowEventInfo(nil);
end;

procedure TevStatisticsViewerFrm.ShowEventInfo(const AEvent: IisStatEvent);
var
  s: String;
  i: Integer;
  C: IisNamedValue;
begin
  mEventInfo.Text := '';

  if Assigned(AEvent) then
  begin
    mEventInfo.Lines.Add('Event name:   ' + AEvent.Name);
    mEventInfo.Lines.Add('Begin time:   ' + FormatDateTime('hh:nn:ss AM/PM', AEvent.BeginTime));
    mEventInfo.Lines.Add('Duration:   ' + IntToStr(AEvent.Duration) + ' ms');

    for i := 0 to AEvent.Properties.Count - 1 do
    begin
      C := AEvent.Properties[i];
      s := C.Name + ':   ' + VarToStr(C.Value);
      mEventInfo.Lines.Add(s);
    end;
  end;

  mEventInfo.SelStart := 0;
  mEventInfo.SelLength := 0;

  BuildChart(AEvent);
  BuildSummary(AEvent);
end;

procedure TevStatisticsViewerFrm.BuildChart(const AEvent: IisStatEvent);
var
  Series: TChartSeries;
  Stat: IisListOfValues;
  i: Integer;
  Cl: TColor;

  procedure AddChildren(const AEvent: IisStatEvent);
  var
    i: Integer;
    ChildrenTime, CalcTime: Cardinal;
    sProp: String;
  begin
    if AEvent.EventsCount > 0 then
    begin
      ChildrenTime := 0;
      for i := 0 to AEvent.EventsCount - 1 do
        Inc(ChildrenTime, AEvent.Events[i].Duration);
      CalcTime := AEvent.Duration - ChildrenTime;

      if StartsWith(AEvent.Name, 'Remote call') then
        sProp := 'Transport'
      else
        sProp := AEvent.Name + ' (Other)';

      for i := 0 to AEvent.EventsCount - 1 do
        AddChildren(AEvent.Events[i]);
    end

    else
    begin
      sProp := AEvent.Name;
      CalcTime := AEvent.Duration;
    end;

    if CalcTime > 0 then
      if Stat.ValueExists(sProp) then
        Stat.Value[sProp] := Stat.Value[sProp] + CalcTime
      else
        Stat.AddValue(sProp, CalcTime);
  end;

begin
  Series := Chart.SeriesList[0];
  Series.Clear;

  if Assigned(AEvent) then
  begin
    Stat := TisListOfValues.Create;
    AddChildren(AEvent);

    for i := 0 to Stat.Count - 1 do
    begin
      if StartsWith(Stat.Values[i].Name, 'DB.') then
        Cl := clGreen
      else if StartsWith(Stat.Values[i].Name, 'EvoRP') then
        Cl := clRed
      else if StartsWith(Stat.Values[i].Name, 'EvoRB') then
        Cl := clRed
      else if AnsiSameText(Stat.Values[i].Name, 'Transport') then
        Cl := clBlue
      else if AnsiSameText(Stat.Values[i].Name, 'Remote call') then
        Cl := clRed
      else if FinishesWith(Stat.Values[i].Name, ' (Other)') then
        Cl := clWhite
      else
        Cl := clTeeColor;

      Series.Add(Stat.Values[i].Value, Stat.Values[i].Name, Cl);
    end;
  end;
end;

procedure TevStatisticsViewerFrm.BuildSummary(const AEvent: IisStatEvent);
var
  Stat: IisListOfValues;
  i: Integer;
  lvItem: TListItem;

  procedure Analyze(const AEvent: IisStatEvent);
  var
    i: Integer;
  begin
    if StartsWith(AEvent.Name, 'DB.Query') then
      Stat.Value['Queries'] := Stat.Value['Queries'] + 1

    else if StartsWith(AEvent.Name, 'Remote call') then
    begin
      Stat.Value['Remote calls'] := Stat.Value['Remote calls'] + 1;
      Stat.Value['Sent data'] := Stat.Value['Sent data'] + AEvent.Properties.TryGetValue('Sent bytes', 0);
      Stat.Value['Received data'] := Stat.Value['Received data'] + AEvent.Properties.TryGetValue('Received bytes', 0);
      Stat.Value['Send time'] := Stat.Value['Send time'] + AEvent.Properties.TryGetValue('Send time', 0);
      Stat.Value['Receive time'] := Stat.Value['Receive time'] + AEvent.Properties.TryGetValue('Receive time', 0);
      Stat.Value['Server time'] := Stat.Value['Server time'] + AEvent.Duration;
    end;

    for i := 0 to AEvent.EventsCount - 1 do
      Analyze(AEvent.Events[i]);
  end;

begin
  lvSummary.Clear;

  if Assigned(AEvent) then
  begin
    Stat := TisListOfValues.Create;
    Stat.AddValue('Remote calls', 0);
    Stat.AddValue('Server time', 0);
    Stat.AddValue('Send time', 0);
    Stat.AddValue('Receive time', 0);
    Stat.AddValue('Sent data', 0);
    Stat.AddValue('Received data', 0);
    Stat.AddValue('Queries', 0);

    Analyze(AEvent);

    if Stat.Value['Sent data'] > 0 then
      Stat.Value['Sent data'] := SizeToString(Stat.Value['Sent data']);
    if Stat.Value['Received data'] > 0 then
      Stat.Value['Received data'] := SizeToString(Stat.Value['Received data']);
    if Stat.Value['Server time'] > 0 then
      Stat.Value['Server time'] := PeriodToString(Stat.Value['Server time']);


    for i := 0 to Stat.Count - 1 do
      if Stat.Values[i].Value <> '0' then
      begin
        lvItem := lvSummary.Items.Add;
        lvItem.Caption := Stat.Values[i].Name;
        lvItem.SubItems.Add(Stat.Values[i].Value);
      end;
  end;
end;

procedure TevStatisticsViewerFrm.InitForEvent(const ATopEvent: IisStatEvent);
var
  TopItem: TEventNode;

  function AddEvent(const AParent: TEventNode; const AEvent: IisStatEvent): TEventNode;
  begin
    Result := TEventNode.Create(tvEvents.Items);
    Result.FEvent := AEvent;
    tvEvents.Items.AddNode(Result, AParent, Result.FEvent.Name  + ' ' + IntToStr(Result.FEvent.Duration), nil, naAddChild);
  end;

  procedure AddEvents(const AParent: TEventNode);
  var
    i: Integer;
    N: TEventNode;
  begin
    for i := 0 to AParent.FEvent.EventsCount - 1 do
    begin
      N := AddEvent(AParent, AParent.FEvent.Events[i]);
      AddEvents(N);
    end;
  end;

begin
  pnlFileList.Visible := False;

  tvEvents.Items.BeginUpdate;
  try
    tvEvents.Items.Clear;
    if Assigned(ATopEvent) then
    begin
      TopItem := AddEvent(nil, ATopEvent);
      AddEvents(TopItem);
      TopItem.Expand(False);
    end;
  finally
    tvEvents.Items.EndUpdate;
  end;
end;

end.
