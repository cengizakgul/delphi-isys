// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCompanyCopyWizard;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SCopyWizard, Db,   SDataStructure,
  Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, StdCtrls, Buttons,
  ExtCtrls, wwdbdatetimepicker, Mask, wwdbedit, Wwdbspin, DBCtrls,
  EvUtils, wwIntl, Wwdotdot, Wwdbcomb, wwdblook, SFieldCodeValues,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, isVCLBugFix, Variants, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBDateTimePicker,
  isUIwwDBEdit, isUIEdit, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  SDataDictbureau;

type
  TCompanyCopyWizard = class(TCopyWizard)
    tshCO: TTabSheet;
    grCO: TevDBGrid;
    dsCO: TevDataSource;
    dsCO_ADDITIONAL_INFO_NAMES: TevDataSource;
    lblCOxxCUSTOM_COMPANY_NUMBER: TevLabel;
    COxxCUSTOM_COMPANY_NUMBER: TevDBEdit;
    lblCOxxNAME: TevLabel;
    COxxNAME: TevDBEdit;
    lblCOxxADDRESS1: TevLabel;
    COxxADDRESS1: TevDBEdit;
    lblCOxxADDRESS2: TevLabel;
    COxxADDRESS2: TevDBEdit;
    lblCOxxCITY: TevLabel;
    COxxCITY: TevDBEdit;
    lblCOxxSTATE: TevLabel;
    COxxSTATE: TevDBEdit;
    lblCOxxZIP_CODE: TevLabel;
    COxxZIP_CODE: TevDBEdit;
    lblCOxxSTART_DATE: TevLabel;
    COxxSTART_DATE: TevDBDateTimePicker;
    lblCOxxLEGAL_NAME: TevLabel;
    lblCOxxLEGAL_ADDRESS1: TevLabel;
    lblCOxxLEGAL_ADDRESS2: TevLabel;
    lblCOxxLEGAL_CITY: TevLabel;
    lblCOxxLEGAL_STATE: TevLabel;
    lblCOxxLEGAL_ZIP_CODE: TevLabel;
    lblCOxxFEIN: TevLabel;
    COxxFEIN: TevDBEdit;
    lblCOxxBUSINESS_START_DATE: TevLabel;
    COxxBUSINESS_START_DATE: TevDBDateTimePicker;
    lblCOxxTAX_SERVICE_START_DATE: TevLabel;
    COxxTAX_SERVICE_START_DATE: TevDBDateTimePicker;
    lblCOxxPIN_NUMBER: TevLabel;
    evGroupBox1: TevGroupBox;
    lblCOxxCOBRA_FEE_DAY_OF_MONTH_DUE: TevLabel;
    lblCOxxCOBRA_NOTIFICATION_DAYS: TevLabel;
    lblCOxxCOBRA_ELIGIBILITY_CONFIRM_DAYS: TevLabel;
    lblCOxxSS_DISABILITY_ADMIN_FEE: TevLabel;
    evdgChargeAdminFee: TevDBRadioGroup;
    COxxCOBRA_FEE_DAY_OF_MONTH_DUE: TevDBSpinEdit;
    COxxCOBRA_NOTIFICATION_DAYS: TevDBSpinEdit;
    COxxCOBRA_ELIGIBILITY_CONFIRM_DAYS: TevDBSpinEdit;
    COxxSS_DISABILITY_ADMIN_FEE: TevDBEdit;
    tshEndPage: TTabSheet;
    lblCOxxCL_COMMON_PAYMASTER_NBR: TevLabel;
    lblCOxxCL_CO_CONSOLIDATION_NBR: TevLabel;
    tshCO_ADDITIONAL_INFO_NAMES: TTabSheet;
    lblCO_ADDITIONAL_INFO_NAMESxxNAME: TevLabel;
    CO_ADDITIONAL_INFO_NAMESxxNAME: TevDBEdit;
    grInfoNames: TevDBGrid;
    tshCO_LOCAL_TAX: TTabSheet;
    grLocalTax: TevDBGrid;
    dsCO_LOCAL_TAX: TevDataSource;
    lblCO_LOCAL_TAXxxLOCAL_EIN_NUMBER: TevLabel;
    CO_LOCAL_TAXxxLOCAL_EIN_NUMBER: TevDBEdit;
    lblCO_LOCAL_TAXxxEFT_NAME: TevLabel;
    CO_LOCAL_TAXxxEFT_NAME: TevDBEdit;
    lblCO_LOCAL_TAXxxEFT_PIN_NUMBER: TevLabel;
    CO_LOCAL_TAXxxEFT_PIN_NUMBER: TevDBEdit;
    lblCO_LOCAL_TAXxxEFT_STATUS: TevLabel;
    CO_LOCAL_TAXxxEFT_STATUS: TevDBComboBox;
    CO_LOCAL_TAXxxFINAL_TAX_RETURN: TevDBRadioGroup;
    lblCO_LOCAL_TAXxxFINAL_TAX_RETURN: TevLabel;
    lblCO_LOCAL_TAXxxFILLER: TevLabel;
    tshCO_PHONE: TTabSheet;
    grPhone: TevDBGrid;
    lblCO_PHONExxCONTACT: TevLabel;
    CO_PHONExxCONTACT: TevDBEdit;
    lblCO_PHONExxPHONE_NUMBER: TevLabel;
    CO_PHONExxPHONE_NUMBER: TevDBEdit;
    lblCO_PHONExxPHONE_TYPE: TevLabel;
    CO_PHONExxPHONE_TYPE: TevDBComboBox;
    CO_PHONExxDESCRIPTION: TevDBEdit;
    lblCO_PHONExxDESCRIPTION: TevLabel;
    tshCO_STATES: TTabSheet;
    grStates: TevDBGrid;
    dsCO_STATES: TevDataSource;
    lblCO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR: TevLabel;
    CO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR: TevDBLookupCombo;
    lblCO_STATESxxSTATE_EIN: TevLabel;
    CO_STATESxxSTATE_EIN: TevDBEdit;
    lblCO_STATESxxSTATE_SDI_EIN: TevLabel;
    CO_STATESxxSTATE_SDI_EIN: TevDBEdit;
    lblCO_STATESxxSTATE_EFT_ENROLLMENT_STATUS: TevLabel;
    CO_STATESxxSTATE_EFT_ENROLLMENT_STATUS: TevDBComboBox;
    lblCO_STATESxxSTATE_EFT_PIN_NUMBER: TevLabel;
    CO_STATESxxSTATE_EFT_PIN_NUMBER: TevDBEdit;
    lblCO_STATESxxSTATE_EFT_EIN: TevLabel;
    CO_STATESxxSTATE_EFT_EIN: TevDBEdit;
    lblCO_STATESxxSTATE_SDI_EFT_EIN: TevLabel;
    CO_STATESxxSTATE_SDI_EFT_EIN: TevDBEdit;
    lblCO_STATESxxSUI_EIN: TevLabel;
    CO_STATESxxSUI_EIN: TevDBEdit;
    lblCO_STATESxxSUI_EFT_PIN_NUMBER: TevLabel;
    lblCO_STATESxxSUI_EFT_NAME: TevLabel;
    CO_STATESxxSUI_EFT_NAME: TevDBEdit;
    lblCO_STATESxxSUI_EFT_ENROLLMENT_STATUS: TevLabel;
    CO_STATESxxSUI_EFT_ENROLLMENT_STATUS: TevDBComboBox;
    CO_STATESxxSUI_EFT_PIN_NUMBER: TevDBEdit;
    lblCO_STATESxxSUI_EFT_EIN: TevLabel;
    CO_STATESxxSUI_EFT_EIN: TevDBEdit;
    evButton1: TevBitBtn;
    lblCOxxCHARGE_COBRA_ADMIN_FEE: TevLabel;
    lblCO_LOCAL_TAXxxLocalName: TevLabel;
    lblCO_STATESxxSTATE: TevLabel;
    dsCO_PHONE: TevDataSource;
    tsEmployees: TTabSheet;
    gEmployees: TevDBGrid;
    EE: TevClientDataSet;
    DM_CLIENT: TDM_CLIENT;
    EEEmployee_LastName_Calculate: TStringField;
    EEEmployee_FirstName_Calculate: TStringField;
    EEEmployee_MI_Calculate: TStringField;
    EECURRENT_TERMINATION_CODE_DESC: TStringField;
    EEEE_NBR: TIntegerField;
    EESOCIAL_SECURITY_NUMBER: TStringField;
    EECUSTOM_EMPLOYEE_NUMBER: TStringField;
    EECL_PERSON_NBR: TIntegerField;
    EECURRENT_TERMINATION_CODE: TStringField;
    dsEmployees: TevDataSource;
    EECO_NBR: TIntegerField;
    CO_STATESxxTaxCollectionDistrict: TevDBLookupCombo;
    lblCO_STATESxxTaxCollectionDistrict: TevLabel;
    CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR: TevDBLookupCombo;
    lblCO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR: TevLabel;
    lblCO_STATESxxTCD_PAYMENT_METHOD: TevLabel;
    CO_STATESxxTCD_PAYMENT_METHOD: TevDBComboBox;
    grbHR: TevGroupBox;
    evGroupBox3: TevGroupBox;
    COxxADVANCED_HR_CORE: TevDBCheckBox;
    COxxADVANCED_HR_ATS: TevDBCheckBox;
    COxxADVANCED_HR_TALENT_MGMT: TevDBCheckBox;
    COxxADVANCED_HR_ONLINE_ENROLLMENT: TevDBCheckBox;
    COxxENABLE_HR: TevDBComboBox;
    lblCOxxENABLE_HR: TevLabel;
    COxxENABLE_ANALYTICS: TevDBRadioGroup;
    evLabel69: TevLabel;
    COxxANALYTICS_LICENSE: TevDBComboBox;
    lblCOxxHCM_URL: TevLabel;
    COxxHCM_URL: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure btNextClick(Sender: TObject);
    procedure tshCO_PHONEShow(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grStatesRowChanged(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure btBackClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EECalcFields(DataSet: TDataSet);
    procedure dsClientDataChange(Sender: TObject; Field: TField);
    procedure CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBREnter(Sender: TObject);
    procedure dsCO_STATESDataChange(Sender: TObject; Field: TField);
    procedure COxxENABLE_HRChange(Sender: TObject);
    procedure COxxENABLE_ANALYTICSChange(Sender: TObject);
  private
    { Private declarations }
    FDBLevel: TDBLevel;
    EETableList: TtList;
    procedure HandleInitialPage;
    procedure CopyEmployees;
  protected
    function ProcessValue(TableName: String; aField: TField): Variant; override;
  public
    { Public declarations }
    CLList: TtList;//TTableList;
    MyTableList: TtList;//TTableList;
    constructor CreateTCopyWizard(AOwner: TComponent; L: TDBLevel);
  end;
var
  CompanyCopyWizard: TCompanyCopyWizard;

implementation

uses SDataDictsystem, EvConsts;

{$R *.DFM}

constructor TCompanyCopyWizard.CreateTCopyWizard(AOwner: TComponent; L: TDBLevel);
begin
  Inherited Create(AOwner);
  FDBLevel := L;
end;

procedure TCompanyCopyWizard.HandleInitialPage;
var
  aTableInfo: TvsTableInf;
  S: String;
begin
  SetLength(Nbrs, 1);
  Nbrs[0] := gdCompany.DataSource.DataSet['CO_NBR'];
  if not Assigned(MyTableList.TableList) then
  begin
    CLList.CLBase := DM_TEMPORARY.TMP_CO['CL_NBR'];
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR']);
    if FDBLevel <> evEmployee then
      MyTableList.TableList := CreateTableList(evCompany, TablesToCopyCOList);
    MyTableList.COBase := DM_TEMPORARY.TMP_CO['CO_NBR'];
  end;
  if FDBLevel = evEmployee then
  begin
    MyTableList.CONew := CLList.CONew;
    CopyEmployees;
  end
  else
  begin
    if cbUseImport.Checked then
      if not EvDialog('Input Required', 'Please, enter a company number you want to create from import file:', S) then
      begin
        evPanel2.Visible := True;
        AbortEx;
      end;
    CONumber := S;
    pctCopyWizard.ActivePageIndex := pctCopyWizard.ActivePageIndex + 1;
    S := Copy(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name, 4, Length(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name));
    aTableInfo := MyTableList.TableList[GetTableIndexByName(S, MyTableList.TableList)];
    Self.Caption := 'Company Copy Wizard - ' + aTableInfo.Name;
    ctx_DataAccess.OpenClient(CLList.CLNew);
    LoadExcludeList(aTableInfo);
    CleanOutFields(aTableInfo);
    dsCO.DataSet := aTableInfo.DSBase;
    SetFocusOnActivePage;
  end;
end;

procedure TCompanyCopyWizard.btNextClick(Sender: TObject);
var
  CTable: String;
  I: Integer;
  b: Boolean;
  bs: TBookmarkStr;
begin
  inherited;
  if pctCopyWizard.ActivePage = tsEmployees then
  begin
    btBack.Enabled := False;
    ctx_StartWait('Copying Employees...');
    SetLength(Nbrs, 0);
    bs := gEmployees.DataSource.DataSet.Bookmark;
    gEmployees.DataSource.DataSet.DisableControls;
    try
      for i := 0 to Pred(gEmployees.SelectedList.Count) do
      begin
        gEmployees.DataSource.DataSet.GotoBookmark(gEmployees.SelectedList[i]);
        SetLength(Nbrs, Succ(Length(Nbrs)));
        Nbrs[High(Nbrs)] := gEmployees.DataSource.DataSet['EE_NBR'];
      end;
    finally
      gEmployees.DataSource.DataSet.Bookmark := bs;
      gEmployees.DataSource.DataSet.EnableControls;
    end;
    b := ctx_DataAccess.Copying;
    ctx_DataAccess.Copying := True;
    try
      EETableList.TableList := CreateTableList(evEmployee, TablesToCopyEEList);
      CacheCo_And_EE_States;
      ctx_DataAccess.OpenClient(CLList.CLNew);
      DM_CLIENT.CL_E_DS.Close;
      DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(MyTableList.CONew));

      for I := Low(EETableList.TableList) to High(EETableList.TableList) do
        CleanOutFields(EETableList.TableList[I], MyTableList.CONew);
      CreateNewDBStructure(CLList, EETableList, evEmployee);
      ModalResult := mrOk;
    finally
      ctx_DataAccess.Copying := b;
      ctx_EndWait;
    end;
  end
  else
    case pctCopyWizard.ActivePageIndex of
      0:  HandleInitialPage
      else
      begin
        CTable := HandleClientPages(MyTableList.TableList, True);
        if CTable = 'EndPage' then
        begin
          btNext.Enabled := False;
          ctx_StartWait('Saving new company...');
          try
            MyTableList.CONew := CreateNewDBStructure(CLList, MyTableList, evCompany);
          finally
            ctx_EndWait;
          end;
          if EvMessage('Would you like to copy employees?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then
            CopyEmployees
          else
            ModalResult := mrOk;
        end;
      end;
    end;
end;

procedure TCompanyCopyWizard.tshCO_PHONEShow(Sender: TObject);
begin
  inherited;
  CO_PHONExxPHONE_TYPE.Items.Text := PhoneType_ComboChoices;
end;

procedure TCompanyCopyWizard.FormShow(Sender: TObject);
var
  enable_Analytics: Boolean;
begin
  inherited;
  if Assigned(dsCompany.DataSet) then
    btNext.enabled := ( dsCompany.DataSet.FieldByName('CO_NBR').asInteger > 0 );

  ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ, DM_SYSTEM_STATE.SY_SUI,
                DM_SYSTEM_STATE.SY_GLOBAL_AGENCY,DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ]);
  CO_STATESxxSY_STATE_DEPOSIT_FREQ_NBR.LookupTable := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ;

  DM_SYSTEM_STATE.SY_GLOBAL_AGENCY.Filter := 'AGENCY_TYPE = ''Y''';
  DM_SYSTEM_STATE.SY_GLOBAL_AGENCY.Filtered := True;
  CO_STATESxxTaxCollectionDistrict.LookupTable := DM_SYSTEM_STATE.SY_GLOBAL_AGENCY;
  CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR.LookupTable := DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ;

  DM_SERVICE_BUREAU.SB.DataRequired('');
  enable_Analytics := DM_SERVICE_BUREAU.SB.ANALYTICS_LICENSE.Value = 'Y';
  evGroupBox3.Visible := enable_Analytics;
end;

procedure TCompanyCopyWizard.grStatesRowChanged(Sender: TObject);
begin
  inherited;
  if dsCO_STATES.DataSet.FieldByName('SY_STATES_NBR').AsString <> '' then
  begin
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Filter := 'SY_STATES_NBR='+dsCO_STATES.DataSet.FieldByName('SY_STATES_NBR').AsString;
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Filtered := True;
  end;
end;

procedure TCompanyCopyWizard.evButton1Click(Sender: TObject);
var
  T: TevClientDataSet;
begin
  inherited;
  if Assigned(CLList.TableList) then
    T := CLList.TableList[GetTableIndexByName('CL', CLList.TableList)].DSBase
  else
  begin
    DM_CLIENT.CL.DataRequired('');
    T := DM_CLIENT.CL;
  end;

  with dsCO.DataSet do
  begin
    Edit;
    FieldByName('CUSTOM_COMPANY_NUMBER').AsString := T.FieldByName('CUSTOM_CLIENT_NUMBER').AsString;
    FieldByName('NAME').AsString := T.FieldByName('NAME').AsString;
    FieldByName('ADDRESS1').AsString := T.FieldByName('ADDRESS1').AsString;
    FieldByName('ADDRESS2').AsString := T.FieldByName('ADDRESS2').AsString;
    FieldByName('CITY').AsString := T.FieldByName('CITY').AsString;
    FieldByName('STATE').AsString := T.FieldByName('STATE').AsString;
    FieldByName('ZIP_CODE').AsString := T.FieldByName('ZIP_CODE').AsString;
    Post;
  end;
end;

procedure TCompanyCopyWizard.btBackClick(Sender: TObject);
begin
  HandleClientPages(MyTableList.TableList, False);
  inherited;
end;

procedure TCompanyCopyWizard.btCancelClick(Sender: TObject);
begin
  with DM_COMPANY do
    ctx_DataAccess.CancelDataSets([CO, CO_ADDITIONAL_INFO_NAMES, CO_LOCAL_TAX, CO_PAY_GROUP,
                    CO_PENSIONS, CO_PHONE, CO_PR_CHECK_TEMPLATES,
                    CO_SALESPERSON, CO_SALESPERSON_FLAT_AMT, CO_SERVICES,
                    CO_ENLIST_GROUPS, CO_AUTO_ENLIST_RETURNS,
                    CO_STATES, CO_SUI, CO_WORKERS_COMP, CO_TIME_OFF_ACCRUAL,
                    CO_TIME_OFF_ACCRUAL_RATES, CO_SHIFTS, CO_JOBS, CO_E_D_CODES,
                    CO_BATCH_LOCAL_OR_TEMPS, CO_BATCH_STATES_OR_TEMPS,
                    CO_JOBS_LOCALS, CO_DIVISION, CO_DIVISION_LOCALS,
                    CO_DIV_PR_BATCH_DEFLT_ED, CO_BRANCH, CO_BRANCH_LOCALS,
                    CO_BRCH_PR_BATCH_DEFLT_ED, CO_DEPARTMENT,
                    CO_DEPARTMENT_LOCALS, CO_DEPT_PR_BATCH_DEFLT_ED, CO_TEAM,
                    CO_TEAM_LOCALS, CO_TEAM_PR_BATCH_DEFLT_ED, CO_REPORTS,
                    CO_BENEFITS, CO_BENEFIT_CATEGORY, CO_BENEFIT_RATES,
                    CO_BENEFIT_SUBTYPE, CO_PR_BATCH_DEFLT_ED, CO_PR_FILTERS,
                    CO_GENERAL_LEDGER, CO_LOCATIONS]);
  with DM_EMPLOYEE do
    ctx_DataAccess.CancelDataSets([DM_CLIENT.CL_PERSON, EE, EE_ADDITIONAL_INFO, EE_AUTOLABOR_DISTRIBUTION,
                    EE_BENEFITS, EE_CHILD_SUPPORT_CASES, EE_BENEFIT_PAYMENT, EE_BENEFICIARY,
                    EE_BENEFIT_REFUSAL, EE_DIRECT_DEPOSIT,
                    EE_EMERGENCY_CONTACTS, EE_LOCALS, EE_PENSION_FUND_SPLITS, EE_PIECE_WORK,
                    EE_RATES, EE_SCHEDULED_E_DS, EE_STATES, EE_TIME_OFF_ACCRUAL, EE_WORK_SHIFTS]);
  inherited;
end;

procedure TCompanyCopyWizard.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  dsCompany.MasterDataSource := Nil;
  DM_TEMPORARY.TMP_CO.Filtered := False;

end;

function TCompanyCopyWizard.ProcessValue(TableName: String; aField: TField): Variant;
begin
  if (TableName = 'CO_STATES') and (aField.FieldName = 'SY_STATE_DEPOSIT_FREQ_NBR') then
    Result := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('DESCRIPTION', aField.Value, 'SY_STATE_DEPOSIT_FREQ_NBR')
  else
    Result := inherited ProcessValue(TableName, aField);
end;

procedure TCompanyCopyWizard.CopyEmployees;
begin
  Caption  := 'Copy Wizard - Employee';
  evPanel2.Visible := False;
  btBack.Enabled := False;
  btNext.Enabled := True;
  ctx_StartWait;
  try
    ctx_DataAccess.OpenClient(0);
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR']);
    DM_EMPLOYEE.CO_DIVISION.DataRequired('ALL');
    DM_EMPLOYEE.CO_BRANCH.DataRequired('ALL');
    DM_EMPLOYEE.CO_DEPARTMENT.DataRequired('ALL');
    DM_EMPLOYEE.CO_TEAM.DataRequired('ALL');
    DM_EMPLOYEE.EE.DataRequired();
    EE.Data := DM_EMPLOYEE.EE.Data;
    while not EE.Eof do
      if NbrInList(EE['CO_NBR']) then
        EE.Next
      else
        EE.Delete;
    EE.First;
    gEmployees.SelectAll;
  finally
    ctx_EndWait;
  end;
  pctCopyWizard.ActivePage := tsEmployees;
end;

procedure TCompanyCopyWizard.EECalcFields(DataSet: TDataSet);
begin
  EECURRENT_TERMINATION_CODE_DESC.AsString := ReturnDescription(EE_TerminationCode_ComboChoices, EECURRENT_TERMINATION_CODE.AsString);
end;

procedure TCompanyCopyWizard.dsClientDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(dsCompany.DataSet) then
     btNext.enabled := ( dsCompany.DataSet.FieldByName('CO_NBR').asInteger > 0 );
end;

procedure TCompanyCopyWizard.CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBREnter(Sender: TObject);
begin
  inherited;

  if (not dsCO_STATES.DataSet.FieldByName('TaxCollectionDistrict').IsNull)then
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = ' + dsCO_STATES.DataSet.FieldByName('TaxCollectionDistrict').AsString
  else
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = -1';
  DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filtered := true;
end;


procedure TCompanyCopyWizard.dsCO_STATESDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not Assigned(Field) or (UpperCase(Field.FieldName) = 'STATE') then
  begin
       //   TCD fields
    CO_STATESxxTaxCollectionDistrict.Enabled := dsCO_STATES.DataSet.FieldByName('STATE').AsString = 'PA';
    CO_STATESxxTCD_DEPOSIT_FREQUENCY_NBR.Enabled := CO_STATESxxTaxCollectionDistrict.Enabled;
    CO_STATESxxTCD_PAYMENT_METHOD.Enabled := CO_STATESxxTaxCollectionDistrict.Enabled;
  end;
end;

procedure TCompanyCopyWizard.COxxENABLE_HRChange(Sender: TObject);
begin
  inherited;
   COxxADVANCED_HR_ONLINE_ENROLLMENT.Enabled :=  COxxENABLE_HR.Value = GROUP_BOX_ADVANCED_HR;
   COxxADVANCED_HR_CORE.Enabled := COxxADVANCED_HR_ONLINE_ENROLLMENT.Enabled;
   COxxADVANCED_HR_ATS.Enabled := COxxADVANCED_HR_ONLINE_ENROLLMENT.Enabled;
   COxxADVANCED_HR_TALENT_MGMT.Enabled := COxxADVANCED_HR_ONLINE_ENROLLMENT.Enabled;
   btNext.Enabled:= (COxxENABLE_HR.Value <> GROUP_BOX_ADVANCED_HR) or
                    ((COxxENABLE_HR.Value = GROUP_BOX_ADVANCED_HR) and (Trim(COxxHCM_URL.Text) <> ''));

   if not btNext.Enabled then
      EvMessage('HCM Url must be entered when Advanced HR is selected.');
end;

procedure TCompanyCopyWizard.COxxENABLE_ANALYTICSChange(Sender: TObject);
begin
  inherited;
  if Assigned(dsCO.DataSet) and ( dsCO.DataSet.State in [dsInsert, dsEdit] ) then
    if COxxENABLE_ANALYTICS.ItemIndex = 1 then
       dsCO.DataSet.FieldByName('ANALYTICS_LICENSE').AsString :=ANALYTICS_LICENSE_NA
    else
       dsCO.DataSet.FieldByName('ANALYTICS_LICENSE').AsString :=ANALYTICS_LICENSE_BASIC;
end;

end.
