// Copyright � 2000-2012 iSystems LLC. All rights reserved.
// This is automatically generated file
// Do not modify it!

unit SDataDictsystem;

interface

uses
  SDDClasses, db, classes, EvTypes;

type
  TSY_COUNTY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_COUNTY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property COUNTY_NAME: TStringField index 5 read GetStringField;
    property SY_STATES_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSY_DELIVERY_METHOD = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_DELIVERY_METHOD_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_DELIVERY_SERVICE_NBR: TIntegerField index 5 read GetIntegerField;
    property CLASS_NAME: TStringField index 6 read GetStringField;
    property NAME: TStringField index 7 read GetStringField;
  end;

  TSY_DELIVERY_SERVICE = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_DELIVERY_SERVICE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CLASS_NAME: TStringField index 5 read GetStringField;
    property NAME: TStringField index 6 read GetStringField;
  end;

  TSY_FED_EXEMPTIONS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_EXEMPTIONS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property E_D_CODE_TYPE: TStringField index 5 read GetStringField;
    property EXEMPT_FEDERAL: TStringField index 6 read GetStringField;
    property EXEMPT_FUI: TStringField index 7 read GetStringField;
    property EXEMPT_EMPLOYEE_OASDI: TStringField index 8 read GetStringField;
    property EXEMPT_EMPLOYER_OASDI: TStringField index 9 read GetStringField;
    property EXEMPT_EMPLOYEE_MEDICARE: TStringField index 10 read GetStringField;
    property EXEMPT_EMPLOYER_MEDICARE: TStringField index 11 read GetStringField;
    property EXEMPT_EMPLOYEE_EIC: TStringField index 12 read GetStringField;
    property W2_BOX: TStringField index 13 read GetStringField;
  end;

  TSY_FED_REPORTING_AGENCY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_REPORTING_AGENCY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
  end;

  TSY_FED_TAX_PAYMENT_AGENCY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_REPORTS_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSY_FED_TAX_TABLE = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_TAX_TABLE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property FILLER: TStringField index 5 read GetStringField;
    property EXEMPTION_AMOUNT: TFloatField index 6 read GetFloatField;
    property FEDERAL_MINIMUM_WAGE: TFloatField index 7 read GetFloatField;
    property FEDERAL_TIP_CREDIT: TFloatField index 8 read GetFloatField;
    property SUPPLEMENTAL_TAX_PERCENTAGE: TFloatField index 9 read GetFloatField;
    property OASDI_RATE: TFloatField index 10 read GetFloatField;
    property ER_OASDI_RATE: TFloatField index 11 read GetFloatField;
    property EE_OASDI_RATE: TFloatField index 12 read GetFloatField;
    property OASDI_WAGE_LIMIT: TFloatField index 13 read GetFloatField;
    property MEDICARE_RATE: TFloatField index 14 read GetFloatField;
    property MEDICARE_WAGE_LIMIT: TFloatField index 15 read GetFloatField;
    property FUI_RATE_REAL: TFloatField index 16 read GetFloatField;
    property FUI_RATE_CREDIT: TFloatField index 17 read GetFloatField;
    property FUI_WAGE_LIMIT: TFloatField index 18 read GetFloatField;
    property SY_401K_LIMIT: TFloatField index 19 read GetFloatField;
    property SY_403B_LIMIT: TFloatField index 20 read GetFloatField;
    property SY_457_LIMIT: TFloatField index 21 read GetFloatField;
    property SY_501C_LIMIT: TFloatField index 22 read GetFloatField;
    property SIMPLE_LIMIT: TFloatField index 23 read GetFloatField;
    property SEP_LIMIT: TFloatField index 24 read GetFloatField;
    property DEFERRED_COMP_LIMIT: TFloatField index 25 read GetFloatField;
    property FED_DEPOSIT_FREQ_THRESHOLD: TFloatField index 26 read GetFloatField;
    property FIRST_EIC_LIMIT: TFloatField index 27 read GetFloatField;
    property FIRST_EIC_PERCENTAGE: TFloatField index 28 read GetFloatField;
    property SECOND_EIC_LIMIT: TFloatField index 29 read GetFloatField;
    property SECOND_EIC_AMOUNT: TFloatField index 30 read GetFloatField;
    property THIRD_EIC_ADDITIONAL_PERCENT: TFloatField index 31 read GetFloatField;
    property SY_CATCH_UP_LIMIT: TFloatField index 32 read GetFloatField;
    property SY_DEPENDENT_CARE_LIMIT: TFloatField index 33 read GetFloatField;
    property SY_HSA_SINGLE_LIMIT: TFloatField index 34 read GetFloatField;
    property SY_HSA_FAMILY_LIMIT: TFloatField index 35 read GetFloatField;
    property SY_PENSION_CATCH_UP_LIMIT: TFloatField index 36 read GetFloatField;
    property SY_SIMPLE_CATCH_UP_LIMIT: TFloatField index 37 read GetFloatField;
    property COMPENSATION_LIMIT: TFloatField index 38 read GetFloatField;
    property SY_HSA_CATCH_UP_LIMIT: TFloatField index 39 read GetFloatField;
    property S132_PARKING_LIMIT: TFloatField index 40 read GetFloatField;
    property ROTH_IRA_LIMIT: TFloatField index 41 read GetFloatField;
    property SY_IRA_CATCHUP_LIMIT: TFloatField index 42 read GetFloatField;
    property EE_MED_TH_LIMIT: TFloatField index 43 read GetFloatField;
    property EE_MED_TH_RATE: TFloatField index 44 read GetFloatField;
    property FSA_LIMIT: TFloatField index 45 read GetFloatField;
    property FED_POVERTY_LEVEL: TFloatField index 46 read GetFloatField;
    property ACA_AFFORD_PERCENT: TFloatField index 47 read GetFloatField;
  end;

  TSY_FED_TAX_TABLE_BRACKETS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_TAX_TABLE_BRACKETS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property MARITAL_STATUS: TStringField index 5 read GetStringField;
    property GREATER_THAN_VALUE: TFloatField index 6 read GetFloatField;
    property LESS_THAN_VALUE: TFloatField index 7 read GetFloatField;
    property PERCENTAGE: TFloatField index 8 read GetFloatField;
  end;

  TSY_GLOBAL_AGENCY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property AGENCY_NAME: TStringField index 5 read GetStringField;
    property ACCEPTS_CHECK: TStringField index 6 read GetStringField;
    property ACCEPTS_DEBIT: TStringField index 7 read GetStringField;
    property ACCEPTS_CREDIT: TStringField index 8 read GetStringField;
    property NEGATIVE_DIRECT_DEP_ALLOWED: TStringField index 9 read GetStringField;
    property IGNORE_HOLIDAYS: TStringField index 10 read GetStringField;
    property IGNORE_WEEKENDS: TStringField index 11 read GetStringField;
    property PAYMENT_FILE_DEST_DIRECT: TStringField index 12 read GetStringField;
    property RECEIVING_ABA_NUMBER: TStringField index 13 read GetStringField;
    property RECEIVING_ACCOUNT_NUMBER: TStringField index 14 read GetStringField;
    property RECEIVING_ACCOUNT_TYPE: TStringField index 15 read GetStringField;
    property MAG_MEDIA: TStringField index 16 read GetStringField;
    property FILLER: TStringField index 17 read GetStringField;
    property STATE: TStringField index 18 read GetStringField;
    property COUNTY: TStringField index 19 read GetStringField;
    property TAX_PAYMENT_FIELD_OFFICE_NBR: TIntegerField index 20 read GetIntegerField;
    property NOTES: TBlobField index 21 read GetBlobField;
    property SEND_ZERO_EFT: TStringField index 22 read GetStringField;
    property SEND_ZERO_COUPON: TStringField index 23 read GetStringField;
    property PRINT_ZERO_RETURN: TStringField index 24 read GetStringField;
    property CUSTOM_DEBIT_SY_REPORTS_NBR: TIntegerField index 25 read GetIntegerField;
    property CUSTOM_DEBIT_AFTER_STATUS: TStringField index 26 read GetStringField;
    property CUSTOM_DEBIT_POST_TO_REGISTER: TStringField index 27 read GetStringField;
    property CHECK_ONE_PAYMENT_TYPE: TStringField index 28 read GetStringField;
    property EFTP_ONE_PAYMENT_TYPE: TStringField index 29 read GetStringField;
    property ACH_ONE_PAYMENT_TYPE: TStringField index 30 read GetStringField;
    property AGENCY_TYPE: TStringField index 31 read GetStringField;
  end;

  TSY_GL_AGENCY_FIELD_OFFICE = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
    property ADDITIONAL_NAME: TStringField index 6 read GetStringField;
    property ADDRESS1: TStringField index 7 read GetStringField;
    property ADDRESS2: TStringField index 8 read GetStringField;
    property CITY: TStringField index 9 read GetStringField;
    property ZIP_CODE: TStringField index 10 read GetStringField;
    property STATE: TStringField index 11 read GetStringField;
    property ATTENTION_NAME: TStringField index 12 read GetStringField;
    property CONTACT1: TStringField index 13 read GetStringField;
    property PHONE1: TStringField index 14 read GetStringField;
    property DESCRIPTION1: TStringField index 15 read GetStringField;
    property CONTACT2: TStringField index 16 read GetStringField;
    property PHONE2: TStringField index 17 read GetStringField;
    property DESCRIPTION2: TStringField index 18 read GetStringField;
    property FAX: TStringField index 19 read GetStringField;
    property FAX_DESCRIPTION: TStringField index 20 read GetStringField;
    property FILLER: TStringField index 21 read GetStringField;
    property CATEGORY_TYPE: TStringField index 22 read GetStringField;
  end;

  TSY_GL_AGENCY_HOLIDAYS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_GL_AGENCY_HOLIDAYS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 5 read GetIntegerField;
    property HOLIDAY_NAME: TStringField index 6 read GetStringField;
    property CALC_TYPE: TStringField index 7 read GetStringField;
    property WEEK_NUMBER: TIntegerField index 8 read GetIntegerField;
    property DAY_OF_WEEK: TIntegerField index 9 read GetIntegerField;
    property MONTH_NUMBER: TIntegerField index 10 read GetIntegerField;
    property DAY_NUMBER: TIntegerField index 11 read GetIntegerField;
    property SATURDAY_OFFSET: TIntegerField index 12 read GetIntegerField;
    property SUNDAY_OFFSET: TIntegerField index 13 read GetIntegerField;
  end;

  TSY_GL_AGENCY_REPORT = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_GL_AGENCY_REPORT_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SYSTEM_TAX_TYPE: TStringField index 5 read GetStringField;
    property DEPOSIT_FREQUENCY: TStringField index 6 read GetStringField;
    property SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField index 7 read GetIntegerField;
    property WHEN_DUE_TYPE: TStringField index 8 read GetStringField;
    property NUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField index 9 read GetIntegerField;
    property RETURN_FREQUENCY: TStringField index 10 read GetStringField;
    property ENLIST_AUTOMATICALLY: TStringField index 11 read GetStringField;
    property PRINT_WHEN: TStringField index 12 read GetStringField;
    property FILLER: TStringField index 13 read GetStringField;
    property TAX_SERVICE_FILTER: TStringField index 14 read GetStringField;
    property CONSOLIDATED_FILTER: TStringField index 15 read GetStringField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 16 read GetIntegerField;
    property TAX_RETURN_ACTIVE: TStringField index 17 read GetStringField;
    property BEGIN_EFFECTIVE_MONTH: TIntegerField index 18 read GetIntegerField;
    property END_EFFECTIVE_MONTH: TIntegerField index 19 read GetIntegerField;
    property PAYMENT_METHOD: TStringField index 20 read GetStringField;
    property TAX945_CHECKS: TStringField index 21 read GetStringField;
    property TAX943_COMPANY: TStringField index 22 read GetStringField;
    property TAX944_COMPANY: TStringField index 23 read GetStringField;
    property TAXRETURN940: TStringField index 24 read GetStringField;
    property TAX_RETURN_INACTIVE_START_DATE: TDateTimeField index 25 read GetDateTimeField;
  end;

  TSY_HR_EEO = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_EEO_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
  end;

  TSY_HR_ETHNICITY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_ETHNICITY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
  end;

  TSY_HR_HANDICAPS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_HANDICAPS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
  end;

  TSY_HR_INJURY_CODES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_INJURY_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property INJURY_CODE: TStringField index 6 read GetStringField;
  end;

  TSY_HR_OSHA_ANATOMIC_CODES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_OSHA_ANATOMIC_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
  end;

  TSY_LOCALS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_LOCALS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property LOCAL_TYPE: TStringField index 6 read GetStringField;
    property SY_STATES_NBR: TIntegerField index 7 read GetIntegerField;
    property ZIP_CODE: TStringField index 8 read GetStringField;
    property SY_COUNTY_NBR: TIntegerField index 9 read GetIntegerField;
    property LOCAL_TAX_IDENTIFIER: TStringField index 10 read GetStringField;
    property AGENCY_NUMBER: TIntegerField index 11 read GetIntegerField;
    property MINIMUM_HOURS_WORKED_PER: TStringField index 12 read GetStringField;
    property CALCULATION_METHOD: TStringField index 13 read GetStringField;
    property PRINT_RETURN_IF_ZERO: TStringField index 14 read GetStringField;
    property USE_MISC_TAX_RETURN_CODE: TStringField index 15 read GetStringField;
    property SY_LOCAL_TAX_PMT_AGENCY_NBR: TIntegerField index 16 read GetIntegerField;
    property SY_TAX_PMT_REPORT_NBR: TIntegerField index 17 read GetIntegerField;
    property SY_LOCAL_REPORTING_AGENCY_NBR: TIntegerField index 18 read GetIntegerField;
    property TAX_DEPOSIT_FOLLOW_STATE: TStringField index 19 read GetStringField;
    property FILLER: TStringField index 20 read GetStringField;
    property LOCAL_ACTIVE: TStringField index 21 read GetStringField;
    property ROUND_TO_NEAREST_DOLLAR: TStringField index 22 read GetStringField;
    property TAX_TYPE: TStringField index 23 read GetStringField;
    property TAX_FREQUENCY: TStringField index 24 read GetStringField;
    property LOCAL_MINIMUM_WAGE: TFloatField index 25 read GetFloatField;
    property TAX_RATE: TFloatField index 26 read GetFloatField;
    property TAX_AMOUNT: TFloatField index 27 read GetFloatField;
    property TAX_MAXIMUM: TFloatField index 28 read GetFloatField;
    property WAGE_MAXIMUM: TFloatField index 29 read GetFloatField;
    property WEEKLY_TAX_CAP: TFloatField index 30 read GetFloatField;
    property MINIMUM_HOURS_WORKED: TFloatField index 31 read GetFloatField;
    property MISCELLANEOUS_AMOUNT: TFloatField index 32 read GetFloatField;
    property W2_BOX: TStringField index 33 read GetStringField;
    property PAY_WITH_STATE: TStringField index 34 read GetStringField;
    property WAGE_MINIMUM: TFloatField index 35 read GetFloatField;
    property E_D_CODE_TYPE: TStringField index 36 read GetStringField;
    property QUARTERLY_MINIMUM_THRESHOLD: TFloatField index 37 read GetFloatField;
    property AGENCY_CODE: TStringField index 38 read GetStringField;
    property COMBINE_FOR_TAX_PAYMENTS: TStringField index 39 read GetStringField;
  end;

  TSY_LOCAL_DEPOSIT_FREQ = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_LOCAL_DEPOSIT_FREQ_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_LOCALS_NBR: TIntegerField index 5 read GetIntegerField;
    property WHEN_DUE_TYPE: TStringField index 6 read GetStringField;
    property NUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField index 7 read GetIntegerField;
    property THRD_MNTH_DUE_END_OF_NEXT_MNTH: TStringField index 8 read GetStringField;
    property DESCRIPTION: TStringField index 9 read GetStringField;
    property FILLER: TStringField index 10 read GetStringField;
    property FREQUENCY_TYPE: TStringField index 11 read GetStringField;
    property TAX_COUPON_SY_REPORTS_NBR: TIntegerField index 12 read GetIntegerField;
    property HIDE_FROM_USER: TStringField index 13 read GetStringField;
    property TAX_PAYMENT_TYPE_CODE: TStringField index 14 read GetStringField;
    property INC_TAX_PAYMENT_CODE: TStringField index 15 read GetStringField;
    property CHANGE_FREQ_ON_THRESHOLD: TStringField index 16 read GetStringField;
    property FIRST_THRESHOLD_DEP_FREQ_NBR: TIntegerField index 17 read GetIntegerField;
    property FIRST_THRESHOLD_PERIOD: TStringField index 18 read GetStringField;
    property FIRST_THRESHOLD_AMOUNT: TFloatField index 19 read GetFloatField;
    property SECOND_THRESHOLD_DEP_FREQ_NBR: TIntegerField index 20 read GetIntegerField;
    property SECOND_THRESHOLD_PERIOD: TStringField index 21 read GetStringField;
    property SECOND_THRESHOLD_AMOUNT: TFloatField index 22 read GetFloatField;
    property SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField index 23 read GetIntegerField;
    property QE_SY_GL_AGENCY_OFFICE_NBR: TIntegerField index 24 read GetIntegerField;
    property QE_TAX_COUPON_SY_REPORTS_NBR: TIntegerField index 25 read GetIntegerField;
    property PAY_AND_SHIFTBACK: TStringField index 26 read GetStringField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 27 read GetIntegerField;
  end;

  TSY_LOCAL_EXEMPTIONS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_LOCAL_EXEMPTIONS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_LOCALS_NBR: TIntegerField index 5 read GetIntegerField;
    property E_D_CODE_TYPE: TStringField index 6 read GetStringField;
    property EXEMPT: TStringField index 7 read GetStringField;
  end;

  TSY_LOCAL_MARITAL_STATUS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_LOCAL_MARITAL_STATUS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_LOCALS_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_STATE_MARITAL_STATUS_NBR: TIntegerField index 6 read GetIntegerField;
    property FILLER: TStringField index 7 read GetStringField;
    property STANDARD_DEDUCTION_AMOUNT: TFloatField index 8 read GetFloatField;
    property STANDARD_EXEMPTION_ALLOW: TFloatField index 9 read GetFloatField;
  end;

  TSY_LOCAL_TAX_CHART = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_LOCAL_TAX_CHART_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_LOCALS_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_LOCAL_MARITAL_STATUS_NBR: TIntegerField index 6 read GetIntegerField;
    property MINIMUM: TFloatField index 7 read GetFloatField;
    property MAXIMUM: TFloatField index 8 read GetFloatField;
    property PERCENTAGE: TFloatField index 9 read GetFloatField;
  end;

  TSY_MEDIA_TYPE = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_MEDIA_TYPE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property CLASS_NAME: TStringField index 5 read GetStringField;
    property NAME: TStringField index 6 read GetStringField;
  end;

  TSY_QUEUE_PRIORITY = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_QUEUE_PRIORITY_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property METHOD_NAME: TStringField index 5 read GetStringField;
    property PRIORITY: TIntegerField index 6 read GetIntegerField;
    property THREADS: TIntegerField index 7 read GetIntegerField;
  end;

  TSY_RECIPROCATED_STATES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_RECIPROCATED_STATES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property MAIN_STATE_NBR: TIntegerField index 5 read GetIntegerField;
    property PARTICIPANT_STATE_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSY_REPORTS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property ACTIVE_REPORT: TStringField index 6 read GetStringField;
    property SY_REPORT_WRITER_REPORTS_NBR: TIntegerField index 7 read GetIntegerField;
    property FILLER: TStringField index 8 read GetStringField;
    property ACTIVE_YEAR_FROM: TDateField index 9 read GetDateField;
    property ACTIVE_YEAR_TO: TDateField index 10 read GetDateField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 11 read GetIntegerField;
  end;

  TSY_REPORTS_GROUP = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_REPORTS_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_GL_AGENCY_REPORT_NBR: TIntegerField index 6 read GetIntegerField;
    property FILLER: TStringField index 7 read GetStringField;
    property AGENCY_COPY: TStringField index 8 read GetStringField;
    property SB_COPY: TStringField index 9 read GetStringField;
    property CL_COPY: TStringField index 10 read GetStringField;
    property MEDIA_TYPE: TStringField index 11 read GetStringField;
    property NAME: TStringField index 12 read GetStringField;
  end;

  TSY_REPORT_GROUPS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORT_GROUPS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
  end;

  TSY_REPORT_GROUP_MEMBERS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORT_GROUP_MEMBERS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_REPORT_GROUPS_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_REPORT_WRITER_REPORTS_NBR: TIntegerField index 6 read GetIntegerField;
  end;

  TSY_REPORT_WRITER_REPORTS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORT_WRITER_REPORTS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property REPORT_DESCRIPTION: TStringField index 5 read GetStringField;
    property REPORT_TYPE: TStringField index 6 read GetStringField;
    property REPORT_FILE: TBlobField index 7 read GetBlobField;
    property NOTES: TBlobField index 8 read GetBlobField;
    property REPORT_STATUS: TStringField index 9 read GetStringField;
    property MEDIA_TYPE: TStringField index 10 read GetStringField;
    property CLASS_NAME: TStringField index 11 read GetStringField;
    property ANCESTOR_CLASS_NAME: TStringField index 12 read GetStringField;
  end;

  TSY_SEC_TEMPLATES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_SEC_TEMPLATES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property NAME: TStringField index 5 read GetStringField;
    property DATA: TBlobField index 6 read GetBlobField;
  end;

  TSY_STATES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STATES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property STATE: TStringField index 5 read GetStringField;
    property NAME: TStringField index 6 read GetStringField;
    property STATE_WITHHOLDING: TStringField index 7 read GetStringField;
    property STATE_EFT_TYPE: TStringField index 8 read GetStringField;
    property EFT_NOTES: TBlobField index 9 read GetBlobField;
    property PRINT_STATE_RETURN_IF_ZERO: TStringField index 10 read GetStringField;
    property SUI_EFT_TYPE: TStringField index 11 read GetStringField;
    property SUI_EFT_NOTES: TBlobField index 12 read GetBlobField;
    property SDI_RECIPROCATE: TStringField index 13 read GetStringField;
    property SUI_RECIPROCATE: TStringField index 14 read GetStringField;
    property SHOW_S125_IN_GROSS_WAGES_SUI: TStringField index 15 read GetStringField;
    property PRINT_SUI_RETURN_IF_ZERO: TStringField index 16 read GetStringField;
    property PAYROLL: TStringField index 17 read GetStringField;
    property SALES: TStringField index 18 read GetStringField;
    property CORPORATE: TStringField index 19 read GetStringField;
    property RAILROAD: TStringField index 20 read GetStringField;
    property UNEMPLOYMENT: TStringField index 21 read GetStringField;
    property OTHER: TStringField index 22 read GetStringField;
    property UIFSA_NEW_HIRE_REPORTING: TStringField index 23 read GetStringField;
    property DD_CHILD_SUPPORT: TStringField index 24 read GetStringField;
    property USE_STATE_MISC_TAX_RETURN_CODE: TStringField index 25 read GetStringField;
    property SY_STATE_TAX_PMT_AGENCY_NBR: TIntegerField index 26 read GetIntegerField;
    property SY_TAX_PMT_REPORT_NBR: TIntegerField index 27 read GetIntegerField;
    property SY_STATE_REPORTING_AGENCY_NBR: TIntegerField index 28 read GetIntegerField;
    property PAY_SDI_WITH: TStringField index 29 read GetStringField;
    property FILLER: TStringField index 30 read GetStringField;
    property TAX_DEPOSIT_FOLLOW_FEDERAL: TStringField index 31 read GetStringField;
    property ROUND_TO_NEAREST_DOLLAR: TStringField index 32 read GetStringField;
    property SUI_TAX_PAYMENT_TYPE_CODE: TStringField index 33 read GetStringField;
    property INC_SUI_TAX_PAYMENT_CODE: TStringField index 34 read GetStringField;
    property FIPS_CODE: TStringField index 35 read GetStringField;
    property STATE_MINIMUM_WAGE: TFloatField index 36 read GetFloatField;
    property STATE_TIP_CREDIT: TFloatField index 37 read GetFloatField;
    property SUPPLEMENTAL_WAGES_PERCENTAGE: TFloatField index 38 read GetFloatField;
    property WEEKLY_SDI_TAX_CAP: TFloatField index 39 read GetFloatField;
    property EE_SDI_MAXIMUM_WAGE: TFloatField index 40 read GetFloatField;
    property ER_SDI_MAXIMUM_WAGE: TFloatField index 41 read GetFloatField;
    property EE_SDI_RATE: TFloatField index 42 read GetFloatField;
    property ER_SDI_RATE: TFloatField index 43 read GetFloatField;
    property MAXIMUM_GARNISHMENT_PERCENT: TFloatField index 44 read GetFloatField;
    property GARNISH_MIN_WAGE_MULTIPLIER: TFloatField index 45 read GetFloatField;
    property SALES_TAX_PERCENTAGE: TFloatField index 46 read GetFloatField;
    property W2_BOX: TStringField index 47 read GetStringField;
    property RECIPROCITY_TYPE: TStringField index 48 read GetStringField;
    property PRINT_W2: TStringField index 49 read GetStringField;
    property CAP_STATE_TAX_CREDIT: TStringField index 50 read GetStringField;
    property INACTIVATE_MARITAL_STATUS: TStringField index 51 read GetStringField;
    property STATE_OT_TIP_CREDIT: TFloatField index 52 read GetFloatField;
  end;

  TSY_STATE_DEPOSIT_FREQ = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STATE_DEPOSIT_FREQ_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_STATES_NBR: TIntegerField index 5 read GetIntegerField;
    property WHEN_DUE_TYPE: TStringField index 6 read GetStringField;
    property NUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField index 7 read GetIntegerField;
    property THRESHOLD_PERIOD: TStringField index 8 read GetStringField;
    property CHANGE_STATUS_ON_THRESHOLD: TStringField index 9 read GetStringField;
    property THRD_MNTH_DUE_END_OF_NEXT_MNTH: TStringField index 10 read GetStringField;
    property DESCRIPTION: TStringField index 11 read GetStringField;
    property THRESHOLD_DEP_FREQUENCY_NUMBER: TIntegerField index 12 read GetIntegerField;
    property FILLER: TStringField index 13 read GetStringField;
    property SECOND_THRESHOLD_PERIOD: TStringField index 14 read GetStringField;
    property TAX_PAYMENT_TYPE_CODE: TStringField index 15 read GetStringField;
    property INC_TAX_PAYMENT_CODE: TStringField index 16 read GetStringField;
    property SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField index 17 read GetIntegerField;
    property FREQUENCY_TYPE: TStringField index 18 read GetStringField;
    property TAX_COUPON_SY_REPORTS_NBR: TIntegerField index 19 read GetIntegerField;
    property THRESHOLD_AMOUNT: TFloatField index 20 read GetFloatField;
    property SECOND_THRESHOLD_AMOUNT: TFloatField index 21 read GetFloatField;
    property QE_TAX_COUPON_SY_REPORTS_NBR: TIntegerField index 22 read GetIntegerField;
    property QE_SY_GL_AGENCY_OFFICE_NBR: TIntegerField index 23 read GetIntegerField;
    property HIDE_FROM_USER: TStringField index 24 read GetStringField;
    property SECOND_THRESHOLD_DEP_FREQ_NBR: TIntegerField index 25 read GetIntegerField;
    property PAY_AND_SHIFTBACK: TStringField index 26 read GetStringField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 27 read GetIntegerField;
  end;

  TSY_STATE_EXEMPTIONS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STATE_EXEMPTIONS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_STATES_NBR: TIntegerField index 5 read GetIntegerField;
    property E_D_CODE_TYPE: TStringField index 6 read GetStringField;
    property EXEMPT_STATE: TStringField index 7 read GetStringField;
    property EXEMPT_EMPLOYEE_SDI: TStringField index 8 read GetStringField;
    property EXEMPT_EMPLOYER_SDI: TStringField index 9 read GetStringField;
    property EXEMPT_EMPLOYEE_SUI: TStringField index 10 read GetStringField;
    property EXEMPT_EMPLOYER_SUI: TStringField index 11 read GetStringField;
  end;

  TSY_STATE_MARITAL_STATUS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STATE_MARITAL_STATUS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_STATES_NBR: TIntegerField index 5 read GetIntegerField;
    property STATUS_TYPE: TStringField index 6 read GetStringField;
    property STATUS_DESCRIPTION: TStringField index 7 read GetStringField;
    property PERSONAL_EXEMPTIONS: TIntegerField index 8 read GetIntegerField;
    property DEDUCT_FEDERAL: TStringField index 9 read GetStringField;
    property DEDUCT_FICA: TStringField index 10 read GetStringField;
    property FILLER: TStringField index 11 read GetStringField;
    property STATE_PERCENT_OF_FEDERAL: TFloatField index 12 read GetFloatField;
    property STANDARD_DEDUCTION_PCNT_GROSS: TFloatField index 13 read GetFloatField;
    property STANDARD_DEDUCTION_MIN_AMOUNT: TFloatField index 14 read GetFloatField;
    property STANDARD_DEDUCTION_MAX_AMOUNT: TFloatField index 15 read GetFloatField;
    property STANDARD_DEDUCTION_FLAT_AMOUNT: TFloatField index 16 read GetFloatField;
    property STANDARD_PER_EXEMPTION_ALLOW: TFloatField index 17 read GetFloatField;
    property DEDUCT_FEDERAL_MAXIMUM_AMOUNT: TFloatField index 18 read GetFloatField;
    property PER_DEPENDENT_ALLOWANCE: TFloatField index 19 read GetFloatField;
    property PERSONAL_TAX_CREDIT_AMOUNT: TFloatField index 20 read GetFloatField;
    property TAX_CREDIT_PER_DEPENDENT: TFloatField index 21 read GetFloatField;
    property TAX_CREDIT_PER_ALLOWANCE: TFloatField index 22 read GetFloatField;
    property HIGH_INCOME_PER_EXEMPT_ALLOW: TFloatField index 23 read GetFloatField;
    property DEFINED_HIGH_INCOME_AMOUNT: TFloatField index 24 read GetFloatField;
    property MINIMUM_TAXABLE_INCOME: TFloatField index 25 read GetFloatField;
    property ADDITIONAL_EXEMPT_ALLOWANCE: TFloatField index 26 read GetFloatField;
    property ADDITIONAL_DEDUCTION_ALLOWANCE: TFloatField index 27 read GetFloatField;
    property DEDUCT_FICA__MAXIMUM_AMOUNT: TFloatField index 28 read GetFloatField;
    property BLIND_EXEMPTION_AMOUNT: TFloatField index 29 read GetFloatField;
    property ACTIVE_STATUS: TStringField index 30 read GetStringField;
  end;

  TSY_STATE_TAX_CHART = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STATE_TAX_CHART_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_STATES_NBR: TIntegerField index 5 read GetIntegerField;
    property SY_STATE_MARITAL_STATUS_NBR: TIntegerField index 6 read GetIntegerField;
    property ENTRY_TYPE: TStringField index 7 read GetStringField;
    property MINIMUM: TFloatField index 8 read GetFloatField;
    property MAXIMUM: TFloatField index 9 read GetFloatField;
    property PERCENTAGE: TFloatField index 10 read GetFloatField;
  end;

  TSY_STORAGE = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_STORAGE_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property TAG: TStringField index 5 read GetStringField;
    property STORAGE_DATA: TBlobField index 6 read GetBlobField;
  end;

  TSY_SUI = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_SUI_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property SY_STATES_NBR: TIntegerField index 5 read GetIntegerField;
    property SUI_TAX_NAME: TStringField index 6 read GetStringField;
    property EE_ER_OR_EE_OTHER_OR_ER_OTHER: TStringField index 7 read GetStringField;
    property FUTURE_DEFAULT_RATE_BEGIN_DATE: TDateTimeField index 8 read GetDateTimeField;
    property FUTURE_MAX_WAGE_BEGIN_DATE: TDateTimeField index 9 read GetDateTimeField;
    property USE_MISC_TAX_RETURN_CODE: TStringField index 10 read GetStringField;
    property SY_SUI_TAX_PMT_AGENCY_NBR: TIntegerField index 11 read GetIntegerField;
    property SY_TAX_PMT_REPORT_NBR: TIntegerField index 12 read GetIntegerField;
    property SY_SUI_REPORTING_AGENCY_NBR: TIntegerField index 13 read GetIntegerField;
    property FILLER: TStringField index 14 read GetStringField;
    property ROUND_TO_NEAREST_DOLLAR: TStringField index 15 read GetStringField;
    property SUI_ACTIVE: TStringField index 16 read GetStringField;
    property NEW_COMPANY_DEFAULT_RATE: TFloatField index 17 read GetFloatField;
    property FUTURE_DEFAULT_RATE: TFloatField index 18 read GetFloatField;
    property MAXIMUM_WAGE: TFloatField index 19 read GetFloatField;
    property FUTURE_MAXIMUM_WAGE: TFloatField index 20 read GetFloatField;
    property TAX_COUPON_SY_REPORTS_NBR: TIntegerField index 21 read GetIntegerField;
    property FREQUENCY_TYPE: TStringField index 22 read GetStringField;
    property W2_BOX: TStringField index 23 read GetStringField;
    property PAY_WITH_STATE: TStringField index 24 read GetStringField;
    property GLOBAL_RATE: TFloatField index 25 read GetFloatField;
    property E_D_CODE_TYPE: TStringField index 26 read GetStringField;
    property FTE_EXEMPTION: TIntegerField index 27 read GetIntegerField;
    property FTE_WEEKLY_HOURS: TIntegerField index 28 read GetIntegerField;
    property FTE_MULTIPLIER: TIntegerField index 29 read GetIntegerField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 30 read GetIntegerField;
    property ALTERNATE_TAXABLE_WAGE_BASE: TFloatField index 31 read GetFloatField;
    property CO_ADD_ALL_BUTTON: TStringField index 32 read GetStringField;
  end;

  TSY_HR_REFUSAL_REASON = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_HR_REFUSAL_REASON_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property HEALTHCARE_COVERAGE: TStringField index 5 read GetStringField;
    property SY_STATES_NBR: TIntegerField index 6 read GetIntegerField;
    property DESCRIPTION: TStringField index 7 read GetStringField;
  end;

  TSY_AGENCY_DEPOSIT_FREQ = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_AGENCY_DEPOSIT_FREQ_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
    property FREQUENCY_TYPE: TStringField index 6 read GetStringField;
    property NUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField index 7 read GetIntegerField;
    property SY_REPORTS_GROUP_NBR: TIntegerField index 8 read GetIntegerField;
    property SY_GLOBAL_AGENCY_NBR: TIntegerField index 9 read GetIntegerField;
    property TAX_PAYMENT_TYPE_CODE: TStringField index 10 read GetStringField;
    property WHEN_DUE_TYPE: TStringField index 11 read GetStringField;
  end;

  TSY_REPORTS_VERSIONS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_REPORTS_VERSIONS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DB_VERSION: TStringField index 5 read GetStringField;
    property STATUS: TStringField index 6 read GetStringField;
    property STATUS_DATE: TDateTimeField index 7 read GetDateTimeField;
    property STATUS_BY: TIntegerField index 8 read GetIntegerField;
    property CURRENT_REPORT: TStringField index 9 read GetStringField;
    property MODIFIED_ON: TDateTimeField index 10 read GetDateTimeField;
    property TICKET: TStringField index 11 read GetStringField;
    property SY_REPORT_WRITER_REPORTS_NBR: TIntegerField index 12 read GetIntegerField;
    property REPORT_DATA: TBlobField index 13 read GetBlobField;
    property CLASS_NAME: TStringField index 14 read GetStringField;
    property ANCESTOR_CLASS_NAME: TStringField index 15 read GetStringField;
    property NOTES: TBlobField index 16 read GetBlobField;
  end;

  TSY_DASHBOARDS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_DASHBOARDS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DASHBOARD_TYPE: TStringField index 5 read GetStringField;
    property SY_ANALYTICS_TIER_NBR: TIntegerField index 6 read GetIntegerField;
    property NOTES: TBlobField index 7 read GetBlobField;
    property DASHBOARD_ID: TIntegerField index 8 read GetIntegerField;
    property DESCRIPTION: TStringField index 9 read GetStringField;
    property RELEASED: TStringField index 10 read GetStringField;
  end;

  TSY_ANALYTICS_TIER = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_ANALYTICS_TIER_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property TIER_ID: TStringField index 5 read GetStringField;
    property DASHBOARD_DEFAULT_COUNT: TIntegerField index 6 read GetIntegerField;
    property DASHBOARD_MAX_COUNT: TIntegerField index 7 read GetIntegerField;
    property USERS_DEFAULT_COUNT: TIntegerField index 8 read GetIntegerField;
    property USERS_MAX_COUNT: TIntegerField index 9 read GetIntegerField;
    property LOOKBACK_YEARS_DEFAULT: TIntegerField index 10 read GetIntegerField;
    property LOOKBACK_YEARS_MAX: TIntegerField index 11 read GetIntegerField;
    property MOBILE_APP: TStringField index 12 read GetStringField;
    property INTERNAL_BENCHMARKING: TStringField index 13 read GetStringField;
  end;

  TSY_VENDORS = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_VENDORS_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property VENDOR_NAME: TStringField index 5 read GetStringField;
    property SY_VENDOR_CATEGORIES_NBR: TIntegerField index 6 read GetIntegerField;
    property VENDOR_TYPE: TStringField index 7 read GetStringField;
  end;

  TSY_VENDOR_CATEGORIES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_VENDOR_CATEGORIES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property DESCRIPTION: TStringField index 5 read GetStringField;
  end;

  TSY_FED_ACA_OFFER_CODES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_ACA_OFFER_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property ACA_OFFER_CODE: TStringField index 5 read GetStringField;
    property ACA_OFFER_CODE_DESCRIPTION: TStringField index 6 read GetStringField;
  end;

  TSY_FED_ACA_RELIEF_CODES = class(TddSYTable)
  public
    class function GetClassIndex: Integer; override;
    class function GetRequiredFields: TddFieldList; override;
    class function GetParentsDesc: TddReferencesDesc; override;
    class function GetChildrenDesc: TddReferencesDesc; override;
    class function GetFieldSize(FieldName: string): Integer; override;
    class function GetFieldPrecision(FieldName: string): Integer; override;
    class function GetPrimaryKey: TddFieldList; override;
    class function GetLogicalKeys: TddFieldList; override;
    class function GetEntityName: String; override;
    class function GetFieldEntityProperty(const FieldName: string): string; override;
    class function IsVersionedTable: Boolean; override;
    class function IsVersionedField(FieldName: string): Boolean; override;
    class function GetEvTableNbr: Integer; override;
    class function GetEvFieldNbr(const AFieldName: string): Integer; override;
  published
    property REC_VERSION: TIntegerField index 1 read GetIntegerField;
    property SY_FED_ACA_RELIEF_CODES_NBR: TIntegerField index 2 read GetIntegerField;
    property EFFECTIVE_DATE: TDateField index 3 read GetDateField;
    property EFFECTIVE_UNTIL: TDateField index 4 read GetDateField;
    property ACA_RELIEF_CODE: TStringField index 5 read GetStringField;
    property ACA_RELIEF_CODE_DESCRIPTION: TStringField index 6 read GetStringField;
  end;

  TDM_SYSTEM_ = class(TddDatabase)
  public
    class function GetDBType: TevDBType; override;
  private
    function GetSY_COUNTY: TSY_COUNTY;
  published
    property SY_COUNTY: TSY_COUNTY read GetSY_COUNTY;
  private
    function GetSY_DELIVERY_METHOD: TSY_DELIVERY_METHOD;
  published
    property SY_DELIVERY_METHOD: TSY_DELIVERY_METHOD read GetSY_DELIVERY_METHOD;
  private
    function GetSY_DELIVERY_SERVICE: TSY_DELIVERY_SERVICE;
  published
    property SY_DELIVERY_SERVICE: TSY_DELIVERY_SERVICE read GetSY_DELIVERY_SERVICE;
  private
    function GetSY_FED_EXEMPTIONS: TSY_FED_EXEMPTIONS;
  published
    property SY_FED_EXEMPTIONS: TSY_FED_EXEMPTIONS read GetSY_FED_EXEMPTIONS;
  private
    function GetSY_FED_REPORTING_AGENCY: TSY_FED_REPORTING_AGENCY;
  published
    property SY_FED_REPORTING_AGENCY: TSY_FED_REPORTING_AGENCY read GetSY_FED_REPORTING_AGENCY;
  private
    function GetSY_FED_TAX_PAYMENT_AGENCY: TSY_FED_TAX_PAYMENT_AGENCY;
  published
    property SY_FED_TAX_PAYMENT_AGENCY: TSY_FED_TAX_PAYMENT_AGENCY read GetSY_FED_TAX_PAYMENT_AGENCY;
  private
    function GetSY_FED_TAX_TABLE: TSY_FED_TAX_TABLE;
  published
    property SY_FED_TAX_TABLE: TSY_FED_TAX_TABLE read GetSY_FED_TAX_TABLE;
  private
    function GetSY_FED_TAX_TABLE_BRACKETS: TSY_FED_TAX_TABLE_BRACKETS;
  published
    property SY_FED_TAX_TABLE_BRACKETS: TSY_FED_TAX_TABLE_BRACKETS read GetSY_FED_TAX_TABLE_BRACKETS;
  private
    function GetSY_GLOBAL_AGENCY: TSY_GLOBAL_AGENCY;
  published
    property SY_GLOBAL_AGENCY: TSY_GLOBAL_AGENCY read GetSY_GLOBAL_AGENCY;
  private
    function GetSY_GL_AGENCY_FIELD_OFFICE: TSY_GL_AGENCY_FIELD_OFFICE;
  published
    property SY_GL_AGENCY_FIELD_OFFICE: TSY_GL_AGENCY_FIELD_OFFICE read GetSY_GL_AGENCY_FIELD_OFFICE;
  private
    function GetSY_GL_AGENCY_HOLIDAYS: TSY_GL_AGENCY_HOLIDAYS;
  published
    property SY_GL_AGENCY_HOLIDAYS: TSY_GL_AGENCY_HOLIDAYS read GetSY_GL_AGENCY_HOLIDAYS;
  private
    function GetSY_GL_AGENCY_REPORT: TSY_GL_AGENCY_REPORT;
  published
    property SY_GL_AGENCY_REPORT: TSY_GL_AGENCY_REPORT read GetSY_GL_AGENCY_REPORT;
  private
    function GetSY_HR_EEO: TSY_HR_EEO;
  published
    property SY_HR_EEO: TSY_HR_EEO read GetSY_HR_EEO;
  private
    function GetSY_HR_ETHNICITY: TSY_HR_ETHNICITY;
  published
    property SY_HR_ETHNICITY: TSY_HR_ETHNICITY read GetSY_HR_ETHNICITY;
  private
    function GetSY_HR_HANDICAPS: TSY_HR_HANDICAPS;
  published
    property SY_HR_HANDICAPS: TSY_HR_HANDICAPS read GetSY_HR_HANDICAPS;
  private
    function GetSY_HR_INJURY_CODES: TSY_HR_INJURY_CODES;
  published
    property SY_HR_INJURY_CODES: TSY_HR_INJURY_CODES read GetSY_HR_INJURY_CODES;
  private
    function GetSY_HR_OSHA_ANATOMIC_CODES: TSY_HR_OSHA_ANATOMIC_CODES;
  published
    property SY_HR_OSHA_ANATOMIC_CODES: TSY_HR_OSHA_ANATOMIC_CODES read GetSY_HR_OSHA_ANATOMIC_CODES;
  private
    function GetSY_LOCALS: TSY_LOCALS;
  published
    property SY_LOCALS: TSY_LOCALS read GetSY_LOCALS;
  private
    function GetSY_LOCAL_DEPOSIT_FREQ: TSY_LOCAL_DEPOSIT_FREQ;
  published
    property SY_LOCAL_DEPOSIT_FREQ: TSY_LOCAL_DEPOSIT_FREQ read GetSY_LOCAL_DEPOSIT_FREQ;
  private
    function GetSY_LOCAL_EXEMPTIONS: TSY_LOCAL_EXEMPTIONS;
  published
    property SY_LOCAL_EXEMPTIONS: TSY_LOCAL_EXEMPTIONS read GetSY_LOCAL_EXEMPTIONS;
  private
    function GetSY_LOCAL_MARITAL_STATUS: TSY_LOCAL_MARITAL_STATUS;
  published
    property SY_LOCAL_MARITAL_STATUS: TSY_LOCAL_MARITAL_STATUS read GetSY_LOCAL_MARITAL_STATUS;
  private
    function GetSY_LOCAL_TAX_CHART: TSY_LOCAL_TAX_CHART;
  published
    property SY_LOCAL_TAX_CHART: TSY_LOCAL_TAX_CHART read GetSY_LOCAL_TAX_CHART;
  private
    function GetSY_MEDIA_TYPE: TSY_MEDIA_TYPE;
  published
    property SY_MEDIA_TYPE: TSY_MEDIA_TYPE read GetSY_MEDIA_TYPE;
  private
    function GetSY_QUEUE_PRIORITY: TSY_QUEUE_PRIORITY;
  published
    property SY_QUEUE_PRIORITY: TSY_QUEUE_PRIORITY read GetSY_QUEUE_PRIORITY;
  private
    function GetSY_RECIPROCATED_STATES: TSY_RECIPROCATED_STATES;
  published
    property SY_RECIPROCATED_STATES: TSY_RECIPROCATED_STATES read GetSY_RECIPROCATED_STATES;
  private
    function GetSY_REPORTS: TSY_REPORTS;
  published
    property SY_REPORTS: TSY_REPORTS read GetSY_REPORTS;
  private
    function GetSY_REPORTS_GROUP: TSY_REPORTS_GROUP;
  published
    property SY_REPORTS_GROUP: TSY_REPORTS_GROUP read GetSY_REPORTS_GROUP;
  private
    function GetSY_REPORT_GROUPS: TSY_REPORT_GROUPS;
  published
    property SY_REPORT_GROUPS: TSY_REPORT_GROUPS read GetSY_REPORT_GROUPS;
  private
    function GetSY_REPORT_GROUP_MEMBERS: TSY_REPORT_GROUP_MEMBERS;
  published
    property SY_REPORT_GROUP_MEMBERS: TSY_REPORT_GROUP_MEMBERS read GetSY_REPORT_GROUP_MEMBERS;
  private
    function GetSY_REPORT_WRITER_REPORTS: TSY_REPORT_WRITER_REPORTS;
  published
    property SY_REPORT_WRITER_REPORTS: TSY_REPORT_WRITER_REPORTS read GetSY_REPORT_WRITER_REPORTS;
  private
    function GetSY_SEC_TEMPLATES: TSY_SEC_TEMPLATES;
  published
    property SY_SEC_TEMPLATES: TSY_SEC_TEMPLATES read GetSY_SEC_TEMPLATES;
  private
    function GetSY_STATES: TSY_STATES;
  published
    property SY_STATES: TSY_STATES read GetSY_STATES;
  private
    function GetSY_STATE_DEPOSIT_FREQ: TSY_STATE_DEPOSIT_FREQ;
  published
    property SY_STATE_DEPOSIT_FREQ: TSY_STATE_DEPOSIT_FREQ read GetSY_STATE_DEPOSIT_FREQ;
  private
    function GetSY_STATE_EXEMPTIONS: TSY_STATE_EXEMPTIONS;
  published
    property SY_STATE_EXEMPTIONS: TSY_STATE_EXEMPTIONS read GetSY_STATE_EXEMPTIONS;
  private
    function GetSY_STATE_MARITAL_STATUS: TSY_STATE_MARITAL_STATUS;
  published
    property SY_STATE_MARITAL_STATUS: TSY_STATE_MARITAL_STATUS read GetSY_STATE_MARITAL_STATUS;
  private
    function GetSY_STATE_TAX_CHART: TSY_STATE_TAX_CHART;
  published
    property SY_STATE_TAX_CHART: TSY_STATE_TAX_CHART read GetSY_STATE_TAX_CHART;
  private
    function GetSY_STORAGE: TSY_STORAGE;
  published
    property SY_STORAGE: TSY_STORAGE read GetSY_STORAGE;
  private
    function GetSY_SUI: TSY_SUI;
  published
    property SY_SUI: TSY_SUI read GetSY_SUI;
  private
    function GetSY_HR_REFUSAL_REASON: TSY_HR_REFUSAL_REASON;
  published
    property SY_HR_REFUSAL_REASON: TSY_HR_REFUSAL_REASON read GetSY_HR_REFUSAL_REASON;
  private
    function GetSY_AGENCY_DEPOSIT_FREQ: TSY_AGENCY_DEPOSIT_FREQ;
  published
    property SY_AGENCY_DEPOSIT_FREQ: TSY_AGENCY_DEPOSIT_FREQ read GetSY_AGENCY_DEPOSIT_FREQ;
  private
    function GetSY_REPORTS_VERSIONS: TSY_REPORTS_VERSIONS;
  published
    property SY_REPORTS_VERSIONS: TSY_REPORTS_VERSIONS read GetSY_REPORTS_VERSIONS;
  private
    function GetSY_DASHBOARDS: TSY_DASHBOARDS;
  published
    property SY_DASHBOARDS: TSY_DASHBOARDS read GetSY_DASHBOARDS;
  private
    function GetSY_ANALYTICS_TIER: TSY_ANALYTICS_TIER;
  published
    property SY_ANALYTICS_TIER: TSY_ANALYTICS_TIER read GetSY_ANALYTICS_TIER;
  private
    function GetSY_VENDORS: TSY_VENDORS;
  published
    property SY_VENDORS: TSY_VENDORS read GetSY_VENDORS;
  private
    function GetSY_VENDOR_CATEGORIES: TSY_VENDOR_CATEGORIES;
  published
    property SY_VENDOR_CATEGORIES: TSY_VENDOR_CATEGORIES read GetSY_VENDOR_CATEGORIES;
  private
    function GetSY_FED_ACA_OFFER_CODES: TSY_FED_ACA_OFFER_CODES;
  published
    property SY_FED_ACA_OFFER_CODES: TSY_FED_ACA_OFFER_CODES read GetSY_FED_ACA_OFFER_CODES;
  private
    function GetSY_FED_ACA_RELIEF_CODES: TSY_FED_ACA_RELIEF_CODES;
  published
    property SY_FED_ACA_RELIEF_CODES: TSY_FED_ACA_RELIEF_CODES read GetSY_FED_ACA_RELIEF_CODES;
  end;


procedure Register;

implementation

{TSY_COUNTY}

class function TSY_COUNTY.GetClassIndex: Integer;
begin
  Result := 1;
end;

class function TSY_COUNTY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_COUNTY.GetEvTableNbr: Integer;
begin
  Result := 1;
end;

class function TSY_COUNTY.GetEntityName: String;
begin
  Result := 'County';
end;

class function TSY_COUNTY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_COUNTY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 2
  else if AFieldName = 'SY_COUNTY_NBR' then Result := 4
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 1
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 6
  else if AFieldName = 'COUNTY_NAME' then Result := 5
  else if AFieldName = 'SY_STATES_NBR' then Result := 3
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_COUNTY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else if FieldName = 'COUNTY_NAME' then Result := 'Name'
  else if FieldName = 'SY_STATES_NBR' then Result := 'StateId'
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_COUNTY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_COUNTY_NBR';
  Result[1] := 'COUNTY_NAME';
  Result[2] := 'SY_STATES_NBR';
end;

class function TSY_COUNTY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'COUNTY_NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_COUNTY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_COUNTY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_COUNTY_NBR';
end;

class function TSY_COUNTY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := 'Counties';
end;

class function TSY_COUNTY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_COUNTY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_COUNTY_NBR';
end;

class function TSY_COUNTY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'COUNTY_NAME';
  Result[1] := 'SY_STATES_NBR';
end;

{TSY_DELIVERY_METHOD}

class function TSY_DELIVERY_METHOD.GetClassIndex: Integer;
begin
  Result := 2;
end;

class function TSY_DELIVERY_METHOD.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_DELIVERY_METHOD.GetEvTableNbr: Integer;
begin
  Result := 2;
end;

class function TSY_DELIVERY_METHOD.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_DELIVERY_METHOD.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_DELIVERY_METHOD.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 9
  else if AFieldName = 'SY_DELIVERY_METHOD_NBR' then Result := 8
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 10
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 13
  else if AFieldName = 'SY_DELIVERY_SERVICE_NBR' then Result := 7
  else if AFieldName = 'CLASS_NAME' then Result := 12
  else if AFieldName = 'NAME' then Result := 11
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_DELIVERY_METHOD.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_DELIVERY_METHOD.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_DELIVERY_METHOD_NBR';
  Result[1] := 'SY_DELIVERY_SERVICE_NBR';
  Result[2] := 'CLASS_NAME';
  Result[3] := 'NAME';
end;

class function TSY_DELIVERY_METHOD.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_DELIVERY_METHOD.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_DELIVERY_METHOD.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_DELIVERY_METHOD_NBR';
end;

class function TSY_DELIVERY_METHOD.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_DELIVERY_SERVICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_DELIVERY_SERVICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_DELIVERY_SERVICE_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_DELIVERY_METHOD.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_DELIVERY_METHOD.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_DELIVERY_SERVICE}

class function TSY_DELIVERY_SERVICE.GetClassIndex: Integer;
begin
  Result := 3;
end;

class function TSY_DELIVERY_SERVICE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_DELIVERY_SERVICE.GetEvTableNbr: Integer;
begin
  Result := 3;
end;

class function TSY_DELIVERY_SERVICE.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_DELIVERY_SERVICE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_DELIVERY_SERVICE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 14
  else if AFieldName = 'SY_DELIVERY_SERVICE_NBR' then Result := 15
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 18
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 19
  else if AFieldName = 'CLASS_NAME' then Result := 17
  else if AFieldName = 'NAME' then Result := 16
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_DELIVERY_SERVICE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_DELIVERY_SERVICE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_DELIVERY_SERVICE_NBR';
  Result[1] := 'CLASS_NAME';
  Result[2] := 'NAME';
end;

class function TSY_DELIVERY_SERVICE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_DELIVERY_SERVICE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_DELIVERY_SERVICE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_DELIVERY_SERVICE_NBR';
end;

class function TSY_DELIVERY_SERVICE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_DELIVERY_SERVICE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_DELIVERY_METHOD;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_DELIVERY_SERVICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_DELIVERY_SERVICE_NBR';
end;

class function TSY_DELIVERY_SERVICE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_FED_EXEMPTIONS}

class function TSY_FED_EXEMPTIONS.GetClassIndex: Integer;
begin
  Result := 4;
end;

class function TSY_FED_EXEMPTIONS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_FED_EXEMPTIONS.GetEvTableNbr: Integer;
begin
  Result := 4;
end;

class function TSY_FED_EXEMPTIONS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_EXEMPTIONS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := True
  else if FieldName = 'EXEMPT_FEDERAL' then Result := True
  else if FieldName = 'EXEMPT_FUI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYEE_OASDI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYER_OASDI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYEE_MEDICARE' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYER_MEDICARE' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYEE_EIC' then Result := True
  else if FieldName = 'W2_BOX' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_EXEMPTIONS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 20
  else if AFieldName = 'SY_FED_EXEMPTIONS_NBR' then Result := 22
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 31
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 32
  else if AFieldName = 'E_D_CODE_TYPE' then Result := 21
  else if AFieldName = 'EXEMPT_FEDERAL' then Result := 30
  else if AFieldName = 'EXEMPT_FUI' then Result := 29
  else if AFieldName = 'EXEMPT_EMPLOYEE_OASDI' then Result := 28
  else if AFieldName = 'EXEMPT_EMPLOYER_OASDI' then Result := 27
  else if AFieldName = 'EXEMPT_EMPLOYEE_MEDICARE' then Result := 26
  else if AFieldName = 'EXEMPT_EMPLOYER_MEDICARE' then Result := 25
  else if AFieldName = 'EXEMPT_EMPLOYEE_EIC' then Result := 24
  else if AFieldName = 'W2_BOX' then Result := 23
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_EXEMPTIONS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_EXEMPTIONS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 9);
  Result[0] := 'SY_FED_EXEMPTIONS_NBR';
  Result[1] := 'E_D_CODE_TYPE';
  Result[2] := 'EXEMPT_FEDERAL';
  Result[3] := 'EXEMPT_FUI';
  Result[4] := 'EXEMPT_EMPLOYEE_OASDI';
  Result[5] := 'EXEMPT_EMPLOYER_OASDI';
  Result[6] := 'EXEMPT_EMPLOYEE_MEDICARE';
  Result[7] := 'EXEMPT_EMPLOYER_MEDICARE';
  Result[8] := 'EXEMPT_EMPLOYEE_EIC';
end;

class function TSY_FED_EXEMPTIONS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := 2
  else if FieldName = 'EXEMPT_FEDERAL' then Result := 1
  else if FieldName = 'EXEMPT_FUI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYEE_OASDI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYER_OASDI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYEE_MEDICARE' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYER_MEDICARE' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYEE_EIC' then Result := 1
  else if FieldName = 'W2_BOX' then Result := 4
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_EXEMPTIONS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_EXEMPTIONS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_EXEMPTIONS_NBR';
end;

class function TSY_FED_EXEMPTIONS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_FED_EXEMPTIONS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_EXEMPTIONS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'E_D_CODE_TYPE';
end;

{TSY_FED_REPORTING_AGENCY}

class function TSY_FED_REPORTING_AGENCY.GetClassIndex: Integer;
begin
  Result := 5;
end;

class function TSY_FED_REPORTING_AGENCY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_FED_REPORTING_AGENCY.GetEvTableNbr: Integer;
begin
  Result := 5;
end;

class function TSY_FED_REPORTING_AGENCY.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_REPORTING_AGENCY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_REPORTING_AGENCY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 33
  else if AFieldName = 'SY_FED_REPORTING_AGENCY_NBR' then Result := 35
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 36
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 37
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 34
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_REPORTING_AGENCY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_REPORTING_AGENCY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_FED_REPORTING_AGENCY_NBR';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
end;

class function TSY_FED_REPORTING_AGENCY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_REPORTING_AGENCY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_REPORTING_AGENCY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_REPORTING_AGENCY_NBR';
end;

class function TSY_FED_REPORTING_AGENCY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_FED_REPORTING_AGENCY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_REPORTING_AGENCY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_GLOBAL_AGENCY_NBR';
end;

{TSY_FED_TAX_PAYMENT_AGENCY}

class function TSY_FED_TAX_PAYMENT_AGENCY.GetClassIndex: Integer;
begin
  Result := 6;
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetEvTableNbr: Integer;
begin
  Result := 6;
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 38
  else if AFieldName = 'SY_FED_TAX_PAYMENT_AGENCY_NBR' then Result := 41
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 42
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 43
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 40
  else if AFieldName = 'SY_REPORTS_NBR' then Result := 39
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_FED_TAX_PAYMENT_AGENCY_NBR';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
  Result[2] := 'SY_REPORTS_NBR';
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_TAX_PAYMENT_AGENCY_NBR';
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_TAX_PAYMENT_AGENCY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[1] := 'SY_REPORTS_NBR';
end;

{TSY_FED_TAX_TABLE}

class function TSY_FED_TAX_TABLE.GetClassIndex: Integer;
begin
  Result := 7;
end;

class function TSY_FED_TAX_TABLE.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_FED_TAX_TABLE.GetEvTableNbr: Integer;
begin
  Result := 7;
end;

class function TSY_FED_TAX_TABLE.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_TAX_TABLE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'EXEMPTION_AMOUNT' then Result := True
  else if FieldName = 'FEDERAL_MINIMUM_WAGE' then Result := True
  else if FieldName = 'FEDERAL_TIP_CREDIT' then Result := True
  else if FieldName = 'SUPPLEMENTAL_TAX_PERCENTAGE' then Result := True
  else if FieldName = 'OASDI_RATE' then Result := True
  else if FieldName = 'ER_OASDI_RATE' then Result := True
  else if FieldName = 'EE_OASDI_RATE' then Result := True
  else if FieldName = 'OASDI_WAGE_LIMIT' then Result := True
  else if FieldName = 'MEDICARE_RATE' then Result := True
  else if FieldName = 'MEDICARE_WAGE_LIMIT' then Result := True
  else if FieldName = 'FUI_RATE_REAL' then Result := True
  else if FieldName = 'FUI_RATE_CREDIT' then Result := True
  else if FieldName = 'FUI_WAGE_LIMIT' then Result := True
  else if FieldName = 'SY_401K_LIMIT' then Result := True
  else if FieldName = 'SY_403B_LIMIT' then Result := True
  else if FieldName = 'SY_457_LIMIT' then Result := True
  else if FieldName = 'SY_501C_LIMIT' then Result := True
  else if FieldName = 'SIMPLE_LIMIT' then Result := True
  else if FieldName = 'SEP_LIMIT' then Result := True
  else if FieldName = 'DEFERRED_COMP_LIMIT' then Result := True
  else if FieldName = 'FED_DEPOSIT_FREQ_THRESHOLD' then Result := True
  else if FieldName = 'FIRST_EIC_LIMIT' then Result := True
  else if FieldName = 'FIRST_EIC_PERCENTAGE' then Result := True
  else if FieldName = 'SECOND_EIC_LIMIT' then Result := True
  else if FieldName = 'SECOND_EIC_AMOUNT' then Result := True
  else if FieldName = 'THIRD_EIC_ADDITIONAL_PERCENT' then Result := True
  else if FieldName = 'SY_CATCH_UP_LIMIT' then Result := True
  else if FieldName = 'SY_DEPENDENT_CARE_LIMIT' then Result := True
  else if FieldName = 'SY_HSA_SINGLE_LIMIT' then Result := True
  else if FieldName = 'SY_HSA_FAMILY_LIMIT' then Result := True
  else if FieldName = 'SY_PENSION_CATCH_UP_LIMIT' then Result := True
  else if FieldName = 'SY_SIMPLE_CATCH_UP_LIMIT' then Result := True
  else if FieldName = 'COMPENSATION_LIMIT' then Result := True
  else if FieldName = 'SY_HSA_CATCH_UP_LIMIT' then Result := True
  else if FieldName = 'S132_PARKING_LIMIT' then Result := True
  else if FieldName = 'ROTH_IRA_LIMIT' then Result := True
  else if FieldName = 'SY_IRA_CATCHUP_LIMIT' then Result := True
  else if FieldName = 'EE_MED_TH_LIMIT' then Result := True
  else if FieldName = 'EE_MED_TH_RATE' then Result := True
  else if FieldName = 'FSA_LIMIT' then Result := True
  else if FieldName = 'FED_POVERTY_LEVEL' then Result := True
  else if FieldName = 'ACA_AFFORD_PERCENT' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_TAX_TABLE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 79
  else if AFieldName = 'SY_FED_TAX_TABLE_NBR' then Result := 80
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 82
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 83
  else if AFieldName = 'FILLER' then Result := 81
  else if AFieldName = 'EXEMPTION_AMOUNT' then Result := 78
  else if AFieldName = 'FEDERAL_MINIMUM_WAGE' then Result := 77
  else if AFieldName = 'FEDERAL_TIP_CREDIT' then Result := 76
  else if AFieldName = 'SUPPLEMENTAL_TAX_PERCENTAGE' then Result := 75
  else if AFieldName = 'OASDI_RATE' then Result := 74
  else if AFieldName = 'ER_OASDI_RATE' then Result := 532
  else if AFieldName = 'EE_OASDI_RATE' then Result := 531
  else if AFieldName = 'OASDI_WAGE_LIMIT' then Result := 73
  else if AFieldName = 'MEDICARE_RATE' then Result := 72
  else if AFieldName = 'MEDICARE_WAGE_LIMIT' then Result := 71
  else if AFieldName = 'FUI_RATE_REAL' then Result := 70
  else if AFieldName = 'FUI_RATE_CREDIT' then Result := 69
  else if AFieldName = 'FUI_WAGE_LIMIT' then Result := 68
  else if AFieldName = 'SY_401K_LIMIT' then Result := 67
  else if AFieldName = 'SY_403B_LIMIT' then Result := 66
  else if AFieldName = 'SY_457_LIMIT' then Result := 65
  else if AFieldName = 'SY_501C_LIMIT' then Result := 64
  else if AFieldName = 'SIMPLE_LIMIT' then Result := 63
  else if AFieldName = 'SEP_LIMIT' then Result := 62
  else if AFieldName = 'DEFERRED_COMP_LIMIT' then Result := 61
  else if AFieldName = 'FED_DEPOSIT_FREQ_THRESHOLD' then Result := 60
  else if AFieldName = 'FIRST_EIC_LIMIT' then Result := 59
  else if AFieldName = 'FIRST_EIC_PERCENTAGE' then Result := 58
  else if AFieldName = 'SECOND_EIC_LIMIT' then Result := 57
  else if AFieldName = 'SECOND_EIC_AMOUNT' then Result := 56
  else if AFieldName = 'THIRD_EIC_ADDITIONAL_PERCENT' then Result := 55
  else if AFieldName = 'SY_CATCH_UP_LIMIT' then Result := 54
  else if AFieldName = 'SY_DEPENDENT_CARE_LIMIT' then Result := 53
  else if AFieldName = 'SY_HSA_SINGLE_LIMIT' then Result := 52
  else if AFieldName = 'SY_HSA_FAMILY_LIMIT' then Result := 51
  else if AFieldName = 'SY_PENSION_CATCH_UP_LIMIT' then Result := 50
  else if AFieldName = 'SY_SIMPLE_CATCH_UP_LIMIT' then Result := 49
  else if AFieldName = 'COMPENSATION_LIMIT' then Result := 48
  else if AFieldName = 'SY_HSA_CATCH_UP_LIMIT' then Result := 47
  else if AFieldName = 'S132_PARKING_LIMIT' then Result := 46
  else if AFieldName = 'ROTH_IRA_LIMIT' then Result := 45
  else if AFieldName = 'SY_IRA_CATCHUP_LIMIT' then Result := 44
  else if AFieldName = 'EE_MED_TH_LIMIT' then Result := 561
  else if AFieldName = 'EE_MED_TH_RATE' then Result := 562
  else if AFieldName = 'FSA_LIMIT' then Result := 583
  else if AFieldName = 'FED_POVERTY_LEVEL' then Result := 618
  else if AFieldName = 'ACA_AFFORD_PERCENT' then Result := 620
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_TAX_TABLE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_TAX_TABLE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 8);
  Result[0] := 'SY_FED_TAX_TABLE_NBR';
  Result[1] := 'SY_CATCH_UP_LIMIT';
  Result[2] := 'SY_DEPENDENT_CARE_LIMIT';
  Result[3] := 'SY_HSA_SINGLE_LIMIT';
  Result[4] := 'SY_HSA_FAMILY_LIMIT';
  Result[5] := 'SY_PENSION_CATCH_UP_LIMIT';
  Result[6] := 'SY_SIMPLE_CATCH_UP_LIMIT';
  Result[7] := 'SY_HSA_CATCH_UP_LIMIT';
end;

class function TSY_FED_TAX_TABLE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_TAX_TABLE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'EXEMPTION_AMOUNT' then Result := 6
  else if FieldName = 'FEDERAL_MINIMUM_WAGE' then Result := 6
  else if FieldName = 'FEDERAL_TIP_CREDIT' then Result := 6
  else if FieldName = 'SUPPLEMENTAL_TAX_PERCENTAGE' then Result := 6
  else if FieldName = 'OASDI_RATE' then Result := 6
  else if FieldName = 'ER_OASDI_RATE' then Result := 6
  else if FieldName = 'EE_OASDI_RATE' then Result := 6
  else if FieldName = 'OASDI_WAGE_LIMIT' then Result := 6
  else if FieldName = 'MEDICARE_RATE' then Result := 6
  else if FieldName = 'MEDICARE_WAGE_LIMIT' then Result := 6
  else if FieldName = 'FUI_RATE_REAL' then Result := 6
  else if FieldName = 'FUI_RATE_CREDIT' then Result := 6
  else if FieldName = 'FUI_WAGE_LIMIT' then Result := 6
  else if FieldName = 'SY_401K_LIMIT' then Result := 6
  else if FieldName = 'SY_403B_LIMIT' then Result := 6
  else if FieldName = 'SY_457_LIMIT' then Result := 6
  else if FieldName = 'SY_501C_LIMIT' then Result := 6
  else if FieldName = 'SIMPLE_LIMIT' then Result := 6
  else if FieldName = 'SEP_LIMIT' then Result := 6
  else if FieldName = 'DEFERRED_COMP_LIMIT' then Result := 6
  else if FieldName = 'FED_DEPOSIT_FREQ_THRESHOLD' then Result := 6
  else if FieldName = 'FIRST_EIC_LIMIT' then Result := 6
  else if FieldName = 'FIRST_EIC_PERCENTAGE' then Result := 6
  else if FieldName = 'SECOND_EIC_LIMIT' then Result := 6
  else if FieldName = 'SECOND_EIC_AMOUNT' then Result := 6
  else if FieldName = 'THIRD_EIC_ADDITIONAL_PERCENT' then Result := 6
  else if FieldName = 'SY_CATCH_UP_LIMIT' then Result := 6
  else if FieldName = 'SY_DEPENDENT_CARE_LIMIT' then Result := 6
  else if FieldName = 'SY_HSA_SINGLE_LIMIT' then Result := 6
  else if FieldName = 'SY_HSA_FAMILY_LIMIT' then Result := 6
  else if FieldName = 'SY_PENSION_CATCH_UP_LIMIT' then Result := 6
  else if FieldName = 'SY_SIMPLE_CATCH_UP_LIMIT' then Result := 6
  else if FieldName = 'COMPENSATION_LIMIT' then Result := 6
  else if FieldName = 'SY_HSA_CATCH_UP_LIMIT' then Result := 6
  else if FieldName = 'S132_PARKING_LIMIT' then Result := 6
  else if FieldName = 'ROTH_IRA_LIMIT' then Result := 6
  else if FieldName = 'SY_IRA_CATCHUP_LIMIT' then Result := 6
  else if FieldName = 'EE_MED_TH_LIMIT' then Result := 6
  else if FieldName = 'EE_MED_TH_RATE' then Result := 6
  else if FieldName = 'FSA_LIMIT' then Result := 6
  else if FieldName = 'FED_POVERTY_LEVEL' then Result := 6
  else if FieldName = 'ACA_AFFORD_PERCENT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_TAX_TABLE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_TAX_TABLE_NBR';
end;

class function TSY_FED_TAX_TABLE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_FED_TAX_TABLE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_TAX_TABLE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_FED_TAX_TABLE_BRACKETS}

class function TSY_FED_TAX_TABLE_BRACKETS.GetClassIndex: Integer;
begin
  Result := 8;
end;

class function TSY_FED_TAX_TABLE_BRACKETS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetEvTableNbr: Integer;
begin
  Result := 8;
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_TAX_TABLE_BRACKETS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'MARITAL_STATUS' then Result := True
  else if FieldName = 'GREATER_THAN_VALUE' then Result := True
  else if FieldName = 'LESS_THAN_VALUE' then Result := True
  else if FieldName = 'PERCENTAGE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 87
  else if AFieldName = 'SY_FED_TAX_TABLE_BRACKETS_NBR' then Result := 88
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 90
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 91
  else if AFieldName = 'MARITAL_STATUS' then Result := 89
  else if AFieldName = 'GREATER_THAN_VALUE' then Result := 86
  else if AFieldName = 'LESS_THAN_VALUE' then Result := 85
  else if AFieldName = 'PERCENTAGE' then Result := 84
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_FED_TAX_TABLE_BRACKETS_NBR';
  Result[1] := 'MARITAL_STATUS';
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MARITAL_STATUS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'GREATER_THAN_VALUE' then Result := 6
  else if FieldName = 'LESS_THAN_VALUE' then Result := 6
  else if FieldName = 'PERCENTAGE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_TAX_TABLE_BRACKETS_NBR';
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_TAX_TABLE_BRACKETS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_GLOBAL_AGENCY}

class function TSY_GLOBAL_AGENCY.GetClassIndex: Integer;
begin
  Result := 9;
end;

class function TSY_GLOBAL_AGENCY.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_GLOBAL_AGENCY.GetEvTableNbr: Integer;
begin
  Result := 9;
end;

class function TSY_GLOBAL_AGENCY.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_GLOBAL_AGENCY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'AGENCY_NAME' then Result := True
  else if FieldName = 'ACCEPTS_CHECK' then Result := True
  else if FieldName = 'ACCEPTS_DEBIT' then Result := True
  else if FieldName = 'ACCEPTS_CREDIT' then Result := True
  else if FieldName = 'NEGATIVE_DIRECT_DEP_ALLOWED' then Result := True
  else if FieldName = 'IGNORE_HOLIDAYS' then Result := True
  else if FieldName = 'IGNORE_WEEKENDS' then Result := True
  else if FieldName = 'PAYMENT_FILE_DEST_DIRECT' then Result := True
  else if FieldName = 'RECEIVING_ABA_NUMBER' then Result := True
  else if FieldName = 'RECEIVING_ACCOUNT_NUMBER' then Result := True
  else if FieldName = 'RECEIVING_ACCOUNT_TYPE' then Result := True
  else if FieldName = 'MAG_MEDIA' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'STATE' then Result := True
  else if FieldName = 'COUNTY' then Result := True
  else if FieldName = 'TAX_PAYMENT_FIELD_OFFICE_NBR' then Result := True
  else if FieldName = 'NOTES' then Result := True
  else if FieldName = 'SEND_ZERO_EFT' then Result := True
  else if FieldName = 'SEND_ZERO_COUPON' then Result := True
  else if FieldName = 'PRINT_ZERO_RETURN' then Result := True
  else if FieldName = 'CUSTOM_DEBIT_SY_REPORTS_NBR' then Result := True
  else if FieldName = 'CUSTOM_DEBIT_AFTER_STATUS' then Result := True
  else if FieldName = 'CUSTOM_DEBIT_POST_TO_REGISTER' then Result := True
  else if FieldName = 'CHECK_ONE_PAYMENT_TYPE' then Result := True
  else if FieldName = 'EFTP_ONE_PAYMENT_TYPE' then Result := True
  else if FieldName = 'ACH_ONE_PAYMENT_TYPE' then Result := True
  else if FieldName = 'AGENCY_TYPE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_GLOBAL_AGENCY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 93
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 96
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 120
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 121
  else if AFieldName = 'AGENCY_NAME' then Result := 102
  else if AFieldName = 'ACCEPTS_CHECK' then Result := 119
  else if AFieldName = 'ACCEPTS_DEBIT' then Result := 118
  else if AFieldName = 'ACCEPTS_CREDIT' then Result := 117
  else if AFieldName = 'NEGATIVE_DIRECT_DEP_ALLOWED' then Result := 116
  else if AFieldName = 'IGNORE_HOLIDAYS' then Result := 115
  else if AFieldName = 'IGNORE_WEEKENDS' then Result := 114
  else if AFieldName = 'PAYMENT_FILE_DEST_DIRECT' then Result := 101
  else if AFieldName = 'RECEIVING_ABA_NUMBER' then Result := 113
  else if AFieldName = 'RECEIVING_ACCOUNT_NUMBER' then Result := 100
  else if AFieldName = 'RECEIVING_ACCOUNT_TYPE' then Result := 112
  else if AFieldName = 'MAG_MEDIA' then Result := 111
  else if AFieldName = 'FILLER' then Result := 99
  else if AFieldName = 'STATE' then Result := 98
  else if AFieldName = 'COUNTY' then Result := 97
  else if AFieldName = 'TAX_PAYMENT_FIELD_OFFICE_NBR' then Result := 95
  else if AFieldName = 'NOTES' then Result := 92
  else if AFieldName = 'SEND_ZERO_EFT' then Result := 110
  else if AFieldName = 'SEND_ZERO_COUPON' then Result := 109
  else if AFieldName = 'PRINT_ZERO_RETURN' then Result := 108
  else if AFieldName = 'CUSTOM_DEBIT_SY_REPORTS_NBR' then Result := 94
  else if AFieldName = 'CUSTOM_DEBIT_AFTER_STATUS' then Result := 107
  else if AFieldName = 'CUSTOM_DEBIT_POST_TO_REGISTER' then Result := 106
  else if AFieldName = 'CHECK_ONE_PAYMENT_TYPE' then Result := 105
  else if AFieldName = 'EFTP_ONE_PAYMENT_TYPE' then Result := 104
  else if AFieldName = 'ACH_ONE_PAYMENT_TYPE' then Result := 103
  else if AFieldName = 'AGENCY_TYPE' then Result := 560
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_GLOBAL_AGENCY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_GLOBAL_AGENCY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 19);
  Result[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[1] := 'AGENCY_NAME';
  Result[2] := 'ACCEPTS_CHECK';
  Result[3] := 'ACCEPTS_DEBIT';
  Result[4] := 'ACCEPTS_CREDIT';
  Result[5] := 'NEGATIVE_DIRECT_DEP_ALLOWED';
  Result[6] := 'IGNORE_HOLIDAYS';
  Result[7] := 'IGNORE_WEEKENDS';
  Result[8] := 'RECEIVING_ACCOUNT_TYPE';
  Result[9] := 'MAG_MEDIA';
  Result[10] := 'SEND_ZERO_EFT';
  Result[11] := 'SEND_ZERO_COUPON';
  Result[12] := 'PRINT_ZERO_RETURN';
  Result[13] := 'CUSTOM_DEBIT_AFTER_STATUS';
  Result[14] := 'CUSTOM_DEBIT_POST_TO_REGISTER';
  Result[15] := 'CHECK_ONE_PAYMENT_TYPE';
  Result[16] := 'EFTP_ONE_PAYMENT_TYPE';
  Result[17] := 'ACH_ONE_PAYMENT_TYPE';
  Result[18] := 'AGENCY_TYPE';
end;

class function TSY_GLOBAL_AGENCY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'AGENCY_NAME' then Result := 40
  else if FieldName = 'ACCEPTS_CHECK' then Result := 1
  else if FieldName = 'ACCEPTS_DEBIT' then Result := 1
  else if FieldName = 'ACCEPTS_CREDIT' then Result := 1
  else if FieldName = 'NEGATIVE_DIRECT_DEP_ALLOWED' then Result := 1
  else if FieldName = 'IGNORE_HOLIDAYS' then Result := 1
  else if FieldName = 'IGNORE_WEEKENDS' then Result := 1
  else if FieldName = 'PAYMENT_FILE_DEST_DIRECT' then Result := 64
  else if FieldName = 'RECEIVING_ABA_NUMBER' then Result := 9
  else if FieldName = 'RECEIVING_ACCOUNT_NUMBER' then Result := 20
  else if FieldName = 'RECEIVING_ACCOUNT_TYPE' then Result := 1
  else if FieldName = 'MAG_MEDIA' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'COUNTY' then Result := 40
  else if FieldName = 'SEND_ZERO_EFT' then Result := 1
  else if FieldName = 'SEND_ZERO_COUPON' then Result := 1
  else if FieldName = 'PRINT_ZERO_RETURN' then Result := 1
  else if FieldName = 'CUSTOM_DEBIT_AFTER_STATUS' then Result := 1
  else if FieldName = 'CUSTOM_DEBIT_POST_TO_REGISTER' then Result := 1
  else if FieldName = 'CHECK_ONE_PAYMENT_TYPE' then Result := 1
  else if FieldName = 'EFTP_ONE_PAYMENT_TYPE' then Result := 1
  else if FieldName = 'ACH_ONE_PAYMENT_TYPE' then Result := 1
  else if FieldName = 'AGENCY_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_GLOBAL_AGENCY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_GLOBAL_AGENCY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_GLOBAL_AGENCY_NBR';
end;

class function TSY_GLOBAL_AGENCY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'TAX_PAYMENT_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'CUSTOM_DEBIT_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_GLOBAL_AGENCY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_TAX_PMT_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_REPORTING_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_REPORTING_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_TAX_PMT_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_FED_REPORTING_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_FED_TAX_PAYMENT_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_SUI_REPORTING_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_SUI_TAX_PMT_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_AGENCY_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_HOLIDAYS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
end;

class function TSY_GLOBAL_AGENCY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'AGENCY_NAME';
end;

{TSY_GL_AGENCY_FIELD_OFFICE}

class function TSY_GL_AGENCY_FIELD_OFFICE.GetClassIndex: Integer;
begin
  Result := 10;
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetEvTableNbr: Integer;
begin
  Result := 10;
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ADDITIONAL_NAME' then Result := True
  else if FieldName = 'ADDRESS1' then Result := True
  else if FieldName = 'ADDRESS2' then Result := True
  else if FieldName = 'CITY' then Result := True
  else if FieldName = 'ZIP_CODE' then Result := True
  else if FieldName = 'STATE' then Result := True
  else if FieldName = 'ATTENTION_NAME' then Result := True
  else if FieldName = 'CONTACT1' then Result := True
  else if FieldName = 'PHONE1' then Result := True
  else if FieldName = 'DESCRIPTION1' then Result := True
  else if FieldName = 'CONTACT2' then Result := True
  else if FieldName = 'PHONE2' then Result := True
  else if FieldName = 'DESCRIPTION2' then Result := True
  else if FieldName = 'FAX' then Result := True
  else if FieldName = 'FAX_DESCRIPTION' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'CATEGORY_TYPE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 126
  else if AFieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := 132
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 142
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 143
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 131
  else if AFieldName = 'ADDITIONAL_NAME' then Result := 140
  else if AFieldName = 'ADDRESS1' then Result := 125
  else if AFieldName = 'ADDRESS2' then Result := 124
  else if AFieldName = 'CITY' then Result := 139
  else if AFieldName = 'ZIP_CODE' then Result := 130
  else if AFieldName = 'STATE' then Result := 138
  else if AFieldName = 'ATTENTION_NAME' then Result := 137
  else if AFieldName = 'CONTACT1' then Result := 123
  else if AFieldName = 'PHONE1' then Result := 136
  else if AFieldName = 'DESCRIPTION1' then Result := 129
  else if AFieldName = 'CONTACT2' then Result := 122
  else if AFieldName = 'PHONE2' then Result := 135
  else if AFieldName = 'DESCRIPTION2' then Result := 128
  else if AFieldName = 'FAX' then Result := 134
  else if AFieldName = 'FAX_DESCRIPTION' then Result := 127
  else if AFieldName = 'FILLER' then Result := 133
  else if AFieldName = 'CATEGORY_TYPE' then Result := 141
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
  Result[2] := 'ADDRESS1';
  Result[3] := 'CITY';
  Result[4] := 'ZIP_CODE';
  Result[5] := 'STATE';
  Result[6] := 'CATEGORY_TYPE';
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ADDITIONAL_NAME' then Result := 40
  else if FieldName = 'ADDRESS1' then Result := 30
  else if FieldName = 'ADDRESS2' then Result := 30
  else if FieldName = 'CITY' then Result := 20
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'ATTENTION_NAME' then Result := 40
  else if FieldName = 'CONTACT1' then Result := 30
  else if FieldName = 'PHONE1' then Result := 20
  else if FieldName = 'DESCRIPTION1' then Result := 10
  else if FieldName = 'CONTACT2' then Result := 30
  else if FieldName = 'PHONE2' then Result := 20
  else if FieldName = 'DESCRIPTION2' then Result := 10
  else if FieldName = 'FAX' then Result := 20
  else if FieldName = 'FAX_DESCRIPTION' then Result := 10
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'CATEGORY_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_REPORT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'TAX_PAYMENT_FIELD_OFFICE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'QE_SY_GL_AGENCY_OFFICE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'QE_SY_GL_AGENCY_OFFICE_NBR';
end;

class function TSY_GL_AGENCY_FIELD_OFFICE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'ADDITIONAL_NAME';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
end;

{TSY_GL_AGENCY_HOLIDAYS}

class function TSY_GL_AGENCY_HOLIDAYS.GetClassIndex: Integer;
begin
  Result := 11;
end;

class function TSY_GL_AGENCY_HOLIDAYS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetEvTableNbr: Integer;
begin
  Result := 11;
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_GL_AGENCY_HOLIDAYS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'HOLIDAY_NAME' then Result := True
  else if FieldName = 'CALC_TYPE' then Result := True
  else if FieldName = 'WEEK_NUMBER' then Result := True
  else if FieldName = 'DAY_OF_WEEK' then Result := True
  else if FieldName = 'MONTH_NUMBER' then Result := True
  else if FieldName = 'DAY_NUMBER' then Result := True
  else if FieldName = 'SATURDAY_OFFSET' then Result := True
  else if FieldName = 'SUNDAY_OFFSET' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 144
  else if AFieldName = 'SY_GL_AGENCY_HOLIDAYS_NBR' then Result := 145
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 146
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 149
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 147
  else if AFieldName = 'HOLIDAY_NAME' then Result := 148
  else if AFieldName = 'CALC_TYPE' then Result := 542
  else if AFieldName = 'WEEK_NUMBER' then Result := 543
  else if AFieldName = 'DAY_OF_WEEK' then Result := 544
  else if AFieldName = 'MONTH_NUMBER' then Result := 545
  else if AFieldName = 'DAY_NUMBER' then Result := 546
  else if AFieldName = 'SATURDAY_OFFSET' then Result := 547
  else if AFieldName = 'SUNDAY_OFFSET' then Result := 548
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_GL_AGENCY_HOLIDAYS_NBR';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
  Result[2] := 'HOLIDAY_NAME';
  Result[3] := 'CALC_TYPE';
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'HOLIDAY_NAME' then Result := 40
  else if FieldName = 'CALC_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_GL_AGENCY_HOLIDAYS_NBR';
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_GL_AGENCY_HOLIDAYS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'HOLIDAY_NAME';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
end;

{TSY_GL_AGENCY_REPORT}

class function TSY_GL_AGENCY_REPORT.GetClassIndex: Integer;
begin
  Result := 12;
end;

class function TSY_GL_AGENCY_REPORT.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_GL_AGENCY_REPORT.GetEvTableNbr: Integer;
begin
  Result := 12;
end;

class function TSY_GL_AGENCY_REPORT.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_GL_AGENCY_REPORT.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SYSTEM_TAX_TYPE' then Result := True
  else if FieldName = 'DEPOSIT_FREQUENCY' then Result := True
  else if FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := True
  else if FieldName = 'WHEN_DUE_TYPE' then Result := True
  else if FieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := True
  else if FieldName = 'RETURN_FREQUENCY' then Result := True
  else if FieldName = 'ENLIST_AUTOMATICALLY' then Result := True
  else if FieldName = 'PRINT_WHEN' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'TAX_SERVICE_FILTER' then Result := True
  else if FieldName = 'CONSOLIDATED_FILTER' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else if FieldName = 'TAX_RETURN_ACTIVE' then Result := True
  else if FieldName = 'BEGIN_EFFECTIVE_MONTH' then Result := True
  else if FieldName = 'END_EFFECTIVE_MONTH' then Result := True
  else if FieldName = 'PAYMENT_METHOD' then Result := True
  else if FieldName = 'TAX945_CHECKS' then Result := True
  else if FieldName = 'TAX943_COMPANY' then Result := True
  else if FieldName = 'TAX944_COMPANY' then Result := True
  else if FieldName = 'TAXRETURN940' then Result := True
  else if FieldName = 'TAX_RETURN_INACTIVE_START_DATE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_GL_AGENCY_REPORT.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 151
  else if AFieldName = 'SY_GL_AGENCY_REPORT_NBR' then Result := 157
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 173
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 174
  else if AFieldName = 'SYSTEM_TAX_TYPE' then Result := 172
  else if AFieldName = 'DEPOSIT_FREQUENCY' then Result := 171
  else if AFieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := 156
  else if AFieldName = 'WHEN_DUE_TYPE' then Result := 170
  else if AFieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := 155
  else if AFieldName = 'RETURN_FREQUENCY' then Result := 169
  else if AFieldName = 'ENLIST_AUTOMATICALLY' then Result := 168
  else if AFieldName = 'PRINT_WHEN' then Result := 167
  else if AFieldName = 'FILLER' then Result := 158
  else if AFieldName = 'TAX_SERVICE_FILTER' then Result := 166
  else if AFieldName = 'CONSOLIDATED_FILTER' then Result := 165
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 154
  else if AFieldName = 'TAX_RETURN_ACTIVE' then Result := 164
  else if AFieldName = 'BEGIN_EFFECTIVE_MONTH' then Result := 153
  else if AFieldName = 'END_EFFECTIVE_MONTH' then Result := 152
  else if AFieldName = 'PAYMENT_METHOD' then Result := 163
  else if AFieldName = 'TAX945_CHECKS' then Result := 162
  else if AFieldName = 'TAX943_COMPANY' then Result := 161
  else if AFieldName = 'TAX944_COMPANY' then Result := 160
  else if AFieldName = 'TAXRETURN940' then Result := 159
  else if AFieldName = 'TAX_RETURN_INACTIVE_START_DATE' then Result := 150
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_GL_AGENCY_REPORT.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_GL_AGENCY_REPORT.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 16);
  Result[0] := 'SY_GL_AGENCY_REPORT_NBR';
  Result[1] := 'SYSTEM_TAX_TYPE';
  Result[2] := 'DEPOSIT_FREQUENCY';
  Result[3] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[4] := 'WHEN_DUE_TYPE';
  Result[5] := 'RETURN_FREQUENCY';
  Result[6] := 'ENLIST_AUTOMATICALLY';
  Result[7] := 'PRINT_WHEN';
  Result[8] := 'TAX_SERVICE_FILTER';
  Result[9] := 'CONSOLIDATED_FILTER';
  Result[10] := 'TAX_RETURN_ACTIVE';
  Result[11] := 'PAYMENT_METHOD';
  Result[12] := 'TAX945_CHECKS';
  Result[13] := 'TAX943_COMPANY';
  Result[14] := 'TAX944_COMPANY';
  Result[15] := 'TAXRETURN940';
end;

class function TSY_GL_AGENCY_REPORT.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SYSTEM_TAX_TYPE' then Result := 1
  else if FieldName = 'DEPOSIT_FREQUENCY' then Result := 1
  else if FieldName = 'WHEN_DUE_TYPE' then Result := 1
  else if FieldName = 'RETURN_FREQUENCY' then Result := 1
  else if FieldName = 'ENLIST_AUTOMATICALLY' then Result := 1
  else if FieldName = 'PRINT_WHEN' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'TAX_SERVICE_FILTER' then Result := 1
  else if FieldName = 'CONSOLIDATED_FILTER' then Result := 1
  else if FieldName = 'TAX_RETURN_ACTIVE' then Result := 1
  else if FieldName = 'PAYMENT_METHOD' then Result := 1
  else if FieldName = 'TAX945_CHECKS' then Result := 1
  else if FieldName = 'TAX943_COMPANY' then Result := 1
  else if FieldName = 'TAX944_COMPANY' then Result := 1
  else if FieldName = 'TAXRETURN940' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_GL_AGENCY_REPORT.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_GL_AGENCY_REPORT.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_GL_AGENCY_REPORT_NBR';
end;

class function TSY_GL_AGENCY_REPORT.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_GL_AGENCY_REPORT.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_REPORT_NBR';
end;

class function TSY_GL_AGENCY_REPORT.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_HR_EEO}

class function TSY_HR_EEO.GetClassIndex: Integer;
begin
  Result := 13;
end;

class function TSY_HR_EEO.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_EEO.GetEvTableNbr: Integer;
begin
  Result := 13;
end;

class function TSY_HR_EEO.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_EEO.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_EEO.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 175
  else if AFieldName = 'SY_HR_EEO_NBR' then Result := 176
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 178
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 179
  else if AFieldName = 'DESCRIPTION' then Result := 177
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_EEO.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_EEO.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_HR_EEO_NBR';
  Result[1] := 'DESCRIPTION';
end;

class function TSY_HR_EEO.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_EEO.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_EEO.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_EEO_NBR';
end;

class function TSY_HR_EEO.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_HR_EEO.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_EEO.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSY_HR_ETHNICITY}

class function TSY_HR_ETHNICITY.GetClassIndex: Integer;
begin
  Result := 14;
end;

class function TSY_HR_ETHNICITY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_ETHNICITY.GetEvTableNbr: Integer;
begin
  Result := 14;
end;

class function TSY_HR_ETHNICITY.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_ETHNICITY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_ETHNICITY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 180
  else if AFieldName = 'SY_HR_ETHNICITY_NBR' then Result := 181
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 183
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 184
  else if AFieldName = 'DESCRIPTION' then Result := 182
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_ETHNICITY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_ETHNICITY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_HR_ETHNICITY_NBR';
  Result[1] := 'DESCRIPTION';
end;

class function TSY_HR_ETHNICITY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_ETHNICITY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_ETHNICITY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_ETHNICITY_NBR';
end;

class function TSY_HR_ETHNICITY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_HR_ETHNICITY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_ETHNICITY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSY_HR_HANDICAPS}

class function TSY_HR_HANDICAPS.GetClassIndex: Integer;
begin
  Result := 15;
end;

class function TSY_HR_HANDICAPS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_HANDICAPS.GetEvTableNbr: Integer;
begin
  Result := 15;
end;

class function TSY_HR_HANDICAPS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_HANDICAPS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_HANDICAPS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 185
  else if AFieldName = 'SY_HR_HANDICAPS_NBR' then Result := 186
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 188
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 189
  else if AFieldName = 'DESCRIPTION' then Result := 187
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_HANDICAPS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_HANDICAPS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_HR_HANDICAPS_NBR';
  Result[1] := 'DESCRIPTION';
end;

class function TSY_HR_HANDICAPS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_HANDICAPS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_HANDICAPS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_HANDICAPS_NBR';
end;

class function TSY_HR_HANDICAPS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_HR_HANDICAPS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_HANDICAPS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSY_HR_INJURY_CODES}

class function TSY_HR_INJURY_CODES.GetClassIndex: Integer;
begin
  Result := 16;
end;

class function TSY_HR_INJURY_CODES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_INJURY_CODES.GetEvTableNbr: Integer;
begin
  Result := 16;
end;

class function TSY_HR_INJURY_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_INJURY_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_INJURY_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 191
  else if AFieldName = 'SY_HR_INJURY_CODES_NBR' then Result := 192
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 194
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 195
  else if AFieldName = 'DESCRIPTION' then Result := 193
  else if AFieldName = 'INJURY_CODE' then Result := 190
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_INJURY_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_INJURY_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_HR_INJURY_CODES_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'INJURY_CODE';
end;

class function TSY_HR_INJURY_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else if FieldName = 'INJURY_CODE' then Result := 6
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_INJURY_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_INJURY_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_INJURY_CODES_NBR';
end;

class function TSY_HR_INJURY_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_HR_INJURY_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_INJURY_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSY_HR_OSHA_ANATOMIC_CODES}

class function TSY_HR_OSHA_ANATOMIC_CODES.GetClassIndex: Integer;
begin
  Result := 17;
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetEvTableNbr: Integer;
begin
  Result := 17;
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 196
  else if AFieldName = 'SY_HR_OSHA_ANATOMIC_CODES_NBR' then Result := 197
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 199
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 200
  else if AFieldName = 'DESCRIPTION' then Result := 198
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_HR_OSHA_ANATOMIC_CODES_NBR';
  Result[1] := 'DESCRIPTION';
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_OSHA_ANATOMIC_CODES_NBR';
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_OSHA_ANATOMIC_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'DESCRIPTION';
end;

{TSY_LOCALS}

class function TSY_LOCALS.GetClassIndex: Integer;
begin
  Result := 18;
end;

class function TSY_LOCALS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_LOCALS.GetEvTableNbr: Integer;
begin
  Result := 18;
end;

class function TSY_LOCALS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_LOCALS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'NAME' then Result := True
  else if FieldName = 'LOCAL_TYPE' then Result := True
  else if FieldName = 'ZIP_CODE' then Result := True
  else if FieldName = 'LOCAL_TAX_IDENTIFIER' then Result := True
  else if FieldName = 'AGENCY_NUMBER' then Result := True
  else if FieldName = 'MINIMUM_HOURS_WORKED_PER' then Result := True
  else if FieldName = 'CALCULATION_METHOD' then Result := True
  else if FieldName = 'PRINT_RETURN_IF_ZERO' then Result := True
  else if FieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := True
  else if FieldName = 'SY_LOCAL_TAX_PMT_AGENCY_NBR' then Result := True
  else if FieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := True
  else if FieldName = 'SY_LOCAL_REPORTING_AGENCY_NBR' then Result := True
  else if FieldName = 'TAX_DEPOSIT_FOLLOW_STATE' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'LOCAL_ACTIVE' then Result := True
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := True
  else if FieldName = 'TAX_TYPE' then Result := True
  else if FieldName = 'TAX_FREQUENCY' then Result := True
  else if FieldName = 'LOCAL_MINIMUM_WAGE' then Result := True
  else if FieldName = 'TAX_RATE' then Result := True
  else if FieldName = 'TAX_AMOUNT' then Result := True
  else if FieldName = 'TAX_MAXIMUM' then Result := True
  else if FieldName = 'WAGE_MAXIMUM' then Result := True
  else if FieldName = 'WEEKLY_TAX_CAP' then Result := True
  else if FieldName = 'MINIMUM_HOURS_WORKED' then Result := True
  else if FieldName = 'MISCELLANEOUS_AMOUNT' then Result := True
  else if FieldName = 'W2_BOX' then Result := True
  else if FieldName = 'PAY_WITH_STATE' then Result := True
  else if FieldName = 'WAGE_MINIMUM' then Result := True
  else if FieldName = 'E_D_CODE_TYPE' then Result := True
  else if FieldName = 'QUARTERLY_MINIMUM_THRESHOLD' then Result := True
  else if FieldName = 'AGENCY_CODE' then Result := True
  else if FieldName = 'COMBINE_FOR_TAX_PAYMENTS' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_LOCALS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 211
  else if AFieldName = 'SY_LOCALS_NBR' then Result := 221
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 236
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 237
  else if AFieldName = 'NAME' then Result := 224
  else if AFieldName = 'LOCAL_TYPE' then Result := 235
  else if AFieldName = 'SY_STATES_NBR' then Result := 220
  else if AFieldName = 'ZIP_CODE' then Result := 219
  else if AFieldName = 'SY_COUNTY_NBR' then Result := 218
  else if AFieldName = 'LOCAL_TAX_IDENTIFIER' then Result := 223
  else if AFieldName = 'AGENCY_NUMBER' then Result := 217
  else if AFieldName = 'MINIMUM_HOURS_WORKED_PER' then Result := 234
  else if AFieldName = 'CALCULATION_METHOD' then Result := 233
  else if AFieldName = 'PRINT_RETURN_IF_ZERO' then Result := 232
  else if AFieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := 231
  else if AFieldName = 'SY_LOCAL_TAX_PMT_AGENCY_NBR' then Result := 216
  else if AFieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := 215
  else if AFieldName = 'SY_LOCAL_REPORTING_AGENCY_NBR' then Result := 214
  else if AFieldName = 'TAX_DEPOSIT_FOLLOW_STATE' then Result := 230
  else if AFieldName = 'FILLER' then Result := 222
  else if AFieldName = 'LOCAL_ACTIVE' then Result := 229
  else if AFieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 228
  else if AFieldName = 'TAX_TYPE' then Result := 227
  else if AFieldName = 'TAX_FREQUENCY' then Result := 226
  else if AFieldName = 'LOCAL_MINIMUM_WAGE' then Result := 210
  else if AFieldName = 'TAX_RATE' then Result := 209
  else if AFieldName = 'TAX_AMOUNT' then Result := 208
  else if AFieldName = 'TAX_MAXIMUM' then Result := 207
  else if AFieldName = 'WAGE_MAXIMUM' then Result := 206
  else if AFieldName = 'WEEKLY_TAX_CAP' then Result := 205
  else if AFieldName = 'MINIMUM_HOURS_WORKED' then Result := 204
  else if AFieldName = 'MISCELLANEOUS_AMOUNT' then Result := 203
  else if AFieldName = 'W2_BOX' then Result := 213
  else if AFieldName = 'PAY_WITH_STATE' then Result := 225
  else if AFieldName = 'WAGE_MINIMUM' then Result := 202
  else if AFieldName = 'E_D_CODE_TYPE' then Result := 212
  else if AFieldName = 'QUARTERLY_MINIMUM_THRESHOLD' then Result := 201
  else if AFieldName = 'AGENCY_CODE' then Result := 540
  else if AFieldName = 'COMBINE_FOR_TAX_PAYMENTS' then Result := 541
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_LOCALS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_LOCALS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 16);
  Result[0] := 'SY_LOCALS_NBR';
  Result[1] := 'NAME';
  Result[2] := 'LOCAL_TYPE';
  Result[3] := 'SY_STATES_NBR';
  Result[4] := 'AGENCY_NUMBER';
  Result[5] := 'MINIMUM_HOURS_WORKED_PER';
  Result[6] := 'CALCULATION_METHOD';
  Result[7] := 'PRINT_RETURN_IF_ZERO';
  Result[8] := 'USE_MISC_TAX_RETURN_CODE';
  Result[9] := 'TAX_DEPOSIT_FOLLOW_STATE';
  Result[10] := 'LOCAL_ACTIVE';
  Result[11] := 'ROUND_TO_NEAREST_DOLLAR';
  Result[12] := 'TAX_TYPE';
  Result[13] := 'TAX_FREQUENCY';
  Result[14] := 'PAY_WITH_STATE';
  Result[15] := 'COMBINE_FOR_TAX_PAYMENTS';
end;

class function TSY_LOCALS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else if FieldName = 'LOCAL_TYPE' then Result := 1
  else if FieldName = 'ZIP_CODE' then Result := 10
  else if FieldName = 'LOCAL_TAX_IDENTIFIER' then Result := 20
  else if FieldName = 'MINIMUM_HOURS_WORKED_PER' then Result := 1
  else if FieldName = 'CALCULATION_METHOD' then Result := 1
  else if FieldName = 'PRINT_RETURN_IF_ZERO' then Result := 1
  else if FieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := 1
  else if FieldName = 'TAX_DEPOSIT_FOLLOW_STATE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'LOCAL_ACTIVE' then Result := 1
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 1
  else if FieldName = 'TAX_TYPE' then Result := 1
  else if FieldName = 'TAX_FREQUENCY' then Result := 1
  else if FieldName = 'W2_BOX' then Result := 10
  else if FieldName = 'PAY_WITH_STATE' then Result := 1
  else if FieldName = 'E_D_CODE_TYPE' then Result := 2
  else if FieldName = 'AGENCY_CODE' then Result := 10
  else if FieldName = 'COMBINE_FOR_TAX_PAYMENTS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_LOCALS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'LOCAL_MINIMUM_WAGE' then Result := 6
  else if FieldName = 'TAX_RATE' then Result := 6
  else if FieldName = 'TAX_AMOUNT' then Result := 6
  else if FieldName = 'TAX_MAXIMUM' then Result := 6
  else if FieldName = 'WAGE_MAXIMUM' then Result := 6
  else if FieldName = 'WEEKLY_TAX_CAP' then Result := 6
  else if FieldName = 'MINIMUM_HOURS_WORKED' then Result := 6
  else if FieldName = 'MISCELLANEOUS_AMOUNT' then Result := 6
  else if FieldName = 'WAGE_MINIMUM' then Result := 6
  else if FieldName = 'QUARTERLY_MINIMUM_THRESHOLD' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_LOCALS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_LOCALS_NBR';
end;

class function TSY_LOCALS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_COUNTY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_COUNTY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_COUNTY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_REPORTING_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_TAX_PMT_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_LOCALS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_EXEMPTIONS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_TAX_CHART;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
end;

class function TSY_LOCALS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'NAME';
  Result[1] := 'SY_STATES_NBR';
end;

{TSY_LOCAL_DEPOSIT_FREQ}

class function TSY_LOCAL_DEPOSIT_FREQ.GetClassIndex: Integer;
begin
  Result := 19;
end;

class function TSY_LOCAL_DEPOSIT_FREQ.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetEvTableNbr: Integer;
begin
  Result := 19;
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_LOCAL_DEPOSIT_FREQ.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'WHEN_DUE_TYPE' then Result := True
  else if FieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := True
  else if FieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := True
  else if FieldName = 'DESCRIPTION' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'FREQUENCY_TYPE' then Result := True
  else if FieldName = 'HIDE_FROM_USER' then Result := True
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := True
  else if FieldName = 'INC_TAX_PAYMENT_CODE' then Result := True
  else if FieldName = 'CHANGE_FREQ_ON_THRESHOLD' then Result := True
  else if FieldName = 'FIRST_THRESHOLD_DEP_FREQ_NBR' then Result := True
  else if FieldName = 'FIRST_THRESHOLD_PERIOD' then Result := True
  else if FieldName = 'FIRST_THRESHOLD_AMOUNT' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_DEP_FREQ_NBR' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_PERIOD' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := True
  else if FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := True
  else if FieldName = 'QE_SY_GL_AGENCY_OFFICE_NBR' then Result := True
  else if FieldName = 'QE_TAX_COUPON_SY_REPORTS_NBR' then Result := True
  else if FieldName = 'PAY_AND_SHIFTBACK' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 240
  else if AFieldName = 'SY_LOCAL_DEPOSIT_FREQ_NBR' then Result := 251
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 263
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 264
  else if AFieldName = 'SY_LOCALS_NBR' then Result := 250
  else if AFieldName = 'WHEN_DUE_TYPE' then Result := 262
  else if AFieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := 249
  else if AFieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := 261
  else if AFieldName = 'DESCRIPTION' then Result := 253
  else if AFieldName = 'FILLER' then Result := 252
  else if AFieldName = 'FREQUENCY_TYPE' then Result := 260
  else if AFieldName = 'TAX_COUPON_SY_REPORTS_NBR' then Result := 248
  else if AFieldName = 'HIDE_FROM_USER' then Result := 259
  else if AFieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 247
  else if AFieldName = 'INC_TAX_PAYMENT_CODE' then Result := 258
  else if AFieldName = 'CHANGE_FREQ_ON_THRESHOLD' then Result := 257
  else if AFieldName = 'FIRST_THRESHOLD_DEP_FREQ_NBR' then Result := 246
  else if AFieldName = 'FIRST_THRESHOLD_PERIOD' then Result := 256
  else if AFieldName = 'FIRST_THRESHOLD_AMOUNT' then Result := 239
  else if AFieldName = 'SECOND_THRESHOLD_DEP_FREQ_NBR' then Result := 245
  else if AFieldName = 'SECOND_THRESHOLD_PERIOD' then Result := 255
  else if AFieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := 238
  else if AFieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := 244
  else if AFieldName = 'QE_SY_GL_AGENCY_OFFICE_NBR' then Result := 243
  else if AFieldName = 'QE_TAX_COUPON_SY_REPORTS_NBR' then Result := 242
  else if AFieldName = 'PAY_AND_SHIFTBACK' then Result := 254
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 241
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 13);
  Result[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[1] := 'SY_LOCALS_NBR';
  Result[2] := 'WHEN_DUE_TYPE';
  Result[3] := 'NUMBER_OF_DAYS_FOR_WHEN_DUE';
  Result[4] := 'THRD_MNTH_DUE_END_OF_NEXT_MNTH';
  Result[5] := 'DESCRIPTION';
  Result[6] := 'FREQUENCY_TYPE';
  Result[7] := 'HIDE_FROM_USER';
  Result[8] := 'INC_TAX_PAYMENT_CODE';
  Result[9] := 'CHANGE_FREQ_ON_THRESHOLD';
  Result[10] := 'FIRST_THRESHOLD_PERIOD';
  Result[11] := 'SECOND_THRESHOLD_PERIOD';
  Result[12] := 'PAY_AND_SHIFTBACK';
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'WHEN_DUE_TYPE' then Result := 1
  else if FieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 20
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'FREQUENCY_TYPE' then Result := 1
  else if FieldName = 'HIDE_FROM_USER' then Result := 1
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 10
  else if FieldName = 'INC_TAX_PAYMENT_CODE' then Result := 1
  else if FieldName = 'CHANGE_FREQ_ON_THRESHOLD' then Result := 1
  else if FieldName = 'FIRST_THRESHOLD_PERIOD' then Result := 1
  else if FieldName = 'SECOND_THRESHOLD_PERIOD' then Result := 1
  else if FieldName = 'PAY_AND_SHIFTBACK' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'FIRST_THRESHOLD_AMOUNT' then Result := 6
  else if FieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'FIRST_THRESHOLD_DEP_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SECOND_THRESHOLD_DEP_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'QE_TAX_COUPON_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'QE_SY_GL_AGENCY_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'FIRST_THRESHOLD_DEP_FREQ_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'SECOND_THRESHOLD_DEP_FREQ_NBR';
end;

class function TSY_LOCAL_DEPOSIT_FREQ.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_LOCAL_EXEMPTIONS}

class function TSY_LOCAL_EXEMPTIONS.GetClassIndex: Integer;
begin
  Result := 20;
end;

class function TSY_LOCAL_EXEMPTIONS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_LOCAL_EXEMPTIONS.GetEvTableNbr: Integer;
begin
  Result := 20;
end;

class function TSY_LOCAL_EXEMPTIONS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_LOCAL_EXEMPTIONS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := True
  else if FieldName = 'EXEMPT' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_LOCAL_EXEMPTIONS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 265
  else if AFieldName = 'SY_LOCAL_EXEMPTIONS_NBR' then Result := 268
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 270
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 271
  else if AFieldName = 'SY_LOCALS_NBR' then Result := 267
  else if AFieldName = 'E_D_CODE_TYPE' then Result := 266
  else if AFieldName = 'EXEMPT' then Result := 269
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_LOCAL_EXEMPTIONS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_LOCAL_EXEMPTIONS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_LOCAL_EXEMPTIONS_NBR';
  Result[1] := 'SY_LOCALS_NBR';
  Result[2] := 'E_D_CODE_TYPE';
  Result[3] := 'EXEMPT';
end;

class function TSY_LOCAL_EXEMPTIONS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := 2
  else if FieldName = 'EXEMPT' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_LOCAL_EXEMPTIONS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_LOCAL_EXEMPTIONS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_LOCAL_EXEMPTIONS_NBR';
end;

class function TSY_LOCAL_EXEMPTIONS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_LOCAL_EXEMPTIONS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_LOCAL_EXEMPTIONS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_LOCALS_NBR';
  Result[1] := 'E_D_CODE_TYPE';
end;

{TSY_LOCAL_MARITAL_STATUS}

class function TSY_LOCAL_MARITAL_STATUS.GetClassIndex: Integer;
begin
  Result := 21;
end;

class function TSY_LOCAL_MARITAL_STATUS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_LOCAL_MARITAL_STATUS.GetEvTableNbr: Integer;
begin
  Result := 21;
end;

class function TSY_LOCAL_MARITAL_STATUS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_LOCAL_MARITAL_STATUS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SY_STATE_MARITAL_STATUS_NBR' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'STANDARD_DEDUCTION_AMOUNT' then Result := True
  else if FieldName = 'STANDARD_EXEMPTION_ALLOW' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_LOCAL_MARITAL_STATUS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 274
  else if AFieldName = 'SY_LOCAL_MARITAL_STATUS_NBR' then Result := 277
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 279
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 280
  else if AFieldName = 'SY_LOCALS_NBR' then Result := 276
  else if AFieldName = 'SY_STATE_MARITAL_STATUS_NBR' then Result := 275
  else if AFieldName = 'FILLER' then Result := 278
  else if AFieldName = 'STANDARD_DEDUCTION_AMOUNT' then Result := 273
  else if AFieldName = 'STANDARD_EXEMPTION_ALLOW' then Result := 272
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_LOCAL_MARITAL_STATUS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_LOCAL_MARITAL_STATUS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
  Result[1] := 'SY_LOCALS_NBR';
  Result[2] := 'SY_STATE_MARITAL_STATUS_NBR';
end;

class function TSY_LOCAL_MARITAL_STATUS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_LOCAL_MARITAL_STATUS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STANDARD_DEDUCTION_AMOUNT' then Result := 6
  else if FieldName = 'STANDARD_EXEMPTION_ALLOW' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_LOCAL_MARITAL_STATUS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
end;

class function TSY_LOCAL_MARITAL_STATUS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_LOCAL_MARITAL_STATUS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_TAX_CHART;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
end;

class function TSY_LOCAL_MARITAL_STATUS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_LOCALS_NBR';
  Result[1] := 'SY_STATE_MARITAL_STATUS_NBR';
end;

{TSY_LOCAL_TAX_CHART}

class function TSY_LOCAL_TAX_CHART.GetClassIndex: Integer;
begin
  Result := 22;
end;

class function TSY_LOCAL_TAX_CHART.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_LOCAL_TAX_CHART.GetEvTableNbr: Integer;
begin
  Result := 22;
end;

class function TSY_LOCAL_TAX_CHART.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_LOCAL_TAX_CHART.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SY_LOCAL_MARITAL_STATUS_NBR' then Result := True
  else if FieldName = 'MINIMUM' then Result := True
  else if FieldName = 'MAXIMUM' then Result := True
  else if FieldName = 'PERCENTAGE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_LOCAL_TAX_CHART.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 284
  else if AFieldName = 'SY_LOCAL_TAX_CHART_NBR' then Result := 287
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 288
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 289
  else if AFieldName = 'SY_LOCALS_NBR' then Result := 286
  else if AFieldName = 'SY_LOCAL_MARITAL_STATUS_NBR' then Result := 285
  else if AFieldName = 'MINIMUM' then Result := 283
  else if AFieldName = 'MAXIMUM' then Result := 282
  else if AFieldName = 'PERCENTAGE' then Result := 281
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_LOCAL_TAX_CHART.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_LOCAL_TAX_CHART.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_LOCAL_TAX_CHART_NBR';
  Result[1] := 'SY_LOCALS_NBR';
  Result[2] := 'SY_LOCAL_MARITAL_STATUS_NBR';
end;

class function TSY_LOCAL_TAX_CHART.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_LOCAL_TAX_CHART.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MINIMUM' then Result := 6
  else if FieldName = 'MAXIMUM' then Result := 6
  else if FieldName = 'PERCENTAGE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_LOCAL_TAX_CHART.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_LOCAL_TAX_CHART_NBR';
end;

class function TSY_LOCAL_TAX_CHART.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCALS_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_LOCAL_MARITAL_STATUS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_LOCAL_TAX_CHART.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_LOCAL_TAX_CHART.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_MEDIA_TYPE}

class function TSY_MEDIA_TYPE.GetClassIndex: Integer;
begin
  Result := 23;
end;

class function TSY_MEDIA_TYPE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_MEDIA_TYPE.GetEvTableNbr: Integer;
begin
  Result := 23;
end;

class function TSY_MEDIA_TYPE.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_MEDIA_TYPE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_MEDIA_TYPE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 290
  else if AFieldName = 'SY_MEDIA_TYPE_NBR' then Result := 291
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 294
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 295
  else if AFieldName = 'CLASS_NAME' then Result := 293
  else if AFieldName = 'NAME' then Result := 292
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_MEDIA_TYPE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_MEDIA_TYPE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_MEDIA_TYPE_NBR';
  Result[1] := 'CLASS_NAME';
  Result[2] := 'NAME';
end;

class function TSY_MEDIA_TYPE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_MEDIA_TYPE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_MEDIA_TYPE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_MEDIA_TYPE_NBR';
end;

class function TSY_MEDIA_TYPE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_MEDIA_TYPE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_MEDIA_TYPE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_QUEUE_PRIORITY}

class function TSY_QUEUE_PRIORITY.GetClassIndex: Integer;
begin
  Result := 24;
end;

class function TSY_QUEUE_PRIORITY.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_QUEUE_PRIORITY.GetEvTableNbr: Integer;
begin
  Result := 25;
end;

class function TSY_QUEUE_PRIORITY.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_QUEUE_PRIORITY.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_QUEUE_PRIORITY.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 300
  else if AFieldName = 'SY_QUEUE_PRIORITY_NBR' then Result := 303
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 305
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 306
  else if AFieldName = 'METHOD_NAME' then Result := 304
  else if AFieldName = 'PRIORITY' then Result := 302
  else if AFieldName = 'THREADS' then Result := 301
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_QUEUE_PRIORITY.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_QUEUE_PRIORITY.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_QUEUE_PRIORITY_NBR';
  Result[1] := 'METHOD_NAME';
  Result[2] := 'PRIORITY';
  Result[3] := 'THREADS';
end;

class function TSY_QUEUE_PRIORITY.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'METHOD_NAME' then Result := 255
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_QUEUE_PRIORITY.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_QUEUE_PRIORITY.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_QUEUE_PRIORITY_NBR';
end;

class function TSY_QUEUE_PRIORITY.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_QUEUE_PRIORITY.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_QUEUE_PRIORITY.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'METHOD_NAME';
end;

{TSY_RECIPROCATED_STATES}

class function TSY_RECIPROCATED_STATES.GetClassIndex: Integer;
begin
  Result := 25;
end;

class function TSY_RECIPROCATED_STATES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_RECIPROCATED_STATES.GetEvTableNbr: Integer;
begin
  Result := 26;
end;

class function TSY_RECIPROCATED_STATES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_RECIPROCATED_STATES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'PARTICIPANT_STATE_NBR' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_RECIPROCATED_STATES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 307
  else if AFieldName = 'SY_RECIPROCATED_STATES_NBR' then Result := 310
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 311
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 312
  else if AFieldName = 'MAIN_STATE_NBR' then Result := 309
  else if AFieldName = 'PARTICIPANT_STATE_NBR' then Result := 308
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_RECIPROCATED_STATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_RECIPROCATED_STATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_RECIPROCATED_STATES_NBR';
  Result[1] := 'MAIN_STATE_NBR';
  Result[2] := 'PARTICIPANT_STATE_NBR';
end;

class function TSY_RECIPROCATED_STATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_RECIPROCATED_STATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_RECIPROCATED_STATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_RECIPROCATED_STATES_NBR';
end;

class function TSY_RECIPROCATED_STATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'MAIN_STATE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'PARTICIPANT_STATE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_RECIPROCATED_STATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_RECIPROCATED_STATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'MAIN_STATE_NBR';
  Result[1] := 'PARTICIPANT_STATE_NBR';
end;

{TSY_REPORTS}

class function TSY_REPORTS.GetClassIndex: Integer;
begin
  Result := 26;
end;

class function TSY_REPORTS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 27;
end;

class function TSY_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := True
  else if FieldName = 'ACTIVE_REPORT' then Result := True
  else if FieldName = 'SY_REPORT_WRITER_REPORTS_NBR' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'ACTIVE_YEAR_FROM' then Result := True
  else if FieldName = 'ACTIVE_YEAR_TO' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 313
  else if AFieldName = 'SY_REPORTS_NBR' then Result := 318
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 322
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 323
  else if AFieldName = 'DESCRIPTION' then Result := 320
  else if AFieldName = 'ACTIVE_REPORT' then Result := 321
  else if AFieldName = 'SY_REPORT_WRITER_REPORTS_NBR' then Result := 317
  else if AFieldName = 'FILLER' then Result := 319
  else if AFieldName = 'ACTIVE_YEAR_FROM' then Result := 316
  else if AFieldName = 'ACTIVE_YEAR_TO' then Result := 315
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 314
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_REPORTS_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'ACTIVE_REPORT';
end;

class function TSY_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else if FieldName = 'ACTIVE_REPORT' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORTS_NBR';
end;

class function TSY_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_WRITER_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'CUSTOM_DEBIT_SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_FED_TAX_PAYMENT_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'QE_TAX_COUPON_SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'QE_TAX_COUPON_SY_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
end;

class function TSY_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_REPORTS_GROUP}

class function TSY_REPORTS_GROUP.GetClassIndex: Integer;
begin
  Result := 27;
end;

class function TSY_REPORTS_GROUP.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_REPORTS_GROUP.GetEvTableNbr: Integer;
begin
  Result := 28;
end;

class function TSY_REPORTS_GROUP.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORTS_GROUP.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SY_REPORTS_NBR' then Result := True
  else if FieldName = 'SY_GL_AGENCY_REPORT_NBR' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'AGENCY_COPY' then Result := True
  else if FieldName = 'SB_COPY' then Result := True
  else if FieldName = 'CL_COPY' then Result := True
  else if FieldName = 'MEDIA_TYPE' then Result := True
  else if FieldName = 'NAME' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORTS_GROUP.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 324
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 327
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 334
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 335
  else if AFieldName = 'SY_REPORTS_NBR' then Result := 326
  else if AFieldName = 'SY_GL_AGENCY_REPORT_NBR' then Result := 325
  else if AFieldName = 'FILLER' then Result := 329
  else if AFieldName = 'AGENCY_COPY' then Result := 333
  else if AFieldName = 'SB_COPY' then Result := 332
  else if AFieldName = 'CL_COPY' then Result := 331
  else if AFieldName = 'MEDIA_TYPE' then Result := 330
  else if AFieldName = 'NAME' then Result := 328
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORTS_GROUP.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORTS_GROUP.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 5);
  Result[0] := 'SY_REPORTS_GROUP_NBR';
  Result[1] := 'AGENCY_COPY';
  Result[2] := 'SB_COPY';
  Result[3] := 'CL_COPY';
  Result[4] := 'MEDIA_TYPE';
end;

class function TSY_REPORTS_GROUP.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'AGENCY_COPY' then Result := 1
  else if FieldName = 'SB_COPY' then Result := 1
  else if FieldName = 'CL_COPY' then Result := 1
  else if FieldName = 'MEDIA_TYPE' then Result := 1
  else if FieldName = 'NAME' then Result := 60
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORTS_GROUP.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORTS_GROUP.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORTS_GROUP_NBR';
end;

class function TSY_REPORTS_GROUP.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_REPORT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_REPORT_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_REPORTS_GROUP.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_REPORT;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_AGENCY_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
end;

class function TSY_REPORTS_GROUP.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_REPORT_GROUPS}

class function TSY_REPORT_GROUPS.GetClassIndex: Integer;
begin
  Result := 28;
end;

class function TSY_REPORT_GROUPS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_REPORT_GROUPS.GetEvTableNbr: Integer;
begin
  Result := 29;
end;

class function TSY_REPORT_GROUPS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORT_GROUPS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORT_GROUPS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 336
  else if AFieldName = 'SY_REPORT_GROUPS_NBR' then Result := 337
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 339
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 340
  else if AFieldName = 'NAME' then Result := 338
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORT_GROUPS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORT_GROUPS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_REPORT_GROUPS_NBR';
  Result[1] := 'NAME';
end;

class function TSY_REPORT_GROUPS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORT_GROUPS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORT_GROUPS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORT_GROUPS_NBR';
end;

class function TSY_REPORT_GROUPS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_REPORT_GROUPS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_GROUP_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_GROUPS_NBR';
end;

class function TSY_REPORT_GROUPS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_REPORT_GROUP_MEMBERS}

class function TSY_REPORT_GROUP_MEMBERS.GetClassIndex: Integer;
begin
  Result := 29;
end;

class function TSY_REPORT_GROUP_MEMBERS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_REPORT_GROUP_MEMBERS.GetEvTableNbr: Integer;
begin
  Result := 30;
end;

class function TSY_REPORT_GROUP_MEMBERS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORT_GROUP_MEMBERS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORT_GROUP_MEMBERS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 341
  else if AFieldName = 'SY_REPORT_GROUP_MEMBERS_NBR' then Result := 344
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 345
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 346
  else if AFieldName = 'SY_REPORT_GROUPS_NBR' then Result := 343
  else if AFieldName = 'SY_REPORT_WRITER_REPORTS_NBR' then Result := 342
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORT_GROUP_MEMBERS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORT_GROUP_MEMBERS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_REPORT_GROUP_MEMBERS_NBR';
  Result[1] := 'SY_REPORT_GROUPS_NBR';
  Result[2] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

class function TSY_REPORT_GROUP_MEMBERS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORT_GROUP_MEMBERS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORT_GROUP_MEMBERS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORT_GROUP_MEMBERS_NBR';
end;

class function TSY_REPORT_GROUP_MEMBERS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_WRITER_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_GROUPS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_GROUPS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_GROUPS_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_REPORT_GROUP_MEMBERS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_REPORT_GROUP_MEMBERS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_REPORT_GROUPS_NBR';
  Result[1] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

{TSY_REPORT_WRITER_REPORTS}

class function TSY_REPORT_WRITER_REPORTS.GetClassIndex: Integer;
begin
  Result := 30;
end;

class function TSY_REPORT_WRITER_REPORTS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_REPORT_WRITER_REPORTS.GetEvTableNbr: Integer;
begin
  Result := 31;
end;

class function TSY_REPORT_WRITER_REPORTS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORT_WRITER_REPORTS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORT_WRITER_REPORTS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 349
  else if AFieldName = 'SY_REPORT_WRITER_REPORTS_NBR' then Result := 350
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 357
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 358
  else if AFieldName = 'REPORT_DESCRIPTION' then Result := 353
  else if AFieldName = 'REPORT_TYPE' then Result := 356
  else if AFieldName = 'REPORT_FILE' then Result := 348
  else if AFieldName = 'NOTES' then Result := 347
  else if AFieldName = 'REPORT_STATUS' then Result := 355
  else if AFieldName = 'MEDIA_TYPE' then Result := 354
  else if AFieldName = 'CLASS_NAME' then Result := 352
  else if AFieldName = 'ANCESTOR_CLASS_NAME' then Result := 351
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORT_WRITER_REPORTS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORT_WRITER_REPORTS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 5);
  Result[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[1] := 'REPORT_DESCRIPTION';
  Result[2] := 'REPORT_TYPE';
  Result[3] := 'REPORT_STATUS';
  Result[4] := 'MEDIA_TYPE';
end;

class function TSY_REPORT_WRITER_REPORTS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'REPORT_DESCRIPTION' then Result := 40
  else if FieldName = 'REPORT_TYPE' then Result := 1
  else if FieldName = 'REPORT_STATUS' then Result := 1
  else if FieldName = 'MEDIA_TYPE' then Result := 2
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'ANCESTOR_CLASS_NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORT_WRITER_REPORTS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORT_WRITER_REPORTS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

class function TSY_REPORT_WRITER_REPORTS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_REPORT_WRITER_REPORTS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_GROUP_MEMBERS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_VERSIONS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

class function TSY_REPORT_WRITER_REPORTS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'REPORT_DESCRIPTION';
end;

{TSY_SEC_TEMPLATES}

class function TSY_SEC_TEMPLATES.GetClassIndex: Integer;
begin
  Result := 31;
end;

class function TSY_SEC_TEMPLATES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_SEC_TEMPLATES.GetEvTableNbr: Integer;
begin
  Result := 32;
end;

class function TSY_SEC_TEMPLATES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_SEC_TEMPLATES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_SEC_TEMPLATES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 360
  else if AFieldName = 'SY_SEC_TEMPLATES_NBR' then Result := 361
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 363
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 364
  else if AFieldName = 'NAME' then Result := 362
  else if AFieldName = 'DATA' then Result := 359
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_SEC_TEMPLATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_SEC_TEMPLATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_SEC_TEMPLATES_NBR';
  Result[1] := 'NAME';
end;

class function TSY_SEC_TEMPLATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_SEC_TEMPLATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_SEC_TEMPLATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_SEC_TEMPLATES_NBR';
end;

class function TSY_SEC_TEMPLATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_SEC_TEMPLATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_SEC_TEMPLATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'NAME';
end;

{TSY_STATES}

class function TSY_STATES.GetClassIndex: Integer;
begin
  Result := 32;
end;

class function TSY_STATES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_STATES.GetEvTableNbr: Integer;
begin
  Result := 33;
end;

class function TSY_STATES.GetEntityName: String;
begin
  Result := 'State';
end;

class function TSY_STATES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'STATE_WITHHOLDING' then Result := True
  else if FieldName = 'STATE_EFT_TYPE' then Result := True
  else if FieldName = 'EFT_NOTES' then Result := True
  else if FieldName = 'PRINT_STATE_RETURN_IF_ZERO' then Result := True
  else if FieldName = 'SUI_EFT_TYPE' then Result := True
  else if FieldName = 'SUI_EFT_NOTES' then Result := True
  else if FieldName = 'SDI_RECIPROCATE' then Result := True
  else if FieldName = 'SUI_RECIPROCATE' then Result := True
  else if FieldName = 'SHOW_S125_IN_GROSS_WAGES_SUI' then Result := True
  else if FieldName = 'PRINT_SUI_RETURN_IF_ZERO' then Result := True
  else if FieldName = 'PAYROLL' then Result := True
  else if FieldName = 'SALES' then Result := True
  else if FieldName = 'CORPORATE' then Result := True
  else if FieldName = 'RAILROAD' then Result := True
  else if FieldName = 'UNEMPLOYMENT' then Result := True
  else if FieldName = 'OTHER' then Result := True
  else if FieldName = 'UIFSA_NEW_HIRE_REPORTING' then Result := True
  else if FieldName = 'DD_CHILD_SUPPORT' then Result := True
  else if FieldName = 'USE_STATE_MISC_TAX_RETURN_CODE' then Result := True
  else if FieldName = 'SY_STATE_TAX_PMT_AGENCY_NBR' then Result := True
  else if FieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := True
  else if FieldName = 'SY_STATE_REPORTING_AGENCY_NBR' then Result := True
  else if FieldName = 'PAY_SDI_WITH' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'TAX_DEPOSIT_FOLLOW_FEDERAL' then Result := True
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := True
  else if FieldName = 'SUI_TAX_PAYMENT_TYPE_CODE' then Result := True
  else if FieldName = 'INC_SUI_TAX_PAYMENT_CODE' then Result := True
  else if FieldName = 'FIPS_CODE' then Result := True
  else if FieldName = 'STATE_MINIMUM_WAGE' then Result := True
  else if FieldName = 'STATE_TIP_CREDIT' then Result := True
  else if FieldName = 'SUPPLEMENTAL_WAGES_PERCENTAGE' then Result := True
  else if FieldName = 'WEEKLY_SDI_TAX_CAP' then Result := True
  else if FieldName = 'EE_SDI_MAXIMUM_WAGE' then Result := True
  else if FieldName = 'ER_SDI_MAXIMUM_WAGE' then Result := True
  else if FieldName = 'EE_SDI_RATE' then Result := True
  else if FieldName = 'ER_SDI_RATE' then Result := True
  else if FieldName = 'MAXIMUM_GARNISHMENT_PERCENT' then Result := True
  else if FieldName = 'GARNISH_MIN_WAGE_MULTIPLIER' then Result := True
  else if FieldName = 'SALES_TAX_PERCENTAGE' then Result := True
  else if FieldName = 'W2_BOX' then Result := True
  else if FieldName = 'RECIPROCITY_TYPE' then Result := True
  else if FieldName = 'PRINT_W2' then Result := True
  else if FieldName = 'CAP_STATE_TAX_CREDIT' then Result := True
  else if FieldName = 'INACTIVATE_MARITAL_STATUS' then Result := True
  else if FieldName = 'STATE_OT_TIP_CREDIT' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STATES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 378
  else if AFieldName = 'SY_STATES_NBR' then Result := 384
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 414
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 415
  else if AFieldName = 'STATE' then Result := 394
  else if AFieldName = 'NAME' then Result := 393
  else if AFieldName = 'STATE_WITHHOLDING' then Result := 413
  else if AFieldName = 'STATE_EFT_TYPE' then Result := 412
  else if AFieldName = 'EFT_NOTES' then Result := 377
  else if AFieldName = 'PRINT_STATE_RETURN_IF_ZERO' then Result := 411
  else if AFieldName = 'SUI_EFT_TYPE' then Result := 410
  else if AFieldName = 'SUI_EFT_NOTES' then Result := 376
  else if AFieldName = 'SDI_RECIPROCATE' then Result := 409
  else if AFieldName = 'SUI_RECIPROCATE' then Result := 408
  else if AFieldName = 'SHOW_S125_IN_GROSS_WAGES_SUI' then Result := 407
  else if AFieldName = 'PRINT_SUI_RETURN_IF_ZERO' then Result := 406
  else if AFieldName = 'PAYROLL' then Result := 392
  else if AFieldName = 'SALES' then Result := 391
  else if AFieldName = 'CORPORATE' then Result := 390
  else if AFieldName = 'RAILROAD' then Result := 389
  else if AFieldName = 'UNEMPLOYMENT' then Result := 388
  else if AFieldName = 'OTHER' then Result := 387
  else if AFieldName = 'UIFSA_NEW_HIRE_REPORTING' then Result := 405
  else if AFieldName = 'DD_CHILD_SUPPORT' then Result := 404
  else if AFieldName = 'USE_STATE_MISC_TAX_RETURN_CODE' then Result := 403
  else if AFieldName = 'SY_STATE_TAX_PMT_AGENCY_NBR' then Result := 383
  else if AFieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := 382
  else if AFieldName = 'SY_STATE_REPORTING_AGENCY_NBR' then Result := 381
  else if AFieldName = 'PAY_SDI_WITH' then Result := 402
  else if AFieldName = 'FILLER' then Result := 386
  else if AFieldName = 'TAX_DEPOSIT_FOLLOW_FEDERAL' then Result := 401
  else if AFieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 400
  else if AFieldName = 'SUI_TAX_PAYMENT_TYPE_CODE' then Result := 380
  else if AFieldName = 'INC_SUI_TAX_PAYMENT_CODE' then Result := 399
  else if AFieldName = 'FIPS_CODE' then Result := 385
  else if AFieldName = 'STATE_MINIMUM_WAGE' then Result := 375
  else if AFieldName = 'STATE_TIP_CREDIT' then Result := 374
  else if AFieldName = 'SUPPLEMENTAL_WAGES_PERCENTAGE' then Result := 373
  else if AFieldName = 'WEEKLY_SDI_TAX_CAP' then Result := 372
  else if AFieldName = 'EE_SDI_MAXIMUM_WAGE' then Result := 371
  else if AFieldName = 'ER_SDI_MAXIMUM_WAGE' then Result := 370
  else if AFieldName = 'EE_SDI_RATE' then Result := 369
  else if AFieldName = 'ER_SDI_RATE' then Result := 368
  else if AFieldName = 'MAXIMUM_GARNISHMENT_PERCENT' then Result := 367
  else if AFieldName = 'GARNISH_MIN_WAGE_MULTIPLIER' then Result := 366
  else if AFieldName = 'SALES_TAX_PERCENTAGE' then Result := 365
  else if AFieldName = 'W2_BOX' then Result := 379
  else if AFieldName = 'RECIPROCITY_TYPE' then Result := 398
  else if AFieldName = 'PRINT_W2' then Result := 397
  else if AFieldName = 'CAP_STATE_TAX_CREDIT' then Result := 396
  else if AFieldName = 'INACTIVATE_MARITAL_STATUS' then Result := 395
  else if AFieldName = 'STATE_OT_TIP_CREDIT' then Result := 563
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STATES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else if FieldName = 'STATE' then Result := 'Abbreviation'
  else if FieldName = 'NAME' then Result := 'Name'
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STATES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 21);
  Result[0] := 'SY_STATES_NBR';
  Result[1] := 'NAME';
  Result[2] := 'STATE_WITHHOLDING';
  Result[3] := 'STATE_EFT_TYPE';
  Result[4] := 'PRINT_STATE_RETURN_IF_ZERO';
  Result[5] := 'SUI_EFT_TYPE';
  Result[6] := 'SDI_RECIPROCATE';
  Result[7] := 'SUI_RECIPROCATE';
  Result[8] := 'SHOW_S125_IN_GROSS_WAGES_SUI';
  Result[9] := 'PRINT_SUI_RETURN_IF_ZERO';
  Result[10] := 'UIFSA_NEW_HIRE_REPORTING';
  Result[11] := 'DD_CHILD_SUPPORT';
  Result[12] := 'USE_STATE_MISC_TAX_RETURN_CODE';
  Result[13] := 'PAY_SDI_WITH';
  Result[14] := 'TAX_DEPOSIT_FOLLOW_FEDERAL';
  Result[15] := 'ROUND_TO_NEAREST_DOLLAR';
  Result[16] := 'INC_SUI_TAX_PAYMENT_CODE';
  Result[17] := 'RECIPROCITY_TYPE';
  Result[18] := 'PRINT_W2';
  Result[19] := 'CAP_STATE_TAX_CREDIT';
  Result[20] := 'INACTIVATE_MARITAL_STATUS';
end;

class function TSY_STATES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE' then Result := 2
  else if FieldName = 'NAME' then Result := 20
  else if FieldName = 'STATE_WITHHOLDING' then Result := 1
  else if FieldName = 'STATE_EFT_TYPE' then Result := 1
  else if FieldName = 'PRINT_STATE_RETURN_IF_ZERO' then Result := 1
  else if FieldName = 'SUI_EFT_TYPE' then Result := 1
  else if FieldName = 'SDI_RECIPROCATE' then Result := 1
  else if FieldName = 'SUI_RECIPROCATE' then Result := 1
  else if FieldName = 'SHOW_S125_IN_GROSS_WAGES_SUI' then Result := 1
  else if FieldName = 'PRINT_SUI_RETURN_IF_ZERO' then Result := 1
  else if FieldName = 'PAYROLL' then Result := 8
  else if FieldName = 'SALES' then Result := 8
  else if FieldName = 'CORPORATE' then Result := 8
  else if FieldName = 'RAILROAD' then Result := 8
  else if FieldName = 'UNEMPLOYMENT' then Result := 8
  else if FieldName = 'OTHER' then Result := 8
  else if FieldName = 'UIFSA_NEW_HIRE_REPORTING' then Result := 1
  else if FieldName = 'DD_CHILD_SUPPORT' then Result := 1
  else if FieldName = 'USE_STATE_MISC_TAX_RETURN_CODE' then Result := 1
  else if FieldName = 'PAY_SDI_WITH' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'TAX_DEPOSIT_FOLLOW_FEDERAL' then Result := 1
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 1
  else if FieldName = 'SUI_TAX_PAYMENT_TYPE_CODE' then Result := 10
  else if FieldName = 'INC_SUI_TAX_PAYMENT_CODE' then Result := 1
  else if FieldName = 'FIPS_CODE' then Result := 2
  else if FieldName = 'W2_BOX' then Result := 10
  else if FieldName = 'RECIPROCITY_TYPE' then Result := 1
  else if FieldName = 'PRINT_W2' then Result := 1
  else if FieldName = 'CAP_STATE_TAX_CREDIT' then Result := 1
  else if FieldName = 'INACTIVATE_MARITAL_STATUS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STATES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE_MINIMUM_WAGE' then Result := 6
  else if FieldName = 'STATE_TIP_CREDIT' then Result := 6
  else if FieldName = 'SUPPLEMENTAL_WAGES_PERCENTAGE' then Result := 6
  else if FieldName = 'WEEKLY_SDI_TAX_CAP' then Result := 6
  else if FieldName = 'EE_SDI_MAXIMUM_WAGE' then Result := 6
  else if FieldName = 'ER_SDI_MAXIMUM_WAGE' then Result := 6
  else if FieldName = 'EE_SDI_RATE' then Result := 6
  else if FieldName = 'ER_SDI_RATE' then Result := 6
  else if FieldName = 'MAXIMUM_GARNISHMENT_PERCENT' then Result := 6
  else if FieldName = 'GARNISH_MIN_WAGE_MULTIPLIER' then Result := 6
  else if FieldName = 'SALES_TAX_PERCENTAGE' then Result := 6
  else if FieldName = 'STATE_OT_TIP_CREDIT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STATES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STATES_NBR';
end;

class function TSY_STATES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_TAX_PMT_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_REPORTING_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_STATES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_COUNTY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCALS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_RECIPROCATED_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'MAIN_STATE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_RECIPROCATED_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'PARTICIPANT_STATE_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_EXEMPTIONS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_TAX_CHART;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_SUI;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_HR_REFUSAL_REASON;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
end;

class function TSY_STATES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'STATE';
end;

{TSY_STATE_DEPOSIT_FREQ}

class function TSY_STATE_DEPOSIT_FREQ.GetClassIndex: Integer;
begin
  Result := 33;
end;

class function TSY_STATE_DEPOSIT_FREQ.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_STATE_DEPOSIT_FREQ.GetEvTableNbr: Integer;
begin
  Result := 34;
end;

class function TSY_STATE_DEPOSIT_FREQ.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_STATE_DEPOSIT_FREQ.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'WHEN_DUE_TYPE' then Result := True
  else if FieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := True
  else if FieldName = 'THRESHOLD_PERIOD' then Result := True
  else if FieldName = 'CHANGE_STATUS_ON_THRESHOLD' then Result := True
  else if FieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := True
  else if FieldName = 'DESCRIPTION' then Result := True
  else if FieldName = 'THRESHOLD_DEP_FREQUENCY_NUMBER' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_PERIOD' then Result := True
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := True
  else if FieldName = 'INC_TAX_PAYMENT_CODE' then Result := True
  else if FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := True
  else if FieldName = 'FREQUENCY_TYPE' then Result := True
  else if FieldName = 'THRESHOLD_AMOUNT' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := True
  else if FieldName = 'QE_TAX_COUPON_SY_REPORTS_NBR' then Result := True
  else if FieldName = 'QE_SY_GL_AGENCY_OFFICE_NBR' then Result := True
  else if FieldName = 'HIDE_FROM_USER' then Result := True
  else if FieldName = 'SECOND_THRESHOLD_DEP_FREQ_NBR' then Result := True
  else if FieldName = 'PAY_AND_SHIFTBACK' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STATE_DEPOSIT_FREQ.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 418
  else if AFieldName = 'SY_STATE_DEPOSIT_FREQ_NBR' then Result := 429
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 441
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 442
  else if AFieldName = 'SY_STATES_NBR' then Result := 428
  else if AFieldName = 'WHEN_DUE_TYPE' then Result := 440
  else if AFieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := 427
  else if AFieldName = 'THRESHOLD_PERIOD' then Result := 439
  else if AFieldName = 'CHANGE_STATUS_ON_THRESHOLD' then Result := 438
  else if AFieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := 437
  else if AFieldName = 'DESCRIPTION' then Result := 431
  else if AFieldName = 'THRESHOLD_DEP_FREQUENCY_NUMBER' then Result := 426
  else if AFieldName = 'FILLER' then Result := 430
  else if AFieldName = 'SECOND_THRESHOLD_PERIOD' then Result := 436
  else if AFieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 425
  else if AFieldName = 'INC_TAX_PAYMENT_CODE' then Result := 435
  else if AFieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR' then Result := 424
  else if AFieldName = 'FREQUENCY_TYPE' then Result := 434
  else if AFieldName = 'TAX_COUPON_SY_REPORTS_NBR' then Result := 423
  else if AFieldName = 'THRESHOLD_AMOUNT' then Result := 417
  else if AFieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := 416
  else if AFieldName = 'QE_TAX_COUPON_SY_REPORTS_NBR' then Result := 422
  else if AFieldName = 'QE_SY_GL_AGENCY_OFFICE_NBR' then Result := 421
  else if AFieldName = 'HIDE_FROM_USER' then Result := 433
  else if AFieldName = 'SECOND_THRESHOLD_DEP_FREQ_NBR' then Result := 420
  else if AFieldName = 'PAY_AND_SHIFTBACK' then Result := 432
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 419
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STATE_DEPOSIT_FREQ.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STATE_DEPOSIT_FREQ.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 13);
  Result[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
  Result[1] := 'SY_STATES_NBR';
  Result[2] := 'WHEN_DUE_TYPE';
  Result[3] := 'NUMBER_OF_DAYS_FOR_WHEN_DUE';
  Result[4] := 'THRESHOLD_PERIOD';
  Result[5] := 'CHANGE_STATUS_ON_THRESHOLD';
  Result[6] := 'THRD_MNTH_DUE_END_OF_NEXT_MNTH';
  Result[7] := 'DESCRIPTION';
  Result[8] := 'SECOND_THRESHOLD_PERIOD';
  Result[9] := 'INC_TAX_PAYMENT_CODE';
  Result[10] := 'FREQUENCY_TYPE';
  Result[11] := 'HIDE_FROM_USER';
  Result[12] := 'PAY_AND_SHIFTBACK';
end;

class function TSY_STATE_DEPOSIT_FREQ.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'WHEN_DUE_TYPE' then Result := 1
  else if FieldName = 'THRESHOLD_PERIOD' then Result := 1
  else if FieldName = 'CHANGE_STATUS_ON_THRESHOLD' then Result := 1
  else if FieldName = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 20
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'SECOND_THRESHOLD_PERIOD' then Result := 1
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 10
  else if FieldName = 'INC_TAX_PAYMENT_CODE' then Result := 1
  else if FieldName = 'FREQUENCY_TYPE' then Result := 1
  else if FieldName = 'HIDE_FROM_USER' then Result := 1
  else if FieldName = 'PAY_AND_SHIFTBACK' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STATE_DEPOSIT_FREQ.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'THRESHOLD_AMOUNT' then Result := 6
  else if FieldName = 'SECOND_THRESHOLD_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STATE_DEPOSIT_FREQ.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
end;

class function TSY_STATE_DEPOSIT_FREQ.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'QE_TAX_COUPON_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'THRESHOLD_DEP_FREQUENCY_NUMBER';
  Result[High(Result)].DestFields[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SECOND_THRESHOLD_DEP_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GL_AGENCY_FIELD_OFFICE;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'QE_SY_GL_AGENCY_OFFICE_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_STATE_DEPOSIT_FREQ.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'THRESHOLD_DEP_FREQUENCY_NUMBER';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_DEPOSIT_FREQ;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_DEPOSIT_FREQ_NBR';
  Result[High(Result)].DestFields[0] := 'SECOND_THRESHOLD_DEP_FREQ_NBR';
end;

class function TSY_STATE_DEPOSIT_FREQ.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_STATES_NBR';
  Result[1] := 'DESCRIPTION';
end;

{TSY_STATE_EXEMPTIONS}

class function TSY_STATE_EXEMPTIONS.GetClassIndex: Integer;
begin
  Result := 34;
end;

class function TSY_STATE_EXEMPTIONS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_STATE_EXEMPTIONS.GetEvTableNbr: Integer;
begin
  Result := 35;
end;

class function TSY_STATE_EXEMPTIONS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_STATE_EXEMPTIONS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := True
  else if FieldName = 'EXEMPT_STATE' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYEE_SDI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYER_SDI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYEE_SUI' then Result := True
  else if FieldName = 'EXEMPT_EMPLOYER_SUI' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STATE_EXEMPTIONS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 443
  else if AFieldName = 'SY_STATE_EXEMPTIONS_NBR' then Result := 446
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 452
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 453
  else if AFieldName = 'SY_STATES_NBR' then Result := 445
  else if AFieldName = 'E_D_CODE_TYPE' then Result := 444
  else if AFieldName = 'EXEMPT_STATE' then Result := 451
  else if AFieldName = 'EXEMPT_EMPLOYEE_SDI' then Result := 450
  else if AFieldName = 'EXEMPT_EMPLOYER_SDI' then Result := 449
  else if AFieldName = 'EXEMPT_EMPLOYEE_SUI' then Result := 448
  else if AFieldName = 'EXEMPT_EMPLOYER_SUI' then Result := 447
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STATE_EXEMPTIONS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STATE_EXEMPTIONS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 8);
  Result[0] := 'SY_STATE_EXEMPTIONS_NBR';
  Result[1] := 'SY_STATES_NBR';
  Result[2] := 'E_D_CODE_TYPE';
  Result[3] := 'EXEMPT_STATE';
  Result[4] := 'EXEMPT_EMPLOYEE_SDI';
  Result[5] := 'EXEMPT_EMPLOYER_SDI';
  Result[6] := 'EXEMPT_EMPLOYEE_SUI';
  Result[7] := 'EXEMPT_EMPLOYER_SUI';
end;

class function TSY_STATE_EXEMPTIONS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'E_D_CODE_TYPE' then Result := 2
  else if FieldName = 'EXEMPT_STATE' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYEE_SDI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYER_SDI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYEE_SUI' then Result := 1
  else if FieldName = 'EXEMPT_EMPLOYER_SUI' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STATE_EXEMPTIONS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STATE_EXEMPTIONS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STATE_EXEMPTIONS_NBR';
end;

class function TSY_STATE_EXEMPTIONS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_STATE_EXEMPTIONS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_STATE_EXEMPTIONS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_STATES_NBR';
  Result[1] := 'E_D_CODE_TYPE';
end;

{TSY_STATE_MARITAL_STATUS}

class function TSY_STATE_MARITAL_STATUS.GetClassIndex: Integer;
begin
  Result := 35;
end;

class function TSY_STATE_MARITAL_STATUS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_STATE_MARITAL_STATUS.GetEvTableNbr: Integer;
begin
  Result := 36;
end;

class function TSY_STATE_MARITAL_STATUS.GetEntityName: String;
begin
  Result := 'StateMaritalStatus';
end;

class function TSY_STATE_MARITAL_STATUS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'STATUS_TYPE' then Result := True
  else if FieldName = 'STATUS_DESCRIPTION' then Result := True
  else if FieldName = 'PERSONAL_EXEMPTIONS' then Result := True
  else if FieldName = 'DEDUCT_FEDERAL' then Result := True
  else if FieldName = 'DEDUCT_FICA' then Result := True
  else if FieldName = 'FILLER' then Result := True
  else if FieldName = 'STATE_PERCENT_OF_FEDERAL' then Result := True
  else if FieldName = 'STANDARD_DEDUCTION_PCNT_GROSS' then Result := True
  else if FieldName = 'STANDARD_DEDUCTION_MIN_AMOUNT' then Result := True
  else if FieldName = 'STANDARD_DEDUCTION_MAX_AMOUNT' then Result := True
  else if FieldName = 'STANDARD_DEDUCTION_FLAT_AMOUNT' then Result := True
  else if FieldName = 'STANDARD_PER_EXEMPTION_ALLOW' then Result := True
  else if FieldName = 'DEDUCT_FEDERAL_MAXIMUM_AMOUNT' then Result := True
  else if FieldName = 'PER_DEPENDENT_ALLOWANCE' then Result := True
  else if FieldName = 'PERSONAL_TAX_CREDIT_AMOUNT' then Result := True
  else if FieldName = 'TAX_CREDIT_PER_DEPENDENT' then Result := True
  else if FieldName = 'TAX_CREDIT_PER_ALLOWANCE' then Result := True
  else if FieldName = 'HIGH_INCOME_PER_EXEMPT_ALLOW' then Result := True
  else if FieldName = 'DEFINED_HIGH_INCOME_AMOUNT' then Result := True
  else if FieldName = 'MINIMUM_TAXABLE_INCOME' then Result := True
  else if FieldName = 'ADDITIONAL_EXEMPT_ALLOWANCE' then Result := True
  else if FieldName = 'ADDITIONAL_DEDUCTION_ALLOWANCE' then Result := True
  else if FieldName = 'DEDUCT_FICA__MAXIMUM_AMOUNT' then Result := True
  else if FieldName = 'BLIND_EXEMPTION_AMOUNT' then Result := True
  else if FieldName = 'ACTIVE_STATUS' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STATE_MARITAL_STATUS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 472
  else if AFieldName = 'SY_STATE_MARITAL_STATUS_NBR' then Result := 475
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 481
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 482
  else if AFieldName = 'SY_STATES_NBR' then Result := 474
  else if AFieldName = 'STATUS_TYPE' then Result := 478
  else if AFieldName = 'STATUS_DESCRIPTION' then Result := 477
  else if AFieldName = 'PERSONAL_EXEMPTIONS' then Result := 473
  else if AFieldName = 'DEDUCT_FEDERAL' then Result := 480
  else if AFieldName = 'DEDUCT_FICA' then Result := 479
  else if AFieldName = 'FILLER' then Result := 476
  else if AFieldName = 'STATE_PERCENT_OF_FEDERAL' then Result := 471
  else if AFieldName = 'STANDARD_DEDUCTION_PCNT_GROSS' then Result := 470
  else if AFieldName = 'STANDARD_DEDUCTION_MIN_AMOUNT' then Result := 469
  else if AFieldName = 'STANDARD_DEDUCTION_MAX_AMOUNT' then Result := 468
  else if AFieldName = 'STANDARD_DEDUCTION_FLAT_AMOUNT' then Result := 467
  else if AFieldName = 'STANDARD_PER_EXEMPTION_ALLOW' then Result := 466
  else if AFieldName = 'DEDUCT_FEDERAL_MAXIMUM_AMOUNT' then Result := 465
  else if AFieldName = 'PER_DEPENDENT_ALLOWANCE' then Result := 464
  else if AFieldName = 'PERSONAL_TAX_CREDIT_AMOUNT' then Result := 463
  else if AFieldName = 'TAX_CREDIT_PER_DEPENDENT' then Result := 462
  else if AFieldName = 'TAX_CREDIT_PER_ALLOWANCE' then Result := 461
  else if AFieldName = 'HIGH_INCOME_PER_EXEMPT_ALLOW' then Result := 460
  else if AFieldName = 'DEFINED_HIGH_INCOME_AMOUNT' then Result := 459
  else if AFieldName = 'MINIMUM_TAXABLE_INCOME' then Result := 458
  else if AFieldName = 'ADDITIONAL_EXEMPT_ALLOWANCE' then Result := 457
  else if AFieldName = 'ADDITIONAL_DEDUCTION_ALLOWANCE' then Result := 456
  else if AFieldName = 'DEDUCT_FICA__MAXIMUM_AMOUNT' then Result := 455
  else if AFieldName = 'BLIND_EXEMPTION_AMOUNT' then Result := 454
  else if AFieldName = 'ACTIVE_STATUS' then Result := 582
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STATE_MARITAL_STATUS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else if FieldName = 'SY_STATES_NBR' then Result := 'StateId'
  else if FieldName = 'STATUS_DESCRIPTION' then Result := 'Description'
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STATE_MARITAL_STATUS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[1] := 'SY_STATES_NBR';
  Result[2] := 'STATUS_TYPE';
  Result[3] := 'STATUS_DESCRIPTION';
  Result[4] := 'DEDUCT_FEDERAL';
  Result[5] := 'DEDUCT_FICA';
  Result[6] := 'ACTIVE_STATUS';
end;

class function TSY_STATE_MARITAL_STATUS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATUS_TYPE' then Result := 4
  else if FieldName = 'STATUS_DESCRIPTION' then Result := 40
  else if FieldName = 'DEDUCT_FEDERAL' then Result := 1
  else if FieldName = 'DEDUCT_FICA' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'ACTIVE_STATUS' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STATE_MARITAL_STATUS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'STATE_PERCENT_OF_FEDERAL' then Result := 6
  else if FieldName = 'STANDARD_DEDUCTION_PCNT_GROSS' then Result := 6
  else if FieldName = 'STANDARD_DEDUCTION_MIN_AMOUNT' then Result := 6
  else if FieldName = 'STANDARD_DEDUCTION_MAX_AMOUNT' then Result := 6
  else if FieldName = 'STANDARD_DEDUCTION_FLAT_AMOUNT' then Result := 6
  else if FieldName = 'STANDARD_PER_EXEMPTION_ALLOW' then Result := 6
  else if FieldName = 'DEDUCT_FEDERAL_MAXIMUM_AMOUNT' then Result := 6
  else if FieldName = 'PER_DEPENDENT_ALLOWANCE' then Result := 6
  else if FieldName = 'PERSONAL_TAX_CREDIT_AMOUNT' then Result := 6
  else if FieldName = 'TAX_CREDIT_PER_DEPENDENT' then Result := 6
  else if FieldName = 'TAX_CREDIT_PER_ALLOWANCE' then Result := 6
  else if FieldName = 'HIGH_INCOME_PER_EXEMPT_ALLOW' then Result := 6
  else if FieldName = 'DEFINED_HIGH_INCOME_AMOUNT' then Result := 6
  else if FieldName = 'MINIMUM_TAXABLE_INCOME' then Result := 6
  else if FieldName = 'ADDITIONAL_EXEMPT_ALLOWANCE' then Result := 6
  else if FieldName = 'ADDITIONAL_DEDUCTION_ALLOWANCE' then Result := 6
  else if FieldName = 'DEDUCT_FICA__MAXIMUM_AMOUNT' then Result := 6
  else if FieldName = 'BLIND_EXEMPTION_AMOUNT' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STATE_MARITAL_STATUS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STATE_MARITAL_STATUS_NBR';
end;

class function TSY_STATE_MARITAL_STATUS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_STATE_MARITAL_STATUS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_LOCAL_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_TAX_CHART;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
end;

class function TSY_STATE_MARITAL_STATUS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_STATES_NBR';
  Result[1] := 'STATUS_DESCRIPTION';
end;

{TSY_STATE_TAX_CHART}

class function TSY_STATE_TAX_CHART.GetClassIndex: Integer;
begin
  Result := 36;
end;

class function TSY_STATE_TAX_CHART.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_STATE_TAX_CHART.GetEvTableNbr: Integer;
begin
  Result := 37;
end;

class function TSY_STATE_TAX_CHART.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_STATE_TAX_CHART.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SY_STATE_MARITAL_STATUS_NBR' then Result := True
  else if FieldName = 'ENTRY_TYPE' then Result := True
  else if FieldName = 'MINIMUM' then Result := True
  else if FieldName = 'MAXIMUM' then Result := True
  else if FieldName = 'PERCENTAGE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STATE_TAX_CHART.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 486
  else if AFieldName = 'SY_STATE_TAX_CHART_NBR' then Result := 489
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 491
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 492
  else if AFieldName = 'SY_STATES_NBR' then Result := 488
  else if AFieldName = 'SY_STATE_MARITAL_STATUS_NBR' then Result := 487
  else if AFieldName = 'ENTRY_TYPE' then Result := 490
  else if AFieldName = 'MINIMUM' then Result := 485
  else if AFieldName = 'MAXIMUM' then Result := 484
  else if AFieldName = 'PERCENTAGE' then Result := 483
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STATE_TAX_CHART.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STATE_TAX_CHART.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_STATE_TAX_CHART_NBR';
  Result[1] := 'SY_STATES_NBR';
  Result[2] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[3] := 'ENTRY_TYPE';
end;

class function TSY_STATE_TAX_CHART.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ENTRY_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STATE_TAX_CHART.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'MINIMUM' then Result := 6
  else if FieldName = 'MAXIMUM' then Result := 6
  else if FieldName = 'PERCENTAGE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STATE_TAX_CHART.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STATE_TAX_CHART_NBR';
end;

class function TSY_STATE_TAX_CHART.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATE_MARITAL_STATUS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATE_MARITAL_STATUS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_STATE_TAX_CHART.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_STATE_TAX_CHART.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_STORAGE}

class function TSY_STORAGE.GetClassIndex: Integer;
begin
  Result := 37;
end;

class function TSY_STORAGE.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_STORAGE.GetEvTableNbr: Integer;
begin
  Result := 38;
end;

class function TSY_STORAGE.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_STORAGE.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_STORAGE.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 494
  else if AFieldName = 'SY_STORAGE_NBR' then Result := 495
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 497
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 498
  else if AFieldName = 'TAG' then Result := 496
  else if AFieldName = 'STORAGE_DATA' then Result := 493
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_STORAGE.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_STORAGE.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_STORAGE_NBR';
  Result[1] := 'TAG';
end;

class function TSY_STORAGE.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TAG' then Result := 20
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_STORAGE.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_STORAGE.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_STORAGE_NBR';
end;

class function TSY_STORAGE.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_STORAGE.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_STORAGE.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'TAG';
end;

{TSY_SUI}

class function TSY_SUI.GetClassIndex: Integer;
begin
  Result := 38;
end;

class function TSY_SUI.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_SUI.GetEvTableNbr: Integer;
begin
  Result := 39;
end;

class function TSY_SUI.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_SUI.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'SUI_TAX_NAME' then Result := True
  else if FieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER' then Result := True
  else if FieldName = 'FUTURE_DEFAULT_RATE_BEGIN_DATE' then Result := True
  else if FieldName = 'FUTURE_MAX_WAGE_BEGIN_DATE' then Result := True
  else if FieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := True
  else if FieldName = 'SY_SUI_TAX_PMT_AGENCY_NBR' then Result := True
  else if FieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := True
  else if FieldName = 'SY_SUI_REPORTING_AGENCY_NBR' then Result := True
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := True
  else if FieldName = 'SUI_ACTIVE' then Result := True
  else if FieldName = 'NEW_COMPANY_DEFAULT_RATE' then Result := True
  else if FieldName = 'FUTURE_DEFAULT_RATE' then Result := True
  else if FieldName = 'MAXIMUM_WAGE' then Result := True
  else if FieldName = 'FUTURE_MAXIMUM_WAGE' then Result := True
  else if FieldName = 'TAX_COUPON_SY_REPORTS_NBR' then Result := True
  else if FieldName = 'FREQUENCY_TYPE' then Result := True
  else if FieldName = 'W2_BOX' then Result := True
  else if FieldName = 'PAY_WITH_STATE' then Result := True
  else if FieldName = 'GLOBAL_RATE' then Result := True
  else if FieldName = 'E_D_CODE_TYPE' then Result := True
  else if FieldName = 'FTE_EXEMPTION' then Result := True
  else if FieldName = 'FTE_WEEKLY_HOURS' then Result := True
  else if FieldName = 'FTE_MULTIPLIER' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else if FieldName = 'ALTERNATE_TAXABLE_WAGE_BASE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_SUI.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 506
  else if AFieldName = 'SY_SUI_NBR' then Result := 518
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 527
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 528
  else if AFieldName = 'SY_STATES_NBR' then Result := 517
  else if AFieldName = 'SUI_TAX_NAME' then Result := 520
  else if AFieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER' then Result := 526
  else if AFieldName = 'FUTURE_DEFAULT_RATE_BEGIN_DATE' then Result := 505
  else if AFieldName = 'FUTURE_MAX_WAGE_BEGIN_DATE' then Result := 504
  else if AFieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := 525
  else if AFieldName = 'SY_SUI_TAX_PMT_AGENCY_NBR' then Result := 516
  else if AFieldName = 'SY_TAX_PMT_REPORT_NBR' then Result := 515
  else if AFieldName = 'SY_SUI_REPORTING_AGENCY_NBR' then Result := 514
  else if AFieldName = 'FILLER' then Result := 519
  else if AFieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 524
  else if AFieldName = 'SUI_ACTIVE' then Result := 523
  else if AFieldName = 'NEW_COMPANY_DEFAULT_RATE' then Result := 503
  else if AFieldName = 'FUTURE_DEFAULT_RATE' then Result := 502
  else if AFieldName = 'MAXIMUM_WAGE' then Result := 501
  else if AFieldName = 'FUTURE_MAXIMUM_WAGE' then Result := 500
  else if AFieldName = 'TAX_COUPON_SY_REPORTS_NBR' then Result := 513
  else if AFieldName = 'FREQUENCY_TYPE' then Result := 522
  else if AFieldName = 'W2_BOX' then Result := 512
  else if AFieldName = 'PAY_WITH_STATE' then Result := 521
  else if AFieldName = 'GLOBAL_RATE' then Result := 499
  else if AFieldName = 'E_D_CODE_TYPE' then Result := 511
  else if AFieldName = 'FTE_EXEMPTION' then Result := 510
  else if AFieldName = 'FTE_WEEKLY_HOURS' then Result := 509
  else if AFieldName = 'FTE_MULTIPLIER' then Result := 508
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 507
  else if AFieldName = 'ALTERNATE_TAXABLE_WAGE_BASE' then Result := 564
  else if AFieldName = 'CO_ADD_ALL_BUTTON' then Result := 565
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_SUI.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_SUI.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 10);
  Result[0] := 'SY_SUI_NBR';
  Result[1] := 'SY_STATES_NBR';
  Result[2] := 'SUI_TAX_NAME';
  Result[3] := 'EE_ER_OR_EE_OTHER_OR_ER_OTHER';
  Result[4] := 'USE_MISC_TAX_RETURN_CODE';
  Result[5] := 'ROUND_TO_NEAREST_DOLLAR';
  Result[6] := 'SUI_ACTIVE';
  Result[7] := 'FREQUENCY_TYPE';
  Result[8] := 'PAY_WITH_STATE';
  Result[9] := 'CO_ADD_ALL_BUTTON';
end;

class function TSY_SUI.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'SUI_TAX_NAME' then Result := 40
  else if FieldName = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER' then Result := 1
  else if FieldName = 'USE_MISC_TAX_RETURN_CODE' then Result := 1
  else if FieldName = 'FILLER' then Result := 512
  else if FieldName = 'ROUND_TO_NEAREST_DOLLAR' then Result := 1
  else if FieldName = 'SUI_ACTIVE' then Result := 1
  else if FieldName = 'FREQUENCY_TYPE' then Result := 1
  else if FieldName = 'W2_BOX' then Result := 10
  else if FieldName = 'PAY_WITH_STATE' then Result := 1
  else if FieldName = 'E_D_CODE_TYPE' then Result := 2
  else if FieldName = 'CO_ADD_ALL_BUTTON' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_SUI.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'NEW_COMPANY_DEFAULT_RATE' then Result := 6
  else if FieldName = 'FUTURE_DEFAULT_RATE' then Result := 6
  else if FieldName = 'MAXIMUM_WAGE' then Result := 6
  else if FieldName = 'FUTURE_MAXIMUM_WAGE' then Result := 6
  else if FieldName = 'GLOBAL_RATE' then Result := 6
  else if FieldName = 'ALTERNATE_TAXABLE_WAGE_BASE' then Result := 6
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_SUI.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_SUI_NBR';
end;

class function TSY_SUI.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_TAX_PMT_REPORT_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_SUI_REPORTING_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_SUI_TAX_PMT_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'TAX_COUPON_SY_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_SUI.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_SUI.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_STATES_NBR';
  Result[1] := 'SUI_TAX_NAME';
end;

{TSY_HR_REFUSAL_REASON}

class function TSY_HR_REFUSAL_REASON.GetClassIndex: Integer;
begin
  Result := 39;
end;

class function TSY_HR_REFUSAL_REASON.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_HR_REFUSAL_REASON.GetEvTableNbr: Integer;
begin
  Result := 41;
end;

class function TSY_HR_REFUSAL_REASON.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_HR_REFUSAL_REASON.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_HR_REFUSAL_REASON.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 533
  else if AFieldName = 'SY_HR_REFUSAL_REASON_NBR' then Result := 534
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 535
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 536
  else if AFieldName = 'HEALTHCARE_COVERAGE' then Result := 537
  else if AFieldName = 'SY_STATES_NBR' then Result := 538
  else if AFieldName = 'DESCRIPTION' then Result := 539
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_HR_REFUSAL_REASON.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_HR_REFUSAL_REASON.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_HR_REFUSAL_REASON_NBR';
  Result[1] := 'HEALTHCARE_COVERAGE';
  Result[2] := 'DESCRIPTION';
end;

class function TSY_HR_REFUSAL_REASON.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'HEALTHCARE_COVERAGE' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_HR_REFUSAL_REASON.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_HR_REFUSAL_REASON.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_HR_REFUSAL_REASON_NBR';
end;

class function TSY_HR_REFUSAL_REASON.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_STATES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_STATES_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_HR_REFUSAL_REASON.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_HR_REFUSAL_REASON.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'DESCRIPTION';
  Result[1] := 'SY_STATES_NBR';
end;

{TSY_AGENCY_DEPOSIT_FREQ}

class function TSY_AGENCY_DEPOSIT_FREQ.GetClassIndex: Integer;
begin
  Result := 40;
end;

class function TSY_AGENCY_DEPOSIT_FREQ.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetEvTableNbr: Integer;
begin
  Result := 42;
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_AGENCY_DEPOSIT_FREQ.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'FREQUENCY_TYPE' then Result := True
  else if FieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := True
  else if FieldName = 'SY_REPORTS_GROUP_NBR' then Result := True
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := True
  else if FieldName = 'WHEN_DUE_TYPE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 549
  else if AFieldName = 'SY_AGENCY_DEPOSIT_FREQ_NBR' then Result := 550
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 551
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 552
  else if AFieldName = 'DESCRIPTION' then Result := 553
  else if AFieldName = 'FREQUENCY_TYPE' then Result := 554
  else if AFieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE' then Result := 555
  else if AFieldName = 'SY_REPORTS_GROUP_NBR' then Result := 556
  else if AFieldName = 'SY_GLOBAL_AGENCY_NBR' then Result := 557
  else if AFieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 558
  else if AFieldName = 'WHEN_DUE_TYPE' then Result := 559
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 6);
  Result[0] := 'SY_AGENCY_DEPOSIT_FREQ_NBR';
  Result[1] := 'DESCRIPTION';
  Result[2] := 'FREQUENCY_TYPE';
  Result[3] := 'NUMBER_OF_DAYS_FOR_WHEN_DUE';
  Result[4] := 'SY_GLOBAL_AGENCY_NBR';
  Result[5] := 'WHEN_DUE_TYPE';
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 20
  else if FieldName = 'FREQUENCY_TYPE' then Result := 1
  else if FieldName = 'TAX_PAYMENT_TYPE_CODE' then Result := 10
  else if FieldName = 'WHEN_DUE_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_AGENCY_DEPOSIT_FREQ_NBR';
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_GLOBAL_AGENCY;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].DestFields[0] := 'SY_GLOBAL_AGENCY_NBR';
  Result[High(Result)].MasterDetail := True;
  Result[High(Result)].ParentEntityPropName := '';
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORTS_GROUP;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORTS_GROUP_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_AGENCY_DEPOSIT_FREQ.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'DESCRIPTION';
  Result[1] := 'SY_GLOBAL_AGENCY_NBR';
end;

{TSY_REPORTS_VERSIONS}

class function TSY_REPORTS_VERSIONS.GetClassIndex: Integer;
begin
  Result := 41;
end;

class function TSY_REPORTS_VERSIONS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_REPORTS_VERSIONS.GetEvTableNbr: Integer;
begin
  Result := 43;
end;

class function TSY_REPORTS_VERSIONS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_REPORTS_VERSIONS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_REPORTS_VERSIONS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 566
  else if AFieldName = 'SY_REPORTS_VERSIONS_NBR' then Result := 567
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 568
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 569
  else if AFieldName = 'DB_VERSION' then Result := 570
  else if AFieldName = 'STATUS' then Result := 571
  else if AFieldName = 'STATUS_DATE' then Result := 572
  else if AFieldName = 'STATUS_BY' then Result := 573
  else if AFieldName = 'CURRENT_REPORT' then Result := 574
  else if AFieldName = 'MODIFIED_ON' then Result := 575
  else if AFieldName = 'TICKET' then Result := 576
  else if AFieldName = 'SY_REPORT_WRITER_REPORTS_NBR' then Result := 577
  else if AFieldName = 'REPORT_DATA' then Result := 578
  else if AFieldName = 'CLASS_NAME' then Result := 579
  else if AFieldName = 'ANCESTOR_CLASS_NAME' then Result := 580
  else if AFieldName = 'NOTES' then Result := 581
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_REPORTS_VERSIONS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_REPORTS_VERSIONS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 7);
  Result[0] := 'SY_REPORTS_VERSIONS_NBR';
  Result[1] := 'STATUS';
  Result[2] := 'STATUS_DATE';
  Result[3] := 'STATUS_BY';
  Result[4] := 'CURRENT_REPORT';
  Result[5] := 'MODIFIED_ON';
  Result[6] := 'SY_REPORT_WRITER_REPORTS_NBR';
end;

class function TSY_REPORTS_VERSIONS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DB_VERSION' then Result := 20
  else if FieldName = 'STATUS' then Result := 1
  else if FieldName = 'CURRENT_REPORT' then Result := 1
  else if FieldName = 'TICKET' then Result := 20
  else if FieldName = 'CLASS_NAME' then Result := 40
  else if FieldName = 'ANCESTOR_CLASS_NAME' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_REPORTS_VERSIONS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_REPORTS_VERSIONS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_REPORTS_VERSIONS_NBR';
end;

class function TSY_REPORTS_VERSIONS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_REPORT_WRITER_REPORTS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].DestFields[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_REPORTS_VERSIONS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_REPORTS_VERSIONS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_REPORT_WRITER_REPORTS_NBR';
  Result[1] := 'DB_VERSION';
end;

{TSY_DASHBOARDS}

class function TSY_DASHBOARDS.GetClassIndex: Integer;
begin
  Result := 42;
end;

class function TSY_DASHBOARDS.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_DASHBOARDS.GetEvTableNbr: Integer;
begin
  Result := 44;
end;

class function TSY_DASHBOARDS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_DASHBOARDS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_DASHBOARDS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 584
  else if AFieldName = 'SY_DASHBOARDS_NBR' then Result := 585
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 586
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 587
  else if AFieldName = 'DASHBOARD_TYPE' then Result := 588
  else if AFieldName = 'SY_ANALYTICS_TIER_NBR' then Result := 589
  else if AFieldName = 'NOTES' then Result := 590
  else if AFieldName = 'DASHBOARD_ID' then Result := 591
  else if AFieldName = 'DESCRIPTION' then Result := 592
  else if AFieldName = 'RELEASED' then Result := 619
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_DASHBOARDS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_DASHBOARDS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 4);
  Result[0] := 'SY_DASHBOARDS_NBR';
  Result[1] := 'SY_ANALYTICS_TIER_NBR';
  Result[2] := 'DASHBOARD_ID';
  Result[3] := 'DESCRIPTION';
end;

class function TSY_DASHBOARDS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DASHBOARD_TYPE' then Result := 1
  else if FieldName = 'DESCRIPTION' then Result := 80
  else if FieldName = 'RELEASED' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_DASHBOARDS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_DASHBOARDS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_DASHBOARDS_NBR';
end;

class function TSY_DASHBOARDS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_ANALYTICS_TIER;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_ANALYTICS_TIER_NBR';
  Result[High(Result)].DestFields[0] := 'SY_ANALYTICS_TIER_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_DASHBOARDS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_DASHBOARDS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_ANALYTICS_TIER}

class function TSY_ANALYTICS_TIER.GetClassIndex: Integer;
begin
  Result := 43;
end;

class function TSY_ANALYTICS_TIER.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_ANALYTICS_TIER.GetEvTableNbr: Integer;
begin
  Result := 45;
end;

class function TSY_ANALYTICS_TIER.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_ANALYTICS_TIER.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'TIER_ID' then Result := True
  else if FieldName = 'DASHBOARD_DEFAULT_COUNT' then Result := True
  else if FieldName = 'DASHBOARD_MAX_COUNT' then Result := True
  else if FieldName = 'USERS_DEFAULT_COUNT' then Result := True
  else if FieldName = 'USERS_MAX_COUNT' then Result := True
  else if FieldName = 'LOOKBACK_YEARS_DEFAULT' then Result := True
  else if FieldName = 'LOOKBACK_YEARS_MAX' then Result := True
  else if FieldName = 'MOBILE_APP' then Result := True
  else if FieldName = 'INTERNAL_BENCHMARKING' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_ANALYTICS_TIER.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 593
  else if AFieldName = 'SY_ANALYTICS_TIER_NBR' then Result := 594
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 595
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 596
  else if AFieldName = 'TIER_ID' then Result := 597
  else if AFieldName = 'DASHBOARD_DEFAULT_COUNT' then Result := 598
  else if AFieldName = 'DASHBOARD_MAX_COUNT' then Result := 599
  else if AFieldName = 'USERS_DEFAULT_COUNT' then Result := 600
  else if AFieldName = 'USERS_MAX_COUNT' then Result := 601
  else if AFieldName = 'LOOKBACK_YEARS_DEFAULT' then Result := 602
  else if AFieldName = 'LOOKBACK_YEARS_MAX' then Result := 603
  else if AFieldName = 'MOBILE_APP' then Result := 604
  else if AFieldName = 'INTERNAL_BENCHMARKING' then Result := 605
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_ANALYTICS_TIER.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_ANALYTICS_TIER.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_ANALYTICS_TIER_NBR';
  Result[1] := 'TIER_ID';
end;

class function TSY_ANALYTICS_TIER.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'TIER_ID' then Result := 40
  else if FieldName = 'MOBILE_APP' then Result := 1
  else if FieldName = 'INTERNAL_BENCHMARKING' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_ANALYTICS_TIER.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_ANALYTICS_TIER.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_ANALYTICS_TIER_NBR';
end;

class function TSY_ANALYTICS_TIER.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_ANALYTICS_TIER.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_DASHBOARDS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_ANALYTICS_TIER_NBR';
  Result[High(Result)].DestFields[0] := 'SY_ANALYTICS_TIER_NBR';
end;

class function TSY_ANALYTICS_TIER.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_VENDORS}

class function TSY_VENDORS.GetClassIndex: Integer;
begin
  Result := 44;
end;

class function TSY_VENDORS.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_VENDORS.GetEvTableNbr: Integer;
begin
  Result := 47;
end;

class function TSY_VENDORS.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_VENDORS.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'VENDOR_NAME' then Result := True
  else if FieldName = 'VENDOR_TYPE' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_VENDORS.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 611
  else if AFieldName = 'SY_VENDORS_NBR' then Result := 612
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 613
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 614
  else if AFieldName = 'VENDOR_NAME' then Result := 615
  else if AFieldName = 'SY_VENDOR_CATEGORIES_NBR' then Result := 616
  else if AFieldName = 'VENDOR_TYPE' then Result := 617
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_VENDORS.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_VENDORS.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_VENDORS_NBR';
  Result[1] := 'VENDOR_NAME';
  Result[2] := 'SY_VENDOR_CATEGORIES_NBR';
end;

class function TSY_VENDORS.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'VENDOR_NAME' then Result := 80
  else if FieldName = 'VENDOR_TYPE' then Result := 1
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_VENDORS.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_VENDORS.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_VENDORS_NBR';
end;

class function TSY_VENDORS.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_VENDOR_CATEGORIES;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_VENDOR_CATEGORIES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_VENDOR_CATEGORIES_NBR';
  Result[High(Result)].MasterDetail := False;
  Result[High(Result)].ParentEntityPropName := '';
end;

class function TSY_VENDORS.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_VENDORS.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_VENDOR_CATEGORIES}

class function TSY_VENDOR_CATEGORIES.GetClassIndex: Integer;
begin
  Result := 45;
end;

class function TSY_VENDOR_CATEGORIES.IsVersionedTable: Boolean;
begin
  Result := False;
end;

class function TSY_VENDOR_CATEGORIES.GetEvTableNbr: Integer;
begin
  Result := 46;
end;

class function TSY_VENDOR_CATEGORIES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_VENDOR_CATEGORIES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_VENDOR_CATEGORIES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 606
  else if AFieldName = 'SY_VENDOR_CATEGORIES_NBR' then Result := 607
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 608
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 609
  else if AFieldName = 'DESCRIPTION' then Result := 610
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_VENDOR_CATEGORIES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_VENDOR_CATEGORIES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 2);
  Result[0] := 'SY_VENDOR_CATEGORIES_NBR';
  Result[1] := 'DESCRIPTION';
end;

class function TSY_VENDOR_CATEGORIES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_VENDOR_CATEGORIES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_VENDOR_CATEGORIES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_VENDOR_CATEGORIES_NBR';
end;

class function TSY_VENDOR_CATEGORIES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_VENDOR_CATEGORIES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
  SetLength(Result, Succ(Length(Result)));
  Result[High(Result)].Database := TDM_SYSTEM_;
  Result[High(Result)].Table := TSY_VENDORS;
  SetLength(Result[High(Result)].SrcFields, 1);
  SetLength(Result[High(Result)].DestFields, 1);
  Result[High(Result)].SrcFields[0] := 'SY_VENDOR_CATEGORIES_NBR';
  Result[High(Result)].DestFields[0] := 'SY_VENDOR_CATEGORIES_NBR';
end;

class function TSY_VENDOR_CATEGORIES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_FED_ACA_OFFER_CODES}

class function TSY_FED_ACA_OFFER_CODES.GetClassIndex: Integer;
begin
  Result := 46;
end;

class function TSY_FED_ACA_OFFER_CODES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_FED_ACA_OFFER_CODES.GetEvTableNbr: Integer;
begin
  Result := 48;
end;

class function TSY_FED_ACA_OFFER_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_ACA_OFFER_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ACA_OFFER_CODE' then Result := True
  else if FieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_ACA_OFFER_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 621
  else if AFieldName = 'SY_FED_ACA_OFFER_CODES_NBR' then Result := 622
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 623
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 624
  else if AFieldName = 'ACA_OFFER_CODE' then Result := 625
  else if AFieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := 626
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_ACA_OFFER_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_ACA_OFFER_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_FED_ACA_OFFER_CODES_NBR';
  Result[1] := 'ACA_OFFER_CODE';
  Result[2] := 'ACA_OFFER_CODE_DESCRIPTION';
end;

class function TSY_FED_ACA_OFFER_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ACA_OFFER_CODE' then Result := 2
  else if FieldName = 'ACA_OFFER_CODE_DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_ACA_OFFER_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_ACA_OFFER_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_ACA_OFFER_CODES_NBR';
end;

class function TSY_FED_ACA_OFFER_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_FED_ACA_OFFER_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_ACA_OFFER_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TSY_FED_ACA_RELIEF_CODES}

class function TSY_FED_ACA_RELIEF_CODES.GetClassIndex: Integer;
begin
  Result := 47;
end;

class function TSY_FED_ACA_RELIEF_CODES.IsVersionedTable: Boolean;
begin
  Result := True;
end;

class function TSY_FED_ACA_RELIEF_CODES.GetEvTableNbr: Integer;
begin
  Result := 49;
end;

class function TSY_FED_ACA_RELIEF_CODES.GetEntityName: String;
begin
  Result := '';
end;

class function TSY_FED_ACA_RELIEF_CODES.IsVersionedField(FieldName: string): Boolean;
begin
  if false then
  else if FieldName = 'ACA_RELIEF_CODE' then Result := True
  else if FieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := True
  else Result := inherited IsVersionedField(FieldName);
end;

class function TSY_FED_ACA_RELIEF_CODES.GetEvFieldNbr(const AFieldName: string): Integer;
begin
  if false then
  else if AFieldName = 'REC_VERSION' then Result := 627
  else if AFieldName = 'SY_FED_ACA_RELIEF_CODES_NBR' then Result := 628
  else if AFieldName = 'EFFECTIVE_DATE' then Result := 629
  else if AFieldName = 'EFFECTIVE_UNTIL' then Result := 630
  else if AFieldName = 'ACA_RELIEF_CODE' then Result := 631
  else if AFieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := 632
  else Result := inherited GetEvFieldNbr(AFieldName);
end;

class function TSY_FED_ACA_RELIEF_CODES.GetFieldEntityProperty(const FieldName: string): String;
begin
  if false then
  else Result := inherited GetFieldEntityProperty(FieldName);
end;

class function TSY_FED_ACA_RELIEF_CODES.GetRequiredFields: TddFieldList;
begin
  SetLength(Result, 3);
  Result[0] := 'SY_FED_ACA_RELIEF_CODES_NBR';
  Result[1] := 'ACA_RELIEF_CODE';
  Result[2] := 'ACA_RELIEF_CODE_DESCRIPTION';
end;

class function TSY_FED_ACA_RELIEF_CODES.GetFieldSize(FieldName: string): integer;
begin
  if false then
  else if FieldName = 'ACA_RELIEF_CODE' then Result := 2
  else if FieldName = 'ACA_RELIEF_CODE_DESCRIPTION' then Result := 40
  else Result := inherited GetFieldSize(FieldName);
end;

class function TSY_FED_ACA_RELIEF_CODES.GetFieldPrecision(FieldName: string): integer;
begin
  if false then
  else Result := inherited GetFieldPrecision(FieldName);
end;

class function TSY_FED_ACA_RELIEF_CODES.GetPrimaryKey: TddFieldList;
begin
  SetLength(Result, 1);
  Result[0] := 'SY_FED_ACA_RELIEF_CODES_NBR';
end;

class function TSY_FED_ACA_RELIEF_CODES.GetParentsDesc: TddReferencesDesc;
begin
  Result := inherited GetParentsDesc;
end;

class function TSY_FED_ACA_RELIEF_CODES.GetChildrenDesc: TddReferencesDesc;
begin
  Result := inherited GetChildrenDesc;
end;

class function TSY_FED_ACA_RELIEF_CODES.GetLogicalKeys: TddFieldList;
begin
  SetLength(Result, 0);
end;

{TDM_SYSTEM_}

class function TDM_SYSTEM_.GetDBType: TevDBType;
begin
  Result := dbtSystem;
end;

function TDM_SYSTEM_.GetSY_COUNTY: TSY_COUNTY;
begin
  Result := GetDataSet(TSY_COUNTY) as TSY_COUNTY;
end;

function TDM_SYSTEM_.GetSY_DELIVERY_METHOD: TSY_DELIVERY_METHOD;
begin
  Result := GetDataSet(TSY_DELIVERY_METHOD) as TSY_DELIVERY_METHOD;
end;

function TDM_SYSTEM_.GetSY_DELIVERY_SERVICE: TSY_DELIVERY_SERVICE;
begin
  Result := GetDataSet(TSY_DELIVERY_SERVICE) as TSY_DELIVERY_SERVICE;
end;

function TDM_SYSTEM_.GetSY_FED_EXEMPTIONS: TSY_FED_EXEMPTIONS;
begin
  Result := GetDataSet(TSY_FED_EXEMPTIONS) as TSY_FED_EXEMPTIONS;
end;

function TDM_SYSTEM_.GetSY_FED_REPORTING_AGENCY: TSY_FED_REPORTING_AGENCY;
begin
  Result := GetDataSet(TSY_FED_REPORTING_AGENCY) as TSY_FED_REPORTING_AGENCY;
end;

function TDM_SYSTEM_.GetSY_FED_TAX_PAYMENT_AGENCY: TSY_FED_TAX_PAYMENT_AGENCY;
begin
  Result := GetDataSet(TSY_FED_TAX_PAYMENT_AGENCY) as TSY_FED_TAX_PAYMENT_AGENCY;
end;

function TDM_SYSTEM_.GetSY_FED_TAX_TABLE: TSY_FED_TAX_TABLE;
begin
  Result := GetDataSet(TSY_FED_TAX_TABLE) as TSY_FED_TAX_TABLE;
end;

function TDM_SYSTEM_.GetSY_FED_TAX_TABLE_BRACKETS: TSY_FED_TAX_TABLE_BRACKETS;
begin
  Result := GetDataSet(TSY_FED_TAX_TABLE_BRACKETS) as TSY_FED_TAX_TABLE_BRACKETS;
end;

function TDM_SYSTEM_.GetSY_GLOBAL_AGENCY: TSY_GLOBAL_AGENCY;
begin
  Result := GetDataSet(TSY_GLOBAL_AGENCY) as TSY_GLOBAL_AGENCY;
end;

function TDM_SYSTEM_.GetSY_GL_AGENCY_FIELD_OFFICE: TSY_GL_AGENCY_FIELD_OFFICE;
begin
  Result := GetDataSet(TSY_GL_AGENCY_FIELD_OFFICE) as TSY_GL_AGENCY_FIELD_OFFICE;
end;

function TDM_SYSTEM_.GetSY_GL_AGENCY_HOLIDAYS: TSY_GL_AGENCY_HOLIDAYS;
begin
  Result := GetDataSet(TSY_GL_AGENCY_HOLIDAYS) as TSY_GL_AGENCY_HOLIDAYS;
end;

function TDM_SYSTEM_.GetSY_GL_AGENCY_REPORT: TSY_GL_AGENCY_REPORT;
begin
  Result := GetDataSet(TSY_GL_AGENCY_REPORT) as TSY_GL_AGENCY_REPORT;
end;

function TDM_SYSTEM_.GetSY_HR_EEO: TSY_HR_EEO;
begin
  Result := GetDataSet(TSY_HR_EEO) as TSY_HR_EEO;
end;

function TDM_SYSTEM_.GetSY_HR_ETHNICITY: TSY_HR_ETHNICITY;
begin
  Result := GetDataSet(TSY_HR_ETHNICITY) as TSY_HR_ETHNICITY;
end;

function TDM_SYSTEM_.GetSY_HR_HANDICAPS: TSY_HR_HANDICAPS;
begin
  Result := GetDataSet(TSY_HR_HANDICAPS) as TSY_HR_HANDICAPS;
end;

function TDM_SYSTEM_.GetSY_HR_INJURY_CODES: TSY_HR_INJURY_CODES;
begin
  Result := GetDataSet(TSY_HR_INJURY_CODES) as TSY_HR_INJURY_CODES;
end;

function TDM_SYSTEM_.GetSY_HR_OSHA_ANATOMIC_CODES: TSY_HR_OSHA_ANATOMIC_CODES;
begin
  Result := GetDataSet(TSY_HR_OSHA_ANATOMIC_CODES) as TSY_HR_OSHA_ANATOMIC_CODES;
end;

function TDM_SYSTEM_.GetSY_LOCALS: TSY_LOCALS;
begin
  Result := GetDataSet(TSY_LOCALS) as TSY_LOCALS;
end;

function TDM_SYSTEM_.GetSY_LOCAL_DEPOSIT_FREQ: TSY_LOCAL_DEPOSIT_FREQ;
begin
  Result := GetDataSet(TSY_LOCAL_DEPOSIT_FREQ) as TSY_LOCAL_DEPOSIT_FREQ;
end;

function TDM_SYSTEM_.GetSY_LOCAL_EXEMPTIONS: TSY_LOCAL_EXEMPTIONS;
begin
  Result := GetDataSet(TSY_LOCAL_EXEMPTIONS) as TSY_LOCAL_EXEMPTIONS;
end;

function TDM_SYSTEM_.GetSY_LOCAL_MARITAL_STATUS: TSY_LOCAL_MARITAL_STATUS;
begin
  Result := GetDataSet(TSY_LOCAL_MARITAL_STATUS) as TSY_LOCAL_MARITAL_STATUS;
end;

function TDM_SYSTEM_.GetSY_LOCAL_TAX_CHART: TSY_LOCAL_TAX_CHART;
begin
  Result := GetDataSet(TSY_LOCAL_TAX_CHART) as TSY_LOCAL_TAX_CHART;
end;

function TDM_SYSTEM_.GetSY_MEDIA_TYPE: TSY_MEDIA_TYPE;
begin
  Result := GetDataSet(TSY_MEDIA_TYPE) as TSY_MEDIA_TYPE;
end;

function TDM_SYSTEM_.GetSY_QUEUE_PRIORITY: TSY_QUEUE_PRIORITY;
begin
  Result := GetDataSet(TSY_QUEUE_PRIORITY) as TSY_QUEUE_PRIORITY;
end;

function TDM_SYSTEM_.GetSY_RECIPROCATED_STATES: TSY_RECIPROCATED_STATES;
begin
  Result := GetDataSet(TSY_RECIPROCATED_STATES) as TSY_RECIPROCATED_STATES;
end;

function TDM_SYSTEM_.GetSY_REPORTS: TSY_REPORTS;
begin
  Result := GetDataSet(TSY_REPORTS) as TSY_REPORTS;
end;

function TDM_SYSTEM_.GetSY_REPORTS_GROUP: TSY_REPORTS_GROUP;
begin
  Result := GetDataSet(TSY_REPORTS_GROUP) as TSY_REPORTS_GROUP;
end;

function TDM_SYSTEM_.GetSY_REPORT_GROUPS: TSY_REPORT_GROUPS;
begin
  Result := GetDataSet(TSY_REPORT_GROUPS) as TSY_REPORT_GROUPS;
end;

function TDM_SYSTEM_.GetSY_REPORT_GROUP_MEMBERS: TSY_REPORT_GROUP_MEMBERS;
begin
  Result := GetDataSet(TSY_REPORT_GROUP_MEMBERS) as TSY_REPORT_GROUP_MEMBERS;
end;

function TDM_SYSTEM_.GetSY_REPORT_WRITER_REPORTS: TSY_REPORT_WRITER_REPORTS;
begin
  Result := GetDataSet(TSY_REPORT_WRITER_REPORTS) as TSY_REPORT_WRITER_REPORTS;
end;

function TDM_SYSTEM_.GetSY_SEC_TEMPLATES: TSY_SEC_TEMPLATES;
begin
  Result := GetDataSet(TSY_SEC_TEMPLATES) as TSY_SEC_TEMPLATES;
end;

function TDM_SYSTEM_.GetSY_STATES: TSY_STATES;
begin
  Result := GetDataSet(TSY_STATES) as TSY_STATES;
end;

function TDM_SYSTEM_.GetSY_STATE_DEPOSIT_FREQ: TSY_STATE_DEPOSIT_FREQ;
begin
  Result := GetDataSet(TSY_STATE_DEPOSIT_FREQ) as TSY_STATE_DEPOSIT_FREQ;
end;

function TDM_SYSTEM_.GetSY_STATE_EXEMPTIONS: TSY_STATE_EXEMPTIONS;
begin
  Result := GetDataSet(TSY_STATE_EXEMPTIONS) as TSY_STATE_EXEMPTIONS;
end;

function TDM_SYSTEM_.GetSY_STATE_MARITAL_STATUS: TSY_STATE_MARITAL_STATUS;
begin
  Result := GetDataSet(TSY_STATE_MARITAL_STATUS) as TSY_STATE_MARITAL_STATUS;
end;

function TDM_SYSTEM_.GetSY_STATE_TAX_CHART: TSY_STATE_TAX_CHART;
begin
  Result := GetDataSet(TSY_STATE_TAX_CHART) as TSY_STATE_TAX_CHART;
end;

function TDM_SYSTEM_.GetSY_STORAGE: TSY_STORAGE;
begin
  Result := GetDataSet(TSY_STORAGE) as TSY_STORAGE;
end;

function TDM_SYSTEM_.GetSY_SUI: TSY_SUI;
begin
  Result := GetDataSet(TSY_SUI) as TSY_SUI;
end;

function TDM_SYSTEM_.GetSY_HR_REFUSAL_REASON: TSY_HR_REFUSAL_REASON;
begin
  Result := GetDataSet(TSY_HR_REFUSAL_REASON) as TSY_HR_REFUSAL_REASON;
end;

function TDM_SYSTEM_.GetSY_AGENCY_DEPOSIT_FREQ: TSY_AGENCY_DEPOSIT_FREQ;
begin
  Result := GetDataSet(TSY_AGENCY_DEPOSIT_FREQ) as TSY_AGENCY_DEPOSIT_FREQ;
end;

function TDM_SYSTEM_.GetSY_REPORTS_VERSIONS: TSY_REPORTS_VERSIONS;
begin
  Result := GetDataSet(TSY_REPORTS_VERSIONS) as TSY_REPORTS_VERSIONS;
end;

function TDM_SYSTEM_.GetSY_DASHBOARDS: TSY_DASHBOARDS;
begin
  Result := GetDataSet(TSY_DASHBOARDS) as TSY_DASHBOARDS;
end;

function TDM_SYSTEM_.GetSY_ANALYTICS_TIER: TSY_ANALYTICS_TIER;
begin
  Result := GetDataSet(TSY_ANALYTICS_TIER) as TSY_ANALYTICS_TIER;
end;

function TDM_SYSTEM_.GetSY_VENDORS: TSY_VENDORS;
begin
  Result := GetDataSet(TSY_VENDORS) as TSY_VENDORS;
end;

function TDM_SYSTEM_.GetSY_VENDOR_CATEGORIES: TSY_VENDOR_CATEGORIES;
begin
  Result := GetDataSet(TSY_VENDOR_CATEGORIES) as TSY_VENDOR_CATEGORIES;
end;

function TDM_SYSTEM_.GetSY_FED_ACA_OFFER_CODES: TSY_FED_ACA_OFFER_CODES;
begin
  Result := GetDataSet(TSY_FED_ACA_OFFER_CODES) as TSY_FED_ACA_OFFER_CODES;
end;

function TDM_SYSTEM_.GetSY_FED_ACA_RELIEF_CODES: TSY_FED_ACA_RELIEF_CODES;
begin
  Result := GetDataSet(TSY_FED_ACA_RELIEF_CODES) as TSY_FED_ACA_RELIEF_CODES;
end;

procedure Register;
begin
  RegisterComponents('EvoDdSystem', [
    TSY_COUNTY,
    TSY_DELIVERY_METHOD,
    TSY_DELIVERY_SERVICE,
    TSY_FED_EXEMPTIONS,
    TSY_FED_REPORTING_AGENCY,
    TSY_FED_TAX_PAYMENT_AGENCY,
    TSY_FED_TAX_TABLE,
    TSY_FED_TAX_TABLE_BRACKETS,
    TSY_GLOBAL_AGENCY,
    TSY_GL_AGENCY_FIELD_OFFICE,
    TSY_GL_AGENCY_HOLIDAYS,
    TSY_GL_AGENCY_REPORT,
    TSY_HR_EEO,
    TSY_HR_ETHNICITY,
    TSY_HR_HANDICAPS,
    TSY_HR_INJURY_CODES,
    TSY_HR_OSHA_ANATOMIC_CODES,
    TSY_LOCALS,
    TSY_LOCAL_DEPOSIT_FREQ,
    TSY_LOCAL_EXEMPTIONS,
    TSY_LOCAL_MARITAL_STATUS,
    TSY_LOCAL_TAX_CHART,
    TSY_MEDIA_TYPE,
    TSY_QUEUE_PRIORITY,
    TSY_RECIPROCATED_STATES,
    TSY_REPORTS,
    TSY_REPORTS_GROUP,
    TSY_REPORT_GROUPS,
    TSY_REPORT_GROUP_MEMBERS,
    TSY_REPORT_WRITER_REPORTS,
    TSY_SEC_TEMPLATES,
    TSY_STATES,
    TSY_STATE_DEPOSIT_FREQ,
    TSY_STATE_EXEMPTIONS,
    TSY_STATE_MARITAL_STATUS,
    TSY_STATE_TAX_CHART,
    TSY_STORAGE,
    TSY_SUI,
    TSY_HR_REFUSAL_REASON,
    TSY_AGENCY_DEPOSIT_FREQ,
    TSY_REPORTS_VERSIONS,
    TSY_DASHBOARDS,
    TSY_ANALYTICS_TIER,
    TSY_VENDORS,
    TSY_VENDOR_CATEGORIES,
    TSY_FED_ACA_OFFER_CODES,
    TSY_FED_ACA_RELIEF_CODES]);
end;

initialization
  RegisterClasses([
    TSY_COUNTY,
    TSY_DELIVERY_METHOD,
    TSY_DELIVERY_SERVICE,
    TSY_FED_EXEMPTIONS,
    TSY_FED_REPORTING_AGENCY,
    TSY_FED_TAX_PAYMENT_AGENCY,
    TSY_FED_TAX_TABLE,
    TSY_FED_TAX_TABLE_BRACKETS,
    TSY_GLOBAL_AGENCY,
    TSY_GL_AGENCY_FIELD_OFFICE,
    TSY_GL_AGENCY_HOLIDAYS,
    TSY_GL_AGENCY_REPORT,
    TSY_HR_EEO,
    TSY_HR_ETHNICITY,
    TSY_HR_HANDICAPS,
    TSY_HR_INJURY_CODES,
    TSY_HR_OSHA_ANATOMIC_CODES,
    TSY_LOCALS,
    TSY_LOCAL_DEPOSIT_FREQ,
    TSY_LOCAL_EXEMPTIONS,
    TSY_LOCAL_MARITAL_STATUS,
    TSY_LOCAL_TAX_CHART,
    TSY_MEDIA_TYPE,
    TSY_QUEUE_PRIORITY,
    TSY_RECIPROCATED_STATES,
    TSY_REPORTS,
    TSY_REPORTS_GROUP,
    TSY_REPORT_GROUPS,
    TSY_REPORT_GROUP_MEMBERS,
    TSY_REPORT_WRITER_REPORTS,
    TSY_SEC_TEMPLATES,
    TSY_STATES,
    TSY_STATE_DEPOSIT_FREQ,
    TSY_STATE_EXEMPTIONS,
    TSY_STATE_MARITAL_STATUS,
    TSY_STATE_TAX_CHART,
    TSY_STORAGE,
    TSY_SUI,
    TSY_HR_REFUSAL_REASON,
    TSY_AGENCY_DEPOSIT_FREQ,
    TSY_REPORTS_VERSIONS,
    TSY_DASHBOARDS,
    TSY_ANALYTICS_TIER,
    TSY_VENDORS,
    TSY_VENDOR_CATEGORIES,
    TSY_FED_ACA_OFFER_CODES,
    TSY_FED_ACA_RELIEF_CODES]);

end.
