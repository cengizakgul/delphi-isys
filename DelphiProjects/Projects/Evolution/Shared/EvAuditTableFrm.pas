unit EvAuditTableFrm;

interface

uses
  Windows, Forms, Classes, Controls, StdCtrls, Variants, EvUIComponents, SysUtils,
  Grids, Wwdbigrd, Wwdbgrid, DB, Wwdatsrc, Buttons, ExtCtrls, DateUtils,
  evDataSet, isDataSet, EvCommonInterfaces, evContext, EvUtils, EvConsts,
  isBasicUtils, EvStreamUtils, ISBasicClasses, isTypes, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isBaseClasses, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, isUIwwDBComboBox, SDataStructure, SDDClasses;

type
  TevAuditTable = class(TFrame)
    pnlParams: TevPanel;
    grVersionAudit: TevDBGrid;
    dsrcVersionAudit: TevDataSource;
    cbOperationType: TevDBComboBox;
    procedure OnRecordInteract(Sender: TObject);
    procedure grVersionAuditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure grVersionAuditCreatingSortIndex(Sender: TObject;
      var AFields: String; var AOptions: TIndexOptions;
      var ADescFields: String);
    procedure grVersionAuditSortingChanged(Sender: TObject);
  private
    FTable: String;
    FStartDate: TisDate;
    FChangesDS: IevDataSet;
    procedure BuildAuditData;
  public
    procedure Init(const ATable: String; const AStartDate: TisDate); reintroduce;
  end;

implementation

uses EvAuditViewFrm;

{$R *.dfm}



{ TevAuditRecord }

procedure TevAuditTable.Init(const ATable: String; const AStartDate: TisDate);
begin
  if (FTable <> ATable) or (FStartDate <> AStartDate) then
  begin
    FTable := ATable;
    FStartDate := AStartDate;

    BuildAuditData;
  end;
end;

procedure TevAuditTable.BuildAuditData;
begin
  FChangesDS := ctx_DBAccess.GetTableAudit(FTable, FStartDate);
  FChangesDS.IndexFieldNames := 'change_date';
  dsrcVersionAudit.DataSet := FChangesDS.vclDataSet;
end;

procedure TevAuditTable.OnRecordInteract(Sender: TObject);
begin
  if FChangesDS.RecNo > 0 then
    TevAuditView.ShowRecordAudit(FTable, FChangesDS['nbr'], Trunc(FChangesDS['change_date']));
end;

procedure TevAuditTable.grVersionAuditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    OnRecordInteract(Self);
end;

procedure TevAuditTable.grVersionAuditCreatingSortIndex(Sender: TObject;
  var AFields: String; var AOptions: TIndexOptions; var ADescFields: String);
begin
  if (AFields <> '') and not AnsiSameText(AFields, 'change_date') or
     (ADescFields <> '') and not AnsiSameText(ADescFields, 'change_date')
  then
  begin
    AFields := '';
    ADescFields := '';
  end
  else
    AFields := 'change_date';
end;

procedure TevAuditTable.grVersionAuditSortingChanged(Sender: TObject);
begin
  FChangesDS.First;
end;

end.

