unit rwEvUtils;

interface

uses Classes, SysUtils, Windows, Dialogs,  EvStreamUtils, EvUtils, EvConsts, SDataStructure,
     DB, SyncObjs, EvTypes , ISBasicClasses, Variants, SReportSettings, rwEngineTypes, isBaseClasses,
     EvBasicUtils, ISZippingRoutines, ISBasicUtils, EvDataAccessComponents, ISDataAccessComponents,
     EvContext, evExceptions, evDataSet, EvCommonInterfaces, EvUIUtils;

const
  {SY_Storage IDs}
  RW_DATA_DICTIONARY = 'RW_DATA_DICTIONARY';
  RW_QB_TEMPLATES =    'RW_QB_TEMPLATES';
  RW_QB_CONSTANTS =    'RW_QB_CONSTANTS';
  RW_LIB_COMPONENTS =  'RW_LIB_COMPONENTS';
  RW_LIB_FUNCTIONS =   'RW_LIB_FUNCTIONS';


var rwCriticalSection: TCriticalSection = nil;

function  SaveStreamToStorage(AStream: TStream; ATag: string; var AReadDate: TDateTime): Boolean;
function  LoadStreamFromStorage(AStream: TStream; ATag: string; AAsOfDate: TDateTime = 0): TDateTime;
function  GetUniqueFileName(const APath: string; const APrefix: string; const AExt: string): string;

procedure CheckLocalMode;

procedure GetReportRes(const Report_NBR: Integer; const ALevel: String; const ACheckReportVersion: Boolean;
                       out ReportType: TReportType; out ReportName: String; const AData: IEvDualStream; out MediaType: String;
                       out ReportClassName: String; out AncestorClassName: String);


function   EvRepParamsToParamList(const EvRepParams: TrwReportParams): TrwReportParamList;
procedure  ParamListToEvRepParams(const ReportParams: TrwReportParamList; const EvRepParams: TrwReportParams);
function   DumpParams(const AParams: TrwReportParams; AFileNamePriff: String = 'Report'): String;

function   NormalizeFileName(const AFileName: String): String;


implementation

uses DateUtils;

var
  FCachedReports: TISBasicClientDataSet = nil;
  rwParamsFolder: String = '';

function CachedReports: TISBasicClientDataSet;
var
  DS: TISBasicClientDataSet;
begin
  if not Assigned(FCachedReports) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(FCachedReports) then
      begin
        DS := TISBasicClientDataSet.Create(nil);
        DS.FieldDefs.Add('Level', ftString, 40);
        DS.FieldDefs.Add('Nbr', ftInteger);
        DS.FieldDefs.Add('Name', ftString, 40);
        DS.FieldDefs.Add('Type', ftString, 1);
        DS.FieldDefs.Add('Media_type', ftString, 2);
        DS.FieldDefs.Add('Class_Name', ftString, 40);
        DS.FieldDefs.Add('Ancestor_Class_Name', ftString, 40);
        DS.FieldDefs.Add('Data', ftBlob);
        DS.FieldDefs.Add('Verion', ftDateTime);
        DS.FieldDefs.Add('Rec_Version', ftInteger);
        DS.CreateDataSet;
        FCachedReports := DS;
      end;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FCachedReports;
end;

procedure CheckLocalMode;
begin
  if UnAttended then
    raise EevException.Create('Dialog form cannot be shown in this context');
end;

function SaveStreamToStorage(AStream: TStream; ATag: string; var AReadDate: TDateTime): Boolean;
var
  MS: IEvDualStream;
  Q: IevQuery;
  fl: Boolean;
  un: String;
  rec_version: Integer;
begin
  Result := False;

  if AReadDate <> 0 then
  begin
    Q := TevQuery.Create('SELECT rec_version FROM sy_storage WHERE {AsOfNow<sy_storage>} AND tag = :tag');
    Q.Params.AddValue('tag', ATag);

    Q.Execute;
    rec_version := Q.Result.Fields[0].AsInteger;

    Q := TevQuery.Create(Format('{SystemLastTableRecVerChange<%s,%d>}', ['SY_STORAGE', rec_version]));
    Q.Execute;

    fl := (Q.Result.RecordCount > 0) and not DateTimeCompare(Q.Result.FieldByName('change_date').AsDateTime, coEqual, AReadDate);
    if fl then
    begin
      DM_SERVICE_BUREAU.SB_USER.CheckDSCondition('');
      ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_USER]);
      if DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR', Q.Result.FieldByName('changed_by').AsInteger, []) then
        un := DM_SERVICE_BUREAU.SB_USER.FieldByName('USER_ID').AsString
      else
        un := 'Unknown';
    end;

    if fl then
    begin
      EvMessage('Data has been changed by another user ('+ un +
       '). Saving operation has been aborted.', mtError, [mbOk]);
      Exit;
    end;
  end;

  MS := TEvDualStreamHolder.Create;
  AStream.Position := 0;
  DeflateStream(AStream, MS.RealStream);
  MS.Position := 0;

  DM_SYSTEM_MISC.SY_STORAGE.CheckDSCondition('tag = ''' + ATag + '''');
  ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
  if DM_SYSTEM_MISC.SY_STORAGE.RecordCount = 0 then
  begin
    DM_SYSTEM_MISC.SY_STORAGE.Append;
    DM_SYSTEM_MISC.SY_STORAGE.FieldByName('TAG').AsString := ATag;
  end
  else
    DM_SYSTEM_MISC.SY_STORAGE.Edit;
  TBlobField(DM_SYSTEM_MISC.SY_STORAGE.FieldByName('STORAGE_DATA')).LoadFromStream(MS.RealStream);
  DM_SYSTEM_MISC.SY_STORAGE.Post;
  ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
  AReadDate := SysTime;

  Result := True;
end;


function LoadStreamFromStorage(AStream: TStream; ATag: string; AAsOfDate: TDateTime = 0): TDateTime;
var
  MS: IEvDualStream;
  Q: IevQuery;
  rec_version: Integer;
begin
  MS := TEvDualStreamHolder.Create;

  Q := TevQuery.Create('SELECT rec_version, storage_data FROM sy_storage WHERE {AsOfDate<sy_storage>} AND tag = :tag');
  if AAsOfDate <> 0 then
    Q.Params.AddValue('StatusDate', AAsOfDate)
  else
    Q.Params.AddValue('StatusDate', 'now');
  Q.Params.AddValue('tag', ATag);

  Assert(Q.Result.RecordCount = 1, 'Cannot load SY_STORAGE data for ' + ATag);

  rec_version := Q.Result.FieldByName('rec_version').AsInteger;
  TBlobField(Q.Result.FieldByName('storage_data')).SaveToStream(MS.RealStream);

  MS.Position := 0;
  InflateStream(MS.RealStream, AStream);
  AStream.Position := 0;

  Q := TevQuery.Create(Format('{SystemLastTableRecVerChange<%s,%d>}', ['SY_STORAGE', rec_version]));

  Result := Q.Result.FieldByName('change_date').AsDateTime;
end;

function GetUniqueFileName(const APath: string; const APrefix: string; const AExt: string): string;
var
  i: Integer;
begin
  for i := 1 to 100000 do
  begin
    Result := APath+APrefix + IntToStr(i) + AExt;
    if not FileExists(Result) then
      break;
  end;
end;


procedure GetReportRes(const Report_NBR: Integer; const ALevel: String; const ACheckReportVersion: Boolean;
                       out ReportType: TReportType; out ReportName: String; const AData: IEvDualStream;
                       out MediaType: string; out ReportClassName: String; out AncestorClassName: String);

var
  MS1: IEvDualStream;
  lPrif, lPrif2, lPrif3: String;
  s: String;
  cDBType: Char;
  iClientID: Integer;

  function CacheReportRes: Boolean;
  var
    lKey: String;

    function AddReport(const UpdateFlag: Boolean): Boolean;
    var
      Q: IevQuery;
      rec_version: Integer;
    begin
      Result := False;

      if cDBType = CH_DATABASE_CLIENT then
        ctx_DataAccess.OpenClient(iClientID);

      Q := TevQuery.Create('select report_file, report_description, report_type, media_type, class_name, ancestor_class_name, rec_version from ' +
           lPrif +'_report_writer_reports t where {AsOfDate<t>} and ' +
           lPrif + '_report_writer_reports_nbr = :nbr');

      if ctx_DataAccess.AsOfDate = 0 then
        Q.Params.AddValue('StatusDate', 'now')
      else
        Q.Params.AddValue('StatusDate', ctx_DataAccess.AsOfDate);

      Q.Params.AddValue('Nbr', Report_NBR);
      Q.Execute;

      if Q.Result.RecordCount = 1 then
      begin
       if UpdateFlag then
         CachedReports.Edit
        else
        begin
          CachedReports.Append;
          CachedReports.Fields.FieldByName('Level').AsString := lKey;
          CachedReports.Fields.FieldByName('Nbr').AsInteger := Report_NBR;
        end;

        rec_version := Q.Result.FieldByName('Rec_Version').AsInteger;

        CachedReports.Fields.FieldByName('Name').AsString := Q.Result.FieldByName('report_description').AsString;
        CachedReports.Fields.FieldByName('Type').AsString := Q.Result.FieldByName('report_type').AsString;
        CachedReports.Fields.FieldByName('Data').Value := Q.Result.FieldByName('report_file').Value;
        CachedReports.Fields.FieldByName('Media_type').Value := Q.Result.FieldByName('media_type').Value;
        CachedReports.Fields.FieldByName('class_name').Value := Q.Result.FieldByName('class_name').Value;
        CachedReports.Fields.FieldByName('ancestor_class_name').Value := Q.Result.FieldByName('ancestor_class_name').Value;
        CachedReports.Fields.FieldByName('Rec_Version').Value := rec_version;
        CachedReports.Post;

        Q := TevQuery.Create(Format('{GenericLastTableRecVerChange<%s,%s,%d>}',
          [lPrif3, AnsiUpperCase(lPrif +'_report_writer_reports'), rec_version]));
        Q.Execute;

        CachedReports.Edit;
        CachedReports.Fields.FieldByName('Verion').AsDateTime := Q.Result.FieldByName('change_date').AsDateTime;
        CachedReports.Post;

        Result := True;
      end;
    end;

    function CheckReportVersion: Boolean;
    var
      Q: IevQuery;
    begin
      if cDBType = CH_DATABASE_CLIENT then
        ctx_DataAccess.OpenClient(iClientID);

      Q := TevQuery.Create(Format('{GenericLastTableRecVerChange<%s,%s,%d>}',
        [lPrif3, AnsiUpperCase(lPrif +'_report_writer_reports'), CachedReports.FieldByName('Rec_Version').AsInteger]));

      if CachedReports.Fields.FieldByName('Verion').AsDateTime <> Q.Result.FieldByName('change_date').AsDateTime then
      begin
        Result := AddReport(True);
        if not Result then
          CachedReports.Delete;
      end
      else
        Result := True;
    end;

  begin
    lKey := cDBType;
    if cDBType = CH_DATABASE_SYSTEM then
    begin
      lPrif := 'SY';
      lPrif2 := 'S';
      lPrif3 := DB_System;
    end
    else if cDBType = CH_DATABASE_SERVICE_BUREAU then
    begin
      lKey := lKey + '_' + Context.UserAccount.Domain;
      lPrif := 'SB';
      lPrif2 := 'B';
      lPrif3 := DB_S_Bureau;
    end
    else if cDBType = CH_DATABASE_CLIENT then
    begin
      lKey := lKey + '_' + Context.UserAccount.Domain + '_' + IntToStr(iClientID);
      lPrif := 'CL';
      lPrif2 := 'C' + IntToStr(iClientID) + ' ';
      lPrif3 := DB_Cl;
    end
    else
      Assert(False, 'Report level is incorrect');

    if CachedReports.Locate('Level;Nbr', VarArrayOf([lKey, Report_NBR]), []) then
      Result := not ACheckReportVersion or CheckReportVersion
    else
      Result := AddReport(False);
  end;

begin
  cDBType := ALevel[1];

  if cDBType = CH_DATABASE_CLIENT then
    iClientID := StrToInt(Copy(ALevel, 2, Length(ALevel) - 1));


  MS1 := TEvDualStreamHolder.Create;
  GlobalNameSpace.BeginWrite;
  try
    AData.Size := 0;
    ReportType := rtReport;

    if CacheReportRes then
    begin
      s := CachedReports.Fields.FieldByName('Data').AsString;
      if Length(s) > 0 then
        MS1.WriteBuffer(s[1], Length(s));

      ReportName := CachedReports.Fields.FieldByName('Name').AsString + ' (' + lPrif2 + IntToStr(Report_NBR) + ')';
      MediaType := CachedReports.Fields.FieldByName('Media_Type').AsString;
      ReportClassName := CachedReports.Fields.FieldByName('class_name').AsString;
      AncestorClassName := CachedReports.Fields.FieldByName('ancestor_class_name').AsString;

      case CachedReports.Fields.FieldByName('Type').AsString[1] of
        SYSTEM_REPORT_TYPE_CHECK:      ReportType := rtCheck;

        SYSTEM_REPORT_TYPE_TAX_RETURN_XML,
        SYSTEM_REPORT_TYPE_ASCII:      ReportType := rtASCIIFile;

        SYSTEM_REPORT_TYPE_TAXCOUPON,
        SYSTEM_REPORT_TYPE_TAX_RETURN_MULTICLIENT,
        SYSTEM_REPORT_TYPE_TAX_RETURN: ReportType := rtTaxReturn;

        SYSTEM_REPORT_TYPE_MULTICLIENT:ReportType := rtMultiClient;

        SYSTEM_REPORT_TYPE_HR:         ReportType := rtHR;
      end;
    end
    else
      raise EevException.Create('Report not found');

  finally
    GlobalNameSpace.EndWrite;
  end;

  if MS1.Size > 0 then
  begin
    MS1.Position := 0;
    InflateStream(MS1.RealStream, AData.RealStream);
  end;
  AData.Position := 0;
end;


function EvRepParamsToParamList(const EvRepParams: TrwReportParams): TrwReportParamList;
var
  i: Integer;
begin
  SetLength(Result, EvRepParams.Count);

  for i := 0 to EvRepParams.Count - 1 do
  begin
    Result[i].Name := EvRepParams[i].Name;
    if not VarIsNull(EvRepParams[i].Value) then
      case VarType(EvRepParams[i].Value) of
        varInteger:  Result[i].ParamType := rwvInteger;
        varDouble:   Result[i].ParamType := rwvFloat;
        varCurrency: Result[i].ParamType := rwvCurrency;
        varDate:     Result[i].ParamType := rwvDate;
        varString:   Result[i].ParamType := rwvString;
        varBoolean:  Result[i].ParamType := rwvBoolean;
      else
        if VarIsArray(EvRepParams[i].Value) then
          Result[i].ParamType :=rwvArray
        else
          Result[i].ParamType :=rwvUnknown;
      end;

    Result[i].RunTimeParam := not EvRepParams[i].Store;
    Result[i].Value := EvRepParams[i].Value;
  end;
end;


procedure ParamListToEvRepParams(const ReportParams: TrwReportParamList; const EvRepParams: TrwReportParams);
var
  i: Integer;
begin
  for i := Low(ReportParams) to High(ReportParams) do
    with ReportParams[i] do
      EvRepParams.Add(Name, Value, not RunTimeParam);
end;


function DumpParams(const AParams: TrwReportParams; AFileNamePriff: String = 'Report'): String;
begin
  Result := rwParamsFolder;
  if Length(AFileNamePriff) <> 0 then
    Result := Result + NormalizeFileName(AFileNamePriff) + ' ';
  Result := Result + GetUniqueID + '.rwp';
  AParams.SaveToFile(Result);
end;


function  NormalizeFileName(const AFileName: String): String;
var
  i: Integer;
  c: Char;
begin
  Result := AFileName;

  for i := 1 to Length(Result) do
  begin
    c := AnsiUpperCase(Result[i])[1];
    if not((c >= '0') and (c <= '9') or
           (c >= 'A') and (c <= 'Z') or
           (Pos(c, ' .,;~_[]{}!@#$%^()-+=`') > 0)) then
      Result[i] := '_';
  end;
end;

procedure InitializeRWParamFolder;

  procedure CleanTempParamFiles;
  var
    Srec: TSearchRec;
    t: Integer;
  begin
    if SysUtils.FindFirst(rwParamsFolder + '*.rwp', faAnyFile, Srec) = 0 then
    begin
      t := DateTimeToFileDate(Now - 8);  //8 day threshold
      repeat
        if Srec.Time <= t then
          SysUtils.DeleteFile(rwParamsFolder + Srec.Name);
      until SysUtils.FindNext(Srec) <> 0;
      SysUtils.FindClose(Srec);
    end;
  end;

begin
  rwParamsFolder := AppTempFolder + 'rwParams\';
  ForceDirectories(rwParamsFolder);
  CleanTempParamFiles;
end;


initialization
  rwCriticalSection := TCriticalSection.Create;
  InitializeRWParamFolder;

finalization
  FreeAndNil(FCachedReports);
  FreeAndNil(rwCriticalSection);

end.
