// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvBasicUtils;

interface

{Don't add any Evolution modules in uses in this unit!!! If you need to do this, put your functions in EvUtils.pas}
uses
  {$IFDEF MSWINDOWS}
    Windows,
  {$ENDIF}
  {$IFDEF LINUX}
    Types,
    libc,
  {$ENDIF}
  SysUtils, Classes, RTLConsts, SSecurityInterface, EvStreamUtils, ISBasicUtils,
  UnxCrypt;

//proxy definitions for backward compatibility
function Fetch(var AInput: string; const ADelim: Char = ' '): string;
function GetMemStatus: TMemoryStatus;
function RandomInteger(iLow, iHigh: Integer): Integer;
function ValidateFileName(const s: string): string;
// Date usefull functions
function TimeStampToMSecs64(const TimeStamp: TTimeStamp): Int64;
function MSecs64ToTimeStamp(const i: Int64): TTimeStamp;
function GetQuarterNumber(MyDate: TDateTime): Integer;
function GetWeekNumber(MyDate: TDateTime): Integer;
function GetWeekNumberYear(WeekDate: TDatetime): integer;
function GetYear(MyDate: TDateTime): Integer;
function GetMonth(MyDate: TDateTime): Integer;
function GetDay(MyDate: TDateTime): Integer;
function GetBeginYear(Date: TDateTime): TDateTime;
function GetBeginQuarter(Date: TDateTime): TDateTime;
function GetBeginMonth(Date: TDateTime): TDateTime;
function GetEndYear(Date: TDateTime): TDateTime;
function GetEndQuarter(Date: TDateTime): TDateTime;
function GetEndMonth(Date: TDateTime): TDateTime;
function GetYearAgoDate(const TheDate: TDateTime): TDateTime;
procedure GetPreviousQuarter(Date: TDatetime; var Quarter: Integer;
  var Year: Word; var BeginQuarter: TDatetime; var EndQuarter: TDatetime);

function  EncodeUserAtDomain(const AUser, ADomain: String): String;
procedure DecodeUserAtDomain(ADomainUser: String; out AUser, ADomain: String);
function  HashPassword(const APassword: String): String;
function  HashText(const AText: String): String;

function  IsGuestUser(const AUser: String): Boolean;
function  IsADRUser(const AUser: String): Boolean;
function  IsDBAUser(const AUser: String): Boolean;

function BoolToYN(const b: Boolean): string;
procedure CleanDir(Dir: string);

function MakeString(const Args: array of const; const Spacer: Char = ' '): string;

type  TIntLstQSortCompareFunct = function (const Item1, Item2: IInterface): Integer;

procedure SortInterfaceList(const List: IInterfaceList; const CompareFunct: TIntLstQSortCompareFunct);

implementation

uses evConsts;

function Fetch(var AInput: string; const ADelim: Char = ' '): string;
begin
  Result := ISBasicUtils.FetchToken(AInput, ADelim);
end;

function RandomInteger(iLow, iHigh: Integer): Integer;
begin
  Result := ISBasicUtils.RandomInteger(iLow, iHigh);
end;

function ValidateFileName(const s: string): string;
begin
  Result := ISBasicUtils.ValidateFileName(s);
end;

function GetMemStatus: TMemoryStatus;
begin
  Result := ISBasicUtils.GetMemStatus;
end;

function GetYearAgoDate(const TheDate: TDateTime): TDateTime;
var
  d1, m1, y1: Word;
begin
  DecodeDate(TheDate, y1, m1, d1);
  Result := EncodeDate(y1 - 1, m1, d1);
end;

function TimeStampToMSecs64(const TimeStamp: TTimeStamp): Int64;
begin
  Result := TimeStamp.Date;
  Result := Result* MSecsPerDay;
  Result := Result+ TimeStamp.Time;
end;

function MSecs64ToTimeStamp(const i: Int64): TTimeStamp;
begin
  Result.Date := i div MSecsPerDay;
  Result.Time := i mod MSecsPerDay;
end;

function GetQuarterNumber(MyDate: TDateTime): Integer;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  Result := ((Month - 1) div 3) + 1;
end;

function GetWeekNumber(MyDate: TDateTime): Integer;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  Result := ((Day - 1) div 7) + 1;
end;


function GetWeekNumberYear(WeekDate: TDatetime): integer;
var
  day: word;
  month: word;
  year: word;
  FirstOfYear: TDateTime;
begin
  DecodeDate(WeekDate, year, month, day);
  FirstOfYear := EncodeDate(year, 1, 1);
  Result := Trunc(WeekDate - FirstOfYear) div 7 + 1;
end;


function GetYear(MyDate: TDateTime): Integer;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  Result := Year;
end;

function GetMonth(MyDate: TDateTime): Integer;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  Result := Month;
end;

function GetDay(MyDate: TDateTime): Integer;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  Result := Day;
end;

function GetBeginYear(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Result := EncodeDate(Year, 1, 1);
end;

function GetBeginQuarter(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Month := ((Month - 1) div 3) * 3 + 1; // Get to beginning of quarter
  Result := EncodeDate(Year, Month, 1);
end;

function GetBeginMonth(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Result := EncodeDate(Year, Month, 1);
end;

function GetEndYear(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Result := EncodeDate(Year, 12, 31);
end;

function GetEndQuarter(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Month := ((Month - 1) div 3) * 3 + 1; // Get to beginning of quarter
  Month := Month + 3;
  if Month > 12 then
  begin
    Month := 1;
    Inc(Year);
  end;
  Result := EncodeDate(Year, Month, 1) - 1;
end;

function GetEndMonth(Date: TDateTime): TDateTime;
var
  Year, Month, Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Inc(Month);
  if Month > 12 then
  begin
    Month := 1;
    Inc(Year);
  end;
  Result := EncodeDate(Year, Month, 1) - 1;
end;

procedure GetPreviousQuarter(Date: TDatetime; var Quarter: Integer;
  var Year: Word; var BeginQuarter: TDatetime; var EndQuarter: TDatetime);
var
  Month,
    Day: word;
begin
  DecodeDate(Date, Year, Month, Day);
  Quarter := GetQuarterNumber(Date);
  if (Quarter = 1) then
  begin
    Quarter := 4;
    Year := Year - 1;
  end
  else
  begin
    Dec(Quarter);
  end;
  BeginQuarter := GetBeginQuarter(GetBeginQuarter(Date) - 1);
  EndQuarter := GetEndQuarter(GetBeginQuarter(Date) - 1);
end;

function BoolToYN(const b: Boolean): string;
begin
  if b then
    Result := 'Y'
  else
    Result := 'N';
end;

procedure CleanDir(Dir: string);
var
  r: TSearchRec;
begin
  if FindFirst(Dir+ '*.*', faAnyFile, r) = 0 then
  try
    repeat
      if (faDirectory and r.Attr) = 0 then
        DeleteFile(Dir+ r.Name)
      else if (r.Name <> '.') and (r.Name <> '..') then
      begin
        CleanDir(Dir+ r.Name + PathDelim);
        RemoveDir(Dir+ r.Name);
      end;
    until FindNext(r) <> 0;
  finally
    FindClose(r);
  end;
end;

function MakeString(const Args: array of const; const Spacer: Char = ' '): string;
var
  i: Integer;
begin
  Result := '';
  for I := 0 to High(Args) do
    with Args[I] do
      case VType of
        vtInt64:      Result := Result+ Spacer+ IntToStr(VInt64^);
        vtInteger:    Result := Result+ Spacer+ IntToStr(VInteger);
        vtString:     Result := Result+ Spacer+ VString^;
        vtPChar:      Result := Result+ Spacer+ VPChar;
        vtChar:       Result := Result+ Spacer+ VChar;
        vtAnsiString: Result := Result+ Spacer+ string(VAnsiString);
        vtWideString: Result := Result+ Spacer+ WideString(VWideString);
        vtVariant:    Result := Result+ Spacer+ string(VVariant^);
        vtExtended:   Result := Result+ Spacer+ FloatToStr(VExtended^);
        vtBoolean:    Result := Result+ Spacer+ BoolToYN(VBoolean);
        vtCurrency:   Result := Result+ Spacer+ CurrToStr(VCurrency^);
      else
        Assert(False, 'Unexpected type: '+ IntToStr(VType));
      end;
  Delete(Result, 1, 1);
end;

// Quick sort for Interfaced list

procedure SortInterfaceList(const List: IInterfaceList; const CompareFunct: TIntLstQSortCompareFunct);

  procedure doQSort(const lo0, hi0: Integer);
  var
    lo, hi, mid : Integer;
    t: IInterface;
  begin
    lo := lo0;
    hi := hi0;
    if (lo < hi) then begin
      mid := (lo + hi) div 2;
      while (lo < hi) do begin
        while ((lo<hi) and (CompareFunct(List[lo], List[mid]) < 0)) do inc(lo);
        while ((lo<hi) and (CompareFunct(List[hi], List[mid]) > 0)) do dec(hi);
        if CompareFunct(List[hi], List[lo]) = 0 then
          Break
        else
        if (lo < hi) then begin
          t := List[lo];
          List[lo] := List[hi];
          List[hi] := t;
        end;
      end;
      if (hi < lo) then begin
        t := List[hi];
        List[hi] := List[lo];
        List[lo] := t;
      end;
      doQSort(lo0, lo);
      if (lo = lo0) then mid := lo+1 else mid := lo;
      doQSort(mid, hi0);
    end;
  end;

begin
  List.Lock;
  try
    doQSort(0, List.Count - 1);
  finally
    List.Unlock
  end;
end;


function EncodeUserAtDomain(const AUser, ADomain: String): String;
begin
  if ADomain = '' then
    Result := AUser + '@' + sDefaultDomain
  else
    Result := AUser + '@' + ADomain;
end;


procedure DecodeUserAtDomain(ADomainUser: String; out AUser, ADomain: String);
begin
  AUser := GetNextStrValue(ADomainUser, '@');
  if ADomainUser = '' then
    ADomain := sDefaultDomain
  else
    ADomain := ADomainUser;
end;

function  IsGuestUser(const AUser: String): Boolean;
begin
  Result := AnsiSameText(AUser, sGuestUserName);
end;

function  IsADRUser(const AUser: String): Boolean;
begin
  Result := AnsiSameText(AUser, sADRUserName);
end;

function  IsDBAUser(const AUser: String): Boolean;
begin
  Result := AnsiSameText(AUser, sDBAUserName);
end;

function HashPassword(const APassword: String): String;

  function OldCreateInterbasePassword(const Pwd: string): string;
  begin
    Result := Copy(UnixCrypt(Pwd, '9z'), 3, 99);
    Result := Copy(UnixCrypt(Result, '9z'), 3, 99);
  end;

begin
  Result := Copy(OldCreateInterbasePassword(Copy(APassword, 1, 8))
    + OldCreateInterbasePassword(Copy(APassword, 9, 8)+ '64_dsFDg')
    + OldCreateInterbasePassword(Copy(APassword, 17, 8)+ '5!@efZdx'), 1, 32);
end;

function  HashText(const AText: String): String;
begin
  Result := UnixCrypt(AText, '9z');
end;


end.
