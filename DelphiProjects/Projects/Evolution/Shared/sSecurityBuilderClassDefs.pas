unit sSecurityBuilderClassDefs;

interface

uses sSecurityClassDefs, SSecurityInterface, Graphics;

type
  TBuilderSecurityContext = class(TSecurityContext)
  public
    function LoadIcon(const Resource: string): TIcon;
    constructor CreateFromRecord(const AtomContextRecord: TAtomContextRecord);
  end;

  TBuilderElementStates = class(TElementStates)
  protected
    procedure AddState(const ElementState: TElementState);
  public
    constructor CreateFromRecordArray(const ar: TElementStatesArray);
    procedure ExpandStatesFromRecordArray(const ar: TElementStatesArray);
  end;

  TBuilderElementToAtomLink = class(TElementToAtomLink)
  public
    constructor CreateFromRecordArray(const Atom: TSecurityAtom; const ar: TAtomElementRelationsArray);
  end;

  TBuilderSecurityElement = class(TSecurityElement)
  public
    constructor CreateFromInterface(const Int: ISecurityElement; const Collection: TSecurityElementCollection);
    procedure ExpandStatesFromInterface(const Int: ISecurityElement);
    procedure SetRelationsWithAtom(const Atom: TSecurityAtom; const ar: TAtomElementRelationsArray);
  end;

  TBuilderSecurityAtom = class(TSecurityAtom)
  public
    constructor CreateFromInterface(const Int: ISecurityAtom; const Collection: TSecurityAtomCollection);
  end;

  TBuilderInterfaceHolder = class(TInterfacedObject, ISchemeBuilder)
  public
    Collection: TSecurityAtomCollection;
    procedure AddAtom(const Atom: ISecurityAtom);
    procedure AddElement(const Element: ISecurityElement);
  end;

  TBuilderSecurityAtomCollection = class(TSecurityAtomCollection, ISchemeBuilder)
  private
    FHolder: ISchemeBuilder;
  public
    constructor Create;
    destructor Destroy; override;
    property Holder: ISchemeBuilder read FHolder implements ISchemeBuilder;
  end;

implementation

uses Classes, Windows;

{$R sSecurityResources.res}

{ TBuilderSecurityAtomCollection }

constructor TBuilderSecurityAtomCollection.Create;
var
  c: TBuilderInterfaceHolder;
begin
  inherited Create;
  c := TBuilderInterfaceHolder.Create;
  c.Collection := Self;
  FHolder := c;
end;

destructor TBuilderSecurityAtomCollection.Destroy;
begin
  inherited;
  FHolder := nil;
end;

{ TBuilderSecurityAtom }

constructor TBuilderSecurityAtom.CreateFromInterface(
  const Int: ISecurityAtom; const Collection: TSecurityAtomCollection);
var
  i: Integer;
  ar: TAtomContextRecordArray;
begin
  FContexts := TList.Create;
  FElements := TList.Create;
  FAtomType := Int.SGetType;
  FTag := Int.SGetTag;
  FCaption := Int.SGetCaption;
  FCollection := Collection;
  ar := Int.SGetPossibleSecurityContexts;
  for i := 0 to High(ar) do
    FContexts.Add(TBuilderSecurityContext.CreateFromRecord(ar[i]));
end;

{ TBuilderSecurityContext }

constructor TBuilderSecurityContext.CreateFromRecord(
  const AtomContextRecord: TAtomContextRecord);
begin
  FCaption := AtomContextRecord.Caption;
  FTag := AtomContextRecord.Tag;
  FPriority := AtomContextRecord.Priority;
  if AtomContextRecord.IconName <> '' then
    FIcon := LoadIcon(AtomContextRecord.IconName)
  else
  {if AtomContextRecord.Icon <> nil then
  begin
    FIcon := TIcon.Create;
    FIcon.Assign(AtomContextRecord.Icon);
  end
  else}
    FIcon := nil;
end;

function TBuilderSecurityContext.LoadIcon(const Resource: string): TIcon;
begin
  if Resource = '' then
    Result := nil
  else
  begin
    Result := TIcon.Create;
    Result.Handle := Windows.LoadIcon(hInstance, PChar(Resource));
    {if Result.Handle = 0 then
      Result := nil;}
    Assert(Result.Handle <> 0, Resource);
  end;
end;

{ TBuilderSecurityElement }

constructor TBuilderSecurityElement.CreateFromInterface(
  const Int: ISecurityElement; const Collection: TSecurityElementCollection);
begin
  FCollection := Collection;
  FTag := Int.SGetTag;
  FElementType := Int.SGetType;
  FStates := TBuilderElementStates.CreateFromRecordArray(Int.SGetStates);
  FAtomLinks := TElementToAtomLinks.Create;
  FInStateChangeProcess := False;
end;

procedure TBuilderSecurityElement.ExpandStatesFromInterface(
  const Int: ISecurityElement);
begin
  TBuilderElementStates(FStates).ExpandStatesFromRecordArray(Int.SGetStates); 
end;

procedure TBuilderSecurityElement.SetRelationsWithAtom(
  const Atom: TSecurityAtom; const ar: TAtomElementRelationsArray);
begin
  Atom.LinkElement(Self);
  FAtomLinks.Add(TBuilderElementToAtomLink.CreateFromRecordArray(Atom, ar));
end;

{ TBuilderElementStates }

procedure TBuilderElementStates.AddState(
  const ElementState: TElementState);
begin
  SetLength(FStates, Length(FStates)+ 1);
  FStates[High(FStates)] := ElementState;
end;

constructor TBuilderElementStates.CreateFromRecordArray(
  const ar: TElementStatesArray);
begin
  FStates := ar;
end;

procedure TBuilderElementStates.ExpandStatesFromRecordArray(
  const ar: TElementStatesArray);
var
  i: Integer;
begin
  for i := 0 to High(ar) do
    if IndexOfState(ar[i].Tag) = -1 then
      AddState(ar[i]);
end;

{ TBuilderElementToAtomLink }

constructor TBuilderElementToAtomLink.CreateFromRecordArray(
  const Atom: TSecurityAtom; const ar: TAtomElementRelationsArray);
begin
  FAtom := Atom;
  FRelations := ar;
end;

{ TBuilderInterfaceHolder }

procedure TBuilderInterfaceHolder.AddAtom(const Atom: ISecurityAtom);
begin
  if not Assigned(Collection.FindAtom(Atom.SGetTag, Atom.SGetType)) then
    Collection.AddUniqueAtom(TBuilderSecurityAtom.CreateFromInterface(Atom, Collection));
end;

procedure TBuilderInterfaceHolder.AddElement(
  const Element: ISecurityElement);
var
  e: TBuilderSecurityElement;
begin
  if Assigned(Element.Atom) then
  begin
    AddAtom(Element.Atom);
    e := TBuilderSecurityElement(Collection.SecurityElementCollection.FindElement(Element.SGetTag, Element.SGetType));
    if not Assigned(e) then
    begin
      e := TBuilderSecurityElement.CreateFromInterface(Element, Collection.SecurityElementCollection);
      Collection.SecurityElementCollection.AddUniqueElement(e);
    end
    else
      e.ExpandStatesFromInterface(Element);
    e.SetRelationsWithAtom(Collection.FindAtom(Element.Atom.SGetTag, Element.Atom.SGetType), Element.SGetAtomElementRelationsArray);
  end;
end;

end.
