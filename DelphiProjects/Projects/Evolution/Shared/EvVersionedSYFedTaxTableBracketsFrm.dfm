inherited EvVersionedSYFedTaxTableBrackets: TEvVersionedSYFedTaxTableBrackets
  Left = 755
  Top = 514
  ClientHeight = 564
  ClientWidth = 820
  PixelsPerInch = 96
  TextHeight = 13
  inherited fpEffectivePeriod: TisUIFashionPanel
    Width = 820
    Height = 384
    inherited grFieldData: TevDBGrid
      Width = 772
      Height = 257
    end
    inherited pnlEdit: TevPanel
      Top = 301
      Width = 772
      inherited pnlButtons: TevPanel
        Left = 463
      end
    end
  end
  object pnlBottom: TevPanel [2]
    Left = 0
    Top = 384
    Width = 820
    Height = 180
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object isUIFashionPanel1: TisUIFashionPanel
      Left = 0
      Top = 0
      Width = 180
      Height = 180
      Align = alLeft
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'isUIFashionPanel1'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Tax Table Brackets'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      Margins.Left = 8
      Margins.Right = 8
      Margins.Bottom = 8
      object lblGreaterThanValue: TevLabel
        Left = 20
        Top = 35
        Width = 93
        Height = 13
        Caption = 'Greater Than Value'
      end
      object lblLessThanValue: TevLabel
        Left = 20
        Top = 74
        Width = 80
        Height = 13
        Caption = 'Less Than Value'
      end
      object lblPercentage: TevLabel
        Left = 20
        Top = 114
        Width = 55
        Height = 13
        Caption = 'Percentage'
      end
      object edLessThanValue: TevDBEdit
        Left = 20
        Top = 89
        Width = 130
        Height = 21
        DataField = 'LESS_THAN_VALUE'
        DataSource = dsFieldData
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edGreaterThanValue: TevDBEdit
        Left = 20
        Top = 50
        Width = 130
        Height = 21
        DataField = 'GREATER_THAN_VALUE'
        DataSource = dsFieldData
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
      object edPercentage: TevDBEdit
        Left = 20
        Top = 129
        Width = 130
        Height = 21
        DataField = 'PERCENTAGE'
        DataSource = dsFieldData
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = UpdateFieldOnChange
        Glowing = False
      end
    end
  end
end
