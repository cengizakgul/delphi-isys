inherited SBUserChoiceQuery: TSBUserChoiceQuery
  Left = 539
  Width = 520
  Height = 446
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 366
    Width = 504
    DesignSize = (
      504
      41)
    inherited btnYes: TevBitBtn
      Left = 256
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 382
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 126
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 504
    Height = 331
    Selected.Strings = (
      'USER_ID'#9'17'#9'User id'#9'F'
      'FIRST_NAME'#9'17'#9'First name'#9'F'
      'LAST_NAME'#9'20'#9'Last name'#9'F'
      'StereoType'#9'10'#9'StereoType'#9'F'
      'ACTIVE_USER'#9'10'#9'Active'#9'F')
    IniAttributes.SectionName = 'TSBUserChoiceQuery\dgChoiceList'
  end
  inherited pnlEffectiveDate: TevPanel
    Top = 331
    Width = 504
  end
  inherited dsChoiceList: TevDataSource
    Left = 192
    Top = 40
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 256
    Top = 152
  end
end
