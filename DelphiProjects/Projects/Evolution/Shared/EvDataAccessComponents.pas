// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvDataAccessComponents;

interface

uses
  ISDataAccessComponents, EvStreamUtils, DB, Classes, SysUtils, Variants, DateUtils,
  SUniversalDataReader, IsBaseClasses, isTypes, EvConsts;

type
  TEvReconcileEvent = procedure (const Field: TField; const OldValue, NewValue: Integer) of object;

  TEvBasicClientDataSet = class(TISBasicClientDataSet)
  private
    FOnRecordReconcileEvent: TEvReconcileEvent;
    FOnDestroy: TNotifyEvent;
    FReconcileMap: IisValueMap;
    FReconcileFields: array of TField;
    FReconcileFieldsHigh: Integer;
    FDataAsOfDate: TisDate;
    FOpenAsOfDate: TisDate;
    FOpenedAsOfToday: Boolean;

    procedure ReconcileCallBack(const Version, Index, Total: Longint);
    procedure SetDataAsOfDate(const AValue: TisDate);
  protected
    function  GetAsOfDate: TisDate; virtual;
    procedure SetAsOfDate(const AValue: TisDate); virtual;
    procedure DoAfterClose; override;
    procedure CustomReconcile(const MapList: IisValueMap); virtual;
    function  OpenedAsOfToday: Boolean;
  public
    procedure BeforeDestruction; override;
    procedure Reconcile(const MapList: IisValueMap);
    procedure ClearAsOfDateOverride;
    function  UsingDefaultAsOfDate: Boolean;

    property  AsOfDate: TisDate read GetAsOfDate write SetAsOfDate;
    property  OnRecordReconcileEvent: TEvReconcileEvent read FOnRecordReconcileEvent write FOnRecordReconcileEvent;
    property  OnDestroy: TNotifyEvent read FOnDestroy write FOnDestroy;
  end;


  TEvBasicParams = class
  private
    FStream: IEvDualStream;
    function GetPosition: LongInt;
    procedure SetPosition(const Value: LongInt);
  protected
    procedure StripItem(ItemName: string);
  public
    constructor Create;
    procedure AddItem(const ItemName: string; const FieldType: TFieldType; const Buffer; const Length: Integer; const CheckForDuplicate: Boolean);
    function  NextItem(out ItemName: string): Variant;
    function  GetValueByName(ItemName: string): Variant;
    procedure SetValueByName(const ItemName: string; const v: Variant; const CheckForDuplicate: Boolean = False);
    procedure AddParam(const ItemName: string; const v: Variant);
    function  CheckIfExists(ItemName: string): Boolean;
    procedure Clear;
    property Position: LongInt read GetPosition write SetPosition;
    function EOS: Boolean;

    property  Stream: IEvDualStream read FStream;
  end;

  TExecDSWrapper = class;

  IExecDSWrapper = interface
  ['{DBC73A8A-B695-4765-8C21-1C787D59DB1F}']
    function  QueryText: string;
    function  AsMemStream: IisStream;
    function  GetParams: IisListOfValues;
    function  GetMacros: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    procedure SetMacros(const AValue: IisListOfValues);
    function  GetBlobsLazyLoad: Boolean;
    procedure SetBlobsLazyLoad(const AValue: Boolean);
    property  Params: IisListOfValues read GetParams write SetParams;
    property  Macros: IisListOfValues read GetMacros write SetMacros;
    property  BlobsLazyLoad: Boolean read GetBlobsLazyLoad write SetBlobsLazyLoad;
  end;


  TInterPackageData = class
  protected
    FMemStream: IisStream;
  public
    constructor Create; overload;
    constructor Create(const MemStream: IisStream); overload;
    property  MemStream: IisStream read FMemStream;
    function  EndOfParams: Boolean;
    procedure GotoFirst;
    procedure Clear;
  end;

  TGetDataParams = class(TInterPackageData)
  private
    FWriteCounter: Integer;
  public
    constructor Create; overload;
    property  WriteCounter: Integer read FWriteCounter;
    procedure Write(const ProviderName: string; const OverrideClNbr: Integer; ExecDsWrapper: IExecDSWrapper);
    procedure Read(out ProviderName: string; out OverrideClNbr: Integer; out ExecDsWrapper: IExecDSWrapper);
  end;

  TGetDataResults = class(TInterPackageData)
  private
    FQueryResults: TIEvQueryResultArray;
  public
    procedure Write(const QueryResult: IEvQueryResult);
    procedure Read(const cd: TEvBasicClientDataSet; const SkipPostProcess: Boolean; const ContentAsOfDate: TisDate = 0);
    procedure FinalizeWrite;
    constructor Create; overload;
    function QueryResult(const Index: Integer): IEvQueryResult;
    function QueryResultCount: Integer;
  end;

  TExecDSWrapper = class (TInterfacedObject, IExecDSWrapper)
  private
    FQueryText: string;
    FParams: IisListOfValues;
    FMacros: IisListOfValues;
    FBlobsLazyLoad: Boolean;
  public
    constructor Create(const sQueryText: string; const AsOfDate: TisDate = 0; ABlobsLazyLoad: Boolean = False); reintroduce; overload;
    constructor Create; reintroduce; overload;
    constructor Create(const Stream: IEvDualStream); reintroduce; overload;
    procedure SetParam(const sName: string; const Value: Integer; const CheckForDuplicate: Boolean = False); overload;
    procedure SetParam(const sName: string; const Value: Double; const CheckForDuplicate: Boolean = False; const FieldType: TFieldType = ftFloat); overload;
    procedure SetParam(const sName: string; const Value: string; const CheckForDuplicate: Boolean = False); overload;
    procedure SetParam(const sName: string; const Value: Variant; const CheckForDuplicate: Boolean = False); overload;
    procedure SetParamNull(const sName: string; CheckForDuplicate: Boolean = False);
    procedure SetMacro(const sName: string; const Value: string; const CheckForDuplicate: Boolean = False);
    function  AsMemStream: IisStream;
    function  GetParams: IisListOfValues;
    function  GetMacros: IisListOfValues;
    procedure SetParams(const AValue: IisListOfValues);
    procedure SetMacros(const AValue: IisListOfValues);
    function  GetBlobsLazyLoad: Boolean;
    procedure SetBlobsLazyLoad(const AValue: Boolean);
    function  QueryText: string;
    function  AsVariant: IExecDSWrapper;
    property  Params: IisListOfValues read GetParams write SetParams;
    property  Macros: IisListOfValues read GetMacros write SetMacros;
    property  BlobsLazyLoad: Boolean read GetBlobsLazyLoad write SetBlobsLazyLoad;
  end;

implementation

uses EvClientDataSet, EvUtils;


{ TEvBasicParams }

procedure TEvBasicParams.AddItem(const ItemName: string;
  const FieldType: TFieldType; const Buffer; const Length: Integer;
  const CheckForDuplicate: Boolean);
begin
  with Stream do
  begin
    if CheckForDuplicate then
      StripItem(ItemName);
    WriteString(ItemName);
    WriteBuffer(FieldType, SizeOf(FieldType));
    WriteInteger(Length);
    if Length > 0 then
      WriteBuffer(Buffer, Length);
  end;
end;

procedure TEvBasicParams.AddParam(const ItemName: string; const v: Variant);
begin
  SetValueByName(ItemName, v);
end;

function TEvBasicParams.CheckIfExists(ItemName: string): Boolean;
var
  p, i: Integer;
begin
  with Stream do
  begin
    Result := False;
    p := Position;
    ItemName := UpperCase(ItemName);
    Position := 0;
    while not Eos do
    begin
      if UpperCase(ReadString) = ItemName then
      begin
        Position := p;
        Result := True;
        Break
      end
      else
      begin
        Seek(SizeOf(TFieldType), soCurrent);
        i := ReadInteger;
        Seek(i, soCurrent);
      end;
    end;
  end;
end;

procedure TEvBasicParams.Clear;
begin
  FStream.Clear;
end;

constructor TEvBasicParams.Create;
begin
  FStream := TEvDualStreamHolder.CreateInMemory;
end;

function TEvBasicParams.EOS: Boolean;
begin
  Result := FStream.EOS;
end;

function TEvBasicParams.GetPosition: LongInt;
begin
  Result := FStream.Position;
end;

function TEvBasicParams.GetValueByName(
  ItemName: string): Variant;
var
  p, i: Integer;
  s: string;
begin
  with Stream do
  begin
    ItemName := UpperCase(ItemName);
    Position := 0;
    while not EOS do
    begin
      p := Position;
      if UpperCase(ReadString) = ItemName then
      begin
        Position := p;
        Result := NextItem(s);
        Seek(0, soEnd);
        Exit;
      end
      else
      begin
        Seek(SizeOf(TFieldType), soCurrent);
        i := ReadInteger;
        Seek(i, soCurrent);
      end;
    end;
  end;
  Result := Null;
end;

function TEvBasicParams.NextItem(
  out ItemName: string): Variant;
var
  fk: TFieldType;
begin
  with Stream do
  begin
    ItemName := ReadString;
    ReadBuffer(fk, SizeOf(fk));
    case fk of
    ftUnknown:
      begin
        ReadInteger;
        Result := Null;
      end;
    ftString:
      Result := ReadString;
    ftInteger:
      begin
        ReadInteger;
        Result := ReadInteger;
      end;
    ftCurrency:
      begin
        ReadInteger;
        Result := Currency(ReadDouble);
      end;
    ftFloat:
      begin
        ReadInteger;
        Result := ReadDouble;
      end;
    ftDateTime:
      begin
        ReadInteger;
        Result := TDateTime(ReadDouble);
      end;
    else
      Assert(False, IntToStr(Ord(fk)));
    end;
  end;
end;


procedure TEvBasicParams.SetPosition(const Value: LongInt);
begin
  FStream.Position := Value;
end;

procedure TEvBasicParams.SetValueByName(
  const ItemName: string; const v: Variant;
  const CheckForDuplicate: Boolean);
var
  ft: TFieldType;
  s: string; //gdy
begin
  with Stream do
  begin
    case VarType(v) of
    varEmpty, varNull:
      AddItem(ItemName, ftUnknown, nil^, 0, CheckForDuplicate);
    varInteger, varByte:
      AddItem(ItemName, ftInteger, TVarData(v).VInteger, SizeOf(TVarData(v).VInteger), CheckForDuplicate);
    varDouble:
      AddItem(ItemName, ftFloat, TVarData(v).VDouble, SizeOf(TVarData(v).VDouble), CheckForDuplicate);
    varDate:
      AddItem(ItemName, ftDateTime, TVarData(v).VDate, SizeOf(TVarData(v).VDate), CheckForDuplicate);
    varCurrency:
      AddItem(ItemName, ftCurrency, TVarData(v).VCurrency, SizeOf(TVarData(v).VCurrency), CheckForDuplicate);
    varString:
      AddItem(ItemName, ftString, TVarData(v).VString^, Length(v), CheckForDuplicate);
    (varByte or varArray):
      begin
      //AddItem(ItemName, ftString, string(TVarData(v).VString^)[1], Length(string(TVarData(v).VString^)), CheckForDuplicate);
        if CheckForDuplicate then
          StripItem(ItemName);
        WriteString(ItemName);
        ft := ftString;
        WriteBuffer(ft, SizeOf(ft));
        WriteVarArrayOfByte(v);
      end;
    varOleStr: //gdy
      begin
        s := OleStrToString( TVarData(v).VOleStr );
        AddItem(ItemName, ftString, pchar(s)^, Length(s), CheckForDuplicate); //same as varString
      end;
    else
      Assert(False);
    end;
  end;
end;

procedure TEvBasicParams.StripItem(ItemName: string);
var
  itn: string;
  m1: PByte;
  p, p2, s: Int64;
begin
  with Stream do
  begin
    ItemName := UpperCase(ItemName);
    Position := 0;
    while not EOS do
    begin
      p := Position;
      itn := UpperCase(ReadString);
      Seek(SizeOf(TFieldType), soCurrent);
      s := ReadInteger;
      Seek(s, soCurrent);
      if itn = ItemName then
      begin
        if Position < Size then
        begin
          p2 := Position;
          s := Size- p2;
          GetMem(m1, s);
          ReadBuffer(m1^, s);
          Position := p;
          WriteBuffer(m1^, s);
          FreeMem(m1);
          Size := Size- (p2- p);
          Position := p;
        end;
      end;
    end;
  end;
end;

{ TInterPackageData }

constructor TInterPackageData.Create;
begin
  FMemStream := TisStream.Create;
end;

procedure TInterPackageData.Clear;
begin
  FMemStream.Clear;
end;

constructor TInterPackageData.Create(const MemStream: IisStream);
begin
  Assert(Assigned(MemStream));
  FMemStream := MemStream;
  GotoFirst;
end;

function TInterPackageData.EndOfParams: Boolean;
begin
  Result := MemStream.EOS;
end;

procedure TInterPackageData.GotoFirst;
begin
  MemStream.Position := 0;
end;

{ TGetDataResults }

constructor TGetDataResults.Create;
begin
  inherited;
  SetLength(FQueryResults, 0);
end;

procedure TGetDataResults.FinalizeWrite;
var
  i: Integer;
  li: LongInt;
begin
  li := 0;
  for i := 0 to High(FQueryResults) do
  begin
    li := li+ FQueryResults[i].DualStream.Size+ 1+ 4;
  end;
  FMemStream := TEvDualStreamHolder.Create(li);
  for i := 0 to High(FQueryResults) do
  begin
    MemStream.WriteBoolean(FQueryResults[i].StreamFormat = sfOpenQuery);
    MemStream.WriteStream(FQueryResults[i].DualStream);
  end;
end;

function TGetDataResults.QueryResult(
  const Index: Integer): IEvQueryResult;
begin
  Result := FQueryResults[Index];
end;

function TGetDataResults.QueryResultCount: Integer;
begin
  Result := Length(FQueryResults);
end;

procedure TGetDataResults.Read(const cd: TEvBasicClientDataSet; const SkipPostProcess: Boolean; const ContentAsOfDate: TisDate = 0);
var
  i: Integer;
begin
  if MemStream.ReadBoolean then
  begin // Data is coming
    if not SkipPostProcess and (cd is TevClientDataSet) then
      TevClientDataSet(cd).PrepareLookups;
    cd.Close;
    i := MemStream.ReadInteger;
    if i > 0 then
    begin
      cd.SetDataAsOfDate(ContentAsOfDate);
      cd.LoadFromUniversalStream(MemStream);
      if not SkipPostProcess and (cd is TevClientDataSet) then
        TevClientDataSet(cd).UnprepareLookups;
    end;
  end;
end;

procedure TGetDataResults.Write(const QueryResult: IEvQueryResult);
begin
  SetLength(FQueryResults, Length(FQueryResults)+1);
  FQueryResults[High(FQueryResults)] := QueryResult;
end;


{ TExecDSWrapper }

constructor TExecDSWrapper.Create(const sQueryText: string; const AsOfDate: TisDate; ABlobsLazyLoad: Boolean);
begin
  Create;
  FQueryText := sQueryText;

  if AsOfDate <> 0 then
    FParams.AddValue('StatusDate', AsOfDate);

  BlobsLazyLoad := ABlobsLazyLoad;
end;

constructor TExecDSWrapper.Create;
begin
  FParams := TisListOfValues.Create;
  FMacros := TisListOfValues.Create;
end;

constructor TExecDSWrapper.Create(const Stream: IEvDualStream);
begin
  Create;
  FQueryText := Stream.ReadString;
  FParams.ReadFromStream(Stream);
  FMacros.ReadFromStream(Stream);
  FBlobsLazyLoad := Stream.ReadBoolean;
end;

procedure TExecDSWrapper.SetMacro(const sName: string; const Value: string; const CheckForDuplicate: Boolean);
begin
  FMacros.AddValue(sName, Value);
end;

procedure TExecDSWrapper.SetParam(const sName: string; const Value: Integer; const CheckForDuplicate: Boolean);
begin
  FParams.AddValue(sName, Value);
end;

procedure TExecDSWrapper.SetParam(const sName: string; const Value: string; const CheckForDuplicate: Boolean);
begin
  FParams.AddValue(sName, Value);
end;

function TExecDSWrapper.AsMemStream: IisStream;
begin
  Result := TisStream.Create;
  with Result do
  begin
    WriteString(FQueryText);
    FParams.WriteToStream(Result);
    FMacros.WriteToStream(Result);
    Result.WriteBoolean(FBlobsLazyLoad);
  end;
end;

function TExecDSWrapper.GetMacros: IisListOfValues;
begin
  Result := FMacros;
end;

function TExecDSWrapper.GetParams: IisListOfValues;
begin
  Result := FParams;
end;

function TExecDSWrapper.QueryText: string;
begin
  Result := FQueryText;
end;

procedure TExecDSWrapper.SetParamNull(const sName: string;
  CheckForDuplicate: Boolean);
begin
  FParams.AddValue(sName, Null);
end;

procedure TExecDSWrapper.SetParam(const sName: string; const Value: Double;
  const CheckForDuplicate: Boolean; const FieldType: TFieldType);
begin
  FParams.AddValue(sName, Value);
end;

function TExecDSWrapper.AsVariant: IExecDSWrapper;
begin
  Result := Self;
end;

procedure TExecDSWrapper.SetMacros(const AValue: IisListOfValues);
begin
  if Assigned(AValue) then
    FMacros := AValue
  else
    FMacros.Clear;
end;

procedure TExecDSWrapper.SetParams(const AValue: IisListOfValues);
begin
  if Assigned(AValue) then
    FParams := AValue
  else
    FParams.Clear;
end;

procedure TExecDSWrapper.SetParam(const sName: string;
  const Value: Variant; const CheckForDuplicate: Boolean);
begin
  FParams.AddValue(sName, Value);
end;

function TExecDSWrapper.GetBlobsLazyLoad: Boolean;
begin
  Result := FBlobsLazyLoad;
end;

procedure TExecDSWrapper.SetBlobsLazyLoad(const AValue: Boolean);
begin
  FBlobsLazyLoad := AValue;
end;

{ TGetDataParams }

constructor TGetDataParams.Create;
begin
  inherited;
  FWriteCounter := 0;
end;

procedure TGetDataParams.Read(out ProviderName: string;
  out OverrideClNbr: Integer; out ExecDsWrapper: IExecDSWrapper);
begin
  ProviderName := MemStream.ReadString;
  OverrideClNbr := MemStream.ReadInteger;
  ExecDsWrapper := TExecDSWrapper.Create(MemStream.ReadStream(TEvDualStreamHolder.CreateInMemory));
end;

procedure TGetDataParams.Write(const ProviderName: string;
  const OverrideClNbr: Integer; ExecDsWrapper: IExecDSWrapper);
begin
  with MemStream do
  begin
    WriteString(ProviderName);
    WriteInteger(OverrideClNbr);
    WriteStream(ExecDsWrapper.AsMemStream);
  end;
  Inc(FWriteCounter);
end;

{ TEvBasicClientDataSet }

procedure TEvBasicClientDataSet.DoAfterClose;
begin
  inherited;
  FDataAsOfDate := EmptyDate;
  FOpenedAsOfToday := False;
end;

procedure TEvBasicClientDataSet.BeforeDestruction;
begin
  if Assigned(OnDestroy) then
    OnDestroy(Self);
  inherited;
end;


function TEvBasicClientDataSet.GetAsOfDate: TisDate;
begin
  if FDataAsOfDate <> EmptyDate then
    Result := FDataAsOfDate
  else
    Result := FOpenAsOfDate;
end;

procedure TEvBasicClientDataSet.Reconcile(const MapList: IisValueMap);
var
  i: Integer;
begin
  Assert(Assigned(MapList));
  if MapList.Count > 0 then
  begin
    SetLength(FReconcileFields, 0);
    for i := 0 to FieldCount-1 do
      if (Fields[i].FieldKind = fkData)
      and (Fields[i] is TIntegerField)
      and (Copy(Fields[i].FieldName, Length(Fields[i].FieldName) - 3, 4) = '_NBR') then
      begin
        SetLength(FReconcileFields, Length(FReconcileFields)+1);
        FReconcileFields[High(FReconcileFields)] := Fields[i];
      end;
    FReconcileFieldsHigh := High(FReconcileFields);
    FReconcileMap := MapList;
    IterateRecords(ReconcileCallBack, False, False, False, True);
  end;

  CustomReconcile(MapList);

  FReconcileMap := nil;
  MergeChangeLog;
end;

procedure TEvBasicClientDataSet.ReconcileCallBack(const Version, Index, Total: Integer);
var
  j: Integer;
  V: Variant;
begin
  if UpdateStatus <> usDeleted then
    for j := 0 to FReconcileFieldsHigh do
      if FReconcileFields[j].AsInteger < 0 then
      begin
        V := FReconcileMap.DecodeValue(FReconcileFields[j].Value);
        if Assigned(FOnRecordReconcileEvent) then
          FOnRecordReconcileEvent(FReconcileFields[j], FReconcileFields[j].AsInteger, V);
        FReconcileFields[j].AsInteger := V;
      end;
end;

procedure TEvBasicClientDataSet.SetAsOfDate(const AValue: TisDate);
begin
  if AValue <> GetAsOfDate then
  begin
    Close;
    FOpenAsOfDate := AValue;
  end;
end;

procedure TEvBasicClientDataSet.SetDataAsOfDate(const AValue: TisDate);
begin
  Close;
  FDataAsOfDate := AValue;
  FOpenedAsOfToday := FDataAsOfDate = SysDate;
end;

procedure TEvBasicClientDataSet.ClearAsOfDateOverride;
begin
  FOpenAsOfDate := EmptyDate;
end;

function TEvBasicClientDataSet.UsingDefaultAsOfDate: Boolean;
begin
  Result := (FOpenAsOfDate = EmptyDate);
end;

procedure TEvBasicClientDataSet.CustomReconcile(const MapList: IisValueMap);
begin
end;

function TEvBasicClientDataSet.OpenedAsOfToday: Boolean;
begin
  Result := FOpenedAsOfToday;
end;

end.

