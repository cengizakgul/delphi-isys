inherited ClientChoiceQuery: TClientChoiceQuery
  Left = 470
  Width = 416
  Height = 372
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 304
    Width = 408
    inherited btnYes: TevBitBtn
      Left = 162
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 288
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 32
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 408
    Height = 304
    Selected.Strings = (
      'CUSTOM_CLIENT_NUMBER'#9'20'#9'Number'#9'F'
      'NAME'#9'40'#9'Name'#9'F')
  end
  inherited dsChoiceList: TevDataSource
    DataSet = DM_TMP_CL.TMP_CL
    Left = 192
    Top = 40
  end
  object DM_TEMPORARY: TDM_TEMPORARY [3]
    Left = 64
    Top = 40
  end
end
