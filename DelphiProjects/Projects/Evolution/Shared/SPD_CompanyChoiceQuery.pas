// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CompanyChoiceQuery;
// Usage:
//   Create
//   SetFilter
//   ShowModal
//   Free

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_SafeChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure, 
  SPD_ChoiceFromListQueryEx, ActnList, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDicttemp, ISBasicClasses, EvClientDataSet, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, ComCtrls;

type
  TCompanyChoiceQuery = class(TSafeChoiceFromListQuery)
    DM_TEMPORARY: TDM_TEMPORARY;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilter( flt: string );
  end;


implementation

uses
  evutils;
{$R *.DFM}

{ TCompanyChoiceQuery }

procedure TCompanyChoiceQuery.SetFilter(flt: string);
begin
//  cdsChoiceList.Close;
  //ODS('entering set filter');
  dgChoiceList.DataSource := nil;
  cdsChoiceList.Data := DM_TEMPORARY.TMP_CO.Data;
//  cdsChoiceList.CloneCursor( DM_TEMPORARY.TMP_CO, false, false );
  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
//  cdsChoiceList.Open;}
  dgChoiceList.DataSource := dsChoiceList;
end;

end.
