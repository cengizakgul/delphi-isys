unit EvVersionedAddressFrm;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit;

type
  TevVersionedAddress = class(TevVersionedFieldBase)
    pnlBottom: TevPanel;
    isUIFashionPanel1: TisUIFashionPanel;
    lAddress1: TevLabel;
    lAddress2: TevLabel;
    lCity: TevLabel;
    lState: TevLabel;
    lZipCode: TevLabel;
    edAddress1: TevDBEdit;
    edAddress2: TevDBEdit;
    edCity: TevDBEdit;
    edState: TevDBEdit;
    edZipCode: TevDBEdit;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedAddress }

procedure TevVersionedAddress.FormShow(Sender: TObject);
begin
  inherited;

  Fields[0].AddValue('Title', lAddress1.Caption);
  Fields[1].AddValue('Title', lAddress2.Caption);
  Fields[2].AddValue('Title', lCity.Caption);
  Fields[3].AddValue('Title', lState.Caption);
  Fields[4].AddValue('Title', lZipCode.Caption);

  Fields[0].AddValue('Width', 20);
  Fields[1].AddValue('Width', 20);
  Fields[2].AddValue('Width', 15);
  Fields[3].AddValue('Width', 6);
  Fields[4].AddValue('Width', 10);

  lAddress1.Caption := Iff(Fields[0].Value['Required'], '~', '') + lAddress1.Caption;
  edAddress1.DataField := Fields.ParamName(0);

  lAddress2.Caption := Iff(Fields[1].Value['Required'], '~', '') + lAddress2.Caption;
  edAddress2.DataField := Fields.ParamName(1);

  lCity.Caption := Iff(Fields[2].Value['Required'], '~', '') + lCity.Caption;
  edCity.DataField := Fields.ParamName(2);

  lState.Caption := Iff(Fields[3].Value['Required'], '~', '') + lState.Caption;
  edState.DataField := Fields.ParamName(3);

  lZipCode.Caption := Iff(Fields[4].Value['Required'], '~', '') + lZipCode.Caption;
  edZipCode.DataField := Fields.ParamName(4);
end;

end.

