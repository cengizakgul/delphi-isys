object InfoList: TInfoList
  Left = 0
  Top = 0
  Width = 501
  Height = 266
  TabOrder = 0
  object Panel: TevPanel
    Left = 0
    Top = 228
    Width = 501
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object evPanel1: TevPanel
      Left = 375
      Top = 0
      Width = 126
      Height = 38
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btnSend: TevButton
        Left = 6
        Top = 6
        Width = 112
        Height = 28
        Caption = 'Send'
        TabOrder = 0
        OnClick = btnSendClick
      end
    end
  end
  object Grid: TevStringGrid
    Left = 0
    Top = 0
    Width = 501
    Height = 228
    Align = alClient
    Anchors = []
    Color = clBtnFace
    ColCount = 3
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goRowSelect]
    TabOrder = 1
    SortOptions.CanSort = False
    SortOptions.SortStyle = ssNormal
    SortOptions.SortCaseSensitive = False
    SortOptions.SortCol = -1
    SortOptions.SortDirection = sdAscending
    ColWidths = (
      150
      350
      100)
  end
end
