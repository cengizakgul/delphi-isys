unit EvFieldValueToText;

interface

uses Variants, SysUtils, Types,
     isBaseClasses, isTypes, SDDStreamClasses, evDataSet, evCommonInterfaces,
     rwCustomDataDictionary, rwEngineTypes, IsBasicUtils, isStrUtils;

type
  IevFieldValueToTextConverter = interface
  ['{0D9D3B05-431C-4DAD-BD82-498B6A5FA3CA}']
    function Convert(const ATable, AField: String; const AValue: Variant; const AAsOfDate: TisDate; const AddValueToText: Boolean): String;
  end;


  TEvFieldValueToTextConverter = class(TisInterfacedObject, IevFieldValueToTextConverter)
  private
    FImpliedTables: IisStringList;
    FCache: IisListOfValues;

    function  GetDataSetFromCache(const AKey: String): IevDataSet;
    procedure BuildImplTableList(ATable: TddsTable);
    function  FieldValueAsText(const ATable, AField: String; const AValue: Variant; const AAsOfDate: TisDate; const AddValueToText: Boolean): String;

  protected
    function Convert(const ATable, AField: String; const AValue: Variant; const AAsOfDate: TisDate; const AddValueToText: Boolean): String;
  end;

implementation

uses isDataSet, DB, DateUtils, Classes;


procedure TEvFieldValueToTextConverter.BuildImplTableList(ATable: TddsTable);
var
  mk: TddsForeignKey;
begin
  FImpliedTables.Add(ATable.Name);
  mk := ATable.GetMasterKey;
  if Assigned(mk) then
  begin
    FImpliedTables.Add(TddsTable(mk.Table).Name);
    BuildImplTableList(TddsTable(mk.Table));
  end;
end;

function TEvFieldValueToTextConverter.FieldValueAsText(const ATable, AField: String;
  const AValue: Variant; const AAsOfDate: TisDate; const AddValueToText: Boolean): String;
const
  EnumDSConsts: array [0..1] of TDSFieldDef = (
    (FieldName: 'value';  DataType: ftString;  Size: 10;  Required: False),
    (FieldName: 'description';  DataType: ftString;  Size: 256;  Required: False));
var
  Q: IevQuery;
  tbl: TddsTable;
  fld: TddsField;
  fk, ref: TddsForeignKey;
  i: Integer;
  s, s2, dispFld, sDispKey, cacheKey: String;
  DS: IevDataSet;
  d: TDateTime;
begin
  if AValue <> Null then
  begin
    tbl :=  TddsTable(EvoDataDictionary.Tables.TableByName(ATable));
    fld := TddsField(tbl.Fields.FieldByName(AField));
    fk := TddsForeignKey(tbl.ForeignKeys.ForeignKeyByField(fld));
  end
  else
  begin
    fk := nil;
    fld := nil;
    tbl := nil;
  end;

  if not Assigned(fk) then
  begin
    Result := Trim(VarToStr(AValue));
    if Assigned(fld) then
    begin
      if fld.FieldValues.Count > 0 then
      begin
        if AddValueToText then
        begin
          Result := fld.FieldValues.Values[Result];
          Result := Result + ' [' + VarToStr(AValue) + ']';
        end
        else if fld.DisplayFormat <> '' then
        begin
          cacheKey := tbl.Name + '.' + fld.Name;
          DS := GetDataSetFromCache(cacheKey);
          if not Assigned(DS) then
          begin
            DS := TevDataSet.Create(EnumDSConsts);
            for i := 0 to fld.FieldValues.Count - 1 do
            begin
              s := fld.FieldValues[i];
              s2 := GetNextStrValue(s, '=');
              DS.AppendRecord([s2, s]);
            end;
            DS.IndexFieldNames := 'value';
            FCache.AddValue(cacheKey, DS);
          end;
          if DS.FindKey([AValue]) then
            Result := FormatDisplayKey(DS.Fields, fld.DisplayFormat);
        end
        else
          Result := fld.FieldValues.Values[Result];
      end
      else if fld.DisplayFormat <> '' then
      begin
        // TODO: add formating for other types
      end;
    end;
    Exit;
  end;

  if not Assigned(FImpliedTables) then
  begin
    // Need to make list of tables on which lookups should not be resolved
    FImpliedTables := TisStringList.CreateUnique;
    BuildImplTableList(tbl);
  end;

  tbl := TddsTable(fk.Table);
  sDispKey := tbl.DisplayKey;

  if sDispKey = '' then
    Result := Trim(VarToStr(AValue))
  else
  begin
    cacheKey := tbl.Name;
    DS := GetDataSetFromCache(cacheKey);

    if not Assigned(DS) then
    begin
      s := 't.' + tbl.PrimaryKey[0].Field.Name + ', t.effective_date';
      while sDispKey <> '' do
      begin
        GetNextStrValue(sDispKey, '[');
        if sDispKey = '' then
          break;
        dispFld := GetNextStrValue(sDispKey, ']');
        dispFld := GetNextStrValue(dispFld, ':');

        fld := TddsField(tbl.Fields.FieldByName(dispFld));
        Assert(Assigned(fld));
        if fld.FieldType = ddtInteger then
        begin
          ref := TddsForeignKey(tbl.ForeignKeys.ForeignKeyByField(fld));
          if Assigned(ref) and (FImpliedTables.IndexOf(ref.Table.Name) <> -1) then
            continue;
          AddStrValue(s, 'CAST(t.' + fld.Name + ' AS VARCHAR(80)) AS ' + fld.Name, ', ');
        end
        else if (fld.FieldType = ddtString) and (fld.Size < 80) then
          AddStrValue(s, 'CAST(t.' + fld.Name + ' AS VARCHAR(80)) AS ' + fld.Name, ', ')
        else
          AddStrValue(s, 't.' + fld.Name, ', ');
      end;

      Q := TevQuery.CreateFmt('SELECT %s FROM %s t', [s, tbl.Name]);

      DS := Q.Result;
      DS.LogChanges := False;
      DS.IndexFieldNames := DS.Fields[0].FieldName + ';' + DS.Fields[1].FieldName;

      FCache.AddValue(cacheKey, DS);
    end;

    d := AAsOfDate;
    if not DS.FindNearest([AValue, d]) then
      s := 'BROKEN REFERENCE'
    else
    begin
      if (DS.Fields[0].AsInteger <> AValue) or (CompareDate(DS.Fields[1].AsDateTime, d) = GreaterThanValue) then
        DS.Prior;

      if (DS.Fields[0].AsInteger <> AValue) or (CompareDate(DS.Fields[1].AsDateTime, d) = GreaterThanValue) then
        s := 'BROKEN REFERENCE'
      else
      begin
        s := '';
        DS.Edit;
        for i := 2 to DS.Fields.Count - 1 do
          if DS.Fields[i].DataType = ftString then
            DS.Fields[i].Value := FieldValueAsText(tbl.Name, DS.Fields[i].FieldName, DS.Fields[i].Value, AAsOfDate, False); // recursion!
        s := FormatDisplayKey(DS.Fields, tbl.DisplayKey);
        DS.Cancel;
      end;
    end;

    Result := s;
  end;

  if AddValueToText then
    Result := Result + ' [' + VarToStr(AValue) + ']';
end;

function TEvFieldValueToTextConverter.Convert(const ATable, AField: String;
  const AValue: Variant; const AAsOfDate: TisDate; const AddValueToText: Boolean): String;
begin
  Result := FieldValueAsText(ATable, AField, AValue, AAsOfDate, AddValueToText);
end;

function TEvFieldValueToTextConverter.GetDataSetFromCache(const AKey: String): IevDataSet;
begin
  if not Assigned(FCache) then
    FCache := TisListOfValues.Create;

  Result := nil;
  Result := IInterface(FCache.TryGetValue(AKey, Result)) as IevDataSet;
end;

end.

