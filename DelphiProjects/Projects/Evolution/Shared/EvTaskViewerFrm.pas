unit EvTaskViewerFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvCommonInterfaces, EvTaskResultFrame, EvConsts, RReportPreview,
  RProcessPR, RPrPreProcessList, RReprintPR, RPayTaxes, RProcessSBACH,
  EvTaskParamFrame, PReportParams, PPrPreprocessList, PPrCreateParams,
  PPreProcessForQuarter, PProcessQuarterReturns, PPrQueueList, PRebuildTempTables,
  StdCtrls, RQEC, RPAMC, RPreProcessForQuarter, RProcessQuarterReturns, isBaseClasses,
  RProcessPrenoteACH, RProcessManualACH, EvTaskRequestsFrame, EvoXImportTaskResult,
  PAcaUpdate, RAcaUpdate;

type
  TTaskViewer = class(TFrame)
  private
    FItem: IInterface;
    FViewerObj: TWinControl;
  protected
    function GetTaskResultClass(const ATaskType: String): TTaskResultFrameClass;
    function GetTaskParamClass(const ATaskType: String): TTaskParamFrameClass;
    function GetTaskRequestsClass(const ATaskType: String): TTaskRequestsFrameClass;
  public
    procedure BeforeDestruction; override;
    function  AssignedItem: IInterface;
    procedure ShowTaskParams(const ATask: IevTask);
    procedure ShowTaskRequests(const ARequestList: IisListOfValues);
    procedure ShowTaskResults(const ATask: IevTask);
    procedure CloseTaskInfo;
    procedure ApplyUpdates;
    procedure CancelUpdates;
  end;

implementation

{$R *.dfm}

{ TTaskViewer }


procedure TTaskViewer.ApplyUpdates;
begin
  if FViewerObj is TTaskParamFrame then
    TTaskParamFrame(FViewerObj).ApplyUpdates;
end;

procedure TTaskViewer.CancelUpdates;
begin
  if FViewerObj is TTaskParamFrame then
    TTaskParamFrame(FViewerObj).CancelUpdates;
end;

function TTaskViewer.AssignedItem: IInterface;
begin
  Result := FItem;
end;

procedure TTaskViewer.BeforeDestruction;
begin
  CloseTaskInfo;
  inherited;
end;

procedure TTaskViewer.CloseTaskInfo;
begin
  FItem := nil;
  FreeAndNil(FViewerObj);
end;

function TTaskViewer.GetTaskParamClass(const ATaskType: String): TTaskParamFrameClass;
begin
  if AnsiSameText(ATaskType, QUEUE_TASK_RUN_REPORT) then
    Result := TpfReportParams
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_PAYROLL) then
    Result := TpfPrQueueList
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_PAYROLL) then
    Result := TpfPrPreprocessList
  else if AnsiSameText(ATaskType, QUEUE_TASK_CREATE_PAYROLL) then
    Result := TpfPrCreateParams
  else if AnsiSameText(ATaskType, QUEUE_TASK_REBUILD_TMP_TABLES) then
    Result := TpfRebuildTempTables
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_QUARTER_END) then
    Result := TpfPreProcessForQuarterParams
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_TAX_RETURNS) then
    Result := TProcessQuarterReturnsParamFrame
  else if AnsiSameText(ATaskType, QUEUE_TASK_ACA_STATUS_UPDATE) then
    Result := TpfAcaUpdate
  else
    Result := TTaskParamFrame;
end;

function TTaskViewer.GetTaskRequestsClass(const ATaskType: String): TTaskRequestsFrameClass;
begin
  Result := TTaskRequestsFrame;
end;

function TTaskViewer.GetTaskResultClass(const ATaskType: String): TTaskResultFrameClass;
begin
  if AnsiSameText(ATaskType, QUEUE_TASK_RUN_REPORT) then
    Result := TrfReportPreview
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_PAYROLL) then
    Result := TrfProcessPR
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_PAYROLL) then
    Result := TrfPrPreprocessList
  else if AnsiSameText(ATaskType, QUEUE_TASK_REPRINT_PAYROLL) then
    Result := TrfReprintPR
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_TAX_PAYMENTS) then
    Result := TPayTaxesReturnsResultFrame
  else if AnsiSameText(ATaskType, QUEUE_TASK_PAYROLL_ACH) then
    Result := TrfProcessSBACH
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_PRENOTE_ACH) then
    Result := TrfProcessPrenoteACH
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_MANUAL_ACH) then
    Result := TrfProcessManualACH
  else if AnsiSameText(ATaskType, QUEUE_TASK_PREPROCESS_QUARTER_END) then
    Result := TrfPreProcessForQuarter
  else if AnsiSameText(ATaskType, QUEUE_TASK_PROCESS_TAX_RETURNS) then
    Result := TrfProcessTaxReturns
  else if AnsiSameText(ATaskType, QUEUE_TASK_QEC) then
    Result := TrfQEC
  else if AnsiSameText(ATaskType, QUEUE_TASK_PAMC) then
    Result := TrfPAMC
  else if AnsiSameText(ATaskType, QUEUE_TASK_EVOX_IMPORT) then
    Result := TrfEvoXImportTask
  else if AnsiSameText(ATaskType, QUEUE_TASK_ACA_STATUS_UPDATE) then
    Result := TrfAcaUpdate
  else
    Result := TTaskResultFrame;
end;

procedure TTaskViewer.ShowTaskParams(const ATask: IevTask);
begin
  CloseTaskInfo;
  if Assigned(ATask) then
    FViewerObj := GetTaskParamClass(ATask.TaskType).Create(Self, Self, ATask);
  FItem := ATask;
end;

procedure TTaskViewer.ShowTaskRequests(const ARequestList: IisListOfValues);
var
  Cl: TTaskRequestsFrameClass;
begin
  if Assigned(ARequestList) then
  begin
    Cl := GetTaskRequestsClass('');
    if not Assigned(FViewerObj) or (FViewerObj.ClassType <> Cl) then
    begin
      CloseTaskInfo;
      FViewerObj := Cl.Create(Self, Self, ARequestList);
    end
    else
      (FViewerObj as TTaskRequestsFrame).UpdateContent(ARequestList);
  end
  else
    CloseTaskInfo;

  FItem := ARequestList;
end;

procedure TTaskViewer.ShowTaskResults(const ATask: IevTask);
begin
  CloseTaskInfo;
  if Assigned(ATask) then
    FViewerObj := GetTaskResultClass(ATask.TaskType).Create(Self, Self, ATask);
  FItem := ATask;
end;

end.
