// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCalendarCreation;

interface

uses SysUtils, Classes,  Graphics, DB, Math, Windows, EvUtils, SDataStructure,
  EvConsts, EvTypes,  Variants, EvBasicUtils,
  Dialogs, DateUtils, Controls, EvDataAccessComponents, EvCommonInterfaces,
  EvDataset, EvContext;

  type DateType = (dtCheck, dtCallIn, dtDelivery, dtEqual);

  TCalendarDefaults = record
    CO_NBR                        : Integer;
    LAST_REAL_SCHEDULED_CHECK_DATE: TDate;
    CALL_IN_TIME                  : TTime;
    DELIVERY_TIME                 : TTime;
    NUMBER_OF_DAYS_PRIOR          : Integer;
    NUMBER_OF_DAYS_AFTER          : Integer;
    FREQUENCY                     : String;
    FILLER                        : String;
    PERIOD_BEGIN_DATE             : TDate;
    PERIOD_END_DATE               : TDate;
    LAST_REAL_SCHED_CHECK_DATE2   : TDate;
    CALL_IN_TIME2                 : TTime;
    DELIVERY_TIME2                : TTime;
    NUMBER_OF_DAYS_PRIOR2         : Integer;
    NUMBER_OF_DAYS_AFTER2         : Integer;
    PERIOD_BEGIN_DATE2            : TDate;
    PERIOD_END_DATE2              : TDate;
    MOVE_CHECK_DATE               : Integer;
    MOVE_CALLIN_DATE              : Integer;
    MOVE_DELIVERY_DATE            : Integer;
    NUMBER_OF_MONTHS              : Integer;
  end;


  function IsWeekend(aDate: TDateTime; Adjustment: Integer; var MoveBy: Integer): Boolean;
  function IsHoliday(DS: TEvBasicClientDataSet; aDate: TDateTime; aDateType: DateType): Boolean;
  function AdjustDate(DS: TEvBasicClientDataSet; aDate: TDateTime; Adjustment: Integer; aDateType: DateType): TDateTime;
  function HasWeekly(Frequency: Char): Boolean;
  function HasBiWeekly(Frequency: Char): Boolean;
  function HasSemiMonthly(Frequency: Char): Boolean;
  function HasMonthly(Frequency: Char): Boolean;
  function HasQuarterly(Frequency: Char): Boolean;
  Procedure CheckCalendarDefaults(var CalendarDefaults :TCalendarDefaults; Frequency : Char);
  procedure CreateCalendar(DS, Holiday: TEvBasicClientDataSet;var CalendarDefaults: TCalendarDefaults;
                            CheckDate, PeriodBeginDate, PeriodEndDate : TDate;
                            CheckEndOfMonth, PeriodEndOfMonth, FirstPart : Boolean); 

  procedure GetHolidayList(DS: TEvBasicClientDataSet);

  function GetWeekly_BiWeeklyDefaults(CoNbr, AddAmount: Integer; Frequency: String; var CheckDate: TDate;
                                      var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime;
                                      var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;

  function GetSemiMonthlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate; var PeriodBegin: TDate;
                                  var PeriodEnd: TDate; var CallIn: TDateTime; var Delivery: TDateTime;
                                  var CheckDate2: TDate; var PeriodBegin2: TDate; Var PeriodEnd2: TDate;
                                  var CallIn2: TDateTime; var Delivery2: TDateTime; var DAYS_PRIOR: Integer;
                                  var DAYS_POST: Integer; var DAYS_PRIOR2: Integer; var DAYS_POST2: Integer): Boolean;

  function GetMonthlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate;
                                      var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime;
                                      var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;

  function GetQuarterlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate;
                                      var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime;
                                      var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;

  procedure AddToCalendar(DS, Holiday: TEvBasicClientDataSet; var WCrossed: Boolean; var BCrossed: Boolean; var SCrossed: Boolean; var MCrossed: Boolean; var QCrossed: Boolean);

  procedure UpdateDefaultsByFrequency(Defaults: TCalendarDefaults);
  function GetDefaultsFromTable(ACompany: Integer; AFrequency: String; var CalendarDefaults: TCalendarDefaults): Boolean;
  function ReturnCBIndex(AValue: Integer): Integer;

implementation

uses StrUtils;

  function IsWeekend(aDate: TDateTime; Adjustment: Integer; var MoveBy: Integer): Boolean;
  begin
    if (DayOfWeek(aDate) = 1) and (Adjustment < 0) then
      MoveBy := 2
    else if (DayOfWeek(aDate) = 1) and (Adjustment > 0) then
      MoveBy := 1;

    if (DayOfWeek(aDate) = 7) and (Adjustment < 0) then
      MoveBy := 1
    else if (DayOfWeek(aDate) = 7) and (Adjustment > 0) then
      MoveBy := 2;
{
    if (DayOfWeek(aDate) = 7) then
      MoveBy := 1;
 }
    Result := (DayOfWeek(aDate) = 1) or (DayOfWeek(aDate) = 7);
  end;

  function IsHoliday(DS: TEvBasicClientDataSet; aDate: TDateTime; aDateType: DateType): Boolean;
  begin
    Result := False;
    if DS.Locate('HOLIDAY_DATE', aDate, []) then
      if (DS['USED_BY'] = GROUP_BOX_SB) and (aDateType = dtCheck) then
        Result := False
      else
        Result := True;
  end;

  function AdjustDate(DS: TEvBasicClientDataSet; aDate: TDateTime; Adjustment: Integer; aDateType: DateType): TDateTime;
  var
    MoveBy: Integer;
    i: Integer;
    aWeekend: Boolean;
  begin
    i := 0;
    if Adjustment < 0 then
      i := -1;
    if Adjustment > 0 then
      i := 1;


    Result := aDate;
    MoveBy := 0;

    if Adjustment = 0 then
      Exit;

    if (aDateType = dtEqual) then
      Result := IncDay(Result, Adjustment);

    aWeekend := IsWeekend(Result, Adjustment, MoveBy);
    if aWeekend then
      Result := IncDay(Result, MoveBy*i);

     while IsHoliday(DS, Result, aDateType) do
     begin
       Result := IncDay(Result, Adjustment);
       aWeekend := IsWeekend(Result, Adjustment, MoveBy);
       if aWeekend then
         Result := IncDay(Result, MoveBy*i);
     end;

  end;

  function HasWeekly(Frequency: Char): Boolean;
  begin
    Result := (Frequency in [CO_FREQ_WEEKLY, CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_WEEKLY_SMONTHLY,
                          CO_FREQ_WEEKLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]);
  end;

  function HasBiWeekly(Frequency: Char): Boolean;
  begin
    Result := (Frequency in [CO_FREQ_BWEEKLY, CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_BWEEKLY_SMONTHLY,
                          CO_FREQ_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]);
  end;

  function HasSemiMonthly(Frequency: Char): Boolean;
  begin
    Result := (Frequency in [CO_FREQ_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_SMONTHLY,
                          CO_FREQ_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY,
                          CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]);
  end;

  function HasMonthly(Frequency: Char): Boolean;
  begin
    Result := (Frequency in [CO_FREQ_MONTHLY, CO_FREQ_WEEKLY_MONTHLY, CO_FREQ_BWEEKLY_MONTHLY,
                          CO_FREQ_SMONTHLY_MONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY,
                          CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
                          CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY]);
  end;

  function HasQuarterly(Frequency: Char): Boolean;
  begin
    Result := (Frequency in [CO_FREQ_QUARTERLY]);
  end;


  Procedure CheckCalendarDefaults(var CalendarDefaults :TCalendarDefaults; Frequency : Char);
  var
   tmpDate : TDate;
   pCount : integer;
   s : String;
   Q : IevQuery;
  begin
      s := 'select a.CO_NBR, a.SCHEDULED_CHECK_DATE, b.FREQUENCY, b.PERIOD_BEGIN_DATE, b.PERIOD_END_DATE ' +
              'from PR_SCHEDULED_EVENT a, PR_SCHEDULED_EVENT_BATCH b ' +
              'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
              'a.PR_SCHEDULED_EVENT_NBR = b.PR_SCHEDULED_EVENT_NBR and ' +
              'b.FREQUENCY = :Frequency and a.CO_NBR  = :Co_Nbr ' +
              'order by a.CO_NBR, b.FREQUENCY, a.SCHEDULED_CHECK_DATE';

      if not Assigned(Q) then
         Q := TevQuery.Create(s)
      else
         Q.Result.Clear;
      Q.Params.AddValue('Frequency', Frequency);
      Q.Params.AddValue('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      Q.Execute;
      if Q.Result.RecordCount > 0 then
      begin
        if Q.Result.Locate('FREQUENCY;PERIOD_BEGIN_DATE;PERIOD_END_DATE',
              VarArrayOf([CalendarDefaults.FREQUENCY,CalendarDefaults.PERIOD_BEGIN_DATE,
                           CalendarDefaults.PERIOD_END_DATE]), []) then
        begin
           if (CalendarDefaults.FREQUENCY = CO_FREQ_WEEKLY) or
              (CalendarDefaults.FREQUENCY = CO_FREQ_BWEEKLY) then
           begin
            tmpDate := IncMonth(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE , CalendarDefaults.NUMBER_OF_MONTHS);
            pCount := WeeksBetween(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE, tmpDate);
            CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncDay(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE, pCount*7);
            CalendarDefaults.PERIOD_BEGIN_DATE := IncDay(CalendarDefaults.PERIOD_BEGIN_DATE, pCount*7);
            CalendarDefaults.PERIOD_END_DATE := IncDay(CalendarDefaults.PERIOD_END_DATE, pCount*7);
           end
           else
           if CalendarDefaults.FREQUENCY = CO_FREQ_QUARTERLY then
           begin
            CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncYear(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE, CalendarDefaults.NUMBER_OF_MONTHS div 12);
            CalendarDefaults.PERIOD_BEGIN_DATE := IncYear(CalendarDefaults.PERIOD_BEGIN_DATE, CalendarDefaults.NUMBER_OF_MONTHS div 12);
            CalendarDefaults.PERIOD_END_DATE := IncYear(CalendarDefaults.PERIOD_END_DATE, CalendarDefaults.NUMBER_OF_MONTHS div 12);
           end
           else
           begin
             CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncMonth(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
             CalendarDefaults.PERIOD_BEGIN_DATE := IncMonth(CalendarDefaults.PERIOD_BEGIN_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
             CalendarDefaults.PERIOD_END_DATE := IncMonth(CalendarDefaults.PERIOD_END_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
             if CalendarDefaults.FREQUENCY = CO_FREQ_SMONTHLY then
             begin
               CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2 := IncMonth(CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
               CalendarDefaults.PERIOD_BEGIN_DATE2 := IncMonth(CalendarDefaults.PERIOD_BEGIN_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
               CalendarDefaults.PERIOD_END_DATE2 := IncMonth(CalendarDefaults.PERIOD_END_DATE, CalendarDefaults.NUMBER_OF_MONTHS);
             end;
           end;
        end;
      end;
  end;

  procedure CreateCalendar(DS, Holiday: TEvBasicClientDataSet;var CalendarDefaults: TCalendarDefaults;
                          CheckDate, PeriodBeginDate, PeriodEndDate : TDate;
                          CheckEndOfMonth, PeriodEndOfMonth, FirstPart : Boolean);
  var
    i, x, CalculateDay: Integer;
    BusinessDay : integer;
    CI, DD: TDateTime;
    LCount : Integer;
    DaysBefore : Integer;

    pNUMBER_OF_DAYS_AFTER : integer;
    pCALL_IN_TIME, pDELIVERY_TIME :  TDateTime;
    LastDate : TDate;

  begin

    pCALL_IN_TIME := CalendarDefaults.CALL_IN_TIME;
    pDELIVERY_TIME := CalendarDefaults.DELIVERY_TIME;
    pNUMBER_OF_DAYS_AFTER := CalendarDefaults.NUMBER_OF_DAYS_AFTER;
    CalculateDay := 0;
    DaysBefore := -CalendarDefaults.NUMBER_OF_DAYS_PRIOR;

    if (CalendarDefaults.FREQUENCY = CO_FREQ_WEEKLY) or
       (CalendarDefaults.FREQUENCY = CO_FREQ_BWEEKLY) then
    begin
      LastDate := IncMonth(CheckDate, CalendarDefaults.NUMBER_OF_MONTHS);
      if (CalendarDefaults.FREQUENCY = CO_FREQ_WEEKLY) then
      begin
        x := 7;
        LCount := WeeksBetween(CheckDate, LastDate);
      end
      else
      begin
        x := 14;
        LCount := WeeksBetween(CheckDate, LastDate) div 2;
      end;
      CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncDay(CheckDate, x*LCount);
      CalendarDefaults.PERIOD_BEGIN_DATE := IncDay(PeriodBeginDate, x*LCount);
      CalendarDefaults.PERIOD_END_DATE := IncDay(PeriodEndDate, x*LCount);
    end
    else
    if CalendarDefaults.FREQUENCY = CO_FREQ_QUARTERLY then
    begin
      x := 3;
      LCount := CalendarDefaults.NUMBER_OF_MONTHS div 3;
      CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncYear(CheckDate, CalendarDefaults.NUMBER_OF_MONTHS div 12);
      CalendarDefaults.PERIOD_BEGIN_DATE := IncYear(PeriodBeginDate, CalendarDefaults.NUMBER_OF_MONTHS div 12);
      CalendarDefaults.PERIOD_END_DATE := IncYear(PeriodEndDate, CalendarDefaults.NUMBER_OF_MONTHS div 12);
    end
    else
    begin
      x := 1;
      LCount := CalendarDefaults.NUMBER_OF_MONTHS;

      if FirstPart then
      begin
       CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := IncMonth(CheckDate, CalendarDefaults.NUMBER_OF_MONTHS);
       CalendarDefaults.PERIOD_BEGIN_DATE := IncMonth(PeriodBeginDate, CalendarDefaults.NUMBER_OF_MONTHS);
       CalendarDefaults.PERIOD_END_DATE := IncMonth(PeriodEndDate, CalendarDefaults.NUMBER_OF_MONTHS);
      end
      else
      begin
       CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2 := IncMonth(CheckDate, CalendarDefaults.NUMBER_OF_MONTHS);
       CalendarDefaults.PERIOD_BEGIN_DATE2 := IncMonth(PeriodBeginDate, CalendarDefaults.NUMBER_OF_MONTHS);
       CalendarDefaults.PERIOD_END_DATE2 := IncMonth(PeriodEndDate, CalendarDefaults.NUMBER_OF_MONTHS);

       pCALL_IN_TIME := CalendarDefaults.CALL_IN_TIME2;
       pDELIVERY_TIME := CalendarDefaults.DELIVERY_TIME2;
       pNUMBER_OF_DAYS_AFTER := CalendarDefaults.NUMBER_OF_DAYS_AFTER2;
       DaysBefore := -CalendarDefaults.NUMBER_OF_DAYS_PRIOR2;
      end;
    end;


    if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
    begin
      CalculateDay :=1;
      DaysBefore := DaysBefore*(-1);                 //  This - value for reguler day, for business day has to be +
    end;

    for i := 0 to LCount - 1 do
    begin
      DS.Append;
      DS['PAY_FREQUENCY'] := CalendarDefaults.FREQUENCY;;
      if (CalendarDefaults.FREQUENCY = CO_FREQ_WEEKLY) or
          (CalendarDefaults.FREQUENCY = CO_FREQ_BWEEKLY) then
      begin
        DS['CHECK_DATE'] := AdjustDate(Holiday, IncDay(CheckDate, i*x), CalendarDefaults.MOVE_CHECK_DATE, dtCheck);
        DS['LAST_REAL_CHECK_DATE'] := IncDay(CheckDate, i*x);
        DS['PERIOD_BEGIN_DATE'] := IncDay(PeriodBeginDate, i*x);
        DS['PERIOD_END_DATE'] := IncDay(PeriodEndDate, i*x);
      end
      else
      begin
        if not CheckEndOfMonth then
          DS['CHECK_DATE'] := AdjustDate(Holiday, IncMonth(CheckDate, i*x), CalendarDefaults.MOVE_CHECK_DATE, dtCheck)
        else
          DS['CHECK_DATE'] := AdjustDate(Holiday, EndOfTheMonth(IncMonth(CheckDate, i*x)), CalendarDefaults.MOVE_CHECK_DATE, dtCheck);
        DS['LAST_REAL_CHECK_DATE'] := IncMonth(CheckDate, i*x);
        DS['PERIOD_BEGIN_DATE'] := IncMonth(PeriodBeginDate, i*x);
        if not PeriodEndOfMonth then
          DS['PERIOD_END_DATE'] := IncMonth(PeriodEndDate, i*x)
        else
          DS['PERIOD_END_DATE'] := EndOfTheMonth(IncMonth(PeriodEndDate, i*x));
      end;
      if CalculateDay = 1 then      //   Business Day Calculating
       Begin
         CI := DS['CHECK_DATE'];
          ReplaceTime(CI, pCALL_IN_TIME);
         if DaysBefore > 0 then
          for BusinessDay := 0 to DaysBefore-1 do
           Begin
             CI := IncDay(CI, -1);
             CI := AdjustDate(Holiday, CI, -1, dtCallIn);
           End;
         DS['CALL_IN_DATE'] := CI;
         DS['LAST_REAL_CALL_IN_DATE'] := CI;

         DD := CI;
         ReplaceTime(DD, pDELIVERY_TIME);
         if pNUMBER_OF_DAYS_AFTER > 0 then
          for BusinessDay := 0 to pNUMBER_OF_DAYS_AFTER-1 do
           Begin
            DD := IncDay(DD, 1);
            DD := AdjustDate(Holiday, DD, 1, dtDelivery);
           End;
         DS['DELIVERY_DATE'] := DD;
         DS['LAST_REAL_DELIVERY_DATE'] := DD;
       End
       else                            //   Regular Day Calculating
       Begin
         CI := IncDay(DS['CHECK_DATE'], DaysBefore);
         ReplaceTime(CI, pCALL_IN_TIME);
         DS['CALL_IN_DATE'] := AdjustDate(Holiday, CI, CalendarDefaults.MOVE_CALLIN_DATE, dtCallIn);
         DS['LAST_REAL_CALL_IN_DATE'] := CI;
         DD := IncDay(CI, pNUMBER_OF_DAYS_AFTER);
         ReplaceTime(DD, pDELIVERY_TIME);
         DS['DELIVERY_DATE'] := AdjustDate(Holiday, DD, CalendarDefaults.MOVE_DELIVERY_DATE, dtDelivery);
         DS['LAST_REAL_DELIVERY_DATE'] := DD;
       end;

      DS['DAYS_PRIOR'] := DaysBefore;
      DS['DAYS_POST'] := pNUMBER_OF_DAYS_AFTER;
      DS.Post;
    end;
  end;


  function CheckForCrossingYears(CheckDate, PeriodBegin, PeriodEnd: TDateTime): Boolean;
  begin
    Result := False;
    if YearOf(CheckDate) <> YearOf(PeriodBegin) then
      Result := True;

    if YearOf(CheckDate) <> YearOf(PeriodEnd) then
      Result := True;
  end;

  procedure GetHolidayList(DS: TEvBasicClientDataSet);
  begin
    DS.Close;
    ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelectCurrentOrdered') do
    begin
      SetMacro('Columns', 'holiday_date, holiday_name, used_by');
      SetMacro('TableName', 'sb_holidays');
      SetMacro('Order', 'holiday_date');
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.SB_CUSTOM_VIEW.Open;
    DS.Data := ctx_DataAccess.SB_CUSTOM_VIEW.Data;
  end;

  function GetWeekly_BiWeeklyDefaults(CoNbr, AddAmount: Integer; Frequency: String; var CheckDate: TDate; var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime; var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;
  begin
    Result := False;
    DAYS_PRIOR := 0;
    DAYS_POST := 0;
    with TExecDSWrapper.Create('SelectLastScheduledFrequencyCheckDate') do
    begin
      SetParam('CoNbr', CoNbr);
      SetParam('Freq', Frequency);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount <> 0 then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Last;
      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString <> '' then
      begin
        CheckDate := StrToDate(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 1, 10));
        CheckDate := IncDay(CheckDate, AddAmount);
        if Length(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString) > 10 then
        begin
          DAYS_PRIOR := Abs(StrToInt(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 11, 3)));
          DAYS_POST := Abs(StrToInt(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 14, 3)));
        end;
      end
      else
      begin
        CheckDate   := IncDay(ctx_DataAccess.CUSTOM_VIEW['scheduled_check_date'], AddAmount);
        Result := True;
      end;

      PeriodBegin := IncDay(ctx_DataAccess.CUSTOM_VIEW['period_begin_date'], AddAmount);
      PeriodEnd   := IncDay(ctx_DataAccess.CUSTOM_VIEW['period_end_date'], AddAmount);

      CallIn := CheckDate;
      ReplaceTime(CallIn, ctx_DataAccess.CUSTOM_VIEW['scheduled_call_in_date']);

      Delivery := CallIn;
      ReplaceTime(Delivery, ctx_DataAccess.CUSTOM_VIEW['scheduled_delivery_date']);

    end
    else
    begin
      CheckDate   := EncodeDate(YearOf(Today), 1, 1);
      PeriodBegin := CheckDate;
      PeriodEnd   := CheckDate;
      CallIn      := CheckDate;
      Delivery    := CheckDate;
      Result := True;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close
  end;


  function GetSemiMonthlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate; var PeriodBegin: TDate;
                                  var PeriodEnd: TDate; var CallIn: TDateTime; var Delivery: TDateTime;
                                  var CheckDate2: TDate; var PeriodBegin2: TDate; Var PeriodEnd2: TDate;
                                  var CallIn2: TDateTime; var Delivery2: TDateTime; var DAYS_PRIOR: Integer;
                                  var DAYS_POST: Integer; var DAYS_PRIOR2: Integer; var DAYS_POST2: Integer): Boolean;
  begin
    DAYS_PRIOR := 0;
    DAYS_POST := 0;
    DAYS_PRIOR2 := 0;
    DAYS_POST2 := 0;
    Result := False;
    with TExecDSWrapper.Create('SelectLastScheduledFrequencyCheckDate') do
    begin
      SetParam('CoNbr', CoNbr);
      SetParam('Freq', Frequency);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount <> 0 then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Last;

      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString <> '' then
      begin
        CheckDate := StrToDate(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 1, 10));
        CheckDate := IncMonth(CheckDate, 1);
        if Length(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString) > 10 then
        begin
          DAYS_PRIOR := Abs(StrToInt(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 11, 3)));
          DAYS_POST := Abs(StrToInt(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 14, 3)));
        end;
      end
      else
      begin
        CheckDate   := IncMonth(ctx_DataAccess.CUSTOM_VIEW['scheduled_check_date'], 1);
        Result := True;
      end;


      PeriodBegin := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_begin_date'], 1);
      PeriodEnd := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_end_date'], 1);


      CallIn := CheckDate;
      ReplaceTime(Callin, ctx_DataAccess.CUSTOM_VIEW['scheduled_call_in_date']);

      Delivery := CallIn;
      ReplaceTime(Delivery, ctx_DataAccess.CUSTOM_VIEW['scheduled_delivery_date']);


      if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 1 then
      begin
        ctx_DataAccess.CUSTOM_VIEW.Prior;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString <> '' then
        begin
          CheckDate2 := StrToDate(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 1, 10));
          CheckDate2 := IncMonth(CheckDate2, 1);
          if Length(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString) > 10 then
          begin
          DAYS_PRIOR2 := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 11, 3))));
          DAYS_POST2 := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 14, 3))));
         end;
        end
        else
        begin
          CheckDate2   := IncMonth(ctx_DataAccess.CUSTOM_VIEW['scheduled_check_date'], 1);
          Result := True;
        end;

        PeriodBegin2 := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_begin_date'], 1);
        PeriodEnd2 := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_end_date'], 1);


        CallIn2 := CheckDate2;
        ReplaceTime(Callin2, ctx_DataAccess.CUSTOM_VIEW['scheduled_call_in_date']);

        Delivery2 := CheckDate2;
        ReplaceTime(Delivery2, ctx_DataAccess.CUSTOM_VIEW['scheduled_delivery_date']);
      end;
    end
    else
    begin
      CheckDate    := EncodeDate(YearOf(Today), 1, 1);
      PeriodBegin  := CheckDate;
      PeriodEnd    := CheckDate;
      CheckDate2   := CheckDate;
      PeriodBegin2 := CheckDate;
      PeriodEnd2   := CheckDate;
      Result := True;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  function GetMonthlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate;
                                      var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime;
                                      var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;
  begin
    Result := False;
    DAYS_PRIOR := 0;
    DAYS_POST := 0;
    with TExecDSWrapper.Create('SelectLastScheduledFrequencyCheckDate') do
    begin
      SetParam('CoNbr', CoNbr);
      SetParam('Freq', Frequency);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount <> 0 then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Last;

      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString <> '' then
      begin
        CheckDate := StrToDate(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 1, 10));
        CheckDate := IncMonth(CheckDate, 1);
        if Length(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString) > 10 then
        begin
          DAYS_PRIOR := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 11, 3))));
          DAYS_POST := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 14, 3))));
        end;
      end
      else
      begin
        CheckDate   := IncMonth(ctx_DataAccess.CUSTOM_VIEW['scheduled_check_date'], 1);
        Result := True;
      end;

      PeriodBegin := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_begin_date'], 1);
      PeriodEnd := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_end_date'], 1);


      CallIn := CheckDate;
      ReplaceTime(CallIn, ctx_DataAccess.CUSTOM_VIEW['scheduled_call_in_date']);

      Delivery := CallIn;
      ReplaceTime(Delivery, ctx_DataAccess.CUSTOM_VIEW['scheduled_delivery_date']);
    end
    else
    begin
      CheckDate   :=  EncodeDate(YearOf(Today), 1, 1);
      PeriodBegin :=  CheckDate;
      PeriodEnd   :=  CheckDate;
      Result := True;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  function GetQuarterlyDefaults(CoNbr: Integer; Frequency: String; var CheckDate: TDate;
                                      var PeriodBegin: TDate; var PeriodEnd: TDate; var CallIn: TDateTime;
                                      var Delivery: TDateTime; var DAYS_PRIOR: Integer; var DAYS_POST: Integer): Boolean;
  begin
    Result := False;
    DAYS_PRIOR := 0;
    DAYS_POST := 0;
    with TExecDSWrapper.Create('SelectLastScheduledFrequencyCheckDate') do
    begin
      SetParam('CoNbr', CoNbr);
      SetParam('Freq', Frequency);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount <> 0 then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Last;

      if ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString <> '' then
      begin
        CheckDate := StrToDate(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 1, 10));
        CheckDate := IncMonth(CheckDate, 1);
        if Length(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString) > 10 then
        begin
          DAYS_PRIOR := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 11, 3))));
          DAYS_POST := Abs(StrToInt(Trim(Copy(ctx_DataAccess.CUSTOM_VIEW.FieldByName('filler').AsString, 14, 3))));
        end;
      end
      else
      begin
        CheckDate   := IncMonth(ctx_DataAccess.CUSTOM_VIEW['scheduled_check_date'], 3);
        Result := True;
      end;

      PeriodBegin := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_begin_date'], 3);
      PeriodEnd := IncMonth(ctx_DataAccess.CUSTOM_VIEW['period_end_date'], 3);

      CallIn := CheckDate;
      ReplaceTime(CallIn, ctx_DataAccess.CUSTOM_VIEW['scheduled_call_in_date']);

      Delivery := CallIn;
      ReplaceTime(Delivery, ctx_DataAccess.CUSTOM_VIEW['scheduled_delivery_date']);
    end
    else
    begin
      CheckDate   :=  EncodeDate(YearOf(Today), 1, 1);
      PeriodBegin :=  CheckDate;
      PeriodEnd   :=  CheckDate;
      Result := True;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  procedure AddToCalendar(DS, Holiday: TEvBasicClientDataSet; var WCrossed: Boolean; var BCrossed: Boolean; var SCrossed: Boolean; var MCrossed: Boolean; var QCrossed: Boolean);
  var
    CoNbr: Integer;
    EvolutionStartDate: String;
  begin

    CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
    EvolutionStartDate := '01/01/1900';

    DS.First;
    while not DS.Eof do
    begin
      if Trunc(StrToDateTime(DS['CHECK_DATE'])) >= Trunc(StrToDateTime(EvolutionStartDate)) then
      begin
        if not DM_PAYROLL.PR_SCHEDULED_EVENT.Locate('SCHEDULED_CHECK_DATE', DS['CHECK_DATE'], []) then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT.Insert;
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('CO_NBR').AsInteger := CoNbr;
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('EVENT_DATE').AsDateTime := 0;
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := DS['CALL_IN_DATE'];
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := DS['DELIVERY_DATE'];
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime := DS['CHECK_DATE'];
          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('STATUS').AsString := 'N';

          DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('FILLER').AsString := FormatDateTime('mm/dd/yyyy', DS['LAST_REAL_CHECK_DATE'])+PadStringLeft(IntToStr(DS['DAYS_PRIOR']), ' ', 3)+PadStringLeft(IntToStr(DS['DAYS_POST']), ' ', 3);

          DM_PAYROLL.PR_SCHEDULED_EVENT.Post;
        end;

        if not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Locate('PR_SCHEDULED_EVENT_NBR;FREQUENCY', VarArrayOf([DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_SCHEDULED_EVENT_NBR').Value, DS['PAY_FREQUENCY']]), []) then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Insert;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PR_SCHEDULED_EVENT_NBR').AsInteger := DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_SCHEDULED_EVENT_NBR').AsInteger;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsDateTime := DS['PERIOD_BEGIN_DATE'];
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_END_DATE').AsDateTime := DS['PERIOD_END_DATE'];

          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime := DS['CALL_IN_DATE'];
          if DS['DAYS_PRIOR'] <> 0 then
            if CompareDate(DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime, DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime) = 0 then
              DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime := AdjustDate(Holiday, DS['CALL_IN_DATE'], -1, dtEqual);

          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('FREQUENCY').AsString := DS['PAY_FREQUENCY'];
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Post;
        end;
        if CheckForCrossingYears(DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsDateTime, StrToDate(DS['PERIOD_END_DATE']), StrToDate(DS['PERIOD_END_DATE'])) then
        begin
          if DS['PAY_FREQUENCY'] = CO_FREQ_WEEKLY then WCrossed := True;
          if DS['PAY_FREQUENCY'] = CO_FREQ_BWEEKLY then BCrossed := True;
          if DS['PAY_FREQUENCY'] = CO_FREQ_SMONTHLY then SCrossed := True;
          if DS['PAY_FREQUENCY'] = CO_FREQ_MONTHLY then MCrossed := True;
          if DS['PAY_FREQUENCY'] = CO_FREQ_QUARTERLY then QCrossed := True;
        end;
      end;
      DS.Next;
    end;
  end;

  function GetDefaultsFromTable(ACompany: Integer; AFrequency: String; var CalendarDefaults: TCalendarDefaults): Boolean;
  begin

    Result := False;

    DM_COMPANY.CO_CALENDAR_DEFAULTS.DataRequired('CO_NBR = ' + QuotedStr(IntToStr(ACompany)));

    if DM_COMPANY.CO_CALENDAR_DEFAULTS.Locate('CO_NBR;FREQUENCY', VarArrayOf([ACompany, AFrequency]), []) then
    begin
      Result := True;
      CalendarDefaults.CO_NBR := DM_COMPANY.CO_CALENDAR_DEFAULTS.CO_NBR.AsInteger;
      CalendarDefaults.NUMBER_OF_MONTHS := DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_MONTHS.AsInteger;
      CalendarDefaults.FREQUENCY := DM_COMPANY.CO_CALENDAR_DEFAULTS.FREQUENCY.AsString;
      CalendarDefaults.FILLER := DM_COMPANY.CO_CALENDAR_DEFAULTS.FILLER.AsString;
      CalendarDefaults.MOVE_CHECK_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_CHECK_DATE.AsInteger;
      CalendarDefaults.MOVE_CALLIN_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_CALLIN_DATE.AsInteger;
      CalendarDefaults.MOVE_DELIVERY_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_DELIVERY_DATE.AsInteger;
      CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.LAST_REAL_SCHEDULED_CHECK_DATE.AsDateTime;
      CalendarDefaults.CALL_IN_TIME := DM_COMPANY.CO_CALENDAR_DEFAULTS.CALL_IN_TIME.AsDateTime;
      CalendarDefaults.DELIVERY_TIME := DM_COMPANY.CO_CALENDAR_DEFAULTS.DELIVERY_TIME.AsDateTime;
      CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_PRIOR.AsInteger;
      CalendarDefaults.NUMBER_OF_DAYS_AFTER := DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_AFTER.AsInteger;
      CalendarDefaults.PERIOD_BEGIN_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_BEGIN_DATE.AsDateTime;
      CalendarDefaults.PERIOD_END_DATE := DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_END_DATE.AsDateTime;

      if AFrequency = CO_FREQ_SMONTHLY then
      begin
        CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.LAST_REAL_SCHED_CHECK_DATE2.AsDateTime;
        CalendarDefaults.CALL_IN_TIME2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.CALL_IN_TIME2.AsDateTime;
        CalendarDefaults.DELIVERY_TIME2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.DELIVERY_TIME2.AsDateTime;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_PRIOR2.AsInteger;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_AFTER2.AsInteger;
        CalendarDefaults.PERIOD_BEGIN_DATE2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_BEGIN_DATE2.AsDateTime;
        CalendarDefaults.PERIOD_END_DATE2 := DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_END_DATE2.AsDateTime;
      end;
    end;
  end;

  procedure UpdateDefaultsByFrequency(Defaults: TCalendarDefaults);
  begin
    if DM_COMPANY.CO_CALENDAR_DEFAULTS.Locate('CO_NBR;FREQUENCY', VarArrayOf([Defaults.CO_NBR, Defaults.FREQUENCY]), []) then
      DM_COMPANY.CO_CALENDAR_DEFAULTS.Edit
    else
      DM_COMPANY.CO_CALENDAR_DEFAULTS.Insert;

    DM_COMPANY.CO_CALENDAR_DEFAULTS.CO_NBR.AsInteger := Defaults.CO_NBR;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_MONTHS.AsInteger := Defaults.NUMBER_OF_MONTHS;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.FREQUENCY.AsString := Defaults.FREQUENCY;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.FILLER.AsString := Defaults.FILLER;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_CHECK_DATE.AsInteger := Defaults.MOVE_CHECK_DATE;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_CALLIN_DATE.AsInteger := Defaults.MOVE_CALLIN_DATE;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.MOVE_DELIVERY_DATE.AsInteger := Defaults.MOVE_DELIVERY_DATE;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.LAST_REAL_SCHEDULED_CHECK_DATE.AsDateTime := Defaults.LAST_REAL_SCHEDULED_CHECK_DATE;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.CALL_IN_TIME.AsDateTime := Defaults.CALL_IN_TIME;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.DELIVERY_TIME.AsDateTime := Defaults.DELIVERY_TIME;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_PRIOR.AsInteger := Defaults.NUMBER_OF_DAYS_PRIOR;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_AFTER.AsInteger := Defaults.NUMBER_OF_DAYS_AFTER;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_BEGIN_DATE.AsDateTime := Defaults.PERIOD_BEGIN_DATE;
    DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_END_DATE.AsDateTime := Defaults.PERIOD_END_DATE;

    if Defaults.FREQUENCY = CO_FREQ_SMONTHLY then
    begin
      DM_COMPANY.CO_CALENDAR_DEFAULTS.LAST_REAL_SCHED_CHECK_DATE2.AsDateTime := Defaults.LAST_REAL_SCHED_CHECK_DATE2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.CALL_IN_TIME2.AsDateTime := Defaults.CALL_IN_TIME2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.DELIVERY_TIME2.AsDateTime := Defaults.DELIVERY_TIME2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_PRIOR2.AsInteger := Defaults.NUMBER_OF_DAYS_PRIOR2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.NUMBER_OF_DAYS_AFTER2.AsInteger := Defaults.NUMBER_OF_DAYS_AFTER2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_BEGIN_DATE2.AsDateTime := Defaults.PERIOD_BEGIN_DATE2;
      DM_COMPANY.CO_CALENDAR_DEFAULTS.PERIOD_END_DATE2.AsDateTime := Defaults.PERIOD_END_DATE2;
    end;

    DM_COMPANY.CO_CALENDAR_DEFAULTS.Post;
  end;

  function ReturnCBIndex(AValue: Integer): Integer;
  begin
    case AValue of
       1: Result := 0;
      -1: Result := 1;
    else
      Result := 2;
    end;
  end;
end.
