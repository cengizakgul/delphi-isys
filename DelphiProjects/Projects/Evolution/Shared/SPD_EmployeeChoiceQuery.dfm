inherited EmployeeChoiceQuery: TEmployeeChoiceQuery
  Left = 539
  Width = 416
  Height = 425
  Caption = ''
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 346
    Width = 400
    DesignSize = (
      400
      41)
    inherited btnYes: TevBitBtn
      Left = 162
      Width = 113
    end
    inherited btnNo: TevBitBtn
      Left = 288
      Width = 113
    end
    inherited btnAll: TevBitBtn
      Left = 32
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 400
    Height = 311
    Selected.Strings = (
      'CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F'
      'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
      'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
      'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F')
    IniAttributes.SectionName = 'TEmployeeChoiceQuery\dgChoiceList'
  end
  inherited pnlEffectiveDate: TevPanel
    Top = 311
    Width = 400
  end
  inherited dsChoiceList: TevDataSource
    Left = 192
    Top = 40
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 208
    Top = 168
  end
end
