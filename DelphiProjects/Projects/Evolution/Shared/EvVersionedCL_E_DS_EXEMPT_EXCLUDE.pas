unit EvVersionedCL_E_DS_EXEMPT_EXCLUDE;

interface

uses
  Forms, Classes, Controls, StdCtrls, ISBasicClasses, EvUIComponents, EvStreamUtils,
  Variants, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SysUtils,
  evDataSet, EvCommonInterfaces, EvDataAccessComponents, Grids, SDataStructure,
  Wwdbigrd, Wwdbgrid, Wwdatsrc, Buttons, SDDClasses, SDDStreamClasses, rwCustomDataDictionary,
  isBasicUtils, isTypes, ExtCtrls, ComCtrls, wwdbdatetimepicker, Graphics, EvUtils,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, evUIUtils, Dialogs, isBaseClasses, isThreadManager,
  evContext, DateUtils, evConsts, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, isDataSet,
  isUISpeedButton, isUIwwDBDateTimePicker, isUIFashionPanel, evClientDataSet,
  EvVersionedFieldBaseFrm, isUIwwDBEdit, wwdblook, isUIwwDBLookupCombo,
  SDataDictsystem, SDataDictclient, isUIwwDBComboBox, DBCtrls;

type
  TevVersionedCL_E_DS_EXEMPT_EXCLUDE = class(TevVersionedFieldBase)
    DM_CLIENT: TDM_CLIENT;
    drgpExemptExclude: TevDBRadioGroup;
    procedure FormShow(Sender: TObject);
  end;

implementation

{$R *.dfm}


{ TevVersionedCL_E_DS_EXEMPT_EXCLUDE }

procedure TevVersionedCL_E_DS_EXEMPT_EXCLUDE.FormShow(Sender: TObject);
var
  Found: Boolean;
  Code, DefaultValue, SetString, SyExemptName, FieldName: String;
  i: Integer;

  function ToState(const s: string): string;
  var
    i: Integer;
  begin
    Result := s;
    for i := 1 to Length(Result) do
      if Result[i] = GROUP_BOX_EXEMPT then
        Result[i] := GROUP_BOX_EXE
      else
        if Result[i] = GROUP_BOX_EXCLUDE then
          Result[i] := GROUP_BOX_EXC;
  end;

  function FromState(const s: string): string;
  var
    i: Integer;
  begin
    Result := s;
    for i := 1 to Length(Result) do
      if Result[i] = GROUP_BOX_EXE then
        Result[i] := GROUP_BOX_EXEMPT
      else
    if Result[i] = GROUP_BOX_EXC then
      Result[i] := GROUP_BOX_EXCLUDE;
  end;

begin
  inherited;

  FieldName := Fields.ParamName(0);

  if Table = 'CL_E_DS' then
  begin
    if FieldName = 'EE_EXEMPT_EXCLUDE_FEDERAL' then
      SyExemptName := 'EXEMPT_FEDERAL'
    else if FieldName = 'EE_EXEMPT_EXCLUDE_OASDI' then
      SyExemptName := 'EXEMPT_EMPLOYEE_OASDI'
    else if FieldName = 'ER_EXEMPT_EXCLUDE_OASDI' then
      SyExemptName := 'EXEMPT_EMPLOYER_OASDI'
    else if FieldName = 'EE_EXEMPT_EXCLUDE_MEDICARE' then
      SyExemptName := 'EXEMPT_EMPLOYEE_MEDICARE'
    else if FieldName = 'EE_EXEMPT_EXCLUDE_EIC' then
      SyExemptName := 'EXEMPT_EMPLOYEE_EIC'
    else if FieldName = 'ER_EXEMPT_EXCLUDE_MEDICARE' then
      SyExemptName := 'EXEMPT_EMPLOYER_MEDICARE'
    else if FieldName = 'ER_EXEMPT_EXCLUDE_FUI' then
      SyExemptName := 'EXEMPT_FUI';

    DM_SYSTEM.SY_FED_EXEMPTIONS.DataRequired('ALL');
    Code := DM_CLIENT.CL_E_DS.E_D_CODE_TYPE.AsString;
    Found := DM_SYSTEM.SY_FED_EXEMPTIONS.Locate('E_D_CODE_TYPE', Code, []);
    DefaultValue := DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_FED_EXEMPTIONS[SyExemptName]);
    SetString := DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DefaultValue);
    drgpExemptExclude.Caption := Iff(Fields[0].Value['Required'], '~', '') + DM_CLIENT.CL_E_DS.FieldByName(FieldName).DisplayName;
    drgpExemptExclude.DataField := FieldName;

    Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');

    if Length(SetString) = 1 then
      DefaultValue := SetString;

    for i := 0 to drgpExemptExclude.ControlCount - 1 do
    begin
      Assert(Length(drgpExemptExclude.Values[i]) <= 1, 'DBRadioButtonValues are supposed to be a single character');
      drgpExemptExclude.Controls[i].Enabled := Pos(drgpExemptExclude.Values[i], SetString) > 0;
      if (drgpExemptExclude.Values[i] = DefaultValue) and drgpExemptExclude.Controls[i].Enabled then
      begin
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style + [fsBold];
      end
      else
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style - [fsBold];
    end;
  end
  else
  if Table = 'CL_E_D_STATE_EXMPT_EXCLD' then
  begin

    if FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_STATE' then
      SyExemptName := 'EXEMPT_STATE'
    else if FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_SDI' then
      SyExemptName := 'EXEMPT_EMPLOYEE_SDI'
    else if FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_SUI' then
      SyExemptName := 'EXEMPT_EMPLOYEE_SUI'
    else if FieldName = 'EMPLOYER_EXEMPT_EXCLUDE_SDI' then
      SyExemptName := 'EXEMPT_EMPLOYER_SDI'
    else if FieldName = 'EMPLOYER_EXEMPT_EXCLUDE_SUI' then
      SyExemptName := 'EXEMPT_EMPLOYEE_SUI';

    drgpExemptExclude.Items.Clear;
    drgpExemptExclude.Values.Clear;

    drgpExemptExclude.Items.Add('Exempt');
    drgpExemptExclude.Items.Add('Block');
    drgpExemptExclude.Items.Add('Include');

    drgpExemptExclude.Values.Add('X');
    drgpExemptExclude.Values.Add('E');
    drgpExemptExclude.Values.Add('I');

    DM_SYSTEM.SY_STATE_EXEMPTIONS.DataRequired('ALL');
    Code := DM_CLIENT.CL_E_DS.E_D_CODE_TYPE.AsString;
    Found := DM_SYSTEM.SY_STATE_EXEMPTIONS.Locate('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD['SY_STATES_NBR'], Code]), []);

    if SyExemptName = 'EXEMPT_STATE' then
      DefaultValue := DBRadioGroup_ED_GetRightDefault(Code, Found, FromState(DM_SYSTEM.SY_STATE_EXEMPTIONS[SyExemptName]))
    else
      DefaultValue := DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_STATE_EXEMPTIONS[SyExemptName]);

    SetString := ToState(DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DefaultValue));
    drgpExemptExclude.Caption := Iff(Fields[0].Value['Required'], '~', '') + DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD.FieldByName(FieldName).DisplayName;

    drgpExemptExclude.DataField := FieldName;

    Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');

    if Length(SetString) = 1 then
      DefaultValue := SetString;

    for i := 0 to drgpExemptExclude.ControlCount - 1 do
    begin
      Assert(Length(drgpExemptExclude.Values[i]) <= 1, 'DBRadioButtonValues are supposed to be a single character');
      drgpExemptExclude.Controls[i].Enabled := Pos(drgpExemptExclude.Values[i], SetString) > 0;
      if (drgpExemptExclude.Values[i] = DefaultValue) and drgpExemptExclude.Controls[i].Enabled then
      begin
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style + [fsBold];
      end
      else
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style - [fsBold];
    end;

  end
  else if Table = 'CL_E_D_LOCAL_EXMPT_EXCLD' then
  begin

    DM_SYSTEM.SY_LOCAL_EXEMPTIONS.DataRequired('ALL');
    Code := DM_CLIENT.CL_E_DS.E_D_CODE_TYPE.AsString;
    Found := DM_SYSTEM.SY_LOCAL_EXEMPTIONS.Locate('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD['SY_LOCALS_NBR'], Code]), []);
    DefaultValue := DBRadioGroup_ED_GetRightDefault(Code, Found, DM_SYSTEM.SY_LOCAL_EXEMPTIONS['EXEMPT']);
    SetString := DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DefaultValue, DM_SYSTEM.SY_LOCALS['SY_STATES_NBR']);
    drgpExemptExclude.Caption := Iff(Fields[0].Value['Required'], '~', '') + DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD.FieldByName(FieldName).DisplayName;

    drgpExemptExclude.DataField := FieldName;

    Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');

    if Length(SetString) = 1 then
      DefaultValue := SetString;

    for i := 0 to drgpExemptExclude.ControlCount - 1 do
    begin
      Assert(Length(drgpExemptExclude.Values[i]) <= 1, 'DBRadioButtonValues are supposed to be a single character');
      drgpExemptExclude.Controls[i].Enabled := Pos(drgpExemptExclude.Values[i], SetString) > 0;
      if (drgpExemptExclude.Values[i] = DefaultValue) and drgpExemptExclude.Controls[i].Enabled then
      begin
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style + [fsBold];
      end
      else
        TRadioButton(drgpExemptExclude.Controls[i]).Font.Style := TRadioButton(drgpExemptExclude.Controls[i]).Font.Style - [fsBold];
    end;
  end;

end;

end.

