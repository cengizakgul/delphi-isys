// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SBUserChoiceQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_SafeChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure, 
  SPD_ChoiceFromListQueryEx, ActnList, SDDClasses, SDataDictclient,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, ISBasicClasses, EvClientDataSet, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, ComCtrls, SDataDictbureau;

type
  TSBUserChoiceQuery = class(TSafeChoiceFromListQuery)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilter( flt: string );
  end;


implementation

uses
  evutils;
{$R *.DFM}

{ TSBUserChoiceQuery }

procedure TSBUserChoiceQuery.SetFilter(flt: string);
begin
  dgChoiceList.DataSource := nil;
  cdsChoiceList.Data := DM_SERVICE_BUREAU.SB_USER.Data;
  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
  dgChoiceList.DataSource := dsChoiceList;
end;

end.
