object DM_MISC_LIAB_CORRECT: TDM_MISC_LIAB_CORRECT
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdTaxes: TevClientDataSet
    AfterInsert = cdTaxesAfterInsert
    Left = 64
    Top = 32
    object cdTaxesNbr2: TIntegerField
      FieldName = 'Nbr'
    end
    object cdTaxesTaxType: TStringField
      FieldName = 'TaxType'
      Size = 1
    end
    object cdTaxesNbr: TIntegerField
      FieldName = 'TaxNbr'
    end
    object cdTaxesTaxDesc: TStringField
      DisplayWidth = 15
      FieldName = 'TaxDesc'
    end
    object cdTaxesTotalCalc: TCurrencyField
      FieldName = 'TotalCalc'
    end
    object cdTaxesTotalReal: TCurrencyField
      FieldName = 'TotalReal'
    end
    object cdTaxesTotalDiff: TCurrencyField
      FieldName = 'TotalDiff'
    end
    object cdTaxesDo: TBooleanField
      FieldName = 'ToDo'
    end
    object cdTaxesDueDate: TDateTimeField
      FieldName = 'DueDate'
    end
    object cdTaxesAdjType: TStringField
      FieldName = 'AdjType'
      Size = 1
    end
    object cdTaxesState: TStringField
      FieldName = 'State'
      Size = 2
    end
  end
end
