unit dmAdapterFrm;

interface

uses
  SysUtils, Classes, Forms, Controls, DB, rwCallback, rwEngineTypes, StdCtrls, Windows, Graphics, EvStreamUtils,
  DBTables, IniFiles, DBXpress, SqlExpr, FMTBcd, ADODB, dmDBAccessFrm;

type
  TDBModulesList = array of TdmDBAccess;


  TdmAdapter = class(TDataModule)
    rwAppAdapter: TrwAppAdapter;
    procedure rwAppAdapterDBCacheCleaning(
      const DataDictExternalTableName: String; var Accept: Boolean);
    procedure rwEngineCallBackEndProgressBar;
    procedure rwAppAdapterGetDDFieldInfo(const ATable, AField: String;
      out FieldInfo: TrwDDFieldInfoRec);
    procedure rwAppAdapterGetDDFields(const ATable: String;
      out Fields: String);
    procedure rwAppAdapterGetDDTables(out Tables: String);
    procedure rwAppAdapterGetTempDir(out TempDir: String);
    procedure rwAppAdapterLoadDataDictionary(Data: TStream);
    procedure rwAppAdapterLoadLibraryComponents(Data: TStream);
    procedure rwAppAdapterLoadLibraryFunctions(Data: TStream);
    procedure rwAppAdapterLoadQBTemplates(Data: TStream);
    procedure rwAppAdapterLoadSQLConstants(Data: TStream);
    procedure rwEngineCallBackStartProgressBar(const Title: String;
      MaxValue: Integer);
    procedure rwAppAdapterTableDataRequest(
      const CachedTable: IrwCachedTable; const sbTable: IrwSBTable;
      const Params: array of TrwParamRec);
    procedure rwAppAdapterUnloadDataDictionary(Data: TStream);
    procedure rwAppAdapterUnloadLibraryComponents(Data: TStream);
    procedure rwAppAdapterUnloadLibraryFunctions(Data: TStream);
    procedure rwAppAdapterUnloadQBTemplates(Data: TStream);
    procedure rwAppAdapterUnloadSQLConstants(Data: TStream);
    procedure rwDesignerCallBackGetSecurityDescriptor(
      var ASecurityDescriptor: TrwDesignerSecurityRec);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    procedure ReadConfiguration;
    function  EncodedTableName(const ATable: String; ADBModule: TdmDBAccess): String;
    function  DecodedTableName(const ATable: String): String;
    function  GetDBModuleForTable(const ATable: String): TdmDBAccess;
    function  GetDBModules: TDBModulesList;
    function  ConvertDataType(const ADataType: TFieldType): TrwDDType;
    function  GetAppPath: String;
  end;

var
  dmAdapter: TdmAdapter;

  procedure LoadFileIntoStream(const AFileName: String; const AStream: TStream);
  procedure StoreStreamIntoFile(const AFileName: String; const AStream: TStream);


implementation

{$R *.dfm}

function ReportWriterAppAdapter: IrwAppAdapter; stdcall;
begin
  Result := dmAdapter.rwAppAdapter;
end;

exports ReportWriterAppAdapter;


procedure LoadFileIntoStream(const AFileName: String;
  const AStream: TStream);
var
  FS: TEvFileStream;
begin
  AStream.Size := 0;
  AStream.Position := 0;

  if FileExists(AFileName) then
  begin
    FS := TEvFileStream.Create(AFileName, fmOpenRead);
    try
      AStream.CopyFrom(FS, FS.Size);
    finally
      FS.Free
    end;
  end;
end;

procedure StoreStreamIntoFile(const AFileName: String;
  const AStream: TStream);
var
  FS: TEvFileStream;
begin
  FS := TEvFileStream.Create(AFileName, fmCreate);
  try
    AStream.Position := 0;
    FS.CopyFrom(AStream, AStream.Size);
  finally
    FS.Free
  end;
end;

function TdmAdapter.ConvertDataType(const ADataType: TFieldType): TrwDDType;
begin
  case ADataType of
    ftString, ftWideString:         Result := ddtString;
    ftSmallint, ftInteger,
    ftWord, ftAutoInc:              Result := ddtInteger;
    ftFloat, ftBCD:                 Result := ddtFloat;
    ftCurrency:                     Result := ddtCurrency;
    ftBoolean:                      Result := ddtBoolean;
    ftDate, ftTime, ftDateTime:     Result := ddtDateTime;
    ftBlob:                         Result := ddtBLOB;
    ftMemo:                         Result := ddtMemo;
    ftGraphic:                      Result := ddtGraphic;
    ftArray:                        Result := ddtArray;
  else
    Result := ddtUnknown;
  end;
end;

function TdmAdapter.GetAppPath: String;
begin
  Result := ExtractFilePath(Application.ExeName);
end;

procedure TdmAdapter.rwAppAdapterDBCacheCleaning(
  const DataDictExternalTableName: String; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TdmAdapter.rwEngineCallBackEndProgressBar;
begin
  Screen.Cursor := crDefault;
end;

procedure TdmAdapter.rwAppAdapterGetDDFieldInfo(const ATable, AField: String; out FieldInfo: TrwDDFieldInfoRec);
var
  DB: TdmDBAccess;
begin
  DB := GetDBModuleForTable(ATable);

  DB.tblInfo.TableName := DecodedTableName(ATable);
  DB.tblInfo.Open;
  try
    FieldInfo.DisplayName := DB.tblInfo.FieldByName(AField).DisplayName;
    FieldInfo.FieldType := ConvertDataType(DB.tblInfo.FieldByName(AField).DataType);
    FieldInfo.Size := DB.tblInfo.FieldByName(AField).Size;
    FieldInfo.FieldValues := '';
  finally
    DB.tblInfo.Close;
  end;
end;

procedure TdmAdapter.rwAppAdapterGetDDFields(const ATable: String; out Fields: String);
var
  L: TStringList;
  i: Integer;
  DB: TdmDBAccess;
begin
  DB := GetDBModuleForTable(ATable);

  Fields := '';
  L := TStringList.Create;
  try
    DB.Connection.GetFieldNames(DecodedTableName(ATable), L);

    for i := 0 to L.Count - 1 do
    begin
      if i > 0 then
        Fields := Fields + ',';
      Fields := Fields + L[i]
    end;

  finally
    L.Free;
  end;
end;

procedure TdmAdapter.rwAppAdapterGetDDTables(out Tables: String);
var
  L: TStringList;
  i, j: Integer;
  DB: TdmDBAccess;
  DBs: TDBModulesList;
begin
  DBs := GetDBModules;

  Tables := '';
  for j := Low(DBs) to High(DBs) do
  begin
    DB := DBs[j];
    L := TStringList.Create;
    try
      DB.Connection.GetTableNames(L);
      for i := 0 to L.Count - 1 do
      begin
        if Tables <> '' then
          Tables := Tables + ',';
        Tables := Tables + EncodedTableName(L[i], DB);
      end;
    finally
      L.Free;
    end;
  end;  
end;

procedure TdmAdapter.rwAppAdapterGetTempDir(out TempDir: String);
begin
  TempDir := GetAppPath;
end;

procedure TdmAdapter.rwAppAdapterLoadDataDictionary(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'DataDictionary.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadLibraryComponents(
  Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'LibComponents.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadLibraryFunctions(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'LibFunctions.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadQBTemplates(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'QBTemplates.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterLoadSQLConstants(Data: TStream);
begin
  LoadFileIntoStream(GetAppPath + 'QBConstants.rwe', Data);
end;

procedure TdmAdapter.rwEngineCallBackStartProgressBar(const Title: String; MaxValue: Integer);
begin
  Screen.Cursor := crHourGlass;
end;

procedure TdmAdapter.rwAppAdapterTableDataRequest(const CachedTable: IrwCachedTable; const sbTable: IrwSBTable; const Params: array of TrwParamRec);
var
  FldVals: array of Variant;
  i: Integer;
  h, flt: String;
  Flds: TrwStrList;
  DB: TdmDBAccess;
begin
  DB := GetDBModuleForTable(CachedTable.ddTable.GetTableExternalName);
  
  with DB.Query do
  begin
    Flds := CachedTable.GetExternalFields;
    flt := CachedTable.GetFilter;

    SetLength(FldVals, High(Flds) - Low(Flds) + 1);
    h := '';
    for i := Low(FldVals) to High(FldVals) do
    begin
      if i > 0 then
        h := h + ', ';
      h := h + Flds[i];
    end;
    h := 'Select ' + h + ' From ' + DecodedTableName(CachedTable.ddTable.GetTableExternalName);
    if flt <> '' then
      h := h + ' where ' + flt;
    SQL.Text := h;

    Open;
    try
      while not Eof do
      begin
        for i := Low(FldVals) to High(FldVals) do
          FldVals[i] := FieldByName(Flds[i]).Value;
        sbTable.AppendRecord(FldVals);
        Next;
      end;
    finally
      Close;
      SQL.Text := '';
    end;
  end;
end;

procedure TdmAdapter.rwAppAdapterUnloadDataDictionary(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'DataDictionary.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadLibraryComponents(
  Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'LibComponents.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadLibraryFunctions(
  Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'LibFunctions.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadQBTemplates(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'QBTemplates.rwe', Data);
end;

procedure TdmAdapter.rwAppAdapterUnloadSQLConstants(Data: TStream);
begin
  StoreStreamIntoFile(GetAppPath + 'QBConstants.rwe', Data);
end;

procedure TdmAdapter.rwDesignerCallBackGetSecurityDescriptor(var ASecurityDescriptor: TrwDesignerSecurityRec);
begin
  ASecurityDescriptor.DataDictionary.Modify := True;
  ASecurityDescriptor.QBTemplates.Modify := True;
  ASecurityDescriptor.QBConstants.Modify := True;
  ASecurityDescriptor.LibComponents.Modify := True;
  ASecurityDescriptor.LibFunctions.Modify := True;
end;


function TdmAdapter.GetDBModules: TDBModulesList;
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TdmDBAccess then
    begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := TdmDBAccess(Components[i]);
    end;
end;

procedure TdmAdapter.DataModuleCreate(Sender: TObject);
begin
  ReadConfiguration;
end;

procedure TdmAdapter.ReadConfiguration;
var
  Ini: TIniFile;
  ODBCList: TStringList;
  i: Integer;
  dmDB: TdmDBAccess;
  h: String;
begin
  ODBCList := TStringList.Create;
  Ini := TIniFile.Create(GetAppPath + 'rwODBCAdapter.ini');
  try
    Ini.ReadSection('ODBC', ODBCList);

    for i := 0 to ODBCList.Count - 1 do
    begin
      h := Ini.ReadString('ODBC', ODBCList[i], '');
      if h <> '' then
      begin
        dmDB := TdmDBAccess.Create(Self);
        dmDB.Name := 'DB' + ODBCList[i];
        dmDB.ODBCInfo := h;
      end;
    end;

  finally
    Ini.Free;
    ODBCList.Free;
  end;
end;

function TdmAdapter.GetDBModuleForTable(const ATable: String): TdmDBAccess;
var
  i: Integer;
begin
  i := Pos('_', ATable);
  Assert(i  > 0);
  Result := FindComponent('DB' + Copy(ATable, 1, i - 1)) as TdmDBAccess;
end;

function TdmAdapter.DecodedTableName(const ATable: String): String;
var
  i: Integer;
begin
  Result := ATable;
  i := Pos('_', Result);
  Assert(i  > 0);
  Delete(Result, 1, i);
end;

function TdmAdapter.EncodedTableName(const ATable: String; ADBModule: TdmDBAccess): String;
var
  h: String;
begin
  h := ADBModule.Name;
  Delete(h, 1, 2);
  Result := h + '_' + ATable;
end;

procedure TdmAdapter.DataModuleDestroy(Sender: TObject);
var
  DBs: TDBModulesList;
  i: Integer;
begin
  DBs := GetDBModules;
  for i := Low(DBs) to High(DBs) do
  begin
    DBs[i].Free;
  end;
end;

initialization
  dmAdapter := TdmAdapter.Create(Application);

finalization
  FreeAndNil(dmAdapter);

end.

