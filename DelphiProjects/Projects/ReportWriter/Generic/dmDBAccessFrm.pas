unit dmDBAccessFrm;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmDBAccess = class(TDataModule)
    Connection: TADOConnection;
    Query: TADOQuery;
    tblInfo: TADOTable;
    procedure DataModuleDestroy(Sender: TObject);
  private
    procedure SetODBCInfo(const AValue: String);
    function  GetODBCInfo: String;
  public
    property ODBCInfo: String read GetODBCInfo  write SetODBCInfo;
  end;

implementation

{$R *.dfm}

procedure TdmDBAccess.DataModuleDestroy(Sender: TObject);
begin
  Connection.Connected := False;
end;

function TdmDBAccess.GetODBCInfo: String;
begin
  Result := Connection.ConnectionString;
end;

procedure TdmDBAccess.SetODBCInfo(const AValue: String);
begin
  try
    Connection.ConnectionString := AValue;
    Connection.Connected := True;
  except
    on E: Exception do
      ApplicationShowException(E);
  end;
end;

end.
