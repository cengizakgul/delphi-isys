@ECHO OFF
REM Parameters: 
REM    1. Version(optional)

SET pAppVersion=%1

ECHO *Compiling

..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion%

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
XCOPY /Y /Q .\AppProjects\*.mes ..\..\Bin\ReportWriter\*.* > NUL
FOR %%a IN (..\..\Bin\ReportWriter\*.exe) DO ..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL
FOR %%a IN (..\..\Bin\ReportWriter\*.exe) DO ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
FOR %%a IN (..\..\Bin\ReportWriter\*.dll) DO ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
FOR %%a IN (..\..\Bin\ReportWriter\*.mes) DO DEL /F /Q ..\..\Bin\ReportWriter\*.mes
FOR %%a IN (..\..\Bin\ReportWriter\*.map) DO DEL /F /Q ..\..\Bin\ReportWriter\*.map

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end

