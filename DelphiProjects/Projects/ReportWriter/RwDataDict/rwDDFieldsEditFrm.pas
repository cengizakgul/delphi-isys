unit rwDDFieldsEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictParamEditFrm, StdCtrls, DataDictFieldEditFrm,
  rwCustomDataDictionary, rwDataDictionary, ExtCtrls, rwDDFieldEditFrm,
  DataDictAvailableItemsFrm, rwUtils, rwBasicUtils, rwEngineTypes;

type
  TrwDDFieldsEdit = class(TDataDictCollectionEdit)
    FieldInfo: TrwDDFieldEdit;
    procedure FieldInfoedNameExit(Sender: TObject);
    procedure btnAddItemClick(Sender: TObject);
  private
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
    procedure DeleteItem(const AItem: TCollectionItem); override;
  public
    procedure AfterConstruction; override;
    procedure OnShowItem; override;
  end;


implementation

{$R *.dfm}

uses rwEngine, DataDictTableEditorFrm;

{ TddFieldsEdit }

procedure TrwDDFieldsEdit.AfterConstruction;
begin
  inherited;
  FItemFrame := FieldInfo;
end;

function TrwDDFieldsEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictField(AItem).Name;
end;

procedure TrwDDFieldsEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TrwDDFieldsEdit.FieldInfoedNameExit(Sender: TObject);
begin
  FieldInfo.edNameExit(Sender);
  UpdateItem(SelectedItem);
end;

procedure TrwDDFieldsEdit.DeleteItem(const AItem: TCollectionItem);
begin
  inherited;
  TableEditor.ddTable.FixupReferences;
  TableEditor.RefreshTableItem(ddTITPrimaryKey);
  TableEditor.RefreshTableItem(ddForeignKeys);
end;

procedure TrwDDFieldsEdit.btnAddItemClick(Sender: TObject);
var
  F, flds, h: String;
  FldInfo: TrwDDFieldInfoRec;
begin
  flds := '';
  F := RWEngineExt.AppAdapter.GetDDFields(TrwDataDictTable(TableEditor.ddTable).ExternalName);
  while F <> '' do
  begin
    h := Trim(GetNextStrValue(F, ','));
    if not Assigned(TrwDataDictFields(DataDictionaryItem).FieldByExternalName(h)) then
    begin
      if flds <> '' then
        flds := flds + Chr(VK_RETURN);
      flds := flds + h;
    end;
  end;

  flds := ShowAvailableFields(flds, TableEditor, True, 'Available External Fields');

  while flds <> '' do
  begin
    h := Trim(GetNextStrValue(flds, Chr(VK_RETURN)));
    inherited;
    FldInfo := RWEngineExt.AppAdapter.GetDDFieldInfo(TrwDataDictTable(TableEditor.ddTable).ExternalName, h);
    if FldInfo.Name = '' then
      FldInfo.Name := h;
    TrwDataDictField(SelectedItem).Name := FldInfo.Name;
    TrwDataDictField(SelectedItem).ExternalName := h;
    TrwDataDictField(SelectedItem).DisplayName := FldInfo.DisplayName;

    TrwDataDictField(SelectedItem).FieldType := TDataDictDataType(Ord(FldInfo.FieldType));
    TrwDataDictField(SelectedItem).Size := FldInfo.Size;
    if (TrwDataDictField(SelectedItem).Size > 255) and (TrwDataDictField(SelectedItem).FieldType = ddtString) then
    begin
      TrwDataDictField(SelectedItem).FieldType := ddtMemo;
      TrwDataDictField(SelectedItem).Size := 0;
    end;

    TrwDataDictField(SelectedItem).FieldValues.Text := FldInfo.FieldValues;
    UpdateItem(SelectedItem);
  end;

  lbCollection.OnClick(nil);
end;


procedure TrwDDFieldsEdit.OnShowItem;
begin
  inherited;
  btnAddItem.Enabled := TrwDataDictTable(TableEditor.ddTable).TableType = rwtDDExternal;
  btnDeleteItem.Enabled := btnAddItem.Enabled;
end;

end.

