unit RwDataDictionary;

interface

uses Classes, SysUtils, rwCustomDataDictionary, rwBasicClasses, Variants,
     EvStreamUtils, rwEngineTypes, rwBasicUtils;

type

  TrwDataDictTable = class;
//  TrwDataDictTablesClass = class of TrwDataDictTables;
//  TrwDataDictFieldsClass = class of TrwDataDictFields;
//  TrwDataDictParamsClass = class of TrwDataDictParams;
//  TrwDataDictFieldClass = class of TrwDataDictField;
//  TrwDataDictTableClass = class of TrwDataDictTable;

  TrwDataDictionary = class(TDataDictionary, IrwDataDictionary)
  private
    FSyncLog: String;

    procedure CleanUpSysTables;

    //Interface Imlementation
    function GetTableByExternalName(const AExternalTableName: String): IrwDDTable;
    function GetTable(const ATableName: String): IrwDDTable;

  public
    property SyncLog: String read FSyncLog;

    constructor Create(AOwner: TComponent); override;
    function GetTablesClass: TDataDictTablesClass; override;
    function GetFieldsClass: TDataDictFieldsClass; override;
    function GetParamsClass: TDataDictParamsClass; override;
    function GetTableClass:  TDataDictTableClass; override;
    function GetFieldClass:  TDataDictFieldClass; override;
    function GetParamClass:  TDataDictParamClass; override;

    procedure DeleteField(const AField: TDataDictField); override;

    procedure SaveToStream(const Stream: TStream); override;
    procedure LoadFromStream(const Stream: TStream); override;

    procedure UpdateSysTablesStructure;
    procedure Update;
    procedure Initialize;
    procedure Synchronize;
  end;


  TrwDDTableType = (rwtDDUnknown, rwtDDExternal, rwtDDQuery, rwtDDComponent, rwtBuffer, rwtDDSystem);


  TrwDataDictTables = class(TDataDictTables)
    function TableByExternalName(const ATableExternalName: String): TrwDataDictTable;
  end;


  TrwDataDictTable = class(TDataDictTable, IrwDDTable)
  private
    FTableType: TrwDDTableType;
    FExternalName: String;
    FSQLText: string;
    FGroupName: string;
    FQBInfo: TMemoryStream;
    FBufferCreationCount: Integer;
    FPrepared: Boolean;
    function ExternalNameNotEmpty: Boolean;
    function SQLTextNotEmpty: Boolean;
    function GroupNameNotEmpty: Boolean;
    procedure SetTableType(const Value: TrwDDTableType);
    procedure WriteQBInfo(Stream: TStream);
    procedure ReadQBInfo(Stream: TStream);
    function  GetExternalName: String; virtual;
    procedure SetExternalName(const AValue: String); virtual;

    //Interface Imlementation
    function GetTableName: String;
    function GetTableExternalName: String;
    function GetPrimaryKey: TCommaDilimitedString;
    function GetLogicalKey: TCommaDilimitedString;
    function GetForeignKeys: TIniString;
    function GetFieldList: TCommaDilimitedString;
    function GetFieldByExternalName(const AExternalFieldName: String): IrwDDField;
    function GetField(const AFieldName: String): IrwDDField;
    function GetForeignKeysByField(const AField: String): TIniString;
    function GetForeignKeysByTable(const ATable: String): TIniString;

  protected
    procedure  DefineProperties(Filer: TFiler); override;

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure  Assign(Source: TPersistent); override;
    procedure FixupReferences; override;
    property QBInfo: TMemoryStream read FQBInfo;
    property BufferCreationCount: Integer read FBufferCreationCount write FBufferCreationCount;
    property Prepared: Boolean read FPrepared write FPrepared;  // for Component and Query type only! Used intrnally by query preparation process (TrwLQVirtualTable.Initial)

  published
    property TableType: TrwDDTableType read FTableType write SetTableType default rwtDDExternal;
    property ExternalName: String read GetExternalName write SetExternalName stored ExternalNameNotEmpty;
    property GroupName: string read FGroupName write FGroupName stored GroupNameNotEmpty;
    property SQLText: string read FSQLText write FSQLText stored SQLTextNotEmpty;
  end;


  TrwDataDictSysTable = class(TrwDataDictTable)
  private
    function  GetExternalName: String; override;
    procedure SetExternalName(const AValue: String); override;
    procedure CreateContent; virtual;
    procedure CleanUpContent;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
  published
    property ExternalName: String read GetExternalName write SetExternalName; //I need to do this 'cause of RTTI Delphi bug!!! It doesn't see overriden Get and Set functions!
  end;


  TrwDDSTables = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  TrwDDSTableParams = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  TrwDDSTableParamValues = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;


  TrwDDSFields = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;


  TrwDDSFieldValues = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;

  TrwDDSTableKeys = class(TrwDataDictSysTable)
  private
    procedure CreateContent; override;
  public
    constructor Create(Collection: TCollection); override;
  end;


  TrwDataDictField = class;

  TrwDataDictFields = class(TDataDictFields)
  public
    procedure Assign(Source: TPersistent); override;
    function FieldByExternalName(const AFieldExternalName: String): TrwDataDictField;
  end;

  TrwDataDictField = class(TDataDictField, IrwDDField)
  private
    FExternalName: String;
    function ExternalNameNotEmpty: Boolean;

    //Interface Imlementation
    function GetFieldExternalName: String;
    function GetFieldName: String;

  public
    procedure Assign(Source: TPersistent); override;
  published
    property ExternalName: String read FExternalName write FExternalName stored ExternalNameNotEmpty;
  end;


  TrwDataDictParams = class(TDataDictParams)
  public
    procedure FixupReferences;
    function  ParamStr: string;
    function  ParamValueStr: string;
  end;


  TrwDataDictParam = class(TDataDictParam)
  private
    FRefField: TDataDictField;
    FRefTable: String;
    FDefaultValue: String;
    procedure SetDefaultValue(const Value: String);
    function  DefaultValueNotEmpty: Boolean;
    procedure ReadRefKey(Reader: TReader);
    procedure WriteRefKey(Writer: TWriter);
    procedure SetRefField(const Value: TDataDictField);
  protected
    FReadRefField: String;
    procedure DefineProperties(Filer: TFiler); override;
    function  GetRefFieldID: String;
    procedure FixupReferences;
  public
    procedure Assign(Source: TPersistent); override;
    property  RefField: TDataDictField read FRefField write SetRefField;
  published
    property DefaultValue: String read FDefaultValue write SetDefaultValue stored DefaultValueNotEmpty;
  end;


  TrwDictionary = class(TrwComponent)
  private
    procedure CheckPointer(const AObject: Pointer; const AEntity: String);
    function  GetTable(const ATable: String): TDataDictTable;
    function  GetField(const ATable, AField: String): TDataDictField;

  published
    function  PGetFields(ATable: Variant): Variant;
    function  PGetPrimKey(ATable: Variant): Variant;
    function  PGetLogPrimKey(ATable: Variant): Variant;
    function  PGetDisplayLogKey(ATable: Variant): Variant;
    function  PLookUpTo(ATable: Variant; AField: Variant): Variant;
    function  PFieldDisplayName(ATable: Variant; AField: Variant): Variant;
    function  PGetTables: Variant;
    function  PTableDisplayName(ATable: Variant): Variant;
    function  PTableGroupName(ATable: Variant): Variant;
    function  PGetFieldValueDescription(ATable: Variant; AField: Variant; AValue: Variant): Variant;
    function  PGetFieldValues(ATable: Variant; AField: Variant; ADescriptions: Variant): Variant;
  end;



implementation

uses rwEngine, rwUtils, sbAPI, rwLogQuery;

const cDDTables = 'dd$Tables';
      cDDTableParams = 'dd$Table_Params';
      cDDTableParamValues = 'dd$Table_Param_Values';
      cDDTableKeys = 'dd$Table_Keys';
      cDDFields = 'dd$Fields';
      cDDFieldValues = 'dd$Field_Values';


{ TrwDataDictionary }

procedure TrwDataDictionary.CleanUpSysTables;
var
  i: Integer;
begin
  for i := 0 to Tables.Count - 1 do
    if TrwDataDictTable(Tables[i]).TableType = rwtDDSystem then
      (Tables[i] as TrwDataDictSysTable).CleanUpContent;
end;

constructor TrwDataDictionary.Create(AOwner: TComponent);
begin
  inherited;
  FSyncLog := '';
end;

procedure TrwDataDictionary.DeleteField(const AField: TDataDictField);
var
  T: TDataDictTable;
  i, j: Integer;
begin
  for i := 0 to Tables.Count - 1 do
  begin
    T := Tables[i];
    for j := 0 to T.Params.Count -1 do
      if TrwDataDictParam(T.Params[j]).RefField = AField then
        TrwDataDictParam(T.Params[j]).RefField := nil;
  end;

  inherited;
end;

function TrwDataDictionary.GetFieldClass: TDataDictFieldClass;
begin
  Result := TrwDataDictField;
end;

function TrwDataDictionary.GetFieldsClass: TDataDictFieldsClass;
begin
  Result := TrwDataDictFields;
end;

function TrwDataDictionary.GetParamClass: TDataDictParamClass;
begin
  Result := TrwDataDictParam;
end;

function TrwDataDictionary.GetParamsClass: TDataDictParamsClass;
begin
  Result := TrwDataDictParams;
end;

function TrwDataDictionary.GetTable(const ATableName: String): IrwDDTable;
begin
  Result := TrwDataDictTable(Tables.TableByName(ATableName));
end;

function TrwDataDictionary.GetTableByExternalName(const AExternalTableName: String): IrwDDTable;
begin
  Result := TrwDataDictTables(Tables).TableByExternalName(AExternalTableName);
end;

function TrwDataDictionary.GetTableClass: TDataDictTableClass;
begin
  Result := TrwDataDictTable;
end;

function TrwDataDictionary.GetTablesClass: TDataDictTablesClass;
begin
  Result := TrwDataDictTables;
end;

procedure TrwDataDictionary.Initialize;
var
  MS1: IEvDualStream;
begin
  FSyncLog := '';
  MS1 := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.LoadDataDictionary(MS1.RealStream);
  MS1.Position := 0;
  if MS1.Size > 0 then
    LoadFromStream(MS1.RealStream);
end;

procedure TrwDataDictionary.LoadFromStream(const Stream: TStream);
begin
  inherited;
  UpdateSysTablesStructure;
end;

procedure TrwDataDictionary.SaveToStream(const Stream: TStream);
var
  L: TList;
  i: Integer;
begin
  L := TList.Create;
  try
    for i := 0 to Tables.Count - 1 do  // remove buffers and system tables
      if TrwDataDictTable(Tables[i]).TableType in [rwtBuffer, rwtDDSystem] then
        L.Add(Tables[i]);

    for i := 0 to L.Count - 1 do
      TrwDataDictTable(L[i]).Collection := nil;

    try
      inherited;
    finally
      for i := 0 to L.Count - 1 do     // add deleted tables back
        TrwDataDictTable(L[i]).Collection := Tables;
    end;

  finally
    FreeAndNil(L);
  end;
end;

procedure TrwDataDictionary.Synchronize;

  procedure AddInfoToSyncLog(const AInfo: String);
  begin
    if FSyncLog <> '' then
      FSyncLog := FSyncLog + #13;
    FSyncLog := FSyncLog + AInfo;
  end;

  function SameStrings(const AStrings: TStrings; const AText: TIniString): Boolean;
  var
    h: String;
  begin
    h := StringReplace(AStrings.Text, #13#10, #13, [rfReplaceAll]);
    if (h <> '') and (h[Length(h)] = #13) then
      Delete(h, Length(h), 1);

    if AText <> '' then
      Result := Atext = h
    else
      Result := True;
  end;

  function SyncField(const AField: TrwDataDictField): String;
  var
    RF: TrwDDFieldInfoRec;
  begin
    RF := RWEngineExt.AppAdapter.GetDDFieldInfo(TrwDataDictTable(AField.Table).ExternalName, AField.ExternalName);

    Result := '';
    if AField.FieldType <> RF.FieldType then
    begin
      AField.FieldType := RF.FieldType;
      Result := Result + ', Type';
    end;

    if AField.Size <> RF.Size then
    begin
      AField.Size := RF.Size;
      Result := Result + ', Size';
    end;

    if not SameStrings(AField.FieldValues, RF.FieldValues) then
    begin
      AField.FieldValues.Text := RF.FieldValues;
      Result := Result + ', Values';
    end;

    if Result <> '' then
      Delete(Result, 1, 1);
  end;


  procedure SyncParams(const ATable: TrwDataDictTable);
  var
    i: Integer;
    L: TStringList;
    TR: TrwDDTableInfoRec;
    PR: TrwDDParamInfoRec;
    P: TrwDataDictParam;
    h: String;
  begin
    L := TStringList.Create;
    try
      TR := RWEngineExt.AppAdapter.GetDDTableInfo(ATable.ExternalName);
      L.CommaText := TR.Params;
      if L.CommaText <> '' then
      begin
        if L.Count <> ATable.Params.Count then
        begin
          ATable.Params.Clear;
          for i := 0 to L.Count - 1 do
          begin
            PR := RWEngineExt.AppAdapter.GetDDParamInfo(ATable.ExternalName, L[i]);
            P := TrwDataDictParam(ATable.Params.AddParam);
            P.Name := PR.Name;
            P.DisplayName := PR.DisplayName;
            P.ParamType := PR.ParamType;
            P.ParamValues.Text := PR.ParamValues;
            P.DefaultValue := PR.DefaultValue;
          end;
          AddInfoToSyncLog('CHANGE Parameters:     ' + ATable.Name);
        end

        else
        begin
          for i := 0 to L.Count -1 do
          begin
            PR := RWEngineExt.AppAdapter.GetDDParamInfo(ATable.ExternalName, L[i]);
            P := TrwDataDictParam(ATable.Params[i]);
            h := '';
            if P.Name <> PR.Name then
            begin
              h := h + ', Name';
              P.Name := PR.Name;
              P.DisplayName := PR.DisplayName;
            end;

            if P.ParamType <> PR.ParamType then
            begin
              h := h + ', Type';
              P.ParamType := PR.ParamType;
            end;

            if not SameStrings(P.ParamValues, PR.ParamValues) then
            begin
              h := h + ', Values';
              P.ParamValues.Text := PR.ParamValues;
            end;

            if (PR.DefaultValue <> '') and (P.DefaultValue <> PR.DefaultValue) then
            begin
              h := h + ', Default';
              P.DefaultValue := PR.DefaultValue;
            end;

            if h <> '' then
            begin
              Delete(h, 1, 1);
              AddInfoToSyncLog('CHANGE Parameters ' + '(' + h + '):     ' + ATable.Name);
            end;
          end;
        end;
      end;

    finally
      L.Free;
    end;
  end;


  procedure SyncTable(const ATable: TrwDataDictTable);
  var
    i: Integer;
    L: TStringList;
    Fld: TrwDataDictField;
    s, fn: String;
  begin
    SyncParams(ATable);

    L := TStringList.Create;
    try
      L.CommaText := AnsiUpperCase(RWEngineExt.AppAdapter.GetDDFields(ATable.ExternalName));
      if L.CommaText <> '' then
      begin
        i := 0;
        while i < ATable.Fields.Count do
        begin
          if L.IndexOf(AnsiUpperCase(TrwDataDictField(ATable.Fields[i]).ExternalName)) = -1 then
          begin
            AddInfoToSyncLog('DELETE Field:     ' + ATable.Name + '.' + ATable.Fields[i].Name);
            DeleteField(ATable.Fields[i]);
          end
          else
          begin
            s := SyncField(TrwDataDictField(ATable.Fields[i]));
            if s <> '' then
              AddInfoToSyncLog('CHANGE Field ' + '(' + s + '):     ' + ATable.Name + '.' + ATable.Fields[i].Name);
            Inc(i);
          end
        end;
      end;

      for i := 0 to L.Count - 1 do
        if TrwDataDictFields(ATable.Fields).FieldByExternalName(L[i]) = nil then
        begin
          Fld := TrwDataDictField(ATable.Fields.AddField);

          s := L[i];
          fn := '';
          while s <> '' do
          begin
            if fn <> '' then
              fn := fn + '_';
            fn := fn + AnsiUpperCase(s[1]);
            Delete(s, 1, 1);
            fn := fn + AnsiLowerCase(GetNextStrValue(s, '_'));
          end;

          Fld.Name := fn;
          Fld.ExternalName := L[i];
          SyncField(Fld);

          AddInfoToSyncLog('ADD Field:     ' + ATable.Name + '.' + Fld.Name);
        end;

    finally
      L.Free;
    end;
  end;


  procedure SyncFKs;
  var
    i, j, k, m: Integer;
    T, T1: TrwDataDictTable;
    TR: TrwDDTableInfoRec;
    L: TStringList;
    h, h1, h2, h3, sRefTable: String;
    fl: Boolean;
    FK: TDataDictForeignKey;
    Fld: TrwDataDictField;
  begin
    L := TStringList.Create;
    try
      for i := 0 to Tables.Count - 1 do
      begin
        T := TrwDataDictTable(Tables[i]);
        if T.TableType <> rwtDDExternal then
          Continue;
        TR := RWEngineExt.AppAdapter.GetDDTableInfo(T.ExternalName);
        L.Text := TR.ForeignKey;

        for k := T.ForeignKeys.Count - 1 downto 0 do
        begin
          fl := False;
          for j := 0 to L.Count - 1 do
          begin
            h := L[j];
            h1 := GetNextStrValue(h, '=');
            if AnsiSameText(TrwDataDictTable(T.ForeignKeys[k].Table).ExternalName, h1) then
            begin
              fl := True;
              for m := 0 to T.ForeignKeys[k].Fields.Count - 1 do
              begin
                h1 := GetNextStrValue(h, ',');
                if not AnsiSameText(TrwDataDictField(T.ForeignKeys[k].Fields[m].Field).ExternalName, h1) then
                begin
                  fl := False;
                  break;
                end;
              end;
              if h <> '' then
                fl := False;

              if fl then
                break;
            end;
          end;

          if not fl and (TrwDataDictTable(T.ForeignKeys[k].Table).TableType = rwtDDExternal) then
          begin
            h := '';
            for m := 0 to T.ForeignKeys[k].Fields.Count - 1 do
            begin
              if h <> '' then
                h := h + ', ';
              h := h + T.ForeignKeys[k].Fields[m].Field.Name;
            end;
            h := T.Name + '(' + h + ') -> ' + T.ForeignKeys[k].Table.Name;
            AddInfoToSyncLog('DELETE FK:        ' + h);
            T.ForeignKeys[k].Free;
          end;
        end;

        for j := 0 to L.Count - 1 do
        begin
          h := L[j];
          sRefTable := GetNextStrValue(h, '=');
          h2 := h;
          T1 := TrwDataDictTables(Tables).TableByExternalName(sRefTable);
          if Assigned(T1) then
          begin
            fl := False;
            for k := 0 to T.ForeignKeys.Count - 1 do
            begin
              if AnsiSameText(TrwDataDictTable(T.ForeignKeys[k].Table).ExternalName, sRefTable) then
              begin
                fl := True;
                h := h2;
                for m := 0 to T.ForeignKeys[k].Fields.Count - 1 do
                begin
                  h3 := GetNextStrValue(h, ',');
                  if not AnsiSameText(TrwDataDictField(T.ForeignKeys[k].Fields[m].Field).ExternalName, h3) then
                  begin
                    fl := False;
                    break;
                  end;
                end;

                if h <> '' then
                  fl := False;

                if fl then
                  break;
              end;
            end;

            if not fl then
            begin
              fl := True;
              h := '';
              FK := T.ForeignKeys.AddForeignKey(T1);
              while h2 <> '' do
              begin
                h1 := GetNextStrValue(h2, '=');
                Fld := TrwDataDictFields(T.Fields).FieldByExternalName(h1);
                if not Assigned(Fld) then
                begin
                  FK.Free;
                  fl := False;
                  break;
                end;
                FK.Fields.AddKeyField(Fld);
                if h <> '' then
                  h := h + ',';
                h := h + Fld.Name;
              end;
              if fl then
              begin
                h := T.Name + '(' + h + ') -> ' + T1.Name;
                AddInfoToSyncLog('ADD FK:           ' + h);
              end;
            end;
          end

          else
            AddInfoToSyncLog('NEW Table for FK (add manually):     ' + sRefTable);
        end;
      end;
    finally
      FreeAndNil(L);
    end;
  end;


  procedure SyncTables;
  var
    i: Integer;
    L: TStringList;
  begin
    L := TStringList.Create;
    try
      L.CommaText := AnsiUpperCase(RWEngineExt.AppAdapter.GetDDTables);

      if L.CommaText <> '' then
      begin
        i := 0;
        while i < Tables.Count do
        begin
          if TrwDataDictTable(Tables[i]).TableType = rwtDDExternal then
          begin
            if L.IndexOf(AnsiUpperCase(TrwDataDictTable(Tables[i]).ExternalName)) = -1 then
            begin
              AddInfoToSyncLog('DELETE Table:     ' + Tables[i].Name);
              DeleteTable(Tables[i]);
            end
            else
            begin
              SyncTable(TrwDataDictTable(Tables[i]));
              Inc(i);
            end
          end
          else
            Inc(i);
        end;
      end;

    finally
      L.Free;
    end;

    SyncFKs;
  end;

{
  procedure FixReferences;
  var
    i, j, k: Integer;
    h: String;
    T: TDataDictTable;
  begin
    for i := 0 to Tables.Count - 1 do
      for j := 0 to Tables[i].Fields.Count - 1 do
      begin
        h := AnsiUpperCase(Tables[i].Fields[j].Name);
        if (Copy(h, Length(h) - 3, 4) = '_NBR') and
            not Assigned(Tables[i].PrimaryKey.FindField(Tables[i].Fields[j])) and
            not Assigned(Tables[i].ForeignKeys.ForeignKeyByField(Tables[i].Fields[j])) then
        begin
          T := Tables.TableByName(Copy(h, 1, Length(h) - 4));

          if not Assigned(T) then
          begin
            Delete(h, Length(h) - 3, 4);
            for k := 0 to Tables.Count - 1 do
            begin
              if Length(h) < Length(Tables[k].Name) then
                Continue;

              if AnsiSameText(Copy(h, Length(h) - Length(Tables[k].Name) + 1, Length(Tables[k].Name)), Tables[k].Name) then
              begin
                T := Tables[k];
                break;
              end;
            end;
          end;

          if Assigned(T) then
          begin
            Tables[i].ForeignKeys.AddForeignKey(T).Fields.AddKeyField(Tables[i].Fields[j]);
            AddInfoToSyncLog('ADD Foreign Key:     ' + Tables[i].Name + '.' + Tables[i].Fields[j].Name + ' -> ' + T.Name);
          end;
        end;
      end;
  end;
}
begin
  FSyncLog := '';
  SyncTables;
  CleanUpSysTables;

//  FixReferences;
end;

procedure TrwDataDictionary.Update;
var
  MS1: IEvDualStream;
  L: TList;
  i: Integer;
begin
  L := TList.Create;
  try
    for i := 0 to Tables.Count - 1 do  // remove buffers and system tables
      if TrwDataDictTable(Tables[i]).TableType in [rwtBuffer, rwtDDSystem] then
        L.Add(Tables[i]);

    for i := 0 to L.Count - 1 do
      TrwDataDictTable(L[i]).Collection := nil;

    try
      MS1 := TEvDualStreamHolder.Create;
      SaveToStream(MS1.RealStream);
      MS1.Position := 0;
      RWEngineExt.AppAdapter.UnloadDataDictionary(MS1.RealStream);
      UpdateDataDictionary(Self);
      FSyncLog := '';
    finally
      for i := 0 to L.Count - 1 do     // add deleted tables back
        TrwDataDictTable(L[i]).Collection := Tables;
    end;
  finally
    FreeAndNil(L);
  end;
end;


procedure TrwDataDictionary.UpdateSysTablesStructure;
var
  Tbl: TDataDictTable;
begin
  // tables
  Tbl := Tables.TableByName(cDDTables);
  if Assigned(Tbl) then
    (Tbl as TrwDDSTables).CleanUpContent
  else
    TrwDDSTables.Create(Tables);

  // Table params
  Tbl := Tables.TableByName(cDDTableParams);
  if Assigned(Tbl) then
    (Tbl as TrwDDSTableParams).CleanUpContent
  else
    TrwDDSTableParams.Create(Tables);

  // Table param values
  Tbl := Tables.TableByName(cDDTableParamValues);
  if Assigned(Tbl) then
    (Tbl as TrwDDSTableParamValues).CleanUpContent
  else
    TrwDDSTableParamValues.Create(Tables);

  // fields
  Tbl := Tables.TableByName(cDDFields);
  if Assigned(Tbl) then
    (Tbl as TrwDDSFields).CleanUpContent
  else
    TrwDDSFields.Create(Tables);

  // field values
  Tbl := Tables.TableByName(cDDFieldValues);
  if Assigned(Tbl) then
    (Tbl as TrwDDSFieldValues).CleanUpContent
  else
    TrwDDSFieldValues.Create(Tables);

  // Keys
  Tbl := Tables.TableByName(cDDTableKeys);
  if Assigned(Tbl) then
    (Tbl as TrwDDSTableKeys).CleanUpContent
  else
    TrwDDSTableKeys.Create(Tables);
end;

{ TrwDataDictTable }

constructor TrwDataDictTable.Create(Collection: TCollection);
begin
  inherited;
  FTableType := rwtDDExternal;
end;

destructor TrwDataDictTable.Destroy;
begin
  FreeAndNil(FQBInfo);
  inherited;
end;

function TrwDataDictTable.ExternalNameNotEmpty: Boolean;
begin
  Result := FExternalName <> '';
end;

procedure TrwDataDictTable.Assign(Source: TPersistent);
begin
  inherited;

  TableType := TrwDataDictTable(Source).TableType;
  ExternalName :=  TrwDataDictTable(Source).ExternalName;
  GroupName := TrwDataDictTable(Source).GroupName;
  SQLText :=  TrwDataDictTable(Source).SQLText;
  if Assigned(TrwDataDictTable(Source).FQBInfo) then
    QBInfo.LoadFromStream(TrwDataDictTable(Source).QBInfo);
end;

function TrwDataDictTable.GroupNameNotEmpty: Boolean;
begin
  Result := FGroupName <> '';
end;

procedure TrwDataDictTable.SetTableType(const Value: TrwDDTableType);
begin
  FTableType := Value;

  if FTableType = rwtDDQuery then
    if Assigned(FQBInfo) then
      FQBInfo.Size := 0
    else
      FQBInfo := TIsMemoryStream.Create
  else
    FreeAndNil(FQBInfo);
end;

function TrwDataDictTable.SQLTextNotEmpty: Boolean;
begin
  Result := FSQLText <>'';
end;

procedure TrwDataDictTable.ReadQBInfo(Stream: TStream);
begin
  QBInfo.LoadFromStream(Stream);
end;

procedure TrwDataDictTable.WriteQBInfo(Stream: TStream);
begin
  QBInfo.SaveToStream(Stream);
end;

procedure TrwDataDictTable.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('QBInfo', ReadQBInfo, WriteQBInfo, Assigned(FQBInfo));
end;

procedure TrwDataDictTable.FixupReferences;
begin
  inherited;
  TrwDataDictParams(Params).FixupReferences;
end;

function TrwDataDictTable.GetField(const AFieldName: String): IrwDDField;
begin
  Result := TrwDataDictField(Fields.FieldByName(AFieldName));
end;

function TrwDataDictTable.GetFieldByExternalName(const AExternalFieldName: String): IrwDDField;
begin
  Result := TrwDataDictFields(Fields).FieldByExternalName(AExternalFieldName);
end;

function TrwDataDictTable.GetFieldList: TCommaDilimitedString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Fields.Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + Fields[i].Name;
  end;
end;

function TrwDataDictTable.GetForeignKeys: TIniString;
var
  i, j: Integer;
begin
  Result := '';
  for i := 0 to ForeignKeys.Count - 1 do
  begin
    if i > 0 then
      Result := Result + #13;

    Result := ForeignKeys[i].Table.Name + '=';
    for j := 0 to ForeignKeys[i].Fields.Count - 1 do
    begin
      if j > 0 then
        Result := Result + ',';
      Result := Result + ForeignKeys[i].Fields[j].Field.Name;
    end;
  end;
end;

function TrwDataDictTable.GetLogicalKey: TCommaDilimitedString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to LogicalKey.Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + LogicalKey[i].Field.Name;
  end;
end;

function TrwDataDictTable.GetPrimaryKey: TCommaDilimitedString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to PrimaryKey.Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + PrimaryKey[i].Field.Name;
  end;
end;

function TrwDataDictTable.GetTableName: String;
begin
  Result := Name;
end;

function TrwDataDictTable.GetTableExternalName: String;
begin
  Result := ExternalName;
end;

function TrwDataDictTable.GetForeignKeysByField(const AField: String): TIniString;
var
  j: Integer;
  F: TDataDictField;
  FK: TDataDictForeignKey;
begin
  Result := '';

  F := Fields.FieldByName(AField);

  FK := ForeignKeys.ForeignKeyByField(F, 0);
  while Assigned(FK) do
  begin
    if Result <> '' then
      Result := Result + #13;

    Result := FK.Table.Name + '=';
    for j := 0 to FK.Fields.Count - 1 do
    begin
      if j > 0 then
        Result := Result + ',';
      Result := Result + FK.Fields[j].Field.Name;
    end;
    FK := ForeignKeys.ForeignKeyByField(F, FK.Index + 1);
  end;
end;

function TrwDataDictTable.GetForeignKeysByTable(const ATable: String): TIniString;
var
  j: Integer;
  T: TDataDictTable;
  FK: TDataDictForeignKey;
begin
  Result := '';

  T := DataDictionaryObject.Tables.TableByName(ATable);
  FK := ForeignKeys.ForeignKeyByTable(T, 0);
  while Assigned(FK) do
  begin
    if Result <> '' then
      Result := Result + #13;

    Result := FK.Table.Name + '=';
    for j := 0 to FK.Fields.Count - 1 do
    begin
      if j > 0 then
        Result := Result + ',';
      Result := Result + FK.Fields[j].Field.Name;
    end;
    FK := ForeignKeys.ForeignKeyByTable(T, FK.Index + 1);
  end;
end;

function TrwDataDictTable.GetExternalName: String;
begin
  Result := FExternalName;
end;

procedure TrwDataDictTable.SetExternalName(const AValue: String);
begin
  FExternalName := AValue;
end;


{ TrwDataDictField }

procedure TrwDataDictField.Assign(Source: TPersistent);
begin
  inherited;
  ExternalName := TrwDataDictField(Source).ExternalName;
end;

function TrwDataDictField.ExternalNameNotEmpty: Boolean;
begin
  Result := FExternalName <> '';
end;

function TrwDataDictField.GetFieldExternalName: String;
begin
  Result := ExternalName;
end;

function TrwDataDictField.GetFieldName: String;
begin
  Result := Name;
end;


{ TrwDataDictParam }

procedure TrwDataDictParam.Assign(Source: TPersistent);
begin
  inherited;
  DefaultValue := TrwDataDictParam(Source).DefaultValue;
  FReadRefField  := TrwDataDictParam(Source).GetRefFieldID;
  FixupReferences;
end;

function TrwDataDictParam.DefaultValueNotEmpty: Boolean;
begin
  Result := FDefaultValue <> '';
end;

procedure TrwDataDictParam.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('RefField', ReadRefKey, WriteRefKey, Assigned(FRefField));
end;

procedure TrwDataDictParam.SetDefaultValue(const Value: String);
begin
  FDefaultValue := Value;
end;

procedure TrwDataDictParam.ReadRefKey(Reader: TReader);
begin
  FReadRefField := Reader.ReadString;
end;

procedure TrwDataDictParam.WriteRefKey(Writer: TWriter);
begin
  Writer.WriteString(GetRefFieldID);
end;

function TrwDataDictParam.GetRefFieldID: String;
begin
  if Assigned(FRefField) then
    Result := TrwDataDictTable(TDataDictFields(FRefField.Collection).Owner).Name + '.' + FRefField.Name
  else
    Result := '';
end;

procedure TrwDataDictParam.FixupReferences;
var
  i: Integer;
  Tbl, Fld: String;
  T: TDataDictTable;
begin
  if FReadRefField <> '' then
  begin
    try
      RefField := nil;
      i := Pos('.', FReadRefField);
      if i = 0 then Exit;
      Tbl := Copy(FReadRefField, 1, i - 1);
      Fld := Copy(FReadRefField, i + 1, Length(FReadRefField) - i);
      T := DataDictionaryObject.Tables.TableByName(Tbl);
      if not Assigned(T) then Exit;
      RefField := T.Fields.FieldByName(Fld);
    finally
      FReadRefField := '';
    end;
  end;

  T := DataDictionaryObject.Tables.TableByName(FRefTable);
  if not Assigned(T) or (T.Fields.IndexOf(RefField) = -1) then
    RefField := nil;
end;

procedure TrwDataDictParam.SetRefField(const Value: TDataDictField);
begin
  FRefField := Value;
  if Assigned(FRefField) then
    FRefTable := TrwDataDictTable(TDataDictFields(FRefField.Collection).Owner).Name
  else
    FRefTable := '';
end;


{ TrwDataDictParams }

procedure TrwDataDictParams.FixupReferences;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    TrwDataDictParam(Items[i]).FixupReferences;
end;

function TrwDataDictParams.ParamStr: string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result+':' + Items[i].Name;
  end;
end;

function TrwDataDictParams.ParamValueStr: string;
var
  i: Integer;
  h: String;
begin
  Result := '';
  for i := 0 to Count - 1 do
  begin
    if Result <> '' then
      Result := Result + ',';
    with TrwDataDictParam(Items[i]) do
    begin
      if DefaultValue = '' then
        h := ':' + Name
      else
      begin
        if DefaultValue[1] = '@' then
          h := DefaultValue

        else if DefaultValue = '<NULL>' then
          h := 'Null'

        else
          case ParamType of
            ddtInteger,
            ddtCurrency,
            ddtFloat:    h := DefaultValue;

            ddtDateTime,
            ddtBoolean,
            ddtString:   h := '''' + DefaultValue + '''';

            ddtArray:    h := '[' + DefaultValue + ']';
          end;
      end;
    end;

    Result := Result + h;
  end;
end;


{ TrwDataDictTables }

function TrwDataDictTables.TableByExternalName(const ATableExternalName: String): TrwDataDictTable;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if AnsiSameText(ATableExternalName, TrwDataDictTable(Items[i]).ExternalName) then
    begin
      Result := TrwDataDictTable(Items[i]);
      break;
    end;
end;


{ TrwDataDictFields }

procedure TrwDataDictFields.Assign(Source: TPersistent);
var
  L: TList;
  i, j: Integer;
  Tbls: TDataDictTables;
  Fld: TDataDictField;
begin
  // fixup Table params references
  L := TList.Create;
  try
    if Count > 0 then
    begin
      Tbls := TDataDictTables(TDataDictTable(Owner).Collection);
      for i := 0 to Tbls.Count - 1 do
      begin
        if Tbls[i] = Owner then
         continue;

        for j := 0 to Tbls[i].Params.Count - 1 do
        begin
          Fld := TrwDataDictParam(Tbls[i].Params[j]).RefField;
          if Assigned(Fld) and (Fld.Table = Owner) then
          begin
            TrwDataDictParam(Tbls[i].Params[j]).FReadRefField := TrwDataDictParam(Tbls[i].Params[j]).GetRefFieldID;
            if L.IndexOf(Tbls[i]) = -1 then
              L.Add(Tbls[i]);
          end;
        end;
      end;
    end;

    inherited;

    for i := 0 to L.Count - 1 do
      for j := 0 to TDataDictTable(L[i]).Params.Count - 1 do
        TrwDataDictParam(TDataDictTable(L[i]).Params[j]).FixupReferences;

  finally
    FreeAndNil(L);
  end;
end;

function TrwDataDictFields.FieldByExternalName(const AFieldExternalName: String): TrwDataDictField;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
    if AnsiSameText(AFieldExternalName, TrwDataDictField(Items[i]).ExternalName) then
    begin
      Result := TrwDataDictField(Items[i]);
      break;
    end;
end;


{ TrwDictionary }

function  TrwDictionary.PGetFields(ATable: Variant): Variant;
var
  rwT: TDataDictTable;
  j: Integer;
begin
  rwT := GetTable(ATable);
  Result := VarArrayCreate([0, rwT.Fields.Count - 1], varVariant);
  for j := 0 to rwT.Fields.Count - 1 do
    Result[j] := rwT.Fields[j].Name;
end;


function TrwDictionary.PGetLogPrimKey(ATable: Variant): Variant;
var
  rwT: TDataDictTable;
  j: Integer;
begin
  rwT := GetTable(ATable);
  Result := VarArrayCreate([0, rwT.LogicalKey.Count - 1], varVariant);
  for j := 0 to rwT.LogicalKey.Count - 1 do
    Result[j] := rwT.LogicalKey[j].Field.Name;
end;


function TrwDictionary.PGetPrimKey(ATable: Variant): Variant;
var
  rwT: TDataDictTable;
  j: Integer;
begin
  rwT := GetTable(ATable);
  Result := VarArrayCreate([0, rwT.PrimaryKey.Count - 1], varVariant);
  for j := 0 to rwT.PrimaryKey.Count - 1 do
    Result[j] := rwT.PrimaryKey[j].Field.Name;
end;


function TrwDictionary.PLookUpTo(ATable: Variant; AField: Variant): Variant;
var
  rwT: TDataDictTable;
  rwF: TDataDictField;
  rwFK: TDataDictForeignKey;
begin
  rwT := GetTable(ATable);
  rwF := GetField(ATable, AField);
  rwFK := rwT.ForeignKeys.ForeignKeyByField(rwF);
  if Assigned(rwFK) then
    Result := rwFK.Table.Name
  else
    Result := '';
end;

function TrwDictionary.PFieldDisplayName(ATable, AField: Variant): Variant;
begin
  Result := GetField(ATable, AField).DisplayName;
end;


function TrwDictionary.PGetTables: Variant;
var
  i: Integer;
begin
  Result := VarArrayCreate([0, DataDictionary.Tables.Count - 1], varVariant);
  for i := 0 to DataDictionary.Tables.Count - 1 do
    Result[i] := DataDictionary.Tables[i].Name;
end;


function TrwDictionary.PTableDisplayName(ATable: Variant): Variant;
begin
  Result := GetTable(ATable).DisplayName;
end;


function TrwDictionary.PTableGroupName(ATable: Variant): Variant;
begin
  Result := TrwDataDictTable(GetTable(ATable)).GroupName;
end;

function TrwDictionary.PGetFieldValueDescription(ATable, AField, AValue: Variant): Variant;
var
  rwF: TDataDictField;
  h: String;
  L: TStringList;
begin
  L := TStringList.Create;
  try
    rwF := GetField(ATable, AField);
    L.Text := rwF.FieldValuesReal;
    if VarIsNull(AValue) then
      h := 'null'
    else
      h := VarToStr(AValue);
    Result := L.Values[h];
  finally
    L.Free;
  end;
end;


procedure TrwDictionary.CheckPointer(const AObject: Pointer; const AEntity: String);
begin
  if not Assigned(AObject) then
    raise Exception.Create(AEntity + ' is not found');
end;

function TrwDictionary.GetField(const ATable, AField: String): TDataDictField;
begin
  Result := GetTable(ATable).Fields.FieldByName(AField);
  CheckPointer(Result, 'Field "' + AField + '"');
end;

function TrwDictionary.GetTable(const ATable: String): TDataDictTable;
begin
  Result := DataDictionary.Tables.TableByName(ATable);
  CheckPointer(Result, 'Table "' + ATable + '"');
end;

function TrwDictionary.PGetFieldValues(ATable, AField, ADescriptions: Variant): Variant;
var
  rwF: TDataDictField;
  L: TStringList;
  i: Integer;
begin
  L := TStringList.Create;
  try
    rwF := GetField(ATable, AField);
    L.Text := rwF.FieldValuesReal;
    Result := VarArrayCreate([0, L.Count - 1], varVariant);
    for i := 0 to L.Count - 1 do
      if ADescriptions then
        Result[i] := l[i]
      else
        Result[i] := L.Names[i];
  finally
    L.Free;
  end;
end;

function TrwDictionary.PGetDisplayLogKey(ATable: Variant): Variant;
var
  rwT: TDataDictTable;
  j: Integer;
  Key: TDataDictPrimKey;
begin
  rwT := GetTable(ATable);

  if rwT.DisplayLogicalKey.Count > 0 then
    Key := rwT.DisplayLogicalKey
  else if rwT.LogicalKey.Count > 0 then
    Key := rwT.LogicalKey
  else
    Key := rwT.PrimaryKey;

  Result := VarArrayCreate([0, Key.Count - 1], varVariant);
  for j := 0 to Key.Count - 1 do
    Result[j] := Key[j].Field.Name;
end;

{ TrwDataDictSysTable }

procedure TrwDataDictSysTable.CleanUpContent;
var
  i: Integer;
begin
  if FExternalName <> '' then
    try
      i := CacheInfo.FindTable(Name);
      if i <> - 1 then
        CacheInfo[i].DeleteFiles;

      if FileExists(FExternalName) then
        try
          sbDropTable(FExternalName);
        except
        end;
    finally
      FExternalName := '';
    end;
end;

constructor TrwDataDictSysTable.Create(Collection: TCollection);
begin
  inherited;
  TableType := rwtDDSystem;
  GroupName := 'Metadata';
end;

procedure TrwDataDictSysTable.CreateContent;
var
  Flds: TsbFields;
  rwF: TrwDataDictField;
  i: Integer;
begin
  FExternalName := CacheInfo.GetCachePath + Name + sbTableExt;

  Flds := TsbFields.Create;
  try
    for i := 0 to Fields.Count - 1 do
    begin
      rwF := TrwDataDictField(Fields[i]);
      Flds.CreateField(rwF.Name, DDTypetoSBType(rwF.FieldType), rwF.Size);
    end;
    sbCreateTable(FExternalName, Flds);
  finally
    FreeAndNil(Flds);
  end;
end;

destructor TrwDataDictSysTable.Destroy;
begin
  CleanUpContent;
  inherited;
end;

function TrwDataDictSysTable.GetExternalName: String;
begin
  if (FExternalName <> '') and not FileExists(FExternalName) then
    FExternalName := '';

  if FExternalName = '' then
    CreateContent;

  Result := FExternalName;
end;

procedure TrwDataDictSysTable.SetExternalName(const AValue: String);
begin
// empty
end;

{ TrwDDSTables }

constructor TrwDDSTables.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDTables;
  DisplayName := 'DD Tables';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Type';
  Fld.FieldType := ddtString;
  Fld.Size := 1;
  Fld.DisplayName := 'Table Type';
  Fld.FieldValues.Add('U=Unknown');
  Fld.FieldValues.Add('E=External');
  Fld.FieldValues.Add('Q=QB Query');
  Fld.FieldValues.Add('C=RW Component');
  Fld.FieldValues.Add('B=Buffer');
  Fld.FieldValues.Add('S=System');

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$External_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'External Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Display_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Display Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Misc_Data';
  Fld.FieldType := ddtBLOB;
  Fld.DisplayName := 'Misc Data';
end;

procedure TrwDDSTables.CreateContent;
var
  i: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;
  TblType: String;
begin
  inherited;

  Tbl := sbOpenTable(FExternalName, sbmReadWrite);
  for i := 0 to DataDictionaryObject.Tables.Count - 1 do
  begin
    ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
    TblType := Fields[1].FieldValues.Names[Ord(ddT.TableType)];
    Tbl.AppendRecord([ddT.Name, TblType, ddT.ExternalName, ddT.DisplayName, ddT.SQLText]);
  end;
end;

{ TrwDDSFields }

constructor TrwDDSFields.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDFields;
  DisplayName := 'Table Fields';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Field Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Type';
  Fld.FieldType := ddtString;
  Fld.Size := 1;
  Fld.DisplayName := 'Field Type';  
  Fld.FieldValues.Add('U=Unknown');
  Fld.FieldValues.Add('I=Integer');
  Fld.FieldValues.Add('F=Float');
  Fld.FieldValues.Add('C=Currency');
  Fld.FieldValues.Add('D=DateTime');
  Fld.FieldValues.Add('S=String');
  Fld.FieldValues.Add('A=Array');
  Fld.FieldValues.Add('B=Blob');
  Fld.FieldValues.Add('M=Memo');
  Fld.FieldValues.Add('G=Graphic');
  Fld.FieldValues.Add('L=Boolean');

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Size';
  Fld.FieldType := ddtInteger;
  Fld.DisplayName := 'Field Size';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$External_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'External Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Display_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Display Name';
end;

procedure TrwDDSFields.CreateContent;
var
  i, j: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;
  ddF: TrwDataDictField;
  FldType: String;
begin
  inherited;

  Tbl := sbOpenTable(FExternalName, sbmReadWrite);
  for i := 0 to DataDictionaryObject.Tables.Count - 1 do
  begin
    ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
    for j := 0 to ddT.Fields.Count - 1 do
    begin
      ddF := ddT.Fields[j] as TrwDataDictField;
      FldType := Fields[2].FieldValues.Names[Ord(ddF.FieldType)];
      Tbl.AppendRecord([ddT.Name, ddF.Name, FldType, ddF.Size, ddF.ExternalName, ddF.DisplayName]);
    end;
  end;
end;


{ TrwDDSFieldValues }

constructor TrwDDSFieldValues.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDFieldValues;
  DisplayName := 'Field Values';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Field Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Value';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Value';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Display_Value';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Display_Value';
end;

procedure TrwDDSFieldValues.CreateContent;
var
  i, j, k: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;
  ddF: TrwDataDictField;
  FldVal: String;
  S: TStringList;
begin
  inherited;

  S := TStringList.Create;
  try
    Tbl := sbOpenTable(FExternalName, sbmReadWrite);
    for i := 0 to DataDictionaryObject.Tables.Count - 1 do
    begin
      ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
      for j := 0 to ddT.Fields.Count - 1 do
      begin
        ddF := ddT.Fields[j] as TrwDataDictField;
        S.Text := ddF.FieldValuesReal;
        for k := 0 to S.Count - 1 do
        begin
          FldVal := S[k];
          Tbl.AppendRecord([ddT.Name, ddF.Name, GetNextStrValue(FldVal, '='), FldVal]);
        end;
      end;
    end;
  finally
    S.Free;
  end;
end;

{ TrwDDSTableParams }

constructor TrwDDSTableParams.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDTableParams;
  DisplayName := 'DD Table Parameters';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Param_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Parameter Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Param_Type';
  Fld.FieldType := ddtString;
  Fld.Size := 1;
  Fld.DisplayName := 'Parameter Type';
  Fld.FieldValues.Add('U=Unknown');
  Fld.FieldValues.Add('I=Integer');
  Fld.FieldValues.Add('F=Float');
  Fld.FieldValues.Add('C=Currency');
  Fld.FieldValues.Add('D=DateTime');
  Fld.FieldValues.Add('S=String');
  Fld.FieldValues.Add('A=Array');
  Fld.FieldValues.Add('B=Blob');
  Fld.FieldValues.Add('M=Memo');
  Fld.FieldValues.Add('G=Graphic');
  Fld.FieldValues.Add('L=Boolean');

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Param_Position';
  Fld.FieldType := ddtInteger;
  Fld.DisplayName := 'Parameter Position';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Display_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Display Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Ref_Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Reference Table';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Ref_Field_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Reference Field';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Default_Value';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Default Value';
end;

procedure TrwDDSTableParams.CreateContent;
var
  i, j: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;
  ddP: TrwDataDictParam;
  ParType: String;
  RefT,RefF: Variant;
begin
  inherited;

  Tbl := sbOpenTable(FExternalName, sbmReadWrite);
  for i := 0 to DataDictionaryObject.Tables.Count - 1 do
  begin
    ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
    for j := 0 to ddT.Params.Count - 1 do
    begin
      ddP := ddT.Params[j] as TrwDataDictParam;
      ParType := Fields[2].FieldValues.Names[Ord(ddP.ParamType)];

      if ddP.RefField <> nil then
      begin
        RefT := ddP.RefField.Table.Name;
        RefF := ddP.RefField.Name;        
      end
      else
      begin
        RefT := Null;
        RefF := Null;        
      end;

      Tbl.AppendRecord([ddT.Name, ddP.Name, ParType, j + 1, ddP.DisplayName, RefT, RefF, ddP.DefaultValue]);
    end;
  end;
end;

{ TrwDDSTableParamValues }

constructor TrwDDSTableParamValues.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDTableParamValues;
  DisplayName := 'Parameter Values';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Param_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Param Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Value';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Value';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Display_Value';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Display_Value';
end;

procedure TrwDDSTableParamValues.CreateContent;
var
  i, j, k: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;
  ddP: TrwDataDictParam;
  ParVal: String;
  S: TStringList;
begin
  inherited;

  S := TStringList.Create;
  try
    Tbl := sbOpenTable(FExternalName, sbmReadWrite);
    for i := 0 to DataDictionaryObject.Tables.Count - 1 do
    begin
      ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
      for j := 0 to ddT.Params.Count - 1 do
      begin
        ddP := ddT.Params[j] as TrwDataDictParam;
        S.Text := ddP.ParamValuesReal;
        for k := 0 to S.Count - 1 do
        begin
          ParVal := S[k];
          Tbl.AppendRecord([ddT.Name, ddP.Name, GetNextStrValue(ParVal, '='), ParVal]);
        end;
      end;
    end;
  finally
    S.Free;
  end;
end;

{ TrwDDSTableKeys }

constructor TrwDDSTableKeys.Create(Collection: TCollection);
var
  Fld: TrwDataDictField;
begin
  inherited;

  Name := cDDTableKeys;
  DisplayName := 'Table Keys';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Table Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Key_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Internal Key ID';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Key_Type';
  Fld.FieldType := ddtString;
  Fld.Size := 1;
  Fld.DisplayName := 'Key Type';
  Fld.FieldValues.Add('P=Primary');
  Fld.FieldValues.Add('L=Logical');
  Fld.FieldValues.Add('F=Foreign');

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Field Name';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Field_Position';
  Fld.FieldType := ddtInteger;
  Fld.DisplayName := 'Field Position';

  Fld := TrwDataDictField(Fields.AddField);
  Fld.Name := 'dd$Ref_Table_Name';
  Fld.FieldType := ddtString;
  Fld.Size := 64;
  Fld.DisplayName := 'Reference Table';
end;

procedure TrwDDSTableKeys.CreateContent;
var
  i: Integer;
  Tbl: IrwSBTable;
  ddT: TrwDataDictTable;

  procedure AddPK;
  var
    i: Integer;
  begin
    for i := 0 to ddT.PrimaryKey.Count - 1 do
      Tbl.AppendRecord([ddT.Name, 'PK', 'P', ddT.PrimaryKey[i].Field.Name, i + 1, Null]);
  end;

  procedure AddLK;
  var
    i: Integer;
  begin
    for i := 0 to ddT.LogicalKey.Count - 1 do
      Tbl.AppendRecord([ddT.Name, 'LK', 'L', ddT.LogicalKey[i].Field.Name, i + 1, Null]);
  end;

  procedure AddFK;
  var
    i, j: Integer;
    FK: TDataDictForeignKey;
  begin
    for i := 0 to ddT.ForeignKeys.Count - 1 do
    begin
      FK := ddT.ForeignKeys[i];
      for j := 0 to FK.Fields.Count - 1 do
        Tbl.AppendRecord([ddT.Name, 'FK' + IntToStr(i + 1), 'F', FK.Fields[j].Field.Name, j + 1, FK.Table.Name]);
    end;  
  end;

begin
  inherited;

  Tbl := sbOpenTable(FExternalName, sbmReadWrite);
  for i := 0 to DataDictionaryObject.Tables.Count - 1 do
  begin
    ddT := DataDictionaryObject.Tables[i] as TrwDataDictTable;
    AddPK;
    AddLK;
    AddFK;
  end;
end;

end.
