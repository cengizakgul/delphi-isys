unit rwDDForeignKeyEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, StdCtrls, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictParamEditFrm, rwCustomDataDictionary,
  ExtCtrls, DataDictForeignKeyEditFrm;

type
  TrwDDForeignKeyEdit = class(TDataDictCollectionEdit)
    btnEdit: TButton;
    procedure btnEditClick(Sender: TObject);
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
  public
  end;

implementation

{$R *.dfm}


{ TddForeignKeyEdit }

function TrwDDForeignKeyEdit.GetItemText(const AItem: TCollectionItem): String;
var
  i: Integer;
begin
  Result := TDataDictForeignKey(AItem).Table.Name + '   (';
  for i := 0 to TDataDictForeignKey(AItem).Fields.Count - 1 do
  begin
    Result := Result + TDataDictForeignKey(AItem).Fields[i].Field.Name;
    if i < TDataDictForeignKey(AItem).Fields.Count - 1 then
      Result := Result + ', ';
  end;
  Result := Result + ')';
end;

procedure TrwDDForeignKeyEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
begin
  if not ShowForeignKey(TableEditor, TDataDictForeignKey(AItem)) then
    Accept := False;
end;

procedure TrwDDForeignKeyEdit.btnEditClick(Sender: TObject);
begin
  if ShowForeignKey(Self, TDataDictForeignKey(SelectedItem)) then
  begin
    UpdateItem(SelectedItem);
    OnChange;
  end;
end;

end.
