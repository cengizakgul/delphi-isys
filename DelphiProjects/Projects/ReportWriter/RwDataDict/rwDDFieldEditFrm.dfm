inherited rwDDFieldEdit: TrwDDFieldEdit
  Width = 320
  Height = 220
  inherited pnlContainer: TPanel
    Width = 320
    Height = 220
    DesignSize = (
      320
      220)
    inherited lDisplayName: TLabel
      Top = 56
    end
    inherited lNotes: TLabel
      Top = 110
    end
    inherited lSize: TLabel
      Top = 83
    end
    object lExternalName: TLabel [4]
      Left = 0
      Top = 29
      Width = 69
      Height = 13
      Caption = 'External Name'
    end
    inherited edName: TEdit
      Width = 237
    end
    inherited edDisplayName: TEdit
      Top = 54
      Width = 237
      TabOrder = 2
    end
    inherited memNotes: TMemo
      Top = 126
      Width = 320
      Height = 94
      TabOrder = 5
    end
    inherited FieldType: TDataDictDataTypes
      Top = 81
      TabOrder = 3
      inherited cbType: TComboBox
        Enabled = False
      end
    end
    object edExternalName: TEdit [9]
      Left = 82
      Top = 27
      Width = 237
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 1
      OnChange = edExternalNameChange
      OnExit = edExternalNameExit
    end
    inherited edSize: TEdit
      Top = 81
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 4
    end
  end
  inherited pcFieldProp: TPageControl
    Width = 320
    Height = 220
  end
end
