inherited rwDDTablePropEdit: TrwDDTablePropEdit
  Width = 449
  Height = 208
  inherited pnlContainer: TPanel
    Width = 449
    Height = 208
    DesignSize = (
      449
      208)
    inherited lDisplayName: TLabel
      Top = 56
    end
    inherited lNotes: TLabel
      Top = 127
    end
    object lExternalName: TLabel [3]
      Left = 0
      Top = 29
      Width = 69
      Height = 13
      Caption = 'External Name'
    end
    object lTableType: TLabel [4]
      Left = 0
      Top = 83
      Width = 54
      Height = 13
      Caption = 'Table Type'
    end
    object Label1: TLabel [5]
      Left = 220
      Top = 83
      Width = 29
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Group'
    end
    inherited edName: TEdit
      Width = 366
    end
    inherited edDisplayName: TEdit
      Top = 54
      Width = 367
      TabOrder = 2
    end
    inherited memNotes: TMemo
      Top = 144
      Width = 449
      Height = 64
      TabOrder = 6
    end
    object btnQB: TButton
      Left = 369
      Top = 111
      Width = 80
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'Query Builder'
      TabOrder = 4
      OnClick = btnQBClick
    end
    object cbTableType: TComboBox
      Left = 82
      Top = 81
      Width = 119
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 3
      OnChange = cbTableTypeChange
      Items.Strings = (
        'Unknown'
        'External'
        'RW Query'
        'RW Component'
        'Buffer')
    end
    object edExternalName: TEdit
      Left = 82
      Top = 27
      Width = 366
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 1
      OnChange = edExternalNameChange
      OnExit = edExternalNameExit
    end
    object cbGroupName: TComboBox
      Left = 256
      Top = 81
      Width = 193
      Height = 21
      Anchors = [akTop, akRight]
      ItemHeight = 13
      TabOrder = 5
      OnChange = cbGroupNameChange
      OnExit = cbGroupNameExit
    end
  end
end
