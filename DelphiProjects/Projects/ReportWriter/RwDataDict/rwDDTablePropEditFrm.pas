unit rwDDTablePropEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictTablePropEditFrm, StdCtrls, ExtCtrls, rwDataDictionary, rmTypes;

type
  TrwDDTablePropEdit = class(TDataDictTablePropEdit)
    lExternalName: TLabel;
    btnQB: TButton;
    lTableType: TLabel;
    cbTableType: TComboBox;
    Label1: TLabel;
    edExternalName: TEdit;
    cbGroupName: TComboBox;
    procedure btnQBClick(Sender: TObject);
    procedure cbTableTypeChange(Sender: TObject);
    procedure edExternalNameExit(Sender: TObject);
    procedure edExternalNameChange(Sender: TObject);
    procedure cbGroupNameChange(Sender: TObject);
    procedure cbGroupNameExit(Sender: TObject);
  private
    procedure InitGroupBox;
  public
    procedure OnShowItem; override;
  end;

implementation

uses DataDictItemEditFrm, rwQueryBuilderFrm, rwQBInfo, rwUtils, rwDDTableEditorFrm, sbAPI,
  rwCustomDataDictionary, rwTypes, rwDB;

{$R *.dfm}

{ TrwDDTablePropEdit }

procedure TrwDDTablePropEdit.OnShowItem;
begin
  inherited;
  if ddItem <> nil then
  begin
    InitGroupBox;
    cbTableType.ItemIndex := Ord(TrwDataDictTable(ddItem).TableType);
    edExternalName.Text := TrwDataDictTable(ddItem).ExternalName;
    cbGroupName.Text := TrwDataDictTable(ddItem).GroupName;
    btnQB.Visible := TrwDataDictTable(ddItem).TableType in [rwtDDQuery, rwtDDComponent];
    if TrwDataDictTable(ddItem).TableType = rwtDDQuery then
      btnQB.Caption := 'Query Builder'
    else if TrwDataDictTable(ddItem).TableType = rwtDDComponent then
      btnQB.Caption := 'Sync Sructure';
  end
  else
  begin
    cbTableType.ItemIndex := 0;
    cbGroupName.Text := '';
    btnQB.Visible := False;
  end;
end;

procedure TrwDDTablePropEdit.btnQBClick(Sender: TObject);
var
  QB: TrwQBQuery;
  Q: TrwQuery;

  procedure SyncFields;
  var
    i, j: Integer;
    h: String;
    SFld: TrwQBShowingField;
    Fld, iFld: TrwDataDictField;
    lParams: TsbParams;
    Prm: TrwDataDictParam;
  begin
    // Update Table Parameters

    if not Assigned(Q) then
    begin
      lParams := TsbParams.Create;
      try
        lParams.ParseSQL(TrwDataDictTable(ddItem).SQLText, True);

        for i := TrwDataDictTable(ddItem).Params.Count - 1 downto 0 do
        begin
          Prm := TrwDataDictParam(lParams.ParamByName(TrwDataDictTable(ddItem).Params[i].Name));
          if not Assigned(Prm) then
            TrwDataDictTable(ddItem).Params[i].Free;
        end;

        for i := 0 to lParams.ItemCount -1 do
        begin
          h := lParams[i].Name;
          if not Assigned(TrwDataDictTable(ddItem).Params.ParamByName(h)) then
          begin
            Prm := TrwDataDictParam(TrwDataDictTable(ddItem).Params.Add);
            Prm.Name := h;
            Prm.DisplayName := h;
          end;
        end;

      finally
        lParams.Free;
      end;
    end

    else
    begin
      for i := TrwDataDictTable(ddItem).Params.Count - 1 downto 0 do
      begin
        j := Q.GlobalVarFunc.Variables.IndexOf(TrwDataDictTable(ddItem).Params[i].Name);
        if (j = -1) or (Q.GlobalVarFunc.Variables[j].Visibility <> rvvPublished) then
          TrwDataDictTable(ddItem).Params[i].Free;
      end;

      for i := 0 to Q.GlobalVarFunc.Variables.Count - 1 do
        if Q.GlobalVarFunc.Variables[i].Visibility = rvvPublished then
        begin
          Prm := TrwDataDictParam(TrwDataDictTable(ddItem).Params.ParamByName(Q.GlobalVarFunc.Variables[i].Name));

          if not Assigned(Prm) then
          begin
            Prm := TrwDataDictParam(TrwDataDictTable(ddItem).Params.Add);
            Prm.Name := Q.GlobalVarFunc.Variables[i].Name;
            Prm.DisplayName := Q.GlobalVarFunc.Variables[i].DisplayName;
            if Prm.DisplayName = '' then
              Prm.DisplayName := Prm.Name;
          end;

          Prm.ParamType := RWTypeToDDType(Q.GlobalVarFunc.Variables[i].VarType);
        end;
    end;


    // Delete removed fields
    for i := TrwDataDictTable(ddItem).Fields.Count - 1 downto 0 do
    begin
      SFld := QB.ShowingFields.FindField(TrwDataDictField(TrwDataDictTable(ddItem).Fields[i]).ExternalName);
      if not Assigned(SFld) then
        TrwDataDictTable(ddItem).DataDictionaryObject.DeleteField(TrwDataDictTable(ddItem).Fields[i]);
    end;

    // Add new fields and update old ones
    for i := 0 to QB.ShowingFields.Count - 1 do
    begin
      h := QB.ShowingFields[i].GetFieldName(False, False);
      Fld := TrwDataDictField(TrwDataDictTable(ddItem).Fields.FieldByName(h));
      if not Assigned(Fld) then
      begin
        Fld := TrwDataDictField(TrwDataDictTable(ddItem).Fields.AddField);
        Fld.Name := h;
        Fld.ExternalName := h;
        Fld.DisplayName := h;
      end;

      Fld.FieldType := RWTypeToDDType(QB.ShowingFields[i].ExprType);

      // Check for physical fields
      if (QB.ShowingFields[i].Count = 1) and (QB.ShowingFields[i][0].ItemType = eitField) then
      begin
        iFld := QB.ShowingFields[i][0].GetDDFieldInfo;
        if Assigned(iFld) then
        begin
          if iFld.FieldValues.Count > 0 then
            Fld.FieldValues.Text := cFieldValueRef + '=' + iFld.Table.Name + '.' + iFld.Name;
          Fld.Size := iFld.Size;
        end;
      end
      else
        Fld.Size := 0;
    end;

    TrwDDTableEditor(TableEditor).ddParamsEdit.OnShowItem;
    TrwDDTableEditor(TableEditor).ddFieldsEdit.OnShowItem;
  end;

begin
  Q := nil;
  QB := nil;
  
  if TrwDataDictTable(ddItem).TableType = rwtDDQuery then
  begin
    QB := TrwQBQuery.Create(nil);
    try
      if TrwDataDictTable(ddItem).QBInfo.Size > 0 then
      begin
        TrwDataDictTable(ddItem).QBInfo.Position := 0;
        QB.ReadFromStream(TrwDataDictTable(ddItem).QBInfo);
      end;

      if ShowQueryBuilder(QB, False) then
      begin
        TrwDataDictTable(ddItem).SQLText := QB.SQL;
        TrwDataDictTable(ddItem).QBInfo.Clear;
        QB.SaveToStream(TrwDataDictTable(ddItem).QBInfo);
        SyncFields;
      end;

    finally
      QB.Free;
    end;
  end

  else
  begin
    Q := TrwQuery.Create(nil);
    try
      SystemLibComponents[SystemLibComponents.IndexOf(TrwDataDictTable(ddItem).ExternalName)].CreateComp(Q);
      QB := Q.QueryBuilderInfo;
      SyncFields;
    finally
      FreeAndNil(Q);
    end;
  end;

  OnChange;
end;

procedure TrwDDTablePropEdit.cbTableTypeChange(Sender: TObject);
begin
  TrwDataDictTable(ddItem).TableType := TrwDDTableType(cbTableType.ItemIndex);
  // Make Cleanup
  OnChange;
end;

procedure TrwDDTablePropEdit.edExternalNameExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  TrwDataDictTable(ddItem).ExternalName := edExternalName.Text;
end;

procedure TrwDDTablePropEdit.edExternalNameChange(Sender: TObject);
begin
  OnChange;
end;

procedure TrwDDTablePropEdit.cbGroupNameChange(Sender: TObject);
begin
  OnChange;
end;

procedure TrwDDTablePropEdit.cbGroupNameExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  TrwDataDictTable(ddItem).GroupName := cbGroupName.Text;
end;

procedure TrwDDTablePropEdit.InitGroupBox;
var
  i: Integer;
  h: String;
begin
  cbGroupName.Clear;
  cbGroupName.Sorted := False;
  with TableEditor.DataDictionary.Tables do
    for i := 0  to Count - 1 do
    begin
      h := TrwDataDictTable(Items[i]).GroupName;
      if (h <> '') and (cbGroupName.Items.IndexOf(h) = -1) then
        cbGroupName.Items.Add(h);
    end;
  cbGroupName.Sorted := True;    
end;

end.
