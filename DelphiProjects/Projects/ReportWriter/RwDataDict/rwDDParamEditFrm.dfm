inherited rwDDParamEdit: TrwDDParamEdit
  Width = 272
  Height = 248
  inherited pcParamProp: TPageControl [0]
    Width = 272
    Height = 248
    inherited tsParamValues: TTabSheet
      Caption = 'Param Value'
      DesignSize = (
        264
        220)
      object lDefaultValue: TLabel [0]
        Left = 0
        Top = 196
        Width = 64
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Default Value'
      end
      object Label1: TLabel [1]
        Left = 0
        Top = 4
        Width = 81
        Height = 13
        Caption = 'Approved Values'
      end
      inherited vleValues: TValueListEditor
        Top = 22
        Width = 264
        Height = 162
        Align = alNone
        ColWidths = (
          47
          211)
      end
      object edDefaultValue: TEdit
        Left = 74
        Top = 194
        Width = 190
        Height = 21
        Anchors = [akLeft, akBottom]
        TabOrder = 1
        OnChange = edDefaultValueChange
        OnExit = edDefaultValueExit
      end
    end
  end
  inherited pnlContainer: TPanel [1]
    Width = 272
    Height = 248
    DesignSize = (
      272
      248)
    inherited lNotes: TLabel
      Top = 172
    end
    inherited edName: TEdit
      Width = 190
      Enabled = False
    end
    inherited edDisplayName: TEdit
      Width = 190
    end
    inherited memNotes: TMemo
      Top = 188
      Width = 272
      Height = 59
      TabOrder = 4
    end
    inherited ParamType: TDataDictDataTypes
      Width = 273
      inherited cbType: TComboBox
        Width = 191
        Enabled = False
      end
    end
    object grbRefField: TGroupBox
      Left = 0
      Top = 84
      Width = 272
      Height = 77
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Table Reference'
      TabOrder = 3
      DesignSize = (
        272
        77)
      object lRefTable: TLabel
        Left = 8
        Top = 22
        Width = 27
        Height = 13
        Caption = 'Table'
      end
      object lRefField: TLabel
        Left = 8
        Top = 49
        Width = 22
        Height = 13
        Caption = 'Field'
      end
      object cbRefTables: TComboBox
        Left = 49
        Top = 18
        Width = 215
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbRefTablesChange
        OnExit = cbRefTablesExit
      end
      object cbRefFields: TComboBox
        Left = 49
        Top = 45
        Width = 215
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbRefFieldsChange
        OnExit = cbRefFieldsExit
      end
    end
  end
end
