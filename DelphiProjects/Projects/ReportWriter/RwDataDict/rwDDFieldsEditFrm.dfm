inherited rwDDFieldsEdit: TrwDDFieldsEdit
  Width = 355
  Height = 383
  inherited pnlContainer: TPanel
    Width = 355
    Height = 383
    DesignSize = (
      355
      383)
    inline FieldInfo: TrwDDFieldEdit [0]
      Left = 0
      Top = 170
      Width = 355
      Height = 213
      Anchors = [akLeft, akRight, akBottom]
      AutoScroll = False
      TabOrder = 3
      inherited pnlContainer: TPanel
        Width = 355
        Height = 213
        Anchors = [akLeft, akRight, akBottom]
        inherited edName: TEdit
          Width = 272
          OnExit = FieldInfoedNameExit
        end
        inherited edDisplayName: TEdit
          Width = 272
        end
        inherited memNotes: TMemo
          Width = 355
          Height = 87
        end
        inherited edExternalName: TEdit
          Width = 272
        end
      end
      inherited pcFieldProp: TPageControl
        Width = 355
        Height = 213
      end
    end
    inherited lbCollection: TListBox [1]
      Width = 354
      Height = 158
      Align = alNone
      Anchors = [akLeft, akTop, akRight, akBottom]
    end
    inherited btnAddItem: TButton [2]
      Left = 196
      Top = 164
      Caption = 'Add Fields'
    end
    inherited btnDeleteItem: TButton
      Left = 280
      Top = 164
      Caption = 'Delete Field'
    end
  end
end
