unit rwDDTableEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictTableEditorFrm, StdCtrls, ComCtrls, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictTablePropEditFrm, rwDDTablePropEditFrm,
  DataDictCollectionEditFrm, rwDDParamsEditFrm, rwDDForeignKeyEditFrm,
  DataDictLogicalKeyEditFrm, DataDictPrimaryKeyEditFrm, rwDDFieldsEditFrm,
  DataDictDisplayLogicalKeyEditFrm;

type
  TrwDDTableEditor = class(TDataDictTableEditor)
    ddTablePropEdit: TrwDDTablePropEdit;
    grbTableParams: TGroupBox;
    ddParamsEdit: TrwDDParamsEdit;
    ddForeignKeyEdit: TrwDDForeignKeyEdit;
    ddPrimaryKeyEdit: TDataDictPrimaryKeyEdit;
    lPrimaryKey: TLabel;
    ddLogicalKeyEdit: TDataDictLogicalKeyEdit;
    lLogicalKey: TLabel;
    ddFieldsEdit: TrwDDFieldsEdit;
    Label1: TLabel;
    ddDisplayLogicalKeyEdit: TDataDictDisplayLogicalKeyEdit;
    procedure ddTablePropEditbtnQBClick(Sender: TObject);
  protected
    procedure PrepareToEdit; override;
  public
    procedure RefreshTableItem(const ATableItemType: TDataDictTableItemType); override;  
  end;

implementation

uses rwCustomDataDictionary;

{$R *.dfm}

{ TrwDDTableEditor }

procedure TrwDDTableEditor.PrepareToEdit;
begin
  ddTablePropEdit.DataDictionaryItem := ddTable;
  ddParamsEdit.DataDictionaryItem := ddTable.Params;
  ddFieldsEdit.DataDictionaryItem := ddTable.Fields;
  ddPrimaryKeyEdit.DataDictionaryItem := ddTable.PrimaryKey;
  ddLogicalKeyEdit.DataDictionaryItem := ddTable.LogicalKey;
  ddDisplayLogicalKeyEdit.DataDictionaryItem := ddTable.DisplayLogicalKey;  
  ddForeignKeyEdit.DataDictionaryItem := ddTable.ForeignKeys;
end;


procedure TrwDDTableEditor.RefreshTableItem(const ATableItemType: TDataDictTableItemType);
begin
  case ATableItemType of
    ddTITPrimaryKey:
      begin
        ddPrimaryKeyEdit.DataDictionaryItem := ddTable.PrimaryKey;
        ddLogicalKeyEdit.DataDictionaryItem := ddTable.LogicalKey;
        ddDisplayLogicalKeyEdit.DataDictionaryItem := ddTable.DisplayLogicalKey;
      end;

    ddForeignKeys:
      ddForeignKeyEdit.DataDictionaryItem := ddTable.ForeignKeys;
  end;
end;

procedure TrwDDTableEditor.ddTablePropEditbtnQBClick(Sender: TObject);
begin
  inherited;
  ddTablePropEdit.btnQBClick(Sender);

end;

end.
 