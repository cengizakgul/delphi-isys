inherited rwDDParamsEdit: TrwDDParamsEdit
  Width = 418
  Height = 248
  inherited pnlContainer: TPanel
    Width = 418
    Height = 248
    DesignSize = (
      418
      248)
    inherited lbCollection: TListBox
      Height = 248
      TabOrder = 3
    end
    inherited btnAddItem: TButton
      Left = 255
      Top = 227
      Caption = 'New Param'
      Visible = False
    end
    inherited btnDeleteItem: TButton
      Left = 339
      Top = 227
      Caption = 'Delete Param'
      Visible = False
    end
    inline ParamInfo: TrwDDParamEdit
      Left = 143
      Top = 0
      Width = 272
      Height = 248
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoScroll = False
      TabOrder = 0
      inherited pnlContainer: TPanel [0]
        inherited edName: TEdit
          OnChange = ParamInfoedNameChange
        end
      end
      inherited pcParamProp: TPageControl [1]
      end
    end
  end
end
