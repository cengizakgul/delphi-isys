unit rwDDFieldEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DataDictFieldEditFrm, Grids, ValEdit, ComCtrls,
  DataDictDataTypeFrm, StdCtrls, ExtCtrls, rwDataDictionary;

type
  TrwDDFieldEdit = class(TDataDictFieldEdit)
    edExternalName: TEdit;
    lExternalName: TLabel;
    procedure edExternalNameChange(Sender: TObject);
    procedure edExternalNameExit(Sender: TObject);
  private
  public
    procedure OnShowItem; override;
  end;

implementation

{$R *.dfm}

procedure TrwDDFieldEdit.edExternalNameChange(Sender: TObject);
begin
  OnChange;
end;

procedure TrwDDFieldEdit.edExternalNameExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  TrwDataDictField(ddItem).ExternalName := edExternalName.Text;
end;

procedure TrwDDFieldEdit.OnShowItem;
begin
  inherited;
  if ddItem <> nil then
    edExternalName.Text := TrwDataDictField(ddItem).ExternalName
  else
    edExternalName.Text := '';
end;

end.
