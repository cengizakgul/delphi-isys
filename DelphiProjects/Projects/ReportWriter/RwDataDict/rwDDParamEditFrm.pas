unit rwDDParamEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictParamEditFrm, Grids, ValEdit, ComCtrls,
  DataDictDataTypeFrm, StdCtrls, ExtCtrls, rwCustomDataDictionary,
  rwDataDictionary;

type
  TrwDDParamEdit = class(TDataDictParamEdit)
    lDefaultValue: TLabel;
    edDefaultValue: TEdit;
    Label1: TLabel;
    grbRefField: TGroupBox;
    lRefTable: TLabel;
    lRefField: TLabel;
    cbRefTables: TComboBox;
    cbRefFields: TComboBox;
    procedure cbRefTablesChange(Sender: TObject);
    procedure cbRefFieldsChange(Sender: TObject);
    procedure edDefaultValueChange(Sender: TObject);
    procedure edDefaultValueExit(Sender: TObject);
    procedure cbRefTablesExit(Sender: TObject);
    procedure cbRefFieldsExit(Sender: TObject);
  private
    procedure PrepareTableList;
    procedure PrepareFieldList;
  public
    procedure OnShowItem; override;
  end;

implementation

uses DataDictItemEditFrm, DataDictTableEditorFrm;

{$R *.dfm}

procedure TrwDDParamEdit.cbRefTablesChange(Sender: TObject);
begin
  OnChange;
  PrepareFieldList;
end;

procedure TrwDDParamEdit.cbRefFieldsChange(Sender: TObject);
begin
  OnChange;
end;

procedure TrwDDParamEdit.edDefaultValueChange(Sender: TObject);
begin
  OnChange;
end;

procedure TrwDDParamEdit.OnShowItem;
var
  T: TDataDictTable;
begin
  inherited;

  PrepareTableList;

  if ddItem <> nil then
  begin
    if Assigned(TrwDataDictParam(ddItem).RefField) then
    begin
      T := TDataDictTable(TDataDictFields(TrwDataDictParam(ddItem).RefField.Collection).Owner);
      cbRefTables.ItemIndex := cbRefTables.Items.IndexOfObject(T);
      cbRefTables.OnChange(cbRefTables);
      cbRefFields.ItemIndex := cbRefFields.Items.IndexOfObject(TrwDataDictParam(ddItem).RefField);
      cbRefFields.OnChange(cbRefFields);
    end
    else
    begin
      cbRefTables.ItemIndex := -1;
      cbRefTables.OnChange(cbRefTables);
      cbRefFields.ItemIndex := -1;
      cbRefFields.OnChange(cbRefFields);
    end;

    edDefaultValue.Text := TrwDataDictParam(ddItem).DefaultValue;
  end
  else
  begin
    cbRefTables.ItemIndex := -1;
    cbRefTables.OnChange(cbRefTables);
    cbRefFields.ItemIndex := -1;
    cbRefFields.OnChange(cbRefFields);
    edDefaultValue.Text := '';
  end;
end;

procedure TrwDDParamEdit.edDefaultValueExit(Sender: TObject);
begin
  OnExitEdit(Sender);
  TrwDataDictParam(ddItem).DefaultValue := edDefaultValue.Text;
end;

procedure TrwDDParamEdit.cbRefTablesExit(Sender: TObject);
begin
  cbRefFields.OnExit(cbRefFields);
end;

procedure TrwDDParamEdit.cbRefFieldsExit(Sender: TObject);
begin
  if cbRefFields.ItemIndex <> - 1 then
    TrwDataDictParam(ddItem).RefField := TDataDictField(cbRefFields.Items.Objects[cbRefFields.ItemIndex])
  else
    TrwDataDictParam(ddItem).RefField := nil;
end;

procedure TrwDDParamEdit.PrepareTableList;
var
  i: Integer;
  Tbls: TDataDictTables;
begin
  cbRefTables.Clear;

  if ddItem <> nil then
  begin
    cbRefTables.Sorted := False;
    Tbls :=  TableEditor.DataDictionary.Tables;
    for i := 0 to Tbls.Count - 1 do
      cbRefTables.AddItem(Tbls[i].Name, Tbls[i]);
    cbRefTables.Sorted := True;      
  end;

  cbRefTables.OnChange(cbRefTables);
end;

procedure TrwDDParamEdit.PrepareFieldList;
var
  i: Integer;
  Flds: TDataDictFields;
begin
  cbRefFields.Clear;

  if (ddItem <> nil) and (cbRefTables.ItemIndex <> -1) then
  begin
    cbRefFields.Sorted := False;
    Flds :=  TDataDictTable(cbRefTables.Items.Objects[cbRefTables.ItemIndex]).Fields;
    for i := 0 to Flds.Count - 1 do
      cbRefFields.AddItem(Flds[i].Name, Flds[i]);
    cbRefFields.Sorted := True;
  end;

  cbRefFields.OnChange(cbRefTables);
end;

end.
