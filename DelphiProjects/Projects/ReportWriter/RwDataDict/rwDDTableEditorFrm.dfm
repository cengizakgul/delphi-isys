inherited rwDDTableEditor: TrwDDTableEditor
  Width = 454
  Height = 560
  DesignSize = (
    454
    560)
  inherited pcTableProps: TPageControl
    Width = 454
    Height = 560
    inherited tsTableInfo: TTabSheet
      DesignSize = (
        446
        532)
      inline ddTablePropEdit: TrwDDTablePropEdit
        Left = 6
        Top = 6
        Width = 435
        Height = 206
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 435
          Height = 206
          inherited Label1: TLabel
            Left = 206
          end
          inherited edName: TEdit
            Width = 352
          end
          inherited edDisplayName: TEdit
            Width = 353
          end
          inherited memNotes: TMemo
            Top = 142
            Width = 435
          end
          inherited btnQB: TButton
            Left = 355
            OnClick = ddTablePropEditbtnQBClick
          end
          inherited edExternalName: TEdit
            Width = 352
          end
          inherited cbGroupName: TComboBox
            Left = 242
          end
        end
      end
      object grbTableParams: TGroupBox
        Left = 6
        Top = 219
        Width = 435
        Height = 277
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Table Parameters'
        TabOrder = 1
        DesignSize = (
          435
          277)
        inline ddParamsEdit: TrwDDParamsEdit
          Left = 10
          Top = 18
          Width = 418
          Height = 248
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoScroll = False
          TabOrder = 0
        end
      end
    end
    inherited tsFields: TTabSheet
      DesignSize = (
        446
        532)
      inline ddFieldsEdit: TrwDDFieldsEdit
        Left = 6
        Top = 6
        Width = 435
        Height = 483
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 435
          Height = 483
          inherited FieldInfo: TrwDDFieldEdit
            Top = 270
            Width = 435
            inherited pnlContainer: TPanel
              Width = 435
              inherited edName: TEdit
                Width = 352
              end
              inherited edDisplayName: TEdit
                Width = 352
              end
              inherited memNotes: TMemo
                Width = 435
              end
              inherited edExternalName: TEdit
                Width = 352
              end
            end
            inherited pcFieldProp: TPageControl
              Width = 435
            end
          end
          inherited lbCollection: TListBox
            Width = 434
            Height = 258
          end
          inherited btnAddItem: TButton
            Left = 276
            Top = 264
          end
          inherited btnDeleteItem: TButton
            Left = 360
            Top = 264
          end
        end
      end
    end
    inherited tsPrimaryKey: TTabSheet
      DesignSize = (
        446
        532)
      object lPrimaryKey: TLabel
        Left = 6
        Top = 6
        Width = 55
        Height = 13
        Caption = 'Primary Key'
      end
      object lLogicalKey: TLabel
        Left = 6
        Top = 162
        Width = 55
        Height = 13
        Caption = 'Logical Key'
      end
      object Label1: TLabel
        Left = 6
        Top = 315
        Width = 92
        Height = 13
        Caption = 'Display Logical Key'
      end
      inline ddPrimaryKeyEdit: TDataDictPrimaryKeyEdit
        Left = 6
        Top = 22
        Width = 440
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 440
          Height = 109
          DesignSize = (
            440
            109)
          inherited lbCollection: TListBox
            Width = 351
            Height = 109
          end
          inherited btnAddItem: TButton
            Left = 359
          end
          inherited btnDeleteItem: TButton
            Left = 359
          end
        end
      end
      inline ddLogicalKeyEdit: TDataDictLogicalKeyEdit
        Left = 7
        Top = 179
        Width = 440
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 1
        inherited pnlContainer: TPanel
          Width = 440
          Height = 109
          DesignSize = (
            440
            109)
          inherited lbCollection: TListBox
            Width = 351
            Height = 109
          end
          inherited btnAddItem: TButton
            Left = 359
          end
          inherited btnDeleteItem: TButton
            Left = 359
          end
        end
      end
      inline ddDisplayLogicalKeyEdit: TDataDictDisplayLogicalKeyEdit
        Left = 6
        Top = 333
        Width = 440
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 2
        inherited pnlContainer: TPanel
          Width = 440
          Height = 109
          DesignSize = (
            440
            109)
          inherited lbCollection: TListBox
            Width = 351
            Height = 109
          end
          inherited btnAddItem: TButton
            Left = 359
          end
          inherited btnDeleteItem: TButton
            Left = 359
          end
        end
      end
    end
    inherited tsForeignKey: TTabSheet
      DesignSize = (
        446
        532)
      inline ddForeignKeyEdit: TrwDDForeignKeyEdit
        Left = 6
        Top = 8
        Width = 435
        Height = 483
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 435
          Height = 483
          inherited lbCollection: TListBox
            Width = 351
            Height = 483
          end
          inherited btnAddItem: TButton
            Left = 359
          end
          inherited btnDeleteItem: TButton
            Left = 359
          end
          inherited btnEdit: TButton
            Left = 359
          end
        end
      end
    end
  end
  inherited btnCancel: TButton
    Left = 283
    Top = 529
    Height = 21
  end
  inherited btnOK: TButton
    Left = 198
    Top = 529
    Height = 21
  end
  inherited btnApply: TButton
    Left = 367
    Top = 529
    Height = 21
  end
end
