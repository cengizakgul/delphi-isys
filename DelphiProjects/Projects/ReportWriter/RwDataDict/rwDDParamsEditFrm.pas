unit rwDDParamsEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, StdCtrls, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictParamEditFrm, rwCustomDataDictionary,
  ExtCtrls, rwDDParamEditFrm, rwDataDictionary, rwEngineTypes;

type
  TrwDDParamsEdit = class(TDataDictCollectionEdit)
    ParamInfo: TrwDDParamEdit;
    procedure ParamInfoedNameExit(Sender: TObject);
    procedure ParamInfoedNameChange(Sender: TObject);
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
  public
    procedure AfterConstruction; override;
    procedure OnShowItem; override;
  end;

implementation

{$R *.dfm}

uses rwDDTableEditorFrm;

procedure TrwDDParamsEdit.AfterConstruction;
begin
  inherited;
  FItemFrame := ParamInfo;
end;


function TrwDDParamsEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictParam(AItem).Name;
end;

procedure TrwDDParamsEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
begin
  TDataDictParam(AItem).Name := 'New_Param';
  TDataDictParam(AItem).ParamType := ddtString;
end;

procedure TrwDDParamsEdit.ParamInfoedNameExit(Sender: TObject);
begin
  ParamInfo.edNameExit(Sender);
  UpdateItem(SelectedItem);
end;

procedure TrwDDParamsEdit.ParamInfoedNameChange(Sender: TObject);
begin
  inherited;
  ParamInfo.edNameChange(Sender);
  UpdateItem(SelectedItem);
end;

procedure TrwDDParamsEdit.OnShowItem;
begin
  inherited;
  ParamInfo.ParamType.cbType.Enabled := TrwDataDictTable(TrwDDTableEditor(TableEditor).ddTable).TableType = rwtDDQuery;
end;

end.
