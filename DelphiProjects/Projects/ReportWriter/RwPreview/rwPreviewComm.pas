unit rwPreviewComm;

interface

uses SysUtils, Windows, Messages, Forms, isBaseClasses, evTransportInterfaces,
     isBasicUtils, isThreadManager, rwPreviewApp, rwConnectionInt,
     EvStreamUtils, isLogFile, ISErrorUtils;

type
  TrwPreviewServer = class(TrwServer)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;

  TrwPreviewDatagramDispatcher = class(TrwServerDatagramDispatcher)
  private
    procedure dmQueryInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPreview(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPrint(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmReportsInGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmClearGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmAddToGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPreviewGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmPrintGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmMergeGroup(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFormBounds(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetFormBounds(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure DoOnConstruction; override;
    procedure RegisterHandlers; override;
  end;

  procedure PreviewSrv;

implementation

uses rwFormPreviewFrm;

var
  rwPreviewServer: IrwServer;
  FPreviewApp: IrwPreviewApp;  

procedure PreviewSrv;
var
  DummyMainForm: TForm;
begin
  SetUnhandledErrorSettings(uesLogAllShowNothing);
  try
    Application.ShowMainForm := False;
    Application.CreateForm(TForm, DummyMainForm);
    Application.CreateForm(TrwFormPreview, PreviewMainForm);
    PreviewMainForm.Preview.AutoConvertOldFormat := True;
    PreviewMainForm.Preview.StandAloneUtilityMode := True;
    PreviewMainForm.RunAsCOM := True;

    rwPreviewServer := TrwPreviewServer.Create;
  except
    on E: Exception do
    begin
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etFatalError, 'Server', E.Message, GetStack(E, ExceptAddr));
      raise;  
    end
  end;
end;


{ TrwPreviewServer }

function TrwPreviewServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TrwPreviewDatagramDispatcher.Create;
end;


{ TrwPreviewDatagramDispatcher }

procedure TrwPreviewDatagramDispatcher.dmAddToGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  ReportResultInfo: TrwReportResultInfo;
  PrintJobInfo: TrwPrintJobInfo;
begin
  ReportResultInfo.Name := ADatagram.Params.Value['AReportResultInfo.Name'];
  ReportResultInfo.FormatType := TrwReportResultFormatType(ADatagram.Params.Value['AReportResultInfo.FormatType']);
  ReportResultInfo.SecuredMode := ADatagram.Params.Value['AReportResultInfo.SecuredMode'];
  ReportResultInfo.PreviewPwd := ADatagram.Params.Value['AReportResultInfo.PreviewPwd'];
  ReportResultInfo.PrintPwd := ADatagram.Params.Value['AReportResultInfo.PrintPwd'];

  PrintJobInfo.PrintJobName := ADatagram.Params.Value['APrintJobInfo.PrintJobName'];
  PrintJobInfo.PrinterName := ADatagram.Params.Value['APrintJobInfo.PrinterName'];
  PrintJobInfo.TrayName := ADatagram.Params.Value['APrintJobInfo.TrayName'];
  PrintJobInfo.OutBinNbr := ADatagram.Params.Value['APrintJobInfo.OutBinNbr'];
  PrintJobInfo.DuplexingMode := TrwDuplexingMode(ADatagram.Params.Value['APrintJobInfo.DuplexingMode']);
  PrintJobInfo.PrinterVerticalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterVerticalOffset'];
  PrintJobInfo.PrinterHorizontalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterHorizontalOffset'];
  PrintJobInfo.FirstPage := ADatagram.Params.Value['APrintJobInfo.FirstPage'];
  PrintJobInfo.LastPage := ADatagram.Params.Value['APrintJobInfo.LastPage'];
  PrintJobInfo.Layers := ADatagram.Params.Value['APrintJobInfo.Layers'];
  PrintJobInfo.Copies := ADatagram.Params.Value['APrintJobInfo.Copies'];

  FPreviewApp.AddToGroup(IInterface(ADatagram.Params.Value['AReportResult']) as IevDualStream,
     ReportResultInfo, PrintJobInfo);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwPreviewDatagramDispatcher.dmClearGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FPreviewApp.ClearGroup;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwPreviewDatagramDispatcher.dmGetFormBounds(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure DoInMainThread(const Params: TTaskParamList);
  var
    Res: TrwBounds;
  begin
    Res := FPreviewApp.FormBounds;
    AResult.Params.AddValue(METHOD_RESULT + '.Left', Res.Left);
    AResult.Params.AddValue(METHOD_RESULT + '.Top', Res.Top);
    AResult.Params.AddValue(METHOD_RESULT + '.Width', Res.Width);
    AResult.Params.AddValue(METHOD_RESULT + '.Height', Res.Height);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(@DoInMainThread, MakeTaskParams([ADatagram, AResult]));
end;

procedure TrwPreviewDatagramDispatcher.dmMergeGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IEvDualStream;
begin
  Res := FPreviewApp.MergeGroup;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TrwPreviewDatagramDispatcher.dmPreview(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FPreviewApp.Preview(IInterface(ADatagram.Params.Value['AReportResult']) as IevDualStream,
      ADatagram.Params.Value['AParentWindow']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwPreviewDatagramDispatcher.dmPreviewGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FPreviewApp.PreviewGroup(ADatagram.Params.Value['AParentWindow']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwPreviewDatagramDispatcher.dmPrint(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    PrintJobInfo: TrwPrintJobInfo;
  begin
    PrintJobInfo.PrintJobName := ADatagram.Params.Value['APrintJobInfo.PrintJobName'];
    PrintJobInfo.PrinterName := ADatagram.Params.Value['APrintJobInfo.PrinterName'];
    PrintJobInfo.TrayName := ADatagram.Params.Value['APrintJobInfo.TrayName'];
    PrintJobInfo.OutBinNbr := ADatagram.Params.Value['APrintJobInfo.OutBinNbr'];
    PrintJobInfo.DuplexingMode := TrwDuplexingMode(ADatagram.Params.Value['APrintJobInfo.DuplexingMode']);
    PrintJobInfo.PrinterVerticalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterVerticalOffset'];
    PrintJobInfo.PrinterHorizontalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterHorizontalOffset'];
    PrintJobInfo.FirstPage := ADatagram.Params.Value['APrintJobInfo.FirstPage'];
    PrintJobInfo.LastPage := ADatagram.Params.Value['APrintJobInfo.LastPage'];
    PrintJobInfo.Layers := ADatagram.Params.Value['APrintJobInfo.Layers'];
    PrintJobInfo.Copies := ADatagram.Params.Value['APrintJobInfo.Copies'];

    FPreviewApp.Print(IInterface(ADatagram.Params.Value['AReportResult']) as IevDualStream,
      PrintJobInfo);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwPreviewDatagramDispatcher.dmPrintGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    PrintJobInfo: TrwPrintJobInfo;
  begin
    PrintJobInfo.PrintJobName := ADatagram.Params.Value['APrintJobInfo.PrintJobName'];
    PrintJobInfo.PrinterName := ADatagram.Params.Value['APrintJobInfo.PrinterName'];
    PrintJobInfo.TrayName := ADatagram.Params.Value['APrintJobInfo.TrayName'];
    PrintJobInfo.OutBinNbr := ADatagram.Params.Value['APrintJobInfo.OutBinNbr'];
    PrintJobInfo.DuplexingMode := TrwDuplexingMode(ADatagram.Params.Value['APrintJobInfo.DuplexingMode']);
    PrintJobInfo.PrinterVerticalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterVerticalOffset'];
    PrintJobInfo.PrinterHorizontalOffset := ADatagram.Params.Value['APrintJobInfo.PrinterHorizontalOffset'];
    PrintJobInfo.FirstPage := ADatagram.Params.Value['APrintJobInfo.FirstPage'];
    PrintJobInfo.LastPage := ADatagram.Params.Value['APrintJobInfo.LastPage'];
    PrintJobInfo.Layers := ADatagram.Params.Value['APrintJobInfo.Layers'];
    PrintJobInfo.Copies := ADatagram.Params.Value['APrintJobInfo.Copies'];

    FPreviewApp.PrintGroup(PrintJobInfo);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwPreviewDatagramDispatcher.dmQueryInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;
begin
  Res := FPreviewApp.QueryInfo(ADatagram.Params.Value['AItemName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TrwPreviewDatagramDispatcher.dmReportsInGroup(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Integer;
begin
  Res := FPreviewApp.ReportsInGroup;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TrwPreviewDatagramDispatcher.dmSetFormBounds(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwBounds;
  begin
    Res.Left := ADatagram.Params.Value['ABounds.Left'];
    Res.Top := ADatagram.Params.Value['ABounds.Top'];
    Res.Width := ADatagram.Params.Value['ABounds.Width'];
    Res.Height := ADatagram.Params.Value['ABounds.Height'];

    FPreviewApp.FormBounds := Res;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
//  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
  MethodProc(ADatagram, AResult);  // exception of rules! it uses PostMessage for this operation!
end;

procedure TrwPreviewDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FPreviewApp := TrwPreviewApp.Create;
end;

procedure TrwPreviewDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RWPREVIEW_QUERYINFO, dmQueryInfo);
  AddHandler(METHOD_RWPREVIEW_PREVIEW, dmPreview);
  AddHandler(METHOD_RWPREVIEW_PRINT, dmPrint);
  AddHandler(METHOD_RWPREVIEW_REPORTSINGROUP, dmReportsInGroup);
  AddHandler(METHOD_RWPREVIEW_CLEARGROUP, dmClearGroup);
  AddHandler(METHOD_RWPREVIEW_ADDTOGROUP, dmAddToGroup);
  AddHandler(METHOD_RWPREVIEW_PREVIEWGROUP, dmPreviewGroup);
  AddHandler(METHOD_RWPREVIEW_PRINTGROUP, dmPrintGroup);
  AddHandler(METHOD_RWPREVIEW_MERGEGROUP, dmMergeGroup);
  AddHandler(METHOD_RWPREVIEW_GETFORMBOUNDS, dmGetFormBounds);
  AddHandler(METHOD_RWPREVIEW_SETFORMBOUNDS, dmSetFormBounds);
end;

end.
