// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwPreviewUtils;

interface

uses Classes, Forms, Windows, Dialogs, Sysutils, Controls, rwPasswordFrm, ISErrorUtils;


procedure PrvInitCachePath;
function  PrvGetCachePath: String;
procedure PrvClearCachePath;

procedure PrvStartWait(AMessage: String; AMaxProgress: Integer = -1);
procedure PrvUpdateWait(AMessage: String; AProgress: Integer = -1);
procedure PrvEndWait;
procedure PrvHideWait;

function  PrvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                     DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word; overload;
procedure PrvMessage(const Msg: string; HelpCtx: Integer = 0); overload;

function  PrvUnAttended: Boolean;

function  PrvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
function  PrvPasswordDialog(const ACaption: String = ''; const APrompt: String = ''): string;

function  PrvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
                       AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;

function GetStack: string;

implementation

var
  FTempDir: String = '';



procedure PrvInitCachePath;

  function GetTmpDir: string;
  var
    Buf: array[0..2047] of char;
  begin
    if GetTempPath(High(Buf) - 1, @Buf) > 0 then
      Result := String(Buf)
    else
      Result := ExtractFilePath(Application.ExeName);
  end;

begin
  FTempDir := GetTmpDir + 'EvRwPreview'+ FormatDateTime('mm_dd_yy_hh_mm_ss', now) + PathDelim;
  CreateDir(FTempDir);
end;

function PrvGetCachePath: String;
begin
  Result := FTempDir;
end;

procedure PrvClearCachePath;

  function NormalDir(const DirName: string): string;
  begin
    Result := DirName;
    if (Result <> '') and
      not (AnsiLastChar(Result)^ in [':', '\']) then
    begin
      if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
        Result := Result + ':\'
      else
        Result := Result + '\';
    end;
  end;

  function ClearDir(const Path: string; Delete: Boolean = False): Boolean;
  const
    FileNotFound = 18;
  var
    FileInfo: TSearchRec;
    DosCode: Integer;
  begin
    Result := DirectoryExists(Path) or FileExists(Path);
    if not Result then Exit;
    DosCode := SysUtils.FindFirst(NormalDir(Path) + '*.*', faAnyFile, FileInfo);
    try
      while DosCode = 0 do
      begin
        if FileInfo.Name[1] <> '.' then
        begin
          if (FileInfo.Attr and faDirectory = faDirectory) then
            Result := ClearDir(NormalDir(Path) + FileInfo.Name, True) and Result
          else
            Result := Sysutils.DeleteFile(NormalDir(Path) + FileInfo.Name) and Result;
        end;
        DosCode := SysUtils.FindNext(FileInfo);
      end
    finally
      Sysutils.FindClose(FileInfo);
    end;
    if Delete and Result and (DosCode = FileNotFound) and
      not ((Length(Path) = 2) and (Path[2] = ':')) then
    begin
      Result := RemoveDir(Path);
    end;
  end;

begin
  ClearDir(FTempDir, True);
end;


procedure PrvStartWait(AMessage: String; AMaxProgress: Integer = -1);
begin
  Screen.Cursor := crHourGlass;
end;


procedure PrvUpdateWait(AMessage: String; AProgress: Integer = -1);
begin
end;


procedure PrvEndWait;
begin
  Screen.Cursor := crDefault;
end;


procedure PrvHideWait;
begin
  Screen.Cursor := crDefault;
end;


function PrvMessage(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons;
                     DefaultButton: TMsgDlgBtn = mbOK; HelpCtx: Integer = 0): Word;
begin
  Result := MessageDlg(Msg, DlgType, Buttons, HelpCtx);
end;


procedure PrvMessage(const Msg: string; HelpCtx: Integer = 0); overload;
begin
  MessageDlg(Msg, mtCustom, [mbOK], HelpCtx);
end;


function PrvUnAttended: Boolean;
begin
  Result := False;
end;

function PrvDefaultDialog(const ACaption, APrompt, ADefault: string): string;
begin
  Result := ADefault;
  InputQuery(ACaption, APrompt, Result);
end;


function PrvNbrDialog(const ACaption, APrompt: string; var Value: Extended;
 AIncrement: Extended = 1; AMinVal: Extended = 0; AMaxVal: Extended = 0): Boolean;
var
  h: String;
begin
  h := IntToStr(Trunc(Value));
  Result := InputQuery(ACaption, APrompt, h);
  Value := StrToInt(h);
end;

function  PrvPasswordDialog(const ACaption: String = ''; const APrompt: String = ''): string;
begin
  Result := PasswordDialog(ACaption, APrompt);
end;

function GetStack: string;
begin
  Result := ISErrorUtils.GetStack;
end;

end.
