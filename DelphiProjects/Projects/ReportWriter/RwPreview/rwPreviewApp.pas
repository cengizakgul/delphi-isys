unit rwPreviewApp;

interface

uses
  Windows, Classes, SysUtils, Controls, rwConnectionInt, rwFormPreviewFrm,
  rwEngineTypes, EvStreamUtils, isErrorUtils, isBasicUtils, isBaseClasses,
  rwBasicUtils, rwBasicPrint, rwPrintPage, rwTypes, isLogFile;

type
  TrwReportResultGroup  = class
  private
    FRWAInfoList: TrwRWAInfoList;
    FGroupType: TrwResultFormatType;
    procedure Add(const AReportResult: IevDualStream; const AReportResultInfo: TrwReportResultInfo;
                  const APrintJobInfo: TrwPrintJobInfo);
    procedure Clear;
    function  Count: Integer;
  end;


  TrwPreviewApp = class(TisInterfacedObject, IrwPreviewApp)
  private
    FrwReportResultGroup: TrwReportResultGroup;
    FPreviewInfo: TrwPreviewInfoRec;

    function  QueryInfo(const AItemName: String): String; 
    procedure Preview(const AReportResult: IevDualStream; const AParentWindow: Integer);
    procedure Print(const AReportResult: IevDualStream; const APrintJobInfo: TrwPrintJobInfo);
    function  ReportsInGroup: Integer;
    procedure ClearGroup;
    procedure AddToGroup(const AReportResult: IevDualStream; const AReportResultInfo: TrwReportResultInfo;
                         const APrintJobInfo: TrwPrintJobInfo);
    procedure PreviewGroup(const AParentWindow: Integer);
    procedure PrintGroup(const APrintJobInfo: TrwPrintJobInfo);
    function  MergeGroup: IevDualStream;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

implementation

function TranslatePrintJobInfoToNativeStruct(const OlePrintJobInfo: TrwPrintJobInfo): TrwPrintJobInfoRec;
var
  h: String;
begin
  with Result  do
  begin
    PrintJobName := OlePrintJobInfo.PrintJobName;
    PrinterName := OlePrintJobInfo.PrinterName;
    TrayName := OlePrintJobInfo.TrayName;
    OutBinNbr := OlePrintJobInfo.OutBinNbr;
    Duplexing := TrwDuplexPrintType(OlePrintJobInfo.DuplexingMode);
    PrintPCLAsGraphic := True;
    PrinterVerticalOffset := OlePrintJobInfo.PrinterVerticalOffset;
    PrinterHorizontalOffset := OlePrintJobInfo.PrinterHorizontalOffset;
    FirstPage := OlePrintJobInfo.FirstPage;
    LastPage := OlePrintJobInfo.LastPage;
    Copies := OlePrintJobInfo.Copies;
    Layers := [];

    h := OlePrintJobInfo.Layers;
    while h <> '' do
      Layers := Layers + [StrToInt(GetNextStrValue(h, ','))];
  end;
end;

{ TrwPreviewApp }

procedure TrwPreviewApp.AfterConstruction;
begin
  inherited;
  FrwReportResultGroup := TrwReportResultGroup.Create;
end;

procedure TrwPreviewApp.BeforeDestruction;
begin
  FrwReportResultGroup.Free;
  inherited;
end;

function TrwPreviewApp.QueryInfo(const AItemName: String): String;

  procedure AddValue(const AItem, AValue: String);
  begin
    if Result <> '' then
      Result := Result + #13;
    Result := Result + AItem + '=' + AValue;
  end;

begin
  Result := '';

  if AnsiSameText(AItemName, 'Version') then
    AddValue('Version', GetFileVersion(GetModuleName(0)));
end;

function TrwPreviewApp.GetFormBounds: TrwBounds;
var
  Frm: TControl;
begin
  if PreviewMainForm.Visible then
    Frm := PreviewMainForm
  else
    Frm := PreviewMainForm.Preview;

  with Frm do
  begin
    Result.Left := Left;
    Result.Top := Top;
    Result.Width := Width;
    Result.Height := Height;
  end;
end;

function TrwPreviewApp.MergeGroup: IevDualStream;

  procedure MakeRWA;
  var
    i, j, k: Integer;
    n: Cardinal;
    lHeader, H: TrwPrintHeader;
    FS: IEvDualStream;
    flDestroyStream: Boolean;
    S: TStream;
    M: TrwPrintMark;
    R: PTrwRWAInfoRec;
    lrs: TrwPrintableLayers;
    lTray: String;
    lOutBin: Integer;
  begin
    flDestroyStream := False;
    S := nil;

    FS := TEvDualStreamHolder.Create;

    lHeader := TrwPrintHeader.Create(nil);
    try
      lHeader.ReportType := rtReport;
      lHeader.PagesList.Clear;

      for i := 0 to FrwReportResultGroup.Count - 1 do
      begin
        R := @(FrwReportResultGroup.FRWAInfoList[i]);

        for j := 1 to R.PrintJobInfo.Copies do
        begin
          try
            if R^.Data4 = '' then
            begin
              if Assigned(R^.Data1) then
                S := R^.Data1.Data
              else if Assigned(R^.Data2) then
                S := R^.Data2
              else if Assigned(R^.Data3) then
                S := R^.Data3.RealStream;
            end
            else
            begin
              S := TEvFileStream.Create(R^.Data4, fmOpenRead);
              flDestroyStream := True;
            end;

            if R^.PrintJobInfo.Layers <> [] then
              lrs := R^.PrintJobInfo.Layers
            else
              lrs := [];

            if R^.PrintJobInfo.TrayName <> '' then
              lTray := R^.PrintJobInfo.TrayName
            else
              lTray := '';

            if R^.PrintJobInfo.OutBinNbr = 0 then
              lOutBin := R^.PrintJobInfo.OutBinNbr
            else
              lOutBin := 0;

            S.Position := S.Size - SizeOf(n);
            S.ReadBuffer(n, SizeOf(n));
            S.Position := n;
            H := TrwPrintHeader(S.ReadComponent(nil));

            try
              if H.PagesCount > 0 then
              begin
                for k := 0  to H.Marks.Count - 1 do
                begin
                  M := TrwPrintMark(lHeader.Marks.Add);
                  M.Assign(H.Marks[k]);
                  M.PageNum := M.PageNum + lHeader.PagesCount;
                end;

                for k := 0  to H.PagesCount - 1 do
                  lHeader.PagesList.Add(Pointer(Cardinal(H.PagesList[k]) + Cardinal(FS.Position)));

                for k := 0  to H.ReposList.Count - 1 do
                begin
                  lHeader.ReposList.Add(Pointer(Cardinal(H.ReposList[k]) + Cardinal(FS.Position)));
                  if lrs <> [] then
                    lHeader.LayersList.Add(Pointer(Byte((@lrs)^)))
                  else
                    if H.LayersList.Count > k then
                      lHeader.LayersList.Add(H.LayersList[k])
                    else
                      lHeader.LayersList.Add(Pointer(Byte((@lrs)^)));

                  if lTray <> '' then
                    lHeader.TraysList.Add(lTray)
                  else
                    if H.TraysList.Count > k then
                      lHeader.TraysList.Add(H.TraysList[k])
                    else
                      lHeader.TraysList.Add(lTray);

                  if lOutBin > 0 then
                    lHeader.OutBinsList.Add(Pointer(lOutBin))
                  else
                    if H.OutBinsList.Count > k then
                      lHeader.OutBinsList.Add(H.OutBinsList[k])
                    else
                      lHeader.OutBinsList.Add(Pointer(lOutBin));
                end;

                if lHeader.Version > H.Version then
                  lHeader.Version := H.Version;

                S.Position := 0;
                FS.RealStream.CopyFrom(S, n + 1);
              end;
            finally
              H.Free;
            end;

          finally
            if flDestroyStream then
            begin
              FreeAndNil(S);
              flDestroyStream := False;
            end;
          end;
        end;
      end;

      n := FS.Position;
      FS.WriteComponent(lHeader);
      FS.WriteBuffer(n, SizeOf(n));

      if lHeader.PagesCount > 0 then
        Result := FS;

    finally
      lHeader.Free;
    end;
  end;


  procedure MakeASCII;
  var
    i: Integer;
    FS: IEvDualStream;
    flDestroyStream: Boolean;
    S: TStream;
    R: PTrwRWAInfoRec;
  begin
    flDestroyStream := False;
    S := nil;

    FS := TEvDualStreamHolder.Create;

    for i := 0 to FrwReportResultGroup.Count - 1 do
    begin
      R := @(FrwReportResultGroup.FRWAInfoList[i]);

      try
        if R^.Data4 = '' then
        begin
          if Assigned(R^.Data1) then
            S := R^.Data1.Data
          else if Assigned(R^.Data2) then
            S := R^.Data2
          else if Assigned(R^.Data3) then
            S := R^.Data3.RealStream;
        end
        else
        begin
          S := TEvFileStream.Create(R^.Data4, fmOpenRead);
          flDestroyStream := True;
        end;

        S.Position := 0;
        FS.RealStream.CopyFrom(S, S.Size);

      finally
        if flDestroyStream then
        begin
          FreeAndNil(S);
          flDestroyStream := False;
        end;
      end;
    end;

    Result := FS;
  end;
 
begin
  Result := nil;

  if FrwReportResultGroup.FGroupType = rwRTRWA then
    MakeRWA

  else if FrwReportResultGroup.FGroupType = rwRTASCII then
    MakeASCII

  else
    raise ErwException.Create('Group merge operation supports only RWA and ASCII formats');
end;

procedure TrwPreviewApp.Preview(const AReportResult: IevDualStream; const AParentWindow: Integer);
begin
  if Assigned(AReportResult) then
  begin
    with FPreviewInfo do
    begin
      Caption := '';
      Owner := nil;
      Parent := 0;
      Reports := nil;
      SingleInstance := False;
      PreviewFrame := nil;
    end;

    SetLength(FPreviewInfo.Reports, 1);

    with FPreviewInfo.Reports[0] do
    begin
      Name := '';
      Data1 := nil;
      Data2 := nil;
      Data3 := nil;
      Data4 := '';
      SecuredMode := False;
      PreviewPswd := '';
      PrintPswd := '';
      with PrintJobInfo do
      begin
        PrintJobName := '';
        PrinterName := '';
        TrayName := '';
        OutBinNbr := 0;
        Duplexing := rdpDefault;
        PrintPCLAsGraphic := False;
        PrinterVerticalOffset := 0;
        PrinterHorizontalOffset := 0;
        FirstPage := 0;
        LastPage := 0;
        Layers := [];
        Copies := 0;
      end;
    end;

    FPreviewInfo.Reports[0].Data3 := AReportResult;

    PreviewMainForm.Preview.PreviewReport(@FPreviewInfo);

    if (AParentWindow <> 0) and IsWindow(AParentWindow) then
      PreviewMainForm.EmbedInto(AParentWindow)
    else
      PreviewMainForm.EmbedInto(0);
  end

  else
    PreviewMainForm.Close;
end;


procedure TrwPreviewApp.PreviewGroup(const AParentWindow: Integer);
begin
  with FPreviewInfo do
  begin
    Caption := '';
    Owner := nil;
    Parent := 0;
    Reports := nil;
    SingleInstance := False;
    PreviewFrame := nil;
  end;

  FPreviewInfo.Reports := FrwReportResultGroup.FRWAInfoList;

  PreviewMainForm.Preview.PreviewReport(@FPreviewInfo);

  if (AParentWindow <> 0) and IsWindow(AParentWindow) then
    PreviewMainForm.EmbedInto(AParentWindow)
  else
    PreviewMainForm.EmbedInto(0);
end;

procedure TrwPreviewApp.Print(const AReportResult: IevDualStream; const APrintJobInfo: TrwPrintJobInfo);
var
  PJ: TrwPrintJobInfoRec;
begin
  PJ := TranslatePrintJobInfoToNativeStruct(APrintJobInfo);
  PrintReport(AReportResult.RealStream, nil, @PJ, nil);
end;

procedure TrwPreviewApp.PrintGroup(const APrintJobInfo: TrwPrintJobInfo);
var
  Res: IEvDualStream;
  PJ: TrwPrintJobInfoRec;
begin
  Res := MergeGroup;

  if Assigned(Res) then
  begin
    PJ := TranslatePrintJobInfoToNativeStruct(APrintJobInfo);
    PrintReport(Res, nil, @PJ, nil);
  end;
end;

procedure TrwPreviewApp.SetFormBounds(const ABounds: TrwBounds);
begin
  if PreviewMainForm.Visible then
    PreviewMainForm.SetBoundsExt(ABounds)
  else
    PreviewMainForm.Preview.SetBoundsExt(ABounds);
end;

function TrwPreviewApp.ReportsInGroup: Integer;
begin
  Result := FrwReportResultGroup.Count;
end;

procedure TrwPreviewApp.ClearGroup;
begin
  FrwReportResultGroup.Clear;
end;

procedure TrwPreviewApp.AddToGroup(const AReportResult: IevDualStream;
  const AReportResultInfo: TrwReportResultInfo; const APrintJobInfo: TrwPrintJobInfo);
begin
  FrwReportResultGroup.Add(AReportResult, AReportResultInfo, APrintJobInfo);
end;

{ TrwReportResultGroup }
   
procedure TrwReportResultGroup.Add(const AReportResult: IevDualStream;
  const AReportResultInfo: TrwReportResultInfo; const APrintJobInfo: TrwPrintJobInfo);
begin
  SetLength(FRWAInfoList, Length(FRWAInfoList) + 1);

  with FRWAInfoList[High(FRWAInfoList)] do
  begin
    Name := AReportResultInfo.Name;
    Data1 := nil;
    Data2 :=  nil;
    Data3 := AReportResult;
    Data4 := '';
    SecuredMode := AReportResultInfo.SecuredMode;
    PreviewPswd := AReportResultInfo.PreviewPwd;
    PrintPswd := AReportResultInfo.PrintPwd;
  end;

  FRWAInfoList[High(FRWAInfoList)].PrintJobInfo := TranslatePrintJobInfoToNativeStruct(APrintJobInfo);

  if  Length(FRWAInfoList) = 1 then
    FGroupType := TrwResultFormatType(Ord(AReportResultInfo.FormatType))
  else if FGroupType <> TrwResultFormatType(Ord(AReportResultInfo.FormatType)) then
    FGroupType := rwRTUnknown;
end;

procedure TrwReportResultGroup.Clear;
begin
  SetLength(FRWAInfoList, 0);
  FGroupType := rwRTUnknown;
end;

function TrwReportResultGroup.Count: Integer;
begin
  Result := Length(FRWAInfoList);
end;

initialization
  SetGlobalLogFile(TLogFile.Create(ChangeFileExt(AppFileName, '.log')));

finalization
  SetGlobalLogFile(nil);

end.
