// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwCompareUtils;

interface

uses Classes, Windows, SysUtils, rwPrintPage, TypInfo, Controls, Forms,
     Graphics, rwPrintElements, rwUtils, rwEngine, rwGraphics, EvStreamUtils,
     isBaseClasses, IniFiles;

type

  IrwResultComparatorOptions = interface
  ['{6CF333B9-E2D7-4FA9-BDD9-9F78E4F9C9E5}']
    function  GetDeviation: integer;
    function  GetAttrAligment: boolean;
    function  GetAttrBoundLines: boolean;
    function  GetAttrBoundLinesColor: boolean;
    function  GetAttrBoundLinesStyle: boolean;
    function  GetAttrBoundLinesWidth: boolean;
    function  GetAttrColor: boolean;
    function  GetAttrFont: boolean;
    function  GetAttrLayout: boolean;
    function  GetAttrPCLtextFont: boolean;
    function  GetAttrText: boolean;
    function  GetAttrType: boolean;
    function  GetObjFrames: boolean;
    function  GetObjImages: boolean;
    function  GetObjShapes: boolean;
    function  GetObjTexts: boolean;
    function  GetPCLTexts: boolean;

    procedure SetDeviation(const Value: integer);
    procedure SetAttrAligment(const Value: boolean);
    procedure SetAttrBoundLines(const Value: boolean);
    procedure SetAttrBoundLinesColor(const Value: boolean);
    procedure SetAttrBoundLinesStyle(const Value: boolean);
    procedure SetAttrBoundLinesWidth(const Value: boolean);
    procedure SetAttrColor(const Value: boolean);
    procedure SetAttrFont(const Value: boolean);
    procedure SetAttrLayout(const Value: boolean);
    procedure SetAttrPCLtextFont(const Value: boolean);
    procedure SetAttrText(const Value: boolean);
    procedure SetAttrType(const Value: boolean);
    procedure SetObjFrames(const Value: boolean);
    procedure SetObjImages(const Value: boolean);
    procedure SetObjShapes(const Value: boolean);
    procedure SetObjTexts(const Value: boolean);
    procedure SetPCLTexts(const Value: boolean);

    property Deviation: integer read GetDeviation write SetDeviation;
    property ObjTexts: boolean read GetObjTexts write SetObjTexts;
    property ObjFrames: boolean read GetObjFrames write SetObjFrames;
    property ObjImages: boolean read GetObjImages write SetObjImages;
    property ObjShapes: boolean read GetObjShapes write SetObjShapes;
    property PCLTexts: boolean read GetPCLTexts write SetPCLTexts;
    property AttrText: boolean read GetAttrText write SetAttrText;
    property AttrAligment: boolean read GetAttrAligment write SetAttrAligment;
    property AttrLayout: boolean read GetAttrLayout write SetAttrLayout;
    property AttrFont: boolean read GetAttrFont write SetAttrFont;
    property AttrColor: boolean read GetAttrColor write SetAttrColor;
    property AttrBoundLines: boolean read GetAttrBoundLines write SetAttrBoundLines;
    property AttrBoundLinesColor: boolean read GetAttrBoundLinesColor write SetAttrBoundLinesColor;
    property AttrBoundLinesWidth: boolean read GetAttrBoundLinesWidth write SetAttrBoundLinesWidth;
    property AttrBoundLinesStyle: boolean read GetAttrBoundLinesStyle write SetAttrBoundLinesStyle;
    property AttrType: boolean read GetAttrType write SetAttrType;
    property AttrPCLtextFont: boolean read GetAttrPCLtextFont write SetAttrPCLtextFont;

    procedure LoadFromIniFile(const AFileName: String);
    procedure SaveToIniFile(const AFileName: String);
  end;

  TrwResultComparatorOptions = class(TisInterfacedObject, IrwResultComparatorOptions)
  private
    FDeviation: integer;
    FObjShapes: boolean;
    FAttrBoundLinesWidth: boolean;
    FObjImages: boolean;
    FAttrAligment: boolean;
    FAttrColor: boolean;
    FAttrPCLtextFont: boolean;
    FAttrFont: boolean;
    FAttrLayout: boolean;
    FObjTexts: boolean;
    FAttrBoundLinesStyle: boolean;
    FPCLTexts: boolean;
    FAttrBoundLinesColor: boolean;
    FAttrType: boolean;
    FAttrText: boolean;
    FAttrBoundLines: boolean;
    FObjFrames: boolean;

    function  GetDeviation: integer;
    function  GetAttrAligment: boolean;
    function  GetAttrBoundLines: boolean;
    function  GetAttrBoundLinesColor: boolean;
    function  GetAttrBoundLinesStyle: boolean;
    function  GetAttrBoundLinesWidth: boolean;
    function  GetAttrColor: boolean;
    function  GetAttrFont: boolean;
    function  GetAttrLayout: boolean;
    function  GetAttrPCLtextFont: boolean;
    function  GetAttrText: boolean;
    function  GetAttrType: boolean;
    function  GetObjFrames: boolean;
    function  GetObjImages: boolean;
    function  GetObjShapes: boolean;
    function  GetObjTexts: boolean;
    function  GetPCLTexts: boolean;

    procedure SetDeviation(const Value: integer);
    procedure SetAttrAligment(const Value: boolean);
    procedure SetAttrBoundLines(const Value: boolean);
    procedure SetAttrBoundLinesColor(const Value: boolean);
    procedure SetAttrBoundLinesStyle(const Value: boolean);
    procedure SetAttrBoundLinesWidth(const Value: boolean);
    procedure SetAttrColor(const Value: boolean);
    procedure SetAttrFont(const Value: boolean);
    procedure SetAttrLayout(const Value: boolean);
    procedure SetAttrPCLtextFont(const Value: boolean);
    procedure SetAttrText(const Value: boolean);
    procedure SetAttrType(const Value: boolean);
    procedure SetObjFrames(const Value: boolean);
    procedure SetObjImages(const Value: boolean);
    procedure SetObjShapes(const Value: boolean);
    procedure SetObjTexts(const Value: boolean);
    procedure SetPCLTexts(const Value: boolean);
  protected
    procedure DoOnConstruction; override;

  public
    property Deviation: integer read GetDeviation write SetDeviation;

    property ObjTexts: boolean read FObjTexts write SetObjTexts;
    property ObjFrames: boolean read FObjFrames write SetObjFrames;
    property ObjImages: boolean read FObjImages write SetObjImages;
    property ObjShapes: boolean read FObjShapes write SetObjShapes;
    property PCLTexts: boolean read FPCLTexts write SetPCLTexts;

    property AttrText: boolean read FAttrText write SetAttrText;
    property AttrAligment: boolean read FAttrAligment write SetAttrAligment;
    property AttrLayout: boolean read FAttrLayout write SetAttrLayout;
    property AttrFont: boolean read FAttrFont write SetAttrFont;
    property AttrColor: boolean read FAttrColor write SetAttrColor;
    property AttrBoundLines: boolean read FAttrBoundLines write SetAttrBoundLines;
    property AttrBoundLinesColor: boolean read FAttrBoundLinesColor write SetAttrBoundLinesColor;
    property AttrBoundLinesWidth: boolean read FAttrBoundLinesWidth write SetAttrBoundLinesWidth;
    property AttrBoundLinesStyle: boolean read FAttrBoundLinesStyle write SetAttrBoundLinesStyle;
    property AttrType: boolean read FAttrType write SetAttrType;
    property AttrPCLtextFont: boolean read FAttrPCLtextFont write SetAttrPCLtextFont;

    procedure LoadFromIniFile(const AFileName: String);
    procedure SaveToIniFile(const AFileName: String);

    destructor Destroy; override;
  end;

  TrwCustomResultComparator = class
  private
    FResFileName: String;
    FWaitText:  String;
  public
    constructor Create(const ARefRWFile, ARWFile: string); virtual;
    function    Compare: Boolean; virtual; abstract;

    property    ResFileName: String read FResFileName;
    property    WaitText: String read FWaitText;
  end;


  TrwResultComparator = class (TrwCustomResultComparator)
  private
    FRefStream: TEvFileStream;
    FStream:    TEvFileStream;
    FResStream: TEvFileStream;
    FRefPage:   TrwVirtualPage;
    FPage:      TrwVirtualPage;
    FResPage:   TrwVirtualPage;
    FRWACompareOptions: IrwResultComparatorOptions;

    function  ComparePages: Boolean;
    function  CompareControls(ARefControl, AControl: TrwPrintObject): string;
    procedure CreateMark(ABounds: TRect; AText: string);
    procedure WritePages;
    function  ComparePrintObj(ARefControl, AControl: TrwPrintObject): string;
    function  CompareTexts(ARefLabel, ALabel: TrwPrintText): string;
    function  CompareFrames(ARefFrame, AFrame: TrwPrintFrame): string;
    function  CompareImages(ARefImage, AImage: TrwPrintImage): string;
    function  CompareShape(ARefShape, AShape: TrwPrintShape): string;
    function  ComparePCLTexts(ARefLabel, ALabel: TrwPrintPCLText): string;
    procedure CreateInfoLabel(APos: TPoint; AText: String; AFontSize: Integer; APage: TrwVirtualPage);

  public
    procedure   SetRWACompareOptions(const ACompareOptions: IrwResultComparatorOptions);
    constructor Create(const ARefRWFile, ARWFile: string); override;
    destructor  Destroy; override;
    function    Compare: boolean; override;
  end;


procedure CompareASCII(const AFileRef, AFileComp: String);

implementation

{$R examdiff.res}

procedure CompareASCII(const AFileRef, AFileComp: String);
var
  StartupInfo: TStartupInfo;
  SA: TSecurityAttributes;
  P: TProcessInformation;
  h: String;
  Stream: TResourceStream;
begin
  h := GetTmpDir+ 'examdiff.exe';
  if not FileExists(h) then
  begin
    Stream := TResourceStream.Create(hInstance, 'examdiff', 'BINARY');
    try
      Stream.SaveToFile(h);
    finally
      Stream.Free;
    end;
  end;
  //h := ExtractFilePath(Application.ExeName) + 'ExamDiff.exe';
  if not FileExists(h) then
    raise Exception.Create('Comparing utility "' + h + '" in not found');

  h := h + ' "' + AFileRef + '" "' + AFileComp + '" /t /n';
  FillChar(SA, SizeOf(TSecurityAttributes), 0);
  SA.nLength:=SizeOf(SA);
  SA.lpSecurityDescriptor:=nil;
  SA.bInheritHandle:=TRUE;

  FillChar(StartupInfo, SizeOf(TStartupInfo), 0);
  StartupInfo.cb:=SizeOf(TStartupInfo);
  StartupInfo.lpDesktop:=nil;
  StartupInfo.dwFlags:=STARTF_USESHOWWINDOW;
  StartupInfo.wshowWindow:=SW_SHOW;

  if not CreateProcess(nil, PChar(h), nil, @SA, True, NORMAL_PRIORITY_CLASS , nil, nil, StartupInfo, P) then
    raise Exception.Create('Unexpected file compare error: ' + SysErrorMessage(GetLastError));

  if WaitForSingleObject(P.hProcess, IGNORE) = WAIT_FAILED then
    raise Exception.Create('Unexpected file compare error: ' + SysErrorMessage(GetLastError));
end;


{ TrwResultComparator }

constructor TrwResultComparator.Create(const ARefRWFile, ARWFile: string);
begin
  inherited;

  FRefStream := TEvFileStream.Create(ARefRWFile, fmOpenRead or fmShareDenyNone);
  FStream := TEvFileStream.Create(ARWFile, fmOpenRead or fmShareDenyNone);
  FRefPage := TrwVirtualPage.Create(nil);
  FRefPage.Initialize(FRefStream);
  FPage := TrwVirtualPage.Create(nil);
  FPage.Initialize(FStream);

  FRWACompareOptions := TrwResultComparatorOptions.Create;
end;


destructor TrwResultComparator.Destroy;
begin
  FRefPage.Free;
  FPage.Free;
  FRefStream.Free;
  FStream.Free;
  inherited;
end;


function TrwResultComparator.Compare: Boolean;
var
  i: Integer;
  h: String;
begin
  Result := False;

  FResStream := TEvFileStream.Create(FResFileName, fmCreate);
  FResPage := TrwVirtualPage.Create(nil);
  try
    FResPage.Header.ReportType := FRefPage.Header.ReportType;
    FResPage.Header.PixelsPerInch := FRefPage.PixelsPerInch;

    if FRefPage.Header.PagesCount <> FPage.Header.PagesCount then
    begin
      if FRefPage.Header.PagesCount > 0 then
        FRefPage.PreparePage(1, FRefStream)
      else
        CreateInfoLabel(Point(25, 25), 'EMPTY REPORT', 8, FRefPage);

      if FPage.Header.PagesCount > 0 then
      begin
        FPage.PreparePage(1, FStream);
        h := 'PAGE NUMBERS ARE DIFFERENT';
      end
      else
        h := 'EMPTY REPORT';

      CreateInfoLabel(Point(25, 25), h, 8, FPage);
      WritePages;
      Exit;
    end;

    RWEngineExt.AppAdapter.StartEngineProgressBar(FWaitText, FRefPage.Header.PagesCount);
    try
      for i := 1 to FRefPage.Header.PagesCount do
      begin
        FRefPage.PreparePage(i, FRefStream);
        FPage.PreparePage(i, FStream);

        if (FRefPage.PageSize <> FPage.PageSize) or
           (FRefPage.PageOrientation <> FPage.PageOrientation) or
           (FRefPage.Width <> FPage.Width) or
           (FRefPage.Height <> FPage.Height) then
        begin
          CreateInfoLabel(Point(25, 25), 'Page formats do not match', 8, FPage);
          WritePages;
        end

        else if ComparePages then
          WritePages;

         RWEngineExt.AppAdapter.UpdateEngineProgressBar(FWaitText, i);
      end;
    finally
      RWEngineExt.AppAdapter.EndEngineProgressBar;
    end;

  finally
    try
      if FResPage.Header.PagesCount = 0 then
      begin
        FResStream.Free;
        FResStream := nil;
        Result := True;
        DeleteFile(FResFileName);
      end

      else
      begin
        FResStream.Position := FResStream.Size;
        FResPage.Header.ReposList.Add(Pointer(FResStream.Position));
        FResStream.WriteComponent(FResPage.Repository);
        FResPage.WriteHeaderInfo(FResStream);
      end;
    finally
      FResStream.Free;
      FResPage.Free;
    end;
  end;
end;


function TrwResultComparator.ComparePages: Boolean;
var
  d, dm, j, i: Integer;
  C, Cref: TrwPrintObject;
  n: Word;
  Res: string;
  L: TList;
  Ccomp, P: Integer;

  function PCompare(Item1, Item2: Pointer): Integer;
  begin
    Result := Integer(Item1) - Integer(Item2);
  end;

begin
  Result := False;

  L := TList.Create;
  try
    FRefPage.PageData.Position := 0;

    while FRefPage.PageData.Position < FRefPage.PageData.Size do
    begin
      FRefPage.PageData.ReadBuffer(n, SizeOf(n));
      Cref := TrwPrintObject(FRefPage.Repository.Components[n]);
      Cref.PrepareToPaint(FRefPage.PageData);
      if not Cref.Compare then
        continue;

      dm := MaxInt;
      FPage.PageData.Position := 0;
      j := 0;
      i := 0;
      Ccomp := -1;
      while FPage.PageData.Position < FPage.PageData.Size do
      begin
        Inc(j);
        P := FPage.PageData.Position;
        FPage.PageData.ReadBuffer(n, SizeOf(n));
        C := TrwPrintObject(FPage.Repository.Components[n]);
        C.PrepareToPaint(FPage.PageData);

        if Cref.ClassInfo = C.ClassInfo then
        begin
          d := Abs(C.Left - Cref.Left) +
               Abs(C.Top - Cref.Top) +
               Abs(C.Right - Cref.Right) +
               Abs(C.Bottom - Cref.Bottom);

          if d < dm then
          begin
            Ccomp := P;
            i := j;
            dm := d;
          end;
        end;
      end;

      if (Ccomp = -1) or (dm > FRWACompareOptions.Deviation) then
        C := nil
      else
      begin
        FPage.PageData.Position := Ccomp;
        FPage.PageData.ReadBuffer(n, SizeOf(n));
        C := TrwPrintObject(FPage.Repository.Components[n]);
        C.PrepareToPaint(FPage.PageData);
      end;

      if Assigned(C) then
      begin
        Res := CompareControls(Cref, C);
        if L.Count < j then
          L.Count := j;
        L[i - 1] := Pointer(i);
        FResPage.PageNum := FResPage.Header.PagesCount + 1;
      end
      else
      begin
        Res := 'Element is absent';
        FResPage.PageNum := FResPage.Header.PagesCount + 2;
      end;

      if Length(Res) <> 0 then
      begin
        CreateMark(Rect(Cref.Left, Cref.Top, Cref.Right, Cref.Bottom), Res);
        Result := True;
      end;
    end;

    FPage.PageData.Position := 0;
    j := 0;
    L.Sort(@PCompare);
    FResPage.PageNum := FResPage.Header.PagesCount + 1;
    while FPage.PageData.Position < FPage.PageData.Size do
    begin
      Inc(j);
      FPage.PageData.ReadBuffer(n, SizeOf(n));
      C := TrwPrintObject(FPage.Repository.Components[n]);
      C.PrepareToPaint(FPage.PageData);
      if not C.Compare then
        continue;
      if (C is TrwPrintText) and  not FRWACompareOptions.ObjTexts then
        continue;
      if (C is TrwPrintFrame) and not FRWACompareOptions.ObjFrames then
        continue;
      if (C is TrwPrintImage) and not FRWACompareOptions.ObjImages then
        continue;
      if (C is TrwPrintShape) and not FRWACompareOptions.ObjShapes then
        continue;
      if (C is TrwPrintPCLText) and not FRWACompareOptions.PCLTexts then
        continue;

      if L.IndexOf(Pointer(j)) = -1 then
      begin
        CreateMark(Rect(C.Left, C.Top, C.Right, C.Bottom), 'Element is odd');
        Result := True;
      end;
    end;

  finally
    L.Free;
  end;
end;


function TrwResultComparator.CompareControls(ARefControl, AControl: TrwPrintObject): string;
begin
  Result := '';

  if (ARefControl is TrwPrintText) and FRWACompareOptions.ObjTexts then
    Result := CompareTexts(TrwPrintText(ARefControl), TrwPrintText(AControl))
  else if (ARefControl is TrwPrintFrame) and FRWACompareOptions.ObjFrames then
    Result := CompareFrames(TrwPrintFrame(ARefControl), TrwPrintFrame(AControl))
  else if (ARefControl is TrwPrintImage) and FRWACompareOptions.ObjImages then
    Result := CompareImages(TrwPrintImage(ARefControl), TrwPrintImage(AControl))
  else if (ARefControl is TrwPrintShape) and FRWACompareOptions.ObjShapes then
    Result := CompareShape(TrwPrintShape(ARefControl), TrwPrintShape(AControl))
  else if (ARefControl is TrwPrintPCLText) and FRWACompareOptions.PCLTexts then
    Result := ComparePCLTexts(TrwPrintPCLText(ARefControl), TrwPrintPCLText(AControl));
end;


procedure TrwResultComparator.CreateMark(ABounds: TRect; AText: string);
var
  x1,y1, x2,y2, d: Integer;
begin
  x1 := ABounds.Left - 7;
  y1 := ABounds.Top - 7;
  x2 := ABounds.Right + 7;
  y2 := ABounds.Bottom + 7;

  if x2 - x1 < 20 then
  begin
    d := (20 - (x2 - x1)) div 2;
    x1 := x1 - d;
    x2 := x2 + d;
  end;

  if y2 - y1 < 20 then
  begin
    d := (20 - (y2 - y1)) div 2;
    y1 := y1 - d;
    y2 := y2 + d;
  end;

  FResPage.CreateMark(x1, y1, x2 - x1, y2 - y1, AText);
end;



procedure TrwResultComparator.WritePages;

  procedure WritePage(APage: TrwVirtualPage);
  var
    n: Word;
    C: TrwPrintObject;
  begin
    FResPage.Width := APage.Width;
    FResPage.Height := APage.Height;
    FResPage.PageSize := APage.PageSize;
    FResPage.PageOrientation := APage.PageOrientation;
    FResPage.Duplexing := False;

    FResPage.Data.Size := 0;
    APage.PageData.Position := 0;
    while APage.PageData.Position < APage.PageData.Size do
    begin
      APage.PageData.ReadBuffer(n, SizeOf(n));
      C := TrwPrintObject(APage.Repository.Components[n]);
      C.PrepareToPaint(APage.PageData);
      StorePrintObject(C, FResPage);
    end;
  end;

begin
  WritePage(FPage);
  FResPage.Header.PagesList.Add(Pointer(FResStream.Position));
  FResStream.WriteComponent(FResPage);
  WritePage(FRefPage);
  CreateInfoLabel(Point(25, 25), 'reference', 8, FResPage);
  FResPage.Header.PagesList.Add(Pointer(FResStream.Position));
  FResStream.WriteComponent(FResPage);
end;


function TrwResultComparator.CompareTexts(ARefLabel, ALabel: TrwPrintText): string;
begin
  Result := ComparePrintObj(ARefLabel, ALabel);

  if FRWACompareOptions.AttrText then
    if not AnsiSameStr(ARefLabel.Text, ALabel.Text) then
      Result := Result + 'Text'#13;

  if FRWACompareOptions.AttrAligment then
    if (ARefLabel.Layout <> ALabel.Layout) or
       (ARefLabel.Alignment <> ALabel.Alignment) then
      Result := Result + 'Alignment'#13;

  if FRWACompareOptions.AttrLayout then
    if (ARefLabel.Layout <> ALabel.Layout) then
      Result := Result + 'Layout'#13;

  if FRWACompareOptions.AttrFont then
    if (ARefLabel.Font.Name <> ALabel.Font.Name) or
       (ARefLabel.Font.Size <> ALabel.Font.Size) or
       (ARefLabel.Font.Style <> ALabel.Font.Style) or
       (ARefLabel.Font.Color <> ALabel.Font.Color) then
      Result := Result + 'Font'#13;

  if FRWACompareOptions.AttrColor then
    if (ARefLabel.Color <> ALabel.Color) then
      Result := Result + 'Color'#13;

  if Length(Result) > 0 then
  begin
    Delete(Result, Length(Result), 1);
    Result := 'LABEL:'#13 + Result;
  end;
end;


procedure TrwResultComparator.CreateInfoLabel(APos: TPoint; AText: String; AFontSize: Integer; APage: TrwVirtualPage);
var
  C: TrwPrintText;
  k: Extended;
begin
  C := TrwPrintText.Create(nil);
  try
    k := APage.PixelsPerInch / cScreenCanvasRes;

    C.Left := Round(APos.X * k);
    C.Top := Round(APos.Y * k);
    C.Right := Round((C.Left + 200) * k);
    C.Bottom := Round((C.Top + 20) * k);
    C.Transparent := True;
    C.Text := AText;
    C.Font.Name := 'Arial';
    C.Font.Size := Round(AFontSize * k);
    C.Font.Color := clRed;
    APage.Data.Position := APage.Data.Size;
    StorePrintObject(C, APage);
  finally
    C.Free;
  end;
end;


function TrwResultComparator.CompareFrames(ARefFrame, AFrame: TrwPrintFrame): string;
begin
  Result := ComparePrintObj(ARefFrame, AFrame);

  if FRWACompareOptions.AttrBoundLines then
    if (ARefFrame.BoundLines <> AFrame.BoundLines) then
      Result := Result + 'Bound lines'#13;

  if FRWACompareOptions.AttrBoundLinesColor then
    if (ARefFrame.BoundLinesColor <> AFrame.BoundLinesColor) then
      Result := Result + 'Bound lines color'#13;

  if FRWACompareOptions.AttrBoundLinesWidth then
    if (ARefFrame.BoundLinesWidth <> AFrame.BoundLinesWidth) then
      Result := Result + 'Bound lines width'#13;

  if FRWACompareOptions.AttrBoundLinesStyle then
    if (ARefFrame.BoundLinesStyle <> AFrame.BoundLinesStyle) then
      Result := Result + 'Bound lines style'#13;

  if Length(Result) > 0 then
  begin
    Delete(Result, Length(Result), 1);
    Result := 'FRAME:'#13 + Result;
  end;
end;


function TrwResultComparator.CompareImages(ARefImage, AImage: TrwPrintImage): string;
var
  MS1, MS2: TMemoryStream;
begin
  Result := ComparePrintObj(ARefImage, AImage);

  if (ARefImage.Picture.Bitmap.Height <> AImage.Picture.Bitmap.Height) or
     (ARefImage.Picture.Bitmap.Width <> AImage.Picture.Bitmap.Width) then
    Result := Result + 'Content'#13
  else
  begin
    MS1 := TIsMemoryStream.Create;
    MS2 := TIsMemoryStream.Create;
    try
      ARefImage.Picture.Bitmap.SaveToStream(MS1);
      AImage.Picture.Bitmap.SaveToStream(MS2);
      if not CompareMem(MS1.Memory, MS2.Memory, MS1.Size) then
        Result := Result + 'Content'#13;
    finally
      MS1.Free;
      MS2.Free;
    end;
  end;

  if Length(Result) > 0 then
  begin
    Delete(Result, Length(Result), 1);
    Result := 'IMAGE:'#13 + Result;
  end;
end;


function TrwResultComparator.CompareShape(ARefShape, AShape: TrwPrintShape): string;
begin
  Result := ComparePrintObj(ARefShape, AShape);

  if FRWACompareOptions.AttrType then
    if (ARefShape.ShapeType <> AShape.ShapeType) then
      Result := Result + 'Type'#13;

  if FRWACompareOptions.AttrColor then
    if (ARefShape.Color <> AShape.Color) then
      Result := Result + 'Color'#13;

  if FRWACompareOptions.AttrBoundLinesColor then
    if (ARefShape.BoundLinesColor <> AShape.BoundLinesColor) then
      Result := Result + 'Bound lines color'#13;

  if FRWACompareOptions.AttrBoundLinesWidth then
    if (ARefShape.BoundLinesWidth <> AShape.BoundLinesWidth) then
      Result := Result + 'Bound lines width'#13;

  if FRWACompareOptions.AttrBoundLinesStyle then
    if (ARefShape.BoundLinesStyle <> AShape.BoundLinesStyle) then
      Result := Result + 'Bound lines style'#13;

  if Length(Result) > 0 then
  begin
    Delete(Result, Length(Result), 1);
    Result := 'Shape:'#13 + Result;
  end;
end;

function TrwResultComparator.ComparePCLTexts(ARefLabel, ALabel: TrwPrintPCLText): string;
begin
  Result := ComparePrintObj(ARefLabel, ALabel);

  if FRWACompareOptions.AttrText then
    if not AnsiSameStr(ARefLabel.Text, ALabel.Text) then
      Result := Result + 'Text'#13;

  if FRWACompareOptions.AttrPCLtextFont then
    if CompareStr(ARefLabel.PCLFont.Data, ALabel.PCLFont.Data) <> 0 then
      Result := Result + 'PCL Font'#13;

  if Length(Result) > 0 then
  begin
    Delete(Result, Length(Result), 1);
    Result := 'PCL LABEL:'#13 + Result;
  end;
end;

function TrwResultComparator.ComparePrintObj(ARefControl, AControl: TrwPrintObject): string;
begin
  Result := '';

  if (ARefControl.Top <> AControl.Top) or
     (ARefControl.Bottom <> AControl.Bottom) or
     (ARefControl.Left <> AControl.Left) or
     (ARefControl.Right <> AControl.Right) then
    Result := Result + 'Position'#13;

  if (ARefControl.Transparent <> AControl.Transparent) then
    Result := Result + 'Transparency'#13;

  if (ARefControl.LayerNumber <> AControl.LayerNumber) then
    Result := Result + 'Layer'#13;
end;


procedure TrwResultComparator.SetRWACompareOptions(const ACompareOptions: IrwResultComparatorOptions);
begin
  if ACompareOptions = nil then
    raise Exception.Create('RWA compare options cannot be nil');
  FRWACompareOptions := ACompareOptions;
end;

{ TrwCustomResultComparator }

constructor TrwCustomResultComparator.Create(const ARefRWFile, ARWFile: string);
var
  i: Integer;
  h: String;
begin
  FWaitText := ExtractFileName(ARWFile) + '  and  ' + ExtractFileName(ARWFile);

  i := 0;
  h := ExtractFileExt(ARefRWFile);
  repeat
    Inc(i);
    FResFileName := GetSysTempDir + '~file_compare' + IntToStr(i) + h;
  until not FileExists(FResFileName);
end;

{ TrwResultComparatorOptions }

destructor TrwResultComparatorOptions.Destroy;
begin

  inherited;
end;

procedure TrwResultComparatorOptions.DoOnConstruction;
begin
  inherited;
  FDeviation := 10;
  FObjShapes := true;
  FAttrBoundLinesWidth := true;
  FObjImages := true;
  FAttrAligment := true;
  FAttrColor := true;
  FAttrPCLtextFont := true;
  FAttrFont := true;
  FAttrLayout := true;
  FObjTexts := true;
  FAttrBoundLinesStyle := true;
  FPCLTexts := true;
  FAttrBoundLinesColor := true;
  FAttrType := true;
  FAttrText := true;
  FAttrBoundLines := true;
  FObjFrames := true;
end;

function TrwResultComparatorOptions.GetAttrAligment: boolean;
begin
  Result := FAttrAligment;
end;

function TrwResultComparatorOptions.GetAttrBoundLines: boolean;
begin
  Result := FAttrBoundLines;
end;

function TrwResultComparatorOptions.GetAttrBoundLinesColor: boolean;
begin
  Result := FAttrBoundLinesColor;
end;

function TrwResultComparatorOptions.GetAttrBoundLinesStyle: boolean;
begin
  Result := FAttrBoundLinesStyle;
end;

function TrwResultComparatorOptions.GetAttrBoundLinesWidth: boolean;
begin
  Result := FAttrBoundLinesWidth;
end;

function TrwResultComparatorOptions.GetAttrColor: boolean;
begin
  Result := FAttrColor;
end;

function TrwResultComparatorOptions.GetAttrFont: boolean;
begin
  Result := FAttrFont;
end;

function TrwResultComparatorOptions.GetAttrLayout: boolean;
begin
  Result := FAttrLayout;
end;

function TrwResultComparatorOptions.GetAttrPCLtextFont: boolean;
begin
  Result := FAttrPCLtextFont;
end;

function TrwResultComparatorOptions.GetAttrText: boolean;
begin
  Result := FAttrText;
end;

function TrwResultComparatorOptions.GetAttrType: boolean;
begin
  Result := FAttrType;
end;

function TrwResultComparatorOptions.GetDeviation: integer;
begin
  Result := FDeviation;
end;

function TrwResultComparatorOptions.GetObjFrames: boolean;
begin
  Result := FObjFrames;
end;

function TrwResultComparatorOptions.GetObjImages: boolean;
begin
  Result := FObjImages;
end;

function TrwResultComparatorOptions.GetObjShapes: boolean;
begin
  Result := FObjShapes;
end;

function TrwResultComparatorOptions.GetObjTexts: boolean;
begin
  Result := FObjTexts;
end;

function TrwResultComparatorOptions.GetPCLTexts: boolean;
begin
  Result := FPCLTexts;
end;

procedure TrwResultComparatorOptions.LoadFromIniFile(const AFileName: String);
var
  IniFile: TIniFile;
begin
  if FileExists(AFileName) then
  begin
    IniFile := TIniFile.Create(AFileName);
    try
      FDeviation := IniFile.ReadInteger('RWAOptions', 'Deviation', FDeviation);
      FObjShapes := IniFile.ReadBool('RWAOptions', 'ObjShapes', true);
      FAttrBoundLinesWidth := IniFile.ReadBool('RWAOptions', 'AttrBoundLinesWidth', true);
      FObjImages := IniFile.ReadBool('RWAOptions', 'ObjImages', true);
      FAttrAligment := IniFile.ReadBool('RWAOptions', 'AttrAligment', true);
      FAttrColor := IniFile.ReadBool('RWAOptions', 'AttrColor', true);
      FAttrPCLtextFont := IniFile.ReadBool('RWAOptions', 'AttrPCLtextFont', true);
      FAttrFont := IniFile.ReadBool('RWAOptions', 'AttrFont', true);
      FAttrLayout := IniFile.ReadBool('RWAOptions', 'AttrLayout', true);
      FObjTexts := IniFile.ReadBool('RWAOptions', 'ObjTexts', true);
      FAttrBoundLinesStyle := IniFile.ReadBool('RWAOptions', 'AttrBoundLinesStyle', true);
      FPCLTexts := IniFile.ReadBool('RWAOptions', 'PCLTexts', true);
      FAttrBoundLinesColor := IniFile.ReadBool('RWAOptions', 'AttrBoundLinesColor', true);
      FAttrType := IniFile.ReadBool('RWAOptions', 'AttrType', true);
      FAttrText := IniFile.ReadBool('RWAOptions', 'AttrText', true);
      FAttrBoundLines := IniFile.ReadBool('RWAOptions', 'AttrBoundLines', true);
      FObjFrames := IniFile.ReadBool('RWAOptions', 'ObjFrames', true);
    finally
      FreeAndNil(IniFile);
    end;
  end;
end;

procedure TrwResultComparatorOptions.SaveToIniFile(const AFileName: String);
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(AFileName);
  try
    IniFile.WriteInteger('RWAOptions', 'Deviation', FDeviation);
    IniFile.WriteBool('RWAOptions', 'ObjShapes', FObjShapes);
    IniFile.WriteBool('RWAOptions', 'AttrBoundLinesWidth', FAttrBoundLinesWidth);
    IniFile.WriteBool('RWAOptions', 'ObjImages', FObjImages);
    IniFile.WriteBool('RWAOptions', 'AttrAligment', FAttrAligment);
    IniFile.WriteBool('RWAOptions', 'AttrColor', FAttrColor);
    IniFile.WriteBool('RWAOptions', 'AttrPCLtextFont', FAttrPCLtextFont);
    IniFile.WriteBool('RWAOptions', 'AttrFont', FAttrFont);
    IniFile.WriteBool('RWAOptions', 'AttrLayout', FAttrLayout);
    IniFile.WriteBool('RWAOptions', 'ObjTexts', FObjTexts);
    IniFile.WriteBool('RWAOptions', 'AttrBoundLinesStyle', FAttrBoundLinesStyle);
    IniFile.WriteBool('RWAOptions', 'PCLTexts', FPCLTexts);
    IniFile.WriteBool('RWAOptions', 'AttrBoundLinesColor', FAttrBoundLinesColor);
    IniFile.WriteBool('RWAOptions', 'AttrType', FAttrType);
    IniFile.WriteBool('RWAOptions', 'AttrText', FAttrText);
    IniFile.WriteBool('RWAOptions', 'AttrBoundLines', FAttrBoundLines);
    IniFile.WriteBool('RWAOptions', 'ObjFrames', FObjFrames);
    IniFile.UpdateFile;
  finally
    FreeAndNil(IniFile);
  end;
end;

procedure TrwResultComparatorOptions.SetAttrAligment(const Value: boolean);
begin
  FAttrAligment := Value;
end;

procedure TrwResultComparatorOptions.SetAttrBoundLines(
  const Value: boolean);
begin
  FAttrBoundLines := Value;
end;

procedure TrwResultComparatorOptions.SetAttrBoundLinesColor(
  const Value: boolean);
begin
  FAttrBoundLinesColor := Value;
end;

procedure TrwResultComparatorOptions.SetAttrBoundLinesStyle(
  const Value: boolean);
begin
  FAttrBoundLinesStyle := Value;
end;

procedure TrwResultComparatorOptions.SetAttrBoundLinesWidth(
  const Value: boolean);
begin
  FAttrBoundLinesWidth := Value;
end;

procedure TrwResultComparatorOptions.SetAttrColor(const Value: boolean);
begin
  FAttrColor := Value;
end;

procedure TrwResultComparatorOptions.SetAttrFont(const Value: boolean);
begin
  FAttrFont := Value;
end;

procedure TrwResultComparatorOptions.SetAttrLayout(const Value: boolean);
begin
  FAttrLayout := Value;
end;

procedure TrwResultComparatorOptions.SetAttrPCLtextFont(
  const Value: boolean);
begin
  FAttrPCLtextFont := Value;
end;

procedure TrwResultComparatorOptions.SetAttrText(const Value: boolean);
begin
  FAttrText := Value;
end;

procedure TrwResultComparatorOptions.SetAttrType(const Value: boolean);
begin
  FAttrType := Value;
end;

procedure TrwResultComparatorOptions.SetDeviation(const Value: integer);
begin
  if not ((Value > 0) and (Value <= 10)) then
    raise Exception.Create('Deviation is out of range (1-10)');
  FDeviation := Value;
end;

procedure TrwResultComparatorOptions.SetObjFrames(const Value: boolean);
begin
  FObjFrames := Value;
end;

procedure TrwResultComparatorOptions.SetObjImages(const Value: boolean);
begin
  FObjImages := Value;
end;

procedure TrwResultComparatorOptions.SetObjShapes(const Value: boolean);
begin
  FObjShapes := Value;
end;

procedure TrwResultComparatorOptions.SetObjTexts(const Value: boolean);
begin
  FObjTexts := Value;
end;

procedure TrwResultComparatorOptions.SetPCLTexts(const Value: boolean);
begin
  FPCLTexts := Value;
end;

end.
