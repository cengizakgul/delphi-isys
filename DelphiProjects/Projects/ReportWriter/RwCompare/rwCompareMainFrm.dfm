object rwCompareMain: TrwCompareMain
  Left = 509
  Top = 290
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'RW Compare'
  ClientHeight = 268
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    458
    268)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 458
    Height = 249
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'File to File'
      object Label1: TLabel
        Left = 4
        Top = 6
        Width = 69
        Height = 13
        Caption = 'Reference File'
      end
      object Label2: TLabel
        Left = 4
        Top = 62
        Width = 61
        Height = 13
        Caption = 'Compare File'
      end
      object cmpFiles: TButton
        Left = 362
        Top = 116
        Width = 75
        Height = 25
        Caption = 'Compare'
        TabOrder = 2
        OnClick = cmpFilesClick
      end
      object cbRefFile: TwwDBComboDlg
        Left = 4
        Top = 22
        Width = 437
        Height = 21
        OnCustomDlg = cbRefFileCustomDlg
        ShowButton = True
        Style = csDropDown
        TabOrder = 0
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object cbCompFile: TwwDBComboDlg
        Left = 4
        Top = 78
        Width = 437
        Height = 21
        OnCustomDlg = cbCompFileCustomDlg
        ShowButton = True
        Style = csDropDown
        TabOrder = 1
        WordWrap = False
        UnboundDataType = wwDefault
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Folder to Folder'
      ImageIndex = 1
      object Label3: TLabel
        Left = 4
        Top = 6
        Width = 82
        Height = 13
        Caption = 'Reference Folder'
      end
      object Label4: TLabel
        Left = 4
        Top = 62
        Width = 74
        Height = 13
        Caption = 'Compare Folder'
      end
      object cbRefFolder: TwwDBComboDlg
        Left = 4
        Top = 22
        Width = 437
        Height = 21
        OnCustomDlg = cbRefFolderCustomDlg
        ShowButton = True
        Style = csDropDown
        TabOrder = 0
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object cbCompFolder: TwwDBComboDlg
        Left = 4
        Top = 78
        Width = 437
        Height = 21
        OnCustomDlg = cbCompFolderCustomDlg
        ShowButton = True
        Style = csDropDown
        TabOrder = 1
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object cmpFolders: TButton
        Left = 362
        Top = 116
        Width = 75
        Height = 25
        Caption = 'Compare'
        TabOrder = 2
        OnClick = cmpFoldersClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Script'
      ImageIndex = 2
      object Label5: TLabel
        Left = 4
        Top = 6
        Width = 46
        Height = 13
        Caption = 'Script File'
      end
      object cbScript: TwwDBComboDlg
        Left = 4
        Top = 22
        Width = 437
        Height = 21
        OnCustomDlg = cbScriptCustomDlg
        ShowButton = True
        Style = csDropDown
        TabOrder = 0
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object cmpScript: TButton
        Left = 362
        Top = 116
        Width = 75
        Height = 25
        Caption = 'Compare'
        TabOrder = 1
        OnClick = cmpScriptClick
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'RWA compare options'
      ImageIndex = 3
      object GroupBox1: TGroupBox
        Left = 6
        Top = 1
        Width = 439
        Height = 78
        Caption = ' Compare objects '
        TabOrder = 0
        object Label6: TLabel
          Left = 205
          Top = 54
          Width = 48
          Height = 13
          Caption = 'Deviation:'
        end
        object cbObjectsTexts: TCheckBox
          Left = 10
          Top = 15
          Width = 97
          Height = 17
          Caption = 'Texts'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object cbObjectsFrames: TCheckBox
          Left = 10
          Top = 34
          Width = 97
          Height = 17
          Caption = 'Frames'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object cbObjectsImages: TCheckBox
          Left = 10
          Top = 53
          Width = 97
          Height = 17
          Caption = 'Images'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object cbObjectsShapes: TCheckBox
          Left = 205
          Top = 15
          Width = 97
          Height = 17
          Caption = 'Shapes'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object cbObjectsPCLTexts: TCheckBox
          Left = 205
          Top = 34
          Width = 97
          Height = 17
          Caption = 'PCL Texts'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object DeviationEdit: TEdit
          Left = 256
          Top = 50
          Width = 24
          Height = 21
          ReadOnly = True
          TabOrder = 5
          Text = '10'
        end
        object UpDown1: TUpDown
          Left = 280
          Top = 50
          Width = 16
          Height = 21
          Associate = DeviationEdit
          Min = 1
          Max = 10
          Position = 10
          TabOrder = 6
        end
      end
      object GroupBox2: TGroupBox
        Left = 6
        Top = 82
        Width = 439
        Height = 135
        Caption = ' Compare attributes '
        TabOrder = 1
        object cbAttrText: TCheckBox
          Left = 10
          Top = 17
          Width = 161
          Height = 17
          Caption = 'Text (Text objects)'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object cbAttrLayout: TCheckBox
          Left = 10
          Top = 36
          Width = 169
          Height = 17
          Caption = 'Layout (Text objects)'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object cbAttrFont: TCheckBox
          Left = 10
          Top = 55
          Width = 161
          Height = 17
          Caption = 'Font (Text objects)'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object cbAttrBoundLinesWidth: TCheckBox
          Left = 205
          Top = 36
          Width = 228
          Height = 17
          Caption = 'Bound Lines Width  (Frame, Shape objects)'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object cbAttrBoundLinesColor: TCheckBox
          Left = 205
          Top = 17
          Width = 228
          Height = 17
          Caption = 'Bound Lines Color  (Frame, Shape objects)'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object cbAttrColor: TCheckBox
          Left = 10
          Top = 74
          Width = 183
          Height = 17
          Caption = 'Color (Text, Shape objects)'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object cbAttrAligment: TCheckBox
          Left = 10
          Top = 93
          Width = 191
          Height = 17
          Caption = 'Alignment (Text, PCL Text objects)'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
        object cbAttrBoundLines: TCheckBox
          Left = 10
          Top = 112
          Width = 191
          Height = 17
          Caption = 'Bound Lines (Frame objects)'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
        object cbAttrBoundLinesStyle: TCheckBox
          Left = 205
          Top = 55
          Width = 228
          Height = 17
          Caption = 'Bound Lines Style  (Frame, Shape objects)'
          Checked = True
          State = cbChecked
          TabOrder = 8
        end
        object cbAttrType: TCheckBox
          Left = 205
          Top = 74
          Width = 193
          Height = 17
          Caption = 'Type (Shape objects)'
          Checked = True
          State = cbChecked
          TabOrder = 9
        end
        object cbAttrPCLTextFont: TCheckBox
          Left = 205
          Top = 93
          Width = 193
          Height = 17
          Caption = 'PCL Text font (PCL Text objects)'
          Checked = True
          State = cbChecked
          TabOrder = 10
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 249
    Width = 458
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object ProgressBar: TProgressBar
    Left = 340
    Top = 251
    Width = 118
    Height = 17
    Anchors = [akRight, akBottom]
    Smooth = True
    TabOrder = 2
  end
  object odFiles: TOpenDialog
    DefaultExt = 'rwa'
    Filter = 
      'Report Writer result files (*.rwa)|*.rwa|ASCII Files (*.txt)|*.t' +
      'xt|All Files (*.*)|*.*'
    Title = 'Open RW Result File'
    Left = 316
  end
  object odScript: TOpenDialog
    DefaultExt = 'rcs'
    Filter = 'RW Compare script files (*.rcs)|*.rcs|All Files (*.*)|*.*'
    Title = 'Open compare script file'
    Left = 348
  end
  object rwAppAdapter: TrwAppAdapter
    OnStartEngineProgressBar = rwEngineCallBack1StartProgressBar
    OnUpdateEngineProgressBar = rwEngineCallBack1UpdateProgressBar
    OnEndEngineProgressBar = rwEngineCallBack1EndProgressBar
    Left = 388
  end
end
