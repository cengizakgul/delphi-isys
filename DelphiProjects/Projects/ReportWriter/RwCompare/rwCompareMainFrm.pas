// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwCompareMainFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwCompareUtils, Mask, wwdbedit, Wwdotdot, ComCtrls, rwFormPreviewFrm,
  EvStreamUtils, rwUtils, rwEngineTypes, rwCallback, rwDesignClasses,
  rwEngine, Spin, ISBasicClasses;

type
  TrwCompareMain = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    cmpFiles: TButton;
    Label1: TLabel;
    cbRefFile: TwwDBComboDlg;
    Label2: TLabel;
    cbCompFile: TwwDBComboDlg;
    Label3: TLabel;
    cbRefFolder: TwwDBComboDlg;
    Label4: TLabel;
    cbCompFolder: TwwDBComboDlg;
    cmpFolders: TButton;
    odFiles: TOpenDialog;
    Label5: TLabel;
    cbScript: TwwDBComboDlg;
    cmpScript: TButton;
    odScript: TOpenDialog;
    rwAppAdapter: TrwAppAdapter;
    StatusBar: TStatusBar;
    ProgressBar: TProgressBar;
    TabSheet4: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cbObjectsTexts: TCheckBox;
    cbObjectsFrames: TCheckBox;
    cbObjectsImages: TCheckBox;
    cbObjectsShapes: TCheckBox;
    cbObjectsPCLTexts: TCheckBox;
    cbAttrText: TCheckBox;
    cbAttrLayout: TCheckBox;
    cbAttrFont: TCheckBox;
    cbAttrBoundLinesWidth: TCheckBox;
    cbAttrBoundLinesColor: TCheckBox;
    cbAttrColor: TCheckBox;
    cbAttrAligment: TCheckBox;
    Label6: TLabel;
    cbAttrBoundLines: TCheckBox;
    cbAttrBoundLinesStyle: TCheckBox;
    cbAttrType: TCheckBox;
    cbAttrPCLTextFont: TCheckBox;
    DeviationEdit: TEdit;
    UpDown1: TUpDown;
    procedure cmpFilesClick(Sender: TObject);
    procedure cbRefFileCustomDlg(Sender: TObject);
    procedure cbCompFileCustomDlg(Sender: TObject);
    procedure cbRefFolderCustomDlg(Sender: TObject);
    procedure cbCompFolderCustomDlg(Sender: TObject);
    procedure cmpFoldersClick(Sender: TObject);
    procedure cbScriptCustomDlg(Sender: TObject);
    procedure cmpScriptClick(Sender: TObject);
    procedure rwEngineCallBack1EndProgressBar;
    procedure rwEngineCallBack1StartProgressBar(const Title: String;
      MaxValue: Integer);
    procedure rwEngineCallBack1UpdateProgressBar(
      const ProgressText: String; CurrentValue: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FRWACompareOptions: IrwResultComparatorOptions;

    procedure StoreRWAOptionsToObject;
    procedure ReadRWACompareOptionsFromObject;
  public
  end;

var
  rwCompareMain: TrwCompareMain;

implementation

{$R *.DFM}

procedure TrwCompareMain.cmpFilesClick(Sender: TObject);

  procedure CompareRWA(const AFileRef, AFileComp: String);
  var
    C: TrwResultComparator;
    h: String;
  begin
    StoreRWAOptionsToObject;
    RWEngineExt.AppAdapter.StartEngineProgressBar('Comparing files...');

    C := TrwResultComparator.Create(AFileRef, AFileComp);
    try
      C.SetRWACompareOptions(FRWACompareOptions);
      try
        if not C.Compare then
          h := C.ResFileName
        else
          h := '';
      except
        RWEngineExt.AppAdapter.EndEngineProgressBar;
        raise;
      end;
    finally
      C.Free;
    end;

    if Length(h) > 0 then
    begin
      RWEngineExt.AppAdapter.EndEngineProgressBar;
      rwFormPreviewFrm.ModalPreview(h);
      DeleteFile(h);
    end
    else
    begin
      RWEngineExt.AppAdapter.EndEngineProgressBar;
      ShowMessage('Files are identical');
    end;
  end;

begin
  if SameText(ExtractFileExt(cbRefFile.Text), '.rwa') then
    CompareRWA(cbRefFile.Text, cbCompFile.Text)
  else
    CompareASCII(cbRefFile.Text, cbCompFile.Text);
end;

procedure TrwCompareMain.cbRefFileCustomDlg(Sender: TObject);
begin
  odFiles.FileName := cbRefFile.Text;
  if odFiles.Execute then
    cbRefFile.Text := odFiles.FileName;
end;

procedure TrwCompareMain.cbCompFileCustomDlg(Sender: TObject);
begin
  odFiles.FileName := cbCompFile.Text;
  if odFiles.Execute then
    cbCompFile.Text := odFiles.FileName;
end;

procedure TrwCompareMain.cbRefFolderCustomDlg(Sender: TObject);
begin
  odFiles.InitialDir := cbRefFolder.Text;
  odFiles.FileName := '';
  if odFiles.Execute then
    cbRefFolder.Text := ExtractFilePath(odFiles.FileName);
end;


procedure TrwCompareMain.cbCompFolderCustomDlg(Sender: TObject);
begin
  odFiles.InitialDir := cbCompFolder.Text;
  odFiles.FileName := '';
  if odFiles.Execute then
    cbCompFolder.Text := ExtractFilePath(odFiles.FileName);
end;


procedure TrwCompareMain.cmpFoldersClick(Sender: TObject);
var
  C: TrwResultComparator;
  h: String;
  SR: TSearchRec;
  Res: Integer;
  n, i: Integer;
  FL: TrwRWAInfoList;
  R: TrwRWAInfoRec;
begin
  StoreRWAOptionsToObject;

  Res := FindFirst(cbCompFolder.Text + '*.txt', faAnyFile, SR);
  try
    while Res = 0 do
    begin
      h := cbRefFolder.Text + ExtractFileName(SR.Name);
      if not FileExists(h) then
        raise Exception.Create('File "' + h + '" doesn''t exist in the reference folder!');

      CompareASCII(h, SR.Name);

      Res := FindNext(SR);
    end;

  finally
    FindClose(SR);
  end;

  n := 0;
  Res := FindFirst(cbCompFolder.Text + '*.rwa', faAnyFile, SR);
  try
    while Res = 0 do
    begin
      Inc(n);
      h := cbRefFolder.Text + ExtractFileName(SR.Name);
      if not FileExists(h) then
        raise Exception.Create('File "' + h + '" doesn''t exist in the reference folder!');
      Res := FindNext(SR);
    end;
  finally
    FindClose(SR);
  end;

  SetLength(FL, 0);

  RWEngineExt.AppAdapter.StartEngineProgressBar('Comparing files...' + #13#13'Files left: '+IntToStr(n));
  Res := FindFirst(cbRefFolder.Text + '*.rwa', faAnyFile, SR);
  try
    i := 0;
    while Res = 0 do
    begin
      Inc(i);
      h := cbCompFolder.Text + SR.Name;
      RWEngineExt.AppAdapter.UpdateEngineProgressBar('Comparing files...' + #13#13'Files left: '+IntToStr(n - i + 1));
      if not FileExists(h) then
        raise Exception.Create('File "' + h + '" doesn''t exist in the compare folder!');

      C := TrwResultComparator.Create(cbRefFolder.Text + SR.Name, h);
      try
        C.SetRWACompareOptions(FRWACompareOptions);
        try
          if not C.Compare then
          begin
            SetLength(FL, High(FL) - Low(FL) + 2);
            R := FL[High(FL)];
            R.Data3 := TEvDualStreamHolder.CreateFromFile(C.ResFileName, True);
            R.Name := C.WaitText;
          end;
        except
          RWEngineExt.AppAdapter.EndEngineProgressBar;
          raise;
        end;
      finally
        C.Free;
      end;

      Res := FindNext(SR);
    end;

  finally
    FindClose(SR);
    RWEngineExt.AppAdapter.EndEngineProgressBar;
  end;

  if High(FL) - Low(FL) >= 0 then
    ModalPreview(@FL)
  else
    ShowMessage('Content of folders are identical');
end;

procedure TrwCompareMain.cbScriptCustomDlg(Sender: TObject);
begin
  odScript.InitialDir := cbScript.Text;
  odScript.FileName := '';
  if odScript.Execute then
    cbScript.Text := odScript.FileName;
end;


procedure TrwCompareMain.cmpScriptClick(Sender: TObject);
var
  h, f1, f2: String;
  lScript: TStringList;
  i,j: Integer;
  FL: TrwRWAInfoList;
  R: TrwRWAInfoRec;

  procedure CompareRWA;
  var
    C: TrwResultComparator;
  begin
    C := TrwResultComparator.Create(f1, f2);
    try
      C.SetRWACompareOptions(FRWACompareOptions);
      try
        if not C.Compare then
        begin
          SetLength(FL, High(FL) - Low(FL) + 2);
          R := FL[High(FL)];
          R.Data3 := TEvDualStreamHolder.CreateFromFile(C.ResFileName, True);
          R.Name := C.WaitText;
        end;
      except
        RWEngineExt.AppAdapter.EndEngineProgressBar;
        raise;
      end;
    finally
      C.Free;
    end;
  end;

begin
  StoreRWAOptionsToObject;

  SetLength(FL, 0);
  lScript := TStringList.Create;
  try
    lScript.LoadFromFile(cbScript.Text);
    RWEngineExt.AppAdapter.StartEngineProgressBar('Comparing files...' + #13#13'Files Left: '+IntToStr(lScript.Count));

    for i := 0 to lScript.Count -1 do
    begin
      h := lScript[i];
      RWEngineExt.AppAdapter.UpdateEngineProgressBar('Comparing files...' + #13#13'Files Left: '+IntToStr(lScript.Count - i));
      j := Pos('=', h);
      f1 := Trim(Copy(h, 1, j-1));
      f2 := Trim(Copy(h, j+1, Length(h)-j));

      if SameText(ExtractFileExt(f1), '.rwa') then
        CompareRWA
      else
        CompareASCII(f1, f2);
    end;

  finally
    lScript.Free;
    RWEngineExt.AppAdapter.EndEngineProgressBar;
  end;

  if High(FL) - Low(FL) >= 0 then
    ModalPreview(@FL)
  else
    ShowMessage('Content of folders are identical');
end;

procedure TrwCompareMain.rwEngineCallBack1EndProgressBar;
begin
  StatusBar.Panels[0].Text := '';
  ProgressBar.Position := 0;
  Ready;
end;

procedure TrwCompareMain.rwEngineCallBack1StartProgressBar(const Title: String; MaxValue: Integer);
begin
  Busy;
  if MaxValue < 0 then
    ProgressBar.Max := 0
  else
    ProgressBar.Max := MaxValue;
  StatusBar.Panels[0].Text := Title;
end;

procedure TrwCompareMain.rwEngineCallBack1UpdateProgressBar(const ProgressText: String; CurrentValue: Integer);
begin
  if CurrentValue < 0 then
    ProgressBar.Position := 0
  else
    ProgressBar.Position := CurrentValue;
  StatusBar.Panels[0].Text := ProgressText;
end;

procedure TrwCompareMain.FormCreate(Sender: TObject);
begin
  InitializeRWEngine(rwAppAdapter);

  FRWACompareOptions := TrwResultComparatorOptions.Create;
  FRWACompareOptions.LoadFromIniFile(ChangeFileExt(ParamStr(0), '.cfg'));
  ReadRWACompareOptionsFromObject;
end;

procedure TrwCompareMain.FormDestroy(Sender: TObject);
begin
  DestroyRWEngine;

  StoreRWAOptionsToObject;
  FRWACompareOptions.SaveToIniFile(ChangeFileExt(ParamStr(0), '.cfg'));
end;

procedure TrwCompareMain.ReadRWACompareOptionsFromObject;
begin
  cbObjectsTexts.Checked := FRWACompareOptions.ObjTexts;
  cbObjectsFrames.Checked := FRWACompareOptions.ObjFrames;
  cbObjectsImages.Checked := FRWACompareOptions.ObjImages;
  cbObjectsShapes.Checked := FRWACompareOptions.ObjShapes;
  cbObjectsPCLTexts.Checked := FRWACompareOptions.PCLTexts;
  UpDown1.Position := FRWACompareOptions.Deviation;

  cbAttrText.Checked := FRWACompareOptions.AttrText;
  cbAttrLayout.Checked := FRWACompareOptions.AttrLayout;
  cbAttrFont.Checked := FRWACompareOptions.AttrFont;
  cbAttrColor.Checked := FRWACompareOptions.AttrColor;
  cbAttrAligment.Checked := FRWACompareOptions.AttrAligment;
  cbAttrBoundLines.Checked := FRWACompareOptions.AttrBoundLines;
  cbAttrBoundLinesColor.Checked := FRWACompareOptions.AttrBoundLinesColor;
  cbAttrBoundLinesWidth.Checked := FRWACompareOptions.AttrBoundLinesWidth;
  cbAttrBoundLinesStyle.Checked := FRWACompareOptions.AttrBoundLinesStyle;
  cbAttrType.Checked := FRWACompareOptions.AttrType;
  cbAttrPCLTextFont.Checked := FRWACompareOptions.AttrPCLtextFont;
end;

procedure TrwCompareMain.StoreRWAOptionsToObject;
begin
  FRWACompareOptions.ObjTexts := cbObjectsTexts.Checked;
  FRWACompareOptions.ObjFrames := cbObjectsFrames.Checked;
  FRWACompareOptions.ObjImages := cbObjectsImages.Checked;
  FRWACompareOptions.ObjShapes := cbObjectsShapes.Checked;
  FRWACompareOptions.PCLTexts := cbObjectsPCLTexts.Checked;
  FRWACompareOptions.Deviation := UpDown1.Position;

  FRWACompareOptions.AttrText := cbAttrText.Checked;
  FRWACompareOptions.AttrLayout := cbAttrLayout.Checked;
  FRWACompareOptions.AttrFont := cbAttrFont.Checked;
  FRWACompareOptions.AttrColor := cbAttrColor.Checked;
  FRWACompareOptions.AttrAligment := cbAttrAligment.Checked;
  FRWACompareOptions.AttrBoundLines := cbAttrBoundLines.Checked;
  FRWACompareOptions.AttrBoundLinesColor := cbAttrBoundLinesColor.Checked;
  FRWACompareOptions.AttrBoundLinesWidth := cbAttrBoundLinesWidth.Checked;
  FRWACompareOptions.AttrBoundLinesStyle := cbAttrBoundLinesStyle.Checked;
  FRWACompareOptions.AttrType := cbAttrType.Checked;
  FRWACompareOptions.AttrPCLtextFont := cbAttrPCLTextFont.Checked;
end;

end.
