unit ddFieldsEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, ddParamsEditFrm, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictParamEditFrm, StdCtrls, DataDictFieldEditFrm,
  rwCustomDataDictionary, ExtCtrls, rwEngineTypes;

type
  TddFieldsEdit = class(TDataDictCollectionEdit)
    FieldInfo: TDataDictFieldEdit;
    procedure FieldInfoedNameExit(Sender: TObject);
  private
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
    procedure DeleteItem(const AItem: TCollectionItem); override;    
  public
    procedure AfterConstruction; override;
  end;


implementation

{$R *.dfm}

uses DataDictTableEditorFrm;

{ TddFieldsEdit }

procedure TddFieldsEdit.AfterConstruction;
begin
  inherited;
  FItemFrame := FieldInfo;
end;

function TddFieldsEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictField(AItem).Name;
end;

procedure TddFieldsEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
begin
  TDataDictField(AItem).Name := 'New_Field';
  TDataDictField(AItem).FieldType := ddtString;
end;

procedure TddFieldsEdit.FieldInfoedNameExit(Sender: TObject);
begin
  FieldInfo.edNameExit(Sender);
  UpdateItem(SelectedItem);
end;

procedure TddFieldsEdit.DeleteItem(const AItem: TCollectionItem);
begin
  inherited;
  TableEditor.ddTable.FixupReferences;
  TableEditor.RefreshTableItem(ddTITPrimaryKey);
  TableEditor.RefreshTableItem(ddForeignKeys);
end;

end.
