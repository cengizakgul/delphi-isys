inherited ddParamsEdit: TddParamsEdit
  Width = 373
  Height = 212
  inherited pnlContainer: TPanel
    Width = 373
    Height = 212
    DesignSize = (
      373
      212)
    inherited lbCollection: TListBox
      Height = 212
    end
    inherited btnAddItem: TButton
      Left = 210
      Top = 191
      Caption = 'New Param'
      TabOrder = 2
    end
    inherited btnDeleteItem: TButton
      Left = 294
      Top = 191
      Caption = 'Delete Param'
      TabOrder = 3
    end
    inline ParamInfo: TDataDictParamEdit
      Left = 143
      Top = 0
      Width = 225
      Height = 183
      AutoScroll = False
      TabOrder = 1
      inherited pnlContainer: TPanel
        Width = 225
        Height = 183
        inherited lNotes: TLabel
          Top = 87
        end
        inherited edName: TEdit
          Width = 143
        end
        inherited edDisplayName: TEdit
          Width = 144
        end
        inherited memNotes: TMemo
          Width = 225
          Height = 79
        end
        inherited ParamType: TDataDictDataTypes
          Width = 226
          inherited cbType: TComboBox
            Width = 144
          end
        end
      end
      inherited pcParamProp: TPageControl
        Width = 225
        Height = 183
      end
    end
  end
end
