inherited ddFieldsEdit: TddFieldsEdit
  Width = 355
  Height = 361
  inherited pnlContainer: TPanel
    Width = 355
    Height = 361
    DesignSize = (
      355
      361)
    inline FieldInfo: TDataDictFieldEdit [0]
      Left = 0
      Top = 156
      Width = 355
      Height = 205
      Align = alBottom
      AutoScroll = False
      TabOrder = 3
      inherited pnlContainer: TPanel
        Width = 355
        DesignSize = (
          355
          205)
        inherited edName: TEdit
          Width = 272
          OnChange = nil
          OnExit = FieldInfoedNameExit
        end
        inherited edDisplayName: TEdit
          Width = 272
        end
        inherited memNotes: TMemo
          Width = 355
        end
      end
      inherited pcFieldProp: TPageControl
        Width = 355
        inherited tsFieldValues: TTabSheet
          inherited vleValues: TValueListEditor
            Width = 347
            ColWidths = (
              47
              294)
          end
        end
      end
    end
    inherited lbCollection: TListBox [1]
      Width = 354
      Height = 136
      Align = alNone
      Anchors = [akLeft, akTop, akRight, akBottom]
    end
    inherited btnAddItem: TButton [2]
      Left = 196
      Top = 142
      Caption = 'New Field'
    end
    inherited btnDeleteItem: TButton
      Left = 280
      Top = 142
      Caption = 'Delete Field'
    end
  end
end
