object ddEditor: TddEditor
  Left = 283
  Top = 170
  BorderStyle = bsDialog
  Caption = 'Data Dictionary Editor'
  ClientHeight = 495
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    710
    495)
  PixelsPerInch = 96
  TextHeight = 13
  object lbTables: TListBox
    Left = 1
    Top = 2
    Width = 270
    Height = 408
    Anchors = [akLeft, akTop, akBottom]
    ItemHeight = 13
    PopupMenu = pmDataDict
    TabOrder = 4
    OnClick = lbTablesClick
  end
  object memNotes: TMemo
    Left = 0
    Top = 413
    Width = 271
    Height = 81
    Anchors = [akLeft, akBottom]
    TabOrder = 1
    OnExit = memNotesExit
  end
  inline ddTableEditor: TddTableEditor
    Left = 276
    Top = 0
    Width = 432
    Height = 494
    AutoScroll = False
    Constraints.MinHeight = 250
    Constraints.MinWidth = 267
    TabOrder = 0
    inherited btnOK: TButton
      Visible = False
    end
  end
  object btnAddTable: TButton
    Left = 284
    Top = 459
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'New Table'
    TabOrder = 2
    OnClick = btnAddTableClick
  end
  object btnDeleteTable: TButton
    Left = 368
    Top = 459
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Delete Table'
    TabOrder = 3
    OnClick = btnDeleteTableClick
  end
  object pmDataDict: TPopupMenu
    Left = 160
    Top = 88
    object miLoadDD: TMenuItem
      Caption = 'Load from File...'
      OnClick = miLoadDDClick
    end
    object miSaveDD: TMenuItem
      Caption = 'Save To File...'
      OnClick = miSaveDDClick
    end
  end
end
