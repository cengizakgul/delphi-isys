unit ddTableEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictTableEditorFrm, StdCtrls, ComCtrls, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictTablePropEditFrm,
  DataDictCollectionEditFrm, ddParamsEditFrm, DataDictFieldEditFrm,
  ddFieldsEditFrm, DataDictPrimaryKeyEditFrm, ddForeignKeyEditFrm,
  DataDictLogicalKeyEditFrm;

type
  TddTableEditor = class(TDataDictTableEditor)
    ddTablePropEdit: TDataDictTablePropEdit;
    grTableParams: TGroupBox;
    ddParamsEdit: TddParamsEdit;
    ddFieldsEdit: TddFieldsEdit;
    ddPrimaryKeyEdit: TDataDictPrimaryKeyEdit;
    ddForeignKeyEdit: TddForeignKeyEdit;
    ddLogicalKeyEdit: TDataDictLogicalKeyEdit;
    lPrimaryKey: TLabel;
    lLogicalKey: TLabel;
  private
  protected
    procedure PrepareToEdit; override;
  public
    procedure RefreshTableItem(const ATableItemType: TDataDictTableItemType); override;
  end;

implementation

uses rwCustomDataDictionary;

{$R *.dfm}


{ TddTableEditor }

procedure TddTableEditor.PrepareToEdit;
begin
  ddTablePropEdit.DataDictionaryItem := ddTable;
  ddParamsEdit.DataDictionaryItem := ddTable.Params;
  ddFieldsEdit.DataDictionaryItem := ddTable.Fields;
  ddPrimaryKeyEdit.DataDictionaryItem := ddTable.PrimaryKey;
  ddLogicalKeyEdit.DataDictionaryItem := ddTable.LogicalKey;
  ddForeignKeyEdit.DataDictionaryItem := ddTable.ForeignKeys;
end;

procedure TddTableEditor.RefreshTableItem(const ATableItemType: TDataDictTableItemType);
begin
  case ATableItemType of
    ddTITPrimaryKey:
      begin
        ddPrimaryKeyEdit.DataDictionaryItem := ddTable.PrimaryKey;
        ddLogicalKeyEdit.DataDictionaryItem := ddTable.LogicalKey;
      end;

    ddForeignKeys:
      ddForeignKeyEdit.DataDictionaryItem := ddTable.ForeignKeys;
  end;
end;

end.
