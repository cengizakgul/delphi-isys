unit ddEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, DataDictTableEditorFrm, ddTableEditorFrm,
  rwCustomDataDictionary, Menus, RwDataDictionary, rwEngineTypes;

type
  TddEditor = class(TForm)
    lbTables: TListBox;
    btnAddTable: TButton;
    btnDeleteTable: TButton;
    memNotes: TMemo;
    ddTableEditor: TddTableEditor;
    pmDataDict: TPopupMenu;
    miLoadDD: TMenuItem;
    miSaveDD: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btnAddTableClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memNotesExit(Sender: TObject);
    procedure lbTablesClick(Sender: TObject);
    procedure btnDeleteTableClick(Sender: TObject);
    procedure miSaveDDClick(Sender: TObject);
    procedure miLoadDDClick(Sender: TObject);
  private
    FDataDictionary: TrwDataDictionary;
    function  AddTableIntoList(const ATable: TDataDictTable): Integer;
    procedure UpdateTableIntoList(const ATable: TDataDictTable);
    procedure OnApplyChanges(Sender: TObject);
  public
    procedure PrepareToEdit;
    function  SelectedTable: TDataDictTable;
    procedure SelectTable(const AIndex: Integer);

  end;


implementation

{$R *.dfm}

uses OldDataDictionary, SFieldCodeValues;

procedure TddEditor.FormCreate(Sender: TObject);

  procedure ConvertOldDD;
  var
    OldDD: TrwOldDataDictionary;
    FS: TEvFileStream;
    i, j, l: Integer;
    h: String;
    T, T2: TrwDataDictTable;
    P: TrwDataDictParam;
    F: TrwDataDictField;
    PKF: TDataDictPrimKeyField;
    FK: TDataDictForeignKey;
    O: TlqObject;
    A: TlqAttribute;
    Fc: TextFile;

    function ConvertOldType(AType: TlqDataType): TDataDictDataType;
    begin
      case AType of
        lqdtDateTime:  Result := ddtDateTime;
        lqdtInteger:   Result := ddtInteger;
        lqdtExtended:  Result := ddtFloat;
        lqdtString:    Result := ddtString;
        lqdtBLOB:      Result := ddtBLOB;
        lqdtMemo:      Result := ddtMemo;
        lqdtGraphic:   Result := ddtGraphic;
        lqdtArray:     Result := ddtArray;
      else
        Result := ddtUnknown;
      end;
    end;

    function FixName(AName: String): String;
    var
      i: Integer;
    begin
      Result := '';
      i := Pos('_', AName);
      while i > 0 do
      begin
        Result := Result + AnsiUpperCase(AName[1]) + AnsiLowerCase(Copy(AName, 2, i - 1));
        Delete(AName, 1, i);
        i := Pos('_', AName);
      end;

      if AName <> '' then
        Result := Result + AnsiUpperCase(AName[1]) + AnsiLowerCase(Copy(AName, 2, Length(AName) - 1));
    end;

  begin
    AssignFile(Fc, 'c:\Consol.txt');
    Rewrite(Fc);

    OldDD := TrwOldDataDictionary.Create;
    try
      FS := TEvFileStream.Create('c:\ISystems\Report Writer\Crap\OldDataDictionary.rwd', fmOpenRead);
      OldDD.ContentFromStream(FS);
      FS.Free;

      for i := 0 to OldDD.Objects.Count - 1 do
      begin
        O := OldDD.Objects[i];


        if O.ConsolType <> ctNone then
        begin
          case O.ConsolType of
            ctNoMerge:          h := 'ctNoMerge';
            ctSimpleMerge:      h := 'ctSimpleMerge';
            ctMergeNoChanges:   h := 'ctMergeNoChanges';
            ctMergeWithChanges: h := 'ctMergeWithChanges';
          end;

          h := AnsiUpperCase(O.PhysicalName) + '   ' + h;

          if Assigned(O.ConsFieldToChange) then
            h := h + '   ' + AnsiUpperCase(O.ConsFieldToChange.PhysicalName);

          Writeln(Fc, h);
        end;

        T := TrwDataDictTable(FDataDictionary.Tables.AddTable);

        if O.DataBase = obLogic then
          T.TableType := rwtDDQuery
        else if O.DataBase = obComponent then
          T.TableType := rwtDDComponent
        else
          T.TableType := rwtDDExternal;

        T.GroupName := O.GroupName;

        T.ExternalName := AnsiUpperCase(Trim(O.PhysicalName));

        if T.TableType = rwtDDExternal then
        begin
          h := T.ExternalName;
          if AnsiSameText(Copy(h, Length(h)- 4, 5), '_Hist') then
            Delete(h, Length(h)- 4, 5);
          T.ExternalName := h;
        end;
        T.Name := FixName(O.ObjectName);
        T.DisplayName := O.DisplayName;
        T.Notes := O.Description;

        for j := 0 to O.Params.Count - 1 do
        begin
          P := TrwDataDictParam(T.Params.Add);
          P.Name := FixName(O.Params[j].Name);
          if AnsiSameText(P.Name, 'MY_DATE') then
            P.Name := 'AsOfDate';
          P.DisplayName := O.Params[j].DisplayName;
          if (P.Name = 'AsOfDate') and (P.DisplayName = '') then
            P.DisplayName := 'As Of Date';
          P.ParamType := ConvertOldType(O.Params[j].ParType);

          h := O.Params[j].Value;
          if Pos(#13, h) <> 0 then
            P.ParamValues.Text := h
          else
            P.DefaultValue := h;

          if O.Params[j].TableName <> '' then
          begin
            T2 := TrwDataDictTable(FDataDictionary.Tables.TableByName(O.Params[j].TableName));
            if Assigned(T2) and (O.Params[j].FieldName <> '') then
              P.RefField := T2.Fields.FieldByName(O.Params[j].FieldName);
          end;
        end;

        for j := 0 to O.Attributes.Count - 1 do
        begin
          A := O.Attributes[j];
          F := TrwDataDictField(T.Fields.AddField);
          F.ExternalName := AnsiUpperCase(Trim(A.PhysicalName));
          F.Name := FixName(A.AttributeName);
          F.FieldType := ConvertOldType(A.PhysicalType);
          F.DisplayName := A.DisplayName;
          F.Notes := A.Description;
          F.FieldValues.Text := GetConstChoisesByField(AnsiUpperCase(T.ExternalName + '_' + F.ExternalName));
        end;

        for j := 0 to O.KeyInterface.Count - 1 do
        begin
          PKF := TDataDictPrimKeyField(T.PrimaryKey.Add);
          PKF.Field := T.Fields.FieldByName(O.KeyInterface[j].AttributeName);
        end;

        for j := 0 to O.LogKey.Count - 1 do
        begin
          PKF := TDataDictPrimKeyField(T.LogicalKey.Add);
          PKF.Field := T.Fields.FieldByName(O.LogKey[j].AttributeName);
        end;

        for j := 0 to O.RefInterface.Count - 1 do
        begin
          T2 := TrwDataDictTable(FDataDictionary.Tables.TableByName(O.RefInterface[j].RefObject.ObjectName));
          if Assigned(T2) then
          begin
            FK := TDataDictForeignKey(T.ForeignKeys.Add);
            FK.Table := T2;
            for l := 0 to O.RefInterface[j].Count - 1 do
              TDataDictForeignKeyField(FK.Fields.Add).Field := T.Fields.FieldByName(O.RefInterface[j][l].AttributeName);
          end;
        end;

        if O.DataBase = obLogic then
        begin
          O.QBInfo.Position := 0;
          T.QBInfo.LoadFromStream(O.QBInfo);
        end;

      end;

    finally
      OldDD.Free;
      CloseFile(Fc);
    end;
  end;

begin
  FDataDictionary := TrwDataDictionary.Create(Self);
  ddTableEditor.DataDictionary := FDataDictionary;
  ddTableEditor.OnApplyChanges := OnApplyChanges;

  FDataDictionary.Notes := 'Data Dictionary Example';

  ConvertOldDD;
end;

procedure TddEditor.btnAddTableClick(Sender: TObject);
var
  T: TDataDictTable;
begin
  ddTableEditor.pcTableProps.ActivePage := ddTableEditor.tsTableInfo; 
  T := FDataDictionary.Tables.AddTable;
  T.Name := 'New Table';
  SelectTable(AddTableIntoList(T));
end;

function TddEditor.AddTableIntoList(const ATable: TDataDictTable): Integer;
begin
  Result := lbTables.Items.AddObject(ATable.Name, ATable);
end;

procedure TddEditor.FormShow(Sender: TObject);
begin
  PrepareToEdit;
end;

procedure TddEditor.PrepareToEdit;
var
  i: Integer;
begin
  memNotes.Text := FDataDictionary.Notes;

  lbTables.Items.BeginUpdate;
  try
    lbTables.Clear;
    for i := 0 to FDataDictionary.Tables.Count - 1 do
      AddTableIntoList(FDataDictionary.Tables[i]);
  finally
    lbTables.Items.EndUpdate;
  end;

  if lbTables.Count > 0 then
    SelectTable(0)
  else
    SelectTable(-1);
end;

procedure TddEditor.memNotesExit(Sender: TObject);
begin
  FDataDictionary.Notes := memNotes.Text;
end;

procedure TddEditor.lbTablesClick(Sender: TObject);
begin
  ddTableEditor.ShowTable(SelectedTable);
end;

function TddEditor.SelectedTable: TDataDictTable;
begin
  if lbTables.ItemIndex = -1 then
    Result := nil
  else
    Result := TDataDictTable(lbTables.Items.Objects[lbTables.ItemIndex]);
end;

procedure TddEditor.btnDeleteTableClick(Sender: TObject);
var
  i: Integer;
begin
  if SelectedTable <> nil then
  begin
    i := lbTables.ItemIndex;
    FDataDictionary.DeleteTable(SelectedTable);
    lbTables.Items.Delete(i);
    if i > lbTables.Count - 1 then
      i := lbTables.Count - 1;
    SelectTable(i);
  end;
end;

procedure TddEditor.SelectTable(const AIndex: Integer);
begin
  lbTables.ItemIndex := AIndex;
  lbTables.OnClick(nil);
end;

procedure TddEditor.OnApplyChanges(Sender: TObject);
begin
  UpdateTableIntoList(SelectedTable);
end;

procedure TddEditor.UpdateTableIntoList(const ATable: TDataDictTable);
var
  i: Integer;
begin
  i := lbTables.Items.IndexOfObject(ATable);
  lbTables.Items[i] := ATable.Name;
end;

procedure TddEditor.miSaveDDClick(Sender: TObject);
begin
  FDataDictionary.SaveToFile('C:\DD.dat');
end;

procedure TddEditor.miLoadDDClick(Sender: TObject);
begin
  FDataDictionary.LoadFromFile('C:\DD.dat');
  PrepareToEdit;  
end;

end.
