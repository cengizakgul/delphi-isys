inherited ddTableEditor: TddTableEditor
  Width = 432
  Height = 494
  Enabled = True
  DesignSize = (
    432
    494)
  inherited pcTableProps: TPageControl
    Width = 432
    Height = 494
    inherited tsTableInfo: TTabSheet
      DesignSize = (
        424
        466)
      inline ddTablePropEdit: TDataDictTablePropEdit
        Left = 6
        Top = 8
        Width = 412
        Height = 120
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 412
          Height = 120
          inherited edName: TEdit
            Width = 329
          end
          inherited edDisplayName: TEdit
            Width = 330
          end
          inherited memNotes: TMemo
            Width = 412
          end
        end
      end
      object grTableParams: TGroupBox
        Left = 6
        Top = 149
        Width = 412
        Height = 242
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Table Parameters'
        TabOrder = 1
        DesignSize = (
          412
          242)
        inline ddParamsEdit: TddParamsEdit
          Left = 10
          Top = 18
          Width = 395
          Height = 212
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoScroll = False
          TabOrder = 0
          inherited pnlContainer: TPanel
            Width = 395
            inherited btnAddItem: TButton
              Left = 232
            end
            inherited btnDeleteItem: TButton
              Left = 316
            end
            inherited ParamInfo: TDataDictParamEdit
              Width = 248
              inherited pnlContainer: TPanel [0]
                Width = 248
                inherited edName: TEdit
                  Width = 166
                end
                inherited edDisplayName: TEdit
                  Width = 167
                end
                inherited memNotes: TMemo
                  Width = 248
                end
                inherited ParamType: TDataDictDataTypes
                  Width = 249
                  inherited cbType: TComboBox
                    Width = 167
                  end
                end
              end
              inherited pcParamProp: TPageControl [1]
                Width = 248
              end
            end
          end
        end
      end
    end
    inherited tsFields: TTabSheet
      DesignSize = (
        424
        466)
      inline ddFieldsEdit: TddFieldsEdit
        Left = 6
        Top = 6
        Width = 414
        Height = 422
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 414
          Height = 422
          inherited FieldInfo: TDataDictFieldEdit
            Top = 217
            Width = 414
            inherited pnlContainer: TPanel [0]
              Width = 414
              inherited edName: TEdit
                Width = 331
              end
              inherited edDisplayName: TEdit
                Width = 331
              end
              inherited memNotes: TMemo
                Width = 414
              end
            end
            inherited pcFieldProp: TPageControl [1]
              Width = 414
              inherited tsFieldValues: TTabSheet
                inherited vleValues: TValueListEditor
                  Width = 406
                  ColWidths = (
                    47
                    353)
                end
              end
            end
          end
          inherited lbCollection: TListBox
            Width = 413
            Height = 200
          end
          inherited btnAddItem: TButton
            Left = 255
            Top = 206
          end
          inherited btnDeleteItem: TButton
            Left = 339
            Top = 206
          end
        end
      end
    end
    inherited tsPrimaryKey: TTabSheet
      DesignSize = (
        424
        466)
      object lPrimaryKey: TLabel
        Left = 6
        Top = 6
        Width = 55
        Height = 13
        Caption = 'Primary Key'
      end
      object lLogicalKey: TLabel
        Left = 6
        Top = 162
        Width = 55
        Height = 13
        Caption = 'Logical Key'
      end
      inline ddPrimaryKeyEdit: TDataDictPrimaryKeyEdit
        Left = 6
        Top = 23
        Width = 417
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 417
          Height = 109
          DesignSize = (
            417
            109)
          inherited lbCollection: TListBox
            Width = 328
            Height = 109
          end
          inherited btnAddItem: TButton
            Left = 336
          end
          inherited btnDeleteItem: TButton
            Left = 336
          end
        end
      end
      inline ddLogicalKeyEdit: TDataDictLogicalKeyEdit
        Left = 7
        Top = 179
        Width = 417
        Height = 109
        Anchors = [akLeft, akTop, akRight]
        AutoScroll = False
        TabOrder = 1
        inherited pnlContainer: TPanel
          Width = 417
          Height = 109
          DesignSize = (
            417
            109)
          inherited lbCollection: TListBox
            Width = 328
            Height = 109
          end
          inherited btnAddItem: TButton
            Left = 336
          end
          inherited btnDeleteItem: TButton
            Left = 336
          end
        end
      end
    end
    inherited tsForeignKey: TTabSheet
      inline ddForeignKeyEdit: TddForeignKeyEdit
        Left = 6
        Top = 6
        Width = 412
        Height = 236
        AutoScroll = False
        TabOrder = 0
        inherited pnlContainer: TPanel
          Width = 412
          Height = 236
          inherited lbCollection: TListBox
            Width = 328
            Height = 236
          end
          inherited btnAddItem: TButton
            Left = 336
          end
          inherited btnDeleteItem: TButton
            Left = 336
          end
          inherited btnEdit: TButton
            Left = 336
          end
        end
      end
    end
  end
  inherited btnCancel: TButton
    Left = 261
    Top = 459
    Enabled = True
  end
  inherited btnOK: TButton
    Left = 176
    Top = 459
    Enabled = True
  end
  inherited btnApply: TButton
    Left = 345
    Top = 459
    Enabled = True
  end
end
