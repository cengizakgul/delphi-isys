inherited ddForeignKeyEdit: TddForeignKeyEdit
  Width = 313
  Height = 146
  inherited pnlContainer: TPanel
    Width = 313
    Height = 146
    DesignSize = (
      313
      146)
    inherited lbCollection: TListBox
      Width = 229
      Height = 146
      Anchors = [akLeft, akTop, akRight, akBottom]
    end
    inherited btnAddItem: TButton
      Left = 237
      Top = 0
      Anchors = [akTop, akRight]
      Caption = 'Add Key'
    end
    inherited btnDeleteItem: TButton
      Left = 237
      Top = 53
      Anchors = [akTop, akRight]
      Caption = 'Delete Key'
      TabOrder = 3
    end
    object btnEdit: TButton
      Left = 237
      Top = 27
      Width = 75
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'Edit Key'
      TabOrder = 2
      OnClick = btnEditClick
    end
  end
end
