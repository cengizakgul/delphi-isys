unit ddParamsEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictCollectionEditFrm, StdCtrls, DataDictItemEditFrm,
  DataDictNamedItemEditFrm, DataDictParamEditFrm, rwCustomDataDictionary,
  ExtCtrls, rwEngineTypes;

type
  TddParamsEdit = class(TDataDictCollectionEdit)
    ParamInfo: TDataDictParamEdit;
    procedure ParamInfoedNameExit(Sender: TObject);
  protected
    function  GetItemText(const AItem: TCollectionItem): String; override;
    procedure SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean); override;
  public
    procedure AfterConstruction; override;
  end;

implementation

{$R *.dfm}

{ TDataDictParamsEdit }

procedure TddParamsEdit.AfterConstruction;
begin
  inherited;
  FItemFrame := ParamInfo;
end;


function TddParamsEdit.GetItemText(const AItem: TCollectionItem): String;
begin
  Result := TDataDictParam(AItem).Name;
end;

procedure TddParamsEdit.SetDefaultValues(const AItem: TCollectionItem; var Accept: Boolean);
begin
  TDataDictParam(AItem).Name := 'New_Param';
  TDataDictParam(AItem).ParamType := ddtString;
end;

procedure TddParamsEdit.ParamInfoedNameExit(Sender: TObject);
begin
  ParamInfo.edNameExit(Sender);
  UpdateItem(SelectedItem);
end;

end.
