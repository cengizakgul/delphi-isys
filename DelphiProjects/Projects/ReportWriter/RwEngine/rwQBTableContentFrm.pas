unit rwQBTableContentFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, rwDataDictionary, sbAPI, rwLogQuery,  rwUtils, rwDB, rwInputFormControls,
  kbmMemTable, rwDesignClasses;

type
  TrwQBTableContent = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    FGrid: TrwDBGrid;
    FResultRec: TsbResultRec;
    FDataSet: TrwQuery;
    procedure CreateQuery(AObject: TrwDataDictTable); overload;
    procedure CreateQuery(ASQL: string; AParams: TsbParams; ADataSet: TrwQuery); overload;
    procedure FixDisplayFields(ADataSet: TDataSet);
  public
  end;

  procedure ShowTableContents(AObject: TrwDataDictTable);
  procedure ShowSQLContents(ASQL: string;  AParams: TsbParams); overload;
  function  ShowSQLContents(ASQL: string;  AParams: TsbParams; ADataSet: TrwQuery): TsbResultRec; overload;
//  function  ShowSQLContents(ASQL: string;  AParams: TsbParams; ADataSet: TISRWClientDataSet): TsbResultRec; overload;
  procedure ShowDataSetContents(ADataSet: TrwDataSet);

implementation

{$R *.DFM}

procedure ShowTableContents(AObject: TrwDataDictTable);
var
  Frm: TrwQBTableContent;
begin
  Frm := TrwQBTableContent.Create(nil);
  try
    Frm.Caption := AObject.Name;
    Frm.CreateQuery(AObject);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;


procedure ShowSQLContents(ASQL: string; AParams: TsbParams);
var
  Frm: TrwQBTableContent;
begin
  Frm := TrwQBTableContent.Create(nil);
  try
    Frm.Caption := 'SQL';
    Frm.CreateQuery(ASQL, AParams, Frm.FDataSet);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;


function ShowSQLContents(ASQL: string;  AParams: TsbParams; ADataSet: TrwQuery): TsbResultRec;
var
  Frm: TrwQBTableContent;
begin
  Frm := TrwQBTableContent.Create(nil);
  try
    Frm.CreateQuery(ASQL, AParams, ADataSet);
    Result := Frm.FResultRec;
  finally
    Frm.Free;
  end;
end;

{
function  ShowSQLContents(ASQL: string;  AParams: TsbParams; ADataSet: TISRWClientDataSet): TsbResultRec;
var
  Frm: TrwQBTableContent;
begin
  Frm := TrwQBTableContent.Create(nil);
  try
    Frm.CreateQuery(ASQL, AParams, Frm.FDataSet);
    Result := Frm.FResultRec;
    ADataSet.LoadFromDataSet(Frm.FDataSet.VCLDataSet, [mtcpoStructure, mtcpoOnlyActiveFields]);
  finally
    Frm.Free;
  end;
end;
}

procedure ShowDataSetContents(ADataSet: TrwDataSet);
var
  Frm: TrwQBTableContent;
begin
  Frm := TrwQBTableContent.Create(nil);
  try
    Frm.Caption := 'Data Result';
    Frm.FGrid.DataSource := ADataSet;
    Frm.WindowState := wsMaximized;
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;


{ TrwQBTableContent }

procedure TrwQBTableContent.CreateQuery(AObject: TrwDataDictTable);
var
  SQL: String;
begin
  Busy;
  try
    SQL := 'SELECT * FROM '+AObject.Name;
    if TrwDataDictParams(AObject.Params).ParamStr <> '' then
      SQL := SQL + '(' + TrwDataDictParams(AObject.Params).ParamValueStr +')';
    SQL := SQL + ' t';

    FDataSet.Close;
    FDataSet.SQL.Text := SQL;
    FDataSet.Open;
    FixDisplayFields(FDataSet.VCLDataSet);
  finally
    Ready;
  end;
end;


procedure TrwQBTableContent.CreateQuery(ASQL: string; AParams: TsbParams; ADataSet: TrwQuery);
var
  DS: TrwQuery;
begin
  Busy;
  try
    if Assigned(ADataSet) then
      DS := ADataSet
    else
      DS := FDataSet;
    DS.SQL.Text := ASQL;
    if Assigned(AParams) then
      TrwLQQuery(DS.DataSet).Params.Assign(AParams);
    if Assigned(ADataSet) then
      DS.Open
    else
      DS.PExecute;
    FResultRec := TrwLQQuery(DS.DataSet).ResInfo;
    if Assigned(ADataSet) then
      FixDisplayFields(DS.VCLDataSet);
  finally
    Ready;
  end;
end;


procedure TrwQBTableContent.FixDisplayFields(ADataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to ADataSet.Fields.Count - 1 do
    if (ADataSet.Fields[i] is TFloatField) or (ADataSet.Fields[i] is TCurrencyField) then
      TFloatField(ADataSet.Fields[i]).DisplayFormat := ''
    else if (ADataSet.Fields[i] is TStringField) and
            (TStringField(ADataSet.Fields[i]).DisplayWidth > 40)  then
      TStringField(ADataSet.Fields[i]).DisplayWidth := 40;
end;


procedure TrwQBTableContent.FormCreate(Sender: TObject);
begin
  FDataSet := TrwQuery.Create(nil);
  FGrid := TrwDBGrid.Create(nil);
  FGrid.MultiSelection := False;
  FGrid.VisualControl.RealObject.Parent := Self;
  FGrid.Align := alClient;
  FGrid.DataSource := FDataSet;
end;

procedure TrwQBTableContent.FormDestroy(Sender: TObject);
begin
  FDataSet.Close;
  FGrid.Free;
  FDataSet.Free;
end;

procedure TrwQBTableContent.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_ESCAPE then
    Close;
end;

end.
