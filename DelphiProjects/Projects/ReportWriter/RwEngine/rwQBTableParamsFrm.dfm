object rwQBTableParams: TrwQBTableParams
  Left = 436
  Top = 211
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Table Parameters'
  ClientHeight = 330
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 97
    Width = 387
    Height = 8
    Cursor = crVSplit
    Align = alTop
  end
  object Panel1: TPanel
    Left = 0
    Top = 298
    Width = 387
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      387
      32)
    object btnOK: TButton
      Left = 229
      Top = 4
      Width = 65
      Height = 23
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 310
      Top = 4
      Width = 65
      Height = 23
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object lvParams: TListView
    Left = 0
    Top = 0
    Width = 387
    Height = 97
    Align = alTop
    Columns = <
      item
        AutoSize = True
        Caption = 'Parameter'
      end
      item
        Caption = 'Type'
        Width = 80
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnSelectItem = lvParamsSelectItem
  end
  object pcParam: TPageControl
    Left = 0
    Top = 105
    Width = 387
    Height = 193
    ActivePage = tsConst
    Align = alClient
    Style = tsButtons
    TabOrder = 1
    object tsConst: TTabSheet
      Caption = 'Constant Value'
      OnShow = tsConstShow
      object nbConstType: TNotebook
        Left = 0
        Top = 0
        Width = 379
        Height = 162
        Align = alClient
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'StringAndDate'
          object Label1: TLabel
            Left = 4
            Top = 18
            Width = 78
            Height = 13
            Caption = 'Parameter Value'
          end
          object cbLookup: TisRWDBComboBox
            Left = 101
            Top = 15
            Width = 266
            Height = 21
            ShowButton = True
            Style = csDropDown
            MapList = True
            AllowClearKey = False
            DropDownCount = 8
            ItemHeight = 0
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnExit = cbLookupExit
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Boolean'
          object rgrBoolValue: TRadioGroup
            Left = 4
            Top = 9
            Width = 101
            Height = 72
            Caption = 'Parameter Value'
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 0
            OnExit = rgrBoolValueExit
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'List'
        end
      end
    end
    object tsParam: TTabSheet
      Caption = 'External Parameter'
      ImageIndex = 1
      OnShow = tsParamShow
      object Label4: TLabel
        Left = 4
        Top = 18
        Width = 89
        Height = 13
        Caption = 'External Parameter'
      end
      object cbParamList: TisRWDBComboBox
        Left = 101
        Top = 15
        Width = 266
        Height = 21
        ShowButton = True
        Style = csDropDown
        MapList = True
        AllowClearKey = False
        DropDownCount = 8
        ItemHeight = 0
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
        OnExit = cbParamListExit
      end
    end
    object tsQBExpression: TTabSheet
      Caption = 'QB Expression'
      ImageIndex = 2
      OnShow = tsQBExpressionShow
      inline ExprPnl: TrwQBExprPanel
        Left = 0
        Top = 0
        Width = 379
        Height = 162
        Align = alClient
        AutoScroll = False
        PopupMenu = ExprPnl.pmClipbrd
        TabOrder = 0
        inherited pnlCanvas: TPanel
          Width = 379
          Height = 162
          OnClick = ExprPnlpnlCanvasClick
        end
      end
    end
    object tsNull: TTabSheet
      Caption = 'Empty'
      ImageIndex = 3
      OnShow = tsNullShow
    end
  end
end
