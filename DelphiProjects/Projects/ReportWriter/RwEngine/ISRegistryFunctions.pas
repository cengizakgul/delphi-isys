unit ISRegistryFunctions;

interface

uses
  Registry, Windows, SysUtils, Forms, Classes, IniFiles;

function ReadKeyFieldNames(const Key: string; KeyPath: String = ''): TStringList;
function ReadFromRegistry(const Key, KeyField: string; DefaultValue: string; KeyPath: String = ''): string;
function ReadSectionFromRegistry(const Key: string; var List: TStringList; KeyPath: String = ''): string;
procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; KeyPath: String = '');
function CheckKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = ''): boolean;
procedure CleanKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = '');
procedure CleanKeyInRegistry(const Key: string; KeyPath: String = '');
function ReadFromConnectIni(const Key, KeyField: string; DefaultValue: string): string;

procedure RegisterDefaultApp(const FileExt, AppID, AppDescription : String);



const
  ISYSTEMS_MISC_SETTINGS = 'Misc\';

var
  ISYSTEMS_REGISTRY: string;
const
  sRootKey = HKEY_CURRENT_USER;

implementation

function ReadFromRegistry(const Key, KeyField: string; DefaultValue: string; KeyPath: String = ''): string;
var
  TempRegistry: TRegistry;
begin
  Result := DefaultValue;

  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) and
         ValueExists(KeyField) then
        Result := ReadString(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function ReadKeyFieldNames(const Key: string; KeyPath: String = ''): TStringList;
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  Result := TStringList.Create;
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;
      if OpenKey(KeyPath + Key, False) then
        GetValueNames(Result);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function ReadSectionFromRegistry(const Key: string; var List: TStringList; KeyPath: String = ''): string;
var
  TempRegistry: TRegIniFile;
begin
  Result := '';

  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegIniFile.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        ReadSection('', List);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; KeyPath: String = '');
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;
      if OpenKey(KeyPath + Key, NewValue <> '') then
        if NewValue = '' then
          DeleteValue(KeyField)
        else
          WriteString(KeyField, NewValue);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure CleanKeyInRegistry(const Key: string; KeyPath: String = ''); 
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath, False) then
        DeleteKey(Key);
    end
  finally
    TempRegistry.Free;
  end;
end;

procedure CleanKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = '');
var
  TempRegistry: TRegistry;
begin
  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        if ValueExists(KeyField) then
          DeleteValue(KeyField);
    end
  finally
    TempRegistry.Free;
  end;
end;

function CheckKeyFieldInRegistry(const Key, KeyField: string; KeyPath: String = ''): boolean;
var
  TempRegistry: TRegistry;
begin
  Result := False;

  if KeyPath = '' then
    KeyPath := ISYSTEMS_REGISTRY;

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := sRootKey;

      if OpenKey(KeyPath + Key, False) then
        Result := ValueExists(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;

function GetFileName(FileName: string): string;
begin
  Result := ExtractFileName(FileName);
  if Pos('.', Result) > 0 then
    Result := Copy(Result, 1, Pred(Pos('.', Result)));
end;

function ReadFromConnectIni(const Key, KeyField: string; DefaultValue: string): string;
begin
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'connect.ini') do
  try
    Result := ReadString(Key, KeyField, DefaultValue);
  finally
    Free;
  end;
end;

procedure RegisterDefaultApp(const FileExt, AppID, AppDescription : String);
var
  TempRegistry: TRegistry;
begin
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      try
        RootKey := HKEY_CLASSES_ROOT;
        OpenKey(FileExt, True);
        WriteString('', AppID);
        CloseKey;
        OpenKey(AppID, True);
        WriteString('', AppDescription);
        CloseKey;
        OpenKey(AppID + '\shell\open\command', True);
        WriteString('', Application.ExeName + ' "%1"');
      except
        // it's done in case if user is not administrator
      end;
    end;
  finally
    TempRegistry.Free;
  end;
end;



initialization
  ISYSTEMS_REGISTRY := 'SOFTWARE\iSystems\' + GetFileName(Application.ExeName) + '\';

end.
