unit rwQBConditionFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, rwQBExpressionFrm, ExtCtrls, Grids, rwQBTableFrm,
  rwQBInfo, Db, rwDataDictionary, Buttons, rwTypes, rwDB, sbSQL,
  rwDesignClasses, kbmMemTable, ISKbmMemDataSet, rwEngineTypes,
  ISBasicClasses;

type
  TrwQBCondition = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    GroupBox2: TGroupBox;
    pcOper: TPageControl;
    tsExpression: TTabSheet;
    GroupBox1: TGroupBox;
    cbOper: TComboBox;
    chbNot: TCheckBox;
    GroupBox3: TGroupBox;
    tsBetween: TTabSheet;
    lAnd: TLabel;
    tsLike: TTabSheet;
    gbOperAux: TGroupBox;
    cbLikeCS: TCheckBox;
    rbLikeBeg: TRadioButton;
    rbLikeAny: TRadioButton;
    tsINCustom: TTabSheet;
    lbList: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    exprLeft: TrwQBExpress;
    exprRight1: TrwQBExpress;
    exprRight2: TrwQBExpress;
    Label2: TLabel;
    exprBtw1: TrwQBExpress;
    exprBtw2: TrwQBExpress;
    pnlSUBQuery: TPanel;
    Label1: TLabel;
    cbSUBQuery: TComboBox;
    btnCheck: TButton;
    procedure cbOperChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure chbNotClick(Sender: TObject);
    procedure gbOperAuxExit(Sender: TObject);
    procedure pnlExprpnlCanvasClick2(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure tsINCustomShow(Sender: TObject);
    procedure tsBetweenResize(Sender: TObject);
    procedure tsExpressionShow(Sender: TObject);
    procedure tsBetweenShow(Sender: TObject);
    procedure tsLikeShow(Sender: TObject);
    procedure dsSourceBeforeRefresh(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbSUBQueryChange(Sender: TObject);
    procedure btnCheckClick(Sender: TObject);
  private
    FConditions: TrwQBFilterNodeCollection;
    FCondition: TrwQBFilterCondition;
    FTable: TrwQBTable;
    FDSSource: TrwDataSet;
    procedure CreateOperList;
    procedure CreateSUBQueryList;
    procedure CreateAvailableFieldValues;
    procedure SyncInList;
    procedure FixData;
    procedure CheckErrors;
    function  DSSource: TrwDataset;
  public
  end;


  function QBCondition(ATable: TrwQBTable; ACondition: TrwQBFilterCondition): Boolean;


implementation

{$R *.DFM}

uses
  rwQBExprEditorFrm, rwQBTableContentFrm;


var
  FOperations: array [0..10] of String = ('=', '<>', '>', '<', '>=', '<=',
                                          'LIKE', 'BETWEEN', 'IN', 'EXISTS', 'IS NULL');


function QBCondition(ATable: TrwQBTable; ACondition: TrwQBFilterCondition): Boolean;
var
  Frm: TrwQBCondition;
begin
  Result := False;

  Frm := TrwQBCondition.Create(nil);
  with Frm do
    try
      FTable := ATable;
      CreateSUBQueryList;

      FCondition.Assign(ACondition);
      FConditions.Table := ATable.Table;

      if Length(FCondition.OperationAux) > 0 then
        if AnsiSameText(FCondition.Operation, 'LIKE') then
        begin
          if FCondition.OperationAux[1] = 'B' then
            rbLikeBeg.Checked := True
          else if FCondition.OperationAux[1] = 'A' then
            rbLikeAny.Checked := True;
          if FCondition.OperationAux[2] = '0' then
            cbLikeCS.Checked := False
          else
            cbLikeCS.Checked := True;
        end;

      cbOper.ItemIndex := cbOper.Items.IndexOf(FCondition.Operation);
      cbOper.OnChange(nil);

      chbNot.Checked := FCondition.NotOper;

      if ShowModal = mrOK then
      begin
        Result := True;
        FixData;
        ACondition.Assign(FCondition);
        FCondition.OperationAux := '';
      end;
    finally
      Free;
    end;
end;


procedure TrwQBCondition.CreateOperList;
begin
  cbOper.Clear;
  cbOper.Items.AddObject(FOperations[0], Pointer(0));
  cbOper.Items.AddObject(FOperations[1], Pointer(1));
  cbOper.Items.AddObject(FOperations[2], Pointer(2));
  cbOper.Items.AddObject(FOperations[3], Pointer(3));
  cbOper.Items.AddObject(FOperations[4], Pointer(4));
  cbOper.Items.AddObject(FOperations[5], Pointer(5));
  cbOper.Items.AddObject(FOperations[6], Pointer(6));
  cbOper.Items.AddObject(FOperations[7], Pointer(7));
  cbOper.Items.AddObject(FOperations[8], Pointer(8));
  cbOper.Items.AddObject(FOperations[9], Pointer(9));
  cbOper.Items.AddObject(FOperations[10], Pointer(10));
  cbOper.ItemIndex := 0;
  cbOper.OnChange(nil);
end;


procedure TrwQBCondition.cbOperChange(Sender: TObject);
var
  i: Integer;
begin
  FCondition.Operation := AnsiUpperCase(cbOper.Text);

  case cbOper.ItemIndex of
    0, 1, 2,
    3, 4, 5: begin
               FCondition.OperationAux := '';
               pcOper.ActivePage := tsExpression;
             end;

    6:       begin
               pcOper.ActivePage := tsLike;
               gbOperAux.OnExit(nil);
             end;

    7:       begin
               FCondition.OperationAux := '';
               pcOper.ActivePage := tsBetween;
             end;

    8:       begin
               FCondition.OperationAux := '';
               pcOper.ActivePage := tsINCustom;
             end;

    9:       pcOper.ActivePage := nil;

    10:      begin
               pcOper.ActivePage := nil;
               FCondition.OperationAux := '';
             end;
  end;

  pnlSUBQuery.Visible := (cbOper.ItemIndex = 9);
  exprLeft.Visible := not pnlSUBQuery.Visible;

  if pnlSUBQuery.Visible then
  begin
    if FCondition.OperationAux <> '' then
      for i := 0 to cbSUBQuery.Items.Count - 1 do
      begin
        if AnsiSameText(TrwQBSelectedTable(cbSUBQuery.Items.Objects[i]).InternalName,
           FCondition.OperationAux) then
        begin
          cbSUBQuery.ItemIndex := i;
          break;
        end;
      end
    else
      cbSUBQuery.ItemIndex := -1;

    cbSUBQuery.OnChange(nil);
  end;
end;

procedure TrwQBCondition.FormCreate(Sender: TObject);
begin
  FConditions := TrwQBFilterNodeCollection.Create(TrwQBFilterCondition);
  FCondition := TrwQBFilterCondition(FConditions.Add);
  exprLeft.Expression := FCondition.LeftPart;
  exprLeft.DefaultType := eitField;
  exprLeft.DefaultValue := '';

  exprRight1.Expressions := FCondition.RightPart;
  exprRight1.ExpressionIndex := 0;
  exprRight1.DefaultType := eitField;
  exprRight1.DefaultValue := '';
  exprRight1.LookUpDataSet := dsSource;

  exprRight2.Expressions := FCondition.RightPart;
  exprRight2.ExpressionIndex := 0;
  exprRight2.DefaultType := eitStrConst;
  exprRight2.DefaultValue := '';
  exprRight2.LookUpDataSet := dsSource;

  exprBtw1.Expressions := FCondition.RightPart;
  exprBtw1.ExpressionIndex := 0;
  exprBtw1.DefaultType := eitStrConst;
  exprBtw1.DefaultValue := '';
  exprBtw1.LookUpDataSet := dsSource;

  exprBtw2.Expressions := FCondition.RightPart;
  exprBtw2.ExpressionIndex := 1;
  exprBtw2.DefaultType := eitStrConst;
  exprBtw2.DefaultValue := '';
  exprBtw2.LookUpDataSet := dsSource;
  
  CreateOperList;
end;

procedure TrwQBCondition.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FdsSource);
  FreeAndNil(FConditions);
end;

procedure TrwQBCondition.chbNotClick(Sender: TObject);
begin
  FCondition.NotOper := chbNot.Checked;
 
end;

procedure TrwQBCondition.gbOperAuxExit(Sender: TObject);
begin
  FCondition.OperationAux := '';

  if rbLikeBeg.Checked then
    FCondition.OperationAux := 'B'
  else
    FCondition.OperationAux := 'A';

  if cbLikeCS.Checked then
    FCondition.OperationAux := FCondition.OperationAux + '1'
  else
    FCondition.OperationAux := FCondition.OperationAux + '0';
end;

procedure TrwQBCondition.pnlExprpnlCanvasClick2(Sender: TObject);
begin
  if TrwQBExprEditor.ShowEditor(exprLeft.Expression, eitField, '', nil) then
  begin
    if Assigned(FdsSource) then
      FdsSource.Close;
    exprLeft.pnlExpr.ReDraw;
  end;
end;


procedure TrwQBCondition.Button1Click(Sender: TObject);
var
  Expr, E: TrwQBExpression;
  fl: Boolean;
  t: TrwQBExprItemType;
begin
  Expr := TrwQBExpression(FCondition.RightPart.Add);

  fl := False;
  try
    if (FCondition.RightPart.Count > 1) then
    begin
      E := TrwQBExpression(FCondition.RightPart.Items[FCondition.RightPart.Count - 2]);
      t := TrwQBExpressionItem(E.Items[0]).ItemType;
      if (E.Count = 1) and (TrwQBExpressionItem(E.Items[0]).ItemType in
           [eitField, eitParam, eitIntConst, eitFloatConst, eitStrConst, eitDateConst]) then
        Expr.Assign(E)
      else
        t := eitStrConst;
    end
    else
      t := eitStrConst;

    fl := TrwQBExprEditor.ShowEditor(Expr, t, '', dsSource);
    if fl then
      lbList.ItemIndex := lbList.Items.AddObject(Expr.TextForDesign, Expr);

  finally
    if not fl then
      Expr.Free;
  end;
end;

procedure TrwQBCondition.Button3Click(Sender: TObject);
var
  Expr: TrwQBExpression;
begin
  if lbList.ItemIndex = -1 then
    Exit;

  Expr := TrwQBExpression(lbList.Items.Objects[lbList.ItemIndex]);

  if TrwQBExprEditor.ShowEditor(Expr, eitStrConst, '', dsSource) then
    lbList.Items[lbList.ItemIndex] := Expr.TextForDesign;
end;

procedure TrwQBCondition.Button2Click(Sender: TObject);
var
  Expr: TrwQBExpression;
  i: Integer;
begin
  if lbList.ItemIndex = -1 then
    Exit;

  Expr := TrwQBExpression(lbList.Items.Objects[lbList.ItemIndex]);
  Expr.Free;
  i := lbList.ItemIndex;
  if i > lbList.Items.Count - 1 then
    i := lbList.Items.Count - 1;
  lbList.Items.Delete(lbList.ItemIndex);
  lbList.ItemIndex := i;
end;

procedure TrwQBCondition.CreateAvailableFieldValues;
var
  Tbl: TrwQBSelectedTable;
begin
  if Assigned(FdsSource) then
    if FdsSource.PActive then
      Exit
    else
      FreeAndNil(FdsSource);

  if (FCondition.LeftPart.Count = 1) and (FCondition.LeftPart.Items[0].ItemType = eitField) then
  begin
    Tbl := TrwQBSelectedTables(FConditions.Table.Collection).TableByInternalName(FCondition.LeftPart.Items[0].Description);
    FdsSource := CreateLookupList(Tbl.lqObject, FCondition.LeftPart.Items[0].Value);
  end;
end;

procedure TrwQBCondition.SyncInList;
var
  i: Integer;
begin
  lbList.Clear;
  for i := 0 to FCondition.RightPart.Count - 1 do
    lbList.Items.AddObject(TrwQBExpression(FCondition.RightPart.Items[i]).TextForDesign,
      FCondition.RightPart.Items[i]);
end;

procedure TrwQBCondition.tsINCustomShow(Sender: TObject);
begin
  SyncInList;
end;

procedure TrwQBCondition.tsBetweenResize(Sender: TObject);
begin
  exprBtw1.Width := (tsBetween.Width - exprBtw1.Left * 2 - lAnd.Width) div 2;
  lAnd.Left := exprBtw1.Left + exprBtw1.Width;
  lAnd.Top := (exprBtw1.Height - lAnd.Height) div 2;
  exprBtw2.Width := exprBtw1.Width;
  exprBtw2.Left := lAnd.Left + lAnd.Width;
end;

procedure TrwQBCondition.tsExpressionShow(Sender: TObject);
begin
  if FCondition.RightPart.Count > 0 then
    exprRight1.Expression := TrwQBExpression(FCondition.RightPart.Items[0])
  else
    exprRight1.Expression := nil;
  exprRight1.pnlExpr.Redraw;    
end;

procedure TrwQBCondition.tsBetweenShow(Sender: TObject);
begin
  if FCondition.RightPart.Count >= 1 then
    exprBtw1.Expression := TrwQBExpression(FCondition.RightPart.Items[0])
  else
    exprBtw1.Expression := nil;
  exprBtw1.pnlExpr.Redraw;

  if FCondition.RightPart.Count >= 2 then
    exprBtw2.Expression := TrwQBExpression(FCondition.RightPart.Items[1])
  else
    exprBtw2.Expression := nil;
  exprBtw2.pnlExpr.Redraw;    
end;

procedure TrwQBCondition.tsLikeShow(Sender: TObject);
begin
  if FCondition.RightPart.Count > 0 then
    exprRight2.Expression := TrwQBExpression(FCondition.RightPart.Items[0])
  else
    exprRight2.Expression := nil;
  exprRight2.pnlExpr.Redraw;
end;


procedure TrwQBCondition.dsSourceBeforeRefresh(DataSet: TDataSet);
begin
  CreateAvailableFieldValues;
end;

procedure TrwQBCondition.FixData;
begin
  with FCondition do
  begin
    if (Pos('=', Operation) > 0) or (Pos('<', Operation) > 0) then
    begin
      OperationAux := '';
      while RightPart.Count > 1 do
        RightPart.Items[1].Free;
    end

    else if Operation = 'IS NULL' then
    begin
      OperationAux := '';
      RightPart.Clear;
    end

    else if Operation = 'BETWEEN' then
    begin
      OperationAux := '';
      while RightPart.Count > 2 do
        RightPart.Items[2].Free;
    end

    else if Operation = 'IN' then
      OperationAux := ''

    else if Operation = 'LIKE' then
    begin
      while RightPart.Count > 1 do
        RightPart.Items[1].Free;
    end

    else if Operation = 'EXISTS' then
    begin
      LeftPart.Clear;
      RightPart.Clear;
    end;
  end;
end;


procedure TrwQBCondition.CheckErrors;
const errTM = 'Types of left and right parts mismatch';
var
  Err: String;
  i: Integer;
begin
  Err := '';
  with FCondition do
  begin
    if (LeftPart.Count = 0) and (Operation <> 'EXISTS') then
      Err := 'Left part of the condition is empty';

    if (Pos('=', Operation) > 0) or (Pos('<', Operation) > 0) or
       (Operation = 'LIKE') or (Operation = 'IN') then
    begin
      if (RightPart.Count = 0) or (TrwQBExpression(RightPart.Items[0]).Count = 0)  then
        Err := 'Right part of the condition is empty';
    end

    else if Operation = 'BETWEEN' then
    begin
      if (RightPart.Count < 2) or
         (TrwQBExpression(RightPart.Items[0]).Count = 0) or
         (TrwQBExpression(RightPart.Items[1]).Count = 0)  then
        Err := 'Right part of the condition is incorrect';
    end

    else if (Operation = 'EXISTS') and (OperationAux = '') then
      Err := 'SubQuery is empty';


    if Err = '' then
    begin
      if (Pos('=', Operation) > 0) or (Pos('<', Operation) > 0) then
      begin
        if not LeftPart.CheckTypeCompatibility(LeftPart.ExprType,
           TrwQBExpression(RightPart.Items[0]).ExprType, True) then
          Err := errTM;
      end

      else if Operation = 'LIKE' then
      begin
        if not (LeftPart.ExprType in [rwvString, rwvVariant]) or
           not (TrwQBExpression(RightPart.Items[0]).ExprType in [rwvString, rwvVariant]) then
          Err := 'Left and right parts must by a String';   
      end

      else if Operation = 'BETWEEN' then
      begin
        if not LeftPart.CheckTypeCompatibility(LeftPart.ExprType,
           TrwQBExpression(RightPart.Items[0]).ExprType) then
          Err := errTM
        else if not LeftPart.CheckTypeCompatibility(LeftPart.ExprType,
           TrwQBExpression(RightPart.Items[1]).ExprType) then
          Err := errTM;
      end

      else if Operation = 'IN' then
      begin
        for i := 0 to RightPart.Count -1 do
          if not LeftPart.CheckTypeCompatibility(LeftPart.ExprType,
             TrwQBExpression(RightPart.Items[i]).ExprType, True) then
          begin
            Err := 'Types of left part and item #' + IntToStr(i + 1) +' mismatch';
            break;
          end;
      end;
    end;


    if Err <> '' then
      raise ErwException.Create(Err);
  end;
end;

procedure TrwQBCondition.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then
  begin
    CanClose := False;
    CheckErrors;
    CanClose := True;
  end;
end;

procedure TrwQBCondition.cbSUBQueryChange(Sender: TObject);
begin
  if cbSUBQuery.ItemIndex = -1 then
    FCondition.OperationAux := ''
  else
    FCondition.OperationAux := TrwQBSelectedTable(cbSUBQuery.Items.Objects[cbSUBQuery.ItemIndex]).InternalName;
end;

procedure TrwQBCondition.CreateSUBQueryList;
var
  i, j: Integer;
  fl: Boolean;
begin
  cbSUBQuery.Clear;
  for i := 0 to TrwQBSelectedTables(FTable.Table.Collection).Count - 1 do
    if TrwQBSelectedTables(FTable.Table.Collection)[i].IsSUBQuery and
       (TrwQBSelectedTables(FTable.Table.Collection)[i] <> FTable.Table) then
    begin
      fl := True;
      for j := 0 to TrwQBSelectedTables(FTable.Table.Collection).QBQuery.Joins.Count - 1 do
        if (TrwQBSelectedTables(FTable.Table.Collection).QBQuery.Joins[j].LeftTable = TrwQBSelectedTables(FTable.Table.Collection)[i])
            or
           (TrwQBSelectedTables(FTable.Table.Collection).QBQuery.Joins[j].RightTable = TrwQBSelectedTables(FTable.Table.Collection)[i]) then
        begin
          fl := False;
          break;
        end;

      if fl then
        cbSUBQuery.Items.AddObject(TrwQBSelectedTables(FTable.Table.Collection)[i].Alias, TrwQBSelectedTables(FTable.Table.Collection)[i]);
    end;
end;

procedure TrwQBCondition.btnCheckClick(Sender: TObject);
begin
  CheckErrors;
end;

function TrwQBCondition.DSSource: TrwDataset;
begin
  CreateAvailableFieldValues;
  Result := FdsSource;
end;

end.
