unit rwGraphics;

interface

uses Classes, Windows, Graphics, StdCtrls, SysUtils, Math, Variants, rwEngineTypes, rwBasicUtils, rwTypes,
     EvStreamUtils;

const
    cVirtualCanvasRes = 600;  // Everything gets calculated in this resolution
    cScreenCanvasRes = 96;    // The constant! ever!
    cCanvasToScreenZoomRatio = cVirtualCanvasRes / cScreenCanvasRes;

type
  TrwGraphicObject = class(TrwPersistent)
  private
    FHandle: THandle;
    function  GetHandle: THandle;
    procedure DestroyHandle;    
  protected
    procedure Changed; override;
    function  CreateHandle: THandle; virtual; abstract;
  public
    destructor Destroy; override;
    function TheSame(AObject: TrwGraphicObject): Boolean; virtual;
    property Handle: THandle read GetHandle;
  end;


  TrwFont = class(TrwGraphicObject)
  private
    FTextAngle: Integer;
    FHeight: Integer;
    FName: String;
    FColor: TColor;
    FCharset: TFontCharset;
    FPitch: TFontPitch;
    FStyle: TFontStyles;
    FHeightInPixels: Integer;
    procedure SetTextAngle(const Value: Integer);
    function  GetSize: Integer;
    procedure SetCharset(const Value: TFontCharset);
    procedure SetColor(const Value: TColor);
    procedure SetHeight(const Value: Integer);
    procedure SetName(const Value: String);
    procedure SetPitch(const Value: TFontPitch);
    procedure SetSize(const Value: Integer);
    procedure SetStyle(const Value: TFontStyles);
  protected
    function CreateHandle: THandle; override;
    procedure Changed; override;
  public
    constructor Create(AOwner: TPersistent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure Assign(Source: TPersistent); override;
    function TheSame(AObject: TrwGraphicObject): Boolean; override;
    function HeightInPixels: Integer;
  published
    property Charset: TFontCharset read FCharset write SetCharset;
    property Color: TColor read FColor write SetColor;
    property Height: Integer read FHeight write SetHeight;
    property Name: String read FName write SetName;
    property Pitch: TFontPitch read FPitch write SetPitch;
    property Size: Integer read GetSize write SetSize stored False;
    property Style: TFontStyles read FStyle write SetStyle;
    property TextAngle: Integer read FTextAngle write SetTextAngle;
  end;


  TrwPen = class(TrwGraphicObject)
  private
    FColor: TColor;
    FMode:  TPenMode;
    FStyle: TPenStyle;
    FWidth: Integer;
    procedure SetColor(Value: TColor);
    procedure SetMode(Value: TPenMode);
    procedure SetStyle(Value: TPenStyle);
    procedure SetWidth(Value: Integer);
  protected
    function CreateHandle: THandle; override;
  public
    constructor Create(AOwner: TPersistent); override;
    procedure Assign(Source: TPersistent); override;
  published
    property Color: TColor read FColor write SetColor;
    property Mode:  TPenMode read FMode write SetMode;
    property Style: TPenStyle read FStyle write SetStyle;
    property Width: Integer read FWidth write SetWidth;
  end;


  TrwBrush = class(TrwGraphicObject)
  private
    FColor: TColor;
    FStyle: TBrushStyle;
    FBitmap: TBitmap;
    procedure SetStyle(const Value: TBrushStyle);
    procedure SetColor(Value: TColor);
    function  GetBitmap: TBitmap;
    procedure SetBitmap(const Value: TBitmap);
    function  BitmapNotEmpty: Boolean;
  protected
    function CreateHandle: THandle; override;
  public
    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Color:  TColor read FColor write SetColor;
    property Style:  TBrushStyle read FStyle write SetStyle;
    property Bitmap: TBitmap read GetBitmap write SetBitmap stored BitmapNotEmpty;
  end;


  TrwPictureHolder = class(TrwGraphicObject)
  private
    FPictureHandler: TPicture;
    FData:    IevDualStream;
    function  GetBitmap: TBitmap;
    function  GetGraphic: TGraphic;
    function  GetMetafile: TMetafile;
    function  GetHeight: Integer;
    function  GetWidth: Integer;
    procedure OnChangePictureHandler(Sender: TObject);
    procedure ReadData(Stream: TStream);
    procedure WriteData(Stream: TStream);
    procedure SyncPictureContent;
    procedure PictureToStream(APicture: TPicture; AStream: IEvDualStream);
  protected
    function  CreateHandle: THandle; override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure CreatePictureHandler;
  public
    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    procedure DestroyPictureHandler;
    function  PictureIsTheSame(APicture: TrwPictureHolder): Boolean; overload;
    function  PictureIsTheSame(APicture: TPicture): Boolean; overload;

    property PictureHandler: TPicture read FPictureHandler;
    property Bitmap: TBitmap read GetBitmap;
    property Graphic: TGraphic read GetGraphic;
    property Metafile: TMetafile read GetMetafile;
    property Height: Integer read GetHeight;
    property Width: Integer read GetWidth;
  end;


  TrwCanvas = class(TPersistent)
  private
    FHandle: HDC;
    FState: TCanvasState;
    FFont: TrwFont;
    FPen: TrwPen;
    FBrush: TrwBrush;
    procedure CreateBrush;
    procedure CreateFont;
    procedure CreatePen;
    function  GetClipRect: TRect;
    function  GetHandle: HDC;
    function  GetPenPos: TPoint;
    function  GetPixel(X, Y: Integer): TColor;
    procedure SetHandle(const Value: HDC);
    procedure SetPenPos(const Value: TPoint);
    procedure SetPixel(X, Y: Integer; const Value: TColor);
    procedure SetBrush(const Value: TrwBrush);
    procedure SetFont(const Value: TrwFont);
    procedure SetPen(const Value: TrwPen);
  protected
    procedure RequiredState(ReqState: TCanvasState);
    procedure CreateHandle; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Draw(X, Y: Integer; Graphic: TGraphic);
    procedure DrawFocusRect(const Rect: TRect);
    procedure Ellipse(X1, Y1, X2, Y2: Integer); overload;
    procedure Ellipse(const Rect: TRect); overload;
    procedure FillRect(const Rect: TRect);
    procedure FrameRect(const Rect: TRect);
    function  HandleAllocated: Boolean;
    procedure LineTo(X, Y: Integer);
    procedure MoveTo(X, Y: Integer);
    procedure Rectangle(X1, Y1, X2, Y2: Integer); overload;
    procedure Rectangle(const Rect: TRect); overload;
    procedure Refresh;
    procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
    procedure StretchDraw(const Rect: TRect; Graphic: TGraphic);
    function  TextExtent(const Text: string): TSize;
    function  TextHeight(const Text: string): Integer;
    procedure TextOut(X, Y: Integer; const Text: string; ATextFlags: Integer);
    procedure TextRect(Rect: TRect; X, Y: Integer; const Text: string);
    procedure DrawText(const Text: String; ARect: TRect; ATextFlags: Integer);
    function  TextWidth(const Text: string): Integer;

    property  Brush: TrwBrush read FBrush write SetBrush;
    property  Font: TrwFont read FFont write SetFont;
    property  Pen: TrwPen read FPen write SetPen;
    property  ClipRect: TRect read GetClipRect;
    property  Handle: HDC read GetHandle write SetHandle;
    property  PenPos: TPoint read GetPenPos write SetPenPos;
    property  Pixels[X, Y: Integer]: TColor read GetPixel write SetPixel;
  end;


  TrwDigitNetRec = record
    Command: Char;
    Cells:  Integer;
    Width:  Integer;
  end;


  TrwTextDrawInfoRec = record
    CalcOnly: Boolean;
    ZoomRatio: Extended;
    Canvas: TObject;
    Text: String;               // input/output
    Font: TrwFont;
    Color: TColor;
    DigitNet: TIniString;
    WordWrap: Boolean;
    Alignment: TAlignment;
    Layout:   TTextLayout;
    TextRect: TRect;            // input/output
    LineHeight: Integer;        // output
  end;

  PTrwTextDrawInfoRec = ^TrwTextDrawInfoRec;


  TStringBoundsRec = record
    BeginPos: Integer;
    EndPos:   Integer;
  end;


procedure DrawFrame(Canvas: TCanvas; BoundLines: TrwSetColumnLines;
                    BoundLinesColor: TColor; BoundLinesWidth: Integer; BoundLinesStyle: TPenStyle;
                    Width: Integer; Height: Integer; Left: Integer = 0; Top: Integer = 0);

procedure DrawCorners(Canvas: TCanvas; Width: Integer; Height: Integer);
procedure DrawCornersRM(Canvas: TrwCanvas; Width: Integer; Height: Integer);

procedure DrawMetafile(AMetafile: TMetaFile; AHDC: THandle; KfZoom: Extended; X, Y: Integer);

procedure DrawTextImage(const pTextDrawInfoRec: PTrwTextDrawInfoRec);
procedure DrawTextImageRM(const pTextDrawInfoRec: PTrwTextDrawInfoRec);

function TextInRect(const ARect: TRect; const AText: String; const ATextHeight: Integer; const Clipping: Boolean): TStringBoundsRec;
function TextLineCount(const AText: String): Integer;

procedure DrawDiagCrossBoxRM(ACanvas: TrwCanvas; ARect: TRect; const ACaption: String);

function NTRotateMetafile90ClockWise(TheMeta: HENHMETAFILE; w,h:integer; ReferenceCanvas: HDC = 0): TMetafile;

implementation

uses rwPreviewUtils;

const
  csAllValid = [csHandleValid..csBrushValid];

function NTRotateMetafile90ClockWise(TheMeta: HENHMETAFILE; w,h:integer; ReferenceCanvas: HDC = 0): TMetafile;
var
  buf: TEnhMetaHeader;
  metaCanvas: TMetafileCanvas;
  mat : XForm;
begin
  GetEnhMetaFileHeader(TheMeta, sizeof(TEnhMetaHeader), @buf);

  Result := TMetafile.Create;
  Result.Enhanced := True;
  Result.Height :=w;
  Result.Width :=h;

  metaCanvas := TMetafileCanvas.Create(Result, ReferenceCanvas);
  try
    SetGraphicsMode(metaCanvas.Handle, GM_ADVANCED);

    mat.eM11 := 0;
    mat.eM12 := 1;
    mat.eM21 := -1;
    mat.eM22 := 0;
    mat.eDx := (buf.rclBounds.Bottom  - buf.rclBounds.Top);
    mat.eDy :=0;

    SetWorldTransform(metaCanvas.Handle, mat);
    PlayEnhMetaFile(metaCanvas.Handle, TheMeta , rect(0,0,w,h));
  finally
    metaCanvas.free;
  end;
end;

procedure DrawDiagCrossBoxRM(ACanvas: TrwCanvas; ARect: TRect; const ACaption: String);
begin
  with ACanvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clMoneyGreen;
    FillRect(ARect);

    Font.Name := 'Arial';
    Font.Height := -11;
    Font.Style := [fsBold];
    Font.Color := clBlue;
    SetBkMode(Handle, TRANSPARENT);

    DrawText(ACaption, ARect, DT_SINGLELINE	or DT_CENTER or DT_VCENTER);
  end;
end;


procedure DrawTextImage(const pTextDrawInfoRec: PTrwTextDrawInfoRec);
var
  R: TRect;
  lDrawCanvas: TCanvas;
  S, h, h1, h2: String;
  d, i: Integer;
  Ss: TSize;
  vkz, kz: Extended;
  lFastDrawing: Boolean;
  TextLineHeight: Integer;
  DC: THandle;
  SaveIndex: Integer;
  lSize: TSize;

  function ScaleScreenToDestRes(const AValue: Integer): Integer;
  begin
    Result := Round(AValue * kz);
  end;

  function ScaleCanvasToDestRes(const AValue: Integer): Integer;
  begin
    Result := Round(AValue * kz / vkz);
  end;

  procedure ScaleRect(var ARect: TRect; K: Extended);
  begin
    ARect.Left := Round(ARect.Left * k);
    ARect.Top := Round(ARect.Top * k);
    ARect.Right := Round(ARect.Right * k);
    ARect.Bottom := Round(ARect.Bottom * k);
  end;

  procedure InitComRec(ACommand: String; var ARecCom: TrwDigitNetRec);
  var
    i: Integer;
  begin
    ARecCom.Command := ACommand[1];
    i := Pos(',', ACommand);
    ARecCom.Cells := StrToInt(Copy(ACommand, 2, i-2));
    ARecCom.Width := StrToInt(Copy(ACommand, i+1, Length(ACommand)-i));
  end;


  function CalcDiginetTextRect: TRect;
  var
    d: Integer;
    RCom: TrwDigitNetRec;
    dn: String;
  begin
    Result := Rect(0, 0, 0 ,0);

    d := 0;
    dn := pTextDrawInfoRec^.DigitNet;
    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);
      Inc(d, RCom.Width * RCom.Cells);
    end;
    Result.Right := Result.Left + d;
    Result.Bottom := Result.Top + ScaleCanvasToDestRes(TextLineHeight);
  end;

  function CalcTextRect(AText: String): TRect;
  var
    sLine: String;
    lw: Integer;
  begin
    Result := Rect(0, 0, 0 ,0);

    while AText <> '' do
    begin
      sLine := GetNextStrValue(AText, #13);
      lw := lDrawCanvas.TextWidth(sLine);
      if lw > Result.Right then
        Result.Right := lw;
      Inc(Result.Bottom, TextLineHeight);
    end;

    ScaleRect(Result, kz/vkz);
  end;

  procedure DrawCellText;
  var
    RCom: TrwDigitNetRec;
    R1, R: TRect;
    h, s, dn: String;
    Len, BegCell, EndCell, Ind, x, j, d: Integer;
  begin
    lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * kz);
    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    IntersectClipRect(lDrawCanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    S := pTextDrawInfoRec^.Text;

    d := 0;
    dn := pTextDrawInfoRec^.DigitNet;
    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);
      if RCom.Command = NET_CELL then
        Inc(d, RCom.Cells);
    end;
    Ind := 1;
    BegCell := 1;
    EndCell := d;

    Len := Length(S);
    case pTextDrawInfoRec^.Alignment of
      taLeftJustify:
        begin
          Ind := 1;
          BegCell := 1;
          if Len > d then
            EndCell := d
          else
            EndCell := Len;
        end;

      taCenter:
          if Len > d then
          begin
            BegCell := 1;
            EndCell := d;
            Ind := (Len - d) div 2 + 1;
          end
          else
          begin
            Ind := 1;
            BegCell := (d - Len) div 2 + 1;
            EndCell := BegCell + Len - 1;
          end;

      taRightJustify:
        begin
          if Len > d then
          begin
            BegCell := 1;
            Ind := Len - d + 1;
          end
          else
          begin
            BegCell := d - Len + 1;
            Ind := 1;
          end;
          EndCell := d;
        end;
    end;

    if pTextDrawInfoRec^.Color <> clNone then
    begin
      lDrawCanvas.Brush.Color := pTextDrawInfoRec^.Color;
      lDrawCanvas.Brush.Style := bsSolid;
      lDrawCanvas.FillRect(R);
    end;
    SetBkMode(lDrawCanvas.Handle, TRANSPARENT);

    x := R.Left;
    d := 1;
    dn := pTextDrawInfoRec^.DigitNet;

    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);

      case RCom.Command of
        NET_CELL:
            begin
              for j := 1 to RCom.Cells do
              begin
                if (Ind > Len) or (d > EndCell) or (x >= R.Right) then
                  Exit;

                if d >= BegCell then
                begin
                  R1 := Rect(x, R.Top, x + ScaleScreenToDestRes(RCom.Width), R.Bottom);
                  h := S[Ind];
                  DrawText(lDrawCanvas.Handle, PChar(h), 1, R1, DT_CENTER or DT_NOCLIP or DT_NOPREFIX);
                  Inc(Ind);
                end;
                Inc(x, ScaleScreenToDestRes(RCom.Width));
                Inc(d);
              end;
            end;

        NET_SPACE:
          Inc(x, ScaleScreenToDestRes(RCom.Width) * RCom.Cells);
      end;
    end;
  end;


  procedure DrawPlainText(AText: String);
  var
    i, dx, dy, h: Integer;
    wn, dw: Extended;
    R: TRect;
    ChrInfo: TGCPResults;
    CharWidths: array of Integer;
    sLine: String;
    taFlag: DWord;
  begin
    lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * kz);

    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    taFlag := 0;
    case pTextDrawInfoRec^.Alignment of
      taRightJustify: begin
                        taFlag := taFlag or TA_RIGHT;
                        dx := R.Right;
                      end;

      taCenter:       begin
                        taFlag := taFlag or TA_CENTER;
                        dx := R.Left + (R.Right - R.Left ) div 2;
                      end;
    else
      taFlag := taFlag or TA_LEFT;
      dx := R.Left;
    end;

    h := 1;
    for i := 1 to Length(AText) do
      if AText[i] = #13 then
        Inc(h);

    case pTextDrawInfoRec^.Layout of
      tlBottom:  dy := R.Top + (R.Bottom - R.Top) - ScaleCanvasToDestRes(h * TextLineHeight);
      tlCenter:  dy := R.Top + ((R.Bottom - R.Top) - ScaleCanvasToDestRes(h * TextLineHeight)) div 2;
    else
      dy := R.Top;
    end;

    if pTextDrawInfoRec^.Color <> clNone then
    begin
      lDrawCanvas.Brush.Color := pTextDrawInfoRec^.Color;
      lDrawCanvas.Brush.Style := bsSolid;
      lDrawCanvas.FillRect(R);
    end;

    SetBkMode(lDrawCanvas.Handle, TRANSPARENT);

    while AText <> '' do
    begin
      sLine := GetNextStrValue(AText, #13);
      if sLine <> '' then
      begin
        lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * vkz);
        R := pTextDrawInfoRec^.TextRect;
        ScaleRect(R, vkz);

        SetLength(CharWidths, Length(sLine));
        ZeroMemory(@ChrInfo, SizeOf(ChrInfo));
        ChrInfo.lStructSize := SizeOf(ChrInfo);
        ChrInfo.lpDx := @(CharWidths[0]);
        ChrInfo.nGlyphs := Length(sLine);
        ChrInfo.nMaxFit := Length(sLine);

        SetTextAlign(lDrawCanvas.Handle, TA_LEFT);
        i := GetCharacterPlacement(lDrawCanvas.Handle, PChar(sLine), Length(sLine), MaxInt, ChrInfo, GCP_MAXEXTENT or GCP_USEKERNING);
        if i = 0 then
          Continue;

        if not lFastDrawing then
        begin
          dw := 0;
          for i := 0 to ChrInfo.nGlyphs - 1 do
          begin
            wn := CharWidths[i] * kz / vkz + dw;
            dw := Frac(wn);
            if i = Integer(ChrInfo.nGlyphs) - 1 then
              CharWidths[i] := Round(wn)
            else
              CharWidths[i] := Trunc(wn);
          end;

          lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * kz);
          R := pTextDrawInfoRec^.TextRect;
          ScaleRect(R, kz);
        end;

        SetTextAlign(lDrawCanvas.Handle, taFlag);
        ExtTextOut(lDrawCanvas.Handle, dx, dy, ETO_CLIPPED, @R, PChar(sLine), ChrInfo.nGlyphs, @(CharWidths[0]));
      end;  

      Inc(dy, pTextDrawInfoRec^.LineHeight);
    end;
  end;

begin
  kz := pTextDrawInfoRec^.ZoomRatio;
  vkz := cVirtualCanvasRes / cScreenCanvasRes;

  lFastDrawing := Round(vkz / kz) = 1;

  pTextDrawInfoRec^.DigitNet := StringReplace(pTextDrawInfoRec^.DigitNet, #13#10, #13, [rfReplaceAll]);

  lDrawCanvas := TCanvas(pTextDrawInfoRec^.Canvas);
  if not Assigned(lDrawCanvas) then
  begin
    lDrawCanvas := TCanvas.Create;
    lDrawCanvas.Handle := GetDC(0);
  end;

  SaveIndex := SaveDC(lDrawCanvas.Handle);

  SetMapMode(lDrawCanvas.Handle, MM_TEXT);
  pTextDrawInfoRec^.Font.AssignTo(lDrawCanvas.Font);

  lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * vkz);
  GetTextExtentPoint(lDrawCanvas.Handle, 'W', 1, lSize);
  TextLineHeight := lSize.cy;

  pTextDrawInfoRec^.LineHeight := ScaleCanvasToDestRes(TextLineHeight);

  try
    S := pTextDrawInfoRec^.Text;
    S := StringReplace(S, #13#10, #13, [rfReplaceAll]);

    if pTextDrawInfoRec^.DigitNet = '' then
    begin
      R := pTextDrawInfoRec^.TextRect;
      ScaleRect(R, vkz);

      if pTextDrawInfoRec^.WordWrap then
      begin
        h1 := S;
        S := '';
        while h1 <> '' do
        begin
          h := GetNextStrValue(h1, #13);
          h2 := '';
          while h <> '' do
          begin
            GetTextExtentExPoint(lDrawCanvas.Handle, PAnsiChar(h), Length(h), R.Right - R.Left, @d, nil, Ss);
            if d < Length(h) then
            begin
              i := d;
              while (i > 0) and (h[i] <> ' ') do
                Dec(i);

              if i = 0 then
              begin
                i := d;
                while (i < Length(h)) and (h[i] <> ' ') do
                  Inc(i);
              end;
              d := i;
            end;

            if h2 <> '' then
              h2 := h2 + #13;
            h2 := h2 + Copy(h, 1, d);
            Delete(h, 1, d);
          end;

          S := S + h2;

          if h1 <> '' then
            S := S + #13;
        end;

        pTextDrawInfoRec^.Text := S;
      end;

      if pTextDrawInfoRec^.CalcOnly then
        pTextDrawInfoRec^.TextRect := CalcTextRect(S)
      else
        DrawPlainText(S);
    end

    else
    begin
      if pTextDrawInfoRec^.CalcOnly then
        pTextDrawInfoRec^.TextRect := CalcDiginetTextRect
      else
        DrawCellText;
    end;

  finally
    RestoreDC(lDrawCanvas.Handle, SaveIndex);

    if not Assigned(pTextDrawInfoRec^.Canvas) then
    begin
      DC := lDrawCanvas.Handle;
      FreeAndNil(lDrawCanvas);
      ReleaseDC(0, DC);
    end;
  end;
end;

procedure DrawTextImageRM(const pTextDrawInfoRec: PTrwTextDrawInfoRec);
var
  R: TRect;
  lDrawCanvas: TrwCanvas;
  S, h, h1, h2: String;
  d, i: Integer;
  Ss: TSize;
  kz: Extended;
  lFastDrawing: Boolean;
  TextLineHeight: Integer;
  DC: THandle;
  SaveIndex: Integer;
  lSize: TSize;
  flAddEndReturn: Boolean;

  function ScaleCanvasToDestRes(const AValue: Integer): Integer;
  begin
    Result := Round(AValue * kz);
  end;

  procedure ScaleRect(var ARect: TRect; K: Extended);
  begin
    ARect.Left := Round(ARect.Left * k);
    ARect.Top := Round(ARect.Top * k);
    ARect.Right := Round(ARect.Right * k);
    ARect.Bottom := Round(ARect.Bottom * k);
  end;

  procedure InitComRec(ACommand: String; var ARecCom: TrwDigitNetRec);
  var
    i: Integer;
  begin
    ARecCom.Command := ACommand[1];
    i := Pos(',', ACommand);
    ARecCom.Cells := StrToInt(Copy(ACommand, 2, i-2));
    ARecCom.Width := StrToInt(Copy(ACommand, i+1, Length(ACommand)-i));
  end;


  function CalcDiginetTextRect: TRect;
  var
    d: Integer;
    RCom: TrwDigitNetRec;
    dn: String;
  begin
    Result := Rect(0, 0, 0 ,0);

    d := 0;
    dn := pTextDrawInfoRec^.DigitNet;
    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);
      Inc(d, RCom.Width * RCom.Cells);
    end;
    Result.Right := Result.Left + d;
    Result.Bottom := Result.Top + TextLineHeight;
    ScaleRect(Result, kz);
  end;

  function CalcTextRect(AText: String): TRect;
  var
    sLine: String;
    lw: Integer;
  begin
    Result := Rect(0, 0, 0 ,0);

    while AText <> '' do
    begin
      sLine := GetNextStrValue(AText, #13);
      lw := lDrawCanvas.TextWidth(sLine);
      if lw > Result.Right then
        Result.Right := lw;
      Inc(Result.Bottom, TextLineHeight);
    end;

    if flAddEndReturn then
      Inc(Result.Bottom, TextLineHeight);

    ScaleRect(Result, kz);
  end;

  procedure DrawCellText;
  var
    RCom: TrwDigitNetRec;
    R1, R: TRect;
    h, s, dn: String;
    Len, BegCell, EndCell, Ind, x, j, d: Integer;
  begin
    lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * kz);
    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    IntersectClipRect(lDrawCanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    S := pTextDrawInfoRec^.Text;

    d := 0;
    dn := pTextDrawInfoRec^.DigitNet;
    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);
      if RCom.Command = NET_CELL then
        Inc(d, RCom.Cells);
    end;
    Ind := 1;
    BegCell := 1;
    EndCell := d;

    Len := Length(S);
    case pTextDrawInfoRec^.Alignment of
      taLeftJustify:
        begin
          Ind := 1;
          BegCell := 1;
          if Len > d then
            EndCell := d
          else
            EndCell := Len;
        end;

      taCenter:
          if Len > d then
          begin
            BegCell := 1;
            EndCell := d;
            Ind := (Len - d) div 2 + 1;
          end
          else
          begin
            Ind := 1;
            BegCell := (d - Len) div 2 + 1;
            EndCell := BegCell + Len - 1;
          end;

      taRightJustify:
        begin
          if Len > d then
          begin
            BegCell := 1;
            Ind := Len - d + 1;
          end
          else
          begin
            BegCell := d - Len + 1;
            Ind := 1;
          end;
          EndCell := d;
        end;
    end;

    if pTextDrawInfoRec^.Color <> clNone then
    begin
      lDrawCanvas.Brush.Color := pTextDrawInfoRec^.Color;
      lDrawCanvas.Brush.Style := bsSolid;
      lDrawCanvas.FillRect(R);
    end;
    SetBkMode(lDrawCanvas.Handle, TRANSPARENT);

    x := R.Left;
    d := 1;
    dn := pTextDrawInfoRec^.DigitNet;

    while dn <> '' do
    begin
      InitComRec(GetNextStrValue(dn, #13), RCom);

      case RCom.Command of
        NET_CELL:
            begin
              for j := 1 to RCom.Cells do
              begin
                if (Ind > Len) or (d > EndCell) or (x >= R.Right) then
                  Exit;

                if d >= BegCell then
                begin
                  R1 := Rect(x, R.Top, x + ScaleCanvasToDestRes(RCom.Width), R.Bottom);
                  h := S[Ind];
                  DrawText(lDrawCanvas.Handle, PChar(h), 1, R1, DT_CENTER or DT_NOCLIP or DT_NOPREFIX);
                  Inc(Ind);
                end;
                Inc(x, ScaleCanvasToDestRes(RCom.Width));
                Inc(d);
              end;
            end;

        NET_SPACE:
          Inc(x, ScaleCanvasToDestRes(RCom.Width) * RCom.Cells);
      end;
    end;
  end;


  procedure DrawPlainText(AText: String);
  var
    i, dx, dy, h: Integer;
    wn, dw: Extended;
    R: TRect;
    ChrInfo: TGCPResults;
    CharWidths: array of Integer;
    sLine: String;
    taFlag: DWord;
  begin
    lDrawCanvas.Font.Size := Round(pTextDrawInfoRec^.Font.Size * kz);

    R := pTextDrawInfoRec^.TextRect;
    ScaleRect(R, kz);

    taFlag := 0;
    case pTextDrawInfoRec^.Alignment of
      taRightJustify: begin
                        taFlag := taFlag or TA_RIGHT;
                        dx := R.Right;
                      end;

      taCenter:       begin
                        taFlag := taFlag or TA_CENTER;
                        dx := R.Left + (R.Right - R.Left ) div 2;
                      end;
    else
      taFlag := taFlag or TA_LEFT;
      dx := R.Left;
    end;

    h := 1;
    for i := 1 to Length(AText) do
      if AText[i] = #13 then
        Inc(h);

    case pTextDrawInfoRec^.Layout of
      tlBottom:  dy := R.Top + (R.Bottom - R.Top) - ScaleCanvasToDestRes(h * TextLineHeight);
      tlCenter:  dy := R.Top + ((R.Bottom - R.Top) - ScaleCanvasToDestRes(h * TextLineHeight)) div 2;
    else
      dy := R.Top;
    end;

    if pTextDrawInfoRec^.Color <> clNone then
    begin
      lDrawCanvas.Brush.Color := pTextDrawInfoRec^.Color;
      lDrawCanvas.Brush.Style := bsSolid;
      lDrawCanvas.FillRect(R);
    end;

    SetBkMode(lDrawCanvas.Handle, TRANSPARENT);

    while AText <> '' do
    begin
      sLine := GetNextStrValue(AText, #13);
      if sLine <> '' then
      begin
        SetLength(CharWidths, Length(sLine));
        ZeroMemory(@ChrInfo, SizeOf(ChrInfo));
        ChrInfo.lStructSize := SizeOf(ChrInfo);
        ChrInfo.lpDx := @(CharWidths[0]);
        ChrInfo.nGlyphs := Length(sLine);
        ChrInfo.nMaxFit := Length(sLine);

        lDrawCanvas.Font.Size := pTextDrawInfoRec^.Font.Size;
        SetTextAlign(lDrawCanvas.Handle, TA_LEFT);
        i := GetCharacterPlacement(lDrawCanvas.Handle, PChar(sLine), Length(sLine), MaxInt, ChrInfo, GCP_MAXEXTENT or GCP_USEKERNING);
        if i = 0 then
          Continue;

        R := pTextDrawInfoRec^.TextRect;
        if not lFastDrawing then
        begin
          dw := 0;
          for i := 0 to ChrInfo.nGlyphs - 1 do
          begin
            wn := CharWidths[i] * kz + dw;
            dw := Frac(wn);
            if i = Integer(ChrInfo.nGlyphs) - 1 then
              CharWidths[i] := Round(wn)
            else
              CharWidths[i] := Trunc(wn);
          end;

          lDrawCanvas.Font.Size := ScaleCanvasToDestRes(pTextDrawInfoRec^.Font.Size);
          ScaleRect(R, kz);
        end;

        SetTextAlign(lDrawCanvas.Handle, taFlag);
        ExtTextOut(lDrawCanvas.Handle, dx, dy, ETO_CLIPPED, @R, PChar(sLine), ChrInfo.nGlyphs, @(CharWidths[0]));
      end;

      Inc(dy, pTextDrawInfoRec^.LineHeight);
    end;
  end;

begin
  kz := pTextDrawInfoRec^.ZoomRatio;
  lFastDrawing := kz = 1;

  pTextDrawInfoRec^.DigitNet := StringReplace(pTextDrawInfoRec^.DigitNet, #13#10, #13, [rfReplaceAll]);

  lDrawCanvas := TrwCanvas(pTextDrawInfoRec^.Canvas);
  if not Assigned(lDrawCanvas) then
  begin
    lDrawCanvas := TrwCanvas.Create;
    lDrawCanvas.Handle := GetDC(0);
  end;

  SaveIndex := SaveDC(lDrawCanvas.Handle);

  SetMapMode(lDrawCanvas.Handle, MM_TEXT);
  lDrawCanvas.Font := pTextDrawInfoRec^.Font;

  GetTextExtentPoint(lDrawCanvas.Handle, 'W', 1, lSize);
  TextLineHeight := lSize.cy;
  pTextDrawInfoRec^.LineHeight := ScaleCanvasToDestRes(TextLineHeight);

  try
    S := pTextDrawInfoRec^.Text;
    S := StringReplace(S, #13#10, #13, [rfReplaceAll]);

    flAddEndReturn := (Length(S) > 0) and (S[Length(S)] = #13);

    if pTextDrawInfoRec^.DigitNet = '' then
    begin
      R := pTextDrawInfoRec^.TextRect;

      if pTextDrawInfoRec^.WordWrap then
      begin
        h1 := S;
        S := '';
        while h1 <> '' do
        begin
          h := GetNextStrValue(h1, #13);
          h2 := '';
          while h <> '' do
          begin
            GetTextExtentExPoint(lDrawCanvas.Handle, PAnsiChar(h), Length(h), R.Right - R.Left, @d, nil, Ss);
            if d = 0 then
              d := 1;
            if d < Length(h) then
            begin
              i := d;
              while (i > 0) and (h[i] <> ' ')do
                Dec(i);

              if i = 0 then
              begin
                i := d;
                while (i < Length(h)) and (h[i] <> ' ') do
                  Inc(i);
              end;
              d := i;
            end;

            if h2 <> '' then
              h2 := h2 + #13;
            h2 := h2 + Copy(h, 1, d);
            Delete(h, 1, d);
          end;

          S := S + h2;

          if h1 <> '' then
            S := S + #13;
        end;

        if flAddEndReturn then
          S := S + #13;

        pTextDrawInfoRec^.Text := S;
      end;

      if pTextDrawInfoRec^.CalcOnly then
        pTextDrawInfoRec^.TextRect := CalcTextRect(S)
      else
        DrawPlainText(S);
    end

    else
    begin
      if pTextDrawInfoRec^.CalcOnly then
        pTextDrawInfoRec^.TextRect := CalcDiginetTextRect
      else
        DrawCellText;
    end;

  finally
    RestoreDC(lDrawCanvas.Handle, SaveIndex);

    if not Assigned(pTextDrawInfoRec^.Canvas) then
    begin
      DC := lDrawCanvas.Handle;
      FreeAndNil(lDrawCanvas);
      ReleaseDC(0, DC);
    end;
  end;
end;


function TextInRect(const ARect: TRect; const AText: String; const ATextHeight: Integer; const Clipping: Boolean): TStringBoundsRec;
var
  LinesToSkip: Integer;
begin
  if not Clipping and (ATextHeight > (ARect.Bottom - ARect.Top)) then
  begin
    Result.BeginPos := 0;
    Result.EndPos := 0;
    Exit;
  end;

  Result.BeginPos := 1;
  LinesToSkip := ARect.Top div ATextHeight;
  while (Result.BeginPos <= Length(AText)) and (LinesToSkip > 0) do
  begin
    if AText[Result.BeginPos] = #13 then
      Dec(LinesToSkip);
    Inc(Result.BeginPos);
  end;

  Result.EndPos := Result.BeginPos;

  LinesToSkip := (ARect.Bottom - ARect.Top) div ATextHeight;
  if Clipping and ((ARect.Bottom - ARect.Top) mod ATextHeight > 0) then
    Inc(LinesToSkip);

  while Result.EndPos <= Length(AText) do
  begin
    if AText[Result.EndPos] = #13 then
      Dec(LinesToSkip);
    if LinesToSkip = 0 then
      break;
    Inc(Result.EndPos);
  end;
end;

function TextLineCount(const AText: String): Integer;
var
  i: Integer;
begin
  if Length(AText) > 0 then
    Result := 1
  else
    Result := 0;
      
  for i := 1 to Length(AText) do
    if AText[i] = #13 then
      Inc(Result);
end;

procedure DrawMetafile(AMetafile: TMetaFile; AHDC: THandle; KfZoom: Extended; X, Y: Integer);

  procedure DrawMetafile98;
  var
    R: TRect;
    S1, S2: TSize;
    MM: Integer;
  begin
    R := Rect(0, 0, AMetafile.Width, AMetafile.Height);
    MM := SetMapMode(AHDC, MM_ISOTROPIC);
    SetWindowExtEx(AHDC, R.Right, R.Bottom, @S1);
    SetViewportExtEx(AHDC, Round(AMetafile.Width * KfZoom), Round(AMetafile.Height * KfZoom), @S2);
    OffsetViewportOrgEx(AHDC, X, Y, nil);
    try
      PlayEnhMetaFile(AHDC, AMetafile.Handle, R);
    finally
      SetMapMode(AHDC, MM);
      SetWindowExtEx(AHDC, S1.cx, S1.cy, nil);
      SetViewportExtEx(AHDC, S2.cx, S2.cy, nil);
      OffsetViewportOrgEx(AHDC, -X, -Y, nil);
    end;
  end;

  procedure DrawMetafileNT;
  var
    R: TRect;
    xForm: TXForm;
    gm: Integer;
  begin
    R := Rect(0, 0, AMetafile.Width, AMetafile.Height);
    gm := SetGraphicsMode(AHDC, GM_ADVANCED);
    xForm.eM11 := KfZoom;
    xForm.eM21 := 0;
    xForm.eDx := 0;
    xForm.eM12 := 0;
    xForm.eM22 := xForm.eM11;
    xForm.eDy := 0;
    SetWorldTransform(AHDC, xForm);
    SetMapMode(AHDC, MM_TEXT);
    OffsetViewportOrgEx(AHDC, X, Y, nil);
    try
      PlayEnhMetaFile(AHDC, AMetafile.Handle, R);
    finally
      xForm.eM11 := 1;
      xForm.eM21 := 0;
      xForm.eDx := 0;
      xForm.eM12 := 0;
      xForm.eM22 := 1;
      xForm.eDy := 0;
      SetWorldTransform(AHDC, xForm);
      SetGraphicsMode(AHDC, gm);
      OffsetViewportOrgEx(AHDC, -X, -Y, nil);
    end;
  end;

begin
  if Win32Platform = VER_PLATFORM_WIN32_WINDOWS	 then
    DrawMetafile98
  else
    DrawMetafileNT;
end;

procedure DrawFrame(Canvas: TCanvas; BoundLines: TrwSetColumnLines;
  BoundLinesColor: TColor; BoundLinesWidth: Integer; BoundLinesStyle: TPenStyle;
  Width: Integer; Height: Integer; Left: Integer = 0; Top: Integer = 0);
var
  X, Y, W, H: Integer;
begin
  with Canvas do
  begin
    Pen.Mode := pmCopy;
    Pen.Color := BoundLinesColor;
    Pen.Style := BoundLinesStyle;

    if BoundLinesStyle = psSolid then
    begin
      Pen.Width := BoundLinesWidth;
//      BoundLinesWidth := 1;
    end
    else if BoundLinesWidth = 0 then
      Pen.Width := 0
    else
      Pen.Width := 1;

    X := Left + Pen.Width div 2;
    Y := Top + Pen.Width div 2;
    W := Width - Pen.Width;
    H := Height - Pen.Width;

    if Pen.Width = 0 then
    begin
      Dec(W);
      Dec(H);
    end;

 {                                           Works too slow  :(
    for i := 0 to BoundLinesWidth - 1 do
    begin
      if (rclLeft in BoundLines) then
      begin
        MoveTo(X + i, Y);
        LineTo(X + i, Y + H);
      end;

      if (rclRight in BoundLines) then
      begin
        MoveTo(X + W - i, Y);
        LineTo(X + W - i, Y + H);
      end;

      if (rclTop in BoundLines) then
      begin
        MoveTo(X, Y + i);
        LineTo(X + W, Y + i);
      end;

      if (rclBottom in BoundLines) then
      begin
        MoveTo(X, Y + H - i);
        LineTo(X + W, Y + H - i);
      end;
    end;}

    if (rclLeft in BoundLines) then
    begin
      MoveTo(X, Y);
      LineTo(X, Y + H);
    end;

    if (rclRight in BoundLines) then
    begin
      MoveTo(X + W, Y);
      LineTo(X + W, Y + H);
    end;

    if (rclTop in BoundLines) then
    begin
      MoveTo(X, Y);
      LineTo(X + W, Y);
    end;

    if (rclBottom in BoundLines) then
    begin
      MoveTo(X, Y + H);
      LineTo(X + W, Y + H);
    end;
  end;
end;

procedure DrawCorners(Canvas: TCanvas; Width: Integer; Height: Integer);
var
  l: Integer;
begin
  with Canvas do
  begin
    Pen.Mode := pmCopy;
    Pen.Color := clBlue;
    Pen.Style := psSolid;

    l := Min(5, Min(Height div 3, Width div 3));

    MoveTo(0, 0);
    LineTo(l, 0);
    MoveTo(0, 0);
    LineTo(0, l);

    MoveTo(Width - 1, 0);
    LineTo(Width - l, 0);
    MoveTo(Width - 1, 0);
    LineTo(Width - 1, l);

    MoveTo(0, Height - 1);
    LineTo(l, Height - 1);
    MoveTo(0, Height - 1);
    LineTo(0, Height - l);

    MoveTo(Width - 1, Height - 1);
    LineTo(Width - l, Height - 1);
    MoveTo(Width - 1, Height - 1);
    LineTo(Width - 1, Height - l);
  end;
end;


procedure DrawCornersRM(Canvas: TrwCanvas; Width: Integer; Height: Integer);
var
  l: Integer;
begin
  with Canvas do
  begin
    Pen.Mode := pmCopy;
    Pen.Color := clBlue;
    Pen.Style := psSolid;

    l := Min(5, Min(Height div 3, Width div 3));

    MoveTo(0, 0);
    LineTo(l, 0);
    MoveTo(0, 0);
    LineTo(0, l);

    MoveTo(Width - 1, 0);
    LineTo(Width - l, 0);
    MoveTo(Width - 1, 0);
    LineTo(Width - 1, l);

    MoveTo(0, Height - 1);
    LineTo(l, Height - 1);
    MoveTo(0, Height - 1);
    LineTo(0, Height - l);

    MoveTo(Width - 1, Height - 1);
    LineTo(Width - l, Height - 1);
    MoveTo(Width - 1, Height - 1);
    LineTo(Width - 1, Height - l);
  end;
end;

{ TrwFont }

procedure TrwFont.Assign(Source: TPersistent);
begin
  if Source is TrwFont then
  begin
    FCharset := TrwFont(Source).Charset;
    FColor := TrwFont(Source).Color;
    FHeight := TrwFont(Source).Height;
    FName := TrwFont(Source).Name;
    FPitch := TrwFont(Source).Pitch;
    FStyle := TrwFont(Source).Style;
    FTextAngle := TrwFont(Source).TextAngle;
  end

  else if Source is TFont then
  begin
    FCharset := TFont(Source).Charset;
    FColor := TFont(Source).Color;
    FHeight := TFont(Source).Height;
    FName := TFont(Source).Name;
    FPitch := TFont(Source).Pitch;
    FStyle := TFont(Source).Style;
  end;

  inherited;
end;

procedure TrwFont.AssignTo(Dest: TPersistent);
begin
  if Dest is TFont then
  begin
    TFont(Dest).Charset := Charset;
    TFont(Dest).Color := Color;
    TFont(Dest).PixelsPerInch := cScreenCanvasRes;    
    TFont(Dest).Height := Height;
    TFont(Dest).Name := Name;
    TFont(Dest).Pitch := Pitch;
    TFont(Dest).Style := Style;
  end

  else if Dest is TrwFont then
    TrwFont(Dest).Assign(Self)

  else
    inherited;
end;

procedure TrwFont.Changed;
begin
  inherited;
  FHeightInPixels := 0;
end;

constructor TrwFont.Create(AOwner: TPersistent);
begin
  inherited;
  Height := -11;
  Pitch := fpDefault;
  Style := [];
  Charset := DEFAULT_CHARSET;
  Name := 'MS Sans Serif';
  Color := clWindowText;
end;

function TrwFont.CreateHandle: THandle;
var
  LogFont: TLogFont;
begin
  with LogFont do
  begin
    lfHeight := Height;
    lfWidth := 0; { have font mapper choose }
    lfEscapement := 0; { only straight fonts }
    lfOrientation := 0; { no rotation }
    if fsBold in Style then
      lfWeight := FW_BOLD
    else
      lfWeight := FW_NORMAL;
    lfItalic := Byte(fsItalic in Style);
    lfUnderline := Byte(fsUnderline in Style);
    lfStrikeOut := Byte(fsStrikeOut in Style);
    lfCharSet := Byte(Charset);
    StrPCopy(lfFaceName, Name);
    lfQuality := DEFAULT_QUALITY;
    lfOutPrecision := OUT_DEFAULT_PRECIS;
    lfClipPrecision := CLIP_DEFAULT_PRECIS;
    case Pitch of
      fpVariable: lfPitchAndFamily := VARIABLE_PITCH;
      fpFixed: lfPitchAndFamily := FIXED_PITCH;
    else
      lfPitchAndFamily := DEFAULT_PITCH;
    end;

    //text angle?

    Result := CreateFontIndirect(LogFont);
  end;
end;

function TrwFont.GetSize: Integer;
begin
  Result := -MulDiv(Height, 72, cScreenCanvasRes);
end;

function TrwFont.HeightInPixels: Integer;
var
  Tinf: TrwTextDrawInfoRec;
begin
  if FHeightInPixels = 0 then
  begin
    Tinf.CalcOnly := True;
    Tinf.ZoomRatio := 1;
    Tinf.Text := 'W';
    Tinf.Font := Self;
    Tinf.DigitNet := '';
    Tinf.WordWrap := False;
    Tinf.TextRect := Rect(0, 0, 0, 0);
    DrawTextImageRM(@Tinf);
    FHeightInPixels := Tinf.LineHeight;
  end;
  Result := FHeightInPixels;
end;

procedure TrwFont.SetCharset(const Value: TFontCharset);
begin
  if FCharset <> Value then
  begin
    FCharset := Value;
    Changed;
  end;
end;

procedure TrwFont.SetColor(const Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
    Changed;
  end;
end;

procedure TrwFont.SetHeight(const Value: Integer);
begin
  if FHeight <> Value then
  begin
    FHeight := Value;
    Changed;
  end;
end;

procedure TrwFont.SetName(const Value: String);
begin
  if FName <> Value then
  begin
    FName := Value;
    Changed;
  end;
end;

procedure TrwFont.SetPitch(const Value: TFontPitch);
begin
  if FPitch <> Value then
  begin
    FPitch := Value;
    Changed;
  end;
end;

procedure TrwFont.SetSize(const Value: Integer);
begin
  Height := -MulDiv(Value, cScreenCanvasRes, 72);
end;

procedure TrwFont.SetStyle(const Value: TFontStyles);
begin
  if FStyle <> Value then
  begin
    FStyle := Value;
    Changed;
  end;
end;

procedure TrwFont.SetTextAngle(const Value: Integer);
begin
  if FTextAngle <> Value then
  begin
    FTextAngle := Value;
    Changed;
  end;
end;

function TrwFont.TheSame(AObject: TrwGraphicObject): Boolean;
begin
  Result := (TrwFont(AObject).Charset = Charset) and
            (TrwFont(AObject).Color = Color) and
            (TrwFont(AObject).Height = Height) and
            (TrwFont(AObject).Name = Name) and
            (TrwFont(AObject).Pitch = Pitch) and
            (TrwFont(AObject).Style = Style) and
            (TrwFont(AObject).TextAngle = TextAngle);
end;

{ TrwPen }


constructor TrwPen.Create(AOwner: TPersistent);
begin
  inherited;
  FColor := clBlack;
  FMode := pmCopy;
  FStyle := psSolid;
  FWidth := 1;
end;

procedure TrwPen.Assign(Source: TPersistent);
begin
  if Source is TrwPen then
  begin
    FColor := TrwPen(Source).Color;
    FMode := TrwPen(Source).Mode;
    FStyle := TrwPen(Source).Style;
    FWidth := TrwPen(Source).Width;
  end;

  inherited;
end;


procedure TrwPen.SetColor(Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
    Changed;
  end;
end;

procedure TrwPen.SetMode(Value: TPenMode);
begin
  if FMode <> Value then
  begin
    FMode := Value;
    Changed;
  end;
end;

procedure TrwPen.SetStyle(Value: TPenStyle);
begin
  if FStyle <> Value then
  begin
    FStyle := Value;
    Changed;
  end;
end;

procedure TrwPen.SetWidth(Value: Integer);
begin
  if FWidth <> Value then
  begin
    FWidth := Value;
    Changed;
  end;
end;

function TrwPen.CreateHandle: THandle;
const
  PenStyles: array[TPenStyle] of Word = (PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT, PS_NULL, PS_INSIDEFRAME);
var
  LogPen: TLogPen;
begin
  with LogPen do
  begin
    lopnStyle := PenStyles[Style];
    lopnWidth.X := Width;
    lopnColor := ColorToRGB(Color);
  end;

  Result := CreatePenIndirect(LogPen);  
end;

{ TrwBrush }

constructor TrwBrush.Create(AOwner: TPersistent);
begin
  inherited;
  FColor := clWhite;
  FStyle := bsSolid;
end;

procedure TrwBrush.Assign(Source: TPersistent);
begin
  if Source is TrwBrush then
  begin
    FColor := TrwBrush(Source).Color;
    FStyle := TrwBrush(Source).Style;

    if TrwBrush(Source).BitmapNotEmpty then
      Bitmap := TrwBrush(Source).Bitmap
    else
      SetBitmap(nil);
  end;

  inherited;
end;

procedure TrwBrush.SetColor(Value: TColor);
begin
  if FColor <> Value then
  begin
    FColor := Value;
    Changed;
  end;
end;

procedure TrwBrush.SetStyle(const Value: TBrushStyle);
begin
  if FStyle <> Value then
  begin
    FStyle := Value;
    Changed;
  end;
end;

function TrwBrush.CreateHandle: THandle;
var
  LogBrush: TLogBrush;
begin
  with LogBrush do
  begin
    if BitmapNotEmpty then
    begin
      lbStyle := BS_PATTERN;
      Bitmap.HandleType := bmDDB;
      lbHatch := LongInt(Bitmap.Handle);
    end

    else
    begin
      lbHatch := 0;
      case Style of
        bsSolid: lbStyle := BS_SOLID;
        bsClear: lbStyle := BS_HOLLOW;
      else
        lbStyle := BS_HATCHED;
        lbHatch := Ord(Style) - Ord(bsHorizontal);
      end;
    end;
    lbColor := ColorToRGB(Color);
  end;

  Result := CreateBrushIndirect(LogBrush);
end;


destructor TrwBrush.Destroy;
begin
  FreeAndNil(FBitmap);
  inherited;
end;

function TrwBrush.GetBitmap: TBitmap;
begin
  if not Assigned(FBitmap) then
    FBitmap := TBitmap.Create;
  Result := FBitmap;
end;

procedure TrwBrush.SetBitmap(const Value: TBitmap);
begin
  if (Value = nil) or TBitmap(Value).Empty then
  begin
    if Assigned(FBitmap) then
    begin
      FreeAndNil(FBitmap);
      Changed;
    end;
  end
  else
  begin
    Bitmap.Assign(Value);
    Changed;
  end;  
end;

function TrwBrush.BitmapNotEmpty: Boolean;
begin
  Result := Assigned(FBitmap) and not FBitmap.Empty;
end;

{ TrwGraphicObject }

procedure TrwGraphicObject.Changed;
begin
  DestroyHandle;
  inherited;
end;

destructor TrwGraphicObject.Destroy;
begin
  DestroyHandle;
  inherited;
end;

procedure TrwGraphicObject.DestroyHandle;
begin
  if FHandle <> 0 then
  begin
    DeleteObject(FHandle);
    FHandle := 0;
  end;
end;

function TrwGraphicObject.GetHandle: THandle;
begin
  if (FHandle = 0) and (Owner is TrwCanvas) then
    FHandle := CreateHandle;
  Result := FHandle;  
end;

function TrwGraphicObject.TheSame(AObject: TrwGraphicObject): Boolean;
begin
  Result := False;
end;

{ TrwCanvas }

constructor TrwCanvas.Create;
begin
  FFont := TrwFont.Create(Self);
  FPen := TrwPen.Create(Self);
  FBrush := TrwBrush.Create(Self);
  FState := [];
end;

procedure TrwCanvas.CreateBrush;
begin
  SelectObject(FHandle, Brush.Handle);
  if Brush.Style = bsSolid then
  begin
    SetBkColor(FHandle, ColorToRGB(Brush.Color));
    SetBkMode(FHandle, OPAQUE);
  end
  else
  begin
    { Win95 doesn't draw brush hatches if bkcolor = brush color }
    { Since bkmode is transparent, nothing should use bkcolor anyway }
    SetBkColor(FHandle, not Cardinal(ColorToRGB(Brush.Color)));
    SetBkMode(FHandle, TRANSPARENT);
  end;
end;

procedure TrwCanvas.CreateFont;
begin
  SelectObject(FHandle, Font.Handle);
  SetTextColor(FHandle, ColorToRGB(Font.Color));
end;

procedure TrwCanvas.CreateHandle;
begin
end;

procedure TrwCanvas.CreatePen;
const
  PenModes: array[TPenMode] of Word =
    (R2_BLACK, R2_WHITE, R2_NOP, R2_NOT, R2_COPYPEN, R2_NOTCOPYPEN, R2_MERGEPENNOT,
     R2_MASKPENNOT, R2_MERGENOTPEN, R2_MASKNOTPEN, R2_MERGEPEN, R2_NOTMERGEPEN,
     R2_MASKPEN, R2_NOTMASKPEN, R2_XORPEN, R2_NOTXORPEN);
begin
  SelectObject(FHandle, Pen.Handle);
  SetROP2(FHandle, PenModes[Pen.Mode]);
end;

destructor TrwCanvas.Destroy;
begin
  FreeAndNil(FFont);
  FreeAndNil(FPen);
  FreeAndNil(FBrush);
  inherited;
end;

procedure TrwCanvas.Draw(X, Y: Integer; Graphic: TGraphic);
var
  C: TCanvas;
begin
  C := TCanvas.Create;
  try
    C.Handle := Handle;
    C.Draw(X, Y, Graphic);
  finally
    FreeAndNil(C);
  end;
end;

procedure TrwCanvas.DrawFocusRect(const Rect: TRect);
begin
  RequiredState([csHandleValid, csBrushValid]);
  Windows.DrawFocusRect(FHandle, Rect);
end;

procedure TrwCanvas.Ellipse(X1, Y1, X2, Y2: Integer);
begin
  RequiredState([csHandleValid, csPenValid, csBrushValid]);
  Windows.Ellipse(FHandle, X1, Y1, X2, Y2);
end;

procedure TrwCanvas.DrawText(const Text: String; ARect: TRect; ATextFlags: Integer);
begin
  Windows.DrawText(Handle, PChar(Text), Length(Text), ARect, ATextFlags);
end;

procedure TrwCanvas.Ellipse(const Rect: TRect);
begin
  Ellipse(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);
end;

procedure TrwCanvas.FillRect(const Rect: TRect);
begin
  RequiredState([csHandleValid, csBrushValid]);
  Windows.FillRect(FHandle, Rect, Brush.Handle);
end;

procedure TrwCanvas.FrameRect(const Rect: TRect);
begin
  RequiredState([csHandleValid, csBrushValid]);
  Windows.FrameRect(FHandle, Rect, Brush.Handle);
end;

function TrwCanvas.GetClipRect: TRect;
begin
  RequiredState([csHandleValid]);
  GetClipBox(FHandle, Result);
end;

function TrwCanvas.GetHandle: HDC;
begin
  RequiredState(csAllValid);
  Result := FHandle;
end;

function TrwCanvas.GetPenPos: TPoint;
begin
  RequiredState([csHandleValid]);
  Windows.GetCurrentPositionEx(FHandle, @Result);
end;

function TrwCanvas.GetPixel(X, Y: Integer): TColor;
begin
  RequiredState([csHandleValid]);
  GetPixel := Windows.GetPixel(FHandle, X, Y);
end;

function TrwCanvas.HandleAllocated: Boolean;
begin
  Result := FHandle <> 0;
end;

procedure TrwCanvas.LineTo(X, Y: Integer);
begin
  RequiredState([csHandleValid, csPenValid, csBrushValid]);
  Windows.LineTo(FHandle, X, Y);
end;

procedure TrwCanvas.MoveTo(X, Y: Integer);
begin
  Windows.MoveToEx(FHandle, X, Y, nil);
end;

procedure TrwCanvas.Rectangle(X1, Y1, X2, Y2: Integer);
begin
  RequiredState([csHandleValid, csBrushValid, csPenValid]);
  Windows.Rectangle(FHandle, X1, Y1, X2, Y2);
end;

procedure TrwCanvas.Rectangle(const Rect: TRect);
begin
  Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);
end;

procedure TrwCanvas.Refresh;
begin

end;

procedure TrwCanvas.RequiredState(ReqState: TCanvasState);
var
  NeededState: TCanvasState;
begin
  NeededState := ReqState - FState;
//  if NeededState <> [] then
  begin
    if csHandleValid in NeededState then
    begin
      CreateHandle;
      if FHandle = 0 then
        raise EInvalidOperation.Create('Canvas does not allow drawing');
    end;

    if csFontValid in ReqState then
      CreateFont;
    if csPenValid in ReqState then
      CreatePen;
    if csBrushValid in ReqState then
      CreateBrush;
    FState := FState + NeededState;
  end;
end;

procedure TrwCanvas.RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
begin
  RequiredState([csHandleValid, csBrushValid, csPenValid]);
  Windows.RoundRect(FHandle, X1, Y1, X2, Y2, X3, Y3);
end;

procedure TrwCanvas.SetBrush(const Value: TrwBrush);
begin
  FBrush.Assign(Value);
end;

procedure TrwCanvas.SetFont(const Value: TrwFont);
begin
  FFont.Assign(Value);
end;

procedure TrwCanvas.SetHandle(const Value: HDC);
begin
  FHandle := Value;
end;

procedure TrwCanvas.SetPen(const Value: TrwPen);
begin
  FPen.Assign(Value);
end;

procedure TrwCanvas.SetPenPos(const Value: TPoint);
begin
  MoveTo(Value.X, Value.Y);
end;

procedure TrwCanvas.SetPixel(X, Y: Integer; const Value: TColor);
begin
  RequiredState([csHandleValid, csPenValid]);
  Windows.SetPixel(FHandle, X, Y, ColorToRGB(Value));
end;

procedure TrwCanvas.StretchDraw(const Rect: TRect; Graphic: TGraphic);
var
  C: TCanvas;
begin
  C := TCanvas.Create;
  try
    C.Handle := Handle;
    C.StretchDraw(Rect, Graphic);
  finally
    FreeAndNil(C);
  end;
end;


function TrwCanvas.TextExtent(const Text: string): TSize;
var
  flHandleCreated: Boolean;
begin
  if FHandle = 0 then
  begin
    FHandle := GetDC(0);
    flHandleCreated := True;
  end
  else
    flHandleCreated := False;

  RequiredState([csHandleValid, csFontValid]);
  Result.cX := 0;
  Result.cY := 0;
  Windows.GetTextExtentPoint32(FHandle, PChar(Text), Length(Text), Result);

  if flHandleCreated then
  begin
    ReleaseDC(0, FHandle);
    FHandle := 0;
  end;
end;

function TrwCanvas.TextHeight(const Text: string): Integer;
begin
  Result := TextExtent(Text).cY;
end;

procedure TrwCanvas.TextOut(X, Y: Integer; const Text: string; ATextFlags: Integer);
begin
  RequiredState([csHandleValid, csFontValid, csBrushValid]);
  Windows.ExtTextOut(FHandle, X, Y, ATextFlags, nil, PChar(Text),
   Length(Text), nil);
  MoveTo(X + TextWidth(Text), Y);
end;

procedure TrwCanvas.TextRect(Rect: TRect; X, Y: Integer; const Text: string);
var
  Options: Longint;
begin
  RequiredState([csHandleValid, csFontValid, csBrushValid]);
  Options := ETO_CLIPPED;
  if Brush.Style <> bsClear then
    Options := Options or ETO_OPAQUE;
  Windows.ExtTextOut(FHandle, X, Y, Options, @Rect, PChar(Text),
    Length(Text), nil);
end;

function TrwCanvas.TextWidth(const Text: string): Integer;
begin
  Result := TextExtent(Text).cX;
end;


{ TrwPictureHolder }

constructor TrwPictureHolder.Create(AOwner: TPersistent);
begin
  inherited;
  FData := TEvDualStreamHolder.Create;
end;

destructor TrwPictureHolder.Destroy;
begin
  DestroyPictureHandler;
  inherited;
end;

procedure TrwPictureHolder.Assign(Source: TPersistent);
begin
  FData.Size := 0;

  if Source is TrwPictureHolder then
  begin
    TrwPictureHolder(Source).FData.Position := 0;
    FData.RealStream.CopyFrom(TrwPictureHolder(Source).FData.RealStream, TrwPictureHolder(Source).FData.Size);
  end

  else if Source is TPicture then
    PictureToStream(TPicture(Source), FData);

  SyncPictureContent;

  inherited;
end;

procedure TrwPictureHolder.PictureToStream(APicture: TPicture; AStream: IEvDualStream);
var
  Writer: TrwWriter;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(-1, True);
  Writer := TrwWriter.Create(MS.RealStream, 1024);
  try
    Writer.WriteProperties(APicture);
  finally
    FreeAndNil(Writer);
  end;

  if MS.Size > 0 then
  begin
    MS.Position := 10;   // skip property name
    AStream.RealStream.CopyFrom(MS.RealStream, MS.Size - MS.Position);
  end
  else
    AStream.Clear;
end;



function TrwPictureHolder.CreateHandle: THandle;
begin
  Result := 0;
end;

function TrwPictureHolder.GetBitmap: TBitmap;
begin
  CreatePictureHandler;
  Result := FPictureHandler.Bitmap;
end;

function TrwPictureHolder.GetGraphic: TGraphic;
begin
  CreatePictureHandler;
  Result := FPictureHandler.Graphic;
end;

function TrwPictureHolder.GetHeight: Integer;
begin
  CreatePictureHandler;
  Result := FPictureHandler.Height;
end;

function TrwPictureHolder.GetWidth: Integer;
begin
  CreatePictureHandler;
  Result := FPictureHandler.Width;
end;


procedure TrwPictureHolder.OnChangePictureHandler(Sender: TObject);
begin
  Changed;
end;

procedure TrwPictureHolder.DefineProperties(Filer: TFiler);
begin
  Filer.DefineBinaryProperty('Data', ReadData, WriteData, True);
end;

procedure TrwPictureHolder.ReadData(Stream: TStream);
begin
  DestroyPictureHandler;
  FData.Size := 0;
  FData.RealStream.CopyFrom(Stream, 0);
end;

procedure TrwPictureHolder.WriteData(Stream: TStream);
begin
  Stream.CopyFrom(FData.RealStream, 0);
end;

procedure TrwPictureHolder.CreatePictureHandler;
begin
  if not Assigned(FPictureHandler) then
  begin
    FPictureHandler := TPicture.Create;
    SyncPictureContent;
    FPictureHandler.OnChange := OnChangePictureHandler;
  end;
end;

procedure TrwPictureHolder.SyncPictureContent;
var
  PropName: String[4];
  MS: IEvDualStream;
  Reader: TrwReader;
begin
  if not Assigned(FPictureHandler) then
    Exit;

  if Assigned(FPictureHandler.Graphic) then
    FPictureHandler.Graphic.Assign(nil);

  if FData.Size > 0 then
  begin
    MS := TEvDualStreamHolder.Create(FData.Size + 10, True);
    PropName := 'Data';
    MS.WriteBuffer(PropName[0], 5);
    MS.WriteByte(Ord(vaBinary));
    MS.WriteInteger(FData.Size);
    FData.Position := 0;
    MS.RealStream.CopyFrom(FData.RealStream, 0);

    MS.Position := 0;
    Reader := TrwReader.Create(MS.RealStream);
    try
      Reader.ReadProperty(FPictureHandler);
    finally
      FreeAndNil(Reader);
    end;
  end;
end;

procedure TrwPictureHolder.DestroyPictureHandler;
begin
  FreeAndNil(FPictureHandler);
end;

function TrwPictureHolder.GetMetafile: TMetafile;
begin
  CreatePictureHandler;
  Result := FPictureHandler.Metafile;
end;

function TrwPictureHolder.PictureIsTheSame(APicture: TrwPictureHolder): Boolean;
begin
  Result := FData.IsEqualTo(APicture.FData);
end;

function TrwPictureHolder.PictureIsTheSame(APicture: TPicture): Boolean;
var
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(-1, True);
  PictureToStream(APicture, MS);
  Result := MS.IsEqualTo(FData);
end;

end.
