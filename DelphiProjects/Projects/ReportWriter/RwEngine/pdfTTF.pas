{**************************************************}
{                                                  }
{                   llPDFLib                       }
{            Version  2.3,  10.04.2003             }
{      Copyright (c) 2002, 2003 llionsoft          }
{             All rights reserved                  }
{            mailto:info@llion.net                 }
{                                                  }
{**************************************************}
{$i pdf.inc}
unit PDFTTF;

interface

uses
  Windows, SysUtils, Classes,{$IFDEF VARIANTS}Variants, {$ENDIF}PDF, Graphics, PDFResources, EvStreamUtils;

procedure GetFontSubset(TP: TPDFFontEmbeddingType; Font: TPDFFont; OutStream: TStream);
implementation

type
  ETrueTypeFile = class(Exception);

{ TTF data types }

  USHORT = Word;
  SHORT = Smallint;
  ULONG = Longint;
  LONG = Longint;
  FIXED = packed record
    Hi, Lo: USHORT;
  end;

{ Table directory }

  FileHeaderRec = packed record
    Version: FIXED;
    NumTables: USHORT;
    SearchRange: USHORT;
    EntrySelector: USHORT;
    RangeShift: USHORT;
  end;

  TableInfoRec = packed record
    Tag: ULONG;
    Checksum: ULONG;
    Offset: ULONG;
    Length: ULONG;
  end;

  Maxp_type = packed record
    version: fixed;
    numGlyphs: USHORT;
    maxPoints: USHORT;
    maxContours: USHORT;
    maxCompositePoints: USHORT;
    maxCompositeContours: USHORT;
    maxZones: USHORT;
    maxTwilightPoints: USHORT;
    maxStorage: USHORT;
    maxFunctionDefs: USHORT;
    maxInstructionDefs: USHORT;
    maxStackElements: USHORT;
    maxSizeOfInstructions: USHORT;
    maxComponentElements: USHORT;
    maxComponentDepth: USHORT;
  end;

  PGlyf_type = ^TGlyf_type;
  TGlyf_type = packed record
    num: integer;
    size: long;
    Glyf_: Pchar;
    next: PGlyf_type;
    tag: boolean; //�������
  end;

  THead_type = packed record
    version_number: FIXED;
    fontRevision: FIXED;
    checkSum: ULONG;
    magicNumber: ULONG;
    flags: USHORT;
    unitsPerEm: USHORT;
    two_date: array[1..16] of byte;
    xMin: SHORT; // ������������� FWORD;
    yMin: SHORT; // ������������� FWORD;
    xMax: SHORT; // ������������� FWORD;
    yMax: SHORT; // ������������� FWORD;
    macStyle: USHORT;
    lowestRec: USHORT;
    fontDirection: SHORT;
    indexToLocFormat: SHORT;
    glyphDataFormat: SHORT
  end;

  CmapHeaderRec = packed record
    Version: USHORT;
    NumTables: USHORT;
  end;
  CmapTableInfoRec = packed record
    Platform_ID: USHORT;
    Encoding_ID: USHORT;
    Offset: ULONG;
  end;
     (*The format 4 table contains segCount segments *)
  TCMap4HEAD = record
    Format: USHORT;
    length: USHORT;
    version: USHORT;
  end;

  TCMap4Segment = packed record
    endCount: array[0..1000] of UShort;
    zero_byte: UShort;
    startCount: array[0..1000] of UShort;
    idDelta: array[0..1000] of Short;
    idRangeOffset: array[0..1000] of UShort;
  end;


  (* Microsoft standard character to glyph index mapping table *)
  TCMap4 = record
    segCountX2: UShort; (* segments number * 2          *)
    searchRange: UShort; (* these parameters can be used *)
    entrySelector: UShort; (* for a binary search          *)
    rangeShift: UShort;
  end;

{ Misc. types }

  PLongint = ^Longint;
  TDirectory = packed array[0..2047] of TableInfoRec;
  PDirectory = ^TDirectory;

const

{ "name" table constants }

  names: array[1..13] of string = ('hhea', 'loca', 'cmap',
    'head', 'maxp', 'cvt ',
    'prep', 'glyf', 'hmtx',
    'fpgm', 'name', 'post',
    'OS/2');
  colnames = 10;

{ --- TRUE TYPE FILE ------------------------------------------------------- }

type
  TNameEncoding = (neWindowsUnknown, neWindowsUnicode);
  TTrueTypeFile = class
  private
    FFileName: string;
    FHeader: FileHeaderRec;
    FDirectory: PDirectory;
    FTableCount: Integer;
    FHead: THead_type;
    FMaxp_type: Maxp_type;
    FLoca_big: PByteArray;
    FLoca_small: PByteArray;
    g_array_Id: Variant;
    FGlyf: PGlyf_type;
    CmapHeader: CmapHeaderRec;
    CmapTableInfo: CmapTableInfoRec;
    Cmap4_HEAD: TCMap4HEAD;
    CMap4: TCMap4;
    segments: TCMap4Segment;
    FGood: Boolean;
    procedure save_modifyed(st, st1: TStream);
    function IndexOf(Tag: string): Integer;
    procedure Error(Msg: string);
    procedure chistka;
    procedure cmap(st: Tstream);
  public
    destructor Destroy; override;
    procedure Load(st: Tstream);
    procedure Save(st, st1: Tstream);
    procedure getglyf(Unicode: UShort);
  end;

function Swap32(L: Longint): Longint; assembler;
asm
  bswap eax
end;

destructor TTrueTypeFile.Destroy;
var p, p1: PGlyf_type;
  i: integer;
begin
  p := Fglyf;
  if fgood = true then
  begin
    for i := 1 to swap(FMaxp_type.numGlyphs) do
    begin
      p1 := p.next;
      if p.Glyf_ <> nil then freemem(p.Glyf_);
      dispose(p);
      p := p1;
    end;
    if swap(Fhead.indexToLocFormat) = 1 then
      FreeMem(FLoca_big)
    else FreeMem(FLoca_small);
  end;
  FreeMem(FDirectory, FTableCount * sizeof(TableInfoRec));
  FDirectory := nil;
  FGlyf.next := nil;
  FLoca_big := nil;
  FLoca_small := nil;
  VarClear(g_array_Id);
  inherited Destroy;
end;

procedure TTrueTypeFile.Error(Msg: string);
begin
  if FFileName <> '' then
    Msg := Msg + ' (' + FFileName + ')';
  raise ETrueTypeFile.Create(Msg);
end;


function TTrueTypeFile.IndexOf(Tag: string): Integer;
var
  ITag: ULONG;
begin
  Result := -1;
  if Tag = '' then Exit;
  ITag := PLongint(Tag)^;
  Result := 0;
  while Result < FTableCount do
  begin
    if ITag = FDirectory^[Result].Tag then Break;
    Inc(Result);
  end;
  if Result = FTableCount then Result := -1;
end;

procedure TTrueTypeFile.Load(st: TStream);
var
  MemSize: Integer;
begin
  if st.Read(FHeader, sizeof(FHeader)) <> sizeof(FHeader) then
    Error(SFileCorrupt);
  if Swap(FHeader.Version.Hi) <> 1 then
    Error(SUnknownVersion);
  FTableCount := Swap(FHeader.NumTables);
  MemSize := FTableCount * sizeof(TableInfoRec);
  GetMem(FDirectory, MemSize);
  if st.Read(FDirectory^, MemSize) <> MemSize then
  begin
    Error(SFileCorrupt);
  end;
  CMap(st);
end;


procedure TTrueTypeFile.Save(st, st1: Tstream);
var
  i, j, x: integer;
  FHeader_save: FileHeaderRec;
  off, size, size1, offset: longint;
  Buffer: pchar;
  tt: TableInfoRec;
begin
//������ header
  if Fgood then
  begin
    save_modifyed(st, st1);
    Exit;
  end;
  FHeader_save := FHeader; x := 0;
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then x := x + 1;
  end;
  FHeader_save.NumTables := Swap(x);
  if st1.Write(FHeader_save, sizeof(FHeader)) <> sizeof(FHeader) then Error(SFileCorrupt);
  off := 12 + x * 16;
//������ heder ������
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then
    begin
      tt.Tag := FDirectory^[i].Tag;
      tt.Checksum := FDirectory^[i].Checksum;
      tt.Offset := swap32(off);
      off := off + Swap32(FDirectory^[i].length);
      off := (off + 3) and not 3;
      tt.Length := FDirectory^[i].length;
      if st1.Write(tt, 16) <> 16 then
        Error(SFileCorrupt);
    end
  end;
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then
    begin
      Size := Swap32(FDirectory^[I].Length);
      size1 := (Size + 3) and not 3;
      Offset := Swap32(FDirectory^[I].Offset);
      getmem(buffer, Size1);
      FillChar(buffer^, size1, 0);
      if st.Seek(Offset, soFromBeginning) <> Offset then
        Error(SFileReadError);
      if st.Read(buffer^, Size) <> Size then
        Error(SFileReadError);
      if st1.Write(buffer^, size1) <> size1 then
        Error(SFileCorrupt);
      freemem(buffer);
    end;
  end;
end;

procedure TTrueTypeFile.cmap(st: TStream);
var
  i, j: integer;
  off, offset, size: longint;
  UShort_read: UShort;
  Glyf_next, Glyf_tmp: PGlyf_type;
  x: Ulong;
  x1: Ushort;
begin
//������ cmap
  new(FGlyf);
  FGlyf.next := nil;
  FGlyf.Glyf_ := nil;
  Fgood := true;
  i := IndexOf('cmap');
  Offset := Swap32(FDirectory^[I].Offset);
  if st.Seek(offset, soFromBeginning) <> Offset then
    Error(SFileReadError);
  st.Read(CmapHeader, sizeof(CmapHeader));
  j := Swap(CmapHeader.NumTables);
  off := 0;
  for i := 1 to j do
  begin
    st.Read(CmapTableInfo, sizeof(CmapTableInfo));
    if (swap(CmapTableInfo.Platform_ID) = 3) and (swap(CmapTableInfo.Encoding_ID) = 1) then
    begin
      off := swap32(CmapTableInfo.Offset);
      break;
    end;
    if (swap(CmapTableInfo.Platform_ID) = 3) and (swap(CmapTableInfo.Encoding_ID) = 0) then
    begin
      off := swap32(CmapTableInfo.Offset);
    end;
  end;
  if off = 0 then
  begin
    Fgood := False;
    Exit;
  end;
  i := off + offset;
  if st.Seek(i, soFromBeginning) <> i then Error(SFileReadError);
  st.Read(Cmap4_HEAD, sizeof(Cmap4_HEAD));
  if swap(Cmap4_HEAD.Format) <> 4 then
  begin
    Fgood := false;
    Exit;
  end;
  st.Read(CMap4.segCountX2, 2);
  st.Read(CMap4.searchRange, 2);
  st.Read(CMap4.entrySelector, 2);
  st.Read(CMap4.rangeShift, 2);
  for i := 0 to trunc(swap(CMap4.segCountX2) / 2) - 1 do
  begin
    st.Read(UShort_read, 2);
    j := swap(UShort_read);
    segments.endCount[i] := j;
  end;
  st.Read(UShort_read, 2);
  segments.zero_byte := swap(UShort_read);
  for i := 0 to trunc(swap(CMap4.segCountX2) / 2) - 1 do
  begin
    st.Read(UShort_read, 2);
    segments.startCount[i] := swap(UShort_read);
  end;
  for i := 0 to trunc(swap(CMap4.segCountX2) / 2) - 1 do
  begin
    st.Read(UShort_read, 2);
    segments.idDelta[i] := swap(UShort_read);
  end;
  for i := 0 to trunc(swap(CMap4.segCountX2) / 2) - 1 do
  begin
    st.Read(UShort_read, 2);
    segments.idRangeOffset[i] := swap(UShort_read);
  end;
  j := trunc(swap(Cmap4_HEAD.length) / 2) - 8 - 4 * trunc(swap(CMap4.segCountX2) / 2);
  g_array_Id := VarArrayCreate([0, j], varSmallint);
  for i := 0 to j do
  begin
    st.Read(UShort_read, 2);
    g_array_Id[i] := swap(UShort_read);
  end;

 //������ maxp
  i := IndexOf('maxp');
  Offset := Swap32(FDirectory^[I].Offset);
  if st.Seek(Offset, soFromBeginning) <> Offset then
    Error(SFileReadError);
  st.Read(FMaxp_type, sizeof(FMaxp_type));

  //������ head
  i := IndexOf('head');
  Offset := Swap32(FDirectory^[I].Offset);
  if st.Seek(Offset, soFromBeginning) <> Offset then Error(SFileReadError);
  st.Read(FHead, sizeof(THead_type));

 //������ loca
  i := IndexOf('loca');
  Size := Swap32(FDirectory^[I].Length);
  Offset := Swap32(FDirectory^[I].Offset);
  if st.Seek(Offset, soFromBeginning) <> Offset then Error(SFileReadError);

  if swap(Fhead.indexToLocFormat) = 1 then
  begin
    getmem(FLoca_big, size);
    st.Read(FLoca_big^, size);
  end
  else
  begin
    getmem(FLoca_small, size);
    st.Read(FLoca_small^, size);
  end;

 //������ glyph
  i := IndexOf('glyf');
  Offset := Swap32(FDirectory^[I].Offset);
  if st.Seek(Offset, soFromBeginning) <> Offset then Error(SFileReadError);

  Glyf_next := Fglyf;
  j := swap(FMaxp_type.numGlyphs);
  for i := 1 to j do
  begin
    if swap(Fhead.indexToLocFormat) = 1 then
    begin
      x := byte(FLoca_big[i * 4]) shl 24 + byte(FLoca_big[i * 4 + 1]) shl 16 +
        byte(FLoca_big[i * 4 + 2]) shl 8 + byte(FLoca_big[i * 4 + 3]);
      size := x - byte(FLoca_big[(i - 1) * 4]) shl 24 - byte(FLoca_big[(i - 1) * 4 + 1]) shl 16 -
      byte(FLoca_big[(i - 1) * 4 + 2]) shl 8 - byte(FLoca_big[(i - 1) * 4 + 3]);
    end
    else
    begin
      size := 2 * (byte(FLoca_small[i * 2]) shl 8 + byte(FLoca_small[i * 2 + 1]) -
        byte(FLoca_small[(i - 1) * 2]) shl 8 - byte(FLoca_small[(i - 1) * 2 + 1]));
    end;
    if size <> 0 then getmem(Glyf_next.Glyf_, size) else Glyf_next.Glyf_ := nil;
    Glyf_next.num := i;
    Glyf_next.size := size;
    Glyf_next.tag := false;
    if swap(Fhead.indexToLocFormat) = 1 then
    begin
      x := byte(FLoca_big[(i - 1) * 4]) shl 24 + byte(FLoca_big[(i - 1) * 4 + 1]) shl 16 +
      byte(FLoca_big[(i - 1) * 4 + 2]) shl 8 + byte(FLoca_big[(i - 1) * 4 + 3]);
      st.Seek(Offset + x, soFromBeginning)
    end else
    begin
      x1 := byte(FLoca_small[(i - 1) * 2]) shl 8 + byte(FLoca_small[(i - 1) * 2 + 1]);
      st.Seek(Offset + 2 * x1, soFromBeginning);
    end;
    st.Read(Glyf_next.Glyf_^, size);
    if i <> j then
    begin
      new(glyf_tmp);
      glyf_tmp.next := nil;
      glyf_tmp.Glyf_ := nil;
      Glyf_next.next := glyf_tmp;
      Glyf_next := glyf_tmp;
    end else Glyf_next := nil;
  end;
end;

procedure TTrueTypeFile.getglyf(Unicode: UShort);
var
  i: integer;
  off: longint;
  unicode_glyf: UShort;
  Glyf_next: PGlyf_type;
begin
  if not Fgood then Exit;
  unicode_glyf := 0;
  for i := 0 to trunc(swap(CMap4.segCountX2) / 2) - 1 do
    if (unicode <= segments.endCount[i]) and (unicode >= segments.startCount[i]) then
    begin
      if segments.idRangeOffset[i] = 0 then
      begin
        unicode_glyf := unicode + segments.idDelta[i];
        Break;
      end
      else
      begin
        off := i + trunc(segments.idRangeOffset[i] / 2) - trunc(swap(CMap4.segCountX2) / 2) + unicode - segments.startCount[i];
        unicode_glyf := UShort(g_array_Id[off]) + segments.idDelta[i];
        Break;
      end;
    end
    else unicode_glyf := 0;
  Glyf_next := FGlyf;
  for i := 1 to swap(FMaxp_type.numGlyphs) do
  begin
    if (i = unicode_glyf + 1) then
    begin
      Glyf_next.tag := true; break; end;
    Glyf_next := Glyf_next.next;
  end;
end;

procedure TTrueTypeFile.save_modifyed(st, st1: TStream);
  function SizeOfGlyf(): ULONG;
  var i: longint;
    size: ULONG;
    Glyf_next: PGlyf_type;
  begin
    Glyf_next := Fglyf; size := 0;
    for i := 1 to swap(FMaxp_type.numGlyphs) do
    begin
      size := size + Glyf_next.size;
      Glyf_next := Glyf_next.next;
    end;
    result := size;
  end;

var
  i, j, x: integer;
  off, offset, size, size1: longint;
  Glyf_next: PGlyf_type;
     // ��������� ��� ������
  FHeader_save: FileHeaderRec;
  Buffer1: pchar;
  tt: TableInfoRec;

begin
//              ������� �������� ���������� �������                  //
//������ header
  chistka;
  FHeader_save := FHeader; x := 0;
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then x := x + 1;
  end;
  FHeader_save.NumTables := Swap(x);
  if st1.Write(FHeader_save, sizeof(FHeader)) <> sizeof(FHeader) then
    Error(SFileCorrupt);
  off := 12 + x * 16; // ��������� offset ����� ��������� � ������ ������
//������ heder ������
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then
    begin
      tt.Tag := FDirectory^[i].Tag;
      tt.Checksum := FDirectory^[i].Checksum;
      tt.Offset := swap32(off);
      tt.Length := FDirectory^[i].length;
      if names[j] = 'glyf' then
      begin
        off := off + SizeOfGlyf();
        tt.Length := swap32(SizeOfGlyf());
      end;
      if (names[j] <> 'glyf') then off := off + Swap32(FDirectory^[i].length);
      off := (off + 3) and not 3;
      if st1.Write(tt, 16) <> 16 then
        Error(SFileCorrupt);
    end
  end;

//������ ����� ������
  for j := 1 to colnames do
  begin
    i := IndexOf(names[j]);
    if i >= 0 then
    begin
      Size := Swap32(FDirectory^[I].Length);
      size1 := (Size + 3) and not 3;
      Offset := Swap32(FDirectory^[I].Offset);
      getmem(buffer1, Size1);
      FillChar(buffer1^, size1, 0);
      if st.Seek(Offset, soFromBeginning) <> Offset then Error(SFileReadError);
      if st.Read(buffer1^, Size) <> Size then Error(SFileReadError);
      if names[j] = 'loca' then
      begin
        if swap(Fhead.indexToLocFormat) = 1 then st1.Write(FLoca_big^, 4 + 4 * swap(FMaxp_type.numGlyphs))
        else st1.Write(FLoca_small^, 2 + 2 * swap(FMaxp_type.numGlyphs));
        off := trunc(size / 4) * 4;
        if off <> size then
          st1.Write(FLoca_small[swap(FMaxp_type.numGlyphs)], (off + 4 - size));
      end;
      if names[j] = 'glyf' then
      begin
        Glyf_next := Fglyf;
        for i := 1 to swap(FMaxp_type.numGlyphs) do
        begin
          st1.Write(Glyf_next.Glyf_^, Glyf_next.size);
          Glyf_next := Glyf_next.next;
        end;
        off := ((SizeOfGlyf() + 3) and not 3);
        if SizeOfGlyf() < off then
          st1.Write(j, off - SizeOfGlyf());
      end;
      if (names[j] <> 'glyf') and (names[j] <> 'loca') then
        if st1.Write(buffer1^, size1) <> size1 then Error(SFileCorrupt);
      freemem(buffer1);
    end;
  end;

end;

procedure TTrueTypeFile.chistka;
  procedure composit_glyf(Glyf: PGlyf_type);
  var j, x: integer;
    Glyf_next: PGlyf_type;
    ind: integer;
    all: boolean; // All glyf are processed
  begin
    Glyf_next := Fglyf;
    ind := 11; // first flag of comp. glyf in GLYF
    all := false;
    j := byte(Glyf.Glyf_[ind + 1]) shl 8 + byte(Glyf.Glyf_[ind + 2]); // index glyf
    for x := 1 to j do Glyf_next := Glyf_next.next;
    Glyf_next.tag := true;
    if (byte(Glyf_next.Glyf_[0]) = 255) and (byte(Glyf_next.Glyf_[1]) = 255) then composit_glyf(Glyf_next);

     //��������� �� ���������� ���. �����
    repeat
      if (byte(Glyf.Glyf_[ind])) and (33) = 33 then ind := ind + 8
      else if (byte(Glyf.Glyf_[ind])) and (32) = 32 then ind := ind + 6
      else all := true;
      j := byte(Glyf.Glyf_[ind + 1]) shl 8 + byte(Glyf.Glyf_[ind + 2]); // index glyf
      Glyf_next := Fglyf;
      for x := 1 to j do Glyf_next := Glyf_next.next;
      Glyf_next.tag := true;
      if (byte(Glyf_next.Glyf_[0]) = 255) and (byte(Glyf_next.Glyf_[1]) = 255) then composit_glyf(Glyf_next);
    until all = true;
  end;
var
  i: integer;
  off, x: longint;
  Glyf_next: PGlyf_type;
  x1: Ushort;
  mod_loca: PChar;
begin
  if not Fgood then Exit;
//������ glyf-��
  Glyf_next := Fglyf;
  for i := 1 to swap(FMaxp_type.numGlyphs) do
  begin
    if (Glyf_next.tag = true) and (Glyf_next.size <> 0) then
      if (byte(Glyf_next.Glyf_[0]) = 255) and (byte(Glyf_next.Glyf_[1]) = 255)
        then composit_glyf(Glyf_next);
    Glyf_next := Glyf_next.next;
  end;

  off := 0; Glyf_next := Fglyf;
  for i := 1 to swap(FMaxp_type.numGlyphs) do
  begin
    if Glyf_next.tag = false then Glyf_next.size := 0;
        // ������������ loca
    if swap(Fhead.indexToLocFormat) = 1 then
    begin
      x := swap32(off);
      mod_loca := @x;
      FLoca_big[(i - 1) * 4] := byte(mod_loca[0]);
      FLoca_big[(i - 1) * 4 + 1] := byte(mod_loca[1]);
      FLoca_big[(i - 1) * 4 + 2] := byte(mod_loca[2]);
      FLoca_big[(i - 1) * 4 + 3] := byte(mod_loca[3]);
      off := off + Glyf_next.size;
    end
    else
    begin
      x1 := swap(trunc(off / 2));
      mod_loca := @x1;
      FLoca_small[(i - 1) * 2] := byte(mod_loca[0]);
      FLoca_small[(i - 1) * 2 + 1] := byte(mod_loca[1]);
      off := off + Glyf_next.size;
    end;
    Glyf_next := Glyf_next.next;
  end;
  if swap(Fhead.indexToLocFormat) = 1 then
  begin
    x := swap32(off);
    mod_loca := @x;
    FLoca_big[swap(FMaxp_type.numGlyphs) * 4] := byte(mod_loca[0]);
    FLoca_big[swap(FMaxp_type.numGlyphs) * 4 + 1] := byte(mod_loca[1]);
    FLoca_big[swap(FMaxp_type.numGlyphs) * 4 + 2] := byte(mod_loca[2]);
    FLoca_big[swap(FMaxp_type.numGlyphs) * 4 + 3] := byte(mod_loca[3]);
  end
  else
  begin
    x1 := swap(trunc(off / 2));
    mod_loca := @x1;
    FLoca_small[swap(FMaxp_type.numGlyphs) * 2] := byte(mod_loca[0]);
    FLoca_small[swap(FMaxp_type.numGlyphs) * 2 + 1] := byte(mod_loca[1]);
  end;
end;


procedure GetFontSubset(TP: TPDFFontEmbeddingType; Font: TPDFFont; OutStream: TStream);
var
  B: TBitmap;
  MS: TMemoryStream;
  FZ: Cardinal;
  TTF: TTrueTypeFile;
  s: string;
  I: Integer;
  CodePage: Integer;
  A: array of Word;
  W: PWideChar;
  OS: Integer;
begin
  B := TBitmap.Create;
  try
    B.Canvas.Font.Name := Font.RealName;
    B.Canvas.Font.Style := Font.RealStyle;
    FZ := GetFontData(B.Canvas.Handle, 0, 0, nil, 0);
    if FZ = GDI_ERROR then
      raise TPDFException.Create(SCannotReceiveDataForFont + Font.RealName);
    MS := TIsMemoryStream.Create;
    try
      MS.SetSize(FZ);
      if GetFontData(B.Canvas.Handle, 0, 0, MS.Memory, FZ) = GDI_ERROR then
        raise TPDFException.Create(SCannotReceiveDataForFont + Font.RealName);
      MS.Position := 0;
      TTF := TTrueTypeFile.Create;
      try
        s := '';
        TTF.Load(MS);
        if TP = fetUsedGlyphsOnly then
        begin
          for I := 32 to 255 do if Font.fUsed[I] then s := s + chr(I);
        end else for I := 32 to 255 do s := s + chr(I);
        case Font.RealCharset of
          EASTEUROPE_CHARSET: CodePage := 1250;
          RUSSIAN_CHARSET: CodePage := 1251;
          GREEK_CHARSET: CodePage := 1253;
          TURKISH_CHARSET: CodePage := 1254;
          BALTIC_CHARSET: CodePage := 1257;
        else
          CodePage := 1252;
        end;
        OS := MultiByteToWideChar(CodePage, 0, PChar(S), Length(S), nil, 0);
        if OS = 0 then Exit;
        SetLength(A, OS);
        W := @a[0];
        if MultiByteToWideChar(CodePage, 0, PChar(S), Length(S), W, OS) <> 0 then
          for i := 0 to OS - 1 do TTF.getglyf(A[i]);
        A := nil;
        TTF.Save(MS, OutStream);
      finally
        TTF.Free;
      end;
    finally
      MS.Free;
    end;
  finally
    B.Free;
  end;
end;



end.

