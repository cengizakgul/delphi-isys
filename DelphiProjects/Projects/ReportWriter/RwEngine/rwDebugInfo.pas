unit rwDebugInfo;

interface

uses Classes, Sysutils, Controls, rwVirtualMachine, rwTypes, rwBasicClasses,
     rmTypes, rwBasicUtils, rwUtils, rwEngineTypes;

type

  {BreakPoints structures}

  TrwBreakPoint = record
    ComponentPath: string;
    Event: string;
    LibClass: string;
    Line: Integer;
    Address: Integer;
  end;

  PTrwBreakPoint = ^TrwBreakPoint;

  TrwBreakPointsList = class(TList)
  private
    function GetItem(Index: Integer): TrwBreakPoint;
  public
    property Items[Index: Integer]: TrwBreakPoint read GetItem; default;

    destructor Destroy; override;
    function   Add(const AComponentPath: string; const AEvent: string; const ALibClass: string;
       const ALine: Integer; const AAddr: Integer): Integer;
    function   FindBreakPoint(const AComponentPath: string; const AEvent: string; const ALibClass: string;
       const ALine: Integer): Integer; overload;
    function   FindBreakPoint(const AAddr: Integer): Integer; overload;
    procedure  ChangeComponentName(const AOldPath: string; const ANewPath: string);
    procedure  Clear; override;
    procedure  Delete(Index: Integer);
    procedure  SetAddress(const AIndex: Integer; const AAddress: Integer);
  end;



  {Watch List}

  TrwWatch = record
    Expression: string;
    DataSet: Boolean;
    Address: Integer;
    Result: String;
    Error: Boolean;
    PointerType: String;
  end;

  PTrwWatch = ^TrwWatch;


  TrwWatchListInfo = class(TList)
  private
    FCompiledCode: TrwCode;
    FNeedCompileWatches: Boolean;

    function GetItem(Index: Integer): PTrwWatch;
  public
    property Items[Index: Integer]: PTrwWatch read GetItem; default;
    property CompiledCode: TrwCode read FCompiledCode;
    property NeedCompileWatches: Boolean read FNeedCompileWatches write FNeedCompileWatches;

    constructor Create;
    destructor Destroy; override;
    function   Add(const AExpression: string): Integer;
    procedure  Clear; override;
    procedure  Delete(Index: Integer); overload;
    procedure  Delete(AWatch: PTrwWatch); overload;
    procedure  SetLengthCompiledCode(const ALen: Integer);
    procedure  Compile;
  end;



  {TrwDebugInfo store debugging information}

  TrwDebugInfo = class
  private
    FWaiting: Boolean;
    FDesignerControl: TWinControl;
    FCurrentEventPosition: Integer;
    FAborted: Boolean;
    FCurrentAsmPosition: Integer;
    FCodeSegment: TrwVMCodeSegment;
    FReport: IrmReportObject;
    FBreakPointsList: TrwBreakPointsList;
    FWatchList: TrwWatchListInfo;
    FCurrentComponent: TrwComponent;

    procedure SetCurrentEventPosition(Value: Integer);
    procedure SetCurrentAsmPosition(const Value: Integer);
    procedure SetCodeSegment(const Value: TrwVMCodeSegment);
    procedure SetWaiting(const Value: Boolean);
    function  GetAborted: Boolean;
    procedure SetReport(const Value: IrmReportObject);
    procedure SetCurrentComponent(const Value: TrwComponent);

  public
    CurDebLine: Integer;
    LineBegAddr: Integer;
    LineEndAddr: Integer;
    PDebInfo: PTrwDebInf;
    PDebSymbInfo: PTrwDebInf;
    Identif: string;
    Line: Integer;
    DontStop: Boolean;
    GoByStep: Boolean;
    TraceInto: Boolean;
    CalcWatches: Boolean;
    CurrentEvent: string;
    CurrentLibClass: string;
    ErrorHappend: Boolean;
    InputFormCode: Boolean;

    property CodeSegment: TrwVMCodeSegment read FCodeSegment write SetCodeSegment;
    property DesignerControl: TWinControl read FDesignerControl write FDesignerControl;
    property Waiting: Boolean read FWaiting write SetWaiting;
    property CurrentEventPosition: Integer read FCurrentEventPosition write SetCurrentEventPosition;
    property Aborted: Boolean read GetAborted;
    property CurrentAsmPosition: Integer read FCurrentAsmPosition write SetCurrentAsmPosition;
    property Report: IrmReportObject read FReport write SetReport;
    property BreakPointsList: TrwBreakPointsList read FBreakPointsList;
    property WatchList: TrwWatchListInfo read FWatchList;
    property CurrentComponent: TrwComponent read FCurrentComponent write SetCurrentComponent;

    constructor Create;
    destructor  Destroy; override;
    function    CheckForBreakPoint(AAddr: Integer): Integer;
    procedure   Abort;
    procedure   Initialize;
    procedure   SetBrkPointsAddr(const AIndex: Integer = -1);
    function    GetDebSymbInfo(const ADebSymbInfo: PTrwDebInf; const AIndent: string; const AEvent: string): string;
    function    GetDebInfo(const ADebInfo: PTrwDebInf; ASymbInfo: string; const ALine: Integer): string;
  end;


var
  DebugInfo: TrwDebugInfo = nil;


procedure InitDebugInfo(const ADesignerControl: TWinControl);
procedure FreeDebugInfo;
function  Debugging: Boolean;

implementation

uses rwParser;

function Debugging: Boolean;
begin
  Result := Assigned(DebugInfo) and Assigned(DebugInfo.Report);
end;

procedure InitDebugInfo(const ADesignerControl: TWinControl);
begin
  if Assigned(DebugInfo) then
    DebugInfo.Free;
  DebugInfo := TrwDebugInfo.Create;
  DebugInfo.DesignerControl := ADesignerControl;
end;


procedure FreeDebugInfo;
begin
  if Assigned(DebugInfo) then
     DebugInfo.Free;
  DebugInfo := nil;
end;

  {TrwDebugInfo}

constructor TrwDebugInfo.Create;
begin
  FBreakPointsList := TrwBreakPointsList.Create;
  FWatchList := TrwWatchListInfo.Create;

  Initialize;
end;


procedure TrwDebugInfo.SetCurrentEventPosition(Value: Integer);
begin
  FCurrentEventPosition := Value;
  if Assigned(DesignerControl) then
    FDesignerControl.Perform(RW_CHANGE_DEBUG_POSITION, 0, 0);
end;


procedure TrwDebugInfo.SetCurrentAsmPosition(const Value: Integer);
begin
  FCurrentAsmPosition := Value;
  if Assigned(DesignerControl) then
    if Supports(FDesignerControl, IrmDesigner) then
      (FDesignerControl as IrmDesigner).Notify(rmdVMChangeIPreg, FCurrentAsmPosition)
    else
      FDesignerControl.Perform(RW_CHANGE_DEBUG_ASM_POSITION, 0, 0);
end;


function TrwDebugInfo.CheckForBreakPoint(AAddr: Integer): Integer;
begin
  Result := BreakPointsList.FindBreakPoint(AAddr);

  if Assigned(DesignerControl) then
    if Result <> -1 then
      if Supports(FDesignerControl, IrmDesigner) then
        (FDesignerControl as IrmDesigner).Notify(rmdVMBreakPointFound, Result)
      else
        FDesignerControl.Perform(RW_BREAKPOINT_FOUND, Result, 0);
end;


procedure TrwDebugInfo.Abort;
begin
  DebugInfo.Waiting := False;
  FAborted := True;
end;


procedure TrwDebugInfo.SetCodeSegment(const Value: TrwVMCodeSegment);
begin
  FCodeSegment := Value;

  if Assigned(DesignerControl) then
    if Supports(FDesignerControl, IrmDesigner) then
      (FDesignerControl as IrmDesigner).Notify(rmdVMChangeCSreg, FCodeSegment)
    else
      FDesignerControl.Perform(RW_CHANGE_DEBUG_ASM_CS, 0, 0);
end;


procedure TrwDebugInfo.SetWaiting(const Value: Boolean);
begin
  if FWaiting <> Value then
  begin
    FWaiting := Value;
    if FWaiting then
      DontStop := False;
  end;
end;


function TrwDebugInfo.GetAborted: Boolean;
begin
  Result := FAborted;
end;

destructor TrwDebugInfo.Destroy;
begin
  FreeAndNil(FBreakPointsList);
  FreeAndNil(FWatchList);

  inherited;
end;

procedure TrwDebugInfo.Initialize;
begin
  FCurrentEventPosition := 0;
  FCurrentAsmPosition := 0;
  CurDebLine := -1;
  LineBegAddr := -1;
  LineEndAddr := -1;

//  FDesignerControl := nil;
  PDebInfo := nil;
  PDebSymbInfo := nil;
  CurrentComponent := nil;
  FReport := nil;

  FWaiting := False;
  FAborted := False;
  CalcWatches := False;
  ErrorHappend := False;
  InputFormCode := False;
end;

function TrwDebugInfo.GetDebSymbInfo(const ADebSymbInfo: PTrwDebInf; const AIndent: string; const AEvent: string): string;
var
  i: Integer;
  h1, h2: string;
begin
  Result := '';
  for i := 0 to High(ADebSymbInfo^) do
  begin
    h1 := ADebSymbInfo^[i];
    h2 := GetNextStrValue(h1, ';');
    if SameText(h2, AIndent) then
    begin
      h2 := GetNextStrValue(h1, ';');
      if SameText(h2, AEvent) then
      begin
        Result := ADebSymbInfo^[i];
        break;
      end;
    end;
  end;
end;

function TrwDebugInfo.GetDebInfo(const ADebInfo: PTrwDebInf; ASymbInfo: string; const ALine: Integer): string;
var
  i, j, lFirstAddr, lEndAddr: Integer;
  h: string;
begin
  Result := '';
  GetNextStrValue(ASymbInfo, ';');
  GetNextStrValue(ASymbInfo, ';');
  lFirstAddr := StrToInt(GetNextStrValue(ASymbInfo, ';'));
  lEndAddr := StrToInt(GetNextStrValue(ASymbInfo, ';'));

  for i := lFirstAddr to lEndAddr do
  begin
    h := ADebInfo^[i-1];
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    GetNextStrValue(h, ';');
    j := StrToInt(GetNextStrValue(h, ';'));
    if j = ALine then
    begin
      Result := ADebInfo^[i-1];
      break;
    end;
  end;
end;


procedure TrwDebugInfo.SetBrkPointsAddr(const AIndex: Integer);
var
  i, a, n, m: Integer;
  Psymb, Pdeb: PTrwDebInf;
  h, lCompPath: string;
  C, Own: IrmDesignObject;
  lBaseAddr: Integer;
begin
  if AIndex = -1 then
  begin
    n := 0;
    m := BreakPointsList.Count - 1;
  end

  else
  begin
    n := AIndex;
    m := AIndex;
  end;

  for i := n to m do
  begin
    BreakPointsList.SetAddress(i, -1);
    C := ComponentByPath(BreakPointsList[i].ComponentPath, Report);
    if not Assigned(C) then
      continue;

    if BreakPointsList[i].LibClass <> '' then
    begin
      Own := C;
      while Assigned(Own) and not Own.ObjectInheritsFrom(BreakPointsList[i].LibClass) do
        Own := Own.GetObjectOwner;

      if Assigned(Own) then
      begin
        h := BreakPointsList[i].ComponentPath;
        lCompPath := Own.GetPathFromRootOwner;
        Delete(h, 1, Length(lCompPath));
        lCompPath := BreakPointsList[i].LibClass + h;
      end
      else
        lCompPath := '';

      Pdeb :=  SystemLibComponents.GetDebInf;
      Psymb := SystemLibComponents.GetDebSymbInf;
      lBaseAddr := GetOffSet(rcsLibComponents, Length(Report.GetCompiledCode^));
    end
    else
    begin
      lCompPath := BreakPointsList[i].ComponentPath;
      Pdeb := Report.GetDebInf;
      Psymb := Report.GetDebSymbInf;
      lBaseAddr := 0;
    end;

    h := GetDebSymbInfo(Psymb, lCompPath, BreakPointsList[i].Event);
    if h = '' then
      continue;

    h := GetDebInfo(Pdeb, h, BreakPointsList[i].Line);
    if h = '' then
      continue;
    a := StrToInt(GetNextStrValue(h, ';')) + lBaseAddr;
    BreakPointsList.SetAddress(i, a);
  end;

  if AIndex = -1 then
  begin
    i := 0;
    while i <= m do
    begin
      if BreakPointsList[i].Address = -1 then
      begin
       BreakPointsList.Delete(i);
       Dec(m);
      end
      else
        Inc(i);
    end;
  end
  else
   if BreakPointsList[AIndex].Address = -1 then
     BreakPointsList.Delete(AIndex);
end;

procedure TrwDebugInfo.SetReport(const Value: IrmReportObject);
begin
  FReport := Value;
  WatchList.NeedCompileWatches := True;
end;

procedure TrwDebugInfo.SetCurrentComponent(const Value: TrwComponent);
begin
  if FCurrentComponent <> Value then
  begin
    FCurrentComponent := Value;
    FWatchList.NeedCompileWatches := True;
  end;
end;

{TrwBreakPointsList}

destructor TrwBreakPointsList.Destroy;
begin
  Clear;
  inherited;
end;


function TrwBreakPointsList.GetItem(Index: Integer): TrwBreakPoint;
begin
  Result := TrwBreakPoint(inherited Items[Index]^);
end;


function TrwBreakPointsList.Add(const AComponentPath: string; const AEvent: string; const ALibClass: string;
  const ALine: Integer; const AAddr: Integer): Integer;
var
  R: PTrwBreakPoint;
begin
  New(R);
  R^.ComponentPath := AComponentPath;
  R^.Event := AEvent;
  R^.LibClass := ALibClass;
  R^.Line := ALine;
  R^.Address := AAddr;

  Result := inherited Add(R);
end;


procedure TrwBreakPointsList.ChangeComponentName(const AOldPath: string; const ANewPath: string);
var
  R: PTrwBreakPoint;
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    R := PTrwBreakPoint(inherited Items[i]);
    R^.ComponentPath := StringReplace(R^.ComponentPath, AOldPath, ANewPath, [rfIgnoreCase]);
  end;
end;


function TrwBreakPointsList.FindBreakPoint(const AComponentPath: string; const AEvent: string;
  const ALibClass: string; const ALine: Integer): Integer;
var
  i: Integer;
  R: TrwBreakPoint;
begin
  Result := -1;

  for i := 0 to Count - 1 do
  begin
    R := Items[i];
    if SameText(R.ComponentPath, AComponentPath) and SameText(R.Event, AEvent) and
       SameText(R.LibClass, ALibClass) and (R.Line = ALine) then
    begin
      Result := i;
      break;
    end;
  end;
end;


function TrwBreakPointsList.FindBreakPoint(const AAddr: Integer): Integer;
var
  i: Integer;
  R: TrwBreakPoint;
begin
  Result := -1;

  for i := 0 to Count - 1 do
  begin
    R := Items[i];
    if R.Address = AAddr then
    begin
      Result := i;
      break;
    end;
  end;
end;


procedure TrwBreakPointsList.Delete(Index: Integer);
var
  R: PTrwBreakPoint;
begin
  R := inherited Items[Index];
  Dispose(R);
  inherited Delete(Index);
end;


procedure TrwBreakPointsList.Clear;
var
  R: PTrwBreakPoint;
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    R := inherited Items[i];
    Dispose(R);
  end;

  inherited Clear;
end;


procedure TrwBreakPointsList.SetAddress(const AIndex, AAddress: Integer);
begin
  TrwBreakPoint(inherited Items[AIndex]^).Address := AAddress;
end;



{ TrwWatchListInfo }

constructor TrwWatchListInfo.Create;
begin
  inherited;
  FNeedCompileWatches := False;
end;


destructor TrwWatchListInfo.Destroy;
begin
  Clear;
  inherited;
end;


function TrwWatchListInfo.Add(const AExpression: string): Integer;
var
  R: PTrwWatch;
begin
  FNeedCompileWatches := True;
  New(R);
  R^.Expression := AExpression;
  R^.DataSet := False;
  R^.Address := -1;
  R^.Result := '';
  R^.PointerType := '';
  R^.Error := False;
  Result := inherited Add(R);
end;


procedure TrwWatchListInfo.Clear;
var
  R: PTrwWatch;
  i: Integer;
begin
  FNeedCompileWatches := True;
  
  for i := 0 to Count - 1 do
  begin
    R := inherited Items[i];
    Dispose(R);
  end;
  inherited Clear;
end;


procedure TrwWatchListInfo.Delete(Index: Integer);
var
  R: PTrwWatch;
begin
  FNeedCompileWatches := True;
  R := inherited Items[Index];
  Dispose(R);
  inherited Delete(Index);
end;


procedure TrwWatchListInfo.Delete(AWatch: PTrwWatch);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if Items[i] = AWatch then
    begin
      Delete(i);
      break;
    end;
end;


function TrwWatchListInfo.GetItem(Index: Integer): PTrwWatch;
begin
  Result := PTrwWatch(inherited Items[Index]);
end;


procedure TrwWatchListInfo.SetLengthCompiledCode(const ALen: Integer);
begin
  SetLength(FCompiledCode, ALen);
end;



procedure TrwWatchListInfo.Compile;
var
  h1, lVarInfo, lCompPath: string;
  i: Integer;
  C: IrmDesignObject;
begin
  SetLengthCompiledCode(0);
  SetCompiledCode(Addr(CompiledCode), cVMDsgnWatchListOffset);
  SetCompilingPointer(0);
  SetDebInf(nil);
  SetDebSymbInf(nil);

  for i := 0 to Count - 1 do
  begin
    h1 := DebugInfo.WatchList[i].Expression + '; end'#0;
    DebugInfo.WatchList[i].Address := -1;

    C := ComponentByPath(DebugInfo.Identif, DebugInfo.Report);
    if not Assigned(C) then
      continue;

    if Length(DebugInfo.CurrentLibClass) > 0 then
    begin
      if C.IsLibComponent and C.ObjectInheritsFrom(DebugInfo.CurrentLibClass) then
        lCompPath := DebugInfo.CurrentLibClass
      else
        lCompPath := DebugInfo.CurrentLibClass + '.' + C.GetPropertyValue('Name');
    end
    else
      lCompPath := DebugInfo.Identif;

    lVarInfo := DebugInfo.GetDebSymbInfo(DebugInfo.PDebSymbInfo, lCompPath, DebugInfo.CurrentEvent);
    if Length(lVarInfo) = 0 then
      Continue;

    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    GetNextStrValue(lVarInfo, ';');
    lVarInfo := GetNextStrValue(lVarInfo, ';');

    try
      DebugInfo.WatchList[i].Result := '';
      DebugInfo.WatchList[i].PointerType := '';
      DebugInfo.WatchList[i].Address := GetCompilingPointer + cVMDsgnWatchListOffset;
      CompileWatchList(h1, lVarInfo, DebugInfo.CurrentComponent);

      with GetCodeInsightInfo do
        if VarType = rwvPointer then
          if Assigned(ClassInf) then
            DebugInfo.WatchList[i].PointerType := ClassInf.GetClassName
          else
            DebugInfo.WatchList[i].PointerType := 'Pointer'

        else if VarType = rwvUnknown then
          raise ErwException.Create('Incorrect watch expression');

    except
      on E: Exception do
      begin
        SetLengthCompiledCode(DebugInfo.WatchList[i].Address - cVMDsgnWatchListOffset);
        if E is ErwParserError then
          DebugInfo.WatchList[i].Result := ErwParserError(E).GeneralMessage
        else
          DebugInfo.WatchList[i].Result := E.Message;
        SetCompilingPointer(DebugInfo.WatchList[i].Address - cVMDsgnWatchListOffset);
        DebugInfo.WatchList[i].Address := -1;
      end;
    end;
  end;

  SetLengthCompiledCode(GetCompilingPointer);

  NeedCompileWatches := False;
end;

initialization

finalization
  FreeDebugInfo;

end.
