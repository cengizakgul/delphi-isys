unit sbConstants;

interface

uses Classes, Variants, SysUtils, StrUtils;

type
  TsbConstType = (sctString, sctInteger, sctDateTime, sctFloat);

  TsbCustomSQLConstant = class(TCollectionItem)
  private
    FConstName: String;
    FStrValue: String;
    FConstType: TsbConstType;
    FValue: Variant;
    FDisplayName: String;
    procedure DestroyValue;
    procedure SetStrValue(const Value: String);
    procedure SetConstType(const Value: TsbConstType);
  protected
    function  GetDisplayName: string; override;
    procedure SetDisplayName(const AValue: string); override;
  public
    property ConstName: String read FConstName write FConstName;
    property StrValue: String read FStrValue write SetStrValue;
    property ConstType: TsbConstType read FConstType write SetConstType;

    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(ASource: TPersistent); override;
    function    Value: Variant;
  end;


  TsbCustomSQLConstants = class(TCollection)
  private
    function  GetItem(Index: Integer): TsbCustomSQLConstant;
    procedure SetItem(Index: Integer; const Value: TsbCustomSQLConstant);
  public
    class function Initialize: TsbCustomSQLConstants; virtual; abstract;
    function ConstantByName(const ConstName: String): TsbCustomSQLConstant;
    property Items[Index: Integer]: TsbCustomSQLConstant read GetItem write SetItem; default;
  end;

  
  TsbCustomSQLConstantClass = class of TsbCustomSQLConstant;
  TsbCustomSQLConstantsClass = class of TsbCustomSQLConstants;

implementation


{ TsbCustomSQLConstant }

constructor TsbCustomSQLConstant.Create(Collection: TCollection);
begin
  inherited;
  FValue := Unassigned;
end;

destructor TsbCustomSQLConstant.Destroy;
begin
  DestroyValue;
  inherited;
end;

procedure TsbCustomSQLConstant.DestroyValue;
begin
//  if VarIsArray(FValue) then
//    Dispose(PTsbArrayParam(Pointer(Integer(FValue[0]))));
  FValue := Unassigned;
end;

procedure TsbCustomSQLConstant.SetConstType(const Value: TsbConstType);
begin
  FConstType := Value;
  DestroyValue;
end;

procedure TsbCustomSQLConstant.SetStrValue(const Value: String);
begin
  FStrValue := Value;
  DestroyValue;
end;

function TsbCustomSQLConstant.Value: Variant;
var
  i, j, n: Integer;
  lValue: Variant;

  function CastStrVal(const Val: String): Variant;
  begin
    if AnsiSameText(Val, 'Null') then
      Result := varNull
    else
      case FConstType of
        sctString:   Result := Val;
        sctInteger:  Result := StrToInt(Val);
        sctDateTime: Result := StrToDateTime(Val);
        sctFloat:    Result := StrToFloat(Val);
      end;
  end;

begin
  if VarIsEmpty(FValue) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if VarIsEmpty(FValue) then
      begin
        if Pos(#13, FStrValue) = 0 then
          lValue := CastStrVal(FStrValue)
        else
        begin
          i := 1;
          n := 1;
          while i <= Length(FStrValue) do
          begin
            if FStrValue[i] = #13 then
              Inc(n);
            Inc(i);
          end;
          lValue := VarArrayCreate([0, n - 1], varVariant);

          i := 1;
          n := 0;
          while i <= Length(FStrValue) do
          begin
            j := PosEx(#13, FStrValue, i);
            if j = 0 then
              j := Length(FStrValue) + 1;

            lValue[n] := CastStrVal(Copy(FStrValue, i, j - i));
            i := j + 1;
            Inc(n);
          end;
        end;

        FValue := lValue;
      end;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := FValue;
end;

procedure TsbCustomSQLConstant.Assign(ASource: TPersistent);
begin
  ConstName := TsbCustomSQLConstant(ASource).ConstName;
  DisplayName := TsbCustomSQLConstant(ASource).DisplayName;
  StrValue := TsbCustomSQLConstant(ASource).StrValue;
  ConstType := TsbCustomSQLConstant(ASource).ConstType;
end;


function TsbCustomSQLConstant.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

procedure TsbCustomSQLConstant.SetDisplayName(const AValue: string);
begin
  FDisplayName := AValue;
end;

{ TsbCustomSQLConstants }

function TsbCustomSQLConstants.ConstantByName(const ConstName: String): TsbCustomSQLConstant;
var
  i: Integer;
begin
  Result := nil;
  for i := 0  to Count -1 do
    if AnsiSameText(Items[i].ConstName, ConstName) then
    begin
      Result := Items[i];
      break;
    end;
end;


function TsbCustomSQLConstants.GetItem(Index: Integer): TsbCustomSQLConstant;
begin
  Result := TsbCustomSQLConstant(inherited Items[Index]);
end;

procedure TsbCustomSQLConstants.SetItem(Index: Integer; const Value: TsbCustomSQLConstant);
begin
  inherited Items[Index].Assign(Value);
end;

end.
