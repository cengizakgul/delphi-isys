unit rwQBSortFldEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rwQBInfo, rwDataDictionary, ExtCtrls;

type
  TrwQBSortFldEditor = class(TForm)
    Label1: TLabel;
    cbFields: TComboBox;
    btnCancel: TButton;
    btnOK: TButton;
    rgDirection: TRadioGroup;
  private
  public
  end;

function QBEditSortField(AQBQuery: TrwQBQuery; AField: TrwQBSortField): Boolean;

implementation

{$R *.DFM}

uses rwQueryBuilderFrm;

function QBEditSortField(AQBQuery: TrwQBQuery; AField: TrwQBSortField): Boolean;
var
  Frm: TrwQBSortFldEditor;
  i: Integer;
  h: string;
begin
  Frm := TrwQBSortFldEditor.Create(Application);

  with Frm do
  try
    for i := 0 to AQBQuery.ShowingFields.Count - 1 do
    begin
      h := TrwQBShowingField(AQBQuery.ShowingFields[i]).GetFieldName(FQBFrm.WizardView);
      cbFields.Items.AddObject(h, TrwQBShowingField(AQBQuery.ShowingFields[i]));
    end;

    if Assigned(AField) then
    begin
      cbFields.ItemIndex := cbFields.Items.IndexOfObject(AField.Field);
      rgDirection.ItemIndex := Ord(AField.SortDirection);
    end;

    Result := (ShowModal = mrOK) and (cbFields.ItemIndex <> -1);

    if Result then
    begin
      if cbFields.ItemIndex = -1 then
        AField.FieldIntName := ''
      else
        AField.FieldIntName := TrwQBShowingField(cbFields.Items.Objects[cbFields.ItemIndex]).InternalName;
      AField.SortDirection := TrwQBSortDirection(rgDirection.ItemIndex);
    end;

  finally
    Free;
  end;
end;

end.
