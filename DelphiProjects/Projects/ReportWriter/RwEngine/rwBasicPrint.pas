unit rwBasicPrint;

interface

uses
    Windows, Classes, SysUtils, PDF, rwPrintElements, rwPrintPage, rwTypes,
    rwPrinters, Printers, rwPreviewUtils, rwPDFPrinter, rwEngineTypes,
    EvStreamUtils;

const
  sRWPrevRegKey = 'SOFTWARE\Evolution\Evolution\';

type
  {TrwPrinterPage is printers page}

  TrwPrinterPage = class(TrwVirtualPage)
  private
    FPrevDuplexing: Integer;
    procedure SetPaperSize(PaperSize: TrwPaperSize; Length: Integer; Width: Integer;
                           Orientation: TrwPaperOrientation; ADuplexing: Boolean);
  protected
    procedure OnChangeRepository; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure Print;
  end;


  procedure PrintReport(const AStream: TStream; const AMarks: TrwPrintMarks; const pPrintJobInfo: PTrwPrintJobInfoRec; const ALabelChanges: IrwLabelChanges); overload;
  procedure PrintReport(const AStream: IEvDualStream; const AMarks: TrwPrintMarks; const pPrintJobInfo: PTrwPrintJobInfoRec; const ALabelChanges: IrwLabelChanges); overload;
  procedure PrintReport(const AFileName: string; const pPrintJobInfo: PTrwPrintJobInfoRec); overload;
  procedure SetPrinterSettings(const pPrinterInfo: PTrwPrintJobInfoRec);
  procedure PDFReport(const AFileName: String; AStream: TStream; AMarks: TrwPrintMarks);
  function  ConvertRWAtoPDF(const AStream: TStream; const AMarks: TrwPrintMarks; const pPDFInfoRec: PTrwPDFInfoRec; const AFileName: TFileName = ''): IEvDualStream;
  function  IsItOldFormat(AStream: TStream): Boolean;
  function  ConvertOldFormat(AStream: TStream): Boolean; overload;
  procedure SkipZeros(AStream: TStream);


implementation

uses dynapdf, ISBasicUtils;

procedure SetPrinterSettings(const pPrinterInfo: PTrwPrintJobInfoRec);
var
  i: Integer;
begin
  if Assigned(pPrinterInfo) then
    rwPrinter.PrintJobInfo^ := pPrinterInfo^;

  if rwPrinter.PrinterIndex <> rwPrinter.Printers.IndexOf(rwPrinter.PrintJobInfo.PrinterName) then
  begin
    i := rwPrinter.Printers.IndexOf(rwPrinter.PrintJobInfo.PrinterName);
    if i = -1 then
    begin
      rwPrinter.Refresh;
      i := rwPrinter.Printers.IndexOf(rwPrinter.PrintJobInfo.PrinterName);
    end;
    rwPrinter.PrinterIndex := i;
  end;

  rwPrinter.Title := rwPrinter.PrintJobInfo.PrintJobName;
  rwPrinter.PrintPCLAsGraphic := rwPrinter.PrintJobInfo.PrintPCLAsGraphic;
end;


procedure PrintReport(const AStream: IEvDualStream; const AMarks: TrwPrintMarks; const pPrintJobInfo: PTrwPrintJobInfoRec; const ALabelChanges: IrwLabelChanges); overload;
begin
  PrintReport(AStream.RealStream, AMarks, pPrintJobInfo, ALabelChanges);
end;

procedure PrintReport(const AStream: TStream;const AMarks: TrwPrintMarks; const pPrintJobInfo: PTrwPrintJobInfoRec; const ALabelChanges: IrwLabelChanges);
var
  lPage: TrwPrinterPage;

  procedure Print;
  var
    n, i, lBegPage, lEndPage: Integer;
  begin
    if (rwPrinter.PrintJobInfo.FirstPage = 0) or (rwPrinter.PrintJobInfo.LastPage = 0) then
    begin
      lBegPage := 1;
      lEndPage := lPage.Header.PagesCount;
    end
    else
    begin
      lBegPage := rwPrinter.PrintJobInfo.FirstPage;
      lEndPage := rwPrinter.PrintJobInfo.LastPage;
    end;
    n := lEndPage - lBegPage + 1;

    PrvStartWait('Printing...', n);

    try
      for i := lBegPage to lEndPage do
      begin
        PrvUpdateWait('Printing page ' + IntToStr(i - lBegPage + 1) + ' of ' + IntToStr(n), i);
        lPage.PreparePage(i, AStream);
        lPage.Print;
      end;

    finally
      if rwPrinter.Printing then
        rwPrinter.EndDoc;

      PrvEndWait;
//      PrvHideWait;
    end;
  end;

begin
  if rwPrinter is TrwDummyPrinter then
    raise Exception.Create('Please set up a printer and restart the application');

  GlobalNameSpace.BeginWrite;
  try
    lPage := TrwPrinterPage.Create(nil);
    try
      lPage.Initialize(AStream);

      if Assigned(AMarks) then
        lPage.Header.Marks.Assign(AMarks);

      if Assigned(ALabelChanges) then
        lPage.Header.LabelChangesList.AsStream := ALabelChanges.AsStream;

      SetPrinterSettings(pPrintJobInfo);

      Print;
    finally
      lPage.Free;
    end;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;


procedure PrintReport(const AFileName: string; const pPrintJobInfo: PTrwPrintJobInfoRec);
var
  lFileStream: TEvFileStream;
begin
  lFileStream := TEvFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    if AnsiSameText(ExtractFileExt(AFileName), '.rwa') then
      PrintReport(lFileStream, nil, pPrintJobInfo, nil);
  finally
    lFileStream.Free;
  end;
end;


function IsItOldFormat(AStream: TStream): Boolean;
var
  h: String;
begin
  Result := False;
  try
    AStream.Position := 0;
    h := StringOfChar(' ', 27);
    AStream.ReadBuffer(h[1], 27);
    Result := AnsiSameStr(h, 'TPF0'#22'TrwReportArchiveHeader');
  except
  end;
end;


function ConvertOldFormat(AStream: TStream): Boolean;
var
  j, n: Integer;
  C: TComponent;
  H: TrwReportArchiveHeader;
  VP: TrwVirtualPage;
  RS: TTempFileStream;
begin
  Result := False;
  try
    AStream.Position := 0;
    C := AStream.ReadComponent(nil);
    if C is TrwReportArchiveHeader then
      H := TrwReportArchiveHeader(C)
    else
    begin
      C.Free;
      H := nil;
    end;
  except
    H := nil;
  end;

  if not Assigned(H) then
    Exit;

  PrvStartWait('Converting into new format', H.PagesNum);
  try
    SkipZeros(AStream);
    RS := TTempFileStream.Create;
    VP := TrwVirtualPage.Create(nil);
    try
      RS.Position := 0;

      for j := 0 to H.PagesNum - 1 do
      begin
        PrvUpdateWait('Converting into new format', j + 1);
        AStream.ReadComponent(VP);
        CompressPage(VP);
        VP.Header.PagesList.Add(Pointer(RS.Position));
        RS.WriteComponent(VP);
      end;

      n := RS.Position;
      VP.Header.ReposList.Add(Pointer(n));
      RS.WriteComponent(VP.Repository);
      VP.WriteHeaderInfo(RS);

      RS.Size := RS.Position;
      AStream.Size := 0;
      AStream.CopyFrom(RS, 0);
    finally
      H.Free;
      VP.Free;
      RS.Free;
    end;
  finally
    PrvEndWait;
  end;

  Result := True;
end;


procedure SkipZeros(AStream: TStream);
var
  C: Char;
  i: Integer;
begin
  C := #0;
  repeat
    i := AStream.Read(C, 1);
  until (C <> #0) or (i = 0);
  if C <> #0 then
    AStream.Position := AStream.Position-1;
end;

procedure PDFReport(const AFileName: String; AStream: TStream; AMarks: TrwPrintMarks); overload;
begin
  ConvertRWAtoPDF(AStream, AMarks, nil, AFileName);
end;

procedure ApplySettingsToPrinterPage(pPDFInfoRec: PTrwPDFInfoRec;  aPDFinstance: TPDFDocument; ApplyProtection:Boolean);
begin
      if Assigned(pPDFInfoRec) then
      begin
        if pPDFInfoRec.Author <> '' then
          aPDFinstance.DocumentInfo.Author := pPDFInfoRec.Author;
        if pPDFInfoRec.Creator <> '' then
          aPDFinstance.DocumentInfo.Creator := pPDFInfoRec.Creator;
        if pPDFInfoRec.Subject <>'' then
          aPDFinstance.DocumentInfo.Subject := pPDFInfoRec.Subject;
        if pPDFInfoRec.Title <> '' then
          aPDFinstance.DocumentInfo.Title := pPDFInfoRec.Title;

        if pPDFInfoRec.Compression then
          aPDFinstance.Compression := ctFlate
        else
          aPDFinstance.Compression := ctNone;

        if ApplyProtection then //temporary file must be always created unprotected;
            if (pPDFInfoRec.ProtectionOptions = []) and (pPDFInfoRec.OwnerPassword = '') and (pPDFInfoRec.UserPassword = '') then
            begin
              aPDFinstance.ProtectionEnabled := False;
              aPDFinstance.ProtectionOptions := [];
            end
            else
            begin
              aPDFinstance.ProtectionEnabled := True;
              aPDFinstance.ProtectionOptions := TPDFCtiptoOptions(pPDFInfoRec.ProtectionOptions);
              aPDFinstance.OwnerPassword := pPDFInfoRec.OwnerPassword;
              aPDFinstance.UserPassword  := pPDFInfoRec.UserPassword;
            end
        else
        begin // internal non-ISMF apply protection case
              aPDFinstance.ProtectionEnabled := False;
              aPDFinstance.ProtectionOptions := [];
        end;
      end;
end;

function ConvertRWAtoUnprotectedPDFTempFile(const AStream: TStream; const AMarks: TrwPrintMarks; const pPDFInfoRec: PTrwPDFInfoRec): string;
// returns name of temp PDF file
var
  lPage: TrwPDFPrinterPage;
  tmpDynaPDFFile : String;
  tmpPDFFile : String;
  tmpStream : IisStream;

  procedure Print;
  var
    n: Integer;
    i: Integer;
  begin
    n := lPage.Header.PagesCount;

    PrvStartWait('Converting RWA to PDF...', n);
    try
      for i := 1 to lPage.Header.PagesCount do
      begin
        PrvUpdateWait('Current page ' + IntToStr(i) + ' of ' + IntToStr(n), i);
        lPage.PreparePage(i, AStream);
        lPage.Print;
      end;

    finally
      if lPage.PDFPrinter.Printing then
        lPage.PDFPrinter.EndDoc;
      PrvEndWait;
      PrvHideWait;
    end;
  end;

begin
  lPage := TrwPDFPrinterPage.Create(nil);

  if Assigned(lPage.DynaPDFPrinter) then
  begin

    tmpDynaPDFFile := AppTempFolder + 'DynaPDFOutput' + FormatDateTime('@MM-DD-YYYY-HH-NN-ZZZ-', Now)+ IntToStr(RandomInteger(0, 100000)) + '.pdf';
    tmpPDFFile := AppTempFolder + 'PDFOutput' + FormatDateTime('@MM-DD-YYYY-HH-NN-ZZZ-', Now)+ IntToStr(RandomInteger(0, 100000)) + '.pdf';

    try
      if not lPage.DynaPDFPrinter.CreateNewPDFA(tmpDynaPDFFile) then
         raise Exception.Create('Using DynaPDF. Cannot CreateNewPDFA ' + tmpDynaPDFFile);
//      try
      try
        ApplySettingsToPrinterPage( pPDFInfoRec, lPage.PDFPrinter, False);

        tmpStream := TEvDualStreamHolder.CreateFromNewFile(tmpPDFFile);
        lPage.PDFPrinter.OutputStream := tmpStream.RealStream;

        lPage.Initialize(AStream);
        // ticket 105488 - optimization on import : no extra fonts and backgrounds
        lPage.DynaPDFPrinter.SetImportFlags2(if2MergeLayers or if2DuplicateCheck);

        if Assigned(AMarks) then
          lPage.Header.Marks.Assign(AMarks);

        Print;

        // merging
        if lPage.DynaAmoutOfPrintedObj > 0 then
        begin
          tmpStream := nil;  // need to close file in order to flush data
          if pPDFInfoRec.OwnerPassword <> '' then
          begin
            if lPage.DynaPDFPrinter.OpenImportFileA(tmpPDFFile, ptOwner, pPDFInfoRec.OwnerPassword) <> 0 then
              raise Exception.Create('Using DynaPDF. Cannot OpenImportFile ' + tmpPDFFile)
          end
          else
          begin
            if pPDFInfoRec.UserPassword <> '' then
            begin
              if lPage.DynaPDFPrinter.OpenImportFileA(tmpPDFFile, ptOpen, pPDFInfoRec.UserPassword) <> 0 then
                raise Exception.Create('Using DynaPDF. Cannot OpenImportFile ' + tmpPDFFile)
            end
            else
              if lPage.DynaPDFPrinter.OpenImportFileA(tmpPDFFile, ptOpen, '') <> 0 then
                raise Exception.Create('Using DynaPDF. Cannot OpenImportFile ' + tmpPDFFile);
          end;
          if lPage.DynaPDFPrinter.ImportPDFFile(1, 1.0, 1.0) < 0 then
            raise Exception.Create('Using DynaPDF. Cannot ImportPDFFile ' + tmpPDFFile);
        end;

      finally
        lPage.DynaPDFPrinter.CloseFile;
      end;
// if we had ISMF background, result will be in tmpDynaPDFFile
// otherwise, result will be in stream - tmpStream, without file presentation yet

      if lPage.DynaAmoutOfPrintedObj > 0 then // check for ISMF background; PDF file already created
      begin
        if Assigned(tmpStream) then
          tmpStream := nil;  // need to close file
        DeleteFile(tmpPDFFile); // it is rwa-content without PDF background, already overprinted over ISMF
        Result := tmpDynaPDFFile;
      end
      else// Evolution-generated content only, without 3rd party PDF from ISMF;  tmpDynaPDFFile is empty and to be deleted;
      begin
        Result := tmpPDFFile;
        DeleteFile(tmpDynaPDFFile);
      end;
    finally
      lPage.Free;
    end;
  end;
end;

procedure PDFFileToStreamClear(PDFFileName: string; StreamOut:IEvDualStream);
var
  tmpStream: TFileStream;
  sMem: TMemoryStream;
begin
  tmpStream := TFileStream.Create(PDFFileName, fmOpenRead or fmShareDenyNone );
  try
     sMem := TMemoryStream.Create;
     try
       sMem.CopyFrom(tmpStream, 0);
       sMem.Position := 0;
       StreamOut.RealStream.CopyFrom(sMem, 0);
     finally
       sMem.Free;
     end;
  finally
     tmpStream.Free;
     DeleteFile(PDFFileName);
  end;
end;

function ConvertRWAtoPDF(const AStream: TStream; const AMarks: TrwPrintMarks; const pPDFInfoRec: PTrwPDFInfoRec; const AFileName: TFileName = ''): IEvDualStream;
var
  secPage: TrwPDFPrinterPage;
  criptPDFfile: string;
  tmpMediaPDFFile: string;
  tmpRestrict : integer;
  effectiveOwnerPassword: string;
  effectiveUserPassword: string;
begin
   if AFileName <> '' then
     Result := TEvDualStreamHolder.CreateFromNewFile(AFileName)
   else
     Result := TEvDualStreamHolder.Create;

   tmpMediaPDFFile:=ConvertRWAtoUnprotectedPDFTempFile(AStream, AMarks, pPDFInfoRec);
   if (NOT Assigned(pPDFInfoRec))
       or
      (pPDFInfoRec.ProtectionOptions = [])
   then
   begin
      PDFFileToStreamClear(tmpMediaPDFFile, Result);
   end
   else
   begin
     try
         criptPDFFile := AppTempFolder + 'SecPDFOutput' + FormatDateTime('@MM-DD-YYYY-HH-NN-ZZZ-', Now)+ IntToStr(RandomInteger(0, 100000)) + '.pdf';

         secPage := TrwPDFPrinterPage.Create(nil);
         try

           secPage.DynaPDFPrinter.CreateNewPDFA('');
           ApplySettingsToPrinterPage( pPDFInfoRec, secPage.PDFPrinter, True);

           secPage.DynaPDFPrinter.SetImportFlags( ifImportAll or ifImportAsPage);
           if secPage.DynaPDFPrinter.OpenImportFileA(tmpMediaPDFFile, ptOwner, '') <> 0 then
                raise Exception.Create('Using DynaPDF. Cannot Open tmp file to apply security ' + tmpMediaPDFFile);
           if secPage.DynaPDFPrinter.ImportPDFFile(1, 1.0,1.0) < 0 then
            raise Exception.Create('Using DynaPDF. Cannot ImportPDFFile on apply security TMP ' + tmpMediaPDFFile);
           secPage.DynaPDFPrinter.CloseImportFile;
           if secPage.DynaPDFPrinter.HaveOpenDoc then
           begin
             if NOT secPage.DynaPDFPrinter.OpenOutputFileA(criptPDFFile) then
                raise Exception.Create('Using DynaPDF. Cannot OpenOutputFile tmp apply security ' + tmpMediaPDFFile);
           end;
           tmpRestrict := 0;
           {   // ticket 105786

              // this is working only for Adobe Reader. Chrome forbids everything for any password; Mozilla permits averything for any password

          if coPrint in TPDFCtiptoOptions(pPDFInfoRec.ProtectionOptions) then
            tmpRestrict := tmpRestrict or rsPrint;
          if coModifyStructure in TPDFCtiptoOptions(pPDFInfoRec.ProtectionOptions) then
            tmpRestrict := tmpRestrict or rsModify;
          if coCopyInformation in TPDFCtiptoOptions(pPDFInfoRec.ProtectionOptions) then
            tmpRestrict := tmpRestrict or rsCopyObj;
          if coModifyAnnotation in TPDFCtiptoOptions(pPDFInfoRec.ProtectionOptions) then
            tmpRestrict := tmpRestrict or rsAddObj;


          if NOT secPage.DynaPDFPrinter.CloseFileEx(pPDFInfoRec.UserPassword, pPDFInfoRec.OwnerPassword, kl128bit, tmpRestrict) then
                raise Exception.Create('Using DynaPDF. Cannot finalize apply security TMP ' + tmpMediaPDFFile);
           }

           // ticket 105786
           // we decide use the only password  - Owner password; and only one protection - coModifyStructure ... unless better approach to be worked out in future

           if (Length(pPDFInfoRec.UserPassword) > 0 )
              or
              (Length(pPDFInfoRec.OwnerPassword) > 0 )
           then
                 tmpRestrict := rsModify;   // set protection level not concering printing abilities

           if (Length(pPDFInfoRec.UserPassword) > 0 )
              and
              (Length(pPDFInfoRec.OwnerPassword) > 0 )
           then
           begin
             EffectiveUserPassword := pPDFInfoRec.UserPassword;
             EffectiveOwnerPassword := pPDFInfoRec.OwnerPassword;
           end;

           if (Length(pPDFInfoRec.UserPassword) = 0 )
              and
              (Length(pPDFInfoRec.OwnerPassword) > 0 )
           then
           begin
             EffectiveUserPassword := pPDFInfoRec.OwnerPassword;
             EffectiveOwnerPassword := pPDFInfoRec.OwnerPassword;
           end;

           if (Length(pPDFInfoRec.UserPassword) > 0 )
              and
              (Length(pPDFInfoRec.OwnerPassword) = 0 )
           then
           begin
             EffectiveUserPassword := pPDFInfoRec.UserPassword;
             EffectiveOwnerPassword := pPDFInfoRec.UserPassword;
           end;

           if (Length(pPDFInfoRec.UserPassword) = 0 )
              and
              (Length(pPDFInfoRec.OwnerPassword) = 0 )
           then
           begin
             EffectiveUserPassword := '';
             EffectiveOwnerPassword := '';
           end;

          if NOT secPage.DynaPDFPrinter.CloseFileEx(EffectiveUserPassword, EffectiveOwnerPassword, kl128bit, tmpRestrict) then
                raise Exception.Create('Using DynaPDF. Cannot finalize apply security TMP ' + tmpMediaPDFFile);

          if NOT FileExists( criptPDFFile) then
                raise Exception.Create('Using DynaPDF. Cannot find secured TMP ' + tmpMediaPDFFile);
         finally
           secPage.Free;
         end;
       finally
         PDFFileToStreamClear(criptPDFFile, Result);
         DeleteFile(tmpMediaPDFFile);
       end;
    end;
end;


{TrwPrinterPage}

constructor TrwPrinterPage.Create(AOwner: TComponent);
begin
  inherited;
  FPrevDuplexing := 0;
end;


procedure TrwPrinterPage.Print;
var
  n: Word;
  p: Integer;
  C: TrwPrintObject;
  i: Integer;
begin
  FCanvas := rwPrinter.Canvas;

  FScaleFonts := False;

  DefinePrintMetric;
  ChangeZoom(FPrnResX/PixelsPerInch, FPrnResY/PixelsPerInch);

  SetPaperSize(PageSize, System.Round(Height*Kf_ZoomY), System.Round(Width* Kf_ZoomY), PageOrientation, Duplexing);

  // need to update offsets according to paper orientation
  DefinePrintMetric;

  SetViewportOrgEx(FCanvas.Handle,
    -FPrinterOffSets.x + rwPrinter.PrintJobInfo.PrinterHorizontalOffset,
    -FPrinterOffSets.y + rwPrinter.PrintJobInfo.PrinterVerticalOffset, nil);

  ChangeZoom(FPrnResX/PixelsPerInch, FPrnResY/PixelsPerInch);

  FData.Position := 0;
  while FData.Position < FData.Size do
  begin
    FData.ReadBuffer(n, SizeOf(n));
    p := FData.Position;
    C := TrwPrintObject(Repository.Components[n]);
    if C.Visible then
      C.Print(Self)
    else
      C.PrepareToPaint(FData);  //skip object data
    if FData.Position <= p then
      break;
  end;

//marks
  for i := 0 to Header.Marks.Count -1 do
    if Header.Marks[i].PageNum = PageNum then
      DrawMark(Header.Marks[i], FCanvas);
end;


procedure TrwPrinterPage.SetPaperSize(PaperSize: TrwPaperSize; Length,  Width: Integer;
                                      Orientation: TrwPaperOrientation; ADuplexing: Boolean);
var
  PDMode: PDEVMODE;
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port: array[0..255] of char;
  hDeviceMode: THandle;

  procedure CheckTray;
  var
    n: Integer;
    lTray: String;
  begin
    if rwPrinter.PrintJobInfo.TrayName = '' then
      lTray := Tray
    else
      lTray := rwPrinter.PrintJobInfo.TrayName;

    if lTray <> '' then
{      if rwPrinter.PCLDriver and not rwPrinter.PrintPCLasGraphic then
      begin
        if FTray = 1 then
          n := 8
        else
          n := 18 + FTrayNbr;
        rwPrinter.PCLPrintString(#27'&l' + IntToStr(n) + 'H');
      end

      else }
      begin
        n := rwPrinter.Trays.IndexOf(lTray);
        if n <> -1 then
        begin
          pDMode^.dmFields := pDMode^.dmFields or DM_DEFAULTSOURCE;
          pDMode^.dmDefaultSource := Word(rwPrinter.Trays.Objects[n]);
        end;
      end;
  end;


  procedure CheckOutBin;
  var
    lOutBin: Integer;
  begin
    if rwPrinter.PrintJobInfo.OutBinNbr = 0 then
      lOutBin := OutBin
    else
      lOutBin := rwPrinter.PrintJobInfo.OutBinNbr;

    if lOutBin > 0 then
      if rwPrinter.PCLDriver and not rwPrinter.PrintPCLasGraphic then
        rwPrinter.PCLPrintString(#27'&l' + IntToStr(lOutBin) + 'G')
      else
      begin
        //how to ????
      end;
  end;


  procedure CheckDuplex;
  var
    lAlwaysDuplexPrinting: Boolean;
    dx: Integer;
  begin
    if ((rwPrinter.OriginalDevMode.dmFields and DM_DUPLEX) <> 0) and (rwPrinter.OriginalDevMode.dmDuplex <> DMDUP_SIMPLEX) then
      dx := rwPrinter.OriginalDevMode.dmDuplex

    else
    begin
      lAlwaysDuplexPrinting := (ADuplexing or (rwPrinter.PrintJobInfo.Duplexing = rdpDuplex)) and
                               (rwPrinter.PrintJobInfo.Duplexing <> rdpSimplex);

      if lAlwaysDuplexPrinting and (pcDuplexing in rwPrinter.Capabilities) then
        dx := DMDUP_VERTICAL
      else
        dx := DMDUP_SIMPLEX;
    end;

    if FPrevDuplexing <> dx then
    begin
      FPrevDuplexing := dx;

      if rwPrinter.PCLDriver and not rwPrinter.PrintPCLasGraphic then
      begin
        if FPrevDuplexing = DMDUP_SIMPLEX then
          dx := 0
        else
          dx := 1;
        CheckOutBin;
        rwPrinter.PCLPrintString(#27'&l' + IntToStr(dx) + 'S');
      end
      else
      begin
        pDMode^.dmFields := pDMode^.dmFields or DM_DUPLEX;
        pDMode^.dmDuplex := dx;
        CheckOutBin;
      end
    end
    else
      CheckOutBin;
  end;


begin
  if rwPrinter.Printing then
    rwPrinter.EndPage
  else
    rwPrinter.BeginDoc;

  rwPrinter.GetPrinter(Device, Driver, Port, hDeviceMode);

  pDMode := GlobalLock(hDeviceMode);
  try
    pDMode^.dmFields := pDMode^.dmFields or dm_PaperSize;
    case PaperSize of
      psExecutive:
        pDMode^.dmPaperSize := DMPAPER_EXECUTIVE;
      psLetter:
        pDMode^.dmPaperSize := DMPAPER_LETTER;
      psLegal:
        pDMode^.dmPaperSize := DMPAPER_LEGAL;
      psTabloid:
        pDMode^.dmPaperSize := DMPAPER_TABLOID;
      psStatement:
        pDMode^.dmPaperSize := DMPAPER_STATEMENT;
      psA3:
        pDMode^.dmPaperSize := DMPAPER_A3;
      psA4:
        pDMode^.dmPaperSize := DMPAPER_A4;
      psA5:
        pDMode^.dmPaperSize := DMPAPER_A5;
      psB4:
        pDMode^.dmPaperSize := DMPAPER_B4;
      psB5:
        pDMode^.dmPaperSize := DMPAPER_B5;
      psFolio:
        pDMode^.dmPaperSize := DMPAPER_FOLIO;
      psQuarto:
        pDMode^.dmPaperSize := DMPAPER_QUARTO;
      ps10X14:
        pDMode^.dmPaperSize := DMPAPER_10X14;
      ps11X17:
        pDMode^.dmPaperSize := DMPAPER_11X17;
      psNote:
        pDMode^.dmPaperSize := DMPAPER_NOTE;
      psEnv9:
        pDMode^.dmPaperSize := DMPAPER_ENV_9;
      psEnv10:
        pDMode^.dmPaperSize := DMPAPER_ENV_10;
      psEnv11:
        pDMode^.dmPaperSize := DMPAPER_ENV_11;
      psEnv12:
        pDMode^.dmPaperSize := DMPAPER_ENV_12;
      psEnv14:
        pDMode^.dmPaperSize := DMPAPER_ENV_14;
      psCSheet:
        pDMode^.dmPaperSize := DMPAPER_CSHEET;
      psDSheet:
        pDMode^.dmPaperSize := DMPAPER_DSHEET;
      psESheet:
        pDMode^.dmPaperSize := DMPAPER_ESHEET;
      psCustom:
      begin
        pDMode^.dmPaperSize := DMPAPER_USER;
        pDMode^.dmPaperLength := Length;
        pDMode^.dmPaperWidth := Width;
      end;
    end;

    pDMode^.dmFields := pDMode^.dmFields or DM_ORIENTATION;
    case Orientation of
      rpoPortrait: pDMode^.dmOrientation := DMORIENT_PORTRAIT;
      rpoLandscape: pDMode^.dmOrientation := DMORIENT_LANDSCAPE;
    end;

    if not rwPrinter.PCLDriver or rwPrinter.PrintPCLasGraphic then
      CheckDuplex;

    CheckTray;

    ResetDC(rwPrinter.Handle, pDMode^);

  finally
    GlobalUnlock(hDeviceMode);
  end;

  if rwPrinter.PCLDriver and not rwPrinter.PrintPCLasGraphic then
    CheckDuplex;

  rwPrinter.NewPage;
end;


procedure TrwPrinterPage.OnChangeRepository;
begin
  inherited;

  if not rwPrinter.Printing or ((rwPrinter.PageNumber mod 2) = 0) then
    Exit;

  if rwPrinter.PrintJobInfo.Duplexing = rdpSimplex then
    Exit;

  if ((rwPrinter.OriginalDevMode.dmFields and DM_DUPLEX) <> 0) and (rwPrinter.OriginalDevMode.dmDuplex <> DMDUP_SIMPLEX) then
    Exit;

  if FPrevDuplexing <> DMDUP_SIMPLEX then
  begin
    rwPrinter.EndPage;
    rwPrinter.NewPage;
  end;
end;


end.
