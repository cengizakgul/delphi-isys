unit sbAPI;

interface

uses
  Windows, Classes, SysUtils, Consts, mmSystem, DBConsts, DB, Variants, RTLConsts,
  rwEngineTypes, ISBasicClasses, ISDataAccessComponents, EvStreamUtils, rwCustomDataDictionary,
  isUtils, isBasicUtils;


const
  sbSignature1_1 = 'SB1.1';
  sbFieldNameSize = 32;
  sbMaxFieldsNumber = 512;
//  sbMaxIndexesNumber = 256;
  sbReadBufferSize =  1024;
  sbWriteBufferSize = 2*1024*1024;
  sbBLOBClasterSize = 64;
  sbTableExt = '.sbt';
  sbBlobFileExt = '.sbb';
  sbIndexExt = '.sbi';

resourcestring
  sbeTableExists = 'Table "%s" already exists';
  sbeTableDoesntExist = 'Table "%s" doesn''t exist';
  sbeTableIsOpened = 'Can not apply this operation for opened table';
  sbeWrongTableStructure = 'Fields structure is incorrect';
  sbeWrongTableName = 'Table name "%s" is incorrect';
  sbeValueTypeIsWrong = 'Type of value is incorrect';
  sbeFieldTypeIsWrong = 'Type of field is incorrect';
  sbeQuantityFieldsBig = 'Quantity of fields exceeds the limit';
  sbeWrongTableFormat = 'Wrong table format';
  sbeIOStreamError = 'I/O stream error';
  sbeIndexIsNotSet = 'Index is not set';
  sbeTableInNotEditMode = 'Table is not in edit mode';
  sbeCannotCreateTableIndex = 'Cannot create table index';
  sbeInternalError = 'Internal SbCursor Error';
  sbeWrongData = 'Data and table structure must be adequate';

type

  TsbFieldType = (sbfString, sbfInteger, sbfByte, sbfDateTime, sbfFloat, sbfCurrency, sbfBoolean, sbfBLOB, sbfAutoInc, sbfUnknown);
  TsbCursorStatus = (sbcInactive, sbcBrowse, sbcAppend, sbcInsert, sbcEdit, sbcFindKey, sbcInternal);
  TsbAccessModeType = (sbmRead, sbmWrite, sbmReadWrite, sbmMemory);
  TsbBLOBClasterType = (sbbBLOBClaster, sbbEndBLOBClaster);
  TsbDataLoadMode =(sbDLMAll, sbDLMStructureOnly, sbDLMDataOnly);

  TsbIndexOption = (isbUnique, isbDescending, isbCaseInsensitive);
  TsbIndexOptions = set of TsbIndexOption;

  TsbStreamModeType = (sbsRead, sbsWrite, sbsCreate, sbsReadWriteShare);
  TsbStreamMode = set of TsbStreamModeType;

  EsbException = class(Exception);

  TsbStream = class
  private
    FFileName: String;
    FMode: TsbStreamMode;
    FSize: Cardinal;
    function GetSize: Cardinal; virtual; abstract;
    function GetPosition: Cardinal; virtual; abstract;
    procedure SetPosition(const Value: Cardinal); virtual; abstract;

  public
    constructor Create(const AFileName: string; AMode: TsbStreamMode); virtual;
    function Read(var Buffer; Count: Integer): Integer; virtual; abstract;
    function Write(const Buffer; Count: Integer): Integer; virtual; abstract;

    property FileName: string read FFileName;
    property Size: Cardinal read GetSize;
    property Position: Cardinal read GetPosition write SetPosition;
  end;


  TsbFileStream = class(TsbStream)
  private
    FHandleWrite: Integer;
    FHandleRead: Integer;
    FPosition: Cardinal;
    FOldWritePosition: Cardinal;
    FOldReadPosition: Cardinal;
    FSyncWithWriteBuff: Boolean;
    FOneHandle: Boolean;

    function GetSize: Cardinal; override;
    function GetPosition: Cardinal; override;
    procedure SetPosition(const Value: Cardinal); override;

  protected
    procedure SetBufferSize(AReadBufSize: Integer; AWriteBufSize: Integer);

  public
    constructor Create(const AFileName: string; AMode: TsbStreamMode); override;
    destructor Destroy; override;
    function Read(var Buffer; Count: Integer): Integer; override;
    function Write(const Buffer; Count: Integer): Integer; override;
    procedure FlushWriteBuffer;
  end;


  TsbMemoryStream = class(TsbStream)
  private
    FStream: TMemoryStream;
    function GetSize: Cardinal; override;
    function GetPosition: Cardinal; override;
    procedure SetPosition(const Value: Cardinal); override;

  public
    constructor Create(const AFileName: string; AMode: TsbStreamMode); override;
    destructor Destroy; override;
    function Read(var Buffer; Count: Integer): Integer; override;
    function Write(const Buffer; Count: Integer): Integer; override;
  end;


  TsbInterfacedObject = class(TObject, IInterface)
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;


  TsbItemList = class(TsbInterfacedObject)
  protected
    FList: TList;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure   Clear; virtual;
    procedure   Exchange(Index1, Index2: Integer);
    function ItemCount: Integer;
    function IndexOf(APointer: Pointer): Integer;
    procedure Assign(ASource: TsbItemList); virtual;
  end;


  TsbFldValRec = packed record
    FieldType: TsbFieldType;
    FieldSize: Byte;
    FieldName: String[sbFieldNameSize];
    NullValue: Boolean;
    RecOffset: Cardinal;
    case TsbFieldType of
      sbfString:        (StringVal:   ShortString);
      sbfInteger:       (IntegerVal:  Integer);
      sbfByte:          (ByteVal:     Byte);
      sbfDateTime:      (DateTimeVal: TDateTime);
      sbfFloat:         (FloatVal:    Double);
      sbfCurrency:      (CurrencyVal: Currency);
      sbfBoolean:       (BooleanVal:  ByteBool);
      sbfBlob:          (BlobVal:     Integer);
      sbfAutoInc:       (AutoIncVal:  Cardinal);
  end;

  PsbFldValRec = ^TsbFldValRec;

  TsbFields = class;
  TsbTableCursor = class;

  TsbField = class(TsbInterfacedObject, IrwSBField)
  private
    FPValRec: PsbFldValRec;
    FOwner: TsbFields;
    FInitialized: Boolean;
    FModified: Boolean;
    FIndex:   Integer;
    FDescInd: Boolean;
    FBLOBTable: TsbTableCursor;
    FBLOBData: String;
    procedure SetBLOBData(const AValue: String);
    function  GetFieldType: TsbFieldType;
    procedure FreeRecRes;
    procedure SyncContent;
    function  GetAsString: String;
    procedure SetAsString(const AValue: String);
    function  GetAsInteger: Integer;
    procedure SetAsInteger(const AValue: Integer);
    function  GetAsCardinal: Cardinal;
    function  GetAsByte: Byte;
    procedure SetAsByte(const AValue: Byte);
    function  GetAsDateTime: TDateTime;
    procedure SetAsDateTime(const AValue: TDateTime);
    function  GetAsCurrency: Currency;
    function  GetAsBoolean: ByteBool;
    function  GetAsFloat: Double;
    procedure SetAsCurrency(const AValue: Currency);
    procedure SetAsFloat(const AValue: Double);
    procedure SetAsBoolean(const AValue: ByteBool);
    procedure FreeBlobTable;
    procedure CheckMode;
    procedure InternalClear;
    procedure ReadFieldFromBuffer(ABuffer: Pointer);
    procedure NotifySetFieldValue;
    procedure CleanupBlobData;

    //Interface
    function  GetFieldName: string;
    function  GetDataType: TrwDDType;
    function  GetSize: Byte;
    function  GetIndex: Integer;
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;

  public
    constructor Create(AOwner: TsbFields; AName: String; AType: TsbFieldType; ASize: Integer = 0);
    destructor  Destroy; override;
    procedure   Clear;
    function    IsNull: Boolean;
    function    Cursor: TsbTableCursor;
    procedure   LoadBlobFromStream(const AStream: TStream; ASize: Integer = 0);
    procedure   SaveBlobToStream(const AStream: TStream);

    property FieldName: string read GetFieldName;
    property FieldType: TsbFieldType read GetFieldType;
    property Size: Byte read GetSize;
    property Modified: Boolean read FModified;
    property Value: Variant read GetValue write SetValue;
    property AsString: String read GetAsString write SetAsString;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property AsByte: Byte read GetAsByte write SetAsByte;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsCurrency: Currency read GetAsCurrency write SetAsCurrency;
    property AsBoolean:  ByteBool read GetAsBoolean write SetAsBoolean;
    property Index: Integer read FIndex;
  end;

  PsbNullInfo = ^TsbNullInfo;
  TsbNullInfo = array [1..(sbMaxFieldsNumber + 7) div 8] of Byte;


  TsbFields = class(TList)
  private
    FCursor: TsbTableCursor;
    FNullInfo: TsbNullInfo;
    FNullInfoInitialized: Boolean;
    FAutoincFieldIndex: Integer;
    function  GetItems(Index: Integer): TsbField;
    procedure ReadFieldFromStream(AField: TsbField);
    procedure WriteFieldsToStream;
    procedure SetInitializedFlag(AValue: Boolean);
    function  FieldValueIsNull(AFldIndex: Word): Boolean;
    procedure AddNullInfo(AFldIndex: Word; ANull: Boolean);
    procedure InitNullInfo;
    procedure ClearValues;
    procedure IncAutoIncFields;
    procedure DecAutoIncFields;
   public
    constructor Create;
    property Items[Index: Integer]: TsbField read GetItems; default;

    function  CreateField(AName: String; AType: TsbFieldType; ASize: Integer = 0): TsbField;
    procedure Clear; override;
    function  FindField(AName: string): TsbField;
    function  FieldByName(AName: string): TsbField;
    procedure Assign(ASource: TsbFields);
  end;


  TsbIndex = class
  private
    FName: String;
    FCursor: TsbTableCursor;
    FStream: TsbStream;
    FAccessMode: TsbAccessModeType;
    FFields: array of TsbField;
    FOptions: TsbIndexOptions;
    FHeaderSize: Integer;
    FIndexExprSize: Word;
    FIndexRecSize: Word;
    FBeginFirstRecordPos: Cardinal;
    FBeginLastRecordPos: Cardinal;
    FCurrRecordPos: Cardinal;
    FCurrTablePosition: Cardinal;
    FRecBuffer: Pointer;
    FPrevRecBuffer: Pointer;
    FBuffer: Pointer;
    FTemporary: Boolean;
    FRecordCount: Integer;

    function  IsEmptyIndex: Boolean;
    procedure ReadHeader;
    procedure WriteHeader;
    procedure GetIndexData(ABuffer: Pointer);
    procedure AllocBuf;
    procedure FreeBuf;
    function  CompareKeys(AKey1: Pointer; AKey2: Pointer): Shortint;
    procedure First;
    procedure Last;
    procedure Next;
    procedure Prior;
    procedure ReadCurrentKey;
    function  FindKey: Boolean;
    procedure SetInternalPos(const AIntPos: Cardinal);
    function  ValidInternalRecordPos(const Value: Cardinal): Boolean;

  public
    property    Name: String read FName;
    constructor Create(ATableCursor: TsbTableCursor; AIndexName: String; AMode: TsbAccessModeType = sbmReadWrite; ANewIndex: Boolean = False; ATemporary: Boolean = False);
    destructor  Destroy; override;
  end;



  TsbFilterRecordEvent = procedure(DataSet: TsbTableCursor; var Accept: Boolean) of object;

  TsbTableCursor = class(TInterfacedObject, IrwSBTable)
  private
    FStream: TsbStream;
    FFileName: string;
    FFields: TsbFields;
    FRecordSize: Cardinal;
    FCurrRecordPos: Cardinal;
    FStatus: TsbCursorStatus;
    FAccessMode: TsbAccessModeType;
    FBOF: Boolean;
    FEOF: Boolean;
    FRecordCount: Integer;
    FUpdHeaderInfoPos: Integer;
    FHeaderSize: Cardinal;
    FBeginFirstRecordPos: Cardinal;
    FBeginLastRecordPos: Cardinal;
    FBeginFirstDelRecordPos: Cardinal;
    FLastAutoIncValue: Cardinal;    
    FPrevRecordPos: Cardinal;
    FNextRecordPos: Cardinal;
    FNullInfoSize:  Cardinal;
    FModified: Boolean;
    FRecNo: Integer;
    FFiltered: Boolean;
    FFilter: String;
    FFilterSQLObject: TObject;
    FFilterCond: TObject;
    FFiltRec: TList;
    FOuterPosition: Boolean;
    FAuxInternalRecordPos: Int64;
    FNotify: Boolean;
    FNotifyCouter: Integer;
    FOnChangeDataState: TNotifyEvent;
    FOnAfterScroll: TNotifyEvent;
    FOnFilterRecord: TsbFilterRecordEvent;
    FInternalTag: Integer;
    FStInternalPos: Cardinal;
    FStIntIndPos: Cardinal;
    FStBof: Boolean;
    FStEof: Boolean;
    FStRecNo: Integer;
    FIndexEnabled: Boolean;
    FIntPosBeforeDisable: Cardinal;

    procedure Open(AFileName: string; ABLOBTable: Boolean = False); overload;
    procedure ReadHeader;
    procedure UpdateHeaderInfo;
    procedure WriteRecord;
    procedure WriteRecordHeader(APrevRec: Integer; ANextRec: Integer);
    procedure WriteRecordHeaderPrevPos(APrevRec: Cardinal);
    procedure WriteRecordHeaderNextPos(ANextRec: Cardinal);
    procedure ReadRecordHeader(var APrevRec, ANextRec: Cardinal);
//    procedure ReadRecordHeaderPrevPos(var APrevRec: Integer);
    procedure ReadRecordHeaderNextPos(var ANextRec: Cardinal);
    function  IsEmptyCursor: Boolean;
    procedure SetRecNo(const AValue: Integer);
    function  GetRecNo: Integer;
    procedure InitialStatus;
//    function  GetUniqIndexFileName: String;
    procedure CreateFilter(ACondition: TObject = nil);
    procedure DestroyFilter;
    function  GetRecordCount: Integer;
    procedure SetInternalRecordPos(const Value: Cardinal);
    function  InternalFindKey: Boolean;
    function  GetActive: Boolean;
    procedure SetFileName(const Value: string);
    function  CheckRecordForFilter: boolean;
    function  GetCanModify: Boolean;
    procedure NotifyStateChanged;
    procedure StoreState;
    procedure RestoreState;
    procedure SetIndexEnabled(const Value: Boolean);
    procedure SetBrowseStatus;

    // Interface
    function  TableFileName: String;
    function  GetFields: TrwSBFieldList;
    procedure AppendRecord(const FieldValues: array of Variant);
    function  IsEof: Boolean;
    function  IsBof: Boolean;
    function  GetFieldByName(const FieldName: String): IrwSBField;
    procedure SetFilter(const AFilter: String);
    function  GetFilter: String;
    procedure SetFiltered(const Value: Boolean);
    function  GetFiltered: Boolean;
    function  RealObject: TObject;

  protected
    FTemporary: Boolean;
    FAuxParams: TList;
    FIndexList: TStringList;
    FActiveIndex: TsbIndex;
    procedure BeforeOpen; virtual;
    procedure AfterOpen;  virtual;

  public
    property Fields: TsbFields read FFields;
    property RecordSize: Cardinal read FRecordSize;
    property BOF: Boolean read FBOF;
    property EOF: Boolean read FEOF;
    property RecordCount: Integer read GetRecordCount;
    property RecNo: Integer read GetRecNo write SetRecNo;
    property Filter: String read FFilter write FFilter;
    property Filtered: Boolean read FFiltered write SetFiltered;
    property OuterPosition: Boolean read FOuterPosition write FOuterPosition;
    property InternalRecordPos: Cardinal read FCurrRecordPos write SetInternalRecordPos;
    property AuxInternalRecordPos: Int64 read FAuxInternalRecordPos write FAuxInternalRecordPos;
    property State: TsbCursorStatus read FStatus;
    property Active: Boolean read GetActive;
    property TableName: string read FFileName write SetFileName;
    property AuxParams: TList read FAuxParams;
    property NotificationEnabled: Boolean read FNotify;
    property InternalTag: Integer read FInternalTag write FInternalTag;
    property IndexEnabled: Boolean read FIndexEnabled write SetIndexEnabled;
    property CanModify: Boolean read GetCanModify;
    property OnChangeDataState: TNotifyEvent read FOnChangeDataState write FOnChangeDataState;
    property OnAfterScroll: TNotifyEvent read FOnAfterScroll write FOnAfterScroll;
    property OnFilterRecord: TsbFilterRecordEvent read FOnFilterRecord write FOnFilterRecord;

    constructor Create; virtual;
    destructor  Destroy; override;
    procedure   Close; virtual;
    procedure   Open(const AAccessMode: TsbAccessModeType = sbmReadWrite); overload;
    procedure   First;
    procedure   Prior;
    procedure   Next;
    procedure   Last;
    procedure   MoveBy(const AValue: Integer);
    procedure   Append;
    procedure   Insert;
    procedure   Edit;
    procedure   Post;
    procedure   Cancel;
    procedure   Delete;
    procedure   Clear;
    procedure   DropTable;
    function    CreateIndex(AIndexName: string; const AFields: array of string; AOptions: TsbIndexOptions; ADontChangeCurrentIndex: Boolean = False): string;
    procedure   SetIndex(AIndexName: string; ATemporary: Boolean = False); virtual;
    procedure   CloseIndex(AIndexName: string); virtual;
    procedure   CloseActiveIndex;
    procedure   CloseAllIndexes;
    function    IsIndexSet: Boolean;
    function    FindKey(const AValues: array of variant): Boolean;
    function    FindKeyByFieldList(AList: TList): Boolean;
    function    LocateByPreparedCondition(ACondition: TObject): Boolean;
    function    Locate(const AFields: array of string; const AValues: array of variant;
                       const CaseSensitive: Boolean; const PartialKey: Boolean): Boolean;
    function    ValidInternalRecordPos(const Value: Cardinal): Cardinal;
    procedure   ApplayPreparedFilter(ACondition: TObject);
    function    IsKeyValueChanged: Boolean;
    procedure   DisableNotification;
    procedure   EnableNotification;
    procedure   NotifyChange;
    function    TableSize: Cardinal;
    procedure   FlushBuffers;

    function    GetStrData(const AMode: TsbDataLoadMode = sbDLMAll): String;
    procedure   SetStrData(const AData: String; AMode: TsbDataLoadMode);

  end;



  TsbParam = class(TsbInterfacedObject, IrwParam)
  private
    FValue: Variant;
    FName: String;

    procedure SetName(const AName: String);
    function  GetName: String;
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;
  public
    property Value: Variant read FValue write FValue;
    property Name:  String read FName write FName;
  end;


  TsbParams = class(TsbItemList, IrwParams)
  private
    function GetItem(Index: Integer): TsbParam;
  public
    function  Add(AParamName: string): Integer;
    function  ParamByName(AName: String): TsbParam;
    property  Items[Index: Integer]: TsbParam read GetItem; default;
    procedure Assign(ASource: TsbItemList); override;
    function  ParseSQL(const SQL: String; DoCreate: Boolean): String;

    function  GetParamByName(const AName: String): IrwParam;
    function  GetParamByIndex(const Index: Integer): IrwParam;
  end;


  TsbResultRec = record
    Cursor: TsbTableCursor;
    Plan: String;
    Cost: Int64;
    Time: Int64; 
  end;


  {TDataSet}

  PsbBookmark = ^TsbBookmark;
  TsbBookmark = record
    RecNo: Integer;
    InternalPos: Cardinal;
    BookmarkFlag: TBookmarkFlag;
  end;

  PRecInfo = ^TRecInfo;
  TRecInfo = record
    Data: Pointer;
    Bookmark: TsbBookmark;
  end;


  TsbBlobStream = class(TStream)
  private
    FField: TsbField;
    FPosition: Integer;
  public
    constructor Create(Field: TsbField);
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
  end;


  TsbDataSet = class(TDataSet)
  private
    FCursor: TsbTableCursor;
    FTmpFields: TsbFields;
    procedure SetCursor(const Value: TsbTableCursor);
    procedure GetCurRecord(Buffer: Pointer);

  protected
    procedure InternalOpen; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalClose; override;
    function  IsCursorOpen: Boolean; override;
    function  GetCanModify: Boolean; override;
    function  GetRecordSize: Word; override;
    function  GetRecDataSize: Word;
    function  AllocRecordBuffer: PChar; override;
    procedure FreeRecordBuffer(var Buffer: PChar); override;
    function  GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    procedure GetBookmarkData(Buffer: PChar; Data: Pointer); override;
    procedure SetBookmarkData(Buffer: PChar; Data: Pointer); override;
    function  GetBookmarkFlag(Buffer: PChar): TBookmarkFlag; override;
    procedure SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag); override;
    procedure InternalGotoBookmark(Bookmark: Pointer); override;
    procedure InternalSetToRecord(Buffer: PChar); override;
    procedure InternalFirst; override;
    procedure InternalLast; override;
    function  GetRecNo: Integer; override;
    procedure SetRecNo(Value: Integer); override;
    function  GetRecordCount: Integer; override;
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
    procedure InternalInitRecord(Buffer: PChar); override;
    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure InternalPost; override;
    procedure InternalRefresh; override;
    procedure InternalEdit; override;
    procedure InternalInsert; override;
    procedure InternalCancel; override;
    procedure InternalDelete; override;
    procedure InternalHandleException; override;
    function  GetNextRecords: Integer; override;
    function  GetPriorRecords: Integer; override;
    procedure SetFiltered(Value: Boolean); override;
    procedure SetFilterText(const Value: string); override;
    procedure SetOnFilterRecord(const Value: TFilterRecordEvent); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function  GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
    function  CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    procedure SyncPosition;
    procedure Resync(Mode: TResyncMode); override;
    function  Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; override;
    function  BookmarkValid(Bookmark: TBookmark): Boolean; override;
    function  CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;

    property  Cursor: TsbTableCursor read FCursor write SetCursor;
  end;


  TsbIndexPool = class
  private
    FList: TStringList;
  public
    constructor Create;
    destructor  Destroy; override;
  end;


  procedure ShowSBError(const AMessage: String; AParam: array of const);

  procedure sbCreateTable(AFileName: string; AFields: TsbFields; ABLOBTable: Boolean = False);

  function  sbOpenTable(const AFileName: string; ATableCursor: TsbTableCursor = nil;
                        AMode: TsbAccessModeType = sbmReadWrite; ATemporary: Boolean = False): TsbTableCursor; overload;

  function  sbOpenTable(const AFileName: string; AMode: TsbAccessModeType = sbmReadWrite; ATemporary: Boolean = False): IrwSBTable; overload;

  function  sbOpenQuery(const ASQL: string; AParams: TsbParams = nil; AResultTable: TsbTableCursor = nil): TsbResultRec;

  procedure sbExecQuery(const ASQL: string; AParams: TsbParams = nil);

  procedure sbClose(ATable: TsbTableCursor);

  procedure sbDropTable(ATable: TsbTableCursor); overload;

  procedure sbDropTable(const ATableName: string); overload;

  procedure sbPackTable(const AFileName: string);

  function  sbGetUniqueFileName(const APath: string; const APrefix: string; const AExt: string): string;

  procedure sbCreateDefFields(ADataSet: TDataSet; ATbl: TsbTableCursor);
  function  sbSBCursorToClientDataSet(ATbl: TsbTableCursor; ADataSet: TISBasicClientDataSet = nil): TISBasicClientDataSet;
  procedure sbClientDataSetToSBCursor(ADataSet: TISBasicClientDataSet; ATbl: TsbTableCursor; ACreateTable: Boolean);

  procedure sbCopyTable(ATbl: TsbTableCursor; const ANewName: string);

  procedure InitThreadVars_SUVbase;
  procedure FreeSysIndPool;
  function  sbFindSysIndInfo(AIndInfo: string): Integer;
  function  sbFindSysIndInfoByName(const AIndName: string): Integer;


implementation

uses sbSQL;

const
  sbFieldRecordSize = sbFieldNameSize + SizeOf(TsbFieldType) + SizeOf(Byte);


threadvar
  FSysIndInfo: TsbIndexPool;



procedure InitThreadVars_SUVbase;
begin
  FSysIndInfo := nil;

  InitThreadVars_sbSQL
end;


function  SBTypetoDDType(const AType: TsbFieldType): TDataDictDataType;
begin
  case AType of
    sbfInteger:       Result := ddtInteger;
    sbfFloat:         Result := ddtFloat;
    sbfCurrency:      Result := ddtCurrency;
    sbfDateTime:      Result := ddtDateTime;
    sbfString:        Result := ddtString;
    sbfBLOB:          Result := ddtBLOB;
    sbfBoolean:       Result := ddtBoolean;
    sbfAutoInc:       Result := ddtInteger;    
  else
    Result := ddtUnknown;
    Assert(True, 'Field type is not supported');
  end;
end;


function SysIndPool: TsbIndexPool;
begin
  if not Assigned(FSysIndInfo) then
    FSysIndInfo := TsbIndexPool.Create;
  Result := FSysIndInfo;
end;


procedure FreeSysIndPool;
begin
  FreeAndNil(FSysIndInfo);
end;


function sbFindSysIndInfo(AIndInfo: string): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to SysIndPool.FList.Count - 1 do
    if AnsiSameText(SysIndPool.FList.Names[i], AIndInfo) then
    begin
      Result := i;
      break;
    end;
end;


function sbFindSysIndInfoByName(const AIndName: string): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to SysIndPool.FList.Count - 1 do
    if AnsiSameText(SysIndPool.FList.Values[SysIndPool.FList.Names[i]], AIndName) then
    begin
      Result := i;
      break;
    end;
end;


function ConvertAccessMode(AMode: TsbAccessModeType): TsbStreamMode;
begin
  case AMode of
    sbmReadWrite: Result := [sbsRead, sbsWrite];
    sbmRead:      Result := [sbsRead];
    sbmWrite:     Result := [sbsWrite];
    sbmMemory:    Result := [sbsRead];
  end;
end;


function sbGetUniqueFileName(const APath: string; const APrefix: string; const AExt: string): string;
var
  i: Integer;
begin
  for i := 1 to 100000 do
  begin
    Result := APath+APrefix+IntToStr(i)+AExt;
    if not FileExists(Result) then
      break;
  end;
end;


procedure sbCreateDefFields(ADataSet: TDataSet; ATbl: TsbTableCursor);
var
  i, s: Integer;
  t: TFieldType;
begin
  ADataSet.FieldDefs.Clear;
  for i := 0 to ATbl.Fields.Count - 1 do
  begin
    s := 0;
    case ATbl.Fields[i].FieldType of
      sbfString:   begin
                     t := ftString;
                     s := ATbl.Fields[i].Size;
                   end;
      sbfInteger:  t := ftInteger;
      sbfAutoInc:  t := ftInteger;
      sbfByte:     begin
                     t := ftBytes;
                     s := 1;
                   end;
      sbfDateTime: t := ftDateTime;
      sbfFloat:    t := ftFloat;
      sbfCurrency: t := ftCurrency;
      sbfBoolean:  t := ftBoolean;
      sbfBLOB:     t := ftBLOB;
    else
      t := ftUnknown;
      ShowSBError(sbeFieldTypeIsWrong, []);
    end;
    ADataSet.FieldDefs.Add(ATbl.Fields[i].FieldName, t, s);
  end;
end;


function sbSBCursorToClientDataSet(ATbl: TsbTableCursor; ADataSet: TISBasicClientDataSet = nil): TISBasicClientDataSet;
var
  i, n: Integer;
begin
  if Assigned(ADataSet) then
    Result := ADataSet
  else
    Result := TISBasicClientDataSet.Create(nil);

  sbCreateDefFields(Result, ATbl);

  Result.CreateDataSet;
  Result.DisableControls;
  Result.Open;

  ATbl.StoreState;
  ATbl.DisableNotification;
  try
    ATbl.First;
    while not ATbl.Eof do
    begin
      Result.Append;
      n := ATbl.Fields.Count - 1;
      for i := 0 to n do
        Result.Fields[i].Value := ATbl.Fields[i].Value;
      Result.Post;
      ATbl.Next;
    end;
    Result.First;

  finally
    ATbl.RestoreState;
    ATbl.EnableNotification;
    Result.EnableControls;
  end;
end;


procedure sbClientDataSetToSBCursor(ADataSet: TISBasicClientDataSet; ATbl: TsbTableCursor; ACreateTable: Boolean);
var
  i, n: Integer;
  t: TsbFieldType;
  Flds: TsbFields;
  h: String;
begin
  if ACreateTable then
  begin
    Flds := TsbFields.Create;
    try
      n := ADataSet.Fields.Count - 1;
      for i := 0 to n do
      begin
        case ADataSet.Fields[i].DataType of
          ftString:    if ADataSet.Fields[i].Size <= 255 then
                         t := sbfString
                       else
                         t := sbfBLOB;

          ftInteger:   t := sbfInteger;
 
          ftDate,
          ftDateTime:  t := sbfDateTime;

          ftFloat,
          ftBCD:       t := sbfFloat;

          ftCurrency:  t := sbfCurrency;

          ftBoolean:   t := sbfBoolean;

          ftBLOB,
          ftMemo,
          ftGraphic:   t := sbfBLOB;
        else
          t := sbfUnknown;
          ShowSBError(sbeFieldTypeIsWrong, []);
        end;

        Flds.CreateField(ADataSet.Fields[i].FieldName, t, ADataSet.Fields[i].Size);
      end;

      h := ATbl.TableName;
      ATbl.DropTable;
      sbCreateTable(h, Flds);
    finally
      Flds.Free;
    end;
  end;


  ATbl.DisableNotification;
  ADataSet.DisableControls;
  try
    ATbl.Open;
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      ATbl.Append;
      n := ADataSet.Fields.Count - 1;
      for i := 0 to n do
        ATbl.Fields[i].Value := ADataSet.Fields[i].Value;
      ATbl.Post;
      ADataSet.Next;
    end;
    ATbl.First;

  finally
    ATbl.EnableNotification;
    ADataSet.EnableControls;
    ATbl.NotifyChange;
  end;
end;


procedure DeleteSysIndInfo(const ATableName: String); overload;
var
  i, j: Integer;
  h: string;
begin
  if not Assigned(FSysIndInfo) then
    Exit;

  i := 0;
  while i < SysIndPool.FList.Count do
  begin
    h := Copy(SysIndPool.FList.Names[i], 1, Length(ATableName));
    if AnsiSameText(h, ATableName) then
    begin
      try
        j := Pos('=', SysIndPool.FList[i]);
        h := Copy(SysIndPool.FList[i], j + 1, Length(SysIndPool.FList[i])- j);
        DeleteFile(h);
      except
      end;
      SysIndPool.FList.Delete(i);
    end
    else
      Inc(i);
  end;
end;


procedure DeleteSysIndInfo(ATable: TsbTableCursor); overload;
begin
  DeleteSysIndInfo(ATable.TableName);
end;


procedure sbCopyTable(ATbl: TsbTableCursor; const ANewName: string);

  procedure SimpleCopy;
  var
    i: Integer;
    h: string;
  begin
    ATbl.FlushBuffers;
    CopyFile(PChar(ATbl.TableName), PChar(ANewName), False);

    for i := 0 to ATbl.Fields.Count - 1 do
      if ATbl.Fields[i].FieldType = sbfBLOB then
      begin
        h := ChangeFileExt(ANewName, '') + '_' + ATbl.Fields[i].FieldName + sbBlobFileExt;
        CopyFile(PChar(ATbl.Fields[i].FBLOBTable.TableName), PChar(h), False);
      end;
  end;

  procedure CopyRowByRow;
  var
    NewFlds: TsbFields;
    NewTbl: TsbTableCursor;
    i, n: Integer;
  begin
    NewFlds := TsbFields.Create;
    try
      NewFlds.Assign(ATbl.Fields);
      sbCreateTable(ANewName, NewFlds);
    finally
      FreeAndNil(NewFlds);
    end;

    NewTbl := sbOpenTable(ANewName, nil);
    try
      ATbl.First;
      n := ATbl.Fields.Count - 1;
      while not ATbl.Eof do
      begin
        NewTbl.Append;

        for i := 0 to n do
          NewTbl.Fields[i].Value := ATbl.Fields[i].Value;

        NewTbl.Post;

        ATbl.Next;
      end;

    finally
      sbClose(NewTbl);
    end;
  end;

begin
  if ATbl.FStream is TsbFileStream then
  begin
    DeleteSysIndInfo(ANewName);

    if ATbl.IsIndexSet then
      CopyRowByRow
    else
      SimpleCopy;
  end;
end;


procedure ShowSBError(const AMessage: String; AParam: array of const);
begin
  raise EsbException.CreateFmt(AMessage, AParam);
end;


procedure WriteValue(AStream: TsbStream; Source: Pointer; AType: TsbFieldType; ASize: Integer = 0); overload;

  procedure WriteStr;
  var
    n: Integer;
  begin
    n := Length(ShortString(Source^)) + 1;
    if n < ASize then
      ZeroMemory(Pointer(Cardinal(Source) + Cardinal(n)), ASize - n);
    AStream.Write(Source, ASize + 1);
  end;

begin
  case AType of
    sbfString:   WriteStr;
    sbfByte:     AStream.Write(Source, SizeOf(Byte));
    sbfInteger:  AStream.Write(Source, SizeOf(Integer));
    sbfAutoInc:  AStream.Write(Source, SizeOf(Cardinal));    
    sbfBoolean:  AStream.Write(Source, SizeOf(ByteBool));
    sbfDateTime: AStream.Write(Source, SizeOf(TDateTime));
    sbfFloat:    AStream.Write(Source, SizeOf(Double));
    sbfCurrency: AStream.Write(Source, SizeOf(Currency));
    sbfBlob:     AStream.Write(Source, SizeOf(Integer));
    sbfUnknown:  AStream.Write(Source, ASize);
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
end;


procedure WriteValue(ASource: Pointer; ADest: Pointer; AType: TsbFieldType; ASize: Integer = 0); overload;

  procedure WriteStr;
  var
    n: Integer;
  begin
    n := Length(ShortString(ASource^)) + 1;
    if n < ASize then
      ZeroMemory(Pointer(Cardinal(ASource) + Cardinal(n)), ASize - n);
    Move(ASource^, ADest^, ASize + 1);
  end;

begin
  case AType of
    sbfString:   WriteStr;
    sbfByte:     Move(ASource^, ADest^, SizeOf(Byte));
    sbfInteger:  Move(ASource^, ADest^, SizeOf(Integer));
    sbfAutoInc:  Move(ASource^, ADest^, SizeOf(Cardinal));
    sbfBoolean:  Move(ASource^, ADest^, SizeOf(ByteBool));
    sbfDateTime: Move(ASource^, ADest^, SizeOf(TDateTime));
    sbfFloat:    Move(ASource^, ADest^, SizeOf(Double));
    sbfCurrency: Move(ASource^, ADest^, SizeOf(Currency));
    sbfBlob:     Move(ASource^, ADest^, SizeOf(Integer));
    sbfUnknown:  Move(ASource^, ADest^, ASize);
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
end;


procedure ReadValue(AStream: TsbStream; Dest: Pointer; AType: TsbFieldType; ASize: Integer = 0); overload;
begin
  case AType of
    sbfString:   AStream.Read(Dest, ASize + 1);
    sbfInteger:  AStream.Read(Dest, SizeOf(Integer));
    sbfAutoInc:  AStream.Read(Dest, SizeOf(Cardinal));    
    sbfByte:     AStream.Read(Dest, SizeOf(Byte));
    sbfBoolean:  AStream.Read(Dest, SizeOf(ByteBool));
    sbfDateTime: AStream.Read(Dest, SizeOf(TDateTime));
    sbfFloat:    AStream.Read(Dest, SizeOf(Double));
    sbfCurrency: AStream.Read(Dest, SizeOf(Currency));
    sbfBlob:     AStream.Read(Dest, SizeOf(Integer));
    sbfUnknown:  AStream.Read(Dest, ASize);
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
end;


procedure ReadValue(Source: Pointer; Dest: Pointer; AType: TsbFieldType; ASize: Integer = 0); overload;
begin
  case AType of
    sbfString:   Move(Source^, Dest^, ASize + 1);
    sbfInteger:  Move(Source^, Dest^, SizeOf(Integer));
    sbfAutoInc:  Move(Source^, Dest^, SizeOf(Cardinal));    
    sbfByte:     Move(Source^, Dest^, SizeOf(Byte));
    sbfBoolean:  Move(Source^, Dest^, SizeOf(ByteBool));
    sbfDateTime: Move(Source^, Dest^, SizeOf(TDateTime));
    sbfFloat:    Move(Source^, Dest^, SizeOf(Double));
    sbfCurrency: Move(Source^, Dest^, SizeOf(Currency));
    sbfBlob:     Move(Source^, Dest^, SizeOf(Integer));
    sbfUnknown:  Move(Source^, Dest^, ASize);
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
end;


function CheckNull(AFldIndex: Word; ANullInfo: PsbNullInfo): Boolean;
var
  i: word;
  n: byte;
begin
  AFldIndex := AFldIndex + 8;
  i := AFldIndex div 8;
  n := $80;
  n := n shr (AFldIndex mod 8);
  Result := Boolean(ANullInfo[i] and n);
end;


procedure SetNull(AFldIndex: Word; ANullInfo: PsbNullInfo; ANull: Boolean);
var
  i: word;
  n: byte;
begin
  AFldIndex := AFldIndex + 8;
  i := AFldIndex div 8;
  n := $80;
  n := n shr (AFldIndex mod 8);
  if ANull then
    ANullInfo[i] := ANullInfo[i] or n
  else
    ANullInfo[i] := ANullInfo[i] and not n;
end;



function CalcFltCond(ACondition: TsbConditionNode): Boolean;
var
  i, n: Integer;
  R: Boolean;
begin
  if ACondition.OperType = sbnAND then
    Result := True
  else
    Result := False;

  n := ACondition.ItemCount - 1;
  for i := 0 to n do
  begin
    if ACondition.Items[i] is TsbConditionItem then
      R := VM.CalcCondition(TsbConditionItem(ACondition.Items[i]))
    else
      R := CalcFltCond(TsbConditionNode(ACondition.Items[i]));

    if ACondition.OperType = sbnAND then
    begin
      Result := Result AND R;
      if not Result then
        Break;
    end
    else
    begin
      Result := Result OR R;
      if Result then
        Break;
    end;
  end;
end;


procedure sbCreateTable(AFileName: string; AFields: TsbFields; ABLOBTable: Boolean = False);
var
 FStream: TsbStream;
 i, m: Integer;
 fl: Boolean;
 S: ShortString;
 N: Integer;
 B: Byte;
 BlbFields: TsbFields;
begin
 if AFields.Count > sbMaxFieldsNumber then
   ShowSBError(sbeQuantityFieldsBig, []);

 if ABLOBTable then
   AFileName := ChangeFileExt(AFileName, sbBLOBFileExt)
 else
   AFileName := ChangeFileExt(AFileName, sbTableExt);
 fl := False;
 DeleteFile(AFileName);
 FStream := TsbFileStream.Create(AFileName, [sbsCreate, sbsRead, sbsWrite]);
 try
   FStream.Position := 0;
   S := sbSignature1_1;
   WriteValue(FStream, @S, sbfString, sbFieldNameSize);
   N := AFields.Count;
   WriteValue(FStream, @N, sbfInteger);

   m := AFields.Count - 1;
   for i := 0 to m do
   begin
     S := AFields[i].FieldName;
     WriteValue(FStream, @S, sbfString, sbFieldNameSize);
     B := Ord(AFields[i].FieldType);
     WriteValue(FStream, @B, sbfByte);
     B := AFields[i].Size;
     WriteValue(FStream, @B, sbfByte);
     if AFields[i].FieldType = sbfBLOB then
     begin
       BlbFields := TsbFields.Create;
       try
         s := ExtractFilePath(AFileName) + ChangeFileExt(ExtractFileName(AFileName), '') + '_' + AFields[i].FieldName +sbBlobFileExt;
         BlbFields.CreateField('FLAG', sbfByte);
         BlbFields.CreateField('DATA', sbfString, sbBLOBClasterSize);
         sbCreateTable(s, BlbFields, True);
       finally
         BlbFields.Free;
       end;
     end;
   end;
   N := 0;
   WriteValue(FStream, @N, sbfInteger);
   N := FStream.Size + SizeOf(Integer) * 4;
   WriteValue(FStream, @N, sbfInteger);
   WriteValue(FStream, @N, sbfInteger);
   N := 0;
   WriteValue(FStream, @N, sbfInteger);
   WriteValue(FStream, @N, sbfAutoInc);
   fl := True;
 finally
   FStream.Free;
   if not fl then
   begin
     DeleteFile(AFileName);
     m := AFields.Count - 1;
     for i := 0 to m do
       if AFields[i].FieldType = sbfBLOB then
       begin
         s := ExtractFilePath(AFileName) + ChangeFileExt(ExtractFileName(AFileName), '') + '_' + AFields[i].FieldName +sbBlobFileExt;
         DeleteFile(s);
       end;
   end;
 end;
end;


function sbOpenTable(const AFileName: string; ATableCursor: TsbTableCursor = nil; AMode: TsbAccessModeType = sbmReadWrite; ATemporary: Boolean = False): TsbTableCursor;
begin
  if Assigned(ATableCursor) then
    Result := ATableCursor
  else
    Result := TsbTableCursor.Create;

  try
    Result.FAccessMode := AMode;
    Result.FTemporary := ATemporary;
    Result.Open(AFileName);
  except
    if not Assigned(ATableCursor) then
      Result.Free;
    raise;
  end;
end;


function sbOpenTable(const AFileName: string; AMode: TsbAccessModeType = sbmReadWrite; ATemporary: Boolean = False): IrwSBTable;
begin
  Result := TsbTableCursor.Create;
  sbOpenTable(AFileName, TsbTableCursor(Result.RealObject), AMode, ATemporary);
end;


function sbOpenQuery(const ASQL: string; AParams: TsbParams = nil; AResultTable: TsbTableCursor = nil): TsbResultRec;
var
  P: TsbSQLParser;
  t: Cardinal;
begin
  t := GetTickCount;
  P := TsbSQLParser.Create;
  try
    P.SQL := ASQL;
    if Assigned(AParams) then
      P.Params.Assign(AParams);

    P.Open(AResultTable);
    Result.Cursor := P.GetResultCursor;
    Result.Plan := P.Plan;
    Result.Cost := P.CostEvaluation;
    Result.Time := GetTickCount - t; 
  finally
    P.Free;
  end;
end;


procedure sbExecQuery(const ASQL: string; AParams: TsbParams = nil);
var
  P: TsbSQLParser;
begin
  P := TsbSQLParser.Create;
  try
    P.SQL := ASQL;
    if Assigned(AParams) then
      P.Params.Assign(AParams);

    P.Execute;
  finally
    P.Free;
  end;
end;


procedure sbClose(ATable: TsbTableCursor);
begin
  ATable.Free;
end;


procedure sbDropTable(ATable: TsbTableCursor);
begin
  DeleteSysIndInfo(ATable);
  ATable.FTemporary := True;
  sbClose(ATable);
end;


procedure sbDropTable(const ATableName: string); overload;
var
  lTable: TsbTableCursor;
begin
  lTable := sbOpenTable(ATableName, nil, sbmRead, True);
  sbDropTable(lTable);
end;


procedure sbPackTable(const AFileName: string);
var
  S: TsbTableCursor;
  D: TsbFileStream;
  B: Pointer;
  p: Int64;
  f: string;
begin
  S := sbOpenTable(AFileName, nil, sbmRead);
  f := ChangeFileExt(AFileName, '.tmp');
  D := TsbFileStream.Create(f, [sbsCreate, sbsWrite]);
  if S.FRecordSize > S.FHeaderSize then
    B := AllocMem(S.FRecordSize)
  else
    B := AllocMem(S.FHeaderSize);

  try
    S.FStream.Position := 0;
    S.FStream.Read(B, S.FHeaderSize);
    D.Write(B, S.FHeaderSize);
    p := 0;
    S.DisableNotification;
    while not S.Eof do
    begin
      S.FStream.Position := S.FCurrRecordPos;
      S.FStream.Read(B, S.FRecordSize);
      if p > 0 then
        Dec(p, S.FRecordSize);
      CopyMemory(B, @p, SizeOf(p));
      if S.FCurrRecordPos = S.FBeginLastRecordPos then
        p := 0
      else
        p := D.Size + S.FRecordSize;
      CopyMemory(Pointer(Cardinal(B) + SizeOf(Integer)), @p, SizeOf(p));
      D.Write(B, S.FRecordSize);
      S.Next;
    end;
    S.EnableNotification;
    D.Position := S.FUpdHeaderInfoPos + SizeOf(Integer);
    WriteValue(D, @S.FHeaderSize, sbfInteger);
    p := D.Size - S.FRecordSize;
    WriteValue(D, @p, sbfInteger);
    p := 0;
    WriteValue(D, @p, sbfInteger);
  finally
    FreeMem(B);
    sbClose(S);
    D.Free;
  end;
  DeleteFile(AFileName);
  RenameFile(f, AFileName);
end;




{ TsbField }

constructor TsbField.Create(AOwner: TsbFields; AName: String; AType: TsbFieldType; ASize: Integer = 0);
var
  s: string;
begin
  New(FPValRec);
  FOwner := AOwner;
  FPValRec^.FieldName := AnsiUpperCase(AName);
  FPValRec^.FieldType := AType;
  FBLOBTable := nil;
  case AType of
    sbfString:   FPValRec^.FieldSize := ASize;
    sbfInteger:  FPValRec^.FieldSize := SizeOf(Integer);
    sbfByte:     FPValRec^.FieldSize := SizeOf(Byte);
    sbfDateTime: FPValRec^.FieldSize := SizeOf(TDateTime);
    sbfFloat:    FPValRec^.FieldSize := SizeOf(Double);
    sbfCurrency: FPValRec^.FieldSize := SizeOf(Currency);
    sbfBoolean:  FPValRec^.FieldSize := SizeOf(ByteBool);
    sbfBlob:     begin
                   FPValRec^.FieldSize := SizeOf(Integer);
                   if Assigned(FOwner.FCursor) and (FOwner.FCursor.FInternalTag = 0) then
                   begin
                     s := ExtractFilePath(FOwner.FCursor.TableName) +
                       ChangeFileExt(ExtractFileName(FOwner.FCursor.TableName), '') + '_' + AName +sbBlobFileExt;
                     FBLOBTable := TsbTableCursor.Create;
                     FBLOBTable.FAccessMode := FOwner.FCursor.FAccessMode;
                     FBLOBTable.Open(s, True);
                   end;
                 end;

    sbfAutoInc:  FPValRec^.FieldSize := SizeOf(Cardinal);
  end;
  FInitialized := False;
  FModified := False;
end;


destructor TsbField.Destroy;
begin
  FreeBlobTable;
  FreeRecRes;
  inherited;
end;


procedure TsbField.Clear;
begin
  CheckMode;
  InternalClear;
  FInitialized := True;
  FModified := True;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  NotifySetFieldValue;
end;


procedure TsbField.FreeRecRes;
begin
  Dispose(FPValRec);
end;

function TsbField.GetAsByte: Byte;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfBoolean:  Result := Byte(Ord(FPValRec^.BooleanVal));
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;

function TsbField.GetAsInteger: Integer;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfString:   Result := StrToInt(FPValRec^.StringVal);
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfFloat:    Result := Trunc(FPValRec^.FloatVal);
      sbfBoolean:  Result := Ord(FPValRec^.BooleanVal);
      sbfCurrency: Result := Trunc(FPValRec^.CurrencyVal);
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;

function TsbField.GetAsString: String;
begin
  if IsNull then
    Result := ''

  else
    case FPValRec^.FieldType of
      sbfString:   Result := String(FPValRec^.StringVal);
      sbfInteger:  Result := IntToStr(FPValRec^.IntegerVal);
      sbfByte:     Result := IntToStr(FPValRec^.ByteVal);
      sbfDateTime: Result := DateToStr(FPValRec^.DateTimeVal);
      sbfFloat:    Result := FloatToStr(FPValRec^.FloatVal);
      sbfCurrency: Result := FloatToStr(FPValRec^.CurrencyVal);
      sbfBoolean:  Result := IntToStr(Ord(FPValRec^.BooleanVal));
      sbfBlob:     Result := FBLOBData;
      sbfAutoInc:  Result := IntToStr(FPValRec^.AutoIncVal);
    else
      Result := '';
    end;
end;

function TsbField.GetFieldName: string;
begin
  Result := FPValRec^.FieldName;
end;


function TsbField.GetFieldType: TsbFieldType;
begin
  Result := FPValRec^.FieldType;
end;


function TsbField.GetSize: Byte;
begin
  Result := FPValRec^.FieldSize;
end;


function TsbField.GetValue: Variant;
begin
  SyncContent;
  if FPValRec^.NullValue then
    TVarData(Result).VType := varNull
  else
    case FPValRec^.FieldType of
      sbfString:   Result := FPValRec^.StringVal;
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfDateTime: Result := FPValRec^.DateTimeVal;
      sbfFloat:    Result := FPValRec^.FloatVal;
      sbfCurrency: Result := FPValRec^.CurrencyVal;
      sbfBoolean:  Result := FPValRec^.BooleanVal;
      sbfBLOB:     Result := FBLOBData;
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;
    end;
end;

procedure TsbField.SetAsByte(const AValue: Byte);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfByte:    FPValRec^.ByteVal := AValue;
    sbfString:  FPValRec^.StringVal := Copy(IntToStr(AValue), 1, FPValRec^.FieldSize);
    sbfInteger: FPValRec^.IntegerVal := AValue;
    sbfBoolean: FPValRec^.BooleanVal := ByteBool(AValue);
    sbfBLOB:    SetBLOBData(IntToStr(AValue));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;  
end;

procedure TsbField.SetAsInteger(const AValue: Integer);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfInteger:  FPValRec^.IntegerVal := AValue;
    sbfString:   FPValRec^.StringVal := Copy(IntToStr(AValue), 1, FPValRec^.FieldSize);
    sbfFloat:    FPValRec^.FloatVal := AValue;
    sbfCurrency: FPValRec^.CurrencyVal := AValue;
    sbfBoolean:  FPValRec^.BooleanVal := ByteBool(AValue);
    sbfBLOB:     SetBLOBData(IntToStr(AValue));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;

procedure TsbField.SetAsString(const AValue: String);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfString:   FPValRec^.StringVal := Copy(AValue, 1, FPValRec^.FieldSize);
    sbfDateTime: FPValRec^.DateTimeVal := StrToDate(AValue);
    sbfInteger:  FPValRec^.IntegerVal := StrToInt(AValue);
    sbfByte:     FPValRec^.ByteVal := StrToInt(AValue);
    sbfFloat:    FPValRec^.FloatVal := StrToFloat(AValue);
    sbfCurrency: FPValRec^.CurrencyVal := StrToFloat(AValue);
    sbfBoolean:  FPValRec^.BooleanVal := ByteBool(StrToInt(AValue));
    sbfBLOB:     SetBLOBData(AValue);
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;

procedure TsbField.SetValue(const AValue: Variant);
begin
  if VarIsNull(AValue) then
  begin
    Clear;
    Exit;
  end
  else
  begin
    CheckMode;
    case FPValRec^.FieldType of
      sbfString:   FPValRec^.StringVal := Copy(AValue, 1, FPValRec^.FieldSize);
      sbfInteger:  FPValRec^.IntegerVal := AValue;
      sbfByte:     FPValRec^.ByteVal := AValue;
      sbfDateTime: FPValRec^.DateTimeVal := AValue;
      sbfFloat:    FPValRec^.FloatVal := AValue;
      sbfCurrency: FPValRec^.CurrencyVal := AValue;
      sbfBoolean:  FPValRec^.BooleanVal := TVarData(AValue).VBoolean;
      sbfBLOB:     SetBLOBData(AValue);
    end;
    FPValRec^.NullValue := False;
  end;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;


procedure TsbField.SyncContent;
begin
  if not FInitialized then
    FOwner.ReadFieldFromStream(Self);
end;


function TsbField.GetAsDateTime: TDateTime;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfDateTime: Result := FPValRec^.DateTimeVal;
      sbfString:   Result := StrToDate(FPValRec^.StringVal);
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfFloat:    Result := FPValRec^.FloatVal;
      sbfCurrency: Result := FPValRec^.CurrencyVal;
      sbfBoolean:  Result := Ord(FPValRec^.BooleanVal);
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;


procedure TsbField.SetAsDateTime(const AValue: TDateTime);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfDateTime: FPValRec^.DateTimeVal := AValue;
    sbfString:   FPValRec^.StringVal := Copy(DateToStr(AValue), 1, FPValRec^.FieldSize);
    sbfFloat:    FPValRec^.FloatVal := AValue;
    sbfCurrency: FPValRec^.CurrencyVal := AValue;
    sbfBoolean:  FPValRec^.BooleanVal := ByteBool(Trunc(AValue));
    sbfBLOB:     SetBLOBData(DateToStr(AValue));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;



function TsbField.GetAsCurrency: Currency;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfCurrency: Result := FPValRec^.CurrencyVal;
      sbfFloat:    Result := FPValRec^.FloatVal;
      sbfString:   Result := StrToFloat(FPValRec^.StringVal);
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfDateTime: Result := FPValRec^.DateTimeVal;
      sbfBoolean:  Result := Ord(FPValRec^.BooleanVal);
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;      
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;


function TsbField.GetAsFloat: Double;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfFloat:    Result := FPValRec^.FloatVal;
      sbfCurrency: Result := FPValRec^.CurrencyVal;
      sbfString:   Result := StrToFloat(FPValRec^.StringVal);
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfDateTime: Result := FPValRec^.DateTimeVal;
      sbfBoolean:  Result := Ord(FPValRec^.BooleanVal);
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;      
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;


procedure TsbField.SetAsCurrency(const AValue: Currency);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfCurrency: FPValRec^.CurrencyVal := AValue;
    sbfFloat:    FPValRec^.FloatVal := AValue;
    sbfDateTime: FPValRec^.DateTimeVal := AValue;
    sbfString:   FPValRec^.StringVal := Copy(FloatToStr(AValue), 1, FPValRec^.FieldSize);
    sbfBoolean:  FPValRec^.BooleanVal := ByteBool(Trunc(AValue));
    sbfBLOB:     SetBLOBData(FloatToStr(AValue));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;  
end;

procedure TsbField.SetAsFloat(const AValue: Double);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfFloat:    FPValRec^.FloatVal := AValue;
    sbfCurrency: FPValRec^.CurrencyVal := AValue;
    sbfDateTime: FPValRec^.DateTimeVal := AValue;
    sbfString:   FPValRec^.StringVal := Copy(FloatToStr(AValue), 1, FPValRec^.FieldSize);
    sbfBoolean:  FPValRec^.BooleanVal := ByteBool(Trunc(AValue));
    sbfBLOB:     SetBLOBData(FloatToStr(AValue));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;  
end;

function TsbField.IsNull: Boolean;
begin
  SyncContent;
  Result := FPValRec^.NullValue;
end;

function TsbField.Cursor: TsbTableCursor;
begin
  Result := FOwner.FCursor;
end;


procedure TsbField.FreeBlobTable;
begin
  if Assigned(FBLOBTable) then
  begin
    FBLOBTable.FTemporary := Cursor.FTemporary;
    FreeAndNil(FBLOBTable);
  end;
end;


procedure TsbField.SetBLOBData(const AValue: String);
begin
  SyncContent;
  FBLOBData := AValue;
end;


procedure TsbField.CheckMode;
begin
  if FOwner.FCursor.FStatus < sbcAppend then
    ShowSBError(sbeTableInNotEditMode, []);
end;


procedure TsbField.InternalClear;
begin
  FPValRec^.NullValue := True;
  FPValRec^.IntegerVal := 0;
  FPValRec^.ByteVal := 0;
  FPValRec^.StringVal := '';
  FPValRec^.BlobVal := 0;
  FBLOBData := '';
end;


function TsbField.GetAsBoolean: ByteBool;
begin
  if IsNull then
  begin
    Result := False;
    ShowSBError(sbeValueTypeIsWrong, []);
  end
  else
    case FPValRec^.FieldType of
      sbfBoolean:  Result := FPValRec^.BooleanVal;
      sbfCurrency: Result := FPValRec^.CurrencyVal <> 0;
      sbfFloat:    Result := FPValRec^.FloatVal <> 0;
      sbfInteger:  Result := FPValRec^.IntegerVal <> 0;
      sbfDateTime: Result := FPValRec^.DateTimeVal <> 0;
      sbfByte:     Result := FPValRec^.ByteVal <> 0;
      sbfAutoInc:  Result := FPValRec^.AutoIncVal <> 0;      
    else
      Result := False;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;


procedure TsbField.SetAsBoolean(const AValue: ByteBool);
begin
  CheckMode;
  case FPValRec^.FieldType of
    sbfBoolean:  FPValRec^.BooleanVal := AValue;
    sbfCurrency: FPValRec^.CurrencyVal := Ord(AValue);
    sbfFloat:    FPValRec^.FloatVal := Ord(AValue);
    sbfDateTime: FPValRec^.DateTimeVal := Ord(AValue);
    sbfString:   FPValRec^.StringVal := Copy(IntToStr(Ord(AValue)), 1, FPValRec^.FieldSize);
    sbfBLOB:     SetBLOBData(IntToStr(Ord(AValue)));
  else
    ShowSBError(sbeValueTypeIsWrong, []);
  end;
  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;


procedure TsbField.ReadFieldFromBuffer(ABuffer: Pointer);
var
  P: Pointer;
  lNullInfo: TsbNullInfo;
begin
  P := Pointer(Cardinal(ABuffer) + SizeOf(Integer)*2);
  System.Move(P^, lNullInfo, FOwner.FCursor.FNullInfoSize);

  with FPValRec^ do
  begin
    NullValue := CheckNull(FIndex, @lNullInfo);
    if not NullValue then
    begin
      P := Pointer(Cardinal(ABuffer) + RecOffset);
      case FieldType of
        sbfString:   ReadValue(P, @StringVal,FieldType, FieldSize);
        sbfInteger:  ReadValue(P, @IntegerVal, FieldType, FieldSize);
        sbfByte:     ReadValue(P, @ByteVal, FieldType, FieldSize);
        sbfBoolean:  ReadValue(P, @BooleanVal, FieldType, FieldSize);
        sbfDateTime: ReadValue(P, @DateTimeVal, FieldType, FieldSize);
        sbfFloat:    ReadValue(P, @FloatVal, FieldType, FieldSize);
        sbfCurrency: ReadValue(P, @CurrencyVal, FieldType, FieldSize);
        sbfAutoInc:  ReadValue(P, @AutoIncVal, FieldType, FieldSize);

        sbfBlob:     begin
                       FBLOBData := '';
                       if Assigned(FBLOBTable) then
                       begin
                         ReadValue(P, @BlobVal, FieldType, FieldSize);
                         FBLOBTable.InternalRecordPos := BlobVal;
                         while not FBLOBTable.Eof do
                         begin
                           FBLOBData := FBLOBData + FBLOBTable.Fields[1].AsString;
                           if TsbBLOBClasterType(FBLOBTable.Fields[0].AsByte) = sbbEndBLOBClaster then
                             break;
                           FBLOBTable.Next;
                         end;
                       end;
                     end;
      end;
    end

    else
      InternalClear;
  end;
end;


procedure TsbField.NotifySetFieldValue;
begin
  FOwner.FCursor.NotifyStateChanged;
end;

function TsbField.GetDataType: TrwDDType;
begin
  Result := SBTypetoDDType(FieldType);
end;

function TsbField.GetIndex: Integer;
begin
  Result := Index;
end;

procedure TsbField.LoadBlobFromStream(const AStream: TStream; ASize: Integer = 0);
begin
  CheckMode;
  if FPValRec^.FieldType <> sbfBLOB then
    ShowSBError(sbeValueTypeIsWrong, []);

  SyncContent;
  if ASize = 0 then
    ASize := AStream.Size - AStream.Position;
  SetLength(FBLOBData, ASize);
  AStream.ReadBuffer(FBLOBData[1], ASize);

  FPValRec^.NullValue := False;
  FOwner.AddNullInfo(FIndex, FPValRec^.NullValue);
  FInitialized := True;
  FModified := True;
  NotifySetFieldValue;
end;

procedure TsbField.SaveBlobToStream(const AStream: TStream);
begin
  if IsNull then
    Exit

  else
  begin
    if FPValRec^.FieldType <> sbfBlob then
      ShowSBError(sbeValueTypeIsWrong, []);
    if Length(FBLOBData) > 0 then
      AStream.WriteBuffer(FBLOBData[1], Length(FBLOBData));
  end;
end;

procedure TsbField.CleanupBlobData;
var
  BlobRef: Integer;
  SavedPos: Cardinal;
begin
  // Deletes blob clusters. Internal use only!

  SavedPos := FOwner.FCursor.FStream.Position;
  try
    FOwner.FCursor.FStream.Position := FOwner.FCursor.FCurrRecordPos + FPValRec^.RecOffset;
    ReadValue(FOwner.FCursor.FStream, @BlobRef, sbfBLOB, 0);

    if BlobRef <> 0 then
    begin
      FBLOBTable.InternalRecordPos := BlobRef;
      if not FBLOBTable.OuterPosition then
        while True do
          if TsbBLOBClasterType(FBLOBTable.Fields[0].AsByte) = sbbBLOBClaster then
            FBLOBTable.Delete
          else
          begin
            FBLOBTable.Delete;
            break;
          end;
    end;

  finally
    FOwner.FCursor.FStream.Position := SavedPos;
  end;
end;

function TsbField.GetAsCardinal: Cardinal;
begin
  if IsNull then
    Result := 0

  else
    case FPValRec^.FieldType of
      sbfInteger:  Result := FPValRec^.IntegerVal;
      sbfString:   Result := StrToInt(FPValRec^.StringVal);
      sbfByte:     Result := FPValRec^.ByteVal;
      sbfFloat:    Result := Trunc(FPValRec^.FloatVal);
      sbfBoolean:  Result := Ord(FPValRec^.BooleanVal);
      sbfCurrency: Result := Trunc(FPValRec^.CurrencyVal);
      sbfAutoInc:  Result := FPValRec^.AutoIncVal;
    else
      Result := 0;
      ShowSBError(sbeValueTypeIsWrong, []);
    end;
end;


{ TsbFields }

function TsbFields.CreateField(AName: String; AType: TsbFieldType; ASize: Integer): TsbField;
begin
  Result :=  TsbField.Create(Self, AName, AType, ASize);
  Result.FIndex := Add(Result);

  if AType = sbfAutoInc then
    if FAutoincFieldIndex <> -1 then
      ShowSBError('Only one auto increment field is allowed ', [])
    else
      FAutoincFieldIndex := Result.FIndex;
end;

procedure TsbFields.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Free;
  FAutoincFieldIndex := -1;

  inherited;
end;


function TsbFields.GetItems(Index: Integer): TsbField;
begin
  Result := TsbField(inherited Items[Index]);
end;


procedure TsbFields.ClearValues;
var
  i, n: Integer;
begin
  FCursor.DisableNotification;
  try
    FNullInfoInitialized := True;
    n := Count - 1;
    for i := 0 to n do
      Items[i].Clear;
  finally
    FCursor.EnableNotification;
  end;
end;


procedure TsbFields.WriteFieldsToStream;
var
  i, n: Integer;
  F: TsbField;

  procedure WriteBlobData;
  var
    n, p: Integer;
    PrevType: TsbBLOBClasterType;
    fl: Boolean;

    procedure DeleteClasters;
    begin
      while True do
        if TsbBLOBClasterType(F.FBLOBTable.Fields[0].AsByte) = sbbBLOBClaster then
          F.FBLOBTable.Delete
        else
        begin
          F.FBLOBTable.Delete;
          break;
        end;
    end;

  begin
    if F.IsNull or (Length(F.FBLOBData) = 0) then
    begin
      if FCursor.State = sbcEdit then
        F.CleanupBlobData;
      F.FPValRec^.BlobVal := 0;
      Exit;
    end;

    F.FBLOBTable.InternalRecordPos := F.FPValRec^.BlobVal;
    n := Length(F.FBLOBData);
    p := 1;
    if F.FPValRec^.BlobVal = 0 then
      PrevType := sbbEndBLOBClaster
    else
      PrevType := sbbBLOBClaster;

    fl := False;
    while p <= n do
    begin
      if PrevType = sbbBLOBClaster then
      begin
        PrevType := TsbBLOBClasterType(F.FBLOBTable.Fields[0].AsByte);
        F.FBLOBTable.Edit;
      end
      else
      begin
        if F.FBLOBTable.OuterPosition then
        begin
          F.FBLOBTable.InternalRecordPos := F.FBLOBTable.InternalRecordPos; // switch back from Outer state. Very important!
          F.FBLOBTable.Append;
        end
        else
          if F.FBLOBTable.EOF then
            F.FBLOBTable.Append
          else
            F.FBLOBTable.Insert;
      end;

      F.FBLOBTable.Fields[1].AsString := Copy(F.FBLOBData, p, sbBLOBClasterSize);
      Inc(p, sbBLOBClasterSize);
      if p <= n then
        F.FBLOBTable.Fields[0].AsByte := Ord(sbbBLOBClaster)
      else
      begin
        F.FBLOBTable.Fields[0].AsByte := Ord(sbbEndBLOBClaster);
        fl := True;
      end;
      F.FBLOBTable.Post;
      if F.FPValRec^.BlobVal = 0 then
        F.FPValRec^.BlobVal := F.FBLOBTable.InternalRecordPos;

      F.FBLOBTable.Next;

      if fl then
        break;
    end;

    if PrevType = sbbBLOBClaster then
      DeleteClasters;
  end;

begin
  FCursor.FStream.Position := FCursor.FCurrRecordPos + SizeOf(Integer)*2;
  WriteValue(FCursor.FStream, @FNullInfo, sbfUnknown, FCursor.FNullInfoSize);
  n := Count - 1;
  for i := 0 to n do
  begin
    F := Items[i];
    if not F.Modified then
      Continue;

    with F.FPValRec^ do
    begin
      FCursor.FStream.Position := FCursor.FCurrRecordPos + RecOffset;
      case FieldType of
        sbfString:   WriteValue(FCursor.FStream, @StringVal, FieldType, FieldSize);
        sbfInteger:  WriteValue(FCursor.FStream, @IntegerVal, FieldType);
        sbfByte:     WriteValue(FCursor.FStream, @ByteVal, FieldType);
        sbfBoolean:  WriteValue(FCursor.FStream, @BooleanVal, FieldType);
        sbfFloat:    WriteValue(FCursor.FStream, @FloatVal, FieldType);
        sbfDateTime: WriteValue(FCursor.FStream, @DateTimeVal, FieldType);
        sbfCurrency: WriteValue(FCursor.FStream, @CurrencyVal, FieldType);
        sbfBLOB:     begin
                       WriteBlobData;
                       WriteValue(FCursor.FStream, @BlobVal, FieldType);
                     end;
        sbfAutoInc:  WriteValue(FCursor.FStream, @AutoIncVal, FieldType);                     
      end;
    end;
  end;
end;


procedure TsbFields.SetInitializedFlag(AValue: Boolean);
var
  i, n: Integer;
begin
  n := Count - 1;
  for i := 0 to n do
  begin
    Items[i].FInitialized := AValue;
    Items[i].FModified := False;
  end;
  FNullInfoInitialized := AValue;
end;

procedure TsbFields.ReadFieldFromStream(AField: TsbField);
begin
  if FCursor.FRecNo > 0 then
  begin
    InitNullInfo;

    with AField.FPValRec^ do
    begin
      NullValue := FieldValueIsNull(AField.FIndex);
      if not NullValue then
      begin
        FCursor.FStream.Position := FCursor.FCurrRecordPos + RecOffset;
        case FieldType of
          sbfString:   ReadValue(FCursor.FStream, @StringVal, FieldType, FieldSize);
          sbfInteger:  ReadValue(FCursor.FStream, @IntegerVal, FieldType, FieldSize);
          sbfByte:     ReadValue(FCursor.FStream, @ByteVal, FieldType, FieldSize);
          sbfBoolean:  ReadValue(FCursor.FStream, @BooleanVal, FieldType, FieldSize);
          sbfDateTime: ReadValue(FCursor.FStream, @DateTimeVal, FieldType, FieldSize);
          sbfFloat:    ReadValue(FCursor.FStream, @FloatVal, FieldType, FieldSize);
          sbfCurrency: ReadValue(FCursor.FStream, @CurrencyVal, FieldType, FieldSize);
          sbfAutoInc:  ReadValue(FCursor.FStream, @AutoIncVal, FieldType, FieldSize);

          sbfBlob:     begin
                         AField.FBLOBData := '';
                         ReadValue(FCursor.FStream, @BlobVal, FieldType, FieldSize);
                         AField.FBLOBTable.InternalRecordPos := BlobVal;
                         while not AField.FBLOBTable.Eof do
                         begin
                           AField.FBLOBData := AField.FBLOBData + AField.FBLOBTable.Fields[1].AsString;
                           if TsbBLOBClasterType(AField.FBLOBTable.Fields[0].AsByte) = sbbEndBLOBClaster then
                             break;
                           AField.FBLOBTable.Next;
                         end;
                       end;
        end;
      end
      else
        AField.InternalClear;
    end;
  end
  else
    AField.InternalClear;

  AField.FModified := False;
  AField.FInitialized := True;
end;


function TsbFields.FieldValueIsNull(AFldIndex: Word): Boolean;
begin
  Result := CheckNull(AFldIndex, @FNullInfo);
end;


procedure TsbFields.AddNullInfo(AFldIndex: Word; ANull: Boolean);
begin
  InitNullInfo;
  SetNull(AFldIndex, @FNullInfo, ANull);
end;


function TsbFields.FindField(AName: string): TsbField;
var
  i, n: Integer;
begin
  Result := nil;
  AName := AnsiUpperCase(AName);
  n := Count - 1;
  for i := 0 to n do
    if CompareStr(AName, Items[i].GetFieldName) = 0 then
    begin
      Result := Items[i];
      break;
    end;
end;

function TsbFields.FieldByName(AName: string): TsbField;
begin
  Result := FindField(AName);
  if not Assigned(Result) then
    ShowSBError(SFieldNotFound, [AName]);
end;


procedure TsbFields.Assign(ASource: TsbFields);
var
  i, n: Integer;
  F: TsbField;
begin
  Clear;
  n := ASource.Count -  1;
  for i := 0 to n do
  begin
    F := CreateField(ASource[i].FieldName, ASource[i].FieldType, ASource[i].Size);
    F.FPValRec.RecOffset := ASource[i].FPValRec.RecOffset;
  end;
end;


procedure TsbFields.InitNullInfo;
begin
  if not FNullInfoInitialized then
  begin
    FCursor.FStream.Position := FCursor.FCurrRecordPos + SizeOf(Integer)*2;
    ReadValue(FCursor.FStream, @FNullInfo, sbfUnknown, FCursor.FNullInfoSize);
    FNullInfoInitialized := True;
  end;
end;




procedure TsbFields.DecAutoIncFields;
begin
  if FAutoincFieldIndex <> -1 then
    Dec(FCursor.FLastAutoIncValue);
end;

procedure TsbFields.IncAutoIncFields;
begin
  if FAutoincFieldIndex <> -1 then
  begin
    Inc(FCursor.FLastAutoIncValue);
    Items[FAutoincFieldIndex].FPValRec.AutoIncVal := FCursor.FLastAutoIncValue;
  end;
end;

constructor TsbFields.Create;
begin
  FAutoincFieldIndex := -1;
end;

{ TsbTableCursor }

constructor TsbTableCursor.Create;
begin
  inherited Create;
  FStatus := sbcInactive;
  FFields := TsbFields.Create;
  FFields.FCursor := Self;
  FStream := nil;
  FActiveIndex := nil;
  FFiltered := False;
  FFilter := '';
  FFilterCond := nil;
  FFilterSQLObject := nil;
  FTemporary := False;
  FIndexList := TStringList.Create;
  FIndexEnabled := False;

  FNotifyCouter := 0;
  FInternalTag := 0;
  EnableNotification;
end;


destructor TsbTableCursor.Destroy;
begin
  try
    try
      Close;
    finally
      try
        FIndexList.Free;
      finally
        try
          DestroyFilter;
        finally
          FFields.Free;
        end;
      end;
    end;
  finally
    FreeAndNil(FStream);
    inherited;
  end;
end;


procedure TsbTableCursor.Append;
begin
  if FFiltered then
  begin
    FCurrRecordPos := FBeginLastRecordPos;
    ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
  end

  else if FNextRecordPos <> 0 then
    Last;

  FStatus := sbcAppend;
  FFields.ClearValues;
  FFields.IncAutoincFields;
  NotifyStateChanged;
end;


procedure TsbTableCursor.Insert;
begin
  if IsEmptyCursor then
    Append
  else
  begin
    FStatus := sbcInsert;
    FFields.ClearValues;
    FFields.IncAutoincFields;
    NotifyStateChanged;
  end;
end;


procedure TsbTableCursor.Delete;
var
  DelRec: Integer;

  procedure CleanupBlobs;
  var
    i: Integer;
    Fld: TsbField;
  begin
    for i := 0 to Fields.Count - 1 do
    begin
      Fld := Fields[i];
      if Fld.FieldType = sbfBLOB then
        Fld.CleanupBlobData;
    end;
  end;

begin
  if IsEmptyCursor then
    Exit;

  CleanupBlobs;

  if FFiltered then
    FFiltRec.Delete(FRecNo - 1);

  DelRec := FCurrRecordPos;
  WriteRecordHeaderNextPos(FBeginFirstDelRecordPos);
  if FBeginFirstDelRecordPos <> 0 then
  begin
    FCurrRecordPos := FBeginFirstDelRecordPos;
    WriteRecordHeaderPrevPos(DelRec);
    FCurrRecordPos := DelRec;
  end;
  FBeginFirstDelRecordPos := DelRec;

  FModified := True;

  if FCurrRecordPos = FBeginFirstRecordPos then
  begin
    if FNextRecordPos <> 0 then
    begin
      FCurrRecordPos := FNextRecordPos;
      WriteRecordHeaderPrevPos(0);
      FBeginFirstRecordPos := FNextRecordPos;
    end;
    FBOF := False;
    Dec(FRecordCount);
    First;
  end
  else if FCurrRecordPos = FBeginLastRecordPos then
  begin
    if FPrevRecordPos <> 0 then
    begin
      FCurrRecordPos := FPrevRecordPos;
      WriteRecordHeaderNextPos(0);
      FBeginLastRecordPos := FPrevRecordPos;
    end;
    FEOF := False;
    Dec(FRecordCount);
    Last;
  end
  else
  begin
    FCurrRecordPos := FPrevRecordPos;
    WriteRecordHeaderNextPos(FNextRecordPos);
    FCurrRecordPos := FNextRecordPos;
    WriteRecordHeaderPrevPos(FPrevRecordPos);
    Dec(FRecordCount);
    Dec(FRecNo);
    Next;
  end;

  SetBrowseStatus;
end;


procedure TsbTableCursor.Edit;
begin
  if IsEmptyCursor then
    Append
  else
  begin
    FStatus := sbcEdit;
    NotifyStateChanged;
  end;  
end;


procedure TsbTableCursor.First;
begin
  if not FBOF then
  begin
    if FFiltered then
    begin
      if not IsEmptyCursor then
        FCurrRecordPos := Integer(FFiltRec[0])
      else
        FCurrRecordPos := FBeginFirstRecordPos;
    end
    else
    begin
      if FIndexEnabled then
      begin
        FActiveIndex.First;
        FCurrRecordPos := FActiveIndex.FCurrTablePosition;
      end
      else
        FCurrRecordPos := FBeginFirstRecordPos;
    end;

    FFields.SetInitializedFlag(False);
    FEOF := IsEmptyCursor;
    if FEOF then
    begin
      FPrevRecordPos := 0;
      FNextRecordPos := 0;
      FRecNo := 0;
    end
    else
    begin
      ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
      FRecNo := 1;
    end;
    FBof := True;

    NotifyChange;
  end;
  SetBrowseStatus;
end;


function TsbTableCursor.IsEmptyCursor: Boolean;
begin
  Result := (RecordCount = 0);
end;


procedure TsbTableCursor.Last;
begin
  if not FEOF then
  begin
    if FFiltered then
    begin
      if not IsEmptyCursor then
        FCurrRecordPos := Integer(FFiltRec[FFiltRec.Count-1])
      else
        FCurrRecordPos := FBeginLastRecordPos;
    end
    else
    begin
      if FIndexEnabled then
      begin
        FActiveIndex.Last;
        FCurrRecordPos := FActiveIndex.FCurrTablePosition;
      end
      else
        FCurrRecordPos := FBeginLastRecordPos;
    end;

    FFields.SetInitializedFlag(False);
    FBOF := IsEmptyCursor;
    if FBOF then
    begin
      FPrevRecordPos := 0;
      FNextRecordPos := 0;
      FRecNo := 0;
    end
    else
    begin
      ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
      FRecNo := RecordCount;
    end;
    FEOF := True;

    NotifyChange;
  end;
  SetBrowseStatus;
end;

procedure TsbTableCursor.Next;
var
  p: Cardinal;
begin
  if not FEOF then
  begin
    if FFiltered then
    begin
      FEOF := FRecNo >= FFiltRec.Count;
      if not FEOF then
        p := Cardinal(FFiltRec[FRecNo])
      else
        p := FBeginFirstRecordPos;
    end

    else
    begin
      if FIndexEnabled then
      begin
        FActiveIndex.Next;
        p := FActiveIndex.FCurrTablePosition;
        FEOF := (p = FCurrRecordPos);
      end
      else
      begin
        FEOF := (FNextRecordPos = 0);
        p := FNextRecordPos;
      end;
    end;

    if not FEOF then
    begin
      FCurrRecordPos := p;
      FFields.SetInitializedFlag(False);
      ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
      Inc(FRecNo);
    end;
    FBOF := False;

    NotifyChange;
  end;
  SetBrowseStatus;
end;


procedure TsbTableCursor.Post;
var
  f: Boolean;
  flt: Boolean;
  p, n, m: Integer;
begin
  flt := FFiltered;
  FFiltered := False;

  case FStatus of
    sbcAppend:
      begin
        if FBeginFirstDelRecordPos = 0 then
        begin
          f := IsEmptyCursor;
          if f then
          begin
            p := 0;
            FCurrRecordPos := FBeginFirstRecordPos;
          end
          else
          begin
            n := FStream.Size;
            WriteRecordHeader(FPrevRecordPos, n);
            p := FCurrRecordPos;
            FCurrRecordPos := n;
          end;
          FPrevRecordPos := p;
          FNextRecordPos := 0;
          WriteRecordHeader(FPrevRecordPos, FNextRecordPos);
          WriteRecord;
          if not f then
            FBeginLastRecordPos := FCurrRecordPos;
        end
        else
        begin
          f := IsEmptyCursor;
          n := FBeginFirstDelRecordPos;
          FCurrRecordPos := n;
          ReadRecordHeaderNextPos(FBeginFirstDelRecordPos);
          if f then
            FPrevRecordPos := 0
          else
            FPrevRecordPos := FBeginLastRecordPos;
          FNextRecordPos := 0;
          WriteRecordHeader(FPrevRecordPos, FNextRecordPos);
          WriteRecord;
          if not f then
          begin
            FCurrRecordPos := FBeginLastRecordPos;
            WriteRecordHeaderNextPos(n);
          end;
          FBeginLastRecordPos := n;
          FCurrRecordPos := FBeginLastRecordPos;
        end;

        FEOF := False;
        FBOF := False;
        Inc(FRecordCount);
        FRecNo := FRecordCount;
      end;

    sbcInsert:
      begin
        if FBeginFirstDelRecordPos = 0 then
          n := FStream.Size
        else
        begin
          n := FBeginFirstDelRecordPos;
          p := FCurrRecordPos;
          FCurrRecordPos := n;
          ReadRecordHeaderNextPos(FBeginFirstDelRecordPos);
          FCurrRecordPos := p;
        end;

        p := FPrevRecordPos;
        WriteRecordHeaderPrevPos(n);
        m := FCurrRecordPos;
        if p <> 0 then
        begin
          FCurrRecordPos := p;
          WriteRecordHeaderNextPos(n);
        end
        else
          FBeginFirstRecordPos := n;

        FCurrRecordPos := n;
        FPrevRecordPos := p;
        FNextRecordPos := m;
        WriteRecordHeader(FPrevRecordPos, FNextRecordPos);
        WriteRecord;
        FEOF := False;
        FBOF := False;
        Inc(FRecordCount);
      end;

    sbcEdit:
      WriteRecord;
  end;


  if flt then
  begin
    FFiltered := True;
    f := CheckRecordForFilter;
    case FStatus of
      sbcEdit: if not f then
               begin
                 FFiltRec.Delete(FRecNo - 1);
                 FBOF := False;
                 First;
               end;

      sbcAppend:
               if f then
                 FRecNo := FFiltRec.Add(Pointer(FCurrRecordPos)) + 1
               else
               begin
                 FEOF := False;
                 Last;
               end;

      sbcInsert:
               if f then
                 FFiltRec.Insert(RecNo - 1, Pointer(FCurrRecordPos));
    end;
  end;

  FModified := True;
  FStatus := sbcBrowse;  // don't call SetBrowseStatus because of AutoInc fields logic
  NotifyChange;
end;


procedure TsbTableCursor.Prior;
var
  p: Cardinal;
begin
  if not FBOF then
  begin
    if FFiltered then
    begin
      FBOF := FRecNo <= 1;
      if not FBOF then
        p := Integer(FFiltRec[FRecNo-2])
      else
        p := FBeginFirstRecordPos;
    end

    else
    begin
      if FIndexEnabled then
      begin
        FActiveIndex.Prior;
        p := FActiveIndex.FCurrTablePosition;
        FBOF := (p = FCurrRecordPos);
      end
      else
      begin
        FBOF := (FPrevRecordPos = 0);
        p := FPrevRecordPos;
      end;
    end;

    if not FBOF then
    begin
      FCurrRecordPos := p;
      FFields.SetInitializedFlag(False);
      ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
      Dec(FRecNo);
    end;
    FEOF := False;

    NotifyChange;
  end;
  SetBrowseStatus;  
end;


procedure TsbTableCursor.ReadHeader;
var
  s: ShortString;
  i, FldCount: Integer;
  F: TsbField;
  lName: ShortString;
  lType: TsbFieldType;
  lSize: Byte;
begin
  FStream.Position := 0;
  ReadValue(FStream, @s, sbfString, sbFieldNameSize);
  if s <> sbSignature1_1 then
    raise EsbException.Create(sbeWrongTableFormat);

  ReadValue(FStream, @FldCount, sbfInteger);
  if FldCount <= 0 then
    raise EsbException.Create(sbeWrongTableFormat);

  FRecordSize := 0;
  FNullInfoSize := (FldCount + 7) div 8;
  FFields.Clear;
  for i := 1 to FldCount do
  begin
    ReadValue(FStream, @lName, sbfString, sbFieldNameSize);
    ReadValue(FStream, @lType, sbfByte);
    ReadValue(FStream, @lSize, sbfByte);
    F := FFields.CreateField(lName, lType, lSize);
    F.FPValRec.RecOffset := FRecordSize + SizeOf(Integer)*2 + FNullInfoSize;
    Inc(FRecordSize, F.Size);
    if lType = sbfString then
      Inc(FRecordSize);
  end;
  Inc(FRecordSize, SizeOf(Integer)*2 + FNullInfoSize);
  FUpdHeaderInfoPos := FStream.Position;
  ReadValue(FStream, @FRecordCount, sbfInteger);
  ReadValue(FStream, @FBeginFirstRecordPos, sbfInteger);
  ReadValue(FStream, @FBeginLastRecordPos, sbfInteger);
  ReadValue(FStream, @FBeginFirstDelRecordPos, sbfInteger);
  ReadValue(FStream, @FLastAutoIncValue, sbfAutoInc);  

  FHeaderSize := FStream.Position;
  FStatus := sbcInternal;
  FFields.ClearValues;

  if (FStream is TsbFileStream) and (FRecordSize > sbReadBufferSize) then
    TsbFileStream(FStream).SetBufferSize(sbWriteBufferSize, FRecordSize);
end;


procedure TsbTableCursor.WriteRecord;
begin
  Fields.WriteFieldsToStream;
end;


procedure TsbTableCursor.Open(AFileName: string; ABLOBTable: Boolean = False);
var
  f: Boolean;
begin
  if FStatus <> sbcInactive then
    ShowSBError(sbeTableIsOpened, []);

  if ABLOBTable then
    AFileName := ChangeFileExt(AFileName, sbBLOBFileExt)
  else
    AFileName := ChangeFileExt(AFileName, sbTableExt);

  FFileName := AFileName;

  BeforeOpen;

  if not FileExists(AFileName) then
    ShowSBError(sbeTableDoesntExist, [AFileName]);

  if FAccessMode = sbmMemory then
    FStream := TsbMemoryStream.Create(AFileName, ConvertAccessMode(FAccessMode))
  else
    FStream := TsbFileStream.Create(AFileName, ConvertAccessMode(FAccessMode));

  FAuxInternalRecordPos := -1;
  FIntPosBeforeDisable := 0;
  ReadHeader;
  f := FFiltered;
  FFiltered := False;
  InitialStatus;
  Filtered := f;

  AfterOpen;
end;


procedure TsbTableCursor.Close;
var
  fl: Boolean;
begin
  if FStatus = sbcInactive then
    Exit;

  fl := FFiltered;
  FFiltered := False;

  CloseAllIndexes;

  FFiltered := fl;

  if FFiltered then
    DestroyFilter;

  if FModified then
  begin
    DeleteSysIndInfo(Self);
    UpdateHeaderInfo;
  end;

  FFields.Clear;
  FreeAndNil(FStream);
  FBOF := True;
  FEOF := True;
  FRecordCount := 0;
  FRecNo := 0;
  FCurrRecordPos := 0;
  FModified := False;
  FStatus := sbcInactive;

  if FTemporary then
    DropTable;

  NotifyStateChanged;
end;


procedure TsbTableCursor.Cancel;
begin
  SetBrowseStatus;
  FFields.SetInitializedFlag(False);
  NotifyStateChanged;
end;

{
procedure TsbTableCursor.ReadRecordHeaderPrevPos(var APrevRec: Integer);
begin
  FStream.Position := FCurrRecordPos;
  ReadValue(FStream, @APrevRec, sbfInteger, SizeOf(Integer));
end;
}

procedure TsbTableCursor.ReadRecordHeaderNextPos(var ANextRec: Cardinal);
begin
  FStream.Position := FCurrRecordPos + SizeOf(Integer);
  ReadValue(FStream, @ANextRec, sbfInteger, SizeOf(Integer));
end;

procedure TsbTableCursor.ReadRecordHeader(var APrevRec, ANextRec: Cardinal);
begin
  FStream.Position := FCurrRecordPos;
  ReadValue(FStream, @APrevRec, sbfInteger, SizeOf(Cardinal));
  ReadValue(FStream, @ANextRec, sbfInteger, SizeOf(Cardinal));
end;

procedure TsbTableCursor.WriteRecordHeaderPrevPos(APrevRec: Cardinal);
begin
  FStream.Position := FCurrRecordPos;
  WriteValue(FStream, @APrevRec, sbfInteger);
end;


procedure TsbTableCursor.WriteRecordHeaderNextPos(ANextRec: Cardinal);
begin
  FStream.Position := FCurrRecordPos + SizeOf(Cardinal);
  WriteValue(FStream, @ANextRec, sbfInteger);
end;


procedure TsbTableCursor.WriteRecordHeader(APrevRec, ANextRec: Integer);
begin
  FStream.Position := FCurrRecordPos;
  WriteValue(FStream, @APrevRec, sbfInteger);
  WriteValue(FStream, @ANextRec, sbfInteger);
end;


procedure TsbTableCursor.UpdateHeaderInfo;
begin
  FStream.Position := FUpdHeaderInfoPos;
  WriteValue(FStream, @FRecordCount, sbfInteger);
  WriteValue(FStream, @FBeginFirstRecordPos, sbfInteger);
  WriteValue(FStream, @FBeginLastRecordPos, sbfInteger);
  WriteValue(FStream, @FBeginFirstDelRecordPos, sbfInteger);
  WriteValue(FStream, @FLastAutoIncValue, sbfAutoInc);
end;


procedure TsbTableCursor.SetRecNo(const AValue: Integer);
var
  i, j: Integer;
begin
  if (AValue = 0) or (RecordCount < AValue) then
    Exit;

  i := AValue - FRecNo;
  j := RecordCount - AValue;
  if i > 0 then
  begin
    if i > j then
    begin
      DisableNotification;
      Last;
      if not IsEmptyCursor then
        FEOF := False;
      EnableNotification;
      i := -j;
      if i = 0 then
        NotifyChange;
    end;
    MoveBy(i);
  end

  else if i < 0 then
  begin
    if -i > AValue then
    begin
      DisableNotification;
      First;
      if not IsEmptyCursor then
        FBOF := False;
      EnableNotification;
      i := AValue - 1;
      if i = 0 then
        NotifyChange;
    end;
    MoveBy(i);
  end;

  if not IsEmptyCursor then
  begin
    FBOF := False;
    FEOF := False;
  end;
end;


procedure TsbTableCursor.MoveBy(const AValue: Integer);
var
  i: Integer;
begin
  DisableNotification;

  if AValue < 0 then
  begin
    for i := AValue to -1 do
      Prior;
  end

  else if AValue > 0 then
  begin
    for i := 1 to AValue do
      Next;
  end;

  EnableNotification;

  if AValue <> 0 then
    NotifyChange;
end;

function TsbTableCursor.GetRecNo: Integer;
begin
  if IsEmptyCursor then
    Result := 0
  else
    Result := FRecNo;
end;


function TsbTableCursor.CreateIndex(AIndexName: string; const AFields: array of string;
   AOptions: TsbIndexOptions; ADontChangeCurrentIndex: Boolean = False): string;
var
  i, j: Integer;
  Ind:  TsbIndex;
  Ftmp: TsbFileStream;
  h, h1: String;
  lDesc: Boolean;
  Ph: Pointer;
  Flds: array of TsbField;
  lIndInfo: string;

  procedure ReadKey(ABuffer: Pointer; ARecNum: Integer);
  begin
    Ftmp.Position := (ARecNum-1) * Ind.FIndexRecSize;
    Ftmp.Read(ABuffer, Ind.FIndexRecSize);
  end;

 //sorting of temp file (method of quick sort)
  procedure Sort;
  var
    i: Integer;
    B1, B2, B3: Pointer;
    RealTmpKeySize: Word;

    procedure WriteKey(ABuffer: Pointer; ARecNum: Integer);
    begin
      Ftmp.Position := (ARecNum-1) * Ind.FIndexRecSize;
      Ftmp.Write(ABuffer, Ind.FIndexRecSize);
    end;

    procedure Quicksort (lb, ub: Integer);
    var
      m: Integer;

      function Partition(lb, ub: integer): integer;
      var
        i, j, p: Integer;
      begin
        p := lb + (ub - lb) div 2;
        ReadKey(B1, p);
        CopyMemory(B2, B1, RealTmpKeySize);
        ReadKey(B3, lb);
        WriteKey(B3, p);
        WriteKey(B2, lb);
        i := lb+1;
        j := ub;
        while True do
        begin
          ReadKey(B2, i);
          while (i < j) and (Ind.CompareKeys(B1, B2) > 0) do
          begin
            Inc(i);
            ReadKey(B2, i);
          end;
          ReadKey(B3, j);
          while (j >= i) and (Ind.CompareKeys(B3, B1) > 0) do
          begin
            Dec(j);
            ReadKey(B3, j);
          end;

          if i >= j then
            break;

          WriteKey(B3, i);
          WriteKey(B2, j);
          Dec(j);
          Inc(i);
        end;

        ReadKey(B2, j);
        WriteKey(B2, lb);
        WriteKey(B1, j);

        Result := j;
      end;

    begin
      while lb < ub do
      begin
        m := Partition(lb, ub);
        if m - lb <= ub - m  then
        begin
          QuickSort(lb, m - 1);
          lb := m + 1;
        end
        else
        begin
          QuickSort(m + 1, ub);
          ub := m - 1;
        end;
      end;
    end;

  begin
    i := Ind.FIndexRecSize mod SizeOf(DWord);
    RealTmpKeySize := Ind.FIndexRecSize;
    if i <> 0 then
      Inc(RealTmpKeySize,SizeOf(DWord) - i);

    B1 := AllocMem(RealTmpKeySize);
    B2 := AllocMem(RealTmpKeySize);
    B3 := AllocMem(RealTmpKeySize);
    try
      Quicksort(1, RecordCount);
    finally
      FreeMem(B1, RealTmpKeySize);
      FreeMem(B2, RealTmpKeySize);
      FreeMem(B3, RealTmpKeySize);
    end;
  end;


  function GetIndFieldsInfo: String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 0 to Length(Flds) - 1 do
    begin
      if Flds[i].FDescInd then
        Result := Result + '#';
      Result := Result + AnsiUpperCase(Flds[i].FieldName) + ';';
    end;

    if AOptions = [] then
      System.Delete(Result, Length(Result), 1)
    else
    begin
      if isbDescending in AOptions then
        Result := Result + '$D';
      if isbCaseInsensitive in AOptions then
        Result := Result + '$C';
      if isbUnique in AOptions then
        Result := Result + '$U';
    end;
  end;


begin
  if Length(AFields) = 0 then
    ShowSBError(sbeCannotCreateTableIndex, []);

  SetLength(Flds, Length(AFields));
  for i := Low(AFields) to High(AFields) do
  begin
    j := Pos(' ', AFields[i]);
    if j = 0 then
    begin
      h := AFields[i];
      lDesc := False;
    end
    else
    begin
     h := AnsiUpperCase(Copy(AFields[i], 1, j-1));
     h1 := Copy(AFields[i], j+1, Length(AFields[i])-j);
     if h1 = 'DESC' then
       lDesc := True
     else
       lDesc := False;
    end;
    Flds[i] := Fields.FieldByName(h);
    Flds[i].FDescInd := lDesc;
  end;


  lIndInfo := TableName + ':' + GetIndFieldsInfo;
  if not Filtered and (sbFindSysIndInfo(lIndInfo) <> -1) then
  begin
    Result := SysIndPool.FList.Values[lIndInfo];
    Exit;
  end
  else
    Result := AIndexName;

  if not ADontChangeCurrentIndex then
    SetIndex('');

  Ind := TsbIndex.Create(Self, AIndexName, FAccessMode, True);
  Ftmp := TsbFileStream.Create(ExtractFilePath(AIndexName) + ChangeFileExt('~'+ExtractFileName(AIndexName), sbIndexExt),
    [sbsCreate, sbsReadWriteShare]);
  try
    SetLength(Ind.FFields, Length(Flds));
    for i := Low(Flds) to High(Flds) do
      Ind.FFields[i] := Flds[i];

    Ind.FOptions := AOptions;
    Ind.WriteHeader;
    Ind.AllocBuf;

    //filling temp file with index expression
    i := Ind.FIndexRecSize*RecordCount;
    Ftmp.SetBufferSize(i, i);
    DisableNotification;
    First;
    while not Eof do
    begin
      Ind.GetIndexData(Ind.FRecBuffer);
      WriteValue(Ftmp, Ind.FRecBuffer, sbfUnknown, Ind.FIndexExprSize);
      WriteValue(Ftmp, @FCurrRecordPos, sbfInteger);
      Next;
    end;

    Sort;
    EnableNotification;


    //filling index with B-Tree of expressions
    Ind.FCurrRecordPos := Ind.FHeaderSize;
    if (isbUnique in AOptions) and (RecordCount > 1) then
    begin
      ReadKey(Ind.FBuffer, 1);
      WriteValue(Ind.FStream, Ind.FBuffer, sbfUnknown, Ind.FIndexRecSize);
      i := 2;
      repeat
        ReadKey(Ind.FRecBuffer, i);
        if Ind.CompareKeys(Ind.FBuffer, Ind.FRecBuffer) <> 0 then
        begin
          WriteValue(Ind.FStream, Ind.FRecBuffer, sbfUnknown, Ind.FIndexRecSize);
          Ph := Ind.FRecBuffer;
          Ind.FRecBuffer := Ind.FBuffer;
          Ind.FBuffer := Ph;
        end;
        Inc(i);
      until i > RecordCount;
    end

    else
    begin
      for i := 1 to RecordCount do
      begin
        ReadKey(Ind.FRecBuffer, i);
        WriteValue(Ind.FStream, Ind.FRecBuffer, sbfUnknown, Ind.FIndexRecSize);
      end;
    end;

    if Ind.FStream.Position < Ind.FIndexRecSize then
      Ind.FBeginLastRecordPos := 0
    else
      Ind.FBeginLastRecordPos := Ind.FStream.Position - Ind.FIndexRecSize;
    Ind.WriteHeader;

  finally
    h := Ftmp.FFileName;
    Ftmp.Free;
    DeleteFile(h);
    Ind.Free;
  end;

  if not Filtered and not IsIndexSet then
    SysIndPool.FList.Add(lIndInfo + '=' + Result);
end;

{
function TsbTableCursor.GetUniqIndexFileName: String;
var
  i: Integer;
begin
  for i := 1 to sbMaxIndexesNumber do
  begin
    Result := ExtractFileName(FStream.FFileName)+IntToStr(i)+sbIndexExt;
    if not FileExists(Result) then
      break
    else
      Result := '';
  end;
end;
}


procedure TsbTableCursor.SetIndex(AIndexName: string; ATemporary: Boolean = False);
var
  j: Integer;
  fl: Boolean;
  OldIndex: TsbIndex;
begin
  OldIndex := FActiveIndex;
  FActiveIndex := nil;
  FIndexEnabled := False;

  if Length(AIndexName) = 0 then
  begin
    if Active then
    begin
      FBOF := False;
      FEOF := False;
      First;
      if Filtered and Assigned(OldIndex) then
      begin
        Filtered := False;
        Filtered := True;
      end;
    end;
    Exit;
  end;

  fl := Filtered;
  if fl then
    Filtered := False;
  FBOF := False;
  FEOF := False;

  AIndexName := AnsiUpperCase(AIndexName);
  j := FIndexList.IndexOf(AIndexName);
  if j = -1 then
  begin
    FActiveIndex := TsbIndex.Create(Self, AIndexName, FAccessMode, False, ATemporary);
    FIndexList.AddObject(AIndexName, FActiveIndex);
  end
  else
    FActiveIndex :=  TsbIndex(FIndexList.Objects[j]);

  FIndexEnabled := True;

  if FActiveIndex.IsEmptyIndex then
  begin
    FStatus := sbcInternal;
    FFields.ClearValues;
    SetBrowseStatus;
    FBOF := True;
    FEOF := True;
  end
  else
    if Active then
      First;

  if fl then
    Filtered := True;
end;


function TsbTableCursor.IsIndexSet: Boolean;
begin
  Result := Assigned(FActiveIndex);
end;


function TsbTableCursor.FindKeyByFieldList(AList: TList): Boolean;
var
  i, n: Integer;
begin
  Result := False;
  if not FIndexEnabled then
    ShowSBError(sbeIndexIsNotSet, []);
  if (RecordCount = 0) then
    Exit;

  FFields.SetInitializedFlag(True);
  FStatus := sbcInternal;

  n := AList.Count - 1;
  for i := 0 to n do
    FActiveIndex.FFields[i].Value := TsbField(AList[i]).Value;

  SetBrowseStatus;
  Result := InternalFindKey;
  FFields.SetInitializedFlag(False);
end;


function TsbTableCursor.InternalFindKey: Boolean;
begin
  Result := FActiveIndex.FindKey;
  if Result then
  begin
    FCurrRecordPos := FActiveIndex.FCurrTablePosition;
    ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
    FRecNo := (FActiveIndex.FCurrRecordPos - FActiveIndex.FBeginFirstRecordPos) div FActiveIndex.FIndexRecSize + 1;
    FEOF := False;
    FBOF := False;
  end;

  NotifyChange;
end;


function TsbTableCursor.FindKey(const AValues: array of variant): Boolean;
var
  i: Integer;
begin
  Result := False;
  if not FIndexEnabled then
    ShowSBError(sbeIndexIsNotSet, []);
  if (RecordCount = 0) then
    Exit;

  FFields.SetInitializedFlag(True);
  FStatus := sbcInternal;

  for i := Low(AValues) to High(AValues) do
    FActiveIndex.FFields[i].Value := AValues[i];

  SetBrowseStatus;
  Result := InternalFindKey;
  FFields.SetInitializedFlag(False);
end;


procedure TsbTableCursor.Clear;
var
  B: Pointer;
  h:  String;
  M: TsbStreamMode;
begin
  DeleteSysIndInfo(Self);

  if FRecordCount = 0 then
    Exit;

  B := AllocMem(FHeaderSize);
  try
    FStream.Position := 0;
    ReadValue(FStream, B, sbfUnknown, FHeaderSize);
    h := FStream.FileName;
    M := FStream.FMode;
    FreeAndNil(FStream);
    DeleteFile(h);
    FStream := TsbFileStream.Create(h, [sbsCreate, sbsWrite]);
    FStream.Position := 0;
    WriteValue(FStream, B, sbfUnknown, FHeaderSize);
    FRecordCount := 0;
    FBeginFirstRecordPos := FHeaderSize;
    FBeginLastRecordPos := FHeaderSize;
    FBeginFirstDelRecordPos := 0;
    FLastAutoIncValue := 0;
    UpdateHeaderInfo;
    FreeAndNil(FStream);
  finally
    FreeMem(B, FHeaderSize);
  end;

  FStream := TsbFileStream.Create(h, M);
  InitialStatus;
end;


procedure TsbTableCursor.InitialStatus;
begin
  FStatus := sbcBrowse;
  FBOF := False;
  FEOF := False;
  FModified := False;
  First;
end;


procedure TsbTableCursor.SetFiltered(const Value: Boolean);
begin
  if FFiltered = Value then
    Exit;

  if Active then
  begin
    if not FFiltered then
      CreateFilter
    else
      DestroyFilter;

    FFiltered := Value;
    FBOF := False;
    First;
  end
  else
    FFiltered := Value;
end;


procedure TsbTableCursor.CreateFilter(ACondition: TObject = nil);
var
  i, n: Integer;
  ind_fl: Boolean;

  function CompareProc(Item1, Item2: Pointer): Integer;
  begin
    if Cardinal(Item1) < Cardinal(Item2) then
      Result := -1
    else if Cardinal(Item1) > Cardinal(Item2) then
      Result := 1
    else
      Result := 0;
  end;

begin
  FFiltRec := TList.Create;

  if (Length(FFilter) > 0) or Assigned(ACondition) then
    if Assigned(ACondition) then
      FFilterCond := ACondition

    else
      try
        InitializationSbVM;
        FFilterSQLObject := PrepareFilter(Self);
        FFilterCond := TsbSQLInfo(FFilterSQLObject).Where;
      except
        DeInitializationSbVM;
        FreeAndNil(FFiltRec);
        raise;
      end;

  DisableNotification;

  if FIndexEnabled then
  begin
    FIndexEnabled := False;
    ind_fl := True;
    FBOF := False;
  end
  else
    ind_fl := False;

  FFiltRec.Count := RecordCount;
  i := 0;
  First;
  while not EOF do
  begin
    if CheckRecordForFilter then
    begin
      if ind_fl then
      begin
        if FActiveIndex.ValidInternalRecordPos(FCurrRecordPos) then
        begin
          FFiltRec[i] := Pointer(FActiveIndex.FCurrRecordPos);
          Inc(i);
        end
      end
      else
      begin
        FFiltRec[i] := Pointer(FCurrRecordPos);
        Inc(i);
      end;
    end;
    Next;
  end;
  FFiltRec.Count := i;

  if ind_fl then
  begin
    FFiltRec.Sort(@CompareProc);
    n := FFiltRec.Count - 1;
    for i := 0 to n do
    begin
      FActiveIndex.FCurrRecordPos := Cardinal(FFiltRec[i]);
      FActiveIndex.ReadCurrentKey;
      FFiltRec[i] := Pointer(FActiveIndex.FCurrTablePosition);
    end;
  end;

  FIndexEnabled := ind_fl;

  EnableNotification;
end;


procedure TsbTableCursor.DestroyFilter;
begin
  if Assigned(FFilterSQLObject) then
  begin
    FreeAndNil(FFilterSQLObject);
    FFilterCond := nil;
    DeInitializationSbVM;
  end;

  FreeAndNil(FFiltRec);
end;


function TsbTableCursor.GetRecordCount: Integer;
begin
  if FFiltered then
    Result := FFiltRec.Count
  else if Assigned(FActiveIndex) then
    Result := FActiveIndex.FRecordCount
  else
    Result := FRecordCount;
end;


procedure TsbTableCursor.CloseAllIndexes;
var
  i, n: Integer;
begin
  try
    n := FIndexList.Count-1;
    for i := 0 to n do
      TsbIndex(FIndexList.Objects[i]).Free;
  finally
    FIndexList.Clear;
    SetIndex('');
  end;
end;


procedure TsbTableCursor.CloseIndex(AIndexName: string);
var
  j: Integer;
  Ind: TsbIndex;
begin
  if Length(AIndexName) = 0 then
    Exit;

  AIndexName := AnsiUpperCase(AIndexName);
  j := FIndexList.IndexOf(AIndexName);
  if j <> -1 then
  begin
    Ind := TsbIndex(FIndexList.Objects[j]);
    FIndexList.Delete(j);
    Ind.Free;
    if FActiveIndex = Ind then
      SetIndex('');
  end;
end;


procedure TsbTableCursor.CloseActiveIndex;
begin
  if Assigned(FActiveIndex) then
    CloseIndex(FActiveIndex.FName);
end;


procedure TsbTableCursor.ApplayPreparedFilter(ACondition: TObject);
begin
  CreateFilter(ACondition);
  FFiltered := True;
  First;
end;


procedure TsbTableCursor.SetInternalRecordPos(const Value: Cardinal);
begin
  if Value < FHeaderSize then
  begin
   FOuterPosition := True;
   FStatus := sbcInternal;
   FFields.ClearValues;
   SetBrowseStatus;
   FBOF := True;
   FEOF := True;
  end

  else
  begin
    FOuterPosition := False;
    FCurrRecordPos := Value;

    if FIndexEnabled then
      FActiveIndex.SetInternalPos(FCurrRecordPos);

    FFields.SetInitializedFlag(False);
    ReadRecordHeader(FPrevRecordPos, FNextRecordPos);
    FEOF := False;
    FBOF := False;
  end;

  FAuxInternalRecordPos := -1;
end;


function TsbTableCursor.LocateByPreparedCondition(ACondition: TObject): Boolean;
begin
  Result := False;
  DisableNotification;
  while not Eof do
  begin
    if CheckConditions(TsbConditionNode(ACondition)) then
    begin
      Result := True;
      Exit;
    end;
    Next;
  end;
  EnableNotification;

  NotifyChange;
end;


function TsbTableCursor.IsKeyValueChanged: Boolean;
begin
  Result := FIndexEnabled and (FActiveIndex.CompareKeys(FActiveIndex.FRecBuffer, FActiveIndex.FPrevRecBuffer) <> 0);
end;


function TsbTableCursor.Locate(const AFields: array of string; const AValues: array of variant;
           const CaseSensitive: Boolean; const PartialKey: Boolean): Boolean;
var
  i: Integer;
  h1, h2: String;
begin
  Result := False;
  DisableNotification;
  StoreState;
  First;

  while not Eof do
  begin
    for i := Low(AFields) to High(AFields) do
    begin
      if VarIsNull(AValues[i]) and VarIsNull(Fields.FieldByName(AFields[i]).Value) then
        Result := True

      else if not VarIsNull(AValues[i]) and not VarIsNull(Fields.FieldByName(AFields[i]).Value) then
      begin
        h1 := VarAsType(Fields.FieldByName(AFields[i]).Value, varString);
        h2 := VarAsType(AValues[i], varString);

        if PartialKey then
          h1 := Copy(h1, 1, Length(h2));

        if CaseSensitive then
          Result := AnsiSameStr(h1, h2)
        else
          Result := AnsiSameText(h1, h2);
      end

      else
        Result := False;

      if not Result then
        Break;
    end;

      if Result then
        Break;
    Next;
  end;

  if not Result then
    RestoreState;

  EnableNotification;

  NotifyChange;
end;


function TsbTableCursor.GetActive: Boolean;
begin
  Result := (FStatus <> sbcInactive);
end;


procedure TsbTableCursor.Open(const AAccessMode: TsbAccessModeType = sbmReadWrite);
begin
  FAccessMode := AAccessMode;
  Open(FFileName);
end;


procedure TsbTableCursor.SetFileName(const Value: string);
begin
  if FStatus <> sbcInactive then
    ShowSBError(sbeTableIsOpened, []);
  FFileName := Value;
end;


procedure TsbTableCursor.AfterOpen;
begin
end;

procedure TsbTableCursor.BeforeOpen;
begin
end;


procedure TsbTableCursor.NotifyChange;
var
  fl: Boolean;
begin
  if FNotify then
  begin
    fl := FIntPosBeforeDisable <> FCurrRecordPos;
    FIntPosBeforeDisable := FCurrRecordPos;

    NotifyStateChanged;

    if fl and Assigned(FOnAfterScroll) then
      OnAfterScroll(Self);
  end;
end;


procedure TsbTableCursor.DisableNotification;
begin
  if FNotify then
    FIntPosBeforeDisable := FCurrRecordPos;
  Inc(FNotifyCouter);
  FNotify := False;
end;

procedure TsbTableCursor.EnableNotification;
begin
  Dec(FNotifyCouter);
  if FNotifyCouter <= 0 then
  begin
    FNotifyCouter := 0;
    FNotify := True;
  end;
end;

function TsbTableCursor.CheckRecordForFilter: boolean;
begin
  Result := True;
  if Assigned(FFilterCond) then
    Result := CalcFltCond(TsbConditionNode(FFilterCond));
  if Result and Assigned(FOnFilterRecord) then
    FOnFilterRecord(Self, Result);
end;

procedure TsbTableCursor.DropTable;
var
  s: string;
  i, n: Integer;
begin
  DeleteSysIndInfo(Self);
  s := TableName;
  if Active then
    Close;
  DeleteFile(s);
  n := FFields.Count - 1;
  for i := 0 to n do
   if Assigned(FFields[i].FBLOBTable) then
      FFields[i].FBLOBTable.FTemporary := True;
  FFields.Clear;
end;


function TsbTableCursor.GetCanModify: Boolean;
begin
  Result := Active and (FAccessMode in [sbmWrite, sbmReadWrite, sbmMemory]);
end;

procedure TsbTableCursor.NotifyStateChanged;
begin
  if FNotify and Assigned(FOnChangeDataState) then
    OnChangeDataState(Self);
end;


function TsbTableCursor.ValidInternalRecordPos( const Value: Cardinal): Cardinal;
begin
  Result := 0;
  DisableNotification;
  StoreState;
  try
    First;
    while not Eof do
    begin
      if InternalRecordPos = Value then
      begin
        Result := RecNo;
        break;
      end;
      Next;
    end;

  finally
    RestoreState;
    EnableNotification;
  end;
end;


procedure TsbTableCursor.RestoreState;
begin
  InternalRecordPos := FStInternalPos;
  if FIndexEnabled then
  begin
    FActiveIndex.FCurrRecordPos := FStIntIndPos;
    FActiveIndex.ReadCurrentKey;
  end;
  FBOF := FStBof;
  FEOF := FStEof;
  FRecNo := FStRecNo;
end;

procedure TsbTableCursor.StoreState;
begin
  if FIndexEnabled then
    FStIntIndPos := FActiveIndex.FCurrRecordPos;
  FStInternalPos := InternalRecordPos;
  FStBof := FBOF;
  FStEof := FEOF;
  FStRecNo := FRecNo;
end;


procedure TsbTableCursor.SetIndexEnabled(const Value: Boolean);
begin
  if IsIndexSet then
    FIndexEnabled := Value;
end;

function TsbTableCursor.TableSize: Cardinal;
var
  i: Integer;
begin
  Result := FStream.Size;

  for i := 0 to Fields.Count - 1 do
    if Fields[i].FieldType = sbfBLOB then
      Result := Result + Fields[i].FBLOBTable.TableSize;
end;

function TsbTableCursor.TableFileName: String;
begin
  Result := FFileName;
end;


procedure TsbTableCursor.AppendRecord(const FieldValues: array of Variant);
var
  i: Integer;
begin
  Append;
  for i := Low(FieldValues) to High(FieldValues) do
    Fields[i].Value := FieldValues[i];
  Post;
end;


function TsbTableCursor.GetFields: TrwSBFieldList;
var
  i: Integer;
begin
  SetLength(Result, Fields.Count);
  for i := 0 to Fields.Count - 1 do
    Result[i] := Fields[i];
end;

function TsbTableCursor.IsBof: Boolean;
begin
  Result := Bof;
end;

function TsbTableCursor.IsEof: Boolean;
begin
  Result := Eof;
end;

function TsbTableCursor.GetFieldByName(const FieldName: String): IrwSBField;
begin
  Fields.FieldByName(FieldName).GetInterface(IrwSBField, Result);
end;

function TsbTableCursor.GetFilter: String;
begin
  Result := Filter;
end;

function TsbTableCursor.GetFiltered: Boolean;
begin
  Result := Filtered;
end;

procedure TsbTableCursor.SetFilter(const AFilter: String);
begin
  Filter := AFilter; 
end;

function TsbTableCursor.RealObject: TObject;
begin
  Result := Self;
end;


const cData_Symbol = '&';
const cData_RecEnd = 'END';
const cData_FIELDS = 'FIELDS';
const cData_ROW = 'ROW';
const cData_NULL = 'NULL';
const cData_DataBeg = '<';
const cData_DataEnd = '>';
const cData_Types: array [sbfString..sbfBLOB] of String = ('String', 'Integer', 'Byte', 'DateTime', 'Float', 'Currency', 'Boolean', 'BLOB');

function TsbTableCursor.GetStrData(const AMode: TsbDataLoadMode = sbDLMAll): String;
var
  L: TStringList;
  h: String;
  i: Integer;
  PrevFieldNull: Boolean;

  function DataToStr(const AData: String): String;
  var
    i, j: Integer;
    C: Char;
    Code: Byte;
  begin
    Result := '';
    j := 1;
    for i := 1 to Length(AData) do
    begin
      C := AData[i];
      if not(C in [' '..'~']) or (C in [cData_DataBeg, cData_DataEnd, cData_Symbol]) then
      begin
        Code := Ord(C);
        Result := Result + Copy(AData, j, i - j) + cData_Symbol + BufferToHex(Code, SizeOf(Code));
        j := i + 1;
      end;
    end;

    if j <= Length(AData) then
      Result := Result + Copy(AData, j, Length(AData) - j + 1);
  end;

begin
  L := TStringList.Create;

  DisableNotification;
  StoreState;
  First;
  try
   if AMode in [sbDLMAll, sbDLMDataOnly] then
     L.Capacity := RecordCount + 1;

    if AMode in [sbDLMAll, sbDLMStructureOnly] then
    begin
      h := cData_FIELDS;
      for i := 0 to Fields.Count - 1 do
      begin
        h := h + cData_DataBeg + Fields[i].FieldName + ',' + cData_Types[Fields[i].FieldType];
        if Fields[i].FieldType = sbfString then
          h := h + ',' + IntToStr(Fields[i].Size);
        h := h + cData_DataEnd;
      end;
      L.Add(h + cData_RecEnd);
    end;

    if AMode in [sbDLMAll, sbDLMDataOnly] then
      while not Eof do
      begin
        h := cData_ROW;
        PrevFieldNull := True;
        for i := 0 to Fields.Count - 1 do
          if Fields[i].IsNull then
          begin
            if PrevFieldNull then
              h := h + ' ' + cData_NULL
            else
              h := h + cData_NULL;
            PrevFieldNull := True;
          end
          else
          begin
            h := h + cData_DataBeg + DataToStr(Fields[i].AsString) + cData_DataEnd;
            PrevFieldNull := False;
          end;

        if PrevFieldNull then
          h := h + ' '; 
        L.Add(h + cData_RecEnd);
        Next;
      end;


    Result := L.Text;

  finally
    FreeAndNil(L);
    RestoreState;
    EnableNotification;
  end;
end;

procedure TsbTableCursor.SetStrData(const AData: String; AMode: TsbDataLoadMode);
var
  CurrPos: Integer;
  WasActive: Boolean;

  function StrToData(const AStr: String): String;
  var
    i, j: Integer;
    Code: Byte;
  begin
    Result := '';
    j := 1;
    i := 1;
    while i <= Length(AStr) do
      if AStr[i] = cData_Symbol then
      begin
        HexToBuffer(AStr[i + 1] + AStr[i + 2], Code, SizeOf(Code));
        Result := Result + Copy(AStr, j, i - j) + Char(Code);
        Inc(i, 3);
        j := i;
      end
      else
        Inc(i);

    if j <= Length(AStr) then
      Result := Result + Copy(AStr, j, Length(AStr) - j + 1);
  end;

  function DataEof: Boolean;
  begin
    Result := CurrPos > Length(AData);
  end;

  procedure ReadStrData(var ARes: String);
  var
    FirstPos: Integer;
    BracketCount: Integer;
  begin
    FirstPos := CurrPos + 1;
    BracketCount := 0;
    while not DataEof do
    begin
      if AData[CurrPos] = cData_DataBeg then
        Inc(BracketCount)
      else if AData[CurrPos] = cData_DataEnd then
        Dec(BracketCount);
      Inc(CurrPos);

      if BracketCount = 0 then
        break;
    end;

    ARes := StrToData(Copy(AData, FirstPos, CurrPos - FirstPos - 1));
  end;

  function ReadToken(var ARes: String): Boolean;
  var
    FirstPos: Integer;
  begin
    ARes := '';
    Result := False;

    while not DataEof and (AData[CurrPos] in [' ', #13, #10]) do
      Inc(CurrPos);

    if DataEof then
      Exit;

    if AData[CurrPos] = cData_DataBeg then
      ReadStrData(ARes)

    else
    begin
      FirstPos := CurrPos;
      while not DataEof do
      begin
        if not ((AData[CurrPos] >= 'A') and  (AData[CurrPos] <= 'z')) then
          break;
        Inc(CurrPos);
      end;
      Result := True;
      ARes := Trim(Copy(AData, FirstPos, CurrPos - FirstPos));
    end;
  end;

  procedure ReadStructure;
  var
    Flds: TsbFields;
    Str: String;
    FldName: String;
    FldType, i: TsbFieldType;
//    j: Integer;
    FldSize: Integer;
    Tbl, TypeStr: String;
    flLoadStructure: Boolean;
  begin
    flLoadStructure := AMode in [sbDLMAll, sbDLMStructureOnly];

    if not flLoadStructure and not Active then
      Open;

    Flds := TsbFields.Create;
    try
      while not DataEof do
      begin
        if ReadToken(Str) and AnsiSameText(Str, cData_RecEnd)  then
          break;

        FldName := GetNextStrValue(Str, ',');
        TypeStr := GetNextStrValue(Str, ',');
        if Str <> '' then
          FldSize := StrToInt(Str)
        else
          FldSize := 0;

        FldType := sbfUnknown;
        for i := sbfString to sbfBLOB do
          if AnsiSameText(cData_Types[i], TypeStr) then
          begin
            FldType := i;
            break;
          end;

        Flds.CreateField(FldName, FldType, FldSize);
      end;

      if flLoadStructure then
      begin
        // create structure
        Tbl := TableName;
        DropTable;
        sbCreateTable(Tbl, Flds);
      end;

    finally
      FreeAndNil(Flds);
    end;

    if AMode = sbDLMStructureOnly then
      CurrPos := Length(AData) + 1;
  end;

  procedure ReadRow;
  var
    FldInd: Integer;
    FldValue: String;
  begin
    if not Active then
      Open;

    FldInd := 0;
    Append;
    while not DataEof do
    begin
      FldValue := '';
      if ReadToken(FldValue) and AnsiSameText(FldValue, cData_RecEnd) then
        break;

     if not AnsiSameText(FldValue, cData_NULL) then
       Fields[FldInd].AsString := FldValue;

     Inc(FldInd);
    end;
    Post;
  end;

  procedure ReadRecord;
  var
    Token: String;
  begin
    if ReadToken(Token) then
      if AnsiSameText(Token, cData_FIELDS) then
        ReadStructure
      else if AnsiSameText(Token, cData_ROW) then
        ReadRow
      else
        raise EsbException.Create('Unexpected token "' + Token + '"');
  end;

begin
  WasActive := Active;

  DisableNotification;
  try
    CurrPos := 1;
    while not DataEof do
      ReadRecord;
    Close;
  finally
    EnableNotification;
  end;

  if WasActive then
    Open;
end;

procedure TsbTableCursor.FlushBuffers;
var
  i: Integer;
begin
  if FModified then
  begin
    UpdateHeaderInfo;
    TsbFileStream(FStream).FlushWriteBuffer;
  end;  

  for i := 0 to Fields.Count - 1 do
    if Fields[i].FieldType = sbfBLOB then
    begin
      if Fields[i].FBLOBTable.FModified then
      begin
        Fields[i].FBLOBTable.UpdateHeaderInfo;
        TsbFileStream(Fields[i].FBLOBTable.FStream).FlushWriteBuffer;
      end;
    end;
end;


procedure TsbTableCursor.SetBrowseStatus;
begin
  if FStatus in [sbcAppend, sbcInsert] then
    FFields.DecAutoIncFields;

  FStatus := sbcBrowse;
end;

{TsbFileStream}

constructor TsbFileStream.Create(const AFileName: string; AMode: TsbStreamMode);
var
  S: String;
begin
  inherited;

  FHandleRead := 0;
  FHandleWrite := 0;
  FOneHandle := False;

  try
    if sbsCreate in AMode then
    begin
      if sbsRead in AMode then
      begin
        FHandleRead := Integer(mmioOpen(PChar(FFileName), nil, MMIO_CREATE or MMIO_READ));
        if FHandleRead = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName, SysErrorMessage(GetLastError)]);
      end;
      if sbsWrite in AMode then
      begin
        FHandleWrite := Integer(mmioOpen(PChar(FFileName), nil, MMIO_CREATE or MMIO_WRITE));
        if FHandleWrite = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName, SysErrorMessage(GetLastError)]);
      end
      else if sbsReadWriteShare in AMode then
      begin
        FHandleRead := Integer(mmioOpen(PChar(FFileName), nil, MMIO_CREATE or MMIO_READ or MMIO_WRITE));
        FHandleWrite := FHandleRead;
        FOneHandle := True;
        if FHandleRead = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName,  SysErrorMessage(GetLastError)]);
      end
    end

    else
    begin
      if sbsRead in AMode then
      begin
        FHandleRead := Integer(mmioOpen(PChar(FFileName), nil, MMIO_READ));
        if FHandleRead = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName, SysErrorMessage(GetLastError)]);
      end;
      if sbsWrite in AMode then
      begin
        FHandleWrite := Integer(mmioOpen(PChar(FFileName), nil, MMIO_WRITE));
        if FHandleWrite = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName, SysErrorMessage(GetLastError)]);
      end
      else if sbsReadWriteShare in AMode then
      begin
        FHandleRead := Integer(mmioOpen(PChar(FFileName), nil, MMIO_READ or MMIO_WRITE));
        FHandleWrite := FHandleRead;
        FOneHandle := True;
        if FHandleRead = 0 then
          raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [FFileName, SysErrorMessage(GetLastError)]);
      end;
    end;
  except
    on E: EFCreateError do
    begin
      S := '. Additional information: ';
      if FileExists(FFileName) then
        S := S + 'File exists. '
      else
        S := S + 'File not exists. ';
      if DirectoryExists(ExtractFileDir(FFileName)) then
        S := S + 'Folder exists.'
      else
        S := S + 'Folder not exists.';
      E.Message := E.Message + #13#10 + S;
      raise;
    end;
  end;

  SetBufferSize(sbReadBufferSize, sbWriteBufferSize);

  if FHandleRead <> 0 then
    FSize := mmioSeek(FHandleRead, 0, SEEK_END)
  else
    FSize := mmioSeek(FHandleWrite, 0, SEEK_END);

  FSyncWithWriteBuff := False;
  FPosition := 0;
  FOldWritePosition := 0;
  FOldReadPosition := 0;
  mmioSeek(FHandleRead, 0, SEEK_SET);
end;


destructor TsbFileStream.Destroy;
begin
  if FHandleWrite <> 0 then
  begin
    mmioFlush(FHandleWrite, 0);
    mmioClose(FHandleWrite, 0);
  end;

  if not FOneHandle and (FHandleRead <> 0) then
    mmioClose(FHandleRead, 0);
end;


procedure TsbFileStream.SetBufferSize(AReadBufSize: Integer; AWriteBufSize: Integer);
begin
  if FHandleWrite <> 0 then
    mmioSetBuffer(FHandleWrite, nil, AWriteBufSize, 0);
  if not FOneHandle and (FHandleRead <> 0) then
    mmioSetBuffer(FHandleRead, nil, AReadBufSize, 0);
end;


function TsbFileStream.Read(var Buffer; Count: Integer): Integer;
var
  n: Int64;
begin
  if FSyncWithWriteBuff then
  begin
    mmioAdvance(FHandleWrite, nil, MMIO_WRITE);
    mmioSeek(FHandleRead, FPosition, SEEK_SET);
    mmioAdvance(FHandleRead, nil, MMIO_READ);
    FSyncWithWriteBuff := False;
  end
  else
  begin
    n := Int64(FPosition) - Int64(FOldReadPosition);
    if n <> 0 then
      mmioSeek(FHandleRead, n, SEEK_CUR);
  end;

  Result := mmioRead(FHandleRead, PChar(Buffer), Count);
  if Result = -1 then
    ShowSBError(SysErrorMessage(GetLastError), [])

  else
  begin
    Inc(FPosition, Count);
    FOldReadPosition := FPosition;
    if FOneHandle then
      FOldWritePosition := FOldReadPosition;
  end;
end;


function TsbFileStream.Write(const Buffer; Count: Integer): Integer;
var
  n: Int64;
begin
  n := Int64(FPosition) - Int64(FOldWritePosition);
  if n <> 0 then
    mmioSeek(FHandleWrite, n, SEEK_CUR);
  Result := mmioWrite(FHandleWrite, PChar(Buffer), Count);
  if Result = -1 then
    ShowSBError(SysErrorMessage(GetLastError), [])
  else
  begin
    if FSize < FPosition + Cardinal(Count) then
      FSize := FPosition + Cardinal(Count);
    Inc(FPosition, Count);
    FOldWritePosition := FPosition;
    if FOneHandle then
      FOldReadPosition := FOldWritePosition
    else
      FSyncWithWriteBuff := True;
  end;
end;


function TsbFileStream.GetSize: Cardinal;
begin
  Result := FSize;
end;


function TsbFileStream.GetPosition: Cardinal;
begin
  Result := FPosition;
end;


procedure TsbFileStream.SetPosition(const Value: Cardinal);
begin
  FPosition := Value;
end;


procedure TsbFileStream.FlushWriteBuffer;
begin
  if FHandleWrite <> 0 then
    mmioFlush(FHandleWrite, 0);
end;


{ TsbIndex }

constructor TsbIndex.Create(ATableCursor: TsbTableCursor; AIndexName: String;
   AMode: TsbAccessModeType = sbmReadWrite; ANewIndex: Boolean = False; ATemporary: Boolean = False);
begin
  FCursor := ATableCursor;
  FAccessMode := AMode;
  FBuffer := nil;
  FRecBuffer := nil;
  FPrevRecBuffer := nil;
  FName := AnsiUpperCase(AIndexName);
  FTemporary := ATemporary;
  if ANewIndex then
  begin
    FStream := TsbFileStream.Create(ChangeFileExt(AIndexName, sbIndexExt), [sbsCreate, sbsRead, sbsWrite]);
  end
  else
  begin
    FStream := TsbFileStream.Create(ChangeFileExt(AIndexName, sbIndexExt), ConvertAccessMode(AMode));
    ReadHeader;
    AllocBuf;
    First;

    if FBeginLastRecordPos < FBeginFirstRecordPos then
      FRecordCount := 0
    else
      FRecordCount := (FBeginLastRecordPos - FBeginFirstRecordPos) div FIndexRecSize + 1;
  end;
end;


destructor TsbIndex.Destroy;
var
  i: Integer;
begin
  try
    FreeBuf;
    SetLength(FFields, 0);
    FreeAndNil(FStream);
    if FTemporary then
    begin
      DeleteFile(FName);
      i := sbFindSysIndInfoByName(FName);
      if i <> -1 then
        SysIndPool.FList.Delete(i);
    end;
  finally
    inherited;
  end;
end;


procedure TsbIndex.ReadHeader;
var
  i: Integer;
  s: String[sbFieldNameSize];
begin
  FStream.Position := 0;

  ReadValue(FStream, @i , sbfInteger, SizeOf(Integer));
  SetLength(FFields, i);
  for i := Low(FFields) to High(FFields) do
  begin
    ReadValue(FStream, @s, sbfString, sbFieldNameSize);
    FFields[i] := FCursor.FFields.FieldByName(s);
  end;

  ReadValue(FStream, @FOptions, sbfInteger, SizeOf(TsbIndexOptions));
  ReadValue(FStream, @FBeginFirstRecordPos, sbfInteger, SizeOf(Integer));
  ReadValue(FStream, @FBeginLastRecordPos, sbfInteger, SizeOf(Integer));
  FHeaderSize := FStream.Position;
end;


procedure TsbIndex.WriteHeader;
var
  i: Integer;
begin
  FStream.Position := 0;

  i := High(FFields) - Low(FFields) + 1;
  WriteValue(FStream, @i , sbfInteger);

  for i := Low(FFields) to High(FFields) do
    WriteValue(FStream, @(FFields[i].FPValRec^.FieldName), sbfString, sbFieldNameSize);

  WriteValue(FStream, @FOptions, sbfInteger);
  FBeginFirstRecordPos := FStream.Position + SizeOf(Integer)*2;
  if FBeginLastRecordPos = 0 then
    FBeginLastRecordPos := FBeginFirstRecordPos - 1;
  WriteValue(FStream, @FBeginFirstRecordPos, sbfInteger);
  WriteValue(FStream, @FBeginLastRecordPos, sbfInteger);
  FHeaderSize := FStream.Position;
end;



procedure TsbIndex.AllocBuf;
var
  i: Integer;
begin
  FreeBuf;
  FIndexExprSize := 0;
  for i := Low(FFields) to High(FFields) do
    if FFields[i].FPValRec^.FieldType in [sbfString, sbfByte] then
      FIndexExprSize := FIndexExprSize + FFields[i].Size
    else
      FIndexExprSize := FIndexExprSize + SizeOf(Double);
  FIndexExprSize := FIndexExprSize + Length(FFields);  //add null bytes
  FIndexRecSize := FIndexExprSize + SizeOf(Integer);
  FRecBuffer := AllocMem(FIndexRecSize);
  FBuffer := AllocMem(FIndexRecSize);
  FPrevRecBuffer := AllocMem(FIndexRecSize);
end;


procedure TsbIndex.GetIndexData(ABuffer: Pointer);
var
  i: Integer;
  n: Integer;
  P, lP: Pointer;
  D: Double;
  s: ShortString;

begin
  P := ABuffer;
  ZeroMemory(P, FIndexExprSize);

  for i := Low(FFields) to High(FFields) do
  begin
    lP := P;
    if FFields[i].IsNull then
    begin
      Byte(P^) := $00;
//      ZeroMemory(P, FFields[i].FPValRec^.FieldSize+1);
      P := Pointer(Cardinal(P) + FFields[i].FPValRec^.FieldSize + 1);
    end

    else
    begin
      Byte(P^) := $FF;
      P := Pointer(Cardinal(P) + 1);
      with FFields[i].FPValRec^ do
      begin
        FFields[i].SyncContent;
        case FieldType of
          sbfInteger, sbfFloat, sbfDateTime, sbfCurrency, sbfAutoInc:
            begin
              case FieldType of
                sbfInteger:  D := IntegerVal;
                sbfFloat:    D := FloatVal;
                sbfDateTime: D := DateTimeVal;
                sbfCurrency: D := CurrencyVal;
                sbfAutoInc:  D := AutoIncVal;
              end;

              CopyMemory(P, @D, SizeOf(Double));

              //Revert bytes sequence of IEEE float number
              asm
                PUSH EBX;
                PUSH ESI;
                PUSH EDI;

                XOR  ESI, ESI;
                XOR  EDI, EDI;
                MOV  EBX, P;
                MOV  ECX, 4;
                MOV  DI, 7;

  @@L:          MOV  AL, [EBX + ESI];
                XCHG BYTE PTR [EBX + EDI], AL;
                XCHG BYTE PTR [EBX + ESI], AL;
                INC  SI;
                DEC  DI;
                LOOP @@L;

                MOV  AX, 7;
                BT   [EBX], AX;
                JC   @@L1;
                BTS  [EBX], AX;
                JMP  @@End;

               //invert of negative numbers
@@L1:           XOR  ESI, ESI;
                MOV  ECX,  8;

@@L3:           MOV  AL, [EBX + ESI];
                NOT  AL;
                MOV  [EBX + ESI], AL;
                INC  ESI;
                LOOP @@L3;

@@End:          POP EDI;
                POP ESI;
                POP EBX;
              end;

              P := Pointer(Cardinal(P) + SizeOf(Double));
            end;

          sbfString:
            begin
              n := Length(StringVal);
              if isbCaseInsensitive in FOptions then
              begin
                s := AnsiUpperCase(StringVal);
                CopyMemory(P, @(s[1]), n);
              end
              else
                CopyMemory(P, @(StringVal[1]), n);
//              ZeroMemory(Pointer(Cardinal(P) + Cardinal(n)), FieldSize - Cardinal(n));
              P := Pointer(Cardinal(P) + FieldSize);
            end;

          sbfByte:
            begin
              CopyMemory(P, @ByteVal, FieldSize);
              P := Pointer(Cardinal(P) + FieldSize);
            end;
        end;
      end;

//      ZeroMemory(P, FIndexExprSize - (LongInt(P) - LongInt(ABuffer)));
    end;


    //Invert bytes sequence for DESC index
    if FFields[i].FDescInd then
    begin
      n := Integer(P) - Integer(lP);
      asm
              PUSH EBX;
              PUSH ESI;

              XOR  ESI, ESI;
              MOV  EBX, lP;
              MOV  ECX, DWORD PTR n;

@@L:          MOV  AL, [EBX + ESI];
              NOT  AL;
              MOV  [EBX + ESI], AL;
              INC  ESI;
              LOOP @@L;

              POP ESI;
              POP EBX;
      end;
    end;
  end;
end;


procedure TsbIndex.FreeBuf;
begin
  if Assigned(FRecBuffer) then
  begin
    FreeMem(FRecBuffer, FIndexRecSize);
    FRecBuffer := nil;
  end;

  if Assigned(FBuffer) then
  begin
    FreeMem(FBuffer, FIndexRecSize);
    FBuffer := nil;
  end;

  if Assigned(FPrevRecBuffer) then
  begin
    FreeMem(FPrevRecBuffer, FIndexRecSize);
    FPrevRecBuffer := nil;
  end;
end;


function TsbIndex.CompareKeys(AKey1, AKey2: Pointer): Shortint;
var
  K1, K2: Pointer;
  I: Word;
begin
  K1 := AKey1;
  K2 := AKey2;
  i := Self.FIndexExprSize;
  asm
              PUSH  ESI
              PUSH  EDI
              XOR   ECX, ECX;
              MOV   CX,  I;
              MOV   ESI, K1;
              MOV   EDI, K2;
              CLD   ;
              REPE  CMPSB;
              POP   EDI
              POP   ESI;
              JNZ   @@NotEqual;
              MOV   @Result, 0;
              JMP   @@End;
  @@NotEqual: JC    @@Less;
              MOV   @Result, 1;
              JMP   @@End;
  @@Less:     MOV   @Result, -1;
  @@End:
  end;
end;


procedure TsbIndex.First;
begin
  FCurrRecordPos := FBeginFirstRecordPos;
  ReadCurrentKey;
end;


procedure TsbIndex.Last;
begin
  FCurrRecordPos := FBeginLastRecordPos;
  ReadCurrentKey;
end;

procedure TsbIndex.Next;
begin
  Inc(FCurrRecordPos, FIndexRecSize);
  if FCurrRecordPos > FBeginLastRecordPos then
    Last
  else
    ReadCurrentKey;
end;

procedure TsbIndex.Prior;
begin
  if FCurrRecordPos < Cardinal(FIndexRecSize) then
    First
    
  else
  begin
    Dec(FCurrRecordPos, FIndexRecSize);
    if FCurrRecordPos < FBeginFirstRecordPos then
      First
    else
      ReadCurrentKey;
  end;
end;

procedure TsbIndex.ReadCurrentKey;
var
  P: Pointer;
begin
  P := FRecBuffer;
  FRecBuffer := FPrevRecBuffer;
  FPrevRecBuffer := P;

  if not IsEmptyIndex then
  begin
    FStream.Position := FCurrRecordPos;
    ReadValue(FStream, FRecBuffer, sbfUnknown, FIndexRecSize);
  end;  

  CopyMemory(@FCurrTablePosition, Pointer(Cardinal(FRecBuffer)+FIndexExprSize), SizeOf(FCurrTablePosition));
end;


function TsbIndex.FindKey: Boolean;
var
  i, bp, ep, m: Integer;
begin
  Result := False;
  GetIndexData(FBuffer);
  bp := 1;
  ep := FRecordCount;

  while ep >= bp do
  begin
    m := bp + (ep+1-bp) div 2;
    FCurrRecordPos := FBeginFirstRecordPos + Cardinal((m - 1) * FIndexRecSize);
    ReadCurrentKey;
    i := CompareKeys(FBuffer, FRecBuffer);
    if i < 0 then
      ep := m - 1

    else if i > 0 then
      bp := m + 1

    else
    begin
      if not (isbUnique in FOptions) and (FCurrRecordPos > FBeginFirstRecordPos) then
      begin
        repeat
          Prior;
          i := CompareKeys(FBuffer, FRecBuffer);
        until (FCurrRecordPos <= FBeginFirstRecordPos) or (i <> 0);
        if i <> 0 then
          Next;
      end;
      Result := True;
      break;
    end;
  end;

end;


function TsbIndex.IsEmptyIndex: Boolean;
begin
  Result := FBeginFirstRecordPos > FBeginLastRecordPos;
end;


procedure TsbIndex.SetInternalPos(const AIntPos: Cardinal);
begin
  if FCurrTablePosition = AIntPos then
    Exit;

  if FCursor.IsEmptyCursor and IsEmptyIndex then
    Exit;

  First;
  while FCurrRecordPos < FBeginLastRecordPos do
  begin
    if FCurrTablePosition = AIntPos then
      break;
    Next;
  end;

  if FCurrTablePosition <> AIntPos then
    ShowSBError(sbeInternalError, []);
end;

function TsbIndex.ValidInternalRecordPos(const Value: Cardinal): Boolean;
begin
  Result := False;

  if FCursor.IsEmptyCursor and IsEmptyIndex then
    Exit;

  First;
  while FCurrRecordPos < FBeginLastRecordPos do
  begin
    if Cardinal(FCurrTablePosition) = Value then
    begin
      Result := True;
      Exit;
    end;
    Next;
  end;

  Result := Cardinal(FCurrTablePosition) = Value;
end;



{ TsbItemList }

constructor TsbItemList.Create;
begin
  FList := TList.Create;
end;


destructor TsbItemList.Destroy;
begin
  try
    Clear;
    FList.Free;
  finally
    inherited;
  end;
end;


procedure TsbItemList.Clear;
var
  i, n: Integer;
begin
  try
    n := FList.Count-1;
    for i := 0 to n do
      TObject(FList[i]).Free;
  finally
    FList.Clear;
  end;  
end;


function TsbItemList.ItemCount: Integer;
begin
  Result := FList.Count;
end;

function TsbItemList.IndexOf(APointer: Pointer): Integer;
var
  i, n: Integer;
begin
  Result := -1;
  n := FList.Count-1;
  for i := 0 to n do
    if FList[i] = APointer then
    begin
      Result := i;
      break;
    end;
end;


procedure TsbItemList.Exchange(Index1, Index2: Integer);
begin
  FList.Exchange(Index1, Index2);
end;

procedure TsbItemList.Assign(ASource: TsbItemList);
begin
end;

{ TsbParams }

function TsbParams.Add(AParamName: string): Integer;
var
  P: TsbParam;
begin
  P := ParamByName(AParamName);
  if Assigned(P) then
    Result := IndexOf(P)
  else
  begin
    P := TsbParam.Create;
    P.FName := AnsiUpperCase(AParamName);
    TVarData(P.FValue).VType := varNull;
    Result := FList.Add(P);
  end;
end;


procedure TsbParams.Assign(ASource: TsbItemList);
var
  i, j, n: Integer;
begin
  Clear;
  n := ASource.ItemCount - 1;
  for i := 0 to n do
  begin
    j := Add(TsbParams(ASource)[i].Name);
    Items[j].Value := TsbParams(ASource)[i].Value;
  end;
end;

function TsbParams.GetItem(Index: Integer): TsbParam;
begin
  Result := TsbParam(FList[Index]);
end;


function TsbParams.ParamByName(AName: String): TsbParam;
var
  i, n: Integer;
begin
  Result := nil;
  AName := AnsiUpperCase(AName);
  n := FList.Count-1;
  for i := 0 to n do
    if CompareStr(Items[i].Name, AName) = 0 then
    begin
       Result := Items[i];
       break;
    end;
end;


function TsbParams.ParseSQL(const SQL: String; DoCreate: Boolean): String;
const
  Literals = ['''', '"', '`'];
var
  Value, CurPos, StartPos: PChar;
  CurChar: Char;
  Literal: Boolean;
  EmbeddedLiteral: Boolean;
  Name: string;

  function NameDelimiter: Boolean;
  begin
    Result := CurChar in [' ', ',', ';', ')', '-', '+', '/', '*', #13, #10];
  end;

  function IsLiteral: Boolean;
  begin
    Result := CurChar in Literals;
  end;

  function StripLiterals(Buffer: PChar): string;
  var
    Len: Word;
    TempBuf: PChar;

    procedure StripChar;
    begin
      if TempBuf^ in Literals then
        StrMove(TempBuf, TempBuf + 1, Len - 1);
      if TempBuf[StrLen(TempBuf) - 1] in Literals then
        TempBuf[StrLen(TempBuf) - 1] := #0;
    end;

  begin
    Len := StrLen(Buffer) + 1;
    TempBuf := AllocMem(Len);
    Result := '';
    try
      StrCopy(TempBuf, Buffer);
      StripChar;
      Result := StrPas(TempBuf);
    finally
      FreeMem(TempBuf, Len);
    end;
  end;

begin
  Result := SQL + ' ';
  Value := PChar(Result);
  if DoCreate then Clear;
  CurPos := Value;
  Literal := False;
  EmbeddedLiteral := False;
  repeat
    while (CurPos^ in LeadBytes) do Inc(CurPos, 2);
    CurChar := CurPos^;
    if (CurChar = ':') and not Literal and ((CurPos + 1)^ <> ':') then
    begin
      StartPos := CurPos;
      while (CurChar <> #0) and (Literal or not NameDelimiter) do
      begin
        Inc(CurPos);
        while (CurPos^ in LeadBytes) do Inc(CurPos, 2);
        CurChar := CurPos^;
        if IsLiteral then
        begin
          Literal := Literal xor True;
          if CurPos = StartPos + 1 then EmbeddedLiteral := True;
        end;
      end;
      CurPos^ := #0;
      if EmbeddedLiteral then
      begin
        Name := StripLiterals(StartPos + 1);
        EmbeddedLiteral := False;
      end
      else Name := StrPas(StartPos + 1);

      if DoCreate then
        Add(Name);

      CurPos^ := CurChar;
      StartPos^ := '?';
      Inc(StartPos);
      StrMove(StartPos, CurPos, StrLen(CurPos) + 1);
      CurPos := StartPos;
    end
    else if (CurChar = ':') and not Literal and ((CurPos + 1)^ = ':') then
      StrMove(CurPos, CurPos + 1, StrLen(CurPos) + 1)
    else if IsLiteral then Literal := Literal xor True;
    Inc(CurPos);
  until CurChar = #0;
end;

function TsbParams.GetParamByIndex(const Index: Integer): IrwParam;
begin
 Items[Index].GetInterface(IrwParam, Result);
end;

function TsbParams.GetParamByName(const AName: String): IrwParam;
begin
  ParamByName(AName).GetInterface(IrwParam, Result);
end;



{ TsbStream }

constructor TsbStream.Create(const AFileName: string; AMode: TsbStreamMode);
begin
  FFileName := AFileName;
  FMode := AMode;
end;



{ TsbMemoryStream }

constructor TsbMemoryStream.Create(const AFileName: string;  AMode: TsbStreamMode);
begin
  inherited;
  FStream := TIsMemoryStream.Create;
  FStream.LoadFromFile(AFileName);
  FStream.Position := 0;
  FSize := FStream.Size;
end;


destructor TsbMemoryStream.Destroy;
begin
  FreeAndNil(FStream);
  inherited;
end;


function TsbMemoryStream.GetPosition: Cardinal;
begin
  Result := FStream.Position;
end;


function TsbMemoryStream.GetSize: Cardinal;
begin
  Result := FSize;
end;


function TsbMemoryStream.Read(var Buffer; Count: Integer): Integer;
begin
  Result := FStream.Read(Pointer(Buffer)^, Count);
end;


procedure TsbMemoryStream.SetPosition(const Value: Cardinal);
begin
  FStream.Position := Value;
end;


function TsbMemoryStream.Write(const Buffer; Count: Integer): Integer;
begin
  Result := FStream.Write(Pointer(Buffer)^, Count);
end;



{ TsbDataSet }
constructor TsbDataSet.Create(AOwner: TComponent);
begin
  inherited;
  FTmpFields := TsbFields.Create; 
end;


destructor TsbDataSet.Destroy;
begin
  FreeAndNil(FTmpFields);
  inherited;
end;


function TsbDataSet.AllocRecordBuffer: PChar;
begin
  GetMem(Result, GetRecordSize);
  FillChar(Result^, GetRecordSize, 0);
  GetMem(PRecInfo(Result).Data, GetRecDataSize);
end;


function TsbDataSet.CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream;
var
  F: TsbField;
begin
  F := FCursor.Fields.FindField(Field.FieldName);
  Result := TsbBlobStream.Create(F);
  InternalSetToRecord(ActiveBuffer);
end;


procedure TsbDataSet.DataEvent(Event: TDataEvent; Info: Integer);
begin
  inherited;
  if Event = deDataSetScroll then
  begin
    if FCursor.RecNo = PRecInfo(ActiveBuffer).Bookmark.RecNo then
      FCursor.NotifyChange
    else
    begin
      FCursor.RecNo := PRecInfo(ActiveBuffer).Bookmark.RecNo;
      CursorPosChanged;
    end
  end

  else if Event = deDataSetChange then
    FCursor.NotifyChange;

{  else if Event = deFieldListChange then
  begin
    for i := 0 to Fields.Count - 1 do

    FTmpFields;
  end;}
end;



procedure TsbDataSet.FreeRecordBuffer(var Buffer: PChar);
begin
  FreeMem(PRecInfo(Buffer).Data, GetRecDataSize);
  FreeMem(Buffer, GetRecordSize);
end;


procedure TsbDataSet.GetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(PRecInfo(Buffer).Bookmark, Data^, SizeOf(PRecInfo(Buffer).Bookmark));
end;


function TsbDataSet.GetBookmarkFlag(Buffer: PChar): TBookmarkFlag;
begin
  Result := PRecInfo(ActiveBuffer).Bookmark.BookmarkFlag;
end;


function TsbDataSet.GetCanModify: Boolean;
begin
  Result := Assigned(FCursor) and FCursor.CanModify;
end;


procedure TsbDataSet.GetCurRecord(Buffer: Pointer);
begin
  with PRecInfo(Buffer)^ do
  begin
    Bookmark.RecNo := FCursor.RecNo;
    Bookmark.InternalPos := FCursor.InternalRecordPos;
    Bookmark.BookmarkFlag := bfCurrent;

    if not FCursor.IsEmptyCursor then
    begin
      FCursor.FStream.Position := FCursor.FCurrRecordPos;
      FCursor.FStream.Read(Data, GetRecDataSize);
    end;
  end;
end;


function TsbDataSet.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
var
  t: TDateTimeRec;
  F: TsbField;
  D: Double;
  B: WordBool;
begin
  if RecNo <= 0 then
  begin
    Result := False;
    Exit;
  end;  


  FTmpFields.FCursor := FCursor;
  try
    F := FTmpFields.FindField(Field.FieldName);
    if not FCursor.IsEmptyCursor then
    begin
      F.ReadFieldFromBuffer(PRecInfo(ActiveBuffer).Data);
      F.FInitialized := True;
    end
    else
      F.FInitialized := False;

    if F.IsNull then
    begin
      Result := False;
      Exit;
    end
    else
      Result := True;

    if not Assigned(Buffer) then
      Exit;

    case Field.DataType of
      ftString:   StrCopy(Buffer, PChar(F.AsString));

      ftInteger:  Move(F.FPValRec^.IntegerVal, Buffer^, SizeOf(F.FPValRec^.IntegerVal));

      ftBytes:    Move(F.FPValRec^.ByteVal, Buffer^, SizeOf(F.FPValRec^.ByteVal));

      ftDateTime: begin
                    DataConvert(Field, Addr(F.FPValRec^.DateTimeVal), Addr(t), True);
                    Move(t, Buffer^, SizeOf(t));
                  end;

      ftFloat,
      ftBCD:      Move(F.FPValRec^.FloatVal, Buffer^, SizeOf(F.FPValRec^.FloatVal));

      ftCurrency: begin
                    D := F.FPValRec^.CurrencyVal;
                    Move(D, Buffer^, SizeOf(D));
                  end;

      ftBoolean:  begin
                    B := F.FPValRec^.BooleanVal;
                    Move(B, Buffer^, SizeOf(B));
                  end;

      ftBLOB:     ;
    end;

  finally
    FTmpFields.FCursor := nil;
  end;
end;


function TsbDataSet.GetRecDataSize: Word;
begin
  Result := FCursor.FRecordSize;
end;


function TsbDataSet.GetRecNo: Integer;
begin
  Result := PRecInfo(ActiveBuffer)^.Bookmark.RecNo;
end;


function TsbDataSet.GetRecord(Buffer: PChar; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
begin
  FCursor.DisableNotification;

  try
    Result := grOK;
    if FCursor.OuterPosition then
      FCursor.OuterPosition := False

    else
    begin
      case GetMode of
        gmNext:  FCursor.Next;
        gmPrior: FCursor.Prior;
      end;

      if FCursor.BOF then
        Result := grBOF
      else if FCursor.EOF then
        Result := grEOF
      else
        Result := grOK;
    end;

    if Result = grOK  then
      GetCurRecord(Buffer);

  finally
    FCursor.EnableNotification;
  end;
end;


function TsbDataSet.GetRecordCount: Integer;
begin
  Result := FCursor.RecordCount;
end;


function TsbDataSet.GetRecordSize: Word;
begin
  Result := SizeOf(TRecInfo);
end;


procedure TsbDataSet.InternalAddRecord(Buffer: Pointer; Append: Boolean);
begin
end;


procedure TsbDataSet.InternalCancel;
begin
  FCursor.Cancel;
  if FCursor.RecNo = 1 then
    InternalFirst
  else if FCursor.RecNo = FCursor.RecordCount then
    InternalLast;
end;


procedure TsbDataSet.InternalClose;
begin
  FCursor.Close;
  BindFields(False);
  if DefaultFields then
    DestroyFields;
end;


procedure TsbDataSet.InternalEdit;
begin
  UpdateCursorPos;
  FCursor.Edit;
end;

procedure TsbDataSet.InternalFirst;
begin
  FCursor.First;
  FCursor.OuterPosition := True;
end;


procedure TsbDataSet.InternalGotoBookmark(Bookmark: Pointer);
var
  R: Cardinal;
begin
  R := FCursor.ValidInternalRecordPos(PsbBookmark(Bookmark)^.InternalPos);
  if R > 0 then
  begin
    FCursor.DisableNotification;
    FCursor.InternalRecordPos := PsbBookmark(Bookmark)^.InternalPos;
    FCursor.FRecNo := R;
    FCursor.EnableNotification;
  end
  else
  begin
    FCursor.DisableNotification;
    FCursor.RecNo := 1;
    FCursor.EnableNotification;
  end;
end;


procedure TsbDataSet.InternalInitFieldDefs;
begin
  sbCreateDefFields(Self, FCursor);
end;


procedure TsbDataSet.InternalInitRecord(Buffer: PChar);
var
  P: Pointer;
begin
  FillChar(PRecInfo(Buffer).Data^, GetRecDataSize, 0);
  P := Pointer(Cardinal(PRecInfo(Buffer).Data) + SizeOf(Integer)*2);
  FillChar(P^, FCursor.FNullInfoSize, $FF);
end;

procedure TsbDataSet.InternalInsert;
begin
  UpdateCursorPos;
  if PRecInfo(ActiveBuffer)^.Bookmark.BookmarkFlag = bfEOF then
    FCursor.Append
  else
    FCursor.Insert;
end;


procedure TsbDataSet.InternalLast;
begin
  FCursor.Last;
  FCursor.OuterPosition := True;
end;


procedure TsbDataSet.InternalOpen;
begin
  if not FCursor.Active then
    FCursor.Open;
  FTmpFields.Assign(FCursor.Fields);
  BookmarkSize := SizeOf(TsbBookmark);
  InternalFirst;
  InternalInitFieldDefs;
  if DefaultFields then
    CreateFields;
  BindFields(True);
end;


procedure TsbDataSet.InternalPost;
begin
  FCursor.Post;
  FCursor.OuterPosition := True;
end;


procedure TsbDataSet.InternalRefresh;
var
  n: Cardinal;
begin
  n := FCursor.RecNo;
  FCursor.DisableNotification;
  try
    FCursor.Close;
    FCursor.Open;
    InternalFirst;
    FCursor.RecNo := n;
    CursorPosChanged;
  finally
    FCursor.EnableNotification;
  end;
  FCursor.NotifyChange;
end;


procedure TsbDataSet.InternalSetToRecord(Buffer: PChar);
begin
  FCursor.DisableNotification;
  try
    if FCursor.RecNo <> PRecInfo(Buffer).Bookmark.RecNo then
      FCursor.RecNo := PRecInfo(Buffer).Bookmark.RecNo;
    CursorPosChanged;
  finally
    FCursor.EnableNotification;
  end
end;


function TsbDataSet.IsCursorOpen: Boolean;
begin
  Result := Assigned(FCursor) and FCursor.Active;
end;


procedure TsbDataSet.SetBookmarkData(Buffer: PChar; Data: Pointer);
begin
  Move(Data^, PRecInfo(Buffer).Bookmark, SizeOf(PRecInfo(Buffer).Bookmark));
end;


procedure TsbDataSet.SetBookmarkFlag(Buffer: PChar; Value: TBookmarkFlag);
begin
  PRecInfo(Buffer).Bookmark.BookmarkFlag := Value;
end;

procedure TsbDataSet.SetCursor(const Value: TsbTableCursor);
begin
  if FCursor <> Value then
    Close;
  FCursor := Value;
  if  Assigned(FCursor) and FCursor.Active then
    Open;
end;


procedure TsbDataSet.SetFieldData(Field: TField; Buffer: Pointer);
var
  d: TDateTime;
  F: TsbField;
  S: ShortString;
  P: Pointer;
  C: Currency;
begin
  F := FCursor.Fields.FindField(Field.FieldName);

  P := PRecInfo(ActiveBuffer).Data;
  Inc(PChar(P), SizeOf(Integer)*2);

  if Assigned(Buffer) then
  begin
    SetNull(F.Index, PsbNullInfo(P), False);

    P := PRecInfo(ActiveBuffer).Data;
    Inc(PChar(P), F.FPValRec.RecOffset);

    case Field.DataType of
      ftString:   begin
                    S := ShortString(PChar(Buffer));
                    WriteValue(@S, P, F.FieldType, F.Size);
                  end;

      ftInteger,
      ftBytes,
      ftFloat,
      ftBCD,
      ftBoolean:  WriteValue(Buffer, P, F.FieldType);

      ftCurrency: begin
                    C := Double(P^);
                    WriteValue(Buffer, Addr(C), F.FieldType);
                  end;

      ftDateTime: begin
                    DataConvert(Field, Buffer, Addr(d), False);
                    WriteValue(@d, P, F.FieldType);
                  end;

      ftBLOB:     ;
    end;

    F.ReadFieldFromBuffer(PRecInfo(ActiveBuffer)^.Data);
  end

  else
    SetNull(F.Index, PsbNullInfo(P), True);

  F.FInitialized := True;
  F.FModified := True;
  F.FOwner.AddNullInfo(F.Index, not Assigned(Buffer));
  F.NotifySetFieldValue;

  DataEvent(deFieldChange, LongInt(Field));
end;


procedure TsbDataSet.SetRecNo(Value: Integer);
begin
  CheckBrowseMode;
  FCursor.RecNo := Value;
  SyncPosition;
end;


procedure TsbDataSet.InternalDelete;
begin
  FCursor.Delete;
  FCursor.OuterPosition := True;
end;


procedure TsbDataSet.InternalHandleException;
begin
end;


function TsbDataSet.GetNextRecords: Integer;
var
  b,e: Boolean;
  r: Cardinal;
begin
  b := FCursor.BOF;
  e := FCursor.EOF;
  r := FCursor.RecNo;
//  FCursor.StoreState;
  Result := inherited GetNextRecords;
//  FCursor.RestoreState;
  FCursor.DisableNotification;
  FCursor.RecNo := r;
  FCursor.EnableNotification;
  FCursor.FBOF := b;
  FCursor.FEOF := e;
  CursorPosChanged;
end;


function TsbDataSet.GetPriorRecords: Integer;
var
  b,e: Boolean;
  r: Cardinal;
begin
  b := FCursor.BOF;
  e := FCursor.EOF;
  r := FCursor.RecNo;
//  FCursor.StoreState;
  Result := inherited GetPriorRecords;
//  FCursor.RestoreState;
  FCursor.DisableNotification;
  FCursor.RecNo := r;
  FCursor.EnableNotification;
  FCursor.FBOF := b;
  FCursor.FEOF := e;
  CursorPosChanged;
end;

procedure TsbDataSet.SyncPosition;
begin
  DisableControls;
  FCursor.DisableNotification;
  CursorPosChanged;
  if FCursor.EOF then
    Last
  else if FCursor.BOF then
    First
  else
    MoveBy(FCursor.RecNo - RecNo);
  FCursor.EnableNotification;
  EnableControls;
end;

procedure TsbDataSet.Resync(Mode: TResyncMode);
begin
  FCursor.FOuterPosition := True;
  FCursor.StoreState;
  inherited;
  FCursor.RestoreState;
end;


procedure TsbDataSet.SetFiltered(Value: Boolean);
begin
  inherited;
  FCursor.Filtered := Value;
  if Active then
    First;
end;


procedure TsbDataSet.SetFilterText(const Value: string);
begin
  inherited;
  FCursor.Filter := Value;
end;


function TsbDataSet.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
var
  lFields: array of string;
  lValues: array of Variant;
  h: String;
  i: Integer;
begin
  h := KeyFields;
  while Length(h) > 0 do
  begin
    SetLength(lFields, Length(lFields) + 1);
    i := Pos(';', h);
    if i > 0 then
    begin
      lFields[High(lFields)] := Trim(Copy(h, 1, i - 1));
      System.Delete(h, 1, i);
    end

    else
    begin
      lFields[High(lFields)] := Trim(h);
      h := '';
    end;
  end;

  if varIsArray(KeyValues) then
  begin
    SetLength(lValues, VarArrayHighBound(KeyValues, 0) - VarArrayLowBound(KeyValues, 0));
    for i := VarArrayLowBound(KeyValues, 0) to VarArrayHighBound(KeyValues, 0) do
      lValues[i] := KeyValues[i];
  end

  else
  begin
    SetLength(lValues, 1);
    lValues[0] := KeyValues;
  end;

  DoBeforeScroll;
  Result := FCursor.Locate(lFields, lValues, not (loCaseInsensitive in Options), loPartialKey in Options);
  if Result then
  begin
    Resync([rmExact, rmCenter]);
    DoAfterScroll;
  end;
end;


function TsbDataSet.CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer;
begin
  if PsbBookmark(Bookmark1).InternalPos = PsbBookmark(Bookmark2).InternalPos then
    Result := 0
  else if PsbBookmark(Bookmark1).RecNo < PsbBookmark(Bookmark2).RecNo then
    Result := -1
  else
    Result := 1;
end;


function TsbDataSet.BookmarkValid(Bookmark: TBookmark): Boolean;
begin
  Result :=  FCursor.ValidInternalRecordPos(PsbBookmark(Bookmark)^.InternalPos) > 0;
end;


procedure TsbDataSet.SetOnFilterRecord(const Value: TFilterRecordEvent);
begin
  inherited;
end;



{ TsbBlobStream }

constructor TsbBlobStream.Create(Field: TsbField);
begin
  FField := Field;
  FPosition := 0;
end;


function TsbBlobStream.Read(var Buffer; Count: Integer): Longint;
begin
  Result := Length(FField.AsString);
  Move(Pointer(FField.AsString)^, Buffer, Result);
end;


function TsbBlobStream.Seek(Offset: Integer; Origin: Word): Longint;
begin
  case Origin of
    0: FPosition := Offset;
    1: Inc(FPosition, Offset);
    2: FPosition :=  Length(FField.AsString) + Offset;
  end;
  Result := FPosition;
end;


function TsbBlobStream.Write(const Buffer; Count: Integer): Longint;
var
  h: string;
begin
  SetLength(h, Count);
  Move(Buffer, Pointer(h)^, Count);
  FField.AsString := h;
  Result := Count;
end;


{ TsbIndexPool }

constructor TsbIndexPool.Create;
begin
  FList := TStringList.Create;
end;


destructor TsbIndexPool.Destroy;
var
  i, j: Integer;
  h: String;
begin
  try
    for i := 0 to FList.Count - 1 do
    begin
      j := Pos('=', FList[i]);
      h := Copy(FList[i], j + 1, Length(FList[i])- j);
      DeleteFile(h);
    end;

    FreeAndNil(FList);
  finally
    inherited;
  end;
end;


{ TsbParam }

function TsbParam.GetName: String;
begin
  Result := FName;
end;

function TsbParam.GetValue: Variant;
begin
  Result := FValue;
end;

procedure TsbParam.SetName(const AName: String);
begin
  FName := AName;
end;

procedure TsbParam.SetValue(const AValue: Variant);
begin
  FValue := AValue;
end;

{ TsbInterfacedObject }

function TsbInterfacedObject._AddRef: Integer;
begin
  Result := -1;
end;

function TsbInterfacedObject._Release: Integer;
begin
  Result := -1;
end;

function TsbInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

end.
