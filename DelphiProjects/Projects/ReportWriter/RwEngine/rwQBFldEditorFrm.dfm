object rwQBFldEditor: TrwQBFldEditor
  Left = 502
  Top = 337
  Width = 395
  Height = 223
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Showing Field Editor'
  Color = clBtnFace
  Constraints.MinHeight = 127
  Constraints.MinWidth = 352
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    387
    196)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTmpFld: TPanel
    Left = 2
    Top = 136
    Width = 382
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    DesignSize = (
      382
      21)
    object Label5: TLabel
      Left = 0
      Top = 3
      Width = 78
      Height = 13
      Caption = 'Destination Field'
    end
    object cbTmpFlds: TComboBox
      Left = 86
      Top = 0
      Width = 297
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTmpFldsChange
    end
  end
  inline pnlField: TrwQBExpress
    Left = 2
    Top = 29
    Width = 383
    Height = 135
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoScroll = False
    TabOrder = 2
    inherited pnlExpr: TrwQBExprPanel
      Width = 383
      Height = 120
      PopupMenu = pnlField.pnlExpr.pmClipbrd
      inherited pnlCanvas: TPanel
        Width = 383
        Height = 120
      end
    end
  end
  object btnCancel: TButton
    Left = 319
    Top = 170
    Width = 65
    Height = 23
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 8
  end
  object btnOK: TButton
    Left = 244
    Top = 170
    Width = 65
    Height = 23
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 7
  end
  object pnlAlias: TPanel
    Left = 2
    Top = 2
    Width = 382
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      382
      21)
    object lAlias: TLabel
      Left = 0
      Top = 3
      Width = 47
      Height = 13
      Caption = 'Field Alias'
    end
    object edtAlias: TEdit
      Left = 53
      Top = 0
      Width = 329
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnExit = edtAliasExit
    end
  end
  object btnSUM: TButton
    Left = 2
    Top = 170
    Width = 50
    Height = 23
    Anchors = [akLeft, akBottom]
    Caption = 'SUM'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btnSUMClick
  end
  object btnCOUNT: TButton
    Left = 52
    Top = 170
    Width = 50
    Height = 23
    Anchors = [akLeft, akBottom]
    Caption = 'COUNT'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = btnCOUNTClick
  end
  object btnMIN: TButton
    Left = 102
    Top = 170
    Width = 50
    Height = 23
    Anchors = [akLeft, akBottom]
    Caption = 'MIN'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = btnMINClick
  end
  object btnMAX: TButton
    Left = 152
    Top = 170
    Width = 50
    Height = 23
    Anchors = [akLeft, akBottom]
    Caption = 'MAX'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btnMAXClick
  end
end
