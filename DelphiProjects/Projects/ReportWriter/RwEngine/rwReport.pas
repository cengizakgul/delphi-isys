unit rwReport;

interface

uses Classes, Math, TypInfo, SysUtils, Controls, Windows, ExtCtrls, Dialogs,
     Graphics, Forms, Variants, BlockCiphers, AbZipPrc, AbUnzPrc, EvStreamUtils,
     rwBasicClasses, rwCommonClasses, rwTypes, rwExtendedControls, rwDB, sbSQL, rwLogQuery,
     rwEngineTypes, rmTypes;

type

  {TrwPage store information about page property of report}

  TrwPage = class(TPersistent)
  private
    FPaperSize: TrwPaperSize;
    FPaperOrientation: TrwPaperOrientation;
    FHeight: Integer;
    FWidth: Integer;
    FOnChangePageFormat: TNotifyEvent;
    FTopMargin: Integer;
    FBottomMargin: Integer;
    FLeftMargin: Integer;
    FRightMargin: Integer;
    FDuplexing: Boolean;

    procedure SetPaperSize(Value: TrwPaperSize);
    procedure SetPaperOrientation(Value: TrwPaperOrientation);
    procedure SetHeight(Value: Integer);
    procedure SetWidth(Value: Integer);
    procedure SetMargin(Index: Integer; Value: Integer);
    function  IsNotStandardSize: Boolean;

  public
    property OnChangePageFormat: TNotifyEvent read FOnChangePageFormat write FOnChangePageFormat;

    procedure Assign(Source: TPersistent); override;

  published
    property PaperSize: TrwPaperSize read FPaperSize write SetPaperSize;
    property PaperOrientation: TrwPaperOrientation read FPaperOrientation write SetPaperOrientation;
    property Height: Integer read FHeight write SetHeight stored IsNotStandardSize;
    property Width: Integer read FWidth write SetWidth stored IsNotStandardSize;
    property TopMargin: Integer index 0 read FTopMargin write SetMargin;
    property BottomMargin: Integer index 1 read FBottomMargin write SetMargin;
    property LeftMargin: Integer index 2 read FLeftMargin write SetMargin;
    property RightMargin: Integer index 3 read FRightMargin write SetMargin;
    property Duplexing: Boolean read FDuplexing write FDuplexing;
  end;

  TrwReportForm = class;

  {TrwGroup is class for grouping}

  TrwGroup = class(TrwCollectionItem)
  private
    FFieldName: string;
    FGroupHeader: TrwBand;
    FGroupFooter: TrwBand;
    FGroupHeaderVisible: Boolean;
    FGroupFooterVisible: Boolean;
    FDisabled: Boolean;

    function GetGroupVisible(Index: Integer): Boolean;
    procedure SetGroupVisible(Index: Integer; Value: Boolean);

  public
    LastValue: Variant;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

  published
    property FieldName: string read FFieldName write FFieldName;
    property GroupHeader: TrwBand read FGroupHeader write FGroupHeader;
    property GroupFooter: TrwBand read FGroupFooter write FGroupFooter;
    property GroupHeaderVisible: Boolean index 0 read GetGroupVisible write SetGroupVisible;
    property GroupFooterVisible: Boolean index 1 read GetGroupVisible write SetGroupVisible;
    property Disabled: Boolean read FDisabled write FDisabled;
  end;

  {TrwGroups is class-collection of groups}

  TrwGroups = class(TrwCollection)
  private
    function GetItem(Index: Integer): TrwGroup;
    procedure SetItem(Index: Integer; Value: TrwGroup);

  public
    property Items[Index: Integer]: TrwGroup read GetItem write SetItem; default;
    procedure Assign(Source: TPersistent); override;
    function  IndexOfGroup(AFieldName: String): Integer;
  end;

  {TrwASCIIBand is class for store information during ASCII-printing process}

  TrwASCIIBand = class
  private
    FLines: TList;
    function GetLine(Index: Integer): string;
    function GetLinesCount: Integer;

  public
    property LinesCount: Integer read GetLinesCount;
    property Lines[Index: Integer]: string read GetLine;

    constructor Create;
    destructor Destroy; override;
    procedure PrinText(ALine: Integer; ACol: Integer; AText: string);
    procedure Clear;
  end;

  {TrwReportForm is contents of Report}

  TrwChangeComponentList = procedure(Sender: TObject; AComponent: TComponent; Operation: TOperation) of object;

  TrwReportForm = class(TrwComponent)
  private
    FBands: TList;
    FGroupsWork: TrwGroups;
    FGroupsRead: TrwGroups;
    FOnChangeComponentList: TrwChangeComponentList;
    FASCIIBand: TrwASCIIBand;
    FDesignParent: TWinControl;

    function  GetBand(Index: Integer): TrwBand;
    function  GetBandCount: Integer;
    procedure SetGroups(Value: TrwGroups);
    procedure AssignUniqueName;
    function  GetGroups: TrwGroups;
    procedure AlignBands;

  protected
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    function  GetDesignParent: TWinControl; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure SetInheritedComponentProperty(AValue: Boolean); override;
    procedure Loaded; override;

  public
    function IsDesignMode: Boolean; override;
    property Bands[Index: Integer]: TrwBand read GetBand;
    property BandCount: Integer read GetBandCount;
    property OnChangeComponentList: TrwChangeComponentList read FOnChangeComponentList write FOnChangeComponentList;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    CreateBand(ABandType: TrwBandType): TrwBand;
    function    AddBand(ABandType: TrwBandType): Integer;
    procedure   DeleteBand(ABandType: TrwBandType); overload;
    procedure   DeleteBand(ABand: TrwBand); overload;
    function    IndexOfBand(ABandType: TrwBandType): Integer;
    procedure   InsertGroup(Index: Integer);
    function    ASCIIBand: TrwASCIIBand;

  published
    property Groups: TrwGroups read GetGroups write SetGroups;
  end;

  {TrwBands is additional class for editing Bands}

  TrwBands = class(TPersistent)
  private
    FReportForm: TrwReportForm;
    FPageHeader: Boolean;
    FTitle: Boolean;
    FHeader: Boolean;
    FFooter: Boolean;
    FSummary: Boolean;
    FPageFooter: Boolean;
    FPageStyle: Boolean;
    FGroups: TrwGroups;
    FSubSummary: Boolean;
    FSubDetail: Boolean;
    FCustom: Boolean;

    procedure SetBand(Index: TrwBandType; Value: Boolean);

  public
    property PageHeader: Boolean index rbtPageHeader read FPageHeader write SetBand;
    property Title: Boolean index rbtTitle read FTitle write SetBand;
    property Header: Boolean index rbtHeader read FHeader write SetBand;
    property Footer: Boolean index rbtFooter read FFooter write SetBand;
    property Summary: Boolean index rbtSummary read FSummary write SetBand;
    property PageFooter: Boolean index rbtPageFooter read FPageFooter write SetBand;
    property PageStyle: Boolean index rbtPageStyle read FPageStyle write SetBand;
    property Groups: TrwGroups read FGroups;
    property SubDetail: Boolean index rbtSubDetail read FSubDetail write SetBand;
    property SubSummary: Boolean index rbtSubSummary read FSubSummary write SetBand;
    property Custom: Boolean index rbtCustom read FCustom write SetBand;

    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  end;


  {TrwInputFormContainer is container for elements of Input Form}

  TrwInputFormContainer = class(TrwWinFormControl)
  private
    FCloseResult: TModalResult;
    FShownOnForm: Boolean;
    FRunTimeContainer: TWinControl;

    procedure HideRunTimeOnly(AOwner: TrwComponent);

  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure SetParentComponent(Value: TComponent); override;
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure RegisterComponentEvents; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure SetDesigner(ADesigner: IrmDesigner); override;

  public
    FRunTimeMode: Boolean;

    constructor Create(AOwner: TComponent); override;
    function  IsDesignMode: Boolean; override;
    procedure CompileChildren(ASyntaxCheck: Boolean = False); override;

    procedure HideRunTimeControls;
    procedure SetParentForm(Value: TWinControl);  //show Input Form
    procedure ShowInRunTime(const AParent: HWND);
    procedure CloseInRunTime;
    procedure DoAfterCreate;
    function  DoCloseQuery: Boolean;
    procedure DoClose;
    procedure SetInputFormParams;
    procedure GetInputFormParams;

  published
    property Width;
    property Height;
    property Color;
    property CloseResult: TModalResult read FCloseResult write FCloseResult stored False;

    function PRunTimeMode: Variant;
  end;

   {TrwCustomReport is general Report class}

  TrwCustomReport = class(TrwNoVisualComponent)
  private
    FReportForm: TrwReportForm;
    FPageFormat: TrwPage;
    FOnChangePageFormat: TNotifyEvent;
    FMasterDataSource: TrwDataSet;
    FDetailDataSource: TrwDataSet;
    FBands: TrwBands;
    FRendering: TObject;
    FOnDestroy: TNotifyEvent;
    FOnChangeName: TNotifyEvent;
    FStandAlone: Boolean;

    procedure ChangedPageFormat(Sender: TObject);
    procedure SetPageFormat(Value: TrwPage);
    function  GetBands: TrwBands;
    procedure SetMasterDataSource(const Value: TrwDataSet);
    procedure SetDetailDataSource(const Value: TrwDataSet);

  protected
    procedure RegisterComponentEvents; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure SetName(const NewName: TComponentName); override;
    procedure ReadState(Reader: TReader); override;
    procedure Loaded; override;

  public
    property ReportForm: TrwReportForm read FReportForm;
    property Rendering: TObject read FRendering;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;
    function IsDesignMode: Boolean; override;
    procedure BeforeDestruction; override;
    procedure CompileChildren(ASyntaxCheck: Boolean = False); override;
    property OnChangePageFormat: TNotifyEvent read FOnChangePageFormat write FOnChangePageFormat;
    property OnDestroy: TNotifyEvent read FOnDestroy write FOnDestroy;
    property OnChangeName: TNotifyEvent read FOnChangeName write FOnChangeName;
    property StandAlone: Boolean read FStandAlone write FStandAlone;

    function  FindVirtualChild(const AName: string): Integer; override;
    function  VirtualComponentCount: Integer; override;
    function  GetVirtualChild(const AIndex: Integer): TComponent; override;
    procedure InsertVirtualComponent(AComponent: TrwComponent); override;

  published
    property PageFormat: TrwPage read FPageFormat write SetPageFormat;
    property MasterDataSource: TrwDataSet read FMasterDataSource write SetMasterDataSource;
    property DetailDataSource: TrwDataSet read FDetailDataSource write SetDetailDataSource;
    property Bands: TrwBands read GetBands;

    function  PGroupByField(AParam: Variant): Variant;
  end;


  TrwReport = class;

  TrwAfterPrintPageEvent = procedure (APage: TObject) of object;


  TrwSecureWrapper = class(TComponent)
  private
    FLicence: String;
    FReport:  TrwReport;

    procedure WriteData(AStream: TStream);
    procedure ReadData(AStream: TStream);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  published
    property Licence: String read FLicence write FLicence;
  end;


   {TrwReport is Abstract Head Report}

  TrwReport = class(TrwCustomReport, IrmReportObject)
  private
    FInputForm: TrwInputFormContainer;
    FASCIIDelimiter: String;
    FWizardInfo: IEvDualStream;
    FDescription: WideString;
    FASCIIFileName: string;
    FReportType: TReportType;
    FReportName: string;
    FResultFileName: String;
    FCompiledCode: TrwCode;
    FDebInf: TrwDebInf;
    FDebSymbInf: TrwDebInf;
    FLibVersion: TDateTime;
    FDuplexing: Boolean;
    FOnAfterPrintPage: TrwAfterPrintPageEvent;
    FSecurityInfo: TrwSecureWrapper;
    FASCIILastLineBreak: boolean;

    procedure ReaderError(Reader: TReader; const Message: string; var Handled: Boolean);
    procedure ReaderSetName(Reader: TReader; Component: TComponent; var Name: string);
    procedure WriteWizardInfo(Stream: TStream); virtual;
    procedure ReadWizardInfo(Stream: TStream); virtual;
    procedure WriteCompiledCode(Stream: TStream);
    procedure ReadCompiledCode(Stream: TStream);
    procedure WriteLibVersion(Writer: TWriter);
    procedure ReadLibVersion(Reader: TReader);
    function  GetSecurityInfo: TrwSecureWrapper;
    procedure ReadReport(AStream: TStream);
    procedure WriteReport(AStream: TStream);
    function GetWizardInfo: IEvDualStream;

  protected
    procedure ReadState(Reader: TReader); override;
    procedure Loaded; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure AfterInitLibComponent; override;

    function  GetCompiledCode: PTrwCode;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property  InputForm: TrwInputFormContainer read FInputForm;
    property  WizardInfo: IEvDualStream read GetWizardInfo;
    property  LibVersion: TDateTime read FLibVersion;
    property  OnAfterPrintPage: TrwAfterPrintPageEvent read FOnAfterPrintPage write FOnAfterPrintPage;
    property  SecurityInfo: TrwSecureWrapper read GetSecurityInfo;

    procedure SetReportParams(const AParams: PTrwReportParamList);
    procedure GetReportParams(const AParams: PTrwReportParamList);

    function  InputParams: Boolean;
    function  ShowInputForm(AParent: HWND; ARunTime: Boolean = True): Boolean;
    procedure CloseInputForm;
    function  Print(AResultFileName: string; AShowInputForm: Boolean = False): Boolean;
    procedure Prepare;
    procedure LoadFromFile(const AFileName: string);
    procedure SaveToFile(const AFileName: string);
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
    property  ResultFileName: string read FResultFileName;
    property  ASCIIFileName: string  read FASCIIFileName write FASCIIFileName;
    property  ReportName: string read FReportName write FReportName;
    property  Duplexing: Boolean read FDuplexing write FDuplexing;
    property  ReportType: TReportType read FReportType write FReportType;
    property  CompiledCode: TrwCode read FCompiledCode;
    property  DebInf: TrwDebInf read FDebInf;
    property  DebSymbInf: TrwDebInf read FDebSymbInf;
    procedure CompileChildren(ASyntaxCheck: Boolean = False); override;
    procedure Compile(ASyntaxCheck: Boolean = False); override;
    procedure ClearCompiledInfo;
    procedure BeginWork;
    procedure EndWork;
    function  IsLicenced: Boolean;

  published
    property ASCIIDelimiter: string read FASCIIDelimiter write FASCIIDelimiter;
    property ASCIILastLineBreak: boolean read FASCIILastLineBreak write FASCIILastLineBreak default True;
    property Description: WideString read FDescription write FDescription;
  end;

   {TrwSubReport is embedded Report into another Report}

  TrwSubReport = class(TrwCustomReport)
  protected
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure Loaded; override;

  public
    procedure Print(Sender: TObject); reintroduce;
    constructor Create(AOwner: TComponent); override;

  published
    property Left;
    property Top;
    property StandAlone;

    procedure PPrint;
  end;

  procedure ClearTempTables(AOwner: TrwComponent);

implementation

uses
  rwEngine, rwRendering, rwParser, rwVirtualMachine, rwUtils, rwInputParamsFormFrm, rwInputFormControls,
  rwReportControls, rwDsgnControls, rmReport;


procedure ClearTempTables(AOwner: TrwComponent);
var
  i: Integer;
begin
  for i := 0 to AOwner.ComponentCount - 1 do
  begin
    if AOwner.Components[i] is TrwBuffer then
    begin
      TrwBuffer(AOwner.Components[i]).PDisableControls;
      try
        TrwBuffer(AOwner.Components[i]).PClear;
      finally
        TrwBuffer(AOwner.Components[i]).PEnableControls;
      end;
    end;

    if (AOwner.Components[i] is TrwComponent) and TrwComponent(AOwner.Components[i]).IsLibComponent then
      ClearTempTables(TrwComponent(AOwner.Components[i]));
  end;
end;

  {TrwPage}


procedure TrwPage.SetPaperSize(Value: TrwPaperSize);
begin
  FPaperSize := Value;
  FWidth := cPaperInfo[FPaperSize].Width;
  FHeight := cPaperInfo[FPaperSize].Height;

  if Assigned(FOnChangePageFormat) then
    FOnChangePageFormat(Self);
end;

procedure TrwPage.SetPaperOrientation(Value: TrwPaperOrientation);
var
  h: Integer;
begin
  FPaperOrientation := Value;

  if (FPaperOrientation = rpoPortrait) then
  begin
    h := Min(FWidth, FHeight);
    FHeight := Max(FWidth, FHeight);
    FWidth := h;
  end
  else
  begin
    h := Max(FWidth, FHeight);
    FHeight := Min(FWidth, FHeight);
    FWidth := h;
  end;

  if Assigned(FOnChangePageFormat) then
    FOnChangePageFormat(Self);
end;

procedure TrwPage.SetHeight(Value: Integer);
begin
  if (FPaperSize = psCustom) then
  begin
    FHeight := Value;
    if Assigned(FOnChangePageFormat) then
      FOnChangePageFormat(Self);
  end;
end;

procedure TrwPage.SetWidth(Value: Integer);
begin
  if (FPaperSize = psCustom) then
  begin
    FWidth := Value;
    if Assigned(FOnChangePageFormat) then
      FOnChangePageFormat(Self);
  end;
end;

procedure TrwPage.SetMargin(Index: Integer; Value: Integer);
begin
  case Index of
    0: FTopMargin := Value;
    1: FBottomMargin := Value;
    2: FLeftMargin := Value;
    3: FRightMargin := Value;
  end;

  if Assigned(FOnChangePageFormat) then
    FOnChangePageFormat(Self);
end;

procedure TrwPage.Assign(Source: TPersistent);
begin
  PaperSize := TrwPage(Source).PaperSize;
  Height := TrwPage(Source).Height;
  Width := TrwPage(Source).Width;
  PaperOrientation := TrwPage(Source).PaperOrientation;
  TopMargin := TrwPage(Source).TopMargin;
  BottomMargin := TrwPage(Source).BottomMargin;
  LeftMargin := TrwPage(Source).LeftMargin;
  RightMargin := TrwPage(Source).RightMargin;
  Duplexing := TrwPage(Source).Duplexing;
end;

function TrwPage.IsNotStandardSize: Boolean;
begin
  Result := PaperSize = psCustom; 
end;

{TrwGroup}

constructor TrwGroup.Create(Collection: TCollection);
begin
  inherited Create(Collection);

  FGroupHeaderVisible := False;
  FGroupFooterVisible := False;
  FDisabled := False;
end;

destructor TrwGroup.Destroy;
begin
  if Assigned(FGroupHeader) then
    if not (csDestroying in FGroupHeader.ComponentState) then
      FGroupHeader.Free;

  if Assigned(FGroupFooter) then
    if not (csDestroying in FGroupFooter.ComponentState) then
      FGroupFooter.Free;

  inherited;
end;

procedure TrwGroup.Assign(Source: TPersistent);
begin
  inherited;
  FieldName := TrwGroup(Source).FieldName;
  Disabled := TrwGroup(Source).Disabled;
  DisplayName := FieldName;
  if TrwGroups(Collection).Owner <> nil then
  begin
    if Assigned(TrwGroup(Source).FGroupHeader) then
      FGroupHeader := TrwGroup(Source).FGroupHeader;
    if Assigned(TrwGroup(Source).FGroupFooter) then
      FGroupFooter := TrwGroup(Source).FGroupFooter;
  end;    
  GroupHeaderVisible := TrwGroup(Source).GroupHeaderVisible;
  GroupFooterVisible := TrwGroup(Source).GroupFooterVisible;
end;

function TrwGroup.GetGroupVisible(Index: Integer): Boolean;
begin
  case Index of
    0: Result := FGroupHeaderVisible;
    1: Result := FGroupFooterVisible;
  else
    Result := False;
  end;
end;

procedure TrwGroup.SetGroupVisible(Index: Integer; Value: Boolean);
begin
  case Index of
    0:
      begin
        if (TrwGroups(Collection).Owner <> nil) and
          not (csLoading in TrwComponent(TrwGroups(Collection).Owner).ComponentState) then
          if Value then
            if not Assigned(FGroupHeader) then
              FGroupHeader := TrwReportForm(TrwGroups(Collection).Owner).CreateBand(rbtGroupHeader)
            else

          else
          begin
            if Assigned(FGroupHeader) then
              TrwReportForm(TrwGroups(Collection).Owner).DeleteBand(FGroupHeader);
            FGroupHeader := nil;
          end;

        FGroupHeaderVisible := Value;
      end;

    1:
      begin
        if (TrwGroups(Collection).Owner <> nil) and
          not (csLoading in TrwComponent(TrwGroups(Collection).Owner).ComponentState) then
          if Value then
            if not Assigned(FGroupFooter) then
              FGroupFooter := TrwReportForm(TrwGroups(Collection).Owner).CreateBand(rbtGroupFooter)
            else

          else
          begin
            if Assigned(FGroupFooter) then
              TrwReportForm(TrwGroups(Collection).Owner).DeleteBand(FGroupFooter);
            FGroupFooter := nil;
          end;

        FGroupFooterVisible := Value;
      end;
  end;
end;


{TrwGroups}

procedure TrwGroups.Assign(Source: TPersistent);
var
  i, j, n: Integer;
  CI: TrwCollectionItem;
begin
  if (Owner <> nil) and (TrwComponent(Owner).InheritedComponent or TrwComponent(Owner).IsLibComponent) then
  begin
    i := 0;
    while i < Count do
      if not Items[i].InheritedItem then
        Delete(i)
      else
        Inc(i);

    for i := 0 to TrwCollection(Source).Count-1 do
    begin
      if not (TrwCollectionItem(TrwCollection(Source).Items[i]).InheritedItem) then
      begin
        CI := TrwCollectionItem(Add);
        CI.Assign(TrwCollection(Source).Items[i]);
      end
      else
      begin
        j := IndexOfGroup(TrwGroup(TrwCollection(Source).Items[i]).FieldName);
        if j <> -1 then
          Items[j].Disabled := TrwGroup(TrwCollection(Source).Items[i]).Disabled;
      end;
    end;

    n := 0;
    for i := 0 to TrwCollection(Source).Count-1 do
    begin
      j := IndexOfGroup(TrwGroups(Source)[i].FieldName);
      if j <> -1 then
      begin
        Items[j].Index := n;
        Inc(n);
      end;
    end;
  end

  else
    inherited;
end;

function TrwGroups.IndexOfGroup(AFieldName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AFieldName := AnsiUpperCase(AFieldName);
  for i := 0 to Count - 1 do
    if AnsiUpperCase(Items[i].FieldName) = AFieldName then
    begin
      Result := i;
      break;
    end;
end;

function TrwGroups.GetItem(Index: Integer): TrwGroup;
begin
  Result := TrwGroup(inherited Items[Index]);
end;

procedure TrwGroups.SetItem(Index: Integer; Value: TrwGroup);
begin
  inherited SetItem(Index, TCollectionItem(Value));
end;


  {TrwReportForm}

constructor TrwReportForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  AssignUniqueName;

  FBands := TList.Create;
  FGroupsWork := TrwGroups.Create(Self, TrwGroup);
  FGroupsRead := TrwGroups.Create(Self, TrwGroup);
  FASCIIBand := nil;
end;

destructor TrwReportForm.Destroy;
begin
  FreeAndNil(FGroupsWork);
  FreeAndNil(FGroupsRead);
  FreeAndNil(FBands);
  FreeAndNil(FASCIIBand);
  inherited;
end;

procedure TrwReportForm.AssignUniqueName;
begin
  if Name = '' then
    Name := 'RF' + IntToStr(LongInt(Pointer(Self)));
end;

function TrwReportForm.ASCIIBand: TrwASCIIBand;
begin
  if not Assigned(FASCIIBand) then
    FASCIIBand := TrwASCIIBand.Create;
  Result := FASCIIBand;
end;

function TrwReportForm.IsDesignMode: Boolean;
begin
  Result := (DesignParent <> nil);
end;

function TrwReportForm.GetDesignParent: TWinControl;
begin
  Result := FDesignParent;
end;

procedure TrwReportForm.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  FDesignParent := Value;

  if Assigned(DesignParent) then
    DesignParent.DisableAlign;

  for i := 0 to ComponentCount - 1 do
    if ((Components[i] is TrwNoVisualComponent) or (Components[i] is TrwBand)) then
      TrwComponent(Components[i]).DesignParent := DesignParent;

  if Assigned(DesignParent) then
  begin
    AlignBands;
    DesignParent.EnableAlign;
    DesignParent.Realign;
  end;
end;

procedure TrwReportForm.InsertGroup(Index: Integer);
var
  lGrp: TrwGroup;
begin
  lGrp := TrwGroup.Create(Groups);
  lGrp.Index := Index;
end;

function TrwReportForm.GetBand(Index: Integer): TrwBand;
begin
  Result := TrwBand(FBands[Index]);
end;

function TrwReportForm.IndexOfBand(ABandType: TrwBandType): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to FBands.Count - 1 do
    if Bands[i].BandType = ABandType then
    begin
      Result := i;
      break;
    end;
end;

function TrwReportForm.CreateBand(ABandType: TrwBandType): TrwBand;
var
  OrdTypeInfo: PTypeInfo;
  lTypeName: string;
begin
  Result := TrwBand.Create(Self);
  Result.BandType := ABandType;
  if Assigned(DesignParent) then
    Result.DesignParent := DesignParent;

  OrdTypeInfo := TypeInfo(TrwBandType);
  lTypeName := GetEnumName(OrdTypeInfo, LongInt(Result.BandType));
  Delete(lTypeName, 1, 3);

  if ABandType in [rbtGroupHeader, rbtGroupFooter] then
    Result.Name := GetUniqueNameForComponent(Result, True, lTypeName, '')

  else
    Result.Name := GetUniqueNameForComponent(Result, False, lTypeName, '');

  AlignBands;

  if Assigned(DesignParent) then
    SendMessage(DesignParent.Parent.Handle, RW_CHANGE_BAND_LIST, 1, LongInt(Pointer(Result)));
end;

function TrwReportForm.AddBand(ABandType: TrwBandType): Integer;
begin
  Result := IndexOfBand(ABandType);
  if Result <> -1 then
    Exit;
  CreateBand(ABandType);
  Result := FBands.Count - 1;
end;

procedure TrwReportForm.DeleteBand(ABandType: TrwBandType);
var
  i: Integer;
begin
  i := IndexOfBand(ABandType);
  if (i <> -1) then
  begin
    if Assigned(DesignParent) then
      SendMessage(DesignParent.Parent.Handle, RW_CHANGE_BAND_LIST, 0, LongInt(Pointer(Bands[i])));
    Bands[i].Free;
  end;
end;

procedure TrwReportForm.DeleteBand(ABand: TrwBand);
begin
  if Assigned(DesignParent) then
    SendMessage(DesignParent.Parent.Handle, RW_CHANGE_BAND_LIST, 0, LongInt(Pointer(ABand)));
  ABand.Free;
end;

function TrwReportForm.GetBandCount: Integer;
begin
  Result := FBands.Count;
end;

procedure TrwReportForm.SetGroups(Value: TrwGroups);
begin
  Groups.Assign(Value);
  AlignBands;
end;

procedure TrwReportForm.Notification(AComponent: TComponent; Operation: TOperation);
var
  i: Integer;
begin
  inherited;

  if AComponent.Owner = Self then
  begin
    if (AComponent is TrwBand) then
      if (Operation = opInsert) then
        FBands.Add(AComponent)

      else if (Operation = opRemove) then
      begin
        i := FBands.IndexOf(AComponent);
        FBands.Delete(i);
      end;

    if Assigned(FOnChangeComponentList) then
      FOnChangeComponentList(Self, AComponent, Operation);
  end;
end;

procedure TrwReportForm.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  if Assigned(GlobalVarFunc) and (Owner is TrwReport) then
    Proc(GlobalVarFunc);

  for i := 0 to ComponentCount - 1 do
    if ((Components[i] is TrwBand) or
       (Components[i] is TrwNoVisualComponent)) then
      Proc(Components[i]);
end;

procedure TrwReportForm.WriteState(Writer: TWriter);
var
  OldRoot: TComponent;
  OldRootAncestor: TComponent;
begin
  AssignUniqueName;
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := TrwCustomReport(OldRootAncestor).ReportForm;
    Writer.Root := Self;

    inherited;

  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;

procedure TrwReportForm.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
begin
  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited ReadState(Reader);
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrwReportForm.SetInheritedComponentProperty(AValue: Boolean);
begin
  inherited;
  Groups.SetInherited(AValue);
end;

function TrwReportForm.GetGroups: TrwGroups;
begin
  if Loading then
    Result := FGroupsRead
  else
    Result := FGroupsWork;
end;

procedure TrwReportForm.Loaded;
var
  i, j: Integer;
  fl: Boolean;
begin
  FGroupsWork.Assign(FGroupsRead);

  for i := 0 to FGroupsRead.Count-1 do
  begin
    //removing deleted group bands
    if Assigned(FGroupsRead[i].FGroupHeader) then
    begin
      fl := False;
      for j := 0 to FGroupsWork.Count-1 do
        if FGroupsRead[i].FGroupHeader = FGroupsWork[j].FGroupHeader then
        begin
          fl := True;
          break;
        end;

      if not fl then
        FGroupsRead[i].FGroupHeader.Free;
    end;

    if Assigned(FGroupsRead[i].FGroupFooter) then
    begin
      fl := False;
      for j := 0 to FGroupsWork.Count-1 do
        if FGroupsRead[i].FGroupFooter = FGroupsWork[j].FGroupFooter then
        begin
          fl := True;
          break;
        end;

      if not fl then
        FGroupsRead[i].FGroupFooter.Free;
    end;

    FGroupsRead[i].FGroupHeader := nil;
    FGroupsRead[i].FGroupFooter := nil;
  end;
  FGroupsRead.Clear;

  inherited;
end;

procedure TrwReportForm.AlignBands;
var
  x, i: Integer;

  procedure AlignBand(ABand: TrwBand);
  begin
    ABand.Top := x;
    x := x + ABand.Height;
  end;

  procedure AlignBandByType(ABandType: TrwBandType);
  var
    i: Integer;
  begin
    i := IndexOfBand(ABandType);
    if i <> -1 then
      AlignBand(Bands[i]);
  end;

begin
  if not Assigned(DesignParent) then
    Exit;

  x := 0;

  AlignBandByType(rbtPageHeader);
  AlignBandByType(rbtTitle);
  AlignBandByType(rbtHeader);

  for i := 0 to Groups.Count - 1 do
   if Assigned(Groups[i].FGroupHeader) then
     AlignBand(Groups[i].FGroupHeader);

  AlignBandByType(rbtDetail);
  AlignBandByType(rbtSubDetail);
  AlignBandByType(rbtSubSummary);

  for i := Groups.Count - 1 downto 0 do
   if Assigned(Groups[i].FGroupFooter) then
     AlignBand(Groups[i].FGroupFooter);

  AlignBandByType(rbtSummary);
  AlignBandByType(rbtFooter);
//  AlignBandByType(rbtPageFooter);
  AlignBandByType(rbtPageStyle);
  AlignBandByType(rbtCustom);
end;


{TrwBands}

destructor TrwBands.Destroy;
begin
  if Assigned(FGroups) and not Assigned(FReportForm) then
    FGroups.Free;

  inherited;
end;

procedure TrwBands.Assign(Source: TPersistent);
var
  i, j: Integer;
begin
  if not Assigned(FGroups) and not Assigned(FReportForm) then
    FGroups := TrwGroups.Create(nil, TrwGroup);

  PageHeader := TrwBands(Source).PageHeader;
  Title := TrwBands(Source).Title;
  Header := TrwBands(Source).Header;
  Footer := TrwBands(Source).Footer;
  Summary := TrwBands(Source).Summary;
  PageFooter := TrwBands(Source).PageFooter;
  PageStyle := TrwBands(Source).PageStyle;
  SubSummary := TrwBands(Source).SubSummary;
  SubDetail := TrwBands(Source).SubDetail;
  Custom := TrwBands(Source).Custom;

  //Delete removed groups
  i := 0;
  while i < FGroups.Count do
  begin
    j := TrwBands(Source).Groups.IndexOfGroup(TrwGroup(FGroups[i]).FieldName);
    if (j = -1) then
      TrwGroup(FGroups[i]).Free
    else
      Inc(i);
  end;

  //Arrange groups and add new group
  for i := 0 to TrwBands(Source).Groups.Count - 1 do
  begin
    j := FGroups.IndexOfGroup(TrwGroup(TrwBands(Source).Groups[i]).FieldName);

    if (j = -1) then
    begin
      if Assigned(FReportForm) then
        FReportForm.InsertGroup(i)
      else
        TrwGroup.Create(FGroups);
    end

    else
      if (j <> i) then
        FGroups[j].Index := i;

    if not TrwGroup(FGroups[i]).InheritedItem then
      TrwGroup(FGroups[i]).Assign(TrwBands(Source).Groups[i])
    else
      TrwGroup(FGroups[i]).Disabled := TrwBands(Source).Groups[i].Disabled;
  end;

  if Assigned(FReportForm) then
    FReportForm.AlignBands;
end;

procedure TrwBands.SetBand(Index: TrwBandType; Value: Boolean);
begin
  case Index of
    rbtPageHeader: FPageHeader := Value;
    rbtTitle: FTitle := Value;
    rbtHeader: FHeader := Value;
    rbtFooter: FFooter := Value;
    rbtSummary: FSummary := Value;
    rbtPageFooter: FPageFooter := Value;
    rbtPageStyle: FPageStyle := Value;
    rbtSubSummary: FSubSummary := Value;
    rbtSubDetail: FSubDetail := Value;
    rbtCustom: FCustom := Value;
  end;

  if Assigned(FReportForm) then
    if Value then
      FReportForm.AddBand(Index)
    else
      FReportForm.DeleteBand(Index);
end;


  {TrwCustomReport}

constructor TrwCustomReport.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FVirtOwner := True;  
  FReportForm := TrwReportForm.Create(Self);
  FPageFormat := TrwPage.Create;
  FPageFormat.PaperSize := psLetter;
  FPageFormat.PaperOrientation := rpoPortrait;
  FPageFormat.TopMargin := Round(ConvertUnit(0.25, utInches, utMillimeters) * 10);
  FPageFormat.BottomMargin := Round(ConvertUnit(0.25, utInches, utMillimeters) * 10);
  FPageFormat.LeftMargin := Round(ConvertUnit(0.6, utInches, utMillimeters) * 10);
  FPageFormat.RightMargin := Round(ConvertUnit(0.3, utInches, utMillimeters) * 10);
  FPageFormat.Duplexing := False;
  FPageFormat.OnChangePageFormat := ChangedPageFormat;

  FBands := TrwBands.Create;
  FBands.FReportForm := FReportForm;

  FStandAlone := False;

  FOnDestroy := nil;
  FRendering := nil;
end;

destructor TrwCustomReport.Destroy;
begin
  MasterDataSource := nil;
  DetailDataSource := nil;

  FBands.Free;
  FPageFormat.Free;
  FReportForm.Free;

  inherited;
end;

procedure TrwCustomReport.BeforeDestruction;
begin
  if Assigned(FOnDestroy) then FOnDestroy(Self);

  inherited;
end;

procedure TrwCustomReport.SetPageFormat(Value: TrwPage);
begin
  FPageFormat.Assign(Value);
end;

procedure TrwCustomReport.RegisterComponentEvents;
begin
  inherited;
  UnRegisterEvents([cEventOnPrint]);
  RegisterEvents([cEventBeforePrint, cEventAfterPrint, cEventBeforeWriteASCIILine]);
end;

procedure TrwCustomReport.ChangedPageFormat(Sender: TObject);
var
  i: Integer;
begin
  if Assigned(FReportForm) then
    for i := 0 to FReportForm.BandCount - 1 do
      FReportForm.Bands[i].AlignBandByForm;

  if Assigned(FOnChangePageFormat) then
    FOnChangePageFormat(Self);
end;

procedure TrwCustomReport.GetChildren(Proc: TGetChildProc; Root: TComponent);
begin
  Proc(FReportForm);
end;

function TrwCustomReport.IsDesignMode: Boolean;
begin
  Result := Assigned(FReportForm) and FReportForm.IsDesignMode;
end;

function TrwCustomReport.GetBands: TrwBands;
begin
  FBands.FPageHeader := (FReportForm.IndexOfBand(rbtPageHeader) <> -1);
  FBands.FPageFooter := (FReportForm.IndexOfBand(rbtPageFooter) <> -1);
  FBands.FHeader := (FReportForm.IndexOfBand(rbtHeader) <> -1);
  FBands.FFooter := (FReportForm.IndexOfBand(rbtFooter) <> -1);
  FBands.FTitle := (FReportForm.IndexOfBand(rbtTitle) <> -1);
  FBands.FSummary := (FReportForm.IndexOfBand(rbtSummary) <> -1);
  FBands.FPageStyle := (FReportForm.IndexOfBand(rbtPageStyle) <> -1);
  FBands.FGroups := FReportForm.Groups;
  FBands.FSubSummary := (FReportForm.IndexOfBand(rbtSubSummary) <> -1);
  FBands.FSubDetail := (FReportForm.IndexOfBand(rbtSubDetail) <> -1);
  FBands.FCustom := (FReportForm.IndexOfBand(rbtCustom) <> -1);

  Result := FBands;
end;

procedure TrwCustomReport.SetName(const NewName: TComponentName);
begin
  inherited;

  if Assigned(FOnChangeName) then FOnChangeName(Self);
end;

procedure TrwCustomReport.Loaded;
var
  i: Integer;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TrwReportForm then
    begin
      FReportForm := TrwReportForm(Components[i]);
      break;
    end;

  FBands.FReportForm := FReportForm;
  FReportForm.FGroupsWork.SetOwner(FReportForm);
  FReportForm.FGroupsRead.SetOwner(FReportForm);

  inherited;
end;


function TrwCustomReport.PGroupByField(AParam: Variant): Variant;
var
  i: Integer;
begin
  i := ReportForm.Groups.IndexOfGroup(AParam);
  if i <> -1 then
    Result := Integer(Pointer(ReportForm.Groups[i]))
  else
    Result := Integer(nil);
end;


procedure TrwCustomReport.CompileChildren(ASyntaxCheck: Boolean = False);
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to ReportForm.ComponentCount -1 do
  begin
    C := ReportForm.Components[i];
    if C is TrwComponent then
      if C is TrwCustomReport then
        TrwCustomReport(C).Compile(ASyntaxCheck)
      else
        TrwComponent(C).CompileEvents(ASyntaxCheck);
  end;
end;


function TrwCustomReport.FindVirtualChild(const AName: string): Integer;
var
  C: TComponent;
begin
  Result := inherited FindVirtualChild(AName);

  if Result >= -1 then
  begin
    C := ReportForm.FindComponent(AName);
    if Assigned(C) then
      Result := C.ComponentIndex;
  end;
end;


function TrwCustomReport.GetVirtualChild(const AIndex: Integer): TComponent;
begin
  if AIndex < 0 then
    Result := inherited GetVirtualChild(AIndex)
  else
    Result := ReportForm.Components[AIndex];
end;

function TrwCustomReport.VirtualComponentCount: Integer;
begin
  Result := ReportForm.ComponentCount;
end;


procedure TrwCustomReport.ReadState(Reader: TReader);
begin
  if Assigned(FReportForm) and not FReportForm.InheritedComponent then
  begin
    SetExternalGlobalVarFunc(nil);  
    FReportForm.Free;
    FReportForm := nil;
  end;

  inherited;
end;

procedure TrwCustomReport.InsertVirtualComponent(AComponent: TrwComponent);
begin
  FReportForm.InsertComponent(AComponent);
end;


procedure TrwCustomReport.SetMasterDataSource(const Value: TrwDataSet);
begin
  if Assigned(FMasterDataSource) then
    FMasterDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FMasterDataSource := Value;
end;



procedure TrwCustomReport.SetDetailDataSource(const Value: TrwDataSet);
begin
  if Assigned(FDetailDataSource) then
    FDetailDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FDetailDataSource := Value;
end;


procedure TrwCustomReport.RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation);
begin
  inherited;

  if AOperation = rnoDestroy then
  begin
    if Sender = FMasterDataSource then
      MasterDataSource := nil;
    if Sender = FDetailDataSource then
      DetailDataSource := nil;
  end;
end;


{TrwInputFormContainer}

constructor TrwInputFormContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FShownOnForm := False;
  Color := clBtnFace;
  FVirtOwner := True;
  FRunTimeMode := True;
  Width := 270;
  Height := 150;
  FCloseResult := mrNone;
end;

function TrwInputFormContainer.IsDesignMode: Boolean;
begin
  Result := Assigned(DesignParent) and not FShownOnForm;
end;

procedure TrwInputFormContainer.SetParentComponent(Value: TComponent);
begin
end;

procedure TrwInputFormContainer.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  inherited;

  if RootOwner is TrwReport then
    for i := 0 to ComponentCount - 1 do
      if (Components[i] is TrwNoVisualComponent) then
        Proc(Components[i]);
end;

procedure TrwInputFormContainer.WriteState(Writer: TWriter);
var
  OldRoot: TComponent;
  OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      if OldRootAncestor is TrwReport then
        Writer.RootAncestor := TrwReport(OldRootAncestor).InputForm
      else if OldRootAncestor is TrmReport then
        Writer.RootAncestor := TrmReport(OldRootAncestor).InputForm;
    Writer.Root := Self;

    inherited;

  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;

procedure TrwInputFormContainer.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
begin
  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited ReadState(Reader);
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrwInputFormContainer.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnShow, cEventOnClose, cEventOnCloseQuery]);
end;

procedure TrwInputFormContainer.DoAfterCreate;

  procedure ExecAfterCreate(AComp: TrwComponent);
  var
    i: Integer;
  begin
    if AComp is TrwFormControl then
      AComp.ExecuteEventsHandler('AfterCreate', []);

    for i := 0 to AComp.ComponentCount - 1 do
      if AComp.Components[i] is TrwFormControl then
        ExecAfterCreate(TrwComponent(AComp.Components[i]));
  end;

begin
  CloseResult := mrNone;
  ExecAfterCreate(Self);
  SetInputFormParams;
  ExecuteEventsHandler('OnShow', []);
end;


procedure TrwInputFormContainer.DoClose;
begin
  ExecuteEventsHandler('OnClose', []);
  if CloseResult = mrOk then
    GetInputFormParams;

  if EventsList.IndexOf('AfterCloseInputForm') <> -1 then
    ExecuteEventsHandler('AfterCloseInputForm', []);
end;

function TrwInputFormContainer.DoCloseQuery: Boolean;
var
  V: Variant;
begin
  if CloseResult = mrOK then
  begin
    V := True;
    ExecuteEventsHandler('OnCloseQuery', [Integer(Addr(V))]);
    Result := V;
  end
  else
    Result := True;
end;


procedure TrwInputFormContainer.CompileChildren(ASyntaxCheck: Boolean = False);
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to ComponentCount -1 do
  begin
    C := Components[i];
    if C is TrwComponent then
      TrwComponent(C).CompileEvents(ASyntaxCheck);
  end;
end;

procedure TrwInputFormContainer.HideRunTimeOnly(AOwner: TrwComponent);
var
  i: Integer;
begin
  for i := 0 to AOwner.ComponentCount - 1 do
    if AOwner.Components[i] is TrwFormControl then
    begin
      if TrwFormControl(AOwner.Components[i]).RunTimeOnly then
        TrwFormControl(AOwner.Components[i]).HideRT;
      HideRunTimeOnly(TrwFormControl(AOwner.Components[i]));
    end;
end;


function TrwInputFormContainer.PRunTimeMode: Variant;
begin
  Result := FRunTimeMode;
end;

procedure TrwInputFormContainer.SetDesignParent(Value: TWinControl);
var
  i: Integer;
  P: TWinControl;

  procedure MakeVisible(AComp: TrwComponent);
  var
    i: Integer;
  begin
    for i := 0 to AComp.ComponentCount - 1 do
    begin
      if AComp.Components[i] is TrwControl then
      begin
        if not (AComp.Components[i] is TrwTabSheet) then
        begin
          TrwControl(AComp.Components[i]).VisualControl.RealObject.Visible := True;
          TrwControl(AComp.Components[i]).VisualControl.RealObject.Enabled := True;
        end;
        MakeVisible(TrwControl(AComp.Components[i]));
      end;
    end;
  end;

begin
  inherited;

  if not FShownOnForm then
  begin
    if Assigned(Value) then
      if Owner is TrwReport then
        P := DesignParent
      else
        P := DesignParent.Parent  // exception for TrmReport
    else
      P := nil;

    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrwNoVisualComponent then
        TrwComponent(Components[i]).DesignParent := P;

    if Assigned(Value) then
      MakeVisible(Self);

    TrwInpFrmPanel(VisualControl.RealObject).SetToDesigning;
  end

  else
    TrwInpFrmPanel(VisualControl.RealObject).OnClick := FOnClick;
end;


function TrwInputFormContainer.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwInpFrmPanel.Create(Self);
  Result.PropChanged(rwCPCreation);
end;

procedure TrwInputFormContainer.SetParentForm(Value: TWinControl);
begin
  FShownOnForm := Assigned(Value);
  SetDesignParent(Value);
end;


procedure TrwInputFormContainer.SetDesigner(ADesigner: IrmDesigner);
var
  P: TWinControl;
begin
  inherited;
  if Assigned(ADesigner) then
  begin
    P := ADesigner.GetDesignParent(Name);
    if P is TrwInputParamsForm then
      TrwInputParamsForm(P).InputForm := Self;
    DesignParent := P;
  end
  else
    DesignParent := nil;
end;

procedure TrwInputFormContainer.HideRunTimeControls;
begin
  if not FRunTimeMode then
    HideRunTimeOnly(Self);
end;

procedure TrwInputFormContainer.GetInputFormParams;
var
  i, j: Integer;
  h: string;
  GlVar: TrwGlobalVariable;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i].InheritsFrom(TrwParamFormControl) then
    begin
      h := TrwParamFormControl(Components[i]).ParamName;
      j := TrwComponent(Owner).GlobalVarFunc.Variables.IndexOf(h);
      if j <> -1 then
      begin
        GlVar := TrwComponent(Owner).GlobalVarFunc.Variables[j];
        GlVar.Value := TrwParamFormControl(Components[i]).ParamValue;
      end;
    end;
end;

procedure TrwInputFormContainer.SetInputFormParams;
var
  i, j: Integer;
  h: string;
  GlVar: TrwGlobalVariable;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i].InheritsFrom(TrwParamFormControl) then
    begin
      h := TrwParamFormControl(Components[i]).ParamName;
      j := TrwComponent(Owner).GlobalVarFunc.Variables.IndexOf(h);
      if j <> -1 then
      begin
        GlVar := TrwComponent(Owner).GlobalVarFunc.Variables[j];
        TrwParamFormControl(Components[i]).ParamValue := GlVar.Value;
      end;
    end;
end;

procedure TrwInputFormContainer.ShowInRunTime(const AParent: HWND);
begin
  FRunTimeContainer := TrwInputParamsForm.CreateAsFrame;
  FRunTimeContainer.ClientWidth := Width;
  FRunTimeContainer.ClientHeight :=Height;
  SetParentForm(FRunTimeContainer);
  Align := alClient;
  HideRunTimeControls;
  DoAfterCreate;

  TrwInputParamsForm(FRunTimeContainer).SetExternalParentWindow(AParent);
  FRunTimeContainer.Visible := True;
  FRunTimeContainer.Top := 0;
  FRunTimeContainer.Left := 0;
end;

procedure TrwInputFormContainer.CloseInRunTime;
begin
  try
    DoClose;
  finally
    FRunTimeContainer.Visible := False;
    SetParentForm(nil);
    TrwInputParamsForm(FRunTimeContainer).SetExternalParentWindow(0);
    FreeAndNil(FRunTimeContainer);
  end;
end;

{TrwASCIIBand}

constructor TrwASCIIBand.Create;
begin
  inherited Create;

  FLines := TList.Create;
end;

destructor TrwASCIIBand.Destroy;
var
  i: Integer;
begin
  for i := 0 to FLines.Count - 1 do
    TStringList(FLines[i]).Free;

  FLines.Free;
  inherited;
end;

procedure TrwASCIIBand.Clear;
var
  i: Integer;
begin
  for i := 0 to FLines.Count - 1 do
    TStringList(FLines[i]).Clear;
end;

function TrwASCIIBand.GetLine(Index: Integer): string;
var
  i: Integer;
begin
  if (TStringList(FLines[Index]).Count = 0) then
    Result := #0
  else
  begin
    Result := '';
    for i := 0 to TStringList(FLines[Index]).Count - 1 do
      Result := Result + TStringList(FLines[Index])[i];
  end;
end;

procedure TrwASCIIBand.PrinText(ALine: Integer; ACol: Integer; AText: string);
var
  StrLst: TStringList;
begin
  while (ALine > FLines.Count) do
    FLines.Add(TStringList.Create);

  StrLst := TStringList(FLines[ALine - 1]);

  while (ACol > StrLst.Count) do
    StrLst.Add('');

  StrLst[ACol - 1] := AText;
end;

function TrwASCIIBand.GetLinesCount: Integer;
begin
  Result := FLines.Count;
end;



{TrwReport}

constructor TrwReport.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FWizardInfo := nil;

  FStandAlone := True;
  FLibVersion := 0;

  FInputForm := TrwInputFormContainer.Create(Self);
  FInputForm.Name := 'InputForm';

  FReportForm.InitGlobalVarFunc;

  SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);
  FInputForm.SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);

  FASCIIFileName := '';
  FDuplexing := False;

  FOnAfterPrintPage := nil;
  FSecurityInfo := nil;
  FASCIILastLineBreak := True;
end;


destructor TrwReport.Destroy;
begin
  FInputForm.Free;
  FSecurityInfo.Free;

  inherited;
end;


procedure TrwReport.Loaded;
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TrwInputFormContainer then
    begin
      if Components[i] <> FInputForm then
      begin
        if Assigned(FInputForm) then
        begin
          FInputForm.Width := TrwInputFormContainer(Components[i]).Width;
          FInputForm.Height := TrwInputFormContainer(Components[i]).Height;

          while Components[i].ComponentCount > 1 do
          begin
            C := Components[i].Components[1];
            Components[i].RemoveComponent(C);
            FInputForm.InsertComponent(C);
            if (C is TrwFormControl) and (TrwFormControl(C).Parent = Components[i]) then
              TrwFormControl(C).Parent := FInputForm;
          end;
//          Components[i].Free;
        end

        else
          FInputForm := TrwInputFormContainer(Components[i]);
        break;
      end;
    end;

  if not Assigned(FInputForm) then
  begin
    FInputForm := TrwInputFormContainer.Create(Self);
    FInputForm.Name := 'InputForm';
  end;

  inherited;

  if not Assigned(FReportForm.GlobalVarFunc) then
    FReportForm.InitGlobalVarFunc;

  FInputForm.SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);
  SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);
end;

procedure TrwReport.GetChildren(Proc: TGetChildProc; Root: TComponent);
begin
  inherited GetChildren(Proc, Root);
  Proc(FInputForm);
end;

procedure TrwReport.LoadFromFile(const AFileName: string);
begin
  LoadRWResFromFile(Self, AFileName);
end;

procedure TrwReport.SaveToFile(const AFileName: string);
begin
  SaveRWResToFile(Self, AFileName);
end;

procedure TrwReport.LoadFromStream(AStream: TStream);
var
  P: Int64;
  h: String;
begin
  P := AStream.Position;
  h := StringOfChar(' ', 25);
  AStream.ReadBuffer(h[1], Length(h));
  if Pos(TrwSecureWrapper.ClassName, h) > 0 then
  begin
    AStream.Position := p;
    AStream.ReadComponent(SecurityInfo);
  end
  else
  begin
    AStream.Position := p;
    ReadReport(AStream);
  end;
end;

procedure TrwReport.SaveToStream(AStream: TStream);
begin
  if IsLicenced then
    AStream.WriteComponent(SecurityInfo)
  else
    WriteReport(AStream);
end;

procedure TrwReport.ReaderError(Reader: TReader; const Message: string; var Handled: Boolean);
begin
  if IgnoreLoadingErrors then
  begin
    Handled := True;
    Exit;
  end;

  if RWDesigning then
    Handled := MessageDlg(Message, mtError, [mbAbort, mbIgnore], 0) = mrIgnore
  else
    Handled := False;
end;


procedure TrwReport.Prepare;
begin
  InitializationSbVM;
  try
    PrepareQueries(FReportForm);
    PrepareQueries(FInputForm);
  finally
    DeInitializationSbVM;
  end;
end;


function TrwReport.Print(AResultFileName: string; AShowInputForm: Boolean = False): Boolean;
var
  lTime: DWord;
begin
  Result := False;

  BeginWork;
  try
    SysUtils.DeleteFile(AResultFileName);
    ClearTempTables(FReportForm);

    if AShowInputForm and not InputParams then
      Exit;

    if not RWDesigning then
      RWEngineExt.AppAdapter.StartEngineProgressBar('Running Report...');

    FRendering := TrwRendering.Create;
    try
      FResultFileName := AResultFileName;
      TrwRendering(FRendering).ResultFileName := AResultFileName;

      with RwStatRec^ do
      begin
        ReportName := Self.ReportName;
        DateTimeStamp := Now;
        TotalTime := 0;
        ConnectionTime := 0;
        CacheDataTime := 0;
        CacheDataSize := 0;
        RemoteQueriesTime := 0;
        LogicalQueriesTime := 0;
        RemoteConnections := 0;
        RemoteQueries := 0;
        LogicalQueries := 0;
        Lock := False;
      end;

      lTime := GetTickCount;
      TrwRendering(FRendering).ExecuteReport(Self);
      RwStatRec.TotalTime := GetTickCount - lTime;
      RwStatRec.CacheDataSize := RwStatRec.CacheDataSize div 1024;

      Result := True;

    finally
      if not RWDesigning then
        RWEngineExt.AppAdapter.EndEngineProgressBar;

      TrwRendering(FRendering).Free;
      FRendering := nil;
    end;

  finally
    EndWork;
  end;
end;

procedure TrwReport.DefineProperties(Filer: TFiler);

  function DoWrite: Boolean;
  begin
    Result := Assigned(FWizardInfo) and (WizardInfo.Size > 0);
  end;

  function IsCompiledCodeNotEmpty: Boolean;
  begin
    Result := Length(FCompiledCode) > 0;
  end;

begin
  inherited;
  Filer.DefineBinaryProperty('WizardInfo', ReadWizardInfo, WriteWizardInfo, DoWrite);
  Filer.DefineBinaryProperty('CompiledCode', ReadCompiledCode, WriteCompiledCode, IsCompiledCodeNotEmpty);
  Filer.DefineProperty('LibVersion', ReadLibVersion, WriteLibVersion, True);
end;

procedure TrwReport.ReadWizardInfo(Stream: TStream);
begin
  if not Assigned(FWizardInfo) then
    FWizardInfo := TEvDualStreamHolder.Create
  else
    WizardInfo.Clear;
  WizardInfo.RealStream.CopyFrom(Stream, Stream.Size);
end;

procedure TrwReport.WriteWizardInfo(Stream: TStream);
begin
  WizardInfo.Position := 0;
  Stream.CopyFrom(WizardInfo.RealStream, WizardInfo.Size);
end;


procedure TrwReport.Compile(ASyntaxCheck: Boolean = False);
begin
  if LibDesignMode then
    inherited
  else
  begin
    SetLength(FCompiledCode, 0);
    SetCompiledCode(@FCompiledCode, cVMReportOffset);
    SetCompilingPointer(0);

    SetLength(FDebInf, 0);
    SetDebInf(@FDebInf);
    SetDebInfPointer(0);
    SetLength(FDebSymbInf, 0);
    SetDebSymbInf(@FDebSymbInf);
    SetDebSymbInfPointer(0);

    try
      inherited;
      FLibVersion := RWEngineExt.AppAdapter.CurrentDateTime;
    except
      FLibVersion := 0;
      ClearCompiledInfo;
      raise;
    end;

    SetLength(FCompiledCode, GetCompilingPointer);
    SetLength(FDebInf, GetDebInfPointer);
    SetLength(FDebSymbInf, GetDebSymbInfPointer);
  end;
end;


procedure TrwReport.CompileChildren(ASyntaxCheck: Boolean = False);
begin
  inherited;
  InputForm.Compile(ASyntaxCheck);
end;


procedure TrwReport.ReadCompiledCode(Stream: TStream);
var
  A: Variant;
  i: Integer;
begin
  Stream.ReadBuffer(i, SizeOf(i));
  A := StreamToVarArray(Stream);

  SetLength(FCompiledCode, VarArrayHighBound(A, 1) - VarArrayLowBound(A, 1) + 1);
  for i := Low(CompiledCode) to High(CompiledCode) do
  begin
    CompiledCode[i] := A[i];
    if VarType(CompiledCode[i]) = varOleStr then
      CompiledCode[i] := VarAsType(CompiledCode[i], varString);
  end;
end;


procedure TrwReport.WriteCompiledCode(Stream: TStream);
var
  A: Variant;
  i: Integer;
  MS: TStream;
begin
  A := VarArrayCreate([Low(CompiledCode), High(CompiledCode)], varVariant);
  for i := Low(CompiledCode) to High(CompiledCode) do
    A[i] := CompiledCode[i];

 MS := VarArrayToStream(A, nil, True);
 try
   MS.Position := 0;
   i := MS.Size;
   Stream.WriteBuffer(i, SizeOf(i));
   Stream.CopyFrom(MS, i);
 finally
   MS.Free;
 end
end;


procedure TrwReport.ClearCompiledInfo;
begin
  SetLength(FCompiledCode, 0);
  SetLength(FDebInf, 0);
  SetLength(FDebSymbInf, 0);
end;


function TrwReport.InputParams: Boolean;
begin
  if InputForm.Visible and (InputForm.ComponentCount > 0) then
  begin
    ClearTempTables(InputForm);
    Result := ShowInFormContents(InputForm, ReportName, nil)
  end
  else
    Result := True;
end;


procedure TrwReport.ReadLibVersion(Reader: TReader);
begin
  FLibVersion := Reader.ReadDate;
end;


procedure TrwReport.WriteLibVersion(Writer: TWriter);
begin
  Writer.WriteDate(FLibVersion);
end;


procedure TrwReport.ReadState(Reader: TReader);
begin
  if Assigned(FInputForm) and not FInputForm.InheritedComponent then
  begin
    FInputForm.Free;
    FInputForm := nil;
  end;

  inherited;
end;

procedure TrwReport.ReaderSetName(Reader: TReader; Component: TComponent; var Name: string);
begin
  if (Component is TrwInputFormContainer) and Assigned(FInputForm) then
    Name := '';
end;



function TrwReport.ShowInputForm(AParent: HWND; ARunTime: Boolean = True): Boolean;
begin
  if InputForm.Visible and (InputForm.ComponentCount > 0) and (ARunTime or not InputForm.RunTimeOnly) then
  begin
    InputForm.FRunTimeMode := ARunTime;
    PrepareQueries(InputForm);
    ClearTempTables(InputForm);
    if AParent = 0 then
      Result := ShowInFormContents(InputForm, '', nil)
    else
    begin
      InputForm.ShowInRunTime(AParent);
      Result := True;
    end;
  end
  else
    Result := False;
end;

procedure TrwReport.CloseInputForm;
begin
  InputForm.CloseInRunTime;
end;

procedure TrwReport.BeginWork;
begin
  InitializationSbVM;
  CreateRwVM(@FCompiledCode);
end;


procedure TrwReport.EndWork;
begin
  DestroyRwVM;
  DeInitializationSbVM;
end;


function TrwReport.GetSecurityInfo: TrwSecureWrapper;
begin
  if not Assigned(FSecurityInfo) then
  begin
    FSecurityInfo := TrwSecureWrapper.Create(nil);
    FSecurityInfo.FReport := Self;
  end;
  Result := FSecurityInfo;
end;


procedure TrwReport.ReadReport(AStream: TStream);
var
  Reader: TReader;
begin
  Reader := TReader.Create(AStream, 1024);
  try
    Reader.OnError := ReaderError;
    Reader.OnSetName := ReaderSetName;
    InitializationSbVM;
    try
      Reader.ReadRootComponent(Self);
      FixUpUserProperties;
    finally
      DeInitializationSbVM;
    end;
  finally
    Reader.Free;
  end;
end;


procedure TrwReport.WriteReport(AStream: TStream);
begin
  InitializationSbVM;
  try
    AStream.WriteComponent(Self);
  finally
    DeInitializationSbVM;
  end;
end;

function TrwReport.IsLicenced: Boolean;
begin
  Result := Assigned(FSecurityInfo) and (FSecurityInfo.Licence <> '');
end;


procedure TrwReport.GetReportParams(const AParams: PTrwReportParamList);
begin
  VariablesToParamList(GlobalVarFunc.Variables, AParams);
end;

procedure TrwReport.SetReportParams(const AParams: PTrwReportParamList);
begin
  ParamListToVariables(GlobalVarFunc.Variables, AParams);
end;

function TrwReport.GetWizardInfo: IEvDualStream;
begin
  if not Assigned(FWizardInfo) then
    FWizardInfo := TEvDualStreamHolder.Create;
  Result := FWizardInfo;
end;

function TrwReport.GetCompiledCode: PTrwCode;
begin
  Result := Addr(FCompiledCode);
end;

function TrwReport.GetDebInf: PTrwDebInf;
begin
  Result := Addr(FDebInf);
end;

function TrwReport.GetDebSymbInf: PTrwDebInf;
begin
  Result := Addr(FDebSymbInf);
end;


procedure TrwReport.AfterInitLibComponent;
begin
  inherited;
  SetLength(FCompiledCode, 0);
end;

{TrwSubReport}

constructor TrwSubReport.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FPageFormat.TopMargin := 0;
  FPageFormat.BottomMargin := 0;
  FPageFormat.LeftMargin := 0;
  FPageFormat.RightMargin := 0;

  FReportForm.SetExternalGlobalVarFunc(TrwComponent(AOwner).GlobalVarFunc);
  SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);
end;

procedure TrwSubReport.WriteState(Writer: TWriter);
var
  OldRoot, OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := TrwReportForm(OldRootAncestor).FindComponent(Name);
    Writer.Root := Self;
    inherited;
  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;

procedure TrwSubReport.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
begin
  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited;
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;


procedure TrwSubReport.Print(Sender: TObject);
begin
  ClearTempTables(FReportForm);
  FRendering := Sender;
  try
    TrwRendering(FRendering).ExecuteReport(Self);
  finally
    FRendering := nil;
  end;
end;

procedure TrwSubReport.PPrint;
begin
  Print(TrwCustomReport(Owner.Owner).Rendering);
end;

procedure TrwSubReport.Loaded;
var
  Ow: TComponent;
begin
  inherited;

  Ow := Owner;
  while Assigned(Ow) do
    if not Assigned(TrwComponent(Ow).GlobalVarFunc) then
      Ow := Ow.Owner
    else
    begin
      FReportForm.SetExternalGlobalVarFunc(TrwComponent(Ow).GlobalVarFunc);
      SetExternalGlobalVarFunc(FReportForm.GlobalVarFunc);
      break;
    end;
end;



{ TrwSecurityInfo }

procedure TrwSecureWrapper.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('Data', ReadData, WriteData, True); 
end;

procedure TrwSecureWrapper.WriteData(AStream: TStream);
var
  S1, S2: IEvDualStream;
  BF: TBlowfishCipherFixed;
begin
  S1 := TEvDualStreamHolder.Create;
  S2 := TEvDualStreamHolder.Create;

  FReport.WriteReport(S1.RealStream);
  S1.RealStream.Position := 0;
  DeflateStream(S1.RealStream, S2.RealStream);

  BF := TBlowfishCipherFixed.CreateStrKey(RWEngineExt.AppAdapter.GetLicencePassword(Licence));
  try
    S2.Position := 0;
    BF.EncryptStream(S2.RealStream, AStream);
  finally
    BF.Free;
  end;
end;

procedure TrwSecureWrapper.ReadData(AStream: TStream);
var
  S1, S2: IEvDualStream;
  BF: TBlowfishCipherFixed;
begin
  S1 := TEvDualStreamHolder.Create;
  S2 := TEvDualStreamHolder.Create;

  BF := TBlowfishCipherFixed.CreateStrKey(RWEngineExt.AppAdapter.GetLicencePassword(Licence));
  try
    AStream.Position := 0;
    BF.DecryptStream(AStream, S1.RealStream);
  finally
    BF.Free;
  end;

  S1.Position := 0;
  InflateStream(S1.RealStream, S2.RealStream);

  S2.Position := 0;
  FReport.ReadReport(S2.RealStream);
end;


end.
