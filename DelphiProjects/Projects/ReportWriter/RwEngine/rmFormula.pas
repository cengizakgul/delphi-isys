unit rmFormula;

interface

uses Classes, SysUtils, Variants, EvStreamUtils, rwTypes, rwEngineTypes,
     rwBasicUtils, rwVirtualMachine, rmTypes;

type
  TrmFMLAggrFunctHolder = class(TrwPersistent)
  private
    FValueCounter: Variant;
    FCurrentValue: Variant;
    FCurrentValues: array of Variant;
    FCodeAddress: Integer;
    FChanging: Boolean;
    FFunctionID: String;

    procedure AddValue(const Params: Variant);
    function  CalcValue: Variant;

  public
    constructor Create(AOwner: TPersistent); override;
    procedure   ClearValues;
    procedure   NotifyOfValueChanges;
    function    Calculate: Variant;

  published
    property AddValCodeAddress: Integer read FCodeAddress write FCodeAddress default -1;
    property FunctionID: String read FFunctionID write FFunctionID;
  end;


  TrmFMLFormulaItem = class(TCollectionItem)
  private
    FValue: Variant;
    FAddInfo: String;
    FItemType: TrmFMLItemType;
    FAggrFunctHolder: TrmFMLAggrFunctHolder;
    procedure SetValue(const Value: Variant);
    procedure SetItemType(const Value: TrmFMLItemType);
    procedure SetAggrFunctHolder(const Value: TrmFMLAggrFunctHolder);
    procedure PrepareAggrFnctHolder;
    procedure SetAddInfo(const AValue: String);
    function  IsNotDefaultAddInfo: Boolean;
  public
    destructor Destroy; override;
    procedure  Assign(ASource: TPersistent); override;
    function   IsAggregateFunction: Boolean;
    function   GetQBField(const ARootObject: IrmDesignObject = nil): IrwQBShowingField;
  published
    property ItemType: TrmFMLItemType read FItemType write SetItemType;
    property Value: Variant read FValue write SetValue;
    property AddInfo: String read FAddInfo write SetAddInfo stored IsNotDefaultAddInfo;
    property AggrFunctHolder: TrmFMLAggrFunctHolder read FAggrFunctHolder write SetAggrFunctHolder stored IsAggregateFunction;
  end;


  TrmFMLFormulaContent = class(TOwnedCollection)
  private
    FExprType: TrwVarTypeKind;
    function  GetExprType: TrwVarTypeKind;
    function  GetItem(Index: Integer): TrmFMLFormulaItem;
    procedure SetItem(Index: Integer; const Value: TrmFMLFormulaItem);
    function  ComponentOwner: TComponent;

  public
    property    Items[Index: Integer]: TrmFMLFormulaItem read GetItem write SetItem; default;

    function  AggrFunctSkipThrough(AFnct: TrmFMLFormulaItem): Integer;
    function    AddItem(AType: TrmFMLItemType; const AValue: Variant; const AAddInfo: String = ''): TrmFMLFormulaItem;
    function    CheckTypeCompatibility(AType1, AType2: TrwVarTypeKind): Boolean;
    function    CheckErrors(var ErrItem: Integer): String;
    procedure   Assign(Source: TPersistent); override;
    function    ContentToString: String;
    procedure   ContentFromString(const AContent: String);
    procedure   Clear;
    function    RWExpression(BeginIndex, EndIndex: Integer): String;
  published
    property ExprType: TrwVarTypeKind read GetExprType stored False;
  end;



  TrmFMLFormula = class(TrwCollectionItem, IrmFMLFormula)
  private
    FContent: TrmFMLFormulaContent;
    FActionInfo: String;
    FActionType: TrmFMLActionType;
    FCodeAddress: Integer;
    procedure SetContent(const Value: TrmFMLFormulaContent);
    procedure WriteCodeAddress(Writer: TWriter);
    procedure ReadCodeAddress(Reader: TReader);
    procedure OnChange;
    procedure CompileAggrFunction(AFnct: TrmFMLFormulaItem; ASyntaxCheck: Boolean = False);
  protected
    procedure DefineProperties(Filer: TFiler); override;

  // Interface
    procedure SetActionType(const Value: TrmFMLActionType);
    function  GetActionType: TrmFMLActionType;
    procedure SetActionInfo(const Value: String);
    function  GetActionInfo: String;
    procedure Clear;
    function  AddItem(const AType: TrmFMLItemType; const AValue: Variant;  const AddInfo: String = ''; const AIndex: Integer = -1): Integer;
    function  GetItemCount: Integer;
    function  GetItemInfo(const AItemIndex: Integer): TrmFMLItemInfoRec;
    function  ThereAreObjectsBelongingTo(const AObjectParentPath: String): Boolean;
  // End

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
    function    ContentToString: String;
    procedure   ContentFromString(const AContent: String);
    function    RWExpression: String;
    procedure   Compile(ASyntaxCheck: Boolean = False);
    function    Calculate(const AParams: array of Variant): Variant;

  published
    property Content: TrmFMLFormulaContent read FContent write SetContent;
    property ActionType: TrmFMLActionType read GetActionType write SetActionType;
    property ActionInfo: String read GetActionInfo write SetActionInfo;
    property CodeAddress: Integer read FCodeAddress write FCodeAddress default -1;
  end;


  TrmFMLFormulas = class(TrwCollection, IrmFMLFormulas)
  private
    function  GetItem(Index: Integer): TrmFMLFormula;
    procedure SetItem(Index: Integer; const Value: TrmFMLFormula);
    function  AggrFunctHolderByID(const AFunctionID: String): TrmFMLAggrFunctHolder;

  protected
  // interface
    function  GetFormulaByAction(AActionType: TrmFMLActionType; const AActionInfo: String): IrmFMLFormula;
  // end

  public
    property Items[Index: Integer]: TrmFMLFormula read GetItem write SetItem; default;

    function  AddFormula(AActionType: TrmFMLActionType; const AActionInfo: String): TrmFMLFormula;
    function  FormulaByAction(AActionType: TrmFMLActionType; const AActionInfo: String): TrmFMLFormula;
    procedure Compile(ASyntaxCheck: Boolean = False);
    procedure EmptyAggregateFunctions;
    procedure PrepareForCalculation;
    function  CalcAggrFunctResult(const AFunctionID: String): Variant;
  end;

implementation

uses rwParser, rwBasicClasses, rwUtils, rmCommonClasses, StrUtils,
     TypInfo;


{ TrmFormula }

constructor TrmFMLFormula.Create(Collection: TCollection);
begin
  inherited;
  FContent := TrmFMLFormulaContent.Create(Self, TrmFMLFormulaItem);
  FCodeAddress := -1;
end;

destructor TrmFMLFormula.Destroy;
begin
  FreeAndNil(FContent);
  inherited;
end;


procedure TrmFMLFormula.SetContent(const Value: TrmFMLFormulaContent);
begin
  FContent.Assign(Value);
end;

{ TrmFMLFormulaItem }

procedure TrmFMLFormulaItem.Assign(ASource: TPersistent);
begin
  ItemType := TrmFMLFormulaItem(ASource).ItemType;
  Value := TrmFMLFormulaItem(ASource).Value;
  AddInfo := TrmFMLFormulaItem(ASource).AddInfo;
end;

destructor TrmFMLFormulaItem.Destroy;
begin
  FreeAndNil(FAggrFunctHolder);
  inherited;
end;

function TrmFMLFormulaItem.GetQBField(const ARootObject: IrmDesignObject = nil): IrwQBShowingField;
var
  O: IrmDesignObject;
  Q: IrmQuery;
  h, h1: String;
begin
  Result := nil;

  h := VarToStr(Value);
  h1 := GetPrevStrValue(h, '.');
  if Assigned(ARootObject) then
    O := ComponentByPath(h, ARootObject)
  else
    O := ComponentByPath(h, TrwComponent(TrmFMLFormulaContent(Collection).ComponentOwner).RootOwner);

  if not Assigned(O) or not Supports(O.RealObject, IrmQueryDrivenObject) then
    Exit;

  Q := (O.RealObject as IrmQueryDrivenObject).GetQuery;
  Result := Q.GetQBQuery.GetQBFieldByID(h1);
end;

function TrmFMLFormulaItem.IsAggregateFunction: Boolean;
begin
  Result := (ItemType = fitFunction) and (AggrFunctions.IndexOf(Value) <> -1);
end;

function TrmFMLFormulaItem.IsNotDefaultAddInfo: Boolean;
begin
  Result := (ItemType = fitObject) and (FAddInfo <> '') and not AnsiSameText(FAddInfo, 'Value');
end;

procedure TrmFMLFormulaItem.PrepareAggrFnctHolder;
begin
  if IsAggregateFunction and not Assigned(FAggrFunctHolder) then
    FAggrFunctHolder := TrmFMLAggrFunctHolder.Create(Self)
  else
    FreeAndNil(FAggrFunctHolder);  
end;

procedure TrmFMLFormulaItem.SetAddInfo(const AValue: String);
begin
  if ItemType = fitObject then
    FAddInfo := AValue
  else
    FAddInfo := '';
end;

procedure TrmFMLFormulaItem.SetAggrFunctHolder(const Value: TrmFMLAggrFunctHolder);
begin
  FAggrFunctHolder.Assign(Value);
end;

procedure TrmFMLFormulaItem.SetItemType(const Value: TrmFMLItemType);
begin
  FItemType := Value;
  AddInfo := '';
  PrepareAggrFnctHolder;
end;

procedure TrmFMLFormulaItem.SetValue(const Value: Variant);
begin
  FValue := Value;
  if VarType(FValue) in [varSmallint, varShortInt] then
    FValue := Integer(FValue);

  if ItemType = fitObject then
    AddInfo := 'Value';

  PrepareAggrFnctHolder;    
end;

procedure TrmFMLFormula.Assign(Source: TPersistent);
begin
  inherited;
  FContent.Assign(TrmFMLFormula(Source).Content);
  FActionType := TrmFMLFormula(Source).ActionType;
  FActionInfo := TrmFMLFormula(Source).ActionInfo;
  OnChange;  
end;

procedure TrmFMLFormula.ContentFromString(const AContent: String);
var
  Reader: TrwReader;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(Length(AContent));
  MS.WriteBuffer(AContent[1], Length(AContent));
  MS.Position := 0;

  Reader := TrwReader.Create(MS.RealStream);
  try
    Reader.ReadListBegin;
    while not Reader.EndOfList do
      Reader.ReadProperty(Self);

  finally
    Reader.Free;
  end;

  OnChange;  
end;

function TrmFMLFormula.ContentToString: String;
var
  Writer: TrwWriter;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  Writer := TrwWriter.Create(MS.RealStream, 1024);
  try
    Writer.WriteListBegin;
    Writer.WriteProperties(Self);
    Writer.WriteListEnd;
  finally
    Writer.Free;
  end;
  SetLength(Result, MS.Size);
  MS.Position := 0;
  MS.ReadBuffer(Result[1], MS.Size);
end;

{ TrmFMLFormulaContent }

function TrmFMLFormulaContent.AddItem(AType: TrmFMLItemType; const AValue: Variant; const AAddInfo: String = ''): TrmFMLFormulaItem;
begin
  Result := TrmFMLFormulaItem(Add);
  Result.FItemType := AType;
  Result.FValue := AValue;
  Result.AddInfo := AAddInfo;
  FExprType := rwvUnknown;
  Result.PrepareAggrFnctHolder;
end;

procedure TrmFMLFormulaContent.Assign(Source: TPersistent);
begin
  inherited;
  FExprType := TrmFMLFormulaContent(Source).FExprType;
end;

function TrmFMLFormulaContent.CheckErrors(var ErrItem: Integer): String;
var
  lCurrItem: TrmFMLFormulaItem;
  lPos: Integer;
  lExprType: TrwVarTypeKind;
  FEOFItem: TrmFMLFormulaItem;

  procedure Error(AErrNbr: Integer; APar: string = '');
  begin
    case AErrNbr of
      1: Result := 'Syntax error';
      2: Result := 'Object is not found';
      4: Result := 'Function is not found';
      5: Result := 'Expected "' + APar + '"';
      6: Result := 'Types mismatch';
      7: Result := 'Expected Integer type';
      8: Result := 'Expected String type';
      9: Result := 'Expected Numeric type';
      10: Result := 'Expected Date type';
      11: Result := 'Field is not found';
      12: Result := 'Expected Boolean type';
      13: Result := 'Variable is not found';
    end;

    raise ErwException.Create(Result);
  end;


  procedure GetNextToken;
  begin
    if lPos = Count - 1 then
      lCurrItem := FEOFItem
    else
    begin
      Inc(lPos);
      lCurrItem := Items[lPos];
    end;
  end;


  procedure CheckNextToken(AItemType: TrmFMLItemType; AValue: Variant);
  begin
    GetNextToken;
    if (lCurrItem.ItemType <> AItemType) or (VarType(lCurrItem.Value) <> VarType(AValue)) or (lCurrItem.Value <> AValue) then
      Error(5, AValue);
  end;

  procedure CheckType(const AType: TrwVarTypeKind; const ATypes: array of TrwVarTypeKind);
  var
    i: Integer;
  begin
    if AType = rwvVariant then
      Exit;

    for i := Low(ATypes) to High(ATypes) do
      if AType = ATypes[i] then
        Exit;

    case ATypes[Low(ATypes)] of
      rwvInteger:  Error(7);

      rwvString:   Error(8);

      rwvFloat,
      rwvCurrency: Error(9);

      rwvDate:     Error(10);

      rwvBoolean:  Error(12);
    end;
  end;

  function DefineMathOperType(const AType1,  AType2: TrwVarTypeKind): TrwVarTypeKind;
  begin
    Result := AType1;

    if AType1 = rwvVariant then
      Result := AType2

    else if AType1 <> AType2 then
      if AType2 in [rwvFloat, rwvCurrency, rwvDate] then
        Result := rwvFloat;
  end;


  procedure ExpressionLevel2(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel3(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel4(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel5(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel6(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel7(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel8(var AType: TrwVarTypeKind); forward;
  procedure ExpressionLevel9(var AType: TrwVarTypeKind); forward;



  (*         Logical operation of comparision
           { <element> {<|>|=|<>|>=|<=} <element> }
  *)

  procedure ExpressionLevel1(var AType: TrwVarTypeKind);
  var
    lOper: TrmFMLFormulaItem;
    t: TrwVarTypeKind;
  begin
    ExpressionLevel2(AType);

    lOper := lCurrItem;
    t := AType;

    if (lOper.ItemType = fitOperation) and (String(lOper.Value)[1] in ['>', '<', '=']) then
    begin
      CheckType(t, [rwvBoolean, rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString]);

      GetNextToken;
      ExpressionLevel2(AType);

      CheckType(AType, [rwvBoolean, rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString]);
      CheckTypeCompatibility(AType, t);
      t := rwvBoolean;
    end;

    AType := t;
  end;




  (*      <element>  is Additon and Subtraction for two terms
                 { <term>{+|-}<term> }
  *)
  procedure ExpressionLevel2(var AType: TrwVarTypeKind);
  var
    lOper: TrmFMLFormulaItem;
    t: TrwVarTypeKind;
  begin
    ExpressionLevel3(AType);

    lOper := lCurrItem;
    t := AType;

    while (lOper.ItemType = fitOperation) and (String(lOper.Value)[1] in ['+', '-']) do
    begin
      GetNextToken;
      ExpressionLevel3(AType);

      if lOper.Value = '+' then
      begin
        if t in [rwvFloat, rwvInteger, rwvCurrency, rwvDate, rwvString, rwvVariant] then
        begin
          if t = rwvString then
            CheckType(AType, [rwvString])

          else if (AType = rwvString) and (AType = rwvVariant) then
            t := rwvString

          else
          begin
            CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
            t := DefineMathOperType(t, AType);
          end;
        end
        else
          Error(6);
      end

      else
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        t := DefineMathOperType(t, AType);
      end;

      lOper := lCurrItem;
    end;

    AType := t;
  end;


  (*      <term>  is Multiplication or Division for two factors
                 { <factor> {*|/} <factor> }
  *)
  procedure ExpressionLevel3(var AType: TrwVarTypeKind);
  var
    lOper: TrmFMLFormulaItem;
    t: TrwVarTypeKind;
  begin
    ExpressionLevel4(AType);

    lOper := lCurrItem;
    t := AType;

    while (lOper.ItemType = fitOperation) and (String(lOper.Value)[1] in ['*', '/']) do
    begin
      GetNextToken;
      ExpressionLevel4(AType);

      if lOper.Value = '*' then
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        t := DefineMathOperType(t, AType);
      end

      else
      begin
        CheckType(t, [rwvFloat, rwvInteger, rwvCurrency, rwvDate]);
        CheckType(AType, [t, rwvFloat, rwvInteger, rwvCurrency]);
        t := rwvFloat;
      end;

      lOper := lCurrItem;
    end;

    AType := t;
  end;


  (*               Operation 'OR'
                { <factor> or <factor> }
  *)
  procedure ExpressionLevel4(var AType: TrwVarTypeKind);
  var
    lOper: TrmFMLFormulaItem;
  begin
    ExpressionLevel5(AType);

    lOper := lCurrItem;

    while (lOper.ItemType = fitOperation) and SameText(String(lOper.Value), 'OR') do
    begin
      CheckType(AType, [rwvBoolean]);
      GetNextToken;
      ExpressionLevel5(AType);
      CheckType(AType, [rwvBoolean]);
      lOper := lCurrItem;
    end;
  end;


  (*               Operation 'AND'
                { <factor> and <factor> }
  *)
  procedure ExpressionLevel5(var AType: TrwVarTypeKind);
  var
    lOper: TrmFMLFormulaItem;
  begin
    ExpressionLevel6(AType);

    lOper := lCurrItem;

    while (lOper.ItemType = fitOperation) and SameText(String(lOper.Value), 'AND') do
    begin
      CheckType(AType, [rwvBoolean]);
      GetNextToken;
      ExpressionLevel6(AType);
      CheckType(AType, [rwvBoolean]);
      lOper := lCurrItem;
    end;
  end;


  (*               Operation 'NOT'
                  { not <factor> }
  *)

  procedure ExpressionLevel6(var AType: TrwVarTypeKind);
  var
    flNot: Boolean;
  begin
    if (lCurrItem.ItemType = fitOperation) and SameText(String(lCurrItem.Value), 'NOT') then
    begin
      flNot := True;
      GetNextToken;
    end
    else
      flNot := False;

    ExpressionLevel7(AType);

    if flNot then
      CheckType(AType, [rwvBoolean]);
  end;


  (*             Unary - or +
                 { {+|-} <factor> }
  *)
  procedure ExpressionLevel7(var AType: TrwVarTypeKind);
  var
    f: Boolean;
  begin
    if (lCurrItem.ItemType = fitOperation) and (String(lCurrItem.Value)[1] in ['-', '+']) then
    begin
      f := True;
      GetNextToken;
    end
    else
      f := False;

    ExpressionLevel8(AType);

    if f then
      CheckType(AType, [rwvFloat, rwvInteger, rwvCurrency]);
  end;


  (*          Expression in round brackets
                  { ( <expression> ) }
  *)
  procedure ExpressionLevel8(var AType: TrwVarTypeKind);
  begin
    if (lCurrItem.ItemType = fitBracket) and (lCurrItem.Value = '(') then
    begin
      GetNextToken;
      ExpressionLevel1(AType);
      if not((lCurrItem.ItemType = fitBracket) and (lCurrItem.Value = ')')) then
        Error(5, ')');
      GetNextToken;
    end
    else
      ExpressionLevel9(AType);
  end;


  (*                <factor>
         { <identificator>|<Number>|<String> }
  *)
  procedure ExpressionLevel9(var AType: TrwVarTypeKind);
  var
    Fnct: TrwFunction;
    lType: TrwVarTypeKind;
    i: Integer;
    vt: TVarType;
    O: IrmDesignObject;
    fvHolder: IrmVarFunctHolder;
    Fld: IrwQBShowingField;
    v: IrmVariable;
  begin
    case lCurrItem.ItemType of
      fitError:
        Error(1);

      fitConst:
        begin
          vt := VarType(lCurrItem.Value);
          case vt of
            varString:    AType := rwvString;
            varInteger:   AType := rwvInteger;
            varDouble:    AType := rwvFloat;
            varDate:      AType := rwvDate;
            varNull:      AType := rwvVariant;
            varBoolean:   AType := rwvBoolean;
          end;
        end;

      fitVariable:
        begin
          O := TrwComponent(ComponentOwner).RootOwner;
          fvHolder := O.GetVarFunctHolder;
          if not Assigned(fvHolder) then
            Error(13);
          v := fvHolder.GetVariables.GetVariableByName(lCurrItem.Value);
          if not Assigned(v) then
            Error(13);
          AType := v.GetVarInfo.VarType;
        end;

      fitObject:
        begin
          if AnsiSameText(lCurrItem.Value, cParserSelf) then
            O := TrwComponent(ComponentOwner)
          else
            O := ComponentByPath(lCurrItem.Value, TrwComponent(ComponentOwner).RootOwner);
          AType := rwvVariant;
          if not Assigned(O) then
            Error(2);
        end;

      fitField:
        begin
          Fld := lCurrItem.GetQBField;
          if not Assigned(Fld) then
            Error(11);
          AType := Fld.GetDataType;
        end;

      fitFunction:
        begin
          Fnct := GlobalFindFunction(lCurrItem.Value);
          if not Assigned(Fnct) then
            Error(4);

          if Fnct.Params.Count = 0 then
            GetNextToken;

          AType := rwvUnknown;
          for i := 0 to Fnct.Params.Count - 1 do
          begin
            GetNextToken;
            ExpressionLevel1(lType);
            if not CheckTypeCompatibility(Fnct.Params[i].Descriptor.VarType, lType) then
              Error(6);

            if i = 0 then
              if (Fnct.Name = 'MIN') or (Fnct.Name = 'MAX') or (Fnct.Name = 'SUM') then  //an exception for aggrigate functions
                AType := lType;

            if i < Fnct.Params.Count - 1 then
              if not((lCurrItem.ItemType = fitSeparator) and (lCurrItem.Value = ',')) then
                Error(5, ',');
          end;

          if not((lCurrItem.ItemType = fitBracket) and (lCurrItem.Value = ')')) then
            Error(5, ')');

          if AType = rwvUnknown then
            AType := Fnct.Params.Result.VarType;
        end;

    else
      Error(1);
    end;

    GetNextToken;
  end;

begin
  Result := '';
  ErrItem := -1;
  FEOFItem := TrmFMLFormulaItem.Create(nil);
  try
    FEOFItem.ItemType := fitNone;
    FEOFItem.Value := '';
    FExprType := rwvUnknown;
    lPos := -1;
    GetNextToken;

    try
      ExpressionLevel1(lExprType);
      if lCurrItem.ItemType <> fitNone then
        Error(1);
      FExprType := lExprType
    except
      if lCurrItem.ItemType = fitNone then
        ErrItem := Count -1
      else
        ErrItem := lPos;
    end;
  finally
    FEOFItem.Free;
  end;
end;

function TrmFMLFormulaContent.CheckTypeCompatibility(AType1, AType2: TrwVarTypeKind): Boolean;
begin
  if (AType1 = rwvVariant) or (AType2 = rwvVariant) then
  begin
    Result := True;
    Exit;
  end;

  case AType1 of
    rwvInteger:  Result := AType2 in [rwvInteger, rwvFloat, rwvCurrency];

    rwvString:   Result := AType2 in [AType1, rwvBoolean];

    rwvBoolean:  Result := AType2 in [rwvBoolean];

    rwvDate:     Result := AType2 in [rwvDate, rwvFloat, rwvCurrency, rwvInteger];

    rwvFloat,
    rwvCurrency: Result := AType2 in [rwvFloat, rwvCurrency, rwvInteger, rwvDate];
  else
    Result := False;
  end;
end;

procedure TrmFMLFormulaContent.Clear;
begin
  inherited Clear;
  FExprType := rwvUnknown;
end;

procedure TrmFMLFormulaContent.ContentFromString(const AContent: String);
var
  Reader: TrwReader;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create(Length(AContent));
  MS.WriteBuffer(AContent[1], Length(AContent));
  MS.Position := 0;

  Reader := TrwReader.Create(MS.RealStream);
  try
    Reader.NextValue;
    Reader.ReadValue;
    Reader.ReadCollection(Self);
  finally
    Reader.Free;
  end;
end;

function TrmFMLFormulaContent.ContentToString: String;
var
  Writer: TrwWriter;
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  Writer := TrwWriter.Create(MS.RealStream, 1024);
  try
    Writer.WriteCollection(Self);
  finally
    Writer.Free;
  end;
  SetLength(Result, MS.Size);
  MS.Position := 0;
  MS.ReadBuffer(Result[1], MS.Size);
end;

function TrmFMLFormulaContent.GetExprType: TrwVarTypeKind;
var
  n: Integer;
begin
  if FExprType = rwvUnknown then
    CheckErrors(n);

  Result := FExprType;
end;

function TrmFMLFormulaContent.GetItem(Index: Integer): TrmFMLFormulaItem;
begin
  Result := TrmFMLFormulaItem(inherited Items[Index]);
end;

procedure TrmFMLFormulaContent.SetItem(Index: Integer; const Value: TrmFMLFormulaItem);
begin
  TrmFMLFormulaItem(inherited Items[Index]).Assign(Value);
end;

procedure TrmFMLFormula.SetActionInfo(const Value: String);
begin
  FActionInfo := Value;
  OnChange;
end;

procedure TrmFMLFormula.SetActionType(const Value: TrmFMLActionType);
begin
  FActionType := Value;
  OnChange;
end;

function TrmFMLFormulaContent.AggrFunctSkipThrough(AFnct: TrmFMLFormulaItem): Integer;
var
  i, Brk: Integer;
  Fml: TrmFMLFormulaContent;
begin
  Fml := TrmFMLFormulaContent(AFnct.Collection);
  Brk := 1;
  i := AFnct.Index + 1;
  while (Brk >= 1) and (i < Fml.Count) do
  begin
    if Fml[i].ItemType = fitBracket then
    begin
      if Fml[i].Value = '(' then
        Inc(Brk)
      else if Fml[i].Value = ')' then
        Dec(Brk);
    end

    else if Fml[i].ItemType = fitFunction then
      Inc(Brk);

    Inc(i);
  end;

  Result := i;
end;


function TrmFMLFormulaContent.ComponentOwner: TComponent;
begin
  Result := TrwComponent(TrmFMLFormulas(TrmFMLFormula(Owner).Collection).Owner);
end;


function TrmFMLFormulaContent.RWExpression(BeginIndex, EndIndex: Integer): String;
var
  i: Integer;
  h, h1: String;
  F: TrmFMLFormulaItem;
  V: Variant;
  Fld: IrwQBShowingField;
  O: IrmDesignObject;
begin
  Result := '';
  i := BeginIndex;
  while i <= EndIndex do
  begin
    F := TrmFMLFormulaItem(Items[i]);
    V := F.Value;
    if F.ItemType = fitConst then
    begin
      case VarType(V) of
        varNull:     h := 'Null';
        varDate:     h := 'Date(''' + VarToStr(V) + ''')';
        varString:   h := '''' + VarToStr(V) + '''';
        varBoolean:
            if Boolean(V) then
              h := 'True'
            else
              h := 'False';
      else
        h := VarToStr(V);
      end;
    end

    else if F.ItemType = fitVariable then
      h := VarToStr(V)

    else if F.ItemType = fitObject then
    begin
      if TrmFMLFormula(Owner).ActionType = fatDataFilter then
        h := 'AData'
      else
      begin
        h := VarToStr(V);
        if AnsiSameText(h, cParserSelf) then
          h := 'Self.' + F.AddInfo
        else
        begin
          GetNextStrValue(h, '.');
          h := cParserRootOwner + '.' + h + '.' + F.AddInfo;
        end;
      end;
    end

    else if F.ItemType = fitField then
    begin
      h := VarToStr(V);
      GetPrevStrValue(h, '.');
      O := ComponentByPath(h, TrwComponent(ComponentOwner).RootOwner, True);
      GetNextStrValue(h, '.');

      h1 := FindDataSetProp(O);
      if h = '' then
        raise ErwException.Create('Object "' + O.GetPathFromRootOwner + '" does not have TrwDataSet property');

      Fld := F.GetQBField;
      if not Assigned(Fld) then
        raise ErwException.Create('Object "' + TrwComponent(ComponentOwner).PathFromRootOwner + '": Cannot find the field "' + F.Value + '"');

      h := cParserRootOwner + '.' + h + '.' + h1 + '.Fields[' + IntToStr(Fld.GetIndex) + '].Value';
    end

    else if F.ItemType = fitError then
    begin
      Result := 'Null';
      break;
    end

    else if F.ItemType = fitFunction then
      if F.IsAggregateFunction then
      begin
        h := 'Self.CalcAggrFunctResult(''' + F.AggrFunctHolder.FunctionID + ''')';
        i := AggrFunctSkipThrough(F);
      end

      else
        h := VarToStr(V) + '('

    else if F.ItemType = fitSeparator then
      h := VarToStr(V) + ' '

    else if F.ItemType = fitOperation then
      h := ' ' + VarToStr(V) + ' '

    else
      h := VarToStr(V);

    if not F.IsAggregateFunction then
      Inc(i);

    Result := Result + h;
  end;
end;


{ TrmFMLFormulas }

function TrmFMLFormulas.AddFormula(AActionType: TrmFMLActionType; const AActionInfo: String): TrmFMLFormula;
begin
  Result := TrmFMLFormula(Add);
  Result.FActionType := AActionType;
  Result.FActionInfo := AActionInfo;
end;

function TrmFMLFormulas.AggrFunctHolderByID(const AFunctionID: String): TrmFMLAggrFunctHolder;
var
  i, j: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    for j := 0 to Items[i].Content.Count - 1 do
      if Items[i].Content[j].IsAggregateFunction and
         SameText(Items[i].Content[j].AggrFunctHolder.FunctionID, AFunctionID) then
      begin
        Result := Items[i].Content[j].AggrFunctHolder;
        break;
      end;
end;

function TrmFMLFormulas.CalcAggrFunctResult(const AFunctionID: String): Variant;
var
  Fnct: TrmFMLAggrFunctHolder;
begin
  Fnct := AggrFunctHolderByID(AFunctionID);
  if Assigned(Fnct) then
    Result := Fnct.Calculate
  else
    raise ErwException.Create('Cannot find aggregate function "' + AFunctionID + '"');
end;

procedure TrmFMLFormulas.Compile(ASyntaxCheck: Boolean = False);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Compile(ASyntaxCheck);
end;

procedure TrmFMLFormulas.EmptyAggregateFunctions;
var
  i, j: Integer;
begin
  for i := 0 to Count - 1 do
    for j := 0 to Items[i].Content.Count - 1 do
      if Items[i].Content[j].IsAggregateFunction then
        Items[i].Content[j].FAggrFunctHolder.ClearValues;
end;

function TrmFMLFormulas.FormulaByAction(AActionType: TrmFMLActionType; const AActionInfo: String): TrmFMLFormula;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if (Items[i].ActionType = AActionType) and SameText(Items[i].ActionInfo, AActionInfo) then
    begin
      Result := Items[i];
      break;
    end
end;

function TrmFMLFormulas.GetFormulaByAction(AActionType: TrmFMLActionType; const AActionInfo: String): IrmFMLFormula;
begin
  Result := FormulaByAction(AActionType, AActionInfo);
end;

function TrmFMLFormulas.GetItem(Index: Integer): TrmFMLFormula;
begin
  Result := TrmFMLFormula(inherited Items[Index]);
end;

procedure TrmFMLFormulas.PrepareForCalculation;
var
  i, j: Integer;
  cRoot: TrwComponent;

  function ParseAggregateFunction(AFnct: TrmFMLFormulaItem): Integer;
  var
    i, Brk: Integer;
    Fml: TrmFMLFormulaContent;
    C: TrwComponent;
    h, h1: String;
  begin
    Fml := TrmFMLFormulaContent(AFnct.Collection);
    Brk := 1;
    i := AFnct.Index + 1;
    while (Brk >= 1) and (i < Fml.Count) do
    begin
      case Fml[i].ItemType of
        fitBracket:
          begin
            if Fml[i].Value = '(' then
              Inc(Brk)
            else if Fml[i].Value = ')' then
              Dec(Brk);
          end;

        fitFunction:
          if not Fml[i].IsAggregateFunction then
            Inc(Brk);

        fitObject:
          begin
            if AnsiSameText(Fml[i].Value, cParserSelf) then
              C := TrwComponent(Owner)
            else
              C := ComponentByPath(Fml[i].Value, cRoot);
            Assert(Assigned(C));
            C.RegisterAggregateFunction(AFnct.AggrFunctHolder);
          end;

        fitField:
          begin
            h := Fml[i].Value;
            GetPrevStrValue(h, '.');
            C := ComponentByPath(h, cRoot);
            Assert(Assigned(C));
            h := FindDataSetProp(C);
            Assert(h1 = '');
            C := TrwComponent(Pointer(Integer(C.PGetPropertyValue(h))));
            Assert(Assigned(C));
            C.RegisterAggregateFunction(AFnct.AggrFunctHolder);
          end;
      end;

      if Fml[i].IsAggregateFunction then
        i := ParseAggregateFunction(Fml[i])
      else
        Inc(i);
    end;

    Result := i;
  end;

begin
  cRoot := TrwComponent(Owner).RootOwner;

  for i := 0 to Count - 1 do
    for j := 0 to Items[i].Content.Count - 1 do
      if Items[i].Content[j].IsAggregateFunction then
         ParseAggregateFunction(Items[i].Content[j]);
end;

procedure TrmFMLFormulas.SetItem(Index: Integer; const Value: TrmFMLFormula);
begin
  TrmFMLFormula(inherited Items[Index]).Assign(Value);
end;

function TrmFMLFormula.RWExpression: String;
begin
  Result := 'Result := ' + Content.RWExpression(0, Content.Count - 1) + ';';
end;

procedure TrmFMLFormula.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('CodeAddress', ReadCodeAddress, WriteCodeAddress, CodeAddress <> -1);
end;

procedure TrmFMLFormula.ReadCodeAddress(Reader: TReader);
begin
  FCodeAddress := Reader.ReadInteger;
end;

procedure TrmFMLFormula.WriteCodeAddress(Writer: TWriter);
begin
  Writer.WriteInteger(CodeAddress);
end;

procedure TrmFMLFormula.OnChange;
begin
  FCodeAddress := -1;
end;

procedure TrmFMLFormula.Compile(ASyntaxCheck: Boolean = False);
var
  h, h1: String;
  A, i: Integer;
begin
  h := RWExpression;

  if h = '' then
    FCodeAddress := -1

  else
  begin
    A := FCodeAddress;
    FCodeAddress := GetCompilingPointer + GetEntryPointOffset;

    if ActionType = fatDataFilter then
    begin
      if AnsiSameText(ActionInfo, 'Text') then
        h1 := 'AData: String';
    end
    else
      h1 := '';

    h := 'function CalcFormula(' + h1 + '): Variant' + #13#10 + h + #13#10 + 'end' + #0;
    CompileHandler(TrwComponent(Collection.Owner), h);
    if ASyntaxCheck then
      FCodeAddress := A;

    for i := 0 to Content.Count - 1 do
      if Content[i].IsAggregateFunction then
        CompileAggrFunction(Content[i], ASyntaxCheck);
  end;
end;

function TrmFMLFormula.Calculate(const AParams: array of Variant): Variant;
var
  C: TrwComponent;
  EP: TrwVMAddress;
  i: Integer;
begin
  C := TrwComponent(Collection.Owner);

  try
    for i := Low(AParams) to High(AParams) do
      VM.Push(AParams[i]);

    EP := GetEntryPoint(CodeAddress);
    VM.Push(Integer(C));
    VM.Run(EP);
    Result := VM.RegA;
  except
    on E: Exception do
    begin
      E.Message := 'Formula calculation error for ' + C.PathFromRootOwner + #13 + E.Message;
      raise;
    end;
  end;
end;



{ TrmFMLAggrFunctHolder }

constructor TrmFMLAggrFunctHolder.Create(AOwner: TPersistent);
begin
  inherited;
  AddValCodeAddress := -1;
  FunctionID := FormatDateTime('ddmmhhsszzz', Now) + IntToStr(Random(MaxInt));
  ClearValues;
end;

procedure TrmFMLAggrFunctHolder.AddValue(const Params: Variant);
var
  fnct: String;

  procedure _MINpar;
  begin
    if Params[1] then
      if not VarIsNull(Params[0]) and (( FValueCounter = 0 ) or (FCurrentValue > Params[0])) then
      begin
        FCurrentValue := Params[0];
        Inc(FValueCounter);
      end;
  end;

  procedure _MAXpar;
  begin
    if Params[1] then
      if not VarIsNull(Params[0]) and (( FValueCounter = 0 ) or (FCurrentValue < Params[0])) then
      begin
        FCurrentValue := Params[0];
        Inc(FValueCounter);
      end;
  end;

  procedure _COUNTpar;
  var
    i: Integer;
    fl: Boolean;
  begin
    if Params[1] and not VarIsNull(Params[0]) then
    begin
      fl := True;
      if Params[2] then
      begin
        for i := Low(FCurrentValues) to High(FCurrentValues) do
          if FCurrentValues[i] = Params[0] then
          begin
            fl := False;
            break;
          end;

        if fl then
        begin
          SetLength(FCurrentValues, Length(FCurrentValues) + 1);
          FCurrentValues[High(FCurrentValues)] := Params[0];
        end;
      end;

      if fl then
        Inc(FValueCounter);
    end;
  end;

  procedure _SUMpar;
  begin
    if Params[1] then
      if not VarIsNull(Params[0]) or (VarType(Params[0]) in [varSmallint..varCurrency, varShortInt, varInt64]) then
      begin
        if FValueCounter = 0 then
          FCurrentValue := Params[0]
        else
          FCurrentValue := FCurrentValue + Params[0];
        Inc(FValueCounter);          
      end;
  end;

  procedure _AVGpar;
  begin
    _SUMpar;
  end;

begin
  fnct := UpperCase(TrmFMLFormulaItem(Owner).Value);

  if fnct = 'SUM' then
    _SUMpar
  else if fnct = 'MIN' then
    _MINpar
  else if fnct = 'MAX' then
    _MAXpar
  else if fnct = 'AVG' then
    _AVGpar
  else if fnct = 'COUNT' then
    _COUNTpar
  else
    raise ErwException.Create('Aggregate function "' + fnct + '" is not defined');
end;

function TrmFMLAggrFunctHolder.Calculate: Variant;
begin
  Result := CalcValue;
end;

function TrmFMLAggrFunctHolder.CalcValue: Variant;
var
  fnct: String;

  procedure _SIMPLEcalc;
  begin
    if FValueCounter > 0 then
      Result := FCurrentValue;
  end;

  procedure _COUNTcalc;
  begin
    Result := FValueCounter;
  end;

  procedure _AVGcalc;
  begin
    if FValueCounter > 0 then
      Result := FCurrentValue / FValueCounter;
  end;


begin
  Result := Null;

  fnct := UpperCase(TrmFMLFormulaItem(Owner).Value);

  if AnsiSameStr(fnct, 'COUNT') then
    _COUNTcalc
  else if AnsiSameStr(fnct, 'AVG') then
    _AVGcalc
  else
    _SIMPLEcalc;
end;

procedure TrmFMLAggrFunctHolder.ClearValues;
begin
  FValueCounter := 0;
  FCurrentValue := Null;
  SetLength(FCurrentValues, 0);
  FChanging := False;
end;

procedure TrmFMLAggrFunctHolder.NotifyOfValueChanges;
var
  C: TrwComponent;
  EP: TrwVMAddress;
begin
  if FChanging then
    Exit;

  FChanging := True;
  try
    // Calc scalar Expressions

    C := TrwComponent(TrmFMLFormulaContent(TrmFMLFormulaItem(Owner).Collection).ComponentOwner);
    EP := GetEntryPoint(AddValCodeAddress);
    VM.Push(Integer(C));
    VM.Run(EP);

    // and store it into array;
    AddValue(VM.RegA);
    
  finally
    FChanging := False;
  end;
end;

procedure TrmFMLFormula.CompileAggrFunction(AFnct: TrmFMLFormulaItem; ASyntaxCheck: Boolean = False);
var
  h: String;
  i: Integer;
  A: Integer;
begin
  i := TrmFMLFormulaContent(AFnct.Collection).AggrFunctSkipThrough(AFnct);
  h := TrmFMLFormulaContent(AFnct.Collection).RWExpression(AFnct.Index + 1, i - 2);

  if h = '' then
    AFnct.AggrFunctHolder.AddValCodeAddress := -1

  else
  begin
    A := AFnct.AggrFunctHolder.AddValCodeAddress;
    AFnct.AggrFunctHolder.AddValCodeAddress := GetCompilingPointer + GetEntryPointOffset;
    h := 'function CalcAggrFunctParams(): Array' + #13#10 +
          'Result := [' + h + '];' + #13#10 +
          'end' + #0;
    CompileHandler(TrwComponent(Collection.Owner), h);
    if ASyntaxCheck then
      AFnct.AggrFunctHolder.AddValCodeAddress := A;
  end;
end;

function TrmFMLFormula.GetActionInfo: String;
begin
  Result := FActionInfo;
end;

function TrmFMLFormula.GetActionType: TrmFMLActionType;
begin
  Result := FActionType;
end;

function TrmFMLFormula.AddItem(const AType: TrmFMLItemType; const AValue: Variant;  const AddInfo: String = ''; const AIndex: Integer = -1): Integer;
var
  Itm: TrmFMLFormulaItem;
begin
  Itm := Content.AddItem(AType, AValue, AddInfo);

  if AIndex <> -1 then
    Itm.Index := AIndex;

  Result := Itm.Index;
end;

procedure TrmFMLFormula.Clear;
begin
  Content.Clear;
end;

function TrmFMLFormula.GetItemInfo(const AItemIndex: Integer): TrmFMLItemInfoRec;
begin
  Result.ItemType := Content[AItemIndex].ItemType;
  Result.Value := Content[AItemIndex].Value;  
end;

function TrmFMLFormula.GetItemCount: Integer;
begin
  Result := Content.Count;
end;


function TrmFMLFormula.ThereAreObjectsBelongingTo(const AObjectParentPath: String): Boolean;
var
  i: Integer;
  O: IrmDesignObject;
  h: String;
begin
  Result := False;

  for i := 0 to Content.Count - 1 do
  begin
    if Content.Items[i].ItemType = fitObject then
    begin
      h := VarToStr(Content.Items[i].Value);
      if AnsiSameText(h, cParserSelf) then
        O := TrwComponent(Collection.Owner)
      else
        O := ComponentByPath(h, TrwComponent(Collection.Owner).RootOwner);

      h := O.GetPathFromRootParent;
    end

    else if Content.Items[i].ItemType = fitField then
      h := VarToStr(Content.Items[i].Value)

    else
      Continue;

//    GetNextStrValue(h, '.');

    if AnsiSameText(Copy(h, 1, Length(AObjectParentPath)), AObjectParentPath) then
    begin
      Result := True;
      Break;
    end;
  end;
end;

end.
