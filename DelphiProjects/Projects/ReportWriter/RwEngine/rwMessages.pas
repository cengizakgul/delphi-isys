unit rwMessages;

interface

resourcestring

                           { Errors }
  ResErrWrongEvent = 'Event "%s" is not registered for class "%s"';
  ResNotUserEvent = 'Event "%s" is not an user event';
  ResErrWrongInheritedEvent = 'Inherited event "%s" is not registered for class "%s"';
  ResErrEmptyProperty = 'Property "%s" is not defined for report "%s"';
  ResErrWrongClass = 'Class "%s" is not registered';
  ResErrNDefClProc = 'Procedure "%s" is not defined for class "%s"';
  ResErrWrongLibComp = 'Library component "%s" is not registered';
  ResErrWrongParams = 'Parameters is wrong';
  ResErrWrongBufferStructure = 'Structure of buffer "%s" is not correct';
  ResErrWrongWizFldFormat = 'Format for wizard''s field "%s" is wrong (Component is "%s")';
  ResErrCircularReference = 'Circular reference';
  ResErrSQL = 'Check DataSource "%s"';
  ResErrWrongMacroName = 'Macro "%s" is not found';
  ResErrASCIIFileWrongParams = 'Format of ASCII-file is wrong';
  ResErrWrongParameterName = 'Parameter "%s" is not found';
  ResErrWrongFieldName = 'Field "%s" is not found';
  ResErrDataSetIsNotActive = 'DataSet is not active';
  ResErrDataSetIsNotEdit = 'DataSet is not in edit mode';


  ResErrSyntaxError = 'Syntax error';
  ResErrNBalRB = 'Not balanced round brackets';
  ResErrNClQuotes = 'Not closed quotes';
  ResErrNDefVar = 'Variable "%s" is not defined';
  ResErrWrongForCounter = 'Loop counter must be local Integer variable';
  ResErrAlreadyDefVar = 'Variable "%s" is already defined';
  ResErrNDefFunc = 'Function "%s" not defined';
  ResErrNDefPar = 'Report parameter "%s" is not defined';
  ResErrNDefProc = 'Procedure or function "%s" is not defined';
  ResErrNDefProp = 'Property or method "%s" for class "%s" is not defined';
  ResErrNExpToken = 'Expected "%s" but "%s" met';
  ResErrWrongNumb = 'Wrong number "%s"';
  ResErrExpInteger = 'Expression type must be Integer';
  ResErrExpFloat = 'Expression type must be Float';
  ResErrExpBoolean = 'Expression type must be Boolean';
  ResErrExpString = 'Expression type must be String';
  ResErrExpDate = 'Expression type must be Date';
  ResErrExpObject = 'Expression type must be Pointer';
  ResErrExpArray = 'Expression type must be Array';
  ResErrIncompatibleTypes = 'Incompatible types: "%s" and "%s"';
  ResErrExpVarName = 'Expected variable name';
  ResErrWrongIndex = 'Index %s is out of "%s" array bounds';
  ResErrWrongStrIndex = 'Index %s is out of "%s" string bounds';
  ResErrNDefConst = 'Constant "%s" is not defined';
  ResErrUnknSymbol = 'Unknown symbol "%s"';
  ResErrUnknIdentif = 'Unknown identificator "%s"';
  ResErrWrongType = 'Types mismatch';
  ResErrTypeIsIncorrect = 'Type "%s" is incorrect';
  ResErrMismParams = 'Parameters mismatch';
  ResErrInheritedChangeName = 'You can''t change name-property for inherited component';
  ResErrEmptyName = 'Property Name must be not empty';
  ResErrInheritedDelete = 'You cannot delete inherited component';
  ResErrInheritedChange = 'You cannot change this property of inherited component';
  ResErrRWProcDelete = 'You cannot delete property write/read procedure';
  ResErrInheritedUserEventDel = 'You cannot unregister inherited user event';
  ResErrInheritedUserPropDel = 'You cannot unregister inherited user property';
  ResErrUfinishedText = 'Text is not finished. Expected operator End, EndIf, EndFor e.t.c.';
  ResErrLeftSideCannotAssign = 'Left side cannot be assigned';
  ResErrAbstractPointer = 'Abstract pointer cannot be used for this operation';
  ResErrInternalCompileError = 'Internal compile error';
  ResErrNotApplicableInstr = 'Instruction is not applicable on this place';
  ResErrNoIndexProperty = 'Object does not have index property';
  ResErrAccessDenied = 'You do not have rights to access to this report';

  ResErrCreateCustomObject = 'Cannot create custom object. Class "%s" is not registered';

  ResErrTableNotFound = 'Virtual table "%s" is not found';
  ResErrTableDoesNotParams = 'Virtual table "%s" does not have any parameters';
  ResErrTableRequiresParams = 'Virtual table "%s" requires parameters';
  ResErrWrongOperationForTable = 'Operation is not applicable for table "%s"';
  ResErrWrongAlias = 'Wrong alias. Cannot use virtual table name as alias';
  ResErrBufferExists = 'Buffer "%s" already exists';
  ResErrAttrNotDefined = 'Attribute "%s" of virtual table is not defined';
  ResErrExpTable = 'Expected virtual table';
  ResErrCacheTable = 'Table "%s" is not in a cache';
  ResErrInternalLQError = 'Internal logical queries error';
  ResErrConsolPrimCo = 'Data consolidation error! Primary company has been set up incorrectly';
  ResErrWrongLogKeyType = 'Data consolidation error! Type is not applicable for operation';

  ResErrLibCompExists = 'Component "%s" already exists';
  ResErrLibFunctExists = 'Function "%s" already exists';
  ResErrPropertyExists = 'Property "%s" already exists';


  ResErrNeverEndingPrintLoop = 'Infinite print loop for band "%s" has been terminated';


                     { Captions and texts }
  ResTxtBandDetail = 'Detail';
  ResTxtBandPageHeader = 'Page Header';
  ResTxtBandTitle = 'Title';
  ResTxtBandHeader = 'Header';
  ResTxtBandGroupHeader = 'Group Header';
  ResTxtBandGroupFooter = 'Group Footer';
  ResTxtBandFooter = 'Footer';
  ResTxtBandSummary = 'Summary';
  ResTxtBandPageFooter = 'Page Footer';
  ResTxtBandPageStyle = 'Page Style';
  ResTxtBandSubDetail = 'SubDetail';
  ResTxtBandSubSummary = 'SubSummary';
  ResTxtBandCustom = 'Custom';


implementation

end.
