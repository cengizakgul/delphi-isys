unit rwPDFPrinter;

interface

uses Classes, Types, rwPrintPage, rwPrintElements, PDF, rwTypes, rwGraphics, dynapdf, SysUtils;

type
  TrwPDFPrinterPage = class(TrwVirtualPage)
  private
    FPDFPrinter: TPDFDocument;
    FPDFBackgrounds: TStringList;
    FDynaPDFPrinter: TPDF;
    FDynaAmoutOfPrintedObj: integer;

    procedure SetPaperSize(APaperSize: TrwPaperSize; ALength: Integer; AWidth: Integer; AOrientation: TrwPaperOrientation);
    procedure SetDynaAmoutOfPrintedObj(const Value: integer);

  public
    property PDFBackgrounds : TStringList read FPDFBackgrounds;
    procedure Print;
    procedure DynaImportPageFromMemoryStream(AStream : TMemoryStream);
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    property    PDFPrinter: TPDFDocument read FPDFPrinter;
    property DynaPDFPrinter : TPDF read FDynaPDFPrinter;
    property DynaAmoutOfPrintedObj : integer read FDynaAmoutOfPrintedObj write SetDynaAmoutOfPrintedObj;
  end;

implementation

{ TrwPDFPrinterPage }

constructor TrwPDFPrinterPage.Create(AOwner: TComponent);
begin
  inherited;

  FPDFBackgrounds := TStringList.Create;

  FPDFPrinter := TPDFDocument.Create(nil);

  FPDFPrinter.DocumentInfo.Author := 'Evolution';
  FPDFPrinter.DocumentInfo.Creator := 'Evolution';
  FPDFPrinter.DocumentInfo.Keywords := 'Evolution';
  FPDFPrinter.DocumentInfo.Title := 'Evolution Report';

  FDynaAmoutOfPrintedObj := 0;
  try
    FDynaPDFPrinter := TPDF.Create;
    //FDynaPDFPrinter.SetLicenseKey('12D0C40A50D80C70D30BC0CF0A00B70100B70E60CB');
    //update to version 3
    FDynaPDFPrinter.SetLicenseKey('1490866-17092015-2-0-0-FE81A17C43EFB2EB80C0322B527BAEF9-B8963627172F67FFF1FAC2C75B88F567');

    FDynaPDFPrinter.SetDocInfoA(diCreator, 'Evolution');
    FDynaPDFPrinter.SetDocInfoA(diTitle, 'Evolution');
    FDynaPDFPrinter.SetDocInfoA(diAuthor, 'Evolution');
    FDynaPDFPrinter.SetDocInfoA(diKeywords, 'Evolution');
    FDynaPDFPrinter.SetViewerPreferences(vpDisplayDocTitle, avNone);
    FDynaPDFPrinter.SetImportFlags(ifImportAll + ifImportAsPage);
  except
    on E:Exception do
    begin
      FDynaPDFPrinter := nil;
      { TODO : add message? }
    end;
  end;
end;


destructor TrwPDFPrinterPage.Destroy;
begin
  FPDFBackgrounds.Clear;
  FPDFBackgrounds.Free;
  FPDFPrinter.Free;

  if Assigned(FDynaPDFPrinter) then
    FDynaPDFPrinter.Free;

  inherited;
end;

procedure TrwPDFPrinterPage.DynaImportPageFromMemoryStream(
  AStream: TMemoryStream);
begin
  if FDynaPDFPrinter.OpenImportBuffer(AStream.Memory, AStream.Size, dynapdf.ptOpen, '') <> 0 then
    raise Exception.Create('Using DynaPDF. Cannot OpenImportBuffer');
  if FDynaPDFPrinter.ImportPageEx(1, 1.0, 1.0) < 0 then
    raise Exception.Create('Using DynaPDF. Cannot ImportPage');
  FDynaPDFPrinter.CloseImportFile;
  Inc(FDynaAmoutOfPrintedObj);
end;

procedure TrwPDFPrinterPage.Print;
var
  n: Word;
  p: Integer;
  C: TrwPrintObject;
  i: Integer;
begin
  FPDFBackgrounds.Clear;

  FScaleFonts := True;

  FPDFPrinter.Resolution := cScreenCanvasRes;
  ChangeZoom(cScreenCanvasRes/PixelsPerInch, cScreenCanvasRes/PixelsPerInch);

  if FPDFPrinter.Printing then
    FPDFPrinter.NewPage
  else
    PDFPrinter.BeginDoc;

  if Assigned(FDynaPDFPrinter) then
    if not DynaPDFPrinter.Append then
      raise Exception.Create('Using DynaPDF. Cannot Append');

  try
    FCanvas := FPDFPrinter.Canvas;

    SetPaperSize(PageSize, System.Round(Height*Kf_ZoomY), System.Round(Width* Kf_ZoomY), PageOrientation);

    FData.Position := 0;
    while FData.Position < FData.Size do
    begin
      FData.ReadBuffer(n, SizeOf(n));
      p := FData.Position;
      C := TrwPrintObject(Repository.Components[n]);
      if C.Visible then
        C.Paint(FCanvas, Self)
      else
        C.PrepareToPaint(FData);  //skip object data
      if FData.Position <= p then
        break;
    end;

  //marks
    for i := 0 to Header.Marks.Count -1 do
      if Header.Marks[i].PageNum = PageNum then
        DrawMark(Header.Marks[i], FCanvas);
  finally
    if Assigned(FDynaPDFPrinter) then
      if not DynaPDFPrinter.EndPage then
        raise Exception.Create('Using DynaPDF. Cannot ClosePage');
  end;
end;


procedure TrwPDFPrinterPage.SetDynaAmoutOfPrintedObj(const Value: integer);
begin
  FDynaAmoutOfPrintedObj := Value;
end;

procedure TrwPDFPrinterPage.SetPaperSize(APaperSize: TrwPaperSize; ALength, AWidth: Integer; AOrientation: TrwPaperOrientation);
begin
  with FPDFPrinter.CurrentPage do
  begin
    case APaperSize of
      psExecutive:
      begin
        Size := PDF.psExecutive;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(522) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(756) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;
      end;
      psLetter:
      begin
        Size := PDF.psLetter;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfUS_Letter);
      end;
      psLegal:
      begin
        Size := PDF.psLegal;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfUS_Legal);
      end;
      psA3:
      begin
        Size := PDF.psA3;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfDIN_A3);
      end;
      psA4:
      begin
        Size := PDF.psA4;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfDIN_A4);
      end;
      psA5:
      begin
        Size := PDF.psA5;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfDIN_A5);
      end;
      psB4:
      begin
        Size := PDF.psB4;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfDIN_B4);
      end;
      psB5:
      begin
        Size := PDF.psB5;
        if Assigned(FDynaPDFPrinter) then
          FDynaPDFPrinter.SetPageFormat(pfDIN_B5);
      end;
      psFolio:
      begin
        Size := PDF.psFolio;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(612) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(936) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
      psQuarto:
      begin
        Size := PDF.psFolio;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(612) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(936) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
      psEnv9:
      begin
        Size := PDF.psEnv9;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(279) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(639) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
      psEnv10:
      begin
        Size := PDF.psEnv10;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(297) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(684) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
      psEnv11:
      begin
        Size := PDF.psEnv11;
        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(324) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');
          if not DynaPDFPrinter.SetPageHeight(747) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
    else
      begin
        Size := PDF.psUserDefined;
        Width := AWidth;
        Height := ALength;

        if Assigned(FDynaPDFPrinter) then
        begin
          if not DynaPDFPrinter.SetPageWidth(AWidth) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageWidth');

          if not DynaPDFPrinter.SetPageHeight(ALength) then
            raise Exception.Create('Using DynaPDF. Cannot SetPageHeigth');
        end;    
      end;
    end;

    case AOrientation of
      rpoPortrait:
      begin
        Orientation := poPagePortrait;
        if Assigned(FDynaPDFPrinter) then
          if not DynaPDFPrinter.SetOrientation(0) then
            raise Exception.Create('Using DynaPDF. Cannot SetOrientation');
      end;
      rpoLandscape:
      begin
        Orientation := poPageLandscape;
        if Assigned(FDynaPDFPrinter) then
          if not DynaPDFPrinter.SetOrientation(90) then
            raise Exception.Create('Using DynaPDF. Cannot SetOrientation');
      end;
    end;
  end;
end;

end.
