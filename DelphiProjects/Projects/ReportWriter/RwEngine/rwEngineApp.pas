unit rwEngineApp;

interface

uses
  Windows, Messages, Classes, SysUtils, Controls, Variants, Forms, rwConnectionInt,
  isLocale, EvStreamUtils, rwEngineTypes, rwBasicClasses, rmTypes, rwRTTI,
  ISErrorUtils, SyncObjs, rwEngine, rwAdapterUtils, isBasicUtils, isBaseClasses,
  Dialogs, isLogFile;

type
  TrwProcessController = class;

  TrwEngineApp = class (TisInterfacedObject, IrwEngineApp)
  private
    FAdapterModule: THandle;
    FAdapter: IrwAppAdapter;
    FProcessController: TrwProcessController;
    FQueryBuilderFormHolder: IrwDesignerForm;
    FActiveReport: IrwReportStructure;
    procedure LoadAdapter;
    procedure UnloadAdapter;

    procedure OpenReportFromClass(const AClassName: String);
    procedure OpenReportFromStream(const AStream: IevDualStream);
    function  RunActiveReport(const ARunSettings: TrwRunReportSettings): TrwReportResult;
    function  GetActiveReport: IrwReportStructure;
    procedure CloseActiveReport;
    function  GetProcessController: IrwProcessController;
    procedure InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
    function  RWAtoPDF(const ARWAData: IevDualStream; const APDFInfo: TrwPDFFileInfo): IevDualStream;
    function  RunReport(const AReportData: IevDualStream; const AReportName: String; out AReportResult: IevDualStream): Variant;
    function  CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
    function  GetClassInfo(const AClassName: String): TrwClassInfo;
    function  ClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function ChangeRWAInfo(const ARWAData: IevDualStream; const ACurrentPassword: String;
                            const ANewInfo: TrwRWAFileInfo) : IevDualStream;
    procedure SetExternalModuleData(const AModuleName: String; const AData: IevDualStream);
    function  OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  GetMainWindowHandle: Integer;
    function  RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
    function  QueryInfo(const AItemName: String): String;
    function  OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
    function  IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure Terminate;
  end;


  TrwReportStructure = class;

  TrwReportParameters = class (TisInterfacedObject, IrwReportParameters)
  private
    FParams: TrwReportParamList;

    function  GetCount: Integer;
    function  ParamByName(const AParamName: String): TrwReportParam;
    function  IndexOf(const AParamName: String): Integer;
    function  GetItems(const AIndex: Integer): TrwReportParam;
    function  GetParamValue(const AParamName: String): Variant;
    procedure SetParamValue(const AParamName: String; const AValue: Variant);
    procedure ParamsFromStream(const AParams: IevDualStream);
    function  ParamsToStream: IevDualStream;

    property  Count: Integer read GetCount;
    property  Items[const Index: Integer]: TrwReportParam read GetItems;
  end;


  TrwReportInputForm = class (TisInterfacedObject, IrwReportInputForm)
  private
    FReportStructure: TrwReportStructure;
    FrwInputForm: IrwInputForm;

    procedure CloseOK;
    procedure CloseCancel;
    function  Show(const AParentWndHandle: Integer; const ASetupMode: Boolean): Boolean;
    function  GetFormBounds: TrwBounds;
    procedure SetFormBounds(const ABounds: TrwBounds);
  public
    constructor Create(const ReportStructure: TrwReportStructure);
  end;


  TrwReportStructure = class(TisInterfacedObject, IrwReportStructure)
  private
    FData: IEvDualStream;
    FReportParams: IrwReportParameters;
    FReportInputForm: IrwReportInputForm;

    function  GetReportParameters: IrwReportParameters;
    function  GetData: IevDualStream;
    function  GetReportInputForm: IrwReportInputForm;
    function  GetStructureInfo: TrwReportStructureInfo;
    procedure Compile;
    procedure PrepareForRunning;
  public
    constructor CreateEmpty(const AOwner: IrwEngineApp);
    constructor CreateFromStream(const AOwner: IrwEngineApp; const ASource: IevDualStream);
    constructor CreateFromClass(const AOwner: IrwEngineApp; const AClassName: String);
  end;


  TrwDebugLineInfo = record
    Segment:            TrwVMCodeSegment;
    FirstAddress:       Integer;
    LastAddress:        Integer;
    ClassName:          String;
    ObjectLocation:     String;
    FunctionName:       String;
    SourceCodeLineNbr:  Integer;
    SourceCodeLineTxt:  String;
  end;


  TrwDebugSymbolsInfo = record
    Segment:            TrwVMCodeSegment;
    FirstAddress:       Integer;
    LastAddress:        Integer;
    DebSymbols:         String;
  end;


  PTrwDebugLineInfo = ^TrwDebugLineInfo;

  TrwDebugLineList = array of TrwDebugLineInfo;
  PTrwDebugLineList = ^TrwDebugLineList;

  TrwDebugSymbolsList =  array of TrwDebugSymbolsInfo;
  PTrwDebugSymbolsList = ^TrwDebugSymbolsList;



  TrwDebugSourceCodeInfo = record
    Report:                   TrwDebugLineList;
    ReportDebSymbols:         TrwDebugSymbolsList;
    LibFunctions:             TrwDebugLineList;
    LibFunctionsDebSymbols:   TrwDebugSymbolsList;
    LibComponents:            TrwDebugLineList;
    LibComponentsDebSymbols:  TrwDebugSymbolsList;
  end;


  TrwDebugStopLineInfo = record
    Address:            TrwVMAddress;
    SorceCodeLineInfo:  PTrwDebugLineInfo;
  end;


  TrwProcessController = class
  private
    FProxy: IrwProcessController;
    FCS: TCriticalSection;
    FReportData: IEvDualStream;

    FExceptionText: String;
    FDebugSourceCode: TrwDebugSourceCodeInfo;
    FBreakPoints: TrwVMAddressList;
    FState: TrwVMStateType;
    FRunAction: TrwRunAction;
    FLastStopPoint: TrwDebugStopLineInfo;
    FLastStackDepth: Integer;

    procedure SetDebuggingState(Debugging: WordBool);
    procedure InitializeDebugInfo;
    procedure VMDebugInterruption;
    procedure VMDebugOnError(const AErrorMessage: String);
    procedure WaitForDebuggerEvent;
    procedure WaitInPauseLoop;
    procedure CleanupDebugEvents;
    procedure BeforeExecuteVMInstruction;
    function  FindDebugLine(const AAddress: TrwVMAddress): PTrwDebugLineInfo;

    function  GetState: TrwVMStateType;
    procedure SetState(const AValue: TrwVMStateType);
    function  GetRunAction: TrwRunAction;
    procedure SetRunAction(const AValue: TrwRunAction);
    function  GetExceptionText: String;
    procedure SetExceptionText(const AValue: String);

    procedure Run(ARunAction: TrwRunAction);
    procedure Pause;
    procedure Stop;
    function  GetVMRegisters: TrwVMRegisters;
    function  GetVMCallStack: Variant;
    function  CalcExpression(const Expression: String; const DebugSymbolInfo: String): TrwVMExpressionResult;
    procedure SetBreakpoints(const Breakpoints: Variant);
    function  DoShowQB(AData: TStream; ADescriptionView: Boolean): Boolean;

    property  State: TrwVMStateType read GetState write SetState;
    property  RunAction: TrwRunAction read GetRunAction write SetRunAction;
    property  ReportData: IEvDualStream read FReportData write FReportData;
    property  ExceptionText: String read GetExceptionText write SetExceptionText;

  public
    constructor Create;
    destructor Destroy; override;
  end;



  TrwDebuggerRequest = (rwDRUnknown, rwDRGetRegisters, rwDRGetStack, rwDRCalcExpression, rwDRShowQueryBuilder);

  PTrwVMRegisters = ^TrwVMRegisters;
  PTrwVMExpressionResult = ^TrwVMExpressionResult;

  TrwDebuggerProxyRequest = packed record
    Request: TrwDebuggerRequest;
    ThreadID: Integer;
    case TrwDebuggerRequest of
       rwDRGetRegisters:    (outRegisters: PTrwVMRegisters);
       rwDRGetStack:        (outStack: PVariant);
       rwDRCalcExpression:  (inExpression: PString;
                             inDebugSymbolInfo: PString;
                             outExpression: PTrwVMExpressionResult);
       rwDRShowQueryBuilder: (QBData: TStream;
                              inDescriptionView: PWordBool);
  end;

  PTrwDebuggerProxyRequest = ^TrwDebuggerProxyRequest;


  TrwProcessControllerProxy = class (TisInterfacedObject, IrwProcessController)
  private
    FOwner: TrwProcessController;
    FRequestInfo: TrwDebuggerProxyRequest;

    procedure RunRequest;
    procedure NotifyEngine;

    // interface thread safe calls (ALWAYS NOT IN MAIN THREAD)
    procedure Run(const ARunAction: TrwRunAction);
    procedure Pause;
    procedure Stop;
    function  GetVMRegisters: TrwVMRegisters;
    function  GetState: TrwProcessState;
    function  CalcExpression(const AExpression: String; const ADebugSymbolInfo: String): TrwVMExpressionResult;
    function  GetVMCallStack: Variant;
    procedure SetBreakpoints(const ABreakpoints: Variant);
    procedure OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean);
  public
    constructor Create(const AOwner: TrwProcessController);
  end;


implementation

uses rwUtils, rwParser, rwVirtualMachine, rwTypes, rwBasicUtils, rmReport, rwReport, rwMessages,
     rwEngineMainFrm, ISDataAccessComponents;

const WM_RMDEBUG_REQUEST = WM_RMDEBUG + 1;

{ TrwEngineApp }

procedure TrwEngineApp.AfterConstruction;
begin
//  ShowMessage('Ready to work...');
  FProcessController := TrwProcessController.Create;
  inherited;
end;

procedure TrwEngineApp.BeforeDestruction;
begin
  FreeAndNil(FProcessController);
  UnloadAdapter;
  inherited;
end;

function TrwEngineApp.CallAppAdapterFunction(const AFunctionName: String; const AParams: Variant): Variant;
begin
  Result := RWEngineExt.AppAdapter.DispatchCustomFunction(AFunctionName, AParams);
end;

function TrwEngineApp.ChangeRWAInfo(const ARWAData: IevDualStream; const ACurrentPassword: String;
  const ANewInfo: TrwRWAFileInfo): IevDualStream;
var
  PreviewInfo: TrwRWAInfoRec;
  SecInfo: TrwRWASecurityRec;
begin
  with PreviewInfo do
  begin
    Name := '';
    Data1 := nil;
    Data2 := nil;
    Data3 := nil;
    Data4 := '';
    SecuredMode := False;
    PreviewPswd := '';
    PrintPswd := '';
    with PrintJobInfo do
    begin
      PrintJobName := '';
      PrinterName := '';
      TrayName := '';
      OutBinNbr := 0;
      Duplexing := rdpDefault;
      PrintPCLAsGraphic := False;
      PrinterVerticalOffset := 0;
      PrinterHorizontalOffset := 0;
      FirstPage := 0;
      LastPage := 0;
      Layers := [];
      Copies := 0;
    end;
  end;

  ARWAData.Position := 0;
  PreviewInfo.Data2 := ARWAData.RealStream;
  PreviewInfo.PreviewPswd := ACurrentPassword;
  PreviewInfo.PrintPswd := ACurrentPassword;

  SecInfo.PreviewPassword := ANewInfo.PreviewPassword;
  SecInfo.PrintPassword := ANewInfo.PrintPassword;
  SecInfo.Compressed := ANewInfo.Compression;

  RWEngineExt.SetReportResultSecurity(@PreviewInfo, @SecInfo);

  ARWAData.Position := 0;
  Result := ARWAData;
end;

function TrwEngineApp.GetClassInfo(const AClassName: String): TrwClassInfo;
var
  Cl: IrwClass;
begin
  Cl := rwRTTIInfo.FindClass(AClassName);
  if Assigned(Cl) then
  begin
    Result.ClassName := Cl.GetClassName;
    Result.AncestorClassName := Cl.GetAncestor.GetClassName;
    Result.DisplayName := Cl.GetClassDisplayName;
  end
  else
  begin
    with Result do
    begin
      ClassName := '';
      AncestorClassName := '';
      DisplayName := '';
    end;
  end;
end;

function TrwEngineApp.GetMainWindowHandle: Integer;
begin
  Result := Application.MainForm.Handle;
end;

function TrwEngineApp.GetProcessController: IrwProcessController;
begin
  Result := FProcessController.FProxy;
end;

procedure TrwEngineApp.InitializeAppAdapter(const AModuleName: String; const AAppCustomData: Variant);
begin
  // Allow initialization only one time!
  if AppAdapterModuleName = '' then
  begin
    AppAdapterModuleName := AModuleName;
    AppAdapterAppCustomData := AAppCustomData;
    LoadAdapter;
  end;
end;

function TrwEngineApp.IsQueryBuilderFormClosed(out AQueryData: IevDualStream): Boolean;
var
  QData: IEvDualStream;
begin
  Result := True;
  AQueryData := nil;

  if Assigned(FQueryBuilderFormHolder) then
    if FQueryBuilderFormHolder.IsClosed then
    begin
      QData := FQueryBuilderFormHolder.GetData;
      if Assigned(QData) then
        AQueryData := FQueryBuilderFormHolder.GetData;
      FQueryBuilderFormHolder := nil;
    end
    else
      Result := False;
end;

procedure TrwEngineApp.LoadAdapter;
var
  GetRWAppAdapterProc: TGetRWAppAdapterProc;
  RWInitializeProc: TrwInitializeProc;
begin
  if AppAdapterModuleName = '' then
    raise ErwException.Create('AppAdapter is not defined');

  try
    FAdapterModule := LoadLibrary(PChar(AppAdapterModuleName));
  except
    on E: Exception do
    begin
      E.Message := 'Cannot load Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
      raise;
    end;
  end;

  if FAdapterModule <> 0 then
  begin
    RWInitializeProc := GetProcAddress(FAdapterModule, cInitialize);
    if not Assigned(RWInitializeProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cInitialize + '"');
    try
      RWInitializeProc;
    except
      on E: Exception do
      begin
        E.Message := 'Cannot initialize Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
        raise;
      end;
    end;

    AppAdapterInfo := GetAppAdapterInfo(AppAdapterModuleName);
    rwEngineMain.UpdateTrayHint;

    GetRWAppAdapterProc := GetProcAddress(FAdapterModule, cReportWriterAppAdapter);
    if not Assigned(GetRWAppAdapterProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cReportWriterAppAdapter + '"');

    FAdapter := GetRWAppAdapterProc;
    InitializeRWEngine(FAdapter);
  end

  else
    raise ErwException.Create('Cannot load Application RW Adapter "' + AppAdapterModuleName + '"');
end;

function TrwEngineApp.OpenQueryBuilder(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
begin
  if Assigned(AQueryData) then
    Result := RWEngineExt.ShowQueryBuilder(AQueryData.RealStream, ADescriptionView)
  else
    Result := RWEngineExt.ShowQueryBuilder(nil, ADescriptionView);
end;

function TrwEngineApp.OpenQueryBuilder2(const AQueryData: IevDualStream; const ADescriptionView: Boolean): Boolean;
begin
  FQueryBuilderFormHolder := RWEngineExt.ShowQueryBuilder(AQueryData, ADescriptionView);
  Result := Assigned(FQueryBuilderFormHolder);
end;

function TrwEngineApp.QueryInfo(const AItemName: String): String;

  procedure AddValue(const AItem, AValue: String);
  begin
    if Result <> '' then
      Result := Result + #13;
    Result := Result + AItem + '=' + AValue;
  end;

begin
  Result := '';

  if AnsiSameText(AItemName, 'Version') then
    AddValue('Version', GetFileVersion(GetModuleName(0)));

  if AnsiSameText(AItemName, 'AdapterVersion') and (FAdapterModule <> 0) then
    AddValue('AdapterVersion', GetModuleName(FAdapterModule));
end;

procedure TrwEngineApp.OpenReportFromClass(const AClassName: String);
begin
  CloseActiveReport;
  FActiveReport := TrwReportStructure.CreateFromClass(Self, AClassName);
end;

procedure TrwEngineApp.OpenReportFromStream(const AStream: IevDualStream);
begin
  CloseActiveReport;
  FActiveReport := TrwReportStructure.CreateFromStream(Self, AStream);
end;

function TrwEngineApp.RunQuery(const AQueryData: IevDualStream; const AParams: Variant): Variant;
var
  QueryParams: TrwParamList;
  Res: TISBasicClientDataSet;
  n, i, j: Integer;
begin
  Res := TISBasicClientDataSet.Create(nil);
  try
    if VarIsArray(AParams) then
    begin
      n := (VarArrayHighBound(AParams, 1) - VarArrayLowBound(AParams, 1) + 1) div 2;
      Assert((VarArrayHighBound(AParams, 1) - VarArrayLowBound(AParams, 1) + 1) mod 2 = 0);
      SetLength(QueryParams, n);

      for i := Low(QueryParams) to High(QueryParams) do
      begin
        j := i * 2;
        QueryParams[i].Name := AParams[j];
        QueryParams[i].ParamType := ddtUnknown;
        QueryParams[i].Value := AParams[j + 1];
      end;
    end

    else
      SetLength(QueryParams, 0);

    RWEngineExt.RunQBQuery(AQueryData.RealStream, QueryParams, Res);
    Result := Res.Data;
  finally
    FreeAndNil(Res);
  end;
end;

function TrwEngineApp.RunActiveReport(const ARunSettings: TrwRunReportSettings): TrwReportResult;
var
  ReportInfoRec: TrwReportRec;
  RunInfoRec: TrwRunReportRec;
  RepRes: IrwResult;
  ReportParams: TrwReportParamList;
  i: Integer;
  P: PTrwResultPageInfoList;
  S: IisStream;
begin
  with Result do
  begin
    Data := nil;
    FormatType := rwRRTUnknown;
    PageInfo := Null;
    MiscInfo := Null;
  end;

  with ReportInfoRec do
  begin
    Data := nil;
    ReportClassName := '';
    ReportAncestorClassName := '';
    FormatType := rwRFTbinary;
    Password := '';
    ReportName := '';
  end;

  with RunInfoRec do
  begin
    Params := nil;
    ReturnParamsBack := False;
    DoNotPrepare := False;
    ShowInputForm := False;
    Duplexing := False;
    ReturnValue := False;
  end;

  FProcessController.ReportData := FActiveReport.Data;
  try
    RunInfoRec.ReturnParamsBack := ARunSettings.ReturnParamsBack;
    RunInfoRec.Duplexing := ARunSettings.Duplexing;
    RunInfoRec.ShowInputForm := ARunSettings.ShowInputForm;
    RunInfoRec.DoNotPrepare := True;  // prepare should be called separatly

    SetLength(ReportParams, FActiveReport.ReportParameters.Count);
    for i := 0 to FActiveReport.ReportParameters.Count - 1 do
      with ReportParams[i], FActiveReport.ReportParameters do
      begin
        Name := Items[i].Name;
        ParamType := TrwRPType(Ord(Items[i].ParamType));
        RunTimeParam := Items[i].RunTimeParam;
        Value := Items[i].Value;
      end;
    RunInfoRec.Params := @ReportParams;

    ReportInfoRec.Data := FProcessController.ReportData.RealStream;
    ReportInfoRec.FormatType := rwRFTbinary;
    ReportInfoRec.ReportName := ARunSettings.ReportName;

    FProcessController.ExceptionText := '';
    if ARunSettings.Suspended then
    begin
      FProcessController.SetDebuggingState(True);
      FProcessController.State := rwVMSTPaused;
      FProcessController.WaitInPauseLoop;
    end
    else
    begin
      FProcessController.SetDebuggingState(False);
      FProcessController.State := rwVMSTRunning;
    end;

    try
      RepRes := RWEngineExt.RunReport(@ReportInfoRec, @RunInfoRec);
    finally
      FProcessController.SetDebuggingState(False);
      FProcessController.State := rwVMSTStopped;
      FProcessController.CleanupDebugEvents;
    end;

    if ARunSettings.ReturnParamsBack then
      for i := Low(ReportParams) to High(ReportParams) do
        FActiveReport.ReportParameters.ParamValue[ReportParams[i].Name] := ReportParams[i].Value;

  finally
    FProcessController.ReportData := nil;
  end;

  if Assigned(RepRes) then
  begin
    Result.Data := TevDualStreamHolder.Create;
    S := TisStream.CreateFromAuxStream(RepRes.Data, False);
    Result.Data.CopyFrom(S, 0);

    Result.FormatType := TrwReportResultFormatType(Ord(RepRes.ResultType));

    P := RepRes.PageInfo;
    Result.PageInfo := VarArrayCreate([Low(P^), High(P^)], varVariant);
    for i := Low(P^) to High(P^) do
      Result.PageInfo[i] := IntToStr(P^[i].PageNumber) + ',' +
                   IntToStr(P^[i].WidthMM) + ',' +
                   IntToStr(P^[i].HeightMM) + ',' +
                   IntToStr(Ord(P^[i].Duplexing));

    Result.MiscInfo := VarArrayCreate([0, 1], varVariant);
    Result.MiscInfo[0] := RunInfoRec.ReturnValue;
    Result.MiscInfo[1] := RepRes.MiscInfo.DefaultFileName;
  end;
end;

function TrwEngineApp.RunReport(const AReportData: IevDualStream; const AReportName: String; out AReportResult: IevDualStream): Variant;
var
  ReportInfoRec: TrwReportRec;
  RunInfoRec: TrwRunReportRec;
  RepRes: IrwResult;
begin
  with ReportInfoRec do
  begin
    Data := nil;
    ReportClassName := '';
    ReportAncestorClassName := '';
    FormatType := rwRFTbinary;
    Password := '';
    ReportName := '';
  end;

  with RunInfoRec do
  begin
    Params := nil;
    ReturnParamsBack := False;
    DoNotPrepare := False;
    ShowInputForm := False;
    Duplexing := False;
    ReturnValue := False;
  end;

  FProcessController.ReportData := AReportData;
  try
    RunInfoRec.ShowInputForm := True;
    ReportInfoRec.Data := FProcessController.ReportData.RealStream;
    ReportInfoRec.FormatType := rwRFTbinary;
    ReportInfoRec.ReportName := AReportName;

    FProcessController.State := rwVMSTRunning;
    try
      RepRes := RWEngineExt.RunReport(@ReportInfoRec, @RunInfoRec);
    finally
      FProcessController.State := rwVMSTStopped;
    end;

    Result := RunInfoRec.ReturnValue;
  finally
     FProcessController.ReportData := nil;
  end;

  if Assigned(RepRes) then
  begin
    AReportResult := TevDualStreamHolder.Create;
    AReportResult.RealStream.CopyFrom(RepRes.Data, 0);
    AReportResult.Position := 0;
  end
  else
    AReportResult := nil;
end;

function TrwEngineApp.RWAtoPDF(const ARWAData: IevDualStream; const APDFInfo: TrwPDFFileInfo): IevDualStream;
var
  RWAInfoRec: TrwRWAInfoRec;
  PDFInfoRec: TrwPDFInfoRec;
begin
  with RWAInfoRec do
  begin
    Name := '';
    Data1 := nil;
    Data2 := nil;
    Data3 := nil;
    Data4 := '';
    SecuredMode := False;
    PreviewPswd := '';
    PrintPswd := '';
    with PrintJobInfo do
    begin
      PrintJobName := '';
      PrinterName := '';
      TrayName := '';
      OutBinNbr := 0;
      Duplexing := rdpDefault;
      PrintPCLAsGraphic := False;
      PrinterVerticalOffset := 0;
      PrinterHorizontalOffset := 0;
      FirstPage := 0;
      LastPage := 0;
      Layers := [];
      Copies := 0;
    end;
  end;

  RWAInfoRec.Data3 := ARWAData;

  PDFInfoRec.Author := APDFInfo.Author;
  PDFInfoRec.Creator := APDFInfo.Creator;
  PDFInfoRec.Subject := APDFInfo.Subject;
  PDFInfoRec.Title := APDFInfo.Title;
  PDFInfoRec.Compression := APDFInfo.Compression;
  PDFInfoRec.OwnerPassword := APDFInfo.OwnerPassword;
  PDFInfoRec.UserPassword := APDFInfo.UserPassword;
  PDFInfoRec.ProtectionOptions := [];

  if (APDFInfo.ProtectionOptions and $1) <> 0 then
    PDFInfoRec.ProtectionOptions := PDFInfoRec.ProtectionOptions + [pdfPrint];

  if (APDFInfo.ProtectionOptions and $2) <> 0 then
    PDFInfoRec.ProtectionOptions := PDFInfoRec.ProtectionOptions + [pdfModifyStructure];

  if (APDFInfo.ProtectionOptions and $4) <> 0 then
    PDFInfoRec.ProtectionOptions := PDFInfoRec.ProtectionOptions + [pdfCopyInformation];

  if (APDFInfo.ProtectionOptions and $8) <> 0 then
    PDFInfoRec.ProtectionOptions := PDFInfoRec.ProtectionOptions + [pdfModifyAnnotation];

  Result := RWEngineExt.RWAtoPDF(@RWAInfoRec, @PDFInfoRec);
end;

procedure TrwEngineApp.SetExternalModuleData(const AModuleName: String; const AData: IevDualStream);
begin
  if AnsiSameText(AModuleName, 'SystemLibFunctions') then
    SystemLibFunctions(AData)

  else if AnsiSameText(AModuleName, 'SystemLibComponents') then
    SystemLibComponents(AData)

  else if AnsiSameText(AModuleName, 'SystemDataDictionary') then
    DataDictionary(AData);
end;

procedure TrwEngineApp.UnloadAdapter;
var
  RWFinalizeProc: TrwFinalizeProc;
begin
  if Assigned(FAdapter) then
  begin
    FAdapter := nil;
    DestroyRWEngine;
  end;

  if FAdapterModule <> 0 then
  begin
    RWFinalizeProc := GetProcAddress(FAdapterModule, cFinalize);
    if not Assigned(RWFinalizeProc) then
      raise ErwException.Create('Module "' + AppAdapterModuleName + '" does not contain an exported function "' + cFinalize + '"');
    try
      RWFinalizeProc;
    except
      on E: Exception do
      begin
        E.Message := 'Cannot finalize Application RW Adapter "' + AppAdapterModuleName + '"'#13 + E.Message ;
        raise;
      end;
    end;

    AppAdapterModuleName := '';
    AppAdapterAppCustomData := varEmpty;

    FreeLibrary(FAdapterModule);
    FAdapterModule := 0;
  end;

  with AppAdapterInfo do
  begin
    ModuleName := '';
    ApplicationName := '';
    Version := '';
  end;

  if not not Application.Terminated then
    rwEngineMain.UpdateTrayHint;
end;

procedure TrwEngineApp.CloseActiveReport;
begin
  FActiveReport := nil;
end;

function TrwEngineApp.GetActiveReport: IrwReportStructure;
begin
  Result := FActiveReport;
end;

function TrwEngineApp.ClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
var
  Cl: IrwClass;
begin
  Cl := rwRTTIInfo.FindClass(AClassName);
  if Assigned(Cl) then
    Result := Cl.DescendantOf(AAncestorClassName)
  else
    Result := False;  
end;


procedure TrwEngineApp.Terminate;
begin
  if Assigned(FAdapter) then
    FAdapter.Terminate;
end;

{ TrwReportStructure }

procedure TrwReportStructure.Compile;
var
  R: TrwReportRec;
begin
  with R do
  begin
    Data := nil;
    ReportClassName := '';
    ReportAncestorClassName := '';
    FormatType := rwRFTbinary;
    Password := '';
  end;

  R.Data := FData.RealStream;
  R.FormatType := rwRFTbinary;

  RWEngineExt.CompileReport(@R);
end;

constructor TrwReportStructure.CreateEmpty(const AOwner: IrwEngineApp);
begin
  Create;
  FData := TEvDualStreamHolder.Create;
  FReportParams := TrwReportParameters.Create;
  FReportInputForm := TrwReportInputForm.Create(Self);
end;

constructor TrwReportStructure.CreateFromClass(const AOwner: IrwEngineApp; const AClassName: String);
var
  Cl: IrwClass;
  FReport: TrwComponent;
begin
  CreateEmpty(AOwner);

  Cl := rwRTTIInfo.FindClass(AClassName);
  if not Assigned(Cl) then
    raise ErwException.CreateFmt(ResErrWrongLibComp, [AClassName]);

  FReport := Cl.CreateObjectInstance as TrwComponent;
  try
    FReport.Name := 'Report';
    FReport.Compile;

    if FReport is TrwReport then
      TrwReport(FReport).SaveToStream(FData.RealStream)
    else
      (FReport as TrmReport).SaveToStream(FData.RealStream);

    RWEngineExt.GetReportParameters(FData.RealStream,
      @(TrwReportParameters((FReportParams as IisInterfacedObject).GetImplementation).FParams));
  finally
    FreeAndNil(FReport);
  end;  
end;

constructor TrwReportStructure.CreateFromStream(const AOwner: IrwEngineApp; const ASource: IevDualStream);
begin
  CreateEmpty(AOwner);
  FData := ASource;
  RWEngineExt.GetReportParameters(FData.RealStream, @(TrwReportParameters((FReportParams as IisInterfacedObject).GetImplementation).FParams));
end;

function TrwReportStructure.GetData: IevDualStream;
begin
  Result := FData;
end;

function TrwReportStructure.GetReportInputForm: IrwReportInputForm;
begin
   Result := FReportInputForm;
end;

function TrwReportStructure.GetReportParameters: IrwReportParameters;
begin
  Result := FReportParams;
end;


function TrwReportStructure.GetStructureInfo: TrwReportStructureInfo;
var
  R: TrwReportRec;
  RInf: TrwReportContentRec;
begin
  with R do
  begin
    Data := nil;
    ReportClassName := '';
    ReportAncestorClassName := '';
    FormatType := rwRFTbinary;
    Password := '';
  end;

  R.Data := FData.RealStream;
  R.FormatType := rwRFTbinary;

  RInf := RWEngineExt.AnalyzeReport(@R);

  Result.ReportClassName := RInf.ClassName;
  Result.AncestorClassName := RInf.AncestorClassName;
  Result.UsedLibComponents := RInf.UsedLibComponents;
  Result.Description := RInf.Description;
end;

procedure TrwReportStructure.PrepareForRunning;
var
  R: TrwReportRec;
begin
  with R do
  begin
    Data := nil;
    ReportClassName := '';
    ReportAncestorClassName := '';
    FormatType := rwRFTbinary;
    Password := '';
  end;

  R.Data := FData.RealStream;
  R.FormatType := rwRFTbinary;

  RWEngineExt.PrepareReport(@R);
end;


{ TrwReportParameters }

function TrwReportParameters.GetCount: Integer;
begin
  Result := Length(FParams);
end;

function TrwReportParameters.GetItems(const AIndex: Integer): TrwReportParam;
begin
  if (AIndex >= 0) and (AIndex < Count) then
  begin
    Result.Name := FParams[AIndex].Name;
    Result.ParamType := TrwReportParamType(FParams[AIndex].ParamType);
    Result.Value := FParams[AIndex].Value;
    Result.RunTimeParam := FParams[AIndex].RunTimeParam;
  end
  else
    with Result do
    begin
      Name := '';
      Value := Null;
      RunTimeParam := False;
      ParamType := rwConnectionInt.rwvUnknown;
    end;
end;

function TrwReportParameters.GetParamValue(const AParamName: String): Variant;
begin
  Result := Items[IndexOf(AParamName)].Value;
end;

function  TrwReportParameters.IndexOf(const AParamName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := Low(FParams) to High(FParams) do
    if AnsiSameText(FParams[i].Name, AParamName) then
    begin
      Result := i;
      break;
    end;
end;

function TrwReportParameters.ParamByName(const AParamName: String): TrwReportParam;
begin
  Result := Items[IndexOf(AParamName)];
end;

procedure TrwReportParameters.ParamsFromStream(const AParams: IevDualStream);
var
  Prms: TrwReportParamList;
  i: Integer;
begin
  AParams.Position := 0;
  Prms := RWEngineExt.AppAdapter.ReadReportParamsFromStream(AParams.RealStream);

  for i := Low(Prms) to High(Prms) do
    SetParamValue(Prms[i].Name, Prms[i].Value);
end;

function TrwReportParameters.ParamsToStream: IevDualStream;
begin
  Result := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.WriteReportParamsToStream(@FParams, Result.RealStream);
end;

procedure TrwReportParameters.SetParamValue(const AParamName: String; const AValue: Variant);
var
  i: Integer;
begin
  i := IndexOf(AParamName);
  if i <> -1 then
    FParams[i].Value := AValue;
end;

{ TrwReportInputForm }

procedure TrwReportInputForm.CloseCancel;
begin
  if Assigned(FrwInputForm) then
  begin
    FrwInputForm.Close(nil);
    FrwInputForm := nil;
  end;
end;

procedure TrwReportInputForm.CloseOK;
begin
  if Assigned(FrwInputForm) then
  begin
    FrwInputForm.Close(@(TrwReportParameters((FReportStructure.FReportParams as IisInterfacedObject).GetImplementation).FParams));
    FrwInputForm := nil;
  end;
end;

constructor TrwReportInputForm.Create(const ReportStructure: TrwReportStructure);
begin
  inherited Create;
  FReportStructure := ReportStructure;
end;

function TrwReportInputForm.GetFormBounds: TrwBounds;
begin
  if Assigned(FrwInputForm) then
  begin
    Result.Left := TControl(FrwInputForm.VCLObject).Left;
    Result.Top := TControl(FrwInputForm.VCLObject).Top;
    Result.Width := TControl(FrwInputForm.VCLObject).Width;
    Result.Height := TControl(FrwInputForm.VCLObject).Height;
  end
  else
    with Result do
    begin
      Left := 0;
      Top := 0;
      Width := 0;
      Height := 0;
    end;
end;

procedure TrwReportInputForm.SetFormBounds(const ABounds: TrwBounds);
begin
  if Assigned(FrwInputForm) then
  begin
    TControl(FrwInputForm.VCLObject).Left := ABounds.Left;
    TControl(FrwInputForm.VCLObject).Top := ABounds.Top;
    TControl(FrwInputForm.VCLObject).Width := ABounds.Width;
    TControl(FrwInputForm.VCLObject).Height := ABounds.Height;
  end;
end;

function TrwReportInputForm.Show(const AParentWndHandle: Integer; const ASetupMode: Boolean): Boolean;
var
  ReportInfoRec: TrwReportRec;
  ShowInputFormRec: TrwShowInputFormRec;
begin
  if not Assigned(FrwInputForm) then
  begin
    if AParentWndHandle <> 0 then
      HideAppFromTaskBar
    else
      ShowAppOnTaskBar;

    with ReportInfoRec do
    begin
      Data := nil;
      ReportClassName := '';
      ReportAncestorClassName := '';
      FormatType := rwRFTbinary;
      Password := '';
    end;

    ReportInfoRec.Data := FReportStructure.FData.RealStream;
    ReportInfoRec.FormatType := rwRFTbinary;

    with ShowInputFormRec do
    begin
      Params := nil;
      Mode := rwIFRunTime;
      Parent := 0;
    end;

    ShowInputFormRec.Params := @(TrwReportParameters((FReportStructure.FReportParams as IisInterfacedObject).GetImplementation).FParams);
    ShowInputFormRec.Parent := AParentWndHandle;

    if ASetupMode then
      ShowInputFormRec.Mode := rwIFSetUpTime;

    FrwInputForm := rwEngineExt.ShowInputForm(@ReportInfoRec, @ShowInputFormRec);

    Result := Assigned(FrwInputForm);
  end
  else
    Result := False;
end;

{ TrwProcessController }

procedure TrwProcessController.BeforeExecuteVMInstruction;
var
  TimeToStop: Boolean;

  function DoTrace(ATraceIn: Boolean): Boolean;
  var
    CurrentStopAddress: TrwVMAddress;
    CurrentStackDepth: Integer;
    CallStack: TrwVMAddressList;
    TmpSP: PTrwDebugLineInfo;
  begin
    RWEngineExt.GetVM.GetCallStack(CallStack);
    CurrentStopAddress := RWEngineExt.GetVM.CurrentEntryPoint;
    TmpSP := FindDebugLine(CurrentStopAddress);

    if Assigned(FLastStopPoint.SorceCodeLineInfo) then
    begin
      CurrentStackDepth := Length(CallStack);

      Result := not ((CurrentStopAddress.Segment = FLastStopPoint.Address.Segment) and
                     (FLastStopPoint.SorceCodeLineInfo.FirstAddress <= CurrentStopAddress.Offset) and
                     (FLastStopPoint.SorceCodeLineInfo.LastAddress >= CurrentStopAddress.Offset)) and
                     Assigned(TmpSP) and (ATraceIn or (CurrentStackDepth <= FLastStackDepth));

      if Result then
      begin
        FLastStackDepth := CurrentStackDepth;
        FLastStopPoint.SorceCodeLineInfo := TmpSP;
        FLastStopPoint.Address := CurrentStopAddress;
      end;
    end

    else
    begin
      Result := Assigned(TmpSP);
      if Result then
      begin
        FLastStopPoint.SorceCodeLineInfo := TmpSP;
        FLastStopPoint.Address := CurrentStopAddress;
      end;
    end;
  end;

  function DoCheckBreakPoint: Boolean;
  var
    CS: TrwVMAddress;
    CallStack: TrwVMAddressList;
    i: Integer;
  begin
    CS := RWEngineExt.GetVM.CurrentEntryPoint;

    Result := False;
    for i := Low(FBreakPoints) to High(FBreakPoints) do
      with FBreakPoints[i] do
      if (Segment = CS.Segment) and (Offset = CS.Offset) then
      begin
        Result := True;
        break;
      end;

    if Result then
    begin
      RWEngineExt.GetVM.GetCallStack(CallStack);
      FLastStackDepth := Length(CallStack);
    end;
  end;

begin
  case FRunAction of
    rwRATraceOver: TimeToStop := DoTrace(False);
    rwRATraceIn:   TimeToStop := DoTrace(True);
  else
    TimeToStop := DoCheckBreakPoint;
  end;

  if TimeToStop then
    Pause
  else if State = rwVMSTPaused then
    Run(rwRATraceOver);
end;

function TrwProcessController.CalcExpression(const Expression, DebugSymbolInfo: String): TrwVMExpressionResult;
var
  ExprPCode: TrwCode;
  EntryPoint: TrwVMAddress;

  procedure CompileExpression;
  begin
    SetLength(ExprPCode, 0);
    SetCompiledCode(Addr(ExprPCode), cVMDsgnWatchListOffset);
    SetCompilingPointer(0);
    SetDebInf(nil);
    SetDebSymbInf(nil);

    try
      CompileWatchList(Expression + '; end'#0, DebugSymbolInfo, nil, VM);

      with GetCodeInsightInfo do
      begin
        Result.ResultType := TrwReportParamType(VarType);
        if VarType = rwvPointer then
          if Assigned(ClassInf) then
              Result.ResultTypeText := ClassInf.GetClassName
            else
              Result.ResultTypeText := 'Pointer'

          else if VarType = rwvUnknown then
            raise ErwException.Create('Unknown')

        else
          Result.ResultTypeText := NameOfVarType(VarType);
      end;

    except
      on E: Exception do
      begin
        if E is ErwParserError then
          Result.ResultText := ErwParserError(E).GeneralMessage
        else
          Result.ResultText := E.Message;
      end;
    end;

    EntryPoint.Segment := rcsWatchList;
    EntryPoint.Offset := 0;
    SetLength(ExprPCode, GetCompilingPointer);
  end;

  procedure CalcExpression;
  begin
    with VM do
    begin
      Push(RegA);
      Push(RegB);
      Push(Flag);
      try
        try
          Run(EntryPoint, @ExprPCode);
          Result.Result := RegA;
        except
          on E: Exception do
            Result.ResultText := E.Message;
        end;
      finally
        Flag := Pop;
        RegB := Pop;
        RegA := Pop;
      end;
    end;
  end;

  procedure PrepareResult;
  begin
    if Result.ResultText <> '' then
      Exit;

    try
      if VarIsNull(Result.Result) then
         Result.ResultText := 'Null'

      else
      begin
        if TrwRPType(Ord(Result.ResultType)) = rwvVariant then
           Result.ResultType := TrwReportParamType(VariantToInternalType(Result.Result));

        case TrwRPType(Ord(Result.ResultType)) of
          rwvBoolean:  if Result.Result then
                         Result.ResultText := 'True'
                       else
                         Result.ResultText := 'False';

          rwvString:   Result.ResultText := '''' + VarAsType(Result.Result, varString) + '''';

          rwvArray:    Result.ResultText := 'Array';

          rwvCurrency: Result.ResultText := CurrencyString + FormatCurr('#,##0.00', Result.Result);

          rwvPointer:  Result.ResultText := Result.ResultTypeText + '($' + IntToHex(Result.Result, 8) + ')';
        else
          Result.ResultText := VarAsType(Result.Result, varString);
        end;
      end;

    except
      Result.ResultText := 'Type conversion error';
    end;
  end;

begin
  Result.Result := varUnknown;
  Result.ResultType := rwConnectionInt.rwvUnknown;
  Result.ResultText := 'Unknown';
  Result.ResultTypeText := 'Unknown';

  try
   CompileExpression;
   if Length(ExprPCode) > 0 then
   begin
     Result.ResultText := '';
     CalcExpression;
     PrepareResult;
  end;

  except
    on E: Exception do
      Result.ResultText := E.Message;
  end;
end;


constructor TrwProcessController.Create;
begin
  FCS := TCriticalSection.Create;
  FProxy := TrwProcessControllerProxy.Create(Self);
end;

destructor TrwProcessController.Destroy;
begin
  FProxy := nil;
  FreeAndNil(FCS);
  inherited;
end;

function TrwProcessController.FindDebugLine(const AAddress: TrwVMAddress): PTrwDebugLineInfo;
var
  PLines: PTrwDebugLineList;
  CurrPos, BegPos, EndPos: Integer;
begin
  Result := nil;

  if AAddress.Segment = rcsReport then
    PLines := Addr(FDebugSourceCode.Report)
  else if AAddress.Segment = rcsLibFunctions then
    PLines := Addr(FDebugSourceCode.LibFunctions)
  else if AAddress.Segment = rcsLibComponents then
    PLines := Addr(FDebugSourceCode.LibComponents)
  else
    exit;

  if Length(PLines^) = 0 then
    exit;

  // search
  BegPos := Low(PLines^);
  EndPos := High(PLines^);
  CurrPos := BegPos + (EndPos - BegPos) div 2;
  repeat
    if (PLines^[CurrPos].FirstAddress <= AAddress.Offset) and (PLines^[CurrPos].LastAddress >= AAddress.Offset) then
    begin
      Result := @PLines^[CurrPos];
      break;
    end

    else if PLines^[CurrPos].LastAddress < AAddress.Offset then
      BegPos := CurrPos + 1

    else if PLines^[CurrPos].FirstAddress > AAddress.Offset then
      EndPos := CurrPos - 1

    else
      break;

    CurrPos := BegPos + (EndPos - BegPos) div 2;

  until BegPos > EndPos;
end;

function TrwProcessController.GetState: TrwVMStateType;
begin
  FCS.Enter;
  try
    Result := FState;
  finally
    FCS.Leave;
  end;
end;

function TrwProcessController.GetVMCallStack: Variant;
var
  CallStack: TrwVMAddressList;
begin
  RWEngineExt.GetVM.GetCallStack(CallStack);
  Result := VMAddrListToVariant(CallStack);
end;

function TrwProcessController.GetVMRegisters: TrwVMRegisters;
begin
  Result.RegA := RWEngineExt.GetVM.RegA;
  Result.RegB := RWEngineExt.GetVM.RegB;
  Result.RegF := RWEngineExt.GetVM.Flag;
  Result.CSIP := VMAddressToVariant(RWEngineExt.GetVM.CurrentEntryPoint);
end;

procedure TrwProcessController.Pause;
begin
  State := rwVMSTPaused;
end;

procedure TrwProcessController.InitializeDebugInfo;
var
  Report: TrmReport;

  procedure PrepareCodeSegment(ACodeSegment: TrwVMCodeSegment);
  var
    i: Integer;
    h: String;
    PDebInfoSource: PTrwDebInf;
    PDebInfoDest: PTrwDebugLineList;
    PDebSymbSource: PTrwDebInf;
    PDebSymbDest: PTrwDebugSymbolsList;
  begin
    case ACodeSegment of
      rcsReport:
        begin
          PDebInfoSource := Addr(Report.DebInf);
          PDebInfoDest := Addr(FDebugSourceCode.Report);
          PDebSymbSource := Addr(Report.DebSymbInf);
          PDebSymbDest := Addr(FDebugSourceCode.ReportDebSymbols);
        end;

      rcsLibFunctions:
        begin
          PDebInfoSource := SystemLibFunctions.GetDebInf;
          PDebInfoDest := Addr(FDebugSourceCode.LibFunctions);
          PDebSymbSource := SystemLibFunctions.GetDebSymbInf;
          PDebSymbDest := Addr(FDebugSourceCode.LibFunctionsDebSymbols);
        end;

      rcsLibComponents:
        begin
          PDebInfoSource := SystemLibComponents.GetDebInf;
          PDebInfoDest := Addr(FDebugSourceCode.LibComponents);
          PDebSymbSource := SystemLibComponents.GetDebSymbInf;
          PDebSymbDest := Addr(FDebugSourceCode.LibComponentsDebSymbols);
        end;
    else
      PDebInfoSource := nil;
      PDebInfoDest := nil;
      PDebSymbSource := nil;
      PDebSymbDest := nil;
    end;


    if Assigned(PDebInfoSource) then
    begin
      SetLength(PDebInfoDest^, Length(PDebInfoSource^));

      for i := Low(PDebInfoSource^) to High(PDebInfoSource^) do
      begin
        h:= PDebInfoSource^[i];
        with PDebInfoDest^[i] do
        begin
          Segment := ACodeSegment;
          FirstAddress := StrToInt(GetNextStrValue(h, ';'));
          LastAddress := StrToInt(GetNextStrValue(h, ';'));
          ObjectLocation := GetNextStrValue(h, ';');
          FunctionName := GetNextStrValue(h, ';');
          SourceCodeLineNbr := StrToInt(GetNextStrValue(h, ';'));
          SourceCodeLineTxt := GetNextStrValue(h, ';');

          h := ObjectLocation;
          h := GetNextStrValue(h, '.');
          if h <> Report.PGetPropertyValue('Name') then
            ClassName := h
          else
            ClassName := '';
        end;
      end;
    end

    else
      SetLength(PDebInfoDest^, 0);


    if Assigned(PDebSymbSource) then
    begin
      SetLength(PDebSymbDest^, Length(PDebSymbSource^));

      for i := Low(PDebSymbSource^) to High(PDebSymbSource^) do
      begin
        h:= PDebSymbSource^[i];
        if h <> '' then
          with PDebSymbDest^[i] do
          begin
            Segment := ACodeSegment;
            GetNextStrValue(h, ';');
            GetNextStrValue(h, ';');
            FirstAddress := PDebInfoDest^[StrToInt(GetNextStrValue(h, ';'))].FirstAddress;
            LastAddress := PDebInfoDest^[StrToInt(GetNextStrValue(h, ';'))].LastAddress;
            DebSymbols := h;
          end;
      end;
    end

    else
      SetLength(PDebSymbDest^, 0);
  end;


  procedure PrepareDebInfo;
  begin
    if Length(SystemLibFunctions.GetDebInf^) = 0 then
      SystemLibFunctions.Compile;

    if Length(SystemLibComponents.GetDebInf^) = 0 then
      SystemLibComponents.Compile;

    if Assigned(ReportData) and (ReportData.Size > 0) then
    begin
      ReportData.Position := 0;
      Report := RWEngineExt.CreateReportObject(ReportData.RealStream) as TrmReport;
      try
        PrepareCodeSegment(rcsReport);
        PrepareCodeSegment(rcsLibFunctions);
        PrepareCodeSegment(rcsLibComponents);
      finally
        FreeAndNil(Report);
      end;
    end;
  end;

begin
  PrepareDebInfo;
end;

procedure TrwProcessController.Run(ARunAction: TrwRunAction);
begin
  FCS.Enter;
  try
    RunAction := ARunAction;
    State := rwVMSTRunning;
  finally
    FCS.Leave;
  end;
end;

procedure TrwProcessController.SetBreakpoints(const Breakpoints: Variant);
begin
  FCS.Enter;
  try
    FBreakPoints := VariantToVMAddrList(Breakpoints);
  finally
    FCS.Leave;
  end;
end;

procedure TrwProcessController.SetDebuggingState(Debugging: WordBool);
begin
  if Debugging then
  begin
    RWEngineExt.GetVM.SetRMDebuggerCallback(nil, nil);
    InitializeDebugInfo;
    RWEngineExt.GetVM.SetRMDebuggerCallback(VMDebugInterruption, VMDebugOnError)
  end
  else
    RWEngineExt.GetVM.SetRMDebuggerCallback(nil, nil);

  RWDesigning := Debugging;
end;

procedure TrwProcessController.SetState(const AValue: TrwVMStateType);
begin
  FCS.Enter;
  try
    FState := AValue;
  finally
    FCS.Leave;
  end;
end;

procedure TrwProcessController.Stop;
begin
  State := rwVMSTStopped;
end;

procedure TrwProcessController.VMDebugInterruption;
begin
  // here is MAIN THREAD context ALWAYS!
  
  FCS.Enter;
  try
    if State = rwVMSTStopped then
    begin
      RWEngineExt.GetVM.Halt := True;
      exit;
    end;

    BeforeExecuteVMInstruction;
  finally
    FCS.Leave;
  end;

  WaitInPauseLoop;
end;

procedure TrwProcessController.VMDebugOnError(const AErrorMessage: String);
begin
  // here is MAIN THREAD context ALWAYS!
  ExceptionText := AErrorMessage;

  if State = rwVMSTStopped then
    Exit;

  Pause;
  WaitInPauseLoop;
end;

procedure TrwProcessController.WaitForDebuggerEvent;
var
  Msg: TMsg;

  procedure PerformDebuggerRequest;
  begin
    case PTrwDebuggerProxyRequest(Msg.wParam)^.Request of
      rwDRGetRegisters:        PTrwDebuggerProxyRequest(Msg.wParam)^.outRegisters^ := GetVMRegisters;
      rwDRGetStack:            PTrwDebuggerProxyRequest(Msg.wParam)^.outStack^ := GetVMCallStack;
      rwDRCalcExpression:      PTrwDebuggerProxyRequest(Msg.wParam)^.outExpression^ := CalcExpression(PTrwDebuggerProxyRequest(Msg.wParam)^.inExpression^, PTrwDebuggerProxyRequest(Msg.wParam)^.inDebugSymbolInfo^);
      rwDRShowQueryBuilder:    DoShowQB(PTrwDebuggerProxyRequest(Msg.wParam)^.QBData, PTrwDebuggerProxyRequest(Msg.wParam)^.inDescriptionView^);
    end;

    PostThreadMessage(PTrwDebuggerProxyRequest(Msg.wParam)^.ThreadID, WM_RMDEBUG, 0, 0);
  end;


begin
  if GetMessage(Msg, 0, WM_RMDEBUG, WM_RMDEBUG_REQUEST) then
    if Msg.message = WM_RMDEBUG_REQUEST then
      PerformDebuggerRequest
    else
      if Assigned(CurrentInputForm) then
        CurrentInputForm.BringToFront;
end;

procedure TrwProcessController.WaitInPauseLoop;
begin
   while State = rwVMSTPaused do
     WaitForDebuggerEvent;
end;

procedure TrwProcessController.CleanupDebugEvents;
var
  Msg: TMsg;
begin
  while PeekMessage(Msg, 0, WM_RMDEBUG, WM_RMDEBUG_REQUEST, PM_REMOVE) do
  ;
end;

function TrwProcessController.GetRunAction: TrwRunAction;
begin
  FCS.Enter;
  try
    Result := FRunAction;
  finally
    FCS.Leave;
  end;
end;

procedure TrwProcessController.SetRunAction(const AValue: TrwRunAction);
begin
  FCS.Enter;
  try
    FRunAction := AValue;
  finally
    FCS.Leave;
  end;
end;

function TrwProcessController.GetExceptionText: String;
begin
  FCS.Enter;
  try
    Result := FExceptionText;
  finally
    FCS.Leave;
  end;
end;

procedure TrwProcessController.SetExceptionText(const AValue: String);
begin
  FCS.Enter;
  try
    FExceptionText := AValue;
  finally
    FCS.Leave;
  end;
end;

function TrwProcessController.DoShowQB(AData: TStream; ADescriptionView: Boolean): Boolean;
begin
  Result := RWEngineExt.ShowQueryBuilder(AData, ADescriptionView);
end;


{ TrwProcessControllerProxy }

function TrwProcessControllerProxy.CalcExpression(const AExpression: String;
  const ADebugSymbolInfo: String): TrwVMExpressionResult;
begin
  if FOwner.State = rwVMSTPaused then
  begin
    FRequestInfo.Request := rwDRCalcExpression;
    FRequestInfo.inExpression := @AExpression;
    FRequestInfo.inDebugSymbolInfo := @ADebugSymbolInfo;
    FRequestInfo.outExpression := @Result;
    RunRequest;
  end

  else
  begin
    Result.Result := varUnknown;
    Result.ResultType := rwConnectionInt.rwvUnknown;
    Result.ResultText := 'Unknown';
    Result.ResultTypeText := 'Unknown';
  end;
end;

constructor TrwProcessControllerProxy.Create(const AOwner: TrwProcessController);
begin
  FOwner := AOwner;
  inherited Create;
end;

function TrwProcessControllerProxy.GetVMCallStack: Variant;
begin
  if FOwner.State = rwVMSTPaused then
  begin
    FRequestInfo.Request := rwDRGetStack;
    FRequestInfo.outStack := @Result;
    RunRequest;
  end
  else
    Result := VarArrayOf([]);
end;

function TrwProcessControllerProxy.GetVMRegisters: TrwVMRegisters;
begin
  with Result do
  begin
    RegA := Null;
    RegB := Null;
    RegF := Null;
    CSIP := Null;
  end;

  if FOwner.State = rwVMSTPaused then
  begin
    FRequestInfo.Request := rwDRGetRegisters;
    FRequestInfo.outRegisters := @Result;
    RunRequest;
  end;
end;

function TrwProcessControllerProxy.GetState: TrwProcessState;
begin
  Result.VMState := FOwner.State;
  Result.ExceptionText := FOwner.ExceptionText;
end;

procedure TrwProcessControllerProxy.Pause;
begin
  FOwner.Pause;
end;

procedure TrwProcessControllerProxy.RunRequest;
var
  Msg: TMsg;
begin
  FRequestInfo.ThreadID := GetCurrentThreadId;
  PostThreadMessage(MainThreadID, WM_RMDEBUG_REQUEST, Integer(@FRequestInfo), 0);
  GetMessage(Msg, 0, WM_RMDEBUG, WM_RMDEBUG);
end;

procedure TrwProcessControllerProxy.Run(const ARunAction: TrwRunAction);
begin
  FOwner.Run(ARunAction);
  NotifyEngine;
end;

procedure TrwProcessControllerProxy.SetBreakpoints(const ABreakpoints: Variant);
begin
  FOwner.SetBreakpoints(ABreakpoints);
end;

procedure TrwProcessControllerProxy.Stop;
begin
  FOwner.Stop;
  NotifyEngine;
end;


procedure TrwProcessControllerProxy.NotifyEngine;
begin
  PostThreadMessage(MainThreadID, WM_RMDEBUG, 0, 0);
end;


procedure TrwProcessControllerProxy.OpenQueryBuilder(const AQueryData: IevDualStream;
  const ADescriptionView: Boolean);
begin
  if FOwner.State = rwVMSTPaused then
  begin
    FRequestInfo.Request := rwDRShowQueryBuilder;
    FRequestInfo.inDescriptionView := @ADescriptionView;

    if Assigned(AQueryData) then
      FRequestInfo.QBData := AQueryData.RealStream
    else
      FRequestInfo.QBData := nil;

    RunRequest;
  end;
end;

initialization
  SetGlobalLogFile(TLogFile.Create(ChangeFileExt(AppFileName, '.log')));

finalization
  SetGlobalLogFile(nil);

end.
