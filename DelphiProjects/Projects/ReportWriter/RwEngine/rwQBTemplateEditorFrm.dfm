object rwQBTemplateEditor: TrwQBTemplateEditor
  Left = 494
  Top = 341
  BorderStyle = bsToolWindow
  Caption = 'Template Editor'
  ClientHeight = 217
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 2
    Top = 7
    Width = 28
    Height = 13
    Caption = 'Name'
  end
  object Label2: TLabel
    Left = 2
    Top = 34
    Width = 29
    Height = 13
    Caption = 'Group'
  end
  object Label3: TLabel
    Left = 2
    Top = 65
    Width = 53
    Height = 13
    Caption = 'Description'
  end
  object btnLoad: TButton
    Left = 80
    Top = 191
    Width = 65
    Height = 23
    Caption = 'Load...'
    TabOrder = 4
    OnClick = btnLoadClick
  end
  object edName: TEdit
    Left = 41
    Top = 4
    Width = 256
    Height = 21
    TabOrder = 0
    OnExit = edNameExit
  end
  object edGroup: TEdit
    Left = 41
    Top = 31
    Width = 256
    Height = 21
    TabOrder = 1
    OnExit = edGroupExit
  end
  object memDescr: TMemo
    Left = 2
    Top = 81
    Width = 295
    Height = 104
    TabOrder = 2
    OnExit = memDescrExit
  end
  object btnCancel: TButton
    Left = 232
    Top = 191
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object btnOK: TButton
    Left = 160
    Top = 191
    Width = 65
    Height = 23
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 5
  end
  object pnlStatus: TPanel
    Left = 2
    Top = 193
    Width = 53
    Height = 19
    BevelOuter = bvLowered
    Caption = 'Empty'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object odOpen: TOpenDialog
    DefaultExt = 'rwq'
    Filter = 'Data Dictionary resources (*.rwq)|*.rwq|All Files (*.*)|*.*'
    Title = 'Load Query from File'
    Left = 170
    Top = 103
  end
end
