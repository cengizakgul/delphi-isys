unit rwQBVTParamsFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, StdCtrls, Mask, Variants, rwDataDictionary, rwCustomDataDictionary,
  rwTypes, rwUtils, rwQBInfo, rwQBExprEditorFrm, rwDesignClasses, isVCLBugFix,
  wwdbedit, Wwdotdot, Wwdbigrd, Wwdbgrid, kbmMemTable, ISKbmMemDataSet,
  rwEngineTypes, rwBasicUtils, ISBasicClasses, rwDB, ISDataAccessComponents;

type
  TrwQBVTParams = class(TForm)
    Button1: TButton;
    Button2: TButton;
    grParams: TisRWDBGrid;
    dsParams: TisRWClientDataSet;
    evDataSource1: TDataSource;
    fldName: TStringField;
    fldValue: TStringField;
    dsParamsType: TStringField;
    cbExpression: TwwDBComboDlg;
    procedure FormCreate(Sender: TObject);
    procedure dsParamsBeforeDelete(DataSet: TDataSet);
    procedure dsParamsNewRecord(DataSet: TDataSet);
    procedure cbExpressionCustomDlg(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure grParamsRowChanged(Sender: TObject);

  private
    FTable: TrwQBSelectedTable;
    FParams: TrwQBExpressions;
    FDSLookup: TrwDataSet;
    procedure FillParams;
    function  DSLookup: TrwDataSet;
  public
  end;

  function QBVirtTableParams(ATable: TrwQBSelectedTable): Boolean;

implementation

{$R *.DFM}


function QBVirtTableParams(ATable: TrwQBSelectedTable): Boolean;
var
  Frm: TrwQBVTParams;
begin
  Frm := TrwQBVTParams.Create(Application);
  with Frm do
    try
      FTable := ATable;
      FillParams;
      if dsParams.RecordCount > 0 then
      begin
        Result := ShowModal = mrOK;
        if Result then
          FTable.TableParams.Assign(FParams);
      end
      else
        Result := False;
    finally
      Free;
    end;
end;


procedure TrwQBVTParams.FormCreate(Sender: TObject);
begin
  dsParams.CreateDataSet;
  FParams := TrwQBExpressions.Create(TrwQBExpression);
end;

procedure TrwQBVTParams.dsParamsBeforeDelete(DataSet: TDataSet);
begin
  AbortEx;
end;

procedure TrwQBVTParams.dsParamsNewRecord(DataSet: TDataSet);
begin
  if dsParams.Tag <> 0 then
    AbortEx;
end;


procedure TrwQBVTParams.FillParams;
var
  i: Integer;
  t, h: String;
  VT: TrwDataDictTable;
  ParStr: String;

  function ReadParamValue: String;
  begin
    Result := '';
    ParStr := Trim(ParStr);
    if ParStr = '' then
      Exit;

    if ParStr[1] = '''' then
    begin
      Delete(ParStr, 1, 1);
      Result := GetNextStrValue(ParStr, '''');
      GetNextStrValue(ParStr, ',');
    end
    else
      Result := Trim(GetNextStrValue(ParStr, ','));
  end;

begin
  VT := FTable.lqObject;
  if not Assigned(VT) then
    Exit;

  FParams.Assign(FTable.TableParams);

  if VT.DisplayName = '' then
    h := VT.Name
  else
    h := VT.DisplayName;
  Caption := 'Parameters of "' + h + '"';

  dsParams.Tag := 0;

  for i := 0 to FParams.Count - 1 do
  begin
    with VT.Params[i] do
    begin
      if DisplayName = '' then
        h := Name
      else
        h := DisplayName;

      case ParamType of
        ddtDateTime:  t := 'Date';
        ddtInteger:   t := 'Integer';
        ddtFloat:     t := 'Float';
        ddtCurrency:  t := 'Currency';
        ddtBoolean:   t := 'Boolean';
        ddtString:    t := 'String';
        ddtArray:     t := 'Array';
      else
        t := 'Unknown';
      end;
    end;

    dsParams.Append;
    dsParams.FieldByName('name').AsString := h;
    dsParams.FieldByName('type').AsString := t;
    dsParams.FieldByName('value').AsString :=  TrwQBExpression(FParams.Items[i]).TextForDesign;
    dsParams.Post;
  end;

  dsParams.Tag := 1;
end;


procedure TrwQBVTParams.cbExpressionCustomDlg(Sender: TObject);
begin
  if TrwQBExprEditor.ShowEditor(TrwQBExpression(FParams.Items[dsParams.RecNo - 1]), eitParam, '', DSLookup) then
  begin
    dsParams.Edit;
    dsParams.FieldByName('value').AsString :=  TrwQBExpression(FParams.Items[dsParams.RecNo - 1]).TextForDesign;
    dsParams.Post;
  end;
end;

procedure TrwQBVTParams.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FDSLookup);
  FreeAndNil(FParams);
end;

procedure TrwQBVTParams.grParamsRowChanged(Sender: TObject);
begin
  if Assigned(FDSLookup) then
    FDSLookup.Close;
end;

function TrwQBVTParams.DSLookup: TrwDataSet;
var
  P: TrwDataDictParam;
begin
  if not Assigned(FDSLookup) or not Boolean(FDSLookup.PActive) then
  begin
    FreeAndNil(FDSLookup);
    P := TrwDataDictParam(FTable.lqObject.Params[dsParams.RecNo - 1]);
    FDSLookup := CreateLookupList(FTable.lqObject, P.Name, True);
  end;

  Result := FDSLookup;
end;

end.
