unit rwInputFormControls;

interface

uses
  Classes, StdCtrls, ExtCtrls, rwCommonClasses, Graphics, Controls,
  ComCtrls, SysUtils, rwBasicClasses, rwTypes, rwUtils, rwBasicUtils,
  rwDB, DB, Forms, Dialogs, Variants, wwdbgrid, wwframe;

type

  {TrwText is just text}

  TrwText = class(TrwFormControl)
  private
    FWordWrap: Boolean;
    FAlignment: TAlignment;
    FAutosize: Boolean;
    FLayout: TTextLayout;

    procedure SetAlignment(Value: TAlignment);
    function  GetAlignment: TAlignment;
    procedure SetAutosize(Value: Boolean);
    function  GetAutosize: Boolean;
    procedure SetLayout(Value: TTextLayout);
    function  GetLayout: TTextLayout;
    procedure SetWordWrap(Value: Boolean);
    function  GetWordWrap: Boolean;

  protected
    function CreateVisualControl: IrwVisualControl; override;
    procedure SetText(const Value: string); override;
    function  GetText: string; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;

  published
    property Align;
    property Anchors;
    property Color;
    property Font;
    property Text;
    property WordWrap: Boolean read GetWordWrap write SetWordWrap;
    property Alignment: TAlignment read GetAlignment write SetAlignment;
    property Autosize: Boolean read GetAutoSize write SetAutosize;
    property Layout: TTextLayout read GetLayout write SetLayout;
  end;


  {TrwEdit is EditBox}

  TrwEdit = class(TrwParamFormControl)
  private
    FReadOnly: Boolean;
    procedure FOnChange(Sender: TObject);
    function  TextIsNotEmpty: Boolean;
    function  GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);

  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  CreateVisualControl: IrwVisualControl; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Anchors;
    property Font;
    property Color;
    property Text stored TextIsNotEmpty;
    property ParamName;
    property ParamValue;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly;
    property TabOrder;
    property TabStop;
  end;


(*
  {TrwSpinEdit is SpinBox}

  TrwSpinEdit = class(TrwParamFormControl)
  private
    FReadOnly: Boolean;
    procedure FOnChange(Sender: TObject);
    function  GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);

  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  CreateVisualControl: IrwVisualControl; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Anchors;
    property Font;
    property Color default clWindow;
    property ParamName;
    property ParamValue;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property TabOrder;
    property TabStop;
  end;
*)

    {TrwCheckBox is CheckBox}

  TrwCheckBox = class(TrwParamFormControl)
  private
    FAllowGrayed: Boolean;
    FState: TCheckBoxState;
    procedure SetState(Value: TCheckBoxState);
    function  GetState: TCheckBoxState;
    procedure SetAllowGrayed(Value: Boolean);
    function  GetAllowGrayed: Boolean;

  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetColor(Value: TColor); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;

  published
    property Anchors;
    property Color;
    property AllowGrayed: Boolean read GetAllowGrayed write SetAllowGrayed;
    property State: TCheckBoxState read GetState write SetState;
    property Text;
    property Font;
    property ParamName;
    property ParamValue;
    property TabOrder;
    property TabStop;
  end;


  {TrwDateEdit is DateEditBox}

  TrwDateEdit = class(TrwParamFormControl)
  private
    FAllowClearKey: Boolean;
    FDate: TDateTime;
    FKind: TDateTimeKind;
    procedure SetDate(Value: TDateTime);
    function  GetDate: TDateTime;
    procedure FOnChange(Sender: TObject);
    function  GetKind: TDateTimeKind;
    procedure SetKind(const Value: TDateTimeKind);
    function  GetAllowClearKey: Boolean;
    procedure SetAllowClearKey(const Value: Boolean);

  protected
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Anchors;
    property AllowClearKey: Boolean read GetAllowClearKey write SetAllowClearKey;
    property Color;
    property Date: TDateTime read GetDate write SetDate;
    property Font;
    property Kind: TDateTimeKind read GetKind write SetKind;
    property ParamName;
    property ParamValue;
    property TabOrder;
    property TabStop;
  end;


  {TrwPanel is Panel}

  TrwPanel = class(TrwWinFormControl)
  private
    FBevelInner: TBevelCut;
    FBevelOuter: TBevelCut;
    FBevelWidth: Integer;
    FBorderStyle: TBorderStyle;
    FBorderWidth: Integer;
    function  GetBevelInner: TBevelCut;
    procedure SetBevelInner(const Value: TBevelCut);
    function  GetBevelOuter: TBevelCut;
    procedure SetBevelOuter(const Value: TBevelCut);
    function  GetBevelWidth: Integer;
    procedure SetBevelWidth(const Value: Integer);
    function  GetBorderStyle: TBorderStyle;
    procedure SetBorderStyle(const Value: TBorderStyle);
    function  GetBorderWidth: Integer;
    procedure SetBorderWidth(const Value: Integer);
  protected
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Anchors;
    property Color;
    property BevelInner: TBevelCut read GetBevelInner write SetBevelInner;
    property BevelOuter: TBevelCut read GetBevelOuter write SetBevelOuter;
    property BevelWidth: Integer read GetBevelWidth write SetBevelWidth;
    property BorderStyle: TBorderStyle read GetBorderStyle write SetBorderStyle;
    property BorderWidth: Integer read GetBorderWidth write SetBorderWidth;
    property TabOrder;
    property TabStop;
  end;


  {TrwGroupBox is GroupBox}

  TrwGroupBox = class(TrwWinFormControl)
  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetColor(Value: TColor); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;

  published
    property Align;
    property Anchors;
    property Text;
    property Font;
    property Color;
    property TabOrder;
    property TabStop;
  end;


  {TrwRadioGroup is RadioGroupBox}

  TrwRadioGroup = class(TrwParamFormControl)
  private
    FItems: TrwStrings;
    FItemIndex: Integer;
    FColumns: Integer;
    procedure SetItems(Value: TrwStrings);
    function  GetItems: TrwStrings;
    procedure SetItemIndex(Value: Integer);
    function  GetItemIndex: Integer;
    function  GetColumns: Integer;
    procedure SetColumns(const Value: Integer);
    procedure FOnChangeItems(Sender: TObject);
    function  GetValue: String;
    procedure SetValue(const AValue: String);
    function  StoreValue: Boolean;

  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetColor(Value: TColor); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure InitializeForDesign; override;

  published
    property Anchors;
    property Color;
    property Items: TrwStrings read GetItems write SetItems;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property Font;
    property Columns: Integer read GetColumns write SetColumns;
    property Text;
    property ParamName;
    property ParamValue;
    property Value: String read GetValue write SetValue stored StoreValue;
    property TabOrder;
    property TabStop;
  end;


  {TrwComboBox is ComboBox}

  TrwComboBox  = class(TrwParamFormControl)
  private
    FItems: TrwStrings;
    FAllowClearKey: Boolean;
    FAllowEdit: Boolean;
    FItemIndex: Integer;
    procedure SetItems(Value: TrwStrings);
    function  GetItems: TrwStrings;
    procedure SetItemIndex(Value: Integer);
    function  GetItemIndex: Integer;
    procedure FOnChangeItems(Sender: TObject);
    function  GetValue: String;
    procedure SetValue(const AValue: String);
    procedure FOnChange(Sender: TObject);
    function  GetAllowEdit: Boolean;
    procedure SetAllowEdit(const Value: Boolean);
    function  GetAllowClearKey: Boolean;
    procedure SetAllowClearKey(const Value: Boolean);

  protected
    function  GetText: string; override;
    procedure SetText(const Value: string); override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property Anchors;
    property AllowClearKey: Boolean read GetAllowClearKey write SetAllowClearKey;
    property Color;
    property AllowEdit: Boolean read GetAllowEdit write SetAllowEdit;
    property Items:TrwStrings read GetItems write SetItems;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex stored False;
    property Font;
    property Text;
    property ParamName;
    property ParamValue;
    property Value: String read GetValue write SetValue;
    property TabOrder;
    property TabStop;
  end;


  {TrwComboBox is ComboBox}

  TrwDBComboBox  = class(TrwParamFormControl)
  private
    FDataSource: TrwDataSet;
    FDisplayFields: String;
    FKeyField: string;
    FAllowClearKey: Boolean;

    function  GetValue: String;
    procedure SetValue(const AValue: String);
    procedure FOnChange(Sender: TObject);
    procedure SetDataSource(const Value: TrwDataSet);
    procedure SetKeyField(const Value: string);
    function  GetKeyField: string;
    procedure SetDisplayFields(const Value: string);
    function  StoreValue: Boolean;
    function  GetAllowClearKey: Boolean;
    procedure SetAllowClearKey(const Value: Boolean);

  protected
    procedure SetText(const Value: string); override;
    function  GetText: string; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;

  published
    property Anchors;
    property AllowClearKey: Boolean read GetAllowClearKey write SetAllowClearKey;
    property Color;
    property Font;
    property Text stored False;
    property ParamName;
    property ParamValue;
    property Value: String read GetValue write SetValue stored StoreValue;
    property DataSource: TrwDataSet read FDataSource write SetDataSource;
    property DisplayFields: string read FDisplayFields write SetDisplayFields;
    property KeyField: string read GetKeyField write SetKeyField;
    property TabOrder;
    property TabStop;
  end;


  {TrwGrid}

  TrwDBGrid  = class(TrwParamFormControl)
  private
    FDataSource: TrwDataSet;
    FDataLink:   TDataSource;
    FKeyField:   String;
    FMultiSelection: Boolean;

    procedure SetDataSource(const Value: TrwDataSet);
    procedure TitleButtonClick(Sender: TObject; AFieldName: String);
    procedure OnFilterChange(Sender: TObject; DataSet: TDataSet; NewFilter: string);
    function  KeyFieldIsNotEmpty: Boolean;
    function  GetMultiSelection: Boolean;
    procedure SetMultiSelection(const Value: Boolean);
    procedure FOnRowChanged(Sender: Tobject);
    procedure FOnCellChanged(Sender: Tobject);
    procedure FOnMultiSelectRecord(Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
    procedure FOnSelectionChange(Sender: Tobject);

  protected
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;

  published
    property Align;
    property Anchors;
    property Color;
    property ParamName;
    property ParamValue;
    property Font;
    property DataSource: TrwDataSet read FDataSource write SetDataSource;
    property KeyField: String read FKeyField write FKeyField stored KeyFieldIsNotEmpty;
    property MultiSelection: Boolean read GetMultiSelection write SetMultiSelection;
    property TabOrder;
    property TabStop;

    procedure PSelectAll;
    procedure PUnSelectAll;
    procedure PSelectRecord;
  end;


  {TrwTabSheet}
  TrwTabSheet = class(TrwWinFormControl)
  private
    FTabVisible: Boolean;
    function  GetTabVisible: Boolean;
    procedure SetTabVisible(const Value: Boolean);
    procedure FOnShow(Sender: TObject);
    procedure FOnHide(Sender: TObject);

  protected
    function  GetText: String; override;
    procedure SetText(const Value: String); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure RegisterComponentEvents; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure   HideRT; override;
    procedure   InitializeForDesign; override;

  published
    property Caption: String read GetText write SetText;
    property TabVisible: Boolean read GetTabVisible write SetTabVisible;
  end;


  {TrwPageControl}
  TrwPageControl = class(TrwWinFormControl)
  private
    FActivePageIndex: Integer;
    FDoNotFireEvents: Boolean;
    function  GetActivePageIndex: Integer;
    procedure SetActivePageIndex(const Value: Integer);
    procedure FOnChange(Sender: TObject);
    procedure FOnChanging(Sender: TObject; var AllowChange: Boolean);
    function  GetActivePage: TrwTabSheet;
    procedure SetActivePage(const Value: TrwTabSheet);

  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure RegisterComponentEvents; override;
    procedure SetDesignParent(Value: TWinControl); override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Anchors;
    property ActivePageIndex: Integer read GetActivePageIndex write SetActivePageIndex;
    property ActivePage: TrwTabSheet read GetActivePage write SetActivePage stored False;
    property TabOrder;
    property TabStop;

    function PageCount: Variant;
    function PageByIndex(AIndex: Variant): Variant;
  end;


  TrwButton = class(TrwFormControl)
  private
  protected
    function  GetText: String; override;
    procedure SetText(const Value: String); override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;

  published
    property Anchors;
    property Font;
    property Caption: String read GetText write SetText;
    property TabOrder;
    property TabStop;
  end;


  TrwFileDialogType = (rfdOpenFile, rfdSaveFile);

  TrwFileDialog = class(TrwParamFormControl)
  private
    FDialogType: TrwFileDialogType;
    FFilterIndex: Integer;
    FDefaultExt: string;
    FInitialDir: string;
    FFilter: string;
    FOptions: TOpenOptions;
    FReadOnly: Boolean;

    procedure FOnChange(Sender: TObject);
    procedure FOnCustomDlg(Sender: TObject);
    procedure SetDialogType(const AValue: TrwFileDialogType);
    function  GetDialogType: TrwFileDialogType;
    function  GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
    function  GetFileName: string;
    procedure SetFileName(const AValue: string);
  protected
    function  GetParamValue: Variant; override;
    procedure SetParamValue(Value: Variant); override;
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Anchors;
    property Font;
    property ParamName;
    property ParamValue;
    property DialogType: TrwFileDialogType read GetDialogType write SetDialogType;
    property DefaultExt: string read FDefaultExt write FDefaultExt;
    property Filter: string read FFilter write FFilter;
    property FileName: string read GetFileName write SetFileName;
    property FilterIndex: Integer read FFilterIndex write FFilterIndex;
    property InitialDir: string read FInitialDir write FInitialDir;
    property Options: TOpenOptions read FOptions write FOptions;
    property Title: string read GetText write SetText;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly;
    property TabOrder;
    property TabStop;

    function PShowDialog: Variant;
  end;


implementation

uses rwDsgnControls;

  {TrwText}

constructor TrwText.Create(AOwner: TComponent);
begin
  inherited;
  Color := clBtnFace;
  WordWrap := False;
  Autosize := True;
  Layout := tlTop;
end;
  
procedure TrwText.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwTextControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    inherited;
end;

function TrwText.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwTextControl(VisualControl.RealObject).Caption;
  Result := Inherited GetText;  
end;

procedure TrwText.SetAlignment(Value: TAlignment);
begin
  if VisualControlCreated then
  begin
    TrwTextControl(VisualControl.RealObject).Alignment := Value;
    GetAlignment;
  end
  else
    FAlignment := Value;
end;

function TrwText.GetAlignment: TAlignment;
begin
  if VisualControlCreated then
    FAlignment := TrwTextControl(VisualControl.RealObject).Alignment;
  Result := FAlignment;  
end;

procedure TrwText.SetAutosize(Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwTextControl(VisualControl.RealObject).Autosize := Value;
    GetAutosize;
  end
  else
    FAutosize := Value;
end;

function TrwText.GetAutosize: Boolean;
begin
  if VisualControlCreated then
    FAutosize := TrwTextControl(VisualControl.RealObject).Autosize;
  Result := FAutosize;
end;

procedure TrwText.SetWordWrap(Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwTextControl(VisualControl.RealObject).WordWrap := Value;
    GetWordWrap;
  end
  else
    FWordWrap := Value;
end;

function TrwText.GetWordWrap: Boolean;
begin
  if VisualControlCreated then
    FWordWrap := TrwTextControl(VisualControl.RealObject).WordWrap;
  Result := FWordWrap;
end;

procedure TrwText.SetLayout(Value: TTextLayout);
begin
  if VisualControlCreated then
  begin
    TrwTextControl(VisualControl.RealObject).Layout := Value;
    GetLayout;
  end
  else
    FLayout := Value;
end;

function TrwText.GetLayout: TTextLayout;
begin
  if VisualControlCreated then
    FLayout := TrwTextControl(VisualControl.RealObject).Layout;
  Result := FLayout;  
end;

procedure TrwText.InitializeForDesign;
begin
  inherited;
  Text := Name;
  Autosize := not Autosize;
  Autosize := not Autosize;
end;


function TrwText.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwTextControl.Create(Self);
  TrwTextControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);
end;


{TrwEdit}

constructor TrwEdit.Create(AOwner: TComponent);
begin
  inherited;
  Color := clWindow;
  ReadOnly := False;
  Width := 121;
  Height := 21;
end;

procedure TrwEdit.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwEditControl(VisualControl.RealObject).Text := Value;
    GetText;
  end
  else
    FText := Value;
end;

function TrwEdit.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwEditControl(VisualControl.RealObject).Text;
  Result := FText;  
end;

function TrwEdit.GetParamValue: Variant;
begin
  if VisualControlCreated and TrwEditControl(VisualControl.RealObject).Grayed then
    FParamValue := Null
  else
    FParamValue := Text;

  Result := FParamValue;
end;

procedure TrwEdit.SetParamValue(Value: Variant);
begin
  if VarIsNull(Value) then
  begin
    if VisualControlCreated then
      TrwEditControl(VisualControl.RealObject).Grayed := True;
    FParamValue := Null;
  end
  else
  begin
    if VisualControlCreated then
      TrwEditControl(VisualControl.RealObject).Grayed := False;
    FParamValue := Value;      
    Text := VarToStr(Value);
  end;
end;

procedure TrwEdit.FOnChange(Sender: TObject);
begin
  ExecuteEventsHandler('OnChange', []);
end;

procedure TrwEdit.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnChange]);
end;

function TrwEdit.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwEditControl.Create(Self);
  TrwEditControl(Result.RealObject).OnChange := FOnChange;
  TrwEditControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);
end;

function TrwEdit.TextIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(Text) > 0);
end;

function TrwEdit.GetReadOnly: Boolean;
begin
  if VisualControlCreated then
    FReadOnly := TrwEditControl(VisualControl.RealObject).ReadOnly;
  Result := FReadOnly;  
end;

procedure TrwEdit.SetReadOnly(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwEditControl(VisualControl.RealObject).ReadOnly := Value;
    GetReadOnly;
  end
  else
    FReadOnly := Value;
end;



{TrwDateEdit}

constructor TrwDateEdit.Create(AOwner: TComponent);
begin
  inherited;
  Color := clWindow;
  AllowClearKey := False;
  Kind := dtkDate;
  Date := Now;
  Width := 186;
  Height := 21;  
end;

procedure TrwDateEdit.SetDate(Value: TDateTime);
begin
  if VisualControlCreated then
  begin
    TrwDateEditControl(VisualControl.RealObject).SetDateTime(Value);
    GetDate;
  end
  else
    if Kind = dtkDate then
      FDate := Trunc(Value)
    else
      FDate := SysUtils.Date + Frac(Value);
end;

function TrwDateEdit.GetDate: TDateTime;
begin
  if VisualControlCreated then
    FDate := TrwDateEditControl(VisualControl.RealObject).DateTime;

  if Kind = dtkDate then
    FDate := Trunc(FDate)
  else
    FDate := Frac(FDate);

  Result := FDate;
end;

function TrwDateEdit.GetParamValue: Variant;
var
  F: Real;
  D: TDateTime;
begin
  if VisualControlCreated and TrwDateEditControl(VisualControl.RealObject).Grayed then
    FParamValue := Null

  else
  begin
    if Kind = dtkDate then
      F := Trunc(Date)
    else
      F := Frac(Date);
    D := F;
    FParamValue := D;
  end;

  Result := FParamValue;
end;

procedure TrwDateEdit.SetParamValue(Value: Variant);
var
  D: TDateTime;
begin
  if VarIsNull(Value) then
  begin
    if VisualControlCreated then
      TrwDateEditControl(VisualControl.RealObject).Grayed := True;
    FParamValue := Null;  
  end
  else
  begin
    if VisualControlCreated then
      TrwDateEditControl(VisualControl.RealObject).Grayed := False;
    D := VarAsType(Value, varDate);
    if Kind = dtkDate then
      D := Trunc(D)
    else
      D := Frac(D);
    Date := D;
    FParamValue := Date;
  end;
end;

procedure TrwDateEdit.FOnChange(Sender: TObject);
begin
  ExecuteEventsHandler('OnChange', []);
end;

procedure TrwDateEdit.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnChange]);
end;

function TrwDateEdit.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwDateEditControl.Create(Self);
  TrwDateEditControl(Result.RealObject).OnChange := FOnChange;
  TrwDateEditControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

function TrwDateEdit.GetKind: TDateTimeKind;
begin
  if VisualControlCreated then
    FKind := TrwDateEditControl(VisualControl.RealObject).Kind;
  Result := FKind;
end;

procedure TrwDateEdit.SetKind(const Value: TDateTimeKind);
begin
  if VisualControlCreated then
  begin
    TrwDateEditControl(VisualControl.RealObject).Kind := Value;
    GetKind;
  end
  else
    FKind := Value;
end;

function TrwDateEdit.GetAllowClearKey: Boolean;
begin
  if VisualControlCreated then
    FAllowClearKey := TrwDateEditControl(VisualControl.RealObject).AllowClearKey;
  Result := FAllowClearKey;  
end;

procedure TrwDateEdit.SetAllowClearKey(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwDateEditControl(VisualControl.RealObject).AllowClearKey := Value;
    GetAllowClearKey;
  end
  else
    FAllowClearKey := Value;
end;



{TrwCheckBox}

constructor TrwCheckBox.Create(AOwner: TComponent);
begin
  inherited;
  Color := clBtnFace;
  AllowGrayed := False;
  State := cbUnchecked;
  Width := 97;
  Height := 17;
end;

procedure TrwCheckBox.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwCheckBoxControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    FText := Value;
end;

function TrwCheckBox.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwCheckBoxControl(VisualControl.RealObject).Caption;
  Result := FText;
end;

function TrwCheckBox.GetParamValue: Variant;
begin
  if VisualControlCreated and TrwCheckBoxControl(VisualControl.RealObject).Grayed then
    FParamValue := Null

  else
    case TrwCheckBoxControl(VisualControl.RealObject).State of
      cbUnchecked: FParamValue := False;
      cbChecked:   FParamValue := True;
      cbGrayed:    FParamValue := Null;
    end;

  Result := FParamValue;
end;

procedure TrwCheckBox.SetParamValue(Value: Variant);
begin
  if VarIsNull(Value) then
  begin
    if VisualControlCreated then
      TrwCheckBoxControl(VisualControl.RealObject).Grayed := True;
    FParamValue := Null;
  end
  else
  begin
    if VisualControlCreated then
      if VarAsType(Value, varBoolean) then
        State := cbChecked
      else
        State := cbUnchecked;
    FParamValue := VarAsType(Value, varBoolean);
  end;
end;

procedure TrwCheckBox.SetAllowGrayed(Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwCheckBoxControl(VisualControl.RealObject).AllowGrayed := Value;
    GetAllowGrayed;
  end
  else
    FAllowGrayed := Value;
end;

function TrwCheckBox.GetAllowGrayed: Boolean;
begin
  if VisualControlCreated then
    FAllowGrayed := TrwCheckBoxControl(VisualControl.RealObject).AllowGrayed;
  Result := FAllowGrayed;  
end;

procedure TrwCheckBox.SetState(Value: TCheckBoxState);
begin
  if VisualControlCreated then
  begin
    TrwCheckBoxControl(VisualControl.RealObject).State := Value;
    GetState;
  end
  else
    FState := Value;
end;

function TrwCheckBox.GetState: TCheckBoxState;
begin
  if VisualControlCreated then
    FState := TrwCheckBoxControl(VisualControl.RealObject).State;
  Result := FState;  
end;


function TrwCheckBox.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwCheckBoxControl.Create(Self);
  TrwCheckBoxControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

procedure TrwCheckBox.InitializeForDesign;
begin
  inherited;
  Text := Name;
end;


procedure TrwCheckBox.SetColor(Value: TColor);
begin
  Value := clBtnFace;
  inherited;
end;


{TrwPanel}

constructor TrwPanel.Create(AOwner: TComponent);
begin
  inherited;
  Color := clBtnFace;
  TabStop := False;
  BevelInner := bvNone;
  BevelOuter := bvRaised;
  BevelWidth := 1;
  BorderStyle := bsNone;
  BorderWidth := 0;
  Width := 185;
  Height := 41;
end;

function TrwPanel.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwPanelControl.Create(Self);
  TrwPanelControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

function TrwPanel.GetBevelInner: TBevelCut;
begin
  if VisualControlCreated then
    FBevelInner := TrwPanelControl(VisualControl.RealObject).BevelInner;
  Result := FBevelInner;  
end;

function TrwPanel.GetBevelOuter: TBevelCut;
begin
  if VisualControlCreated then
    FBevelOuter := TrwPanelControl(VisualControl.RealObject).BevelOuter;
  Result := FBevelOuter;  
end;

function TrwPanel.GetBevelWidth: Integer;
begin
  if VisualControlCreated then
    FBevelWidth := TrwPanelControl(VisualControl.RealObject).BevelWidth;
  Result := FBevelWidth;  
end;

function TrwPanel.GetBorderStyle: TBorderStyle;
begin
  if VisualControlCreated then
    FBorderStyle := TrwPanelControl(VisualControl.RealObject).BorderStyle;
  Result := FBorderStyle;  
end;

function TrwPanel.GetBorderWidth: Integer;
begin
  if VisualControlCreated then
    FBorderWidth := TrwPanelControl(VisualControl.RealObject).BorderWidth;
  Result := FBorderWidth;  
end;

procedure TrwPanel.SetBevelInner(const Value: TBevelCut);
begin
  if VisualControlCreated then
  begin
    TrwPanelControl(VisualControl.RealObject).BevelInner := Value;
    GetBevelInner;
  end
  else
    FBevelInner := Value;
end;

procedure TrwPanel.SetBevelOuter(const Value: TBevelCut);
begin
  if VisualControlCreated then
  begin
    TrwPanelControl(VisualControl.RealObject).BevelOuter := Value;
    GetBevelOuter;
  end
  else
    FBevelOuter := Value;
end;

procedure TrwPanel.SetBevelWidth(const Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwPanelControl(VisualControl.RealObject).BevelWidth := Value;
    GetBevelWidth;
  end
  else
    FBevelWidth := Value;
end;

procedure TrwPanel.SetBorderStyle(const Value: TBorderStyle);
begin
  if VisualControlCreated then
  begin
    TrwPanelControl(VisualControl.RealObject).BorderStyle := Value;
    GetBorderStyle;
  end
  else
    FBorderStyle := Value;
end;


procedure TrwPanel.SetBorderWidth(const Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwPanelControl(VisualControl.RealObject).BorderWidth := Value;
    GetBorderWidth;
  end
  else
    FBorderWidth := Value;
end;


{TrwGroupBox}

constructor TrwGroupBox.Create(AOwner: TComponent);
begin
  inherited;
  Color := clBtnFace;
  TabStop := False;
  Width := 185;
  Height := 105;
end;

procedure TrwGroupBox.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwGroupBoxControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    FText := Value;
end;

function TrwGroupBox.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwGroupBoxControl(VisualControl.RealObject).Caption;
  Result := FText;  
end;

function TrwGroupBox.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwGroupBoxControl.Create(Self);
  TrwGroupBoxControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

procedure TrwGroupBox.InitializeForDesign;
begin
  inherited;
  Text := Name;
end;


procedure TrwGroupBox.SetColor(Value: TColor);
begin
  Value := clBtnFace;
  inherited;
end;

{ TrwRadioGroup }

constructor TrwRadioGroup.Create(AOwner: TComponent);
begin
  inherited;
  FItems := TrwStrings.Create;
  TStringList(FItems).OnChange := FOnChangeItems;
  TabStop := False;
  Color := clBtnFace;
  Columns := 1;
  Width := 185;
  Height := 105;
end;

destructor TrwRadioGroup.Destroy;
begin
  FreeAndNil(FItems);
  inherited;
end;

function TrwRadioGroup.GetParamValue: Variant;
begin
  if VisualControlCreated and TrwRadioGroupControl(VisualControl.RealObject).Grayed then
    FParamValue := Null

  else if (ItemIndex = -1) or (ItemIndex >= FItems.Count) then
    FParamValue := ''
  else if FItems.Names[ItemIndex] = '' then
    FParamValue := FItems[ItemIndex]
  else
    FParamValue := FItems.Values[FItems.Names[ItemIndex]];

  Result := FParamValue;
end;

procedure TrwRadioGroup.SetParamValue(Value: Variant);
var
  i, j: Integer;
begin
  if VarIsNull(Value) then
  begin
    if VisualControlCreated then
      TrwRadioGroupControl(VisualControl.RealObject).Grayed := True;
    FParamValue := Null;
  end
  else
  begin
    FParamValue := '';
    j := -1;
    for i := 0 to FItems.Count-1 do
      if FItems.Values[FItems.Names[i]] = Value then
      begin
        j := i;
        FParamValue := Value;        
        break;
      end;
    ItemIndex := j;
  end;
end;

function TrwRadioGroup.GetItems: TrwStrings;
begin
  Result := FItems;
end;

procedure TrwRadioGroup.SetItems(Value: TrwStrings);
begin
  FItems.Assign(Value);
end;

function TrwRadioGroup.GetColumns: Integer;
begin
  if VisualControlCreated then
    FColumns := TrwRadioGroupControl(VisualControl.RealObject).Columns;
  Result := FColumns;  
end;

procedure TrwRadioGroup.SetColumns(const Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwRadioGroupControl(VisualControl.RealObject).Columns := Value;
    GetColumns;
  end
  else
    FColumns := Value;
end;

function TrwRadioGroup.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwRadioGroupControl(VisualControl.RealObject).Caption;
  Result := FText;
end;

procedure TrwRadioGroup.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwRadioGroupControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    FText := Value;
end;

procedure TrwRadioGroup.FOnChangeItems(Sender: TObject);
var
  i: Integer;
begin
  if VisualControlCreated then
  begin
    TrwRadioGroupControl(VisualControl.RealObject).Items.BeginUpdate;
    try
      TrwRadioGroupControl(VisualControl.RealObject).Items.Clear;
      for i := 0 to FItems.Count-1 do
        if FItems.Names[i] = '' then
          TrwRadioGroupControl(VisualControl.RealObject).Items.Add(FItems[i])        
        else
          TrwRadioGroupControl(VisualControl.RealObject).Items.Add(FItems.Names[i]);
    finally
      TrwRadioGroupControl(VisualControl.RealObject).Items.EndUpdate;
    end;
  end;  
end;

function TrwRadioGroup.GetItemIndex: Integer;
begin
  if VisualControlCreated then
    FItemIndex := TrwRadioGroupControl(VisualControl.RealObject).ItemIndex;
  Result := FItemIndex;  
end;

procedure TrwRadioGroup.SetItemIndex(Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwRadioGroupControl(VisualControl.RealObject).ItemIndex := Value;
    GetItemIndex;
  end
  else
    FItemIndex := Value;
end;

function TrwRadioGroup.GetValue: String;
var
  V: Variant;
begin
  V := GetParamValue;
  if VarIsNull(V) then
    Result := ''
  else
    Result := V;
end;

procedure TrwRadioGroup.SetValue(const AValue: String);
begin
  SetParamValue(AValue);
end;

function TrwRadioGroup.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwRadioGroupControl.Create(Self);
  TrwRadioGroupControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;


function TrwRadioGroup.StoreValue: Boolean;
begin
  Result := not (csWriting in ComponentState);
end;

procedure TrwRadioGroup.InitializeForDesign;
begin
  inherited;
  Text := Name;
end;


procedure TrwRadioGroup.SetColor(Value: TColor);
begin
  Value := clBtnFace;
  inherited;
end;

{ TrwComboBox }

constructor TrwComboBox.Create(AOwner: TComponent);
begin
  inherited;
  FItems := TrwStrings.Create;
  FItems.OnChange := FOnChangeItems;
  AllowEdit := False;
  AllowClearKey := False;
  Color := clWindow;
  ItemIndex := -1;
  Width := 145;
  Height := 21;
end;

destructor TrwComboBox.Destroy;
begin
  FreeAndNil(FItems);
  inherited;
end;

procedure TrwComboBox.FOnChangeItems(Sender: TObject);
var
  i: Integer;
begin
  if VisualControlCreated then
  begin
    TrwComboBoxControl(VisualControl.RealObject).Items.BeginUpdate;
    try
      TrwComboBoxControl(VisualControl.RealObject).Items.Clear;
      for i := 0 to FItems.Count-1 do
        if Length(FItems.Names[i]) = 0 then
          TrwComboBoxControl(VisualControl.RealObject).Items.Add(FItems[i])
        else
          TrwComboBoxControl(VisualControl.RealObject).Items.Add(FItems.Names[i]);
    finally
      TrwComboBoxControl(VisualControl.RealObject).Items.EndUpdate;
    end;
  end;  
end;

procedure TrwComboBox.FOnChange(Sender: TObject);
begin
  ExecuteEventsHandler('OnChange', []);
end;

function TrwComboBox.GetAllowEdit: Boolean;
begin
  if VisualControlCreated then
    FAllowEdit := TrwComboBoxControl(VisualControl.RealObject).Style = StdCtrls.csDropDown;
  Result := FAllowEdit;    
end;

function TrwComboBox.GetItemIndex: Integer;
begin
  if VisualControlCreated then
    FItemIndex := TrwComboBoxControl(VisualControl.RealObject).ItemIndex;
  Result := FItemIndex;  
end;

function TrwComboBox.GetItems: TrwStrings;
begin
  Result := FItems;
end;

function TrwComboBox.GetParamValue: Variant;
begin
  if (ItemIndex = -1) or (ItemIndex >= FItems.Count) then
    if VisualControlCreated and TrwComboBoxControl(VisualControl.RealObject).Grayed then
      FParamValue := Null
    else
      FParamValue := ''

  else
    if Length(FItems.Names[ItemIndex]) = 0 then
      FParamValue := FItems[ItemIndex]
    else
      FParamValue := FItems.Values[FItems.Names[ItemIndex]];

  Result := FParamValue;    
end;

function TrwComboBox.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwComboBoxControl(VisualControl.RealObject).Text;
  Result := FText;  
end;

function TrwComboBox.GetValue: String;
var
  V: Variant;
begin
  V := GetParamValue;
  if VarIsNull(V) then
    Result := ''
  else
    Result := V;
end;

procedure TrwComboBox.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnChange]);
end;

procedure TrwComboBox.SetAllowEdit(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    if Value then
      TrwComboBoxControl(VisualControl.RealObject).Style := StdCtrls.csDropDown
    else
      TrwComboBoxControl(VisualControl.RealObject).Style := StdCtrls.csDropDownList;
    GetAllowEdit;  
  end
  else
    FAllowEdit := Value;
end;

procedure TrwComboBox.SetItemIndex(Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwComboBoxControl(VisualControl.RealObject).ItemIndex := Value;
    GetItemIndex;
  end
  else
    FItemIndex := Value;
end;

procedure TrwComboBox.SetItems(Value: TrwStrings);
begin
  FItems.Assign(Value);
end;

procedure TrwComboBox.SetParamValue(Value: Variant);
var
  i, j: Integer;
begin
  if VarIsNull(Value) then
  begin
    if VisualControlCreated then
      TrwComboBoxControl(VisualControl.RealObject).Grayed := True;
    FParamValue:= Null;
  end

  else
  begin
    if VisualControlCreated then
      TrwComboBoxControl(VisualControl.RealObject).Grayed := False;
    FParamValue := '';
    j := -1;
    for i := 0 to FItems.Count-1 do
      if (Length(FItems.Names[i]) > 0) and (FItems.Values[FItems.Names[i]] = Value) or
         (Length(FItems.Names[i]) = 0) and (FItems[i] = Value) then
      begin
        j := i;
        FParamValue := Value;
        break;
      end;

    if (j = -1) and not AllowClearKey and (FItems.Count > 0) then
      j := 0;
    ItemIndex := j;
  end;
end;


procedure TrwComboBox.SetValue(const AValue: String);
begin
  SetParamValue(AValue);
end;

function TrwComboBox.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwComboBoxControl.Create(Self);
  TrwComboBoxControl(Result.RealObject).OnChange := FOnChange;
  TrwComboBoxControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

function TrwComboBox.GetAllowClearKey: Boolean;
begin
  if VisualControlCreated then
    FAllowClearKey := TrwComboBoxControl(VisualControl.RealObject).AllowClearKey;
  Result := FAllowClearKey;  
end;

procedure TrwComboBox.SetAllowClearKey(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwComboBoxControl(VisualControl.RealObject).AllowClearKey := Value;
    GetAllowClearKey;
  end
  else
    FAllowClearKey := Value;
end;

procedure TrwComboBox.SetText(const Value: string);
begin
end;


{ TrwDBGrid }

constructor TrwDBGrid.Create(AOwner: TComponent);
begin
  inherited;
  FDataLink := TDataSource.Create(nil);
  MultiSelection := True;
  Width := 320;
  Height := 120;
end;

function TrwDBGrid.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwDBGridControl.Create(Self);
  TrwDBGridControl(Result.RealObject).DataSource := FDataLink;
  TrwDBGridControl(Result.RealObject).OnTitleButtonClick := TitleButtonClick;
  TrwDBGridControl(Result.RealObject).OnFilterChange := OnFilterChange;
  TrwDBGridControl(Result.RealObject).OnRowChanged := FOnRowChanged;
  TrwDBGridControl(Result.RealObject).OnCellChanged := FOnCellChanged;
  TrwDBGridControl(Result.RealObject).OnMultiSelectRecord := FOnMultiSelectRecord;
  TrwDBGridControl(Result.RealObject).OnSelectionChange := FOnSelectionChange;
  Result.PropChanged(rwCPCreation);
end;

destructor TrwDBGrid.Destroy;
begin
  DataSource := nil;
  FreeAndNil(FDataLink);
  inherited;
end;


procedure TrwDBGrid.FOnCellChanged(Sender: Tobject);
begin
  ExecuteEventsHandler('OnCellChanged', []);
end;

procedure TrwDBGrid.FOnRowChanged(Sender: Tobject);
begin
  ExecuteEventsHandler('OnRowChanged', []);
end;

function TrwDBGrid.GetMultiSelection: Boolean;
begin
  if VisualControlCreated then
    FMultiSelection := TrwDBGridControl(VisualControl.RealObject).Multiselection;
  Result := FMultiSelection;   
end;

function TrwDBGrid.GetParamValue: Variant;
var
  i: Integer;
  BM: TBookmark;
  F: TField;
begin
  try
    if VisualControlCreated then
    begin
      if MultiSelection then
        FParamValue := VarArrayCreate([0, TrwDBGridControl(VisualControl.RealObject).SelectedList.Count - 1], varVariant)
      else
        FParamValue := VarArrayCreate([0, 0], varVariant);

      if Assigned(DataSource) and KeyFieldIsNotEmpty then
        with TrwDBGridControl(VisualControl.RealObject), TrwDBGridControl(VisualControl.RealObject).DataSource do
        begin
          if not DataSet.Active then
            Exit;

          F := DataSet.FieldByName(FKeyField);
          if not MultiSelection then
          begin
            FParamValue[0] := F.Value;
            Exit;
          end;

          if GrayMode then
          begin
            FParamValue := Null;
            Exit;
          end;

          DataSet.DisableControls;
          BM := DataSet.GetBookmark;
          for i := 0 to SelectedList.Count - 1 do
          begin
            DataSet.GotoBookmark(SelectedList[i]);
            FParamValue[i] := F.Value;
          end;
          DataSet.GotoBookmark(BM);
          DataSet.FreeBookmark(BM);
          DataSet.EnableControls;
        end;
    end;

  finally
    Result := FParamValue;
  end;  
end;


function TrwDBGrid.KeyFieldIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(FKeyField) > 0);
end;

procedure TrwDBGrid.OnFilterChange(Sender: TObject; DataSet: TDataSet; NewFilter: string);
begin
  DataSource.SetFilterNoApplay(NewFilter);
end;


procedure TrwDBGrid.FOnMultiSelectRecord(Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
var
  A: Variant;
begin
  A := Accept;
  ExecuteEventsHandler('OnSelectRecord', [Selecting, Integer(Addr(A))]);
  Accept := A;

  if Accept then
    FOnSelectionChange(Grid);
end;

procedure TrwDBGrid.PSelectAll;
begin
  if VisualControlCreated then
    TrwDBGridControl(VisualControl.RealObject).SelectAll;
end;

procedure TrwDBGrid.PSelectRecord;
begin
  if VisualControlCreated then
    TrwDBGridControl(VisualControl.RealObject).SelectRecord;
end;

procedure TrwDBGrid.PUnSelectAll;
begin
  if VisualControlCreated then
    TrwDBGridControl(VisualControl.RealObject).UnSelectAll;
end;

procedure TrwDBGrid.RegisterComponentEvents;
begin
  inherited;
  UnRegisterEvents([cEventOnClick]);
  RegisterEvents([cEventRowChanged, cEventCellChanged, cEventSelectRecord, cEventSelectionChange]);
end;


procedure TrwDBGrid.RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation);
var
  i: Integer;
  F: TField;
  h: String;
begin
  inherited;

  if Sender = FDataSource then
    if AOperation = rnoDestroy then
      DataSource := nil

//    else if AOperation = rnoDataSetChanged then
//      GetParamValue
    else if (AOperation = rnoLayoutChanged) and VisualControlCreated then
    begin
      if TrwDBGridControl(VisualControl.RealObject).DataSource.DataSet.Active then
      begin
        for i := 0 to DataSource.Fields.Count -1 do
        begin
          F := TrwDBGridControl(VisualControl.RealObject).DataSource.DataSet.Fields.FindField(DataSource.Fields[i].FieldName);
          if Assigned(F) then
          begin
            F.Visible := DataSource.Fields[i].Visible;
            F.DisplayLabel := DataSource.Fields[i].DisplayLabel;
            F.DisplayWidth := DataSource.Fields[i].DisplayWidth;
          end;
        end;

        h := DataSource.Sort;
        if (Length(h) > 0) and (h[Length(h)] <> ';') then
          h := h + ';';

        if not AnsiSameText(TrwDBGridControl(VisualControl.RealObject).SortField, h) then
          TrwDBGridControl(VisualControl.RealObject).SortField := DataSource.Sort;

        if not TrwDBGridControl(VisualControl.RealObject).DataSource.DataSet.ControlsDisabled then
          TrwDBGridControl(VisualControl.RealObject).LayoutChanged;
      end;
    end;
end;

procedure TrwDBGrid.SetDataSource(const Value: TrwDataSet);
begin
  if FDataSource = Value then
    Exit;

  if Assigned(FDataSource) then
    FDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FDataSource := Value;

  if VisualControlCreated then
//    if Assigned(Value) then
//      TrwDBGridControl(VisualControl.RealObject).SortField := DataSource.Sort
//    else
      if not Assigned(FDataSource) and (TrwDBGridControl(VisualControl.RealObject).SelectedList.Count > 0) then
        TrwDBGridControl(VisualControl.RealObject).UnselectAll;

  if Assigned(Value) then
    FDataLink.DataSet := Value.VCLDataSet
  else
    FDataLink.DataSet := nil;

  if Assigned(FDataSource) then
    RWNotification(FDataSource, rnoLayoutChanged);
end;

procedure TrwDBGrid.SetMultiSelection(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwDBGridControl(VisualControl.RealObject).Multiselection := Value;
    GetMultiSelection;
  end
  else
    FMultiSelection := Value;  
end;

procedure TrwDBGrid.SetParamValue(Value: Variant);
var
  i: Integer;
  BM: TBookmark;
  F: TField;
  flFound: Boolean;
begin
  FParamValue := Value;

  if VisualControlCreated then
  begin
    if Assigned(DataSource) and KeyFieldIsNotEmpty then
      with TrwDBGridControl(VisualControl.RealObject), TrwDBGridControl(VisualControl.RealObject).DataSource do
      begin
        if not DataSet.Active then
          Exit;

        if VarIsNull(FParamValue) then
        begin
          GrayMode := True;
          Exit;
        end
        else
          GrayMode := False;

        F := DataSet.FieldByName(FKeyField);
        DataSet.DisableControls;
        TrwDBGridControl(VisualControl.RealObject).UnselectAll;
        BM := DataSet.GetBookmark;
        for i := VarArrayLowBound(Value, 1) to VarArrayHighBound(Value, 1) do
        begin
          DataSet.First;
          flFound := False;
          while not DataSet.Eof do
          begin
            if FParamValue[i] = F.Value then
            begin
              flFound := True;
              if MultiSelection then
                SelectedList.Add(DataSet.GetBookmark);
              break;
            end;
            DataSet.Next;
          end;

          if not MultiSelection then
          begin
            if not flFound then
              DataSet.GotoBookmark(BM);
            break;
          end;
        end;

        if MultiSelection then
          DataSet.First;

        DataSet.FreeBookmark(BM);
        DataSet.EnableControls;

        FOnSelectionChange(TrwDBGridControl(VisualControl.RealObject));
      end;
  end;
end;


procedure TrwDBGrid.TitleButtonClick(Sender: TObject; AFieldName: String);
begin
  if VisualControlCreated then
    DataSource.Sort := TrwDBGridControl(VisualControl.RealObject).SortField;
end;


procedure TrwDBGrid.FOnSelectionChange(Sender: Tobject);
begin
  ExecuteEventsHandler('OnSelectionChange', []);
end;


{ TrwDBComboBox }

constructor TrwDBComboBox.Create(AOwner: TComponent);
begin
  inherited;
  AllowClearKey := False;
  Color := clWindow;
  Width := 145;
  Height := 21;
end;

function TrwDBComboBox.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwDBComboBoxControl.Create(Self);
  TrwDBComboBoxControl(Result.RealObject).OnChange := FOnChange;
  TrwDBComboBoxControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

destructor TrwDBComboBox.Destroy;
begin
  DataSource := nil;
  inherited;
end;

procedure TrwDBComboBox.FOnChange(Sender: TObject);
begin
  ExecuteEventsHandler('OnChange', []);
end;

function TrwDBComboBox.GetAllowClearKey: Boolean;
begin
  if VisualControlCreated then
    FAllowClearKey := TrwDBComboBoxControl(VisualControl.RealObject).AllowClearKey;
  Result := FAllowClearKey;  
end;

function TrwDBComboBox.GetKeyField: string;
begin
  if VisualControlCreated then
    FKeyField := TrwDBComboBoxControl(VisualControl.RealObject).LookupField;
  Result := FKeyField;  
end;

function TrwDBComboBox.GetParamValue: Variant;
begin
  if VisualControlCreated then
    if TrwDBComboBoxControl(VisualControl.RealObject).Grayed then
      FParamValue := Null
    else
      if Assigned(TrwDBComboBoxControl(VisualControl.RealObject).LookupTable) and
         TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.Active and
         (KeyField <> '') then
        FParamValue := TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue;

  Result := FParamValue;    
end;

function TrwDBComboBox.GetText: string;
begin
  if VisualControlCreated then
    FText := TrwDBComboBoxControl(VisualControl.RealObject).Text;
  Result := FText;
end;

function TrwDBComboBox.GetValue: String;
var
  V: Variant;
begin
  V := GetParamValue;
  if VarIsNull(V) then
    Result := ''
  else
    Result := V;
end;

procedure TrwDBComboBox.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnChange]);
end;


procedure TrwDBComboBox.RWNotification(Sender: TrwComponent;
  AOperation: TrwNotifyOperation);
begin
  inherited;

  if Sender = FDataSource then
    if AOperation = rnoDestroy then
      DataSource := nil

    else if AOperation = rnoDataSetChanged then
    begin
      if not varIsEmpty(FParamValue) then
        GetParamValue;
    end;
end;

procedure TrwDBComboBox.SetAllowClearKey(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwDBComboBoxControl(VisualControl.RealObject).AllowClearKey := Value;
    GetAllowClearKey;
  end
  else
    FAllowClearKey := Value;
end;

procedure TrwDBComboBox.SetDataSource(const Value: TrwDataSet);
begin
  if FDataSource = Value then
    Exit;

  if Assigned(FDataSource) then
    FDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FDataSource := Value;

  if VisualControlCreated then
    if Assigned(Value) then
      TrwDBComboBoxControl(VisualControl.RealObject).LookupTable := Value.VCLDataSet
    else
      TrwDBComboBoxControl(VisualControl.RealObject).LookupTable := nil;
end;

procedure TrwDBComboBox.SetDisplayFields(const Value: string);
begin
  if VisualControlCreated then
    TrwDBComboBoxControl(VisualControl.RealObject).SetDisplayFields(Value);
  FDisplayFields := Value;
end;

procedure TrwDBComboBox.SetKeyField(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwDBComboBoxControl(VisualControl.RealObject).LookupField := Value;
    GetKeyField;
  end
  else
    FKeyField := Value;
end;

procedure TrwDBComboBox.SetParamValue(Value: Variant);

  procedure CheckEmptyValue;
  begin
    if not AllowClearKey and (TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue <> TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.FieldByName(KeyField).AsString) and
       (TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.RecordCount > 0) then
    begin
      TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.First;
      TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue := TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.FieldByName(KeyField).AsString;
      FParamValue := TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue;
    end;
  end;

begin
  FParamValue := Value;
  
  if VisualControlCreated and
     Assigned(TrwDBComboBoxControl(VisualControl.RealObject).LookupTable) and
     TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.Active and
     (Length(KeyField) > 0) then
  begin
    if VarIsNull(FParamValue) then
      TrwDBComboBoxControl(VisualControl.RealObject).Grayed := True

    else if VarToStr(FParamValue) = '' then
    begin
      TrwDBComboBoxControl(VisualControl.RealObject).Clear;
      TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue := '';
      CheckEmptyValue;
    end

    else
    begin
      TrwDBComboBoxControl(VisualControl.RealObject).LookUpValue := FParamValue;
      TrwDBComboBoxControl(VisualControl.RealObject).LookupTable.Locate(KeyField, FParamValue, []);
      CheckEmptyValue;
    end;
  end;
end;

procedure TrwDBComboBox.SetText(const Value: string);
begin
  if VisualControlCreated then
  begin
    TrwDBComboBoxControl(VisualControl.RealObject).Text := Value;
    GetText;
  end
  else
    FText := Value;
end;

procedure TrwDBComboBox.SetValue(const AValue: String);
begin
  SetParamValue(AValue);
end;


function TrwDBComboBox.StoreValue: Boolean;
begin
  Result := not (csWriting in ComponentState);
end;

{ TrwPageControl }

constructor TrwPageControl.Create(AOwner: TComponent);
begin
  inherited;
  FDoNotFireEvents := False;
  TabStop := True;
  ActivePageIndex := -1;
  Width := 289;
  Height := 193;
end;

function TrwPageControl.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwPageCtrlControl.Create(Self);
  TrwPageCtrlControl(Result.RealObject).OnChange := FOnChange;
  TrwPageCtrlControl(Result.RealObject).OnChanging := FOnChanging;
  Result.PropChanged(rwCPCreation);
end;

procedure TrwPageControl.FOnChange(Sender: TObject);
begin
  if not FDoNotFireEvents then
    ExecuteEventsHandler('OnChange', []);
end;


procedure TrwPageControl.FOnChanging(Sender: TObject; var AllowChange: Boolean);
var
  V: Variant;
begin
  if not FDoNotFireEvents then
  begin
    V := AllowChange;
    ExecuteEventsHandler('OnChanging', [Integer(Addr(V))]);
    AllowChange := V;
  end;  
end;


function TrwPageControl.GetActivePage: TrwTabSheet;
begin
  if VisualControlCreated and (ActivePageIndex <> -1) then
    Result := TrwTabSheet((TrwPageCtrlControl(VisualControl.RealObject).ActivePage as IrwVisualControl).RWObject)
  else
    Result := nil;  
end;

function TrwPageControl.GetActivePageIndex: Integer;
begin
  if VisualControlCreated then
    FActivePageIndex := TrwPageCtrlControl(VisualControl.RealObject).ActivePageIndex;
  Result := FActivePageIndex;
end;

function TrwPageControl.PageByIndex(AIndex: Variant): Variant;
var
  I: IrwVisualControl;
begin
  if VisualControlCreated then
  begin
    I := TrwTabSheetControl(TrwPageCtrlControl(VisualControl.RealObject).Pages[AIndex]);
    Result := Integer(Pointer(I.RealObject));
  end
  else
    Result := 0;  
end;


function TrwPageControl.PageCount: Variant;
begin
  if VisualControlCreated then
    Result := TrwPageCtrlControl(VisualControl.RealObject).PageCount
  else
    Result := 0;
end;


procedure TrwPageControl.RegisterComponentEvents;
begin
  inherited;
  UnRegisterEvents([cEventOnClick]);
  RegisterEvents([cEventOnChange, cEventOnChanging]);
end;

procedure TrwPageControl.SetActivePage(const Value: TrwTabSheet);
begin
  if VisualControlCreated then
    if not Assigned(Value) then
      TrwPageCtrlControl(VisualControl.RealObject).ActivePage := nil
    else
      TrwPageCtrlControl(VisualControl.RealObject).ActivePage := TrwTabSheetControl(Value.VisualControl.RealObject);
end;

procedure TrwPageControl.SetActivePageIndex(const Value: Integer);
begin
  if VisualControlCreated then
  begin
    TrwPageCtrlControl(VisualControl.RealObject).ActivePageIndex := Value;
    GetActivePageIndex;
  end
  else
    FActivePageIndex := Value;
end;


procedure TrwPageControl.SetDesignParent(Value: TWinControl);
var
  ap: Integer;
begin
  FDoNotFireEvents := True;
  try
    ap := ActivePageIndex;
    inherited;
    ActivePageIndex := -1;
  finally
    FDoNotFireEvents := False;  
  end;
  ActivePageIndex := ap;
end;

{ TrwTabSheet }

constructor TrwTabSheet.Create(AOwner: TComponent);
begin
  inherited;
  TabVisible := True;
end;

function TrwTabSheet.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwTabSheetControl.Create(Self);
  TrwTabSheetControl(Result.RealObject).OnShow := FOnShow;
  TrwTabSheetControl(Result.RealObject).OnHide := FOnHide;
  Result.PropChanged(rwCPCreation);
  TrwTabSheetControl(Result.RealObject).Hide;
end;

procedure TrwTabSheet.FOnHide(Sender: TObject);
begin
  if Assigned(Parent) and not TrwPageControl(Parent).FDoNotFireEvents then
    ExecuteEventsHandler('OnHide', []);
end;

procedure TrwTabSheet.FOnShow(Sender: TObject);
begin
  if Assigned(Parent) and not TrwPageControl(Parent).FDoNotFireEvents then
    ExecuteEventsHandler('OnShow', []);
end;

function TrwTabSheet.GetTabVisible: Boolean;
begin
  if VisualControlCreated then
    FTabVisible := TrwTabSheetControl(VisualControl.RealObject).TabVisible;
  Result := FTabVisible;
end;

function TrwTabSheet.GetText: String;
begin
  if VisualControlCreated then
    FText := TrwTabSheetControl(VisualControl.RealObject).Caption;
  Result := FText;
end;

procedure TrwTabSheet.HideRT;
begin
  inherited;
  TabVisible := False;
end;

procedure TrwTabSheet.InitializeForDesign;
begin
  inherited;
  Caption := Name;
end;

procedure TrwTabSheet.RegisterComponentEvents;
begin
  inherited;
  UnRegisterEvents([cEventOnClick]);
  RegisterEvents([cEventOnShow, cEventOnHide]);  
end;

procedure TrwTabSheet.SetTabVisible(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwTabSheetControl(VisualControl.RealObject).TabVisible := Value;
    GetTabVisible;
  end
  else
    FTabVisible := Value;
end;

procedure TrwTabSheet.SetText(const Value: String);
begin
  if VisualControlCreated then
  begin
    TrwTabSheetControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    FText := Value;
end;


{ TrwButton }

constructor TrwButton.Create(AOwner: TComponent);
begin
  inherited;
  TabStop := False;
  Width := 75;
  Height := 25;
end;

function TrwButton.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwButtonControl.Create(Self);
  TrwButtonControl(Result.RealObject).OnClick := FOnClick;
  Result.PropChanged(rwCPCreation);  
end;

function TrwButton.GetText: String;
begin
  if VisualControlCreated then
    FText := TrwButtonControl(VisualControl.RealObject).Caption;
  Result := FText;  
end;

procedure TrwButton.InitializeForDesign;
begin
  inherited;
  Caption := Name;
end;

procedure TrwButton.SetText(const Value: String);
begin
  if VisualControlCreated then
  begin
    TrwButtonControl(VisualControl.RealObject).Caption := Value;
    GetText;
  end
  else
    FText := Value;
end;


{ TrwFileDialog }

constructor TrwFileDialog.Create(AOwner: TComponent);
begin
  inherited;
  DialogType := rfdOpenFile;
  Options := [ofHideReadOnly, ofEnableSizing];
  Filter := 'All Files(*.*)|*.*';
  FilterIndex := 1;
  ReadOnly := False;
  TabStop := False;
  Width := 186;
  Height := 21;
end;

function TrwFileDialog.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFileDialogControl.Create(Self);
  TrwFileDialogControl(Result.RealObject).OnClick := FOnClick;
  TrwFileDialogControl(Result.RealObject).OnChange := FOnChange;
  TrwFileDialogControl(Result.RealObject).OnCustomDlg := FOnCustomDlg;
  Result.PropChanged(rwCPCreation);  
end;

function TrwFileDialog.GetParamValue: Variant;
begin
  if VisualControlCreated then
    FParamValue := TrwFileDialogControl(VisualControl.RealObject).Text;

  Result := FParamValue;
end;

procedure TrwFileDialog.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnChange]);
end;

procedure TrwFileDialog.SetParamValue(Value: Variant);
begin
  if VisualControlCreated then
  begin
    TrwFileDialogControl(VisualControl.RealObject).Text := Value;
    GetParamValue;
  end
  else
    FParamValue := Value;
end;

procedure TrwFileDialog.FOnChange(Sender: TObject);
begin
  ExecuteEventsHandler('OnChange', []);
end;

procedure TrwFileDialog.SetDialogType(const AValue: TrwFileDialogType);
begin
  if VisualControlCreated then
  begin
    TrwFileDialogControl(VisualControl.RealObject).DialogType := AValue;
    GetDialogType;
  end
  else
    FDialogType := AValue;
end;


procedure TrwFileDialog.FOnCustomDlg(Sender: TObject);
begin
  PShowDialog;
end;

function TrwFileDialog.GetReadOnly: Boolean;
begin
  if VisualControlCreated then
    FReadOnly := TrwFileDialogControl(VisualControl.RealObject).ReadOnly;
  Result := FReadOnly;  
end;

procedure TrwFileDialog.SetReadOnly(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    TrwFileDialogControl(VisualControl.RealObject).ReadOnly := Value;
    GetReadOnly;
  end
  else
    FReadOnly := Value;  
end;

function TrwFileDialog.GetFileName: string;
begin
  Result := VarToStr(ParamValue);
end;

procedure TrwFileDialog.SetFileName(const AValue: string);
begin
  ParamValue := AValue;
end;

function TrwFileDialog.PShowDialog: Variant;
begin
  if VisualControlCreated then
    Result := TrwFileDialogControl(VisualControl.RealObject).ShowDialog
  else
    Result := False;  
end;

function TrwFileDialog.GetDialogType: TrwFileDialogType;
begin
  if VisualControlCreated then
    FDialogType := TrwFileDialogControl(VisualControl.RealObject).DialogType;
  Result := FDialogType;  
end;

end.
