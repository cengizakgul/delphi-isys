unit rwDDTableEditFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DataDictTableEditorFrm, rwDDTableEditorFrm, rwDataDictionary;

type
  TrwDDTableEdit = class(TForm)
    ddTableEditor: TrwDDTableEditor;
  private
  public
  end;

function EditDDTable(const ATable: TrwDataDictTable): Boolean;

implementation

{$R *.dfm}

uses rwQueryBuilderFrm, rwUtils;


function EditDDTable(const ATable: TrwDataDictTable): Boolean;
begin
  with TrwDDTableEdit.Create(FQBFrm) do
    try
      ddTableEditor.DataDictionary := DataDictionary;
      ddTableEditor.ShowTable(ATable);
      Result := ShowModal = mrOK;

    finally
      Free;
    end;
end;


end.
 