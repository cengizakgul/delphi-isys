object rwQBTable: TrwQBTable
  Left = 494
  Top = 108
  Width = 228
  Height = 332
  ActiveControl = lvFields
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Table'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = pmTable
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnTblName: TPanel
    Left = 0
    Top = 0
    Width = 220
    Height = 20
    Align = alTop
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    BorderStyle = bsSingle
    Color = 3839285
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 0
    OnDblClick = pnTblNameDblClick
    OnDragOver = pnTblNameDragOver
    OnMouseDown = pnTblNameMouseDown
    OnMouseMove = pnTblNameMouseMove
    OnMouseUp = pnTblNameMouseUp
    object lAlias: TLabel
      Left = 186
      Top = 0
      Width = 30
      Height = 16
      Align = alRight
      Caption = 'lAlias'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Transparent = True
      OnDblClick = pnTblNameDblClick
      OnDragOver = pnTblNameDragOver
      OnMouseDown = pnTblNameMouseDown
      OnMouseMove = pnTblNameMouseMove
      OnMouseUp = pnTblNameMouseUp
    end
    object lTable: TLabel
      Left = 0
      Top = 0
      Width = 186
      Height = 16
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 'lName'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Transparent = True
      OnDblClick = pnTblNameDblClick
      OnDragOver = pnTblNameDragOver
      OnMouseDown = pnTblNameMouseDown
      OnMouseMove = pnTblNameMouseMove
      OnMouseUp = pnTblNameMouseUp
    end
  end
  object pnContents: TPanel
    Left = 0
    Top = 20
    Width = 220
    Height = 285
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter: TSplitter
      Left = 0
      Top = 262
      Width = 220
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      MinSize = 25
      OnMoved = SplitterMoved
    end
    object lvFields: TListView
      Left = 0
      Top = 0
      Width = 220
      Height = 262
      Align = alClient
      Columns = <
        item
          AutoSize = True
          Caption = 'Field'
        end
        item
          Caption = 'Type'
          Width = 58
        end>
      DragMode = dmAutomatic
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      LargeImages = rwQueryBuilder.ilPict
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      ShowColumnHeaders = False
      SmallImages = rwQueryBuilder.ilPict
      TabOrder = 0
      ViewStyle = vsReport
      OnDblClick = lvFieldsDblClick
      OnEndDrag = lvFieldsEndDrag
      OnDragOver = lvFieldsDragOver
      OnMouseMove = lvFieldsMouseMove
      OnSelectItem = lvFieldsSelectItem
      OnStartDrag = lvFieldsStartDrag
    end
    object tvFilter: TTreeView
      Left = 0
      Top = 265
      Width = 220
      Height = 20
      Align = alBottom
      DragMode = dmAutomatic
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Images = rwQueryBuilder.ilPict
      Indent = 19
      ParentFont = False
      PopupMenu = pmFilter
      ReadOnly = True
      TabOrder = 1
      OnCustomDrawItem = tvFilterCustomDrawItem
      OnDblClick = tvFilterDblClick
      OnDragOver = tvFilterDragOver
      OnEndDrag = tvFilterEndDrag
      OnMouseMove = tvFilterMouseMove
    end
  end
  object pmTable: TPopupMenu
    OnPopup = pmTablePopup
    Left = 112
    Top = 88
    object miAlias: TMenuItem
      Caption = 'Table Alias...'
      OnClick = miAliasClick
    end
    object miParams: TMenuItem
      Caption = 'Parameters...'
      OnClick = miParamsClick
    end
    object miIsolatedFiltering: TMenuItem
      AutoCheck = True
      Caption = 'Isolate Filtering from Outer Join'
      OnClick = miIsolatedFilteringClick
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object miSCTable: TMenuItem
      Caption = 'Show/Close Table'
      OnClick = miSCTableClick
    end
    object miShowSubQuery: TMenuItem
      Caption = 'Show SubQuery'
      OnClick = miShowSubQueryClick
    end
    object Delete1: TMenuItem
      Caption = 'Remove Table'
      OnClick = Delete1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object miCopy: TMenuItem
      Caption = 'Copy'
      ShortCut = 16451
      OnClick = miCopyClick
    end
  end
  object pmFilter: TPopupMenu
    Left = 112
    Top = 128
    object miAddCond: TMenuItem
      Caption = 'Add Condition...'
      ShortCut = 45
      OnClick = miAddCondClick
    end
    object miAddAnd: TMenuItem
      Caption = 'Add AND'
      OnClick = miAddAndClick
    end
    object miAddOr: TMenuItem
      Caption = 'Add OR'
      OnClick = miAddOrClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miEditNode: TMenuItem
      Caption = 'Edit...'
      ShortCut = 13
      OnClick = miEditNodeClick
    end
    object miDeleteNode: TMenuItem
      Caption = 'Delete'
      ShortCut = 46
      OnClick = miDeleteNodeClick
    end
  end
end
