unit rmXMLFormControls;

interface

uses Classes, Controls, Types, Windows, Graphics, SysUtils, Messages, Forms, Math, Variants, StdCtrls,
     rwEngineTypes, rwGraphics, rwBasicClasses, rwTypes, rmTypes, rmCommonClasses,
     rwDB, rwQBInfo, rmFormula, rwUtils, rwBasicUtils, rwRTTI, rmReport, EvStreamUtils,
     MSXML2_TLB;

type
  TrmXMLRenderingStatus = (rmXMLRSNone, rmXMLRSCalculating, rmXMLRSCalculated);

  TrmXMLNode = class;
  TrmXMLForm = class;

  // Abstract XML Schema Item

  TrmXMLItem = class(TrmGraphicControl, IrmXMLItem)
  private
    FReadItemIndex: Integer;
    FEnabled: Boolean;
    FRealigning: Boolean;
    FIsForm: Boolean;

    function  GetItemIndex: Integer;
    procedure SetItemIndex(const AValue: Integer);
    procedure SetEnabled(const Value: Boolean);

  protected
    procedure SetVisible(Value: Boolean); override;
    function  GetItemLevel: Integer; virtual;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure AssignScreenPosition;
    procedure DoAssignScreenPosition; virtual; abstract;
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); virtual;
    function  TreeBranchRect: TRect; virtual;
    function  XMLForm: TrmXMLForm;
    procedure SetDescriptionFont(AFont: TrwFont); virtual;
    procedure DrawItemText(const ACanvas: TrwCanvas; const AText: String);
    procedure RegisterComponentEvents; override;
    function  ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
    function  ZoomedClientRect(ACanvas: TrwCanvas): TRect;
    function  MouseResizingThreshold: Integer; override;
    function  CalcTextSize(const AText: String): TRect;
    procedure RefreshParent;
    procedure UnselectIfSelected; virtual;

  //Interface
    function  GetWinControlParent: TWinControl; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  GetParentExtRect: TRect; override;
    function  GetParent: IrmDesignObject; override;
    procedure SetParent(AParent: IrmDesignObject); override;
    function  ObjectType: TrwDsgnObjectType; override;
    function  ClientToScreen(APos: TPoint): TPoint; override;
    function  ScreenToClient(APos: TPoint): TPoint; override;
    function  GetXMLForm: IrmDesignObject;
  //end

  //rendering
  private
    FRendStatus: TrmXMLRenderingStatus;
    FXMLDocNode: TxmlDocNodeHandle;
    procedure   SetRendStatus(const Value: TrmXMLRenderingStatus); virtual;
    function    FormulaBlockBeforeCalc: TrmFMLFormula;
    function    FormulaBlockAfterCalc: TrmFMLFormula;
    function    GetXMLDocNode: TxmlDocNodeHandle; virtual;
    procedure   SetXMLDocNode(AValue: TxmlDocNodeHandle); virtual;
  protected
    function    Renderer: IrmXMLRenderEngine; virtual;
    procedure   CalcSetPropFormulas;
    function    BeforeCalculate: Boolean; virtual;
    procedure   DoCalculate; virtual;
    function    AfterCalculate: Boolean; virtual;
    procedure   EraseItemData; virtual;
  public
    procedure   Calculate;
    property    RendStatus: TrmXMLRenderingStatus read FRendStatus write SetRendStatus;
    property    XMLDocNode: TxmlDocNodeHandle read GetXMLDocNode write SetXMLDocNode;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    Parent: TrmXMLNode; virtual;
    function    ClientToForm(APoint: TPoint): TPoint; overload;
    function    FormToClient(APoint: TPoint): TPoint; overload;
    function    ClientToForm(ARect: TRect): TRect; overload;
    function    FormToClient(ARect: TRect): TRect; overload;
    function    CanvasDPI: Integer; override;
    function    DoNotPaint: Boolean; override;

  published
    property Left stored False;
    property Top stored False;
    property Width stored False;
    property Height stored False;
    property Visible stored False;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
  end;


  TrmdXMLNodeButton = class;

  TrmXMLNode = class(TrmXMLItem)
  private
    FItems: TList;
    FChidItemsOffset: TPoint;
    FBlockParentIfEmpty: Boolean;
    FXMLNodeTreeButton: TrmdXMLNodeButton;
    FNodeClosed: Boolean;

    function  GetItem(Index: Integer): TrmXMLItem;
    procedure AddItem(AItem: TrmXMLItem);
    procedure DeleteItem(AItem: TrmXMLItem);
    procedure ResortItems; virtual;
    procedure SetBlockParentIfEmpty(const Value: Boolean);

    procedure SyncTreeButton;
    function  IsTreeButtonVisible: Boolean; virtual;
    procedure CloseNode;
    procedure OpenNode;

  protected
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure RWLoaded; override;
    procedure PaintForeground(ACanvas: TrwCanvas); virtual;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure PaintChildren(ACanvas: TrwCanvas); virtual;
    procedure PaintTreeLines(ACanvas: TrwCanvas); virtual;
    procedure DoAssignScreenPosition; override;
    procedure Invalidate; override;
    procedure Refresh; override;
    function  TreeBranchRect: TRect; override;
    procedure Notify(AEvent: TrmDesignEventType); override;
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); override;
    procedure UnselectIfSelected; override;

    procedure SyncTagPos; override;

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  GetBoundsRect: TRect; override;
    procedure SetBoundsRect(ARect: TRect); override;
    procedure AddChildObject(AObject: IrmDesignObject); override;
    procedure RemoveChildObject(AObject: IrmDesignObject); override;
  public
    function  ControlAtPos(APos: TPoint): TrwComponent; override;
  // End

  // rendering
  protected
    procedure  DoCalculate; override;
  public
    procedure  SetRendStatusForChildren(AStatus: TrmXMLRenderingStatus);
    procedure  SetRendStatusChildrenToo(AStatus: TrmXMLRenderingStatus);
 // end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    ItemCount: Integer;
    procedure   PaintToCanvas(ACanvas: TrwCanvas); override;
    function    ItemIndexOf(const AItem: TrmXMLItem): Integer;

    property    Items[Index: Integer]: TrmXMLItem read GetItem;
    property    BlockParentIfEmpty: Boolean read FBlockParentIfEmpty write SetBlockParentIfEmpty;
  end;



  TrmdXMLNodeButton = class(TrmdCustomDesignObject)
  protected
    procedure PaintToCanvas(ACanvas: TrwCanvas); override;
    function  GetShapeRegion: HRGN; override;
  public
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  end;


  // =================== UR Types ========================


  // anySimpleType
  TrmXMLSimpleType = class(TrmXMLNode)
  protected
    function  GetValue: Variant; virtual;
    procedure SetValue(const AValue: Variant); virtual;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure DoAssignScreenPosition; override;
    function  GetText: string; virtual;
    procedure DoOnFilterText(var AText: String);
    procedure DoOnChangeValue(var ANewValue: Variant);
    procedure RegisterComponentEvents; override;

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  // end

  public
    property Value: Variant read GetValue write SetValue stored False;
  end;


  TrmXMLSimpleTypeByRestriction = class(TrmXMLSimpleType, IrmQueryAwareObject)
  private
    FValueHolder: TrmsValueHolder;
    function  CanWriteValue: Boolean;
    function  GetDataField: String;
    procedure SetDataField(const AValue: String);
    function  FormulaValue: TrmFMLFormula;
    function  FormulaTextFilter: TrmFMLFormula;
  protected
    function  PostprocessText(const AText: String): String;
    function  ApplyValueRestrictions(const AValue: Variant): Variant; virtual;
    function  GetValue: Variant; override;
    procedure SetValue(const AValue: Variant); override;
    procedure DoAssignScreenPosition; override;
    function  GetShowText: String; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    function  GetText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  GetFieldName: String;
    procedure SetFieldName(const AFieldName: String);
    function  GetDataFieldInfo: TrmDataFieldInfoRec;
    procedure SetValueFromDataSource;
  // End

  //rendering
  protected
    function    BeforeCalculate: Boolean; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property  Value stored CanWriteValue;
    property  DataField: String read GetDataField write SetDataField;
  end;



  TrmXMLFragmentValue = class(TrmXMLSimpleTypeByRestriction, IrmQueryAwareObject)
  private
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  public
  end;


  TrmXMLItemizedSimpleType = class(TrmXMLSimpleType)
  protected
    procedure DoAssignScreenPosition; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure SetValue(const AValue: Variant); override;

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  // end
  end;


  TrmXMLSimpleTypeByList = class(TrmXMLItemizedSimpleType)
  private
    FValList: Variant;
    FText: String;
    function  SimpleContent: TrmXMLSimpleType;
    function  CanWriteValue: Boolean;
    function  FormulaValue: TrmFMLFormula;
  protected
    procedure SetValue(const AValue: Variant); override;
    function  GetValue: Variant; override;
    function  GetText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  // rendering
  protected
    function  BeforeCalculate: Boolean; override;
    procedure DoCalculate; override;
  // end

  public
    constructor Create(AOwner: TComponent); override;

  published
    property  Value stored CanWriteValue;
  end;


  TrmXMLSimpleTypeByUnion = class(TrmXMLItemizedSimpleType)
  protected
    function  GetValue: Variant; override;
    function  GetText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  end;


  TrmXMLanyAttribute = class(TrmXMLNode)
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  end;


  TrmXMLattribute = class(TrmXMLanyAttribute)
  private
    FNSName: String;
    function  IsSimpleContent: Boolean;
    function  SimpleContent: TrmXMLSimpleType;
    procedure SetNSName(const AValue: String);
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
  protected
    procedure DoAssignScreenPosition; override;
    function  GetShowText: String; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  // rendering
  protected
    procedure DoCalculate; override;
  // end

  public
    constructor Create(AOwner: TComponent); override;
  published
    property NSName: String read FNSName write SetNSName;
    property Value: Variant read GetValue write SetValue stored False;
  end;


  TrmXMLattributeGroup = class(TrmXMLanyAttribute)
  protected
    function  GetXMLDocNode: TxmlDocNodeHandle; override;
    procedure SetXMLDocNode(AValue: TxmlDocNodeHandle); override;
    procedure DoAssignScreenPosition; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  end;


  TrmXMLanyElement = class(TrmXMLNode)
  private
    function  IsTreeButtonVisible: Boolean; override;
    procedure WriteNodeClosed(Writer: TWriter);
    procedure ReadNodeClosed(Reader: TReader);

  protected
    procedure DoAssignScreenPosition; override;
    procedure RegisterComponentEvents; override;

  protected
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); override;
    procedure DefineProperties(Filer: TFiler); override;    

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  // End

  //rendering
  private
    FBranchBegDocPosition: Integer;
    FLastDocPosition: Integer;

    function  FormulaBlockBeforeBranchCalc: TrmFMLFormula;
    function  FormulaBlockAfterBranchCalc: TrmFMLFormula;
  protected
    function  BeforeCalculate: Boolean; override;
    function  BeforeCalculateBranch: Boolean; virtual;
    procedure DoCalculateBranch; virtual;
    function  AfterCalculateBranch: Boolean; virtual;
    procedure EraseItemData; override;
    procedure EraseBranchData;
  public
    procedure CalculateBranch;
  //end

  published
    property BlockParentIfEmpty;
  end;

  TrmXMLgroup = class(TrmXMLanyElement)
  private
    function  GetXMLDocNode: TxmlDocNodeHandle; override;
    procedure SetXMLDocNode(AValue: TxmlDocNodeHandle); override;
  protected
    procedure PaintControl(ACanvas: TrwCanvas); override;
    function  GetShowText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  end;


  TrmXMLchoice = class(TrmXMLanyElement)
  private
    function  GetXMLDocNode: TxmlDocNodeHandle; override;
    procedure SetXMLDocNode(AValue: TxmlDocNodeHandle); override;
  protected
    procedure PaintControl(ACanvas: TrwCanvas); override;
    function  GetShowText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  //rendering
  protected
    procedure DoCalculate; override;
  //end

  end;


  TrmXMLQueryElement = class;

  TrmXMLQuery = class(TrwQuery)
  private
    FNode: TrmXMLQueryElement;
  protected
    procedure OnAssignQBInfo(Sender: TObject); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    property Node: TrmXMLQueryElement read FNode;
  end;


  TrmXMLQueryElement = class(TrmXMLanyElement, IrmQueryDrivenObject)
  private
    FQuery: TrmXMLQuery;
    function  GetQueryBuilderInfo: TrwQBQuery;
    procedure SetQueryBuilderInfo(const Value: TrwQBQuery);
    function  GetParamsFromVars: Boolean;
    procedure SetParamsFromVars(const Value: Boolean);
    function  GetMasterFields: TrwStrings;
    procedure SetMasterFields(const Value: TrwStrings);
    procedure SyncMasterQueryLink;
    function  GetDataSource: TrwDataSet;

  protected
    procedure RWLoaded; override;

  // Interface
  protected
    function  GetQuery: IrmQuery;
  // End

  //rendering
  private
    FQueryOpenedInternally: Boolean;

  protected
    function  BeforeCalculateBranch: Boolean; override;
    procedure DoCalculateBranch; override;
    function  AfterCalculateBranch: Boolean; override;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property  QueryBuilderInfo: TrwQBQuery read GetQueryBuilderInfo write SetQueryBuilderInfo;
    property  ParamsFromVars: Boolean read GetParamsFromVars write SetParamsFromVars stored DontStoreProperty;
    property  MasterFields: TrwStrings read GetMasterFields write SetMasterFields;
    property  DataSource: TrwDataSet read GetDataSource;
  end;


  TrmXMLsequence = class(TrmXMLQueryElement)
  private
    function  GetXMLDocNode: TxmlDocNodeHandle; override;
    procedure SetXMLDocNode(AValue: TxmlDocNodeHandle); override;
  protected
    procedure PaintControl(ACanvas: TrwCanvas); override;
    function  GetShowText: String; override;

  // Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  // End

  end;



  TrmXMLelement = class(TrmXMLQueryElement)
  private
    FNSName: String;
    FFirstAttributeIndex: Integer;
    FLastAttributeIndex: Integer;
    procedure SetNSName(const AValue: String); virtual;
    procedure ResortItems; override;
    function  IsSimpleContent: Boolean;
    function  IsComplexContent: Boolean;
    function  SimpleContent: TrmXMLSimpleType;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
  protected
    procedure DoAssignScreenPosition; override;
    function  GetShowText: String; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure PaintTreeLines(ACanvas: TrwCanvas); override;

  //Interface
  protected
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
  //end

  // rendering
  protected
    procedure DoCalculate; override;
  public

  public
    constructor Create(AOwner: TComponent); override;

  published
    property NSName: String read FNSName write SetNSName;
    property Value: Variant read GetValue write SetValue stored False;
  end;

  TrmXMLfragment = class(TrmXMLelement)
  private
    procedure SetNSName(const AValue: String); override;
  protected
    procedure DoCalculate; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
  public
    procedure AfterConstruction; override;
    procedure InitializeForDesign; override;
  end;


  // Storage for XML schemas (last item represents root schema, others are basically "includes")
  TrmXMLSchema = class(TrwCollectionItem, IrmXMLSchema)
  private
    FName: String;
    FContent: String;
    FSchemaFileName: String;
    FDescription: String;
  protected
    function  AsAncestor(const AncestorItem: TrwCollectionItem): Boolean; override;

  // interface
  protected
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetDescription: string;
    procedure SetDescription(const AValue: string);
  // end

  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromFile(const AFileName: TFileName);
    procedure SaveToFile(const AFileName: TFileName);
    procedure CreateTmpFile;
    procedure DeleteTmpFile;
    property  SchemaFileName: String read FSchemaFileName;
  published
    property Name: String read FName write FName;
    property Description: String read FDescription write FDescription;
    property Content: String read FContent write FContent;
  end;


  TrmXMLSchemas = class(TrwCollection, IrmXMLSchemas)
  private
    function GetItem(Index: Integer): TrmXMLSchema;

  // interface
  protected
    function GetSchemaByName(const ASchemaName: String): IrmXMLSchema;
  // end

  public
    function  FindSchema(const AName: String): TrmXMLSchema;
    procedure BeginWorkWithItems;
    procedure EndWorkWithItems;
    property Items[Index: Integer]: TrmXMLSchema read GetItem; default;
  end;

  
  // XML Schema per se

  TrmXMLForm = class(TrmXMLNode, IrmXMLFormObject)
  private
    FNameSpace: String;
    FSchemas: TrmXMLSchemas;
    FSchemasRW: TrmXMLSchemas;  // streaming only
    FDefaultFileName: String;
    FNameSpaceAlias: String;
    procedure ReadSchemas(Reader: TReader);
    procedure WriteSchemas(Writer: TWriter);

  protected
    procedure DefineProperties(Filer: TFiler); override;
    procedure SetParentComponent(Value: TComponent); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure PaintChildren(ACanvas: TrwCanvas); override;
    procedure Invalidate; override;
    procedure DoAssignScreenPosition; override;
    function  GetItemLevel: Integer; override;
    procedure Refresh; override;
    function  TreeBranchRect: TRect; override;
    procedure SyncTagPos; override;

  //Interface
  protected
    procedure SetDesigner(ADesigner: IrmDesigner); override;
    procedure Notify(AEvent: TrmDesignEventType); override;
    procedure LoadXSDSchema(const AFileName: TFileName);
    procedure SaveXSDSchema(const ASchemaName: String; const AFileName: TFileName);
    procedure UpdateXSDSchema(const ASchemaName: String; const AFileName: TFileName);
    procedure AddXSDSchema(const AFileName: TFileName);
    function  GetSchemas: IrmXMLSchemas;
  //End

  // Rendering
  protected
    function  Renderer: IrmXMLRenderEngine; override;
    procedure DoCalculate; override;
  public
    function  TopElement: TrmXMLelement;
  // End

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   InitializeForDesign; override;
    function    Parent: TrmXMLNode; override;

    property    Schemas: TrmXMLSchemas read FSchemas;

  published
    property  ItemIndex stored False;
    property  NameSpace: String read FNameSpace write FNameSpace;
    property  NameSpaceAlias: String read FNameSpaceAlias write FNameSpaceAlias;
    property  DefaultFileName: String read FDefaultFileName write FDefaultFileName;
  end;



  TrmdXMLForm = class(TrmdCustomGraphicWorkSpace)
  private
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
  protected
    procedure PropChanged(APropType: TrwChangedPropID); override;
    procedure UpdateSize; override;
    function  DispatchMessage(var Message: TMessage): Boolean; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses rmXMLxsdSchemaParser;

const
  cLevelOffset = 50;
  cFirstLevelOffset = 20;
  cItemHeight = 50;
  cItemLeftOffset = 10;
  cXMLSimpleTypeColor =  $00CEE7FF;
  cXMLAttributeColor =   $00F0FFEC;
  cXMLElementColor =     clYellow;


{ TrmXMLanyType }

function TrmXMLItem.AfterCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockAfterCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('AfterCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
  begin
    RendStatus := rmXMLRSCalculated;
    NotifyAggregateFunctions;
  end
  else
  begin
    EraseItemData;
    RendStatus := rmXMLRSNone;
  end;
end;

procedure TrmXMLItem.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
begin
end;

procedure TrmXMLItem.AssignScreenPosition;
begin
  if IsDesignMode then
  begin
    FRealigning := True;
    try
      DoAssignScreenPosition;
    finally
      FRealigning := False;
      SyncTagPos;
    end;
  end;
end;

function TrmXMLItem.BeforeCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockBeforeCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('BeforeCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
    RendStatus := rmXMLRSCalculating
  else
    RendStatus := rmXMLRSNone;
end;

procedure TrmXMLItem.CalcSetPropFormulas;
var
  i: Integer;
  v: Variant;
begin
  for i := 0 to Formulas.Count - 1 do
    if Formulas[i].ActionType = fatSetProperty then
    begin
      v := Formulas[i].Calculate([]);
      SetPropertyValue(Formulas[i].ActionInfo, v);
    end;
end;

function TrmXMLItem.CalcTextSize(const AText: String): TRect;
var
  Tinf: TrwTextDrawInfoRec;
  Font: TrwFont;
begin
  Font := TrwFont.Create(nil);
  try
    SetDescriptionFont(Font);
    Tinf.CalcOnly := True;
    Tinf.Canvas := nil;
    Tinf.ZoomRatio := 1;
    Tinf.Text := AText;
    Tinf.Font := Font;
    Tinf.DigitNet := '';
    Tinf.WordWrap := False;
    Tinf.Alignment := taLeftJustify;
    Tinf.Layout := tlTop;
    Tinf.TextRect := Rect(0, 0, Zoomed(cItemHeight * 5), Zoomed(cItemHeight));
    DrawTextImageRM(@Tinf);
    Result := UnZoomed(Tinf.TextRect);
    InflateRect(Result, 4, 4);
  finally
    FreeAndNil(Font);
  end;
end;

procedure TrmXMLItem.Calculate;
begin
//  if Parent <> nil then
//    Parent.Calculate;

  if Enabled and (RendStatus = rmXMLRSNone) then
  begin
    if BeforeCalculate then
    begin
      DoCalculate;
      AfterCalculate;
    end;
  end;
end;

function TrmXMLItem.CanvasDPI: Integer;
begin
  Result := cScreenCanvasRes;
end;

function TrmXMLItem.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
begin
  if not FRealigning then
  begin
    if (ALeft <> Left) or (ATop <> Top) then
      ApplyDsgnMoveLogic(ALeft, ATop);

    ALeft := Left;
    ATop := Top;
    AWidth := Width;
    AHeight := Height;
    Result := False;
  end
  else
    Result := True;
end;

function TrmXMLItem.ClientToForm(ARect: TRect): TRect;
begin
  Result.TopLeft := ClientToForm(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmXMLItem.ClientToForm(APoint: TPoint): TPoint;
var
  C: TrmXMLItem;
begin
  Result := APoint;
  Inc(Result.X, Left);
  Inc(Result.Y, Top);
  C := Parent;
  while Assigned(C) do
  begin
    Inc(Result.X, C.Left);
    Inc(Result.Y, C.Top);
    C := C.Parent;
  end;
end;

function TrmXMLItem.ClientToScreen(APos: TPoint): TPoint;
var
  P: TrmdXMLForm;
begin
  P := TrmdXMLForm(GetWinControlParent);
  Result := ClientToForm(APos);
  Inc(Result.X, P.FObjectOffset.X);
  Inc(Result.Y, P.FObjectOffset.Y);
  Result := P.ClientToScreen(Zoomed(Result));
end;

constructor TrmXMLItem.Create(AOwner: TComponent);
begin
  inherited;
  FReadItemIndex := -1;
  FEnabled := True;

  if Parent <> nil then
    Parent.AddItem(Self);
end;

function TrmXMLItem.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  if VisualControlCreated then
    Result := inherited DesignBehaviorInfo
  else
  begin
    Result := inherited DesignBehaviorInfo;
    Result.MovingDirections := [];
    Result.ResizingDirections := [];
    Result.Selectable := {(FMousePosition = rmdMPClient) and} (DsgnLockState = rwLSUnlocked) or Result.Selectable;
  end;
end;

destructor TrmXMLItem.Destroy;
begin
  if (Parent <> nil) and not (csDestroying in Parent.ComponentState) then
    Parent.DeleteItem(Self);

  inherited;
end;

procedure TrmXMLItem.DoCalculate;
begin
  CalcSetPropFormulas;
  ExecuteEventsHandler('OnCalculate', []);
end;

function TrmXMLItem.DoNotPaint: Boolean;
var
  P: TrmXMLNode;
begin
  Result := inherited DoNotPaint;

  if not Result then
  begin
    P := Parent;
    while Assigned(P) do
    begin
      if P.FNodeClosed then
      begin
        Result := True;
        break;
      end;
      P := P.Parent;
    end;
  end;
end;

procedure TrmXMLItem.DrawItemText(const ACanvas: TrwCanvas; const AText: String);
var
  Txt: String;
  R: TRect;
begin
  with ACanvas do
  begin
    SetDescriptionFont(Font);
    Brush.Style := bsClear;
    R := BoundsRect;
    InflateRect(R, -4, -4);
    Inc(R.Top, 10);
    R := Zoomed(R);

    Txt := AText;
    DrawText(GetNextStrValue(Txt, #13), R, DT_TOP	or DT_LEFT or DT_NOPREFIX);

    if (Txt <> '') and (Txt[1] = '[') then
      Font.Style := [fsItalic]

    else if Txt = cErrorTag then
    begin
      Font.Style := [fsBold];
      Font.Color := clRed;
    end
    else
      Font.Style := [];

    Inc(R.Top, -Font.Height);
    DrawText(Txt, R, DT_TOP	or DT_LEFT or DT_NOPREFIX);
  end;
end;

procedure TrmXMLItem.EraseItemData;
begin
end;

function TrmXMLItem.FormToClient(ARect: TRect): TRect;
begin
  Result.TopLeft := FormToClient(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmXMLItem.FormToClient(APoint: TPoint): TPoint;
var
  C: TrmXMLItem;
begin
  Result := APoint;
  Dec(Result.X, Left);
  Dec(Result.Y, Top);
  C := Parent;
  while Assigned(C) do
  begin
    Dec(Result.X, C.Left);
    Dec(Result.Y, C.Top);
    C := C.Parent;
  end;
end;

function TrmXMLItem.FormulaBlockAfterCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockAfterCalc');
end;

function TrmXMLItem.FormulaBlockBeforeCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockBeforeCalc');
end;

function TrmXMLItem.GetItemIndex: Integer;
begin
  Result := -1;

  if Parent <> nil then
    Result := Parent.ItemIndexOf(Self);
end;

function TrmXMLItem.GetItemLevel: Integer;
var
  C: TrwComponent;
begin
  Result := 0;
  C := TrwComponent(Owner);
  while not (C is TrmXMLForm) do
  begin
    C := TrwComponent(C.Owner);
    Inc(Result);
  end;
end;

function TrmXMLItem.GetParent: IrmDesignObject;
begin
  Result := Parent;
end;

function TrmXMLItem.GetParentExtRect: TRect;
var
  P: TrmdXMLForm;
begin
  P := TrmdXMLForm(GetWinControlParent);

  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := ClientToForm(Result.TopLeft);
  Result.BottomRight := ClientToForm(Result.BottomRight);
  OffsetRect(Result, P.FObjectOffset.X, P.FObjectOffset.Y);
  Result := Zoomed(Result);
end;

function TrmXMLItem.GetWinControlParent: TWinControl;
var
  P: TrmXMLForm;
begin
  P := XMLForm;

  if Assigned(P) and P.VisualControlCreated then
    Result := P.VisualControl.Container
  else
    Result := nil;
end;

function TrmXMLItem.GetXMLDocNode: TxmlDocNodeHandle;
begin
  Result := FXMLDocNode;
end;

function TrmXMLItem.GetXMLForm: IrmDesignObject;
begin
  Result := XMLForm;
end;

function TrmXMLItem.MouseResizingThreshold: Integer;
begin
  Result := Round(UnZoomed(inherited MouseResizingThreshold));
end;

procedure TrmXMLItem.Notify(AEvent: TrmDesignEventType);
begin
  inherited;
  if Parent <> nil then
    if AEvent in [rmdSelectObject, rmdSelectChildObject] then
      Parent.Notify(rmdSelectChildObject)
    else if AEvent in [rmdDestroyResizer, rmdDestroyChildResizer] then
      Parent.Notify(rmdDestroyChildResizer);
end;

function TrmXMLItem.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTXMLForm;
end;

function TrmXMLItem.Parent: TrmXMLNode;
begin
  Result := TrmXMLNode(Owner);
end;

procedure TrmXMLItem.RefreshParent;
begin
  if Parent <> nil then
    Parent.Refresh
  else
    Refresh;
end;

procedure TrmXMLItem.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventBeforeCalculate, cEventOnCalculate, cEventAfterCalculate]);
end;

function TrmXMLItem.Renderer: IrmXMLRenderEngine;
var
  F: TrmXMLForm;
begin
  F := XMLForm;
  if Assigned(F) then
    Result := F.Renderer
  else
    Result := nil;
end;

function TrmXMLItem.ScreenToClient(APos: TPoint): TPoint;
var
  P: TrmdXMLForm;
begin
  P := TrmdXMLForm(GetWinControlParent);
  Result := UnZoomed(P.ScreenToClient(APos));
  Dec(Result.X, P.FObjectOffset.X);
  Dec(Result.Y, P.FObjectOffset.Y);
  Result := FormToClient(Result);
end;

procedure TrmXMLItem.SetDescriptionFont(AFont: TrwFont);
begin
  AFont.Name := 'Arial';
  AFont.Style := [fsBold];
  AFont.Size := Round(Zoomed(8));

  if Enabled then
    AFont.Color := clBlack
  else
    AFont.Color := clGray;
end;

procedure TrmXMLItem.SetEnabled(const Value: Boolean);
begin
  FEnabled := Value;
  Invalidate;
end;

procedure TrmXMLItem.SetItemIndex(const AValue: Integer);
var
  i: Integer;
begin
  if Loading then
    FReadItemIndex := AValue

  else
    if (Parent <> nil) and (ItemIndex <> AValue) then
    begin
      i := ItemIndex;
      if i = -1 then
        i := Parent.FItems.Add(Self);
      Parent.FItems.Move(i, AValue);
      Parent.AssignScreenPosition;
    end;
end;

procedure TrmXMLItem.SetParent(AParent: IrmDesignObject);
begin
  if Assigned(AParent) then
    if AParent.RealObject <> Parent then
      AParent.AddChildObject(Self)
    else
      XMLForm.Refresh;
end;

procedure TrmXMLItem.SetRendStatus(const Value: TrmXMLRenderingStatus);
begin
  if Enabled then
    FRendStatus := Value
  else
    FRendStatus := rmXMLRSNone;

  if FRendStatus = rmXMLRSNone then
    EmptyAggregateFunctions;
end;

procedure TrmXMLItem.SetVisible(Value: Boolean);
begin
  Value := True;
  inherited;
end;

procedure TrmXMLItem.SetXMLDocNode(AValue: TxmlDocNodeHandle);
begin
  FXMLDocNode := AValue;
end;

function TrmXMLItem.TreeBranchRect: TRect;
begin
  Result := Rect(0, 0, Width, Height);
end;

procedure TrmXMLItem.UnselectIfSelected;
var
  xmlForm : IrmDesignObject;
  SelObj : TrmListOfObjects;
  i : integer;

  function ObjectsAreTheSame(const AObject1, AObject2: IrmInterfacedObject): Boolean;
  begin
    Result := not Assigned(AObject1) and not Assigned(AObject2) or
             Assigned(AObject1) and Assigned(AObject2) and (AObject1.VCLObject = AObject2.VCLObject);
  end;

begin
  // unselect itself
  xmlForm := GetXMLForm;
  if Assigned(xmlForm) and Assigned(xmlForm.GetDesigner) then
  begin
    SelObj := xmlForm.GetDesigner.GetSelectedObjects;
    for i := Low(SelObj) to High(SelObj) do
      if ObjectsAreTheSame(SelObj[i], IrmDesignObject(self)) then
      begin
        xmlForm.GetDesigner.UnSelectObject(SelObj[i]);
        break;
      end;
  end;
end;

function TrmXMLItem.XMLForm: TrmXMLForm;
var
  P: TrwComponent;
begin
  P := Self;
  while Assigned(P) and not TrmXMLItem(P).FIsForm do
    P := TrwComponent(P.Owner);

{  if P is TrmXMLForm then
    Result := TrmXMLForm(P)
  else
    Result := nil;}

  if Assigned(P) then
    Result := TrmXMLForm(P)
  else
    Result := nil;
end;

function TrmXMLItem.ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
begin
  GetWindowOrgEx(ACanvas.Handle, P);

  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := Zoomed(ClientToForm(Result.TopLeft));
  Result.BottomRight := Zoomed(ClientToForm(Result.BottomRight));

  OffsetRect(Result, P.X, P.Y);
end;

function TrmXMLItem.ZoomedClientRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
begin
  GetWindowOrgEx(ACanvas.Handle, P);

  Result := ClientRect;
  Result.TopLeft := Zoomed(ClientToForm(Result.TopLeft));
  Result.BottomRight := Zoomed(ClientToForm(Result.BottomRight));

  OffsetRect(Result, P.X, P.Y);
end;


{ TrmdXMLForm }

constructor TrmdXMLForm.Create(AOwner: TComponent);
begin
  inherited;
  FObjectOffset := Point(10, 5);
end;

function TrmdXMLForm.DispatchMessage(var Message: TMessage): Boolean;
var
  P: TPoint;
  O: TrmXMLItem;

  procedure ControlMouseEvent;
  begin
    with TWMMouse(Message) do
      case Message.Msg of
        WM_MOUSEMOVE:      O.MouseMove(KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDOWN:    O.MouseDown(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONUP:      O.MouseUp(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDBLCLK:;
        WM_RBUTTONDOWN:    O.MouseDown(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONUP:      O.MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONDBLCLK:;
        WM_MBUTTONDOWN:    O.MouseDown(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONUP:      O.MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONDBLCLK:;
        WM_MOUSEWHEEL:   ;
      end;
  end;

begin
  Result := Inherited DispatchMessage(Message);

  if Result then
    UpdateMouseLastControl(nil)

  else
    if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
    begin
      P := UnZoomed(Point(Smallint(Message.LParamLo), Smallint(Message.LParamHi)));
      Dec(P.X, FObjectOffset.X);
      Dec(P.Y, FObjectOffset.Y);

      if GetCaptureRMControl = nil then
        O := TrmXMLItem(TrmXMLForm(rwObject).ControlAtPos(P))
      else
        O := TrmXMLItem(GetCaptureRMControl);
      if Assigned(O) then
      begin
        P := O.FormToClient(P);
        Message.lParam := LongInt(MakeLong(Word(P.X), Word(P.Y)));
        if not IsDesignMsg(O, Message) then
          ControlMouseEvent;
      end
      else
      begin
        Message.lParam := LongInt(MakeLong(Word(P.X), Word(P.Y)));
        if not IsDesignMsg(rwObject, Message) then
        begin
          O := TrmXMLForm(rwObject);
          ControlMouseEvent;
        end;
      end;

      Result := True;

      UpdateMouseLastControl(O)      
    end;
end;

procedure TrmdXMLForm.Paint;
var
  SaveIndex: Integer;
  R: TRect;
begin
  SaveIndex := SaveDC(Canvas.Handle);
  try
    with Canvas do
    begin
      R := Rect(0, 0, TrmXMLForm(RWObject).Width, TrmXMLForm(RWObject).Height);
      OffsetRect(R, FObjectOffset.X, FObjectOffset.Y);
      R := Zoomed(R);
      ExcludeClipRect(Canvas.Handle, R.Left, R.Top, R.Right, R.Bottom);
      R := Canvas.ClipRect;
      Brush.Color := clAppWorkSpace;
      Brush.Style := bsSolid;
      FillRect(R);
    end;
  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;

  SaveIndex := SaveDC(Canvas.Handle);
  try
    MoveWindowOrg(Canvas.Handle, Zoomed(FObjectOffset.X), Zoomed(FObjectOffset.Y));
    TrmXMLForm(RWObject).PaintToCanvas(Canvas);

    R := Zoomed(Rect(0, 0, TrmXMLForm(RWObject).Width, TrmXMLForm(RWObject).Height));
    with Canvas do
    begin
      Pen.Width := 2;
      Pen.Color := clBlack;
      Pen.Mode := pmCopy;
      Pen.Style := psSolid;
      MoveTo(R.Left, R.Bottom + 1);
      LineTo(R.Right + 1, R.Bottom + 1);
      LineTo(R.Right + 1, R.Top);
    end;

  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;
end;

procedure TrmdXMLForm.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
    UpdateSize;

  inherited;
end;

procedure TrmdXMLForm.UpdateSize;
var
  l, t, w, h: Integer;
begin
  if TrmXMLForm(RWObject).GetDesigner <> nil then
  begin
    FZoomFactor := TrmXMLForm(RWObject).GetDesigner.GetZoomFactor;

    if FZoomFactor = 0 then
      Exit;

    w := Zoomed(TrmXMLForm(RWObject).Width + FObjectOffset.X * 2);
    h := Zoomed(TrmXMLForm(RWObject).Height + FObjectOffset.Y * 2);
    l := Left;
    t := Top;
    SetBounds(l, t, w, h);
  end;
end;

procedure TrmdXMLForm.WMSetCursor(var Message: TWMSetCursor);
var
  Crs: TCursor;
  Control: TrmXMLItem;
  P: TPoint;
begin
  inherited;
  with Message do
    if CursorWnd = Handle then
      case Smallint(HitTest) of
        HTCLIENT:
          begin
            Crs := Screen.Cursor;
            if Crs = crDefault then
            begin
              GetCursorPos(P);
              P := TrmXMLForm(rwObject).ScreenToClient(P);
              Control := TrmXMLItem(TrmXMLForm(rwObject).ControlAtPos(P));
              if Assigned(Control) then
                Crs := Control.Cursor;

              if Crs = crDefault then
                Crs := Cursor;
            end;

            if Crs <> crDefault then
            begin
              Windows.SetCursor(Screen.Cursors[Crs]);
              Result := 1;
            end;
          end;
      end;
end;

{ TrmXMLForm }

constructor TrmXMLForm.Create(AOwner: TComponent);
begin
  inherited;
  FIsForm := True;
  FSchemas := TrmXMLSchemas.Create(Self, TrmXMLSchema);
  FSubChildrenNamesUnique := True;
end;

function TrmXMLForm.CreateVisualControl: IrwVisualControl;
begin
  Result := TrmdXMLForm.Create(Self);
end;


procedure TrmXMLForm.DefineProperties(Filer: TFiler);
var
  Ancestor: TrmXMLForm;
begin
  inherited;

  if Filer is TWriter then
    Ancestor := TWriter(Filer).Ancestor as TrmXMLForm
  else
    Ancestor := nil;

  FSchemasRW := TrmXMLSchemas.Create(Self, TrmXMLSchema);
  try
    if Filer is TWriter then
    begin
      FSchemasRW.Assign(FSchemas);
      if Assigned(Ancestor) then
        FSchemasRW.MakeDelta(Ancestor.FSchemas)
    end;

    Filer.DefineProperty('Schemas', ReadSchemas, WriteSchemas, FSchemasRW.Count > 0);
  finally
    FreeAndNil(FSchemasRW);
  end;
end;

destructor TrmXMLForm.Destroy;
begin
  FreeAndNil(FSchemas);
  inherited;
end;

procedure TrmXMLForm.DoAssignScreenPosition;
var
  i: Integer;
  R: TRect;
begin
  for i := 0 to ItemCount - 1 do
    Items[i].AssignScreenPosition;

  R := TreeBranchRect;
  SetBoundsRect(R);
end;

procedure TrmXMLForm.DoCalculate;
begin
//  XMLDocNode := Renderer.AddComment('Generated by iSystems Report Writer at ' + DateToStr(Date));
  inherited;
end;

function TrmXMLForm.GetItemLevel: Integer;
begin
  Result := 0;
end;

function TrmXMLForm.GetSchemas: IrmXMLSchemas;
begin
  Result := Schemas;
end;

procedure TrmXMLForm.InitializeForDesign;
begin
  inherited;

  FNameSpace := 'http://www.w3.org/2001/XMLSchema';
end;

procedure TrmXMLForm.Invalidate;
begin
  if VisualControlCreated then
    VisualControl.PropChanged(rwCPInvalidate);
end;

procedure TrmXMLForm.LoadXSDSchema(const AFileName: TFileName);
var
  SchemaParser: IrmXMLxsdSchemaParser;
begin
  SchemaParser := TrmXMLxsdSchemaParser.Create(Self, AFileName);
  SchemaParser.ExecuteParsing;
end;

procedure TrmXMLForm.Notify(AEvent: TrmDesignEventType);
begin
  if AEvent = rmdZoomChanged then
  begin
    TrmdXMLForm(VisualControl.RealObject).UpdateSize;
    AssignScreenPosition;
  end;
    
  inherited;
end;

procedure TrmXMLForm.PaintChildren(ACanvas: TrwCanvas);
var
  SaveIndex: Integer;
  R: TRect;
begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
    R := Rect(Left, Top, Width + Left, Height + Top);
    R := Zoomed(R);
    IntersectClipRect(ACanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

    inherited;

  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;
end;

procedure TrmXMLForm.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := ClientRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;
    Pen.Color := clBlack;
    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;
end;

function TrmXMLForm.Parent: TrmXMLNode;
begin
  Result := nil;
end;

procedure TrmXMLForm.ReadSchemas(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FSchemasRW);

  FSchemas.ApplyDelta(FSchemasRW);
end;

procedure TrmXMLForm.Refresh;
begin
  AssignScreenPosition;
  Invalidate;
end;

function TrmXMLForm.Renderer: IrmXMLRenderEngine;
begin
  if Assigned(Owner) and Assigned(TrmReport(Owner).Renderer) then
    Result := TrmReport(Owner).Renderer.XMLRenderEngine
  else
    Result := nil;
end;

procedure TrmXMLForm.SaveXSDSchema(const ASchemaName: String; const AFileName: TFileName);
var
  Schema: TrmXMLSchema;
begin
  Schema := Schemas.FindSchema(ASchemaName);
  if Assigned(Schema) then
    Schema.SaveToFile(AFileName);
end;

procedure TrmXMLForm.SetDesigner(ADesigner: IrmDesigner);
begin
  inherited;
  if Assigned(ADesigner) then
    DesignParent := ADesigner.GetDesignParent(Name)
  else
    DesignParent := nil;
end;

procedure TrmXMLForm.SetDesignParent(Value: TWinControl);
begin
  if Assigned(Value) and ((DsgnLockState <> rwLSHidden) or LibDesignMode or GetDesigner.LibComponentDesigninig) then
    VisualControl.RealObject.Parent := Value
  else
    DestroyVisualControl;

  inherited;

//  AssignScreenPosition; RM designer cares of this situation
end;

procedure TrmXMLForm.SetParentComponent(Value: TComponent);
begin
  // Do not need to do anything in here!
end;

procedure TrmXMLForm.SyncTagPos;

   procedure SyncTreeButtons(ANode: TrmXMLNode);
   var
     i: Integer;
   begin
     for i := 0 to ANode.ItemCount - 1 do
       if ANode.Items[i] is TrmXMLNode then
       begin
         TrmXMLNode(ANode.Items[i]).SyncTreeButton;
         SyncTreeButtons(TrmXMLNode(ANode.Items[i]));
       end;
   end;

begin
  inherited;
  SyncTreeButtons(Self);
end;

function TrmXMLForm.TopElement: TrmXMLelement;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLelement then
      Result := TrmXMLelement(Items[i]);
end;

function TrmXMLForm.TreeBranchRect: TRect;
var
  i: Integer;
  Item: TrmXMLItem;
  W, H: Integer;
  R: TRect;
begin
  W := 1000;
  H := 1000;
  for i := 0 to ItemCount - 1 do
  begin
    Item := Items[i];

    if Item.DoNotPaint then
      Continue;

    R := Item.TreeBranchRect;
    Inc(R.Right, cLevelOffset);
    Inc(R.Bottom, cLevelOffset);

    W := Max(Item.Left + R.Right, W);
    H := Max(Item.Top + R.Bottom, H);
  end;

  Result := Rect(0, 0, W, H);
end;


procedure TrmXMLForm.UpdateXSDSchema(const ASchemaName: String; const AFileName: TFileName);
var
  Schema: TrmXMLSchema;
  SchemaDoc: IXMLDOMDocument2;
  N: IXMLDOMNode;
  SchemaDescription: String;
begin
  Schema := Schemas.FindSchema(ASchemaName);
  if Assigned(Schema) then
  begin
    if Schema.Name <> ChangeFileExt(ExtractFileName(AFileName), '') then
      raise Exception.Create('Schema name should equals the file name');
    SchemaDoc := CreateXMLDoc;
    SchemaDoc.setProperty('SelectionNamespaces', 'xmlns:xsd="' + cXMLrwW3CNameSpace + '"');
    SchemaDoc.async := False;
    SchemaDoc.load(AFileName);
    N := SchemaDoc.DocumentElement.selectSingleNode('./xsd:annotation/xsd:documentation');
    if Assigned(N) then
      SchemaDescription := N.text
    else
      SchemaDescription := '';
    Schema.Description := SchemaDescription;
    Schema.LoadFromFile(AFileName);
  end;
end;

procedure TrmXMLForm.AddXSDSchema(const AFileName: TFileName);
var
  sSchemaName: String;
  Schema: TrmXMLSchema;
  SchemaDoc: IXMLDOMDocument2;
  N: IXMLDOMNode;
  sSchemaDescription: String;
begin
  sSchemaName := ChangeFileExt(ExtractFileName(AFileName), '');
  Schema := Schemas.FindSchema(sSchemaName);
  if Assigned(Schema) then
    raise Exception.Create('Schema "' + sSchemaName + '" already exists');

  SchemaDoc := CreateXMLDoc;
  SchemaDoc.setProperty('SelectionNamespaces', 'xmlns:xsd="' + cXMLrwW3CNameSpace + '"');
  SchemaDoc.async := False;
  SchemaDoc.load(AFileName);
  N := SchemaDoc.DocumentElement.selectSingleNode('./xsd:annotation/xsd:documentation');
  if Assigned(N) then
    sSchemaDescription := N.text
  else
    sSchemaDescription := '';

  Schemas.BeginWorkWithItems;
  try
    Schema := Schemas.Add as TrmXMLSchema;
    Schema.Name := sSchemaName;
    Schema.Description := sSchemaDescription;
    Schema.LoadFromFile(AFileName);
  finally
    Schemas.EndWorkWithItems;
  end;
end;

procedure TrmXMLForm.WriteSchemas(Writer: TWriter);
begin
  Writer.WriteCollection(FSchemasRW);
end;


{ TrmXMLSimpleType }

function TrmXMLSimpleType.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := False;
end;

procedure TrmXMLSimpleType.DoAssignScreenPosition;
var
  i, l: Integer;
begin
  if LibDesignMode then
  begin
    Left := cFirstLevelOffset;
    Top := cFirstLevelOffset;
    Height := cItemHeight;
  end

  else
  begin
    Height := cItemHeight;
    
    if Parent is TrmXMLelement then
      Top := (Parent.Height - Height) div 2
    else
      Top := Parent.FChidItemsOffset.Y;

    i := ItemIndex;
    if i = 0 then
      l := Parent.FChidItemsOffset.X
    else
      l := Parent.Items[i - 1].Left + Parent.Items[i - 1].Width;

    if Parent is TrmXMLelement then
      Inc(l, cItemLeftOffset);

    Left := l;
  end;

  Width := cItemHeight;
end;

procedure TrmXMLSimpleType.DoOnChangeValue(var ANewValue: Variant);
begin
  ExecuteEventsHandler('OnChangeValue', [Integer(Addr(ANewValue))]);
end;

procedure TrmXMLSimpleType.DoOnFilterText(var AText: String);
var
  V: Variant;
begin
  V := AText;
  ExecuteEventsHandler('OnFilterText', [Integer(Addr(V))]);
  AText := V;
end;

function TrmXMLSimpleType.GetText: string;
begin
  Result := VarToStr(Value);
  DoOnFilterText(Result);
end;

function TrmXMLSimpleType.GetValue: Variant;
begin
  Result := Unassigned;
end;

procedure TrmXMLSimpleType.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;
    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;
    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := cXMLSimpleTypeColor;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;
end;

procedure TrmXMLSimpleType.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnFilterText, cEventOnChangeValue]);
end;

procedure TrmXMLSimpleType.SetValue(const AValue: Variant);
begin
end;


{ TrmXMLNode }

procedure TrmXMLNode.AddChildObject(AObject: IrmDesignObject);
var
  Frm: TrmXMLForm;
begin
  if AObject.RealObject is TrmXMLItem then
  begin
    if TrmXMLItem(AObject.RealObject).Parent <> nil then
      TrmXMLItem(AObject.RealObject).Parent.DeleteItem(TrmXMLItem(AObject.RealObject));
    inherited;
    AddItem(TrmXMLItem(AObject.RealObject));
    Frm := XMLForm;
    if Assigned(Frm) then
      Frm.Refresh;
  end
  else
    inherited;
end;

procedure TrmXMLNode.AddItem(AItem: TrmXMLItem);
begin
  FItems.Add(AItem);
  if not Loading then
  begin
    ResortItems;
    AItem.AssignScreenPosition;
    SyncTreeButton;
  end;
end;

procedure TrmXMLNode.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
var
  i, NewInd, EndItem: Integer;
  Fld: TrmXMLNode;
begin
  if Parent = nil then
    Exit;

  if Parent is TrmXMLelement then
    EndItem := TrmXMLelement(Parent).FLastAttributeIndex
  else
    EndItem := TrmXMLelement(Parent).ItemCount - 1;

  if EndItem = -1 then
    Exit;  

  if ALeft < 0 then
    NewInd := 0

  else if ALeft > Parent.Width then
    NewInd := EndItem

  else
  begin
    NewInd := ItemIndex;

    for i := 0 to EndItem do
    begin
      Fld := Parent.Items[i] as TrmXMLNode;
      if (Fld.Left + Fld.Width > ALeft) and (Fld.Left + Fld.Width < ALeft + Width) or
         (Fld.Left < ALeft) and (Fld.Left + Fld.Width > ALeft + Width) then
      begin
        NewInd := Fld.ItemIndex;
        if ALeft < Left then
          Inc(NewInd);
        break;
      end;
    end;
  end;

  if NewInd > EndItem then
    NewInd := EndItem;

  ItemIndex := NewInd;
end;

procedure TrmXMLNode.CloseNode;
var
  i : integer;
begin
  FNodeClosed := True;
  for i := 0 to FItems.Count - 1 do
    TrmXMLItem(FITems[i]).UnselectIfSelected;
  XMLForm.Refresh;
end;

function TrmXMLNode.ControlAtPos(APos: TPoint): TrwComponent;
var
  i: Integer;
  R: TRect;
  P: TPoint;
  C, Cin: TrmXMLItem;
begin
  Cin := nil;

  if not FNodeClosed then
    for i := 0 to ItemCount - 1 do
    begin
      C := Items[i];

      if C.DoNotPaint then
        Continue;

      if C is TrmXMLNode then
      begin
        R := C.BoundsRect;
        if PtInRect(R, APos) then
          Cin := C;

        P := APos;
        Dec(P.X, R.Left);
        Dec(P.Y, R.Top);
        C := TrmXMLItem(TrmXMLNode(C).ControlAtPos(P));
        if Assigned(C) then
          Cin := C;

        if Assigned(Cin) then
          break;
      end;
    end;

  if not Assigned(Cin) then
    for i := 0 to ItemCount - 1 do
    begin
      C := Items[i];

      if C.DoNotPaint then
        Continue;

      if (C is TrmXMLItem) and not (C is TrmXMLNode) then
      begin
        R := C.BoundsRect;
        if PtInRect(R, APos) then
        begin
          Cin := C;
          break;
        end;
      end;
    end;

  Result := Cin;
end;

constructor TrmXMLNode.Create(AOwner: TComponent);
begin
  FItems := TList.Create;
  inherited;
  FVirtOwner := True;
  FBlockParentIfEmpty := False;  
end;

procedure TrmXMLNode.DeleteItem(AItem: TrmXMLItem);
var
  i: Integer;
begin
  i := AItem.ItemIndex;
  if i <> -1 then
  begin
    ResortItems;
    FItems.Delete(i);
    SyncTreeButton;
    if XMLForm <> nil then
      XMLForm.Refresh;
  end;
end;

function TrmXMLNode.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := True;
end;

destructor TrmXMLNode.Destroy;
begin
  FreeAndNil(FXMLNodeTreeButton);
  inherited;
  FreeAndNil(FItems);
end;

procedure TrmXMLNode.DoAssignScreenPosition;
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    Items[i].AssignScreenPosition;
end;

procedure TrmXMLNode.DoCalculate;
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLanyElement then
      TrmXMLanyElement(Items[i]).CalculateBranch
    else
      Items[i].Calculate;

  inherited;
end;

function TrmXMLNode.GetBoundsRect: TRect;
begin
  Result := TreeBranchRect;
  OffsetRect(Result, Left, Top);
end;

function TrmXMLNode.GetItem(Index: Integer): TrmXMLItem;
begin
  Result := TrmXMLItem(FItems[Index]);
end;

procedure TrmXMLNode.Invalidate;
begin
  InvalidateRect(TreeBranchRect);
end;

function TrmXMLNode.IsTreeButtonVisible: Boolean;
begin
  Result := False;
end;

function TrmXMLNode.ItemCount: Integer;
begin
  Result := FItems.Count;
end;

function TrmXMLNode.ItemIndexOf(const AItem: TrmXMLItem): Integer;
begin
  Result := FItems.IndexOf(AItem);
end;

procedure TrmXMLNode.Notify(AEvent: TrmDesignEventType);
var
  i: Integer;
begin
  inherited;

  if (AEvent = rmdSelectChildObject) and FNodeClosed then
    OpenNode;

  if AEvent = rmdZoomChanged then
  begin
    for i := 0 to ItemCount - 1 do
      Items[i].Notify(AEvent);
  end;
end;

procedure TrmXMLNode.OpenNode;
begin
  FNodeClosed := False;
  XMLForm.Refresh;
end;

procedure TrmXMLNode.PaintChildren(ACanvas: TrwCanvas);
var
  i: Integer;
  SaveIndexChld: Integer;
  Item: TrmXMLItem;
begin
  if FNodeClosed then
    exit;

  SaveIndexChld := SaveDC(ACanvas.Handle);
  try
    MoveWindowOrg(ACanvas.Handle, Zoomed(Left), Zoomed(Top));

    for i := 0 to ItemCount - 1 do
    begin
      Item := Items[i];

      if Item.DoNotPaint then
        Continue;

      Item.PaintToCanvas(ACanvas);
    end;

  finally
    RestoreDC(ACanvas.Handle, SaveIndexChld);
  end;
end;

procedure TrmXMLNode.PaintControl(ACanvas: TrwCanvas);
begin
  if GetItemLevel > 0 then
    PaintTreeLines(ACanvas);
end;

procedure TrmXMLNode.PaintForeground(ACanvas: TrwCanvas);
begin
end;

procedure TrmXMLNode.PaintToCanvas(ACanvas: TrwCanvas);
var
  SaveIndex: Integer;
begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
//    ExcludeNotOpaqueRegions;
    inherited;
  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;

  PaintChildren(ACanvas);
  PaintForeground(ACanvas);
end;

procedure TrmXMLNode.PaintTreeLines(ACanvas: TrwCanvas);
const cDashGap = 3;
var
  R: TRect;
begin
  with ACanvas do
  begin
    Brush.Style := bsSolid;

    if not BlockParentIfEmpty then
    begin
      if Enabled then
        Brush.Color := clBlack
      else
        Brush.Color := clGray;

      R.Left := cItemHeight div 2 - 2;
      R.Top := Top + Height div 2 - 2;
      R.Bottom := R.Top + 2;
      R.Right := R.Left + cDashGap;

      while R.Left < Left do
      begin
        if R.Right > Left then
          R.Right := Left;
        FillRect(Zoomed(R));
        OffsetRect(R, cDashGap * 2, 0);
      end;
    end

    else
    begin
      Brush.Color := clBlack;
      R.Left := cItemHeight div 2 - 2;
      R.Top := Top + Height div 2 - 2;
      R.Right := Left;
      R.Bottom := R.Top + 2;
      FillRect(Zoomed(R));
    end;

    R.Left := cItemHeight div 2 - 2;
    R.Bottom := Top + Height div 2;
    R.Top := Parent.Height;
    R.Right := R.Left + 2;
    FillRect(Zoomed(R));
  end;
end;

procedure TrmXMLNode.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
  i: Integer;
begin
  i := 0;
  while i < ComponentCount do
    if not TrwComponent(Components[i]).InheritedComponent then
      Components[i].Free
    else
      Inc(i);

  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited;
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrmXMLNode.Refresh;
var
  Rold, Rnew: TRect;
begin
  if XMLForm = nil then
    Exit;

  Rold := XMLForm.TreeBranchRect;
  AssignScreenPosition;
  Rnew := XMLForm.TreeBranchRect;
  if (Rold.Right <> Rnew.Right) or (Rold.Bottom <> Rnew.Bottom) then
    XMLForm.Refresh
  else
    inherited;
end;

procedure TrmXMLNode.RemoveChildObject(AObject: IrmDesignObject);
begin
  if AObject.RealObject is TrmXMLItem then
    DeleteItem(TrmXMLItem(AObject.RealObject));
  inherited;
end;

procedure TrmXMLNode.ResortItems;
begin
end;

procedure TrmXMLNode.RWLoaded;
var
  i: Integer;
  Itm: TrmXMLItem;
begin
  inherited;


  for i := 0 to FItems.Count - 1 do
    FItems[i] := nil;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TrmXMLItem then
    begin
      Itm := TrmXMLItem(Components[i]);
      if Itm.FReadItemIndex <> -1 then
      begin
        if FItems[Itm.FReadItemIndex] = nil then
          FItems[Itm.FReadItemIndex] := Itm
        else
          FItems.Add(Itm);
        Itm.FReadItemIndex := -1;
      end
      else
        FItems.Add(Itm); // In case if index is corrupted
    end;

  FItems.Pack;

  ResortItems;
end;

procedure TrmXMLNode.SetBlockParentIfEmpty(const Value: Boolean);
begin
  FBlockParentIfEmpty := Value;
  if Parent <> nil then
    Parent.Invalidate;
end;

procedure TrmXMLNode.SetBoundsRect(ARect: TRect);
begin
  inherited;
end;

procedure TrmXMLNode.SetRendStatusChildrenToo(AStatus: TrmXMLRenderingStatus);
begin
  RendStatus := AStatus;
  SetRendStatusForChildren(AStatus);
end;

procedure TrmXMLNode.SetRendStatusForChildren(AStatus: TrmXMLRenderingStatus);
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLNode then
      TrmXMLNode(Items[i]).SetRendStatusChildrenToo(AStatus)
    else
      Items[i].RendStatus := AStatus;
end;

procedure TrmXMLNode.SyncTagPos;
begin
  inherited;
  if Assigned(FXMLNodeTreeButton) then
    FXMLNodeTreeButton.UpdateRegion;
end;

procedure TrmXMLNode.SyncTreeButton;
begin
  if IsDesignMode and IsTreeButtonVisible then
  begin
    if not Assigned(FXMLNodeTreeButton) then
    begin
      FXMLNodeTreeButton := TrmdXMLNodeButton.Create(Self);
      FXMLNodeTreeButton.Visible := True;
    end

    else
      FXMLNodeTreeButton.UpdateRegion;
  end

  else if Assigned(FXMLNodeTreeButton) and not IsTreeButtonVisible then
    FreeAndNil(FXMLNodeTreeButton);
end;

function TrmXMLNode.TreeBranchRect: TRect;
var
  i: Integer;
  Item: TrmXMLItem;
  W, H: Integer;
  R: TRect;
begin
  W := Width;
  H := Height;
  for i := 0 to ItemCount - 1 do
  begin
    Item := Items[i];

    if Item.DoNotPaint then
      Continue;

    R := Item.TreeBranchRect;
    W := Max(Item.Left + R.Right, W);

    if not FNodeClosed then
      H := Max(Item.Top + R.Bottom, H);
  end;

  Result := Rect(0, 0, W, H);
end;

procedure TrmXMLNode.UnselectIfSelected;
var
  i : integer;
begin
  inherited;
  for i := 0 to FItems.Count - 1 do
     TrmXMLItem(FITems[i]).UnselectIfSelected;
end;

procedure TrmXMLNode.WriteState(Writer: TWriter);
var
  OldRoot, OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := OldRootAncestor.FindComponent(Name);
    if not Assigned(Writer.RootAncestor) then
      Writer.RootAncestor := Self;
    Writer.Root := Self;
    inherited;
  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;


{ TrmXMLSimpleTypeByList }

function TrmXMLSimpleTypeByList.BeforeCalculate: Boolean;
begin
  FText := '';
  Result := SimpleContent <> nil;
  if Result then
    Result := inherited BeforeCalculate;
end;

function TrmXMLSimpleTypeByList.CanWriteValue: Boolean;
begin
  Result := FormulaValue = nil;
end;

constructor TrmXMLSimpleTypeByList.Create(AOwner: TComponent);
begin
  inherited;
  FValList := Null;
end;

function TrmXMLSimpleTypeByList.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if Cl.InheritsFrom(TrmXMLItem) then
    if Cl.InheritsFrom(TrmXMLSimpleType) then
      SimpleContent.Free
    else
    begin
      if  Parent <> nil then
        Result := Parent.CreateChildObject(AClassName);
      Exit;
    end;

  Result := inherited CreateChildObject(AClassName);
end;

procedure TrmXMLSimpleTypeByList.DoCalculate;
var
  i: Integer;
  v: Variant;
begin
  v := Value;
  if not VarIsNull(v) then
    for i := varArrayLowBound(v, 1) to varArrayHighBound(v, 1) do
    begin
      SimpleContent.RendStatus := rmXMLRSNone;
      SimpleContent.Value := v[i];
      SimpleContent.Calculate;

      if SimpleContent.RendStatus = rmXMLRSCalculated then
      begin
        if FText <> '' then
          FText := FText + ' ' ;
        FText := FText + SimpleContent.GetText;
      end;
    end;

  SimpleContent.RendStatus := rmXMLRSCalculated;

  inherited;
end;

function TrmXMLSimpleTypeByList.FormulaValue: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatSetProperty, 'Value');
end;

function TrmXMLSimpleTypeByList.GetText: String;
begin
  GetValue; // run calc process
  Result := FText;
  DoOnFilterText(Result);
end;

function TrmXMLSimpleTypeByList.GetValue: Variant;
begin
  if not (csWriting in ComponentState) and (Renderer <> nil) then
    Calculate;

  Result := FValList;
end;

procedure TrmXMLSimpleTypeByList.SetValue(const AValue: Variant);
var
  V: Variant;
begin
  if VarIsNull(AValue) then
    V := Null

  else
  begin
    if not varIsArray(AValue) then
      V := varArrayOf([AValue])
    else
      V := AValue;
  end;

  DoOnChangeValue(V);
  FValList := V;

  Invalidate;
end;

function TrmXMLSimpleTypeByList.SimpleContent: TrmXMLSimpleType;
begin
  if ItemCount > 0 then
    Result := Items[0] as TrmXMLSimpleType
  else
    Result := nil;    
end;

{ TrmXMLelement }

constructor TrmXMLelement.Create(AOwner: TComponent);
begin
  FFirstAttributeIndex := -1;
  FLastAttributeIndex := -1;  
  inherited;
  NSName := '';
end;

function TrmXMLelement.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if IsSimpleContent then
  begin
    if Cl.InheritsFrom(TrmXMLanyElement) then
      rwError('Simple element cannot contain other elements')
    else if Cl.InheritsFrom(TrmXMLSimpleType) then
      rwError('Element cannot contain multiple values');
  end

  else if IsComplexContent and Cl.InheritsFrom(TrmXMLSimpleType) then
    rwError('Complex element cannot have value');

  Result := inherited CreateChildObject(AClassName);
end;

procedure TrmXMLelement.DoAssignScreenPosition;
var
  R: TRect;
begin
  ResortItems;

  R := CalcTextSize(GetShowText);
  FChidItemsOffset := Point(R.Right - R.Left, 0);

  inherited;

  Width := FChidItemsOffset.X;
end;

procedure TrmXMLelement.DoCalculate;
begin
  XMLDocNode := Renderer.AddElement(NSName);

  inherited;

  if IsSimpleContent then
    if VarIsNull(SimpleContent.Value) then
      Renderer.AddText('')
    else
      Renderer.AddText(SimpleContent.GetText)
  else
    Renderer.AddElementTerminator;
end;

function TrmXMLelement.GetShowText: String;
begin
  Result := NSName;
end;

function TrmXMLelement.GetValue: Variant;
begin
  if IsSimpleContent then
    Result := SimpleContent.Value
  else
    Result := Null
end;

function TrmXMLelement.IsComplexContent: Boolean;
var
  i: Integer;
begin
  Result := False;

  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLanyElement then
    begin
      Result := True;
      break;
    end;
end;

function TrmXMLelement.IsSimpleContent: Boolean;
begin
  Result := SimpleContent <> nil;
end;

procedure TrmXMLelement.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := cXMLElementColor;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);

  PaintTreeLines(ACanvas);
end;

procedure TrmXMLelement.PaintTreeLines(ACanvas: TrwCanvas);
var
  R: TRect;
  i, n: Integer;
begin
  if not FNodeClosed then
  begin
    with ACanvas do
    begin
      if Enabled then
        Brush.Color := clBlack
      else
        Brush.Color := clGray;

      Brush.Style := bsSolid;

      R.Left := Left + Width;
      R.Top := Top + Height div 2 - 2;
      R.Bottom := R.Top + 2;

      n := FLastAttributeIndex;
      if (n = -1) and IsSimpleContent then
        n := 0;

      for i := 0 to n do
      begin
        R.Right := Left + Items[i].Left;
        FillRect(Zoomed(R));

        R.Left := R.Right + Items[i].Width;
      end;
    end;
  end;

  if GetItemLevel > 0 then
    inherited;
end;

procedure TrmXMLelement.ResortItems;
var
  Attrs, Nodes: TList;
  SimpTypeInd, i, n: Integer;
begin
{  if not Loading then
  begin
    for i := 0 to FItems.Count - 1 do
      if TrmXMLItem(FItems[i]).ItemIndex = -1 then
        TrmXMLItem(FItems[i]).ItemIndex := i;
  end; }

  FFirstAttributeIndex := -1;
  FLastAttributeIndex := -1;
  
  Attrs := TList.Create;
  Nodes := TList.Create;
  try
    SimpTypeInd := -1;
    Attrs.Capacity := FItems.Capacity;
    Nodes.Capacity := FItems.Capacity;
    for i := 0 to ItemCount - 1 do
      if Items[i] is TrmXMLanyAttribute then
        Attrs.Add(FItems[i])
      else if Items[i] is TrmXMLanyElement then
        Nodes.Add(FItems[i])
      else if Items[i] is TrmXMLSimpleType then
        SimpTypeInd := i;

    // Simple content on first place
    if SimpTypeInd <> -1 then
    begin
      FItems[0] := FItems[SimpTypeInd];
      if Attrs.Count > 0 then
        FFirstAttributeIndex := 1;
    end
    else
      if Attrs.Count > 0 then
        FFirstAttributeIndex := 0;

    // Attributes on second place
    for i := 0 to Attrs.Count - 1 do
      FItems[FFirstAttributeIndex + i] := Attrs[i];

    if Attrs.Count > 0 then
    begin
      FLastAttributeIndex := FFirstAttributeIndex + (Attrs.Count - 1);
      n := FLastAttributeIndex + 1;
    end
    else
      if SimpTypeInd <> -1 then
        n := 1
      else
        n := 0;

    // Complex content on third place
    for i := 0 to Nodes.Count - 1 do
      FItems[n + i] := Nodes[i];

  finally
    FreeAndNil(Attrs);
    FreeAndNil(Nodes);
  end;
end;

procedure TrmXMLelement.SetNSName(const AValue: String);
begin
  FNSName := Trim(AValue);
  if NSName = '' then
    NSName := 'SomeElement';
  RefreshParent;
end;

procedure TrmXMLelement.SetValue(const AValue: Variant);
begin
  if IsSimpleContent then
    SimpleContent.Value := AValue;
end;

function TrmXMLelement.SimpleContent: TrmXMLSimpleType;
begin
  Result := nil;
  if (ItemCount > 0) and (Items[0] is TrmXMLSimpleType) then
    Result := TrmXMLSimpleType(Items[0]);
end;

{ TrmXMLSimpleTypeByRestriction }

function TrmXMLSimpleTypeByRestriction.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := AValue;
end;

function TrmXMLSimpleTypeByRestriction.BeforeCalculate: Boolean;
begin
  FValueHolder.SetValueFromDataSource;
  Result := inherited BeforeCalculate;
end;

function TrmXMLSimpleTypeByRestriction.CanWriteValue: Boolean;
begin
  Result := FormulaValue = nil;
end;

constructor TrmXMLSimpleTypeByRestriction.Create(AOwner: TComponent);
begin
  FValueHolder := TrmsValueHolder.Create(Self);
  FValueHolder.OnChangeValue := DoOnChangeValue;
  inherited;
end;

function TrmXMLSimpleTypeByRestriction.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if Cl.InheritsFrom(TrmXMLItem) then
  begin
    if Parent <> nil then
      Result := Parent.CreateChildObject(AClassName)
  end

  else
    Result := inherited CreateChildObject(AClassName);
end;

function TrmXMLSimpleTypeByRestriction.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.Selectable := (FMousePosition = rmdMPClient) and Result.Selectable;
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight];
end;

destructor TrmXMLSimpleTypeByRestriction.Destroy;
begin
  FreeAndNil(FValueHolder);
  inherited;
end;

procedure TrmXMLSimpleTypeByRestriction.DoAssignScreenPosition;
var
  R: TRect;
begin
  inherited;

  R := CalcTextSize(GetShowText);
  if R.Right - R.Left < cItemHeight then
    Width := cItemHeight
  else
    Width := R.Right - R.Left;
end;

function TrmXMLSimpleTypeByRestriction.FormulaTextFilter: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatDataFilter, 'Text');
end;

function TrmXMLSimpleTypeByRestriction.FormulaValue: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatSetProperty, 'Value');
end;

function TrmXMLSimpleTypeByRestriction.GetDataField: String;
begin
  Result := FValueHolder.DataField;
end;

function TrmXMLSimpleTypeByRestriction.GetDataFieldInfo: TrmDataFieldInfoRec;
var
  P: TrmXMLNode;
begin
  P := Parent;
  while Assigned(P) and not (P is TrmXMLQueryElement) do
    P := P.Parent;

  Result := DataFieldInfo(Self, P);
end;

function TrmXMLSimpleTypeByRestriction.GetFieldName: String;
begin
  Result := DataField;
end;

function TrmXMLSimpleTypeByRestriction.GetShowText: String;
begin
  Result := 'Value';
end;

function TrmXMLSimpleTypeByRestriction.GetText: String;
begin
  Result := VarToStr(Value);
  Result := PostprocessText(Result);
end;

function TrmXMLSimpleTypeByRestriction.GetValue: Variant;
begin
  if not (csWriting in ComponentState) and (Renderer <> nil) then
    Calculate;

  Result := FValueHolder.Value;
end;

procedure TrmXMLSimpleTypeByRestriction.PaintControl(ACanvas: TrwCanvas);
begin
  inherited;
  DrawItemText(ACanvas, GetShowText);
end;

function TrmXMLSimpleTypeByRestriction.PostprocessText(const AText: String): String;
begin
  Result := AText;
  if (FormulaTextFilter <> nil) and (Renderer <> nil) then
    Result := FormulaTextFilter.Calculate([AText]);

  DoOnFilterText(Result);
end;

procedure TrmXMLSimpleTypeByRestriction.SetDataField(const AValue: String);
begin
  FValueHolder.DataField := AValue;
end;

procedure TrmXMLSimpleTypeByRestriction.SetFieldName(const AFieldName: String);
begin
  DataField := AFieldName;
end;

procedure TrmXMLSimpleTypeByRestriction.SetValue(const AValue: Variant);
begin
  if VarIsNull(AValue) then
    FValueHolder.Value := Null
  else
    FValueHolder.Value := ApplyValueRestrictions(AValue);

  Invalidate;
end;

procedure TrmXMLSimpleTypeByRestriction.SetValueFromDataSource;
begin
  FValueHolder.SetValueFromDataSource;
end;

{ TrmXMLattribute }

constructor TrmXMLattribute.Create(AOwner: TComponent);
begin
  inherited;
  NSName := '';
end;

function TrmXMLattribute.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if Cl.InheritsFrom(TrmXMLItem) then
    if Cl.InheritsFrom(TrmXMLSimpleType) then
      SimpleContent.Free
    else
    begin
      if  Parent <> nil then
        Result := Parent.CreateChildObject(AClassName);
      Exit;
    end;

  Result := inherited CreateChildObject(AClassName);
end;

procedure TrmXMLattribute.DoAssignScreenPosition;
var
  i, h, l: Integer;
  R: TRect;
begin
  if LibDesignMode then
  begin
    Left := cFirstLevelOffset;
    Top  := cFirstLevelOffset;
  end

  else
  begin
    i := ItemIndex;
    if i = 0 then
      l := Parent.FChidItemsOffset.X
    else
      l := Parent.Items[i - 1].Left + Parent.Items[i - 1].Width;

    if Parent is TrmXMLelement then
      Inc(l, cItemLeftOffset);

    Left := l;
  end;

  R := CalcTextSize(GetShowText);
  if R.Right - R.Left < cItemHeight then
    Width := cItemHeight
  else
    Width := R.Right - R.Left;

  FChidItemsOffset := Point(R.Right - R.Left, 0);

  inherited;

  h := 0;
  for i := 0 to ItemCount - 1 do
    if h < Items[i].Height then
      h := Items[i].Height;

  if h = 0 then
    Height := cItemHeight
  else
    Height := h + FChidItemsOffset.Y * 2;

  if not LibDesignMode then
    if Parent <> nil then
      if Parent is TrmXMLelement then
        Top := (Parent.Height - Height) div 2
      else
        Top := Parent.FChidItemsOffset.Y;

  if SimpleContent <> nil then
    Width := SimpleContent.Left + SimpleContent.Width;
end;

procedure TrmXMLattribute.DoCalculate;
begin
  inherited;

  if IsSimpleContent then
    if VarIsNull(SimpleContent.Value) then
      XMLDocNode := Renderer.AddAttribute(NSName, '')
    else
      XMLDocNode := Renderer.AddAttribute(NSName, SimpleContent.GetText);
end;

function TrmXMLattribute.GetShowText: String;
begin
  Result := NSName;
end;

function TrmXMLattribute.GetValue: Variant;
begin
  if IsSimpleContent then
    Result := SimpleContent.Value
  else
    Result := Null
end;

function TrmXMLattribute.IsSimpleContent: Boolean;
begin
  Result := SimpleContent <> nil;
end;

procedure TrmXMLattribute.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := cXMLAttributeColor;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);
end;

procedure TrmXMLattribute.SetValue(const AValue: Variant);
begin
  if IsSimpleContent then
    SimpleContent.Value := AValue;
end;

procedure TrmXMLattribute.SetNSName(const AValue: String);
begin
  FNSName := Trim(AValue);
  if FNSName = '' then
    FNSName := 'SomeAttribute';
  RefreshParent;
end;

function TrmXMLattribute.SimpleContent: TrmXMLSimpleType;
begin
  if ItemCount > 0 then
    Result := TrmXMLSimpleType(Items[0])
  else
    Result := nil;
end;


{ TrmXMLanyElement }

function TrmXMLanyElement.AfterCalculateBranch: Boolean;
var
  Fml: TrmFMLFormula;
  i: Integer;
  Node: TrmXMLanyElement;
  V: Variant;
begin
  Fml := FormulaBlockAfterBranchCalc;
  if Assigned(Fml) then
    Result := Fml.Calculate([])
  else
    Result := True;

  V := Result;
  ExecuteEventsHandler('OnFinishBranch', [Integer(Addr(V))]);
  Result := V;

  if Result then
  begin
    // Check inner master-details
    for i := 0 to ItemCount - 1 do
      if Items[i] is TrmXMLanyElement then
      begin
        Node := TrmXMLanyElement(Items[i]);
        if Node.BlockParentIfEmpty and Node.Enabled and (Node.RendStatus <> rmXMLRSCalculated) then
        begin
          EraseBranchData;
          Result := False;
          break;
        end;
      end;
  end
  else
    EraseBranchData;
end;

procedure TrmXMLanyElement.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
var
  i, NewInd, bInd, eInd: Integer;
  Rec: TrmXMLNode;
begin
  if Parent = nil then
    Exit;

  if Parent is TrmXMLElement then
    bInd := TrmXMLElement(Parent).FLastAttributeIndex + 1
  else
    bInd := 0;
  eInd := Parent.ItemCount - 1;

  if ATop < Parent.TreeBranchRect.Top then
    NewInd := bInd

  else if ATop > Parent.TreeBranchRect.Bottom then
    NewInd := eInd

  else
  begin
    NewInd := ItemIndex;

    for i := bInd to eInd do
    begin
      Rec := TrmXMLNode(Parent.Items[i]);
      if (Rec.Top + Rec.Height > ATop) and (Rec.Top + Rec.Height < ATop + Height) then
      begin
        NewInd := Rec.ItemIndex;
        if ATop < Top then
          Inc(NewInd);
        break;
      end

      else if (Rec.Top > ATop) and (Rec.Top < ATop + Height) then
      begin
        NewInd := Rec.ItemIndex;
        if ATop > Top then
          Dec(NewInd);
        break;
      end

      else if Rec.Top + Rec.Height < ATop then
        NewInd := Rec.ItemIndex + 1

      else if Rec.Top > ATop + Height then
        NewInd := Rec.ItemIndex;
    end;
  end;

  if NewInd > eInd then
    NewInd := eInd;

  ItemIndex := NewInd;
end;

function TrmXMLanyElement.BeforeCalculate: Boolean;
begin
  Result := inherited BeforeCalculate;
  if Result then
    FLastDocPosition := Renderer.LastItemPos
  else
    FLastDocPosition := -1;        
end;

function TrmXMLanyElement.BeforeCalculateBranch: Boolean;
var
  Fml: TrmFMLFormula;
  V: Variant;
begin
  FBranchBegDocPosition := Renderer.LastItemPos;

  Fml := FormulaBlockBeforeBranchCalc;
  if Assigned(Fml) then
    Result := Fml.Calculate([])
  else
    Result := True;

  V := Result;
  ExecuteEventsHandler('OnStartBranch', [Integer(Addr(V))]);
  Result := V;
end;

procedure TrmXMLanyElement.CalculateBranch;
begin
  if Enabled and (RendStatus = rmXMLRSNone) then
  begin
    if BeforeCalculateBranch then
    begin
      DoCalculateBranch;
      AfterCalculateBranch;
    end;
  end;
end;

procedure TrmXMLanyElement.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('NodeClosed', ReadNodeClosed, WriteNodeClosed, FNodeClosed);
end;

function TrmXMLanyElement.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.MovingDirections := [rwDBIMDTop, rwDBIMDBottom];
end;

procedure TrmXMLanyElement.DoAssignScreenPosition;
var
  i: Integer;
  R: TRect;
begin
  Height := cItemHeight;
  Width := cItemHeight * 2;

  i := GetItemLevel;
  if i = 0 then
    Left := cFirstLevelOffset
  else
    Left := cLevelOffset;

  i := ItemIndex;
  if i = 0 then
  begin
    if Parent is TrmXMLForm then
      Top := cFirstLevelOffset
    else
      Top := Parent.Height + cLevelOffset div 2;
  end
  else
  begin
    R := Parent.Items[i - 1].TreeBranchRect;
    Top := Parent.Items[i - 1].Top + R.Bottom + cLevelOffset div 2;
  end;

  inherited;
end;

procedure TrmXMLanyElement.DoCalculateBranch;
begin
  Calculate;
end;

procedure TrmXMLanyElement.EraseBranchData;
begin
  if FBranchBegDocPosition <> -1 then
  begin
    Renderer.CutOffUpToPos(FBranchBegDocPosition);
    FBranchBegDocPosition := -1;
    FLastDocPosition := -1;
  end;

  RendStatus := rmXMLRSNone;  
end;

procedure TrmXMLanyElement.EraseItemData;
begin
  inherited;
  if FLastDocPosition <> -1 then
  begin
    Renderer.CutOffUpToPos(FLastDocPosition);
    FLastDocPosition := -1;
  end;
end;

function TrmXMLanyElement.FormulaBlockAfterBranchCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockAfterBranchCalc');
end;

function TrmXMLanyElement.FormulaBlockBeforeBranchCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockBeforeBranchCalc');
end;

function TrmXMLanyElement.IsTreeButtonVisible: Boolean;
var
  i: Integer;
begin
  Result := False;

  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLanyElement then
    begin
      Result := True;
      break;
    end;
end;

procedure TrmXMLanyElement.ReadNodeClosed(Reader: TReader);
begin
  FNodeClosed := Reader.ReadBoolean;
end;

procedure TrmXMLanyElement.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnStartBranch, cEventOnFinishBranch]);
end;

procedure TrmXMLanyElement.WriteNodeClosed(Writer: TWriter);
begin
  Writer.WriteBoolean(FNodeClosed);
end;

{ TrmXMLSimpleTypeByUnion }

function TrmXMLSimpleTypeByUnion.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if Cl.InheritsFrom(TrmXMLItem) and not Cl.InheritsFrom(TrmXMLSimpleType) then
  begin
    if  Parent <> nil then
      Result := Parent.CreateChildObject(AClassName);
    Exit;
  end;

  Result := inherited CreateChildObject(AClassName);
end;

function TrmXMLSimpleTypeByUnion.GetText: String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ItemCount - 1 do
    Result := Result + (Items[i] as TrmXMLSimpleType).GetText;

  DoOnFilterText(Result);
end;

function TrmXMLSimpleTypeByUnion.GetValue: Variant;
begin
  Result := GetText;
end;


{ TrmXMLItemizedSimpleType }

function TrmXMLItemizedSimpleType.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.Selectable := (FMousePosition = rmdMPClient) and Result.Selectable;
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight];
  Result.AcceptControls := True;
end;

procedure TrmXMLItemizedSimpleType.DoAssignScreenPosition;
var
  i, h: Integer;
begin
  if LibDesignMode then
  begin
    Left := cFirstLevelOffset;
    Top := cFirstLevelOffset;
  end
  else
    inherited;

  FChidItemsOffset.Y := cItemLeftOffset;
  FChidItemsOffset.X := cItemLeftOffset;
  Height := Height + cItemLeftOffset * 2;

  h := 0;
  for i := 0 to ItemCount - 1 do
  begin
    Items[i].AssignScreenPosition;
    if h < Items[i].Height then
      h := Items[i].Height;
  end;

  if h = 0 then
    Height := cItemHeight
  else
    Height := h + FChidItemsOffset.Y * 2;

  // allign items vertically
  for i := 0 to ItemCount - 1 do
    Items[i].FTop := (Height - Items[i].Height) div 2;

  if ItemCount = 0 then
    Width := cItemHeight
  else
    Width := Items[ItemCount - 1].Left + Items[ItemCount - 1].Width + cItemLeftOffset;

  if Parent <> nil then
    if Parent is TrmXMLelement then
      Top := (Parent.Height - Height) div 2
    else
      Top := Parent.FChidItemsOffset.Y;
end;

procedure TrmXMLItemizedSimpleType.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;
    Pen.Color := clGray;
    Pen.Mode := pmCopy;
    Pen.Style := psDot;
    Brush.Style := bsClear;
    Rectangle(R);
  end;
end;

procedure TrmXMLItemizedSimpleType.SetValue(const AValue: Variant);
begin
  //nothing
end;

{ TrmXMLattributeGroup }

function TrmXMLattributeGroup.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if Cl.InheritsFrom(TrmXMLItem) and not Cl.InheritsFrom(TrmXMLanyAttribute) then
  begin
    if Parent <> nil then
      Result := Parent.CreateChildObject(AClassName);
    Exit;
  end;

  Result := inherited CreateChildObject(AClassName);
end;

procedure TrmXMLattributeGroup.DoAssignScreenPosition;
var
  i, h, l: Integer;
begin
  if LibDesignMode then
  begin
    Left := cFirstLevelOffset;
    Top  := cFirstLevelOffset;
  end

  else
  begin
    i := ItemIndex;
    if i = 0 then
      l := Parent.FChidItemsOffset.X
    else
      l := Parent.Items[i - 1].Left + Parent.Items[i - 1].Width;

    if Parent is TrmXMLelement then
      Inc(l, cItemLeftOffset);

    Left := l;
  end;

  FChidItemsOffset.Y := cItemLeftOffset;
  FChidItemsOffset.X := 0;
  Height := Height + cItemLeftOffset * 2;

  h := 0;
  for i := 0 to ItemCount - 1 do
  begin
    Items[i].AssignScreenPosition;
    Items[i].FLeft := Items[i].Left + cItemLeftOffset;
    if h < Items[i].Height then
      h := Items[i].Height;
  end;

  if h = 0 then
    Height := cItemHeight
  else
    Height := h + FChidItemsOffset.Y * 2;

  if Parent <> nil then
    if Parent is TrmXMLelement then
      Top := (Parent.Height - Height) div 2
    else
      Top := Parent.FChidItemsOffset.Y;

  // allign items vertically
  for i := 0 to ItemCount - 1 do
    Items[i].FTop := (Height - Items[i].Height) div 2;

  if ItemCount = 0 then
    Width := cItemHeight
  else
    Width := Items[ItemCount - 1].Left + Items[ItemCount - 1].Width + cItemLeftOffset;
end;

function TrmXMLattributeGroup.GetXMLDocNode: TxmlDocNodeHandle;
begin
  if Parent <> nil then
    Result := Parent.XMLDocNode
  else
    Result := 0;
end;

procedure TrmXMLattributeGroup.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;
    Pen.Color := clGray;
    Pen.Mode := pmCopy;
    Pen.Style := psDot;
    Brush.Style := bsClear;
    Rectangle(R);
  end;
end;

procedure TrmXMLattributeGroup.SetXMLDocNode(AValue: TxmlDocNodeHandle);
begin
  // nothing
end;

{ TrmXMLQuery }

constructor TrmXMLQuery.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FNode := TrmXMLQueryElement(AOwner);
  Name := 'Query';
  FNode.FQuery := Self;
  ParamsFromVars := True;
  FAllowToNotifyFormulas := False;
end;

destructor TrmXMLQuery.Destroy;
begin
  if Assigned(FNode) then
    FNode.FQuery := nil;
  inherited;
end;

procedure TrmXMLQuery.OnAssignQBInfo(Sender: TObject);
begin
  inherited;
  FNode.SyncMasterQueryLink;
  if FNode.IsDesignMode then
  begin
    FNode.GetDesigner.Notify(rmdQueryChanged, Integer(Pointer(Self as IrmDesignObject)));
    FNode.Invalidate;
  end;
end;

{ TrmXMLsequence }

function TrmXMLsequence.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if not Cl.InheritsFrom(TrmXMLanyElement) then
    rwError('XML item cannot be created in here');

  Result := inherited CreateChildObject(AClassName);
end;

function TrmXMLsequence.GetShowText: String;
begin
  Result := Name;
end;

function TrmXMLsequence.GetXMLDocNode: TxmlDocNodeHandle;
begin
  Result := Parent.XMLDocNode;
end;

procedure TrmXMLsequence.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := clGreen;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);

  PaintTreeLines(ACanvas);
end;

procedure TrmXMLsequence.SetXMLDocNode(AValue: TxmlDocNodeHandle);
begin
  // nothing
end;


{ TrmXMLQueryElement }

function TrmXMLQueryElement.AfterCalculateBranch: Boolean;
begin
  if FQueryOpenedInternally then
    FQuery.Close;

  Result := inherited AfterCalculateBranch;
end;

function TrmXMLQueryElement.BeforeCalculateBranch: Boolean;
begin
  FQueryOpenedInternally := False;

  Result := inherited BeforeCalculateBranch;

  if Result then
  begin
    if FQuery.PActive then
      FQuery.PFirst
    else if FQuery.SQL.Count > 0 then
    begin
      FQueryOpenedInternally := True;
      FQuery.Open;
    end;

    NotifyAggregateFunctions;
  end;
end;

constructor TrmXMLQueryElement.Create(AOwner: TComponent);
begin
  inherited;
  FQuery := TrmXMLQuery.Create(Self);
end;

destructor TrmXMLQueryElement.Destroy;
begin
  FreeAndNil(FQuery);
  inherited;
end;

procedure TrmXMLQueryElement.DoCalculateBranch;
begin
  if FQuery.PActive then
  begin
    while not FQuery.PEof do
    begin
      inherited;
      FQuery.PNext;
      if not FQuery.PEof then
        SetRendStatusChildrenToo(rmXMLRSNone);
    end;
  end

  else
    inherited;
end;

function TrmXMLQueryElement.GetDataSource: TrwDataSet;
begin
  Result := FQuery;
end;

function TrmXMLQueryElement.GetMasterFields: TrwStrings;
begin
  Result := FQuery.MasterFields;
end;

function TrmXMLQueryElement.GetParamsFromVars: Boolean;
begin
  Result := FQuery.ParamsFromVars;
end;

function TrmXMLQueryElement.GetQuery: IrmQuery;
begin
  Result := FQuery;
end;

function TrmXMLQueryElement.GetQueryBuilderInfo: TrwQBQuery;
begin
  Result := FQuery.QueryBuilderInfo;
end;

procedure TrmXMLQueryElement.RWLoaded;
begin
  inherited;
  FQuery.SQL.Text := QueryBuilderInfo.SQL;
  SyncMasterQueryLink;
end;

procedure TrmXMLQueryElement.SetMasterFields(const Value: TrwStrings);
begin
  FQuery.MasterFields := Value;
end;

procedure TrmXMLQueryElement.SetParamsFromVars(const Value: Boolean);
begin
  FQuery.ParamsFromVars := Value;
end;

procedure TrmXMLQueryElement.SetQueryBuilderInfo(const Value: TrwQBQuery);
begin
  FQuery.QueryBuilderInfo.Assign(Value);
end;

procedure TrmXMLQueryElement.SyncMasterQueryLink;
var
  i: Integer;
  qNotEmpt: Boolean;
begin
  qNotEmpt := QueryBuilderInfo.SelectedTables.Count <> 0;

  if (Owner is TrmXMLsequence) and (TrmXMLsequence(Owner).QueryBuilderInfo.SelectedTables.Count <> 0) and qNotEmpt then
    FQuery.MasterDataSource := TrmXMLsequence(Owner).FQuery
  else
    FQuery.MasterDataSource := nil;

  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmXMLsequence then
      if qNotEmpt and (TrmXMLsequence(Items[i]).QueryBuilderInfo.SelectedTables.Count <> 0) then
        TrmXMLsequence(Items[i]).FQuery.MasterDataSource := FQuery
      else
        TrmXMLsequence(Items[i]).FQuery.MasterDataSource := nil;
end;


{ TrmXMLchoice }

function TrmXMLchoice.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if not Cl.InheritsFrom(TrmXMLanyElement) then
    rwError('XML item cannot be created in here');

  Result := inherited CreateChildObject(AClassName);
end;

procedure TrmXMLchoice.DoCalculate;
//var
//  i, ItemCalcCount: Integer;
begin
  inherited;
{
  ItemCalcCount := 0;

  for i := 0 to ItemCount - 1 do
    if Items[i].RendStatus = rmXMLRSCalculated then
      Inc(ItemCalcCount);

  if ItemCalcCount > 1 then
    rwError('Choice element "' + Name +'" has produced more than one item. Please check choice logic.');
}
end;

function TrmXMLchoice.GetShowText: String;
begin
  Result := Name;
end;

function TrmXMLchoice.GetXMLDocNode: TxmlDocNodeHandle;
begin
  Result := Parent.XMLDocNode;
end;

procedure TrmXMLchoice.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := clBlue;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);

  PaintTreeLines(ACanvas);
end;

procedure TrmXMLchoice.SetXMLDocNode(AValue: TxmlDocNodeHandle);
begin
  // nothing
end;

{ TrmXMLgroup }

function TrmXMLgroup.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  Cl: TClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName).GetVCLClass;

  if not Cl.InheritsFrom(TrmXMLanyElement) then
    rwError('XML item cannot be created in here');

  Result := inherited CreateChildObject(AClassName);
end;

function TrmXMLgroup.GetShowText: String;
begin
  Result := 'Group';
end;

function TrmXMLgroup.GetXMLDocNode: TxmlDocNodeHandle;
begin
  Result := Parent.XMLDocNode;
end;

procedure TrmXMLgroup.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := clSilver;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);

  PaintTreeLines(ACanvas);
end;

procedure TrmXMLgroup.SetXMLDocNode(AValue: TxmlDocNodeHandle);
begin
  // nothing
end;


{ TrmXMLSchema }

function TrmXMLSchema.AsAncestor(const AncestorItem: TrwCollectionItem): Boolean;
begin
  Result := True;
end;

procedure TrmXMLSchema.Assign(Source: TPersistent);
begin
  inherited;
  Name := TrmXMLSchema(Source).Name;
  Description := TrmXMLSchema(Source).Description;
  Content := TrmXMLSchema(Source).Content;
end;

procedure TrmXMLSchema.CreateTmpFile;
begin
  if FSchemaFileName = '' then
  begin
    FSchemaFileName := GetThreadTmpDir + ChangeFileExt(Name, '.xsd');
    SaveToFile(FSchemaFileName);
  end;
end;

procedure TrmXMLSchema.DeleteTmpFile;
begin
  if FSchemaFileName <> '' then
  begin
    DeleteFile(FSchemaFileName);
    FSchemaFileName := '';
  end;
end;

destructor TrmXMLSchema.Destroy;
begin
  DeleteTmpFile;
  inherited;
end;

function TrmXMLSchema.GetDescription: string;
begin
  Result := Description;
end;

function TrmXMLSchema.GetName: string;
begin
  Result := Name;
end;

procedure TrmXMLSchema.LoadFromFile(const AFileName: TFileName);
var
  FS: IevDualStream;
begin
  FS := TEvDualStreamHolder.CreateFromFile(AFileName);
  SetLength(FContent, FS.Size);
  FS.ReadBuffer(FContent[1], FS.Size);
end;

procedure TrmXMLSchema.SaveToFile(const AFileName: TFileName);
var
  FS: IevDualStream;
begin
  FS := TEvDualStreamHolder.CreateFromNewFile(AFileName);
  FS.WriteBuffer(FContent[1], Length(FContent));
end;


procedure TrmXMLSchema.SetDescription(const AValue: string);
begin
  Description := AValue;
end;

procedure TrmXMLSchema.SetName(const AValue: string);
begin
  Name := AValue;
end;

{ TrmXMLSchemas }

procedure TrmXMLSchemas.BeginWorkWithItems;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].CreateTmpFile;
end;

procedure TrmXMLSchemas.EndWorkWithItems;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].DeleteTmpFile;
end;

function TrmXMLSchemas.FindSchema(const AName: String): TrmXMLSchema;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if SameText(Items[i].Name, AName) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TrmXMLSchemas.GetItem(Index: Integer): TrmXMLSchema;
begin
  Result := TrmXMLSchema(inherited Items[Index]);
end;

function TrmXMLSchemas.GetSchemaByName(const ASchemaName: String): IrmXMLSchema;
begin
  Result := FindSchema(ASchemaName);
end;

{ TrmXMLanyAttribute }

function TrmXMLanyAttribute.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.Selectable := (FMousePosition = rmdMPClient) and Result.Selectable;
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight];
end;


{ TrmdXMLNodeButton }

function TrmdXMLNodeButton.GetShapeRegion: HRGN;
var
  R: TRect;
begin
  if (Owner as TrmXMLNode).DoNotPaint then
    Result := 0
  else
  begin
    R.TopLeft := (Owner as TrmXMLNode).ClientToScreen(Point(0, 0));
    R.TopLeft := Parent.ScreenToClient(R.TopLeft);
    R.Top := R.Top + Zoomed(Owner.Height - 6);
    R.Left := R.Left + Zoomed(cItemHeight div 2 - 6);
    R.Right   := R.Left + Zoomed(11);
    R.Bottom  := R.Top + Zoomed(11);
    Result := CreateRectRgnIndirect(R);    
  end;
end;

procedure TrmdXMLNodeButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Owner as TrmXMLNode).FNodeClosed then
    (Owner as TrmXMLNode).OpenNode
  else
    (Owner as TrmXMLNode).CloseNode;
end;

procedure TrmdXMLNodeButton.PaintToCanvas(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  if Shape <> 0 then
    with ACanvas do
    begin
      Brush.Color := clWhite;
      Brush.Style := bsSolid;
      FillRgn(Handle, Shape, Brush.Handle);
      Brush.Style := bsClear;

      GetRgnBox(Shape, R);
      Pen.Mode := pmCopy;
      Pen.Style := psSolid;

      if (Owner as TrmXMLNode).Enabled then
        Pen.Color := clBlack
      else
        Pen.Color := clGray;

      Pen.Width := 1;
      Rectangle(R);

      MoveTo(R.Left + 2, R.Top + (R.Bottom - R.Top) div 2);
      LineTo(R.Right - 2, R.Top + (R.Bottom - R.Top) div 2);

      if (Owner as TrmXMLNode).FNodeClosed then
      begin
        MoveTo(R.Left + (R.Right - R.Left) div 2, R.Top + 2);
        LineTo(R.Left + (R.Right - R.Left) div 2, R.Bottom - 2);
      end;
    end;
end;

{ TrmXMLfragment }

procedure TrmXMLfragment.AfterConstruction;
begin
  inherited;
  FNSName := 'XML Fragment';
end;

function TrmXMLfragment.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := False;
end;

procedure TrmXMLfragment.DoCalculate;
var
  S: String;
begin
  if not VarIsNull(SimpleContent.Value) then
  begin
    S := SimpleContent.GetText;
    S := GetNextStrValue(S, '<?');
    S := GetNextStrValue(S, '?>');
    Renderer.AddFragment(S);
  end;
end;

procedure TrmXMLfragment.InitializeForDesign;
var
  Obj: TrmXMLFragmentValue;
begin
  inherited;
  Obj := TrmXMLFragmentValue.Create(Self);
  AddItem(Obj);
  Obj.InitializeForDesign;
end;

procedure TrmXMLfragment.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := BoundsRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;

    if Enabled then
      Pen.Color := clBlack
    else
      Pen.Color := clGray;

    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := cXMLSimpleTypeColor;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;

  DrawItemText(ACanvas, GetShowText);

  PaintTreeLines(ACanvas);
end;

procedure TrmXMLfragment.SetNSName(const AValue: String);
begin
// empty
end;

{ TrmXMLFragmentValue }

function TrmXMLFragmentValue.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.StandAloneObject := False;
end;

end.
