unit rwQBExprPanelFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, stdctrls, rwQBInfo, Menus, Clipbrd, rwCustomDataDictionary, rwUtils;

type
  TrwQBExprItemLabel = class;

  TrwQBExprPanel = class(TFrame)
    pnlCanvas: TPanel;
    pmClipbrd: TPopupMenu;
    miCopy: TMenuItem;
    miPaste: TMenuItem;
    procedure pnlCanvasClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure pnlCanvasDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure miCopyClick(Sender: TObject);
    procedure miPasteClick(Sender: TObject);
    procedure pmClipbrdPopup(Sender: TObject);
  private
    FBlinkTimer: TTimer;
    FExpression: TrwQBExpression;
    FSelectedItem: TrwQBExprItemLabel;
    FSelectedItemPair: TrwQBExprItemLabel;
    FOnSelectItem: TNotifyEvent;
    FReadOnly: Boolean;
    FRedrawing: Boolean;
    FCurDragOverItem: TrwQBExprItemLabel;
    procedure SetExpression(const Value: TrwQBExpression);
    procedure ItemMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ItemMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ItemEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure ItemCancelDrag(Sender, Target: TObject; X, Y: Integer);
    procedure ItemDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN;
    procedure SetSelectedItem(const Value: TrwQBExprItemLabel);
    procedure SetReadOnly(const Value: Boolean);
    procedure OnBlinkTimer(Sender: TObject);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure ReDraw;
    function  FindPairBracket(AItemIndex: Integer): TrwQBExprItemLabel;
    function  FindFunction(AItemIndex: Integer): TrwQBExprItemLabel;
    function  ItemByData(AExprItem: TrwQBExpressionItem): TrwQBExprItemLabel;
    property  Expression: TrwQBExpression read FExpression write SetExpression;
    property  SelectedItem: TrwQBExprItemLabel read FSelectedItem write SetSelectedItem;
    property  OnSelectItem: TNotifyEvent read FOnSelectItem write FOnSelectItem;
    property  ReadOnly: Boolean read FReadOnly write SetReadOnly;
  end;


  TrwQBExprItemLabel = class(TLabel)
  private
    FExprItem: TrwQBExpressionItem;
    procedure SetExprItem(const Value: TrwQBExpressionItem);
    function  GetSelected: Boolean;
    procedure SetSelected(const Value: Boolean);
  protected
    function IsWizardView: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure ReDraw;

    property ExprItem: TrwQBExpressionItem read FExprItem write SetExprItem;
    property Selected: Boolean read GetSelected write SetSelected;
  end;


implementation

{$R *.DFM}

uses rwQueryBuilderFrm;


{ TrwQBExprPanel }


procedure TrwQBExprPanel.ItemMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not FReadOnly and (Button = mbLeft) then
  begin
    FCurDragOverItem := nil;
    SelectedItem := TrwQBExprItemLabel(Sender);
    if Assigned(SelectedItem.ExprItem) then
    begin
      SelectedItem.Selected := True;
      SelectedItem.BeginDrag(False, 1);
    end;
    SetFocus;
  end;
end;


procedure TrwQBExprPanel.ItemMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FReadOnly and (Button = mbLeft) and Assigned(pnlCanvas.OnClick) then
    pnlCanvas.OnClick(pnlCanvas);
end;


procedure TrwQBExprPanel.ReDraw;
var
  i, x, y: Integer;
  ExprItm: TrwQBExpressionItem;

  procedure AddLabel(AItem: TrwQBExpressionItem);
  var
    L: TrwQBExprItemLabel;
  begin
    L := TrwQBExprItemLabel.Create(pnlCanvas);
    L.PopupMenu := PopupMenu;
    L.OnMouseDown := ItemMouseDown;
    L.OnMouseUp := ItemMouseUp;
    if not FReadOnly and Assigned(AItem) and not (AItem.ItemType = eitSeparator) then
      L.OnEndDrag := ItemEndDrag
    else
      L.OnEndDrag := ItemCancelDrag;

    L.OnDragOver := ItemDragOver;

    L.ExprItem := AItem;
    if x + L.Width  > pnlCanvas.ClientWidth then
    begin
      x := 0;
      y := y + 17;
    end;

    L.Left := x;
    L.Top := y;
    x := x + L.Width;

    L.Parent := pnlCanvas;

    if Assigned(ExprItm) and (ExprItm = AItem) then
      FSelectedItem := L;
  end;

begin
  if FRedrawing then
    Exit;

  FRedrawing := True;
  try
    if not FReadOnly then
      FBlinkTimer.Enabled := False;

    if Assigned(SelectedItem) then
      ExprItm := SelectedItem.ExprItem
    else
      ExprItm := nil;

    SelectedItem := nil;
    pnlCanvas.DestroyComponents;

    if not Assigned(FExpression) then
      Exit;

    x := 0;
    y := 0;
    for i := 0 to FExpression.Count - 1 do
    begin
      if not FReadOnly then
        AddLabel(nil);
      AddLabel(TrwQBExpressionItem(FExpression.Items[i]));
    end;

    if not FReadOnly then
      AddLabel(nil);

    SetSelectedItem(FSelectedItem);

    if not FReadOnly then
      FBlinkTimer.Enabled := True;

  finally
    FRedrawing := False;
  end;
end;


procedure TrwQBExprPanel.SetExpression(const Value: TrwQBExpression);
begin
  FExpression := Value;
  ReDraw;
end;


procedure TrwQBExprPanel.SetSelectedItem(const Value: TrwQBExprItemLabel);
begin
  if not FReadOnly then
    FBlinkTimer.Enabled := False;

  if Assigned(FSelectedItem) then
    FSelectedItem.Selected := False;

  if Assigned(FSelectedItemPair) then
  begin
    FSelectedItemPair.Selected := False;
    FSelectedItemPair := nil;
  end;

  FSelectedItem := Value;
  if Assigned(FSelectedItem) then
  begin
    FSelectedItem.Selected := True;
    if Assigned(FSelectedItem.ExprItem) and (FSelectedItem.ExprItem.ItemType in [eitBracket, eitFunction]) then
    begin
      FSelectedItemPair := FindPairBracket(FSelectedItem.ComponentIndex);
      if Assigned(FSelectedItemPair) then
        FSelectedItemPair.Selected := True;
    end;
  end;

  if Assigned(FOnSelectItem) then
    FOnSelectItem(Self);

  if not FReadOnly then
    FBlinkTimer.Enabled := True;
end;



procedure TrwQBExprPanel.CNKeyDown(var Message: TWMKeyDown);
begin
  if not FReadOnly and Assigned(FSelectedItem) then
    case Message.CharCode of
      VK_LEFT:
        begin
          if SelectedItem.ComponentIndex > 0 then
            SelectedItem := TrwQBExprItemLabel(pnlCanvas.Components[SelectedItem.ComponentIndex - 1]);
        end;

      VK_RIGHT:
        begin
          if SelectedItem.ComponentIndex < pnlCanvas.ComponentCount - 1 then
            SelectedItem := TrwQBExprItemLabel(pnlCanvas.Components[SelectedItem.ComponentIndex + 1]);
        end;
    end;

  inherited;
end;

procedure TrwQBExprPanel.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
  if FReadOnly and Assigned(FBlinkTimer) then
    FreeAndNil(FBlinkTimer)

  else if not FReadOnly and not Assigned(FBlinkTimer) then
  begin
    FBlinkTimer := TTimer.Create(nil);
    FBlinkTimer.OnTimer := OnBlinkTimer;
    FBlinkTimer.Interval := GetCaretBlinkTime;
  end;

  ReDraw;
end;

constructor TrwQBExprPanel.Create(AOwner: TComponent);
begin
  inherited;
  FReadOnly := True;
  FBlinkTimer := nil;
end;


destructor TrwQBExprPanel.Destroy;
begin
  if Assigned(FBlinkTimer) then
  begin
    FBlinkTimer.Enabled := False;
    FreeAndNil(FBlinkTimer);
  end;
  inherited;
end;

procedure TrwQBExprPanel.OnBlinkTimer(Sender: TObject);
begin
  if not HandleAllocated or SelectedItem.Dragging then
    Exit;

  if Assigned(FSelectedItem) then
    FSelectedItem.Selected := not FSelectedItem.Selected;
  if Assigned(FSelectedItemPair) then
    FSelectedItemPair.Selected := not FSelectedItemPair.Selected;
end;


function TrwQBExprPanel.FindPairBracket(AItemIndex: Integer): TrwQBExprItemLabel;
var
  i, lLimit,
  ADirect,
  lBrCount: Integer;
  L: TrwQBExprItemLabel;
  LeftBracket, RightBracket: String;
  BracketType: Integer;

  function BracketTypeOf(const ABracket: String): Integer;
  begin
    if (ABracket = '[') or (ABracket = ']') then
      Result := 2
    else
      Result := 1;
  end;

begin
  Result := nil;
  L := TrwQBExprItemLabel(pnlCanvas.Components[AItemIndex]);

  if (L.Caption = '[') or (L.Caption = ']') then
  begin
    LeftBracket := '[';
    RightBracket := ']';
  end
  else
  begin
    LeftBracket := '(';
    RightBracket := ')';
  end;

  BracketType := BracketTypeOf(L.Caption);


  if (L.Caption = LeftBracket) or ((BracketType = 1) and Assigned(L.ExprItem) and (L.ExprItem.ItemType = eitFunction)) then
  begin
    ADirect := 1;
    lLimit := pnlCanvas.ComponentCount - 1;
  end

  else if L.Caption = RightBracket then
  begin
    ADirect := -1;
    lLimit := 0;
  end

  else
    Exit;

  lBrCount := 1;
  i := AItemIndex + ADirect;
  while i <> lLimit do
  begin
    L := TrwQBExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and
      ((BracketType = 1) and ((L.ExprItem.ItemType = eitFunction) or (L.ExprItem.ItemType = eitBracket) and (BracketTypeOf(L.Caption) = BracketType)) or
       (BracketType = 2) and (L.ExprItem.ItemType = eitBracket) and (BracketTypeOf(L.Caption) = BracketType)) then
    begin
      if (L.Caption = LeftBracket) or ((BracketType = 1) and Assigned(L.ExprItem) and (L.ExprItem.ItemType = eitFunction)) then
        Inc(lBrCount, ADirect)
      else
        Dec(lBrCount, ADirect);

      if lBrCount = 0 then
      begin
        Result := L;
        break;
      end;
    end;

    Inc(i, ADirect);
  end;
end;

procedure TrwQBExprPanel.pnlCanvasClick(Sender: TObject);
begin
  if pnlCanvas.ComponentCount > 0 then
    SelectedItem := TrwQBExprItemLabel(pnlCanvas.Components[pnlCanvas.ComponentCount - 1]);

  SetFocus;
end;

procedure TrwQBExprPanel.FrameResize(Sender: TObject);
begin
  ReDraw;
end;

procedure TrwQBExprPanel.ItemEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  i, j: Integer;
  ExprItm, PairItem: TrwQBExpressionItem;
  L: TrwQBExprItemLabel;
begin
  SelectedItem.EndDrag(True);

  FCurDragOverItem := nil;
  ExprItm := TrwQBExprItemLabel(Sender).ExprItem;

  if Target is TrwQBExprItemLabel then
    if Assigned(TrwQBExprItemLabel(Target).ExprItem) then
      i := TrwQBExprItemLabel(Target).ExprItem.Index
    else
      if TrwQBExprItemLabel(Target).ComponentIndex = pnlCanvas.ComponentCount - 1 then
        i := Expression.Count - 1
      else
      begin
        i := TrwQBExprItemLabel(pnlCanvas.Components[TrwQBExprItemLabel(Target).ComponentIndex + 1]).ExprItem.Index;
        if i > ExprItm.Index then
          Dec(i);
        if i = Expression.Count - 1 then
          Dec(i);
      end

  else if Target = pnlCanvas then
    i := Expression.Count - 1

  else
    Exit;

  j := ExprItm.Index;
  PairItem := nil;
  if ExprItm.ItemType in [eitBracket, eitFunction] then
  begin
    L := FindPairBracket(SelectedItem.ComponentIndex);
    if Assigned(L) then
      PairItem := L.ExprItem;
  end;

  ExprItm.Index := i;
  ReDraw;

  if ExprItm.ItemType in [eitBracket, eitFunction] then
  begin
    if not Assigned(FindPairBracket(SelectedItem.ComponentIndex)) then
    begin
      ExprItm.Index := j;
      ReDraw;
    end;

    if Assigned(PairItem) then
    begin
      L := ItemByData(PairItem);
      if not Assigned(FindPairBracket(L.ComponentIndex)) then
      begin
        ExprItm.Index := j;
        ReDraw;
      end;
    end;
  end;

  SetFocus;
end;


procedure TrwQBExprPanel.ItemDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if Source is TrwQBExprItemLabel then
  begin
    Accept := True;
    if Assigned(FCurDragOverItem) then
    begin
      FCurDragOverItem.Selected := False;
      FCurDragOverItem := nil;
    end;

    if Sender <> Source then
    begin
      FCurDragOverItem := TrwQBExprItemLabel(Sender);
      FCurDragOverItem.Selected := True;
    end;
  end;
end;


procedure TrwQBExprPanel.pnlCanvasDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if Source is TrwQBExprItemLabel then
  begin
    if Assigned(FCurDragOverItem) then
    begin
      FCurDragOverItem.Selected := False;
      FCurDragOverItem := nil;
    end;
    Accept := True;
  end
  else
    Accept := False;
end;



function TrwQBExprPanel.FindFunction(AItemIndex: Integer): TrwQBExprItemLabel;
var
  i, lBrCount: Integer;
  L: TrwQBExprItemLabel;
begin
  Result := nil;

  if TrwQBExprItemLabel(pnlCanvas.Components[AItemIndex]).Caption <> ',' then
    Exit;

  lBrCount := 0;
  i := AItemIndex - 1;
  while i > 0 do
  begin
    L := TrwQBExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and (L.ExprItem.ItemType = eitBracket) then
    begin
      if L.Caption = ')' then
        Dec(lBrCount)
      else
        Inc(lBrCount);
    end

    else if Assigned(L.ExprItem) and (L.ExprItem.ItemType = eitFunction) and (lBrCount = 0) then
    begin
      Result := L;
      break;
    end;

    Dec(i);
  end;
end;



procedure TrwQBExprPanel.ItemCancelDrag(Sender, Target: TObject; X, Y: Integer);
begin
  if Assigned(FCurDragOverItem) then
  begin
    FCurDragOverItem.Selected := False;
    FCurDragOverItem := nil;
    CancelDrag;
  end;
end;

function TrwQBExprPanel.ItemByData(AExprItem: TrwQBExpressionItem): TrwQBExprItemLabel;
var
  i: Integer;
  L: TrwQBExprItemLabel;
begin
  Result := nil;
  for i := 0 to pnlCanvas.ComponentCount - 1 do
  begin
    L := TrwQBExprItemLabel(pnlCanvas.Components[i]);
    if Assigned(L.ExprItem) and (L.ExprItem = AExprItem) then
    begin
      Result := L;
      break;
    end;
  end;
end;



{ TrwQBExprItemLabel }

constructor TrwQBExprItemLabel.Create(AOwner: TComponent);
begin
  inherited;
  ShowAccelChar := False;
  DragMode := dmManual;
end;

function TrwQBExprItemLabel.GetSelected: Boolean;
begin
  Result := (Color <> clWhite);
end;


function TrwQBExprItemLabel.IsWizardView: Boolean;
begin
  Result := FQBFrm.WizardView;
end;


procedure TrwQBExprItemLabel.ReDraw;
var
  Tbl: TrwQBSelectedTable;
  Fld: TrwQBShowingField;
  rwF: TDataDictField;
  h: String;

  procedure WrongField;
  begin
    FExprItem.ItemType := eitStrConst;
    FExprItem.Value := 'UNKNOWN FIELD';
    FExprItem.Description := '';
    Redraw;
  end;

begin
  Color := clWhite;
  Font.Name := 'MS Sans Serif';
  Font.Size := 10;
  Font.Style := [];

  if not Assigned(FExprItem) then
  begin
    Caption := ' ';
    exit;
  end;


  case FExprItem.ItemType of
    eitNone:      begin
                    Font.Color := clWhite;
                    Caption := '';
                  end;

    eitBracket,
    eitSeparator: begin
                    Font.Color := clGray;
                    Caption := FExprItem.Value;
                  end;

    eitOperation: begin
                    Font.Style := [fsBold];
                    Font.Color := clMaroon;
                    Caption := FExprItem.Value;
                  end;

    eitField:     begin
                    Font.Color := clBlack;
                    Tbl := FExprItem.GetSelectedTable;
                    if not Assigned(Tbl) then
                    begin
                      WrongField;
                      Exit;
                    end;

                    h := Tbl.GetTableName(IsWizardView);
                    if Tbl.IsSUBQuery then
                    begin
                      Fld := Tbl.SubQuery.ShowingFields.FieldByInternalName(FExprItem.Value);
                      if not Assigned(Fld) then
                      begin
                        WrongField;
                        Exit;
                      end;
                      if Fld.FieldAlias <> '' then
                        Caption := Fld.FieldAlias
                      else
                        Caption := Fld.GetFieldName(IsWizardView);
                    end

                    else
                      if IsWizardView then
                      begin
                        rwF := Tbl.lqObject.Fields.FieldByName(FExprItem.Value);
                        if  not Assigned(rwF) then
                        begin
                          WrongField;
                          Exit;
                        end;

                        Caption := rwF.DisplayName
                      end
                      else
                        Caption := FExprItem.Value;

                    Caption := h + '.' +Caption;
                  end;

    eitFunction: begin
                    Font.Color := clGreen;
                    Caption := FExprItem.Value + '(';
                  end;

    eitParam:     begin
                    Font.Color := clTeal;
                    if FExprItem.IsDescrNotEmpty then
                      Caption := FExprItem.Description
                    else
                      Caption := FExprItem.Value;
                  end;

    eitIntConst,
    eitFloatConst,
    eitDateConst: begin
                    Font.Color := clFuchsia;
                    if FExprItem.IsDescrNotEmpty then
                      Caption := FExprItem.Description
                    else
                      Caption := FExprItem.Value;
                  end;

    eitStrConst:  begin
                    Font.Color := clBlue;
                    if FExprItem.IsDescrNotEmpty then
                      Caption := FExprItem.Description
                    else
                      Caption := '''' + FExprItem.Value + '''';
                  end;

    eitConst:     begin
                    Font.Color := clFuchsia;
                    if FExprItem.IsDescrNotEmpty then
                      Caption := FExprItem.Description
                    else
                      Caption := '@' + FExprItem.Value;
                  end;

    eitNull,
    eitTrue,
    eitFalse:    begin
                    Font.Color := clRed;
                    Caption := FExprItem.Description;
                 end;
  end;
end;


procedure TrwQBExprItemLabel.SetExprItem(const Value: TrwQBExpressionItem);
begin
  FExprItem := Value;
  ReDraw;
end;


procedure TrwQBExprItemLabel.SetSelected(const Value: Boolean);
begin
  if Selected <> Value then
    if Value then
    begin
      Font.Color := clYellow;
      Color := clNavy;
    end
    else
      SetExprItem(ExprItem);
end;


procedure TrwQBExprPanel.miCopyClick(Sender: TObject);
begin
  if Assigned(Expression) then
    PutToClipboard(CF_QBEXPRESSION, Expression.ContentToString);
end;


procedure TrwQBExprPanel.miPasteClick(Sender: TObject);
begin
  Expression.ContentFromString(GetFromClipboard(CF_QBEXPRESSION));
  ReDraw;
end;

procedure TrwQBExprPanel.pmClipbrdPopup(Sender: TObject);
begin
  miPaste.Enabled := not ReadOnly and Clipboard.HasFormat(CF_QBEXPRESSION);
end;

end.
