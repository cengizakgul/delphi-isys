unit rwBasicClasses;

interface

uses
  SysUtils, Classes,  Windows, ExtCtrls, Controls, Graphics, TypInfo, rwTypes, RTLConsts, rwMessages,
  Dialogs, DB, Variants, rwVirtualMachine, EvStreamUtils, rwBasicUtils, rwRTTI, isBasicUtils,
  rwEngineTypes, rwGraphics, Messages, rmTypes, rmFormula, ComCtrls, Forms, rwCustomDataDictionary;

const
  cInheritedKeyWord = 'inherited';

type             
  TrwComponent = class;
  TrwGlobalVarFunct = class;

  TrwComponentClass = class of TrwComponent;

  TrwChangedPropID = (rwCPAll, rwCPInvalidate, rwCPCreation, rwCPDestroy, rwCPPosition, rwCPParent,
                      rwCPColor, rwCPFont, rwCPTransparent, rwCPAlign);

  IrwVisualControl = Interface(IInterface)
  ['{86BF0876-A719-4986-83A2-1BDD2CE12864}']
    function  RealObject: TControl;
    function  Container: TWinControl;
    function  RWObject: TrwComponent;
    procedure PropChanged(APropType: TrwChangedPropID);
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    procedure SetDesigning(Value: Boolean; SetChildren: Boolean = True);
    procedure Notify(AEvent: TrmDesignEventType);
  end;


  {Structure for storing information about default properties by dbl-click in designer}

  TrwDefPropComponent = class(TCollectionItem)
  private
    FCompClassName: string;
    FPropName: string;
  published
    property CompClassName: string read FCompClassName write FCompClassName;
    property PropName: string read FPropName write FPropName;
  end;


  TrwDefPropComponents = class(TCollection)
  private
    function  GetItem(Index: Integer): TrwDefPropComponent;
    procedure SetItem(Index: Integer; Value: TrwDefPropComponent);

  public
    procedure Initialize;
    function Add(AClassName: String; APropName: String): TrwDefPropComponent;
    function IndexOf(AName: String): Integer;
    property Items[Index: Integer]: TrwDefPropComponent read GetItem write SetItem; default;
  end;



       { Event of rwComponent}

  TrwFuncParam = class(TInterfacedCollectionItem)
  private
    FDescriptor: TrwVariable;
    FDomain: TrwDomain;
    FDefaultValue: Variant;
    FDisplayName: String;
    procedure SetDomain(const AValue: TrwDomain);
    function  GetDescriptor: PTrwVariable;
    procedure WriteDomain(Writer: TWriter);
    procedure ReadDomain(Reader: TReader);
    procedure WriteDefaultValue(Writer: TWriter);
    procedure ReadDefaultValue(Reader: TReader);
    function  GetName: String;
    function  GetVarType: TrwVarTypeKind;
    procedure SetName(const AValue: String);
    procedure SetVarType(const AValue: TrwVarTypeKind);
    function  DisplayNameIsNotEmpty: Boolean;
  protected
    procedure DefineProperties(Filer: TFiler); override;
    function  GetDisplayName: string; override;
    procedure SetDisplayName(const Value: string); override;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(ASource: TPersistent); override;
    function DomainValueToStr(const AValue: Variant): String;
    function StrToDomainValue(const AValue: String): Variant;
    property Descriptor: PTrwVariable read GetDescriptor;
    property Domain: TrwDomain read FDomain write SetDomain;
    property DefaultValue: Variant read FDefaultValue write FDefaultValue;
  published
    property Name: String read GetName write SetName;
    property DisplayName stored DisplayNameIsNotEmpty;
    property VarType: TrwVarTypeKind read GetVarType write SetVarType;
  end;


  TrwFuncParams = class(TInterfacedCollection)
  private
    FResult: TrwVariable;
    FTag: Integer;
    function GetItem(AIndex: Integer): TrwFuncParam;
    function  GetResult: PTrwVariable;
  public
    procedure Assign(ASource: TPersistent); override;
    function  AddParam(const AVar: Boolean; const AName: string): TrwFuncParam;
    function  ParamByName(const AParamName: String): TrwFuncParam;

    property Items[index: Integer]: TrwFuncParam read GetItem; default;
    property Result: PTrwVariable read GetResult;
    property Tag: Integer read FTag write FTag;
  end;


  TrwEventsList = class;

  TrwEvent = class(TObject, IrmFunction, IrmLibCollectionItem, IrwCollectionItem, IrmInterfacedObject)
  private
    FOwner: TrwEventsList;
    function  GetEventFullText: String;
    procedure SetEventFullText(const Value: String);

  protected
    // Interface (some functions are bogus since it is not TrwFunction)
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function  QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function  VCLObject: TObject;
    function  GetCollection: IrwCollection;
    function  RealObject: TCollectionItem;
    function  GetItemIndex: Integer;
    procedure SetItemIndex(const AIndex: Integer);
    function  GetItemID: String;
    function  IsInheritedItem: Boolean;
    function  GetName: String;
    function  GetDisplayName: String;
    procedure SetDisplayName(const ADisplayName: String);
    function  GetGroup: String;
    procedure SetGroup(const AGroupName: String);
    function  GetDescription: String;
    procedure SetDescription(const ADescription: String);
    function  GetFunctionText: String;
    procedure SetFunctionText(const AFunctText: String);
    function  GetInheritanceInfo: TrmInheritedFunctInfoList;

  public
    Name: string;
    ParamStr: string;
    HandlersText: string;
    InheritedItem: Boolean;
    FCodeAddress: Integer;
    Params: TrwFuncParams;
    UserEvent: Boolean;

    constructor Create(AOwner: TrwEventsList);
    destructor  Destroy; override;

    property EventFullText: String read GetEventFullText write SetEventFullText;
  end;

       { Class for storing registered events for component}

  TrwEventsList = class(TList, IrmFunctions, IrmLibCollection, IrwCollection, IrmInterfacedObject)
  private
    FOwner:  TrwComponent;
    function GetItem(Index: Integer): TrwEvent;

  protected
    // Interface (some functions are bogus since it is not TrwFunctions)
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function  QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function  VCLObject: TObject;
    function  RealObject: TCollection;
    function  GetItemCount: Integer;
    function  GetItemByIndex(const AItemIndex: Integer): IrwCollectionItem;
    function  GetItemByID(const AItemID: String): IrwCollectionItem;
    function  GetComponentOwner: IrmDesignObject;
    function  AddItem: IrwCollectionItem;
    procedure DeleteItem(const AItemIndex: Integer);
    procedure BeginUpdate;
    procedure EndUpdate;
    function  GetFunctionByName(const AFunctName: String): IrmFunction;
    procedure CreateDebInf;
    procedure ClearDebInf;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;
    procedure Compile;

  public
    procedure SetInherited(Value: Boolean);
    function  Add(AEventName: string; AEventParams: string; AResultType: TrwVarTypeKind; APointertType: String): Integer;
    procedure Delete(Index: Integer);
    function  IndexOf(const AName: string): Integer;
    procedure Clear; override;
    procedure Assign(ASource: TrwEventsList);
    destructor Destroy; override;
    property  Items[Index: Integer]: TrwEvent read GetItem; default;
  end;


  {Virtual Methods Table for Inherited components}

  TrwVMTRow = class (TCollectionItem)
  private
    FName: string;
    FText: string;
    FCodeAddress: Integer;

  public
    property Name: string read FName write FName;
    property Text: string read FText write FText;
    property CodeAddress: Integer read FCodeAddress write FCodeAddress;
  end;


  TrwVMT = class (TCollection)
  private
    FAlreadyInitialized: Boolean;

    function  GetItem(Index: Integer): TrwVMTRow;
    procedure SetItem(Index: Integer; Value: TrwVMTRow);

  public
    constructor Create(ItemClass: TCollectionItemClass);
    function Add: TrwVMTRow;
    function IndexOf(AName: String): Integer;
    property Items[Index: Integer]: TrwVMTRow read GetItem write SetItem; default;
  end;


  TrwLibCollectionItem = class(TrwCollectionItem, IrmLibCollectionItem)
  private
    FName: string;
    FDisplayName: string;    
    FChangedBy: String;
    FLastChange: TDateTime;
    FChangeReason: String;
    FGroup: String;
    FDescription: String;
    FData: String;
    FCompiled: Boolean;

    function  ChangeReasonIsNotEmpty: Boolean;
    function  DescriptionIsNotEmpty: Boolean;
    function  GroupIsNotEmpty: Boolean;
    function  DisplayNameIsNotEmpty: Boolean;
    function  DataIsNotEmpty: Boolean;
    function  GetDescription: String;
    procedure SetDescription(const AValue: String);
    function  GetGroup: String;
    procedure SetGroup(const AValue: String);
    function  GetName: string;
    procedure SetName(const AValue: string); virtual;

  protected
    function  GetData: string; virtual;
    procedure SetData(const AValue: string); virtual;
    function  GetDisplayName: string; override;
    procedure SetDisplayName(const AValue: string); override;

  public
    procedure Assign(Source: TPersistent); override;
    procedure Compile; virtual;
  published
    property LastChange: TDateTime read FLastChange write FLastChange;
    property ChangedBy: String read FChangedBy write FChangedBy;
    property ChangeReason: String read FChangeReason write FChangeReason stored ChangeReasonIsNotEmpty;
    property Name: string read GetName write SetName;
    property DisplayName stored DisplayNameIsNotEmpty;
    property Group: String read GetGroup write SetGroup stored GroupIsNotEmpty;
    property Description: String read GetDescription write SetDescription stored DescriptionIsNotEmpty;
    property Data: string read GetData write SetData stored DataIsNotEmpty;
  end;


  TrwLibCollection = class (TrwCollection, IrmLibCollection)
  private
    FModified: Boolean;
    FCompiledCode: TrwCode;
    FDebInf: TrwDebInf;
    FDebSymbInf: TrwDebInf;
    FLibVersion: TDateTime;
    FReadVersion: TDateTime;
    FCompilingProgress: Integer;
    FStoreDebInfo: Boolean;
    FCompilingInProgress: Boolean;

    procedure WriteCompiledCode(const AWriter: TWriter);
    procedure ReadCompiledCode(const AReader: TReader);
    procedure WriteDebugInfo(const AWriter: TWriter);
    procedure ReadDebugInfo(const AReader: TReader);
    procedure CacheDebugInfo;

  protected
    function EntryPointOffset: Integer; virtual;

  public
    function  AddLibItem(const AName, AGroup, ADescription: String): TrwLibCollectionItem;
    property  ReadVersion: TDateTime read FReadVersion write FReadVersion;
    property  LibVersion: TDateTime read FLibVersion;
    procedure SaveToStream(AStream: TStream); virtual;
    procedure LoadFromStream(AStream: TStream); virtual;
    procedure Initialize; virtual; abstract;
    procedure Store; virtual;
    procedure Compile; virtual;
    property  Modified: Boolean read FModified write FModified;
    property  CompiledCode: TrwCode read FCompiledCode write FCompiledCode;
    procedure CreateDebInf;
    procedure ClearDebInf;
    procedure Execute(const APar1: string; const APar2: string = ''); virtual;
    function  IndexOf(const AName: String): Integer;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;
    function  GetCompiledCode: PTrwCode;

    property  StoreDebInfo: Boolean read FStoreDebInfo write FStoreDebInfo;
    property  CompilingInProgress: Boolean read FCompilingInProgress;
  published
    function  PCount: Variant;
  end;


  {Components Library}

  TrwLibComponent = class (TrwLibCollectionItem, IrwLibraryComponent, IrmComponent)
  private
    FParentName: string;
    FVMT: TrwVMT;
    FPicture: TMemoryStream;
    FDsgnPaletteFlags: Byte;
    FDsgnPaletteSettings: TrwDsgnPaletteSettings;

    procedure ReadPicture(AStream: TStream);
    procedure WritePicture(AStream: TStream);
    procedure SetDsgnPaletteFlags(const Value: Byte);
    function IsItDesigner : boolean;

    //Interface
    function  GetClassName: String;
    procedure SetClassName(const AClassName: String);
    function  GetAncestorClassName: String;
    procedure GetPicture(const ABitmap: TBitmap);
    procedure SetPictureData(const AStream: TStream);
    procedure SetComponentData(const AComponent: IrmDesignObject);
    function  GetComponentData: IevDualStream;
    function  GetDsgnPaletteSettings: TrwDsgnPaletteSettings;
    procedure SetDsgnPaletteSettings(const AValue: TrwDsgnPaletteSettings);
    function  GetObjectType: TrwDsgnObjectType;
    function  GetVCLClass: TClass;
    function  CreateComponent(const AExistingComponent: IrmDesignObject = nil; const AInherit: Boolean = False; const ADontRunFixUp: Boolean = False): IrmDesignObject;

  protected
    procedure SetData(const Value: String); override;
    procedure DefineProperties(Filer: TFiler); override;

  public
    property VMT: TrwVMT read FVMT;

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure GenerateNewID; override;
    function  CreateComp(AComp: TrwComponent = nil; AInherit: Boolean = False; ADontRunFixUp: Boolean = False): TrwComponent;
    procedure SaveComp(AComp: TComponent);
    function  GetBaseClass: TrwComponentClass;
    function  IsInheritsFrom(const ArwClassName: string): Boolean;
    procedure InitVMT(AInstance: TrwComponent; APriffix: String = '');
    procedure Assign(Source: TPersistent); override;
    procedure Compile; override;
    function  OwnPictureIsEmpty: Boolean;

  published
    property ParentName: string read FParentName write FParentName;

    // bit 1     Library View only
    // bit 2     Print Form View
    // bit 3     Input Form View
    // bit 4     Do not show on Library View
    // bit 5     Show in Advanced Mode
    // bit 6     ASCII Form View
    property DsgnPaletteFlags: Byte read FDsgnPaletteFlags write SetDsgnPaletteFlags stored false default 0; // obsolete
    property DsgnPaletteSettings: TrwDsgnPaletteSettings read FDsgnPaletteSettings write FDsgnPaletteSettings default [];
  end;


  TrwLibComponents = class (TrwLibCollection, IrwLibraryComponents, IrmComponents)
  private
    function  GetItem(Index: Integer): TrwLibComponent;
    procedure SetItem(Index: Integer; Value: TrwLibComponent);

    //Interface
    function GetComponentList: TCommaDilimitedString;
    function GetComponent(const AClassName: String): IrwLibraryComponent;
    function GetComponentByClassName(const AClassName: String): IrmComponent;
    function GetComponentByIndex(const AIndex: Integer): IrmComponent;

  protected
    function EntryPointOffset: Integer; override;

  public
    procedure Initialize; override;
    procedure Store; override;
    function  Add: TrwLibComponent;
    property  Items[Index: Integer]: TrwLibComponent read GetItem write SetItem; default;
    procedure Execute(const APar1: string; const APar2: string = ''); override;
    function  ClassInheritsFromClass(const AClassName: string; const AAncClassName: string): Boolean;
    procedure RegisterRTTIInfo;
  published
    function  PIndexOf(AName: Variant): Variant;
    function  PItems(AIndex: Variant): Variant;
  end;


  TrwLocalComponent = class(TrwLibComponent)
  protected
    function AsAncestor(const AncestorItem: TrwCollectionItem): Boolean; override;
  end;


  TrwLocalComponents = class(TrwLibComponents)
  private
    function  GetItem(Index: Integer): TrwLocalComponent;
  public
    procedure Compile; override;
    procedure Initialize; override;
    procedure Store; override;

    property  Items[Index: Integer]: TrwLocalComponent read GetItem; default;
  end;



  { Lib Functions }

  TrwFunction = class(TrwLibCollectionItem)
  private
    FParams: TrwFuncParams;
    FDesignTimeInfo: TrwFunctDsgnTimeInfoSet;
    procedure WriteParams(Writer: TWriter);
    procedure ReadParams(Reader: TReader);
  protected
    procedure DefineProperties(Filer: TFiler); override;
  public
    property Params: TrwFuncParams read FParams;

    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
    function    GetFunctionHeader: String;
  published
    property Name stored False;
    property DesignTimeInfo: TrwFunctDsgnTimeInfoSet read FDesignTimeInfo write FDesignTimeInfo;
  end;

  TrwFunctions = class(TrwLibCollection)
  private
    function GetItem(index: Integer): TrwFunction;
  public
    function FindFunction(const AName: string): TrwFunction;
    property Items[index: Integer]: TrwFunction read GetItem; default;
  end;


  TrmAggrFunction = class(TrwFunction)
  private
    FHeaderText: String;
  public
    property HeaderText: String read FHeaderText;
  end;


  TrmAggrFunctions = class(TrwFunctions)
  public
    procedure Initialize; override;
    procedure AddFunction(const AHeaderText: String; const AGroup: String; const ADescription: String; const ADefaultValues: array of Variant);
    procedure Execute(const APar1: string; const APar2: string = ''); override;
  end;


  TrwLibFunction = class(TrwFunction, IrwLibraryFunction, IrmFunction)
  private
    FText: string;
    FCodeAddress: Integer;

    function  GetFunctionText: String;
    procedure SetFunctionText(const AFunctText: String); virtual;
    procedure SetName(const AValue: String); override;

  protected
    function  GetInheritanceInfo: TrmInheritedFunctInfoList; virtual;

  public
    constructor Create(Collection: TCollection); override;
    procedure Compile; override;

  published
    property Text: string read GetFunctionText write SetFunctionText;
    property CodeAddress: Integer read FCodeAddress write FCodeAddress;
  end;


  TrwLibFunctions = class(TrwFunctions, IrwLibraryFunctions, IrmFunctions)
  public
    procedure LoadFromStream(AStream: TStream); override;  
    procedure Initialize; override;
    procedure Store; override;
    procedure InitializeHeaders;
    function  AddFunc(const AText: String): TrwLibFunction;
    procedure Execute(const APar1: string; const APar2: string = ''); override;

  protected
    function  EntryPointOffset: Integer; override;

  //interface
  protected
    function  GetFunctionByName(const AFunctName: String): IrmFunction;

  published
    function  PIndexOf(AName: Variant): Variant;
    function  PItems(AIndex: Variant): Variant;
  end;


  { Published methods}

  TrwPublishedMethod = class(TrwFunction)
  private
    FCompClass: TClass;
    FHeaderText: String;
  public
    property CompClass: TClass read FCompClass;
    property HeaderText: String read FHeaderText;
  end;


  TrwPublishedMethods = class(TrwFunctions)
  public
    procedure Initialize; override;
    procedure AddMethod(const AHeaderText: String; const AClass: String);
    procedure Execute(const APar1: string; const APar2: string = ''); override;
    function  IndexOf(const AClass: TClass; const AMethodName: string): TrwPublishedMethod; reintroduce;
    procedure ListOfFunction(const AClass: TClass; AList: TList);
  end;


  {Local variables and functions}

  TrwGlobalVariable = class (TrwCollectionItem, IrmVariable)
  private
    FVarRec:  TrwVariable;
    FValue:   Variant;
    FVisibility: TrwVarVisibility;
    FDisplayName: String;
    FShowInQB: Boolean;
    FShowInFmlEditor: Boolean;
    procedure SetVarType(const Value: TrwVarTypeKind);
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    procedure SetName(const Value: string);
    function  GetName: string;
    function  GetVarType: TrwVarTypeKind;
    function  GetPointerType: String;
    procedure SetPointerType(const AValue: String);
    procedure ReadValue(Reader: TReader);
    procedure WriteValue(Writer: TWriter);
    function  IsNotInherited: Boolean;

    // interface
    function  GetVarInfo: TrmVFHVarInfoRec;
    function  GetVisibility: TrwVarVisibility;
    procedure SetVisibility(const AValue: TrwVarVisibility);
    function  GetShowInQB: Boolean;
    procedure SetShowInQB(const AValue: Boolean);
    function  GetShowInFmlEditor: Boolean;
    procedure SetShowInFmlEditor(const AValue: Boolean);

  protected
    procedure DefineProperties(Filer: TFiler); override;
    function  GetDisplayName: string; override;
    procedure SetDisplayName(const Value: string); override;
    function  AsAncestor(const AncestorItem: TrwCollectionItem): Boolean; override;
    procedure ApplyDelta(const DeltaCollectionItem: TrwCollectionItem); override;

  public
    constructor Create(Collection: TCollection); override;
    function    PVarRec: PTrwVariable;
    function    VarAddr: Pointer;
    procedure   Assign(Source: TPersistent); override;
    function    AsString: String;
    procedure   UpdateValue;

  published
    property Name: string read GetName write SetName stored IsNotInherited;
    property VarType: TrwVarTypeKind read GetVarType write SetVarType;
    property PointerType: String read GetPointerType write SetPointerType stored IsNotInherited;
    property Value: Variant read GetValue write SetValue stored False;
    property Visibility: TrwVarVisibility read GetVisibility write SetVisibility stored IsNotInherited;
    property DisplayName stored IsNotInherited;
    property ShowInQB: Boolean read FShowInQB write FShowInQB stored IsNotInherited default False;
    property ShowInFmlEditor: Boolean read FShowInFmlEditor write FShowInFmlEditor stored IsNotInherited default False;
  end;


  TrwGlobalVariables = class (TrwCollection, IrmVariables)
  private
    function  GetItem(Index: Integer): TrwGlobalVariable;
    procedure SetItem(Index: Integer; Value: TrwGlobalVariable);

    //interface
    function  GetVariableByName(const AVarName: String): IrmVariable;

  public
    function Add: TrwGlobalVariable;
    function IndexOf(AName: String): Integer;
    property Items[Index: Integer]: TrwGlobalVariable read GetItem write SetItem; default;
  end;



  {User-defined component property}

  TrwUserProperty = class (TrwCollectionItem)
  private
    FPropType: TrwVarTypeKind;
    FPointerType: String;
    FName: string;
    FDelegatedName: String;
    FReadValue: Variant;
    FReadObjValue: TPersistent;
    FPPropInfo: PPropInfo;
    FInLimbo: Boolean;
    function  GetPPropInfo: PPropInfo;
    function  IsObject: Boolean;
    function  IsPersistent: Boolean;
    function  CanWritePersistent: Boolean;
    function  CanWriteComponent: Boolean;
    function  CanWriteValue: Boolean;
    function  IsComponent: Boolean;
    function  GetValue: Variant;
    function  GetPersistentValue: TPersistent;
    function  GetComponentValue: TComponent;
    procedure SetValue(const Value: Variant);
    procedure SetPersistentValue(const AValue: TPersistent);
    procedure SetComponentValue(const AValue: TComponent);
    procedure CreateReadPersistentObj(AClassName: String);
    procedure DelegatedPropertyInfo(var AObject: TObject; AProp: PTPropNamesArray);

    function  CallReadProc: Variant;
    procedure CallWriteProc(AValue: Variant);
    function  CallReadProcDelegated: Variant;
    procedure CallWriteProcDelegated(AValue: Variant);

    function  IsTheSame(AUserProperty: TrwUserProperty): Boolean;
    function  FixUpValue: Boolean;
    function  IsDelegated: Boolean;
    procedure SetPointerType(const Value: String);

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
    function    GetPointerClass: TClass;

    property    PPropInfo: PPropInfo read GetPPropInfo;
    property    InLimbo: Boolean read FInLimbo;

  published
    property Name: string read FName write FName;
    property DelegatedName: String read FDelegatedName write FDelegatedName;  //Delegated property of a child component
    property PropType: TrwVarTypeKind read FPropType write FPropType;
    property PointerType: String read FPointerType write SetPointerType;
    property Value: Variant read GetValue write SetValue stored CanWriteValue;
    property PersistentValue: TPersistent read GetPersistentValue write SetPersistentValue stored CanWritePersistent;
    property ComponentValue: TComponent read GetComponentValue write SetComponentValue stored CanWriteComponent;
  end;


  TrwUserProperties = class (TrwCollection)
  private
    function  GetItem(Index: Integer): TrwUserProperty;
    procedure SetItem(Index: Integer; Value: TrwUserProperty);

  public
    procedure Assign(Source: TPersistent); override;
    function Add: TrwUserProperty;
    function IndexOf(AName: String): Integer;
    procedure ClearNotInLimbo;

    property Items[Index: Integer]: TrwUserProperty read GetItem write SetItem; default;
  end;


  {Design Information}
  TrwDesignInfo = class
  private
    FEventsPositions: TStringList;
    FLastEvent: string;
  public
    constructor Create;
    destructor  Destroy; override;
    property LastEvent: string read FLastEvent write FLastEvent;
    property EventsPositions: TStringList read FEventsPositions;
  end;


         { TrwComponent is ancestor for all RW-components}

  TrwComponent = class(TComponent, IrmDesignObject)
  private
    FVisualControl: IrwVisualControl;
    FEventsList: TrwEventsList;
    FGlobalVarFunc: TrwGlobalVarFunct;
    FOnDestroy: TNotifyEvent;
    FLibDesignMode: Boolean;
    FLibComponent: String;
    FLibComponentRef: IrmComponent;
    FLoading: Boolean;
    FInheritedComponent: Boolean;
    FLoadingInheritances: Boolean;
    FDesignInfo: TrwDesignInfo;
    FChildList: TList;
    FChildOwner: TrwComponent;
    FUserPropertiesRead: TrwUserProperties;
    FUserPropertiesWork: TrwUserProperties;
    FrwClassName: String;
    FIgnoreDesignMode: Boolean;
    FDsgnLockState: TrwDsgnLockState;
    FDescription: String;

    FRMDesigner: IrmDesigner;
    FFormulas: TrmFMLFormulas;
    FAggrFunctList: TList; // list of aggregate function holders for notification purposes

    function  GetEvent(const AName: string): TrwEvent;
    function  GetIndexOfEvent(const AName: string): Integer;

    procedure WriteEventsData(Writer: TWriter); virtual;
    procedure ReadEventsData(Reader: TReader); virtual;
    function  IsNotEmptyEvents: Boolean;
    function  CanWriteLibInfo: Boolean;
    function  GetEventCount: Integer;
    procedure SetLibComponent(Value: String);
    procedure SetLibComponentByRef(ALibComponent: IrmComponent);
    function  GetDesignInfo: TrwDesignInfo;
    function  FormulasIsNotEmpty: Boolean;

    procedure LoadFromLibComp(const AClassName: String); overload;
    procedure LoadFromLibComp(const ALibComponent: IrmComponent); overload;

    procedure FGetChildProc(Child: TComponent);
    procedure GetChildrenList(AList: TList; AOwner: TrwComponent);
    function  GetUserProperties: TrwUserProperties;
    procedure SetUserProperties(const Value: TrwUserProperties);
    function  RwClassNameIsNotEmpty: Boolean;
    function  GetVisualControl: IrwVisualControl;
    procedure SetVisualControl(const Value: IrwVisualControl);
    procedure SetFormulas(const Value: TrmFMLFormulas);

  protected
    FWritingOnlySelf: Boolean;
    FVirtOwner: Boolean;
    FSubChildrenNamesUnique: Boolean;

    function  CreateVisualControl: IrwVisualControl; dynamic;
    function  GetDesignParent: TWinControl; virtual;
    procedure SetDesignParent(Value: TWinControl); virtual;
    procedure DestroyVisualControl;

    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure RegisterComponentEvents; virtual;
    procedure SendNotification(AOperation: TrwNotifyOperation); virtual;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure WriteState(Writer: TWriter); override;
    procedure ReadState(Reader: TReader); override;
    procedure Loaded; override;
    procedure RWLoaded; virtual;
    procedure SetName(const NewName: TComponentName); override;
    procedure AncestorNotFound(Reader: TReader; const ComponentName: string; ComponentClass: TPersistentClass;
                               var Component: TComponent);
    procedure HideInheritedEvent;
    procedure SetInheritedComponentProperty(AValue: Boolean); virtual;
    procedure AfterInitLibComponent; virtual;
    procedure SetDsgnLockState(const Value: TrwDsgnLockState);
    function  GetDsgnLockState: TrwDsgnLockState; virtual;
    function  GetDescription: String; virtual;
    procedure SetDescription(const Value: String); virtual;

    function  ControlAtPos(APos: TPoint): TrwComponent; virtual;


    //Interface
    function  GetDesigner: IrmDesigner;
    procedure SetDesigner(ADesigner: IrmDesigner); virtual;
    function  VCLObject: TObject;
    function  RealObject: TComponent;
    function  ObjectType: TrwDsgnObjectType; virtual;
    function  ObjectAtPos(APos: TPoint): IrmDesignObject;
    function  GetWinControlParent: TWinControl; virtual;
    procedure Notify(AEvent: TrmDesignEventType); virtual;
    function  GetParentExtRect: TRect; virtual;
    function  GetVarFunctHolder: IrmVarFunctHolder;
    function  GetFormulas: IrmFMLFormulas;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; virtual;
    function  GetParent: IrmDesignObject; virtual;
    procedure SetParent(AParent: IrmDesignObject); virtual;
    function  GetObjectIndex: Integer;
    procedure SetObjectIndex(const AIndex: Integer);
    function  ClientToScreen(APos: TPoint): TPoint; virtual;
    function  ScreenToClient(APos: TPoint): TPoint; virtual;
    function  GetBoundsRect: TRect; virtual;
    procedure SetBoundsRect(ARect: TRect); virtual;
    function  GetLeft: Integer; virtual;
    procedure SetLeft(AValue: Integer); virtual;
    function  GetTop: Integer; virtual;
    procedure SetTop(AValue: Integer); virtual;
    function  GetWidth: Integer; virtual;
    procedure SetWidth(AValue: Integer); virtual;
    function  GetHeight: Integer; virtual;
    procedure SetHeight(AValue: Integer); virtual;
    procedure Refresh; virtual;
    function  DontStoreProperty: Boolean;
    function  DontStoreIndProperty(Index: Integer): Boolean;
    function  GetClassName: String;
    function  ObjectInheritsFrom(const AClassName: String): Boolean;
    function  PropertyExists(const APropertyName: String): Boolean;
    function  PropertiesByTypeName(const ATypeName: String): TrwStrList;
    procedure SetPropertyValue(const APropertyName: String; Value: Variant);
    function  GetPropertyValue(const APropertyName: String): Variant;
    function  CreateChildObject(const AClassName: String): IrmDesignObject; virtual;
    procedure AddChildObject(AObject: IrmDesignObject); virtual;
    procedure RemoveChildObject(AObject: IrmDesignObject); virtual;
    function  GetObjectOwner: IrmDesignObject;
    function  GetChildrenCount: Integer;
    function  GetChildByIndex(AIndex: Integer): IrmDesignObject;
    function  GetControlCount: Integer; virtual;
    function  GetControlByIndex(AIndex: Integer): IrmDesignObject; virtual;
    function  GetObjectClassDescription: String;
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant; virtual;
    function  GetRootOwner: IrmDesignObject;
    function  GetPathFromRootOwner: String;
    function  GetPathFromRootParent: String;
    function  IsInheritedComponent: Boolean;
    function  GetInterfacedPropByType(const APropertyType: String): IrmInterfacedObject;
    function  GetEvents:  IrmFunctions;
    function  GetLibComponentByClass(const AClassName: String): IrmComponent;
    procedure GetAvailableClassList(const AList: TStringList);

  public
    procedure InitializeForDesign; virtual;
    function  IsVirtualOwner: Boolean;
    procedure WriteComp(Writer: TWriter);
    function  IsLibComponent: Boolean;

  //rendering
    procedure  RegisterAggregateFunction(const AggrFunct: TrmFMLAggrFunctHolder);
    procedure  NotifyAggregateFunctions; virtual;
    procedure  EmptyAggregateFunctions;
    function   ParticipantOfAggregateFunctions: Boolean;
  //end

  public
    property Loading: Boolean read FLoading;
    property Events[const AName: string]: TrwEvent read GetEvent;
    property EventCount: Integer read GetEventCount;
    property EventsList: TrwEventsList read FEventsList;
    property OnDestroy: TNotifyEvent read FOnDestroy write FOnDestroy;
    property InheritedComponent: Boolean read FInheritedComponent;
    property GlobalVarFunc: TrwGlobalVarFunct read FGlobalVarFunc;
    property LoadingInheritances: Boolean read FLoadingInheritances;

    property Designer: IrmDesigner read GetDesigner;
    property VisualControl: IrwVisualControl read GetVisualControl write SetVisualControl;
    property LibDesignMode: Boolean read FLibDesignMode write FLibDesignMode;
    property DesignParent: TWinControl read GetDesignParent write SetDesignParent;
    property DesignInfo: TrwDesignInfo read GetDesignInfo;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function  VisualControlCreated: Boolean;
    procedure RegisterEvents(const AEvent: array of string; AUserEvents: Boolean = False);
    procedure UnRegisterEvents(const AEvent: array of string);
    procedure GetEventsList(AStrings: TStrings; ANoEmpty: Boolean = False);
    procedure ExecuteEventsHandler(const AEventName: string; const AParams: array of variant);
    procedure ExecuteEventsHandlerByNumber(const EventNum: Integer);
    procedure RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); virtual;
    function  IsDesignMode: Boolean; virtual;
    function  IsInheritsFrom(AClassName: string): Boolean;
    procedure RemoveGlobalVarFunc;
    procedure InitGlobalVarFunc;
    procedure FixUpCompIndexes;
    function  EventByIndex(AIndex: Integer): TrwEvent;
    procedure CompileEvent(const AEvent: TrwEvent);
    procedure CompileEvents(ASyntaxCheck: Boolean = False);
    procedure CompileChildren(ASyntaxCheck: Boolean = False); virtual;
    procedure Compile(ASyntaxCheck: Boolean = False); virtual;
    procedure SetExternalGlobalVarFunc(AGlobalVarFunc: TrwGlobalVarFunct);
    function  FixUpUserProperties: Boolean;
    function  UserPropertyIsNotEmpty: Boolean;
    procedure Assign(Source: TPersistent); override;

    procedure DoSyncVisualControl(const APropID: TrwChangedPropID); virtual;

    function  VirtualOwner: TrwComponent; virtual;
    function  VirtualComponentCount: Integer; virtual;
    function  FindVirtualChild(const AName: string): Integer; virtual;
    function  GetVirtualChild(const AIndex: Integer): TComponent; virtual;
    procedure InsertVirtualComponent(AComponent: TrwComponent); virtual;
    function  RootOwner: TrwComponent;
    function  PathFromRootOwner: String;
    function  GetGlobalOwner: TrwComponent;
    function  GlobalFindComponent(const AComponentName: String): TrwComponent;
    procedure PrepareFormulas;
    property  LibComponentRef: IrmComponent read FLibComponentRef;
    function  RunEventByName(const AEventName: String; const AParams: Variant): Variant;

  published
    property _RwClassName: String read FrwClassName write FrwClassName stored RwClassNameIsNotEmpty;
    property _LibComponent: string read FLibComponent write SetLibComponent stored CanWriteLibInfo;
    property _UserProperties: TrwUserProperties read GetUserProperties write SetUserProperties stored UserPropertyIsNotEmpty;

    property  DsgnLockState: TrwDsgnLockState read GetDsgnLockState write SetDsgnLockState default rwLSUnlocked;
    property  Description: String read GetDescription write SetDescription;
    property  Formulas: TrmFMLFormulas read FFormulas write SetFormulas stored FormulasIsNotEmpty;

    function  PPropertyExists(APropertyName: Variant): Variant;
    procedure PSetPropertyValue(APropertyName: Variant; Value: Variant);
    function  PGetPropertyValue(APropertyName: Variant): Variant;
    function  PExecEvent(AEventName: Variant; AParams: Variant): Variant;
    function  POwner: Variant;
    function  PComponentCount: Variant;
    function  PFindComponent(AName: Variant): Variant;
    function  PComponentByIndex(AIndex: Variant): Variant;
    function  PComponentIndexByName(AName: Variant): Variant;
    function  PClassName: Variant;
    function  PInheritsFrom(AClassName: Variant): Variant;
    function  PExecFunction(AFunctionName: Variant; AParams: Variant): Variant;
    function  PPathFromRootOwner: Variant;
    function  PPathFromRootParent: Variant;

    function  PRootOwner: Variant;  // Internal, not exposed.
    function  PCalcAggrFunctResult(AFunctionID: Variant): Variant; // Internal, not exposed.
  end;



  TrwGlobalVarFunct = class(TrwComponent, IrmVarFunctHolder)
  private
    FVariables: TrwGlobalVariables;
    FVariablesRW: TrwGlobalVariables;        // For streaming only
    FLocalComponents: TrwLocalComponents;
    FLocalComponentsRW: TrwLocalComponents;  // For streaming only

    FAncestorMode: Boolean;

    procedure WriteEventsData(Writer: TWriter); override;
    procedure ReadEventsData(Reader: TReader); override;
    procedure ReadVariables(Reader: TReader);
    procedure WriteVariables(Writer: TWriter);
    procedure ReadLocalComponents(Reader: TReader);
    procedure WriteLocalComponents(Writer: TWriter);

    //interface
    function  GetVariables: IrmVariables;
    function  GetLocalComponents: IrmComponents;
    function  GetLocalFunctions:  IrmFunctions;

  protected
    procedure DefineProperties(Filer: TFiler); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

    property Variables: TrwGlobalVariables read FVariables;
    property LocalComponents: TrwLocalComponents read FLocalComponents;
  published  
    property Description stored False;
  end;


  {TrwNoVisualComponent is parent class for non-visual components}

  TrwNoVisualComponent = class(TrwComponent)
  private
    FLeft: Integer;
    FTop: Integer;
    procedure SetPoition(Index: Integer; Value: Integer);
  protected
    procedure SetDesignParent(Value: TWinControl); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetBoundsRect(ARect: TRect); override;
    function  ObjectType: TrwDsgnObjectType; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;

    procedure SetDesigner(ADesigner: IrmDesigner); override;
    function  GetLeft: Integer; override;
    procedure SetLeft(AValue: Integer); override;
    function  GetTop: Integer; override;
    procedure SetTop(AValue: Integer); override;
    function  GetWidth: Integer; override;
    function  GetHeight: Integer; override;
    
  public
    property Left: Integer index 0 read FLeft write SetPoition;
    property Top: Integer index 1 read FTop write SetPoition;
  end;

function  MakeEventHandlerHeader(AEvent: TrwEvent; AObjectName: String = ''): string;
procedure SaveVarsToStream(const AVariables: TrwGlobalVariables; const AStream: TStream);
procedure ReadVarsFromStream(const AVariables: TrwGlobalVariables; const AStream: TStream);
procedure SaveVarsToFile(const AVariables: TrwGlobalVariables; const AFileName: string);
procedure ReadVarsFromFile(const AVariables: TrwGlobalVariables; const AFileName: string);
procedure GetUserPropInfos(AProps: TrwUserProperties; APropList: PPropList);
procedure VariablesToParamList(const Vars: TrwGlobalVariables; const Params: PTrwReportParamList);
procedure ParamListToVariables(const Vars: TrwGlobalVariables; const Params: PTrwReportParamList);

function  IsDesignMsg(AObject: TrwComponent; var Message: TMessage): Boolean;


implementation

{$R rwComponents.res}

uses rwEngine, rwDebugInfo, rwUtils, rwReport, rwParser, rwQBInfo, rwDsgnControls, rwDB,
     Types, rmReport, StrUtils;


function IsDesignMsg(AObject: TrwComponent; var Message: TMessage): Boolean;
var
  C: TControl;
  P: TPoint;

  procedure DispatchDesignMsg;
  begin
    Result := AObject.GetDesigner.DispatchDesignMsg(AObject, Message);
  end;

begin
  Result := False;

  if Assigned(AObject) and (AObject.GetDesigner <> nil) then
  begin
    if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
    begin
      if AObject.ObjectType = rwDOTInputForm then
      begin
        C := AObject.VisualControl.RealObject;
        if C is TWinControl then
        begin
          P := Point(Smallint(Message.LParamLo), Smallint(Message.LParamHi));
          C := TWinControl(C).ControlAtPos(P, False);
          if not Assigned(C) then
            DispatchDesignMsg;
        end
        else
          DispatchDesignMsg;
      end
      else
        DispatchDesignMsg;      
    end
    else
      DispatchDesignMsg;
  end;
end;

procedure VariablesToParamList(const Vars: TrwGlobalVariables; const Params: PTrwReportParamList);
var
  i, n: Integer;
begin
  SetLength(Params^, Vars.Count);

  n := 0;
  for i := 0 to Vars.Count - 1 do
    if Vars[i].Visibility in [rvvPublished, rvvPublic] then
    begin
      Params^[n].Name := Vars[i].Name;
      Params^[n].ParamType := Vars[i].VarType;
      Params^[n].Value := Vars[i].Value;
      Params^[n].RunTimeParam := Vars[i].Visibility = rvvPublic;
      Inc(n, 1);
    end;

  SetLength(Params^, n);
end;

procedure ParamListToVariables(const Vars: TrwGlobalVariables; const Params: PTrwReportParamList);
var
  i, n: Integer;
begin
  // Clear some public vars
  for i := 0 to Vars.Count - 1 do
    if Vars[i].Visibility = rvvPublic then
      case Vars[i].VarType of
        rwvArray:    Vars[i].Value := varArrayOf([]);
        rwvPointer:  Vars[i].Value := 0;
      end;

  for i := Low(Params^) to High(Params^) do
  begin
    n := Vars.IndexOf(Params^[i].Name);
    if (n <> -1) and (Vars[n].Visibility in [rvvPublished, rvvPublic]) then
      Vars[n].Value := Params^[i].Value;
  end;
end;

function MakeEventHandlerHeader(AEvent: TrwEvent; AObjectName: String = ''): string;
var
  h: String;
begin
  if AEvent.Params.Result.VarType = rwvUnknown then
    Result := 'procedure'
  else
    Result := 'function';

  h := AEvent.Name;
  if AObjectName <> '' then
    h := AObjectName+'.'+h;
  Result := Result+' '+h + '(' + AEvent.ParamStr + ')';

  if AEvent.Params.Result.VarType <> rwvUnknown then
    Result := Result +': '+VarTypeToString(AEvent.Params.Result^);
end;

procedure SaveVarsToStream(const AVariables: TrwGlobalVariables; const AStream: TStream);
var
  Writer: TWriter;
  PrevOwner: TrwComponent;
begin
  Writer := TWriter.Create(AStream, 1024);
  PrevOwner := TrwComponent(AVariables.Owner);
  try
    AVariables.SetOwner(nil);
    Writer.WriteCollection(AVariables);
  finally
    AVariables.SetOwner(PrevOwner);
    Writer.Free;
  end;
end;

procedure ReadVarsFromStream(const AVariables: TrwGlobalVariables; const AStream: TStream);
var
  Reader: TReader;
  lGlVars: TrwGlobalVariables;
  i, j: Integer;
begin
  Reader := TReader.Create(AStream, 1024);
  lGlVars := TrwGlobalVariables.Create(nil, TrwGlobalVariable);
  try
    try
      Reader.NextValue;
      Reader.ReadValue;
      Reader.ReadCollection(lGlVars);

      for i := 0 to AVariables.Count - 1  do
      begin
        j := lGlVars.IndexOf(AVariables[i].Name);
        if j <> -1 then
          AVariables[i].Assign(lGlVars[j]);
      end;
    except
      on E: Exception do
        raise ErwException.Create('Report parameters stream is corrupted' + #13 + E.Message);
    end;

  finally
    lGlVars.Free;
    Reader.Free;
  end;
end;

procedure SaveVarsToFile(const AVariables: TrwGlobalVariables; const AFileName: string);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmCreate);
  try
    SaveVarsToStream(AVariables, F);
  finally
    F.Free;
  end;
end;


procedure ReadVarsFromFile(const AVariables: TrwGlobalVariables; const AFileName: string);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmOpenRead);
  try
    ReadVarsFromStream(AVariables, F);
  finally
    F.Free;
  end;
end;



procedure GetUserPropInfos(AProps: TrwUserProperties; APropList: PPropList);
var
  i: Integer;
begin
  for i := 0 to AProps.Count - 1 do
    APropList^[i] := AProps[i].PPropInfo;
end;

function IsEmptyInheritedEvent(const AEventText: String): Boolean;
begin
  Result := SameText(Trim(AEventText), cInheritedKeyWord + ';');
end;


{ TrwLibComponents }

function TrwLibComponents.Add: TrwLibComponent;
begin
  Result := TrwLibComponent(inherited Add);
end;

function TrwLibComponents.GetItem(Index: Integer): TrwLibComponent;
begin
  Result := TrwLibComponent(inherited GetItem(Index));
end;

procedure TrwLibComponents.SetItem(Index: Integer; Value: TrwLibComponent);
begin
  inherited SetItem(Index, Value);
end;

procedure TrwLibComponents.Execute(const APar1: string; const APar2: string = '');
var
  C: TrwLibComponent;
  i: Integer;
  EP: TrwVMAddress;
begin
  i := IndexOf(APar1);
  if i = -1 then
    raise ErwException.CreateFmt(ResErrWrongClass, [APar1]);

  C := Items[i];
  i := C.VMT.IndexOf(APar2);
  if i = -1 then
    raise ErwException.CreateFmt(ResErrNDefClProc, [APar2, APar1]);

  EP := GetEntryPoint(C.VMT[i].FCodeAddress);

  VM.Run(EP);
end;

function TrwLibComponents.PIndexOf(AName: Variant): Variant;
begin
  Result := IndexOf(AName);
end;

function TrwLibComponents.PItems(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Items[AIndex]));
end;


function TrwLibComponents.ClassInheritsFromClass(const AClassName, AAncClassName: string): Boolean;
var
  i: Integer;
begin
  if AnsiSameText(AClassName, AAncClassName) then
  begin
    Result := True;
    Exit;
  end;

  i := IndexOf(AClassName);
  if i = -1 then
    Result := GetClass(AClassName).InheritsFrom(GetClass(AAncClassName))

  else
    Result := Items[i].IsInheritsFrom(AAncClassName);
end;

procedure TrwLibComponents.Initialize;
var
  MS: IEvDualStream;
begin
  Clear;
  MS := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.LoadLibraryComponents(MS.RealStream);
  if MS.Size > 0 then
  begin
    MS.Position := 0;
    LoadFromStream(MS.RealStream);
  end;  
end;

procedure TrwLibComponents.Store;
var
  MS: IEvDualStream;
begin
  StoreDebInfo := False;
  MS := TEvDualStreamHolder.Create;
  SaveToStream(MS.RealStream);
  RWEngineExt.AppAdapter.UnloadLibraryComponents(MS.RealStream);
  inherited;
end;


function TrwLibComponents.GetComponent(const AClassName: String): IrwLibraryComponent;
var
  i: Integer;
begin
  i := IndexOf(AClassName);
  if i = -1 then
    Result := nil
  else
    Result := Items[i];
end;

function TrwLibComponents.GetComponentList: TCommaDilimitedString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + Items[i].Name;
  end;
end;

function TrwLibComponents.GetComponentByClassName(const AClassName: String): IrmComponent;
var
  i: Integer;
begin
  i := IndexOf(AClassName);
  if i = -1 then
    Result := nil
  else
    Result := Items[i];
end;


function TrwLibComponents.GetComponentByIndex(const AIndex: Integer): IrmComponent;
begin
  Result := Items[AIndex];
end;


function TrwLibComponents.EntryPointOffset: Integer;
begin
  Result := cVMLibraryComponentsOffset;
end;

procedure TrwLibComponents.RegisterRTTIInfo;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if rwRTTIInfo.FindClass(Items[i].Name) = nil then
      rwRTTIInfo.RegisterClass(Items[i]);
end;

{ TrwLibComponent }

constructor TrwLibComponent.Create(Collection: TCollection);
begin
  inherited;
  FPicture := nil;
  if rwRTTIInfo <> nil then
    rwRTTIInfo.RegisterClass(Self);
end;

destructor TrwLibComponent.Destroy;
begin
  if rwRTTIInfo <> nil then
    rwRTTIInfo.UnregisterClass(Self);

  FreeAndNil(FVMT);
  FreeAndNil(FPicture);
  inherited;
end;

function TrwLibComponent.CreateComp(AComp: TrwComponent = nil; AInherit: Boolean = False; ADontRunFixUp: Boolean = False): TrwComponent;
var
  MS: IEvDualStream;
  h: String;
  BaseCl: TrwComponentClass;
  Reader: TReader;
  lOldLoading: Boolean;
begin
  BaseCl := TrwComponentClass(GetBaseClass);
  if AInherit then
  begin
    AComp := BaseCl.Create(nil);
//    AComp._LibComponent := Name;
    AComp.SetLibComponentByRef(Self);
    Result := AComp;
  end

  else
  begin
    Result := AComp;
    MS := TEvDualStreamHolder.Create(Length(FData));
    Reader := TrwReader.Create(MS.RealStream);
    try
      MS.WriteBuffer(FData[1], Length(FData));
      MS.Position := 0;
      if Assigned(Result) then
      begin
        h := Result.Name;
//        lOldLoading := Result.FLoading;
//        Result.FLoading := True;
      end
      else
      begin
        h := '';
//        lOldLoading := False;
      end;
      if BaseCl.InheritsFrom(TrwReport) then
      begin
        if not Assigned(Result) then
          Result := TrwReport.Create(nil);
      end;

      if Assigned(Result) then
        lOldLoading := Result.FLoading
      else
        lOldLoading := False;

      try
        Result := TrwComponent(Reader.ReadRootComponent(Result));
      except
        on E: Exception do
        begin
          E.Message := 'Error reading Library Component "' + Name + '"'#13 + E.Message;
          raise;
        end;
      end;

      Result.FLoading := True;
      Result.Name := h;
      Result.FLoading := lOldLoading;

      if not ADontRunFixUp then
        if not Result.FixUpUserProperties then
          if IsItDesigner then
            ShowMessage(Name + ': User properties have been read incorrectly')
          else
            raise Exception.Create(Name + ': User properties have been read incorrectly');
    finally
      Reader.Free;
    end;
  end;
end;

procedure TrwLibComponent.SaveComp(AComp: TComponent);
var
  MS: IEvDualStream;
begin
  MS := TEvDualStreamHolder.Create;
  MS.WriteComponent(AComp);
  SetLength(FData, MS.Size);
  MS.Position := 0;
  MS.ReadBuffer(FData[1], MS.Size);
  if Assigned(FVMT) then
  begin
    FVMT.Free;
    FVMT := nil;
  end;
end;


procedure TrwLibComponent.InitVMT(AInstance: TrwComponent; APriffix: String = '');
var
  L: TStringList;
  i: Integer;
  VR: TrwVMTRow;
begin
  if Assigned(FVMT) and FVMT.FAlreadyInitialized then
    Exit;

  GlobalNameSpace.BeginWrite;
  try
    if Assigned(FVMT) and FVMT.FAlreadyInitialized then
      Exit;

    if not Assigned(FVMT) then
      FVMT := TrwVMT.Create(TrwVMTRow);

    L := TStringList.Create;
    try
      AInstance.GetEventsList(L, True);
      for i := 0 to L.Count-1 do
      begin
        VR := VMT.Add;
        if Length(APriffix) > 0 then
          VR.Name := APriffix + '.' + L[i]
        else
          VR.Name := L[i];
        VR.Text := AInstance.Events[L[i]].HandlersText;
        VR.CodeAddress := AInstance.Events[L[i]].FCodeAddress;
      end;
    finally
      L.Free;
    end;

    FVMT.FAlreadyInitialized := (Length(APriffix) = 0);

  finally
    GlobalNameSpace.EndWrite;
  end;
end;

procedure TrwLibComponent.SetData(const Value: String);
begin
  inherited;
  if Assigned(FVMT) then
  begin
     FVMT.Free;
     FVMT := nil;
  end;
end;

function TrwLibComponent.GetBaseClass: TrwComponentClass;
var
  Cl: IrwClass;
begin
  Result := TrwComponentClass(GetClass(ParentName));
  if not Assigned(Result) then
  begin
    Cl := rwRTTIInfo.FindClass(ParentName);
    Assert(Assigned(Cl), 'Class "' + ParentName + '" is not registered');
    Result := TrwComponentClass(Cl.GetVCLClass);
  end;
end;


function TrwLibComponent.IsInheritsFrom(const ArwClassName: string): Boolean;
var
  ParCl: IrwClass;
begin
  Result := SameText(Name, ArwClassName);
  if Result then
    Exit;

  Result := SameText(ArwClassName, ParentName);
  if not Result then
  begin
    ParCl := rwRTTIInfo.FindClass(ParentName);
    if Assigned(ParCl) then
      Result := ParCl.DescendantOf(ArwClassName);
  end;
end;

procedure TrwLibComponent.Assign(Source: TPersistent);
begin
  inherited;
  ParentName := TrwLibComponent(Source).ParentName;
  DsgnPaletteFlags := TrwLibComponent(Source).DsgnPaletteFlags;

  if TrwLibComponent(Source).OwnPictureIsEmpty then
    FreeAndNil(FPicture)
  else
  begin
    TrwLibComponent(Source).FPicture.Position := 0;
    ReadPicture(TrwLibComponent(Source).FPicture);
  end;
end;


procedure TrwLibComponent.Compile;
var
  C: TrwComponent;
  Chld: TComponent;
  i: Integer;

  procedure SetVMT(AOwner: TrwComponent; VMTPreffix: String = '');
  var
    Chld: TComponent;
    i: Integer;
  begin
    for i := 0 to AOwner.VirtualComponentCount - 1 do
    begin
      Chld := AOwner.GetVirtualChild(i);
      if Chld is TrwComponent then
      begin
        InitVMT(TrwComponent(Chld), VMTPreffix + Chld.Name);
        if TrwComponent(Chld).IsVirtualOwner then
          SetVMT(TrwCustomReport(Chld), VMTPreffix + Chld.Name + '.');
      end;
    end;
  end;

  procedure CompileChildren;
  const cLibTag = #13'_LibComponent'#6;
  var
    h, clName: String;
    i, len: Integer;
  begin
    h := Data;
    while h <> '' do
    begin
      i := Pos(cLibTag, h);
      if i = 0 then
        break;

      len := Byte(h[i + Length(cLibTag)]);
      clName := Copy(h, i + Length(cLibTag) + 1, len);
      Delete(h, 1, i + Length(cLibTag) + len);

      if AnsiSameText(clName, Name) then
        continue;

      i := TrwLibComponents(Collection).IndexOf(clName);
      if (i <> -1) and not TrwLibComponents(Collection)[i].FCompiled then
        TrwLibComponents(Collection)[i].Compile;  // compile child component (recursion)
    end;
  end;

begin
  i := TrwLibComponents(Collection).IndexOf(ParentName);
  if (i <> -1) and not TrwLibComponents(Collection)[i].FCompiled then
    TrwLibComponents(Collection)[i].Compile;  // compile Ancestor (recursion)

  CompileChildren;

  RWEngineExt.AppAdapter.UpdateEngineProgressBar('Loading component:  ' + Name +'  (' + DisplayName + ')', TrwLibCollection(Collection).FCompilingProgress);

  C := CreateComp(nil, False);

  RWEngineExt.AppAdapter.UpdateEngineProgressBar('Compiling component:  ' + Name +'  (' + DisplayName + ')', TrwLibCollection(Collection).FCompilingProgress);

  C.FLoading := True;
  C.Name := StringReplace(Name, ' ', '_', [rfReplaceAll]);
  C.Name := StringReplace(Name, '''', '_', [rfReplaceAll]);
  C.FLoading := False;

  C.LibDesignMode := True;
  try
    try
      C.FrwClassName := Name;
      C.Compile;
      C.FrwClassName := '';

      if not C.FixUpUserProperties then
        if IsItDesigner then
          ShowMessage(Name + ': User properties have been read incorrectly. Please recompile again.')
        else
          raise Exception.Create(Name + ': User properties have been read incorrectly. Please recompile again.');

      SaveComp(C);

      if Assigned(FVMT) then
      begin
        FVMT.Clear;
        FVMT.FAlreadyInitialized := False;
      end;

      SetVMT(C);

      if (C is TrwReport) and Assigned(TrwReport(C).InputForm) then
      begin
        for i :=0 to TrwReport(C).InputForm.VirtualComponentCount - 1 do
        begin
          Chld := TrwReport(C).InputForm.GetVirtualChild(i);
          if Chld is TrwComponent then
            InitVMT(TrwComponent(Chld), TrwReport(C).InputForm.Name + '.' + Chld.Name);
        end;
        InitVMT(TrwReport(C).InputForm, TrwReport(C).InputForm.Name);
      end;

      InitVMT(C, '');

    except
      on E: ErwParserError do
      begin
        E.LibComponentName := Name;
        if E.ComponentName = '' then
          E.ComponentName := cParserLibComp;
        raise;
      end
      else
        raise;
    end;

  finally
    C.Free;
    RWEngineExt.AppAdapter.UpdateEngineProgressBar(' ', TrwLibCollection(Collection).FCompilingProgress);
  end;

  inherited;
end;

procedure TrwLibComponent.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('Picture', ReadPicture, WritePicture, not OwnPictureIsEmpty);
end;

procedure TrwLibComponent.ReadPicture(AStream: TStream);
begin
  if not Assigned(FPicture) then
    FPicture := TIsMemoryStream.Create
  else
    FPicture.Clear;

  if Assigned(AStream) then
    FPicture.CopyFrom(AStream, 0);
end;

procedure TrwLibComponent.WritePicture(AStream: TStream);
begin
  AStream.CopyFrom(FPicture, 0);
end;

procedure TrwLibComponent.GetPicture(const ABitmap: TBitmap);
begin
  if Assigned(FPicture) then
  begin
    FPicture.Position := 0;
    ABitmap.LoadFromStream(FPicture);
  end
  else
    ABitmap.Assign(nil);
end;

procedure TrwLibComponent.SetPictureData(const AStream: TStream);
begin
  ReadPicture(AStream);
end;

function TrwLibComponent.OwnPictureIsEmpty: Boolean;
begin
  Result := not Assigned(FPicture) or (FPicture.Size = 0);
end;

function TrwLibComponent.GetClassName: String;
begin
  Result := Name;
end;

function TrwLibComponent.GetAncestorClassName: String;
begin
  Result := ParentName;
end;

procedure TrwLibComponent.SetClassName(const AClassName: String);
begin
  Name := AClassName;
end;

procedure TrwLibComponent.SetComponentData(const AComponent: IrmDesignObject);
begin
  if TrwComponent(AComponent.RealObject)._LibComponent = '' then
    TrwComponent(AComponent.RealObject).FLibComponent := TrwComponent(AComponent.RealObject).ClassName;

  ParentName := TrwComponent(AComponent.RealObject)._LibComponent;

  SaveComp(AComponent.RealObject);
end;

function TrwLibComponent.GetComponentData: IevDualStream;
begin
  Result := TEvDualStreamHolder.Create(Length(FData));
  Result.WriteBuffer(FData[1], Length(FData));
  Result.Position := 0;
end;

function TrwLibComponent.GetDsgnPaletteSettings: TrwDsgnPaletteSettings;
begin
  Result := DsgnPaletteSettings;
end;

procedure TrwLibComponent.SetDsgnPaletteSettings(const AValue: TrwDsgnPaletteSettings);
begin
  DsgnPaletteSettings := AValue;
end;

function TrwLibComponent.GetObjectType: TrwDsgnObjectType;
begin
  Result := ClassObjectType(GetBaseClass.ClassName);
end;


function TrwLibComponent.GetVCLClass: TClass;
begin
  Result := GetBaseClass;
end;

function TrwLibComponent.CreateComponent(const AExistingComponent: IrmDesignObject = nil; const AInherit: Boolean = False; const ADontRunFixUp: Boolean = False): IrmDesignObject;
var
 O: TrwComponent;
begin
  if AExistingComponent = nil then
    O := nil
  else
    O := TrwComponent(AExistingComponent.RealObject);

  Result := CreateComp(O, AInherit, ADontRunFixUp);
end;


procedure TrwLibComponent.GenerateNewID;
begin
  inherited;
  // GUID algorithm
end;

procedure TrwLibComponent.SetDsgnPaletteFlags(const Value: Byte);
begin
  DsgnPaletteSettings := [];

  if (Value and $01) <> 0 then
    DsgnPaletteSettings := DsgnPaletteSettings + [rwDPSComponentEditModeOnly];

  if (Value and $10) <> 0 then
    DsgnPaletteSettings := DsgnPaletteSettings + [rwDPSAdvancedModeOnly];

  if (Value and $26) <> 0 then
    DsgnPaletteSettings := DsgnPaletteSettings + [rwDPSShowOnPalette];
end;


function TrwLibComponent.IsItDesigner: boolean;
begin
  result := Pos('DESIGNER', UpperCase(ExtractFileName(Application.ExeName))) > 0;
end;

{ TrwVMT }

constructor TrwVMT.Create(ItemClass: TCollectionItemClass);
begin
  inherited Create(ItemClass);
  FAlreadyInitialized := False;
end;

function TrwVMT.Add: TrwVMTRow;
begin
  Result := TrwVMTRow(inherited Add);
end;

function TrwVMT.GetItem(Index: Integer): TrwVMTRow;
begin
  Result := TrwVMTRow(inherited GetItem(Index));
end;

procedure TrwVMT.SetItem(Index: Integer; Value: TrwVMTRow);
begin
  inherited SetItem(Index, Value);
end;

function TrwVMT.IndexOf(AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AName := AnsiUpperCase(AName);
  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].Name) = AName then
    begin
      Result := i;
      break;
    end;
end;




{TrwEventsList}

destructor TrwEventsList.Destroy;
begin
  Clear;

  inherited;
end;

procedure TrwEventsList.Clear;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].Free;

  inherited Clear;
end;

function TrwEventsList.GetItem(Index: Integer): TrwEvent;
begin
  Result := TrwEvent(inherited Items[Index]);
end;

procedure TrwEventsList.Delete(Index: Integer);
begin
  Items[Index].Free;

  inherited Delete(Index);
end;

function TrwEventsList.Add(AEventName: string; AEventParams: string; AResultType: TrwVarTypeKind; APointertType: String): Integer;
var
  lrwEvent: TrwEvent;
begin
  lrwEvent := TrwEvent.Create(Self);
  lrwEvent.Name := AEventName;
  lrwEvent.ParamStr := AEventParams;
  lrwEvent.Params.Result.VarType := AResultType;
  lrwEvent.Params.Result.PointerType := APointertType;
  lrwEvent.FCodeAddress := -1;

  Result := inherited Add(lrwEvent);
end;

function TrwEventsList.IndexOf(const AName: string): Integer;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to Count - 1 do
    if AnsiSameText(Items[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwEventsList.Assign(ASource: TrwEventsList);
var
  i, j: Integer;
begin
  if Assigned(FOwner) and FOwner.InheritedComponent then
  begin
    i := 0;
    while i < Count do
      if not Items[i].InheritedItem then
        Delete(i)
      else
        Inc(i);

    for i := 0 to ASource.Count-1 do
    begin
      if ASource.Items[i].InheritedItem then
      begin
        j := IndexOf(ASource.Items[i].Name);
        if j <> -1 then
          Items[j].HandlersText := ASource.Items[i].HandlersText;
      end
      else
      begin
        j := Add(ASource[i].Name, ASource[i].ParamStr, ASource[i].Params.Result.VarType,
                 ASource[i].Params.Result.PointerType);
        Items[j].HandlersText := ASource[i].HandlersText;
        Items[j].InheritedItem := False;
        Items[j].Params.Assign(ASource.Items[i].Params);
      end;
    end;
  end

  else
  begin
    Clear;
    for i := 0 to ASource.Count-1 do
    begin
      j := Add(ASource[i].Name, ASource[i].ParamStr, ASource[i].Params.Result.VarType,
               ASource[i].Params.Result.PointerType);
      Items[j].HandlersText := ASource[i].HandlersText;
      Items[j].InheritedItem := ASource[i].InheritedItem;
      Items[j].Params.Assign(ASource.Items[i].Params);
    end;
  end;
end;

procedure TrwEventsList.SetInherited(Value: Boolean);
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    Items[i].InheritedItem := Value;
end;



function TrwEventsList._AddRef: Integer;
begin
  Result := -1;
end;

function TrwEventsList._Release: Integer;
begin
  Result := -1;
end;

function TrwEventsList.AddItem: IrwCollectionItem;
var
  Evnt: TrwEvent;
begin
  Evnt := TrwEvent.Create(Self);
  inherited Add(Evnt);
  Result := Evnt;
end;

procedure TrwEventsList.BeginUpdate;
begin
end;

procedure TrwEventsList.DeleteItem(const AItemIndex: Integer);
begin
  Delete(AItemIndex);
end;

procedure TrwEventsList.EndUpdate;
begin
end;

function TrwEventsList.GetComponentOwner: IrmDesignObject;
begin
  Result := FOwner;
end;

function TrwEventsList.GetFunctionByName(const AFunctName: String): IrmFunction;
var
  i: Integer;
begin
  i := IndexOf(AFunctName);
  Result := Items[i];
end;

function TrwEventsList.GetItemByID(const AItemID: String): IrwCollectionItem;
begin
  Result := nil;
end;

function TrwEventsList.GetItemByIndex(const AItemIndex: Integer): IrwCollectionItem;
begin
  Result := Items[AItemIndex];
end;

function TrwEventsList.GetItemCount: Integer;
begin
  Result := Count;
end;

function TrwEventsList.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TrwEventsList.RealObject: TCollection;
begin
  Result := nil;
end;

function TrwEventsList.VCLObject: TObject;
begin
  Result := Self;
end;

procedure TrwEventsList.Compile;
begin
end;


procedure TrwEventsList.ClearDebInf;
begin
end;

procedure TrwEventsList.CreateDebInf;
begin
end;

function TrwEventsList.GetDebInf: PTrwDebInf;
begin
  Result := nil;
end;

function TrwEventsList.GetDebSymbInf: PTrwDebInf;
begin
  Result := nil;
end;

{TrwComponent}

constructor TrwComponent.Create(AOwner: TComponent);
begin
  inherited;

  FEventsList := TrwEventsList.Create;
  FEventsList.FOwner := Self;
  FLibDesignMode := False;
  FLoadingInheritances := False;
  FInheritedComponent := False;
  FLibComponent := '';
  FLibComponentRef := nil;
  FrwClassName := '';
  FDesignInfo := nil;
  FGlobalVarFunc := nil;
  FOnDestroy := nil;
  FLoading := False;
  FLoadingInheritances := False;
  FChildList := nil;
  FChildOwner := nil;
  FWritingOnlySelf := False;
  FIgnoreDesignMode := False;
  FVirtOwner := False;
  FDsgnLockState := rwLSUnlocked;

  FUserPropertiesWork := TrwUserProperties.Create(Self, TrwUserProperty);
  FUserPropertiesRead := TrwUserProperties.Create(Self, TrwUserProperty);

  FFormulas := TrmFMLFormulas.Create(Self, TrmFMLFormula);
  FAggrFunctList := TList.Create;

  RegisterComponentEvents;
end;

destructor TrwComponent.Destroy;
begin
  DestroyVisualControl;

  BeforeDestruction;

  FreeAndNil(FEventsList);
  FreeAndNil(FDesignInfo);

  FreeAndNil(FUserPropertiesWork);
  FreeAndNil(FUserPropertiesRead);

  FreeAndNil(FAggrFunctList);
  FreeAndNil(FFormulas);

  if Assigned(FOnDestroy) then
    FOnDestroy(Self);

  inherited;
end;

function TrwComponent.IsDesignMode: Boolean;
begin
  if Assigned(Owner) and (Owner is TrwComponent) then
    Result := TrwComponent(Owner).IsDesignMode
  else
    Result := LibDesignMode or Assigned(FRMDesigner);

  Result := Result and not FIgnoreDesignMode;
end;

function TrwComponent.GetIndexOfEvent(const AName: string): Integer;
begin
  Result := EventsList.IndexOf(AName);

  if Result = -1 then
    raise ErwException.CreateFmt(ResErrWrongEvent, [AName, ClassName]);
end;


function TrwComponent.GetEvent(const AName: string): TrwEvent;
var
  i: Integer;
begin
  i := GetIndexOfEvent(AName);
  if i <> -1 then
    Result := EventsList[i]
  else
    Result := nil;
end;


procedure TrwComponent.RegisterEvents(const AEvent: array of string; AUserEvents: Boolean = False);
var
  i, j, k, Pb, Pe, P: Integer;
  lName, h: string;
begin
  for i := Low(AEvent) to High(AEvent) do
  begin
    h := Trim(AEvent[i]);
    j := EventsList.Add('', '', rwvUnknown, '');
    try
      lName := ParseFunctionHeader(AEvent[i], EventsList[j].Params, Pb, Pe, P);
      EventsList[j].ParamStr := Trim(Copy(h, Pb, Pe-Pb));
    except
      EventsList.Delete(j);
      raise;
    end;

    k := EventsList.IndexOf(lName);
    if AUserEvents then
    begin
      if k <> -1 then
        EventsList.Delete(j)
      else
      begin
        EventsList[j].Name := lName;
        EventsList[j].UserEvent := True;
      end;
    end

    else
    begin
      EventsList[j].Name := lName;
      EventsList[j].UserEvent := False;
      if k <> -1 then
        EventsList.Delete(k);
    end;
  end;
end;

procedure TrwComponent.UnRegisterEvents(const AEvent: array of string);
var
  i, j: Integer;
  lName, lParams, h: string;
begin
  for i := Low(AEvent) to High(AEvent) do
  begin
    h := Trim(AEvent[i]);
    j := Pos(' ', h);
    if j <> 0 then
    begin
      Delete(h, 1, j);
      h := Trim(h);
    end;  

    j := Pos('(', h);
    if j = 0 then
    begin
      lName := h;
      lParams := '';
    end
    else
    begin
      lName := Trim(Copy(h, 1, j - 1));
      lParams := Trim(Copy(h, j + 1, Length(h) - j - 1));
    end;

    j := EventsList.IndexOf(lName);
    if j <> -1 then
      EventsList.Delete(j);
  end;
end;

function TrwComponent.IsNotEmptyEvents: Boolean;
var
  i: Integer;
  h: String;
begin
  Result := False;
  for i := 0 to EventsList.Count - 1 do
  begin
    h := EventsList[i].HandlersText;
    if (h <> '') and not (EventsList[i].InheritedItem and IsEmptyInheritedEvent(Trim(h)))
       or
       (FEventsList[i].UserEvent and not FEventsList[i].InheritedItem) then
    begin
      Result := True;
      break;
    end;
  end;
end;

procedure TrwComponent.DefineProperties(Filer: TFiler);
begin
  inherited;

  Filer.DefineProperty('Events', ReadEventsData, WriteEventsData, IsNotEmptyEvents);
end;

procedure TrwComponent.ReadEventsData(Reader: TReader);
var
  lEvent, lEventName: string;
  i, j, a: Integer;
  c, u: Boolean;
begin
  if not FInheritedComponent then
    for i := 0 to EventsList.Count - 1 do
      if not FEventsList[i].InheritedItem then
        FEventsList[i].HandlersText := '';

  Reader.ReadListBegin;

  while not Reader.EndOfList do
  begin
    lEvent := Reader.ReadString;

    i := Pos(#13, lEvent);
    lEventName := Copy(lEvent, 1, i - 1);
    Delete(lEvent, 1, i);
    i := Pos(#01, lEventName);
    if i > 0 then                    //inherited event flag
    begin
      Delete(lEventName, i, 1);
      c := True;
    end
    else
      c := False;

    i := Pos(#03, lEventName);         //user event flag
    if i > 0 then
    begin
      u := True;
      Delete(lEventName, i, 1);
    end
    else
      u := False;

    i := Pos(#02, lEventName);       //entry point in the P-code
    if i > 0 then
    begin
      a := StrToInt(Copy(lEventName, i + 1, Length(lEventName) - i));
      Delete(lEventName, i, Length(lEventName) - i + 1);
    end
    else
      a := -1;

    j := EventsList.IndexOf(lEventName);

    if u and not c and (j = -1) then
    begin
      RegisterEvents([lEventName], True);
      j := EventsList.Count - 1;
    end;

    if (j <> -1) then
    begin
      if not IsEmptyInheritedEvent(Trim(lEvent)) then
      begin
        FEventsList[j].HandlersText := lEvent;
        FEventsList[j].FCodeAddress := a;
      end;  
      FEventsList[j].InheritedItem := c;
      FEventsList[j].UserEvent := u;
    end;
  end;

  Reader.ReadListEnd;
end;

procedure TrwComponent.WriteEventsData(Writer: TWriter);
var
  i: Integer;
  h: string;
  c, u: String[1];
  Addr, Ev: String;
begin
  Writer.WriteListBegin;
  for i := 0 to EventsList.Count - 1 do
  begin
    h := TrimRight(FEventsList[i].HandlersText);

    if FEventsList[i].InheritedItem then
      c := #01
    else
      c := '';

    if FEventsList[i].UserEvent then
      u := #03
    else
      u := '';

    if FEventsList[i].FCodeAddress <> -1 then
      Addr := #02+IntToStr(FEventsList[i].FCodeAddress)
    else
      Addr := '';

    if FEventsList[i].UserEvent and not FEventsList[i].InheritedItem then
      Ev := MakeEventHandlerHeader(FEventsList[i])
    else
      Ev := FEventsList[i].Name;

    if (h <> '') and not IsEmptyInheritedEvent(Trim(h))
        or
       (FEventsList[i].UserEvent and not FEventsList[i].InheritedItem) then
      Writer.WriteString(Ev + c + u + Addr + #13 + h);
  end;
  Writer.WriteListEnd;
end;


procedure TrwComponent.RegisterComponentEvents;
begin
end;

procedure TrwComponent.GetEventsList(AStrings: TStrings; ANoEmpty: Boolean = False);
var
  i: Integer;
begin
  AStrings.Clear;
  for i := 0 to EventsList.Count - 1 do
    if not ANoEmpty or (FEventsList[i].HandlersText <> '') then
      AStrings.AddObject(FEventsList[i].Name, FEventsList[i]);
end;


procedure TrwComponent.ExecuteEventsHandlerByNumber(const EventNum: Integer);
var
  OldComp: TrwComponent;
  EP: TrwVMAddress;
begin
  EP := GetEntryPoint(EventsList[EventNum].FCodeAddress);
  if Debugging then
  begin
    OldComp := DebugInfo.CurrentComponent;
    DebugInfo.CurrentComponent := Self;
  end
  else
    OldComp := nil;

  VM.Push(Integer(Self));
  VM.Run(EP);

  if Debugging then
    DebugInfo.CurrentComponent := OldComp;
end;


procedure TrwComponent.ExecuteEventsHandler(const AEventName: string; const AParams: array of variant);
var
  i, n: Integer;
  lEvent: TrwEvent;
begin
  n := GetIndexOfEvent(AEventName);
  lEvent := EventsList[n];

  if (lEvent.FCodeAddress = -1) or IsDesignMode or
     (csLoading in ComponentState) or (csDestroying in ComponentState) or
      FLoadingInheritances or FLoading then Exit;

  for i := Low(AParams) to High(AParams) do
    VM.Push(AParams[i]);

  ExecuteEventsHandlerByNumber(n);
end;

procedure TrwComponent.RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation);
begin
end;

procedure TrwComponent.SendNotification(AOperation: TrwNotifyOperation);
begin
end;

procedure TrwComponent.InitializeForDesign;
begin
  if Name = '' then
    Name := GetUniqueNameForComponent(Self);
end;

procedure TrwComponent.SetDesignParent(Value: TWinControl);
begin
  if Assigned(Value) and ((DsgnLockState <> rwLSHidden) or LibDesignMode or (GetDesigner <> nil) and (GetDesigner.LibComponentDesigninig)) then
    VisualControl.RealObject.Parent := Value
  else
    DestroyVisualControl;
end;


procedure TrwComponent.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  if FLibDesignMode or IsVirtualOwner then
    for i := 0 to ComponentCount - 1 do
      if (Components[i] is TrwComponent) then
        Proc(Components[i]);
end;


function TrwComponent.IsLibComponent: Boolean;
begin
  Result := FLibComponent <> '';
end;


procedure TrwComponent.HideInheritedEvent;
var
  i: Integer;
begin
  for i := 0 to EventsList.Count-1 do
  begin
    if FEventsList[i].HandlersText <> '' then
      FEventsList[i].HandlersText := '  ' + cInheritedKeyWord +';';
    FEventsList[i].InheritedItem := True;
  end;
end;


procedure TrwComponent.SetLibComponent(Value: String);
var
  OldLoading: Boolean;
begin
  if FLoadingInheritances then
    Exit;

  OldLoading := FLoading;
  FLoadingInheritances := True;
  try
    if Value <> '' then
      LoadFromLibComp(Value)
    else
      FLibComponent := Value;
  finally
    FLoadingInheritances := False;
    FLoading := OldLoading;
  end;
end;


procedure TrwComponent.LoadFromLibComp(const AClassName: String);
var
  C: IrmComponent;
begin
  if Assigned(GetClass(AClassName)) then
  begin
    FLibComponentRef := nil;
    FLibComponent := AClassName;
    Exit;
  end;

  C := GetLibComponentByClass(AClassName);
  if not Assigned(C) then
    raise ErwException.CreateFmt(ResErrWrongLibComp, [AClassName]);

  LoadFromLibComp(C);
end;

procedure TrwComponent.LoadFromLibComp(const ALibComponent: IrmComponent);

  procedure SetInheritedForChildren(AOwner: TrwComponent; VMTPreffix: String = '');
  var
    i: Integer;
  begin
    AOwner.FUserPropertiesWork.SetInherited(True);
    for i :=0 to AOwner.ComponentCount-1 do
      if AOwner.Components[i] is TrwComponent then
      begin
        TrwLibComponent(FLibComponentRef.RealObject).InitVMT(TrwComponent(AOwner.Components[i]), VMTPreffix + AOwner.Components[i].Name);
        TrwComponent(AOwner.Components[i]).SetInheritedComponentProperty(True);
        TrwComponent(AOwner.Components[i]).HideInheritedEvent;
        if AOwner.Components[i] is TrwGlobalVarFunct then
        begin
          TrwGlobalVarFunct(AOwner.Components[i]).Variables.SetInherited(True);
          TrwGlobalVarFunct(AOwner.Components[i]).LocalComponents.SetInherited(True);          
          TrwGlobalVarFunct(AOwner.Components[i]).EventsList.SetInherited(True);
        end;

        if TrwComponent(AOwner.Components[i]).IsVirtualOwner then
          SetInheritedForChildren(TrwComponent(AOwner.Components[i]), VMTPreffix + AOwner.Components[i].Name + '.')
        else if AOwner.Components[i] is TrwReportForm then
          SetInheritedForChildren(TrwComponent(AOwner.Components[i]), VMTPreffix);
      end;
  end;

begin
  LoadFromLibComp(ALibComponent.GetAncestorClassName);
  ALibComponent.CreateComponent(Self);
  FLibComponentRef := ALibComponent;

  SetInheritedForChildren(Self);

  FLibComponent := FLibComponentRef.GetClassName;
  TrwLibComponent(FLibComponentRef.RealObject).InitVMT(Self);
  HideInheritedEvent;

  AfterInitLibComponent;
end;


procedure TrwComponent.AncestorNotFound(Reader: TReader;
  const ComponentName: string; ComponentClass: TPersistentClass;
  var Component: TComponent);
begin
  if Reader.Root <> Reader.LookupRoot then
    Component := Reader.Root.FindComponent(ComponentName);
  if not Assigned(Component) then
  begin
    Component := TrwComponentClass(ComponentClass).Create(Reader.Owner);
    Component.Name := ComponentName;
  end;
end;


procedure TrwComponent.ReadState(Reader: TReader);
var
  OldAncestorNotFound: TAncestorNotFoundEvent;
begin
  FLoading := True;
  if Reader.Owner = Self then
  begin
    OldAncestorNotFound := Reader.OnAncestorNotFound;
    Reader.OnAncestorNotFound := AncestorNotFound;
  end
  else
    OldAncestorNotFound := nil;

  inherited ReadState(Reader);

  if not FLoadingInheritances then
    RWLoaded;

  if Reader.Owner = Self then
    Reader.OnAncestorNotFound := OldAncestorNotFound;
end;


procedure TrwComponent.WriteState(Writer: TWriter);
var
  AncComp: TPersistent;
  OldAnc:  TPersistent;
  OldRootAncestor: TComponent;
  flFreeAncestor: Boolean;
  i, j: Integer;
  fl: Boolean;
  P: TrwUserProperty;
  Props: TrwUserProperties;
  Val1, Val2: Variant;
  FOldInhComp: Boolean;
begin
  flFreeAncestor := False;
  AncComp := nil;

  FOldInhComp := FInheritedComponent;
  if FInheritedComponent and not Assigned(Writer.Ancestor) then  //for Copy operation
    FInheritedComponent := False;

  OldAnc := Writer.Ancestor;
  OldRootAncestor := Writer.RootAncestor;

  try
    if not FInheritedComponent and IsLibComponent then
    begin
      AncComp := TrwComponentClass(ClassType).Create(nil);
      TComponent(AncComp).Name := Name;
      flFreeAncestor := True;
//      TrwComponent(AncComp)._LibComponent := _LibComponent;
      TrwComponent(AncComp).SetLibComponentByRef(FLibComponentRef);
      TrwComponent(AncComp).FLibComponent := '';
      TrwComponent(AncComp).LibDesignMode := True;
      Writer.Ancestor := AncComp;
      if ((Writer.Ancestor is TrwReport) or (Writer.Ancestor is TrmReport) or LibDesignMode) then
        Writer.RootAncestor := TrwComponent(AncComp)
      else
      begin
        Writer.RootAncestor := Writer.Root;  // Very important! Otherwise ancestor validation fails
        SetInline(True);
      end;
    end;

    if Writer.Ancestor is TrwGlobalVarFunct then
      TrwGlobalVarFunct(Writer.Ancestor).FAncestorMode := True;

    FUserPropertiesRead.ClearNotInLimbo;
    for i := 0 to FUserPropertiesWork.Count - 1 do
    begin
      if FUserPropertiesRead.IndexOf(FUserPropertiesWork[i].Name) <> -1 then
        Continue;

      if Assigned(Writer.Ancestor) and FUserPropertiesWork[i].InheritedItem then
      begin
        fl := False;
        Props := TrwComponent(Writer.Ancestor)._UserProperties;
        j := Props.IndexOf(FUserPropertiesWork[i].Name);
        try
          Val1 := FUserPropertiesWork[i].Value;
          Val2 := Props[j].Value;
          if (Val1 = Null) and (Val2 <> Null) or (Val1 <> Null) and (Val2 = Null) or
              not FUserPropertiesWork[i].IsTheSame(Props[j]) then
            fl := True;
        except
        end;
      end
      else
        fl := True;

      if fl then
      begin
        P := FUserPropertiesRead.Add;
        P.Assign(FUserPropertiesWork[i]);
      end;
    end;

    inherited;

  finally
    FUserPropertiesRead.ClearNotInLimbo;

    FInheritedComponent := FOldInhComp;
    Writer.Ancestor := OldAnc;
    Writer.RootAncestor := OldRootAncestor;
    if flFreeAncestor then
    begin
      SetInline(False);
      AncComp.Free;
    end;
  end;
end;


procedure TrwComponent.Loaded;
var
  i: Integer;
begin
  SetInline(False);

  if Assigned(FGlobalVarFunc) and (FGlobalVarFunc.Owner = Self) then
    FGlobalVarFunc.ComponentIndex := 0;
  FLoading := False;

  for i := 0 to FUserPropertiesRead.Count -1 do
    if not FUserPropertiesRead[i].InheritedItem then
      if FUserPropertiesWork.IndexOf(FUserPropertiesRead[i].Name) = -1 then
        FUserPropertiesWork.Add.Assign(FUserPropertiesRead[i]);

  inherited;
end;


function TrwComponent.IsInheritsFrom(AClassName: string): Boolean;
var
  Cl: IrwClass;
begin
  if IsLibComponent then
  begin
    Cl := rwRTTIInfo.FindClass(_LibComponent);
    if Assigned(Cl) then
      Result := Cl.DescendantOf(AClassName)
    else
      Result := False;
  end
  else
    Result := False;  
end;


procedure TrwComponent.InitGlobalVarFunc;
begin
  RemoveGlobalVarFunc;
  TrwGlobalVarFunct.Create(Self);
  Self.GlobalVarFunc.Name := VAR_FUNCT_COMP_NAME;
  Self.GlobalVarFunc.ComponentIndex := 0;
end;


function TrwComponent.CanWriteLibInfo: Boolean;
begin
  Result := (FLibComponent <> '') and not FInheritedComponent;
end;


procedure TrwComponent.SetInheritedComponentProperty(AValue: Boolean);
begin
  FInheritedComponent := AValue;

  _UserProperties.SetInherited(AValue);

  if Assigned(FGlobalVarFunc) then
  begin
    FGlobalVarFunc.Variables.SetInherited(AValue);
    FGlobalVarFunc.EventsList.SetInherited(AValue);
  end;
end;

function TrwComponent.GetEventCount: Integer;
begin
  Result := EventsList.Count;
end;

function TrwComponent.EventByIndex(AIndex: Integer): TrwEvent;
begin
  Result := FEventsList[AIndex];
end;

procedure TrwComponent.AfterInitLibComponent;
begin
end;

function TrwComponent.GetDesignInfo: TrwDesignInfo;
begin
  if not Assigned(FDesignInfo) then
    FDesignInfo := TrwDesignInfo.Create;
  Result := FDesignInfo;
end;


procedure TrwComponent.CompileChildren(ASyntaxCheck: Boolean = False);
var
  i: Integer;
  C: TComponent;
begin
  for i := 0 to ComponentCount - 1 do
  begin
    C := Components[i];
    if C is TrwComponent then
      TrwComponent(C).Compile(ASyntaxCheck);
  end;
end;


procedure TrwComponent.CompileEvents(ASyntaxCheck: Boolean = False);
var
  i: Integer;
  L: TStringList;
  A: Integer;
begin
  L := TStringList.Create;
  try
    GetEventsList(L);
    for i := 0 to L.Count -1 do
    begin
      A := TrwEvent(L.Objects[i]).FCodeAddress;
      CompileEvent(TrwEvent(L.Objects[i]));
      if ASyntaxCheck then
        TrwEvent(L.Objects[i]).FCodeAddress := A;
    end;
  finally
    L.Free;
  end;
end;


function TrwComponent.VirtualOwner: TrwComponent;
begin
  if Owner is TrwComponent then
    if Owner is TrwReportForm then
      Result := TrwCustomReport(TrwReportForm(Owner).Owner)
    else
      Result := TrwComponent(Owner)

  else if IsLibComponent then
    Result := Self
  else
    Result := nil;
end;


procedure TrwComponent.CompileEvent(const AEvent: TrwEvent);
var
  h: String;
begin
  h := AEvent.HandlersText;
  if h = '' then
    AEvent.FCodeAddress := -1

  else if not IsEmptyInheritedEvent(Trim(h)) then
  begin
    h := MakeEventHandlerHeader(AEvent) + #13#10 + h + #13#10 + 'end' + #0;
    AEvent.FCodeAddress := GetCompilingPointer + GetEntryPointOffset;
    CompileHandler(Self, h);
  end;
end;

function TrwComponent.FindVirtualChild(const AName: string): Integer;
var
  C: TComponent;
begin
  Result := -1;

  if IsVirtualOwner and Assigned(GlobalVarFunc) and SameText(AName, VAR_FUNCT_COMP_NAME) then
    Result := -3
  else
  begin
    C := FindComponent(AName);
    if Assigned(C) then
      Result := C.ComponentIndex
    else if IsVirtualOwner and SameText(AName, Name) then
      Result := -2;
  end;
end;

function TrwComponent.GetVirtualChild(const AIndex: Integer): TComponent;
begin
  Result := nil;
  if IsVirtualOwner then
    if AIndex = -2 then
      Result := Self
    else if AIndex = -3 then
      Result := GlobalVarFunc;

  if not Assigned(Result) then
    Result := Components[AIndex];
end;


procedure TrwComponent.Compile(ASyntaxCheck: Boolean = False);
begin
  if Assigned(GlobalVarFunc) and Assigned(GlobalVarFunc.LocalComponents) then
    GlobalVarFunc.LocalComponents.Compile;

  CompileEvents(ASyntaxCheck);
  Formulas.Compile(ASyntaxCheck);
  CompileChildren(ASyntaxCheck);
end;


function TrwComponent.VirtualComponentCount: Integer;
begin
  Result := ComponentCount;
end;


function TrwComponent.PPropertyExists(APropertyName: Variant): Variant;
begin
  Result := PropertyExists(APropertyName);
end;


function TrwComponent.PGetPropertyValue(APropertyName: Variant): Variant;
begin
  Result := GetPropertyValue(APropertyName);
end;


procedure TrwComponent.PSetPropertyValue(APropertyName: Variant; Value: Variant);
begin
  SetPropertyValue(APropertyName, Value);
end;


function TrwComponent.POwner: Variant;
begin
  Result := Integer(Pointer(VirtualOwner));
end;


function TrwComponent.PComponentByIndex(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(GetVirtualChild(AIndex)));
end;


function TrwComponent.PComponentCount: Variant;
begin
  Result := VirtualComponentCount;
end;


function TrwComponent.PFindComponent(AName: Variant): Variant;
var
  i: Integer;
begin
  i := FindVirtualChild(AName);
  if i <> -1 then
    Result := Integer(Pointer(GetVirtualChild(i)))
  else
    Result := 0;
end;


function TrwComponent.PComponentIndexByName(AName: Variant): Variant;
begin
  Result := FindVirtualChild(AName);
end;


procedure TrwComponent.InsertVirtualComponent(AComponent: TrwComponent);
begin
  InsertComponent(AComponent);
end;


procedure TrwComponent.FGetChildProc(Child: TComponent);
begin
  if Child.Owner = FChildOwner then
  begin
    FChildList.Add(Child);
    TrwComponent(Child).GetChildrenList(FChildList, FChildOwner);
  end;  
end;


procedure TrwComponent.GetChildrenList(AList: TList; AOwner: TrwComponent);
begin
  FChildList := AList;
  FChildOwner := AOwner;
  GetChildren(FGetChildProc, nil);
end;


procedure TrwComponent.FixUpCompIndexes;
var
  L: TList;
  i: Integer;
begin
  L := TList.Create;
  try
    GetChildrenList(L, Self);
    for i := ComponentCount - 1 downto 0 do
      if not (Components[i] is TrwComponent) or
         TrwComponent(Components[i]).InheritedComponent then
        continue
      else
        RemoveComponent(Components[i]);

    for i := 0 to L.Count - 1 do
      if not TrwComponent(L[i]).InheritedComponent then
        InsertComponent(TComponent(L[i]));

  finally
    L.Free;
  end;
end;


function TrwComponent.PClassName: Variant;
begin
  Result := GetClassName;
end;


function TrwComponent.FixUpUserProperties: Boolean;
var
  i: Integer;
  C: TComponent;
begin
  Result := True;

  for i := 0 to ComponentCount - 1 do
  begin
    C := Components[i];
    if C is TrwComponent then
      Result := Result and TrwComponent(C).FixUpUserProperties;
  end;

  if Assigned(GlobalVarFunc) then
  begin
    for i := 0 to FUserPropertiesRead.Count -1 do
      Result := Result and FUserPropertiesRead[i].FixUpValue;

    FUserPropertiesRead.ClearNotInLimbo;
  end;
end;


function TrwComponent.UserPropertyIsNotEmpty: Boolean;
begin
  Result := (_UserProperties.Count > 0);
end;

function TrwComponent.GetUserProperties: TrwUserProperties;
begin
  if Loading or (csLoading in ComponentState) or (csWriting in ComponentState) then
    Result := FUserPropertiesRead
  else
    Result := FUserPropertiesWork;
end;

procedure TrwComponent.SetUserProperties(const Value: TrwUserProperties);
begin
  _UserProperties.Assign(Value);
end;

procedure TrwComponent.RemoveGlobalVarFunc;
begin
  FreeAndNil(FGlobalVarFunc);
end;


function TrwComponent.RwClassNameIsNotEmpty: Boolean;
begin
  Result := Length(FrwClassName) > 0;
end;


function TrwComponent.PExecEvent(AEventName, AParams: Variant): Variant;
var
  Ev: TrwEvent;
  P: array of Variant;
  i: Integer;
begin
  Ev := Events[AEventName];
  if Assigned(Ev) then
  begin
    SetLength(P, VarArrayHighBound(AParams, 1) - VarArrayLowBound(AParams, 1) + 1);
    for i := Low(P) to High(P) do
      P[i] := AParams[i];

    ExecuteEventsHandler(AEventName, P);
    if Ev.Params.Result.VarType = rwvUnknown then
      Result := varUnknown
    else
      Result := VM.RegA;
  end
  else
    raise ErwException.CreateFmt(ResErrWrongEvent, [AEventName, PClassName]);
end;


procedure TrwComponent.WriteComp(Writer: TWriter);
begin
  FWritingOnlySelf := True;
  try
    Writer.WriteSignature;
    Writer.WriteComponent(Self);
  finally
    FWritingOnlySelf := False;
  end;
end;

procedure TrwComponent.Assign(Source: TPersistent);
begin
end;

procedure TrwComponent.DoSyncVisualControl(const APropID: TrwChangedPropID);
begin
  if VisualControlCreated then
    FVisualControl.PropChanged(APropID);
end;

function TrwComponent.CreateVisualControl: IrwVisualControl;
begin
  Result := nil;
end;


function TrwComponent.GetVisualControl: IrwVisualControl;
begin
  if not VisualControlCreated then
  begin
    FVisualControl := CreateVisualControl;
    DoSyncVisualControl(rwCPAll);
  end;

  Result := FVisualControl;
end;

procedure TrwComponent.DestroyVisualControl;
begin
  if VisualControlCreated then
    try
      FVisualControl.RealObject.Free;
    finally
      FVisualControl := nil;
    end;
end;

function TrwComponent.GetDesignParent: TWinControl;
begin
  if VisualControlCreated then
    Result := VisualControl.RealObject.Parent
  else
    Result := nil;
end;

procedure TrwComponent.SetVisualControl(const Value: IrwVisualControl);
begin
  Assert(not Assigned(Value), 'Prohibited operation');
  FVisualControl := nil
end;

function TrwComponent.VisualControlCreated: Boolean;
begin
  Result := Assigned(FVisualControl);
end;

function TrwComponent.PInheritsFrom(AClassName: Variant): Variant;
begin
  Result := ObjectInheritsFrom(AClassName);
end;

procedure TrwComponent.SetDesigner(ADesigner: IrmDesigner);
var
  i: Integer;
begin
  LibDesignMode := Assigned(ADesigner) and ADesigner.LibComponentDesigninig and
                   (ADesigner.DesigningObject <> nil) and (ADesigner.DesigningObject.RealObject = Self);

  FRMDesigner := ADesigner;


  for i := 0 to ComponentCount - 1 do
    TrwComponent(Components[i]).SetDesigner(ADesigner);

  if LibDesignMode and not Assigned(GlobalVarFunc) then
    InitGlobalVarFunc;
end;

procedure TrwComponent.SetExternalGlobalVarFunc(AGlobalVarFunc: TrwGlobalVarFunct);
begin
  FGlobalVarFunc := AGlobalVarFunc;
end;

procedure TrwComponent.SetDsgnLockState(const Value: TrwDsgnLockState);
begin
  FDsgnLockState := Value;
end;

function TrwComponent.GetDsgnLockState: TrwDsgnLockState;
begin
  Result := FDsgnLockState;
end;


function TrwComponent.GetDescription: String;
begin
  Result := FDescription;
end;

procedure TrwComponent.SetDescription(const Value: String);
begin
  FDescription := Value;
end;

function TrwComponent.RealObject: TComponent;
begin
  Result := Self;
end;

function TrwComponent.GetWinControlParent: TWinControl;
begin
  Result := DesignParent;
end;

procedure TrwComponent.Notify(AEvent: TrmDesignEventType);
begin
  if VisualControlCreated then
    VisualControl.Notify(AEvent);
end;

function TrwComponent.GetParentExtRect: TRect;
begin
  Result := VisualControl.RealObject.BoundsRect;
end;

function TrwComponent.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  if VisualControlCreated then
    Result := VisualControl.DesignBehaviorInfo
  else
  begin
    Result.MovingDirections := [];
    Result.ResizingDirections := [];
    Result.Selectable := False;
    Result.SelectorType := rwDBISBNone;
    Result.StandaloneObject := False;
    Result.AcceptControls := False;
    Result.AcceptedClases := '';
  end;
end;

function TrwComponent.ControlAtPos(APos: TPoint): TrwComponent;
begin
  Result := nil;
end;

function TrwComponent.GetParent: IrmDesignObject;
begin
  Result := nil;
end;

function TrwComponent.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTInternal;
end;

function TrwComponent.ClientToScreen(APos: TPoint): TPoint;
begin
  Result := VisualControl.RealObject.ClientToScreen(APos);
end;

function TrwComponent.ScreenToClient(APos: TPoint): TPoint;
begin
  Result := VisualControl.RealObject.ScreenToClient(APos);
end;

function TrwComponent.GetBoundsRect: TRect;
begin
  Result := VisualControl.RealObject.BoundsRect;
end;

procedure TrwComponent.SetBoundsRect(ARect: TRect);
begin
end;

function TrwComponent.DontStoreProperty: Boolean;
begin
  Result := IsDesignMode and not (csWriting in ComponentState);
end;

function TrwComponent.DontStoreIndProperty(Index: Integer): Boolean;
begin
  Result := DontStoreProperty;
end;

function TrwComponent.GetPropertyValue(const APropertyName: String): Variant;
var
  i: Integer;
begin
  Result := varUnknown;
  if Assigned(GlobalVarFunc) then
  begin
    i:= GlobalVarFunc.Variables.IndexOf(APropertyName);
    if (i <> -1) and (GlobalVarFunc.Variables[i].Visibility in [rvvPublished, rvvPublic]) then
    begin
      Result := GlobalVarFunc.Variables[i].Value;
      Exit;
    end;
  end;

  i:= _UserProperties.IndexOf(APropertyName);
  if i <> -1 then
  begin
    Result := _UserProperties[i].Value;
    Exit;
  end;

  if Pos('.', APropertyName) <> 0 then
    Result := GetComponentProperty(Self, APropertyName)
  else
    Result := GetPropValue(Self, APropertyName, False);
end;


function TrwComponent.PropertyExists(const APropertyName: String): Boolean;
var
 s: string;
 i: Integer;
begin
  if Assigned(GlobalVarFunc) then
  begin
    i := GlobalVarFunc.Variables.IndexOf(APropertyName);
    Result := (i <> -1) and (GlobalVarFunc.Variables[i].Visibility in [rvvPublished, rvvPublic]);
    if Result then
      Exit;
  end;

  Result := _UserProperties.IndexOf(APropertyName) <> -1;
  if Result then
    Exit;

  s := APropertyName;
  Result := IsPublishedProp(Self, s);
end;

procedure TrwComponent.SetPropertyValue(const APropertyName: String; Value: Variant);
var
  i: Integer;
begin
  if Assigned(GlobalVarFunc) then
  begin
    i:= GlobalVarFunc.Variables.IndexOf(APropertyName);
    if (i <> -1) and (GlobalVarFunc.Variables[i].Visibility in [rvvPublished, rvvPublic])  then
    begin
      GlobalVarFunc.Variables[i].Value := Value;
      Exit;
    end;
  end;

  i := _UserProperties.IndexOf(APropertyName);
  if i <> -1 then
  begin
    _UserProperties[i].Value := Value;
    Exit;
  end;

//  if Pos('.', APropertyName) <> 0 then
  SetComponentProperty(Self, APropertyName, Value)
//  else
//    SetPropValue(Self, APropertyName, Value);
end;


function TrwComponent.GetClassName: String;
begin
  if Length(FrwClassName) > 0 then
    Result := FrwClassName
  else if IsLibComponent then
    Result := _LibComponent
  else
    Result := ClassName;
end;

function TrwComponent.ObjectInheritsFrom(const AClassName: String): Boolean;
var
  Cl: TClass;
begin
  if IsLibComponent then
    Result := IsInheritsFrom(AClassName)
  else
    Result := False;

  if not Result then
  begin
    Cl := GetClass(AClassName);
    if Assigned(Cl) then
      Result := InheritsFrom(Cl)
    else
      Result := False;
  end;
end;


procedure TrwComponent.SetParent(AParent: IrmDesignObject);
begin
end;

function TrwComponent.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  C: TComponent;
begin
  Result := CreateRWComponentByClass(RWEngineExt.AppAdapter.SubstDefaultClassName(AClassName));

  if Assigned(Result) then
    try
      AddChildObject(Result);
    except
      try
        C := Result.RealObject;
        Result := nil;
        FreeAndNil(C);
      except
      end;

      raise;
    end;
  
  if IsDesignMode then
  begin
    Result.InitializeForDesign;
    Result.SetDesigner(GetDesigner);
  end;
end;


function TrwComponent.ObjectAtPos(APos: TPoint): IrmDesignObject;
begin
  Result := ControlAtPos(APos);
end;

function TrwComponent.GetChildByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := TrwComponent(Components[AIndex]);
end;

function TrwComponent.GetChildrenCount: Integer;
begin
  Result := ComponentCount;
end;

function TrwComponent.GetControlByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := nil;
end;

function TrwComponent.GetControlCount: Integer;
begin
  Result := 0;
end;

function TrwComponent.GetObjectOwner: IrmDesignObject;
begin
  Result := TrwComponent(Owner);
end;

function TrwComponent.GetHeight: Integer;
begin
  Result := 0;
end;

function TrwComponent.GetLeft: Integer;
begin
  Result := 0;
end;

function TrwComponent.GetTop: Integer;
begin
  Result := 0;
end;

function TrwComponent.GetWidth: Integer;
begin
  Result := 0;
end;

procedure TrwComponent.SetHeight(AValue: Integer);
begin
end;

procedure TrwComponent.SetLeft(AValue: Integer);
begin
end;

procedure TrwComponent.SetTop(AValue: Integer);
begin
end;

procedure TrwComponent.SetWidth(AValue: Integer);
begin
end;

function TrwComponent.IsVirtualOwner: Boolean;
begin
  Result := FVirtOwner;
end;

function TrwComponent.GetDesigner: IrmDesigner;
begin
  Result := FrmDesigner;
end;

function TrwComponent.GetObjectClassDescription: String;
var
  Cl: IrwClass;
begin
  Cl := rwRTTIInfo.FindClass(PClassName);
  if Assigned(Cl) then
    Result := Cl.GetClassDescription
  else
    Result := '';  
end;

function TrwComponent.CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
var
  lCompCode: TrwCode;
  fOldLibDesignMode: Boolean;
begin
  if SameText(AFunctionName, 'CompileAsLibComp') then
  begin
    fOldLibDesignMode := LibDesignMode;
    try
      SetLength(lCompCode, 0);
      SetCompiledCode(@lCompCode, cVMLibraryComponentsOffset);
      SetCompilingPointer(0);
      SetDebInf(nil);
      SetDebSymbInf(nil);

      LibDesignMode := True;
      Compile(True);
    finally
      LibDesignMode := fOldLibDesignMode;
      SetLength(lCompCode, 0);      
    end;
  end;
end;

function TrwComponent.PropertiesByTypeName(const ATypeName: String): TrwStrList;
begin
  Result := FindClassPropValue(Self, ATypeName);
end;

procedure TrwComponent.SetFormulas(const Value: TrmFMLFormulas);
begin
  FFormulas.Assign(Value);
end;


function TrwComponent.RootOwner: TrwComponent;
var
  O: TComponent;
begin
  Result := Self;
  O := Self;
  while Assigned(O) and (O is TrwComponent) do
  begin
    Result := TrwComponent(O);
    O := O.Owner;
  end;
end;


function TrwComponent.PathFromRootOwner: String;
var
  RO, O: TComponent;
begin
  Result := '';
  RO := RootOwner;
  O := Self;
  Result := O.Name;
  while O <> RO do
  begin
    O := O.Owner;
    if not (O is TrwReportForm) then
      Result := O.Name + '.' + Result;
  end;
end;

function TrwComponent.GetPathFromRootOwner: String;
begin
  Result := PathFromRootOwner;
end;

function TrwComponent.GetRootOwner: IrmDesignObject;
begin
  Result := RootOwner;
end;

function TrwComponent.FormulasIsNotEmpty: Boolean;
begin
  Result := Formulas.Count > 0;
end;

procedure TrwComponent.AddChildObject(AObject: IrmDesignObject);
begin
  InsertComponent(TrwComponent(AObject.RealObject));
end;

procedure TrwComponent.RemoveChildObject(AObject: IrmDesignObject);
begin
  RemoveComponent(TrwComponent(AObject.RealObject));
end;


function TrwComponent.IsInheritedComponent: Boolean;
begin
  Result := InheritedComponent;
end;

function TrwComponent.GetVarFunctHolder: IrmVarFunctHolder;
begin
  if Assigned(GlobalVarFunc) then
    Result := GlobalVarFunc
  else
    Result := nil;
end;

function TrwComponent.PRootOwner: Variant;
begin
  Result := Integer(Pointer(RootOwner));
end;

procedure TrwComponent.PrepareFormulas;
var
  i: Integer;
begin
  //run time only
  Formulas.PrepareForCalculation;
  for i := 0 to ComponentCount - 1 do
    TrwComponent(Components[i]).PrepareFormulas;
end;

procedure TrwComponent.Refresh;
begin
  if VisualControlCreated then
    FVisualControl.RealObject.Invalidate;
end;

function TrwComponent.GetFormulas: IrmFMLFormulas;
begin
  Result := Formulas;
end;

function TrwComponent.VCLObject: TObject;
begin
  Result := Self;
end;

procedure TrwComponent.EmptyAggregateFunctions;
begin
  Formulas.EmptyAggregateFunctions;
end;

procedure TrwComponent.NotifyAggregateFunctions;
var
  i: Integer;
begin
  for i := 0 to FAggrFunctList.Count - 1 do
    TrmFMLAggrFunctHolder(FAggrFunctList[i]).NotifyOfValueChanges;
end;

procedure TrwComponent.RegisterAggregateFunction(const AggrFunct: TrmFMLAggrFunctHolder);
begin
  if FAggrFunctList.IndexOf(AggrFunct) = -1 then
    FAggrFunctList.Add(AggrFunct);
end;

function TrwComponent.PCalcAggrFunctResult(AFunctionID: Variant): Variant;
begin
  Result := Formulas.CalcAggrFunctResult(AFunctionID);
end;

function TrwComponent.ParticipantOfAggregateFunctions: Boolean;
begin
  Result := FAggrFunctList.Count > 0;
end;

function TrwComponent.GlobalFindComponent(const AComponentName: String): TrwComponent;
var
  i: Integer;
begin
  Result := TrwComponent(FindComponent(AComponentName));
  if not Assigned(Result) then
    for i := 0 to ComponentCount - 1 do
    begin
      Result := TrwComponent(Components[i]).GlobalFindComponent(AComponentName);
      if Assigned(Result) then
        break;
    end;
end;


procedure TrwComponent.SetName(const NewName: TComponentName);
var
  GlobOwn: TrwComponent;
begin
  if not (csLoading in ComponentState) and not Loading and IsDesignMode and not AnsiSameText(NewName, Name) then
  begin
    GlobOwn := GetGlobalOwner;
    if Assigned(GlobOwn) then
      if GlobOwn.GlobalFindComponent(NewName) <> nil then
        raise EComponentError.CreateResFmt(@SDuplicateName, [NewName]);
  end;

  inherited;

  if Designer <> nil then
    Designer.ObjectPropertyChanged(Self, 'Name');
end;

function TrwComponent.GetGlobalOwner: TrwComponent;
var
  O: TrwComponent;
begin
  Result := nil;
  O := TrwComponent(Owner);

  while Assigned(O) do
  begin
    if O.FSubChildrenNamesUnique then
      Result := O;
    O := TrwComponent(O.Owner);
  end;
end;

procedure TrwComponent.RWLoaded;
begin
end;

function TrwComponent.GetPathFromRootParent: String;
var
  O: IrmDesignObject;
begin
  Result := Name;
  O := GetParent;
  while Assigned(O) do
  begin
    Result := O.RealObject.Name + '.' + Result;
    O := O.GetParent;
  end;
end;

function TrwComponent.GetObjectIndex: Integer;
begin
  Result := ComponentIndex;
end;

procedure TrwComponent.SetObjectIndex(const AIndex: Integer);
begin
  ComponentIndex := AIndex;
end;

function TrwComponent.GetInterfacedPropByType(const APropertyType: String): IrmInterfacedObject;
var
  PropList: TrwStrList;
  Obj: TObject;
begin
  Result := nil;

  PropList := PropertiesByTypeName(APropertyType);
  if Length(PropList) > 0 then
  begin
    Obj := Pointer(Integer(GetPropertyValue(PropList[0])));
    if Assigned(Obj) then
    begin
      if Obj is TrwPersistent then
        Result := TrwPersistent(Obj)
      else if Obj is TrwCollection then
        Result := TrwCollection(Obj) as IrmInterfacedObject
      else if Obj is TrwComponent then
        Result := TrwComponent(Obj) as IrmInterfacedObject;
    end;
  end;
end;

function TrwComponent.GetEvents: IrmFunctions;
begin
  Result := FEventsList;
end;

procedure TrwComponent.GetAvailableClassList(const AList: TStringList);
var
  i, j: Integer;
begin
  // Owners' local components
  if Assigned(Owner) then
    TrwComponent(Owner).GetAvailableClassList(AList)

  else
  begin
    // Global system classes
    for i := 0 to SystemLibComponents.Count - 1 do
      AList.AddObject(UpperCase(SystemLibComponents[i].Name), Pointer(SystemLibComponents[i] as IrmComponent));
  end;

  // Local components
  if Assigned(GlobalVarFunc) and Assigned(GlobalVarFunc.LocalComponents) then
    for i := 0 to GlobalVarFunc.LocalComponents.Count - 1 do
    begin
      j :=  AList.IndexOf(GlobalVarFunc.LocalComponents[i].Name);
      if j = -1 then
        AList.AddObject(UpperCase(GlobalVarFunc.LocalComponents[i].Name), Pointer(GlobalVarFunc.LocalComponents[i] as IrmComponent))
      else
        AList.Objects[j] := Pointer(GlobalVarFunc.LocalComponents[i] as IrmComponent);
    end;
end;

function TrwComponent.GetLibComponentByClass(const AClassName: String): IrmComponent;
var
  Cl: IrwClass;
begin
  Result := nil;

  if Assigned(GlobalVarFunc) then
    if GlobalVarFunc.LocalComponents <> nil then
      Result := GlobalVarFunc.LocalComponents.GetComponentByClassName(AClassName);

  if not Assigned(Result) then
    if Assigned(Owner) then
      Result := TrwComponent(Owner).GetLibComponentByClass(AClassName)
    else
    begin
      Cl := rwRTTIInfo.FindClass(AClassName);
      if Assigned(Cl) and Cl.IsUserClass then
        Result := Cl.GetUserClass;
//      Result := SystemLibComponents.GetComponentByClassName(AClassName);
    end;
end;



procedure TrwComponent.SetLibComponentByRef(ALibComponent: IrmComponent);
var
  OldLoading: Boolean;
begin
  if FLoadingInheritances or not Assigned(ALibComponent) then
    Exit;

  OldLoading := FLoading;
  FLoadingInheritances := True;
  try
    LoadFromLibComp(ALibComponent);
    FLibComponent := ALibComponent.GetClassName;
  finally
    FLoadingInheritances := False;
    FLoading := OldLoading;
  end;
end;

function TrwComponent.PExecFunction(AFunctionName, AParams: Variant): Variant;
begin
  if Assigned(GlobalVarFunc) then
    Result := GlobalVarFunc.PExecEvent(AFunctionName, AParams)
  else
    rwError('Function "' + AFunctionName + '" is not found');
end;

function TrwComponent.RunEventByName(const AEventName: String; const AParams: Variant): Variant;
begin

end;

procedure TrwComponent.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;

  if Assigned(Designer) and (AComponent.Owner = Self) then
    Designer.Notify(rmdChildrenListChanged, VarArrayOf([AComponent as IrmDesignObject, Ord(Operation)]));
end;


function TrwComponent.PPathFromRootOwner: Variant;
begin
  Result := GetPathFromRootOwner;
end;

function TrwComponent.PPathFromRootParent: Variant;
begin
  Result := GetPathFromRootParent;
end;


{TrwNoVisualComponent}

procedure TrwNoVisualComponent.SetPoition(Index: Integer; Value: Integer);
begin
  case Index of
    0: FLeft := Value;
    1: FTop := Value;
  end;

  DoSyncVisualControl(rwCPPosition);
end;


function TrwNoVisualComponent.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwNonVisualComponentDesignPanel.Create(Self);
end;

procedure TrwNoVisualComponent.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  inherited;
  if VisualControlCreated then
  begin
    VisualControl.RealObject.BringToFront;
    if LibDesignMode then
      for i := 0 to ComponentCount - 1 do
        if Components[i] <> GlobalVarFunc then
          TrwComponent(Components[i]).DesignParent := Value;
  end;      
end;

procedure TrwNoVisualComponent.SetBoundsRect(ARect: TRect);
begin
  Left := ARect.Left;
  Top := ARect.Top;
end;

function TrwNoVisualComponent.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTNotVisual; 
end;

function TrwNoVisualComponent.GetLeft: Integer;
begin
  Result := Left;
end;

function TrwNoVisualComponent.GetTop: Integer;
begin
  Result := Top;
end;

procedure TrwNoVisualComponent.SetLeft(AValue: Integer);
begin
  Left := AValue;
end;

procedure TrwNoVisualComponent.SetTop(AValue: Integer);
begin
  Top := AValue;
end;

function TrwNoVisualComponent.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := Result.AcceptControls or LibDesignMode;
  Result.AcceptedClases := '';
end;

function TrwNoVisualComponent.GetHeight: Integer;
begin
  if VisualControlCreated then
    Result := VisualControl.RealObject.Height
  else
    Result := 0;
end;

function TrwNoVisualComponent.GetWidth: Integer;
begin
  if VisualControlCreated then
    Result := VisualControl.RealObject.Width
  else
    Result := 0;
end;

procedure TrwNoVisualComponent.SetDesigner(ADesigner: IrmDesigner);
begin
  inherited;
  if LibDesignMode then
    DesignParent := ADesigner.GetDesignParent(Name)
  else
    if Assigned(Owner) then
      DesignParent := TrwComponent(Owner).GetWinControlParent
    else
      DesignParent := nil;
end;

{ TrwGlobalVarFunct }

constructor TrwGlobalVarFunct.Create(AOwner: TComponent);
begin
  FVariables:= TrwGlobalVariables.Create(Self, TrwGlobalVariable);
  FLocalComponents := TrwLocalComponents.Create(Self, TrwLocalComponent);

  inherited;

  if Assigned(AOwner) then
    TrwComponent(AOwner).FGlobalVarFunc := Self;

  FAncestorMode := False;
  Description := 'Misc Structures';
end;

destructor TrwGlobalVarFunct.Destroy;
begin
  if Assigned(Owner) then
    TrwComponent(Owner).FGlobalVarFunc := nil;

  FreeAndNil(FLocalComponents);
  FreeAndNil(FVariables);  

  inherited;
end;

procedure TrwGlobalVarFunct.Assign(Source: TPersistent);
begin
  Variables.Assign(TrwGlobalVarFunct(Source).Variables);
  LocalComponents.Assign(TrwGlobalVarFunct(Source).LocalComponents);  
  EventsList.Assign(TrwGlobalVarFunct(Source).FEventsList);
end;


procedure TrwGlobalVarFunct.ReadEventsData(Reader: TReader);
var
  lEvent, lName, lParams, lPointerType, h: string;
  lType: TrwVarTypeKind;
  i: Integer;
  c: Boolean;
  a: Integer;
begin
  Reader.ReadListBegin;
  while not Reader.EndOfList do
  begin
    lEvent := Reader.ReadString;

    i := Pos(#13, lEvent);
    lName := Copy(lEvent, 1, i - 1);
    Delete(lEvent, 1, i);
    i := Pos(#13, lEvent);
    lParams := Copy(lEvent, 1, i - 1);
    Delete(lEvent, 1, i);
    i := Pos(#13, lEvent);
    h := Copy(lEvent, 1, i - 1);
    Delete(lEvent, 1, i);
    i := Pos(':', h);
    if i > 0 then
    begin
      lPointerType := Copy(h, i + 1, Length(h) - i);
      Delete(h, i, Length(h) - i + 1);
    end
    else
      lPointerType := '';
    lType := TrwVarTypeKind(StrToInt(h));


    i := Pos(#01, lName);
    if i > 0 then
    begin
      Delete(lName, i, 1);
      c := True;
    end
    else
      c := False;

    i := Pos(#02, lName);
    if i > 0 then
    begin
      a := StrToInt(Copy(lName, i + 1, Length(lName) - i));
      Delete(lName, i, Length(lName) - i + 1);
    end
    else
      a := -1;


    i := EventsList.IndexOf(lName);
    if (i = -1) then
      if c then
        Continue
      else
      begin
        i := EventsList.Add(lName, lParams, lType, lPointerType);
        RegisterEvents([MakeEventHandlerHeader(FEventsList[i])]);
        i := EventsList.IndexOf(lName);
      end

    else if FEventsList[i].InheritedItem then
      c := True;

    FEventsList[i].InheritedItem := c;

    if not IsEmptyInheritedEvent(Trim(lEvent)) then
    begin
      FEventsList[i].HandlersText := lEvent;
      FEventsList[i].FCodeAddress := a;
    end;
  end;

  Reader.ReadListEnd;
end;


procedure TrwGlobalVarFunct.WriteEventsData(Writer: TWriter);
var
  i: Integer;
  h: string;
  h1: string;
  c: String[1];
  Addr: String;
begin
  Writer.WriteListBegin;

  for i := 0 to EventsList.Count - 1 do
  begin
    if FEventsList[i].InheritedItem then
      c := #01
    else
      c := '';

    if FEventsList[i].FCodeAddress <> -1 then
      Addr := #02+IntToStr(FEventsList[i].FCodeAddress)
    else
      Addr := '';

    h := TrimRight(FEventsList[i].HandlersText);
    if (h <> '') and not (FEventsList[i].InheritedItem and IsEmptyInheritedEvent(Trim(h))) then
    begin
      h1 := FEventsList[i].Name+ c + Addr + #13 +FEventsList[i].ParamStr+#13+
           IntToStr(Ord(FEventsList[i].Params.Result.VarType));
      if (FEventsList[i].Params.Result.VarType = rwvPointer) and
         (Length(FEventsList[i].Params.Result.PointerType) > 0) then
        h1 := h1 + ':' + FEventsList[i].Params.Result.PointerType;
      Writer.WriteString(h1 + #13 + h);
    end;
  end;
  Writer.WriteListEnd;
end;

procedure TrwGlobalVarFunct.DefineProperties(Filer: TFiler);
var
  Ancestor: TrwGlobalVarFunct;
begin
  inherited;

  if Filer is TWriter then
    Ancestor := TrwGlobalVarFunct(TWriter(Filer).Ancestor)
  else
    Ancestor := nil;


  FVariablesRW := TrwGlobalVariables.Create(Self, TrwGlobalVariable);
  try
    if Filer is TWriter then
    begin
      FVariablesRW.Assign(FVariables);
      if Assigned(Ancestor) then
        FVariablesRW.MakeDelta(Ancestor.Variables);
    end;

    Filer.DefineProperty('Variables', ReadVariables, WriteVariables, FVariablesRW.Count > 0);
  finally
    FreeAndNil(FVariablesRW);
  end;


  FLocalComponentsRW := TrwLocalComponents.Create(Self, TrwLocalComponent);
  try
    if Filer is TWriter then
    begin
      FLocalComponentsRW.Assign(FLocalComponents);
      if Assigned(Ancestor) then
        FLocalComponentsRW.MakeDelta(Ancestor.LocalComponents);
    end;

    Filer.DefineProperty('LocalComponents', ReadLocalComponents, WriteLocalComponents, FLocalComponentsRW.Count > 0);
  finally
    FreeAndNil(FLocalComponentsRW);
  end;
end;

procedure TrwGlobalVarFunct.ReadLocalComponents(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FLocalComponentsRW);

  FLocalComponents.ApplyDelta(FLocalComponentsRW);

  if SystemLibComponents.CompilingInProgress then
    FLocalComponents.Compile;
end;

procedure TrwGlobalVarFunct.WriteLocalComponents(Writer: TWriter);
begin
  Writer.WriteCollection(FLocalComponentsRW);
end;

function TrwGlobalVarFunct.GetLocalComponents: IrmComponents;
begin
  Result := LocalComponents;
end;

procedure TrwGlobalVarFunct.ReadVariables(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FVariablesRW);

  FVariables.ApplyDelta(FVariablesRW);
end;

procedure TrwGlobalVarFunct.WriteVariables(Writer: TWriter);
begin
  Writer.WriteCollection(FVariablesRW);
end;

function TrwGlobalVarFunct.GetVariables: IrmVariables;
begin
  Result := Variables;
end;

function TrwGlobalVarFunct.GetLocalFunctions: IrmFunctions;
begin
  Result := GetEvents;
end;


{ TrwGlobalVariable }

constructor TrwGlobalVariable.Create(Collection: TCollection);
begin
  inherited;
  FVarRec.Reference := False;
  FVarRec.PointerType := '';
  FVisibility := rvvPublic;
  FDisplayName := '';
  FShowInQB := False;
  FShowInFmlEditor := False;
end;


procedure TrwGlobalVariable.Assign(Source: TPersistent);
begin
  inherited;
  FVarRec.Name := TrwGlobalVariable(Source).Name;
  VarType := TrwGlobalVariable(Source).VarType;
  PointerType := TrwGlobalVariable(Source).PointerType;
  Value := TrwGlobalVariable(Source).Value;
  Visibility := TrwGlobalVariable(Source).Visibility;
  DisplayName := TrwGlobalVariable(Source).DisplayName;
  ShowInQB := TrwGlobalVariable(Source).ShowInQB;
  ShowInFmlEditor := TrwGlobalVariable(Source).ShowInFmlEditor;
end;


procedure TrwGlobalVariable.SetVarType(const Value: TrwVarTypeKind);
begin
  FVarRec.VarType := Value;
  if FVarRec.VarType = rwvArray then
    FValue := VarArrayCreate([0,0], varVariant)
  else
    VarClear(FValue);
  UpdateValue;
end;


function TrwGlobalVariable.GetValue: Variant;
var
  S: string;
begin
  if FVarRec.VarType = rwvArray then
    if (TrwGlobalVariables(Collection).Owner = nil) or
       (csWriting in TComponent(TrwGlobalVariables(Collection).Owner).ComponentState) or
        TrwGlobalVarFunct(TrwGlobalVariables(Collection).Owner).FAncestorMode then
    begin
      S := AsString;
      Result := StringOfChar(' ', Length(S));
      VarCast(Result, Result, varString);
      CopyMemory(TVarData(Result).VString, @S[1], Length(S));
    end
    else
      Result := FValue
  else
    Result := FValue;
end;

procedure TrwGlobalVariable.SetValue(const AValue: Variant);
var
  MS: IEvDualStream;
  S: String;
begin
  if FVarRec.VarType = rwvArray then
  begin
    if (Variants.VarType(AValue) = varString) or (Variants.VarType(AValue) = varOleStr) then
    begin
      S := AValue;
      if Length(S) = 0 then
      begin
        VarArrayRedim(FValue, -1);
        Exit;
      end;

      MS := TEvDualStreamHolder.Create(Length(S));
      try
        MS.WriteBuffer(S[1], Length(S));
        S := '';
        MS.Position := 0;
        FValue := StreamToVarArray(MS.RealStream);
      finally
        MS := nil;
      end
    end
    else
      FValue := AValue;
  end

  else
    FValue := InternalTypeToVariant(FVarRec.VarType, AValue);
end;


function TrwGlobalVariable.AsString: String;
var
  MS: TMemoryStream;
begin
  if FVarRec.VarType = rwvArray then
  begin
    MS := TMemoryStream(VarArrayToStream(FValue, nil, True));
    try
      SetLength(Result, MS.Size);
      CopyMemory(@(Result[1]), MS.Memory, MS.Size);
    finally
      MS.Free;
    end;
  end
  else
    Result := FValue;
end;


procedure TrwGlobalVariable.UpdateValue;
var
  vt: Integer;
begin
  case FVarRec.VarType of
    rwvInteger,
    rwvPointer:   vt := varInteger;

    rwvFloat:    vt := varDouble;

    rwvCurrency: vt := varCurrency;

    rwvDate:     vt := varDate;

    rwvString:   vt := varString;

    rwvBoolean:  vt := varBoolean;
  else
    vt := varUnknown;
  end;

  if vt <> varUnknown then
    VarCast(FValue, FValue, vt);
end;

function TrwGlobalVariable.PVarRec: PTrwVariable;
begin
  Result := Addr(FVarRec);
end;

procedure TrwGlobalVariable.SetName(const Value: string);
begin
  FVarRec.Name := Value;
end;

function TrwGlobalVariable.GetName: string;
begin
  Result := FVarRec.Name;
end;

function TrwGlobalVariable.GetVarType: TrwVarTypeKind;
begin
  Result := FVarRec.VarType;
end;


function TrwGlobalVariable.VarAddr: Pointer;
begin
  Result := Addr(FValue);
end;


function TrwGlobalVariable.GetPointerType: String;
begin
  Result := FVarRec.PointerType;
end;


procedure TrwGlobalVariable.SetPointerType(const AValue: String);
begin
  FVarRec.PointerType := AValue;
end;


procedure TrwGlobalVariable.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('VarValue', ReadValue, WriteValue, True);
end;


procedure TrwGlobalVariable.ReadValue(Reader: TReader);
var
  MS: TisMemoryStream;
  l: Integer;
begin
  if FVarRec.VarType = rwvArray then
  begin
    l := 0;
    if Reader.ReadValue = vaBinary then
      Reader.Read(l, SizeOf(Integer))
    else
      Assert(False, 'Error of reading array data');

    MS := TisMemoryStream.Create;
    try
      MS.SetSize(l);
      Reader.Read(MS.Memory^, l);
      FValue := StreamToVarArray(MS);
    finally
      MS.Free;
    end;
  end
  else
    Value := Reader.ReadVariant;
end;

procedure TrwGlobalVariable.WriteValue(Writer: TWriter);
var
  MS: TMemoryStream;
  l: Integer;
  t: TValueType;
begin
  if FVarRec.VarType = rwvArray then
  begin
    MS := TMemoryStream(VarArrayToStream(FValue, nil, True));
    try
      l := MS.Size;
      t := vaBinary;
      Writer.Write(t, SizeOf(t));
      Writer.Write(l, SizeOf(Integer));
      Writer.Write(MS.Memory^, l);
    finally
      MS.Free;
    end;
  end
  else
    Writer.WriteVariant(FValue);
end;


function TrwGlobalVariable.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

procedure TrwGlobalVariable.SetDisplayName(const Value: string);
begin
  FDisplayName := Value;
  inherited;
end;

procedure TrwGlobalVariable.ApplyDelta(const DeltaCollectionItem: TrwCollectionItem);
var
  V: TrwGlobalVariable;
begin
  V := TrwGlobalVariable(DeltaCollectionItem);
  if V.Visibility <> rvvPrivate then
    Value := V.Value;
end;

function TrwGlobalVariable.AsAncestor(const AncestorItem: TrwCollectionItem): Boolean;
var
  V: TrwGlobalVariable;
begin
  V := TrwGlobalVariable(AncestorItem);
  Result := (V.Visibility = rvvPrivate) or
            not((Value = Null) and (V.Value <> Null) or (Value <> Null) and (V.Value = Null) or (AsString <> V.AsString));
end;

function TrwGlobalVariable.GetVarInfo: TrmVFHVarInfoRec;
begin
  Result.Name := Name;
  Result.VarType := VarType;
  Result.PointerType := PointerType;
  Result.DisplayName := DisplayName;

  if not ((Visibility = rvvPrivate) and IsInheritedItem) and
    (VarType in [rwvInteger, rwvBoolean, rwvString, rwvFloat, rwvDate, rwvArray, rwvCurrency]) then
  begin
    Result.ShowInQB := ShowInQB;
    Result.ShowInFmlEditor := ShowInFmlEditor;
  end
  else
  begin
    Result.ShowInQB := False;
    Result.ShowInFmlEditor := False;
  end;

  Result.Value := Value;
  Result.InheritedVar := IsInheritedItem;
end;


function TrwGlobalVariable.GetShowInFmlEditor: Boolean;
begin
  Result := FShowInFmlEditor;
end;

function TrwGlobalVariable.GetShowInQB: Boolean;
begin
  Result := FShowInQB;
end;

function TrwGlobalVariable.GetVisibility: TrwVarVisibility;
begin
  Result := FVisibility;
end;

function TrwGlobalVariable.IsNotInherited: Boolean;
begin
  Result := not IsInheritedItem;
end;

procedure TrwGlobalVariable.SetShowInFmlEditor(const AValue: Boolean);
begin
  FShowInFmlEditor := AValue;
end;

procedure TrwGlobalVariable.SetShowInQB(const AValue: Boolean);
begin
  FShowInQB := AValue;
end;

procedure TrwGlobalVariable.SetVisibility(const AValue: TrwVarVisibility);
begin
  FVisibility := AValue;
end;

{ TrwGlobalVariables }

function TrwGlobalVariables.Add: TrwGlobalVariable;
begin
  Result := TrwGlobalVariable(inherited Add);
end;

function TrwGlobalVariables.GetItem(Index: Integer): TrwGlobalVariable;
begin
  Result := TrwGlobalVariable(inherited GetItem(Index));
end;

function TrwGlobalVariables.GetVariableByName(const AVarName: String): IrmVariable;
var
  i: Integer;
begin
  i := IndexOf(AVarName);
  if i = -1 then
    Result := nil
  else
    Result := Items[i];
end;

function TrwGlobalVariables.IndexOf(AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AName := AnsiUpperCase(AName);

  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].Name) = AName then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwGlobalVariables.SetItem(Index: Integer;
  Value: TrwGlobalVariable);
begin
  inherited SetItem(Index, Value);
end;



{ TrwDefPropComponents }

function TrwDefPropComponents.Add(AClassName: String; APropName: String): TrwDefPropComponent;
begin
  Result := TrwDefPropComponent(inherited Add);
  Result. CompClassName := AClassName;
  Result. PropName := APropName;
end;

function TrwDefPropComponents.GetItem(Index: Integer): TrwDefPropComponent;
begin
  Result := TrwDefPropComponent(inherited GetItem(Index));
end;

function TrwDefPropComponents.IndexOf(AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AName := AnsiUpperCase(AName);
  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].CompClassName) = AName then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwDefPropComponents.Initialize;
begin
  Add('TrwQuery', 'QueryBuilderInfo');
  Add('TrwBuffer', 'Fields');
  Add('TrwLabel', 'BoundLines');
  Add('TrwDBText', 'BoundLines');
  Add('TrwSysLabel', 'BoundLines');
  Add('TrwFrame', 'BoundLines');
  Add('TrwBand', 'BoundLines');
  Add('TrwContainer', 'BoundLines');
  Add('TrwImage', 'BoundLines');
  Add('TrwDBImage', 'BoundLines');
end;

procedure TrwDefPropComponents.SetItem(Index: Integer;
  Value: TrwDefPropComponent);
begin
  inherited SetItem(Index, Value);
end;


{ TrwDesignInfo }

constructor TrwDesignInfo.Create;
begin
  inherited;
  FEventsPositions := TStringList.Create;
end;

destructor TrwDesignInfo.Destroy;
begin
  FEventsPositions.Free;
  inherited;
end;



{TrwpFuncParamList}

procedure TrwFuncParams.Assign(ASource: TPersistent);
begin
  inherited;
  FTag := TrwFuncParams(ASource).Tag;
end;

function TrwFuncParams.AddParam(const AVar: Boolean; const AName: string): TrwFuncParam;
begin
  Result := TrwFuncParam(Add);
  Result.Descriptor.Reference := AVar;
  Result.Descriptor.Name := AName;
end;

function TrwFuncParams.GetItem(AIndex: Integer): TrwFuncParam;
begin
  Result := TrwFuncParam(inherited Items[AIndex]);
end;

function TrwFuncParams.GetResult: PTrwVariable;
begin
  Result := @FResult;
end;

function TrwFuncParams.ParamByName(const AParamName: String): TrwFuncParam;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(Items[i].Descriptor.Name, AParamName) then
    begin
      Result := Items[i];
      break;
    end;
end;


{ TrwEvent }

constructor TrwEvent.Create(AOwner: TrwEventsList);
begin
  FOwner := AOwner;
  Params := TrwFuncParams.Create(TrwFuncParam);
  UserEvent := False;
  Name := '';
  ParamStr := '';
  HandlersText := '';
  FCodeAddress := 0;
end;


destructor TrwEvent.Destroy;
begin
  Params.Free;
  inherited;
end;




function TrwEvent.GetCollection: IrwCollection;
begin
  Result := FOwner;
end;

function TrwEvent.GetDescription: String;
begin
  Result := '';
end;

function TrwEvent.GetDisplayName: String;
begin
  Result := '';
end;

function TrwEvent.GetEventFullText: String;
begin
  Result := MakeEventHandlerHeader(Self) + #13#10 + HandlersText + #13#10 + 'end';
end;

function TrwEvent.GetFunctionText: String;
begin
  Result := EventFullText;
end;

function TrwEvent.GetGroup: String;
begin
  Result := '';
end;

function TrwEvent.GetInheritanceInfo: TrmInheritedFunctInfoList;
var
  ArrLength: Integer;
  Evnt: String;

  procedure AddInheritedLevels(const ALibComp: TrwLibComponent);
  var
    i: Integer;
    Cl: IrwClass;
  begin
    i := ALibComp.VMT.IndexOf(Evnt);
    if i = -1 then Exit;

    Inc(ArrLength);
    Result[ArrLength - 1].ClassName := ALibComp.Name;
    Result[ArrLength - 1].FunctText := MakeEventHandlerHeader(Self) + #13#10 + ALibComp.VMT[i].Text + #13#10'end';
    Result[ArrLength - 1].ThroughInheritance := IsEmptyInheritedEvent(ALibComp.VMT[i].Text);

    Cl := rwRTTIInfo.FindClass(ALibComp.ParentName);
    if Assigned(Cl) and Cl.IsUserClass then
      AddInheritedLevels(TrwLibComponent(Cl.GetUserClass.RealObject));
  end;

  procedure AddOwnerLevel(const AObject: TrwComponent);
  var
    CurrentOwner: TrwComponent;
    RootPath: String;
    LibComp: TrwLibComponent;
    Cl: IrwClass;    
  begin
    CurrentOwner := AObject.RootOwner;
    RootPath := AObject.PathFromRootOwner;
    GetNextStrValue(RootPath, '.');

    while RootPath <> '' do
    begin
      if CurrentOwner.IsLibComponent then
      begin
        Cl := rwRTTIInfo.FindClass(CurrentOwner._LibComponent);
        if Assigned(Cl) and Cl.IsUserClass then
        begin
          LibComp := TrwLibComponent(Cl.GetUserClass.RealObject);
          Evnt := RootPath + '.' + Name;
          AddInheritedLevels(LibComp);
        end;
      end;
      CurrentOwner := CurrentOwner.FindComponent(GetNextStrValue(RootPath, '.')) as TrwComponent;
    end;

    if AObject.IsLibComponent then
    begin
      Cl := rwRTTIInfo.FindClass(AObject._LibComponent);
      if Assigned(Cl) then
      begin
        LibComp := TrwLibComponent(Cl.GetUserClass.RealObject);
        Evnt := Name;
        AddInheritedLevels(LibComp);
      end;
    end;
  end;

begin
  SetLength(Result, 100);  // Resonable limit
  ArrLength := 0;

  if InheritedItem then
    AddOwnerLevel(FOwner.FOwner);

  SetLength(Result, ArrLength);
end;

function TrwEvent.GetItemID: String;
begin
  Result := '';
end;

function TrwEvent.GetItemIndex: Integer;
begin
  Result := TList(FOwner).IndexOf(Self);
end;

function TrwEvent.GetName: String;
begin
  Result := Name;
end;

function TrwEvent.IsInheritedItem: Boolean;
begin
  Result := InheritedItem;
end;

function TrwEvent.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TrwEvent.RealObject: TCollectionItem;
begin
  Result := nil;
end;

procedure TrwEvent.SetDescription(const ADescription: String);
begin
// do nothing
end;

procedure TrwEvent.SetDisplayName(const ADisplayName: String);
begin
// do nothing
end;

procedure TrwEvent.SetEventFullText(const Value: String);
begin

end;


procedure TrwEvent.SetFunctionText(const AFunctText: String);
var
  i, lBegPos, lEndPos, lEnd: Integer;
  lName: String;
  h: String;

  function  IsRWProc(const AName: String): Boolean;
  begin
    Result :=
      (AnsiSameText('Set', Copy(AName, 1, 3)) or AnsiSameText('Get', Copy(AName, 1, 3))) and
      (FOwner.FOwner._UserProperties.IndexOf(Copy(AName, 4, Length(AName) - 3)) <> -1);
  end;

begin
  try
    lName := ParseFunctionHeader(AFunctText, Params, lBegPos, lEndPos, lEnd);

    while (lEnd <= Length(AFunctText)) and (AFunctText[lEnd] = ' ') do
      Inc(lEnd);

    if AFunctText[lEnd] = #13 then
      Inc(lEnd, 2);

    i := Length(AFunctText);
    while (AFunctText[i] in [#10, #13, ' ']) and (i > lEnd) do
      Dec(i);

    if (i < lEnd) or not SameText(Copy(AFunctText, i - 2, 3), 'END') then
      raise ErwException.Create(ResErrSyntaxError);

    Dec(i, 3);
    h := Copy(AFunctText, lEnd, i-lEnd-1);
    i := Length(h);
    while (i > 0) and (h[i] in [#10, #13]) do
      Dec(i);
    Delete(h, i, Length(h) - i);
  except
    HandlersText := AFunctText;
    raise;
  end;

  if not InheritedItem and not IsRWProc(Name) then
  begin
    Name := lName;
    ParamStr := Trim(Copy(AFunctText, lBegPos, lEndPos-lBegPos));
  end;

  HandlersText := h;
//  if HandlersText = '' then
//    HandlersText := ';';
end;

function TrwEvent._AddRef: Integer;
begin
  Result := -1;
end;

function TrwEvent._Release: Integer;
begin
  Result := -1;
end;

procedure TrwEvent.SetGroup(const AGroupName: String);
begin
// do nothing
end;

procedure TrwEvent.SetItemIndex(const AIndex: Integer);
begin
  TList(FOwner).Move(GetItemIndex, AIndex);
end;

function TrwEvent.VCLObject: TObject;
begin
  Result := Self;
end;


{ TrwLibCollection }

procedure TrwLibCollection.LoadFromStream(AStream: TStream);
var
  Reader: TReader;
begin
  Reader := TReader.Create(AStream, 1024);
  try
    Reader.NextValue;
    Reader.ReadValue;
    Reader.ReadCollection(Self);
    ReadCompiledCode(Reader);
    try
      FLibVersion := Reader.ReadDate;
    except
    end;

    
  finally
    Reader.Free;
  end;
end;

procedure TrwLibCollection.SaveToStream(AStream: TStream);
var
  Writer: TWriter;
begin
  Writer := TWriter.Create(AStream, 1024);
  try
    Writer.WriteCollection(Self);
    WriteCompiledCode(Writer);
    Writer.WriteDate(FLibVersion);
  finally
    Writer.Free;
  end;
end;


procedure TrwLibCollection.Compile;
var
  i: Integer;
  lCompiledCode: TrwCode;
  lDebInf: TrwDebInf;
  lDebSymbInf: TrwDebInf;
begin
  RWEngineExt.AppAdapter.StartEngineProgressBar('Compiling RW Library...',  Count - 1);
  lCompiledCode := Copy(FCompiledCode);
  lDebInf := Copy(FDebInf);
  lDebSymbInf := Copy(FDebSymbInf);
  FCompilingInProgress := True;
  try
    try
      SetLength(FCompiledCode, 0);
      SetCompiledCode(@FCompiledCode, EntryPointOffset);
      SetCompilingPointer(0);

      SetLength(FDebInf, 0);
      SetDebInf(@FDebInf);
      SetDebInfPointer(0);
      SetLength(FDebSymbInf, 0);
      SetDebSymbInf(@FDebSymbInf);
      SetDebSymbInfPointer(0);

      for i := 0 to Count -1 do
        TrwLibCollectionItem(Items[i]).FCompiled := False;
      FCompilingProgress := 0;

      for i := 0 to Count -1 do
        try
          if not TrwLibCollectionItem(Items[i]).FCompiled then
            TrwLibCollectionItem(Items[i]).Compile;
        except
          on E: Exception do
          begin
            E.Message := 'Item "' + TrwLibCollectionItem(Items[i]).Name + '"' + E.Message;
            raise;
          end;
        end;


      SetLength(FCompiledCode, GetCompilingPointer);
      SetLength(FDebInf, GetDebInfPointer);
      SetLength(FDebSymbInf, GetDebSymbInfPointer);

      FLibVersion := RWEngineExt.AppAdapter.CurrentDateTime;

    except
      SetLength(FCompiledCode, 0);
      SetLength(FDebInf, 0);
      SetLength(FDebSymbInf, 0);

      FCompiledCode := Copy(lCompiledCode);
      FDebInf := Copy(lDebInf);
      FDebSymbInf := Copy(lDebSymbInf);

      raise;
    end;

  finally
    FCompilingInProgress := False;
    SetLength(lCompiledCode, 0);
    SetLength(lDebInf, 0);
    SetLength(lDebSymbInf, 0);
    RWEngineExt.AppAdapter.EndEngineProgressBar;
  end;
end;


procedure TrwLibCollection.ClearDebInf;
begin
  SetLength(FDebInf, 0);
  SetLength(FDebSymbInf, 0);
end;


{ TrwpFunction }

constructor TrwFunction.Create(Collection: TCollection);
begin
  inherited;
  FParams := TrwFuncParams.Create(TrwFuncParam);
end;

destructor TrwFunction.Destroy;
begin
  FParams.Free;
  inherited;
end;

procedure TrwFunction.Assign(Source: TPersistent);
begin
  inherited;
  Params.Assign(TrwFunction(Source).Params);
  DesignTimeInfo := TrwFunction(Source).DesignTimeInfo;
end;

procedure TrwFunction.DefineProperties(Filer: TFiler);
var
  SavedParams: TrwFuncParams;

  procedure PrepareParamsForWriting;
  var
    i: Integer;
  begin
    for i := Params.Count - 1 downto 0 do
      if (Length(Params[i].Domain) = 0) and (VarToStr(Params[i].DefaultValue) = '') then
        Params.Delete(i);
  end;

begin
  inherited;

  if Filer is TWriter then
  begin
    SavedParams := TrwFuncParams.Create(TrwFuncParam);
    SavedParams.Assign(Params);
    PrepareParamsForWriting;
  end;

  try
    Filer.DefineProperty('Params', ReadParams, WriteParams, Params.Count > 0);

  finally
    if Filer is TWriter then
    begin
      Params.Assign(SavedParams);
      FreeAndNil(SavedParams);
    end;
  end;
end;

procedure TrwFunction.ReadParams(Reader: TReader);
begin
  Reader.ReadValue;
  Reader.ReadCollection(FParams);
end;

procedure TrwFunction.WriteParams(Writer: TWriter);
begin
  Writer.WriteCollection(FParams);
end;


{ TrwpFunctions }

function TrwFunctions.FindFunction(const AName: string): TrwFunction;
var
  i: Integer;
begin
  i := IndexOf(AName);
  if i <> -1 then
    Result := TrwFunction(Items[i])
  else
    Result := nil;
end;


function TrwFunctions.GetItem(index: Integer): TrwFunction;
begin
  Result := TrwFunction(inherited Items[index]);
end;

procedure TrwLibCollection.ReadCompiledCode(const AReader: TReader);
var
  MS: TMemoryStream;
  A: Variant;
  i: Integer;
begin
  MS := TisMemoryStream.Create;
  try
    MS.Size := AReader.ReadInteger;
    AReader.Read(MS.Memory^, MS.Size);
    MS.Position := 0;
    A := StreamToVarArray(MS);
  finally
    MS.Free;
  end;

  SetLength(FCompiledCode, VarArrayHighBound(A, 1) - VarArrayLowBound(A, 1) + 1);
  for i := Low(CompiledCode) to High(CompiledCode) do
  begin
    CompiledCode[i] := A[i];
    if VarType(CompiledCode[i]) = varOleStr then
      CompiledCode[i] := VarAsType(CompiledCode[i], varString);
  end;

  if AReader.NextValue = vaIdent then
  begin
    Assert(AReader.ReadIdent = 'DebugInfo');
    ReadDebugInfo(AReader);
  end;
end;


procedure TrwLibCollection.WriteCompiledCode(const AWriter: TWriter);
var
  A: Variant;
  i: Integer;
  MS: TStream;
begin
  A := VarArrayCreate([Low(CompiledCode), High(CompiledCode)], varVariant);
  for i := Low(CompiledCode) to High(CompiledCode) do
    A[i] := CompiledCode[i];

  MS := VarArrayToStream(A, nil, True);
  try
    MS.Position := 0;
    AWriter.WriteInteger(MS.Size);
    AWriter.Write(TMemoryStream(MS).Memory^, MS.Size);
  finally
    MS.Free;
  end;

  if FStoreDebInfo then
  begin
    AWriter.WriteIdent('DebugInfo');
    WriteDebugInfo(AWriter);
  end;
end;


function TrwLibCollection.PCount: Variant;
begin
  Result := Count;
end;



{ TrwpLibFunctions }

function TrwLibFunctions.AddFunc(const AText: String): TrwLibFunction;
var
  Pb, Pe, P: Integer;
begin
  Result := TrwLibFunction(Add);
  Result.Name := ParseFunctionHeader(AText, Result.Params, Pb, Pe, P);
  Result.Text := AText;
end;


procedure TrwLibFunctions.InitializeHeaders;
var
  i: Integer;
  Pb, Pe, P: Integer;
begin
  for i := 0 to Count - 1 do
    Items[i].FName := ParseFunctionHeader(TrwLibFunction(Items[i]).Text, Items[i].Params, Pb, Pe, P);
end;

procedure TrwLibFunctions.Initialize;
var
  MS: IEvDualStream;
begin
  Clear;
  MS := TEvDualStreamHolder.Create;
  RWEngineExt.AppAdapter.LoadLibraryFunctions(MS.RealStream);
  if MS.Size > 0 then
  begin
    MS.Position := 0;
    LoadFromStream(MS.RealStream);
  end;
end;

procedure TrwLibFunctions.Store;
var
  MS: IEvDualStream;
begin
  StoreDebInfo := False;
  MS := TEvDualStreamHolder.Create;
  SaveToStream(MS.RealStream);
  RWEngineExt.AppAdapter.UnloadLibraryFunctions(MS.RealStream);
  inherited;
end;

procedure TrwLibFunctions.Execute(const APar1: string; const APar2: string = '');
var
  F: TrwLibFunction;
  EP: TrwVMAddress;
begin
  F := TrwLibFunction(FindFunction(APar1));
  if not Assigned(F) then
    raise ErwException.CreateFmt(ResErrNDefProc, [APar1]);

  EP := GetEntryPoint(F.CodeAddress);
  VM.Run(EP);
end;


function TrwLibFunctions.PIndexOf(AName: Variant): Variant;
begin
  Result := IndexOf(AName);
end;


function TrwLibFunctions.PItems(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Items[AIndex]));
end;

function TrwLibFunctions.GetFunctionByName(const AFunctName: String): IrmFunction;
begin
  Result := TrwLibFunction(FindFunction(AFunctName));
end;


procedure TrwLibCollection.Store;
begin
  FModified := False;
end;

function TrwLibCollection.AddLibItem(const AName, AGroup, ADescription: String): TrwLibCollectionItem;
begin
  Result := TrwLibCollectionItem(Add);
  Result.Name := AName;
  Result.Group := AGroup;
  Result.Description := ADescription;
end;

function TrwLibCollection.IndexOf(const AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Count - 1 do
    if SameText(TrwLibCollectionItem(Items[i]).Name, AName) then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwLibCollection.Execute(const APar1, APar2: string);
begin
end;

function TrwLibCollection.GetDebInf: PTrwDebInf;
begin
  Result := Addr(FDebInf);
end;

function TrwLibCollection.GetDebSymbInf: PTrwDebInf;
begin
  Result := Addr(FDebSymbInf);
end;


function TrwLibCollection.GetCompiledCode: PTrwCode;
begin
  Result := Addr(FCompiledCode);
end;

function TrwLibCollection.EntryPointOffset: Integer;
begin
  Result := 0;
end;


procedure TrwLibCollection.CreateDebInf;
var
  MapFile: String;
  F: IEvDualStream;
  Ver: TDateTime;
  Reader: TReader;
begin
  if Length(FDebInf) > 0 then
    Exit;

  // trying to get cached data

  MapFile := GetSysTempDir + 'DebugInfo ' + ClassName + '.tmp';

  if FileExists(MapFile) then
  begin
    F := TEvDualStreamHolder.CreateFromFile(MapFile);

    Reader := TReader.Create(F.RealStream, 1024);
    try
      Ver := Reader.ReadDate;
      if Ver = FLibVersion then
      begin
        ReadDebugInfo(Reader);
        Exit;
      end;
    finally
      Reader.Free;
      F := nil;      
    end;
  end;

  Ver := FLibVersion;
  Compile;
  FLibVersion := Ver;

  CacheDebugInfo;
end;

procedure TrwLibCollection.WriteDebugInfo(const AWriter: TWriter);
var
  i: Integer;
begin
  AWriter.WriteInteger(Length(FDebInf));
  for i := Low(FDebInf) to High(FDebInf) do
    AWriter.WriteString(FDebInf[i]);

  AWriter.WriteInteger(Length(FDebSymbInf));
  for i := Low(FDebSymbInf) to High(FDebSymbInf) do
    AWriter.WriteString(FDebSymbInf[i]);
end;

procedure TrwLibCollection.ReadDebugInfo(const AReader: TReader);
var
  i: Integer;
begin
  SetLength(FDebInf, AReader.ReadInteger);
  for i := Low(FDebInf) to High(FDebInf) do
    FDebInf[i] := AReader.ReadString;

  SetLength(FDebSymbInf, AReader.ReadInteger);
  for i := Low(FDebSymbInf) to High(FDebSymbInf) do
    FDebSymbInf[i] := AReader.ReadString;
end;

procedure TrwLibCollection.CacheDebugInfo;
var
  MapFile: String;
  F: IEvDualStream;
  Writer: TWriter;
begin
  MapFile := GetSysTempDir + 'DebugInfo ' + ClassName + '.tmp';
  F := TEvDualStreamHolder.CreateFromNewFile(MapFile);

  Writer := TWriter.Create(F.RealStream, 1024);
  try
    Writer.WriteDate(FLibVersion);
    WriteDebugInfo(Writer);
  finally
    Writer.Free;
  end;
end;


{ TrwLibFunction }

constructor TrwLibFunction.Create(Collection: TCollection);
begin
  inherited;
  FCodeAddress := -1;
end;


procedure TrwLibFunction.Compile;
begin
  RWEngineExt.AppAdapter.UpdateEngineProgressBar('Compiling function:  ' + Name);
  FCodeAddress := GetCompilingPointer + cVMLibraryFunctionsOffset;
  CompileFunction(Text);
  inherited;
end;

function TrwLibFunction.GetFunctionText: String;
begin
  Result := FText;
end;

procedure TrwLibFunction.SetFunctionText(const AFunctText: String);
var
  Pb, Pe, P: Integer;
begin
  FText := AFunctText;
  FName := ParseFunctionHeader(FText, Params, Pb, Pe, P);
end;

procedure TrwLibFunction.SetName(const AValue: String);
begin
// do nothing
end;


function TrwLibFunction.GetInheritanceInfo: TrmInheritedFunctInfoList;
begin
  SetLength(Result, 0);
end;


{ TrwPublishedMethods }

procedure TrwPublishedMethods.Initialize;
begin
  AddMethod('function PropertyExists (PropertyName: String): Boolean', 'TrwComponent');
  AddMethod('procedure SetPropertyValue(PropertyName: String; Value: Variant)', 'TrwComponent');
  AddMethod('function GetPropertyValue(PropertyName: String): Variant', 'TrwComponent');
  AddMethod('function Owner: TrwComponent', 'TrwComponent');
  AddMethod('function ComponentCount: Integer', 'TrwComponent');
  AddMethod('function FindComponent(Name: String): TrwComponent', 'TrwComponent');
  AddMethod('function ComponentByIndex(Index: Integer): TrwComponent', 'TrwComponent');
  AddMethod('function ComponentIndexByName(Name: String): Integer', 'TrwComponent');
  AddMethod('function ClassName: String', 'TrwComponent');
  AddMethod('function InheritsFrom(AClassName: String): Boolean', 'TrwComponent');
  AddMethod('function ExecEvent(EventName: String; AParams: Array): Variant', 'TrwComponent');
  AddMethod('function CalcAggrFunctResult(AFunctionID: String): Variant', 'TrwComponent');
  AddMethod('function ExecFunction(AFunctionName: String; AParams: Array): Variant', 'TrwComponent');
  AddMethod('function PathFromRootOwner: String', 'TrwComponent');
  AddMethod('function PathFromRootParent: String', 'TrwComponent');

  AddMethod('procedure FinishPrinting', 'TrwPrintableContainer');
  AddMethod('function ChildCount: Integer', 'TrwPrintableContainer');
  AddMethod('function FindChild(AName: String): TrwChildComponent', 'TrwPrintableContainer');
  AddMethod('function ChildByIndex(AIndex: Integer): TrwChildComponent', 'TrwPrintableContainer');
  AddMethod('function ChildIndexByName(AName: String): Integer', 'TrwPrintableContainer');

  AddMethod('procedure Print', 'TrwBand');

  AddMethod('function FieldValue(FieldName: String): Variant', 'TrwDataSet');
  AddMethod('procedure Next', 'TrwDataSet');
  AddMethod('procedure First', 'TrwDataSet');
  AddMethod('procedure Last', 'TrwDataSet');
  AddMethod('procedure Prior', 'TrwDataSet');
  AddMethod('function MoveBy(Distance: Integer): Integer', 'TrwDataSet');
  AddMethod('function Eof: Boolean', 'TrwDataSet');
  AddMethod('function Bof: Boolean', 'TrwDataSet');
  AddMethod('function State: Integer', 'TrwDataSet');
  AddMethod('function Active: Boolean', 'TrwDataSet');
  AddMethod('procedure Open', 'TrwDataSet');
  AddMethod('procedure Close', 'TrwDataSet');
  AddMethod('function RecordCount: Integer', 'TrwDataSet');
  AddMethod('function Locate(KeyFieldName: String; Value: Variant; CaseSensitive: Boolean; PartialKey: Boolean): Boolean', 'TrwDataSet');
  AddMethod('procedure Edit', 'TrwDataSet');
  AddMethod('procedure Post', 'TrwDataSet');
  AddMethod('procedure Cancel', 'TrwDataSet');
  AddMethod('procedure Insert', 'TrwDataSet');
  AddMethod('procedure Append', 'TrwDataSet');
  AddMethod('procedure Delete', 'TrwDataSet');
  AddMethod('procedure SetFieldValue(FieldName: String; Value: Variant)', 'TrwDataSet');
  AddMethod('function  FieldExists(FieldName: String): Boolean', 'TrwDataSet');
  AddMethod('function  RecordNumber: Integer', 'TrwDataSet');
  AddMethod('function  FieldCount: Integer', 'TrwDataSet');
  AddMethod('procedure DisableControls', 'TrwDataSet');
  AddMethod('procedure EnableControls', 'TrwDataSet');
  AddMethod('function GetDataStr(AStoreStructure: Boolean; AStoreData: Boolean): String', 'TrwDataSet');

  AddMethod('procedure SetParameterValue(ParamName: String; Value: Variant)', 'TrwQuery');
  AddMethod('function  GetParameterValue(ParamName: String): Variant', 'TrwQuery');
  AddMethod('function  ParameterExists(ParamName: String): Boolean', 'TrwQuery');
  AddMethod('procedure Execute', 'TrwQuery');
  AddMethod('procedure SetMacroValue(MacroName: String; Value: String)', 'TrwQuery');
  AddMethod('function  GetMacroValue(MacroName: String): String', 'TrwQuery');

  AddMethod('procedure Clear', 'TrwBuffer');
  AddMethod('procedure CreateLikeDataSet(ADataSet: TrwDataSet)', 'TrwBuffer');
  AddMethod('procedure FillLikeDataSet(ADataSet: TrwDataSet)', 'TrwBuffer');
  AddMethod('procedure LoadFromFile(AFileName: String; ADelimiter: String; AScreenChar: String)', 'TrwBuffer');
  AddMethod('procedure SetDataStr(AData: String; ALoadStructure: Boolean; ALoadData: Boolean)', 'TrwBuffer');

  AddMethod('function Count: Integer', 'TrwFields');
  AddMethod('function FieldByName(AFieldName: String): TrwField', 'TrwFields');
  AddMethod('function Items(AIndex: Integer): TrwField', 'TrwFields');
  AddMethod('function CreateField(AFieldName: string; AFieldType: Integer; AFieldSize: Integer): TrwField', 'TrwFields');

  AddMethod('function GroupByField(FieldName: String): TrwGroup', 'TrwCustomReport');

  AddMethod('function LookUpTo(ATable: String; AField: String): String', 'TrwDictionary');
  AddMethod('function GetFields(ATable: String): Array', 'TrwDictionary');
  AddMethod('function GetPrimKey(ATable: String): Array', 'TrwDictionary');
  AddMethod('function GetLogPrimKey(ATable: String): Array', 'TrwDictionary');
  AddMethod('function GetDisplayLogKey(ATable: String): Array', 'TrwDictionary');
  AddMethod('function FieldDisplayName(ATable: String; AField: String): String', 'TrwDictionary');
  AddMethod('function GetTables: Array', 'TrwDictionary');
  AddMethod('function TableDisplayName(ATable: String): String', 'TrwDictionary');
  AddMethod('function TableGroupName(ATable: String): String', 'TrwDictionary');
  AddMethod('function GetFieldValueDescription(ATable: String; AField: String; AValue: Variant): String', 'TrwDictionary');
  AddMethod('function GetFieldValues(ATable: String; AField: String; ADescriptions: Boolean): Array', 'TrwDictionary');

  AddMethod('function Count: Integer', 'TrwLibComponents');
  AddMethod('function IndexOf(AClassName: String): Integer', 'TrwLibComponents');
  AddMethod('function Items(AIndex: Integer): TrwLibComponent', 'TrwLibComponents');

  AddMethod('function Count: Integer', 'TrwLibFunctions');
  AddMethod('function IndexOf(AFunctionName: String): Integer', 'TrwLibFunctions');
  AddMethod('function Items(AIndex: Integer): TrwLibFunction', 'TrwLibFunctions');

  AddMethod('procedure Print', 'TrwSubReport');

  AddMethod('function Add(Text: String): Integer', 'TrwStrings');
  AddMethod('procedure Delete(Index: Integer)', 'TrwStrings');
  AddMethod('function IndexOf(Text: String): Integer', 'TrwStrings');
  AddMethod('procedure SetLine(Index: Integer; Text: String)', 'TrwStrings');
  AddMethod('function GetLine(Index: Integer): String', 'TrwStrings');
  AddMethod('function Count: Integer', 'TrwStrings');

  AddMethod('procedure SelectAll', 'TrwDBGrid');
  AddMethod('procedure UnSelectAll', 'TrwDBGrid');
  AddMethod('procedure SelectRecord', 'TrwDBGrid');

  AddMethod('function TextWidth(AText: String): Integer', 'TrwFormControl');
  AddMethod('function TextHeight(AText: String): Integer', 'TrwFormControl');

  AddMethod('function RunTimeMode: Boolean', 'TrwInputFormContainer');

  AddMethod('function ControlCount: Integer', 'TrwWinFormControl');
  AddMethod('function FindControl(AName: String): TrwFormControl', 'TrwWinFormControl');
  AddMethod('function ControlByIndex(AIndex: Integer): TrwFormControl', 'TrwWinFormControl');
  AddMethod('function ControlIndexByName(AName: String): Integer', 'TrwWinFormControl');

  AddMethod('function SQLText: String', 'TrwQBQuery');

  AddMethod('procedure Clear', 'TrwCollection');
  AddMethod('procedure Assign(ASource: TrwCollection)', 'TrwCollection');

  AddMethod('procedure Assign(ASource: TrwCollectionItem)', 'TrwCollectionItem');

  AddMethod('function Count: Integer', 'TrwQBShowingFields');
  AddMethod('function FieldByName(AFieldName: String): TrwQBShowingField', 'TrwQBShowingFields');
  AddMethod('function Items(AIndex: Integer): TrwQBShowingField', 'TrwQBShowingFields');
  AddMethod('Procedure DeleteWhatNotInList(AFieldList: Array)', 'TrwQBShowingFields');

  AddMethod('function Name: String', 'TrwQBShowingField');

  AddMethod('function ObjectInstance: Variant', 'TrwCustomObject');

  AddMethod('function ShowDialog: Boolean', 'TrwFileDialog');

  AddMethod('procedure LoadFromFile(AFileName: String)', 'TrwPCLFile');

  AddMethod('function ControlCount: Integer', 'TrmPrintContainer');
  AddMethod('function FindControl(AName: String): TrmPrintControl', 'TrmPrintContainer');
  AddMethod('function ControlByIndex(AIndex: Integer): TrmPrintControl', 'TrmPrintContainer');
  AddMethod('function ControlIndexByName(AName: String): Integer', 'TrmPrintContainer');

  AddMethod('function SubTable: TrmDBTable', 'TrmTableCell');

  AddMethod('procedure LoadFromArray(APicture: Array; AFormatExt: String)', 'TrwPicture');
  AddMethod('function  SaveToArray(var DestArray: Array): String', 'TrwPicture');

  AddMethod('function ControlCount: Integer', 'TrmASCIINode');
  AddMethod('function FindControl(AName: String): TrmASCIIItem', 'TrmASCIINode');
  AddMethod('function ControlByIndex(AIndex: Integer): TrmASCIIItem', 'TrmASCIINode');
  AddMethod('function ControlIndexByName(AName: String): Integer', 'TrmASCIINode');

  AddMethod('function FieldCount: Integer', 'TrmASCIIRecord');
  AddMethod('function FindField(AName: String): TrmASCIIField', 'TrmASCIIRecord');
  AddMethod('function FieldByIndex(AIndex: Integer): TrmASCIIField', 'TrmASCIIRecord');
  AddMethod('function FieldIndexByName(AName: String): Integer', 'TrmASCIIRecord');
  AddMethod('function SubRecordCount: Integer', 'TrmASCIIRecord');
  AddMethod('function FindSubRecord(AName: String): TrmASCIIRecord', 'TrmASCIIRecord');
  AddMethod('function SubRecordByIndex(AIndex: Integer): TrmASCIIRecord', 'TrmASCIIRecord');
  AddMethod('function SubRecordIndexByName(AName: String): Integer', 'TrmASCIIRecord');
  AddMethod('function IsQueryDriven: Boolean', 'TrmASCIIRecord');
end;


procedure TrwPublishedMethods.Execute(const APar1, APar2: string);
begin
end;


procedure TrwPublishedMethods.AddMethod(const AHeaderText: String; const AClass: String);
var
  M: TrwPublishedMethod;
  P, Pb, Pe: Integer;
begin
  M := TrwPublishedMethod(Add);
  M.FHeaderText := AHeaderText;
  M.FCompClass := GetClass(AClass);
  M.Name := ParseFunctionHeader(AHeaderText, M.Params, Pb, Pe, P);
end;


function TrwPublishedMethods.IndexOf(const AClass: TClass; const AMethodName: string): TrwPublishedMethod;
var
  i: Integer;
  M: TrwPublishedMethod;
begin
  Result := nil;
  for i := 0 to Count-1 do
  begin
    M := TrwPublishedMethod(Items[i]);
    if AClass.InheritsFrom(M.CompClass) then
      if SameText(M.Name, AMethodName) then
      begin
        Result := M;
        break;
      end;
  end;
end;


procedure TrwPublishedMethods.ListOfFunction(const AClass: TClass; AList: TList);
var
  i: Integer;
  M: TrwPublishedMethod;
begin
  for i := 0 to Count-1 do
  begin
    M := TrwPublishedMethod(Items[i]);
    if AClass.InheritsFrom(M.CompClass) then
      AList.Add(M);
  end;
end;


{ TrwUserProperty }

constructor TrwUserProperty.Create(Collection: TCollection);
begin
  inherited;

  New(FPPropInfo);

  FName := '';
  FDelegatedName := '';  
  FPointerType := '';
  FPropType :=  rwvUnknown;
  FReadObjValue := nil;
end;


destructor TrwUserProperty.Destroy;
begin
  FreeAndNil(FReadObjValue);
  Dispose(FPPropInfo);
  inherited;
end;

procedure TrwUserProperty.Assign(Source: TPersistent);
begin
  inherited;
  FName := TrwUserProperty(Source).Name;
  FDelegatedName := TrwUserProperty(Source).DelegatedName;
  FPointerType := TrwUserProperty(Source).PointerType;
  FPropType :=  TrwUserProperty(Source).PropType;
end;


function TrwUserProperty.GetValue: Variant;
begin
  if FInLimbo and (TrwUserProperties(Collection).Owner <> nil) and
     (csWriting in TComponent(TrwUserProperties(Collection).Owner).ComponentState) then
  begin
    Result := FReadValue;
    Exit;
  end;

  Result := CallReadProc;
end;

procedure TrwUserProperty.SetValue(const Value: Variant);
var
  vt: Integer;
begin
  if (TrwUserProperties(Collection).Owner <> nil) and
    ((csLoading in TComponent(TrwUserProperties(Collection).Owner).ComponentState) or
      TrwComponent(TrwUserProperties(Collection).Owner).Loading) then
  begin
    FReadValue := Value;

    case FPropType of
      rwvInteger,
      rwvPointer:  vt := varInteger;

      rwvFloat:    vt := varDouble;

      rwvCurrency: vt := varCurrency;

      rwvDate:     vt := varDate;

      rwvString:   vt := varString;

      rwvBoolean:  vt := varBoolean;
    else
      vt := varUnknown;
    end;

    if vt <> varUnknown then
      VarCast(FReadValue, FReadValue, vt);
  end

  else
    CallWriteProc(Value);
end;


function TrwUserProperty.CallReadProc: Variant;
begin
  try
    if IsDelegated then
      Result := CallReadProcDelegated

    else
    begin
      TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := True;
      try
        TrwComponent(TrwUserProperties(Collection).Owner).GlobalVarFunc.ExecuteEventsHandler('Get' + Name, []);
        Result := VM.RegA;
      finally
        TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := False;
      end;
    end;

  except
    on E: Exception do
    begin
      E.Message := 'Error reading user property "' + Name + '"' + #13 + E.Message;
      raise;
    end;
  end;
end;

procedure TrwUserProperty.CallWriteProc(AValue: Variant);
var
  lParams: array [0..0] of variant;
begin
  try
    if IsDelegated then
      CallWriteProcDelegated(AValue)

    else
    begin
      TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := True;
      try
        lParams[0] := AValue;
        TrwComponent(TrwUserProperties(Collection).Owner).GlobalVarFunc.ExecuteEventsHandler('Set' + Name, lParams);
      finally
        TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := False;
      end;
    end;

  except
    on E: Exception do
    begin
      E.Message := 'Error writing user property "' + Name + '"' + #13 + E.Message;
      raise;
    end;
  end;
end;


function TrwUserProperty.IsTheSame(AUserProperty: TrwUserProperty): Boolean;
begin
  Result := False;

  if (Name = AUserProperty.Name) and
     (PropType = AUserProperty.PropType) and (PointerType = AUserProperty.PointerType) then
  begin
    if IsObject then
      if IsComponent then
        Result := (Value = AUserProperty.Value)

      else
        Result := CompareObjects(TPersistent(Pointer(Integer(Value))),
                                 TPersistent(Pointer(Integer(AUserProperty.Value))))
                                 
    else
      Result := (Value = Null) and (AUserProperty.Value = Null) or
                (Value = AUserProperty.Value);
  end;
end;

function TrwUserProperty.FixUpValue: Boolean;
begin
  if not SystemLibComponents.CompilingInProgress then
  begin
    try
      if IsPersistent then
      begin
        if Assigned(FReadObjValue) then
          CallWriteProc(Integer(Pointer(FReadObjValue)));
      end
      else
        CallWriteProc(FReadValue);

      FInLimbo := False;
    except
      FInLimbo := True;
    end;
  end

  else
  begin
    FInLimbo := True;
    Result := True;
    Exit;
  end;

  if not FInLimbo then
  begin
    FReadValue := Unassigned;
    FreeAndNil(FReadObjValue);
  end;

  Result := not FInLimbo;
end;



function TrwUserProperty.IsObject: Boolean;
begin
  Result := (FPropType = rwvPointer) and (Length(FPointerType) > 0);
end;


function TrwUserProperty.IsComponent: Boolean;
var
  Cl: IrwClass;
begin
  if IsObject then
  begin
    Cl := rwRTTIInfo.FindClass(FPointerType);
    Result := Assigned(Cl) and (Cl.IsUserClass or Cl.DescendantOf('TrwComponent'));
  end
  else
    Result := False;
end;

function TrwUserProperty.IsPersistent: Boolean;
var
  Cl: TClass;
begin
  if IsObject then
  begin
    Cl := GetClass(FPointerType);
    Result := Assigned(Cl) and Cl.InheritsFrom(TPersistent) and
              not Cl.InheritsFrom(TComponent);
  end

  else
    Result := False;
end;


function TrwUserProperty.GetComponentValue: TComponent;
begin
  if FInLimbo and (TrwUserProperties(Collection).Owner <> nil) and
     (csWriting in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState) then
  begin
    Result := TComponent(FReadObjValue);
    Exit;
  end;

  if (TrwUserProperties(Collection).Owner <> nil) and
     ((csLoading in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState) or
      TrwComponent(TrwUserProperties(Collection).Owner).Loading) then
    Result := TComponent(FReadObjValue)
  else
    Result := TComponent(Pointer(Integer(Value)));
end;

function TrwUserProperty.GetPersistentValue: TPersistent;
begin
  if FInLimbo and (TrwUserProperties(Collection).Owner <> nil) and
     (csWriting in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState) then
  begin
    Result := FReadObjValue;
    Exit;
  end;

  if (TrwUserProperties(Collection).Owner <> nil) and
    (TrwComponent(TrwUserProperties(Collection).Owner).Loading or
    (csLoading in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState)) then
  begin
    CreateReadPersistentObj(FPointerType);
    Result := FReadObjValue;
  end

  else
    Result := TPersistent(Pointer(Integer(Value)));
end;


procedure TrwUserProperty.SetComponentValue(const AValue: TComponent);
begin
  if (TrwUserProperties(Collection).Owner <> nil) and
    ((csLoading in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState) or
      TrwComponent(TrwUserProperties(Collection).Owner).Loading) then
    FReadValue := Integer(Pointer(AValue))
  else
    Value := Integer(Pointer(AValue));
end;

procedure TrwUserProperty.SetPersistentValue(const AValue: TPersistent);
begin
  if (TrwUserProperties(Collection).Owner <> nil) and
    ((csLoading in TrwComponent(TrwUserProperties(Collection).Owner).ComponentState) or
      TrwComponent(TrwUserProperties(Collection).Owner).Loading) then
  begin
    CreateReadPersistentObj(AValue.ClassName);
    FReadObjValue.Assign(AValue);
  end

  else
    Value := Integer(Pointer(AValue));
end;


function TrwUserProperty.GetPPropInfo: PPropInfo;
begin
  FPPropInfo^.Name := Name;
  FPPropInfo^.PropType := Addr(rwTypeInfoList[PropType]);
  Result := FPPropInfo;
end;



function TrwUserProperty.GetPointerClass: TClass;
var
  Cl: IrwClass;
begin
  Cl := rwRTTIInfo.FindClass(FPointerType);
  if Assigned(Cl) then
    Result := Cl.GetVCLClass
  else
    Result := nil;
end;

function TrwUserProperty.CanWritePersistent: Boolean;
begin
  try
    Result := IsPersistent and (FInLimbo or Assigned(Pointer(Integer(Value))));
  except
    Result := False;
  end
end;

procedure TrwUserProperty.CreateReadPersistentObj(AClassName: String);
var
  lPersistentClass: TPersistentClass;
begin
  if Assigned(FReadObjValue) and not (AnsiSameText(FReadObjValue.ClassName, AClassName)) then
    FreeAndNil(FReadObjValue);

  if not Assigned(FReadObjValue) then
  begin
    lPersistentClass := GetClass(AClassName);

    if (lPersistentClass = TrwFont) or (lPersistentClass = TFont) then
      FReadObjValue := TrwFont.Create(Self)
    else if lPersistentClass = TrwStrings then
      FReadObjValue := TrwStrings.Create
    else if lPersistentClass = TrwQBQuery then
      FReadObjValue := TrwQBQuery.Create(nil)
    else if lPersistentClass = TrwFields then
      FReadObjValue := TrwFields.Create(nil, TrwField)
    else
      raise ErwException.Create('Class ' + AClassName +' is not registered');
  end;
end;


function TrwUserProperty.CanWriteValue: Boolean;
begin
  try
    Result := not IsObject and (FInLimbo or not VarIsEmpty(Value));
  except
    Result := False;
  end
end;

function TrwUserProperty.CanWriteComponent: Boolean;
begin
  try
    Result := IsComponent and (FInLimbo or not VarIsEmpty(Value));
  except
    Result := False;
  end
end;

function TrwUserProperty.IsDelegated: Boolean;
begin
  Result := FDelegatedName <> '';
end;

procedure TrwUserProperty.DelegatedPropertyInfo(var AObject: TObject; AProp: PTPropNamesArray);
var
  h1, h2: String;
  i: Integer;
begin
  h1 := FDelegatedName;
  h2 := GetNextStrValue(h1, '.');
  AObject := TrwComponent(TrwUserProperties(Collection).Owner).FindComponent(h2);

  SetLength(AProp^, 10);  // max hierarchy depth of the delegated property
  i := 0;
  while h1 <> '' do
  begin
    AProp^[i] := GetNextStrValue(h1, '.');
    Inc(i);
  end;
  SetLength(AProp^, i);
end;

function TrwUserProperty.CallReadProcDelegated: Variant;
var
  Obj: TObject;
  P: TPropNamesArray;
begin
  DelegatedPropertyInfo(Obj, @P);
  Result := GetComponentProperty(Obj, P);
end;

procedure TrwUserProperty.CallWriteProcDelegated(AValue: Variant);
var
  Obj: TObject;
  P: TPropNamesArray;
  lParams: array of variant;
  h: String;
begin
  DelegatedPropertyInfo(Obj, @P);
  SetComponentProperty(Obj, P, AValue);

  h := 'OnSet' + Name;
  if TrwComponent(TrwUserProperties(Collection).Owner).GlobalVarFunc.EventsList.IndexOf(h) <> -1 then
  begin
    TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := True;
    try
      SetLength(lParams, 0);
      TrwComponent(TrwUserProperties(Collection).Owner).GlobalVarFunc.ExecuteEventsHandler(h, lParams);
    finally
      TrwComponent(TrwUserProperties(Collection).Owner).FIgnoreDesignMode := False;
    end;
  end;
end;


procedure TrwUserProperty.SetPointerType(const Value: String);
begin
  if SameText(Value, 'TFont') then  // temporary
    FPointerType := 'TrwFont'
  else
    FPointerType := Value;
end;


{ TrwUserProperties }

function TrwUserProperties.Add: TrwUserProperty;
begin
  Result := TrwUserProperty(inherited Add);
end;

procedure TrwUserProperties.Assign(Source: TPersistent);
var
  i, j: Integer;
  CI: TrwCollectionItem;
begin
  if (Owner <> nil) and (TrwComponent(Owner).InheritedComponent or TrwComponent(Owner).IsLibComponent) then
  begin
    i := 0;
    while i < Count do
      if not Items[i].InheritedItem then
        Delete(i)
      else
        Inc(i);
    for i := 0 to TrwCollection(Source).Count-1 do
    begin
      j := IndexOf(TrwUserProperties(Source).Items[i].Name);
      if (j <> -1) and Items[j].InheritedItem and
         (TrwUserProperties(Source).Items[i].PropType = Items[j].PropType) then
      begin
        Items[j].Assign(TrwCollection(Source).Items[i]);
        Items[j].InheritedItem := True;
        Items[j].Value := TrwUserProperties(Source).Items[i].Value;
      end

      else if (j = -1) and not (TrwCollectionItem(TrwCollection(Source).Items[i]).InheritedItem) then
      begin
        CI := Add;
        CI.Assign(TrwCollection(Source).Items[i]);
        TrwUserProperty(CI).Value := TrwUserProperties(Source).Items[i].Value;
      end;
    end;
  end

  else
    inherited;
end;

procedure TrwUserProperties.ClearNotInLimbo;
var
  i: Integer;
begin
  i := Count - 1;
  while i >= 0 do
  begin
    if not Items[i].FInLimbo then
      Items[i].Free;
    Dec(i);  
  end;
end;

function TrwUserProperties.GetItem(Index: Integer): TrwUserProperty;
begin
  Result := TrwUserProperty(inherited GetItem(Index));
end;

function TrwUserProperties.IndexOf(AName: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  AName := AnsiUpperCase(AName);

  for i := 0 to Count-1 do
    if AnsiUpperCase(Items[i].Name) = AName then
    begin
      Result := i;
      break;
    end;
end;

procedure TrwUserProperties.SetItem(Index: Integer; Value: TrwUserProperty);
begin
  inherited SetItem(Index, Value);
end;


{ TrwLibCollectionItem }

procedure TrwLibCollectionItem.Assign(Source: TPersistent);
begin
  inherited;
  FName := TrwLibCollectionItem(Source).Name;
  DisplayName := TrwLibCollectionItem(Source).DisplayName;
  FGroup := TrwLibCollectionItem(Source).Group;
  FDescription := TrwLibCollectionItem(Source).Description;
  Data := TrwLibCollectionItem(Source).Data;
end;

function TrwLibCollectionItem.ChangeReasonIsNotEmpty: Boolean;
begin
  Result := FChangeReason <> '';
end;

procedure TrwLibCollectionItem.Compile;
begin
  FCompiled := True;
  Inc(TrwLibCollection(Collection).FCompilingProgress);
end;

function TrwLibCollectionItem.DataIsNotEmpty: Boolean;
begin
  Result := Data <> '';
end;

function TrwLibCollectionItem.DescriptionIsNotEmpty: Boolean;
begin
  Result := FDescription <> '';
end;

function TrwLibCollectionItem.DisplayNameIsNotEmpty: Boolean;
begin
  Result := DisplayName <> '';
end;

function TrwLibCollectionItem.GetData: string;
begin
  Result := FData;
end;

function TrwLibCollectionItem.GetDescription: String;
begin
  Result := FDescription;
end;

function TrwLibCollectionItem.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

function TrwLibCollectionItem.GetGroup: String;
begin
  Result := FGroup;
end;

function TrwLibCollectionItem.GetName: string;
begin
  Result := FName;
end;

function TrwLibCollectionItem.GroupIsNotEmpty: Boolean;
begin
  Result := FGroup <> '';
end;

procedure TrwLibCollectionItem.SetData(const AValue: string);
begin
  FData := AValue;
end;

procedure TrwLibCollectionItem.SetDescription(const AValue: String);
begin
  FDescription := AValue;
end;

procedure TrwLibCollectionItem.SetDisplayName(const AValue: string);
begin
  FDisplayName := AValue;
  inherited;
end;

function TrwFunction.GetFunctionHeader: String;
var
  lParams: String;
  i: Integer;
begin
  lParams := '';

  for i := 0 to Params.Count - 1 do
  begin
    if i > 0 then
      lParams := lParams + ', ';
    lParams := lParams + Params[i].Descriptor.Name + ': ' + VarTypeToString(Params[i].Descriptor^);
  end;

  Result := Name + ' (' + lParams + ')';

  if Assigned(Params.Result) then
    Result := Result + ': ' + VarTypeToString(Params.Result^);
end;

procedure TrwLibCollectionItem.SetGroup(const AValue: String);
begin
  FGroup := AValue;
end;



procedure TrwLibCollectionItem.SetName(const AValue: string);
begin
  FName := AValue;
end;

{ TrmAggrFunctions }

procedure TrmAggrFunctions.AddFunction(const AHeaderText, AGroup, ADescription: String; const ADefaultValues: array of Variant);
var
  F: TrmAggrFunction;
  P, Pb, Pe, i: Integer;
begin
  F := TrmAggrFunction(AddLibItem('', AGroup, ADescription));
  F.FHeaderText := AHeaderText;
  F.Name := ParseFunctionHeader(AHeaderText, F.Params, Pb, Pe, P);
  F.DesignTimeInfo := [fdtShowInEditor];

  for i := Low(ADefaultValues) to High(ADefaultValues) do
    F.Params[i].DefaultValue := ADefaultValues[i];
end;

procedure TrmAggrFunctions.Execute(const APar1, APar2: string);
begin
end;

procedure TrmAggrFunctions.Initialize;
var
  EmptyVar: Variant;
begin
  VarClear(EmptyVar);

  AddFunction('function AVG(AValue: Float; CheckCondition: Boolean): Float', 'Aggregate Functions',
              'Returns average value', [EmptyVar, True]);

  AddFunction('function COUNT(AValue: Variant; CheckCondition: Boolean; AUnique: Boolean): Integer', 'Aggregate Functions',
              'Returns quanity of values', [EmptyVar, True, False]);

  AddFunction('function MAX(AValue: Variant; CheckCondition: Boolean): Variant', 'Aggregate Functions',
              'Returns maximum value', [EmptyVar, True]);

  AddFunction('function MIN(AValue: Variant; CheckCondition: Boolean): Variant', 'Aggregate Functions',
              'Returns minimum value', [EmptyVar, True]);

  AddFunction('function SUM(AValue: Float; CheckCondition: Boolean): Float', 'Aggregate Functions',
              'Returns sum of values', [EmptyVar, True]);
end;


{ TrwFuncParam }

constructor TrwFuncParam.Create(Collection: TCollection);
begin
  inherited;
  DefaultValue := '';  
end;

procedure TrwFuncParam.Assign(ASource: TPersistent);
begin
  FDescriptor := TrwFuncParam(ASource).Descriptor^;
  FDisplayName := TrwFuncParam(ASource).DisplayName;
  Domain := TrwFuncParam(ASource).Domain;
  DefaultValue := TrwFuncParam(ASource).DefaultValue;
end;

procedure TrwFuncParam.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Domain', ReadDomain, WriteDomain, Length(Domain) > 0);
  Filer.DefineProperty('DefaultValue', ReadDefaultValue, WriteDefaultValue, VarToStr(DefaultValue) <> '');
end;

function TrwFuncParam.DomainValueToStr(const AValue: Variant): String;
var
  i: Integer;
begin
  if VarIsArray(AValue) then
  begin
    Result := '';
    for i := VarArrayLowBound(AValue, 1) to VarArrayHighBound(AValue, 1) do
    begin
      if i > VarArrayLowBound(AValue, 1) then
        Result := Result + ',';
      Result := Result + VarToStr(AValue[i]);
    end;
  end

  else if VarIsNull(AValue) then
    Result := '<NULL>'

  else
    Result := VarToStr(AValue);
end;

function TrwFuncParam.GetDescriptor: PTrwVariable;
begin
  Result := @FDescriptor;
end;

procedure TrwFuncParam.ReadDefaultValue(Reader: TReader);
begin
  DefaultValue := StrToDomainValue(Reader.ReadString);
end;

procedure TrwFuncParam.ReadDomain(Reader: TReader);
var
  i: Integer;
  h: String;
begin
  Reader.ReadListBegin;
  i := Reader.ReadInteger;
  SetLength(FDomain, i);
  for i := Low(Domain) to High(Domain) do
  begin
    h := Reader.ReadString;
    Domain[i].Description := GetNextStrValue(h, '=');
    Domain[i].Value := StrToDomainValue(h);
  end;
  Reader.ReadListEnd;
end;

procedure TrwFuncParam.SetDomain(const AValue: TrwDomain);
begin
  FDomain := Copy(AValue);
end;

function TrwFuncParam.StrToDomainValue(const AValue: String): Variant;
var
  h: String;
  n: Integer;
begin
  if Descriptor.VarType = rwvArray then
  begin
    h := AValue;
    n := 0;
    while h <> '' do
    begin
      GetNextStrValue(h, ',');
      Inc(n);
    end;

    Result := VarArrayCreate([0, n - 1], varVariant);

    h := AValue;
    n := 0;
    while h <> '' do
    begin
      Result[n] := GetNextStrValue(h, ',');
      Inc(n);
    end;
  end

  else
    Result := InternalTypeToVariant(Descriptor.VarType, AValue);
end;

procedure TrwFuncParam.WriteDefaultValue(Writer: TWriter);
begin
  Writer.WriteString(DomainValueToStr(DefaultValue));
end;

procedure TrwFuncParam.WriteDomain(Writer: TWriter);
var
  i: Integer;
  h: String;
begin
  Writer.WriteListBegin;
  Writer.WriteInteger(Length(Domain));
  for i := Low(Domain) to High(Domain) do
  begin
    h := Domain[i].Description + '=' + DomainValueToStr(Domain[i].Value);
    Writer.WriteString(h);
  end;
  Writer.WriteListEnd;
end;

function TrwFuncParam.GetName: String;
begin
  Result := Descriptor.Name;
end;

function TrwFuncParam.GetVarType: TrwVarTypeKind;
begin
  Result := Descriptor.VarType
end;

procedure TrwFuncParam.SetName(const AValue: String);
begin
 Descriptor.Name := AValue;
end;

procedure TrwFuncParam.SetVarType(const AValue: TrwVarTypeKind);
begin
  Descriptor.VarType := AValue;
end;

function TrwFuncParam.GetDisplayName: string;
begin
  Result := FDisplayName;
end;

procedure TrwFuncParam.SetDisplayName(const Value: string);
begin
  FDisplayName := Value;
  inherited;
end;

function TrwFuncParam.DisplayNameIsNotEmpty: Boolean;
begin
  Result := DisplayName <> '';
end;


{ TrwLocalComponents }

procedure TrwLocalComponents.Compile;
var
  i: Integer;
begin
  // Local components always get compiled into owner p-code section
  if Count > 0 then
  begin
    RWEngineExt.AppAdapter.StartEngineProgressBar('Compiling Local Components...', Count - 1);
    FCompilingInProgress := True;
    try
      for i := 0 to Count - 1 do
        TrwLocalComponent(Items[i]).FCompiled := TrwLocalComponent(Items[i]).IsInheritedItem;

      for i := 0 to Count -1 do
        try
          if not TrwLocalComponent(Items[i]).FCompiled then
            TrwLocalComponent(Items[i]).Compile;

        except
          on E: Exception do
          begin
            E.Message := 'Local Component "' + TrwLocalComponent(Items[i]).Name + '"' + E.Message;
            raise;
          end;
        end;
    finally
      FCompilingInProgress := False;
      RWEngineExt.AppAdapter.EndEngineProgressBar;
    end;
  end;  
end;

function TrwLocalComponents.GetItem(Index: Integer): TrwLocalComponent;
begin
  Result := TrwLocalComponent(inherited Items[Index]);
end;

procedure TrwLocalComponents.Initialize;
begin
// do nothing
end;

procedure TrwLocalComponents.Store;
begin
// do nothing
end;


{ TrwLocalComponent }

function TrwLocalComponent.AsAncestor(const AncestorItem: TrwCollectionItem): Boolean;
begin
  Result := True; // do not allow to change inherited components
end;


function TrwLibFunctions.EntryPointOffset: Integer;
begin
  Result := cVMLibraryFunctionsOffset;
end;

procedure TrwLibFunctions.LoadFromStream(AStream: TStream);
begin
  inherited;
  InitializeHeaders;
end;

end.
