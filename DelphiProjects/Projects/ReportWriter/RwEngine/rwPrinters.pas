{$R-,T-,X+,H+}

unit rwPrinters;

interface

uses Windows, WinSpool, SysUtils, Classes, Graphics, Forms, Dialogs, ExtCtrls, StdCtrls,
     Controls, CommDlg, Dlgs, Printers, rwEngineTypes, rwBasicUtils;

type
  ErwPrinter = class(Exception);

  { TrwPrinter }

  { The printer object encapsulates the printer interface of Windows.  A print
    job is started whenever any redering is done either through a Text variable
    or the printers canvas.  This job will stay open until EndDoc is called or
    the Text variable is closed.  The title displayed in the Print Manager (and
    on network header pages) is determined by the Title property.

    EndDoc - Terminates the print job (and closes the currently open Text).
      The print job will being printing on the printer after a call to EndDoc.
    NewPage - Starts a new page and increments the PageNumber property.  The
      pen position of the Canvas is put back at (0, 0).
    Canvas - Represents the surface of the currently printing page.  Note that
      some printer do not support drawing pictures and the Draw, StretchDraw,
      and CopyRect methods might fail.
    Fonts - The list of fonts supported by the printer.  Note that TrueType
      fonts appear in this list even if the font is not supported natively on
      the printer since GDI can render them accurately for the printer.
    PageHeight - The height, in pixels, of the page.
    PageWidth - The width, in pixels, of the page.
    PageNumber - The current page number being printed.  This is incremented
      when ever the NewPage method is called.  (Note: This property can also be
      incremented when a Text variable is written, a CR is encounted on the
      last line of the page).
    PrinterIndex - Specifies which printer in the TrwPrinters list that is
      currently selected for printing.  Setting this property to -1 will cause
      the default printer to be selected.  If this value is changed EndDoc is
      called automatically.
    Printers - A list of the printers installed in Windows.
    Title - The title used by Windows in the Print Manager and for network
      title pages. }

  TrwPrinterState = (psNoHandle, psHandleIC, psHandleDC);
  TrwPrinterOrientation = (poPortrait, poLandscape);
  TrwPrinterCapability = (pcCopies, pcOrientation, pcCollation, pcDuplexing);
  TrwPrinterCapabilities = set of TrwPrinterCapability;

  TrwPrinter = class(TObject)
  private
    FCanvas: TCanvas;
    FFonts: TStrings;
    FPageNumber: Integer;
    FPrinters: TStrings;
    FPrinterIndex: Integer;
    FTitle: string;
    FPrinting: Boolean;
    FAborted: Boolean;
    FCapabilities: TrwPrinterCapabilities;
    State: TrwPrinterState;
    DC: HDC;
    DevMode: PDeviceMode;
    DeviceMode: THandle;
    FPrinterHandle: THandle;
    FMacroCount: Integer;
    FPCLFonts: TStringList;
    FPrintPCLasGraphic: Boolean;
    FPCLDriver: Boolean;
    FTrays: TStringList;
    FOriginalDevMode: DEVMODE;
    FPrintJobInfo: TrwPrintJobInfoRec;

    procedure SetState(Value: TrwPrinterState); virtual;
    function GetCanvas: TCanvas;
    function GetNumCopies: Integer;
    function GetFonts: TStrings;
    function GetHandle: HDC;
    function GetOrientation: TrwPrinterOrientation;
    function GetPageHeight: Integer;
    function GetPageWidth: Integer;
    function GetPrinterIndex: Integer;
    procedure SetPrinterCapabilities(Value: Integer);
    procedure SetPrinterIndex(Value: Integer); virtual;
    function  GetPrinters: TStrings;
    procedure SetNumCopies(Value: Integer);
    procedure SetOrientation(Value: TrwPrinterOrientation); virtual;
    procedure SetToDefaulTrwPrinter; virtual;
    procedure CheckPrinting(Value: Boolean);
    procedure FreePrinters;
    procedure FreeFonts;
    function  GetPrintPCLasGraphic: Boolean;
    procedure SetPrintPCLasGraphic(const Value: Boolean);
    function GetTrays: TStrings;
    procedure RestorePrinter;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Abort;
    procedure BeginDoc;
    procedure EndDoc;
    procedure NewPage;
    procedure EndPage;
    procedure GetPrinter(ADevice, ADriver, APort: PChar; var ADeviceMode: THandle);
    procedure SetPrinter(ADevice, ADriver, APort: PChar; ADeviceMode: THandle);
    procedure Refresh;

    procedure PCLPrintString(const AData: string);
    procedure PCLPrintPosString(const AData: string; const APos: TPoint);
    function  PCLStoreMacro(const AData: string): Integer;
    procedure PCLPrintMacro(AMacroID: Integer);
    function  PCLMacroIsStored(AMacroID: Integer): Boolean;
    function  PCLFontIsStored(const AFontID: String): Boolean;
    function  PCLStoreFont(const AData: string): String;
    procedure PCLPrintPCLText(const AFontID: String; const AInitStr: String; const AText: String; const APos: TPoint);
    function  OriginalDevMode: DEVMODE;
    function  PrintJobInfo: PTrwPrintJobInfoRec;

    property Aborted: Boolean read FAborted;
    property Canvas: TCanvas read GetCanvas;
    property Capabilities: TrwPrinterCapabilities read FCapabilities;
    property Copies: Integer read GetNumCopies write SetNumCopies;
    property Fonts: TStrings read GetFonts;
    property Trays: TStrings read GetTrays;
    property Handle: HDC read GetHandle;
    property Orientation: TrwPrinterOrientation read GetOrientation write SetOrientation;
    property PageHeight: Integer read GetPageHeight;
    property PageWidth: Integer read GetPageWidth;
    property PageNumber: Integer read FPageNumber;
    property PrinterIndex: Integer read GetPrinterIndex write SetPrinterIndex;
    property Printing: Boolean read FPrinting;
    property Printers: TStrings read GetPrinters;
    property PCLDriver: Boolean read FPCLDriver;
    property Title: string read FTitle write FTitle;
    property PrintPCLasGraphic: Boolean read GetPrintPCLasGraphic write SetPrintPCLasGraphic;
  end;


  TrwDummyPrinter = class(TrwPrinter)
  private
    procedure SetState(Value: TrwPrinterState); override;
    procedure SetToDefaulTrwPrinter; override;
    procedure SetPrinterIndex(Value: Integer); override;
    procedure SetOrientation(Value: TrwPrinterOrientation); override;
  end;


{ Printer function - Replaces the Printer global variable of previous versions,
  to improve smart linking (reduce exe size by 2.5k in projects that don't use
  the printer).  Code which assigned to the Printer global variable
  must call SetPrinter instead.  SetPrinter returns current printer object
  and makes the new printer object the current printer.  It is the caller's
  responsibility to free the old printer, if appropriate.  (This allows
  toggling between different printer objects without destroying configuration
  settings.) }


  TrwPrintDialog = class(TPrintDialog)
  private
    FExtendedPanel: TPanel;
    FCheckBox: TCheckBox;

    function  GetPrintPCLAsGraphics: Boolean;
    procedure SetPrintPCLAsGraphics(const Value: Boolean);

  protected
    procedure DoShow; override;
    function  GetStaticRect: TRect;
    function  TaskModalDialog(DialogFunc: Pointer; var DialogData): Bool; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    Execute: Boolean; override;

    property    PrintPCLAsGraphics: Boolean read GetPrintPCLAsGraphics write SetPrintPCLAsGraphics;
  end;



function rwPrinter: TrwPrinter;
function SetPrinter(NewPrinter: TrwPrinter): TrwPrinter;

{ AssignPrn - Assigns a Text variable to the currently selected printer.  Any
  Write or Writeln's going to that file variable will be written on the
  printer using the Canvas property's font.  A new page is automatically
  started if a CR is encountered on (or a Writeln is written to) the last
  line on the page.  Closing the text file will imply a call to the
  Printer.EndDoc method. Note: only one Text variable can be open on the
  printer at a time.  Opening a second will cause an exception.}

procedure AssignPrn(var F: Text);

implementation


uses Consts;

const
  cBufSize = 255;
  PCL_DELETE_LAST_FONT = #27'*c2F';
  PCL_DELETE_ALL_TEMP_MACRO = #27'&f7X';
  PCL_MACRO_START_DEF = #27'&f0X';
  PCL_MACRO_STOP_DEF = #27'&f1X';
  PCL_MACRO_ENABLE_OVERLAY = #27'&f4X';
  PCL_MACRO_MAKE_TEMP = #27'&f9X';
  PCL_MACRO_CALL = #27'&f3X';
  PCL_DELETE_ALL_TEMP_FONTS = #27'*c1F';

  function PCL_ASSIGN_FONT_ID(AFontId: String): String;
  begin
     Result := #27'*c' + AFontID + 'D';
  end;

  function PCL_MACRO_ID(AMacroId: String): String;
  begin
     Result := #27'&f' + AMacroID + 'Y';
  end;

  function PCL_SELECT_PRIMARY_FONT(AFontId: String): String;
  begin
     Result := #27'(' + AFontID + 'X';
  end;


var
  FPrinter: TrwPrinter = nil;

function FetchStr(var Str: PChar): PChar;
var
  P: PChar;
begin
  Result := Str;
  if Str = nil then Exit;
  P := Str;
  while P^ = ' ' do Inc(P);
  Result := P;
  while (P^ <> #0) and (P^ <> ',') do Inc(P);
  if P^ = ',' then
  begin
    P^ := #0;
    Inc(P);
  end;
  Str := P;
end;

procedure RaiseError(const Msg: string);
begin
  raise ErwPrinter.Create(Msg);
end;

function AbortProc(Prn: HDC; Error: Integer): Bool; stdcall;
begin
  Result := not FPrinter.Aborted;
end;

type
  PrnRec = record
    case Integer of
      1: (
        Cur: TPoint;
        Finish: TPoint;         { End of the printable area }
        Height: Integer);       { Height of the current line }
      2: (
        Tmp: array[1..32] of Char);
  end;

procedure NewPage(var Prn: PrnRec);
begin
  with Prn do
  begin
    Cur.X := 0;
    Cur.Y := 0;
    FPrinter.NewPage;
  end;
end;

{ Start a new line on the current page, if no more lines left start a new
  page. }
procedure NewLine(var Prn: PrnRec);

  function CharHeight: Word;
  var
    Metrics: TTextMetric;
  begin
    GetTextMetrics(FPrinter.Canvas.Handle, Metrics);
    Result := Metrics.tmHeight;
  end;

begin
  with Prn do
  begin
    Cur.X := 0;
    if Height = 0 then
      Inc(Cur.Y, CharHeight) else
      Inc(Cur.Y, Height);
    if Cur.Y > (Finish.Y - (Height * 2)) then NewPage(Prn);
    Height := 0;
  end;
end;

{ Print a string to the printer without regard to special characters.  These
  should handled by the caller. }
procedure PrnOutStr(var Prn: PrnRec; Text: PChar; Len: Integer);
var
  Extent: TSize;
  L: Integer;
begin
  with Prn, FPrinter.Canvas do
  begin
    while Len > 0 do
    begin
      L := Len;
      GetTextExtentPoint(Handle, Text, L, Extent);

      while (L > 0) and (Extent.cX + Cur.X > Finish.X) do
      begin
        L := CharPrev(Text, Text+L) - Text;
        GetTextExtentPoint(Handle, Text, L, Extent);
      end;

      if Extent.cY > Height then Height := Extent.cY + 2;
      Windows.TextOut(Handle, Cur.X, Cur.Y, Text, L);
      Dec(Len, L);
      Inc(Text, L);
      if Len > 0 then NewLine(Prn)
      else Inc(Cur.X, Extent.cX);
    end;
  end;
end;

{ Print a string to the printer handling special characters. }
procedure PrnString(var Prn: PrnRec; Text: PChar; Len: Integer);
var
  L: Integer;
  TabWidth: Word;

  procedure Flush;
  begin
    if L <> 0 then PrnOutStr(Prn, Text, L);
    Inc(Text, L + 1);
    Dec(Len, L + 1);
    L := 0;
  end;

  function AvgCharWidth: Word;
  var
    Metrics: TTextMetric;
  begin
    GetTextMetrics(FPrinter.Canvas.Handle, Metrics);
    Result := Metrics.tmAveCharWidth;
  end;

begin
  L := 0;
  with Prn do
  begin
    while L < Len do
    begin
      case Text[L] of
        #9:
          begin
            Flush;
            TabWidth := AvgCharWidth * 8;
            Inc(Cur.X, TabWidth - ((Cur.X + TabWidth + 1)
              mod TabWidth) + 1);
            if Cur.X > Finish.X then NewLine(Prn);
          end;
        #13: Flush;
        #10:
          begin
            Flush;
            NewLine(Prn);
          end;
        ^L:
          begin
            Flush;
            NewPage(Prn);
          end;
      else
        Inc(L);
      end;
    end;
  end;
  Flush;
end;

{ Called when a Read or Readln is applied to a printer file. Since reading is
  illegal this routine tells the I/O system that no characters where read, which
  generates a runtime error. }
function PrnInput(var F: TTextRec): Integer;
begin
  with F do
  begin
    BufPos := 0;
    BufEnd := 0;
  end;
  Result := 0;
end;

{ Called when a Write or Writeln is applied to a printer file. The calls
  PrnString to write the text in the buffer to the printer. }
function PrnOutput(var F: TTextRec): Integer;
begin
  with F do
  begin
    PrnString(PrnRec(UserData), PChar(BufPtr), BufPos);
    BufPos := 0;
    Result := 0;
  end;
end;

{ Will ignore certain requests by the I/O system such as flush while doing an
  input. }
function PrnIgnore(var F: TTextRec): Integer;
begin
  Result := 0;
end;

{ Deallocates the resources allocated to the printer file. }
function PrnClose(var F: TTextRec): Integer;
begin
  with PrnRec(F.UserData) do
  begin
    FPrinter.EndDoc;
    Result := 0;
  end;
end;

{ Called to open I/O on a printer file.  Sets up the TTextFile to point to
  printer I/O functions. }
function PrnOpen(var F: TTextRec): Integer;
const
  Blank: array[0..0] of Char = '';
begin
  with F, PrnRec(UserData) do
  begin
    if Mode = fmInput then
    begin
      InOutFunc := @PrnInput;
      FlushFunc := @PrnIgnore;
      CloseFunc := @PrnIgnore;
    end else
    begin
      Mode := fmOutput;
      InOutFunc := @PrnOutput;
      FlushFunc := @PrnOutput;
      CloseFunc := @PrnClose;
      FPrinter.BeginDoc;

      Cur.X := 0;
      Cur.Y := 0;
      Finish.X := FPrinter.PageWidth;
      Finish.Y := FPrinter.PageHeight;
      Height := 0;
    end;
    Result := 0;
  end;
end;

procedure AssignPrn(var F: Text);
begin
  with TTextRec(F), PrnRec(UserData) do
  begin
    rwPrinter;
    FillChar(F, SizeOf(F), 0);
    Mode := fmClosed;
    BufSize := SizeOf(Buffer);
    BufPtr := @Buffer;
    OpenFunc := @PrnOpen;
  end;
end;

{ TrwPrinterDevice }

type
  TrwPrinterDevice = class
    Driver, Device, Port: String;
    constructor Create(ADriver, ADevice, APort: PChar);
    function IsEqual(ADriver, ADevice, APort: PChar): Boolean;
  end;

constructor TrwPrinterDevice.Create(ADriver, ADevice, APort: PChar);
begin
  inherited Create;
  Driver := ADriver;
  Device := ADevice;
  Port := APort;
end;

function TrwPrinterDevice.IsEqual(ADriver, ADevice, APort: PChar): Boolean;
begin
  Result := (Device = ADevice) and ((Port = '') or (Port = APort));
end;

{ TrwPrinterCanvas }

type
  TrwPrinterCanvas = class(TCanvas)
    Printer: TrwPrinter;
    constructor Create(APrinter: TrwPrinter);
    procedure CreateHandle; override;
    procedure Changing; override;
  end;

constructor TrwPrinterCanvas.Create(APrinter: TrwPrinter);
begin
  inherited Create;
  Printer := APrinter;
end;

procedure TrwPrinterCanvas.CreateHandle;
begin
  rwPrinter.SetState(psHandleIC);
  Handle:= rwPrinter.DC;
end;

procedure TrwPrinterCanvas.Changing;
begin
  rwPrinter.CheckPrinting(True);
  inherited Changing;
end;


{ TrwPrinter }

constructor TrwPrinter.Create;
begin
  inherited Create;
  FPrinterIndex := -1;
  FPCLFonts := TStringList.Create;
  FTrays := TStringList.Create;
  FPrintPCLasGraphic := True;
  FPCLDriver := False;

  with FPrintJobInfo do
  begin
    PrintJobName := 'Report Writer Report';
    PrinterName := '';
    FirstPage := 0;
    LastPage := 0;
    Layers := [];
    Duplexing := rdpDefault;
    PrintPCLAsGraphic := True;
    TrayName := '';
    OutBinNbr := 0;
    PrinterVerticalOffset := 0;
    PrinterHorizontalOffset := 0;
    Copies := 1;
  end;
  Title := FPrintJobInfo.PrintJobName;
end;

destructor TrwPrinter.Destroy;
begin
  if Printing then EndDoc;
  SetState(psNoHandle);
  FreePrinters;
  FreeFonts;
  FCanvas.Free;
  if FPrinterHandle <> 0 then ClosePrinter(FPrinterHandle);
  if DeviceMode <> 0 then
  begin
    GlobalUnlock(DeviceMode);
    GlobalFree(DeviceMode);
    DeviceMode := 0;
  end;
  FPCLFonts.Free;
  FTrays.Free;

  inherited;
end;

procedure TrwPrinter.SetState(Value: TrwPrinterState);
type
  TCreateHandleFunc = function (DriverName, DeviceName, Output: PChar;
    InitData: PDeviceMode): HDC stdcall;
var
  CreateHandleFunc: TCreateHandleFunc;
begin
  if Value <> State then
  begin
    CreateHandleFunc := nil;
    case Value of
      psNoHandle:
        begin
          CheckPrinting(False);
          if Assigned(FCanvas) then FCanvas.Handle := 0;
          DeleteDC(DC);
          DC := 0;
        end;
      psHandleIC:
        if State <> psHandleDC then CreateHandleFunc := CreateIC
        else Exit;
      psHandleDC:
        begin
          if FCanvas <> nil then FCanvas.Handle := 0;
          if DC <> 0 then DeleteDC(DC);
          CreateHandleFunc := CreateDC;
        end;
    end;
    if Assigned(CreateHandleFunc) then
      with TrwPrinterDevice(Printers.Objects[PrinterIndex]) do
      begin
        DC := CreateHandleFunc(PChar(Driver), PChar(Device), PChar(Port), DevMode);
        if DC = 0 then RaiseError(SInvalidPrinter);
        if FCanvas <> nil then FCanvas.Handle := DC;
      end;
    State := Value;
  end;
end;

procedure TrwPrinter.CheckPrinting(Value: Boolean);
begin
  if Printing <> Value then
    if Value then RaiseError(SNotPrinting)
    else RaiseError(SPrinting);
end;

procedure TrwPrinter.Abort;
begin
  CheckPrinting(True);
  AbortDoc(Canvas.Handle);
  FAborted := True;
  EndDoc;
end;

procedure TrwPrinter.BeginDoc;
var
  DocInfo: TDocInfo;
  FlagNotShowed: Boolean;
  oldWidth, oldHeight: integer;
  oldBorderStyle: TFormBorderStyle;
begin
  CheckPrinting(False);

//  Printer.PrinterIndex := rwPrinter.PrinterIndex;

  FMacroCount := 0;
  FPCLFonts.Clear;
  SetState(psHandleDC);
  Canvas.Refresh;
  FPrinting := True;
  FAborted := False;
  FPageNumber := 0;
  FillChar(DocInfo, SizeOf(DocInfo), 0);
  with DocInfo do
  begin
    cbSize := SizeOf(DocInfo);
    lpszDocName := PChar(Title);
  end;
  SetAbortProc(DC, AbortProc);

  if Assigned(Application.MainForm) then
  begin
    oldWidth := Application.MainForm.Width;
    oldHeight := Application.MainForm.Height;
    oldBorderStyle := Application.MainForm.BorderStyle;
    FlagNotShowed := not Application.ShowMainForm;
  end
  else
  begin
    oldWidth := 0;
    oldHeight := 0;
    oldBorderStyle := bsNone;
    FlagNotShowed := False;
  end;
    
  try
    if FlagNotShowed then
    begin
      Application.MainForm.BorderStyle := bsNone;
      Application.MainForm.Width := 1;
      Application.MainForm.Height := 1;
      Application.ShowMainForm := true;
      Application.MainForm.Visible := true;
    end;
    StartDoc(DC, DocInfo);
 finally
    if FlagNotShowed then
    begin
      Application.MainForm.Visible := false;
      Application.ShowMainForm := false;
      Application.MainForm.BorderStyle := oldBorderStyle;
      Application.MainForm.Width := oldWidth;
      Application.MainForm.Height := oldHeight;
    end;
  end;
end;

procedure TrwPrinter.EndDoc;
var
  i: Integer;
begin
  CheckPrinting(True);

  if not FPrintPCLasGraphic then
  begin
    for i := 0 to FPCLFonts.Count - 1 do
    begin
      PCLPrintString(PCL_ASSIGN_FONT_ID(FPCLFonts[i]));
      PCLPrintString(PCL_DELETE_LAST_FONT);
    end;

    PCLPrintString(PCL_DELETE_ALL_TEMP_MACRO);
    PCLPrintString(PCL_DELETE_ALL_TEMP_FONTS);
  end;  
  FPCLFonts.Clear;

  EndPage;
  if not Aborted then Windows.EndDoc(DC);

  FPrinting := False;
  FAborted := False;
  FPageNumber := 0;
  FMacroCount := 0;

  RestorePrinter;
end;

procedure TrwPrinter.NewPage;
begin
  CheckPrinting(True);
  StartPage(Handle);
  Inc(FPageNumber);
  Canvas.Refresh;
end;

procedure TrwPrinter.GetPrinter(ADevice, ADriver, APort: PChar; var ADeviceMode: THandle);
begin
  with TrwPrinterDevice(Printers.Objects[PrinterIndex]) do
  begin
    StrCopySafe(ADevice, PChar(Device), cBufSize);
    StrCopySafe(ADriver, PChar(Driver), cBufSize);
    StrCopySafe(APort, PChar(Port), cBufSize);
  end;
  ADeviceMode := DeviceMode;
end;

procedure TrwPrinter.SetPrinterCapabilities(Value: Integer);
begin
  FCapabilities := [];
  if (Value and DM_ORIENTATION) <> 0 then
    Include(FCapabilities, pcOrientation);
  if (Value and DM_COPIES) <> 0 then
    Include(FCapabilities, pcCopies);
  if (Value and DM_COLLATE) <> 0 then
    Include(FCapabilities, pcCollation);
  if (Value and DM_DUPLEX) <> 0 then
    Include(FCapabilities, pcDuplexing);
end;

procedure TrwPrinter.SetPrinter(ADevice, ADriver, APort: PChar; ADeviceMode: THandle);
var
  I, J, K: Integer;
  StubDevMode: TDeviceMode;
  aBinNames: array of array [1..24] of Char;
  aBins: array of Word;
  h: String;
begin
  CheckPrinting(False);
  if ADeviceMode <> DeviceMode then
  begin  // free the devmode block we have, and take the one we're given
    if DeviceMode <> 0 then
    begin
      GlobalUnlock(DeviceMode);
      GlobalFree(DeviceMode);
    end;
    DeviceMode := ADeviceMode;
  end;
  if DeviceMode <> 0 then
  begin
    DevMode := GlobalLock(DeviceMode);
    SetPrinterCapabilities(DevMode.dmFields);
  end;
  FreeFonts;
  if FPrinterHandle <> 0 then
  begin
    ClosePrinter(FPrinterHandle);
    FPrinterHandle := 0;
  end;
  SetState(psNoHandle);
  J := -1;
  with Printers do   // <- this rebuilds the FPrinters list
    for I := 0 to Count - 1 do
    begin
      if TrwPrinterDevice(Objects[I]).IsEqual(ADriver, ADevice, APort) then
      begin
        TrwPrinterDevice(Objects[I]).Port := APort;
        APort := PChar(TrwPrinterDevice(Objects[I]).Port);
        J := I;
        Break;
      end;
    end;
  if J = -1 then
  begin
    J := FPrinters.Count;
    FPrinters.AddObject(Format(SDeviceOnPort, [ADevice, APort]),
      TrwPrinterDevice.Create(ADriver, ADevice, APort));
  end;
  FPrinterIndex := J;
  if OpenPrinter(ADevice, FPrinterHandle, nil) then
  begin
    if DeviceMode = 0 then  // alloc new device mode block if one was not passed in
    begin
      DeviceMode := GlobalAlloc(GHND,
        DocumentProperties(0, FPrinterHandle, ADevice, StubDevMode,
        StubDevMode, 0));
      if DeviceMode <> 0 then
      begin
        DevMode := GlobalLock(DeviceMode);
        if DocumentProperties(0, FPrinterHandle, ADevice, DevMode^,
          DevMode^, DM_OUT_BUFFER) < 0 then
        begin
          h := SysErrorMessage(GetLastError);
          GlobalUnlock(DeviceMode);
          GlobalFree(DeviceMode);
          DeviceMode := 0;
          FPrinterIndex := -1;
          RaiseError(h);
        end
      end;
    end;
    if DeviceMode <> 0 then
      SetPrinterCapabilities(DevMode^.dmFields);

    FTrays.Clear;
    i := DeviceCapabilities(ADevice, APort, DC_BINS, nil, nil);
    SetLength(aBins, i);
    DeviceCapabilities(ADevice, APort, DC_BINS, @(aBins[0]), nil);
    Assert(DeviceCapabilities(ADevice, APort, DC_BINNAMES, nil, nil) = i);
    SetLength(aBinNames, i);
    DeviceCapabilities(ADevice, APort, DC_BINNAMES, @(aBinNames[0]), nil);
    for k := 0 to i - 1 do
      FTrays.AddObject(PChar(@(aBinNames[k])), Pointer(aBins[k]));

    FOriginalDevMode := DevMode^;

    FPCLDriver := Pos(' PCL', TrwPrinterDevice(Printers.Objects[FPrinterIndex]).Driver) <> 0;
    FPrintPCLasGraphic := not FPCLDriver;
  end

  else
  begin
    FPrinterIndex := -1;
    RaiseError(SysErrorMessage(GetLastError));
  end;
end;

function TrwPrinter.GetCanvas: TCanvas;
begin
  if FCanvas = nil then FCanvas := TrwPrinterCanvas.Create(Self);
  Result := FCanvas;
end;

function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
  FontType: Integer; Data: Pointer): Integer; stdcall;
begin
  TStrings(Data).Add(LogFont.lfFaceName);
  Result := 1;
end;

function TrwPrinter.GetFonts: TStrings;
begin
  if FFonts = nil then
  try
    SetState(psHandleIC);
    FFonts := TStringList.Create;
    EnumFonts(DC, nil, @EnumFontsProc, Pointer(FFonts));
  except
    FreeAndNil(FFonts);
    raise;
  end;
  Result := FFonts;
end;

function TrwPrinter.GetHandle: HDC;
begin
  SetState(psHandleIC);
  Result := DC;
end;

function TrwPrinter.GetNumCopies: Integer;
begin
  GetPrinterIndex;
  if DeviceMode = 0 then RaiseError(SInvalidPrinterOp);
  Result := DevMode^.dmCopies;
end;

procedure TrwPrinter.SetNumCopies(Value: Integer);
begin
  CheckPrinting(False);
  GetPrinterIndex;
  if DeviceMode = 0 then RaiseError(SInvalidPrinterOp);
  SetState(psNoHandle);
  DevMode^.dmCopies := Value;
end;

function TrwPrinter.GetOrientation: TrwPrinterOrientation;
begin
  GetPrinterIndex;
  if DeviceMode = 0 then RaiseError(SInvalidPrinterOp);
  if DevMode^.dmOrientation = DMORIENT_PORTRAIT then Result := poPortrait
  else Result := poLandscape;
end;

procedure TrwPrinter.SetOrientation(Value: TrwPrinterOrientation);
const
  Orientations: array [TrwPrinterOrientation] of Integer = (
    DMORIENT_PORTRAIT, DMORIENT_LANDSCAPE);
begin
  CheckPrinting(False);
  GetPrinterIndex;
  if DeviceMode = 0 then RaiseError(SInvalidPrinterOp);
  SetState(psNoHandle);
  DevMode^.dmOrientation := Orientations[Value];
end;

function TrwPrinter.GetPageHeight: Integer;
begin
  SetState(psHandleIC);
  Result := GetDeviceCaps(DC, VertRes);
end;

function TrwPrinter.GetPageWidth: Integer;
begin
  SetState(psHandleIC);
  Result := GetDeviceCaps(DC, HorzRes);
end;

function TrwPrinter.GetPrinterIndex: Integer;
begin
  if FPrinterIndex = -1 then SetToDefaulTrwPrinter;
  Result := FPrinterIndex;
end;

procedure TrwPrinter.SetPrinterIndex(Value: Integer);
var
  Device: array[0..cBufSize] of char;
  Driver: array[0..cBufSize] of char;
  Port: array[0..cBufSize] of char;
  hDeviceMode: THandle;

begin
  CheckPrinting(False);
  if (Value = -1) or (PrinterIndex = -1) then SetToDefaulTrwPrinter
  else if (Value < 0) or (Value >= Printers.Count) then RaiseError(SPrinterIndexError);
  FPrinterIndex := Value;
  FreeFonts;
  SetState(psNoHandle);

  GetPrinter(Device, Driver, Port, hDeviceMode);
  SetPrinter(Device, Driver, Port, 0);
end;

function TrwPrinter.GetPrinters: TStrings;
var
  LineCur, Port: PChar;
  Buffer, PrinterInfo: PChar;
  Flags, Count, NumInfo: DWORD;
  I: Integer;
  Level: DWORD;
begin
  if FPrinters = nil then
  begin
    FPrinters := TStringList.Create;
    Result := FPrinters;
    try
      if Win32Platform = VER_PLATFORM_WIN32_NT then
      begin
        Flags := PRINTER_ENUM_CONNECTIONS or PRINTER_ENUM_LOCAL;
        Level := 2;
      end
      else
      begin
        Flags := PRINTER_ENUM_LOCAL;
        Level := 5;
      end;
      Count := 0;
      EnumPrinters(Flags, nil, Level, nil, 0, Count, NumInfo);
      if Count = 0 then
        Exit;
      GetMem(Buffer, Count);
      try
        if not EnumPrinters(Flags, nil, Level, PByte(Buffer), Count, Count, NumInfo) then
          Exit;
        PrinterInfo := Buffer;
        for I := 0 to NumInfo - 1 do
        begin
          if Level = 2 then
            with PPrinterInfo2(PrinterInfo)^ do
            begin
              FPrinters.AddObject(pPrinterName,
                TrwPrinterDevice.Create(pDriverName, pPrinterName, pPortName));
              Inc(PrinterInfo, sizeof(TPrinterInfo2));
            end
          else
            with PPrinterInfo5(PrinterInfo)^ do
            begin
              LineCur := pPortName;
              Port := FetchStr(LineCur);
              while Port^ <> #0 do
              begin
                FPrinters.AddObject(Format(SDeviceOnPort, [pPrinterName, Port]),
                  TrwPrinterDevice.Create(nil, pPrinterName, Port));
                Port := FetchStr(LineCur);
              end;
              Inc(PrinterInfo, sizeof(TPrinterInfo5));
            end;
        end;
      finally
        FreeMem(Buffer, Count);
      end;
    except
      FPrinters.Free;
      FPrinters := nil;
      raise;
    end;
  end;
  Result := FPrinters;
end;

procedure TrwPrinter.SetToDefaulTrwPrinter;
var
  I: Integer;
  ByteCnt, StructCnt: DWORD;
  DefaulTrwPrinter: array[0..79] of Char;
  Cur, Device: PChar;
  PrinterInfo: PPrinterInfo5;

  procedure ThereIsNoDefaultPrinter;
  begin
    FPrinterIndex := 0;
    SetPrinterIndex(0);
    if (FPrinterIndex = -1) or (FPrinterIndex = -1) then
      RaiseError(SNoDefaultPrinter);
  end;

begin
  ByteCnt := 0;
  StructCnt := 0;
  if not EnumPrinters(PRINTER_ENUM_DEFAULT, nil, 5, nil, 0, ByteCnt,
    StructCnt) and (GetLastError <> ERROR_INSUFFICIENT_BUFFER) then
  begin
    // With no printers installed, Win95/98 fails above with "Invalid filename".
    // NT succeeds and returns a StructCnt of zero.
    if GetLastError = ERROR_INVALID_NAME then
      ThereIsNoDefaultPrinter
    else
      RaiseLastOSError;
  end;
  PrinterInfo := AllocMem(ByteCnt);
  try
    EnumPrinters(PRINTER_ENUM_DEFAULT, nil, 5, PrinterInfo, ByteCnt, ByteCnt,
      StructCnt);
    if StructCnt > 0 then
      Device := PrinterInfo.pPrinterName
    else begin
      GetProfileString('windows', 'device', '', DefaulTrwPrinter,
        SizeOf(DefaulTrwPrinter) - 1);
      Cur := DefaulTrwPrinter;
      Device := FetchStr(Cur);
    end;
    with Printers do
      for I := 0 to Count-1 do
      begin
        if TrwPrinterDevice(Objects[I]).Device = Device then
        begin
          with TrwPrinterDevice(Objects[I]) do
            SetPrinter(PChar(Device), PChar(Driver), PChar(Port), 0);
          Exit;
        end;
      end;
  finally
    FreeMem(PrinterInfo);
  end;

  ThereIsNoDefaultPrinter;
end;

procedure TrwPrinter.FreePrinters;
var
  I: Integer;
begin
  if FPrinters <> nil then
  begin
    for I := 0 to FPrinters.Count - 1 do
      FPrinters.Objects[I].Free;
    FreeAndNil(FPrinters);
  end;
end;

procedure TrwPrinter.FreeFonts;
begin
  FreeAndNil(FFonts);
end;

procedure TrwPrinter.Refresh;
begin
  FreeFonts;
  FreePrinters;
end;



function rwPrinter: TrwPrinter;
begin
  if FPrinter = nil then
  begin
    FPrinter := TrwPrinter.Create;
    if FPrinter.Printers.Count = 0 then
    begin
      FPrinter.Free;
      FPrinter := TrwDummyPrinter.Create;
    end;
  end;

  Result := FPrinter;
end;


function SetPrinter(NewPrinter: TrwPrinter): TrwPrinter;
begin
  Result := FPrinter;
  FPrinter := NewPrinter;
end;


procedure TrwPrinter.PCLPrintString(const AData: string);
const
  BufSize = 16384;

type
  TPrnBuffRec = record
    BuffLength: word;
    Buffer: array[0..BufSize] of char;
  end;

  pPrnBuffRec = ^TPrnBuffRec;

var
  I: Integer;
  S: String;
  Buff: TPrnBuffRec;
  pBuff: pPrnBuffRec;
begin
  if FPrintPCLasGraphic then
    Exit;

  GetMem(pBuff, SizeOf(TPrnBuffRec));
  Buff := pBuff^;
  for I := 0 to Length(AData) div BufSize do
  begin
    if (I + 1) * BufSize > Length(AData) then
      S := Copy(AData, I * BufSize + 1, Length(AData) - (I * BufSize + 1) + 1)
    else
      S := Copy(AData, I * BufSize + 1, BufSize);
    if S <> '' then
    begin
      CopyMemory(@Buff.Buffer, @S[1], Length(S));
      Buff.BuffLength := Length(S);
      Escape(Handle, PASSTHROUGH, SizeOf(TPrnBuffRec), @Buff, nil);
    end;
  end;
  FreeMem(pBuff);
end;


function TrwPrinter.PCLStoreMacro(const AData: string): Integer;
begin
  if FPrintPCLasGraphic then
  begin
    Result := -1;
    Exit;
  end;

  Inc(FMacroCount);
  PCLPrintString(PCL_MACRO_ID(IntToStr(FMacroCount)) +
                 PCL_MACRO_START_DEF + PCL_MACRO_ENABLE_OVERLAY);
  PCLPrintString(AData);
  PCLPrintString(#27'&f1s' + PCL_MACRO_STOP_DEF + PCL_MACRO_MAKE_TEMP);

  Result := FMacroCount;
end;

procedure TrwPrinter.PCLPrintMacro(AMacroID: Integer);
begin
  PCLPrintString(PCL_MACRO_ID(IntToStr(AMacroID)) + PCL_MACRO_CALL);
end;

function TrwPrinter.PCLMacroIsStored(AMacroID: Integer): Boolean;
begin
  Result := (AMacroID > 0) and (AMacroID <= FMacroCount);
end;


function TrwPrinter.PCLFontIsStored(const AFontID: String): Boolean;
begin
  Result := FPCLFonts.IndexOf(AFontID) <> -1;
end;


function TrwPrinter.PCLStoreFont(const AData: string): String;
begin
  if FPrintPCLasGraphic then
    Exit;

  Result := Copy(AData, 4, 5);
  PCLPrintString(AData);
  FPCLFonts.Add(Result);
end;


procedure TrwPrinter.PCLPrintPCLText(const AFontID, AInitStr, AText: String; const APos: TPoint);
begin
  if FPrintPCLasGraphic then
    Exit;

  PCLPrintString(PCL_SELECT_PRIMARY_FONT(AFontId));
  if AInitStr <> '' then
    PCLPrintString(AInitStr);
  PCLPrintPosString(AText, APos);
end;

procedure TrwPrinter.PCLPrintPosString(const AData: string; const APos: TPoint);
begin
  PCLPrintString(#27'*p' + IntToStr(APos.X) + 'X');
  PCLPrintString(#27'*p' + IntToStr(APos.Y) + 'Y');
  PCLPrintString(AData);
end;


function TrwPrinter.GetPrintPCLasGraphic: Boolean;
begin
//  Result := FPrintPCLasGraphic;
  Result := True;  // always true, since PCL printing SUCKS!
end;


procedure TrwPrinter.SetPrintPCLasGraphic(const Value: Boolean);
begin
  CheckPrinting(False);
  if Value then
    FPrintPCLasGraphic := True
  else
    FPrintPCLasGraphic := not FPCLDriver;
end;

function TrwPrinter.GetTrays: TStrings;
begin
  Result := FTrays;
end;


procedure TrwPrinter.EndPage;
begin
  Windows.EndPage(Handle);
end;


function TrwPrinter.OriginalDevMode: DEVMODE;
begin
  Result := FOriginalDevMode;
end;


procedure TrwPrinter.RestorePrinter;
var
  Device: array[0..cBufSize] of char;
  Driver: array[0..cBufSize] of char;
  Port: array[0..cBufSize] of char;
  hDeviceMode: THandle;
begin
  rwPrinter.GetPrinter(Device, Driver, Port, hDeviceMode);
  if String(Device) = rwPrinter.Printers[rwPrinter.PrinterIndex] then
    rwPrinter.SetPrinter(Device, Driver, Port, hDeviceMode);
end;


function TrwPrinter.PrintJobInfo: PTrwPrintJobInfoRec;
begin
  Result := @FPrintJobInfo;
end;


{ TrwPrintDialog }

constructor TrwPrintDialog.Create(AOwner: TComponent);
begin
  inherited;

  Options := [poPageNums];

  FExtendedPanel := TPanel.Create(self);
  with FExtendedPanel do
  begin
    Caption := '';
    SetBounds(0, 0, 169, 200);
    BevelOuter := bvNone;
    BorderWidth := 6;
    FCheckBox := TCheckBox.Create(self);
    with FCheckBox do
    begin
      Caption := 'Print PCL as Graphics';
      SetBounds( 2, 7, 180, 28 );
      Parent := FExtendedPanel;
    end;
  end;
end;


destructor TrwPrintDialog.Destroy;
begin
  FExtendedPanel.Free;
  inherited;
end;


procedure TrwPrintDialog.DoShow;
var
  PreviewRect, StaticRect: TRect;
begin
  { Set preview area to entire dialog }
  GetClientRect(Handle, PreviewRect);
  StaticRect := GetStaticRect;
  { Move extended area to right of static area }
  PreviewRect.Left := StaticRect.Left;
  PreviewRect.Top  := StaticRect.Bottom;
  Inc(PreviewRect.Top, 4);
  fExtendedPanel.BoundsRect := PreviewRect;
//  fExtendedPanel.ParentWindow := Handle;
  FCheckBox.Caption := 'Print PCL as Graphics';

  inherited DoShow;
end;


function TrwPrintDialog.Execute: Boolean;
var
  Device: array[0..cBufSize] of char;
  Driver: array[0..cBufSize] of char;
  Port: array[0..cBufSize] of char;
  hDeviceMode: THandle;
begin
  Result := False;
  rwPrinter.GetPrinter(Device, Driver, Port, hDeviceMode);
  Printer.SetPrinter(Device, Driver, Port, hDeviceMode);

  if (rwPrinter.PrintJobInfo.FirstPage = 0) and (rwPrinter.PrintJobInfo.LastPage = 0) then
    PrintRange := prAllPages
  else
  begin
    PrintRange := prPageNums;
    FromPage := rwPrinter.PrintJobInfo.FirstPage;
    ToPage := rwPrinter.PrintJobInfo.LastPage;
  end;
  Copies := rwPrinter.PrintJobInfo.Copies;
  PrintPCLAsGraphics := rwPrinter.PrintJobInfo.PrintPCLAsGraphic;

  try
    Result := inherited Execute;
  finally
    if not Result then
      FCheckBox.Checked := rwPrinter.PrintJobInfo.PrintPCLAsGraphic

    else
    begin
      Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
      rwPrinter.SetPrinter(Device, Driver, Port, hDeviceMode);
      
      if rwPrinter.PrinterIndex = -1 then
        rwPrinter.PrintJobInfo.PrinterName := ''
      else
        rwPrinter.PrintJobInfo.PrinterName := rwPrinter.Printers[rwPrinter.PrinterIndex];

      if PrintRange = prAllPages then
      begin
        rwPrinter.PrintJobInfo.FirstPage := 0;
        rwPrinter.PrintJobInfo.LastPage := 0;
      end
      else
      begin
        rwPrinter.PrintJobInfo.FirstPage := FromPage;
        rwPrinter.PrintJobInfo.LastPage := ToPage;
      end;
      rwPrinter.PrintJobInfo.Copies := Copies;
      rwPrinter.PrintJobInfo.PrintPCLAsGraphic := PrintPCLAsGraphics;
    end;
  end;
end;


function TrwPrintDialog.GetPrintPCLAsGraphics: Boolean;
begin
  Result := FCheckBox.Checked;
end;


procedure TrwPrintDialog.SetPrintPCLAsGraphics(const Value: Boolean);
begin
  FCheckBox.Checked := Value;
end;


function TrwPrintDialog.GetStaticRect: TRect;
begin
  if Handle <> 0 then
  begin
    GetWindowRect(GetDlgItem(Handle, grp1), Result); // print range group box
    MapWindowPoints(0, Handle, Result, 2);
  end
  else
    Result := Rect(0,0,0,0)
end;


function TrwPrintDialog.TaskModalDialog(DialogFunc: Pointer; var DialogData): Bool;
begin
  TPrintDlg(DialogData).Flags := TPrintDlg(DialogData).Flags or PD_ENABLESETUPTEMPLATE;
  TPrintDlg(DialogData).lpSetupTemplateName := Template;
  result := inherited TaskModalDialog(DialogFunc, DialogData);
end;



procedure TrwDummyPrinter.SetState(Value: TrwPrinterState);
begin
  if Value <> State then
  begin
    case Value of
      psNoHandle:
        begin
          if Assigned(FCanvas) then
            FCanvas.Handle := 0;
          ReleaseDC(0, DC);
          DC := 0;
        end;

      psHandleIC,
      psHandleDC:
        begin
          if FCanvas <> nil then
            FCanvas.Handle := 0;

          if DC <> 0 then
            ReleaseDC(0, DC);

          DC := GetDC(0);
          if FCanvas <> nil then
            FCanvas.Handle := DC;
        end;
    end;

    State := Value;
  end;
end;


procedure TrwDummyPrinter.SetToDefaulTrwPrinter;
begin
end;


procedure TrwDummyPrinter.SetPrinterIndex(Value: Integer);
begin
 FPrinterIndex := -1;
end;

procedure TrwDummyPrinter.SetOrientation(Value: TrwPrinterOrientation);
begin
end;


initialization

finalization
  FPrinter.Free;
end.
