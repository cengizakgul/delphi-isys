unit rmPrintFormControls;

interface

uses Classes, Windows, StdCtrls, Graphics, SysUtils, Variants, Controls,
     Messages, Math, Forms,
     rwGraphics, rmCommonClasses, rwUtils, rwTypes, rwBasicClasses,
     evStreamUtils, rmTypes, rwEngineTypes, rmFormula;

const
  cLineLiteGrayColor = $00F9F9F9;
  cLineGrayColor = $00F3F3F3;
  cRWAMpage = '<page>';

type
  TrmRWARenderingStatus = (rmRWARSNone, rmRWARSCalculating, rmRWARSCalculated, rmRWARSPrinting, rmRWARSPrinted);
  TrmRWARenderingStatuses = set of TrmRWARenderingStatus;
  TrmRWACuttingState = (rmRWACSNone, rmRWACSTop, rmRWACSMiddle, rmRWACSBottom);

  TrmPrintContainer = class;
  TrmPrintForm = class;

  TrmPrintControl = class(TrmGraphicControl)
  private
    FContainer: TrmPrintContainer;
    FAlignDisableCount: Integer;
    FCompare: Boolean;
    FLayerNumber: TrwLayerNumber;
    FPrintOnEachPage: Boolean;
    FEnabled: Boolean;
    FCutable: Boolean;
    FBlockParentIfEmpty: Boolean;

    function  StoreContainer: Boolean;
  protected
    FInnerMarginLeft: Integer;
    FInnerMarginRight: Integer;
    FInnerMarginTop: Integer;
    FInnerMarginBottom: Integer;

    procedure RegisterComponentEvents; override;
    procedure SetContainer(Value: TrmPrintContainer); virtual;
    procedure SetVisible(Value: Boolean); override;
    procedure SetParentComponent(Value: TComponent); override;
    procedure SetPrintOnEachPage(const Value: Boolean); virtual;
    procedure SetBlockParentIfEmpty(const Value: Boolean); virtual;
    procedure SetAlign(Value: TAlign); override;
    procedure SetInnerMarginBottom(const Value: Integer); virtual;
    procedure SetInnerMarginLeft(const Value: Integer); virtual;
    procedure SetInnerMarginRight(const Value: Integer); virtual;
    procedure SetInnerMarginTop(const Value: Integer); virtual;
    procedure DoAlign; virtual;
    procedure Loaded; override;
    function  GetDsgnLockState: TrwDsgnLockState; override;
    function  GetClientRect: TRect; override;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure AdjustClientRect(var Rect: TRect); virtual;
    procedure UnAdjustClientRect(var Rect: TRect); virtual;
    procedure ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState); override;
    function  MouseResizingThreshold: Integer; override;
    function  ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
    function  ZoomedClientRect(ACanvas: TrwCanvas): TRect;
    function  ZoomedClientPoint(ACanvas: TrwCanvas; APoint: TPoint): TPoint;
    procedure Invalidate; override;
    procedure InvalidateRect(const ARect: TRect); override;
    function  PrintForm: TrmPrintForm; virtual;

  //Interface
    function  GetWinControlParent: TWinControl; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  GetParentExtRect: TRect; override;
    function  GetParent: IrmDesignObject; override;
    procedure SetParent(AParent: IrmDesignObject); override;
    function  ObjectType: TrwDsgnObjectType; override;
    function  ClientToScreen(APos: TPoint): TPoint; override;
    function  ScreenToClient(APos: TPoint): TPoint; override;
  //end

  // rendering
  private
    FRendStatus: TrmRWARenderingStatus;
    FCalculatedBounds: TRect;
    FCutState: TrmRWACuttingState;
    FPrintAccepted: Boolean;
    FPrintRectOnCurrPage: TRect;
    FRendText: String;
    FRendTextHeight: Integer;
    FHeightBorderAmendmend: Integer;
    function  FormulaBlockBeforeCalc: TrmFMLFormula;
    function  FormulaBlockAfterCalc: TrmFMLFormula;
  protected
    FTopCutPosition: Integer;
    FBottomCutPosition: Integer;
    procedure SetRendStatus(const Value: TrmRWARenderingStatus); virtual;
    procedure OnPageCalculated; virtual;                          // Notification from PrintForm
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); virtual; // Routines for storing calculated objects into external stream (functionality of TrmDBTable mostly)
    procedure StorePrintState(const AStream: IEvDualStream); virtual;
    procedure StoreBounds(const AStream: IEvDualStream);
    procedure StoreRendInfo(const AStream: IEvDualStream);
    procedure StoreFont(const AStream: IEvDualStream; const AFont: TrwFont);
    procedure StoreBorderLines(const AStream: IEvDualStream; const BorderLines: TrwBorderLines);
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); virtual;
    procedure RestorePrintState(const AStream: IEvDualStream); virtual;
    procedure ReStoreBounds(const AStream: IEvDualStream);
    procedure ReStoreRendInfo(const AStream: IEvDualStream);
    procedure ReStoreFont(const AStream: IEvDualStream; const AFont: TrwFont);
    procedure ReStoreBorderLines(const AStream: IEvDualStream; const BorderLines: TrwBorderLines);
    function  GetTopClientAmendment: Integer; virtual;    // inner border
    function  GetBottomClientAmendment: Integer; virtual;
    function  GetLeftClientAmendment: Integer; virtual;
    function  GetRightClientAmendment: Integer; virtual;
    function  GetEachPageHeaderHeight: Integer; virtual;  // amendment for inner border
    function  GetEachPageFooterHeight: Integer; virtual;
    function  PreCut: Integer; virtual;  // Performs Pre-cut and returns amendment for cut position
    function  PreCutAmendment: Integer; virtual; // Returns amendment for cut position (only negative value is valid)
    function  CutThreshold: Integer; virtual;

  public
    property    RendText: String read FRendText write FRendText;                        // If control has Text property it stores the rest of Text to be printed
    property    RendTextHeight: Integer read FRendTextHeight write FRendTextHeight;     // Height of the Text in logical pixels
    property    CalculatedBounds: TRect read FCalculatedBounds write FCalculatedBounds; // Stored values of BoundsRect after calculations
    property    RendStatus: TrmRWARenderingStatus read FRendStatus write SetRendStatus;
    property    CutState: TrmRWACuttingState read FCutState;                             // Current cutting part (top, middle, bottom)
    property    PrintRectOnCurrPage: TRect read FPrintRectOnCurrPage;                   // Fragment of the control to be printed on this page (client area) converted to Page's coordinates

    function    Renderer: IrmRWARenderEngine; virtual;
    procedure   Calculate;
    procedure   CalcSetPropFormulas;
    function    BeforeCalculate: Boolean; virtual;
    procedure   DoCalculate; virtual;
    function    AfterCalculate: Boolean; virtual;

    procedure   BeforePrint; virtual;
    procedure   Print;
    procedure   DoPrint; virtual;
    procedure   AfterPrint;
    procedure   CalcCutting; virtual;
    procedure   CalcPositionOnCurrentPage;
    procedure   CreateRWAObject; virtual; abstract;
    procedure   PrepareForNextPrintCycle; virtual;
    procedure   StoreToStream(AStream: IEvDualStream; AStorePrintState: Boolean); virtual;
    procedure   ReStoreFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); virtual;
  // end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   AfterChangePos; override;
    procedure   DisableAlign;
    procedure   EnableAlign;
    function    AlignDisabled: Boolean;
    function    ClientToForm(APoint: TPoint): TPoint; overload;
    function    FormToClient(APoint: TPoint): TPoint; overload;
    function    ClientToForm(ARect: TRect): TRect; overload;
    function    FormToClient(ARect: TRect): TRect; overload;
    function    CanvasDPI: Integer; override;

    property    InnerMarginLeft: Integer read FInnerMarginLeft write SetInnerMarginLeft;
    property    InnerMarginRight: Integer read FInnerMarginRight write SetInnerMarginRight;
    property    InnerMarginTop: Integer read FInnerMarginTop write SetInnerMarginTop;
    property    InnerMarginBottom: Integer read FInnerMarginBottom write SetInnerMarginBottom;

    property    LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber; // will need to redesign this functionality
    property    Compare: Boolean read FCompare write FCompare;

  published
    property  Container: TrmPrintContainer read FContainer write SetContainer stored StoreContainer;
    property  PrintOnEachPage: Boolean read FPrintOnEachPage write SetPrintOnEachPage;
    property  Enabled: Boolean read FEnabled write FEnabled;
    property  Cutable: Boolean read FCutable write FCutable;
    property  BlockParentIfEmpty: Boolean read FBlockParentIfEmpty write SetBlockParentIfEmpty;    
  end;


  TrmPrintContainer = class(TrmPrintControl)
  private
    FControls: TList;
    FMultiPaged: Boolean;

    function  GetControl(Index: Integer): TrmPrintControl;

  protected
    procedure DoAlign; override;
    procedure AlignChildren; virtual;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure Loaded; override;
    procedure DestroyChildren;
    procedure PaintBackground(ACanvas: TrwCanvas); virtual;
    procedure PaintChildren(ACanvas: TrwCanvas); virtual;
    procedure PaintForeground(ACanvas: TrwCanvas); virtual;
    procedure Notify(AEvent: TrmDesignEventType); override;
    procedure SetMultiPaged(const Value: Boolean); virtual;
    procedure SetPrintOnEachPage(const Value: Boolean); override;

    //Interface
    function  GetControlCount: Integer; override;
    function  GetControlByIndex(AIndex: Integer): IrmDesignObject; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   PaintToCanvas(ACanvas: TrwCanvas); override;
    procedure   RemoveControl(AControl: TrmPrintControl);
    function    InsertControl(AControl: TrmPrintControl): Integer;
    function    ControlAtPos(APos: TPoint): TrwComponent; override;
    procedure   AfterChangePos; override;

  //rendering
  protected
    FTopCutClientPosition: Integer;
    FBottomCutClientPosition: Integer;
    FTopCutClPosNoHeaders: Integer;
    FBottomCutClPosNoHeaders: Integer;

    procedure OnPageCalculated; override;
    procedure StorePrintState(const AStream: IEvDualStream); override;
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure StoreChildren(const AStream: IEvDualStream; AStorePrintState: Boolean); virtual;
    procedure ReStorePrintState(const AStream: IEvDualStream); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
    procedure ReStoreChildren(const AStream: IEvDualStream; ARestorePrintState: Boolean); virtual;
    function  PreCutChild(const AControl: TrmPrintControl): Boolean;
    function  PreCutChildren: Boolean; virtual;
    function  PreCutAmendment: Integer; override;
    function  PreCut: Integer; override;
    function  GetEachPageHeaderHeight: Integer; override;  // amendment for inner border of MultiPaged containers
    function  GetEachPageFooterHeight: Integer; override;
    procedure ApplyPrintAmendment(const AValue: Integer; const ARendStatus: TrmRWARenderingStatuses);
  public
    procedure  SetRendStatusForChildren(AStatus: TrmRWARenderingStatus);
    procedure  SetRendStatusChildrenToo(AStatus: TrmRWARenderingStatus);
    procedure  DoCalculate; override;
    function   AfterCalculate: Boolean; override;

    procedure  DoPrint; override;
    procedure  CalcCutting; override;
    procedure  PrepareForNextPrintCycle; override;
    procedure  StoreToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure  ReStoreFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
  //end

  public
    property Controls[Index: Integer]: TrmPrintControl read GetControl;
    property ControlCount: Integer read GetControlCount;

  published
    property Color;
    property MultiPaged: Boolean read FMultiPaged write SetMultiPaged;

    function  PControlCount: Variant;
    function  PFindControl(AName: Variant): Variant;
    function  PControlByIndex(AIndex: Variant): Variant;
    function  PControlIndexByName(AName: Variant): Variant;
  end;


  TrmPrintForm = class(TrmPrintContainer)
  private
    FPaperOrientation: TrwPaperOrientation;
    FTopMargin: Integer;
    FBottomMargin: Integer;
    FLeftMargin: Integer;
    FRightMargin: Integer;
    FDuplexing: Boolean;
    FGridBrush: TBitmap;

    procedure SetPaperOrientation(Value: TrwPaperOrientation);
    procedure SetMargin(Index: Integer; Value: Integer);
    procedure OffsetControlsVert(AOffset: Integer);
    procedure OffsetControlsHor(AOffset: Integer);

  protected
    procedure SetPrintOnEachPage(const Value: Boolean); override;
    procedure SetParentComponent(Value: TComponent); override;
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure PaintBackground(ACanvas: TrwCanvas); override;
    procedure PaintForeground(ACanvas: TrwCanvas); override;
    procedure AdjustClientRect(var Rect: TRect); override;
    procedure UnAdjustClientRect(var Rect: TRect); override;
    procedure Invalidate; override;
    function  PrintForm: TrmPrintForm; override;

  //interface
    procedure Notify(AEvent: TrmDesignEventType); override;
    procedure SetDesigner(ADesigner: IrmDesigner); override;

  //rendering
  protected
    function  GetTopClientAmendment: Integer; override;
    function  GetBottomClientAmendment: Integer; override;
    function  GetLeftClientAmendment: Integer; override;
    function  GetRightClientAmendment: Integer; override;

  public
    procedure CalcCutting; override;
    function  Renderer: IrmRWARenderEngine; override;
    procedure DoCalculate; override;
    function  AfterCalculate: Boolean; override;
    procedure BeforePrint; override;
    procedure DoPrint; override;
    procedure CreateRWAObject; override;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CompileChildren(ASyntaxCheck: Boolean = False); override;
    procedure PrepareGridBrush;
    procedure AfterChangePos; override;
    function  GetPaperSize: TrwPaperSize;

  published
    property Left stored False;
    property Top stored False;
    property PrintOnEachPage stored False;

    property PaperOrientation: TrwPaperOrientation read FPaperOrientation write SetPaperOrientation;
    property Duplexing: Boolean read FDuplexing write FDuplexing;
    property TopMargin: Integer index 0 read FTopMargin write SetMargin;
    property BottomMargin: Integer index 1 read FBottomMargin write SetMargin;
    property LeftMargin: Integer index 2 read FLeftMargin write SetMargin;
    property RightMargin: Integer index 3 read FRightMargin write SetMargin;
  end;



  TrmPrintCustomText = class(TrmPrintControl)
  private
    FAlignment: TAlignment;
    FAutosize: Boolean;
    FFont: TrwFont;
    FWordWrap: Boolean;
    FLayout: TTextLayout;

    procedure SetFont(Value: TrwFont);
    procedure SetAlignment(Value: TAlignment);
    procedure SetAutosize(Value: Boolean);
    procedure SetLayout(Value: TTextLayout);
    procedure SetWordWrap(Value: Boolean);
    procedure OnChangeFont(Sender: TObject);

  protected
    procedure Loaded; override;
    procedure CheckAutosize;
    procedure SetDesigner(ADesigner: IrmDesigner); override;
    function  GetTextForDraw: String; virtual;
    function  GetTextForPrint: string; virtual;
    procedure PaintControl(ACanvas: TrwCanvas); override;

    property Text: string read GetTextForPrint;
    property Alignment: TAlignment read FAlignment write SetAlignment;
    property Autosize: Boolean read FAutoSize write SetAutosize;
    property Font: TrwFont read FFont write SetFont;
    property Layout: TTextLayout read FLayout write SetLayout;
    property WordWrap: Boolean read FWordWrap write SetWordWrap;

 //rendering
  private
    procedure PrepareTextForPrinting;
  protected
    procedure SetRendStatus(const Value: TrmRWARenderingStatus); override;
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
    function  PreCutAmendment: Integer; override;
  public
    procedure CreateRWAObject; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;


  TrmPrintText = class(TrmPrintCustomText, IrmQueryAwareObject)
  private
    FValueHolder: TrmsValueHolder;
    FFormat: string;

    procedure SetFormat(const Value: string);
    procedure SetValue(const AValue: Variant);
    function  GetValue: Variant;
    function  CanWriteValue: Boolean;
    function  FormulaValue: TrmFMLFormula;
    function  FormulaTextFilter: TrmFMLFormula;
    function  GetDataField: String;
    procedure SetDataField(const AValue: String);

  // Interface
  protected
    function  GetFieldName: String;
    procedure SetFieldName(const AFieldName: String);
    function  GetDataFieldInfo: TrmDataFieldInfoRec;
    procedure SetValueFromDataSource;
  // End

  protected
    procedure Loaded; override;
    function  GetTextForDraw: String; override;
    function  GetTextForPrint: string; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   InitializeForDesign; override;

  //rendering
  protected
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;

  published
    property Text;
    property Align;
    property Color;
    property Alignment;
    property Autosize;
    property Font;
    property Layout;
    property Format: string read FFormat write SetFormat;
    property Value: Variant read GetValue write SetValue stored CanWriteValue;
    property DataField: String read GetDataField write SetDataField;
    property WordWrap;
  end;


  TrmPrintPanel = class(TrmPrintContainer)
  public
    procedure CreateRWAObject; override;
  published
    property  Align;  
  end;


  TrmPrintImage = class(TrmPrintControl)
  private
    FStretch: Boolean;
    FPicture: TrwPicture;
    procedure SetPicture(const Value: TrwPicture);
    procedure SetStretch(const Value: Boolean);
    procedure SetTransparent(const Value: Boolean);
    procedure CheckSize;
    procedure OnChangePicture(Sender: TObject);
    function  GetTransparent: Boolean;

  protected
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    //rendering
  protected
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
    function  CutThreshold: Integer; override;
  public
    procedure CreateRWAObject; override;

  published
    property Picture: TrwPicture read FPicture write SetPicture;
    property Stretch: Boolean read FStretch write SetStretch;
    property Transparent: Boolean read GetTransparent write SetTransparent;
  end;


 TrmPrintPageNumber = class(TrmPrintCustomText)
  private
    FStyleFormat: String;
    procedure SetStyleFormat(const AValue: String);

  protected
    function  GetTextForDraw: String; override;
    function  GetTextForPrint: string; override;

  //rendering
  protected
    procedure StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean); override;
    procedure ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean); override;
  public
    procedure CreateRWAObject; override;
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Color;
    property Alignment;
    property Font;
    property Layout;
    property StyleFormat: String read FStyleFormat write SetStyleFormat;
  end;


// Design-time object

  TrmdPrintForm = class(TrmdCustomGraphicWorkSpace)
  private
    FUpdating: Boolean;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
  protected
    procedure PropChanged(APropType: TrwChangedPropID); override;
    procedure UpdateSize; override;
    function  DispatchMessage(var Message: TMessage): Boolean; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses rmReport, rwDsgnControls, Types, rwCommonClasses;


{TrmPrintControl}

constructor TrmPrintControl.Create(AOwner: TComponent);
begin
  inherited;
  FCompare := True;
  PrintOnEachPage := False;
  Enabled := True;
  Cutable := True;
end;

destructor TrmPrintControl.Destroy;
begin
  BeforeDestruction;
  Container := nil;
  inherited;
end;

procedure TrmPrintControl.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventBeforeCalculate, cEventOnCalculate, cEventAfterCalculate]);
end;

procedure TrmPrintControl.SetContainer(Value: TrmPrintContainer);
begin
  if (FContainer = Value) then Exit;

  if Assigned(FContainer) then
    FContainer.RemoveControl(Self);

  FContainer := Value;

  if Assigned(FContainer) then
    FContainer.InsertControl(Self);
end;

procedure TrmPrintControl.SetParentComponent(Value: TComponent);
begin
  Container := TrmPrintContainer(Value);
end;


procedure TrmPrintControl.Loaded;
begin
  if not Assigned(Container) and (Owner is TrmPrintContainer) then
    Container := TrmPrintContainer(Owner);

  inherited;
end;

function TrmPrintControl.StoreContainer: Boolean;
begin
  Result := FWritingOnlySelf;
end;

function TrmPrintControl.ClientToForm(APoint: TPoint): TPoint;
var
  C: TrmPrintContainer;
begin
  Result := APoint;
  C := Container;

  if Assigned(C) then
  begin
    Inc(Result.X, Left);
    Inc(Result.Y, Top);
  end;

  while Assigned(C) and Assigned(C.Container) do
  begin
    Inc(Result.X, C.Left);
    Inc(Result.Y, C.Top);
    C := C.Container;
  end;
end;

function TrmPrintControl.FormToClient(APoint: TPoint): TPoint;
var
  C: TrmPrintContainer;
begin
  Result := APoint;
  C := Container;

  if Assigned(C) then
  begin
    Dec(Result.X, Left);
    Dec(Result.Y, Top);
  end;

  while Assigned(C) and Assigned(C.Container) do
  begin
    Dec(Result.X, C.Left);
    Dec(Result.Y, C.Top);
    C := C.Container;
  end;
end;

function TrmPrintControl.GetDsgnLockState: TrwDsgnLockState;
begin
  if Assigned(Container) and (Container.DsgnLockState <> rwLSUnlocked) then
    Result := Container.DsgnLockState
  else
    Result :=  inherited GetDsgnLockState;
end;

function TrmPrintControl.GetWinControlParent: TWinControl;
var
  P: TrmPrintContainer;
begin
  P := PrintForm;
  if Assigned(P) and P.VisualControlCreated then
    Result := P.VisualControl.Container
  else
    Result := nil;
end;

procedure TrmPrintControl.Notify(AEvent: TrmDesignEventType);
begin
  inherited;

  if Assigned(Container) then
    if AEvent in [rmdSelectObject, rmdSelectChildObject] then
      Container.Notify(rmdSelectChildObject)
    else if AEvent in [rmdDestroyResizer, rmdDestroyChildResizer] then
      Container.Notify(rmdDestroyChildResizer);
end;

function TrmPrintControl.GetParentExtRect: TRect;
var
  F: TrmPrintForm;
begin
  F := PrintForm;
  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := ClientToForm(Result.TopLeft);
  Result.BottomRight := ClientToForm(Result.BottomRight);
  OffsetRect(Result, F.Left, F.Top);
  Result := Zoomed(Result);
end;

function TrmPrintControl.GetParent: IrmDesignObject;
begin
  Result := Container;
end;

function TrmPrintControl.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTPrintForm;
end;

function TrmPrintControl.ClientToScreen(APos: TPoint): TPoint;
var
  F: TrmPrintForm;
begin
  F := PrintForm;
  Result := ClientToForm(APos);
  Inc(Result.X, F.Left);
  Inc(Result.Y, F.Top);
  Result := TWinControl(F.VisualControl.RealObject).ClientToScreen(Zoomed(Result));
end;

function TrmPrintControl.ScreenToClient(APos: TPoint): TPoint;
var
  F: TrmPrintForm;
begin
  F := PrintForm;
  Result := UnZoomed(TWinControl(F.VisualControl.RealObject).ScreenToClient(APos));
  Dec(Result.X, F.Left);
  Dec(Result.Y, F.Top);
  Result := FormToClient(Result);
end;

procedure TrmPrintControl.AfterChangePos;
begin
  if Align <> alNone then
    DoAlign;
  inherited;
end;

function TrmPrintControl.GetClientRect: TRect;
begin
  Result := inherited GetClientRect;
  AdjustClientRect(Result);
end;

procedure TrmPrintControl.AdjustClientRect(var Rect: TRect);
begin
  Inc(Rect.Top, FInnerMarginTop);
  Inc(Rect.Left, FInnerMarginLeft);
  Dec(Rect.Bottom, FInnerMarginBottom);
  Dec(Rect.Right, FInnerMarginRight);
end;

procedure TrmPrintControl.UnAdjustClientRect(var Rect: TRect);
begin
  Dec(Rect.Top, FInnerMarginTop);
  Dec(Rect.Left, FInnerMarginLeft);
  Inc(Rect.Bottom, FInnerMarginBottom);
  Inc(Rect.Right, FInnerMarginRight);
end;

procedure TrmPrintControl.ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState);
begin
  SetBoundsRect(NewBounds);
end;

function TrmPrintControl.BeforeCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockBeforeCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('BeforeCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
    RendStatus := rmRWARSCalculating
  else
    RendStatus := rmRWARSPrinted;
end;

procedure TrmPrintControl.Calculate;
begin
  if Enabled and (RendStatus = rmRWARSNone) then
  begin
    if BeforeCalculate then
    begin
      DoCalculate;
      AfterCalculate;
    end;
  end;
end;

procedure TrmPrintControl.AfterPrint;
begin
//  ExecuteEventsHandler('AfterPrint', []);
end;

procedure TrmPrintControl.Print;
begin
  BeforePrint;

  if FPrintAccepted and (RendStatus in [rmRWARSCalculated, rmRWARSPrinting]) then
  begin
    DoPrint;
    AfterPrint;
  end;
end;

procedure TrmPrintControl.BeforePrint;
begin
  FPrintAccepted := Visible and Assigned(Container) and Container.FPrintAccepted and ((CalculatedBounds.Top < Container.FBottomCutClientPosition) or PrintOnEachPage);

  if not Visible then
    RendStatus := rmRWARSPrinted;
end;

function TrmPrintControl.Renderer: IrmRWARenderEngine;
var
  C: TrmPrintContainer;
begin
  Result := nil;
  C := Container;
  while Assigned(C) and not (C is TrmPrintForm) do
    C := C.Container;

  if Assigned(C) then
    Result := TrmPrintForm(C).Renderer;
end;

procedure TrmPrintControl.DoPrint;
begin
  CalcCutting;
  CalcPositionOnCurrentPage;
  CreateRWAObject;
  PrepareForNextPrintCycle;
end;

function TrmPrintControl.ClientToForm(ARect: TRect): TRect;
begin
  Result.TopLeft := ClientToForm(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmPrintControl.FormToClient(ARect: TRect): TRect;
begin
  Result.TopLeft := FormToClient(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmPrintControl.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
var
  R: TRect;
begin
  Result := inherited CheckSizeConstrains(ALeft, ATop, AWidth, AHeight);

  if AWidth < (GetLeftClientAmendment - GetRightClientAmendment) then
    AWidth := GetLeftClientAmendment - GetRightClientAmendment;

  if AHeight < GetTopClientAmendment - GetBottomClientAmendment then
    AHeight := GetTopClientAmendment - GetBottomClientAmendment;

  if Assigned(Container) then
  begin
    R := Container.ClientRect;
    if ALeft < R.Left then
      ALeft := R.Left;
    if ATop < R.Top then
      ATop := R.Top;
  end;
end;


procedure TrmPrintControl.SetRendStatus(const Value: TrmRWARenderingStatus);
begin
  if Enabled then
  begin
    FRendStatus := Value;
    if FRendStatus in [rmRWARSNone, rmRWARSCalculated]  then
    begin
      FCutState := rmRWACSNone;
      FTopCutPosition := 0;
      FBottomCutPosition := 0;
    end;
  end

  else
    FRendStatus := rmRWARSNone;

  if FRendStatus = rmRWARSNone then
    EmptyAggregateFunctions; 
end;


procedure TrmPrintControl.CalcPositionOnCurrentPage;
var
  Cont: TrmPrintContainer;
begin
  FPrintRectOnCurrPage := Rect(CalculatedBounds.Left, FTopCutPosition, CalculatedBounds.Right, FBottomCutPosition);

  Cont := Container;

  while not (Cont is TrmPrintForm) do
  begin
    OffsetRect(FPrintRectOnCurrPage, Cont.CalculatedBounds.Left, Cont.CalculatedBounds.Top);
    Cont := Cont.Container;
  end;
end;

procedure TrmPrintControl.PrepareForNextPrintCycle;
begin
  if FCutState in [rmRWACSNone, rmRWACSBottom] then
    RendStatus := rmRWARSPrinted
  else
  begin
    RendStatus := rmRWARSPrinting;

    // calculate inner border
    FHeightBorderAmendmend := GetTopClientAmendment - GetBottomClientAmendment;
    Inc(FCalculatedBounds.Bottom, FHeightBorderAmendmend);
  end;
end;

procedure TrmPrintControl.CalcCutting;
begin
  case FCutState of
    rmRWACSNone:         // beginning of printing process
      begin
        FTopCutPosition := CalculatedBounds.Top;
        if (CalculatedBounds.Bottom > Container.FBottomCutClientPosition) and not PrintOnEachPage then // Control doesn't fit
        begin
          // top part of the control
          FCutState := rmRWACSTop;
          FBottomCutPosition := Container.FBottomCutClientPosition;
        end
        else
          // whole control fits into container
          FBottomCutPosition := CalculatedBounds.Bottom;
      end;

    rmRWACSTop, rmRWACSMiddle:     // continue of printing process
      begin
        FTopCutPosition := Container.FTopCutClientPosition;
        if CalculatedBounds.Bottom <= Container.FBottomCutClientPosition then
        begin
          // bottom part of the control
          FBottomCutPosition := CalculatedBounds.Bottom;
          FCutState := rmRWACSBottom;
        end
        else
        begin
          // middle part of the control
          FBottomCutPosition := Container.FBottomCutClientPosition;
          FCutState := rmRWACSMiddle;
        end;
      end;
  end;
end;

procedure TrmPrintControl.StoreBorderLines(const AStream: IEvDualStream; const BorderLines: TrwBorderLines);

  procedure StoreBorderLine(const BorderLine: TrwBorderLine);
  begin
    with BorderLine do
    begin
      AStream.WriteBoolean(Visible);
      AStream.WriteInteger(Ord(Color));
      AStream.WriteInteger(Width);
      AStream.WriteInteger(Ord(Style));
    end;
  end;

begin
  StoreBorderLine(BorderLines.TopLine);
  StoreBorderLine(BorderLines.BottomLine);
  StoreBorderLine(BorderLines.LeftLine);
  StoreBorderLine(BorderLines.RightLine);
end;

procedure TrmPrintControl.StoreFont(const AStream: IEvDualStream; const AFont: TrwFont);
var
  fs: TFontStyles;
begin
  with AFont do
  begin
    AStream.WriteString(Name);
    AStream.WriteInteger(Size);
    fs := Style;
    AStream.WriteByte(PByte(@fs)^);
    AStream.WriteInteger(Ord(Color));
  end;
end;

procedure TrmPrintControl.StoreRendInfo(const AStream: IEvDualStream);
begin
  AStream.WriteString(RendText);
  AStream.WriteInteger(RendTextHeight);
end;

procedure TrmPrintControl.StoreBounds(const AStream: IEvDualStream);
begin
  AStream.WriteInteger(CalculatedBounds.Left);
  AStream.WriteInteger(CalculatedBounds.Top);
  AStream.WriteInteger(CalculatedBounds.Right);
  AStream.WriteInteger(CalculatedBounds.Bottom);
end;

procedure TrmPrintControl.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  if AStorePrintState then
    StorePrintState(AStream);
  StoreBounds(AStream);
  AStream.WriteBoolean(Visible);
end;

procedure TrmPrintControl.StoreToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  StoreSelfToStream(AStream, AStorePrintState);
end;

procedure TrmPrintControl.ReStoreBorderLines(const AStream: IEvDualStream; const BorderLines: TrwBorderLines);

  procedure ReadBorderLine(const BorderLine: TrwBorderLine);
  begin
    with BorderLine do
    begin
      Visible := AStream.ReadBoolean;
      Color := AStream.ReadInteger;
      Width := AStream.ReadInteger;
      Style := TPenStyle(AStream.ReadInteger);
    end;
  end;

begin
  ReadBorderLine(BorderLines.TopLine);
  ReadBorderLine(BorderLines.BottomLine);
  ReadBorderLine(BorderLines.LeftLine);
  ReadBorderLine(BorderLines.RightLine);
end;

procedure TrmPrintControl.ReStoreBounds(const AStream: IEvDualStream);
begin
  FCalculatedBounds.Left := AStream.ReadInteger;
  FCalculatedBounds.Top := AStream.ReadInteger;
  FCalculatedBounds.Right := AStream.ReadInteger;
  FCalculatedBounds.Bottom := AStream.ReadInteger;
end;

procedure TrmPrintControl.ReStoreFont(const AStream: IEvDualStream; const AFont: TrwFont);
var
  fs: TFontStyles;
begin
  with AFont do
  begin
    Name := AStream.ReadString;
    Size := AStream.ReadInteger;
    PByte(@fs)^ := AStream.ReadByte;
    Style := fs;
    Color := AStream.ReadInteger;
  end;
end;

procedure TrmPrintControl.ReStoreRendInfo(const AStream: IEvDualStream);
begin
  RendText := AStream.ReadString;
  RendTextHeight := AStream.ReadInteger;
end;

procedure TrmPrintControl.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  if ARestorePrintState then
    RestorePrintState(AStream);
  ReStoreBounds(AStream);
  Visible := AStream.ReadBoolean;
end;

procedure TrmPrintControl.ReStoreFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  ReStoreSelfFromStream(AStream, ARestorePrintState);
end;

procedure TrmPrintControl.DoCalculate;
begin
  CalcSetPropFormulas;
  ExecuteEventsHandler('OnCalculate', []);
end;

procedure TrmPrintControl.SetPrintOnEachPage(const Value: Boolean);
begin
  FPrintOnEachPage := Value;
end;

function TrmPrintControl.AfterCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockAfterCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('AfterCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
  begin
    RendStatus := rmRWARSCalculated;
    NotifyAggregateFunctions;
  end
  else
    RendStatus := rmRWARSPrinted;
end;

procedure TrmPrintControl.OnPageCalculated;
begin
  // At this point nothing's gonna change on the page
  CalculatedBounds := BoundsRect;
end;

procedure TrmPrintControl.SetParent(AParent: IrmDesignObject);
begin
  if Assigned(AParent) then
    Container := TrmPrintContainer(AParent.RealObject)
  else
    Container := nil; 
end;

procedure TrmPrintControl.CalcSetPropFormulas;
var
  i: Integer;
  v: Variant;
begin
  for i := 0 to Formulas.Count - 1 do
    if Formulas[i].ActionType = fatSetProperty then
    begin
      v := Formulas[i].Calculate([]);
      SetPropertyValue(Formulas[i].ActionInfo, v);
    end;
end;

function TrmPrintControl.PreCut: Integer;
var
  oldCutState: TrmRWACuttingState;
  oldTopCutPosition, oldBottomCutPosition: Integer;
begin
  if CalculatedBounds.Top >= Container.FBottomCutClientPosition then
  begin
    Result := 0;
    Exit;
  end;

  oldCutState := FCutState;
  oldTopCutPosition := FTopCutPosition;
  oldBottomCutPosition := FBottomCutPosition;
  try
    CalcCutting;
    Result := PreCutAmendment;
  finally
    FCutState := oldCutState;
    FTopCutPosition := oldTopCutPosition;
    FBottomCutPosition := oldBottomCutPosition;
  end;
end;

function TrmPrintControl.PreCutAmendment: Integer;
begin
  if (FCutState in [rmRWACSTop, rmRWACSMiddle]) and (FBottomCutPosition - FTopCutPosition < CutThreshold) then
    Result := - (FBottomCutPosition - FTopCutPosition)
  else
    Result := 0;
end;

procedure TrmPrintControl.StorePrintState(const AStream: IEvDualStream);
begin
  AStream.WriteInteger(Ord(FRendStatus));
  AStream.WriteInteger(Ord(FCutState));
  AStream.WriteInteger(FTopCutPosition);
  AStream.WriteInteger(FBottomCutPosition);
  AStream.WriteBoolean(FPrintAccepted);
  AStream.WriteInteger(FPrintRectOnCurrPage.Left);
  AStream.WriteInteger(FPrintRectOnCurrPage.Top);
  AStream.WriteInteger(FPrintRectOnCurrPage.Right);
  AStream.WriteInteger(FPrintRectOnCurrPage.Bottom);
end;

procedure TrmPrintControl.RestorePrintState(const AStream: IEvDualStream);
begin
  FRendStatus := TrmRWARenderingStatus(AStream.ReadInteger);
  FCutState := TrmRWACuttingState(AStream.ReadInteger);
  FTopCutPosition := AStream.ReadInteger;
  FBottomCutPosition := AStream.ReadInteger;
  FPrintAccepted := AStream.ReadBoolean;
  FPrintRectOnCurrPage.Left := AStream.ReadInteger;
  FPrintRectOnCurrPage.Top := AStream.ReadInteger;
  FPrintRectOnCurrPage.Right := AStream.ReadInteger;
  FPrintRectOnCurrPage.Bottom := AStream.ReadInteger;
end;

procedure TrmPrintControl.InvalidateRect(const ARect: TRect);
var
  P: TrmdPrintForm;
  F: TrmPrintForm;
  R: TRect;
begin
  F := PrintForm;
  if Assigned(F) and F.VisualControlCreated and TWinControl(F.VisualControl.RealObject).HandleAllocated then
  begin
    P := TrmdPrintForm(F.VisualControl.RealObject);
    R := ARect;
    R.TopLeft := ClientToForm(R.TopLeft);
    R.BottomRight := ClientToForm(R.BottomRight);
    OffsetRect(R, F.Left, F.Top);
    R := Zoomed(R);
    Windows.InvalidateRect(P.Handle, @R, True);
  end;
end;

function TrmPrintControl.ZoomedClientRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
  F: TrmPrintForm;
begin
  F := PrintForm;
  GetWindowOrgEx(ACanvas.Handle, P);
  Result := ClientRect;
  Result.TopLeft := ClientToForm(Result.TopLeft);
  Result.BottomRight := ClientToForm(Result.BottomRight);
  OffsetRect(Result, F.Left, F.Top);
  Result := Zoomed(Result);
  OffsetRect(Result, P.X, P.Y);
end;

function TrmPrintControl.ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
  F: TrmPrintForm;
begin
  F := PrintForm;
  GetWindowOrgEx(ACanvas.Handle, P);
  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := ClientToForm(Result.TopLeft);
  Result.BottomRight := ClientToForm(Result.BottomRight);
  OffsetRect(Result, F.Left, F.Top);
  Result := Zoomed(Result);
  OffsetRect(Result, P.X, P.Y);
end;

procedure TrmPrintControl.SetAlign(Value: TAlign);
begin
  if Value <> Align then
  begin
    inherited;
    DoAlign;
  end;  
end;

procedure TrmPrintControl.DoAlign;
begin
  if not (csLoading in ComponentState) and not AlignDisabled and Assigned(Container) then
    Container.AlignChildren;
end;

procedure TrmPrintControl.DisableAlign;
begin
  Inc(FAlignDisableCount);
end;

procedure TrmPrintControl.EnableAlign;
begin
  if FAlignDisableCount > 0 then
    Dec(FAlignDisableCount);
end;

function TrmPrintControl.AlignDisabled: Boolean;
begin
  Result := FAlignDisableCount > 0;
end;

function TrmPrintControl.ZoomedClientPoint(ACanvas: TrwCanvas; APoint: TPoint): TPoint;
var
  F: TrmPrintForm;
  P: TPoint;
begin
  F := PrintForm;
  GetWindowOrgEx(ACanvas.Handle, P);
  Result := ClientToForm(APoint);
  Inc(Result.X, F.Left);
  Inc(Result.Y, F.Top);
  Result := Zoomed(Result);
  Inc(Result.X, P.X);
  Inc(Result.Y, P.Y);
end;

procedure TrmPrintControl.SetVisible(Value: Boolean);
var
  fl: Boolean;
begin
  fl := Value <> Visible;

  inherited;

  if fl and (Renderer <> nil) then
    DoAlign;
end;

function TrmPrintControl.GetBottomClientAmendment: Integer;
begin
  Result := - FInnerMarginBottom - GetEachPageFooterHeight;
end;

function TrmPrintControl.GetTopClientAmendment: Integer;
begin
  Result := FInnerMarginTop + GetEachPageHeaderHeight;
end;

function TrmPrintControl.CutThreshold: Integer;
begin
  if Cutable then
    Result := (GetTopClientAmendment - GetBottomClientAmendment) * 2
  else
    Result := CalculatedBounds.Bottom - CalculatedBounds.Top;
end;

function TrmPrintControl.GetLeftClientAmendment: Integer;
begin
  Result := FInnerMarginLeft;
end;

function TrmPrintControl.GetRightClientAmendment: Integer;
begin
  Result := -FInnerMarginRight;
end;

function TrmPrintControl.GetEachPageHeaderHeight: Integer;
begin
  Result := 0;
end;

function TrmPrintControl.GetEachPageFooterHeight: Integer;
begin
  Result := 0;
end;


function TrmPrintControl.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  if VisualControlCreated then
    Result := inherited DesignBehaviorInfo
  else
  begin
    Result := inherited DesignBehaviorInfo;
    Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
    Result.ResizingDirections := [rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom];
    Result.Selectable := (FMousePosition = rmdMPClient) and (DsgnLockState = rwLSUnlocked) or Result.Selectable;
  end;
end;

procedure TrmPrintControl.Invalidate;
begin
  if Assigned(Container) then
    inherited;
end;

function TrmPrintControl.MouseResizingThreshold: Integer;
begin
 Result := Round(UnZoomed(inherited MouseResizingThreshold));
end;

function TrmPrintControl.CanvasDPI: Integer;
begin
  Result := cVirtualCanvasRes;
end;

procedure TrmPrintControl.SetBlockParentIfEmpty(const Value: Boolean);
begin
  FBlockParentIfEmpty := Value;
end;

function TrmPrintControl.FormulaBlockAfterCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockAfterCalc');
end;

function TrmPrintControl.FormulaBlockBeforeCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockBeforeCalc');
end;

function TrmPrintControl.PrintForm: TrmPrintForm;
var
  P: TrmPrintContainer;
begin
  Result := nil;
  P := Container;
  while Assigned(P) do
  begin
    if P is TrmPrintForm then
    begin
      Result := TrmPrintForm(P);
      break;
    end;
    P := P.Container;
  end;
end;

procedure TrmPrintControl.SetInnerMarginBottom(const Value: Integer);
begin
  FInnerMarginBottom := Value;
end;

procedure TrmPrintControl.SetInnerMarginLeft(const Value: Integer);
begin
  FInnerMarginLeft := Value;
end;

procedure TrmPrintControl.SetInnerMarginRight(const Value: Integer);
begin
  FInnerMarginRight := Value;
end;

procedure TrmPrintControl.SetInnerMarginTop(const Value: Integer);
begin
  FInnerMarginTop := Value;
end;

{TrmPrintContainer}

constructor TrmPrintContainer.Create(AOwner: TComponent);
begin
  inherited;

  FControls := TList.Create;

  Width := Round(ConvertUnit(100, utScreenPixels, utLogicalPixels));
  Height := Round(ConvertUnit(50, utScreenPixels, utLogicalPixels));

  Color := clNone;
  FMultiPaged := False;
end;

destructor TrmPrintContainer.Destroy;
begin
  BeforeDestruction;
  DestroyChildren;
  FreeAndNil(FControls);
  inherited;
end;

procedure TrmPrintContainer.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
  C: TComponent;
begin
  if LibDesignMode or IsVirtualOwner then
    for i := 0 to ComponentCount - 1 do
    begin
      C := Components[i];
      if (C is TrmPrintControl) and (TrmPrintControl(C).Container <> Self) then
        Continue;
      Proc(C);
    end;

  for i := 0 to ControlCount - 1 do
  begin
    C := Controls[i];
    if C.Owner <> Self then
      Proc(C);
  end;
end;

function TrmPrintContainer.GetControl(Index: Integer): TrmPrintControl;
begin
  Result := TrmPrintControl(FControls[Index]);
end;

function TrmPrintContainer.GetControlCount: Integer;
begin
  Result := FControls.Count;
end;

procedure TrmPrintContainer.DestroyChildren;
begin
  while ControlCount > 0 do
    Controls[0].Free;
end;

procedure TrmPrintContainer.Loaded;
var
  i,j: Integer;
begin
  inherited;

  for i := 0 to FControls.Count-2 do
    for j := i+1 to FControls.Count-1 do
      if Controls[i].ComponentIndex > Controls[j].ComponentIndex then
        FControls.Exchange(i, j);

  AlignChildren;
end;

function TrmPrintContainer.InsertControl(AControl: TrmPrintControl): Integer;
begin
  Result := FControls.Add(AControl);
  if AControl.Align <> alNone then
    AlignChildren;

  Invalidate;
end;

procedure TrmPrintContainer.RemoveControl(AControl: TrmPrintControl);
var
  i: Integer;
begin
  if Assigned(FControls) then
  begin
    i := FControls.IndexOf(AControl);
    if i <> -1 then
    begin
      FControls.Delete(i);
      if AControl.Align <> alNone then
        AlignChildren;
    end;
  end;
  Invalidate;
end;


procedure TrmPrintContainer.PaintToCanvas(ACanvas: TrwCanvas);
var
  SaveIndex: Integer;

  procedure ExcludeNotOpaqueRegions;
//  var
//    R: TRect;
//    Rgn1, Rgn2: HRGN;

    procedure DoExclusion(AContainer: TrmPrintContainer; dx, dy: Integer);
    var
      i: Integer;
      R, Rc: TRect;
      C: TrmPrintContainer;
    begin
      Rc := AContainer.ZoomedClientRect(ACanvas);
      for i := 0 to AContainer.ControlCount - 1 do
      begin
        if AContainer.Controls[i].DoNotPaint then
          Continue;

        if AContainer.Controls[i].Color <> clNone then
        begin
          R := AContainer.Controls[i].ZoomedBoundsRect(ACanvas);
          IntersectRect(R, R, Rc);
          ExcludeClipRect(ACanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);
        end
        else if AContainer.Controls[i] is TrmPrintContainer then
        begin
          C := TrmPrintContainer(AContainer.Controls[i]);
          R := C.ZoomedBoundsRect(ACanvas);
          DoExclusion(C, 0, 0);
        end;
      end;  
    end;

  begin
    // Exclude opaque controls
    DoExclusion(Self, 0, 0);
{
    //non client area always included!
    R := ZoomedBoundsRect(ACanvas);
    Rgn1 := CreateRectRgn(0, 0, R.Right - R.Left, R.Bottom - R.Top);
    R := ZoomedClientRect(ACanvas);
    Rgn2 := CreateRectRgn(R.Left, R.Top, R.Right, R.Bottom);
    CombineRgn(Rgn1, Rgn1, Rgn2, RGN_DIFF);
    DeleteObject(Rgn2);
    ExtSelectClipRgn(ACanvas.Handle, Rgn1, RGN_OR);
    DeleteObject(Rgn1);
}
  end;

begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
    ExcludeNotOpaqueRegions;
    PaintBackground(ACanvas);
  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;

  inherited;

  PaintChildren(ACanvas);
  PaintControl(ACanvas);
  PaintForeground(ACanvas);
end;

procedure TrmPrintContainer.PaintBackground(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  if Color <> clNone then
    with ACanvas do
    begin
      Brush.Color := Color;
      Brush.Style := bsSolid;
      R := ZoomedBoundsRect(ACanvas);
      OffsetRect(R, -R.Left, -R.Top);
      FillRect(R);
    end;
end;

procedure TrmPrintContainer.PaintChildren(ACanvas: TrwCanvas);
var
  i: Integer;
  SaveIndex, SaveIndexChld: Integer;
  R: TRect;
  C: TrmPrintControl;
begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
    R := ZoomedClientRect(ACanvas);
    IntersectClipRect(ACanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];

      if C.DoNotPaint then
        Continue;

      R := C.ZoomedBoundsRect(ACanvas);
      if RectVisible(ACanvas.Handle, R) then
      begin
        SaveIndexChld := SaveDC(ACanvas.Handle);
        try
          MoveWindowOrg(ACanvas.Handle, R.Left, R.Top);
          IntersectClipRect(ACanvas.Handle, 0, 0, R.Right - R.Left, R.Bottom - R.Top);
          C.PaintToCanvas(ACanvas);
        finally
          RestoreDC(ACanvas.Handle, SaveIndexChld);
        end;
      end;
    end;
  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;
end;

procedure TrmPrintContainer.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    Pen.Width := 1;
    Pen.Mode := pmCopy;
    Pen.Color := clGray;
    Pen.Style := psDot;
    Brush.Color := clNone;
    Brush.Style := bsClear;

    R := ZoomedBoundsRect(ACanvas);
    OffsetRect(R, -R.Left, -R.Top);
    Rectangle(R);
  end;
end;

function TrmPrintContainer.ControlAtPos(APos: TPoint): TrwComponent;
var
  i: Integer;
  R: TRect;
  C, Cin: TrmPrintControl;
begin
  Cin := nil;

  for i := ControlCount - 1  downto 0 do
  begin
    C := Controls[i];

    if C.DoNotPaint then
      Continue;

    if C is TrmPrintContainer then
    begin
      R := C.BoundsRect;
      if PtInRect(R, APos) then
      begin
        Cin := C;
        Dec(APos.X, R.Left);
        Dec(APos.Y, R.Top);
        C := TrmPrintControl(TrmPrintContainer(C).ControlAtPos(APos));
        if Assigned(C) then
          Cin := C;
        break;
      end;
    end;
  end;

  if not Assigned(Cin) then
    for i := ControlCount - 1  downto 0 do
    begin
      C := Controls[i];

      if C.DoNotPaint then
        Continue;

      if (C is TrmPrintControl) and not (C is TrmPrintContainer) then
      begin
        R := C.BoundsRect;
        if PtInRect(R, APos) then
        begin
          Cin := C;
          break;
        end;
      end;
    end;

  Result := Cin;
end;


procedure TrmPrintContainer.Notify(AEvent: TrmDesignEventType);
var
  i: Integer;
begin
  inherited;
  if AEvent = rmdZoomChanged then
    for i := 0 to ControlCount - 1 do
      Controls[i].Notify(AEvent);
end;

procedure TrmPrintContainer.DoCalculate;
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    Controls[i].Calculate;

  inherited;
end;

procedure TrmPrintContainer.DoPrint;
var
  i: Integer;
begin
  CalcCutting;
  CalcPositionOnCurrentPage;
  CreateRWAObject;

  for i := 0 to ControlCount - 1 do
    Controls[i].Print;

  PrepareForNextPrintCycle;
end;



procedure TrmPrintContainer.SetRendStatusChildrenToo(AStatus: TrmRWARenderingStatus);
begin
  RendStatus := AStatus;
  SetRendStatusForChildren(AStatus);
end;


procedure TrmPrintContainer.PrepareForNextPrintCycle;
var
  i: Integer;
  flPrintingNotFinished: Boolean;
  C: TrmPrintControl;

  procedure ApplyBorderAmendment;
  var
    i, HeightAmendmend, AmdDiff: Integer;
  begin
    if FCutState in [rmRWACSNone, rmRWACSBottom] then
      Exit;

    //Search for cut children which have increased their height because of border amendmend
    HeightAmendmend := 0;
    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if C.RendStatus = rmRWARSPrinting then
        HeightAmendmend := Max(HeightAmendmend, C.FHeightBorderAmendmend);
    end;

    if HeightAmendmend <> 0 then
    begin
      Inc(FHeightBorderAmendmend, HeightAmendmend);
      Inc(FCalculatedBounds.Bottom, HeightAmendmend);

      // adjust not printed children
      for i := 0 to ControlCount - 1 do
      begin
        C := Controls[i];
        if C.RendStatus = rmRWARSCalculated then
        begin
          if C.CalculatedBounds.Top > FBottomCutClientPosition then
            OffsetRect(C.FCalculatedBounds, 0, HeightAmendmend)   // move child down
          else
            Inc(C.FCalculatedBounds.Bottom, HeightAmendmend);   // expand child's height
        end

        else if C.RendStatus = rmRWARSPrinting then
        begin
          AmdDiff := HeightAmendmend - C.FHeightBorderAmendmend;
          if AmdDiff > 0 then
          begin
            Inc(C.FHeightBorderAmendmend, AmdDiff);
            Inc(C.FCalculatedBounds.Bottom, AmdDiff);
          end;
        end;
      end;
    end;
  end;

  procedure PreparePrintOnEachPageControls(const AOffset: Integer);
  var
    i: Integer;
    R: TRect;
    C: TrmPrintControl;
  begin
    // Prepare children which have PrintOnEachPage = True
    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if C.PrintOnEachPage and (C.RendStatus = rmRWARSPrinted) then
      begin
        if AOffset <> 0 then
        begin
          R := C.CalculatedBounds;
          OffsetRect(R, 0, AOffset);
          C.CalculatedBounds := R;
        end;

        if C is TrmPrintContainer then
          TrmPrintContainer(C).SetRendStatusChildrenToo(rmRWARSCalculated)
        else
          C.RendStatus := rmRWARSCalculated;
      end;
    end;
  end;

begin
  if MultiPaged then
  begin
    // Are there not printed children?
    flPrintingNotFinished := False;
    for i := 0 to ControlCount - 1 do
      if Controls[i].RendStatus in [rmRWARSCalculated, rmRWARSPrinting] then
      begin
        flPrintingNotFinished := True;
        break;
      end;

    if flPrintingNotFinished then
    begin
      ApplyBorderAmendment;
      RendStatus := rmRWARSPrinting;  // continue printing next time
      PreparePrintOnEachPageControls(0);

      // Move not printed or not fully printed controls up on one multipage frame height (CutPos not Height!)
      ApplyPrintAmendment(-FBottomCutClientPosition + GetTopClientAmendment, [rmRWARSCalculated, rmRWARSPrinting]);
    end
    else
      RendStatus := rmRWARSPrinted;  // stop printing
  end

  else
  begin
    inherited;
    ApplyBorderAmendment;
    PreparePrintOnEachPageControls(FBottomCutClientPosition - FTopCutClientPosition);
    ApplyPrintAmendment(GetTopClientAmendment - GetBottomClientAmendment, [rmRWARSCalculated, rmRWARSPrinting]);
  end;
end;

procedure TrmPrintContainer.CalcCutting;
var
  PreCutAmendment: Integer;
begin
  if MultiPaged then
  begin
    FTopCutPosition := CalculatedBounds.Top;
    FBottomCutPosition := CalculatedBounds.Bottom;
    FTopCutClientPosition := GetTopClientAmendment;
    FBottomCutClientPosition := (CalculatedBounds.Bottom - CalculatedBounds.Top) + GetBottomClientAmendment;

    if FTopCutClientPosition >= FBottomCutClientPosition then
      raise ErwException.Create('Multipaged print container "' + Name + '" is to small');

    PreCutAmendment := PreCut;
    //It is a never-ending loope if the amendment goes beyond of client top
    if (FBottomCutClientPosition - FTopCutClientPosition) > -PreCutAmendment then
      Dec(FBottomCutClientPosition, -PreCutAmendment);
  end

  else
  begin
    inherited;
    FTopCutClientPosition := (FTopCutPosition - CalculatedBounds.Top) + GetTopClientAmendment;
    FBottomCutClientPosition := (FBottomCutPosition - CalculatedBounds.Top) + GetBottomClientAmendment;
  end;
end;

procedure TrmPrintContainer.StoreChildren(const AStream: IEvDualStream; AStorePrintState: Boolean);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    Controls[i].StoreToStream(AStream, AStorePrintState);
end;

procedure TrmPrintContainer.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  AStream.WriteInteger(Ord(Color));
end;


procedure TrmPrintContainer.StoreToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  StoreChildren(AStream, AStorePrintState);
end;

procedure TrmPrintContainer.ReStoreChildren(const AStream: IEvDualStream; ARestorePrintState: Boolean);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    Controls[i].ReStoreFromStream(AStream, ARestorePrintState);
end;

procedure TrmPrintContainer.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  Color := TColor(AStream.ReadInteger);
end;

procedure TrmPrintContainer.ReStoreFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  ReStoreChildren(AStream, ARestorePrintState);
end;


procedure TrmPrintContainer.SetMultiPaged(const Value: Boolean);
begin
  FMultiPaged := Value;
  if not Loading and FMultiPaged then
  begin
    FPrintOnEachPage := False;
    FCutable := False;
  end;
end;

procedure TrmPrintContainer.SetPrintOnEachPage(const Value: Boolean);
begin
  if not Loading and FMultiPaged then
    FPrintOnEachPage := False
  else
    inherited;
end;

procedure TrmPrintContainer.OnPageCalculated;
var
  i: Integer;
begin
  inherited;

  // Notify children about finishing all calculations
  for i := 0 to ControlCount - 1 do
    if Controls[i].RendStatus = rmRWARSCalculated then
      Controls[i].OnPageCalculated;
end;


function TrmPrintContainer.GetControlByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := Controls[AIndex];
end;

function TrmPrintContainer.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := True;  
  Result.AcceptedClases := '';
end;

procedure TrmPrintContainer.SetRendStatusForChildren(AStatus: TrmRWARenderingStatus);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    if Controls[i] is TrmPrintContainer then
      TrmPrintContainer(Controls[i]).SetRendStatusChildrenToo(AStatus)
    else
      Controls[i].RendStatus := AStatus;
end;


function TrmPrintContainer.PControlByIndex(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Controls[AIndex]));
end;

function TrmPrintContainer.PControlCount: Variant;
begin
  Result := ControlCount;
end;

function TrmPrintContainer.PControlIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to ControlCount - 1 do
    if SameText(Controls[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;

function TrmPrintContainer.PFindControl(AName: Variant): Variant;
var
  i: Integer;
begin
  i := PControlIndexByName(AName);
  if i = -1 then
    Result := 0
  else
    Result := PControlByIndex(i);
end;

function TrmPrintContainer.PreCut: Integer;
var
  oldCutState: TrmRWACuttingState;
  oldTopCutPosition, oldBottomCutPosition,
  oldTopCutClientPosition, oldBottomCutClientPosition: Integer;
begin
  // imitate cutting procees. To be or not to be?
  Result := 0;

  // store previous state
  oldCutState := FCutState;
  oldTopCutPosition := FTopCutPosition;
  oldBottomCutPosition := FBottomCutPosition;
  oldTopCutClientPosition := FTopCutClientPosition;
  oldBottomCutClientPosition := FBottomCutClientPosition;
  try
    if MultiPaged then
      Result := PreCutAmendment // Correcting cut position
    else
      if CalculatedBounds.Top < Container.FBottomCutClientPosition then
      begin
        CalcCutting;   // call cutting process
        if FCutState <> rmRWACSNone then
          Result := PreCutAmendment; // Correcting cut position
      end;

  finally
    // restore previous state
    FCutState := oldCutState;
    FTopCutPosition := oldTopCutPosition;
    FBottomCutPosition := oldBottomCutPosition;
    FTopCutClientPosition := oldTopCutClientPosition;
    FBottomCutClientPosition := oldBottomCutClientPosition;
  end;
end;

procedure TrmPrintContainer.StorePrintState(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FTopCutClientPosition);
  AStream.WriteInteger(FBottomCutClientPosition);
end;

procedure TrmPrintContainer.ReStorePrintState(const AStream: IEvDualStream);
begin
  inherited;
  FTopCutClientPosition := AStream.ReadInteger;
  FBottomCutClientPosition := AStream.ReadInteger;
end;


procedure TrmPrintContainer.AlignChildren;
var
  ClientArea: TRect;
  CtrlList: TList;

  procedure PrepareToAlign(AAlign: TAlign; AInheritedOnly: Boolean);
  var
    i, j: Integer;
    fl: Boolean;
  begin
    // find aligned controls
    CtrlList.Clear;
    CtrlList.Capacity := ControlCount;
    for i := 0 to ControlCount - 1 do
      if (Controls[i].Align = AAlign) and ((Controls[i].Visible and Controls[i].Enabled) or (Renderer = nil)) and
         (AInheritedOnly and Controls[i].InheritedComponent and (Controls[i].DsgnLockState <> rwLSUnlocked) or
          not AInheritedOnly and (not Controls[i].InheritedComponent or (Controls[i].DsgnLockState = rwLSUnlocked))) then
        CtrlList.Add(Controls[i]);

    // sort by position
    if AAlign <> alClient then
    begin
      for i := 0 to CtrlList.Count - 2 do
        for j := i + 1 to CtrlList.Count - 1 do
        begin
          case AAlign of
            alTop:    fl := TrmPrintControl(CtrlList[i]).Top > TrmPrintControl(CtrlList[j]).Top;
            alBottom: fl := TrmPrintControl(CtrlList[i]).BoundsRect.Bottom > TrmPrintControl(CtrlList[j]).BoundsRect.Bottom;
            alLeft:   fl := TrmPrintControl(CtrlList[i]).Left > TrmPrintControl(CtrlList[j]).Left;
            alRight:  fl := TrmPrintControl(CtrlList[i]).BoundsRect.Right > TrmPrintControl(CtrlList[j]).BoundsRect.Right;
          else
            fl := False;
          end;

          if fl then
            CtrlList.Exchange(i, j);
        end;
    end;
  end;


  procedure AlignTop(AInheritedOnly: Boolean);
  var
    i: Integer;
    R: TRect;
    C: TrmPrintControl;
  begin
    PrepareToAlign(alTop, AInheritedOnly);
    for i := 0 to CtrlList.Count - 1 do
    begin
      C := TrmPrintControl(CtrlList[i]);
      R := Rect(ClientArea.Left, ClientArea.Top, ClientArea.Right, ClientArea.Top + C.Height);
      C.DisableAlign;
      try
        C.SetBoundsRect(R);
      finally
        C.EnableAlign;
      end;
      Inc(ClientArea.Top, C.Height);
    end
  end;


  procedure AlignBottom(AInheritedOnly: Boolean);
  var
    i: Integer;
    R: TRect;
    C: TrmPrintControl;
  begin
    PrepareToAlign(alBottom, AInheritedOnly);
    for i := CtrlList.Count - 1 downto 0 do
    begin
      C := TrmPrintControl(CtrlList[i]);
      R := Rect(ClientArea.Left, ClientArea.Bottom - C.Height, ClientArea.Right, ClientArea.Bottom);
      C.DisableAlign;
      try
        C.SetBoundsRect(R);
      finally
        C.EnableAlign;
      end;
      Dec(ClientArea.Bottom, C.Height);
    end
  end;


  procedure AlignLeft(AInheritedOnly: Boolean);
  var
    i: Integer;
    R: TRect;
    C: TrmPrintControl;
  begin
    PrepareToAlign(alLeft, AInheritedOnly);
    for i := 0 to CtrlList.Count - 1 do
    begin
      C := TrmPrintControl(CtrlList[i]);
      R := Rect(ClientArea.Left, ClientArea.Top, ClientArea.Left + C.Width, ClientArea.Bottom);
      C.DisableAlign;
      try
        C.SetBoundsRect(R)
      finally
        C.EnableAlign;
      end;
      Inc(ClientArea.Left, C.Width);
    end
  end;


  procedure AlignRight(AInheritedOnly: Boolean);
  var
    i: Integer;
    R: TRect;
    C: TrmPrintControl;
  begin
    PrepareToAlign(alRight, AInheritedOnly);
    for i := CtrlList.Count - 1 downto 0 do
    begin
      C := TrmPrintControl(CtrlList[i]);
      R := Rect(ClientArea.Right - C.Width, ClientArea.Top, ClientArea.Right, ClientArea.Bottom);
      C.DisableAlign;
      try
        C.SetBoundsRect(R);
      finally
        C.EnableAlign;
      end;
      Dec(ClientArea.Right, C.Width);
    end
  end;


  procedure AlignClient(AInheritedOnly: Boolean);
  var
    i: Integer;
    C: TrmPrintControl;
  begin
    PrepareToAlign(alClient, AInheritedOnly);
    for i := 0 to CtrlList.Count - 1 do
    begin
      C := TrmPrintControl(CtrlList[i]);
      C.DisableAlign;
      try
        C.SetBoundsRect(ClientArea);
      finally
        C.EnableAlign;
      end;
    end
  end;



begin
  if csDestroying in ComponentState then
    Exit;

  ClientArea := GetClientRect;
  CtrlList := TList.Create;
  try
    AlignTop(True);
    AlignBottom(True);
    AlignLeft(True);
    AlignRight(True);

    AlignTop(False);
    AlignBottom(False);
    AlignLeft(False);
    AlignRight(False);
    
    AlignClient(True);
    AlignClient(False);
  finally
    FreeAndNil(CtrlList);
  end;
end;

procedure TrmPrintContainer.AfterChangePos;
begin
  inherited;
  AlignChildren;
end;

procedure TrmPrintContainer.DoAlign;
begin
  inherited;
  if not (csLoading in ComponentState) then
    AlignChildren;
end;

procedure TrmPrintContainer.ApplyPrintAmendment(const AValue: Integer; const ARendStatus: TrmRWARenderingStatuses);
var
  i: Integer;
  C: TrmPrintControl;
begin
  // move children (down in most cases)

  if RendStatus = rmRWARSPrinting then
    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if (C.RendStatus in ARendStatus) and
         not C.PrintOnEachPage and not ((C is TrmPrintContainer) and TrmPrintContainer(C).MultiPaged) then
        OffsetRect(Controls[i].FCalculatedBounds, 0, AValue);
    end;
end;


function TrmPrintContainer.PreCutChild(const AControl: TrmPrintControl): Boolean;
var
  CutAmd: Integer;
begin
  CutAmd := AControl.PreCut;
  Assert(CutAmd <= 0);

  Result := CutAmd = 0;
  if not Result then
  begin
    Inc(FBottomCutClientPosition, CutAmd);
    Inc(FBottomCutPosition, CutAmd);
  end;
end;


function TrmPrintContainer.PreCutChildren: Boolean;
var
  i: Integer;
  C: TrmPrintControl;
begin
  Result := True;

  // keep cutting until it satisfies all terms
  for i := 0 to ControlCount - 1 do
  begin
    C := Controls[i];

    // skip not cutable children
    if not (C.RendStatus in [rmRWARSCalculated, rmRWARSPrinting]) or
       not C.PrintOnEachPage and not C.Enabled or (C is TrmPrintContainer) and TrmPrintContainer(C).MultiPaged then
      continue;

    Result := PreCutChild(C);

    if not Result then
      break;
  end;
end;

function TrmPrintContainer.PreCutAmendment: Integer;
var
  OriginalBottomCutPos, OriginalTopCutPos, CutAmd: Integer;
begin
  OriginalTopCutPos := FTopCutPosition;
  OriginalBottomCutPos := FBottomCutPosition;

  repeat
    // correct bottom cut position according to last amendment (cut threshold, border, cut text in the middle etc.)
    CutAmd := inherited PreCutAmendment;
    Inc(FBottomCutClientPosition, CutAmd);
    Inc(FBottomCutPosition, CutAmd);

    if FBottomCutClientPosition <= FTopCutClientPosition then
      break;

    // repeat this process until children and container get cut properly
  until PreCutChildren or (FBottomCutClientPosition <= FTopCutClientPosition);

  if FBottomCutClientPosition > FTopCutClientPosition then
    Result := FBottomCutPosition - OriginalBottomCutPos    // to be cut
  else
    Result := OriginalTopCutPos - OriginalBottomCutPos;    // not to be cut (cut line gets pushed up to top of container)
end;

function TrmPrintContainer.GetEachPageFooterHeight: Integer;
var
  i: Integer;
  C: TrmPrintControl;
begin
  if MultiPaged then
  begin
    // Remove all Bottom aligned controls from client area

    Result := 0;
    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if (C.RendStatus = rmRWARSCalculated) and (C.Align = alBottom) then
        Inc(Result, C.CalculatedBounds.Bottom - C.CalculatedBounds.Top);
    end;
  end
  else
    Result := inherited GetEachPageFooterHeight;
end;

function TrmPrintContainer.GetEachPageHeaderHeight: Integer;
var
  i: Integer;
  C: TrmPrintControl;
begin
  if MultiPaged then
  begin
    // Remove all Top aligned controls from client area

    Result := 0;
    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if (C.RendStatus = rmRWARSCalculated) and (C.Align = alTop) then
        Inc(Result, C.CalculatedBounds.Bottom - C.CalculatedBounds.Top);
    end;
  end
  else
    Result := inherited GetEachPageHeaderHeight;
end;

function TrmPrintContainer.AfterCalculate: Boolean;
var
  i: Integer;
  Ctrl: TrmPrintControl;
begin
  Result := inherited AfterCalculate;

  if Result then
    for i := 0 to ControlCount - 1 do
    begin
      Ctrl := Controls[i];
      if Ctrl.BlockParentIfEmpty and Ctrl.Enabled and (Ctrl.RendStatus <> rmRWARSCalculated) then
      begin
        RendStatus := rmRWARSPrinted;
        Result := False;
        break;
      end;
    end;
end;


{ TrmPrintForm }

procedure TrmPrintForm.CompileChildren(ASyntaxCheck: Boolean);
var
  i: Integer;
  C: TrwComponent;
begin
  for i := 0 to ComponentCount -1 do
  begin
    C := TrwComponent(Components[i]);
    if C.IsVirtualOwner then
      C.Compile(ASyntaxCheck)
    else
    begin
      C.CompileEvents(ASyntaxCheck);
      C.Formulas.Compile(ASyntaxCheck);
    end;
  end;
end;

constructor TrmPrintForm.Create(AOwner: TComponent);
begin
  inherited;
  FVirtOwner := True;

  FPaperOrientation := rpoPortrait;

  // Letter
  Width := Round(ConvertUnit(8.5, utInches, utLogicalPixels));
  Height := Round(ConvertUnit(11, utInches, utLogicalPixels));

  FTopMargin := Round(ConvertUnit(0.25, utInches, utLogicalPixels));
  FBottomMargin := Round(ConvertUnit(0.25, utInches, utLogicalPixels));
  FLeftMargin := Round(ConvertUnit(0.6, utInches, utLogicalPixels));
  FRightMargin := Round(ConvertUnit(0.3, utInches, utLogicalPixels));
  FDuplexing := False;
  FMultiPaged := True;
end;

procedure TrmPrintForm.CreateRWAObject;
begin
  Renderer.StorePageDataToResult;
end;

function TrmPrintForm.CreateVisualControl: IrwVisualControl;
begin
  Result := TrmdPrintForm.Create(Self);
end;

destructor TrmPrintForm.Destroy;
begin
  FreeAndNil(FGridBrush);
  inherited;
end;


procedure TrmPrintForm.SetDesigner(ADesigner: IrmDesigner);
begin
  inherited;
  if Assigned(ADesigner) then
    DesignParent := ADesigner.GetDesignParent(Name)
  else
    DesignParent := nil;
end;

procedure TrmPrintForm.Invalidate;
begin
  if VisualControlCreated then
    VisualControl.PropChanged(rwCPInvalidate);
end;

procedure TrmPrintForm.PaintBackground(ACanvas: TrwCanvas);
var
  R, R1: TRect;
begin
  R := ACanvas.ClipRect;
  R1 := Zoomed(Rect(0,0, Width, Height));
  if not IntersectRect(R, R, R1) then
    Exit;

  InflateRect(R, 1, 1);
  ACanvas.Pen.Width := 1;
  ACanvas.Pen.Color := clBlack;    

  if GetDesigner.GetDesignerSettings.ShowPaperGrid then
  begin
    // draw grid
    if not Assigned(FGridBrush) then
      PrepareGridBrush;

    with ACanvas do
    begin
      Brush.Bitmap := FGridBrush;
      Rectangle(R);
      Brush.Bitmap := nil;
    end;
  end

  else
  begin
    with ACanvas do
    begin
      Brush.Color := clWhite;
      Brush.Style := bsSolid;
      Rectangle(R);
    end;
  end;
end;

procedure TrmPrintForm.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    Brush.Color := clNone;
    Brush.Style := bsClear;
    Pen.Color := clBlue;
    Pen.Mode := pmCopy;
    Pen.Style := psDot;
    Pen.Width := 1;
    R := Zoomed(ClientRect);
    InflateRect(R, 1, 1);
    Rectangle(R);
  end;
end;

procedure TrmPrintForm.PrepareGridBrush;
var
  i, d: Integer;
begin
  if not Assigned(FGridBrush) then
    FGridBrush := TBitmap.Create;

  d := Zoomed(Round(ConvertUnit(1/8, utInches, utLogicalPixels)));
  FGridBrush.Width := d * 8;
  FGridBrush.Height := d * 8;
  FGridBrush.Canvas.Brush.Color := clWhite;
  FGridBrush.Canvas.FillRect(Rect(0, 0, FGridBrush.Width, FGridBrush.Height));
  FGridBrush.Canvas.Pen.Style := psSolid;
  FGridBrush.Canvas.Pen.Mode := pmCopy;
  i := 0;
  while i < FGridBrush.Width do
  begin
    if i = 0 then
      FGridBrush.Canvas.Pen.Color := cLineGrayColor
    else
      FGridBrush.Canvas.Pen.Color := cLineLiteGrayColor;
    FGridBrush.Canvas.MoveTo(i, 0);
    FGridBrush.Canvas.LineTo(i, FGridBrush.Height);
    Inc(i, d);
  end;

  i := 0;
  while i < FGridBrush.Height do
  begin
    if i = 0 then
      FGridBrush.Canvas.Pen.Color := cLineGrayColor
    else
      FGridBrush.Canvas.Pen.Color := cLineLiteGrayColor;
    FGridBrush.Canvas.MoveTo(0, i);
    FGridBrush.Canvas.LineTo(FGridBrush.Width, i);
    Inc(i, d);
  end;
end;

procedure TrmPrintForm.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
begin
  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  try
    inherited;
  finally
    Reader.Owner := PrevOwner;
    Reader.Root := PrevRoot;
    Reader.Parent := PrevParent;
  end;
end;

function TrmPrintForm.Renderer: IrmRWARenderEngine;
begin
  if Assigned(Owner) and Assigned(TrmReport(Owner).Renderer) then
    Result := TrmReport(Owner).Renderer.RWARenderEngine
  else
    Result := nil;
end;

procedure TrmPrintForm.SetDesignParent(Value: TWinControl);
begin
  if Assigned(Value) and ((DsgnLockState <> rwLSHidden) or LibDesignMode or GetDesigner.LibComponentDesigninig) then
    VisualControl.RealObject.Parent := Value
  else
    DestroyVisualControl;

  inherited;    
end;

procedure TrmPrintForm.SetMargin(Index, Value: Integer);
begin
  case Index of
    0: begin
         OffsetControlsVert(Value - FTopMargin);
         FTopMargin := Value;
       end;

    1: FBottomMargin := Value;

    2: begin
         OffsetControlsHor(Value - FLeftMargin);
         FLeftMargin := Value;
       end;

    3: FRightMargin := Value;
  end;

  Invalidate;
end;

procedure TrmPrintForm.SetPaperOrientation(Value: TrwPaperOrientation);
var
  h: Integer;
begin
  if (FPaperOrientation <> Value) then
  begin
    FPaperOrientation := Value;
    if not Loading then
    begin
      h := Height;
      Height := Width;
      Width := h
    end;  
  end;
end;

procedure TrmPrintForm.SetParentComponent(Value: TComponent);
begin
  // Do not need to do anything in here!
end;

procedure TrmPrintForm.WriteState(Writer: TWriter);
var
  OldRoot: TComponent;
  OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := TrmReport(OldRootAncestor).FindComponent(Name);
    Writer.Root := Self;

    inherited;

  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;


procedure TrmPrintForm.DoPrint;
var
  i: Integer;
begin
  CalcCutting;

  // Print all controls first
  for i := 0 to ControlCount - 1 do
    Controls[i].Print;

  PrepareForNextPrintCycle;
  // Store rendered PrintForm into Stream
  CreateRWAObject;
end;

procedure TrmPrintForm.AdjustClientRect(var Rect: TRect);
begin
  Rect.Left := Rect.Left + FLeftMargin;
  Rect.Top := Rect.Top + FTopMargin;
  Rect.Right := Rect.Right - FRightMargin;
  Rect.Bottom := Rect.Bottom - FBottomMargin;
end;

procedure TrmPrintForm.UnAdjustClientRect(var Rect: TRect);
begin
  Rect.Left := Rect.Left - FLeftMargin;
  Rect.Top := Rect.Top - FTopMargin;
  Rect.Right := Rect.Right + FRightMargin;
  Rect.Bottom := Rect.Bottom + FBottomMargin;
end;

procedure TrmPrintForm.CalcCutting;
var
  R: TRect;
begin
  if MultiPaged then
    inherited
  else
  begin
    FCutState := rmRWACSNone;
    FTopCutPosition := 0;
    FBottomCutPosition := CalculatedBounds.Bottom - CalculatedBounds.Top;
    R := CalculatedBounds;
    OffsetRect(R, -R.Left, -R.Top);
    FTopCutClientPosition := R.Top + GetTopClientAmendment;
    FBottomCutClientPosition := R.Bottom + GetBottomClientAmendment;
  end;
end;

procedure TrmPrintForm.SetPrintOnEachPage(const Value: Boolean);
begin
  FPrintOnEachPage := False;
end;

function TrmPrintForm.AfterCalculate: Boolean;
begin
  Result := inherited AfterCalculate;

  if Result then
    OnPageCalculated; // Notify children about finishing all calculations
end;


procedure TrmPrintForm.Notify(AEvent: TrmDesignEventType);
begin
  if AEvent = rmdZoomChanged then
  begin
    TrmdPrintForm(VisualControl.RealObject).UpdateSize;
    TrmdPrintForm(VisualControl.RealObject).Invalidate;
  end;
    
  inherited;
end;

procedure TrmPrintForm.OffsetControlsHor(AOffset: Integer);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    Controls[i].Left := Controls[i].Left + AOffset;
end;

procedure TrmPrintForm.OffsetControlsVert(AOffset: Integer);
var
  i: Integer;
begin
  for i := 0 to ControlCount - 1 do
    Controls[i].Top := Controls[i].Top + AOffset;
end;

procedure TrmPrintForm.AfterChangePos;
begin
  if VisualControlCreated then
    TrmdPrintForm(VisualControl.RealObject).UpdateSize;
  inherited;
end;

function TrmPrintForm.GetPaperSize: TrwPaperSize;
var
  W, H: Integer;
  R: TrwPaperRec;
  i: TrwPaperSize;
begin
  Result := psCustom;

  for i := Low(TrwPaperSize) to High(TrwPaperSize) do
  begin
    R := cPaperInfo[i];
    H := Round(ConvertUnit(R.Height / 10, utMillimeters, utLogicalPixels));
    W := Round(ConvertUnit(R.Width / 10, utMillimeters, utLogicalPixels));

    if (Width = W) and (Height = H) or (Width = H) and (Height = W) then
    begin
      Result := i;
      break;
    end;
  end;
end;


procedure TrmPrintForm.DoCalculate;

  procedure AlignContainer(AContaner: TrmPrintContainer);
  var
    i: Integer;
  begin
    AContaner.AlignChildren;
    for i := 0 to AContaner.ControlCount - 1 do
      if AContaner.Controls[i] is TrmPrintContainer then
        AlignContainer(TrmPrintContainer(AContaner.Controls[i]));
  end;

begin
  AlignContainer(Self);
  inherited;
end;

procedure TrmPrintForm.BeforePrint;
begin
  inherited;
  FPrintAccepted := Enabled and Visible;
end;

function TrmPrintForm.GetBottomClientAmendment: Integer;
begin
  Result := - BottomMargin - GetEachPageFooterHeight;
end;

function TrmPrintForm.GetTopClientAmendment: Integer;
begin
  Result := TopMargin + GetEachPageHeaderHeight;
end;

function TrmPrintForm.GetLeftClientAmendment: Integer;
begin
  Result := LeftMargin;
end;

function TrmPrintForm.GetRightClientAmendment: Integer;
begin
  Result := -RightMargin;
end;


function TrmPrintForm.PrintForm: TrmPrintForm;
begin
  Result := Self;
end;


{ TrmdPrintForm }

constructor TrmdPrintForm.Create(AOwner: TComponent);
begin
  inherited;
  Color := clWhite;
end;

function TrmdPrintForm.DispatchMessage(var Message: TMessage): Boolean;
var
  P: TPoint;
  O: TrmPrintControl;

  procedure ControlMouseEvent;
  begin
    with TWMMouse(Message) do
      case Message.Msg of
        WM_MOUSEMOVE:      O.MouseMove(KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDOWN:    O.MouseDown(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONUP:      O.MouseUp(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDBLCLK:;
        WM_RBUTTONDOWN:    O.MouseDown(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONUP:      O.MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONDBLCLK:;
        WM_MBUTTONDOWN:    O.MouseDown(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONUP:      O.MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONDBLCLK:;
        WM_MOUSEWHEEL:;
      end;
  end;

begin
  Result := Inherited DispatchMessage(Message);

  if Result then
    UpdateMouseLastControl(nil)

  else
    if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
    begin
      P := UnZoomed(Point(Smallint(Message.LParamLo), Smallint(Message.LParamHi)));
      Dec(P.X, TrmPrintForm(RWObject).Left);
      Dec(P.Y, TrmPrintForm(RWObject).Top);

      if GetCaptureRMControl = nil then
        O := TrmPrintControl(TrmPrintForm(RWObject).ControlAtPos(P))
      else
        O := TrmPrintControl(GetCaptureRMControl);
      if Assigned(O) then
      begin
        P := O.FormToClient(P);
        Message.lParam := MakeLong(Word(P.X), Word(P.Y));
        if not IsDesignMsg(O, Message) then
          ControlMouseEvent;
      end
      else
      begin
        Message.lParam := MakeLong(Word(P.X), Word(P.Y));
        if not IsDesignMsg(RWObject, Message) then
        begin
          O := TrmPrintForm(RWObject);
          ControlMouseEvent;
        end;
      end;

      Result := True;

      UpdateMouseLastControl(O);
    end;
end;

procedure TrmdPrintForm.Paint;
var
  SaveIndex: Integer;
  R: TRect;
begin
  SaveIndex := SaveDC(Canvas.Handle);
  try
    with Canvas do
    begin
      R := TrmPrintForm(RWObject).BoundsRect;
      R := Zoomed(R);
      ExcludeClipRect(Canvas.Handle, R.Left, R.Top, R.Right, R.Bottom);
      R := Canvas.ClipRect;
      Brush.Color := clAppWorkSpace;
      Brush.Style := bsSolid;
      FillRect(R);
    end;
  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;

  SaveIndex := SaveDC(Canvas.Handle);
  try
    MoveWindowOrg(Canvas.Handle, Zoomed(TrmPrintForm(RWObject).Left), Zoomed(TrmPrintForm(RWObject).Top));
    TrmPrintForm(RWObject).PaintToCanvas(Canvas);

    R := Zoomed(Rect(0, 0, TrmPrintForm(RWObject).Width, TrmPrintForm(RWObject).Height));
    with Canvas do
    begin
      Pen.Width := 2;
      Pen.Color := clBlack;
      Pen.Mode := pmCopy;
      Pen.Style := psSolid;
      MoveTo(R.Left, R.Bottom + 1);
      LineTo(R.Right + 1, R.Bottom + 1);
      LineTo(R.Right + 1, R.Top);
    end;

  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;
end;

procedure TrmdPrintForm.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
    UpdateSize;

  inherited;
end;

procedure TrmdPrintForm.UpdateSize;
var
  l, t, w, h: Integer;
begin
  if not FUpdating and (TrmPrintForm(RWObject).GetDesigner <> nil) then
  begin
    FZoomFactor := TrmPrintForm(RWObject).GetDesigner.GetZoomFactor;

    if FZoomFactor = 0 then
      Exit;

    FUpdating := True;
    TrmPrintForm(RWObject).PrepareGridBrush;

    w := Zoomed(TrmPrintForm(RWObject).Width);
    h := Zoomed(TrmPrintForm(RWObject).Height);
    l := Left;
    t := Top;

    if Assigned(Parent) then
    begin
      if w < Parent.ClientWidth then
      begin
        l :=  UnZoomed((Parent.ClientWidth - w) div 2);
        if l < 200 then
          l := 200;
      end
      else
        l := 200;

      if h < Parent.ClientHeight then
      begin
        t :=  UnZoomed((Parent.ClientHeight - h) div 2);
        if t < 50 then
          t := 50;
      end
      else
        t := 50;
    end;

    w := Zoomed(TrmPrintForm(RWObject).Width + l * 2);
    h := Zoomed(TrmPrintForm(RWObject).Height + t * 2);

    SetBounds(0, 0, w, h);
    TrmPrintForm(RWObject).Left := l;
    TrmPrintForm(RWObject).Top := t;
    FUpdating := False;
  end;
end;


procedure TrmdPrintForm.WMSetCursor(var Message: TWMSetCursor);
var
  Crs: TCursor;
  Control: TrmPrintControl;
  P: TPoint;
begin
  inherited;
  with Message do
    if CursorWnd = Handle then
      case Smallint(HitTest) of
        HTCLIENT:
          begin
            Crs := Screen.Cursor;
            if Crs = crDefault then
            begin
              GetCursorPos(P);
              P := TrmPrintForm(RWObject).ScreenToClient(P);
              Control := TrmPrintControl(TrmPrintForm(RWObject).ControlAtPos(P));
              if Assigned(Control) then
                Crs := Control.Cursor;

              if Crs = crDefault then
                Crs := Cursor;
            end;

            if Crs <> crDefault then
            begin
              Windows.SetCursor(Screen.Cursors[Crs]);
              Result := 1;
            end;
          end;
      end;
end;


{ TrmPrintText }

function TrmPrintText.CanWriteValue: Boolean;
begin
  Result := FormulaValue = nil;
end;

constructor TrmPrintText.Create(AOwner: TComponent);
begin
  FValueHolder := TrmsValueHolder.Create(Self);
  inherited;
end;

destructor TrmPrintText.Destroy;
begin
  FreeAndNil(FValueHolder);  
  inherited;
end;

function TrmPrintText.FormulaValue: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatSetProperty, 'Value');
end;

function TrmPrintText.GetTextForPrint: string;
begin
  Result := FormatValue(FFormat, Value);

  if (FormulaTextFilter <> nil) and (Renderer <> nil) then
    Result := FormulaTextFilter.Calculate([Result]);
end;

function TrmPrintText.GetValue: Variant;
begin
  if not (csWriting in ComponentState) and (Renderer <> nil) then
    Calculate;

  Result := FValueHolder.Value;
end;

procedure TrmPrintText.InitializeForDesign;
begin
  inherited;

  if Text = '' then
    Value := Name;
end;

procedure TrmPrintText.Loaded;
begin
  if FormulaValue <> nil then
    FValueHolder.Value := Null;

  inherited;
end;

procedure TrmPrintText.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  Value := AStream.ReadVariant;
  Format := AStream.ReadString;
end;

procedure TrmPrintText.SetFormat(const Value: string);
begin
  FFormat := Value;
  Invalidate;
end;

procedure TrmPrintText.SetValue(const AValue: Variant);
begin
  FValueHolder.Value := AValue;
  CheckAutosize;
  Invalidate;
end;

procedure TrmPrintText.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  AStream.WriteVariant(Value);
  AStream.WriteString(Format);
end;

function TrmPrintText.FormulaTextFilter: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatDataFilter, 'Text');
end;


function TrmPrintText.GetDataFieldInfo: TrmDataFieldInfoRec;
begin
  Result := DataFieldInfo(Self, nil);
end;

function TrmPrintText.GetFieldName: String;
begin
  Result := DataField;
end;

procedure TrmPrintText.SetFieldName(const AFieldName: String);
begin
  DataField := AFieldName;
end;

function TrmPrintText.GetDataField: String;
begin
  Result := FValueHolder.DataField;
end;

procedure TrmPrintText.SetDataField(const AValue: String);
begin
  FValueHolder.DataField := AValue;
end;


procedure TrmPrintText.SetValueFromDataSource;
begin
  FValueHolder.SetValueFromDataSource;
  CheckAutosize;
end;

function TrmPrintText.GetTextForDraw: String;
begin
  if IsDesignMode and (FormulaValue <> nil) then
    Result := cCalcTag
  else
    Result := Text;
end;

{ TrmPrintPanel }

procedure TrmPrintPanel.CreateRWAObject;
begin
  Renderer.AddFrame(PrintRectOnCurrPage, Compare, LayerNumber, Color, [], clBlack, 0, psSolid);
end;


{ TrmPrintImage }

constructor TrmPrintImage.Create(AOwner: TComponent);
begin
  inherited;
  FPicture := TrwPicture.Create;
  FPicture.OnChange := OnChangePicture;
  FStretch := True;
  Transparent := True;
  Height := Round(ConvertUnit(105, utScreenPixels, utLogicalPixels));
  Width := Round(ConvertUnit(105, utScreenPixels, utLogicalPixels));
  DsgnShowCorners := True;
end;

destructor TrmPrintImage.Destroy;
begin
  FreeAndNil(FPicture);
  inherited;
end;

function TrmPrintImage.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
begin
  if not Stretch and Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
  begin
    AWidth := Picture.Width;
    AHeight := Picture.Height;
  end;

  Result := inherited CheckSizeConstrains( ALeft, ATop, AWidth, AHeight);
end;

procedure TrmPrintImage.CheckSize;
begin
  if not Stretch and Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
    SetBounds(Left, Top, Picture.Width, Picture.Height);
end;

procedure TrmPrintImage.CreateRWAObject;
begin
  Renderer.AddPicture(PrintRectOnCurrPage, Compare, LayerNumber, Transparent, Picture, Stretch);
end;

procedure TrmPrintImage.OnChangePicture(Sender: TObject);
begin
  if not Loading then
  begin
    CheckSize;
    Invalidate;
  end;
end;

procedure TrmPrintImage.SetPicture(const Value: TrwPicture);
begin
  Picture.Assign(Value);
end;

procedure TrmPrintImage.SetStretch(const Value: Boolean);
begin
  FStretch := Value;
  CheckSize;
  Invalidate;
end;

procedure TrmPrintImage.SetTransparent(const Value: Boolean);
begin
  if Value then
    Color := clNone
  else
    Color := clWhite;
end;

procedure TrmPrintImage.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
  lCaptionText: String;
begin
  R := ZoomedClientRect(ACanvas);

  if not Assigned(Picture.Graphic) or Picture.Graphic.Empty then
  begin
    lCaptionText := GetClassName;
    Delete(lCaptionText, 1 ,3);
    DrawDiagCrossBoxRM(ACanvas, R, lCaptionText);
  end

  else
  begin
    Picture.Graphic.Transparent := Transparent;
	  ACanvas.StretchDraw(R, Picture.Graphic);
  end;

  if DsgnShowCorners then
    DrawCornersRM(ACanvas, R.Right - R.Left, R.Bottom - R.Top);
end;

function TrmPrintImage.GetTransparent: Boolean;
begin
  Result := Color = clNone;
end;

procedure TrmPrintImage.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
var
  Pict: IEvDualStream;
  ClName: String;
  Cl: TGraphicClass;
begin
  inherited;
  Transparent := AStream.ReadBoolean;
  Stretch := AStream.ReadBoolean;

  ClName := AStream.ReadString;
  if ClName <> '' then
  begin
    Cl := TGraphicClass(GetClass(ClName));
    Picture.Graphic := Cl.Create;

    Pict := TEvDualStreamHolder.Create;
    AStream.ReadStream(Pict);
    Pict.Position := 0;    
    Picture.Graphic.LoadFromStream(Pict.RealStream);
  end
  else
    Picture.Assign(nil);
end;

procedure TrmPrintImage.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
var
  Pict: IEvDualStream;
begin
  inherited;
  AStream.WriteBoolean(Transparent);
  AStream.WriteBoolean(Stretch);

  if Assigned(Picture.Graphic) then
  begin
    Pict := TEvDualStreamHolder.Create;  
    AStream.WriteString(Picture.Graphic.ClassName);
    Picture.Graphic.SaveToStream(Pict.RealStream);
    Pict.Position := 0;
    AStream.WriteStream(Pict);
  end
  else
    AStream.WriteString('');
end;

function TrmPrintImage.CutThreshold: Integer;
begin
  // Image cannot be cut
  Result := CalculatedBounds.Bottom - CalculatedBounds.Top;
end;


{ TrmPrintCustomText }

procedure TrmPrintCustomText.CheckAutosize;
var
  Tinf: TrwTextDrawInfoRec;
begin
  if not Autosize or Loading then
    Exit;

  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;
  Tinf.ZoomRatio := 1;

  Tinf.Text := GetTextForDraw;

  if (Tinf.Text = '') and IsDesignMode then
    Tinf.Text := '    ';
  Tinf.Font := Font;
  Tinf.DigitNet := '';
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := GetClientRect;

  DrawTextImageRM(@Tinf);

  case Alignment of
    taLeftJustify:   Tinf.TextRect.Left := Left;
    taRightJustify:  Tinf.TextRect.Left := Left - (Tinf.TextRect.Right - Width);
    taCenter:        Tinf.TextRect.Left := Left - (Tinf.TextRect.Right - Width) div 2;
  end;

  SetBounds(Tinf.TextRect.Left, Top, Tinf.TextRect.Right, Tinf.TextRect.Bottom);
end;

constructor TrmPrintCustomText.Create(AOwner: TComponent);
begin
  inherited;
  DsgnShowCorners := True;

  Width := Round(ConvertUnit(65, utScreenPixels, utLogicalPixels));
  Height := Round(ConvertUnit(17, utScreenPixels, utLogicalPixels));
  Color := clNone;
  FAlignment := taLeftJustify;
  FWordWrap := False;
  FAutosize := True;
  FLayout := tlTop;
  FFont := TrwFont.Create(Self);
  FFont.Name := 'Arial';
  FFont.Height := -83;
  FFont.Color := clBlack;
  FFont.OnChange := OnChangeFont;
end;

procedure TrmPrintCustomText.CreateRWAObject;
var
  R: TRect;
  TR: TStringBoundsRec;
  h: String;
begin
  R := PrintRectOnCurrPage;
  OffsetRect(R, -R.Left, -R.Top);
  TR := TextInRect(R, RendText, RendTextHeight, True);
  if (TR.BeginPos > 0) and (TR.EndPos > 0) then
  begin
    h := Copy(RendText, TR.BeginPos, TR.EndPos);
    RendText := Copy(RendText, TR.EndPos + 1, Length(RendText) - TR.EndPos);
  end
  else
    h := '';

  Renderer.AddText(PrintRectOnCurrPage, Compare, LayerNumber, Alignment, Layout, Color, False, '', Font, h);
end;

destructor TrmPrintCustomText.Destroy;
begin
  FreeAndNil(FFont);
  inherited;
end;

function TrmPrintCustomText.GetTextForDraw: String;
begin
  Result := Text;
end;

function TrmPrintCustomText.GetTextForPrint: string;
begin
  Result := '';
end;

procedure TrmPrintCustomText.Loaded;
begin
  inherited;

  if Autosize then
    CheckAutosize;
end;

procedure TrmPrintCustomText.OnChangeFont(Sender: TObject);
begin
  CheckAutosize;
  Invalidate;
end;

procedure TrmPrintCustomText.PaintControl(ACanvas: TrwCanvas);
var
  Tinf: TrwTextDrawInfoRec;
  R: TRect;
begin
  inherited;

  R := ZoomedBoundsRect(ACanvas);
  ACanvas.Brush.Color := Color;
  if Color <> clNone then
  begin
    ACanvas.Brush.Style  := bsSolid;
    ACanvas.FillRect(Rect(0, 0, R.Right - R.Left, R.Bottom - R.Top));
  end
  else
    ACanvas.Brush.Style  := bsClear;

  Tinf.CalcOnly := False;
  Tinf.ZoomRatio := ZoomFactor;
  Tinf.Canvas := ACanvas;
  Tinf.Text := GetTextForDraw;
  Tinf.Font := Font;
  Tinf.Color := Color;
  Tinf.DigitNet := '';
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := ClientRect;

  DrawTextImageRM(@Tinf);

  if DsgnShowCorners then
    DrawCornersRM(ACanvas, R.Right - R.Left, R.Bottom - R.Top);
end;

function TrmPrintCustomText.PreCutAmendment: Integer;
var
  lc, h: Integer;
begin
  Result := inherited PreCutAmendment;

  if RendText = '' then
    Exit;

  lc := TextLineCount(RendText);
  h := FBottomCutPosition - FTopCutPosition;
  if h div RendTextHeight < lc then
    Result := Result + (h div RendTextHeight) * RendTextHeight - h;
end;

procedure TrmPrintCustomText.PrepareTextForPrinting;
var
  Tinf: TrwTextDrawInfoRec;
begin
  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;
  Tinf.ZoomRatio := 1;
  Tinf.Text := StringReplace(Text, #13#10, #13, [rfReplaceAll]);
  Tinf.Font := Font;
  Tinf.DigitNet := '';
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := GetClientRect;
  OffsetRect(Tinf.TextRect, -Tinf.TextRect.Left, -Tinf.TextRect.Top);

  DrawTextImageRM(@Tinf);

  RendText := Tinf.Text;
  RendTextHeight := Tinf.LineHeight;
end;

procedure TrmPrintCustomText.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  Color := AStream.ReadInteger;
  Alignment := TAlignment(AStream.ReadInteger);
  ReStoreFont(AStream, Font);
  WordWrap := AStream.ReadBoolean;
  Autosize := AStream.ReadBoolean;
  Layout := TTextLayout(AStream.ReadInteger);
  ReStoreRendInfo(AStream);
end;

procedure TrmPrintCustomText.SetAlignment(Value: TAlignment);
begin
  FAlignment := Value;
  Invalidate;
end;

procedure TrmPrintCustomText.SetAutosize(Value: Boolean);
begin
  FAutosize := Value;
  CheckAutosize;
  Invalidate;
end;

procedure TrmPrintCustomText.SetDesigner(ADesigner: IrmDesigner);
begin
  inherited;
  CheckAutosize;
end;

procedure TrmPrintCustomText.SetFont(Value: TrwFont);
begin
  Font.Assign(Value);
end;

procedure TrmPrintCustomText.SetLayout(Value: TTextLayout);
begin
  FLayout := Value;
  Invalidate;
end;

procedure TrmPrintCustomText.SetRendStatus(const Value: TrmRWARenderingStatus);
begin
  inherited;
  if RendStatus = rmRWARSCalculated then
    PrepareTextForPrinting;
end;

procedure TrmPrintCustomText.SetWordWrap(Value: Boolean);
begin
  FWordWrap := Value;
  CheckAutosize;
  Invalidate;
end;

procedure TrmPrintCustomText.StoreSelfToStream(AStream: IEvDualStream;
  AStorePrintState: Boolean);
begin
  inherited;
  AStream.WriteInteger(Ord(Color));
  AStream.WriteInteger(Ord(Alignment));
  StoreFont(AStream, Font);
  AStream.WriteBoolean(WordWrap);
  AStream.WriteBoolean(Autosize);
  AStream.WriteInteger(Ord(Layout));
  StoreRendInfo(AStream);
end;

{ TrmPrintPageNumber }

constructor TrmPrintPageNumber.Create(AOwner: TComponent);
begin
  inherited;
  FStyleFormat := 'Page ' + cRWAMpage;
  PrintOnEachPage := True;
  Cutable := False;
end;

procedure TrmPrintPageNumber.CreateRWAObject;
var
  R: TRect;
  TR: TStringBoundsRec;
  h: String;
begin
  R := PrintRectOnCurrPage;
  OffsetRect(R, -R.Left, -R.Top);
  RendText := GetTextForPrint;
  TR := TextInRect(R, RendText, RendTextHeight, True);
  if (TR.BeginPos > 0) and (TR.EndPos > 0) then
  begin
    h := Copy(RendText, TR.BeginPos, TR.EndPos);
    RendText := Copy(RendText, TR.EndPos + 1, Length(RendText) - TR.EndPos);
  end
  else
    h := '';

  Renderer.AddText(PrintRectOnCurrPage, Compare, LayerNumber, Alignment, Layout, Color, False, '', Font, h);
end;

function TrmPrintPageNumber.GetTextForDraw: String;
begin
  if IsDesignMode or not (Renderer <> nil) then
  begin
    Result := FStyleFormat;
    Result := StringReplace(Result, cRWAMpage, '1', [rfReplaceAll, rfIgnoreCase]);
  end
  else
    Result := inherited GetTextForDraw;
end;

function TrmPrintPageNumber.GetTextForPrint: string;
begin
  Result := FStyleFormat;
  Result := StringReplace(Result, cRWAMpage, IntToStr(Renderer.CurrentPageNumber), [rfReplaceAll, rfIgnoreCase]);
end;

procedure TrmPrintPageNumber.ReStoreSelfFromStream(AStream: IEvDualStream; ARestorePrintState: Boolean);
begin
  inherited;
  StyleFormat := AStream.ReadString;
end;

procedure TrmPrintPageNumber.SetStyleFormat(const AValue: String);
begin
  FStyleFormat := AValue;
  Invalidate;
end;

procedure TrmPrintPageNumber.StoreSelfToStream(AStream: IEvDualStream; AStorePrintState: Boolean);
begin
  inherited;
  AStream.WriteString(StyleFormat);
end;

end.
