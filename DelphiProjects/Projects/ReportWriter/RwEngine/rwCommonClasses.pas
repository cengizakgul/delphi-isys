unit rwCommonClasses;

interface

uses
  Windows, Classes, Graphics, Controls, TypInfo, StdCtrls, SysUtils,
  Variants, rwBasicClasses, rwTypes, rwMessages, rwPrintElements, rwBasicUtils, rwEngineTypes,
  rmTypes, rwGraphics, rwRTTI;

type
  {TrwPrintableComponent is abstract component for printing}


  TrwControl = class(TrwComponent)
  private
    FVisible: Boolean;
    FAlign:   TAlign;
    FColor:   TColor;
    FDsgnShowCorners: Boolean;

    procedure SetDsgnShowCorners(const Value: Boolean);
    function  GetClientHeight: Integer;
    function  GetClientWidth: Integer;

  protected
    FLeft:    Integer;
    FTop:     Integer;
    FWidth:   Integer;
    FHeight:  Integer;

    procedure SetPosition(Index: Integer; Value: Integer); virtual;
    function  GetPosition(Index: Integer): Integer; virtual;
    procedure SetVisible(Value: Boolean); virtual;
    function  GetVisible: Boolean; virtual;
    procedure SetColor(Value: TColor); virtual;
    function  GetColor: TColor; virtual;
    procedure SetAlign(Value: TAlign); virtual;
    function  GetAlign: TAlign; virtual;
    function  GetClientRect: TRect; virtual;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; virtual;

    //interface
    function  GetLeft: Integer; override;
    procedure SetLeft(AValue: Integer); override;
    function  GetTop: Integer; override;
    procedure SetTop(AValue: Integer); override;
    function  GetWidth: Integer; override;
    procedure SetWidth(AValue: Integer); override;
    function  GetHeight: Integer; override;
    procedure SetHeight(AValue: Integer); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); virtual;
    function    BoundsRect: TRect;
    procedure   BeforeChangePos; virtual;
    procedure   AfterChangePos; virtual;

    property Color: TColor read GetColor write SetColor;
    property Align: TAlign read GetAlign write SetAlign;
    property DsgnShowCorners: Boolean read FDsgnShowCorners write SetDsgnShowCorners;
    property ClientRect: TRect read GetClientRect;
    property ClientWidth: Integer read GetClientWidth;
    property ClientHeight: Integer read GetClientHeight;

  published
    property Left: Integer index 0 read GetPosition write SetPosition;
    property Top: Integer index 1 read GetPosition write SetPosition;
    property Width: Integer index 2 read GetPosition write SetPosition;
    property Height: Integer index 3 read GetPosition write SetPosition;
    property Visible: Boolean read GetVisible write SetVisible;
  end;


  {TrwPrintableControl is control on the report paper}

  TrwRMRenderingStatus = (rwRMPSNone, rwRMPSCalculated, rwRMPSPrinting, rwRMPSPrinted, rwRMPSReading);
  TrwRMCuttingState = (rwRMCSNone, rwRMCSTop, rwRMCSMiddle, rwRMCSBottom);

  TrwRenderingPrintInfo = record
    RenderingStatus: TrwRMRenderingStatus;
    PaperLocation: TRect;
    PrevCutPosition: Integer;
    CutPosition: Integer;
    CutState: TrwRMCuttingState;
    CutThreshold: Integer;
    Text: String;
    TextHeight: Integer;
  end;

  PTrwRenderingPrintInfo = ^TrwRenderingPrintInfo;


  TrwPrintableControl = class(TrwControl)
  private
    FLayerNumber: TrwLayerNumber;
    FCompare: Boolean;

  protected
    FRendering: TObject;
    FRenderingInfo: TrwRenderingPrintInfo; //RM
    procedure RegisterComponentEvents; override;
    function  AcceptPrint: Boolean; virtual;
    procedure EndPrint; virtual;
    procedure PrintGraphic; virtual; abstract;
    procedure PrintASCII; virtual;
    function  NeedPrintAsASCII: Boolean; virtual;
    function  IsRenderingReading: Boolean;

  public
    constructor Create(AOwner: TComponent); override;
    function    Print(Sender: TObject): Boolean;

    function    RenderingInfo: PTrwRenderingPrintInfo; //RM
    procedure   InitRenderingInfo(const Recalculate: Boolean); virtual;

  published
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber;
    property Compare: Boolean read FCompare write FCompare;
  end;

  TrwPrintableContainer = class;

  {TrwChildComponent is abstract control on container}

  TrwChildComponent = class(TrwPrintableControl)
  private
    FContainer: TrwPrintableContainer;
    FAlignmentNeeded: Boolean;
    function StoreContainer: Boolean;

  protected
    procedure SetDesignParent(Value: TWinControl); override;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    procedure SetAlign(Value: TAlign); override;
    procedure SetContainer(Value: TrwPrintableContainer); virtual;
    procedure SetParentComponent(Value: TComponent); override;
    procedure CalcPosRelativeBand(AControl: TControl);
    procedure Loaded; override;
    procedure RequestAlign; virtual;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); virtual;
    function  GetDsgnLockState: TrwDsgnLockState; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

  published
    property Container: TrwPrintableContainer read FContainer write SetContainer stored StoreContainer;
  end;

  {TrwPrintableContainer is abstract container on report form}

  TrwPrintableContainer = class(TrwChildComponent)
  private
    FControls: TList;
    FPrintAsASCII: Boolean;
    FAutoHeight: Boolean;
    FAutoWidth: Boolean;
    FAlignLevel: Integer;
    FTransparent: Boolean;
    FCornerText: string;
    FDesignAlign: TAlign;
    FMultiPaged: Boolean;
    FAligningRightNow: Boolean;
    function  GetControl(Index: Integer): TrwChildComponent;
    procedure DestroyChildren;
    procedure SetCornerText(const Value: string);
    procedure AlignControl(AControl: TrwChildComponent);
    procedure SetDesignAlign(const Value: TAlign);
    procedure SetMultiPaged(const Value: Boolean);

  protected
    FFinishPrinting: Boolean;
    FBorderWidth: Integer;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure RecalcSize; virtual;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure Loaded; override;
    procedure SetAutoHeight(Value: Boolean); virtual;
    procedure SetAutoWidth(Value: Boolean); virtual;
    function  AcceptPrint: Boolean; override;
    procedure PrintGraphic; override;
    procedure PrintASCII; override;
    function  NeedPrintAsASCII: Boolean; override;
    procedure PrintChildren;
    procedure SetTransparent(const Value: Boolean); virtual;
    procedure EnableAlign;
    procedure DisableAlign;
    function  Aligninig: Boolean;
    procedure AlignControls(AControl: TrwChildComponent; var Rect: TRect); virtual;
    procedure AdjustClientRect(var Rect: TRect); virtual;
    procedure ControlsAligned; virtual;
    procedure AdjustSize; virtual;
    function  GetClientRect: TRect; override;
    function  GetControlCount: Integer; override;

  public
    property Controls[Index: Integer]: TrwChildComponent read GetControl;
    property ControlCount: Integer read GetControlCount;
    property AutoWidth: Boolean read FAutoWidth write SetAutoWidth;
    property BorderWidth: Integer read FBorderWidth;
    property CornerText: string read FCornerText write SetCornerText;
    property DesignAlign: TAlign read FDesignAlign write SetDesignAlign;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   FinishPrinting;
    procedure   RemoveControl(AControl: TrwChildComponent);
    function    InsertControl(AControl: TrwChildComponent): Integer;
    procedure   Realign;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

  published
    property Color;
    property Transparent: Boolean read FTransparent write SetTransparent;
    property AutoHeight: Boolean read FAutoHeight write SetAutoHeight;
    property PrintAsASCII: Boolean read FPrintAsASCII write FPrintAsASCII;
    property MultiPaged: Boolean read FMultiPaged write SetMultiPaged;

    procedure PFinishPrinting;
    function  PChildCount: Variant;
    function  PFindChild(AName: Variant): Variant;
    function  PChildByIndex(AIndex: Variant): Variant;
    function  PChildIndexByName(AName: Variant): Variant;
  end;


  {TrwFramedContainer has a frame around}

  TrwFramedContainer = class(TrwPrintableContainer)
  private
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);

  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;
    procedure RecalcSize; override;
    procedure AdjustClientRect(var Rect: TRect); override;
  public
    constructor Create(AOwner: TComponent); override;

  published
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines;
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle;
  end;


 {TrwBand is container for report components}

  TrwBand = class(TrwFramedContainer)
  private
    FCutable: Boolean;
    FBandType: TrwBandType;
    procedure SetBandType(Value: TrwBandType);
    procedure WriteBandType(Writer: TWriter);
    procedure ReadBandType(Reader: TReader);

  protected
    procedure SetPosition(Index: Integer; Value: Integer); override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure SetParentComponent(Value: TComponent); override;
    procedure EndPrint; override;
    procedure PrintGraphic; override;
    procedure PrintASCII; override;
    procedure RequestAlign; override;
    procedure Loaded; override;

  public
    property BandType: TrwBandType read FBandType write SetBandType;

    constructor Create(AOwner: TComponent); override;
    procedure AlignBandByForm;

  published
    property Left stored False;
    property Top stored False;
    property Cutable: Boolean read FCutable write FCutable;
    procedure PPrint;
  end;


  {TrwCustomText is abstract class for some text in report}

  TrwCustomText = class(TrwChildComponent)
  private
    FAlignment: TAlignment;
    FFont: TrwFont;
    FWordWrap: Boolean;
    FAutosize: Boolean;
    FLayout: TTextLayout;
    FTransparent: Boolean;
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    FDigitNet: TrwStrings;
    FASCIIPrintLineSequence: Integer;
    FASCIIPrintColSequence: Integer;
    FASCIISize: Integer;
    FASCIIIndent: Integer;
    FLettersCase: TrwLettersCaseType;

    procedure SetFont(Value: TrwFont);
    procedure SetAlignment(Value: TAlignment);
    procedure SetAutosize(Value: Boolean);
    procedure SetLayout(Value: TTextLayout);
    procedure SetWordWrap(Value: Boolean);
    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
    procedure SetLettersCase(Value: TrwLettersCaseType);
    procedure SetTransparent(Value: Boolean);
    procedure SetDigitNet(Value: TrwStrings);
    function  DigitNetIsNotEmpty: Boolean;
    procedure OnChangeDigitNet(Sender: TObject);
    procedure OnChangeFont(Sender: TObject);

  protected
    FText: string;
    procedure SetContainer(Value: TrwPrintableContainer); override;
    procedure SetText(const Value: string); virtual;
    function  GetText: string; virtual;
    function  TransformLettersCase(Value: string): string;
    procedure PrintGraphic; override;
    procedure PrintASCII; override;
    function  NeedPrintAsASCII: Boolean; override;
    procedure CheckAutosize; virtual;
    function  GetClientRect: TRect; override;
    procedure Loaded; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   InitializeForDesign; override;
    function    GetTextForPrint: string; virtual;
    procedure   InitRenderingInfo(const Recalculate: Boolean); override;

  published
    property Align;
    property Color;
    property Alignment: TAlignment read FAlignment write SetAlignment;
    property Text: string read GetText write SetText;
    property ASCIIIndent: Integer read FASCIIIndent write FASCIIIndent;
    property ASCIISize: Integer read FASCIISize write FASCIISize;
    property ASCIIPrintLineSequence: Integer read FASCIIPrintLineSequence write FASCIIPrintLineSequence;
    property ASCIIPrintColSequence: Integer read FASCIIPrintColSequence write FASCIIPrintColSequence;
    property Font: TrwFont read FFont write SetFont;
    property WordWrap: Boolean read FWordWrap write SetWordWrap;
    property Autosize: Boolean read FAutoSize write SetAutosize;
    property Layout: TTextLayout read FLayout write SetLayout;
    property Transparent: Boolean read FTransparent write SetTransparent;
    property LettersCase: TrwLettersCaseType read FLettersCase write SetLettersCase;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines;
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle;
    property DigitNet: TrwStrings read FDigitNet write SetDigitNet stored DigitNetIsNotEmpty;
  end;


  TrwCustomObject = class(TrwNoVisualComponent)
  private
    FObjectClassName: String;
    FObject: TObject;
    procedure SetObjectClassName(const AValue: String);

  protected
    procedure RegisterComponentEvents; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property  Left;
    property  Top;
    property  ObjectClassName: String read FObjectClassName write SetObjectClassName;
    function  PObjectInstance: Variant;
  end;



  TrwWinFormControl = class;

   {TrwFormControl is parented control for the Input Form}

  TrwFormControl = class(TrwControl)
  private
    FFont: TrwFont;
    FEnabled: Boolean;
    FRunTimeOnly: Boolean;
    FTabOrder: TTabOrder;
    FTabStop: Boolean;
    FAnchors: TAnchors;
    procedure OnChangeFont(Sender: TObject);
    procedure SetEnabled(const Value: Boolean);
    function  GetEnabled: Boolean;
    procedure SetTabOrder(const Value: TTabOrder);
    function  GetTabOrder: TTabOrder;
    procedure SetTabStop(const Value: Boolean);
    function  GetTabStop: Boolean;
    procedure SetAnchors(const Value: TAnchors);
    function  GetAnchors: TAnchors;
    function  StoreParent: Boolean;
    procedure SetFont(const Value: TrwFont);
    function  GetFont: TrwFont;

  protected
    FParent:  TrwWinFormControl;
    FText: String;
    procedure SetPosition(Index: Integer; Value: Integer); override;
    function  GetPosition(Index: Integer): Integer; override;
    procedure SetAlign(Value: TAlign); override;
    function  GetAlign: TAlign; override;
    procedure SetVisible(Value: Boolean); override;
    function  GetVisible: Boolean; override;
    procedure SetColor(Value: TColor); override;
    function  GetColor: TColor; override;
    function  GetText: string; virtual;
    procedure SetText(const Value: string); virtual;
    function  GetClientRect: TRect; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure SetCtrlParent(Value: TrwWinFormControl); virtual;
    procedure SetParentComponent(Value: TComponent); override;
    procedure RegisterComponentEvents; override;
    procedure FOnClick(Sender: TObject);
    function  GetDsgnLockState: TrwDsgnLockState; override;

    function  GetParent: IrmDesignObject; override;
    procedure SetParent(AParent: IrmDesignObject); override;
    function  ObjectType: TrwDsgnObjectType; override;
    function  GetBoundsRect: TRect; override;
    procedure SetBoundsRect(ARect: TRect); override;

    property  Text: string read GetText write SetText;
    property  Font: TrwFont read GetFont write SetFont;
    property  TabOrder: TTabOrder read GetTabOrder write SetTabOrder;
    property  TabStop: Boolean read GetTabStop write SetTabStop;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   HideRT; virtual;

    property    Anchors: TAnchors read GetAnchors write SetAnchors;

  published
    property Parent: TrwWinFormControl read FParent write SetCtrlParent stored StoreParent;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property RunTimeOnly: Boolean read FRunTimeOnly write FRunTimeOnly;

    function PTextWidth(AText: Variant): Variant;
    function PTextHeight(AText: Variant): Variant;
  end;

   {TrwFormControl is control-container for the Input Form}

  TrwWinFormControl = class(TrwFormControl)
  private
    FControls: TList;

  protected
    procedure SetPosition(Index: Integer; Value: Integer); override;
    procedure SetDesignParent(Value: TWinControl); override;
    function  GetControl(Index: Integer): TrwFormControl; virtual;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure DestroyChildren; virtual;
    procedure Loaded; override;
    function  ControlAtPos(APos: TPoint): TrwComponent; override;

    //Interface
    function  GetControlCount: Integer; override;
    function  GetControlByIndex(AIndex: Integer): IrmDesignObject; override;

  public
    property Controls[Index: Integer]: TrwFormControl read GetControl;
    property ControlCount: Integer read GetControlCount;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RemoveControl(AControl: TrwControl);
    function    InsertControl(AControl: TrwControl): Integer;

  published
    function  PControlCount: Variant;
    function  PFindControl(AName: Variant): Variant;
    function  PControlByIndex(AIndex: Variant): Variant;
    function  PControlIndexByName(AName: Variant): Variant;
  end;


   {TrwParamFormControl is control for assign report parameter}

  TrwParamFormControl = class(TrwFormControl)
  private
    FParamName: string;
    function ParamNameIsNotEmpty: Boolean;

  protected
    FParamValue: Variant;  
    function  GetParamValue: Variant; virtual;
    procedure SetParamValue(Value: Variant); virtual;

  public
    constructor Create(AOwner: TComponent); override;

    property ParamName: string read FParamName write FParamName stored ParamNameIsNotEmpty;
    property ParamValue: Variant read GetParamValue write SetParamValue stored False;
  end;


implementation

uses rwRendering, rwReport, rmReport, rwUtils, rwQBInfo, rwDsgnControls, rwDB;

  {TrwControl}

constructor TrwControl.Create(AOwner: TComponent);
begin
  inherited;
  FVisible := True;
  FAlign := alNone;
  FColor := clWhite;
  FDsgnShowCorners := False;  
end;

procedure TrwControl.SetColor(Value: TColor);
begin
  FColor := Value;
  DoSyncVisualControl(rwCPColor);
end;

procedure TrwControl.SetAlign(Value: TAlign);
begin
  FAlign := Value;
end;

function TrwControl.GetPosition(Index: Integer): Integer;
begin
  case Index of
    0: Result := FLeft;
    1: Result := FTop;
    2: Result := FWidth;
    3: Result := FHeight;
  else
    Result := 0;
  end;
end;

procedure TrwControl.SetPosition(Index: Integer; Value: Integer);
var
  l, t, w, h: Integer;
begin
  BeforeChangePos;

  l := Left;
  t := Top;
  w := Width;
  h := Height;
  case Index of
    0: l := Value;
    1: t := Value;
    2: w := Value;
    3: h := Value;
  end;

  if not (csLoading in ComponentState) then
    CheckSizeConstrains(l, t, w, h);

  FLeft := l;
  FTop := t;
  FWidth := w;
  FHeight := h;

  AfterChangePos;
end;

procedure TrwControl.SetVisible(Value: Boolean);
begin
  FVisible := Value;
end;

procedure TrwControl.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  BeforeChangePos;
  if not (csLoading in ComponentState) then
    CheckSizeConstrains(ALeft, ATop, AWidth, AHeight);
  FLeft := ALeft;
  FTop := ATop;
  FWidth := AWidth;
  FHeight := AHeight;
  AfterChangePos;
  DoSyncVisualControl(rwCPPosition);
end;

function TrwControl.GetClientRect: TRect;
begin
  Result.Left := 0;
  Result.Top := 0;
  Result.Right := Width;
  Result.Bottom := Height;
end;

procedure TrwControl.SetDsgnShowCorners(const Value: Boolean);
begin
  FDsgnShowCorners := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

function TrwControl.GetAlign: TAlign;
begin
  Result := FAlign;
end;

function TrwControl.GetColor: TColor;
begin
  Result := FColor;
end;

function TrwControl.GetVisible: Boolean;
begin
  Result := FVisible;
end;

function TrwControl.BoundsRect: TRect;
begin
  Result.Left := Left;
  Result.Top := Top;
  Result.Right := Result.Left + Width;
  Result.Bottom := Result.Top + Height;
end;

procedure TrwControl.AfterChangePos;
begin
  if GetDesigner <> nil then
    GetDesigner.Notify(rmdObjectResized, Integer(Pointer(Self as IrmDesignObject)));
end;

procedure TrwControl.BeforeChangePos;
begin
end;

function TrwControl.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
begin
  Result := True;
end;

function TrwControl.GetClientHeight: Integer;
var
  R: TRect;
begin
  R := ClientRect;
  Result := R.Bottom - R.Top;
end;

function TrwControl.GetClientWidth: Integer;
var
  R: TRect;
begin
  R := ClientRect;
  Result := R.Right - R.Left;
end;


function TrwControl.GetHeight: Integer;
begin
  Result := Height;
end;

function TrwControl.GetLeft: Integer;
begin
  Result := Left;
end;

function TrwControl.GetTop: Integer;
begin
  Result := Top;
end;

function TrwControl.GetWidth: Integer;
begin
  Result := Width;
end;

procedure TrwControl.SetHeight(AValue: Integer);
begin
  Height := AValue;
end;

procedure TrwControl.SetLeft(AValue: Integer);
begin
  Left := AValue;
end;

procedure TrwControl.SetTop(AValue: Integer);
begin
  Top := AValue;
end;

procedure TrwControl.SetWidth(AValue: Integer);
begin
  Width := AValue;
end;

{TrwPrintableControl}

procedure TrwPrintableControl.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnPrint, cEventAfterPrint]);
end;


function TrwPrintableControl.AcceptPrint: Boolean;
var
  V: Variant;
begin
  if Visible then
  begin
    Result := True;
    V := Result;
    ExecuteEventsHandler('OnPrint', [Integer(Addr(V))]);
    Result := V;    
  end
  else
    Result := False;
end;

function TrwPrintableControl.Print(Sender: TObject): Boolean;
begin
  FRendering := Sender;
  Result := AcceptPrint;

  if Result then
  begin
    if NeedPrintAsASCII then
      PrintASCII
    else if not TrwRendering(FRendering).RenderingPaper.IsASCIIResult then
      PrintGraphic;

    EndPrint;
  end;
end;

constructor TrwPrintableControl.Create(AOwner: TComponent);
begin
  inherited;

  FAlign := alNone;
  FLayerNumber := 0;
  FCompare := True;
end;


procedure TrwPrintableControl.EndPrint;
begin
  ExecuteEventsHandler('AfterPrint', []);
end;


function TrwPrintableControl.NeedPrintAsASCII: Boolean;
begin
  Result := False;
end;

procedure TrwPrintableControl.PrintASCII;
begin
end;

function TrwPrintableControl.RenderingInfo: PTrwRenderingPrintInfo;
begin
  Result := @FRenderingInfo;
end;


procedure TrwPrintableControl.InitRenderingInfo(const Recalculate: Boolean);
begin
  if Recalculate then
  begin
    FRenderingInfo.RenderingStatus := rwRMPSNone;
    FRenderingInfo.PaperLocation := Rect(0, 0, 0, 0);
    FRenderingInfo.PrevCutPosition := 0;
    FRenderingInfo.CutPosition := 0;
    FRenderingInfo.CutState := rwRMCSNone;
    FRenderingInfo.CutThreshold := 0;
    FRenderingInfo.Text := '';
    FRenderingInfo.TextHeight := 0;
  end;
end;

function TrwPrintableControl.IsRenderingReading: Boolean;
begin
  Result := FRenderingInfo.RenderingStatus = rwRMPSReading; 
end;

{TrwPrintableContainer}

constructor TrwPrintableContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FAlignLevel := 0;
  FBorderWidth := 0;
  FControls := TList.Create;
  FAutoHeight := False;
  FMultiPaged := False;
  FAutoWidth := False;
  FPrintAsASCII := False;
  FTransparent := False;
  FColor := clWhite;
  FTransparent := False;
  FDesignAlign := alNone;
  FAligningRightNow := False;
end;

destructor TrwPrintableContainer.Destroy;
begin
  BeforeDestruction;
  DestroyChildren;
  FreeAndNil(FControls);
  inherited;
end;

procedure TrwPrintableContainer.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  if LibDesignMode then
  begin
    for i := 0 to ComponentCount - 1 do
      if (Components[i] is TrwComponent) then
      begin
        if (Components[i] is TrwChildComponent) and
           (TrwChildComponent(Components[i]).Container <> Self) then
          Continue;

        Proc(Components[i]);
      end;
  end
  else
    for i := 0 to ControlCount - 1 do
      if IsLibComponent then
      begin
        if Controls[i].Owner <> Self then
          Proc(Controls[i]);
      end
      else
        Proc(Controls[i]);
end;

function TrwPrintableContainer.GetControl(Index: Integer): TrwChildComponent;
begin
  Result := TrwChildComponent(FControls[Index]);
end;

function TrwPrintableContainer.GetControlCount: Integer;
begin
  Result := FControls.Count;
end;

procedure TrwPrintableContainer.DestroyChildren;
begin
  while ControlCount > 0 do
    Controls[0].Free;
end;

procedure TrwPrintableContainer.SetTransparent(const Value: Boolean);
begin
  FTransparent := Value;
  DoSyncVisualControl(rwCPTransparent);
end;

procedure TrwPrintableContainer.Loaded;
var
  i,j: Integer;
begin
  inherited;

  for i := 0 to FControls.Count-2 do
    for j := i+1 to FControls.Count-1 do
      if Controls[i].ComponentIndex > Controls[j].ComponentIndex then
        FControls.Exchange(i, j);

  Realign;
end;

procedure TrwPrintableContainer.RecalcSize;
var
  i: Integer;
  C: TrwPrintableControl;
  h, hmax: Integer;
begin
  if AutoHeight then
  begin
    hmax := 0;
    for i := 0 to ControlCount-1 do
    begin
      C := Controls[i];
      if C.Visible then
      begin
        h := C.Top+C.Height;
        if h > hmax then
          hmax := h;
      end;
    end;
    Height := hmax;
  end;

  if AutoWidth then
  begin
    hmax := 0;
    for i := 0 to ControlCount-1 do
    begin
      C := Controls[i];
      if C.Visible then
      begin
        h := C.Left+C.Width;
        if h > hmax then
          hmax := h;
      end;
    end;
    Width := hmax;
  end;
end;

procedure TrwPrintableContainer.FinishPrinting;
begin
  FFinishPrinting := True;
end;

procedure TrwPrintableContainer.PFinishPrinting;
begin
  FinishPrinting;
end;

procedure TrwPrintableContainer.SetAutoHeight(Value: Boolean);
begin
  FAutoHeight := Value;
  if FAutoHeight then
    MultiPaged := False;
end;

procedure TrwPrintableContainer.SetAutoWidth(Value: Boolean);
begin
  FAutoWidth := Value;
end;

function TrwPrintableContainer.InsertControl(AControl: TrwChildComponent): Integer;
begin
  Result := FControls.Add(AControl);
  AlignControl(AControl);
end;

procedure TrwPrintableContainer.RemoveControl(AControl: TrwChildComponent);
var
  i: Integer;
begin
  if Assigned(FControls) then
  begin
    i := FControls.IndexOf(AControl);
    if i <> -1 then
      FControls.Delete(i);
    if not (csDestroying in ComponentState) then
      Realign;
  end;
end;


function TrwPrintableContainer.PChildByIndex(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Controls[AIndex]));
end;


function TrwPrintableContainer.PChildIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to ControlCount - 1 do
    if AnsiSameText(Controls[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;


function TrwPrintableContainer.PChildCount: Variant;
begin
  Result := ControlCount;
end;


function TrwPrintableContainer.PFindChild(AName: Variant): Variant;
var
  i: Integer;
begin
  i := PChildIndexByName(AName);
  if i = -1 then
    Result := 0
  else
    Result := PChildByIndex(i);
end;

procedure TrwPrintableContainer.PrintASCII;
begin
  PrintChildren;
end;

procedure TrwPrintableContainer.PrintGraphic;
var
  lPrnCtrl: TGraphicFrame;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  if not Transparent then
  begin
    lPrnCtrl := TGraphicFrame.Create(lRendering.RenderingPaper);
    lPrnCtrl.BoundLines := [];
    lPrnCtrl.Brush.Color := Color;
    lPrnCtrl.Transparent := Transparent;
    CalcPosRelativeBand(lPrnCtrl);
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end
  else
    lPrnCtrl := nil;

  PrintChildren;

  RecalcSize;
  if Assigned(lPrnCtrl) then
  begin
    lPrnCtrl.Height := Height;
    lPrnCtrl.Width := Width;
    if (Height = 0) or (Width = 0) then
      lPrnCtrl.Free;
  end;
end;

function TrwPrintableContainer.NeedPrintAsASCII: Boolean;
begin
  Result := PrintAsASCII;

  if Result and Assigned(Container) then
    Result := Container.NeedPrintAsASCII;
end;

function TrwPrintableContainer.AcceptPrint: Boolean;
begin
  FFinishPrinting := False;

  if AutoHeight then
    Height := 0;
  if AutoWidth then
    Width := 0;

  Result := inherited AcceptPrint;
end;

procedure TrwPrintableContainer.PrintChildren;
var
  i: Integer;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);
  for i := 0 to ControlCount - 1 do
    if Controls[i].InheritsFrom(TrwPrintableControl) then
    begin
      Controls[i].Print(lRendering);
      if FFinishPrinting then
        Break;
    end;
end;

procedure TrwPrintableContainer.AlignControl(AControl: TrwChildComponent);
var
  Rect: TRect;
  lLeft, lTop, lWidth, lHeight: Integer;
begin
  if (csDestroying in ComponentState) or (csLoading in ComponentState) or FAligningRightNow then
    Exit;

  if Aligninig then
    FAlignmentNeeded := True

  else
  begin
    FAligningRightNow := True;
    DisableAlign;
    try
      Rect := GetClientRect;
      AlignControls(AControl, Rect);
    finally
      EnableAlign;
    end;

    if Assigned(AControl) and (AControl.Align = alNone) then
      with AControl do
      begin
        lLeft := Left;
        lTop := Top ;
        lWidth := Width;
        lHeight := Height;
        ApplyConstrains(lLeft, lTop, lWidth, lHeight);
        if (lLeft <> Left) or (lTop <> Top) or (lWidth <> Width) or (lHeight <> Height) then
          SetBounds(lLeft, lTop, lWidth, lHeight);
      end;

    FAligningRightNow := False;
  end;
end;

procedure TrwPrintableContainer.DisableAlign;
begin
  Inc(FAlignLevel);
end;

procedure TrwPrintableContainer.EnableAlign;
begin
  Dec(FAlignLevel);
  Assert(FAlignLevel >= 0, 'Unexpected Error');
  if not Aligninig then
  begin
    if FAlignmentNeeded then
    begin
      FAlignmentNeeded := False;
      Realign;
    end
  end;
end;

procedure TrwPrintableContainer.AlignControls(AControl: TrwChildComponent; var Rect: TRect);
var
  AlignList: TList;

  function InsertBefore(C1, C2: TrwChildComponent; AAlign: TAlign): Boolean;
  begin
    Result := False;
    case AAlign of
      alTop: Result := C1.Top < C2.Top;
      alBottom: Result := (C1.Top + C1.Height) >= (C2.Top + C2.Height);
      alLeft: Result := C1.Left < C2.Left;
      alRight: Result := (C1.Left + C1.Width) >= (C2.Left + C2.Width);
    end;
  end;

  procedure DoPosition(Control: TrwChildComponent; AAlign: TAlign; AlignInfo: TAlignInfo);
  var
    NewLeft, NewTop, NewWidth, NewHeight: Integer;
  begin
    if AAlign = alNone then
    begin
      with Control do
      begin
        NewLeft := Left;
        NewTop := Top;
        NewWidth := Width;
        NewHeight := Height;
        SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
      end;
      Exit;
    end;

    NewWidth := Rect.Right - Rect.Left;
    if (NewWidth < 0) or (AAlign in [alLeft, alRight]) then
      NewWidth := Control.Width;
    NewHeight := Rect.Bottom - Rect.Top;
    if (NewHeight < 0) or (AAlign in [alTop, alBottom]) then
      NewHeight := Control.Height;
    NewLeft := Rect.Left;
    NewTop := Rect.Top;

    case AAlign of
      alTop: Inc(Rect.Top, NewHeight);
      alBottom:
        begin
          Dec(Rect.Bottom, NewHeight);
          NewTop := Rect.Bottom;
        end;
      alLeft: Inc(Rect.Left, NewWidth);
      alRight:
        begin
          Dec(Rect.Right, NewWidth);
          NewLeft := Rect.Right;
        end;
    end;

    Control.SetBounds(NewLeft, NewTop, NewWidth, NewHeight);

    if (Control.Width <> NewWidth) or (Control.Height <> NewHeight) then
      with Rect do
        case AAlign of
          alTop:    Dec(Top, NewHeight - Control.Height);
          alBottom: Inc(Bottom, NewHeight - Control.Height);
          alLeft:   Dec(Left, NewWidth - Control.Width);
          alRight:  Inc(Right, NewWidth - Control.Width);
          alClient:
            begin
              Inc(Right, NewWidth - Control.Width);
              Inc(Bottom, NewHeight - Control.Height);
            end;
        end;
  end;

  function Anchored(Align: TAlign; Anchors: TAnchors): Boolean;
  begin
    case Align of
      alLeft: Result := akLeft in Anchors;
      alTop: Result := akTop in Anchors;
      alRight: Result := akRight in Anchors;
      alBottom: Result := akBottom in Anchors;
      alClient: Result := Anchors = [akLeft, akTop, akRight, akBottom];
    else
      Result := False;
    end;
  end;

  procedure DoAlign(AAlign: TAlign);
  var
    I, J: Integer;
    Control: TrwChildComponent;
    AlignInfo: TAlignInfo;
  begin
    AlignList.Clear;
    if (AControl <> nil) and ((AAlign = alNone) or (AControl.Visible or AControl.IsDesignMode)) and
       (AControl.Align = AAlign) then
      AlignList.Add(AControl);

    for I := 0 to ControlCount - 1 do
    begin
      Control := Controls[I];
      if (Control.Align = AAlign) and ((AAlign = alNone) or (Control.Visible or Control.IsDesignMode)) then
      begin
        if Control = AControl then
          Continue;
        J := 0;
        while (J < AlignList.Count) and not InsertBefore(Control, TrwChildComponent(AlignList[J]), AAlign) do
          Inc(J);
        AlignList.Insert(J, Control);
      end;
    end;
    for I := 0 to AlignList.Count - 1 do
    begin
      AlignInfo.AlignList := AlignList;
      AlignInfo.ControlIndex := I;
      AlignInfo.Align := AAlign;
      DoPosition(TrwChildComponent(AlignList[I]), AAlign, AlignInfo);
    end;
  end;

  function AlignWork: Boolean;
  var
    I: Integer;
  begin
    Result := True;
    for I := ControlCount - 1 downto 0 do
      if Controls[I].Align <> alNone then
        Exit;
    Result := False;
  end;

begin
  if AlignWork then
  begin
    AlignList := TList.Create;
    try
      DoAlign(alTop);
      DoAlign(alBottom);
      DoAlign(alLeft);
      DoAlign(alRight);
      DoAlign(alClient);
      DoAlign(alNone);

      ControlsAligned;
    finally
      AlignList.Free;
    end;
  end;  

  AdjustSize;
end;


procedure TrwPrintableContainer.Realign;
begin
  AlignControl(nil);
end;

procedure TrwPrintableContainer.AdjustClientRect(var Rect: TRect);
begin
  if FBorderWidth <> 0 then
    InflateRect(Rect, -FBorderWidth, -FBorderWidth);
end;

procedure TrwPrintableContainer.ControlsAligned;
begin
end;

procedure TrwPrintableContainer.AdjustSize;
begin
  if not (csLoading in ComponentState) then
    SetBounds(Left, Top, Width, Height);
end;


procedure TrwPrintableContainer.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
var
  fl: Boolean;
begin
  fl := (AWidth <> Width) or (AHeight <> Height);
  inherited;
  if not (csLoading in Self.ComponentState) and fl and (AWidth = Width) and (AHeight = Height) then
    Realign;
end;

procedure TrwPrintableContainer.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  inherited;
  if VisualControlCreated then
    for i := 0 to ControlCount - 1 do
      if Assigned(Value) then
        if Assigned(VisualControl.Container) then
          TrwChildComponent(Controls[i]).DesignParent := VisualControl.Container
        else
          TrwChildComponent(Controls[i]).DesignParent := TWinControl(VisualControl.RealObject)
      else
        TrwChildComponent(Controls[i]).DesignParent := nil;
end;

function TrwPrintableContainer.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwTransparentContainerControl.Create(Self);
end;

procedure TrwPrintableContainer.SetCornerText(const Value: string);
begin
  FCornerText := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;


procedure TrwPrintableContainer.SetDesignAlign(const Value: TAlign);
begin
  FDesignAlign := Value;
  DoSyncVisualControl(rwCPAlign);  
end;

function TrwPrintableContainer.GetClientRect: TRect;
begin
  Result := inherited GetClientRect;
  AdjustClientRect(Result);
end;

procedure TrwPrintableContainer.SetMultiPaged(const Value: Boolean);
begin
  FMultiPaged := Value;
end;

function TrwPrintableContainer.Aligninig: Boolean;
begin
  Result := FAlignLevel > 0;
end;

{TrwBand}

constructor TrwBand.Create(AOwner: TComponent);
begin
  inherited;
  DesignAlign := alTop;
  FHeight := 70;
  FCutable := False;
end;

procedure TrwBand.SetBandType(Value: TrwBandType);
begin
  FBandType := Value;

  case Value of
    rbtPageHeader: CornerText := ResTxtBandPageHeader;
    rbtTitle:      CornerText := ResTxtBandTitle;
    rbtHeader:     CornerText := ResTxtBandHeader;
    rbtGroupHeader:CornerText := ResTxtBandGroupHeader;
    rbtDetail:     CornerText := ResTxtBandDetail;
    rbtGroupFooter:CornerText := ResTxtBandGroupFooter;
    rbtFooter:     CornerText := ResTxtBandFooter;
    rbtSummary:    CornerText := ResTxtBandSummary;
    rbtPageFooter: CornerText := ResTxtBandPageFooter;
    rbtPageStyle:  CornerText := ResTxtBandPageStyle;
    rbtSubDetail:  CornerText := ResTxtBandSubDetail;
    rbtSubSummary: CornerText := ResTxtBandSubSummary;
    rbtCustom:     CornerText := ResTxtBandCustom;
  end;

  if FBandType in [rbtPageFooter] then
    DesignAlign := alBottom;
end;

procedure TrwBand.WriteBandType(Writer: TWriter);
begin
  Writer.WriteInteger(Ord(BandType));
end;

procedure TrwBand.ReadBandType(Reader: TReader);
begin
  BandType := TrwBandType(Reader.ReadInteger);
end;


procedure TrwBand.DefineProperties(Filer: TFiler);

  function CanWrite: Boolean;
  begin
    Result := not InheritedComponent;
  end;

begin
  inherited DefineProperties(Filer);

  Filer.DefineProperty('BandType', ReadBandType, WriteBandType, CanWrite);
end;


procedure TrwBand.SetParentComponent(Value: TComponent);
begin
end;

procedure TrwBand.PPrint;
begin
  TrwRendering(TrwCustomReport(Owner.Owner).Rendering).PrintBand(BandType);
end;


procedure TrwBand.PrintASCII;
var
  i: Integer;
  h, d: string;
  lRendering: TrwRendering;
  R: TComponent;
begin
  lRendering := TrwRendering(FRendering);
  lRendering.RenderingPaper.ASCIIResult;  //Force the engine to print ASCII
  TrwReportForm(Owner).ASCIIBand.Clear;

  PrintChildren;

  R := TrwReportForm(Owner).Owner;
  while not R.InheritsFrom(TrwReport) do
    R := R.Owner;

  if AnsiSameText(TrwReport(R).ASCIIDelimiter, 'EXCEL') then
    d := Ascii2XlsDelimiter
  else
  begin
    i := Pos(#0, TrwReport(R).ASCIIDelimiter);
    if i = 0 then
      d := TrwReport(R).ASCIIDelimiter
    else
      d := Copy(TrwReport(R).ASCIIDelimiter, 1, i - 1);
  end;
  
  for i := 0 to TrwReportForm(Owner).ASCIIBand.LinesCount - 1 do
    if TrwReportForm(Owner).ASCIIBand.Lines[i] <> #0 then
    begin
      h := TrwReportForm(Owner).ASCIIBand.Lines[i];
      if (Length(d) > 0) and (Copy(h, Length(h) - Length(d) + 1, Length(d)) = d) then
        Delete(h, Length(h) - Length(d) + 1, Length(d));

      lRendering.RenderingPaper.AppendASCIILine(h);
    end;
end;


procedure TrwBand.PrintGraphic;
var
  lPrnCtrl: TGraphicFrame;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  if not(((Color = clWhite) or Transparent) and (BoundLines = [])) then
  begin
    lPrnCtrl := TGraphicFrame.Create(lRendering.RenderingPaper);
    lPrnCtrl.Brush.Color := Color;
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.Width := Width;
    lPrnCtrl.Height := Height;
    lPrnCtrl.Left := 0;
    lPrnCtrl.Top := 0;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end
  else
    lPrnCtrl := nil;

  PrintChildren;

  RecalcSize;
  if Assigned(lPrnCtrl) then
  begin
    lPrnCtrl.Height := Height;
    lPrnCtrl.Width := Width;
    if (Height = 0) or (Width = 0) then
      lPrnCtrl.Free;
  end;

  if (BandType <> rbtPageStyle) then
    if not (Assigned(lRendering.CurrentAutosizeBand) and (lRendering.CurrentAutosizeBand = Self)) then
      lRendering.RenderingPaper.MoveCurrentPosition(Height);
end;

procedure TrwBand.EndPrint;
begin
  if TrwRendering(FRendering).CurrentAutosizeBand <> Self then
    inherited;
end;

procedure TrwBand.SetPosition(Index, Value: Integer);
begin
  if not IsDesignMode or not(Index in [0, 2]) then
    inherited;
end;


procedure TrwBand.RequestAlign;
begin
  AlignControl(Self);
end;


procedure TrwBand.Loaded;
begin
  AlignBandByForm;
  inherited;
end;

procedure TrwBand.AlignBandByForm;
var
  W: Integer;
begin
  W := Round(ConvertUnit((TrwCustomReport(VirtualOwner).PageFormat.Width -
    TrwCustomReport(VirtualOwner).PageFormat.LeftMargin -
    TrwCustomReport(VirtualOwner).PageFormat.RightMargin)/ 10, utMillimeters, utScreenPixels));
  SetBounds(Left, Top, W, Height);
end;

{TrwChildComponent}

destructor TrwChildComponent.Destroy;
begin
  BeforeDestruction;
  Container := nil;
  inherited;
end;

procedure TrwChildComponent.SetContainer(Value: TrwPrintableContainer);
begin
  if (FContainer = Value) then Exit;

  if Assigned(FContainer) then
    FContainer.RemoveControl(Self);

  FContainer := Value;

  if Assigned(FContainer) then
    FContainer.InsertControl(Self);

  DoSyncVisualControl(rwCPParent);
end;

procedure TrwChildComponent.CalcPosRelativeBand(AControl: TControl);
var
  lCont: TrwPrintableContainer;
  P: TPoint;
begin
  P.X := Left;
  P.Y := Top;

  lCont := Container;
  while not (lCont is TrwBand) do
  begin
    P.X := P.X + lCont.Left;
    P.Y := P.Y + lCont.Top;
    lCont := lCont.Container;
  end;

  AControl.Left := P.X;
  AControl.Top := P.Y;
  AControl.Height := Height;
  AControl.Width := Width;
end;

procedure TrwChildComponent.SetParentComponent(Value: TComponent);
begin
  Container := TrwPrintableContainer(Value);
end;


procedure TrwChildComponent.Loaded;
begin
  if not Assigned(Container) and (Owner is TrwPrintableContainer) then
    Container := TrwPrintableContainer(Owner);

  inherited;
end;


function TrwChildComponent.StoreContainer: Boolean;
begin
  Result := FWritingOnlySelf;
end;

procedure TrwChildComponent.RequestAlign;
begin
  if Assigned(Container) then
    Container.AlignControl(Self);
end;

constructor TrwChildComponent.Create(AOwner: TComponent);
begin
  inherited;
  FAlignmentNeeded := False;
end;


procedure TrwChildComponent.SetAlign(Value: TAlign);
begin
  inherited;
  RequestAlign;
end;

procedure TrwChildComponent.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if (ALeft <> Left) or (ATop <> Top) or (AWidth <> Width) or (AHeight <> Height) then
  begin
    ApplyConstrains(ALeft, ATop, AWidth, AHeight);
    inherited;
    RequestAlign;
  end;
end;

procedure TrwChildComponent.ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer);
var
  d: Integer;
begin
  if Assigned(Container) and (Container.BorderWidth <> 0) then
  begin
    if ALeft < Container.BorderWidth then
      ALeft := Container.BorderWidth;

    if ATop < Container.BorderWidth then
      ATop := Container.BorderWidth;

    if ALeft + AWidth > (Container.Width - Container.BorderWidth) then
    begin
      d := ALeft + AWidth - (Container.Width - Container.BorderWidth);
      if AWidth <> Width then
        AWidth := AWidth - d
      else
        ALeft := ALeft - d;
    end;

    if ATop + AHeight > (Container.Height - Container.BorderWidth) then
    begin
      d := ATop + AHeight - (Container.Height - Container.BorderWidth);
      if AHeight <> Height then
        AHeight := AHeight - d
      else
        ATop := ATop - d;
    end;
  end;
end;

procedure TrwChildComponent.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  inherited;
  if LibDesignMode then
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrwNoVisualComponent then
        TrwComponent(Components[i]).DesignParent := Value;
end;


procedure TrwChildComponent.SetPosition(Index, Value: Integer);
var
  ALeft, ATop, AWidth, AHeight: Integer;
begin
  if IsRenderingReading then
    inherited

  else
  begin
    ALeft := FLeft;
    ATop := FTop;
    AWidth := FWidth;
    AHeight := FHeight;

    case Index of
      0: ALeft := Value;
      1: ATop := Value;
      2: AWidth := Value;
      3: AHeight := Value;
    end;

    ApplyConstrains(ALeft, ATop, AWidth, AHeight);

    case Index of
      0: Value := ALeft;
      1: Value := ATop;
      2: Value := AWidth;
      3: Value := AHeight;
    end;

    case Index of
      0: SetBounds(Value, FTop, FWidth, FHeight);
      1: SetBounds(FLeft, Value, FWidth, FHeight);
      2: SetBounds(FLeft, FTop, Value, FHeight);
      3: SetBounds(FLeft, FTop, FWidth, Value);
    end;

    RequestAlign;
  end;
end;


function TrwChildComponent.GetDsgnLockState: TrwDsgnLockState;
begin
  if Assigned(Container) and (Container.DsgnLockState <> rwLSUnlocked) then
    Result := Container.DsgnLockState
  else
    Result :=  inherited GetDsgnLockState;
end;

{TrwCustomText}

constructor TrwCustomText.Create(AOwner: TComponent);
begin
  inherited;
  FDsgnShowCorners := True;

  Width := 65;
  Height := 17;
  Color := clWhite;
  FAlignment := taLeftJustify;
  FWordWrap := False;
  FAutosize := False;
  FLayout := tlTop;
  FTransparent := False;
  FLettersCase := rwNoChange;
  FBoundLines := [];
  FBoundLinesColor := clBlack;
  FBoundLinesWidth := 1;
  FBoundLinesStyle := psSolid;

  FFont := TrwFont.Create(Self);
  FFont.OnChange := OnChangeFont;
  FDigitNet := TrwStrings.Create;
  FDigitNet.OnChange := OnChangeDigitNet;
end;

destructor TrwCustomText.Destroy;
begin
  FreeAndNil(FFont);
  FreeAndNil(FDigitNet);
  inherited;
end;

procedure TrwCustomText.SetAutosize(Value: Boolean);
begin
  FAutosize := Value;
  CheckAutosize;
end;

procedure TrwCustomText.SetWordWrap(Value: Boolean);
begin
  FWordWrap := Value;
  CheckAutosize;  
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetTransparent(Value: Boolean);
begin
  FTransparent := Value;
  DoSyncVisualControl(rwCPTransparent);
end;

procedure TrwCustomText.SetDigitNet(Value: TrwStrings);
begin
  FDigitNet.Assign(Value);
end;

procedure TrwCustomText.SetText(const Value: string);
begin
  FText := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetLettersCase(Value: TrwLettersCaseType);
begin
  FLettersCase := Value;
  SetText(Text);
end;

function TrwCustomText.TransformLettersCase(Value: string): string;
begin
  case FLettersCase of
    rwUpperCase: Result := AnsiUpperCase(Value);
    rwLowerCase: Result := AnsiLowerCase(Value);
  else
    Result := Value;
  end;
end;

procedure TrwCustomText.SetFont(Value: TrwFont);
begin
  Font.Assign(Value);
end;

procedure TrwCustomText.SetAlignment(Value: TAlignment);
begin
  FAlignment := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetLayout(Value: TTextLayout);
begin
  FLayout := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  CheckAutosize;  
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  CheckAutosize;  
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomText.InitializeForDesign;
begin
  inherited;
  Text := Name;
  Font.Name := 'Arial';
  Font.Size := 10;
  Font.Color := clBlack;
  Color := clWhite;
  if Text = '' then
    Text := 'Sample Text';
  Autosize := not Autosize;
  Autosize := not Autosize;
end;


function TrwCustomText.DigitNetIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (DigitNet.Count > 0);
end;

procedure TrwCustomText.SetContainer(Value: TrwPrintableContainer);
var
  P: TrwPrintableContainer;
  i, c: Integer;
begin
  inherited;

  if IsDesignMode then
  begin
    P := Value;
    while Assigned(P) and not (P is TrwBand) do
    begin
      if P.PrintAsASCII then
        break;
      P := P.Container;
    end;

    if (ASCIIPrintLineSequence = 0) and Assigned(P) and P.PrintAsASCII then
      ASCIIPrintLineSequence := 1;

    if (ASCIIPrintColSequence = 0) and Assigned(P) and P.PrintAsASCII then
    begin
      c := 0;
      for i := 0 to P.ControlCount - 1 do
        if (P.Controls[i] is TrwCustomText) and
           (TrwCustomText(P.Controls[i]).ASCIIPrintColSequence > c) then
          c := TrwCustomText(P.Controls[i]).ASCIIPrintColSequence;

       ASCIIPrintColSequence := c + 1;
    end;
  end;
end;



function TrwCustomText.NeedPrintAsASCII: Boolean;
begin
  Result := Container.NeedPrintAsASCII and (ASCIIPrintLineSequence <> 0) and (ASCIIPrintColSequence <> 0);
end;

procedure TrwCustomText.PrintASCII;
var
  h: string;
  d, i: Integer;
  R: TComponent;
  Delim, Surr: String;
begin
  h := GetTextForPrint;

  if ASCIISize <> 0 then
    case Alignment of
      taLeftJustify:
        if (Length(h) > ASCIISize) then
          h := Copy(h, 1, ASCIISize)
        else if (Length(h) < ASCIISize) then
          h := h + StringOfChar(' ', ASCIISize - Length(h));

      taRightJustify:
        if (Length(h) > ASCIISize) then
          h := Copy(h, Length(h) - ASCIISize, ASCIISize)
        else if (Length(h) < ASCIISize) then
          h := StringOfChar(' ', ASCIISize - Length(h)) + h;

      taCenter:
        if (Length(h) > ASCIISize) then
          h := Copy(h, (Length(h) - ASCIISize) div 2, ASCIISize)
        else if (Length(h) < ASCIISize) then
        begin
          d := (ASCIISize - Length(h)) div 2;
          h := StringOfChar(' ', d) + h + StringOfChar(' ', ASCIISize - Length(h) - d);
        end;
    end;

  if ASCIIIndent > 0 then
    h := StringOfChar(' ', ASCIIIndent) + h;

  R := TrwReportForm(Owner).Owner;
  while not R.InheritsFrom(TrwReport) do
    R := R.Owner;

  if TrwReport(R).ASCIIDelimiter <> '' then
  begin
    if AnsiSameText(TrwReport(R).ASCIIDelimiter, 'EXCEL') then
      h := Ascii2XlsQualifier + h + Ascii2XlsQualifier + Ascii2XlsDelimiter
    else
    begin
      i := Pos(#0, TrwReport(R).ASCIIDelimiter);
      if i > 0 then
      begin
        Delim := Copy(TrwReport(R).ASCIIDelimiter, 1, i - 1);
        Surr := Copy(TrwReport(R).ASCIIDelimiter, i + 1, 100);
        h := Surr + h + Surr + Delim;
      end
      else
        h := h + TrwReport(R).ASCIIDelimiter;
    end;
  end;

  TrwReportForm(Owner).ASCIIBand.PrinText(ASCIIPrintLineSequence, ASCIIPrintColSequence, h);
end;

procedure TrwCustomText.PrintGraphic;
var
  lPrnCtrl: TFrameLabel;
  lRendering: TrwRendering;
  h: String;
begin
  h := GetTextForPrint;
  if (Length(h) > 0) or not Autosize then
  begin
    lRendering := TrwRendering(FRendering);
    lPrnCtrl := TFrameLabel.Create(lRendering.RenderingPaper);
    lPrnCtrl.Autosize := False;
    lPrnCtrl.WordWrap := WordWrap;
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.Alignment := Alignment;
    lPrnCtrl.Font.Assign(Font);
    lPrnCtrl.Layout := Layout;
    lPrnCtrl.Caption := h;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.Color := Color;
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.DigitNet := DigitNet;
    CalcPosRelativeBand(lPrnCtrl);

    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end;
end;

function TrwCustomText.GetText: string;
begin
  Result := FText;
end;

procedure TrwCustomText.OnChangeDigitNet(Sender: TObject);
begin
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;


procedure TrwCustomText.OnChangeFont(Sender: TObject);
begin
  CheckAutosize;
  DoSyncVisualControl(rwCPFont);
end;

procedure TrwCustomText.CheckAutosize;
var
  Tinf: TrwTextDrawInfoRec;
begin
  if not Autosize or (csLoading in ComponentState) or IsRenderingReading then
    Exit;

  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;
  Tinf.ZoomRatio := 1;
  Tinf.Text := GetTextForPrint;
  if (Tinf.Text = '') and IsDesignMode then
    Tinf.Text := '    ';
  Tinf.Font := Font;
  Tinf.DigitNet := FDigitNet.Text;
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := GetClientRect;

  DrawTextImage(@Tinf);

  if rclLeft in BoundLines then
    Inc(Tinf.TextRect.Right, BoundLinesWidth + 2);
  if rclRight in BoundLines then
    Inc(Tinf.TextRect.Right, BoundLinesWidth + 2);
  if rclTop in BoundLines then
    Inc(Tinf.TextRect.Bottom, BoundLinesWidth);
  if rclBottom in BoundLines then
    Inc(Tinf.TextRect.Bottom, BoundLinesWidth);

  case Alignment of
    taLeftJustify:   Tinf.TextRect.Left := Left;
    taRightJustify:  Tinf.TextRect.Left := Left - (Tinf.TextRect.Right - Width);
    taCenter:        Tinf.TextRect.Left := Left - (Tinf.TextRect.Right - Width) div 2;
  end;

  SetBounds(Tinf.TextRect.Left, Top, Tinf.TextRect.Right, Tinf.TextRect.Bottom);
end;

function TrwCustomText.GetTextForPrint: string;
begin
  Result := TransformLettersCase(Text);
end;

procedure TrwCustomText.InitRenderingInfo(const Recalculate: Boolean);
var
  Tinf: TrwTextDrawInfoRec;
begin
  inherited;

  Tinf.CalcOnly := True;
  Tinf.Canvas := nil;  
  Tinf.ZoomRatio := cVirtualCanvasRes / cScreenCanvasRes;
  Tinf.Text := StringReplace(GetTextForPrint, #13#10, #13, [rfReplaceAll]);
  Tinf.Font := Font;
  Tinf.LineHeight := -Font.Height;
  if Transparent then
    Tinf.Color := clNone
  else
    Tinf.Color := Color;
  Tinf.DigitNet := FDigitNet.Text;
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := GetClientRect;
  OffsetRect(Tinf.TextRect, -Tinf.TextRect.Left, -Tinf.TextRect.Top);

  if Recalculate then
    DrawTextImage(@Tinf);

  if rclLeft in BoundLines then
    Inc(Tinf.TextRect.Right, BoundLinesWidth + 2);
  if rclRight in BoundLines then
    Inc(Tinf.TextRect.Right, BoundLinesWidth + 2);
  if rclTop in BoundLines then
    Inc(Tinf.TextRect.Bottom, BoundLinesWidth);
  if rclBottom in BoundLines then
    Inc(Tinf.TextRect.Bottom, BoundLinesWidth);

  FRenderingInfo.Text := Tinf.Text;
  FRenderingInfo.TextHeight := Tinf.LineHeight;

  if Tinf.TextRect.Bottom - Tinf.TextRect.Top = Tinf.LineHeight then
    FRenderingInfo.CutThreshold := Height
  else
    FRenderingInfo.CutThreshold := Tinf.LineHeight;
end;

function TrwCustomText.GetClientRect: TRect;
begin
  Result := inherited GetClientRect;

  if rclLeft in BoundLines then
    Inc(Result.Left, BoundLinesWidth + 2);
  if rclRight in BoundLines then
    Dec(Result.Right, BoundLinesWidth + 2);
  if rclTop in BoundLines then
    Inc(Result.Top, BoundLinesWidth);
  if rclBottom in BoundLines then
    Dec(Result.Bottom, BoundLinesWidth);
end;

procedure TrwCustomText.Loaded;
begin
  inherited;
  if Autosize then
    CheckAutosize;
end;

{TrwFormControl}

constructor TrwFormControl.Create(AOwner: TComponent);
begin
  inherited;
  FFont := TrwFont.Create(Self);
  FFont.OnChange := OnChangeFont;
  FText := '';
  FRunTimeOnly := False;
  FEnabled := True;
  FTabOrder := -1;
  FTabStop := True;
  FAnchors := [akTop, akLeft];
end;


destructor TrwFormControl.Destroy;
begin
  BeforeDestruction;
  Parent := nil;
  FreeAndNil(FFont);
  inherited;
end;

procedure TrwFormControl.SetParentComponent(Value: TComponent);
begin
  Parent := TrwWinFormControl(Value);
end;

procedure TrwFormControl.SetCtrlParent(Value: TrwWinFormControl);
begin
  if FParent = Value then
    Exit;

  if Assigned(FParent) then
    FParent.RemoveControl(Self);

  FParent := Value;

  if Assigned(FParent) then
    FParent.InsertControl(Self);

  if Assigned(FParent) and FParent.VisualControlCreated then
    VisualControl; // create visual control

  DoSyncVisualControl(rwCPParent);
  
  TabOrder := FTabOrder;
end;


procedure TrwFormControl.SetVisible(Value: Boolean);
begin
  if not IsDesignMode and VisualControlCreated then
  begin
    if csDesigning in VisualControl.RealObject.ComponentState then
      VisualControl.SetDesigning(False, False);
    VisualControl.RealObject.Visible := Value;
    GetVisible;
  end
  else
    inherited;
end;


procedure TrwFormControl.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventAfterCreate, cEventOnClick]);
end;

procedure TrwFormControl.FOnClick(Sender: TObject);
begin
  ExecuteEventsHandler('OnClick', []);
end;

procedure TrwFormControl.SetEnabled(const Value: Boolean);
begin
  if not IsDesignMode and VisualControlCreated then
  begin
    if csDesigning in VisualControl.RealObject.ComponentState then
      VisualControl.SetDesigning(False, False);
    VisualControl.RealObject.Enabled := Value;
    GetEnabled;
  end
  else
    FEnabled := Value;
end;


procedure TrwFormControl.HideRT;
begin
  Visible := False;
end;


procedure TrwFormControl.SetTabOrder(const Value: TTabOrder);
begin
  if VisualControlCreated then
  begin
    if VisualControl.RealObject is TWinControl then
    begin
      TWinControl(VisualControl.RealObject).TabOrder := Value;
      GetTabOrder;
    end;
  end  
  else
    FTabOrder := Value;
end;


function TrwFormControl.GetTabStop: Boolean;
begin
  if VisualControlCreated then
    FTabStop := Boolean(GetOrdProp(VisualControl.RealObject, 'TabStop'));
  Result := FTabStop;
end;

procedure TrwFormControl.SetTabStop(const Value: Boolean);
begin
  if VisualControlCreated then
  begin
    SetOrdProp(VisualControl.RealObject, 'TabStop', Ord(Value));
    GetTabStop;
  end
  else
    FTabStop := Value;
end;


function TrwFormControl.GetTabOrder: TTabOrder;
begin
  if VisualControlCreated then
  begin
    if VisualControl.RealObject is TWinControl then
      FTabOrder := TWinControl(VisualControl.RealObject).TabOrder
    else
      FTabOrder := -1;
  end;
  Result := FTabOrder;
end;


function TrwFormControl.GetAnchors: TAnchors;
begin
  if VisualControlCreated then
    FAnchors := VisualControl.RealObject.Anchors;
  Result := FAnchors;  
end;

procedure TrwFormControl.SetAnchors(const Value: TAnchors);
begin
  if VisualControlCreated then
  begin
    VisualControl.RealObject.Anchors := Value;
    GetAnchors;
  end
  else
    FAnchors := Value;
end;

function TrwFormControl.StoreParent: Boolean;
begin
  Result := FWritingOnlySelf;
end;

procedure TrwFormControl.SetDesignParent(Value: TWinControl);
var
  i: Integer;
begin
  inherited;
  if LibDesignMode then
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TrwNoVisualComponent then
        TrwComponent(Components[i]).DesignParent := Value;
end;


function TrwFormControl.GetClientRect: TRect;
begin
  if VisualControlCreated then
  begin
    Width;
    Height;
  end;

  Result := inherited GetClientRect;
end;

function TrwFormControl.GetPosition(Index: Integer): Integer;
begin
  if VisualControlCreated then
    case Index of
      0: FLeft   := VisualControl.RealObject.Left;
      1: FTop    := VisualControl.RealObject.Top;
      2: FWidth  := VisualControl.RealObject.Width;
      3: FHeight := VisualControl.RealObject.Height;
    end;

  Result := inherited GetPosition(Index);
end;

procedure TrwFormControl.SetAlign(Value: TAlign);
begin
  if VisualControlCreated then
  begin
    VisualControl.RealObject.Align := Value;
    GetAlign;
    AfterChangePos;
  end
  else
    inherited SetAlign(Value);
end;

procedure TrwFormControl.SetPosition(Index, Value: Integer);
var
  l, t, w, h: Integer;
begin
  if VisualControlCreated then
  begin
    BeforeChangePos;

    l := VisualControl.RealObject.Left;
    t := VisualControl.RealObject.Top;
    w := VisualControl.RealObject.Width;
    h := VisualControl.RealObject.Height;
    case Index of
      0: l := Value;
      1: t := Value;
      2: w := Value;
      3: h := Value;
    end;
    CheckSizeConstrains(l, t, w, h);
    VisualControl.RealObject.SetBounds(l, t, w, h);
    GetPosition(Index);
    AfterChangePos;
  end
  else
    inherited SetPosition(Index, Value);
end;

procedure TrwFormControl.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if VisualControlCreated then
  begin
    VisualControl.RealObject.SetBounds(ALeft, ATop, AWidth, AHeight);
    ALeft := VisualControl.RealObject.Left;
    ATop := VisualControl.RealObject.Top;
    AWidth := VisualControl.RealObject.Width;
    AHeight := VisualControl.RealObject.Height;
  end;

  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
end;

function TrwFormControl.GetAlign: TAlign;
begin
  if VisualControlCreated then
    FAlign := VisualControl.RealObject.Align;

  Result := inherited GetAlign;
end;

function TrwFormControl.GetVisible: Boolean;
begin
  if not IsDesignMode and VisualControlCreated then
    FVisible := VisualControl.RealObject.Visible;

  Result := inherited GetVisible;
end;

function TrwFormControl.GetEnabled: Boolean;
begin
  if not IsDesignMode and VisualControlCreated then
    FEnabled := VisualControl.RealObject.Enabled;
  Result := FEnabled;
end;

procedure TrwFormControl.OnChangeFont(Sender: TObject);
begin
  BeforeChangePos;
  if VisualControlCreated then
    FFont.AssignTo(TPersistent(GetObjectProp(VisualControl.RealObject, 'Font')));
  AfterChangePos;
end;


function TrwFormControl.GetFont: TrwFont;
begin
  if VisualControlCreated then
  begin
    FFont.OnChange := nil;
    try
      FFont.Assign(TPersistent(GetObjectProp(VisualControl.RealObject, 'Font')));
    finally
      FFont.OnChange := OnChangeFont;
    end;
  end;
  
  Result := FFont;
end;

procedure TrwFormControl.SetFont(const Value: TrwFont);
begin
  FFont.Assign(Value);
end;

function TrwFormControl.GetColor: TColor;
begin
  if VisualControlCreated then
    FColor := TColor(GetOrdProp(VisualControl.RealObject, 'Color'));

  Result := FColor;
end;

procedure TrwFormControl.SetColor(Value: TColor);
begin
  if VisualControlCreated then
  begin
    SetOrdProp(VisualControl.RealObject, 'Color', Value);
    GetColor;
  end
  else
    FColor := Value;
end;

function TrwFormControl.GetText: string;
begin
  Result := FText;
end;

procedure TrwFormControl.SetText(const Value: string);
begin
  FText := Value;
end;

function TrwFormControl.PTextWidth(AText: Variant): Variant;
var
  C: TCanvas;
begin
  C := TCanvas.Create;
  try
    C.Handle := GetDC(0);
    C.Font.Assign(Font);
    Result := C.TextWidth(AText);
  finally
    ReleaseDC(0, C.Handle);
    FreeAndNil(C);
  end;
end;

function TrwFormControl.PTextHeight(AText: Variant): Variant;
var
  C: TCanvas;
begin
  C := TCanvas.Create;
  try
    C.Handle := GetDC(0);
    C.Font.Assign(Font);
    Result := C.TextHeight(AText);
  finally
    ReleaseDC(0, C.Handle);
    FreeAndNil(C);
  end;
end;


function TrwFormControl.GetDsgnLockState: TrwDsgnLockState;
begin
  if Assigned(Parent) and (Parent.DsgnLockState <> rwLSUnlocked) then
    Result := Parent.DsgnLockState
  else
    Result :=  inherited GetDsgnLockState;
end;

function TrwFormControl.GetParent: IrmDesignObject;
begin
  Result := Parent;
end;

function TrwFormControl.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTInputForm;
end;


procedure TrwFormControl.SetBoundsRect(ARect: TRect);
begin
  SetBounds(ARect.Left, ARect.Top, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);
end;

function TrwFormControl.GetBoundsRect: TRect;
begin
  Result := BoundsRect;
end;

procedure TrwFormControl.SetParent(AParent: IrmDesignObject);
begin
  if Assigned(AParent) then
  begin
    Parent := TrwWinFormControl(AParent.RealObject);
    if IsDesignMode then
      SetDesignParent(TWinControl(Parent.VisualControl.RealObject));
  end
  else
    Parent := nil;
end;

{TrwWinFormControl}

constructor TrwWinFormControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FControls := TList.Create;
end;

destructor TrwWinFormControl.Destroy;
begin
  BeforeDestruction;
  DestroyChildren;
  FreeAndNil(FControls);
  inherited;
end;

function TrwWinFormControl.GetControl(Index: Integer): TrwFormControl;
begin
  Result := TrwFormControl(FControls[Index]);
end;

function TrwWinFormControl.GetControlCount: Integer;
begin
  Result := FControls.Count;
end;

procedure TrwWinFormControl.DestroyChildren;
begin
  while ControlCount > 0 do
    Controls[0].Free;
end;

procedure TrwWinFormControl.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
  C: TComponent;
begin
  if RootOwner is TrwReport then
  begin
    // RW branch
    if LibDesignMode then
    begin
      for i := 0 to ComponentCount - 1 do
        if (Components[i] is TrwComponent) then
        begin
          if (Components[i] is TrwFormControl) and
             (TrwFormControl(Components[i]).Parent <> Self) then
            Continue;

          Proc(Components[i]);
        end;
    end
    else
      for i := 0 to ControlCount - 1 do
        if IsLibComponent then
        begin
          if Controls[i].Owner <> Self then
            Proc(Controls[i]);
        end
        else
          Proc(Controls[i]);
  end

  else
  begin
    // RM branch
    if LibDesignMode or IsVirtualOwner then
      for i := 0 to ComponentCount - 1 do
      begin
        C := Components[i];
        if (C is TrwFormControl) and (TrwFormControl(C).Parent <> Self) then
          Continue;
        Proc(C);
      end;

    for i := 0 to ControlCount - 1 do
    begin
      C := Controls[i];
      if C.Owner <> Self then
        Proc(C);
    end;
  end;
end;


procedure TrwWinFormControl.Loaded;
var
  i,j: Integer;
begin
  inherited;

  for i := 0 to FControls.Count - 2 do
    for j := i+1 to FControls.Count - 1 do
      if Controls[i].ComponentIndex > Controls[j].ComponentIndex then
        FControls.Exchange(i, j);
end;


function TrwWinFormControl.InsertControl(AControl: TrwControl): Integer;
begin
  Result := FControls.Add(AControl);
end;


procedure TrwWinFormControl.RemoveControl(AControl: TrwControl);
var
  i: Integer;
begin
  if Assigned(FControls) then
  begin
    i := FControls.IndexOf(AControl);
    if i <> -1 then
      FControls.Delete(i);
  end;
end;


function TrwWinFormControl.PControlByIndex(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Controls[AIndex]));
end;


function TrwWinFormControl.PControlCount: Variant;
begin
  Result := ControlCount;
end;


function TrwWinFormControl.PControlIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to ControlCount - 1 do
    if AnsiSameText(Controls[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;


function TrwWinFormControl.PFindControl(AName: Variant): Variant;
var
  i: Integer;
begin
  i := PControlIndexByName(AName);
  if i = -1 then
    Result := 0
  else
    Result := PControlByIndex(i);
end;

procedure TrwWinFormControl.SetDesignParent(Value: TWinControl);
var
  i: Integer;
  lTabOrderList: TList;

  function SortTabOrder(Item1, Item2: Pointer): Integer;
  begin
    if TrwFormControl(Item1).FTabOrder < TrwFormControl(Item2).FTabOrder then
      Result := -1
    else if TrwFormControl(Item1).FTabOrder > TrwFormControl(Item2).FTabOrder then
      Result := 1
    else
      Result := 0;
  end;

begin
  lTabOrderList := nil;
  try
    // GetTab Order Info
    if not Assigned(DesignParent) and Assigned(Value) then
    begin
      lTabOrderList := TList.Create;
      lTabOrderList.Count := ControlCount;
      for i := 0 to ControlCount - 1 do
        lTabOrderList[i] := Pointer(Controls[i]);
      lTabOrderList.Sort(@SortTabOrder);
    end;

    inherited;

    if VisualControlCreated then
    begin
      for i := 0 to ControlCount - 1 do
        if Assigned(Value) then
          if Assigned(VisualControl.Container) then
            TrwFormControl(Controls[i]).DesignParent := VisualControl.Container
          else
            TrwFormControl(Controls[i]).DesignParent := TWinControl(VisualControl.RealObject)
        else
          TrwFormControl(Controls[i]).DesignParent := nil;

      if Assigned(Value) and Assigned(VisualControl.Container) then
        VisualControl.Container.Realign;
    end;

    if Assigned(lTabOrderList) then
      for i := 0 to lTabOrderList.Count - 1 do
        TrwFormControl(lTabOrderList[i]).TabOrder := TrwFormControl(lTabOrderList[i]).FTabOrder;

  finally
    FreeAndNil(lTabOrderList);
  end;
end;


function TrwWinFormControl.ControlAtPos(APos: TPoint): TrwComponent;
var
  i: Integer;
  R: TRect;
  C, Cin: TrwFormControl;
begin
  Cin := nil;

  for i := ControlCount - 1  downto 0 do
  begin
    C := Controls[i];
    if C is TrwWinFormControl then
    begin
      R := C.BoundsRect;
      if PtInRect(R, APos) then
      begin
        Cin := C;
        Dec(APos.X, R.Left);
        Dec(APos.Y, R.Top);
        C := TrwFormControl(TrwWinFormControl(C).ControlAtPos(APos));
        if Assigned(C) then
          Cin := C;
        break;
      end;
    end;
  end;

  if not Assigned(Cin) then
    for i := ControlCount - 1  downto 0 do
    begin
      C := Controls[i];
      if (C is TrwFormControl) and not (C is TrwWinFormControl) then
      begin
        R := C.BoundsRect;
        if PtInRect(R, APos) then
        begin
          Cin := C;
          break;
        end;
      end;
    end;

  Result := Cin;
end;

function TrwWinFormControl.GetControlByIndex(AIndex: Integer): IrmDesignObject;
begin
  Result := Controls[AIndex];
end;



procedure TrwWinFormControl.SetPosition(Index, Value: Integer);
var
  w, h, i: Integer;
  Ctrl: TrwFormControl;
begin
  if VisualControlCreated then
    inherited

  else
  begin
    w := Width;
    h := Height;

    inherited;

    w := Width - w;
    h := Height - h;

    if (w <> 0) or (h <> 0) then
      for i := 0 to ControlCount - 1 do
      begin
        Ctrl := TrwFormControl(Controls[i]);

        if w <> 0 then
          if not (akLeft in Ctrl.Anchors) and not (akRight in Ctrl.Anchors) then
            Ctrl.Left := Ctrl.Left + w div 2
          else if not (akLeft in Ctrl.Anchors) and (akRight in Ctrl.Anchors) then
            Ctrl.Left := Ctrl.Left + w
          else if (akLeft in Ctrl.Anchors) and (akRight in Ctrl.Anchors) then
            Ctrl.Width := Ctrl.Width + w;

        if h <> 0 then
          if not (akTop in Ctrl.Anchors) and not (akBottom in Ctrl.Anchors) then
            Ctrl.Top := Ctrl.Top + h div 2
          else if not (akTop in Ctrl.Anchors) and (akBottom in Ctrl.Anchors) then
            Ctrl.Left := Ctrl.Top + h
          else if (akTop in Ctrl.Anchors) and (akBottom in Ctrl.Anchors) then
            Ctrl.Height := Ctrl.Height + h;
      end;
  end;
end;

{ TrwParamFormControl }

constructor TrwParamFormControl.Create(AOwner: TComponent);
begin
  inherited;
  FParamValue := Null;
end;

function TrwParamFormControl.GetParamValue: Variant;
begin
  Result := FParamValue;
end;

function TrwParamFormControl.ParamNameIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(FParamName) > 0);
end;

procedure TrwParamFormControl.SetParamValue(Value: Variant);
begin
  FParamValue := Value;
end;


{ TrwCustomObject }

constructor TrwCustomObject.Create(AOwner: TComponent);
begin
  inherited;
  FObject := nil;
  FObjectClassName := '';
end;


destructor TrwCustomObject.Destroy;
begin
  FreeAndNil(FObject);
  inherited;
end;


function TrwCustomObject.PObjectInstance: Variant;
var
  lPersistentClass: TPersistentClass;
  Cl: IrwClass;
begin
  if not Assigned(FObject) then
    if Length(FObjectClassName) > 0 then
    begin
      lPersistentClass := GetClass(FObjectClassName);

      if Assigned(lPersistentClass) then
      begin
        if lPersistentClass.InheritsFrom(TrwComponent) then
          FObject := TrwComponentClass(lPersistentClass).Create(Self)
        else
        begin
          if (lPersistentClass = TrwFont) or (lPersistentClass = TFont) then
            FObject := TrwFont.Create(Self)
          else if lPersistentClass = TrwStrings then
            FObject := TrwStrings.Create
          else if lPersistentClass = TrwQBQuery then
            FObject := TrwQBQuery.Create(nil)
          else if lPersistentClass = TrwFields then
            FObject := TrwFields.Create(nil, TrwField);
        end;
      end

      else
      begin
        Cl := rwRTTIInfo.FindClass(FObjectClassName);
        if Assigned(Cl) then
        begin
          FObject := Cl.CreateObjectInstance;
          InsertComponent(TComponent(FObject));
        end;
      end;

      if Assigned(FObject) then
        ExecuteEventsHandler('AfterCreateObject', [])
      else
        raise ErwException.CreateFmt(ResErrCreateCustomObject, [FObjectClassName]);
    end;


  Result := Integer(Pointer(FObject));
end;


procedure TrwCustomObject.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventAfterCreateObject]);
end;

procedure TrwCustomObject.SetObjectClassName(const AValue: String);
begin
  FreeAndNil(FObject);

  if (Length(AValue) > 0) and (rwRTTIInfo.FindClass(AValue) = nil) then
    raise ErwException.CreateFmt(ResErrWrongClass, [AValue]);

  if SameText(AValue, 'TFont') then  // temporary
    FObjectClassName := 'TrwFont'
  else
    FObjectClassName := AValue;
end;


{ TrwFramedContainer }

procedure TrwFramedContainer.AdjustClientRect(var Rect: TRect);
var
  d: Integer;
begin
  inherited;

  if BoundLinesWidth > FBorderWidth then
  begin
    d := BoundLinesWidth - FBorderWidth;
    if rclLeft in BoundLines then
      Rect.Left := Rect.Left + d;
    if rclRight in BoundLines then
      Rect.Right := Rect.Right - d;
    if rclTop in BoundLines then
      Rect.Top := Rect.Top + d;
    if rclBottom in BoundLines then
      Rect.Bottom := Rect.Bottom - d;
  end;
end;

constructor TrwFramedContainer.Create(AOwner: TComponent);
begin
  inherited;
  FBoundLines := [];
  FBoundLinesWidth := 1;
  FBoundLinesColor := clBlack;
  FBoundLinesStyle := psSolid;
end;

function TrwFramedContainer.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFramedTransparentContainerControl.Create(Self);
end;

procedure TrwFramedContainer.PrintGraphic;
var
  lPrnCtrl: TGraphicFrame;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  if not((BoundLines = []) and Transparent) then
  begin
    lPrnCtrl := TGraphicFrame.Create(lRendering.RenderingPaper);
    lPrnCtrl.Brush.Color := Color;
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.BoundLines := BoundLines;
    CalcPosRelativeBand(lPrnCtrl);
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end
  else
    lPrnCtrl := nil;

  PrintChildren;

  RecalcSize;
  if Assigned(lPrnCtrl) then
  begin
    lPrnCtrl.Height := Height;
    lPrnCtrl.Width := Width;
    if (Height = 0) or (Width = 0) then
      lPrnCtrl.Free;
  end;
end;

procedure TrwFramedContainer.RecalcSize;
var
  i: Integer;
  C: TrwPrintableControl;
  h, hmax: Integer;
  hb, vr: Integer;
begin
  if rclBottom in BoundLines then
    hb := BoundLinesWidth
  else
    hb := 0;
  if rclRight in BoundLines then
    vr := BoundLinesWidth
  else
    vr := 0;

  if AutoHeight then
  begin
    hmax := 0;
    for i := 0 to ControlCount-1 do
    begin
      C := Controls[i];
      if C.Visible then
      begin
        h := C.Top+C.Height;
        if h > hmax then
          hmax := h;
      end;
    end;
    Height := hmax+hb;
  end;

  if AutoWidth then
  begin
    hmax := 0;
    for i := 0 to ControlCount-1 do
    begin
      C := Controls[i];
      if C.Visible then
      begin
        h := C.Left+C.Width;
        if h > hmax then
          hmax := h;
      end;
    end;
    Width := hmax+vr;
  end;
end;

procedure TrwFramedContainer.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  DoSyncVisualControl(rwCPInvalidate);
  Realign;
end;

procedure TrwFramedContainer.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwFramedContainer.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwFramedContainer.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  DoSyncVisualControl(rwCPInvalidate);
  Realign;
end;

end.
