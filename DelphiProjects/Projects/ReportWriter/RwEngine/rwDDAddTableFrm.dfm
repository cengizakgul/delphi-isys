object rwAddDDTable: TrwAddDDTable
  Left = 426
  Top = 191
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Add Table'
  ClientHeight = 465
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  DesignSize = (
    395
    465)
  PixelsPerInch = 96
  TextHeight = 13
  object rgTableType: TRadioGroup
    Left = 6
    Top = 6
    Width = 383
    Height = 40
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Table Type'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'External'
      'RW Query'
      'Component')
    TabOrder = 0
    OnClick = rgTableTypeClick
  end
  object gbItems: TGroupBox
    Left = 6
    Top = 56
    Width = 383
    Height = 371
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Available Items'
    TabOrder = 1
    DesignSize = (
      383
      371)
    object lvItems: TListView
      Left = 9
      Top = 16
      Width = 364
      Height = 344
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <
        item
          Caption = 'External Name'
          Width = 200
        end
        item
          AutoSize = True
          Caption = 'Display Name'
        end>
      HideSelection = False
      PopupMenu = pmTables
      TabOrder = 0
      ViewStyle = vsReport
    end
  end
  object btnOK: TButton
    Left = 229
    Top = 438
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 314
    Top = 438
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object pmTables: TPopupMenu
    Left = 102
    Top = 224
    object miShowAll: TMenuItem
      Caption = 'Show All Available tables'
      OnClick = miShowAllClick
    end
  end
end
