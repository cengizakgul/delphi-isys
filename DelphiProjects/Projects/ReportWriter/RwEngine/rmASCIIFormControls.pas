unit rmASCIIFormControls;

interface

uses Classes, Controls, Types, Windows, Graphics, SysUtils, Messages, Forms, Math, Variants, StdCtrls,
     rwEngineTypes, rwGraphics, rwBasicClasses, rwTypes, rmTypes, rmCommonClasses,
     rwDB, rwQBInfo, rmFormula, rwUtils, rwBasicUtils;

type

  TrmASCIIRenderingStatus = (rmASCIIRSNone, rmASCIIRSCalculating, rmASCIIRSCalculated);


  TrmASCIINode = class;
  TrmASCIIForm = class;

  TrmASCIIItem = class(TrmGraphicControl, IrmASCIIItem)
  private
    FReadItemIndex: Integer;
    FEnabled: Boolean;
    FRealigning: Boolean;
    function  GetItemIndex: Integer;
    procedure SetItemIndex(const AValue: Integer);
  protected
    procedure SetVisible(Value: Boolean); override;
    function  GetItemLevel: Integer; virtual;
    function  CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean; override;
    procedure AssignScreenPosition;
    procedure DoAssignScreenPosition; virtual; abstract;
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); virtual;
    function  TreeBranchRect: TRect; virtual;
    function  ASCIIForm: TrmASCIIForm;
    procedure SetDescriptionFont(AFont: TrwFont); virtual;
    procedure RegisterComponentEvents; override;
    function  ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
    function  ZoomedClientRect(ACanvas: TrwCanvas): TRect;
    function  MouseResizingThreshold: Integer; override;

  //Interface
    function  GetWinControlParent: TWinControl; override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  GetParentExtRect: TRect; override;
    function  GetParent: IrmDesignObject; override;
    procedure SetParent(AParent: IrmDesignObject); override;
    function  ObjectType: TrwDsgnObjectType; override;
    function  ClientToScreen(APos: TPoint): TPoint; override;
    function  ScreenToClient(APos: TPoint): TPoint; override;
    function  GetASCIIForm: IrmDesignObject;
  //end

  //rendering
  private
    FRendStatus: TrmASCIIRenderingStatus;
    procedure   SetRendStatus(const Value: TrmASCIIRenderingStatus); virtual;
    function    FormulaBlockBeforeCalc: TrmFMLFormula;
    function    FormulaBlockAfterCalc: TrmFMLFormula;
  protected
    function    Renderer: IrmASCIIRenderEngine; virtual;
    procedure   CalcSetPropFormulas;
    function    BeforeCalculate: Boolean; virtual;
    procedure   DoCalculate; virtual;
    function    AfterCalculate: Boolean; virtual;
    procedure   EraseItemData; virtual;
  public
    procedure   Calculate;
    property    RendStatus: TrmASCIIRenderingStatus read FRendStatus write SetRendStatus;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    Parent: TrmASCIINode; virtual;
    function    ClientToForm(APoint: TPoint): TPoint; overload;
    function    FormToClient(APoint: TPoint): TPoint; overload;
    function    ClientToForm(ARect: TRect): TRect; overload;
    function    FormToClient(ARect: TRect): TRect; overload;
    function    CanvasDPI: Integer; override;

  published
    property Left stored False;
    property Top stored False;
    property Width stored False;
    property Height stored False;
    property Visible stored False;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;


  TrmASCIINode = class(TrmASCIIItem, IrmASCIINode)
  private
    FItems: TList;
    FBlockParentIfEmpty: Boolean;
    function  GetItem(Index: Integer): TrmASCIIItem;
    procedure SetBlockParentIfEmpty(const Value: Boolean);
    procedure AddItem(AItem: TrmASCIIItem);
    procedure DeleteItem(AItem: TrmASCIIItem);
  protected
    procedure ReadState(Reader: TReader); override;
    procedure WriteState(Writer: TWriter); override;
    procedure RWLoaded; override;
    procedure PaintForeground(ACanvas: TrwCanvas); virtual;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure PaintChildren(ACanvas: TrwCanvas); virtual;
    procedure PaintTreeLines(ACanvas: TrwCanvas); virtual;
    procedure DoAssignScreenPosition; override;
    procedure Invalidate; override;
    procedure Refresh; override;
    function  TreeBranchRect: TRect; override;
    procedure Notify(AEvent: TrmDesignEventType); override;

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  GetBoundsRect: TRect; override;
    procedure SetBoundsRect(ARect: TRect); override;
    procedure AddChildObject(AObject: IrmDesignObject); override;
    procedure RemoveChildObject(AObject: IrmDesignObject); override;
  public
    function  ControlAtPos(APos: TPoint): TrwComponent; override;
  // End

  //rendering
  private
    FBranchBegRecNbr: Integer;
    FCurrentRecNbr: Integer;
    function  FormulaBlockBeforeBranchCalc: TrmFMLFormula;
    function  FormulaBlockAfterBranchCalc: TrmFMLFormula;
  protected
    function    BeforeCalculate: Boolean; override;
    function    BeforeCalculateBranch: Boolean; virtual;
    procedure   DoCalculateBranch; virtual;
    function    AfterCalculateBranch: Boolean; virtual;
    procedure   DoCalculate; override;
    procedure   EraseItemData; override;
    procedure   EraseBranchData;
  public
    procedure  SetRendStatusForChildren(AStatus: TrmASCIIRenderingStatus);
    procedure  SetRendStatusChildrenToo(AStatus: TrmASCIIRenderingStatus);
    procedure  CalculateBranch;
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    ItemCount: Integer;
    procedure   PaintToCanvas(ACanvas: TrwCanvas); override;
    function    ItemIndexOf(const AItem: TrmASCIIItem): Integer;

    property    Items[Index: Integer]: TrmASCIIItem read GetItem;

  published
    property    BlockParentIfEmpty: Boolean read FBlockParentIfEmpty write SetBlockParentIfEmpty;

    function  PControlCount: Variant;
    function  PFindControl(AName: Variant): Variant;
    function  PControlByIndex(AIndex: Variant): Variant;
    function  PControlIndexByName(AName: Variant): Variant;
  end;

  TrmASCIIRecord = class;

  TrmASCIIQuery = class(TrwQuery)
  private
    FNode: TrmASCIIRecord;
  protected
//    procedure DoBeforeOpen; override;
//    procedure DoAfterOpen; override;
//    procedure DoAfterClose; override;
    procedure OnAssignQBInfo(Sender: TObject); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    property Node: TrmASCIIRecord read FNode;
  end;


  TrmASCIIRecord = class(TrmASCIINode, IrmASCIIRecord, IrmQueryDrivenObject)
  private
    FQuery: TrmASCIIQuery;
    FLastFieldIndex: Integer;
    procedure ResortItems;
    function  GetQueryBuilderInfo: TrwQBQuery;
    procedure SetQueryBuilderInfo(const Value: TrwQBQuery);
    function  GetParamsFromVars: Boolean;
    procedure SetParamsFromVars(const Value: Boolean);
    function  GetMasterFields: TrwStrings;
    procedure SetMasterFields(const Value: TrwStrings);
    procedure SyncMasterQueryLink;
    function  GetDataSource: TrwDataSet;

  protected
    procedure RWLoaded; override;
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); override;
    procedure DoAssignScreenPosition; override;
    procedure PaintForeground(ACanvas: TrwCanvas); override;
    procedure RegisterComponentEvents; override;

  // Interface
  protected
    function  GetQuery: IrmQuery;
    procedure Notify(AEvent: TrmDesignEventType); override;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
  // End

  //rendering
  private
    FQueryOpenedInternally: Boolean;
    procedure GetDataFromDataSource;
  protected
    function  BeforeCalculateBranch: Boolean; override;
    procedure DoCalculateBranch; override;
    function  AfterCalculateBranch: Boolean; override;
    procedure DoCalculate; override;
  public
  //end

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property  QueryBuilderInfo: TrwQBQuery read GetQueryBuilderInfo write SetQueryBuilderInfo;
    property  ParamsFromVars: Boolean read GetParamsFromVars write SetParamsFromVars stored DontStoreProperty;
    property  MasterFields: TrwStrings read GetMasterFields write SetMasterFields;
    property  DataSource: TrwDataSet read GetDataSource;

    function  PFieldCount: Variant;
    function  PFieldByIndex(AIndex: Variant): Variant;
    function  PFieldIndexByName(AName: Variant): Variant;
    function  PSubRecordCount: Variant;
    function  PSubRecordByIndex(AIndex: Variant): Variant;
    function  PSubRecordIndexByName(AName: Variant): Variant;
    function  PIsQueryDriven: Variant;
  end;


  TrmASCIIField = class(TrmASCIIItem, IrmASCIIField, IrmQueryAwareObject)
  private
    FSize: Integer;
    FBeginPos: Integer;
    FFormat: string;
    FAlignment: TAlignment;
    FValueHolder: TrmsValueHolder;

    procedure SetSize(const AValue: Integer);
    function  CanWriteValue: Boolean;
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
    function  FormulaValue: TrmFMLFormula;
    function  FormulaTextFilter: TrmFMLFormula;
    procedure SetDataField(const AValue: String);
    function  GetText: string;
    procedure SetFormat(const Value: string);
    procedure SetAlignment(const AValue: TAlignment);
    function  GetDataField: String;

  // Interface
  protected
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  GetFieldName: String;
    procedure SetFieldName(const AFieldName: String);
    function  GetDataFieldInfo: TrmDataFieldInfoRec;
    procedure SetValueFromDataSource;
  // End

  //rendering
  private
    FRendText: String;
  protected
    procedure DoCalculate; override;
  //end

  protected
    procedure SetDescription(const Value: String); override;
    procedure SetName(const NewName: TComponentName); override;
    function  GetShowText: String; override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure DoAssignScreenPosition; override;
    procedure ApplyDsgnMoveLogic(const ALeft, ATop: Integer); override;
    procedure ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState); override;
    function  MouseResizingDiscretion: Integer; override;
    procedure DoOnFilterText(var AText: String);
    procedure DoOnChangeValue(var ANewValue: Variant);
    procedure RegisterComponentEvents; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property  Size: Integer read FSize write SetSize;
    property  Alignment: TAlignment read FAlignment write SetAlignment;
    property  Format: string read FFormat write SetFormat;
    property  Value: Variant read GetValue write SetValue stored CanWriteValue;
    property  DataField: String read GetDataField write SetDataField;
  end;



  TrmASCIIForm = class(TrmASCIINode)
  private
    FResultType: TrmASCIIResultType;
    FQualifier: String;
    FDelimiter: String;
    FDefaultFileName: String;
    FLastLineBreak: Boolean;
    procedure SetResultType(const Value: TrmASCIIResultType);
  protected
    procedure SetParentComponent(Value: TComponent); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetDesignParent(Value: TWinControl); override;
    procedure PaintControl(ACanvas: TrwCanvas); override;
    procedure PaintChildren(ACanvas: TrwCanvas); override;
    procedure Invalidate; override;
    procedure DoAssignScreenPosition; override;
    function  GetItemLevel: Integer; override;
    procedure Refresh; override;
    function  TreeBranchRect: TRect; override;

  //Interface
    procedure SetDesigner(ADesigner: IrmDesigner); override;
    procedure Notify(AEvent: TrmDesignEventType); override;
  //End

  // Rendering
  protected
    function  Renderer: IrmASCIIRenderEngine; override;
  // End

  public
    constructor Create(AOwner: TComponent); override;
    function  Parent: TrmASCIINode; override;
  published
    property ItemIndex stored False;
    property ResultType: TrmASCIIResultType read FResultType write SetResultType;
    property Delimiter: String read FDelimiter write FDelimiter;
    property Qualifier: String read FQualifier write FQualifier;
    property DefaultFileName: String read FDefaultFileName write FDefaultFileName;
    property LastLineBreak: Boolean read FLastLineBreak write FLastLineBreak default True;
  end;


  TrmdASCIIForm = class(TrmdCustomGraphicWorkSpace)
  private
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
  protected
    procedure PropChanged(APropType: TrwChangedPropID); override;
    procedure UpdateSize; override;
    function  DispatchMessage(var Message: TMessage): Boolean; override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


  TrmdASCIIRecordTag = class(TrmdSimpleTextTag)
  protected
    procedure CorrectTagBounds(var ATagRect: TRect); override;
  end;



implementation

uses rmReport, rwCommonClasses, StrUtils;

const
  cLevelOffset = 50;
  cFirstLevelOffset = 20;
  cItemHeight = 50;
  cASCIICharSize = 20;
  cASCIIRecordColorEven = $00C1E0FF;
  cASCIIRecordColorOdd =  $00CEE7FF;
  cASCIIQueryRecordColorEven =  $00E2FFD9;
  cASCIIQueryRecordColorOdd =   $00F0FFEC;

{ TrmASCIIForm }

procedure TrmASCIIForm.DoAssignScreenPosition;
var
  i: Integer;
  R: TRect;
begin
  for i := 0 to ItemCount - 1 do
    Items[i].AssignScreenPosition;

  R := TreeBranchRect;
  SetBoundsRect(R);
end;

function TrmASCIIForm.CreateVisualControl: IrwVisualControl;
begin
  Result := TrmdASCIIForm.Create(Self);
end;

function TrmASCIIForm.GetItemLevel: Integer;
begin
  Result := 0;
end;

procedure TrmASCIIForm.Invalidate;
begin
  if VisualControlCreated then
    VisualControl.PropChanged(rwCPInvalidate);
end;

procedure TrmASCIIForm.Notify(AEvent: TrmDesignEventType);
begin
  if AEvent = rmdZoomChanged then
  begin
    TrmdASCIIForm(VisualControl.RealObject).UpdateSize;
    AssignScreenPosition;
  end;
    
  inherited;
end;

procedure TrmASCIIForm.PaintChildren(ACanvas: TrwCanvas);
var
  SaveIndex: Integer;
  R: TRect;
begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
    R := Rect(Left, Top, Width + Left, Height + Top);
    R := Zoomed(R);
    IntersectClipRect(ACanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

    inherited;

  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;
end;

procedure TrmASCIIForm.PaintControl(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    R := ClientRect;
    R := Zoomed(R);
    InflateRect(R, 1, 1);
    Pen.Width := 1;
    Pen.Color := clBlack;
    Pen.Mode := pmCopy;
    Pen.Style := psSolid;
    Brush.Color := clWhite;
    Brush.Style := bsSolid;
    Rectangle(R);
  end;
end;

function TrmASCIIForm.Parent: TrmASCIINode;
begin
  Result := nil;
end;

procedure TrmASCIIForm.Refresh;
begin
  AssignScreenPosition;
  Invalidate;
end;

function TrmASCIIForm.Renderer: IrmASCIIRenderEngine;
begin
  if Assigned(Owner) and Assigned(TrmReport(Owner).Renderer) then
    Result := TrmReport(Owner).Renderer.ASCIIRenderEngine
  else
    Result := nil;
end;

procedure TrmASCIIForm.SetDesigner(ADesigner: IrmDesigner);
begin
  inherited;
  if Assigned(ADesigner) then
    DesignParent := ADesigner.GetDesignParent(Name)
  else
    DesignParent := nil;
end;

procedure TrmASCIIForm.SetDesignParent(Value: TWinControl);
begin
  if Assigned(Value) and ((DsgnLockState <> rwLSHidden) or LibDesignMode or GetDesigner.LibComponentDesigninig) then
    VisualControl.RealObject.Parent := Value
  else
    DestroyVisualControl;

  inherited;

//  AssignScreenPosition; RM designer cares of this situation
end;

procedure TrmASCIIForm.SetParentComponent(Value: TComponent);
begin
  // Do not need to do anything in here!
end;


procedure TrmASCIIForm.SetResultType(const Value: TrmASCIIResultType);
begin
  FResultType := Value;
  Refresh;
end;

function TrmASCIIForm.TreeBranchRect: TRect;
var
  i: Integer;
  Item: TrmASCIIItem;
  W, H: Integer;
  R: TRect;
begin
  W := 1000;
  H := 1000;
  for i := 0 to ItemCount - 1 do
  begin
    Item := Items[i];

    if Item.DoNotPaint then
      Continue;

    R := Item.TreeBranchRect;
    Inc(R.Right, cLevelOffset);
    Inc(R.Bottom, cLevelOffset);

    W := Max(Item.Left + R.Right, W);
    H := Max(Item.Top + R.Bottom, H);
  end;

  Result := Rect(0, 0, W, H);
end;


constructor TrmASCIIForm.Create(AOwner: TComponent);
begin
  inherited;
  FSubChildrenNamesUnique := True;
  FLastLineBreak := True;
end;

{ TrmdASCIIForm }

constructor TrmdASCIIForm.Create(AOwner: TComponent);
begin
  inherited;
  FObjectOffset := Point(10, 5);
end;

function TrmdASCIIForm.DispatchMessage(var Message: TMessage): Boolean;
var
  P: TPoint;
  O: TrmASCIIItem;

  procedure ControlMouseEvent;
  begin
    with TWMMouse(Message) do
      case Message.Msg of
        WM_MOUSEMOVE:      O.MouseMove(KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDOWN:    O.MouseDown(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONUP:      O.MouseUp(mbLeft, KeysToShiftState(Keys), XPos, YPos);
        WM_LBUTTONDBLCLK:;
        WM_RBUTTONDOWN:    O.MouseDown(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONUP:      O.MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
        WM_RBUTTONDBLCLK:;
        WM_MBUTTONDOWN:    O.MouseDown(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONUP:      O.MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
        WM_MBUTTONDBLCLK:;
        WM_MOUSEWHEEL:;
      end;
  end;

begin
  Result := Inherited DispatchMessage(Message);

  if Result then
    UpdateMouseLastControl(nil)

  else
    if (Message.Msg >= WM_MOUSEFIRST) and (Message.Msg <= WM_MOUSELAST) then
    begin
      P := UnZoomed(Point(Smallint(Message.LParamLo), Smallint(Message.LParamHi)));
      Dec(P.X, FObjectOffset.X);
      Dec(P.Y, FObjectOffset.Y);

      if GetCaptureRMControl = nil then
        O := TrmASCIIItem(TrmASCIIForm(rwObject).ControlAtPos(P))
      else
        O := TrmASCIIItem(GetCaptureRMControl);
      if Assigned(O) then
      begin
        P := O.FormToClient(P);
        Message.lParam := LongInt(MakeLong(Word(P.X), Word(P.Y)));
        if not IsDesignMsg(O, Message) then
          ControlMouseEvent;
      end
      else
      begin
        Message.lParam := LongInt(MakeLong(Word(P.X), Word(P.Y)));
        if not IsDesignMsg(rwObject, Message) then
        begin
          O := TrmASCIIForm(rwObject);
          ControlMouseEvent;
        end;
      end;

      Result := True;

      UpdateMouseLastControl(O)
    end;
end;

procedure TrmdASCIIForm.Paint;
var
  SaveIndex: Integer;
  R: TRect;
begin
  SaveIndex := SaveDC(Canvas.Handle);
  try
    with Canvas do
    begin
      R := Rect(0, 0, TrmASCIIForm(RWObject).Width, TrmASCIIForm(RWObject).Height);
      OffsetRect(R, FObjectOffset.X, FObjectOffset.Y);
      R := Zoomed(R);
      ExcludeClipRect(Canvas.Handle, R.Left, R.Top, R.Right, R.Bottom);
      R := Canvas.ClipRect;
      Brush.Color := clAppWorkSpace;
      Brush.Style := bsSolid;
      FillRect(R);
    end;
  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;

  SaveIndex := SaveDC(Canvas.Handle);
  try
    MoveWindowOrg(Canvas.Handle, Zoomed(FObjectOffset.X), Zoomed(FObjectOffset.Y));
    TrmASCIIForm(RWObject).PaintToCanvas(Canvas);

    R := Zoomed(Rect(0, 0, TrmASCIIForm(RWObject).Width, TrmASCIIForm(RWObject).Height));
    with Canvas do
    begin
      Pen.Width := 2;
      Pen.Color := clBlack;
      Pen.Mode := pmCopy;
      Pen.Style := psSolid;
      MoveTo(R.Left, R.Bottom + 1);
      LineTo(R.Right + 1, R.Bottom + 1);
      LineTo(R.Right + 1, R.Top);
    end;

  finally
    RestoreDC(Canvas.Handle, SaveIndex);
  end;
end;

procedure TrmdASCIIForm.PropChanged(APropType: TrwChangedPropID);
begin
  if APropType in [rwCPAll, rwCPPosition] then
    UpdateSize;

  inherited;
end;

procedure TrmdASCIIForm.UpdateSize;
var
  l, t, w, h: Integer;
begin
  if TrmASCIIForm(RWObject).GetDesigner <> nil then
  begin
    FZoomFactor := TrmASCIIForm(RWObject).GetDesigner.GetZoomFactor;

    if FZoomFactor = 0 then
      Exit;

    w := Zoomed(TrmASCIIForm(RWObject).Width + FObjectOffset.X * 2);
    h := Zoomed(TrmASCIIForm(RWObject).Height + FObjectOffset.Y * 2);
    l := Left;
    t := Top;
    SetBounds(l, t, w, h);
  end;
end;


procedure TrmdASCIIForm.WMSetCursor(var Message: TWMSetCursor);
var
  Crs: TCursor;
  Control: TrmASCIIItem;
  P: TPoint;
begin
  inherited;
  with Message do
    if CursorWnd = Handle then
      case Smallint(HitTest) of
        HTCLIENT:
          begin
            Crs := Screen.Cursor;
            if Crs = crDefault then
            begin
              GetCursorPos(P);
              P := TrmASCIIForm(rwObject).ScreenToClient(P);
              Control := TrmASCIIItem(TrmASCIIForm(rwObject).ControlAtPos(P));
              if Assigned(Control) then
                Crs := Control.Cursor;

              if Crs = crDefault then
                Crs := Cursor;
            end;

            if Crs <> crDefault then
            begin
              Windows.SetCursor(Screen.Cursors[Crs]);
              Result := 1;
            end;
          end;
      end;
end;


{ TrmASCIINode }

function TrmASCIINode.AfterCalculateBranch: Boolean;
var
  Fml: TrmFMLFormula;
  i: Integer;
  Node: TrmASCIINode;
begin
  Fml := FormulaBlockAfterBranchCalc;
  if Assigned(Fml) then
    Result := Fml.Calculate([])
  else
    Result := True;

  if Result then
  begin
    // Check inner master-details
    for i := 0 to ItemCount - 1 do
      if Items[i] is TrmASCIINode then
      begin
        Node := TrmASCIINode(Items[i]);
        if Node.BlockParentIfEmpty and Node.Enabled and (Node.RendStatus <> rmASCIIRSCalculated) then
        begin
          EraseBranchData;
          Result := False;
          break;
        end;
      end;
  end
  else
    EraseBranchData;
end;

procedure TrmASCIINode.DoAssignScreenPosition;
var
  i: Integer;
  R: TRect;
begin
  i := GetItemLevel;
  if i = 0 then
    Left := cFirstLevelOffset
  else
    Left := cLevelOffset;

  i := ItemIndex;
  if i = 0 then
  begin
    if Parent is TrmASCIIForm then
      Top := cFirstLevelOffset
    else
      Top := Parent.Height + cLevelOffset div 2;
  end
  else
  begin
    R := Parent.Items[i - 1].TreeBranchRect;
    Top := Parent.Items[i - 1].Top + R.Bottom + cLevelOffset div 2;
  end;

  Width := cItemHeight * 2;
  Height := cItemHeight;

  for i := 0 to ItemCount - 1 do
    Items[i].AssignScreenPosition;
end;

function TrmASCIINode.BeforeCalculateBranch: Boolean;
var
  Fml: TrmFMLFormula;
begin
  FBranchBegRecNbr := Renderer.ASCIILineCount;

  Fml := FormulaBlockBeforeBranchCalc;
  if Assigned(Fml) then
    Result := Fml.Calculate([])
  else
    Result := True;
end;

procedure TrmASCIINode.CalculateBranch;
begin
  if Enabled and (RendStatus = rmASCIIRSNone) then
  begin
    if BeforeCalculateBranch then
    begin
      DoCalculateBranch;
      AfterCalculateBranch;
    end;
  end;
end;

function TrmASCIINode.ControlAtPos(APos: TPoint): TrwComponent;
var
  i: Integer;
  R: TRect;
  P: TPoint;
  C, Cin: TrmASCIIItem;
begin
  Cin := nil;

  for i := 0 to ItemCount - 1 do
  begin
    C := Items[i];

    if C.DoNotPaint then
      Continue;

    if C is TrmASCIINode then
    begin
      R := C.BoundsRect;
      if PtInRect(R, APos) then
        Cin := C;

      P := APos;
      Dec(P.X, R.Left);
      Dec(P.Y, R.Top);
      C := TrmASCIIItem(TrmASCIINode(C).ControlAtPos(P));
      if Assigned(C) then
        Cin := C;

      if Assigned(Cin) then
        break;
    end;
  end;

  if not Assigned(Cin) then
    for i := 0 to ItemCount - 1 do
    begin
      C := Items[i];

      if C.DoNotPaint then
        Continue;

      if (C is TrmASCIIItem) and not (C is TrmASCIINode) then
      begin
        R := C.BoundsRect;
        if PtInRect(R, APos) then
        begin
          Cin := C;
          break;
        end;
      end;
    end;

  Result := Cin;
end;

constructor TrmASCIINode.Create(AOwner: TComponent);
begin
  FItems := TList.Create;
  inherited;
  FVirtOwner := True;
  FBlockParentIfEmpty := False;
end;

destructor TrmASCIINode.Destroy;
begin
  inherited;
  FreeAndNil(FItems);
end;

procedure TrmASCIINode.DoCalculate;
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmASCIINode then
      TrmASCIINode(Items[i]).CalculateBranch
    else
      Items[i].Calculate;

  inherited;
end;

procedure TrmASCIINode.DoCalculateBranch;
begin
  Calculate;
end;

function TrmASCIINode.GetItem(Index: Integer): TrmASCIIItem;
begin
  Result := TrmASCIIItem(FItems[Index]);
end;

procedure TrmASCIINode.Invalidate;
begin
  InvalidateRect(TreeBranchRect);
end;

function TrmASCIINode.ItemCount: Integer;
begin
  Result := FItems.Count;
end;

function TrmASCIINode.ItemIndexOf(const AItem: TrmASCIIItem): Integer;
begin
  Result := FItems.IndexOf(AItem);
end;

procedure TrmASCIINode.RWLoaded;
var
  i: Integer;
  Itm: TrmASCIIItem;
begin
  inherited;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TrmASCIIItem then
    begin
      Itm := TrmASCIIItem(Components[i]);
      if Itm.FReadItemIndex <> -1 then
      begin
        FItems[Itm.FReadItemIndex] := Itm;
        Itm.FReadItemIndex := -1;
      end;
    end;
end;

procedure TrmASCIINode.Notify(AEvent: TrmDesignEventType);
var
  i: Integer;
begin
  inherited;

  if AEvent = rmdZoomChanged then
    for i := 0 to ItemCount - 1 do
      Items[i].Notify(AEvent);
end;

procedure TrmASCIINode.PaintChildren(ACanvas: TrwCanvas);
var
  i: Integer;
  SaveIndexChld: Integer;
  Item: TrmASCIIItem;
begin
 SaveIndexChld := SaveDC(ACanvas.Handle);
 try
   MoveWindowOrg(ACanvas.Handle, Zoomed(Left), Zoomed(Top));

    for i := 0 to ItemCount - 1 do
    begin
      Item := Items[i];

      if Item.DoNotPaint then
        Continue;

      Item.PaintToCanvas(ACanvas);
    end;

  finally
    RestoreDC(ACanvas.Handle, SaveIndexChld);
  end;
end;

procedure TrmASCIINode.PaintControl(ACanvas: TrwCanvas);
begin
  if GetItemLevel > 0 then
    PaintTreeLines(ACanvas);
end;

procedure TrmASCIINode.PaintForeground(ACanvas: TrwCanvas);
begin
end;

procedure TrmASCIINode.PaintToCanvas(ACanvas: TrwCanvas);
var
  SaveIndex: Integer;

  procedure ExcludeNotOpaqueRegions;
    var Rc: TRect;

    procedure DoExclusion(ANode: TrmASCIINode);
    var
      i: Integer;
      R: TRect;
      Itm: TrmASCIIItem;
    begin
      for i := 0 to ANode.ItemCount - 1 do
      begin
        Itm := ANode.Items[i];
        if Itm.DoNotPaint then
          Continue;

        R := ANode.ClientToForm(Itm.BoundsRect);
        IntersectRect(R, R, Rc);
        ExcludeClipRect(ACanvas.Handle, R.Left, R.Top, R.Right, R.Bottom);

        if Itm is TrmASCIINode then
          DoExclusion(TrmASCIINode(Itm));
      end;
    end;
  begin
    Rc := ClientRect;
    DoExclusion(Self);
  end;

begin
  SaveIndex := SaveDC(ACanvas.Handle);
  try
//    ExcludeNotOpaqueRegions;
    inherited;
  finally
    RestoreDC(ACanvas.Handle, SaveIndex);
  end;

  PaintChildren(ACanvas);
  PaintForeground(ACanvas);
end;


procedure TrmASCIINode.PaintTreeLines(ACanvas: TrwCanvas);
const cDashGap = 3;
var
  R: TRect;
begin
  with ACanvas do
  begin
    Brush.Color := clBlack;
    Brush.Style := bsSolid;

    if not BlockParentIfEmpty then
    begin
      R.Left := cItemHeight div 2 - 2;
      R.Top := Top + Height div 2 - 2;
      R.Bottom := R.Top + 2;
      R.Right := R.Left + cDashGap;
      
      while R.Left < Left do
      begin
        if R.Right > Left then
          R.Right := Left;
        FillRect(Zoomed(R));
        OffsetRect(R, cDashGap * 2, 0);
      end;
    end

    else
    begin
      R.Left := cItemHeight div 2 - 2;
      R.Top := Top + Height div 2 - 2;
      R.Right := Left;
      R.Bottom := R.Top + 2;
      FillRect(Zoomed(R));
    end;

    R.Left := cItemHeight div 2 - 2;
    R.Bottom := Top + Height div 2;
    R.Top := Parent.Height;
    R.Right := R.Left + 2;
    FillRect(Zoomed(R));
  end;
end;

procedure TrmASCIINode.ReadState(Reader: TReader);
var
  PrevOwner: TComponent;
  PrevRoot: TComponent;
  PrevParent: TComponent;
  i: Integer;
begin
  i := 0;
  while i < ComponentCount do
    if not TrwComponent(Components[i]).InheritedComponent then
      Components[i].Free
    else
      Inc(i);

  PrevOwner := Reader.Owner;
  PrevParent := Reader.Parent;
  PrevRoot := Reader.Root;
  Reader.Owner := Self;
  Reader.Root := Self;
  Reader.Parent := nil;
  inherited;
  Reader.Owner := PrevOwner;
  Reader.Root := PrevRoot;
  Reader.Parent := PrevParent;
end;

procedure TrmASCIINode.Refresh;
var
  Rold, Rnew: TRect;
begin
  if ASCIIForm = nil then
    Exit;

  Rold := ASCIIForm.TreeBranchRect;
  AssignScreenPosition;
  Rnew := ASCIIForm.TreeBranchRect;
  if (Rold.Right <> Rnew.Right) or (Rold.Bottom <> Rnew.Bottom) then
    ASCIIForm.Refresh
  else
    inherited;
end;

procedure TrmASCIINode.SetRendStatusChildrenToo(AStatus: TrmASCIIRenderingStatus);
begin
  RendStatus := AStatus;
  SetRendStatusForChildren(AStatus);
end;

procedure TrmASCIINode.SetRendStatusForChildren(AStatus: TrmASCIIRenderingStatus);
var
  i: Integer;
begin
  for i := 0 to ItemCount - 1 do
    if Items[i] is TrmASCIINode then
      TrmASCIINode(Items[i]).SetRendStatusChildrenToo(AStatus)
    else
      Items[i].RendStatus := AStatus;
end;

function TrmASCIINode.TreeBranchRect: TRect;
var
  i: Integer;
  Item: TrmASCIIItem;
  W, H: Integer;
  R: TRect;
begin
  W := Width;
  H := Height;
  for i := 0 to ItemCount - 1 do
  begin
    Item := Items[i];

    if Item.DoNotPaint then
      Continue;

    R := Item.TreeBranchRect;
    W := Max(Item.Left + R.Right, W);
    H := Max(Item.Top + R.Bottom, H);
  end;

  Result := Rect(0, 0, W, H); 
end;

procedure TrmASCIINode.WriteState(Writer: TWriter);
var
  OldRoot, OldRootAncestor: TComponent;
begin
  OldRootAncestor := Writer.RootAncestor;
  OldRoot := Writer.Root;
  try
    if Assigned(OldRootAncestor) then
      Writer.RootAncestor := OldRootAncestor.FindComponent(Name);
    if not Assigned(Writer.RootAncestor) then
      Writer.RootAncestor := Self;
    Writer.Root := Self;
    inherited;
  finally
    Writer.Root := OldRoot;
    Writer.RootAncestor := OldRootAncestor;
  end;
end;


function TrmASCIINode.GetBoundsRect: TRect;
begin
  Result := TreeBranchRect;
  OffsetRect(Result, Left, Top);
end;

procedure TrmASCIINode.SetBoundsRect(ARect: TRect);
begin
  inherited;
end;

procedure TrmASCIINode.SetBlockParentIfEmpty(const Value: Boolean);
begin
  FBlockParentIfEmpty := Value;
  if Parent <> nil then
    Parent.Invalidate;
end;

procedure TrmASCIINode.EraseItemData;
begin
  inherited;
  if FCurrentRecNbr > -1 then
  begin
    Renderer.CutOffUpToLine(FCurrentRecNbr);
    FCurrentRecNbr := -1;
  end;
end;

function TrmASCIINode.FormulaBlockAfterBranchCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockAfterBranchCalc');
end;

function TrmASCIINode.FormulaBlockBeforeBranchCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockBeforeBranchCalc');
end;


procedure TrmASCIINode.AddChildObject(AObject: IrmDesignObject);
begin
  if AObject.RealObject is TrmASCIIItem then
  begin
    if TrmASCIIItem(AObject.RealObject).Parent <> nil then
      TrmASCIIItem(AObject.RealObject).Parent.DeleteItem(TrmASCIIItem(AObject.RealObject));
    inherited;
    AddItem(TrmASCIIItem(AObject.RealObject));
    ASCIIForm.Refresh;
  end
  else
    inherited;
end;

procedure TrmASCIINode.RemoveChildObject(AObject: IrmDesignObject);
begin
  if AObject.RealObject is TrmASCIIItem then
    DeleteItem(TrmASCIIItem(AObject.RealObject));
  inherited;    
end;

procedure TrmASCIINode.AddItem(AItem: TrmASCIIItem);
begin
  FItems.Add(AItem);
  if not Loading then
    AItem.AssignScreenPosition;
end;

procedure TrmASCIINode.DeleteItem(AItem: TrmASCIIItem);
var
  i: Integer;
begin
  i := AItem.ItemIndex;
  if i <> -1 then
  begin
    FItems.Delete(i);
    ASCIIForm.Refresh;
  end;
end;


function TrmASCIINode.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.AcceptControls := True;
end;

function TrmASCIINode.PControlByIndex(AIndex: Variant): Variant;
begin
  Result := Integer(Pointer(Items[AIndex]));
end;

function TrmASCIINode.PControlCount: Variant;
begin
  Result := ItemCount;
end;

function TrmASCIINode.PControlIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to ItemCount - 1 do
    if SameText(Items[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;

function TrmASCIINode.PFindControl(AName: Variant): Variant;
var
  i: Integer;
begin
  i := PControlIndexByName(AName);
  if i = -1 then
    Result := 0
  else
    Result := PControlByIndex(i);
end;

procedure TrmASCIINode.EraseBranchData;
begin
  if FBranchBegRecNbr > -1 then
  begin
    Renderer.CutOffUpToLine(FBranchBegRecNbr);
    FBranchBegRecNbr := -1;
    FCurrentRecNbr := -1;
  end;

  RendStatus := rmASCIIRSNone;  
end;


function TrmASCIINode.BeforeCalculate: Boolean;
begin
  Result := inherited BeforeCalculate;
  if Result then
    FCurrentRecNbr := Renderer.ASCIILineCount
  else
    FCurrentRecNbr := -1;        
end;

{ TrmASCIIItem }

function TrmASCIIItem.AfterCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockAfterCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('AfterCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
  begin
    RendStatus := rmASCIIRSCalculated;
    NotifyAggregateFunctions;
  end
  else
  begin
    EraseItemData;
    RendStatus := rmASCIIRSNone;
  end;
end;

procedure TrmASCIIItem.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
begin
end;

function TrmASCIIItem.ASCIIForm: TrmASCIIForm;
var
  P: TrwComponent;
begin
  P := Self;
  while Assigned(P) and not (P is TrmASCIIForm) do
    P := TrwComponent(P.Owner);

  if P is TrmASCIIForm then
    Result := TrmASCIIForm(P)
  else
    Result := nil;
end;

procedure TrmASCIIItem.AssignScreenPosition;
begin
  if IsDesignMode then
  begin
    FRealigning := True;
    try
      DoAssignScreenPosition;
    finally
      FRealigning := False;
      SyncTagPos;      
    end;
  end;  
end;

function TrmASCIIItem.BeforeCalculate: Boolean;
var
  pAccept: Variant;
  Fml: TrmFMLFormula;
begin
  Fml := FormulaBlockBeforeCalc;
  if Assigned(Fml) then
    pAccept := Fml.Calculate([])
  else
    pAccept := True;

  ExecuteEventsHandler('BeforeCalculate', [Integer(Addr(pAccept))]);
  Result := pAccept;

  if Result then
    RendStatus := rmASCIIRSCalculating
  else
    RendStatus := rmASCIIRSNone;
end;

procedure TrmASCIIItem.CalcSetPropFormulas;
var
  i: Integer;
  v: Variant;
begin
  for i := 0 to Formulas.Count - 1 do
    if Formulas[i].ActionType = fatSetProperty then
    begin
      v := Formulas[i].Calculate([]);
      SetPropertyValue(Formulas[i].ActionInfo, v);
    end;
end;

procedure TrmASCIIItem.Calculate;
begin
  if Enabled and (RendStatus = rmASCIIRSNone) then
  begin
    if BeforeCalculate then
    begin
      DoCalculate;
      AfterCalculate;
    end;
  end;
end;

function TrmASCIIItem.CanvasDPI: Integer;
begin
  Result := cScreenCanvasRes;
end;

function TrmASCIIItem.CheckSizeConstrains(var ALeft, ATop, AWidth, AHeight: Integer): Boolean;
begin
  if not FRealigning then
  begin
    if (ALeft <> Left) or (ATop <> Top) then
      ApplyDsgnMoveLogic(ALeft, ATop);

    ALeft := Left;
    ATop := Top;
    AWidth := Width;
    AHeight := Height;
    Result := False;
  end
  else
    Result := True;
end;

function TrmASCIIItem.ClientToForm(ARect: TRect): TRect;
begin
  Result.TopLeft := ClientToForm(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmASCIIItem.ClientToForm(APoint: TPoint): TPoint;
var
  C: TrmASCIINode;
begin
  Result := APoint;
  Inc(Result.X, Left);
  Inc(Result.Y, Top);
  C := Parent;
  while Assigned(C) do
  begin
    Inc(Result.X, C.Left);
    Inc(Result.Y, C.Top);
    C := C.Parent;
  end;
end;

function TrmASCIIItem.ClientToScreen(APos: TPoint): TPoint;
var
  P: TrmdASCIIForm;
begin
  P := TrmdASCIIForm(GetWinControlParent);
  Result := ClientToForm(APos);
  Inc(Result.X, P.FObjectOffset.X);
  Inc(Result.Y, P.FObjectOffset.Y);
  Result := P.ClientToScreen(Zoomed(Result));
end;

constructor TrmASCIIItem.Create(AOwner: TComponent);
begin
  inherited;
  FReadItemIndex := -1;
  FEnabled := True;

  if Parent <> nil then
    Parent.AddItem(Self);
end;

function TrmASCIIItem.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  if VisualControlCreated then
    Result := inherited DesignBehaviorInfo
  else
  begin
    Result := inherited DesignBehaviorInfo;
    Result.MovingDirections := [];
    Result.ResizingDirections := [];
    Result.Selectable := {(FMousePosition = rmdMPClient) and} (DsgnLockState = rwLSUnlocked) or Result.Selectable;
  end;
end;

destructor TrmASCIIItem.Destroy;
begin
  if (Parent <> nil) and not (csDestroying in Parent.ComponentState) then
    Parent.DeleteItem(Self);

  inherited;
end;

procedure TrmASCIIItem.DoCalculate;
begin
  CalcSetPropFormulas;
  ExecuteEventsHandler('OnCalculate', []);
end;

procedure TrmASCIIItem.EraseItemData;
begin
end;

function TrmASCIIItem.FormToClient(ARect: TRect): TRect;
begin
  Result.TopLeft := FormToClient(ARect.TopLeft);
  Result.Bottom := Result.Top + (ARect.Bottom - ARect.Top);
  Result.Right := Result.Left + (ARect.Right - ARect.Left);
end;

function TrmASCIIItem.FormToClient(APoint: TPoint): TPoint;
var
  C: TrmASCIINode;
begin
  Result := APoint;
  Dec(Result.X, Left);
  Dec(Result.Y, Top);
  C := Parent;
  while Assigned(C) do
  begin
    Dec(Result.X, C.Left);
    Dec(Result.Y, C.Top);
    C := C.Parent;
  end;
end;

function TrmASCIIItem.FormulaBlockAfterCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockAfterCalc');
end;

function TrmASCIIItem.FormulaBlockBeforeCalc: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatCalcControl, 'BlockBeforeCalc');
end;

function TrmASCIIItem.GetASCIIForm: IrmDesignObject;
begin
  Result := ASCIIForm;
end;

function TrmASCIIItem.GetItemIndex: Integer;
begin
  Result := -1;

  if Parent <> nil then
    Result := Parent.ItemIndexOf(Self);
end;

function TrmASCIIItem.GetItemLevel: Integer;
var
  C: TrwComponent;
begin
  Result := 0;
  C := TrwComponent(Owner);
  while not (C is TrmASCIIForm) do
  begin
    C := TrwComponent(C.Owner);
    Inc(Result);
  end;
end;

function TrmASCIIItem.GetParent: IrmDesignObject;
begin
  Result := Parent;
end;

function TrmASCIIItem.GetParentExtRect: TRect;
var
  P: TrmdASCIIForm;
begin
  P := TrmdASCIIForm(GetWinControlParent);

  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := ClientToForm(Result.TopLeft);
  Result.BottomRight := ClientToForm(Result.BottomRight);
  OffsetRect(Result, P.FObjectOffset.X, P.FObjectOffset.Y);
  Result := Zoomed(Result);
end;

function TrmASCIIItem.GetWinControlParent: TWinControl;
var
  P: TrmASCIIForm;
begin
  P := ASCIIForm;

  if Assigned(P) and P.VisualControlCreated then
    Result := P.VisualControl.Container
  else
    Result := nil;
end;

function TrmASCIIItem.MouseResizingThreshold: Integer;
begin
  Result := Round(UnZoomed(inherited MouseResizingThreshold));
end;

procedure TrmASCIIItem.Notify(AEvent: TrmDesignEventType);
begin
  inherited;
  if Parent <> nil then
    if AEvent in [rmdSelectObject, rmdSelectChildObject] then
      Parent.Notify(rmdSelectChildObject)
    else if AEvent in [rmdDestroyResizer, rmdDestroyChildResizer] then
      Parent.Notify(rmdDestroyChildResizer);
end;

function TrmASCIIItem.ObjectType: TrwDsgnObjectType;
begin
  Result := rwDOTASCIIForm;
end;

function TrmASCIIItem.Parent: TrmASCIINode;
begin
  Result := TrmASCIINode(Owner);
end;

procedure TrmASCIIItem.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventBeforeCalculate, cEventOnCalculate, cEventAfterCalculate]);
end;

function TrmASCIIItem.Renderer: IrmASCIIRenderEngine;
var
  F: TrmASCIIForm;
begin
  F := ASCIIForm;
  if Assigned(F) then
    Result := F.Renderer
  else
    Result := nil;
end;

function TrmASCIIItem.ScreenToClient(APos: TPoint): TPoint;
var
  P: TrmdASCIIForm;
begin
  P := TrmdASCIIForm(GetWinControlParent);
  Result := UnZoomed(P.ScreenToClient(APos));
  Dec(Result.X, P.FObjectOffset.X);
  Dec(Result.Y, P.FObjectOffset.Y);
  Result := FormToClient(Result);
end;

procedure TrmASCIIItem.SetDescriptionFont(AFont: TrwFont);
begin
  AFont.Name := 'Arial';
  AFont.Style := [fsBold];
  AFont.Size := Round(Zoomed(8));
  AFont.Color := clBlack;
end;

procedure TrmASCIIItem.SetItemIndex(const AValue: Integer);
var
  i: Integer;
begin
  if Loading then
    FReadItemIndex := AValue

  else
    if (Parent <> nil) and (ItemIndex <> AValue) then
    begin
      i := ItemIndex;
      if i = -1 then
        i := Parent.FItems.Add(Self);
      Parent.FItems.Move(i, AValue);
      Parent.AssignScreenPosition;
    end;
end;


procedure TrmASCIIItem.SetParent(AParent: IrmDesignObject);
begin
  if Assigned(AParent) then
    if AParent.RealObject <> Parent then
      AParent.AddChildObject(Self)
    else
      ASCIIForm.Refresh;
end;


procedure TrmASCIIItem.SetRendStatus(const Value: TrmASCIIRenderingStatus);
begin
  if Enabled then
    FRendStatus := Value
  else
    FRendStatus := rmASCIIRSNone;

  if FRendStatus = rmASCIIRSNone then
    EmptyAggregateFunctions;
end;

procedure TrmASCIIItem.SetVisible(Value: Boolean);
begin
  Value := True;
  inherited;
end;

function TrmASCIIItem.TreeBranchRect: TRect;
begin
  Result := Rect(0, 0, Width, Height);
end;

function TrmASCIIItem.ZoomedBoundsRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
begin
  GetWindowOrgEx(ACanvas.Handle, P);

  Result := Rect(0, 0, Width, Height);
  Result.TopLeft := Zoomed(ClientToForm(Result.TopLeft));
  Result.BottomRight := Zoomed(ClientToForm(Result.BottomRight));

  OffsetRect(Result, P.X, P.Y);
end;

function TrmASCIIItem.ZoomedClientRect(ACanvas: TrwCanvas): TRect;
var
  P: TPoint;
begin
  GetWindowOrgEx(ACanvas.Handle, P);

  Result := ClientRect;
  Result.TopLeft := Zoomed(ClientToForm(Result.TopLeft));
  Result.BottomRight := Zoomed(ClientToForm(Result.BottomRight));

  OffsetRect(Result, P.X, P.Y);
end;


{ TrmASCIIField }

constructor TrmASCIIField.Create(AOwner: TComponent);
begin
  FSize := 10;
  FValueHolder := TrmsValueHolder.Create(Self);
  FValueHolder.OnChangeValue := DoOnChangeValue;  
  inherited;
//  FAllowedResizingBounds := [rmdMPLeftBound, rmdMPRightBound];
end;

destructor TrmASCIIField.Destroy;
begin
  FreeAndNil(FValueHolder);
  inherited;
end;

procedure TrmASCIIField.ApplyMouseResizing(NewBounds: TRect; MousePosition: TrmdMousePosition; Shift: TShiftState);
var
  R: TRect;
  d: Integer;
  i: Integer;
  flMove: Boolean;

  procedure ResizeField(const AFieldIndex, ASizeChange: Integer);
  var
    Itm: TrmASCIIField;
  begin
    Itm := TrmASCIIField(Parent.Items[AFieldIndex]);
    if Itm.Size + ASizeChange < 1 then
      Itm.Size := 1
    else
      Itm.Size := Itm.Size + ASizeChange;
  end;

begin
  R := BoundsRect;
  flMove := ssShift in Shift;

  d := Round((R.Left - NewBounds.Left) / cASCIICharSize);
  if d <> 0 then
  begin
    i := ItemIndex;
    if i > 0 then
    begin
      ResizeField(i - 1, - d);
      if not flMove then
        ResizeField(i, d);
    end;
  end;

  d := Round((NewBounds.Right - R.Right) / cASCIICharSize);
  if d <> 0 then
  begin
    i := ItemIndex;
    ResizeField(i, d);
    if not flMove and (i + 1 <= TrmASCIIRecord(Parent).FLastFieldIndex) then
      ResizeField(i + 1, - d);
  end;
end;

procedure TrmASCIIField.DoAssignScreenPosition;
var
  i: Integer;
  R: TRect;

  function CalcTextSize: TRect;
  var
    Tinf: TrwTextDrawInfoRec;
    Font: TrwFont;
  begin
    Font := TrwFont.Create(nil);
    try
      SetDescriptionFont(Font);
      Tinf.CalcOnly := True;
      Tinf.Canvas := nil;
      Tinf.ZoomRatio := 1;
      Tinf.Text := GetShowText;
      Tinf.Font := Font;
      Tinf.DigitNet := '';
      Tinf.WordWrap := False;
      Tinf.Alignment := taLeftJustify;
      Tinf.Layout := tlTop;
      Tinf.TextRect := Rect(0, 0, Zoomed(cItemHeight * 5), Zoomed(cItemHeight));
      DrawTextImageRM(@Tinf);
      Result := UnZoomed(Tinf.TextRect);
      InflateRect(Result, 4, 4);
    finally
      FreeAndNil(Font);
    end;
  end;

begin
  Top := 0;
  Height := Parent.Height - Top;

  i := ItemIndex;
  if i = 0 then
  begin
    Left := 0;
    FBeginPos := 1;
  end
  else
  begin
    Left := Parent.Items[i - 1].Left + Parent.Items[i - 1].Width;
    FBeginPos := TrmASCIIField(Parent.Items[i - 1]).FBeginPos + TrmASCIIField(Parent.Items[i - 1]).Size;
  end;

//  Width := Size * cASCIICharSize;
  R := CalcTextSize;
  if R.Right - R.Left < cItemHeight then
    Width := cItemHeight
  else
    Width := R.Right - R.Left;
end;

function TrmASCIIField.CanWriteValue: Boolean;
begin
  Result := FormulaValue = nil;
end;

function TrmASCIIField.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.Selectable := (FMousePosition = rmdMPClient) and Result.Selectable;
  Result.MovingDirections := [rwDBIMDLeft, rwDBIMDRight];
end;

procedure TrmASCIIField.DoCalculate;
var
  i: integer;
begin
  inherited;

  FRendText := GetText;

  if ASCIIForm.ResultType = rmARTFixedLength then
  begin
    FRendText := Copy(FRendText, 1, Size);
    if Length(FRendText) < Size then
      case Alignment of
      taLeftJustify:  FRendText := FRendText + StringOfChar(' ', Size - Length(FRendText));
      taRightJustify: FRendText := StringOfChar(' ', Size - Length(FRendText)) + FRendText;
      taCenter:
        begin
          i := Size - Length(FRendText);
          FRendText := Copy(StringOfChar(' ', i div 2) + FRendText + StringOfChar(' ', i - (i div 2)), 1, Size);
        end;
      end;
  end;
end;

function TrmASCIIField.FormulaValue: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatSetProperty, 'Value');
end;

function TrmASCIIField.GetDataFieldInfo: TrmDataFieldInfoRec;
begin
  Result := DataFieldInfo(Self, Parent);
end;

function TrmASCIIField.GetFieldName: String;
begin
  Result := DataField;
end;

function TrmASCIIField.GetText: string;
begin
  Result := FormatValue(FFormat, Value);

  if (FormulaTextFilter <> nil) and (Renderer <> nil) then
    Result := FormulaTextFilter.Calculate([Result]);

  DoOnFilterText(Result);
end;

function TrmASCIIField.GetValue: Variant;
begin
  if not (csWriting in ComponentState) and (Renderer <> nil) then
    Calculate;

  Result := FValueHolder.Value;
end;

function TrmASCIIField.MouseResizingDiscretion: Integer;
begin
  if ASCIIForm.ResultType = rmARTFixedLength then
    Result := cASCIICharSize
  else
    Result := inherited MouseResizingDiscretion;
end;

procedure TrmASCIIField.PaintControl(ACanvas: TrwCanvas);
var
  EndPos, n, Ind: Integer;
  R: TRect;
  flQuerySchema: Boolean;

  procedure DrawFieldText;
  var
    Txt: String;
  begin
    with ACanvas do
    begin
      SetDescriptionFont(Font);
      Brush.Style := bsClear;
      R := BoundsRect;
      InflateRect(R, -4, -4);
      Inc(R.Top, 10);
      R := Zoomed(R);

      Txt := GetShowText;
      DrawText(GetNextStrValue(Txt, #13), R, DT_TOP	or DT_LEFT or DT_NOPREFIX);

      if (Txt <> '') and (Txt[1] = '[') then
        Font.Style := [fsItalic]

      else if Txt = cErrorTag then
      begin
        Font.Style := [fsBold];
        Font.Color := clRed;
      end
      else
        Font.Style := [];

      Inc(R.Top, -Font.Height);
      if ASCIIForm.ResultType = rmARTFixedLength then
        case Alignment of
        taLeftJustify:  DrawText(Txt, R, DT_TOP	or DT_LEFT or DT_NOPREFIX);
        taRightJustify: DrawText(Txt, R, DT_TOP	or DT_RIGHT or DT_NOPREFIX);
        taCenter:       DrawText(Txt, R, DT_TOP	or DT_CENTER or DT_NOPREFIX);
        end
      else
        DrawText(Txt, R, DT_TOP	or DT_LEFT or DT_NOPREFIX);
    end;
  end;

begin
  with ACanvas do
  begin
    flQuerySchema := not TrmASCIIRecord(Parent).QueryBuilderInfo.IsEmpty;

    if ItemIndex mod 2 = 0 then
      if flQuerySchema then
        Brush.Color := cASCIIQueryRecordColorEven
      else
        Brush.Color := cASCIIRecordColorEven
    else
      if flQuerySchema then
        Brush.Color := cASCIIQueryRecordColorOdd
      else
        Brush.Color := cASCIIRecordColorOdd;

    Brush.Style := bsSolid;
    Pen.Width := 0;
    FillRect(Zoomed(BoundsRect));

    DrawFieldText;

    // Draw Border
    Pen.Color := clBlack;
    Pen.Style := psSolid;
    Pen.Width := 1;

    Ind := ItemIndex;
    if Ind > 0 then
    begin
      MoveTo(Zoomed(Left), 0);
      LineTo(Zoomed(Left), Zoomed(Height));
    end;
    if Ind < TrmASCIIRecord(Parent).FLastFieldIndex then
    begin
      MoveTo(Zoomed(Left + Width), 0);
      LineTo(Zoomed(Left + Width), Zoomed(Height));
    end;

    // Draw End Positions
    Font.Name := 'Arial Narrow';
    Font.Size := Round(Zoomed(7));
    Font.Style := [];
    Font.Color := clGray;
    Brush.Style := bsClear;

    if ASCIIForm.ResultType = rmARTFixedLength then
    begin
      TextOut(Zoomed(Left) + 2, 0, IntToStr(FBeginPos), 0);

      EndPos := FBeginPos + Size - 1;
      if FBeginPos < EndPos then
      begin
        n := TextWidth(IntToStr(EndPos));
        TextOut(Zoomed(Left + Width) - 2 - n, 0, IntToStr(EndPos), 0);
      end;
    end;

    Font.Color := clBlue;
    Font.Style := [fsBold];
    TextOut(Zoomed(Left + Width div 2), 0, IntToStr(Ind + 1), TA_CENTER);
  end;
end;

procedure TrmASCIIField.SetAlignment(const AValue: TAlignment);
begin
  if FAlignment <> AValue then
  begin
    FAlignment := AValue;
    Refresh;
  end;
end;

procedure TrmASCIIField.SetDataField(const AValue: String);
begin
  FValueHolder.DataField := AValue;
end;

procedure TrmASCIIField.SetDescription(const Value: String);
begin
  inherited;
  if Parent <> nil then
    Parent.Refresh;
end;

procedure TrmASCIIField.SetFieldName(const AFieldName: String);
begin
  DataField := AFieldName;
end;

procedure TrmASCIIField.SetFormat(const Value: string);
begin
  FFormat := Value;
  Invalidate;
end;

procedure TrmASCIIField.SetName(const NewName: TComponentName);
begin
  inherited;
  if Parent <> nil then
    Parent.Refresh;
end;

procedure TrmASCIIField.SetSize(const AValue: Integer);
begin
  if AValue < 1 then
    Exit;

  FSize := AValue;
  if Parent <> nil then
    Parent.Refresh;
end;

procedure TrmASCIIField.SetValue(const AValue: Variant);
begin
  FValueHolder.Value := AValue;
  Invalidate;
end;


procedure TrmASCIIField.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
var
  i, NewInd: Integer;
  Fld: TrmASCIIField;
begin
  if Parent = nil then
    Exit;

  if ALeft < 0 then
    NewInd := 0

  else if ALeft > Parent.Width then
    NewInd := TrmASCIIRecord(Parent).FLastFieldIndex

  else
  begin
    NewInd := ItemIndex;

    for i := 0 to TrmASCIIRecord(Parent).FLastFieldIndex do
    begin
      Fld := TrmASCIIField(Parent.Items[i]);
      if (Fld.Left + Fld.Width > ALeft) and (Fld.Left + Fld.Width < ALeft + Width) or
         (Fld.Left < ALeft) and (Fld.Left + Fld.Width > ALeft + Width) then
      begin
        NewInd := Fld.ItemIndex;
        if ALeft < Left then
          Inc(NewInd);
        break;
      end;
    end;
  end;

  if NewInd > TrmASCIIRecord(Parent).FLastFieldIndex then
    NewInd := TrmASCIIRecord(Parent).FLastFieldIndex;

  ItemIndex := NewInd;
end;

function TrmASCIIField.GetShowText: String;
var
  h: String;
  Rec: TrmDataFieldInfoRec;
begin
  Result := inherited GetShowText;
  h := '';

  if DataField <> '' then
  begin
    Rec := GetDataFieldInfo;
    if Assigned(Rec.QBField) then
      h := '[' + Rec.Name + ']'
    else
      h := cErrorTag;
  end
  else if FormulaValue <> nil then
    h := cCalcTag
  else
    h := FormatValue(FFormat, Value);

  if h <> '' then
    Result := Result + #13 + h;
end;

function TrmASCIIField.FormulaTextFilter: TrmFMLFormula;
begin
  Result := Formulas.FormulaByAction(fatDataFilter, 'Text');
end;

function TrmASCIIField.GetDataField: String;
begin
  Result := FValueHolder.DataField;
end;

procedure TrmASCIIField.SetValueFromDataSource;
begin
  FValueHolder.SetValueFromDataSource;
end;


procedure TrmASCIIField.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnFilterText, cEventOnChangeValue]);
end;

procedure TrmASCIIField.DoOnChangeValue(var ANewValue: Variant);
begin
  ExecuteEventsHandler('OnChangeValue', [Integer(Addr(ANewValue))]);
end;

procedure TrmASCIIField.DoOnFilterText(var AText: String);
var
  V: Variant;
begin
  V := AText;
  ExecuteEventsHandler('OnFilterText', [Integer(Addr(V))]);
  AText := V;
end;

{ TrmASCIIRecord }

constructor TrmASCIIRecord.Create(AOwner: TComponent);
begin
  FLastFieldIndex := -1;
  inherited;
  FObjectTagClass := TrmdASCIIRecordTag;
  FQuery := TrmASCIIQuery.Create(Self);
end;

function TrmASCIIRecord.AfterCalculateBranch: Boolean;
var
  V: Variant;
begin
  if FQueryOpenedInternally then
    FQuery.Close;

  V := True;
  ExecuteEventsHandler('OnFinishBranch', [Integer(Addr(V))]);
  Result := V;

  if not Result then
  begin
    EraseItemData;
    RendStatus := rmASCIIRSNone;
  end
  else
    Result := inherited AfterCalculateBranch;
end;

procedure TrmASCIIRecord.DoAssignScreenPosition;
begin
  ResortItems;

  inherited;

  if FLastFieldIndex <> -1 then
    Width := Items[FLastFieldIndex].Left + Items[FLastFieldIndex].Width
  else
    Width := cItemHeight * 10;
end;

function TrmASCIIRecord.BeforeCalculateBranch: Boolean;
var
  V: Variant;
begin
  FQueryOpenedInternally := False;

  Result := inherited BeforeCalculateBranch;

  V := Result;
  ExecuteEventsHandler('OnStartBranch', [Integer(Addr(V))]);
  Result := V;

  if Result then
  begin
    if FQuery.PActive then
      FQuery.PFirst
    else if FQuery.SQL.Count > 0 then
    begin
      FQueryOpenedInternally := True;
      FQuery.Open;
    end;

    NotifyAggregateFunctions;
  end;
end;

procedure TrmASCIIRecord.DoCalculate;
var
  i, CurrentRecNbr: Integer;
  sLine: Variant;
  Delim, Qual: String;
  Fld: TrmASCIIField;
  pAccept: Variant;
begin
  Renderer.AppendASCIILine('');
  CurrentRecNbr := Renderer.ASCIILineCount;

  inherited;

  if ASCIIForm.ResultType = rmARTFixedLength then
  begin
    Delim := '';
    Qual := '';
  end
  else if ASCIIForm.ResultType = rmARTXls then
  begin
    Delim := Ascii2XlsDelimiter;
    Qual := Ascii2XlsQualifier;
  end
  else
  begin
    Delim := ASCIIForm.Delimiter;
    Qual := ASCIIForm.Qualifier;
  end;

  sLine := '';
  for i := 0 to FLastFieldIndex do
  begin
    Fld := TrmASCIIField(Items[i]);
    if Fld.RendStatus = rmASCIIRSCalculated then
    begin
      sLine := sLine + Qual + Fld.FRendText + Qual;
      if i <> FLastFieldIndex then
        sLine := sLine + Delim;
    end;
  end;

  pAccept := True;
  ExecuteEventsHandler('BeforeWriteASCIILine', [Integer(Addr(sLine)), Integer(Addr(pAccept))]);

  if pAccept then
    Renderer.UpdateASCIILine(sLine, CurrentRecNbr)
  else
    Renderer.DeleteASCIILine(CurrentRecNbr);
end;

procedure TrmASCIIRecord.DoCalculateBranch;
begin
  if FQuery.PActive then
  begin
    while not FQuery.PEof do
    begin
      GetDataFromDataSource;
      inherited;
      FQuery.PNext;
      if not FQuery.PEof then
        SetRendStatusChildrenToo(rmASCIIRSNone);
    end;
  end

  else
  begin
    GetDataFromDataSource;
    inherited;
  end;
end;

function TrmASCIIRecord.GetMasterFields: TrwStrings;
begin
  Result := FQuery.MasterFields;
end;

function TrmASCIIRecord.GetParamsFromVars: Boolean;
begin
  Result := FQuery.ParamsFromVars;
end;

function TrmASCIIRecord.GetQueryBuilderInfo: TrwQBQuery;
begin
  Result := FQuery.QueryBuilderInfo;
end;

procedure TrmASCIIRecord.RWLoaded;
begin
  inherited;

  FQuery.SQL.Text := QueryBuilderInfo.SQL;
  SyncMasterQueryLink;
  ResortItems;
end;


procedure TrmASCIIRecord.Notify(AEvent: TrmDesignEventType);
begin
  if AEvent in [rmdSelectChildObject] then
    ShowTag
  else if AEvent in [rmdDestroyChildResizer] then
    HideTag;

  inherited;
end;

procedure TrmASCIIRecord.PaintForeground(ACanvas: TrwCanvas);
var
  R: TRect;
begin
  with ACanvas do
  begin
    // border
    R := Zoomed(BoundsRect);

    if QueryBuilderInfo.IsEmpty then
      Brush.Color := cASCIIRecordColorEven
    else
      Brush.Color := cASCIIQueryRecordColorEven;

    if FLastFieldIndex = -1 then
      Brush.Style := bsSolid
    else
      Brush.Style := bsClear;
    Pen.Color := clGray;
    Pen.Style := psSolid;
    Pen.Width := 1;
    Rectangle(R);

    if FLastFieldIndex = -1 then
    begin
      SetDescriptionFont(ACanvas.Font);
      DrawText(GetShowText, R, DT_CENTER or DT_VCENTER or DT_SINGLELINE	or DT_NOPREFIX);
    end;
  end;
end;

procedure TrmASCIIRecord.ResortItems;
var
  Flds, Recs: TList;
  i: Integer;
begin
  Flds := TList.Create;
  Recs := TList.Create;
  try
    Flds.Capacity := FItems.Capacity;
    Recs.Capacity := FItems.Capacity;
    for i := 0 to ItemCount - 1 do
      if Items[i] is TrmASCIIField then
        Flds.Add(FItems[i])
      else
        Recs.Add(FItems[i]);

    // fields on first place
    for i := 0 to Flds.Count - 1 do
      FItems[i] := Flds[i];

    FLastFieldIndex := Flds.Count - 1;

    // records on second place
    for i := 0 to Recs.Count - 1 do
      FItems[FLastFieldIndex + 1 + i] := Recs[i];

  finally
    FreeAndNil(Flds);
    FreeAndNil(Recs);
  end;
end;

procedure TrmASCIIRecord.SetMasterFields(const Value: TrwStrings);
begin
  FQuery.MasterFields := Value;
end;

procedure TrmASCIIRecord.SetParamsFromVars(const Value: Boolean);
begin
  FQuery.ParamsFromVars := Value;
end;

procedure TrmASCIIRecord.SetQueryBuilderInfo(const Value: TrwQBQuery);
begin
  FQuery.QueryBuilderInfo.Assign(Value);
end;

procedure TrmASCIIRecord.SyncMasterQueryLink;
var
  i: Integer;
  qNotEmpt: Boolean;
begin
  qNotEmpt := QueryBuilderInfo.SelectedTables.Count <> 0;

  if (Owner is TrmASCIIRecord) and (TrmASCIIRecord(Owner).QueryBuilderInfo.SelectedTables.Count <> 0) and qNotEmpt then
    FQuery.MasterDataSource := TrmASCIIRecord(Owner).FQuery
  else
    FQuery.MasterDataSource := nil;

  for i := FLastFieldIndex + 1 to ItemCount - 1 do
    if (Items[i] is TrmASCIIRecord) then
      if qNotEmpt and (TrmASCIIRecord(Items[i]).QueryBuilderInfo.SelectedTables.Count <> 0) then
        TrmASCIIRecord(Items[i]).FQuery.MasterDataSource := FQuery
      else
        TrmASCIIRecord(Items[i]).FQuery.MasterDataSource := nil;
end;

function TrmASCIIRecord.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
  Result := inherited DesignBehaviorInfo;
  Result.MovingDirections := [rwDBIMDTop, rwDBIMDBottom];
end;

function TrmASCIIRecord.GetQuery: IrmQuery;
begin
  Result := FQuery;
end;


procedure TrmASCIIRecord.ApplyDsgnMoveLogic(const ALeft, ATop: Integer);
var
  i, NewInd, bInd, eInd: Integer;
  Rec: TrmASCIINode;
begin
  if Parent = nil then
    Exit;

  if Parent is TrmASCIIRecord then
    bInd := TrmASCIIRecord(Parent).FLastFieldIndex + 1
  else
    bInd := 0;
  eInd := Parent.ItemCount - 1;

  if ATop < Parent.TreeBranchRect.Top then
    NewInd := bInd

  else if ATop > Parent.TreeBranchRect.Bottom then
    NewInd := eInd

  else
  begin
    NewInd := ItemIndex;

    for i := bInd to eInd do
    begin
      Rec := TrmASCIINode(Parent.Items[i]);
      if (Rec.Top + Rec.Height > ATop) and (Rec.Top + Rec.Height < ATop + Height) then
      begin
        NewInd := Rec.ItemIndex;
        if ATop < Top then
          Inc(NewInd);
        break;
      end

      else if (Rec.Top > ATop) and (Rec.Top < ATop + Height) then
      begin
        NewInd := Rec.ItemIndex;
        if ATop > Top then
          Dec(NewInd);
        break;
      end

      else if Rec.Top + Rec.Height < ATop then
        NewInd := Rec.ItemIndex + 1

      else if Rec.Top > ATop + Height then
        NewInd := Rec.ItemIndex;
    end;
  end;

  if NewInd > eInd then
    NewInd := eInd;

  ItemIndex := NewInd;
end;

procedure TrmASCIIRecord.GetDataFromDataSource;
var
  i: Integer;
begin
  for i := 0 to FLastFieldIndex do
    TrmASCIIField(Items[i]).SetValueFromDataSource;
end;

procedure TrmASCIIRecord.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnStartBranch, cEventOnFinishBranch, cEventBeforeWriteASCIILine]);
end;

function TrmASCIIRecord.PFieldByIndex(AIndex: Variant): Variant;
begin
  if AIndex > FLastFieldIndex then
    Result := Integer(nil)
  else
    Result := Integer(Pointer(Items[AIndex]));
end;

function TrmASCIIRecord.PFieldCount: Variant;
begin
  Result := FLastFieldIndex + 1;
end;

function TrmASCIIRecord.PFieldIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := 0 to FLastFieldIndex do
    if SameText(Items[i].Name, AName) then
    begin
      Result := i;
      break;
    end;
end;

function TrmASCIIRecord.PSubRecordByIndex(AIndex: Variant): Variant;
var
  i: Integer;
begin
  i := AIndex + (FLastFieldIndex + 1);

  if (i <= FLastFieldIndex) or (i >= ItemCount) then
    Result := Integer(nil)
  else
    Result := Integer(Pointer(Items[i]));
end;

function TrmASCIIRecord.PSubRecordCount: Variant;
begin
  Result := ItemCount - (FLastFieldIndex + 1);
end;

function TrmASCIIRecord.PSubRecordIndexByName(AName: Variant): Variant;
var
  i: Integer;
begin
  Result := -1;

  for i := FLastFieldIndex + 1 to ItemCount do
    if SameText(Items[i].Name, AName) then
    begin
      Result := i - (FLastFieldIndex + 1);
      break;
    end;
end;

function TrmASCIIRecord.PIsQueryDriven: Variant;
begin
  Result := not QueryBuilderInfo.IsEmpty;
end;

function TrmASCIIRecord.GetDataSource: TrwDataSet;
begin
  Result := FQuery;
end;



destructor TrmASCIIRecord.Destroy;
begin
  FreeAndNil(FQuery);
  inherited;
end;


{ TrmASCIIQuery }

constructor TrmASCIIQuery.Create(AOwner: TComponent);
begin
  inherited Create(nil);
  FNode := TrmASCIIRecord(AOwner);
  Name := 'Query';
  FNode.FQuery := Self;
  ParamsFromVars := True;
  FAllowToNotifyFormulas := False;
end;


destructor TrmASCIIQuery.Destroy;
begin
  if Assigned(FNode) then
    FNode.FQuery := nil;
  inherited;
end;


procedure TrmASCIIQuery.OnAssignQBInfo(Sender: TObject);
begin
  inherited;
  FNode.SyncMasterQueryLink;
  if FNode.IsDesignMode then
  begin
    FNode.GetDesigner.Notify(rmdQueryChanged, Integer(Pointer(Self as IrmDesignObject)));
    FNode.Invalidate;    
  end;
end;

{ TrmdASCIIRecordTag }

procedure TrmdASCIIRecordTag.CorrectTagBounds(var ATagRect: TRect);
begin
  OffsetRect(ATagRect, 0, ATagRect.Top - ATagRect.Bottom - Zoomed(1));
end;

end.
