object rwQBJoinEditor: TrwQBJoinEditor
  Left = 533
  Top = 386
  ActiveControl = cbField2
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Add Join'
  ClientHeight = 232
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 4
    Top = 2
    Width = 291
    Height = 197
    Caption = 'Join Description'
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 25
      Width = 36
      Height = 13
      Caption = 'Table 1'
    end
    object Label1: TLabel
      Left = 8
      Top = 54
      Width = 22
      Height = 13
      Caption = 'Field'
    end
    object Label2: TLabel
      Left = 8
      Top = 139
      Width = 36
      Height = 13
      Caption = 'Table 2'
    end
    object Label4: TLabel
      Left = 8
      Top = 167
      Width = 22
      Height = 13
      Caption = 'Field'
    end
    object Label5: TLabel
      Left = 8
      Top = 96
      Width = 24
      Height = 13
      Caption = 'Join'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbTable1: TComboBox
      Left = 51
      Top = 22
      Width = 228
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbTable1Change
    end
    object cbField1: TComboBox
      Left = 51
      Top = 50
      Width = 228
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
    object cbTable2: TComboBox
      Left = 51
      Top = 136
      Width = 228
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnChange = cbTable1Change
    end
    object cbField2: TComboBox
      Left = 51
      Top = 164
      Width = 228
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
    end
    object cbJoin: TComboBox
      Left = 51
      Top = 92
      Width = 228
      Height = 21
      Style = csDropDownList
      DropDownCount = 3
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'INNER JOIN'
        'OUTER (Table 1)'
        'OUTER (Table 2)')
    end
  end
  object btnOK: TButton
    Left = 155
    Top = 205
    Width = 65
    Height = 23
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 230
    Top = 205
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
