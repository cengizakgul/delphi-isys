object rwFormPreview: TrwFormPreview
  Left = 372
  Top = 220
  Width = 808
  Height = 560
  Caption = 'RW Report Preview'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline Preview: TrwOneRepPreview
    Left = 0
    Top = 0
    Width = 800
    Height = 526
    Align = alClient
    AutoScroll = False
    Constraints.MinHeight = 230
    Constraints.MinWidth = 480
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    inherited ctbTools: TControlBar
      Width = 800
      inherited tlbTools: TToolBar
        Width = 726
      end
    end
    inherited sbxPreview: TScrollBox
      Width = 800
      Height = 500
    end
    inherited pnlBrowser: TPanel
      Top = 80
    end
    inherited aclActions: TActionList
      inherited actOpen: TAction
        OnExecute = PreviewactOpenExecute
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 258
    Top = 140
  end
  object PopupMenu: TPopupMenu
    Left = 200
    Top = 216
    object Close1: TMenuItem
      Caption = 'Terminate Process'
      OnClick = Close1Click
    end
  end
end
