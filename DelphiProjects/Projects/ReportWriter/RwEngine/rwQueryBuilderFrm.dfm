object rwQueryBuilder: TrwQueryBuilder
  Left = 294
  Top = 240
  AutoScroll = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Query Builder'
  ClientHeight = 585
  ClientWidth = 961
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 236
    Top = 26
    Height = 559
    AutoSnap = False
    MinSize = 100
  end
  object Panel1: TPanel
    Left = 239
    Top = 26
    Width = 722
    Height = 559
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter2: TSplitter
      Left = 0
      Top = 287
      Width = 722
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 100
    end
    object pcFields: TPageControl
      Left = 0
      Top = 290
      Width = 722
      Height = 269
      ActivePage = tsSelFlds
      Align = alBottom
      MultiLine = True
      TabOrder = 0
      OnChange = pcFieldsChange
      OnDragOver = pcFieldsDragOver
      object tsSelFlds: TTabSheet
        Caption = 'Showing Fields'
        object lvFields: TListView
          Left = 0
          Top = 0
          Width = 714
          Height = 241
          Align = alClient
          Columns = <
            item
              AutoSize = True
              Caption = 'Field'
            end
            item
              Caption = 'Type'
              Width = 60
            end
            item
              Caption = 'Field Alias'
              Width = 120
            end
            item
              Caption = 'Table'
              Width = 120
            end>
          DragMode = dmAutomatic
          ReadOnly = True
          RowSelect = True
          PopupMenu = pmFields
          TabOrder = 0
          ViewStyle = vsReport
          OnClick = lvFieldsClick
          OnDblClick = lvFieldsDblClick
          OnEndDrag = lvFieldsEndDrag
          OnDragOver = lvFieldsDragOver
          OnSelectItem = lvFieldsSelectItem
        end
      end
      object tsSort: TTabSheet
        Caption = 'Sorting'
        ImageIndex = 1
        object lvSort: TListView
          Left = 0
          Top = 0
          Width = 714
          Height = 241
          Align = alClient
          Columns = <
            item
              AutoSize = True
              Caption = 'Field'
            end
            item
              Caption = 'Direction'
              Width = 60
            end>
          DragMode = dmAutomatic
          ReadOnly = True
          RowSelect = True
          PopupMenu = pmSort
          TabOrder = 0
          ViewStyle = vsReport
          OnClick = lvSortClick
          OnDblClick = lvSortDblClick
          OnEndDrag = lvSortEndDrag
          OnDragOver = lvSortDragOver
          OnSelectItem = lvFieldsSelectItem
        end
      end
      object tsMisc: TTabSheet
        Caption = 'Misc'
        ImageIndex = 2
        object chBDistinct: TCheckBox
          Left = 6
          Top = 6
          Width = 222
          Height = 17
          Caption = 'Result should be distinct by selected fields'
          TabOrder = 0
          OnClick = chBDistinctClick
        end
        object pnMainSQL: TPanel
          Left = 0
          Top = 29
          Width = 517
          Height = 28
          BevelOuter = bvNone
          TabOrder = 1
          object lSQLType: TLabel
            Left = 6
            Top = 6
            Width = 75
            Height = 13
            Caption = 'Statement Type'
          end
          object lTmpTbl: TLabel
            Left = 269
            Top = 6
            Width = 96
            Height = 13
            Caption = 'Buffer'#39's Table Name'
          end
          object cbSQLType: TComboBox
            Left = 109
            Top = 3
            Width = 140
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnChange = cbSQLTypeChange
            Items.Strings = (
              'SELECT'
              'INSERT'
              'UPDATE'
              'DELETE')
          end
          object cbTmpTbl: TComboBox
            Left = 372
            Top = 3
            Width = 140
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 0
            TabOrder = 1
            OnChange = cbTmpTblChange
          end
        end
        object gbMacros: TGroupBox
          Left = 6
          Top = 60
          Width = 507
          Height = 173
          Caption = 'Macros (Custom Text)'
          TabOrder = 2
          object pnlInsertMacro: TPanel
            Left = 2
            Top = 15
            Width = 503
            Height = 29
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object lInsMacro: TLabel
              Left = 8
              Top = 14
              Width = 40
              Height = 13
              Caption = 'INSERT'
            end
            object edMacroInsert: TEdit
              Left = 74
              Top = 8
              Width = 418
              Height = 21
              TabOrder = 0
              OnExit = edMacroSelectExit
            end
          end
          object pnlSelectMacro: TPanel
            Left = 2
            Top = 44
            Width = 503
            Height = 29
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label9: TLabel
              Left = 8
              Top = 14
              Width = 41
              Height = 13
              Caption = 'SELECT'
            end
            object edMacroSelect: TEdit
              Left = 74
              Top = 8
              Width = 418
              Height = 21
              TabOrder = 0
              OnExit = edMacroSelectExit
            end
          end
          object pnlFromMacro: TPanel
            Left = 2
            Top = 73
            Width = 503
            Height = 29
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object Label5: TLabel
              Left = 8
              Top = 14
              Width = 31
              Height = 13
              Caption = 'FROM'
            end
            object edMacroFrom: TEdit
              Left = 74
              Top = 8
              Width = 418
              Height = 21
              TabOrder = 0
              OnExit = edMacroSelectExit
            end
          end
          object pnlWhereMacro: TPanel
            Left = 2
            Top = 102
            Width = 503
            Height = 29
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object Label6: TLabel
              Left = 8
              Top = 14
              Width = 41
              Height = 13
              Caption = 'WHERE'
            end
            object edMacroWhere: TEdit
              Left = 74
              Top = 8
              Width = 418
              Height = 21
              TabOrder = 0
              OnExit = edMacroSelectExit
            end
          end
          object pnlOrderByMacro: TPanel
            Left = 2
            Top = 131
            Width = 503
            Height = 29
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object Label7: TLabel
              Left = 8
              Top = 14
              Width = 56
              Height = 13
              Caption = 'ORDER BY'
            end
            object edMacroOrder: TEdit
              Left = 74
              Top = 8
              Width = 418
              Height = 21
              TabOrder = 0
              OnExit = edMacroSelectExit
            end
          end
        end
      end
      object tsSQL: TTabSheet
        Caption = 'SQL'
        ImageIndex = 4
      end
      object tsResult: TTabSheet
        Caption = 'Data Result'
        ImageIndex = 5
        object Splitter5: TSplitter
          Left = 0
          Top = 191
          Width = 714
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          MinSize = 5
        end
        object memPlan: TMemo
          Left = 0
          Top = 194
          Width = 714
          Height = 47
          Align = alBottom
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object pcSQL: TPageControl
      Left = 0
      Top = 0
      Width = 722
      Height = 287
      Align = alClient
      Anchors = [akLeft, akTop, akRight]
      MultiLine = True
      TabOrder = 1
    end
  end
  object ctbBar: TControlBar
    Left = 0
    Top = 0
    Width = 961
    Height = 26
    Align = alTop
    AutoSize = True
    BevelEdges = []
    DockSite = False
    TabOrder = 0
    object ToolBar2: TToolBar
      Left = 11
      Top = 2
      Width = 285
      Height = 22
      Align = alNone
      AutoSize = True
      Caption = 'ToolBar1'
      EdgeBorders = []
      Flat = True
      Images = ilPict
      TabOrder = 0
      Wrapable = False
      object ToolButton11: TToolButton
        Left = 0
        Top = 0
        Action = actClose
        Caption = '`'
      end
      object ToolButton16: TToolButton
        Left = 23
        Top = 0
        Action = actCancel
      end
      object ToolButton10: TToolButton
        Left = 46
        Top = 0
        Width = 8
        Caption = 'ToolButton10'
        ImageIndex = 22
        Style = tbsSeparator
      end
      object ToolButton9: TToolButton
        Left = 54
        Top = 0
        Action = actOpen
      end
      object ToolButton12: TToolButton
        Left = 77
        Top = 0
        Action = actSave
      end
      object ToolButton17: TToolButton
        Left = 100
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 12
        Style = tbsSeparator
      end
      object ToolButton21: TToolButton
        Left = 108
        Top = 0
        Hint = 'Add SubQuery'
        Caption = 'Add SubQuery'
        DropdownMenu = pmQueryType
        ImageIndex = 15
      end
      object ToolButton1: TToolButton
        Left = 131
        Top = 0
        Action = actDelSUBQuery
      end
      object ToolButton20: TToolButton
        Left = 154
        Top = 0
        Width = 8
        Caption = 'ToolButton14'
        ImageIndex = 16
        Style = tbsSeparator
      end
      object ToolButton13: TToolButton
        Left = 162
        Top = 0
        Action = actSearch
      end
      object ToolButton18: TToolButton
        Left = 185
        Top = 0
        Action = actTable
      end
      object ToolButton19: TToolButton
        Left = 208
        Top = 0
        Action = actOpenSQL
      end
      object ToolButton8: TToolButton
        Left = 231
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 17
        Style = tbsSeparator
      end
      object btnHidePnls: TToolButton
        Left = 239
        Top = 0
        Action = actHidePanels
        Style = tbsCheck
      end
      object btnView: TToolButton
        Left = 262
        Top = 0
        Action = actView
        Style = tbsCheck
      end
    end
    object tbDictEdit: TToolBar
      Left = 317
      Top = 2
      Width = 177
      Height = 22
      Align = alNone
      AutoSize = True
      EdgeBorders = []
      Flat = True
      Images = ilPict
      TabOrder = 1
      Visible = False
      Wrapable = False
      object ToolButton2: TToolButton
        Left = 0
        Top = 0
        Action = actAddDict
      end
      object ToolButton3: TToolButton
        Left = 23
        Top = 0
        Action = actEditDict
      end
      object ToolButton4: TToolButton
        Left = 46
        Top = 0
        Action = actDelDict
      end
      object ToolButton15: TToolButton
        Left = 69
        Top = 0
        Width = 8
        Caption = 'ToolButton15'
        ImageIndex = 15
        Style = tbsSeparator
      end
      object ToolButton14: TToolButton
        Left = 77
        Top = 0
        Action = actConsts
      end
      object ToolButton5: TToolButton
        Left = 100
        Top = 0
        Width = 8
        Caption = 'ToolButton5'
        ImageIndex = 15
        Style = tbsSeparator
      end
      object ToolButton6: TToolButton
        Left = 108
        Top = 0
        Action = actExpDict
      end
      object ToolButton7: TToolButton
        Left = 131
        Top = 0
        Action = actImpDict
      end
      object ToolButton22: TToolButton
        Left = 154
        Top = 0
        Action = actSyncDD
      end
    end
    object tbCustom: TToolBar
      Left = 513
      Top = 2
      Width = 40
      Height = 22
      Align = alNone
      EdgeBorders = []
      Flat = True
      Images = ilPict
      TabOrder = 2
      Visible = False
      Wrapable = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 26
    Width = 236
    Height = 559
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter3: TSplitter
      Left = 0
      Top = 114
      Width = 236
      Height = 3
      Cursor = crVSplit
      Align = alTop
      AutoSnap = False
      MinSize = 50
    end
    object Label4: TLabel
      Left = 0
      Top = 0
      Width = 236
      Height = 17
      Align = alTop
      AutoSize = False
      Caption = 'Query Structure'
      Layout = tlCenter
    end
    object Splitter4: TSplitter
      Left = 0
      Top = 481
      Width = 236
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
    end
    object pnlTables: TPanel
      Left = 0
      Top = 117
      Width = 236
      Height = 364
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 236
        Height = 17
        Align = alTop
        AutoSize = False
        Caption = 'All Tables'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
      object tvTables: TTreeView
        Left = 0
        Top = 17
        Width = 236
        Height = 347
        Align = alClient
        DragMode = dmAutomatic
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Images = ilPict
        Indent = 19
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnChange = tvChildTablesChange
        OnClick = tvTablesClick
        OnCompare = tvChildTablesCompare
        OnEndDrag = tvTablesEndDrag
      end
    end
    object pnlMasterTables: TPanel
      Left = 0
      Top = 117
      Width = 236
      Height = 364
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      DesignSize = (
        236
        364)
      object Label3: TLabel
        Left = 0
        Top = 2
        Width = 71
        Height = 14
        AutoSize = False
        Caption = 'Child tables of:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lCurrTable: TLabel
        Left = 72
        Top = 2
        Width = 161
        Height = 14
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Table'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object tvChildTables: TTreeView
        Left = -1
        Top = 17
        Width = 236
        Height = 347
        Anchors = [akLeft, akTop, akRight, akBottom]
        DragMode = dmAutomatic
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Images = ilPict
        Indent = 19
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        OnChange = tvChildTablesChange
        OnClick = tvTablesClick
        OnCompare = tvChildTablesCompare
        OnEndDrag = tvTablesEndDrag
      end
    end
    object tvQuery: TTreeView
      Left = 0
      Top = 17
      Width = 236
      Height = 97
      Align = alTop
      HideSelection = False
      Images = ilPict
      Indent = 19
      TabOrder = 2
      OnChange = tvQueryChange
      OnContextPopup = tvQueryContextPopup
      OnDeletion = tvQueryDeletion
      OnEdited = tvQueryEdited
    end
    object memDescr: TMemo
      Left = 0
      Top = 484
      Width = 236
      Height = 75
      Align = alBottom
      ReadOnly = True
      TabOrder = 3
    end
  end
  object ActionList1: TActionList
    Images = ilPict
    Left = 344
    Top = 60
    object actClose: TAction
      Caption = 'actClose'
      Hint = 'Save&Close'
      ImageIndex = 4
      OnExecute = actCloseExecute
    end
    object actNewTable: TAction
      Caption = 'Add Table'
      Hint = 'Add new table'
      ImageIndex = 5
      OnExecute = actNewTableExecute
    end
    object actSearch: TAction
      Caption = 'Search'
      Hint = 'Search Field/Table'
      ImageIndex = 7
      ShortCut = 16454
      OnExecute = actSearchExecute
    end
    object actNewField: TAction
      Caption = 'Add Field'
      OnExecute = actNewFieldExecute
    end
    object DelField: TAction
      Caption = 'Remove Field'
      OnExecute = DelFieldExecute
    end
    object actCancel: TAction
      Caption = 'Cancel'
      Hint = 'Cancel&Close'
      ImageIndex = 8
      OnExecute = actCancelExecute
    end
    object actEditField: TAction
      Caption = 'Edit...'
      OnExecute = actEditFieldExecute
    end
    object actDelSortField: TAction
      Caption = 'Remove Field'
      ShortCut = 46
      OnExecute = actDelSortFieldExecute
    end
    object actEditSortField: TAction
      Caption = 'Edit...'
      OnExecute = actEditSortFieldExecute
    end
    object actAddSortField: TAction
      Caption = 'Add...'
      ShortCut = 45
      OnExecute = actAddSortFieldExecute
    end
    object actAddField: TAction
      Caption = 'Add...'
      ShortCut = 45
      OnExecute = actAddFieldExecute
    end
    object actEditDict: TAction
      Caption = 'Edit'
      Enabled = False
      Hint = 'Edit selected table'
      ImageIndex = 9
      OnExecute = actEditDictExecute
    end
    object actAddDict: TAction
      Caption = 'Add'
      Enabled = False
      Hint = 'Create new table'
      ImageIndex = 10
      OnExecute = actAddDictExecute
    end
    object actDelDict: TAction
      Caption = 'Delete'
      Enabled = False
      Hint = 'Delete selected table'
      ImageIndex = 11
      OnExecute = actDelDictExecute
    end
    object actTable: TAction
      Caption = 'OpenTable'
      Hint = 'Content of Table'
      ImageIndex = 12
      OnExecute = actTableExecute
    end
    object actExpDict: TAction
      Caption = 'Export Dictionary'
      Enabled = False
      Hint = 'Export Dictionary'
      ImageIndex = 13
      OnExecute = actExpDictExecute
    end
    object actImpDict: TAction
      Caption = 'Import Dictionary'
      Enabled = False
      Hint = 'Import Dictionary'
      ImageIndex = 14
      OnExecute = actImpDictExecute
    end
    object actAddSUBQuery: TAction
      Caption = 'Add SubQuery'
      Hint = 'Add SubQuery'
      OnExecute = actAddSUBQueryExecute
    end
    object actAddParentQuery: TAction
      Caption = 'Add Parent Query'
      Hint = 'Add Parent Query'
      OnExecute = actAddParentQueryExecute
    end
    object actAddJoin: TAction
      Caption = 'Add Join'
      OnExecute = actAddJoinExecute
    end
    object actOpenSQL: TAction
      Caption = 'Open SQL'
      Hint = 'SQL Data Result'
      ImageIndex = 16
      ShortCut = 120
      OnExecute = actOpenSQLExecute
    end
    object actDelSUBQuery: TAction
      Caption = 'Delete SubQuery'
      Hint = 'Delete SubQuery'
      ImageIndex = 17
      OnExecute = actDelSUBQueryExecute
    end
    object actEdSubQueryDescr: TAction
      Caption = 'Edit SubQuery Description'
      OnExecute = actEdSubQueryDescrExecute
    end
    object actHidePanels: TAction
      Hint = 'Hide/Show Panels'
      ImageIndex = 21
      ShortCut = 16456
      OnExecute = actHidePanelsExecute
    end
    object actSave: TAction
      Caption = 'actSave'
      Hint = 'Save Query to File'
      ImageIndex = 23
      OnExecute = actSaveExecute
    end
    object actOpen: TAction
      Caption = 'actOpen'
      Hint = 'Load Query from File'
      ImageIndex = 22
      OnExecute = actOpenExecute
    end
    object actMaximizeResult: TAction
      Caption = 'Maximize Data Result'
      ShortCut = 16461
      OnExecute = actMaximizeResultExecute
    end
    object actConsts: TAction
      Caption = 'Constants'
      Enabled = False
      Hint = 'Evolution Constants'
      ImageIndex = 12
      OnExecute = actConstsExecute
    end
    object actMakeUnion: TAction
      Caption = 'Make UNION'
      OnExecute = actMakeUnionExecute
    end
    object actSyncDD: TAction
      Caption = 'Synchronize Data Dictionary'
      Enabled = False
      Hint = 'Synchronize Data Dictionary'
      ImageIndex = 27
      OnExecute = actSyncDDExecute
    end
    object actView: TAction
      Caption = 'actView'
      Hint = 'Wizard View'
      ImageIndex = 30
      OnExecute = actViewExecute
    end
  end
  object pmFields: TPopupMenu
    Left = 332
    Top = 402
    object Add2: TMenuItem
      Action = actAddField
    end
    object Edit1: TMenuItem
      Action = actEditField
      ShortCut = 13
    end
    object RemoveField1: TMenuItem
      Action = DelField
      ShortCut = 46
    end
  end
  object pmSort: TPopupMenu
    Left = 288
    Top = 402
    object Add1: TMenuItem
      Action = actAddSortField
    end
    object Edit2: TMenuItem
      Action = actEditSortField
      ShortCut = 13
    end
    object MenuItem1: TMenuItem
      Action = actDelSortField
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'rwd'
    Filter = 'Data Dictionary resources (*.rwd)|*.rwd|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export Data Dictionary'
    Left = 346
    Top = 95
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'rwd'
    Filter = 'Data Dictionary resources (*.rwd)|*.rwd|All Files (*.*)|*.*'
    Title = 'Import Data Dictionary'
    Left = 378
    Top = 95
  end
  object pmSubQuery: TPopupMenu
    AutoPopup = False
    Images = DsgnRes.ilPict
    Left = 120
    Top = 82
    object AddQuery1: TMenuItem
      Caption = 'Add'
      ImageIndex = 15
      object AddSubQuery1: TMenuItem
        Action = actAddSUBQuery
        Caption = 'SubQuery'
        SubMenuImages = DsgnRes.ilPict
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ParentQuery2: TMenuItem
        Action = actAddParentQuery
        Caption = 'Parent Query'
      end
      object MakeUNION1: TMenuItem
        Action = actMakeUnion
        Caption = 'Parent Query (UNION)'
      end
    end
    object Edit3: TMenuItem
      Action = actDelSUBQuery
    end
    object EditSubQueryDescription1: TMenuItem
      Action = actEdSubQueryDescr
      Caption = 'Edit Query Description'
    end
  end
  object pmQueryType: TPopupMenu
    Left = 120
    Top = 50
    object AddSubQuery2: TMenuItem
      Action = actAddSUBQuery
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ParentQuery1: TMenuItem
      Action = actAddParentQuery
    end
    object MakeUNION2: TMenuItem
      Action = actMakeUnion
      Caption = 'Add Parent Query (UNION)'
    end
  end
  object sdQuery: TSaveDialog
    DefaultExt = 'rwq'
    Filter = 'Query Builder Query (*.rwq)|*.rwq|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save Query to File'
    Left = 346
    Top = 127
  end
  object odQuery: TOpenDialog
    DefaultExt = 'rwq'
    Filter = 'Data Dictionary resources (*.rwq)|*.rwq|All Files (*.*)|*.*'
    Title = 'Load Query from File'
    Left = 378
    Top = 127
  end
  object pmJoin: TPopupMenu
    OnPopup = pmJoinPopup
    Left = 432
    Top = 120
    object Jointype1: TMenuItem
      Caption = 'Join type'
      object miINNER: TMenuItem
        Caption = 'INNER'
        Checked = True
        RadioItem = True
        OnClick = miOUTERrClick
      end
      object miOUTERl: TMenuItem
        Caption = 'OUTER'
        RadioItem = True
        OnClick = miOUTERrClick
      end
      object miOUTERr: TMenuItem
        Caption = 'OUTER'
        RadioItem = True
        OnClick = miOUTERrClick
      end
    end
    object RemoveJoin1: TMenuItem
      Caption = 'Remove Join'
      OnClick = RemoveJoin1Click
    end
  end
  object pmPasteTable: TPopupMenu
    OnPopup = pmPasteTablePopup
    Left = 431
    Top = 154
    object miPasteTable: TMenuItem
      Caption = 'Paste Table'
      OnClick = miPasteTableClick
    end
  end
  object pmResults: TPopupMenu
    Left = 499
    Top = 429
    object Maximize1: TMenuItem
      Action = actMaximizeResult
    end
  end
  object ilPict: TImageList
    Left = 280
    Top = 64
    Bitmap = {
      494C010120002200040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000009000000001002000000000000090
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000080808000008080000080
      80000080800000000000808080008080800000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      00000000000000000000FF00000000000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800000000000000000000000000000808000008080000080
      800000808000008080000000000080808000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF000000000000000000000000000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000000000000
      0000808080008080800080808000808080000080800000808000008080000080
      800000808000008080000000000080808000000000000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000008080800080808000808080000080800000FFFF00008080000080
      80000080800000808000008080000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF0000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF0000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000000000000000FF000000FF000000
      FF000000FF000000000000000000808080000080800000FFFF0000FFFF000080
      8000008080000080800000808000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000FFFF00000000FF00FFFF000000000000FFFF000000000000000000000000
      000000000000FF000000FF000000FF000000000000000000FF000000FF000000
      FF0000008000000080000000800000000000000000008080800000FFFF0000FF
      FF00008080000080800000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000080000000FF000000FF00
      0000FF000000FF000000FF000000FF0000000000000000000000000000000000
      000000000000FF00000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000FFFF00000000FF00FFFF0000000000000000
      0000FF000000FF000000FF00000000000000000000000000FF000000FF000000
      8000000080000000800000008000000000000000000000000000008080000080
      8000008080000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000000000000000FF00000080000000FF00
      0000FF000000FF000000FF000000FF000000FF00000000000000000000000000
      00000000000000000000FF000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000000000FF00
      0000FF000000FF0000000000000000000000000000000000FF000000FF000000
      8000000080000000000000000000000000008080800080808000808080008080
      8000808080000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FF0000008000
      0000FF000000FF00000000000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000FF000000FF00
      0000FF000000000000000000000000000000000000000000FF00000080000000
      000000000000000000000000000000000000FF000000FF000000FF0000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      000080000000FF000000FF000000FF00000000000000FF000000FF0000000000
      00000000000000000000FF000000000000000000000000000000000000000000
      0000FFFF00000000FF00FFFF00000000000000000000FF000000FF000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000080000000FF000000FF000000FF000000FF00
      0000000000008080800000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00000080000000FF0000000000000000000000FF000000FF000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF00000000000000000000FF000000FF000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
      00000000000080808000000000000000000000000000000000000000FF000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FF00000080000000FF000000FF000000FF000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FF0000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
      000000000000808080000000000000000000000000000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      00000000000000000000FF00000080000000FF000000FF000000FF000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C0C0C00000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
      000000000000808080000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00000080000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C0C0C00000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF00000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      80000000000000000000FFFF0000FFFF0000FFFFFF00FFFF0000FF0000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000FFFF00000000FF00FFFF0000FFFF000000000000FF000000FFFF00000000
      FF00FFFF00000000000000000000808080000000000000000000000000000000
      0000000000008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000FF000000FF00000080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000FF00FFFF0000FFFF0000FFFFFF00FFFF0000FF0000000000
      FF00FF000000FFFF0000FF000000000000000000000000000000000000000000
      0000000000000000000000000000808080008080800000000000808080008080
      8000808080008080800000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FF000000FF000000FFFFFF00FFFFFF008080
      800000000000000000000000000000000000FF000000FF000000FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF000000FF000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000FF00FFFF0000FFFF000000000000FF000000FFFF00000000
      FF00FFFF0000FF000000FFFF0000000000000000000000000000000000000000
      000000FF000000FF000000800000000000008080800000FF000000FF000000FF
      000000FF0000808080000000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0C0008080
      800000000000000000000000000000000000FF0000000000000000000000FF00
      000000000000000000000000FF00000000000000000000000000000000000000
      0000FF0000000000000000000000FF0000000000000000000000FFFF0000FFFF
      0000FFFF00000000FF00FFFF0000FFFF0000FFFFFF00FFFF0000FF0000000000
      FF00FF000000FFFF0000FF0000000000000000000000000000000000000000FF
      000000FF000000FF00000080000000000000808080000080000000FF000000FF
      000000FF00008080800000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FF000000FF0000008080
      800000000000000000000000000000000000FF0000000000000000000000FF00
      00000000000000000000000000000000FF000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000FF00FFFF0000FFFF000000000000FF000000FFFF00000000
      FF00FFFF0000FF000000FFFF000000000000000000008080800000FF000000FF
      000000FF0000808080000080000000000000000000000000000000FF000000FF
      000000FF0000808080000000000000000000808080008080800080808000C0C0
      C000C0C0C000FF000000FF000000C0C0C000C0C0C000FF000000FF0000008080
      800080808000808080008080800000000000FF0000000000000000000000FF00
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000FF0000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000FF008080800080808000FFFFFF0080808000808080000000
      FF00FF000000FFFF0000FF0000000000000000000000000000008080800000FF
      0000808080008080800000800000008000000080000000FF000000FF000000FF
      000000FF00008080800000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00FF000000FF000000FFFFFF00C0C0C000FF000000FF0000008080
      8000FFFFFF00FFFFFF008080800000000000FF000000FF000000FF0000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000FF0000000000000000000000000000000000000000000000FFFF0000FFFF
      0000808080000000FF00FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF00000000
      FF0080808000FF000000FFFF0000000000000000000000000000000000008080
      80000000000080808000008000000080000000FF000000FF000000FF00000000
      000000FF00000000000000000000000000008080800080808000808080008080
      80008080800080808000FF000000FF000000FF000000FF000000808080008080
      8000C0C0C000C0C0C0008080800000000000FF0000000000000000000000FF00
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000FF0000000000000000000000000000000000000000000000808080008080
      8000FFFFFF00FFFF00000000FF00FFFF0000FFFFFF00FFFF00000000FF00FFFF
      0000FFFFFF008080800080808000000000000000000000000000000000000000
      000000000000808080000080000000FF000000FF000000FF0000000000000000
      0000000000000000000000000000000000008080800000FFFF008080800000FF
      FF0000FFFF008080800000FFFF0000FFFF008080800000FFFF0000FFFF008080
      8000FFFFFF00FFFFFF008080800000000000FF0000000000000000000000FF00
      000000000000000000000000000000000000000000000000FF00000000000000
      0000FF0000000000000000000000FF0000000000000000000000FFFF0000FFFF
      FF00FFFF0000FFFFFF00FFFF00000000FF000000FF000000FF00FFFF0000FFFF
      FF00FFFF0000FFFFFF00FFFF0000000000000000000000000000000000000000
      0000000000008080800000FF000000FF000000FF000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000C0C0C000C0C0C0008080800000000000FF000000FF000000FF0000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      000000000000FF000000FF000000000000000000000080808000000000000000
      0000FFFFFF00FFFF00000000FF00FFFFFF00FFFF0000FFFFFF000000FF00FFFF
      0000FFFFFF000000000000000000808080000000000000000000000000000000
      000000000000000000008080800000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      800000FFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0
      C000FFFFFF00FFFFFF0080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      80000000000000000000FFFFFF00FFFF0000FFFFFF00FFFF0000FFFFFF000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000808080000000000000000000FFFF000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      800000FFFF008080800000FFFF0000FFFF008080800000FFFF0000FFFF008080
      800000FFFF0000FFFF0080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000808080000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      800080808000808080000000000000000000808080000000000000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF0000000000FFFF0000FFFF00000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080000000
      000000000000000000000000000000000000808080000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000808080000000000000000000000000000000
      000000000000FFFFFF000000000000FFFF00FFFFFF0000FFFF000000000000FF
      FF00FFFFFF0000FFFF0000000000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      800000000000000000000000000000000000000000000080000000800000C0C0
      C0000080000000800000C0C0C000C0C0C000C0C0C000C0C0C000008000000080
      00000080000000000000000000000000000000000000FFFFFF0000FFFF000000
      000000FFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000000000000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00000000000000000000FFFF00000000000080
      8000008080000080800000808000008080000080800000808000008080000000
      000080808000000000000000000000000000008000000080000000800000C0C0
      C0000080000000800000C0C0C000C0C0C000C0C0C000C0C0C000008000000080
      00000080000000000000000000000000000000000000FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF00000000000000000000FFFF0000FFFF000000
      0000008080000080800000808000008080000080800000808000008080000080
      800000000000808080000000000000000000008000000080000000800000C0C0
      C0000080000000800000C0C0C000C0C0C000C0C0C000C0C0C000008000000080
      0000008000000000000000000000000000008080800000000000FFFFFF00FFFF
      FF00FFFFFF000000000080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF0000000000FFFF
      FF0000FFFF00FFFFFF0000FFFF00000000000000000000FFFF0000FFFF0000FF
      FF00000000000080800000808000008080000080800000808000008080000080
      800000808000000000008080800000000000008000000080000000800000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000008000000080
      0000008000000000000000000000000000000000000080808000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008080800000000000000000000000000000000000FFFF
      FF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF0000000000FFFFFF0000FF
      FF000000000000FFFF00FFFFFF00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000808000008080000080800000808000008080000080
      8000008080000080800000000000808080000080000000800000008000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000000000000000000000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FFFFFF00FF000000FF000000FF00
      0000FF000000FFFFFF008080800000000000FFFF000000000000FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000FFFF00FFFFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000008000000000000000000000000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008080800000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000FFFFFF0000000000FFFFFF00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000008080
      8000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFF
      FF00FFFFFF00FFFFFF008080800000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0080808000808080008080800000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF0080808000FFFFFF008080800000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0080808000808080000000000000000000FFFF00000000000000000000FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00008000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000008000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000008080
      0000800000000000000000000000000000000000000000000000000000000000
      0000800000008000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000008080
      8000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008000000000FF
      FF00808000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000008080
      8000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008000
      000000FFFF008080000080000000000000000000000000000000000000000000
      000080000000800000000000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000008080
      8000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080000000800000008000
      00008000000000FFFF0080800000800000000000000000000000000000000000
      0000800000008000000080000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FF000000FF000000FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000008080
      8000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00808080000000000000008000000000000000
      0000000080000000000000000000000080000000800000008000000000000000
      000000000000000080000000000000000000000000008000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00808000008000000000000000000000000000
      000000000000800000008000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C000808080008080800080808000808080008080
      8000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000008000000000000000
      0000000080000000000000008000000000000000000000000000000080000000
      000000008000000000000000000000000000000000008000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF008000000000000000000000000000
      0000000000000000000080000000800000008080800000FFFF0080808000FFFF
      FF00FFFFFF00FF000000FF000000FFFFFF00C0C0C000FF000000FF000000C0C0
      C000FFFFFF00FFFFFF00FFFFFF008080800080808000FFFFFF00FFFFFF008080
      8000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00808080000000000000008000000000000000
      0000000080000000000000008000000000000000000000000000000080000000
      00000000800000000000000000000000000000000000000000008000000000FF
      FF00808000008000000080000000800000008000000000000000800000008000
      000000000000000000008000000080000000808080008080800080808000C0C0
      C000C0C0C000FF000000FF000000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C0008080800080808000FFFFFF00FF0000008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000008000000000000000
      0000000080000000000000008000000000000000000000000000000080000000
      00000000800000000000000000000000000000000000000000008000000000FF
      FF0000FFFF008080000080000000000000000000000000000000800000008000
      0000800000008000000080000000800000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FF000000FF000000FF000000FF000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008080800080808000FFFFFF00FFFFFF008080
      8000FFFF0000FFFF000080808000FFFF0000FFFF000080808000FFFF0000FFFF
      000080808000FFFF0000FFFF0000808080000000000000008000000000000000
      0000000080000000000000008000000000000000000000000000000080000000
      0000000080000000000000000000000000008000000080000000800000008000
      000000FFFF0000FFFF0080800000800000000000000000000000000000008000
      000080000000800000008000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0008080800080808000FFFFFF00FF0000008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000008000000080000000
      8000000000000000000000000000000080000000800000008000000000000000
      0000000080000000800000000000000000008000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00808000008000000000000000000000000000
      0000000000000000000000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C00000800000008000000080
      00000080000000800000008000000080000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000008000000000000000000000000000000000008000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF008080000080000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      800080808000808080008080800080808000808080000080000000FF000000FF
      000000FF000000FF000000FF00000080000080808000FFFFFF00FF000000FF00
      0000FF000000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFFFF008080
      8000FFFFFF008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0080800000800000000000
      0000000000000000000000000000000000008080800000FFFF008080800000FF
      FF0000FFFF008080800000FFFF0000FFFF008080800000800000008000000080
      00000080000000800000008000000080000080808000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080000000800000008000
      0000800000008000000080000000800000008000000080000000800000008000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000080808000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF0080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      00000000000000000000FFFFFF0000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C00080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FF000000FF000000FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000080808000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF0080808000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF0000000000C0C0C000000000000000
      0000FFFFFF0000000000FFFFFF0000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C00080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00FF000000FF000000FFFFFF00C0C0C000FF000000FF000000C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000080808000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF0080808000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      FF0000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000FFFFFF000000000000000000FFFFFF000000
      000000000000000000000000000000000000808080008080800080808000C0C0
      C000C0C0C000FF000000FF000000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C00080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000FF000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FF000000FF000000FF000000FF000000FFFFFF000080
      00000080000000800000FFFFFF00808080000000000080808000FFFFFF00FFFF
      FF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF0080808000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000C0C0C000FFFFFF000000
      0000FFFFFF00000000000000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C0000080
      000000FF000000800000C0C0C000808080000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      80008080800080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C00000800000008000000080
      000000FF00000080000000800000008000000000000080808000FFFF0000FFFF
      000080808000FFFF0000FFFF000080808000FFFF0000FFFF000080808000FFFF
      0000FFFF000080808000000000000000000000000000FFFFFF0000000000C0C0
      C000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      800080808000808080008080800080808000808080000080000000FF000000FF
      000000FF000000FF000000FF0000008000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      80008080800080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800000FFFF008080800000FF
      FF0000FFFF008080800000FFFF0000FFFF008080800000800000008000000080
      000000FF00000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080000080
      000000FF00000080000080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      0000008000000080000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000080000000
      80008080800000000000000000000000000000000000000000000000FF008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000080000000
      800000008000808080000000000000000000000000000000FF00000080000000
      80008080800000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000000000000000000000FF00000080000000
      8000000080000000800080808000000000000000FF0000008000000080000000
      80000000800080808000000000000000000000000000FFFFFF0080000000FFFF
      FF00800000008000000080000000800000008000000080000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8000000080000000800000008000808080000000800000008000000080000000
      80000000800080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      FF00000080000000800000008000000080000000800000008000000080000000
      80008080800000000000000000000000000000000000FFFFFF0080000000FFFF
      FF00800000008000000080000000800000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      00000000FF000000800000008000000080000000800000008000000080008080
      80000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000800000008000000080000000800000008000808080000000
      00000000000000000000000000000000000000000000FFFFFF0080000000FFFF
      FF00800000008000000080000000800000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008000000080000000800000008000808080000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000FFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000800000008000000080000000800000008000808080000000
      00000000000000000000000000000000000000000000FFFFFF0080000000FFFF
      FF00800000008000000080000000800000000000000000000000FFFFFF0000FF
      FF00000000000000000000000000000000000000000080808000FFFFFF008080
      800080808000FFFFFF00FFFFFF0080808000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000800000008000808080000000800000008000000080008080
      80000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FFFF00FFFF
      FF000000000000000000000000000000000000000000000000008080800000FF
      FF0080808000FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8000000080000000800080808000000000000000FF0000008000000080000000
      80008080800000000000000000000000000000000000FFFFFF0080000000FFFF
      FF008000000080000000800000008000000080000000000000000000000000FF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFFFF008080
      8000FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000000000000000FF000000
      800000008000808080000000000000000000000000000000FF00000080000000
      80000000800080808000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF0000FFFF000000000000000000000000000000000000000000808080008080
      800000FFFF00FFFFFF00FFFFFF00808080008080800080808000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      FF000000800000000000000000000000000000000000000000000000FF000000
      80000000800000008000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000FF00000000000000000000000000000000008080800000FF
      FF008080800000FFFF0080808000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000080000000FF0000000000000000000000000080808000FFFFFF000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      000080808000FFFFFF00000000000000000080808000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008000000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008000000080000000000000000000000000000000000000000000
      0000800000000080000000800000800000000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000800000008000
      0000800000008000000080000000800000008000000080000000800000008000
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000000000000080000000800000000000000000000000000000008000
      0000008000000080000000800000008000008000000000000000000000000000
      00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008000
      0000000000008000000080000000800000000000000000000000800000000080
      0000008000000080000000800000008000000080000080000000000000000000
      00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00800000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000000000
      0000800000008000000080000000000000000000000080000000008000000080
      00000080000000FF000000800000008000000080000000800000800000000000
      00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00800000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080000000000000008000
      0000800000008000000000000000000000000000000000800000008000000080
      000000FF00000000000000FF0000008000000080000000800000800000000000
      00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      8000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0080808000800000008000
      0000800000000000000000000000000000000000000000FF00000080000000FF
      000000000000000000000000000000FF00000080000000800000008000008000
      00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008080
      800000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000008080
      80000000000000000000000000000000000000000000FFFFFF00808080000000
      000000000000000000000000000000000000000000000000000000FF00000000
      00000000000000000000000000000000000000FF000000800000008000000080
      0000800000000000000000000000000000008000000080000000800000008000
      0000800000008000000080000000800000008000000080000000800000008080
      8000FFFFFF008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000008000000080
      00000080000080000000000000000000000080000000FFFFFF00800000008000
      000080000000800000008000000080000000808080008080800080808000FFFF
      FF0000FFFF0000FFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000080
      0000008000000080000080000000000000008000000080000000800000008000
      0000800000008000000080000000800000008000000000FFFF008080800000FF
      FF00FFFFFF008080800080808000808080000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000008000000080000000800000800000000000000000000000000000000000
      000000000000000000000000000000000000000000008080800000FFFF008080
      800000FFFF008080800000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000008080800000FFFF00000000008080
      8000FFFFFF00000000008080800000FFFF000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000FFFF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000800000008000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000008080
      800000FFFF000000000000000000808080000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF0080808000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000008080
      8000FFFFFF0080808000FFFFFF00000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000808000A56E3A00A56E
      3A00A56E3A00A56E3A00A56E3A00A56E3A00A56E3A00A56E3A00A56E3A00A56E
      3A00A56E3A00A56E3A00A56E3A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000080800000000000C0C0
      C000C0DCC000C0C0C000C0DCC000C0C0C000C0DCC000C0C0C000C0DCC000C0C0
      C000C0C0C000C0C0C000A56E3A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0C0C000C0DCC000C0C0C000C0DCC000C0C0C000C0DC
      C000C0C0C000C0C0C000A56E3A00000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FF000000FF000000FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      000000000000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0C0C000C0DCC000C0C0
      C000C0DCC000C0C0C000A56E3A00000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008080800000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      000000000000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0C0C000C0DCC000C0C0C000C0DC
      C000C0C0C000C0DCC000A56E3A00000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FFFFFF00FF000000FF000000FF00
      0000FF000000FFFFFF0080808000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00FF000000FF000000FFFFFF00C0C0C000FF000000FF000000C0C0
      C000FFFFFF00FFFFFF00FFFFFF0080808000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0C0
      C000C0DCC000C0C0C000A56E3A00000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008080800000000000808080008080800080808000C0C0
      C000C0C0C000FF000000FF000000C0C0C000C0C0C000FF000000FF000000C0C0
      C000C0C0C000C0C0C000C0C0C00080808000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0C0C000C0DC
      C000C0C0C000C0DCC000A56E3A00000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFF
      FF00FFFFFF00FFFFFF0080808000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FF000000FF000000FF000000FF000000FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      000000000000000000000000000000000000000000000080800000000000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DC
      C000C0DCC000C0C0C000A56E3A00000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0080808000808080008080800000000000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000000000000000000000000000000000000000000000808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0DCC000A56E3A00000000000000000080808000FFFFFF00FF00
      0000FF000000FF000000FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF0080808000FFFFFF0080808000000000008080800000FFFF0080808000FFFF
      FF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00C0C0
      C000FFFFFF00FFFFFF00FFFFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000808000A56E3A00A56E
      3A00A56E3A00A56E3A00A56E3A00A56E3A00A56E3A0000808000008080000080
      8000008080000080800000808000000000000000000080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00808080008080800000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      00000000000000000000C0DCC000C0DCC0000080800000000000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080000000000000000000000000008080800000FFFF008080800000FF
      FF0000FFFF008080800000FFFF0000FFFF008080800000FFFF0000FFFF008080
      800000FFFF0000FFFF0000FFFF00808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      8000008080000080800000808000008080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000900000000100010000000000800400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF9ADAFFC1FFFFFEFFA8D8F780
      DFFBFC7FAADAE3808FFFF93FAD8D800087F7F39F9FFF8001C7EFE1CFFBFC8001
      E3CFC0E7F1788083F19F8073FA3181C7F83F8039FF638707FC7FC01CFBC79F03
      F83FE009F18FFE03F19FF007FB1FFE03C3CFF807FE3FFE0387E7FC0FFC7FFE03
      8FFBFE3FF8FFFE07FFFFFFFFF9FFFF07FE3FFFFFFE7FFFFFF88FFFFFFE7FFFFF
      E003FFFFFFFFFFFF8080F8FF000FFFFF8000F043000F1FF98080E003000F6DF6
      8000C003000F6EF780808003000168178000C00300011F778000E81700016817
      8000F83F00016FB68000F87F00011FD98000FCFFE001FFFFE003FFFFE001FFFF
      F80FFFFFE001FFFFFE3FFFFFE001FFFF83F1F000FFFF800303F1F000801F0003
      0000F000000F00030000F000000700030000F000000300030001E00000010003
      8001000000000003800100000000000380010000000F000380010000001F0003
      8001000087FF000380010000FFE3000380030000FF73000380072001FF8B0003
      FFFFF003FFFF0003FFFFF007FFFFFFFFFFFFFE7FE000FFFFCFF3FE7FE000FFFF
      C7F30000E000FFFFC3FF0000E000FFFFE1F30000E000FFFF80F10000E000B63B
      807900000000B5D7807C00000000B5D7C04C00000000B5D7C1C000000000B5D7
      00E1000000008E33007F00000003FFF7803F00000003FFFF801F00000007FFFF
      800F0000000FFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFE7FFFFFFFCFFC00FE7F
      FFFFFFC7FC00000080030003FC00000080030001FC00000080030000EC000000
      80030001E400000080030003E000000080030007000000008003000F00010000
      8003001F000300008003007F00070000800300FF000F0000800301FFE3FF0000
      FFFF03FFE7FF0000FFFFFFFFEFFFFFE3FFFFFFFFF003FFFF87CF0007F7FBFFFF
      83870007F00BFFF981030007F00BE7FFC0030007F00BC3F3E0070007F00BC3E7
      F00F0007F00BE1C7F81F0007F00BF08FF81F0007B00BF81FF01F00078003FC3F
      E00F0007C027F81FC1070003802FF09FC3830003C01FC1C7E7C30001C0FF83E3
      FFE3000193FF8FF1FFFFFFF3B33FFFFFF9FFFFFFFFFFFFF9F0FF0007FFFFFFF4
      E07F0007FFF9FFE8C03F0007E7FFFFD1801F0007C3F3F823841F0007C3E7E707
      8E0F0007E1C7CF8FDF070003F08FDFCFFF830000F81FBFF7FFC10000FC3FBFF7
      FFE0FF81F81FA7F7FFF0FF24F09FA7F7FFF8FF66C1C7C1EFFFFCFFFF83E3C1CF
      FFFEFFFF8FF1E79FFFFFFFFFFFFFF87FFFFFFFFFFE7FFFFFFFFFFFFFFE7FFFFF
      C000FFFF0000FFFF8000FFFF0000FFBFA000FFFF0000FF9FA00080010000FF8F
      A00080010000C007A00080010000C003A00080010000C003A00080010000C007
      A00080010000FF8FBFF880010000FF9F800180030000FFBFDC3F80070000FFFF
      E07FFFFF0000FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
