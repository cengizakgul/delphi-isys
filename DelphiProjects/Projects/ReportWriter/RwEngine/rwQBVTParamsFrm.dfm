object rwQBVTParams: TrwQBVTParams
  Left = 367
  Top = 198
  ActiveControl = grParams
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Parameters'
  ClientHeight = 250
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    516
    250)
  PixelsPerInch = 96
  TextHeight = 13
  object grParams: TisRWDBGrid
    Left = 0
    Top = 0
    Width = 516
    Height = 213
    ControlType.Strings = (
      'Null;CheckBox;1;0'
      'Value;CustomEdit;cbExpression;F')
    Selected.Strings = (
      'Name'#9'32'#9'Parameter'#9'T'#9
      'Type'#9'7'#9'Type'#9'T'#9
      'Value'#9'40'#9'Value'#9'F'#9)
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrwQBVTParams\grParams'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    OnRowChanged = grParamsRowChanged
    FixedCols = 1
    ShowHorzScrollBar = False
    ShowVertScrollBar = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = evDataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = clCream
    LocateDlg = False
    FilterDlg = False
    Sorting = False
  end
  object Button1: TButton
    Left = 350
    Top = 221
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 437
    Top = 221
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object cbExpression: TwwDBComboDlg
    Left = 256
    Top = 48
    Width = 121
    Height = 21
    OnCustomDlg = cbExpressionCustomDlg
    ShowButton = True
    Style = csDropDownList
    Picture.PictureMaskFromDataSet = False
    TabOrder = 3
    WordWrap = False
    UnboundDataType = wwDefault
  end
  object dsParams: TisRWClientDataSet
    ControlType.Strings = (
      'Value;CustomEdit;cbExpression;T')
    BeforeDelete = dsParamsBeforeDelete
    OnNewRecord = dsParamsNewRecord
    Left = 136
    Top = 104
    object fldName: TStringField
      DisplayLabel = 'Parameter'
      DisplayWidth = 32
      FieldName = 'Name'
      Size = 32
    end
    object dsParamsType: TStringField
      DisplayWidth = 7
      FieldName = 'Type'
      Size = 7
    end
    object fldValue: TStringField
      DisplayWidth = 40
      FieldName = 'Value'
      Size = 64
    end
  end
  object evDataSource1: TDataSource
    DataSet = dsParams
    Left = 176
    Top = 104
  end
end
