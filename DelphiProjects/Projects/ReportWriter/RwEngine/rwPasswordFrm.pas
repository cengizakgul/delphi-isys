// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwPasswordFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TrwPassword = class(TForm)
    OK: TButton;
    Cancel: TButton;
    edPass: TEdit;
    Label1: TLabel;
  private
  public
  end;

  function PasswordDialog(const ACaption, APrompt: String): String;

implementation

{$R *.dfm}

function PasswordDialog(const ACaption, APrompt: String): String;
var
  f: TrwPassword;
begin
  f := TrwPassword.Create(Application);
  try
    if ACaption <> '' then
      f.Caption := ACaption;
    if APrompt <> '' then
      f.Label1.Caption := APrompt;
    if f.ShowModal = mrOK then
      Result := f.edPass.Text
    else
      Result := '';
  finally
    f.Free;
  end;
end;

end.
