unit rwAdapterUtils;

interface

uses Windows, SysUtils;

type
  TrwAppAdapterInfo = record
    ModuleName:      String;
    ApplicationName: String;
    Version:         String;
  end;

  TrwAppAdaptersList = array of TrwAppAdapterInfo;


  function  GetAppAdapterInfo(const AFileName: String): TrwAppAdapterInfo;
  function  FindRWAdaptersInFolder(const AFolder: String): TrwAppAdaptersList;

implementation

function  GetAppAdapterInfo(const AFileName: String): TrwAppAdapterInfo;
var
  n, VerSize: Cardinal;
  VerInfo: Pointer;
  VerData: Pointer;
  VerDataLen: Cardinal;
  Descr: String;
begin
  Result.ModuleName := '';
  Result.ApplicationName := '';
  Result.Version := '';

  try
    VerSize := GetFileVersionInfoSize(PAnsiChar(AFileName), n);
    if VerSize <> 0 then
    begin
      VerInfo := AllocMem(VerSize);
      try
        GetFileVersionInfo(PAnsiChar(AFileName), 0, VerSize, VerInfo);
        VerQueryValue(VerInfo, '\StringFileInfo\040904E4\InternalName', VerData, VerDataLen);

        if VerDataLen > 1 then
        begin
          SetLength(Descr, VerDataLen - 1);
          StrLCopy(PChar(@Descr[1]), PChar(VerData),  VerDataLen - 1);
        end;

        if AnsiSameText(Descr, 'RWAppAdapter') then
        begin
          Result.ModuleName := AFileName;

          VerQueryValue(VerInfo, '\StringFileInfo\040904E4\FileDescription', VerData, VerDataLen);
          if VerDataLen > 1 then
          begin
            SetLength(Result.ApplicationName, VerDataLen - 1);
            StrLCopy(PChar(@Result.ApplicationName[1]), PChar(VerData),  VerDataLen - 1);
          end
          else
            Result.ApplicationName := 'Unknown';

          VerQueryValue(VerInfo, '\StringFileInfo\040904E4\FileVersion', VerData, VerDataLen);
          if VerDataLen > 1 then
          begin
            SetLength(Result.Version, VerDataLen - 1);
            StrLCopy(PChar(@Result.Version[1]), PChar(VerData),  VerDataLen - 1);
          end
          else
            Result.Version := '';
        end;

      finally
        FreeMem(VerInfo);
      end;
    end;
  except
  end;
end;


function FindRWAdaptersInFolder(const AFolder: String): TrwAppAdaptersList;
var
  SearchRec: TSearchRec;
  AppInfo: TrwAppAdapterInfo;
begin
    if FindFirst(AFolder + '*.dll', faAnyFile, SearchRec) = 0 then
      repeat
        AppInfo := GetAppAdapterInfo(AFolder + SearchRec.Name);
        if AppInfo.ModuleName <> '' then
        begin
          SetLength(Result, Length(Result) + 1);
          Result[High(Result)] := AppInfo;
        end;
      until FindNext(SearchRec) <> 0;

    FindClose(SearchRec);
end;

end.
 