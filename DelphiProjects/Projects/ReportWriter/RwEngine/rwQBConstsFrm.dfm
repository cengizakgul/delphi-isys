object rwQBConstsEdit: TrwQBConstsEdit
  Left = 357
  Top = 106
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'SQL Constants'
  ClientHeight = 570
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evDBGrid1: TisRWDBGrid
    Left = 0
    Top = 0
    Width = 635
    Height = 409
    DisableThemesInTitle = False
    Selected.Strings = (
      'Group'#9'47'#9'Table.Field'
      'Name'#9'19'#9'Constant Name'
      'DisplayName'#9'31'#9'Description')
    IniAttributes.Enabled = True
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TrwQBConstsEdit\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = evDataSource1
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object Panel1: TPanel
    Left = 0
    Top = 409
    Width = 635
    Height = 161
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 74
      Width = 33
      Height = 13
      Caption = 'Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 178
      Top = 74
      Width = 65
      Height = 13
      Caption = 'Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 470
      Top = 31
      Width = 39
      Height = 13
      Caption = 'Values'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 33
      Width = 64
      Height = 13
      Caption = 'Table.Field'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object rgTypes: TDBRadioGroup
      Left = 8
      Top = 118
      Width = 240
      Height = 37
      Caption = 'Type'
      Columns = 4
      DataField = 'Type'
      DataSource = evDataSource1
      Items.Strings = (
        'String'
        'Date'
        'Integer'
        'Float')
      TabOrder = 3
      Values.Strings = (
        '0'
        '1'
        '2'
        '3')
    end
    object memValue: TDBMemo
      Left = 469
      Top = 46
      Width = 160
      Height = 108
      DataField = 'Value'
      DataSource = evDataSource1
      TabOrder = 4
    end
    object edNAme: TDBEdit
      Left = 8
      Top = 89
      Width = 151
      Height = 21
      DataField = 'Name'
      DataSource = evDataSource1
      TabOrder = 1
    end
    object edDescr: TDBEdit
      Left = 177
      Top = 89
      Width = 280
      Height = 21
      DataField = 'DisplayName'
      DataSource = evDataSource1
      TabOrder = 2
    end
    object DBNavigator1: TDBNavigator
      Left = 174
      Top = 11
      Width = 234
      Height = 25
      DataSource = evDataSource1
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
      TabOrder = 0
    end
    object cbGroup: TwwDBComboBox
      Left = 8
      Top = 49
      Width = 450
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      DataField = 'Group'
      DataSource = evDataSource1
      DropDownCount = 8
      ItemHeight = 13
      Sorted = False
      TabOrder = 5
      UnboundDataType = wwDefault
      OnCloseUp = cbGroupCloseUp
      OnExit = cbGroupExit
    end
  end
  object dsConst: TisRWClientDataSet
    BeforePost = dsConstBeforePost
    AfterPost = dsConstAfterPost
    BeforeDelete = dsConstBeforeDelete
    Left = 200
    Top = 80
    object dsConstID: TStringField
      DisplayLabel = 'Table.Field'
      DisplayWidth = 47
      FieldName = 'Group'
      Size = 64
    end
    object dsConstName: TStringField
      DisplayLabel = 'Constant Name'
      DisplayWidth = 19
      FieldName = 'Name'
      Size = 64
    end
    object dsConstDisplayName: TStringField
      DisplayLabel = 'Description'
      DisplayWidth = 31
      FieldName = 'DisplayName'
      Size = 64
    end
    object dsConstType: TIntegerField
      DisplayWidth = 10
      FieldName = 'Type'
      Visible = False
    end
    object dsConstValue: TMemoField
      FieldName = 'Value'
      BlobType = ftMemo
    end
  end
  object evDataSource1: TDataSource
    DataSet = dsConst
    Left = 232
    Top = 80
  end
end
