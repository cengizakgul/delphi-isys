unit rwDDAddTableFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, rwCustomDataDictionary, rwDataDictionary,
  rwBasicUtils, Menus;

type
  TrwAddDDTable = class(TForm)
    rgTableType: TRadioGroup;
    gbItems: TGroupBox;
    btnOK: TButton;
    btnCancel: TButton;
    lvItems: TListView;
    pmTables: TPopupMenu;
    miShowAll: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure rgTableTypeClick(Sender: TObject);
    procedure miShowAllClick(Sender: TObject);
  private
    procedure CreateExternalTableList;
    procedure CreateComponentList;
    procedure ClearItemList;
  public
  end;

  function AddDDTable: TrwDataDictTable;

implementation

{$R *.dfm}

uses rwQueryBuilderFrm, rwEngine, rwUtils, rwEngineTypes, rwDB;


function AddDDTable: TrwDataDictTable;

  procedure AddQueryTable;
  begin
    Result := TrwDataDictTable(DataDictionary.Tables.AddTable);
    Result.Name := 'RW Query';
    Result.ExternalName := '';
    Result.DisplayName := Result.Name;
    Result.TableType := rwtDDQuery;
  end;

  procedure AddComponentTable(const Name, DisplayName: String);
  begin
    Result := TrwDataDictTable(DataDictionary.Tables.AddTable);
    Result.Name := 'RW Component';
    Result.ExternalName := Name;
    Result.DisplayName := DisplayName;
    Result.TableType := rwtDDComponent;
  end;

  procedure AddTable(const Name, DisplayName: String);
  var
    Flds, FK, Fld, Tbl: String;
    rwT: TrwDataDictTable;
    rwP: TrwDataDictParam;
    rwF: TrwDataDictField;
    rwFK: TDataDictForeignKey;
    FldInfo: TrwDDFieldInfoRec;
    TableInfo: TrwDDTableInfoRec;
    ParamInfo: TrwDDParamInfoRec;
  begin
    Result := TrwDataDictTable(DataDictionary.Tables.AddTable);
    Result.Name := Name;
    Result.ExternalName := Name;
    Result.DisplayName := DisplayName;
    Result.TableType := rwtDDExternal;

    // Add Fields
    Flds := RWEngineExt.AppAdapter.GetDDFields(Result.ExternalName);
    while Flds <> '' do
    begin
      Fld := Trim(GetNextStrValue(Flds, ','));
      rwF := TrwDataDictField(Result.Fields.AddField);
      rwF.ExternalName := Fld;
      FldInfo := RWEngineExt.AppAdapter.GetDDFieldInfo(Result.ExternalName, rwF.ExternalName);
      if FldInfo.Name = '' then
        FldInfo.Name := Fld;
      rwF.Name := FldInfo.Name;
      rwF.DisplayName := FldInfo.DisplayName;

      rwF.FieldType := TDataDictDataType(Ord(FldInfo.FieldType));
      rwF.Size := FldInfo.Size;
      if (rwF.Size > 255) and (rwF.FieldType = ddtString) then
      begin
        rwF.FieldType := ddtMemo;
        rwF.Size := 0;
      end;

      rwF.FieldValues.Text := FldInfo.FieldValues;
    end;

    TableInfo := RWEngineExt.AppAdapter.GetDDTableInfo(Result.ExternalName);

    // Add Table Params
    Flds := TableInfo.Params;
    while Flds <> '' do
    begin
      Fld := Trim(GetNextStrValue(Flds, ','));
      rwP := TrwDataDictParam(Result.Params.AddParam);
      ParamInfo := RWEngineExt.AppAdapter.GetDDParamInfo(Result.ExternalName, Fld);
      if ParamInfo.Name = '' then
        ParamInfo.Name := Fld;
      rwP.Name := ParamInfo.Name;
      rwP.DisplayName := ParamInfo.DisplayName;
      rwP.ParamType := TDataDictDataType(Ord(ParamInfo.ParamType));
      rwP.ParamValues.Text := ParamInfo.ParamValues;
      rwP.DefaultValue := ParamInfo.DefaultValue;
    end;

    // Add Primary Keys
    Flds := TableInfo.PrimaryKey;
    while Flds <> '' do
    begin
      Fld := Trim(GetNextStrValue(Flds, ','));
      rwF := TrwDataDictFields(Result.Fields).FieldByExternalName(Fld);
      if Assigned(rwF) then
        Result.PrimaryKey.AddKeyField(rwF);
    end;

    // Add Foreign Keys
    Flds := TableInfo.ForeignKey;
    while Flds <> '' do
    begin
      FK := GetNextStrValue(Flds, Chr(VK_RETURN));

      Tbl := Trim(GetNextStrValue(FK, '='));
      rwT := TrwDataDictTables(DataDictionary.Tables).TableByExternalName(Tbl);
      if Assigned(rwT) and (FK <> '') then
      begin
        rwFK := Result.ForeignKeys.AddForeignKey(rwT);
        while FK <> '' do
        begin
          Fld := Trim(GetNextStrValue(FK, ','));
          rwF := TrwDataDictFields(Result.Fields).FieldByExternalName(Fld);
          if Assigned(rwF) then
            rwFK.Fields.AddKeyField(rwF);
        end;
      end;
    end;


  end;

begin
  Result := nil;

  with TrwAddDDTable.Create(FQBFrm) do
    try
      if ShowModal = mrOK then
        if (rgTableType.ItemIndex = 0) and Assigned(lvItems.Selected) then
          AddTable(lvItems.Selected.Caption, lvItems.Selected.SubItems[0])
        else if rgTableType.ItemIndex = 1 then
          AddQueryTable
        else if rgTableType.ItemIndex = 2 then
          AddComponentTable(lvItems.Selected.Caption, lvItems.Selected.SubItems[0]);
    finally
      Free;
    end;
end;

procedure TrwAddDDTable.ClearItemList;
begin
  lvItems.Clear;
  gbItems.Enabled := False;
end;

procedure TrwAddDDTable.CreateExternalTableList;
var
  Tbls, Tbl: String;
  LI: TListItem;
  TableInfo: TrwDDTableInfoRec;
begin
  lvItems.Clear;
  lvItems.SortType := stNone;
  gbItems.Enabled := True;

  Tbls := RWEngineExt.AppAdapter.GetDDTables;

  while Tbls <> '' do
  begin
    Tbl := Trim(GetNextStrValue(Tbls, ','));
    if miShowAll.Checked or not Assigned(TrwDataDictTables(DataDictionary.Tables).TableByExternalName(Tbl)) then
    begin
      TableInfo := RWEngineExt.AppAdapter.GetDDTableInfo(Tbl);
      LI := lvItems.Items.Add;
      if TableInfo.Name = '' then
        TableInfo.Name := Tbl;
      LI.Caption := TableInfo.Name;
      LI.SubItems.Add(TableInfo.DisplayName);
    end;
  end;
  lvItems.SortType := stText;
end;

procedure TrwAddDDTable.CreateComponentList;
var
  i: Integer;
  LI: TListItem;
begin
  lvItems.Clear;
  lvItems.SortType := stNone;
  gbItems.Enabled := True;

  for i := 0 to SystemLibComponents.Count - 1 do
  begin
    if SystemLibComponents[i].IsInheritsFrom('TrwlDDCustomQuery') then
    begin
      LI := lvItems.Items.Add;
      LI.Caption := SystemLibComponents[i].Name;
      LI.SubItems.Add(SystemLibComponents[i].DisplayName);
    end;
  end;

  lvItems.SortType := stText;
end;

procedure TrwAddDDTable.FormCreate(Sender: TObject);
begin
  rgTableType.OnClick(nil);
end;

procedure TrwAddDDTable.rgTableTypeClick(Sender: TObject);
begin
  if rgTableType.ItemIndex = 0 then
    CreateExternalTableList
  else if rgTableType.ItemIndex = 2 then
    CreateComponentList
  else
    ClearItemList;
end;

procedure TrwAddDDTable.miShowAllClick(Sender: TObject);
begin
  miShowAll.Checked := not miShowAll.Checked;
  CreateExternalTableList;
end;

end.
