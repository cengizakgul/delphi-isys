unit rwPrintElements;

interface

uses
  Classes, Controls, ExtCtrls, StdCtrls, Windows, Messages,
  ComCtrls, Forms, Sysutils, Graphics, Math,
  rwTypes, rwPrintPage, rwPrinters, rwPCLCanvas, rwEngineTypes,
  rwBasicUtils, rwGraphics, EvStreamUtils, Barcode1D, PDF417;

type
  TExtShape = class(TShape)
  private
    FLayerNumber: TrwLayerNumber;
    FCompare: Boolean;
  public
    constructor Create(AOwner: TComponent); override;

  published
    property PopupMenu;
    property OnDblClick;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber default 0;
    property Compare: Boolean read FCompare write FCompare default True;
  end;


  {TGraphicFrame is the frame or fragment of frame}

  TGraphicFrame = class(TGraphicControl)
  private
    FLayerNumber: TrwLayerNumber;
    FPen: TPen;
    FBrush: TBrush;
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    FTransparent: Boolean;
    FCompare: Boolean;
    FShowCorners: Boolean;

    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
    procedure SetBrush(Value: TBrush);
    procedure SetPen(Value: TPen);
    procedure StyleChanged(Sender: TObject);
    procedure SetTransparent(Value: Boolean);
    procedure SetShowCorners(const Value: Boolean);

  protected
    procedure Paint; override;

  public
    destructor Destroy; override;
    constructor Create(AOwner: TComponent); override;
    procedure Assign(Source: TPersistent); override;

  published
    property Align;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property Transparent: Boolean read FTransparent write SetTransparent default True;
    property Brush: TBrush read FBrush write SetBrush;
    property Pen: TPen read FPen write SetPen;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines default [rclLeft, rclRight, rclTop, rclBottom];
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor default clBlack;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth default 1;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle default psSolid;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber default 0;
    property Compare: Boolean read FCompare write FCompare default True;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
    property ShowCorners: Boolean read FShowCorners write SetShowCorners;
  end;


  TFrameLabel = class(TCustomLabel)
  private
    FLayerNumber: TrwLayerNumber;
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    FDigitNet: TrwStrings;
    FExtraSpacing: Integer;
    FCompare: Boolean;
    FShowCorners: Boolean;

    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
    procedure SetDigitNet(Value: TrwStrings);
    procedure OnChangeDigitNet(Sender: TObject);
    function  DigitNetIsNotEmpty: Boolean;
    procedure SetShowCorners(const Value: Boolean);

  protected
    procedure Paint; override;
    procedure DoDrawCellText(ARect: TRect; Flags: Longint);
    procedure AdjustBounds; override;
    procedure DoDrawText(var Rect: TRect; Flags: Longint); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property ParentFont;

  published
//    property Align;
    property Alignment;
    property Autosize;
    property Left;
    property Top;
    property Width;
    property Height;
    property Layout;
    property Visible;
    property Color default clWhite;
    property Caption;
    property Font;
    property WordWrap;
    property PopupMenu;
    property ShowHint;
    property Transparent;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines default [];
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor default clBlack;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth default 1;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle default psSolid;
    property DigitNet: TrwStrings read FDigitNet write SetDigitNet stored DigitNetIsNotEmpty;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber default 0;
    property Compare: Boolean read FCompare write FCompare default True;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
    property ShowCorners: Boolean read FShowCorners write SetShowCorners;
  end;


  {TFrameImage is Image inside frame}

  TrwPCLFile = class (TPersistent)
  private
    FData: String;
    FOnChange: TNotifyEvent;
    procedure SetData(Value: String);

  public
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
    procedure Assign(Source: TPersistent); override;
    procedure LoadFromFile(AFileName: String);
    procedure SaveToFile(AFileName: String);
    function  CleanPCL: String;

    constructor Create;

  published
    property Data: String read FData write SetData;
    procedure PLoadFromFile(AFileName: Variant);
  end;


    {TPCLLabel}
  TPCLLabel = class(TFrameLabel)
  private
    FPCLFont: TrwPCLFile;
    FPreparedToDraw: Boolean;
    FPicture: TMetafile;
    FWidth:   Integer;
    FHeight:  Integer;

    procedure SetPCLFont(Value: TrwPCLFile);

  protected
    procedure Paint; override;
    procedure AdjustBounds; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   PreparePicture;
    procedure   SetPCLSize(AWidth, AHeight: Integer);

  published
    property PCLFont: TrwPCLFile read FPCLFont write SetPCLFont;
  end;


  TFrameImage = class(TImage)
  private
    FLayerNumber: TrwLayerNumber;
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    FPCLData: TrwPCLFile;
    FHandle: THandle;
    FOldStretch: Boolean;
    FOnChangePCL: TNotifyEvent;
    FCompare: Boolean;
    FPreparedToDraw: Boolean;
    FWidth:   Integer;
    FHeight:  Integer;
    FPrevPictChanged: TNotifyEvent;
    FShowCorners: Boolean;

    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
    procedure SetPCLData(Value: TrwPCLFile);
    procedure ChangePCL(Sender: TObject);
    function  PCLDataIsNotEmpty: Boolean;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure PictureChanged(Sender: TObject);
    procedure SetShowCorners(const Value: Boolean);

  protected
   procedure  AdjustSize; override;
   function   CanAutoSize(var NewWidth, NewHeight: Integer): Boolean; override;

  public
    CaptionText: String;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   PreparePicture;
    procedure   ChangeScale(M, D: Integer); override;
    procedure   RotatePicture(AAngle: Integer);
    procedure   SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure   SetPCLSize(AWidth, AHeight: Integer);
    property    OnChangePCL: TNotifyEvent read FOnChangePCL write FOnChangePCL;

  published
    property PCLData: TrwPCLFile read FPCLData write SetPCLData stored PCLDataIsNotEmpty;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines default [];
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor default clBlack;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth default 1;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle default psSolid;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber default 0;
    property Compare: Boolean read FCompare write FCompare default True;
    property ShowCorners: Boolean read FShowCorners write SetShowCorners;
  end;


  TExtBarcode1D = class(TBarcode1D)
  private
    FLayerNumber: TrwLayerNumber;
    FCompare: Boolean;
    FAutoSize: boolean;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property AutoSize: boolean read FAutoSize write FAutoSize;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber;
    property Compare: Boolean read FCompare write FCompare;

    function Transparent: Boolean;
  end;

  TExtBarcodePDF417 = class(TPDF417)
  private
    FLayerNumber: TrwLayerNumber;
    FCompare: Boolean;
    FAutoSize: boolean;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property AutoSize: boolean read FAutoSize write FAutoSize;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber;
    property Compare: Boolean read FCompare write FCompare;

    function Transparent: Boolean;
  end;

//New printing objects NEW PREVIEW FROMAT!!!

  TrwPrintObjectClass = class of TrwPrintObject;

  TrwPrintObject = class(TComponent)
  private
    FCompare: Boolean;
    FLayerNumber: TrwLayerNumber;
    FVisible: Boolean;
    FReadTransparen: Boolean;

    FpLeft:   Smallint;
    FpTop:    Smallint;
    FpRight:  Smallint;
    FpBottom: Smallint;
    FColor: TColor;
    FUsedOnCurrentPaper: Boolean;

    function  GetBoundsRect: TRect;
    procedure SetBoundsRect(const Value: TRect);
    function  GetTransparent: Boolean; virtual;
    procedure SetTransparent(const Value: Boolean); virtual;
    procedure SetColor(const Value: TColor); virtual;

  protected
    procedure Loaded; override;

  public
    constructor Create(AOwner: TComponent); override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; virtual;
    procedure   Paint(ACanvas: TCanvas; APage: TrwVirtualPage); virtual;
    procedure   Print(APage: TrwVirtualPage); virtual;
    function    CompareObject(APrintObject: TComponent): Boolean; overload; virtual; abstract;
    function    CompareObject(APrintObject: TrwPrintObject): Boolean; overload; virtual;
    procedure   WriteObjectInfo(AStream: TStream; APrintObject: TComponent); overload; virtual;
    procedure   WriteObjectInfo(AStream: TStream); overload; virtual;
    procedure   Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); reintroduce; virtual;
    procedure   PrepareToPaint(AData: TStream); virtual;
    property    Visible: Boolean read FVisible write FVisible;
    property    Left: Smallint read FpLeft write FpLeft;
    property    Top: Smallint read FpTop write FpTop;
    property    Right: Smallint read FpRight write FpRight;
    property    Bottom: Smallint read FpBottom write FpBottom;
    property    BoundsRect: TRect read GetBoundsRect write SetBoundsRect;
    property    Color: TColor read FColor write SetColor;
    property    UsedOnCurrentPaper: Boolean read FUsedOnCurrentPaper write FUsedOnCurrentPaper;

  published
    property Compare: Boolean read FCompare write FCompare default True;
    property LayerNumber: TrwLayerNumber read FLayerNumber write FLayerNumber default 0;
    property Transparent: Boolean read GetTransparent write SetTransparent stored False default False;
  end;


  TrwPrintText = class(TrwPrintObject)
  private
    FDigitNet: TStrings;
    FWordWrap: Boolean;
    FAlignment: TAlignment;
    FFont: TrwFont;
    FLayout: TTextLayout;
    FpText: String;
    FImage: TMetafile;
    FItemPosOnPage: integer;

    function  DigitNetIsNotEmpty: Boolean;
    procedure SetDigitNet(const Value: TStrings);
    procedure SetFont(const Value: TrwFont);

 public
    constructor Create(AOwner: TComponent); override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    destructor  Destroy; override;
    procedure   Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    function    CompareObject(APrintObject: TComponent): Boolean; override;
    function    CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure   WriteObjectInfo(AStream: TStream; APrintObject: TComponent); override;
    procedure   WriteObjectInfo(AStream: TStream); override;
    procedure   PrepareToPaint(AData: TStream); override;
    procedure   Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

    property    Text: String read FpText write FpText;
    property    ItemPosOnPage: integer read FItemPosOnPage write FItemPosOnPage;

  published
    property Alignment: TAlignment read FAlignment write FAlignment default taLeftJustify;
    property Layout: TTextLayout read FLayout write FLayout default tlTop;
    property Color default clWhite;
    property Font: TrwFont read FFont write SetFont;
    property WordWrap: Boolean read FWordWrap write FWordWrap;
    property DigitNet: TStrings read FDigitNet write SetDigitNet stored DigitNetIsNotEmpty;
  end;


  TrwPrintFrame = class(TrwPrintObject)
  private
    FBoundLinesWidth: Integer;
    FBoundLinesColor: TColor;
    FBoundLinesStyle: TPenStyle;
    FBoundLines: TrwSetColumnLines;

    procedure SetTransparent(const Value: Boolean); override;
    procedure SetColor(const Value: TColor); override;

  public
    constructor Create(AOwner: TComponent); override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function CompareObject(APrintObject: TComponent): Boolean; override;
    function CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

  published
    property Color default clNone;
    property BoundLines: TrwSetColumnLines read FBoundLines write FBoundLines default [rclLeft, rclRight, rclTop, rclBottom];
    property BoundLinesColor: TColor read FBoundLinesColor write FBoundLinesColor default clBlack;
    property BoundLinesWidth: Integer read FBoundLinesWidth write FBoundLinesWidth default 1;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write FBoundLinesStyle default psSolid;
  end;


  TrwPrintLine = class(TrwPrintObject)
  private
    FLineWidth: Integer;
    FLineColor: TColor;
    FLineStyle: TPenStyle;
  public
    constructor Create(AOwner: TComponent); override;
    function  CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

  published
    property LineColor: TColor read FLineColor write FLineColor default clBlack;
    property LineWidth: Integer read FLineWidth write FLineWidth default 1;
    property LineStyle: TPenStyle read FLineStyle write FLineStyle default psSolid;
  end;


  TrwPrintShape = class(TrwPrintObject)
  private
    FBoundLinesWidth: Integer;
    FBoundLinesColor: TColor;
    FBoundLinesStyle: TPenStyle;
    FShapeType: TShapeType;
  public
    constructor Create(AOwner: TComponent); override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function CompareObject(APrintObject: TComponent): Boolean; override;
    function CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

  published
    property Color default clWhite;
    property BoundLinesColor: TColor read FBoundLinesColor write FBoundLinesColor default clBlack;
    property BoundLinesWidth: Integer read FBoundLinesWidth write FBoundLinesWidth default 1;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write FBoundLinesStyle default psSolid;
    property ShapeType: TShapeType read FShapeType write FShapeType default stRectangle;
  end;


  TrwPrintImage = class(TrwPrintObject)
  private
    FStretch: Boolean;
    FPicture: TrwPictureHolder;
    FPCLData: TrwPCLFile;
    FMacroID: Integer;
    FTransparent: Boolean;
    procedure SetPCLData(const Value: TrwPCLFile);
    procedure SetPicture(const Value: TrwPictureHolder);
    procedure PreparePicture(APage: TrwVirtualPage);
    function  IsNotPCL: Boolean;
    function  GetTransparent: Boolean; override;
    procedure SetTransparent(const Value: Boolean); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function CompareObject(APrintObject: TComponent): Boolean; override;
    function CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure Print(APage: TrwVirtualPage); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

  published
    property Transparent stored True default False;
    property Picture: TrwPictureHolder read FPicture write SetPicture stored IsNotPCL;
    property PCLData: TrwPCLFile read FPCLData write SetPCLData;
    property Stretch: Boolean read FStretch write FStretch stored IsNotPCL default True;
  end;


  TrwPCLTextImage = class(TCollectionItem)
  private
    FPicture: TMetafile;
    FText: String;
    FSize: TSize;
  public
    constructor Create(Collection: TCollection);override;
    destructor Destroy; override;
  end;

  TrwPCLTextImages = class(TCollection)
  public
    function ImageByText(const AText: String): TrwPCLTextImage;
    function AddImage: TrwPCLTextImage;
  end;


  TrwPrintPCLText = class(TrwPrintObject)
  private
    FPCLFont: TrwPCLFile;
    FpText: String;
    FFontID: String;
    FWidth:   Integer;
    FHeight:  Integer;
    FImages:  TrwPCLTextImages;
    FPicture: TMetafile;
    FInitString: String;
//    FFontHeight: Integer;

    procedure SetPCLFont(const Value: TrwPCLFile);
    procedure PreparePicture(ACanvas: TCanvas; APage: TrwVirtualPage);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function CompareObject(APrintObject: TComponent): Boolean; override;
    function CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure Print(APage: TrwVirtualPage); override;
    procedure WriteObjectInfo(AStream: TStream; APrintObject: TComponent); override;
    procedure WriteObjectInfo(AStream: TStream); override;
    procedure PrepareToPaint(AData: TStream); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

    property Text: string read FpText write FpText;

  published
    property PCLFont: TrwPCLFile read FPCLFont write SetPCLFont;
    property InitString: String read FInitString write FInitString stored False;
  end;


  Trw1DBarType = TSymbology;
  TrwPDF417BarEncoding = TEncoding;

  TrwPrintBarcode1D = class(TrwPrintObject)
  private
    FStretch: Boolean;
    FFont: TrwFont;
    FpText: string;
    FBarHeight: Integer;
    FBarType: Trw1DBarType;
    FBarColor: TColor;
    FShowText: Boolean;
    FSupplementalCode: String;
    FCheckCharacter: boolean;
    FPostnetHeightTallBar: Integer;
    FPostnetHeightShortBar: integer;
    FGuardBars: Boolean;
    FUPCEANSupplement5: Boolean;
    FUPCEANSupplement2: Boolean;
    FUPCESytem: String;
    FCODABARStartChar: String;
    FCODABARStopChar: String;
    FCode128Set: String;
    FSupSeparation: Integer;
    FVerticalOrientation: boolean;
    FTopMargin: integer;
    FLeftMargin: integer;

    procedure SetFont(const AValue: TrwFont);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function  CompareObject(APrintObject: TComponent): Boolean; override;
    function  CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure WriteObjectInfo(AStream: TStream; APrintObject: TComponent); override;
    procedure WriteObjectInfo(AStream: TStream); override;
    procedure PrepareToPaint(AData: TStream); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

    property Text: string read FpText write FpText;
  published
    property Color default clWhite;
    property Stretch: Boolean read FStretch write FStretch default True;
    property Font: TrwFont read FFont write SetFont;
	  property BarType: Trw1DBarType read FBarType write FBarType;
    property BarHeight: Integer read FBarHeight write FBarHeight default 40;
	  property BarColor: TColor read FBarColor write FBarColor default clBlack;
    property ShowText: Boolean read FShowText write FShowText default True;
    property UPCEANSupplement2: Boolean read FUPCEANSupplement2 write FUPCEANSupplement2;
    property UPCEANSupplement5: Boolean read FUPCEANSupplement5 write FUPCEANSupplement5;
    property SupplementalCode: String read FSupplementalCode write FSupplementalCode;
	  property CheckCharacter: Boolean read FCheckCharacter write FCheckCharacter;
    property PostnetHeightTallBar: Integer read FPostnetHeightTallBar write FPostnetHeightTallBar;
    property PostnetHeightShortBar: Integer read FPostnetHeightShortBar write FPostnetHeightShortBar;
    property GuardBars: Boolean read FGuardBars write FGuardBars;
    property UPCESytem: String read FUPCESytem write FUPCESytem;
    property CODABARStartChar: String read FCODABARStartChar write FCODABARStartChar;
    property CODABARStopChar:String read FCODABARStopChar write FCODABARStopChar;
    property Code128Set: String read FCode128Set write FCode128Set;
    property SupSeparation: Integer read FSupSeparation write FSupSeparation;
    property VerticalOrientation: boolean read FVerticalOrientation write FVerticalOrientation;
    property TopMargin: integer read FTopMargin write FTopMargin;
    property LeftMargin: integer read FLeftMargin write FLeftMargin;
  end;

  TrwPrintBarcodePDF417 = class(TrwPrintObject)
  private
    FStretch: Boolean;
    FpText: string;
    FFont: TrwFont;
    FTopMargin: integer;
    FLeftMargin: integer;
    FBarColor: TColor;
    FBarHeightPixels: integer;
    FBarWidthPixels: integer;
    FColumnsNumber: integer;
    FRowsNumber: integer;
    FMaxRows: integer;
    FECLevel: integer;
    FEncodingMode: TrwPDF417BarEncoding;
    FTruncated: boolean;
    FHorizontalPixelShaving: integer;
    FVerticalPixelShaving: integer;
    procedure SetFont(const AValue: TrwFont);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    class function CreateAs(AOwner: TComponent; AInitObject: TComponent): TrwPrintObject; override;
    function  CompareObject(APrintObject: TComponent): Boolean; override;
    function  CompareObject(APrintObject: TrwPrintObject): Boolean; override;
    procedure Paint(ACanvas: TCanvas; APage: TrwVirtualPage); override;
    procedure WriteObjectInfo(AStream: TStream; APrintObject: TComponent); override;
    procedure WriteObjectInfo(AStream: TStream); override;
    procedure PrepareToPaint(AData: TStream); override;
    procedure Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False); override;

    property Text: string read FpText write FpText;
  published
    property Color default clWhite;
    property Stretch: Boolean read FStretch write FStretch default True;
    property Font: TrwFont read FFont write SetFont;
    property TopMargin: integer read FTopMargin write FTopMargin;
    property LeftMargin: integer read FLeftMargin write FLeftMargin;
	  property BarColor: TColor read FBarColor write FBarColor default clBlack;
    property BarHeightPixels: Integer read FBarHeightPixels write FBarHeightPixels default 7;
    property BarWidthPixels: Integer read FBarWidthPixels write FBarWidthPixels default 1;
    property ColumnsNumber: integer read FColumnsNumber write FColumnsNumber default 10;
    property RowsNumber: integer read FRowsNumber write FRowsNumber default 0;
    property MaxRows: integer read FMaxRows write FMaxRows default 0;
    property ECLevel: integer read FECLevel write FECLevel default 0;
    property EncodingMode: TrwPDF417BarEncoding read FEncodingMode write FEncodingMode;
    property Truncated: boolean read FTruncated write FTruncated default false;
    property HorizontalPixelShaving: integer read FHorizontalPixelShaving write FHorizontalPixelShaving default 0;
    property VerticalPixelShaving: integer read FVerticalPixelShaving write FVerticalPixelShaving default 0;
  end;


implementation

uses Types, rwPDFPrinter, isMetafileUnit;

procedure DoDrawText(ACanvas: TCanvas; var Rect: TRect; Flags: Longint; AText: string; APHighlightInfo: PTrwHighlightInfoRec = nil);
var
  Text: string;
begin
  Text := AText;

  if (Flags and DT_CALCRECT <> 0) and (Text = '') then
    Text := Text + ' ';

  Flags := Flags or DT_NOPREFIX;

  if Assigned(APHighlightInfo) then
  begin
    ACanvas.Brush.Color := clHighlight;
    ACanvas.Brush.Style := bsSolid;
    ACanvas.Font.Color := clHighlightText;
  end;

  DrawText(ACanvas.Handle, PChar(Text), Length(Text), Rect, Flags);
end;

procedure PCLRestoreFont(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  Rec: TRect;
begin
  with ACanvas do
  begin
    Rec := Rect(APage.PrinterOffSets.X, APage.PrinterOffSets.Y,
                APage.PrinterOffSets.X + 20, APage.PrinterOffSets.Y + 20);

    Font.Handle := 0;
{    Font.Color := clBlack;
    Brush.Style := bsClear;}
    Font.Name := 'Arial';
    Font.Size := 1;
  end;

  DoDrawText(ACanvas, Rec, 0, '.');
end;

{ TExtShape }

constructor TExtShape.Create(AOwner: TComponent);
begin
  inherited;
  FCompare := True;
end;


{TGraphicFrame}

constructor TGraphicFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  ControlStyle := ControlStyle + [csReplicatable];
  FPen := TPen.Create;
  FPen.OnChange := StyleChanged;
  FBrush := TBrush.Create;
  FBrush.Color := clWhite;
  FBrush.OnChange := StyleChanged;

  Width := 70;
  Height := 45;
  FBoundLines := [rclLeft, rclRight, rclTop, rclBottom];
  FBoundLinesWidth := 1;
  FBoundLinesColor := clBlack;
  FBoundLinesStyle := psSolid;
  FTransparent := True;

  FCompare := True;
  FShowCorners := True;
end;

procedure TGraphicFrame.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;

  Invalidate;
end;

procedure TGraphicFrame.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;

  Invalidate;
end;

procedure TGraphicFrame.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;

  Invalidate;
end;

procedure TGraphicFrame.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;

  Invalidate;
end;

procedure TGraphicFrame.SetBrush(Value: TBrush);
begin
  FBrush.Assign(Value);
end;

procedure TGraphicFrame.SetPen(Value: TPen);
begin
  FPen.Assign(Value);
end;

procedure TGraphicFrame.StyleChanged(Sender: TObject);
begin
  Invalidate;
end;

procedure TGraphicFrame.Paint;
begin
  Canvas.Brush := Brush;
  Canvas.Pen := Pen;

  if not FTransparent then
    with Canvas do
      FillRect(Rect(0, 0, Width, Height));

  DrawFrame(Canvas, FBoundLines, FBoundLinesColor, FBoundLinesWidth,
    FBoundLinesStyle, Width, Height);

  if ShowCorners then
    DrawCorners(Canvas, Width, Height)
end;

procedure TGraphicFrame.SetTransparent(Value: Boolean);
begin
  FTransparent := Value;
  Invalidate;
end;


procedure TGraphicFrame.Assign(Source: TPersistent);
begin
  Top := TGraphicFrame(Source).Top;
  Left := TGraphicFrame(Source).Left;
  Width := TGraphicFrame(Source).Width;
  Height := TGraphicFrame(Source).Height;
  Align := TGraphicFrame(Source).Align;
  ShowHint := TGraphicFrame(Source).ShowHint;
  Visible := TGraphicFrame(Source).Visible;
  Transparent := TGraphicFrame(Source).Transparent;
  Pen := TGraphicFrame(Source).Pen;
  Brush := TGraphicFrame(Source).Brush;
  BoundLines := TGraphicFrame(Source).BoundLines;
  BoundLinesColor := TGraphicFrame(Source).BoundLinesColor;
  BoundLinesWidth := TGraphicFrame(Source).BoundLinesWidth;
  BoundLinesStyle := TGraphicFrame(Source).BoundLinesStyle;
end;

destructor TGraphicFrame.Destroy;
begin
  FPen.Free;
  FBrush.Free;
  inherited;
end;


procedure TGraphicFrame.SetShowCorners(const Value: Boolean);
begin
  FShowCorners := Value;
  Invalidate;
end;

{TFrameLabel}

constructor TFrameLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Autosize := False;
  ShowAccelChar := False;
  BoundLinesWidth := 1;
  BoundLinesColor := clBlack;
  BoundLinesStyle := psSolid;
  FExtraSpacing := 0;
  FLayerNumber := 0;
  Color := clWhite;

  FDigitNet := TrwStrings.Create;
  FDigitNet.OnChange := OnChangeDigitNet;

  FCompare := True;
  FShowCorners := True;

  ParentColor := False;
  ParentFont := False;
end;

destructor TFrameLabel.Destroy;
begin
  FDigitNet.Free;

  inherited;
end;

procedure TFrameLabel.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  AdjustBounds;
  Invalidate;
end;

procedure TFrameLabel.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  Invalidate;
end;

procedure TFrameLabel.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  AdjustBounds;
  Invalidate;
end;

procedure TFrameLabel.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  Invalidate;
end;


procedure TFrameLabel.SetDigitNet(Value: TrwStrings);
begin
  FDigitNet.Assign(Value);
end;


procedure TFrameLabel.OnChangeDigitNet(Sender: TObject);
begin
  AdjustBounds;
  Invalidate;
end;


procedure TFrameLabel.DoDrawCellText(ARect: TRect; Flags: Longint);
var
  Ind: Integer;
  RCom: TrwDigitNetRec;
  Rect: TRect;
  h: String[2];
  Len: Integer;
  i, j: Integer;

  procedure InitComRec(ACommand: String; var ARecCom: TrwDigitNetRec);
  var
    i: Integer;
  begin
    ARecCom.Command := ACommand[1];
    i := Pos(',', ACommand);
    ARecCom.Cells := StrToInt(Copy(ACommand, 2, i-2));
    ARecCom.Width := StrToInt(Copy(ACommand, i+1, Length(ACommand)-i));
  end;

begin
  Len := Length(Caption);

  with Canvas do
    if Alignment = taLeftJustify then
    begin
      Ind := 1;

      for i := 0 to FDigitNet.Count-1 do
      begin
        InitComRec(FDigitNet[i], RCom);
        if RCom.Command = NET_CELL then
          for j := 1 to RCom.Cells do
          begin
            if (Len < Ind) or (ARect.Left >= ARect.Right) then Exit;

            Rect := ARect;
            Rect.Right := Rect.Left + RCom.Width;
            h := Caption[Ind]+#0;
            DrawText(Canvas.Handle, PChar(@h[1]), 1, Rect, Flags);

            ARect.Left := ARect.Left + RCom.Width;
            Inc(Ind);
          end
        else if RCom.Command = NET_SPACE then
          ARect.Left := ARect.Left + RCom.Width*RCom.Cells;
      end;
    end

   else  if Alignment = taRightJustify then
   begin
     Ind := Len;

     for i := FDigitNet.Count-1 downto 0 do
     begin
       InitComRec(FDigitNet[i], RCom);
       if RCom.Command = NET_CELL then
         for j := 1 to RCom.Cells do
         begin
           if (Ind = 0) or (ARect.Left >= ARect.Right) then Exit;

           Rect := ARect;
           Rect.Left := Rect.Right - RCom.Width;
           h := Caption[Ind]+#0;
           DrawText(Canvas.Handle, PChar(@h[1]), 1, Rect, Flags);

           ARect.Right := ARect.Right - RCom.Width;
           Dec(Ind);
         end
       else if RCom.Command = NET_SPACE then
         ARect.Right := ARect.Right - RCom.Width*RCom.Cells;
     end;
   end;
end;


procedure TFrameLabel.Paint;
const
  Alignments: array[TAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
  WordWraps: array[Boolean] of Word = (0, DT_WORDBREAK);
var
  Rect, CalcRect: TRect;
  DrawStyle: Longint;
begin
  with Canvas do
  begin
    Canvas.Font := Self.Font;
    if not Transparent then
    begin
      Brush.Color := Self.Color;
      Brush.Style := bsSolid;
      FillRect(ClientRect);
    end;
    Brush.Style := bsClear;
    Rect := ClientRect;


    if (rclLeft in BoundLines) then
      Rect.Left := Rect.Left + FBoundLinesWidth + 2;
    if (rclRight in BoundLines) then
      Rect.Right := Rect.Right - FBoundLinesWidth - 2;
    if (rclTop in BoundLines) then
      Rect.Top := Rect.Top + FBoundLinesWidth;
    if (rclBottom in BoundLines) then
      Rect.Bottom := Rect.Bottom - FBoundLinesWidth;

    { DoDrawText takes care of BiDi alignments }
    DrawStyle := DT_EXPANDTABS or WordWraps[WordWrap] or Alignments[Alignment];
    { Calculate vertical layout }
    if Layout <> tlTop then
    begin
      CalcRect := Rect;
      DoDrawText(CalcRect, DrawStyle or DT_CALCRECT);
      if Layout = tlBottom then
        OffsetRect(Rect, 0, Height - CalcRect.Bottom)
      else
        OffsetRect(Rect, 0, (Height - CalcRect.Bottom) div 2);
    end;
    if FDigitNet.Count = 0 then
      DoDrawText(Rect, DrawStyle)
    else
    begin
      DrawStyle := DT_EXPANDTABS or Alignments[taCenter];
      DoDrawCellText(Rect, DrawStyle);
    end;
  end;

  DrawFrame(Canvas, FBoundLines, FBoundLinesColor, FBoundLinesWidth, FBoundLinesStyle, Width, Height);

  if ShowCorners then
    DrawCorners(Canvas, Width, Height)
end;


procedure TFrameLabel.AdjustBounds;
begin
  inherited;
{
  if Assigned(Parent) and (Parent is TContainer) then
  begin
    if csLoading in Owner.ComponentState then
      exit;
    TContainer(Parent).FixUpAlign(Self);
  end;
}  
end;


procedure TFrameLabel.DoDrawText(var Rect: TRect; Flags: Integer);
const
  lMaxRes = 600;
var
  Text: string;
  CalcRect: TRect;
begin
  if (Flags and DT_CALCRECT) <> 0 then
  begin
    Text := GetLabelText;
    if ((Flags and DT_CALCRECT) <> 0) and ((Text = '') or ShowAccelChar and
      (Text[1] = '&') and (Text[2] = #0)) then Text := Text + ' ';
    if not ShowAccelChar then
      Flags := Flags or DT_NOPREFIX;
    Flags := DrawTextBiDiModeFlags(Flags);

    Canvas.Font := Font;
    CalcRect := Rect;
    Rect.Right := Round((Rect.Right - Rect.Left) * lMaxRes/cScreenCanvasRes);
    Rect.Bottom := Round((Rect.Bottom - Rect.Top) * lMaxRes/cScreenCanvasRes);
    DrawText(Canvas.Handle, PChar(Text), Length(Text), CalcRect, Flags);
    Canvas.Font.Height := -MulDiv(Font.Size, lMaxRes, 72);
    DrawText(Canvas.Handle, PChar(Text), Length(Text), Rect, Flags);
    Canvas.Font.Height := -MulDiv(Font.Size, cScreenCanvasRes, 72);
    Rect.Right := Round((Rect.Right - Rect.Left) * cScreenCanvasRes/lMaxRes + 0.49);
    Rect.Bottom := Round((Rect.Bottom - Rect.Top) * cScreenCanvasRes/lMaxRes + 0.49);

    if (Length(Text) > 1) and (Abs(Rect.Right - CalcRect.Right) > 0) then
    begin
      FExtraSpacing := Round((Rect.Right - CalcRect.Right)/(Length(Text) - 1));
      if (FExtraSpacing = 0) and ((Rect.Right - CalcRect.Right) < 0 ) then
        FExtraSpacing := -1;
    end
    else
      FExtraSpacing := 0;

    if (rclLeft in BoundLines) then
      Rect.Right := Rect.Right + FBoundLinesWidth + 2;
    if (rclRight in BoundLines) then
      Rect.Right := Rect.Right + FBoundLinesWidth + 2;
    if (rclTop in BoundLines) then
      Rect.Bottom := Rect.Bottom + FBoundLinesWidth;
    if (rclBottom in BoundLines) then
      Rect.Bottom := Rect.Bottom + FBoundLinesWidth;
  end

  else
  begin
    CalcRect := Rect;
    DoDrawText(CalcRect, Flags or DT_CALCRECT);
    SetTextCharacterExtra(Canvas.Handle, FExtraSpacing);
    inherited;
    SetTextCharacterExtra(Canvas.Handle, 0);
  end;
end;


function TFrameLabel.DigitNetIsNotEmpty: Boolean;
begin
  Result := FDigitNet.Count > 0;
end;


procedure TFrameLabel.SetShowCorners(const Value: Boolean);
begin
  FShowCorners := Value;
  Invalidate;
end;

{TrwPCLFile}

constructor TrwPCLFile.Create;
begin
  inherited;

  FData := '';
end;

procedure TrwPCLFile.Assign(Source: TPersistent);
begin
  Data := TrwPCLFile(Source).Data;
end;

procedure TrwPCLFile.SetData(Value: String);
begin
  FData := Value;
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TrwPCLFile.LoadFromFile(AFileName: String);
var
  F: TEvFileStream;
  Buf: Array [0 .. 1023] of Char;
  C: Integer;
  s: String;
begin
  FData := '';
  F := TEvFileStream.Create(AFileName, fmOpenRead);
  try
    repeat
      C := F.Read(Buf, 1024);
      if C > 0 then
      begin
        SetLength(S, C);
        CopyMemory(@S[1], @Buf, C);
        FData := FData+S;
      end;
    until C = 0;
  finally
    F.Free;
  end;
end;


procedure TrwPCLFile.SaveToFile(AFileName: String);
var
  F: TEvFileStream;
begin
  F := TEvFileStream.Create(AFileName, fmCreate);
  try
    if Length(FData) > 0 then
      F.WriteBuffer(FData[1], Length(FData));
  finally
    F.Free;
  end;
end;


function TrwPCLFile.CleanPCL: String;
var
  x: Integer;
begin
  Result := FData;

 // Remove all up to ESC&k0G
  x := Pos(#27 + '&k0G', Result);
  if (x > 0) then
    Delete(Result, 1, x - 1);

// Remove [ESC E] (Escape) at the beginning of the file
  if (Length(Result) > 1) and (Result[1] = chr(27)) and (Result[2] = 'E') then
    Delete(Result, 1, 2);

// Remove [ESC] (form feed) at the end of the file
  if (FData[Length(Result)] = chr(12)) then
    Delete(Result, Length(Result), 1);

// Remove Executive Paper size
  x := Pos(chr(27) + '&l1A', Result);
  if (x > 0) then
    Delete(Result, x, 5);

 // Remove Letter Paper size
  x := Pos(chr(27) + '&l2A', Result);
  if (x > 0) then
    Delete(Result, x, 5);

 // Remove legal paper size
  x := Pos(chr(27) + '&l3A', Result);
  if (x > 0) then
    Delete(Result, x, 5);

  // Remove A4 paper size
  x := Pos(chr(27) + '&l26A', Result);
  if (x > 0) then
    Delete(Result, x, 6);

  // Remove Number of copies
  x := Pos(chr(27) + '&l1X', Result);
  if (x > 0) then
    Delete(Result, x, 5);
end;


procedure TrwPCLFile.PLoadFromFile(AFileName: Variant);
begin
  LoadFromFile(AFileName);
end;


{TPCLLabel}

constructor TPCLLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FWidth := 30;
  FHeight := 20;

  FPCLFont := TrwPCLFile.Create;
  FPicture := TMetafile.Create;
  FPreparedToDraw := False;
  Transparent := True;
  Autosize := True;
end;

destructor TPCLLabel.Destroy;
begin
  FPicture.Free;
  FPCLFont.Free;
  inherited;
end;



procedure TPCLLabel.Paint;
var
  R: TRect;
begin
  if not FPreparedToDraw then
    PreparePicture;

  R := ClientRect;

  if (Length(Caption) = 0) or (Length(FPCLFont.Data) = 0) then
  begin
    Canvas.Brush.Color := clSilver;
    Canvas.Brush.Style := bsDiagCross;
    Canvas.Pen.Color := clSilver;
    Canvas.Rectangle(R);

    Canvas.Font.Name := 'Arial';
    Canvas.Font.Height := -11;
    Canvas.Font.Color := clBlue;
    Canvas.Font.Style := Canvas.Font.Style + [fsBold];
    DrawText(Canvas.Handle, 'PCL', 3, R, DT_SINGLELINE	or DT_CENTER or DT_VCENTER);
  end

  else
  begin
    Canvas.StretchDraw(R, FPicture);
    if ShowCorners then
      DrawCorners(Canvas, Width, Height)
  end;
end;


procedure TPCLLabel.SetPCLFont(Value: TrwPCLFile);
begin
  FPCLFont.Assign(Value);
  FPreparedToDraw := False;
end;

procedure TPCLLabel.PreparePicture;
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  R:TRect;
  k: Extended;

  P: TPoint;
  fID, h: String;
  i: Integer;
begin
  if not Assigned(Parent) then
    Exit;

  FPicture.Clear;

  if (Length(Caption) = 0) or (Length(FPCLFont.Data) = 0) then
  begin
    FWidth := 30;
    FHeight := 20;
    FPreparedToDraw := True;
    AdjustBounds;
    Exit;
  end;

  C := TrwPCLCanvas.Create;
  try
    S := TIsMemoryStream.Create;
    try
      C.NoMargins := True;

      if Copy(FPCLFont.Data, 1, 3) <> #27'*c' then
      begin
        fID := '21002';
        h := #27'*c' + fID + 'D';
        S.WriteBuffer(h[1], Length(h));
      end
      else
      begin
        fID := '';
        i := Pos('D', FPCLFont.Data);
        fID := Copy(FPCLFont.Data, 4, i - 4);
      end;

      S.WriteBuffer(FPCLFont.Data[1], Length(FPCLFont.Data));
      h := #27'*c' + fID + 'D'#27'*c5F'#27'(' + fID + 'X';
      S.WriteBuffer(h[1], Length(h));
      S.WriteBuffer(Caption[1], Length(Caption));
      R := C.CalcTextRect(S, Canvas, cVirtualCanvasRes);
      FPicture.Width := R.Right - R.Left;
      FPicture.Height := R.Bottom - R.Top;

      case C.PrimaryFont.SoftFont.Header.Orientation of
        0, 2: P := Point(0, FPicture.Height);
        1, 3: P := Point(FPicture.Width, FPicture.Height);
      end;

      C.PaintTo(S, FPicture, P, cVirtualCanvasRes);

      k := cScreenCanvasRes / cVirtualCanvasRes;
      FWidth := Round((R.Right - R.Left) * k);
      FHeight := Round((R.Bottom - R.Top) * k);
      if FWidth = 0 then
        FWidth := 30;
      if FHeight = 0 then
        FHeight := 20;
      FPreparedToDraw := True;
      AdjustBounds;
   finally
      S.Free;
    end;
  finally
    C.Free;
  end;
end;

procedure TPCLLabel.AdjustBounds;
begin
  SetBounds(Left, Top, FWidth, FHeight);
end;


procedure TPCLLabel.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if csLoading in Owner.ComponentState then
  begin
    FWidth := AWidth;
    FHeight := AHeight;
  end;

  AWidth := FWidth;
  AHeight := FHeight;
  inherited;
end;


procedure TPCLLabel.SetPCLSize(AWidth, AHeight: Integer);
begin
  FWidth  := AWidth;
  FHeight := AHeight;
  Width := AWidth;
  Height := AHeight;
end;


{TFrameImage}

constructor TFrameImage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FPrevPictChanged := Picture.OnChange;
  Picture.OnChange := PictureChanged;

  FPCLData := TrwPCLFile.Create;
  FPCLData.OnChange := ChangePCL;

  BoundLinesWidth := 1;
  BoundLinesColor := clBlack;
  BoundLinesStyle := psSolid;

  FHandle := 0;
  FWidth := 0;
  FHeight := 0;

  FCompare := True;
  FPreparedToDraw := False;
  FShowCorners := True;

  CaptionText := 'Picture';
end;


destructor TFrameImage.Destroy;
begin
  if FHandle <> 0 then
    FreeLibrary(FHandle);

  FPCLData.Free;

  inherited;
end;


procedure TFrameImage.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  Invalidate;
end;

procedure TFrameImage.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;

  Invalidate;
end;

procedure TFrameImage.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;

  Invalidate;
end;

procedure TFrameImage.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;

  Invalidate;
end;

procedure TFrameImage.ChangePCL(Sender: TObject);
begin
  if PCLDataIsNotEmpty then
  begin
    FBoundLines := [];
    Transparent := True;
    Stretch := False;
    AutoSize := False;
  end;

  if (csLoading in Owner.ComponentState) or
      not Assigned(Parent) then exit;

  FPreparedToDraw := False;
  Invalidate;
end;


procedure TFrameImage.SetPCLData(Value: TrwPCLFile);
begin
  FPCLData.Assign(Value);
end;

procedure TFrameImage.ChangeScale(M, D: Integer);
begin
  inherited;
  if M <> D then
  begin
    FOldStretch := Stretch;
    Stretch := True;
  end
  else
    Stretch := FOldStretch;
end;


procedure TFrameImage.RotatePicture(AAngle: Integer);
var
  srcBitmap: TBitmap;
  destBitmap: TBitmap;
  from_canvas,
  to_Canvas: TCanvas;
  iResult: HResult;
  iOldHeight,
  iOldWidth,
  iRotatedHeight,
  iRotatedWidth: Integer;
  Theta: single;
  sin_theta, cos_theta: Single;
  from_cx, from_cy: Single;
  to_cx, to_cy: Single;
  sfrom_y, sfrom_x: Single;
  ifrom_y, ifrom_x: Integer;
  to_y, to_x: Integer;
  weight_x, weight_y: array[0..1] of Single;
  weight: Single;
  new_red, new_green: Integer;
  new_blue: Integer;
  total_red, total_green: Single;
  total_blue: Single;
  ix, iy: Integer;
  from_x1, from_y1,
  from_x2, from_y2: Integer;
  to_x1, to_y1,
  to_x2, to_y2: Integer;

  procedure SeparateColor(color: TColor;
    var red, green, blue: Integer);
  begin
    red := color mod 256;
    green := (color div 256) mod 256;
    blue := color div 65536;
  end;

begin
  srcBitmap := TBitmap.Create;
  destBitmap := TBitmap.Create;

  try
    srcBitmap.Assign(Picture.Bitmap);
    from_canvas := srcBitmap.Canvas;
    to_canvas := destBitmap.Canvas;

    Theta := Pi * AAngle / 180.0;
    iOldHeight := Picture.Bitmap.Height;
    iOldWidth := Picture.Bitmap.Width;
    iRotatedWidth := Round(Abs(iOldWidth * Cos(Theta)) + Abs(iOldHeight * Sin(theta)));
    iRotatedHeight := Round(Abs(iOldWidth * Sin(theta)) + Abs(iOldHeight * Cos(theta)));
    destBitmap.Height := iRotatedHeight;
    destBitmap.Width := iRotatedWidth;
    from_x1 := 0;
    from_y1 := 0;
    from_x2 := iOldWidth - 1;
    from_y2 := iOldHeight - 1;
    to_x1 := 0;
    to_y1 := 0;
    to_x2 := iRotatedWidth - 1;
    to_y2 := iRotatedHeight - 1;

      // Calculate the sine and cosine of theta for later.
    sin_theta := Sin(theta);
    cos_theta := Cos(theta);

      // Find the centers of the canvases.
    from_cx := (from_x2 - from_x1) / 2;
    from_cy := (from_y2 - from_y1) / 2;
    to_cx := (to_x2 - to_x1) / 2;
    to_cy := (to_y2 - to_y1) / 2;

      // Perform the rotation.
    for to_y := to_y1 to to_y2 do
    begin
      for to_x := to_x1 to to_x2 do
      begin
          // Find the location (from_x, from_y) that
            // rotates to position (to_x, to_y).
        sfrom_x := from_cx + (to_x - to_cx) * cos_theta - (to_y - to_cy) * sin_theta;
        ifrom_x := Trunc(sfrom_x);

        sfrom_y := from_cy + (to_x - to_cx) * sin_theta + (to_y - to_cy) * cos_theta;
        ifrom_y := Trunc(sfrom_y);

            // Only process this pixel if all four
            // adjacent input pixels are inside the
            // allowed input area.
        if (ifrom_x >= from_x1) and (ifrom_x < from_x2) and
          (ifrom_y >= from_y1) and (ifrom_y < from_y2) then
        begin
               // Calculate the weights.
          weight_y[1] := sfrom_y - ifrom_y;
          weight_y[0] := 1 - weight_y[1];
          weight_x[1] := sfrom_x - ifrom_x;
          weight_x[0] := 1 - weight_x[1];

               // Average the color components of the four
               // nearest pixels in from_canvas.
          total_red := 0.0;
          total_green := 0.0;
          total_blue := 0.0;
          for ix := 0 to 1 do
          begin
            for iy := 0 to 1 do
            begin
              SeparateColor(from_canvas.Pixels[ifrom_x + ix, ifrom_y + iy], new_red, new_green, new_blue);
              weight := weight_x[ix] * weight_y[iy];
              total_red := total_red + new_red * weight;
              total_green := total_green + new_green * weight;
              total_blue := total_blue + new_blue * weight;
            end;
          end;

               // Set the output pixel's value.
          to_canvas.Pixels[to_x, to_y] := RGB(Round(total_red), Round(total_green), Round(total_blue));
        end; // End if adjacent pixels in bounds.
      end; // End for to_x := to_x1 to to_x2 loop.
    end; // End for to_y := to_y1 to to_y2 loop.
      // Now copy the new canvas to the original one
    Picture.Bitmap.Height := iRotatedHeight;
    Picture.Bitmap.Width := iRotatedWidth;
    Picture.Bitmap.Assign(destBitmap);
      //from_canvas.CopyRect(Rect(0, 0, iRotatedWidth, iRotatedHeight), to_canvas, Rect(0, 0, iRotatedWidth, iRotatedHeight));
    iResult := S_OK;
  except
    iResult := E_UNEXPECTED;
  end;
  srcBitmap.Free;
  destBitmap.Free;
end;

function TFrameImage.PCLDataIsNotEmpty: Boolean;
begin
  Result := Length(FPCLData.Data) > 0;
end;


procedure TFrameImage.PreparePicture;
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  k: Extended;
begin
  if not Assigned(Parent) then
    Exit;

  if not PCLDataIsNotEmpty then
  begin
    FPreparedToDraw := True;
    Picture.Metafile.Clear;
    Exit;
  end;

  C := TrwPCLCanvas.Create;
  try
    S := TIsMemoryStream.Create;
    try
      S.WriteBuffer(FPCLData.Data[1], Length(FPCLData.Data));
      FPreparedToDraw := True;
      Picture.Metafile.Width := 0;
      Picture.Metafile.Height := 0;
      C.PaintTo(S, Picture.Metafile, Point(0, 0), cVirtualCanvasRes);
      k := cScreenCanvasRes / cVirtualCanvasRes;
      FWidth := Round(Picture.Metafile.Width * k);
      FHeight := Round(Picture.Metafile.Height * k);
      AdjustSize;
   finally
      S.Free;
    end;
  finally
    C.Free;
  end;
end;

procedure TFrameImage.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if csLoading in Owner.ComponentState then
  begin
    FWidth := AWidth;
    FHeight := AHeight;
  end;

  if Assigned(FPCLData) and PCLDataIsNotEmpty then
  begin
    AWidth := FWidth;
    AHeight := FHeight;
  end;

  inherited;
end;


procedure TFrameImage.AdjustSize;
begin
  if PCLDataIsNotEmpty and not (csLoading in Owner.ComponentState) then
    SetBounds(Left, Top, FWidth, FHeight)
  else
    inherited;
end;


procedure TFrameImage.WMPaint(var Message: TWMPaint);
var
  lBRUSH: HBRUSH;
  BrushRec: LOGBRUSH;
  lFont: HFONT;
  FontRec: LogFont;
  R: TRect;

  procedure DrawCorners;
  var
    l: Integer;
    PenRec: LOGPEN;
    lPEN: HPEN;    
  begin
    PenRec.lopnStyle := PS_SOLID;
    PenRec.lopnWidth.X := 1;
    PenRec.lopnColor := ColorToRGB(clBlue);
    lPEN := CreatePenIndirect(PenRec);
    SelectObject(Message.DC, lPEN);
    try
      l := Min(5, Min(Height div 3, Width div 3));

      MoveToEx(Message.DC, 0, 0, nil);
      LineTo(Message.DC, l, 0);
      MoveToEx(Message.DC, 0, 0, nil);
      LineTo(Message.DC, 0, l);

      MoveToEx(Message.DC, Width - 1, 0, nil);
      LineTo(Message.DC, Width - l, 0);
      MoveToEx(Message.DC, Width - 1, 0, nil);
      LineTo(Message.DC, Width - 1, l);

      MoveToEx(Message.DC, 0, Height - 1, nil);
      LineTo(Message.DC, l, Height - 1);
      MoveToEx(Message.DC, 0, Height - 1, nil);
      LineTo(Message.DC, 0, Height - l);

      MoveToEx(Message.DC, Width - 1, Height - 1, nil);
      LineTo(Message.DC, Width - l, Height - 1);
      MoveToEx(Message.DC, Width - 1, Height - 1, nil);
      LineTo(Message.DC, Width - 1, Height - l);
    finally
      DeleteObject(lPEN);
    end;
  end;


  procedure DrawFrame;
  const
    PenStyles: array[TPenStyle] of Word =
      (PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT, PS_NULL,
       PS_INSIDEFRAME);
  var
    X, Y, W, H: Integer;
    lPEN: HPEN;
    PenRec: LOGPEN;
  begin
    PenRec.lopnStyle := PenStyles[BoundLinesStyle];
    PenRec.lopnWidth.X := BoundLinesWidth;
    PenRec.lopnColor := ColorToRGB(BoundLinesColor);
    lPEN := CreatePenIndirect(PenRec);
    SelectObject(Message.DC, lPEN);
    try
      X := BoundLinesWidth div 2;
      Y := BoundLinesWidth div 2;
      W := Width - BoundLinesWidth;
      H := Height - BoundLinesWidth;

      if BoundLinesWidth = 0 then
      begin
        Dec(W);
        Dec(H);
      end;

      if (rclLeft in BoundLines) then
      begin
        MoveToEx(Message.DC, X, Y, nil);
        LineTo(Message.DC, X, Y + H);
      end;

      if (rclRight in BoundLines) then
      begin
        MoveToEx(Message.DC, X + W, Y, nil);
        LineTo(Message.DC, X + W, Y + H);
      end;

      if (rclTop in BoundLines) then
      begin
        MoveToEx(Message.DC, X, Y, nil);
        LineTo(Message.DC, X + W, Y);
      end;

      if (rclBottom in BoundLines) then
      begin
        MoveToEx(Message.DC, X, Y + H, nil);
        LineTo(Message.DC, X + W, Y + H);
      end;

    finally
      DeleteObject(lPEN);


    end;


  end;

begin
  if PCLDataIsNotEmpty then
  begin
    if Message.DC <> 0 then
    begin
      if not FPreparedToDraw then
        PreparePicture;
      if Picture.Metafile.Width > 0 then
        DrawMetafile(Picture.Metafile, Message.DC, FWidth / Picture.Metafile.Width, Left, Top);
    end;
  end

  else if not Assigned(Picture.Graphic) or Picture.Graphic.Empty then
  begin
    BrushRec.lbStyle := BS_HATCHED;
    BrushRec.lbColor := ColorToRGB(clSilver);
    BrushRec.lbHatch := HS_DIAGCROSS;
    lBRUSH := CreateBrushIndirect(BrushRec);
    R :=Rect(0, 0, Width, Height);
    SetBkMode(Message.DC, Windows.OPAQUE);
    SetBkColor(Message.DC, ColorToRGB(clWhite));
    SelectObject(Message.DC, lBRUSH);
    FillRect(Message.DC, R, lBRUSH);
    DeleteObject(lBRUSH);

    FontRec.lfHeight := -11;
    FontRec.lfWidth := 0;
    FontRec.lfEscapement := 0;
    FontRec.lfOrientation := 0;
    FontRec.lfWeight := FW_BOLD;
    FontRec.lfFaceName := 'Arial';
    FontRec.lfItalic := 0;
    FontRec.lfUnderline := 0;
    FontRec.lfStrikeOut := 0;
    FontRec.lfQuality := DEFAULT_QUALITY;
    FontRec.lfOutPrecision := OUT_DEFAULT_PRECIS;
    FontRec.lfClipPrecision := CLIP_DEFAULT_PRECIS;
    FontRec.lfPitchAndFamily := DEFAULT_PITCH;
    lFont := CreateFontIndirect(FontRec);
    SelectObject(Message.DC, lFont);
    SetTextColor(Message.DC, ColorToRGB(clBlue));
    SetBkMode(Message.DC, Windows.TRANSPARENT);
    DrawText(Message.DC, PChar(CaptionText), Length(CaptionText), R, DT_SINGLELINE	or DT_CENTER or DT_VCENTER);
    DeleteObject(lFont);    
  end
  else
    inherited;

  DrawFrame;

  if ShowCorners and not (not Assigned(Picture.Graphic) or Picture.Graphic.Empty) then
    DrawCorners;
end;


procedure TFrameImage.PictureChanged(Sender: TObject);
begin
  if (Picture.Graphic is TBitmap) and not (csLoading in Owner.ComponentState) then
  begin
    PCLData.Data := '';
    if not Stretch and  not AutoSize then
      AutoSize := True;
  end;

  FPrevPictChanged(Sender);
end;


procedure TFrameImage.SetPCLSize(AWidth, AHeight: Integer);
begin
  FWidth  := AWidth;
  FHeight := AHeight;
  Width := AWidth;
  Height := AHeight;
end;


function TFrameImage.CanAutoSize(var NewWidth, NewHeight: Integer): Boolean;
var
  lNewWidth, lNewHeight: Integer;
begin
  lNewWidth := NewWidth;
  lNewHeight := NewHeight;

  Result := inherited CanAutoSize(NewWidth, NewHeight);

  if not (Owner is TControl) and ((Width = 0) or (NewHeight = 0))  then
  begin
    NewWidth := lNewWidth;
    NewHeight := lNewHeight;
    Result := False;
  end;
end;


procedure TFrameImage.SetShowCorners(const Value: Boolean);
begin
  FShowCorners := Value;
  Invalidate;
end;


{ TrwPrintObject }

constructor TrwPrintObject.Create(AOwner: TComponent);
begin
  inherited;
  FCompare := True;
  FLayerNumber := 0;
  FVisible := True;
end;


class function TrwPrintObject.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  Result := Create(AOwner);
end;


procedure TrwPrintObject.WriteObjectInfo(AStream: TStream; APrintObject: TComponent);
var
  w: Smallint;
begin
  with AStream do
  begin
    w := ComponentIndex;
    WriteBuffer(w, SizeOf(Smallint));
    w := TControl(APrintObject).Left;
    WriteBuffer(w, SizeOf(Smallint));
    w := TControl(APrintObject).Top;
    WriteBuffer(w, SizeOf(Smallint));
    w := TControl(APrintObject).Left + TControl(APrintObject).Width;
    WriteBuffer(w, SizeOf(Smallint));
    w := TControl(APrintObject).Top + TControl(APrintObject).Height;
    WriteBuffer(w, SizeOf(Smallint));
  end;
end;


procedure TrwPrintObject.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
begin
  PrepareToPaint(APage.Data);
end;

procedure TrwPrintObject.Print(APage: TrwVirtualPage);
begin
  Paint(rwPrinter.Canvas, APage);
end;

procedure TrwPrintObject.PrepareToPaint(AData: TStream);
begin
  AData.ReadBuffer(FpLeft, SizeOf(FpLeft));
  AData.ReadBuffer(FpTop, SizeOf(FpTop));
  AData.ReadBuffer(FpRight, SizeOf(FpRight));
  AData.ReadBuffer(FpBottom, SizeOf(FpBottom));
end;


function TrwPrintObject.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := (Compare = APrintObject.Compare) and (LayerNumber = APrintObject.LayerNumber);
end;


procedure TrwPrintObject.WriteObjectInfo(AStream: TStream);
var
  w: Smallint;
begin
  with AStream do
  begin
    w := ComponentIndex;
    WriteBuffer(w, SizeOf(Smallint));
    w := Left;
    WriteBuffer(w, SizeOf(Smallint));
    w := Top;
    WriteBuffer(w, SizeOf(Smallint));
    w := Right;
    WriteBuffer(w, SizeOf(Smallint));
    w := Bottom;
    WriteBuffer(w, SizeOf(Smallint));
  end;
end;


procedure TrwPrintObject.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  FpLeft := TrwPrintObject(ASource).Left;
  FpTop := TrwPrintObject(ASource).Top;
  FpRight := TrwPrintObject(ASource).Right;
  FpBottom := TrwPrintObject(ASource).Bottom;
  if not SkipRepProp then
  begin
    FCompare := TrwPrintObject(ASource).Compare;
    FLayerNumber := TrwPrintObject(ASource).LayerNumber;
    FColor := TrwPrintObject(ASource).Color;
  end;
end;


function TrwPrintObject.GetBoundsRect: TRect;
begin
  Result := Rect(Left, Top, Right, Bottom);
end;

procedure TrwPrintObject.SetBoundsRect(const Value: TRect);
begin
  Left := Value.Left;
  Top := Value.Top;
  Right := Value.Right;
  Bottom := Value.Bottom;
end;

function TrwPrintObject.GetTransparent: Boolean;
begin
  Result := Color = clNone;
end;


procedure TrwPrintObject.SetTransparent(const Value: Boolean);
begin
  if csLoading in ComponentState then
  begin
    FReadTransparen := Value;
    Exit;
  end;

  if Value then
    Color := clNone
  else if Transparent then
    Color := clWhite;
end;

procedure TrwPrintObject.Loaded;
begin
  inherited;
  if FReadTransparen then
    Transparent := True;
end;

procedure TrwPrintObject.SetColor(const Value: TColor);
begin
  FColor := Value;
end;

{ TrwPrintText }

function TrwPrintText.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TFrameLabel(APrintObject).Compare) and
        (LayerNumber = TFrameLabel(APrintObject).LayerNumber) and
        (Alignment = TFrameLabel(APrintObject).Alignment) and
        (Layout = TFrameLabel(APrintObject).Layout) and
        (Transparent = TFrameLabel(APrintObject).Transparent) and
        (Transparent or (Color = TFrameLabel(APrintObject).Color)) and
        (WordWrap = TFrameLabel(APrintObject).WordWrap) and
        (DigitNet.Text = TFrameLabel(APrintObject).DigitNet.Text) and
        (Font.Charset = TFrameLabel(APrintObject).Font.Charset) and
        (Font.Color = TFrameLabel(APrintObject).Font.Color) and
        (Font.Height = TFrameLabel(APrintObject).Font.Height) and
        (Font.Name = TFrameLabel(APrintObject).Font.Name) and
        (Font.Pitch = TFrameLabel(APrintObject).Font.Pitch) and
        (Font.Size = TFrameLabel(APrintObject).Font.Size) and
        (Font.Style = TFrameLabel(APrintObject).Font.Style);
end;

constructor TrwPrintText.Create(AOwner: TComponent);
begin
  inherited;
  FFont := TrwFont.Create(Self);
  FDigitNet := TStringList.Create;
  FColor := clWhite;
  FAlignment := taLeftJustify;
  FLayout := tlTop;

  FImage := TMetafile.Create;
  FImage.Enhanced := True;
end;


class function TrwPrintText.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintText(Result) do
  begin
    Compare := TFrameLabel(AInitObject).Compare;
    LayerNumber := TFrameLabel(AInitObject).LayerNumber;
    Alignment := TFrameLabel(AInitObject).Alignment;
    Layout := TFrameLabel(AInitObject).Layout;
    if TFrameLabel(AInitObject).Transparent then
      Color := clNone
    else
      Color := TFrameLabel(AInitObject).Color;
    Font.Assign(TFrameLabel(AInitObject).Font);
    WordWrap := TFrameLabel(AInitObject).WordWrap;
    DigitNet := TFrameLabel(AInitObject).DigitNet;
  end;
end;


destructor TrwPrintText.Destroy;
begin
  FFont.Free;
  FDigitNet.Free;
  FreeAndNil(FImage);
  inherited;
end;

function TrwPrintText.DigitNetIsNotEmpty: Boolean;
begin
  Result := FDigitNet.Count > 0;
end;

procedure TrwPrintText.WriteObjectInfo(AStream: TStream; APrintObject: TComponent);
var
  w: Word;
  h: String;
  R, Rp: TRect;
begin
  with TFrameLabel(APrintObject) do
    if BoundLines <> [] then
    begin
      Rp := TFrameLabel(APrintObject).BoundsRect;
      R := Rp;
      if rclLeft in BoundLines then
        R.Left := R.Left + BoundLinesWidth + 2;
      if rclRight in BoundLines then
        R.Right := R.Right - BoundLinesWidth - 2;
      if rclTop in BoundLines then
        R.Top := R.Top + BoundLinesWidth;
      if rclBottom in BoundLines then
        R.Bottom := R.Bottom - FBoundLinesWidth;

      TFrameLabel(APrintObject).BoundsRect := R;

      inherited;

      TFrameLabel(APrintObject).BoundsRect := Rp;
    end

    else
      inherited;

  with AStream do
  begin
    w := Length(TFrameLabel(APrintObject).Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := TFrameLabel(APrintObject).Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;

procedure TrwPrintText.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  Rect: TRect;
  Phr: PTrwHighlightInfoRec;
  Tinf: TrwTextDrawInfoRec;
  LabelChange: String;
begin
  if (APage.HighlightInfo.Text <> '') and (APage.Data.Position = APage.HighlightInfo.TextObjPosition) then
    Phr := Addr(APage.HighlightInfo)
  else
    Phr := nil;

  inherited;

  Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);

  Tinf.CalcOnly := False;
  Tinf.ZoomRatio := APage.Kf_ZoomX;
  Tinf.Canvas := ACanvas;

  LabelChange := APage.Header.LabelChangesList.FindChange(APage.PageNum, FItemPosOnPage);
  if LabelChange = '' then
    Tinf.Text := FpText
  else
    Tinf.Text := LabelChange;
  Tinf.Font := Font;
  Tinf.Color := Color;
  Tinf.DigitNet := FDigitNet.Text;
  Tinf.WordWrap := WordWrap;
  Tinf.Alignment := Alignment;
  Tinf.Layout := Layout;
  Tinf.TextRect := Rect;

  DrawTextImage(@Tinf);

  if Assigned(Phr) then
    InvertRect(ACanvas.Handle, APage.ScaleRect(Phr.TextRect));
end;


procedure TrwPrintText.SetDigitNet(const Value: TStrings);
begin
  FDigitNet.Assign(Value);
end;

procedure TrwPrintText.SetFont(const Value: TrwFont);
begin
  FFont.Assign(Value);
end;

procedure TrwPrintText.PrepareToPaint(AData: TStream);
var
  w: Word;
begin
  inherited;

  FItemPosOnPage := AData.Position;
  AData.ReadBuffer(w, SizeOf(w));
  SetLength(FpText, w);
  if w > 0 then
    AData.ReadBuffer(FpText[1], w);
end;



function TrwPrintText.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := inherited CompareObject(APrintObject);
  Result := Result and
        (Alignment = TrwPrintText(APrintObject).Alignment) and
        (Layout = TrwPrintText(APrintObject).Layout) and
        (Color = TrwPrintText(APrintObject).Color) and
        (WordWrap = TrwPrintText(APrintObject).WordWrap) and
        (DigitNet.Text = TrwPrintText(APrintObject).DigitNet.Text) and
        (Font.Charset = TrwPrintText(APrintObject).Font.Charset) and
        (Font.Color = TrwPrintText(APrintObject).Font.Color) and
        (Font.Height = TrwPrintText(APrintObject).Font.Height) and
        (Font.Name = TrwPrintText(APrintObject).Font.Name) and
        (Font.Pitch = TrwPrintText(APrintObject).Font.Pitch) and
        (Font.Size = TrwPrintText(APrintObject).Font.Size) and
        (Font.Style = TrwPrintText(APrintObject).Font.Style);
end;

procedure TrwPrintText.WriteObjectInfo(AStream: TStream);
var
  w: Word;
  h: String;
begin
  inherited;

  with AStream do
  begin
    w := Length(Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;


procedure TrwPrintText.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  inherited;

  FpText := TrwPrintText(ASource).Text;

  if not SkipRepProp then
  begin
    FAlignment := TrwPrintText(ASource).Alignment;
    Layout := TrwPrintText(ASource).Layout;
    Color := TrwPrintText(ASource).Color;
    Font.Assign(TrwPrintText(ASource).Font);
    WordWrap := TrwPrintText(ASource).WordWrap;
    DigitNet.Text := TrwPrintText(ASource).DigitNet.Text;
  end;
end;

{ TrwPrintFrame }

constructor TrwPrintFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FColor := clNone;
  FBoundLines := [rclLeft, rclRight, rclTop, rclBottom];
  FBoundLinesWidth := 1;
  FBoundLinesColor := clBlack;
  FBoundLinesStyle := psSolid;
end;


class function TrwPrintFrame.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  if not (AInitObject.ClassInfo = TGraphicFrame.ClassInfo) then
    raise ErwException.Create('Wrong print object');

  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintFrame(Result) do
  begin
    Compare := TGraphicFrame(AInitObject).Compare;
    LayerNumber := TGraphicFrame(AInitObject).LayerNumber;
    if TGraphicFrame(AInitObject).Transparent then
      Color := clNone
    else
      Color := TGraphicFrame(AInitObject).Brush.Color;
    BoundLines := TGraphicFrame(AInitObject).BoundLines;
    BoundLinesColor := TGraphicFrame(AInitObject).BoundLinesColor;
    BoundLinesWidth := TGraphicFrame(AInitObject).BoundLinesWidth;
    BoundLinesStyle := TGraphicFrame(AInitObject).BoundLinesStyle;
  end;
end;

function TrwPrintFrame.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TGraphicFrame(APrintObject).Compare) and
        (LayerNumber = TGraphicFrame(APrintObject).LayerNumber) and
        (Transparent = TGraphicFrame(APrintObject).Transparent) and
        (Transparent or (Color = TGraphicFrame(APrintObject).Brush.Color)) and
        (BoundLines = TGraphicFrame(APrintObject).BoundLines) and
        (BoundLinesColor = TGraphicFrame(APrintObject).BoundLinesColor) and
        (BoundLinesWidth = TGraphicFrame(APrintObject).BoundLinesWidth) and
        (BoundLinesStyle = TGraphicFrame(APrintObject).BoundLinesStyle);
end;


procedure TrwPrintFrame.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  Rect: TRect;
begin
  inherited;

  Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
  Rect := APage.ScaleRect(Rect);

  ACanvas.Brush.Color := Color;
  if not Transparent then
    ACanvas.FillRect(Rect);

  DrawFrame(ACanvas, BoundLines, BoundLinesColor,
    Round(BoundLinesWidth * APage.Kf_ZoomY), BoundLinesStyle,
    Rect.Right - Rect.Left, Rect.Bottom - Rect.Top , Rect.Left, Rect.Top);
end;


function TrwPrintFrame.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := inherited CompareObject(APrintObject);
  Result := Result and
        (Color = TrwPrintFrame(APrintObject).Color) and
        (BoundLines = TrwPrintFrame(APrintObject).BoundLines) and
        (BoundLinesColor = TrwPrintFrame(APrintObject).BoundLinesColor) and
        (BoundLinesWidth = TrwPrintFrame(APrintObject).BoundLinesWidth) and
        (BoundLinesStyle = TrwPrintFrame(APrintObject).BoundLinesStyle);
end;


procedure TrwPrintFrame.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  inherited;

  if not SkipRepProp then
  begin
    FBoundLinesWidth := TrwPrintFrame(ASource).BoundLinesWidth;
    FBoundLinesColor := TrwPrintFrame(ASource).BoundLinesColor;
    FBoundLinesStyle := TrwPrintFrame(ASource).BoundLinesStyle;
    FBoundLines := TrwPrintFrame(ASource).BoundLines;
    FColor := TrwPrintFrame(ASource).Color;
  end;
end;

procedure TrwPrintFrame.SetColor(const Value: TColor);
begin
  inherited;
  if csLoading in ComponentState then
  begin
    if Value <> clNone then
      FReadTransparen := False;
  end;
end;

procedure TrwPrintFrame.SetTransparent(const Value: Boolean);
begin
  inherited;
  if (csLoading in ComponentState) and not Value then
    Color := clWhite;
end;

{ TrwPrintShape }

constructor TrwPrintShape.Create(AOwner: TComponent);
begin
  inherited;
  FColor := clWhite;
  FBoundLinesColor := clBlack;
  FBoundLinesWidth := 1;
  FBoundLinesStyle := psSolid;
  FShapeType := stRectangle;
end;

class function TrwPrintShape.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  if not (AInitObject.ClassInfo = TExtShape.ClassInfo) then
    raise ErwException.Create('Wrong print object');

  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintShape(Result) do
  begin
    Compare := TExtShape(AInitObject).Compare;
    LayerNumber := TExtShape(AInitObject).LayerNumber;
    if TExtShape(AInitObject).Brush.Style = bsClear then
      Color := clNone
    else
      Color := TExtShape(AInitObject).Brush.Color;
    BoundLinesColor := TExtShape(AInitObject).Pen.Color;
    BoundLinesWidth := TExtShape(AInitObject).Pen.Width;
    BoundLinesStyle := TExtShape(AInitObject).Pen.Style;
    ShapeType := TExtShape(AInitObject).Shape;
  end;
end;

function TrwPrintShape.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TExtShape(APrintObject).Compare) and
        (LayerNumber = TExtShape(APrintObject).LayerNumber) and
        (Transparent = (TExtShape(APrintObject).Brush.Style = bsClear)) and
        (Transparent or (Color = TExtShape(APrintObject).Brush.Color)) and
        (BoundLinesColor = TExtShape(APrintObject).Pen.Color) and
        (BoundLinesWidth = TExtShape(APrintObject).Pen.Width) and
        (BoundLinesStyle = TExtShape(APrintObject).Pen.Style) and
        (ShapeType = TExtShape(APrintObject).Shape);
end;

procedure TrwPrintShape.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  X, Y, W, H, S: Integer;
  Rect: TRect;
begin
  inherited;

  Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
  Rect := APage.ScaleRect(Rect);

  with ACanvas do
  begin
    Pen.Color := BoundLinesColor;
    if Transparent then
    begin
      Brush.Color := clNone;
      Brush.Style := bsClear;
    end
    else
    begin
      Brush.Color := Color;
      Brush.Style := bsSolid;
    end;

    Pen.Width := Round(FBoundLinesWidth * APage.Kf_ZoomY);
    Pen.Style := BoundLinesStyle;

    X := Rect.Left + Pen.Width div 2;
    Y := Rect.Top + Pen.Width div 2;

    W := (Rect.Right - Rect.Left) - Pen.Width + 1;
    H := (Rect.Bottom - Rect.Top) - Pen.Width + 1;
    if Pen.Width = 0 then
    begin
      Dec(W);
      Dec(H);
    end;
    if W < H then
      S := W
    else
      S := H;
    if ShapeType in [stSquare, stRoundSquare, stCircle] then
    begin
      Inc(X, (W - S) div 2);
      Inc(Y, (H - S) div 2);
      W := S;
      H := S;
    end;
    case ShapeType of
      stRectangle, stSquare:
        Rectangle(X, Y, X + W, Y + H);
      stRoundRect, stRoundSquare:
        RoundRect(X, Y, X + W, Y + H, S div 4, S div 4);
      stCircle, stEllipse:
        Ellipse(X, Y, X + W, Y + H);
    end;
  end;
end;

function TrwPrintShape.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := inherited CompareObject(APrintObject);
  Result := Result and
        (Color = TrwPrintShape(APrintObject).Color) and
        (BoundLinesColor = TrwPrintShape(APrintObject).BoundLinesColor) and
        (BoundLinesWidth = TrwPrintShape(APrintObject).BoundLinesWidth) and
        (BoundLinesStyle = TrwPrintShape(APrintObject).BoundLinesStyle) and
        (ShapeType = TrwPrintShape(APrintObject).ShapeType);
end;


procedure TrwPrintShape.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  inherited;

  if not SkipRepProp then
  begin
    FBoundLinesWidth := TrwPrintShape(ASource).BoundLinesWidth;
    FBoundLinesColor := TrwPrintShape(ASource).BoundLinesColor;
    FColor := TrwPrintShape(ASource).Color;
    FBoundLinesStyle := TrwPrintShape(ASource).BoundLinesStyle;
    FShapeType := TrwPrintShape(ASource).ShapeType;
  end;  
end;

{ TrwPrintImage }

constructor TrwPrintImage.Create(AOwner: TComponent);
begin
  inherited;
  FPicture := TrwPictureHolder.Create(Self);
  FPCLData := TrwPCLFile.Create;
  FStretch := True;
  FMacroID := 0;
  FTransparent := False;
end;

class function TrwPrintImage.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  if not (AInitObject.ClassInfo = TFrameImage.ClassInfo) then
    raise ErwException.Create('Wrong print object');

  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintImage(Result) do
  begin
    Compare := TFrameImage(AInitObject).Compare;
    LayerNumber := TFrameImage(AInitObject).LayerNumber;
    Transparent := TFrameImage(AInitObject).Transparent;
    PCLData := TFrameImage(AInitObject).PCLData;
    Picture.Assign(TFrameImage(AInitObject).Picture);
    Stretch := TFrameImage(AInitObject).Stretch;
  end;
end;

destructor TrwPrintImage.Destroy;
begin
  FPicture.Free;
  FPCLData.Free;
  inherited;
end;


procedure TrwPrintImage.SetPCLData(const Value: TrwPCLFile);
begin
  Picture.DestroyPictureHandler;
  FPCLData.Assign(Value);
end;

procedure TrwPrintImage.SetPicture(const Value: TrwPictureHolder);
begin
  FPicture.Assign(Value);
end;

function TrwPrintImage.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TFrameImage(APrintObject).Compare) and
        (LayerNumber = TFrameImage(APrintObject).LayerNumber) and
        (Transparent = TFrameImage(APrintObject).Transparent) and
        (CompareStr(PCLData.Data, TFrameImage(APrintObject).PCLData.Data) = 0);

  if Result and (Length(PCLData.Data) = 0)  then
    Result := Picture.PictureIsTheSame(TFrameImage(APrintObject).Picture);
end;


procedure TrwPrintImage.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  Rect: TRect;
  PDFStream : TMemoryStream;
begin
  inherited;

  if (APage.Header.Version >= 2) and (Length(FPCLData.Data) <> 0) or
     (APage.Header.Version < 2) and (Length(FPCLData.Data) <> 0) and (not Assigned(Picture.Graphic) or Picture.Graphic.Empty) then
  begin
    PreparePicture(APage);
    Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
    Rect := APage.ScaleRect(Rect);
    if Picture.Metafile.Width > 0 then
      DrawMetafile(Picture.Metafile, ACanvas.Handle, (Rect.Right - Rect.Left) / Picture.Metafile.Width, Rect.Left, Rect.Top);
  end

  else if Assigned(Picture.Graphic) then
  begin
    Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
    Rect := APage.ScaleRect(Rect);
    Picture.Graphic.Transparent := FTransparent;

    if Picture.Graphic is TMetafile then
      Picture.Metafile.Inch := APage.PixelsPerInch;

    if (Picture.Graphic is TisMetafile) and (APage is TrwPDFPrinterPage) then
    begin
      if (TisMetafile(Picture.Graphic).PDFExists) and (Assigned(TrwPDFPrinterPage(APage).DynaPDFPrinter)) then
      begin
        PDFStream := TMemoryStream.Create;
        try
          TisMetafile(Picture.Graphic).GetPDFBackground(PDFStream);
          TrwPDFPrinterPage(APage).DynaImportPageFromMemoryStream(PDFStream);
        finally
          PDFStream.Free;
        end;
      end
      else
        ACanvas.StretchDraw(Rect, Picture.Graphic);
    end
    else
      ACanvas.StretchDraw(Rect, Picture.Graphic);
  end;
end;


procedure TrwPrintImage.Print(APage: TrwVirtualPage);
var
  Rect: TRect;
begin
  if Length(PCLData.Data) <> 0 then
  begin
    PrepareToPaint(APage.Data);

    if (APage.Header.Version >= 2) and rwPrinter.PrintPCLasGraphic then
    begin
      PreparePicture(APage);
      Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
      Rect := APage.ScaleRect(Rect);
      if Picture.Metafile.Width > 0 then
        DrawMetafile(Picture.Metafile, rwPrinter.Canvas.Handle, (Rect.Right - Rect.Left) / Picture.Metafile.Width, Rect.Left, Rect.Top);
    end

    else
    begin
      PCLRestoreFont(rwPrinter.Canvas, APage);
      if not rwPrinter.PCLMacroIsStored(FMacroID) then
        FMacroID := rwPrinter.PCLStoreMacro(FPCLData.CleanPCL);
      rwPrinter.PCLPrintString(#27+'*v0O');  //NELCO SUCKS! Some forms need to turn on transparecy manualy
      rwPrinter.PCLPrintMacro(FMacroID);
      PCLRestoreFont(rwPrinter.Canvas, APage);
    end;
  end

  else
    inherited;
end;

function TrwPrintImage.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := inherited CompareObject(APrintObject);
  Result := Result and (Transparent = TrwPrintImage(APrintObject).Transparent) and
           (CompareStr(PCLData.Data, TrwPrintImage(APrintObject).PCLData.Data) = 0);

  if Result and (Length(PCLData.Data) = 0)  then
    Result := Picture.PictureIsTheSame(TrwPrintImage(APrintObject).Picture);
end;


procedure TrwPrintImage.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  inherited;

  if not SkipRepProp then
  begin
    Transparent := TrwPrintImage(ASource).Transparent;
    FStretch := TrwPrintImage(ASource).Stretch;
    FPicture.Assign(TrwPrintImage(ASource).Picture);
    FPCLData.Assign(TrwPrintImage(ASource).PCLData);
  end;
end;


procedure TrwPrintImage.PreparePicture(APage: TrwVirtualPage);
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  k: Extended;
begin
  if (APage.Header.Version < 2) and (Length(FPCLData.Data) = 0) then
  begin
    Picture.Metafile.Clear;
    Exit;
  end;

  if not Assigned(Picture.PictureHandler) then
  begin
    C := TrwPCLCanvas.Create;
    try
      S := TIsMemoryStream.Create;
      try
        S.WriteBuffer(FPCLData.Data[1], Length(FPCLData.Data));
        Picture.Metafile.Width := 0;
        Picture.Metafile.Height := 0;
        C.PaintTo(S, Picture.Metafile, Point(0, 0), cVirtualCanvasRes);
     finally
        S.Free;
      end;
    finally
      C.Free;
    end;
  end;

  k := APage.PixelsPerInch / cVirtualCanvasRes;
  FpRight :=  FpLeft + Round(FPicture.Width * k);
  FpBottom := FpTop + Round(FPicture.Height * k);
end;


function TrwPrintImage.IsNotPCL: Boolean;
begin
  Result := Length(PCLData.Data) = 0;
end;


function TrwPrintImage.GetTransparent: Boolean;
begin
  Result := FTransparent;
end;

procedure TrwPrintImage.SetTransparent(const Value: Boolean);
begin
  if csLoading in ComponentState then
    FReadTransparen := Value;

  FTransparent := Value;
end;


{ TrwPrintPCLText }

function TrwPrintPCLText.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TPCLLabel(APrintObject).Compare) and
        (LayerNumber = TPCLLabel(APrintObject).LayerNumber) and
        (CompareStr(PCLFont.Data, TPCLLabel(APrintObject).PCLFont.Data) = 0);
end;

procedure TrwPrintPCLText.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean = False);
begin
  inherited;

  FpText := TrwPrintPCLText(ASource).Text;
  
  if not SkipRepProp then
    FPCLFont.Assign(TrwPrintPCLText(ASource).PCLFont);
end;

function TrwPrintPCLText.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := (Compare = TrwPrintPCLText(APrintObject).Compare) and
        (LayerNumber = TrwPrintPCLText(APrintObject).LayerNumber) and
        (CompareStr(PCLFont.Data, TrwPrintPCLText(APrintObject).PCLFont.Data) = 0);
end;

constructor TrwPrintPCLText.Create(AOwner: TComponent);
begin
  inherited;
  FPCLFont := TrwPCLFile.Create;
  FFontID := '';
  FImages :=  TrwPCLTextImages.Create(TrwPCLTextImage);
  FWidth := 0;
  FHeight := 0;
end;

class function TrwPrintPCLText.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintPCLText(Result) do
  begin
    Compare := TPCLLabel(AInitObject).Compare;
    LayerNumber := TPCLLabel(AInitObject).LayerNumber;
    PCLFont := TPCLLabel(AInitObject).PCLFont;
  end;
end;

destructor TrwPrintPCLText.destroy;
begin
  FImages.Free;
  FPCLFont.Free;
  inherited;
end;

procedure TrwPrintPCLText.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  Rect: TRect;
  IM: TrwPCLTextImage;
begin
  inherited;

  if (Length(FpText) = 0) or (Length(FPCLFont.Data) = 0) then
    Exit;

  IM := FImages.ImageByText(FpText);
  if not Assigned(IM) then
  begin
    IM := FImages.AddImage;
    IM.FText := FpText;
    FPicture := IM.FPicture;
    PreparePicture(ACanvas, APage);
    IM.FSize.cx := FpRight - FpLeft;
    IM.FSize.cy := FpBottom - FpTop;
  end
  else
  begin
    FPicture := IM.FPicture;
    FpRight := FpLeft + IM.FSize.cx;
    FpBottom := FpTop + IM.FSize.cy;
  end;

  Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
  Rect := APage.ScaleRect(Rect);

  FPicture.Transparent := True;
  ACanvas.StretchDraw(Rect, FPicture);
end;

procedure TrwPrintPCLText.PrepareToPaint(AData: TStream);
var
  w: Word;
begin
  inherited;

  AData.ReadBuffer(w, SizeOf(w));
  SetLength(FpText, w);
  if w > 0 then
    AData.ReadBuffer(FpText[1], w);
end;

procedure TrwPrintPCLText.SetPCLFont(const Value: TrwPCLFile);
begin
  FPCLFont.Assign(Value);
end;

procedure TrwPrintPCLText.WriteObjectInfo(AStream: TStream; APrintObject: TComponent);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(TPCLLabel(APrintObject).Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := TPCLLabel(APrintObject).Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;

procedure TrwPrintPCLText.WriteObjectInfo(AStream: TStream);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;


procedure TrwPrintPCLText.PreparePicture(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  R: TRect;
  k: Extended;

  P: TPoint;
  fID, h: String;
  i: Integer;
begin
  FPicture.Clear;

  C := TrwPCLCanvas.Create;
  try
    S := TIsMemoryStream.Create;
    try
      C.NoMargins := True;

      if Copy(FPCLFont.Data, 1, 3) <> #27'*c' then
      begin
        fID := '21002';
        h := #27'*c' + fID + 'D';
        S.WriteBuffer(h[1], Length(h));
      end
      else
      begin
        fID := '';
        i := Pos('D', FPCLFont.Data);
        fID := Copy(FPCLFont.Data, 4, i - 4);
      end;

      S.WriteBuffer(FPCLFont.Data[1], Length(FPCLFont.Data));
      h := #27'*c' + fID + 'D'#27'*c5F'#27'(' + fID + 'X';
      S.WriteBuffer(h[1], Length(h));
      S.WriteBuffer(FpText[1], Length(FpText));
      R := C.CalcTextRect(S, ACanvas, cVirtualCanvasRes);

      FPicture.Width := R.Right - R.Left;
      FPicture.Height := R.Bottom - R.Top;

      case C.PrimaryFont.SoftFont.Header.Orientation of
        0, 2: P := Point(0, FPicture.Height);
        1, 3: P := Point(FPicture.Width, FPicture.Height);
      end;

      k := APage.PixelsPerInch / cVirtualCanvasRes;

      C.PaintTo(S, FPicture, P, cVirtualCanvasRes);

      FpRight :=  FpLeft + Round(FPicture.Width * k);
      FpBottom :=   FpTop + Round(FPicture.Height * k);


//      FFontHeight := C.PrimaryFont.SoftFont.Header.CellHeight * Round((APage.PixelsPerInch * APage.Kf_ZoomX) / 300) -
//        (C.PrimaryFont.SoftFont.Header.CellHeight - C.PrimaryFont.SoftFont.Header.BaselinePos) * Round((APage.PixelsPerInch * APage.Kf_ZoomX) / 300);
   finally
      S.Free;
    end;
  finally
    C.Free;
  end;
end;


procedure TrwPrintPCLText.Print(APage: TrwVirtualPage);
var
  h, h1: String;
  fh: Integer;
  i: integer;
  Rect: TRect;
begin
  if APage.Header.Version >= 2 then
    inherited

  else
  begin
    PrepareToPaint(APage.Data);

    if PCLFont.Data = '' then Exit;

    PCLRestoreFont(rwPrinter.Canvas, APage);

    Rect := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
    Rect := APage.ScaleRect(Rect);


    if not rwPrinter.PCLFontIsStored(FFontID) then
      FFontID := rwPrinter.PCLStoreFont(PCLFont.Data);

    h := InitString;
    h1 := '';
    while Length(h) > 0 do
    begin
      i := Pos('(s', h);
      if i = -1 then break;

      i := i+2;
      while (i <= Length(h)) and (h[i] in ['0'..'9', '.']) do
      begin
        h1 := h1+h[i];
        i := i+1;
      end;
      if (i > Length(h)) or (h[i] in ['v', 'V']) then break;

      Delete(h, 1, i);
      h1 := '';
    end;

    if h1 <> '' then
      try
        fh := System.Round(StrToFloat(h1)*0.013837* APage.PrnResX);
        Rect.Top := Rect.Top + fh;
      except
      end;

    rwPrinter.PCLPrintPCLText(FFontID, InitString, FpText, Rect.TopLeft);

    PCLRestoreFont(rwPrinter.Canvas, APage);
  end;
end;


{ TrwPCLTextImage }

constructor TrwPCLTextImage.Create(Collection: TCollection);
begin
  inherited;
  FPicture := TMetafile.Create;
end;

destructor TrwPCLTextImage.Destroy;
begin
  FPicture.Free;
  inherited;
end;


{ TrwPCLTextImages }

function TrwPCLTextImages.AddImage: TrwPCLTextImage;
const cMaxCachedImages = 10;
begin
  if Count = cMaxCachedImages then
    Delete(0);

  Result := TrwPCLTextImage(Add);
end;

function TrwPCLTextImages.ImageByText(const AText: String): TrwPCLTextImage;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameStr(TrwPCLTextImage(Items[i]).FText, AText) then
    begin
      Result := TrwPCLTextImage(Items[i]);
      break;
    end;
end;



{ TrwPrintLine }

constructor TrwPrintLine.Create(AOwner: TComponent);
begin
  inherited;
  FColor := clNone;
  FLineWidth := 1;
  FLineColor := clBlack;
  FLineStyle := psSolid;
end;

procedure TrwPrintLine.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean);
begin
  inherited;

  if not SkipRepProp then
  begin
    FLineWidth := TrwPrintLine(ASource).LineWidth;
    FLineColor := TrwPrintLine(ASource).LineColor;
    FLineStyle := TrwPrintLine(ASource).LineStyle;
  end;
end;

function TrwPrintLine.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := inherited CompareObject(APrintObject);
  Result := Result and
        (LineColor = TrwPrintLine(APrintObject).LineColor) and
        (LineWidth = TrwPrintLine(APrintObject).LineWidth) and
        (LineStyle = TrwPrintLine(APrintObject).LineStyle);
end;

procedure TrwPrintLine.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  R: TRect;
begin
  inherited;

  R := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
  R := APage.ScaleRect(R);

  with ACanvas do
  begin
    Pen.Mode := pmCopy;
    Pen.Width := Round(LineWidth * APage.Kf_ZoomY);
    if Pen.Width = 0 then
      Pen.Width := 1;

    Pen.Style := LineStyle;
    Pen.Color := LineColor;

    MoveTo(R.Left, R.Top);
    LineTo(R.Right, R.Bottom);
  end;
end;


{ TrwPrintBarcode1D }

procedure TrwPrintBarcode1D.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean);
begin
  inherited;

  FpText := TrwPrintBarcode1D(ASource).Text;

  if not SkipRepProp then
  begin
    Stretch := TrwPrintBarcode1D(ASource).Stretch;
    Font.Assign(TrwPrintBarcode1D(ASource).Font);
    BarHeight := TrwPrintBarcode1D(ASource).BarHeight;
    BarType := TrwPrintBarcode1D(ASource).BarType;
    BarColor := TrwPrintBarcode1D(ASource).BarColor;
    ShowText := TrwPrintBarcode1D(ASource).ShowText;

    SupplementalCode := TrwPrintBarcode1D(ASource).SupplementalCode;
    UPCEANSupplement5 := TrwPrintBarcode1D(ASource).UPCEANSupplement5;
    UPCEANSupplement2 := TrwPrintBarcode1D(ASource).UPCEANSupplement2;
    GuardBars := TrwPrintBarcode1D(ASource).GuardBars;
    PostnetHeightShortBar := TrwPrintBarcode1D(ASource).PostnetHeightShortBar;
    PostnetHeightTallBar := TrwPrintBarcode1D(ASource).PostnetHeightTallBar;
    CheckCharacter := TrwPrintBarcode1D(ASource).CheckCharacter;
    Transparent := TrwPrintBarcode1D(ASource).Color = clNone;
    Color := TrwPrintBarcode1D(ASource).Color;
    UPCESytem := TrwPrintBarcode1D(ASource).UPCESytem;
    CODABARStartChar := TrwPrintBarcode1D(ASource).CODABARStartChar;
    CODABARStopChar := TrwPrintBarcode1D(ASource).CODABARStopChar;
    Code128Set := TrwPrintBarcode1D(ASource).Code128Set;
    SupSeparation := TrwPrintBarcode1D(ASource).SupSeparation;
    VerticalOrientation := TrwPrintBarcode1D(ASource).VerticalOrientation;
    TopMargin := TrwPrintBarcode1D(ASource).TopMargin;
    LeftMargin := TrwPrintBarcode1D(ASource).LeftMargin;
  end;
end;

function TrwPrintBarcode1D.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := (Compare = TrwPrintBarcode1D(APrintObject).Compare) and
        (LayerNumber = TrwPrintBarcode1D(APrintObject).LayerNumber) and
        (Transparent = TrwPrintBarcode1D(APrintObject).Transparent) and
        (Transparent or (Color = TrwPrintBarcode1D(APrintObject).Color)) and
        (Stretch = TrwPrintBarcode1D(APrintObject).Stretch) and
        (Font.Charset = TrwPrintBarcode1D(APrintObject).Font.Charset) and
        (Font.Color = TrwPrintBarcode1D(APrintObject).Font.Color) and
        (Font.Height = TrwPrintBarcode1D(APrintObject).Font.Height) and
        (Font.Name = TrwPrintBarcode1D(APrintObject).Font.Name) and
        (Font.Pitch = TrwPrintBarcode1D(APrintObject).Font.Pitch) and
        (Font.Size = TrwPrintBarcode1D(APrintObject).Font.Size) and
        (Font.Style = TrwPrintBarcode1D(APrintObject).Font.Style) and
        (BarHeight = TrwPrintBarcode1D(APrintObject).BarHeight) and
        (BarType = TrwPrintBarcode1D(APrintObject).BarType) and
        (BarColor = TrwPrintBarcode1D(APrintObject).BarColor) and
        (CheckCharacter = TrwPrintBarcode1D(APrintObject).CheckCharacter) and
        (PostnetHeightTallBar = TrwPrintBarcode1D(APrintObject).PostnetHeightTallBar) and
        (PostnetHeightShortBar = TrwPrintBarcode1D(APrintObject).PostnetHeightShortBar) and
        (SupplementalCode = TrwPrintBarcode1D(APrintObject).SupplementalCode) and
        (GuardBars = TrwPrintBarcode1D(APrintObject).GuardBars) and
        (UPCEANSupplement2 = TrwPrintBarcode1D(APrintObject).UPCEANSupplement2) and
        (UPCEANSupplement5 = TrwPrintBarcode1D(APrintObject).UPCEANSupplement5) and
        (UPCESytem = TrwPrintBarcode1D(APrintObject).UPCESytem) and
        (CODABARStartChar = TrwPrintBarcode1D(APrintObject).CODABARStartChar) and
        (CODABARStopChar = TrwPrintBarcode1D(APrintObject).CODABARStopChar) and
        (Code128Set = TrwPrintBarcode1D(APrintObject).Code128Set) and
        (SupSeparation = TrwPrintBarcode1D(APrintObject).SupSeparation) and
        (VerticalOrientation = TrwPrintBarcode1D(APrintObject).VerticalOrientation) and
        (ShowText = TrwPrintBarcode1D(APrintObject).ShowText);
end;

function TrwPrintBarcode1D.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TExtBarcode1D(APrintObject).Compare) and
        (LayerNumber = TExtBarcode1D(APrintObject).LayerNumber) and
        (Transparent = TExtBarcode1D(APrintObject).Transparent) and
        (Transparent or (Color = TExtBarcode1D(APrintObject).BackColor)) and
        (Font.Charset = TExtBarcode1D(APrintObject).Font.Charset) and
        (Font.Color = TExtBarcode1D(APrintObject).Font.Color) and
        (Font.Height = TExtBarcode1D(APrintObject).Font.Height) and
        (Font.Name = TExtBarcode1D(APrintObject).Font.Name) and
        (Font.Pitch = TExtBarcode1D(APrintObject).Font.Pitch) and
        (Font.Size = TExtBarcode1D(APrintObject).Font.Size) and
        (Font.Style = TExtBarcode1D(APrintObject).Font.Style) and
        (BarHeight = TExtBarcode1D(APrintObject).BarHeightPixels) and
        (BarType = TExtBarcode1D(APrintObject).BarType) and
        (BarColor = TExtBarcode1D(APrintObject).BarColor) and
        (CheckCharacter = TExtBarcode1D(APrintObject).CheckCharacter) and
        (PostnetHeightTallBar = TExtBarcode1D(APrintObject).PostnetHeightTallBar) and
        (PostnetHeightShortBar = TExtBarcode1D(APrintObject).PostnetHeightShortBar) and
        (SupplementalCode = TExtBarcode1D(APrintObject).CodeSup) and
        (GuardBars = TExtBarcode1D(APrintObject).GuardBars) and
        (UPCEANSupplement2 = TExtBarcode1D(APrintObject).UPCEANSupplement2) and
        (UPCEANSupplement5 = TExtBarcode1D(APrintObject).UPCEANSupplement5) and
        (UPCESytem = TExtBarcode1D(APrintObject).UPCESytem) and
        (CODABARStartChar = TExtBarcode1D(APrintObject).CODABARStartChar) and
        (CODABARStopChar = TExtBarcode1D(APrintObject).CODABARStopChar) and
        (Code128Set = TExtBarcode1D(APrintObject).Code128Set) and
        (SupSeparation = TExtBarcode1D(APrintObject).SupSeparation) and
        (VerticalOrientation = TExtBarcode1D(APrintObject).VerticalOrientation) and
        (ShowText = TExtBarcode1D(APrintObject).ShowText);
end;

constructor TrwPrintBarcode1D.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
  FFont := TrwFont.Create(Self);
  FColor := clWhite;
  FStretch := True;
  FBarType := CODE39;
  FBarHeight := 40;
  FBarColor := clBlack;
  FShowText := True;
  FGuardBars := True;
  FCheckCharacter := True;
  FPostnetHeightTallBar := 20;
  FPostnetHeightShortBar := 10;
  FUPCESytem := '1';
  FCODABARStartChar := 'A';
  FCODABARStopChar := 'B';
  FCode128Set := 'B';
  FSupSeparation := 10;
  FTopMargin := 10;
  FleftMargin := 10;
end;

class function TrwPrintBarcode1D.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  if not (AInitObject.ClassInfo = TExtBarcode1D.ClassInfo) then
    raise ErwException.Create('Wrong print object');

  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintBarcode1D(Result) do
  begin
    Compare := TExtBarcode1D(AInitObject).Compare;
    LayerNumber := TExtBarcode1D(AInitObject).LayerNumber;
    if TExtBarcode1D(AInitObject).BackColor = clNone then
      Transparent := true
    else
      Transparent := false;
    Color := TExtBarcode1D(AInitObject).BackColor;
    Font.Assign(TExtBarcode1D(AInitObject).Font);
    BarHeight := TExtBarcode1D(AInitObject).BarHeightPixels;
    BarType := TExtBarcode1D(AInitObject).BarType;
    BarColor := TExtBarcode1D(AInitObject).BarColor;
    ShowText := TExtBarcode1D(AInitObject).ShowText;
    SupplementalCode := TExtBarcode1D(AInitObject).CodeSup;
    UPCEANSupplement5 := TExtBarcode1D(AInitObject).UPCEANSupplement5;
    UPCEANSupplement2 := TExtBarcode1D(AInitObject).UPCEANSupplement2;
    GuardBars := TExtBarcode1D(AInitObject).GuardBars;
    PostnetHeightShortBar := TExtBarcode1D(AInitObject).PostnetHeightShortBar;
    PostnetHeightTallBar := TExtBarcode1D(AInitObject).PostnetHeightTallBar;
    CheckCharacter := TExtBarcode1D(AInitObject).CheckCharacter;
    UPCESytem := TExtBarcode1D(AInitObject).UPCESytem;
    CODABARStartChar := TExtBarcode1D(AInitObject).CODABARStartChar;
    CODABARStopChar := TExtBarcode1D(AInitObject).CODABARStopChar;
    Code128Set := TExtBarcode1D(AInitObject).Code128Set;
    SupSeparation := TExtBarcode1D(AInitObject).supSeparation;
    VerticalOrientation := TExtBarcode1D(AInitObject).VerticalOrientation;
    TopMargin := TExtBarcode1D(AInitObject).TopMargin;
    LeftMargin := TExtBarcode1D(AInitObject).LeftMargin;
  end;
end;

destructor TrwPrintBarcode1D.Destroy;
begin
  FFont.Free;
  inherited;
end;

procedure TrwPrintBarcode1D.Paint(ACanvas: TCanvas; APage: TrwVirtualPage);
var
  R: TRect;
  S: TSize;
  Barcode: TExtBarcode1D;
  Metafile, RotatedMetafile: TMetafile;
begin
  inherited;

  Barcode := TExtBarcode1D.Create(nil);
  try
    Barcode.barType := BarType;
    Barcode.AutoSize := false;
    Barcode.Code := Text;
    Barcode.BarHeightPixels := BarHeight;
    Barcode.BackColor := Color;
    Barcode.BarColor := BarColor;
    Barcode.ShowText := ShowText;
    Barcode.CheckCharacter := CheckCharacter;
    Barcode.PostnetHeightTallBar := PostnetHeightTallBar;
    Barcode.PostnetHeightShortBar := PostnetHeightShortBar;
    Barcode.GuardBars := GuardBars;
    Barcode.UPCEANSupplement2 := UPCEANSupplement2;
    Barcode.UPCEANSupplement5 := UPCEANSupplement5;
    Barcode.CodeSup := SupplementalCode;
    Barcode.UPCESytem := UPCESytem;
    Barcode.CODABARStartChar := CODABARStartChar;
    Barcode.CODABARStopChar := CODABARStopChar;
    Barcode.Code128Set := Code128Set;
    Barcode.supSeparation := SupSeparation;
    Font.AssignTo(Barcode.Font);
    if VerticalOrientation then
    begin
      Barcode.VerticalOrientation := false;
      Barcode.TopMargin := 0;
      Barcode.LeftMargin := 0;
    end
    else
    begin
      Barcode.VerticalOrientation := VerticalOrientation;
      Barcode.TopMargin := TopMargin;
      Barcode.LeftMargin := LeftMargin;
    end;


    S := Barcode.GetActualSize(ACanvas.Handle);
    Barcode.SetBounds(0, 0, S.cx, S.cy);

    Metafile := Barcode.GetMetafilePicture(ACanvas.Handle);
    try
      R := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
      R := APage.ScaleRect(R);
      if VerticalOrientation then
      begin
        RotatedMetafile := NTRotateMetafile90ClockWise(MetaFile.Handle, Metafile.Width, Metafile.Height, ACanvas.Handle);
        try
          if Barcode.BackColor <> clNone then
          begin
            ACanvas.Brush.Color:= Barcode.BackColor;
            ACanvas.Brush.Style := bsSolid;
            ACanvas.FillRect(R);
          end;
          R.Left := R.Left + TopMargin;
          R.Right := R.Right - TopMargin;
          R.Top := R.Top + LeftMargin;
          R.Bottom := R.Bottom - LeftMargin;
          ACanvas.StretchDraw(R, RotatedMetafile);
        finally
          FreeAndNil(RotatedMetafile);
        end;
      end
      else
        ACanvas.StretchDraw(R, Metafile);
    finally
      FreeAndNil(Metafile);
    end;
  finally
    Barcode.Free;
  end;
end;

procedure TrwPrintBarcode1D.PrepareToPaint(AData: TStream);
var
  w: Word;
begin
  inherited;
  AData.ReadBuffer(w, SizeOf(w));
  SetLength(FpText, w);
  if w > 0 then
    AData.ReadBuffer(FpText[1], w);
end;

procedure TrwPrintBarcode1D.SetFont(const AValue: TrwFont);
begin
  FFont.Assign(AValue);
end;

procedure TrwPrintBarcode1D.WriteObjectInfo(AStream: TStream; APrintObject: TComponent);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(TExtBarcode1D(APrintObject).Code);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := TExtBarcode1D(APrintObject).Code;
      WriteBuffer(h[1], w);
    end;
  end;
end;

procedure TrwPrintBarcode1D.WriteObjectInfo(AStream: TStream);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;

{ TExtBarcode1D }

constructor TExtBarcode1D.Create(AOwner: TComponent);
begin
  inherited;
  TopMargin := 0;
  LeftMargin := 0;
  AutoSize := True;
  AutoFit := True;
end;

destructor TExtBarcode1D.Destroy;
begin
  inherited;
end;

function TExtBarcode1D.Transparent: Boolean;
begin
  Result := BackColor = clNone;
end;

{ TExtBarcodePDF417 }

constructor TExtBarcodePDF417.Create(AOwner: TComponent);
begin
  inherited;
  TopMargin := 0;
  LeftMargin := 0;
  AutoSize := True;
  AutoFit := True;
end;

destructor TExtBarcodePDF417.Destroy;
begin

  inherited;
end;

function TExtBarcodePDF417.Transparent: Boolean;
begin
  Result := BackColor = clNone;
end;

{ TrwPrintBarcodePDF417 }

procedure TrwPrintBarcodePDF417.Assign(ASource: TrwPrintObject; SkipRepProp: Boolean);
begin
  inherited;

  FpText := TrwPrintBarcodePDF417(ASource).Text;

  if not SkipRepProp then
  begin
    Stretch := TrwPrintBarcodePDF417(ASource).Stretch;
    BarColor := TrwPrintBarcodePDF417(ASource).BarColor;
    Transparent := TrwPrintBarcodePDF417(ASource).Color = clNone;
    Color := TrwPrintBarcodePDF417(ASource).Color;
    TopMargin := TrwPrintBarcodePDF417(ASource).TopMargin;
    LeftMargin := TrwPrintBarcodePDF417(ASource).LeftMargin;
    BarHeightPixels := TrwPrintBarcodePDF417(ASource).BarHeightPixels;
    BarWidthPixels := TrwPrintBarcodePDF417(ASource).BarWidthPixels;
    ColumnsNumber := TrwPrintBarcodePDF417(ASource).ColumnsNumber;
    RowsNumber:= TrwPrintBarcodePDF417(ASource).RowsNumber;
    MaxRows := TrwPrintBarcodePDF417(ASource).MaxRows;
    ECLevel := TrwPrintBarcodePDF417(ASource).ECLevel;
    EncodingMode := TrwPrintBarcodePDF417(ASource).EncodingMode;
    Truncated := TrwPrintBarcodePDF417(ASource).Truncated;
    HorizontalPixelShaving := TrwPrintBarcodePDF417(ASource).HorizontalPixelShaving;
    VerticalPixelShaving := TrwPrintBarcodePDF417(ASource).VerticalPixelShaving;
  end;
end;

function TrwPrintBarcodePDF417.CompareObject(APrintObject: TComponent): Boolean;
begin
  Result := (Compare = TExtBarcodePDF417(APrintObject).Compare) and
        (LayerNumber = TExtBarcodePDF417(APrintObject).LayerNumber) and
        (Transparent = TExtBarcodePDF417(APrintObject).Transparent) and
        (Transparent or (Color = TExtBarcodePDF417(APrintObject).BackColor)) and
        (BarHeightPixels = TExtBarcodePDF417(APrintObject).BarHeightPixels) and
        (BarColor = TExtBarcodePDF417(APrintObject).BarColor) and
        (BarWidthPixels = TExtBarcodePDF417(APrintObject).BarWidthPixels) and
        (ColumnsNumber = TExtBarcodePDF417(APrintObject).pdfColumns) and
        (RowsNumber = TExtBarcodePDF417(APrintObject).pdfRows) and
        (MaxRows = TExtBarcodePDF417(APrintObject).pdfMaxRows) and
        (ECLevel = TExtBarcodePDF417(APrintObject).pdfECLevel) and
        (EncodingMode = TExtBarcodePDF417(APrintObject).pdfMode) and
        (HorizontalPixelShaving = TExtBarcodePDF417(APrintObject).HorizontalPixelShaving) and
        (VerticalPixelShaving = TExtBarcodePDF417(APrintObject).VerticalPixelShaving) and
        (Truncated = TExtBarcodePDF417(APrintObject).pdfTruncated);
end;

function TrwPrintBarcodePDF417.CompareObject(APrintObject: TrwPrintObject): Boolean;
begin
  Result := (Compare = TrwPrintBarcodePDF417(APrintObject).Compare) and
        (LayerNumber = TrwPrintBarcodePDF417(APrintObject).LayerNumber) and
        (Transparent = TrwPrintBarcodePDF417(APrintObject).Transparent) and
        (Transparent or (Color = TrwPrintBarcodePDF417(APrintObject).Color)) and
        (Stretch = TrwPrintBarcodePDF417(APrintObject).Stretch) and
        (BarHeightPixels = TrwPrintBarcodePDF417(APrintObject).BarHeightPixels) and
        (BarColor = TrwPrintBarcodePDF417(APrintObject).BarColor) and
        (BarWidthPixels = TrwPrintBarcodePDF417(APrintObject).BarWidthPixels) and
        (ColumnsNumber = TrwPrintBarcodePDF417(APrintObject).ColumnsNumber) and
        (RowsNumber = TrwPrintBarcodePDF417(APrintObject).RowsNumber) and
        (MaxRows = TrwPrintBarcodePDF417(APrintObject).MaxRows) and
        (ECLevel = TrwPrintBarcodePDF417(APrintObject).ECLevel) and
        (EncodingMode = TrwPrintBarcodePDF417(APrintObject).EncodingMode) and
        (HorizontalPixelShaving = TrwPrintBarcodePDF417(APrintObject).HorizontalPixelShaving) and
        (VerticalPixelShaving = TrwPrintBarcodePDF417(APrintObject).VerticalPixelShaving) and
        (Truncated = TrwPrintBarcodePDF417(APrintObject).Truncated);
end;

constructor TrwPrintBarcodePDF417.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
  FColor := clWhite;
  FStretch := True;
  FFont := TrwFont.Create(Self);
  FBarColor := clBlack;
  FBarHeightPixels := 7;
  FBarWidthPixels := 1;
  FTopMargin := 10;
  FLeftMargin := 10;
  FColumnsNumber := 10;
  FRowsNumber := 0;
  FMaxRows := 0;
  FECLevel := 0;
  FEncodingMode := PDF_BINARY;
  FTruncated := false;
  FHorizontalPixelShaving := 0;
  FVerticalPixelShaving := 0;
end;

class function TrwPrintBarcodePDF417.CreateAs(AOwner, AInitObject: TComponent): TrwPrintObject;
begin
  if not (AInitObject.ClassInfo = TExtBarcodePDF417.ClassInfo) then
    raise ErwException.Create('Wrong print object');

  Result := inherited CreateAs(AOwner, AInitObject);

  with TrwPrintBarcodePDF417(Result) do
  begin
    Compare := TExtBarcodePDF417(AInitObject).Compare;
    LayerNumber := TExtBarcodePDF417(AInitObject).LayerNumber;
    if TExtBarcodePDF417(AInitObject).BackColor = clNone then
      Transparent := true
    else
      Transparent := false;
    Color := TExtBarcodePDF417(AInitObject).BackColor;
    BarHeightPixels := TExtBarcodePDF417(AInitObject).BarHeightPixels;
    BarWidthPixels := TExtBarcodePDF417(AInitObject).BarWidthPixels;
    BarColor := TExtBarcodePDF417(AInitObject).BarColor;
    TopMargin := TExtBarcodePDF417(AInitObject).TopMargin;
    LeftMargin := TExtBarcodePDF417(AInitObject).LeftMargin;
    ColumnsNumber := TExtBarcodePDF417(AInitObject).PDFColumns;
    RowsNumber := TExtBarcodePDF417(AInitObject).PDFRows;
    MaxRows := TExtBarcodePDF417(AInitObject).PDFMaxRows;
    ECLevel := TExtBarcodePDF417(AInitObject).PDFECLevel;
    EncodingMode := TExtBarcodePDF417(AInitObject).PDFMode;
    Truncated := TExtBarcodePDF417(AInitObject).PDFTruncated;
    HorizontalPixelShaving := TExtBarcodePDF417(AInitObject).HorizontalPixelShaving;
    VerticalPixelShaving := TExtBarcodePDF417(AInitObject).VerticalPixelShaving;
  end;
end;

destructor TrwPrintBarcodePDF417.Destroy;
begin
  FFont.Free;
  inherited;
end;

procedure TrwPrintBarcodePDF417.Paint(ACanvas: TCanvas;  APage: TrwVirtualPage);
var
  R: TRect;
  S: TSize;
  Barcode: TExtBarcodePDF417;
  Metafile: TMetafile;
begin
  inherited;

  Barcode := TExtBarcodePDF417.Create(nil);
  try
    Barcode.AutoSize := false;
    Barcode.Code := Text;
    Barcode.BarHeightPixels := BarHeightPixels;
    Barcode.BackColor := Color;
    Barcode.BarColor := BarColor;
    Barcode.BarWidthPixels := BarWidthPixels;
    Barcode.TopMargin := TopMargin;
    Barcode.LeftMargin := LeftMargin;
    Barcode.PDFColumns := ColumnsNumber;
    Barcode.PDFRows := RowsNumber;
    Barcode.PDFMaxRows := MaxRows;
    Barcode.PDFECLevel := ECLevel;
    Barcode.PDFMode := EncodingMode;
    Barcode.PDFTruncated := Truncated;
    Barcode.HorizontalPixelShaving := HorizontalPixelShaving;
    Barcode.VerticalPixelShaving := VerticalPixelShaving;

    S := Barcode.GetActualSize(ACanvas.Handle);
    Barcode.SetBounds(0, 0, S.cx, S.cy);

    Metafile := Barcode.GetMetafilePicture(ACanvas.Handle);
    try
      R := Classes.Rect(FpLeft, FpTop, FpRight, FpBottom);
      R := APage.ScaleRect(R);
      ACanvas.StretchDraw(R, Metafile);
    finally
      FreeAndNil(Metafile);
    end;
  finally
    Barcode.Free;
  end;
end;

procedure TrwPrintBarcodePDF417.PrepareToPaint(AData: TStream);
var
  w: Word;
begin
  inherited;
  AData.ReadBuffer(w, SizeOf(w));
  SetLength(FpText, w);
  if w > 0 then
    AData.ReadBuffer(FpText[1], w);
end;

procedure TrwPrintBarcodePDF417.SetFont(const AValue: TrwFont);
begin
  FFont.Assign(AValue);
end;

procedure TrwPrintBarcodePDF417.WriteObjectInfo(AStream: TStream);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(Text);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := Text;
      WriteBuffer(h[1], w);
    end;
  end;
end;

procedure TrwPrintBarcodePDF417.WriteObjectInfo(AStream: TStream; APrintObject: TComponent);
var
  w: Word;
  h: String;
begin
  inherited;
  with AStream do
  begin
    w := Length(TExtBarcodePDF417(APrintObject).Code);
    WriteBuffer(w, SizeOf(Word));
    if w > 0 then
    begin
      h := TExtBarcodePDF417(APrintObject).Code;
      WriteBuffer(h[1], w);
    end;
  end;
end;

initialization
  RegisterClasses([TExtShape, TFrameLabel, TGraphicFrame, TFrameImage, TPCLLabel]);

end.
