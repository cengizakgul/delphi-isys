unit rmTypes;

interface

uses Windows, Graphics, Classes, Controls, StdCtrls, Messages, rwEngineTypes, EvStreamUtils, SysUtils;

const
  WM_RMDEBUG = WM_USER + 500;
  Ascii2XlsDelimiter = #9;
  Ascii2XlsQualifier = '';

type
  TrwCode = array of Variant;
  PTrwCode = ^TrwCode;

  TrwDebInf = array of String;
  PTrwDebInf = ^TrwDebInf;

  TrwVarTypeKind = TrwRPType;

  TrwVarVisibility = (rvvPublished, rvvPublic, rvvProtected, rvvPrivate);
  TrwVarVisibilities = set of TrwVarVisibility;

  TPositionAlign = (alPositionNone, alPositionLeft, alPositionTop, alPositionBottom, alPositionRight, alPositionCenter, alPositionMiddle, alPositionContainerCenter, alPositionContainerMiddle, alPositionVerticalSpaces, alPositionHorisontalSpaces);

  TrwDsgnPaletteSetting = (rwDPSShowOnPalette, rwDPSAdvancedModeOnly, rwDPSComponentEditModeOnly);

  TrwDsgnPaletteSettings = set of TrwDsgnPaletteSetting;


  TrwDsgnObjectType = (rwDOTInternal, rwDOTNotVisual, rwDOTPrintForm, rwDOTInputForm, rwDOTASCIIForm, rwDOTXMLForm);

  TrwDBIMoveSizeDirection = (rwDBIMDLeft, rwDBIMDRight, rwDBIMDTop, rwDBIMDBottom);
  TrwDBIMoveSizeDirections = set of TrwDBIMoveSizeDirection;
  TrwDBISelectorType = (rwDBISBNone, rwDBISBDots);

  TrwDsgnLockState = (rwLSUnlocked, rwLSLocked, rwLSHidden);

  TrwDesignBehaviorInfoRec = record
    MovingDirections:   TrwDBIMoveSizeDirections;
    ResizingDirections: TrwDBIMoveSizeDirections;
    Selectable: Boolean;
    SelectorType: TrwDBISelectorType;
    StandAloneObject: Boolean;
    AcceptControls:  Boolean;
    AcceptedClases: TCommaDilimitedString;
  end;

  TrmDesignEventType = (rmdQueryChanged,
                        rmdDestroyResizer, rmdDestroyChildResizer,
                        rmdObjectResized,  rmdSelectObject, rmdSelectChildObject, rmdObjectSelected,
                        rmdObjectTagMouseDown, rmdActionStarted, rmdActionStoped,
                        rmdZoomChanged, rmdChildrenListChanged,

                        rmdVMChangeCSreg, rmdVMChangeIPreg, rmdVMBreakPointFound, rmdBreakPointToggle
                       );


  TrwVMAddressList = array of TrwVMAddress;


  IrmInterfacedObject = Interface(IInterface)
  ['{2FA6A947-478F-402E-A635-86A16309215E}']
    function  VCLObject: TObject;
  end;

  IrmDesigner = interface;
  IrmVarFunctHolder = interface;
  IrmFMLFormulas = interface;
  IrmComponent = interface;


   IrwClass = interface
   ['{A10D48A8-52FC-4FE5-91F8-94B45ACCD3D1}']
     function  GetClassName: String;
     function  GetClassGUID: String;
     function  GetAncestor: IrwClass;
     function  DescendantOf(const AClassName: String): Boolean; overload;
     function  DescendantOf(const AClass: IrwClass): Boolean; overload;
     function  GetVCLClass: TClass;
     function  GetUserClass: IrmComponent;     
     function  CreateObjectInstance(const AInherit: Boolean = True; const IgnoreUserProperties: Boolean = False): TObject;
     function  IsUserClass: Boolean;
     function  GetClassDisplayName: String;
     function  GetClassDescription: String;
   end;


  IrmFunctions = interface;   

  IrmDesignObject = Interface(IrmInterfacedObject)
  ['{2F6532FE-91AC-4B1E-96E8-DDDF3F1E8BF4}']
    function  GetDesigner: IrmDesigner;
    procedure SetDesigner(ADesigner: IrmDesigner);
    function  RealObject: TComponent;
    function  ObjectType: TrwDsgnObjectType;
    function  IsVirtualOwner: Boolean;
    function  IsInheritedComponent: Boolean;
    function  GetWinControlParent: TWinControl;
    procedure Notify(AEvent: TrmDesignEventType);
    function  GetParentExtRect: TRect;
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
    function  ObjectAtPos(APos: TPoint): IrmDesignObject;
    function  GetParent: IrmDesignObject;
    procedure SetParent(AParent: IrmDesignObject);
    function  GetObjectIndex: Integer;
    procedure SetObjectIndex(const AIndex: Integer);
    function  ClientToScreen(APos: TPoint): TPoint;
    function  ScreenToClient(APos: TPoint): TPoint;
    procedure WriteComp(Writer: TWriter);
    function  GetVarFunctHolder: IrmVarFunctHolder;
    function  GetFormulas: IrmFMLFormulas;
    function  GetBoundsRect: TRect;
    procedure SetBoundsRect(ARect: TRect);
    function  GetLeft: Integer;
    procedure SetLeft(AValue: Integer);
    function  GetTop: Integer;
    procedure SetTop(AValue: Integer);
    function  GetWidth: Integer;
    procedure SetWidth(AValue: Integer);
    function  GetHeight: Integer;
    procedure SetHeight(AValue: Integer);
    procedure Refresh;

    function  GetClassName: String;
    function  IsLibComponent: Boolean;
    function  ObjectInheritsFrom(const AClassName: String): Boolean;
    function  PropertyExists(const APropertyName: String): Boolean;
    function  PropertiesByTypeName(const ATypeName: String): TrwStrList;
    procedure SetPropertyValue(const APropertyName: String; Value: Variant);
    function  GetPropertyValue(const APropertyName: String): Variant;
    function  GetRootOwner: IrmDesignObject;
    function  GetPathFromRootOwner: String;
    function  GetPathFromRootParent: String;
    function  GetInterfacedPropByType(const APropertyType: String): IrmInterfacedObject;

    function  GetLibComponentByClass(const AClassName: String): IrmComponent;
    procedure GetAvailableClassList(const AList: TStringList);

    function  CreateChildObject(const AClassName: String): IrmDesignObject;
    procedure AddChildObject(AObject: IrmDesignObject);
    procedure RemoveChildObject(AObject: IrmDesignObject);
    procedure InitializeForDesign;
    function  GetObjectOwner: IrmDesignObject;
    function  GetChildrenCount: Integer;
    function  GetChildByIndex(AIndex: Integer): IrmDesignObject;
    function  GetControlCount: Integer;
    function  GetControlByIndex(AIndex: Integer): IrmDesignObject;
    function  GetEvents: IrmFunctions;    

    function  GetObjectClassDescription: String;
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
  end;


  TrmListOfObjects = array of IrmDesignObject;

  IrwCollection = interface;

  IrwCollectionItem = interface(IrmInterfacedObject)
  ['{2A325A91-76FC-43D8-B98F-73B6CD08BBCD}']
    function  GetCollection: IrwCollection;
    function  RealObject: TCollectionItem;
    function  GetItemIndex: Integer;
    procedure SetItemIndex(const AIndex: Integer);
    function  GetItemID: String;
    function  IsInheritedItem: Boolean;
  end;

  IrwCollection = interface(IrmInterfacedObject)
  ['{9792CE8A-E0FF-4840-93A6-1D04C73AE02B}']
    function  RealObject: TCollection;
    function  GetItemCount: Integer;
    function  GetItemByIndex(const AItemIndex: Integer): IrwCollectionItem;
    function  GetItemByID(const AItemID: String): IrwCollectionItem;
    function  GetComponentOwner: IrmDesignObject;
    function  AddItem: IrwCollectionItem;
    procedure DeleteItem(const AItemIndex: Integer);
    procedure Clear;
    procedure BeginUpdate;
    procedure EndUpdate;
  end;


  TrwFunctDsgnTimeInfo = (fdtShowInEditor, fdtDataFilter);
  TrwFunctDsgnTimeInfoSet = set of TrwFunctDsgnTimeInfo;


  IrwQBShowingField = interface(IrmInterfacedObject)
  ['{4D325D62-C874-46AC-8363-F4697B6CEFA9}']
    function GetID: String;
    function GetIndex: Integer;
    function GetName: String;
    function GetDataType: TrwVarTypeKind;
  end;


  IrwQBQuery = interface(IrmInterfacedObject)
  ['{20A3DC7B-1641-42C3-8BC2-C36EAA75363D}']
    function  GetFieldList: TCommaDilimitedString;
    procedure Clear;
    function  GetQBFieldByName(const AFieldName: String): IrwQBShowingField;
    function  GetQBFieldByID(const AFieldID: String): IrwQBShowingField;
  end;


  TrmFMLActionType =(fatNone, fatSetProperty, fatDataFilter, fatCalcControl);
  TrmFMLItemType = (fitNone, fitBracket, fitSeparator, fitOperation, fitFunction, fitObject, fitField, fitConst, fitVariable, fitError);

  TrmFMLItemInfoRec = record
    ItemType: TrmFMLItemType;
    Value: Variant;
  end;


  IrmFMLFormula = interface(IrwCollectionItem)
  ['{6900F524-4C85-4571-BB4F-4DECB33F4CD1}']
    procedure SetActionType(const Value: TrmFMLActionType);
    function  GetActionType: TrmFMLActionType;
    procedure SetActionInfo(const Value: String);
    function  GetActionInfo: String;
    procedure Clear;
    function  AddItem(const AType: TrmFMLItemType; const AValue: Variant; const AddInfo: String = ''; const AIndex: Integer = -1): Integer;
    function  GetItemCount: Integer;
    function  GetItemInfo(const AItemIndex: Integer): TrmFMLItemInfoRec;
    function  ThereAreObjectsBelongingTo(const AObjectParentPath: String): Boolean;    
  end;


  IrmFMLFormulas = interface(IrwCollection)
  ['{4E2DBE85-AF77-4968-95A4-85672DFAAFC5}']
    function  GetFormulaByAction(AActionType: TrmFMLActionType; const AActionInfo: String): IrmFMLFormula;
  end;


  IrmQuery = interface(IrmInterfacedObject)
  ['{C95D5435-74F7-4140-85C4-2AE92B3D8404}']
    function  GetMasterQuery: IrmQuery;
    procedure Clear;
    procedure Prepare;
    function  GetQBQuery: IrwQBQuery;    
  end;


  IrmQueryDrivenObject = interface(IrmDesignObject)
  ['{EA6BE366-1030-498A-BBDE-0D129CED0ACA}']
    function  GetQuery: IrmQuery;
  end;

  TrmDataFieldInfoRec = record
    Name:       String;
    RootPath:   String;
    DataSource: IrmQueryDrivenObject;
    DefaultDS:  Boolean;
    QBField:    IrwQBShowingField;
  end;


  IrmQueryAwareObject = interface(IrmDesignObject)
  ['{C601F6A2-A093-499C-A0E6-AAC875EA5580}']
    function  GetFieldName: String;
    procedure SetFieldName(const AFieldName: String);
    function  GetDataFieldInfo: TrmDataFieldInfoRec;
    procedure SetValueFromDataSource;
  end;


  IrmTableCell = interface;

  IrmTable = interface(IrmDesignObject)
  ['{0F892B67-5514-4177-AE66-E8126E124767}']
    function  MergeCells(const ACornerCell1Name, ACornerCell2Name: String): IrmTableCell;
    function  GetCellsInRect(const ACornerCell1: IrmTableCell; const ACornerCell2: IrmTableCell): TrmListOfObjects;
    procedure ClearTable;
  end;


  IrmTableCell = interface(IrmDesignObject)
  ['{49DBEBC5-CF3A-4E66-925A-2E4145467064}']
    procedure CreateSubTable;
    procedure DestroySubTable;
    procedure SplitCell(const ACols, ARows: Integer);
    function  GetClosestCell(const ADirection: Char): IrmTableCell;  // U, D, L, R - Up, Down, Left, Right
  end;


  IrmDBTableGroup = interface(IrwCollectionItem)
  ['{44070A40-BC0B-4EDE-BBA7-8D71037A69A8}']
    function  GetGroupName: String;
    procedure SetGroupName(const Value: String);
    function  GetFields: TCommaDilimitedString;
    procedure SetFields(const Value: TCommaDilimitedString);
    function  GetDisabled: Boolean;
    procedure SetDisabled(const Value: Boolean);
    function  GetPrintHeaderBand: Boolean;
    procedure SetPrintHeaderBand(const Value: Boolean);
    function  GetPrintFooterBand: Boolean;
    procedure SetPrintFooterBand(const Value: Boolean);
  end;


  IrmDBTableGroups = Interface(IrwCollection)
  ['{BFC824C7-1448-4D59-AA5A-4097CC927B57}']
    function  GetGroupByName(const AGroupName: String): IrmDBTableGroup;
    function  GetPageBreakGroupID: String;
    procedure SetPageBreakGroup(const AGroupID: String);
  end;


  IrmDBTableObject = interface(IrmDesignObject)
  ['{551DA8C0-29CA-455A-B37B-1E38C0256158}']
    function GetGroups: IrmDBTableGroups;
    function IsSubTable: Boolean;
  end;


  IrmReportObject = interface(IrmDesignObject)
  ['{F9B57F57-1CA0-49C5-8525-9E498A7C5CCD}']
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
    procedure LoadFromFile(const AFileName: string);
    procedure SaveToFile(const AFileName: string);
    procedure Compile(ASyntaxCheck: Boolean = False);
    procedure SetReportParams(const AParams: PTrwReportParamList);
    procedure GetReportParams(const AParams: PTrwReportParamList);
    function  GetCompiledCode: PTrwCode;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;
  end;


  TrmVFHVarInfoRec = record
    Name: string;
    VarType: TrwVarTypeKind;
    PointerType: String;
    Visibility: TrwVarVisibility;
    InheritedVar: Boolean;
    DisplayName: String;
    ShowInQB: Boolean;
    ShowInFmlEditor: Boolean;
    Value: Variant;
  end;


  IrmVariable = Interface(IrwCollectionItem)
  ['{F971A548-095A-42D4-8456-D2A4271AC41B}']
    function  GetVarInfo: TrmVFHVarInfoRec;
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetVarType: TrwVarTypeKind;
    procedure SetVarType(const AValue: TrwVarTypeKind);
    function  GetPointerType: String;
    procedure SetPointerType(const AValue: String);
    function  GetVisibility: TrwVarVisibility;
    procedure SetVisibility(const AValue: TrwVarVisibility);
    function  GetDisplayName: String;
    procedure SetDisplayName(const AValue: String);
    function  GetShowInQB: Boolean;
    procedure SetShowInQB(const AValue: Boolean);
    function  GetShowInFmlEditor: Boolean;
    procedure SetShowInFmlEditor(const AValue: Boolean);
    function  GetValue: Variant;
    procedure SetValue(const AValue: Variant);
  end;


  IrmVariables = Interface(IrwCollection)
  ['{3A99009D-63E4-4173-8A80-CAB4795E0AB6}']
    function  GetVariableByName(const AVarName: String): IrmVariable;
  end;


  IrmLibCollectionItem = Interface(IrwCollectionItem)
  ['{B77A67ED-1317-40D5-8C95-49C2CAEC0F3A}']
    function  GetName: String;
    function  GetDisplayName: String;
    procedure SetDisplayName(const ADisplayName: String);
    function  GetGroup: String;
    procedure SetGroup(const AGroupName: String);
    function  GetDescription: String;
    procedure SetDescription(const ADescription: String);
  end;


  IrmLibCollection = Interface(IrwCollection)
  ['{7A4F6952-419C-4F85-81B2-4109A5BB84D7}']
    function  IndexOf(const AName: String): Integer;
    procedure CreateDebInf;
    procedure ClearDebInf;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;
    procedure Compile;
  end;


  TrmInheritedFunctInfoRec = record
    ClassName:          String;
    FunctText:          String;
    ThroughInheritance: Boolean;
  end;

  TrmInheritedFunctInfoList = array of TrmInheritedFunctInfoRec;

  IrmFunction = Interface(IrmLibCollectionItem)
  ['{F4695388-5CD7-43DC-A937-CA957B42EF4E}']
    function  GetFunctionText: String;
    procedure SetFunctionText(const AFunctText: String);
    function  GetInheritanceInfo: TrmInheritedFunctInfoList;
  end;


  IrmFunctions = Interface(IrmLibCollection)
  ['{B7A96773-038B-4AF1-851C-F42DC34EB72B}']
    function  GetFunctionByName(const AFunctName: String): IrmFunction;
  end;


  IrmComponent = Interface(IrmLibCollectionItem)
  ['{2D367EC4-5482-47ED-A742-B71113D36BB6}']
    function  GetClassName: String;
    procedure SetClassName(const AClassName: String);
    function  GetAncestorClassName: String;
    function  GetVCLClass: TClass;
    function  GetObjectType: TrwDsgnObjectType;
    function  IsInheritsFrom(const AClassName: String): Boolean;
    procedure GetPicture(const ABitmap: TBitmap);
    procedure SetPictureData(const AStream: TStream);
    procedure SetComponentData(const AComponent: IrmDesignObject);
    function  GetComponentData: IevDualStream;
    function  GetDsgnPaletteSettings: TrwDsgnPaletteSettings;
    procedure SetDsgnPaletteSettings(const AValue: TrwDsgnPaletteSettings);
    function  CreateComponent(const AExistingComponent: IrmDesignObject = nil; const AInherit: Boolean = False; const ADontRunFixUp: Boolean = False): IrmDesignObject;
  end;


  IrmComponents = Interface(IrmLibCollection)
  ['{B81A89D2-9FB8-4767-B087-8789CA955ED3}']
    function  GetComponentByClassName(const AClassName: String): IrmComponent;
    function  GetComponentByIndex(const AIndex: Integer): IrmComponent;
    function  ClassInheritsFromClass(const AClassName: string; const AAncClassName: string): Boolean;
  end;


  IrmVarFunctHolder = Interface(IrmInterfacedObject)
  ['{450C5A80-6749-45A6-91B2-347C25EFE4D1}']
    function  GetVariables: IrmVariables;
    function  GetLocalFunctions:  IrmFunctions;
    function  GetLocalComponents: IrmComponents;
  end;



  TrmASCIIResultType = (rmARTFixedLength, rmARTDelimited, rmARTXls);


  IrmASCIIItem = interface(IrmDesignObject)
  ['{F3BE6247-2F63-4289-A3C3-A345CC21B93E}']
    function GetASCIIForm: IrmDesignObject;
  end;

  IrmASCIINode = interface(IrmASCIIItem)
  ['{7254CA62-020E-4DBA-9EDB-1324088D9949}']
  end;


  IrmASCIIField = interface(IrmASCIIItem)
  ['{1C312D52-1093-4593-AA3E-F9DACF3F8FB6}']
  end;
  

  IrmASCIIRecord = interface(IrmASCIINode)
  ['{7254CA62-020E-4DBA-9EDB-1324088D9949}']
  end;




  TrmXMLTypeName = string;
  TrmXMLQName = string;
  TrmXMLData = string;

  TrmXMLItemType = (rmXITUnknown, rmXITSimpleType, rmXITAttribute, rmXITSimpleContent, rmXITComplexContent);

  TrmXMLFacetType = (rmXFTenumeration, rmXFTfractionDigits, rmXFTlength, rmXFTmaxExclusive, rmXFTmaxInclusive, rmXFTmaxLength, rmXFTminExclusive,
                     rmXFTminInclusive, rmXFTminLength, rmXFTpattern, rmXFTtotalDigits, rmXFTwhiteSpace);


  IrmXMLItem = interface(IrmDesignObject)
  ['{F2F399A2-EB6A-4453-A9C7-6EE07BBD5A94}']
    function GetXMLForm: IrmDesignObject;
  end;


  IrmXMLSchema = interface(IrwCollectionItem)
  ['{103FB882-7893-4D88-AF54-DF4CFA43EEF2}']
    function  GetName: string;
    procedure SetName(const AValue: string);
    function  GetDescription: string;
    procedure SetDescription(const AValue: string);
  end;


  IrmXMLSchemas = interface(IrwCollection)
  ['{CC23A1D8-541B-4C04-9274-245D0BE7F413}']
    function  GetSchemaByName(const ASchemaName: String): IrmXMLSchema;
  end;


  IrmXMLFormObject = interface(IrmDesignObject)
  ['{E6883C05-6957-4319-8514-F1EF2F0CFE3A}']
    procedure LoadXSDSchema(const AFileName: TFileName);
    procedure SaveXSDSchema(const ASchemaName: String; const AFileName: TFileName);
    procedure UpdateXSDSchema(const ASchemaName: String; const AFileName: TFileName);
    procedure AddXSDSchema(const AFileName: TFileName);
    function  GetSchemas: IrmXMLSchemas;
  end;



  TrmDesignerSettingsRec = record
    AdvancedMode: Boolean;
    ShowPaperGrid: Boolean;
    ShowTableGrid: Boolean;
  end;


  IrmDesigner = Interface
  ['{6E748426-646A-4BB6-A6FC-9B485D4B5AC6}']
    function  DesigningObject: IrmDesignObject;
    function  InputFormDesigninig: Boolean;
    function  PrintFormDesigninig: Boolean;
    function  ASCIIFormDesigninig: Boolean;
    function  LibComponentDesigninig: Boolean;
    function  DispatchDesignMsg(Sender: IrmDesignObject; var Message: TMessage): Boolean;

    procedure Notify(const AEventType: TrmDesignEventType; const AParam: Variant);
    procedure ObjectPropertyChanged(const AObject: IrmDesignObject; const APropertyName: String);

    function  GetDesignParent(const AName: String): TWinControl;
    procedure GetObjectPicture(const AClassName: String; AImage: TBitmap);
    function  GetZoomFactor: Extended;
    function  GetObjectCanvasDPI: Integer;
    function  GetSelectedObjects: TrmListOfObjects;
    function  GetDesignerSettings: TrmDesignerSettingsRec;
    function  IsObjectSelected(AObject: IrmDesignObject): Boolean;
    procedure SelectObject(AObject: IrmDesignObject);
    function  UnSelectObject(AObject: IrmDesignObject): Boolean;
  end;


implementation

end.
