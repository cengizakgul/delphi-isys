unit rwRWASecurityFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, rwPreviewUtils;

type
  TrwRWASecurity = class(TForm)
    rgPasswd: TGroupBox;
    evLabel1: TLabel;
    evLabel2: TLabel;
    chbCompression: TCheckBox;
    btnOK: TButton;
    btnCancel: TButton;
    rgLevel: TRadioGroup;
    edtFullAccess: TEdit;
    edtPreviewAccess: TEdit;
    procedure rgLevelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
  public
  end;

  function RWASecurity(var APreviewPasswd, APrintPasswd: String; var ACompress: Boolean): Boolean;

implementation

{$R *.dfm}


function RWASecurity(var APreviewPasswd, APrintPasswd: String; var ACompress: Boolean): Boolean;
var
  Frm: TrwRWASecurity;
begin
  Result := False;
  Frm := TrwRWASecurity.Create(nil);
  with Frm do
    try
      if (APrintPasswd = '') and (APreviewPasswd = '') then
        rgLevel.ItemIndex := 0

      else if APrintPasswd = APreviewPasswd then
      begin
        rgLevel.ItemIndex := 1;
        edtFullAccess.Text := '********************';
      end

      else
      begin
        rgLevel.ItemIndex := 2;
        edtFullAccess.Text := '********************';
        edtPreviewAccess.Text := '********************';
      end;

      rgLevel.OnClick(nil);

      chbCompression.Checked := ACompress;

      edtPreviewAccess.Modified := False;
      edtFullAccess.Modified := False;

      if ShowModal = mrOK then
      begin
        if (ACompress <> chbCompression.Checked) or edtPreviewAccess.Modified or
           edtFullAccess.Modified then
          Result := True;

        if rgLevel.ItemIndex = 0 then
        begin
          APreviewPasswd := '';
          APrintPasswd := '';
        end
        else
        begin
          if rgLevel.ItemIndex = 1 then
          begin
            if edtFullAccess.Modified then
              APrintPasswd := edtFullAccess.Text;
            APreviewPasswd := APrintPasswd;
          end

          else if rgLevel.ItemIndex = 2 then
          begin
            if edtPreviewAccess.Modified then
              APreviewPasswd := edtPreviewAccess.Text;
            if edtFullAccess.Modified then
              APrintPasswd := edtFullAccess.Text;
          end;
        end;

        ACompress := chbCompression.Checked;
      end;

    finally
      Free;
    end;
end;



procedure TrwRWASecurity.rgLevelClick(Sender: TObject);
begin
  case rgLevel.ItemIndex of
    0: begin
         edtPreviewAccess.Enabled := False;
         edtPreviewAccess.Color := clInactiveBorder;
         edtFullAccess.Enabled := False;
         edtFullAccess.Color := clInactiveBorder;
         edtFullAccess.Text := '';
         edtFullAccess.Modified := True;
         edtPreviewAccess.Text := '';
       end;

    1: begin
         edtPreviewAccess.Enabled := False;
         edtFullAccess.Enabled := True;
         edtPreviewAccess.Color := clInactiveBorder;
         edtFullAccess.Color := clWindow;
         edtPreviewAccess.Text := '';
       end;

    2: begin
         edtPreviewAccess.Enabled := True;
         edtFullAccess.Enabled := True;
         edtPreviewAccess.Color := clWindow;
         edtFullAccess.Color := clWindow;
       end;
  end;
end;

procedure TrwRWASecurity.FormCloseQuery(Sender: TObject; var CanClose: Boolean);

  function CheckPass(const APrompt, APasswd: String): Boolean;
  begin
    Result :=  PrvPasswordDialog('Confirm Password', APrompt) = APasswd;
    if not Result then
      PrvMessage(APrompt + ' is not confirmed', mtError, [mbOK]);
  end;

begin
  if ModalResult = mrOK then
  begin
    if (rgLevel.ItemIndex in [1, 2]) and (edtFullAccess.Text = '') then
    begin
      CanClose := False;
      PrvMessage('Full Access Password is empty', mtError, [mbOK]);
      Exit;
    end

    else if (rgLevel.ItemIndex = 2) and (edtPreviewAccess.Text = '') then
    begin
      CanClose := False;
      PrvMessage('Preview Password is empty', mtError, [mbOK]);
      Exit;
    end;


    if edtFullAccess.Enabled and edtFullAccess.Modified then
    begin
      CanClose := CheckPass('Full Access Password', edtFullAccess.Text);
      if not CanClose then
        Exit;
    end;

    if edtPreviewAccess.Enabled and edtPreviewAccess.Modified then
    begin
      CanClose := CheckPass('Preview Password', edtPreviewAccess.Text);
      if not CanClose then
        Exit;
    end;
  end;
end;

end.
