unit rwRTTI;

interface

uses SysUtils, Classes, ComObj,
     rmTypes, rwCustomDataDictionary;

type
   IrwRTTI = interface
   ['{E3924600-72EB-424E-9CBC-A37062908D21}']
     procedure RegisterClass(const ALibComponent: IrmComponent); overload;
     procedure RegisterClass(const AVCLClass: TClass); overload;
     procedure UnregisterClass(const AClassName: String); overload;
     procedure UnregisterClass(const ALibComponent: IrmComponent); overload;
     function  FindClass(const AClassName: String): IrwClass;
     function  FindClassByGUID(const AGUID: String): IrwClass;
     function  GetItemCount: Integer;
     function  GetItemByIndex(const Index: Integer): IrwClass;
   end;


   TrwRTTIItem = class(TInterfacedCollectionItem, IrwClass)
   private
     FLibComponent: IrmComponent;
     FVCLClass: TClass;
   protected
     function GetClassName: String;
     function GetClassGUID: String;
     function GetAncestor: IrwClass;
     function DescendantOf(const AClassName: String): Boolean; overload;
     function DescendantOf(const AClass: IrwClass): Boolean; overload;
     function GetVCLClass: TClass;
     function GetUserClass: IrmComponent;
     function CreateObjectInstance(const AInherit: Boolean = True; const IgnoreUserProperties: Boolean = False): TObject;
     function IsUserClass: Boolean;
     function GetClassDisplayName: String;
     function GetClassDescription: String;
   end;


   TrwRTTI = class(TInterfacedCollection, IrwRTTI)
   private
     function  FindClassInt(const AClassName: String): TrwRTTIItem;
     function  FindClassByGUIDInt(const AClassGUID: String): TrwRTTIItem;
     function  FindClassByInterface(const ALibComponent: IrmComponent): TrwRTTIItem;
     function  GetItem(const Index: Integer): TrwRTTIItem;
     procedure RegisterStaticClasses;

   protected
     procedure RegisterClass(const ALibComponent: IrmComponent); overload;
     procedure RegisterClass(const AVCLClass: TClass); overload;
     procedure UnregisterClass(const AClassName: String); overload;
     procedure UnregisterClass(const ALibComponent: IrmComponent); overload;
     function  FindClass(const AClassName: String): IrwClass;
     function  FindClassByGUID(const AGUID: String): IrwClass;
     function  GetItemCount: Integer;
     function  GetItemByIndex(const Index: Integer): IrwClass;

   public
     constructor Create;
   end;

   procedure InitializeRTTIInfo;
   function  rwRTTIInfo: IrwRTTI;
   procedure DestroyRTTIInfo;
   function  CreateRWGUID: String;


implementation

uses rwUtils, TypInfo;

threadvar
  FRTTI: TrwRTTI;


function CreateRWGUID: String;
begin
  Result := CreateClassID;
end;


procedure InitializeRTTIInfo;
begin
  FRTTI := TrwRTTI.Create;
end;


function  rwRTTIInfo: IrwRTTI;
begin
  if Assigned(FRTTI) then
    SystemLibComponents;

  Result := FRTTI;
end;


procedure DestroyRTTIInfo;
begin
  FreeAndNil(FRTTI)
end;


{ TrwRTTIItem }

function TrwRTTIItem.CreateObjectInstance(const AInherit: Boolean = True; const IgnoreUserProperties: Boolean = False): TObject;
begin
  if IsUserClass then
    Result := FLibComponent.CreateComponent(nil, AInherit, IgnoreUserProperties).RealObject
  else
  begin
    if FVCLClass.InheritsFrom(TComponent) then
      Result := TComponentClass(FVCLClass).Create(nil)
    else if FVCLClass.InheritsFrom(TCollectionItem) then
      Result := TCollectionItemClass(FVCLClass).Create(nil)
    else
      Result := FVCLClass.Create;
  end;
end;

function TrwRTTIItem.DescendantOf(const AClassName: String): Boolean;
begin
  if IsUserClass then
    Result := FLibComponent.IsInheritsFrom(AClassName)
  else
    Result := FVCLClass.InheritsFrom(GetClass(AClassName));
end;

function TrwRTTIItem.DescendantOf(const AClass: IrwClass): Boolean;
begin
  Result := Assigned(AClass) and ((AClass = (Self as IrwClass)) or DescendantOf(AClass.GetClassName));
end;

function TrwRTTIItem.GetAncestor: IrwClass;
var
  Cl: String;
begin
  if IsUserClass then
    Cl := FLibComponent.GetAncestorClassName
  else
    Cl := FVCLClass.ClassParent.ClassName;

  Result := TrwRTTI(Collection).FindClass(Cl)
end;

function TrwRTTIItem.GetClassDescription: String;
var
  i: Integer;
begin
  if IsUserClass then
    Result := FLibComponent.GetDescription
  else
    Result := '';

  if Result = '' then
  begin
    i := RWClassList.IndexOfObject(Pointer(GetVCLClass));
    if i <> -1 then
      Result := RWClassList[i]; //need to add real description
  end;
end;

function TrwRTTIItem.GetClassDisplayName: String;
var
  i: integer;
begin
  if IsUserClass then
    Result := FLibComponent.GetDisplayName

  else
  begin
    i := RWClassList.IndexOfObject(Pointer(FVCLClass));
    if i <> -1 then
      Result := RWClassList[i]
    else
      Result := '';
  end;
end;

function TrwRTTIItem.GetClassGUID: String;
begin
  if IsUserClass then
    Result := FLibComponent.GetItemID
  else
    Result := FVCLClass.ClassName;
end;

function TrwRTTIItem.GetClassName: String;
begin
  if IsUserClass then
    Result := FLibComponent.GetClassName
  else
    Result := FVCLClass.ClassName;
end;


function TrwRTTIItem.GetUserClass: IrmComponent;
begin
  Result := FLibComponent;
end;

function TrwRTTIItem.GetVCLClass: TClass;
begin
  if IsUserClass then
    Result := FLibComponent.GetVCLClass
  else
    Result := FVCLClass;
end;


{ TrwRTTII }

constructor TrwRTTI.Create;
begin
  inherited Create(TrwRTTIItem);
  RegisterStaticClasses;
end;

function TrwRTTI.FindClass(const AClassName: String): IrwClass;
begin
  Result := FindClassInt(AClassName);
end;

function TrwRTTI.FindClassByGUID(const AGUID: String): IrwClass;
begin
  Result := FindClassByGUIDInt(AGUID);
end;

function TrwRTTI.FindClassByGUIDInt(const AClassGUID: String): TrwRTTIItem;
var
  i: Integer;
begin
  Result := nil;

  if AClassGUID = '' then
    Exit;

  for i := 0 to Count - 1 do
    if AnsiSameStr(GetItem(i).GetClassGUID, AClassGUID) then
    begin
      Result := GetItem(i);
      break;
    end;
end;

function TrwRTTI.FindClassByInterface(const ALibComponent: IrmComponent): TrwRTTIItem;
var
  i: Integer;
begin
  Result := nil;

  if not Assigned(ALibComponent) then
    Exit;

  for i := 0 to Count - 1 do
    if GetItem(i).FLibComponent = ALibComponent then
    begin
      Result := GetItem(i);
      break;
    end;
end;

function TrwRTTI.FindClassInt(const AClassName: String): TrwRTTIItem;
var
  i: Integer;
  Cl: String;
begin
  Result := nil;

  if AClassName = '' then
    Exit;

  Cl := UpperCase(AClassName);
  
  for i := 0 to Count - 1 do
    if AnsiSameStr(UpperCase(GetItem(i).GetClassName), Cl) then
    begin
      Result := GetItem(i);
      break;
    end;
end;

function TrwRTTI.GetItem(const Index: Integer): TrwRTTIItem;
begin
  Result := TrwRTTIItem(Items[Index]);
end;

procedure TrwRTTI.RegisterClass(const ALibComponent: IrmComponent);
var
  NewItem: TrwRTTIItem;
  Cl: String;
begin
  Cl := ALibComponent.GetClassName;
  NewItem := FindClassInt(Cl);

  Assert(not Assigned(NewItem));

  NewItem := TrwRTTIItem(Add);
  NewItem.FLibComponent := ALibComponent;
end;

function TrwRTTI.GetItemByIndex(const Index: Integer): IrwClass;
begin
  Result := GetItem(Index);
end;

function TrwRTTI.GetItemCount: Integer;
begin
  Result := Count;
end;

procedure TrwRTTI.RegisterClass(const AVCLClass: TClass);
var
  NewItem: TrwRTTIItem;
begin
  NewItem := FindClassInt(AVCLClass.ClassName);

  Assert(not Assigned(NewItem));

  NewItem := TrwRTTIItem(Add);
  NewItem.FVCLClass := AVCLClass;
end;

procedure TrwRTTI.RegisterStaticClasses;
var
  i: Integer;
begin
  for i := 0 to RWClassList.Count - 1 do
    RegisterClass(TClass(RWClassList.Objects[i]));
end;

procedure TrwRTTI.UnregisterClass(const AClassName: String);
var
  Item: TrwRTTIItem;
begin
  Item := FindClassInt(AClassName);
  FreeAndNil(Item);
end;

function TrwRTTIItem.IsUserClass: Boolean;
begin
  Result := Assigned(FLibComponent);
end;

procedure TrwRTTI.UnregisterClass(const ALibComponent: IrmComponent);
var
  Item: TrwRTTIItem;
begin
  Item := FindClassByInterface(ALibComponent);
  FreeAndNil(Item);
end;

end.
