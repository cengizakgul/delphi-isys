unit rwEngine;

interface

uses Classes, SysUtils, ShellAPI, Windows, sbAPI, sbSQL, Forms, Messages, Controls, Variants,
     rwEngineTypes, rwReport, rmReport, ISBasicClasses, rwBasicClasses,
     rwMessages, evStreamUtils, rwGraphics, rwTypes, rmTypes, rwVirtualMachine, ISDataAccessComponents,
     ISErrorUtils;

type
  ErwAdpterException = class(ErwException);

  IrwDesignerForm = interface
  ['{D719C195-5B1A-4D99-8F2C-A7CFD9AC0749}']
    function  IsClosed: Boolean;
    function  GetData: IEvDualStream;
  end;


  IrwEngineExt = interface(IrwEngine)
  ['{322730C5-7D50-4175-914E-80220F4126E0}']
    function  AppAdapter: IrwAppAdapter;
    procedure GetReportParameters(const ReportStructure: TStream; const PParamList: PTrwReportParamList);
    function  GetVM: TrwVirtualMachine;
    function  CreateReportObject(const ReportStructure: TStream): TrwComponent;
    function  ShowQueryBuilder(const AQuery: TStream; const NiceNames: Boolean = True): Boolean; overload;
    function  ShowQueryBuilder(const AQueryData: IEvDualStream; const NiceNames: Boolean = True): IrwDesignerForm; overload;
    function  RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;
  end;


  TrwEngine = class(TInterfacedObject, IrwEngine, IrwEngineExt)
  private
    FAppAdapter: IrwAppAdapter;
    FRunningReport: TObject;

    procedure CompileIfNeeds(const AReport: TrwReport); overload;
    procedure CompileIfNeeds(const AReport: TrmReport); overload;
    procedure Compile(const AComponent: TrwComponent);
    function  EngineVersion(const ReportStructure: TStream): Integer;
  public
    constructor Create;
    destructor  Destroy; override;

    function  DataDictionary: IrwDataDictionary;
    function  LibraryComponents: IrwLibraryComponents;
    function  LibraryFunctions: IrwLibraryFunctions;
    function  SQLConstants: IrwSQLConstants;

    function  ShowInputForm(const pReportInfo: PTrwReportRec; const pShowInfo: PTrwShowInputFormRec): IrwInputForm;
    function  RunReport(const pReportInfo: PTrwReportRec; const pRunInfo: PTrwRunReportRec): IrwResult;

    procedure CompileReport(const pReportInfo: PTrwReportRec);
    procedure PrepareReport(const pReportInfo: PTrwReportRec);
    function  AnalyzeReport(const pReportInfo: PTrwReportRec): TrwReportContentRec;
    function  DBCache: IrwDBCache;

    function  OpenSBTable(const ATableName: String; const AReadOnly: Boolean): IrwSBTable;
    function  OpenSBSQL(const ASQL: String; const Params: array of TrwParamRec): IrwSBTable;
    procedure ExecSBSQL(const ASQL: String; const Params: array of TrwParamRec);
    procedure CopySBTable(const ATable: IrwSBTable; const ANewTableName: String);

    procedure OpenQuery(const ASQL: String; const Params: array of TrwParamRec; const ResultDataSet: TObject);

    function  MakeReportResult(const RWResults: PTrwRWAInfoList; const AType: TrwResultFormatType): IrwResult;

    procedure SetReportResultSecurity(const pPreviewInfo: PTrwRWAInfoRec; const pNewSecurityInfo: PTrwRWASecurityRec);

    function  RWAtoPDF(const pPreviewInfo: PTrwRWAInfoRec; const pPDFInfoRec: PTrwPDFInfoRec): IEvDualStream;

    function  ReportExecutionHalted: Boolean;
    procedure HaltReportExecution;

    procedure Preview(const pPreviewInfo: PTrwPreviewInfoRec);
    procedure Print(const pPreviewInfo: PTrwRWAInfoRec);
    procedure PrintFromThread(const pReportList: PTrwRWAInfoList);

    function  ParamStreamToParamList(const AParamStream: TStream): TrwReportParamList;
    procedure ParamListToParamStream(const AParamList: TrwReportParamList; const AParamStream: TStream);

    function  AppAdapter: IrwAppAdapter;
    procedure GetReportParameters(const ReportStructure: TStream; const PParamList: PTrwReportParamList); overload;
    procedure GetReportParameters(const Report: TObject; const PParamList: PTrwReportParamList); overload;
    function  CreateReportObject(const ReportStructure: TStream): TrwComponent;
    function  ShowQueryBuilder(const AQuery: TStream; const NiceNames: Boolean = True): Boolean; overload;
    function  ShowQueryBuilder(const AQueryData: IEvDualStream; const NiceNames: Boolean = True): IrwDesignerForm; overload;
    function  RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;

    function  GetVM: TrwVirtualMachine;

    procedure RaiseException(const AErrorMessage: String);
  end;


  TrwResultHolder = class(TInterfacedObject, IrwResult)
  private
    FFileName: String;
    FReadOnly: Boolean;
    FData: TStream;
    FDestroyStream: Boolean;
    FIStream: IEvDualStream;
    FPResultPageInfoList: PTrwResultPageInfoList;
    FType: TrwResultFormatType;
    FMiscInfo: TrwResultMiscInfo;
  public
    constructor Create(const AFileName: string; const AFormat: TrwResultFormatType = rwRTUnknown; AReadOnly: Boolean = True); overload;
    constructor Create(const AIStream: IEvDualStream; AType: TrwResultFormatType); overload;
    destructor  Destroy; override;
    procedure   ReleaseFileHandle;
    procedure   SetMiscInfo(const AMiscInfo: TrwResultMiscInfo);

    function Data: TStream;
    function FileName: String;
    function ResultType: TrwResultFormatType;
    function PageInfo: PTrwResultPageInfoList;
    function MiscInfo: TrwResultMiscInfo;
  end;


  TrwInputFormHolder = class(TInterfacedObject, IrwInputForm)
  private
    FOwner: TrwEngine;
    FReport: TrwComponent;
    FModalDialog: Boolean;
  public
    constructor Create(const AOwner: TrwEngine; const pReportInfo: PTrwReportRec; const pShowInfo: PTrwShowInputFormRec);
    destructor  Destroy; override;
    function    Close(const Params: PTrwReportParamList): Boolean;
    function    IsModalDialog: Boolean;
    function    VCLObject: TObject;
  end;


  procedure InitializeRWEngine(const AppAdapter: IrwAppAdapter); stdcall;
  procedure DestroyRWEngine; stdcall;
  function  ReportWriterEngine: IrwEngine; stdcall;

  function  RWEngineExt: IrwEngineExt;

var CurrentInputForm: TForm = nil;  // temporary for debugging only  

implementation

uses rwUtils, rwLogQuery, rwOneRepPreviewFrm, rwBasicPrint, rwFormPreviewFrm,
     rwPrintPage, StrUtils, TypInfo, rwDebugInfo, rwRTTI, rwParser, rwBasicUtils,
     rwQueryBuilderFrm, rwQBInfo;

const
  cPrintUtil = 'rwPreview.exe';

threadvar
  tvEngine: IrwEngineExt;

procedure InitializeRWEngine(const AppAdapter: IrwAppAdapter);
var
  EngineObject: TrwEngine;
begin
  EngineObject := TrwEngine.Create;
  EngineObject.FAppAdapter := AppAdapter;
  tvEngine := EngineObject;
  AppAdapter.Activate(tvEngine);
  AppAdapter.SetAppCustomData(AppAdapterAppCustomData);
  InitializeRTTIInfo;
  InitCacheInfo;
  CreateRwVM(nil);
end;

procedure DestroyRWEngine;
begin
  if Assigned(tvEngine) then
    try
      if tvEngine.AppAdapter <> nil then
        tvEngine.AppAdapter.Deactivate;

      try
        DestroyRwVM;
      finally
        DestroyRTTIInfo;
        FreeCacheInfo;
      end;
    finally
      tvEngine := nil;
    end;
end;


function ReportWriterEngine: IrwEngine;
begin
  Result := tvEngine;
end;

function  RWEngineExt: IrwEngineExt;
begin
  Result := tvEngine;
  if not Assigned(Result) then
    raise ErwException.Create('AppAdapter is not initialized');
end;


{ TrwEngine }

constructor TrwEngine.Create;
begin
  InitThreadVars_rwShared;
end;

destructor TrwEngine.Destroy;
begin
  try
    FAppAdapter := nil;
  except
    Pointer(FAppAdapter) := nil;
  end;
    
  inherited;
end;


function TrwEngine.AppAdapter: IrwAppAdapter;
begin
  Result := FAppAdapter;
end;

procedure TrwEngine.CompileReport(const pReportInfo: PTrwReportRec);

  procedure CompileRWReport;
  var
    Report: TrwReport;
  begin
    Report := TrwReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);
      Compile(Report);
      pReportInfo.ReportClassName := Report._RwClassName;
      pReportInfo.ReportAncestorClassName := Report._LibComponent;

      pReportInfo.Data.Size := 0;
      Report.SaveToStream(pReportInfo.Data);
    finally
      FreeAndNil(Report);
    end;
  end;

  procedure CompileRMReport;
  var
    Report: TrmReport;
  begin
    Report := TrmReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);

      Compile(Report);
      pReportInfo.ReportClassName := Report._RwClassName;
      pReportInfo.ReportAncestorClassName := Report._LibComponent;

      pReportInfo.Data.Size := 0;
      Report.SaveToStream(pReportInfo.Data);
    finally
      FreeAndNil(Report);
    end;
  end;

begin
  if EngineVersion(pReportInfo.Data) = 0 then
    CompileRWReport
  else
    CompileRMReport;
end;

procedure TrwEngine.PrepareReport(const pReportInfo: PTrwReportRec);

  procedure PrepareRWReport;
  var
    Report: TrwReport;
  begin
    Report := TrwReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);

      CompileIfNeeds(Report);
      Report.Prepare;

      pReportInfo.Data.Size := 0;
      Report.SaveToStream(pReportInfo.Data);
    finally
      FreeAndNil(Report);
    end;
  end;

  procedure PrepareRMReport;
  var
    Report: TrmReport;
  begin
    Report := TrmReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);

      CompileIfNeeds(Report);
      Report.Prepare;

      pReportInfo.Data.Size := 0;
      Report.SaveToStream(pReportInfo.Data);
    finally
      FreeAndNil(Report);
    end;
  end;

begin
  if EngineVersion(pReportInfo.Data) = 0 then
    PrepareRWReport
  else
    PrepareRMReport;
end;

function TrwEngine.RunReport(const pReportInfo: PTrwReportRec; const pRunInfo: PTrwRunReportRec): IrwResult;
var
  Err: String;

  procedure RunRWReport;
  var
    Report: TrwReport;
    ResFileName: String;
    flAborted: Boolean;
    ResType: TrwResultFormatType;
  begin
    Report := TrwReport.Create(nil);
    try
      FRunningReport := Report;
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);

      if not pRunInfo.DoNotPrepare then
        Report.Prepare;

      CompileIfNeeds(Report);

      if Assigned(pRunInfo^.Params) then
        Report.SetReportParams(pRunInfo^.Params);

      ResFileName := GetUniqueFileName(GetThreadTmpDir);

      Report.Duplexing := pRunInfo^.Duplexing;

      flAborted := False;
      try
        if not Report.Print(ResFileName, pRunInfo^.ShowInputForm) then
          Exit;
      except
        on E: ErwVMAbort do
        begin
          flAborted := True;
          pRunInfo.ReturnValue := E.ReturnValue;
        end

        else
          raise;
      end;


      if pRunInfo^.ReturnParamsBack and Assigned(pRunInfo^.Params) then
        Report.GetReportParams(pRunInfo^.Params);

    finally
      FRunningReport := nil;
      FreeAndNil(Report);
    end;

    if not flAborted then
    begin
      if FileExists(ChangeFileExt(ResFileName, '.txt')) then
      begin
        ResFileName := ChangeFileExt(ResFileName, '.txt');
        ResType := rwRTASCII;
      end
      else if FileExists(ChangeFileExt(ResFileName, '.xls')) then
      begin
        ResFileName := ChangeFileExt(ResFileName, '.xls');
        ResType := rwRTXLS;
      end
      else
        ResType := rwRTRWA;

      Result := TrwResultHolder.Create(ResFileName, ResType);
    end;
  end;

  procedure RunRMReport;
  var
    Report: TrmReport;
  begin
    Report := TrmReport.Create(nil);
    try
      GetVM.DisableDebCallbacks := True;
      try
        FRunningReport := Report;
        pReportInfo^.Data.Position := 0;
        Report.LoadFromStream(pReportInfo^.Data);

        if not pRunInfo.DoNotPrepare then
          Report.Prepare;

        CompileIfNeeds(Report);

        if Assigned(pRunInfo^.Params) then
          Report.SetReportParams(pRunInfo^.Params);
      finally
        GetVM.DisableDebCallbacks := False;      
      end;

      try
        Result := Report.Execute(pRunInfo^.ShowInputForm);
      except
        on E: ErwVMAbort do
          pRunInfo.ReturnValue := E.ReturnValue
        else
          raise;
      end;

      if pRunInfo^.ReturnParamsBack and Assigned(pRunInfo^.Params) then
        Report.GetReportParams(pRunInfo^.Params);

    finally
      FRunningReport := nil;
      FreeAndNil(Report);
    end;
  end;

var
  sOldThreadName: String;

begin
  sOldThreadName := GetThreadName(GetCurrentThreadId);
  sOldThreadName := Trim(GetNextStrValue(sOldThreadName, '(')); // stupid madExcept adds thread ID to the name
  try
    SetThreadName(GetCurrentThreadId, sOldThreadName + ' (' + pReportInfo.ReportName + ')');
    try
      Result := nil;
      pRunInfo.ReturnValue := Unassigned;

      if EngineVersion(pReportInfo.Data) = 0 then
        RunRWReport
      else
        RunRMReport;
    except
      on E: Exception do
      begin
        Err := E.Message;
        AppAdapter.HandleException(Err);
        E.Message := Err;
        raise;
      end;
    end;
  finally
    SetThreadName(GetCurrentThreadId, sOldThreadName);
  end;
end;


function TrwEngine.ShowInputForm(const pReportInfo: PTrwReportRec; const pShowInfo: PTrwShowInputFormRec): IrwInputForm;
var
  Res: TrwInputFormHolder;
begin
  Res := TrwInputFormHolder.Create(Self, pReportInfo, pShowInfo);
  try
    if Res.FReport is TrwReport then
    begin
      if not TrwReport(Res.FReport).ShowInputForm(pShowInfo^.Parent, pShowInfo^.Mode = rwIFRunTime) then
        FreeAndNil(Res);
    end

    else
    begin
      if TrmReport(Res.FReport).CanShowInputForm(pShowInfo^.Mode = rwIFRunTime) then
      begin
        if not TrmReport(Res.FReport).ShowInputForm(pShowInfo^.Parent, pShowInfo^.Mode = rwIFRunTime) then
          FreeAndNil(Res);
      end
      else
        FreeAndNil(Res);
    end;

  except
    FreeAndNil(Res);
    raise;
  end;

  Result := Res
end;


procedure TrwEngine.Preview(const pPreviewInfo: PTrwPreviewInfoRec);
var
  Frm: TrwOneRepPreview;
  F: TrwFormPreview;
  i, n: Integer;
begin
  if Assigned(pPreviewInfo^.Owner) and (pPreviewInfo^.Parent <> 0) then
  begin
    Frm := nil;
    n := 1;
    if pPreviewInfo^.SingleInstance then
    begin
      for i := 0 to pPreviewInfo^.Owner.ComponentCount - 1 do
        if pPreviewInfo^.Owner.Components[i] is TrwOneRepPreview then
        begin
          Frm := TrwOneRepPreview(pPreviewInfo^.Owner.Components[i]);
          break;
        end;
    end
    else
    begin
      for i := 0 to pPreviewInfo^.Owner.ComponentCount - 1 do
        if pPreviewInfo^.Owner.Components[i] is TrwOneRepPreview then
          Inc(n);
   end;

    if not Assigned(Frm) then
    begin
      Frm := TrwOneRepPreview.Create(pPreviewInfo^.Owner);
      Frm.Name := Frm.Name + IntToStr(n);
    end;

    Frm.PreviewReport(pPreviewInfo);
    pPreviewInfo^.PreviewFrame := Frm;
  end

  else
  begin
    F := TrwFormPreview.Create(pPreviewInfo^.Owner);
    try
//      F.Position := poOwnerFormCenter;
      F.Preview.PreviewReport(pPreviewInfo);
      F.ShowModal;
    finally
      F.Free;
    end;
  end;
end;

procedure TrwEngine.Print(const pPreviewInfo: PTrwRWAInfoRec);
begin
  with pPreviewInfo^ do
  begin
    if Assigned(Data1) then
      PrintReport(Data1.Data, nil, @pPreviewInfo^.PrintJobInfo, nil)
    else if Assigned(Data2) then
      PrintReport(Data2, nil, @pPreviewInfo^.PrintJobInfo, nil)
    else if Assigned(Data3) then
      PrintReport(Data3, nil, @pPreviewInfo^.PrintJobInfo, nil)
    else
      PrintReport(Data4, @pPreviewInfo^.PrintJobInfo);
  end;
end;

procedure TrwEngine.PrintFromThread(const pReportList: PTrwRWAInfoList);
type
  TPrintProcessInfo = record
    ReportFile: String;
    ParamFile:  String;
    ErrorFile:  String;
    ErrorText:  String;
    pRWAInfoRec: PTrwRWAInfoRec;
    Process: TProcessInformation;
  end;

  PTPrintProcessInfo = ^TPrintProcessInfo;

var
  repFile, h: string;
  PrintProcesses: array of TPrintProcessInfo;
  FS: TEvFileStream;
  ST: TStream;
  i: Integer;

{  function LayersToStr(const ALayers: TrwPrintableLayers): String;
  var
    i: TrwLayerNumber;
  begin
    Result := '';
    for i := Low(TrwLayerNumber) to High(TrwLayerNumber) do
      if i in ALayers then
      begin
        if Result <> '' then
          Result := Result + ',';
        Result := Result + IntToStr(i);
      end;
  end;
}
  procedure PrintProcess(const pPrintProcessInfo: PTPrintProcessInfo);
  var
    StartupInfo1: TStartupInfo;
    SA: TSecurityAttributes;
    PT: TrwThreadPrintInfoRec;
    h:  String;
  begin
    PT.Password := pPrintProcessInfo^.pRWAInfoRec^.PrintPswd;
    PT.ErrorFile := pPrintProcessInfo^.ErrorFile;
    PT.PrintJobInfo := pPrintProcessInfo^.pRWAInfoRec^.PrintJobInfo;
    PT.FileName := pPrintProcessInfo^.ReportFile;
    MakeParamFile(pPrintProcessInfo^.ParamFile, @PT);

    h := cPrintUtil + ' "' + pPrintProcessInfo^.ReportFile + '" "' + pPrintProcessInfo^.ParamFile + '"';

    FillChar(SA, SizeOf(TSecurityAttributes), 0);
    SA.nLength := SizeOf(SA);
    SA.lpSecurityDescriptor := nil;
    SA.bInheritHandle := TRUE;

    FillChar(StartupInfo1, SizeOf(TStartupInfo), 0);
    StartupInfo1.cb := SizeOf(TStartupInfo);
    StartupInfo1.lpDesktop := nil;
    StartupInfo1.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo1.wshowWindow := SW_HIDE;

    if not CreateProcess(nil, PChar(h), nil, @SA, True, NORMAL_PRIORITY_CLASS ,
                         nil, nil, StartupInfo1, pPrintProcessInfo^.Process) then
      raise ErwException.Create('Unexpected print error: ' + SysErrorMessage(GetLastError));
  end;


  procedure WaitProcess(const pPrintProcessInfo: PTPrintProcessInfo);
  var
    FS: TEvFileStream;
  begin
    if WaitForSingleObject(pPrintProcessInfo^.Process.hProcess, INFINITE) = WAIT_FAILED then
      pPrintProcessInfo^.ErrorText := SysErrorMessage(GetLastError)

    else
      if FileExists(pPrintProcessInfo^.ErrorFile) then
      begin
        FS := TEvFileStream.Create(pPrintProcessInfo^.ErrorFile, fmOpenRead);
        try
          SetLength(pPrintProcessInfo^.ErrorText, FS.Size);
          FS.Read(pPrintProcessInfo^.ErrorText[1], FS.Size);
        finally
          FS.Free;
          SysUtils.DeleteFile(pPrintProcessInfo^.ErrorFile);
        end;
      end;

    CloseHandle(pPrintProcessInfo^.Process.hProcess);
    CloseHandle(pPrintProcessInfo^.Process.hThread);

    SysUtils.DeleteFile(pPrintProcessInfo^.ParamFile);
    SysUtils.DeleteFile(pPrintProcessInfo^.ReportFile);
  end;

begin
  if High(pReportList^) < Low(pReportList^) then
    Exit;

  // Prepare to print
  h := ExtractFilePath(Application.ExeName) + cPrintUtil;

  if not FileExists(h) then
    raise ErwException.Create('RW printing utility "' + h + '" is not found');

  SetLength(PrintProcesses, High(pReportList^) - Low(pReportList^) + 1);
  for i := Low(PrintProcesses) to High(PrintProcesses) do
  begin
    PrintProcesses[i].ReportFile  := '';
    PrintProcesses[i].ErrorFile   := '';
    PrintProcesses[i].ErrorText   := '';
    PrintProcesses[i].pRWAInfoRec := nil;
  end;

  //Start printing processes

  repFile := GetUniqueFileName(GetTmpDir);

  for i := Low(PrintProcesses) to High(PrintProcesses) do
  begin
    PrintProcesses[i].ReportFile := repFile + '_' + IntToStr(i) + '.rwa';
    PrintProcesses[i].ErrorFile  := repFile + '_' + IntToStr(i) + '.err';
    PrintProcesses[i].ParamFile  := repFile + '_' + IntToStr(i) + '.rwj';

    if pReportList^[i].Data4 = '' then
    begin
      FS := TEvFileStream.Create(PrintProcesses[i].ReportFile, fmCreate);
      try
        if Assigned(pReportList^[i].Data1) then
          ST := pReportList^[i].Data1.Data
        else if Assigned(pReportList^[i].Data2) then
          ST := pReportList^[i].Data2
        else if Assigned(pReportList^[i].Data3) then
          ST := pReportList^[i].Data3.RealStream
        else
          ST := nil;

        ST.Position := 0;
        FS.CopyFrom(ST, 0);
      finally
        FreeAndNil(FS);
      end;
    end

    else
      CopyFile(PChar(pReportList^[i].Data4), PChar(PrintProcesses[i].ReportFile), False);

    PrintProcesses[i].pRWAInfoRec := @(pReportList^[i]);

    PrintProcess(@(PrintProcesses[i]));
  end;

  // Wait for finishing
  for i := Low(PrintProcesses) to High(PrintProcesses) do
    WaitProcess(@(PrintProcesses[i]));

  h := '';
  for i := Low(PrintProcesses) to High(PrintProcesses) do
  begin
    if Assigned(PrintProcesses[i].pRWAInfoRec) then
      if PrintProcesses[i].ErrorText <> '' then
      begin
        if h <> '' then
          h := h + #13;
        h := h + PrintProcesses[i].pRWAInfoRec^.Name + ': ' + PrintProcesses[i].ErrorText;
      end;
  end;

  if h <> '' then
    raise ErwException.Create(h);
end;

procedure TrwEngine.CompileIfNeeds(const AReport: TrwReport);
begin
  if (Length(AReport.CompiledCode) = 0) or
     (AReport.LibVersion <= SystemLibFunctions.LibVersion) or (AReport.LibVersion <= SystemLibComponents.LibVersion) then
    Compile(AReport);
end;


procedure TrwEngine.CompileIfNeeds(const AReport: TrmReport);
begin
  if (Length(AReport.CompiledCode) = 0) or
     (AReport.LibVersion <> SystemLibFunctions.LibVersion) or (AReport.LibVersion <> SystemLibComponents.LibVersion) then
    Compile(AReport);
end;

function TrwEngine.OpenSBSQL(const ASQL: String; const Params: array of TrwParamRec): IrwSBTable;
var
  P: TsbParams;
  i: Integer;
begin
  P := TsbParams.Create;
  try
    for i := Low(Params) to High(Params) do
    begin
      P.Add(Params[i].Name);
      P[i].Value := Params[i].Value;
    end;

    Result := TsbTableCursor.Create;
    CustomSQL(ASQL, P, TsbTableCursor(Result.RealObject), nil, nil);
  finally
    P.Free;
  end;
end;


procedure TrwEngine.ExecSBSQL(const ASQL: String; const Params: array of TrwParamRec);
var
  P: TsbParams;
  i: Integer;
begin
  P := TsbParams.Create;
  try
    for i := Low(Params) to High(Params) do
    begin
      P.Add(Params[i].Name);
      P[i].Value := Params[i].Value;
    end;

    CustomSQL(ASQL, P, nil, nil, nil);
  finally
    P.Free;
  end;
end;

function TrwEngine.DataDictionary: IrwDataDictionary;
begin
  Result := rwUtils.DataDictionary;
end;

function TrwEngine.DBCache: IrwDBCache;
begin
  Result := CacheInfo; 
end;

function TrwEngine.OpenSBTable(const ATableName: String; const AReadOnly: Boolean): IrwSBTable;
var
  ma: TsbAccessModeType;
begin
  if AReadOnly then
    ma := sbmRead
  else
    ma := sbmReadWrite;

  Result := sbOpenTable(ATableName, ma);
end;


procedure TrwEngine.CopySBTable(const ATable: IrwSBTable; const ANewTableName: String);
begin
  sbCopyTable(TsbTableCursor(ATable.RealObject), ANewTableName);
end;

procedure TrwEngine.OpenQuery(const ASQL: String; const Params: array of TrwParamRec; const ResultDataSet: TObject);
var
  Q: TrwLQQuery;
  i: Integer;
begin
  Q := TrwLQQuery.Create;
  try
    Q.SQL.Text := ASQL;
    for i := Low(Params) to High(Params) do
      Q.Params[Q.Params.Add(Params[i].Name)].Value := Params[i].Value;
    Q.OpenSQL;

    if Assigned(ResultDataSet) then
    begin
//      sbSBCursorToClientDataSet(Q, ResultDataSet);
    end;

    Q.Close;

  finally
    FreeAndNil(Q);
  end;
end;



function TrwEngine.LibraryComponents: IrwLibraryComponents;
begin
  Result := SystemLibComponents;
end;

procedure TrwEngine.ParamListToParamStream(const AParamList: TrwReportParamList; const AParamStream: TStream);
var
  i: Integer;
  RWP: TrwGlobalVariables;
begin
  RWP := TrwGlobalVariables.Create(nil, TrwGlobalVariable);
  try
    for i := Low(AParamList) to High(AParamList) do
      with RWP.Add do
      begin
        Name := AParamList[i].Name;
        VarType := AParamList[i].ParamType;
        PointerType := '';
        Value := AParamList[i].Value;
        if AParamList[i].RunTimeParam then
          Visibility := rvvPublic
        else
          Visibility := rvvPublished;
      end;
      
    SaveVarsToStream(RWP, AParamStream);

  finally
    FreeAndNil(RWP);
  end;  
end;

function TrwEngine.ParamStreamToParamList(const AParamStream: TStream): TrwReportParamList;
var
  i, n: Integer;
  RWP: TrwGlobalVariables;
begin
  RWP := TrwGlobalVariables.Create(nil, TrwGlobalVariable);
  try
    ReadVarsFromStream(RWP, AParamStream);

    SetLength(Result, RWP.Count);
    n := 0;

    for i := 0 to RWP.Count - 1 do
      with RWP[i] do
        if Visibility in [rvvPublic, rvvPublished] then
        begin
          Result[i].Name := Name;
          Result[i].ParamType := VarType;
          Result[i].Value := Value;
          Result[i].RunTimeParam := Visibility = rvvPublic;
          Inc(n);
        end;

    SetLength(Result, n);

  finally
    FreeAndNil(RWP);
  end;
end;

procedure TrwEngine.HaltReportExecution;
begin
  VM.Halt := True;
end;

function TrwEngine.ReportExecutionHalted: Boolean;
begin
  Result := VM.Halt;
end;


function TrwEngine.LibraryFunctions: IrwLibraryFunctions;
begin
  Result := SystemLibFunctions;
end;

function TrwEngine.SQLConstants: IrwSQLConstants;
begin
  Result := nil;
end;

function TrwEngine.MakeReportResult(const RWResults: PTrwRWAInfoList; const AType: TrwResultFormatType): IrwResult;

  procedure MakeRWA;
  var
    i, k: Integer;
    n: Cardinal;
    lHeader, H: TrwPrintHeader;
    FS: IEvDualStream;
    flDestroyStream: Boolean;
    S: TStream;
    M: TrwPrintMark;
    R: PTrwRWAInfoRec;
    lrs: TrwPrintableLayers;
    lTray: String;
    lOutBin: Integer;
  begin
    flDestroyStream := False;
    S := nil;

    FS := TEvDualStreamHolder.Create;

    lHeader := TrwPrintHeader.Create(nil);
    try
      lHeader.ReportType := rtReport;
      lHeader.PagesList.Clear;

      for i := Low(RWResults^) to High(RWResults^) do
      begin
        R := @(RWResults^[i]);

        try
          if R^.Data4 = '' then
          begin
            if Assigned(R^.Data1) then
              S := R^.Data1.Data
            else if Assigned(R^.Data2) then
              S := R^.Data2
            else if Assigned(R^.Data3) then
              S := R^.Data3.RealStream;
          end
          else
          begin
            S := TEvFileStream.Create(R^.Data4, fmOpenRead);
            flDestroyStream := True;
          end;

          if R^.PrintJobInfo.Layers <> [] then
            lrs := R^.PrintJobInfo.Layers
          else
            lrs := [];

          if R^.PrintJobInfo.TrayName <> '' then
            lTray := R^.PrintJobInfo.TrayName
          else
            lTray := '';

          if R^.PrintJobInfo.OutBinNbr = 0 then
            lOutBin := R^.PrintJobInfo.OutBinNbr
          else
            lOutBin := 0;

          S.Position := S.Size - SizeOf(n);
          S.ReadBuffer(n, SizeOf(n));
          S.Position := n;
          H := TrwPrintHeader(S.ReadComponent(nil));

          try
            if H.PagesCount > 0 then
            begin
              for k := 0  to H.Marks.Count - 1 do
              begin
                M := TrwPrintMark(lHeader.Marks.Add);
                M.Assign(H.Marks[k]);
                M.PageNum := M.PageNum + lHeader.PagesCount;
              end;

              for k := 0  to H.PagesCount - 1 do
                lHeader.PagesList.Add(Pointer(Cardinal(H.PagesList[k]) + Cardinal(FS.Position)));

              for k := 0  to H.ReposList.Count - 1 do
              begin
                lHeader.ReposList.Add(Pointer(Cardinal(H.ReposList[k]) + Cardinal(FS.Position)));
                if lrs <> [] then
                  lHeader.LayersList.Add(Pointer(Byte((@lrs)^)))
                else
                  if H.LayersList.Count > k then
                    lHeader.LayersList.Add(H.LayersList[k])
                  else
                    lHeader.LayersList.Add(Pointer(Byte((@lrs)^)));

                if lTray <> '' then
                  lHeader.TraysList.Add(lTray)
                else
                  if H.TraysList.Count > k then
                    lHeader.TraysList.Add(H.TraysList[k])
                  else
                    lHeader.TraysList.Add(lTray);

                if lOutBin > 0 then
                  lHeader.OutBinsList.Add(Pointer(lOutBin))
                else
                  if H.OutBinsList.Count > k then
                    lHeader.OutBinsList.Add(H.OutBinsList[k])
                  else
                    lHeader.OutBinsList.Add(Pointer(lOutBin));
              end;

              if lHeader.Version > H.Version then
                lHeader.Version := H.Version;

              S.Position := 0;
              FS.RealStream.CopyFrom(S, n + 1);
            end;
          finally
            H.Free;
          end;

        finally
          if flDestroyStream then
          begin
            FreeAndNil(S);
            flDestroyStream := False;
          end;
        end;
      end;

      n := FS.Position;
      FS.WriteComponent(lHeader);
      FS.WriteBuffer(n, SizeOf(n));

      if lHeader.PagesCount > 0 then
        Result := TrwResultHolder.Create(FS, AType);

    finally
      lHeader.Free;
    end;
  end;


  procedure MakeASCII;
  var
    i: Integer;
    FS: IEvDualStream;
    flDestroyStream: Boolean;
    S: TStream;
    R: PTrwRWAInfoRec;
  begin
    flDestroyStream := False;
    S := nil;

    FS := TEvDualStreamHolder.Create;

    for i := Low(RWResults^) to High(RWResults^) do
    begin
      R := @(RWResults^[i]);

      try
        if R^.Data4 = '' then
        begin
          if Assigned(R^.Data1) then
            S := R^.Data1.Data
          else if Assigned(R^.Data2) then
            S := R^.Data2
          else if Assigned(R^.Data3) then
            S := R^.Data3.RealStream;
        end
        else
        begin
          S := TEvFileStream.Create(R^.Data4, fmOpenRead);
          flDestroyStream := True;
        end;

        S.Position := 0;
        FS.RealStream.CopyFrom(S, S.Size);

      finally
        if flDestroyStream then
        begin
          FreeAndNil(S);
          flDestroyStream := False;
        end;
      end;
    end;

    Result := TrwResultHolder.Create(FS, AType);
  end;

begin
  Result := nil;

  if AType = rwRTRWA then
    MakeRWA
  else if AType = rwRTASCII then
    MakeASCII;
end;



procedure TrwEngine.Compile(const AComponent: TrwComponent);
begin
  try
    AComponent.Compile;
  except
    on E: ErwException do
    begin
      E.Message := 'Compile error' + #13 + E.Message;
      raise;
    end;
  end;
end;

function TrwEngine.AnalyzeReport(const pReportInfo: PTrwReportRec): TrwReportContentRec;
  var
    L: TStringList;

  procedure AnlzLibComponents(AComponent: TrwComponent);
  var
    i: Integer;
  begin
    if AComponent.IsLibComponent and (L.IndexOf(AComponent._LibComponent) = -1) then
      L.Add(AComponent._LibComponent);

    for i := 0 to AComponent.ComponentCount - 1 do
    begin
      if not (AComponent.Components[i] is TrwComponent) then
        Continue;
      AnlzLibComponents(TrwComponent(AComponent.Components[i]));
    end;
  end;

  procedure AnalyzeRWReport;
  var
    Report: TrwReport;
  begin
    Report := TrwReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);

      L.Clear;
      AnlzLibComponents(Report);
      Result.ClassName := Report._RwClassName;
      Result.AncestorClassName := Report._LibComponent;
      Result.Description := Report.Description;      
      Result.UsedLibComponents := L.CommaText;

    finally
      FreeAndNil(Report);
    end;
  end;


  procedure AnalyzeRMReport;
  var
    Report: TrmReport;
  begin
    Report := TrmReport.Create(nil);
    try
      pReportInfo^.Data.Position := 0;
      Report.LoadFromStream(pReportInfo^.Data);


      L.Clear;
      AnlzLibComponents(Report);
      Result.ClassName := Report._RwClassName;
      Result.AncestorClassName := Report._LibComponent;
      Result.Description := Report.Description;
      Result.UsedLibComponents := L.CommaText;

    finally
      FreeAndNil(Report);
    end;
  end;

begin
  with Result do
  begin
    ClassName := '';
    AncestorClassName := '';
    Description := '';
    UsedLibComponents := '';
  end;

  L := TStringList.Create;
  try
    if EngineVersion(pReportInfo.Data) = 0 then
      AnalyzeRWReport
    else
      AnalyzeRMReport;
  finally
    FreeAndNil(L);
  end;
end;


procedure TrwEngine.SetReportResultSecurity(const pPreviewInfo: PTrwRWAInfoRec; const pNewSecurityInfo: PTrwRWASecurityRec);
var
  FS: IEvDualStream;
  S: TStream;
begin
  if Assigned(pPreviewInfo.Data1) then
    S := pPreviewInfo.Data1.Data
  else if Assigned(pPreviewInfo.Data2) then
    S := pPreviewInfo.Data2
  else if Assigned(pPreviewInfo.Data3) then
    S := pPreviewInfo.Data3.RealStream
  else if pPreviewInfo.Data4 <> '' then
  begin
    FS := TEvDualStreamHolder.CreateFromFile(pPreviewInfo.Data4, False, False);
    S := FS.RealStream;
  end
  else
    S := nil;

  S.Position := 0;
  if IsSecureRWA(S) <> 0 then
  begin
    UnSecureRWA(S, S, pPreviewInfo.PrintPswd);
    S.Position := 0;
  end;

  SecureRWA(S, S, pNewSecurityInfo.PreviewPassword, pNewSecurityInfo.PrintPassword, pNewSecurityInfo.Compressed);
  S.Position := 0;

  pPreviewInfo.PreviewPswd := pNewSecurityInfo.PreviewPassword;
  pPreviewInfo.PrintPswd := pNewSecurityInfo.PrintPassword;
end;

function TrwEngine.RWAtoPDF(const pPreviewInfo: PTrwRWAInfoRec; const pPDFInfoRec: PTrwPDFInfoRec): IEvDualStream;
var
  S: TStream;
  FS: IEvDualStream;
begin
  with pPreviewInfo^ do
  begin
    if Assigned(Data1) then
      S := Data1.Data
    else if Assigned(Data2) then
      S := Data2
    else if Assigned(Data3) then
      S := Data3.RealStream
    else
    begin
      FS := TEvDualStreamHolder.CreateFromFile(Data4);
      S := FS.RealStream;
    end;
  end;

  Result := ConvertRWAtoPDF(S, nil, pPDFInfoRec);
end;


function TrwEngine.EngineVersion(const ReportStructure: TStream): Integer;
var
  P: Integer;
begin
  P := ReportStructure.Position;
  try
    ReportStructure.Position := 0;
    if IsRMReport(ReportStructure) then
      Result := 1
    else
      Result := 0;
  finally
    ReportStructure.Position := P;
  end;
end;

procedure TrwEngine.GetReportParameters(const ReportStructure: TStream; const PParamList: PTrwReportParamList);
var
  Report: TComponent;
  ver: Integer;
begin
  try
    ver := EngineVersion(ReportStructure);
    ReportStructure.Position := 0;

    if ver = 0 then
    begin
      Report := TrwReport.Create(nil);
      TrwReport(Report).LoadFromStream(ReportStructure);
    end

    else if ver = 1 then
    begin
      Report := TrmReport.Create(nil);
      TrmReport(Report).LoadFromStream(ReportStructure);
    end

    else
      Report := nil;

    GetReportParameters(Report, PParamList);

  finally
    FreeAndNil(Report);
  end;
end;

function TrwEngine.GetVM: TrwVirtualMachine;
begin
  Result := VM;
end;

function TrwEngine.CreateReportObject(const ReportStructure: TStream): TrwComponent;

  procedure RWReport;
  var
    Report: TrwReport;
  begin
    Report := TrwReport.Create(nil);
    ReportStructure.Position := 0;
    Report.LoadFromStream(ReportStructure);
    Report.Compile;
    Result := Report;
  end;

  procedure RMReport;
  var
    Report: TrmReport;
  begin
    Report := TrmReport.Create(nil);
    ReportStructure.Position := 0;
    Report.LoadFromStream(ReportStructure);
    Report.Compile;
    Result := Report;
  end;

begin
  if EngineVersion(ReportStructure) = 0 then
    RWReport
  else
    RMReport;
end;

procedure TrwEngine.GetReportParameters(const Report: TObject; const PParamList: PTrwReportParamList);
var
  V: TrwGlobalVariables;
begin
  if Report is TrwReport then
    V := TrwReport(Report).GlobalVarFunc.Variables
  else if Report is TrmReport then
    V := TrmReport(Report).GlobalVarFunc.Variables
  else
    V := nil;

  if Assigned(V) then
    VariablesToParamList(V, PParamList);
end;


procedure TrwEngine.RaiseException(const AErrorMessage: String);
begin
  raise ErwException.Create(AErrorMessage);
end;


function TrwEngine.ShowQueryBuilder(const AQuery: TStream; const NiceNames: Boolean = True): Boolean;
var
  Q: TrwQBQuery;
  Vars: IrmVariables;
begin
  if Assigned(FRunningReport) then
    Vars := (FRunningReport as TrwComponent).GlobalVarFunc.Variables
  else
    Vars := nil;

  if Assigned(AQuery) then
  begin
    Q := TrwQBQuery.Create(nil);
    try
      AQuery.Position := 0;
      if AQuery.Size > 0 then
        Q.ReadFromStream(AQuery);

      Result := rwQueryBuilderFrm.ShowQueryBuilderExt(Q, False, NiceNames, Vars);

      if Result then
      begin
        AQuery.Size := 0;
        Q.SaveToStream(AQuery);
      end;

    finally
      FreeAndNil(Q);
    end;
  end

  else
  begin
    rwQueryBuilderFrm.ShowQueryBuilderExt(nil, True, NiceNames, Vars);
    Result := False;
  end;
end;


function TrwEngine.RunQBQuery(const AQuery: TStream; const Params: TrwParamList; const AClientDataSet: TISBasicClientDataSet): IrwSBTable;
var
  Q: TrwQBQuery;
  SQL: String;
  i: Integer;
  Prm: TsbParam;
begin
  Q := TrwQBQuery.Create(nil);
  try
    AQuery.Position := 0;
    Q.ReadFromStream(AQuery);
    SQL := Q.SQL;
  finally
    FreeAndNil(Q);
  end;

  Result := TrwLQQuery.Create;
  TrwLQQuery(Result.RealObject).SQL.Text := SQL;

  for i := Low(Params) to High(Params) do
  begin
    Prm := TrwLQQuery(Result.RealObject).Params.ParamByName(Params[i].Name);
    if Assigned(Prm) then
      Prm.Value := Params[i].Value;
  end;

  TrwLQQuery(Result.RealObject).OpenSQL;

  if Assigned(AClientDataSet) then
    sbSBCursorToClientDataSet(TsbTableCursor(Result.RealObject), AClientDataSet);
end;

function TrwEngine.ShowQueryBuilder(const AQueryData: IEvDualStream; const NiceNames: Boolean): IrwDesignerForm;
var
  Vars: IrmVariables;
begin
  if Assigned(FRunningReport) then
    Vars := (FRunningReport as TrwComponent).GlobalVarFunc.Variables
  else
    Vars := nil;

  Result := rwQueryBuilderFrm.ShowQueryBuilderExt2(AQueryData, NiceNames, Vars);
end;

{ TrwResultHolder }

constructor TrwResultHolder.Create(const AFileName: string; const AFormat: TrwResultFormatType = rwRTUnknown; AReadOnly: Boolean = True);
var
  h: String;
begin
  FFileName := AFileName;
  FReadOnly := AReadOnly;
  FDestroyStream := False;
  FPResultPageInfoList := nil;
  FType := AFormat;

  if FType = rwRTUnknown then
  begin
    h := ExtractFileExt(FFileName);
    if AnsiSameText(h, '.rwa') then
      FType := rwRTRWA
    else if AnsiSameText(h, '.txt') then
      FType := rwRTASCII
    else if AnsiSameText(h, '.pdf') then
      FType := rwRTPDF
    else if AnsiSameText(h, '.hml') then
      FType := rwRTHTML
    else if AnsiSameText(h, '.xml') then
      FType := rwRTXML
    else if AnsiSameText(h, '.xls') then
      FType := rwRTXLS;
  end;
end;

constructor TrwResultHolder.Create(const AIStream: IEvDualStream; AType: TrwResultFormatType);
begin
  FDestroyStream := False;
  FIStream := AIStream;
  FData := FIStream.RealStream;
  FFileName := '';
  FType := AType;
  FPResultPageInfoList := nil;
end;


destructor TrwResultHolder.Destroy;
begin
  if Assigned(FPResultPageInfoList) then
    Dispose(FPResultPageInfoList);

  if FDestroyStream then
  begin
    FreeAndNil(FData);

    if FFileName <> '' then
    begin
      SysUtils.DeleteFile(FFileName);
      SysUtils.DeleteFile(ChangeFileExt(FFileName, '.tmp'));      
    end;
  end;

  inherited;
end;

function TrwResultHolder.Data: TStream;
var
  fm: Word;
begin
  if not Assigned(FData) then
  begin
    if FReadOnly then
      fm := fmOpenRead
    else if FileExists(FFileName) then
      fm := fmOpenReadWrite
    else
      fm := fmCreate;
    FData := TEvFileStream.Create(FFileName, fm);
    FDestroyStream := True;
  end;
  Result := FData;
end;



function TrwResultHolder.FileName: String;
begin
  Result := FFileName;
end;

function TrwResultHolder.ResultType: TrwResultFormatType;
begin
  Result := FType;
end;

function TrwResultHolder.PageInfo: PTrwResultPageInfoList;
var
  P: TrwVirtualPage;
  i: Integer;
  fl: Boolean;
begin
  if not Assigned(FPResultPageInfoList) then
  begin
    New(FPResultPageInfoList);

    if ResultType = rwRTRWA then
    begin
      P := TrwVirtualPage.Create(nil);
      try
        fl := Assigned(FData);
        P.Initialize(Data);
        SetLength(FPResultPageInfoList^, P.Header.PagesCount);

        for i := 1 to P.Header.PagesCount do
        begin
          P.PreparePage(i, Data);
          FPResultPageInfoList^[i - 1].PageNumber := i;
          FPResultPageInfoList^[i - 1].WidthMM := Round(ConvertUnit(P.Width, utScreenPixels, utMillimeters));
          FPResultPageInfoList^[i - 1].HeightMM := Round(ConvertUnit(P.Height, utScreenPixels, utMillimeters));
          FPResultPageInfoList^[i - 1].Duplexing := P.Duplexing;
        end;

        if not fl and FDestroyStream then
        begin
          FDestroyStream := False;
          FreeAndNil(FData);
        end;
      finally
        FreeAndNil(P);
      end;
    end;
  end;

  Result := FPResultPageInfoList;
end;


procedure TrwResultHolder.ReleaseFileHandle;
begin
  if (FFileName <> '') and FDestroyStream then
  begin
    FDestroyStream := False;
    FreeAndNil(FData);
  end;
end;

function TrwResultHolder.MiscInfo: TrwResultMiscInfo;
begin
  Result := FMiscInfo;
end;

procedure TrwResultHolder.SetMiscInfo(const AMiscInfo: TrwResultMiscInfo);
begin
  FMiscInfo := AMiscInfo;
end;

{ TrwInputFormHolder }

constructor TrwInputFormHolder.Create(const AOwner: TrwEngine; const pReportInfo: PTrwReportRec; const pShowInfo: PTrwShowInputFormRec);

  procedure CreateAncestorInstance;
  var
    Cl: IrwClass;
  begin
    Cl := rwRTTIInfo.FindClass(pReportInfo^.ReportAncestorClassName);
    if not Assigned(Cl) then
      raise ErwException.CreateFmt(ResErrWrongLibComp, [pReportInfo^.ReportAncestorClassName]);

    FReport := Cl.CreateObjectInstance as TrwComponent;
    FReport.Name := 'Report';
    FReport.Compile;
  end;

begin
  FOwner := AOwner;

  if Assigned(pReportInfo^.Data) then
  begin
    if FOwner.EngineVersion(pReportInfo.Data) = 0 then
    begin
      FReport := TrwReport.Create(nil);
      pReportInfo^.Data.Position := 0;
      TrwReport(FReport).LoadFromStream(pReportInfo^.Data);
      FOwner.CompileIfNeeds(TrwReport(FReport));
    end
    else
    begin
      FReport := TrmReport.Create(nil);
      pReportInfo^.Data.Position := 0;
      TrmReport(FReport).LoadFromStream(pReportInfo^.Data);
      FOwner.CompileIfNeeds(TrmReport(FReport));
    end;
  end
  else
    CreateAncestorInstance;

  if FReport is TrmReport then
  begin
    TrmReport(FReport).SetReportParams(pShowInfo.Params);
    FModalDialog := pShowInfo^.Parent = 0;
    TrmReport(FReport).BeginWork;
  end
  else
  begin
    TrwReport(FReport).SetReportParams(pShowInfo.Params);
    FModalDialog := pShowInfo^.Parent = 0;
    TrwReport(FReport).BeginWork;
  end;
end;

destructor TrwInputFormHolder.Destroy;
begin
  if Assigned(DebugInfo) then
    DebugInfo.Report := nil;

  if FReport is TrwReport then
    TrwReport(FReport).EndWork
  else
    TrmReport(FReport).EndWork;

  FreeAndNil(FReport);
  inherited;
end;

function TrwInputFormHolder.Close(const Params: PTrwReportParamList): Boolean;
var
 InpForm: TrwInputFormContainer;
begin
  Result := False;

  if FReport is TrwReport then
    InpForm := TrwReport(FReport).InputForm
  else
    InpForm := TrmReport(FReport).InputForm;

  if not InpForm.Visible then
    Exit;

  if not FModalDialog then
  begin
    if Assigned(Params) then
      InpForm.CloseResult := mrOK
    else
      InpForm.CloseResult := mrCancel;

    if not InpForm.DoCloseQuery then
    begin
      InpForm.CloseResult := mrNone;
      Exit;
    end;

    if FReport is TrwReport then
      TrwReport(FReport).CloseInputForm
    else
      TrmReport(FReport).CloseInputForm
  end;

  if Assigned(Params) and (InpForm.CloseResult = mrOk) then
  begin
    if FReport is TrwReport then
      TrwReport(FReport).GetReportParams(Params)
    else
      TrmReport(FReport).GetReportParams(Params);

    Result := True;
  end;
end;

function TrwInputFormHolder.IsModalDialog: Boolean;
begin
  Result := FModalDialog;
end;

function TrwInputFormHolder.VCLObject: TObject;
begin
 if FReport is TrwReport then
   Result := TrwReport(FReport).InputForm.DesignParent

 else if FReport is TrmReport then
   Result := TrmReport(FReport).InputForm.DesignParent

 else
   Result := nil;
end;

end.
