unit rwRendering;

interface

uses Classes, Messages, Windows, Controls, ExtCtrls, Graphics, rwReport, rwTypes,
  Forms, SysUtils, rwMessages, rwCommonClasses, rwPrintElements,
  rwPrintPage, rwDebugInfo, rwBasicClasses, rwUtils, Variants, sbAPI, rwEngineTypes,
  rwGraphics, EvStreamUtils, rwBasicUtils, rmTypes;

type

  TrwRendering = class;

  {TrwRenderingPaper is image of current preparing page}

  TrwRenderingPaper = class(TrwVirtualPage)
  private
    FCreator: TrwRendering;
    FCurrentRendering: TrwRendering;
    FCurrentTop: Integer;
    FBottomLimit: Integer;
    FPageFooterTop: Integer;
    FOnNewPage: TNotifyEvent;
    FResultStream: TStream;
    FASCIIResult: TsbTableCursor;
    FTopMargin: Integer;
    FLeftMargin: Integer;
    FBottomMargin: Integer;
    FRightMargin: Integer;

    procedure CMControlListChange(var Message: TMessage); message CM_CONTROLLISTCHANGE;
    procedure Initialize;
    function  GetASCIIResult: TsbTableCursor;
    function  BeforePostASCII(var AText: Variant): Boolean;
    procedure SetCurrASCIILine(const ALine: Integer);

  protected
    procedure WriteResultHeader;

  public
    property ASCIIResult: TsbTableCursor read GetASCIIResult;
    property Creator: TrwRendering read FCreator;
    property CurrentRendering: TrwRendering read FCurrentRendering;
    property CurrentTop: Integer read FCurrentTop write FCurrentTop;
    property BottomLimit: Integer read FBottomLimit;
    property PageFooterTop: Integer read FPageFooterTop;
    property TopMargin: Integer read FTopMargin;
    property LeftMargin: Integer read FLeftMargin;
    property BottomMargin: Integer read FBottomMargin;
    property RightMargin: Integer read FRightMargin;
    property OnPrintEvent: TNotifyEvent read FOnNewPage write FOnNewPage;

    constructor Create(ACreator: TrwRendering); reintroduce;
    destructor  Destroy; override;
    procedure   EndOfResult;
    procedure   MoveCurrentPosition(Delta: Integer);
    function    ResultStream: TStream;
    function    IsASCIIResult: Boolean;
    procedure   AppendASCIILine(AText: String);
    procedure   InsertASCIILine(AText: String; ALine: Integer);
    procedure   UpdateASCIILine(AText: String; ALine: Integer);
    procedure   DeleteASCIILine(ALine: Integer);
    function    GetASCIILine(ALine: Integer): String;
    function    ASCIILineCount: Integer;
  end;

  {TrwRendering is class for execution Main Report}

  TrwRendering = class
  private
    FRenderingPaper: TrwRenderingPaper;
    FReport: TrwCustomReport;
    FMainReport: TrwCustomReport;
    FCurrentAutosizeBand: TrwBand;
    FLastControlIndex: Integer;
    FCurrentPosition: Integer;
    FBufList: TList;
    FNeedGoToNewPage: Boolean;
    FPageWasChanged: Boolean;
    FPageIsEmpty: Boolean;

    procedure InitNewPage;

  protected
    TitlePrinted: Boolean;
    PageHeaderPrinted: Boolean;
    PageFooterPrinted: Boolean;
    HeaderPrinted: Boolean;
    FooterPrinted: Boolean;

    function CheckChangeGroupValue(const Footers: Boolean): Boolean;

  public
    ResultFileName: string;
    OwnerASCIIResult: TrwRenderingPaper;
    property Report: TrwCustomReport read FReport;
    property RenderingPaper: TrwRenderingPaper read FRenderingPaper;
    property CurrentAutosizeBand: TrwBand  read FCurrentAutosizeBand;

    constructor Create;
    destructor Destroy; override;
    procedure ExecuteReport(AReport: TrwCustomReport);
    procedure PrintBand(ABandType: TrwBandType; ABand: TrwBand = nil);
    procedure SkipPage;
  end;


  procedure InitThreadVars_rwRendering;
  function  CurrentRendering: TrwRendering;

implementation

threadvar
  ltvRendering: TrwRendering;

procedure InitThreadVars_rwRendering;
begin
  ltvRendering :=nil;
end;


function CurrentRendering: TrwRendering;
begin
  Result := ltvRendering;
end;



  {TrwRenderingPaper}

constructor TrwRenderingPaper.Create(ACreator: TrwRendering);
begin
  inherited Create(nil);

  FCreator := ACreator;

  FCurrentRendering := nil;
  FOnNewPage := nil;

  Header.PixelsPerInch := cScreenCanvasRes;
  Repository.PixelsPerInch := cScreenCanvasRes;

  FResultStream := nil;
  FASCIIResult := nil;
end;

destructor TrwRenderingPaper.Destroy;
var
  i: Integer;
  F: TextFile;
  sTxtFileName, sXLSFileName: String;
  ConvertToXls: boolean;
  LastLineBreak: boolean;
begin
  if Assigned(FResultStream) then
    FResultStream.Free;

  if Creator.FMainReport is TrwReport then
    ConvertToXls := AnsiSameText(TrwReport(Creator.FMainReport).ASCIIDelimiter, 'EXCEL')
  else
    ConvertToXls := false;

  if Creator.FMainReport is TrwReport then
    LastLineBreak := TrwReport(Creator.FMainReport).ASCIILastLineBreak
  else
    LastLineBreak := false;

  if IsASCIIResult and not Assigned(FCreator.OwnerASCIIResult) then
  begin
    if ConvertToXls then
      sTxtFileName := ChangeFileExt(FASCIIResult.TableName, '.tmp')
    else
      sTxtFileName := ChangeFileExt(FASCIIResult.TableName, '.txt');
    AssignFile(F, sTxtFileName);
    Rewrite(F);
    for i := 1 to FASCIIResult.RecordCount do
    begin
      FASCIIResult.RecNo := i;
      if (LastLineBreak or ConvertToXls) or (i <> FASCIIResult.RecordCount) then
        Writeln(F, FASCIIResult.Fields[0].AsString)
      else
        Write(F, FASCIIResult.Fields[0].AsString);
    end;
    CloseFile(F);
    FASCIIResult.Free;

    if ConvertToXls then
    begin
      sXLSFileName := ChangeFileExt(sTxtFileName, '.xls');
      try
        ConvertASCII2Xls(sTxtFileName, sXLSFileName, Ascii2XlsDelimiter, Ascii2XlsQualifier);
      finally
        DeleteFile(sTxtFileName);
      end;
    end;
  end;

  inherited;
end;

function TrwRenderingPaper.ResultStream: TStream;
begin
  if not Assigned(FResultStream) then
    FResultStream := TEvFileStream.Create(FCreator.ResultFileName, fmCreate);

  Result := FResultStream;
end;

function TrwRenderingPaper.GetASCIIResult: TsbTableCursor;
var
  s: String;
  Flds: TsbFields;
begin
  if not IsASCIIResult then
  begin
    if Assigned(FCreator.OwnerASCIIResult) then
      FASCIIResult := FCreator.OwnerASCIIResult.ASCIIResult

    else
    begin
      s := ChangeFileExt(FCreator.ResultFileName, sbTableExt);
      Flds := TsbFields.Create;
      try
        Flds.CreateField('ASCIILine', sbfBlob);
        sbCreateTable(s, Flds);
        FASCIIResult := sbOpenTable(s, nil, sbmReadWrite, True);
      finally
        Flds.Free;
      end;
    end;

    if Assigned(FResultStream) then
      ResultStream.Size := 0;
    DestroyComponents;
  end;
  Result := FASCIIResult;
end;


procedure TrwRenderingPaper.Initialize;
var
  lReport: TrwCustomReport;
  i: Integer;
begin
  lReport := Creator.Report;

  PageSize := lReport.PageFormat.PaperSize;
  PageOrientation := lReport.PageFormat.PaperOrientation;

  if Creator.FMainReport is TrwReport then
    Duplexing := lReport.PageFormat.Duplexing or TrwReport(Creator.FMainReport).Duplexing
  else
    Duplexing := False;

  FTopMargin := System.Round(ConvertUnit(lReport.PageFormat.TopMargin / 10, utMillimeters, utScreenPixels));

  FLeftMargin := System.Round(ConvertUnit(lReport.PageFormat.LeftMargin / 10, utMillimeters, utScreenPixels));

  FBottomMargin := System.Round(ConvertUnit(lReport.PageFormat.BottomMargin / 10, utMillimeters, utScreenPixels));

  FRightMargin := System.Round(ConvertUnit(lReport.PageFormat.RightMargin / 10, utMillimeters, utScreenPixels));

  Width := System.Round(ConvertUnit(lReport.PageFormat.Width / 10, utMillimeters, utScreenPixels));

  Height := System.Round(ConvertUnit(lReport.PageFormat.Height / 10, utMillimeters, utScreenPixels));

  FCurrentTop := FTopMargin;
  FBottomLimit := Height - FBottomMargin;

  i := lReport.ReportForm.IndexOfBand(rbtPageFooter);
  if (i <> -1) then
  begin
    FPageFooterTop := FBottomLimit - lReport.ReportForm.Bands[i].Height;
    FBottomLimit := FPageFooterTop;
  end;
  i := lReport.ReportForm.IndexOfBand(rbtFooter);
  if (i <> -1) then
    FBottomLimit := FBottomLimit - lReport.ReportForm.Bands[i].Height;
end;

procedure TrwRenderingPaper.WriteResultHeader;
var
  n: Cardinal;
begin
  if (Creator.ResultFileName = '') or IsASCIIResult then
    Exit;

  if Creator.FMainReport is TrwReport then
    Header.ReportType := TrwReport(Creator.FMainReport).ReportType
  else
    Header.ReportType := rtUnknown;

  n := ResultStream.Position;
  Header.ReposList.Add(Pointer(n));
  ResultStream.WriteComponent(Repository);
  WriteHeaderInfo(ResultStream);
end;

procedure TrwRenderingPaper.CMControlListChange(var Message: TMessage);
var
  lControl: Tcontrol;
begin
  inherited;

  if not Boolean(Message.LParam) then Exit;

  lControl := TControl(Message.WParam);
  lControl.Top := lControl.Top + FCurrentTop;
  lControl.Left := lControl.Left + FLeftMargin;
end;

procedure TrwRenderingPaper.EndOfResult;
begin
  FResultStream.Free;
  FResultStream := nil;
end;

procedure TrwRenderingPaper.MoveCurrentPosition(Delta: Integer);
begin
  FCurrentTop := FCurrentTop + Delta;
end;

procedure TrwRenderingPaper.UpdateASCIILine(AText: String; ALine: Integer);
var
  V: Variant;
begin
  SetCurrASCIILine(ALine);
  V := AText;
  if BeforePostASCII(V) then
  begin
    ASCIIResult.Edit;
    try
      ASCIIResult.Fields[0].AsString := V;
      ASCIIResult.Post;
    except
      ASCIIResult.Cancel;
      raise;
    end;
  end;
end;


procedure TrwRenderingPaper.DeleteASCIILine(ALine: Integer);
begin
  SetCurrASCIILine(ALine);
  ASCIIResult.Delete;
end;


function TrwRenderingPaper.BeforePostASCII(var AText: Variant): Boolean;
var
  r: Variant;
begin
  r := True;
  FCurrentRendering.FReport.ExecuteEventsHandler('BeforeWriteASCIILine', [Integer(Addr(AText)), Integer(Addr(r))]);
  Result := r;
end;


procedure TrwRenderingPaper.AppendASCIILine(AText: String);
var
  V: Variant;
begin
  ASCIIResult.Last;
  V := AText;
  if BeforePostASCII(V) then
  begin
    ASCIIResult.Append;
    try
      ASCIIResult.Fields[0].AsString := V;
      ASCIIResult.Post;
    except
      ASCIIResult.Cancel;
      raise;
    end;
  end;
end;


procedure TrwRenderingPaper.InsertASCIILine(AText: String; ALine: Integer);
var
  V: Variant;
begin
  SetCurrASCIILine(ALine);
  V := AText;
  if BeforePostASCII(V) then
  begin
    ASCIIResult.Insert;
    try
      ASCIIResult.Fields[0].AsString := V;
      ASCIIResult.Post;
    except
      ASCIIResult.Cancel;
      raise;
    end;
  end;
end;


function TrwRenderingPaper.GetASCIILine(ALine: Integer): String;
begin
  SetCurrASCIILine(ALine);
  Result := ASCIIResult.Fields[0].AsString;
end;


procedure TrwRenderingPaper.SetCurrASCIILine(const ALine: Integer);
begin
  if not IsASCIIResult or (ALine <= 0) or (Integer(ASCIIResult.RecordCount) < ALine) then
    raise ErwException.Create('Requested ASCII line #' + IntToStr(ALine) + ' is out of bounds')
  else
    ASCIIResult.RecNo := ALine;
end;


function TrwRenderingPaper.ASCIILineCount: Integer;
begin
  if not IsASCIIResult then
    Result := 0
  else
    Result := ASCIIResult.RecordCount;
end;


function TrwRenderingPaper.IsASCIIResult: Boolean;
begin
  Result := Assigned(FASCIIResult);
end;

{TrwRendering}

constructor TrwRendering.Create;
begin
  inherited;
  FBufList := TList.Create;
  FCurrentAutosizeBand := nil;
  OwnerASCIIResult  := nil;
  FRenderingPaper := nil;
  FReport := nil;
  FMainReport := nil;
end;


destructor TrwRendering.Destroy;
begin
  if Assigned(FRenderingPaper) then
    if (FRenderingPaper.Creator = Self) then
      FRenderingPaper.Free;

  FBufList.Free;
  inherited;
end;

procedure TrwRendering.ExecuteReport(AReport: TrwCustomReport);
var
  lLastRendering: TrwRendering;
  lLastCustomReport: TrwCustomReport;
  lTitlePrinted: Boolean;
  lHeaderPrinted: Boolean;
  lFooterPrinted: Boolean;
  lPageWasChanged: Boolean;
  i: Integer;
  s: String;
  lPrevRendering: TrwRendering;
begin
  if (AReport is TrwSubReport) and AReport.StandAlone and not FPageIsEmpty then
    SkipPage;

  lPrevRendering := ltvRendering;
  ltvRendering := Self;

  lTitlePrinted := TitlePrinted;
  lHeaderPrinted := HeaderPrinted;
  lFooterPrinted := FooterPrinted;
  lPageWasChanged := FPageWasChanged;
  FPageWasChanged := False;
  TitlePrinted := False;
  lLastCustomReport := FReport;
  FReport := AReport;

  if not Assigned(FRenderingPaper) then
  begin
    FMainReport :=  FReport;
    FRenderingPaper := TrwRenderingPaper.Create(Self);
    s := ChangeFileExt(ResultFileName, '.txt');
    DeleteFile(s);
  end;

  if FReport.StandAlone then
    FRenderingPaper.Initialize;

  lLastRendering := FRenderingPaper.CurrentRendering;

  FRenderingPaper.FCurrentRendering := Self;

  if FReport.StandAlone then
    InitNewPage
  else
  begin
    HeaderPrinted := False;
    FooterPrinted := False;
  end;

  FReport.ExecuteEventsHandler('BeforePrint', []);

  if Assigned(DebugInfo) and DebugInfo.Aborted then Exit;

  if Assigned(FReport.MasterDataSource) then
  begin
    if not FReport.MasterDataSource.PActive then
      try
        FReport.MasterDataSource.Open
      except
        on E: Exception do
        begin
          if not (E is ErwAbort) then
            E.Message := Format(ResErrSQL, [FReport.MasterDataSource.Name])+#13+E.Message;
          raise;
        end
      end
    else
      if FReport.MasterDataSource.PRecordCount > 0 then
        FReport.MasterDataSource.PFirst;

    for i := 0 to FReport.Bands.Groups.Count - 1 do
      FReport.Bands.Groups[i].LastValue := Unassigned;

    while not FReport.MasterDataSource.PEof do
    begin
      if Assigned(FReport.DetailDataSource) then
      begin
        FReport.DetailDataSource.Close;
        FReport.DetailDataSource.Open;
        FReport.DetailDataSource.PFirst;
      end;

      CheckChangeGroupValue(False);

      PrintBand(rbtDetail);

      if Assigned(DebugInfo) and DebugInfo.Aborted then Exit;

      if Assigned(FReport.DetailDataSource) then
      begin
        while not FReport.DetailDataSource.PEof do
        begin
          PrintBand(rbtSubDetail);
          if Assigned(DebugInfo) and DebugInfo.Aborted then
            Exit;
          FReport.DetailDataSource.PNext;
        end;
        if not (FReport.DetailDataSource.PEof and FReport.DetailDataSource.PBof) then
          PrintBand(rbtSubSummary);
      end;

      if not CheckChangeGroupValue(True) then
        FReport.MasterDataSource.PNext;
    end;

    if TitlePrinted then
    begin
      for i := Report.Bands.Groups.Count - 1 downto 0 do
        if not Report.Bands.Groups[i].Disabled and Assigned(Report.Bands.Groups[i].GroupFooter) then
          PrintBand(rbtGroupFooter, Report.Bands.Groups[i].GroupFooter);

      PrintBand(rbtSummary);
      if not FooterPrinted then
      begin
        FooterPrinted := True;
        PrintBand(rbtFooter);
      end;
    end;
  end

  else
  begin
    PrintBand(rbtDetail);
    PrintBand(rbtSummary);
    if not FooterPrinted then
    begin
      FooterPrinted := True;
      PrintBand(rbtFooter);
    end;
  end;

  if ((FReport is TrwReport) or FReport.StandAlone) and not FPageIsEmpty then
    SkipPage;

  FReport.ExecuteEventsHandler('AfterPrint', []);

  if (FReport is TrwReport) then
    FRenderingPaper.WriteResultHeader;

  if Assigned(DebugInfo) and DebugInfo.Aborted then
    Exit;

  if (FReport is TrwReport) then
    FRenderingPaper.EndOfResult;

  FReport := lLastCustomReport;
  FRenderingPaper.FCurrentRendering := lLastRendering;

  if ResultFileName = '' then
    FPageWasChanged := False;

  if FPageWasChanged then
  begin
    HeaderPrinted := False;
    FooterPrinted := False;
  end
  else
  begin
    HeaderPrinted := lHeaderPrinted;
    FooterPrinted := lFooterPrinted;
  end;

  TitlePrinted := lTitlePrinted;
  FPageWasChanged := lPageWasChanged or FPageWasChanged;
  ltvRendering := lPrevRendering;

  if Assigned(FReport) and AReport.StandAlone then
    FRenderingPaper.Initialize;
end;

function TrwRendering.CheckChangeGroupValue(const Footers: Boolean): Boolean;
var
  i, j: Integer;
  lFlGroupChange: Boolean;
begin
  Result := False;

  with Report.Bands do
  begin
    if Footers and (Groups.Count > 0) then
    begin
      Report.MasterDataSource.PDisableControls;
      Report.MasterDataSource.PNext;
    end;

    for i := 0 to Groups.Count - 1 do
    begin
      if Groups[i].Disabled then
        Continue;

      lFlGroupChange := False;

      if VarIsEmpty(Groups[i].LastValue) or
        VarIsNull(Groups[i].LastValue) and not VarIsNull(Report.MasterDataSource.GetFieldValue(Groups[i].FieldName)) or
        not VarIsNull(Groups[i].LastValue) and VarIsNull(Report.MasterDataSource.GetFieldValue(Groups[i].FieldName)) then

        lFlGroupChange := True

      else
        if not VarIsNull(Groups[i].LastValue) and
        not VarIsNull(Report.MasterDataSource.GetFieldValue(Groups[i].FieldName)) and
        (string(Groups[i].LastValue) <> string(Report.MasterDataSource.GetFieldValue(Groups[i].FieldName))) then
        lFlGroupChange := True;

      if lFlGroupChange then
      begin

        if VarIsEmpty(Groups[i].LastValue) then // Print GroupHeader
          for j := i to Groups.Count - 1 do
          begin
            if Groups[j].Disabled then
              Continue;
            Groups[j].LastValue := Report.MasterDataSource.GetFieldValue(Groups[j].FieldName);
            if Assigned(Groups[j].GroupHeader) then
              PrintBand(rbtGroupHeader, Groups[j].GroupHeader);
          end

        else // Print GroupFooter
        begin
          try
            if not Report.MasterDataSource.PEof then
              Report.MasterDataSource.PPrior;
            for j := Groups.Count - 1 downto i do
            begin
              if Groups[j].Disabled then
                Continue;
              Groups[j].LastValue := Unassigned;
              if Assigned(Groups[j].GroupFooter) then
                PrintBand(rbtGroupFooter, Groups[j].GroupFooter);
            end;
          finally
            if not Report.MasterDataSource.PEof then
              Report.MasterDataSource.PNext;
          end;
          break;
        end;
      end;
    end;

    if Footers and (Groups.Count > 0) then
    begin
//      if not Report.MasterDataSource.PEof then
//        Report.MasterDataSource.PPrior;
      Report.MasterDataSource.PEnableControls;
      Result := True;
    end;
  end;
end;

procedure TrwRendering.InitNewPage;
begin
  HeaderPrinted := False;
  FooterPrinted := False;
  PageHeaderPrinted := False;
  PageFooterPrinted := False;
  FRenderingPaper.CurrentTop := FRenderingPaper.TopMargin;
  RenderingPaper.DestroyComponents;
  FNeedGoToNewPage := False;
  FPageWasChanged := True;
  FPageIsEmpty := True;
end;

procedure TrwRendering.SkipPage;
begin
  if (Length(ResultFileName) = 0) or FRenderingPaper.IsASCIIResult then
    Exit;

  if FPageIsEmpty then
  begin
    PageHeaderPrinted := True;
    PrintBand(rbtPageHeader);
  end;

  if not PageFooterPrinted then
  begin
    FRenderingPaper.MoveCurrentPosition(FRenderingPaper.BottomLimit - FRenderingPaper.CurrentTop);
    PageFooterPrinted := True;
    PrintBand(rbtPageFooter);
  end;

  if FPageIsEmpty then
    RenderingPaper.Header.PagesList.Add(nil);

  CompressPage(RenderingPaper);

  RenderingPaper.Header.PagesList[RenderingPaper.Header.PagesCount - 1] := Pointer(RenderingPaper.ResultStream.Position);
  RenderingPaper.ResultStream.WriteComponent(RenderingPaper);

  if (RenderingPaper.Creator.FMainReport is TrwReport) and
     Assigned(TrwReport(RenderingPaper.Creator.FMainReport).OnAfterPrintPage) then
    TrwReport(RenderingPaper.Creator.FMainReport).OnAfterPrintPage(RenderingPaper);

  InitNewPage;
end;

procedure TrwRendering.PrintBand(ABandType: TrwBandType; ABand: TrwBand = nil);
var
  i,j,d, h1, h2, n, m, l: Integer;
  Rep: TrwCustomReport;
  C, SplitComp: TControl;
  fl_cut: Boolean;

  procedure RePrintComp(Comp: TControl; Shift: Integer);
  begin
    RenderingPaper.InsertComponent(Comp);
    Comp.Top := Comp.Top-Shift;
    h1 := Comp.Top;
    h2 := Comp.Left;
    Comp.Parent := RenderingPaper;
    Comp.Top := h1;
    Comp.Left := h2;
  end;

begin
  if (ABandType in [rbtPageHeader, rbtPageFooter]) and not Report.StandAlone then
    Rep := FMainReport
  else
    Rep := Report;

  i := Rep.ReportForm.IndexOfBand(ABandType);
  if (i = -1) then Exit;

  if FPageIsEmpty then
  begin
    RenderingPaper.Header.PagesList.Add(nil);
    FPageIsEmpty := False;
  end;


  if not Assigned(ABand) then
    ABand := Rep.ReportForm.Bands[i];

  if ABandType = rbtPageStyle then
  begin
    j := FCurrentPosition;
    RenderingPaper.MoveCurrentPosition(0);
    ABand.Print(Self);
    RenderingPaper.MoveCurrentPosition(j);
    Exit;
  end;

  if (FRenderingPaper.TopMargin + ABand.Height > FRenderingPaper.BottomLimit) and not (ABand.Cutable or ABand.AutoHeight) then
    raise ErwException.CreateFmt(ResErrNeverEndingPrintLoop, [ABand.Name]);

  if (ABand.AutoHeight or ABand.Cutable and (RenderingPaper.CurrentTop+ABand.Height > RenderingPaper.BottomLimit)) and
      not Assigned(FCurrentAutosizeBand) then
  begin

    FCurrentAutosizeBand := ABand;
    FCurrentPosition := RenderingPaper.CurrentTop;
    FLastControlIndex := RenderingPaper.ControlCount;
    ABand.Print(Self);
    FBufList.Clear;
    while FLastControlIndex < RenderingPaper.ControlCount do
    begin
      FBufList.Add(RenderingPaper.Controls[FLastControlIndex]);
      TControl(FBufList[FBufList.Count-1]).Parent := nil;
      RenderingPaper.RemoveComponent(TControl(FBufList[FBufList.Count-1]));
    end;
    h1 := ABand.Height;
    PrintBand(ABandType, ABand);
    ABand.Height := h1;
    Exit;
  end;

  if not PageFooterPrinted and PageHeaderPrinted and HeaderPrinted and
    ((RenderingPaper.CurrentTop+ABand.Height) > RenderingPaper.BottomLimit) and (ABandType <> rbtFooter) then
    if not PageFooterPrinted  then
    begin
      if FNeedGoToNewPage then
          raise ErwException.CreateFmt(ResErrNeverEndingPrintLoop, [ABand.Name]);

      n := 0;
      j := Rep.ReportForm.IndexOfBand(rbtPageHeader);
      if j <> -1 then
        n := Rep.ReportForm.Bands[j].Height;
      j := Rep.ReportForm.IndexOfBand(rbtHeader);
      if j <> -1 then
        n := n + Rep.ReportForm.Bands[j].Height;

      fl_cut := ABand.Cutable or ABand.AutoHeight and (RenderingPaper.TopMargin + n + ABand.Height > RenderingPaper.BottomLimit);

      n := 0;

      if fl_cut then
        if FBufList.Count > 0 then
        begin
          d := FCurrentPosition-RenderingPaper.CurrentTop;
//          if d < 0 then d := d;   ?????????
          j := 0;
          n := FBufList.Count;
          m := RenderingPaper.CurrentTop;
          l := RenderingPaper.BottomLimit;
          while j < FBufList.Count do
          begin
            C := TControl(FBufList[j]);
            if C.Top-d < RenderingPaper.BottomLimit then
              if C.Top-d+C.Height <= RenderingPaper.BottomLimit then
              else if C is TGraphicFrame then
              begin
                SplitComp := TGraphicFrame.Create(nil);
                FBufList.Insert(j+1, SplitComp);
                SplitComp.Assign(C);
                TGraphicFrame(C).BoundLines := TGraphicFrame(C).BoundLines - [rclBottom];
                C.Height := RenderingPaper.BottomLimit - C.Top+d;
                SplitComp.Tag := 777;
                SplitComp.Height := SplitComp.Height-(RenderingPaper.BottomLimit-SplitComp.Top+d);
                SplitComp.Top := RenderingPaper.BottomLimit+d;
                TGraphicFrame(SplitComp).BoundLines := TGraphicFrame(SplitComp).BoundLines - [rclTop];
              end
              else
              begin
                if l > C.Top-d then
                  l := C.Top-d;
                C := nil;
              end
            else
            begin
              if l > C.Top-d then
                l := C.Top-d;
              C := nil;
            end;

            if Assigned(C) then
            begin
              RePrintComp(C, d);
              FBufList.Delete(j);
              if m < C.Top+C.Height then
                m := C.Top+C.Height;
            end
            else
              j := j+1;
          end;

          ABand.Height := ABand.Height-(l-RenderingPaper.CurrentTop);
          FCurrentPosition := l+d;

          for j := 0 to FBufList.Count-1 do
          begin
            C := TControl(FBufList[j]);
            if (C.Tag = 777) and (C is TGraphicFrame) then
            begin
              C.Height := C.Height+(C.Top-FCurrentPosition);
              C.Top := FCurrentPosition;
            end;
          end;

          RenderingPaper.MoveCurrentPosition(RenderingPaper.BottomLimit-RenderingPaper.CurrentTop);
        end;

      if fl_cut then
        if FBufList.Count < n then
          FNeedGoToNewPage := False
        else
          FNeedGoToNewPage := True
      else
        FNeedGoToNewPage := True;

      if not FooterPrinted then
      begin
        FooterPrinted := True;
        PrintBand(rbtFooter);
      end;
      SkipPage;
      PrintBand(ABandType, ABand);
      FNeedGoToNewPage := False;
      Exit;
    end;

  if not HeaderPrinted then
  begin
    HeaderPrinted := True;
    PrintBand(rbtHeader);
    PrintBand(ABandType, ABand);
    Exit;
  end;

  if not TitlePrinted then
  begin
    TitlePrinted := True;
    PrintBand(rbtTitle);
    PrintBand(ABandType, ABand);
    Exit;
  end;

  if not PageHeaderPrinted then
  begin
    PageHeaderPrinted := True;
    PrintBand(rbtPageStyle);
    PrintBand(rbtPageHeader);
    PrintBand(ABandType, ABand);
    Exit;
  end;

//  if ABandType in [rbtDetail, rbtSummary, rbtGroupHeader, rbtGroupFooter, rbtSubDetail, rbtSubSummary] then

  if not FooterPrinted then
  begin
    i := Rep.ReportForm.IndexOfBand(rbtFooter);
    if (i <> -1) then
      if (RenderingPaper.CurrentTop+ABand.Height+Rep.ReportForm.Bands[i].Height) >= RenderingPaper.BottomLimit then
      begin
        FooterPrinted := True;
        PrintBand(rbtFooter);
        PrintBand(ABandType, ABand);
        Exit;
      end;
  end;


  if (ABand.AutoHeight or ABand.Cutable) and (FCurrentAutosizeBand = ABand) then
  begin
    if FBufList.Count > 0 then
    begin
      d := FCurrentPosition-RenderingPaper.CurrentTop;
      for j := 0 to FBufList.Count-1 do
        ReprintComp(TControl(FBufList[j]), d);
      RenderingPaper.MoveCurrentPosition(ABand.Height);
      ABand.ExecuteEventsHandler('AfterPrint', []);
    end;
    FCurrentAutosizeBand := nil;
    FBufList.Clear;
  end
  else
    ABand.Print(Self);
end;


end.
