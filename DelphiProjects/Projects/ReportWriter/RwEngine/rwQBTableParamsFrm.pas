unit rwQBTableParamsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, rwQBExprPanelFrm, Mask, Math, DB,
  Grids, ISBasicClasses, rwDesignClasses, wwdblook, rwQBInfo, Wwdotdot, Wwdbcomb, wwdbedit,
  Wwdbspin, Wwdbigrd, Wwdbgrid, rwDB, rwEngineTypes, rwDataDictionary, rwBasicUtils,
  rwQBExprEditorFrm, rwInputFormControls;

type
  TrwQBTableParams = class(TForm)
    lvParams: TListView;
    pcParam: TPageControl;
    tsConst: TTabSheet;
    tsParam: TTabSheet;
    tsQBExpression: TTabSheet;
    ExprPnl: TrwQBExprPanel;
    Splitter1: TSplitter;
    nbConstType: TNotebook;
    Label1: TLabel;
    rgrBoolValue: TRadioGroup;
    cbParamList: TisRWDBComboBox;
    Label4: TLabel;
    tsNull: TTabSheet;
    Panel1: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    cbLookup: TisRWDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tsNullShow(Sender: TObject);
    procedure tsQBExpressionShow(Sender: TObject);
    procedure tsConstShow(Sender: TObject);
    procedure cbLookupExit(Sender: TObject);
    procedure rgrBoolValueExit(Sender: TObject);
    procedure lvParamsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ExprPnlpnlCanvasClick(Sender: TObject);
    procedure tsParamShow(Sender: TObject);
    procedure cbParamListExit(Sender: TObject);
  private
    FTable: TrwQBSelectedTable;
    FParams: TrwQBExpressions;
    FDSLookup: TrwDataSet;
    FCurrentParam: TrwDataDictParam;
    FCurrentParamContent: TrwQBExpression;
    FgrLookupList: TrwLookupValuesGrid;
    procedure FillParams;
    function  DSLookup: TrwDataSet;
    procedure SyncValue;
    procedure SyncConstPage;
    procedure OnLookupListExit(Sender: TObject);
  public
  end;

  function QBVirtTableParams(ATable: TrwQBSelectedTable): Boolean;

implementation

{$R *.dfm}

uses
  rwQueryBuilderFrm;


function QBVirtTableParams(ATable: TrwQBSelectedTable): Boolean;
var
  Frm: TrwQBTableParams;
begin
  Frm := TrwQBTableParams.Create(Application);
  with Frm do
    try
      FTable := ATable;
      FillParams;
      if lvParams.Items.Count > 0 then
      begin
        Result := ShowModal = mrOK;
        if Result then
          FTable.TableParams.Assign(FParams);
      end
      else
        Result := False;
    finally
      Free;
    end;
end;

{ TrwQBTableParams }

function TrwQBTableParams.DSLookup: TrwDataSet;
begin
  if not Assigned(FDSLookup) or not Boolean(FDSLookup.PActive) then
  begin
    FreeAndNil(FDSLookup);
    if Assigned(FCurrentParam) then
      FDSLookup := CreateLookupList(FTable.lqObject, FCurrentParam.Name, True);
  end;

  Result := FDSLookup;
end;

procedure TrwQBTableParams.FillParams;
var
  i: Integer;
  t, h: String;
  VT: TrwDataDictTable;
  ParStr: String;
  LI: TListItem;

  function ReadParamValue: String;
  begin
    Result := '';
    ParStr := Trim(ParStr);
    if ParStr = '' then
      Exit;

    if ParStr[1] = '''' then
    begin
      Delete(ParStr, 1, 1);
      Result := GetNextStrValue(ParStr, '''');
      GetNextStrValue(ParStr, ',');
    end
    else
      Result := Trim(GetNextStrValue(ParStr, ','));
  end;

begin
  pcParam.Tag := 1;

  VT := FTable.lqObject;
  if not Assigned(VT) then
    Exit;

  FParams.Assign(FTable.TableParams);

  if VT.DisplayName = '' then
    h := VT.Name
  else
    h := VT.DisplayName;
  Caption := 'Parameters of "' + h + '"';

  for i := 0 to FParams.Count - 1 do
  begin
    with VT.Params[i] do
    begin
      if DisplayName = '' then
        h := Name
      else
        h := DisplayName;

      case ParamType of
        ddtDateTime:  t := 'Date';
        ddtInteger:   t := 'Integer';
        ddtFloat:     t := 'Float';
        ddtCurrency:  t := 'Currency';
        ddtBoolean:   t := 'Boolean';
        ddtString:    t := 'String';
        ddtArray:     t := 'Array';
      else
        t := 'Unknown';
      end;
    end;

    LI := lvParams.Items.Add;
    LI.Data := VT.Params[i];
    LI.Caption := h;
    LI.SubItems.Add(t);
  end;

  pcParam.Tag := 0;
end;

procedure TrwQBTableParams.FormCreate(Sender: TObject);
var
  P: TPage;
  i: Integer;
  ParamName, ParamDescr: String;
begin
  FParams := TrwQBExpressions.Create(TrwQBExpression);
  ExprPnl.ReadOnly := True;
  ExprPnl.ReadOnly := True;

  P := nil;
  for i := 0 to nbConstType.ControlCount - 1 do
    if TPage(nbConstType.Controls[1]).Caption = 'List' then
    begin
      P := TPage(nbConstType.Controls[1]);
      break;
    end;

  FgrLookupList := TrwLookupValuesGrid.Create(nil);
  TwwDBGrid(FgrLookupList.VisualControl.RealObject).OnExit := OnLookupListExit;
  FgrLookupList.MultiSelection := True;
  FgrLookupList.Align := alClient;
  FgrLookupList.KeyField := '_key';
  FgrLookupList.VisualControl.RealObject.Parent := P;  

//  Prepare External Params
  if Assigned(FQBFrm.ExternalParams) then
  begin
    cbParamList.Items.Clear;
    for i := 0 to FQBFrm.ExternalParams.Count - 1 do
    begin
      ParamName := FQBFrm.ExternalParams[i].ParamName;
      ParamDescr := FQBFrm.ExternalParams[i].ParamDisplayName;
      if ParamDescr = '' then
        ParamDescr := ParamName;
      cbParamList.Items.Add(ParamDescr + #9 + ParamName);
    end;
  end;
end;

procedure TrwQBTableParams.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FDSLookup);
  FreeAndNil(FParams);
end;

procedure TrwQBTableParams.SyncValue;
var
  FirstItem: TrwQBExpressionItem;
  ActPage: TTabSheet;
begin
  pcParam.Tag := 1;

  try
    if Assigned(FCurrentParam) then
    begin
      pcParam.Visible := True;

      if (FCurrentParamContent.Content.Count = 1) or
         (FCurrentParamContent.Content.Count > 1) and (FCurrentParamContent.Items[0].ItemType = eitBracket) and (FCurrentParamContent.Items[0].Value = '[')
         then
      begin
        FirstItem := TrwQBExpressionItem(FCurrentParamContent.Content.Items[0]);
        if FirstItem.ItemType = eitNull then
          ActPage := tsNull
        else if FirstItem.ItemType = eitParam then
          ActPage := tsParam
        else
          ActPage := tsConst;
      end
      else
        ActPage := tsQBExpression;

      pcParam.ActivePage := ActPage;

      if ActPage = tsConst then
        SyncConstPage

      else if ActPage = tsParam then
      begin
        cbParamList.Value := FCurrentParamContent.Items[0].Value;
        if cbParamList.Value = '' then
          cbParamList.Text := FCurrentParamContent.Items[0].Value;
      end;
    end

    else
      pcParam.Visible := False;

  finally
    pcParam.Tag := 0;
  end;      
end;

procedure TrwQBTableParams.tsNullShow(Sender: TObject);
begin
  if (pcParam.Tag = 0) and Assigned(FCurrentParamContent) then
  begin
    FCurrentParamContent.Clear;
    FCurrentParamContent.AddItem(eitNull, 'Null', 'Null');
  end;
end;

procedure TrwQBTableParams.tsQBExpressionShow(Sender: TObject);
begin
  ExprPnl.Expression := FCurrentParamContent;
end;

procedure TrwQBTableParams.tsConstShow(Sender: TObject);

  procedure InitialValue;
  var
    t: TrwQBExprItemType;
    v: Variant;
    dscr: String;
  begin
    dscr := '';

    if FCurrentParam.ParamType = ddtArray then
    begin
      FCurrentParamContent.AddItem(eitBracket, '[', '');
      FCurrentParamContent.AddItem(eitBracket, ']', '');
      Exit;
    end

    else if FCurrentParam.ParamType = ddtInteger then
    begin
      t := eitIntConst;
      v := StrToInt(FCurrentParam.DefaultValue);
    end

    else if FCurrentParam.ParamType = ddtCurrency then
    begin
      t := eitFloatConst;
      v := StrToCurr(FCurrentParam.DefaultValue);
    end

    else if FCurrentParam.ParamType = ddtFloat then
    begin
      t := eitFloatConst;
      v := StrToFloat(FCurrentParam.DefaultValue);
    end

    else if FCurrentParam.ParamType = ddtDateTime then
    begin
      t := eitDateConst;
      v := FCurrentParam.DefaultValue;
    end

    else if FCurrentParam.ParamType = ddtBoolean then
    begin
      if FCurrentParam.DefaultValue = 'Y' then
      begin
        t := eitTrue;
        v := '''Y''';
        dscr := 'Yes';
      end
      else
      begin
        t := eitFalse;
        v := '''N''';
        dscr := 'No';
      end;
    end

    else
    begin
      t := eitStrConst;
      v := FCurrentParam.DefaultValue;
    end;

    FCurrentParamContent.Clear;
    FCurrentParamContent.AddItem(t, v, dscr);
  end;

begin
  if (pcParam.Tag = 0) and Assigned(FCurrentParamContent) then
  begin
    FCurrentParamContent.Clear;
    InitialValue;
    SyncConstPage;
  end;
end;

procedure TrwQBTableParams.SyncConstPage;
var
  ConstPage: String;

  procedure PrepareLookup;
  begin
    FgrLookupList.DataSource := nil;
    cbLookup.Items.Clear;

    DSLookup;

    if Assigned(FDSLookup) then
    begin
      if ConstPage = 'List' then
        FgrLookupList.DataSource := FDSLookup

      else
      begin
        cbLookup.Items.Capacity := FDSLookup.PRecordCount;
        FDSLookup.PFirst;
        while not FDSLookup.PEof do
        begin
          cbLookup.Items.Add(FDSLookup.Fields[1].Value + #9 + FDSLookup.Fields[0].Value);
          FDSLookup.PNext;
        end;
      end;
    end;
  end;


  procedure SyncArray;
  var
    i, j: Integer;
    ParList: Variant;
  begin
    if FCurrentParamContent.Count <= 2 then
      Exit;

    ParList := VarArrayCreate([0, FCurrentParamContent.Count - 3], varVariant);
    i := 1;
    j := 0;
    while not((FCurrentParamContent.Items[i].ItemType = eitBracket) and (FCurrentParamContent.Items[i].Value = ']')) do
    begin
      ParList[j] := FCurrentParamContent.Items[i].Value;
      while not((FCurrentParamContent.Items[i].ItemType = eitBracket) and (FCurrentParamContent.Items[i].Value = ']')) and
            not((FCurrentParamContent.Items[i].ItemType = eitSeparator) and (FCurrentParamContent.Items[i].Value = ',')) do
        Inc(i);

      if (FCurrentParamContent.Items[i].ItemType = eitBracket) and (FCurrentParamContent.Items[i].Value = ']') then
        break;

      Inc(i);
      Inc(j);
    end;

    FgrLookupList.ParamValue := ParList;
  end;

begin
  if FCurrentParam.ParamType = ddtBoolean then
    ConstPage := 'Boolean'
  else if FCurrentParam.ParamType = ddtArray then
    ConstPage := 'List'
  else
    ConstPage := 'StringAndDate';

  PrepareLookup;

  nbConstType.ActivePage := ConstPage;

  if ConstPage = 'List' then
    SyncArray

  else if ConstPage = 'Boolean' then
  begin
    if FCurrentParamContent.Items[0].Value = 'Y' then
      rgrBoolValue.ItemIndex := 0
    else
      rgrBoolValue.ItemIndex := 1;
  end

  else
  begin
    cbLookup.Value := FCurrentParamContent.Items[0].Value;
    if cbLookup.Value = '' then
      cbLookup.Text := FCurrentParamContent.Items[0].Value;
  end;
end;

procedure TrwQBTableParams.cbLookupExit(Sender: TObject);
var
  t: TrwQBExprItemType;
  v: Variant;
  TxtValue: String;
begin
  if cbLookup.Value = '' then
    TxtValue := cbLookup.Text
  else
    TxtValue := cbLookup.Value;

  if FCurrentParam.ParamType = ddtInteger then
  begin
    t := eitIntConst;
    v := Round(StrToFloat(Trim(TxtValue)));
  end

  else if FCurrentParam.ParamType = ddtCurrency then
  begin
    t := eitFloatConst;
    v := RoundTo(StrToFloat(Trim(TxtValue)), -2);
  end

  else if FCurrentParam.ParamType = ddtFloat then
  begin
    t := eitFloatConst;
    v := StrToFloat(Trim(TxtValue));
  end

  else if FCurrentParam.ParamType = ddtDateTime then
  begin
    t := eitDateConst;
    v := Trim(TxtValue);
  end

  else
  begin
    t := eitStrConst;
    v := TxtValue;
  end;

  FCurrentParamContent.Clear;
  FCurrentParamContent.AddItem(t, v, '');
end;

procedure TrwQBTableParams.rgrBoolValueExit(Sender: TObject);
begin
  FCurrentParamContent.Clear;

  if rgrBoolValue.ItemIndex = 0 then
    FCurrentParamContent.AddItem(eitTrue, '''Y''', 'Yes')
  else
    FCurrentParamContent.AddItem(eitFalse, '''N''', 'No');
end;

procedure TrwQBTableParams.lvParamsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  if Assigned(lvParams.Selected) then
  begin
    FCurrentParam := TrwDataDictParam(lvParams.Selected.Data);
    FCurrentParamContent := TrwQBExpression(FParams.Items[FCurrentParam.Index]);
  end
  else
  begin
    FCurrentParam := nil;
    FCurrentParamContent := nil;
  end;

  FreeAndNil(FDSLookup);
  SyncValue;
end;

procedure TrwQBTableParams.FormShow(Sender: TObject);
begin
  lvParams.Selected := lvParams.Items[0];
end;

procedure TrwQBTableParams.ExprPnlpnlCanvasClick(Sender: TObject);
begin
  if TrwQBExprEditor.ShowEditor(FCurrentParamContent, eitStrConst, '', DSLookup) then
    ExprPnl.ReDraw;
end;


procedure TrwQBTableParams.tsParamShow(Sender: TObject);
begin
  if (pcParam.Tag = 0) and Assigned(FCurrentParamContent) then
  begin
    FCurrentParamContent.Clear;
    FCurrentParamContent.AddItem(eitParam, 'SomeReportParameter', '');
  end;
end;

procedure TrwQBTableParams.cbParamListExit(Sender: TObject);
var
  par, dscr: String;
begin
  if cbParamList.Value = '' then
  begin
    par := Trim(cbParamList.Text);
    dscr := '';
  end
  else
  begin
    par := cbParamList.Value;
    dscr := cbParamList.Text;
  end;

  FCurrentParamContent.Clear;
  FCurrentParamContent.AddItem(eitParam, par, dscr);
end;

procedure TrwQBTableParams.OnLookupListExit(Sender: TObject);
var
  Gr: TwwDBGrid;
  i: Integer;
  BM: TBookmark;
begin
  FCurrentParamContent.Clear;
  FCurrentParamContent.AddItem(eitBracket, '[', '');

  Gr := TwwDBGrid(FgrLookupList.VisualControl.RealObject);
  FDSLookup.PDisableControls;
  BM := FDSLookup.VCLDataSet.GetBookmark;
  try
    for i := 0 to Gr.SelectedList.Count - 1 do
    begin
      if FDSLookup.VCLDataSet.BookmarkValid(TBookmark(Gr.SelectedList[i])) then
      begin
        FDSLookup.VCLDataSet.GotoBookmark(TBookmark(Gr.SelectedList[i]));
        FCurrentParamContent.AddItem(eitStrConst, String(FDSLookup.Fields[0].Value), FDSLookup.Fields[1].Value);
        if i < Gr.SelectedList.Count - 1 then
          FCurrentParamContent.AddItem(eitSeparator, ',', '');
      end;
    end;

    FDSLookup.VCLDataSet.GotoBookmark(BM);
  finally
    FDSLookup.VCLDataSet.FreeBookmark(BM);
    FDSLookup.PEnableControls;
  end;

  FCurrentParamContent.AddItem(eitBracket, ']', '');
end;

end.

