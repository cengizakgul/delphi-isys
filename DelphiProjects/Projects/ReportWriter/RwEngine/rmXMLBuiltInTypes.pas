unit rmXMLBuiltInTypes;

interface

uses SysUtils,
     rmXMLFormControls, rmTypes, rwBasicUtils;

type
  // =================== Built-in Primitive Types ========================


  // duration
  TrmXMLduration = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // dateTime
  TrmXMLdateTime = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // time
  TrmXMLtime = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // date
  TrmXMLdate = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // gYearMonth
  TrmXMLgYearMonth = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // gYear
  TrmXMLgYear = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // gMonthDay
  TrmXMLgMonthDay = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // gDay
  TrmXMLgDay = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // gMonth
  TrmXMLgMonth = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // string
  TrmXMLstring = class(TrmXMLSimpleTypeByRestriction)
  end;

  // Boolean
  TrmXMLBoolean = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
    function  GetText: String; override;
  end;


  // Base64Binary
  TrmXMLBase64Binary = class(TrmXMLSimpleTypeByRestriction)
  end;


  // hexBinary
  TrmXMLhexBinary = class(TrmXMLSimpleTypeByRestriction)
  end;


  // float
  TrmXMLfloat = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // decimal
  TrmXMLdecimal = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // double
  TrmXMLdouble = class(TrmXMLSimpleTypeByRestriction)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // anyURI
  TrmXMLanyURI = class(TrmXMLSimpleTypeByRestriction)
  end;

  // QName
  TrmXMLQName = class(TrmXMLSimpleTypeByRestriction)
  end;


  // NOTATION
  TrmXMLNotation = class(TrmXMLSimpleTypeByRestriction)
  end;


  // =================== Built-in Derived Types ========================

  // normalizedString
  TrmXMLnormalizedString = class(TrmXMLString)
  end;


  // integer
  TrmXMLinteger = class(TrmXMLDecimal)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // token
  TrmXMLtoken = class(TrmXMLnormalizedString)
  end;


  // language
  TrmXMLlanguage = class(TrmXMLtoken)
  end;


  // Name
  TrmXMLName = class(TrmXMLtoken)
  end;


  // NCName
  TrmXMLNCName = class(TrmXMLName)
  end;


  // ID
  TrmXMLID = class(TrmXMLNCName)
  end;


  // IDREF
  TrmXMLIDREF = class(TrmXMLNCName)
  end;


  // ENTITY
  TrmXMLENTITY = class(TrmXMLNCName)
  end;


  // nonPositiveInteger
  TrmXMLnonPositiveInteger = class(TrmXMLInteger)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // negativeInteger
  TrmXMLnegativeInteger = class(TrmXMLnonPositiveInteger)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // long
  TrmXMLlong = class(TrmXMLinteger)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // int
  TrmXMLint = class(TrmXMLlong)
  end;


  // short
  TrmXMLshort = class(TrmXMLint)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // byte
  TrmXMLbyte = class(TrmXMLshort)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // nonNegativeInteger
  TrmXMLnonNegativeInteger = class(TrmXMLinteger)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // positiveInteger
  TrmXMLpositiveInteger = class(TrmXMLnonNegativeInteger)
  protected
    function  ApplyValueRestrictions(const AValue: Variant): Variant; override;
  end;


  // unsignedLong
  TrmXMLunsignedLong = class(TrmXMLnonNegativeInteger)
  end;


  // unsignedInt
  TrmXMLunsignedInt = class(TrmXMLunsignedLong)
  end;


  // unsignedShort
  TrmXMLunsignedShort = class(TrmXMLunsignedInt)
  end;


  // unsignedByte
  TrmXMLunsignedByte = class(TrmXMLunsignedShort)
  end;


implementation

uses Variants, DateUtils;

{ TrmXMLduration }

function TrmXMLduration.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varDouble);
end;

function TrmXMLduration.GetText: String;
var
  lHours, lMinute, lSecond, lMSecond, d, y, m: Word;
begin
  DecodeTime(TimeOf(Value), lHours, lMinute, lSecond, lMSecond);

  d := Trunc(Value);
  y := d div 365;
  m := (d - 365 * y) div 30;
  d := d - 365 * y - 30 * m;

  Result := 'P'+ IntToStr(y) + 'Y' +
            IntToStr(m) + 'M' +
            IntToStr(d) + 'D' + 'T' +
            IntToStr(lHours) + 'H' +
            IntToStr(lMinute) + 'M' +
            IntToStr(lSecond) + 'S';

  Result := PostprocessText(Result);
end;


{ TrmXMLdateTime }

function TrmXMLdateTime.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarToDateTime(AValue);
end;

function TrmXMLdateTime.GetText: String;
begin
  Result := FormatDateTime('YYYY-MM-DD', Value) + 'T' + FormatDateTime('hh:mm:ss', Value);
  Result := PostprocessText(Result);
end;

{ TrmXMLtime }

function TrmXMLtime.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := TimeOf(VarToDateTime(AValue));
end;

function TrmXMLtime.GetText: String;
begin
  Result := FormatDateTime('hh:mm:ss', Value);
  Result := PostprocessText(Result);  
end;

{ TrmXMLdate }

function TrmXMLdate.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := DateOf(VarToDateTime(AValue));
end;

function TrmXMLdate.GetText: String;
begin
  Result := FormatDateTime('YYYY-MM-DD', Value);
  Result := PostprocessText(Result);  
end;

{ TrmXMLgYearMonth }

function TrmXMLgYearMonth.ApplyValueRestrictions(const AValue: Variant): Variant;
var
  lYear, lMonth, lDay: Word;
begin
  DecodeDate(VarToDateTime(AValue), lYear, lMonth, lDay);
  Result := EncodeDate(lYear, lMonth, 1);
end;

function TrmXMLgYearMonth.GetText: String;
begin
  Result := FormatDateTime('YYYY-MM', Value);
  Result := PostprocessText(Result);  
end;

{ TrmXMLgYear }

function TrmXMLgYear.ApplyValueRestrictions(const AValue: Variant): Variant;
var
  lYear, lMonth, lDay: Word;
begin
  DecodeDate(VarToDateTime(AValue), lYear, lMonth, lDay);
  Result := EncodeDate(lYear, 1, 1);
end;

function TrmXMLgYear.GetText: String;
begin
  Result := FormatDateTime('YYYY', Value);
  Result := PostprocessText(Result);  
end;

{ TrmXMLgMonthDay }

function TrmXMLgMonthDay.ApplyValueRestrictions(const AValue: Variant): Variant;
var
  lYear, lMonth, lDay: Word;
begin
  DecodeDate(VarToDateTime(AValue), lYear, lMonth, lDay);
  Result := EncodeDate(1900, lMonth, lDay);  //1900 is a leap-year
end;

function TrmXMLgMonthDay.GetText: String;
begin
  Result := FormatDateTime('--MM-DD', Value);
  Result := PostprocessText(Result);  
end;

{ TrmXMLgDay }

function TrmXMLgDay.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if (Result < 1) or (Result > 31) then
    rwError('Day number should be between 1 and 31');
end;

function TrmXMLgDay.GetText: String;
begin
  Result := '---' + FormatFloat('00', Value);
  Result := PostprocessText(Result);
end;

{ TrmXMLgMonth }

function TrmXMLgMonth.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if (Result < 1) or (Result > 12) then
    rwError('Month number should be between 1 and 12');
end;

function TrmXMLgMonth.GetText: String;
begin
  Result := '--' + FormatFloat('00', Value) + '--';
  Result := PostprocessText(Result);
end;

{ TrmXMLBoolean }

function TrmXMLBoolean.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varBoolean);
end;

function TrmXMLBoolean.GetText: String;
begin
  if Value then
    Result := 'true'
  else
    Result := 'false';
  Result := PostprocessText(Result);      
end;

{ TrmXMLfloat }

function TrmXMLfloat.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varDouble);
end;

{ TrmXMLdecimal }

function TrmXMLdecimal.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varDouble);
end;

{ TrmXMLdouble }

function TrmXMLdouble.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varDouble);
end;

{ TrmXMLinteger }

function TrmXMLinteger.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
end;


{ TrmXMLnonPositiveInteger }

function TrmXMLnonPositiveInteger.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if Result > 0 then
    rwError('Value should be less than or equal to zero');
end;

{ TrmXMLnegativeInteger }

function TrmXMLnegativeInteger.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if Result >=0 then
    rwError('Value should be less than zero');
end;

{ TrmXMLlong }

function TrmXMLlong.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInt64);
end;

{ TrmXMLshort }

function TrmXMLshort.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varSmallint);
end;

{ TrmXMLbyte }

function TrmXMLbyte.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varShortInt);
end;

{ TrmXMLnonNegativeInteger }

function TrmXMLnonNegativeInteger.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if Result < 0 then
    rwError('Value should be greater than or equal to zero');
end;

{ TrmXMLpositiveInteger }

function TrmXMLpositiveInteger.ApplyValueRestrictions(const AValue: Variant): Variant;
begin
  Result := VarAsType(AValue, varInteger);
  if Result <= 0 then
    rwError('Value should be greater than zero');
end;

end.
