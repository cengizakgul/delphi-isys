unit rwTypes;

interface

uses Messages, Windows, Forms, Classes, Sysutils, TypInfo, RTLConsts,
     Graphics, Controls, rwEngineTypes, rwCustomDataDictionary, rmTypes,
     rwBasicUtils, Variants, Dialogs, EvStreamUtils;


//const
//  RWENGINE_MUTEX_NAME = 'RW Engine #';

type
  TrwNotifyOperation = (rnoDestroy, rnoDataSetChanged, rnoLayoutChanged);

  TrwBandType = (rbtPageHeader, rbtTitle, rbtHeader, rbtGroupHeader, rbtDetail,
    rbtGroupFooter, rbtFooter, rbtSummary, rbtPageFooter, rbtPageStyle,
    rbtSubDetail, rbtSubSummary, rbtCustom);

  TPropNamesArray = array of ShortString;
  PTPropNamesArray = ^TPropNamesArray;

  {TrwVariable is descriptor of variable}

  TrwVariable = record
    Name:        ShortString;
    VarType:     TrwVarTypeKind;
    PointerType: String;
    Reference:   Boolean;
  end;


  TrwDomainItem = record
    Value: Variant;
    Description: String;
  end;

  TrwDomain = array of TrwDomainItem;
  TrwDomainList = array of TrwDomain;

  PTrwVariable = ^TrwVariable;

  TrwPaperOrientation = (rpoPortrait, rpoLandscape);

  TReportType = (rtUnknown, rtReport, rtCheck, rtASCIIFile, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR);
  TrwReportDestination = (rdtNone, rdtCancel, rdtPrinter, rdtPreview, rdtVMR, rdtPrepareVMR);

  TrwPaperSize = (psLetter, psTabloid, psLegal,
    psStatement, psExecutive, psA3, psA4, psA5, psB4, psB5, psFolio,
    psQuarto, ps10X14, ps11X17, psNote, psEnv9, psEnv10, psEnv11, psEnv12,
    psEnv14, psCSheet, psDSheet, psESheet, psCustom);

  TrwTypeSysInfo = (rsiPageNum, rsiDate, rsiTime, rsiDateTime);

  TrwDataType = (rdtNull, rdtArray, rdtBoolean, rdtDate, rdtFloat, rdtInteger,
    rdtString);

  TrwReportServerResultType = (srtSuccesful, srtError);

  TrwLettersCaseType = (rwNoChange, rwUpperCase, rwLowerCase);

  TrwPaperRec = record
    Name: string;
    Width: Word;
    Height: Word;
  end;


  TrwStrings = class(TStringList)
  protected
    procedure DefineProperties(Filer: TFiler); override;
  published
    property Text stored False;

    function  PAdd(AText: Variant): Variant;
    procedure PDelete(AIndex: Variant);
    function  PIndexOf(AText: Variant): Variant;
    procedure PSetLine(AIndex: Variant; AText: Variant);
    function  PGetLine(AIndex: Variant): Variant;
    function  PCount: Variant;
  end;


  TrwPersistent = class(TPersistent, IrmInterfacedObject)
  private
    FOwner: TPersistent;
    FOnChange: TNotifyEvent;
    FUpdateCont: Integer;
  protected
    // Interface
    function  _AddRef: Integer; stdcall;
    function  _Release: Integer; stdcall;
    function   QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function  VCLObject: TObject;
    // End

    procedure Changed; virtual;
    function  GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TPersistent); virtual;
    procedure  BeginUpdate;
    procedure  EndUpdate;

    procedure Assign(Source: TPersistent); override;
    property  Owner: TPersistent read FOwner write FOwner;
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;


  TrwCollectionItem = class(TInterfacedCollectionItem, IrwCollectionItem)
  private
    FInheritedItem: Boolean;
    FItemID: String;
    procedure ReadItemID(Reader: TReader);
    procedure WriteItemID(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
    function  IsNotInherited: Boolean;
    function  AsAncestor(const AncestorItem: TrwCollectionItem): Boolean; virtual;  // for streaming
    procedure ApplyDelta(const DeltaCollectionItem: TrwCollectionItem); virtual;

    //interface
    function  VCLObject: TObject;
    function  RealObject: TCollectionItem;
    function  GetCollection: IrwCollection;    
    function  GetItemIndex: Integer;
    procedure SetItemIndex(const AIndex: Integer);
    function  GetItemID: String;
    function  IsInheritedItem: Boolean;

  public
    property ItemID: String read GetItemID;
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
    procedure GenerateNewID; virtual;
  published
    property InheritedItem: Boolean read FInheritedItem write FInheritedItem default False;

    procedure PAssign(ASource: Variant);
  end;


  TrwCollectionItemClass = class of TrwCollectionItem;

  
  TrwCollection = class(TInterfacedCollection, IrwCollection)
  private
    FOwner: TComponent;
    
  protected
    function  GetOwner: TPersistent; override;

    //interface
    function  VCLObject: TObject;
    function  RealObject: TCollection;    
    function  GetItemCount: Integer;
    function  GetItemByIndex(const AItemIndex: Integer): IrwCollectionItem;
    function  GetItemByID(const AItemID: String): IrwCollectionItem;
    function  GetComponentOwner: IrmDesignObject;
    function  AddItem: IrwCollectionItem;
    procedure DeleteItem(const AItemIndex: Integer); virtual;

  public
    constructor Create(AOwner: TComponent; ItemClass: TrwCollectionItemClass);
    procedure SetOwner(AOwner: TComponent);
    function  FindItemByID(const AItemID: String): TrwCollectionItem;
    procedure SetInherited(Value: Boolean);
    procedure MakeDelta(const AncestorCollection: TrwCollection); // for streaming
    procedure ApplyDelta(const DeltaCollection: TrwCollection);
    function  GetItemOrder: String;
    procedure SetItemOrder(AItemOrder: String);

  published
    procedure PClear;
    procedure PAssign(ASource: Variant);
  end;


  TrwBorderLine = class(TrwPersistent)
  private
    FWidth: Integer;
    FColor: TColor;
    FStyle: TPenStyle;
    FVisible: Boolean;
    procedure SetWidth(const Value: Integer);
    procedure SetColor(const Value: TColor);
    procedure SetStyle(const Value: TPenStyle);
    procedure SetVisible(const Value: Boolean);
    function  LineIsVisible: Boolean;
  public
    constructor Create(AOwner: TPersistent); override;
    procedure Assign(Source: TPersistent); override;
  published
    property Visible: Boolean read FVisible write SetVisible default True;
    property Color: TColor read FColor write SetColor stored LineIsVisible default clBlack;
    property Width: Integer read FWidth write SetWidth stored LineIsVisible default 1;
    property Style: TPenStyle read FStyle write SetStyle stored LineIsVisible default psSolid;
  end;


  TrwBorderLines = class(TrwPersistent)
  private
    FTopLine: TrwBorderLine;
    FBottomLine: TrwBorderLine;
    FLeftLine: TrwBorderLine;
    FRightLine: TrwBorderLine;
    procedure SetTopLine(const Value: TrwBorderLine);
    procedure SetBottomLine(const Value: TrwBorderLine);
    procedure SetLeftLine(const Value: TrwBorderLine);
    procedure SetRightLine(const Value: TrwBorderLine);
    procedure OnChangeLine(Sender: TObject);
  public
    constructor Create(AOwner: TPersistent); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;
  published
    property TopLine: TrwBorderLine read FTopLine write SetTopLine;
    property BottomLine: TrwBorderLine read FBottomLine write SetBottomLine;
    property LeftLine: TrwBorderLine read FLeftLine write SetLeftLine;
    property RightLine: TrwBorderLine read FRightLine write SetRightLine;
  end;

     {ErwParserError parser exception class}

  ErwParserError = class(ErwException)
  private
    FLibComponentName: string;
    FComponentName: string;
    FOwnerName: string;
    FEventName: string;
    FLineNum: Integer;
    FColNum: Integer;
    FPosition: Integer;
    FGeneralMessage: string;
    FComponentPath: string;

  public
    property LibComponentName: string read FLibComponentName write FLibComponentName;
    property ComponentName: string read FComponentName write FComponentName;
    property OwnerName: string read FOwnerName write FOwnerName;
    property EventName: string read FEventName write FEventName;
    property ComponentPath: string read FComponentPath write FComponentPath;
    property Position: Integer read FPosition;
    property LineNum: Integer read FLineNum;
    property ColNum: Integer read FColNum;
    property GeneralMessage: string read FGeneralMessage;

    constructor CreateParserError(ALibComponentName: string; AComponentName: string;
      AOwnerName: string; AEventName: string; AComponentPath: string; ALineNum: Integer; AColNum: Integer;
      APosition: Integer; AMessage: string);
    constructor CreateErrorMessage(AMessage: string);
  end;

  TVarArray = array of Variant;
  PTVarArray = ^TVarArray;

  TrwColumnLine = (rclLeft, rclRight, rclTop, rclBottom);
  TrwSetColumnLines = set of TrwColumnLine;

  TrwWriter = class(TWriter)
  public
    procedure WriteProperty(Instance: TPersistent; PropInfo: Pointer);
    procedure WriteProperties(Instance: TPersistent);
  end;


  TrwReader = class(TReader)
  private
    procedure ReaderFindComponentClass(Reader: TReader; const ClassName: string; var ComponentClass: TComponentClass);
    procedure ReaderError(Reader: TReader; const Message: string; var Handled: Boolean);
  public
    constructor Create(Stream: TStream);
    procedure ReadProperty(AInstance: TPersistent);
  end;


  TTempFileStream = class(THandleStream)
  public
    constructor Create; overload;
    destructor  Destroy; override;
  end;


  TrwPicture = class(TPicture)
  published
    procedure PLoadFromArray(APicture: Variant; AFormatExt: Variant);
    function  PSaveToArray(DestArray: Variant): Variant;
  end;

const

  {Print object types}

  PO_TEXT = 'T';
  PO_PCL_TEXT = 'P';
  PO_FRAME = 'F';
  PO_SHAPE = 'S';
  PO_IMAGE = 'I';


  {Windows messages}

  RW_CHANGE_BAND_LIST = WM_USER + 100;
  RW_CHANGE_DEBUG_COMPONENT = WM_USER + 101;
  RW_CHANGE_DEBUG_FUNCTION = WM_USER + 102;
  RW_CHANGE_DEBUG_EVENT = WM_USER + 103;
  RW_CHANGE_DEBUG_POSITION = WM_USER + 104;
  RW_BREAKPOINT_FOUND = WM_USER + 105;
  RW_CHANGE_DEBUG_LIBCOMPONENT = WM_USER + 106;
  RW_INPUTFORM_ERROR = WM_USER + 107;
  RW_QBTABLE_OPEN = WM_USER + 108;
  RW_QBTABLE_CLOSE = WM_USER + 109;
  RW_QBTABLE_REFRESH = WM_USER + 110;
  RW_QBTABLE_ADDCONDITION = WM_USER + 111;
  RW_CHANGE_DEBUG_ASM_POSITION  = WM_USER + 112;
  RW_CHANGE_DEBUG_ASM_CS  = WM_USER + 113;

  {Components events}

  cEventOnPrint = 'procedure OnPrint(var Accept: Boolean)';
  cEventOnFilter = 'procedure OnFilterRecord(var Accept: Boolean)';
  cEventBeforeOpen = 'procedure BeforeOpen';
  cEventAfterOpen = 'procedure AfterOpen';
  cEventBeforeClose = 'procedure BeforeClose';
  cEventAfterClose = 'procedure AfterClose';
  cEventBeforePrint = 'procedure BeforePrint';
  cEventAfterPrint = 'procedure AfterPrint';
  cEventAfterScroll = 'procedure AfterScroll';
  cEventOnSetText = 'procedure OnSetText(var Text: string)';
  cEventOnShow = 'procedure OnShow';
  cEventOnHide = 'procedure OnHide';
  cEventOnClose = 'procedure OnClose';
  cEventOnCloseQuery = 'procedure OnCloseQuery(var CanClose: Boolean)';
  cEventOnClick = 'procedure OnClick';
  cEventOnChange = 'procedure OnChange';
  cEventOnChanging = 'procedure OnChanging(var AllowChange: Boolean)';
  cEventBeforeInsert = 'procedure BeforeInsert(var Accept: Boolean)';
  cEventBeforeEdit = 'procedure BeforeEdit(var Accept: Boolean)';
  cEventBeforeDelete = 'procedure BeforeDelete(var Accept: Boolean)';
  cEventBeforeCancel = 'procedure BeforeCancel(var Accept: Boolean)';
  cEventBeforePost = 'procedure BeforePost(var Accept: Boolean)';
  cEventAfterInsert = 'procedure AfterInsert';
  cEventAfterEdit = 'procedure AfterEdit';
  cEventAfterDelete = 'procedure AfterDelete';
  cEventAfterCancel = 'procedure AfterCancel';
  cEventAfterPost = 'procedure AfterPost';
  cEventAfterCreate = 'procedure AfterCreate';
  cEventRowChanged =  'procedure OnRowChanged';
  cEventCellChanged = 'procedure OnCellChanged';
  cEventSelectRecord = 'procedure OnSelectRecord(Selecting: Boolean; var Accept: Boolean)';
  cEventSelectionChange = 'procedure OnSelectionChange';
  cEventAfterCreateObject = 'procedure AfterCreateObject';
  cEventBeforeWriteASCIILine = 'procedure BeforeWriteASCIILine(var Text: String; var Accept: Boolean)';
  cEventBeforePrintReport = 'procedure BeforePrintReport(var Accept: Boolean)';
  cEventBeforeShowInputForm = 'procedure BeforeShowInputForm(var Accept: Boolean)';
  cEventAfterPrintReport = 'procedure AfterPrintReport';
  cEventAfterCloseInputForm = 'procedure AfterCloseInputForm';
  cEventBeforeCalculate = 'procedure BeforeCalculate(var Accept: Boolean)';
  cEventOnCalculate = 'procedure OnCalculate';
  cEventAfterCalculate = 'procedure AfterCalculate(var Accept: Boolean)';
  cEventOnStartBranch = 'procedure OnStartBranch(var Accept: Boolean)';
  cEventOnFinishBranch = 'procedure OnFinishBranch(var Accept: Boolean)';
  cEventOnFilterText = 'procedure OnFilterText(var Text: string)';
  cEventOnChangeValue = 'procedure OnChangeValue(var NewValue: Variant)';
  cEventBeforeProcessForm = 'procedure BeforeProcessForm(var AFormName: String)';
  cEventAfterProcessForm = 'procedure AfterProcessForm(AFormName: String)';


  {Width and Height in 1/10 mm !}

  cPaperInfo: array[psLetter..psCustom] of TrwPaperRec = (
    (Name: 'Letter 8 1/2 x 11 in'; Width: 2159; Height: 2794),
    (Name: 'Tabloid 11 x 17 in'; Width: 2794; Height: 4318),
    (Name: 'Legal 8 1/2 x 14 in'; Width: 2159; Height: 3556),
    (Name: 'Statement 5 1/2 x 8 1/2 in'; Width: 1397; Height: 2159),
    (Name: 'Executive 7 1/4 x 10 1/2 in'; Width: 1842; Height: 2667),
    (Name: 'A3 297 x 420 mm'; Width: 2970; Height: 4200),
    (Name: 'A4 210 x 297 mm'; Width: 2100; Height: 2970),
    (Name: 'A5 148 x 210 mm'; Width: 1480; Height: 2100),
    (Name: 'B4 (JIS) 250 x 354 mm'; Width: 2500; Height: 3540),
    (Name: 'B5 (JIS) 182 x 257 mm'; Width: 1820; Height: 2570),
    (Name: 'Folio 8 1/2 x 13 in'; Width: 2159; Height: 3302),
    (Name: 'Quarto 215 x 275 mm'; Width: 2150; Height: 2750),
    (Name: '10 x 14 in'; Width: 2540; Height: 3556),
    (Name: '10 x 17 in'; Width: 2540; Height: 4318),
    (Name: 'Note 8 12 x 11 in'; Width: 2159; Height: 2794),
    (Name: 'Envelope #9 3 7/8 x 8 7/8 in'; Width: 984; Height: 2254),
    (Name: 'Envelope #10 4 1/8 x 9 1/2 in'; Width: 1048; Height: 2413),
    (Name: 'Envelope #11 4 1/2 x 10 3/8 in'; Width: 1143; Height: 2635),
    (Name: 'Envelope #12 4 3/4 x 11 in'; Width: 1207; Height: 2794),
    (Name: 'Envelope #14 5 x 11 1/2 in'; Width: 1270; Height: 2921),
    (Name: 'C Sheet 17 x 22 in'; Width: 4318; Height: 5588),
    (Name: 'D Sheet 22 x 34 in'; Width: 5588; Height: 8636),
    (Name: 'E Sheet 33 x 44 in'; Width: 8382; Height: 11176),
    (Name: 'Custom'; Width: 2100; Height: 2970)
    );


  { Types of Command for drawing in the cells}
  NET_CELL =  'C';
  NET_SPACE = 'S';


  VAR_FUNCT_COMP_NAME = '_VariablesFunctions';

  cParserLibComp = 'LIBCOMP';
  cParserRootOwner = 'ROOTOWNER';
  cParserSelf    = 'SELF';
  cParserLibFunct    = '_LIBFUNCTIONS';


  {Data Dictionary standard groups}
  DDG_TEMPTABLES = 'Buffers';

  {SY_Storage IDs}
  RW_DATA_DICTIONARY = 'RW_DATA_DICTIONARY';
  RW_QB_TEMPLATES =    'RW_QB_TEMPLATES';
  RW_QB_CONSTANTS =    'RW_QB_CONSTANTS';
  RW_LIB_COMPONENTS =  'RW_LIB_COMPONENTS';
  RW_LIB_FUNCTIONS =   'RW_LIB_FUNCTIONS';


var
  IgnoreLoadingErrors: Boolean = False;
  RWDesigning: Boolean = False;
  rwTypeInfoList: array [rwvUnknown .. rwvVariant] of PTypeInfo;  

implementation

uses rwPreviewUtils;

{ TrwStrings }

procedure TrwStrings.DefineProperties(Filer: TFiler);
begin
  inherited;
end;

function TrwStrings.PAdd(AText: Variant): Variant;
begin
  Result := Add(AText);
end;

function TrwStrings.PCount: Variant;
begin
  Result := Count;
end;

procedure TrwStrings.PDelete(AIndex: Variant);
begin
  Delete(AIndex);
end;

function TrwStrings.PGetLine(AIndex: Variant): Variant;
begin
  Result := Strings[AIndex];
end;

function TrwStrings.PIndexOf(AText: Variant): Variant;
begin
  Result := IndexOf(AText);
end;

procedure TrwStrings.PSetLine(AIndex: Variant; AText: Variant);
begin
  Strings[AIndex] := AText;
end;


  {ErwParserError}

constructor ErwParserError.CreateErrorMessage(AMessage: string);
begin
  Message := AMessage;
  FGeneralMessage := AMessage;
end;

constructor ErwParserError.CreateParserError(ALibComponentName: string; AComponentName: string;
  AOwnerName: string; AEventName: string; AComponentPath: String; ALineNum: Integer; AColNum: Integer;
  APosition: Integer; AMessage: string);
begin
  FLibComponentName := ALibComponentName;
  FComponentName := AComponentName;
  FOwnerName := AOwnerName;
  FEventName := AEventName;
  FComponentPath := AComponentPath;
  FLineNum := ALineNum;
  FColNum := AColNum;
  FPosition := APosition;
  Message := 'Compile Error';
  if FComponentName <> '' then
    Message := Message+#13+'Event Handler: ' + FComponentPath + '.' + FEventName
  else
    Message := Message+#13+'Function: ' + FEventName;

  Message := Message+#13+'Line: '+IntToStr(FLineNum)+'  Column: '+IntToStr(FColNum)+#13+#13+AMessage;

  FGeneralMessage := AMessage;
end;


{ TrwWriter }

procedure TrwWriter.WriteProperties(Instance: TPersistent);
begin
  inherited WriteProperties(Instance);
end;

procedure TrwWriter.WriteProperty(Instance: TPersistent; PropInfo: Pointer);
begin
  inherited WriteProperty(Instance, PropInfo);
end;


{ TrwReader }

constructor TrwReader.Create(Stream: TStream);
begin
  inherited Create(Stream, 1024);
  OnFindComponentClass := ReaderFindComponentClass;
  OnError := ReaderError;  
end;

procedure TrwReader.ReaderError(Reader: TReader; const Message: string; var Handled: Boolean);
begin
  if IgnoreLoadingErrors then
  begin
    Handled := True;
    Exit;
  end;

  if RWDesigning then
    Handled := MessageDlg(Message, mtError, [mbAbort, mbIgnore], 0) = mrIgnore
  else
    Handled := False;
end;

procedure TrwReader.ReaderFindComponentClass(Reader: TReader; const ClassName: string; var ComponentClass: TComponentClass);
begin
  // This is temporary! needs to be removed in couple versions
  if AnsiSameText(ClassName, 'TrmDBTableCell') then
    ComponentClass := TComponentClass(GetClass('TrmTableCell'))
  else if AnsiSameText(ClassName, 'TrmDBSubTable') then
    ComponentClass := TComponentClass(GetClass('TrmDBTable'));
end;


procedure TrwReader.ReadProperty(AInstance: TPersistent);
begin
  inherited ReadProperty(AInstance);
end;


{ TTempFileStream }

constructor TTempFileStream.Create;
var
  Buf1: array[0..1023] of AnsiChar;
  Buf2: array[0..1023] of AnsiChar;
  H: DWORD;
begin
  if GetTempPath(1023, Buf1) = 0 then
    RaiseLastOSError;

  if GetTempFileName(Buf1, 'tmp', 0, Buf2) = 0 then
    RaiseLastOSError;

  H := Integer(CreateFile(Buf2,
                          GENERIC_READ or GENERIC_WRITE,
                          0{FILE_SHARE_READ or FILE_SHARE_WRITE},
                          nil,
                          OPEN_EXISTING,
                          FILE_ATTRIBUTE_TEMPORARY or FILE_FLAG_DELETE_ON_CLOSE,
                          0));

  if H = INVALID_HANDLE_VALUE then
    RaiseLastOSError;

  inherited Create(H);

  if DWORD(FHandle) <> H then
    raise EFOpenError.CreateResFmt(@SFOpenErrorEx,
       [ExpandFileName(String(Buf2)), SysErrorMessage(GetLastError)]);
end;


destructor TTempFileStream.Destroy;
begin
  if FHandle >= 0 then
    FileClose(FHandle);
  inherited;
end;



{ TrwPersistent }

constructor TrwPersistent.Create(AOwner: TPersistent);
begin
  FOwner := AOwner;
  FUpdateCont := 0;
end;

procedure TrwPersistent.Changed;
begin
  if Assigned(FOnChange) and (FUpdateCont = 0) then
    FOnChange(Self);
end;

function TrwPersistent.GetOwner: TPersistent;
begin
  Result := FOwner;
end;


procedure TrwPersistent.Assign(Source: TPersistent);
begin
  Changed;
end;


procedure TrwPersistent.BeginUpdate;
begin
  Inc(FUpdateCont);
end;

procedure TrwPersistent.EndUpdate;
begin
  Dec(FUpdateCont);

  if FUpdateCont = 0 then
    Changed
  else if FUpdateCont < 0 then
    FUpdateCont := 0;
end;


function TrwPersistent._AddRef: Integer;
begin
  Result := -1;
end;

function TrwPersistent._Release: Integer;
begin
  Result := -1;
end;

function TrwPersistent.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TrwPersistent.VCLObject: TObject;
begin
  Result := Self;
end;


{ TrwCollectionItem }

constructor TrwCollectionItem.Create(Collection: TCollection);
begin
  inherited;
  FInheritedItem := False;
  GenerateNewID;
end;

procedure TrwCollectionItem.Assign(Source: TPersistent);
begin
  FItemID := TrwCollectionItem(Source).ItemID;
  InheritedItem := TrwCollectionItem(Source).InheritedItem;
end;

procedure TrwCollectionItem.PAssign(ASource: Variant);
begin
  Assign(TrwCollectionItem(Pointer(Integer(ASource))));
end;

procedure TrwCollectionItem.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('ItemID', ReadItemID, WriteItemID, (ItemID <> ''));
end;

procedure TrwCollectionItem.ReadItemID(Reader: TReader);
begin
  FItemID := Reader.ReadString;
end;

procedure TrwCollectionItem.WriteItemID(Writer: TWriter);
begin
  Writer.WriteString(ItemID);
end;

procedure TrwCollectionItem.GenerateNewID;
var
  h: String;
begin
  if Assigned(Collection) then
    while True do
    begin
      h := IntToStr(Random(MaxInt));
      if TrwCollection(Collection).FindItemByID(h) = nil then
      begin
        FItemID := h;
        break;
      end;
    end;
end;

function TrwCollectionItem.GetItemID: String;
begin
  Result := FItemID;
end;

function TrwCollectionItem.IsInheritedItem: Boolean;
begin
  Result := InheritedItem;
end;

function TrwCollectionItem.GetItemIndex: Integer;
begin
  Result := Index;
end;

procedure TrwCollectionItem.SetItemIndex(const AIndex: Integer);
begin
  Index := AIndex;
end;

function TrwCollectionItem.AsAncestor(const AncestorItem: TrwCollectionItem): Boolean;
begin
  Result := False;
end;

procedure TrwCollectionItem.ApplyDelta(const DeltaCollectionItem: TrwCollectionItem);
begin
end;

function TrwCollectionItem.IsNotInherited: Boolean;
begin
  Result := not InheritedItem;
end;


function TrwCollectionItem.RealObject: TCollectionItem;
begin
  Result := Self;
end;

function TrwCollectionItem.VCLObject: TObject;
begin
  Result := Self;
end;

function TrwCollectionItem.GetCollection: IrwCollection;
begin
  Result := TrwCollection(Collection);
end;


{ TrwCollection }

function TrwCollection.FindItemByID(const AItemID: String): TrwCollectionItem;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count -1 do
    if AnsiSameText(TrwCollectionItem(Items[i]).ItemID, AItemID) then
    begin
      Result := TrwCollectionItem(Items[i]);
      break;
    end;
end;

function TrwCollection.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

procedure TrwCollection.PAssign(ASource: Variant);
begin
  Assign(TrwCollection(Pointer(Integer(ASource))));
end;

procedure TrwCollection.PClear;
begin
  Clear;
end;

procedure TrwCollection.SetInherited(Value: Boolean);
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    TrwCollectionItem(Items[i]).InheritedItem := Value;
end;

function TrwCollection.GetItemByID(const AItemID: String): IrwCollectionItem;
begin
  Result := FindItemByID(AItemID);
end;

function TrwCollection.GetComponentOwner: IrmDesignObject;
begin
  Result := FOwner as IrmDesignObject;
end;

function TrwCollection.GetItemByIndex(const AItemIndex: Integer): IrwCollectionItem;
begin
  Result := TrwCollectionItem(Items[AItemIndex]);
end;

function TrwCollection.GetItemCount: Integer;
begin
  Result := Count;
end;

function TrwCollection.AddItem: IrwCollectionItem;
begin
  Result := TrwCollectionItem(Add);
end;

procedure TrwCollection.DeleteItem(const AItemIndex: Integer);
begin
  Delete(AItemIndex);
end;

procedure TrwCollection.ApplyDelta(const DeltaCollection: TrwCollection);
var
  i: Integer;
  Itm: TrwCollectionItem;
begin
  for i := 0 to DeltaCollection.Count - 1 do
  begin
    Itm := FindItemByID(TrwCollectionItem(DeltaCollection.Items[i]).ItemID);
    if Assigned(Itm) then
      Itm.ApplyDelta(TrwCollectionItem(DeltaCollection.Items[i]))

    else if not TrwCollectionItem(DeltaCollection.Items[i]).InheritedItem then
    begin
      Itm := TrwCollectionItem(Add);
      Itm.Assign(TrwCollectionItem(DeltaCollection.Items[i]));
    end;
  end;
end;

procedure TrwCollection.MakeDelta(const AncestorCollection: TrwCollection);
var
  i: Integer;
  Itm: TrwCollectionItem;
begin
  for i := 0 to AncestorCollection.Count - 1 do
  begin
    Itm := FindItemByID(TrwCollectionItem(AncestorCollection.Items[i]).ItemID);
    if Assigned(Itm) then
    begin
      if Itm.AsAncestor(TrwCollectionItem(AncestorCollection.Items[i])) then
        FreeAndNil(Itm);
    end;
  end
end;

function TrwCollection.GetItemOrder: String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Count - 1 do
  begin
    if i > 0 then
      Result := Result + ',';
    Result := Result + TrwCollectionItem(Items[i]).ItemID;
  end;
end;

procedure TrwCollection.SetItemOrder(AItemOrder: String);
var
  i: Integer;
  Itm: TrwCollectionItem;
  h: String;
begin
  i := 0;
  while AItemOrder <> '' do
  begin
    h := GetNextStrValue(AItemOrder, ',');
    Itm := FindItemByID(h);
    if Assigned(Itm) then
    begin
      Itm.Index := i;
      Inc(i);
    end;
  end;
end;


constructor TrwCollection.Create(AOwner: TComponent; ItemClass: TrwCollectionItemClass);
begin
  FOwner := AOwner;
  inherited Create(ItemClass);
end;

procedure TrwCollection.SetOwner(AOwner: TComponent);
begin
  FOwner := AOwner;
end;

function TrwCollection.RealObject: TCollection;
begin
  Result := Self;
end;

function TrwCollection.VCLObject: TObject;
begin
  Result := Self;
end;

{ TrwBorderLine }

constructor TrwBorderLine.Create(AOwner: TPersistent);
begin
  inherited;
  FVisible := True;
  FColor := clBlack;
  FWidth := 3;
  FStyle := psSolid;
end;

procedure TrwBorderLine.Assign(Source: TPersistent);
begin
  FVisible := TrwBorderLine(Source).Visible;
  FWidth := TrwBorderLine(Source).Width;
  FColor := TrwBorderLine(Source).Color;
  FStyle := TrwBorderLine(Source).Style;

  inherited;
end;

procedure TrwBorderLine.SetColor(const Value: TColor);
begin
  FColor := Value;
  Changed;  
end;

procedure TrwBorderLine.SetWidth(const Value: Integer);
begin
  FWidth := Value;
  Changed;  
end;

procedure TrwBorderLine.SetStyle(const Value: TPenStyle);
begin
  FStyle := Value;
  Changed;
end;


procedure TrwBorderLine.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  Changed;
end;


function TrwBorderLine.LineIsVisible: Boolean;
begin
  Result := Visible;
end;

{ TrwBorderLines }

constructor TrwBorderLines.Create(AOwner: TPersistent);
begin
  inherited;
  FTopLine := TrwBorderLine.Create(Self);
  FTopLine.OnChange := OnChangeLine;

  FBottomLine := TrwBorderLine.Create(Self);
  FBottomLine.OnChange := OnChangeLine;

  FLeftLine := TrwBorderLine.Create(Self);
  FLeftLine.OnChange := OnChangeLine;

  FRightLine := TrwBorderLine.Create(Self);
  FRightLine.OnChange := OnChangeLine;
end;

destructor TrwBorderLines.Destroy;
begin
  FreeAndNil(FTopLine);
  FreeAndNil(FBottomLine);
  FreeAndNil(FLeftLine);
  FreeAndNil(FRightLine);
  inherited;
end;

procedure TrwBorderLines.Assign(Source: TPersistent);
begin
  BeginUpdate;
  try
    TopLine := TrwBorderLines(Source).TopLine;
    BottomLine := TrwBorderLines(Source).BottomLine;
    LeftLine := TrwBorderLines(Source).LeftLine;
    RightLine := TrwBorderLines(Source).RightLine;
  finally
    EndUpdate;
  end;
end;

procedure TrwBorderLines.OnChangeLine(Sender: TObject);
begin
  Changed;
end;

procedure TrwBorderLines.SetBottomLine(const Value: TrwBorderLine);
begin
  FBottomLine.Assign(Value);
end;

procedure TrwBorderLines.SetLeftLine(const Value: TrwBorderLine);
begin
  FLeftLine.Assign(Value);
end;

procedure TrwBorderLines.SetRightLine(const Value: TrwBorderLine);
begin
  FRightLine.Assign(Value);
end;

procedure TrwBorderLines.SetTopLine(const Value: TrwBorderLine);
begin
  FTopLine.Assign(Value);
end;



var
  rwUnknownType: TTypeInfo = (Kind: tkUnknown; Name: 'Unknown');
  rwArrayType: TTypeInfo = (Kind: tkArray; Name: 'Array');
  rwClassType: TTypeInfo = (Kind: tkClass; Name: 'Class');


{ TrwPicture }

procedure TrwPicture.PLoadFromArray(APicture, AFormatExt: Variant);
var
  FS: TEvFileStream;
  P: Pointer;
  l: Integer;
  FileName: String;
begin
  l := VarArrayHighBound(APicture, 1) - VarArrayLowBound(APicture, 1) + 1;
  if l = 0 then
  begin
    if Assigned(Graphic) then
      Graphic.Assign(nil);
    Exit;
  end;

  repeat
    FileName := PrvGetCachePath + 'Pict' + FormatDateTime('ddmmyyhhmmss', Now) + '_' + IntToStr(Random(MaxInt)) + '.' + AFormatExt;
  until not FileExists(FileName);

  try
    FS := TEvFileStream.Create(FileName, fmCreate);
    try
      P := VarArrayLock(APicture);
      try
        FS.Write(P^, l);
      finally
        VarArrayUnlock(APicture);
      end
    finally
      FreeAndNil(FS);
    end;

    LoadFromFile(FileName);

  finally
    DeleteFile(FileName);
  end;
end;

function TrwPicture.PSaveToArray(DestArray: Variant): Variant;
var
  FS: TEvFileStream;
  P: Pointer;
  FileName: String;
  Pict: Variant;
begin
  Result := '';

  if not Assigned(Graphic) then
    Exit;

  Result := GraphicExtension(TGraphicClass(Graphic.ClassType));

  repeat
    FileName := PrvGetCachePath + 'Pict' + FormatDateTime('ddmmyyhhmmss', Now) + '_' + IntToStr(Random(MaxInt)) + '.' + Result;
  until not FileExists(FileName);

  try
    SaveToFile(FileName);

    FS := TEvFileStream.Create(FileName, fmOpenRead);
    try
      Pict := VarArrayCreate([0, FS.Size - 1], varByte);
      P := VarArrayLock(Pict);
      try
        FS.Read(P^, FS.Size);
      finally
        VarArrayUnlock(Pict);
      end
    finally
      FreeAndNil(FS);
    end;

  finally
    DeleteFile(FileName);
  end;

  PVariant(Pointer(Integer(DestArray)))^ := Pict;
end;


initialization
  rwTypeInfoList[rwvUnknown] := @rwUnknownType;
  rwTypeInfoList[rwvInteger] := PTypeInfo(TypeInfo(Integer));
  rwTypeInfoList[rwvBoolean] := PTypeInfo(TypeInfo(Boolean));
  rwTypeInfoList[rwvString] := PTypeInfo(TypeInfo(String));
  rwTypeInfoList[rwvFloat] := PTypeInfo(TypeInfo(Extended));
  rwTypeInfoList[rwvDate] := PTypeInfo(TypeInfo(Real));
  rwTypeInfoList[rwvArray] := @rwArrayType;
  rwTypeInfoList[rwvPointer] := @rwClassType;
  rwTypeInfoList[rwvCurrency] := PTypeInfo(TypeInfo(Currency));
  rwTypeInfoList[rwvVariant] := PTypeInfo(TypeInfo(Variant));

end.
