unit rwExtendedControls;

interface

uses
  Classes, Controls, ExtCtrls, StdCtrls, Windows, Messages, CommCtrl,
  ComCtrls, Forms, SysUtils, Graphics, Grids, DB, DBGrids, DBCtrls,
  rwUtils, rwBasicClasses, rwTypes, rwDesignClasses, wwdbigrd, wwexport,
  rwBasicUtils, isBasicClasses, LMDCheckBox, LMDButton, LMDRadioGroupButton;

type
  {TExtDateTimePicker}

  TExtEdit = class(TISEdit)
  private
    FGrayed: Boolean;
    FIsFocused: Boolean;
    procedure SetGrayed(const Value: Boolean);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;

  protected
    procedure Change; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;

  public
    property  Grayed: Boolean read FGrayed write SetGrayed;
    constructor Create(AOwner: TComponent); override;
  end;


  TExtDateTimePicker = class(TDateTimePicker)
  private
    FGrayed: Boolean;
    FIsFocused: Boolean;
    FAllowClearKey: Boolean;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;
    procedure SetGrayed(const Value: Boolean);
    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
    procedure MessagePaint(var Msg: TMessage); message WM_NCPAINT;
    procedure SetBorder(AColor: TColor);
    procedure PaintEdit(DC: HDC; ARect: TRect; EColor, BColor: TColor); 
  protected
    procedure Change; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;

  public
    property  Grayed: Boolean read FGrayed write SetGrayed;
    constructor Create(AOwner: TComponent); override;
    procedure SetDateTime(ADateTime: TDateTime);

  published
    property AllowClearKey: Boolean read FAllowClearKey write FAllowClearKey default False;

    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  TExtCheckBox = class(TCheckBox)        //TCheckBox         TLMDCheckBox
  private
    FGrayed: Boolean;
    procedure SetGrayed(const Value: Boolean);
  protected
    procedure Click; override;

  public
    property  Grayed: Boolean read FGrayed write SetGrayed;
    constructor Create(AOwner: TComponent); override;

  published
    property OnDblClick;
  end;


  {TExtRadioGroup}

  TExtRadioGroup = class(TRadioGroup)    //TRadioGroup
  private
    FGrayed: Boolean;
    procedure SetGrayed(const Value: Boolean);

  protected
    procedure Click; override;

  public
    property  Grayed: Boolean read FGrayed write SetGrayed;
    constructor Create(AOwner: TComponent); override;

  published
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  {TExtComboBox}

  TExtComboBox = class(TComboBox)
  private
    FGrayed: Boolean;
    FAllowClearKey: Boolean;
    procedure SetGrayed(const Value: Boolean);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;

  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure Change; override;

  public
    constructor Create(AOwner: TComponent); override;
    property  Grayed: Boolean read FGrayed write SetGrayed;

  published
    property AllowClearKey: Boolean read FAllowClearKey write FAllowClearKey default False;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  {TExtDBComboBox}
  TExtDBComboBox = class(TisRWDBLookupCombo)
  private
    FGrayed:  Boolean;
    FIsFocused: Boolean;
    procedure SetGrayed(const Value: Boolean);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;

  protected
     procedure Change; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DropDown; override;
    procedure SetDisplayFields(const ADisplayFields: String);
    property  Grayed: Boolean read FGrayed write SetGrayed;

  published
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnDblClick;
  end;


  {TExtDBGrid}
  TExtDBGrid = class(TisRWDBCheckGrid)
  private
    FSortField: string;
    FSelectionChange: TNotifyEvent;

    procedure SetSortField(const Value: string);
    procedure ModifySort(AFieldName: string);

  protected
    procedure DoTitleButtonClick(AFieldName: string); override;
    procedure DoCalcTitleImage(Sender: TObject; Field: TField;
             var TitleImageAttributes: TwwTitleImageAttributes); override;

  public
    procedure DoCalcTitleAttributes(AFieldName: string; AFont: TFont; ABrush: TBrush;
	     var FTitleAlignment: TAlignment); override;
    procedure SelectAll; override;
    procedure UnSelectAll; override;
    procedure ReverseSelection; override;
    procedure LayoutChanged; override;
    constructor Create(AOwner: TComponent); override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    property  SortField: string read FSortField write SetSortField;
    property  OnSelectionChange: TNotifyEvent read FSelectionChange write FSelectionChange;
  end;


  {TExtPageControl}
  TExtPageControl = class(TPageControl)
  published
    property OnDblClick;
  end;


  {TExtTabSheet}
  TExtTabSheet = class(TTabSheet)
  published
    property OnDblClick;
  end;


  TextButton = class(TLMDButton)     //TButton
  published
    property OnDblClick;
  end;

implementation

const cByDefault = '<By Default>';


{ TExtRadioGroup }

constructor TExtRadioGroup.Create(AOwner: TComponent);
begin
  inherited;
  ParentColor := False;
  ParentFont := False;
  FGrayed := False;
end;

procedure TExtRadioGroup.SetGrayed(const Value: Boolean);
begin
  if Value then
    ItemIndex := -1;

  FGrayed := Value;
  Invalidate;
end;

procedure TExtRadioGroup.Click;
begin
  Grayed := False;
  inherited;
end;



{ TExtDBGrid }

constructor TExtDBGrid.Create(AOwner: TComponent);
begin
  inherited;
  UseTFields := True;
  Sorting := True;
  Options := Options + [dgTrailingEllipsis, dgDblClickColSizing];
  ExportOptions.ExportType := wwgetSYLK;
  ExportOptions.Options := [esoShowHeader,esoDblQuoteFields,esoClipboard];
  IniAttributes.Enabled := False;
end;


procedure TExtDBGrid.DoCalcTitleAttributes(AFieldName: string;
  AFont: TFont; ABrush: TBrush; var FTitleAlignment: TAlignment);
var
  h, hf: string;
begin
  if (Length(FSortField) > 0) then
  begin
    h := FSortField;
    AFieldName := AnsiUpperCase(AFieldName);
    while Length(h) > 0 do
    begin
      hf := GetNextStrValue(h, ';');
      if AnsiSameStr(hf, AFieldName) or AnsiSameStr(hf, 'DESC ' + AFieldName) then
      begin
        AFont.Color := clBlue;
        ABrush.Color := clYellow;
        break;
      end;
    end;
  end;

  inherited;
end;


procedure TExtDBGrid.DoCalcTitleImage(Sender: TObject; Field: TField;
  var TitleImageAttributes: TwwTitleImageAttributes);
var
  h, hf: String;
begin
  if (Length(FSortField) > 0) then
  begin
    h := FSortField;
    TitleImageAttributes.ImageIndex := -1;
    TitleImageAttributes.Alignment := taRightJustify;
    while Length(h) > 0 do
    begin
      hf := GetNextStrValue(h, ';');
      if AnsiSameText(hf, Field.FieldName) then
      begin
        TitleImageAttributes.ImageIndex := 1;
        break;
      end
      else if AnsiSameText(hf, 'DESC ' + Field.FieldName) then
      begin
        TitleImageAttributes.ImageIndex := 0;
        break;
      end;
    end;
  end;

  inherited;
end;


procedure TExtDBGrid.DoTitleButtonClick(AFieldName: string);
begin
  ModifySort(AFieldName);

  if Assigned(OnTitleButtonClick) then
     OnTitleButtonClick(Self, AFieldName);

  inherited;
end;


procedure TExtDBGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; AState: TGridDrawState);
var
  t: TImageList;
begin
  if Length(FSortField) > 0 then
  begin
    t := TitleImageList;
    TitleImageList := FTitleImageList;
    try
      inherited;
    finally
      TitleImageList := t;
    end;
  end
  else
    inherited;
end;


procedure TExtDBGrid.LayoutChanged;
begin
  inherited;
end;


procedure TExtDBGrid.ModifySort(AFieldName: string);
var
  hl, hf, hr: String;
begin
  if Length(AFieldName) = 0 then
  begin
    FSortField := '';
    Invalidate;
    Exit;
  end;

  AFieldName := AnsiUpperCase(AFieldName);
  hl := '';
  hf := '';
  hr := FSortField;
  while Length(hr) > 0 do
  begin
    hf := GetNextStrValue(hr, ';');
    if AnsiSameStr(hf, AFieldName) or AnsiSameStr(hf, 'DESC ' + AFieldName) then
      break;
    hl := hl + hf + ';';
  end;

  if AnsiSameStr(hf, AFieldName) then
    hf := 'DESC ' + AFieldName + ';'
  else if AnsiSameStr(hf, 'DESC ' + AFieldName) then
    hf := ''
  else
    hf := AFieldName + ';';

  if ssCtrl in FShift then
    FSortField := hl + hf + hr
  else
    FSortField := hf;

  Invalidate;    
end;

procedure TExtDBGrid.ReverseSelection;
begin
  inherited;
  if Assigned(FSelectionChange) then
    FSelectionChange(Self);
end;

procedure TExtDBGrid.SelectAll;
begin
  inherited;
  if Assigned(FSelectionChange) then
    FSelectionChange(Self);
end;

procedure TExtDBGrid.SetSortField(const Value: string);
begin
  FShift := [];
  ModifySort(Value);
end;

procedure TExtDBGrid.UnSelectAll;
begin
  inherited;
  if Assigned(FSelectionChange) then
    FSelectionChange(Self);
end;



{ TExtComboBox }

procedure TExtComboBox.Change;
begin
  Grayed := False;
  inherited;
end;

constructor TExtComboBox.Create(AOwner: TComponent);
begin
  inherited;
  FAllowClearKey := False;
  FGrayed := False;
  Style := StdCtrls.csDropDownList;
  inherited BevelKind := bkFlat; // Turned off. This was a Shelly hard-code,
end;

procedure TExtComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if FAllowClearKey and (Key = VK_DELETE) then
  begin
    Grayed := False;
    Text := '';
    ItemIndex := -1;
    Change;
  end;
end;

procedure TExtComboBox.KeyPress(var Key: Char);
begin
  if Ord(Key) <> VK_BACK then
    inherited;
end;

procedure TExtComboBox.SetGrayed(const Value: Boolean);
begin
  if Value then
  begin
    if Style = StdCtrls.csDropDown then
      Text := cByDefault
    else
      Text := '';
    ItemIndex := -1;
  end;

  FGrayed := Value;
  Invalidate;
end;


procedure TExtComboBox.WMKillFocus(var Message: TWMKillFocus);
begin
  inherited;
  Invalidate;
end;

procedure TExtComboBox.WMPaint(var Message: TWMPaint);
var
  R: TRect;
  C: TControlCanvas;
  h, d: Integer;
begin
  inherited;

  if FGrayed and (Style = StdCtrls.csDropDownList) then
  begin
    C := TControlCanvas.Create;
    try
      C.Control:=Self;
      C.Font := Font;
      R := ClientRect;
      h := C.TextHeight(cByDefault);
      d := (ClientHeight - h) div 2;
      InflateRect(R, -d, -d);
      R.Right  := R.Right - GetSystemMetrics(SM_CXHTHUMB);
      if FIsFocused and not DroppedDown then
        C.Font.Color := clHighlightText
      else
        C.Font.Color := clGrayText;
      C.Brush.Style := bsClear;
      C.TextRect(R, R.Left, R.Top, cByDefault);
    finally
      C.Free;
    end;
  end;
end;

procedure TExtComboBox.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  Invalidate;
end;


{ TExtDateTimePicker }

constructor TExtDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  FGrayed := False;
  FAllowClearKey := False;
end;

procedure TExtDateTimePicker.Change;
begin
  FGrayed := False;
  inherited;
end;

procedure TExtDateTimePicker.CNNotify(var Message: TWMNotify);
begin
  case Message.NMHdr.Code of
    DTN_DROPDOWN:
      if FGrayed then
        SetDateTime(Now);

    DTN_CLOSEUP:
      if FGrayed then
        SetDateTime(0);
  end;

  inherited;
end;

procedure TExtDateTimePicker.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if FAllowClearKey and (Key = VK_DELETE) then
  begin
    SetDateTime(0);
    Grayed := False;
  end;
end;

procedure TExtDateTimePicker.SetGrayed(const Value: Boolean);
begin
  if Value then
    SetDateTime(0);

  FGrayed := Value;
  Invalidate;
end;

procedure TExtDateTimePicker.WMKillFocus(var Message: TWMKillFocus);
begin
  inherited;
  FIsFocused := False;
  Invalidate;
end;

procedure TExtDateTimePicker.WMPaint(var Message: TWMPaint);
var
  R: TRect;
  C: TControlCanvas;
  h, d: Integer;
  s: String;
begin
  inherited;

  if FGrayed or (DateTime = 0) and (Kind = dtkDate) then
  begin
    C := TControlCanvas.Create;
    try
      C.Control:=Self;
      C.Font := Font;
      R := ClientRect;
      h := C.TextHeight(cByDefault);
      d := (ClientHeight - h) div 2;
      InflateRect(R, -d, -d);
      R.Right  := R.Right - GetSystemMetrics(SM_CXHTHUMB);
      C.Brush.Style := bsSolid;
      if FIsFocused then
      begin
        C.Font.Color := clHighlightText;
        C.Brush.Color := clHighlight;
      end
      else
      begin
        C.Font.Color := clGrayText;
        C.Brush.Color := Color;
      end;
      if FGrayed then
        s := cByDefault
      else
        s := '<Empty>';

      C.TextRect(R, R.Left, R.Top, s);

      if FIsFocused then
      begin
        InflateRect(R, 1, 1);
        C.DrawFocusRect(R);
      end;
    finally
      C.Free;
    end;
  end;
end;

procedure TExtDateTimePicker.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  FIsFocused := True;
  Invalidate;
end;


procedure TExtDateTimePicker.SetDateTime(ADateTime: TDateTime);
begin
  if Kind = dtkDate then
    DateTime := Trunc(ADateTime)
  else
    DateTime := SysUtils.Date + Frac(ADateTime);
end;


procedure TExtDateTimePicker.MessagePaint(var Msg: TMessage);
var
  DC: HDC;
  Rect: TRect;
begin
    DC := GetWindowDC(Handle);
  try
    Windows.GetClientRect(Handle, Rect);
    PaintEdit(DC, Rect, clWindow, clBtnShadow);
  finally
    ReleaseDC(Handle, DC);
  end;
end;

procedure TExtDateTimePicker.PaintEdit(DC: HDC; ARect: TRect; EColor,
  BColor: TColor);
var
  WindowColor: TColor;
  BorderColor: TColor;
begin
  WindowColor := EColor; // Color of TEdit
  BorderColor := BColor; // Border Color of TEdit
  if not Enabled then
  begin
    WindowColor := clBtnFace;
    BorderColor := clBtnShadow;
  end;
  InflateRect(ARect, 4, 4);
  Brush.Color := WindowColor;
  Windows.FillRect(DC, ARect, Brush.Handle);
  SetBorder(BorderColor);
end;

procedure TExtDateTimePicker.SetBorder(AColor: TColor);
var
  Canvas: TCanvas;
begin
    Canvas := TCanvas.Create;
  try
    Canvas.Handle := GetWindowDC(Handle);
    Canvas.Pen.Style := psSolid;
    Canvas.Pen.Color := AColor;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(0, 0, Width, Height);
  finally
    ReleaseDC(Handle, Canvas.Handle);
    Canvas.Free;
  end;
end;

{ TExtDBComboBox }

constructor TExtDBComboBox.Create(AOwner: TComponent);
begin
  inherited;
  FGrayed := False;
end;

procedure TExtDBComboBox.DropDown;
var
 i, w: Integer;
 h, fld: String;
begin
  for i := 0 to Selected.Count - 1 do
  begin
    h := Selected[i];
    fld := GetNextStrValue(h, #9);
    w := StrToInt(GetNextStrValue(h, #9));
    if w = 0 then
      w := LookupTable.FieldByName(fld).DisplayWidth + 1;

    h := fld + #9 + IntToStr(w) + #9 + h;
    Selected[i] := h;
  end;

  inherited;
end;

procedure TExtDBComboBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if AllowClearKey and (Key = VK_DELETE) then
  begin
    Clear;
    Grayed := False;
  end;
end;

procedure TExtDBComboBox.SetGrayed(const Value: Boolean);
begin
  if Value then
    Clear;

  FGrayed := Value;
  Invalidate;
end;

procedure TExtDBComboBox.WMKillFocus(var Message: TWMKillFocus);
begin
  inherited;
  FIsFocused := False;
  Invalidate;
end;

procedure TExtDBComboBox.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  FIsFocused := True;
  Invalidate;
end;

procedure TExtDBComboBox.WMPaint(var Message: TWMPaint);
var
  R: TRect;
  C: TControlCanvas;
  h, d: Integer;
begin
  inherited;

  if FGrayed then
  begin
    C := TControlCanvas.Create;
    try
      C.Control:=Self;
      C.Font := Font;
      R := ClientRect;
      h := C.TextHeight(cByDefault);
      d := (ClientHeight - h) div 2;
      InflateRect(R, -d, -d);
      R.Right  := R.Right - GetSystemMetrics(SM_CXHTHUMB);
      C.Brush.Style := bsSolid;
      if FIsFocused then
      begin
        C.Font.Color := clHighlightText;
        C.Brush.Color := clHighlight;
      end
      else
      begin
        C.Font.Color := clGrayText;
        C.Brush.Color := Color;
      end;
      C.TextRect(R, R.Left, R.Top, cByDefault);

      if FIsFocused then
      begin
        InflateRect(R, 1, 1);
        C.DrawFocusRect(R);
      end;
    finally
      C.Free;
    end;
  end;
end;

procedure TExtDBComboBox.Change;
begin
  Grayed := False;
  inherited;
end;


procedure TExtDBComboBox.SetDisplayFields(const ADisplayFields: String);
var
  h, h1, h2, w, Delim: String;
begin
  if Pos(#13, ADisplayFields) > 0 then
    Delim := #13
  else
    Delim := ';';

  Selected.Clear;
  h := ADisplayFields;
  while Length(h) > 0 do
  begin
    w := GetNextStrValue(h, Delim);
    h1 := GetNextStrValue(w, ',');
    h2  := GetNextStrValue(w, ',');

    if w = '' then
      w := '0';

    Selected.Add(h1+#9+w+#9+h2+#9+'F');
  end;
end;

{ TExtEdit }

procedure TExtEdit.Change;
begin
  FGrayed := False;
  inherited;
end;

constructor TExtEdit.Create(AOwner: TComponent);
begin
  inherited;
  FGrayed := False;
end;

procedure TExtEdit.SetGrayed(const Value: Boolean);
begin
  if Value then
    Text := '';

  FGrayed := Value;
  Invalidate;
end;

procedure TExtEdit.WMKillFocus(var Message: TWMKillFocus);
begin
  inherited;
  FIsFocused := False;
  Invalidate;
end;

procedure TExtEdit.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  FIsFocused := True;
  Invalidate;
end;


procedure TExtEdit.WMPaint(var Message: TWMPaint);
var
  R: TRect;
  C: TControlCanvas;
  h, d: Integer;
begin
  inherited;

  if FGrayed then
  begin
    C := TControlCanvas.Create;
    try
      C.Control:=Self;
      C.Font := Font;
      R := ClientRect;
      h := C.TextHeight(cByDefault);
      d := (ClientHeight - h) div 2;
      InflateRect(R, -d, -d);
      C.Brush.Style := bsSolid;
      if FIsFocused then
      begin
        C.Font.Color := clHighlightText;
        C.Brush.Color := clHighlight;
      end
      else
      begin
        C.Font.Color := clGrayText;
        C.Brush.Color := Color;
      end;
      C.TextRect(R, R.Left, R.Top, cByDefault);

      if FIsFocused then
      begin
        InflateRect(R, 1, 1);
        C.DrawFocusRect(R);
      end;
    finally
      C.Free;
    end;
  end;
end;

procedure TExtEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if FGrayed and (Key = VK_DELETE) then
  begin
    Text := '';
    Grayed := False;
  end;
end;

{ TExtCheckBox }

constructor TExtCheckBox.Create(AOwner: TComponent);
begin
  inherited;
  ParentColor := False;
  ParentFont := False;
  FGrayed := False;
end;

procedure TExtCheckBox.SetGrayed(const Value: Boolean);
begin
  FGrayed := Value;
  if Value then
    State := cbGrayed;
  Invalidate;
end;

procedure TExtCheckBox.Click;
begin
  Grayed := False;
  inherited;
end;


end.
