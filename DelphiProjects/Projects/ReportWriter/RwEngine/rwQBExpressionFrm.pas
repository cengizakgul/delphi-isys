unit rwQBExpressionFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, rwQBExprEditorFrm, rwQBInfo, rwQBExprPanelFrm, DB, rwDB;

type
  TrwQBExpress = class(TFrame)
    lExpr: TLabel;
    pnlExpr: TrwQBExprPanel;
    procedure pnlExprpnlCanvasClick(Sender: TObject);
  private
    FExpressions: TrwQBExpressions;
    FDefaultType: TrwQBExprItemType;
    FDefaultValue: Variant;
    FLookUpDataSet: TrwQBGetLookupDataset;
    FExpressionIndex: Integer;
    FOnChangeExpression: TNotifyEvent;
    FReadOnly: Boolean;
    procedure SetExpression(const Value: TrwQBExpression);
    function  GetExpression: TrwQBExpression;
  public
    constructor Create(AOwner: TComponent); override;

    property Expression: TrwQBExpression read GetExpression write SetExpression;
    property Expressions: TrwQBExpressions read FExpressions write FExpressions;
    property ExpressionIndex: Integer read FExpressionIndex write FExpressionIndex;
    property DefaultType: TrwQBExprItemType read FDefaultType write FDefaultType;
    property DefaultValue: Variant read FDefaultValue write FDefaultValue;
    property LookUpDataSet: TrwQBGetLookupDataset read FLookUpDataSet write FLookUpDataSet;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property OnChangeExpression: TNotifyEvent read FOnChangeExpression write FOnChangeExpression;
  end;


implementation

{$R *.DFM}


procedure TrwQBExpress.SetExpression(const Value: TrwQBExpression);
begin
  pnlExpr.Expression := Value;
end;


function TrwQBExpress.GetExpression: TrwQBExpression;
begin
  Result := pnlExpr.Expression;
end;

procedure TrwQBExpress.pnlExprpnlCanvasClick(Sender: TObject);
var
  Expr: TrwQBExpression;
  i: Integer;
begin
  if ReadOnly then
    Exit;

  if not Assigned(Expression) then
  begin
    Expr := nil;
    for i := 1 to ExpressionIndex + 1 - Expressions.Count do
      Expr := TrwQBExpression(Expressions.Add);
    if not Assigned(Expr) then
      Expr := TrwQBExpression(Expressions.Items[ExpressionIndex]);

    if TrwQBExprEditor.ShowEditor(Expr, DefaultType, DefaultValue, LookUpDataSet) then
    begin
      Expression := Expr;
      pnlExpr.ReDraw;
      if Assigned(FOnChangeExpression) then
        OnChangeExpression(Self);
    end
    else
      Expr.Free;
  end

  else
    if TrwQBExprEditor.ShowEditor(Expression, DefaultType, DefaultValue, LookUpDataSet) then
    begin
      pnlExpr.ReDraw;
      if Assigned(FOnChangeExpression) then
        OnChangeExpression(Self);
    end;
end;


constructor TrwQBExpress.Create(AOwner: TComponent);
begin
  inherited;
  FReadOnly := False;
end;

end.
