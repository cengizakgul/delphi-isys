unit rwEngineComm;

interface

uses SysUtils, Windows, Messages, Forms, isBaseClasses,
     evTransportInterfaces, isBasicUtils, isThreadManager,
     rwEngineApp, rwConnectionInt, EvStreamUtils, rwEngineMainFrm,
     isLogFile, isErrorUtils;

  procedure EngineSrv;

implementation

type
  TrwEngineServer = class(TrwServer)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure DoOnTerminate; override;    
  end;

  
  TrwEngineDatagramDispatcher = class(TrwServerDatagramDispatcher)
  private
    procedure dmOpenReportFromStream(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenReportFromClass(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCloseActiveReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunActiveReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmInitializeAppAdapter(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRWAtoPDF(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCallAppAdapterFunction(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetClassInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmClassInheritsFrom(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmChangeRWAInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetExternalModuleData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenQueryBuilder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetMainWindowHandle(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunQuery(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmQueryInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmOpenQueryBuilder2(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIsQueryBuilderFormClosed(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmProcCtrl_Run(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_Pause(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_Stop(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_GetVMRegisters(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_GetState(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_CalcExpression(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_GetVMCallStack(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_SetBreakpoints(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmProcCtrl_OpenQueryBuilder(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmRepStruct_GetData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepStruct_GetStructureInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepStruct_Compile(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepStruct_PrepareForRunning(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmRepParams_GetCount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_ParamByName(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_IndexOf(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_GetItems(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_GetParamValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_SetParamValue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_ParamsFromStream(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepParams_ParamsToStream(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmRepInpFrm_CloseOK(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepInpFrm_CloseCancel(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepInpFrm_Show(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepInpFrm_GetFormBounds(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRepInpFrm_SetFormBounds(const ADatagram: IevDatagram; out AResult: IevDatagram);

  protected
    procedure DoOnConstruction; override;
    procedure RegisterHandlers; override;
  end;


var
  rwEngineServer: IrwServer;
  FEngineApp: IrwEngineApp;

procedure EngineSrv;
begin
  try
    Application.ShowMainForm := False;
    Application.CreateForm(TrwEngineMain, rwEngineMain);
    rwEngineServer := TrwEngineServer.Create;
  except
    on E: Exception do
    begin
      if GlobalLogFile <> nil then
        GlobalLogFile.AddEvent(etFatalError, 'Server', E.Message, GetStack(E, ExceptAddr));
      raise;  
    end
  end;

  SetThreadPriority(MainThreadID, THREAD_PRIORITY_BELOW_NORMAL);
end;

{ TrwEngineDatagramDispatcher }

procedure TrwEngineDatagramDispatcher.dmCallAppAdapterFunction(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FEngineApp.CallAppAdapterFunction(ADatagram.Params.Value['AFunctionName'],
      ADatagram.Params.Value['AParams']);

    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmChangeRWAInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    NewInfo: TrwRWAFileInfo;
  begin
    NewInfo.PreviewPassword := ADatagram.Params.Value['ANewInfo.PreviewPassword'];
    NewInfo.PrintPassword := ADatagram.Params.Value['ANewInfo.PrintPassword'];
    NewInfo.Compression := ADatagram.Params.Value['ANewInfo.Compression'];
    FEngineApp.ChangeRWAInfo(IInterface(ADatagram.Params.Value['ARWAData']) as IevDualStream,
      ADatagram.Params.Value['ACurrentPassword'], NewInfo);

    AResult.Params.AddValue(METHOD_RESULT,(IInterface(ADatagram.Params.Value['ARWAData']) as IevDualStream))
  end;
begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmCloseActiveReport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.CloseActiveReport;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmGetClassInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwClassInfo;
  begin
    Res := FEngineApp.GetClassInfo(ADatagram.Params.Value['AClassName']);

    AResult.Params.AddValue(METHOD_RESULT + '.ClassName', Res.ClassName);    
    AResult.Params.AddValue(METHOD_RESULT + '.AncestorClassName', Res.AncestorClassName);
    AResult.Params.AddValue(METHOD_RESULT + '.DisplayName', Res.DisplayName);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmClassInheritsFrom(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FEngineApp.ClassInheritsFrom(ADatagram.Params.Value['AClassName'],
      ADatagram.Params.Value['AAncestorClassName']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmGetMainWindowHandle(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    AResult.Params.AddValue(METHOD_RESULT, FEngineApp.GetMainWindowHandle);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmInitializeAppAdapter(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.InitializeAppAdapter(ADatagram.Params.Value['AModuleName'],
                                    ADatagram.Params.Value['AAppCustomData']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmIsQueryBuilderFormClosed(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    QRes: IevDualStream;
    Res: Boolean;
  begin
    Res := FEngineApp.IsQueryBuilderFormClosed(QRes);

    AResult.Params.AddValue(METHOD_RESULT, Res);
    AResult.Params.AddValue('AQueryData', QRes);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmOpenQueryBuilder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.OpenQueryBuilder(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['ADescriptionView']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmOpenQueryBuilder2(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FEngineApp.OpenQueryBuilder2(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['ADescriptionView']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmOpenReportFromClass(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.OpenReportFromClass(ADatagram.Params.Value['AClassName']);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmOpenReportFromStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.OpenReportFromStream(IInterface(ADatagram.Params.Value['AStream']) as IevDualStream);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_CalcExpression(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: TrwVMExpressionResult;
begin
  Res := FEngineApp.ProcessController.CalcExpression(
    ADatagram.Params.Value['AExpression'], ADatagram.Params.Value['ADebugSymbolInfo']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT + '.ResultType', Res.ResultType);
  AResult.Params.AddValue(METHOD_RESULT + '.Result', Res.Result);
  AResult.Params.AddValue(METHOD_RESULT + '.ResultTypeText', Res.ResultTypeText);
  AResult.Params.AddValue(METHOD_RESULT + '.ResultText', Res.ResultText);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_GetState(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: TrwProcessState;
begin
  Res := FEngineApp.ProcessController.GetState;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT + '.VMState', Res.VMState);
  AResult.Params.AddValue(METHOD_RESULT + '.ExceptionText', Res.ExceptionText);  
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_GetVMCallStack(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Variant;
begin
  Res := FEngineApp.ProcessController.GetVMCallStack;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_GetVMRegisters(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: TrwVMRegisters;
begin
  Res := FEngineApp.ProcessController.GetVMRegisters;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT + '.RegA', Res.RegA);
  AResult.Params.AddValue(METHOD_RESULT + '.RegB', Res.RegB);
  AResult.Params.AddValue(METHOD_RESULT + '.RegF', Res.RegF);
  AResult.Params.AddValue(METHOD_RESULT + '.CSIP', Res.CSIP);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_OpenQueryBuilder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FEngineApp.ProcessController.OpenQueryBuilder(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
    ADatagram.Params.Value['ADescriptionView']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_Pause(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FEngineApp.ProcessController.Pause;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_Run(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FEngineApp.ProcessController.Run(TrwRunAction(ADatagram.Params.Value['ARunAction']));
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_SetBreakpoints(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FEngineApp.ProcessController.SetBreakpoints(ADatagram.Params.Value['ABreakpoints']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwEngineDatagramDispatcher.dmProcCtrl_Stop(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FEngineApp.ProcessController.Stop;
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TrwEngineDatagramDispatcher.dmQueryInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    AResult.Params.AddValue(METHOD_RESULT, FEngineApp.QueryInfo(ADatagram.Params.Value['AItemName']));
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepInpFrm_CloseCancel(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.ReportInputForm.CloseCancel;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepInpFrm_CloseOK(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.ReportInputForm.CloseOK;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepInpFrm_GetFormBounds(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwBounds;
  begin
    Res := FEngineApp.GetActiveReport.ReportInputForm.FormBounds;

    AResult.Params.AddValue(METHOD_RESULT + '.Left', Res.Left);
    AResult.Params.AddValue(METHOD_RESULT + '.Top', Res.Top);
    AResult.Params.AddValue(METHOD_RESULT + '.Width', Res.Width);
    AResult.Params.AddValue(METHOD_RESULT + '.Height', Res.Height);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepInpFrm_SetFormBounds(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwBounds;
  begin
    Res.Left := ADatagram.Params.Value['ABounds.Left'];
    Res.Top := ADatagram.Params.Value['ABounds.Top'];
    Res.Width := ADatagram.Params.Value['ABounds.Width'];
    Res.Height := ADatagram.Params.Value['ABounds.Height'];

    FEngineApp.GetActiveReport.ReportInputForm.FormBounds := Res;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepInpFrm_Show(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Boolean;
  begin
    Res := FEngineApp.GetActiveReport.ReportInputForm.Show(
      ADatagram.Params.Value['AParentWndHandle'], ADatagram.Params.Value['ASetupMode']);

    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_GetCount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    AResult.Params.AddValue(METHOD_RESULT, FEngineApp.GetActiveReport.ReportParameters.Count);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_GetItems(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwReportParam;
  begin
    Res := FEngineApp.GetActiveReport.ReportParameters.Items[ADatagram.Params.Value['AIndex']];
    AResult.Params.AddValue(METHOD_RESULT + '.Name', Res.Name);
    AResult.Params.AddValue(METHOD_RESULT + '.Value', Res.Value);
    AResult.Params.AddValue(METHOD_RESULT + '.RunTimeParam', Res.RunTimeParam);
    AResult.Params.AddValue(METHOD_RESULT + '.ParamType', Res.ParamType);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_GetParamValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FEngineApp.GetActiveReport.ReportParameters.ParamValue[ADatagram.Params.Value['AParamName']];
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_IndexOf(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Integer;
  begin
    Res := FEngineApp.GetActiveReport.ReportParameters.IndexOf(ADatagram.Params.Value['AParamName']);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_ParamByName(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwReportParam;
  begin
    Res := FEngineApp.GetActiveReport.ReportParameters.ParamByName(ADatagram.Params.Value['AParamName']);
    AResult.Params.AddValue(METHOD_RESULT + '.Name', Res.Name);
    AResult.Params.AddValue(METHOD_RESULT + '.Value', Res.Value);
    AResult.Params.AddValue(METHOD_RESULT + '.RunTimeParam', Res.RunTimeParam);
    AResult.Params.AddValue(METHOD_RESULT + '.ParamType', Res.ParamType);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_ParamsFromStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.ReportParameters.ParamsFromStream(
      IInterface(ADatagram.Params.Value['AParams']) as IevDualStream);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;


procedure TrwEngineDatagramDispatcher.dmRepParams_ParamsToStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: IevDualStream;
  begin
    Res := FEngineApp.GetActiveReport.ReportParameters.ParamsToStream;
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepParams_SetParamValue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.ReportParameters.ParamValue[ADatagram.Params.Value['AParamName']] :=
      ADatagram.Params.Value['AValue'];
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepStruct_Compile(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.Compile;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepStruct_GetData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, FEngineApp.GetActiveReport.Data);
end;

procedure TrwEngineDatagramDispatcher.dmRepStruct_GetStructureInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: TrwReportStructureInfo;
  begin
    Res := FEngineApp.GetActiveReport.StructureInfo;

    AResult.Params.AddValue(METHOD_RESULT + '.ReportClassName', Res.ReportClassName);
    AResult.Params.AddValue(METHOD_RESULT + '.AncestorClassName', Res.AncestorClassName);
    AResult.Params.AddValue(METHOD_RESULT + '.UsedLibComponents', Res.UsedLibComponents);
    AResult.Params.AddValue(METHOD_RESULT + '.Description', Res.Description);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRepStruct_PrepareForRunning(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.GetActiveReport.PrepareForRunning;
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRunActiveReport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    RunSettings: TrwRunReportSettings;
    Res: TrwReportResult;
  begin
    RunSettings.ReturnParamsBack := ADatagram.Params.Value['ARunSettings.ReturnParamsBack'];
    RunSettings.Duplexing := ADatagram.Params.Value['ARunSettings.Duplexing'];
    RunSettings.ShowInputForm := ADatagram.Params.Value['ARunSettings.ShowInputForm'];
    RunSettings.Suspended := ADatagram.Params.Value['ARunSettings.Suspended'];
    RunSettings.ReportName := ADatagram.Params.Value['ARunSettings.ReportName'];

    Res := FEngineApp.RunActiveReport(RunSettings);

    AResult.Params.AddValue(METHOD_RESULT + '.Data', Res.Data);
    AResult.Params.AddValue(METHOD_RESULT + '.FormatType', Res.FormatType);
    AResult.Params.AddValue(METHOD_RESULT + '.PageInfo', Res.PageInfo);
    AResult.Params.AddValue(METHOD_RESULT + '.MiscInfo', Res.MiscInfo);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRunQuery(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: Variant;
  begin
    Res := FEngineApp.RunQuery(IInterface(ADatagram.Params.Value['AQueryData']) as IevDualStream,
      ADatagram.Params.Value['AParams']);

    AResult.Params.AddValue(METHOD_RESULT, Res);      
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRunReport(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: IevDualStream;
  begin
    FEngineApp.RunReport(IInterface(ADatagram.Params.Value['AReportData']) as IevDualStream, ADatagram.Params.Value['AReportName'], Res);
    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmRWAtoPDF(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  var
    Res: IevDualStream;
    PDFRec: TrwPDFFileInfo;
  begin
    PDFRec.Author := ADatagram.Params.Value['APDFInfo.Author'];
    PDFRec.Creator := ADatagram.Params.Value['APDFInfo.Creator'];
    PDFRec.Subject := ADatagram.Params.Value['APDFInfo.Subject'];
    PDFRec.Title := ADatagram.Params.Value['APDFInfo.Title'];
    PDFRec.Compression := ADatagram.Params.Value['APDFInfo.Compression'];
    PDFRec.ProtectionOptions := ADatagram.Params.Value['APDFInfo.ProtectionOptions'];
    PDFRec.OwnerPassword := ADatagram.Params.Value['APDFInfo.OwnerPassword'];
    PDFRec.UserPassword := ADatagram.Params.Value['APDFInfo.UserPassword'];

    Res := FEngineApp.RWAtoPDF(IInterface(ADatagram.Params.Value['ARWAData']) as IevDualStream, PDFRec);

    AResult.Params.AddValue(METHOD_RESULT, Res);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.dmSetExternalModuleData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);

  procedure MethodProc(const ADatagram, AResult: IevDatagram);
  begin
    FEngineApp.SetExternalModuleData(ADatagram.Params.Value['AModuleName'],
      IInterface(ADatagram.Params.Value['AData']) as IevDualStream);
  end;

begin
  AResult := CreateResponseFor(ADatagram.Header);
  GlobalThreadManager.RunInMainThread(Self, DoInMainThread, MakeTaskParams([ADatagram, AResult, @MethodProc]));
end;

procedure TrwEngineDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FEngineApp := TrwEngineApp.Create;
end;

procedure TrwEngineDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RWENGINE_OpenReportFromStream, dmOpenReportFromStream);
  AddHandler(METHOD_RWENGINE_OpenReportFromClass, dmOpenReportFromClass);
  AddHandler(METHOD_RWENGINE_CloseActiveReport, dmCloseActiveReport);
  AddHandler(METHOD_RWENGINE_RunActiveReport, dmRunActiveReport);
  AddHandler(METHOD_RWENGINE_RunReport, dmRunReport);
  AddHandler(METHOD_RWENGINE_InitializeAppAdapter, dmInitializeAppAdapter);
  AddHandler(METHOD_RWENGINE_RWAtoPDF, dmRWAtoPDF);
  AddHandler(METHOD_RWENGINE_CallAppAdapterFunction, dmCallAppAdapterFunction);
  AddHandler(METHOD_RWENGINE_GetClassInfo, dmGetClassInfo);
  AddHandler(METHOD_RWENGINE_ClassInheritsFrom, dmClassInheritsFrom);
  AddHandler(METHOD_RWENGINE_ChangeRWAInfo, dmChangeRWAInfo);
  AddHandler(METHOD_RWENGINE_SetExternalModuleData, dmSetExternalModuleData);
  AddHandler(METHOD_RWENGINE_OpenQueryBuilder, dmOpenQueryBuilder);
  AddHandler(METHOD_RWENGINE_GetMainWindowHandle, dmGetMainWindowHandle);
  AddHandler(METHOD_RWENGINE_RunQuery, dmRunQuery);
  AddHandler(METHOD_RWENGINE_QueryInfo, dmQueryInfo);
  AddHandler(METHOD_RWENGINE_OpenQueryBuilder2, dmOpenQueryBuilder2);
  AddHandler(METHOD_RWENGINE_IsQueryBuilderFormClosed, dmIsQueryBuilderFormClosed);
  AddHandler(METHOD_RWENGINE_PC_Run, dmProcCtrl_Run);
  AddHandler(METHOD_RWENGINE_PC_Pause, dmProcCtrl_Pause);
  AddHandler(METHOD_RWENGINE_PC_Stop, dmProcCtrl_Stop);
  AddHandler(METHOD_RWENGINE_PC_GetVMRegister, dmProcCtrl_GetVMRegisters);
  AddHandler(METHOD_RWENGINE_PC_GetState, dmProcCtrl_GetState);
  AddHandler(METHOD_RWENGINE_PC_CalcExpression, dmProcCtrl_CalcExpression);
  AddHandler(METHOD_RWENGINE_PC_GetVMCallStack, dmProcCtrl_GetVMCallStack);
  AddHandler(METHOD_RWENGINE_PC_SetBreakpoints, dmProcCtrl_SetBreakpoints);
  AddHandler(METHOD_RWENGINE_PC_OpenQueryBuilder, dmProcCtrl_OpenQueryBuilder);
  AddHandler(METHOD_RWENGINE_RS_GetData, dmRepStruct_GetData);
  AddHandler(METHOD_RWENGINE_RS_GetStructureInfo, dmRepStruct_GetStructureInfo);
  AddHandler(METHOD_RWENGINE_RS_Compile, dmRepStruct_Compile);
  AddHandler(METHOD_RWENGINE_RS_PrepareForRunning, dmRepStruct_PrepareForRunning);
  AddHandler(METHOD_RWENGINE_RP_GetCount, dmRepParams_GetCount);
  AddHandler(METHOD_RWENGINE_RP_ParamByName, dmRepParams_ParamByName);
  AddHandler(METHOD_RWENGINE_RP_IndexOf, dmRepParams_IndexOf);
  AddHandler(METHOD_RWENGINE_RP_GetItems, dmRepParams_GetItems);
  AddHandler(METHOD_RWENGINE_RP_GetParamValue, dmRepParams_GetParamValue);
  AddHandler(METHOD_RWENGINE_RP_SetParamValue, dmRepParams_SetParamValue);
  AddHandler(METHOD_RWENGINE_RP_ParamsFromStream, dmRepParams_ParamsFromStream);
  AddHandler(METHOD_RWENGINE_RP_ParamsToStream, dmRepParams_ParamsToStream);
  AddHandler(METHOD_RWENGINE_RI_CloseOK, dmRepInpFrm_CloseOK);
  AddHandler(METHOD_RWENGINE_RI_CloseCancel, dmRepInpFrm_CloseCancel);
  AddHandler(METHOD_RWENGINE_RI_Show, dmRepInpFrm_Show);
  AddHandler(METHOD_RWENGINE_RI_GetFormBounds, dmRepInpFrm_GetFormBounds);
  AddHandler(METHOD_RWENGINE_RI_SetFormBounds, dmRepInpFrm_SetFormBounds);
end;

{ TrwEngineServer }

function TrwEngineServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TrwEngineDatagramDispatcher.Create;
end;

procedure TrwEngineServer.DoOnTerminate;
begin
  inherited;
  TrwEngineApp((FEngineApp as IisInterfacedObject).GetImplementation).Terminate;
end;

end.
