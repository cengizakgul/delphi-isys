unit rwQBTableFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, rwCustomDataDictionary, rwDataDictionary, Menus,
  StdCtrls, ImgList, rwQBInfo, rmTypes, rwTypes, rwUtils, rwQBTableParamsFrm,
  rwEngineTypes;

type


  {TrwQBTable class for visual reflection of table}

  TrwQBTable = class(TForm)
    pmTable: TPopupMenu;
    Delete1: TMenuItem;
    pnTblName: TPanel;
    pmFilter: TPopupMenu;
    miAddCond: TMenuItem;
    miAddAnd: TMenuItem;
    miAddOr: TMenuItem;
    miDeleteNode: TMenuItem;
    miEditNode: TMenuItem;
    N1: TMenuItem;
    miParams: TMenuItem;
    miAlias: TMenuItem;
    N3: TMenuItem;
    pnContents: TPanel;
    lvFields: TListView;
    Splitter: TSplitter;
    tvFilter: TTreeView;
    miSCTable: TMenuItem;
    lAlias: TLabel;
    lTable: TLabel;
    N2: TMenuItem;
    miCopy: TMenuItem;
    miShowSubQuery: TMenuItem;
    miIsolatedFiltering: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lvFieldsEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure miAddAndClick(Sender: TObject);
    procedure miAddOrClick(Sender: TObject);
    procedure miDeleteNodeClick(Sender: TObject);
    procedure miAddCondClick(Sender: TObject);
    procedure tvFilterCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure tvFilterDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure miEditNodeClick(Sender: TObject);
    procedure miParamsClick(Sender: TObject);
    procedure lvFieldsDblClick(Sender: TObject);
    procedure SplitterMoved(Sender: TObject);
    procedure lvFieldsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure miAliasClick(Sender: TObject);
    procedure pnTblNameMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnTblNameMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnTblNameMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure miSCTableClick(Sender: TObject);
    procedure pnTblNameDblClick(Sender: TObject);
    procedure tvFilterDblClick(Sender: TObject);
    procedure pnTblNameDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lvFieldsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure FormShow(Sender: TObject);
    procedure tvFilterEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure tvFilterMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure miCopyClick(Sender: TObject);
    procedure lvFieldsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure lvFieldsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure miShowSubQueryClick(Sender: TObject);
    procedure miIsolatedFilteringClick(Sender: TObject);
    procedure pmTablePopup(Sender: TObject);

  private
    FTable: TrwQBSelectedTable;
    FMoving: Boolean;
    FMovePoint: TPoint;
    FProcessing: Boolean;

    procedure WMMove(var Message: TWMMove); message WM_MOVE;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure AddFltNode(AType: TrwQBFilterNodeType);
    procedure AddCond(AField: string);
    procedure RwQBTableOpen(var Message: TMessage); message RW_QBTABLE_OPEN;
    procedure RwQBTableClose(var Message: TMessage); message RW_QBTABLE_CLOSE;
    procedure RwQBTableRefresh(var Message: TMessage); message RW_QBTABLE_REFRESH;
    procedure RwQBTableAddCondition(var Message: TMessage); message RW_QBTABLE_ADDCONDITION;
    procedure SyncFilter;
    procedure UpdateColor;
    procedure DoCloseState;
    procedure DoOpenState;

  protected
    procedure CreateParams(var Params: TCreateParams); override;

  public
    property  Table: TrwQBSelectedTable read FTable;
    procedure Initialize(ATable: TrwQBSelectedTable);
    procedure RemoveTable;
    procedure UpdateCaption;
    procedure UpdateFieldPosition;
    procedure CheckUnionStructure;
  end;


  procedure FillListViewWithFields(AListView: TListView; AObject: TrwDataDictTable); overload;
  procedure FillListViewWithFields(AListView: TListView; AFields: TrwQBShowingFields); overload;
  procedure AddShowingFldToListView(AListView: TListView; AField: TrwQBShowingField; DontRefresh: Boolean = False);
  function  QBTypeIntoString(AType: TrwVarTypeKind): String;
  function  DDTypeIntoString(AType: TDataDictDataType): String;

implementation

{$R *.DFM}

uses
  rwOperFrm, rwQueryBuilderFrm, rwQBConditionFrm, Math;


function QBTypeIntoString(AType: TrwVarTypeKind): String;
begin
  case AType of
    rwvInteger:  Result := 'Integer';

    rwvBoolean,
    rwvString:   Result := 'String';

    rwvFloat:    Result := 'Float';

    rwvDate:     Result := 'Date';

    rwvCurrency: Result := 'Currency';
  else
    Result := '';
  end;
end;


function DDTypeIntoString(AType: TDataDictDataType): String;
begin
    case AType of
      ddtDateTime: Result := 'Date';
      ddtInteger:  Result := 'Integer';
      ddtFloat:    Result := 'Float';
      ddtCurrency: Result := 'Currency';
      ddtBoolean:  Result := 'Boolean';
      ddtString:   Result := 'String';
      ddtBLOB:     Result := 'Blob';
      ddtMemo:     Result := 'Memo';
      ddtGraphic:  Result := 'Graphic';
    else
      Result := '';
    end;
end;


procedure InitTypeAndPict(AListItem: TListItem; Attr: TrwDataDictField; AType: TrwVarTypeKind; ASubQuery: Boolean = False);
var
  h: String;
begin
  if not Assigned(Attr) then
  begin
    h := QBTypeIntoString(AType);
    AListItem.ImageIndex := 1;
  end

  else
  begin
    if Assigned(Attr.Table.ForeignKeys.ForeignKeyByField(Attr)) then
      AListItem.ImageIndex := 18
    else if not ASubQuery and Assigned(Attr.Table.PrimaryKey.FindField(Attr)) then
      AListItem.ImageIndex := 20
    else
      AListItem.ImageIndex := 1;
    h := DDTypeIntoString(Attr.FieldType);
  end;

  AListItem.StateIndex := AListItem.ImageIndex;
  AListItem.SubItems.Add(h);
end;


procedure AddShowingFldToListView(AListView: TListView; AField: TrwQBShowingField; DontRefresh: Boolean = False);
var
  lListItem: TListItem;
begin
  if not DontRefresh then
    AListView.UpdateItems(0, MaxInt);

  lListItem := AListView.Items.Add;

  if Length(AField.FieldAlias) > 0 then
    lListItem.Caption := AField.FieldAlias
  else
    lListItem.Caption := AField.GetFieldName(FQBFrm.WizardView);

  InitTypeAndPict(lListItem, AField.Attribute, AField.ExprType, TrwQBShowingFields(AField.Collection).Owner <> nil);
  lListItem.Data := AField;
end;


procedure FillListViewWithFields(AListView: TListView; AObject: TrwDataDictTable); overload;
var
  i: Integer;
  lListItem: TListItem;
  rwFK: TDataDictForeignKey;
begin
  AListView.Items.Clear;

  for i := 0 to AObject.Fields.Count - 1 do
  begin
    if FQBFrm.WizardView and (AObject.Fields[i].DisplayName = '') then
      continue;

    lListItem := AListView.Items.Add;
    if FQBFrm.WizardView then
    begin
      lListItem.Caption := AObject.Fields[i].DisplayName;
      rwFK := AObject.ForeignKeys.ForeignKeyByField(AObject.Fields[i]);
      if Assigned(rwFK) and
         not AnsiSameText(lListItem.Caption, TrwDataDictTable(rwFK.Table).GroupName) then
        lListItem.Caption := lListItem.Caption + ' [' + TrwDataDictTable(rwFK.Table).GroupName + ']';
    end
    else
      lListItem.Caption := AObject.Fields[i].Name;
    lListItem.Data := AObject.Fields[i];

    InitTypeAndPict(lListItem, TrwDataDictField(AObject.Fields[i]), rwvUnknown);
  end;
end;


procedure FillListViewWithFields(AListView: TListView; AFields: TrwQBShowingFields); overload;
var
  i: Integer;
begin
  AListView.Items.Clear;

  for i := 0 to AFields.Count - 1 do
    AddShowingFldToListView(AListView, AFields[i], True);
end;


  {TrwQBTable}

procedure TrwQBTable.Initialize(ATable: TrwQBSelectedTable);
var
  Obj: TrwDataDictTable;

begin
  Left := ATable.Left;
  Top := ATable.Top;
  Width := ATable.Width;
  Height := ATable.Height;
  tvFilter.Height := ATable.Splitter;

  FTable := ATable;

  Obj := ATable.lqObject;

  UpdateCaption;
  UpdateColor;

  // This block needs to run right here. Otherwise it recreates the Handle of lvFields and doesn't
  // restore the content. Looks like it is Delphi's bug.
  if ATable.IsSUBQuery and ATable.SUBQuery.IsUnionItem then
    lvFields.HideSelection := False;
  DoCloseState;
  lvFields.Columns[0].Width := ATable.FieldColumnWidth;

  lvFields.Items.BeginUpdate;
  lvFields.Items.Clear;

  if not ATable.IsSUBQuery then
  begin
    if Obj.Params.Count = 0 then
      miParams.Enabled := False
    else
      miParams.Enabled := True;
    miShowSubQuery.Visible := False;

    FillListViewWithFields(lvFields, Obj);
  end

  else
  begin
    miParams.Enabled := False;
    miShowSubQuery.Visible := True;
    FillListViewWithFields(lvFields, ATable.SUBQuery.ShowingFields);
  end;

  lvFields.Selected := lvFields.TopItem;

  if ATable.IsSUBQuery and ATable.SUBQuery.IsUnionItem then
  begin
    Splitter.Visible := False;
    tvFilter.Visible := False;
    miAlias.Enabled := False;
    CheckUnionStructure;
  end

  else if ATable.IsSUBQuery and ATable.SUBQuery.IsConditionedSQL then
  begin
    Splitter.Visible := False;
    tvFilter.Visible := False;
  end

  else
    lvFields.SortType := stText;

  lvFields.Items.EndUpdate;
end;

procedure TrwQBTable.FormCreate(Sender: TObject);
begin
  FMoving := False;
  FProcessing := False;
end;

procedure TrwQBTable.FormDestroy(Sender: TObject);
var
  N: TTreeNode;
begin
  if not FTable.Destroying then
  begin
    if FTable.IsSUBQuery and not FQBFrm.Refreshing then
    begin
      N := FQBFrm.NodeByQuery(FTable.SUBQuery);
      N.Free;
      FQBFrm.tvQuery.Invalidate;
    end;

    FTable.VisualComponent := nil;

    if not FQBFrm.Refreshing then
    begin
      FTable.Free;
      FQBFrm.SwitchQuery;
    end;
  end;
end;

procedure TrwQBTable.WMMove(var Message: TWMMove);
begin
  FQBFrm.HideJoinHint;

  inherited;

  if Assigned(FTable) and not FProcessing then
  begin
    FTable.Top := Top;
    FTable.Left := Left;
    if Assigned(Parent) then
      Parent.Invalidate;
  end;
end;

procedure TrwQBTable.WMSize(var Message: TWMSize);
begin
  FQBFrm.HideJoinHint;

  inherited;

  if Assigned(FTable) and not FProcessing and (BorderStyle = bsSizeable) then
  begin
    FTable.Top := Top;
    FTable.Left := Left;
    FTable.Width := Width;
    FTable.Height := Height;

    if Assigned(Parent) then
      Parent.Invalidate;
  end;
end;

procedure TrwQBTable.Delete1Click(Sender: TObject);
begin
  RemoveTable;
end;

procedure TrwQBTable.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TrwQBTable.lvFieldsEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  Attr: TrwDataDictField;
  li1, li2: TListItem;
  i: Integer;
  T: TrwQBSelectedTable;
begin
  if not Assigned(lvFields.Selected) then
  begin
    FQBFrm.DragObject.ObjectType := rdoNone;
    Exit;
  end;

  if FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem and (Sender = lvFields) and (Target = lvFields) then
  begin
    li2 := lvFields.GetItemAt(X, Y);
    if Assigned(li2) then
    begin
      li1 := lvFields.Selected;
      TrwQBShowingField(li1.Data).Index := TrwQBShowingField(li2.Data).Index;
      i := TrwQBShowingField(li1.Data).Index;
      lvFields.Items.BeginUpdate;
      try
        FillListViewWithFields(lvFields, FTable.SUBQuery.ShowingFields);
      finally
        lvFields.Items.EndUpdate;
      end;

      if FTable.Index = 0 then
      begin
        FQBFrm.SyncFields;
        FQBFrm.SyncUnionTables;
        T := TrwQBSelectedTables(FTable.Collection).QBQuery.ParentTable;
        while Assigned(T) do
        begin
          PostMessage(TForm(T.VisualComponent).Handle, RW_QBTABLE_REFRESH, 0, 0);
          T := TrwQBSelectedTables(T.Collection).QBQuery.ParentTable;
        end;
      end;

      CheckUnionStructure;
      
      lvFields.Items[i].Selected := True;
    end;

    FQBFrm.DragObject.ObjectType := rdoNone;
    Exit;
  end;

  if TObject(lvFields.Selected.Data) is TrwDataDictField then
  begin
    FQBFrm.DragObject.TableOwner := FTable;
    FQBFrm.DragObject.FieldName := TrwDataDictField(lvFields.Selected.Data).Name;
  end

  else
  begin
    FQBFrm.DragObject.TableOwner := FTable;
    FQBFrm.DragObject.FieldName := TrwQBShowingField(lvFields.Selected.Data).InternalName;
  end;

  FQBFrm.DragObject.TableOwnerName := FTable.TableName;

  if Target <> Parent then
  begin
    if Target = tvFilter then
      PostMessage(Handle, RW_QBTABLE_ADDCONDITION, 0, 0)

    else if Target = FQBFrm.lvFields then
      FQBFrm.actNewField.Execute

    else if (Target is TLabel) and ((TLabel(Target).Name = 'lAlias') or (TLabel(Target).Name = 'lTable')) then
    begin
      FQBFrm.DragObject.TableDest := TrwQBTable(TPanel(Target).Owner).FTable;
      FQBFrm.actAddJoin.Execute;
    end;
  end

  else
  begin
    if FTable.IsSUBQuery then
    begin
      FQBFrm.DragObject.Field := TrwQBShowingField(lvFields.Selected.Data);
      Attr := TrwQBShowingField(lvFields.Selected.Data).Attribute;
      if Assigned(Attr) then
      begin
        FQBFrm.DragObject.TableOwnerName := Attr.Table.Name;
        FQBFrm.DragObject.FieldName := Attr.Name;
      end

      else
      begin
        FQBFrm.DragObject.ObjectType := rdoNone;
        Exit;
      end;

    end;

    FQBFrm.DragObject.MiscParams := qmpDontActivateTable;
    FQBFrm.DragObject.DropPoint := Point(X, Y);
    FQBFrm.actNewTable.Execute;
  end;

  FQBFrm.DragObject.ObjectType := rdoNone;
end;

procedure TrwQBTable.miAddAndClick(Sender: TObject);
begin
  AddFltNode(rntAnd);
end;

procedure TrwQBTable.AddFltNode(AType: TrwQBFilterNodeType);
var
  Node: TTreeNode;
  FltNode: TrwQBFilterNode;
begin
  Node := tvFilter.Selected;
  if not Assigned(Node) then Exit;

  FQBFrm.SyncSQLResult;

  if TObject(Node.Data) is TrwQBFilterCondition then
    Node := Node.Parent;

  FltNode := TrwQBFilterNode(Node.Data);

  FltNode := TrwQBFilterNode.Create(FltNode.Nodes);
  FltNode.NotOper := False;
  FltNode.NodeType := AType;

  Node := tvFilter.Items.AddChild(Node, FltNode.TextForDesign);

  Node.Data := FltNode;
  Node.ImageIndex := -1;
  Node.SelectedIndex := -1;
  Node.StateIndex := -1;
  tvFilter.Selected := Node;
end;

procedure TrwQBTable.miAddOrClick(Sender: TObject);
begin
  AddFltNode(rntOr);
end;

procedure TrwQBTable.miDeleteNodeClick(Sender: TObject);
var
  Node: TTreeNode;
  FltNode: TCollectionItem;
begin
  Node := tvFilter.Selected;
  if not Assigned(Node) or (Node.Level = 0) then Exit;

  FQBFrm.SyncSQLResult;  
  FltNode := TCollectionItem(Node.Data);
  FltNode.Free;
  Node.Free;
  FQBFrm.pcFields.OnChange(nil);  
end;

procedure TrwQBTable.miAddCondClick(Sender: TObject);
begin
  AddCond('');
end;

procedure TrwQBTable.AddCond(AField: string);
var
  Node: TTreeNode;
  Cnd: TrwQBFilterCondition;
  N: TrwQBFilterNode;
  fl: Boolean;
begin
  Node := tvFilter.Selected;
  if not Assigned(Node) then
    Exit;
  if TObject(Node.Data) is TrwQBFilterCondition then
    Node := tvFilter.Selected.Parent;

  N := TrwQBFilterNode(Node.Data);

  fl := False;
  Cnd := TrwQBFilterCondition(N.Conditions.Add);
  try
    if AField <> '' then
      Cnd.LeftPart.AddItem(eitField, AField, FTable.InternalName);
    Cnd.Operation := '=';

    fl := QBCondition(Self, Cnd);

    if fl then
    begin
      FQBFrm.SyncSQLResult;

      Node := tvFilter.Items.AddChild(Node, Cnd.TextForDesign(FQBFrm.WizardView));
      Node.Data := Cnd;

      if Cnd.NotOper then
        Node.ImageIndex := 19
      else
        Node.ImageIndex := -1;

      Node.SelectedIndex := Node.ImageIndex;
      Node.StateIndex := Node.ImageIndex;
      tvFilter.Selected := Node;

      FQBFrm.pcFields.OnChange(nil);

      if tvFilter.Height <= 20 then
        tvFilter.Height := 50;
    end
  finally
    if not fl then
      Cnd.Free;
  end;
end;

procedure TrwQBTable.tvFilterCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  with tvFilter.Canvas do
    if (TObject(Node.Data) is TrwQBFilterNode) then
      if Node.DropTarget or (cdsSelected in State) then
      begin
        Font.Color := clHighLightText;
        Brush.Color := clHighLight;
      end
      else if (TrwQBFilterNode(Node.Data)).NodeType = rntAnd then
        Font.Color := clBlue
      else
        Font.Color := clRed;
end;

procedure TrwQBTable.tvFilterDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Source = lvFields) and (Sender = tvFilter) or
            (Source = tvFilter) and (Sender = tvFilter);
end;

procedure TrwQBTable.miEditNodeClick(Sender: TObject);
var
  Node: TTreeNode;
begin
  Node := tvFilter.Selected;
  if not Assigned(Node) then
    Exit;

  if TObject(Node.Data) is TrwQBFilterCondition then
  begin
    if QBCondition(Self, TrwQBFilterCondition(Node.Data)) then
    begin
      FQBFrm.SyncSQLResult;
      Node.Text := TrwQBFilterCondition(Node.Data).TextForDesign(FQBFrm.WizardView);
      if TrwQBFilterCondition(Node.Data).NotOper then
        Node.ImageIndex := 19
      else
        Node.ImageIndex := -1;
    end
  end

  else if EditOperation(Self) then
  begin
    FQBFrm.SyncSQLResult;
    Node.Text := TrwQBFilterNode(Node.Data).TextForDesign;
    if TrwQBFilterNode(Node.Data).NotOper then
      Node.ImageIndex := 19
    else
      Node.ImageIndex := -1;
  end;

  Node.SelectedIndex := Node.ImageIndex;
  Node.StateIndex := Node.ImageIndex;
  FQBFrm.pcFields.OnChange(nil);
end;

procedure TrwQBTable.miParamsClick(Sender: TObject);
begin
  if QBVirtTableParams(Table) then
  begin
    FQBFrm.SyncSQLResult;
    FQBFrm.pcFields.OnChange(nil);
  end;
end;

procedure TrwQBTable.lvFieldsDblClick(Sender: TObject);
begin
  if not (FTable.IsSUBQuery and (FTable.SUBQuery.IsConditionedSQL or FTable.SUBQuery.IsUnionItem)) then
    lvFieldsEndDrag(Self, FQBFrm.lvFields, 0, 0);
end;

procedure TrwQBTable.SplitterMoved(Sender: TObject);
begin
  if Assigned(FTable) and not FProcessing then
    FTable.Splitter := tvFilter.Height;
end;

procedure TrwQBTable.lvFieldsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  FQBFrm.HideJoinHint;
  if Assigned(FTable) then
    FTable.FieldColumnWidth := lvFields.Columns[0].Width;
end;

procedure TrwQBTable.RwQBTableClose(var Message: TMessage);
begin
  FQBFrm.CurrentActiveTable := nil;
  DoCloseState;
end;

procedure TrwQBTable.RwQBTableOpen(var Message: TMessage);
begin
  if Assigned(FQBFrm.CurrentActiveTable) then
    FQBFrm.CurrentActiveTable.Perform(RW_QBTABLE_CLOSE, 0, 0);

  FQBFrm.CurrentActiveTable := Self;
  DoOpenState;
  BringToFront;
  FQBFrm.ChildTables(True);
end;

procedure TrwQBTable.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style :=  Params.Style and not WS_CAPTION;
end;

procedure TrwQBTable.miAliasClick(Sender: TObject);
var
  h, OldAlias: string;
  N: TTreeNode;
begin
  OldAlias := Table.Alias;
  h := OldAlias;
  if InputQuery('Table Alias', 'Input table alias', h) then
  begin
    FQBFrm.SyncSQLResult;

    h := StringReplace(Trim(h), ' ', '_', [rfReplaceAll]);

    if FTable.IsSUBQuery and (h = '') then
      raise ErwException.Create('Alias must not be empty for a SubQuery');

    Table.Alias := h;
    UpdateCaption;

    SyncFilter;
    FQBFrm.RefreshFields(FTable);

    if Table.IsSUBQuery then
    begin
      N := FQBFrm.NodeByQuery(Table.SUBQuery);
      if AnsiSameText(OldAlias, N.Text) then
        N.Text := Table.Alias;
    end;

    FQBFrm.pcFields.OnChange(nil);
  end;
end;

procedure TrwQBTable.pnTblNameMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and not FProcessing then
  begin
    FMoving := True;
    SetCapture(pnTblName.Handle);
    FMovePoint := Point(X, Y);
  end;
  FProcessing := False;
end;

procedure TrwQBTable.pnTblNameMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
    FMoving := False;
    ReleaseCapture;
  end;
end;

procedure TrwQBTable.pnTblNameMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  L, T: Integer;
begin
  FQBFrm.HideJoinHint;

  if FMoving and ((X <> FMovePoint.X) or (Y <> FMovePoint.Y)) then
  begin
    L := Left + X - FMovePoint.X;
    if L >= 0 then
      Left := L
    else
      Left := 0;
    T := Top + Y - FMovePoint.Y;
    if T >= 0 then
      Top := T
    else
      Top := 0;  
  end;
end;

procedure TrwQBTable.miSCTableClick(Sender: TObject);
begin
  if FQBFrm.CurrentActiveTable <> Self then
    Perform(RW_QBTABLE_OPEN, 0, 0)
  else
  begin
    Perform(RW_QBTABLE_CLOSE, 0, 0);
    FQBFrm.ChildTables(False);
  end;
end;

procedure TrwQBTable.pnTblNameDblClick(Sender: TObject);
begin
  miSCTable.Click;
  FProcessing := True;
end;

procedure TrwQBTable.tvFilterDblClick(Sender: TObject);
begin
  miEditNode.OnClick(nil);
end;

procedure TrwQBTable.pnTblNameDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Source <> lvFields) and not (FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem);
end;


procedure TrwQBTable.SyncFilter;

  procedure SyncCondition(AFltCond: TrwQBFilterCondition; ANode: TTreeNode);
  var
    Node: TTreeNode;
  begin
    Node := tvFilter.Items.AddChildObject(ANode, AFltCond.TextForDesign(FQBFrm.WizardView), AFltCond);

    if AFltCond.NotOper then
      Node.ImageIndex := 19
    else
      Node.ImageIndex := -1;

    Node.SelectedIndex := Node.ImageIndex;
    Node.StateIndex := Node.ImageIndex;
  end;

  procedure SyncNode(AFltNode: TrwQBFilterNode; ANode: TTreeNode);
  var
    Node: TTreeNode;
    i: Integer;
  begin
    Node := tvFilter.Items.AddChildObject(ANode, AFltNode.TextForDesign, AFltNode);
    Node.StateIndex := Ord(AFltNode.NodeType);

    if AFltNode.NotOper then
      Node.ImageIndex := 19
    else
      Node.ImageIndex := -1;

    Node.SelectedIndex := Node.ImageIndex;
    Node.StateIndex := Node.ImageIndex;

    for i := 0 to AFltNode.Nodes.Count - 1 do
      SyncNode(TrwQBFilterNode(AFltNode.Nodes.Items[i]), Node);

    for i := 0 to AFltNode.Conditions.Count - 1 do
      SyncCondition(TrwQBFilterCondition(AFltNode.Conditions.Items[i]), Node);

    tvFilter.Selected := tvFilter.Items[0];
  end;

begin
  tvFilter.Items.BeginUpdate;
  try
    tvFilter.Items.Clear;
    SyncNode(FTable.Filter, nil);
    tvFilter.FullExpand;
  finally
    tvFilter.Items.EndUpdate;
  end;
end;

procedure TrwQBTable.UpdateColor;
begin
  if FTable.IsSUBQuery then
    pnTblName.Color := $003A9535 // $00558C3E
  else
    pnTblName.Color := $00D96C00;
end;

procedure TrwQBTable.DoCloseState;
begin
  FProcessing := True;
  try
    ActiveControl := nil;
    BorderStyle := bsSingle;
    ClientHeight := pnTblName.Height;
    pnTblName.BevelOuter := bvRaised;
    pnTblName.BevelInner := bvLowered;
    pnTblName.BorderStyle := bsNone;
  finally
    FProcessing := False;
  end;
end;

procedure TrwQBTable.DoOpenState;
begin
  FProcessing := True;
  try
    BorderStyle := bsSizeable;
    Height := FTable.Height;
    Width := FTable.Width;
    pnTblName.BevelOuter := bvNone;
    pnTblName.BevelInner := bvNone;
    pnTblName.BorderStyle := bsSingle;
    Left := FTable.Left;
    Top := FTable.Top;
    SyncFilter;

    if FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem then
      CheckUnionStructure;


    OnShow(nil);
  finally
    FProcessing := False;
  end;

  UpdateFieldPosition;  
end;


procedure TrwQBTable.lvFieldsStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  FQBFrm.DragObject.ObjectType := rdoField;
end;

procedure TrwQBTable.FormShow(Sender: TObject);
var
  n: Integer;
begin
  n := lvFields.Columns[0].Width + lvFields.Columns[1].Width + GetSystemMetrics(SM_CYHSCROLL) + 4;
  if lvFields.Width < n then
    lvFields.Columns[0].Width := lvFields.Columns[0].Width - (n - lvFields.Width);

  if Assigned(Parent) then
    Parent.Invalidate;
end;


procedure TrwQBTable.RwQBTableRefresh(var Message: TMessage);
begin
  if FTable.IsSUBQuery then
  begin
    FillListViewWithFields(lvFields, FTable.SUBQuery.ShowingFields);
    if FTable.SUBQuery.IsUnionItem then
      CheckUnionStructure;
  end;    
end;

procedure TrwQBTable.RemoveTable;
begin
  FQBFrm.SyncSQLResult;

  if Assigned(FQBFrm.CurrentActiveTable) then
    FQBFrm.CurrentActiveTable.Perform(RW_QBTABLE_CLOSE, 0, 0);
  FQBFrm.CurrentActiveTable := nil;

  FQBFrm.pcFields.OnChange(nil);

  RemoveAllExprItems(TrwQBSelectedTables(FTable.Collection).QBQuery, '', FTable.InternalName);

  Close;
  if Assigned(Parent) then
    Parent.Invalidate;

  FQBFrm.ChildTables(False);
end;

procedure TrwQBTable.tvFilterEndDrag(Sender, Target: TObject; X, Y: Integer);
var
  N, P: TTreeNode;
begin
  if Target = tvFilter then
  begin
    N := tvFilter.GetNodeAt(X, Y);
    if not Assigned(N) or not Assigned(tvFilter.Selected.Parent) then
      Exit;

    if TObject(N.Data) is TrwQBFilterCondition then
      N := N.Parent;

    if TObject(tvFilter.Selected.Data) is TrwQBFilterCondition then
    begin
      TrwQBFilterCondition(tvFilter.Selected.Data).Collection := TrwQBFilterNode(N.Data).Conditions;
      tvFilter.Selected.MoveTo(N, naAddChild);
    end

    else
    begin
      P := N;
      while Assigned(P) do
      begin
        if P = tvFilter.Selected then
          Exit;
        P := P.Parent;
      end;

      TrwQBFilterNode(tvFilter.Selected.Data).Collection := TrwQBFilterNode(N.Data).Nodes;
      tvFilter.Selected.MoveTo(N, naAddChild);
    end;
  end;
end;

procedure TrwQBTable.UpdateCaption;
begin
  if FTable.IsSUBQuery then
  begin
    if FTable.SUBQuery.IsUnionItem then
      lAlias.Caption := ' #' + IntToStr(FTable.Index + 1) + ' '
    else
      lAlias.Caption := ' ' + FTable.Alias + ' ';

    if (FTable.SUBQuery.Description <> '') and not AnsiSameText(FTable.SUBQuery.Description, FTable.Alias) then
      lTable.Caption := FTable.SUBQuery.Description
    else
      lTable.Caption := qbEmbeddedQueryName;
  end

  else
  begin
    if FQBFrm.WizardView then
    begin
      lTable.Caption := FTable.lqObject.DisplayName;
      if not AnsiSameText(lTable.Caption, FTable.lqObject.GroupName) then
        lTable.Caption := lTable.Caption  + ' [' + FTable.lqObject.GroupName + ']';
    end
    else
      lTable.Caption := FTable.TableName;
    if FTable.Alias <> '' then
      lAlias.Caption := ' ' + FTable.Alias + ' '
    else
      lAlias.Caption := '';
  end;
end;

procedure TrwQBTable.tvFilterMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  FQBFrm.HideJoinHint;
end;

procedure TrwQBTable.miCopyClick(Sender: TObject);
begin
  PutToClipboard(CF_QBTABLE, FTable.ContentToString);
end;

procedure TrwQBTable.RwQBTableAddCondition(var Message: TMessage);
begin
  if FTable.IsSUBQuery then
    AddCond(TrwQBShowingField(lvFields.Selected.Data).InternalName)
  else
    AddCond(TrwDataDictField(lvFields.Selected.Data).Name);
end;


procedure TrwQBTable.CheckUnionStructure;
var
  Struct, Flds: TrwQBShowingFields;
  i, j, n: Integer;
  fl: Boolean;
begin
  Struct := TrwQBSelectedTables(FTable.Collection).QBQuery.ShowingFields;
  Flds := FTable.SUBQuery.ShowingFields;

  fl := Struct.Count > 0;  

  n := Min(Flds.Count, Struct.Count) - 1;
  for i := 0 to n do
  begin
    if Struct[i].ExprType = Flds[i].ExprType then
      j := 1
    else
    begin
      j := 27;
      fl := False;
    end;

    lvFields.Items[i].ImageIndex := j;
    lvFields.Items[i].StateIndex := j;
  end;

  for i := n + 1 to Flds.Count - 1 do
  begin
    j := 28;
    lvFields.Items[i].ImageIndex := j;
    lvFields.Items[i].StateIndex := j;
    fl := False;
  end;

  if fl and (Flds.Count = Struct.Count) then
    lTable.Font.Color := clWhite
  else
    lTable.Font.Color := clRed;
end;

procedure TrwQBTable.UpdateFieldPosition;
var
  SelItem: TListItem;
begin
  if Visible and not FProcessing and Assigned(FQBFrm.lvFields.Selected) and FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem then
  begin
    if lvFields.Items.Count <= FQBFrm.lvFields.Selected.Index then
      SelItem := nil
    else
      SelItem := lvFields.Items[FQBFrm.lvFields.Selected.Index];
    if lvFields.Selected <> SelItem then
    begin
      FProcessing := True;
      try
        lvFields.Selected := SelItem;
      finally
        FProcessing := False;
      end;
    end;
  end;
end;

procedure TrwQBTable.lvFieldsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
  SelItem: TListItem;
begin
  if Visible and not FProcessing and Assigned(Item) and FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem then
  begin
    if FQBFrm.lvFields.Items.Count <= Item.Index then
      SelItem := nil
    else
      SelItem := FQBFrm.lvFields.Items[Item.Index];
    if FQBFrm.lvFields.Selected <> SelItem then
    begin
      FProcessing := True;
      try
        FQBFrm.lvFields.Selected := SelItem;
      finally
        FProcessing := False;
      end;
    end;
  end;
end;


procedure TrwQBTable.lvFieldsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := (Source = lvFields) and FTable.IsSUBQuery and FTable.SUBQuery.IsUnionItem;
end;


procedure TrwQBTable.miShowSubQueryClick(Sender: TObject);
begin
  FQBFrm.tvQuery.Selected := FQBFrm.NodeByQuery(FTable.SUBQuery);
  FQBFrm.SwitchQuery;
end;

procedure TrwQBTable.miIsolatedFilteringClick(Sender: TObject);
begin
  Table.IsolatedFiltering := miIsolatedFiltering.Checked;
  FQBFrm.SyncSQLResult;
  FQBFrm.pcFields.OnChange(nil);
end;

procedure TrwQBTable.pmTablePopup(Sender: TObject);
begin
  miIsolatedFiltering.Checked := Table.IsolatedFiltering;
end;

end.
