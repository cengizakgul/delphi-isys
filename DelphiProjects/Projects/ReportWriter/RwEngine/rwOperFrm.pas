unit rwOperFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwQueryBuilderFrm, rwQBTableFrm, rwQBInfo;

type

  TrwOper = class(TForm)
    rgOper: TRadioGroup;
    chbNot: TCheckBox;
    btnCancel: TButton;
    btnOK: TButton;
  private
  public
  end;

function EditOperation(ATable: TrwQBTable): Boolean;

implementation

{$R *.DFM}

function EditOperation(ATable: TrwQBTable): Boolean;
var
  Frm: TrwOper;
  Cond: TrwQBFilterNode;
begin
  Frm := TrwOper.Create(Application);
  with Frm do
  begin
    Cond := TrwQBFilterNode(ATable.tvFilter.Selected.Data);

    rgOper.ItemIndex := Ord(Cond.NodeType);
    chbNot.Checked := Cond.NotOper;

    Result := (ShowModal = mrOk);

    if Result then
    begin
      Cond.NodeType := TrwQBFilterNodeType(rgOper.ItemIndex);
      Cond.NotOper := chbNot.Checked;
    end;

    Free;
  end;
end;

end.
