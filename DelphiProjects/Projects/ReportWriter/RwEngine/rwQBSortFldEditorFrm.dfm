object rwQBSortFldEditor: TrwQBSortFldEditor
  Left = 376
  Top = 292
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Sort Field Editor'
  ClientHeight = 122
  ClientWidth = 211
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 4
    Width = 66
    Height = 13
    Caption = 'Showing Field'
  end
  object cbFields: TComboBox
    Left = 4
    Top = 19
    Width = 203
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 142
    Top = 94
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object btnOK: TButton
    Left = 68
    Top = 94
    Width = 65
    Height = 23
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object rgDirection: TRadioGroup
    Left = 4
    Top = 49
    Width = 203
    Height = 35
    Caption = 'Sort Direction'
    Columns = 2
    Items.Strings = (
      'Ascend'
      'Descend')
    TabOrder = 1
  end
end
