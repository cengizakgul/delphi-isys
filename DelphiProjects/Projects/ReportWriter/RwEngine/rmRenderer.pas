unit rmRenderer;

interface

uses  Classes, SysUtils, Types, Graphics, StdCtrls, Dialogs,
      rwTypes, rwUtils, rwPrintPage, rwPrintElements, rwEngineTypes, rwEngine,
      rmReport, rmCommonClasses, rwBasicUtils, rwDB, sbAPI, isBaseClasses,
      evStreamUtils, rwGraphics, rmPrintTable, rmPrintFormControls, rmTypes,
      rmASCIIFormControls, rmXMLFormControls, MSXML2_TLB, rmXMLxsdSchemaParser;

type
  TrmCustomRenderEngine = class(TInterfacedObject, IrmCustomRenderEngine)
  private
    FResult:  TrwResultHolder;
    FRenderingStopped: Boolean;
    function  ResultType: TrwResultFormatType; virtual; abstract;
    procedure SealUpResult; virtual;
    procedure EmptyResult; virtual;

  // Interface implementation
    function RealObject: TObject;
  // End

  public
    constructor Create; virtual;
    destructor  Destroy; override;
  end;


  TrmRWARenderEngine = class(TrmCustomRenderEngine, IrmRWARenderEngine)
  private
    FPage:    TrwVirtualPage;
    FMacrosPos:  TList;
    function  ResultType: TrwResultFormatType; override;
    procedure SealUpResult; override;
    procedure EmptyResult; override;
    procedure RenderPrintForm(APrintForm: TrmPrintForm);
    function  CompareSimpleObj(AObject: TrwPrintObject; ACompare: Boolean; ALayerNumber: Integer): Boolean;
    procedure SetSimpleObj(AObject: TrwPrintObject; ACompare: Boolean; ALayerNumber: Integer);
    function  ObjectRepository: TrwPrintRepository;
    procedure AddPrintObjectToResult(AObject: TrwPrintObject);

  // Interface implementation
    function  CurrentPageNumber: Integer;
    procedure StorePageDataToResult;
    procedure AddFrame(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer; AColor: TColor;
                       ABoundLines: TrwSetColumnLines; ABoundLinesColor: TColor;
                       ABoundLinesWidth: Integer; ABoundLinesStyle: TPenStyle);

    procedure AddLine(X1, X2, Y1, Y2: Integer; ACompare: Boolean; ALayerNumber: Integer; ALineColor: TColor;
                      ALineWidth: Integer; ALineStyle: TPenStyle);

    procedure AddText(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer;
                      AAlignment: TAlignment; ALayout: TTextLayout; AColor: TColor; AWordWrap: Boolean;
                      const ADigitNet: String; AFont: TrwFont; const AText: String);

    procedure AddPicture(ABounds: TRect; ACompare: Boolean; ALayerNumber: Integer;
                         ATransparent: Boolean; APicture: TrwPicture; AStretch: Boolean);

  // End

  public
    constructor Create; override;
    destructor  Destroy; override;
  end;


  // virtual ASCII file
  TrmASCIIFileHolder = class
    FHandle: TsbTableCursor;
    FFileName: String;
    FConvertToXls: boolean;
    FLastLineBreak: boolean;
    procedure StoreDataToFile;
    procedure SetCurrASCIILine(const ALine: Integer);
  public
    procedure   Append(const AText: String);
    procedure   Insert(const AText: String; const ALine: Integer);
    procedure   Update(const AText: String; const ALine: Integer);
    procedure   Delete(const ALine: Integer);
    procedure   CutOff(const ANewLineCount: Integer);
    function    GetLine(const ALine: Integer): String;
    function    LineCount: Integer;

    property    ConvertToXls: boolean read FConvertToXls write FConvertToXls;
    property    LastLineBreak: boolean read FLastLineBreak write FLastLineBreak;

    constructor Create(const AFileName: String);
    destructor  Destroy; override;
  end;


  TrmASCIIRenderEngine = class(TrmCustomRenderEngine, IrmASCIIRenderEngine)
  private
    FASCIIFile: TrmASCIIFileHolder;
    function   ResultType: TrwResultFormatType; override;
    procedure  RenderASCIIForm(AASCIIForm: TrmASCIIForm);

  // Interface implementation
    procedure   AppendASCIILine(const AText: String);
    procedure   InsertASCIILine(const AText: String; const ALine: Integer);
    procedure   UpdateASCIILine(const AText: String; const ALine: Integer);
    procedure   DeleteASCIILine(const ALine: Integer);
    procedure   CutOffUpToLine(const ALine: Integer);
    function    GetASCIILine(const ALine: Integer): String;
    function    ASCIILineCount: Integer;
  // End
  public
    constructor Create; override;
    destructor  Destroy; override;
  end;


  TrmXMLFileHolderItemType = (rmXFITUnknown, rmXFITelement, rmXFITelementTerm, rmXFITattribute, rmXFITtext,
    rmXFITrawdata, rmXFITcomment, rmXFITFragment);

  // virtual XML file
  TrmXMLFileHolder = class
    FHandle: TsbTableCursor;
    FFileName: String;
    procedure StoreDataToFile;
    function  CurrentItemType: TrmXMLFileHolderItemType;
    function  CurrentItemData: String;
  public
    function    IsEmpty: Boolean;
    function    AppendItem(const AItemType: TrmXMLFileHolderItemType; const AData: String): TxmlDocNodeHandle;
    function    LastItemPos: Integer;
    procedure   CutOff(const ANewLastItemPos: Integer);
    constructor Create(const AFileName: String);
    destructor  Destroy; override;
  end;


  TrmXMLRenderEngine = class(TrmCustomRenderEngine, IrmXMLRenderEngine)
  private
    FXMLFile:  TrmXMLFileHolder;
    FXMLForm:  TrmXMLForm;
    function   ResultType: TrwResultFormatType; override;
    procedure  RenderXMLForm(const AXMLForm: TrmXMLForm);
    procedure  ValidateResult;

  // Interface implementation
    function  AddElement(const AElementName: String): TxmlDocNodeHandle;
    procedure AddElementTerminator;
    function  AddAttribute(const AAttributeName: String; const AValue: String): TxmlDocNodeHandle;
    function  AddText(const AText: String): TxmlDocNodeHandle;
    function  AddComment(const AComment: String): TxmlDocNodeHandle;
    function  AddFragment(const AText: String): TxmlDocNodeHandle;
    procedure CutOffUpToPos(const AItemPos: Integer);
    function  LastItemPos: Integer;

  // End

  public
    constructor Create; override;
    destructor  Destroy; override;
  end;



  TrmRenderer = class(TInterfacedObject, IrmReportRenderer)
  private
    FReport: TrmReport;
    FResults: array of IrwResult;
    FRWARenderEngine: IrmRWARenderEngine;
    FASCIIRenderEngine: IrmASCIIRenderEngine;
    FXMLRenderEngine: IrmXMLRenderEngine;
    procedure RenderReport;

  // Interface implementation
    function  Render: IrwResult;
    function  RWARenderEngine: IrmRWARenderEngine;
    function  ASCIIRenderEngine: IrmASCIIRenderEngine;
    function  XMLRenderEngine: IrmXMLRenderEngine;
  // end

  public
    constructor Create(AReport: TObject);
    destructor  Destroy; override;
  end;


implementation

uses rwReport;


{ TrmRenderer }

function TrmRenderer.ASCIIRenderEngine: IrmASCIIRenderEngine;
begin
  if not Assigned(FASCIIRenderEngine) then
  begin
    FASCIIRenderEngine := TrmASCIIRenderEngine.Create;
    SetLength(FResults, Length(FResults) + 1);
    FResults[High(FResults)] := TrmCustomRenderEngine(FASCIIRenderEngine.RealObject).FResult;
  end;
  Result := FASCIIRenderEngine;
end;

constructor TrmRenderer.Create(AReport: TObject);
begin
  FReport := AReport as TrmReport;
end;

destructor TrmRenderer.Destroy;
begin
  FRWARenderEngine := nil;
  FASCIIRenderEngine := nil;
  FXMLRenderEngine := nil;
  inherited;
end;

function TrmRenderer.Render: IrwResult;
begin
  RenderReport;
  if Length(FResults) > 0 then
    Result := FResults[0]  // for now
  else
    Result := nil;  
end;

procedure TrmRenderer.RenderReport;
var
  i: Integer;
  Forms: IisStringList;
  FormName: Variant;
  Obj: TObject;
begin
  Forms := TisStringList.Create;

  // Enlist all Forms
  for i := 0 to FReport.ComponentCount - 1 do
    if (FReport.Components[i] is TrmPrintForm) or
       (FReport.Components[i] is TrmASCIIForm) or
       (FReport.Components[i] is TrmXMLForm) then
      Forms.Add(FReport.Components[i].Name);

  // Render all forms
  while True do
  begin
    if Forms.Count > 0 then
      FormName := Forms[0]
    else
      FormName := '';

    FReport.ExecuteEventsHandler('BeforeProcessForm', [Integer(Addr(FormName))]);

    if FormName = '' then
      break;

    Obj := FReport.FindComponent(FormName);
    if not Assigned(Obj) then
      raise ErwException.Create('Form "' + FormName + '" is not found');

    if Obj is TrmPrintForm then
      TrmRWARenderEngine(RWARenderEngine.RealObject).RenderPrintForm(TrmPrintForm(Obj))

    else if Obj is TrmASCIIForm then
      TrmASCIIRenderEngine(ASCIIRenderEngine.RealObject).RenderASCIIForm(TrmASCIIForm(Obj))

    else if Obj is TrmXMLForm then
      (XMLRenderEngine.RealObject as TrmXMLRenderEngine).RenderXMLForm(TrmXMLForm(Obj));

    i := Forms.IndexOf(FormName);
    if i <> -1 then
      Forms.Delete(i);

    FReport.ExecuteEventsHandler('AfterProcessForm', [FormName]);
  end;
end;

function TrmRenderer.RWARenderEngine: IrmRWARenderEngine;
begin
  if not Assigned(FRWARenderEngine) then
  begin
    FRWARenderEngine := TrmRWARenderEngine.Create;
    SetLength(FResults, Length(FResults) + 1);
    FResults[High(FResults)] := TrmCustomRenderEngine(FRWARenderEngine.RealObject).FResult;
  end;
  Result := FRWARenderEngine;
end;


function TrmRenderer.XMLRenderEngine: IrmXMLRenderEngine;
begin
  if not Assigned(FXMLRenderEngine) then
  begin
    FXMLRenderEngine := TrmXMLRenderEngine.Create;
    SetLength(FResults, Length(FResults) + 1);
    FResults[High(FResults)] := TrmCustomRenderEngine(FXMLRenderEngine.RealObject).FResult;
  end;
  Result := FXMLRenderEngine;
end;


{ TrmCustomRenderEngine }

constructor TrmCustomRenderEngine.Create;
var
  fn: String;
begin
  fn := GetUniqueFileName(GetTmpDir);
  FResult := TrwResultHolder.Create(fn, ResultType, False);
end;

destructor TrmCustomRenderEngine.Destroy;
begin
  SealUpResult;
  // Do not destroy FResult in here!
  inherited;
end;

function TrmCustomRenderEngine.RealObject: TObject;
begin
  Result := Self;
end;

procedure TrmCustomRenderEngine.SealUpResult;
begin
  FResult.Data.Position := 0;
  FResult.ReleaseFileHandle;
end;

procedure TrmCustomRenderEngine.EmptyResult;
begin
  FRenderingStopped := True;
  FResult.Data.Size := 0;
end;


{ TrmRWARenderEngine }

constructor TrmRWARenderEngine.Create;
begin
  inherited;
  FMacrosPos :=  TList.Create;
  FPage := TrwVirtualPage.Create(nil);
end;

destructor TrmRWARenderEngine.Destroy;
begin
  inherited;
  FreeAndNil(FMacrosPos);  
  FreeAndNil(FPage);
end;

function TrmRWARenderEngine.ResultType: TrwResultFormatType;
begin
  Result := rwRTRWA;
end;

procedure TrmRWARenderEngine.AddPrintObjectToResult(AObject: TrwPrintObject);
begin
  AObject.WriteObjectInfo(FPage.Data);
end;

function TrmRWARenderEngine.CompareSimpleObj(AObject: TrwPrintObject; ACompare: Boolean; ALayerNumber: Integer): Boolean;
begin
  with AObject do
    Result := (Compare = ACompare) and (LayerNumber = ALayerNumber);
end;

function TrmRWARenderEngine.ObjectRepository: TrwPrintRepository;
begin
  Result := FPage.Repository;
end;

procedure TrmRWARenderEngine.SetSimpleObj(AObject: TrwPrintObject; ACompare: Boolean; ALayerNumber: Integer);
begin
  with AObject do
  begin
    Compare := ACompare;
    LayerNumber := ALayerNumber;
  end;
end;

procedure TrmRWARenderEngine.SealUpResult;
begin
  FPage.Header.PixelsPerInch := cVirtualCanvasRes;
  FPage.Repository.PixelsPerInch := cVirtualCanvasRes;
  FPage.Header.ReposList.Add(Pointer(FResult.Data.Position));
  FResult.Data.WriteComponent(FPage.Repository);
  FPage.WriteHeaderInfo(FResult.Data);

  inherited;
end;

procedure TrmRWARenderEngine.AddFrame(ABounds: TRect; ACompare: Boolean;
  ALayerNumber: Integer; AColor: TColor; ABoundLines: TrwSetColumnLines;
  ABoundLinesColor: TColor; ABoundLinesWidth: Integer;
  ABoundLinesStyle: TPenStyle);
var
  i: Integer;
  C: TrwPrintFrame;
  ObjExists: Boolean;
begin
  ObjExists := False;
  C := nil;

  with ObjectRepository do
    for i := 0 to ComponentCount - 1 do
      if  Components[i] is TrwPrintFrame then
      begin
        C := TrwPrintFrame(Components[i]);

        ObjExists := CompareSimpleObj(C, ACompare, ALayerNumber) and (C.Color = AColor) and
              ((C.BoundLines = []) and (C.BoundLines = ABoundLines) or
               (C.BoundLines = ABoundLines) and
               (C.BoundLinesColor = ABoundLinesColor) and
               (C.BoundLinesWidth = ABoundLinesWidth) and
               (C.BoundLinesStyle = ABoundLinesStyle)
              );

        if ObjExists then
          break;
      end;

  if not ObjExists then
  begin
    C := TrwPrintFrame.Create(ObjectRepository);
    SetSimpleObj(C, ACompare, ALayerNumber);
    with C do
    begin
      Color := AColor;
      BoundLines := ABoundLines;
      BoundLinesColor := ABoundLinesColor;
      BoundLinesWidth := ABoundLinesWidth;
      BoundLinesStyle := ABoundLinesStyle;
    end;
  end;
  C.BoundsRect := ABounds;

  AddPrintObjectToResult(C);
end;

procedure TrmRWARenderEngine.AddLine(X1, X2, Y1, Y2: Integer;
  ACompare: Boolean; ALayerNumber: Integer; ALineColor: TColor;
  ALineWidth: Integer; ALineStyle: TPenStyle);
var
  i: Integer;
  C: TrwPrintLine;
  ObjExists: Boolean;
begin
  ObjExists := False;
  C := nil;

  with ObjectRepository do
    for i := 0 to ComponentCount - 1 do
      if  Components[i] is TrwPrintLine then
      begin
        C := TrwPrintLine(Components[i]);

        ObjExists := CompareSimpleObj(C, ACompare, ALayerNumber) and
               (C.LineColor = ALineColor) and (C.LineWidth = ALineWidth) and (C.LineStyle = ALineStyle);

        if ObjExists then
          break;
      end;

  if not ObjExists then
  begin
    C := TrwPrintLine.Create(ObjectRepository);
    SetSimpleObj(C, ACompare, ALayerNumber);
    with C do
    begin
      LineColor := ALineColor;
      LineWidth := ALineWidth;
      LineStyle := ALineStyle;
    end;
  end;
  C.BoundsRect := Rect(X1, X2, Y1, Y2);

  AddPrintObjectToResult(C);
end;


procedure TrmRWARenderEngine.AddPicture(ABounds: TRect; ACompare: Boolean;
  ALayerNumber: Integer; ATransparent: Boolean; APicture: TrwPicture;
  AStretch: Boolean);
var
  i: Integer;
  C: TrwPrintImage;
  ObjExists: Boolean;
  MS1, MS2: TMemoryStream;
begin
  if (APicture.Width = 0) and (APicture.Height = 0) then
    Exit;

  ObjExists := False;
  C := nil;

  MS1 := TIsMemoryStream.Create;
  MS2 := TIsMemoryStream.Create;
  try
    APicture.Graphic.SaveToStream(MS1);

    with ObjectRepository do
      for i := 0 to ComponentCount - 1 do
        if  Components[i] is TrwPrintImage then
        begin
          C := TrwPrintImage(Components[i]);

          ObjExists := CompareSimpleObj(C, ACompare, ALayerNumber) and (C.Transparent = ATransparent) and (C.Stretch = AStretch);
          if ObjExists and (APicture.Height = C.Picture.Height) and (APicture.Width = C.Picture.Width) then
          begin
            MS2.Clear;
            C.Picture.Graphic.SaveToStream(MS2);
            ObjExists := CompareMem(MS1.Memory, MS2.Memory, MS1.Size);
          end
          else
            ObjExists := False;

          if ObjExists then
            break;
        end;
  finally
    FreeAndNil(MS1);
    FreeAndNil(MS2);
  end;

  if not ObjExists then
  begin
    C := TrwPrintImage.Create(ObjectRepository);
    SetSimpleObj(C, ACompare, ALayerNumber);
    with C do
    begin
      Picture.Assign(APicture);
      Stretch := AStretch;
      Transparent := ATransparent;
    end;
  end;

  C.BoundsRect := ABounds;

  AddPrintObjectToResult(C);
end;

procedure TrmRWARenderEngine.AddText(ABounds: TRect; ACompare: Boolean;
  ALayerNumber: Integer; AAlignment: TAlignment; ALayout: TTextLayout;
  AColor: TColor; AWordWrap: Boolean; const ADigitNet: String;
  AFont: TrwFont; const AText: String);
var
  i: Integer;
  C: TrwPrintText;
  ObjExists: Boolean;
begin
  ObjExists := False;
  C := nil;

  with ObjectRepository do
    for i := 0 to ComponentCount - 1 do
      if  Components[i] is TrwPrintText then
      begin
        C := TrwPrintText(Components[i]);

        ObjExists := CompareSimpleObj(C, ACompare, ALayerNumber) and
          (C.Alignment = AAlignment) and
          (C.Layout = ALayout) and
          (C.Color = AColor) and
          (C.WordWrap = AWordWrap) and
          AnsiSameStr(C.DigitNet.Text, ADigitNet) and
          (C.Font.Charset = AFont.Charset) and
          (C.Font.Color = AFont.Color) and
          (C.Font.Height = AFont.Height) and
          AnsiSameStr(C.Font.Name, AFont.Name) and
          (C.Font.Pitch = AFont.Pitch) and
          (C.Font.Style = AFont.Style);

        if ObjExists then
          break;
      end;

  if not ObjExists then
  begin
    C := TrwPrintText.Create(ObjectRepository);
    SetSimpleObj(C, ACompare, ALayerNumber);
    with C do
    begin
      Color := AColor;
      Alignment := AAlignment;
      Layout := ALayout;
      Color := AColor;
      WordWrap := AWordWrap;
      DigitNet.Text := ADigitNet;
      Font.Assign(AFont);
    end;
  end;

  C.Text := AText;

  C.BoundsRect := ABounds;

  AddPrintObjectToResult(C);
end;

procedure TrmRWARenderEngine.StorePageDataToResult;
begin
  FPage.Header.PagesList.Add(nil);
  FPage.PageNum := FPage.Header.PagesCount;
  FPage.Header.PagesList[FPage.Header.PagesCount - 1] := Pointer(FResult.Data.Position);
  FResult.Data.WriteComponent(FPage);
  FPage.Data.Size := 0;
end;

procedure TrmRWARenderEngine.RenderPrintForm(APrintForm: TrmPrintForm);
begin
  if FRenderingStopped then
    Exit;

  APrintForm.SetRendStatusChildrenToo(rmRWARSNone);  // Prepare Print Form for printing
  APrintForm.Calculate;

  if APrintForm.RendStatus = rmRWARSCalculated then
  begin
    repeat
      FPage.PageSize := APrintForm.GetPaperSize;
      FPage.Width := APrintForm.Width;
      FPage.Height := APrintForm.Height;
      FPage.PageOrientation := APrintForm.PaperOrientation;
      FPage.Duplexing := APrintForm.Duplexing;

      APrintForm.Print;
    until APrintForm.RendStatus = rmRWARSPrinted;
  end

  else if APrintForm.Enabled and APrintForm.BlockParentIfEmpty then
    EmptyResult;
end;


procedure TrmRWARenderEngine.EmptyResult;
begin
  inherited;
  ObjectRepository.DestroyComponents;
  FPage.Header.PagesList.Clear;
  FPage.PageNum := 0;
  FPage.Data.Size := 0;
end;


function TrmRWARenderEngine.CurrentPageNumber: Integer;
begin
  Result := FPage.Header.PagesCount + 1;
end;

{ TrmASCIIRenderEngine }

procedure TrmASCIIRenderEngine.AppendASCIILine(const AText: String);
begin
  FASCIIFile.Append(AText);
end;

function TrmASCIIRenderEngine.ASCIILineCount: Integer;
begin
  Result := FASCIIFile.LineCount;
end;

constructor TrmASCIIRenderEngine.Create;
begin
  inherited;
  FASCIIFile := TrmASCIIFileHolder.Create(FResult.FileName);
end;

procedure TrmASCIIRenderEngine.CutOffUpToLine(const ALine: Integer);
begin
  FASCIIFile.CutOff(ALine);
end;

procedure TrmASCIIRenderEngine.DeleteASCIILine(const ALine: Integer);
begin
  FASCIIFile.Delete(ALine);
end;

destructor TrmASCIIRenderEngine.Destroy;
begin
  inherited;
  FreeAndNil(FASCIIFile);
end;

function TrmASCIIRenderEngine.GetASCIILine(const ALine: Integer): String;
begin
  Result := FASCIIFile.GetLine(ALine);
end;

procedure TrmASCIIRenderEngine.InsertASCIILine(const AText: String; const ALine: Integer);
begin
  FASCIIFile.Insert(AText, ALine);
end;

procedure TrmASCIIRenderEngine.RenderASCIIForm(AASCIIForm: TrmASCIIForm);
var
  MiscInfo: TrwResultMiscInfo;
begin
  if FRenderingStopped then
    Exit;

  AASCIIForm.SetRendStatusChildrenToo(rmASCIIRSNone);
  AASCIIForm.CalculateBranch;

  if AASCIIForm.Enabled and AASCIIForm.BlockParentIfEmpty and (AASCIIForm.RendStatus <> rmASCIIRSCalculated) then
    EmptyResult;

  Initialize(MiscInfo);
  MiscInfo.DefaultFileName := AASCIIForm.DefaultFileName;
  FResult.SetMiscInfo(MiscInfo);
  if AASCIIForm.ResultType = rmARTXls then
    FASCIIFile.ConvertToXls := true;
  FASCIIFile.LastLineBreak := AASCIIForm.LastLineBreak;
end;

function TrmASCIIRenderEngine.ResultType: TrwResultFormatType;
begin
  Result := rwRTASCII;
  if Assigned(FASCIIFile) and FASCIIFile.ConvertToXls then
    Result := rwRTXLS;
end;


procedure TrmASCIIRenderEngine.UpdateASCIILine(const AText: String; const ALine: Integer);
begin
  FASCIIFile.Update(AText, ALine);
end;

{ TrmASCIIFileHolder }

procedure TrmASCIIFileHolder.Append(const AText: String);
begin
  FHandle.Last;
  FHandle.Append;
  try
    FHandle.Fields[0].AsString := AText;
    FHandle.Post;
  except
    FHandle.Cancel;
    raise;
  end;
end;

constructor TrmASCIIFileHolder.Create(const AFileName: String);
var
  Flds: TsbFields;
begin
  FFileName := AFileName;

  Flds := TsbFields.Create;
  try
    Flds.CreateField('ASCIILine', sbfBlob);
    sbCreateTable(FFileName, Flds); // substitutes file extension with ".sbt"
    FHandle := sbOpenTable(AFileName, nil, sbmReadWrite, True);
  finally
    Flds.Free;
  end;
end;

procedure TrmASCIIFileHolder.CutOff(const ANewLineCount: Integer);
begin
  FHandle.Last;
  while FHandle.RecordCount > ANewLineCount do
    FHandle.Delete;
end;

procedure TrmASCIIFileHolder.Delete(const ALine: Integer);
begin
  SetCurrASCIILine(ALine);
  FHandle.Delete;
end;

destructor TrmASCIIFileHolder.Destroy;
begin
  StoreDataToFile;
  sbDropTable(FHandle);
  inherited;
end;

function TrmASCIIFileHolder.GetLine(const ALine: Integer): String;
begin
  SetCurrASCIILine(ALine);
  Result := FHandle.Fields[0].AsString;
end;

procedure TrmASCIIFileHolder.Insert(const AText: String; const ALine: Integer);
begin
  SetCurrASCIILine(ALine);
  FHandle.Insert;
  try
    FHandle.Fields[0].AsString := AText;
    FHandle.Post;
  except
    FHandle.Cancel;
    raise;
  end;
end;

function TrmASCIIFileHolder.LineCount: Integer;
begin
  Result := FHandle.RecordCount;
end;

procedure TrmASCIIFileHolder.SetCurrASCIILine(const ALine: Integer);
begin
  if (ALine <= 0) or (ALine > LineCount) then
    raise ErwException.Create('Requested ASCII line #' + IntToStr(ALine) + ' is out of bounds')
  else
    FHandle.RecNo := ALine;
end;

procedure TrmASCIIFileHolder.StoreDataToFile;
var
  tmpFileName: String;

  procedure StoreToFile(const AFileName: String);
  var
    F: TextFile;
  begin
    AssignFile(F, AFileName);
    try
      Rewrite(F);
      FHandle.First;
      while not FHandle.Eof do
      begin
        if (not FConvertToXls) and (not FLastLineBreak) and (FHandle.RecNo = FHandle.RecordCount) then
          Write(F, FHandle.Fields[0].AsString)
        else
          Writeln(F, FHandle.Fields[0].AsString);
        FHandle.Next;
      end;
    finally
      CloseFile(F);
    end;
  end;
begin
  if not FConvertToXls then
    StoreToFile(FFileName)
  else
  begin
    tmpFileName := sbGetUniqueFileName(GetThreadTmpDir, ChangeFileExt(ExtractFileName(FFileName), ''), '.txt');
    StoreToFile(tmpFileName);
    try
      ConvertASCII2Xls(tmpFileName, FFileName, Ascii2XlsDelimiter, Ascii2XlsQualifier);
    finally
      DeleteFile(tmpFileName);
    end;
  end;
end;

procedure TrmASCIIFileHolder.Update(const AText: String; const ALine: Integer);
begin
  SetCurrASCIILine(ALine);
  FHandle.Edit;
  try
    FHandle.Fields[0].AsString := AText;
    FHandle.Post;
  except
    FHandle.Cancel;
    raise;
  end;
end;

{ TrmXMLRenderEngine }

constructor TrmXMLRenderEngine.Create;
begin
  inherited;
  FXMLFile := TrmXMLFileHolder.Create(FResult.FileName);
end;

destructor TrmXMLRenderEngine.Destroy;
begin
  inherited;
  FreeAndNil(FXMLFile);
end;

procedure TrmXMLRenderEngine.RenderXMLForm(const AXMLForm: TrmXMLForm);
var
  MiscInfo: TrwResultMiscInfo;
begin
  if FRenderingStopped or (AXMLForm.TopElement = nil) then
    Exit;

  FXMLForm := AXMLForm;
  FXMLForm.SetRendStatusChildrenToo(rmXMLRSNone);
  FXMLForm.Calculate;

  FXMLFile.StoreDataToFile;
  ValidateResult;

  if FXMLForm.Enabled and FXMLForm.BlockParentIfEmpty and (FXMLForm.RendStatus <> rmXMLRSCalculated) then
    EmptyResult;

  Initialize(MiscInfo);
  MiscInfo.DefaultFileName := FXMLForm.DefaultFileName;
  FResult.SetMiscInfo(MiscInfo);
end;

function TrmXMLRenderEngine.ResultType: TrwResultFormatType;
begin
  Result := rwRTXML;
end;

function TrmXMLRenderEngine.AddElement(const AElementName: String): TxmlDocNodeHandle;
var
  AddNamespaceInfo: Boolean;
  s: String;
begin
  AddNamespaceInfo := FXMLFile.IsEmpty;

  Result := FXMLFile.AppendItem(rmXFITelement, AElementName);

  if AddNamespaceInfo then
  begin
    if FXMLForm.NameSpace <> '' then
    begin
      s := 'xmlns';
      if FXMLForm.NameSpaceAlias <> '' then
        s := s + ':' + FXMLForm.NameSpaceAlias;
      AddAttribute(s, FXMLForm.NameSpace);
    end;
    AddAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  end;  
end;

function TrmXMLRenderEngine.AddAttribute(const AAttributeName: String; const AValue: String): TxmlDocNodeHandle;
begin
  Result := FXMLFile.AppendItem(rmXFITattribute, AAttributeName + '="' + AValue + '"');
end;

function TrmXMLRenderEngine.AddComment(const AComment: String): TxmlDocNodeHandle;
begin
  Result := FXMLFile.AppendItem(rmXFITcomment, AComment);
end;

function TrmXMLRenderEngine.AddText(const AText: String): TxmlDocNodeHandle;
begin
  Result := FXMLFile.AppendItem(rmXFITtext, AText);
end;

procedure TrmXMLRenderEngine.AddElementTerminator;
begin
  FXMLFile.AppendItem(rmXFITelementTerm, '');
end;

procedure TrmXMLRenderEngine.ValidateResult;
var
  xmldoc: IXMLDOMDocument2;
  schemacache: IXMLDOMSchemaCollection;
  error: IXMLDOMParseError;
  Err: String;
begin
  if FXMLForm.Schemas.Count = 0 then
    Exit;

  FXMLForm.Schemas.BeginWorkWithItems;
  try
    // Register the schema with the cache
    schemacache := CoXMLSchemaCache60.Create;
    schemacache.add(FXMLForm.NameSpace, FXMLForm.Schemas[FXMLForm.Schemas.Count - 1].SchemaFileName);

    // Load the data
    xmldoc := CreateXMLDoc;
    xmldoc.async := False;
    xmldoc.load(FResult.FileName);
    xmldoc.schemas := schemacache;

    // Validate and report
    error := xmldoc.validate;

  finally
    FXMLForm.Schemas.EndWorkWithItems;
  end;

  if error.errorCode <> S_OK then
  begin
    Err := error.reason + #13#13 + 'Line: ' + IntToStr(error.Line) + '   Pos:' + IntToStr(error.LinePos) + #13#13 + error.srcText;

    if RWDesigning then
      ShowMessage(Err)
    else
      raise ErwException.Create(Err);
  end;
end;

procedure TrmXMLRenderEngine.CutOffUpToPos(const AItemPos: Integer);
begin
  FXMLFile.CutOff(AItemPos);
end;

function TrmXMLRenderEngine.LastItemPos: Integer;
begin
  Result := FXMLFile.LastItemPos;
end;

function TrmXMLRenderEngine.AddFragment(const AText: String): TxmlDocNodeHandle;
begin
  Result := FXMLFile.AppendItem(rmXFITFragment, AText);
end;

{ TrmXMLFileHolder }

function TrmXMLFileHolder.AppendItem(const AItemType: TrmXMLFileHolderItemType; const AData: String): TxmlDocNodeHandle;
begin
  FHandle.Last;
  FHandle.Append;
  try
    FHandle.Fields[0].AsByte := Byte(Ord(AItemType));
    FHandle.Fields[1].AsString := AData;
    FHandle.Post;

    Result := FHandle.InternalRecordPos;
  except
    FHandle.Cancel;
    raise;
  end;
end;

constructor TrmXMLFileHolder.Create(const AFileName: String);
var
  Flds: TsbFields;
begin
  FFileName := AFileName;

  Flds := TsbFields.Create;
  try
    Flds.CreateField('ItemType', sbfByte);  
    Flds.CreateField('Content', sbfBlob);
    sbCreateTable(FFileName, Flds); // substitutes file extension with ".sbt"
    FHandle := sbOpenTable(AFileName, nil, sbmReadWrite, True);
  finally
    Flds.Free;
  end;
end;

function TrmXMLFileHolder.CurrentItemData: String;
begin
  Result := FHandle.Fields[1].AsString;
end;

function TrmXMLFileHolder.CurrentItemType: TrmXMLFileHolderItemType;
begin
  Result := TrmXMLFileHolderItemType(FHandle.Fields[0].AsByte);
end;

procedure TrmXMLFileHolder.CutOff(const ANewLastItemPos: Integer);
begin
  FHandle.Last;
  while FHandle.RecordCount > ANewLastItemPos do
    FHandle.Delete;
end;

destructor TrmXMLFileHolder.Destroy;
begin
  sbDropTable(FHandle);
  inherited;
end;

function TrmXMLFileHolder.IsEmpty: Boolean;
begin
  Result := FHandle.RecordCount = 0;
end;

function TrmXMLFileHolder.LastItemPos: Integer;
begin
  Result := FHandle.RecordCount;
end;

procedure TrmXMLFileHolder.StoreDataToFile;
var
  F: TextFile;

  function EndOfFile: Boolean;
  begin
    Result := FHandle.Eof;
  end;

  procedure GoToNextItem;
  begin
    FHandle.Next;
  end;

  function MakeIndent(const IndentNbr: Byte): String;
  begin
    if IndentNbr > 0 then
      Result := StringOfChar(' ', IndentNbr * 2)
    else
      Result := '';
  end;


  procedure WriteElementData(const ALevel: Byte);
  var
    ElementName, Indent: String;
    ChildElementsWritten: Boolean;
  begin
    ElementName := CurrentItemData;
    Indent := MakeIndent(ALevel);
    Write(F,  Indent + '<' + ElementName);

    ChildElementsWritten := False;

    GoToNextItem;

    while not EndOfFile do
    begin
      if CurrentItemType = rmXFITelement then
      begin
        if not ChildElementsWritten then
          Writeln(F, '>');
        WriteElementData(ALevel + 1);
        ChildElementsWritten := True;
      end

      else if CurrentItemType = rmXFITattribute then
        Write(F, ' ' + CurrentItemData)

      else if CurrentItemType = rmXFITtext then
      begin
        Writeln(F, '>' + CurrentItemData + '</' + ElementName + '>');
        break;
      end

      else if CurrentItemType = rmXFITelementTerm then
      begin
        if ChildElementsWritten then
          Writeln(F, Indent + '</' + ElementName + '>')
        else
          Writeln(F, '/>');
        break;
      end

      else if CurrentItemType = rmXFITFragment then
      begin
        if not ChildElementsWritten then
        begin
          Writeln(F, '>');
          ChildElementsWritten := True;
        end;
        Writeln(F, CurrentItemData);
      end

      else
        rwError('Unexpected XML item type ' + IntToStr(Ord(CurrentItemType)));

      GoToNextItem;
    end;
  end;

begin
  AssignFile(F, FFileName);
  try
    Rewrite(F);

    if FHandle.RecordCount > 0 then
    begin
      Writeln(F, '<?xml version="1.0" encoding="UTF-8"?>');

      FHandle.First;
      while not EndOfFile do
      begin
        if CurrentItemType = rmXFITelement then
          WriteElementData(0);
        GoToNextItem;
      end;
    end;  

  finally
    CloseFile(F);
  end;
end;


end.
