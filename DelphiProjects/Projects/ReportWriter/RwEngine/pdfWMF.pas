{**************************************************}
{                                                  }
{                   llPDFLib                       }
{            Version  2.3,  10.04.2003             }
{      Copyright (c) 2002, 2003 llionsoft          }
{             All rights reserved                  }
{            mailto:info@llion.net                 }
{                                                  }
{**************************************************}
{$I pdf.inc}


unit pdfWMF;
interface
uses windows, classes, sysutils, graphics, pdfResources, PDF, pdfMisc, EvStreamUtils
     ,isBaseClasses
     ;

type
  TPDFPen = record
    lopnStyle: UINT;
    lopnWidth: Extended;
    lopnColor: COLORREF;
  end;

  TExtRect = record
    Left, Top, Right, Bottom: Extended;
  end;

  TEMWParser = class
  private
    FiImagePosList: IIsStringList;  // SYS-436
    FTransparentRenderMetafile: TMetafile;
    CurWidth: TLargeWidthArray;
    MapMode: Integer;
    FontInited: Boolean;
    FPage: TPDFPage;
    Cal: Extended;
    FontScale: Extended;
    Meta: TMetafile;
    MetaCanvas: TMetafileCanvas;
    DC: HDC;
    MS: TMemoryStream;
    CV: Pointer;
    CurVal: TPoint;
    PolyFIllMode: Boolean;
    BGMode: Boolean;
    TextColor: Cardinal;
    BGColor: Cardinal;
    VertMode: TVertJust;
    HorMode: THorJust;
    UpdatePos: Boolean;
    Clipping: Boolean;
    CCW: Boolean;
    CPen: TPDFPen;
    CBrush: TLogBrush;
    CFont: TLogFont;
    CurFill: Cardinal;
    ClipRect: TExtRect;
    isCR: Boolean;
    FInText: Boolean;
    FInPath: Boolean;
    MFH: THandle;
    SBM: Integer;
    com: Integer;
    DDC: Boolean;
    IsNullBrush: Boolean;
    CWPS: TSize;
    CurRec: Integer;
    RE: TRect;
    FCha: Boolean;
    FCCha: Boolean;
    BCha: Boolean;
    PCha: Boolean;
    Off: Integer;
    FEX: Boolean;
    NT: Boolean;
    XF: TXForm;
    TransfStack: array of TXForm;
    StackSize: Integer;
    XOff, YOff, XScale, YScale: Extended;
    WOX, VOX, WEX, VEX, WOY, VOY, WEY, VEY: Integer;
    HandlesCount: DWORD;
    WNG: Boolean;
    HandleTable: array of HGDIOBJ;
{$IFDEF CANVASDBG}
    LastRecordInContents: Integer;
    TXTStr: string;
    Debug: TStringList;
    procedure SaveToLog(EMRPointer: Pointer);
{$ENDIF}
    procedure PStroke;
    procedure PFillAndStroke;
    function GetTextWidth(Text: string): Extended;
    function MapX(Value: Extended): Extended;
    function MapY(Value: Extended): Extended;
    function FX: Extended;
    function FY: Extended;
    function GX(Value: Extended; Map: Boolean = True): Extended;
    function GY(Value: Extended; Map: Boolean = True): Extended;

    procedure SetCurFont;

    procedure DoSetWindowExtEx;
    procedure DoSetWindowOrgEx;
    procedure DoSetViewPortExtEx;
    procedure DoSetViewPortOrgEx;
    procedure DoSetMapMode;

    procedure DoPolyBezier; //
    procedure DoPolygon; //
    procedure DoPolyLine; //
    procedure DoPolyBezierTo; //
    procedure DoPolyLineTo; //
    procedure DoPolyPolyLine; //
    procedure DoPolyPolyGon; //
    procedure DoSetBKMode; //
    procedure DoSetPolyFillMode; //
    procedure DoSetTextAlign; //
    procedure DoSetTextColor; //
    procedure DoSetBKColor; //
    procedure DoMoveToEx; //
    procedure DoSetPixelV; //
    procedure DoInterSectClipRect; //
    procedure DoSaveDC; //
    procedure DoRestoreDC; //

    procedure DoSetWorldTransform; //
    procedure DoModifyWorldTransform;

    procedure DoSelectObject; //
    procedure DoCreatePen; //
    procedure DoCreateBrushInDirect; //
    procedure DoDeleteObject; //
    procedure DoAngleArc; //
    procedure DoEllipse; //
    procedure DoRectangle; //
    procedure DoRoundRect; //
    procedure DoArc; //
    procedure DoChord; //
    procedure DoPie; //
    procedure DoLineTo; //
    procedure DoArcTo; //
    procedure DoPolyDraw; //
    procedure DoSetArcDirection; //
    procedure DoSetMiterLimit; //
    procedure DoBeginPath; //
    procedure DoEndPath; //
    procedure DoCloseFigure; //
    procedure DoFillPath; //
    procedure DoStrokeAndFillPath; //
    procedure DoStrokePath; //
    procedure DoSelectClipPath; //
    procedure DoAbortPath; //
    procedure DoSetDibitsToDevice;
    procedure DoStretchDiBits;
    procedure DoCreateFontInDirectW; //

    procedure DoExtTextOut; //
    procedure DoSmallTextOut;

    procedure DoPolyBezier16; //
    procedure DoPolygon16; //
    procedure DoPolyLine16; //
    procedure DoPolyBezierTo16; //
    procedure DoPolyLineTo16; //
    procedure DoPolyPolyLine16; //
    procedure DoPolyPolygon16; //
    procedure DoPolyDraw16; //
    procedure DoSetTextJustification; //
    procedure DoExtCreatePen; //
    procedure DoExcludeClipRect;
    procedure DoExtSelectClipRGN;
    procedure DoBitBlt;
    procedure DoSetStretchBltMode;
    procedure DoStretchBlt;
    procedure SetInPath(const Value: Boolean);
    procedure SetInText(const Value: Boolean);
    procedure SetBrushColor;
    procedure SetPenColor;
    procedure SetFontColor;
    procedure SetBGColor;
    procedure ExecuteRecord(Data: Pointer);
    procedure InitExecute;
  protected
    property InText: Boolean read FInText write SetInText;
    property InPath: Boolean read FInPath write SetInPath;
  public
    constructor Create(APage: TPDFPage);
    procedure LoadMetaFile(MF: TMetafile);
    procedure Execute;
    function GetMax: TSize;
    destructor Destroy; override;
  end;

  TEMRSmallTextOut = packed record
    emr: TEMR;
    ptlReference: TPointL;
    nChars: DWORD;
    fOptions: DWORD;
    iGraphicsMode: DWORD;
    exScale: Single;
    eyScale: Single;
    Text: array[1..1] of Char;
  end;

  PEMRSmallTextOut = ^TEMRSmalltextOut;

implementation

uses math;

type
  TSmallPointArray = array[0..MaxInt div SizeOf(TSmallPoint) - 1] of TSmallPoint;
  PSmallPointArray = ^TSmallPointArray;
  TPointArray = array[0..MaxInt div SizeOf(TPoint) - 1] of TPoint;
  PPointArray = ^TPointArray;


{ TEMWParser }

function IP(Old: Pointer; sz: Integer): Pointer;
var
  v: PByte;
begin
  v := Old;
  Inc(v, sz);
  Result := Pointer(v);
end;


constructor TEMWParser.Create(APage: TPDFPage);
begin
  FPage := APage;
  MS := TIsMemoryStream.Create;
{$IFDEF CANVASDBG}
  Debug := TStringList.Create;
{$ENDIF}
  Meta := TMetafile.Create;
  FTransparentRenderMetafile := TMetafile.Create;
  MetaCanvas := TMetafileCanvas.Create(Meta, 0);
  Cal := FPage.Owner.Resolution / MetaCanvas.Font.PixelsPerInch;
  FiImagePosList := TisStringList.Create(); // sys-436; interface assignment
end;


destructor TEMWParser.Destroy;
begin
  MetaCanvas.Free;
  Meta.Free;
{$IFDEF CANVASDBG}
  Debug.Free;
{$ENDIF}
  MS.Free;
  FTransparentRenderMetafile.Free;
  inherited;
end;

procedure TEMWParser.DoAbortPath;
begin
  FPage.NewPath;
  InPath := False;
end;

procedure TEMWParser.DoAngleArc;
var
  Data: PEMRAngleArc;
begin
  Data := CV;
  FPage.MoveTo(GX(CurVal.x), GY(CurVal.y));
  FPage.LineTo(GX(Data^.ptlCenter.x + cos(Data^.eStartAngle * Pi / 180) * Data^.nRadius), GY(Data^.ptlCenter.y -
    sin(Data^.eStartAngle * Pi / 180) * Data^.nRadius));
  if Abs(Data^.eSweepAngle) >= 360 then
    FPage.Ellipse(GX(data^.ptlCenter.x - Integer(Data^.nRadius)), GY(data^.ptlCenter.y - Integer(Data^.nRadius)),
      GX(data^.ptlCenter.x + Integer(Data^.nRadius)), GY(data^.ptlCenter.y + Integer(Data^.nRadius))) else
    FPage.Arc(GX(data^.ptlCenter.x - Integer(Data^.nRadius)), GY(data^.ptlCenter.y - Integer(Data^.nRadius)),
      GX(data^.ptlCenter.x + Integer(Data^.nRadius)), GY(data^.ptlCenter.y + Integer(Data^.nRadius)),
      data^.eStartAngle, data^.eStartAngle + data^.eSweepAngle);
  CurVal := Point(Round(Data^.ptlCenter.x + cos((Data^.eStartAngle + Data^.eSweepAngle) * Pi / 180) * Data^.nRadius),
    Round(Data^.ptlCenter.y - sin((Data^.eStartAngle + Data^.eSweepAngle) * Pi / 180) * Data^.nRadius));
  if not InPath then PStroke;
end;

procedure TEMWParser.DoArc;
var
  Data: PEMRArc;
begin
  Data := CV;
  if CCW then FPage.Arc(GX(Data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
      GX(Data^.ptlStart.x), GY(Data^.ptlStart.y), GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y))
  else
    FPage.Arc(GX(Data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
      GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y), GX(Data^.ptlStart.x), GY(Data^.ptlStart.y));
  if not InPath then PStroke;
end;

procedure TEMWParser.DoArcTo;
var
  Data: PEMRArc;
  CenterX, CenterY: Extended;
  RadiusX, RadiusY: Extended;
  StartAngle, EndAngle: Extended;
begin
  Data := CV;
  FPage.MoveTo(GX(CurVal.x), GY(CurVal.y));
  if not CCW then
  begin
    swp(Data^.ptlStart.x, Data^.ptlEnd.x);
    swp(Data^.ptlStart.y, Data^.ptlEnd.y);
  end;
  CenterX := (Data^.rclBox.Left + Data^.rclBox.Right) / 2;
  CenterY := (Data^.rclBox.Top + Data^.rclBox.Bottom) / 2;
  RadiusX := (abs(Data^.rclBox.Left - Data^.rclBox.Right) - 1) / 2;
  RadiusY := (abs(Data^.rclBox.Top - Data^.rclBox.Bottom) - 1) / 2;
  if RadiusX < 0 then RadiusX := 0;
  if RadiusY < 0 then RadiusY := 0;
  StartAngle := ArcTan2(-(Data^.ptlStart.y - CenterY) * RadiusX,
    (Data^.ptlStart.x - CenterX) * RadiusY);
  EndAngle := ArcTan2(-(Data^.ptlEnd.y - CenterY) * RadiusX,
    (Data^.ptlEnd.x - CenterX) * RadiusY);
  FPage.LineTo(GX(CenterX + RadiusX * cos(StartAngle)), GY(CenterY - RadiusY * sin(StartAngle)));
  FPage.Arc(GX(Data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
    GX(Data^.ptlStart.x), GY(Data^.ptlStart.y), GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y));
  CurVal := Point(round(CenterX + RadiusX * cos(EndAngle)), Round(CenterY - RadiusY * sin(StartAngle)));
  if not InPath then PStroke;
end;

procedure TEMWParser.DoBeginPath;
begin
  InPath := True;
  FPage.NewPath;
  FPage.MoveTo(GX(CurVal.x), GY(CurVal.y));
end;

procedure TEMWParser.DoBitBlt;
var
  Data: PEMRBitBlt;
  it: Boolean;
begin
  Data := CV;
  if InText then
  begin
    InText := False;
    it := True;
  end else it := False;
  if (data^.cxDest = 0) or (data^.cyDest = 0) then FPage.NewPath
  else
  begin
    FPage.Rectangle(gX(data^.xDest), gY(Data^.yDest), gX(Data^.xDest + Data^.cxDest), gY(Data^.yDest + Data^.cyDest));
    FPage.Fill;
  end;
  if it then InText := True;
end;

procedure TEMWParser.DoChord;
var
  Data: PEMRChord;
  DP: TDoublePoint;
begin
  Data := CV;
  if CCW then DP := FPage.Arc(GX(Data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
      GX(Data^.ptlStart.x), GY(Data^.ptlStart.y), GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y))
  else
    DP := FPage.Arc(GX(Data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
      GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y), GX(Data^.ptlStart.x), GY(Data^.ptlStart.y));
  FPage.LineTo(GX(DP.x), GY(DP.y));
  if not InPath then PFillAndStroke;
end;

procedure TEMWParser.DoCloseFigure;
begin
  FPage.ClosePath;
end;

procedure TEMWParser.DoCreateBrushInDirect;
var
  Data: PEMRCreateBrushIndirect;
begin
  Data := CV;
  if Data^.ihBrush >= HandlesCount then Exit;
  if HandleTable[Data^.ihBrush] <> $FFFFFFFF then DeleteObject(HandleTable[Data^.ihBrush]);
  HandleTable[Data^.ihBrush] := CreateBrushIndirect(Data^.lb);
end;

procedure TEMWParser.DoCreateFontInDirectW;
var
  Data: PEMRExtCreateFontIndirect;
  A: TLogFontW;
  F: TLogFont;
  At: array[0..31] of Char;
begin
  Data := CV;
  if Data^.ihFont >= HandlesCount then Exit;
  if HandleTable[Data^.ihFont] <> $FFFFFFFF then DeleteObject(HandleTable[Data^.ihFont]);
  Move(data^.elfw.elfLogFont, A, SizeOf(A));
  Move(a, F, SizeOf(F));
//  if F.lfCharSet=1 then F.lfCharSet:=GetDefFontCharSet;
  WideCharToMultiByte(1252, 0, A.lfFaceName, 32, At, 32, nil, nil);
  Move(AT, F.lfFaceName, 32);
  HandleTable[Data^.ihFont] := CreateFontIndirect(F);
end;

procedure TEMWParser.DoCreatePen;
var
  Data: PEMRCreatePen;
begin
  Data := CV;
  if Data^.ihPen >= HandlesCount then Exit;
  if HandleTable[Data^.ihPen] <> $FFFFFFFF then DeleteObject(HandleTable[Data^.ihPen]);
  HandleTable[Data^.ihPen] := CreatePen(Data^.lopn.lopnStyle, Data^.lopn.lopnWidth.x, Data^.lopn.lopnColor);
end;

procedure TEMWParser.DoDeleteObject;
var
  Data: PEMRDeleteObject;
begin
  Data := CV;
  if Data^.ihObject >= HandlesCount then Exit;
  DeleteObject(HandleTable[data^.ihObject]);
  HandleTable[data^.ihObject] := $FFFFFFFF;
end;

procedure TEMWParser.DoEllipse;
var
  Data: PEMREllipse;
begin
  Data := CV;
  FPage.Ellipse(GX(data^.rclBox.Left), GY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom));
  if not InPath then
    if (CPen.lopnWidth <> 0) and (CPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke else FPage.Stroke else
      if not IsNullBrush then FPage.Fill else FPage.NewPath;
end;

procedure TEMWParser.DoEndPath;
begin
  InPath := False;
end;

procedure TEMWParser.DoExcludeClipRect;
begin
  if Clipping then
  begin
    Clipping := False;
    FPage.GStateRestore;
    FCha := True;
  end;
end;

procedure TEMWParser.DoExtCreatePen;
var
  Data: PEMRExtCreatePen;
begin
  Data := CV;
  if Data^.ihPen >= HandlesCount then Exit;
  if HandleTable[Data^.ihPen] <> $FFFFFFFF then DeleteObject(HandleTable[Data^.ihPen]);
  HandleTable[Data^.ihPen] := CreatePen(Data^.elp.elpPenStyle, Data^.elp.elpWidth, Data^.elp.elpColor);
end;

procedure TEMWParser.DoExtTextOut;
var
  Data: PEMRExtTextOut;
  S: string;
  o: Pointer;
  CodePage: Integer;
  I: Integer;
  sd: PByteArray;
  RSZ, A, DDD: Extended;
  NRZ: Extended;
  PIN: PINT;
  RS: Integer;
  X, Y: Extended;
  DX, DY: Extended;
  AN: Extended;
  L: Extended;
  HC: Boolean;
  function GetStr: string;
  var
    S: string;
    I: Integer;
  begin
    if Data^.emr.iType = EMR_EXTTEXTOUTW then
    begin
      case CFont.lfCharSet of
        EASTEUROPE_CHARSET: CodePage := 1250;
        RUSSIAN_CHARSET: CodePage := 1251;
        GREEK_CHARSET: CodePage := 1253;
        TURKISH_CHARSET: CodePage := 1254;
        BALTIC_CHARSET: CodePage := 1257;
        SHIFTJIS_CHARSET: CodePage := 932;
        129: CodePage := 949;
        CHINESEBIG5_CHARSET: CodePage := 950;
        GB2312_CHARSET: CodePage := 936;
        Symbol_charset: CodePage := -1;
      else
        CodePage := 1252;
      end;
      if (CodePage <> -1) and (not WNG) then
      begin
        o := IP(CV, Data^.emrtext.offString);
        S := '';
        rs := WideCharToMultiByte(CodePage, 0, o, Data^.emrtext.nChars, nil, 0, nil, nil);
        if rs <> 0 then
        begin
          SetLength(S, RS);
          WideCharToMultiByte(CodePage, 0, o, Data^.emrtext.nChars, @s[1], RS, nil, nil)
        end;
      end
      else
      begin
        SetLength(S, Data^.emrtext.nChars);
        SD := IP(CV, Data^.emrtext.offString);
        for i := 1 to Data^.emrtext.nChars do s[i] := chr(sd[((i - 1) shl 1)]);
      end;
    end else
    begin
      SetLength(S, Data^.emrtext.nChars);
      o := IP(CV, Data^.emrtext.offString);
      Move(o^, S[1], Data^.emrtext.nChars);
    end;
    Result := S;
  end;
begin
  SetCurFont;
  Data := CV;
{$IFDEF CANVASDBG}
  TXTStr := #13#10 + Format('Angle = %d Bounds = (%d %d %d %d) Opaque = %d Clipped= %d',
    [CFont.lfEscapement div 10, Data^.rclBounds.Left, Data^.rclBounds.Top, Data^.rclBounds.Right, Data^.rclBounds.Bottom, Data^.emrtext.fOptions and ETO_CLIPPED, Data^.emrtext.fOptions and ETO_OPAQUE]);
  TXTStr := TXTStr + #13#10 + Format('XScale = %f YScale = %f Reference = (%d %d)', [Data^.exScale, Data^.eyScale, Data^.emrText.ptlReference.x, Data^.emrText.ptlReference.y]);
  TXTStr := TXTStr + #13#10 + Format('RTL = (%d %d %d %d)', [Data^.emrText.rcl.Left, Data^.emrText.rcl.Top, Data^.emrText.rcl.Right, Data^.emrText.rcl.bottom]);
{$ENDIF}
  if Data^.emrtext.nChars = 0 then Exit;
  if not InPath then
  begin
    if (Data^.emrtext.fOptions and ETO_CLIPPED <> 0) or (Data^.emrtext.fOptions and ETO_OPAQUE <> 0) then
    begin
      FPage.EndText;
      if (Data^.emrtext.fOptions and ETO_Clipped <> 0) then
      begin
        FPage.GStateSave;
        FPage.NewPath;
        FPage.Rectangle(GX(Data^.emrtext.rcl.Left), GY(Data^.emrtext.rcl.Top),
          GX(Data^.emrtext.rcl.Right), GY(Data^.emrtext.rcl.Bottom));
        FPage.Clip;
        FPage.NewPath;
      end;
      if (Data^.emrtext.fOptions and ETO_OPAQUE <> 0) then
      begin
        SetBGColor;
        FPage.NewPath;
        FPage.Rectangle(GX(Data^.emrtext.rcl.Left), GY(Data^.emrtext.rcl.Top),
          GX(Data^.emrtext.rcl.Right), GY(Data^.emrtext.rcl.Bottom));
        FPage.Fill;
      end;
      FPage.BeginText;
    end;
    SetFontColor;
  end;
  S := GetStr;
  if not (CFont.lfCharSet in [SHIFTJIS_CHARSET, 129, CHINESEBIG5_CHARSET, GB2312_CHARSET]) then
  begin
    if (FPage.Owner.EMFSimpleText) and (CFont.lfEscapement = 0) and (CFont.lfStrikeOut = 0) and (CFont.lfUnderline = 0) then
    begin
      HC := True;
      FPage.SetCharacterSpacing(0);
      PIN := IP(CV, Data^.emrtext.offDx);
      RSZ := 0;
      for i := 1 to Data^.emrtext.nChars do
      begin
        RSZ := RSZ + pin^;
        Inc(PIN);
      end;
      RSZ := RSZ * FX;
      A := RSZ;
    end else
    begin
      HC := False;
      if S[Length(s)] = ' ' then
      begin
        PIN := IP(CV, Data^.emrtext.offDx);
        RSZ := 0;
        for i := 1 to Data^.emrtext.nChars - 1 do
        begin
          RSZ := RSZ + pin^;
          Inc(PIN);
        end;
        DDD := pin^;
        RSZ := RSZ * FX;
        DDD := DDD * FX;
        A := RSZ + DDD;
        RSZ := XScale * Cal * RSZ - 1;
        NRZ := GetTextWidth(Copy(S, 1, Length(S) - 1));
        if Data^.emrtext.nChars <> 1 then FPage.SetCharacterSpacing((RSZ - NRZ) / (Data^.emrtext.nChars))
        else FPage.SetCharacterSpacing(0)
      end else
      begin
        PIN := IP(CV, Data^.emrtext.offDx);
        RSZ := 0;
        for i := 1 to Data^.emrtext.nChars do
        begin
          RSZ := RSZ + pin^;
          Inc(PIN);
        end;
        RSZ := RSZ * FX;
        A := RSZ;
        RSZ := XScale * Cal * RSZ - 1;
        NRZ := GetTextWidth(S);
        FPage.SetCharacterSpacing((RSZ - NRZ) / Data^.emrtext.nChars)
      end;
    end;
    if UpdatePos then
    begin
      X := CurVal.X;
      Y := CurVal.Y;
      if CFont.lfEscapement <> 0 then
      begin
        CurVal.X := CurVal.X + round(a * cos(CFont.lfEscapement * Pi / 1800));
        CurVal.Y := CurVal.Y - round(a * sin(CFont.lfEscapement * Pi / 1800));
      end else CurVal.X := CurVal.X + round(A);
    end else
    begin
      X := Data^.emrtext.ptlReference.X;
      Y := Data^.emrtext.ptlReference.Y;
    end;
    if CFont.lfEscapement = 0 then
    begin
      case VertMode of
        vjCenter: Y := GY(Y - MetaCanvas.TextHeight('W'));
        vjDown: Y := GY(Y - MetaCanvas.TextHeight('Wg'));
      else
        Y := GY(Y);
      end;
      case HorMode of
        hjRight: x := GX(X - A);
        hjCenter: x := GX(X - A / 2);
      else
        X := GX(X);
      end;
    end else
    begin
      if (VertMode = vjUp) and (HorMode = hjLeft) then
      begin
        Y := GY(Y);
        X := GX(X);
      end else
      begin
        case VertMode of
          vjCenter: DY := MetaCanvas.TextHeight('W');
          vjDown: DY := MetaCanvas.TextHeight('Wg');
        else
          DY := 0;
        end;
        case HorMode of
          hjRight: DX := A;
          hjCenter: DX := A / 2;
        else
          DX := 0;
        end;
        AN := CFont.lfEscapement * Pi / 1800;
        if DY = 0 then
        begin
          X := X - DX * cos(AN);
          Y := Y + DX * sin(AN);
        end else
        begin
          L := sqrt(sqr(DX) + sqr(DY));
          X := X - L * cos(AN - ArcSin(DY / L));
          Y := Y + L * sin(AN - ArcSin(DY / L));
        end;
        Y := GY(Y);
        X := GX(X);
      end;
    end;
  end else
  begin
    HC := False;
    FPage.SetCharacterSpacing(-0.6);
    Y := GY(Data^.emrtext.ptlReference.y);
    x := GX(Data^.emrtext.ptlReference.x);
  end;
{$IFDEF CANVASDBG}
  TXTStr := TXTStr + #13#10 + Format('X = %f Y = %f ', [X, Y]);
{$ENDIF}
  if not HC then
{$IFDEF CB}
    FPage.TextOutput(X, Y, CFont.lfEscapement / 10, s)
{$ELSE}
    FPage.TextOut(X, Y, CFont.lfEscapement / 10, s)
{$ENDIF}
  else
  begin
    PIN := IP(CV, Data^.emrtext.offDx);
    RS := Length(S);
    FPage.SetTextPosition(X, Y);
    X := 72 * FX * XScale * Cal / FPage.Owner.Resolution;
//    X := 72 * FX * Cal / FPage.Owner.Resolution;
    for I := 1 to RS do
    begin
      FPage.TextShow(S[i]);
      if I <> RS then
      begin
        FPage.AppendAction(FormatFloat(X * Pin^) + ' 0 Td');
        Inc(Pin);
      end;
    end;
  end;


  if not InPath then
  begin
    if (Data^.emrtext.fOptions and ETO_Clipped <> 0) then
    begin
      FPage.EndText;
      FPage.GStateRestore;
      FPage.BeginText;
    end;
  end;
end;

procedure TEMWParser.DoFillPath;
begin
  InPath := False;
  if not IsNullBrush then FPage.Fill;
  FPage.NewPath;
end;

procedure TEMWParser.DoInterSectClipRect;
var
  Data: PEMRIntersectClipRect;
begin
  Data := CV;
  if Clipping then
  begin
    FPage.GStateRestore;
    FCha := True;
  end;
  FPage.GStateSave;
  Clipping := True;
  FPage.NewPath;
  FPage.Rectangle(GX(Data^.rclClip.Left), GY(Data^.rclClip.Top), GX(Data^.rclClip.Right), GY(Data^.rclClip.Bottom));
  isCR := True;
  ClipRect.Left := GX(Data^.rclClip.Left);
  ClipRect.Top := GY(Data^.rclClip.Top);
  ClipRect.Right := GX(Data^.rclClip.Right);
  ClipRect.Bottom := GY(Data^.rclClip.Bottom);
  FPage.Clip;
  FPage.NewPath;
end;

procedure TEMWParser.DoLineTo;
var
  Data: PEMRLineTo;
begin
  Data := CV;
  if not InPath then FPage.MoveTo(GX(CurVal.x), GY(CurVal.y));
  FPage.LineTo(GX(Data^.ptl.x), GY(Data^.ptl.y));
  CurVal := Data^.ptl;
  if not InPath then PStroke;
end;

procedure TEMWParser.DoMoveToEx;
var
  PMove: PEMRLineTo;
begin
  PMove := CV;
  CurVal.x := pmove^.ptl.x;
  CurVal.y := pmove^.ptl.y;
  if InPath then
  begin
    if InText then InText := False;
    FPage.MoveTo(GX(CurVal.x), GY(CurVal.y));
  end;
end;

procedure TEMWParser.DoPie;
var
  Data: PEMRPie;
begin
  Data := CV;
  FPage.Pie(GX(Data^.rclBox.Left), gY(Data^.rclBox.Top), GX(Data^.rclBox.Right), GY(Data^.rclBox.Bottom),
    GX(Data^.ptlStart.x), GY(Data^.ptlStart.y), GX(Data^.ptlEnd.x), GY(Data^.ptlEnd.y));
  if not InPath then PFillAndStroke;
end;

procedure TEMWParser.DoPolyBezier;
var
  i: Integer;
  PL: PEMRPolyline;
begin
  with FPage do
  begin
    PL := CV;
    if PL^.cptl >= 4 then
    begin
      MoveTo(GX(PL^.aptl[0].x), GY(PL^.aptl[0].y));
      for i := 1 to (PL^.cptl - 1) div 3 do
        Curveto(GX(PL^.aptl[1 + (i - 1) * 3].x), GY(PL^.aptl[1 + (i - 1) * 3].y),
          GX(PL^.aptl[1 + (i - 1) * 3 + 1].x), GY(PL^.aptl[1 + (i - 1) * 3 + 1].y),
          GX(PL^.aptl[1 + (i - 1) * 3 + 2].x), GY(PL^.aptl[1 + (i - 1) * 3 + 2].y));
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyBezier16;
var
  i: Integer;
  PL16: PEMRPolyline16;
begin
  with FPage do
  begin
    PL16 := CV;
    if PL16^.cpts >= 4 then
    begin
      MoveTo(GX(PL16^.apts[0].x), GY(PL16^.apts[0].y));
      for i := 1 to (PL16^.cpts - 1) div 3 do
        Curveto(GX(PL16^.apts[1 + (i - 1) * 3].x), GY(PL16^.apts[1 + (i - 1) * 3].y),
          GX(PL16^.apts[1 + (i - 1) * 3 + 1].x), GY(PL16^.apts[1 + (i - 1) * 3 + 1].y),
          GX(PL16^.apts[1 + (i - 1) * 3 + 2].x), GY(PL16^.apts[1 + (i - 1) * 3 + 2].y));
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyBezierTo;
var
  i: Integer;
  PL: PEMRPolyline;
begin
  with FPage do
  begin
    PL := CV;
    if PL^.cptl >= 3 then
    begin
      if not InPath then MoveTo(GX(CurVal.x), GY(CurVal.y));
      for i := 1 to (PL^.cptl) div 3 do
      begin
        Curveto(GX(PL^.aptl[(i - 1) * 3].x), GY(PL^.aptl[(i - 1) * 3].y),
          GX(PL^.aptl[(i - 1) * 3 + 1].x), GY(PL^.aptl[(i - 1) * 3 + 1].y),
          GX(PL^.aptl[(i - 1) * 3 + 2].x), GY(PL^.aptl[(i - 1) * 3 + 2].y));
        CurVal := Point(PL^.aptl[(i - 1) * 3 + 2].x, PL^.aptl[(i - 1) * 3 + 2].y);
      end;
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyBezierTo16;
var
  i: Integer;
  PL16: PEMRPolyline16;
begin
  with FPage do
  begin
    PL16 := CV;
    if PL16^.cpts >= 3 then
    begin
      if not InPath then MoveTo(GX(CurVal.x), GY(CurVal.y));
      for i := 1 to PL16^.cpts div 3 do
      begin
        Curveto(GX(PL16^.apts[(i - 1) * 3].x), GY(PL16^.apts[(i - 1) * 3].y),
          GX(PL16^.apts[(i - 1) * 3 + 1].x), GY(PL16^.apts[(i - 1) * 3 + 1].y),
          GX(PL16^.apts[(i - 1) * 3 + 2].x), GY(PL16^.apts[(i - 1) * 3 + 2].y));
        CurVal := Point(PL16^.apts[(i - 1) * 3 + 2].x, PL16^.apts[(i - 1) * 3 + 2].y);
      end;
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyDraw;
var
  I: Integer;
  K: Cardinal;
  Types: PByteArray;
  Data: PEMRPolyDraw;
  TV: TPoint;
begin
  with FPage do
  begin
    Data := CV;
    if not InPath then NewPath;
    MoveTo(GX(CurVal.x), GY(CurVal.y));
    TV := CurVal;
    K := SizeOf(TEMRPolyDraw) - 1 + SizeOf(TPoint) * (Data^.cptl - 1);
    Types := IP(CV, K);
    K := 0;
    I := 0;
    while K < Data^.cptl do
    begin
      if Types[I] = PT_MOVETO then
      begin
        TV.x := Data^.aPTL[K].x;
        TV.y := Data^.aPTL[K].y;
        MoveTo(GX(TV.x), GY(TV.y));
        Inc(K);
        CurVal := TV;
      end else
        if (Types[I] and PT_LINETO) <> 0 then
        begin
          LineTo(GX(Data^.aPTL[K].x), GY(Data^.aPTL[K].y));
          Inc(K);
          CurVal := Point(Data^.aPTL[K].x, Data^.aPTL[K].y);
          if (Types[I] and PT_ClOSEFIGURE) <> 0 then
          begin
            LineTo(GX(TV.x), GY(TV.y));
            CurVal := TV;
          end;
        end else
          if (Types[I] and PT_BEZIERTO) <> 0 then
          begin
            Curveto(GX(Data^.aPTL[K].x), GY(Data^.aPTL[K].y),
              GX(Data^.aPTL[K + 1].x), GY(Data^.aPTL[K + 1].y),
              GX(Data^.aPTL[K + 2].x), GY(Data^.aPTL[K + 2].y));
            CurVal := Point(Data^.aPTL[K + 2].x, Data^.aPTL[K + 2].y);
            Inc(K, 3);
            if (Types[I] and PT_ClOSEFIGURE) <> 0 then
            begin
              LineTo(GX(TV.x), GY(TV.y));
              CurVal := TV;
            end;
          end
    end;
    if not InPath then PStroke;
  end;
end;

procedure TEMWParser.DoPolyDraw16;
var
  I: Integer;
  K: Cardinal;
  Types: PByteArray;
  Data: PEMRPolyDraw16;
  TV: TPoint;
begin
  with FPage do
  begin
    Data := CV;
    if not InPath then NewPath;
    MoveTo(GX(CurVal.x), GY(CurVal.y));
    TV := CurVal;
    K := SizeOf(TEMRPolyDraw16) - 1 + SizeOf(TSmallPoint) * (Data^.cpts - 1);
    Types := IP(CV, K);
    K := 0;
    I := 0;
    while K < Data^.cpts do
    begin
      if Types[I] = PT_MOVETO then
      begin
        TV.x := Data^.aPTs[K].x;
        TV.y := Data^.aPTs[K].y;
        MoveTo(GX(TV.x), GY(TV.y));
        Inc(K);
        CurVal := TV;
      end else
        if (Types[I] and PT_LINETO) <> 0 then
        begin
          LineTo(GX(Data^.aPTs[K].x), GY(Data^.aPTs[K].y));
          Inc(K);
          CurVal := Point(Data^.aPTS[K].x, Data^.aPTs[K].y);
          if (Types[I] and PT_ClOSEFIGURE) <> 0 then
          begin
            LineTo(GX(TV.x), GY(TV.y));
            CurVal := TV;
          end;
        end else
          if (Types[I] and PT_BEZIERTO) <> 0 then
          begin
            Curveto(GX(Data^.aPTs[K].x), GY(Data^.aPTs[K].y),
              GX(Data^.aPTs[K + 1].x), GY(Data^.aPTs[K + 1].y),
              GX(Data^.aPTs[K + 2].x), GY(Data^.aPTs[K + 2].y));
            CurVal := Point(Data^.aPTs[K + 2].x, Data^.aPTs[K + 2].y);
            Inc(K, 3);
            if (Types[I] and PT_ClOSEFIGURE) <> 0 then
            begin
              LineTo(GX(TV.x), GY(TV.y));
              CurVal := TV;
            end;
          end
    end;
    if not InPath then PStroke;
  end;
end;

procedure TEMWParser.DoPolygon;
var
  i: Integer;
  PL: PEMRPolyline;
begin
  with FPage do
  begin
    PL := CV;
    if PL^.cptl > 0 then
    begin
      NewPath;
      MoveTo(GX(pl^.aptl[0].x), GY(PL^.aptl[0].y));
      for I := 1 to pl^.cptl - 1 do LineTo(GX(pl^.aptl[i].x), GY(pl^.aptl[i].y));
      if not InPath then
      begin
        ClosePath;
        PFillAndStroke;
      end;
    end;
  end;
end;

procedure TEMWParser.DoPolygon16;
var
  i: Integer;
  PL16: PEMRPolyline16;
begin
  with FPage do
  begin
    PL16 := CV;
    if PL16^.cpts > 0 then
    begin
      NewPath;
      MoveTo(GX(pl16^.apts[0].x), GY(PL16^.apts[0].y));
      for I := 1 to pl16^.cpts - 1 do
        LineTo(GX(pl16^.apts[i].x), GY(pl16^.apts[i].y));
      if not InPath then
      begin
        ClosePath;
        PFillAndStroke;
      end;
    end;
  end;
end;

procedure TEMWParser.DoPolyLine;
var
  i: Integer;
  PL: PEMRPolyline;
begin
  with FPage do
  begin
    PL := CV;
    if PL^.cptl > 0 then
    begin
      NewPath;
      MoveTo(GX(pl^.aptl[0].x), GY(PL^.aptl[0].y));
      for I := 1 to pl^.cptl - 1 do LineTo(GX(pl^.aptl[i].x), GY(pl^.aptl[i].y));
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyLine16;
var
  i: Integer;
  PL16: PEMRPolyline16;
begin
  with FPage do
  begin
    PL16 := CV;
    if PL16^.cpts > 0 then
    begin
      NewPath;
      MoveTo(GX(pl16^.apts[0].x), GY(PL16^.apts[0].y));
      for I := 1 to pl16^.cpts - 1 do LineTo(GX(pl16^.apts[i].x), GY(pl16^.apts[i].y));
      if not InPath then PStroke;
    end;
  end;
end;

procedure TEMWParser.DoPolyLineTo;
var
  i: Integer;
  PL: PEMRPolyline;
begin
  with FPage do
  begin
    PL := CV;
    if PL^.cptl > 0 then
    begin
      NewPath;
      MoveTo(GX(CurVal.x), GY(CurVal.y));
      for I := 0 to pl^.cptl - 1 do LineTo(GX(pl^.aptl[i].x), GY(pl^.aptl[i].y));
      if not InPath then PStroke;
      CurVal := Point(pl^.aptl[pl^.cptl - 1].x, pl^.aptl[pl^.cptl - 1].y);
    end;
  end;
end;

procedure TEMWParser.DoPolyLineTo16;
var
  i: Integer;
  PL16: PEMRPolyline16;
begin
  with FPage do
  begin
    PL16 := CV;
    if PL16^.cpts > 0 then
    begin
      NewPath;
      MoveTo(GX(CurVal.x), GY(CurVal.y));
      for I := 0 to pl16^.cpts - 1 do LineTo(GX(pl16^.apts[i].x), GY(pl16^.apts[i].y));
      if not InPath then PStroke;
      CurVal := Point(pl16^.apts[pl16^.cpts - 1].x, pl16^.apts[pl16^.cpts - 1].y);
    end;
  end;
end;

procedure TEMWParser.DoPolyPolyGon;
var
  I, J, K, L: Integer;
  PPPAL: PPointArray;
  PPL: PEMRPolyPolyline;
begin
  with FPage do
  begin
    PPL := CV;
    NewPath;
    K := SizeOf(TEMRPolyPolyline) - SizeOf(TPoint) + SizeOf(dword) * (ppl^.nPolys - 1);
    PPPAL := IP(CV, K);
    K := 0;
    for j := 0 to ppl^.nPolys - 1 do
    begin
      MoveTo(GX(PPPAL[K].x), GY(PPPAL[K].y));
      L := K;
      Inc(K);
      for I := 1 to ppl^.aPolyCounts[J] - 1 do
      begin
        LineTo(GX(PPPAL[K].x), GY(PPPAL[K].y));
        Inc(K);
      end;
      LineTo(GX(PPPAL[L].x), GY(PPPAL[L].y));
    end;
    PFillAndStroke;
  end;
end;

procedure TEMWParser.DoPolyPolygon16;
var
  I, J, K, L: Integer;
  PPPA: PSmallPointArray;
  PPL16: PEMRPolyPolyline16;
begin
  with FPage do
  begin
    PPL16 := CV;
    NewPath;
    K := SizeOf(TEMRPolyPolyline16) - SizeOf(TSmallPoint) + SizeOf(dword) * (ppl16^.nPolys - 1);
    PPPA := IP(CV, K);
    K := 0;
    for j := 0 to ppl16^.nPolys - 1 do
    begin
      MoveTo(GX(PPPA[K].x), GY(PPPA[K].y));
      L := K;
      Inc(K);
      for I := 1 to ppl16^.aPolyCounts[J] - 1 do
      begin
        LineTo(GX(PPPA[K].x), GY(PPPA[K].y));
        Inc(K);
      end;
      LineTo(GX(PPPA[L].x), GY(PPPA[L].y));
    end;
    PFillAndStroke;
  end;
end;

procedure TEMWParser.DoPolyPolyLine;
var
  I, J, K: Integer;
  PPPAL: PPointArray;
  PPL: PEMRPolyPolyline;
begin
  with FPage do
  begin
    PPL := CV;
    NewPath;
    K := SizeOf(TEMRPolyPolyline) - SizeOf(TPoint) + SizeOf(dword) * (ppl^.nPolys - 1);
    PPPAL := IP(CV, K);
    K := 0;
    for j := 0 to ppl^.nPolys - 1 do
    begin
      MoveTo(GX(PPPAL[K].x), GY(PPPAL[K].y));
      Inc(K);
      for I := 1 to ppl^.aPolyCounts[J] - 1 do
      begin
        LineTo(GX(PPPAL[K].x), GY(PPPAL[K].y));
        Inc(K);
      end;
    end;
    if not InPath then PStroke;
  end;
end;

procedure TEMWParser.DoPolyPolyLine16;
var
  I, J, K: Integer;
  PPPA: PSmallPointArray;
  PPL16: PEMRPolyPolyline16;
begin
  with FPage do
  begin
    PPL16 := CV;
    NewPath;
    K := SizeOf(TEMRPolyPolyline16) - SizeOf(TSmallPoint) + SizeOf(dword) * (ppl16^.nPolys - 1);
    PPPA := IP(CV, K);
    K := 0;
    for j := 0 to ppl16^.nPolys - 1 do
    begin
      MoveTo(GX(PPPA[K].x), GY(PPPA[K].y));
      Inc(K);
      for I := 1 to ppl16^.aPolyCounts[J] - 1 do
      begin
        LineTo(GX(PPPA[K].x), GY(PPPA[K].y));
        Inc(K);
      end;
    end;
    if not InPath then PStroke;
  end;
end;

procedure TEMWParser.DoRectangle;
var
  Data: PEMREllipse;
begin
  Data := CV;
  if (data^.rclBox.Left = data^.rclBox.Right) or (data^.rclBox.Top = data^.rclBox.Bottom) then
  begin
    FPage.NewPath;
    Exit;
  end;
  FPage.Rectangle(GX(data^.rclBox.Left), GY(data^.rclBox.Top), GX(Data^.rclBox.Right - 1), GY(Data^.rclBox.Bottom - 1));
  if not InPath then
    if (CPen.lopnWidth <> 0) and (CPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke else FPage.Stroke else
      if not IsNullBrush then FPage.Fill else FPage.NewPath;
end;


procedure TEMWParser.DoRestoreDC;
var
  Data: PEMRRestoreDC;
  TR: TXForm;
  H: HGDIOBJ;
  NPen: TLogPen;
  NBrush: TLogBrush;
  NFont: TLogFont;
  I: DWORD;
  S: TSize;
  P: TPoint;
begin
  Data := CV;
  RestoreDC(DC, Data^.iRelative);
// CheckTransform
  if NT then
  begin
    GetWorldTransform(DC, TR);
    XOff := TR.eDx * Cal;
    YOff := TR.eDy * Cal;
    XScale := TR.eM11;
    YScale := TR.eM22;
  end else
  begin
    if Data^.iRelative < 0 then
      if StackSize + Data^.iRelative >= 0 then
      begin
        XF := Transfstack[StackSize + Data^.iRelative];
        XScale := XF.eM11;
        YScale := XF.eM22;
        XOff := XF.eDx * Cal;
        YOff := XF.eDy * Cal;
        StackSize := StackSize + Data^.iRelative;
        SetLength(TransfStack, StackSize);
      end
      else
        TransfStack := nil
    else
      if StackSize > Data^.iRelative then
      begin
        XF := Transfstack[Data^.iRelative - 1];
        XScale := XF.eM11;
        YScale := XF.eM22;
        XOff := XF.eDx * Cal;
        YOff := XF.eDy * Cal;
        StackSize := Data^.iRelative - 1;
        SetLength(TransfStack, StackSize);
      end;
  end;
  MapMode := GetMapMode(DC);
  BGMode := not (GetBkMode(DC) = TRANSPARENT);
  TextColor := GetTextColor(DC);
  if InText then SetFontColor;
  BGColor := GetBkColor(DC);
  PolyFIllMode := (GetPolyFillMode(DC) = ALTERNATE);
  GetViewportExtEx(DC, S);
  VEX := S.cx;
  VEY := S.cy;
  GetWindowExtEx(DC, S);
  WEX := S.cx;
  WEY := S.cy;
  GetViewportOrgEx(DC, P);
  VOX := P.x;
  VOY := P.y;
  GetWindowOrgEx(dc, P);
  WOX := P.x;
  WOY := P.y;
  if Clipping then
  begin
    Clipping := False;
    FCha := True;
    if InText then
    begin
      FPage.EndText;
      FPage.GStateRestore;
      FPage.BeginText;
    end
    else FPage.GStateRestore;
  end;

// Check Pen
  H := GetCurrentObject(DC, OBJ_PEN);
  GetObject(H, SizeOf(NPen), @NPen);
  if NPen.lopnColor <> CPen.lopnColor then
  begin
    CPen.lopnColor := NPen.lopnColor;
    SetPenColor;
  end;
  if NPen.lopnStyle <> CPen.lopnStyle then
  begin
    CPen.lopnStyle := NPen.lopnStyle;
    case CPen.lopnStyle of
      ps_Solid, ps_InsideFrame: FPage.NoDash;
      ps_Dash: FPage.SetDash('[8 8] 0');
      ps_Dot: FPage.SetDash('[2 2] 0');
      ps_DashDot: FPage.SetDash('[8 2 2 2] 0');
      ps_DashDotDot: FPage.SetDash('[8 2 2 2 2 2] 0');
    end;
  end;
  if NPen.lopnWidth.x * XScale * FX <> CPen.lopnWidth then
  begin
    if NPen.lopnWidth.x = 0 then CPen.lopnWidth := FX * xscale else
      CPen.lopnWidth := NPen.lopnWidth.x * XScale * FX;
    FPage.SetLineWidth(Cal * CPen.lopnWidth);
  end;

//Chech Brush
  H := GetCurrentObject(DC, OBJ_BRUSH);
  GetObject(H, SizeOf(NBrush), @NBrush);
  if NBrush.lbColor <> CBrush.lbColor then
  begin
    CBrush.lbColor := NBrush.lbColor;
    if not InText then SetBrushColor;
  end;
  if NBrush.lbStyle = 1 then IsNullBrush := True;

//Check Font
  H := GetCurrentObject(DC, OBJ_FONT);
  GetObject(H, SizeOf(NFont), @NFont);
  if (CFont.lfFaceName <> NFont.lfFaceName) or (CFont.lfWeight <> NFont.lfWeight) or
    (CFont.lfItalic <> NFont.lfItalic) or (CFont.lfUnderline <> NFont.lfUnderline) or
    (CFont.lfStrikeOut <> NFont.lfStrikeOut) or (CFont.lfCharSet <> NFont.lfCharSet) or
    (CFont.lfHeight <> NFont.lfHeight) then
  begin
    Move(NFont, CFont, SizeOf(CFont));
    Fcha := True;
//    if inText then SetCurFont;
  end;
  I := GetTextAlign(DC);
  case I and (TA_LEFT or ta_Right or ta_center) of
    ta_left: HorMode := hjLeft;
    ta_right: HorMode := hjRight;
    ta_center: HorMode := hjCenter;
  end;
//Vertical Detect
  case I and (TA_Top or ta_BaseLine or ta_Bottom) of
    TA_Top: VertMode := vjUp;
    ta_Bottom: VertMode := vjDown;
    TA_BASELINE: VertMode := vjCenter;
  end;
  UpdatePos := (I and TA_UPDATECP = TA_UPDATECP);
end;

procedure TEMWParser.DoRoundRect;
var
  Data: PEMRRoundRect;
begin
  Data := CV;
  FPage.RoundRect(Round(GX(Data^.rclBox.Left)), Round(GY(Data^.rclBox.Top)),
    Round(GX(Data^.rclBox.Right)), Round(GY(Data^.rclBox.Bottom)),
    Round(GX(Data^.szlCorner.cx)), Round(GY(Data^.szlCorner.cy)));
  if not InPath then
    if (CPen.lopnWidth <> 0) and (CPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke else FPage.Stroke else
      if not IsNullBrush then FPage.Fill else FPage.NewPath;
end;

procedure TEMWParser.DoSaveDC;
begin
  SaveDC(DC);
  if not NT then
  begin
    Inc(StackSize);
    if StackSize >= 1 then
    begin
      SetLength(TransfStack, StackSize);
      TransfStack[StackSize - 1] := XF;
    end;
  end;
end;

procedure TEMWParser.DoSelectClipPath;
begin
  if Clipping then
  begin
    FPage.GStateRestore;
    FCha := True;
  end;
  FPage.GStateSave;
  Clipping := True;
  FPage.Clip;
  isCR := False;
  FPage.NewPath;
  InPath := False;
end;

procedure TEMWParser.DoSelectObject;
var
  Data: PEMRSelectObject;
  I: DWORD;
  NPen: TLogPen;
  NBrush: TLogBrush;
  NFont: TLogFont;
begin
  Data := CV;
  if (Data^.ihObject and $80000000) = 0 then
  begin
    if Data^.ihObject >= HandlesCount then Exit;
    SelectObject(DC, HandleTable[Data^.ihObject]);
    I := GetObjectType(HandleTable[Data^.ihObject]);
    case I of
      OBJ_PEN:
        begin
          GetObject(HandleTable[Data^.ihObject], SizeOf(NPen), @NPen);
          if NPen.lopnColor <> CPen.lopnColor then
          begin
            CPen.lopnColor := NPen.lopnColor;
            SetPenColor;
          end;
          if NPen.lopnStyle <> CPen.lopnStyle then
          begin
            CPen.lopnStyle := NPen.lopnStyle;
            case CPen.lopnStyle of
              ps_Solid, ps_InsideFrame: FPage.NoDash;
              ps_Dash: FPage.SetDash('[8 8] 0');
              ps_Dot: FPage.SetDash('[2 2] 0');
              ps_DashDot: FPage.SetDash('[8 2 2 2] 0');
              ps_DashDotDot: FPage.SetDash('[8 2 2 2 2 2] 0');
            end;
          end;
          if NPen.lopnWidth.x * XScale * FX <> CPen.lopnWidth then
          begin
            if NPen.lopnWidth.x = 0 then CPen.lopnWidth := XScale * FX else
              CPen.lopnWidth := NPen.lopnWidth.x * XScale * FX;
            FPage.SetLineWidth(Cal * CPen.lopnWidth);
          end;
        end;
      OBJ_BRUSH:
        begin
          IsNullBrush := False;
          GetObject(HandleTable[Data^.ihObject], SizeOf(NBrush), @NBrush);
          if NBrush.lbColor <> CBrush.lbColor then
          begin
            CBrush.lbColor := NBrush.lbColor;
            if not InText then SetBrushColor;
          end;
          if NBrush.lbStyle = 1 then IsNullBrush := True;
        end;
      OBJ_FONT:
        begin
          GetObject(HandleTable[Data^.ihObject], SizeOf(NFont), @NFont);
          if (CFont.lfFaceName <> NFont.lfFaceName) or (CFont.lfWeight <> NFont.lfWeight) or
            (CFont.lfItalic <> NFont.lfItalic) or (CFont.lfUnderline <> NFont.lfUnderline) or
            (CFont.lfStrikeOut <> NFont.lfStrikeOut) or (CFont.lfCharSet <> NFont.lfCharSet) or
            (CFont.lfHeight <> NFont.lfHeight) then
          begin
            Move(NFont, CFont, SizeOf(CFont));
            Fcha := True;
//            if InText then SetCurFont;
          end;
        end;
    end;
  end
  else
  begin
    I := Data^.ihObject and $7FFFFFFF;
    SelectObject(DC, GetStockObject(I));
    case I of
      WHITE_BRUSH:
        begin
          IsNullBrush := False;
          CBrush.lbColor := clWhite;
          if not InText then SetBrushColor;
        end;
      LTGRAY_BRUSH:
        begin
          IsNullBrush := False;
          CBrush.lbColor := $AAAAAA;
          if not InText then SetBrushColor;
        end;
      GRAY_BRUSH:
        begin
          IsNullBrush := False;
          CBrush.lbColor := $808080;
          if not InText then SetBrushColor;
        end;
      DKGRAY_BRUSH:
        begin
          IsNullBrush := False;
          CBrush.lbColor := $666666;
          if not InText then SetBrushColor;
        end;
      BLACK_BRUSH:
        begin
          IsNullBrush := False;
          CBrush.lbColor := 0;
          if not InText then SetBrushColor;
        end;
      Null_BRUSH:
        begin
          CBrush.lbColor := clWhite;
          IsNullBrush := True;
          if not InText then SetBrushColor;
        end;
      WHITE_PEN:
        begin
          CPen.lopnColor := clWhite;
          FPage.SetRGBColorStroke(1, 1, 1);
        end;
      BLACK_PEN:
        begin
          CPen.lopnColor := clBlack;
          FPage.SetRGBColorStroke(0, 0, 0);
        end;
      Null_PEN:
        begin
          CPen.lopnStyle := PS_NULL;
        end;
      OEM_FIXED_FONT, ANSI_FIXED_FONT, ANSI_VAR_FONT, SYSTEM_FONT:
        begin
          CFont.lfFaceName := 'Arial';
          Fcha := True;
//          if InText then SetCurFont;
        end;
    end;
  end;
end;


procedure TEMWParser.DoSetArcDirection;
var
  Data: PEMRSetArcDirection;
begin
  Data := CV;
  CCW := Data^.iArcDirection = AD_COUNTERCLOCKWISE;
end;

procedure TEMWParser.DoSetBKColor;
var
  PColor: PEMRSetTextColor;
begin
  PColor := CV;
  BGColor := PColor^.crColor;
  SetBkColor(DC, PColor^.crColor);
end;

procedure TEMWParser.DoSetBKMode;
var
  PMode: PEMRSelectclippath;
begin
  PMode := CV;
  BGMode := not (PMode^.iMode = TRANSPARENT);
  SetBkMode(DC, pmode^.iMode);
end;

procedure TEMWParser.DoSetDibitsToDevice;
var
  Data: PEMRSetDIBitsToDevice;
  B: TBitmap;
  O: Pointer;
  P: PBitmapInfo;
  I: Integer;
begin
  Data := CV;
  P := IP(CV, Data^.offBmiSrc);
  O := IP(CV, Data^.offBitsSrc);
  B := TBitmap.Create;
  try
    B.Width := Data^.cxSrc;
    B.Height := Data^.cySrc;
    SetDIBitsToDevice(B.Canvas.Handle, 0, 0, B.Width, B.Height, Data^.xSrc, Data^.ySrc,
      Data^.iStartScan, Data^.cScans, O, p^, Data^.iUsageSrc);
    if B.PixelFormat = pf1bit then I := FPage.Owner.AddImage(B, itcCCITT4) else
      if not FPage.Owner.EMFImageAsJpeg then I := FPage.Owner.AddImage(B, itcFlate) else
        I := FPage.Owner.AddImage(B, itcJpeg);
    FPage.ShowImage(I, GX(Data^.rclBounds.Left, false), GY(Data^.rclBounds.Top, False),
      GX(Data^.cxSrc, False), GY(Data^.cySrc, False), 0);
  finally
    B.Free;
  end;
end;

procedure TEMWParser.DoSetMiterLimit;
//var
//  Data: PEMRSetMiterLimit;
begin
//  Data := CV;
//  FPage.SetMiterLimit(Data^.eMiterLimit);
end;

procedure TEMWParser.DoSetPixelV;
var
  Data: PEMRSetPixelV;
begin
  Data := CV;
  FPage.NewPath;
  if Data^.crColor <> CPen.lopnColor then
    FPage.SetRGBColorStroke(GetRValue(Data^.crColor) / 255,
      GetGValue(Data^.crColor) / 255, GetBValue(Data^.crColor) / 255);
  if CPen.lopnWidth <> 1 then
    FPage.SetLineWidth(1);
  FPage.MoveTo(GX(Data^.ptlPixel.x), GY(Data^.ptlPixel.y));
  FPage.LineTo(GX(Data^.ptlPixel.x) + 0.01, GY(Data^.ptlPixel.y) + 0.01);
  PStroke;
  if CPen.lopnWidth <> 1 then
    FPage.SetLineWidth(Cal * CPen.lopnWidth);
  if Data^.crColor <> CPen.lopnColor then
    FPage.SetRGBColorStroke(GetRValue(CPen.lopnColor) / 255,
      GetGValue(CPen.lopnColor) / 255, GetBValue(CPen.lopnColor) / 255);
end;

procedure TEMWParser.DoSetPolyFillMode;
var
  PMode: PEMRSelectclippath;
begin
  PMode := CV;
  PolyFIllMode := (PMode^.iMode = ALTERNATE);
  SetPolyFillMode(DC, pmode^.iMode);
end;

procedure TEMWParser.DoSetStretchBltMode;
var
  Data: PEMRSetStretchBltMode;
begin
  Data := CV;
  SBM := Data^.iMode;
  SetStretchBltMode(DC, data^.iMode);
end;

procedure TEMWParser.DoSetTextAlign;
var
  PMode: PEMRSelectclippath;
begin
  PMode := CV;
  SetTextAlign(DC, PMode^.iMode);
// Horisontal Detect
  case PMode^.iMode and (TA_LEFT or ta_Right or ta_center) of
    ta_left: HorMode := hjLeft;
    ta_right: HorMode := hjRight;
    ta_center: HorMode := hjCenter;
  end;
//Vertical Detect
  case PMode^.iMode and (TA_Top or ta_BaseLine or ta_Bottom) of
    TA_Top: VertMode := vjUp;
    ta_Bottom: VertMode := vjDown;
    TA_BASELINE: VertMode := vjCenter;
  end;
  UpdatePos := (PMode^.iMode and TA_UPDATECP = TA_UPDATECP);
end;

procedure TEMWParser.DoSetTextColor;
var
  PColor: PEMRSetTextColor;
begin
  PColor := CV;
  TextColor := PColor^.crColor;
  SetTextColor(DC, pcolor^.crColor);
  if InText then SetFontColor;
end;

procedure TEMWParser.DoSetTextJustification;
//var
//  Data: PEMRLineTo;
begin
//  Data := CV;
//  if (Data^.ptl.x = 0) or (Data^.ptl.y = 0) then
//    FPage.SetWordSpacing(0) else FPage.SetWordSpacing(G(Data^.ptl.x / Data^.ptl.y));
end;

procedure TEMWParser.DoSetViewPortExtEx;
var
  Data: PEMRSetViewportExtEx;
begin
  Data := CV;
  VEX := Data^.szlExtent.cx;
  VEY := Data^.szlExtent.cy;
  SetViewportExtEx(DC, Data^.szlExtent.cx, data^.szlExtent.cy, nil);
end;

procedure TEMWParser.DoSetViewPortOrgEx;
var
  Data: PEMRSetViewportOrgEx;
begin
  Data := CV;
  VOX := Data^.ptlOrigin.X;
  VOY := Data^.ptlOrigin.Y;
  SetViewportOrgEx(DC, data^.ptlOrigin.X, data^.ptlOrigin.Y, nil);
end;

procedure TEMWParser.DoSetWindowExtEx;
var
  Data: PEMRSetViewportExtEx;
begin
  Data := CV;
  WEX := data^.szlExtent.cx;
  WEY := Data^.szlExtent.cy;
  SetWindowExtEx(DC, data^.szlExtent.cx, Data^.szlExtent.cy, nil);
end;

procedure TEMWParser.DoSetWindowOrgEx;
var
  Data: PEMRSetViewportOrgEx;
begin
  Data := CV;
  WOX := Data^.ptlOrigin.X;
  WOY := Data^.ptlOrigin.Y;
  SetWindowOrgEx(DC, Data^.ptlOrigin.X, Data^.ptlOrigin.Y, nil);
end;

procedure TEMWParser.DoSetWorldTransform;
var
  PWorldTransf: PEMRSetWorldTransform;
begin
  PWorldTransf := CV;
  XScale := PWorldTransf^.xform.eM11;
  YScale := PWorldTransf^.xform.eM22;
  XOff := PWorldTransf^.xform.eDx * Cal;
  YOff := PWorldTransf^.xform.eDy * Cal;
  if NT then SetWorldTransform(DC, PWorldTransf^.xform)
  else Move(PWorldTransf^.xform, XF, SizeOf(XF));
end;


procedure MetafileCopyRect(MFile: TMetafile;
                srcRct,dstRct: TRect; Target: TCanvas);
var
 rgn: hrgn;
 xmag, ymag: double;
 magleft, Magtop: integer;
begin
if (MFile = nil) then exit;
  rgn:= CreateRectRgnIndirect(Rect(dstRct.Left,dstRct.Top, dstRct.Right + 1, DstRct.Bottom + 1));
  With Target do
    try
        SelectClipRgn(Target.Handle,rgn);

        xMag :=  ((dstRct.Right - dstRct.Left +1) * 1.0) / ((srcRct.Right - srcRct.Left +1) * 1.0);
        yMag :=  ((dstRct.Bottom - dstRct.Top +1) * 1.0) / ((srcRct.Bottom - srcRct.Top +1) * 1.0);

        magLeft := Round(- (srcRct.Left) * xMag);
        magTop := Round(- (srcRct.Top) * yMag);

        Target.StretchDraw(Rect(magLeft, MagTop, Round(Mfile.Width * xMag + MagLeft) , Round(Mfile.Height * yMag + magTop) ) ,MFile); // draw whole metafile, so we need to transform whole dest Rect
        SelectClipRgn(Target.handle,0);
    finally
      DeleteObject(rgn);
    end;
end;

procedure TEMWParser.DoStretchBlt;
var
  Data: PEMRStretchBlt;
  B: TBitmap;
  O: Pointer;
  P: PBitmapInfo;
  I: Integer;
  b32: TBitmap;
begin

  Data := CV;
  P := IP(CV, Data^.offBmiSrc);
  O := IP(CV, Data^.offBitsSrc);
  if (Data^.cySrc <> 0) and (Data^.cxSrc <> 0) then
  begin
    if FiImagePosList.IndexOf( IntToStr(Data^.rclBounds.Left)+'|'+IntToStr(Data^.rclBounds.Top)+'|'+IntToStr(Data^.rclBounds.Right)+'|'+IntToStr(Data^.rclBounds.Bottom)  ) < 0 then
    begin // altered drawing of images: if image of that pos-string-signature already exists, then all operations ignored
          // because existing image is always rendered by proven metafile, not just extracted from metafile content

          //***************************************
          // Generation of PDF images altered as SYS-436 ticket = 104210 Reso
          // Previously, all images were taken from incoming metafile, drawn on new bitmap and stored in PDF - problems with transparent images since always drawn on new surface, not affected one
          // Now, any image operation results in drawing of bitmap-region image of completely processed metafile of the page, and transfrer of resulting image into PDF. consequent drawing of that region ignored.
          //***************************************
      FiImagePosList.Add(     IntToStr(Data^.rclBounds.Left)+'|'+IntToStr(Data^.rclBounds.Top)+'|'+IntToStr(Data^.rclBounds.Right)+'|'+IntToStr(Data^.rclBounds.Bottom));

      B := TBitmap.Create;
      try
        b32 := TBitmap.Create;
        try

          B.Width := Data^.cxSrc;
          B.Height := Data^.cySrc;

          b32.Width := b.Width;
          b32.Height := b.Height;
          b32.Handletype := bmDIB;


          StretchDIBits(B.Canvas.Handle, 0, 0, B.Width, B.Height, Data^.xSrc, Data^.ySrc,
            B.Width, B.Height, O, p^, Data^.iUsageSrc, Data^.dwRop);  // B - get original image stored in metafile ( we do not  use it after SYS-436)

          MetafileCopyRect(FTransparentRenderMetafile // we already have image for completely drawn page so we draw what we have on screen as metafile on bitmap
                  ,Rect(Data^.rclBounds.Left,Data^.rclBounds.Top, Data^.rclBounds.Right, Data^.rclBounds.Bottom)
                  ,Rect(0, 0, B.Width-1, b.Height - 1)
                  ,b32.Canvas);

            if B.PixelFormat = pf1bit then    // pixelformat is pfDevice
              I := FPage.Owner.AddImage(B, itcCCITT4)
            else
              if not FPage.Owner.EMFImageAsJpeg then  // settings on creation prevents that
                 I := FPage.Owner.AddImage(B32, itcFlate)
              else
                I := FPage.Owner.AddImage(B, itcJpeg); // never happens on PDF generation

            FPage.ShowImage(I, GX(Data^.rclBounds.Left, False), GY(Data^.rclBounds.Top, False),
              GX(Data^.rclBounds.Right - Data^.rclBounds.Left, False), GY(Data^.rclBounds.Bottom - Data^.rclBounds.Top, False), 0);

        finally
          b32.Free;
        end;
      finally
        B.Free;
      end;
    end; // check of image existence in PDF - SYS-436
  end
  else
  begin
    if (data^.cxDest = 0) or (data^.cyDest = 0) then FPage.NewPath
    else
    begin
      FPage.Rectangle(gX(data^.xDest), gY(Data^.yDest), gX(Data^.xDest + Data^.cxDest), gY(Data^.yDest + Data^.cyDest));
      FPage.Fill;
    end;
  end;
end;

procedure TEMWParser.DoStretchDiBits;
var
  Data: PEMRStretchDiBits;
  B: TBitmap;
  O: Pointer;
  P: PBitmapInfo;
  I: Integer;
begin
  Data := CV;
  P := IP(CV, Data^.offBmiSrc);
  O := IP(CV, Data^.offBitsSrc);
  B := TBitmap.Create;
  try
    B.Width := Data^.cxSrc;
    B.Height := Data^.cySrc;
    StretchDIBits(B.Canvas.Handle, 0, 0, B.Width, B.Height, Data^.xSrc, Data^.ySrc,
      B.Width, B.Height, O, p^, Data^.iUsageSrc, Data^.dwRop);
    if B.PixelFormat = pf1bit then I := FPage.Owner.AddImage(B, itcCCITT4) else
      if not FPage.Owner.EMFImageAsJpeg then I := FPage.Owner.AddImage(B, itcFlate) else
        I := FPage.Owner.AddImage(B, itcJpeg);
    if (Data^.rclBounds.Right - Data^.rclBounds.Left > 0) and
      (Data^.rclBounds.Bottom - Data^.rclBounds.Top > 0) then
      FPage.ShowImage(I, gx(Data^.rclBounds.Left, False), gy(Data^.rclBounds.Top, False),
        GX(Data^.rclBounds.Right - Data^.rclBounds.Left, False), GY(Data^.rclBounds.Bottom - Data^.rclBounds.Top, False), 0) else
      FPage.ShowImage(I, GX(Data^.xDest), GY(Data^.yDest), GX(data^.cxDest), GY(Data^.cyDest), 0);
  finally
    B.Free;
  end;
end;

procedure TEMWParser.DoStrokeAndFillPath;
begin
  InPath := False;
  PFillAndStroke;
  InPath := False;
  FPage.NewPath;
end;

procedure TEMWParser.DoStrokePath;
begin
  InPath := False;
  PStroke;
  FPage.NewPath;
end;

{$IFDEF CANVASDBG}
var
  iii: Integer = 0;
{$ENDIF}


procedure TEMWParser.Execute;
var
  Header: PEnhMetaHeader;
  I: Integer;
begin
  if MS.Size = 0 then
    raise TPDFException.Create(SMetafileNotLoaded);
  CV := MS.Memory;
  Header := CV;
  Off := Header^.nSize;
  RE := Header^.rclBounds;
  HandlesCount := Header^.nHandles;
  SetLength(HandleTable, HandlesCount);
  for I := 0 to HandlesCount - 1 do HandleTable[i] := $FFFFFFFF;
  Meta.Clear;
  DC := MetaCanvas.Handle;
  SetGraphicsMode(DC, GM_ADVANCED);
  XF.eM11 := 1;
  XF.eM12 := 0;
  XF.eM21 := 0;
  XF.eM22 := 1;
  XF.eDx := 0;
  XF.eDy := 0;
  NT := SetWorldTransform(DC, XF);
  XScale := 1;
  YScale := 1;
  XOff := 0;
  YOff := 0;
  StackSize := 0;
  TransfStack := nil;
  InitExecute;
  while not FEX do
  begin
    Inc(CurRec);
    CV := IP(CV, Off);
    ExecuteRecord(CV);
{$IFDEF CANVASDBG}
    SaveToLog(CV);
{$ENDIF}
  end;
{$IFDEF CANVASDBG}
  Debug.SaveToFile('HDCDebug\' + IntToStr(iii) + '.txt');
  Inc(iii);
{$ENDIF}
  for I := 1 to HandlesCount - 1 do DeleteObject(HandleTable[I]);
  HandleTable := nil;
  TransfStack := nil;
end;

function TEMWParser.GX(Value: Extended; Map: Boolean = True): Extended;
begin
  if Map then Result := XOff + XScale * Mapx(Value) * Cal else Result := Value * Cal
end;

function TEMWParser.GY(Value: Extended; Map: Boolean = True): Extended;
begin
  if Map then Result := YOff + YScale * Mapy(Value) * Cal else Result := Value * Cal;
end;

function TEMWParser.GetMax: TSize;
var
  Header: PEnhMetaHeader;
begin
  if MS.Size = 0 then
    raise TPDFException.Create(SMetafileNotLoaded);
  CV := MS.Memory;
  Header := CV;
  Result.cx := Round(Cal * header^.rclBounds.Right) + 1;
  Result.cy := Round(Cal * header^.rclBounds.Bottom) + 1;
//  Header^.szlDevice.cx:=header^.rclBounds.Right;
//  Header^.szlDevice.cy:=header^.rclBounds.Bottom;
end;

function TEMWParser.GetTextWidth(Text: string): Extended;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to Length(Text) do
    Result := Result + CurWidth[Ord(Text[i])];
  Result := Result / 1000;
end;

procedure TEMWParser.LoadMetaFile(MF: TMetafile);
begin
  MS.Clear;
  MF.SaveToStream(MS);
  MS.Seek(0, soFromBeginning);
  FTransparentRenderMetafile.LoadFromStream(MS);
  MS.Seek(0, soFromBeginning);

  MFH := MF.Handle;
end;

procedure TEMWParser.SetCurFont;
var
  Rp: Boolean;
  St: TFontStyles;
  FS: Extended;
begin
  if not Fcha then Exit;
  Fcha := False;
  St := [];
  if CFont.lfWeight >= 600 then St := St + [fsBold];
  if CFont.lfItalic <> 0 then St := St + [fsItalic];
  if CFont.lfStrikeOut <> 0 then St := St + [fsStrikeOut];
  if CFont.lfUnderline <> 0 then St := St + [fsUnderline];
  Rp := False;
  if not IsTrueType(CFont.lfFaceName) then RP := True;
  if UpperCase(CFont.lfFaceName) = 'ZAPFDINGBATS' then Rp := False;
  if UpperCase(CFont.lfFaceName) = 'SYMBOL' then Rp := False;
  if UpperCase(CFont.lfFaceName) = 'WINGDINGS' then WNG := True else WNG := False;
  FS := -MulDiv(CFont.lfHeight, 72, MetaCanvas.Font.PixelsPerInch) * abs(YScale);
  if CFont.lfCharSet <> SYmbol_charset then
    if not Rp then FPage.SetActiveFont(CFont.lfFaceName, St, FS * FY, CFont.lfCharSet) else
    FPage.SetActiveFont('Arial', St, FS * FY, CFont.lfCharSet) else
    if not Rp then FPage.SetActiveFont(CFont.lfFaceName, St, FS * FY, 0) else
      FPage.SetActiveFont('Arial', St, FS * FY, 0);
  FPage.LoadCurrentFontWidth(CurWidth);
  FontInited := True;
end;

procedure TEMWParser.SetInPath(const Value: Boolean);
begin
  FInPath := Value;
  if FInPath then FPage.MoveTo(GX(CurVal.X), GY(CurVal.Y));
end;

procedure TEMWParser.PFillAndStroke;
begin
  if not IsNullBrush then
  begin
    if (CPen.lopnWidth <> 0) and (CPen.lopnStyle <> ps_null) then
      if PolyFIllMode then FPage.EoFillAndStroke else FPage.FillAndStroke
    else if PolyFIllMode then FPage.EoFill else FPage.Fill
  end else PStroke;
end;

procedure TEMWParser.PStroke;
begin
  if (CPen.lopnWidth <> 0) and (CPen.lopnStyle <> ps_null) then
    FPage.Stroke else FPage.NewPath;
end;

procedure TEMWParser.SetInText(const Value: Boolean);
begin
  if FInText = Value then Exit;
  FInText := Value;
  if Value = True then
  begin
    if Clipping then
      FPage.GStateRestore;
    FPage.BeginText;
    //SetCurFont;
    SetFontColor;
  end else
  begin
    FPage.EndText;
    SetBrushColor;
    if Clipping then
      if isCR then
      begin
        FPage.GStateSave;
        FPage.Rectangle(ClipRect.Left, ClipRect.Top, ClipRect.Right, ClipRect.Bottom);
        FPage.Clip;
        FPage.NewPath;
      end else Clipping := False;
  end;
end;

procedure TEMWParser.SetBrushColor;
begin
  if CurFill <> CBrush.lbColor then
  begin
    CurFill := CBrush.lbColor;
    FPage.SetRGBColorFill(GetRValue(CBrush.lbColor) / 255,
      GetGValue(CBrush.lbColor) / 255, GetBValue(CBrush.lbColor) / 255);
  end;
end;

procedure TEMWParser.SetFontColor;
begin
  if CurFill <> TextColor then
  begin
    CurFill := TextColor;
    FPage.SetRGBColorFill(GetRValue(TextColor) / 255, GetGValue(TextColor) / 255, GetBValue(TextColor) / 255);
  end;
end;

procedure TEMWParser.SetPenColor;
begin
  FPage.SetRGBColorStroke(GetRValue(CPen.lopnColor) / 255,
    GetGValue(CPen.lopnColor) / 255, GetBValue(CPen.lopnColor) / 255);
end;

procedure TEMWParser.SetBGColor;
begin
  if CurFill <> BGColor then
  begin
    CurFill := BGColor;
    FPage.SetRGBColorFill(GetRValue(BGColor) / 255, GetGValue(BGColor) / 255, GetBValue(BGColor) / 255);
  end;
end;

procedure TEMWParser.DoModifyWorldTransform;
var
  PWorldTransf: PEMRModifyWorldTransform;
  TR: TXForm;
  function MultiplyXForm(S, T: TXForm): TXForm;
  begin
    Result.eM11 := S.eM11 * T.eM11 + S.eM12 * T.eM21;
    Result.eM12 := S.eM11 * T.eM12 + S.eM12 * T.eM22;
    Result.eM21 := S.eM21 * T.eM11 + S.eM22 * T.eM21;
    Result.eM22 := S.eM21 * T.eM12 + S.eM22 * T.eM22;
    Result.eDx := S.eDx * T.eM11 + S.eDy * T.eM21 + T.eDx;
    Result.eDy := S.eDy * T.eM12 + S.eDy * T.eM22 + T.eDy;
  end;
begin
  PWorldTransf := CV;
  if NT then
  begin
    ModifyWorldTransform(DC, PWorldTransf^.xform, pworldTransf^.iMode);
    GetWorldTransform(DC, TR);
    XOff := TR.eDx * Cal;
    YOff := TR.eDy * Cal;
    XScale := TR.eM11;
    YScale := TR.eM22;
  end
  else
    case PWorldTransf^.iMode of
      MWT_LEFTMULTIPLY:
        begin
          XF := MultiplyXForm(pworldTransf^.xform, XF);
          XScale := XF.eM11;
          YScale := XF.eM22;
          XOff := XF.eDx * Cal;
          YOff := XF.eDy * Cal;
        end;
      MWT_RIGHTMULTIPLY:
        begin
          XF := MultiplyXForm(XF, pworldTransf^.xform);
          XScale := XF.eM11;
          YScale := XF.eM22;
          XOff := XF.eDx * Cal;
          YOff := XF.eDy * Cal;
        end;
      4:
        begin
          XScale := PWorldTransf^.xform.eM11;
          YScale := PWorldTransf^.xform.eM22;
          XOff := PWorldTransf^.xform.eDx * Cal;
          YOff := PWorldTransf^.xform.eDy * Cal;
          Move(PWorldTransf^.xform, XF, SizeOf(XF));
        end;
    end;
end;


procedure TEMWParser.DoSetMapMode;
var
  Data: PEMRSetMapMode;
begin
  Data := CV;
  MapMode := data^.iMode;
  SetMapMode(DC, data^.iMode);
  FCha := True;
end;

{$IFDEF CANVASDBG}

procedure TEMWParser.SaveToLog(EMRPointer: Pointer);
var
  RH: PEMR;
  S: string;
  NDW: DWORD;
  Pin: ^Integer;
  I: Integer;
begin
  RH := EMRPointer;
  case RH^.iType of
    EMR_HEADER: S := 'HEADER';
    EMR_POLYBEZIER: S := 'POLYBEZIER';
    EMR_POLYGON: S := 'POLYGON';
    EMR_POLYLINE: S := 'POLYLINE';
    EMR_POLYBEZIERTO: S := 'POLYBEZIERTO';
    EMR_POLYLINETO: S := 'POLYLINETO';
    EMR_POLYPOLYLINE: S := 'POLYPOLYLINE';
    EMR_POLYPOLYGON: S := 'POLYPOLYGON';
    EMR_SETWINDOWEXTEX: S := 'SETWINDOWEXTEX';
    EMR_SETWINDOWORGEX: S := 'SETWINDOWORGEX';
    EMR_SETVIEWPORTEXTEX: S := 'SETVIEWPORTEXTEX';
    EMR_SETVIEWPORTORGEX: S := 'SETVIEWPORTORGEX';
    EMR_SETBRUSHORGEX: S := 'SETBRUSHORGEX';
    EMR_EOF:
      begin
        S := 'EOF'; FEX := True; end;
    EMR_SETPIXELV: S := 'SETPIXELV';
    EMR_SETMAPPERFLAGS: S := 'SETMAPPERFLAGS';
    EMR_SETMAPMODE: S := 'SETMAPMODE';
    EMR_SETBKMODE: S := 'SETBKMODE';
    EMR_SETPOLYFILLMODE: S := 'SETPOLYFILLMODE';
    EMR_SETROP2: S := 'SETROP2';
    EMR_SETSTRETCHBLTMODE: S := 'SETSTRETCHBLTMODE';
    EMR_SETTEXTALIGN: S := 'SETTEXTALIGN';
    EMR_SETCOLORADJUSTMENT: S := 'SETCOLORADJUSTMENT';
    EMR_SETTEXTCOLOR: S := 'SETTEXTCOLOR';
    EMR_SETBKCOLOR: S := 'SETBKCOLOR';
    EMR_OFFSETCLIPRGN: S := 'OFFSETCLIPRGN';
    EMR_MOVETOEX: S := 'MOVETOEX';
    EMR_SETMETARGN: S := 'SETMETARGN';
    EMR_EXCLUDECLIPRECT: S := 'EXCLUDECLIPRECT';
    EMR_INTERSECTCLIPRECT: S := 'INTERSECTCLIPRECT';
    EMR_SCALEVIEWPORTEXTEX: S := 'SCALEVIEWPORTEXTEX';
    EMR_SCALEWINDOWEXTEX: S := 'SCALEWINDOWEXTEX';
    EMR_SAVEDC: S := 'SAVEDC';
    EMR_RESTOREDC: S := 'RESTOREDC';
    EMR_SETWORLDTRANSFORM: S := 'SETWORLDTRANSFORM';
    EMR_MODIFYWORLDTRANSFORM: S := 'MODIFYWORLDTRANSFORM';
    EMR_SELECTOBJECT: S := 'SELECTOBJECT';
    EMR_CREATEPEN: S := 'CREATEPEN';
    EMR_CREATEBRUSHINDIRECT: S := 'CREATEBRUSHINDIRECT';
    EMR_DELETEOBJECT: S := 'DELETEOBJECT';
    EMR_ANGLEARC: S := 'ANGLEARC';
    EMR_ELLIPSE: S := 'ELLIPSE';
    EMR_RECTANGLE: S := 'RECTANGLE';
    EMR_ROUNDRECT: S := 'ROUNDRECT';
    EMR_ARC: S := 'ARC';
    EMR_CHORD: S := 'CHORD';
    EMR_PIE: S := 'PIE';
    EMR_SELECTPALETTE: S := 'SELECTPALETTE';
    EMR_CREATEPALETTE: S := 'CREATEPALETTE';
    EMR_SETPALETTEENTRIES: S := 'SETPALETTEENTRIES';
    EMR_RESIZEPALETTE: S := 'RESIZEPALETTE';
    EMR_REALIZEPALETTE: S := 'REALIZEPALETTE';
    EMR_EXTFLOODFILL: S := 'EXTFLOODFILL';
    EMR_LINETO: S := 'LINETO';
    EMR_ARCTO: S := 'ARCTO';
    EMR_POLYDRAW: S := 'POLYDRAW';
    EMR_SETARCDIRECTION: S := 'SETARCDIRECTION';
    EMR_SETMITERLIMIT: S := 'SETMITERLIMIT';
    EMR_BEGINPATH: S := 'BEGINPATH';
    EMR_ENDPATH: S := 'ENDPATH';
    EMR_CLOSEFIGURE: S := 'CLOSEFIGURE';
    EMR_FILLPATH: S := 'FILLPATH';
    EMR_STROKEANDFILLPATH: S := 'STROKEANDFILLPATH';
    EMR_STROKEPATH: S := 'STROKEPATH';
    EMR_FLATTENPATH: S := 'FLATTENPATH';
    EMR_WIDENPATH: S := 'WIDENPATH';
    EMR_SELECTCLIPPATH: S := 'SELECTCLIPPATH';
    EMR_ABORTPATH: S := 'ABORTPATH';
    EMR_GDICOMMENT: S := 'GDICOMMENT';
    EMR_FILLRGN: S := 'FILLRGN';
    EMR_FRAMERGN: S := 'FRAMERGN';
    EMR_INVERTRGN: S := 'INVERTRGN';
    EMR_PAINTRGN: S := 'PAINTRGN';
    EMR_EXTSELECTCLIPRGN: S := 'EXTSELECTCLIPRGN';
    EMR_BITBLT: S := 'BITBLT';
    EMR_STRETCHBLT: S := 'STRETCHBLT';
    EMR_MASKBLT: S := 'MASKBLT';
    EMR_PLGBLT: S := 'PLGBLT';
    EMR_SETDIBITSTODEVICE: S := 'SETDIBITSTODEVICE';
    EMR_STRETCHDIBITS: S := 'STRETCHDIBITS';
    EMR_EXTCREATEFONTINDIRECTW: S := 'EXTCREATEFONTINDIRECTW';
    EMR_EXTTEXTOUTA: S := 'EXTTEXTOUTA';
    EMR_EXTTEXTOUTW: S := 'EXTTEXTOUTW';
    EMR_POLYBEZIER16: S := 'POLYBEZIER16';
    EMR_POLYGON16: S := 'POLYGON16';
    EMR_POLYLINE16: S := 'POLYLINE16';
    EMR_POLYBEZIERTO16: S := 'POLYBEZIERTO16';
    EMR_POLYLINETO16: S := 'POLYLINETO16';
    EMR_POLYPOLYLINE16: S := 'POLYPOLYLINE16';
    EMR_POLYPOLYGON16: S := 'POLYPOLYGON16';
    EMR_POLYDRAW16: S := 'POLYDRAW16';
    EMR_CREATEMONOBRUSH: S := 'CREATEMONOBRUSH';
    EMR_CREATEDIBPATTERNBRUSHPT: S := 'CREATEDIBPATTERNBRUSHPT';
    EMR_EXTCREATEPEN: S := 'EXTCREATEPEN';
    EMR_POLYTEXTOUTA: S := 'POLYTEXTOUTA';
    EMR_POLYTEXTOUTW: S := 'POLYTEXTOUTW';
    EMR_SETICMMODE: S := 'SETICMMODE';
    EMR_CREATECOLORSPACE: S := 'CREATECOLORSPACE';
    EMR_SETCOLORSPACE: S := 'SETCOLORSPACE';
    EMR_DELETECOLORSPACE: S := 'DELETECOLORSPACE';
    EMR_GLSRECORD: S := 'GLSRECORD';
    EMR_GLSBOUNDEDRECORD: S := 'GLSBOUNDEDRECORD';
    EMR_PIXELFORMAT: S := 'PIXELFORMAT';
    EMR_DRAWESCAPE: S := 'DRAWESCAPE';
    EMR_EXTESCAPE: S := 'EXTESCAPE';
    EMR_STARTDOC: S := 'STARTDOC';
    EMR_SMALLTEXTOUT: S := 'SMALLTEXTOUT';
    EMR_FORCEUFIMAPPING: S := 'FORCEUFIMAPPING';
    EMR_NAMEDESCAPE: S := 'NAMEDESCAPE';
    EMR_COLORCORRECTPALETTE: S := 'COLORCORRECTPALETTE';
    EMR_SETICMPROFILEA: S := 'SETICMPROFILEA';
    EMR_SETICMPROFILEW: S := 'SETICMPROFILEW';
    EMR_ALPHABLEND: S := 'ALPHABLEND';
    EMR_ALPHADIBBLEND: S := 'ALPHADIBBLEND';
    EMR_TRANSPARENTBLT: S := 'TRANSPARENTBLT';
    EMR_TRANSPARENTDIB: S := 'TRANSPARENTDIB';
    EMR_GRADIENTFILL: S := 'GRADIENTFILL';
    EMR_SETLINKEDUFIS: S := 'SETLINKEDUFIS';
    EMR_SETTEXTJUSTIFICATION: S := 'SETTEXTJUSTIFICATION';
  end;
  NDW := (RH^.nSize - 8) div 4;
  Pin := Pointer(RH);
  Inc(Pin);
  for i := 0 to NDW - 1 do
  begin
    Inc(Pin);
    S := S + ' ' + IntToStr(Pin^);
    if i > 16 then Break;
  end;
  if RH^.iType = EMR_EXTTEXTOUTW then S := S + TXTStr;
  Debug.Add(IntToStr(CurRec) + '   ' + S);
  if LastRecordInContents <> FPage.FContent.Count then
  begin
    Debug.Add('');
    Debug.Add('-----------------------------------');
    for i := LastRecordInContents to FPage.FContent.Count - 1 do
      Debug.Add('     ' + FPage.FContent[i]);
    Debug.Add('-----------------------------------');
    Debug.Add('');
    LastRecordInContents := FPage.FContent.Count;
  end;
end;
{$ENDIF}

procedure TEMWParser.ExecuteRecord(Data: Pointer);
var
  RH: PEMR;
begin
  RH := CV;
  Off := RH^.nSize;
  if InText then
    if not (RH^.iType in
      [EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW, EMR_SELECTOBJECT, EMR_BITBLT,
      EMR_CREATEBRUSHINDIRECT, EMR_CREATEPEN, EMR_SAVEDC, EMR_RESTOREDC,
        EMR_SETTEXTALIGN, EMR_SETBKMODE, EMR_EXTCREATEFONTINDIRECTW, EMR_SMALLTEXTOUT,
        EMR_DELETEOBJECT, EMR_SETTEXTCOLOR, EMR_MOVETOEX, EMR_SETBKCOLOR]) then
      InText := False;
  if (RH^.iType in [EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW, EMR_SMALLTEXTOUT]) then
    if not InText then InText := True;
  case RH^.iType of
    EMR_SETWINDOWEXTEX: DoSetWindowExtEx;
    EMR_SETWINDOWORGEX: DoSetWindowOrgEx;
    EMR_SETVIEWPORTEXTEX: DoSetViewPortExtEx;
    EMR_SETVIEWPORTORGEX: DoSetViewPortOrgEx;
    EMR_SETMAPMODE: DoSetMapMode;
    EMR_POLYBEZIER: DoPolyBezier;
    EMR_POLYGON: DoPolygon;
    EMR_POLYLINE: DoPolyLine;
    EMR_POLYBEZIERTO: DoPolyBezierTo;
    EMR_POLYLINETO: DoPolyLineTo;
    EMR_POLYPOLYLINE: DoPolyPolyLine;
    EMR_POLYPOLYGON: DoPolyPolyGon;
    EMR_EOF: FEX := True;
    EMR_SETPIXELV: DoSetPixelV;
    EMR_SETBKMODE: DoSetBKMode;
    EMR_SETPOLYFILLMODE: DoSetPolyFillMode;
    EMR_SETTEXTALIGN: DoSetTextAlign;
    EMR_SETTEXTCOLOR: DoSetTextColor;
    EMR_SETBKCOLOR: DoSetBKColor;
    EMR_MOVETOEX: DoMoveToEx;
    EMR_INTERSECTCLIPRECT: DoInterSectClipRect;
    EMR_EXCLUDECLIPRECT: DoExcludeClipRect;
    EMR_EXTSELECTCLIPRGN: DoExtSelectClipRGN;
    EMR_SAVEDC: DoSaveDC;
    EMR_RESTOREDC: DoRestoreDC;
    EMR_SETWORLDTRANSFORM: DoSetWorldTransform;
    EMR_MODIFYWORLDTRANSFORM: DoModifyWorldTransform;
    EMR_SELECTOBJECT: DoSelectObject;
    EMR_CREATEPEN: DoCreatePen;
    EMR_CREATEBRUSHINDIRECT: DoCreateBrushInDirect;
    EMR_DELETEOBJECT: DoDeleteObject;
    EMR_ANGLEARC: DoAngleArc;
    EMR_ELLIPSE: DoEllipse;
    EMR_RECTANGLE: DoRectangle;
    EMR_ROUNDRECT: DoRoundRect;
    EMR_ARC: DoArc;
    EMR_CHORD: DoChord;
    EMR_PIE: DoPie;
    EMR_LINETO: DoLineTo;
    EMR_ARCTO: DoArcTo;
    EMR_POLYDRAW: DoPolyDraw;
    EMR_SETARCDIRECTION: DoSetArcDirection;
    EMR_SETMITERLIMIT: DoSetMiterLimit;
    EMR_BEGINPATH: DoBeginPath;
    EMR_ENDPATH: DoEndPath;
    EMR_CLOSEFIGURE: DoCloseFigure;
    EMR_FILLPATH: DoFillPath;
    EMR_STROKEANDFILLPATH: DoStrokeAndFillPath;
    EMR_STROKEPATH: DoStrokePath;
    EMR_SELECTCLIPPATH: DoSelectClipPath;
    EMR_ABORTPATH: DoAbortPath;
    EMR_SETDIBITSTODEVICE: DoSetDibitsToDevice;
    EMR_STRETCHDIBITS: DoStretchDiBits;
    EMR_EXTCREATEFONTINDIRECTW: DoCreateFontInDirectW;
    EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW: DoExtTextOut;
    EMR_POLYBEZIER16: DoPolyBezier16;
    EMR_POLYGON16: DoPolygon16;
    EMR_POLYLINE16: DoPolyLine16;
    EMR_POLYBEZIERTO16: DoPolyBezierTo16;
    EMR_POLYLINETO16: DoPolyLineTo16;
    EMR_POLYPOLYLINE16: DoPolyPolyLine16;
    EMR_POLYPOLYGON16: DoPolyPolygon16;
    EMR_POLYDRAW16: DoPolyDraw16;
    EMR_EXTCREATEPEN: DoExtCreatePen;
    EMR_SETTEXTJUSTIFICATION: DoSetTextJustification;
    EMR_BITBLT: DoBitBLT;
    EMR_SETSTRETCHBLTMODE: DoSetStretchBltMode;
    EMR_STRETCHBLT: DoStretchBlt;
    EMR_SMALLTEXTOUT: DoSmallTextOut;
  end;
end;

procedure TEMWParser.InitExecute;
begin
  BGMode := True;
  PolyFIllMode := True;
  VertMode := vjUp;
  HorMode := hjLeft;
  UpdatePos := False;
  Clipping := False;
  FInPath := False;
  CCW := True;
  com := 72000 div MetaCanvas.Font.PixelsPerInch;
  DDC := True;
  CWPS.cx := 0;
  CWPS.cy := 0;
  FontInited := False;
  CurRec := 0;
  XScale := 1;
  YScale := 1;
  FontScale := 1;
  Fcha := True;
  FCCha := True;
  BCha := True;
  PCha := True;
  MapMode := 1;
  IsNullBrush := False;
  XOff := 0;
  YOff := 0;
  CurVal.X := 0;
  CurVal.Y := 0;
  VEX := 1;
  WEX := 1;
  VEY := 1;
  WEY := 1;
{$IFDEF CANVASDBG}
  LastRecordInContents := FPage.FContent.Count;
  CreateDir('HDCDebug');
  MS.SaveToFile('HDCDebug\' + IntToStr(iii) + '.emf');
{$ENDIF}
  FEX := False;
end;


function TEMWParser.MapX(Value: Extended): Extended;
begin
  case MapMode of
//    MM_LOMETRIC: Result := (Value * MetaCanvas.Font.PixelsPerInch / 256) + VOX;
//    MM_HIMETRIC: ;
//    MM_LOENGLISH: ;
//    MM_HIENGLISH: ;
    MM_ISOTROPIC: Result := ((Value - WOX) * VEX / WEX) + VOX;
    MM_ANISOTROPIC: Result := ((Value - WOX) * VEX / WEX) + VOX;
  else
    Result := Value + VOX;
  end;
end;

function TEMWParser.MapY(Value: Extended): Extended;
begin
  case MapMode of
//    MM_LOMETRIC: Result := (Value * MetaCanvas.Font.PixelsPerInch / 256) + VOY;
//    MM_HIMETRIC: ;
//    MM_LOENGLISH: ;
//    MM_HIENGLISH: ;
    MM_ISOTROPIC: Result := ((Value - WOY) * VEY / WEY) + VOY;
    MM_ANISOTROPIC: Result := ((Value - WOY) * VEY / WEY) + VOY;
  else
    Result := Value + VOY;
  end;
end;

function TEMWParser.FX: Extended;
begin
  if MapMode = 1 then Result := 1 else Result := abs(VEX / WEX);
end;

function TEMWParser.FY: Extended;
begin
  if MapMode = 1 then Result := 1 else Result := abs(VEY / WEY);
end;

procedure TEMWParser.DoSmallTextOut;
var
  SM: PEMRSmallTextOut;
  S: string;
  x, y: Extended;
  A: Extended;
  DY, DX: Extended;
  AN: Extended;
  L: Extended;
  RSZ, NRZ: Extended;
  function GetStr: string;
  var
    S: string;
    CodePage: Integer;
    RS: Integer;
    P: Pointer;
    I: Integer;
  begin
    if CFont.lfCharSet in [0..2] then
    begin
      SetLength(S, sm^.nChars);
      for I := 1 to sm^.nChars do
        S[I] := sm^.Text[i];
      Result := S;
      Exit;
    end else
    begin
      case CFont.lfCharSet of
        EASTEUROPE_CHARSET: CodePage := 1250;
        RUSSIAN_CHARSET: CodePage := 1251;
        GREEK_CHARSET: CodePage := 1253;
        TURKISH_CHARSET: CodePage := 1254;
        BALTIC_CHARSET: CodePage := 1257;
        SHIFTJIS_CHARSET: CodePage := 932;
        129: CodePage := 949;
        CHINESEBIG5_CHARSET: CodePage := 950;
        GB2312_CHARSET: CodePage := 936;
        Symbol_charset: CodePage := -1;
      else
        CodePage := 1252;
      end;
      S := '';
      P := @SM^.Text;
      rs := WideCharToMultiByte(CodePage, 0, P, SM^.nChars, nil, 0, nil, nil);
      if rs <> 0 then
      begin
        SetLength(S, RS);
        WideCharToMultiByte(CodePage, 0, P, SM^.nChars, @s[1], RS, nil, nil)
      end;
    end;
    Result := S;
  end;
begin
  SetCurFont;
  SM := CV;
  if SM^.nChars = 0 then Exit;
  S := GetStr;
  RSZ := MetaCanvas.TextWidth(S);
  RSZ := RSZ * FX;
  A := RSZ;
  RSZ := XScale * Cal * RSZ - 1;
  NRZ := GetTextWidth(S);
  FPage.SetCharacterSpacing((RSZ - NRZ) / SM^.nChars);
  if UpdatePos then
  begin
    X := CurVal.X;
    Y := CurVal.Y;
    if CFont.lfEscapement <> 0 then
    begin
      CurVal.X := CurVal.X + round(a * cos(CFont.lfEscapement * Pi / 1800));
      CurVal.Y := CurVal.Y - round(a * sin(CFont.lfEscapement * Pi / 1800));
    end else CurVal.X := CurVal.X + round(A);
  end else
  begin
    X := SM^.ptlReference.X;
    Y := SM^.ptlReference.Y;
  end;
  if CFont.lfEscapement = 0 then
  begin
    case VertMode of
      vjCenter: Y := GY(Y - MetaCanvas.TextHeight('W'));
      vjDown: Y := GY(Y - MetaCanvas.TextHeight('Wg'));
    else
      Y := GY(Y);
    end;
    case HorMode of
      hjRight: x := GX(X - A);
      hjCenter: x := GX(X - A / 2);
    else
      X := GX(X);
    end;
  end else
  begin
    if (VertMode = vjUp) and (HorMode = hjLeft) then
    begin
      Y := GY(Y);
      X := GX(X);
    end else
    begin
      case VertMode of
        vjCenter: DY := MetaCanvas.TextHeight('W');
        vjDown: DY := MetaCanvas.TextHeight('Wg');
      else
        DY := 0;
      end;
      case HorMode of
        hjRight: DX := A;
        hjCenter: DX := A / 2;
      else
        DX := 0;
      end;
      AN := CFont.lfEscapement * Pi / 1800;
      if DY = 0 then
      begin
        X := X - DX * cos(AN);
        Y := Y + DX * sin(AN);
      end else
      begin
        L := sqrt(sqr(DX) + sqr(DY));
        X := X - L * cos(AN - ArcSin(DY / L));
        Y := Y + L * sin(AN - ArcSin(DY / L));
      end;
      Y := GY(Y);
      X := GX(X);
    end;
  end;
{$IFDEF CANVASDBG}
  TXTStr := TXTStr + #13#10 + Format('X = %f Y = %f ', [X, Y]);
{$ENDIF}
{$IFDEF CB}
  FPage.TextOutput(X, Y, CFont.lfEscapement / 10, s);
{$ELSE}
  FPage.TextOut(X, Y, CFont.lfEscapement / 10, s);
{$ENDIF}
end;

procedure TEMWParser.DoExtSelectClipRGN;
var
  RGNs: PRgnData;
  P: Pointer;
  Data: PEMRExtSelectClipRgn;
  I: Integer;
  RCT:TRect;
begin
  Data := CV;
  if Clipping then
  begin
    Clipping := False;
    FPage.GStateRestore;
    FCha := True;
  end;
  if Data^.cbRgnData <> 0 then
  begin
    FPage.GStateSave;
    GetMem(P, Data^.cbRgnData);
    try
      FPage.NewPath;
      Clipping:=True;
      isCR:=False;
      RGNs:=P;
      Move(Data^.RgnData,P^,data^.cbRgnData);
      for I:=0 to RGNs^.rdh.nCount-1 do
      begin
        Move(Rgns^.Buffer[I*SizeOf(TRect)],RCT,SizeOf(RCT));
        FPage.Rectangle(GX(RCT.Left,False)+1, GY(RCT.Top,False)+1, GX(RCT.Right,False),GY(RCT.Bottom,False));
      end;
      FPage.Clip;
      FPage.NewPath;
    finally
      FreeMem(P);
    end;
  end;
end;

end.

