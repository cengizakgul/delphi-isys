unit rwQBConstsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, Grids, StdCtrls, DBCtrls, Mask, sbSQL,
  rwQBInfo, rwTypes, rwDesignClasses, Wwdbigrd, Wwdbgrid, wwdbedit,
  Wwdotdot, Wwdbcomb, rwCustomDataDictionary, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, rwEngineTypes;

type
  TrwQBConstsEdit = class(TForm)
    evDBGrid1: TisRWDBGrid;
    Panel1: TPanel;
    dsConst: TISRWClientDataSet;
    evDataSource1: TDataSource;
    dsConstName: TStringField;
    dsConstDisplayName: TStringField;
    dsConstType: TIntegerField;
    rgTypes: TDBRadioGroup;
    memValue: TDBMemo;
    edNAme: TDBEdit;
    edDescr: TDBEdit;
    DBNavigator1: TDBNavigator;
    dsConstID: TStringField;
    dsConstValue: TMemoField;
    cbGroup: TwwDBComboBox;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure dsConstBeforePost(DataSet: TDataSet);
    procedure dsConstBeforeDelete(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsConstAfterPost(DataSet: TDataSet);
    procedure cbGroupCloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure cbGroupExit(Sender: TObject);

  private
    FModified: Boolean;

  public
    property Modified: Boolean read FModified;
  end;


implementation

uses sbConstants, rwUtils;

{$R *.dfm}


procedure TrwQBConstsEdit.FormCreate(Sender: TObject);
var
  i, j: Integer;
begin
  for i := 0 to DataDictionary.Tables.Count - 1 do
    for j := 0 to DataDictionary.Tables[i].Fields.Count - 1 do
      cbGroup.Items.AddObject(DataDictionary.Tables[i].Name + '.' + DataDictionary.Tables[i].Fields[j].Name, DataDictionary.Tables[i].Fields[j]);
  cbGroup.Sorted := True;

  dsConst.CreateDataSet;

  dsConst.Tag := 1;
  dsConst.DisableControls;
  for i := 0 to SQLConsts.Count - 1 do
  begin
    dsConst.Append;
    with TrwQBConst(SQLConsts[i]) do
    begin
      dsConst.Fields[0].AsString := Group;
      dsConst.Fields[1].AsString := ConstName;
      dsConst.Fields[2].AsString := DisplayName;
      dsConst.Fields[3].AsInteger := Ord(ConstType);
      dsConst.Fields[4].AsString := StrValue;
    end;
    dsConst.Post;
  end;
  dsConst.First;
  dsConst.EnableControls;
  dsConst.Tag := 0;
  FModified := False;
end;

procedure TrwQBConstsEdit.dsConstBeforePost(DataSet: TDataSet);
var
  C: TrwQBConst;
begin
  if dsConst.Tag <> 0 then
    Exit;

  if dsConst.State = dsInsert then
  begin
    if SQLConsts.ConstantByName(dsConst.Fields[1].AsString) <> nil then
      raise ErwException.Create('The constan name is not unique');

    C := TrwQBConsts(SQLConsts).AddConst(
      dsConst.Fields[0].AsString, dsConst.Fields[1].AsString,
      dsConst.Fields[2].AsString, TsbConstType(dsConst.Fields[3].AsInteger),
      StringReplace(dsConst.Fields[4].AsString, #10, '', [rfReplaceAll]));
    try
      C.Value;
    except
      C.Free;
      raise;
    end;
  end

  else
  begin
    C := TrwQBConst(SQLConsts.ConstantByName(dsConst.Fields[1].OldValue));
    with C do
    begin
      if (ConstName <> dsConst.Fields[1].AsString) and
         (SQLConsts.ConstantByName(dsConst.Fields[1].AsString) <> nil) then
        raise ErwException.Create('The constan name is not unique');
      Group := dsConst.Fields[0].AsString;
      ConstName := dsConst.Fields[1].AsString;
      DisplayName := dsConst.Fields[2].AsString;
      ConstType := TsbConstType(dsConst.Fields[3].AsInteger);
      StrValue := StringReplace(dsConst.Fields[4].AsString, #10, '', [rfReplaceAll]);
    end;
    C.Value;
  end;
end;

procedure TrwQBConstsEdit.dsConstBeforeDelete(DataSet: TDataSet);
begin
  SQLConsts.ConstantByName(dsConst.Fields[1].AsString).Free;
end;

procedure TrwQBConstsEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TrwQBConstsEdit.dsConstAfterPost(DataSet: TDataSet);
begin
  FModified := True;
end;

procedure TrwQBConstsEdit.cbGroupCloseUp(Sender: TwwDBComboBox;
  Select: Boolean);
begin
  if Select then
    cbGroup.OnExit(nil);
end;

procedure TrwQBConstsEdit.cbGroupExit(Sender: TObject);
begin
  if (dsConst.State = dsInsert) and dsConst.FieldByName('Value').IsNull then
    dsConst.FieldByName('Value').AsString := TDataDictField(cbGroup.Items.Objects[cbGroup.ItemIndex]).FieldValues.Text;
end;

end.
