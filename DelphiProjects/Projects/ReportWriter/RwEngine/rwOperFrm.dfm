object rwOper: TrwOper
  Left = 488
  Top = 271
  ActiveControl = rgOper
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Operation'
  ClientHeight = 93
  ClientWidth = 156
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object rgOper: TRadioGroup
    Left = 6
    Top = 4
    Width = 73
    Height = 61
    Caption = 'Operation'
    Items.Strings = (
      'And'
      'Or')
    TabOrder = 0
  end
  object chbNot: TCheckBox
    Left = 6
    Top = 74
    Width = 49
    Height = 17
    Caption = 'Not'
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 87
    Top = 42
    Width = 65
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object btnOK: TButton
    Left = 87
    Top = 9
    Width = 65
    Height = 23
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
