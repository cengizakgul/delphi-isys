unit rwQBParamsFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, StdCtrls, Mask, sbAPI, Variants, rwUtils, isVCLBugFix,
  Wwdatsrc, rwDesignClasses, wwdbedit, Wwdotdot, Wwdbcomb, Wwdbigrd, Wwdbgrid,
  kbmMemTable, ISKbmMemDataSet, rwBasicUtils, ISBasicClasses,
  ISDataAccessComponents;

type
  TrwQBParams = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    grParams: TisRWDBGrid;
    dsParams: TISRWClientDataSet;
    evDataSource1: TDataSource;
    fldName: TStringField;
    fldValue: TStringField;
    fldType: TIntegerField;
    evDBComboBox1: TisRWDBComboBox;
    dsParamsDescription: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure fldTypeChange(Sender: TField);
    procedure FormDestroy(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure dsParamsFilter(DataSet: TDataSet; var Accept: Boolean);
    procedure dsParamsBeforeDelete(DataSet: TDataSet);
    procedure dsParamsNewRecord(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    FParams: TsbParams;
    procedure SyncParams;
  public
    property  Params: TsbParams read FParams;
    procedure FillParams(ASQL: String);
  end;

implementation

{$R *.DFM}

uses rwQueryBuilderFrm;


procedure TrwQBParams.FormCreate(Sender: TObject);
begin
  FParams := TsbParams.Create;
  dsParams.CreateDataSet;
end;

procedure TrwQBParams.fldTypeChange(Sender: TField);
begin
  if fldType.Value = 0 then
  begin
    fldValue.ReadOnly := False;
    fldValue.AsString := '';
    fldValue.ReadOnly := True;
  end
  else
    fldValue.ReadOnly := False;
end;


procedure TrwQBParams.FillParams(ASQL: String);
var
  i: Integer;
begin
  FParams.ParseSQL(ASQL, True);

  dsParams.Filtered := False;
  dsParams.Tag := 0;
  for i := 0 to FParams.ItemCount - 1 do
  begin
    if not dsParams.Locate('name', VarArrayOf([FParams[i].Name]), [loCaseInsensitive]) then
    begin
      dsParams.Append;
      dsParams.FieldByName('name').AsString := FParams[i].Name;
      dsParams.FieldByName('Description').AsString := FParams[i].Name;      
      dsParams.FieldByName('type').AsInteger := 0;
      dsParams.Post;
    end;
  end;
  dsParams.Tag := 1;
  dsParams.Filtered := True;
end;

procedure TrwQBParams.FormDestroy(Sender: TObject);
begin
  FParams.Free;
end;

procedure TrwQBParams.btnOKClick(Sender: TObject);
var
  P: TsbParam;

  function NumToArray: Variant;
  var
    n, i: Integer;
    h, h2, AStr: String;
  begin
    Result := VarArrayOf([]);
    AStr := Trim(dsParams.FieldByName('value').AsString);
    if  AStr = '' then
      Exit;

    n := 0;
    for i := 1 to Length(AStr) do
      if AStr[i] = ',' then
        Inc(n);

    VarArrayRedim(Result, n);

    i := 0;
    h2 := '';
    while AStr <> '' do
    begin
      h := Trim(GetNextStrValue(Astr, ','));
      Result[i] := StrToFloat(h);
      h2 := h2 + ', ' + FloatToStr(Result[i]);
      Inc(i);
    end;

    Delete(h2, 1, 2);
    VarArrayRedim(Result, i - 1);
    dsParams.Edit;
    dsParams.FieldByName('value').AsString := h2;
    dsParams.Post;
  end;


  function StrToArray: Variant;
  var
    n, i: Integer;
    h, h2, AStr: String;
  begin
    Result := VarArrayOf([]);
    AStr := Trim(dsParams.FieldByName('value').AsString);
    if  AStr = '' then
      Exit;

    n := 0;
    for i := 1 to Length(AStr) do
      if AStr[i] = ',' then
        Inc(n);

    VarArrayRedim(Result, n);

    i := 0;
    h2 := '';
    h := Trim(GetNextStrValue(Astr, ''''));
    while AStr <> '' do
    begin
      h := Trim(GetNextStrValue(Astr, ''''));
      Result[i] := h;
      h2 := h2 + ', ''' + Result[i] + '''';
      h := Trim(GetNextStrValue(Astr, ''''));
      Inc(i);
    end;

    Delete(h2, 1, 2);
    VarArrayRedim(Result, i - 1);
    dsParams.Edit;
    dsParams.FieldByName('value').AsString := h2;
    dsParams.Post;
  end;

begin
 dsParams.First;
 while not dsParams.Eof do
 begin
   P := FParams.ParamByName(dsParams.FieldByName('name').AsString);
   case dsParams.FieldByName('type').AsInteger of
     0: P.Value := Null;

     1: if AnsiSameText(dsParams.FieldByName('value').AsString, 'now') or
          AnsiSameText(dsParams.FieldByName('value').AsString, 'today') then
          P.Value := dsParams.FieldByName('value').AsString
        else
          P.Value := StrToDateTime(dsParams.FieldByName('value').AsString);

     2: P.Value := dsParams.FieldByName('value').AsInteger;
     3: P.Value := dsParams.FieldByName('value').AsFloat;
     4: P.Value := dsParams.FieldByName('value').AsString;
     5: P.Value := StrToArray;
     6: P.Value := NumToArray;
   end;
   dsParams.Next;
 end;
end;


procedure TrwQBParams.dsParamsFilter(DataSet: TDataSet;  var Accept: Boolean);
begin
  Accept := Assigned(FParams.ParamByName(dsParams.FieldByName('name').AsString));
end;

procedure TrwQBParams.dsParamsBeforeDelete(DataSet: TDataSet);
begin
  AbortEx;
end;

procedure TrwQBParams.dsParamsNewRecord(DataSet: TDataSet);
begin
  if dsParams.Tag <> 0 then
    AbortEx;
end;

procedure TrwQBParams.FormShow(Sender: TObject);
begin
  SyncParams;
end;

procedure TrwQBParams.SyncParams;
var
  P: TrwQBExtParam;

  function ArrayToNum(const Value: Variant): String;
  var
    i: Integer;
  begin
    Result := '';

    for i := VarArrayLowBound(Value, 1) to varArrayHighBound(Value, 1) do
    begin
      if i > VarArrayLowBound(Value, 1) then
        Result := Result + ',';
      Result := Result + VarToStr(Value[i]);
    end;
  end;


  function ArrayToStr(const Value: Variant): String;
  var
    i: Integer;
  begin
    Result := '';

    for i := VarArrayLowBound(Value, 1) to varArrayHighBound(Value, 1) do
    begin
      if i > VarArrayLowBound(Value, 1) then
        Result := Result + ',';
      Result := Result + '''' + VarToStr(Value[i]) + '''';
    end;
  end;

begin
 if Assigned(FQBFrm.ExternalParams) then
 begin
   dsParams.First;
   while not dsParams.Eof do
   begin
     P := FQBFrm.ExternalParams.ParamByName(dsParams.FieldByName('name').AsString);

     if Assigned(P) then
     begin
       dsParams.Edit;
            
       dsParams.FieldByName('Description').AsString := P.ParamDisplayName;     

       if VarIsNull(P.Value) then
         dsParams.FieldByName('type').AsInteger := 0

       else if VarIsType(P.Value, varDate) then
       begin
         dsParams.FieldByName('type').AsInteger := 1;
         dsParams.FieldByName('value').AsDateTime := P.Value;
       end

       else if VarIsType(P.Value, varString) then
       begin
         if AnsiSameText(P.Value, 'now') or AnsiSameText(P.Value, 'today') then
           dsParams.FieldByName('type').AsInteger := 1
         else
           dsParams.FieldByName('type').AsInteger := 4;

         dsParams.FieldByName('value').AsString := P.Value;
       end

       else if VarIsType(P.Value, varInteger) then
       begin
         dsParams.FieldByName('type').AsInteger := 2;
         dsParams.FieldByName('value').AsInteger := P.Value;
       end

       else if VarIsType(P.Value, varDouble) then
       begin
         dsParams.FieldByName('type').AsInteger := 3;
         dsParams.FieldByName('value').AsFloat := P.Value;
       end

       else if VarIsArray(P.Value) then
       begin
         if VarArrayHighBound(P.Value, 1) - VarArrayLowBound(P.Value, 1) + 1 = 0 then
         begin
           dsParams.FieldByName('type').AsInteger := 5;
           dsParams.FieldByName('value').AsString := '';
         end

         else
           if VarIsType(P.Value[0], varInteger) or VarIsType(P.Value[0], varDouble) then
           begin
             dsParams.FieldByName('type').AsInteger := 6;
             dsParams.FieldByName('value').AsString := ArrayToNum(P.Value);
           end
           else
           begin
             dsParams.FieldByName('type').AsInteger := 5;
             dsParams.FieldByName('value').AsString := ArrayToStr(P.Value);
           end
       end;

       dsParams.Post;
     end;

     dsParams.Next;
   end;
 end;  
end;

end.
