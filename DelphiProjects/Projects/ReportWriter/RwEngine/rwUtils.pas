unit rwUtils;

interface

uses Forms, Windows, Controls, Sysutils, Classes, DB, Dialogs, TypInfo, SyncObjs, Variants,
     rwTypes, rwMessages, rwBasicClasses, rwCustomDataDictionary, rwDataDictionary, rwQBInfo,
     rwEngineTypes, sbAPI, Graphics, EvStreamUtils, rwBasicUtils, Math, rmTypes, rwRTTI, Clipbrd,
     rwAdapterUtils, isExceptions;

type
  TrwUnitType = (utInches, utMillimeters, utMMThousandths, utScreenPixels, utLogicalPixels);

  TrwStatisticRec = record
    Lock: Boolean;
    DateTimeStamp: TDateTime;
    ReportName: ShortString;
    TotalTime: DWORD;
    ConnectionTime: DWORD;
    RemoteQueriesTime: DWORD;
    CacheDataTime: DWORD;
    CacheDataSize: DWORD;
    LogicalQueriesTime: DWORD;
    RemoteConnections: DWORD;
    RemoteQueries: DWORD;
    LogicalQueries: DWORD;
  end;

  PTrwStatisticRec = ^TrwStatisticRec;

function CreateRWComponentByClass(const AClassName: String; const AInherit: Boolean = True): IrmDesignObject;

function SetComponentProperty(AObject: TObject; const PropName: array of ShortString; const Value: Variant): Boolean; overload;
function GetComponentProperty(AObject: TObject; const PropName: array of ShortString): Variant; overload;
function SetComponentProperty(AObject: TObject; PropName: String; const Value: Variant): Boolean; overload;
function GetComponentProperty(AObject: TObject; PropName: String): Variant; overload;
function GetComponentPropertyInfo(var AObject: TObject; const PropName: array of ShortString): PPropInfo;
function FindClassPropValue(AObject: TObject; const APropType: String): TrwStrList;

function GetUniqueNameForComponent(AComponent: TrwComponent; TrailNumber: Boolean = True; Prefix: string = ''; Postfix: string = ''; NoClassName: Boolean = False; ForceGlobalUniqueness: Boolean = False): string;
function ConvertUnit(Value: Extended; FromUnit: TrwUnitType; ToUnit: TrwUnitType): Extended;
function InternalTypeToVariant(AType: TrwVarTypeKind; const AValue: Variant): Variant;
function VariantToInternalType(const AValue: Variant): TrwVarTypeKind;
procedure SaveRWResToFile(AComponent: TComponent; AFileName: string);
procedure SaveRWResToStream(AComponent: TComponent; AStream: TStream);
function  LoadRWResFromFile(AComponent: TComponent; AFileName: string): TComponent;
function  LoadRWResFromStream(AComponent: TComponent; AStream: TStream): TComponent;
function  NameOfVarType(const AVarType: TrwVarTypeKind): String;
function  VarTypeToString(AVar: TrwVariable): String;
procedure CheckReadOnlyAttr(AFileName: string);
function  GetComponentPath(const AComponent: TrwComponent): string;
function  ComponentByPath(const APath: String; AOwner: TrwComponent; ShowError: Boolean = False): TrwComponent; overload;
function  ComponentByPath(const APath: String; AOwner: IrmDesignObject; ShowError: Boolean = False): IrmDesignObject; overload;
function  GetClassHierarchy(const AClassName: String): String;
function  FormatValue(Format: string; const Value: Variant): string;
function  DDTypeToRWType(AType: TDataDictDataType): TrwVarTypeKind;
function  RWTypeToDDType(AType: TrwVarTypeKind): TDataDictDataType;
function  DDTypetoSBType(const AType: TDataDictDataType): TsbFieldType;
function  SBTypetoDDType(const AType: TsbFieldType): TDataDictDataType;
procedure RegisterRWClasses(const AClasses: array of TPersistentClass; const ANames: array of String);
function  GetRWClassDescription(const AClass: TPersistentClass): String;
function  CompareObjects(AObj1, AObj2: TPersistent): Boolean;
function  VariantsAreSame(const A, B: Variant): Boolean;
function  GlobalFindFunction(const AFunctionName: String): TrwFunction;
function  IsRMReport(AData: TStream): Boolean;
function  FindQBQueryProp(const AComponent: IrmDesignObject): String;
function  FindDataSetProp(const AComponent: IrmDesignObject): String;
procedure CheckInheritedAndShowError(AObj: TrwComponent);

function  MakeDomainList(const AValues: array of Variant; const ADescriptions: array of String): TrwDomainList;
function  MakeEmptyDomainList: TrwDomainList;

procedure PutToClipboard(ADataType: WORD; const AData: String);
function  GetFromClipboard(ADataType: WORD): String;

function  GetEntryPoint(AbsoluteAddress: Integer): TrwVMAddress;
function  GetAbsoluteAddress(const AAddress: TrwVMAddress): Integer;
function  VMAddressToString(const AAddress: TrwVMAddress): String;
function  VMAddrListToVariant(const AddrList: TrwVMAddressList): Variant;
function  VariantToVMAddrList(const AddrList: Variant): TrwVMAddressList;
function  VMAddressToVariant(const VMAddress: TrwVMAddress): Variant;
function  VariantToVMAddress(const VMAddress: Variant): TrwVMAddress;

function  DataDictionary(const AExternalData: IEvDualStream = nil): TrwDataDictionary;
procedure UpdateDataDictionary(ADict: TrwDataDictionary);
function  SystemLibComponents(const AExternalData: IEvDualStream = nil): TrwLibComponents;
function  SystemLibFunctions(const AExternalData: IEvDualStream = nil): TrwLibFunctions;
function  IsFunctLibInitialized: Boolean;
function  IsCompLibInitialized: Boolean;
function  DefPropComponents: TrwDefPropComponents;
function  PublishedMethods: TrwPublishedMethods;
function  QBFunctions: TrwQBFunctions;
function  QBTemplates: TrwQBTemplates;
function  AggrFunctions: TrmAggrFunctions;
procedure FreeDataDictionary;
function  GlobalLocateRWFunction(const AFunctionName: String): TrwFunction;
function  ClassObjectType(const AClassName: String): TrwDsgnObjectType;

function  rwDictionary: TrwDictionary;

procedure Busy;
procedure BusyBackground;
procedure Ready;
function  GetSysTempDir: String;
function  GetTmpDir: string;
function  GetThreadTmpDir: string;
function  GetUniqueFileName(const ADir: String): string;
function  DirExists(Name: string): Boolean;
function  NormalDir(const DirName: string): string;
function  ClearDir(const Path: string; Delete: Boolean = False): Boolean;
function  NormalizeFileName(const AFileName: String): String;

function  RwStatRec: PTrwStatisticRec;
procedure PrepareQueries(AOwner: TrwComponent);

function  VarArrayToStream(const vData: Variant; const AStream: TStream = nil; const AOleStrToStr: Boolean = False): TStream;
function  StreamToVarArray(const AStream: TStream): Variant;

procedure  InitThreadVars_rwShared;




var
  RWClassList: TStringList = nil;
  AppAdapterInfo: TrwAppAdapterInfo;

implementation

uses rwEngine, rwReport, rmReport, rwDB, rwLogQuery, rwRegister, rwRendering,
     rwParser, rwVirtualMachine, rwGraphics;

type
  TevVariantToStreamHolder = class
  private
    FStream: TStream;
    function InternalTypeToVarType(AIntType: Byte): Integer;
    function ReadArray: Variant;
    function ReadElement(AType: Byte): Variant;
  public
    function StreamToVarArray(const AStream: TStream): Variant;
  end;


var
  gvSystemLibComponents: TrwLibComponents = nil;
  gvSystemLibFunctions: TrwLibFunctions = nil;
  gvSourceDataDictionary: IEvDualStream = nil;
  gvrwDictionary: TrwDictionary = nil;
  gvQBTemplates: TrwQBTemplates = nil;
  gvQBFunctions: TrwQBFunctions = nil;
  gvAggrFunctions: TrmAggrFunctions = nil;
  gvDefPropComponents: TrwDefPropComponents = nil;
  gvPublishedMethods: TrwPublishedMethods = nil;

threadvar
  tvSystemLibComponents: TrwLibComponents;
  tvSystemLibFunctions: TrwLibFunctions;
  tvDataDictionary: TrwDataDictionary;
  tvrwDictionary: TrwDictionary;
  tvQBTemplates: TrwQBTemplates;
  tvQBFunctions: TrwQBFunctions;
  tvAggrFunctions: TrmAggrFunctions;
  tvDefPropComponents: TrwDefPropComponents;
  tvPublishedMethods: TrwPublishedMethods;

  tvStat: TrwStatisticRec;

procedure InitThreadVars_rwShared;
begin
  tvSystemLibComponents := nil;
  tvSystemLibFunctions := nil;
  tvDataDictionary := nil;
  tvrwDictionary := nil;
  tvQBTemplates := nil;
  tvQBFunctions := nil;
  tvAggrFunctions := nil;
  tvDefPropComponents := nil;
  tvPublishedMethods := nil;

  InitThreadVars_lqQuery;
  InitThreadVars_rwRendering;
  InitThreadVars_rwParser;
  InitThreadVars_rwVirtualMachine;
  InitThreadVars_SUVbase;
end;



procedure RegisterRWClasses(const AClasses: array of TPersistentClass; const ANames: array of String);
var
  i: Integer;
begin
  RegisterClasses(AClasses);

  for i := Low(AClasses) to High(AClasses) do
  begin
    if RWClassList.IndexOfObject(Pointer(AClasses[i])) = -1 then
      RWClassList.AddObject(ANames[i], Pointer(AClasses[i]));
  end;
end;

function  GetRWClassDescription(const AClass: TPersistentClass): String;
var
  i: Integer;
begin
  i := RWClassList.IndexOfObject(Pointer(AClass));
  if i <> - 1 then
    Result := RWClassList[i]
  else
    Result := '';
end;

function RwStatRec: PTrwStatisticRec;
begin
  Result := @tvStat;
end;


function GetTmpDir: string;
begin
  Result := RWEngineExt.AppAdapter.GetTempDir;
  if Result = '' then
    Result := GetSysTempDir;
  Result := Result + 'RWTMP' + PathDelim;
end;

function GetThreadTmpDir: string;
begin
  if CacheInfo <> nil then
    Result := CacheInfo.GetCachePath
  else
    Result := GetTmpDir;
end;

procedure Busy;
begin
  Screen.Cursor := crHourGlass;
end;


procedure BusyBackground;
begin
  Screen.Cursor := crAppStart;
end;


procedure Ready;
begin
  Screen.Cursor := crDefault;
end;


function DirExists(Name: string): Boolean;
begin
  Result :=  DirectoryExists(Name) or FileExists(Name);
end;

function NormalDir(const DirName: string): string;
begin
  Result := DirName;
  if (Result <> '') and not (AnsiLastChar(Result)^ in [':', '\']) then
  begin
    if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
      Result := Result + ':\'
    else
      Result := Result + PathDelim;
  end;
end;

function ClearDir(const Path: string; Delete: Boolean = False): Boolean;
const
  FileNotFound = 18;
var
  FileInfo: TSearchRec;
  DosCode: Integer;
begin
  Result := DirExists(Path);
  if not Result then Exit;
  DosCode := SysUtils.FindFirst(NormalDir(Path) + '*.*', faAnyFile, FileInfo);
  try
    while DosCode = 0 do
    begin
      if (FileInfo.Name[1] <> '.') then
      begin
        if (FileInfo.Attr and faDirectory = faDirectory) then
          Result := ClearDir(NormalDir(Path) + FileInfo.Name, True) and Result
        else
          Result := Sysutils.DeleteFile(NormalDir(Path) + FileInfo.Name) and Result;
      end;
      DosCode := SysUtils.FindNext(FileInfo);
    end;
  finally
    Sysutils.FindClose(FileInfo);
  end;
  if Delete and Result and (DosCode = FileNotFound) and
    not ((Length(Path) = 2) and (Path[2] = ':')) then
  begin
    Result := RemoveDir(Path);
//    Result := (IOResult = 0) and Result;
  end;
end;


function SetComponentProperty(AObject: TObject; PropName: String; const Value: Variant): Boolean; overload;
var
  i: Integer;
  lProp: TPropNamesArray;
begin
  SetLength(lProp, 10);  // max hierarchy depth of the property
  i := 0;
  while PropName <> '' do
  begin
    lProp[i] := GetNextStrValue(PropName, '.');
    Inc(i);
  end;
  SetLength(lProp, i);

  Result := SetComponentProperty(AObject, lProp, Value);
end;

function GetComponentProperty(AObject: TObject; PropName: String): Variant; overload;
var
  i: Integer;
  lProp: TPropNamesArray;
begin
  SetLength(lProp, 10);  // max hierarchy depth of the property
  i := 0;
  while PropName <> '' do
  begin
    lProp[i] := GetNextStrValue(PropName, '.');
    Inc(i);
  end;
  SetLength(lProp, i);

  Result := GetComponentProperty(AObject, lProp);
end;

function GetComponentPropertyInfo(var AObject: TObject; const PropName: array of ShortString): PPropInfo;
var
  PTypeInf: PTypeInfo;
  j, n: Integer;
begin
  Result := nil;

  PTypeInf := AObject.ClassInfo;
  n := High(PropName);
  for j := 0 to High(PropName) do
  begin
    Result := GetPropInfo(PTypeInf, PropName[j]);

    if not Assigned(Result) then
    begin
      if AObject is TComponent then
        AObject := TComponent(AObject).FindComponent(PropName[j]);
      if not Assigned(AObject) then
        break;
      PTypeInf := AObject.ClassInfo;
      Continue;
    end;

    if j < n then
    begin
      AObject := TObject(GetOrdProp(AObject, Result));
      PTypeInf := AObject.ClassInfo;
    end;
  end;
end;


function GetComponentProperty(AObject: TObject; const PropName: array of ShortString): Variant;
var
  PPropInf: PPropInfo;
begin
  Result := Null;
  PPropInf := GetComponentPropertyInfo(AObject, PropName);

  if not Assigned(AObject) or not Assigned(PPropInf) then
    Exit;

  case PPropInf^.PropType^.Kind of
    tkClass,
    tkInteger,
    tkEnumeration:
        begin
          Result := GetOrdProp(AObject, PPropInf);
          if PPropInf^.PropType^ = TypeInfo(Boolean) then
            Result := VarAsType(Result, varBoolean);
        end;

    tkFloat: Result := GetFloatProp(AObject, PPropInf);

    tkSet: Result := GetOrdProp(AObject, PPropInf);

    tkString,
    tkLString,
    tkWString: Result := GetStrProp(AObject, PPropInf);

    tkVariant: Result := GetVariantProp(AObject, PPropInf);
  end;
end;


function SetComponentProperty(AObject: TObject; const PropName: array of ShortString; const Value: Variant): Boolean;
var
  PPropInf: PPropInfo;
  SetData1, SetData2: LongInt;
  h: String;
  SetOperation: Char;
begin
  Result := False;

  PPropInf := GetComponentPropertyInfo(AObject, PropName);
  if not Assigned(AObject) or not Assigned(PPropInf) then
    Exit;

  case PPropInf^.PropType^.Kind of
    tkInteger,
    tkClass,
    tkEnumeration:
       SetOrdProp(AObject, PPropInf, Value);

    tkFloat:
       SetFloatProp(AObject, PPropInf, Value);

    tkSet:
      begin
        h := String(Value);
        SetOperation := h[1];
        if h[1] <> '[' then
          Delete(h, 1, 1);
        SetData1 := GetOrdProp(AObject, PPropInf);
        SetSetProp(AObject, PPropInf, h);
        SetData2 := GetOrdProp(AObject, PPropInf);
        case SetOperation of
          '+': SetData2 := (SetData1 or SetData2);
          '-': SetData2 := (SetData1 and not SetData2);
        end;
        SetOrdProp(AObject, PPropInf, SetData2);
      end;

    tkString,
    tkLString,
    tkWString: SetStrProp(AObject, PPropInf, string(Value));

    tkVariant: SetVariantProp(AObject, PPropInf, Value);
  end;

  Result := True;
end;

function FindClassPropValue(AObject: TObject; const APropType: String): TrwStrList;
var
  PropList: PPropList;
  i, n: Integer;
  UP: TrwUserProperties;

  procedure AddProp(const AName: String);
  begin
    SetLength(Result, Length(Result) + 1);
    Result[High(Result)] := AName;
  end;

begin
  SetLength(Result, 0);

  if AObject is TrwComponent then
  begin
    UP := TrwComponent(AObject)._UserProperties;
    for i := 0 to UP.Count - 1 do
      if (UP[i].PPropInfo.PropType^.Kind = tkClass) and SameText(UP[i].PointerType, APropType) then
        AddProp(UP[i].Name);
  end;

  n := GetPropList(AObject, PropList);

  for i := 0 to n - 1 do
    if SameText(PropList^[i].PropType^.Name, APropType) then
      AddProp(PropList^[i].Name);
end;

function GetUniqueNameForComponent(AComponent: TrwComponent; TrailNumber: Boolean = True; Prefix: string = ''; Postfix: string = '';  NoClassName: Boolean = False; ForceGlobalUniqueness: Boolean = False): string;
var
  lStr, lh: string;
  i, n: Integer;
  Own: TrwComponent;
  GlobalUniqness: Boolean;
begin
  Result := '';
  Own := AComponent.GetGlobalOwner;
  GlobalUniqness := Assigned(Own);
  if not GlobalUniqness then
    if ForceGlobalUniqueness then
    begin
      GlobalUniqness := True;
      Own := AComponent.RootOwner;
    end
    else
      Own := TrwComponent(AComponent.Owner);

  if not Assigned(Own) then
    Exit;

  if NoClassName then
    lStr := ''
  else
  begin
    lStr := AComponent.ClassName;
    Delete(lStr, 1, 3);
  end;

  if TrailNumber then
    n := 1
  else
    n := 0;

  for i := n to 1000 do //Limit quantity of components of the same class
  begin
    lh := Prefix + lStr + Postfix;
    if i > 0 then
      lh := lh + IntToStr(i);

    if GlobalUniqness and (Own.GlobalFindComponent(lh) = nil) or
       not GlobalUniqness and (Own.FindComponent(lh) = nil) then
    begin
      Result := lh;
      break;
    end;
  end;
end;


function ConvertUnit(Value: Extended; FromUnit: TrwUnitType; ToUnit: TrwUnitType): Extended;
var
  h: Extended;
begin
  case FromUnit of
    utMillimeters:    h := (Value / 25.4) * cScreenCanvasRes * cCanvasToScreenZoomRatio;
    utMMThousandths:  h := ((Value / 1000) / 25.4) * cScreenCanvasRes * cCanvasToScreenZoomRatio;
    utInches:         h := Value * cScreenCanvasRes * cCanvasToScreenZoomRatio;
    utScreenPixels:   h := Value * cCanvasToScreenZoomRatio;
    utLogicalPixels:  h := Value;
  else
    h := Value;
  end;

  case ToUnit of
    utMillimeters:    Result := ((h / cCanvasToScreenZoomRatio) / cScreenCanvasRes) * 25.4;
    utMMThousandths:  Result := ((h / cCanvasToScreenZoomRatio) / cScreenCanvasRes) * 25.4 * 1000;
    utInches:         Result := (h / cCanvasToScreenZoomRatio) / cScreenCanvasRes;
    utScreenPixels:   Result := h / cCanvasToScreenZoomRatio;
    utLogicalPixels:  Result := h;
  else
    Result := Value;
  end;
end;


function InternalTypeToVariant(AType: TrwVarTypeKind; const AValue: Variant): Variant;
begin
  if VarIsNull(AValue) then
    Result := Null
  else
    case AType of
      rwvInteger:  Result := VarAsType(AValue, varInteger);

      rwvBoolean:  if AValue then
                     Result := True
                   else
                     Result := False;

      rwvString:   Result := VarAsType(AValue, varString);
      rwvFloat:    Result := VarAsType(AValue, varDouble);
      rwvCurrency: Result := VarAsType(RoundTo(VarAsType(AValue, varCurrency), -2), varCurrency);
      rwvDate:     Result := VarAsType(AValue, varDate);
      rwvPointer:  Result := VarAsType(AValue, varInteger);
      rwvVariant:  Result := AValue;
      rwvUnknown:  VarClear(Result);
    else
      raise ErwException.Create(ResErrWrongType);
    end
end;


function VariantToInternalType(const AValue: Variant): TrwVarTypeKind;
begin
  if VarIsArray(AValue) then
    Result := rwvArray

  else
    case VarType(AValue) of
      varInteger:             Result := rwvInteger;
      varDouble:              Result := rwvFloat;
      varString, varOleStr:   Result := rwvString;
      varCurrency:            Result := rwvCurrency;
      varDate:                Result := rwvDate;
    else
      Result := rwvVariant;
    end;
end;

procedure SaveRWResToFile(AComponent: TComponent; AFileName: string);
var
  lFileStream: TEvFileStream;
  hSt: IEvDualStream;
begin
  CheckReadOnlyAttr(AFileName);

  lFileStream := TEvFileStream.Create(AFileName, fmCreate);
  hSt := nil;

  try
    if AnsiUpperCase(ExtractFileExt(AFileName)) = '.TXT' then
    begin
      hSt := TEvDualStreamHolder.Create;
      SaveRWResToStream(AComponent, hSt.RealStream);
      hSt.Position := 0;
      ObjectBinaryToText(hSt.RealStream, lFileStream);
    end
    else
    begin
      lFileStream.Position := 0;
      SaveRWResToStream(AComponent, lFileStream);
    end;
  finally
    lFileStream.Free;
  end;
end;


procedure SaveRWResToStream(AComponent: TComponent; AStream: TStream);
begin
  if AComponent is TrwReport then
    TrwReport(AComponent).SaveToStream(AStream)
  else if AComponent is TrmReport then
    TrmReport(AComponent).SaveToStream(AStream)
  else
    AStream.WriteComponent(AComponent);
end;


function LoadRWResFromFile(AComponent: TComponent; AFileName: string): TComponent;
var
  lFileStream: TEvFileStream;
  hSt: IEvDualStream;
  St: TStream;
begin
  lFileStream := TEvFileStream.Create(AFileName, fmOpenRead);
  hSt := nil;

  try
    if AnsiUpperCase(ExtractFileExt(AFileName)) = '.TXT' then
    begin
      hSt := TEvDualStreamHolder.Create;
      lFileStream.Position := 0;
      ObjectTextToBinary(lFileStream, hSt.RealStream);
      St := hSt.RealStream;
    end
    else
      St := lFileStream;

    St.Position := 0;
    Result := LoadRWResFromStream(AComponent, St);

  finally
    lFileStream.Free;
  end;
end;

function LoadRWResFromStream(AComponent: TComponent; AStream: TStream): TComponent;
var
  Reader: TrwReader;
begin
  if AComponent is TrwReport then
  begin
    TrwReport(AComponent).LoadFromStream(AStream);
    Result := AComponent;
  end

  else if AComponent is TrmReport then
  begin
    TrmReport(AComponent).LoadFromStream(AStream);
    Result := AComponent;
  end

  else
  begin
    Reader := TrwReader.Create(AStream);
    try
      Result := Reader.ReadRootComponent(AComponent);
      if Result is TrwComponent then
        TrwComponent(Result).FixUpUserProperties;
    finally
      FreeAndNil(Reader);
    end;
  end;
end;


function NameOfVarType(const AVarType: TrwVarTypeKind): String;
begin
  Result := GetEnumName(TypeInfo(TrwVarTypeKind), Ord(AVarType));
  Delete(Result, 1, 3);
end;


function VarTypeToString(AVar: TrwVariable): String;
begin
  if (AVar.VarType = rwvPointer) and
     (Length(AVar.PointerType) > 0) then
    Result := AVar.PointerType
  else
    Result := NameOfVarType(AVar.VarType);
end;


procedure CheckReadOnlyAttr(AFileName: string);
var
  fa: DWORD;
begin
  if FileExists(AFileName) then
  begin
    fa := GetFileAttributes(PChar(AFileName));
    if (fa and FILE_ATTRIBUTE_READONLY) <> 0 then
      raise EInOutError.Create('Can not write to file. File "'+AFileName+'" has a "Read Only" attribute!');
  end;
end;


function GetComponentPath(const AComponent: TrwComponent): string;
var
  Own: TrwComponent;
begin
  Result := '';
  Own := TrwComponent(AComponent.Owner);
  while Assigned(Own) do
  begin
    if not (Own is TrwReportForm) then
      Result := Own.Name + '.' + Result;
    Own := TrwComponent(Own.Owner);
  end;

  Result := Result + AComponent.Name;
end;


function ComponentByPath(const APath: String; AOwner: IrmDesignObject; ShowError: Boolean = False): IrmDesignObject;
begin
  Result := ComponentByPath(APath, TrwComponent(AOwner.RealObject), ShowError);
end;

function ComponentByPath(const APath: String; AOwner: TrwComponent; ShowError: Boolean = False): TrwComponent;
var
  h, cName: String;
  O: TrwComponent;
begin
  Result := nil;

  h := APath;
  cName := GetNextStrValue(h, '.');
  O := AOwner;

  if SameText(O.Name, cName) then
  begin
    while h <> '' do
    begin
      cName := GetNextStrValue(h, '.');
      O := TrwComponent(O.FindComponent(cName));
      if not Assigned(O) then
        break;
    end;

    if h = '' then
      Result := O;
  end;

  if ShowError and not Assigned(Result) then
    raise ErwException.Create('Object "' + APath + '" is not found');
end;

function GetClassHierarchy(const AClassName: String): String;
var
  ClName: String;
  Cl: IrwClass;
begin
  ClName := AClassName;
  Result := AClassName;
  while not SameText(ClName, 'TrwComponent') do
  begin
    Cl := rwRTTIInfo.FindClass(ClName);

    if not Assigned(Cl) then
      break;

    Cl := Cl.GetAncestor;
    if not Assigned(Cl) then
      break;

    ClName := Cl.GetClassName;

    Result := ClName + '.' + Result;
  end;
end;

function FormatValue(Format: string; const Value: Variant): string;
begin
  if VarIsNull(Value) then
  begin
    Result := '';
    Exit;
  end;

  if (Format = '') then
  begin
    Result := VarToStr(Value);
    Exit;
  end

  else
    case VarType(Value) of
      varSmallint,
      varInteger,
      varSingle,
      varDouble,
      varByte,
      varCurrency: Result := FormatFloat(Format, Value);
      varDate:     Result := FormatDateTime(Format, Value);
    else
      Result := Value;
    end;
end;


function DDTypeToRWType(AType: TDataDictDataType): TrwVarTypeKind;
begin
  case AType of
    ddtDateTime: Result := rwvDate;
    ddtInteger:  Result := rwvInteger;
    ddtFloat:    Result := rwvFloat;
    ddtString:   Result := rwvString;
    ddtCurrency: Result := rwvCurrency;
    ddtArray:    Result := rwvArray;
    ddtBoolean:  Result := rwvBoolean;
    ddtBLOB,
    ddtMemo,
    ddtGraphic:  Result := rwvPointer;
  else
    Result := rwvUnknown;
  end;
end;


function RWTypeToDDType(AType: TrwVarTypeKind): TDataDictDataType;
begin
  case AType of
    rwvDate:     Result := ddtDateTime;
    rwvInteger:  Result := ddtInteger;
    rwvFloat:    Result := ddtFloat;
    rwvString:   Result := ddtString;
    rwvCurrency: Result := ddtCurrency;
    rwvArray:    Result := ddtArray;
    rwvBoolean:  Result := ddtBoolean;
    rwvPointer:  Result := ddtBLOB;
  else
    Result := ddtUnknown;
  end;
end;


function  DDTypetoSBType(const AType: TDataDictDataType): TsbFieldType;
begin
  case AType of
    ddtInteger:  Result := sbfInteger;
    ddtFloat:    Result := sbfFloat;
    ddtCurrency: Result := sbfCurrency;
    ddtDateTime: Result := sbfDateTime;
    ddtString:   Result := sbfString;
    ddtBLOB,
    ddtMemo,
    ddtGraphic:  Result := sbfBLOB;
    ddtBoolean:  Result := sbfBoolean;
  else
    Result := sbfUnknown;
    Assert(True, 'Field type is not supported');
  end;
end;

function  SBTypetoDDType(const AType: TsbFieldType): TDataDictDataType;
begin
  case AType of
    sbfInteger:  Result := ddtInteger;
    sbfFloat:    Result := ddtFloat;
    sbfCurrency: Result := ddtCurrency;
    sbfDateTime: Result := ddtDateTime;
    sbfString:   Result := ddtString;
    sbfBLOB:     Result := ddtBLOB;
    sbfBoolean:  Result := ddtBoolean;
  else
    Result := ddtUnknown;
    Assert(True, 'Field type is not supported');
  end;
end;


function DataDictionary(const AExternalData: IEvDualStream = nil): TrwDataDictionary;
var
  dd: TrwDataDictionary;
begin
  if Assigned(tvDataDictionary) and Assigned(AExternalData) then
  begin
    gvSourceDataDictionary := nil;
    FreeAndNil(tvDataDictionary);
  end;

  if not Assigned(tvDataDictionary) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvSourceDataDictionary) then
      begin
        if Assigned(AExternalData) then
          gvSourceDataDictionary := AExternalData
        else
        begin
          dd := TrwDataDictionary.Create(nil);
          try
            dd.Initialize;
            gvSourceDataDictionary := TEvDualStreamHolder.Create;
            dd.SaveToStream(gvSourceDataDictionary.RealStream);
          finally
            dd.Free;
          end;
        end;
      end;

      tvDataDictionary := TrwDataDictionary.Create(nil);
      try
        gvSourceDataDictionary.Position := 0;
        tvDataDictionary.LoadFromStream(gvSourceDataDictionary.RealStream);
      except
        FreeAndNil(tvDataDictionary);
      end;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvDataDictionary;
end;

procedure UpdateDataDictionary(ADict: TrwDataDictionary);
begin
  GlobalNameSpace.BeginWrite;
  try
    if Assigned(gvSourceDataDictionary) then
    begin
      gvSourceDataDictionary.Clear;
      ADict.SaveToStream(gvSourceDataDictionary.RealStream);
    end;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;


function rwDictionary: TrwDictionary;
begin
  if not Assigned(tvrwDictionary) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvrwDictionary) then
        gvrwDictionary := TrwDictionary.Create(nil);

      tvrwDictionary := gvrwDictionary;
      
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;  

  Result := tvrwDictionary;
end;


procedure FreeDataDictionary;
begin
  FreeAndNil(tvDataDictionary);
end;


function DefPropComponents: TrwDefPropComponents;
begin
  if not Assigned(tvDefPropComponents) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvDefPropComponents) then
      begin
        gvDefPropComponents := TrwDefPropComponents.Create(TrwDefPropComponent);
        gvDefPropComponents.Initialize;
      end;
      tvDefPropComponents := gvDefPropComponents;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvDefPropComponents;
end;


function SystemLibComponents(const AExternalData: IEvDualStream = nil): TrwLibComponents;
begin
  if IsCompLibInitialized and Assigned(AExternalData) then
  begin
    FreeAndNil(gvSystemLibComponents);
    tvSystemLibComponents := nil;
  end;

  if not Assigned(tvSystemLibComponents) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvSystemLibComponents) then
      begin
        RWEngineExt.AppAdapter.StartEngineProgressBar('Initialization of RW Component Library...');
        try
          gvSystemLibComponents := TrwLibComponents.Create(nil, TrwLibComponent);
          try
           if Assigned(AExternalData) then
           begin
             AExternalData.Position := 0;
             gvSystemLibComponents.LoadFromStream(AExternalData.RealStream)
           end
           else
             gvSystemLibComponents.Initialize;
          except
            FreeAndNil(gvSystemLibComponents);
            raise;
          end;
        finally
          RWEngineExt.AppAdapter.EndEngineProgressBar;
        end;
      end;

      tvSystemLibComponents := gvSystemLibComponents;      
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvSystemLibComponents;
end;


function PublishedMethods: TrwPublishedMethods;
begin
  if not Assigned(tvPublishedMethods) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvPublishedMethods) then
      begin
        gvPublishedMethods := TrwPublishedMethods.Create(nil, TrwPublishedMethod);
        gvPublishedMethods.Initialize;
      end;
      tvPublishedMethods := gvPublishedMethods;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvPublishedMethods;
end;


function QBFunctions: TrwQBFunctions;
begin
  if not Assigned(tvQBFunctions) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvQBFunctions) then
      begin
        gvQBFunctions := TrwQBFunctions.Create(nil, TrwQBFunction);
        gvQBFunctions.Initialize;
      end;
      tvQBFunctions := gvQBFunctions;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvQBFunctions;
end;


function AggrFunctions: TrmAggrFunctions;
begin
  if not Assigned(tvAggrFunctions) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvAggrFunctions) then
      begin
        gvAggrFunctions := TrmAggrFunctions.Create(nil, TrmAggrFunction);
        try
          gvAggrFunctions.Initialize;
        except
          FreeAndNil(gvAggrFunctions);
          raise;
        end;
      end;

      tvAggrFunctions := gvAggrFunctions;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvAggrFunctions;
end;


function QBTemplates: TrwQBTemplates;
begin
  if not Assigned(tvQBTemplates) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvQBTemplates) then
      begin
        gvQBTemplates := TrwQBTemplates.Create(nil, TrwQBTemplate);
        try
          gvQBTemplates.Initialize;
        except
          FreeAndNil(gvQBTemplates);
          raise;
        end;
      end;

      tvQBTemplates := gvQBTemplates;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvQBTemplates;
end;


function IsFunctLibInitialized: Boolean;
begin
  Result := Assigned(gvSystemLibFunctions);
end;


function IsCompLibInitialized: Boolean;
begin
  Result := Assigned(gvSystemLibComponents);
end;


function SystemLibFunctions(const AExternalData: IEvDualStream = nil): TrwLibFunctions;
begin
  if IsFunctLibInitialized and Assigned(AExternalData) then
  begin
    FreeAndNil(gvSystemLibFunctions);
    tvSystemLibFunctions := nil;
  end;

  if not Assigned(tvSystemLibFunctions) then
  begin
    GlobalNameSpace.BeginWrite;
    try
      if not Assigned(gvSystemLibFunctions) then
      begin
        RWEngineExt.AppAdapter.StartEngineProgressBar('Initialization of RW Functions Library...');
        try
          gvSystemLibFunctions := TrwLibFunctions.Create(nil, TrwLibFunction);
          try
            if Assigned(AExternalData) then
            begin
              AExternalData.Position := 0;
              gvSystemLibFunctions.LoadFromStream(AExternalData.RealStream)
            end
            else
              gvSystemLibFunctions.Initialize;
          except
            FreeAndNil(gvSystemLibFunctions);
            raise;
          end;
        finally
          RWEngineExt.AppAdapter.EndEngineProgressBar;
        end;
      end;
      tvSystemLibFunctions := gvSystemLibFunctions;
    finally
      GlobalNameSpace.EndWrite;
    end;
  end;

  Result := tvSystemLibFunctions;
end;

procedure PrepareQueries(AOwner: TrwComponent);
var
  i: Integer;
  Q: IrmQuery;
begin
  for i := 0 to AOwner.ComponentCount - 1 do
  begin
    if not (AOwner.Components[i] is TrwComponent) then
      Continue;

    if Supports(AOwner.Components[i], IrmQuery) then
      Q := AOwner.Components[i] as IrmQuery
    else if Supports(AOwner.Components[i], IrmQueryDrivenObject) then
      Q := (AOwner.Components[i] as IrmQueryDrivenObject).GetQuery
    else
      Q := nil;  

    if Assigned(Q) then
      try
        Q.Prepare;
      except
      end;

    PrepareQueries(TrwComponent(AOwner.Components[i]));

{    else if AOwner.Components[i] is TrwCustomReport then
      PrepareQueries(TrwCustomReport(AOwner.Components[i]).ReportForm);

    if not (AOwner.Components[i] is TrwCustomReport) and TrwComponent(AOwner.Components[i]).IsLibComponent then
      PrepareQueries(TrwComponent(AOwner.Components[i]));}
  end;
end;


function  NormalizeFileName(const AFileName: String): String;
var
  i: Integer;
  c: Char;
  fPath: String;
begin
  Result := ExtractFileName(AFileName);
  fPath := ExtractFilePath(AFileName);

  for i := 1 to Length(Result) do
  begin
    c := AnsiUpperCase(Result[i])[1];
    if not((c >= '0') and (c <= '9') or
           (c >= 'A') and (c <= 'Z') or
           (Pos(c, ' .,;~_[]{}!@#$%^()-+=`/\') > 0)) then
      Result[i] := '_';
  end;

  Result := NormalDir(fPath) + Result;
end;


function CompareObjects(AObj1, AObj2: TPersistent): Boolean;
var
  MS1, MS2: TMemoryStream;

  procedure FillStream(MS: TMemoryStream; AObj: TPersistent);
  var
    Writer: TrwWriter;
  begin
    Writer := TrwWriter.Create(MS, 1024);
    try
      Writer.WriteProperties(AObj);
      MS.Position := 0;
    finally
      Writer.Free;
    end;
  end;

begin
  MS1 := TisMemoryStream.Create;
  MS2 := TisMemoryStream.Create;
  try
    FillStream(MS1, AObj1);
    FillStream(MS2, AObj2);

    Result := (MS1.Size = MS2.Size) and CompareMem(MS1.Memory, MS2.Memory, MS1.Size);

  finally
    MS1.Free;
    MS2.Free;
  end;
end;


function VariantsAreSame(const A, B: Variant): Boolean;
var
  LA, LB: TVarData;
begin
  LA := FindVarData(A)^;
  LB := FindVarData(B)^;
  if LA.VType = varEmpty then
    Result := LB.VType = varEmpty
  else if LA.VType = varNull then
    Result := LB.VType = varNull
  else if LB.VType in [varEmpty, varNull] then
    Result := False
  else if (LB.VType = varOleStr) or (LB.VType = varString) or
          (LA.VType = varOleStr) or (LA.VType = varString) then
    Result := VarAsType(A, varString) = VarAsType(B, varString)
  else
    Result := A = B;
end;


function GetSysTempDir: String;
var
  Buf: array[0..1023] of AnsiChar;
begin
  if GetTempPath(1023, Buf) = 0 then
    RaiseLastOSError
  else
    Result := PAnsiChar(@(Buf[0]));
end;


function GlobalFindFunction(const AFunctionName: String): TrwFunction;
begin
  Result := SystemLibFunctions.FindFunction(AFunctionName);

  if not Assigned(Result) then
    Result := AggrFunctions.FindFunction(AFunctionName);

  if not Assigned(Result) then
    Result := RWFunctions.FindFunction(AFunctionName);
end;


function CreateRWComponentByClass(const AClassName: String; const AInherit: Boolean = True): IrmDesignObject;
var
  O: TObject;
  Cl: IrwClass;
begin
  Result := nil;

  Cl := rwRTTIInfo.FindClass(AClassName);

  if Assigned(Cl) then
    O := Cl.CreateObjectInstance(AInherit)
  else
    O := nil;

  if not (O is TrwComponent) then
    raise ERWException.CreateFmt(ResErrWrongClass, [AClassName]);

  Result := TrwComponent(O);
end;

function IsRMReport(AData: TStream): Boolean;
const
  cReportDNA    = 'TPF0';
var
  s: String;
  p: Integer;
  n: Byte;
begin
  Result := False;
  p := AData.Position;
  try
    AData.Position := 0;
    s := StringOfChar(' ', Length(cReportDNA));
    AData.Read(s[1], Length(cReportDNA));
    if AnsiSameStr(s, cReportDNA) then
    begin
      AData.Read(n, 1);
      if n = 241 then
        AData.Read(n, 1);
      s := StringOfChar(' ', n);
      AData.Read(s[1], n);
      Result := SameText(s, 'TrmReport') or SameText(s, 'TrmSecureWrapper');
    end;

  finally
    AData.Position := p;
  end;
end;

function FindQBQueryProp(const AComponent: IrmDesignObject): String;
var
  PropList: TrwStrList;
begin
  PropList := AComponent.PropertiesByTypeName('TrwQBQuery');
  if Length(PropList) > 0 then
    Result := PropList[0]
  else
    Result := '';
end;


function FindDataSetProp(const AComponent: IrmDesignObject): String;
var
  PropList: TrwStrList;
begin
  PropList := AComponent.PropertiesByTypeName('TrwDataSet');
  if Length(PropList) > 0 then
    Result := PropList[0]
  else
    Result := '';
end;


function MakeDomainList(const AValues: array of Variant; const ADescriptions: array of String): TrwDomainList;
begin
  SetLength(Result, 0);
end;

function MakeEmptyDomainList: TrwDomainList;
begin
  Result := MakeDomainList([], []);
end;


function GlobalLocateRWFunction(const AFunctionName: String): TrwFunction;
begin
  Result := SystemLibFunctions.FindFunction(AFunctionName);
  if not Assigned(Result) then
    Result := RWFunctions.FindFunction(AFunctionName);
end;


function ClassObjectType(const AClassName: String): TrwDsgnObjectType;
var
  Cl: TClass;
begin
  Cl := GetClass(AClassName);

  if Cl.InheritsFrom(GetClass('TrmPrintControl')) then
    Result := rwDOTPrintForm

  else if Cl.InheritsFrom(GetClass('TrwFormControl')) then
    Result := rwDOTInputForm

  else if Cl.InheritsFrom(GetClass('TrmASCIIItem')) then
    Result := rwDOTASCIIForm

  else if Cl.InheritsFrom(GetClass('TrmXMLItem')) then
    Result := rwDOTXMLForm

  else if Cl.InheritsFrom(GetClass('TrwNoVisualComponent')) then
    Result := rwDOTNotVisual

  else
    Result := rwDOTInternal;
end;


function GetUniqueFileName(const ADir: String): string;
var
  S: PAnsiChar;
begin
  GetMem(S, 512);
  try
    GetTempFileName(PAnsiChar(NormalDir(ADir)), PAnsiChar('RW'), 0, S);
    Result := S;
  finally
    FreeMem(S);
  end;
end;


function GetAbsoluteAddress(const AAddress: TrwVMAddress): Integer;
begin
  Result := AAddress.Offset;

  case AAddress.Segment of
    rcsLibFunctions:   Inc(Result, cVMLibraryFunctionsOffset);
    rcsLibComponents:  Inc(Result, cVMLibraryComponentsOffset);
  end;
end;


function GetEntryPoint(AbsoluteAddress: Integer): TrwVMAddress;
begin
  if AbsoluteAddress >= cVMDsgnWatchListOffset then
  begin
    Result.Offset := AbsoluteAddress - cVMDsgnWatchListOffset;
    Result.Segment := rcsWatchList;
  end

  else if AbsoluteAddress >= cVMLibraryComponentsOffset then
  begin
    Result.Offset := AbsoluteAddress - cVMLibraryComponentsOffset;
    Result.Segment := rcsLibComponents;
  end

  else if AbsoluteAddress >= cVMLibraryFunctionsOffset then
  begin
    Result.Offset := AbsoluteAddress - cVMLibraryFunctionsOffset;
    Result.Segment := rcsLibFunctions;
  end

  else if AbsoluteAddress >= cVMReportOffset then
  begin
    Result.Offset := AbsoluteAddress - cVMReportOffset;
    Result.Segment := rcsReport;
  end

  else
  begin
    Result.Offset := -1;
    Result.Segment := rcsNone;
  end;
end;

function VMAddressToString(const AAddress: TrwVMAddress): String;
var
  ModuleName: String;
begin
  case AAddress.Segment of
    rcsReport:         ModuleName := 'Report';
    rcsLibFunctions:   ModuleName := 'Library of Functions';
    rcsLibComponents:  ModuleName := 'Library of Components';
  else
    ModuleName := 'Unknown';
  end;

  Result := 'Module: <' + ModuleName + '>   @' + FormatFloat('0000000000', AAddress.Offset);
end;

function  VMAddrListToVariant(const AddrList: TrwVMAddressList): Variant;
var
  i: Integer;
begin
  Result := VarArrayCreate([Low(AddrList), High(AddrList)], varOleStr);

  for i := Low(AddrList) to High(AddrList) do
    Result[i] := VMAddressToVariant(AddrList[i]);
end;

function  VariantToVMAddrList(const AddrList: Variant): TrwVMAddressList;
var
  i: Integer;
begin
  SetLength(Result, VarArrayHighBound(AddrList, 1) - VarArrayLowBound(AddrList, 1) + 1);

  for i := Low(Result) to High(Result) do
    Result[i] := VariantToVMAddress(AddrList[i]);
end;


function  VMAddressToVariant(const VMAddress: TrwVMAddress): Variant;
begin
  Result := IntToStr(Ord(VMAddress.Segment)) + ':' + IntToStr(VMAddress.Offset);
end;


function VariantToVMAddress(const VMAddress: Variant): TrwVMAddress;
var
  h: String;
begin
  with Result do
  begin
    Segment := rcsNone;
    Offset := 0;
  end;

  if (VarType(VMAddress) = varOleStr) or (VarType(VMAddress) = varString) then
  begin
    h := VMAddress;
    if h <> '' then
    begin
      Result.Segment := TrwVMCodeSegment(StrToInt(GetNextStrValue(h, ':')));
      Result.Offset := StrToInt(h);
    end;
  end;
end;


procedure CheckInheritedAndShowError(AObj: TrwComponent);
var
 VO: TrwComponent;
begin
  if Assigned(AObj) and AObj.InheritedComponent then
  begin
    VO := AObj.VirtualOwner;
    if Assigned(VO) and not (VO.IsVirtualOwner or (VO = VO.VirtualOwner)) then
      raise ErwException.Create(ResErrInheritedChange);
  end;
end;


procedure PutToClipboard(ADataType: WORD; const AData: String);
var
  lhData: THandle;
  lpDataPtr: Pointer;
begin
  lhData := GlobalAlloc(GMEM_MOVEABLE, Length(AData));

  try
    lpDataPtr := GlobalLock(lhData);

    try
      Move(PString(AData)^, lpDataPtr^, Length(AData));
      ClipBoard.Open;
      ClipBoard.SetAsHandle(ADataType, lhData);
      ClipBoard.AsText := IntToStr(Length(AData));
      ClipBoard.Close;
    finally
      GlobalUnlock(lhData);
    end;

  except
    GlobalFree(lhData);
    raise;
  end;
end;


function  GetFromClipboard(ADataType: WORD): String;
var
  lhData: THandle;
  lpDataPtr: Pointer;
  llSize: LongInt;
begin
  if not Clipboard.HasFormat(ADataType) then
    Exit;

  Clipboard.Open;
  lhData := Clipboard.GetAsHandle(ADataType);

  try
    if lhData = 0 then
      Exit;

    lpDataPtr := GlobalLock(lhData);
    llSize := StrToInt(ClipBoard.AsText);
    SetLength(Result, llSize);
    try
      Move(lpDataPtr^, PString(Result)^, llSize);
    finally
      GlobalUnlock(lhData);
      Clipboard.Close;
    end;
  except
    GlobalFree(lhData);
    raise;
  end;
end;


function VariantToStream(const AValue: Variant; const FStream: TStream = nil): TStream;

  procedure VarToStream(const AValue: Variant; const FStream: TStream; const FUseChunksForWriting: Boolean);
  var
    I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
    ArrayData: Pointer;

    procedure WriteData(const Buffer; const Count: Longint);
    begin
      if (FStream.Position + Count >= FStream.Size) and FUseChunksForWriting then
        FStream.Size := FStream.Size + 1024;
      FStream.WriteBuffer(Buffer, Count);
    end;

    procedure WriteInterfacedData(const AInt: IInterface);
    var
      S: IEvDualStream;
      n: Integer;
    begin
      S := AInt as IEvDualStream;
      if Assigned(S) then
      begin
        S.Position := 0;
        n := S.Size;
        FStream.WriteBuffer(n, SizeOf(n));
        FStream.CopyFrom(S.RealStream, n);
      end
      else
      begin
        n := -1;
        FStream.WriteBuffer(n, SizeOf(n));
      end;
    end;

  begin
    i := VarType(AValue);
    WriteData(i, SizeOf(i));
    if VarIsArray(AValue) then
    begin
      DimCount:= VarArrayDimCount(AValue);
      WriteData(DimCount, SizeOf(DimCount));
      ElemCount:= 1;
      for I:= 0 to DimCount - 1 do
      begin
        DimElemCount:= VarArrayHighBound(AValue, I + 1) - VarArrayLowBound(AValue, I + 1) + 1;
        ElemCount:= ElemCount * DimElemCount;
        WriteData(DimElemCount, SizeOf(DimElemCount));
      end;
      try
        ArrayData:= VarArrayLock(AValue);
        for I:= 0 to ElemCount - 1 do
        begin
          case VarType(AValue) and varTypeMask of
            varShortInt, varByte:
            begin
              ElemSize:= SizeOf(Byte);
              WriteData(PByte(ArrayData)^, ElemSize);
            end;
            varSmallint, varWord:
            begin
              ElemSize:= SizeOf(SmallInt);
              WriteData(PSmallInt(ArrayData)^, ElemSize);
            end;
            varInteger, varLongWord:
            begin
              ElemSize:= SizeOf(Integer);
              WriteData(PInteger(ArrayData)^, ElemSize);
            end;
            varSingle:
            begin
              ElemSize:= SizeOf(Single);
              WriteData(PSingle(ArrayData)^, ElemSize);
            end;
            varDouble:
            begin
              ElemSize:= SizeOf(Double);
              WriteData(PDouble(ArrayData)^, ElemSize);
            end;
            varCurrency:
            begin
              ElemSize:= SizeOf(Currency);
              WriteData(PCurrency(ArrayData)^, ElemSize);
            end;
            varDate:
            begin
              ElemSize:= SizeOf(TDateTime);
              WriteData(PDateTime(ArrayData)^, ElemSize);
            end;
            varOleStr:
              begin
                ElemSize:= SizeOf(PWideChar);
                Len := Length(PWideString(ArrayData)^);
                WriteData(Len, SizeOf(Len));
                WriteData(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
              end;
{           varString:  This array item type is not supported by Delphi!
              begin
                ElemSize:= SizeOf(PChar);
                Len := Length(PChar(ArrayData));
                WriteData(StrLen, SizeOf(Len));
                WriteData(String(PChar(ArrayData))[1], Len * SizeOf(Char));
              end;
}
            varBoolean:
            begin
              ElemSize:= SizeOf(WordBool);
              WriteData(PWordBool(ArrayData)^, ElemSize);
            end;
            varVariant:
            begin
              ElemSize:= SizeOf(TVarData);
              VarToStream(Variant(PVarData(ArrayData)^), FStream, FUseChunksForWriting);
            end;
          else
            raise EisException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
            ElemSize := 0;
          end;
          ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
        end;
      finally
        VarArrayUnlock(AValue);
      end;
    end
    else
    begin
      case VarType(AValue) of
        varEmpty, varNull:;
        varShortInt, varByte:
          WriteData(TVarData(AValue).VByte, SizeOf(TVarData(AValue).VByte));
        varSmallint, varWord:
          WriteData(TVarData(AValue).VSmallInt, SizeOf(TVarData(AValue).VSmallInt));
        varInteger, varLongWord:
          WriteData(TVarData(AValue).VInteger, SizeOf(TVarData(AValue).VInteger));
        varInt64:
          WriteData(TVarData(AValue).VInt64, SizeOf(TVarData(AValue).VInt64));
        varSingle:
          WriteData(TVarData(AValue).VSingle, SizeOf(TVarData(AValue).VSingle));
        varDouble:
          WriteData(TVarData(AValue).VDouble, SizeOf(TVarData(AValue).VDouble));
        varCurrency:
          WriteData(TVarData(AValue).VCurrency, SizeOf(TVarData(AValue).VCurrency));
        varDate:
          WriteData(TVarData(AValue).VDate, SizeOf(TVarData(AValue).VDate));
        varOleStr:
          begin
            i := Length(TVarData(AValue).VOleStr);
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VOleStr^, i*SizeOf(WideChar));
          end;
        varString:
          begin
            i := Length(PChar(TVarData(AValue).VString));
            WriteData(i, SizeOf(i));
            WriteData(TVarData(AValue).VString^, i*SizeOf(Char));
          end;
        varBoolean:
          WriteData(TVarData(AValue).VBoolean, SizeOf(TVarData(AValue).VBoolean));
        varUnknown:
          WriteInterfacedData(IInterface(TVarData(AValue).VUnknown));
      else
        raise EisException.Create('Unsupported type: ' + IntToStr(VarType(AValue)));
      end;
    end;
  end;
var
  ver: Byte;
begin
  if Assigned(FStream) then
    Result := FStream
  else
    Result := TisMemoryStream.Create;
  ver := $FF;
  Result.WriteBuffer(ver, SizeOf(ver));
  VarToStream(AValue, Result, VarIsArray(AValue));
  Result.Size := Result.Position;
end;


procedure StreamToVariant(const FStream: TStream; var Result: Variant); overload;
var
  VarType: Integer;
  I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
  StrLen: Integer;
  ArrayData: Pointer;
  ArrayDims: array of Integer;
  IntStream: IEvDualStream;
begin
  FStream.ReadBuffer(VarType, SizeOf(Integer));
  if (VarType and varArray) <> 0 then
  begin
    FStream.ReadBuffer(DimCount, SizeOf(DimCount));
    SetLength(ArrayDims, DimCount * 2);
    ElemCount:= 1;
    for I:= 0 to DimCount - 1 do
    begin
      FStream.ReadBuffer(DimElemCount , SizeOf(DimElemCount));
      ElemCount:= ElemCount * DimElemCount;
      ArrayDims[I * 2]:= 0;
      ArrayDims[I * 2 + 1]:= DimElemCount - 1;
    end;
    Result:= VarArrayCreate(ArrayDims, VarType and varTypeMask);
    try
      ArrayData:= VarArrayLock(Result);
      for I:= 0 to ElemCount - 1 do
      begin
        case VarType and varTypeMask of
          varShortInt, varByte:
          begin
            ElemSize:= SizeOf(Byte);
            FStream.ReadBuffer(PByte(ArrayData)^, ElemSize);
          end;
          varSmallint, varWord:
          begin
            ElemSize:= SizeOf(SmallInt);
            FStream.ReadBuffer(PSmallInt(ArrayData)^, ElemSize);
          end;
          varInteger, varLongWord:
          begin
            ElemSize:= SizeOf(Integer);
            FStream.ReadBuffer(PInteger(ArrayData)^, ElemSize);
          end;
          varSingle:
          begin
            ElemSize:= SizeOf(Single);
            FStream.ReadBuffer(PSingle(ArrayData)^, ElemSize);
          end;
          varDouble:
          begin
            ElemSize:= SizeOf(Double);
            FStream.ReadBuffer(PDouble(ArrayData)^, ElemSize);
          end;
          varCurrency:
          begin
            ElemSize:= SizeOf(Currency);
            FStream.ReadBuffer(Currency(ArrayData^), ElemSize);
          end;
          varDate:
          begin
            ElemSize:= SizeOf(TDateTime);
            FStream.ReadBuffer(TDateTime(ArrayData^), ElemSize);
          end;
          varOleStr:
          begin
            ElemSize:= SizeOf(PWideChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            PWideString(ArrayData)^ := WideString(StringOfChar(' ', Len));
            FStream.ReadBuffer(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
          end;
{         varString:    This array item type is not supported by Delphi!
          begin
            ElemSize:= SizeOf(PChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            string(ArrayData^) := StringOfChar(' ', Len);
            FStream.Read(PChar(ArrayData)^, Len * SizeOf(Char));
          end;
}
          varBoolean:
          begin
            ElemSize:= SizeOf(WordBool);
            FStream.ReadBuffer(WordBool(ArrayData^), ElemSize);
          end;
          varVariant:
          begin
            ElemSize:= SizeOf(TVarData);
            Variant(PVarData(ArrayData)^) := Unassigned;
            StreamToVariant(FStream, Variant(PVarData(ArrayData)^));
          end;
        else
          raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
          ElemSize := 0;
        end;
        ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
      end;
    finally
      VarArrayUnlock(Result);
    end;
  end
  else
  begin
    case VarType and varTypeMask of
    varEmpty:
      Result := Unassigned;
    varNull:
      Result := null;
    varSmallint:
      FStream.ReadBuffer(TVarData(Result).VSmallint , SizeOf(TVarData(Result).VSmallint));
    varShortint:
      FStream.ReadBuffer(TVarData(Result).VShortint , SizeOf(TVarData(Result).VShortint));
    varInteger:
      FStream.ReadBuffer(TVarData(Result).VInteger , SizeOf(TVarData(Result).VInteger));
    varInt64:
      FStream.ReadBuffer(TVarData(Result).VInt64 , SizeOf(TVarData(Result).VInt64));
    varSingle:
      FStream.ReadBuffer(TVarData(Result).VSingle, SizeOf(TVarData(Result).VSingle));
    varDouble:
      FStream.ReadBuffer(TVarData(Result).VDouble, SizeOf(TVarData(Result).VDouble));
    varCurrency:
      FStream.ReadBuffer(TVarData(Result).VCurrency, SizeOf(TVarData(Result).VCurrency));
    varDate:
      FStream.ReadBuffer(TVarData(Result).VDate, SizeOf(TVarData(Result).VDate));
    varBoolean:
      FStream.ReadBuffer(TVarData(Result).VBoolean, SizeOf(TVarData(Result).VBoolean));
    varByte:
      FStream.ReadBuffer(TVarData(Result).VByte, SizeOf(TVarData(Result).VByte));
    varWord:
      FStream.ReadBuffer(TVarData(Result).VWord, SizeOf(TVarData(Result).VWord));
    varLongWord:
      FStream.ReadBuffer(TVarData(Result).VLongWord, SizeOf(TVarData(Result).VLongWord));
    varOleStr:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := WideString(StringOfChar(' ', StrLen));
      FStream.Read(TVarData(Result).VOleStr^, StrLen*SizeOf(WideChar));
    end;
    varString:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := StringOfChar(' ', StrLen);
      FStream.Read(TVarData(Result).VString^, StrLen*SizeOf(Char));
    end;
    varUnknown:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      if StrLen = -1 then
        IntStream := nil
      else
      begin
        IntStream := TEvDualStreamHolder.Create(StrLen);
        if StrLen > 0 then
        begin
          IntStream.RealStream.CopyFrom(FStream, StrLen);
          IntStream.Position := 0;
        end;
      end;

      Result := IntStream;
      IntStream := nil;
    end;

    else
      raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
    end;
    TVarData(Result).VType := VarType;
  end;
end;

function StreamToVariant(const FStream: TStream): Variant; overload;
var
  ver: byte;
begin
  FStream.ReadBuffer(ver, SizeOf(ver));
  Assert(ver = $FF);
  StreamToVariant(FStream, Result);
end;

function  VarArrayToStream(const vData: Variant; const AStream: TStream = nil; const AOleStrToStr: Boolean = False): TStream;
begin
  Result := VariantToStream(vData, AStream);
end;

function  StreamToVarArray(const AStream: TStream): Variant;
var
  i: Integer;
  ver: byte;
begin
  i := AStream.Position;
  AStream.ReadBuffer(ver, SizeOf(ver));
  AStream.Position := i;
  if ver = $FF then
    Result := StreamToVariant(AStream)
  else
    with TevVariantToStreamHolder.Create do
    try
      Result := StreamToVarArray(AStream);
    finally
      Free;
    end;
end;


function TevVariantToStreamHolder.InternalTypeToVarType(AIntType: Byte): Integer;
var
  fArray: Boolean;
begin
  fArray := (AIntType and 128) <> 0;
  if fArray then
    AIntType := AIntType and 127;

  case AIntType of
    0:  Result := varEmpty;
    1:  Result := varNull;
    2:  Result := varSmallint;
    3:  Result := varInteger;
    4:  Result := varSingle;
    5:  Result := varDouble;
    6:  Result := varCurrency;
    7:  Result := varDate;
    8:  Result := varOleStr;
    9:  Result := varBoolean;
    10: Result := varVariant;
    11: Result := varByte;
    12: Result := varString;
    13: Result := varShortint;
  else
    raise EisException.Create('Variant type is not applicable');
  end;

  if fArray then
    Result := Result or varArray;
end;

function TevVariantToStreamHolder.ReadArray: Variant;
var
  i, n, m: Integer;
  t: Byte;
  P: Pointer;
begin
  FStream.Read(t, SizeOf(t));
  if (t and 128) = 0 then
    raise EisException.Create('Array type expected');
  FStream.Read(n, SizeOf(n));
  FStream.Read(m, SizeOf(m));
  t := t and 127;
  Result := VarArrayCreate([n, m], InternalTypeToVarType(t));

  if t = 11 then
  begin
    P := VarArrayLock(Result);
    try
      FStream.ReadBuffer(P^, m - n + 1);
    finally
      VarArrayUnLock(Result);
    end;
  end

  else
  begin
    if t = 10 then
      t := 0;
    for i := n to m do
      Result[i] := ReadElement(t);
  end;
end;

function TevVariantToStreamHolder.ReadElement(AType: Byte): Variant;
var
  vt: Integer;
  i: Integer;
  t: Byte;
begin
  i := FStream.Position;
  if AType = 0 then
    FStream.Read(t, SizeOf(t))
  else
    t := AType;
  vt := InternalTypeToVarType(t);
  Result := Unassigned;
  if (vt and varArray) = 0 then
    Result := VarAsType(Result, vt)
  else
  begin
    FStream.Position := i;
    Result := ReadArray;
    Exit;
  end;

  case vt of
    varEmpty:    Result := Unassigned;

    varNull:     VarAsType(Result, varNull);

    varSmallint: FStream.Read(TVarData(Result).VSmallint , SizeOf(TVarData(Result).VSmallint));

    varShortint: FStream.Read(TVarData(Result).VShortint , SizeOf(TVarData(Result).VShortint));

    varInteger:  FStream.Read(TVarData(Result).VInteger , SizeOf(TVarData(Result).VInteger));

    varInt64:    FStream.Read(TVarData(Result).VInt64 , SizeOf(TVarData(Result).VInt64));

    varSingle:   FStream.Read(TVarData(Result).VSingle, SizeOf(TVarData(Result).VSingle));

    varDouble:   FStream.Read(TVarData(Result).VDouble, SizeOf(TVarData(Result).VDouble));

    varCurrency: FStream.Read(TVarData(Result).VCurrency, SizeOf(TVarData(Result).VCurrency));

    varDate:     FStream.Read(TVarData(Result).VDate, SizeOf(TVarData(Result).VDate));

    varBoolean:  FStream.Read(TVarData(Result).VBoolean, SizeOf(TVarData(Result).VBoolean));

    varByte:     FStream.Read(TVarData(Result).VByte, SizeOf(TVarData(Result).VByte));

    varOleStr:   begin
                   FStream.Read(i, SizeOf(i));
                   Result := VarAsType(StringOfChar(' ', i), varOleStr);
                   FStream.Read(TVarData(Result).VOleStr^, i*SizeOf(WideChar));
                 end;

    varString:   begin
                   FStream.Read(i, SizeOf(i));
                   Result := VarAsType(StringOfChar(' ', i), varString);
                   FStream.Read(TVarData(Result).VString^, i*SizeOf(Char));
                 end;
  else
    raise EisException.Create('Variant type is not applicable');
  end;
end;

function TevVariantToStreamHolder.StreamToVarArray(const AStream: TStream): Variant;
begin
  FStream := AStream;
  try
    Result := ReadArray;
  finally
    FStream := nil;
  end;
end;


initialization
  InitThreadVars_rwShared;
  RWClassList := TStringList.Create;
  RegRWClasses;

finalization
  FreeDataDictionary;
  FreeAndNil(gvrwDictionary);
  FreeAndNil(gvSystemLibComponents);
  FreeAndNil(gvSystemLibFunctions);
  FreeAndNil(gvDefPropComponents);
  FreeAndNil(gvPublishedMethods);
  FreeAndNil(gvQBFunctions);
  FreeAndNil(gvQBTEmplates);
  FreeAndNil(RWClassList);

end.

