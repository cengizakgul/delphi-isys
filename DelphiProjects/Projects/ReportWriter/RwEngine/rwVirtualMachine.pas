unit rwVirtualMachine;

interface

uses SysUtils, Classes, TypInfo, Forms, Windows, rwTypes, DB, Variants, rwEngineTypes,
     Math, rmTypes, rwBasicUtils, Messages, SyncObjs;

const
  // this offsets help to distinguish Report's P-code entry points from Library's ones
  cVMReportOffset             = 0;
  cVMLibraryFunctionsOffset   = 10000000;
  cVMLibraryComponentsOffset  = 20000000;
  cVMDsgnWatchListOffset      = 30000000;

  rwVMHeapSize =  8192;
  rwVMExternalFunctBase = 1000;
  rwAccRoundingCorrection: Extended = 1 / MaxLongInt;

type
  TrwVMOperation = (rmoNope,
                      rmoMOVAconst, rmoMOVAlocVar, rmoMOVAglobVar,
                      rmoMOVBconst, rmoMOVBlocVar, rmoMOVBglobVar,
                      rmoMOVlocVarA, rmoMOVglobVarA,
                      rmoMOVlocVarB, rmoMOVglobVarB,
                      rmoMOVAB, rmoMOVBA,
                      rmoMOVArefB, rmoMOVrefBA,
                      rmoReverseSignA,
                      rmoAdd, rmoSubstract, rmoDivide, rmoDiv, rmoMod, rmoMultiply, rmoAexpB,
                      rmoIncA, rmoDecA,
                      rmoIncB, rmoDecB,
                      rmoAandB, rmoAorB, rmoNotA,
                      rmoPushA, rmoPushB, rmoPushFlag, rmoPushConst,
                      rmoPopA, rmoPopB, rmoPopFlag,
                      rmoSwapAB,
                      rmoCmpAlB,
                      rmoCmpAeB,
                      rmoCmpAneB,
                      rmoCmpAgB,
                      rmoCmpAleB,
                      rmoCmpAgeB,
                      rmoCall, rmoCallEvent, rmoCallLibFunc, rmoCallLibCompEvent, rmoRet,
                      rmoNewVar,
                      rmoCallEmbProc,
                      rmoCallEmbFunc,
                      rmoJump,
                      rmoJumpIfTrue,
                      rmoJumpIfFalse);



  TrwVMEmbeddedProc = (repUnknown, repSetObjectProp, repSetArrayElement, repSetStrElement, repSetArrLength,
                       repSetTime, repDestroyObject, repShowError, repAbort);

  TrwVMEmbeddedFunc = (refUnknown, refFindObjectByIndex, refObjectPropValue,
                       refObjectMethod, refConvertToType, refArrayElement,
                       refAddrLocVar, refAddrGlobVar, refStrElement, refLength, refArrLength, refFormat,
                       refSubstFldValue, refSubstSubStr, refPosSubStr,
                       refCopySubStr, refDelSubStr, refRound,
                       refTrunc, refToday, refTime, refChr, refOrd, refCreateObject,
                       refIndexOfArrayElement, refLowerCase, refUpperCase, refTrim, refTrimLeft, refTrimRight,
                       refTypeOf, refSign, refAbs, refCreateConstArray, refCustomCall,
                       refDesignerOpened, refNow, refRandomRange, refTimeZoneOffset);



  ErwVMError = class(ErwException)
  private
    FCS: TrwVMCodeSegment;
    FIP: Integer;

  public
    property CS: TrwVMCodeSegment read FCS;
    property IP: Integer read FIP;
    constructor CreateVMError(ACS: TrwVMCodeSegment; AAddr: Integer; AMessage: string);
  end;


  ErwVMAbort = class(ErwVMError)
  private
    FReturnValue: Variant;
  public
    property ReturnValue: Variant read FReturnValue;
  end;


  TrwVMOnStep = procedure of object;
  TrwVMOnError = procedure (const AErrorMessage: String) of object;


  TrwVirtualMachine = class
  private
    FCode: PTrwCode;          //CS
    FReportCode: PTrwCode;    //Pure compiled p-code
    FStackPointer: Integer;   //SP
    FInstrPointer: Integer;   //IP
    FHeapPointer: Integer;
    FHeapTop: Integer;
    FHeapBottom: Integer;
    FCurHeapTop: Integer;
    FCodeMemBottom: Integer;
    FNextStep: Integer;
    FCodeSegment: TrwVMCodeSegment;
    FOffSetAddress: Integer;
    FHalt: Boolean;
    FMemory: TrwCode;
    FCallStack: TList;
    FUsageCounter: Integer;
    FOnStep: TrwVMOnStep;
    FOnError: TrwVMOnError;
    FInPause: Boolean;

    FFlag:  Boolean;
    FRegA:  Variant;
    FRegB:  Variant;
    FDisableDebCallbacks: Boolean;

    procedure DoOnError(const AErrorMessage: String);
    procedure DoOnStep;    

    procedure GoFurtherBy(const ADelta: Integer);
    procedure CreateNewVar(const AType: TrwVarTypeKind);
    procedure CheckHeapPointer(const APointer: Integer);
    procedure CheckStackPointer(const APointer: Integer);
    procedure ClearMemory(const ABeginAddr, AEndAddr: Integer);

    procedure NewVar;
    procedure MOVRconst(var AReg: Variant);
    procedure MOVRlocVar(var AReg: Variant);
    procedure MOVRglobVar(var AReg: Variant);
    procedure MOVlocVarR(var AReg: Variant);
    procedure MOVglobVarR(var AReg: Variant);
    procedure PushConst;
    procedure SwapAB;
    procedure CallEvent;
    procedure CallLibFunc;
    procedure CallLibCompEvent;
    procedure CallEmbProc;
    procedure CallEmbFunc;
    procedure Jump;
    procedure JumpIf(const Cond: Boolean);
    procedure CmpAB(const Oper: TrwVMOperation);
    procedure MathOper(const Oper: TrwVMOperation);
    procedure LogOper(const Oper: TrwVMOperation);

    procedure FindObjectByIndex;
    procedure ObjectPropValue;
    procedure ObjectMethod;
    procedure SetObjectProp;
    procedure SetArrayElement;
    procedure ConvertToType;
    procedure IntTypeConversion(var AValue: Variant; const AType: TrwVarTypeKind; const AConvNull: Boolean);
    procedure ArrayElement;
    procedure StrElement;
    procedure SetStrElement;
    procedure GetAddrLocVar;
    procedure GetAddrGlobVar;
    procedure SetArrLength;
    procedure GetLength;
    procedure GetArrLength;
    procedure FormatVal;
    procedure SubstFldValue;
    procedure SubstSubStr;
    procedure PosSubStr;
    procedure CopySubStr;
    procedure DelSubStr;
    procedure RoundFloat;
    procedure RandomRange;
    procedure TruncFloat;
    procedure GetTime;
    procedure SetTime;
    procedure ChrByCode;
    procedure CodeByChr;
    procedure CreateObject;
    procedure DestroyObject;
    procedure IndexOfArrayElement;
    procedure StrLowerCase;
    procedure StrUpperCase;
    procedure TrimStr(const AType: TrwVMEmbeddedFunc);
    procedure CustomCall;
    procedure ShowError;
    procedure AbortExecution;
    procedure TypeOf;
    procedure SignFloat;
    procedure AbsFloat;
    procedure CreateConstArray;
    procedure DesignerOpened;
    procedure TimeZoneOffset;

    procedure SkipPage;
    procedure PageNumber;
    procedure SetPrintPos;
    procedure GetPrintPos;
    procedure FileExists;
    procedure AppendASCIILine;
    procedure InsertASCIILine;
    procedure UpdateASCIILine;
    procedure DeleteASCIILine;
    procedure GetASCIILine;
    procedure ASCIILineCount;

    procedure DebuggerLoop;
    function  BeforeTraceInto: Boolean;
    procedure AfterTraceInto(const APrevState: Boolean);
    procedure MainLoop;
    procedure PumpMainFormMessages;    

    procedure InitVM(ACode: PTrwCode);
    procedure DeInitVM;

  public
    constructor Create;
    destructor  Destroy; override;
    procedure Run(const AEntryPoint: TrwVMAddress; const ACustomPCode: PTrwCode = nil);
    procedure CalcWatches;
    procedure ShowVMError(const AMessage: String; const AParam: array of const);
    procedure SetRMDebuggerCallback(AOnStep: TrwVMOnStep; AOnError: TrwVMOnError);

    procedure Push(const AValue: Variant);
    function  Pop: Variant;
    function  CurrentEntryPoint: TrwVMAddress;
    procedure GetCallStack(var ACallStack: TrwVMAddressList);

    property  Halt: Boolean read FHalt write FHalt;
    property  DisableDebCallbacks: Boolean read FDisableDebCallbacks write FDisableDebCallbacks;
    property  StackPointer: Integer read FStackPointer write FStackPointer;
    property  InstrPointer: Integer read FInstrPointer write FInstrPointer;
    property  HeapPointer:  Integer read FHeapPointer write FHeapPointer;
    property  CurrHeapTop:  Integer read FCurHeapTop;
    property  Flag:  Boolean read FFlag write FFlag;
    property  RegA:  Variant read FRegA write FRegA;
    property  RegB:  Variant read FRegB write FRegB;
    property  ReportCode: PTrwCode read FReportCode;
    property  HeapTop: Integer read FHeapTop;
    property  CodeSegment: TrwVMCodeSegment read FCodeSegment;
  end;



  procedure InitThreadVars_rwVirtualMachine;
  function  VM: TrwVirtualMachine;
  procedure CreateRwVM(ACode: PTrwCode);
  procedure DestroyRwVM;
  function  GetOffSet(const ACSType: TrwVMCodeSegment; const AReportCodeLen: Integer): Integer;

implementation

uses rwRendering, rwBasicClasses, rwMessages,  rwDebugInfo, rwUtils, rwLogQuery, rwEngine, rwRTTI;

resourcestring
  ResErrVMStackViolation =      'Stack violation';
  ResErrVMStackOverflow =       'Stack overflow';
  ResErrVMOutOfMemory =         'Out of memory';
  ResErrVMAccessViolation =     'Access violation at address %s';
  ResErrVMUnknownInstruction =  'Unknown instruction';
  ResErrVMUnknownProp =         'Unknown property "%s" for class "%s"';
  ResErrVMRunTimeError =        'Runtime error at address %s';
  ResErrVMAbort =               'Process has been terminated by user';
  ResErrVMOperNotApplicable =   'Operation is not applicable';


threadvar
   FVM:  TrwVirtualMachine;


procedure InitThreadVars_rwVirtualMachine;
begin
  FVM := nil;
end;


function VM: TrwVirtualMachine;
begin
  Result := FVM;
end;

procedure CreateRwVM(ACode: PTrwCode);
begin
  if not Assigned(FVM) then
    FVM := TrwVirtualMachine.Create;
  Inc(FVM.FUsageCounter);

  if Assigned(ACode) then
    FVM.InitVM(ACode);
end;


procedure DestroyRwVM;
begin
  if Assigned(FVM) then
  begin
    FVM.DeInitVM;
    Dec(FVM.FUsageCounter);

    if FVM.FUsageCounter = 0 then
      FreeAndNil(FVM);
  end;
end;


function GetOffSet(const ACSType: TrwVMCodeSegment; const AReportCodeLen: Integer): Integer;
begin
  case ACSType of
    rcsLibFunctions:  Result := AReportCodeLen;
    rcsLibComponents: Result := AReportCodeLen + Length(SystemLibFunctions.CompiledCode);
  else
    Result := 0;
  end;
end;



{ TrwVirtualMachine }


//Initialization for first running

constructor TrwVirtualMachine.Create;
begin
  FCallStack := TList.Create;
  FUsageCounter := 0;
  InitVM(@FMemory);
end;

procedure TrwVirtualMachine.InitVM(ACode: PTrwCode);
begin
  Halt := False;

  SetLength(FMemory, 0);

  FReportCode := ACode;
  FCode := ACode;

  FInstrPointer := 0;
  FCodeSegment := rcsNone;
  FHeapTop := High(FReportCode^) + 1;
  FCurHeapTop := FHeapTop;
  FHeapPointer := FHeapTop;
  FCodeMemBottom :=  High(FReportCode^);

  SetLength(FReportCode^, Length(FReportCode^) + rwVMHeapSize);

  FStackPointer := High(FReportCode^) + 1;
  FHeapBottom := High(FReportCode^);

  FCallStack.Clear;
  FCallStack.Capacity := 255;
  
  FInPause := False;
end;


procedure TrwVirtualMachine.DeInitVM;
begin
  SetLength(FReportCode^, Length(FReportCode^) - rwVMHeapSize);
  InitVM(@FMemory);
end;


procedure TrwVirtualMachine.ShowVMError(const AMessage: String; const AParam: array of const);
begin
  raise ErwVMError.CreateVMError(CodeSegment, InstrPointer, Format(AMessage, AParam));
end;


procedure TrwVirtualMachine.CheckHeapPointer(const APointer: Integer);
begin
  if (APointer < FHeapTop) or (APointer >= FStackPointer) then
    ShowVMError(ResErrVMAccessViolation, [FormatFloat('0000000000', APointer)]);
end;


procedure TrwVirtualMachine.CheckStackPointer(const APointer: Integer);
begin
  if (APointer <= FHeapPointer) then
    ShowVMError(ResErrVMStackOverflow, [])
  else if APointer > FHeapBottom then
    ShowVMError(ResErrVMStackViolation, []);
end;


//Change IP register
procedure TrwVirtualMachine.GoFurtherBy(const ADelta: Integer);
begin
  Inc(FInstrPointer, ADelta);
  if (FInstrPointer > FCodeMemBottom) or (FInstrPointer < 0) or ((FCodeSegment = rcsReport) and (FInstrPointer >= FHeapTop)) then
    ShowVMError(ResErrVMAccessViolation, [FormatFloat('0000000000', FInstrPointer + FOffSetAddress)]);
end;


//Release allocated memory
procedure TrwVirtualMachine.ClearMemory(const ABeginAddr, AEndAddr: Integer);
var
  i: Integer;
begin
  for i := ABeginAddr to AEndAddr do
    VarClear(FReportCode^[i]);
end;


//Create new variable
procedure TrwVirtualMachine.NewVar;
begin
  GoFurtherBy(1);
  CreateNewVar(TrwVarTypeKind(FCode^[FInstrPointer]));
end;


// MOV Reg, Const
procedure TrwVirtualMachine.MOVRconst(var AReg: Variant);
begin
  GoFurtherBy(1);
  AReg := FCode^[FInstrPointer];
end;


// MOV A, GlobVar
procedure TrwVirtualMachine.MOVRglobVar(var AReg: Variant);
var
  C: TrwComponent;
begin
  GoFurtherBy(1);
  C := TrwComponent(Pointer(Integer(Pop)));
  AReg := C.GlobalVarFunc.Variables[FCode^[FInstrPointer]].Value;
end;


// MOV A, LocVar
procedure TrwVirtualMachine.MOVRlocVar(var AReg: Variant);
var
  i: Integer;
begin
  GoFurtherBy(1);
  i := FCurHeapTop+FCode^[FInstrPointer];
  CheckHeapPointer(i);
  AReg := FReportCode^[i];
end;


// MOV LocVar, A
procedure TrwVirtualMachine.MOVlocVarR(var AReg: Variant);
var
  i: Integer;
begin
  GoFurtherBy(1);
  i := FCurHeapTop+FCode^[FInstrPointer];
  CheckHeapPointer(i);
  FReportCode^[i] := AReg;
end;


procedure TrwVirtualMachine.MOVglobVarR(var AReg: Variant);
var
  C: TrwComponent;
begin
  GoFurtherBy(1);
  C := TrwComponent(Pointer(Integer(Pop)));
  C.GlobalVarFunc.Variables[FCode^[FInstrPointer]].Value := AReg;
end;


procedure TrwVirtualMachine.PushConst;
begin
  GoFurtherBy(1);
  Push(FCode^[FInstrPointer]);
end;


//A <-> B
procedure TrwVirtualMachine.SwapAB;
var
  v: Variant;
begin
  v := FRegA;
  FRegA := FRegB;
  FRegB := v;
end;


//CMP A, B
procedure TrwVirtualMachine.CmpAB(const Oper: TrwVMOperation);
begin
  case Oper of
    rmoCmpAlB:  FFlag := FRegA < FRegB;

    rmoCmpAeB:  begin
                  if VarIsNull(FRegA) then
                    FFlag := VarIsNull(FRegB)
                  else if VarIsNull(FRegB) then
                    FFlag := VarIsNull(FRegA)
                  else
                    FFlag := FRegA = FRegB;
                end;

    rmoCmpAgB:  FFlag := FRegA > FRegB;

    rmoCmpAleB: FFlag := FRegA <= FRegB;

    rmoCmpAgeB: FFlag := FRegA >= FRegB;

    rmoCmpAneB: begin
                  if VarIsNull(FRegA) then
                    FFlag := not VarIsNull(FRegB)
                  else if VarIsNull(FRegB) then
                    FFlag :=  not VarIsNull(FRegA)
                  else
                    FFlag := FRegA <> FRegB;
                end;
  end;
end;


//Call event of component
procedure TrwVirtualMachine.CallEvent;
var
  ev: string;
  n, i: Integer;
  C: TrwComponent;
  lParams: array of variant;
  fl: Boolean;
begin
  n := Pop;  //count of params
  SetLength(lParams, n);
  for i := High(lParams) downto Low(lParams) do
    lParams[i] := Pop;
  C := TrwComponent(Integer(Pop));

  GoFurtherBy(1);
  ev := FCode^[FInstrPointer];


  fl := BeforeTraceInto;
  C.ExecuteEventsHandler(ev, lParams);
  AfterTraceInto(fl);
end;


//Call library function
procedure TrwVirtualMachine.CallLibFunc;
var
  fl: Boolean;
begin
  GoFurtherBy(1);

  fl := BeforeTraceInto;
  SystemLibFunctions.Execute(FCode^[FInstrPointer]);
  AfterTraceInto(fl);
end;


procedure TrwVirtualMachine.CallLibCompEvent;
var
 cl: String;
 fl: Boolean;
 Comp: IrwClass;
begin
  GoFurtherBy(1);
  cl := FCode^[FInstrPointer];
  GoFurtherBy(1);

  fl := BeforeTraceInto;

  Comp := rwRTTIInfo.FindClass(cl);
  TrwLibComponents(Comp.GetUserClass.RealObject.Collection).Execute(cl, FCode^[FInstrPointer]);

  AfterTraceInto(fl);
end;


//Embedded procedures
procedure TrwVirtualMachine.CallEmbProc;
begin
  GoFurtherBy(1);

  case TrwVMEmbeddedProc(FCode^[FInstrPointer]) of
    repSetObjectProp:     SetObjectProp;
    repSetArrayElement:   SetArrayElement;
    repSetStrElement:     SetStrElement;
    repSetArrLength:      SetArrLength;
    repSetTime:           SetTime;
    repDestroyObject:     DestroyObject;
    repShowError:         ShowError;
    repAbort:             AbortExecution;
  else
    ShowVMError(ResErrVMUnknownInstruction, []);
  end;
end;


procedure TrwVirtualMachine.CallEmbFunc;
begin
  GoFurtherBy(1);

  case TrwVMEmbeddedFunc(FCode^[FInstrPointer]) of
    refFindObjectByIndex: FindObjectByIndex;
    refObjectPropValue:   ObjectPropValue;
    refObjectMethod:      ObjectMethod;
    refConvertToType:     ConvertToType;
    refArrayElement:      ArrayElement;
    refAddrLocVar:        GetAddrLocVar;
    refAddrGlobVar:       GetAddrGlobVar;
    refStrElement:        StrElement;
    refLength:            GetLength;
    refArrLength:         GetArrLength;
    refFormat:            FormatVal;
    refSubstFldValue:     SubstFldValue;
    refSubstSubStr:       SubstSubStr;
    refPosSubStr:         PosSubStr;
    refCopySubStr:        CopySubStr;
    refDelSubStr:         DelSubStr;
    refRound:             RoundFloat;
    refRandomRange:       RandomRange;
    refTrunc:             TruncFloat;
    refToday:             FRegA := VarAsType(Trunc(RWEngineExt.AppAdapter.CurrentDateTime), varDate);
    refTime:              GetTime;
    refNow:               FRegA := VarAsType(RWEngineExt.AppAdapter.CurrentDateTime, varDate);
    refChr:               ChrByCode;
    refOrd:               CodeByChr;
    refCreateObject:      CreateObject;
    refIndexOfArrayElement: IndexOfArrayElement;
    refLowerCase:         StrLowerCase;
    refUpperCase:         StrUpperCase;
    refTypeOf:            TypeOf;
    refSign:              SignFloat;
    refAbs:               AbsFloat;
    refCreateConstArray:  CreateConstArray;
    refDesignerOpened:    DesignerOpened;

    refTrim,
    refTrimLeft,
    refTrimRight:         TrimStr(TrwVMEmbeddedFunc(FCode^[FInstrPointer]));

    refTimeZoneOffset:      TimeZoneOffset;

    refCustomCall:        CustomCall;

 else
    ShowVMError(ResErrVMUnknownInstruction, []);
  end;
end;


procedure TrwVirtualMachine.Jump;
begin
  GoFurtherBy(1);
  FNextStep := FCode^[FInstrPointer];
end;


procedure TrwVirtualMachine.JumpIf(const Cond: Boolean);
begin
  if (FFlag and Cond) or (not FFlag and not Cond) then
    Jump
  else
    FNextStep := 2;
end;

// A <- A {+|-|*|/} B
procedure TrwVirtualMachine.MathOper(const Oper: TrwVMOperation);
var
  b: Variant;
begin
  b := FRegB;
  if VarIsNull(FRegA) then
    if VarType(FRegB) = varString then
      FRegA := ''
    else
      FRegA := 0;

  if VarIsNull(b) then
    if VarType(FRegA) = varString then
      b := ''
    else
      b := 0;

  case Oper of
      rmoAdd:       FRegA := FRegA + b;
      rmoSubstract: FRegA := FRegA - b;
      rmoDivide:    FRegA := FRegA / b;
      rmoDiv:       FRegA := FRegA div b;
      rmoMod:       FRegA := FRegA mod b;
      rmoMultiply:  FRegA := FRegA * b;

      rmoAexpB:     begin
                      if (b = 0) then
                        FRegA := 1
                      else
                        if (b < 0) then
                          FRegA := 1 / Exp(b * ln(-FRegA))
                        else
                          FRegA := Exp(b * ln(FRegA));
                    end;
  end;
end;


// A <- {A {OR|AND} B} | {NOT A}
procedure TrwVirtualMachine.LogOper(const Oper: TrwVMOperation);
begin
  case Oper of
    rmoAandB: FRegA := Boolean(FRegA) and Boolean(FRegB);
    rmoAorB:  FRegA := Boolean(FRegA) or Boolean(FRegB);
    rmoNotA:  FRegA := not Boolean(FRegA);
  end;
end;


procedure TrwVirtualMachine.FindObjectByIndex;
var
  i: Integer;
  C: TrwComponent;
begin
  i := Pop;
  C := TrwComponent(Integer(Pop));
  FRegA := Integer(C.GetVirtualChild(i));
end;


procedure TrwVirtualMachine.ObjectPropValue;
var
  h: string;
  C: TObject;
  lClassTypeInfo: PTypeInfo;
  lPropInfo: PPropInfo;
  j: Integer;
begin
  h := Pop;
  C := TObject(Integer(Pop));

  lClassTypeInfo := C.ClassInfo;
  lPropInfo := GetPropInfo(lClassTypeInfo, h);
  if not Assigned(lPropInfo) then
    if C is TrwComponent then
    begin
      j := TrwComponent(C)._UserProperties.IndexOf(h);
      if j <> -1 then
        FRegA := TrwComponent(C)._UserProperties[j].Value
      else
        ShowVMError(ResErrVMUnknownProp, [h, TrwComponent(C).PClassName]);
    end
    else
      ShowVMError(ResErrVMUnknownProp, [h, C.ClassName])

  else
    FRegA := GetComponentProperty(C, [h]);
end;


procedure TrwVirtualMachine.ObjectMethod;
var
  h: string;
  n, i: Integer;
  C: TObject;
  lParams: array of variant;
  lMethodAddr: Pointer;
  lEDX, lECX, P: Pointer;
  lRes: Variant;
  fl: Boolean;
  ResType: TrwVarTypeKind;
begin
  h := Pop;
  ResType := TrwVarTypeKind(Integer(Pop));
  n := Pop;  //count of params
  SetLength(lParams, n);
  for i := High(lParams) downto Low(lParams) do
    lParams[i] := Pop;
  C := TObject(Integer(Pop));

  lMethodAddr := C.MethodAddress('P' + h);
  if not Assigned(lMethodAddr) then
    ShowVMError(ResErrVMUnknownProp, [h, C.ClassName]);

  if (Length(lParams) >= 1) then
    lEDX := Addr(lParams[0]);

  if (Length(lParams) >= 2) then
    lECX := Addr(lParams[1]);

  for i := 2 to High(lParams) do
  begin
    P := Addr(lParams[i]);
    asm
      PUSH P;
    end;
  end;

  if (ResType <> rwvUnknown) then
    case Length(lParams) of
      0: lEDX := Addr(lRes);
      1: lECX := Addr(lRes);
    else
      P := Addr(lRes);
      asm
        PUSH P;
      end
    end;

  fl := BeforeTraceInto;

  asm
    MOV   EDX, lEDX
    MOV   ECX, lECX
    MOV   EAX, C
    CALL  lMethodAddr
  end;

  if (ResType = rwvUnknown) then
    VarClear(lRes);

  FRegA := lRes;

  AfterTraceInto(fl);
end;


procedure TrwVirtualMachine.SetObjectProp;
var
  C: TObject;
  h: string;
  v: Variant;
  fl: Boolean;
  lClassTypeInfo: PTypeInfo;
  lPropInfo: PPropInfo;
  j: Integer;
begin
  v := Pop;
  h := Pop;
  C := TObject(Integer(Pop));

  fl := BeforeTraceInto;

  lClassTypeInfo := C.ClassInfo;
  lPropInfo := GetPropInfo(lClassTypeInfo, h);
  if not Assigned(lPropInfo) then
    if C is TrwComponent then
    begin
      j := TrwComponent(C)._UserProperties.IndexOf(h);
      if j <> -1 then
        TrwComponent(C)._UserProperties[j].Value := v
      else
        ShowVMError(ResErrVMUnknownProp, [h, TrwComponent(C).PClassName]);
    end
    else
      ShowVMError(ResErrVMUnknownProp, [h, C.ClassName])

  else
    SetComponentProperty(C, [h], v);

  AfterTraceInto(fl);
end;


procedure TrwVirtualMachine.SetArrayElement;
var
  val: Variant;
  n: Integer;
  v: PVariant;
begin
  val := Pop;
  n := Pop;
  v := Pointer(Integer(Pop));

  v^[n] := val;
end;


procedure TrwVirtualMachine.SetStrElement;
var
  val: Variant;
  n: Integer;
  v: PVariant;
  s: string;
begin
  val := Pop;
  n := Pop;
  v := Pointer(Integer(Pop));
  s := v^;
  s[n] := String(val)[1];
  v^ := s;
end;


procedure TrwVirtualMachine.IntTypeConversion(var AValue: Variant;  const AType: TrwVarTypeKind; const AConvNull: Boolean);
begin
  if not VarIsNull(AValue) then
    case AType of
      rwvInteger:  AValue := VarAsType(AValue, varInteger);
      rwvBoolean:  AValue := VarAsType(AValue, varBoolean);
      rwvString:   AValue := VarAsType(AValue, varString);
      rwvFloat:    AValue := VarAsType(AValue, varDouble);
      rwvCurrency: AValue := VarAsType(RoundTo(VarAsType(AValue, varDouble) + rwAccRoundingCorrection, -2), varCurrency);
      rwvDate:     AValue := VarAsType(AValue, varDate);
      rwvPointer:  AValue := VarAsType(AValue, varInteger);
      rwvArray:    if not VarIsArray(AValue) then
                     AValue := VarAsType(AValue, varArray);  //for exception purpose
    end

  else if AConvNull then
  begin
    case AType of
      rwvInteger:  AValue := VarAsType(0, varInteger);
      rwvFloat:    AValue := VarAsType(0, varDouble);
      rwvCurrency: AValue := VarAsType(0, varCurrency);
      rwvPointer:  AValue := VarAsType(0, varInteger);
      rwvDate:     AValue := VarAsType(0, varDate);
      rwvBoolean:  AValue := False;
      rwvString:   AValue := '';
      rwvArray:    AValue := VarAsType(AValue, varArray);  //for exception purpose
    end
  end;
end;



procedure TrwVirtualMachine.ConvertToType;
var
  t: TrwVarTypeKind;
  cn: Boolean;
begin
  cn := Pop;
  t := TrwVarTypeKind(Pop);
  FRegA := Pop;

  IntTypeConversion(FRegA, t, cn);
end;


procedure TrwVirtualMachine.ArrayElement;
var
  i: Integer;
  v: PVariant;
begin
  i := Pop;
  v := Pointer(Integer(Pop));
  FregA := V^[i];
end;



procedure TrwVirtualMachine.StrElement;
var
  i: Integer;
  v: String;
begin
  i := Pop;
  v := Pop;
  FregA := V[i];
end;



procedure TrwVirtualMachine.GetAddrLocVar;
var
  i: Integer;
begin
  i := Pop;
  Inc(i, FCurHeapTop);
  CheckHeapPointer(i);
  FRegA := Integer(Addr(FReportCode^[i]));
end;


procedure TrwVirtualMachine.GetAddrGlobVar;
var
  i: Integer;
  C: TrwComponent;
begin
  i := Pop;
  C := TrwComponent(Pointer(Integer(Pop)));
  FRegA := Integer(C.GlobalVarFunc.Variables[i].VarAddr);
end;


procedure TrwVirtualMachine.SetArrLength;
var
  i: Integer;
  v: PVariant;
begin
  i := Pop;
  v := Pointer(Integer(Pop));
  VarArrayRedim(V^, i-1);
end;


procedure TrwVirtualMachine.GetLength;
begin
  FRegA := Length(Pop);
end;


procedure TrwVirtualMachine.GetArrLength;
var
  v: PVariant;
begin
  v := Pointer(Integer(Pop));
  FRegA := VarArrayHighBound(v^, 1) - VarArrayLowBound(v^, 1) + 1;
end;


procedure TrwVirtualMachine.FormatVal;
var
  v: Variant;
  f: String;
begin
  v := Pop;
  f := Pop;
  FregA := FormatValue(f, v);
end;


procedure TrwVirtualMachine.SkipPage;
begin
  CurrentRendering.SkipPage;
end;


procedure TrwVirtualMachine.SubstFldValue;
var
  v2, TableName: String;
  v1: Variant;
begin
  v2 := Pop;
  v1 := Pop;

  if Pos('.', v2) <> 0 then
  begin
    TableName := GetNextStrValue(v2, '.');
    FRegA := rwDictionary.PGetFieldValueDescription(TableName, v2, v1);
  end
  else
    FRegA := RWEngineExt.AppAdapter.SubstituteValue(v1, AnsiUpperCase(v2));
end;

procedure TrwVirtualMachine.SubstSubStr;
var
  v1, v2, v3: String;
  n, i: Integer;
begin
  v3 := Pop;
  v2 := Pop;
  v1 := Pop;

  FRegA := '';
  n := Length(v2);
  while True do
  begin
    i := Pos(v2, v1);
    if i = 0 then
    begin
      FRegA := FRegA + v1;
      break;
    end;
    FRegA := FRegA + Copy(v1, 1, i-1)+ v3;
    Delete(v1, 1, i+n-1);
  end;
end;


procedure TrwVirtualMachine.PosSubStr;
var
  v1, v2: String;
begin
  v2 := Pop;
  v1 := Pop;
  FRegA := Pos(v2, v1);
end;


procedure TrwVirtualMachine.CopySubStr;
var
  v1: String;
  v2, v3: Integer;
begin
  v3 := Pop;
  v2 := Pop;
  v1 := Pop;

  FRegA := Copy(v1, v2, v3);
end;


procedure TrwVirtualMachine.DelSubStr;
var
  v1: String;
  v2, v3: Integer;
begin
  v3 := Pop;
  v2 := Pop;
  v1 := Pop;

  Delete(v1, v2, v3);
  FRegA := v1;
end;


procedure TrwVirtualMachine.PageNumber;
begin
  FRegA := CurrentRendering.RenderingPaper.Header.PagesCount;
end;


procedure TrwVirtualMachine.SetPrintPos;
begin
  if (CurrentRendering = nil) or CurrentRendering.RenderingPaper.IsASCIIResult then
    ShowVMError(ResErrVMOperNotApplicable, [])
  else
    CurrentRendering.RenderingPaper.CurrentTop := Pop;
end;


procedure TrwVirtualMachine.GetPrintPos;
begin
  if (CurrentRendering = nil) or CurrentRendering.RenderingPaper.IsASCIIResult then
    ShowVMError(ResErrVMOperNotApplicable, [])
  else
    FRegA := CurrentRendering.RenderingPaper.CurrentTop;
end;


procedure TrwVirtualMachine.RoundFloat;
var
  v1: Extended;
  v2: Integer;
begin
  v2 := Pop;
  v1 := Pop;
  FRegA := RoundTo(v1 + rwAccRoundingCorrection, - v2);
end;


procedure TrwVirtualMachine.TruncFloat;
begin
  FRegA := Integer(Trunc(Pop));
end;


procedure TrwVirtualMachine.SetTime;
var
  v1: TDateTime;
  v2: string;
  F: Real;
begin
  v2 := Pop;
  v1 := Pop;
  F := Trunc(v1);
  v1 := TDateTime(F);
  F := StrToTime(v2);
  v1 := Real(v1)+F;
  FRegA := v1;
end;



procedure TrwVirtualMachine.ChrByCode;
begin
  FRegA := Chr(Byte(Pop));
end;


procedure TrwVirtualMachine.CodeByChr;
var
  v: string;
begin
  v := Pop;
  FRegA := Ord(v[1]);
end;


procedure TrwVirtualMachine.CreateObject;
var
  v1: string;
  lComponent: TrwComponent;
  O: TrwComponent;
  Cl: IrwClass;
begin
  O := TrwComponent(Pointer(Integer(Pop)));
  v1 := Pop;

  Cl := rwRTTIInfo.FindClass(v1);

  if not Assigned(Cl) then
    ShowVMError(ResErrWrongClass, []);

  if Cl.IsUserClass then
    lComponent := (Cl.GetUserClass.CreateComponent(nil, True).RealObject as TrwComponent)
  else
    lComponent := (Cl.CreateObjectInstance as TrwComponent);

  if Assigned(O) then
    O.InsertVirtualComponent(lComponent);

  FRegA := Integer(Pointer(lComponent));
end;


procedure TrwVirtualMachine.DestroyObject;
var
  v: Integer;
begin
  v := Pop;
  TObject(Pointer(v)).Free;
end;


procedure TrwVirtualMachine.IndexOfArrayElement;
var
  v1: PVariant;
  v2: Variant;
  i, n, m: Integer;
begin
  v2:= Pop;
  v1:= Pointer(Integer(Pop));

  FRegA := -1;
  n := VarArrayLowBound(v1^, 1);
  m := VarArrayHighBound(v1^, 1);
  for i := n to m do
  begin
    if VarIsNull(v2) and not VarIsNull(v1^[i]) or
       not VarIsNull(v2) and VarIsNull(v1^[i]) then
      continue;

    if VarIsNull(v2) and VarIsNull(v1^[i]) or (v1^[i] = v2) then
    begin
      FRegA := i;
      break;
    end;
  end;
end;


procedure TrwVirtualMachine.StrLowerCase;
begin
  FRegA := AnsiLowerCase(Pop);
end;


procedure TrwVirtualMachine.StrUpperCase;
begin
  FRegA := AnsiUpperCase(Pop);
end;


procedure TrwVirtualMachine.TrimStr(const AType: TrwVMEmbeddedFunc);
var
  v: String;
begin
  v := Pop;
  case AType of
    refTrim:      FRegA := Trim(v);
    refTrimLeft:  FRegA := TrimLeft(v);
    refTrimRight: FRegA := TrimRight(v);
  end;
end;


procedure TrwVirtualMachine.CustomCall;
var
  v, ParCount, i: Integer;
  lParams: array of variant;
  R: Variant;
begin
  v := Pop;
  if v < rwVMExternalFunctBase then
    case v of
      1:  FRegA := Integer(Pointer(rwDictionary));
      2:  SkipPage;
      3:  SetPrintPos;
      4:  GetPrintPos;
      5:  PageNumber;
      6:  FRegA := Integer(Pointer(SystemLibComponents));
      7:  FRegA := Integer(Pointer(SystemLibFunctions));
      8:  FileExists;
      9:  AppendASCIILine;
      10: InsertASCIILine;
      11: UpdateASCIILine;
      12: DeleteASCIILine;
      13: GetASCIILine;
      14: ASCIILineCount;
    else
      FRegA := 0;
    end

  else
  begin
    v := v - rwVMExternalFunctBase;
    ParCount := Pop;
    SetLength(lParams, ParCount);
    for i := High(lParams) downto Low(lParams) do
      lParams[i] := Pop;
    RWEngineExt.AppAdapter.ExecuteExternalFunction(v, lParams, R);
    FregA := R;
  end;
end;



procedure TrwVirtualMachine.ShowError;
var
  v: String;
begin
  v := Pop;
  raise ErwAbort.Create(v);
end;


procedure TrwVirtualMachine.TypeOf;
var
  v: Variant;
begin
  v := Pop;
  if VarIsArray(V) then
    FRegA := rwvArray
  else
    case VarType(v) of
      varInteger:  FRegA := rwvInteger;
      varBoolean:  FRegA := rwvBoolean;
      varString:   FRegA := rwvString;
      varDouble:   FRegA := rwvFloat;
      varCurrency: FRegA := rwvCurrency;
      varDate:     FRegA := rwvDate;
      varVariant:  FRegA := rwvVariant;
    else
      FRegA := rwvUnknown;
    end;
end;


procedure TrwVirtualMachine.AbsFloat;
begin
   FRegA := Abs(Pop);
end;


procedure TrwVirtualMachine.CreateConstArray;
var
  n, i: Integer;
begin
  n := Pop;
  FRegA := VarArrayCreate([0, n-1], varVariant);
  for i := n - 1 downto 0 do
    FRegA[i] := Pop;
end;


procedure TrwVirtualMachine.SignFloat;
begin
  if Pop < 0 then
    FRegA := -1
  else
    FRegA := 1;
end;


//create new local variable
procedure TrwVirtualMachine.CreateNewVar(const AType: TrwVarTypeKind);
begin
  if FHeapPointer < FHeapTop then
   ShowVMError(ResErrVMAccessViolation, [FormatFloat('0000000000', FHeapPointer)])

  else if FHeapPointer >= FStackPointer then
    ShowVMError(ResErrVMOutOfMemory, []);

  case AType of
    rwvInteger:  FReportCode^[FHeapPointer] := 0;
    rwvBoolean:  FReportCode^[FHeapPointer] := False;
    rwvFloat:    FReportCode^[FHeapPointer] := 0.0;
    rwvCurrency: FReportCode^[FHeapPointer] := 0.00;
    rwvString:   FReportCode^[FHeapPointer] := '';
    rwvDate:     FReportCode^[FHeapPointer] := 0;
    rwvPointer:  FReportCode^[FHeapPointer] := 0;
    rwvVariant:  VarClear(FReportCode^[FHeapPointer]);
    rwvArray:    FReportCode^[FHeapPointer] := VarArrayCreate([0, -1], varVariant);
  end;

  Inc(FHeapPointer);
end;



//POP from stack

function TrwVirtualMachine.Pop: Variant;
begin
  CheckStackPointer(FStackPointer);
  Result := FReportCode^[FStackPointer];
  VarClear(FReportCode^[FStackPointer]);
  Inc(FStackPointer);
end;


//PUSH to stack

procedure TrwVirtualMachine.Push(const AValue: Variant);
begin
  Dec(FStackPointer);
  CheckStackPointer(FStackPointer);
  FReportCode^[FStackPointer] := AValue;
end;


//Programm loop

procedure TrwVirtualMachine.Run(const AEntryPoint: TrwVMAddress; const ACustomPCode: PTrwCode = nil);
var
  OldCurHeapTop: Integer;
  OldHeapPointer: Integer;
  OldNextStep: Integer;
  OldInstrPointer: Integer;
  OldCS: PTrwCode;
  OldCodeSegment: TrwVMCodeSegment;
  OldCodeMemBottom: Integer;
  OldOffSetAddress: Integer;
begin
  if not FInPause and (AEntryPoint.Segment <> rcsWatchList) and (CurrentEntryPoint.Segment <> rcsNone) then
    FCallStack.Add(Pointer(GetAbsoluteAddress(CurrentEntryPoint)));

  OldCurHeapTop := FCurHeapTop;
  OldHeapPointer := FHeapPointer;
  OldCodeMemBottom := FCodeMemBottom;
  OldNextStep := FNextStep;
  OldInstrPointer := InstrPointer;
  OldCS := FCode;
  OldCodeSegment := FCodeSegment;
  OldOffSetAddress := FOffSetAddress;

  FOffSetAddress := GetOffSet(AEntryPoint.Segment, Length(FReportCode^));

  try
    case AEntryPoint.Segment of
      rcsReport:        FCode := FReportCode;

      rcsLibFunctions:  FCode := Addr(SystemLibFunctions.CompiledCode);

      rcsLibComponents: FCode := SystemLibComponents.GetCompiledCode;

      rcsWatchList:     if Assigned(ACustomPCode) then
                          FCode := ACustomPCode
                        else
                          FCode := Addr(DebugInfo.WatchList.CompiledCode);
    else
      ShowVMError(ResErrVMAccessViolation, [FormatFloat('0000000000', AEntryPoint.Offset + FOffSetAddress)]);
    end;

    FCodeMemBottom := High(FCode^);
    FInstrPointer := AEntryPoint.Offset;

    if AEntryPoint.Segment <> rcsWatchList then
      FCurHeapTop := FHeapPointer;

    FCodeSegment := AEntryPoint.Segment;

    try
      MainLoop;
    except
      on E: Exception do
      begin
        if FCodeSegment <> rcsWatchList then
          DoOnError(E.Message);
        raise;
      end;
    end;

  finally
    Halt := False;
    FCode := OldCS;

    if AEntryPoint.Segment <> rcsWatchList then
      FCurHeapTop := OldCurHeapTop;

    FOffSetAddress := OldOffSetAddress;
    ClearMemory(OldHeapPointer, FHeapPointer);
    FHeapPointer := OldHeapPointer;
    FCodeMemBottom := OldCodeMemBottom;
    FInstrPointer := OldInstrPointer;
    FNextStep := OldNextStep;
    FCodeSegment := OldCodeSegment;

    if not FInPause and (AEntryPoint.Segment <> rcsWatchList) and (CurrentEntryPoint.Segment <> rcsNone) then
      FCallStack.Delete(FCallStack.Count - 1);
  end;
end;


procedure TrwVirtualMachine.DebuggerLoop;
var
  BrkPntIndex: Integer;
begin
  if not DebugInfo.InputFormCode then
    Application.ProcessMessages;

  if DebugInfo.Aborted then
    VM.ShowVMError(ResErrVMAbort, []);

  if not DebugInfo.GoByStep then
    BrkPntIndex := DebugInfo.CheckForBreakPoint(FInstrPointer + FOffSetAddress)
  else
    BrkPntIndex := -1;

  if BrkPntIndex <> -1 then
  begin
    DebugInfo.GoByStep := True;
    DebugInfo.Waiting := True;
  end;

  if DebugInfo.GoByStep then
  begin
    DebugInfo.CodeSegment := CodeSegment;
    DebugInfo.CurrentAsmPosition := FInstrPointer;
  end;

  repeat
    if DebugInfo.Waiting then
    begin
      Application.ProcessMessages;
      Sleep(10);
    end;
  until not DebugInfo.Waiting;
end;


procedure TrwVirtualMachine.AfterTraceInto(const APrevState: Boolean);
begin
  if Debugging and not DebugInfo.DontStop and not DebugInfo.CalcWatches  then
    DebugInfo.GoByStep := APrevState;
end;


function TrwVirtualMachine.BeforeTraceInto: Boolean;
begin
  if Debugging then
  begin
    Result := DebugInfo.GoByStep;
    if DebugInfo.TraceInto then
      DebugInfo.TraceInto := False
    else
    begin
      DebugInfo.Waiting := False;
      DebugInfo.GoByStep := False;
      DebugInfo.TraceInto := False;
    end;
  end
  else
    Result := False;
end;


procedure TrwVirtualMachine.CalcWatches;
var
  i: Integer;
  OldWaiting: Boolean;
  EP: TrwVMAddress;

  function ResToStr: string;
  begin
    try
      case VarType(FRegA) of
        varUnknown: Result := '<Unknown>';
        varNull:    Result := '<Null>';
        varString,
        varOleStr:  Result := ''''+FRegA+'''';

      else
        if VarIsArray(FRegA) then
          Result := '<Array>'
        else
          Result := VarAsType(FRegA, varString);
      end;
    except
      Result := 'Type conversion error';
    end;
  end;

begin
  OldWaiting := DebugInfo.Waiting;
  try
    DebugInfo.CalcWatches := True;
    Push(FRegA);
    Push(FRegB);
    Push(FFlag);

    for i := 0 to DebugInfo.WatchList.Count -1 do
      if DebugInfo.WatchList[i].Address <> -1 then
        try
          EP := GetEntryPoint(DebugInfo.WatchList[i].Address);
          Run(EP);
          DebugInfo.WatchList[i].Result := ResToStr;
          DebugInfo.WatchList[i].Error := False;
        except
          on E: Exception do
          begin
            DebugInfo.WatchList[i].Error := True;
            DebugInfo.WatchList[i].Result := E.Message;
          end;
        end
      else
        DebugInfo.WatchList[i].Error := True;

  finally
    FFlag := Pop;
    FRegB := Pop;
    FRegA := Pop;
    DebugInfo.CalcWatches := False;
    DebugInfo.Waiting := OldWaiting;
  end;
end;


procedure TrwVirtualMachine.MainLoop;
var
  Op: TrwVMOperation;

  procedure rmDebuggerCallback;
  begin
    if not FInPause  and (FCodeSegment <> rcsWatchList) then
    begin
      FInPause := True;
      try
        DoOnStep;  // debugging interruption
        if Halt then
          VM.ShowVMError(ResErrVMAbort, []);
      finally
        FInPause := False;
      end;
    end;
  end;

begin
  while True do
  begin
    if Debugging and not DebugInfo.CalcWatches then
    begin
      DebuggerLoop;
      if DebugInfo.Aborted then
        VM.ShowVMError(ResErrVMAbort, []);
    end
    else
    begin
      PumpMainFormMessages;
      if Halt or Application.Terminated then
        VM.ShowVMError(ResErrVMAbort, []);
    end;

    rmDebuggerCallback;

    try
      FNextStep := 1;
      Op := TrwVMOperation(FCode^[FInstrPointer]);
      case Op of
        rmoNewVar:        NewVar;

        rmoMOVAconst:     MOVRconst(FRegA);
        rmoMOVBconst:     MOVRconst(FRegB);
        rmoMOVAlocVar:    MOVRlocVar(FRegA);
        rmoMOVAglobVar:   MOVRglobVar(FRegA);
        rmoMOVBlocVar:    MOVRlocVar(FRegB);
        rmoMOVBglobVar:   MOVRglobVar(FRegB);
        rmoMOVlocVarA:    MOVlocVarR(FRegA);
        rmoMOVlocVarB:    MOVlocVarR(FRegB);
        rmoMOVglobVarA:   MOVglobVarR(FRegA);
        rmoMOVglobVarB:   MOVglobVarR(FRegB);
        rmoMOVAB:         FRegA := FRegB;
        rmoMOVBA:         FRegB := FRegA;
        rmoMOVArefB:      FRegA := PVariant(Pointer(Integer(FRegB)))^;
        rmoMOVrefBA:      PVariant(Pointer(Integer(FRegB)))^ := FRegA;

        rmoSwapAB:        SwapAB;

        rmoPushA:         Push(FRegA);
        rmoPushB:         Push(FRegB);
        rmoPushFlag:      Push(FFlag);
        rmoPushConst:     PushConst;
        rmoPopA:          FRegA := Pop;
        rmoPopB:          FRegB := Pop;
        rmoPopFlag:       FFlag := Pop;

        rmoAdd,
        rmoSubstract,
        rmoDivide,
        rmoDiv,
        rmoMod,
        rmoMultiply,
        rmoAexpB:         MathOper(Op);

        rmoIncA:          Inc(FRegA);
        rmoDecA:          Dec(FRegA);
        rmoIncB:          Inc(FRegB);
        rmoDecB:          Dec(FRegB);


        rmoReverseSignA:  FRegA := - FRegA;

        rmoAandB,
        rmoAorB,
        rmoNotA:          LogOper(Op);

        rmoCmpAlB,
        rmoCmpAeB,
        rmoCmpAgB,
        rmoCmpAneB,
        rmoCmpAleB,
        rmoCmpAgeB:       CmpAB(Op);

        rmoJump:          Jump;
        rmoJumpIfTrue:    JumpIf(True);
        rmoJumpIfFalse:   JumpIf(False);

        rmoCallEmbProc:  CallEmbProc;
        rmoCallEmbFunc:  CallEmbFunc;
        rmoCallEvent:    CallEvent;
        rmoCallLibFunc:  CallLibFunc;
        rmoCallLibCompEvent: CallLibCompEvent;

        rmoRet:
          break;

        rmoNope:;
      else
        ShowVMError(ResErrVMUnknownInstruction, []);
      end;

      GoFurtherBy(FNextStep);
    except
      on E: ErwVMError do
        raise;

      on E: Exception do
        ShowVMError(Format(ResErrVMRunTimeError, [FormatFloat('0000000000', FInstrPointer + FOffSetAddress)]) + #13 + E.Message, []);
    end;
  end;
end;

procedure TrwVirtualMachine.FileExists;
var
  v: String;
begin
  v := Pop;
  FRegA := SysUtils.FileExists(v);
end;


procedure TrwVirtualMachine.AppendASCIILine;
var
  s: String;
begin
  s := Pop;
  CurrentRendering.RenderingPaper.AppendASCIILine(s);
end;


procedure TrwVirtualMachine.ASCIILineCount;
begin
  FRegA := CurrentRendering.RenderingPaper.ASCIILineCount;
end;


procedure TrwVirtualMachine.DeleteASCIILine;
var
  l: Integer;
begin
  l := Pop;
  CurrentRendering.RenderingPaper.DeleteASCIILine(l);
end;


procedure TrwVirtualMachine.GetASCIILine;
var
  l: Integer;
begin
  l := Pop;
  FRegA := CurrentRendering.RenderingPaper.GetASCIILine(l);
end;


procedure TrwVirtualMachine.InsertASCIILine;
var
  l: Integer;
  s: String;
begin
  l := Pop;
  s := Pop;
  CurrentRendering.RenderingPaper.InsertASCIILine(s, l);
end;


procedure TrwVirtualMachine.UpdateASCIILine;
var
  l: Integer;
  s: String;
begin
  l := Pop;
  s := Pop;
  CurrentRendering.RenderingPaper.UpdateASCIILine(s, l);
end;


{ ErwVMError }

constructor ErwVMError.CreateVMError(ACS: TrwVMCodeSegment; AAddr: Integer; AMessage: string);
begin
  FCS := ACS;
  FIP := AAddr;
  if AMessage <> '' then
    AMessage := 'RWVM: ' + AMessage;

  Create(AMessage, True);
end;


procedure TrwVirtualMachine.GetTime;
var
  t: TDateTime;
begin
  t := Frac(RWEngineExt.AppAdapter.CurrentDateTime);
  FRegA := t;
end;

procedure TrwVirtualMachine.DesignerOpened;
begin
  FRegA := Assigned(FOnStep);
end;

procedure TrwVirtualMachine.AbortExecution;
var
  v: Variant;
  E: ErwVMAbort;
begin
  v := Pop;
  E := ErwVMAbort.CreateVMError(CodeSegment, InstrPointer, 'Abort');
  E.FReturnValue := v;
  raise E;
end;

function TrwVirtualMachine.CurrentEntryPoint: TrwVMAddress;
begin
  Result.Segment := FCodeSegment;
  Result.Offset := FInstrPointer;
end;

destructor TrwVirtualMachine.Destroy;
begin
  FCallStack.Free;
  inherited;
end;

procedure TrwVirtualMachine.GetCallStack(var ACallStack: TrwVMAddressList);
var
  i: Integer;
  A: TrwVMAddress;
begin
  SetLength(ACallStack, FCallStack.Count);

  for i := 0 to FCallStack.Count - 1 do
  begin
    A := GetEntryPoint(Integer(FCallStack[i]));
    ACallStack[i].Segment := A.Segment;
    ACallStack[i].Offset := A.Offset;    
  end;
end;


procedure TrwVirtualMachine.DoOnError(const AErrorMessage: String);
begin
  if Assigned(FOnError) and not DisableDebCallbacks then
    FOnError(AErrorMessage);
end;

procedure TrwVirtualMachine.DoOnStep;
begin
  if Assigned(FOnStep) and not DisableDebCallbacks then
    FOnStep;
end;

procedure TrwVirtualMachine.SetRMDebuggerCallback(AOnStep: TrwVMOnStep; AOnError: TrwVMOnError);
begin
  FOnStep := AOnStep;
  FOnError := AOnError;
end;

procedure TrwVirtualMachine.PumpMainFormMessages;
var
  Msg: TMsg;
begin
  if Assigned(Application.MainForm) then
    while PeekMessage(Msg, Application.MainForm.Handle, 0, 0, PM_REMOVE) do
    begin
      TranslateMessage(Msg);
      DispatchMessage(Msg);
    end;
end;

procedure TrwVirtualMachine.RandomRange;
var
  v1, v2: Integer;
begin
  v2 := Pop;
  v1 := Pop;
  FRegA := Math.RandomRange(v1, v2);
end;

procedure TrwVirtualMachine.TimeZoneOffset;
var
  TimeZoneRec: TTimeZoneInformation;
  Res: DWORD;
  iBias: integer;
begin
  Res := GetTimeZoneInformation(TimeZoneRec);
  if Res = TIME_ZONE_ID_INVALID then
    ShowVMError(SysErrorMessage(GetLastError), []);

  iBias := TimeZoneRec.Bias;
  FRegA := -iBias;
end;

end.
