unit rwBasicUtils;

interface

uses SysUtils, StrUtils, rmTypes, rwEngineTypes, Windows, Forms;

function  GetNextStrValue(var AStr: string; const ADelim: String): String;
function  GetPrevStrValue(var AStr: string; const ADelim: String): String;
function  StrCopySafe(Dest: PChar; const Source: PChar; const DestBufSize: Cardinal): PChar;

procedure rwError(const AErrorMessage: String);

procedure DestroyInterfacedObject(var AIObject: IrmInterfacedObject);

procedure HideAppFromTaskBar;
procedure ShowAppOnTaskBar;

procedure ConvertASCII2Xls(const ASourceFile, ATargetFile, ADelimiter, AQualifier: String);

implementation

uses
  XLSFile, XLSWorkbook;

procedure rwError(const AErrorMessage: String);
begin
  raise ErwException.Create(AErrorMessage);
end;

procedure DestroyInterfacedObject(var AIObject: IrmInterfacedObject);
var
  O: TObject;
begin
  O := AIObject.VCLObject;
  AIObject := nil;
  FreeAndNil(O);
end;

function StrCopySafe(Dest: PChar; const Source: PChar; const DestBufSize: Cardinal): PChar;
begin
  if Cardinal(StrLen(Source)) + 1 > DestBufSize then
    raise ERangeError.Create('Cannot perform string copy operation. Allocated buffer is too small.');

  Result := StrCopy(Dest, Source);
end;


function GetNextStrValue(var AStr: string; const ADelim: String): String;
var
  j: Integer;
begin
   j := Pos(ADelim, AStr);
   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end

   else
   begin
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j-1+Length(ADelim));
   end;
end;

function GetPrevStrValue(var AStr: string; const ADelim: String): String;
var
  i, j: Integer;
begin
   i := 0;
   repeat
     j := i;
     if i <> 0 then
       Inc(i, Length(ADelim));
     i := PosEx(ADelim, AStr, i);
   until i = 0;

   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end

   else
   begin
     Result := Copy(AStr, j + Length(ADelim), Length(AStr) - (j + Length(ADelim)) + 1);
     Delete(AStr, j, Length(AStr) - j + 1);
   end;
end;

var OldAppTitle: String;

procedure HideAppFromTaskBar;
var
  AppActive: Boolean;
begin
  if Application.Title <> '' then
  begin
    OldAppTitle := Application.Title;
    Application.Title := '';  // to hide from task manager
  end;

  AppActive := Application.Active;
  ShowWindow(Application.Handle, SW_HIDE);
  SetWindowLong(Application.Handle, GWL_EXSTYLE, GetWindowLong(Application.Handle, GWL_EXSTYLE) or WS_EX_TOOLWINDOW );
  if AppActive then
    ShowWindow(Application.Handle, SW_SHOW)
  else
    ShowWindow(Application.Handle, SW_SHOWNOACTIVATE);
end;

procedure ShowAppOnTaskBar;
var
  AppActive: Boolean;
begin
  if OldAppTitle <> '' then
    Application.Title := OldAppTitle;

  AppActive := Application.Active;
  ShowWindow(Application.Handle, SW_HIDE);
  SetWindowLong(Application.Handle, GWL_EXSTYLE, GetWindowLong(Application.Handle, GWL_EXSTYLE) and not WS_EX_TOOLWINDOW );
  if AppActive then
    ShowWindow(Application.Handle, SW_SHOW)
  else
    ShowWindow(Application.Handle, SW_SHOWNOACTIVATE);
end;

procedure ConvertASCII2Xls(const ASourceFile, ATargetFile, ADelimiter, AQualifier: String);
var
  F: TextFile;
  sLine: String;
  iLineNum: integer;
  XLS: TXLSFile;
  Sheet: TSheet;

  procedure ParseLine(const ALine: String; const ALineNum: integer);
  var
    S, V: String;
    ColNum: integer;
    iQLength: integer;
  begin
    S := ALine;
    ColNum := 0;
    repeat
      V := GetNextStrValue(S, ADelimiter);
      if (V <> '') or ((V = '') and (S <> '')) then
      begin
        if AQualifier <> '' then
        begin
          iQLength := Length(AQualifier);
          if Copy(V, 1, iQLength) = AQualifier then
            Delete(V, 1, iQLength);
          if Copy(V, Length(V) - iQLength + 1, iQLength) = AQualifier then
            Delete(V, Length(V) - iQLength + 1, iQLength);
        end;
        Sheet.Cells[ALineNum, ColNum].Value := V;
        Inc(ColNum);
      end;
    until S = '';
  end;
begin
  if ADelimiter = '' then
    raise ErwException.Create('Delimiter cannot be empty');
  if not FileExists(ASourceFile) then
    raise ErwException.Create('Source file does not exist');

  XLS := TXLSFile.Create;
  try
    Sheet := XLS.Workbook.Sheets[0];
    AssignFile(F, ASourceFile);
    Reset(F);
    try
      iLineNum := 0;
      while not Eof(F) do
      begin
        Readln(F, sLine);
        ParseLine(sLine, iLineNum);
        Inc(iLineNum);
      end;
    finally
      CloseFile(F);
    end;
    Sheet.Columns.AutoFit;
    XLS.SaveAs(ATargetFile);
  finally
    XLS.Free;
  end;
end;

end.
