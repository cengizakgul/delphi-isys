unit rmReport;

interface

uses Classes, Controls, SysUtils, Windows, Types, Dialogs, Variants, AbZipPrc, AbUnzPrc,
     rwBasicClasses, rmCommonClasses, rwReport, rwTypes, rwUtils, sbSQL,
     evStreamUtils, BlockCiphers, rwEngineTypes, rmTypes,
     rmPrintFormControls, rmASCIIFormControls;

type
  TrmReport = class;

  TrmSecureWrapper = class(TComponent)
  private
    FLicence: String;
    FReport:  TrmReport;

    procedure WriteData(AStream: TStream);
    procedure ReadData(AStream: TStream);

  protected
    procedure DefineProperties(Filer: TFiler); override;

  published
    property Licence: String read FLicence write FLicence;
  end;


  TrmReport = class(TrwComponent, IrmReportObject)
  private
    FInputForm: TrwInputFormContainer;
    FSecurityInfo: TrmSecureWrapper;
    FCompiledCode: TrwCode;
    FDebInf: TrwDebInf;
    FDebSymbInf: TrwDebInf;
    FLibVersion: TDateTime;
    FBeginWorkCount: Integer;
    FRenderer: IrmReportRenderer;

    procedure ReadReport(AStream: TStream);
    procedure WriteReport(AStream: TStream);
    function  GetSecurityInfo: TrmSecureWrapper;
    procedure WriteCompiledCode(Stream: TStream);
    procedure ReadCompiledCode(Stream: TStream);
    procedure WriteLibVersion(Writer: TWriter);
    procedure ReadLibVersion(Reader: TReader);

    procedure CheckMultipleInputFormSituation;
    procedure FixInputFormComponentIndex;

  protected
    procedure RegisterComponentEvents; override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure Loaded; override;
    procedure AfterInitLibComponent; override;

  //Interface
    function  DesignBehaviorInfo: TrwDesignBehaviorInfoRec; override;
    function  CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant; override;
    function  CreateChildObject(const AClassName: String): IrmDesignObject; override;
    procedure AddChildObject(AObject: IrmDesignObject); override;
    procedure RemoveChildObject(AObject: IrmDesignObject); override;
    function  GetCompiledCode: PTrwCode;
    function  GetDebInf: PTrwDebInf;
    function  GetDebSymbInf: PTrwDebInf;

  public
    property  InputForm: TrwInputFormContainer read FInputForm;
    property  SecurityInfo: TrmSecureWrapper read GetSecurityInfo;
    property  LibVersion: TDateTime read FLibVersion;
    property  Renderer: IrmReportRenderer read FRenderer;
    property  CompiledCode: TrwCode read FCompiledCode;
    property  DebInf: TrwDebInf read FDebInf;
    property  DebSymbInf: TrwDebInf read FDebSymbInf;

    constructor Create(AComponent: TComponent); override;
    destructor Destroy; override;

    procedure Compile(ASyntaxCheck: Boolean = False); override;
    procedure ClearCompiledInfo;

    procedure SetReportParams(const AParams: PTrwReportParamList);
    procedure GetReportParams(const AParams: PTrwReportParamList);

    procedure InitializeForDesign; override;
    function  IsDesignMode: Boolean; override;
    function  IsLicenced: Boolean;

    procedure InsertVirtualComponent(AComponent: TrwComponent); override;

    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
    procedure LoadFromFile(const AFileName: string);
    procedure SaveToFile(const AFileName: string);

    procedure BeginWork;
    procedure EndWork;
    procedure Prepare;
    function  CanShowInputForm(ARunTime: Boolean): Boolean;
    function  ShowInputForm(AParent: HWND; ARunTime: Boolean = True): Boolean;
    procedure CloseInputForm;
    function  Execute(const AShowInputForm: Boolean): IrwResult;
  end;


implementation

uses rwEngine, rwParser, rwVirtualMachine, rwDsgnControls, rmRenderer, rwReportControls, rwInputParamsFormFrm,
     rwDebugInfo, rmXMLFormControls;

{ TrmReport }

constructor TrmReport.Create(AComponent: TComponent);
begin
  inherited;
  FVirtOwner := True;
end;

destructor TrmReport.Destroy;
begin
  if FBeginWorkCount > 0 then
  begin
    FBeginWorkCount := 0;
    EndWork;  // For Exception only
  end;

  FreeAndNil(FSecurityInfo);
  inherited;
end;

procedure TrmReport.ClearCompiledInfo;
begin
  SetLength(FCompiledCode, 0);
  SetLength(FDebInf, 0);
  SetLength(FDebSymbInf, 0);
end;

procedure TrmReport.Compile(ASyntaxCheck: Boolean);
begin
  if LibDesignMode then
    inherited
  else
  begin
    SetLength(FCompiledCode, 256 * 1024);  // reservation for running user properties. Should be bigger than compiling P-code size! It's a "feature" and needs to be fixed.
    SetCompiledCode(@FCompiledCode, cVMReportOffset);
    SetCompilingPointer(0);

    SetLength(FDebInf, 0);
    SetDebInf(@FDebInf);
    SetDebInfPointer(0);
    SetLength(FDebSymbInf, 0);
    SetDebSymbInf(@FDebSymbInf);
    SetDebSymbInfPointer(0);

    try
      CreateRwVM(@FCompiledCode);
      try
        inherited;
      finally
        DestroyRwVM;
      end;
      FLibVersion := RWEngineExt.AppAdapter.CurrentDateTime;
    except
      FLibVersion := 0;
      ClearCompiledInfo;
      raise;
    end;

    SetLength(FCompiledCode, GetCompilingPointer);
    SetLength(FDebInf, GetDebInfPointer);
    SetLength(FDebSymbInf, GetDebSymbInfPointer);
  end;
end;

procedure TrmReport.DefineProperties(Filer: TFiler);

  function IsCompiledCodeNotEmpty: Boolean;
  begin
    Result := Length(FCompiledCode) > 0;
  end;

begin
  inherited;
  Filer.DefineBinaryProperty('CompiledCode', ReadCompiledCode, WriteCompiledCode, IsCompiledCodeNotEmpty);
  Filer.DefineProperty('LibVersion', ReadLibVersion, WriteLibVersion, True);
end;

function TrmReport.Execute(const AShowInputForm: Boolean): IrwResult;
var
  R: Variant;

  function InputParams: Boolean;
  var
    R: Variant;
  begin
    if CanShowInputForm(True) then
    begin
      R := True;
      ExecuteEventsHandler('BeforeShowInputForm', [Integer(Addr(R))]);
      if R then
        Result := ShowInFormContents(InputForm, '', nil)
      else
        Result := True;
    end
    else
      Result := True;
  end;

begin
  ClearTempTables(Self);

  BeginWork;
  try
    Result := nil;
    if AShowInputForm then
      R := InputParams
    else
      R := True;
    if R then
    begin
      PrepareFormulas;
      ExecuteEventsHandler('BeforePrintReport', [Integer(Addr(R))]);
      if R then
      begin
        FRenderer := TrmRenderer.Create(Self);
        Result := FRenderer.Render;
        ExecuteEventsHandler('AfterPrintReport', []);
      end;
    end;
  finally
    EndWork;
    FRenderer := nil;
  end;
end;

function TrmReport.GetSecurityInfo: TrmSecureWrapper;
begin
  if not Assigned(FSecurityInfo) then
  begin
    FSecurityInfo := TrmSecureWrapper.Create(nil);
    FSecurityInfo.FReport := Self;
  end;
  Result := FSecurityInfo;
end;

procedure TrmReport.InitializeForDesign;
begin
  Name := 'Report';
  inherited;
  if not IsLibComponent then
    InitGlobalVarFunc;
end;

procedure TrmReport.InsertVirtualComponent(AComponent: TrwComponent);
begin
  if AComponent is TrmPrintForm then
    inherited;
end;

function TrmReport.IsDesignMode: Boolean;
begin
  Result := (GetDesigner <> nil);
end;

function TrmReport.IsLicenced: Boolean;
begin
  Result := Assigned(FSecurityInfo) and (FSecurityInfo.Licence <> '');
end;

procedure TrmReport.Loaded;
var
  i: Integer;
begin
  inherited;

  for i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TrwInputFormContainer then
      FInputForm := TrwInputFormContainer(Components[i]);
  end;
end;

procedure TrmReport.LoadFromStream(AStream: TStream);
var
  P: Int64;
  h: String;
begin
  P := AStream.Position;
  h := StringOfChar(' ', 25);
  AStream.ReadBuffer(h[1], Length(h));
  if Pos(TrwSecureWrapper.ClassName, h) > 0 then
  begin
    AStream.Position := p;
    AStream.ReadComponent(SecurityInfo);
  end
  else
  begin
    AStream.Position := p;
    ReadReport(AStream);
  end;
end;

procedure TrmReport.ReadReport(AStream: TStream);
var
  Reader: TrwReader;
begin
  Reader := TrwReader.Create(AStream);
  try
//    Reader.OnSetName := ReaderSetName;
    InitializationSbVM;
    try
      Reader.ReadRootComponent(Self);
      FixUpUserProperties;
    finally
      DeInitializationSbVM;
    end;
  finally
    Reader.Free;
  end;
end;

procedure TrmReport.SaveToStream(AStream: TStream);
begin
  if IsLicenced then
    AStream.WriteComponent(SecurityInfo)
  else
    WriteReport(AStream);
end;

procedure TrmReport.WriteReport(AStream: TStream);
begin
  InitializationSbVM;
  try
    AStream.WriteComponent(Self);
  finally
    DeInitializationSbVM;
  end;
end;

procedure TrmReport.ReadCompiledCode(Stream: TStream);
var
  A: Variant;
  i: Integer;
begin
  Stream.ReadBuffer(i, SizeOf(i));
  A := StreamToVarArray(Stream);

  SetLength(FCompiledCode, VarArrayHighBound(A, 1) - VarArrayLowBound(A, 1) + 1);
  for i := Low(CompiledCode) to High(CompiledCode) do
  begin
    CompiledCode[i] := A[i];
    if VarType(CompiledCode[i]) = varOleStr then
      CompiledCode[i] := VarAsType(CompiledCode[i], varString);
  end;
end;

procedure TrmReport.ReadLibVersion(Reader: TReader);
begin
  FLibVersion := Reader.ReadDate;
end;

procedure TrmReport.WriteCompiledCode(Stream: TStream);
var
  A: Variant;
  i: Integer;
  MS: TStream;
begin
  A := VarArrayCreate([Low(CompiledCode), High(CompiledCode)], varVariant);
  for i := Low(CompiledCode) to High(CompiledCode) do
    A[i] := CompiledCode[i];

  MS := VarArrayToStream(A, nil, True);
  try
    MS.Position := 0;
    i := MS.Size;
    Stream.WriteBuffer(i, SizeOf(i));
    Stream.CopyFrom(MS, i);
  finally
    MS.Free;
  end
end;

procedure TrmReport.WriteLibVersion(Writer: TWriter);
begin
  Writer.WriteDate(FLibVersion);
end;

procedure TrmReport.GetReportParams(const AParams: PTrwReportParamList);
begin
  VariablesToParamList(GlobalVarFunc.Variables, AParams);
end;

procedure TrmReport.SetReportParams(const AParams: PTrwReportParamList);
begin
  ParamListToVariables(GlobalVarFunc.Variables, AParams);
end;

function TrmReport.ShowInputForm(AParent: HWND; ARunTime: Boolean): Boolean;
var
  R: Variant;
begin
  Result := False;
  if CanShowInputForm(ARunTime) then
  begin
    InputForm.FRunTimeMode := ARunTime;
    InputForm.CloseResult := mrNone;
    PrepareQueries(FInputForm);

    R := True;
    ExecuteEventsHandler('BeforeShowInputForm', [Integer(Addr(R))]);
    if R then
    begin
      if AParent = 0 then
        Result := ShowInFormContents(InputForm, '', nil)
      else
      begin
        InputForm.ShowInRunTime(AParent);
        Result := True;
      end;
    end;
  end;
end;


procedure TrmReport.BeginWork;
begin
  if FBeginWorkCount = 0 then
  begin
    InitializationSbVM;
    CreateRwVM(@FCompiledCode);

    if Assigned(DebugInfo) then
    begin
      DebugInfo.Report := Self;
      DebugInfo.SetBrkPointsAddr;
    end;
  end;

  Inc(FBeginWorkCount);
end;

procedure TrmReport.EndWork;
begin
  Dec(FBeginWorkCount);
  if FBeginWorkCount = 0 then
  begin
    DestroyRwVM;
    DeInitializationSbVM;
    if Assigned(DebugInfo) then
      DebugInfo.Report := nil;
  end
  else if FBeginWorkCount < 0 then
    raise ErwException.Create('Not balanced EndWork');
end;

procedure TrmReport.CloseInputForm;
begin
  InputForm.CloseInRunTime;
end;

procedure TrmReport.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventBeforePrintReport, cEventBeforeShowInputForm, cEventBeforeProcessForm,
                  cEventAfterProcessForm, cEventAfterPrintReport,  cEventAfterCloseInputForm ]);
end;


function TrmReport.CallCustomFunction(const AFunctionName: String; const AParams: array of Variant): Variant;
begin
  if SameText(AFunctionName, 'DeleteInputForm') then
  begin
    FreeAndNil(FInputForm);
    Result := Unassigned;
  end

  else if SameText(AFunctionName, 'Compile') then
    Compile(AParams[0])

  else
    Result := inherited CallCustomFunction(AFunctionName, AParams);
end;

procedure TrmReport.LoadFromFile(const AFileName: string);
begin
  LoadRWResFromFile(Self, AFileName);
end;

procedure TrmReport.SaveToFile(const AFileName: string);
begin
  SaveRWResToFile(Self, AFileName);
end;

procedure TrmReport.AddChildObject(AObject: IrmDesignObject);
begin
  if AObject.ObjectInheritsFrom('TrwInputFormContainer') then
  begin
    CheckMultipleInputFormSituation;
    FInputForm := TrwInputFormContainer(AObject.RealObject);
    inherited;
    FixInputFormComponentIndex;
  end

  else
    inherited;
end;

procedure TrmReport.CheckMultipleInputFormSituation;
begin
  if Assigned(FInputForm) then
    raise ErwException.Create('Multiple Input Forms are not supported yet');
end;

procedure TrmReport.FixInputFormComponentIndex;
begin
  if Assigned(GlobalVarFunc) then
    FInputForm.ComponentIndex := GlobalVarFunc.ComponentIndex + 1
  else
    FInputForm.ComponentIndex := 0;
end;

procedure TrmReport.RemoveChildObject(AObject: IrmDesignObject);
begin
  if AObject.RealObject = FInputForm then
    FInputForm := nil;

  inherited;
end;

function TrmReport.GetCompiledCode: PTrwCode;
begin
  Result := Addr(FCompiledCode);
end;

function TrmReport.GetDebInf: PTrwDebInf;
begin
  Result := Addr(FDebInf);
end;

function TrmReport.GetDebSymbInf: PTrwDebInf;
begin
  Result := Addr(FDebSymbInf);
end;


function TrmReport.CanShowInputForm(ARunTime: Boolean): Boolean;
begin
  Result := Assigned(InputForm) and InputForm.Visible and (InputForm.ComponentCount > 0) and
           (ARunTime or not InputForm.RunTimeOnly);
end;

procedure TrmReport.Prepare;
begin
  InitializationSbVM;
  try
    PrepareQueries(Self);
  finally
    DeInitializationSbVM;
  end;
end;

procedure TrmReport.AfterInitLibComponent;
begin
  inherited;
  SetLength(FCompiledCode, 0);
end;

function TrmReport.CreateChildObject(const AClassName: String): IrmDesignObject;
var
  C: TrwComponent;
  ObjName: String;
begin
  Result := inherited CreateChildObject(AClassName);
  C := TrwComponent(Result.RealObject);

  if IsDesignMode then
  begin
    if C is TrmPrintForm then
      ObjName := 'PrintForm'
    else if C is TrwInputFormContainer then
      ObjName := 'InputForm'
    else if C is TrmASCIIForm then
      ObjName := 'ASCIIForm'
    else if C is TrmXMLForm then
      ObjName := 'XMLForm'
    else
      ObjName := '';

    C.Name := GetUniqueNameForComponent(C, False, ObjName, '', Length(ObjName) > 0);

//    if not C.IsLibComponent then
//      C.InitializeForDesign;
  end;
end;


function TrmReport.DesignBehaviorInfo: TrwDesignBehaviorInfoRec;
begin
   Result := inherited DesignBehaviorInfo;
   Result.Selectable := True;
end;

{ TrmSecureWrapper }

procedure TrmSecureWrapper.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryProperty('Data', ReadData, WriteData, True); 
end;

procedure TrmSecureWrapper.ReadData(AStream: TStream);
var
  S1, S2: IEvDualStream;
  BF: TBlowfishCipherFixed;
begin
  S1 := TEvDualStreamHolder.Create;
  S2 := TEvDualStreamHolder.Create;

  BF := TBlowfishCipherFixed.CreateStrKey(RWEngineExt.AppAdapter.GetLicencePassword(Licence));
  try
    AStream.Position := 0;
    BF.DecryptStream(AStream, S1.RealStream);
  finally
    BF.Free;
  end;

  S1.Position := 0;
  InflateStream(S1.RealStream, S2.RealStream);

  S2.Position := 0;
  FReport.ReadReport(S2.RealStream);
end;

procedure TrmSecureWrapper.WriteData(AStream: TStream);
var
  S1, S2: IEvDualStream;
  BF: TBlowfishCipherFixed;
begin
  S1 := TEvDualStreamHolder.Create;
  S2 := TEvDualStreamHolder.Create;

  FReport.WriteReport(S1.RealStream);
  S1.RealStream.Position := 0;
  DeflateStream(S1.RealStream, S2.RealStream);

  BF := TBlowfishCipherFixed.CreateStrKey(RWEngineExt.AppAdapter.GetLicencePassword(Licence));
  try
    S2.Position := 0;
    BF.EncryptStream(S2.RealStream, AStream);
  finally
    BF.Free;
  end;
end;

end.
