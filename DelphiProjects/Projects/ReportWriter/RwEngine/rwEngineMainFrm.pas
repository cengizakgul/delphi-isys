unit rwEngineMainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CoolTrayIcon, Menus;

const WM_TERMINATE_EXECUTION = WM_USER + 1000;

type
  TrwEngineMain = class(TForm)
    PopupMenu: TPopupMenu;
    Close1: TMenuItem;
    procedure Close1Click(Sender: TObject);
  private
    procedure WMTerminateExecution(var Message: TMessage); message WM_TERMINATE_EXECUTION;
  public
    procedure UpdateTrayHint;
  end;

var
  rwEngineMain: TrwEngineMain;

implementation

uses rwEngineApp, rwUtils, rwEngine;

{$R *.dfm}

{ TrwEngineMain }

procedure TrwEngineMain.Close1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TrwEngineMain.UpdateTrayHint;
begin
{  TrayIcon.Hint := 'Report Writer Engine'#13 +
                   'Application: ' + AppAdapterInfo.ApplicationName + #13 +
                   'Location:    ' + AppAdapterInfo.ModuleName + #13 +
                   'Version:     ' + AppAdapterInfo.Version;}
end;

procedure TrwEngineMain.WMTerminateExecution(var Message: TMessage);
begin
  RWEngineExt.GetVM.Halt := True;
end;

end.
