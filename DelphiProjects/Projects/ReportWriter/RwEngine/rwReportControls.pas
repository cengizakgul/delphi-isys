unit rwReportControls;

interface

uses
  Windows, Classes, Types, StdCtrls, ExtCtrls, rwCommonClasses, rwDB, rwBasicClasses,
  Forms, Sysutils, rwRendering, Graphics, rwPrintElements, Controls, rwPCLCanvas,
  rwReport, rwTypes, rwUtils, Variants, evStreamUtils, rwQBInfo, rwEngineTypes,
  rwGraphics, rmTypes, rwBasicUtils;

const
  cMinTableCellWidth = 20;
  cMinTableCellHeight = 15;

type

  {TrwLabel is static text}

  TrwLabel = class(TrwCustomText)
  private
    FFormat: string;
    FValue: Variant;
    FTextByValue: Boolean;
    FSettingValue: Boolean;
    procedure SetFormat(const Value: string);
    procedure SetTextByValue(const Value: Boolean);
    function  TextIsNotByValue: Boolean;
    function  FormatIsNotEmpty: Boolean;

  protected
    procedure SetValue(const Value: Variant); virtual;
    procedure SetText(const Value: string); override;
    procedure SetName(const NewName: TComponentName); override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure   InitializeForDesign; override;

  published
    property TextByValue: Boolean read FTextByValue write SetTextByValue;
    property Format: string read FFormat write SetFormat stored FormatIsNotEmpty;
    property Value: Variant read FValue write SetValue;
    property Text stored TextIsNotByValue;
  end;


  {TrwDBText is text from database}

  TrwDBText = class(TrwCustomText)
  private
    FDataSource: TrwDataSet;
    FDataField: string;
    FFormat: string;
    FSubstitution: Boolean;
    FEmptyText: string;
    FSubstID: string;

    procedure SetDataSource(Value: TrwDataSet);
    procedure SetDataField(Value: string);
    procedure SetSubstID(Value: string);
    procedure SetFormat(Value: string);
    procedure SetSubstitution(Value: Boolean);
    function  GetValue: Variant;
    function  EmptyTextIsNotEmpty: Boolean;
    function  FormatIsNotEmpty: Boolean;
    function  SubstIDIsNotEmpty: Boolean;
    procedure SetTextFromDataSource;

  protected
    function  GetText: string; override;
    procedure SetName(const NewName: TComponentName); override;
    procedure RegisterComponentEvents; override;
    function  CreateVisualControl: IrwVisualControl; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;

  published
    property SubstID: String read FSubstID write SetSubstID stored SubstIDIsNotEmpty;
    property Substitution: Boolean read FSubstitution write SetSubstitution;
    property DataSource: TrwDataSet read FDataSource write SetDataSource;
    property DataField: string read FDataField write SetDataField;
    property Format: string read FFormat write SetFormat stored FormatIsNotEmpty;
    property EmptyText: string read FEmptyText write FEmptyText stored EmptyTextIsNotEmpty;
    property Value: Variant read GetValue stored False;
    property Text: String read GetText stored False;
  end;



  {TrwSysLabel is system information}

  TrwSysLabel = class(TrwCustomText)
  private
    FTypeSysInfo: TrwTypeSysInfo;
    FFormat: string;
    procedure SetFormat(Value: string);
    function  GetTextByType: String;
    function  FormatIsNotEmpty: Boolean;
    procedure SetTypeSysInfo(Value: TrwTypeSysInfo);

  protected
    function  GetText: string; override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Text stored False;
    property TypeSysInfo: TrwTypeSysInfo read FTypeSysInfo write SetTypeSysInfo;
    property Format: string read FFormat write SetFormat stored FormatIsNotEmpty;
  end;


    {TrwShape is rectangle, circle etc.}

  TrwShape = class(TrwChildComponent)
  private
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    FShapeType: TShapeType;
    FTransparent: Boolean;

    procedure SetShapeType(Value: TShapeType);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
    procedure SetTransparent(const Value: Boolean);

  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Color;
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle;
    property ShapeType: TShapeType read FShapeType write SetShapeType;
    property Transparent: Boolean read FTransparent write SetTransparent;
  end;


    {TrwFrame is graphic frame}

  TrwFrame = class(TrwChildComponent)
  private
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;
  public
    constructor Create(AOwner: TComponent); override;

  published
    property Align;
    property Color stored False;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines;
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle;
  end;

  {TrwContainer is like TPanel for Form}

  TrwContainer = class(TrwFramedContainer)
  private
    FSubReport: TrwSubReport;
    procedure SetSubReport(const Value: TrwSubReport);
    procedure SetSubReportPaper;

  protected
    procedure SetPosition(Index: Integer; Value: Integer); override;
    procedure SetAutoHeight(Value: Boolean); override;
    procedure SetAutoWidth(Value: Boolean); override;
    procedure PrintGraphic; override;
    procedure PrintASCII; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

  published
    property Align;
    property AutoWidth;
    property SubReport: TrwSubReport read FSubReport write SetSubReport;
  end;


  TrwCustomImage = class(TrwChildComponent)
  private
    FPicture: TrwPicture;
    FStretch: Boolean;
    FTransparent: Boolean;
    FBoundLines: TrwSetColumnLines;
    FBoundLinesColor: TColor;
    FBoundLinesWidth: Integer;
    FBoundLinesStyle: TPenStyle;
    procedure SetPicture(Value: TrwPicture);
    procedure SetBoundLines(Value: TrwSetColumnLines);
    procedure SetBoundLinesColor(Value: TColor);
    procedure SetBoundLinesWidth(Value: Integer);
    procedure SetBoundLinesStyle(Value: TPenStyle);
  protected
    procedure SetStretch(Value: Boolean); virtual;
    procedure SetTransparent(Value: Boolean); virtual;
    procedure OnChangePicture(Sender: TObject); virtual;
    procedure CheckSize; virtual;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); override;    
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property Picture: TrwPicture read FPicture write SetPicture;
    property Align;
    property Stretch: Boolean read FStretch write SetStretch;
    property BoundLines: TrwSetColumnLines read FBoundLines write SetBoundLines;
    property BoundLinesColor: TColor read FBoundLinesColor write SetBoundLinesColor;
    property BoundLinesWidth: Integer read FBoundLinesWidth write SetBoundLinesWidth;
    property BoundLinesStyle: TPenStyle read FBoundLinesStyle write SetBoundLinesStyle;
    property Transparent: Boolean read FTransparent write SetTransparent;
  end;


    {TrwImage is static picture}

  TrwImage = class(TrwCustomImage)
  private
    FPCLFile: TrwPCLFile;
    FPCLPicturePrepared: Boolean;
    FPreparingPCLPicture: Boolean;
    FPCLSize: TPoint;
    procedure SetPCLFile(Value: TrwPCLFile);
    procedure OnChangePCL(Sender: TObject);
    function  PictureIsNotEmpty: Boolean;
    function  PCLFileIsNotEmpty: Boolean;
  protected
    procedure SetStretch(Value: Boolean); override;
    procedure SetTransparent(Value: Boolean); override;
    procedure CheckSize; override;
    procedure OnChangePicture(Sender: TObject); override;
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Loaded; override;

  public
    property PCLSize: TPoint read FPCLSize;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    PreparePCLPicture: TMetafile;

  published
    property Picture stored PictureIsNotEmpty;
    property PCLFile: TrwPCLFile read FPCLFile write SetPCLFile stored PCLFileIsNotEmpty;
  end;


  TrwDBImage = class(TrwCustomImage)
  private
    FDataSource: TrwDataSet;
    FDataField: string;
    procedure SetDataSource(Value: TrwDataSet);
    procedure SetDataField(Value: string);
    procedure GetImageFromDataSource;
  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure PrintGraphic; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation); override;
  published
    property Picture stored False;
    property DataSource: TrwDataSet read FDataSource write SetDataSource;
    property DataField: string read FDataField write SetDataField;
  end;


  TrwPCLLabel = class(TrwLabel)
  private
    FInitString: String;
    FPCLFont: TrwPCLFile;
    FPCLPicturePrepared: Boolean;
    FPCLSize: TPoint;
    FImage: TMetaFile;
    procedure SetPCLFont(Value: TrwPCLFile);
    procedure OnChangePCL(Sender: TObject);
  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure SetText(const Value: string); override;
    procedure PrintGraphic; override;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure Loaded; override;
    procedure CheckAutosize; override;
  public
    property PCLSize: TPoint read FPCLSize;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    function    PrepareImage(PCalcRect: PRect; AReferenceDevice: HDC = 0): TMetaFile;
    procedure InitializeForDesign; override;
  published
    property Autosize stored False;
    property Alignment stored False;
    property BoundLines stored False;
    property BoundLinesColor stored False;
    property BoundLinesStyle stored False;
    property BoundLinesWidth stored False;
    property Color stored False;
    property DigitNet stored False;
    property Font stored False;
    property Layout stored False;
    property Transparent stored False;
    property WordWrap stored False;

    property PCLFont: TrwPCLFile read FPCLFont write SetPCLFont;
    property InitString: String read FInitString write FInitString stored False;
  end;


  {Bar codes}

  TrwBarcode1D = class(TrwCustomText)
  private
    FBarHeight: Integer;
    FBarType: Trw1DBarType;
    FActualSize: TSize;
    FBarColor: TColor;
    FShowText: Boolean;
    FCheckCharacter: Boolean;
    FPostnetHeightTallBar: Integer;
    FPostnetHeightShortBar: Integer;
    FSupplementalCode: String;
    FGuardBars: Boolean;
    FUPCEANSupplement5: Boolean;
    FUPCEANSupplement2: Boolean;
    FUPCESytem: String;
    FCODABARStartChar: String;
    FCODABARStopChar: String;
    FCode128Set: String;
    FSupSeparation: Integer;
    FVerticalOrientation: boolean;
    FTopMargin: integer;
    FLeftMargin: integer;
    procedure SetBarHeight(const AValue: Integer);
    procedure SetBarType(const AValue: Trw1DBarType);
    procedure SetBarColor(const AValue: TColor);
    procedure SetShowText(const AValue: Boolean);
    procedure SetCheckCharacter(const Value: Boolean);
    procedure SetPostnetHeightTallBar(const Value: Integer);
    procedure SetPostnetHeightShortBar(const Value: Integer);
    procedure SetSupplementalCode(const Value: String);
    procedure SetGuardBars(const Value: Boolean);
    procedure SetUPCEANSupplement2(const Value: Boolean);
    procedure SetUPCEANSupplement5(const Value: Boolean);
    procedure SetUPCESytem(const Value: String);
    procedure SetCODABARStartChar(const Value: String);
    procedure SetCODABARStopChar(const Value: String);
    procedure SetCode128Set(const Value: String);
    procedure SetSupSeparation(const Value: Integer);
    procedure SetVerticalOrientation(const Value: boolean);
    procedure SetTopMargin(const Value: integer);
    procedure SetLeftMargin(const Value: integer);
  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure CheckAutosize; override;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure PrintGraphic; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;
  published
    property Alignment stored False;
    property WordWrap stored False;
    property Layout stored False;
    property LettersCase stored False;
    property BoundLines stored False;
    property BoundLinesColor stored False;
    property BoundLinesWidth stored False;
    property BoundLinesStyle stored False;
    property DigitNet stored False;
	  property BarType: Trw1DBarType read FBarType write SetBarType;
    property BarHeight: Integer read FBarHeight write SetBarHeight default 40;
	  property BarColor: TColor read FBarColor write SetBarColor default clBlack;
    property ShowText: Boolean read FShowText write SetShowText default True;
	  property CheckCharacter: Boolean read FCheckCharacter write SetCheckCharacter default True; // if true, the checksum character will be calculated and appended to the code
    property PostnetHeightTallBar: Integer read FPostnetHeightTallBar write SetPostnetHeightTallBar default 20;  // height in pixels of POSTNET's tall bars
    property PostnetHeightShortBar: Integer read FPostnetHeightShortBar write SetPostnetHeightShortBar default 10; // height in pixels of POSTNET's short bars
    property SupplementalCode: String read FSupplementalCode write SetSupplementalCode;  // user defined supplement, barcode suplement, can be 2 or 5 digits (for EAN and UPC only)
    property GuardBars: Boolean read FGuardBars write SetGuardBars default True;  // if true guardbars in EAN and UPC codes will be longer than data bars
    property UPCEANSupplement2: Boolean read FUPCEANSupplement2 write SetUPCEANSupplement2; // 2 digit supplement for EAN or UPC codes
    property UPCEANSupplement5: Boolean read FUPCEANSupplement5 write SetUPCEANSupplement5; // 5 digit supplement for EAN or UPC codes
    property UPCESytem: String read FUPCESytem write SetUPCESytem; // system to be used in UPCE
    property CODABARStartChar: String read FCODABARStartChar write SetCODABARStartChar;  // start character for CODABAR
    property CODABARStopChar:String read FCODABARStopChar write SetCODABARStopChar;      // stop character for CODABAR
    property Code128Set: String read FCode128Set write SetCode128Set;                    // set of character to be used in code 128. Possible values are "A", "B" (default) or "C" (only for numeric codes)
    property SupSeparation: Integer read FSupSeparation write SetSupSeparation default 10;          // separation in CN between the barcode and the supplement
    property VerticalOrientation: boolean read FVerticalOrientation write SetVerticalOrientation default False;  // if true, barcode orientation is vertical
    property TopMargin: integer read FTopMargin write SetTopMargin default 10;
    property LeftMargin: integer read FLeftMargin write SetLeftMargin default 10;
  end;

  TrwBarcodePDF417 = class(TrwCustomText)
  private
    FActualSize: TSize;
    FBarColor: TColor;
    FBarHeightPixels: integer;
    FBarWidthPixels: integer;
    FTopMargin: integer;
    FLeftMargin: integer;
    FColumnsNumber: integer;
    FRowsNumber: integer;
    FMaxRows: integer;
    FECLevel: integer;
    FEncodingMode: TrwPDF417BarEncoding;
    FTruncated: boolean;
    FHorizontalPixelShaving: integer;
    FVerticalPixelShaving: integer;
    procedure SetBarColor(const AValue: TColor);
    procedure SetBarHeightPixels(const AValue: Integer);
    procedure SetBarWidthPixels(const AValue: integer);
    procedure SetTopMargin(const AValue: integer);
    procedure SetLeftMargin(const AValue: integer);
    procedure SetRowsNumber(const AValue: integer);
    procedure SetColumnsNumber(const AValue: integer);
    procedure SetMaxRows(const AValue: integer);
    procedure SetECLevel(const AValue: integer);
    procedure SetEncodingMode(const AValue: TrwPDF417BarEncoding);
    procedure SetTruncated(const AValue: boolean);
    procedure SetHorizontalPixelShaving(const AValue: integer);
    procedure SetVerticalPixelShaving(const AValue: integer);
  protected
    function  CreateVisualControl: IrwVisualControl; override;
    procedure CheckAutosize; override;
    procedure ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure PrintGraphic; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure InitializeForDesign; override;
  published
    property Alignment stored False;
    property WordWrap stored False;
    property Layout stored False;
    property LettersCase stored False;
    property BoundLines stored False;
    property BoundLinesColor stored False;
    property BoundLinesWidth stored False;
    property BoundLinesStyle stored False;
    property DigitNet stored False;
	  property BarColor: TColor read FBarColor write SetBarColor default clBlack;
    property BarHeightPixels: Integer read FBarHeightPixels write SetBarHeightPixels default 7;
    property BarWidthPixels: Integer read FBarWidthPixels write SetBarWidthPixels default 1;
    property TopMargin: integer read FTopMargin write SetTopMargin default 10;
    property LeftMargin: integer read FLeftMargin write SetLeftMargin default 10;
    property ColumnsNumber: integer read FColumnsNumber write SetColumnsNumber default 10;
    property RowsNumber: integer read FRowsNumber write SetRowsNumber default 0;
    property MaxRows: integer read FMaxRows write SetMaxRows default 0;
    property ECLevel: integer read FECLevel write SetECLevel default 0;
    property EncodingMode: TrwPDF417BarEncoding read FEncodingMode write SetEncodingMode;
    property Truncated: boolean read FTruncated write SetTruncated default false;
    property HorizontalPixelShaving: integer read FHorizontalPixelShaving write SetHorizontalPixelShaving default 0;
    property VerticalPixelShaving: integer read FVerticalPixelShaving write SetVerticalPixelShaving default 0;
  end;

implementation

uses rwEngine, rwDsgnControls, Barcode1D, PDF417;

  {TrwLabel}

constructor TrwLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FValue := '';
  FTextByValue := False;
  FSettingValue := False;
end;

procedure TrwLabel.SetName(const NewName: TComponentName);
var
  fl: Boolean;
begin
  if FTextByValue then
    fl := (Name <> '') and AnsiSameText(VarToStr(FValue), Name)
  else
    fl := (Name <> '') and AnsiSameText(Text, Name);

  inherited;

  if fl then
    if FTextByValue then
      Value := Name
    else
      Text := Name;
end;

procedure TrwLabel.SetFormat(const Value: string);
begin
  FFormat := Value;
  SetValue(FValue);
end;

procedure TrwLabel.SetValue(const Value: Variant);
begin
  FValue := Value;
  if FTextByValue then
  begin
    FSettingValue := True;
    try
      Text := FormatValue(FFormat, FValue);
    finally
      FSettingValue := False;
    end;  
  end;
end;

procedure TrwLabel.SetTextByValue(const Value: Boolean);
begin
  FTextByValue := Value;
  if IsDesignMode and Value and (FValue = '') then
    FValue := Name;
  SetValue(FValue);
end;

function TrwLabel.TextIsNotByValue: Boolean;
begin
  if (csWriting in ComponentState) then
    Result := not FTextByValue or InheritedComponent or IsLibComponent
  else
    Result := True;
end;

function TrwLabel.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFramedLabelControl.Create(Self);
end;

function TrwLabel.FormatIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(Format) > 0);
end;

procedure TrwLabel.InitializeForDesign;
begin
  inherited;
  TextByValue := True;
end;

procedure TrwLabel.SetText(const Value: string);
begin
  if FSettingValue or not TextByValue then
    inherited;
end;

{TrwDBText}

constructor TrwDBText.Create(AOwner: TComponent);
begin
  inherited;
  FSubstitution := False;
end;

procedure TrwDBText.RegisterComponentEvents;
begin
  inherited;
  RegisterEvents([cEventOnSetText]);
end;

function TrwDBText.GetText: string;
begin
  Result := inherited GetText;
end;

procedure TrwDBText.SetFormat(Value: string);
begin
  FFormat := Value;

  if Assigned(FDataSource) and Boolean(FDataSource.PActive) and (FDataField <> '') then
    SetTextFromDataSource;
end;

destructor TrwDBText.Destroy;
begin
  DataSource := nil;
  inherited;
end;

procedure TrwDBText.SetDataSource(Value: TrwDataSet);
begin
  if FDataSource = Value then
    Exit;

  if Assigned(FDataSource) then
    FDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FDataSource := Value;

  if not ((csLoading in ComponentState) or Loading or (csDestroying in ComponentState)) and
    (not Assigned(FDataSource) or Assigned(FDataSource) and Boolean(FDataSource.PActive)) then
    SetTextFromDataSource;
end;

procedure TrwDBText.RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation);
begin
  inherited;

  if Sender = FDataSource then
    case AOperation of
      rnoDataSetChanged: SetTextFromDataSource;

      rnoDestroy: DataSource := nil;
    end;
end;

procedure TrwDBText.SetTextFromDataSource;
var
  h, TableName: string;
  v, sbstVal: Variant;
begin
  if Assigned(FDataSource) and Boolean(FDataSource.PActive) and (FDataField <> '') then
  begin
    v := FDataSource.GetFieldValue(FDataField);

    if FSubstitution then
    begin
      if Pos('.', FSubstID) <> 0 then
      begin
        h := FSubstID;
        TableName := GetNextStrValue(h, '.');
        sbstVal := rwDictionary.PGetFieldValueDescription(TableName, h, v);
      end
      else
        sbstVal := RWEngineExt.AppAdapter.SubstituteValue(v, FSubstID);

      h := VarToStr(FormatValue(FFormat, sbstVal));
      if h = '' then
        h := FormatValue(FFormat, v);
    end
    else
    begin
      h := FormatValue(FFormat, v);
      if h = '' then
        h := EmptyText;
    end;

    if h = '' then
      h := FormatValue(FFormat, v);

    FText := '';
    v := h;
    ExecuteEventsHandler('OnSetText', [Integer(Addr(v))]);
    h := VarToStr(v);

    if IsDesignMode and (h = '') then
      h := '<empty>';

    SetText(h);
  end
  else
    if IsDesignMode then
      SetText(Name)
    else
      SetText(EmptyText);
end;

function TrwDBText.GetValue: Variant;
begin
  if Assigned(FDataSource) and Boolean(FDataSource.PActive) and (FDataField <> '') then
    Result := FDataSource.GetFieldValue(FDataField)
  else
    Result := Null;
end;

procedure TrwDBText.SetName(const NewName: TComponentName);
begin
  inherited;
  SetTextFromDataSource;
end;

procedure TrwDBText.SetSubstitution(Value: Boolean);
begin
  if FSubstitution <> Value then
  begin
    FSubstitution := Value;
    SetTextFromDataSource;
  end;
end;

procedure TrwDBText.SetDataField(Value: string);
begin
  if (Value <> FDataField) then
  begin
    FDataField := Value;
    SetTextFromDataSource;
  end;
end;

procedure TrwDBText.SetSubstID(Value: string);
begin
  FSubstID := Value;
  SetTextFromDataSource;  
end;

function TrwDBText.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFramedLabelControl.Create(Self);
  SetText(Name);
end;

function TrwDBText.EmptyTextIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(FEmptyText) > 0);
end;

function TrwDBText.FormatIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(Format) > 0);
end;

function TrwDBText.SubstIDIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(FSubstID) > 0);
end;


{TrwSysLabel}

constructor TrwSysLabel.Create(AOwner: TComponent);
begin
  inherited;
  FFormat := 'Page #';
  TypeSysInfo := rsiPageNum;
end;

function TrwSysLabel.GetText: string;
begin
  Result := GetTextByType;
end;

procedure TrwSysLabel.SetTypeSysInfo(Value: TrwTypeSysInfo);
begin
  FTypeSysInfo := Value;

  if not(csLoading in ComponentState) then
    case FTypeSysInfo of
      rsiPageNum:  Format := 'Page #';
      rsiDate:     Format := 'dd/mm/yyyy';
      rsiTime:     Format := 'hh:mm:ss';
      rsiDateTime: Format := 'dd/mm/yyyy hh:mm:ss';
    end;
  Text := GetTextByType;  
end;

function TrwSysLabel.GetTextByType: String;
var
  FPageNum: Integer;
begin
  if IsDesignMode or not Assigned(FRendering) then
    FPageNum := 1
  else
    FPageNum := TrwRendering(FRendering).RenderingPaper.Header.PagesCount;

  case FTypeSysInfo of
    rsiPageNum:  Result := FormatValue(FFormat, FPageNum);
    rsiDate:     Result := FormatValue(FFormat, Date);
    rsiTime:     Result := FormatValue(FFormat, Time);
    rsiDateTime: Result := FormatValue(FFormat, Now);
  else
    Result := '';
  end;
end;

procedure TrwSysLabel.SetFormat(Value: string);
begin
  FFormat := Value;
  Text := GetTextByType;
end;

function TrwSysLabel.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFramedLabelControl.Create(Self);
end;

function TrwSysLabel.FormatIsNotEmpty: Boolean;
begin
  Result := InheritedComponent or IsLibComponent or not (csWriting in ComponentState)
            or (Length(Format) > 0);
end;


procedure TrwSysLabel.PrintGraphic;
begin
  Text := GetTextByType;
  inherited;
end;

{TrwShape}

constructor TrwShape.Create(AOwner: TComponent);
begin
  inherited;
  Width := 50;
  Height := 50;
  FShapeType := stRectangle;
  Color := clWhite;
  FBoundLinesColor := clBlack;
  FBoundLinesWidth := 1;
  FBoundLinesStyle := psSolid;
  FTransparent := False;
end;

procedure TrwShape.SetShapeType(Value: TShapeType);
begin
  FShapeType := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwShape.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwShape.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwShape.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

function TrwShape.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwShapeControl.Create(Self);
end;

procedure TrwShape.SetTransparent(const Value: Boolean);
begin
  FTransparent := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwShape.PrintGraphic;
var
  lPrnCtrl: TExtShape;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  lPrnCtrl := TExtShape.Create(lRendering.RenderingPaper);
  lPrnCtrl.Brush.Color := Color;
  if Transparent then
    lPrnCtrl.Brush.Style := bsClear
  else
    lPrnCtrl.Brush.Style := bsSolid;
  lPrnCtrl.Pen.Color := BoundLinesColor;
  lPrnCtrl.Pen.Width := BoundLinesWidth;
  lPrnCtrl.Pen.Style := BoundLinesStyle;
  lPrnCtrl.Shape := ShapeType;
  lPrnCtrl.LayerNumber := LayerNumber;
  lPrnCtrl.Compare := Compare;
  CalcPosRelativeBand(lPrnCtrl);
  lPrnCtrl.Parent := lRendering.RenderingPaper;
end;


{TrwFrame}

constructor TrwFrame.Create(AOwner: TComponent);
begin
  inherited;
  Width := 70;
  Height := 50;
  FBoundLines := [rclLeft, rclRight, rclTop, rclBottom];
  FBoundLinesColor := clBlack;
  FBoundLinesWidth := 1;
  FBoundLinesStyle := psSolid;
  DsgnShowCorners := True;
end;

procedure TrwFrame.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwFrame.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwFrame.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwFrame.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

function TrwFrame.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwFrameControl.Create(Self);
end;

procedure TrwFrame.PrintGraphic;
var
  lPrnCtrl: TGraphicFrame;
  lRendering: TrwRendering;
begin
  if BoundLines <> [] then
  begin
    lRendering := TrwRendering(FRendering);
    lPrnCtrl := TGraphicFrame.Create(lRendering.RenderingPaper);
    lPrnCtrl.Brush.Color := Color;
    lPrnCtrl.Transparent := True;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    CalcPosRelativeBand(lPrnCtrl);
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end;
end;



{TrwImage}

constructor TrwImage.Create(AOwner: TComponent);
begin
  inherited;
  FPCLFile := TrwPCLFile.Create;
  FPCLFile.OnChange := OnChangePCL;
  FPCLPicturePrepared := False;
  FPreparingPCLPicture := False;
end;

destructor TrwImage.Destroy;
begin
  FreeAndNil(FPCLFile);
  inherited;
end;

procedure TrwImage.SetPCLFile(Value: TrwPCLFile);
begin
  FPCLFile.Assign(Value);
end;


procedure TrwImage.SetStretch(Value: Boolean);
begin
  if PCLFile.Data <> '' then
    Value := False;
  inherited SetStretch(Value);
end;

procedure TrwImage.SetTransparent(Value: Boolean);
begin
  if PCLFile.Data <> '' then
    Value := False;
  inherited SetTransparent(Value);
end;

procedure TrwImage.OnChangePCL(Sender: TObject);
begin
  if (PCLFile.Data <> '') and not (csLoading in Owner.ComponentState) then
  begin
    FStretch := False;
    BoundLines := [];
    BoundLinesColor := clBlack;
    BoundLinesWidth := 1;
    BoundLinesStyle := psSolid;
    FPCLPicturePrepared := False;
    FTransparent := True;
    PreparePCLPicture;
    CheckSize;
    DoSyncVisualControl(rwCPInvalidate);
  end;
end;

function TrwImage.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwPCLImageControl.Create(Self);
end;

function TrwImage.PictureIsNotEmpty: Boolean;
begin
  if csWriting in ComponentState then
    Result := (PCLFile.Data = '') and Assigned(Picture.Graphic) and not Picture.Graphic.Empty
  else
    Result := True;
end;

procedure TrwImage.PrintGraphic;
var
  lPrnCtrl: TFrameImage;
  lRendering: TrwRendering;
begin
  if (PCLFile.Data <> '') or Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
  begin
    lRendering := TrwRendering(FRendering);
    lPrnCtrl := TFrameImage.Create(lRendering.RenderingPaper);
    if PCLFile.Data <> '' then
    begin
      lPrnCtrl.PCLData.Assign(PCLFile);
      lPrnCtrl.SetPCLSize(Width, Height);
    end
    else
      lPrnCtrl.Picture.Assign(Picture);
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.Autosize := False;
    CalcPosRelativeBand(lPrnCtrl);
    lPrnCtrl.Stretch := Stretch;
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end;
end;

function TrwImage.PCLFileIsNotEmpty: Boolean;
begin
  if csWriting in ComponentState then
    Result := PCLFile.Data <> ''
  else
    Result := True;
end;

function TrwImage.PreparePCLPicture: TMetafile;
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  k: Extended;
begin
  FPreparingPCLPicture := True;
  try
    Result := Picture.Metafile;

    if not FPCLPicturePrepared then
    begin
      C := TrwPCLCanvas.Create;
      try
        FPCLPicturePrepared := True;
        S := TIsMemoryStream.Create;
        try
          S.WriteBuffer(FPCLFile.Data[1], Length(FPCLFile.Data));
          Result.Clear;
          Result.Width := 0;
          Result.Height := 0;
          C.PaintTo(S, Result, Point(0, 0), cVirtualCanvasRes);
          k := cScreenCanvasRes /cVirtualCanvasRes;
          FPCLSize.X := Round(Result.Width * k);
          FPCLSize.Y := Round(Result.Height * k);

          if not IsDesignMode then
          begin
            Result.Clear;
            FPCLPicturePrepared := False;
          end;
        finally
          FreeAndNil(S);
        end;

      finally
        FreeAndNil(C);
      end;
    end;

  finally
    FPreparingPCLPicture := False;
  end;
end;


procedure TrwImage.OnChangePicture(Sender: TObject);
begin
  if not FPreparingPCLPicture then
  begin
    PCLFile.Data := '';
    inherited;
  end;
end;

procedure TrwImage.ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer);
begin
  if Assigned(PCLFile) and (PCLFile.Data <> '') then
  begin
    if Align in [alNone, alLeft, alRight] then
      AWidth := FPCLSize.X;
    if Align in [alNone, alTop, alBottom] then
      AHeight := FPCLSize.Y;
    FStretch := True;
    inherited;
    FStretch := False;
  end
  else
    inherited;
end;

procedure TrwImage.CheckSize;
begin
  if FPCLFile.Data <> '' then
    SetBounds(Left, Top, FPCLSize.X, FPCLSize.Y)
  else
    inherited;
end;

procedure TrwImage.Loaded;
begin
  inherited;
  if PCLFile.Data <> '' then
  begin
    FPCLSize.X := Width;
    FPCLSize.Y := Height;

    if (FPCLSize.X = 0) or (FPCLSize.Y = 0) then  // in case if PCL size has screwed up
    begin
      PreparePCLPicture;
      CheckSize;      
    end;
  end;
end;

{TrwPCLLabel}

procedure TrwPCLLabel.SetPCLFont(Value: TrwPCLFile);
begin
  FPCLFont.Assign(Value);
end;

function TrwPCLLabel.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwPCLLabelControl.Create(Self);
end;

procedure TrwPCLLabel.SetText(const Value: string);
begin
  FPCLPicturePrepared := False;
  inherited;
  Transparent := (FText <> '') and (PCLFont.Data <> '');
end;

procedure TrwPCLLabel.OnChangePCL(Sender: TObject);
begin
  FPCLPicturePrepared := False;
  CheckAutosize;
  Transparent := (FText <> '') and (PCLFont.Data <> '');  
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwPCLLabel.PrintGraphic;
var
  lPrnCtrl: TPCLLabel;
  lRendering: TrwRendering;
begin
  if Length(Text) > 0 then
  begin
    lRendering := TrwRendering(FRendering);
    lPrnCtrl := TPCLLabel.Create(lRendering.RenderingPaper);
    lPrnCtrl.Caption := Text;
    lPrnCtrl.PCLFont := PCLFont;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    lPrnCtrl.SetPCLSize(Width, Height);
    CalcPosRelativeBand(lPrnCtrl);

    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end;
end;



constructor TrwPCLLabel.Create(AOwner: TComponent);
begin
  inherited;
  FImage := TMetaFile.Create;
  FImage.Enhanced := True;

  FPCLFont := TrwPCLFile.Create;
  FPCLFont.OnChange := OnChangePCL;
  FPCLPicturePrepared := False;
  Autosize := True;
end;

destructor TrwPCLLabel.Destroy;
begin
  FreeAndNil(FImage);
  FreeAndNil(FPCLFont);
  inherited;
end;

function TrwPCLLabel.PrepareImage(PCalcRect: PRect; AReferenceDevice: HDC): TMetaFile;
var
  C: TrwPCLCanvas;
  S: TMemoryStream;
  R:TRect;
  k: Extended;
  lText: String;
  P: TPoint;
  fID, h: String;
  i: Integer;
  CalcC: TCanvas;
  flCalc: Boolean;
begin
  Result := FImage;

  flCalc := Assigned(PCalcRect);

  if FPCLPicturePrepared then
  begin
    if flCalc then
    begin
      PCalcRect^ := GetClientRect;
      FPCLSize := PCalcRect^.BottomRight;
    end;
    Exit;
  end;

  Result.Clear;
  lText := GetTextForPrint;

  if (Length(lText) = 0) or (PCLFont.Data = '') then
  begin
    if flCalc then
    begin
      PCalcRect^ := Rect(0, 0, 70, 30);
      FPCLSize := PCalcRect^.BottomRight;
    end;
    Exit;
  end;

  C := TrwPCLCanvas.Create;
  try
    S := TIsMemoryStream.Create;
    try
      C.NoMargins := True;

      if Copy(PCLFont.Data, 1, 3) <> #27'*c' then
      begin
        fID := '21002';
        h := #27'*c' + fID + 'D';
        S.WriteBuffer(h[1], Length(h));
      end
      else
      begin
        fID := '';
        i := Pos('D', PCLFont.Data);
        fID := Copy(PCLFont.Data, 4, i - 4);
      end;

      S.WriteBuffer(PCLFont.Data[1], Length(PCLFont.Data));
      h := #27'*c' + fID + 'D'#27'*c5F'#27'(' + fID + 'X';
      S.WriteBuffer(h[1], Length(h));
      S.WriteBuffer(lText[1], Length(lText));

      CalcC := TMetafileCanvas.Create(Result, AReferenceDevice);
      try
        R := C.CalcTextRect(S, CalcC, cVirtualCanvasRes);
      finally
        FreeAndNil(CalcC);
      end;

      Result.Clear;
      Result.Width := R.Right - R.Left;
      Result.Height := R.Bottom - R.Top;

      case C.PrimaryFont.SoftFont.Header.Orientation of
        0, 2: P := Point(0, Result.Height);
        1, 3: P := Point(Result.Width, Result.Height);
      end;

      if not flCalc then
        C.PaintTo(S, Result, P, cVirtualCanvasRes);

      k := cScreenCanvasRes / cVirtualCanvasRes;

      if flCalc then
      begin
        PCalcRect^.Left := 0;
        PCalcRect^.Top := 0;
        PCalcRect^.Right := Round((R.Right - R.Left) * k);
        PCalcRect^.Bottom := Round((R.Bottom - R.Top) * k);

        if PCalcRect^.Right = 0 then
          PCalcRect^.Right := PCalcRect^.Left + 70;
        if PCalcRect^.Bottom = 0 then
          PCalcRect^.Bottom := PCalcRect^.Top + 30;

        FPCLSize := PCalcRect^.BottomRight;
      end;

   finally
      S.Free;
    end;
  finally
    C.Free;
  end;

  if not IsDesignMode or flCalc then
  begin
    Result.Clear;
    FPCLPicturePrepared := False;
  end
  else
    FPCLPicturePrepared := True;
end;

procedure TrwPCLLabel.InitializeForDesign;
begin
  inherited;
  Value := '';
end;


procedure TrwPCLLabel.ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer);
begin
  if Align in [alNone, alLeft, alRight] then
    AWidth := FPCLSize.X;
  if Align in [alNone, alTop, alBottom] then
    AHeight := FPCLSize.Y;
  inherited;
end;

procedure TrwPCLLabel.Loaded;
begin
  inherited;
  CheckAutosize;
end;

procedure TrwPCLLabel.CheckAutosize;
var
  R: TRect;
begin
  R := Rect(0, 0, 0, 0);
  PrepareImage(@R, 0);
  SetBounds(Left, Top, R.Right - R.Left, R.Bottom - R.Top);
end;


{TrwDBImage}

constructor TrwDBImage.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TrwDBImage.Destroy;
begin
  DataSource := nil;
  inherited;
end;

procedure TrwDBImage.SetDataField(Value: string);
begin
  if (Value <> FDataField) then
  begin
    FDataField := Value;
    GetImageFromDataSource;
  end;
end;

procedure TrwDBImage.GetImageFromDataSource;
var
  S: String;
  P: Pointer;
  MemStr: IEvDualStream;
begin
  if Assigned(FDataSource) and Boolean(FDataSource.PActive) and (FDataField <> '') then
  begin
    S := VarToStr(FDataSource.GetFieldValue(FDataField));
    if Length(S) > 0 then
    begin
      P := @S[1];
      MemStr := TEvDualStreamHolder.Create(Length(S));
      MemStr.RealStream.Write(P^, Length(S));
      MemStr.Position := 0;
      Picture.Bitmap.LoadFromStream(MemStr.RealStream); // could be not only Bitmap
    end
    else
      Picture.Bitmap.Assign(nil);
  end
  else
    Picture.Bitmap.Assign(nil);
end;

procedure TrwDBImage.SetDataSource(Value: TrwDataSet);
begin
  if FDataSource = Value then
    Exit;

  if Assigned(FDataSource) then
    FDataSource.UnRegisterComponent(Self);

  if Assigned(Value) then
    Value.RegisterComponent(Self);

  FDataSource := Value;

  if not ((csLoading in ComponentState) or Loading or (csDestroying in ComponentState)) and
    (not Assigned(FDataSource) or Assigned(FDataSource) and Boolean(FDataSource.PActive)) then
    GetImageFromDataSource;
end;


procedure TrwDBImage.RWNotification(Sender: TrwComponent; AOperation: TrwNotifyOperation);
begin
  inherited;
  if Sender = FDataSource then
    case AOperation of
      rnoDataSetChanged: GetImageFromDataSource;
      rnoDestroy: DataSource := nil;
    end;
end;

function TrwDBImage.CreateVisualControl: IrwVisualControl;
begin
 Result := TrwFramedImageControl.Create(Self);
end;

procedure TrwDBImage.PrintGraphic;
var
  lPrnCtrl: TFrameImage;
  lRendering: TrwRendering;
begin
  GetImageFromDataSource;

  if Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
  begin
    lRendering := TrwRendering(FRendering);
    lPrnCtrl := TFrameImage.Create(lRendering.RenderingPaper);
    lPrnCtrl.Picture.Assign(Picture);
    lPrnCtrl.Stretch := Stretch;
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.Autosize := False;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    CalcPosRelativeBand(lPrnCtrl);
    lPrnCtrl.Parent := lRendering.RenderingPaper;
  end;
end;


{ TrwContainer }

constructor TrwContainer.Create(AOwner: TComponent);
begin
  inherited;
  Width := 100;
  Height := 50;
end;

procedure TrwContainer.PrintASCII;
var
  lRendering, nRendering: TrwRendering;
begin
  if Assigned(SubReport) then
  begin
    lRendering := TrwRendering(FRendering);
    nRendering := TrwRendering.Create;
    try
      SubReport.PageFormat.PaperSize :=psCustom;
      SubReport.PageFormat.Width := MaxInt;
      SubReport.PageFormat.Height := MaxInt;
      SubReport.StandAlone := True;
      nRendering.OwnerASCIIResult := lRendering.RenderingPaper;
      SubReport.Print(nRendering);
      Height := 0;
    finally
      nRendering.Free;
    end;
  end

  else
    inherited;
end;


procedure TrwContainer.PrintGraphic;
var
  lRendering, nRendering: TrwRendering;
  lPrnCtrl: TGraphicFrame;
  C: TControl;
  dX, dY: Integer;
begin
  if Assigned(SubReport) then
  begin
    lRendering := TrwRendering(FRendering);

    lPrnCtrl := TGraphicFrame.Create(lRendering.RenderingPaper);
    lPrnCtrl.BoundLines := BoundLines;
    lPrnCtrl.BoundLinesColor := BoundLinesColor;
    lPrnCtrl.BoundLinesWidth := BoundLinesWidth;
    lPrnCtrl.BoundLinesStyle := BoundLinesStyle;
    lPrnCtrl.Brush.Color := Color;
    lPrnCtrl.Transparent := Transparent;
    lPrnCtrl.LayerNumber := LayerNumber;
    lPrnCtrl.Compare := Compare;
    CalcPosRelativeBand(lPrnCtrl);
    dX := lPrnCtrl.Left;
    dY := lPrnCtrl.Top;
    lPrnCtrl.Parent := lRendering.RenderingPaper;

    nRendering := TrwRendering.Create;
    try
      SubReport.PageFormat.PaperSize :=psCustom;
      SubReport.PageFormat.Width := System.Round(ConvertUnit(Width, utScreenPixels, utMillimeters)*10);
      SubReport.PageFormat.Height := MaxInt;
      SubReport.PageFormat.TopMargin := 0;
      SubReport.PageFormat.LeftMargin := 0;
      SubReport.PageFormat.BottomMargin := 0;
      SubReport.PageFormat.RightMargin := 0;
      SubReport.StandAlone := True;
      nRendering.OwnerASCIIResult := lRendering.RenderingPaper;
      SubReport.Print(nRendering);

      if nRendering.RenderingPaper.ControlCount = 0 then
      begin
        lPrnCtrl.Free;
        Height := 0;
      end
      else
      begin
        Height := nRendering.RenderingPaper.CurrentTop;
        lPrnCtrl.Height := nRendering.RenderingPaper.CurrentTop;

        while nRendering.RenderingPaper.ControlCount > 0 do
        begin
          C := nRendering.RenderingPaper.Controls[0];
          C.Parent := nil;
          nRendering.RenderingPaper.RemoveComponent(C);
          lRendering.RenderingPaper.InsertComponent(C);
          C.Top := C.Top + dY;
          C.Left := C.Left + dX;
          C.Parent :=lRendering.RenderingPaper;
        end;
      end;
    finally
      nRendering.Free;
    end;
  end

  else
    inherited;
end;

procedure TrwContainer.SetAutoHeight(Value: Boolean);
begin
  if Assigned(FSubReport) then
    Value := True;
  inherited;
end;


procedure TrwContainer.SetAutoWidth(Value: Boolean);
begin
  if Assigned(FSubReport) then
    Value := False;
  inherited;
end;

procedure TrwContainer.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited;
  SetSubReportPaper;
end;

procedure TrwContainer.SetPosition(Index, Value: Integer);
begin
  inherited;
  if Index = 2 then
    SetSubReportPaper;
end;

procedure TrwContainer.SetSubReport(const Value: TrwSubReport);
begin
  FSubReport := Value;
  if Assigned(FSubReport) then
  begin
    AutoHeight := True;
    SetSubReportPaper;
  end;
end;
 
procedure TrwContainer.SetSubReportPaper;
begin
  if Assigned(FSubReport) then
  begin
    FSubReport.PageFormat.PaperOrientation := rpoPortrait;
    FSubReport.PageFormat.TopMargin := 0;
    FSubReport.PageFormat.BottomMargin := 0;
    FSubReport.PageFormat.RightMargin := 0;
    FSubReport.PageFormat.LeftMargin := 0;
    FSubReport.PageFormat.PaperSize := psCustom;
    FSubReport.PageFormat.Width := Round(ConvertUnit(Width, utScreenPixels, utMillimeters) * 10);
  end;
end;


{ TrwCustomImage }

procedure TrwCustomImage.ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer);
begin
  if not Stretch and Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
  begin
    if Align in [alNone, alLeft, alRight] then
      AWidth := Picture.Width;
    if Align in [alNone, alTop, alBottom] then
      AHeight := Picture.Height;
  end;

  inherited;
end;

procedure TrwCustomImage.CheckSize;
begin
  if not Stretch and Assigned(Picture.Graphic) and not Picture.Graphic.Empty then
    SetBounds(Left, Top, Picture.Width, Picture.Height);
end;

constructor TrwCustomImage.Create(AOwner: TComponent);
begin
  inherited;
  FPicture := TrwPicture.Create;
  FPicture.OnChange := OnChangePicture;
  FStretch := True;
  FTransparent := True;
  FBoundLines := [];
  FBoundLinesColor := clBlack;
  FBoundLinesWidth := 1;
  FBoundLinesStyle := psSolid;
  Height := 105;
  Width := 105;
  DsgnShowCorners := True;
end;

destructor TrwCustomImage.Destroy;
begin
  FreeAndNil(FPicture);
  inherited;
end;

procedure TrwCustomImage.OnChangePicture(Sender: TObject);
begin
  if Picture.Graphic is TMetafile then
    Picture.Metafile.Inch := Screen.PixelsPerInch;

  if not (csLoading in ComponentState) then
  begin
    CheckSize;
    DoSyncVisualControl(rwCPInvalidate);
  end;
end;

procedure TrwCustomImage.SetBoundLines(Value: TrwSetColumnLines);
begin
  FBoundLines := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomImage.SetBoundLinesColor(Value: TColor);
begin
  FBoundLinesColor := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomImage.SetBoundLinesStyle(Value: TPenStyle);
begin
  FBoundLinesStyle := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomImage.SetBoundLinesWidth(Value: Integer);
begin
  FBoundLinesWidth := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomImage.SetPicture(Value: TrwPicture);
begin
  Picture.Assign(Value);
end;

procedure TrwCustomImage.SetStretch(Value: Boolean);
begin
  FStretch := Value;
  CheckSize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwCustomImage.SetTransparent(Value: Boolean);
begin
  FTransparent := Value;
  DoSyncVisualControl(rwCPInvalidate);
end;

{ TrwBarcode1D }

procedure TrwBarcode1D.ApplyConstrains(var ALeft, ATop, AWidth, AHeight: Integer);
begin
  if not (csLoading in ComponentState) and AutoSize then
  begin
    AWidth := FActualSize.cx;
    AHeight := FActualSize.cy;
  end;

  inherited;
end;

procedure TrwBarcode1D.CheckAutosize;
var
  C: TExtBarcode1D;
begin
  if not Autosize or (csLoading in ComponentState) or IsRenderingReading then
    Exit;

  C := TExtBarcode1D.Create(nil);
  try
    Font.AssignTo(C.Font);
    C.AutoSize := AutoSize;
    C.BarType := BarType;
    C.Code := Text;
    C.BarHeightPixels := BarHeight;
    C.ShowText := ShowText;
    C.CodeSup := SupplementalCode;
    C.CheckCharacter := CheckCharacter;
    C.PostnetHeightTallBar := PostnetHeightTallBar;
    C.PostnetHeightShortBar := PostnetHeightShortBar;
    C.GuardBars := GuardBars;
    C.UPCEANSupplement2 := UPCEANSupplement2;
    C.UPCEANSupplement5 := UPCEANSupplement5;
    C.UPCESytem := UPCESytem;
    C.CODABARStartChar := CODABARStartChar;
    C.CODABARStopChar := CODABARStopChar;
    C.Code128Set := Code128Set;
    C.supSeparation := SupSeparation;
    C.VerticalOrientation := VerticalOrientation;
    C.TopMargin := TopMargin;
    C.LeftMargin := LeftMargin;
    C.BackColor := Color;

    FActualSize := C.GetActualSize(0);
  finally
    C.Free;
  end;

  SetBounds(Left, Top, FActualSize.cx, FActualSize.cy);
end;

constructor TrwBarcode1D.Create(AOwner: TComponent);
begin
  inherited;
  FBarType := CODE39;
  FBarHeight := 40;
  FBarColor := clBlack;
  FShowText := True;
  Transparent := True;
  Autosize := True;
  FGuardBars := True;
  FCheckCharacter := True;
  FPostnetHeightTallBar := 20;
  FPostnetHeightShortBar := 10;
  FUPCESytem := '1';
  FCODABARStartChar := 'A';
  FCODABARStopChar := 'B';
  FCode128Set := 'B';
  FSupSeparation := 10;
  FTopMargin := 10;
  FLeftMargin := 10;
end;

function TrwBarcode1D.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwBarcode1DControl.Create(Self);
end;

procedure TrwBarcode1D.InitializeForDesign;
begin
  inherited;
  Text := '123456789';
end;

procedure TrwBarcode1D.PrintGraphic;
var
  lPrnCtrl: TExtBarcode1D;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  lPrnCtrl := TExtBarcode1D.Create(lRendering.RenderingPaper);
  if Transparent then
    lPrnCtrl.BackColor := clNone
  else
    lPrnCtrl.BackColor := Color;
  lPrnCtrl.BarColor := BarColor;
  Font.AssignTo(lPrnCtrl.Font);

  lPrnCtrl.AutoSize := AutoSize;
  lPrnCtrl.BarType := BarType;
  lPrnCtrl.Code := Text;
  lPrnCtrl.LayerNumber := LayerNumber;
  lPrnCtrl.Compare := Compare;
  lPrnCtrl.BarHeightPixels := BarHeight;
  lPrnCtrl.ShowText := ShowText;
  lPrnCtrl.guardBars := GuardBars;
  lPrnCtrl.CheckCharacter := CheckCharacter;
  lPrnCtrl.PostnetHeightTallBar := PostnetHeightTallBar;
  lPrnCtrl.PostnetHeightShortBar := PostnetHeightShortBar;
  lPrnCtrl.codeSup := SupplementalCode;
  lPrnCtrl.UPCESytem := UPCESytem;
  lPrnCtrl.UPCEANSupplement2 := UPCEANSupplement2;
  lPrnCtrl.UPCEANSupplement5 := UPCEANSupplement5;
  lPrnCtrl.CODABARStartChar := CODABARStartChar;
  lPrnCtrl.CODABARStopChar := CODABARStopChar;
  lPrnCtrl.Code128Set := Code128Set;
  lPrnCtrl.supSeparation := SupSeparation;
  lPrnCtrl.VerticalOrientation := VerticalOrientation;
  lPrnCtrl.TopMargin := TopMargin;
  lPrnCtrl.LeftMargin := LeftMargin;

  CalcPosRelativeBand(lPrnCtrl);
  lPrnCtrl.Parent := lRendering.RenderingPaper;
end;

procedure TrwBarcode1D.SetBarColor(const AValue: TColor);
begin
  FBarColor := AValue;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetBarHeight(const AValue: Integer);
begin
  FBarHeight := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetBarType(const AValue: Trw1DBarType);
begin
  FBarType := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetCheckCharacter(const Value: Boolean);
begin
  FCheckCharacter := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetCODABARStartChar(const Value: String);
begin
  FCODABARStartChar := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetCODABARStopChar(const Value: String);
begin
  FCODABARStopChar := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetCode128Set(const Value: String);
begin
  if (Value <> 'A') and (Value <> 'B') and (Value <> 'C') then
    raise Exception.Create('Code128Set could be A, B or C');
  FCode128Set := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetGuardBars(const Value: Boolean);
begin
  FGuardBars := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetLeftMargin(const Value: integer);
begin
  FLeftMargin := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetPostnetHeightShortBar(const Value: Integer);
begin
  FPostnetHeightShortBar := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetPostnetHeightTallBar(const Value: Integer);
begin
  FPostnetHeightTallBar := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetShowText(const AValue: Boolean);
begin
  FShowText := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetSupplementalCode(const Value: String);
begin
  FSupplementalCode := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetSupSeparation(const Value: Integer);
begin
  FSupSeparation := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetTopMargin(const Value: integer);
begin
  FTopMargin := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetUPCEANSupplement2(const Value: Boolean);
begin
  FUPCEANSupplement2 := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetUPCEANSupplement5(const Value: Boolean);
begin
  FUPCEANSupplement5 := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetUPCESytem(const Value: String);
begin
  if (Value <> '0') and (Value <> '1') then
    raise Exception.Create('UPC-E sytem could be 0 or 1');
  FUPCESytem := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcode1D.SetVerticalOrientation(const Value: boolean);
begin
  FVerticalOrientation := Value;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

{ TrwBarcodePDF417 }

procedure TrwBarcodePDF417.ApplyConstrains(var ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  if not (csLoading in ComponentState) and AutoSize then
  begin
    AWidth := FActualSize.cx;
    AHeight := FActualSize.cy;
  end;

  inherited;
end;

procedure TrwBarcodePDF417.CheckAutosize;
var
  C: TExtBarcodePDF417;
begin
  if not Autosize or (csLoading in ComponentState) or IsRenderingReading then
    Exit;

  C := TExtBarcodePDF417.Create(nil);
  try
    C.AutoSize := AutoSize;
    C.Code := Text;
    C.BarHeightPixels := BarHeightPixels;
    C.BarWidthPixels := BarWidthPixels;
    C.PDFColumns := ColumnsNumber;
    C.PDFRows := RowsNumber;
    C.PDFMaxRows := MaxRows;
    C.PDFECLevel := ECLevel;
    C.PDFMode := EncodingMode;
    C.PDFTruncated := Truncated;
    C.TopMargin := TopMargin;
    C.LeftMargin := LeftMargin;
    C.BackColor := Color;
    C.HorizontalPixelShaving := HorizontalPixelShaving;
    C.VerticalPixelShaving := VerticalPixelShaving;

    FActualSize := C.GetActualSize(0);
  finally
    C.Free;
  end;

  SetBounds(Left, Top, FActualSize.cx, FActualSize.cy);
end;

constructor TrwBarcodePDF417.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
  FBarColor := clBlack;
  FBarHeightPixels := 7;
  FBarWidthPixels := 1;
  FTopMargin := 10;
  FLeftMargin := 10;
  FColumnsNumber := 10;
  FRowsNumber := 0;
  FMaxRows := 0;
  FECLevel := 0;
  FEncodingMode := PDF_BINARY;
  FTruncated := false;
  FHorizontalPixelShaving := 0;
  FVerticalPixelShaving := 0;
  Autosize := true;
end;

function TrwBarcodePDF417.CreateVisualControl: IrwVisualControl;
begin
  Result := TrwBarcodePDF417Control.Create(Self);
end;

procedure TrwBarcodePDF417.InitializeForDesign;
begin
  inherited;
  Text := 'This is a PDF417 Barcode';
end;

procedure TrwBarcodePDF417.PrintGraphic;
var
  lPrnCtrl: TExtBarcodePDF417;
  lRendering: TrwRendering;
begin
  lRendering := TrwRendering(FRendering);

  lPrnCtrl := TExtBarcodePDF417.Create(lRendering.RenderingPaper);
  if Transparent then
    lPrnCtrl.BackColor := clNone
  else
    lPrnCtrl.BackColor := Color;
  lPrnCtrl.BarColor := BarColor;
  lPrnCtrl.AutoSize := AutoSize;
  lPrnCtrl.Code := Text;
  lPrnCtrl.LayerNumber := LayerNumber;
  lPrnCtrl.Compare := Compare;
  lPrnCtrl.BarHeightPixels := BarHeightPixels;
  lPrnCtrl.BarWidthPixels := BarWidthPixels;
  lPrnCtrl.pdfCOlumns := ColumnsNumber;
  lPrnCtrl.pdfRows := RowsNumber;
  lPrnCtrl.pdfMaxRows := MaxRows;
  lPrnCtrl.pdfECLevel := ECLevel;
  lPrnCtrl.pdfMode := EncodingMode;
  lPrnCtrl.pdfTruncated := Truncated;
  lPrnCtrl.TopMargin := TopMargin;
  lPrnCtrl.LeftMargin := LeftMargin;
  lPrnCtrl.HorizontalPixelShaving := HorizontalPixelShaving;
  lPrnCtrl.VerticalPixelShaving := VerticalPixelShaving; 

  CalcPosRelativeBand(lPrnCtrl);
  lPrnCtrl.Parent := lRendering.RenderingPaper;
end;

procedure TrwBarcodePDF417.SetBarColor(const AValue: TColor);
begin
  FBarCOlor := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetBarHeightPixels(const AValue: Integer);
begin
  FBarHeightPixels := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetBarWidthPixels(const AValue: integer);
begin
  FBarWidthPixels := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetColumnsNumber(const AValue: integer);
begin
  FColumnsNumber := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetECLevel(const AValue: integer);
begin
  if not (AValue in [0, 1, 2, 3, 4, 5]) then
    raise Exception.Create('Wrong value for ECLevel. Valid values are 0, 1, 2, 3, 4 and 5');
  FECLevel := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetEncodingMode(const AValue: TrwPDF417BarEncoding);
begin
  FEncodingMode := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetHorizontalPixelShaving(const AValue: integer);
begin
  FHorizontalPixelShaving := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetLeftMargin(const AValue: integer);
begin
  FLeftMargin := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetMaxRows(const AValue: integer);
begin
  FMaxRows := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetRowsNumber(const AValue: integer);
begin
  FRowsNumber := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetTopMargin(const AValue: integer);
begin
  FTopMargin := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetTruncated(const AValue: boolean);
begin
  FTruncated := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

procedure TrwBarcodePDF417.SetVerticalPixelShaving(const AValue: integer);
begin
  FVerticalPixelShaving := AValue;
  CheckAutosize;
  DoSyncVisualControl(rwCPInvalidate);
end;

end.

