unit rwQBTemplateEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rwQBInfo, EvStreamUtils;

type
  TrwQBTemplateEditor = class(TForm)
    Label1: TLabel;
    edName: TEdit;
    Label2: TLabel;
    edGroup: TEdit;
    Label3: TLabel;
    memDescr: TMemo;
    btnCancel: TButton;
    btnOK: TButton;
    btnLoad: TButton;
    pnlStatus: TPanel;
    odOpen: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure edNameExit(Sender: TObject);
    procedure edGroupExit(Sender: TObject);
    procedure memDescrExit(Sender: TObject);
  private
    FTemplate: TrwQBTEmplate;
  public
  end;


  function EditTemplate(ATemplate: TrwQBTEmplate): Boolean;

implementation

{$R *.DFM}


function EditTemplate(ATemplate: TrwQBTEmplate): Boolean;
var
  Frm: TrwQBTemplateEditor;
begin
  Result := False;

  Frm := TrwQBTemplateEditor.Create(nil);

  with Frm do
    try
      FTemplate.Assign(ATemplate);

      edName.Text := FTemplate.Name;
      edGroup.Text := FTemplate.Group;
      memDescr.Text := FTemplate.Description;

      if Length(FTemplate.Data) > 0 then
      begin
        pnlStatus.Caption := 'Loaded';
        pnlStatus.Font.Color := clGreen;
      end;

      if ShowModal = mrOK then
      begin
        ATemplate.Assign(FTemplate);
        Result := True;
      end;

    finally
      Free;
    end;
end;


procedure TrwQBTemplateEditor.FormCreate(Sender: TObject);
begin
  FTemplate := TrwQBTEmplate.Create(nil);
end;

procedure TrwQBTemplateEditor.FormDestroy(Sender: TObject);
begin
  FTemplate.Free;
end;

procedure TrwQBTemplateEditor.btnLoadClick(Sender: TObject);
var
  FS: TEvFileStream;
begin
  if odOpen.Execute then
  begin
    FS := TEvFileStream.Create(odOpen.FileName, fmOpenRead);
    try
      FTemplate.LoadFromStream(FS);
      pnlStatus.Caption := 'Loaded';
      pnlStatus.Font.Color := clGreen;
    finally
      FS.Free;
    end;
  end;
end;

procedure TrwQBTemplateEditor.edNameExit(Sender: TObject);
begin
  FTemplate.Name := edName.Text;
end;

procedure TrwQBTemplateEditor.edGroupExit(Sender: TObject);
begin
  FTemplate.Group := edGroup.Text;
end;

procedure TrwQBTemplateEditor.memDescrExit(Sender: TObject);
begin
  FTemplate.Description := memDescr.Text;
end;

end.
