unit rwOneRepPreviewFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Menus, Dialogs,
  ExtCtrls, StdCtrls, rwPrinters, ComCtrls, ToolWin, ImgList, ActnList,
  rwPrintPage, mwCustomEdit, rwPreviewUtils, rwTypes, rwRWASecurityFrm,
  rwBasicPrint, rwPrintElements, isRegistryFunctions, rwEngineTypes, EvStreamUtils,
  Math, rwGraphics, rwBasicUtils, mwKeyCmds, OleCtrls, ShDocVw, rwConnectionInt,
  isBasicUtils, isBaseClasses, jpeg, Buttons;

const
  CM_SET_BOUNDS_EXT = WM_USER + 1;

type
  TrwPreviewPage = class(TrwVirtualPage)
  private
    FDrawing: Boolean;
    FMark: TrwPrintMark;
    Fxy: TPoint;
    FDblClk: Boolean;
    FMarking: Boolean;
    FMarksChanged: Boolean;
    FLabelsEdited: boolean;

  protected
    procedure Paint; override;
    procedure ScaleSize; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DblClick; override;
    procedure DeleteSelectedMark;
    procedure EditSelectedMark;
    function  TextAtPos(const X, Y: Word; var AItemIndex: integer): TrwPrintText;
    procedure EditSelectedText(const ATextObject: TrwPrintText);

  public
    constructor Create(AOwner: TComponent); override;
    function    SearchWord(const AWord: String): Boolean;
    procedure   PreparePage(const APageNum: Integer; AData: TStream); override;
    procedure   ClearHighlighting;
  end;


  {TrwPreview is class for priview results of report}

  TrwOneRepPreview = class(TFrame)
    ctbTools: TControlBar;
    sbxPreview: TScrollBox;
    tlbTools: TToolBar;
    ToolButton1: TToolButton;
    btnFirst: TToolButton;
    btnPrior: TToolButton;
    btnNext: TToolButton;
    ToolButton5: TToolButton;
    btnLast: TToolButton;
    ilTools: TImageList;
    aclActions: TActionList;
    actPrint: TAction;
    actFirst: TAction;
    actLast: TAction;
    actNext: TAction;
    actPrev: TAction;
    pnlPages: TPanel;
    Label1: TLabel;
    pnlPageNum: TPanel;
    btnAsIs: TToolButton;
    btnFitToWnd: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    actPrinterSetup: TAction;
    actOpen: TAction;
    actSave: TAction;
    opdOpenResult: TOpenDialog;
    svdSaveResult: TSaveDialog;
    btnFitToWidth: TToolButton;
    actFitToWnd: TAction;
    actAsIs: TAction;
    actFitToWidth: TAction;
    ToolButton7: TToolButton;
    btnMarking: TToolButton;
    actMarking: TAction;
    actDown: TAction;
    actUp: TAction;
    actLeft: TAction;
    actRight: TAction;
    actHome: TAction;
    actEnd: TAction;
    actCtrlLeft: TAction;
    actCtrlRight: TAction;
    actCtrlUp: TAction;
    actCtrlDown: TAction;
    actJumpToPage: TAction;
    pmMark: TPopupMenu;
    Deletemark1: TMenuItem;
    Changetext1: TMenuItem;
    pnlASCIIPos: TPanel;
    lPos: TLabel;
    pnlPos: TPanel;
    pnlChar: TPanel;
    lChar: TLabel;
    ToolButton2: TToolButton;
    actSecurity: TAction;
    ToolButton3: TToolButton;
    actSearchText: TAction;
    dlgSearch: TFindDialog;
    pnlReports: TPanel;
    Label2: TLabel;
    cbReports: TComboBox;
    pnlBrowser: TPanel;
    tbSaveAll: TToolButton;
    actSaveAll: TAction;
    pnlExcel: TPanel;
    SpeedButton1: TSpeedButton;
    procedure actNextExecute(Sender: TObject);
    procedure actPrevExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actPrinterSetupExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actPrintExecute(Sender: TObject);
    procedure actFirstExecute(Sender: TObject);
    procedure actLastExecute(Sender: TObject);
    procedure actFitToWndExecute(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure actAsIsExecute(Sender: TObject);
    procedure actFitToWidthExecute(Sender: TObject);
    procedure actMarkingExecute(Sender: TObject);
    procedure actDownExecute(Sender: TObject);
    procedure actUpExecute(Sender: TObject);
    procedure actLeftExecute(Sender: TObject);
    procedure actRightExecute(Sender: TObject);
    procedure actHomeExecute(Sender: TObject);
    procedure actEndExecute(Sender: TObject);
    procedure actCtrlDownExecute(Sender: TObject);
    procedure actCtrlUpExecute(Sender: TObject);
    procedure actCtrlLeftExecute(Sender: TObject);
    procedure actCtrlRightExecute(Sender: TObject);
    procedure actJumpToPageExecute(Sender: TObject);
    procedure pnlPageNumClick(Sender: TObject);
    procedure Changetext1Click(Sender: TObject);
    procedure Deletemark1Click(Sender: TObject);
    procedure actSecurityExecute(Sender: TObject);
    procedure actSearchTextExecute(Sender: TObject);
    procedure sbxPreviewClick(Sender: TObject);
    procedure dlgSearchFind(Sender: TObject);
    procedure dlgSearchShow(Sender: TObject);
    procedure dlgSearchClose(Sender: TObject);
    procedure cbReportsChange(Sender: TObject);
    procedure actSaveAllExecute(Sender: TObject);
    function PreparePreviewInfo(const AFiles : TStrings) : PTrwPreviewInfoRec;
    procedure btnViewInExcelClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);

  private
    FWebBrowser: TWebBrowser;
    FFileStream: TStream;
    FStream: TStream;
    FPage: TrwPreviewPage;
    FClearTempDir: Boolean;
    FAutoConvertOldFormat: Boolean;
    FFileName: String;
    FPrintDialog: TrwPrintDialog;
    FSecurityRec: TrwRWASecurityRec;
    FResultHolder: IrwResult;
    FStandAloneUtilityMode: Boolean;
    FSecurityChanged: Boolean;
    FnoShowPage : boolean;
    FTempFile: String;

    procedure ShowPage(APageNum: Integer);
    procedure UpdatePage;
    procedure SyncFitting;
    procedure SyncSize;
    procedure SetSecurePreview(const Value: Boolean);
    procedure ChangeASCIICursorPos(Sender: TObject);
    procedure SaveCurrentDataToFile(const AFileName: String); overload;
    procedure ShowRWAReport(AStream: TStream);
    procedure ShowASCIIReport(AStream: TStream);
    procedure ShowXMLReport(AStream: TStream);
    procedure ShowXLSReport(AStream: TStream);
    procedure CloseSearchDialogs;
    function  CurrentDataType: TrwResultFormatType;
    procedure CMSetBoundExt(var Message: TMessage); message CM_SET_BOUNDS_EXT;
    procedure LoadAdditionalChanges;
    procedure SaveAdditionalChanges(const AOriginalFileName: String);

  protected
    FAskToSaveChanges: Boolean;
    FSecurePreview: Boolean;
    FmeASCII: TmwCustomEdit;

    procedure AfterPrint; virtual;
    procedure SetButtons(AEnabled: Boolean); virtual;
    procedure FixToolBarSize;

    property  SecurePreview: Boolean read FSecurePreview write SetSecurePreview;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   BeforeDestruction; override;
    procedure   BeforeClosePreview; virtual;
    procedure   OnParentShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure   ScrollInViewRect(const ARect: TRect);
    procedure   SetBoundsExt(const ABounds: TrwBounds);

    procedure PreviewReport(AStream: TStream); overload; virtual;
    procedure PreviewReport(AStream: IEvDualStream); overload; virtual;
    procedure PreviewReport(AFileName: string); overload;
    procedure PreviewReport(const pPreviewInfo: PTrwPreviewInfoRec; const AStandAloneUtilityMode : boolean = false); overload;

    property  AutoConvertOldFormat: Boolean read FAutoConvertOldFormat write FAutoConvertOldFormat;
    property  FileName: string read FFileName;
    property  StandAloneUtilityMode: Boolean read FStandAloneUtilityMode write FStandAloneUtilityMode;
  end;

implementation

{$IFNDEF PREVIEW_UTIL}
uses  rwEngine;
{$ENDIF}

{$R *.DFM}

const
  LabelChangesName = 'LabelChanges';

  {TrwPreview}

constructor TrwOneRepPreview.Create(AOwner: TComponent);
var
  P: String;
begin
  inherited;

  FPrintDialog := TrwPrintDialog.Create(Self);
  rwPrinter.PrintJobInfo.PrintPCLAsGraphic := ReadFromRegistry('Settings', 'PrintPCLAsGraphics', '0', sRWPrevRegKey) = '1';

  btnFitToWnd.Down := False;
  btnFitToWidth.Down := False;
  btnAsIs.Down := False;
  P := ReadFromRegistry('Settings', 'PreviewPageFit', '0', sRWPrevRegKey);
  case P[1] of
    '1': btnFitToWnd.Down := True;
    '2': btnFitToWidth.Down := True;
  else
    btnAsIs.Down := True;
  end;

  FStandAloneUtilityMode := False;
  FSecurePreview := False;
  FAutoConvertOldFormat := False;
  FAskToSaveChanges := True;

  pnlPages.Left := btnLast.BoundsRect.Right;
  pnlPages.Visible := True;
  pnlASCIIPos.Visible := False;
  pnlReports.Visible := False;
  FixToolBarSize;

  FmeASCII := TmwCustomEdit.Create(Self);
  FmeASCII.Parent := Self;
  FmeASCII.Visible := False;
  FmeASCII.Align := alClient;
  FmeASCII.ReadOnly := True;
  FmeASCII.Gutter.ShowLineNumbers := True;
  FmeASCII.OnSelectionChange := ChangeASCIICursorPos;
  FmeASCII.InsertCaret := ctBlock;

  pnlBrowser.Align := alClient;
  pnlExcel.Align := alClient;

  if Length(PrvGetCachePath) = 0 then
  begin
    FClearTempDir := True;
    PrvInitCachePath;
  end

  else
    FClearTempDir := False;

  SetButtons(False);
end;

destructor TrwOneRepPreview.Destroy;
begin
  BeforeClosePreview;

  FmeASCII.Free;

  if FClearTempDir then
    PrvClearCachePath;

  if Assigned(aclActions) and not Assigned(aclActions.Owner) then
    aclActions.Free;

{$IFNDEF PREVIEW_UTIL}
   RWEngineExt.AppAdapter.AfterClosePreview(Self);
{$ENDIF}

  inherited;
end;


procedure TrwOneRepPreview.BeforeClosePreview;
var
  P: String;
begin
  if Assigned(FFileStream) then
  begin
    if FAskToSaveChanges and
      (FSecurityChanged or (Assigned(FPage) and (FPage.FMarksChanged or FPage.FLabelsEdited))) then
      if PrvMessage('Do you want to save changes?', mtConfirmation, [mbYes, mbNo]) = mrYes then
        try
          SaveCurrentDataToFile(FFileName);
        except
          on E: Exception do
            PrvMessage(E.Message, mtError, [mbOk]);
        end;

    FreeAndNil(FFileStream);
  end;

  if Assigned(FPage) then
  begin
    FreeAndNil(FPage);
    SetButtons(False);
  end;

  FResultHolder := nil;

  pnlPageNum.Caption := '';
  pnlPages.Left := btnLast.BoundsRect.Right;
  pnlPages.Caption := '';
  pnlPages.Visible := True;
  pnlASCIIPos.Visible := False;
  pnlReports.Visible := cbReports.Items.Count > 1;
  FixToolBarSize;
  sbxPreview.Visible := True;
  FmeASCII.Visible := False;
  FmeASCII.Text := '';

  if pnlBrowser.Visible then
  begin
    FreeAndNil(FWebBrowser);
    pnlBrowser.Visible := False;
  end;

  if pnlExcel.Visible then
  begin
    pnlExcel.Visible := False;
    TForm(Parent).VertScrollBar.Visible := True;
  end;

  FSecurityRec.PreviewPassword := '';
  FSecurityRec.PrintPassword := '';
  FSecurityRec.Rights := 0;
  FSecurityRec.Compressed := False;

  if btnFitToWnd.Down then
    p := '1'
  else if btnFitToWidth.Down then
    p := '2'
  else
    p := '0';

  WriteToRegistry('Settings', 'PreviewPageFit', P, sRWPrevRegKey);

  if FTempFile <> '' then
  begin
    DeleteFile(FTempFile);
    FTempFile := '';
  end;
end;


procedure TrwOneRepPreview.ShowPage(APageNum: Integer);
var
  PPI: Integer;
begin
  if FnoShowPage then
    exit;
  PPI := FPage.PixelsPerInch;
  FPage.PreparePage(APageNum, FStream);

  if PPI <> FPage.PixelsPerInch then
    SyncFitting;

  UpdatePage;

  ctbTools.Update;
end;

procedure TrwOneRepPreview.UpdatePage;
begin
  sbxPreview.HorzScrollBar.Position := 0;
  sbxPreview.VertScrollBar.Position := 0;
  FPage.DoubleBuffered := True;
  FPage.Invalidate;
  FPage.Update;
  FPage.DoubleBuffered := False;
  pnlPageNum.Caption := IntToStr(FPage.PageNum) + ' of ' + IntToStr(FPage.Header.PagesCount);
end;


procedure TrwOneRepPreview.actNextExecute(Sender: TObject);
begin
  if (FPage.PageNum = FPage.Header.PagesCount) then
    Exit;

  ShowPage(FPage.PageNum + 1);
end;

procedure TrwOneRepPreview.actPrevExecute(Sender: TObject);
begin
  if (FPage.PageNum <= 1) then
    Exit;

  ShowPage(FPage.PageNum - 1);
end;

procedure TrwOneRepPreview.actSaveExecute(Sender: TObject);
var
  lSecurityRec: TrwRWASecurityRec;

  procedure FixExtension;
  var
    i: Integer;
    h, Ext: String;
  begin
    if svdSaveResult.FileName <> '' then
    begin
      h := svdSaveResult.Filter;
      for i := 1 to svdSaveResult.FilterIndex do
      begin
        GetNextStrValue(h, '|');
        Ext := GetNextStrValue(h, '|');
      end;
      Delete(Ext, 1, 1);

      svdSaveResult.FileName := ChangeFileExt(svdSaveResult.FileName, Ext);
    end;
  end;

begin
  CloseSearchDialogs;

  if CurrentDataType = rwRTASCII then
  begin
    svdSaveResult.Filter := 'ReportWriter Result File (*.rwa)|*.rwa|Text File (*.txt)|*.txt';    svdSaveResult.Filter := 'ReportWriter Result File (*.rwa)|*.rwa|Text File (*.txt)|*.txt|All Files (*.*)|*.*';
    if FSecurityRec.Rights <> 0 then
      svdSaveResult.FilterIndex := 1
    else
      svdSaveResult.FilterIndex := 2;

    FixExtension;

    if svdSaveResult.Execute then
    begin
      if svdSaveResult.FilterIndex <> 3 then
      FixExtension;

      if (FSecurityRec.Rights = 0) and SameText(ExtractFileExt(svdSaveResult.FileName), '.RWA') then
      begin
        if PrvMessage('File doesn''t have security set up. Only compression will be applyed.'#13'Continue?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then
        begin
          lSecurityRec := FSecurityRec;
          try
            FSecurityRec.Rights := 2;
            FSecurityRec.Compressed := True;
            FSecurityRec.PreviewPassword := '';
            FSecurityRec.PrintPassword := '';
            SaveCurrentDataToFile(svdSaveResult.FileName);
          finally
            FSecurityRec := lSecurityRec;          
          end;
        end;
      end

      else if (FSecurityRec.Rights <> 0) and not SameText(ExtractFileExt(svdSaveResult.FileName), '.RWA') then
      begin
        if PrvMessage('Text files are unsecure, but this file is stored in secure RWA format.'#13 +
                      'Do you want to save this secured RWA file as a raw text(ASCII) file?', mtConfirmation, [mbYes, mbNo], mbNo) = mrYes then
        begin
          lSecurityRec := FSecurityRec;
          try
            FSecurityRec.Rights := 0;
            FSecurityRec.Compressed := False;
            FSecurityRec.PreviewPassword := '';
            FSecurityRec.PrintPassword := '';
            SaveCurrentDataToFile(svdSaveResult.FileName);
          finally
            FSecurityRec := lSecurityRec;
          end;
        end;
      end

      else
        SaveCurrentDataToFile(svdSaveResult.FileName);
    end;
  end


  else if CurrentDataType = rwRTXML then
  begin
    svdSaveResult.Filter := 'XML File (*.xml)|*.xml';
    svdSaveResult.FilterIndex := 1;
    FixExtension;

    if svdSaveResult.Execute then
    begin
      FixExtension;
      SaveCurrentDataToFile(svdSaveResult.FileName);
    end;
  end

  else if CurrentDataType = rwRTXLS then
  begin
    svdSaveResult.Filter := 'Excel File (*.xls)|*.xls';
    svdSaveResult.FilterIndex := 1;
    FixExtension;

    if svdSaveResult.Execute then
    begin
      FixExtension;
      SaveCurrentDataToFile(svdSaveResult.FileName);
    end;
  end

  else if CurrentDataType = rwRTRWA then
  begin
    svdSaveResult.Filter := 'ReportWriter Result File (*.rwa)|*.rwa|PDF File (*.pdf)|*.pdf';
    svdSaveResult.FilterIndex := 1;
    FixExtension;

    if svdSaveResult.Execute then
    begin
      FixExtension;
      SaveCurrentDataToFile(svdSaveResult.FileName);
    end;
  end;

  // Redraw whole window in order to avoid refresh issue when Preview window is embedded into Evolution
  InvalidateRect(self.ParentWindow, nil, false);
  RedrawWindow(self.ParentWindow, PRect(nil), HRGN(nil), RDW_ALLCHILDREN);
end;


procedure TrwOneRepPreview.actPrinterSetupExecute(Sender: TObject);
begin
  CloseSearchDialogs;
  if FPrintDialog.Execute then
  begin
    FPage.DefinePrintMetric;
    SyncFitting;
    FPage.Update;
    actPrint.Execute;
  end;
end;

procedure TrwOneRepPreview.actOpenExecute(Sender: TObject);
var
  pPreviewInfo: PTrwPreviewInfoRec;
begin
  CloseSearchDialogs;
  if opdOpenResult.Execute then
  begin
    cbReports.Clear;
    BeforeClosePreview;
    if opdOpenResult.Files.Count = 1 then  // one file
      PreviewReport(opdOpenResult.FileName)
    else
    begin // many files
      pPreviewInfo := PreparePreviewInfo(opdOpenResult.Files);
      PreviewReport(pPreviewInfo, true);
    end;
  end;
end;

procedure TrwOneRepPreview.actPrintExecute(Sender: TObject);
var
  lPos: LongInt;
begin
  lPos := FStream.Position;
  try
    rwPrinter.PrintJobInfo.PrintJobName := cbReports.Text;
    PrintReport(FStream, FPage.Header.Marks, nil, FPage.Header.LabelChangesList);
  finally
    FStream.Position := lPos;
  end;

  AfterPrint;
end;


procedure TrwOneRepPreview.actFirstExecute(Sender: TObject);
begin
  if (FPage.PageNum = 1) then
    Exit;
  ShowPage(1);
end;


procedure TrwOneRepPreview.SyncFitting;
begin
  if btnAsIs.Down then
    actAsIs.Execute
  else if btnFitToWidth.Down then
    actFitToWidth.Execute
  else if btnFitToWnd.Down then
    actFitToWnd.Execute;
end;


procedure TrwOneRepPreview.actLastExecute(Sender: TObject);
begin
  if (FPage.PageNum = FPage.Header.PagesCount) then
    Exit;

  ShowPage(FPage.Header.PagesCount);
end;

procedure TrwOneRepPreview.actFitToWndExecute(Sender: TObject);
var
  h1, h2: Integer;
  w, h: Integer;
  k: Extended;
begin
  sbxPreview.AutoScroll := False;
  w := FPage.FReadBounds.Right - FPage.FReadBounds.Left;
  h := FPage.FReadBounds.Bottom - FPage.FReadBounds.Top;

  h1 := sbxPreview.Width - 20;
  h2 := sbxPreview.Height - 20;

  k := Min(h1/w, h2/h);

  FPage.ChangeZoom(k, k);
  SyncSize;
end;

procedure TrwOneRepPreview.actFitToWidthExecute(Sender: TObject);
var
  h1, h2: Integer;
begin
  sbxPreview.AutoScroll := True;

  h1 := sbxPreview.Width - 20;
  h2 := FPage.FReadBounds.Right - FPage.FReadBounds.Left;

  FPage.ChangeZoom(h1/h2, h1/h2);

  SyncSize;

  if sbxPreview.VertScrollBar.IsScrollBarVisible then
  begin
    h1 := sbxPreview.Width - 20 - sbxPreview.VertScrollBar.Size;
    h2 := FPage.FReadBounds.Right - FPage.FReadBounds.Left;
    FPage.ChangeZoom(h1/h2, h1/h2);
    SyncSize;
  end;
end;

procedure TrwOneRepPreview.FrameResize(Sender: TObject);
begin
  if Assigned(FPage) and (FPage.PageNum > 0) then
    if not btnAsIs.Down then
      SyncFitting
    else
      SyncSize;
end;

procedure TrwOneRepPreview.actAsIsExecute(Sender: TObject);
begin
  sbxPreview.AutoScroll := True;
  FPage.ChangeZoom(cScreenCanvasRes / FPage.PixelsPerInch, cScreenCanvasRes / FPage.PixelsPerInch);
  SyncSize;
end;

procedure TrwOneRepPreview.SyncSize;
var
  h: Integer;
  R: TRect;
begin
  sbxPreview.HorzScrollBar.Position := 0;
  sbxPreview.VertScrollBar.Position := 0;

  R := FPage.FReadBounds;
  R.Right := Round(R.Right * FPage.Kf_ZoomX);
  R.Bottom := Round(R.Bottom * FPage.Kf_ZoomY);

  h := (sbxPreview.ClientWidth - R.Right) div 2;
  if h < 0 then
  begin
    R.Right := R.Right - R.Left;
    R.Left := 0
  end
  else
  begin
    R.Right := R.Right + h;
    R.Left := h;
  end;
  R.Top := 10;
  R.Bottom := R.Bottom + 10;
  FPage.SetBounds(R.Left, R.Top, R.Right - R.Left, R.Bottom - R.Top);
end;


procedure TrwOneRepPreview.SetButtons(AEnabled: Boolean);
begin
  if (FSecurityRec.Rights = 0) or not AEnabled then
  begin
    actPrint.Enabled := AEnabled and not FSecurePreview;
    actPrinterSetup.Enabled := AEnabled and not FSecurePreview;
    actSave.Enabled := AEnabled and not FSecurePreview;
    actSaveAll.Enabled := AEnabled and not FSecurePreview and (cbReports.Items.Count > 1);
    actMarking.Enabled := AEnabled;
    actSecurity.Enabled := AEnabled and not FSecurePreview;
    FmeASCII.SecureMode := not AEnabled or FSecurePreview;
  end
  else if FSecurityRec.Rights = 1 then
  begin
    actPrint.Enabled := False;
    actPrinterSetup.Enabled := False;
    actSave.Enabled := False;
    actSaveAll.Enabled := False;
    actMarking.Enabled := False;
    actSecurity.Enabled := False;
    FmeASCII.SecureMode := True;
  end
  else if FSecurityRec.Rights = 2 then
  begin
    actPrint.Enabled := True;
    actPrinterSetup.Enabled := True;
    actSave.Enabled := True;
    actSaveAll.Enabled := (cbReports.Items.Count > 1);
    actMarking.Enabled := True;
    actSecurity.Enabled := True;
    FmeASCII.SecureMode := False;
  end;

  actOpen.Enabled := StandAloneUtilityMode;

  actFirst.Enabled := AEnabled;
  actLast.Enabled := AEnabled;
  actNext.Enabled := AEnabled;
  actPrev.Enabled := AEnabled;
  actFitToWnd.Enabled := AEnabled;
  actAsIs.Enabled := AEnabled;
  actFitToWidth.Enabled := AEnabled;

  if not actMarking.Enabled then
    btnMarking.Down := False;

  actDown.Enabled := AEnabled and (CurrentDataType = rwRTRWA);
  actUp.Enabled := actDown.Enabled;
  actLeft.Enabled := actDown.Enabled;
  actRight.Enabled := actDown.Enabled;
  actHome.Enabled := actDown.Enabled;
  actEnd.Enabled := actDown.Enabled;
  actCtrlLeft.Enabled := actDown.Enabled;
  actCtrlRight.Enabled := actDown.Enabled;
  actCtrlUp.Enabled := actDown.Enabled;
  actCtrlDown.Enabled := actDown.Enabled;
  actJumpToPage.Enabled := actDown.Enabled;

  if rwPrinter is TrwDummyPrinter then
    actPrinterSetup.Enabled := False;

  actSearchText.Enabled := AEnabled and (CurrentDataType <> rwRTXML) and (CurrentDataType <> rwRTXLS);
end;



procedure TrwOneRepPreview.actMarkingExecute(Sender: TObject);
begin
  FPage.FMarking := btnMarking.Down;
  if FPage.FMarking then
    FPage.Cursor := crHandPoint
  else
    FPage.Cursor := crDefault;
end;


procedure TrwOneRepPreview.SetSecurePreview(const Value: Boolean);
begin
  FSecurePreview := Value;
  actPrint.Enabled := not FSecurePreview;
  actPrinterSetup.Enabled := not FSecurePreview;
  actSecurity.Enabled := not FSecurePreview;
  actSave.Enabled := not FSecurePreview;
//  actMarking.Enabled := not FSecurePreview;
end;


procedure TrwOneRepPreview.ShowASCIIReport(AStream: TStream);
begin
  FStream := AStream;
  FStream.Position := 0;

  pnlPages.Visible := False;
  pnlASCIIPos.Visible := True;
  pnlASCIIPos.Left := btnLast.BoundsRect.Right;
  FixToolBarSize;

  FmeASCII.Visible := True;
  sbxPreview.Visible := False;

  SetButtons(FStream.Size > 0);
    
  FmeASCII.Lines.LoadFromStream(AStream);
  actUp.Enabled := False;
  actDown.Enabled := False;
  actLeft.Enabled := False;
  actRight.Enabled := False;
  actHome.Enabled := False;
  actEnd.Enabled := False;
  actCtrlLeft.Enabled := False;
  actCtrlRight.Enabled := False;
  actCtrlUp.Enabled := False;
  actCtrlDown.Enabled := False;

  actPrint.Enabled := False;
  actPrinterSetup.Enabled := False;
  actFirst.Enabled := False;
  actLast.Enabled := False;
  actNext.Enabled := False;
  actPrev.Enabled := False;
  actFitToWnd.Enabled := False;
  actAsIs.Enabled := False;
  actFitToWidth.Enabled := False;
  actMarking.Enabled := False;

  svdSaveResult.FileName := rwPrinter.Title;
end;


procedure TrwOneRepPreview.PreviewReport(AFileName: string);
var
  ss: Integer;
  fs: TTempFileStream;
  lPassword: String;
begin
  if AFileName = '' then
  begin
    FFileName := '';
    SetButtons(False)
  end

  else
  begin
    pnlReports.Visible := cbReports.Items.Count > 1;
    FFileStream := TEvFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
    try
      FFileName := AFileName;
      Update;
      if FAutoConvertOldFormat then
        if IsItOldFormat(FFileStream) then
        begin
          FFileStream.Free;
          FFileStream := nil;
          if PrvMessage('Selected RW preview file is in old format!'#13+
                        'Do you want to convert it into new preview format?',
                         mtConfirmation, [mbYes, mbNo]) = mrYes then
            try
              Update;
              FFileStream := TEvFileStream.Create(AFileName, fmOpenReadWrite or fmShareDenyNone);
              ConvertOldFormat(FFileStream);
              FFileStream.Free;
              FFileStream := nil;
              FFileStream := TEvFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
            except
              on E: Exception do
              begin
                PrvMessage('Cannot to convert RW preview file!'#13 + E.Message, mtError, [mbOk]);
                Exit;
              end;
            end

          else
            Exit;
        end;

      FSecurityRec.PreviewPassword := '';
      FSecurityRec.PrintPassword := '';
      FSecurityRec.Rights := 0;
      FSecurityRec.Compressed := False;

      FFileStream.Position := 0;
      ss := IsSecureRWA(FFileStream);

      if ss <> 0 then
      begin
        if (ss and 2) <> 0 then
        begin
          lPassword := PrvPasswordDialog('Access password', 'Please input password');
          if lPassword = '' then
            raise ErwException.Create('Access denied');
        end;

        fs := TTempFileStream.Create;
        try
          FSecurityRec := UnSecureRWA(FFileStream, fs, lPassword);
          FFileStream.Free;
          FFileStream := fs;
        except
          on E: Exception do
          begin
            E.Message := 'Access denied';
            fs.Free;
            raise;
          end;
        end;
      end;

      PreviewReport(FFileStream);

      svdSaveResult.FileName := AFileName;

    except
      FreeAndNil(FFileStream);
      raise;
    end;
  end;
end;

procedure TrwOneRepPreview.PreviewReport(AStream: TStream);
begin
  FSecurityChanged := False;

  CloseSearchDialogs;

  if IsRWAFormat(AStream) then
    ShowRWAReport(AStream)
  else if IsXMLFormat(AStream) then
    ShowXMLReport(AStream)
  else if IsXLSFormat(AStream) then
    ShowXLSReport(AStream)
  else
    ShowASCIIReport(AStream);
end;


procedure TrwOneRepPreview.AfterPrint;
begin
end;


{ TrwPreviewPage }

constructor TrwPreviewPage.Create(AOwner: TComponent);
begin
  inherited;
  FDrawing := False;
  FDblClk := False;
  FMarking := False;
  FMarksChanged := False;
  FLabelsEdited := False;
  TabStop := True;
end;

procedure TrwPreviewPage.DblClick;
var
  TextObject: TRwPrintText;
  n: integer;
begin
  inherited;
  if FDrawing then
  begin
    FMark.Free;
    FDrawing := False;
  end;

  FMark := MarkAtPos(Round(Fxy.X / Kf_ZoomX), Round(Fxy.Y / Kf_ZoomY));
  if TrwOneRepPreview(Owner).FSecurityRec.Rights in [0, 2] then
    EditSelectedMark;

  n := -1;
  TextObject := TextAtPos(Round(Fxy.X / Kf_ZoomX), Round(Fxy.Y / Kf_ZoomY), n);
  if Assigned(TextObject) and isIsystemsDomain then
    EditSelectedText(TextObject);
  FDblClk := True;
end;

procedure TrwPreviewPage.DeleteSelectedMark;
var
  R: TRect;
begin
  if Assigned(FMark) then
  begin
    with FMark do
    begin
      R := Rect(Left, Top, Left + Width, Top + Height);
      Free;
      FMark := nil;
      FMarksChanged := True;
    end;
    R := ScaleRect(R);
    DoubleBuffered := True;
    InvalidateRect(Handle, @R, False);
    Update;
    DoubleBuffered := False;
  end;
end;

procedure TrwPreviewPage.EditSelectedMark;
var
  h: String;
begin
  if Assigned(FMark) then
  begin
    HideMarkText;
    h := PrvDefaultDialog('Notes', 'Text for the mark', FMark.Text);
    if h <> FMark.Text then
    begin
      FMark.Text := Trim(h);
      FMarksChanged := True;
    end;
  end;
end;


procedure TrwPreviewPage.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  ClearHighlighting;
  SetFocus;
  Fxy := Point(x, y);
  if FMarking and (Button = mbLeft)then
  begin
    if not FDblClk then
    begin
      DoubleBuffered := True;
      FDrawing := True;
      FMark := CreateMark(Round(X / Kf_ZoomX), Round(Y / Kf_ZoomY), 0, 0, '');
    end;
    FDblClk := False;
  end;
end;

procedure TrwPreviewPage.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  R: TRect;
begin
  inherited;
  if FDrawing then
  begin
    R := Rect(FMark.Left, FMark.Top, FMark.Left + FMark.Width, FMark.Top + FMark.Height);
    FMark.Height := Round(Y / Kf_ZoomY) - FMark.Top;
    FMark.Width := Round(X / Kf_ZoomX) - FMark.Left;

    if R.Right < FMark.Left + FMark.Width then
      R.Right := FMark.Left + FMark.Width;
    if R.Bottom <  FMark.Top + FMark.Height then
      R.Bottom := FMark.Top + FMark.Height;
    R := ScaleRect(R);
    InvalidateRect(Handle, @R, False);
  end;
end;

procedure TrwPreviewPage.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  R: TRect;
begin
  inherited;
  if FMarking then
  begin
    if FDrawing then
    begin
     if (Abs(FMark.Width) < 5) or (Abs(FMark.Height) < 5) then
     begin
       R := Rect(FMark.Left, FMark.Top, FMark.Left + FMark.Width, FMark.Top + FMark.Height);
       if R.Right < FMark.Left + FMark.Width then
         R.Right := FMark.Left + FMark.Width;
       if R.Bottom <  FMark.Top + FMark.Height then
         R.Bottom := FMark.Top + FMark.Height;
       FMark.Free;
       R := ScaleRect(R);
       InvalidateRect(Handle, @R, False);
     end
     else
     begin
       if FMark.Width < 0 then
       begin
         FMark.Left := FMark.Left + FMark.Width;
         FMark.Width := - FMark.Width;
       end;
       if FMark.Height < 0 then
       begin
         FMark.Top := FMark.Top + FMark.Height;
         FMark.Height := - FMark.Height;
       end;

       FMark.Text := PrvDefaultDialog('Notes', 'Text for the mark', '');
       Update;
       
       InvalidateRect(self.ParentWindow, nil, false);
       RedrawWindow(self.ParentWindow, PRect(nil), HRGN(nil), RDW_ALLCHILDREN);
     end;
    end;
    DoubleBuffered := False;
    FDrawing := False;
    FMark := nil;
    FMarksChanged := True;
  end;

  if Button = mbRight then
  begin
    FMark := MarkAtPos(Round(X / Kf_ZoomX), Round(Y / Kf_ZoomY));
    if Assigned(FMark) then
    begin
      HideMarkText;
      P := Point(X, Y);
      P := ClientToScreen(P);
      if TrwOneRepPreview(Owner).FSecurityRec.Rights in [0, 2] then
        TrwOneRepPreview(Owner).pmMark.Popup(P.X, P.Y);
    end;
  end;
end;

procedure TrwPreviewPage.Paint;
var
  R: TRect;
begin
  inherited;

  //Draw margins
  FCanvas.Pen.Style := psSolid;
  FCanvas.Brush.Color := clWhite;
  FCanvas.Brush.Style := bsSolid;

  R := Rect(0, 0, Round(FPrinterOffSets.X * PixelsPerInch * Kf_ZoomX/FPrnResX), Height);
  if RectVisible(FCanvas.Handle, R) then
    FCanvas.FillRect(R);
  R := Rect(0, 0, Width, Round(FPrinterOffSets.Y * PixelsPerInch * Kf_ZoomY/FPrnResY));
  if RectVisible(FCanvas.Handle, R) then
    FCanvas.FillRect(R);
  R := Rect(Width - Round(FPrinterOffSets.X * PixelsPerInch * Kf_ZoomX/FPrnResX), 0, Width, Height);
  if RectVisible(FCanvas.Handle, R) then
    FCanvas.FillRect(R);
  R := Rect(0 , Height - Round(FPrinterOffSets.Y * PixelsPerInch * Kf_ZoomY/FPrnResY), Width, Height);
  if RectVisible(FCanvas.Handle, R) then
    FCanvas.FillRect(R);

  FCanvas.Pen.Width := 1;    
  FCanvas.Pen.Color := clBlack;
  FCanvas.Brush.Style := bsClear;
  R := Rect(0, 0, Width, Height);
  FCanvas.Rectangle(R);
end;


function TrwPreviewPage.SearchWord(const AWord: String): Boolean;
var
  n: Word;
  C: TrwPrintObject;
  P: Int64;
begin
  Result := False;
  FData.Position := 0;

  while FData.Position < FData.Size do
  begin
    FData.ReadBuffer(n, SizeOf(n));
    p := FData.Position;
    C := TrwPrintObject(Repository.Components[n]);
    C.PrepareToPaint(FData);
    if (FHighlightInfo.TextObjPosition < p) and (C is TrwPrintText) and C.Visible then
    begin
      if Pos(AWord, AnsiUpperCase(TrwPrintText(C).Text)) > 0 then
      begin
        ClearHighlighting;
        FHighlightInfo.Text := AWord;
        FHighlightInfo.TextObjPosition := p;
        FHighlightInfo.TextRect := Rect(C.Left, C.Top, C.Right, C.Bottom);
        Result := True;
        break;
      end;
    end;
  end;
end;

procedure TrwPreviewPage.ClearHighlighting;
var
  R: TRect;
begin
  R := ScaleRect(FHighlightInfo.TextRect);
  FHighlightInfo.Text := '';
  FHighlightInfo.TextObjPosition := 0;
  FHighlightInfo.TextRect := Rect(0, 0, 0, 0);
  InvalidateRect(Handle, @R, False);
end;


procedure TrwOneRepPreview.actDownExecute(Sender: TObject);
begin
  if sbxPreview.VertScrollBar.IsScrollBarVisible then
    sbxPreview.VertScrollBar.Position := sbxPreview.VertScrollBar.Position + sbxPreview.VertScrollBar.Increment;
end;

procedure TrwOneRepPreview.actUpExecute(Sender: TObject);
begin
  if sbxPreview.VertScrollBar.IsScrollBarVisible then
    sbxPreview.VertScrollBar.Position := sbxPreview.VertScrollBar.Position - sbxPreview.VertScrollBar.Increment;
end;

procedure TrwOneRepPreview.actLeftExecute(Sender: TObject);
begin
  if sbxPreview.HorzScrollBar.IsScrollBarVisible then
    sbxPreview.HorzScrollBar.Position := sbxPreview.HorzScrollBar.Position - sbxPreview.HorzScrollBar.Increment;
end;

procedure TrwOneRepPreview.actRightExecute(Sender: TObject);
begin
  if sbxPreview.HorzScrollBar.IsScrollBarVisible then
    sbxPreview.HorzScrollBar.Position := sbxPreview.HorzScrollBar.Position + sbxPreview.HorzScrollBar.Increment;
end;

procedure TrwOneRepPreview.actHomeExecute(Sender: TObject);
begin
  if sbxPreview.VertScrollBar.IsScrollBarVisible then
    sbxPreview.VertScrollBar.Position := 0;
end;

procedure TrwOneRepPreview.actEndExecute(Sender: TObject);
begin
  if sbxPreview.VertScrollBar.IsScrollBarVisible then
    sbxPreview.VertScrollBar.Position := sbxPreview.VertScrollBar.Range;
end;

procedure TrwOneRepPreview.actCtrlDownExecute(Sender: TObject);
begin
  actEnd.Execute;
end;

procedure TrwOneRepPreview.actCtrlUpExecute(Sender: TObject);
begin
  actHome.Execute;
end;

procedure TrwOneRepPreview.actCtrlLeftExecute(Sender: TObject);
begin
  if sbxPreview.HorzScrollBar.IsScrollBarVisible then
    sbxPreview.HorzScrollBar.Position := 0;
end;

procedure TrwOneRepPreview.actCtrlRightExecute(Sender: TObject);
begin
  if sbxPreview.HorzScrollBar.IsScrollBarVisible then
    sbxPreview.HorzScrollBar.Position := sbxPreview.HorzScrollBar.Range;
end;

procedure TrwOneRepPreview.actJumpToPageExecute(Sender: TObject);
var
  P: Extended;
begin
  if Assigned(FPage) and (FPage.Header.PagesCount > 1) then
  begin
    P := FPage.PageNum;
    if PrvNbrDialog('Jump onto page', 'Page number', P, 1, 1, FPage.Header.PagesCount) then
      ShowPage(Trunc(P));
    InvalidateRect(self.ParentWindow, nil, false);
    RedrawWindow(self.ParentWindow, PRect(nil), HRGN(nil), RDW_ALLCHILDREN);
  end;
end;

procedure TrwOneRepPreview.pnlPageNumClick(Sender: TObject);
begin
  actJumpToPage.Execute;
end;


procedure TrwOneRepPreview.Changetext1Click(Sender: TObject);
begin
  FPage.DblClick;
end;

procedure TrwOneRepPreview.Deletemark1Click(Sender: TObject);
begin
  FPage.DeleteSelectedMark;
end;

procedure TrwPreviewPage.ScaleSize;
begin
  DefinePrintMetric;
  TrwOneRepPreview(Owner).SyncFitting;
end;

procedure TrwOneRepPreview.ChangeASCIICursorPos(Sender: TObject);
begin
  pnlPos.Caption := IntToStr(FmeASCII.CaretY)+' : '+ IntToStr(FmeASCII.CaretX);
  if FmeASCII.CaretX > Length(FmeASCII.LineText) then
    pnlChar.Caption := ''
  else
    pnlChar.Caption := IntToStr(Ord(FmeASCII.LineText[FmeASCII.CaretX]));
end;


procedure TrwOneRepPreview.SaveCurrentDataToFile(const AFileName: String);
var
  lFile, lFile2: TStream;
  lPos: LongInt;
  fmode: Word;
  n: Cardinal;
  flRWA: Boolean;

  procedure CreatePDF;
  var
    lPos: LongInt;
    SecInfo: TrwPDFInfoRec;
  begin
    lPos := FStream.Position;
    try
      SecInfo.Author := '';
      SecInfo.Creator := 'RW Preview';
      SecInfo.Subject := 'RW Report';
      SecInfo.Title := AFileName;

      SecInfo.OwnerPassword := FSecurityRec.PrintPassword;
      SecInfo.UserPassword := FSecurityRec.PreviewPassword;

      SecInfo.Compression := FSecurityRec.Compressed;
// ticket 102131 - looks like condition outright wrong; user complains ...
//      if SecInfo.UserPassword <> SecInfo.OwnerPassword then
      if //SecInfo.UserPassword <> SecInfo.OwnerPassword
             (Length(SecInfo.UserPassword)=0)
         or
             (Length(SecInfo.OwnerPassword) = 0)
         or
              AnsiSameStr(SecInfo.OwnerPassword, 'nopassword')    // rwBasicPrint.pas ConvertRWAtoPDF - hardcoded
      then
        SecInfo.ProtectionOptions := []
      else
        SecInfo.ProtectionOptions := [pdfPrint, pdfCopyInformation];

      ConvertRWAtoPDF(FStream, FPage.Header.Marks, @SecInfo, AFileName);
    finally
      FStream.Position := lPos;
    end;
  end;

begin
  if SameText(ExtractFileExt(AFileName), '.PDF') then
  begin
    if actPrint.Enabled then
      CreatePDF;
    Exit;
  end;

  flRWA := False;

  if AnsiSameText(AFileName, FFileName) then
    fmode := fmOpenReadWrite or fmShareDenyNone
  else
    fmode := fmCreate;

  if FSecurityRec.Rights = 0  then
    lFile := TEvFileStream.Create(AFileName, fmode)
  else
    lFile := TTempFileStream.Create;

  lPos := FStream.Position;
  try
    flRWA := IsRWAFormat(FStream);
    FStream.Position := 0;
    lFile.CopyFrom(FStream, FStream.Size);
    lFile.Size := FStream.Size;

    if flRWA then
    begin
      if FPage.FMarksChanged then
      begin
        lFile.Position := lFile.Size - SizeOf(n);
        lFile.ReadBuffer(n, SizeOf(n));
        lFile.Position := n;
        FPage.WriteHeaderInfo(lFile);
        lFile.Size := lFile.Position;
      end;
    end;

    if FSecurityRec.Rights <> 0 then
    begin
      lFile.Position := 0;
      lFile2 := TEvFileStream.Create(AFileName, fmode);
      try
        SecureRWA(lFile, lFile2, FSecurityRec.PreviewPassword, FSecurityRec.PrintPassword, FSecurityRec.Compressed);
      finally
        lFile2.Free;
      end;
    end;

  finally
    FStream.Position := lPos;
    lFile.Free;
    FSecurityChanged := False;
    if flRWA and (fmode <> fmCreate) then
      FPage.FMarksChanged := False;
  end;

  SaveAdditionalChanges(AFileName);
end;


procedure TrwOneRepPreview.actSecurityExecute(Sender: TObject);
begin
  CloseSearchDialogs;
  if RWASecurity(FSecurityRec.PreviewPassword, FSecurityRec.PrintPassword, FSecurityRec.Compressed) then
  begin
    if (FSecurityRec.PreviewPassword = '') and (FSecurityRec.PrintPassword = '') and not FSecurityRec.Compressed then
      FSecurityRec.Rights := 0
    else
      FSecurityRec.Rights := 2;

    FSecurityChanged := True;
  end;
end;

procedure TrwOneRepPreview.actSearchTextExecute(Sender: TObject);
begin
  CloseSearchDialogs;
  if CurrentDataType = rwRTASCII then
    FmeASCII.CommandProcessor(ecFind, #0, nil)
  else if CurrentDataType = rwRTRWA then
    dlgSearch.Execute;
end;

procedure TrwOneRepPreview.sbxPreviewClick(Sender: TObject);
begin
  if Assigned(FPage) then
    FPage.SetFocus;
end;

procedure TrwOneRepPreview.OnParentShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
 if Assigned(FPage) and FPage.Focused then
   if aclActions.IsShortCut(Msg) then
     Handled := True;
end;


procedure TrwOneRepPreview.dlgSearchFind(Sender: TObject);
var
  R: TRect;
  n: Integer;
  PrevPage: Integer;
begin
  if dlgSearch.FindText <> '' then
  begin
    if AnsiUpperCase(dlgSearch.FindText) <> FPage.HighlightInfo.Text then
      FPage.ClearHighlighting;
    n := FPage.Header.PagesCount - FPage.PageNum + 1;
    PrevPage := FPage.PageNum;
    repeat
      if FPage.SearchWord(AnsiUpperCase(dlgSearch.FindText)) then
      begin
        if PrevPage = FPage.PageNum then
        begin
          R := FPage.ScaleRect(FPage.HighlightInfo.TextRect);
          InvalidateRect(FPage.Handle, @R, False);
        end;
        break;
      end;
      Dec(n);

      if n > 0 then
        FPage.PreparePage(FPage.PageNum + 1, FStream);

    until n = 0;

    if n = 0 then
    begin
      if PrevPage <> FPage.PageNum then
        ShowPage(PrevPage);
      PrvMessage('Text "' + dlgSearch.FindText + '" is not found', mtInformation, [mbOk]);
    end
    else
    begin
      if PrevPage <> FPage.PageNum then
        UpdatePage;
      ScrollInViewRect(FPage.ScaleRect(FPage.HighlightInfo.TextRect));
    end;
  end;
end;


procedure TrwOneRepPreview.dlgSearchShow(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to aclActions.ActionCount - 1 do
    if aclActions.Actions[i].Category = 'Navigation' then
      TCustomAction(aclActions.Actions[i]).Enabled := False;

end;

procedure TrwOneRepPreview.dlgSearchClose(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to aclActions.ActionCount - 1 do
    if aclActions.Actions[i].Category = 'Navigation' then
      TCustomAction(aclActions.Actions[i]).Enabled := True;
end;

procedure TrwOneRepPreview.ScrollInViewRect(const ARect: TRect);
var
  Rect: TRect;
begin
  Rect := ARect;
  Dec(Rect.Left, sbxPreview.HorzScrollBar.Margin);
  Inc(Rect.Right, sbxPreview.HorzScrollBar.Margin);
  Dec(Rect.Top, sbxPreview.VertScrollBar.Margin);
  Inc(Rect.Bottom, sbxPreview.VertScrollBar.Margin);
  Rect.TopLeft := ScreenToClient(FPage.ClientToScreen(Rect.TopLeft));
  Rect.BottomRight := ScreenToClient(FPage.ClientToScreen(Rect.BottomRight));
  if Rect.Left < 0 then
    with sbxPreview.HorzScrollBar do
      Position := Position + Rect.Left
  else if Rect.Right > sbxPreview.ClientWidth then
  begin
    if Rect.Right - Rect.Left > sbxPreview.ClientWidth then
      Rect.Right := Rect.Left + sbxPreview.ClientWidth;
    with sbxPreview.HorzScrollBar do
      Position := Position + Rect.Right - sbxPreview.ClientWidth;
  end;

  if Rect.Top < 0 then
    with sbxPreview.VertScrollBar do
      Position := Position + Rect.Top

  else if Rect.Bottom > ClientHeight then
  begin
    if Rect.Bottom - Rect.Top > sbxPreview.ClientHeight then
      Rect.Bottom := Rect.Top + sbxPreview.ClientHeight;
    with sbxPreview.VertScrollBar do
      Position := Position + Rect.Bottom - sbxPreview.ClientHeight;
  end;
end;

procedure TrwOneRepPreview.BeforeDestruction;
begin
  dlgSearch.OnClose := nil;
  dlgSearch.CloseDialog;
  RemoveComponent(dlgSearch);
  dlgSearch.Free;
  dlgSearch := nil;
  inherited;
end;

procedure TrwPreviewPage.PreparePage(const APageNum: Integer; AData: TStream);
begin
  inherited;
  ClearHighlighting;
end;


procedure TrwOneRepPreview.PreviewReport(const pPreviewInfo: PTrwPreviewInfoRec; const AStandAloneUtilityMode : boolean = false);
var
  i: Integer;
  A: TActionList;
  h: String;
begin
  with pPreviewInfo^ do
  begin
    StandAloneUtilityMode := AStandAloneUtilityMode;

    cbReports.Clear;
    cbReports.Items.BeginUpdate;
    for i := Low(Reports) to High(Reports) do
    begin
      h := Reports[i].Name;
      if not Assigned(Reports[i].Data1) and not Assigned(Reports[i].Data2) and not Assigned(Reports[i].Data3) and (Reports[i].Data4 = '') then
        h := h + '     (EMPTY)';
      cbReports.AddItem(h, @Reports[i]);
    end;

    cbReports.Items.EndUpdate;

    pnlReports.Visible := cbReports.Items.Count > 1;
    cbReports.ItemIndex := 0;
    cbReports.OnChange(cbReports);

    if Parent <> 0 then
    begin
      A := aclActions;
      RemoveComponent(aclActions);
      Align := alClient;
      Self.Parent := FindControl(Parent);
      aclActions := A;
    end;
  end;
end;

procedure TrwOneRepPreview.cbReportsChange(Sender: TObject);
var
  R: PTrwRWAInfoRec;
begin
  BeforeClosePreview;

  if cbReports.ItemIndex = -1 then
    Exit;

  R := PTrwRWAInfoRec(cbReports.Items.Objects[cbReports.ItemIndex]);

  with R^ do
    if Assigned(Data1) or Assigned(Data2) or Assigned(Data3) or (Data4 <> '') then
    begin
      SecurePreview := SecuredMode;
      SetPrinterSettings(@PrintJobInfo);

      if Assigned(Data1) then
      begin
        FResultHolder := Data1;
        PreviewReport(FResultHolder.Data);
      end

      else if Assigned(Data2) then
        PreviewReport(Data2)

      else if Assigned(Data3) then
        PreviewReport(Data3)

      else if Data4 <> '' then
        PreviewReport(Data4);
    end;
end;

procedure TrwOneRepPreview.PreviewReport(AStream: IEvDualStream);
begin
  PreviewReport(AStream.RealStream);
end;

procedure TrwOneRepPreview.FixToolBarSize;
begin
  if pnlReports.Visible then
    tlbTools.Width := pnlReports.BoundsRect.Right
  else if pnlASCIIPos.Visible then
    tlbTools.Width := pnlASCIIPos.BoundsRect.Right
  else
    tlbTools.Width := pnlPages.BoundsRect.Right;
end;


procedure TrwOneRepPreview.CloseSearchDialogs;
begin
  FmeASCII.CloseSearchDialog;

  if dlgSearch.Handle <> 0 then
  begin
    dlgSearch.CloseDialog;
    Application.ProcessMessages;
  end;  
end;

procedure TrwOneRepPreview.ShowXMLReport(AStream: TStream);
var
//  StreamAdapter: IStream;
//  PersistStreamInit: IPersistStreamInit;
  Flags: OleVariant;
  F: TEvFileStream;
begin
  FStream := AStream;
  FStream.Position := 0;

  pnlPages.Visible := False;
  pnlASCIIPos.Visible := False;
  FixToolBarSize;

  sbxPreview.Visible := False;
  pnlBrowser.Visible := True;
  FWebBrowser := TWebBrowser.Create(Self);
  FWebBrowser.Align := alClient;
  pnlBrowser.InsertControl(FWebBrowser);

  SetButtons(FStream.Size > 0);

  FTempFile := PrvGetCachePath + 'Result.xml';
  F := TEvFileStream.Create(FTempFile, fmCreate);
  try
    F.CopyFrom(AStream, 0);
  finally
    FreeAndNil(F);
  end;

  Flags := navNoHistory or navNoReadFromCache or navNoWriteToCache;
  FWebBrowser.Navigate(WideString(FTempFile), Flags);

{
  Flags := navNoHistory or navNoReadFromCache or navNoWriteToCache;
  WebBrowser.Navigate(WideString('about:blank'), Flags);

  while WebBrowser.ReadyState <> READYSTATE_COMPLETE do
  begin
    Application.ProcessMessages;
    Sleep(0);
  end;

  Assert(Assigned(WebBrowser.Document));

  // Get IPersistStreamInit interface on document object
  if WebBrowser.Document.QueryInterface(IPersistStreamInit, PersistStreamInit) = S_OK then
  begin
    // Clear document
    if PersistStreamInit.InitNew = S_OK then
    begin
      // Get IStream interface on stream
      AStream.Position := 0;
      StreamAdapter:= TStreamAdapter.Create(AStream);
      // Load data from Stream into WebBrowser
      OleCheck(PersistStreamInit.Load(StreamAdapter));  // type of stream is unknown :(  thinking...
    end;
  end;
}

//  FmeASCII.Lines.LoadFromStream(AStream);
  actUp.Enabled := False;
  actDown.Enabled := False;
  actLeft.Enabled := False;
  actRight.Enabled := False;
  actHome.Enabled := False;
  actEnd.Enabled := False;
  actCtrlLeft.Enabled := False;
  actCtrlRight.Enabled := False;
  actCtrlUp.Enabled := False;
  actCtrlDown.Enabled := False;

  actSecurity.Enabled := False;
  actPrint.Enabled := False;
  actPrinterSetup.Enabled := False;
  actFirst.Enabled := False;
  actLast.Enabled := False;
  actNext.Enabled := False;
  actPrev.Enabled := False;
  actFitToWnd.Enabled := False;
  actAsIs.Enabled := False;
  actFitToWidth.Enabled := False;
  actMarking.Enabled := False;

  svdSaveResult.FileName := rwPrinter.Title;
end;

function TrwOneRepPreview.CurrentDataType: TrwResultFormatType;
begin
  if FmeASCII.Visible then
    Result := rwRTASCII
  else if pnlBrowser.Visible then
    Result := rwRTXML
  else if pnlExcel.Visible then
    Result := rwRTXLS
  else if sbxPreview.Visible then
    Result := rwRTRWA
  else
    Result := rwRTUnknown;
end;

procedure TrwOneRepPreview.ShowRWAReport(AStream: TStream);
begin
  sbxPreview.Visible := True;
  pnlPageNum.Caption := '';
  pnlReports.Visible := cbReports.Items.Count > 1;

  FPage := TrwPreviewPage.Create(Self);
  FStream := AStream;
  FPage.Parent := sbxPreview;

  FPage.Initialize(FStream);

  if FSecurityRec.Rights <> 0 then
    SecurePreview := False;

  FPage.PageNum := 0;
  FPrintDialog.FromPage := 1;
  FPrintDialog.ToPage := FPage.Header.PagesCount;
  FPrintDialog.MaxPage := FPage.Header.PagesCount;
  SetButtons(FPage.Header.PagesCount > 0);

  if FFileName <> '' then
    LoadAdditionalChanges;

  actNext.Execute;
end;


procedure TrwOneRepPreview.actSaveAllExecute(Sender: TObject);
var
  i : integer;
  Save_Cursor : TCursor;
  ReportExtention : String;
  FileName : String;
  oldTitle : String;
  oldFileName : String;
  oldFilter : String;
  OldItemIndex : integer;
  oldSecurityRec: TrwRWASecurityRec;
  tmpDir : String;

  // check if file exists and add "_???" number to its name
  function CheckIfFileExists(ADir, AFile, AExt : String) : String;
  var
    sr: TSearchRec;
    FilesList : TStringList;
    tmpStr : String;
    i : integer;
  begin
    result := AFile;
    if not FileExists(ADir + AFile + AExt) then
      exit;
    FilesList := TStringList.Create;
    try
      if FindFirst(ADir + AFile + '_???' + AExt, faAnyFile, sr) = 0 then
        try
          repeat
            tmpStr := ExtractFileName(sr.Name);
            tmpStr := Copy(tmpStr, 1, Length(tmpStr) - Length(ExtractFileExt(tmpStr))); // trow out extention
            tmpStr := Trim(Copy(tmpStr, Length(tmpStr) - 2, 3)); // getting the last three characters
            if length(tmpStr) < 3 then
              tmpStr := StringOfChar('0', 3 - Length(tmpStr)) + tmpStr;
            try
              StrToInt(tmpStr);
              FilesList.Add(tmpStr);
            except
              on E:Exception do
            end;
          until FindNext(sr) <> 0;
        finally
          FindClose(sr);
        end;
      if FilesList.Count > 0 then
      begin
        FilesList.Sort;
        i := StrToInt(FilesList[FilesList.count-1]) + 1;
        tmpStr := IntToStr(i);
        if length(tmpStr) < 3 then
          tmpStr := StringOfChar('0', 3 - Length(tmpStr)) + tmpStr;
        result := AFile + '_' + tmpStr;
      end
      else
        result := AFile + '_001';
    finally
      FilesList.Free;
    end;
  end;

  function  NormalizeFileName(const AFileName: String): String;
  var
    i: Integer;
    c: Char;
  begin
    Result := AFileName;

    for i := 1 to Length(Result) do
    begin
      c := AnsiUpperCase(Result[i])[1];
      if not((c >= '0') and (c <= '9') or
             (c >= 'A') and (c <= 'Z') or
             (Pos(c, ' .,;~_[]{}!@#$%^()-+=`') > 0)) then
        Result[i] := '_';
    end;
  end;

begin
  CloseSearchDialogs;

  oldTitle := svdSaveResult.Title;
  OldFileName := svdSaveResult.Filename;
  OldFilter := svdSaveResult.Filter;
  oldItemIndex := cbReports.ItemIndex;
  oldSecurityRec.Rights := FSecurityRec.Rights;
  oldSecurityRec.Compressed := FSecurityRec.Compressed;
  oldSecurityRec.PreviewPassword := FSecurityRec.PreviewPassword;
  oldSecurityRec.PrintPassword := FSecurityRec.PrintPassword;

  svdSaveResult.FileName := 'Filename will be assigned from report name';
  svdSaveResult.Filter := 'File format will be assigned from report format (*.*)|*.*|PDF File (*.pdf)|*.pdf';
  svdSaveResult.FilterIndex := 1;
  ReportExtention := '.rwa';
  svdSaveResult.Title := 'Save All Report Writer result files';
  tmpDir := '';

  try
    if svdSaveResult.Execute then
    begin
      tmpDir := ExtractFilePath(svdSaveResult.FileName);
      Save_Cursor := Screen.Cursor;
      Screen.Cursor := crHourGlass;
      try
        try
          for i := 0 to cbReports.Items.Count - 1 do
          begin
            try
              FnoShowPage := true;
              cbReports.ItemIndex := i;
              cbReports.OnChange(cbReports);
              if not actSave.Enabled then
                continue;
              if svdSaveResult.FilterIndex = 1 then // current format
              begin
                if CurrentDataType = rwRTRWA then
                  ReportExtention := '.rwa'
                else if CurrentDataType = rwRTASCII then
                    ReportExtention := '.txt'
                else if CurrentDataType = rwRTXLS then
                    ReportExtention := '.xls'
                else if CurrentDataType = rwRTXML then
                    ReportExtention := '.xml';
              end
              else // PDF
                if CurrentDataType = rwRTRWA then
                  ReportExtention := '.pdf'
                else
                begin
                  Application.MessageBox(PChar('Cannot conver report: ' + cbReports.Items[i] + #13 + ' to PDF format.'), 'Information');
                  continue;
                end;
              FileName := CheckIfFileExists(tmpDir, NormalizeFileName(Trim(cbReports.Items[i])), ReportExtention);
              // saving with the same security
              FSecurityRec.Rights := oldSecurityRec.Rights;
              FSecurityRec.Compressed := oldSecurityRec.Compressed;
              FSecurityRec.PreviewPassword := oldSecurityRec.PreviewPassword;
              FSecurityRec.PrintPassword := oldSecurityRec.PrintPassword;
              SaveCurrentDataToFile(tmpDir + FileName + ReportExtention);
            except
              on E:Exception do
                Application.MessageBox(PChar('Cannot save report: ' + cbReports.Items[i] + #13 + 'Error: ' + E.Message), 'Error');
            end;
          end;  // for
        finally
          FnoShowPage := false;
          cbReports.ItemIndex := oldItemIndex;
          cbReports.OnChange(cbReports);
        end;
      finally
        Screen.Cursor := Save_Cursor;
      end;
    end;  // if
  finally
    svdSaveResult.Title := oldTitle;
    svdSaveResult.Filename := OldFileName;
    svdSaveResult.Filter := OldFilter;
  end;

  // Redraw whole window in order to avoid refresh issue when Preview window is embedded into Evolution
  InvalidateRect(self.ParentWindow, nil, false);
  RedrawWindow(self.ParentWindow, PRect(nil), HRGN(nil), RDW_ALLCHILDREN);
end;

function TrwOneRepPreview.PreparePreviewInfo(
  const AFiles: TStrings): PTrwPreviewInfoRec;
var
  i, j : integer;
begin
  Result := New(PTrwPreviewInfoRec);
  Result.Caption := '';
  Result.Owner := self.Owner;
  Result.SingleInstance := false;
  Result.PreviewFrame := nil;
  Result.Parent := 0;
  SetLength(Result.Reports, AFiles.Count);
  for i := 0 to AFiles.Count - 1 do
  begin
    Result.Reports[i].Name := ExtractFileName(AFiles[i]);
    j := LastDelimiter('.', Result.Reports[i].Name);
    if j > 0 then
      Delete(Result.Reports[i].Name, j, Length(Result.Reports[i].Name));
    Result.Reports[i].Data1 := nil;
    Result.Reports[i].Data2 := nil;
    Result.Reports[i].Data3 := nil;
    Result.Reports[i].Data4 := AFiles[i];
    Result.Reports[i].SecuredMode := false;
    Result.Reports[i].PrintPswd := '';
    Result.Reports[i].PreviewPswd := '';
  end;  // for
end;

procedure TrwOneRepPreview.SetBoundsExt(const ABounds: TrwBounds);
begin
  if HandleAllocated then
    PostMessage(Handle, CM_SET_BOUNDS_EXT,
      MakeWParam(ABounds.Left, ABounds.Top), MakeLParam(ABounds.Width, ABounds.Height));
end;

procedure TrwOneRepPreview.CMSetBoundExt(var Message: TMessage);
begin
  SetBounds(Message.WParamLo, Message.WParamHi, Message.LParamLo, Message.LParamHi);
end;

procedure TrwOneRepPreview.ShowXLSReport(AStream: TStream);
var
  F: TEvFileStream;
begin
  FStream := AStream;
  FStream.Position := 0;

  pnlPages.Visible := False;
  pnlASCIIPos.Visible := False;
  FixToolBarSize;

  sbxPreview.Visible := False;
  pnlExcel.Visible := True;

  SetButtons(FStream.Size > 0);

  FTempFile := PrvGetCachePath + 'Result.xls';
  try
    F := TEvFileStream.Create(FTempFile, fmCreate);
  except
    Randomize;
    FTempFile := PrvGetCachePath + 'Temp_' + IntToStr(Random(10000)) + '.xls';
    F := TEvFileStream.Create(FTempFile, fmCreate);
  end;
  try
    F.CopyFrom(AStream, 0);
  finally
    FreeAndNil(F);
  end;

  Self.Top := 0;
  Self.Left := 0;
  Self.Height := Parent.ClientHeight;
  Self.Width := Parent.ClientWidth;
  TForm(Parent).VertScrollBar.Visible := false;

  actUp.Enabled := False;
  actDown.Enabled := False;
  actLeft.Enabled := False;
  actRight.Enabled := False;
  actHome.Enabled := False;
  actEnd.Enabled := False;
  actCtrlLeft.Enabled := False;
  actCtrlRight.Enabled := False;
  actCtrlUp.Enabled := False;
  actCtrlDown.Enabled := False;
  actSearchText.Enabled := false;

  actSecurity.Enabled := False;
  actPrint.Enabled := False;
  actPrinterSetup.Enabled := False;
  actFirst.Enabled := False;
  actLast.Enabled := False;
  actNext.Enabled := False;
  actPrev.Enabled := False;
  actFitToWnd.Enabled := False;
  actAsIs.Enabled := False;
  actFitToWidth.Enabled := False;
  actMarking.Enabled := False;

  svdSaveResult.FileName := rwPrinter.Title;

  if StandAloneUtilityMode then
    try
      RunIsolatedProcess(FTempFile, '', []);
    except
      on E: Exception do
        PrvMessage(E.Message, mtError, [mbOk]);
    end;
end;

procedure TrwOneRepPreview.btnViewInExcelClick(Sender: TObject);
begin
  try
    RunIsolatedProcess(FTempFile, '', []);
  except
    on E: Exception do
      PrvMessage(E.Message, mtError, [mbOk]);
  end
end;
function TrwPreviewPage.TextAtPos(const X, Y: Word; var AItemIndex: integer): TrwPrintText;
var
  n: Word;
  C: TrwPrintObject;
  P: TPoint;
  LabelChange: String;
begin
  Result := nil;
  AItemIndex := -1;
  P := Point(X, Y);

  FData.Position := 0;

  while FData.Position < FData.Size do
  begin
    FData.ReadBuffer(n, SizeOf(n));
    C := TrwPrintObject(Repository.Components[n]);
    C.PrepareToPaint(FData);
    if (C is TrwPrintText) and C.Visible then
    begin
      if PtInRect(Rect(C.Left, C.Top, C.Right, C.Bottom), P)  then
      begin
        Result := TrwPrintText(C);
        AItemIndex := Result.ItemPosOnPage;
        LabelChange := Header.LabelChangesList.FindChange(PageNum, AItemIndex);
        if LabelChange <> '' then
          Result.Text := LabelChange;
        exit;
      end;
    end;
  end;  // while
end;

procedure TrwPreviewPage.EditSelectedText(const ATextObject: TrwPrintText);
var
  s: String;
begin
  if Assigned(ATextObject) then
  begin
    s := PrvDefaultDialog('Edit values', 'New value for the label', ATextObject.Text);
    if s <> ATextObject.Text then
    begin
      Header.LabelChangesList.AddChange(PageNum, ATextObject.ItemPosOnPage, s);
      FLabelsEdited := true;
      Invalidate;
    end;
  end;
end;

procedure TrwOneRepPreview.LoadAdditionalChanges;
var
  AdditionalDataFileName: String;
  AdditionalDataStream: IevDualStream;
  AdditionalData: IisListOfValues;
  LabelChanges: IrwLabelChanges;
begin
  AdditionalDataFileName := FFileName + '.dat';
  if FileExists(AdditionalDataFileName) then
  begin
    AdditionalData := TisListOfValues.Create;
    try
      AdditionalDataStream := TEvDualStreamHolder.CreateFromFile(AdditionalDataFileName);
      AdditionalData.ReadFromStream(AdditionalDataStream);
      if AdditionalData.ValueExists(LabelChangesName) then
      begin
        LabelChanges := IInterface(AdditionalData.Value[LabelChangesName]) as IrwLabelChanges;
        FPage.Header.LabelChangesList.AsStream := LabelChanges.AsStream;
      end;
    except
      on E: Exception do
        PrvMessage('Cannot to read additional changes for rwa file!'#13 + E.Message, mtError, [mbOk]);
    end;
  end;
end;

procedure TrwOneRepPreview.SaveAdditionalChanges(const AOriginalFileName: String);
var
  AdditionalDataFileName: String;
  AdditionalDataStream: IevDualStream;
  AdditionalData: IisListOfValues;
  LabelChanges: IrwLabelChanges;
begin
  if SameText(ExtractFileExt(AOriginalFileName), '.RWA') then
    try
      AdditionalDataFileName := AOriginalFileName + '.dat';
      LabelChanges := FPage.Header.LabelChangesList;
      if LabelChanges.ChangesCount > 0 then
      begin
        AdditionalData := TisListOfValues.Create;
        AdditionalData.AddValue(LabelChangesName, LabelChanges);
        AdditionalDataStream := TEvDualStreamHolder.CreateFromNewFile(AdditionalDataFileName);
        AdditionalData.WriteToStream(AdditionalDataStream);
      end
      else
      begin
        if FileExists(AdditionalDataFileName) then
          DeleteFile(AdditionalDataFileName);
      end;
      FPage.FLabelsEdited := False;
    except
      on E: Exception do
        PrvMessage('Cannot to save additional changes for rwa file!'#13 + E.Message, mtError, [mbOk]);
    end;
end;

procedure TrwOneRepPreview.SpeedButton1Click(Sender: TObject);
begin
  try
    RunIsolatedProcess(FTempFile, '', []);
  except
    on E: Exception do
      PrvMessage(E.Message, mtError, [mbOk]);
  end
end;

end.

