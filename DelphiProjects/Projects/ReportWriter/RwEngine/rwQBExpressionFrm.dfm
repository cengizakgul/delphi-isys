object rwQBExpress: TrwQBExpress
  Left = 0
  Top = 0
  Width = 272
  Height = 74
  AutoScroll = False
  TabOrder = 0
  DesignSize = (
    272
    74)
  object lExpr: TLabel
    Left = 0
    Top = 0
    Width = 51
    Height = 13
    Caption = 'Expression'
  end
  inline pnlExpr: TrwQBExprPanel
    Left = 0
    Top = 15
    Width = 272
    Height = 59
    Anchors = [akLeft, akTop, akRight, akBottom]
    AutoScroll = False
    PopupMenu = pnlExpr.pmClipbrd
    TabOrder = 0
    inherited pnlCanvas: TPanel
      Width = 272
      Height = 59
      OnClick = pnlExprpnlCanvasClick
    end
  end
end
