unit rwQBExprEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, rwQBInfo, rwQBExprPanelFrm, rwDataDictionary,
  Buttons, rwTypes, Grids, rwDesignClasses, Db, rwUtils, Variants,
  sbSQL, rwQBTableContentFrm, Wwdbigrd, Wwdbgrid, rwEngineTypes,
  ISBasicClasses, rwInputFormControls, rwBasicClasses, rwDsgnControls,
  rwDB, ImgList;

type

  TrwLookupValuesGrid = class (TrwDBGrid)
  private
    FOnRowChanged: TNotifyEvent;
    FOnDblClick: TNotifyEvent;
  protected
    function  CreateVisualControl: IrwVisualControl; override;
  public
    property OnRowChanged: TNotifyEvent read FOnRowChanged write FOnRowChanged;
    property OnDblClick: TNotifyEvent read FOnDblClick write FOnDblClick;
  end;

  TrwQBGetLookupDataset = function : TrwDataSet of object;

  TrwQBExprEditor = class(TForm)
    Panel2: TPanel;
    pcItem: TPageControl;
    tsField: TTabSheet;
    Label3: TLabel;
    lvFields: TListView;
    cbTables: TComboBox;
    tsValue: TTabSheet;
    Label1: TLabel;
    cbConst: TComboBox;
    pnlVal: TPanel;
    Label2: TLabel;
    cbExtParams: TComboBox;
    edValue: TEdit;
    pnlGrid: TPanel;
    tsFunction: TTabSheet;
    lFuncDecl: TLabel;
    tvFuncts: TTreeView;
    memFuncDescr: TMemo;
    Panel1: TPanel;
    btnPlus: TSpeedButton;
    btnMinus: TSpeedButton;
    btnMul: TSpeedButton;
    btnDiv: TSpeedButton;
    btnBrackets: TSpeedButton;
    btnDel: TSpeedButton;
    btnIns: TSpeedButton;
    btnArrayBrackets: TSpeedButton;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    pnlExpr: TrwQBExprPanel;
    Splitter1: TSplitter;
    Panel4: TPanel;
    btnClear: TButton;
    btnCheck: TButton;
    btnOK: TButton;
    btnCancel: TButton;
    ilPict: TImageList;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure lvFieldsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure tsFieldShow(Sender: TObject);
    procedure tsFunctionShow(Sender: TObject);
    procedure btnPlusClick(Sender: TObject);
    procedure btnMinusClick(Sender: TObject);
    procedure btnMulClick(Sender: TObject);
    procedure btnDivClick(Sender: TObject);
    procedure btnBracketsClick(Sender: TObject);
    procedure edValueKeyPress(Sender: TObject; var Key: Char);
    procedure btnInsClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure edValueExit(Sender: TObject);
    procedure cbConstChange(Sender: TObject);
    procedure grValRowChanged(Sender: TObject);
    procedure pcItemChanging(Sender: TObject; var AllowChange: Boolean);
    procedure cbTablesChange(Sender: TObject);
    procedure tvFunctsChange(Sender: TObject; Node: TTreeNode);
    procedure tvFunctsDblClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCheckClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnArrayBracketsClick(Sender: TObject);

  private
    FExpression: TrwQBExpression;
    FExpressions: TrwQBExpressions;
    FCurrentItemType: TrwQBExprItemType;
    FCurrentItemValue: Variant;
    FCurrentItemDescr: String;
    FInsertion: Boolean;
    FPrimaryTable: TrwQBSelectedTable;
    FTables: TrwQBSelectedTables;
    grVal: TrwLookupValuesGrid;
    FPLookupDataSet: TrwQBGetLookupDataset;

    procedure AddItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String);
    procedure UpdateItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String);
    procedure RemoveItem;
    procedure Initialize;
    procedure OnSelectItem(Sender: TObject);
    procedure SyncSelection(AItem: TrwQBExpressionItem);
    procedure SetButtons;
    procedure CheckValue;
    procedure SetResevedConstValue(AType: TrwQBExprItemType; var AValue: Variant; var ADescr: String);
    procedure AssignChValues(APage: TTabSheet);

  public
    class function ShowEditor(AExpression: TrwQBExpression; AType: TrwQBExprItemType; AValue: Variant; PLookupDataSet: TrwQBGetLookupDataset):Boolean;
  end;


  function CreateLookupList(AOwner: TrwDataDictTable; const AField: String; const ATableParameter: Boolean = False): TrwDataSet;

implementation

{$R *.DFM}

uses
  rwQBTableFrm, rwQueryBuilderFrm, rwCustomDataDictionary;


function CreateLookupList(AOwner: TrwDataDictTable; const AField: String; const ATableParameter: Boolean = False): TrwDataSet;
var
  h, lSelect, lFrom, lWhere : string;
  i, t: Integer;
  Attr: TDataDictField;
  Par: TrwDataDictParam;
  DDItem: TDataDictNamedItem;
  FldNames: TStringList;
  Obj: TDataDictTable;
  L: TStringList;

  function CreateSQL(AObj: TrwDataDictTable; ATblNum: Integer): string;
  var
    i: Integer;
    rwFK: TDataDictForeignKey;
    rwLogKey: TDataDictPrimKey;
  begin
    if lFrom <> '' then
      lFrom := lFrom + ', ';
    lFrom := lFrom + AObj.Name;
    if AObj.Params.Count > 0 then
      lFrom := lFrom + '(' + TrwDataDictParams(AObj.Params).ParamValueStr + ')';
    lFrom := lFrom + ' t' + IntToStr(ATblNum);

     if AObj.DisplayLogicalKey.Count > 0 then
       rwLogKey := AObj.DisplayLogicalKey
     else
       rwLogKey := AObj.LogicalKey;

    if rwLogKey.Count > 0 then
    begin
      Result :=  '';

      for i := 0 to rwLogKey.Count - 1 do
      begin
        if Result <> '' then
          Result := Result + ', ';

        rwFK := AObj.ForeignKeys.ForeignKeyByField(rwLogKey[i].Field);
        if Assigned(rwFK) then
        begin
          Result := Result + CreateSQL(TrwDataDictTable(rwFK.Table),  ATblNum + 1);
          if lWhere <> '' then
            lWhere := lWhere + ' AND ';
          lWhere := lWhere + 't' + IntToStr(ATblNum) + '.' + rwLogKey[i].Field.Name +
            ' += ' + 't' + IntToStr(ATblNum + 1) + '.' +
            rwFK.Table.PrimaryKey[0].Field.Name;
        end

        else
        begin
          Result := Result + 't' + IntToStr(ATblNum) + '.' + rwLogKey[i].Field.Name;
          if rwLogKey[i].Field.DisplayName <> '' then
            FldNames.Add(rwLogKey[i].Field.DisplayName)
          else
            FldNames.Add(rwLogKey[i].Field.Name);
        end;
      end
    end

    else
    begin
      Result := 't' + IntToStr(ATblNum) + '.' + AObj.PrimaryKey[0].Field.Name;
      if AObj.PrimaryKey[0].Field.DisplayName <> '' then
        FldNames.Add(AObj.PrimaryKey[0].Field.DisplayName)
      else
        FldNames.Add(AObj.PrimaryKey[0].Field.Name);
    end;
  end;

  procedure FieldRef(const ATbl: TrwDataDictTable; const AItem: String);
  var
    rwFK: TDataDictForeignKey;
  begin
    Attr := ATbl.Fields.FieldByName(AItem);
    rwFK := ATbl.ForeignKeys.ForeignKeyByField(Attr);
    if Assigned(rwFK) then
      Obj := rwFK.Table
    else
      if (ATbl.PrimaryKey.Count > 0) and (ATbl.PrimaryKey[0].Field = Attr) then
        Obj := ATbl;
    DDItem := Attr;
  end;

begin
  Result := nil;

  Obj := nil;
  DDItem := nil;

  if not Assigned(AOwner) then
    exit;

  if not ATableParameter then
    FieldRef(AOwner, AField)

  else
  begin
    Par := TrwDataDictParam(AOwner.Params.ParamByName(AField));
    if Assigned(Par) then
    begin
      DDItem := Par;
      if Assigned(TrwDataDictParam(Par).RefField) then
        FieldRef(TrwDataDictTable(TrwDataDictParam(Par).RefField.Table), TrwDataDictParam(Par).RefField.Name);
    end;
  end;

  FldNames := TStringList.Create;
  try
    if not Assigned(Obj) then
    begin
      L := TStringList.Create;
      try

        if DDItem is TDataDictField then
        begin
          L.Text := TDataDictField(DDItem).FieldValuesReal;
          if ATableParameter then
            TrwQBConsts(SQLConsts).ConstsByGroup(TDataDictField(DDItem).Table.Name + '.' + TDataDictField(DDItem).Name, L)
          else
            TrwQBConsts(SQLConsts).ConstsByGroup(AOwner.Name + '.' + AField, L);
        end
        else if DDItem is TDataDictParam then
        begin
          L.Text := TDataDictParam(DDItem).ParamValuesReal;
          TrwQBConsts(SQLConsts).ConstsByGroup(AOwner.Name + '.' + AField, L);
        end;

        if L.Count > 0 then
        begin
          Result := TrwBuffer.Create(nil);
          TrwBuffer(Result).TableName := '_QBLookup';
          h := '';
          TrwBuffer(Result).Fields.CreateField('_key', ftString, 64);
          TrwBuffer(Result).Fields.CreateField('text', ftString, 254);
          TrwBuffer(Result).Fields.CreateField('ConstVal', ftMemo, 0);
          TrwBuffer(Result).CreateBuffer;
          TrwBuffer(Result).Open;
          TrwBuffer(Result).Fields[0].DisplayLabel := 'Code';
          TrwBuffer(Result).Fields[0].DisplayWidth := 7;
          TrwBuffer(Result).Fields[1].DisplayWidth := 20;
          TrwBuffer(Result).Fields[2].Visible := False;

          for i := 0 to L.Count - 1 do
          begin
            TrwBuffer(Result).PAppend;

            if L.Objects[i] <> nil then
            begin
              TrwBuffer(Result).Fields[0].Value := '@' + TrwQBConst(L.Objects[i]).ConstName;
              TrwBuffer(Result).Fields[2].Value := TrwQBConst(L.Objects[i]).StrValue;
              TrwBuffer(Result).Fields[1].Value := TrwQBConst(L.Objects[i]).DisplayName +
                ' (' + StringReplace(TrwBuffer(Result).Fields[2].Value, #13, ', ', [rfReplaceAll]) + ')';
              TrwBuffer(Result).Fields[0].DisplayWidth := 20;
            end

            else
            begin
              TrwBuffer(Result).Fields[0].Value := L.Names[i];
              TrwBuffer(Result).Fields[1].Value := L.Values[L.Names[i]];
            end;

            TrwBuffer(Result).PPost;
          end;
          TrwBuffer(Result).PFirst;
        end

        else
        begin
          h := 'SELECT DISTINCT ' + AField + ' _key, ' + AField +
            ' FROM ' + AOwner.Name;
          if AOwner.Params.Count > 0 then
            h := h + '(' + TrwDataDictParams(AOwner.Params).ParamValueStr  +')';
        end;

      finally
        L.Free;
      end;

      if DDItem.DisplayName <> '' then
        FldNames.Add(DDItem.DisplayName)
      else
        FldNames.Add(DDItem.Name);
    end

    else
    begin
      t := 1;
      lSelect := 't' + IntToStr(t) + '.' +
        Obj.PrimaryKey[0].Field.Name + ' _key, ';
      lFrom := '';
      lWhere := '';
      lSelect  := lSelect + CreateSQL(TrwDataDictTable(Obj), t) +
        ' text';
      h := 'SELECT ' + lSelect + ' FROM ' + lFrom;
      if Length(lWhere) > 0 then
        h := h + ' WHERE ' + lWhere;
    end;

    if h <> '' then
    begin
      Result := TrwQuery.Create(nil);
      ShowSQLContents(h, nil, TrwQuery(Result));
      Result.Fields[0].Visible := False;
    end;

    for i := 1 to Result.Fields.Count - 1 do
      if Result.Fields[i].Visible then
        Result.Fields[i].DisplayLabel := FldNames[i-1];

  finally
    FldNames.Free;
  end;
end;


class function TrwQBExprEditor.ShowEditor(AExpression: TrwQBExpression; AType: TrwQBExprItemType; AValue: Variant; PLookupDataSet: TrwQBGetLookupDataset): Boolean;
var
  Frm: TrwQBExprEditor;
  lItem: TrwQBExpressionItem;
begin
  Result := False;

  Frm := TrwQBExprEditor.Create(nil);
  with Frm do
    try
      if Assigned(AExpression.Collection) then
      begin
        FExpressions := TrwQBExpressionsClass(AExpression.Collection.ClassType).Create(TCollectionItemClass(AExpression.ClassType));
        FExpressions.FOwner := TrwQBExpressions(AExpression.Collection).Owner;
      end
      else
      begin
        FExpressions := TrwQBExpressions.Create(TCollectionItemClass(AExpression.ClassType));
        FExpressions.FOwner := AExpression.Owner as TPersistent;
      end;

      FExpression := TrwQBExpression(FExpressions.Add);
      pnlExpr.Expression := FExpression;
      pnlExpr.OnSelectItem := OnSelectItem;
      pnlExpr.ReadOnly := False;

      FExpression.Assign(AExpression);

      FPLookupDataSet := PLookupDataSet;

      Initialize;
      pnlExpr.ReDraw;

      if AExpression.Count = 0 then
      begin
        pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[0]);
        lItem := TrwQBExpressionItem.Create(nil);
        lItem.ItemType := AType;
        lItem.Value := AValue;
        try
          SyncSelection(lItem);
        finally
          lItem.Free;
        end;
        FInsertion := True;
        SetButtons;
      end

      else if AExpression.Count > 0 then
      begin
        if Assigned(grVal.DataSource) then
          grVal.DataSource.PDisableControls;
        try
          pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[1]);
          SyncSelection(pnlExpr.SelectedItem.ExprItem);
        finally
          if Assigned(grVal.DataSource) then
            grVal.DataSource.PEnableControls;
        end;
      end;

      if ShowModal = mrOK then
      begin
        AExpression.Assign(FExpression);
        Result := True;
        Application.ProcessMessages;
      end;
    finally
      Free;
    end;
end;


procedure TrwQBExprEditor.FormDestroy(Sender: TObject);
begin
  FreeAndNil(grVal);
  FreeAndNil(FExpressions);
end;

procedure TrwQBExprEditor.AddItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String);
var
  Expr: TrwQBExpressionItem;
  i, j, n: Integer;
  Fnct: TrwQBFunction;
begin
  if not Assigned(pnlExpr.SelectedItem) then
    Exit;

  if (AType = eitOperation) and not FInsertion then
    UpdateItem(AType, AValue, ADescr)

  else
  begin
    Expr := FExpression.AddItem(AType, AValue);

    Expr.Description := ADescr;
    n := pnlExpr.SelectedItem.ComponentIndex;
    if n > 0 then
      Expr.Index := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[n - 1]).ExprItem.Index + 1
    else
      Expr.Index := 0;

    if AType = eitFunction then
    begin
      j := Expr.Index + 1;
      Fnct := TrwQBFunction(QBFunctions[QBFunctions.IndexOf(AValue)]);
      for i := 1 to Fnct.Params.Count do
      begin
        Fnct.GetParam(i, AType, AValue, ADescr);
        if AType <> eitNone then
        begin
          SetResevedConstValue(AType, AValue, ADescr);
          Expr := FExpression.AddItem(AType, AValue);
          Expr.Description := ADescr;
          Expr.Index := j;
          Inc(j);
        end;

        if i < Fnct.Params.Count then
        begin
          Expr := FExpression.AddItem(eitSeparator, ',');
          Expr.Index := j;
          Inc(j);
        end;
      end;

      Expr := FExpression.AddItem(eitBracket, ')');
      Expr.Index := j;
    end;

    pnlExpr.ReDraw;
    pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[n + 2]);
  end;
end;

procedure TrwQBExprEditor.Initialize;
var
  i: Integer;
  h: String;
  lPrevGrp: String;
  lGrp, N: TTreeNode;
  Obj: TrwDataDictTable;
begin
  if FExpressions.Owner is TrwQBFilterCondition then
  begin
    FPrimaryTable := TrwQBFilterNodeCollection(TrwQBFilterCondition(FExpressions.Owner).Collection).Table;
    FTables := TrwQBSelectedTables(FPrimaryTable.Collection);
  end
  else
  begin
    FPrimaryTable := nil;
    if FExpressions.Owner is TrwQBQuery then
      FTables := TrwQBQuery(FExpressions.Owner).SelectedTables
    else
      FTables := nil;
  end;

  cbTables.Clear;
  if Assigned(FTables) then
    for i := 0 to FTables.Count - 1 do
    begin
      if FQBFrm.WizardView and not TrwQBSelectedTable(FTables.Items[i]).IsSUBQuery then
      begin
        Obj := TrwQBSelectedTable(FTables.Items[i]).lqObject;
        h := Obj.DisplayName;
        if not AnsiSameText(Obj.GroupName, h) then
          h := h + ' [' + Obj.GroupName + ']';
      end

      else
        h := TrwQBSelectedTable(FTables.Items[i]).TableName;
      if TrwQBSelectedTable(FTables.Items[i]).Alias <> '' then
        h := TrwQBSelectedTable(FTables.Items[i]).Alias + '   ' + h;
      cbTables.Items.AddObject(h, TrwQBSelectedTable(FTables.Items[i]));
    end;

  cbTables.ItemIndex := cbTables.Items.IndexOfObject(FPrimaryTable);
  cbTables.OnChange(nil);
  tsField.TabVisible := (cbTables.Items.Count > 0);

  cbConst.Items.Clear;
  cbConst.Items.AddObject('Date', Pointer(Ord(eitDateConst)));
  cbConst.Items.AddObject('Float', Pointer(Ord(eitFloatConst)));
  cbConst.Items.AddObject('Integer', Pointer(Ord(eitIntConst)));
  cbConst.Items.AddObject('String', Pointer(Ord(eitStrConst)));
  cbConst.Items.AddObject('Lookup', Pointer(0));
  cbConst.Items.AddObject('Parameter', Pointer(Ord(eitParam)));
  cbConst.Items.AddObject('Null', Pointer(Ord(eitNull)));
  cbConst.Items.AddObject('True', Pointer(Ord(eitTrue)));
  cbConst.Items.AddObject('False', Pointer(Ord(eitFalse)));
  cbConst.Items.AddObject('Now', Pointer(Ord(eitDateConst)));
  cbConst.Items.AddObject('Today', Pointer(Ord(eitDateConst)));

  tvFuncts.Items.Clear;
  lPrevGrp := '';
  lGrp := nil;
  for i := 0 to QBFunctions.Count - 1 do
  begin
    if not (FExpression is TrwQBShowingField) and
       AnsiSameText(TrwQBFunction(QBFunctions[i]).Group, 'Aggregate Functions') then
      continue;

    if not AnsiSameText(lPrevGrp, TrwQBFunction(QBFunctions[i]).Group) then
    begin
      lPrevGrp := TrwQBFunction(QBFunctions[i]).Group;
      lGrp := tvFuncts.Items.AddChildObject(nil, lPrevGrp, nil);
      lGrp.ImageIndex := 0;
      lGrp.SelectedIndex := lGrp.ImageIndex;
      lGrp.StateIndex := lGrp.ImageIndex;
    end;

    N := tvFuncts.Items.AddChildObject(lGrp, TrwQBFunction(QBFunctions[i]).Name, QBFunctions[i]);
    N.ImageIndex := -1;
    N.SelectedIndex := N.ImageIndex;
    N.StateIndex := N.ImageIndex;
  end;

  if Assigned(FQBFrm.ExternalParams) then
  begin
    cbExtParams.Items.Clear;
    for i := 0 to FQBFrm.ExternalParams.Count - 1 do
      cbExtParams.Items.AddObject(FQBFrm.ExternalParams[i].ParamDisplayName, FQBFrm.ExternalParams[i]);
  end;
end;


procedure TrwQBExprEditor.FormShow(Sender: TObject);
var
  n: Integer;
begin
  n := lvFields.Columns[0].Width + lvFields.Columns[1].Width + GetSystemMetrics(SM_CYHSCROLL) + 4;
  if lvFields.Width < n then
    lvFields.Columns[0].Width := lvFields.Columns[0].Width - (n - lvFields.Width);
end;

procedure TrwQBExprEditor.btnClearClick(Sender: TObject);
begin
  pnlExpr.SelectedItem := nil;
  FExpression.Clear;
  pnlExpr.ReDraw;
  pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[0]);  
end;

procedure TrwQBExprEditor.lvFieldsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
   if not Assigned(lvFields.Selected) then
     Exit;

   lvFields.Selected.MakeVisible(False);
   AssignChValues(tsField);

   if not FInsertion then
     btnIns.Click;  
end;

procedure TrwQBExprEditor.tsFieldShow(Sender: TObject);
begin
   FCurrentItemType := eitField;
   AssignChValues(tsField);
end;

procedure TrwQBExprEditor.tsFunctionShow(Sender: TObject);
begin
  FCurrentItemType := eitFunction;
  AssignChValues(tsFunction);
end;

procedure TrwQBExprEditor.UpdateItem(AType: TrwQBExprItemType; AValue: Variant; ADescr: String);
var
  Fnct1, Fnct2: TrwQBFunction;
begin
  if not Assigned(pnlExpr.SelectedItem) then
    Exit;

  if ((AType = eitOperation) and (pnlExpr.SelectedItem.ExprItem.ItemType = AType) or
     (pnlExpr.SelectedItem.ExprItem.ItemType >= eitField) and (AType >= eitField) or
     (AType = eitFunction) and (pnlExpr.SelectedItem.ExprItem.ItemType = AType)) and
     ((pnlExpr.SelectedItem.ExprItem.ItemType <> AType) or
      (String(pnlExpr.SelectedItem.ExprItem.Value) <> String(AValue))) then
  begin
    if AType = eitFunction then
    begin
      Fnct1 := TrwQBFunction(QBFunctions[QBFunctions.IndexOf(pnlExpr.SelectedItem.ExprItem.Value)]);
      Fnct2 := TrwQBFunction(QBFunctions[QBFunctions.IndexOf(AValue)]);
      if Fnct1.Params.Count <> Fnct2.Params.Count then
        Exit
      else
        pnlExpr.SelectedItem.ExprItem.Value := AValue;
    end

    else
    begin
      pnlExpr.SelectedItem.ExprItem.ItemType := AType;
      pnlExpr.SelectedItem.ExprItem.Value := AValue;
      pnlExpr.SelectedItem.ExprItem.Description := ADescr;
    end;

    pnlExpr.ReDraw;
  end;
end;

procedure TrwQBExprEditor.btnPlusClick(Sender: TObject);
begin
  AddItem(eitOperation, '+', '');
end;

procedure TrwQBExprEditor.btnMinusClick(Sender: TObject);
begin
  AddItem(eitOperation, '-', '');
end;

procedure TrwQBExprEditor.btnMulClick(Sender: TObject);
begin
  AddItem(eitOperation, '*', '');
end;

procedure TrwQBExprEditor.btnDivClick(Sender: TObject);
begin
  AddItem(eitOperation, '/', '');
end;

procedure TrwQBExprEditor.btnBracketsClick(Sender: TObject);
var
  n: Integer;
begin
  AddItem(eitBracket, '(', '');
  n := pnlExpr.SelectedItem.ComponentIndex;
  AddItem(eitBracket, ')', '');
  pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[n]);
end;

procedure TrwQBExprEditor.OnSelectItem(Sender: TObject);
begin
  if Assigned(pnlExpr.SelectedItem) then
    SyncSelection(pnlExpr.SelectedItem.ExprItem);
end;

procedure TrwQBExprEditor.SyncSelection(AItem: TrwQBExpressionItem);
var
  i: Integer;
  h: String;
  v: Variant;
  L: TrwQBExprItemLabel;
  ExtPar: TrwQBExtParam;

  procedure SelectFunction;
  var
    i: Integer;
  begin
    pcItem.ActivePage := tsFunction;
    for i := 0 to tvFuncts.Items.Count - 1 do
      if AnsiSameText(tvFuncts.Items[i].Text, AItem.Value) then
      begin
        tvFuncts.Items[i].Selected := True;
        break;
      end;
  end;

  procedure WrongLookup;
  begin
    AItem.ItemType := eitStrConst;
    AItem.Value := 'UNKNOWN LOOKUP';
    AItem.Description := '';
    pnlExpr.Redraw;
  end;


begin
  btnPlus.Down := False;
  btnMinus.Down := False;
  btnMul.Down := False;
  btnDiv.Down := False;
  lvFields.Selected := nil;
  cbConst.ItemIndex := -1;
  cbConst.OnChange(nil);
  edValue.Text := '';
  cbExtParams.ItemIndex := -1;

   if Assigned(AItem) then
   begin
     case AItem.ItemType of
       eitOperation:
         begin
           case String(AItem.Value)[1] of
             '+': btnPlus.Down := True;
             '-': btnMinus.Down := True;
             '*': btnMul.Down := True;
             '/': btnDiv.Down := True;
           end;
         end;

       eitBracket:
         begin
           L := pnlExpr.FindPairBracket(pnlExpr.SelectedItem.ComponentIndex);
           if Assigned(L) and (L.ExprItem.ItemType = eitFunction) then
           begin
             AItem := L.ExprItem;
             SelectFunction;
           end;
         end;

       eitSeparator:
         begin
           L := pnlExpr.FindFunction(pnlExpr.SelectedItem.ComponentIndex);
           if Assigned(L) and (L.ExprItem.ItemType = eitFunction) then
           begin
             AItem := L.ExprItem;
             SelectFunction;
           end;
         end;

       eitFunction:
         SelectFunction;

       eitField:
         begin
           pcItem.ActivePage := tsField;
           i := cbTables.Items.IndexOfObject(FTables.TableByInternalName(AItem.Description));
           if i <> cbTables.ItemIndex then
           begin
             cbTables.ItemIndex := i;
             cbTables.OnChange(nil);
           end;

           for i := 0 to lvFields.Items.Count - 1 do
           begin
             if TObject(lvFields.Items[i].Data) is TrwQBShowingField then
               h := TrwQBShowingField(lvFields.Items[i].Data).InternalName
             else if TObject(lvFields.Items[i].Data) is TrwDataDictField then
               h := TrwDataDictField(lvFields.Items[i].Data).Name;
             if AnsiSameText(AItem.Value, h) then
             begin
               lvFields.Items[i].Selected := True;
               break;
             end;
           end;
         end;

       eitParam..eitDateConst:
         begin
           pcItem.ActivePage := tsValue;
           if AItem.IsDescrNotEmpty and (AItem.ItemType <> eitParam) then
           begin
             cbConst.ItemIndex := cbConst.Items.IndexOfObject(Pointer(0));
             cbConst.OnChange(nil);
             try
               v := AItem.Value;
               if AItem.ItemType = eitConst then
                 v := '@' + AItem.Value;
               if Assigned(grVal.DataSource) and (grVal.DataSource.Fields[0].Value <> v) then
                 if not grVal.DataSource.PLocate('_key', v, False, False) then
                   WrongLookup;
             except
               WrongLookup;
             end;
           end
           else
           begin
             cbConst.ItemIndex := cbConst.Items.IndexOfObject(Pointer(Ord(AItem.ItemType)));
             if (AItem.ItemType = eitDateConst) and (AItem.Value <> null) then
             begin
               if AnsiSameText(AItem.Value, 'Now') then
                 cbConst.ItemIndex := cbConst.Items.IndexOf('Now');
               if AnsiSameText(AItem.Value, 'Today') then
                 cbConst.ItemIndex := cbConst.Items.IndexOf('Today');
             end;
             cbConst.OnChange(nil);
           end;

           if (AItem.ItemType = eitParam) and Assigned(FQBFrm.ExternalParams) then
           begin
             ExtPar := FQBFrm.ExternalParams.ParamByName(VarAsType(AItem.Value, varString));
             cbExtParams.ItemIndex := cbExtParams.Items.IndexOfObject(ExtPar);
           end
           else
             edValue.Text := VarAsType(AItem.Value, varString);
         end;

     else
       pcItem.ActivePage := tsValue;
       cbConst.ItemIndex := cbConst.Items.IndexOfObject(Pointer(Ord(AItem.ItemType)));
       cbConst.OnChange(nil);
     end;

     FInsertion := False;
   end
   else
     FInsertion := True;

   SetButtons;
end;

procedure TrwQBExprEditor.SetButtons;

  procedure EnableButtons(AEnable: Boolean);
  begin
    btnPlus.Enabled := AEnable;
    btnMinus.Enabled := AEnable;
    btnMul.Enabled := AEnable;
    btnDiv.Enabled := AEnable;
  end;

begin
  if FInsertion or not Assigned(pnlExpr.SelectedItem.ExprItem) then
  begin
    btnDel.Enabled := False;
    btnBrackets.Enabled := True;
    btnArrayBrackets.Enabled := True;
    EnableButtons(True);
  end

  else
  begin
    btnDel.Enabled := True;
    btnBrackets.Enabled := False;
    btnArrayBrackets.Enabled := False;    

    case pnlExpr.SelectedItem.ExprItem.ItemType of
      eitOperation: EnableButtons(True);

      eitBracket:   EnableButtons(False);

      eitSeparator: begin
                      btnDel.Enabled := False;
                      btnBrackets.Enabled := False;
                      btnArrayBrackets.Enabled := False;
                      EnableButtons(False);
                    end;
    else
      EnableButtons(False);
    end;
  end;

  btnIns.Enabled := FInsertion;
end;

procedure TrwQBExprEditor.RemoveItem;
var
  L1, L2: TrwQBExprItemLabel;
  n: Integer;

  procedure DeleteFunction(ALeftExpr: TrwQBExpressionItem; ARightExpr: TrwQBExpressionItem);
  var
    i: Integer;
  begin
    i := ALeftExpr.Index;
    while TrwQBExpressionItem(FExpression.Items[i]) <> ARightExpr do
      TrwQBExpressionItem(FExpression.Items[i]).Free;
    ARightExpr.Free;
  end;

begin
  if not (Assigned(pnlExpr.SelectedItem) and Assigned(pnlExpr.SelectedItem.ExprItem))  then
    Exit;

  if pnlExpr.SelectedItem.ExprItem.ItemType = eitSeparator then
    Exit;

  n := pnlExpr.SelectedItem.ComponentIndex;
  L1 := pnlExpr.SelectedItem;
  pnlExpr.SelectedItem := nil;

  if L1.ExprItem.ItemType = eitBracket then
  begin
    L2 := pnlExpr.FindPairBracket(L1.ComponentIndex);
    if Assigned(L2) then
      if Assigned(L2.ExprItem) and (L2.ExprItem.ItemType = eitFunction) then
        DeleteFunction(L2.ExprItem, L1.ExprItem)
      else
      begin
        L2.ExprItem.Free;
        L1.ExprItem.Free;
      end;
  end

  else if L1.ExprItem.ItemType = eitFunction then
  begin
    L2 := pnlExpr.FindPairBracket(L1.ComponentIndex);
    if Assigned(L2) then
      DeleteFunction(L1.ExprItem, L2.ExprItem);
  end

  else
    L1.ExprItem.Free;

  pnlExpr.ReDraw;

  if n > pnlExpr.pnlCanvas.ComponentCount - 1 then
    if pnlExpr.pnlCanvas.ComponentCount - 2 < 0 then
      pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[pnlExpr.pnlCanvas.ComponentCount - 1])
    else
      pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[pnlExpr.pnlCanvas.ComponentCount - 2])
  else
    pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[n]);    
end;


procedure TrwQBExprEditor.edValueKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
    btnIns.Click;
end;

procedure TrwQBExprEditor.CheckValue;
begin
  if cbConst.ItemIndex = -1 then
    raise ErwException.Create('Value type is empty')
  else
    FCurrentItemType := TrwQBExprItemType(Integer(cbConst.Items.Objects[cbConst.ItemIndex]));

  case FCurrentItemType of
    eitParam:       begin
                      if edValue.Visible then
                      begin
                        edValue.Text := Trim(edValue.Text);
                        if (Length(edValue.Text) = 0) or (not (edValue.Text[1] in ['A'..'z'])) then
                          raise ErwException.Create('Name of parameter is incorrect');
                        FCurrentItemValue := edValue.Text;
                      end
                      else
                      begin
                        if cbExtParams.ItemIndex = -1 then
                          raise ErwException.Create('Parameter is not selected');
                        FCurrentItemValue := FQBFrm.ExternalParams[cbExtParams.ItemIndex].ParamName;
                        FCurrentItemDescr := FQBFrm.ExternalParams[cbExtParams.ItemIndex].ParamDisplayName;
                      end;
                    end;

    eitIntConst:    begin
                      edValue.Text := Trim(edValue.Text);
                      FCurrentItemValue := StrToInt(edValue.Text);
                    end;

    eitFloatConst:  begin
                      edValue.Text := Trim(edValue.Text);
                      FCurrentItemValue := StrToFloat(edValue.Text);
                    end;

    eitStrConst:    begin
                      edValue.Text := StringReplace(edValue.Text, '''', '', [rfReplaceAll]);
                      FCurrentItemValue := edValue.Text;
                    end;

    eitDateConst:   begin
                      if AnsiSameText(cbConst.Items[cbConst.ItemIndex], 'Now') then
                        FCurrentItemValue := 'Now'
                      else if AnsiSameText(cbConst.Items[cbConst.ItemIndex], 'Today') then
                        FCurrentItemValue := 'Today'
                      else
                      begin
                        edValue.Text := Trim(edValue.Text);
                        if AnsiSameText(edValue.Text, 'now') or AnsiSameText(edValue.Text, 'today') then
                          FCurrentItemValue := edValue.Text
                        else
                          FCurrentItemValue := StrToDateTime(edValue.Text);
                      end;
                    end;

    eitNull,
    eitTrue,
    eitFalse:       SetResevedConstValue(FCurrentItemType, FCurrentItemValue, FCurrentItemDescr);
  end;
end;

procedure TrwQBExprEditor.btnInsClick(Sender: TObject);
begin
  if (pcItem.ActivePage = tsValue) and (FCurrentItemDescr = '') then
    CheckValue;
  
  if FInsertion then
    AddItem(FCurrentItemType, FCurrentItemValue, FCurrentItemDescr)
  else
    UpdateItem(FCurrentItemType, FCurrentItemValue, FCurrentItemDescr);
end;

procedure TrwQBExprEditor.btnDelClick(Sender: TObject);
begin
  RemoveItem;
end;

procedure TrwQBExprEditor.edValueExit(Sender: TObject);
begin
  if not FInsertion and pnlVal.Visible then
    btnIns.Click;
end;

procedure TrwQBExprEditor.cbConstChange(Sender: TObject);
begin
  if cbConst.ItemIndex = -1 then
  begin
    pnlVal.Visible := False;
    grVal.Visible := False;
  end

  else
  begin
    if Integer(cbConst.Items.Objects[cbConst.ItemIndex]) = 0 then
    begin
      if Assigned(FPLookupDataSet) and (not Assigned(grVal.DataSource) or not Boolean(grVal.DataSource.PActive)) then
      begin
        grVal.DataSource := FPLookupDataSet;
        grVal.DataSource.PDisableControls;
        try
          grVal.OnRowChanged(nil);
        finally
          grVal.DataSource.PEnableControls;
        end;
      end;
      grVal.Visible := True;
    end
    else
    begin
      grVal.Visible := False;
      FCurrentItemDescr := '';
    end;

    if (TrwQBExprItemType(Integer(cbConst.Items.Objects[cbConst.ItemIndex])) >= eitNull) or
      (AnsiSameText(cbConst.Items[cbConst.ItemIndex], 'Now')) or
      (AnsiSameText(cbConst.Items[cbConst.ItemIndex], 'Today')) then
    begin
      pnlVal.Visible := False;
      if not FInsertion then
        btnIns.Click;
    end

    else
    begin
      cbExtParams.Visible :=
        (TrwQBExprItemType(Integer(cbConst.Items.Objects[cbConst.ItemIndex])) = eitParam) and
         Assigned(FQBFrm.ExternalParams);
      edValue.Visible := not cbExtParams.Visible;
      pnlVal.Visible := not grVal.Visible
    end;

  end;
end;

procedure TrwQBExprEditor.grValRowChanged(Sender: TObject);
var
  i: Integer;
begin
  if not Boolean(grVal.DataSource.PActive) or (grVal.DataSource.PRecordCount = 0) then
    Exit;

  case grVal.DataSource.Fields[0].FieldType of
    ftSmallint,
    ftInteger,
    ftWord:      FCurrentItemType := eitIntConst;

    ftFloat,
    ftCurrency,
    ftBCD:       FCurrentItemType := eitFloatConst;

    ftDate,
    ftTime,
    ftDateTime:  FCurrentItemType := eitDateConst;
  else
    FCurrentItemType := eitStrConst;
  end;

  FCurrentItemValue := grVal.DataSource.Fields[0].Value;

  if VarIsNull(FCurrentItemValue) then
  begin
    FCurrentItemType := eitNull;
    FCurrentItemValue := 'Null';
    FCurrentItemDescr := 'Null';
  end
  else
  begin
    if (FCurrentItemType = eitStrConst) and
       (Length(VarToStr(FCurrentItemValue)) > 0) and
       (VarToStr(FCurrentItemValue)[1] = '@') then
    begin
      FCurrentItemType := eitConst;
      FCurrentItemValue := Copy(VarToStr(FCurrentItemValue), 2, Length(VarToStr(FCurrentItemValue)) - 1);
    end;

    FCurrentItemDescr := '';
    for i := 1 to grVal.DataSource.Fields.Count - 1 do
      if grVal.DataSource.Fields[i].Visible then
        FCurrentItemDescr := FCurrentItemDescr + VarToStr(grVal.DataSource.Fields[i].Value) + ' ';
    FCurrentItemDescr := Trim(FCurrentItemDescr);
  end;

  if not FInsertion and not grVal.DataSource.VCLDataSet.ControlsDisabled then
    btnIns.Click;
end;

procedure TrwQBExprEditor.pcItemChanging(Sender: TObject; var AllowChange: Boolean);
begin
  FCurrentItemType := eitNone;
  FCurrentItemValue := '';
  FCurrentItemDescr := '';
end;

procedure TrwQBExprEditor.cbTablesChange(Sender: TObject);
var
  Tbl: TrwQBSelectedTable;
begin
  if cbTables.ItemIndex = -1 then
  begin
    lvFields.Items.Clear;
    Exit;
  end;

  Tbl := TrwQBSelectedTable(cbTables.Items.Objects[cbTables.ItemIndex]);

  if Tbl.IsSUBQuery then
    FillListViewWithFields(lvFields, Tbl.SubQuery.ShowingFields)
  else
    FillListViewWithFields(lvFields, Tbl.lqObject);
end;


procedure TrwQBExprEditor.tvFunctsChange(Sender: TObject; Node: TTreeNode);
begin
  AssignChValues(tsFunction);

  if not Assigned(tvFuncts.Selected) or not Assigned(tvFuncts.Selected.Data) then
  begin
    lFuncDecl.Caption := '';
    memFuncDescr.Text := '';
  end

  else
  begin
    lFuncDecl.Caption := TrwQBFunction(tvFuncts.Selected.Data).HeaderText;
    memFuncDescr.Text := TrwQBFunction(tvFuncts.Selected.Data).Description;

    tvFuncts.Selected.MakeVisible;

    if not FInsertion then
      btnIns.Click;
  end;
end;

procedure TrwQBExprEditor.tvFunctsDblClick(Sender: TObject);
begin
  if Assigned(tvFuncts.Selected) and Assigned(tvFuncts.Selected.Data) then
    btnIns.Click;
end;

procedure TrwQBExprEditor.SetResevedConstValue(AType: TrwQBExprItemType; var AValue: Variant; var ADescr: String);
begin
  case AType of
    eitNull:
      begin
        AValue := 'Null';
        ADescr := 'Null';
      end;

    eitTrue:
      begin
        AValue := '''Y''';
        ADescr := 'Yes';
      end;


    eitFalse:
      begin
        AValue := '''N''';
        ADescr := 'No';
      end;
  end;
end;

procedure TrwQBExprEditor.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOk then
  begin
    CanClose := False;
    if (FExpression.Count = 0) and not (FCurrentItemType in [eitFunction]) then
    begin
      ActiveControl := nil;
      btnIns.OnClick(nil);
    end;
    btnCheck.OnClick(nil);
    CanClose := True;
  end;
end;

procedure TrwQBExprEditor.btnCheckClick(Sender: TObject);
var
  n: Integer;
  Err: String;
begin
  Err := FExpression.CheckErrors(n);
  if n <> -1 then
  begin
    pnlExpr.SelectedItem := pnlExpr.ItemByData(FExpression[n]);
    raise ErwException.Create(Err);
  end;
end;

procedure TrwQBExprEditor.AssignChValues(APage: TTabSheet);
begin
  if APage = tsField then
  begin
    if not Assigned(lvFields.Selected) then
      Exit;

    if TObject(lvFields.Selected.Data) is TrwQBShowingField then
      FCurrentItemValue := TrwQBShowingField(lvFields.Selected.Data).InternalName

    else if TObject(lvFields.Selected.Data) is TrwDataDictField then
      FCurrentItemValue := TrwDataDictField(lvFields.Selected.Data).Name;

    FCurrentItemDescr := TrwQBSelectedTable(cbTables.Items.Objects[cbTables.ItemIndex]).InternalName;
  end

  else if APage = tsFunction then
    if not Assigned(tvFuncts.Selected) or not Assigned(tvFuncts.Selected.Data) then
      FCurrentItemValue :=  ''
    else
    begin
      FCurrentItemValue := TrwQBFunction(tvFuncts.Selected.Data).Name;
      FCurrentItemDescr := '';
    end;
end;


{ TrwLookupValuesGrid }

function TrwLookupValuesGrid.CreateVisualControl: IrwVisualControl;
begin
  Result := inherited CreateVisualControl;
  TrwDBGridControl(Result.RealObject).OnRowChanged := FOnRowChanged;
  TrwDBGridControl(Result.RealObject).OnDblClick := FOnDblClick;
end;

procedure TrwQBExprEditor.FormCreate(Sender: TObject);
begin
  grVal := TrwLookupValuesGrid.Create(nil);
  grVal.OnRowChanged := grValRowChanged;
  grVal.OnDblClick := btnInsClick;
  grVal.MultiSelection := False;
  grVal.VisualControl.RealObject.Parent := pnlGrid;
  grVal.Align := alClient;
end;


procedure TrwQBExprEditor.btnArrayBracketsClick(Sender: TObject);
var
  n: Integer;
begin
  AddItem(eitBracket, '[', '');
  n := pnlExpr.SelectedItem.ComponentIndex;
  AddItem(eitBracket, ']', '');
  pnlExpr.SelectedItem := TrwQBExprItemLabel(pnlExpr.pnlCanvas.Components[n]);
end;

end.
